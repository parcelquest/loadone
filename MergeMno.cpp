/**************************************************************************
 *
 * Notes:
 *    - GRGR file contains all recorded documents from 06/2000 to present.
 *
 * Revision
 * 07/31/2006 1.0.0     First version by Tung Bui
 * 03/08/2007 1.4.4     Adding GrGr processing - TCB
 * 03/21/2007 1.4.6     Release with GrGr update
 * 04/18/2007 1.4.11    Rework on GrGr input since the input file is a cumulative
 *                      file so we don't have to loop.
 * 11/19/2007 1.4.32    Modify Mno_ConvertChar() to handle different APN formats.
 *                      If the APN has '-' embeded, remove it before creating new CHAR rec.
 * 12/20/2007 1.5.1     Fix GrGr extract and automate it.
 * 03/18/2008 1.5.6     Use standard function to update usecode
 * 03/24/2008 1.5.7.2   Format IMAPLINK.
 * 06/04/2008 1.6.2     Fix Mno_LoadGrGr() to copy MNO_GrGr.srt to MNO_GrGr.sls
 *                      instead of append because data from the county is cumulative.
 * 06/06/2008 1.6.2.2   Modify MergeMnoRoll()
 * 06/06/2008 1.6.2.3   Update LastRecDate using GrGr data.
 * 08/29/2008 8.0.5     Adding Mno_ExtrLien() to create lien extract file.  This file
 *                      contains all LDR values and exemptions
 * 12/11/2008 8.5.1     Make StrNum left justified
 * 01/17/2009 8.5.6     Fix TRA
 * 01/23/2009 8.5.7     Fix Mno_LoadGrGrCsv() to support records that has embeded null
 *                      or lies on multiple rows.
 *
 ***************************************************************************/

#include "stdafx.h"
#include "Prodlib.h"
#include "Utils.h"
#include "CountyInfo.h"
#include "R01.h"
#include "RecDef.h"
#include "Tables.h"
#include "SaleRec.h"
#include "Logs.h"
#include "LoadOne.h"
#include "Utils.h"
#include "doOwner.h"
#include "doSort.h"
#include "doZip.h"
#include "FormatApn.h"
#include "CharRec.h"
#include "doGrGr.h"
#include "UseCode.h"
#include "LOExtrn.h"
#include "PQ.h"
#include "MergeMno.h"

static   FILE  *fdRoll, *fdChar, *fdBP;
static   long  lCharMatch, lCharSkip, lBPMatch, lBPSkip;

extern   FILE  *fdSale;

/******************************* Mno_MatchRoll ******************************
 *
 * Match GrGr file against R01 and populate APNMatch and Owner match field.
 *
 ****************************************************************************/

int Mno_MatchRoll(LPCSTR pGrGrFile)
{
   char     acBuf[2048], acRoll[2048], acTmpFile[_MAX_PATH], acTmp[256];
   char     *pTmp;
   FILE     *fdIn, *fdOut;
   GRGR_DEF *pGrGr = (GRGR_DEF *)&acBuf[0];
   int      iRet, iTmp, iNoApn, iApnMatch, iApnUnmatch, iOwnerMatch;
   HANDLE   hRoll;
   DWORD    nBytesRead;
   bool     bEof;

   // Open input file
   if (!(fdIn = fopen(pGrGrFile, "r")))
   {
      LogMsg("***** Error opening %s", pGrGrFile);
      return -1;
   }

   // Create output temp file
   strcpy(acTmpFile, pGrGrFile);
   pTmp = strrchr(acTmpFile, '.');
   strcpy(pTmp, ".tmp");
   if (!(fdOut = fopen(acTmpFile, "w")))
   {
      LogMsg("***** Error creating %s", acTmpFile);
      fclose(fdIn);
      return -2;
   }

   // Open roll file
   sprintf(acTmp, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "R01");
   if (_access(acTmp, 0))
   {
      sprintf(acTmp, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "S01");
      if (_access(acTmp, 0))
      {
         LogMsg("***** Missing input file %s.  Please check!", acTmp);
         fclose(fdIn);
         fclose(fdOut);
         return -3;
      }
   }

   LogMsg("Open input file %s", acTmp);
   hRoll = CreateFile(acTmp, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (hRoll == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening %s in Mon_MatchRoll().", acTmp);
      fclose(fdIn);
      fclose(fdOut);
      return -3;
   }

   // Skip first record
   iRet = ReadFile(hRoll, acRoll, iRecLen, &nBytesRead, NULL);
   iRet = ReadFile(hRoll, acRoll, iRecLen, &nBytesRead, NULL);

   // Initialize counters
   iNoApn=iApnMatch=iApnUnmatch=iOwnerMatch = 0;
   bEof = false;

   // Loop through input file
   while (!bEof)
   {
      // Read input record
      if (!(pTmp = fgets(acBuf, 2048, fdIn)))
         break;

      pGrGr->APN_Matched = 'N';
      pGrGr->Owner_Matched = 'N';

      // If no APN, skip
      if (pGrGr->APN[0] == ' ')
      {
         iNoApn++;
         fputs(acBuf, fdOut);    // No APN, output anyway for data entry
         continue;
      }
#ifdef _DEBUG
      //if (!memcmp(pGrGr->APN, "0104015000000", 13))
      //   iRet = 0;
#endif
      // Match APN
      do
      {
         // Compare
         iRet = memcmp(pGrGr->APN, acRoll, iGrGrApnLen);
         if (!iRet)
         {
            pGrGr->APN_Matched = 'Y';
            iApnMatch++;

            // Match owner - match last name or the first 10 bytes of grantors
            int   iIdx, iLen;
            char  acOwner[64];

            memcpy(acOwner, (char *)&acRoll[OFF_NAME1], SIZ_NAME1);
            acOwner[SIZ_NAME1] = 0;
            if (pTmp = strchr(acOwner, ' '))
            {
               iLen = pTmp - (char *)&acOwner[0];
               if (iLen < 3) iLen = 10;
            }

            for (iIdx = 0; iIdx < atoin(pGrGr->NameCnt, SIZ_GR_NAMECNT); iIdx++)
            {
               // Compare grantors only
               if (!memcmp((char *)&pGrGr->Grantors[iIdx].Name[0], acOwner, iLen))
               {
                  pGrGr->Owner_Matched = 'Y';
                  iOwnerMatch++;
                  break;
               }
            }

            // Populate with roll data - to be done when needed

            break;
         } else
         {
            if (iRet == 1)
            {
               // Read R01 record
               iTmp = ReadFile(hRoll, acRoll, iRecLen, &nBytesRead, NULL);
               // Check for EOF
               if (!iTmp)
               {
                  LogMsg("***** Error reading roll file (%f)", GetLastError());
                  bEof = true;
                  break;
               }

               // EOF ?
               if (!nBytesRead)
               {
                  bEof = true;
                  break;
               }
            }
         }
      } while (iRet > 0);

      // Output record
      fputs(acBuf, fdOut);
   }

   // Check for leftover
   while (!feof(fdIn))
   {
      // Read input record
      if (!(pTmp = fgets(acBuf, 2048, fdIn)))
         break;

      // If no APN, skip
      if (pGrGr->APN[0] == ' ')
         iNoApn++;

      pGrGr->APN_Matched = 'N';
      pGrGr->Owner_Matched = 'N';

      // Output record
      fputs(acBuf, fdOut);
   }

   if (fdIn)
      fclose(fdIn);
   if (fdOut)
      fclose(fdOut);
   if (hRoll)
      CloseHandle(hRoll);

   // If everything OK, rename output file to original file
   if (remove(pGrGrFile))
      LogMsg("***** Error removing temp file: %s (%d)", pGrGrFile, errno);
   else if (rename(acTmpFile, pGrGrFile))
      LogMsg("***** Error renaming temp file: %s --> %s (%d)", acTmpFile, pGrGrFile, errno);

   LogMsg("                     APN matched: %d", iApnMatch);
   LogMsg("                   Owner matched: %d", iOwnerMatch);
   LogMsg("                         No APN : %d", iNoApn);

   return 0;
}

/****************************** Mno_ExtrSaleMatch ***************************
 *
 * Extract data to existing Sale_Exp.dat.  This output is to be merged
 * into R01 file.  Extract only records with ApnMatch=Y.
 *
 * Return 0 if successful, otherwise error
 *
 ****************************************************************************

int Mno_ExtrSaleMatched()
{
   char     acTmp[256], acTmpFile[256], acGrGrFile[256], acBuf[1280];
   long     lCnt=0, lTmp;
   int      iRet;
   char     *pTmp, *pCnty = "MNO";
   long     lPrice, lTax;
   double   dTax;

   CString  sTmp, sApn, sType;
   FILE     *fdGrGr;
   SALE_REC SaleRec;
   GRGR_DEF *pGrGr = (GRGR_DEF *)&acBuf[0];

   sprintf(acGrGrFile, acGrGrTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "SRT");
   LogMsg("Extract matched grgr to %s", acGrGrFile);

   // Open output file
   sprintf(acTmpFile, acGrGrTmpl, pCnty, "DAT");
   if (!(fdSale = fopen(acTmpFile, "w")))
   {
      LogMsg("***** Error creating %s file", acTmpFile);
      return -1;
   }

   // Open input file
   if (!(fdGrGr = fopen(acGrGrFile, "r")))
   {
      LogMsg("***** Error creating %s file", acGrGrFile);
      return -1;
   }


   // Loop through
   while (!feof(fdGrGr))
   {
      // Get input rec
      pTmp = fgets(acBuf, 2048, fdGrGr);
      if (!pTmp)
         break;         // EOF

      // Drop all record without APN matched
      if (pGrGr->APN_Matched != 'Y')
         continue;

      // Drop records without sale price
      lPrice = atoin(pGrGr->SalePrice, SIZ_GR_SALE);
      if (lPrice < 1000)
         continue;

      // Take DEED or TRUSTEE DEED with sale price only
      if (memcmp(pGrGr->DocTitle, "DEED  ", 6) && memcmp(pGrGr->DocTitle, "TRUSTEE", 7) )
      {
         if (bDebug)
            LogMsg("** Skip doc type: %*s", sizeof(pGrGr->DocTitle), pGrGr->DocTitle);
         continue;
      }

      memset((void *)&SaleRec, 32, sizeof(SALE_REC));

      memcpy(SaleRec.acApn, pGrGr->APN, SALE_SIZ_APN);
      memcpy(SaleRec.acDocDate, pGrGr->DocDate, SALE_SIZ_DOCDATE);
      memcpy(SaleRec.acDocNum, pGrGr->DocNum, SALE_SIZ_DOCNUM);
      memcpy(SaleRec.acDocType, pGrGr->DocTitle, SALE_SIZ_DOCTYPE);

      // Reformat sale price
      iRet = sprintf(acTmp, "%d", lPrice);
      memcpy(SaleRec.acSalePrice, acTmp, iRet);

      dTax = atofn(pGrGr->Tax, 10);
      if (dTax > 0.0)
      {
         lTax = (long)(dTax*100.0);
         iRet = sprintf(acTmp, "%d", lTax);
         memcpy(SaleRec.acStampAmt, acTmp, iRet);
      }

      // Get grantors grantees
      memcpy(SaleRec.acSeller, pGrGr->Grantors[0].Name, SALE_SIZ_SELLER);

      SaleRec.CRLF[0] = '\n';
      SaleRec.CRLF[1] = 0;
      fputs((char *)&SaleRec, fdSale);
      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   fclose(fdGrGr);
   fclose(fdSale);

   // Append to cumulative file
   sprintf(acTmp, acGrGrTmpl, pCnty, "SLS");
   LogMsg("Append to %s", acTmp);
   if (appendTxtFile(acTmpFile, acTmp) < 0)
      LogMsg("*** Error appending %s to cum sale file %s", acTmpFile, acTmp);

   LogMsg("Total number of output matched records: %ld", lCnt);
   return 0;
}

/**************************** Mno_ExtrSaleMatched ***************************
 *
 * Extract data to GrGr_Exp.dat or GrGr_Exp.sls.  This output is to be merged
 * into R01 file.  Extract only records with ApnMatch=Y.
 * Input: pMode can be "w" or "a+" (overwrite or append)
 * Output: GrGr_Exp.dat or GrGr_Exp.sls in SALE_REC1 format
 * Return 0 if successful, otherwise error
 *
 ****************************************************************************/

int Mno_ExtrSaleMatched(char *pGrGrFile, char *pESaleFile, char *pMode, bool bOwner=false)
{
   char     *pTmp, acBuf[2048];
   long     lCnt=0, lPrice, lDocSkip=0;

   CString   sTmp, sApn, sType;
   FILE     *fdGrGr;
   SALE_REC1 SaleRec;
   GRGR_DEF *pGrGr = (GRGR_DEF *)&acBuf[0];

   LogMsg("Extract matched grgr from %s to %s", pGrGrFile, pESaleFile);

   // Open output file
   if (!(fdSale = fopen(pESaleFile, pMode)))
   {
      LogMsg("***** Error creating %s file", pESaleFile);
      return -1;
   }

   // Open input file
   if (!(fdGrGr = fopen(pGrGrFile, "r")))
   {
      LogMsg("***** Error creating %s file", pGrGrFile);
      return -1;
   }


   // Loop through
   while (!feof(fdGrGr))
   {
      // Get input rec
      pTmp = fgets(acBuf, 2048, fdGrGr);
      if (!pTmp)
         break;         // EOF

      // Drop all record without APN matched
      if (pGrGr->APN_Matched != 'Y')
         continue;

      // Drop records without sale price
      lPrice = atoin(pGrGr->SalePrice, SIZ_GR_SALE);
      if (lPrice < 1000)
         continue;

      // Take DEED or TRUSTEE DEED with sale price only
      if (memcmp(pGrGr->DocTitle, "DEED  ", 6) && memcmp(pGrGr->DocTitle, "TRUSTEE", 7)
          && memcmp(pGrGr->DocTitle, "BILL OF SALE", 12))
      {
         if (bDebug)
            LogMsg("** Skip doc type: %.25s APN=%.14s", pGrGr->DocTitle, pGrGr->APN);
         lDocSkip++;
         continue;
      }

      memset((void *)&SaleRec, 32, sizeof(SALE_REC1));

      memcpy(SaleRec.acApn, pGrGr->APN, SALE_SIZ_APN);
      memcpy(SaleRec.acDocDate, pGrGr->DocDate, SALE_SIZ_DOCDATE);
      memcpy(SaleRec.acDocNum, pGrGr->DocNum, SALE_SIZ_DOCNUM);
      memcpy(SaleRec.acStampAmt, pGrGr->Tax, SIZ_GR_TAX);
      memcpy(SaleRec.acSalePrice, pGrGr->SalePrice, SIZ_GR_SALE);

      // Get grantors grantees
      memcpy(SaleRec.acSeller, pGrGr->Grantors[0].Name, SALE_SIZ_SELLER);

      // Copy Owner name
      if (bOwner)
      {
         memcpy(SaleRec.acName1, pGrGr->Grantees[0].Name, SALE_SIZ_NAME);
         memcpy(SaleRec.acName2, pGrGr->Grantees[1].Name, SALE_SIZ_NAME);
      }

      SaleRec.CRLF[0] = '\n';
      SaleRec.CRLF[1] = 0;
      fputs((char *)&SaleRec, fdSale);
      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   fclose(fdGrGr);
   fclose(fdSale);

   LogMsg("Total output matched records: %ld", lCnt);
   LogMsg("      skipped records       : %ld", lDocSkip);
   return 0;
}

/******************************************************************************
 *
 * Rework on:
 *    Multiple APN
 *    Prepend APN with 0 if book number is one digit
 *
 ******************************************************************************/

int Mno_LoadGrGrCsv(char *pInfile, FILE *fdOut)
{
   FILE     *fdIn; 
   char     acBuf[2048], acBuf1[2048], acTmp[256], acTmp2[256], *pRec, *pdest, *pTmp;
   GRGR_DEF myGrGrRec;
   int      iRet, iTmp, result, iCnt=0, ReadGrGr=0;
   long     lSalePrice, lTax;

   if (!(fdIn = fopen(pInfile, "r")))
   {
      LogMsg("***** Error opening GrGr file %s", pInfile);
      return -1;
   }

   // Update GrGr file date
   iTmp = getFileDate(pInfile);
   if (iTmp > lLastGrGrDate)
      lLastGrGrDate = iTmp;

   memset(acBuf1, 0, 2048);
   pRec = fgets(acBuf, 2048, fdIn);

   while (!feof(fdIn))
   {
      printf("\r%u", iCnt);
      // Read a record - in this file, a record may be on more than one line.
      // It's safe to use myGetStrQC() instead of fgets() as normal
      iRet = myGetStrQC(acBuf, ',', 2048, fdIn);
      if (iRet <= 100)
         break;

      iRet = ParseStringNQ(acBuf, ',', MNO_GR_AW_FORM_TYPE+1, apTokens);
      if (iRet < MNO_GR_AW_FORM_TYPE)
      {
         LogMsg0("*** Invalid GrGr rec: %s", acBuf);
         continue;
      }
      memset((void *)&myGrGrRec, ' ', sizeof(GRGR_DEF));

      strcpy(acTmp, apTokens[MNO_GR_APN_NUMBER]);

#ifdef _DEBUG
      //if (!memcmp(acTmp, "39-040-03", 9) )
      //   iTmp = 0;
#endif

      // Check for multiple APN
      if (strstr(_strupr(acTmp), "VOID") != NULL)
         acTmp[0] = 0;

      if (pTmp = isCharIncluded(acTmp, 0))
         *pTmp = 0;

      if ((pTmp = strchr(acTmp, ';')) ||
          (pTmp = strchr(acTmp, ',')) ||
          (pTmp = strchr(acTmp, '.')) ||
          (pTmp = strchr(acTmp, ' ')) ||
          (pTmp = strchr(acTmp, '(')))
         *pTmp = 0;

      // 99-999-99-9999-99
      iRet = strlen(acTmp);

      if (iRet > 8)
      {
         if (acTmp[2] == '-')
         {
            if (acTmp[0] == '0' && acTmp[1] == '0')
            {
               strcpy(acTmp2, &acTmp[3]);
               strcpy(acTmp, acTmp2);
            }
            if (iRet == 6)
            {
               if (acTmp[6] != '-')
                  acTmp[0] = 0;
            }
            if (iRet == 10)
            {
               if (acTmp[10] != '-')
                  acTmp[0] = 0;
            }
         } else
         {
            if (acTmp[1] == '-')
            {
               strcpy(acTmp2, "0");
               strcat(acTmp2, acTmp);
               strcpy(acTmp, acTmp2);
            } else
               acTmp[0] = 0;
         }
      }

      remChar(acTmp, '-');
      strcat(acTmp, "0000000000000");
      acTmp[iApnLen] = 0;
      memcpy(myGrGrRec.APN, acTmp, iApnLen);

      if (strstr(acTmp, "0000000000000") == NULL)
      {
         memcpy(myGrGrRec.SourceTable, apTokens[MNO_GR_SOURCETABLE], strlen(apTokens[MNO_GR_SOURCETABLE]));

         memcpy(myGrGrRec.Grantors[0].Name, apTokens[MNO_GR_GRANTOR], strlen(apTokens[MNO_GR_GRANTOR]));
         //memcpy(myGrGrRec.Grantors[0].NameType, "O", 1);
         memcpy(myGrGrRec.Grantees[0].Name, apTokens[MNO_GR_GRANTEE], strlen(apTokens[MNO_GR_GRANTEE]));
         //memcpy(myGrGrRec.Grantees[0].NameType, "E", 1);

         memcpy(myGrGrRec.DocTitle, apTokens[MNO_GR_INSTRUMENT_TYPE], strlen(apTokens[MNO_GR_INSTRUMENT_TYPE]));

         if (strlen(apTokens[MNO_GR_AW_FILE_DATE]) > 8)
         {
            strcpy(acTmp, apTokens[MNO_GR_AW_FILE_DATE]);
            remChar(acTmp, '-');
            memcpy(myGrGrRec.DocDate, acTmp, 8);
         } else if (strlen(apTokens[MNO_GR_OR_INSTRUMENT_DATE]) > 0)
         {
            strcpy(acTmp, apTokens[MNO_GR_OR_INSTRUMENT_DATE]);
            remChar(acTmp, '-');
            memcpy(myGrGrRec.DocDate, acTmp, 8);
         }

         if (strlen(apTokens[MNO_GR_AW_INSTRUMENT_NUMBER]) > 0)
         {
            remChar(apTokens[MNO_GR_AW_INSTRUMENT_NUMBER], ' ');
            if (strlen(apTokens[MNO_GR_AW_INSTRUMENT_NUMBER]) < 10)
            {
               if (myGrGrRec.DocDate)
               {
                  strncpy(acTmp, myGrGrRec.DocDate,4);
                  strncpy(acTmp+5, "00", 2);
                  strcpy(acTmp+7, apTokens[MNO_GR_AW_INSTRUMENT_NUMBER]);
                  memcpy(myGrGrRec.DocNum, acTmp, 10);
               }
            } else
               memcpy(myGrGrRec.DocNum, apTokens[MNO_GR_AW_INSTRUMENT_NUMBER], strlen(apTokens[MNO_GR_AW_INSTRUMENT_NUMBER]));
         } else if (strlen(apTokens[MNO_GR_OR_INSTRUMENT_NUMBER]) > 0)
         {
            remChar(apTokens[MNO_GR_OR_INSTRUMENT_NUMBER], ' ');

            if (strlen(apTokens[MNO_GR_OR_INSTRUMENT_NUMBER]) < 10)
            {
               if (myGrGrRec.DocDate)
               {
                  strncpy(acTmp, myGrGrRec.DocDate,4);
                  strncpy(acTmp+5, "00", 2);
                  strcpy(acTmp+7, apTokens[MNO_GR_OR_INSTRUMENT_NUMBER]);
                  memcpy(myGrGrRec.DocNum, acTmp, 10);
               }
            } else
               memcpy(myGrGrRec.DocNum, apTokens[MNO_GR_OR_INSTRUMENT_NUMBER], strlen(apTokens[MNO_GR_OR_INSTRUMENT_NUMBER]));
         }

         // AW_DTT -> Tax
         double dTax;
         strcpy(acTmp, apTokens[MNO_GR_AW_DTT]);
         if (pTmp = strchr(acTmp, '/'))
            *pTmp = 0;
         remChar(acTmp, '$');
         remChar(acTmp, ',');
         dTax = atof(acTmp);
         if (dTax > 0.0)
         {
            lTax = (long)(dTax*100.0);
            sprintf(acTmp, "%*d", SIZ_GR_TAX, lTax);
            memcpy(myGrGrRec.Tax, acTmp, SIZ_GR_TAX);
         }

         // AW_IndicatedPrice -> Saleprice
         strcpy(acTmp, apTokens[MNO_GR_AW_INDICATED_PRICE]);
         remChar(acTmp, '$');
         remChar(acTmp, ',');
         lSalePrice = atoi(acTmp);
         if (lSalePrice > 0)
         {
            sprintf(acTmp, "%*d", SIZ_GR_SALE, lSalePrice);
            memcpy(myGrGrRec.SalePrice, acTmp, SIZ_GR_SALE);
         } else if (dTax > 0.0)
         {
            lSalePrice = (long)(dTax*SALE_FACTOR);
            sprintf(acTmp, "%*d", SIZ_GR_SALE, lSalePrice);
            memcpy(myGrGrRec.SalePrice, acTmp, SIZ_GR_SALE);
         }

         if (strlen(apTokens[MNO_GR_AW_FILE_DATE]) > 0)
         {
            strcpy(acTmp, apTokens[MNO_GR_AW_FILE_DATE]);
            remChar(acTmp, '-');
            pdest = strchr(acTmp, ' ');
            result = pdest - acTmp;

            memcpy(myGrGrRec.DocDate, acTmp, result);
         }

         iRet = atoin(myGrGrRec.DocDate, 8);
         if (iRet > lLastRecDate && iRet < lToday)
            lLastRecDate = iRet;

         // Subdivision -> Legal/reference data
         if (strlen(apTokens[MNO_GR_AW_SUBDIVISION]) > 0)
            memcpy(myGrGrRec.ReferenceData, apTokens[MNO_GR_AW_SUBDIVISION], strlen(apTokens[MNO_GR_AW_SUBDIVISION]));

         if (strlen(apTokens[MNO_GR_AW_PARCEL_COUNT]) > 0)
         {
            iTmp = atoin(apTokens[MNO_GR_AW_PARCEL_COUNT], MNOSIZ_GR_AW_PARCEL_COUNT);
            if (iTmp > 0)
            {
               sprintf(acTmp, "%*d", SIZ_GR_PRCLCNT, iTmp);
               memcpy(myGrGrRec.ParcelCount, acTmp, SIZ_GR_PRCLCNT);
            }
         }

         sprintf(acTmp, "%d    ", 1);
         memcpy(myGrGrRec.NameCnt, acTmp, SIZ_GR_NAMECNT);

         // Use AW first.  If blank, use OR
         // If InstrumentNumber is < 10 digits, format it to 6 digits (i.e. 003456)
         // then prepend it with Instrument year

         myGrGrRec.CRLF[0] = '\n';
         myGrGrRec.CRLF[1] = '\0';
         iRet = fputs((char *)&myGrGrRec, fdOut);
      }
      memset(acBuf,0,2048);

      if (!(++iCnt % 1000))
         printf("\r%u", iCnt);
   }

   if (fdIn) fclose(fdIn);
   if (fdOut) fclose(fdOut);

   LogMsg("Number of GrGr record output:  %d\n", iCnt);
   printf("\nNumber of GrGr record output:  %d\n", iCnt);

   return iCnt;
}

/********************************* Mno_LoadGrGr *****************************
 *
 * If successful, return 0.  Otherwise error.
 *
 ****************************************************************************/

int Mno_LoadGrGr(char *pGrGr)
{
   char     acTmp[256];
   char     acGrGrIn[_MAX_PATH], acGrGrOut[_MAX_PATH];

   FILE     *fdOut;

   int      iCnt, iRet;
   long     lCnt;

   // Create Output file - Mno_GrGr.dat
   sprintf(acGrGrOut, acGrGrTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "Dat");
   LogMsg("Open output file %s", acGrGrOut);
   fdOut = fopen(acGrGrOut, "w");
   if (fdOut == NULL)
   {
      LogMsg("Error creating GrGr output file: %s\n", acGrGrOut);
      return -2;
   }

   iCnt = lCnt = 0;

   // Parse input file
   lCnt = Mno_LoadGrGrCsv(pGrGr, fdOut);
   if (lCnt < 0)
      LogMsg("*** Bad input file: %s", pGrGr);

   // Close files
   if (fdOut)
      fclose(fdOut);

   LogMsg("Total processed records  : %u", lCnt);

   // Sort output
   if (lCnt > 0)
   {
      // Sort output file and dedup if same docdate and docnum
      // Sort on APN asc, DocNum asc, RecDate asc
      //sprintf(acTmp,"S(82,10,C,A, 92,10,C,A) F(TXT) DUPO(B%d, 1,101) ", sizeof(GRGR_DEF)+64);
      sprintf(acTmp,"S(17,13,C,A,1,10,C,A,37,8,C,A,1109,2,C,A) F(TXT) DUPO(B%d, 1,44) ", sizeof(GRGR_DEF)+64);
      strcpy(acGrGrIn, acGrGrOut);
      sprintf(acGrGrOut, acGrGrTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "Srt");

      // Sort Mon_GrGr.dat to Mon_GrGr.srt
      lCnt = sortFile(acGrGrIn, acGrGrOut, acTmp);

      // Match with roll file
      //iRet = Mno_MatchRoll(acGrGrOut);
      iRet = GrGr_MatchRoll(acGrGrOut, iGrGrApnLen);

      // Update cumulative sale file
      if (lCnt > 0)
      {
         // Append Mon_GrGr.srt to Mon_GrGr.sls for backup
         // This may not be needed since input file already contains all history.
         // So we do copy instead.
         sprintf(acTmp, acGrGrTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "Sls");
         iRet = CopyFile(acGrGrOut, acTmp, false);

         // Extract to GrGr_Exp.dat
         sprintf(acTmp, acEGrGrTmpl, myCounty.acCntyCode, "dat");
         iRet = Mno_ExtrSaleMatched(acGrGrOut, acTmp, "w", false);
      } else
         iRet = 1;
   } else
      iRet = 1;

   LogMsg("Total output records after dedup: %u", lCnt);
   printf("\nTotal output records after dedup: %u\n", lCnt);

   return iRet;
}


/******************************** Mno_MergeOwner *****************************
 *
 * Return 0 if successful, >0 if error
 *
 *****************************************************************************/

void Mno_MergeOwner(char *pOutbuf, char *pNames)
{
   int   iTmp, iTmp1;
   char  acTmp[128], acSave[64], acSave1[64], *pTmp, *pTmp1;
   char  acName2[64], acOwners[64];
   OWNER myOwner;

   // Clear output buffer if needed
   memset(pOutbuf+OFF_NAME1, ' ', SIZ_NAME1+SIZ_NAME2);
   memset(pOutbuf+OFF_VEST, ' ', SIZ_VEST);
   memset(pOutbuf+OFF_NAME_SWAP, ' ', SIZ_NAME_SWAP);
   memset(pOutbuf+OFF_NAME_TYPE1, ' ', SIZ_NAME_TYPE1);
   memset(pOutbuf+OFF_NAME_TYPE2, ' ', SIZ_NAME_TYPE2);

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "6420021000000", 13) )
   //   iTmp = 0;
#endif

   // Remove multiple spaces
   pTmp = strcpy(acOwners, pNames);
   replChar(acOwners, ',', ' ');
   replChar(acOwners, '.', ' ');
   iTmp = iTmp1 = 0;
   while (*pTmp)
   {
      // Mark name with numeric value
      if (isdigit(*pTmp))
         iTmp1++;

      // skip unwanted chars at beginning of name
      if (*pTmp < '1' && iTmp == 0)
      {
         pTmp++;
         continue;
      }

      acTmp[iTmp++] = *pTmp;
      // Remove multi-space
      if (*pTmp == ' ')
         while (*pTmp == ' ') pTmp++;
      else
         pTmp++;
   }
   if (acTmp[iTmp-1] == ' ')
      iTmp -= 1;
   acTmp[iTmp] = 0;

   acName2[0] = 0;
   acSave1[0] = 0;
   acSave[0] = 0;

   // Testing
   if ((pTmp=strstr(acTmp, " ETAL &")) || (pTmp=strstr(acTmp, " ET AL &")) )
   {
      // Remove filtered words
      memset(pTmp+1, ' ', 5);
      blankRem(pTmp);
   }
   if ((pTmp=strstr(acTmp, " TRUST &")))
   {
      strcpy(acSave1, " TRUST");
      // Remove filtered words
      memset(pTmp+1, ' ', 5);
      blankRem(pTmp);
   }

   // Drop everything from these words
   if (pTmp=strstr(acTmp, "(ET AL"))
      *pTmp = 0;
   else if (pTmp=strstr(acTmp, " ETAL"))
      *pTmp = 0;
   else if (pTmp=strstr(acTmp, " ET AL"))
      *pTmp = 0;
   else if (pTmp=strstr(acTmp, " CO-TR"))
      *pTmp = 0;
   else if (pTmp=strstr(acTmp, ",FAM"))
      *pTmp = 0;


   if (pTmp = strchr(acTmp, '&'))
   {
      if (pTmp1 = strchr(pTmp+1, '&'))
      {
         *pTmp1++ = 0;
         if (*pTmp1 == ' ') pTmp1++;
         strcpy(acName2, pTmp1);
      }
   }

   // Now parse owners
   acTmp[63] = 0;
   splitOwner(acTmp, &myOwner, 0);
   if (acSave1[0])
   {
      if (pTmp = strchr(myOwner.acName1, '&'))
      {
         *(pTmp-1) = 0;
         strcpy(acTmp, myOwner.acName1);
         strcat(acTmp, acSave1);
         pTmp--;
         *pTmp = ' ';
         strcat(acTmp, pTmp);
         strcpy(myOwner.acName1, acTmp);
      } else
         strcat(myOwner.acName1, acSave1);
   }

   if (acSave[0])
      strcat(myOwner.acName1, acSave);


   blankRem(myOwner.acName1);
   iTmp = strlen(myOwner.acName1);
   if (iTmp > SIZ_NAME1)
      iTmp = SIZ_NAME1;
   memcpy(pOutbuf+OFF_NAME1, myOwner.acName1, iTmp);

   iTmp = strlen(myOwner.acSwapName);
   if (iTmp > SIZ_NAME_SWAP)
      iTmp = SIZ_NAME_SWAP;
   memcpy(pOutbuf+OFF_NAME_SWAP, myOwner.acSwapName, iTmp);


}

/******************************** Mno_MergeMAdr ******************************
 *
 * Merge Mail address
 *
 *****************************************************************************/

void Mno_MergeMAdr(char *pOutbuf, char *pRollRec)
{
   MNO_LIEN   *pRec;

   char       *pAddr1, acTmp[256], acAddr1[64];
   int        iTmp;
   bool       bNoMail = false;

   ADR_REC  sMailAdr;

   pRec = (MNO_LIEN *)pRollRec;

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "3102015001300", 13) )
   //   lTmp = 0;
#endif

   // Clear old Mailing
   memset(pOutbuf+OFF_M_STRNUM, ' ', SIZ_M_ADDR);
   memset(pOutbuf+OFF_M_ADDR_D, ' ', SIZ_M_ADDRB_D);

   // Check for blank address
   if (memcmp(pRec->Mail_Addr_1, "     ", 5))
   {
      memcpy(acAddr1, pRec->Mail_Addr_1, MNOSIZ_LIEN_MAIL_ADDR_1);
      pAddr1 = myTrim(acAddr1, MNOSIZ_LIEN_MAIL_ADDR_1);

      // Parsing mail address
      memset((void *)&sMailAdr, 0, sizeof(ADR_REC));
      parseMAdr1(&sMailAdr, acAddr1);

      if (sMailAdr.lStrNum > 0)
      {
         sprintf(acTmp, "%d       ", sMailAdr.lStrNum);
         memcpy(pOutbuf+OFF_M_STRNUM, acTmp, SIZ_M_STRNUM);
         if (sMailAdr.strSub[0] > '0')
         {
            sprintf(acTmp, "%s  ", sMailAdr.strSub);
            memcpy(pOutbuf+OFF_M_STR_SUB, acTmp, SIZ_M_STR_SUB);
         }
      }
      blankPad(sMailAdr.strDir, SIZ_M_DIR);
      memcpy(pOutbuf+OFF_M_DIR, sMailAdr.strDir, SIZ_M_DIR);
      blankPad(sMailAdr.strName, SIZ_M_STREET);
      memcpy(pOutbuf+OFF_M_STREET, sMailAdr.strName, SIZ_M_STREET);
      blankPad(sMailAdr.strSfx, SIZ_M_SUFF);
      memcpy(pOutbuf+OFF_M_SUFF, sMailAdr.strSfx, SIZ_M_SUFF);
      if (sMailAdr.Unit[0] > ' ')
      {
         if (sMailAdr.Unit[0] == '#')
            memcpy(pOutbuf+OFF_M_UNITNO, sMailAdr.Unit, strlen(sMailAdr.Unit));
         else
         {
            *(pOutbuf+OFF_M_UNITNO) = '#';
            memcpy(pOutbuf+OFF_M_UNITNO+1, sMailAdr.Unit, strlen(sMailAdr.Unit));
         }
      }

      memcpy(pOutbuf+OFF_M_ADDR_D, acAddr1, strlen(acAddr1));

      ADR_REC sAdrRec;
      memset((char *)&sAdrRec, 0, sizeof(ADR_REC));

      memcpy(acTmp, pRec->Mail_City_State, MNOSIZ_LIEN_MAIL_CITY_STATE);
      blankRem(acTmp, MNOSIZ_LIEN_MAIL_CITY_STATE);
      parseMAdr2(&sAdrRec, acTmp);

      if (sAdrRec.City[0] > ' ')
      {
         iTmp = strlen(sAdrRec.City);
         if (iTmp > SIZ_M_CITY)
            iTmp = SIZ_M_CITY;
         memcpy(pOutbuf+OFF_M_CITY, sAdrRec.City, iTmp);
         memcpy(pOutbuf+OFF_M_ST, sAdrRec.State, strlen(sAdrRec.State));

         iTmp = sprintf(acTmp, "%s %s", sAdrRec.City, sAdrRec.State);
         memcpy(pOutbuf+OFF_M_CTY_ST_D, acTmp, iTmp);
      }


      memcpy(acTmp, pRec->Zip_Zip_4, MNOSIZ_LIEN_ZIP_ZIP_4);

      if (memcmp(acTmp, "     ", 5))
      {
         memcpy(pOutbuf+OFF_M_ZIP, pRec->Zip_Zip_4, SIZ_M_ZIP);
         if (memcmp(pRec->Zip_Zip_4, "-", 1))
            memcpy(pOutbuf+OFF_M_ZIP4, pRec->Zip_Zip_4+6, SIZ_M_ZIP4);
         else
            memcpy(pOutbuf+OFF_M_ZIP4, BLANK32, SIZ_M_ZIP4);
      }
   }
}

/******************************** Mno_MergeChar ******************************
 *
 * Merge Char
 *
 *****************************************************************************/

int Mno_MergeChar(char *pOutbuf)
{
   static char    acRec[1024], *pRec=NULL;
   char           acTmp[256], acCode[256];
   long           lTmp, lBldgSqft, lGarSqft;
   int            iRet, iTmp, iLoop, iBeds, iFBath, iHBath, iFp, iRooms;
   MNO_CHAR       *pChar;

   iRet=iBeds=iFBath=iHBath=iFp=iRooms=0;
   lBldgSqft=lGarSqft=0;

   // Get first Char rec for first call
   if (!pRec)
      pRec = fgets(acRec, 1024, fdChar);

   do
   {
      // Compare Apn
      iLoop = memcmp(pOutbuf, acRec, 7);
      if (iLoop > 0)
      {
         if (bDebug)
            LogMsg0("Skip Char rec  %.*s", iApnLen, pRec);

         pRec = fgets(acRec, 1024, fdChar);
         if (!pRec)
         {
            fclose(fdChar);
            fdChar = NULL;
            return 1;      // EOF
         }
         lCharSkip++;
      }
   } while (iLoop > 0);

   // If not match, return
   if (iLoop)
      return 1;

   pChar = (MNO_CHAR *)acRec;

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "01040150", 7) )
   //   iTmp = 0;
#endif

   // YrEff
   lTmp = atoin(pChar->EffYr, 4);
   if (lTmp > 1700)
      memcpy(pOutbuf+OFF_YR_EFF, pChar->EffYr, SIZ_YR_BLT);

   lBldgSqft = atoin(pChar->Sqft, MNOSIZ_CHAR_SQFT);
   if (lBldgSqft > 0)
   {
      sprintf(acTmp, "%*d", SIZ_BLDG_SF, lBldgSqft);
      memcpy(pOutbuf+OFF_BLDG_SF, acTmp, SIZ_BLDG_SF);
   }

   // Yrblt
   lTmp = atoin(pChar->YrBlt, MNOSIZ_CHAR_YRBLT);
   if (lTmp > 1700)
      memcpy(pOutbuf+OFF_YR_BLT, pChar->YrBlt, SIZ_YR_BLT);

   // Stories
   iTmp = atoin(pChar->Stories, MNOSIZ_CHAR_STORIES);
   if (iTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_STORIES, iTmp*10);
      memcpy(pOutbuf+OFF_STORIES, acTmp, SIZ_STORIES);
   }

   // Beds
   iBeds = atoin(pChar->Beds, MNOSIZ_CHAR_BEDS);
   if (iBeds > 0)
   {
      sprintf(acTmp, "%*d", SIZ_BEDS, iBeds);
      memcpy(pOutbuf+OFF_BEDS, acTmp, SIZ_BEDS);
   }

   // Bath
   if (pChar->Bath[0] > '0')
   {
      iFBath = pChar->Bath[0] & 0x0F;
      sprintf(acTmp, "%*d", SIZ_BATH_F, iFBath);
      memcpy(pOutbuf+OFF_BATH_F, acTmp, SIZ_BATH_F);

   }

   // Half bath
   if (pChar->HBath[0] > '0')
   {
      iFBath = pChar->HBath[0] & 0x0F;
      sprintf(acTmp, "%*d", SIZ_BATH_H, iFBath);
      memcpy(pOutbuf+OFF_BATH_H, acTmp, SIZ_BATH_H);
   }

   // Rooms
   iRooms = atoin(pChar->Rooms, MNOSIZ_CHAR_ROOMS);
   if (iRooms > 0)
   {
      sprintf(acTmp, "%*d", SIZ_ROOMS, iRooms);
      memcpy(pOutbuf+OFF_ROOMS, acTmp, SIZ_ROOMS);
   }


   // Gargage Size
   lGarSqft = atoin(pChar->GSize, MNOSIZ_CHAR_GSIZE);
   if (lGarSqft > 0)
   {
      sprintf(acTmp, "%*d", SIZ_GAR_SQFT, lGarSqft);
      memcpy(pOutbuf+OFF_GAR_SQFT, acTmp, SIZ_GAR_SQFT);
      *(pOutbuf+OFF_PARK_TYPE) = '2';
   }


   // Fireplace
   if (pChar->FirePl[0] > ' ')
      memcpy(pOutbuf+OFF_FIRE_PL, " 1", 2);

   // Pool/Spa
   if (pChar->Pool[0] > ' ')
      *(pOutbuf+OFF_POOL) = 'P';       // Pool

   // BldgClass
   if (pChar->Class[0] > ' ')
      *(pOutbuf+OFF_BLDG_CLASS) = pChar->Class[0];

   // BldgQual
   if (pChar->Qual[0] > ' ')
   {
      double fTmp;
      fTmp = atof(pChar->Qual);
      sprintf(acTmp, "%1.1f", fTmp);
      iRet = Value2Code(acTmp, acCode, NULL);
      blankPad(acCode, SIZ_BLDG_QUAL);
      memcpy(pOutbuf+OFF_BLDG_QUAL, acCode, SIZ_BLDG_QUAL);
   }

   // Sewer
   if (pChar->Septic[0] == 'Y')
      *(pOutbuf+OFF_SEWER) = 'S';

   // Water
   if (pChar->Well[0] > ' ')
      *(pOutbuf+OFF_WATER) = pChar->Well[0];

   lCharMatch++;

   // Get next Char rec
   pRec = fgets(acRec, 1024, fdChar);

   return 0;
}

/******************************** Mno_MergeBParcels***************************
 *
 * Merge BParcels
 *
 *****************************************************************************/

int Mno_MergeBP(char *pOutbuf)
{
   static char    acRec[1024], *pRec=NULL;
   char           acTmp[256], acAddr1[256], acCode[64];
   long           lTmp;
   int            iLoop, iRet;
   MNO_BP         *pBP;

   // Get first BP rec for first call
   if (!pRec)
      pRec = fgets(acRec, 1024, fdBP);

   do
   {
      // Compare Apn
      iLoop = memcmp(pOutbuf, acRec, iApnLen);
#ifdef _DEBUG
      //if (!memcmp(pOutbuf, "0104015", 7) )
      //   iTmp = 0;
#endif
      if (iLoop > 0)
      {
         if (bDebug)
            LogMsg0("Skip BParcels rec  %.*s", myCounty.iApnLen, pRec);

         pRec = fgets(acRec, 1024, fdBP);
         if (!pRec)
         {
            fclose(fdChar);
            fdBP = NULL;
            return 1;      // EOF
         }
         lBPSkip++;
      }
   } while (iLoop > 0);

   // If not match, return
   if (iLoop)
      return 1;

   pBP = (MNO_BP *)acRec;

   // ZONING
   if (pBP->Zoning[0] > ' ' && memcmp(pBP->Zoning, "N/A", 3))
      memcpy(pOutbuf+OFF_ZONE, pBP->Zoning, SIZ_ZONE);

   // TRA
   lTmp = atoin(pBP->Tra, MNOSIZ_BP_TRA);
   if (lTmp > 0)
   {
      iRet = sprintf(acTmp, "%.6d", lTmp);
      memcpy(pOutbuf+OFF_TRA, acTmp, iRet);
   }

   // Usecode
   if (*(pOutbuf+OFF_USE_CO) == ' ' && pBP->UseCode[0] > '0')
   {
      //memset(pOutbuf+OFF_USE_STD, ' ', SIZ_USE_CO+SIZ_USE_STD);
      memcpy(pOutbuf+OFF_USE_CO, pBP->UseCode, SIZ_USE_CO);
      updateStdUse(pOutbuf+OFF_USE_STD, pBP->UseCode, SIZ_USE_CO);
   }

   // Initialize
   acAddr1[0] = 0;
   lTmp = atoin(pBP->St_Num, MNOSIZ_BP_NUM);
   if (lTmp > 0)
   {
      sprintf(acAddr1, "%d       ", lTmp);
      memcpy(pOutbuf+OFF_S_STRNUM, acAddr1, SIZ_S_STRNUM);
      sprintf(acAddr1, "%d %.*s %.*s", lTmp, MNOSIZ_BP_PREFIX, pBP->St_Prefix, MNOSIZ_BP_STREET, pBP->Street);
      blankRem(acAddr1, SIZ_S_ADDR_D);
      memcpy(pOutbuf+OFF_S_ADDR_D, acAddr1, strlen(acAddr1));
   }

   ADR_REC AdrRec;

   acTmp[0] = 0;
   if (pBP->St_Prefix[0] > ' ')
   {
      sprintf(acAddr1, "%.*s %.*s", MNOSIZ_BP_PREFIX, pBP->St_Prefix, MNOSIZ_BP_STREET, pBP->Street);
      blankRem(acAddr1, SIZ_S_STREET);
      memcpy(pOutbuf+OFF_S_STREET, acAddr1, strlen(acAddr1));
   } else if (pBP->Street[0] > ' ')
   {
      memcpy(acTmp, pBP->Street, MNOSIZ_BP_STREET);
      blankRem(acTmp, MNOSIZ_BP_STREET);
      parseAdrDNS(&AdrRec, acTmp);
      memcpy(pOutbuf+OFF_S_STREET, AdrRec.strName, strlen(AdrRec.strName));
      if (AdrRec.strDir[0] > ' ')
         memcpy(pOutbuf+OFF_S_DIR, AdrRec.strDir, strlen(AdrRec.strDir));
      if (AdrRec.strSfx[0] > ' ')
         memcpy(pOutbuf+OFF_S_SUFF, AdrRec.strSfx, strlen(AdrRec.strSfx));
      if (AdrRec.Unit[0] > ' ')
         memcpy(pOutbuf+OFF_S_UNITNO, AdrRec.Unit, strlen(AdrRec.Unit));
   }

   if (pBP->City[0] > ' ')
   {
      memcpy(acTmp, pBP->City, MNOSIZ_BP_CITY);
      blankRem(acTmp, SIZ_S_CITY);
      City2Code(acTmp, acCode, pBP->APN);
      if (acCode[0] > ' ')
      {
         memcpy(pOutbuf+OFF_S_CITY, acCode, strlen(acCode));
         memcpy(pOutbuf+OFF_S_ST, "CA",2);
         strcat(acTmp, " CA");
         memcpy(pOutbuf+OFF_S_CTY_ST_D, acTmp, strlen(acTmp));
      }
   }

   lBPMatch++;

   // Get next Char rec
   pRec = fgets(acRec, 1024, fdBP);

   return 0;
}
/********************************* Mno_MergeRoll *****************************
 *
 * Return 0 if successful, < 0 if error
 *        1 retired record, not use
 *
 *****************************************************************************/

int Mno_MergeRoll(char *pOutbuf, char *pRollRec, int iLen, int iFlag)
{
   char     acTmp[256], acTmp1[256], *pTmp, *pSave;
   long     lTmp;
   int      iRet=0, iTmp;
   MNO_LIEN *pRec;

   pRec = (MNO_LIEN *)pRollRec;

   if (iFlag & CREATE_R01)
   {
      // Clear output buffer
      memset(pOutbuf, ' ', iRecLen);

      // Start copying data
      memcpy(pOutbuf, pRec->Apn, MNOSIZ_LIEN_APN);
      *(pOutbuf+OFF_STATUS) = 'A';

      // Format APN
      iRet = formatApn(pRec->Apn, acTmp, &myCounty);
      memcpy(pOutbuf+OFF_APN_D, acTmp, iRet);

      // Create MapLink and output new record
      iRet = formatMapLink(pRec->Apn, acTmp, &myCounty);
      memcpy(pOutbuf+OFF_MAPLINK, acTmp, iRet);

      // Create index map link
      if (getIndexPage(acTmp, acTmp1, &myCounty))
         memcpy(pOutbuf+OFF_IMAPLINK, acTmp1, iRet);

      // County code
      memcpy(pOutbuf+OFF_CO_NUM, "26MNO", 5);

      // Year assessed
      memcpy(pOutbuf+OFF_YR_ASSD, myCounty.acYearAssd, 4);


      // HO Exempt
      lTmp = atoin(pRec->Homeowner_Exemp, MNOSIZ_LIEN_HOMEOWNER_EXEMP);
      if (lTmp == 7000)
      {
         *(pOutbuf+OFF_HO_FL) = '1';      // 'Y'
         sprintf(acTmp, "%*d", SIZ_EXE_TOTAL, lTmp);
         memcpy(pOutbuf+OFF_EXE_TOTAL, acTmp, SIZ_EXE_TOTAL);
      } else
         *(pOutbuf+OFF_HO_FL) = '2';      // 'N'

      // Land
      long lLand = atoin(pRec->Land, MNOSIZ_LIEN_LAND);
      if (lLand > 0)
      {
         sprintf(acTmp, "%*d", SIZ_LAND, lLand);
         memcpy(pOutbuf+OFF_LAND, acTmp, SIZ_LAND);
      }

      // Improve
      long lImpr = atoin(pRec->Improv, MNOSIZ_LIEN_IMPROV);
      if (lImpr > 0)
      {
         sprintf(acTmp, "%*d", SIZ_IMPR, lImpr);
         memcpy(pOutbuf+OFF_IMPR, acTmp, SIZ_IMPR);
      }

      // Other value: FixtureRealProperty, PPBusiness, PPMH
      lTmp = atoin(pRec->Other_Value, MNOSIZ_LIEN_OTHER_VALUE);
      lTmp += atoin(pRec->Pers_Prop, MNOSIZ_LIEN_PERS_PROP);
      lTmp += atoin(pRec->Trade_Fixt, MNOSIZ_LIEN_TRADE_FIXT);
      lTmp += atoin(pRec->Mob_Hm, MNOSIZ_LIEN_MOB_HM);
      if (lTmp > 0)
      {
         sprintf(acTmp, "%*d", SIZ_OTHER, lTmp);
         memcpy(pOutbuf+OFF_OTHER, acTmp, SIZ_OTHER);
      }

      // Gross total
      lTmp += (lLand+lImpr);
      if (lTmp > 0)
      {
         sprintf(acTmp, "%*d", SIZ_GROSS, lTmp);
         memcpy(pOutbuf+OFF_GROSS, acTmp, SIZ_GROSS);
      }

      // Ratio
      if (lImpr > 0)
      {
         sprintf(acTmp, "%*d", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
         if (atoi(acTmp) > 0)
            memcpy(pOutbuf+OFF_RATIO, acTmp, SIZ_RATIO);
      }

      // TRA
      lTmp = atoin(pRec->Tra, MNOSIZ_LIEN_TRA);
      if (lTmp > 0)
      {
         iTmp = sprintf(acTmp, "%.6d", lTmp);
         memcpy(pOutbuf+OFF_TRA, acTmp, iTmp);
      }
   }

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "3113003000000", 13) )
   //   iTmp = 0;
#endif

   // CARE OF
   strncpy(acTmp, pRec->Careof, MNOSIZ_LIEN_CAREOF);

   if (acTmp[0] > ' ')
   {
      if (!memcmp(acTmp, "C/O ATTN:", 9) || !memcmp(acTmp, "C/O ATTN.", 9))
         pTmp = (char *)&acTmp[9];
      else if (!memcmp(acTmp, "C/O ATTN", 9))
         pTmp = (char *)&acTmp[8];
      else if (!memcmp(acTmp, "C/O", 3))
         pTmp = (char *)&acTmp[3];
      else if (!memcmp(acTmp, "ATTN", 4))
         pTmp = (char *)&acTmp[4];

      if (pSave=strstr(pTmp, "/"))
         *pSave = 0;

      blankRem(pTmp,SIZ_CARE_OF);
      iTmp = strlen(pTmp);
      if (iTmp > SIZ_CARE_OF)
         iTmp = SIZ_CARE_OF;
      memset(pOutbuf+OFF_CARE_OF, ' ', SIZ_CARE_OF);
      memcpy(pOutbuf+OFF_CARE_OF, pTmp, iTmp);
   }


   // Owner
   strncpy(acTmp, pRec->Owner_Name, MNOSIZ_LIEN_OWNER_NAME);
   blankRem(acTmp, MNOSIZ_LIEN_OWNER_NAME);
   Mno_MergeOwner(pOutbuf, acTmp);

   // Mailing
   Mno_MergeMAdr(pOutbuf, pRollRec);

   return 0;
}

/********************************** MergeMnoRoll ******************************
 *
 * This function has not been tested.
 *
 ******************************************************************************/

int MergeMnoRoll(int iSkip)
{
   char     *pTmp, acRec[MAX_RECSIZE], acBuf[MAX_RECSIZE], acRollRec[MAX_RECSIZE];
   char     cFileCnt=1;
   char     acRawFile[_MAX_PATH], acOutFile[_MAX_PATH];

   HANDLE   fhIn, fhOut;
   FILE     *fdRoll;

   int      iRet, iTmp, iRollUpd=0, iNewRec=0, iRetiredRec=0;
   DWORD    nBytesRead;
   DWORD    nBytesWritten;
   BOOL     bRet, bEof;
   long     lRet=0, lCnt=0;

   sprintf(acRawFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "S01");
   sprintf(acOutFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "R01");

   // Rename files for processing

   if (_access(acRawFile, 0))
   {
      if (!_access(acOutFile, 0))
         rename(acOutFile, acRawFile);
      else
      {
         LogMsg("***** Missing input file %s.  Please recheck!", acRawFile);
         return 1;
      }
   }

   // Open updated roll file
   // Open roll file
   LogMsg("Open Roll file %s", acRollFile);
   fdRoll = fopen(acRollFile, "r");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", acRollFile);
      return 2;
   }

   // Open Char file
   if (!_access(acCharFile, 0))
   {
      LogMsg("Open Char file %s", acCharFile);
      fdChar = fopen(acCharFile, "r");
      if (fdChar == NULL)
      {
         LogMsg("***** Error opening Char file: %s\n", acCharFile);
         return 2;
      }
   } else
      LogMsg("*** Missing Char file %s", acCharFile);

   // Open BParcels file
   if (!_access(acBPFile, 0))
   {
      LogMsg("Open BP file %s", acBPFile);
      fdBP = fopen(acBPFile, "r");
      if (fdBP == NULL)
      {
         LogMsg("***** Error opening BP file: %s\n", acBPFile);
         return 2;
      }
   } else
      LogMsg("*** Missing BP file %s", acBPFile);

   // Open Input file
   LogMsg("Open input file %s", acRawFile);
   fhIn = CreateFile(acRawFile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhIn == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening input file: %s\n", acRawFile);
      return 3;
   }

   // Open Output file - use tmp file so we can sort at the end to R01 file
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening output file: %s\n", acOutFile);
      return 4;
   }

   pTmp = fgets((char *)&acRollRec[0], MAX_RECSIZE, fdRoll);
   bEof = (pTmp ? false:true);

   // Copy skip record
   memset(acBuf, ' ', iRecLen);
   while (iSkip-- > 0)
   {
      ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);
      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
   }

   iNoMatch=iBadCity=iBadSuffix=0;

   // Merge loop
   while (!bEof)
   {
      bRet = ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);

      // Check for EOF
      if (!bRet)
      {
         LogMsg("***** Error reading input file %s (%f)", acRawFile, GetLastError());
         break;
      }

      // EOF ?
      if (!nBytesRead)
         break;

NextRollRec:
      iTmp = memcmp(acBuf, (char *)&acRollRec[2], iApnLen);
      if (!iTmp)
      {
#ifdef _DEBUG
         //if (!memcmp(acBuf, "3113003000000", 13) )
         //   iTmp = 0;
#endif
         // Create R01 record from roll data
         iRet = Mno_MergeRoll(acBuf, acRollRec, MAX_RECSIZE, UPDATE_R01);

         // Merge Char
         if (fdChar)
            lRet = Mno_MergeChar(acBuf);

         // Merge situs
         if (fdBP)
            lRet = Mno_MergeBP(acBuf);

         bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
         iRollUpd++;
         lCnt++;

         // Save last recording date
         lRet = atoin((char *)&acBuf[OFF_TRANSFER_DT], 8);
         if (lRet > lLastRecDate && lRet < lToday)
            lLastRecDate = lRet;

         // Read next roll record
         pTmp = fgets(acRollRec, MAX_RECSIZE, fdRoll);

         if (!pTmp)
            bEof = true;    // Signal to stop
      } else if (iTmp > 0)       // Roll not match, new roll record?
      {
         if (bDebug)
            LogMsg0("New roll record : %.*s (%d)", iApnLen, &acRollRec[2], lCnt);

         // Create new R01 record
         iRet = Mno_MergeRoll(acRec, acRollRec, MAX_RECSIZE, CREATE_R01);

         // Merge Char
         if (fdChar)
            lRet = Mno_MergeChar(acRec);
         if (fdBP)
            lRet = Mno_MergeBP(acRec);

         // Save last recording date
         lRet = atoin((char *)&acRec[OFF_TRANSFER_DT], 8);
         if (lRet > lLastRecDate && lRet < lToday)
            lLastRecDate = lRet;

         iNewRec++;
         bRet = WriteFile(fhOut, acRec, iRecLen, &nBytesWritten, NULL);
         lCnt++;

         // Get next roll record
         pTmp = fgets(acRollRec, MAX_RECSIZE, fdRoll);

         if (!pTmp)
            bEof = true;    // Signal to stop
         else
            goto NextRollRec;
      } else
      {
         // Record may be retired
         if (bDebug)
            LogMsg0("Retired record? : R01->%.*s < Roll->%.*s (%d)", iApnLen, acBuf, iApnLen, (char *)&acRollRec[2], lCnt);
         iRetiredRec++;

         continue;
      }

      if (!(lCnt % 1000))
         printf("\r%u", lCnt);

      if (!bRet)
      {
         LogMsg("***** Error occurs: %d\n", GetLastError());
         lRet = WRITE_ERR;
         break;
      }
   }

   // Do the rest of the file
   while (!bEof)
   {
      if (bDebug)
         LogMsg0("New roll record : %.*s (%d) ", iApnLen, acRollRec[2], lCnt);

      // Create new R01 record
      iRet = Mno_MergeRoll(acRec, acRollRec, MAX_RECSIZE, CREATE_R01);

      // Merge Char
      if (fdChar)
         lRet = Mno_MergeChar(acRec);
      if (fdBP)
         lRet = Mno_MergeBP(acRec);

      // Save last recording date
      lRet = atoin((char *)&acRec[OFF_TRANSFER_DT], 8);
      if (lRet > lLastRecDate && lRet < lToday)
         lLastRecDate = lRet;

      iNewRec++;
      bRet = WriteFile(fhOut, acRec, iRecLen, &nBytesWritten, NULL);
      lCnt++;

      // Get next roll record
      pTmp = fgets(acRollRec, MAX_RECSIZE, fdRoll);

      if (!pTmp)
         bEof = true;    // Signal to stop
   }


   // Close files
   if (fdRoll)
      fclose(fdRoll);
   if (fdChar)
      fclose(fdChar);
   if (fdBP)
      fclose(fdBP);
   if (fhOut)
      CloseHandle(fhOut);
   if (fhIn)
      CloseHandle(fhIn);

   LogMsg("Total output records:       %u", lCnt);
   LogMsg("Total new records:          %u", iNewRec);
   LogMsg("Total retired records:      %u", iRetiredRec);
   LogMsg("Total bad-city records:     %u", iBadCity);
   LogMsg("Total bad-suffix records:   %u\n", iBadSuffix);

   LogMsg("Number of Char matched:     %u", lCharMatch);
   LogMsg("Number of Char skiped:      %u", lCharSkip);
   LogMsg("Number of BP matched:       %u", lBPMatch);
   LogMsg("Number of BP skiped:        %u", lBPSkip);

   LogMsg("Last recording date:        %u", lLastRecDate);

   printf("\nTotal output records: %u", lCnt);

   bCreateUpd = true;
   lRecCnt = lCnt;
   return 0;
}



/******************************** CreateMnoRoll ****************************
 *
 * Tested on 9/1/2005 by SPN
 *
 ****************************************************************************/

int CreateMnoRoll(int iFirstRec)
{
   char     *pTmp, acBuf[MAX_RECSIZE], acRec[MAX_RECSIZE];
   char     cFileCnt=1;
   char     acOutFile[_MAX_PATH];

   HANDLE   fhOut;
   FILE     *fdRoll;

   int      iRet, iRollUpd=0, iNewRec=0, iRetiredRec=0;
   DWORD    nBytesWritten;
   BOOL     bRet, bEof;
   long     lRet=0, lCnt=0;

   // Use TMP file only if output needs resort
   //sprintf(acTmpFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "TMP");
   sprintf(acOutFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "R01");

   // Open roll file
   LogMsg("Open Roll file %s", acRollFile);
   fdRoll = fopen(acRollFile, "r");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", acRollFile);
      return 2;
   }

   // Open Char file
   if (!_access(acCharFile, 0))
   {
      LogMsg("Open Char file %s", acCharFile);
      fdChar = fopen(acCharFile, "r");
      if (fdChar == NULL)
      {
         LogMsg("***** Error opening Char file: %s\n", acCharFile);
         return 2;
      }
   } else
      LogMsg("*** Missing Char file %s", acCharFile);

   // Open BParcels file
   if (!_access(acBPFile, 0))
   {
      LogMsg("Open BP file %s", acBPFile);
      fdBP = fopen(acBPFile, "r");
      if (fdBP == NULL)
      {
         LogMsg("***** Error opening BP file: %s\n", acBPFile);
         return 2;
      }
   } else
      LogMsg("*** Missing BP file %s", acBPFile);

   // Open Output file
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening output file: %s\n", acOutFile);
      return 4;
   }

   // Output first header record
   if (iFirstRec > 0)
   {
      memset(acBuf, '9', iRecLen);
      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
   }

   // Get 1st rec
   pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdRoll);
   bEof = (pTmp ? false:true);

   // Init variables
   iNoMatch=iBadCity=iBadSuffix=0;

   // Merge loop
   while (pTmp && !feof(fdRoll))
   {
      // Create new R01 record
      iRet = Mno_MergeRoll(acBuf, acRec, MAX_RECSIZE, CREATE_R01|CREATE_LIEN);

      // Merge Char
      if (fdChar)
         lRet = Mno_MergeChar(acBuf);
      if (fdBP)
         lRet = Mno_MergeBP(acBuf);

      if (!iRet)
      {
         iNewRec++;
         // Save last recording date
         lRet = atoin((char *)&acBuf[OFF_TRANSFER_DT], 8);
         if (lRet > lLastRecDate && lRet < lToday)
            lLastRecDate = lRet;

         bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
         if (!(++lCnt % 1000))
            printf("\r%u", lCnt);

         if (!bRet)
         {
            LogMsg("Error writing to output file at record %d\n", lCnt);
            lRet = WRITE_ERR;
            break;
         }
      }

      // Get next roll record
      pTmp = fgets(acRec, MAX_RECSIZE, fdRoll);
   }

   // Close files
   if (fdRoll)
      fclose(fdRoll);
   if (fdChar)
      fclose(fdChar);
   if (fdBP)
      fclose(fdBP);
   if (fhOut)
      CloseHandle(fhOut);

   // Sort output file
   //LogMsg("Sort output file %s to %s", acTmpFile, acOutFile);
   //sprintf(acTmp, "S(1,14,C,A) F(FIX,%d) B(%d,R)", iRecLen, iRecLen);
   //lRet = sortFile(acTmpFile, acOutFile, acTmp);

   LogMsg("Total output records:       %u", lCnt);
   LogMsg("Total retired records:      %u", iRetiredRec);
   LogMsg("Total bad-city records:     %u", iBadCity);
   LogMsg("Total bad-suffix records:   %u\n", iBadSuffix);

   LogMsg("Number of Char matched:     %u", lCharMatch);
   LogMsg("Number of Char skiped:      %u", lCharSkip);
   LogMsg("Number of BP matched:       %u", lBPMatch);
   LogMsg("Number of BP skiped:        %u", lBPSkip);

   LogMsg("Last recording date:        %u", lLastRecDate);
   printf("\nTotal output records: %u", lCnt);

   lRecCnt = lCnt;
   return 0;
}

/******************************* Mno_ConvertChar() **************************
 *
 * Return > 0 if successful.  < 0 is file open error, 0 is sort error.
 *
 ****************************************************************************/

int Mno_ConvertChar(char *pInfile, char *pOutfile)
{
   FILE     *fdIn, *fdOut;
   char     acBuf[1024], acTmp[256], *pRec;
   int      iRet, iTmp, iCnt=0;
   MNO_CHAR  myCharRec;

   if (!(fdIn = fopen(pInfile, "r")))
   {
      LogMsg("***** Error opening CHAR file %s", pInfile);
      return -1;
   }

   if (!(fdOut = fopen(pOutfile, "w")))
   {
      fclose(fdIn);
      LogMsg("***** Error creating output file %s", pOutfile);
      return -2;
   }

   pRec = fgets(acBuf, 1024, fdIn);

   while (!feof(fdIn))
   {
      pRec = fgets(acBuf, 1024, fdIn);
      if (!pRec)
         break;

      iRet = ParseStringNQ(pRec, ',', MNO_CHAR_WELL+1, apTokens);
      memset((void *)&myCharRec, ' ', sizeof(MNO_CHAR));

#ifdef _DEBUG
      //if (!memcmp(apTokens[MNO_CHAR_APN], "0104015", 7) )
      //   iTmp = 0;
#endif

      strcpy(acTmp, apTokens[MNO_CHAR_APN]);
      if (acTmp[2] == '-')
         remChar(acTmp, '-');
      if (strlen(acTmp) < iApnLen)
      {
         strcat(acTmp, "0000000000");
         memcpy(myCharRec.APN, acTmp, iApnLen);
      } else
      {
         memcpy(myCharRec.APN, acTmp, iApnLen);
      }

      memcpy(myCharRec.Image, apTokens[MNO_CHAR_IMAGE], strlen(apTokens[MNO_CHAR_IMAGE]));
      memcpy(myCharRec.YrBlt, apTokens[MNO_CHAR_YRBLT], strlen(apTokens[MNO_CHAR_YRBLT]));
      memcpy(myCharRec.EffYr, apTokens[MNO_CHAR_EFFYR], strlen(apTokens[MNO_CHAR_EFFYR]));
      iTmp = atoi(apTokens[MNO_CHAR_BEDS]);
      if (iTmp > 0)
         memcpy(myCharRec.Beds, apTokens[MNO_CHAR_BEDS], strlen(apTokens[MNO_CHAR_BEDS]));

      iTmp = atoi(apTokens[MNO_CHAR_BATH]);
      if (iTmp > 0)
         memcpy(myCharRec.Bath, apTokens[MNO_CHAR_BATH], strlen(apTokens[MNO_CHAR_BATH]));

      iTmp = atoi(apTokens[MNO_CHAR_HBATH]);
      if (iTmp > 0)
         memcpy(myCharRec.HBath, apTokens[MNO_CHAR_HBATH], strlen(apTokens[MNO_CHAR_HBATH]));

      iTmp = atoi(apTokens[MNO_CHAR_ROOMS]);
      if (iTmp > 0)
         memcpy(myCharRec.Rooms, apTokens[MNO_CHAR_ROOMS], strlen(apTokens[MNO_CHAR_ROOMS]));

      iTmp = atoi(apTokens[MNO_CHAR_FIREPL]);
      if (iTmp > 0)
         memcpy(myCharRec.FirePl, apTokens[MNO_CHAR_FIREPL], strlen(apTokens[MNO_CHAR_FIREPL]));

      iTmp = atoi(apTokens[MNO_CHAR_WS]);
      if (iTmp > 0)
         memcpy(myCharRec.Ws, apTokens[MNO_CHAR_WS], strlen(apTokens[MNO_CHAR_WS]));

      iTmp = atoi(apTokens[MNO_CHAR_POOL]);
      if (iTmp > 0)
         memcpy(myCharRec.Pool, apTokens[MNO_CHAR_POOL], strlen(apTokens[MNO_CHAR_POOL]));

      memcpy(myCharRec.Sqft, apTokens[MNO_CHAR_SQFT], strlen(apTokens[MNO_CHAR_SQFT]));
      memcpy(myCharRec.Stories, apTokens[MNO_CHAR_STORIES], strlen(apTokens[MNO_CHAR_STORIES]));
      if (iRet > MNO_CHAR_GSIZE)
      {
         memcpy(myCharRec.GSize, apTokens[MNO_CHAR_GSIZE], strlen(apTokens[MNO_CHAR_GSIZE]));
         if (iRet > MNO_CHAR_CLASS)
         {
            memcpy(myCharRec.Class, apTokens[MNO_CHAR_CLASS], strlen(apTokens[MNO_CHAR_CLASS]));
            if (iRet > MNO_CHAR_QUAL)
            {
               memcpy(myCharRec.Qual, apTokens[MNO_CHAR_QUAL], strlen(apTokens[MNO_CHAR_QUAL]));
               if (iRet > MNO_CHAR_SEPTIC)
               {
                  memcpy(myCharRec.Septic, apTokens[MNO_CHAR_SEPTIC], strlen(apTokens[MNO_CHAR_SEPTIC]));
                  if (iRet > MNO_CHAR_WELL)
                     memcpy(myCharRec.Well, apTokens[MNO_CHAR_WELL], strlen(apTokens[MNO_CHAR_WELL]));

               }
            }
         }
      }

      myCharRec.CRLF[0] = '\n';
      myCharRec.CRLF[1] = '\0';
      fputs((char *)&myCharRec, fdOut);

      if (!(++iCnt % 1000))
         printf("\r%u", iCnt);
   }

   if (fdIn) fclose(fdIn);
   if (fdOut) fclose(fdOut);

   LogMsg("Number of CHAR record output:  %d\n", iCnt);
   printf("\nNumber of CHAR record output:  %d\n", iCnt);

   // Sort output on ASMT
//   iRet = sortFile(acTmpFile, pRec, "S(1,13,C,A) F(TXT)");
//   printf("\n");
   return iCnt;
}

/******************************* Mno_convertChar() **************************
 *
 * Return > 0 if successful.  < 0 is file open error, 0 is sort error.
 *
 ****************************************************************************/

int Mno_convertBParcels(char *pInfile, char *pOutfile)
{
   FILE     *fdIn, *fdOut;
   char     acBuf[1024], *pRec;
   int      iRet, iCnt=0;
   MNO_BP   MyBPRec;

   if (!(fdIn = fopen(pInfile, "r")))
   {
      LogMsg("***** Error opening BParcel file %s", pInfile);
      return -1;
   }

   if (!(fdOut = fopen(pOutfile, "w")))
   {
      fclose(fdIn);
      LogMsg("***** Error creating output file %s", pOutfile);
      return -2;
   }

   pRec = fgets(acBuf, 1024, fdIn);
   while (!feof(fdIn))
   {
      pRec = fgets(acBuf, 1024, fdIn);
      if (!pRec)
         break;

      iRet = ParseStringNQ(pRec, ',', MNO_BP_USECODE+1, apTokens);
      memset((void *)&MyBPRec, ' ', sizeof(MNO_BP));

#ifdef _DEBUG
      //if (!memcmp(apTokens[MNO_BP_APN], "0104015", 7) )
      //   iTmp = 0;
#endif

      if (strlen(apTokens[MNO_BP_APN]) == 15)
      {
         if ((memcmp(apTokens[MNO_BP_APN], "00000", 5)))
         {
            memcpy(MyBPRec.APN, &apTokens[MNO_BP_APN][2], iApnLen);

            memcpy(MyBPRec.Zoning, apTokens[MNO_BP_ZONING], strlen(apTokens[MNO_BP_ZONING]));
            memcpy(MyBPRec.St_Num, apTokens[MNO_BP_NUM], strlen(apTokens[MNO_BP_NUM]));
            memcpy(MyBPRec.St_Prefix, apTokens[MNO_BP_PREFIX], strlen(apTokens[MNO_BP_PREFIX]));
            memcpy(MyBPRec.Street, apTokens[MNO_BP_STREET], strlen(apTokens[MNO_BP_STREET]));
            memcpy(MyBPRec.City, apTokens[MNO_BP_CITY], strlen(apTokens[MNO_BP_CITY]));
            memcpy(MyBPRec.Tra, apTokens[MNO_BP_TRA], strlen(apTokens[MNO_BP_TRA]));
            memcpy(MyBPRec.UseCode, apTokens[MNO_BP_USECODE], strlen(apTokens[MNO_BP_USECODE]));

            MyBPRec.CRLF[0] = '\n';
            MyBPRec.CRLF[1] = '\0';
            fputs((char *)&MyBPRec, fdOut);
         }
      }

      iCnt++;
      if (!(++iCnt % 1000))
         printf("\r%u", iCnt);
   }

   if (fdIn) fclose(fdIn);
   if (fdOut) fclose(fdOut);

   LogMsg("Number of BParcel records output:  %d\n", iCnt);
   printf("\nNumber of BParcel records output:  %d\n", iCnt);
   return iCnt;
}

/******************************* Mno_CreateLienRec ***************************
 *
 * Return 0 if successful, >0 if error
 *
 *****************************************************************************/

int Mno_CreateLienRec(char *pOutbuf, char *pRollRec)
{
   char     acTmp[256];
   long     lTmp;
   int      iTmp;
   MNO_LIEN *pRec = (MNO_LIEN *)pRollRec;
   LIENEXTR *pLienRec = (LIENEXTR *)pOutbuf;

   // Clear output buffer
   memset(pOutbuf, ' ', sizeof(LIENEXTR));

   // Start copying data
   memcpy(pLienRec->acApn, pRec->Apn, MNOSIZ_LIEN_APN);

   // TRA
   lTmp = atoin(pRec->Tra, MNOSIZ_LIEN_TRA);
   if (lTmp > 0)
   {
      iTmp = sprintf(acTmp, "%.6d", lTmp);
      memcpy(pLienRec->acTRA, acTmp, iTmp);
   }

   // Year assessed
   memcpy(pLienRec->acYear, myCounty.acYearAssd, 4);

   // Land
   long lLand = atoin(pRec->Land, MNOSIZ_LIEN_LAND);
   if (lLand > 0)
   {
      iTmp = sprintf(acTmp, "%*d", sizeof(pLienRec->acLand), lLand);
      memcpy(pLienRec->acLand, acTmp, iTmp);
   }

   // Improve - Ratio
   long lImpr = atoin(pRec->Improv, MNOSIZ_LIEN_IMPROV);
   if (lImpr > 0)
   {
      iTmp = sprintf(acTmp, "%*d", sizeof(pLienRec->acImpr), lImpr);
      memcpy(pLienRec->acImpr, acTmp, iTmp);

      iTmp = sprintf(acTmp, "%*d", sizeof(pLienRec->acRatio), (LONGLONG)lImpr*100/(lLand+lImpr));
      memcpy(pLienRec->acRatio, acTmp, iTmp);
   }

   // PP value
   long lPers = atoin(pRec->Pers_Prop, MNOSIZ_LIEN_PERS_PROP);
   if (lPers > 0)
   {
      iTmp = sprintf(acTmp, "%*d", sizeof(pLienRec->acPP_Val), lPers);
      memcpy(pLienRec->acPP_Val, acTmp, iTmp);
   }

   // Fixture
   long lFixt = atoin(pRec->Trade_Fixt, MNOSIZ_LIEN_TRADE_FIXT);
   if (lFixt > 0)
   {
      iTmp = sprintf(acTmp, "%*d", sizeof(pLienRec->acME_Val), lFixt);
      memcpy(pLienRec->acME_Val, acTmp, iTmp);
   }

   // Mobile home
   long lMh = atoin(pRec->Mob_Hm, MNOSIZ_LIEN_MOB_HM);
   if (lMh > 0)
   {
      iTmp = sprintf(acTmp, "%*d", sizeof(pLienRec->extra.Mno.MH_Val), lMh);
      memcpy(pLienRec->extra.Mno.MH_Val, acTmp, iTmp);
   }

   // Other value
   long lOther = atoin(pRec->Other_Value, MNOSIZ_LIEN_OTHER_VALUE);
   if (lOther > 0)
   {
      iTmp = sprintf(acTmp, "%*d", sizeof(pLienRec->extra.Mno.Other_Val), lOther);
      memcpy(pLienRec->extra.Mno.Other_Val, acTmp, iTmp);
   }

   // Total other
   long lOthers = lPers + lMh + lFixt + lOther;
   if (lOthers > 0)
   {
      iTmp = sprintf(acTmp, "%*d", sizeof(pLienRec->acOther), lOthers);
      memcpy(pLienRec->acOther, acTmp, iTmp);
   }

   // Gross
   lTmp = lOthers + lLand + lImpr;
   if (lTmp > 0)
   {
      iTmp = sprintf(acTmp, "%*d", sizeof(pLienRec->acGross), lTmp);
      memcpy(pLienRec->acGross, acTmp, iTmp);
   }

   // HO Exempt
   long lExe = atoin(pRec->Homeowner_Exemp, MNOSIZ_LIEN_HOMEOWNER_EXEMP);
   if (lExe > 0)
      pLienRec->acHO[0] = '1';         // Y
   else
      pLienRec->acHO[0] = '2';         // N

   // Exe code: R=Religion, W=Welfare, V=Veteran, O=Other
   long lAmt[4], iIdx;
   char bCode[4];

   lAmt[0] = lAmt[1] =0;
   iIdx = 0;
   lAmt[iIdx] = atoin(pRec->Other_Exemp, MNOSIZ_LIEN_OTHER_EXEMP);
   if (lAmt[iIdx] > 0)
   {
      lExe += lAmt[iIdx];
      bCode[iIdx] = 'O';
      iIdx++;
   }
   lAmt[iIdx] = atoin(pRec->Church_Exemp, MNOSIZ_LIEN_CHURCH_EXEMP);
   if (lAmt[iIdx] > 0)
   {
      lExe += lAmt[iIdx];
      bCode[iIdx] = 'R';
      iIdx++;
   }
   lAmt[iIdx] = atoin(pRec->Veteran_Exemp, MNOSIZ_LIEN_VETERAN_EXEMP);
   if (lAmt[iIdx] > 0)
   {
      lExe += lAmt[iIdx];
      bCode[iIdx] = 'V';
      iTmp++;
   }
   lAmt[iIdx] = atoin(pRec->Welfare_Exemp, MNOSIZ_LIEN_WELFARE_EXEMP);
   if (lAmt[iIdx] > 0)
   {
      lExe += lAmt[iIdx];
      bCode[iIdx] = 'W';
      iIdx++;
   }

   if (lAmt[0] > 0)
   {
      iTmp = sprintf(acTmp, "%*d", sizeof(pLienRec->extra.Mno.Exe_Amt1), lAmt[0]);
      memcpy(pLienRec->extra.Mno.Exe_Amt1, acTmp, iTmp);
      pLienRec->extra.Mno.Exe_Code1 = bCode[0];
      if (lAmt[1] > 0)
      {
         iTmp = sprintf(acTmp, "%*d", sizeof(pLienRec->extra.Mno.Exe_Amt2), lAmt[1]);
         memcpy(pLienRec->extra.Mno.Exe_Amt2, acTmp, iTmp);
         pLienRec->extra.Mno.Exe_Code2 = bCode[1];
      }
   }

   if (lExe > 0)
   {
      iTmp = sprintf(acTmp, "%*d", sizeof(pLienRec->acExAmt), lExe);
      memcpy(pLienRec->acExAmt, acTmp, iTmp);
   }

   // Tax Exempt is Y or blank
   pLienRec->extra.Mno.TaxExempt = pRec->Tax_Exempt[0];

   pLienRec->LF[0] = '\n';
   pLienRec->LF[1] = 0;
   return 0;
}

/********************************* Mno_ExtrLien *****************************
 *
 *
 ****************************************************************************/

int Mno_ExtrLien()
{
   char  *pTmp, acBuf[MAX_RECSIZE], acRollRec[MAX_RECSIZE], acOutFile[_MAX_PATH];
   FILE  *fdLien;
   long  lCnt=0;

   // Open roll file
   LogMsg("Open Roll file %s", acRollFile);
   fdRoll = fopen(acRollFile, "r");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", acRollFile);
      return 2;
   }

   // Open Output file
   sprintf(acOutFile, acLienTmpl, myCounty.acCntyCode, myCounty.acCntyCode);
   LogMsg("Open output file %s", acOutFile);
   fdLien = fopen(acOutFile, "w");
   if (fdLien == NULL)
   {
      LogMsg("***** Error opening Lien extract file: %s\n", acOutFile);
      return 4;
   }

   // Merge loop
   while (!feof(fdRoll))
   {
      // Get roll rec
      pTmp = fgets(acRollRec, MAX_RECSIZE, fdRoll);
      if (!pTmp)
         break;

      // Create new lien record
      Mno_CreateLienRec(acBuf, acRollRec);

      fputs(acBuf, fdLien);
      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdRoll)
      fclose(fdRoll);
   if (fdLien)
      fclose(fdLien);

   LogMsgD("Total records output:     %d\n", lCnt);

   lRecCnt = lCnt;
   return 0;
}

/*********************************** loadMno ********************************
 *
 * Options:
 *    -CMNO -L -Mg (load lien)
 *    -CMNO -U -G  (load update)
 *    -CMNO -G (daily run)
 *
 ****************************************************************************/

int loadMno(int iSkip)
{
   int   iRet;
   char  *pTmp, acTmp[_MAX_PATH], acTmpFile[_MAX_PATH], acZipFile[_MAX_PATH];

   iApnLen = myCounty.iApnLen;

   iRet = LoadLUTable((char *)&acLookupTbl[0], "[Quality]", NULL, MAX_ATTR_ENTRIES);
   if (!iRet)
   {
      LogMsg("*** Error Looking for table [Quality] in %s", acLookupTbl);
      return 1;
   }

   // -G
   if (iLoadFlag & LOAD_GRGR)
   {
      // If input is ZIP file, unzip first.
      doZipInit();

      // Unzip
      GetPrivateProfileString(myCounty.acCntyCode, "GrGrIn", "", acTmpFile, _MAX_PATH, acIniFile);
      sprintf(acZipFile, acTmpFile, "zip");

      if (!_access(acZipFile, 0))
      {
         strcpy(acTmp, acZipFile);
         pTmp = strrchr(acTmp, '\\');
         if (pTmp)
            *pTmp = 0;

         // Set unzip folder
         setUnzipToFolder(acTmp);

         // Set to replace file if exist
         setReplaceIfExist(true);

         LogMsg("Unzip input file %s", acZipFile);
         iRet = startUnzip(acZipFile);
         if (iRet)
         {
            LogMsg("***** Error while unzipping %s", acZipFile);
            iRet = -1;
         }
      } else
      {
         LogMsg("*** No GrGr file avail for processing: %s", acZipFile);
         iRet = 1;
      }

      // Shut down Zip server
      doZipShutdown();

      if (iRet < 0)
         return iRet;
      else if (!iRet)
      {
         sprintf(acTmp, acTmpFile, "csv");

         // Create Mon_GrGr.dat
         LogMsg("Load %s GrGr file", myCounty.acCntyCode);
         iRet = Mno_LoadGrGr(acTmp);

         if (!iRet)
         {
            char  acDate[32];

            iLoadFlag |= MERG_GRGR;       // Trigger merge grgr

            // Back up processed file
            pTmp = strrchr(acTmpFile, '\\');
            if (pTmp)
               *++pTmp = 0;
            dateString(acDate, 0);
            sprintf(acTmp, "GrGr_%s", acDate);
            strcat(acTmpFile, acTmp);

            // Create backup folder
            if (_access(acTmpFile, 0))
               _mkdir(acTmpFile);

            // Move file
            pTmp = strrchr(acZipFile, '\\');
            strcat(acTmpFile, pTmp);
            rename(acZipFile, acTmpFile);
         }
      }
   }

   // Extract lien
   if (iLoadFlag & EXTR_LIEN)
      iRet = Mno_ExtrLien();

   // Process LDR or roll update
   if (iLoadFlag & (LOAD_LIEN|LOAD_UPDT))
   {
      if (!_access(acCharFile, 0))
      {
         sprintf(acTmpFile, "%s\\%s\\Mno_Char.dat", acTmpPath, myCounty.acCntyCode);
         LogMsg("Converting %s to %s", acCharFile, acTmpFile);
         iRet = Mno_ConvertChar(acCharFile, acTmpFile);
         if (iRet > 0)
            strcpy(acCharFile, acTmpFile);
         else
         {
            LogMsg("***** Error converting Char file %s", acCharFile);
            return -1;
         }
      } else
         sprintf(acCharFile, "%s\\%s\\Mno_Char.dat", acTmpPath, myCounty.acCntyCode);

      if (!_access(acBPFile, 0))
      {
         sprintf(acTmpFile, "%s\\%s\\Mno_BParcel.dat", acTmpPath, myCounty.acCntyCode);
         LogMsg("Converting %s to %s", acBPFile, acTmpFile);
         iRet = Mno_convertBParcels(acBPFile, acTmpFile);
         if (iRet > 0)
            strcpy(acBPFile, acTmpFile);
         else
         {
            LogMsg("***** Error converting BParcels file %s", acBPFile);
            return -1;
         }
      } else
         sprintf(acCharFile, "%s\\%s\\Mno_BParcel.dat", acTmpPath, myCounty.acCntyCode);

      if (iLoadFlag & LOAD_LIEN)
      {
         // Create Lien file
         LogMsg("Load %s Lien file", myCounty.acCntyCode);
         iRet = CreateMnoRoll(iSkip);
      } else if (iLoadFlag & LOAD_UPDT)
      {
         LogMsg("Load %s roll update file", myCounty.acCntyCode);
         iRet = MergeMnoRoll(iSkip);
      }
   }

   if (!iRet && (iLoadFlag & MERG_GRGR))
   {
      LogMsg("Update %s roll with GrGr file", myCounty.acCntyCode);
      iGrGrApnLen = myCounty.iApnLen;              // Use regular APN len when merge to R01
      iRet = MergeGrGrFile(myCounty.acCntyCode);
      if (!iRet)
      {
         iLoadFlag |= LOAD_UPDT;                   // Trigger buildcda
         bCreateUpd = true;
      }
   }

   // Fix TRA
   if (!iRet && (lOptMisc & M_OPT_FIXTRA))
      iRet = PQ_FixR01(myCounty.acCntyCode, PQ_FIX_TRA);

   return iRet;
}

