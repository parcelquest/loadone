#ifndef  _MERGE_FRE_H
#define  _MERGE_FRE_H

// 06/12/2024
#define  FRE_GR_DOCNUM        0
#define  FRE_GR_DOCDATE       1  // Wed May 15 15:35:38 PDT 2024
#define  FRE_GR_REGDATE	      2  // not used
#define  FRE_GR_NUMPAGES	   3
#define  FRE_GR_APN			   4
#define  FRE_GR_GRANTEE	      5
#define  FRE_GR_GRANTOR	      6
#define  FRE_GR_DOCTYPE	      7
#define  FRE_GR_DOCTITLE      8
#define  FRE_GR_DOCTAX        9
#define  FRE_GR_COLS          10

// 11/21/2018
//#define  FRE_GR_DOCNUM        0
//#define  FRE_GR_DOCDATE       1   // 06/01/2023
//#define  FRE_GR_APN			   2
//#define  FRE_GR_GRANTOR	      3
//#define  FRE_GR_GRANTEE	      4
//#define  FRE_GR_DOCTAX        5
//#define  FRE_GR_DOCTITLE      6
//#define  FRE_GR_DOCTYPE	      7
//#define  FRE_GR_COLS          7

#define  GSIZ_DOCNUM          12
#define  GSIZ_DOCDATE         8
#define  GSIZ_DOCTYPE         40
#define  GSIZ_DOCTAX          10
#define  GSIZ_FMTAPN          11
#define  GSIZ_NAME            50
#define  GSIZ_XREF            40

typedef struct _tFreGrGr
{
   char  acDocNum[GSIZ_DOCNUM];
   char  filler1[1];
   char  acDocDate[GSIZ_DOCDATE+3];
   char  filler2[1];
   char  acDocType[GSIZ_DOCTYPE];
   char  filler3[1];
   char  acMisc[13];
   char  acTaxAmt[GSIZ_DOCTAX];
   char  filler4[1];
   char  acFmtApn[GSIZ_FMTAPN];
   char  filler5[1];
   char  acNameType[2];          // E : Name is trustee (buyer or owner)
                                 // R : Name is trustor (seller or lender)
   char  acName[GSIZ_NAME];
   char  filler6[1];
   char  acXref[GSIZ_XREF];
   char  filler7[2];
} FRE_GRGR;

//#ifdef    MAX_NAMES
//  #undef  MAX_NAMES
//  #define MAX_NAMES   2
//#endif

#define  OFF_GREC_APN         1
#define  OFF_GREC_DOCDATE     15
#define  OFF_GREC_DOCNUM      23
#define  OFF_GREC_TITLE       35
#define  OFF_GREC_SALE        75
#define  OFF_GREC_GRTEE       85
#define  OFF_GREC_GRTOR       185
#define  OFF_GREC_DOCTAX      285
#define  OFF_GREC_APNMATCH    295
#define  OFF_GREC_OWNERMATCH  296
#define  OFF_GREC_ETAL        297

typedef struct t_FreGRec
{  // 299 bytes
   char  Apn[SALE_SIZ_APN];
   char  DocDate[SALE_SIZ_DOCDATE];
   char  DocNum[SALE_SIZ_DOCNUM];
   char  DocType[GSIZ_DOCTYPE];
   char  SalePrice[SALE_SIZ_SALEPRICE];
   char  Grantee[MAX_NAMES][GSIZ_NAME];
   char  Grantor[MAX_NAMES][GSIZ_NAME];
   char  DocTax[GSIZ_DOCTAX];
   char  ApnMatched;
   char  OwnerMatched;
   char  MoreName;
   char  CRLF[2];
} FRE_GREC;

// Old EBCDIC record, replace by FRE_LIEN 2008
#define  ROFF_APN                      1
#define  ROFF_TRA                      10
#define  ROFF_LAND                     16
#define  ROFF_IMPR_FIXT                26
#define  ROFF_FIXT                     36
#define  ROFF_PERSPROP_MB              46
#define  ROFF_MISC_EXE                 56
#define  ROFF_CLASS_CT                 64
#define  ROFF_CLASS_CODE               65
#define  ROFF_HOEXE                    66
#define  ROFF_HOEXE_CODE               74
#define  ROFF_BUS_INVENT_EXE           75
#define  ROFF_BUS_INVENT_EXE_CODE      83
#define  ROFF_FILLER1                  84
#define  ROFF_PEST_ASSMNT              106
#define  ROFF_PEST_CODE                115
#define  ROFF_FILLER2                  116
#define  ROFF_SOLD2STATE_DATE          120
#define  ROFF_REDEMPTION_DATE          125
#define  ROFF_SOLD2STATE_NUMBER        130
#define  ROFF_CNTY_SAYS_NOT_USED1      139
#define  ROFF_CNTY_SAYS_NOT_USED2      140
#define  ROFF_CNTY_SAYS_NOT_USED3      142
#define  ROFF_CNTY_SAYS_NOT_USED4      143
#define  ROFF_CNTY_SAYS_NOT_USED5      148
#define  ROFF_NAME1                    153
#define  ROFF_ZIP                      193
#define  ROFF_FILLER3                  198
#define  ROFF_NAME2                    202
#define  ROFF_ADDR1                    242
#define  ROFF_ADDR2                    267
#define  ROFF_ADDR3                    292
#define  ROFF_ADDR4                    317
#define  ROFF_NARRATIVE                342
#define  ROFF_NARRATIVE_FLAG           392
#define  ROFF_FILLER4                  393
#define  ROFF_PERSPROP_PEN_FLAG        394
#define  ROFF_SENIOR_CIT_CODE          395
#define  ROFF_MB_HOME_VALUE            396
#define  ROFF_FILLER5                  407

#define  RSIZ_APN                      9
#define  RSIZ_TRA                      6
#define  RSIZ_LAND                     10
#define  RSIZ_IMPR_FIXT                10
#define  RSIZ_FIXT                     10
#define  RSIZ_PERSPROP_MB              10
#define  RSIZ_MISC_EXE                 8
#define  RSIZ_CLASS_CT                 1
#define  RSIZ_CLASS_CODE               1
#define  RSIZ_HOEXE                    8
#define  RSIZ_HOEXE_CODE               1
#define  RSIZ_BUS_INVENT_EXE           8
#define  RSIZ_BUS_INVENT_EXE_CODE      1
#define  RSIZ_PEST_ASSMNT              9
#define  RSIZ_PEST_CODE                1
#define  RSIZ_SOLD2ST_DATE             5
#define  RSIZ_REDEMPTION_DATE          5
#define  RSIZ_SOLD2ST_NUMBER           9
#define  RSIZ_NAME1                    40
#define  RSIZ_ZIP                      5
#define  RSIZ_NAME2                    40
#define  RSIZ_ADDR                     25
#define  RSIZ_NARRATIVE                50
#define  RSIZ_NARRATIVE_FLAG           1
#define  RSIZ_PERSPROP_PENALTY_FLAG    1
#define  RSIZ_SENIOR_CITIZEN_CODE      1
#define  RSIZ_MB_HOME_VALUE            11

// Roll Record
typedef struct _tFreRoll
{  // 439-bytes unpacked
   char  Apn[RSIZ_APN];
   char  TRA[RSIZ_TRA];
   char  Land[RSIZ_LAND];
   char  Impr[RSIZ_IMPR_FIXT];
   char  Fixt[RSIZ_FIXT];
   char  PP_Val[RSIZ_PERSPROP_MB];
   char  OthExe[RSIZ_MISC_EXE];
   char  Class_Ct;
   char  Class_Code;
   char  HOExe[RSIZ_HOEXE];
   char  HOExe_Code;
   char  BusExe[RSIZ_BUS_INVENT_EXE];
   char  BusExe_Code;
   char  filler1[22];
   char  Pest_Assmt[RSIZ_PEST_ASSMNT];
   char  Pest_Code;
   char  filler2[4];
   char  Sold2St_Date[RSIZ_SOLD2ST_DATE];        // YYDDD - Julian date - off=120
   char  Redemption_Date[RSIZ_REDEMPTION_DATE];  // YYDDD
   char  Sold2St_Num[RSIZ_SOLD2ST_NUMBER];
   char  CntySaysNotUse[14];
   char  Name1[RSIZ_NAME1];
   char  Zip[RSIZ_ZIP];                          // do not use
   char  filler3[4];
   char  Name2[RSIZ_NAME2];
   char  M_Addr1[RSIZ_ADDR];
   char  M_Addr2[RSIZ_ADDR];
   char  M_Addr3[RSIZ_ADDR];
   char  M_Addr4[RSIZ_ADDR];
   char  Narative[RSIZ_NARRATIVE];
   char  Narative_Flag;
   char  filler4;
   char  PP_Pen_Flag;
   char  Senior_Citz_Code;
   char  MH_Value[RSIZ_MB_HOME_VALUE];
   char  filler5[33];        // Reserve extra space
} FRE_ROLL;

#define  COFF_APN                1
#define  COFF_SHEET_NUM          10
#define  COFF_STRNUM             12
#define  COFF_STRFRA             18
#define  COFF_STRDIR             21
#define  COFF_STRNAME            23
#define  COFF_CITYCODE           49
#define  COFF_NARRATIVE          51
#define  COFF_USECODE            101
#define  COFF_ZONING             108
#define  COFF_LOT_WIDTH          123
#define  COFF_LOT_DEPTH          132
#define  COFF_LOT_AREA           141
#define  COFF_COOLING            150
#define  COFF_FIREPLACE          151
#define  COFF_POOL_WIDTH         152
#define  COFF_POOL_DEPTH         154
#define  COFF_ADDL_IMPR_AREA     156
#define  COFF_2ND_FLOOR_AREA     160
#define  COFF_GARAGE_AREA        169
#define  COFF_BEDS               178
#define  COFF_BATHS              182
#define  COFF_BLDGCLASS          186
#define  COFF_EFFYEAR            191
#define  COFF_BASEYEAR           195
#define  COFF_NEIGHBORHOOD       207
#define  COFF_BASEMENT_AREA      214
#define  COFF_GROSS_RENT         223
#define  COFF_UNITS              232
#define  COFF_1ST_FLOOR_AREA     235
#define  COFF_FLOORS             244
#define  COFF_AVG_UNIT_SIZE      247

#define  CSIZ_SHEET_NUM          2
#define  CSIZ_STRNUM             6
#define  CSIZ_STRFRA             3
#define  CSIZ_STRDIR             2
#define  CSIZ_STRNAME            26
#define  CSIZ_CITYCODE           2
#define  CSIZ_NARRATIVE          50
#define  CSIZ_USECODE            7
#define  CSIZ_ZONING             15
#define  CSIZ_LOT_WIDTH          9
#define  CSIZ_LOT_DEPTH          9
#define  CSIZ_LOT_AREA           9
#define  CSIZ_COOLING            1
#define  CSIZ_FIREPLACE          1
#define  CSIZ_POOL_WIDTH         2
#define  CSIZ_POOL_DEPTH         2
#define  CSIZ_ADDL_IMPR_AREA     4
#define  CSIZ_2ND_FLOOR_AREA     9
#define  CSIZ_GARAGE_AREA        9
#define  CSIZ_BEDS               4
#define  CSIZ_BATHS              4
#define  CSIZ_BLDGCLASS          5
#define  CSIZ_EFFYEAR            4
#define  CSIZ_BASEYEAR           12
#define  CSIZ_NEIGHBORHOOD       7
#define  CSIZ_BASEMENT_AREA      9
#define  CSIZ_GROSS_RENT         9
#define  CSIZ_UNITS              3
#define  CSIZ_1ST_FLOOR_AREA     9
#define  CSIZ_FLOORS             3
#define  CSIZ_AVG_UNIT_SIZE      6

// Characteristic record
typedef struct _tFreChar
{
   char  Apn[RSIZ_APN];
   char  SheetNum[CSIZ_SHEET_NUM];     // BldgSeqNo or UnitSeqNo
   char  S_StrNum[CSIZ_STRNUM];
   char  S_StrFra[CSIZ_STRFRA];
   char  S_StrDir[CSIZ_STRDIR];
   char  S_StrName[CSIZ_STRNAME];
   char  S_CityCode[CSIZ_CITYCODE];
   char  Legal[CSIZ_NARRATIVE];
   char  UseCode[CSIZ_USECODE];
   char  Zoning[CSIZ_ZONING];
   char  LotW[CSIZ_LOT_WIDTH];
   char  LotD[CSIZ_LOT_DEPTH];         // If LotD=1, LotArea contains acres value otherwise sqft
   char  LotArea[CSIZ_LOT_AREA];       // V99
   char  Cooling;
   char  FirePlace;
   char  PoolW[CSIZ_POOL_WIDTH];
   char  PoolD[CSIZ_POOL_DEPTH];
   char  Addl_Impr_Area[CSIZ_ADDL_IMPR_AREA];
   char  Second_Flr_Area[CSIZ_2ND_FLOOR_AREA];
   char  Garage_Area[CSIZ_GARAGE_AREA];
   char  Beds[CSIZ_BEDS];
   char  Baths[CSIZ_BATHS];
   char  BldgClass[CSIZ_BLDGCLASS];
   char  EffYear[CSIZ_EFFYEAR];
   char  BaseYear[CSIZ_BASEYEAR];
   char  NeighborhoodCode[CSIZ_NEIGHBORHOOD];
   char  Basement_Area[CSIZ_BASEMENT_AREA];
   char  Gross_Rent[CSIZ_GROSS_RENT];
   char  Units[CSIZ_UNITS];
   char  First_Flr_Area[CSIZ_1ST_FLOOR_AREA];
   char  Floors[CSIZ_FLOORS];
   char  AvgUnitSize[CSIZ_AVG_UNIT_SIZE];
   char  CrLf[2];
} FRE_CHAR;

// Situs record
typedef struct _tFreSitus
{  // 52
   char  Apn[RSIZ_APN];          // 1
   char  Prime_Flag;             // 10 0=prime, 1=multiple
   char  Delete_Flag;            // 11 D=Delete
   char  StrName[CSIZ_STRNAME];  // 12
   char  StrNum[CSIZ_STRNUM];    // 38
   char  City[CSIZ_CITYCODE];    // 44
   char  StrDir[CSIZ_STRDIR];    // 46
   char  StrFra[CSIZ_STRFRA];    // 48
   char  CrLf[2];
} FRE_SITUS;

// 462 bytes
#define  POFF_STATEMENT_CODE    1
#define  POFF_BUS_ACCT          2
#define  POFF_BRANCH_NO         7
#define  POFF_AFFIL1            10
#define  POFF_APN               13
#define  POFF_CHECK_DIGIT1      23
#define  POFF_SUB_NO1           24
#define  POFF_FILLER1           29
#define  POFF_DOCTYPE           35
#define  POFF_LAST_CHANGE_DATE  36
#define  POFF_ROLL_TYPE         42
#define  POFF_APN1              43
#define  POFF_CHECK_DIGIT2      53
#define  POFF_SUB_NO2           54
#define  POFF_AFFIL2            59
#define  POFF_BRANCH            62
#define  POFF_ACCT              65
#define  POFF_NAME1             70
#define  POFF_NAME2             110
#define  POFF_NAME3             150
#define  POFF_ADDR1             175
#define  POFF_ADDR2             200
#define  POFF_ADDR3             225
#define  POFF_DESCRIPTION1      250
#define  POFF_DESCRIPTION2      270
#define  POFF_DESCRIPTION3      290
#define  POFF_MULTIPLE_IND      310
#define  POFF_SLUC              311
#define  POFF_PENALTY_IND       316
#define  POFF_HOEXE_FLAG        317
#define  POFF_BUSEXE_FLAG       318
#define  POFF_VETEXE_FLAG       319
#define  POFF_CLASS_CODE        320
#define  POFF_AUDIT_YEAR        321
#define  POFF_ADUIT_CODE        322
#define  POFF_LAND              323
#define  POFF_IMPROV            333
#define  POFF_TRADE_FIXT        343
#define  POFF_PERS_PROP         353
#define  POFF_AIRPLANE          363
#define  POFF_BUS_INVENT        373
#define  POFF_PEST_CTRL         383
#define  POFF_MB_HOME           389
#define  POFF_WELFARE_EXE       399
#define  POFF_VETEXE_AMT        409
#define  POFF_HOEXE_AMT         415
#define  POFF_BUS_INVENT_EXE    421
#define  POFF_MAIL_DATE         431
#define  POFF_RETURN_DATE       436
#define  POFF_ORG_V_DATE        441
#define  POFF_LAST_V_DATE       446
#define  POFF_BILL_PAID_FLAG    451
#define  POFF_FILLER2           452
#define  POFF_VALUE_IND         455
#define  POFF_ADD_OCT           456
#define  POFF_ADD_DATE          457

#define  PSIZ_STATEMENT_CODE    1
#define  PSIZ_BUS_ACCT          5
#define  PSIZ_BRANCH_NO         3 
#define  PSIZ_AFFIL1            3
#define  PSIZ_APN               10
#define  PSIZ_CHECK_DIGIT1      1
#define  PSIZ_SUB_NO1           5
#define  PSIZ_FILLER1           6
#define  PSIZ_DOCTYPE           1
#define  PSIZ_LAST_CHANGE_DATE  6
#define  PSIZ_ROLL_TYPE         1
#define  PSIZ_APN1              10
#define  PSIZ_CHECK_DIGIT2      1
#define  PSIZ_SUB_NO2           5
#define  PSIZ_AFFIL2            3
#define  PSIZ_BRANCH            3
#define  PSIZ_ACCT              5
#define  PSIZ_NAME1             40
#define  PSIZ_NAME2             40
#define  PSIZ_NAME3             25
#define  PSIZ_ADDR1             25
#define  PSIZ_ADDR2             25
#define  PSIZ_ADDR3             25
#define  PSIZ_DESCRIPTION1      20
#define  PSIZ_DESCRIPTION2      20
#define  PSIZ_DESCRIPTION3      20
#define  PSIZ_MULTIPLE_IND      1
#define  PSIZ_SLUC              5
#define  PSIZ_PENALTY_IND       1
#define  PSIZ_HOEXE_FLAG        1
#define  PSIZ_BUSEXE_FLAG       1
#define  PSIZ_VETEXE_FLAG       1
#define  PSIZ_CLASS_CODE        1
#define  PSIZ_AUDIT_YEAR        1
#define  PSIZ_ADUIT_CODE        1
#define  PSIZ_LAND              10
#define  PSIZ_IMPROV            10
#define  PSIZ_TRADE_FIXT        10
#define  PSIZ_PERS_PROP         10
#define  PSIZ_AIRPLANE          10
#define  PSIZ_BUS_INVENT        10
#define  PSIZ_PEST              6
#define  PSIZ_MB_HOME           10
#define  PSIZ_WELFARE_EXE       10
#define  PSIZ_VETEXE_AMT        6
#define  PSIZ_HOEXE_AMT         6
#define  PSIZ_BUS_INVENT_EXE    10
#define  PSIZ_MAIL_DATE         5
#define  PSIZ_RETURN_DATE       5
#define  PSIZ_ORG_VDATE         5
#define  PSIZ_LAST_VDATE        5
#define  PSIZ_BILL_PAID_FLAG    1
#define  PSIZ_FILLER2           3
#define  PSIZ_VALUE_IND         1
#define  PSIZ_ADD_OCT           1
#define  PSIZ_ADD_DATE          6

// Personal property record
typedef struct _tFrePersProp
{
   char Statement_code[PSIZ_STATEMENT_CODE];
   char Bus_acct[PSIZ_BUS_ACCT];
   char Branch_no[PSIZ_BRANCH_NO];
   char Affil1[PSIZ_AFFIL1];
   char Apn[PSIZ_APN];
   char Check_digit1[PSIZ_CHECK_DIGIT1];
   char Sub_no1[PSIZ_SUB_NO1];
   char Filler1[PSIZ_FILLER1];
   char Doctype[PSIZ_DOCTYPE];
   char Last_change_date[PSIZ_LAST_CHANGE_DATE];
   char Roll_type[PSIZ_ROLL_TYPE];
   char Apn1[PSIZ_APN];
   char Check_digit2[PSIZ_CHECK_DIGIT2];
   char Sub_no2[PSIZ_SUB_NO2];
   char Affil2[PSIZ_AFFIL2];
   char Branch[PSIZ_BRANCH];
   char Acct[PSIZ_ACCT];
   char Name1[PSIZ_NAME1];
   char Name2[PSIZ_NAME2];
   char Name3[PSIZ_NAME3];
   char Addr1[PSIZ_ADDR1];
   char Addr2[PSIZ_ADDR1];
   char Addr3[PSIZ_ADDR1];
   char Description1[PSIZ_DESCRIPTION1];
   char Description2[PSIZ_DESCRIPTION1];
   char Description3[PSIZ_DESCRIPTION1];
   char Multiple_ind[PSIZ_MULTIPLE_IND];
   char Sluc[PSIZ_SLUC];
   char Penalty_ind[PSIZ_PENALTY_IND];
   char Hoexe_flag[PSIZ_HOEXE_FLAG];
   char Busexe_flag[PSIZ_BUSEXE_FLAG];
   char Vetexe_flag[PSIZ_VETEXE_FLAG];
   char Class_code[PSIZ_CLASS_CODE];
   char Audit_year[PSIZ_AUDIT_YEAR];
   char Aduit_code[PSIZ_ADUIT_CODE];
   char Land[PSIZ_LAND];
   char Improv[PSIZ_IMPROV];
   char Trade_fixt[PSIZ_TRADE_FIXT];
   char Pers_prop[PSIZ_PERS_PROP];
   char Airplane[PSIZ_AIRPLANE];
   char Bus_invent[PSIZ_BUS_INVENT];
   char Pest[PSIZ_PEST];
   char Mb_home[PSIZ_MB_HOME];
   char Welfare_exe[PSIZ_WELFARE_EXE];
   char Vetexe_amt[PSIZ_VETEXE_AMT];
   char Hoexe_amt[PSIZ_HOEXE_AMT];
   char Bus_invent_exe[PSIZ_BUS_INVENT_EXE];
   char Mail_date[PSIZ_MAIL_DATE];
   char Return_date[PSIZ_RETURN_DATE];
   char Orv_date[PSIZ_ORG_VDATE];
   char Last_vdate[PSIZ_LAST_VDATE];
   char Bill_paid_flag[PSIZ_BILL_PAID_FLAG];
   char Filler2[PSIZ_FILLER2];
   char Value_ind[PSIZ_VALUE_IND];
   char Add_oct[PSIZ_ADD_OCT];
   char Add_date[PSIZ_ADD_DATE];   
} FRE_PERS;

/*
#define  SSIZ_DONT_KNOW          2
#define  SSIZ_RECDATE            8
#define  SSIZ_INST_NUMBER        7
#define  SSIZ_DELETE_CODE        2
#define  SSIZ_FILLER_1           2
#define  SSIZ_MAIL_DATE_1        7
#define  SSIZ_DTT_AMT            7
#define  SSIZ_DOC_AMT_STAMP      9
#define  SSIZ_DOC_AMT_FINCED     3
#define  SSIZ_DOC_AMT_2          9
#define  SSIZ_MAIL_ADDR2         25
#define  SSIZ_MAIL_ADDR1         40
#define  SSIZ_RETURN_DATE_1      7
#define  SSIZ_MAIL_DATE_2        7
#define  SSIZ_RETURN_DATE_2      7
#define  SSIZ_HLDAR              7
#define  SSIZ_EXPDT              7
#define  SSIZ_CARE_OF            40
#define  SSIZ_HOLDSALE_CODE      1
#define  SSIZ_TRANS_CODE         2
#define  SSIZ_SALE_AMT           9
#define  SSIZ_SALE_FLAG          1
#define  SSIZ_SALE_CODE          3
#define  SSIZ_OWNER              40
#define  SSIZ_REMARK_DATE        7
#define  SSIZ_SU_CODE            7
#define  SSIZ_ENTRY_DATE         7
#define  SSIZ_SOURCE_ID          8
#define  SSIZ_FINANCE_CODE_2     3
#define  SSIZ_TRANS_DATE         7

#define  SOFF_APN                1
#define  SOFF_DONT_KNOW          10
#define  SOFF_RECDATE            12
#define  SOFF_INST_NUMBER        20
#define  SOFF_DELETE_CODE        27
#define  SOFF_FILLER_1           29
#define  SOFF_MAIL_DATE_1        31
#define  SOFF_DTT_AMT            38
#define  SOFF_DOC_AMT_STAMP      45
#define  SOFF_DOC_AMT_FNCD       54
#define  SOFF_DOC_AMT_2          57
#define  SOFF_MAIL_ADDR2         66
#define  SOFF_MAIL_ADDR1         91
#define  SOFF_RETURN_DATE_1      131
#define  SOFF_MAIL_DATE_2        138
#define  SOFF_RETURN_DATE_2      145
#define  SOFF_HLDAR              152
#define  SOFF_EXPDT              159
#define  SOFF_CARE_OF            166
#define  SOFF_HOLDSALE_CODE      206
#define  SOFF_S_STRNUM           207
#define  SOFF_S_STRFRA           214
#define  SOFF_S_STRDIR           217
#define  SOFF_S_STRNAME          219
#define  SOFF_S_CITY             243
#define  SOFF_TRANS_CODE         245
#define  SOFF_SALE_AMT           247
#define  SOFF_SALE_FLAG          256
#define  SOFF_SALE_CODE          257
#define  SOFF_OWNER              260
#define  SOFF_REMARK_DATE        300
#define  SOFF_SU_CODE            307
#define  SOFF_ENTRY_DATE         314
#define  SOFF_SOURCE_ID          321
#define  SOFF_FINANCE_CODE_2     329
#define  SOFF_TRANS_DATE         332

// Sale record
typedef struct _tFreSale
{  // 340-bytes
   char  Apn[RSIZ_APN];
   char  Dontknow[SSIZ_DONT_KNOW];      
   char  DocDate[SSIZ_RECDATE];                 // yyyymmdd      
   char  DocNum[SSIZ_INST_NUMBER];    
   char  Delete_Code[SSIZ_DELETE_CODE];    
   char  filler1[SSIZ_FILLER_1];         
   char  Mail_Date_1[SSIZ_MAIL_DATE_1];         // mmddyy
   char  Dtt_Amt[SSIZ_DTT_AMT];                 // V99
   char  Doc_Trans_Amt_S[SSIZ_DOC_AMT_STAMP];
   char  Doc_Trans_Amt_F[SSIZ_DOC_AMT_FINCED];
   char  Doc_Trans_Amt_2[SSIZ_DOC_AMT_2];
   char  Mail_Addr2[SSIZ_MAIL_ADDR2];     
   char  Mail_Addr1[SSIZ_MAIL_ADDR1];     
   char  Return_Date_1[SSIZ_RETURN_DATE_1];  
   char  Mail_Date_2[SSIZ_MAIL_DATE_2];    
   char  Return_Date_2[SSIZ_RETURN_DATE_2];  
   char  Hldar[SSIZ_HLDAR];          
   char  Expdt[SSIZ_EXPDT];          
   char  Care_Of[SSIZ_CARE_OF];        
   char  Holdsale_Code;  
   char  S_StrNum[CSIZ_STRNUM+1];
   char  S_StrFra[CSIZ_STRFRA];
   char  S_StrDir[CSIZ_STRDIR];
   char  S_StrName[CSIZ_STRNAME];
   char  Trans_Code[SSIZ_TRANS_CODE];     
   char  SalePrice[SSIZ_SALE_AMT];       
   char  SaleFlag;                              // Sale price verified flag   
   char  SaleCode[SSIZ_SALE_CODE];              // This field looks more like DocType
   char  Owner[SSIZ_OWNER];      
   char  Remark_Date[SSIZ_REMARK_DATE];    
   char  Su_Code[SSIZ_SU_CODE];           
   char  Entry_Date[SSIZ_ENTRY_DATE];     
   char  Source_Id[SSIZ_SOURCE_ID];      
   char  Finance_Code_2[SSIZ_FINANCE_CODE_2]; 
   char  Trans_Date[SSIZ_TRANS_DATE];     
   char  CrLf[2];
} FRE_SALE;
*/

#define  SSIZ_APN                10
#define  SSIZ_TRANS_DATE         8
#define  SSIZ_INST_NUMBER        6
#define  SSIZ_VSAM_CODE          2
#define  SSIZ_FILLER_1           2
#define  SSIZ_MAIL_DATE_1        6
#define  SSIZ_DTT_AMT            7
#define  SSIZ_TD1_AMT            9
#define  SSIZ_PCT_FLAG           3
#define  SSIZ_PCT_XFER           9
#define  SSIZ_MAIL_ADDR2         25
#define  SSIZ_MAIL_ADDR1         40
#define  SSIZ_RETURN_DATE_1      6
#define  SSIZ_MAIL_DATE_2        6
#define  SSIZ_CHANGE_DATE        6
#define  SSIZ_HLDAR              6
#define  SSIZ_EXPDT              6
#define  SSIZ_CARE_OF            40
#define  SSIZ_HOLDSALE_CODE      1
#define  SSIZ_S_STRNUM           6
#define  SSIZ_S_STRFRA           3
#define  SSIZ_S_STRDIR           2
#define  SSIZ_S_STRNAME          24
#define  SSIZ_S_CITY             2
#define  SSIZ_TRANS_CODE         2
#define  SSIZ_SALE_AMT           9
#define  SSIZ_SALE_FLAG          1
#define  SSIZ_DOC_CODE           2
#define  SSIZ_OWNER              40
#define  SSIZ_ENTRY_DATE         6
#define  SSIZ_USE_CODE           7
#define  SSIZ_SOURCE_ID          8
#define  SSIZ_FINANCE_CODE       3
#define  SSIZ_RECDATE            6

#define  SOFF_APN                1
#define  SOFF_TRANS_DATE         11
#define  SOFF_INST_NUMBER        19
#define  SOFF_VSAM_CODE          25
#define  SOFF_FILLER_1           27
#define  SOFF_MAIL_DATE_1        29
#define  SOFF_DTT_AMT            35       // V99
#define  SOFF_TD1_AMT            42
#define  SOFF_PCT_FLAG           51       // "PCT" indicate percentage transfer
#define  SOFF_PCT_XFER           54       // V9
#define  SOFF_MAIL_ADDR2         63
#define  SOFF_MAIL_ADDR1         88
#define  SOFF_RETURN_DATE_1      128
#define  SOFF_MAIL_DATE_2        134
#define  SOFF_CHANGE_DATE        140
#define  SOFF_HLDAR              146
#define  SOFF_EXPDT              152
#define  SOFF_CARE_OF            158
#define  SOFF_HOLDSALE_CODE      198
#define  SOFF_S_STRNUM           199
#define  SOFF_S_STRFRA           205
#define  SOFF_S_STRDIR           208
#define  SOFF_S_STRNAME          210
#define  SOFF_S_CITY             234
#define  SOFF_TRANS_CODE         236
#define  SOFF_SALE_AMT           238
#define  SOFF_SALE_FLAG          247
#define  SOFF_DOC_CODE           248
#define  SOFF_NEWOWNER           250
#define  SOFF_ENTRY_DATE         290
#define  SOFF_USE_CODE           296
#define  SOFF_INDICATOR          303
#define  SOFF_PRINTFLG           304
#define  SOFF_CORRECTIONFLG      305
#define  SOFF_FILLER_2           306
#define  SOFF_SOURCE_ID          307
#define  SOFF_FINANCE_CODE       315
#define  SOFF_RECDATE            318

// Sale record
typedef struct _tFreSale
{  // 325-bytes
   char  Apn[SSIZ_APN];
   char  TransferDate[SSIZ_TRANS_DATE];         // yyyymmdd      
   char  DocNum[SSIZ_INST_NUMBER];    
   char  VSAM_Code[SSIZ_VSAM_CODE];    
   char  filler1[SSIZ_FILLER_1];         
   char  Mail_Date_1[SSIZ_MAIL_DATE_1];         // mmddyy
   char  Dtt_Amt[SSIZ_DTT_AMT];                 // V99
   char  TD1_Amt[SSIZ_TD1_AMT];
   char  Pct_Flag[SSIZ_PCT_FLAG];
   char  Pct_Xfer[SSIZ_PCT_XFER];               // V9
   char  Mail_Addr2[SSIZ_MAIL_ADDR2];     
   char  Mail_Addr1[SSIZ_MAIL_ADDR1];     
   char  Return_Date_1[SSIZ_RETURN_DATE_1];  
   char  Mail_Date_2[SSIZ_MAIL_DATE_2];    
   char  Change_Date_2[SSIZ_CHANGE_DATE];  
   char  Hldar[SSIZ_HLDAR];          
   char  Expdt[SSIZ_EXPDT];          
   char  Care_Of[SSIZ_CARE_OF];        
   char  Holdsale_Code;  
   char  S_StrNum[SSIZ_S_STRNUM];
   char  S_StrFra[SSIZ_S_STRFRA];
   char  S_StrDir[SSIZ_S_STRDIR];
   char  S_StrName[SSIZ_S_STRNAME];
   char  S_City[SSIZ_S_CITY];
   char  Trans_Code[SSIZ_TRANS_CODE];     
   char  SalePrice[SSIZ_SALE_AMT];       
   char  SaleFlag;                              // Sale price verified flag   
   char  DocCode[SSIZ_DOC_CODE];               
   char  Owner[SSIZ_OWNER];      
   char  Entry_Date[SSIZ_ENTRY_DATE];           // 290:mmddyy
   char  Use_Code[SSIZ_USE_CODE];               // 296
   char  IndicatorFlag;                         // 303
   char  PrintFlag;  
   char  CorrectionFlag;                        // 305:Deed correction indicator
   char  Filler2;  
   char  Source_Id[SSIZ_SOURCE_ID];             // 307 
   char  Finance_Code_2[SSIZ_FINANCE_CODE]; 
   char  RecDate[SSIZ_RECDATE];                 // 318
   char  CrLf[2];
} FRE_SALE;

#define  NOFF_APN                    1
#define  NOFF_DONT_KNOW1             10
#define  NOFF_SUPP_NAME1             59
#define  NOFF_DONT_KNOW2             99
#define  NOFF_SUPP_NAME2             108
#define  NOFF_SUPP_ADDR1             148
#define  NOFF_SUPP_ADDR2             173
#define  NOFF_SUPP_ADDR3             198
#define  NOFF_SUPP_ADDR4             223
#define  NOFF_SUPP_NARRATIVE         248
#define  NOFF_SUPP_NARRATIVE_FLAG    298
#define  NOFF_DONT_KNOW3             299
#define  NOFF_TAXDEED_CODE1          301  // L=Senior Citizen Lien
#define  NOFF_PERCENT_INT1           303
#define  NOFF_JT1                    306
#define  NOFF_CHANGE_DATE1           308
#define  NOFF_RECDATE1               313
#define  NOFF_RECNUM1                319
#define  NOFF_TAXDEED_CODE2          325
#define  NOFF_PERCENT_INT2           327
#define  NOFF_JT2                    330
#define  NOFF_CHANGE_DATE2           332
#define  NOFF_RECDATE2               337
#define  NOFF_RECNUM2                343
#define  NOFF_DONT_KNOW4             349
#define  NOFF_ADDL_OWNERS_FLAG       359

#define  NSIZ_DONT_KNOW1             49
#define  NSIZ_SUPP_NAME              40
#define  NSIZ_DONT_KNOW2             9
#define  NSIZ_SUPP_ADDR              25
#define  NSIZ_SUPP_NARRATIVE         50
#define  NSIZ_SUPP_NARRATIVE_FLAG    1
#define  NSIZ_DONT_KNOW3             2
#define  NSIZ_TDCODE                 2
#define  NSIZ_PERCENT_FIN            3
#define  NSIZ_JT                     2
#define  NSIZ_CHANGE_DATE            5
#define  NSIZ_RECDATE                6
#define  NSIZ_RECNUM                 6
#define  NSIZ_DONT_KNOW4             10
#define  NSIZ_ADDL_OWNERS_FLAG       1

// Supplemental record
typedef struct _tFreNav
{
   char  Apn[RSIZ_APN];
   char  Dontknow1[NSIZ_DONT_KNOW1];
   char  Name1[NSIZ_SUPP_NAME];
   char  Dontknow2[NSIZ_DONT_KNOW2];
   char  Name2[NSIZ_SUPP_NAME];
   char  M_Addr1[NSIZ_SUPP_ADDR];
   char  M_Addr2[NSIZ_SUPP_ADDR];
   char  M_Addr3[NSIZ_SUPP_ADDR];
   char  M_Addr4[NSIZ_SUPP_ADDR];
   char  Narative[NSIZ_SUPP_NARRATIVE];
   char  Narative_Flg;
   char  Dontknow3[NSIZ_DONT_KNOW3];
   char  TdCode1[NSIZ_TDCODE];
   char  Percent_Fin1[NSIZ_PERCENT_FIN];
   char  Jt1[NSIZ_JT];
   char  Change_Date1[NSIZ_CHANGE_DATE]; // DDDYY
   char  RecDate1[NSIZ_RECDATE];         // MMDDYY
   char  RecNum1[NSIZ_RECNUM];
   char  TdCode2[NSIZ_TDCODE];
   char  Percent_Fin2[NSIZ_PERCENT_FIN];
   char  Jt2[NSIZ_JT];
   char  Change_Date2[NSIZ_CHANGE_DATE];
   char  RecDate2[NSIZ_RECDATE];
   char  RecNum2[NSIZ_RECNUM];
   char  Dontknow4[NSIZ_DONT_KNOW4];
   char  Addl_Owners_Flag;
} FRE_NAV;

// TRA record - Public parcels
#define  TOFF_APN                    1
#define  TOFF_TRA                    11
#define  TOFF_OWNER                  17
#define  TOFF_TYPE                   57
#define  TOFF_REMARK                 61
#define  TOFF_ACREAGE                79

#define  TSIZ_APN                    10
#define  TSIZ_TRA                    6
#define  TSIZ_OWNER                  40
#define  TSIZ_TYPE                   4
#define  TSIZ_REMARK                 18
#define  TSIZ_ACREAGE                8

typedef struct _tFreTra
{
   char  Apn[TSIZ_APN];
   char  TRA[TSIZ_TRA];
   char  Owner[TSIZ_OWNER];
   char  Type[TSIZ_TYPE];
   char  Remark[TSIZ_REMARK];
   char  Acreage[TSIZ_ACREAGE];     // V99
   char  CrLf[2];
} FRE_TRA;

/* 2006-2013
#define  LOFF_APN                      1
#define  LOFF_TRA                      10
#define  LOFF_FILLER1                  16
#define  LOFF_LAND                     17
#define  LOFF_FILLER2                  27
#define  LOFF_IMPR_FIXT                28
#define  LOFF_FILLER3                  38
#define  LOFF_FIXT                     39
#define  LOFF_FILLER4                  49
#define  LOFF_PERSPROP_MB              50
#define  LOFF_FILLER5                  60
#define  LOFF_CLS_EXE_AMT              61
#define  LOFF_FILLER6                  69
#define  LOFF_CLS_EXE_COUNT            70
#define  LOFF_CLS_EXE_CODE             71
#define  LOFF_FILLER7                  72
#define  LOFF_HOEXE                    73
#define  LOFF_HOEXE_CODE               81
#define  LOFF_FILLER8                  82
#define  LOFF_ASSR_USED                83
#define  LOFF_FILLER9                  114
#define  LOFF_PEST_ASSMNT              115
#define  LOFF_PEST_CODE                124
#define  LOFF_FILLER10                 125
#define  LOFF_SOLD2STATE_DATE          129
#define  LOFF_REDEMPTION_DATE          134
#define  LOFF_SOLD2STATE_NUMBER        139
#define  LOFF_FILLER11                 148
#define  LOFF_NAME1                    162
#define  LOFF_FILLER12                 202
#define  LOFF_NAME2                    211
#define  LOFF_ADDR1                    251
#define  LOFF_ADDR2                    276
#define  LOFF_ADDR3                    301
#define  LOFF_ADDR4                    326
#define  LOFF_NARRATIVE                351
#define  LOFF_NARRATIVE_FLAG           401
#define  LOFF_FILLER13                 402
#define  LOFF_PERSPROP_PEN_FLAG        403
#define  LOFF_SENIOR_CIT_CODE          404
#define  LOFF_FILLER14                 405
#define  LOFF_MB_HOME_VALUE            406

#define  LSIZ_APN                      9
#define  LSIZ_TRA                      6
#define  LSIZ_FILLER1                  1
#define  LSIZ_LAND                     10
#define  LSIZ_FILLER2                  1
#define  LSIZ_IMPR_FIXT                10
#define  LSIZ_FILLER3                  1
#define  LSIZ_FIXT                     10
#define  LSIZ_FILLER4                  1
#define  LSIZ_PERSPROP_MB              10
#define  LSIZ_FILLER5                  1
#define  LSIZ_CLS_EXE_AMT              8
#define  LSIZ_FILLER6                  1
#define  LSIZ_CLS_EXE_COUNT            1
#define  LSIZ_CLS_EXE_CODE             1
#define  LSIZ_FILLER7                  1
#define  LSIZ_HOEXE                    8
#define  LSIZ_HOEXE_CODE               1
#define  LSIZ_FILLER8                  1
#define  LSIZ_ASSR_USED                31
#define  LSIZ_FILLER9                  1
#define  LSIZ_PEST_ASSMNT              9
#define  LSIZ_PEST_CODE                1
#define  LSIZ_FILLER10                 4
#define  LSIZ_SOLD2STATE_DATE          5
#define  LSIZ_REDEMPTION_DATE          5
#define  LSIZ_SOLD2STATE_NUMBER        9
#define  LSIZ_FILLER11                 14
#define  LSIZ_NAME1                    40
#define  LSIZ_FILLER12                 9
#define  LSIZ_NAME2                    40
#define  LSIZ_ADDR                    25
#define  LSIZ_NARRATIVE                50
#define  LSIZ_NARRATIVE_FLAG           1
#define  LSIZ_FILLER13                 1
#define  LSIZ_PERSPROP_PEN_FLAG        1
#define  LSIZ_SENIOR_CIT_CODE          1
#define  LSIZ_FILLER14                 1
#define  LSIZ_MB_HOME_VALUE            10
*/
// 2013 LDR
#define  LOFF_APN                      1
#define  LOFF_TRA                      10
#define  LOFF_FILLER1                  16
#define  LOFF_LAND                     17
#define  LOFF_FILLER2                  27
#define  LOFF_IMPR_FIXT                28
#define  LOFF_FILLER3                  38
#define  LOFF_FIXT                     39
#define  LOFF_FILLER4                  49
#define  LOFF_PERSPROP_MB              50
#define  LOFF_FILLER5                  60
#define  LOFF_CLS_EXE_AMT              61
#define  LOFF_FILLER6                  70

#define  LOFF_CLS_EXE_COUNT            71
#define  LOFF_CLS_EXE_CODE             72
#define  LOFF_FILLER7                  73
#define  LOFF_HOEXE                    74
#define  LOFF_HOEXE_CODE               82
#define  LOFF_FILLER8                  83
#define  LOFF_ASSR_USED                84
#define  LOFF_FILLER9                  115
#define  LOFF_PEST_ASSMNT              116
#define  LOFF_PEST_CODE                125
#define  LOFF_FILLER10                 126
#define  LOFF_SOLD2STATE_DATE          130
#define  LOFF_REDEMPTION_DATE          135
#define  LOFF_SOLD2STATE_NUMBER        140
#define  LOFF_FILLER11                 149
#define  LOFF_NAME1                    163
#define  LOFF_FILLER12                 203
#define  LOFF_NAME2                    212
#define  LOFF_ADDR1                    252
#define  LOFF_ADDR2                    277
#define  LOFF_ADDR3                    302
#define  LOFF_ADDR4                    327
#define  LOFF_NARRATIVE                352
#define  LOFF_NARRATIVE_FLAG           402
#define  LOFF_FILLER13                 403
#define  LOFF_PERSPROP_PEN_FLAG        404
#define  LOFF_SENIOR_CIT_CODE          405
#define  LOFF_FILLER14                 406
#define  LOFF_MB_HOME_VALUE            407

#define  LSIZ_APN                      9
#define  LSIZ_TRA                      6
#define  LSIZ_FILLER1                  1
#define  LSIZ_LAND                     10
#define  LSIZ_FILLER2                  1
#define  LSIZ_IMPR_FIXT                10
#define  LSIZ_FILLER3                  1
#define  LSIZ_FIXT                     10
#define  LSIZ_FILLER4                  1
#define  LSIZ_PERSPROP_MB              10
#define  LSIZ_FILLER5                  1
#define  LSIZ_CLS_EXE_AMT              9

#define  LSIZ_FILLER6                  1
#define  LSIZ_CLS_EXE_COUNT            1
#define  LSIZ_CLS_EXE_CODE             1
#define  LSIZ_FILLER7                  1
#define  LSIZ_HOEXE                    8
#define  LSIZ_HOEXE_CODE               1
#define  LSIZ_FILLER8                  1
#define  LSIZ_ASSR_USED                31
#define  LSIZ_FILLER9                  1
#define  LSIZ_PEST_ASSMNT              9
#define  LSIZ_PEST_CODE                1
#define  LSIZ_FILLER10                 4
#define  LSIZ_SOLD2STATE_DATE          5
#define  LSIZ_REDEMPTION_DATE          5
#define  LSIZ_SOLD2STATE_NUMBER        9
#define  LSIZ_FILLER11                 14
#define  LSIZ_NAME1                    40
#define  LSIZ_FILLER12                 9
#define  LSIZ_NAME2                    40
#define  LSIZ_ADDR                     25
#define  LSIZ_NARRATIVE                50
#define  LSIZ_NARRATIVE_FLAG           1
#define  LSIZ_FILLER13                 1
#define  LSIZ_PERSPROP_PEN_FLAG        1
#define  LSIZ_SENIOR_CIT_CODE          1
#define  LSIZ_FILLER14                 1
#define  LSIZ_MB_HOME_VALUE            10

// Roll Record
typedef struct _tFreLien
{  // 439-bytes 
   char  Apn[LSIZ_APN];
   char  TRA[LSIZ_TRA];
   char  filler1;
   char  Land[LSIZ_LAND];                          // 17
   char  filler2;
   char  Impr[LSIZ_IMPR_FIXT];                     // 28
   char  filler3;
   char  Fixt[LSIZ_FIXT];                          // 39
   char  filler4;
   char  PP_Val[LSIZ_PERSPROP_MB];                 // 50
   char  filler5;
   char  ClsExe[LSIZ_CLS_EXE_AMT];                 // 61-69
   char  filler6;
   char  ClsExe_Cnt;                               // 71
   char  ClsExe_Code;                              // 72 C,E,F,G,H,I,J,K,L,M,O,S,T,U,Z
   char  filler7;
   char  HOExe[LSIZ_HOEXE];                        // 74-81
   char  HOExe_Code;                               // 82 A=HO
   char  filler8;
   char  AssrUse[LSIZ_ASSR_USED];                  // 84-114
   char  filler9;
   char  Pest_Assmt[LSIZ_PEST_ASSMNT];             // 116 - Pest Control Value
   char  Pest_Code;                                // 125 - Pest Control Code
   char  filler10[LSIZ_FILLER10];
   char  Sold2St_Date[LSIZ_SOLD2STATE_DATE];       // 130 YYDDD - Julian date - off=120
   char  Redemption_Date[LSIZ_REDEMPTION_DATE];    // 135 YYDDD
   char  Sold2St_Num[LSIZ_SOLD2STATE_NUMBER];      // 140-148
   char  filler11[LSIZ_FILLER11];
   char  Name1[LSIZ_NAME1];                        // 163-202
   char  filler12[LSIZ_FILLER12];
   char  Name2[LSIZ_NAME2];                        // 212-251
   char  M_Addr1[LSIZ_ADDR];                       // 252
   char  M_Addr2[LSIZ_ADDR];                       // 277
   char  M_Addr3[LSIZ_ADDR];                       // 302
   char  M_Addr4[LSIZ_ADDR];                       // 327
   char  Narative[LSIZ_NARRATIVE];                 // 352 - Legal or situs
   char  Narative_Flag;                            // 402 - '*' Narratiave takes precedance 
   char  filler13;
   char  PP_Pen_Flag;                              // 404 - Personal Property Penalty Indicator ( 1 )
   char  Senior_Code;
   char  filler14;
   char  MH_Value[LSIZ_MB_HOME_VALUE];             // 407
   char  filler[33];        // Reserve extra space
} FRE_LIEN;

// Tax District layout
#define  TSIZ_APN                      10
#define  TSIZ_AGENCY                   18
#define  TSIZ_TAXCODE                  4
#define  TSIZ_EXECODE                  26
#define  TSIZ_TAXRATE                  11
#define  TSIZ_AMOUNT                   11
#define  TSIZ_EXEAMT                   10
#define  TSIZ_TAXYEAR                  9
#define  TSIZ_DATE                     8

typedef struct _tFreDetail
{  // 60-bytes 
   char  Apn[TSIZ_APN];
   char  filler1[1];
   char  Agency[TSIZ_AGENCY];
   char  filler2[1];
   char  TaxCode[TSIZ_TAXCODE];
   char  filler3[1];
   char  TaxRate[TSIZ_TAXRATE];
   char  filler4[1];
   char  TaxAmt[TSIZ_AMOUNT];
   char  CrLf[2];
} FRE_DETAIL;

typedef struct _tBaseInstallment
{  // 73 bytes
   char  DueDate[TSIZ_DATE];        // 58
   char  TotalDue[TSIZ_AMOUNT];     // 66
   char  TaxAmt[TSIZ_AMOUNT];       // 77
   char  PenAmt[TSIZ_AMOUNT];       // 88
   char  FeeAmt[TSIZ_AMOUNT];       // 99
   char  filler1[1];
   char  PaidDate[TSIZ_DATE];       // 111
   char  PaidAmt[TSIZ_AMOUNT];      // 119
   char  filler2[1];                // 130
} BASE_INST;

// Tax Bill layout
typedef struct _tFreTaxBill
{  // 345-bytes 
   char  Apn[TSIZ_APN];             // 1
   char  ExeCode[TSIZ_EXECODE];     // 11
   char  ExeAmt[TSIZ_EXEAMT];       // 37
   char  filler1[1];  
   char  TaxYear[TSIZ_TAXYEAR];     // 48
   char  filler2[1];
   BASE_INST sInst1;                // 58
   BASE_INST sInst2;                // 131
   char  DefaultDate[TSIZ_DATE];    // 204
   char  DelqAmt[TSIZ_AMOUNT];      // 212
   char  NetTaxable[TSIZ_AMOUNT];   // 223
   char  Land[TSIZ_AMOUNT];         // 234
   char  Impr[TSIZ_AMOUNT];         // 245
   char  Fixt_Val[TSIZ_AMOUNT];     // 256
   char  PP_Val[TSIZ_AMOUNT];       // 267
   char  MH_Val[TSIZ_AMOUNT];       // 278
   char  Pest[TSIZ_AMOUNT];         // 289
   char  Esc_Ex_Val[TSIZ_AMOUNT];   // 300
   char  Misc_Ex[TSIZ_AMOUNT];      // 311
   char  filler3[1];                // 322
   char  Ex_Cnt[1];                 // 323
   char  HO_Ex[TSIZ_AMOUNT];        // 324
   char  BI_Ex[TSIZ_AMOUNT];        // 335
   char  CrLf[2];
} FRE_TAX;

#define  FRE_BASE_APN         9
#define  FRE_BASE_APNSFX      2
#define  FRE_BASE_TRA         6
#define  FRE_BASE_OWNER       40
#define  FRE_BASE_ADDR        25
#define  FRE_BASE_NARRATIVE   50
#define  FRE_BASE_AMOUNT      11
#define  FRE_BASE_TAXRATE     11
#define  FRE_BASE_ESC_TITLE   30
#define  FRE_BASE_ROLL_TYPE   1
#define  FRE_BASE_YEAR        2
#define  FRE_BASE_PROJ_CODE   4
#define  FRE_BASE_DATE        8
#define  FRE_BASE_PR_FACTOR   4
#define  FRE_BASE_PCT         8

// SupplementalBase.txt
typedef struct _tFreSupBase
{  // 620-bytes
   char  Apn               [FRE_BASE_APN];         // 1
   char  ApnSfx            [FRE_BASE_APNSFX];      // 10
   char  Tra               [FRE_BASE_TRA];         // 12
   char  Owner1            [FRE_BASE_OWNER];
   char  Owner2            [FRE_BASE_OWNER];
   char  Addr1             [FRE_BASE_ADDR];
   char  Addr2             [FRE_BASE_ADDR];
   char  Addr3             [FRE_BASE_ADDR];
   char  Addr4             [FRE_BASE_ADDR];
   char  Legal             [FRE_BASE_NARRATIVE];   // 198
   char  NarrativeInd;                             // 248
   char  NetTaxable        [FRE_BASE_AMOUNT];      // 249 (first byte is sign)
   char  Land              [FRE_BASE_AMOUNT];      // 260
   char  Impr              [FRE_BASE_AMOUNT];      // 271
   char  Tfi               [FRE_BASE_AMOUNT];
   char  Pers              [FRE_BASE_AMOUNT];
   char  MobilHome         [FRE_BASE_AMOUNT];
   char  Pest              [FRE_BASE_AMOUNT];
   char  Esc_Exe           [FRE_BASE_AMOUNT];
   char  Exe_Misc1         [FRE_BASE_AMOUNT];
   char  Misc1_Code;
   char  Misc1_Cnt;
   char  HO_Exe            [FRE_BASE_AMOUNT];      // 350 - Home owner exemption
   char  BI_Exe            [FRE_BASE_AMOUNT];      // 361
   char  CitzPost;                                 // 372
   char  Neg_Rate_Ind;
   char  TaxRate           [FRE_BASE_TAXRATE];     // 374 - Format -99.9(7)
   char  Esc_Title         [FRE_BASE_ESC_TITLE];   // 385
   char  Esc_Amount        [FRE_BASE_AMOUNT];      // 415
   char  Roll_Type;                                // 426 - C/R
   char  Rate_Yr           [FRE_BASE_YEAR];        // 427
   char  Roll_Yr           [FRE_BASE_YEAR];        // 429
   char  Proj_Code         [FRE_BASE_PROJ_CODE];   // 431
   char  Avtivity_Date     [FRE_BASE_DATE];        // 435
   char  Asmt_Ratio_Ind;                           // 443
   char  Prorate_Factor    [FRE_BASE_PR_FACTOR];   // 444
   char  Prorate_BegDate   [FRE_BASE_DATE];        // 448 - YYYYMMDD
   char  Prorate_EndDate   [FRE_BASE_DATE];        // 456
   char  New_Base_Land     [FRE_BASE_AMOUNT];      // 464
   char  New_Base_Impr     [FRE_BASE_AMOUNT];      // 475
   char  New_Base_Tfi      [FRE_BASE_AMOUNT];      // 486
   char  New_Base_MobilHome[FRE_BASE_AMOUNT];      // 497
   char  Old_Base_Land     [FRE_BASE_AMOUNT];      // 508
   char  Old_Base_Impr     [FRE_BASE_AMOUNT];      // 519
   char  Old_Base_Tfi      [FRE_BASE_AMOUNT];      // 530
   char  Old_Base_MobilHome[FRE_BASE_AMOUNT];      // 541
   char  Prev_SS_Land      [FRE_BASE_AMOUNT];      // 552
   char  Prev_SS_Impr      [FRE_BASE_AMOUNT];
   char  Prev_SS_Tfi       [FRE_BASE_AMOUNT];
   char  Prev_SS_MobilHome [FRE_BASE_AMOUNT];
   char  NewOwner_Flg;                             // 596 - Y/N
   char  NewOwner_Pct      [FRE_BASE_PCT];         // 597
   char  BillDate          [FRE_BASE_DATE];        // 605
   char  filler            [7];
   char  EndOfRec;                                 // 620 - X
   char  CrLf              [2];
} FRE_SUPBASE;

typedef struct _tSupInstallment
{  // 73 bytes
   char  DueDate     [TSIZ_DATE];         // 12
   char  DueDateFlg;
   char  InstAmt     [TSIZ_AMOUNT];       // 21
   char  TaxAmt      [TSIZ_AMOUNT];       // 32
   char  PenAmt      [TSIZ_AMOUNT];       // 43
   char  FeeAmt      [TSIZ_AMOUNT];       // 54
   char  PaidDate    [TSIZ_DATE];         // 65
   char  PaidAmt     [TSIZ_AMOUNT];       // 73
   char  PaidFlg;                         // 84
   char  PenDue;                          // 85
   char  PenPaid;                         // 86
} SUP_INST;

#define  FRE_INST_APN         9
#define  FRE_INST_APNSFX      2

typedef struct _tFreSupInst
{  // 162-bytes
   char     Apn      [FRE_INST_APN];
   char     ApnSfx   [FRE_INST_APNSFX];
   SUP_INST Inst1;
   SUP_INST Inst2;
   char     EndOfRec;
   char     CrLf     [2];
} FRE_SUPINST;

IDX_TBL5 FRE_DocTbl[] =
{  // DocTitle, Index, Non-sale, len1, len2
   "GD  ", "1 ", 'N', 3, 2,
   "QC  ", "4 ", 'N', 3, 2,
   "TD  ", "27", 'N', 3, 2, // Trustee's Deed
   "DE  ", "13", 'N', 3, 2,
   "WD  ", "28", 'N', 3, 2,
   "","",0,0,0
};

#define  FRE_SUP_APN          9
#define  FRE_SUP_APNSFX       2
#define  FRE_SUP_FUNDNBR      6
#define  FRE_SUP_TAXCODE      4
#define  FRE_SUP_TAXGRP       1
#define  FRE_SUP_RATE_YR      2
#define  FRE_SUP_AMOUNT       11

// SupplementalRevDistrict.txt
typedef struct _tFreSupDetail
{  // 36-bytes
   char  Apn      [FRE_SUP_APN];       // 1
   char  ApnSfx   [FRE_SUP_APNSFX];    // 10
   char  FundNbr  [FRE_SUP_FUNDNBR];   // 12
   char  TaxCode  [FRE_SUP_TAXCODE];   // 18
   char  TaxGrp   [FRE_SUP_TAXGRP];    // 22
   char  Rate_Yr  [FRE_SUP_RATE_YR];   // 23
   char  TaxAmt   [FRE_SUP_AMOUNT];    // 25
   char  EndOfRec;                     // 36 -X
   char  CrLf     [2];
} FRE_SUPDET;

IDX_TBL5 FRE_DocCode[] =
{// DOCCODE, DOCTYPE, NONSALE, CODELEN, TYPELEN
   "01","1 ",'N',2,2,  // Sale-Reappraise
   "02","18",'Y',2,2,  // Interspousal Transfer
   "03","57",'N',2,2,  // Partial Interest
   "05","75",'N',2,2,  // Transfer - interfamily
   "08","75",'Y',2,2,  // Transfer for loan purposes. Tax redemption. Partitioning. Change of trustee
   "09","19",'Y',2,2,  // Non market sale, tax sale, improvement only
   "10","13",'Y',2,2,  // Judicial action - Court Order. Death. Decree of distribution. Estate sale. Sheriff�s deed. Foreclosure. Trustee�s deed (sale). Deeded to lien holder
   "11","1 ",'N',2,2,  // Administrative - New parcels. Clearing or perfecting title. P.C.R.s. Collector�s deeds
   "12","74",'Y',2,2,  // Prop 58-Non Reappraisal
   "13","74",'Y',2,2,  // Trust - in/out revocable trust
   "14","74",'Y',2,2,  // Trust - in/out irrevocable trust
   "15","3 ",'N',2,2,  // Join tenancy - Termination of last original transferor�s interest
   "16","3 ",'Y',2,2,  // Join tenancy - Creation or continuation of joint tenancy that includes at least one original transferor
   "17","44",'N',2,2,  // Lease - Creation or transfer of a leasehold interest with a term of 35 years or more including options, or the termination of a leasehold interest with an initial term of 35 years or more including options
   "18","19",'Y',2,2,  // Section 11 - Transfer to a tax exempt agency outside its jurisdiction
   "19","74",'Y',2,2,  // Life estate - Reserving a life estate, or remainderman grants back.
   "20","13",'N',2,2,  // Life estate - Death of life estate holder, transfer of life estate
   // Multi sale
   "21","1 ",'N',2,2,  // Sale - Multi parcels
   "22","18",'Y',2,2,  // Interspousal Transfer - Multi parcels
   "23","57",'N',2,2,  // Partial Interest - Multi parcels
   "25","75",'N',2,2,  // Transfer - interfamily - multi parcels
   "28","74",'Y',2,2,  // Transfer for loan purposes. Tax redemption. Partitioning. Change of trustee
   "29","19",'Y',2,2,  // Non market sale, tax sale, improvement only
   "30","13",'Y',2,2,  // Judicial action - Court Order. Death. Decree of distribution. Estate sale. Sheriff�s deed. Foreclosure. Trustee�s deed (sale). Deeded to lien holder
   "31","74",'Y',2,2,  // Administrative - New parcels. Clearing or perfecting title. P.C.R.s. Collector�s deeds
   "32","74",'Y',2,2,  // Prop 58-Non Reappraisal
   "33","74",'Y',2,2,  // Trust - in/out revocable trust
   "34","74",'Y',2,2,  // Trust - in/out irrevocable trust
   "35","3 ",'N',2,2,  // Join tenancy - Termination of last original transferor�s interest
   "36","3 ",'Y',2,2,  // Join tenancy - Creation or continuation of joint tenancy that includes at least one original transferor
   "37","44",'N',2,2,  // Lease - Creation or transfer of a leasehold interest with a term of 35 years or more including options, or the termination of a leasehold interest with an initial term of 35 years or more including options
   "38","19",'Y',2,2,  // Section 11 - Transfer to a tax exempt agency outside its jurisdiction
   "39","19",'Y',2,2,  // Life estate - Reserving a life estate, or remainderman grants back.
   "40","13",'N',2,2,  // Life estate - Death of life estate holder, transfer of life estate
   "","",0,0,0
};

// Standard codes
// H=Homeowners
// D=Disabled Veterans
// V=Veterans
// C=Church
// L=Library
// M=Museums
// R=Religion
// T=Tribal Housing
// W=Welfare
// U=College
// P=Public School
// E=Cemetery
// I=Hospital
// S=School
// X=Misc exempt
// Y=Military

IDX_TBL4 FRE_Exemption[] = 
{
   "A", "H", 1,1,
   "C", "D", 1,1,
   "D", "V", 1,1,
   "E", "C", 1,1,
   "F", "W", 1,1,
   "G", "S", 1,1,
   "H", "U", 1,1,
   "I", "I", 1,1,
   "J", "R", 1,1,
   "K", "X", 1,1,    // HM AGED
   "L", "S", 1,1,    // LEASE SCHOOL
   "M", "L", 1,1,    // LEASE LIBRARY
   "N", "X", 1,1,    // LEASE EQUIPMENT
   "O", "X", 1,1,    // LOW INCOME
   "P", "V", 1,1,
   "Q", "X", 1,1,    // HISTORICAL AIRCRAFT
   "R", "U", 1,1,
   "S", "R", 1,1,
   "T", "X", 1,1,    // INDIAN HOUSING
   "U", "M", 1,1,
   "V", "S", 1,1,    // GRADE SCHOOl
   "Y", "X", 1,1,    // CONSULAR
   "Z", "X", 1,1,    // LOW VALUE
   "","",0,0
};

#endif