2) Sale file 2_Year_Transfer_List.TXT is not always come at the same time
   of roll file. TO automate daily process, this file should be excluded.

1) Need code description of
   - Condition code
   - Parking code
   - View code
   - Slope code
   - Topography code
   - Hazard code
   - Remodel code
   - Amenities code

IE620XRF code description
-------------------------
P = Prop 13 indexed base
N = New base for this year (transfer or construction)
L = Prop 8 for this year
R = Prop 8 last year - restored to Prop 13 indexed base this year
W = Restricted value computation used (usually Williamson Act (CLCA) or historical property)

Populate prop8 instruction
--------------------------
UPDATE ALA_ROLL SET PROP8_FLG='Y' WHERE APN IN
(SELECT APN FROM ALA_2010P8 WHERE PROP8FLG='L')

