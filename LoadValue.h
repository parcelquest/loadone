#ifndef  _LOADVALUE_H_
#define  _LOADVALUE_H_


#define  VT_ENROLLED         1
#define  VT_ADJBASEYR        2
#define  VT_BASEYEAR         3
#define  VT_PROP8            8

#define  PV_SIZ_APN          20
#define  PV_SIZ_VST          2
#define  PV_SIZ_YEAR         8
#define  PV_SIZ_LAND         12
#define  PV_SIZ_IMPR         12
#define  PV_SIZ_OTHER        12
#define  PV_SIZ_TOTALVAL     12
#define  PV_SIZ_EXE          12
#define  PV_SIZ_FIXTR        12
#define  PV_SIZ_PERSPROP     12
#define  PV_SIZ_PP_MH        12
#define  PV_SIZ_BUSINV       12
#define  PV_SIZ_GROWIMPR     12
#define  PV_SIZ_TREEVINES    12
#define  PV_SIZ_MINERAL      12
#define  PV_SIZ_TIMBER       12
#define  PV_SIZ_CLCALAND     12
#define  PV_SIZ_CLCAIMPR     12
#define  PV_SIZ_HOMESITE     12
#define  PV_SIZ_OTH_IMPR     12
#define  PV_SIZ_FIXTR_RP     12
#define  PV_SIZ_HH_PP        12
#define  PV_SIZ_REASON       4
#define  PV_SIZ_MISC         50
#define  PV_SIZ_CODE         2

#define  PV_OFF_APN          1
#define  PV_OFF_VST          21
#define  PV_OFF_YEAR         23
#define  PV_OFF_LAND         31
#define  PV_OFF_IMPR         43
#define  PV_OFF_OTHER        55
#define  PV_OFF_TOTAL_VAL    67 
#define  PV_OFF_EXE          79
#define  PV_OFF_FIXTR        91
#define  PV_OFF_PERSPROP     103
#define  PV_OFF_PP_MH        115
#define  PV_OFF_BUSINV       127
#define  PV_OFF_GROWIMPR     139
#define  PV_OFF_TREEVINES    151
#define  PV_OFF_MINERAL      163
#define  PV_OFF_TIMBER       175
#define  PV_OFF_CLCALAND     187
#define  PV_OFF_CLCAIMPR     199
#define  PV_OFF_HOMESITE     211
#define  PV_OFF_OTH_IMPR     223
#define  PV_OFF_FIXTR_RP     235
#define  PV_OFF_HH_PP        247
#define  PV_OFF_REASON       259
#define  PV_OFF_BY_VAL       263
#define  PV_OFF_NET_VAL      275
#define  PV_OFF_FIXTR_TYPE   287
#define  PV_OFF_EXE_TYPE     289
#define  PV_OFF_MISC         291

typedef struct _tValueRec
{  // 512-byte record
   char  Apn            [PV_SIZ_APN      ];  // 1
   char  ValueSetType   [PV_SIZ_VST      ];  // 21
   char  TaxYear        [PV_SIZ_YEAR     ];  // 23
   char  Land           [PV_SIZ_LAND     ];  // 31
   char  Impr           [PV_SIZ_IMPR     ];  // 43
   char  Other          [PV_SIZ_OTHER    ];  // 55
   char  TotalVal       [PV_SIZ_TOTALVAL ];  // 67    - gross
   char  Exe            [PV_SIZ_EXE      ];  // 79
   char  Fixtr          [PV_SIZ_FIXTR    ];  // 91
   char  PersProp       [PV_SIZ_PERSPROP ];  // 103
   char  PP_MH          [PV_SIZ_PP_MH    ];  // 115
   char  Businv         [PV_SIZ_BUSINV   ];  // 127
   char  GrowImpr       [PV_SIZ_GROWIMPR ];  // 139
   char  TreeVines      [PV_SIZ_TREEVINES];  // 151
   char  Mineral        [PV_SIZ_MINERAL  ];  // 163
   char  Timber         [PV_SIZ_TIMBER   ];  // 175
   char  ClcaLand       [PV_SIZ_CLCALAND ];  // 187
   char  ClcaImpr       [PV_SIZ_CLCAIMPR ];  // 199
   char  HomeSite       [PV_SIZ_HOMESITE ];  // 211
   char  Oth_Impr       [PV_SIZ_OTH_IMPR ];  // 223
   char  Fixtr_RP       [PV_SIZ_FIXTR_RP ];  // 235
   char  HH_PP          [PV_SIZ_HH_PP    ];  // 247
   char  Reason         [PV_SIZ_REASON   ];  // 259
   char  BaseYearVal    [PV_SIZ_LAND     ];  // 263   - not used
   char  NetVal         [PV_SIZ_LAND     ];  // 275   - net taxable amt
   char  Fixtr_Type     [PV_SIZ_CODE     ];  // 287
   char  Exe_Type       [PV_SIZ_CODE     ];  // 289
   char  Misc           [PV_SIZ_MISC     ];  // 291
   char  filler[170];                        // 341
   char  CRLF[2];                            // 511
} VALUE_REC;

int createValueImport(LPSTR pInfile, LPSTR pOutfile, bool bInclHdr=false);

#endif