if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Lax_GrGr]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[Lax_GrGr]
GO

CREATE TABLE [dbo].[Lax_GrGr] (
	[DocNum] [varchar] (16) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[DocTitle] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[Apn] [varchar] (16) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[Grtee1] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[Grtee2] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[Grtor1] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[Grtor2] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[NameCnt] [int] NULL ,
	[DocDate] [int] NULL ,
	[DocTax] [int] NULL ,
	[SalePrice] [int] NULL ,
	[NumPages] [int] NULL ,
	[NumParcels] [int] NULL 
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[Lax_GrGr] WITH NOCHECK ADD 
	CONSTRAINT [PK_Lax_GrGr] PRIMARY KEY  CLUSTERED 
	(
		[DocNum]
	)  ON [PRIMARY] 
GO

 CREATE  INDEX [IX_Lax_GrGr] ON [dbo].[Lax_GrGr]([Apn]) ON [PRIMARY]
GO

