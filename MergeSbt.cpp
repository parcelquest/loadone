/*********************************** loadScl ********************************
 *
 * Input files:
 *    - Sbt_Lien      (lien file, fixed length 346 /w CRLF)
 *    - Assess        (roll file, fixed length 346 /w CRLF)
 *    - Nontax        (roll file, fixed length 346 /w CRLF)
 *
 * Fields not used:
 *    -
 *
 * Following are tasks to be done here:
 *    - To load lien, run -CSBT -L -O
 *    - For monthly update, run -SBT -U -O
 *
 * 10/01/2006 1.2.37    First release
 * 10/01/2006 1.2.37.1  Fix Acres calculation bug 
 * 10/01/2006 1.2.37.2  Fix HO_Flg bug in Sbt_ExtrLien()
 * 07/31/2007 1.3.23    Fix Sbt_MergeSAdr() and use lot dimension to calculate
 *                      LotSqft when LotAcre is not available.
 * 03/21/2008 1.5.7.1   Use standard function to update usecode.  
 * 12/11/2008 8.5.1     Make StrNum left justified
 *
 ****************************************************************************/

#include "stdafx.h"
#include "CountyInfo.h"
#include "hlAdo.h"
#include "Prodlib.h"
#include "R01.h"
#include "RecDef.h"
#include "Tables.h"
#include "Logs.h"
#include "LoadDef.h"
#include "Utils.h"
#include "XlatTbls.h"
#include "doSort.h"
#include "doOwner.h"
#include "formatApn.h"
#include "LoadOne.h"
#include "MergeSbt.h"
#include "UseCode.h"
#include "LOExtrn.h"

static   FILE  *fdRoll, *fdLien;
static   char  NonTaxFile[_MAX_PATH];

/******************************** Sbx_MergeOwner *****************************
 *
 * Return 0 if successful, >0 if error
 *
 *****************************************************************************/

void Sbt_MergeOwner(char *pOutbuf, char *pRollRec)
{
   int   iTmp, iRet;
   char  acTmp1[128], acTmp[128], acSave1[64];
   char  acName1[64], *pTmp, *pTmp1, *pTmp2;
   bool  bKeep=false;

   OWNER myOwner;
   SBT_ROLL *pRec = (SBT_ROLL *)pRollRec;

   // Clear output buffer if needed
   memset(pOutbuf+OFF_NAME1, ' ', SIZ_NAME1+SIZ_NAME2);
   memset(pOutbuf+OFF_NAME_SWAP, ' ', SIZ_NAME_SWAP);

   if (pRec->Curr_Owner[0] > ' ')
      memcpy(acTmp, pRec->Curr_Owner, RSIZ_CURR_OWNER);
   else
      memcpy(acTmp, pRec->Lien_Owner, RSIZ_CURR_OWNER);
   acTmp[RSIZ_CURR_OWNER] = 0;

   if (pTmp = strchr(acTmp, '('))
      *pTmp = 0;        // Drop what in parenthesis

   // Eliminate ETAL
   if ((pTmp=strstr(acTmp, " ETAL"))  || (pTmp=strstr(acTmp, " ET AL")) ||
       (pTmp=strstr(acTmp, " ETUX"))  || (pTmp=strstr(acTmp, " ET UX")) ||
       (pTmp=strstr(acTmp, " ETVIR")) || (pTmp=strstr(acTmp, " ET VIR")) )
      *++pTmp = 0;

   // Replace - with & and move it to name1.  If more than one found, drop
   // everything after the second one except when only one word goes before
   // the first '-'
   if (pTmp = strrchr(acTmp, '-'))
   {
      *pTmp++ = 0;

      // Remove TRUST in middle of name: 
      // BARBOSA RAYMOND A TRUST-VIRGINIA
      if ((pTmp1 = strstr(acTmp, "REV LIV TR")) || (pTmp1 = strstr(acTmp, "TRUST")) )
         *pTmp1 = 0;

      // Search for second '-'
      // FIGUEROA SILVINO-NATIVIDAD C-JOSIE C 
      // ANDERSON-PAPILLION NATHAN-GERLIE
      // HANETA ATAE-SUMIKO REV LIVE TR-SUMIKO
      if (pTmp2 = strchr(acTmp, '-'))
      {
         *pTmp2 = 0;       // Temporarily remove '-'
         if (pTmp1 = strchr(acTmp, ' '))
         {
            pTmp = pTmp2+1;
         } else
            *pTmp2 = '-';  // Put '-' back and keep it in name1
      }
      sprintf(acTmp1, "%s & %s ", acTmp, pTmp);
      strcpy(acTmp, acTmp1);
   }

   // Remove TRUSTEES
   if ((pTmp1=strstr(acTmp, " TRUSTEE")) || (pTmp1=strstr(acTmp, " TRS")) )
      *pTmp1 = 0;

   // Save Owner1
   strcpy(acSave1, acTmp);
   strcpy(acName1, acTmp);

   // Remove TRUST to format swapname
   if ((pTmp1=strstr(acName1, " REVOC "))    || 
       (pTmp1=strstr(acName1, " REVOCABLE")) ||
       (pTmp1=strstr(acName1, " REV "))      || 
       (pTmp1=strstr(acName1, " IRREV"))     ||
       (pTmp1=strstr(acName1, " FAMILY "))   ||
       (pTmp1=strstr(acName1, " FAM "))      ||
       (pTmp1=strstr(acName1, " LIVING "))   ||
       (pTmp1=strstr(acName1, " LIV "))      ||
       (pTmp1=strstr(acName1, " TESTAMENTARY")) || 
       (pTmp1=strstr(acName1, " INTER VIVOS"))  ||
       (pTmp1=strstr(acName1, " DECLARATION OF TRUST")) || 
       (pTmp1=strstr(acName1, " TRUST"))     ||
       (pTmp1=strstr(acName1, " TR ")) 
      )
   {
      pTmp = acName1;
      while (pTmp < pTmp1 && !isdigit(*(pTmp)))
         pTmp++;
      if (pTmp < pTmp1)
         *--pTmp = 0;
      else
         *pTmp1 = 0;
   } else if ((pTmp1=strstr(acName1, " LIFE EST")) ) 
       *pTmp1 = 0;

   // Do not parse if there is certain word in name
   if (strstr(acName1, " OF ")) 
       bKeep = true;;

   // Parse owner
   blankRem(acName1);
   if (!bKeep && strchr(acName1, ' '))
   {
      iRet = splitOwner(acName1, &myOwner, 3);
      // If name is not swapable, use Name1 for it
      if (!myOwner.acSwapName[0] )
         strcpy(myOwner.acSwapName, acName1);
   } else
      strcpy(myOwner.acSwapName, acSave1);

   memcpy(pOutbuf+OFF_NAME1, acSave1, strlen(acSave1));

   iTmp = strlen(myOwner.acSwapName);
   if (iTmp > SIZ_NAME_SWAP)
      iTmp = SIZ_NAME_SWAP;
   memcpy(pOutbuf+OFF_NAME_SWAP, myOwner.acSwapName, iTmp);
}

/********************************* Sbt_MergeMAdr *****************************
 *
 * Return 0 if successful, >0 if error
 *
 *****************************************************************************/

void Sbt_MergeMAdr(char *pOutbuf, char *pRollRec)
{
   char     acTmp[256], acAddr1[64], acAddr2[64], *pTmp;
   int      iTmp;
   ADR_REC  sMailAdr;
   SBT_ROLL *pRec = (SBT_ROLL *)pRollRec;

   // Clear old Mailing
   memset(pOutbuf+OFF_M_STRNUM, ' ', SIZ_M_ADDR);
   memset(pOutbuf+OFF_M_ADDR_D, ' ', SIZ_M_ADDRB_D);

   // Check for blank address
   if (memcmp(pRec->M_Addr1, "     ", 5))
   {
      memcpy(acAddr1, pRec->M_Addr1, RSIZ_MAIL_ADDR1);
      blankRem(acAddr1, RSIZ_MAIL_ADDR1);

      // Parsing mail address
      memset((void *)&sMailAdr, 0, sizeof(ADR_REC));
      parseMAdr1(&sMailAdr, acAddr1);

      if (sMailAdr.lStrNum > 0)
      {
         iTmp = sprintf(acTmp, "%d", sMailAdr.lStrNum);
         memcpy(pOutbuf+OFF_M_STRNUM, acTmp, iTmp);
         if (sMailAdr.strSub[0] > '0')
         {
            sprintf(acTmp, "%s  ", sMailAdr.strSub);
            memcpy(pOutbuf+OFF_M_STR_SUB, acTmp, SIZ_M_STR_SUB);
         }
      }
      memcpy(pOutbuf+OFF_M_DIR, sMailAdr.strDir, strlen(sMailAdr.strDir));
      memcpy(pOutbuf+OFF_M_STREET, sMailAdr.strName, strlen(sMailAdr.strName));
      memcpy(pOutbuf+OFF_M_SUFF, sMailAdr.strSfx, strlen(sMailAdr.strSfx));

      if (sMailAdr.Unit[0] > ' ')
         memcpy(pOutbuf+OFF_M_UNITNO, sMailAdr.Unit, strlen(sMailAdr.Unit));
      memcpy(pOutbuf+OFF_M_ADDR_D, acAddr1, strlen(acAddr1));
   }

   // City state
   if (pRec->M_City[0] > ' ')
   {
      memcpy(acAddr2, pRec->M_City, RSIZ_MAIL_CITY);
      blankRem(acAddr2, RSIZ_MAIL_CITY);
      if (pTmp = strchr(acAddr2, ','))
         *pTmp = ' ';

      iTmp = strlen(acAddr2);
      if (iTmp > SIZ_M_CITY) iTmp = SIZ_M_CITY;
      memcpy(pOutbuf+OFF_M_CITY, acAddr2, iTmp);

      if (pRec->M_State[0] >= 'A')
      {
         memcpy(pOutbuf+OFF_M_ST, pRec->M_State, SIZ_M_ST);
         sprintf(acAddr2, "%s, %.2s", acAddr2, pRec->M_State);
      }

      iTmp = atoin(pRec->M_Zip, SIZ_M_ZIP);
      if (iTmp > 500)
      {
         memcpy(pOutbuf+OFF_M_ZIP, pRec->M_Zip, SIZ_M_ZIP);
         iTmp = atoin(pRec->M_Zip4, SIZ_M_ZIP4);
         if (iTmp > 0)
         {
            memcpy(pOutbuf+OFF_M_ZIP4, pRec->M_Zip4, SIZ_M_ZIP4);
            sprintf(acAddr2, "%s %.5s-%.4s", acAddr2, pRec->M_Zip, pRec->M_Zip4);
         } else
            sprintf(acAddr2, "%s %.5s", acAddr2, pRec->M_Zip);
      } else
         sprintf(acAddr2, "%s, %s", sMailAdr.City, sMailAdr.State);

      blankRem(acAddr2);
      iTmp = strlen(acAddr2);
      if (iTmp > SIZ_M_CTY_ST_D) iTmp = SIZ_M_CTY_ST_D;
      memcpy(pOutbuf+OFF_M_CTY_ST_D, acAddr2, iTmp);
   }
}

/******************************** Sbt_MergeSAdr ******************************
 *
 * Notes:
 *
 * Return 0 if successful, >0 if error
 *
 *****************************************************************************/

int Sbt_MergeSAdr(char *pOutbuf, char *pRollRec)
{
   char  acAdr[64], acStrName[64], acSfx[8], acAddr1[64], acTmp[128];
   char  *pTmp, *apItems[16], acCity[32];
   int   iStrNo, iTmp, iCnt, iSfxCode, iIdx, iRet;
   bool  bDir, bRet, bAnd;
   SBT_ROLL *pRec = (SBT_ROLL *)pRollRec;
   ADR_REC  sAdrRec;

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "625350002", 9))
   //   iTmp = 0;
#endif

   // Initialize
   memset(&sAdrRec, 0, sizeof(ADR_REC));
   memcpy(acAdr, pRec->PropDesc, RSIZ_PROPDESC);
   acAdr[RSIZ_PROPDESC] = 0;
   acCity[0]=acStrName[0] = 0;

   iStrNo = atoi(acAdr);
   if (iStrNo > 0)
   {
      // Skip every thing within parenthesis
      if (pTmp=strchr(acAdr, '('))
      {
         while (*pTmp && *pTmp != ')')
            *pTmp++ = ' ';

         if (*pTmp == ')')
            *pTmp = ' ';
      }

      replChar(acAdr, '.', ' ');
      replChar(acAdr, ',', ' ');
      blankRem(acAdr);

      iCnt = ParseString(acAdr, ' ', 16, apItems);
      if (iCnt == 1)
      {
         // Not address, may be legal
         return 1;
      }

      // Reset index
      iIdx = 0;

      // Validate StrNum
      // 416-A ROSE AVE
      // 106-108-110-112 PEARCE LANE SJB
      if (pTmp = strchr(apItems[iIdx], '-'))
      {
         *pTmp++ = 0;
         // If more than one '-' found, ignore them
         if (!strchr(pTmp, '-'))
            strncpy(sAdrRec.strSub, pTmp, SIZ_M_STR_SUB);
      } else if (pTmp = strchr(apItems[iIdx], ','))
      {
         // Drop extra number 
         // 348,348 1/2,350 CARPENTERIA RD
         if (strchr(apItems[iIdx+1], ',') || strchr(apItems[iIdx+1], '/'))
            iIdx++;
         else if (isNumber(apItems[iIdx+1]))
            iIdx++;
      } else if (!isNumber(apItems[iIdx]))
      {
         // Move alpha into strSub
         pTmp = apItems[iIdx];
         while (*pTmp && isdigit(*pTmp))
            pTmp++;
         if (*pTmp)
            strncpy(sAdrRec.strSub, pTmp, SIZ_M_STR_SUB);
      } else if (*apItems[iIdx+1] == '&')
      {
         //81 & 81-A 4TH ST SJB
         pTmp = apItems[iIdx+1];
         if (!*(pTmp+1))
            iIdx += 2;
         else
            iIdx++;
      } else if (iCnt > 4 && 
                 strlen(apItems[iIdx+1]) == 1 && 
                 *apItems[iIdx+2] == '&' )
      {
         // 803 A & B MARENTES CT - Drop A & B
         if (strlen(apItems[iIdx+3]) == 1) 
            iIdx += 3;
         else if (isdigit(*(apItems[iIdx+3])) )
         {
            // 504 A & 1026-1028 WEST ST
            if (pTmp = strchr(apItems[iIdx+3], '-'))
               *pTmp = 0;
         }
      } else if (*apItems[iIdx+1] == '#')
      {
         if (*apItems[iIdx+2] >= 'A')
            strncpy(sAdrRec.Unit, apItems[++iIdx], SIZ_M_UNITNO);
         else 
         {  // Skip multi-unit
            iTmp = 3;
            while (iTmp+iIdx < iCnt)
            {
               if (*apItems[iIdx+iTmp] >= 'A')
                  break;

               iTmp++;
            }
            iIdx = iTmp-1;
         }

      } else if (*apItems[iIdx+1] != '&' && strchr(apItems[iIdx+1], '&'))
      {
         // Skip multi-unit
         // 1031 A,B,&C CAPUTO CT
         iIdx++;
      } else if (strchr(apItems[iIdx+1], ','))
      {
         // 1021 A, B, & C CAPUTO CT
         iTmp = 2;
         bAnd = false;
         while (iTmp+iIdx < iCnt)
         {
            if (*(apItems[iIdx+iTmp]) == '&') 
               bAnd = true;
            else if (!strchr(apItems[iIdx+iTmp], ',') )
            {
               if (bAnd)
                  iTmp++;
               break;
            }

            iTmp++;
         }
         iIdx = iTmp-1;
      } else if (*(apItems[iIdx+1]+1) == '-')
      {
         iIdx++;
         remChar(apItems[iIdx], '-');
         strncpy(sAdrRec.Unit, apItems[iIdx], SIZ_M_UNITNO);
      }

      // Copy StrNum
      iTmp = sprintf(sAdrRec.strNum, "%d", iStrNo);
      iIdx++;

      // Check for StrSub
      if (*(apItems[iIdx]) == '\"')
      {
         pTmp = apItems[iIdx];
         sAdrRec.strSub[0] = *++pTmp;
         iIdx++;
      }

      // Dir
      iTmp = 0;
      iSfxCode = 0;
      bRet = bDir = false;
      while (asDir[iTmp])
      {
         if (!strcmp(apItems[iIdx], asDir[iTmp]))
         {
            bDir = true;
            break;
         }
         iTmp++;
      }

      if (!bDir)
      {
         iTmp = 0;
         while (asDir1[iTmp])
         {
            if (!strcmp(apItems[iIdx], asDir1[iTmp]))
            {
               bDir = true;
               break;
            }
            iTmp++;
         }

         if (!bDir)
         {
            iTmp = 0;
            while (asDir2[iTmp])
            {
               if (!strcmp(apItems[iIdx], asDir2[iTmp]))
               {
                  bDir = true;
                  break;
               }
               iTmp++;
            }
         }
      }

      // Possible cases E ST or W RD
      // Check for suffix
      bRet = false;
      if (bDir && iCnt > 2)
      {
         // If next item is suffix, then this one might not be direction
         // ex: 123 S AVE B   --> S is direction and AVE B is street name
         //     321 N AVE     --> N is not a direction
         iSfxCode = GetSfxCodeX(apItems[iIdx+1], acSfx);
         if (iSfxCode && strcmp(apItems[iIdx+1], "HWY") &&
            (iCnt == iIdx+2 || !GetSfxDev(apItems[iIdx+2])) )
         {
            strcpy(acStrName, apItems[iIdx]);
            iTmp = sprintf(sAdrRec.SfxCode, "%d", iSfxCode);
            strcpy(sAdrRec.strSfx, acSfx);
            iIdx += 2;

            if (iCnt==iIdx)
               bRet = true;         // Done
         } else
         {
            // This is direction
            strcpy(sAdrRec.strDir, asDir[iTmp]);
            iIdx++;
            iSfxCode = 0;
         }
      }

      if (!bRet)
      {
         // Check for fraction
         if (!memcmp(apItems[iIdx], "1/2", 3))
         {
            strcpy(sAdrRec.strSub, "1/2");
            iIdx++;
         }

         acTmp[0] = 0;

         // Check for unit #
         if (apItems[iCnt-1][0] == '#' && strcmp(apItems[iCnt-2], "HWY"))
         {
            // NORTH SCHOOL ST #1
            // 1551 MEMORIAL DR  #1 THRU #4
            if (iCnt > 3 && !strcmp(apItems[iCnt-2], "THRU"))
            {
               sprintf(sAdrRec.Unit, "%s-%s", apItems[iCnt-3], apItems[iCnt-1]+1);
               iCnt -= 3;
            } else
            {
               strncpy(sAdrRec.Unit, apItems[iCnt-1], SIZ_M_UNITNO);
               iCnt--;

               // If last token is STE or SUITE, remove it.
               if (iCnt > 3 && 
                  (!strcmp(apItems[iCnt-1], "STE") || 
                   !strcmp(apItems[iCnt-1], "SUITE") ||
                   !strcmp(apItems[iCnt-1], "UNIT") ))
                  iCnt--;
            }
         } else if (iCnt > 3 && (!strcmp(apItems[iCnt-2], "STE") ||
                                 !strcmp(apItems[iCnt-2], "SUITE") ||
                                 !strcmp(apItems[iCnt-2], "APT")))
         {
            strncpy(sAdrRec.Unit, apItems[iCnt-1], SIZ_M_UNITNO);
            iCnt -= 2;
         }

         if (!iSfxCode)
         {
            pTmp = NULL;
            while (iIdx < iCnt)
            {
               if (pTmp = strchr(apItems[iIdx], '-'))
                  *pTmp++ = 0;

               iTmp = 0;
               if ((iSfxCode = GetSfxCodeX(apItems[iIdx], acSfx)) &&
                   !(iCnt > iIdx+1 && (iTmp = GetSfxCodeX(apItems[iIdx+1], acTmp))) &&
                   !(!memcmp(apItems[iIdx], "HWY", 3) && apItems[iIdx+1] && isdigit(*apItems[iIdx+1]) )
                  )
               {
                  if (!memcmp(apItems[iIdx-1], "WAY", 3) && !memcmp(apItems[iIdx], "CR", 2) )
                     iSfxCode = GetSfxCodeX("CIR", acSfx);

                  // Format sfxCode
                  sprintf(sAdrRec.SfxCode, "%d", iSfxCode);
                  strcpy(sAdrRec.strSfx, acSfx);
                  iIdx++;
                  break;
               } else
               {
                  if (!memcmp(apItems[iIdx], "LN", 2))
                     strcat(acStrName, "LANE");
                  else
                     strcat(acStrName, apItems[iIdx]);

                  iIdx++;
                  if (iIdx < iCnt)
                     strcat(acStrName, " ");

                  if (pTmp)
                     break;
               }
            }

            if (pTmp)
            {
               strcpy(acCity, pTmp);
               strcat(acCity, " ");
            }

            // Check for multiple address in strName
            if (pTmp = strchr(acStrName, '&'))
               *pTmp = 0;
         }

         // Left over could be city name, unit#, or notes
         while (iIdx < iCnt && *apItems[iIdx] != '&')
         {
            if (!strcmp(apItems[iIdx], "CA"))
               break;
            strcat(acCity, apItems[iIdx++]);
            strcat(acCity, " ");
         }
      }

      // If no strName, put it in Legal
      if (acStrName[0])
      {
         // Remove comma in street name
         replCharEx(acStrName, ',', ' ');
         blankRem(acStrName);
         strncpy(sAdrRec.strName, acStrName, SIZ_M_STREET);

         // Check City
         if (acCity[0] && (pTmp = strchr(acCity, '-')))
            *pTmp++ = 0;
         if (!memcmp(acCity, "SP ", 3) && isdigit(acCity[3]))
         {
            strncpy(sAdrRec.Unit, acCity, SIZ_M_UNITNO);
            acCity[0] = 0;
            if (pTmp && *pTmp)
               strcpy(acCity, pTmp);
         }

         // StrNum
         memcpy(pOutbuf+OFF_S_STRNUM, sAdrRec.strNum, strlen(sAdrRec.strNum));

         // StrSub
         if (sAdrRec.strSub[0])
            memcpy(pOutbuf+OFF_S_STR_SUB, sAdrRec.strSub, strlen(sAdrRec.strSub));

         // StrDir
         if (sAdrRec.strDir[0])
            memcpy(pOutbuf+OFF_S_DIR, sAdrRec.strDir, strlen(sAdrRec.strDir));

         // StrName
         memcpy(pOutbuf+OFF_S_STREET, sAdrRec.strName, strlen(sAdrRec.strName));

         // StrSfx
         if (sAdrRec.SfxCode[0])
            memcpy(pOutbuf+OFF_S_SUFF, sAdrRec.SfxCode, strlen(sAdrRec.SfxCode));
      
         // StrUnit
         if (sAdrRec.Unit[0])
            memcpy(pOutbuf+OFF_S_UNITNO, sAdrRec.Unit, strlen(sAdrRec.Unit));  
         
         // State
         memcpy(pOutbuf+OFF_S_ST, "CA", 2);

         sprintf(acAddr1, "%d%s %s %s %s %s", iStrNo, sAdrRec.strSub, sAdrRec.strDir, sAdrRec.strName, sAdrRec.strSfx, sAdrRec.Unit);
         blankRem(acAddr1);
         memcpy(pOutbuf+OFF_S_ADDR_D, acAddr1, strlen(acAddr1));

         // City name
         if (acCity[0] > '0')
         {
            City2Code(acCity, acTmp, pOutbuf);
            if (acTmp[0] > '0')
            {
               memcpy(pOutbuf+OFF_S_CITY, acTmp, SIZ_S_CITY);
               iTmp = atoi(acTmp);
               if (pTmp = GetCityName(iTmp))
               {
                  iTmp = sprintf(acAddr1, "%s CA", pTmp);
                  memcpy(pOutbuf+OFF_S_CTY_ST_D, acAddr1, iTmp);
               }
            }
         }

         iRet = 0;
      } else
         iRet = 1;
   } else
      iRet = 1;

   return iRet;
}

/******************************** Sbt_CreatePQ4Rec ***************************
 *
 *
 *****************************************************************************/

void Sbt_CreatePQ4Rec(char *pOutbuf, char *pRollRec)
{
   SBT_ROLL *pRec;
   char     *pTmp, acTmp[256], acTmp1[256];
   long     lTmp;
   int      iRet, iTmp;

   // Replace tab char with 0
   pTmp = pRollRec;

   pRec = (SBT_ROLL *)pRollRec;
   
   // Clear output buffer
   memset(pOutbuf, ' ', iRecLen);

   // Start copying data
   memcpy(pOutbuf, pRec->Apn, RSIZ_APN);
   memcpy(pOutbuf+OFF_CO_NUM, "35SBTA", 6);

   // Format APN
   iRet = formatApn(pRec->Apn, acTmp, &myCounty);
   memcpy(pOutbuf+OFF_APN_D, acTmp, iRet);

   // Create MapLink
   iRet = formatMapLink(pRec->Apn, acTmp, &myCounty);
   memcpy(pOutbuf+OFF_MAPLINK, acTmp, iRet);

   // Create index map link
   if (getIndexPage(acTmp, acTmp1, &myCounty))
		memcpy(pOutbuf+OFF_IMAPLINK, acTmp1, iRet);

   // Year assessed
   memcpy(pOutbuf+OFF_YR_ASSD, myCounty.acYearAssd, 4);

   // HO Exempt
   lTmp = atoin(pRec->Exemp, RSIZ_EXEMP);
   if (lTmp == 7000)
      *(pOutbuf+OFF_HO_FL) = '1';      // 'Y'
   else
      *(pOutbuf+OFF_HO_FL) = '2';      // 'N'

   // Exempt total
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_EXE_TOTAL, lTmp);
      memcpy(pOutbuf+OFF_EXE_TOTAL, acTmp, SIZ_EXE_TOTAL);
   }

   // Land
   long lLand = atoin(pRec->Land, RSIZ_LAND);
   if (lLand > 0)
   {
      sprintf(acTmp, "%*d", SIZ_LAND, lLand);
      memcpy(pOutbuf+OFF_LAND, acTmp, SIZ_LAND);
   }

   // Improve
   long lImpr = atoin(pRec->Impr, RSIZ_IMPR);
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*d", SIZ_IMPR, lImpr);
      memcpy(pOutbuf+OFF_IMPR, acTmp, SIZ_IMPR);
   }

   // Other value
   long lOther = atoin(pRec->Pers_Prop, RSIZ_PERS_PROP);
   lOther += atoin(pRec->Trees_Vines, RSIZ_TREES_VINES);
   if (lOther > 0)
   {
      sprintf(acTmp, "%*d", SIZ_OTHER, lOther);
      memcpy(pOutbuf+OFF_OTHER, acTmp, SIZ_OTHER);
   }

   // Gross total
   lTmp = lLand+lImpr+lOther;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_GROSS, lTmp);
      memcpy(pOutbuf+OFF_GROSS, acTmp, SIZ_GROSS);
   }

   // Ratio
   if (lImpr > 0)
   {
      double dTmp = (LONGLONG)lImpr*100/(lLand+lImpr)+0.5;
      sprintf(acTmp, "%*u", SIZ_RATIO, (long)dTmp);

      memcpy(pOutbuf+OFF_RATIO, acTmp, SIZ_RATIO);
   }

   // UseCode
   if (pRec->UseCode[0] > ' ')
   {
      memcpy(pOutbuf+OFF_USE_CO, pRec->UseCode, RSIZ_USECODE);
      memset(pOutbuf+OFF_USE_STD, ' ', SIZ_USE_STD);
      updateStdUse(pOutbuf+OFF_USE_STD, pRec->UseCode, RSIZ_USECODE);
   }

   // TRA
   memcpy(pOutbuf+OFF_TRA, pRec->Tra, RSIZ_TRA);

   // YrDelq

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "011150120", 9))
   //   iTmp = 0;
#endif
   // Acres V99
   double dLotAcres = atofn(pRec->Acres, RSIZ_ACRES);
   if (dLotAcres > 0.001)
   {
      lTmp = dLotAcres * 1000;
      sprintf(acTmp, "%*d", SIZ_LOT_ACRES, lTmp);
      memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);

      lTmp = (dLotAcres*SQFT_PER_ACRE);
      sprintf(acTmp, "%*d", SIZ_LOT_SQFT, lTmp);
      memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);
   } else if (isdigit(pRec->Lot_Dimension[0]))
   {
      int   iWidth, iHeight;

      // Get dimension
      iWidth = atoin(pRec->Lot_Dimension, 3);
      iHeight = atoin((char *)&pRec->Lot_Dimension[3], 3);
      lTmp = iWidth*iHeight;
      if (lTmp > 0)
      {
         sprintf(acTmp, "%*d", SIZ_LOT_SQFT, lTmp);
         memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);

         dLotAcres = (lTmp * ACRES_FACTOR)/SQFT_PER_ACRE;
         sprintf(acTmp, "%*d", SIZ_LOT_ACRES, (long)dLotAcres);
         memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);
      }
   }

   // Zoning
   if (pRec->Zone[0] > ' ')
   {
      memcpy(acTmp, pRec->Zone, RSIZ_ZONE);
      acTmp[RSIZ_ZONE] = 0;
      memcpy(pOutbuf+OFF_ZONE, strupr(acTmp), RSIZ_ZONE);
   }

   // Transfer
   if (pRec->DocNum[0] > ' ')
      memcpy(pOutbuf+OFF_TRANSFER_DOC, pRec->DocNum, RSIZ_REC_NUM);

   // Owner
   Sbt_MergeOwner(pOutbuf, pRollRec);

   // Mailing
   Sbt_MergeMAdr(pOutbuf, pRollRec);

   // Situs or Legal?
   memcpy(acTmp, pRollRec+ROFF_PROPDESC, RSIZ_PROPDESC);
   blankRem(acTmp, RSIZ_PROPDESC);
   if (pTmp = strstr(acTmp, " VISTA PARK HILL CT"))
   {  // special case                      
      memcpy(pOutbuf+OFF_S_ADDR_D, acTmp, strlen(acTmp));

      *pTmp = 0;
      memcpy(pOutbuf+OFF_S_STRNUM, acTmp, strlen(acTmp));
      memcpy(pOutbuf+OFF_S_STREET, "VISTA PARK HILL", 15);
      memcpy(pOutbuf+OFF_S_SUFF, "14", 2);
      *(pOutbuf+OFF_S_CITY) = '7';
      memcpy(pOutbuf+OFF_S_CTY_ST_D, "HOLLISTER CA", 12);
   } else if (iRet = Sbt_MergeSAdr(pOutbuf, pRollRec))
   {
      memcpy(pOutbuf+OFF_LEGAL, pRec->PropDesc, SIZ_LEGAL);
   } else if (*(pOutbuf+OFF_S_CITY) == ' ')
   {
      char  acCity[32];

      // Match mailing
      if (!memcmp(pOutbuf+OFF_S_STRNUM, pOutbuf+OFF_M_STRNUM, SIZ_S_STRNUM) &&
          !memcmp(pOutbuf+OFF_S_STREET, pOutbuf+OFF_M_STREET, SIZ_S_STREET) &&
          *(pOutbuf+OFF_M_CITY) > ' ' )
      {
         memcpy(acCity, pRec->M_City, RSIZ_MAIL_CITY);
         blankRem(acCity, RSIZ_MAIL_CITY);
         City2Code(acCity, acTmp, pOutbuf);
         if (acTmp[0] > '0')
         {
            memcpy(pOutbuf+OFF_S_CITY, acTmp, SIZ_S_CITY);
            strcat(acCity, ", CA");
            memcpy(pOutbuf+OFF_S_CTY_ST_D, acCity, strlen(acCity));
         }
      } else 
      {  // Check TRA
         iTmp = XrefCode2Idx(&asTRA[0], pRec->Tra, iNumTRA);
         if (iTmp > 0)
         {
            iRet = sprintf(acTmp, "%d", iTmp);
            memcpy(pOutbuf+OFF_S_CITY, acTmp, iRet);
            if (pTmp = GetCityName(iTmp))
            {
               iRet = sprintf(acTmp, "%s CA", pTmp);
               memcpy(pOutbuf+OFF_S_CTY_ST_D, acTmp, iRet);
            }
         }
      }
   }
}

/********************************* Sbt_MergeLien *****************************
 *
 * Return 0 if successful, >0 if error
 *
 *****************************************************************************/

void Sbt_MergeLien(char *pOutbuf, char *pLienRec)
{
   LIENEXTR *pLien = (LIENEXTR *)pLienRec;

   // H/O exempt
   *(pOutbuf+OFF_HO_FL) = pLien->acHO[0];
   memcpy(pOutbuf+OFF_EXE_TOTAL, pLien->acExAmt, SIZ_EXE_TOTAL);

   // Land
   memcpy(pOutbuf+OFF_LAND, pLien->acLand, SIZ_LAND);

   // Improve
   memcpy(pOutbuf+OFF_IMPR, pLien->acImpr, SIZ_IMPR);

   // Others
   memcpy(pOutbuf+OFF_OTHER, pLien->acOther, SIZ_OTHER);

   // Gross
   memcpy(pOutbuf+OFF_GROSS, pLien->acGross, SIZ_GROSS);

   // Ratio
   memcpy(pOutbuf+OFF_RATIO, pLien->acRatio, SIZ_RATIO);

}

/********************************* Sbt_MergeLien ******************************
 *
 *
 ******************************************************************************/

int Sbt_MergeLien(int iSkip)
{
   char     *pTmp, acBuf[MAX_RECSIZE], acLienRec[MAX_RECSIZE];
   char     cFileCnt=1;
   char     acRawFile[_MAX_PATH], acOutFile[_MAX_PATH], acLienExtr[_MAX_PATH];

   HANDLE   fhIn, fhOut;

   int      iApnLen, iTmp, iLienMatch=0, iLienDrop=0, iLienMiss=0;
   DWORD    nBytesRead;
   DWORD    nBytesWritten;
   BOOL     bRet, bEof;
   long     lRet=0, lCnt=0;

   iApnLen = myCounty.iApnLen;
   sprintf(acRawFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "R01");
   sprintf(acOutFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "M01");

   // Rename files for processing
   if (_access(acRawFile, 0))
   {
      if (!_access(acOutFile, 0))
         rename(acOutFile, acRawFile);
      else
      {
         LogMsg("Missing input file %s.  Please recheck!", acRawFile);
         return 1;
      }
   }

   // Open Lien extract file
   sprintf(acLienExtr, acLienTmpl, myCounty.acCntyCode, myCounty.acCntyCode);
   LogMsg("Open Lien extract file %s", acLienExtr);
   fdLien = fopen(acLienExtr, "r");
   if (fdLien == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", acLienExtr);
      return 2;
   }

   // Open Input file
   LogMsg("Open input file %s", acRawFile);
   fhIn = CreateFile(acRawFile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhIn == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening raw file: %s\n", acRawFile);
      return 3;
   }

   // Open Output file
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error creating output file: %s\n", acOutFile);
      return 4;
   }

   // Get first lien rec
   pTmp = fgets((char *)&acLienRec[0], 1024, fdLien);
   bEof = (pTmp ? false:true);

   // Copy skip record
   memset(acBuf, ' ', iRecLen);
   while (iSkip-- > 0)
   {
      ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);
      WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
   }

   iNoMatch=iBadCity=iBadSuffix=0;

   // Merge loop
   while (!bEof)
   {
      bRet = ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);

      // Check for EOF
      if (!bRet)
      {
         LogMsg("Error reading input file %s (%f)", acRawFile, GetLastError());
         break;
      }

      // EOF ?
      if (!nBytesRead)
         break;

NextLienRec:
      iTmp = memcmp(acBuf, acLienRec, iApnLen);
      if (!iTmp)
      {
         iLienMatch++;
         // Merge roll data
         Sbt_MergeLien(acBuf, acLienRec);

         // Read next roll record
         pTmp = fgets(acLienRec, 1024, fdLien);

         if (!pTmp)
            bEof = true;    // Signal to stop
      } else if (iTmp > 0)       
      {  // Roll not match, drop lien record?
         iLienDrop++;
         // Get next lien record
         pTmp = fgets(acLienRec, 1024, fdLien);

         if (!pTmp)
            bEof = true;    // Signal to stop
         else
            goto NextLienRec;
      } else
      {
         iLienMiss++;
         LogMsg0("*** New rec? %.10s", acBuf);
      }

      WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);

      if (!bRet)
      {
         LogMsg("***** Error occurs: %d\n", GetLastError());
         lRet = WRITE_ERR;
         break;
      }
   }

   // Close files
   if (fdLien)
      fclose(fdLien);
   if (fhOut)
      CloseHandle(fhOut);
   if (fhIn)
      CloseHandle(fhIn);

   // Rename output file
   iTmp = remove(acRawFile);
   if (!iTmp)
   {
      iTmp = rename(acOutFile, acRawFile);
      if (iTmp < 0)
      {
         LogMsgD("***** Error renaming file %s to %s", acOutFile, acRawFile);
         iTmp = -1;
      }
   } else
   {
      LogMsgD("***** Error removing file %s", acRawFile);
      iTmp = -1;
   }

   LogMsg("Total output records:       %u", lCnt);
   LogMsg("Total records matched:      %u", iLienMatch);
   LogMsg("Total records missed:       %u", iLienMiss);
   LogMsg("Total lien records dropped: %u", iLienDrop);

   printf("\nTotal output records: %u", lCnt);

   lRecCnt = lCnt;
   return iTmp;
}

/******************************** Sbt_CreateRoll ****************************
 *
 * Merge Assess and Nontax file into SSBT.S01 then sort by APN into SSBT.R01.
 *
 ****************************************************************************/

int Sbt_CreateRoll(int iFirstRec /* 1=create header rec */)
{
   char     *pTmp, acBuf[MAX_RECSIZE], acRec[MAX_RECSIZE];
   char     acTmpFile[_MAX_PATH], acOutFile[_MAX_PATH];

   HANDLE   fhOut;
   FILE     *fdRoll;

   int      iRet;
   DWORD    nBytesWritten;
   BOOL     bRet;
   long     lRet, lCnt=0;

   // Init variables
   iNoMatch=iBadCity=iBadSuffix=0;

   // Open roll file
   LogMsg("Open Roll file %s", acRollFile);
   fdRoll = fopen(acRollFile, "r");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", acRollFile);
      return 2;
   }

   // Open Output file
   sprintf(acOutFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "R01");
   sprintf(acTmpFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "TMP");
   LogMsg("Open output file %s", acTmpFile);
   fhOut = CreateFile(acTmpFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error creating output file: %s\n", acTmpFile);
      return 4;
   }

   // Output first header record
   if (iFirstRec > 0)
   {
      memset(acBuf, '9', iRecLen);
      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
   }

   // Merge loop
   while (!feof(fdRoll))
   {
      // Get roll rec
      do {
         pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdRoll);
      } while (pTmp && !memcmp((char *)&acRec[ROFF_LIEN_OWNER], "RUN\"COMENU", 10));

      if (!pTmp)
         break;

      // Replace TAB character with space
      replCharEx(acRec, 31, ' ');

      // Create new R01 record
      Sbt_CreatePQ4Rec(acBuf, acRec);

#ifdef _DEBUG
      iRet = replCharEx(acBuf, 31, ' ', iRecLen);
      if (iRet)
         LogMsg("***** Error: null is found at %d on %.*s", iRet, iApnLen, acBuf);
#endif

      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
      if (!bRet)
      {
         LogMsg("Error writing to output file at record %d\n", lCnt);
         lRet = WRITE_ERR;
         break;
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdRoll)
      fclose(fdRoll);

   // Extract nontax file
   if (!_access(NonTaxFile, 0))
   {
      LogMsg("Open Roll file %s", NonTaxFile);
      fdRoll = fopen(NonTaxFile, "r");
      if (fdRoll == NULL)
         LogMsg("***** Error opening NonTax file: %s\n", NonTaxFile);
      else
      {
         // Merge loop
         while (!feof(fdRoll))
         {
            // Get roll rec
            pTmp = fgets(acRec, MAX_RECSIZE, fdRoll);

            if (!pTmp)
               break;

            // Replace TAB character with space
            replCharEx(acRec, 31, ' ');

            // Create new R01 record
            Sbt_CreatePQ4Rec(acBuf, acRec);
            acBuf[OFF_TAX_CODE] = 'N';

            bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
            if (!bRet)
            {
               LogMsg("Error writing to output file at record %d\n", lCnt);
               lRet = WRITE_ERR;
               break;
            }

            if (!(++lCnt % 1000))
               printf("\r%u", lCnt);

            if (!bRet)
            {
               LogMsg("***** Error writing to output file at record %d\n", lCnt);
               break;
            }
         }

         fclose(fdRoll);
      }
   }

   if (fhOut)
      CloseHandle(fhOut);

   // Sort output file
   LogMsg("Sort output file %s to %s", acTmpFile, acOutFile);
   sprintf(acRec, "S(1,%d,C,A) F(FIX,%d) B(%d,R)", RSIZ_APN, iRecLen, iRecLen);
   lRet = sortFile(acTmpFile, acOutFile, acRec);
   if (lRet <= 0)
   {
      LogMsg("***** ERROR sorting roll output file %s to %s [%s]", acTmpFile, acOutFile, acRec);
      iRet = -1;
   } else
   {
      iRet = 0;
      remove(acTmpFile);
   }
   LogMsg("Total output records:       %u", lCnt);
   LogMsg("Total bad-city records:     %u", iBadCity);
   LogMsg("Total bad-suffix records:   %u\n", iBadSuffix);

   LogMsg("Last recording date:        %u", lLastRecDate);
   printf("\nTotal output records: %u", lCnt);

   lRecCnt = lCnt;
   return iRet;
}

/******************************* Sbt_CreateLienRec ***************************
 *
 * Return 0 if successful, >0 if error
 *
 *****************************************************************************/

int Sbt_CreateLienRec(char *pOutbuf, char *pRollRec)
{
   char     acTmp[256];
   long     lTmp, lExe;
   int      iTmp;
   LIENEXTR *pLienRec;
   SBT_ROLL *pRec;

   // Inititalize
   pRec = (SBT_ROLL *)pRollRec;
   pLienRec = (LIENEXTR *)pOutbuf;

   // Clear output buffer
   memset(pOutbuf, ' ', sizeof(LIENEXTR));

   // Preset for MB format
   memcpy(&pLienRec->acApn[10], "00", 2);
   pLienRec->acTRA[0] = '0';

   // Start copying data
   memcpy(pLienRec->acApn, pRec->Apn, RSIZ_APN);
   memcpy(&pLienRec->acTRA[1], pRec->Tra, RSIZ_TRA);

   // Year assessed
   memcpy(pLienRec->acYear, myCounty.acYearAssd, 4);

   // Land
   long lLand = atoin(pRec->Land, RSIZ_LAND);
   if (lLand > 0)
   {
      iTmp = sprintf(acTmp, "%*d", sizeof(pLienRec->acLand), lLand);
      memcpy(pLienRec->acLand, acTmp, iTmp);
   }

   // Improve - Ratio
   long lImpr = atoin(pRec->Impr, RSIZ_IMPR);
   if (lImpr > 0)
   {
      iTmp = sprintf(acTmp, "%*d", sizeof(pLienRec->acImpr), lImpr);
      memcpy(pLienRec->acImpr, acTmp, iTmp);

      iTmp = sprintf(acTmp, "%*d", sizeof(pLienRec->acRatio), (LONGLONG)lImpr*100/(lLand+lImpr));
      memcpy(pLienRec->acRatio, acTmp, iTmp);
   }

   // PP value
   long lPers = atoin(pRec->Pers_Prop, RSIZ_PERS_PROP);
   if (lPers > 0)
   {
      iTmp = sprintf(acTmp, "%*d", sizeof(pLienRec->acPP_Val), lPers);
      memcpy(pLienRec->acPP_Val, acTmp, iTmp);
   }

   // Tree/Vines
   long lTree = atoin(pRec->Trees_Vines, RSIZ_TREES_VINES);
   if (lTree > 0)
   {
      iTmp = sprintf(acTmp, "%*d", sizeof(pLienRec->acME_Val), lTree);
      memcpy(pLienRec->acME_Val, acTmp, iTmp);
   }

   // Total other
   long lOther = lPers + lTree;
   if (lOther > 0)
   {
      iTmp = sprintf(acTmp, "%*d", sizeof(pLienRec->acGross), lOther);
      memcpy(pLienRec->acOther, acTmp, iTmp);
   }

   // Gross
   lTmp = lOther + lLand + lImpr;
   if (lTmp > 0)
   {
      iTmp = sprintf(acTmp, "%*d", sizeof(pLienRec->acGross), lTmp);
      memcpy(pLienRec->acGross, acTmp, iTmp);
   }

   // Total Exe
   lExe = atoin(pRec->Exemp, RSIZ_EXEMP);
   if (lExe > 0)
   {
      iTmp = sprintf(acTmp, "%*d", sizeof(pLienRec->acExAmt), lExe);
      memcpy(pLienRec->acExAmt, acTmp, iTmp);
      // HO Exempt
      if (lExe == 7000)
         pLienRec->acHO[0] = '1';      // 'Y'
      else
         pLienRec->acHO[0] = '2';      // 'N'
   } else
      pLienRec->acHO[0] = '2';         // 'N'

   pLienRec->LF[0] = '\n';
   pLienRec->LF[1] = 0;
   return 0;
}

/********************************* Sbt_ExtrLien *****************************
 *
 *
 ****************************************************************************/

int Sbt_ExtrLien()
{
   char     *pTmp, acBuf[MAX_RECSIZE], acRollRec[MAX_RECSIZE];
   char     acOutFile[_MAX_PATH], acLienExtr[_MAX_PATH];
   long     lCnt=0;

   // Open roll file
   LogMsg("Open Roll file %s", acRollFile);
   fdRoll = fopen(acRollFile, "r");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", acRollFile);
      return 2;
   }

   // Open Output file
   sprintf(acOutFile, "%s\\%s\\%s_Lien.ext", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   LogMsg("Open output file %s", acOutFile);
   fdLien = fopen(acOutFile, "w");
   if (fdLien == NULL)
   {
      LogMsg("***** Error opening Lien extract file: %s\n", acOutFile);
      return 4;
   }

   // Merge loop
   while (!feof(fdRoll))
   {
      // Get roll rec
      pTmp = fgets(acRollRec, MAX_RECSIZE, fdRoll);
      if (!pTmp)
         break;

      // Replace TAB character with space
      replCharEx(acRollRec, 31, ' ');

      // Create new lien record
      Sbt_CreateLienRec(acBuf, acRollRec);   

      fputs(acBuf, fdLien);
      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdRoll)
      fclose(fdRoll);

   // Extract nontax file
   if (!_access(NonTaxFile, 0))
   {
      LogMsg("Open Roll file %s", NonTaxFile);
      fdRoll = fopen(NonTaxFile, "r");

      // Merge loop
      while (fdRoll && !feof(fdRoll))
      {
         // Get roll rec
         pTmp = fgets(acRollRec, MAX_RECSIZE, fdRoll);
         if (!pTmp)
            break;

         replCharEx(acRollRec, 31, ' ');

         // Create new lien record
         Sbt_CreateLienRec(acBuf, acRollRec);   

         fputs(acBuf, fdLien);
         if (!(++lCnt % 1000))
            printf("\r%u", lCnt);
      }

      if (fdRoll == NULL)
      {
         LogMsg("***** Error opening NonTax file: %s\n", NonTaxFile);
         return 2;
      } else
         fclose(fdRoll);
   }

   if (fdLien)
      fclose(fdLien);
   LogMsg("Total records output:     %d\n", lCnt);

   // Sort output file
   sprintf(acLienExtr, acLienTmpl, myCounty.acCntyCode, myCounty.acCntyCode);
   sprintf(acBuf, "S(1,%d,C,A) F(TXT) DUPO(1,%d)", RSIZ_APN, RSIZ_APN);
   lCnt = sortFile(acOutFile, acLienExtr, acBuf);
   if (lCnt <= 0)
   {
      LogMsg("***** ERROR sorting lien extract file %s to %s", acOutFile, acLienExtr);
      return -1;
   }

   LogMsg("Total sorted records:     %u", lCnt);
   printf("\nTotal output records: %u\n", lCnt);

   lRecCnt = lCnt;
   return 0;
}

/*********************************** loadSbt ********************************
 *
 *
 ****************************************************************************/

int loadSbt(int iSkip)
{
   int   iRet;

   iApnLen = myCounty.iApnLen;

   // Char file
   GetPrivateProfileString(myCounty.acCntyCode, "NonTaxFile", "", NonTaxFile, _MAX_PATH, acIniFile);

   // Extract lien data
   if (iLoadFlag & (LOAD_LIEN|EXTR_LIEN))
      iRet = Sbt_ExtrLien();

   if (iLoadFlag & (LOAD_LIEN|LOAD_UPDT))
   {
      // Create roll file .R01
      LogMsg("Create %s roll file", myCounty.acCntyCode);
      iRet = Sbt_CreateRoll(iSkip);

      if (!iRet && (iLoadFlag & LOAD_UPDT))
      {
         LogMsg("Merge Lien data into roll file");
         iRet = Sbt_MergeLien(iSkip);
         if (!iRet)
            bCreateUpd = true;
      }
   }
   
   return iRet;
}