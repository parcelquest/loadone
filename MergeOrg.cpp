/**************************************************************************
 *
 * Input files:
 *    - ORG_ROLL.TXT          (roll file, delimited file)
 *    - GGExtract*.zip        (GrGr file, contains Deed.txt and Document.txt)
 *
 * Following are tasks to be done here:
 *    - Load GrGr data by merging Deed.txt and Document.txt into ORG_GRGR.DAT
 *      using command -G
 *    - Merge GrGr data into roll file use -M
 *    - Normal update: LoadOne -CORG -G
 *    - Load Lien: LoadOne -O -CORG -L -G
 *
 * Use -G      to load GrGr data
 *     -Mg     to merge GrGr data to R01 file
 *     -Ma     to merge CHAR data
 *     -Mr     to merge GIS acreage data
 *     -M1     to merge situs addr
 *     -L      to load LDR file
 *     -U -G   to run monthly update
 *     -Uu     special load to update standard usecode on R01
 *     -X1     to extract situs address from O01 file when run LDR
 *     -Xa     to load CHAR file
 *     -E      to send email if error occurs
 *
 * Notes:
 *    1) Due to the problem that county couldn't send us the same name every time.
 *       we have to process ORG manually whenever data arrived.
 *    2) GRGR record contains mailing addr.  We can wrap adr1 from Tax System and
 *       match with this one.  If matched, use it for situs.
 *
 * 07/25/2006           Fix for 2006 LDR
 * 01/11/2007           Fix bug that did not set iLoadFlag correctly causing BUILDCDA fail to start.
 * 07/16/2007 1.4.19    Merge grgr automatically when load LDR.
 * 12/07/2007 1.4.33    Fix mail addr update problem.  We should only update mail addr
 *                      if sale date is newer than lien date.
 * 03/18/2008 1.5.7     Use local table to update usecode.  Write Org_UpdateStdUse()
 *                      to manually translate county usecode to std usecode.
 * 03/24/2008 1.5.7.2   Format IMAPLINK.  Adding -Uu option to update std use.
 * 03/28/2008 1.5.8     Add Org_UpdateCondo() to generate Condo & Timeshare maplink.
 * 06/04/2008 1.6.2     Rename Org_CreateGrGrFile() to Org_LoadGrGr()
 * 06/10/2008 1.6.4     Only update owner name if recdate > liendate
 * 07/29/2008 8.0.5     Change defined offset for Condo list
 * 11/21/2008 8.4.6     Modify Org_MergeGrGr() not to rename S01 to SAV if it is used
 *                      as input to merge GrGr to R01 file.
 * 01/17/2009 8.5.6     Fix TRA
 * 01/28/2009 8.5.9     Send email when loading failed.  Modify Org_UpdateCondo() to
 *                      use R01 for input instead of S01 file.
 * 04/06/2009 8.7.2     Correct display of iMatched & iNotMatched in Org_LoadGrGr().
 * 06/19/2009 8.8.5     Modify Org_MergeRoll() and add Org_ExtrLien() to populate Other Impr,  
 *                      PP_Val, and Full Exe flag.  Wait for Reaason code to populate Prop8 flag.
 * 07/22/2009 9.1.3     Fix problem that roll file may contain null char and double quote.
 * 08/19/2009 9.1.6     Fix Org_ParseMAdr() to handle records with SUITE on the second line.
 *                      Update CONDO layout & sort condo file bfore processing.
 * 08/21/2009 9.1.6.1   Fine tune Org_ParseMAdr().
 * 02/17/2010 9.4.2     Merge long LEGAL into LEGAL1.
 * 04/26/2010 9.5.6     Fix Org_FormatGrGrOwner() making sure owner names are in upper case.
 * 07/29/2010 10.1.7    Add Org_UpdExeVal() to process amended roll to correct exemption value.
 *                      Modify Org_Load_LDR() and Org_ExtrLien() to work with Org_UpdExeVal.
 *                      Modify Org_MergeTimeShare() and Org_MergeCondo() to get better APN
 *                      match by removing '-' before compare.
 * 08/10/2010 10.1.10   Modify Org_MergeRoll() to add EXE_CD to R01 record.
 * 08/22/2010 10.1.13   Add option to INI file "UpdateCondo=Y" to force update condo file after LDR.
 *                      Force EXE_TOTAL to be <= GROSS.
 * 05/24/2011 10.5.13   Fix bad character in Org_MergeGrGrMailing(), Org_ParseMAdr(), 
 *                      Org_MergeAdr(), and Org_MergeRoll().
 * 09/02/2011 11.2.10   Modify Org_UpdateCondo() to support new condo file layout.
 * 01/08/2012 11.6.2    Fix bug in Org_MergeCondo() since APN range in Condo list might spread
 *                      to multiple pages.
 * 03/16/2012 11.8.11   Remove both space and hyphen from GRGR's APN in Org_CreateGrGrRec().
 * 05/10/2012 11.9.5    Fix bug in GrGr processing to pickup fields correctly since ORG may
 *                      send data with various layout.  Reload GRGR from 2006 to get accurate info.
 * 05/18/2012 11.9.6    Fix bug in Org_FormatGrGrOwner() which causes buffer overrun.
 * 06/04/2012 11.9.9    Create GrGr import file if load successful.
 * 10/14/2012 12.3.0    Remove import GRGR code that didn't work correctly.  Let _tmain()
 *                      do it globally by using -Gi option and set ImportGrgr=Y in LOADONE.INI file
 * 11/12/2012 12.3.4    Reset DOCNUM before copy new one over sinve DOCNUM from the roll and 
 *                      from GrGr are not the same length.
 * 11/19/2012 12.3.5    Add Org_ChkCondoList() to check for misalignment of condo sequence.
 * 07/23/2013 13.1.6    Modify Org_ParseMAdr() to fix mailing addr1. Replace known bad chars in 
 *                      CAREOF, LEGAL & OWNER.
 * 08/12/2013 13.4.10   Remove NAME_TYPE, update CARE_OF, fix GrGr duplicate owner name problem.
 * 10/08/2013 13.10.4.2 Modify Org_MergeOwner() and Org_MergeGrGrRec() to update Vesting and Etal flag.
 * 06/30/2014 14.0.1    Fix missing mail adr in Org_ParseMAdr()
 * 07/18/2014 14.1.1    Modify Org_ParseMAdr() to populate M_ADDR_D for foreign addr.
 * 10/10/2014 14.7.1    Modify Org_DocMatch() to deal with various file type on the same batch.
 *                      Sort DocFile in Org_LoadGrGr() before open to warrant match rate.
 * 11/05/2014 14.9.1    Fix memory overun in Org_MergeGrGrRec() by increase buffer size for acSeller.
 * 01/04/2015 14.10.2   Fix special cases of mail addr in Org_MergeAdr() and Org_ParseMAdr().
 * 01/14/2015 14.11.0   Add Org_MergeAcre() to merge lot acres and lot sqft from basemap.
 *                      Rename MergeOrgRoll() to Org_Load_Roll().
 * 01/16/2015 14.11.1   Fix bug in Org_MergeAcre()
 * 08/10/2015 15.1.0    Remove adderss check from Org_MatchGrGr() since GrGr only has mail addr.
 * 08/11/2015 15.1.1    Modify Org_MergeGrGrRec() to update transfer regardless of DocType.
 * 08/22/2015           Add -Xa to load CHARS file.  This file overwrites data from Org_Acreage.txt
 *                      Add Org_ConvStdChar() and Org_MergeChar() to support -Xa & -Ma.
 * 08/28/2015 15.1.3    Add -Mr option to add lot sqft & lot area
 * 03/09/2016 15.8.0    Modify Org_ChkCondoList() to support 2015 data file from Travis.
 *                      Rename old version to Org_ChkCondoList1().
 * 03/26/2016 15.8.1    Modify Org_LoadGrGr() to check for duplicate record using first 60 bytes.
 * 03/29/2016 15.8.2    Modify MergeGrGr() to merge new owner if update XFER.
 * 06/20/2016 15.9.7    Modify Org_MergeGrGrRec() to remove extra code.  Modify Org_MatchGrGr() to 
 *                      ignore incomplete APN.
 * 09/24/2016 16.2.7    Fix mailing adr in Org_ParseMAdr()
 * 10/23/2016 16.5.1    Add option to load tax data.  Add function Org_Zone2Dec().
 * 04/06/2017 16.13.8   Fix bug in Org_CreateGrGrRec() when DocTax is too big. Modify Org_FixGrGr()
 *                      to fix SalePrice too.
 * 04/07/2017 16.13.9   Fix bug in Org_MergeGrGrRec() that NAME2 overflows into NAMEFLG1
 * 05/08/2017 16.14.9   Modify Org_MergeGrGrRec() to update owner only for DEED, GRANT DEED, and TRUSTEES DEED
 * 05/20/2017 16.14.12  Modify tax processing sequence to load all available files.
 * 06/22/2017 17.0.0    Modify Org_FixGrGr() to set NonSale_Flg & IsXfer_Flg
 * 06/23/2017           Fix bug in Org_MergeGrGrRec() when Name2 is longer than SIZ_NAME2.
 * 07/09/2017 17.1.1    Add Org_UpdSitus() to copy situs1 from ORG.TC. Modify Org_MergeRoll() to remove
 *                      XFERDOC since no DOCDATE.  Modify Org_Load_LDR() to update situs using Org_UpdSitus().
 * 12/27/2017 17.5.3    Modify Org_LoadGrGr() to detect UNICODE file and convert to ANSI before processing.
 * 01/17/2018 17.6.2    Loading tax using scraped data only
 * 03/02/2018 17.6.8    Adding option -Xs to use sales data from Attom data extract.
 * 03/05/2018           Modify Org_MergeGrGrRec() to update only if current sale is newer.
 * 03/22/2018 17.6.9    Add -Ps option to purge old sale.  Modify Org_MergeGrGr() to clear old sales.
 *                      Remove setting MERG_CSAL when extract sales from loadOrg()
 * 04/13/2018 17.10.0   Modify Org_MergeGrGrRec() changing logic to update owner name.  
 *                      Call ApplyCumSale() with option to update owner and mailing.
 * 05/18/2018 17.10.11  Fix bug in Org_Load_TaxDelq().  Change tax loading process to load tax base and detail
 *                      from scraped files, but delq from county file TC02CA01.txt.  To load county file only,
 *                      just comment out the Main_TC under [ORG] in INi file.
 * 05/22/2018 17.11.0   Modify -Xs option to use template ASH_File defined in INI for output sale file.
 * 07/20/2018 18.2.2    Use ASH_File as cum sale file to update when -Ms is used.
 * 07/23/2018 18.2.3    Modify Org_UpdSitus()to add HseNo and left justify house number.  Modify Org_Load_LDR()
 *                      to look for situs file in INI file.
 * 08/09/2018 18.3.0    Add -Mn to merge sale from NDC.
 * 08/14/2018 18.4.0    Replace R01.MergeArea() with PQ_MergeLotArea() using new ORG_Basemap.txt
 * 09/29/2018 18.5.1    Move -Mr option to LoadOne.cpp
 * 10/15/2018 18.5.4    Use SetTaxRateIncl() to set include tax rate flag (bInclTaxRate).
 * 10/22/2018 18.5.8    Modify Org_LoadGrGr() to fix broken GrGr records.
 *            18.5.8.1  Force GrGr update to use Org_GrGr.sls
 * 11/26/2018 18.6.5    Modify Org_ParseMAdr() to fix mail addr problem when mail addr1 is split into two.
 * 03/07/2019 18.9.10   Modify Org_DocMatch() to set NoneSale_Flg & isXfer_Flg.  This will fix problem
 *                      that owner name is wrongly update on Quit Claim.
 * 03/10/2019 18.10.0   Add -M1 option to merge situs from NDC.  Modify Org_UpdSitus() to use situs record from NDC.
 * 04/24/2019 18.11.7   Fix suffix in Org_UpdSitus() and remove update situs automatically when loading LDR.
 * 05/16/2019 18.12.2   Add convertSaleData() to processing GRGR to generate SALE_EXP.SLS using SCSAL_EXT format
 *                      to standardize future sale layout and to provide sale export to bulk customer.
 * 06/11/2019 18.12.9   Add Org_ParseTaxSupp() & Org_Load_TaxSupp() to load supplemental file TC11CT01.txt
 * 06/18/2019 18.12.11  Clear old sales before applying GRGR.
 * 06/30/2019 19.0.2    Exit program when input file is missing on -Mn option.
 * 07/24/2019 19.0.5    Fix bug in Org_UpdSitus()
 * 10/30/2019 19.3.4    Modify Org_Load_TaxSupp() to skip processing if input file not available.
 * 11/05/2019 19.4.3    Modify Org_Load_TaxDelq() to skip processing if input file not available.
 * 01/09/2020 19.5.11   Modify Org_ParseTaxSupp() to skip records without tax amount or tax amt=1
 * 04/02/2020 19.8.7    Remove -Xs and replace it with -Xn option to format and merge NDC recorder data.
 * 04/28/2020 19.9.1    Remove -Ut and use -T to load both full & partial tax file with TC_LoadTax().
 *                      Modify Org_ParseTaxSupp() to apply bill status correctly.
 * 06/17/2020 19.10.2   Use ApplyCumSaleNR() to apply NDC sale to R01 instead of ApplyCumSale().
 * 07/24/2020 20.2.3    Modify Org_ExtrLien() to move sort input file into loadOrg() to share input with Org_Load_LDR().
 * 06/03/2021 20.8.5    Modify Org_UpdSitus() to add special case for street name "LE PRT" and check for valid Zipcode.
 * 09/01/2021 21.2.0    Modify tax updating process to support update Delq & Sup files only.
 * 10/18/2021 21.2.8    Update log msg in Org_ParseTaxDetail().
 * 11/24/2021 21.4.1    Add Org_Load_TC04TY30() to load detail file.  Rename Org_Load_TaxDetail() to Org_Load_TC01CT02().
 * 12/14/2021 21.4.5    Add NdcSalePlace option in INI to determine whether load NDC sale before or after GRGR.
 *                      To load GRGR first, set NdcSalePlace=3.  We only use NDC sale to fill in what missing in GRGR.
 * 01/30/2022 21.4.10   Remove tax rate from agency table in Org_ParseTC04TY30() to avoid duplicate.
 * 03/03/2022 21.6.0    Create BillNum in Org_ParseTaxBase() to handle multiple bills in singple APN.
 *                      Modify Org_ParseTC04TY30() to create BillNum by add ".00" to APN.  Also add special handling
 *                      of parcel with multiple bills.  Use updateTotalTaxRate() to update tb_TotalRate.
 * 03/19/2022 21.8.3    Modify Org_LoadGrGr() due to GRGR file name change.  Also the Deed file is now 
 *                      in Unicode.  We have to convert to ANSI before processing.
 * 04/08/2022 21.8.6    Modify Org_LoadGrGr() to ignore deed records without APN.
 * 04/09/2022 21.8.7    Modify Org_LoadGrGr() to support both Deed.txt & Deeds.txt as input file.
 * 04/26/2022 21.9.0    Use acGrBkTmpl as backup folder for GrGr files. 
 *                      Add -Xd option to extract legal.
 * 06/25/2022 21.9.5    Fix bug that apply wrong owner name when load/merge GRGR at the same time.
 * 07/21/2022 22.0.2    Modify -Mn option not to update owner name when -L is used.
 * 10/20/2022 22.2.9    Modify Org_MergeOwner() to keep comma in owner name (Denise request).
 *                      Old version is renamed to Org_MergeOwner1().
 * 03/24/2023 22.6.0    Replace updateTotalTaxRate() with doUpdateTotalRate().
 * 10/16/2023 23.3.4    Modify Org_Load_TC04TY30() to correct sorting command on Agency file.
 * 01/25/2024 23.5.7    Modify Org_ParseTaxBase() & Org_UpdateDfltAmt() due to layout change.
 *                      Modify Org_ParsePayment() and add Org_UpdateRed() for future use.
 * 02/01/2024 23.6.0    Stop update OWNER when update sale using external data.
 *                      Modify Org_MergeGrGrMailing() & Org_MergeMAdr() to fix mail unit issue.
 * 03/21/2024 23.7.3    Modify Org_LoadGrGr() to insert file date processing into ORG_GRGR record.
 * 04/23/2024 23.8.1    Modify Org_MergeOwner() to remove leading space on Owner2.
 * 05/10/2024 23.8.3    Modify Org_MergeMAdr() to fix bad mail zip and to add mailzip to OFF_M_CTY_ST_D (i.e. 01917123)
 * 08/02/2024 24.9.0    Modify Org_MergeRoll() to add ExeType.
 * 08/20/2024 24.1.1    Fix bug in Org_LoadGrGr() that drops some sales when prior record has no APN.
 *
 *********************************************************************************************/

#include "stdafx.h"
#include "Prodlib.h"
#include "CountyInfo.h"
#include "R01.h"
#include "RecDef.h"
#include "Tables.h"
#include "SaleRec.h"
#include "Logs.h"
#include "LoadOne.h"
#include "Utils.h"
#include "XlatTbls.h"
#include "doGrGr.h"
#include "doSort.h"
#include "doOwner.h"
#include "formatApn.h"
#include "doZip.h"
#include "MergeOrg.h"
#include "UseCode.h"
#include "LOExtrn.h"
#include "PQ.h"
#include "CharRec.h"
#include "Tax.h"
#include "NdcExtr.h"

static   int   iGrGrLen;
static   FILE  *fdDeed, *fdDoc, *fdSale, *fdChar, *fdSitus;
static   long  lSaleMatch, lSaleSkip, lCharMatch, lCharSkip, lUpdSitus;

/******************************* Org_MergeChar *******************************
 *
 * Merge Org_Char.dat in STDCHAR format
 *
 *****************************************************************************/

int Org_MergeStdChar(char *pOutbuf)
{
   static   char acRec[2048], *pRec=NULL;
   char     acTmp[256];
   long     lBldgSqft, lGarSqft, lCarSqft, lTmp;
   int      iLoop, iBeds, iFBath, iHBath;
   STDCHAR *pChar;

   // Get first Char rec for first call
   if (!pRec)
      pRec = fgets(acRec, 1024, fdChar);

   pChar = (STDCHAR *)pRec;

   do
   {
      if (!pRec)
      {
         fclose(fdChar);
         fdChar = NULL;
         return 1;      // EOF
      }

      // Compare APN
      iLoop = memcmp(pOutbuf, pRec, iApnLen);
      if (iLoop > 0)
      {
         if (bDebug)
            LogMsg0("Skip Char rec  %.*s", iApnLen, pRec);
         pRec = fgets(acRec, 1024, fdChar);
         lCharSkip++;
      }
   } while (iLoop > 0);

   // If not match, return
   if (iLoop)
      return 1;

   // Yrblt
   lTmp = atoin(pChar->YrBlt, SIZ_CHAR_YRBLT);
   if (lTmp > 1600 && lTmp <= lToyear)
      memcpy(pOutbuf+OFF_YR_BLT, pChar->YrBlt, SIZ_YR_BLT);

   // BldgSqft
   lBldgSqft = atoin(pChar->BldgSqft, SIZ_CHAR_SQFT);
   if (lBldgSqft > 10)
   {
      sprintf(acTmp, "%*u", SIZ_BLDG_SF, lBldgSqft);
      memcpy(pOutbuf+OFF_BLDG_SF, acTmp, SIZ_BLDG_SF);
   }

   // Garage Sqft
   lGarSqft = atoin(pChar->GarSqft, SIZ_CHAR_SQFT);
   lCarSqft = atoin(pChar->CarpSqft, SIZ_CHAR_SQFT);
   if (lGarSqft > 10)
   {
      sprintf(acTmp, "%*u", SIZ_GAR_SQFT, lGarSqft);
      memcpy(pOutbuf+OFF_GAR_SQFT, acTmp, SIZ_GAR_SQFT);
      *(pOutbuf+OFF_PARK_TYPE) = 'Z';  
   } else if (lCarSqft > 10)
   {
      sprintf(acTmp, "%*u", SIZ_GAR_SQFT, lCarSqft);
      memcpy(pOutbuf+OFF_GAR_SQFT, acTmp, SIZ_GAR_SQFT);
      *(pOutbuf+OFF_PARK_TYPE) = 'C';  
   }

   // Lot Sqft
   lTmp = atoin(pChar->LotSqft, SIZ_CHAR_SQFT);
   if (lTmp > 10)
   {
      sprintf(acTmp, "%*u", SIZ_LOT_SQFT, lTmp);
      memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);

      double dTmp = (double)lTmp/SQFT_FACTOR_1000;
      sprintf(acTmp, "%*u", SIZ_LOT_ACRES, (long)(dTmp+0.05));
      memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);
   }

   // Pools
   *(pOutbuf+OFF_POOL) = pChar->Pool[0];

   // Beds
   iBeds = atoin(pChar->Beds, SIZ_CHAR_BEDS);
   if (iBeds > 0)
   {
      sprintf(acTmp, "%*u", SIZ_BEDS, iBeds);
      memcpy(pOutbuf+OFF_BEDS, acTmp, SIZ_BEDS);
   }

   // Bath
   iFBath = atoin(pChar->FBaths, SIZ_CHAR_BATHS);
   if (iFBath > 0)
   {
      sprintf(acTmp, "%*u", SIZ_BATH_F, iFBath);
      memcpy(pOutbuf+OFF_BATH_F, acTmp, SIZ_BATH_F);
   }

   // Half bath
   iHBath = atoin(pChar->HBaths, SIZ_CHAR_BATHS);
   if (iHBath > 0)
   {
      sprintf(acTmp, "%*u", SIZ_BATH_H, iHBath);
      memcpy(pOutbuf+OFF_BATH_H, acTmp, SIZ_BATH_H);
   }

   lCharMatch++;

   // Get next Char rec
   pRec = fgets(acRec, 1024, fdChar);

   return 0;
}

int Org_MergeChar(int iSkip)
{
   char     acBuf[MAX_RECSIZE], acRawFile[_MAX_PATH], acOutFile[_MAX_PATH], acTmp[256];
   int      iRet, iRollUpd=0, lCnt=0;
   DWORD    nBytesRead, nBytesWritten;
   BOOL     bRet, bUseR01=true;
   HANDLE   fhIn, fhOut;

   sprintf(acRawFile, acRawTmpl, "ORG", "ORG", "R01");
   sprintf(acOutFile, acRawTmpl, "ORG", "ORG", "T01");

   // Rename files for processing
   if (_access(acRawFile, 0))
   {
      sprintf(acRawFile, acRawTmpl, "ORG", "ORG", "S01");
      if (!_access(acRawFile, 0))
      {
         LogMsg("Use %s as input for MergeAcres", acRawFile);
         bUseR01 = false;
      } else
      {
         LogMsg("***** Error in MergeAcres(): Missing input file %s.  Please recheck!", acRawFile);
         return -1;
      }
   }

   // Open Char file
   LogMsg("Open Char file %s", acCChrFile);
   fdChar = fopen(acCChrFile, "r");
   if (fdChar == NULL)
   {
      LogMsg("***** Error opening acreage file: %s\n", acCChrFile);
      return -2;
   }

   // Open Input file
   LogMsg("Open input file %s", acRawFile);
   fhIn = CreateFile(acRawFile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhIn == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening input file: %s\n", acRawFile);
      return -3;
   }

   // Open Output file
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening output file: %s\n", acOutFile);
      return -4;
   }

   // Copy skip record
   memset(acBuf, ' ', iRecLen);
   while (iSkip-- > 0)
   {
      ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);
      // Ignore record start with 99999999
      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
   }

   // Merge loop
   do
   {
      bRet = ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);

      // Check for EOF
      if (!bRet)
      {
         LogMsg("***** Error reading input file %s (%f)", acRawFile, GetLastError());
         break;
      }

      // EOF ?
      if (!nBytesRead)
         break;

      // Update char info
      if (fdChar)
      {
         iRet = Org_MergeStdChar(acBuf);
         if (iRet == -1)
         {
            fclose(fdChar);
            fdChar = (FILE *)NULL;
         } else if (!iRet)
            iRollUpd++;
      }

      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);

      if (!bRet)
      {
         LogMsg("***** Error writing to out file %s: %d\n", acOutFile, GetLastError());
         break;
      }
   } while (bRet);

   // Close files
   if (fdChar)
      fclose(fdChar);
   if (fhOut)
      CloseHandle(fhOut);
   if (fhIn)
      CloseHandle(fhIn);

   // Rename R01 to SAV if R01 is used as input
   if (bUseR01)
   {
      sprintf(acTmp, acRawTmpl, "ORG", "ORG", "SAV");
      if (!_access(acTmp, 0))
      {
         LogMsg("Remove saved file %s", acTmp);
         if (iRet=remove(acTmp))
            LogMsg("***** Error removing %s to %s (%d)", acTmp, errno);
      }

      LogMsg("Rename %s to %s", acRawFile, acTmp);
      if (iRet=rename(acRawFile, acTmp))
         LogMsg("***** Error renaming %s to %s (%d)", acRawFile, acTmp, errno);
   } else
      sprintf(acRawFile, acRawTmpl, "ORG", "ORG", "R01");

   LogMsg("Rename %s to %s", acOutFile, acRawFile);
   if (iRet=rename(acOutFile, acRawFile))
      LogMsg("***** Error renaming %s to %s (%d)", acOutFile, acRawFile, errno);

   LogMsg("Total output records:       %u", lCnt);
   LogMsg("Total records updated:      %u", iRollUpd);

   lRecCnt = lCnt;
   return iRet;
}

/****************************** Org_ConvStdChar ******************************
 *
 * Convert char file to STDCHAR format.
 *
 *****************************************************************************/

int Org_ConvStdChar(char *pInfile)
{
   FILE     *fdIn, *fdOut;
   char     acInbuf[1024], acOutbuf[2048], acTmpFile[256], acTmp[256], *pRec;
   int      iRet, iTmp, iCnt=0;
   STDCHAR  *pCharRec=(STDCHAR *)&acOutbuf[0];

   LogMsg0("Converting char file %s", pInfile);
   if (!(fdIn = fopen(pInfile, "r")))
      return -1;

   sprintf(acTmpFile, "%s\\%s\\%s_char.tmp", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   if (!(fdOut = fopen(acTmpFile, "w")))
   {
      fclose(fdIn);
      LogMsg("***** Error creating output file %s", acTmpFile);
      return -2;
   }

   // Skip first record - header
   iTmp = 0;
   for (iTmp = 0; iTmp < iHdrRows; iTmp++)
      pRec = fgets(acInbuf, 1024, fdIn);

   while (!feof(fdIn))
   {
      pRec = fgets(acInbuf, 1024, fdIn);

      if (!pRec)
         break;
#ifdef _DEBUG
      //if (!memcmp(acBuf, "930218200012009759", 9))
      //   iRet = 0;
#endif

      if (cDelim == ',')
         iRet = ParseStringNQ(pRec, cDelim, ORG_CHAR_USE+1, apTokens);
      else
         iRet = ParseStringIQ(pRec, cDelim, ORG_CHAR_USE+1, apTokens);
      if (iRet != ORG_CHAR_USE+1)
      {
         LogMsg("*** Bad CHAR record (%d): %s", iCnt, pRec);
         continue;
      }

      memset(acOutbuf, ' ', sizeof(STDCHAR));

      // Apn
      memcpy(pCharRec->Apn, apTokens[ORG_CHAR_APN], strlen(apTokens[ORG_CHAR_APN]));
      
      // Pool/Spa
      if (*apTokens[ORG_CHAR_POOL_FLG] == 'Y' && *apTokens[ORG_CHAR_SPA_FLG] == 'Y')
         pCharRec->Pool[0] = 'C';
      else if (*apTokens[ORG_CHAR_POOL_FLG] == 'Y') pCharRec->Pool[0] = 'P';
      else if (*apTokens[ORG_CHAR_SPA_FLG] == 'Y') pCharRec->Pool[0] = 'S';

      // Use cat
      iTmp = blankRem(apTokens[ORG_CHAR_USE]);
      memcpy(pCharRec->LandUse, apTokens[ORG_CHAR_USE], iTmp);

      // YrBlt
      iTmp = atoi(apTokens[ORG_CHAR_CNS_YR]);
      if (iTmp > 1600 && iTmp != 1900)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(pCharRec->YrBlt, acTmp, iRet);
      }

      // BldgSqft
      iTmp = atoi(apTokens[ORG_CHAR_IMP_SQFT]);
      if (iTmp > 100)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(pCharRec->BldgSqft, acTmp, iRet);
      }

      // Lot Sqft
      iTmp = atoi(apTokens[ORG_CHAR_LND_SQFT]);
      if (iTmp > 100)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(pCharRec->LotSqft, acTmp, iRet);
      }

      // Garage Sqft
      iTmp = atoi(apTokens[ORG_CHAR_GARAGE_SQFT]);
      if (iTmp > 100)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(pCharRec->GarSqft, acTmp, iRet);
      }

      // Carport Sqft
      iTmp = atoi(apTokens[ORG_CHAR_CARPORT_SQFT]);
      if (iTmp > 100)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(pCharRec->CarpSqft, acTmp, iRet);
      }

      // Beds
      iTmp = atoi(apTokens[ORG_CHAR_NBR_BED]);
      if (iTmp > 0)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(pCharRec->Beds, acTmp, iRet);
      }

      // Baths
      iTmp = atoi(apTokens[ORG_CHAR_NBR_BTH]);
      if (iTmp > 0)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(pCharRec->FBaths, acTmp, iRet);
      }

      // Half baths
      iTmp = atoi(apTokens[ORG_CHAR_NBR_HLF_BTH]);
      if (iTmp > 0)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(pCharRec->HBaths, acTmp, iRet);
      }

      pCharRec->CRLF[0] = '\n';
      pCharRec->CRLF[1] = '\0';
      try {
         fputs(acOutbuf, fdOut);
      } catch (...)
      {
         LogMsg("??? Exception occurs in Org_ConvStdChar(),  APN=%s", apTokens[0]);
      }

      if (!(++iCnt % 1000))
         printf("\r%u", iCnt);
   }

   if (fdIn) fclose(fdIn);
   if (fdOut) fclose(fdOut);

   // Sort output on ASMT
   if (iCnt > 100)
   {
      LogMsgD("\nSorting char file %s --> %s", acTmpFile, acCChrFile);
      iRet = sortFile(acTmpFile, acCChrFile, "S(1,12,C,A) F(TXT)");
   } else
   {
      printf("\n");
      iRet = 0;
   }
   return iRet;
}

/******************************** Org_UpdExeVal ******************************
 *
 * Return 0 if successful, >0 if error
 *
 *****************************************************************************/

int Org_UpdExeVal(FILE *fd, char *pExeTotal, char *pOutbuf)
{
   static char    acRec[1024], *pRec=NULL;
   char           acTmp[1024];
   int            iRet, iLoop;

   // Get first rec for first call
   if (!pRec)
   {
      // Skip headers
      pRec = fgets(acRec, 1024, fd);
      pRec = fgets(acRec, 1024, fd);
      pRec = fgets(acRec, 1024, fd);

      // Get first record
      pRec = fgets(acRec, 1024, fd);
   }

#ifdef _DEBUG
   //if (!memcmp(pApn, "00209118", 8))
   //   iRet = 0;
#endif
   do
   {
      // Parse input - don't change the original input record
      strcpy(acTmp, pRec);
      iRet = ParseStringNQ(acTmp, 9, MAX_FLD_TOKEN, apTokens);
      if (iRet < ORG_AMNT_COR_NET)
      {
         LogMsg("***** Error: bad input record for APN=%s", apTokens[ORG_AMNT_APN]);
         return -2;
      }

      // Compare Apn
      remChar(apTokens[ORG_AMNT_APN], '-');
      iLoop = memcmp(pOutbuf, apTokens[ORG_AMNT_APN], iApnLen);
      if (iLoop > 0)
      {
         pRec = fgets(acRec, 1024, fd);
         if (!pRec)
            return -1;      // EOF
      }
   } while (iLoop > 0);

   if (iLoop < 0)
      return 1;

   // Update Exe value & exe code
   remChar(apTokens[ORG_AMNT_COR_EXE], ',');
   iRet = atol(apTokens[ORG_AMNT_COR_EXE]);
   if (iRet > 0)
   {
      sprintf(acTmp, "%*u", SIZ_LIEN_EXEAMT, iRet);
      memcpy(pExeTotal, acTmp, SIZ_LIEN_EXEAMT);
   }

   if (*apTokens[ORG_AMNT_EXE_TYPE] > ' ')
   {
      if (*(pOutbuf+OFF_EXE_CD1) == ' ')
         *(pOutbuf+OFF_EXE_CD1) = *apTokens[ORG_AMNT_EXE_TYPE];
      else if (*(pOutbuf+OFF_EXE_CD1) != *apTokens[ORG_AMNT_EXE_TYPE])
         *(pOutbuf+OFF_EXE_CD2) = *apTokens[ORG_AMNT_EXE_TYPE];
   }

   return 0;
}

/******************************** Org_MergeOwner *****************************
 *
 * Return 0 if successful, >0 if error
 *
 *****************************************************************************/

void Org_MergeOwner(char *pOutbuf)
{
   int   iTmp;
   char  acOwner1[128], acOwner2[128], acTmp[128], acTmp1[64], acTmp2[64], acTmp3[64];
   char  acLName[64], acCareOf[64], acVesting[8];
   char  *pTmp, *pName1, *pName2, *pName3, *pName4;
   bool  bRet;

   OWNER myOwner;

   // Clear output buffer if needed
   removeNames(pOutbuf, true);

   acCareOf[0] = 0;
   pName1 = apTokens[ORG_ROLL_OWNERNAME];
   pName2 = apTokens[ORG_ROLL_NAD_B];
   pName3 = apTokens[ORG_ROLL_NAD_C];
   pName4 = apTokens[ORG_ROLL_NAD_D];

   if (!*pName1)
      return;

   strcpy(acTmp1, pName1);
   strcpy(acTmp3, pName3);
   if (*pName4)
   {
      strcat(acTmp3, " ");
      strcat(acTmp3, pName4);
   }

   // Check for CareOf
   if (acTmp3[0] == '%')
   {
      strcpy(acCareOf, (char *)&acTmp3[1]);
      acTmp3[0] = 0;
      strcpy(acTmp2, pName2);
   } else if (*pName2 == '%')
   {
      if (*pName3)
      {
         sprintf(acCareOf, "%s %s", pName2+1, acTmp3);
         acTmp3[0] = 0;
      } else
         strcpy(acCareOf, pName2+1);
      acTmp2[0] = 0;
   } else
   {
      strcpy(acTmp2, pName2);
   }

   // Update CareOf
   if (acCareOf[0])
      updateCareOf(pOutbuf, acCareOf, strlen(acCareOf));

   // Save last name
   strcpy(acLName, pName1);
   pTmp = strchr(acLName, ',');
   if (pTmp) *pTmp = 0;

   // If pName1 is a company/trust, ignore the rest
   // If pName2 doesn't contain comma, it is part of Name1.  If it does
   // contain comma, it is Name2.  Merge name if they have the same last name
   // Otherwise, keep them as two separate names.

   // First check to see if Name2 is continuation of Name1
   if (acTmp2[0] > ' ' && !strchr(acTmp2, ',') && memcmp(acLName, acTmp2, strlen(acLName)))
   {
      // Do not merge if Name1 is a TRUST, TRUSTEE, or TR
      bRet = false;
      pTmp = strrchr(acTmp1, ' ');
      if (pTmp && (!memcmp(pTmp, " TR", 3) || strstr(acTmp1, " TR ")))
         bRet = true;

      // Merge Name1 and Name2
      if (!bRet)
      {
         sprintf(acTmp, "%s %s", acTmp1, acTmp2);
         strcpy(acTmp1, acTmp);
         acTmp2[0] = 0;                      // Remove Name2 after merged
      }
   }
   if (acTmp3[0] > ' ' && !strchr(acTmp3, ','))
   {
      // Merge Name2 and Name3
      sprintf(acTmp, "%s %s", acTmp2, acTmp3);
      strcpy(acTmp2, acTmp);
      acTmp3[0] = 0;
   }

   // Fix known bad chars
   replChar(acTmp1, 0xC2, '-');
   replChar(acTmp1, 0xA2, '-');
   replStr(acTmp1, "--AM", "GAM");
   replStr(acTmp1, "P--A", "PA");
   strcpy(acOwner1, acTmp1);

   // Update vesting
   acVesting[0] = 0;
   pTmp = findVesting(acTmp1, acVesting);
   if (pTmp)
   {
      if (!memcmp(acVesting, "EA", 2))
         *(pOutbuf+OFF_ETAL_FLG) = 'Y';
      memcpy(pOutbuf+OFF_VEST, acVesting, strlen(acVesting));

      // Remove vesting from owner name for parsing
      *pTmp = 0;
   }

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "00103201", 8))
   //   iTmp = 0;
#endif

   acOwner2[0] = 0;
   if (acTmp2[0])
   {
      // Remove leading space
      lTrim(acTmp2);
      strcpy(acOwner2, acTmp2);
   } else if (acTmp3[0])
      strcpy(acOwner2, acTmp3);

   // If Owner1 and Owner2 has the same last name, combine them
   if (!pTmp && acOwner2[0])
   {
      pTmp = strchr(acOwner1, ',');
      if (pTmp)
      {
         iTmp = pTmp - &acOwner1[0];
         if (!memcmp(acOwner1, acOwner2, iTmp))
         {
            MergeName(acOwner1, acOwner2, acOwner1);
            acOwner2[0] = 0;
         }
      }
   }

   // Store Name1
   iTmp = blankRem(acOwner1);
   vmemcpy(pOutbuf+OFF_NAME1, acOwner1, SIZ_NAME1, iTmp);

   if (acVesting[0] >= 'A' && strchr(acTmp1, ' '))
      strcpy(acOwner1, acTmp1);

   // Check for the exist of Name2
   if (acOwner2[0])
      vmemcpy(pOutbuf+OFF_NAME2, acOwner2, SIZ_NAME2);

   // Now parse owners
   remChar(acOwner1, ',');
   splitOwner(acOwner1, &myOwner, 3);

   // Store swap name
   vmemcpy(pOutbuf+OFF_NAME_SWAP, myOwner.acSwapName, SIZ_NAME_SWAP);
}

// This version removes comma from owner name
void Org_MergeOwner1(char *pOutbuf)
{
   int   iTmp;
   char  acOwner1[128], acOwner2[128], acTmp[128], acTmp1[64], acTmp2[64], acTmp3[64];
   char  acLName[64], acCareOf[64], acVesting[8];
   char  *pTmp, *pName1, *pName2, *pName3, *pName4;
   bool  bRet;

   OWNER myOwner;

   // Clear output buffer if needed
   removeNames(pOutbuf, true);

   acCareOf[0] = 0;
   pName1 = apTokens[ORG_ROLL_OWNERNAME];
   pName2 = apTokens[ORG_ROLL_NAD_B];
   pName3 = apTokens[ORG_ROLL_NAD_C];
   pName4 = apTokens[ORG_ROLL_NAD_D];

   if (!*pName1)
      return;

   strcpy(acTmp1, pName1);
   strcpy(acTmp3, pName3);
   if (*pName4)
   {
      strcat(acTmp3, " ");
      strcat(acTmp3, pName4);
   }

   // Check for CareOf
   if (acTmp3[0] == '%')
   {
      strcpy(acCareOf, (char *)&acTmp3[1]);
      acTmp3[0] = 0;
      strcpy(acTmp2, pName2);
   } else if (*pName2 == '%')
   {
      if (*pName3)
      {
         sprintf(acCareOf, "%s %s", pName2+1, acTmp3);
         acTmp3[0] = 0;
      } else
         strcpy(acCareOf, pName2+1);
      acTmp2[0] = 0;
   } else
   {
      strcpy(acTmp2, pName2);
   }

   // Update CareOf
   if (acCareOf[0])
      updateCareOf(pOutbuf, acCareOf, strlen(acCareOf));

   // Save last name
   strcpy(acLName, pName1);
   pTmp = strchr(acLName, ',');
   if (pTmp) *pTmp = 0;

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "93726007", 8))
   //   iTmp = 0;
#endif

   // If pName1 is a company/trust, ignore the rest
   // If pName2 doesn't contain comma, it is part of Name1.  If it does
   // contain comma, it is Name2.  Merge name if they have the same last name
   // Otherwise, keep them as two separate names.

   // First check to see if Name2 is continuation of Name1
   if (acTmp2[0] > ' ' && !strchr(acTmp2, ',') && memcmp(acLName, acTmp2, strlen(acLName)))
   {
      // Do not merge if Name1 is a TRUST, TRUSTEE, or TR
      bRet = false;
      pTmp = strrchr(acTmp1, ' ');
      if (pTmp && (!memcmp(pTmp, " TR", 3) || strstr(acTmp1, " TR ")))
         bRet = true;

      // Merge Name1 and Name2
      if (!bRet)
      {
         sprintf(acTmp, "%s %s", acTmp1, acTmp2);
         strcpy(acTmp1, acTmp);
         acTmp2[0] = 0;                      // Remove Name2 after merged
      }
   }
   if (acTmp3[0] > ' ' && !strchr(acTmp3, ','))
   {
      // Merge Name2 and Name3
      sprintf(acTmp, "%s %s", acTmp2, acTmp3);
      strcpy(acTmp2, acTmp);
      acTmp3[0] = 0;
   }

   // Fix known bad chars
   replChar(acTmp1, 0xC2, '-');
   replChar(acTmp1, 0xA2, '-');
   replStr(acTmp1, "--AM", "GAM");
   replStr(acTmp1, "P--A", "PA");
   strcpy(acOwner1, acTmp1);

   // Update vesting
   acVesting[0] = 0;
   pTmp = findVesting(acTmp1, acVesting);
   if (pTmp)
   {
      if (!memcmp(acVesting, "EA", 2))
         *(pOutbuf+OFF_ETAL_FLG) = 'Y';
      memcpy(pOutbuf+OFF_VEST, acVesting, strlen(acVesting));

      // Remove vesting from owner name for parsing
      *pTmp = 0;
   }

   acOwner2[0] = 0;
   if (acTmp2[0])
      strcpy(acOwner2, acTmp2);
   else if (acTmp3[0])
      strcpy(acOwner2, acTmp3);

   // If Owner1 and Owner2 has the same last name, combine them
   if (!pTmp && acOwner2[0])
   {
      pTmp = strchr(acOwner1, ',');
      if (pTmp)
      {
         iTmp = pTmp - &acOwner1[0];
         if (!memcmp(acOwner1, acOwner2, iTmp))
         {
            MergeName(acOwner1, acOwner2, acOwner1);
            acOwner2[0] = 0;
         }
      }
   }

   // Store Name1
   remChar(acOwner1, ',');
   iTmp = blankRem(acOwner1);
   vmemcpy(pOutbuf+OFF_NAME1, acOwner1, SIZ_NAME1, iTmp);

   if (acVesting[0] >= 'A' && strchr(acTmp1, ' '))
      strcpy(acOwner1, acTmp1);

   // Check for the exist of Name2
   if (acOwner2[0])
      vmemcpy(pOutbuf+OFF_NAME2, acOwner2, SIZ_NAME2);

   // Now parse owners
   splitOwner(acOwner1, &myOwner, 3);

   // Store Name1
   //iTmp = strlen(myOwner.acName1);
   //if (iTmp > SIZ_NAME1) iTmp = SIZ_NAME1;
   //memcpy(pOutbuf+OFF_NAME1, myOwner.acName1, iTmp);

   // Store swap name
   vmemcpy(pOutbuf+OFF_NAME_SWAP, myOwner.acSwapName, SIZ_NAME_SWAP);
}

/***************************** Org_FormatGrGrOwner ***************************
 *
 * This function only merge owner names if they have the same last name.
 * Return 1 if merge is successful, otherwise return 0 or 2.
 *
 *****************************************************************************/

int Org_FormatGrGrOwner(char *pOwners, char *pSwapName, char *pSaleRec)
{
   char  acOwner1[128], acOwner2[128], acTmp[128], acSave[128];
   char  *pTmp, *pName1, *pName2;
   int   iOwner1Len, iOwner2Len, iTmp;
   bool  bNoMerge = false;

   ORG_GRGR *pRec = (ORG_GRGR *)pSaleRec;
   OWNER    myOwner;

   acSave[0] = 0;
   memcpy(acOwner1, pRec->Grantee[0], SIZ_OGR_NAME);
   memcpy(acOwner2, pRec->Grantee[1], SIZ_OGR_NAME);
   iOwner1Len = blankRem(acOwner1, SIZ_OGR_NAME);
   iOwner2Len = blankRem(acOwner2, SIZ_OGR_NAME);

   // Make sure names are in upper case
   _strupr(acOwner1);
   _strupr(acOwner2);

   if (iOwner1Len > 0 && iOwner2Len > 0)
   {
      iTmp = (iOwner1Len < iOwner2Len ? iOwner1Len:iOwner2Len);
      if (!memcmp(acOwner1, acOwner2, iTmp))
         bNoMerge = true;
   }
   
#ifdef _DEBUG
   //if (!memcmp(pRec->ParcelNo, "45136121", 8))
   //   pTmp = NULL;
#endif

   // If both owner names are the same, blank out the second
   if (!strcmp(acOwner1, acOwner2))
   {
      memset(pRec->Grantee[1], ' ', SIZ_OGR_NAME);
      return 2;
   }

   // Point to first name
   pName1 = strchr(acOwner1, ' ');
   pName2 = strchr(acOwner2, ' ');

   strcpy(pOwners, acOwner1);
   if (!pName1 || !pName2)
   {
      strcpy(pSwapName, acOwner1);
      return 0;
   }

   // Skip corp
   if (strstr(pName1, " INC "))
   {
      strcpy(pSwapName, acOwner1);
      return 2;
   }

   // Check last word
   pTmp = strrchr(pName1+1, ' ');
   if (pTmp && (!strcmp(pTmp, " TR") || !strcmp(pTmp, " COTR") || !strcmp(pTmp, " INC")) )
   {
      *pName1 = ' ';
      strcpy(pSwapName, acOwner1);
      return 2;
   }

   *pName1 = 0;
   *pName2 = 0;
   // If last name are not the same, update pSwapName and return 2
   if (bNoMerge || strcmp(acOwner1, acOwner2))
   {
      // Forming swap name on Name1
      strcpy(pSwapName, pName1);
      strcat(pSwapName, " ");
      strcat(pSwapName, acOwner1);
      return 2;
   }

   *pOwners = 0;
   *pSwapName = 0;
   *pName1 = ' ';       // Keep Name1 as is
   pName1 = acOwner1;
   pName2++;            // Skip last name of owner2

   // Check for "TR" in name 1
   pTmp = strrchr(pName1, ' ');
   if (pTmp && (!strcmp(pTmp, " TR") || !strcmp(pTmp, " COTR")))
   {
      strcpy(acSave, pTmp);
      *pTmp = 0;
   }
   pTmp = strrchr(pName2, ' ');
   if (pTmp && (!strcmp(pTmp, " TR") || !strcmp(pTmp, " COTR")))
      *pTmp = 0;

   // Combine owners
   sprintf(acTmp, "%s & %s", pName1, pName2);
   strcpy(pOwners, acTmp);

   // Now parse owners
   splitOwner(acTmp, &myOwner, 3);

   // Store swap name
   if (myOwner.acSwapName[0] > ' ')
      strcpy(pSwapName, myOwner.acSwapName);
   else
      strcpy(pSwapName, pOwners);

   if (acSave[0] > ' ')
      strcat(pOwners, acSave);

   return 1;
}

/********************************* Org_MergeAdr ******************************
 *
 * Return 0 if successful, >0 if error
 *
 *****************************************************************************/

void Org_MergeGrGrMailing(char *pOutbuf, char *pSaleRec)
{
   char     acAddr1[128], acTmp[128], acCity[64];
   long     lTmp;
   int      iTmp;
   ORG_GRGR *pRec = (ORG_GRGR *)pSaleRec;
   ADR_REC  sMailAdr;

   memcpy(acAddr1, pRec->Addr1, SIZ_OGR_ADDR1);
   // Check for bad character
   if (iTmp = replUnPrtChar(acAddr1, ' ', SIZ_OGR_ADDR1))
      LogMsg0("*** Bad character in M_Addr1 at %.12s", pOutbuf);

   blankRem(acAddr1, SIZ_OGR_ADDR1);

   memset(&sMailAdr, 0, sizeof(ADR_REC));
   parseMAdr1_1(&sMailAdr, acAddr1);
   vmemcpy(pOutbuf+OFF_M_STRNUM, sMailAdr.strNum, SIZ_M_STRNUM);
   vmemcpy(pOutbuf+OFF_M_STR_SUB, sMailAdr.strSub, SIZ_M_STR_SUB);
   vmemcpy(pOutbuf+OFF_M_DIR, sMailAdr.strDir, SIZ_M_DIR);
   vmemcpy(pOutbuf+OFF_M_STREET, sMailAdr.strName, SIZ_M_STREET);
   vmemcpy(pOutbuf+OFF_M_SUFF, sMailAdr.strSfx, SIZ_M_SUFF);
   vmemcpy(pOutbuf+OFF_M_UNITNO, sMailAdr.Unit, SIZ_M_UNITNO);
   vmemcpy(pOutbuf+OFF_M_UNITNOX, sMailAdr.UnitNox, SIZ_M_UNITNOX);
   vmemcpy(pOutbuf+OFF_M_CITY, pRec->City, SIZ_M_CITY);
   vmemcpy(pOutbuf+OFF_M_ST, pRec->State, SIZ_M_ST);

   lTmp = atoin(pRec->Zip, SIZ_OGR_ZIP);
   if (lTmp > 100000)
      memcpy(pOutbuf+OFF_M_ZIP, pRec->Zip, SIZ_M_ZIP+SIZ_M_ZIP4);
   else if (lTmp > 1000)
      memcpy(pOutbuf+OFF_M_ZIP, pRec->Zip, SIZ_M_ZIP);

   vmemcpy(pOutbuf+OFF_M_ADDR_D, acAddr1, SIZ_M_ADDR_D);

   memcpy(acCity, pRec->City, SIZ_OGR_CITY);
   myTrim(acCity, SIZ_OGR_CITY);
   lTmp = sprintf(acTmp, "%s %.2s", acCity, pRec->State);
   vmemcpy(pOutbuf+OFF_M_CTY_ST_D, acTmp, SIZ_M_CTY_ST_D, lTmp);
}

/********************************* Org_MergeAdr ******************************
 *
 * 09/23/2016 Fix problem where line are breaking
 *
 * Return 0 if successful, >0 if error
 *
 *****************************************************************************/

int Org_ParseMAdr(ADR_REC *pAdrRec, char *pCareOf, char *pMAdr1, char *pMAdr2, char *pAdr1, char *pAdr2, char *pAdr3, char *pAdr4, char *pAdr5)
{
   char  *p0, *p1, *p2, *pTmp;
   char  acAddr1[256], acAddr2[256], acSuite[32];
   char  sAdr1[256],sAdr2[256],sAdr3[256],sAdr4[256],sAdr5[256];

   int   iRet, iCnt;

   // Init output
   memset((void *)pAdrRec, 0, sizeof(ADR_REC));
   *pCareOf = 0;
   acSuite[0] = 0;

   if (*pAdr1 <= ' ')
      return 0;

   sAdr3[0] = 0;
   sAdr4[0] = 0;
   sAdr5[0] = 0;
   iCnt = countWord(pAdr2);
   if (iCnt == 1 || *pAdr2 == '#' || *(pAdr1+strlen(pAdr1)-1) == '&'
      || !memcmp(pAdr2, "STE ", 4) || !memcmp(pAdr2, "APT ", 4) || !memcmp(pAdr2, "SUITE", 5) || !memcmp(pAdr2, "UNIT ", 5) || !memcmp(pAdr2, "UNIT,", 5))      
   {
      // Take first unit# only
      if (iCnt > 1 && *(pAdr2+4) == ',')
         sprintf(sAdr1, "%s %s", pAdr1, pAdr2+5);
      else if (*pAdr3 == ',')
      {
         strcpy(sAdr1, pAdr1);
         strcat(pAdr2, pAdr3);
         pAdr3 = pAdr2;
      } else
      {
         if (*(pAdr1+strlen(pAdr1)-1) == '-')
            sprintf(sAdr1, "%s%s", pAdr1, pAdr2);
         else
            sprintf(sAdr1, "%s %s", pAdr1, pAdr2);
      }
      iCnt = countWord(pAdr4);
      if (iCnt == 1 || *pAdr4 == '#' || 
         !memcmp(pAdr4, "STE ", 4) || !memcmp(pAdr4, "APT ", 4) || !memcmp(pAdr4, "SUITE", 5) || !memcmp(pAdr4, "UNIT ", 5))
      {
         // Take first unit# only
         //if (iRet > 1 && (pTmp = strchr(pAdr4, ' ')))
         //   *pTmp = 0;
         sprintf(sAdr2, "%s %s", pAdr3, pAdr4);
         strcpy(sAdr3, pAdr5);
      } else
      {
         strcpy(sAdr2, pAdr3);
         strcpy(sAdr3, pAdr4);
         strcpy(sAdr4, pAdr5);
      }
   } else
   {
      strcpy(sAdr1, pAdr1);
      iCnt = countWord(pAdr3);
      if (iCnt == 1 || *pAdr3 == '#' || 
         !memcmp(pAdr3, "STE ", 4) || !memcmp(pAdr3, "APT ", 4) || !memcmp(pAdr3, "SUITE", 5) || !memcmp(pAdr3, "UNIT ", 5))
      {
         sprintf(sAdr2, "%s %s", pAdr2, pAdr3);
         strcpy(sAdr3, pAdr4);
         strcpy(sAdr4, pAdr5);
      } else
      {
         strcpy(sAdr2, pAdr2);
         iCnt = countWord(pAdr4);
         if (iCnt == 1 || *pAdr4 == '#' || 
            !memcmp(pAdr4, "STE ", 4) || !memcmp(pAdr4, "APT ", 4) || !memcmp(pAdr4, "SUITE", 5) || !memcmp(pAdr4, "UNIT ", 5))
         {
            sprintf(sAdr3, "%s %s", pAdr3, pAdr4);
            strcpy(sAdr4, pAdr5);
         } else
         {
            // Special case
            if ((pTmp=strstr(pAdr3, ", ,")) && (pTmp-pAdr3) < 3)
            {
               *pTmp = 0;
               pTmp += 3;
               if (*pTmp == ' ') pTmp++;
               if (*pTmp > ' ')
               {
                  sprintf(sAdr2, "%s %s", pAdr2, pAdr3);
                  strcpy(sAdr3, pTmp);
               } else if (!memcmp(pAdr3, "CALIF", 5))
                  sprintf(sAdr2, "%s CA", pAdr2);
               else
                  strcpy(sAdr3, pAdr3);
            } else
               strcpy(sAdr3, pAdr3);
            strcpy(sAdr4, pAdr4);
            strcpy(sAdr5, pAdr5);
         }
      }
   }

   // Check for Care Of
   if (sAdr1[0] == '%')
   {
      strcpy(pCareOf, &sAdr1[1]);
      sAdr1[0] = 0;
   } else if (!memcmp(sAdr1, "ATTN", 4))
   {
      strcpy(pCareOf, sAdr1);
      sAdr1[0] = 0;
   } else if (!memcmp(sAdr1, "C/O", 3))
   {
      strcpy(pCareOf, sAdr1);
      sAdr1[0] = 0;
   } else if (!memcmp(sAdr1, "SUITE", 5) || !memcmp(sAdr1, "UNIT ", 5))
   {
      strcpy(acSuite, sAdr1);
      sAdr1[0] = 0;
   }

   if (sAdr5[0])
   {
      p2 = sAdr5;
      p1 = sAdr4;
      p0 = sAdr3;
   } else if (*sAdr4)
   {
      // Check for unit in second addr field
      if (*sAdr3 < '1')
      {
         strcat(pAdr2, " ");
         strcat(pAdr2, pAdr3);
         p0 = pAdr1;
         p1 = pAdr2;
      } else
      {
         p1 = sAdr3;
         p0 = sAdr2;
      }
      p2 = sAdr4;
   } else if (*sAdr3)
   {
      if (!memcmp(sAdr3, "CA , CA", 6) || !memcmp(sAdr3, "008, CA", 6))
      {
         sprintf(sAdr3, "%s CA", sAdr2);
         p2 = sAdr3;
         p1 = sAdr1;
         p0 = NULL;
      } else if (!memcmp(sAdr3, "CA ", 3) && sAdr3[3] == '9')
      {
         strcat(sAdr2, " ");
         strcat(sAdr2, sAdr3);
         p2 = sAdr2;
         p1 = sAdr1;
         p0 = NULL;
      } else if (sAdr1[0] > '0' && sAdr1[0] <= '9' && memcmp(sAdr2, "P O B", 5))
      {
         strcat(sAdr1, " ");
         strcat(sAdr1, sAdr2);
         p0 = NULL;
         p1 = sAdr1;
         p2 = sAdr3;
      } else
      {
         p0 = sAdr1;
         p1 = sAdr2;
         p2 = sAdr3;
      }
   } else
   {
      p2 = sAdr2;
      p1 = sAdr1;
      p0 = NULL;
   }

   // Check for foreign address - DO NOT PARSE
   if (p2 && *p2 > ' ' && (
      (strstr(p2, "CANADA") && memcmp(p2, "LA ", 3)) || 
      !memcmp(p2, "CHINA RPC", strlen(p2)) || strstr(p2, "SWEDEN") ||
      strstr(p2, "MEXICO")  || strstr(p2, "AUSTRALIA") ||
      strstr(p2, "ENGLAND") || strstr(p2, "IRELAND") ||
      strstr(p2, "JAPAN")   || strstr(p2, "KOREA")   || 
      strstr(p2, "SPAIN")   || strstr(p2, "SCOTTLAND") ||
      strstr(p2, "ICELAND") || strstr(p2, "PHILIPPINES") ||
      strstr(p2, "BELGIUM") || strstr(p2, "GERMANY") ||
      strstr(p2, "ITALY")   || strstr(p2, "GREECE") ||
      strstr(p2, "CHILE")   || strstr(p2, "SINGAPORE") ||      
      strstr(p2, "THAILAND")|| (strstr(p2, "EGYPT") && memcmp(p2, "NEW", 3))  || 
      strstr(p2, "ISRAEL")  || strstr(p2, "FRANCE")  || 
      strstr(p2, "BAHRAIN") || strstr(p2, "NETHERLANDS") ||
      strstr(p2, "KUWAIT")  || strstr(p2, "SWITZERLAND") ||
      strstr(p2, "TAIWAN")  || strstr(p2, "PAKISTAN")|| 
      strstr(p2, "FINLAND") || strstr(p2, "MALAYSIA") ||
      strstr(p2, "NEW ZEALAND") || strstr(p2, "BRISTISH COL") || 
      !strcmp(p2, "GREAT BRITIAN")  || !strcmp(p2, "HONG KONG") || 
      !strcmp(p2, "UNITED KINGDOM") || !strcmp(p2, "SAUDI ARABIA") || 
      !strcmp(p2, "SOUTH KOREA")    || !strcmp(p2, "PUERTO RICO") || 
      !strcmp(p2, "COSTA RICA")     || !strcmp(p2, "CHINA") || 
      !strcmp(p2, "NEW GUINEA")     || !strcmp(p2, "SO AFRICA") || 
      !strcmp(p2, "UK") || !strcmp(p2, "MONACO") || !strcmp(p2, "JP") )  )
   {
      if (p0 && *p0 > ' ')
      {
         strcpy(acAddr1, p0);
         strcpy(acAddr2, p1);
         strcat(acAddr2, " ");
      } else
      {
         if (acSuite[0] == 'S')
            sprintf(acAddr1, "%s %s", p1, acSuite);
         else
            strcpy(acAddr1, p1);
         acAddr2[0] = 0;
      }

      replStr(acAddr1, ", ,", " ");
      strcat(acAddr2, p2);
      replStr(acAddr2, ", ,", " ");
      replStr(acAddr2, " , ", ", ");
      blankRem(acAddr2);

      strncpy(pAdrRec->strName, acAddr1, sizeof(pAdrRec->strName));
      pAdrRec->strName[sizeof(pAdrRec->strName)] = 0;
      strncpy(pAdrRec->City, acAddr2, sizeof(pAdrRec->City)+10);
      pAdrRec->City[sizeof(pAdrRec->City)] = 0;
      strcpy(pMAdr1, acAddr1);
      strcpy(pMAdr2, acAddr2);

      return 1;
   } else if (p0 && (!memcmp(p0, "STE", 3) || !memcmp(p0, "SUITE", 5) || !memcmp(p0, "UNIT", 4) || *p0 == '#'
      || strstr(p0, "FLOOR") ))
   {
      sprintf(acAddr1, "%s %s", p1, p0);
      strcpy(acAddr2, p2);
   } else if (p0 && isdigit(*p0) && isdigit(*p1))
   {  // 89037264
      if (!strstr(p0, "BUILDING"))
         sprintf(acAddr1, "%s %s", p0, p1);
      else
         strcpy(acAddr1, p1);
      strcpy(acAddr2, p2);
   } else if (p0 && !memcmp(p0+strlen(p0)-3, "STE", 3))
   {
      sprintf(acAddr1, "%s %s", p0, p1);
      strcpy(acAddr2, p2);
   } else if (!memcmp(p2, "008", 3))
   {
      strcpy(acAddr1, p0);
      sprintf(acAddr2, "%s CA", p1);
   } else
   {
      strcpy(acAddr2, p2);
      if (isdigit(*p2))
      {
         if ((iCnt = countWord(p2)) == 1)
         {
            sprintf(acAddr1, "%s %s", p1, p2);
            acAddr2[0] = 0;
         } else if (!memcmp(p2+1, "EST", 3))
         {
            strcpy(acAddr1, p1);
            acAddr2[0] = 'W';
         } else if (!memcmp(p2, "0RA", 3))
         {
            strcpy(acAddr1, p1);
            acAddr2[0] = 'O';
         } else if (*p2 == '7')            // 7 HILLS, OH
            strcpy(acAddr1, p1);
         else 
         {
            if (*(p2+1) >= 'A')
            {
               if (*p1 < 'A')
                  sprintf(acAddr1, "%s %s", p1, p2);
               else
                  strcpy(acAddr1, p2);
            } else 
            {
               strcpy(acAddr1, p2);
            }
            acAddr2[0] = 0;
            iRet = 2;
         }
      } else
         strcpy(acAddr1, p1);
   }

   // Fix known bad chars
   replChar(acAddr1, 0xC2, 'P');
   replChar(acAddr1, 0xA2, 'C');
   remCharEx(acAddr1, ".,");
   remChar(acAddr2, '.');
   replStr(acAddr2, ", ,", " ");
   replStr(acAddr2, ",,", " ");
   blankRem(acAddr2);

   // Check for bad character
   iCnt = replUnPrtChar(acAddr1);
   if (iCnt > 0)
      iRet = -1;
   else
      iRet = 0;

   // Remove extra blank
   blankRem(acAddr1);

   // Save MAdr1 and MAdr2
   pTmp = strstr(acAddr1, "PO BOX");
   if (pTmp)
      strcpy(acAddr1, pTmp);
   else if (pTmp = strstr(acAddr1, "P O BOX"))
      strcpy(acAddr1, pTmp);

   if (acAddr1[0] >= 'A' && acAddr2[0] < '0')
   {
      strcpy(acAddr2, acAddr1);
      acAddr1[0] = 0;
   }

   // Parse address
   if (acAddr1[0] > ' ')
   {
      //if ((pTmp = strstr(acAddr1, " SUITE ")) || (pTmp = strstr(acAddr1, " STE ")) || (pTmp = strstr(acAddr1, " UNIT ")) || (pTmp = strstr(acAddr1, " APT ")))
      //{
      //   *pTmp++ = 0;
      //   strcpy(acSuite, pTmp);
      //}
      parseAdr1(pAdrRec, acAddr1);
      if (acSuite[0])
         sprintf(pMAdr1, "%s %s", acAddr1, acSuite);
      else
         strcpy(pMAdr1, acAddr1);
   }
   
   if (acAddr2[0] > ' ')
   {
      strcpy(pMAdr2, acAddr2);
      parseAdr2_1(pAdrRec, acAddr2);
   } else
      iRet = 3;

   return iRet;
}

/********************************* Org_UpdSitus ******************************
 *
 * Update situs ATTOM data
 * 
 * Return 0 if successful, >0 if error
 *
 *****************************************************************************/

int Org_UpdSitus(char *pOutbuf)
{
   static   char acRec[MAX_RECSIZE], *pRec=NULL;
   char     acTmp[256], acAddr1[128], acStrName[64], *pTmp;
   int      iLoop, iCode, iTmp;
   ADR_REC  sSitusAdr;

   // Get first Char rec for first call
   if (!pRec)
   {
      // Skip header
      pRec = fgets(acRec, MAX_RECSIZE, fdSitus);
      pRec = fgets(acRec, MAX_RECSIZE, fdSitus);
   }

   do
   {
      if (!pRec)
      {
         fclose(fdSitus);
         fdSitus = NULL;
         return 1;      // EOF
      }

      // Compare APN
      iLoop = memcmp(pOutbuf, pRec, iApnLen);
      if (iLoop > 0)
      {
         pRec = fgets(acRec, MAX_RECSIZE, fdSitus);
      }
   } while (iLoop > 0);

   // If not match, return
   if (iLoop)
      return 1;

   iTokens = ParseStringIQ(acRec, '|', MAX_FLD_TOKEN, apTokens);
   if (iTokens < NR_S_ZIP4)
   {
      LogMsg("***** Error: bad situs record for APN=%.*s (#tokens=%d)", iApnLen, acRec, iTokens);
      return -1;
   }

   memset((void *)&sSitusAdr, 0, sizeof(ADR_REC));
   strcpy(acAddr1, apTokens[NR_S_FULL_ADDR]);
   replStr(acAddr1, "# ", "#");
   replStr(acAddr1, "LE PRT", "LE PORT");

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "65634211", 8))
   //   iLoop = 0;
#endif

   // Special case
   if (!strcmp(apTokens[NR_S_STREET_NAME], "LE") && !strcmp(apTokens[NR_S_SUF], "PRT"))
   {
      strcpy(acStrName, "LE PORT");
      *apTokens[NR_S_SUF] = 0;
   } else
      strcpy(acStrName, apTokens[NR_S_STREET_NAME]);

   sSitusAdr.lStrNum = atol(apTokens[NR_S_HOUSE_NBR]);
   if (sSitusAdr.lStrNum > 0)
   {
      removeSitus(pOutbuf);
      vmemcpy(pOutbuf+OFF_S_ADDR_D, acAddr1, SIZ_S_ADDR_D);
      sprintf(sSitusAdr.strNum, "%d", sSitusAdr.lStrNum);
      vmemcpy(pOutbuf+OFF_S_STRNUM, sSitusAdr.strNum, SIZ_S_STRNUM);

      if (pTmp = strchr(apTokens[NR_S_HOUSE_NBR], ' '))
      {
         if (*(pTmp+2) == '/' && isdigit(*(pTmp+3) ))
         {
            vmemcpy(pOutbuf+OFF_S_STR_SUB, pTmp+1, SIZ_S_STR_SUB);
            vmemcpy(pOutbuf+OFF_S_HSENO, sSitusAdr.strNum, SIZ_S_HSENO);
         } else
            vmemcpy(pOutbuf+OFF_S_HSENO, apTokens[NR_S_HOUSE_NBR], SIZ_S_HSENO);
      } else
         vmemcpy(pOutbuf+OFF_S_HSENO, apTokens[NR_S_HOUSE_NBR], SIZ_S_HSENO);

      vmemcpy(pOutbuf+OFF_S_DIR, apTokens[NR_S_DIR], SIZ_S_DIR);
      vmemcpy(pOutbuf+OFF_S_STREET, acStrName, SIZ_S_STREET);
      vmemcpy(pOutbuf+OFF_S_UNITNO, apTokens[NR_S_UNIT_VAL], SIZ_S_UNITNO);
      vmemcpy(pOutbuf+OFF_S_UNITNOX, apTokens[NR_S_UNIT_VAL], SIZ_S_UNITNOX);

      if (*apTokens[NR_S_SUF] >= 'A' && (iCode = GetSfxCode(apTokens[NR_S_SUF])))
      {
         iTmp = sprintf(acTmp, "%d", iCode);
         memcpy(pOutbuf+OFF_S_SUFF, acTmp, iTmp);
      }

      lUpdSitus++;
   }

   // Default state
   memcpy(pOutbuf+OFF_S_ST, "CA", 2);

   // City
   if (*apTokens[NR_S_CITY] > ' ')
   {
      char  *pCity;
      
      iCode = City2Code(apTokens[NR_S_CITY], acTmp, pOutbuf);
      if (iCode > 0)
      {
         memcpy(pOutbuf+OFF_S_CITY, acTmp, 5);
         pCity = Code2City(iCode);
         if (!memcmp(apTokens[NR_S_CITY], pOutbuf+OFF_M_CITY, strlen(apTokens[NR_S_CITY])))
         {
            memset(pOutbuf+OFF_M_CITY, ' ', SIZ_M_CITY);
            vmemcpy(pOutbuf+OFF_M_CITY, pCity, SIZ_M_CITY);
         }
      } else
         pCity = apTokens[NR_S_CITY];

      if (*apTokens[NR_S_ZIP] == '9')
      {
         vmemcpy(pOutbuf+OFF_S_ZIP, apTokens[NR_S_ZIP], SIZ_S_ZIP);
         sprintf(acAddr1, "%s, CA %s", pCity, apTokens[NR_S_ZIP]);      
      }
      vmemcpy(pOutbuf+OFF_S_CTY_ST_D, acAddr1, SIZ_S_CTY_ST_D);
   }

   return 0;
}

/******************************** Org_MergeMAdr ******************************
 *
 * Return 0 if successful, >0 if error
 *
 *****************************************************************************/

void Org_MergeMAdr(char *pOutbuf)
{
   char     acAddr1[128], acAddr2[64];
   char     acCareOf[64], adr1[64], adr2[64], adr3[64], adr4[64], adr5[64];
   int      iTmp, iTmp1;
   ADR_REC  sMailAdr;

   // Clear old Mailing
   removeMailing(pOutbuf, false);
   memset((void *)&sMailAdr, 0, sizeof(ADR_REC));

   if (*apTokens[ORG_ROLL_NAD_J] && !strstr(apTokens[ORG_ROLL_NAD_J], "ADDRESS"))
   {
      strcpy(adr1, apTokens[ORG_ROLL_NAD_J]);
      strcpy(adr2, apTokens[ORG_ROLL_NAD_K]);
      strcpy(adr3, apTokens[ORG_ROLL_NAD_L]);
      strcpy(adr4, apTokens[ORG_ROLL_NAD_M]);
      strcpy(adr5, apTokens[ORG_ROLL_NAD_N]);
      blankRem(adr1);
      blankRem(adr2);
      blankRem(adr3);
      blankRem(adr4);
      blankRem(adr5);
      acCareOf[0] = 0;
      acAddr1[0] = 0;
      acAddr2[0] = 0;

#ifdef _DEBUG
      // 00315247, 00226129, 00312226, 03115018, 03429202, 03925129, 04502213, 04829206, 00101108, 05723425, 12330313, 13163339
      // 10458320 (one word city name, no state)
      // 13527130, 16713208, 25353105, 30807138, 30815139, 37224131, 05713303
      //if (lLDRRecCount > 390611)
      //   iTmp = 0;
      //if (!memcmp(pOutbuf, "01917123", 8))
      //   iTmp = 0;
#endif
      iTmp = Org_ParseMAdr(&sMailAdr, acCareOf, acAddr1, acAddr2, adr1, adr2, adr3, adr4, adr5);
      if (bDebug)
      {
         if (iTmp == 1)
            LogMsg0("==> Foreign adr: %s|%s --> %.10s", acAddr1, acAddr2, pOutbuf);
         else if (iTmp >= 2)
            LogMsg0("--> Bad adr: %s|%s --> %.10s", acAddr1, acAddr2, pOutbuf);
         else if (iTmp < 0)
            LogMsg0("*** Bad char in M_Addr --> %.10s", pOutbuf);
      }
      vmemcpy(pOutbuf+OFF_M_STRNUM, sMailAdr.strNum, SIZ_M_STRNUM);
      vmemcpy(pOutbuf+OFF_M_STR_SUB, sMailAdr.strSub, SIZ_M_STR_SUB);
      vmemcpy(pOutbuf+OFF_M_DIR, sMailAdr.strDir, SIZ_M_DIR);
      vmemcpy(pOutbuf+OFF_M_STREET, sMailAdr.strName, SIZ_M_STREET);
      vmemcpy(pOutbuf+OFF_M_SUFF, sMailAdr.strSfx, SIZ_M_SUFF);
      vmemcpy(pOutbuf+OFF_M_UNITNO, sMailAdr.Unit, SIZ_M_UNITNO);
      vmemcpy(pOutbuf+OFF_M_UNITNOX, sMailAdr.UnitNox, SIZ_M_UNITNOX);
      vmemcpy(pOutbuf+OFF_M_CITY, sMailAdr.City, SIZ_M_CITY);
      if (sMailAdr.State[0] >= 'A' && sMailAdr.State[1] >= 'A')
         memcpy(pOutbuf+OFF_M_ST, sMailAdr.State, SIZ_M_ST);

      iTmp = strlen(apTokens[ORG_ROLL_M_ZIP]);
      iTmp1 = atoi(apTokens[ORG_ROLL_M_ZIP]);
      if (iTmp >= SIZ_M_ZIP && iTmp1 > 0 && *(apTokens[ORG_ROLL_M_ZIP]+4) != '-')
      {
         strcat(acAddr2, " ");
         strcat(acAddr2, apTokens[ORG_ROLL_M_ZIP]);
         memcpy(pOutbuf+OFF_M_ZIP, apTokens[ORG_ROLL_M_ZIP], SIZ_M_ZIP);
         if (iTmp == 10)
            memcpy(pOutbuf+OFF_M_ZIP4, apTokens[ORG_ROLL_M_ZIP]+6, SIZ_M_ZIP4);
      }

      vmemcpy(pOutbuf+OFF_M_ADDR_D, acAddr1, SIZ_M_ADDR_D);
      vmemcpy(pOutbuf+OFF_M_CTY_ST_D, acAddr2, SIZ_M_CTY_ST_D);

      // Check for C/O
      if (*(pOutbuf+OFF_CARE_OF) == ' ' && acCareOf[0])
      {
         iTmp = strlen(acCareOf);
         remUChar((unsigned char *)&acCareOf[0], 0xC2, iTmp);
         remUChar((unsigned char *)&acCareOf[0], 0xA2, iTmp);

         updateCareOf(pOutbuf, acCareOf, iTmp);
      }
   } 
}

/********************************** Org_MergeRoll *****************************
 *
 * Return 0 if successful
 * NAD Info:
 *    B, C, D     : OWNER NAME CONTINUATION, ADDITIONAL OWNERS
 *    E, F, G, H  : NEW OWNER NAME; DEED RECORDED AFTER 1 JANUARY
 *    J, K, L, M,N: CURRENT OWNER MAILING ADDRESS
 *    S           : SITUS STREET ADDRESS
 *    T           : SITUS ADDRESS SUFFIX, CITY NAME CODE
 *    L1-L5       : STANDARD LEGAL DESCRIPTION (Tract Block Lot Unit)
 *                  AND/OR EXTENDED LEGAL DESCRIPTION IF NECESSARY
 *
 *****************************************************************************/

int Org_MergeRoll(char *pOutbuf, char *pRollRec, int iLen, int iFlag)
{
   char     acTmp[512], acTmp1[256], *pBuf;
   LONGLONG lTmp;
   int      iRet, iTmp;

   // Replace null char and double quote with single quote
   pBuf = pRollRec;
   for (iTmp = 0; iTmp < iLen; iTmp++, pBuf++)
   {
      if (*pBuf == 0)
         *pBuf = ' ';
      else if (*pBuf == 34)
         *pBuf = 39;
      else if (*pBuf == 10)
         break;
   }

   // Parse input record
   iRet = ParseStringIQ(pRollRec, '|', MAX_FLD_TOKEN, apTokens);
   if (iRet < ORG_ROLL_MISCMSG2)
   {
      LogMsg("***** Error: bad input record for APN=%.10s", pRollRec);
      return -1;
   }

#ifdef _DEBUG
   //if (!memcmp(apTokens[ORG_ROLL_APN], "89037264", 8))
   //   iTmp = 0;
#endif
   if (iFlag & CREATE_R01)
   {
      // Clear output buffer
      memset(pOutbuf, ' ', iRecLen);

      // Start copying data
      memcpy(pOutbuf, apTokens[ORG_ROLL_APN], strlen(apTokens[ORG_ROLL_APN]));

      // County code and Parcel Status
      memcpy(pOutbuf+OFF_CO_NUM, "30ORG", 5);

      // Year assessed
      memcpy(pOutbuf+OFF_YR_ASSD, myCounty.acYearAssd, 4);

      // Format APN
      iRet = formatApn(apTokens[ORG_ROLL_APN], acTmp, &myCounty);
      memcpy(pOutbuf+OFF_APN_D, acTmp, iRet);

      // Merge Maplink
      iRet = formatMapLink(apTokens[ORG_ROLL_APN], acTmp, &myCounty);
      memcpy(pOutbuf+OFF_MAPLINK, acTmp, iRet);

      // Create index map link
      if (getIndexPage(acTmp, acTmp1, &myCounty))
         memcpy(pOutbuf+OFF_IMAPLINK, acTmp1, iRet);

      // Land
      long lLand = atoi(apTokens[ORG_ROLL_LAND]);
      if (lLand > 0)
      {
         sprintf(acTmp, "%*u", SIZ_LAND, lLand);
         memcpy(pOutbuf+OFF_LAND, acTmp, SIZ_LAND);
      }

      // Improve
      long lImpr = atoi(apTokens[ORG_ROLL_IMPR]);
      if (lImpr > 0)
      {
         sprintf(acTmp, "%*u", SIZ_IMPR, lImpr);
         memcpy(pOutbuf+OFF_IMPR, acTmp, SIZ_IMPR);
      }

      // Other value: TradeFixture, LivingImp, PPDeclared, PPUnitVal, PPMH
      long lOthers = atoi(apTokens[ORG_ROLL_OTHERIMPR]);
      long lPers = atoi(apTokens[ORG_ROLL_PP_VAL]);
      lTmp = lPers + lOthers;
      if (lTmp > 0)
      {
         sprintf(acTmp, "%*u", SIZ_OTHER, lTmp);
         memcpy(pOutbuf+OFF_OTHER, acTmp, SIZ_OTHER);

         if (lOthers > 0)
         {
            sprintf(acTmp, "%u         ", lOthers);
            memcpy(pOutbuf+OFF_OTH_IMPR, acTmp, SIZ_OTH_IMPR);
         }
         if (lPers > 0)
         {
            sprintf(acTmp, "%u         ", lPers);
            memcpy(pOutbuf+OFF_PERSPROP, acTmp, SIZ_PERSPROP);
         }
      }

      // Gross total
      LONGLONG lGross = lTmp+lLand+lImpr;
      if (lGross > 0)
      {
         sprintf(acTmp, "%*u", SIZ_GROSS, lGross);
         memcpy(pOutbuf+OFF_GROSS, acTmp, SIZ_GROSS);
      }

      // Ratio
      if (lImpr > 0)
      {
         sprintf(acTmp, "%*u", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
         memcpy(pOutbuf+OFF_RATIO, acTmp, SIZ_RATIO);
      }

      // HO Exempt
      if (*apTokens[ORG_ROLL_EXE1_CD] == '7')
         *(pOutbuf+OFF_HO_FL) = '1';      // 'Y'
      else
         *(pOutbuf+OFF_HO_FL) = '2';      // 'N'

      lTmp = atoi(apTokens[ORG_ROLL_EXE1_AMT]);
      if (lTmp > 0)
      {
         lTmp += atoi(apTokens[ORG_ROLL_EXE2_AMT]);

         if (lTmp > lGross)
         {
            LogMsg("* Overwrite EXE_TOTAL of %u with %u", lTmp, lGross);
            lTmp = lGross;
         }
         sprintf(acTmp, "%*u", SIZ_EXE_TOTAL, lTmp);
         memcpy(pOutbuf+OFF_EXE_TOTAL, acTmp, SIZ_EXE_TOTAL);
      }

      // Exemption code
      if (*apTokens[ORG_ROLL_EXE1_CD] > ' ')
      {
         *(pOutbuf+OFF_EXE_CD1) = *apTokens[ORG_ROLL_EXE1_CD];
         if (*apTokens[ORG_ROLL_EXE2_CD] > ' ')
            *(pOutbuf+OFF_EXE_CD2) = *apTokens[ORG_ROLL_EXE2_CD];
      }

      // Create exemption type
      makeExeType(pOutbuf+OFF_EXE_TYPE, pOutbuf+OFF_EXE_CD1, pOutbuf+OFF_EXE_CD2, pOutbuf+OFF_EXE_CD3, (IDX_TBL4 *)&ORG_Exemption);

      // Full Exempt
      if (*apTokens[ORG_ROLL_EXE1_CD] == 'X')
         *(pOutbuf+OFF_FULL_EXEMPT) = 'Y';

      // Prop8
      //if (pRec->LandKey == '8')
      //   *(pOutbuf+OFF_PROP8_FLG) = 'Y';
   }

   // AG preserved

   // TRA
   lTmp = atol(apTokens[ORG_ROLL_TRA]);
   if (lTmp > 0)
   {
      iRet = sprintf(acTmp, "%.6d", lTmp);
      memcpy(pOutbuf+OFF_TRA, acTmp, iRet);
   }

   // Owner
   iRet = 0;
   try {
      Org_MergeOwner(pOutbuf);
   } catch(...)
   {
      LogMsg("***** Exception in MergeOwner at %.10s", pOutbuf);
      iRet = -1;
   }

   // Mailing
   try {
      Org_MergeMAdr(pOutbuf);
   } catch(...)
   {
      LogMsg("***** Exception in MergeAdr at %.10s", pOutbuf);
      iRet = -1;
   }

   // UseCode
   if (*apTokens[ORG_ROLL_PROPERTYTYPE] > ' ')
   {
      *(pOutbuf+OFF_USE_CO) = *apTokens[ORG_ROLL_PROPERTYTYPE];
      iTmp = atoi(apTokens[ORG_ROLL_PROPERTYTYPE]);
      if (iTmp < 9 && iTmp >= 0)
         memcpy(pOutbuf+OFF_USE_STD, asPropType[iTmp], 3);
      else
      {
         logUsecode(apTokens[ORG_ROLL_PROPERTYTYPE], pOutbuf);
         memcpy(pOutbuf+OFF_USE_STD, USE_MISC, SIZ_USE_STD);
      }
   } else
      memcpy(pOutbuf+OFF_USE_STD, USE_UNASGN, SIZ_USE_STD);

   // Acres

   // Recorded Doc
   //if (*apTokens[ORG_ROLL_DOCNUM] > ' ')
   //{
   //   iTmp = strlen(apTokens[ORG_ROLL_DOCNUM]);
   //   if (iTmp > SIZ_TRANSFER_DOC)
   //      iTmp = SIZ_TRANSFER_DOC;
   //   memcpy(pOutbuf+OFF_TRANSFER_DOC, apTokens[ORG_ROLL_DOCNUM], iTmp);
   //}

   // Legal
   sprintf(acTmp, "%s %s %s %s %s", apTokens[ORG_ROLL_NAD_L1], apTokens[ORG_ROLL_NAD_L2],
      apTokens[ORG_ROLL_NAD_L3], apTokens[ORG_ROLL_NAD_L4], apTokens[ORG_ROLL_NAD_L5]);

   // Fix known bad chars
   //if (strchr(acTmp, 0xC2))
   //   iTmp = 0;
   replChar(acTmp, 0xC2, '-');
   replChar(acTmp, 0xAC, '-');
   replStr(acTmp, "--A", "PAR");

   // Check for bad character
   if (replUnPrtChar(acTmp))
      LogMsg0("*** Bad character in Legal at %.12s", pOutbuf);

   //blankRem(acTmp);
   iTmp = updateLegal(pOutbuf, acTmp);
   if (iTmp > iMaxLegal)
      iMaxLegal = iTmp;

   // Update Tract/Lot info
   UpdateBlkLot(pOutbuf);

   return iRet;
}

/********************************* Org_DocMatch ******************************
 *
 * Populating GrGr record with DocDate and DocType from Document.txt
 *
 * Return 0 if successful, >0 if error
 *
 *****************************************************************************/

int Org_DocMatch(ORG_GRGR *pOrgGrGr, int iFldCnts, bool initFlg=false)
{
   static char acRec[1024], *aFlds[24], *pRec=NULL;
   static int  iNameIdx, iDocIdx, iDateIdx, iCodeIdx, iTitleIdx;

   int    iRet, iTmp, iCnt;
   char   *pTmp;

   if (initFlg)
   {
      pRec=NULL;
      return 0;
   }

   if (!pRec)
   {
      do
      {
         pRec = fgets(acRec, 1024, fdDoc);
         if (!pRec)
         {
            LogMsg0("***** Error in Org_DocMatch(): empty or bad input file.");
            fclose(fdDoc);
            fdDoc = NULL;
            return -1;
         }
      } while (pRec && (*pRec != 34 && !isdigit(*pRec)));   // Check for double quote

      // Use this to determine input record
      pTmp = pRec;
      iRet = strlen(pRec);

      iCnt = ParseStringNQ(pRec, ',', 24, aFlds);
      if (iCnt != iFldCnts)
      {
         LogMsg0("***** Error: bad input record for DocKey=%.12s", aFlds[ORG_DOC_DOCKEY]);
         fclose(fdDoc);
         fdDoc = NULL;
         return -1;
      }

      if (iCnt > ORG_DOC_NAMEFLG)
      {         
         if (strlen(aFlds[ORG_DOC_DOCDATE]) == 8)
         {
            iDocIdx  = ORG_DOC_DOCNUM;
            iDateIdx = ORG_DOC_DOCDATE;
            iTitleIdx= ORG_DOC_DOCTITLE;
            iCodeIdx = ORG_DOC_GCODE;
            iNameIdx = ORG_DOC_NAME1;
         } else
         {
            iDocIdx  = ORG_DOC2_DOCNUM;
            iDateIdx = ORG_DOC2_DOCDATE;
            iTitleIdx= ORG_DOC2_DOCTITLE;
            iCodeIdx = ORG_DOC2_GCODE;
            iNameIdx = ORG_DOC2_NAME1;
         }
      } else
      {
         iDocIdx  = ORG_DOC1_DOCNUM;
         iDateIdx = ORG_DOC1_DOCDATE;
         iTitleIdx= ORG_DOC1_DOCTITLE;
         iCodeIdx = ORG_DOC1_GCODE;
         iNameIdx = ORG_DOC1_NAME1;
      }
      // testing
      iTmp = iNameIdx;
   }

   do
   {
      iTmp = memcmp(pOrgGrGr->DocNum, aFlds[iDocIdx], SIZ_OGR_DOCKEY);
      if (iTmp > 0)
      {
         pRec = fgets(acRec, 512, fdDoc);
         if (!pRec)
         {
            fclose(fdDoc);
            fdDoc = NULL;
            return 1;      // EOF
         }

         pTmp = pRec;
         iTmp = ParseStringNQ(pRec, ',', 24, aFlds);
         if (iTmp < ORG_DOC_NAMEFLG)
         {
            LogMsg0("***** Error: bad input record for DocKey=%.12s", aFlds[ORG_DOC_DOCKEY]);
            fclose(fdDoc);
            fdDoc = NULL;
            return -1;      // EOF
         }
      }
   } while (iTmp > 0);

   // If not match, return
   if (iTmp)
   {
      LogMsg0("   No matched document: %.12s", pOrgGrGr->DocNum);
      return 1;
   }

   // DocDate & DocTitle
   memcpy(pOrgGrGr->DocDate, aFlds[iDateIdx], strlen(aFlds[iDateIdx]));
   memcpy(pOrgGrGr->DocTitle, aFlds[iTitleIdx], strlen(aFlds[iTitleIdx]));
   if (!memcmp(pOrgGrGr->DocTitle, "GRANT DEED", 7) || 
         !memcmp(pOrgGrGr->DocTitle, "DEED   ", 7) ||
         !memcmp(pOrgGrGr->DocTitle, "TRUSTEES DEED", 10))
   {
      pOrgGrGr->NoneSale_Flg = 'N';
      pOrgGrGr->IsXfer_Flg = 'Y';
   } else
   {
      pOrgGrGr->NoneSale_Flg = 'Y';
      if (!memcmp(pOrgGrGr->DocTitle, "AFDVT SUCCSR TR", 10) ||
            !memcmp(pOrgGrGr->DocTitle, "AFFIDAVIT", 8) ||
            !memcmp(pOrgGrGr->DocTitle, "QUITCLAIM", 8) ||
            !memcmp(pOrgGrGr->DocTitle, "ASSIGNMENT LSE  ", 12) )
         pOrgGrGr->IsXfer_Flg = 'Y';
      else
         pOrgGrGr->IsXfer_Flg = 'N';
   }

   // Grantors & Grantees
   int iGrantor = 0;
   int iGrantee = 0;
   memset((char *)&pOrgGrGr->Grantee[0][0], ' ', SIZ_OGR_NAME*2);
   while (!feof(fdDoc))
   {
      iRet = atoi(aFlds[iCodeIdx]);
      if (iRet == 1)
      {  // Grantor
         if (iGrantor == 0)
            vmemcpy(pOrgGrGr->Grantor[iGrantor++], aFlds[iNameIdx], SIZ_OGR_NAME, strlen(aFlds[iNameIdx]));
         else if (iGrantor == 1 && memcmp(pOrgGrGr->Grantor[0], aFlds[iNameIdx], strlen(aFlds[iNameIdx])) )
            vmemcpy(pOrgGrGr->Grantor[iGrantor++], aFlds[iNameIdx], SIZ_OGR_NAME, strlen(aFlds[iNameIdx]));
      } else if (iRet == 2)
      {  // Grantee
         if (iGrantee == 2)
            pOrgGrGr->MoreName = 'Y';
         else if (iGrantee == 0)
            vmemcpy(pOrgGrGr->Grantee[iGrantee++], aFlds[iNameIdx], SIZ_OGR_NAME, strlen(aFlds[iNameIdx]));
         else if (iGrantee == 1 && memcmp(pOrgGrGr->Grantee[0], aFlds[iNameIdx], strlen(aFlds[iNameIdx])))
            vmemcpy(pOrgGrGr->Grantee[iGrantee++], aFlds[iNameIdx], SIZ_OGR_NAME, strlen(aFlds[iNameIdx]));
      }

      pRec = fgets(acRec, 1024, fdDoc);
      if (!pRec)
      {
         fclose(fdDoc);
         fdDoc = NULL;
         break;      // EOF
      }

      pTmp = pRec;
      iTmp = ParseStringNQ(pRec, ',', 24, aFlds);
      if (iTmp < ORG_DOC_NAMEFLG)
      {
         LogMsg0("***** Error: bad input record for DocKey=%.12s", aFlds[ORG_DOC_DOCKEY]);
         fclose(fdDoc);
         fdDoc = NULL;
         return -1;      // EOF
      }

      if (memcmp(pOrgGrGr->DocNum, aFlds[iDocIdx], SIZ_OGR_DOCKEY))
         break;
   }

   return 0;
}

/****************************** Org_CreateGrGrRec ****************************
 *
 * Create ORG_GrGr from DEED.TXT record
 *
 * Return 0 if successful, >0 if error
 *
 *****************************************************************************/

int Org_CreateGrGrRec(ORG_GRGR *pOrgGrGr, char *pGrGrRec, int iCnt /* entry number for same document */)
{
   char     acTmp[256];
   ULONG    lTmp;
   double   dTmp;
   int      iTmp, iAddr1;

   // Only accept up to 2 names (buyer) for each sale.  Ignore the rest.
   if (iCnt < 1 || iCnt > 2)
      return 0;

   // Parse input record
   iTmp = ParseStringNQ(pGrGrRec, ',', MAX_FLD_TOKEN, apTokens);
   if (iTmp < ORG_DEED_NDTAX)
   {
      LogMsg("***** Error: bad input record for DocKey=%.12s", apTokens[ORG_DEED_DOCKEY]);
      return -1;
   }

   if (*apTokens[ORG_DEED_PARCEL] < '0')
      return 1;

   if (iTmp == ORG_DEED_NDTAX)
      iAddr1 = ORG_DEED1_ADDR1;
   else 
      iAddr1 = ORG_DEED_ADDR1;

   if (iCnt == 1)
   {
      // Clear output buffer on first entry
      memset((void *)pOrgGrGr, ' ', sizeof(ORG_GRGR));
      memcpy(pOrgGrGr->DocNum, apTokens[ORG_DEED_DOCNUM], strlen(apTokens[ORG_DEED_DOCNUM]));
      memcpy(pOrgGrGr->DocKey, apTokens[ORG_DEED_DOCKEY], strlen(apTokens[ORG_DEED_DOCKEY]));

      dTmp = atof(apTokens[ORG_DEED_DOCTAX]);
      if (dTmp > 0.0)
      {
         memcpy(pOrgGrGr->DocTax, apTokens[ORG_DEED_DOCTAX], strlen(apTokens[ORG_DEED_DOCTAX]));
         lTmp = (ULONG)((dTmp*100)/0.11);
         sprintf(acTmp, "%*u", SIZ_GR_SALE, lTmp);
         memcpy(pOrgGrGr->SalePrice, acTmp, SIZ_GR_SALE);

         if (dTmp > 200000.0)
            LogMsg("***** DocTax is too big %.2f APN=%s.  Please verify", dTmp, apTokens[ORG_DEED_PARCEL]);
      }

      if ((iTmp = strlen(apTokens[ORG_DEED_DOCCODE])) > 0)
         memcpy(pOrgGrGr->DocCode, apTokens[ORG_DEED_DOCCODE], iTmp);

      // Remove space and hyphen from APN
      remCharEx(apTokens[ORG_DEED_PARCEL], " -");
      if ((iTmp = strlen(apTokens[ORG_DEED_PARCEL])) > 0)
      {
         if (iTmp < myCounty.iApnLen)
         {
            memcpy(pOrgGrGr->ParcelNo, apTokens[ORG_DEED_PARCEL], iTmp);
            LogMsg0("APN? %s", apTokens[ORG_DEED_PARCEL]);
         } else if (iTmp > myCounty.iApnLen)
         {
            // Check for condo 999-99-999
            if (apTokens[ORG_DEED_PARCEL][0] == '9' && apTokens[ORG_DEED_PARCEL][3] == '0')
               sprintf(acTmp, "%.3s%.5s", apTokens[ORG_DEED_PARCEL], apTokens[ORG_DEED_PARCEL]+4);
            else if (apTokens[ORG_DEED_PARCEL][0] < '9' && apTokens[ORG_DEED_PARCEL][6] == '0')
               sprintf(acTmp, "%.6s%.2s", apTokens[ORG_DEED_PARCEL], apTokens[ORG_DEED_PARCEL]+7);
            else
            {
               strcpy(acTmp, apTokens[ORG_DEED_PARCEL]);
               LogMsg0("APN? %s", apTokens[ORG_DEED_PARCEL]);
            }
            memcpy(pOrgGrGr->ParcelNo, acTmp, strlen(acTmp));
         } else
            memcpy(pOrgGrGr->ParcelNo, apTokens[ORG_DEED_PARCEL], iTmp);
      } 

#ifdef _DEBUG
      //if (!memcmp(pOrgGrGr->ParcelNo, "07241302", 8))
      //   LogMsg("Found listed parcel %.8s", pOrgGrGr->ParcelNo);
#endif

      if ((iTmp = strlen(apTokens[iAddr1])) > 0)
         memcpy(pOrgGrGr->Addr1,_strupr(apTokens[iAddr1]), iTmp);

      if ((iTmp = strlen(myTrim(apTokens[iAddr1+1]))) > 0)
         memcpy(pOrgGrGr->City,_strupr(apTokens[iAddr1+1]), iTmp);

      if ((iTmp = strlen(apTokens[iAddr1+2])) > 0)
         memcpy(pOrgGrGr->State, apTokens[iAddr1+2], iTmp);

      if ((iTmp = strlen(apTokens[iAddr1+3])) > 0)
         memcpy(pOrgGrGr->Zip, apTokens[iAddr1+3], iTmp);

      if ((iTmp = strlen(apTokens[iAddr1+4])) > 0)
         memcpy(pOrgGrGr->Lot, apTokens[iAddr1+4], iTmp);

      if ((iTmp = strlen(apTokens[iAddr1+5])) > 0)
         memcpy(pOrgGrGr->Unit, apTokens[iAddr1+5], iTmp);

      if ((iTmp = strlen(apTokens[iAddr1+6])) > 0)
         memcpy(pOrgGrGr->Tract, apTokens[iAddr1+6], iTmp);

      if ((iTmp = strlen(apTokens[iAddr1+7])) > 0)
         memcpy(pOrgGrGr->Block, apTokens[iAddr1+7], iTmp);
   }

   // Populate grantee
   if (iAddr1 == ORG_DEED_ADDR1) 
   {
      iTmp = strlen(myTrim(apTokens[ORG_DEED_NAME]));
      if (iTmp > 0)
         memcpy(pOrgGrGr->Grantee[iCnt-1],_strupr(apTokens[ORG_DEED_NAME]), iTmp);
   }
   return 0;
}

/********************************** Org_FixGrGr *******************************
 *
 * Remove hyphen in APN
 *
 ******************************************************************************/

int Org_FixGrGr(char *pCnty, int iFixOpts)
{
   char     acBuf[1024], acTmp[32], acFmtApn[32];
   char     acGrGrIn[_MAX_PATH], acGrGrOut[_MAX_PATH], *pTmp;

   FILE     *fdIn, *fdOut;
   int      iRet, iBook, iPage, iLot;
   long     lCnt=0;
   ORG_GRGR *pGrGrRec = (ORG_GRGR *)acBuf;

   // Setup file names
   sprintf(acGrGrIn, acGrGrTmpl, pCnty, pCnty, "SLS");
   sprintf(acGrGrOut, acGrGrTmpl, pCnty, pCnty, "NEW");

   if (_access(acGrGrIn, 0))
   {
      LogMsg("***** Missing input file: %s", acGrGrIn);
      return -1;
   }

   if (iFixOpts & PQ_FIX_APN)
      LogMsg("Fix %s file for %s.  Removing hyphen in APN", acGrGrIn, pCnty);
   else if (iFixOpts & PQ_FIX_SALEPRICE)
      LogMsg("Fix %s file for %s.  Fix sale price", acGrGrIn, pCnty);
   else if (iFixOpts & PQ_FIX_NSALEFLG)
      LogMsg("Fix %s file for %s.  Fix NoneSale_Flg", acGrGrIn, pCnty);
   else
      return 1;

   // Open input file
   fdIn = fopen(acGrGrIn, "r");
   if (!fdIn)
   {
      LogMsg("***** Error opening input file: %s [%d]", acGrGrIn, _errno);
      return -2;
   }

   // Open Output file
   fdOut = fopen(acGrGrOut, "w");
   if (!fdOut)
   {
      LogMsg("***** Error creating output file: %s [%d]", acGrGrOut, _errno);
      return -3;
   }

   // Merge loop
   while (!feof(fdIn))
   {
      pTmp = fgets(acBuf, 1024, fdIn);
      if (!pTmp)
         break;

      // Drop doc without DocDate
      if (pGrGrRec->DocDate[0] == ' ' || pGrGrRec->ParcelNo[0] == ' ')
         continue;

      if (iFixOpts & PQ_FIX_NSALEFLG)
      {
         if (!memcmp(pGrGrRec->DocTitle, "GRANT DEED", 7) || 
             !memcmp(pGrGrRec->DocTitle, "DEED   ", 7) ||
             !memcmp(pGrGrRec->DocTitle, "TRUSTEES DEED", 10))
         {
            pGrGrRec->NoneSale_Flg = 'N';
            pGrGrRec->IsXfer_Flg = 'Y';
         } else
         {
            pGrGrRec->NoneSale_Flg = 'Y';
            if (!memcmp(pGrGrRec->DocTitle, "AFDVT SUCCSR TR", 10) ||
                !memcmp(pGrGrRec->DocTitle, "AFFIDAVIT", 8) ||
                !memcmp(pGrGrRec->DocTitle, "QUITCLAIM", 8) ||
                !memcmp(pGrGrRec->DocTitle, "ASSIGNMENT LSE  ", 12) )
               pGrGrRec->IsXfer_Flg = 'Y';
            else
               pGrGrRec->IsXfer_Flg = 'N';
         }
      } else if (iFixOpts & PQ_FIX_APN)
      {
         memcpy(acTmp, acBuf, SIZ_APN_S);
         acTmp[SIZ_APN_S] = 0;
         if (strchr(acTmp, '-'))
         {
            iBook = atol(acTmp);
            iLot = 0;
            iPage = 0;

            if (acTmp[3] == '*')
               pTmp = &acTmp[3];
            else
               pTmp = strchr(acTmp, '-');
            if (pTmp)
            {
               iPage = atol(++pTmp);
               pTmp = strchr(pTmp, '-');
               if (pTmp)
                  iLot = atol(++pTmp);
            }

            // ORG,30, 8,3,2,6,Orange,999-999-99,999-99-999,895,999-999-99,980,2011
            if (iBook < 895 || iBook >= 980)
               sprintf(acFmtApn, "%.3d%.3d%.2d        ", iBook, iPage, iLot);
            else
               sprintf(acFmtApn, "%.3d%.2d%.3d        ", iBook, iPage, iLot);

            memcpy(acBuf, acFmtApn, SIZ_APN_S);
         }
      } else if (iFixOpts & PQ_FIX_SALEPRICE)
      {
         double dTmp;
         ULONG  lPrice;

         if (pGrGrRec->DocTax[0] > '0')
         {
            if (!memcmp(pGrGrRec->ParcelNo, "43003210", 8))
               memcpy(pGrGrRec->DocTax, "26400.00  ", 10);
            else if (!memcmp(pGrGrRec->ParcelNo, "44006409", 8))
               memcpy(pGrGrRec->DocTax, "3629.45   ", 10);
            else if (!memcmp(pGrGrRec->ParcelNo, "58053101", 8))
               memcpy(pGrGrRec->DocTax, "5188.15   ", 10);

            dTmp = atofn(pGrGrRec->DocTax, SIZ_OGR_DOCTAX);
            lPrice = (ULONG)(dTmp*100/0.11);
            sprintf(acTmp, "%*u", SIZ_GR_SALE, lPrice);
            memcpy(pGrGrRec->SalePrice, acTmp, SIZ_GR_SALE);
         }
      }

      iRet = fputs(acBuf, fdOut);
      if (iRet)
         LogMsg("*** Error writing to output file %s at %d", acGrGrOut, lCnt);

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }
   printf("\n");

   // Close files
   if (fdOut)
      fclose(fdOut);
   if (fdIn)
      fclose(fdIn);

   LogMsgD("Total output records:       %u", lCnt);

   return iRet;
}

/******************************* Org_LoadGrGr ********************************
 *
 * In order to capture all needed information, we should merge Documents.txt
 * with Deed.txt
 *
 * Steps:
 *    1) Unzip
 *    2) Read Deed record and create GrGr rec via Org_CreateGrGrRec()
 *    3) Match docnumber with Document record to get DocType and DocDate via Org_DocMatch()
 *    4) Output to GrGr file.
 *
 *****************************************************************************/

int Org_LoadGrGr(char *pCnty)
{
   char     *pTmp, acSavRec[1024], acRec[1024], acTmp[256], acProcDate[16];
   char     acGrGrIn[_MAX_PATH], acGrGrOut[_MAX_PATH], acGrGrBak[_MAX_PATH], acDeedTmpl[64], acDocTmpl[64],
            acDeedFile[_MAX_PATH], acDocFile[_MAX_PATH], acZipFile[_MAX_PATH], acTmpFile[_MAX_PATH],
            acUnzipFolder[_MAX_PATH];
   struct   _finddata_t  sFileInfo;

   FILE     *fdOut;
   ORG_GRGR curGrGrRec;

   int      iDocToken, iCnt, iRet, iGrGrLen, iTmp, iMatched=0, iNotMatched=0;
   BOOL     bEof=false;
   long     lCnt, lHandle;

   // Get raw file name
   GetIniString(pCnty, "GrGrIn", "", acGrGrIn, _MAX_PATH, acIniFile);

   // Open Input file
   lHandle = _findfirst(acGrGrIn, &sFileInfo);
   if (lHandle > 0)
   {
      pTmp = strrchr(acGrGrIn, '\\');
      if (pTmp)
         *pTmp = 0;
      iRet = 0;
   } else
   {
      bGrGrAvail = false;
      LogMsg("*** GrGr file not available for processing: %s", acGrGrIn);
      return -1;
   }

   GetIniString(pCnty, "DeedFile", "", acDeedTmpl, _MAX_PATH, acIniFile);
   GetIniString(pCnty, "DocumentFile", "", acDocTmpl, _MAX_PATH, acIniFile);
   iGrGrLen = GetPrivateProfileInt(pCnty, "GrGrRecSize", 102, acIniFile);

   // Create Output file
   sprintf(acGrGrOut, acGrGrTmpl, pCnty, pCnty, "TMP");

   LogMsg("Open output file %s", acGrGrOut);
   fdOut = fopen(acGrGrOut, "w");
   if (fdOut == NULL)
   {
      printf("Error creating GrGr output file: %s\n", acGrGrOut);
      _findclose(lHandle);
      return -2;
   }

   // Initialize Zip server
   doZipInit();
   // Set unzip folder
   GetIniString(myCounty.acCntyCode, "UnzipFolder", "", acUnzipFolder, _MAX_PATH, acIniFile);
   if (acUnzipFolder[0])
      setUnzipToFolder(acUnzipFolder);
   // Set to replace file if exist
   setReplaceIfExist(true);

   // Prepare backup folder
   sprintf(acGrGrBak, acGrBkTmpl, pCnty, acToday);
   if (_access(acGrGrBak, 0))
      _mkdir(acGrGrBak);

   lCnt = 0;
   while (!iRet)
   {
      sprintf(acZipFile, "%s\\%s", acGrGrIn, sFileInfo.name);

      // Unzip input file
      LogMsg("Unzip GRGR file %s", acZipFile);
      iRet = startUnzip(acZipFile);
      if (iRet)
         break;
      
      // Convert to ASCII file
      sprintf(acTmp, "%s\\%s", acUnzipFolder, acDeedTmpl); 
      if (_access(acTmp, 0))
      {
         GetIniString(pCnty, "DeedsFile", "", acDeedTmpl, _MAX_PATH, acIniFile);
         GetIniString(pCnty, "DocumentsFile", "", acDocTmpl, _MAX_PATH, acIniFile);
         sprintf(acTmp, "%s\\%s", acUnzipFolder, acDeedTmpl); 
      }
      lProcDate = getFileDate(acTmp);
      sprintf(acProcDate, "%d", lProcDate);

      sprintf(acTmpFile, "%s\\Deed.csv", acUnzipFolder);
      HRESULT dErr = UnicodeToAnsi(acTmp, acTmpFile);
      if (!dErr)
         strcpy(acTmp, acTmpFile);

      // Rebuild CSV file to repair broken records
      sprintf(acRec, "%s\\Deed.fix", acUnzipFolder);
      iRet = RebuildCsv(acTmp, acRec, ',', ORG_DEED_NDTAX);
      if (iRet < 0)
      {
         LogMsg("***** Bad input Deed file: %s", acTmp);
         goto Next_GrFile;
      }

      // Sort Deed.txt before processing
      sprintf(acDeedFile, "%s\\Deed.srt", acUnzipFolder);
      iRet = sortFile(acRec, acDeedFile, "S(#2,C,A,#1,C,A,#3,C,D)  F(TXT) ");

      // Open Deed.txt
      fdDeed = fopen(acDeedFile, "r");
      if (!fdDeed)
      {
         LogMsg("***** Bad input file %s", acDeedFile);
         break;
      }

      // Update GrGr file date
      iTmp = getFileDate(acTmp);
      if (iTmp > lLastGrGrDate)
         lLastGrGrDate = iTmp;

      // Open Document.txt
      sprintf(acTmp, "%s\\%s", acUnzipFolder, acDocTmpl); 
      if (!_access(acTmp, 0))
      {
         sprintf(acTmpFile, "%s\\Document.csv", acUnzipFolder);
         HRESULT dErr = UnicodeToAnsi(acTmp, acTmpFile);
         if (!dErr)
            strcpy(acTmp, acTmpFile);

         // Rebuild CSV file to repair broken records
         sprintf(acRec, "%s\\Document.fix", acUnzipFolder);
         iRet = RebuildCsv(acTmp, acRec, ',', ORG_DOC_NAMEFLG);
         if (iRet < 0)
         {
            LogMsg("***** Bad input GrGr document file: %s", acTmpFile);
            fclose(fdDeed);
            goto Next_GrFile;
         }

         // Start at row 17 to be safe.  Row 1 thru 15 could be fld name
         iDocToken = countToken(acRec, 17, ',');
         sprintf(acDocFile, "%s\\Document.srt", acUnzipFolder);

         // Sort Document file on DocNum, DocDate
         if (iDocToken == 10)
            iRet = sortFile(acRec, acDocFile, "S(#3,C,A,#2,C,A)  F(TXT) ");
         else
            iRet = sortFile(acRec, acDocFile, "S(#4,C,A,#3,C,A)  F(TXT) ");

         fdDoc = fopen(acDocFile, "r");
         if (!fdDoc)
         {
            LogMsg("***** Bad input file %s", acDocFile);
            break;
         }
      } else
      {
         LogMsg("***** Missing input file %s", acDocFile);
         break;
      }

      // Init DocMatch to prepare for new file
      iRet = Org_DocMatch(NULL, iDocToken, true);
      memset(&curGrGrRec, ' ', sizeof(ORG_GRGR));

      iCnt = 1;
      while (!feof(fdDeed))
      {
         do
         {
            pTmp = fgets(acRec, 1024, fdDeed);
         } while (pTmp && acRec[0] != 34);   // Check for double quote

#ifdef _DEBUG
         //if (strstr(acRec, "07241302") || strstr(acRec, "39304137") )
         //   iRet = 0;
#endif

         if (!pTmp) 
            break;

         // If same doc, increase iCnt
         if (!memcmp(acSavRec, acRec, 60))   // Change from 30 to 60 on 3/26/2016
            iCnt++;
         else if (curGrGrRec.ParcelNo[0] >= '0')
         {
            // Output GrGr record
            curGrGrRec.CrLf[0] = '\n';
            curGrGrRec.CrLf[1] = 0;

#ifdef _DEBUG
            //if (!memcmp(curGrGrRec.ParcelNo, "07241302", 8))
            //   iRet = 0;
#endif

            // Match with document.txt to get DocDate and DocType
            if (fdDoc)
            {
               iRet = Org_DocMatch(&curGrGrRec, iDocToken, false);
               if (!iRet) 
                  iMatched++;
               else
                  iNotMatched++;
            } else
               iNotMatched++;

            memcpy(curGrGrRec.ProcDate, acProcDate, SIZ_OGR_DOCDATE);
            iRet = fputs((char *)&curGrGrRec, fdOut);
            strcpy(acSavRec, acRec);
            iCnt = 1;      // reset counter
            memset(&curGrGrRec, ' ', sizeof(ORG_GRGR));
         } else
            iCnt = 1;

         iRet = Org_CreateGrGrRec(&curGrGrRec, acRec, iCnt);

         lCnt++;
         if (!(lCnt % 1000))
            printf("\r%d", lCnt);
      }

      LogMsg("Number of matched records:     %d", iMatched);
      LogMsg("Number of records not matched: %d", iNotMatched);
      LogMsg("Total records so far     : %u", lCnt);

      fclose(fdDeed);
      if (fdDoc)
         fclose(fdDoc);

      // Move input file to backup folder
      sprintf(acTmp, "%s\\%s", acGrGrBak, sFileInfo.name);
      rename(acZipFile, acTmp);

      Next_GrFile:
      // Find next file
      iRet = _findnext(lHandle, &sFileInfo);
   }
   printf("\n%d\n", lCnt);


   // Close handle
   _findclose(lHandle);

   // Close files
   if (fdOut)
      fclose(fdOut);

   // Shut down Zip server
   doZipShutdown();

   LogMsg("Total matched records    : %d", iMatched);
   LogMsg("Total records not matched: %d", iNotMatched);
   LogMsg("Total processed records  : %u", lCnt);

   if (lCnt > 0)
   {
      // Sort output file and dedup if same docdate and docnum
      // Sort on APN asc, RecDate asc,DocNum asc
      strcpy(acTmp,"S(1,36,C,A) F(TXT) DUPO(1,36) ");

      strcpy(acGrGrIn, acGrGrOut);
      sprintf(acGrGrOut, acGrGrTmpl, pCnty, pCnty, "DAT");
      // Sort ORG_GrGr.tmp to ORG_GrGr.dat
      lCnt = sortFile(acGrGrIn, acGrGrOut, acTmp);

      // Update cumulative GrGr file
      if (lCnt > 0)
      {
         // Sort file - Append Org_GrGr.Dat to Org_GrGr.sls
         sprintf(acGrGrIn, acGrGrTmpl, pCnty, pCnty, "Sls");
         sprintf(acGrGrBak, acGrGrTmpl, pCnty, pCnty, "Srt");
         sprintf(acRec, "%s+%s", acGrGrOut, acGrGrIn);
         iRet = sortFile(acRec, acGrGrBak, acTmp);
         if (iRet > 1000)
         {
            remove(acGrGrIn);
            iRet = rename(acGrGrBak, acGrGrIn);

            // Set sale file name as Org_GrGr.sls
            strcpy(acSaleFile, acGrGrIn);
         } else
            iRet = -1;
      } else
         acSaleFile[0] = 0;

      iRet = 0;
   } else
      iRet = 1;

   LogMsg("Total output records after dedup: %u", lCnt);
   printf("\nTotal output records after dedup: %u\n", lCnt);

   return iRet;
}

/****************************** Org_MergeGrGrRec *****************************
 *
 * If multiple deed files on the same day, put the one with sale amt in Sale1,
 * other in transfer if doc# is bigger.
 * To make update more accurate, we only update if seller=current owner.  Use
 * sale info from GRANT DEED with sale price only.
 *
 * Return -1 if record skip, 0 if same rec date as previously processed record,
 * and 1 if new sale record applied.
 *
 *****************************************************************************/

int Org_MergeGrGrRec(char *pOutbuf, char *pSaleRec, int iSaleCnt, bool bUpdtOwner)
{
   static char curOwner[256];

   ORG_GRGR *pRec;
   int      iTmp, iRet=0;
   long     lCurSaleDt, lLstSaleDt, lPrice;
   char     *pTmp, acTmp[256], acTmp1[256];

   pRec = (ORG_GRGR *)pSaleRec;
   lCurSaleDt = atoin(pRec->DocDate, SIZ_OGR_DOCDATE);

   // Ignore none xfer doc
   if (pRec->IsXfer_Flg == 'N')
      return 0;

   // Update transfers
   lLstSaleDt = atoin(pOutbuf+OFF_TRANSFER_DT, SIZ_TRANSFER_DT);
#ifdef _DEBUG    
   //if (!memcmp(pOutbuf, "32041113", 8) )
   //   iTmp = 0;
#endif

   if (lCurSaleDt < lLstSaleDt || (lCurSaleDt == lLstSaleDt && memcmp(&pRec->DocNum[4], pOutbuf+OFF_TRANSFER_DOC, 8) <= 0))
      return -1;

   if (lCurSaleDt > lLstSaleDt)
   {
      memset(pOutbuf+OFF_TRANSFER_DOC, ' ', SIZ_TRANSFER_DOC);
      memcpy(pOutbuf+OFF_TRANSFER_DOC, &pRec->DocNum[4], 8);
      memcpy(pOutbuf+OFF_TRANSFER_DT, pRec->DocDate, SIZ_TRANSFER_DT);
   } else if (lCurSaleDt == lLstSaleDt && memcmp(&pRec->DocNum[4], pOutbuf+OFF_TRANSFER_DOC, 8) > 0)
      memcpy(pOutbuf+OFF_TRANSFER_DOC, &pRec->DocNum[4], 8);

   // Take Deed, Grant Deed, and Trustees deed only
   if (pRec->NoneSale_Flg == 'Y')
      return -1;

#ifdef _DEBUG    
   //if (!memcmp(pOutbuf, "32041113", 8) )
   //   iTmp = 0;
#endif
   // Update Owner and Mailing if xfer date > Lien date
	// 05/08/2017
   memcpy(acTmp, pRec->Grantee[0], SIZ_OGR_NAME);
   iTmp = blankRem(acTmp, SIZ_OGR_NAME);
   if ((acTmp[0] > ' ') && (lCurSaleDt > lLienDate) && (memcmp(acTmp, pOutbuf+OFF_NAME1, iTmp) || memcmp(pRec->Grantee[1], pOutbuf+OFF_NAME2, 10)))
   {
      removeNames(pOutbuf, true);
      memcpy(acTmp, pRec->Grantee[0], SIZ_OGR_NAME);
      blankRem(acTmp, SIZ_OGR_NAME);
      iTmp = updateVesting(myCounty.acCntyCode, acTmp, pOutbuf+OFF_VEST, pOutbuf+OFF_ETAL_FLG);

      if (pRec->Grantee[1][0] > ' ')
      {
         iTmp = Org_FormatGrGrOwner(acTmp, acTmp1, pSaleRec);
         if (iTmp == 1)
         {
            vmemcpy(pOutbuf+OFF_NAME1, acTmp, SIZ_NAME1);
            vmemcpy(pOutbuf+OFF_NAME_SWAP, acTmp1, SIZ_NAME_SWAP);
         } else if (iTmp == 2)
         {
            memcpy(pOutbuf+OFF_NAME1, pRec->Grantee[0], SIZ_NAME1);
            memcpy(acTmp, pRec->Grantee[0], SIZ_OGR_NAME);
            acTmp[SIZ_OGR_NAME] = 0;
            if (pTmp = strstr(acTmp, " TR"))
               *pTmp = 0;
            pTmp = swapName(acTmp, acTmp1, 0);
            if (pTmp)
               memcpy(pOutbuf+OFF_NAME_SWAP, acTmp1, strlen(acTmp1));
            else
               memcpy(pOutbuf+OFF_NAME_SWAP, acTmp, strlen(acTmp));
         } else
         {
            memcpy(pOutbuf+OFF_NAME1, acTmp, strlen(acTmp));
            memcpy(pOutbuf+OFF_NAME_SWAP, acTmp1, strlen(acTmp));
         }

         memcpy(pOutbuf+OFF_NAME2, pRec->Grantee[1], SIZ_NAME2);
      } else
      {
         memcpy(pOutbuf+OFF_NAME1, pRec->Grantee[0], SIZ_NAME1);
         memcpy(acTmp, pRec->Grantee[0], SIZ_OGR_NAME);
         acTmp[SIZ_OGR_NAME] = 0;
         if (pTmp = strstr(acTmp, " TR"))
            *pTmp = 0;
         pTmp = swapName(acTmp, acTmp1, 0);
         if (pTmp)
            memcpy(pOutbuf+OFF_NAME_SWAP, acTmp1, strlen(acTmp1));
         else
            memcpy(pOutbuf+OFF_NAME_SWAP, acTmp, strlen(acTmp));
      }

      // If mailing is not avail, use situs
      if (pRec->Addr1[0] > ' ')
      {
         removeMailing(pOutbuf, false);
         Org_MergeGrGrMailing(pOutbuf, pSaleRec);
      } else
      {
         CopySitusToMailing(pOutbuf);
      }
   }

   lPrice = atoin(pRec->SalePrice, SIZ_SALE1_AMT);
   lLstSaleDt = atoin(pOutbuf+OFF_SALE1_DT, SIZ_SALE1_DT);

   // If no sale price, skip it
   if (!lPrice)
      return -1;

   // Check sale date
   if (lCurSaleDt > lLstSaleDt)
   {
      // Move sale2 to sale3
      memcpy(pOutbuf+OFF_SALE3_DOC, pOutbuf+OFF_SALE2_DOC, SIZ_SALE1_DOC);
      memcpy(pOutbuf+OFF_SALE3_DT, pOutbuf+OFF_SALE2_DT, SIZ_SALE1_DT);
      memcpy(pOutbuf+OFF_SALE3_DOCTYPE, pOutbuf+OFF_SALE2_DOCTYPE, SIZ_SALE1_DOCTYPE);
      memcpy(pOutbuf+OFF_SALE3_AMT, pOutbuf+OFF_SALE2_AMT, SIZ_SALE1_AMT);

      // Move sale1 to sale2
      memcpy(pOutbuf+OFF_SALE2_DOC, pOutbuf+OFF_SALE1_DOC, SIZ_SALE1_DOC);
      memcpy(pOutbuf+OFF_SALE2_DT, pOutbuf+OFF_SALE1_DT, SIZ_SALE1_DT);
      memcpy(pOutbuf+OFF_SALE2_DOCTYPE, pOutbuf+OFF_SALE1_DOCTYPE, SIZ_SALE1_DOCTYPE);
      memcpy(pOutbuf+OFF_SALE2_AMT, pOutbuf+OFF_SALE1_AMT, SIZ_SALE1_AMT);

      // Clear sale1
      memset(pOutbuf+OFF_SALE1_DOC, ' ', SIZ_SALE1_DOC);
      memset(pOutbuf+OFF_SALE1_DT, ' ', SIZ_SALE1_DT);
      memset(pOutbuf+OFF_SALE1_DOCTYPE, ' ', SIZ_SALE1_DOCTYPE);
      memset(pOutbuf+OFF_SALE1_AMT, ' ', SIZ_SALE1_AMT);

      iRet = 1;
   }

   // Update DocNum
   if (pRec->DocNum[0] > ' ')
   {
      memset(pOutbuf+OFF_SALE1_DOC, ' ', SIZ_SALE1_DOC);
      memcpy(pOutbuf+OFF_SALE1_DOC, &pRec->DocNum[4], 8);
   }

   // Update current sale
   memcpy(pOutbuf+OFF_SALE1_DT, pRec->DocDate, SIZ_SALE1_DT);
   if (lPrice > 0)
      memcpy(pOutbuf+OFF_SALE1_AMT, pRec->SalePrice, SIZ_SALE1_AMT);

   if (pRec->DocTitle[0] > ' ')
   {
      if (findDocType(pRec->DocTitle, acTmp))
         memcpy(pOutbuf+OFF_SALE1_DOCTYPE, acTmp, strlen(acTmp));
      else
         LogMsg("*** Unknown DocTitle at %.12s", pRec->ParcelNo);
   }

   // Update seller
   memset(pOutbuf+OFF_SELLER, ' ', SIZ_SELLER);
   if (pRec->Grantor[1][0] > ' ')
   {
      char acName1[64], acName2[64], acSeller[128];

      memcpy(acName1, pRec->Grantor[0], SIZ_OGR_NAME);
      memcpy(acName2, pRec->Grantor[1], SIZ_NAME2);

      iTmp = MergeName1(myTrim(acName1,SIZ_OGR_NAME), myTrim(acName2,SIZ_NAME2), acSeller);
      if (iTmp == 1)
      {
         iTmp = strlen(acSeller);
         // If seller is too long, keep one name only
         if (iTmp > SIZ_SELLER)
            memcpy(pOutbuf+OFF_SELLER, pRec->Grantor[0], SIZ_SELLER);
         else
            memcpy(pOutbuf+OFF_SELLER, acSeller, iTmp);
      } else
      {
         memcpy(pOutbuf+OFF_SELLER, pRec->Grantor[0], SIZ_SELLER);
      }
   } else
   {
      memcpy(pOutbuf+OFF_SELLER, pRec->Grantor[0], SIZ_SELLER);
   }

   return iRet;
}

/********************************* Org_MatchGrGr *****************************
 *
 * Return 0 if matched
 *
 *****************************************************************************/

int Org_MatchGrGr(char *pOutbuf, bool bUpdtOwner)
{
   static   char  acRec[1024], *pRec=NULL;
   static   int   iSaleLen;

   int      iRet, iLoop, iSaleCnt;

   ORG_GRGR *pSale = (ORG_GRGR *)&acRec[0];
   iRet = 1;   // Assume no match

   // Get first Char rec for first call
   if (!pRec && !lSaleMatch)
   {
      do
      {
         pRec = fgets(acRec, 1024, fdSale);
      } while (pRec && acRec[0] == ' ');
   }

   do
   {
      if (!pRec)
      {
         fclose(fdSale);
         fdSale = NULL;
         return iRet;      // EOF
      }

      // Compare Apn, ignore bad APN records
      iLoop = memcmp(pOutbuf, acRec, myCounty.iApnLen);
      if (iLoop > 0 || acRec[myCounty.iApnLen] != ' ')
      {
         if (bDebug)
            LogMsg0("Skip Sale rec  %.*s", myCounty.iApnLen, acRec);

         // Read another one
         pRec = fgets(acRec, 1024, fdSale);
         lSaleSkip++;
      } else if (!iLoop)
      {
         // Verify Lot/Tract to make sure we have the right parcel
         // One of them can be wrong, but not both

         // 2015/08/10 - Not all transaction has TRACT & LOT data
         //if (pSale->Lot[0] > ' ' && pSale->Tract[0] > ' ' &&
         //   memcmp(pOutbuf+OFF_LOT, pSale->Lot, SIZ_LOT) &&
         //   memcmp(pOutbuf+OFF_TRACT, pSale->Tract, SIZ_LOT))
         //{
         //   // Verify address
         //   int iStrNo1, iStrNo2;
         //   iStrNo1 = atoi(pOutbuf+OFF_S_STRNUM);
         //   iStrNo2 = atoi(pSale->Addr1);
         //   if (iStrNo1 != iStrNo2)
         //   {
         //      iLoop = 1;
         //      LogMsg0("** Invalid lot on parcel %.10s", pOutbuf);

         //      // Read another one
         //      pRec = fgets(acRec, 1024, fdSale);
         //      lSaleSkip++;
         //   }
         //}
      }
   } while (iLoop > 0);

   // If not match, return
   if (iLoop)
      return iRet;

   iSaleCnt  = 0;
   do
   {
#ifdef _DEBUG
      //if (!memcmp(pOutbuf, "00101127", 8))
      //   iRet = 1;
#endif
      iLoop = Org_MergeGrGrRec(pOutbuf, acRec, iSaleCnt, bUpdtOwner);
      if (iLoop == 1)
      {
         // Update flag
         if (iSaleCnt < 3)
            *(pOutbuf+OFF_AR_CODE1+iSaleCnt) = 'R';           // Assessor-Recorder code
         iSaleCnt++;
         iRet = 0;
      }

      // Get next sale record
      pRec = fgets(acRec, 1024, fdSale);
      if (!pRec)
      {
         fclose(fdSale);
         fdSale = NULL;
         break;
      }

   } while (!memcmp(pOutbuf, acRec, myCounty.iApnLen));

   lSaleMatch++;

   return iRet;
}

/********************************** Org_MergeGrGr ******************************
 *
 *
 ******************************************************************************/

int Org_MergeGrGr(int iSkip, bool bUpdtOwner)
{
   char     acBuf[MAX_RECSIZE];
   char     acRawFile[_MAX_PATH], acOutFile[_MAX_PATH], acTmp[256];

   HANDLE   fhIn, fhOut;

   int      iRet, iRollUpd=0, iNewRec=0;
   DWORD    nBytesRead;
   DWORD    nBytesWritten;
   BOOL     bRet, bUseR01=true;
   long     lRet=0, lCnt=0;

   sprintf(acRawFile, acRawTmpl, "ORG", "ORG", "R01");
   sprintf(acOutFile, acRawTmpl, "ORG", "ORG", "N01");

   // Rename files for processing
   if (_access(acRawFile, 0))
   {
      sprintf(acRawFile, acRawTmpl, "ORG", "ORG", "S01");
      if (!_access(acRawFile, 0))
      {
         LogMsg("Use %s as input for MergeGrGr", acRawFile);
         bUseR01 = false;
      } else
      {
         LogMsg("***** Error in MergeGrGr(): Missing input file %s.  Please recheck!", acRawFile);
         return -1;
      }
   }

   // Open GrGr file
   LogMsg("Open GrGr file %s", acSaleFile);
   fdSale = fopen(acSaleFile, "r");
   if (fdSale == NULL)
   {
      LogMsg("***** Error opening GrGr file: %s\n", acSaleFile);
      return -2;
   }

   // Open Input file
   LogMsg("Open input file %s", acRawFile);
   fhIn = CreateFile(acRawFile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhIn == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening Roll file: %s\n", acRawFile);
      return -3;
   }

   // Open Output file
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening output file: %s\n", acOutFile);
      return -4;
   }


   // Copy skip record
   memset(acBuf, ' ', iRecLen);
   while (iSkip-- > 0)
   {
      ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);
      // Ignore record start with 99999999
      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
   }

   iNoMatch=iBadCity=iBadSuffix=0;

   // Merge loop
   lSaleMatch = 0;
   do
   {
      bRet = ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);

      // Check for EOF
      if (!bRet)
      {
         LogMsg("***** Error reading input file %s (%f)", acRawFile, GetLastError());
         break;
      }

      // EOF ?
      if (!nBytesRead)
         break;

      if (bClearSales)
         ClearOldSale(acBuf, true);

      // Update GrGr info
      if (fdSale)
         if (!Org_MatchGrGr(acBuf, bUpdtOwner))
            iRollUpd++;

      // Update last recorded date
      iRet = atoin((char *)&acBuf[OFF_SALE1_DT], 8);
      if (iRet > lLastRecDate && iRet < lToday)
         lLastRecDate = iRet;

      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);

      if (!bRet)
      {
         LogMsg("***** Error occurs: %d\n", GetLastError());
         lRet = WRITE_ERR;
      }
   } while (bRet);

   // Close files
   if (fdSale)
      fclose(fdSale);
   if (fhOut)
      CloseHandle(fhOut);
   if (fhIn)
      CloseHandle(fhIn);

   // Rename R01 to SAV if R01 is used as input
   if (bUseR01)
   {
      sprintf(acTmp, acRawTmpl, "ORG", "ORG", "SAV");
      if (!_access(acTmp, 0))
      {
         LogMsg("Remove saved file %s", acTmp);
         if (iRet=remove(acTmp))
            LogMsg("***** Error removing %s to %s (%d)", acTmp, errno);
      }

      LogMsg("Rename %s to %s", acRawFile, acTmp);
      if (iRet=rename(acRawFile, acTmp))
         LogMsg("***** Error renaming %s to %s (%d)", acRawFile, acTmp, errno);
   } else
      sprintf(acRawFile, acRawTmpl, "ORG", "ORG", "R01");

   LogMsg("Rename %s to %s", acOutFile, acRawFile);
   if (iRet=rename(acOutFile, acRawFile))
      LogMsg("***** Error renaming %s to %s (%d)", acOutFile, acRawFile, errno);

   LogMsg("Total output records:       %u", lCnt);
   LogMsg("Total records updated:      %u", iRollUpd);
   LogMsg("Total records skipped:      %u", lSaleSkip);
   LogMsg("Last recording date:        %u", lLastRecDate);

   printf("\nTotal output records: %u", lCnt);

   lRecCnt = lCnt;
   return iRet;
}

/******************************** Org_MergeSitus ******************************
 *
 * Merge situs using data from ATTOM 
 *
 ******************************************************************************/

int Org_MergeSitus(int iSkip, char *pSitusFile)
{
   char     acBuf[MAX_RECSIZE];
   char     acRawFile[_MAX_PATH], acOutFile[_MAX_PATH], acTmp[256];

   HANDLE   fhIn, fhOut;

   int      iRet, iRollUpd=0, iNewRec=0;
   DWORD    nBytesRead;
   DWORD    nBytesWritten;
   BOOL     bRet, bUseR01=true;
   long     lRet=0, lCnt=0;

   sprintf(acRawFile, acRawTmpl, "ORG", "ORG", "R01");
   sprintf(acOutFile, acRawTmpl, "ORG", "ORG", "T01");

   // Rename files for processing
   if (_access(acRawFile, 0))
   {
      sprintf(acRawFile, acRawTmpl, "ORG", "ORG", "S01");
      if (!_access(acRawFile, 0))
      {
         LogMsg("Use %s as input for MergeSitus", acRawFile);
         bUseR01 = false;
      } else
      {
         LogMsg("***** Error in MergeGrGr(): Missing input file %s.  Please recheck!", acRawFile);
         return -1;
      }
   }

   // Open situs file
   LogMsg("Open situs file %s", pSitusFile);
   fdSitus = fopen(pSitusFile, "r");
   if (fdSitus == NULL)
   {
      LogMsg("***** Error opening situs file: %s\n", pSitusFile);
      return -2;
   }

   // Open Input file
   LogMsg("Open input file %s", acRawFile);
   fhIn = CreateFile(acRawFile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhIn == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening Roll file: %s\n", acRawFile);
      return -3;
   }

   // Open Output file
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening output file: %s\n", acOutFile);
      return -4;
   }


   // Copy skip record
   memset(acBuf, ' ', iRecLen);
   while (iSkip-- > 0)
   {
      ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);
      // Ignore record start with 99999999
      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
   }

   iNoMatch=iBadCity=iBadSuffix=0;

   // Merge loop
   lSaleMatch = 0;
   do
   {
      bRet = ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);

      // Check for EOF
      if (!bRet)
      {
         LogMsg("***** Error reading input file %s (%f)", acRawFile, GetLastError());
         break;
      }

      // EOF ?
      if (!nBytesRead)
         break;

      // Update GrGr info
      if (fdSitus)
         if (!Org_UpdSitus(acBuf))
            iRollUpd++;

      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);

      if (!bRet)
      {
         LogMsg("***** Error occurs: %d\n", GetLastError());
         lRet = WRITE_ERR;
      }
   } while (bRet);

   // Close files
   if (fdSitus)
      fclose(fdSitus);
   if (fhOut)
      CloseHandle(fhOut);
   if (fhIn)
      CloseHandle(fhIn);

   // Rename R01 to SAV if R01 is used as input
   if (bUseR01)
   {
      sprintf(acTmp, acRawTmpl, "ORG", "ORG", "SAV");
      if (!_access(acTmp, 0))
      {
         LogMsg("Remove saved file %s", acTmp);
         if (iRet=remove(acTmp))
            LogMsg("***** Error removing %s to %s (%d)", acTmp, errno);
      }

      LogMsg("Rename %s to %s", acRawFile, acTmp);
      if (iRet=rename(acRawFile, acTmp))
         LogMsg("***** Error renaming %s to %s (%d)", acRawFile, acTmp, errno);
   } else
      sprintf(acRawFile, acRawTmpl, "ORG", "ORG", "R01");

   LogMsg("Rename %s to %s", acOutFile, acRawFile);
   if (iRet=rename(acOutFile, acRawFile))
      LogMsg("***** Error renaming %s to %s (%d)", acOutFile, acRawFile, errno);

   LogMsg("Total records updated:      %u", iRollUpd);
   LogMsgD("Total output records:       %u", lCnt);

   lRecCnt = lCnt;
   return iRet;
}

/********************************* Org_Load_Roll ******************************
 *
 *
 ******************************************************************************/

int Org_Load_Roll(int iSkip)
{
   char     *pTmp, acRec[MAX_RECSIZE], acBuf[MAX_RECSIZE], acRollRec[MAX_RECSIZE];
   char     acRawFile[_MAX_PATH], acOutFile[_MAX_PATH];

   HANDLE   fhIn, fhOut;

   int      iRet, iTmp, iRollUpd=0, iNewRec=0, iRetiredRec=0;
   DWORD    nBytesRead;
   DWORD    nBytesWritten;
   BOOL     bRet, bEof;
   long     lRet=0, lCnt=0;

   sprintf(acRawFile, acRawTmpl, "ORG", "ORG", "S01");
   sprintf(acOutFile, acRawTmpl, "ORG", "ORG", "R01");

   // Rename files for processing
   if (_access(acRawFile, 0))
   {
      if (!_access(acOutFile, 0))
         rename(acOutFile, acRawFile);
      else
      {
         LogMsg("***** Missing input file %s.  Please recheck!", acRawFile);
         return 1;
      }
   }

   // Open updated roll file
   LogMsg("Open Roll file %s", acRollFile);
   fdRoll = fopen(acRollFile, "r");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", acRollFile);
      return 2;
   }

   // Open Input file
   LogMsg("Open input file %s", acRawFile);
   fhIn = CreateFile(acRawFile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhIn == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening input file: %s\n", acRawFile);
      return 3;
   }

   // Open Output file
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening output file: %s\n", acOutFile);
      return 4;
   }

   // Get first RollRec
   fgets((char *)&acRollRec[0], iRollLen, fdRoll);
   pTmp = fgets((char *)&acRollRec[0], iRollLen, fdRoll);
   bEof = (pTmp ? false:true);

   // Copy skip record
   memset(acBuf, ' ', iRecLen);
   while (iSkip-- > 0)
   {
      ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);
      // Ignore record start with 99999999
      //if (memcmp(acBuf, "99999999", 8))
         bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
   }

   iNoMatch=iBadCity=iBadSuffix=0;

   // Merge loop
   while (!bEof)
   {
      bRet = ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);

      // Check for EOF
      if (!bRet)
      {
         LogMsg("***** Error reading input file %s (%f)", acRawFile, GetLastError());
         break;
      }

      // EOF ?
      if (iRecLen != nBytesRead)
         break;

      NextRollRec:
      iTmp = memcmp(acBuf, acRollRec, iApnLen);
      if (!iTmp)
      {
         // Merge roll data
         iRet = Org_MergeRoll(acBuf, acRollRec, iRollLen, UPDATE_R01);
         iRollUpd++;

         // Read next roll record
         pTmp = fgets(acRollRec, iRollLen, fdRoll);

         if (!pTmp)
            bEof = true;    // Signal to stop
      } else if (iTmp > 0)       // Roll not match, new roll record?
      {
         LogMsg0("   New roll record : %.*s (%d) ", iApnLen, acRollRec, lCnt);

         // Create new R01 record
         iRet = Org_MergeRoll(acRec, acRollRec, iRollLen, CREATE_R01);
         iNewRec++;

         bRet = WriteFile(fhOut, acRec, iRecLen, &nBytesWritten, NULL);
         lCnt++;

         // Get next roll record
         pTmp = fgets(acRollRec, iRollLen, fdRoll);

         if (!pTmp)
            bEof = true;    // Signal to stop
         else
            goto NextRollRec;
      } else
      {
         // Record may be retired
         LogMsg0("   Roll not match (retired record?) : R01->%.*s < Roll->%.*s (%d)", iApnLen, acBuf, iApnLen, acRollRec, lCnt);
         iRetiredRec++;
         continue;
      }

      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);

      if (!bRet)
      {
         LogMsg("***** Error occurs: %d\n", GetLastError());
         lRet = WRITE_ERR;
         break;
      }
   }

   // Do the rest of the file
   while (nBytesRead>0 && ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL))
   {
      if (!nBytesRead)
         break;

      LogMsg0("   No match R01= %.*s Roll=%.*s", iApnLen, acBuf, iApnLen, acRollRec);

      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
      iNoMatch++;
      lCnt++;
   }

   // Close files
   if (fdRoll)
      fclose(fdRoll);
   if (fhOut)
      CloseHandle(fhOut);
   if (fhIn)
      CloseHandle(fhIn);

   LogMsg("Total output records:       %u", lCnt);
   LogMsg("Total new records:          %u", iNewRec);
   LogMsg("Total retired records:      %u", iRetiredRec);
   LogMsg("Total bad-city records:     %u", iBadCity);
   LogMsg("Total bad-suffix records:   %u", iBadSuffix);

   printf("\nTotal output records: %u", lCnt);

   lRecCnt = lCnt;
   return 0;
}

/********************************* Org_Load_LDR *****************************
 *
 * Return 0 if successful, else error code
 *
 ****************************************************************************/

int Org_Load_LDR(LPCSTR pExeUpdFile, int iFirstRec /* 1=create header rec */)
{
   char     *pTmp, acBuf[MAX_RECSIZE], acRollRec[MAX_RECSIZE];
   char     acOutFile[_MAX_PATH], acSitusFile[_MAX_PATH];

   HANDLE   fhOut;
   FILE     *fdUpdVal;

   int      iRet, iRollUpd=0, iNewRec=0, iRetiredRec=0;
   DWORD    nBytesWritten;
   BOOL     bRet, bEof;
   long     lRet=0, lCnt=0;

   sprintf(acOutFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "R01");

   // Open amendment file
   if (pExeUpdFile && *pExeUpdFile > ' ')
   {
      LogMsg("Open Roll Amendment file %s", pExeUpdFile);
      fdUpdVal = fopen(pExeUpdFile, "r");
      if (!fdUpdVal)
      {
         LogMsg("***** Error opening exemption amended file: %s\n", pExeUpdFile);
         return -2;
      }
   } else
      fdUpdVal = (FILE *)NULL;

   // Open roll file
   LogMsg("Open Roll file %s", acRollFile);
   fdRoll = fopen(acRollFile, "r");
   if (fdRoll == NULL)
   {
      printf("***** Error opening roll file: %s\n", acRollFile);
      return -1;
   }

   // Open Char file
   LogMsg("Open Char file %s", acCChrFile);
   fdChar = fopen(acCChrFile, "r");
   if (fdChar == NULL)
   {
      LogMsg("***** Error opening acreage file: %s\n", acCChrFile);
      return -2;
   }

   // Open situs file 
   GetIniString(myCounty.acCntyCode, "SitusFile", "", acSitusFile, _MAX_PATH, acIniFile);
   if (!_access(acSitusFile, 0))
   {
      LogMsg("Open Situs file %s", acSitusFile);
      fdSitus = fopen(acSitusFile, "r");
      if (fdSitus == NULL)
      {
         LogMsg("***** Error opening situs file: %s\n", acSitusFile);
         return -2;
      }  
   } else
      fdSitus = NULL;

   // Open Output file
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
      return -4;

   // Output first header record
   if (iFirstRec > 0)
   {
      memset(acBuf, '9', iRecLen);
      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
   }

   // Get first RollRec - skip rec header
   pTmp = fgets((char *)&acRollRec[0], iRollLen, fdRoll);
   bEof = (pTmp ? false:true);

   // Init variables
   iNoMatch=iBadCity=iBadSuffix=0;

   // Merge loop
   while (pTmp)
   {
      replNull(acRollRec, ' ', 0);

      // Create new R01 record
      iRet = Org_MergeRoll(acBuf, acRollRec, iRollLen, CREATE_R01|CREATE_LIEN);
      if (!iRet)
      {
         if (fdChar)
            iRet = Org_MergeStdChar(acBuf);

         if (fdSitus)
            iRet = Org_UpdSitus(acBuf);

         if (fdUpdVal)
         {
            iRet = Org_UpdExeVal(fdUpdVal, &acBuf[OFF_EXE_TOTAL], acBuf);
            if (iRet == -1)
            {  // EOF
               fclose(fdUpdVal);
               fdUpdVal = NULL;
            }
         }

         lLDRRecCount++;
         bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
         if (!bRet)
         {
            LogMsg("***** Error writing to output file at record %d\n", lCnt);
            lRet = WRITE_ERR;
            break;
         }
      }

      // Get next roll record
      pTmp = fgets(acRollRec, iRollLen, fdRoll);
      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdUpdVal)
      fclose(fdUpdVal);
   if (fdRoll)
      fclose(fdRoll);
   if (fdChar)
      fclose(fdChar);
   if (fdSitus)
      fclose(fdSitus);
   if (fhOut)
      CloseHandle(fhOut);

   LogMsg("Total input records:  %u", lCnt);
   LogMsg("      output records: %u", lLDRRecCount);
   LogMsg("        Situs update: %u", lUpdSitus);
   
   LogMsg("            bad-city: %u", iBadCity);
   LogMsg("          bad-suffix: %u\n", iBadSuffix);
   
   printf("\nTotal output records: %u", lLDRRecCount);

   lRecCnt = lLDRRecCount;
   return 0;
}

/****************************** Org_UpdateStdUse ******************************
 *
 *
 ******************************************************************************

int Org_UpdateStdUse(int iSkip)
{
   char     acBuf[MAX_RECSIZE];
   char     acRawFile[_MAX_PATH], acOutFile[_MAX_PATH];

   HANDLE   fhIn, fhOut;

   int      iTmp;
   DWORD    nBytesRead;
   DWORD    nBytesWritten;
   BOOL     bRet;
   long     lCnt=0;

   LogMsgD("Update Std Usecode");

   sprintf(acRawFile, acRawTmpl, "ORG", "ORG", "S01");
   sprintf(acOutFile, acRawTmpl, "ORG", "ORG", "R01");

   // Rename files for processing
   if (_access(acRawFile, 0))
   {
      if (!_access(acOutFile, 0))
         rename(acOutFile, acRawFile);
      else
      {
         LogMsg("***** Missing input file %s.  Please recheck!", acRawFile);
         return 1;
      }
   }

   // Open Input file
   LogMsg("Open input file %s", acRawFile);
   fhIn = CreateFile(acRawFile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhIn == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening input file: %s\n", acRawFile);
      return 3;
   }

   // Open Output file
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening output file: %s\n", acOutFile);
      return 4;
   }

   // Copy skip record
   memset(acBuf, ' ', iRecLen);
   while (iSkip-- > 0)
   {
      ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);
      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
   }

   iNoMatch=iBadCity=iBadSuffix=0;

   // Merge loop
   while (bRet)
   {
      bRet = ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);

      // Check for EOF
      if (!bRet)
      {
         LogMsg("***** Error reading input file %s (%f)", acRawFile, GetLastError());
         break;
      }

      // EOF ?
      if (iRecLen != nBytesRead)
         break;

      // Translate use code
      iTmp = acBuf[OFF_USE_CO] - '0';
      if (iTmp < 9 && iTmp >= 0)
         memcpy(&acBuf[OFF_USE_STD], asPropType[iTmp], 3);

      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);

      if (!bRet)
      {
         LogMsg("***** Error occurs: %d\n", GetLastError());
         break;
      }
   }

   if (fhOut)
      CloseHandle(fhOut);
   if (fhIn)
      CloseHandle(fhIn);

   printf("\n");
   LogMsgD("Total output records:       %u", lCnt);

   lRecCnt = lCnt;
   return 0;
} */

/******************************** Org_MergeCondo *****************************
 *
 * Returns: 0 if updated, 1 if not found, -1 if eof
 *
 *****************************************************************************/

// 2007 offset
//#define  ORG_CONDO_APN     47
//#define  ORG_CONDO_LOW     59
//#define  ORG_CONDO_HIGH    70
// 2008 offset
//#define  ORG_CONDO_APN     82
//#define  ORG_CONDO_LOW     91
//#define  ORG_CONDO_HIGH    102
// 2009 offset
//#define  ORG_CONDO_APN     47
//#define  ORG_CONDO_LOW     59
//#define  ORG_CONDO_HIGH    70
// 2011
#define  ORG_CONDO_APN     0
#define  ORG_CONDO_PRCL    13
#define  ORG_CONDO_LOW     20
#define  ORG_CONDO_HIGH    24

int Org_MergeCondo(FILE *fd, char *pOutbuf)
{
   static char    acRec[1024], *pRec=NULL;
   char           acApnHigh[16], acApnLow[16], acTmp[256], acTmp1[256];
   int            iRet, iLoop, iLow, iHigh;

   // Get first rec for first call
   if (!pRec)
      pRec = fgets(acRec, 1024, fd);

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "89801277", 8))
   //   iRet = 0;
#endif
   do
   {
      iLow  = atoin(pRec+ORG_CONDO_LOW, 3);
      iHigh = atoin(pRec+ORG_CONDO_HIGH, 3);
      if (iLow > iHigh)
      {
         LogMsg("Condo high verify: %s", pRec);
         iHigh = 999;
      }
      sprintf(acApnHigh, "%.7s%.3d", pRec+ORG_CONDO_PRCL, iHigh);
      remChar(acApnHigh, '-');

      // Compare Apn
      iLoop = memcmp(pOutbuf, acApnHigh, iApnLen);
      if (iLoop > 0)
      {
         pRec = fgets(acRec, 1024, fd);
         if (!pRec)
            return -1;      // EOF
      }
   } while (iLoop > 0);

   // Compare low APN
   memcpy(acApnLow, pRec+ORG_CONDO_PRCL, iApnLen+2);
   remChar(acApnLow, '-', iApnLen+2);
   iLoop = memcmp(pOutbuf, acApnLow, iApnLen);

   // If smaller than low APN, return
   if (iLoop < 0)
   {
      // Clear old value
      if (memcmp(pOutbuf, "895", 3) >= 0)
      {
         memset(pOutbuf+OFF_ALT_APN, ' ', SIZ_ALT_APN);
         memset(pOutbuf+OFF_MAPLINK, ' ', SIZ_IMAPLINK);
      }
      return 1;
   }

   memcpy(acTmp1, pRec+ORG_CONDO_APN, iApnLen+2);
   remChar(acTmp1, '-', iApnLen+2);
   memcpy(pOutbuf+OFF_ALT_APN, acTmp1, iApnLen);

   // Merge Maplink
   iRet = formatMapLink(acTmp1, acTmp, &myCounty);
   memcpy(pOutbuf+OFF_MAPLINK, acTmp, iRet);

   // Create index map link
   if (getIndexPage(acTmp, acTmp1, &myCounty))
      memcpy(pOutbuf+OFF_IMAPLINK, acTmp1, iRet);

   return 0;
}

/***************************** Org_MergeTimeShare ****************************
 *
 * Returns: 0 if updated, 1 if not found, -1 if eof
 *
 *****************************************************************************/

#define  ORG_TIMESHARE_APN     0
#define  ORG_TIMESHARE_LOW     12
#define  ORG_TIMESHARE_HIGH    23

int Org_MergeTimeShare(FILE *fd, char *pOutbuf)
{
   static char    acRec[1024], *pRec=NULL;
   char           acTmp[256], acTmp1[256];
   int            iRet, iLoop;

   // Get first rec for first call
   if (!pRec)
      pRec = fgets(acRec, 1024, fd);

   do
   {
      // Compare Apn
      memcpy(acTmp, pRec+ORG_TIMESHARE_HIGH, iApnLen+2);
      //iRet = atoin(pRec+20, 2);
      //iLoop = atoin(pRec+31, 2);
      //if (iRet > iLoop)
      //   LogMsg("Timeshare high: %s", pRec);

      remChar(acTmp, '-', iApnLen+2);
      iLoop = memcmp(pOutbuf, acTmp, iApnLen);
      if (iLoop > 0)
      {
         pRec = fgets(acRec, 1024, fd);
         if (!pRec)
            return -1;      // EOF
      }
   } while (iLoop > 0);

   // Compare low APN
   memcpy(acTmp, pRec+ORG_TIMESHARE_LOW, iApnLen+2);
   remChar(acTmp, '-', iApnLen+2);
   iLoop = memcmp(pOutbuf, acTmp, iApnLen);

   // If smaller than low APN, return
   if (iLoop < 0)
      return 1;

   // Update Alt-Apn
   memcpy(acTmp1, pRec+ORG_TIMESHARE_APN, iApnLen+2);
   remChar(acTmp1, '-', iApnLen+2);
   memcpy(pOutbuf+OFF_ALT_APN, acTmp1, iApnLen);

   // Merge Maplink
   iRet = formatMapLink(acTmp1, acTmp, &myCounty);
   memcpy(pOutbuf+OFF_MAPLINK, acTmp, iRet);

   // Create index map link
   if (getIndexPage(acTmp, acTmp1, &myCounty))
      memcpy(pOutbuf+OFF_IMAPLINK, acTmp1, iRet);

   return 0;
}

/******************************* Org_UpdateCondo ******************************
 *
 *
 ******************************************************************************/

int Org_UpdateCondo(char *pCondo, char *pTimeShare, int iSkip)
{
   char     acBuf[MAX_RECSIZE], acRawFile[_MAX_PATH], acOutFile[_MAX_PATH];

   HANDLE   fhIn, fhOut;
   FILE     *fdCondo, *fdTimeShare;

   int      iRet, iCondo=0, iTimeShare=0;
   DWORD    nBytesRead, nBytesWritten;
   BOOL     bRet, bEof, bRename=true;
   long     lCnt=0;

   LogMsg0("Update Condo/Timeshare on R01");

   sprintf(acRawFile, acRawTmpl, "ORG", "ORG", "R01");
   sprintf(acOutFile, acRawTmpl, "ORG", "ORG", "X01");

   if (_access(acRawFile, 0))      // If R01 is not available
   {
      sprintf(acRawFile, acRawTmpl, "ORG", "ORG", "S01");
      if (_access(acRawFile, 0))
      {
         LogMsg("***** Error in UpdateCondo(): Missing input file %s.  Please recheck!", acRawFile);
         return -1;
      }
      bRename = false;
   }

   fdCondo = (FILE *)NULL;
   fdTimeShare = (FILE *)NULL;

   // Open condo file
   if (pCondo && *pCondo > ' ')
   {
      // Sort condo file
      //sprintf(acCondo, "%s\\Org\\Org_Condo.lst", acTmpPath);
      //lTmp = sortFile(pCondo, acCondo, "S(60,14,C,A) F(TXT)  OMIT(60,1,C,LT,\"0\",OR,60,1,C,GT,\"9\")");

      LogMsg("Open Condo file %s", pCondo);
      fdCondo = fopen(pCondo, "r");
      if (fdCondo == NULL)
      {
         LogMsg("***** Error opening Condo file: %s\n", pCondo);
         return -2;
      }
   }

   // Open Time share file
   if (pTimeShare && *pTimeShare > ' ')
   {
      LogMsg("Open Time share file %s", pTimeShare);
      fdTimeShare = fopen(pTimeShare, "r");
      if (fdTimeShare == NULL)
      {
         LogMsg("***** Error opening Time share file: %s\n", pTimeShare);
         return -2;
      }
   }

   // Open Input file
   LogMsg("Open input file %s", acRawFile);
   fhIn = CreateFile(acRawFile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhIn == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening input file: %s\n", acRawFile);
      return -3;
   }

   // Open Output file
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening output file: %s\n", acOutFile);
      return -4;
   }

   bEof = !(fdCondo || fdTimeShare);

   // Copy skip record
   memset(acBuf, ' ', iRecLen);
   while (iSkip-- > 0)
   {
      ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);
      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
   }

   // Merge loop
   while (!bEof)
   {
      bRet = ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);

      // Check for EOF
      if (!bRet)
      {
         LogMsg("***** Error reading input file %s (%f)", acRawFile, GetLastError());
         break;
      }

      // EOF ?
      if (iRecLen != nBytesRead)
         break;

      iRet = 1;
      if (fdCondo)
      {
         iRet = Org_MergeCondo(fdCondo, acBuf);
         if (iRet == -1)
         {
            fclose(fdCondo);
            fdCondo = (FILE *)NULL;
         } else if (!iRet)
            iCondo++;
      }

      // If not a condo, check for timeshare
      if (iRet && fdTimeShare)
      {
         iRet = Org_MergeTimeShare(fdTimeShare, acBuf);
         if (iRet == -1)
         {
            fclose(fdTimeShare);
            fdTimeShare = (FILE *)NULL;
         } else if (!iRet)
            iTimeShare++;
      }

      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);

      if (!bRet)
      {
         LogMsg("***** Error occurs: %d\n", GetLastError());
         break;
      }
   }

   // Close files
   if (fdCondo)
      fclose(fdCondo);
   if (fdTimeShare)
      fclose(fdTimeShare);
   if (fhOut)
      CloseHandle(fhOut);
   if (fhIn)
      CloseHandle(fhIn);

   sprintf(acRawFile, acRawTmpl, "ORG", "ORG", "R01");
   sprintf(acOutFile, acRawTmpl, "ORG", "ORG", "X01");
   // Rename R01 to TMP
   if (bRename)
   {
      sprintf(acBuf, acRawTmpl, "ORG", "ORG", "TMP");
      if (!_access(acBuf, 0))
         remove(acBuf);
      rename(acRawFile, acBuf);
   }

   // Rename X01 to R01
   rename(acOutFile, acRawFile);

   LogMsgD("Total output records:            %u", lCnt);
   LogMsg("Total Condo records updated:     %u", iCondo);
   LogMsg("Total Timeshare records updated: %u", iTimeShare);

   return 0;
}

/****************************** Org_ExtrLienRec *****************************
 *
 *
 ****************************************************************************/

int Org_ExtrLienRec(char *pOutbuf, char *pRollRec)
{
   int      iRet;
   LONGLONG lTmp;
   char     acTmp[64];
   LIENEXTR *pLienExtr = (LIENEXTR *)pOutbuf;

   // Parse string ignoring quote
   iRet = ParseStringIQ(pRollRec, '|', MAX_FLD_TOKEN, apTokens);
   if (iRet < ORG_ROLL_MISCMSG2)
   {
      LogMsg("***** Error: bad input record for APN=%s", apTokens[ORG_ROLL_APN]);
      return -1;
   }

#ifdef _DEBUG
   //if (!memcmp(apTokens[ORG_ROLL_APN], "89804373", 8))
   //   iRet = 0;
#endif

   // Clear output buffer
   memset(pOutbuf, ' ', sizeof(LIENEXTR));

   // Start copying data
   memcpy(pLienExtr->acApn, apTokens[ORG_ROLL_APN], strlen(apTokens[ORG_ROLL_APN]));

   // Year assessed
   memcpy(pLienExtr->acYear, myCounty.acYearAssd, 4);

   // TRA
   lTmp = atol(apTokens[ORG_ROLL_TRA]);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%.6d", lTmp);
      memcpy(pLienExtr->acTRA, acTmp, 6);
   }

   // Land
   long lLand = atoi(apTokens[ORG_ROLL_LAND]);
   if (lLand > 0)
   {
      sprintf(acTmp, "%*u", SIZ_LAND, lLand);
      memcpy(pLienExtr->acLand, acTmp, SIZ_LAND);
   }

   // Improve
   long lImpr = atoi(apTokens[ORG_ROLL_IMPR]);
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*u", SIZ_IMPR, lImpr);
      memcpy(pLienExtr->acImpr, acTmp, SIZ_IMPR);
   }

   // Other value: 
   long lOthImpr = atoi(apTokens[ORG_ROLL_OTHERIMPR]);
   long lPP   = atoi(apTokens[ORG_ROLL_PP_VAL]);
   lTmp = lOthImpr+lPP;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*u", SIZ_LIEN_FIXT, lTmp);
      memcpy(pLienExtr->acOther, acTmp, SIZ_LIEN_FIXT);

      if (lOthImpr > 0)
      {
         sprintf(acTmp, "%*u", SIZ_LIEN_FIXT, lOthImpr);
         memcpy(pLienExtr->extra.Org.OtherImpr, acTmp, SIZ_LIEN_FIXT);
      }
      if (lPP > 0)
      {
         sprintf(acTmp, "%*u", SIZ_LIEN_FIXT, lPP);
         memcpy(pLienExtr->acPP_Val, acTmp, SIZ_LIEN_FIXT);
      }
   }

   // Gross total
   LONGLONG lGross = lTmp+lLand+lImpr;
   if (lGross > 0)
   {
      sprintf(acTmp, "%*u", SIZ_LIEN_GROSS, lGross);
      memcpy(pLienExtr->acGross, acTmp, SIZ_LIEN_GROSS);
   }

   // Ratio
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*u", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
      memcpy(pLienExtr->acRatio, acTmp, SIZ_RATIO);
   }

   // HO Exempt
   if (*apTokens[ORG_ROLL_EXE1_CD] == '7')
      pLienExtr->acHO[0] = '1';      // 'Y'
   else
      pLienExtr->acHO[0] = '2';      // 'N'

   lTmp = atoi(apTokens[ORG_ROLL_EXE1_AMT]);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*u", SIZ_LIEN_EXEAMT, lTmp);
      memcpy(pLienExtr->extra.Org.Exe_Amt1, acTmp, SIZ_LIEN_EXEAMT);
      if (*apTokens[ORG_ROLL_EXE1_CD] > ' ')
         pLienExtr->extra.Org.Exe_Code1 = *apTokens[ORG_ROLL_EXE1_CD];

      iRet = atoi(apTokens[ORG_ROLL_EXE2_AMT]);
      if (iRet > 0)
      {
         sprintf(acTmp, "%*u", SIZ_LIEN_EXEAMT, iRet);
         memcpy(pLienExtr->extra.Org.Exe_Amt2, acTmp, SIZ_LIEN_EXEAMT);
         if (*apTokens[ORG_ROLL_EXE2_CD] > ' ')
            pLienExtr->extra.Org.Exe_Code2 = *apTokens[ORG_ROLL_EXE2_CD];
         lTmp += iRet;
      }

      if (lTmp > lGross)
         lTmp = lGross;
      sprintf(acTmp, "%*u", SIZ_LIEN_EXEAMT, lTmp);
      memcpy(pLienExtr->acExAmt, acTmp, SIZ_LIEN_EXEAMT);
   }

   // Full Exempt
   if (*apTokens[ORG_ROLL_PROPERTYTYPE] == '8')
      pLienExtr->SpclFlag = LX_FULLEXE_FLG;

   // Prop8

   pLienExtr->LF[0] = 10;
   pLienExtr->LF[1] = 0;

   return 0;
}

/******************************* Org_ExtrLien *******************************
 *
 * Extract lien data from lien roll
 *
 ****************************************************************************/

int Org_ExtrLien(LPCSTR pCnty, LPCSTR pLDRFile, LPCSTR pExeUpdFile)
{
   char     *pTmp, acBuf[1024], acRec[MAX_RECSIZE];
   char     acOutFile[_MAX_PATH], sLdrFile[_MAX_PATH];
   int      iRet, iNewRec=0, lCnt=0;
   FILE     *fdLienExt, *fdUpdVal;
   LIENEXTR *pLienExtr = (LIENEXTR *)&acBuf[0];

   LogMsg0("Extract lien roll for %s", pCnty);

   // Open lien file
   if (!pLDRFile)
   {
      GetIniString(pCnty, "LienFile", "", sLdrFile, _MAX_PATH, acIniFile);
      if (!sLdrFile[0])
      {
         LogMsg("***** Error LDR file not defined.  Please check LOADONE.INI file");
         return -1;
      }
   } else
      strcpy(sLdrFile, pLDRFile);

   // Open amendment file
   if (pExeUpdFile && *pExeUpdFile > ' ')
   {
      LogMsg("Open Exemption Amendment file %s", pExeUpdFile);
      fdUpdVal = fopen(pExeUpdFile, "r");
      if (!fdUpdVal)
      {
         LogMsg("***** Error opening exemption amended file: %s\n", pExeUpdFile);
         return -2;
      }
   } else
      fdUpdVal = (FILE *)NULL;

   LogMsg("Open Lien Date Roll file %s", sLdrFile);
   fdRoll = fopen(sLdrFile, "r");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening sorted roll file: %s\n", sLdrFile);
      return -3;
   }

   // Create lien extract
   sprintf(acOutFile, acLienTmpl, pCnty, pCnty);
   LogMsg("Open lien extract file %s", acOutFile);
   fdLienExt = fopen(acOutFile, "w");
   if (fdLienExt == NULL)
   {
      LogMsg("***** Error creating lien extract file: %s\n", acOutFile);
      return -4;
   }

   // Get 1st rec
   pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdRoll);

   // Merge loop
   while (pTmp && !feof(fdRoll))
   {
      // Replace null char with space
      iRet = replNull(acRec, ' ', 0);

      // Create new record
      iRet = Org_ExtrLienRec(acBuf, acRec);    
      if (!iRet)
      {
         if (fdUpdVal)
         {
            iRet = Org_UpdExeVal(fdUpdVal, pLienExtr->acExAmt, acBuf);
            if (iRet == -1)
            {
               fclose(fdUpdVal);
               fdUpdVal = NULL;
            }
         }

         // Write to output
         fputs(acBuf, fdLienExt);
         iNewRec++;
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);

      // Get next roll record
      pTmp = fgets(acRec, MAX_RECSIZE, fdRoll);
      if (!isdigit(acRec[1]))
         break;      // EOF
   }

   // Close files
   if (fdRoll)
      fclose(fdRoll);
   if (fdUpdVal)
      fclose(fdUpdVal);
   if (fdLienExt)
      fclose(fdLienExt);

   LogMsg("Total output records:       %u", iNewRec);
   LogMsg("Total records processed:    %u", lCnt);
   printf("\nTotal output records: %u", lCnt);

   lRecCnt = lCnt;
   return 0;
}

/******************************** ChkMail2Situs *******************************
 *
 * This function test the record to see if mail addr is probably situs addr
 *    1) Owner occupy
 *    2) City name matches with Primary TRA
 *    3) No unit number
 *    4) In the same city with prev/next parcel (98888001)
 *    5) Write to log record occurs alone and street name/number not in the same
 *       pattern as prev/next record.
 *
 * Return >= 0 if mail addr can be situs.
 *
 ******************************************************************************/

int ChkMail2Situs(char *pBuf)
{
   int   iRet=-1, iPriTra, iTmp;

   // Test Owner occupy
   if (*(pBuf+OFF_HO_FL) != '1')
      return iRet;

   // Test CA zipcode
   if (*(pBuf+OFF_M_ZIP) != '9')
      return iRet;

   // Test Unit#
   if (*(pBuf+OFF_M_UNITNO) > ' ')
      return iRet;

   // Test local city name
   iTmp = 0;
   iPriTra = 0;
   while (CityTbl[iTmp].pName[0] >= 'A')
   {
      if (!memcmp(CityTbl[iTmp].pName, pBuf+OFF_M_CITY, strlen(CityTbl[iTmp].pName))  )
      {
         iPriTra = CityTbl[iTmp].iPriTra;
         break;
      }
      iTmp++;
   }

   // If not found, return
   if (!iPriTra)
      return iRet;

   iRet = atoin(pBuf+OFF_TRA, 3);

   // If TRA is matched, we got it
   if (iRet == iPriTra)
      iRet = iTmp;
   else
      iRet = -1;

   return iRet;
}

/*********************************** Mail2Situs *******************************
 *
 * This function first update situs addr (if blank) with mail addr based on:
 *    1) Owner occupy
 *    2) City name matches with Primary TRA
 *    3) No unit number
 *    4) In the same city with prev/next parcel (98888001)
 *    5) Write to log record occurs alone and street name/number not in the same
 *       pattern as prev/next record.
 *
 ******************************************************************************/

int Mail2Situs(char *pCnty, int iSkip)
{
   char     acRawFile[_MAX_PATH], acOutFile[_MAX_PATH], acXadrFile[_MAX_PATH], *pTmp;
   char     acBuf[2048], acAdrbuf[512], acTmp[256];
   int      iRet, iTmp, iAdrCnt, iCnt, iUpdCnt;
   FILE     *fdOut;
   HANDLE   fhIn, fhOut;
   unsigned long nBytesRead, nBytesWritten;
   XADR_REC *pRec = (XADR_REC *)&acAdrbuf;

   LogMsg0("Copy Mailing addr to Situs for HO exempt parcels");

   sprintf(acRawFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "R01");
   sprintf(acOutFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "U01");
   
   if (_access(acRawFile, 0))
   {
      LogMsg("***** Missing Input file [%s].  Please verify!", acRawFile);
      return -2;
   }

   // Open Input file
   LogMsg("Open input file %s", acRawFile);
   fhIn = CreateFile(acRawFile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhIn == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening input file: %s\n", acRawFile);
      return -3;
   }

   // Open Output file
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening output file: %s\n", acOutFile);
      return -4;
   }

   // Open Output addr file
   iRet = GetIniString(myCounty.acCntyCode, "XAdrFile", "", acXadrFile, _MAX_PATH, acIniFile);
   if (iRet < 10)
   {
      LogMsg("***** XAdrFile is not defined in INI section of [%s]", pCnty);
      return -1;
   }
   if (!(fdOut = fopen(acXadrFile, "w")))
   {
      LogMsg("***** Error creating new output file [%s]", acXadrFile);
      return -4;
   }

   if (iSkip > 0)
   {
      // Skip test record
      ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);
      WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
   }

   iAdrCnt = 0;
   iUpdCnt = 0;
   iCnt = 0;
   pRec->CRLF[0] = 10;
   pRec->CRLF[1] = 0;

   // Loop through
   while (ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL))
   {
#ifdef _DEBUG
      // 1720 PACIFIC COAST HWY UNIT 202
      //if (!memcmp(acBuf, "15935138", 9))
      //   iTmp = 0;
#endif
      if (!nBytesRead)
         break;

      memset(acAdrbuf, ' ', sizeof(XADR_REC)-2);      // Do not overwrite CRLF

      // Update situs only if it is blank
      iTmp = atoin(&acBuf[OFF_S_STRNUM], SIZ_S_STRNUM);
      if (!iTmp && acBuf[OFF_S_STREET] == ' ')
      {
         // Update Situs
         iRet = ChkMail2Situs(acBuf);
         if (iRet >= 0)
         {
            iUpdCnt++;
            pRec->FromSrc = 'M';
            iTmp = atoin(&acBuf[OFF_M_STRNUM], SIZ_M_STRNUM);
            sprintf(acTmp, "%*u", SIZ_S_STRNUM, iTmp);
            memcpy(&acBuf[OFF_S_STRNUM], acTmp, SIZ_S_STRNUM);
            memcpy(&acBuf[OFF_S_STR_SUB], &acBuf[OFF_M_STR_SUB], SIZ_M_STR_SUB);
            memcpy(&acBuf[OFF_S_DIR], &acBuf[OFF_M_DIR], SIZ_M_DIR);
            memcpy(&acBuf[OFF_S_STREET], &acBuf[OFF_M_STREET], SIZ_M_STREET);
            memcpy(&acBuf[OFF_S_UNITNO], &acBuf[OFF_M_UNITNO], SIZ_M_UNITNO);
            // Recode suffix
            if (acBuf[OFF_M_SUFF] > ' ')
            {
               int iSfxCode;
               
               memcpy(acTmp, &acBuf[OFF_M_SUFF], SIZ_M_SUFF);
               iSfxCode = GetSfxDev(myTrim(acTmp, SIZ_M_SUFF));
               if (iSfxCode > 0)
               {
                  iTmp = sprintf(acTmp, "%d", iSfxCode);
                  memcpy(&acBuf[OFF_S_SUFF], acTmp, iTmp);
               }
            }

            iRet = City2Code(CityTbl[iRet].pName, acTmp, acBuf);
            if (iRet > 0)
               memcpy(&acBuf[OFF_S_CITY], acTmp, 5);
            memcpy(&acBuf[OFF_S_ZIP], &acBuf[OFF_M_ZIP], SIZ_M_ZIP);
            memcpy(&acBuf[OFF_S_ADDR_D], &acBuf[OFF_M_ADDR_D], SIZ_M_ADDR_D);

            memcpy(acTmp, &acBuf[OFF_M_CTY_ST_D], SIZ_M_CTY_ST_D);
            iTmp = blankRem(acTmp, SIZ_M_CTY_ST_D);
            
            // Remove comma
            if ((pTmp = strchr(acTmp, ',')) && *(pTmp+1) != ' ')
               *pTmp = ' ';
            memcpy(&acBuf[OFF_S_CTY_ST_D], acTmp, iTmp);

            // Reload StrNum
            iTmp = atoin(&acBuf[OFF_S_STRNUM], SIZ_S_STRNUM);
         }
      }

      // Create address extract
      if (iTmp > 0)
      {
         memcpy(pRec->Apn, &acBuf[OFF_APN_S], SIZ_APN_S);
         memcpy(pRec->strNum, &acBuf[OFF_S_STRNUM], SIZ_S_ADDR);
         memcpy(acTmp, &acBuf[OFF_S_ADDR_D], SIZ_S_ADDR_D);
         iTmp = blankRem(acTmp, SIZ_S_ADDR_D);
         memcpy(pRec->Addr1, acTmp, iTmp);
         memcpy(pRec->Addr2, &acBuf[OFF_S_CTY_ST_D], SIZ_S_CTY_ST_D);
         fputs(acAdrbuf, fdOut);
         iAdrCnt++;
      }

      WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);

      if (!(++iCnt % 1000))
         printf("\r%u", iCnt);
   }

   if (fhIn)
      CloseHandle(fhIn);
   if (fhOut)
      CloseHandle(fhOut);
   if (fdOut)
      fclose(fdOut);

   LogMsg("Total records processed: %u", iCnt);
   LogMsg("      records extracted: %u", iAdrCnt);
   LogMsg("      situs updated:     %u", iUpdCnt);

   return iRet;

}

/******************************** Org_ChkCondoList ****************************
 *
 * Use this function to find the misalignment of condo sequence in condo list.
 * Based on input format of 2015 condo list from Travis.
 *
 ******************************************************************************/

int Org_ChkCondoList(char *pType)
{
   int   iBad, iCnt, iRet, iLast, iFirst;
   char  acBuf[1024], acTmp[_MAX_PATH], acLast[_MAX_PATH], acCur[_MAX_PATH], *pTmp;
   FILE  *fd;

   GetIniString(myCounty.acCntyCode, pType, "", acTmp, _MAX_PATH, acIniFile);
   if (acTmp[0] > ' ')
   {
      if (_access(acTmp, 0))
      {
         LogMsg("***** Missing %s file: %s.\nPlease check INI file", pType, acTmp);
         return -1;
      }
   } else
   {
      LogMsg("*** WARNING: Condo file is not defined in %s", acIniFile);
      return 0;
   }

   LogMsg("Test %s file %s", pType, acTmp);
   fd = fopen(acTmp, "r");
   if (fd == NULL)
   {
      LogMsg("***** Error opening %s file: %s\n", pType, acTmp);
      return -2;
   }

   // Get first entry
   pTmp = fgets(acBuf, 1024, fd);
   iRet = ParseStringNQ(acBuf, ' ', MAX_FLD_TOKEN, apTokens);
   strcpy(acLast, apTokens[3]);
   pTmp = strchr(acLast, '/');
   iLast = atol(++pTmp);

   iCnt = 1;
   iBad = 0;
   while (!feof(fd))
   {
      pTmp = fgets(acBuf, 1024, fd);
      if (!pTmp)
         break;

      iCnt++;
      iRet = ParseStringNQ(acBuf, ' ', MAX_FLD_TOKEN, apTokens);
      if (iRet < 2)
      {
         LogMsg("***** Bad input string at row# %d", iCnt);
         continue;
      }

      strcpy(acCur, apTokens[3]);
      pTmp = strrchr(acCur, '-');
      iFirst = atol(++pTmp);
      if (!memcmp(acCur, acLast, 7) && iFirst < iLast)
      {
         LogMsg("????? Misalignment found at map: %s project: %s", apTokens[2], acCur);
         iBad++;
      }

      strcpy(acLast, apTokens[3]);
      pTmp = strchr(acLast, '/');
      iLast = atol(++pTmp);
      if (iFirst > iLast)
      {
         LogMsg("????? Misalignment found at map: %s project: %s", apTokens[2], acCur);
         iBad++;
      }
   }

   LogMsg("Checking complete with %d misalignments from %d entries", iBad, iCnt);

   fclose(fd);
   return iBad;
}

/******************************** Org_ChkCondoList ****************************
 *
 * Use this function to find the misalignment of condo sequence in condo list.
 * Based on input format of 2012 condo list from Nick.
 *
 ******************************************************************************/

int Org_ChkCondoList1(char *pType)
{
   int   iBad, iCnt, iRet, iLast, iFirst;
   char  acBuf[1024], acTmp[_MAX_PATH], acLast[_MAX_PATH], acCur[_MAX_PATH], *pTmp;
   FILE  *fd;

   GetIniString(myCounty.acCntyCode, pType, "", acTmp, _MAX_PATH, acIniFile);
   if (acTmp[0] > ' ')
   {
      if (_access(acTmp, 0))
      {
         LogMsg("***** Missing %s file: %s.\nPlease check INI file", pType, acTmp);
         return -1;
      }
   } else
   {
      LogMsg("*** WARNING: Condo file is not defined in %s", acIniFile);
      return 0;
   }

   LogMsg("Test %s file %s", pType, acTmp);
   fd = fopen(acTmp, "r");
   if (fd == NULL)
   {
      LogMsg("***** Error opening %s file: %s\n", pType, acTmp);
      return -2;
   }

   // Skip header
   pTmp = fgets(acBuf, 1024, fd);

   // Get first entry
   pTmp = fgets(acBuf, 1024, fd);
   iRet = ParseStringNQ(acBuf, ',', MAX_FLD_TOKEN, apTokens);
   strcpy(acLast, apTokens[3]);
   pTmp = strchr(acLast, '/');
   iLast = atol(++pTmp);

   iCnt = 1;
   iBad = 0;
   while (!feof(fd))
   {
      pTmp = fgets(acBuf, 1024, fd);
      if (!pTmp)
         break;

      iCnt++;
      iRet = ParseStringNQ(acBuf, ',', MAX_FLD_TOKEN, apTokens);
      if (iRet < 5)
      {
         LogMsg("***** Bad input string at row# %d", iCnt);
         continue;
      }

      strcpy(acCur, apTokens[3]);
      pTmp = strrchr(acCur, '-');
      iFirst = atol(++pTmp);
      if (!memcmp(acCur, acLast, 7) && iFirst < iLast)
      {
         LogMsg("????? Misalignment found at map: %s project: %s", apTokens[2], acCur);
         iBad++;
      }

      strcpy(acLast, apTokens[3]);
      pTmp = strchr(acLast, '/');
      iLast = atol(++pTmp);
      if (iFirst > iLast)
      {
         LogMsg("????? Misalignment found at map: %s project: %s", apTokens[2], acCur);
         iBad++;
      }
   }

   LogMsg("Checking complete with %d misalignments from %d entries", iBad, iCnt);

   fclose(fd);
   return iBad;
}

double Org_Zone2Dec(char *pZoneValue, int iLen)
{
   char     sTmp[32], cChkVal;
   double   dRet;
   bool     bPositive=true;

   memcpy(sTmp, pZoneValue, iLen);
   cChkVal = sTmp[iLen-1];
   if (cChkVal >= 'A' &&  cChkVal <= 'I')
      sTmp[iLen-1] = (cChkVal & 0x0F) + 48;
   else if (cChkVal >= 'J' &&  cChkVal <= 'R')
   {
      bPositive = false;
      sTmp[iLen-1] = ((cChkVal-9) & 0x0F) + 48;
   } else
   {
      sTmp[iLen-1] = '0';
      if (cChkVal == '}')
         bPositive = false;
   }

   sTmp[iLen] = 0;
   dRet = (double)atol(sTmp)/100.0;

   if (!bPositive)
      dRet = -dRet;
   return dRet;
}

/***************************** Org_ParseTaxBase ******************************
 *
 * Parse record from current year tax file TC01CT01.TXT
 *
 *****************************************************************************/

int Org_ParseTaxBase(char *pBase, char *pInbuf)
{
   TAXBASE  *pBaseRec = (TAXBASE *)pBase;
   ORG_TAX  *pInRec   = (ORG_TAX *)pInbuf;
   char     acTmp[256];
   int      iTmp;

   // Clear output buffer
   memset(pBase, 0, sizeof(TAXBASE));

   // APN
   memcpy(pBaseRec->Apn, pInRec->Apn, SIZ_T_APN);

   // BillNum
#ifdef _DEBUG
   //if (!memcmp(pInRec->Apn, "01714322", 8))
   //   iTmp = 0;
#endif
   
   iTmp = sprintf(acTmp, "%.8s.%.2s", pInRec->Apn, pInRec->Apn_Suff);
   memcpy(pBaseRec->BillNum, acTmp, iTmp);

   // TRA
   
   // Tax Year
   sprintf(pBaseRec->TaxYear, "%d", lTaxYear);

   // Check for Tax amount
   double dTotalTax, dTotalDue, dTax1, dTax2, dTotalPaid;
   //dTax1 = Org_Zone2Dec(pInRec->Inst1_Amt, SIZ_T_AMT);
   //dTax2 = Org_Zone2Dec(pInRec->Inst2_Amt, SIZ_T_AMT);
   dTax1 = atoln(pInRec->Inst1_Amt, SIZ_T_AMT);
   dTax2 = atoln(pInRec->Inst2_Amt, SIZ_T_AMT);

   dTotalTax = dTax1+dTax2;
   dTotalPaid = 0;

   // Set default due date
   InstDueDate(pBaseRec->DueDate1, 1, lTaxYear);
   InstDueDate(pBaseRec->DueDate2, 2, lTaxYear);

   if (dTotalTax > 0.0)
   {
      sprintf(pBaseRec->TotalTaxAmt, "%.2f", dTotalTax);
      if (dTax1 > 0.0)
         sprintf(pBaseRec->TaxAmt1, "%.2f", dTax1);
      if (dTax2 > 0.0)
         sprintf(pBaseRec->TaxAmt2, "%.2f", dTax2);
   }

   // Paid
   if (pInRec->PaidDate1[0] > '0')
   {
      dTotalPaid = dTax1;
      sprintf(pBaseRec->PaidAmt1, "%.2f", dTax1);
      memcpy(pBaseRec->PaidDate1, pInRec->PaidDate1, SIZ_T_DATE);
      pBaseRec->Inst1Status[0] = TAX_BSTAT_PAID;
   } else
      pBaseRec->Inst1Status[0] = TAX_BSTAT_UNPAID;

   if (pInRec->PaidDate2[0] > '0')
   {
      dTotalPaid += dTax2;
      sprintf(pBaseRec->PaidAmt2, "%.2f", dTax2);
      memcpy(pBaseRec->PaidDate2, pInRec->PaidDate2, SIZ_T_DATE);
      pBaseRec->Inst2Status[0] = TAX_BSTAT_PAID;
   } else
      pBaseRec->Inst2Status[0] = TAX_BSTAT_UNPAID;

   dTotalDue = dTotalTax - dTotalPaid;
   if (dTotalDue > 0.0)
      sprintf(pBaseRec->TotalDue, "%.2f", dTotalDue);

   pBaseRec->BillType[0] = BILLTYPE_SECURED;
   pBaseRec->isSecd[0] = '1';
   pBaseRec->isSupp[0] = '0';

   return 0;
}

/**************************** Org_Load_TaxBase *******************************
 *
 * Create Base & Delq import file and import into SQL if specified
 *
 * Return 0 if success.
 *
 *****************************************************************************/

int Org_Load_TaxBase(bool bImport)
{
   char     *pTmp, acBaseRec[MAX_RECSIZE], acRec[512];
   char     acBaseFile[_MAX_PATH], acDelqFile[_MAX_PATH], acInFile[_MAX_PATH];

   int      iRet;
   long     lBase=0, lCnt=0, lDelq=0;
   FILE     *fdBase, *fdDelq, *fdIn, *fdR01;
   TAXBASE  *pTaxBase = (TAXBASE *)&acBaseRec[0];

   LogMsg("Loading Tax file");

   GetIniString(myCounty.acCntyCode, "CurrTax", "", acInFile, _MAX_PATH, acIniFile);
   lLastTaxFileDate = getFileDate(acInFile);
   // Only process if new tax file
   iRet = isNewTaxFile(NULL, myCounty.acCntyCode);
   if (iRet <= 0)
   {
      lLastTaxFileDate = 0;
      return iRet;
   }

   NameTaxCsvFile(acBaseFile, myCounty.acCntyCode, "Base");
   NameTaxCsvFile(acDelqFile, myCounty.acCntyCode, "Delq");
   pTmp = strrchr(acBaseFile, '\\');
   *pTmp = 0;
   if (_access(acBaseFile, 0))
      _mkdir(acBaseFile);
   *pTmp = '\\';

   // Open input file
   LogMsg("Open Tax file %s", acInFile);
   fdIn = fopen(acInFile, "r");
   if (fdIn == NULL)
   {
      LogMsg("***** Error opening Tax file: %s\n", acInFile);
      return -2;
   }  

   // Open Delq file
   LogMsg("Open Delq file %s", acDelqFile);
   fdDelq = fopen(acDelqFile, "w");
   if (fdDelq == NULL)
   {
      LogMsg("***** Error creating Delq file: %s\n", acDelqFile);
      return -4;
   }

   // Open base file
   LogMsg("Open Base file %s", acBaseFile);
   fdBase = fopen(acBaseFile, "w");
   if (fdBase == NULL)
   {
      LogMsg("***** Error creating base file: %s\n", acBaseFile);
      return -4;
   }

   // Open raw file
   fdR01 = OpenR01(myCounty.acCntyCode);

   // Merge loop 
   while (!feof(fdIn))
   {
      pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdIn);
      if (!pTmp)
         break;

      if (*pTmp > '9')
         continue;

      // Create new R01 record
      iRet = Org_ParseTaxBase(acBaseRec, acRec);
      if (!iRet)
      {
         // Update TRA
         if (fdR01)
            UpdateTRA(acBaseRec, fdR01);

         // Create TaxBase record
         Tax_CreateTaxBaseCsv(acRec, (TAXBASE *)&acBaseRec);
         lBase++;
         fputs(acRec, fdBase);

         // Create Delq record
         //if (acDelqRec[0] > ' ')
         //{
         //   Tax_CreateDelqCsv(acRec, (TAXDELQ *)&acDelqRec);
         //   fputs(acRec, fdDelq);
         //   lDelq++;
         //}
      } else
      {
         LogMsg0("---> Drop record %d [%.80s]", lCnt, acRec); 
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdIn)
      fclose(fdIn);
   if (fdBase)
      fclose(fdBase);
   if (fdDelq)
      fclose(fdDelq);
   if (fdR01)
      fclose(fdR01);

   LogMsg("Total records processed:    %u", lCnt);
   LogMsg("Total Base records:         %u", lBase);
   LogMsg("Total Delq records:         %u", lDelq);
   LogMsg("Total TRA update:           %u", iApnMatch);

   // Import into SQL
   if (bImport)
   {
      iRet = doTaxImport(myCounty.acCntyCode, TAX_BASE);
      //if (!iRet)
      //   iRet = doTaxImport(myCounty.acCntyCode, TAX_DELINQUENT);
   } else
      iRet = 0;

   return iRet;
}


/***************************** Org_ParseTaxDetail ****************************
 *
 * Parsing Special Assessment record
 *
 *****************************************************************************/

int Org_ParseTC01CT02(char *pDetailBuf, char *pAgencyBuf, char *pInbuf)
{
   TAXDETAIL   *pDetail = (TAXDETAIL *)pDetailBuf;
   TAXAGENCY   *pAgency = (TAXAGENCY *)pAgencyBuf, *pResult;
   ORG_SPCASMT *pInRec   = (ORG_SPCASMT *)pInbuf;
   double      dTmp;

   // Clear output buffer
   memset(pDetailBuf, 0, sizeof(TAXDETAIL));
   memset(pAgencyBuf, 0, sizeof(TAXAGENCY));

   // APN
   memcpy(pDetail->Apn, pInRec->Apn, SIZ_T_APN);

   // Tax Year
   sprintf(pDetail->TaxYear, "%d", lTaxYear);

   // Tax amt - accepting both pos and neg values
   dTmp = Org_Zone2Dec(pInRec->TaxAmt1, SIZ_T_AMT);
   dTmp += Org_Zone2Dec(pInRec->TaxAmt2, SIZ_T_AMT);
   sprintf(pDetail->TaxAmt, "%.2f", dTmp);

   // Agency 
   memcpy(pDetail->TaxCode, pInRec->TaxType, SIZ_T3_TAXTYPE);
#ifdef _DEBUG
   //if (!memcmp(pDetail->Apn, "00101126", iApnLen))
   //   dTmp = 0;
#endif

   if (pDetail->TaxCode[0] > ' ')
   {
      pResult = findTaxAgency(pDetail->TaxCode);
      if (pResult)
      {
         strcpy(pAgency->Code, myTrim(pDetail->TaxCode));
         strcpy(pAgency->Agency, pResult->Agency);
         strcpy(pAgency->Phone, pResult->Phone);
      } else
      {
         pAgency->Agency[0] = 0;
         LogMsg("+++ Unknown TaxCode: %s [%s]", pDetail->TaxCode, pDetail->Apn);
      }
   } 

   return 0;
}

/**************************** Org_Load_TC01CT02 ******************************
 *
 * Loading TC01CT02.txt to populate Tax_Items & Tax_Agency
 *
 * Return 0 if success.
 *
 *****************************************************************************/

int Org_Load_TC01CT02(char *pInfile, bool bImport)
{
   char     *pTmp, acItemsRec[MAX_RECSIZE], acAgencyRec[512], acRec[512];
   char     acItemsFile[_MAX_PATH], acAgencyFile[_MAX_PATH], acTmpFile[_MAX_PATH];

   int      iRet;
   long     lItems=0, lCnt=0, lAgency=0;
   FILE     *fdItems, *fdAgency, *fdIn;
   TAXDETAIL   *pTax    = (TAXDETAIL *)&acItemsRec[0];
   TAXAGENCY   *pAgency = (TAXAGENCY *)&acAgencyRec[0];
   ORG_SPCASMT *pSpcAsmt= (ORG_SPCASMT *)&acRec[0];

   LogMsg("Loading Detail file");

   NameTaxCsvFile(acItemsFile, myCounty.acCntyCode, "Items");
   NameTaxCsvFile(acAgencyFile, myCounty.acCntyCode, "Agency");
   sprintf(acTmpFile, "%s\\%s\\%s_Agency.tmp", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);

   // Open input file
   LogMsg("Open Detail file %s", pInfile);
   fdIn = fopen(pInfile, "r");
   if (fdIn == NULL)
   {
      LogMsg("***** Error opening Detail file: %s\n", pInfile);
      return -2;
   }  

   // Open Items file
   LogMsg("Create Items file %s", acItemsFile);
   fdItems = fopen(acItemsFile, "w");
   if (fdItems == NULL)
   {
      LogMsg("***** Error creating Items file: %s\n", acItemsFile);
      return -4;
   }

   // Open Agency file
   LogMsg("Create Agency file %s", acTmpFile);
   fdAgency = fopen(acTmpFile, "w");
   if (fdAgency == NULL)
   {
      LogMsg("***** Error creating Agency file: %s\n", acTmpFile);
      return -4;
   }

   // Skip
   do {
      pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdIn);
   } while (pTmp && *pTmp < '3');

   // Merge loop 
   while (!feof(fdIn))
   {
      if (pSpcAsmt->Apn[0] <= '9' && pSpcAsmt->Apn[0] >= '0')
      {
         // Create Items & Agency record
         iRet = Org_ParseTC01CT02(acItemsRec, acAgencyRec, acRec);
         if (!iRet)
         {
            // Create TaxBase record
            Tax_CreateDetailCsv(acRec, (TAXDETAIL *)&acItemsRec);
            lItems++;
            fputs(acRec, fdItems);

            // Create Agency record
            if (pAgency->Agency[0] > ' ')
            {
               Tax_CreateAgencyCsv(acRec, (TAXAGENCY *)&acAgencyRec);
               fputs(acRec, fdAgency);
            }
         } else
         {
            LogMsg0("---> Drop record %d [%.80s]", lCnt, acRec); 
         }
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);

      pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdIn);
   }

   // Close files
   if (fdIn)
      fclose(fdIn);
   if (fdItems)
      fclose(fdItems);
   if (fdAgency)
      fclose(fdAgency);

   LogMsg("Total records processed:    %u", lCnt);
   LogMsg("Total Items records:        %u", lItems);

   // Import into SQL
   if (bImport)
   {
      iRet = doTaxImport(myCounty.acCntyCode, TAX_DETAIL);
      if (!iRet)
      {
         // Dedup Agency file before import
         LogMsg("Dedup Agency file %s to %s", acTmpFile, acAgencyFile);
         iRet = sortFile(acTmpFile, acAgencyFile, "S(#1,C,A) DUPOUT F(TXT)");
         if (iRet > 0)
            iRet = doTaxImport(myCounty.acCntyCode, TAX_AGENCY);
      }
   } else
      iRet = 0;

   return iRet;
}

/***************************** Org_ParseTC04TY30 *****************************
 *
 * Generate TAXDETAIL & TAXAGENCY
 *
 * Return 0 if success, 1 if no tax amount
 *
 *****************************************************************************/

int Org_ParseTC04TY30(char *pDetailBuf, char *pAgencyBuf)
{
   int      iRet=1;
   double   dTmp;
   char     acTmp[256];
   TAXDETAIL *pDetail = (TAXDETAIL *)pDetailBuf;
   TAXAGENCY *pAgency = (TAXAGENCY *)pAgencyBuf, *pResult;

   memset(pDetailBuf, 0, sizeof(TAXDETAIL));
   memset(pAgencyBuf, 0, sizeof(TAXAGENCY));

   // APN
   strcpy(pDetail->Apn, myTrim(apTokens[ORG_TD_APN]));

   // BillNum
   int iTmp = sprintf(acTmp, "%.8s.00", apTokens[ORG_TD_APN]);
   memcpy(pDetail->BillNum, acTmp, iTmp);

   // Tax Year
   sprintf(pDetail->TaxYear, "%d", lTaxYear);

   // Agency 
   strcpy(pDetail->TaxCode, myTrim(apTokens[ORG_TD_CODE]));
   strcpy(pAgency->Code, pDetail->TaxCode);

   // Tax Desc
   strcpy(pAgency->Agency, myTrim(apTokens[ORG_TD_AGENCY]));

   // Tax Rate
   dTmp =  atof(apTokens[ORG_TD_RATE]);
   if (dTmp > 0.0)
   {
      // Remove rate from agency table 01/30/2022 - spn
      //sprintf(pAgency->TaxRate, "%.6f", dTmp);
      sprintf(pDetail->TaxRate, "%.6f", dTmp);
   }

   // Tax Amt
   dTmp =  atof(apTokens[ORG_TD_AMOUNT]);
   if (dTmp != 0.0)
   {
      sprintf(pDetail->TaxAmt, "%.2f", dTmp);
      iRet = 0;
   } else if (!_memicmp(apTokens[ORG_TD_AGENCY], "BASIC LEVY RATE", 10))
      iRet = 0;

   if (pAgency->Code[0] > ' ')
   {
      pResult = findTaxDist(pAgency->Agency, pAgency->Code);
      if (pResult)
      {
         strcpy(pDetail->TaxCode, pResult->Code);
         strcpy(pAgency->Code, pResult->Code);
         strcpy(pAgency->Phone, pResult->Phone);
         pAgency->TC_Flag[0] = pResult->TC_Flag[0];
         pDetail->TC_Flag[0] = pResult->TC_Flag[0];
      } else
      {
         pAgency->Agency[0] = 0;
         LogMsg("+++ Unknown TaxCode: %s [%s]", pDetail->TaxCode, pDetail->Apn);
      }
   } 

   return iRet;
}

/**************************** Org_Load_TC04TY30 ******************************
 *
 * Loading TC04TY30.txt to populate Tax_Items & Tax_Agency
 * This file includes TRA and all direct assessments not available in TC01CT02.
 *
 * Return 0 if success.
 *
 *****************************************************************************/

int Org_Load_TC04TY30(char *pInfile, bool bImport)
{
   char     *pTmp, acItemsRec[MAX_RECSIZE], acAgencyRec[512], acRec[512],
            acItemsFile[_MAX_PATH], acAgencyFile[_MAX_PATH], acTmpFile[_MAX_PATH];

   int      iRet;
   long     lItems=0, lCnt=0, lAgency=0;
   bool     isDup;

   FILE     *fdItems, *fdAgency, *fdIn;
   TAXDETAIL   *pTax    = (TAXDETAIL *)&acItemsRec[0];
   TAXAGENCY   *pAgency = (TAXAGENCY *)&acAgencyRec[0];

   LogMsg("Loading Detail file");


   // Sort input file
   sprintf(acTmpFile, "%s\\%s\\%s_Detail.srt", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   LogMsg("Sort detail file %s to %s", pInfile, acTmpFile);
   iRet = sortFile(pInfile, acTmpFile, "S(#1,C,A,#3,C,A,#6,C,D) OMIT(#1,C,GE,\"A\") F(TXT)");
   if (iRet < 1000000)
      return -1;

   // Open input file
   LogMsg("Open Detail file %s", acTmpFile);
   fdIn = fopen(acTmpFile, "r");
   if (fdIn == NULL)
   {
      LogMsg("***** Error opening Detail file: %s\n", acTmpFile);
      return -2;
   }  

   NameTaxCsvFile(acItemsFile, myCounty.acCntyCode, "Items");
   NameTaxCsvFile(acAgencyFile, myCounty.acCntyCode, "Agency");
   sprintf(acTmpFile, "%s\\%s\\%s_Agency.tmp", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);

   // Open Items file
   LogMsg("Create Items file %s", acItemsFile);
   fdItems = fopen(acItemsFile, "w");
   if (fdItems == NULL)
   {
      LogMsg("***** Error creating Items file: %s\n", acItemsFile);
      return -4;
   }

   // Open Agency file
   LogMsg("Create Agency file %s", acTmpFile);
   fdAgency = fopen(acTmpFile, "w");
   if (fdAgency == NULL)
   {
      LogMsg("***** Error creating Agency file: %s\n", acTmpFile);
      return -4;
   }

   // Skip
   do {
      pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdIn);
   } while (pTmp && !isdigit(acRec[1]));

   // Merge loop 
   while (!feof(fdIn))
   {
      // Split record
      pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdIn);
      if (!pTmp)
         break;

      // Parse input
      iTokens = ParseStringNQ(acRec, ',', ORG_TD_FLDS, apTokens);
      if (iTokens < ORG_TD_FLDS)
      {
         LogMsg("*** Bad record: %s", apTokens[ORG_TD_APN]);
         continue;
      }

      if (*apTokens[ORG_TD_APN] <= '9' && *apTokens[ORG_TD_APN] >= '0')
      {
#ifdef _DEBUG
         // One agency bill at 2 different rates: 02301018
         // Multiple bills: 01714322
         //if (!memcmp(apTokens[ORG_TD_APN], "01714322", iApnLen))
         //   iRet = 0;
#endif

         // Strip off detail records of extra bill since they are not parts of original bill
         // And since they have no apn suffix, we don't know which bill they are for.
         if (!memcmp(apTokens[ORG_TD_APN], pTax->Apn, iApnLen) && 
             !memcmp(apTokens[ORG_TD_AGENCY], pAgency->Agency, strlen(pAgency->Agency)))
         {
            if (!memcmp(apTokens[ORG_TD_AGENCY], "BASIC LEVY RATE", 15))
               isDup = true;
            if (isDup)
               continue;
         } else if (memcmp(apTokens[ORG_TD_APN], pTax->Apn, iApnLen))
            isDup = false;

         // Create Items & Agency record
         iRet = Org_ParseTC04TY30(acItemsRec, acAgencyRec);
         if (!iRet)
         {
            // Create TaxBase record
            Tax_CreateDetailCsv(acRec, (TAXDETAIL *)&acItemsRec);
            lItems++;
            fputs(acRec, fdItems);

            // Create Agency record
            if (pAgency->Agency[0] > ' ')
            {
               Tax_CreateAgencyCsv(acRec, (TAXAGENCY *)&acAgencyRec);
               fputs(acRec, fdAgency);
            }
         } else if (bDebug)
         {
            LogMsg0("---> No TaxAmt at APN=%s, Amt=%s, Agency=%s", apTokens[ORG_TD_APN], apTokens[ORG_TD_AMOUNT], apTokens[ORG_TD_AGENCY]); 
         }
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdIn)
      fclose(fdIn);
   if (fdItems)
      fclose(fdItems);
   if (fdAgency)
      fclose(fdAgency);

   LogMsg("Total records processed:    %u", lCnt);
   LogMsg("Total Items records:        %u", lItems);

   // Import into SQL
   if (bImport)
   {
      iRet = doTaxImport(myCounty.acCntyCode, TAX_DETAIL);
      if (!iRet)
      {
         // Dedup Agency file before import
         LogMsg("Dedup Agency file %s to %s", acTmpFile, acAgencyFile);
         iRet = sortFile(acTmpFile, acAgencyFile, "S(#1,C,A) DUPOUT(B2048, #1) DEL(124) F(TXT)");
         if (iRet > 0)
            iRet = doTaxImport(myCounty.acCntyCode, TAX_AGENCY);
      }
   } else
      iRet = 0;

   return iRet;
}

/****************************** Org_UpdateDfltAmt ****************************
 *
 * Parse record type 7
 *
 * Return SeqNum
 *
 *****************************************************************************/

int Org_UpdateDfltAmt(char *pOutbuf, char *pInbuf)
{
   static   double dTaxAmt=0.0;  // reset this value when SeqNum=1
   int      iSeq, iIdx;
   TAXDELQ     *pOutRec= (TAXDELQ *)pOutbuf;
   ORG_TAXITEM *pInRec = (ORG_TAXITEM *)pInbuf;

   iSeq = atoin(pInRec->SeqNum, SIZ_TD_SEQNUM);

   // Tax Year
   if (iSeq == 1)
   {
      dTaxAmt = 0.0;
      memcpy(pOutRec->TaxYear, pInRec->TaxYear, SIZ_TD_YEAR);
   }

   iIdx = 0;
   while (pInRec->asTaxAsmt[iIdx].TaxCode[0] > '0')
   {
      //dTaxAmt += Org_Zone2Dec(pInRec->asTaxAsmt[iIdx].TaxAmt, SIZ_T_AMT);
      dTaxAmt += atoln(pInRec->asTaxAsmt[iIdx].TaxAmt, SIZ_T_AMT);
      iIdx++;
   }

   sprintf(pOutRec->Def_Amt, "%.2f", dTaxAmt);

   return iSeq;
}

int Org_ParseDelq(char *pOutbuf, char *pInbuf)
{
   int      iTmp, lRedDate, lDefDate;
   TAXDELQ  *pOutRec= (TAXDELQ *)pOutbuf;
   ORG_DFLT *pInRec = (ORG_DFLT *)pInbuf;

   // Process active parcel only
   if (pInRec->RecStsCde != 'A')
      return -1;

   // Clear output buffer
   memset(pOutbuf, 0, sizeof(TAXDELQ));
   lRedDate=lDefDate = 0;

   // APN
   memcpy(pOutRec->Apn, pInRec->Apn, SIZ_TD_APN);

   // TDN
   memcpy(pOutRec->Default_No, pInRec->Default_No, SIZ_TD_TDN);

   // Tax Status
   pOutRec->isDelq[0] = '0';
   iTmp = atoin(pInRec->DelqStatus, SIZ_TD_TDN_STS_CDE);
   switch (iTmp)
   {
      case 10:    // Unpaid
         pOutRec->DelqStatus[0] = TAX_STAT_UNPAID;
         pOutRec->isDelq[0] = '1';
         break;
      case 50:    // Active Installment Plan
         pOutRec->DelqStatus[0] = TAX_STAT_ONPAYMENT;
         break;
      case 60:    // Defaulted Installment Plan
         pOutRec->DelqStatus[0] = TAX_STAT_DFLTINST;
         pOutRec->isDelq[0] = '1';
         break;
      case 90:    // Paid
         pOutRec->DelqStatus[0] = TAX_STAT_REDEEMED;
         break;
      case 95:    // Cancelled
         pOutRec->DelqStatus[0] = TAX_STAT_CANCEL;
         break;
   };

   // Updated date
   sprintf(pOutRec->Upd_Date, "%d", lLastTaxFileDate);

   // Redeemed?
   if (pInRec->Paid_Flag == 'P')
      pOutRec->DelqStatus[0] = TAX_STAT_REDEEMED;

   return 0;
}

/****************************** Org_UpdateRed ********************************
 *
 * Parse record type 9
 *
 * Return 0
 *
 *****************************************************************************/

int Org_UpdateRed(char *pOutbuf, char *pInbuf)
{
   double      dTmp, dTotal;
   TAXPAID     *pOutRec= (TAXPAID *)pOutbuf;
   ORG_TAXPAID *pInRec = (ORG_TAXPAID *)pInbuf;

   // Paid tax amount
   dTmp = atofn(pInRec->PaidTaxAmt, SIZ_T_AMT);
   if (dTmp > 0.0)
   {
      sprintf(pOutRec->TaxAmt, "%.2f", dTmp);
      dTotal = dTmp;
   } else
   {
      LogMsg("---> Neg tax amt: %.10s", pInRec->Apn);
      return -1;
   }

   // Paid pen amount
   dTmp = atofn(pInRec->PaidPenAmt, SIZ_T_AMT);
   if (dTmp > 0.0)
   {
      sprintf(pOutRec->PenAmt, "%.2f", dTmp);
      dTotal += dTmp;
   } else if (dTmp < 0.0)
   {
      LogMsg("---> Neg pen amt: %.10s", pInRec->Apn);
      return -1;
   }

   // Paid fee amount
   dTmp = atofn(pInRec->Redemp_Fee, SIZ_TD_REDEMP_FEE);
   if (dTmp > 0.0)
   {
      sprintf(pOutRec->FeeAmt, "%.2f", dTmp);
      dTotal += dTmp;
   } else if (dTmp < 0.0)
   {
      LogMsg("---> Neg fee amt: %.10s", pInRec->Apn);
      return -1;
   }

   // Paid date
   memcpy(pOutRec->PaidDate, pInRec->PaidDate, SIZ_T_DATE);

   if (dTotal > 0.0)
      sprintf(pOutRec->PaidAmt, "%.2f", dTotal);

   return 0;
}

/**************************** Org_Load_TaxDelq *****************************
 *
 * Load TC02CA01.txt.  Only process record type 1 & 7.  Type 4 & 9 will be 
 * processed by different processes.
 *
 ***************************************************************************/

int Org_Load_TaxDelq(bool bImport)
{
   char     acOutFile[_MAX_PATH], acDelqFile[_MAX_PATH], acBuf[512], acRec[512], *pTmp;
   char     sApn[32], acTmp[512];
   int      iRet, lCnt=0, lOut=0, iDrop=0;
   FILE     *fdIn, *fdOut;
   ORG_DFLT    *pDefRec = (ORG_DFLT *)&acRec[0];
   ORG_TAXITEM *pChgRec = (ORG_TAXITEM *)&acRec[0];

   // Get input file
   GetIniString(myCounty.acCntyCode, "TaxDelq", "", acDelqFile, _MAX_PATH, acIniFile);
   if (_access(acDelqFile, 0))
      return 1;

   LogMsg0("Load Tax Delinquent file");

   // Open input file
   LogMsg("Open tax delinquent file %s", acDelqFile);
   fdIn = fopen(acDelqFile, "r");
   if (fdIn == NULL)
   {
      LogMsg("***** Error opening delinquent file: %s\n", acDelqFile);
      return -2;
   }  
   lLastTaxFileDate = getFileDate(acDelqFile);

   // Open output file
   NameTaxCsvFile(acOutFile, myCounty.acCntyCode, "Delq");
   LogMsg("Open output file %s", acOutFile);
   fdOut = fopen(acOutFile, "w");
   if (fdOut == NULL)
   {
      LogMsg("***** Error creating output file: %s\n", acOutFile);
      return -4;
   }

   // Get first record
   do {
      pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdIn);
   } while (*pTmp == 'P');

   // Merge loop 
   while (!feof(fdIn))
   {
      if (!pTmp || *pTmp < ' ')
         break;
      if (pDefRec->RecType == '9')
      {
         pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdIn);
         continue;
      }

      CHK_DEFREC:
      // Process active parcel only
      if (pDefRec->RecType == '1' && pDefRec->RecStsCde != 'A')
      {
         memcpy(sApn, pDefRec->Apn, SIZ_T_APN);

         // Skip all records with same APN
         do {
            pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdIn);
         } while (pTmp && !memcmp(sApn, pDefRec->Apn, SIZ_T_APN));

         goto CHK_DEFREC;
      }

#ifdef _DEBUG
      //if (!memcmp(pDefRec->Apn, "00103349", 8)) // || !memcmp(pDefRec->Apn, "00127306", 8) || !memcmp(pDefRec->Apn, "06657107", 8))
      //   iRet = 0;
#endif
      // Create new TAXDELQ record type 1
      iRet = Org_ParseDelq(acBuf, acRec);
      if (!iRet)
      {
         // Skip legal record type 4
         pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdIn);
         while (pTmp && pDefRec->RecType == '4')
            pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdIn);

         // Parse annual assessments
         while (pTmp && pDefRec->RecType == '7')
         {
            iRet = Org_UpdateDfltAmt(acBuf, acRec);
            pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdIn);

            if (pTmp && pChgRec->RecType == '7' && !memcmp(pChgRec->SeqNum, "01", 2))
            {
               Tax_CreateDelqCsv(acTmp, (TAXDELQ *)&acBuf);
               lOut++;
               fputs(acTmp, fdOut);
            }
         }

         // Create delimited record
         Tax_CreateDelqCsv(acTmp, (TAXDELQ *)&acBuf);

         // Output record			
         lOut++;
         fputs(acTmp, fdOut);
      } else 
      {
         if (acRec[0] != 'P')
         {
            iDrop++;
            if (bDebug)
               LogMsg("---> Drop record %d [%.40s]", lCnt, acRec); 
         }
         pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdIn);
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdIn)
      fclose(fdIn);
   if (fdOut)
      fclose(fdOut);

   LogMsg("Total records processed:      %u", lCnt);
   LogMsg("Total records dropped (paid): %u", iDrop);
   LogMsg("Total output records:         %u", lOut);

   // Import into SQL
   if (bImport)
      iRet = doTaxImport(myCounty.acCntyCode, TAX_DELINQUENT);
   else
      iRet = 0;

   return iRet;
}

/**************************** Org_ParsePayment *****************************
 *
 * Parse record type 9 of TC02CA01.txt.    
 *
 ***************************************************************************/

int Org_ParsePayment(char *pOutbuf, char *pInbuf)
{
   double      dTmp, dTotal;
   TAXPAID     *pOutRec= (TAXPAID *)pOutbuf;
   ORG_TAXPAID *pInRec = (ORG_TAXPAID *)pInbuf;

   // Clear output buffer
   memset(pOutbuf, 0, sizeof(TAXPAID));

#ifdef _DEBUG
      //if (!memcmp(pInRec->Apn, "01829414", 8) )
      //   dTmp = 0;
#endif

   // Paid tax amount
   //dTmp = Org_Zone2Dec(pInRec->PaidTaxAmt, SIZ_T_AMT);
   dTmp = atofn(pInRec->PaidTaxAmt, SIZ_T_AMT);
   if (dTmp > 0.0)
   {
      sprintf(pOutRec->TaxAmt, "%.2f", dTmp);
      dTotal = dTmp;
   } else
   {
      LogMsg("---> Neg tax amt: %.10s", pInRec->Apn);
      return -1;
   }

   // Paid pen amount
   dTmp = atofn(pInRec->PaidPenAmt, SIZ_T_AMT);
   if (dTmp > 0.0)
   {
      sprintf(pOutRec->PenAmt, "%.2f", dTmp);
      dTotal += dTmp;
   } else if (dTmp < 0.0)
   {
      LogMsg("---> Neg pen amt: %.10s", pInRec->Apn);
      return -1;
   }

   // Paid fee amount
   dTmp = atofn(pInRec->Redemp_Fee, SIZ_TD_REDEMP_FEE);
   if (dTmp > 0.0)
   {
      sprintf(pOutRec->FeeAmt, "%.2f", dTmp);
      dTotal += dTmp;
   } else if (dTmp < 0.0)
   {
      LogMsg("---> Neg fee amt: %.10s", pInRec->Apn);
      return -1;
   }

   // APN
   memcpy(pOutRec->Apn, pInRec->Apn, SIZ_TD_APN);

   // TDN
   memcpy(pOutRec->Default_No, pInRec->TDN, SIZ_TD_TDN);

   // Paid date
   memcpy(pOutRec->PaidDate, pInRec->PaidDate, SIZ_T_DATE);

   if (dTotal > 0.0)
      sprintf(pOutRec->PaidAmt, "%.2f", dTotal);

   return 0;
}

/**************************** Org_Load_TaxPaid *****************************
 *
 * Load TC02CA01.txt.  Only process record type 9 (Tax payment).  
 *
 ***************************************************************************/

int Org_Load_TaxPaid(bool bImport)
{
   char     acOutFile[_MAX_PATH], acDelqFile[_MAX_PATH], acBuf[512], acRec[512], *pTmp;
   char     sApn[32], acTmp[512];
   int      iRet, lCnt=0, lOut=0;
   FILE     *fdIn, *fdOut;
   ORG_DFLT    *pDefRec = (ORG_DFLT *)&acRec[0];
   ORG_TAXPAID *pPaidRec = (ORG_TAXPAID *)&acRec[0];

   LogMsg0("Loading Delq payment information");
   GetIniString(myCounty.acCntyCode, "TaxDelq", "", acDelqFile, _MAX_PATH, acIniFile);

   // Open input file
   LogMsg("Open tax delinquent file %s", acDelqFile);
   fdIn = fopen(acDelqFile, "r");
   if (fdIn == NULL)
   {
      LogMsg("***** Error opening Secured tax file: %s\n", acDelqFile);
      return -2;
   }  
   lLastTaxFileDate = getFileDate(acDelqFile);

   // Open output file
   NameTaxCsvFile(acOutFile, myCounty.acCntyCode, "Paid");
   LogMsg("Open output file %s", acOutFile);
   fdOut = fopen(acOutFile, "w");
   if (fdOut == NULL)
   {
      LogMsg("***** Error creating output file: %s\n", acOutFile);
      return -4;
   }

   // Get first record
   do {
      pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdIn);
   } while (*pTmp == 'P');

   // Merge loop 
   while (!feof(fdIn))
   {
      if (!pTmp || *pTmp < ' ')
         break;

      CHK_DEFREC:
      // Process active parcel only
      if (pDefRec->RecType == '1' && pDefRec->RecStsCde != 'A')
      {
         memcpy(sApn, pDefRec->Apn, SIZ_T_APN);

         // Skip all records with same APN
         do {
            pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdIn);
         } while (!memcmp(sApn, pDefRec->Apn, SIZ_T_APN));

         goto CHK_DEFREC;
      }

#ifdef _DEBUG
      //if (!memcmp(pDefRec->Apn, "30218200", 8) || !memcmp(pDefRec->Apn, "00127306", 8) || !memcmp(pDefRec->Apn, "06657107", 8))
      //   iRet = 0;
#endif
      // Skip to '9'
      while (pTmp && pDefRec->RecType != '9')
      {
         pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdIn);

         // If no payment, get out
         if (pTmp && pDefRec->RecType == '1')
            break;
      }

      // Parse paid info
      while (pTmp && pDefRec->RecType == '9')
      {
         iRet = Org_ParsePayment(acBuf, acRec);
         if (!iRet)
         {
            Tax_CreatePaidCsv(acTmp, (TAXPAID *)&acBuf);
            lOut++;
            fputs(acTmp, fdOut);
         }

         // Get next rec
         pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdIn);
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdIn)
      fclose(fdIn);
   if (fdOut)
      fclose(fdOut);

   LogMsg("Total records processed:    %u", lCnt);
   LogMsg("Total output records:       %u", lOut);

   // Import into SQL
   if (bImport)
      iRet = doTaxImport(myCounty.acCntyCode, TAX_PAID);
   else
      iRet = 0;

   return iRet;
}

/***************************** Org_ParseTaxSupp ******************************
 *
 *
 *****************************************************************************/

int Org_ParseTaxSupp(char *pOutbuf, char *pInbuf)
{
   char     acTmp[256];
   int      iTmp, iRet=0;
   int      iTaxAmt1, iTaxAmt2, iTotalTax;

   TAXBASE  *pOutRec = (TAXBASE *) pOutbuf;
   ORG_SUPP *pInRec  = (ORG_SUPP *)pInbuf;

   // Clear output buffer
   memset(pOutbuf, 0, sizeof(TAXBASE));
   iTaxAmt1=iTaxAmt2=iTotalTax=0;

   // APN
   memcpy(pOutRec->Apn, pInRec->Apn, SIZ_TS_APN);

   // BillNum
   iTmp = sprintf(acTmp, "%.8s.%.4s", pInRec->Apn, pInRec->Apn_Suf);
   memcpy(pOutRec->BillNum, acTmp, iTmp);

   // Asmt Year 
   memcpy(pOutRec->TaxYear, pInRec->Asm_Year, SIZ_TS_ASM_YEAR);

   // Event Date

#ifdef _DEBUG
   //if (!memcmp(pInRec->Apn, "00103403", 8))
   //   iTmp = 0;
#endif

   // Check for first installment amount - for dollar amount, last byte may need translation
   // }=0, J=1, K=2, L=3, M=4, ...
   bool bRet = chkSignValue(pInRec->Fst_Ins_Amt, SIZ_TS_INS_AMT);
   iTaxAmt1 = atoin(pInRec->Fst_Ins_Amt, SIZ_TS_INS_AMT);
   if (iTaxAmt1 > 0)
   {
      if (bRet)
         iTaxAmt1 = -iTaxAmt1;
      sprintf(pOutRec->TaxAmt1, "%.2f", (double)iTaxAmt1/100);
   }
   
   bRet = chkSignValue(pInRec->Snd_Ins_Amt, SIZ_TS_INS_AMT);
   iTaxAmt2 = atoin(pInRec->Snd_Ins_Amt, SIZ_TS_INS_AMT);
   if (iTaxAmt2 > 0)
   {
      if (bRet)
         iTaxAmt2 = -iTaxAmt2;
      sprintf(pOutRec->TaxAmt2, "%.2f", (double)iTaxAmt2/100);
   }

   // TotalTax
   iTotalTax = iTaxAmt1+iTaxAmt2;
   if (iTotalTax)
      sprintf(pOutRec->TotalTaxAmt, "%.2f", (double)iTotalTax/100);
   else
      iRet = 1;   // Ignore record with no tax amount

   // Due date
   memcpy(pOutRec->DueDate1, pInRec->Fst_Dlq_Dte, 8);
   memcpy(pOutRec->DueDate2, pInRec->Snd_Dlq_Dte, 8);

   // Paid date
   if (pInRec->Fst_Paid_Dte[0] == '2')
   {
      sprintf(pOutRec->PaidAmt1, "%.2f", (double)iTaxAmt1/100);
      memcpy(pOutRec->PaidDate1, pInRec->Fst_Paid_Dte, 8);
      pOutRec->Inst1Status[0] = TAX_BSTAT_PAID;
      iTaxAmt1 = 0;
   } else
      pOutRec->Inst1Status[0] = TAX_BSTAT_UNPAID;

   if (pInRec->Snd_Paid_Dte[0] == '2')
   {
      sprintf(pOutRec->PaidAmt2, "%.2f", (double)iTaxAmt2/100);
      memcpy(pOutRec->PaidDate2, pInRec->Snd_Paid_Dte, 8);
      pOutRec->Inst2Status[0] = TAX_BSTAT_PAID;
      iTaxAmt2 = 0;
   } else if (iTaxAmt2 < 0)
      pOutRec->Inst2Status[0] = TAX_BSTAT_REFUND;
   else
      pOutRec->Inst2Status[0] = TAX_BSTAT_UNPAID;

   if (pInRec->Sys_Ind[1] == 'S')
   {
      pOutRec->isSecd[0] = '1';
      pOutRec->BillType[0] = BILLTYPE_SECURED_SUPPL;
   } else
   {
      pOutRec->isSecd[0] = '0';
      pOutRec->BillType[0] = BILLTYPE_UNSECURED_SUPPL;
   }
   pOutRec->isSupp[0] = '1';

   // TotalDue
   iTmp = iTaxAmt1+iTaxAmt2;
   sprintf(pOutRec->TotalDue, "%.2f", (double)iTmp/100);
   if (iTmp == 100)
      iRet= 1;

   pOutRec->CRLF[0] = 10;
   pOutRec->CRLF[1] = 0;

   return iRet;
}

/**************************** Org_Load_TaxSupp *******************************
 *
 * Load supplemental file TC11CT01.txt
 * Skip processing if input file not available.
 *
 * Return 0 if success.
 *
 *****************************************************************************/

int Org_Load_TaxSupp(bool bImport)
{
   char     *pTmp, acBuf[MAX_RECSIZE], acRec[MAX_RECSIZE];
   char     acOutFile[_MAX_PATH], acInFile[_MAX_PATH];
   int      iRet;
   long     lOut=0, lCnt=0;
   FILE     *fdOut, *fdIn;

   LogMsg0("Load supplemental tax");

   // Open input file
   iRet = GetIniString(myCounty.acCntyCode, "SuppTax", "", acInFile, _MAX_PATH, acIniFile);
   if (!iRet)
   {
      LogMsg("*** Skip processing Supplemental: tax file not available");
      return 0;
   }

   LogMsg("Open supplemental tax file %s", acInFile);
   fdIn = fopen(acInFile, "r");
   if (fdIn == NULL)
   {
      LogMsg("***** Error opening supplemental tax file: %s\n", acInFile);
      return -2;
   }  
   lLastFileDate = getFileDate(acInFile);

   // Open Output file
   NameTaxCsvFile(acOutFile, myCounty.acCntyCode, "Supp");
   LogMsg("Open output file %s", acOutFile);
   fdOut = fopen(acOutFile, "w");
   if (fdOut == NULL)
   {
      LogMsg("***** Error creating output file: %s\n", acOutFile);
      return -4;
   }

   // Merge loop 
   while (!feof(fdIn))
   {
      pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdIn);
      if (!pTmp || *pTmp < ' ')
         break;

      // Create new R01 record
      iRet = Org_ParseTaxSupp(acBuf, acRec);
      if (!iRet)
      {
         // Create delimited record
         Tax_CreateTaxBaseCsv(acRec, (TAXBASE *)&acBuf);

         // Output record			
         lOut++;
         fputs(acRec, fdOut);
      } else
      {
         LogMsg("---> Drop record %d [%.40s]", lCnt, acRec); 
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdIn)
      fclose(fdIn);
   if (fdOut)
      fclose(fdOut);

   LogMsg("Total records processed:    %u", lCnt);
   LogMsg("Total supp records output:  %u", lOut);

   printf("\nTotal output records: %u", lCnt);

   // Import into Org_Tax_Supp table then merge it into Org_Tax_Base
   if (bImport)
   {
      iRet = doTaxImport(myCounty.acCntyCode, TAX_SUPPLEMENT);
      if (!iRet)
         iRet = doTaxMerge(myCounty.acCntyCode, false, false, true);    // Merge Supplement only
   } else
      iRet = 0;

   return iRet;
}

/******************************** Org_ExtrLegal *****************************
 *
 * Extract legal for specific list of APN from Assessment Roll file
 * Input:  parcel file
 * Output: legal file
 * Return number of records extracted. <0 if error.
 *
 ****************************************************************************/

int Org_ExtrLegal(char *pParcel, char *pOutfile)
{
   char     *pTmp, acBuf[6000], acTmpFile[_MAX_PATH], acOutFile[_MAX_PATH], 
            acApn[32], acTmp[256], acInRec[6000], acLegal[2048];
   long     iTmp, iRet, lCnt=0, lOutRecs=0, lNotFound=0;
   FILE     *fdParcel, *fdOut;
   BOOL     bExtrAll;

   LogMsg0("Extract Legal");

   if (pParcel)
   {
      LogMsg("Extract Legal for %s", pParcel);
      bExtrAll = false;

      // Sort input file
      sprintf(acTmpFile, "%s\\%s\\Parcels.srt", acTmpPath, myCounty.acCntyCode); 
      sprintf(acTmp, "S(1,%d,C,A) OMIT(1,1,C,GT,\"9\") DUPO", iApnLen);
      iRet = sortFile(pParcel, acTmpFile, acTmp);
      if (iRet <= 0)
         return 0;

      // Open parcels file
      LogMsg("Open parcel file %s", acTmpFile);
      fdParcel = fopen(acTmpFile, "r");
      if (fdParcel == NULL)
      {
         LogMsg("***** Error opening parcel file: %s\n", acTmpFile);
         return -1;
      }

      // Get first parcel rec
      pTmp = fgets((char *)&acBuf[0], 512, fdParcel);
      strcpy(acOutFile, pOutfile);
   } else
   {
      LogMsg0("Extract Legal for %s", myCounty.acCntyCode);
      bExtrAll = true;
      fdParcel = NULL;

      GetIniString("Data", "LegalTmpl", "", acTmp, _MAX_PATH, acIniFile);
      sprintf(acOutFile, acTmp, myCounty.acCntyName);
   }

   // Open roll file
   LogMsg("Open Roll file %s", acRollFile);
   fdRoll = fopen(acRollFile, "rb");
   if (fdRoll == NULL)
   {
      printf("***** Error opening roll file: %s\n", acRollFile);
      return -2;
   }

   // Skip header
   pTmp = fgets(acInRec, 6000, fdRoll);

   // Create Output file
   LogMsg("Open output file %s", acOutFile);
   fdOut = fopen(acOutFile, "w");
   if (fdOut == NULL)
   {
      LogMsg("***** Error creating legal file: %s\n", acOutFile);
      return -3;
   }

   // Output header - FIPS|APN_D|LEGAL
   fputs("FIPS|APN_D|LEGAL\n", fdOut);
   iMaxLegal = 0;

   // Merge loop
   while (!feof(fdRoll))
   {
      pTmp = fgets(acInRec, 6000, fdRoll);
      if (!pTmp)
         break;

      // Replace null char and double quote with single quote
      for (iTmp = 0; iTmp < 6000; iTmp++, pTmp++)
      {
         if (*pTmp == 0)
            *pTmp = ' ';
         else if (*pTmp == 34)
            *pTmp = 39;
         else if (*pTmp == 10)
            break;
      }

      // Parse input record
      iRet = ParseStringIQ(acInRec, '|', MAX_FLD_TOKEN, apTokens);
      if (iRet < ORG_ROLL_MISCMSG2)
      {
         LogMsg("***** Error: bad input record for APN=%.10s", acInRec);
         return -1;
      }

#ifdef _DEBUG      
      //if (!memcmp(acInRec, "2079011016", 10))
      //   iRet = 0;
#endif

      if (bExtrAll)
      {
         // Create output record
         sprintf(acLegal, "%s %s %s %s %s", apTokens[ORG_ROLL_NAD_L1], apTokens[ORG_ROLL_NAD_L2],
            apTokens[ORG_ROLL_NAD_L3], apTokens[ORG_ROLL_NAD_L4], apTokens[ORG_ROLL_NAD_L5]);
         iRet = blankRem(acLegal);
         if (iRet > iMaxLegal)
            iMaxLegal = iRet;

         // Fix known bad chars
         replChar(acLegal, 0xC2, '-');
         replChar(acLegal, 0xAC, '-');
         replStr(acLegal, "--A", "PAR");

         iRet = formatApn(apTokens[ORG_ROLL_APN], acApn, &myCounty);
         sprintf(acBuf, "06059|%s|%s\n", acApn, acLegal);   

         fputs(acBuf, fdOut);  
         lOutRecs++;
      } else
      {
ReCheck:
         // Format APN
         iRet = memcmp(acBuf, apTokens[ORG_ROLL_APN], iApnLen);
         // If matched, get legal
         if (!iRet)
         {
            sprintf(acLegal, "%s %s %s %s %s", apTokens[ORG_ROLL_NAD_L1], apTokens[ORG_ROLL_NAD_L2],
               apTokens[ORG_ROLL_NAD_L3], apTokens[ORG_ROLL_NAD_L4], apTokens[ORG_ROLL_NAD_L5]);
            iRet = blankRem(acLegal);
            if (iRet > iMaxLegal)
               iMaxLegal = iRet;

            // Fix known bad chars
            replChar(acLegal, 0xC2, '-');
            replChar(acLegal, 0xAC, '-');
            replStr(acLegal, "--A", "PAR");

            iRet = formatApn(apTokens[ORG_ROLL_APN], acApn, &myCounty);
            sprintf(acBuf, "06059|%s|%s\n", acApn, acLegal);   
            fputs(acBuf, fdOut);  
            lOutRecs++;

            // Get new parcel
            pTmp = fgets((char *)&acBuf[0], 512, fdParcel);
            if (!pTmp)
               break;
         } else if (iRet < 0)
         {
            // No legal for this record
            acBuf[iApnLen] = 0;
            LogMsg("*** No legal for parcel: %s", acBuf);
            lNotFound++;

            // Get new parcel
            pTmp = fgets((char *)&acBuf[0], 512, fdParcel);
            if (!pTmp)
               break;

            lCnt++;
            goto ReCheck;
         }
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdRoll)
      fclose(fdRoll);
   if (fdParcel)
      fclose(fdParcel);
   if (fdOut)
      fclose(fdOut);

   LogMsg("Total input records:  %u", lCnt);
   LogMsg("Total output records: %u", lOutRecs);
   LogMsg("Total no legal:       %u", lNotFound);
   LogMsg("Max legal length:     %u", iMaxLegal);

   return lOutRecs;
}

/*********************************** loadOrg **********************************
 *
 * Use -G      to load GrGr data
 *     -Mg     to merge GrGr data to R01 file
 *     -Mr     to merge LotSqft & LotArea from GIS basemap
 *     -L      to load LDR file
 *     -U -G   to run monthly update
 *     -Uu     special load to update standard usecode on R01
 *     -X1     to extract situs address from O01 file (number one)
 *     -Xl     to extract lien data (letter l)
 *     -Xa     to convert CHAR file to STDCHAR format
 *
 * LDR command: -CORG -L -Mg -Xl [-Xa|-Ma] [-Mr]
 * Update Cmd:  -CORG -U [-G|-Mg] [-Ma] [-Mr]
 *
 ******************************************************************************/

int loadOrg(int iSkip)
{
   int   iRet=0;
   char  acTmp[_MAX_PATH], acExeUpdFile[_MAX_PATH];
   char  acRawFile[_MAX_PATH], acTmpFile[_MAX_PATH], acXadrFile[_MAX_PATH];
   bool  bUpdateCondo, bUpdateOwner;

   iApnLen = myCounty.iApnLen;
   bUpdateOwner = true;                         // Flag to tell MergeGrGr to update owner.

   // Apply mail addr to situs
   //iRet = Mail2Situs(myCounty.acCntyCode, iSkip);

   // Fix Org_GrGr.sls file 6/21/2017
   //iRet = Org_FixGrGr(myCounty.acCntyCode, PQ_FIX_NSALEFLG);

   // Loading Tax
   if (iLoadTax)                                   // -T
   {
      iRet = GetIniString(myCounty.acCntyCode, "TaxDist", "", acTmpFile, _MAX_PATH, acIniFile);
      if (iRet > 0)
         iRet = LoadTaxCodeTable(acTmpFile);

      // Update current tax if there is new scrape
      iRet = GetIniString(myCounty.acCntyCode, "Main_TC", "", acTmp, _MAX_PATH, acIniFile);
      if (iRet > 5)
      {
         char  sFullTaxFile[_MAX_PATH], sPartialTaxFile[_MAX_PATH];

         TC_SetDateFmt(MM_DD_YYYY_1);

         // Get input file names
         sprintf(sFullTaxFile, acTmp, 'F');
         sprintf(sPartialTaxFile, acTmp, 'P');
         iRet = chkFileDate(sFullTaxFile, sPartialTaxFile);
         if (iRet == 1)
         {
            iRet = isNewTaxFile(sFullTaxFile, myCounty.acCntyCode);
            if (iRet <= 0)
            {
               lLastTaxFileDate = 0;               // Signal not to update Tax info in County table
               return iRet;
            } else
               lLastTaxFileDate = getFileDate(sFullTaxFile);

            iRet = TC_LoadTaxBase(myCounty.acCntyCode, sFullTaxFile, bTaxImport);
            if (!iRet)
            {
               iRet = Org_Load_TaxDelq(bTaxImport);
               if (!iRet)
                  iRet = updateDelqFlag(myCounty.acCntyCode);
            }

            iRet = GetIniString(myCounty.acCntyCode, "Dist_TC", "", acTmp, _MAX_PATH, acIniFile);
            sprintf(sFullTaxFile, acTmp, 'F');
            SetTaxRateIncl(myCounty.acCntyCode);
            iRet = TC_LoadTaxAgencyAndDetail(myCounty.acCntyCode, sFullTaxFile, NULL, NULL, bTaxImport);
         } else if (iRet == 2)
         {
            iRet = isNewTaxFile(sPartialTaxFile, myCounty.acCntyCode);
            if (iRet <= 0)
            {
               lLastTaxFileDate = 0;      // Signal not to update Tax info in County table
               return iRet;
            } else
               lLastTaxFileDate = getFileDate(sPartialTaxFile);

            // Update tax base
            iRet = TC_UpdateTaxBase(myCounty.acCntyCode, sPartialTaxFile);

            // Load tax delq
            iRet = Org_Load_TaxDelq(bTaxImport);
            if (!iRet)
               iRet = updateDelqFlag(myCounty.acCntyCode);
         }
      } else
      {
         // Init DueDate format
         TC_SetDateFmt(0, true);
         if (iLoadTax & TAX_LOADING)
         {
            // Load Tax Base and Delq
            iRet = Org_Load_TaxBase(bTaxImport);
            if (!iRet && lLastTaxFileDate > 0)
            {
               // Load Items and Agency
               GetIniString(myCounty.acCntyCode, "TaxDetail", "", acTmpFile, _MAX_PATH, acIniFile);
               if (strstr(acTmpFile, "TC04TY30"))
                  iRet = Org_Load_TC04TY30(acTmpFile, bTaxImport);     // Load tax detail using TC04TY30.txt
               else
                  iRet = Org_Load_TC01CT02(acTmpFile, bTaxImport);     // Load tax detail using TC01CT02.txt

               if (iRet != 0)
                  return iRet;
            }
         }

         // Load tax delq payment (rec type 9 from TC02CA01.txt)
         // This function load redemption amount and redemption date
         //iRet = Org_Load_TaxPaid(bTaxImport);

         // Load delq rec (rec type 1 & 7 from TC02CA01.txt)
         iRet = Org_Load_TaxDelq(bTaxImport);

         // Upate Delq flag in Base table
         iRet = updateDelqFlag(myCounty.acCntyCode);
      }

      // Load supplemental
      iRet = Org_Load_TaxSupp(true);

      // Update total tax rate
      iRet = doUpdateTotalRate(myCounty.acCntyCode, "B");
   }

   // Exit if load/update tax only
   if (!iLoadFlag)
      return iRet;

   // Extract legal.  Use as needed
   if (iLoadFlag & EXTR_DESC)                      // -Xd
   {
      // Get list of APN to extract legal
      iRet = GetIniString(myCounty.acCntyCode, "ParcelFile", "", acRawFile, _MAX_PATH, acIniFile);
      if (iRet > 1)
      {
         // Prepare output file
         GetIniString("Data", "LegalTmpl", "", acTmp, _MAX_PATH, acIniFile);
         sprintf(acTmpFile, acTmp, myCounty.acCntyName);
         iRet = Org_ExtrLegal(acRawFile, acTmpFile);
      } else
         iRet = Org_ExtrLegal(NULL, NULL);
   }

   // Verify GrGr
   GetIniString(myCounty.acCntyCode, "VerifyGrGr", "", acTmp, _MAX_PATH, acIniFile);
   if (acTmp[0] == 'Y')
	{
      sprintf(acSaleFile, acGrGrTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "Sls");
      sprintf(acTmpFile,  acGrGrTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "BadApn");
      iRet = GrGr_ChkApnLen(acSaleFile, acTmpFile, iApnLen);
      return -99;
	}

   // Verify condo reference list
   GetIniString(myCounty.acCntyCode, "VerifyCondo", "", acTmp, _MAX_PATH, acIniFile);
   if (acTmp[0] == 'Y')
      if (iRet = Org_ChkCondoList("Condo"))
         return -1;

   // Extract Attom sale 
   GetIniString(myCounty.acCntyCode, "NdcSale", "", acSaleFile, _MAX_PATH, acIniFile);
   GetIniString("Data", "ASH_File", acESalTmpl, acESalTmpl, _MAX_PATH, acIniFile);
   if (iLoadFlag & EXTR_NRSAL)                      // -Xn
   {
      if (!_access(acSaleFile, 0))
      {
         iRet = NR_CreateSCSale(myCounty.acCntyCode, acSaleFile, iApnLen);
         if (iRet)
            return iRet;
      } else
      {
         LogMsg("*** Sale file not available.  Please verify: %s", acSaleFile);
         return -1;
      }
   } else
   {
      sprintf(acCSalFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "Sls");
   }

	// Load char file
   if (iLoadFlag & EXTR_ATTR)                      // -Xa
   {
      iRet = Org_ConvStdChar(acCharFile);
   }

   // Check update
   GetIniString(myCounty.acCntyCode, "ExeUpd", "", acExeUpdFile, _MAX_PATH, acIniFile);
   GetIniString(myCounty.acCntyCode, "UpdateCondo", "", acTmp, _MAX_PATH, acIniFile);
   if (acTmp[0] == 'Y')
      bUpdateCondo = true;
   else
      bUpdateCondo = false;

   if (iLoadFlag & (LOAD_LIEN|EXTR_LIEN))  
   {
      // Sort LDR file
      sprintf(acRollFile, "%s\\%s\\%s_Lien.txt", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
      iRet = sortFile(acLienFile, acRollFile, "S(#1,C,A) F(TXT) DEL(124)");
   }

   // Extract lien file
   if (iLoadFlag & EXTR_LIEN)                      // -Xl
      iRet = Org_ExtrLien(myCounty.acCntyCode, acRollFile, acExeUpdFile);

   if (iLoadFlag & LOAD_GRGR)                      // -G
   {
      LogMsg0("Load %s GrGr file", myCounty.acCntyCode);
      iRet = Org_LoadGrGr(myCounty.acCntyCode);
      // 06/25/2022 - no longer needed - only used when calling ApplyCumSale()
      //if (!iRet)
      //{
      //   // Convert ORG_GRGR.SLS to ORG_SALE.SLS
      //   sprintf(acTmpFile, acGrGrTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "SLS");
      //   sprintf(acSaleFile, acSaleTmpl, myCounty.acCntyCode, "SLS");
      //   iRet = convertSaleData(myCounty.acCntyCode, acTmpFile, CONV_GRGR_ORG, acSaleFile);
      //}
   } else
   {  // format sale file name
      sprintf(acSaleFile, acGrGrTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "Sls");
   }

   if (iLoadFlag & LOAD_LIEN)                      // -L
   {
      // Create Lien file
      LogMsg0("Load %s Lien file", myCounty.acCntyCode);
      iRet = Org_Load_LDR(acExeUpdFile, iSkip);
      if (iRet)
         return iRet;
      bUpdateOwner = false;
      sprintf(acSaleFile, acGrGrTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "sls");
   } else if (iLoadFlag & LOAD_UPDT)               // -U
   {
      LogMsg0("Load %s roll update file", myCounty.acCntyCode);
      iRet = Org_Load_Roll(iSkip);
   }

   // Merge Char
   if (iLoadFlag & MERG_ATTR)                      // -Ma
   {
      LogMsg0("Merge Char file");
      iRet = Org_MergeChar(iSkip);
   }

   // Update usecode - manually run only
   if (!iRet && (iLoadFlag & UPDT_SUSE))
      iRet = updateR01StdUse(myCounty.acCntyCode, iSkip);

   // Fix TRA
   if (!iRet && (lOptMisc & M_OPT_FIXTRA))
      iRet = PQ_FixR01(myCounty.acCntyCode, PQ_FIX_TRA, 0, true);

   if (!iRet && bMergeOthers)
   {
      sprintf(acTmpFile, acLienTmpl, myCounty.acCntyCode, myCounty.acCntyCode);
      if (!_access(acTmpFile, 0))
         iRet = PQ_MergeOthers(myCounty.acCntyCode, acTmpFile, GRP_ORG, iSkip);
      else
         LogMsg("***** Lien extract is missing: %s.  Please locate the file and retry", acTmpFile);
   }

   if (bUpdateCondo && (!iRet && (iLoadFlag & LOAD_LIEN)))
   {
      // Update condo/timeshare list
      GetIniString(myCounty.acCntyCode, "Condo", "", acTmpFile, _MAX_PATH, acIniFile);
      if (acTmpFile[0] > ' ')
      {
         if (!_access(acTmpFile, 0))
         {
            GetIniString(myCounty.acCntyCode, "TimeShare", "", acRawFile, _MAX_PATH, acIniFile);
            iRet = Org_UpdateCondo(acTmpFile, acRawFile, iSkip);
         } else
            LogMsgD("*** Condo list is missing: %s", acTmpFile);
      }
   }

   if (!iRet && (iLoadFlag & MERG_SADR))           // -M1
   {
      GetIniString(myCounty.acCntyCode, "SitusFile", "", acXadrFile, _MAX_PATH, acIniFile);
      if (!_access(acXadrFile, 0))
      {
         LogMsgD("\nMerge %s situs address1 using %s", myCounty.acCntyCode, acXadrFile);
         iRet = Org_MergeSitus(iSkip, acXadrFile);
      }
   }

   GetIniString(myCounty.acCntyCode, "NdcSalePlace", "1", acTmp, _MAX_PATH, acIniFile);
   if (acTmp[0] == '1')
   {
      // Apply NDC sale file to R01
      if (!iRet && (iLoadFlag & UPDT_XSAL))           // -Mn
      {
         // Apply Org_Ash.sls to R01 file
         GetIniString("Data", "ASH_File", acESalTmpl, acESalTmpl, _MAX_PATH, acIniFile);
         sprintf(acTmpFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "sls");
         iRet = ApplyCumSaleNR(iSkip, acTmpFile, SALE_USE_SCUPDXFR,CLEAR_OLD_SALE|CLEAR_OLD_XFER);
         if (!iRet)
            iLoadFlag |= LOAD_UPDT;
      }

      if (!iRet && (iLoadFlag & MERG_GRGR))           // -Mg
      {
         // Clear old sales before applying GRGR when create file for bulk client
         if (!(iLoadFlag & UPDT_XSAL))
            bClearSales = true;
         LogMsg0("Merge %s GrGr file", myCounty.acCntyCode);
         iRet = Org_MergeGrGr(iSkip, bUpdateOwner);
         if (!iRet)
         {
            if (!(iLoadFlag & LOAD_LIEN))
               iLoadFlag |= LOAD_UPDT;
         } else
            LogMsg("***** ERROR in MergeGrGr %d", iRet);
      }
   } else
   {
      if (!iRet && (iLoadFlag & MERG_GRGR))           // -Mg
      {
         // Clear old sales before applying GRGR when create file for bulk client
         // We can use -Ps to set bClearSales=true
         if (!(iLoadFlag & UPDT_XSAL))
            bClearSales = true;
         LogMsg0("Merge %s GrGr file", myCounty.acCntyCode);
         iRet = Org_MergeGrGr(iSkip, bUpdateOwner);
         if (!iRet)
         {
            if (!(iLoadFlag & LOAD_LIEN))
               iLoadFlag |= LOAD_UPDT;
         } else
            LogMsg("***** ERROR in MergeGrGr %d", iRet);
      }

      if (!iRet && (iLoadFlag & UPDT_XSAL))           // -Mn
      {
         // Apply Org_Ash.sls to R01 file
         GetIniString("Data", "ASH_File", acESalTmpl, acESalTmpl, _MAX_PATH, acIniFile);
         sprintf(acTmpFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "sls");
         if (iLoadFlag & LOAD_LIEN)
            iRet = ApplyCumSaleNR(iSkip, acTmpFile, SALE_USE_SCUPDXFR);
         else
         {
            iRet = ApplyCumSaleNR(iSkip, acTmpFile, SALE_USE_SCUPDXFR);
            //iRet = ApplyCumSaleNR(iSkip, acTmpFile, SALE_USE_SCUPDXFR,UPDATE_OWNER);
            if (!iRet)
               iLoadFlag |= LOAD_UPDT;
         }
      }
   }

   return iRet;
}