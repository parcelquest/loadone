/****************************************************************************
 *
 *
 ****************************************************************************/

#include "stdafx.h"
#include "Logs.h"
#include "Utils.h"
extern "C"
{
   #include "dbc3plus.h"
}

char     *m_MainTabID, *m_RegionTabID;
dBFIELD  m_Fields[256];
FLDNUM   m_nFields;
RECLEN   m_nReclen;
long     m_lNumRecs;

int openDbf(char *pDbfName)
{
   int iRet=1;

   m_MainTabID = NULL;
   if (dBopen(pDbfName, d_SINGLE, &m_MainTabID) != SUCCESS)
   {
      // Most of the time error occured due to attribute of DBF file is READONLY
      // Just remove the READONLY attribute from the file, it will work OK.
      m_MainTabID=NULL;
      AfxMessageBox("Error opening DBF file");
      return -1;
   }

	char    month[8];
	char    day[8];
	char    year[8];
   iRet = dBgetf(m_MainTabID, &m_nReclen, (char *)&month[0], (char *)&day[0], (char *)&year[0], &m_nFields, &m_Fields[0]);
   if (!iRet)
   {
      iRet = dBsize(m_MainTabID, &m_lNumRecs);
   }

   return iRet;
}

void closeDbf()
{
   if (m_MainTabID)
   {
      dBclose(m_MainTabID);
      m_MainTabID=NULL;
   }
}

int DBF2CSV(char *pDbfFile, char *pCsvFile)
{
   int   r, i, iIdx, iRet=0;
   char  acRec[512], acRow[2048], acTmp[256], status;
   FILE  *fdCsv;

   // Open DBF
   iRet = openDbf(pDbfFile);

   // Open CSV
   fdCsv = fopen(pCsvFile, "w");

   for (r = 1; r <= m_lNumRecs; r++)
   {
      if (dBgetr(m_MainTabID, r, acRec, &status) == SUCCESS)
      {
         acRow[0] = 0;
         iIdx = 0;
         for (i = 0; i < m_nFields; i++)
         {
            strncpy(acTmp, (char *)&acRec[iIdx], m_Fields[i].width);
            acTmp[m_Fields[i].width] = 0;
            strcat(acRow, acTmp);
            strcat(acRow, "|");
            iIdx += m_Fields[i].width;
         }
         fputs(acRow, fdCsv);
      } else
      {
         iRet = -2;
         LogMsg("Error reading data");
      }
   }

   closeDbf();
   fclose(fdCsv);

   return iRet;
}

