#ifndef _MERGENEV_H_
#define _MERGENEV_H_

#define  SSIZ_NEV_APN                     10
#define  SSIZ_NEV_DOCNUM                  9
#define  SSIZ_NEV_DOCDATE                 10
#define  SSIZ_NEV_REMARKS                 25
#define  SSIZ_NEV_STAMP_AMOUNT            9
#define  SSIZ_NEV_PCT_XFER                3
#define  SSIZ_NEV_SALE_FLAG               1

#define  SOFF_NEV_APN                     1-1
#define  SOFF_NEV_DOCNUM                  11-1
#define  SOFF_NEV_DOCDATE                 20-1
#define  SOFF_NEV_REMARKS                 30-1
#define  SOFF_NEV_STAMP_AMOUNT            55-1     // V99
#define  SOFF_NEV_PCT_XFER                64-1
#define  SOFF_NEV_FULL_PARTIAL_FLAG       67-1
#define  SOFF_NEV_GROUP_SALE              68-1
#define  SOFF_NEV_DEED_TYPE               69-1
#define  SOFF_NEV_INDI_SALE               70-1


typedef struct _tNevSale
{
   char Apn[SSIZ_NEV_APN];
   char DocNum[SSIZ_NEV_DOCNUM];
   char DocDate[SSIZ_NEV_DOCDATE];
   char Remarks[SSIZ_NEV_REMARKS];
   char DocTax[SSIZ_NEV_STAMP_AMOUNT];
   char Pct_Xfer[SSIZ_NEV_PCT_XFER];
   char Full_Partial_Flag[SSIZ_NEV_SALE_FLAG];  // This flag is questionable
   char Group_Sale[SSIZ_NEV_SALE_FLAG];
   char Deed_Type [SSIZ_NEV_SALE_FLAG];
   char Indi_Sale[SSIZ_NEV_SALE_FLAG];
} NEV_SALE;

#define  CSIZ_NEV_APN                     10
#define  CSIZ_NEV_SQUARE_FEET_IMPROV      6
#define  CSIZ_NEV_UNITS                   4
#define  CSIZ_NEV_TOPOGRAPHY_CODE         1
#define  CSIZ_NEV_VIEW_CODE               1
#define  CSIZ_NEV_FRONTAGE_CODE           1
#define  CSIZ_NEV_STORIES                 4
#define  CSIZ_NEV_CONTRUCTION_YEAR        4
#define  CSIZ_NEV_GARAGE_CODE             1
#define  CSIZ_NEV_MISC_IMPROV_CODE        1
#define  CSIZ_NEV_FIREPL_CODE             1
#define  CSIZ_NEV_BEDROOMS                1
#define  CSIZ_NEV_BATHS                   3
#define  CSIZ_NEV_AIR_HEATING_CODE        1
#define  CSIZ_NEV_POOL_SPA_CODE           1
#define  CSIZ_NEV_GUEST_HOUSE_CODE        1
#define  CSIZ_NEV_PORCH_CODE              1
#define  CSIZ_NEV_FLATWORK_CODE           1
#define  CSIZ_NEV_FILLER                  7

#define  COFF_NEV_APN                     1-1
#define  COFF_NEV_SQUARE_FEET_IMPROV      11-1
#define  COFF_NEV_UNITS                   17-1
#define  COFF_NEV_TOPOGRAPHY CODE         21-1
#define  COFF_NEV_VIEW_CODE               22-1
#define  COFF_NEV_FRONTAGE_CODE           23-1
#define  COFF_NEV_STORIES                 24-1
#define  COFF_NEV_CONTRUCTION_YEAR        28-1
#define  COFF_NEV_GARAGE_CODE             32-1
#define  COFF_NEV_MISC_IMPROV_CODE        33-1
#define  COFF_NEV_FIREPL_CODE             34-1     // # fire places
#define  COFF_NEV_BEDROOMS                35-1     // # bedrooms
#define  COFF_NEV_BATHS                   36-1     // # baths
#define  COFF_NEV_AIR_HEATING_CODE        39-1
#define  COFF_NEV_POOL_SPA_CODE           40-1
#define  COFF_NEV_GUEST_HOUSE_CODE        41-1
#define  COFF_NEV_PORCH_CODE              42-1
#define  COFF_NEV_FLATWORK_CODE           43-1
#define  COFF_NEV_FILLER                  44-1

 typedef struct _tNevChar
 {
   char Apn[CSIZ_NEV_APN];
   char Square_Feet_Improv[CSIZ_NEV_SQUARE_FEET_IMPROV];
   char Units[CSIZ_NEV_UNITS];
   char Topography_Code[CSIZ_NEV_TOPOGRAPHY_CODE];
   char View_Code[CSIZ_NEV_VIEW_CODE];
   char Frontage_Code[CSIZ_NEV_FRONTAGE_CODE];
   char Stories[CSIZ_NEV_STORIES];
   char Contruction_Year[CSIZ_NEV_CONTRUCTION_YEAR];
   char Garage_Code[CSIZ_NEV_GARAGE_CODE];
   char Misc_Improv_Code[CSIZ_NEV_MISC_IMPROV_CODE];
   char Firepl_Code[CSIZ_NEV_FIREPL_CODE];
   char Bedrooms[CSIZ_NEV_BEDROOMS];
   char Baths[CSIZ_NEV_BATHS];
   char Air_Heating_Code[CSIZ_NEV_AIR_HEATING_CODE];
   char Pool_Spa_Code[CSIZ_NEV_POOL_SPA_CODE];
   char Guest_House_Code[CSIZ_NEV_GUEST_HOUSE_CODE];
   char Porch_Code[CSIZ_NEV_PORCH_CODE];
   char Flatwork_Code[CSIZ_NEV_FLATWORK_CODE];
   char Filler[CSIZ_NEV_FILLER];
} NEV_CHAR;

#define RSIZ_NEV_APN                               10
#define RSIZ_NEV_STATUS                            1
#define RSIZ_NEV_TRA                               5
#define RSIZ_NEV_NAME                              36
#define RSIZ_NEV_CAREOF_CODE                       4
#define RSIZ_NEV_CAREOF                            32
#define RSIZ_NEV_MAIL_ADDR                         36
#define RSIZ_NEV_MAIL_CITY_STATE                   25
#define RSIZ_NEV_MAIL_ZIP                          5
#define RSIZ_NEV_CONSTANT_BLANK                    1
#define RSIZ_NEV_MAIL_ZIP4                         4
#define RSIZ_NEV_ADDR_UPDATE_DATE                  8
#define RSIZ_NEV_ANY_UPDATE_DATE                   8
#define RSIZ_NEV_VALUE_UPDATE_DATE                 8
#define RSIZ_NEV_PERC_OWN                          7
#define RSIZ_NEV_JOINT_TEN_NO                      2
#define RSIZ_NEV_ROLL_YEAR                         5
#define RSIZ_NEV_LAND                              8
#define RSIZ_NEV_IMPROV                            8
#define RSIZ_NEV_BUSINESS_PERS_PROP                8
#define RSIZ_NEV_TRADE_FIXT                        8
#define RSIZ_NEV_EXEMPTION_TYPE                    1
#define RSIZ_NEV_EXEMPTION_LATE                    1
#define RSIZ_NEV_EXEMPTION_VALUE                   8
#define RSIZ_NEV_GROSS_VALUE                       8
#define RSIZ_NEV_NET_VALUE                         8
#define RSIZ_NEV_SITUS_STRNUM                      6
#define RSIZ_NEV_SITUS_STRNAME                     24
#define RSIZ_NEV_SITUS_CITY_CODE                   2
#define RSIZ_NEV_USE_CODE                          5
#define RSIZ_NEV_ACRES                             6
#define RSIZ_NEV_PROPERTY_LEAD_LINE                30
#define RSIZ_NEV_CLCA_CODE                         1
#define RSIZ_NEV_PARENT_PARCEL                     40
#define RSIZ_NEV_DATE_PARCEL                       8
#define RSIZ_NEV_CHILD_PARCEL                      40
#define RSIZ_NEV_DATE_PARCEL_VOIDED                8
#define RSIZ_NEV_HO_INITIATION_DATE                8
#define RSIZ_NEV_PERC_COMPLETED                    3
#define RSIZ_NEV_REASON_INCOMPLETE                 1
#define RSIZ_NEV_FILLER1                           13
#define RSIZ_NEV_DOCDATE1                          8
#define RSIZ_NEV_DOCNUM1                           9
#define RSIZ_NEV_STAMP_AMT1                        7
#define RSIZ_NEV_STAMP_TYPE1                       1
#define RSIZ_NEV_PERC_TRANSFERRED1                 3
#define RSIZ_NEV_FILLER2                           2
#define RSIZ_NEV_DOCDATE2                          8
#define RSIZ_NEV_DOCNUM2                           9
#define RSIZ_NEV_STAMP_AMT2                        7
#define RSIZ_NEV_STAMP_TYPE2                       1
#define RSIZ_NEV_PERCENT_TRANSFERRED2              3
#define RSIZ_NEV_FILLER3                           2
#define RSIZ_NEV_DOCDATE3                          8
#define RSIZ_NEV_DOCNUM3                           9
#define RSIZ_NEV_STAMP_AMT3                        7
#define RSIZ_NEV_STAMP_TYPE3                       1
#define RSIZ_NEV_PERCENT_TRANSFERRED3              3
#define RSIZ_NEV_FILLER4                           2
#define RSIZ_NEV_DOCDATE4                          8
#define RSIZ_NEV_DOCNUM4                           9
#define RSIZ_NEV_STAMP_AMT4                        7
#define RSIZ_NEV_STAMP_TYPE4                       1
#define RSIZ_NEV_PERCENT_TRANSFERRED4              3
#define RSIZ_NEV_FILLER5                           2
#define RSIZ_NEV_DOCDATE5                          8
#define RSIZ_NEV_DOCNUM5                           9
#define RSIZ_NEV_STAMP_AMT5                        7
#define RSIZ_NEV_STAMP_TYPE5                       1
#define RSIZ_NEV_PERCENT_TRANSFERRED5              3
#define RSIZ_NEV_FILLER6                           2


#define ROFF_NEV_APN                               1-1
#define ROFF_NEV_STATUS                            11-1
#define ROFF_NEV_TRA                               12-1
#define ROFF_NEV_NAME                              17-1
#define ROFF_NEV_CAREOF_CODE                       53-1
#define ROFF_NEV_CAREOF                            57-1
#define ROFF_NEV_MAIL_ADDR                         89-1
#define ROFF_NEV_MAIL_CITY_STATE                   125-1
#define ROFF_NEV_MAIL_ZIP                          150-1
#define ROFF_NEV_CONSTANT_BLANK                    155-1
#define ROFF_NEV_MAIL_ZIP4                         156-1
#define ROFF_NEV_ADDR_UPDATE_DATE                  160-1
#define ROFF_NEV_ANY_UPDATE_DATE                   168-1
#define ROFF_NEV_VALUE_UPDATE_DATE                 176-1
#define ROFF_NEV_PERC_OWN                          184-1
#define ROFF_NEV_JOINT_TEN_NO                      191-1
#define ROFF_NEV_ROLL_YEAR                         193-1
#define ROFF_NEV_LAND                              198-1
#define ROFF_NEV_IMPROV                            206-1
#define ROFF_NEV_BUSINESS_PERS_PROP                214-1
#define ROFF_NEV_TRADE_FIXT                        222-1
#define ROFF_NEV_EXEMPTION_TYPE                    230-1
#define ROFF_NEV_EXEMPTION_LATE                    231-1
#define ROFF_NEV_EXEMPTION_VALUE                   232-1
#define ROFF_NEV_GROSS_VALUE                       240-1
#define ROFF_NEV_NET_VALUE                         248-1
#define ROFF_NEV_SITUS_STRNUM                      256-1
#define ROFF_NEV_SITUS_STRNAME                     262-1
#define ROFF_NEV_SITUS_CITY_CODE                   286-1
#define ROFF_NEV_USE_CODE                          288-1
#define ROFF_NEV_ACRES                             293-1
#define ROFF_NEV_PROPERTY_LEAD_LINE                299-1
#define ROFF_NEV_CLCA_CODE                         329-1
#define ROFF_NEV_PARENT_PARCEL                     330-1
#define ROFF_NEV_DATE_PARCEL                       370-1
#define ROFF_NEV_CHILD_PARCEL                      378-1
#define ROFF_NEV_DATE_PARCEL_VOIDED                418-1
#define ROFF_NEV_HO_INITIATION_DATE                426-1
#define ROFF_NEV_PERC_COMPLETED                    434-1
#define ROFF_NEV_REASON_INCOMPLETE                 437-1
#define ROFF_NEV_FILLER1                           438-1
#define ROFF_NEV_DOCDATE1                          451-1
#define ROFF_NEV_DOCNUM1                           459-1
#define ROFF_NEV_STAMP_AMT1                        468-1
#define ROFF_NEV_STAMP_TYPE1                       475-1
#define ROFF_NEV_PERC_TRANSFERRED1                 476-1
#define ROFF_NEV_FILLER2                           479-1
#define ROFF_NEV_DOCDATE2                          481-1
#define ROFF_NEV_DOCNUM2                           489-1
#define ROFF_NEV_STAMP_AMT2                        498-1
#define ROFF_NEV_STAMP_TYPE2                       505-1
#define ROFF_NEV_PERCENT_TRANSFERRED2              506-1
#define ROFF_NEV_FILLER3                           509-1
#define ROFF_NEV_DOCDATE3                          511-1
#define ROFF_NEV_DOCNUM3                           519-1
#define ROFF_NEV_STAMP_AMT3                        528-1
#define ROFF_NEV_STAMP_TYPE3                       535-1
#define ROFF_NEV_PERCENT_TRANSFERRED3              536-1
#define ROFF_NEV_FILLER4                           539-1
#define ROFF_NEV_DOCDATE4                          541-1
#define ROFF_NEV_DOCNUM4                           549-1
#define ROFF_NEV_STAMP_AMT4                        558-1
#define ROFF_NEV_STAMP_TYPE4                       565-1
#define ROFF_NEV_PERCENT_TRANSFERRED4              566-1
#define ROFF_NEV_FILLER5                           569-1
#define ROFF_NEV_DOCDATE5                          571-1
#define ROFF_NEV_DOCNUM5                           579-1
#define ROFF_NEV_STAMP_AMT5                        588-1
#define ROFF_NEV_STAMP_TYPE5                       595-1
#define ROFF_NEV_PERCENT_TRANSFERRED5              596-1
#define ROFF_NEV_FILLER6                           599-1

typedef struct _tNevRoll
{
   char Apn[RSIZ_NEV_APN];
   char Status[RSIZ_NEV_STATUS];
   char Tra[RSIZ_NEV_TRA];
   char Name[RSIZ_NEV_NAME];
   char Careof_Code[RSIZ_NEV_CAREOF_CODE];
   char Careof[RSIZ_NEV_CAREOF];
   char Mail_Addr[RSIZ_NEV_MAIL_ADDR];
   char Mail_City_State[RSIZ_NEV_MAIL_CITY_STATE];
   char Mail_Zip[RSIZ_NEV_MAIL_ZIP];
   char Constant_Blank[RSIZ_NEV_CONSTANT_BLANK];
   char Mail_Zip4[RSIZ_NEV_MAIL_ZIP4];
   char Addr_Update_Date[RSIZ_NEV_ADDR_UPDATE_DATE];
   char Any_Update_Date[RSIZ_NEV_ANY_UPDATE_DATE];
   char Value_Update_Date[RSIZ_NEV_VALUE_UPDATE_DATE];
   char Perc_Own[RSIZ_NEV_PERC_OWN];
   char Joint_Ten_No[RSIZ_NEV_JOINT_TEN_NO];
   char Roll_Year[RSIZ_NEV_ROLL_YEAR];
   char Land[RSIZ_NEV_LAND];
   char Improv[RSIZ_NEV_IMPROV];
   char Business_Pers_Prop[RSIZ_NEV_BUSINESS_PERS_PROP];
   char Trade_Fixt[RSIZ_NEV_TRADE_FIXT];
   char Exemption_Type[RSIZ_NEV_EXEMPTION_TYPE];
   char Exemption_Late[RSIZ_NEV_EXEMPTION_LATE];
   char Exemption_Value[RSIZ_NEV_EXEMPTION_VALUE];
   char Gross_Value[RSIZ_NEV_GROSS_VALUE];
   char Net_Value[RSIZ_NEV_NET_VALUE];
   char Situs_StrNum[RSIZ_NEV_SITUS_STRNUM];
   char Situs_StrName[RSIZ_NEV_SITUS_STRNAME];
   char Situs_City_Code[RSIZ_NEV_SITUS_CITY_CODE];
   char Use_Code[RSIZ_NEV_USE_CODE];
   char Acres[RSIZ_NEV_ACRES];
   char Property_Lead_Line[RSIZ_NEV_PROPERTY_LEAD_LINE];
   char Clca_Code[RSIZ_NEV_CLCA_CODE];
   char Parent_Parcel[RSIZ_NEV_PARENT_PARCEL];
   char Date_Parcel[RSIZ_NEV_DATE_PARCEL];
   char Child_Parcel[RSIZ_NEV_CHILD_PARCEL];
   char Date_Parcel_Voided[RSIZ_NEV_DATE_PARCEL_VOIDED];
   char Ho_Initiation_Date[RSIZ_NEV_HO_INITIATION_DATE];
   char Perc_Completed[RSIZ_NEV_PERC_COMPLETED];
   char Reason_Incomplete[RSIZ_NEV_REASON_INCOMPLETE];
   char Filler1[RSIZ_NEV_FILLER1];
   char DocDate1[RSIZ_NEV_DOCDATE1];
   char DocNum1[RSIZ_NEV_DOCNUM1];
   char Stamp_Amt1[RSIZ_NEV_STAMP_AMT1];
   char Stamp_Type1[RSIZ_NEV_STAMP_TYPE1];
   char Perc_Transferred1[RSIZ_NEV_PERC_TRANSFERRED1];
   char Filler2[RSIZ_NEV_FILLER2];
   char DocDate2[RSIZ_NEV_DOCDATE2];
   char DocNum2[RSIZ_NEV_DOCNUM2];
   char Stamp_Amt2[RSIZ_NEV_STAMP_AMT2];
   char Stamp_Type2[RSIZ_NEV_STAMP_TYPE2];
   char Percent_Transferred2[RSIZ_NEV_PERCENT_TRANSFERRED2];
   char Filler3[RSIZ_NEV_FILLER3];
   char DocDate3[RSIZ_NEV_DOCDATE3];
   char DocNum3[RSIZ_NEV_DOCNUM3];
   char Stamp_Amt3[RSIZ_NEV_STAMP_AMT3];
   char Stamp_Type3[RSIZ_NEV_STAMP_TYPE3];
   char Percent_Transferred3[RSIZ_NEV_PERCENT_TRANSFERRED3];
   char Filler4[RSIZ_NEV_FILLER4];
   char DocDate4[RSIZ_NEV_DOCDATE4];
   char DocNum4[RSIZ_NEV_DOCNUM4];
   char Stamp_Amt4[RSIZ_NEV_STAMP_AMT4];
   char Stamp_Type4[RSIZ_NEV_STAMP_TYPE4];
   char Percent_Transferred4[RSIZ_NEV_PERCENT_TRANSFERRED4];
   char Filler5[RSIZ_NEV_FILLER5];
   char DocDate5[RSIZ_NEV_DOCDATE5];
   char DocNum5[RSIZ_NEV_DOCNUM5];
   char Stamp_Amt5[RSIZ_NEV_STAMP_AMT5];
   char Stamp_Type5[RSIZ_NEV_STAMP_TYPE5];
   char Percent_Transferred5[RSIZ_NEV_PERCENT_TRANSFERRED5];
   char Filler6[RSIZ_NEV_FILLER6];
} NEV_ROLL;

USEXREF  Nev_UseTbl1[] =
{
   "A","500",           //AGRICULTURAL PROPERTY
   "C","200",           //COMMERCIAL PROPERTY
   "E","600",           //RECREATIONAL PROPERTY
   "G","801",           //GOVERNMENTAL PROPERTY
   "H","801",           //INSTITUTIONAL PROPERTY
   "I","400",           //INDUSTRIAL PROPERTY
   "M","420",           //MINING
   "R","100",           //RESIDENTIAL PROPERTY
   "T","535",           //TIMBER PRESERVE ZONE
   "X","114",           //MOBILE HOME PARK
   "", ""
};

USEXREF  Nev_UseTbl2[] =
{
   "AA","500",          //AGRICULTURAL W/ SGL FAM RESD
   "AB","500",          //AGRICULTURAL W/ MULTI RESID
   "AC","500",          //AGRICULTURAL W/ MULTI RESID
   "AE","500",          //AGRICULTURAL W/ MOBILEHOME
   "AS","536",          //AGRICULTURAL TIMBER - FEE S.
   "AU","827",          //VACANT - AGRIGULTURAL
   "AY","522",          //AGRICULTURAL/WILLIAMSON ACT
   "CF","200",          //COMMERCIAL - MULTI-COMM
   "CG","110",          //MOTEL
   "CH","200",          //COMMERCIAL - WHOLESALE DIST
   "CI","234",          //SERVICE STATION
   "CJ","211",          //RESTAURANT
   "CL","200",          //RETAIL STORE
   "CM","114",          //COMMERCIAL MOBILEHOME PARK
   "CN","300",          //COMMERCIAL - OFFICE && PROF.
   "CV","835",          //VACANT - COMMERCIAL
   "CX","114",          //COMMERCIAL - TRAILER PARK
   "EA","600",          //RECREATIONAL - SGL FAM RESID
   "EB","600",          //RECREATIONAL - MULTI RESID
   "EC","600",          //RECREATIONAL - MULTI RESID
   "ED","600",          //RECREATIONAL - CONDMINIUM
   "EE","600",          //RECREATIONAL - MOBILEHOME
   "EF","600",          //RECREATIONAL - MULTI-COMM
   "ER","600",          //RECREATIONAL - TRAVEL TRAILR
   "EU","834",          //VACANT - RECREATIONAL SFR
   "EV","834",          //VACANT - RECREATIONAL MULTI
   "EX","114",          //RECREATIONAL - TRAILER PARK
   "IT","400",          //INDUSTRIAL PROPERTY - MFG
   "RA","101",          //RESIDENTIAL - SGL FAMILY RES
   "RB","102",          //MULTI RES - DUPLEX/3-4 UNITS
   "RC","102",          //MULTI RES - 5 PLEX OR LARGER
   "RD","107",          //RESIDENTIAL - CONDMINIUM
   "RE","115",          //RESIDENTIAL - MOBILEHOME
   "RF","100",          //RESIDENTIAL - MULTI-COMM
   "RR","114",          //RESIDENTIAL - TRAVEL TRAILER
   "RU","120",          //VACANT - SINGLE FAMLIY RESID
   "RV","120",          //VACANT - MULTI RESIDENTIAL
   "RX","115",          //RESIDENTIAL - TRAILER PARK
   "UA","101",          //SINGLE FAMILY RESIDENCE
   "UB","102",          //DUPLEX OR 3 TO 4 UNITS INCLU
   "UC","102",          //5 PLEX OR LARGER
   "UD","107",          //CONDMINIUM
   "UE","115",          //MOBILEHOME
   "UF","102",          //MULTI-COMM
   "UG","110",          //MOTEL
   "UH","200",          //WHOLESALE DIST
   "UI","234",          //SERVICE STATION
   "UJ","211",          //RESTAURANT
   "UL","200",          //RETAIL STORE
   "UM","114",          //MOBILEHOME PARK
   "UN","300",          //OFFICE AND/OR PROFESSIONAL
   "UR","115",          //TRAVEL TRAILER
   "UU","120",          //VACANT - SINGLE FAMLIY RESID
   "UV","120",          //VACANT - MULTI UNIT
   "UW","120",          //VACANT - COMM
   "UX","114",          //TRAILER PARK
   "", ""
};

USEXREF  Nev_UseTbl3[] =
{
   "CCC","756",         //REST HOME
   "CGG","109",         //HOTEL
   "CGH","127",         //BED && BREAKFAST
   "CII","233",         //AUTOMOTIVE REPAIR
   "CJJ","212",         //RESTAURANT AND LOUNGE
   "CJK","200",         //COCKTAIL LOUNGE
   "CJL","200",         //BEER BAR
   "CJM","214",         //CAFE
   "CJN","215",         //FAST FOOD ESTABLISHMENT
   "CKK","200",         //COMMERCIAL - ARCADE
   "CKL","200",         //BARBER SHOP
   "CKM","200",         //BEAUTY SALON
   "CKN","607",         //BOWLING ALLEY
   "CKQ","601",         //GOLF COURSE
   "CKR","622",         //HEALTH CLUB
   "CKT","611",         //THEATER
   "CKU","758",         //MORTUARY
   "CKV","200",         //LAUNDRY/DRY CLEANER
   "CKW","200",         //LAUNDROMAT
   "CLA","233",         //AUTO PARTS AND/OR TIRES
   "CLB","233",         //AUTO-RV DLR/EQPT RNTL/WRECKG
   "CLC","200",         //COMMERCIAL - FLORIST
   "CLD","246",         //COMMERCL - NURSERY/TREE FARM
   "CLE","200",         //COMMERCL - FEED/FARM SUPPLY
   "CLL","221",         //SUPERMARKET
   "CLM","200",         //MINIMART/NBHD MART/LIQ STORE
   "CLN","200",         //COMMERCIAL - BAKERY
   "CLQ","200",         //DISCNT/VARIETY/SURPLUS STORE
   "CLR","200",         //PHARMACY
   "CLS","200",         //RETAIL - CLOTHES/SHOES
   "CLT","200",         //HARDWR/PLUMBG/SPORTING GOODS
   "CLU","200",         //JEWLRY/GIFT/MUSIC/STNRY ETC.
   "CLV","200",         //FURN/APPLNCE/CRPT/DRAPRY ETC
   "CLW","200",         //DEPARTMENT STORE
   "CLX","200",         //COMMERCIAL - LUMBER YARD
   "CLY","200",         //ANTIQUES / SECOND-HAND STORE
   "CLZ","200",         //BUSINESS MACH/MEDICAL SUPPLY
   "CNN","316",         //COMMERCIAL - MEDICAL
   "CNQ","316",         //COMMERCIAL - DENTAL
   "CNR","316",         //COMMERCIAL - OPTICAL
   "CNS","317",         //COMMERCIAL - VETERINARY
   "CNT","313",         //COMMERCIAL - BANK / S&&L
   "UCC","756",         //REST HOME
   "UGG","109",         //HOTEL
   "UGH","127",         //BED && BREAKFAST
   "UII","233",         //AUTOMOTIVE REPAIR
   "UJJ","212",         //RESTAURANT AND LOUNGE
   "UJK","200",         //COCKTAIL LOUNGE
   "UJL","200",         //BEER BAR
   "UJM","214",         //CAFE
   "UJN","215",         //FAST FOOD ESTABLISHMENT
   "UKK","200",         //ARCADE
   "UKL","200",         //BARBER SHOP
   "UKM","200",         //BEAUTY SALON
   "UKN","607",         //BOWLING ALLEY
   "UKQ","601",         //GOLF COURSE
   "UKR","622",         //HEALTH CLUB
   "UKT","611",         //THEATER
   "UKU","758",         //MORTUARY
   "UKV","200",         //LAUNDRY/DRY CLEANER
   "UKW","200",         //LAUNDROMAT
   "ULA","233",         //AUTO PARTS AND/OR TIRES
   "ULB","233",         //AUTO-RV DLR/EQPT RNTL/WRECKG
   "ULC","200",         //RETAIL FLORIST
   "ULD","246",         //RETAIL NURSERY / TREE FARM
   "ULE","200",         //RETAIL FEED / FARM SUPPLIES
   "ULL","221",         //SUPERMARKET
   "ULM","200",         //MINIMART/NBHD MART/LIQ STORE
   "ULN","200",         //RETAIL BAKERY
   "ULQ","200",         //DISCNT/VARIETY/SURPLUS STORE
   "ULR","200",         //PHARMACY
   "ULS","200",         //RETAIL - CLOTHES/SHOES
   "ULT","200",         //HARDWR/PLUMBG/SPORTING GOODS
   "ULU","200",         //JEWLRY/GIFT/MUSIC/STNRY ETC.
   "ULV","200",         //FURN/APPLNCE/CRPT/DRAPRY ETC
   "ULW","200",         //DEPARTMENT STORE
   "ULX","200",         //RETAIL LUMBER YARD
   "ULY","200",         //ANTIQUES / SECOND-HAND STORE
   "ULZ","200",         //BUSINESS MACH/MEDICAL SUPPLY
   "UNN","316",         //MEDICAL
   "UNQ","316",         //DENTAL
   "UNR","316",         //OPTICAL
   "UNS","317",         //VETERINARY
   "UNT","313",         //BANK OR SAVING && LOAN
   "", ""
};

#define  NCALS1_APN           0
#define  NCALS1_DEEDNUM       1
#define  NCALS1_DEEDDT        2
#define  NCALS1_USECD         3
#define  NCALS1_REJECT        4
#define  NCALS1_CASHEQUIV     5
#define  NCALS1_SALEPR        6
#define  NCALS1_ACREAGE       7
#define  NCALS1_TOPO          8
#define  NCALS1_FRONTAGE      9
#define  NCALS1_VIEW          10
#define  NCALS1_WATER         11
#define  NCALS1_ZONE          12
#define  NCALS1_QUALITY       13
#define  NCALS1_BLDGSQFT      14
#define  NCALS1_STORY         15
#define  NCALS1_BEDRMS        16
#define  NCALS1_BATHS         17
#define  NCALS1_ROOMS         18
#define  NCALS1_FIREPL        19
#define  NCALS1_AC            20
#define  NCALS1_GARAGE        21
#define  NCALS1_POOL          22
#define  NCALS1_GUEST         23
#define  NCALS1_MISCIMP       24
#define  NCALS1_ABNORMAL      25
#define  NCALS1_CONDITION     26
#define  NCALS1_EFFYR         27
#define  NCALS1_LANDTOT       28
#define  NCALS1_COST_ACR      29
#define  NCALS1_COST_FT       30
#define  NCALS1_REPET_SL_DT   31
#define  NCALS1_REPET_SL_AMT  32
#define  NCALS1_GROUPSL       33
#define  NCALS1_IND_SALE      34
#define  NCALS1_DOWNPYMT      35
#define  NCALS1_NEIGHBOR      36
#define  NCALS1_LEADLINE      37
#define  NCALS1_INVDT         38
#define  NCALS1_INVAPPR       39
#define  NCALS1_INTACCESS     40
#define  NCALS1_APPRREM       41
#define  NCALS1_PCTBLT        42
#define  NCALS1_PARTIAL       43
#define  NCALS1_UNITS         44
#define  NCALS1_ACCESS        45
#define  NCALS1_ROAD          46
#define  NCALS1_GRNDCVR       47
#define  NCALS1_IRREGLOT      48
#define  NCALS1_SEWER         49
#define  NCALS1_SUBDV         50
#define  NCALS1_CORNER        51
#define  NCALS1_POWER         52
#define  NCALS1_GAS           53
#define  NCALS1_YRBLT         54
#define  NCALS1_ARCHITECT     55
#define  NCALS1_SQFTRNG       56
#define  NCALS1_1STFLSQFT     57
#define  NCALS1_BASEMENT      58
#define  NCALS1_BASESQFT      59
#define  NCALS1_2NDFL         60
#define  NCALS1_2NDFLSQFT     61
#define  NCALS1_GARSQFT       62
#define  NCALS1_PORCH         63
#define  NCALS1_PORCHSQFT     64
#define  NCALS1_FLATWRKCD     65
#define  NCALS1_FLATWRKSQFT   66
#define  NCALS1_MISCSQFT      67
#define  NCALS1_GUESTSQFT     68
#define  NCALS1_FUNC_OBS      69
#define  NCALS1_ECON_OBS      70

#define  NCDESC1_APN          0
#define  NCDESC1_DT_UPD       1
#define  NCDESC1_DT_LST_INV   2
#define  NCDESC1_INV_APPR     3
#define  NCDESC1_INT_ACCESS   4
#define  NCDESC1_ACREAGE      5
#define  NCDESC1_LEAD_LINE    6
#define  NCDESC1_FROM_DT      7
#define  NCDESC1_FROM_REM     8
#define  NCDESC1_TO_DT        9
#define  NCDESC1_CHG_TO       10
#define  NCDESC1_APPR_REM     11
#define  NCDESC1_CLER_REM     12
#define  NCDESC1_DRAFT_REM    13
#define  NCDESC1_BLT          14
#define  NCDESC1_PARTIAL      15
#define  NCDESC1_UNITS        16
#define  NCDESC1_ZONE         17
#define  NCDESC1_NEIGHBOR     18
#define  NCDESC1_SALE_PR      19
#define  NCDESC1_CASH_EQUIV   20
#define  NCDESC1_DOWN_PAY     21
#define  NCDESC1_WATER        22
#define  NCDESC1_ACCESS       23
#define  NCDESC1_ROAD         24
#define  NCDESC1_TOPO         25
#define  NCDESC1_VIEW         26
#define  NCDESC1_FRONTAGE     27
#define  NCDESC1_GRND_CVR     28
#define  NCDESC1_IRREG_LOT    29
#define  NCDESC1_SEWER        30
#define  NCDESC1_SUBDV        31
#define  NCDESC1_CORNER       32
#define  NCDESC1_POWER        33
#define  NCDESC1_GAS          34
#define  NCDESC1_REJECT       35
#define  NCDESC1_QUALITY      36
#define  NCDESC1_STORY        37
#define  NCDESC1_YR_BLT       38
#define  NCDESC1_EFF_YR       39
#define  NCDESC1_CONDITION    40
#define  NCDESC1_ARCHITECT    41
#define  NCDESC1_FIREPL       42
#define  NCDESC1_AC_HEAT      43
#define  NCDESC1_POOL         44
#define  NCDESC1_BEDRMS       45
#define  NCDESC1_BATHS        46
#define  NCDESC1_ROOMS        47
#define  NCDESC1_SQFT_RNG     48
#define  NCDESC1_SQFT         49
#define  NCDESC1_1ST_FL_SQFT  50
#define  NCDESC1_BASEMENT     51
#define  NCDESC1_BASE_SQFT    52
#define  NCDESC1_2ND_FL       53
#define  NCDESC1_2ND_FL_SQFT  54
#define  NCDESC1_GARAGE       55
#define  NCDESC1_GAR_SQFT     56
#define  NCDESC1_PORCH        57
#define  NCDESC1_PORCH_SQFT   58
#define  NCDESC1_FLATWRK_CD   59
#define  NCDESC1_FLATWRK_SQFT 60
#define  NCDESC1_MISC_IMP     61
#define  NCDESC1_MISC_SQFT    62
#define  NCDESC1_GUEST        63
#define  NCDESC1_GUEST_SQFT   64
#define  NCDESC1_FUNC_OBS     65
#define  NCDESC1_ECON_OBS     66

// NCADDRSS.TXT
#define  NEV_S_APN            0
#define  NEV_S_STRNUM         1
#define  NEV_S_STRNAME        2
#define  NEV_S_CITY           3

// NVALNOTICE2014
#define  NEV_TAXYEAR          0
#define  NEV_APN              1
#define  NEV_OWNER            2
#define  NEV_CONAME           3
#define  NEV_STREET           4
#define  NEV_CITYSTATE        5
#define  NEV_LANDTOTAL        6
#define  NEV_IMPRTOTAL        7
#define  NEV_FIXTVAL          8
#define  NEV_PPVAL            9
#define  NEV_GROSS            10
#define  NEV_EXMPTNVAL        11
#define  NEV_VALUETOTAL       12
#define  NEV_REASON           13
#define  NEV_SITUS            14
#define  NEV_P13LANDTOTAL     15
#define  NEV_P13IMPRTOTAL     16
#define  NEV_P13FIXTVAL       17
#define  NEV_P13PPVAL         18
#define  NEV_P13GROSS         19
#define  NEV_P13EXMPTNVAL     20
#define  NEV_P13VALUETOTAL    21

// QVALNOTICE2017 & REGVALNOTICE2017.TXT
#define  NEV_QR_TAXYEAR        0
#define  NEV_QR_APN            1
#define  NEV_QR_OWNER          2
#define  NEV_QR_CONAME         3
#define  NEV_QR_STREET         4
#define  NEV_QR_CITYSTATE      5
#define  NEV_QR_LANDTOTAL      6
#define  NEV_QR_IMPRTOTAL      7
#define  NEV_QR_FIXTVAL        8
#define  NEV_QR_PPVAL          9
#define  NEV_QR_EXMPTNVAL      10
#define  NEV_QR_VALUETOTAL     11
#define  NEV_QR_REASON         12
#define  NEV_QR_SITUS          13

// NCSECTAX.TXT
#define  NCSEC_ASSMT				0
#define  NCSEC_APN				1
#define  NCSEC_OWNER				2
#define  NCSEC_CAREOF			3
#define  NCSEC_STREET			4
#define  NCSEC_ZIP				5
#define  NCSEC_TAC				6
#define  NCSEC_ROLL_YR			7
#define  NCSEC_LAND_VALUE		8
#define  NCSEC_IMPR_VALUE		9
#define  NCSEC_EXEMPT_CODE		10
#define  NCSEC_EXEMPT_VALUE	11
#define  NCSEC_NET_VALUE		12
#define  NCSEC_1CHG_CODE		13
#define  NCSEC_1CHG_AMT			14
#define  NCSEC_2CHG_CODE		15
#define  NCSEC_2CHG_AMT			16
#define  NCSEC_1ST_PAID			17
#define  NCSEC_2ND_PAID			18
#define  NCSEC_1ST_DUE			19
#define  NCSEC_2ND_DUE			20
#define  NCSEC_COUNTY_RATE		21
#define  NCSEC_NID_RATE			22
#define  NCSEC_PERS_VALUE		23
#define  NCSEC_BUSF_VALUE		24
#define  NCSEC_RT_CODE			25
#define  NCSEC_STATUS			26
#define  NCSEC_BILL_STATUS		27
#define  NCSEC_PYMT_TAPE		28
#define  NCSEC_PYMT_CODE		29
#define  NCSEC_LENDER			30
#define  NCSEC_USE_CD			31
#define  NCSEC_RC_TRAIL			32
#define  NCSEC_RC_REFUND		33
#define  NCSEC_BATCH				34
#define  NCSEC_SOURCE			35
#define  NCSEC_CMTS				36
#define  NCSEC_OLD_APN			37
#define  NCSEC_OLD_TAC			38
#define  NCSEC_LEND_NBR			39
#define  NCSEC_LEND_LOAN		40
#define  NCSEC_CITY_ST			41
#define  NCSEC_PAID_AMTS		42
#define  NCSEC_PEN_VAL			43
#define  NCSEC_CORR_NUM			44
#define  NCSEC_LEND_BILL_FLAG	45
#define  NCSEC_NUM_UNITS		46
#define  NCSEC_LIEN_DT_OWNER	47
#define  NCSEC_OTHER_CODE		48
#define  NCSEC_OTHER_COSTS		49
#define  NCSEC_OTHER_DATE		50
#define  NCSEC_OCCUP_TAX_FLAG	51

// NCDELINQTAX.TXT
#define  NCDELQ_ID            0
#define  NCDELQ_TAXYEAR       1
#define  NCDELQ_BILL          2
#define  NCDELQ_LAND          3
#define  NCDELQ_IMPROV        4
#define  NCDELQ_PERPROP       5
#define  NCDELQ_OTHER         6
#define  NCDELQ_EXEMP         7
#define  NCDELQ_APN           8
#define  NCDELQ_TAC           9
#define  NCDELQ_PENAMT        10
#define  NCDELQ_COST          11
#define  NCDELQ_CODE          12
#define  NCDELQ_TAXAMT        13
#define  NCDELQ_NO_PEN_STATE  14
#define  NCDELQ_OTHER_CODE    15
#define  NCDELQ_OTHER_COSTS   16
#define  NCDELQ_OTHER_INIT    17
#define  NCDELQ_OTHER_DATE    18
#define  NCDELQ_OTHER_PAID    19
#define  NCDELQ_OTHER_DESC    20

#endif


