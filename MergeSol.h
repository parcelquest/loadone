#ifndef _MERGESOL_H_
#define _MERGESOL_H_

// 2024 SecuredOfficialRoll.txt 2024-07-12
#define  SOL_L_APN            0
#define  SOL_L_MAPBOOK        1
#define  SOL_L_ACRES          2
#define  SOL_L_LOTSQFT        3
#define  SOL_L_USECODE        4
#define  SOL_L_NUM_UNITS      5
#define  SOL_L_MAPTYPE        6
#define  SOL_L_MAPBOOKPAGE    7
#define  SOL_L_MAPBLOCK       8
#define  SOL_L_MAPLOT         9
#define  SOL_L_MAPSUBLOT      10 
#define  SOL_L_SUBDIV_NAME    11
#define  SOL_L_SUBDIV_UNIT    12
#define  SOL_L_CENSUSTRACT    13
#define  SOL_L_TAXAREACODE    14
#define  SOL_L_IMPR_PRCL      15
#define  SOL_L_REMAP_PRCLNO   16
#define  SOL_L_ASSESSEE       17
#define  SOL_L_M_ADDR1        18
#define  SOL_L_M_ADDR2        19
#define  SOL_L_M_ATTN         19
#define  SOL_L_M_CITYST       20
#define  SOL_L_S_STRNUM       21
#define  SOL_L_S_STRNAME      22
#define  SOL_L_S_CITYCODE     23
#define  SOL_L_S_BLDG         24
#define  SOL_L_S_UNIT         25
#define  SOL_L_AG_NUM         26
#define  SOL_L_AG_STATUS      27
#define  SOL_L_MINRIGHT_VAL   28
#define  SOL_L_LAND_VAL       29
#define  SOL_L_IMPR_VAL       30
#define  SOL_L_TREE_VAL       31
#define  SOL_L_PP_DATA        32
#define  SOL_L_PP_STATUS      33
#define  SOL_L_PP_CATEGORY    34
#define  SOL_L_DBANAME        35
#define  SOL_L_PP_VAL         36
#define  SOL_L_FIX_VAL        37
#define  SOL_L_PEN_VAL        38
#define  SOL_L_EXE_TYPE       39    // HOX, DVX, LDVX, NA(Does not apply)
#define  SOL_L_HOEXE          40
#define  SOL_L_WFEXE          41
#define  SOL_L_VETEXE         42
#define  SOL_L_GOV_OWN        43
#define  SOL_L_FLDS           44

// SecuredOfficialRoll.txt 2023-07-07
//#define  SOL_L_APN            0
//#define  SOL_L_ACRES          1
//#define  SOL_L_LOTSQFT        2
//#define  SOL_L_USECODE        3
//#define  SOL_L_NUM_UNITS      4
//#define  SOL_L_MAPTYPE        5
//#define  SOL_L_MAPBOOKPAGE    6
//#define  SOL_L_MAPBLOCK       7
//#define  SOL_L_MAPLOT         8
//#define  SOL_L_MAPSUBLOT      9 
//#define  SOL_L_SUBDIV_NAME    10
//#define  SOL_L_SUBDIV_UNIT    11
//#define  SOL_L_CENSUSTRACT    12
//#define  SOL_L_TAXAREACODE    13
//#define  SOL_L_ASSESSEE       14
//#define  SOL_L_M_ATTN         15
//#define  SOL_L_M_ADDR1        16
//#define  SOL_L_M_ADDR2        17
//#define  SOL_L_S_STRNUM       18
//#define  SOL_L_S_STRNAME      19
//#define  SOL_L_S_CITYCODE     20
//#define  SOL_L_S_BLDG         21
//#define  SOL_L_S_UNIT         22
//#define  SOL_L_MINRIGHT_VAL   23
//#define  SOL_L_LAND_VAL       24
//#define  SOL_L_IMPR_VAL       25
//#define  SOL_L_TREE_VAL       26
//#define  SOL_L_DATECHG        27
//#define  SOL_L_PP_VAL         28
//#define  SOL_L_FIX_VAL        29
//#define  SOL_L_PEN_VAL        30
//#define  SOL_L_PP_CATEGORY    31
//#define  SOL_L_EXE_TYPE       32    // HO, DV(Disable veteran), VT(Veterans), NA(Does not apply)
//#define  SOL_L_HOEXE          33
//#define  SOL_L_WFEXE          34
//#define  SOL_L_VETEXE         35
//#define  SOL_L_FLDS           36

// SecuredAssessorRoll.txt 2023-06-01
#define  SOL_R_APN            0
#define  SOL_R_MAPBOOK        1
#define  SOL_R_ACRES          2
#define  SOL_R_LOTSQFT        3
#define  SOL_R_USECODE        4
#define  SOL_R_NUM_UNITS      5
#define  SOL_R_MAPTYPE        6
#define  SOL_R_MAPBOOKPAGE    7
#define  SOL_R_MAPBLOCK       8
#define  SOL_R_MAPLOT         9
#define  SOL_R_MAPSUBLOT      10
#define  SOL_R_SUBDIV_NAME    11
#define  SOL_R_SUBDIV_UNIT    12
#define  SOL_R_CENSUSTRACT    13
#define  SOL_R_TAXAREACODE    14
#define  SOL_R_IMPR_PARCEL    15
#define  SOL_R_REMAP_APN      16
#define  SOL_R_ASSESSEE       17
#define  SOL_R_M_ADDR1        18
#define  SOL_R_M_ATTN         19
#define  SOL_R_M_ADDR2        20
#define  SOL_R_S_STRNUM       21
#define  SOL_R_S_STRNAME      22
#define  SOL_R_S_CITYCODE     23
#define  SOL_R_S_BLDG         24
#define  SOL_R_S_UNIT         25
#define  SOL_R_AG_NUMBER      26
#define  SOL_R_AG_STATUS      27    // AC
#define  SOL_R_MINRIGHT_VAL   28
#define  SOL_R_LAND_VAL       29
#define  SOL_R_IMPR_VAL       30
#define  SOL_R_TREE_VAL       31
#define  SOL_R_PP_DATA        32
#define  SOL_R_PP_STATUS      33
#define  SOL_R_PP_CATEGORY    34
#define  SOL_R_DBANAME        35
#define  SOL_R_PP_VAL         36
#define  SOL_R_FIX_VAL        37
#define  SOL_R_PEN_VAL        38
#define  SOL_R_EXE_TYPE       39    // HO, DV(Disable veteran), VT(Veterans), NA(Does not apply)
#define  SOL_R_HOEXE          40
#define  SOL_R_WFEXE          41
#define  SOL_R_VETEXE         42
#define  SOL_R_GOV_OWNED      43
#define  SOL_R_FLDS           44

// rbp.txt - 2023-05-18
//#define  SOL_R_APN            0
//#define  SOL_R_ACRES          1
//#define  SOL_R_LOTSQFT        2
//#define  SOL_R_USECODE        3
//#define  SOL_R_NUM_UNITS      4
//#define  SOL_R_MAPTYPE        5
//#define  SOL_R_MAPBOOKPAGE    6
//#define  SOL_R_MAPBLOCK       7
//#define  SOL_R_MAPLOT         8
//#define  SOL_R_MAPSUBLOT      9
//#define  SOL_R_SUBDIV_NAME    10
//#define  SOL_R_SUBDIV_UNIT    11
//#define  SOL_R_CENSUSTRACT    12
//#define  SOL_R_TAXAREACODE    13
//#define  SOL_R_IMPR_PARCEL    14
//#define  SOL_R_REMAP_PARCEL   15
//#define  SOL_R_ASSESSEE       16
//#define  SOL_R_M_ADDR1        17
//#define  SOL_R_M_ATTN         18
//#define  SOL_R_M_ADDR2        19
//#define  SOL_R_S_STRNUM       20
//#define  SOL_R_S_STRNAME      21
//#define  SOL_R_S_CITYCODE     22
//#define  SOL_R_S_BLDG         23
//#define  SOL_R_S_UNIT         24
//#define  SOL_R_AG_NUMBER      25
//#define  SOL_R_AG_STATUS      26    // AC
//#define  SOL_R_MINRIGHT_VAL   27
//#define  SOL_R_LAND_VAL       28
//#define  SOL_R_IMPR_VAL       29
//#define  SOL_R_TREE_VAL       30
//#define  SOL_R_PP_DATA        31
//#define  SOL_R_PP_STATUS      32
//#define  SOL_R_PP_CATEGORY    33
//#define  SOL_R_DBANAME        34
//#define  SOL_R_PP_VAL         35
//#define  SOL_R_FIX_VAL        36
//#define  SOL_R_PEN_VAL        37
//#define  SOL_R_EXE_TYPE       38    // HO, DV, VT, NA
//#define  SOL_R_HOEXE          39
//#define  SOL_R_WFEXE          40
//#define  SOL_R_VETEXE         41
//#define  SOL_R_GOV_OWNED      42
//#define  SOL_R_FLDS           43

// conv_Official_Assessor_Roll_Table.csv - 2023-05-09
//#define  SOL_R_APN            0
//#define  SOL_R_ACRES          1
//#define  SOL_R_LOTSQFT        2
//#define  SOL_R_USECODE        3
//#define  SOL_R_NUM_UNITS      4
//#define  SOL_R_MAPTYPE        5
//#define  SOL_R_MAPBOOKPAGE    6
//#define  SOL_R_MAPBLOCK       7
//#define  SOL_R_MAPLOT         8
//#define  SOL_R_MAPSUBLOT      9
//#define  SOL_R_SUBDIV_NAME    10
//#define  SOL_R_SUBDIV_UNIT    11
//#define  SOL_R_CENSUSTRACT    12
//#define  SOL_R_TAXAREACODE    13
//#define  SOL_R_ASSESSEE       14
//#define  SOL_R_M_ATTN         15
//#define  SOL_R_M_ADDR1        16
//#define  SOL_R_M_ADDR2        17
//#define  SOL_R_S_STRNUM       18
//#define  SOL_R_S_STRNAME      19
//#define  SOL_R_S_CITYCODE     20
//#define  SOL_R_S_BLDG         21
//#define  SOL_R_S_UNIT         22
//#define  SOL_R_MINRIGHT_VAL   23
//#define  SOL_R_LAND_VAL       24
//#define  SOL_R_IMPR_VAL       25
//#define  SOL_R_TREE_VAL       26
//#define  SOL_R_DATEVALUECHG   27
//#define  SOL_R_PP_VAL         28
//#define  SOL_R_FIX_VAL        29
//#define  SOL_R_PEN_VAL        30
//#define  SOL_R_PP_CATEGORY    31
//#define  SOL_R_EXE_TYPE       32    // HO, DV, VT, NA
//#define  SOL_R_HOEXE          33
//#define  SOL_R_WFEXE          34
//#define  SOL_R_VETEXE         35
//#define  SOL_R_FLDS           36

// SecuredAssessorRoll.txt - 2023-05-02
//#define  SOL_R_APN            0
//#define  SOL_R_ACRES          1
//#define  SOL_R_LOTSQFT        2
//#define  SOL_R_USECODE        3
//#define  SOL_R_NUM_UNITS      4
//#define  SOL_R_MAPBLOCK       5
//#define  SOL_R_MAPLOT         6
//#define  SOL_R_MAPSUBLOT      7
//#define  SOL_R_SUBDIV_NAME    8
//#define  SOL_R_SUBDIV_UNIT    9
//#define  SOL_R_CENSUSTRACT    10
//#define  SOL_R_GOV_OWNED      11
//#define  SOL_R_TAXAREACODE    12
//#define  SOL_R_IMPR_PARCEL    13
//#define  SOL_R_REMAP_PARCEL   14
//#define  SOL_R_ASSESSEE       15
//#define  SOL_R_M_ADDR1        16
//#define  SOL_R_M_ATTN         17
//#define  SOL_R_M_ADDR2        18
//#define  SOL_R_EXE_TYPE       19    // HO, DV, VT, NA
//#define  SOL_R_S_STRNAME      20
//#define  SOL_R_S_STRNUM       21
//#define  SOL_R_S_CITYCODE     22
//#define  SOL_R_AG_NUMBER      22
//#define  SOL_R_AG_STATUS      23    // AC
//#define  SOL_R_LAND_VAL       24
//#define  SOL_R_IMPR_VAL       25
//#define  SOL_R_TREE_VAL       26
//#define  SOL_R_PP_DATA        27
//#define  SOL_R_PP_STATUS      28
//#define  SOL_R_PP_CATEGORY    29
//#define  SOL_R_DBANAME        30
//#define  SOL_R_PP_VAL         31
//#define  SOL_R_FIX_VAL        32
//#define  SOL_R_PEN_VAL        33
//#define  SOL_R_FLDS           34

// PropertyCharacteristics_SingleFamilyResidences.txt
#define  SOL_C_APN            0
#define  SOL_C_TAXYEAR        1
#define  SOL_C_ACRES          2
#define  SOL_C_LOTSIZE        3
#define  SOL_C_USECODE        4
#define  SOL_C_USE_DESC       5
#define  SOL_C_CLASS          6
#define  SOL_C_YRBLT          7
#define  SOL_C_STATUS         8
#define  SOL_C_FIRST_AREA     9
#define  SOL_C_SECOND_AREA    10
#define  SOL_C_THIRD_AREA     11
#define  SOL_C_OTHERAREA      12
#define  SOL_C_GARAGE_AREA    13
#define  SOL_C_TOTAL_AREA     14
#define  SOL_C_BEDROOM        15
#define  SOL_C_BATHROOM       16
#define  SOL_C_DINING         17
#define  SOL_C_FAMILY         18
#define  SOL_C_OTHERROOMS     19
#define  SOL_C_UTILITY        20
#define  SOL_C_TOTAL_ROOMS    21
#define  SOL_C_FIREPLC        22
#define  SOL_C_HVAC           23    // HA
#define  SOL_C_POOL           24
#define  SOL_C_SOLAR          25
#define  SOL_C_STORIES        26    // 9.99
#define  SOL_C_FLDS           27

// DelqExport (SD116601) 04.04.23.txt
#define  TD_APN              0
#define  TD_TAX_YEAR         1
#define  TD_BILLNUMBER       2
#define  TD_TAX_DUE          3
#define  TD_PEN_DUE          4
#define  TD_INT_DUE          5
#define  TD_FEE_DUE          6
#define  TD_CUR_BAL_DUE      7
#define  TD_CUR_YR_DELQ      8      // Y/N
#define  TD_PRI_YR_DELQ      9      // Y/N
#define  TD_RED_BAL_DUE      10
#define  TD_RED_1M_DUE       11
#define  TD_RED_2M_DUE       12
#define  TD_ROLLCASTE        13
#define  TD_COMM_RECIP       14
#define  TD_M_ADDR           15
#define  TD_M_ADDR2          16
#define  TD_M_CITY           17
#define  TD_M_ST             18
#define  TD_M_ZIP            19
#define  TD_S_ADDR           20
#define  TD_S_CITY           21
#define  TD_S_ST             22
#define  TD_S_ZIP            23
#define  TD_FLDS             24

// Le04042023.txt
typedef struct _tSd116201
{
   char filler1[16];
   char APN[10];                      // 17
   char filler2[4];
   char Status[4];                    // 31
   char filler3[6];
   char Inst1_Due[9];                 // 41 - 9V99
   char filler4[9];
   char Inst2_Due[9];                 // 59 - 9V99
   char filler5[200];
} SD116201;

#define  SOL_MAXFUNDS         30
#define  TSIZ_FUNDNUM         5
#define  TSIZ_TAXAMT          9
typedef struct _tTaxFund
{
   char TAF_Fund[TSIZ_FUNDNUM];
   char TAF_Tax_Amt[TSIZ_TAXAMT];     // 9V99
} TAX_FUND;

#define  TSIZ_APN             11
#define  TSIZ_BILLNUM         16

// MED04042023.txt
typedef struct _tSd116301
{
   char APN[TSIZ_APN];                // 1
   char filler1[2];
   char TaxYear[4];                   // 14
   char BillNumber[TSIZ_BILLNUM];     // 18
   char RollCaste[15];                // 34
   char CollectionType[15];           // 49
   char TotalTax[TSIZ_TAXAMT];        // 64 - 9V99
   char Penalty[TSIZ_TAXAMT];         // 73 - 9V99
   char Interest[TSIZ_TAXAMT];        // 82 - 9V99
   char Fees[TSIZ_TAXAMT];            // 91 - 9V99
   char BalanceDue[10];               // 100 - 9V99
   TAX_FUND asTaxFunds[SOL_MAXFUNDS]; // 110
   char Inst1_Due[TSIZ_TAXAMT];       // 530 - 9V99
   char Inst2_Due[TSIZ_TAXAMT];       // 539
   char IsDelq;                       // 548 - Y/N
   char Lender_Code[5];               // 549
   char Loan_Account_Code[16];        // 554
   char SA_Addr[35];                  // 570
   char SA_Addr2[35];                 // 605
   char SA_StrNum[5];                 // 640
   char SA_PreDir[2];                 // 645
   char SA_StrName[35];               // 647
   char SA_StrType[6];                // 682
   char SA_UnitNum[5];                // 688
   char SA_PostDir[2];                // 693
   char SA_City[30];                  // 695
   char SA_State[2];                  // 725
   char SA_PostalCd[9];               // 727
   char Owner[35];                    // 736
   char Mail_Name[35];                // 771
   char Mail_Attn[35];                // 806
   char Mail_Address[35];             // 841
   char Mail_LastLine[35];            // 876
   char Export_AA_Value_1[12];        // 911
   char Export_AA_Value_2[12];        // 923
   char Export_AA_Value_3[12];        // 935
   char Export_AA_Value_4[12];        // 947
   char Export_AA_Value_5[12];        // 959
   char Export_AA_Value_6[12];        // 971
   char Export_AA_Value_7[12];        // 983
   char Export_AA_Value_8[12];        // 995
   char Export_AA_Value_9[12];        // 1007
   char Export_AA_Value_10[12];       // 1019
   char Recorded_Acres[6];            // 1031
   char Total_Paid[TSIZ_TAXAMT];      // 1037 - 9V99
   char Inst1_Due_Date[10];           // 1046 - mmddyyyy
   char Inst2_Due_Date[10];           // 1056
   char Payment_Eff_Date[10];         // 1066
   char Red_Grp_Bal_Due[TSIZ_TAXAMT]; // 1076 - 9V99 - Redemption Group Balance Due
   char Red_Due_1_Prior[TSIZ_TAXAMT]; // 1085 - Redemption Group Balance Due - 1 Month Prior
   char Red_Due_2_Prior[TSIZ_TAXAMT]; // 1094 - Redemption Group Balance Due - 2 Months Prior
   char Other_Years_Due;              // 1103 - Y/N
   char Inst1_Receipt_Date[10];       // 1104
   char Inst2_Receipt_Date[10];       // 1114
} SD116301;

/***********************************************************************************
 * SOL roll definition
 ***********************************************************************************/
#define  SIZ_ROLL_APN           10
#define  SIZ_ROLL_ACRES         8
#define  SIZ_ROLL_LOTSIZE       12
#define  SIZ_ROLL_USECODE       4
#define  SIZ_ROLL_NUMUNITS      4
#define  SIZ_ROLL_MAPTYPE       2
#define  SIZ_ROLL_MAPBKPG       8
#define  SIZ_ROLL_BLOCK         4
#define  SIZ_ROLL_LOT           4
#define  SIZ_ROLL_SUBLOT        4
#define  SIZ_ROLL_SUBNAME       30
#define  SIZ_ROLL_SUBDIVUNIT    4
#define  SIZ_ROLL_CENSUS        8
#define  SIZ_ROLL_TRA           6
#define  SIZ_ROLL_ASSESSEE      30
#define  SIZ_ROLL_ADDR1         30
#define  SIZ_ROLL_ADDR2         30
#define  SIZ_ROLL_CITYST        20
#define  SIZ_ROLL_ADDRZIP       10
#define  SIZ_ROLL_ROAD          30
#define  SIZ_ROLL_NUM           6
#define  SIZ_ROLL_CITY          2
#define  SIZ_ROLL_BLDG          2
#define  SIZ_ROLL_UNIT          4
#define  SIZ_ROLL_UNKNOWN       12
#define  SIZ_ROLL_VALCHNGDATE   8
#define  SIZ_ROLL_CATEGORY      4
#define  SIZ_ROLL_HOTYPE        2
#define  SIZ_ROLL_FILLER        12
#define  SIZ_ROLL_CRLF          2
#define  SIZ_ROLL_CONTRACT      8
#define  SIZ_ROLL_STATUS        2
#define  SIZ_ROLL_VALUE         12

typedef struct _tSolRoll
{
   char  Apn[SIZ_ROLL_APN];                // 1-10
   char  Acres[SIZ_ROLL_ACRES];            // 11-18
   char  LotSize[SIZ_ROLL_LOTSIZE];        // 19-30
   char  UseCode[SIZ_ROLL_USECODE];        // 31-34
   char  Units[SIZ_ROLL_NUMUNITS];         // 35-38
   char  MapType[SIZ_ROLL_MAPTYPE];        // 39-40
   char  MapBkPg[SIZ_ROLL_MAPBKPG];        // 41-48
   char  MapBlock[SIZ_ROLL_BLOCK];         // 49-52
   char  MapLot[SIZ_ROLL_LOT];             // 53-56
   char  MapSubLot[SIZ_ROLL_SUBLOT];       // 57-60
   char  MapSubName[SIZ_ROLL_SUBNAME];     // 61-90 Sub Div name
   char  MapSubUnit[SIZ_ROLL_SUBDIVUNIT];  // 91-94
   char  Census[SIZ_ROLL_CENSUS];          // 95-102
   char  TRA[SIZ_ROLL_TRA];                // 103-108
   char  TRA_2[SIZ_ROLL_TRA];              // 109-114
   char  Owners[SIZ_ROLL_ASSESSEE];        // 115-144
   char  M_Addr1[SIZ_ROLL_ADDR1];          // 145-174
   char  M_Addr2[SIZ_ROLL_ADDR2];          // 175-204
   char  M_CitySt[SIZ_ROLL_CITYST];        // 205-224
   char  M_Zip[SIZ_ROLL_ADDRZIP];          // 225-234 99999-9999
   char  S_City[SIZ_ROLL_CITY];            // 235-236
   char  S_StrName[SIZ_ROLL_ROAD];         // 237-266 May contain dir, name, sfx, unit#
   char  S_StrNum[SIZ_ROLL_NUM];           // 267-272
   char  S_Bldg[SIZ_ROLL_BLDG];            // 273-274
   char  S_Unit[SIZ_ROLL_UNIT];            // 275-278
   char  Contract[SIZ_ROLL_CONTRACT];      // 279-286
   char  Status[SIZ_ROLL_STATUS];          // 287-288
   char  Land[SIZ_ROLL_VALUE];             // 289-300
   char  Impr[SIZ_ROLL_VALUE];             // 301-312
   char  TreeVine[SIZ_ROLL_VALUE];         // 313-324
   char  ChgDate[SIZ_ROLL_VALCHNGDATE];    // 325-332
   char  Pers[SIZ_ROLL_VALUE];             // 333-344 Personal property
   char  Fixt[SIZ_ROLL_VALUE];             // 345-356 Fixed machinery & equiptment exempt
   char  Pen[SIZ_ROLL_VALUE];              // 357-368 Late payment penalty
   char  Category[SIZ_ROLL_CATEGORY];      // 369-372
   char  HO_Type[SIZ_ROLL_HOTYPE];         // 373-374
   char  HO_Exe[SIZ_ROLL_VALUE];           // 375-386
   char  Wel_Exe[SIZ_ROLL_VALUE];          // 387-398 Non-profit or welfare exempt
   char  crlf[2];
} SOL_ROLL;

typedef struct _tSolLien
{
   char  Apn[SIZ_ROLL_APN];                // 1-10
   char  Acres[SIZ_ROLL_ACRES];            // 11-18
   char  LotSize[SIZ_ROLL_LOTSIZE];        // 19-30
   char  UseCode[SIZ_ROLL_USECODE];        // 31-34
   char  Units[SIZ_ROLL_NUMUNITS];         // 35-38
   char  MapType[SIZ_ROLL_MAPTYPE];        // 39-40
   char  MapBkPg[SIZ_ROLL_MAPBKPG];        // 41-48
   char  MapBlock[SIZ_ROLL_BLOCK];         // 49-52
   char  MapLot[SIZ_ROLL_LOT];             // 53-56
   char  MapSubLot[SIZ_ROLL_SUBLOT];       // 57-60
   char  MapSubName[SIZ_ROLL_SUBNAME];     // 61-90 Sub Div name
   char  MapSubUnit[SIZ_ROLL_SUBDIVUNIT];  // 91-94
   char  Census[SIZ_ROLL_CENSUS];          // 95-102
   char  TRA[SIZ_ROLL_TRA];                // 103-108
   char  Owners[SIZ_ROLL_ASSESSEE];        // 109-138
   char  M_Addr1[SIZ_ROLL_ADDR1];          // 139-168
   char  M_Addr2[SIZ_ROLL_ADDR2];          // 169-198
   char  M_CitySt[SIZ_ROLL_CITYST];        // 199-218
   char  M_Zip[SIZ_ROLL_ADDRZIP];          // 219-228 99999-9999
   char  S_StrNum[SIZ_ROLL_NUM];           // 229-234
   char  S_StrName[SIZ_ROLL_ROAD];         // 235-264 May contain dir, name, sfx, unit#
   char  S_City[SIZ_ROLL_CITY];            // 265-266
   char  S_Bldg[SIZ_ROLL_BLDG];            // 267-268
   char  S_Unit[SIZ_ROLL_UNIT];            // 269-272
   char  MnrRight[SIZ_ROLL_VALUE];         // 273-284 Mineral right value
   char  Land[SIZ_ROLL_VALUE];             // 285-296
   char  Impr[SIZ_ROLL_VALUE];             // 297-308
   char  TreeVine[SIZ_ROLL_VALUE];         // 309-320
   char  ChgDate[SIZ_ROLL_VALCHNGDATE];    // 321-328
   char  Pers[SIZ_ROLL_VALUE];             // 329-340 Personal property exempt
   char  Fixt[SIZ_ROLL_VALUE];             // 341-352 Fixed machinery & equiptment exempt
   char  Pen[SIZ_ROLL_VALUE];              // 353-364 Late payment penalty
   char  Category[SIZ_ROLL_CATEGORY];      // 365-368
   char  HO_ExeType[SIZ_ROLL_HOTYPE];      // 369-370 HO/DV/VT/NA
   char  HO_Exe[SIZ_ROLL_VALUE];           // 371-382
   char  Wel_Exe[SIZ_ROLL_VALUE];          // 383-394 Non-profit or welfare exempt
   char  Mil_Exe[SIZ_ROLL_VALUE];          // 395-406 Military exempt
   char  crlf[2];                          // 407-408
} SOL_LIEN;

#define  SOL_SIZCHAR_APN            10
#define  SOL_SIZCHAR_DATE           8
#define  SOL_SIZCHAR_TYPE           2
#define  SOL_SIZCHAR_USECODE        4
#define  SOL_SIZCHAR_YEAR           4
#define  SOL_SIZCHAR_LOCATION       30
#define  SOL_SIZCHAR_QUALITY        4
#define  SOL_SIZCHAR_ADDSQFT        4
#define  SOL_SIZCHAR_BLDGAREA       6
#define  SOL_SIZCHAR_TOTALAREA      8
#define  SOL_SIZCHAR_BLDGDESC       30
#define  SOL_SIZCHAR_FLXSQFT        6
#define  SOL_SIZCHAR_CNT            2
#define  SOL_SIZCHAR_QUALCLS        4
#define  SOL_SIZCHAR_4CHAR          4

typedef struct _tSolChar
{
   char  Apn[SOL_SIZCHAR_APN];               // 1
   char  ChgDate[SOL_SIZCHAR_DATE];
   char  PropertyType[SOL_SIZCHAR_TYPE];     // 19 - "SF"
   char  Fl1_Sqft[SOL_SIZCHAR_FLXSQFT];      // 21
   char  Fl2_Sqft[SOL_SIZCHAR_FLXSQFT];      // 27
   char  Flx_Sqft[SOL_SIZCHAR_FLXSQFT];      // 33
   char  Beds[SOL_SIZCHAR_CNT];              // 39
   char  Baths[SOL_SIZCHAR_CNT];             // 41 - 9V9
   char  Dining[SOL_SIZCHAR_CNT];            // 43 - YS/NO
   char  Family[SOL_SIZCHAR_CNT];            // 45 - YS/NO
   char  OthRooms[SOL_SIZCHAR_CNT];          // 47 - count
   char  UtilRoom[SOL_SIZCHAR_CNT];          // 49 - YS/NO
   char  Pool[SOL_SIZCHAR_CNT];              // 51 - Desc of pool: NO/CN/PL
   char  Gar_Sqft[SOL_SIZCHAR_FLXSQFT];      // 53
   char  Qual_Cls[SOL_SIZCHAR_QUALCLS];      // 59 - 999V9
   char  YrBlt[SOL_SIZCHAR_YEAR];            // 63
   char  Fp[SOL_SIZCHAR_TYPE];               // 67 - YS/NO or 1/0
   char  Central_HA[SOL_SIZCHAR_TYPE];       // 69 - HE/HA/NO
   char  Add_Sqft[SOL_SIZCHAR_ADDSQFT];      // 71
   char  Add_Factor[SOL_SIZCHAR_4CHAR];      // 75 - 99V99
   char  Patio_Code[SOL_SIZCHAR_TYPE];       // 79
   char  Patio_Desc[SOL_SIZCHAR_BLDGDESC];   // 81
   char  Patio_sqft[SOL_SIZCHAR_ADDSQFT];    // 111
   char  OtherArea[SOL_SIZCHAR_BLDGAREA];    // 115
   char  OtherDesc[SOL_SIZCHAR_BLDGDESC];    // 121
   char  CrLf[2];
} SOL_CHAR;

typedef struct _tSolComChar
{
   char  Apn[SOL_SIZCHAR_APN];            // 1
   char  ChgDate[SOL_SIZCHAR_DATE];       // 11
   char  UseCode[SOL_SIZCHAR_USECODE];    // 19
   char  BldgArea[SOL_SIZCHAR_BLDGAREA];  // 23
   char  NetArea[SOL_SIZCHAR_BLDGAREA];   // 29
   char  Location[SOL_SIZCHAR_LOCATION];  // 35 - City name
   char  YrBlt[SOL_SIZCHAR_YEAR];         // 65
   char  EffYr[SOL_SIZCHAR_YEAR];         // 69 - 0000=unknown
   char  CrLf[2];
} SOL_COMCHAR;

typedef struct _tSolMultiChar
{
   char  Apn[SOL_SIZCHAR_APN];            // 1
   char  ChgDate[SOL_SIZCHAR_DATE];       // 11
   char  UseCode[SOL_SIZCHAR_USECODE];    // 19
   char  Location[SOL_SIZCHAR_LOCATION];  // 23 - City name
   char  NumStudio[SOL_SIZCHAR_4CHAR];    // 53
   char  SizStudio[SOL_SIZCHAR_4CHAR];    // 57
   char  NumOneBed[SOL_SIZCHAR_4CHAR];    // 61
   char  SizOneBed[SOL_SIZCHAR_4CHAR];    // 65
   char  NumTwoBed[SOL_SIZCHAR_4CHAR];    // 69
   char  SizTwoBed[SOL_SIZCHAR_4CHAR];    // 73
   char  NumOtherRoom[SOL_SIZCHAR_4CHAR]; // 77
   char  SizOtherRoom[SOL_SIZCHAR_4CHAR]; // 81
   char  TotalUnits[SOL_SIZCHAR_4CHAR];   // 85
   char  Qual_Cls[SOL_SIZCHAR_QUALCLS];   // 89 - 999V9
   char  YrBlt[SOL_SIZCHAR_YEAR];         // 93
   char  EffYr[SOL_SIZCHAR_YEAR];         // 97
   char  BldgArea[SOL_SIZCHAR_TOTALAREA]; // 101
   char  RecrArea[SOL_SIZCHAR_BLDGAREA];  // 109
   char  NumStories[SOL_SIZCHAR_CNT];     // 115
   char  NumPools[SOL_SIZCHAR_CNT];       // 117 - 0000=unknown
} SOL_MCHAR;

char *asCityAbbr[]=
{
   "BN", "DX", "FF", "RV", "SU", "UN", "VV", "VJ", NULL
};

char *asCityName[]=
{
   "BENICIA", "DIXON", "FAIRFIELD", "RIO VISTA", "SUISUN CITY",
   "UNINCORPORATED", "VACAVILLE", "VALLEJO", NULL
};

IDX_TBL4 SOL_DocType[] =
{
   "DEED OR GRANT",     "1 ", 10, 2,
   "DEED - GRANT",      "1 ", 10, 2,
   "DEED - TRUSTEES",   "27", 14, 2,
   "QUITCLAIM",         "4 ", 9,  2,
   "AGMT OF SALE",      "8 ", 12, 2,
   "DECREE OF DIST",    "80", 12, 2,
   "","",0,0
};

// Standard codes
// H=Homeowners
// D=Disabled Veterans
// V=Veterans
// C=Church
// L=Library
// M=Museums
// R=Religion
// T=Tribal Housing
// W=Welfare
// U=College
// P=Public School
// E=Cemetery
// I=Hospital
// S=School
// X=Misc exempt
// Y=Military

IDX_TBL4 SOL_Exemption[] = 
{
   "HO", "H",  2,1,
   "DV", "D",  2,1,
   "LD", "V",  2,1,
   "SS", "Y",  2,1,     // SOLDIERS & SAILORS
   "WE", "W",  2,1,
   "","",0,0
};

#endif
