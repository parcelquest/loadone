/**************************************************************************
 *
 *  Usage: LoadOne -C<County code> [-A] [-B?] [-D[?]][-F?] [-G] [-M] [-N] [-P?] [-U[?]] [-L[?]] [-X?] [-S<n>]
 *          -A : Merge attribute file
 *          -B8: Merge prop8 data
 *          -D : Duplicate sale records from old file
 *          -D1: Convert cum sale to SCSAL_REC file
 *          -D2: Convert cum grgr to SCSAL_REC file
 *          -Dc: Create update file
 *          -Dn: Don't create update file.  This is used to skip creating update file when debug.
 *          -D8: Update prop8 flag in DB
 *          -Dr: Do not remove temp files
 *          -Dt: Do not translate EBCDIC to ASCII (use existing file)
 *          -Ft: Fix TRA
 *          -Fn: Fix owner names (convert to upper case)
 *          -Fs: Fix saleamt.  
 *          -Fe: Fix full exemption flag
 *          -Fx: Fix DocType
 *          -Fy: Fix DocNum
 *          -G : Create GrGr file for MergeAdr
 *          -Gi: Create GrGr sql import file
 *          -Is: Import cumumative sale to SQL (???_Sale.sls)
 *          -Ig: Import cumumative GrGr to SQL (???_Grgr.sls)
 *          -La: Load assessor file (or DTW)
 *          -Lg: Load GrGr file (county specific output)
 *          -Ll: Load land file (for county that has sale file).
 *          -Ls: Load sale file (for county that has sale file).
 *          -Lu: Load all update files (VEN)
 *          -Lz: Load zipcode file into SQL
 *          -M1: Merge address1
 *          -Ma: Merge attribute
 *          -Mg: Merge GrGr file
 *          -Mp: Merge public parcel (FRE, KIN, MPA)
 *          -Mr: Merge acreage
 *          -Mn: Merge sale from NDC
 *          -Ms[x]: Merge cumulative sale file [remove transfer]
 *          -Mt: Merge transfer data
 *          -N : Do not encode suffix and city
 *          -Nm: Don't send mail 
 *          -O : Overwrite log file
 *          -Ps: Purge all sales before update data from sale file (SAC)
 *          -Sn: Number of records skip (default 1)
 *          -T : Load tax file
 *          -U : Update roll file using old S01 file
 *          -Ua: Update assessor file using old S01 file
 *          -Uc: Update cumulative Char file
 *          -Ur: Update roll file replace current values in S01 file
 *          -Us: Update cumulative sale file
 *          -Ut: Update tax table
 *          -Uu: Update standard usecode on R01
 *          -Ux: Update sale extracted from R0x file
 *          -X1: Extract situs from roll file.
 *          -X8: Extract prop8.
 *          -Xa: Extract attribute from attr file or from R01 file (CHAR_REC)
 *          -Xc: Extract cumulative sale from mainframe sale file.
 *          -XC: Extract XC file for bulk customer
 *          -Xd: Extract legal description
 *          -Xl: Extract lien value from lien file.
 *          -Xr: Extract region file (LAX).
 *          -Xs: Extract sale data for MergeAdr from sale file.
 *          -Ynnnn: Process year.  This overwrite default year in CountyInfo.csv
 *
 * Notes:, 
 *    1) -A is used for SCR to merge attribute data to R01 file
 *    2) -D is used for SOL to copy sale data from previous month to the next
 *          in case we don't receive sale data anymore
 *    3) -G is used to create grgr data file for MergeAdr.  This option creates
 *          Sale_exp.dat which will be merged into G01 file during MergeAdr process.
 *    4) -L Create new R01 file.  After successful test, it should be renamed
 *          to .S01 and used as based file for monthly update.
 *    5) -N Use this option when create output not for PQ4.
 *    6) -U Use this option for monthly update.  Require S01 file.
 *
 *    7) Even though we have function CreateAlaRoll() to load LDR file, we shouldn't
 *       use -L function for ALA.  Instead you can replace UPDATE_R01 with CREATE_R01
 *       in MergeAlaRoll() and run with -U option.  This can change if county sends us
 *       attribute file again.
 *    8) Counties that has county product: LAX, SDX
 *
 * History:
 * 07/07/2005 1.0.0    First production supports ALA and SOL - spn
 * 07/11/2005 1.0.1    Work around to get attribute data from last year roll
 *                     into the new one.
 * 08/12/2005 1.0.5    Fix bug for SOL update in doOwner.cpp and MergeSol.cpp
 *                     County has changed the roll layout.  Reclen is now 398 bytes.
 * 08/26/2005 1.0.6    Get FRE GrGr working.
 * 09/06/2005 1.0.7    Change sort order in MergeScr.cpp to omit sale record
 *                     without Doc#.
 * 03/30/2006 1.1.12   Adding VEN - support city table Code-City-index
 *                     Adding -Ux option to update sale from extract file
 * 05/16/2006 1.2.13   Reset automation flag State='W' before processing and set
 *                     it to 'P' if successful, 'F' if fail
 * 06/01/2006 1.2.15   Rename MergeGrGrRec() to MergeGrGrDoc() to correctly
 *                     reflect the merging of GRGR_DOC record to roll file.
 * 10/10/2006 1.3.0    Add asRecCnt[] to store reccnt of different products
 *                     in the same county on the same run.
 * 12/17/2007 1.4.33.2 Set lRecCnt in MergeSaleRec() and MergeGrGrRec() to force
 *                     update flag that starts BUILDCDA run.
 * 01/04/2008 1.5.2    Add function MergeLotArea() to merge LotArea data for LAX.
 *                     This function can be used for other counties.
 * 01/22/2008 1.5.3.11 Modify MergeSaleRec() to use S01 if R01 file is not avail.
 *                     This could happen when user run with -Lg without -U.
 * 03/11/2008 1.5.6    Use updCmpS01R01() to create update file.
 * 03/14/2008 1.5.7    Add -Nu option to force not to create update file.
 * 03/19/2008 1.5.7.1  Fix MergeLotArea() for LAX.
 * 03/24/2008 1.5.7.2  Adding -Uu option to update std use.
 * 05/30/2008 1.6.0    Update county table LastRecDate, LastFileDate (roll), and LastGrGrDate.
 * 06/10/2008 1.6.3    Add email option
 * 10/07/2008 8.3.4    Remove SBT, it has been moved to LoadMB.
 * 10/13/2008 8.4.0    Verify record count.
 * 11/17/2008 8.4.3    Add -Lu option to process unsecured file.
 * 11/18/2008 8.4.4    Modify MergeGrGrDoc() to update S01 file if R01 is not avail.
 *                     This occurs when running with -G or -Mg only.
 * 12/10/2008 8.5.0    Set County.Status='W' when process a county.  This prevents 
 *                     ChkCnty program from running it in parallel.  This has occured 
 *                     to SDX and LAX.
 * 01/03/2009 8.5.4    Do not set file date.  This will be done in ChkCnty.
 * 10/25/2009 9.2.4    Add -Ur option to replace LDR values with current values
 * 10/27/2009 9.3.0    Add option to INI file to force update LastFileDate in County table.
 *                     This value is often set automatically by ChkCnty.  We provide an
 *                     option here in case we need to force them.
 * 12/09/2009 9.3.5    Add option to set Prop8 flag using Prop8 Assessment file.
 * 12/23/2009 9.3.6    Add -X8 (extract prop8 flag to text file) and -D8 (set prop8 flag
 *                     in DB).  Modify -B8 to use lOptProp8 instead of bSetProp8 variable.
 * 02/24/2010 9.5.0    Add default sendmail flag.  This can be overide by command line option.
 * 03/29/2010 9.5.1.1  Fix bug in compare S01-R01 output by using chkS01R01() instead of updCmpS01R01().
 * 04/03/2010 9.5.2    Move MergeSaleRec() to SaleRec.cpp
 * 04/26/2010 9.5.6    Add option -Fo to fix owner names.
 * 05/14/2010 9.5.8    Move extrGrGrDoc() to MergeLax.cpp
 * 07/07/2010 10.1.0   Update LastRecsChg to County table
 * 07/15/2010 10.1.3   Remove -Uc (update char file).  Use -Lc instead.  Add -Xd option
 *                     to extract legal desc (for SBD).
 * 07/26/2010 10.1.5   Add check for records changed limit and email to developer.
 * 10/29/2010 10.2.0   Add -Fs option and bFixSaleAmt to allow user to strip off saleamt
 *                     that is less than 10 dollars.
 * 11/08/2010 10.3.0   Add processing date to log file
 * 11/19/2010 10.3.3   Allow processing TRA fix then exit if there is no other option.
 * 12/17/2010 10.3.7   Add -Y option to overwrite year assessed and output ???_LDRyyyy.R01.
 * 12/21/2010 10.4.0   Add -Fe option to fix Full Exemption flag
 * 01/15/2011 10.4.1.1 Fix LienDate by placing it in MergeInit() instead of LoadCountyInfo().
 * 07/07/2011 11.0.2   If CChrFile is defined in county section of INI file, use it.
 *                     If not, use template in the Data section.  Do not need to check recsize.
 * 09/30/2011 11.4.10  Extract sales for import into spatial db on -Xsi or -Usi after county processing is done.
 * 11/10/2011 11.5.8   Add doSaleImport() & doTaxImport() to do bulk import into SQL.
 *                     -Xsi option now can import directly into SQL if setting AutoImport=Y
 * 12/29/2011 11.6.1   Add -T option for loading tax file.
 * 01/19/2012 11.6.4   Add -D2 to convert GrGr file from GRGR_DEF to SCSAL_REC format.
 *                     Replace bAutoImport with bSaleImport, bTaxImport, and bGrGrImport.
 * 02/15/2012 11.6.6   Require specify county as running parameter.
 * 03/07/2012 11.6.8   Use PQ_ChkBadR01() to fix bad char in output R01 file.
 * 03/12/2012 11.7.10  Modify doSaleImport() to remove *.error.txt to avoid sql error 4863.
 * 03/16/2012 11.8.10  Check for bad character only when load successful.
 * 03/27/2012 11.8.12  Add -Mt option to merge transfer data to cum sale file.
 * 05/12/2012 11.9.5   Add cDelim & iHdrRows to support delimited input file (SCR). Add m_iTimeOut
 *                     to avoid timeout during import sale data.
 * 06/28/2012 12.1.1   Add option -Dc to create UPD file and -Dn to replace -Nu option.
 * 07/26/2012 12.2.3   Move MergeGrGrDoc() to SaleRec.cpp.
 * 10/14/2012 12.3.0   Add option -Gi to import GRGR data to SQL table.
 * 12/14/2012 12.3.8   Add option to load unsecured file (will be moved to LoadUOne.cpp)
 * 01/02/2013 12.4.0   Update DocLink to sale history table.
 * 02/12/2013 12.4.5   Add GrDocLink
 * 03/12/2013 12.5.0   Add option -Fz to update zipcode
 * 04/11/2013 12.5.4   Check for bad char before update county status to avoid build index start too early.
 * 04/28/2013 12.6.1   Add updateDocLinks() to update DocLinks in R01 file.
 * 06/03/2013 12.7.0   Add -Xz option to extract ZONING.
 * 07/23/2013 13.1.5   Adding option to output unknown Usecode to specific log file.
 * 07/29/2013 13.3.8   Create flg file on successful.
 * 08/14/2013 13.5.10  Add Situs.h
 * 08/21/2013 13.6.11  Add lOptExtr for extract options EXTR_MH (for LAX) & EXTR_ZONE
 * 08/26/2013 13.7.13  Add -Dr option to keep temp files, default remove them after successful 
 *                     processing. Remove option -Lu & -B and all code related to unsecured file (ALA, LAX).
 * 10/01/2013 13.9.0   Loading Vesting table
 * 10/02/2013 13.10.0  Replace loadVestingTbls() with loadVesting() and use combined vesting file.
 * 10/11/2013 13.11.0  Remove call to loadImp() since new data is processed by LoadMB.
 * 01/13/2013 13.11.13 Add -Fx option to fix DocType.
 * 02/06/2014 13.11.15 Add acRollSale[]
 * 02/14/2014 13.12.0  Add -Is & -Ig options to allow direct import into SQL without 
 *                     normal loading sequence. Just tested on VEN, need testing on other counties.
 * 03/04/2014 13.12.2  Now import GRGR data to SQL using SCSAL_REC format.
 * 03/19/2014 13.12.3.1 Fix bug creating GRGR import.
 * 10/09/2014 14.7.0   Add -Fy option to fix DocNum. Function to do the fix has 
 *                     to be done in county specific module.
 * 12/02/2014 14.10.0  Modify int updateDocLinks() to overflow extra bytes to DOCLINKX
 * 01/14/2015 14.11.0  Add -Mr option to merge acres (ORG)
 * 01/31/2015 14.12.0  Use See4c 7.0 to support Office365 email. Replace GetPrivateProfileString() with
 *                     GetIniString() which will translate [Machine] to local machine name and [CO_DATA]
 *                     with CO_DATA token defined in INI file. Fix GrGrTmpl problem by using standard template 
 *                     ???_GrGr for cum grgr data. Use ???_GrGr.* for acGrGrTmpl[] and GrGr_Exp.* for acEGrGrTmpl[].
 * 03/13/2015 14.13.0  Move updateDocLinks() to R01.cpp
 * 04/15/2015 14.14.0  Check CreateAPNFile option before calling chkS01R01().
 * 04/30/2015 14.14.2  Add -Xv option to import value file. Functions RunSP() and doValueImport()
 *                     will be moved out to SqlExt.cpp to be shared with other load programs.
 * 08/10/2015 15.1.0   Remove -Xx option to extract old sale from O01 file
 * 08/17/2015 15.1.1   Use LoadCityEx() to load city file name ending with N2CX (as in LAX).
 * 09/02/2015 15.2.0   Add -Dt option to bypass translation from EBCDIC to ASCII.
 * 09/13/2015 15.2.2   Add acAreaFile
 * 10/07/2015 15.3.0   Add fdCSale and make it public.
 * 03/09/2016 15.8.0   Automatically update last roll file date unless specify SetFileDate=N under county section.
 *                     Add lLastTaxFileDate.
 * 07/22/2016 16.1.1   Add Value import function.  Remove redundant functions by using SqlExt.cpp
 * 08/19/2016 16.2.1   Add -Xo option to extract Owner Name (SBD).
 * 09/07/2016 16.2.5   Add lTaxYear to allow tax year being set in INI file.
 * 11/03/2016 16.6.0   Add lOptMisc and remove bFix* variables.
 * 12/09/2016 16.7.7   Add -Ut option to update tax table.
 * 01/20/2017 16.9.7.1 Add acTaxDBProvider
 * 02/28/2016 16.10.6  Fix doValueImport() by using BulkInsert instead of BulkImport to import value file.
 * 03/26/2017 16.13.0  Update tax import date as well as last tax file date.  Always check for new file
 *                     before processing.
 * 05/12/2017 16.14.11 Add -XC option to extract XC file for CHAR client
 * 06/08/2017 16.15.0  Do not remove temp files when running with -T option.
 * 06/18/2017 16.15.2  If Load_???() function return 99, send email to default MailTo list.
 * 06/30/2017 17.1.0   Do not set county status when loading tax alone to avoid messing up production automation.
 * 07/12/2017 17.1.1.1 Set Status=R before exit except when loading tax.
 * 11/21/2017 17.5.0   Trap bug in ParseCmd().
 * 03/23/2018 17.7.0   Fix import sale problem when roll file has no change.
 * 03/31/2018 17.8.0   Add -Lt option to load full tax file (.TC)
 * 05/14/2018 17.10.9  Fix -L option to avoid setting load flag wrongly when loading tax.
 * 05/18/2018 17.10.11 Initialize m_bUseFeePrcl in MergeInit().
 * 06/12/2018 17.12.1  Replace bLoadTax with iLoadTax
 * 07/16/2018 18.2.0   Add UpdateDetail in INI file to ignore update Tax Detail when detail file has duplicate 
 *                     tax items with different amount.
 * 08/09/2018 18.3.0   Add -Mn option to merge NDC sale data
 * 08/24/2018 18.4.0   Move MergeLotArea() to PQ.CPP
 * 09/21/2019 18.5.0   Adding -Tu (same as -Ut) for tax update.  The new tax update method
 *                     will be similar to Update_TC() by loading update data into Tmp_Tax_Base table
 *                     then merge into ???_Tax_Base table.
 * 09/27/2018 18.5.1   Process -Mr option after county processing is done.
 * 01/09/2019 18.8.0   Add INI option FixBadChar=Y/N to allow bypass fixing bad char (SAC).
 * 02/03/2019 18.9.2   Send mail to notify new tax file arrive for MPA.
 * 03/08/2019 18.10.0  Add -M1 option to merge situs extract from NDC.
 * 05/16/2019 18.12.2  Add code to import GRGR_DEF.  In the future, use SCSAL_EXT as standard for sale import.
 * 11/21/2019 19.5.5   Add unTar() for SAC
 * 01/18/2020 19.6.0   Add bDupApn (RIV) and send mail if duplicate parcel found.
 * 02/25/2020 19.7.0   Add -Xn option to extract NDC recorder sale.
 * 03/03/2020 19.8.0   Add -Mz option to merge zoning file (CCX) and remove -Mt option (obsolete)
 * 04/09/2020 19.8.9   Add "PhoneTech" to [Mail] section.
 * 04/27/2020 19.9.0   Remove -Ut and use -T to load both full & partial tax files.
 * 05/02/2020 19.9.4   Turn -Ut back on for SAC
 * 07/05/2020 20.1.1   Remove -Xvi and move import value option to UV<n>.CMD 
 *                     Add doSaleImportEx() to replace doSaleImport() where the new function support both Windows & Linux.
 * 08/03/2020 20.2.6   Remove bCopySales and its option to duplicate sale for SOL.
 * 09/30/2020 20.2.13  Add -Msx to remove existing transfer before update new one.
 * 10/15/2020 20.3.0   Now process -Mz in _tmain() after loading roll file without error.
 * 10/29/2020 20.4.0   Add option to set default PQZoning by setting SetZoning=Y in county section.
 * 11/14/2020 20.5.1   Modify doSaleImportEx() to support Linux.  Add ORG_GRGR import.
 * 02/14/2021 20.7.5   Change INI item from "SqlSalesFile" to "SqlSaleFile" in doSaleImportEx()
 * 02/17/2021 20.7.6   Allow create sale import file without actually import into SQL by setting ImportSale=N in INI file.
 * 05/20/2021 20.8.0   Set County.State='W' when merge sale or GrGr alone to avoid other program from working on it.
 * 06/05/2021 20.9.1   Add MergeIny.cpp.  Modify MergeInit() to check for LOAD_DAILY files and to load lookup for QUALITY.
 * 08/11/2021 21.1.2   Remove update DocLink when import sale history or GrGr.
 * 08/26/2021 21.2.0   Add bUseConfSalePrice to control usage of confirm sale price.
 * 09/07/2021 21.2.2   Add "RemTmpFile" to [Debug] section to set default option to remove temp files.
 * 11/06/2021 21.3.0   Add -Ml to merge legal file (county specific).
 * 12/16/2021 21.4.6   Add -Uc option to process Roll Correction.
 * 02/25/2022 21.5.0   Add option -Xf & -Mf to extract/merge final value to V0x file.
 *                     Add global acValueTmpl[] for output template of V0x files.
 *                     Remove INY, MNO, NEV, SBT, YOL.  These counties are now in LoadMB.cpp
 * 03/12/2022 21.7.1   Compare S01 vs R01 when -Mz is used.
 * 04/25/2022 21.9.0   Allow setting county specific backup folder for GrGr files. If county not defined, use default.
 * 10/31/2022 22.3.0   Add -Mf option to merge final value to R01 (output V01 file).
 * 03/02/2023 22.5.0   Skip compare S01 vs R01 when not updating roll data.
 * 03/23/2023 22.6.0   Add -Ext option to export SQL command for tax update.
 *                     Add sTaxSqlTmpl, bExpSql.  Modify MergeInit() to load sTaxSqlTmpl
 * 04/19/2023 22.7.0   Modify -Ext option to create sql command file for blob storage when BRoot is defined.
 * 05/19/2023 22.7.1   Use MaxUpdate in county table for iMaxChgAllowed.
 * 07/10/2023 23.0.1   Add cLdrSep for common delimiter in LDR file.
 * 12/29/2023 23.5.0   Forcing compare full 14 bytes of APN when calling chkS01R01() to fix variable length APN issue.
 * 01/25/2024 23.5.7   Remove EDX, KIN, MEN, MPA, TUL from LoadOne.
 * 06/01/2024 23.9.0   If there is error accessing city file, stop program immediately.
 * 08/10/2024 24.1.0   Modify _tmain() to allow export sql command for TAX_UPDATING.
 *                     Load Jurisdiction table when -Mz is used.
 * 10/09/2024 24.1.6   Add -Lu option to load all update files (VEN).
 *
 **************************************************************************/

#include "stdafx.h"
#include "Prodlib.h"
#include "Logs.h"
#include "getopt.h"
#include "CountyInfo.h"
#include "R01.h"
#include "RecDef.h"
#include "Tables.h"
#include "SaleRec.h"
#include "FormatApn.h"
#include "hlAdo.h"
#include "Utils.h"
#include "doOwner.h"
#include "XlatTbls.h" 
#include "LoadOne.h"
#include "Usecode.h"
#include "Update.h"
#include "PQ.h"
#include "SendMail.h"
#include "Tax.h"
#include "Situs.h"
#include "SqlExt.h"
#include "Tarlib.h"
#include "filesyshelpers.h"
#include "MergeZoning.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

CWinApp theApp;
using namespace std;
using namespace tarlib;
using namespace tarlib::utils;

char  *apTokens[MAX_FLD_TOKEN];
int   iTokens;

char  acRawTmpl[_MAX_PATH], acGrBkTmpl[_MAX_PATH], acSaleTmpl[_MAX_PATH], acESalTmpl[_MAX_PATH], acGrGrTmpl[_MAX_PATH],
      acLienTmpl[_MAX_PATH], acAsrTmpl[_MAX_PATH], acAddrTmpl[_MAX_PATH], acAttrTmpl[_MAX_PATH], acProp8Tmpl[_MAX_PATH],
      acMdbProvider[_MAX_PATH], acTaxDBProvider[_MAX_PATH];
char  acRollFile[_MAX_PATH], acLienFile[_MAX_PATH], acCSalFile[_MAX_PATH], acSaleFile[_MAX_PATH], //acAreaFile[_MAX_PATH], 
      acBPFile[_MAX_PATH], acPubParcelFile[_MAX_PATH], acCharFile[_MAX_PATH], acCChrFile[_MAX_PATH], acValueFile[_MAX_PATH], 
      acExeFile[_MAX_PATH], acToday[16], acTmpPath[_MAX_PATH], acCumSalFile[_MAX_PATH], acLogFile[_MAX_PATH],
      acIniFile[_MAX_PATH], acCntyTbl[_MAX_PATH], acUseTbl[_MAX_PATH], acLookupTbl[_MAX_PATH], acVacFile[_MAX_PATH];
char  sTCTmpl[_MAX_PATH], sTCMTmpl[_MAX_PATH], sTaxTmpl[_MAX_PATH], sRedTmpl[_MAX_PATH], acRollSale[_MAX_PATH],
      acGrGrTbl[64],acEGrGrTmpl[_MAX_PATH], acXferTmpl[_MAX_PATH], acDocPath[_MAX_PATH], acValueTmpl[_MAX_PATH],
      sTRITmpl[_MAX_PATH], sTFTmpl[_MAX_PATH], sTaxOutTmpl[_MAX_PATH], sTaxSqlTmpl[_MAX_PATH], sImportLogTmpl[_MAX_PATH];

unsigned char cDelim, cLdrSep;

int   iRecLen, iRollLen, iSaleLen, iCSalLen, iCharLen, iCChrLen, iGrGrApnLen, iAsrRecLen, iApnLen, iLoadTax, 
      iPubParcelLen, iSkip, iApnFld, iNoMatch, iLoadFlag, iMaxLegal, iMaxChgAllowed, iHdrRows;
bool  bEnCode, bUseSfxXlat, bOverwriteLogfile, bDebug, bClean, bUseGrGr, bCreateUpd, 
      bMergeOthers, bDontUpd, bSendMail, bClearSales, bReplValue, bSetLastFileDate, 
      bConvGrGr, bConvSale, bGrGrAvail, bSaleImport, bTaxImport, bGrgrImport, bMixSale,
      bRemTmpFile, bEbc2Asc, bTaxUpdtDetail, bDupApn, bUseConfSalePrice, bExpSql;

long  lRecCnt, lAssrRecCnt, lLastRecDate, lLastFileDate, lLastGrGrDate, lLastTaxFileDate, lToday, lToyear, alRegCnt[4], 
      lTaxYear, lLienDate, lLienYear, lProcDate, lOptProp8, lOptExtr, lOptMisc, lLDRRecCount, lDupOwners;

REC_CNT  asRecCnt[MAX_REC_CNT];

XREFTBL  asDeed[MAX_DEED_ENTRIES];
int      iNumDeeds;
XREFTBL  asInst[MAX_INST_ENTRIES];
int      iNumInst;
XREFTBL  asTRA[MAX_TRA_ENTRIES];
int      iNumTRA;

COUNTY_INFO myCounty;

FILE     *fdRoll, *fdGrGr, *fdSale, *fdCSale;

hlAdo    hl, dbLoadConn;
hlAdoRs  m_AdoRs;
bool     m_bConnected;

IDX_TBL  asSaleTypes[] =
{
   "FV", "F",               // Full value
   "WL", "I",               // With other properties less lien
   "LL", "N",               // Less lien
   "WP", "W",               // With other properties
   "",   ""
};

int   CopySales(int iSkip);
int   loadAla(int);
int   loadCcx(int);
//int   loadEdx(int);
int   loadFre(int);
int   loadKer(int);
//int   loadKin(int);
int   loadLax(int);
//int   loadMen(int);
//int   loadMpa(int);
int   loadMrn(int);
int   loadOrg(int);
int   loadRiv(int);
int   loadSac(int);
int   loadSbd(int);
int   loadSbx(int);
int   loadScl(int);
int   loadScr(int);
int   loadSdx(int);
int   loadSfx(int);
int   loadSlo(int);
int   loadSmx(int);
int   loadSol(int);
//int   loadSut(int);
//int   loadTul(int);
int   loadVen(int);

void  Fre_MakeDocLink(LPSTR pDocLink, LPSTR pDoc, LPSTR pDate);
void  Ker_MakeDocLink(LPSTR pDocLink, LPSTR pDoc, LPSTR pDate);

/********************************* getFolder ********************************
 *
 * extract folder from a file name
 *
 ****************************************************************************/

std::string getFolder(std::string const &filename)
{
   std::string pResult;

   pResult = extract_foldername(filename);
   return pResult;
}

/*********************************** unTar **********************************
 *
 * Untar to input tar folder
 *
 ****************************************************************************/

void unTar(std::string const &filename, std::string const &destination)
{
	// create tar file with path and read mode
	tarFile tarf(filename, tarModeRead);

   // extract to folder
	tarf.extract(destination);
}

void unTar(std::string const &filename)
{
	// create tar file with path and read mode
	tarFile tarf(filename, tarModeRead);

   std::string pDst;
   pDst = getFolder(filename);

   // extract to folder
	tarf.extract(pDst);
}

/******************************** doValueImport *****************************
 *
 * This function will call store procedure to do bulk insert
 *
 * Return 0 if success
 *
 ****************************************************************************/

int doValueImport(char *pCnty, char *pInFile)
{
   char sErrFile[128], sTblName[32], sDBName[32], sServer[32];
   char sFmtFile[128], sProviderTmpl[255], acTmp[1024];
   int  iRet=1;

   iRet = GetIniString("Database", "DBProvider", "", sProviderTmpl, 255, acIniFile);
   if (!iRet)
   {
      LogMsg("***** Missing DBProvider in INI file.  Please review %s", acIniFile);
      return -1;
   }

   LogMsg("Import Value data to Pcl_Values for %s", pCnty);
   sprintf(sErrFile, sImportLogTmpl, "Value", pCnty, "Value");
   strcpy(sTblName, "Pcl_Values");

   // Delete old log files
   if (!_access(sErrFile, 0))
      DeleteFile(sErrFile);
   sprintf(acTmp, "%s.Error.txt", sErrFile);
   if (!_access(acTmp, 0))
      DeleteFile(acTmp);

   if (_access(pInFile, 0))
   {
      LogMsg("***** Missing import file: %s", pInFile);
      return -1;
   }

   GetIniString("Data", "ValueImportTmpl", "", sFmtFile, _MAX_PATH, acIniFile);
   if (_access(sFmtFile, 0))
   {
      LogMsg("***** Missing import format file: %s", sFmtFile);
      return -1;
   }

   // Prepare to import - Create/truncate table
   GetIniString("Database", "ValueDB", "", sDBName, _MAX_PATH, acIniFile);
   iRet = GetIniString("Database", "DBSvr1", "", sServer, _MAX_PATH, acIniFile);
   if (iRet > 1 && sDBName[0] > ' ')
   {
      sprintf(acTmp, "EXEC [dbo].[spValuePrep] '%s', %d ", pCnty, lLienYear);
      iRet = runSP(acTmp, sDBName, sServer, sProviderTmpl);

      // Calling store procedure
      sprintf(acTmp, "EXEC [dbo].[spBulkInsert] '%s', '%s', '%s', '%s', 1", sTblName, pInFile, sFmtFile, sErrFile);
      iRet = runSP(acTmp, sDBName, sServer, sProviderTmpl);    
   }

   iRet = GetIniString("Database", "DBSvr2", "", sServer, _MAX_PATH, acIniFile);
   if (iRet > 1 && sDBName[0] > ' ')
   {
      sprintf(acTmp, "EXEC [dbo].[spValuePrep] '%s', %d ", pCnty, lLienYear);
      iRet = runSP(acTmp, sDBName, sServer, sProviderTmpl);

      // Calling store procedure
      sprintf(acTmp, "EXEC [dbo].[spBulkInsert] '%s', '%s', '%s', '%s', 1", sTblName, pInFile, sFmtFile, sErrFile);
      iRet = runSP(acTmp, sDBName, sServer, sProviderTmpl);    
   }

   iRet = GetIniString("Database", "DBSvr3", "", sServer, _MAX_PATH, acIniFile);
   if (iRet > 1 && sDBName[0] > ' ')
   {
      sprintf(acTmp, "EXEC [dbo].[spValuePrep] '%s', %d ", pCnty, lLienYear);
      iRet = runSP(acTmp, sDBName, sServer, sProviderTmpl);

      // Calling store procedure
      sprintf(acTmp, "EXEC [dbo].[spBulkInsert] '%s', '%s', '%s', '%s', 1", sTblName, pInFile, sFmtFile, sErrFile);
      iRet = runSP(acTmp, sDBName, sServer, sProviderTmpl);    
   }

   return iRet;
}

/******************************** updateSaleDB ******************************
 *
 * This function will call store procedure to do bulk insert
 *
 * Return TRUE if success
 *
 ****************************************************************************/

int updateSaleDB(char *strCmd)
{                 
   static bool  bConnected=false;
   static hlAdo hDB;

   char acServer[256];
   int  iRet;

   iRet = GetIniString("Database", "SaleProvider", "", acServer, 128, acIniFile);
   if (!iRet)
   {
      LogMsg("***** Missing SaleProvider in INI file.  Please review %s", acIniFile);
      return -1;
   }

   LogMsg("Execute command on: %s", acServer);
   LogMsg(strCmd);
   try
   {
      // open the database connection
      if (!bConnected)
         bConnected = hDB.Connect(acServer);
   }

   // catch any ADO errors
   AdoCatch(e)
   {
      LogMsg("%s", ComError(e));
      return -2;
   }

   // Update county profile table
   iRet = execSqlCmd(strCmd, &hDB);
   return iRet;
}

/******************************** doSaleImport ******************************
 *
 * This function will call store procedure to do bulk insert
 *
 * iType values:
 *    1: Sale data
 *    2: GrGr data
 *    3: 
 *
 ****************************************************************************/

int doSaleImportEx(char *pCnty, char *pDbName, int iType)
{
   char sErrFile[128], sTblName[32], sImportTmpl[_MAX_PATH];
   char sFmtFile[128], sSrcFile[128], sSqlFileTmpl[128], acTmp[1024], sServerType[32];
   int  iRet=1;

   GetIniString("Database", "SaleServerType", "W", sServerType, _MAX_PATH, acIniFile);
   GetIniString("Data", "SaleImportTmpl", "", sImportTmpl, _MAX_PATH, acIniFile);
   GetIniString("System", "ImportLog", "", sImportLogTmpl, _MAX_PATH, acIniFile);
   if (iType == 1)
      GetIniString("Data", "SqlSaleFile", "", sSqlFileTmpl, _MAX_PATH, acIniFile);
   else
      GetIniString("Data", "SqlGrGrFile", "", sSqlFileTmpl, _MAX_PATH, acIniFile);

   switch (iType)
   {
      case 1:
         LogMsg("Import Sale data to: %s using %s", pCnty, pDbName);
         sprintf(sFmtFile, sImportTmpl, "Sales");
         sprintf(sErrFile, sImportLogTmpl, "Sale", pCnty, "Sale");
         sprintf(sTblName, "%s_Sales", pCnty);

         // Delete old log files
         if (!_access(sErrFile, 0))
            DeleteFile(sErrFile);
         sprintf(acTmp, "%s.Error.txt", sErrFile);
         if (!_access(acTmp, 0))
            DeleteFile(acTmp);

         sprintf(sSrcFile, sSqlFileTmpl, pDbName, pCnty);
         if (_access(sSrcFile, 0))
         {
            LogMsg("***** Missing import file: %s", sSrcFile);
            return -1;
         }
         if (_access(sFmtFile, 0))
         {
            LogMsg("***** Missing import format file: %s", sFmtFile);
            return -1;
         }

         // Prepare to import - Create/truncate table
         sprintf(acTmp, "EXEC [dbo].[spSalePrep] '%s'", pCnty);
         iRet = updateSaleDB(acTmp);

         if (sServerType[0] == 'L')
         {
            GetIniString("Data", "LSaleImportTmpl", "", sImportTmpl, _MAX_PATH, acIniFile);
            GetIniString("Data", "LImportLog", "", sImportLogTmpl, _MAX_PATH, acIniFile);
            GetIniString("Data", "LSqlSaleFile", "", sSqlFileTmpl, _MAX_PATH, acIniFile);
            sprintf(sFmtFile, sImportTmpl, "Sales");
            sprintf(sErrFile, sImportLogTmpl, "Sale", pCnty, "Sale");
            sprintf(sSrcFile, sSqlFileTmpl, pDbName, pCnty);
         }

         // Calling store procedure
         sprintf(acTmp, "EXEC [dbo].[spBulkImport] '%s', '%s', '%s', '%s', 1", sTblName, sSrcFile, sFmtFile, sErrFile);
         iRet = updateSaleDB(acTmp);

         // Update DocLink
         /* 08/11/2021 temporary remove 
         GetIniString("Database", "HSDocLink", "", acTmp, 128, acIniFile);
         sprintf(sFmtFile, acTmp, pCnty);
         if (_access(sFmtFile, 0))
         {
            LogMsg("*** Missing sql script file: %s to update DocLink.  Skip this task.", sFmtFile);
         } else
         {
            if (sServerType[0] == 'L')
            {
               GetIniString("Database", "LHSDocLink", "", acTmp, 128, acIniFile);
               sprintf(sFmtFile, acTmp, pCnty);
            }
            sprintf(acTmp, "EXEC sp_executesqlfromfile '%s', NULL, 0", sFmtFile);
            iRet = updateSaleDB(acTmp);    
         }
         */
         break;

      case 2:
         LogMsg("Import Grgr data to: %s using %s", pCnty, pDbName);
         sprintf(sFmtFile, sImportTmpl, "GrGr");
         sprintf(sErrFile, sImportLogTmpl, "Sale", pCnty, "GrGr");
         sprintf(sTblName, "%s_GrGr", pCnty);

         if (!_access(sErrFile, 0))
            DeleteFile(sErrFile);
         sprintf(acTmp, "%s.Error.txt", sErrFile);
         if (!_access(acTmp, 0))
            DeleteFile(acTmp);

         sprintf(sSrcFile, sSqlFileTmpl, pDbName, pCnty);
         if (_access(sSrcFile, 0))
         {
            LogMsg("***** Missing import file: %s", sSrcFile);
            return -1;
         }
         if (_access(sFmtFile, 0))
         {
            LogMsg("***** Missing import format file: %s", sFmtFile);
            return -1;
         }

         // Prepare to import - Create/truncate table
         sprintf(acTmp, "EXEC [dbo].[spGrGrPrep] '%s'", pCnty);
         iRet = updateSaleDB(acTmp);

         // Reformat file name for import into Linux server
         if (sServerType[0] == 'L')
         {
            GetIniString("Data", "LSaleImportTmpl", "", sImportTmpl, _MAX_PATH, acIniFile);
            GetIniString("Data", "LImportLog", "", sImportLogTmpl, _MAX_PATH, acIniFile);
            GetIniString("Data", "LSqlGrGrFile", "", sSqlFileTmpl, _MAX_PATH, acIniFile);
            sprintf(sFmtFile, sImportTmpl, "GrGr");
            sprintf(sErrFile, sImportLogTmpl, "Sale", pCnty, "GrGr");
            sprintf(sSrcFile, sSqlFileTmpl, pDbName, pCnty);
         }

         // Calling store procedure
         sprintf(acTmp, "EXEC [dbo].[spBulkImport] '%s', '%s', '%s', '%s', 1", sTblName, sSrcFile, sFmtFile, sErrFile);
         iRet = updateSaleDB(acTmp);

         // Update DocLink
         /* 08/11/2021 temporary remove 
         GetIniString("Database", "GrDocLink", "", acTmp, 128, acIniFile);
         sprintf(sFmtFile, acTmp, pCnty);
         if (_access(sFmtFile, 0))
         {
            LogMsg("*** Missing sql script file: %s to update DocLink.  Skip this task.", sFmtFile);
         } else
         {
            if (sServerType[0] == 'L')
            {
               GetIniString("Database", "LGrDocLink", "", acTmp, 128, acIniFile);
               sprintf(sFmtFile, acTmp, pCnty);
            }
            sprintf(acTmp, "EXEC sp_executesqlfromfile '%s', NULL, 0", sFmtFile);
            iRet = updateSaleDB(acTmp);    
         }
         */
         break;

      default:
         break;
   }

   return iRet;
}

int doSaleImport(char *pCnty, char *pDbName, char *pInFile, int iType)
{
   char sErrFile[128], sTblName[32], sImportTmpl[_MAX_PATH];
   char sFmtFile[128], acTmp[1024];
   int  iRet=1;

   GetIniString("Data", "SaleImportTmpl", "", sImportTmpl, _MAX_PATH, acIniFile);

   switch (iType)
   {
      case 1:
         LogMsg("Import Sale data to: %s using %s", pCnty, pDbName);
         sprintf(sFmtFile, sImportTmpl, "Sales");
         sprintf(sErrFile, sImportLogTmpl, "Sale", pCnty, "Sale");
         sprintf(sTblName, "%s_Sales", pCnty);

         // Delete old log files
         if (!_access(sErrFile, 0))
            DeleteFile(sErrFile);
         sprintf(acTmp, "%s.Error.txt", sErrFile);
         if (!_access(acTmp, 0))
            DeleteFile(acTmp);

         if (_access(pInFile, 0))
         {
            LogMsg("***** Missing import file: %s", pInFile);
            return -1;
         }
         if (_access(sFmtFile, 0))
         {
            LogMsg("***** Missing import format file: %s", sFmtFile);
            return -1;
         }

         // Prepare to import - Create/truncate table
         sprintf(acTmp, "EXEC [dbo].[spSalePrep] '%s'", pCnty);
         iRet = updateSaleDB(acTmp);

         sprintf(sFmtFile, sImportTmpl, "Sales");
         sprintf(sErrFile, sImportLogTmpl, "Sale", pCnty, "Sale");

         // Calling store procedure
         sprintf(acTmp, "EXEC [dbo].[spBulkImport] '%s', '%s', '%s', '%s', 1", sTblName, pInFile, sFmtFile, sErrFile);
         iRet = updateSaleDB(acTmp);

         // Update DocLink
         GetIniString("Database", "HSDocLink", "", acTmp, 128, acIniFile);
         sprintf(sFmtFile, acTmp, pCnty);
         if (_access(sFmtFile, 0))
         {
            LogMsg("***** Missing sql script file: %s to update DocLink", sFmtFile);
         } else
         {
            sprintf(acTmp, "EXEC sp_executesqlfromfile '%s', NULL, 0", sFmtFile);
            iRet = updateSaleDB(acTmp);    
         }

         break;

      case 2:
         LogMsg("Import Grgr data to: %s using %s", pCnty, pDbName);
         sprintf(sFmtFile, sImportTmpl, "GrGr");
         sprintf(sErrFile, sImportLogTmpl, "Sale", pCnty, "GrGr");
         sprintf(sTblName, "%s_GrGr", pCnty);

         if (!_access(sErrFile, 0))
            DeleteFile(sErrFile);
         sprintf(acTmp, "%s.Error.txt", sErrFile);
         if (!_access(acTmp, 0))
            DeleteFile(acTmp);

         if (_access(pInFile, 0))
         {
            LogMsg("***** Missing import file: %s", pInFile);
            return -1;
         }
         if (_access(sFmtFile, 0))
         {
            LogMsg("***** Missing import format file: %s", sFmtFile);
            return -1;
         }

         // Prepare to import - Create/truncate table
         sprintf(acTmp, "EXEC [dbo].[spGrGrPrep] '%s'", pCnty);
         iRet = updateSaleDB(acTmp);

         // Calling store procedure
         sprintf(acTmp, "EXEC [dbo].[spBulkImport] '%s', '%s', '%s', '%s', 1", sTblName, pInFile, sFmtFile, sErrFile);
         iRet = updateSaleDB(acTmp);

         // Update DocLink
         GetIniString("Database", "GrDocLink", "", acTmp, 128, acIniFile);
         sprintf(sFmtFile, acTmp, pCnty);
         if (_access(sFmtFile, 0))
         {
            LogMsg("***** Missing sql script file: %s to update DocLink", sFmtFile);
         } else
         {
            sprintf(acTmp, "EXEC sp_executesqlfromfile '%s', NULL, 0", sFmtFile);
            iRet = updateSaleDB(acTmp);    
         }
         break;

      default:
         break;
   }

   return iRet;
}

/******************************** getCountyInfo *****************************
 *
 * Retrieve info from SQL server
 *
 ****************************************************************************/

int getCountyInfo()
{
   char  acTmp[256], acServer[256];
   int   iRet;

   GetIniString("Database", "Provider", "", acServer, 128, acIniFile);
   if (acServer[0] == '\0')
      return -1;

   try
   {
      // open the database connection
      if (!m_bConnected)
         m_bConnected = hl.Connect(acServer);

      sprintf(acTmp, "SELECT * FROM County WHERE CountyCode='%s'", myCounty.acCntyCode);
      m_AdoRs.Open(hl, acTmp);

      if (m_AdoRs.next())
      {
         CString	sTmp = m_AdoRs.GetItem("LastRecCount");
         myCounty.iLastRecCnt = atoi(sTmp);
         sTmp = m_AdoRs.GetItem("GrGr_Delay");
         myCounty.iGrGrDelay = atoi(sTmp);
         sTmp = m_AdoRs.GetItem("GrGr_Freq");
         myCounty.GrGrFreq = sTmp.GetAt(0);
         sTmp = m_AdoRs.GetItem("Roll_Delay");
         myCounty.iRollDelay = atoi(sTmp);
         sTmp = m_AdoRs.GetItem("Roll_Freq");
         myCounty.RollFreq = sTmp.GetAt(0);

         sTmp = m_AdoRs.GetItem("MaxUpdate");
         myCounty.iMaxUpdate = atoi(sTmp);
         m_AdoRs.Close();
      }
      iRet = 0;
   } AdoCatch(e)
   {
      LogMsg("***** Error exec cmd: %s", acTmp);
      LogMsgD("%s\n", ComError(e));
      iRet = -1;
   }

   return iRet;
}

/***************************** updateProcessFlags ***************************
 *
 * Update LastBldDate, LastRecDate, RecCount, State
 *
 ****************************************************************************/

bool updateTable(LPCTSTR strCmd)
{
   bool bRet = false;
   char acServer[256];

   GetIniString("Database", "Provider", "", acServer, 128, acIniFile);
   if (acServer[0] == '\0')
      return bRet;

   LogMsg((LPSTR)strCmd);
   try
   {
      // open the database connection
      if (!m_bConnected)
         m_bConnected = hl.Connect(acServer);
   }

   // catch any ADO errors
   AdoCatch(e)
   {
      LogMsg("%s", ComError(e));
   }

   // Update county profile table
   int iRet;
   iRet = execSqlCmd(strCmd);
   if (!iRet)
      bRet = true;
   return bRet;
}

/******************************** LoadCountyInfo ******************************
 *
 * Initialize global variables
 *
 *****************************************************************************/

int LoadCountyInfo(char *pCntyCode, char *pCntyTbl)
{
   char  acTmp[_MAX_PATH], *pTmp, *pFlds[MAX_CNTY_FLDS];
   int   iRet=0, iTmp;
   FILE  *fd;

   fd = fopen(pCntyTbl, "r");
   if (fd)
   {
      // Skip blank line
      pTmp = fgets(acTmp, _MAX_PATH, fd);

      while (!feof(fd))
      {
         pTmp = fgets(acTmp, _MAX_PATH, fd);
         if (pTmp)
         {
            if (!memcmp(acTmp, pCntyCode, 3))
            {
               iTmp = ParseString(myTrim(acTmp), ',', MAX_CNTY_FLDS, pFlds);
               if (iTmp > 0)
               {
                  strcpy(myCounty.acCntyCode, pCntyCode);
                  strcpy(myCounty.acStdApnFmt, pFlds[FLD_APN_FMT]);
                  strcpy(myCounty.acSpcApnFmt[0], pFlds[FLD_SPC_FMT]);
                  strcpy(myCounty.acCase[0], pFlds[FLD_SPC_BOOK]);
                  strcpy(myCounty.acCntyID, pFlds[FLD_CNTY_ID]);
                  strcpy(myCounty.acCntyName, pFlds[FLD_CNTY_NAME]);
                  if (iTmp > FLD_YR_ASSD+2)
                  {
                     strcpy(myCounty.acSpcApnFmt[1], pFlds[FLD_SPC_FMT+2]);
                     strcpy(myCounty.acCase[1], pFlds[FLD_SPC_BOOK+2]);
                     strcpy(myCounty.acYearAssd, pFlds[FLD_YR_ASSD+2]);
                  } else
                     strcpy(myCounty.acYearAssd, pFlds[FLD_YR_ASSD]);

                  iRet = atoi(myCounty.acCntyID);
                  myCounty.iCntyID = iRet;
                  sprintf(myCounty.acFipsCode, "06%.3d", iRet*2-1);
                  myCounty.iApnLen = atoi(pFlds[FLD_APN_LEN]);
                  myCounty.iBookLen = atoi(pFlds[FLD_BOOK_LEN]);
                  myCounty.iPageLen = atoi(pFlds[FLD_PAGE_LEN]);
                  myCounty.iCmpLen = atoi(pFlds[FLD_CMP_LEN]);
                  iRet = 1;
               } else
               {
                  LogMsg("Bad county table file: %s", pCntyTbl);
               }

               break;
            }
         } else
            break;
      }

      fclose(fd);

      if (1 != iRet)
         LogMsg("County not found.  Please verify %s", pCntyTbl);
   } else
      LogMsg("***** Error opening county table %s", pCntyTbl);

   return iRet;
}

/********************************** MergeInit ********************************
 *
 * Initialize global variables
 *
 *****************************************************************************/

int MergeInit(char *pCnty)
{
   char  acTmp[_MAX_PATH], acTmp1[_MAX_PATH], sProcDate[32];
   char  acCityFile[_MAX_PATH];

   int   iRet;

   // Get db provider
   GetIniString("Database", "MdbProvider", "", acMdbProvider, 128, acIniFile);
   m_iTimeOut = GetPrivateProfileInt("Database", "TimeOut", 0, acIniFile);
   GetIniString("Database", "TaxProvider", "", acTmp, 128, acIniFile);
   iRet = GetIniString("Database", "TaxSvr1", "", acTmp1, 128, acIniFile);
   if (iRet > 1)
      sprintf(acTaxDBProvider, acTmp, acTmp1);
   else
      strcpy(acTaxDBProvider, acTmp);
   
   // GrGr Sql table template
   GetIniString("Database", "GrGrTbl", "", acTmp, _MAX_PATH, acIniFile);
   sprintf(acGrGrTbl, acTmp, pCnty);


   // Get BParcels file name
   GetIniString(pCnty, "BParcelsFile", "", acBPFile, _MAX_PATH, acIniFile);
   // Get raw file name
   GetIniString(pCnty, "RawFile", "", acRawTmpl, _MAX_PATH, acIniFile);
   if (acRawTmpl[0] <= ' ')
      GetIniString("Data", "RawFile", "", acRawTmpl, _MAX_PATH, acIniFile);
   iRecLen = GetPrivateProfileInt("Data", "RecSize", 1900, acIniFile);

   GetIniString(pCnty, "AsrFile", "", acAsrTmpl, _MAX_PATH, acIniFile);
   if (acAsrTmpl[0] <= ' ')
      GetIniString("Data", "AsrFile", "", acAsrTmpl, _MAX_PATH, acIniFile);
   iAsrRecLen = GetPrivateProfileInt(pCnty, "AsrRecSize", 1500, acIniFile);

   // Get roll file name
   bSetLastFileDate = true;
   if ((iLoadFlag & (LOAD_LIEN|EXTR_LIEN)) || (lOptProp8 == MYOPT_EXT))
   {
      GetIniString(pCnty, "LienFile", "", acRollFile, _MAX_PATH, acIniFile);
      strcpy(acLienFile, acRollFile);
      iRollLen = GetPrivateProfileInt(pCnty, "LienRecSize", 0, acIniFile);
      if (!iRollLen)
         iRollLen = GetPrivateProfileInt(pCnty, "RollRecSize", 0, acIniFile);
   } else
   {
      GetIniString(pCnty, "RollFile", "", acRollFile, _MAX_PATH, acIniFile);
      iRollLen = GetPrivateProfileInt(pCnty, "RollRecSize", 0, acIniFile);

      if (iLoadFlag & LOAD_ASSR)
         GetIniString(pCnty, "LienFile", "", acLienFile, _MAX_PATH, acIniFile);
      if (iLoadFlag & LOAD_UPDT)
      {
         GetPrivateProfileString(pCnty, "SetFileDate", "Y", acTmp, _MAX_PATH, acIniFile);
         if (acTmp[0] == 'N')
            bSetLastFileDate = false;
      }
   }

   // Char file
   GetIniString(pCnty, "CharFile", "", acCharFile, _MAX_PATH, acIniFile);
   iCharLen = GetPrivateProfileInt(pCnty, "CharRecSize", 0, acIniFile);

   // Sale file
   GetIniString(pCnty, "SaleFile", "", acSaleFile, _MAX_PATH, acIniFile);
   iSaleLen = GetPrivateProfileInt(pCnty, "SaleRecSize", 0, acIniFile);

   if (iLoadFlag & LOAD_DAILY)                     // -Ld
   {
      // Prepare input file
      sprintf(acTmp, "%d", lProcDate);
      sprintf(sProcDate, "%.4s-%.2s-%.2s", acTmp, &acTmp[4], &acTmp[6]);
      replStr(acRollFile, "[Date]", sProcDate);
      replStr(acCharFile, "[Date]", sProcDate);
      replStr(acSaleFile, "[Date]", sProcDate);
   }
   lLastFileDate = getFileDate(acRollFile);

   if (!iRollLen)
      iRollLen = MAX_RECSIZE;

   // Get APN field
   GetIniString(pCnty, "ApnFld", "", acTmp, _MAX_PATH, acIniFile);
   if (strlen(acTmp) > 0)
      iApnFld = atoi(acTmp);
   else
      iApnFld = -1;

   // Public Parcel file
   GetIniString(pCnty, "PubParcelFile", "", acPubParcelFile, _MAX_PATH, acIniFile);
   iPubParcelLen = GetPrivateProfileInt(pCnty, "PubParcelSize", 0, acIniFile);

   // Cumulative Char file
   GetIniString(pCnty, "CChrFile", "", acCChrFile, _MAX_PATH, acIniFile);
   iCChrLen = GetPrivateProfileInt(pCnty, "CChrRecSize", 0, acIniFile);
   if (!acCChrFile[0])
   {
      GetIniString("Data", "CChrFile", "", acTmp, _MAX_PATH, acIniFile);
      sprintf(acCChrFile, acTmp, pCnty, pCnty);
   }

   // Sale Rec export template
   GetIniString(pCnty, "SaleExp", "", acESalTmpl, _MAX_PATH, acIniFile);
   if (!acESalTmpl[0])
      GetIniString("Data", "SaleExp", "", acESalTmpl, _MAX_PATH, acIniFile);

   // Cumulative Sale file
   // It's unexplainable why first byte of acCSalFile is always lost when view but displayed ok.
   // This might be a bug in VC 6.0.  Changing size and location makes no difference
   GetIniString(pCnty, "CSalFile", "", acCSalFile, _MAX_PATH, acIniFile);
   iCSalLen = GetPrivateProfileInt(pCnty, "CSalRecSize", 0, acIniFile);
   if (!acCSalFile[0])
      sprintf(acCSalFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "sls");

   GetIniString(pCnty, "RollSale", "", acRollSale, _MAX_PATH, acIniFile);
   if (!acRollSale[0])
      sprintf(acRollSale, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "rol");

   // Get Sale output file template (Sale_exp.dat)
   GetIniString(pCnty, "SaleOut", "", acSaleTmpl, _MAX_PATH, acIniFile);
   if (acSaleTmpl[0] <= ' ')
      GetIniString("Data", "SaleOut", "", acSaleTmpl, _MAX_PATH, acIniFile);

   // Get GrGr output file template
   GetIniString(pCnty, "GrGrOut", "", acGrGrTmpl, _MAX_PATH, acIniFile);
   if (acGrGrTmpl[0] <= ' ')
      GetIniString("Data", "GrGrOut", "", acGrGrTmpl, _MAX_PATH, acIniFile);
   iGrGrApnLen = GetPrivateProfileInt(pCnty, "GrGrApnLen", 0, acIniFile);

   // Get GrGr extract file template
   GetIniString(pCnty, "GrGrExp", "", acEGrGrTmpl, _MAX_PATH, acIniFile);
   if (acEGrGrTmpl[0] <= ' ')
      GetIniString("Data", "GrGrExp", "", acEGrGrTmpl, _MAX_PATH, acIniFile);

   // Get GrGr backup template
   iRet = GetIniString(pCnty, "GrGrBak", "", acGrBkTmpl, _MAX_PATH, acIniFile);
   if (!iRet)
      GetIniString("Data", "GrGrBak", "", acGrBkTmpl, _MAX_PATH, acIniFile);

   // Get Attr file template
   GetIniString("Data", "AttrOut", "", acAttrTmpl, _MAX_PATH, acIniFile);

   // Lien template name
   GetIniString("Data", "LienOut", "", acLienTmpl, _MAX_PATH, acIniFile);

   // Value export template
   GetIniString("Data", "ValueOut", "", acValueTmpl, _MAX_PATH, acIniFile);

   // Situs extract template name
   GetIniString("Data", "AddrOut", "", acAddrTmpl, _MAX_PATH, acIniFile);

   // Get TmpPath
   GetIniString("System", "TmpPath", "", acTmpPath, _MAX_PATH, acIniFile);
   sprintf(acTmp, "%s\\%s", acTmpPath, myCounty.acCntyCode);
   if (_access(acTmp, 0))
      _mkdir(acTmp);

   // Load suffix table
   GetPrivateProfileString(pCnty, "UseSfxDev", "", acTmp, _MAX_PATH, acIniFile);
   if (acTmp[0] == 'Y')
   {
      bUseSfxXlat = true;
      GetIniString("System", "SfxDevTbl", "", acTmp, _MAX_PATH, acIniFile);
   } else if (acTmp[0] == 'C')
   {
      // Use custom county table
      bUseSfxXlat = true;
      GetIniString(pCnty, "SuffixTbl", ".\\Suffix_Simple.txt", acTmp, _MAX_PATH, acIniFile);
   } else if (acTmp[0] == 'S')
   {
      bUseSfxXlat = true;
      GetIniString("System", "SfxSmlTbl", ".\\Suffix_Simple.txt", acTmp, _MAX_PATH, acIniFile);
   } else
   {
      bUseSfxXlat = false;
      GetIniString("System", "SuffixTbl", ".\\Suffix.txt", acTmp, _MAX_PATH, acIniFile);
   }
   iRet = LoadSuffixTbl(acTmp, bUseSfxXlat);

   // Load Deed xref table
   GetIniString("System", "DeedXref", "", acTmp, _MAX_PATH, acIniFile);
   iNumDeeds = LoadXrefTable(acTmp, (XREFTBL *)&asDeed[0], MAX_DEED_ENTRIES);

   // Load TRA xref table
   GetIniString("Data", "TraXref", "", acTmp, _MAX_PATH, acIniFile);
   sprintf(acCityFile, acTmp, pCnty);
   if (!_access(acCityFile, 0))
      iNumTRA = LoadXrefTable(acCityFile, (XREFTBL *)&asTRA[0], MAX_TRA_ENTRIES);
   else
      iNumTRA = 0;

   // Load Instrument table
   GetIniString(pCnty, "InstTbl", "", acTmp, _MAX_PATH, acIniFile);
   if (!_access(acTmp, 0))
      iNumInst = LoadXrefTable(acTmp, (XREFTBL *)&asInst[0], MAX_INST_ENTRIES);
   else
      iNumInst = 0;

   // Load Site xref table
   //GetIniString("Data", "SiteXref", "", acTmp, _MAX_PATH, acIniFile);
   //sprintf(acCityFile, acTmp, pCnty);
   //if (!_access(acCityFile, 0))
   //   iNumSite = LoadXrefTable(acCityFile, (XREFTBL *)&asSite[0], MAX_TRA_ENTRIES);
   //else
   //   iNumSite = 0;

   // Get Lookup file name
   GetIniString("System", "LookUpTbl", "", acLookupTbl, _MAX_PATH, acIniFile);
   if (_access(acLookupTbl, 0))
   {
      LogMsg("***** Lookup file [%s] is missing.", acLookupTbl);
      return -1;
   } else
   {
      // Load tables
      iRet = LoadLUTable(acLookupTbl, "[Quality]", NULL, MAX_ATTR_ENTRIES);
      if (!iRet)
      {
         LogMsg("*** Error Looking for table [Quality] in %s", acLookupTbl);
         return -1;
      }
   }

   // Get Usecode template
   GetIniString("System", "UseTbl", "", acTmp, _MAX_PATH, acIniFile);
   iNumUseCodes = 0;
   if (acTmp[0])
   {
      sprintf(acUseTbl, acTmp, pCnty);
      if (!_access(acUseTbl, 0))
         iRet = LoadUseTbl(acUseTbl);
   }

   // Get county info
   GetIniString("System", "CountyTbl", ".\\CountyInfo.csv", acTmp, _MAX_PATH, acIniFile);
   printf("Loading county table: %s\n", acTmp);
   iRet = LoadCountyInfo(pCnty, acTmp);

   if (iRet == 1)
   {
      // Load city table
      GetIniString(pCnty, "CityFile", "", acCityFile, _MAX_PATH, acIniFile);
      if (acCityFile[0] < 'A')
      {
         GetIniString("Data", "CityFile", "", acTmp, _MAX_PATH, acIniFile);
         sprintf(acCityFile, acTmp, pCnty);
      }
      printf("Loading city table: %s\n", acCityFile);
      if (strstr(acCityFile, "_N2CX"))
         iRet = Load_N2CX(acCityFile);
      else
         iRet = LoadCities(acCityFile);

      // If there is problem accessing City file, file system may have problem.  You may need to reboot this server.
      if (iRet < 1)
      {
         LogMsg("***** File system may have problem. Try to reboot this server.");
         return iRet;
      }

      if (!lLienYear)
         lLienYear = atol(myCounty.acYearAssd);
      else
         sprintf(myCounty.acYearAssd, "%d", lLienYear);

      strcpy(acTmp, myCounty.acYearAssd);
      strcat(acTmp, "0101");
      lLienDate = atol(acTmp);
   }

   // Load Juristion table
   if (iLoadFlag & MERG_ZONE)
   {
      GetIniString(pCnty, "JurisTbl", "", acCityFile, _MAX_PATH, acIniFile);
      if (acCityFile[0] < 'A')
      {
         GetIniString("Data", "JurisTbl", "", acTmp, _MAX_PATH, acIniFile);
         sprintf(acCityFile, acTmp, pCnty);
      }
      if (!_access(acCityFile, 0))
      {
         printf("Loading Jurisdiction table: %s\n", acCityFile);
         iRet = LoadJurisList(acCityFile);
      }
   }

   // Tax Year
   lTaxYear = GetPrivateProfileInt(pCnty, "TaxYear", lLienYear, acIniFile);

   // Set debug flag
   GetPrivateProfileString(pCnty, "Debug", "N", acTmp, _MAX_PATH, acIniFile);
   if (acTmp[0] == 'Y')
      bDebug = true;
   else
      bDebug = false;

   // Remove temp files - default is "Y"
   GetPrivateProfileString("Debug", "RemTmpFile", "Y", acTmp, _MAX_PATH, acIniFile);
   if (bRemTmpFile && acTmp[0] == 'N')
      bRemTmpFile = false;

   // Clean up flag: bClean
   GetPrivateProfileString(pCnty, "Clean", "Y", acTmp, _MAX_PATH, acIniFile);
   if (acTmp[0] == 'Y')
      bClean = true;
   else
      bClean = false;

   // Init RecCnt array
   for (int i=0; i<MAX_REC_CNT; i++)
      asRecCnt[i].RecCnt = 0;

   // Vacant Land file
   GetIniString(pCnty, "VACANT", "", acVacFile, _MAX_PATH, acIniFile);

   // Prop8 template
   GetIniString("Data", "Prop8Exp", "", acProp8Tmpl, _MAX_PATH, acIniFile);

   // Check default send mail flag
   if (!bSendMail)
   {
      GetPrivateProfileString(pCnty, "SendMail", "", acTmp, _MAX_PATH, acIniFile);
      if (!acTmp[0])
         GetPrivateProfileString("System", "SendMail", "", acTmp, _MAX_PATH, acIniFile);
      if (acTmp[0] == 'Y')
         bSendMail = true;
   }

   // Get max changed records allowed
   iMaxChgAllowed = GetPrivateProfileInt(pCnty, "MaxChgAllowed", 0, acIniFile);

   // Init directions
   InitDirs();

   // Initialize globals
   lLastGrGrDate = 0;

   // Tax file template
   /*
   GetIniString("Data", "TaxCode", "", sTCTmpl, _MAX_PATH, acIniFile);
   GetIniString("Data", "TaxCodeMstr", "", sTCMTmpl, _MAX_PATH, acIniFile);
   GetIniString("Data", "Tax", "", sTaxTmpl, _MAX_PATH, acIniFile);
   GetIniString("Data", "Redemption", "", sRedTmpl, _MAX_PATH, acIniFile);
   GetIniString("Data", "TaxRollInfo", "", sTRITmpl, _MAX_PATH, acIniFile);
   GetIniString("Data", "TrustFund", "", sTFTmpl, _MAX_PATH, acIniFile);
   */
   GetIniString("Data", "TaxOut", "", sTaxOutTmpl, _MAX_PATH, acIniFile);
   GetIniString("Data", "TaxSql", "", sTaxSqlTmpl, _MAX_PATH, acIniFile);
   GetIniString("System", "ImportLog", "", sImportLogTmpl, _MAX_PATH, acIniFile);

   // Check AutoImport option
   GetPrivateProfileString(pCnty, "ImportSale", "", acTmp, _MAX_PATH, acIniFile);
   if (acTmp[0] < ' ')
      GetPrivateProfileString("System", "ImportSale", "", acTmp, _MAX_PATH, acIniFile);
   if (toupper(acTmp[0]) == 'Y')
      bSaleImport = true;
   else
      bSaleImport = false;

   GetPrivateProfileString(pCnty, "ImportGrGr", "", acTmp, _MAX_PATH, acIniFile);
   if (acTmp[0] < ' ')
      GetPrivateProfileString("System", "ImportGrGr", "", acTmp, _MAX_PATH, acIniFile);
   if (toupper(acTmp[0]) == 'Y')
      bGrgrImport = true;
   else
      bGrgrImport = false;

   GetPrivateProfileString(pCnty, "UpdateDetail", "", acTmp, _MAX_PATH, acIniFile);
   if (acTmp[0] < ' ')
      GetPrivateProfileString("System", "UpdateDetail", "", acTmp, _MAX_PATH, acIniFile);
   if (toupper(acTmp[0]) == 'Y')
      bTaxUpdtDetail = true;
   else
      bTaxUpdtDetail = false;

   GetPrivateProfileString(pCnty, "ImportTax", "", acTmp, _MAX_PATH, acIniFile);
   if (acTmp[0] < ' ')
      GetPrivateProfileString("System", "ImportTax", "", acTmp, _MAX_PATH, acIniFile);
   if (toupper(acTmp[0]) == 'Y')
      bTaxImport = true;
   else
      bTaxImport = false;

   GetPrivateProfileString(pCnty, "MixSale", "", acTmp, _MAX_PATH, acIniFile);
   if (acTmp[0] < ' ')
      GetPrivateProfileString("System", "MixSale", "", acTmp, _MAX_PATH, acIniFile);
   if (toupper(acTmp[0]) == 'Y')
      bMixSale = true;
   else
      bMixSale = false;

   // Init variables
   GetPrivateProfileString(myCounty.acCntyCode, "UseFeePrcl", "Y", acTmp, _MAX_PATH, acIniFile);
   if (acTmp[0] == 'Y')
      m_bUseFeePrcl = true;
   else
      m_bUseFeePrcl = false;

   GetIniString("Data", "XferFile", "", acXferTmpl, _MAX_PATH, acIniFile);
   if (acXferTmpl[0] < ' ')
      LogMsg("***** Transfer file wasn't defined.  Please check LOADONE.INI for XferFile= in [Data] section.");

   GetPrivateProfileString(pCnty, "LdrSep", "", acTmp, _MAX_PATH, acIniFile); 
   if (isdigit(acTmp[0]))
   {
      iRet = atoi(acTmp);
      if (iRet < 255)
         cLdrSep = iRet;
      else
         LogMsg("***** Invalid value for LdrSep: %s.  Please modify this value under [%s]", acTmp, pCnty);
   } else
      cLdrSep = acTmp[0];

   GetPrivateProfileString(pCnty, "Delimiter", ",", acTmp, _MAX_PATH, acIniFile); 
   if (isdigit(acTmp[0]))
   {
      iRet = atoi(acTmp);
      if (iRet < 255)
         cDelim = iRet;
      else
         LogMsg("***** Invalid value for Delimiter: %s.  Please modify this value under [%s]", acTmp, pCnty);
   } else
      cDelim = acTmp[0];
   iHdrRows = GetPrivateProfileInt(pCnty, "HdrRows", 1, acIniFile);

   GetIniString("System", "DocPath", "", acTmp, _MAX_PATH, acIniFile);
   if (acTmp[0] > ' ')
      sprintf(acDocPath, acTmp, myCounty.acCntyCode);
   
   //iRet = GetIniString(pCnty, "AreaFile", "", acTmp, _MAX_PATH, acIniFile);
   //if (iRet < 10)
   //   iRet = GetIniString("Data", "AreaFile", "", acTmp, _MAX_PATH, acIniFile);
   //if (acTmp[0] > ' ')
   //   sprintf(acAreaFile, acTmp, pCnty, "dat");

   char acVestTbl[_MAX_PATH];
   GetIniString(pCnty, "VestingTbl", "", acVestTbl, _MAX_PATH, acIniFile);
   if (acVestTbl[0] < ' ')
      GetIniString("System", "VestingTbl", "", acVestTbl, _MAX_PATH, acIniFile);

   iRet = loadVesting(pCnty, acVestTbl);
   bDupApn = false;
   bUseConfSalePrice = false;

   return iRet;
}

/********************************* MergeGrGrDoc ******************************
 *
 * Merge GrGr data to PQ4 product.
 *
 *****************************************************************************

int MergeGrGrDoc(char *pCnty)
{
   char     *pTmp, acBuf[MAX_RECSIZE], acTmp[64];
   char     cFileCnt=1;
   char     acRawFile[_MAX_PATH], acOutFile[_MAX_PATH], acGrGrFile[_MAX_PATH];

   HANDLE   fhIn, fhOut;
   GRGR_DOC SaleRec;

   int      iTmp, iSaleUpd=0;
   DWORD    nBytesRead;
   DWORD    nBytesWritten;
   BOOL     bRet, bEof, bRename=true;
   long     lRet=0, lCnt=0;

   memset(&SaleRec, ' ', sizeof(GRGR_DOC));

   sprintf(acRawFile, acRawTmpl, pCnty, pCnty, "R01");
   if (_access(acRawFile, 0))
   {
      sprintf(acRawFile, acRawTmpl, pCnty, pCnty, "S01");
      bRename = false;
   }

   sprintf(acOutFile, acRawTmpl, pCnty, pCnty, "N01");
   sprintf(acGrGrFile, acGrGrTmpl, pCnty, "DAT");

   if (!iGrGrApnLen)
      iGrGrApnLen = myCounty.iApnLen;

   if (_access(acGrGrFile, 0))
      return 0;

   // Open GrGr file
   LogMsg("Merge GrGr data %s to R01 file", acGrGrFile);
   fdSale = fopen(acGrGrFile, "r");
   if (fdSale == NULL)
   {
      LogMsg("***** Error opening GrGr file: %s\n", acGrGrFile);
      return 2;
   }

   // Get first record
   pTmp = fgets((char *)&SaleRec, sizeof(GRGR_DOC), fdSale);
   if (!pTmp || feof(fdSale))
   {
      LogMsg("*** No GrGr data available");
      return 0;
   }

   // Open Input file
   LogMsg("Open input file %s", acRawFile);
   fhIn = CreateFile(acRawFile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhIn == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening input file to merge GrGr: %s\n", acRawFile);
      return 3;
   }

   // Open Output file
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening output file to merge GrGr: %s\n", acOutFile);
      return 4;
   }

   // Copy skip record
   ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);
   bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);

   iNoMatch=0;
   bEof = false;

   // Merge loop
   while (!bEof)
   {
      bRet = ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);
      // Check for EOF
      if (!bRet)
      {
         LogMsg("***** Error reading input file %s (%f)", acRawFile, GetLastError());
         break;
      }

      if (!nBytesRead)
      {
         // EOF
         CloseHandle(fhIn);
         CloseHandle(fhOut);
         fhIn = 0;
         fhOut = 0;

         LogMsg("Rename output file %s to %s", acOutFile, acRawFile);
         if (bRename)
         {
            sprintf(acTmp, "M0%c", cFileCnt|0x30);
            sprintf(acBuf, acRawTmpl, pCnty, pCnty, acTmp);
            if (!_access(acBuf, 0))
               remove(acBuf);
            iTmp = rename(acRawFile, acBuf);
            iTmp = rename(acOutFile, acRawFile);
         } else
         {
            sprintf(acTmp, "R0%c", cFileCnt|0x30);
            sprintf(acBuf, acRawTmpl, pCnty, pCnty, acTmp);
            iTmp = rename(acOutFile, acBuf);
         }


         // Check for next input
         cFileCnt++;
         acRawFile[strlen(acRawFile)-1] = cFileCnt | 0x30;
         if (!_access(acRawFile, 0))
         {
            // Open next Input file
            LogMsg("Open input file %s", acRawFile);
            fhIn = CreateFile(acRawFile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
                  FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

            if (fhIn == INVALID_HANDLE_VALUE)
            {
               LogMsg("***** Error opening input file to merge GrGr: %s\n", acRawFile);
               break;
            }
            bRet = ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);

            // Open Output file
            acOutFile[strlen(acRawFile)-1] = cFileCnt | 0x30;
            LogMsg("Open output file %s", acOutFile);
            fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
                  FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);
            if (fhOut == INVALID_HANDLE_VALUE)
            {
               LogMsg("***** Error opening output file to merge GrGr: %s\n", acOutFile);
               break;
            }
         } else
            break;
      }

      // Update sale
GrGr_ReLoad:
      iTmp = memcmp(acBuf, SaleRec.APN, iGrGrApnLen);
      if (!iTmp)
      {
         // Merge sale data
         iTmp = MergeGrGr((char *)&SaleRec, acBuf);
         if (iTmp > 0)
            iSaleUpd++;

         // Read next sale record
         pTmp = fgets((char *)&SaleRec, sizeof(GRGR_DOC), fdSale);
         if (!pTmp)
            bEof = false;    // Signal to stop sale update
         else
            goto GrGr_ReLoad;
      } else
      {
         if (iTmp > 0)        // Sale not match, advance to next sale record
         {
            if (!memcmp(acBuf, "99999999", 8))
            {
               nBytesRead = 0;
               continue;
            } else
            {
               iNoMatch++;
               if (bDebug)
                  LogMsg0("*** Sale not match : %.12s > %.12s (%d) ", acBuf, SaleRec.APN, lCnt);
               pTmp = fgets((char *)&SaleRec, sizeof(GRGR_DOC), fdSale);
               if (!pTmp)
                  bEof = true;    // Signal to stop sale update
               else
                  goto GrGr_ReLoad;
            }
         }
      }

      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);

      if (!bRet)
      {
         LogMsg("***** Error occurs: %d\n", GetLastError());
         lRet = WRITE_ERR;
         break;
      }
   }

   // Do the rest of the file
   while (nBytesRead>0 && ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL))
   {
      if (!nBytesRead)
         break;

      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
      lCnt++;
   }


   // Close files
   if (fdSale)
      fclose(fdSale);
   if (fhOut)
      CloseHandle(fhOut);
   if (fhIn)
   {
      CloseHandle(fhIn);

      // Rename outfile
      if (bRename)
      {
         sprintf(acTmp, "M0%c", cFileCnt|0x30);
         sprintf(acBuf, acRawTmpl, pCnty, pCnty, acTmp);
         if (!_access(acBuf, 0))
            remove(acBuf);
         iTmp = rename(acRawFile, acBuf);
      } else
      {
         sprintf(acTmp, "R0%c", cFileCnt|0x30);
         sprintf(acRawFile, acRawTmpl, pCnty, pCnty, acTmp);
      }
      LogMsg("Rename output file %s to %s", acOutFile, acRawFile);
      iTmp = rename(acOutFile, acRawFile);
   }

   // Update output record count
   if (lCnt > 1000)
      lRecCnt = lCnt;

   LogMsg("Total output records:       %u", lCnt);
   LogMsg("Total GrGr records updated: %u", iSaleUpd);
   LogMsg("Total GrGr records skipped: %u", iNoMatch);
   return 0;
}

/********************************* MergeGrGrFile *****************************
 *
 * Merge GrGr
 *
 *****************************************************************************

int MergeGrGrFile(char *pCnty)
{
   char     *pTmp, acBuf[MAX_RECSIZE];
   char     cFileCnt=1;
   char     acRawFile[_MAX_PATH], acOutFile[_MAX_PATH], acGrGrFile[_MAX_PATH];

   HANDLE   fhIn, fhOut;
   FILE     *fdSale;
   SALE_REC SaleRec;

   int      iTmp, iSaleUpd=0;
   DWORD    nBytesRead;
   DWORD    nBytesWritten;
   BOOL     bRet, bEof;
   long     lRet=0, lCnt=0;

   sprintf(acRawFile, acRawTmpl, pCnty, pCnty, "R01");
   sprintf(acOutFile, acRawTmpl, pCnty, pCnty, "N01");
   sprintf(acGrGrFile, acGrGrTmpl, pCnty, "DAT");

   if (!iGrGrApnLen)
      iGrGrApnLen = myCounty.iApnLen;

   if (_access(acGrGrFile, 0))
      return 1;

   // Open Sale file
   LogMsg("Merge GrGr data %s to R01 file", acGrGrFile);
   fdSale = fopen(acGrGrFile, "r");
   if (fdSale == NULL)
   {
      LogMsg("***** Error opening sale file: %s\n", acGrGrFile);
      return -2;
   }

   // Get first record
   pTmp = fgets((char *)&SaleRec, sizeof(SALE_REC), fdSale);
   if (!pTmp || feof(fdSale))
   {
      LogMsg("*** No GrGr data available");
      return 0;
   }

   // Open Input file
   LogMsg("Open input file %s", acRawFile);
   fhIn = CreateFile(acRawFile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhIn == INVALID_HANDLE_VALUE)
      return -3;

   // Open Output file
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
      return -4;

   // Copy skip record
   ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);
   bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);

   iNoMatch=0;
   bEof = false;

   // Merge loop
   while (!bEof)
   {
      bRet = ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);
      // Check for EOF
      if (!bRet)
      {
         LogMsg("Error reading input file %s (%f)", acRawFile, GetLastError());
         break;
      }

      if (!nBytesRead)
      {
         // EOF
         cFileCnt++;
         acRawFile[strlen(acRawFile)-1] = cFileCnt | 0x30;
         if (!_access(acRawFile, 0))
         {
            CloseHandle(fhIn);
            CloseHandle(fhOut);
            fhIn = 0;
            fhOut = 0;

            // Open next Input file
            LogMsg("Open input file %s", acRawFile);
            fhIn = CreateFile(acRawFile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
                  FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

            if (fhIn == INVALID_HANDLE_VALUE)
               break;
            bRet = ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);

            // Open Output file
            acOutFile[strlen(acRawFile)-1] = cFileCnt | 0x30;
            LogMsg("Open output file %s", acOutFile);
            fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
                  FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);
            if (fhOut == INVALID_HANDLE_VALUE)
               break;
         } else
            break;
      }

      // Update sale
GrGr_ReLoad:
      iTmp = memcmp(acBuf, SaleRec.acApn, iGrGrApnLen);
      if (!iTmp)
      {
         // Merge sale data
         iTmp = MergeSale((SALE_REC *)&SaleRec, acBuf);
         if (iTmp > 0)
            iSaleUpd++;

         // Read next sale record
         pTmp = fgets((char *)&SaleRec, SALEREC_SIZE, fdSale);
         if (!pTmp)
            bEof = true;    // Signal to stop sale update
         else
            goto GrGr_ReLoad;
      } else
      {
         if (iTmp > 0)        // Sale not match, advance to next sale record
         {
            LogMsg0("*** Sale not match : %.12s > %.12s (%d) ", acBuf, SaleRec.acApn, lCnt);
            pTmp = fgets((char *)&SaleRec, SALEREC_SIZE, fdSale);
            if (!pTmp)
               bEof = true;    // Signal to stop sale update
            else
               goto GrGr_ReLoad;
         }
      }

      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);

      if (!bRet)
      {
         LogMsg("Error occurs: %d\n", GetLastError());
         lRet = WRITE_ERR;
         break;
      }
   }

   // Do the rest of the file
   while (nBytesRead>0 && ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL))
   {
      if (!nBytesRead)
         break;

      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
      lCnt++;
   }


   // Close files
   if (fdSale)
      fclose(fdSale);
   if (fhOut)
      CloseHandle(fhOut);
   if (fhIn)
      CloseHandle(fhIn);

   // Rename outfile
   LogMsg("Rename output file");
   sprintf(acBuf, acRawTmpl, pCnty, pCnty, "M01");
   if (!_access(acBuf, 0))
      remove(acBuf);
   sprintf(acRawFile, acRawTmpl, pCnty, pCnty, "R01");
   rename(acRawFile, acBuf);
   rename(acOutFile, acRawFile);

   LogMsg("Total output records:       %u", lCnt);
   LogMsg("Total unmatched records:    %u", iNoMatch);
   LogMsg("Total GrGr records updated: %u", iSaleUpd);

   return 0;
}
*/

/************************************** Usage ********************************
 *
 *
 *****************************************************************************/

void Usage()
{
   printf("\nUsage: LoadOne -C<County code> [-A] [-B?] [-D[?]] [-E[x]] [-G] [-I?] [-M[?]] [-N[?]] [-L[?]] [-U[?]] [-Ps] [-X?] [-S<n>]\n");
   printf("\t-A  : Merge attribute file\n");
   printf("\t-B8 : Set prop8 flag\n");
   printf("\t-C  : County to load\n");
   printf("\t-D  : Duplicate sale records from old file\n");
   printf("\t-D1 : Convert cum sale file\n");
   printf("\t-D2 : Convert cum grgr file\n");
   printf("\t-D8 : Update prop8 flag in DB\n");
   printf("\t-Dc : Create update file (for debug only)\n");
   printf("\t-Dn : Do not create update file (for debug only)\n");
   printf("\t-Dr : Do not remove temp files\n");
   printf("\t-E  : Email on error\n");
   printf("\t-Ext: Export SQL command for tax update\n");
   printf("\t-Fn : Fix owner names (convert to upper case)\n");
   printf("\t-Fs : Fix saleamt that is less than 10\n");
   printf("\t-Ft : Fix TRA\n");
   printf("\t-Fz : Updating Zipcode\n");
   printf("\t-G  : Load GrGr file\n");
   printf("\t-Is : Import cumumative sale to SQL (???_Sale.sls\n");
   printf("\t-Ig : Import cumumative GrGr to SQL (???_Grgr.sls)\n");
   printf("\t-M  : Merge GrGr file (for testing only)\n");
   printf("\t-M1 : Merge situs address1\n");
   printf("\t-Ma : Merge attribute data\n");
   printf("\t-Mg : Merge GrGr data\n");
   printf("\t-Mo : Merge other values from Lien extract file\n");
   printf("\t-Ms : Merge cummulative sale\n");
   printf("\t-Mt : Merge transfer data\n");
   printf("\t-L  : Load Lien date roll (create lien file and load roll).\n");
   printf("\t-La : Load Assr using LDR file.\n");
   printf("\t-Lc : Load Char/Attr file.\n");
   printf("\t-Ld : Load daily update file (EDX).\n");
   printf("\t-Lg : Load GrGr file (county specific output)\n");
   printf("\t-Ll : Load Land size file.\n");
   printf("\t-Lr : Load Lien roll file (same as -L).\n");
   printf("\t-Ls : Load Sale file.\n");
   printf("\t-Lz : Load zipcode file into SQL.\n");
   printf("\t-Ps : Purge all sales before update data from sale file\n");
   printf("\t-N  : Do not encode suffix and city\n");
   printf("\t-Nm : Do not send mail\n");
   printf("\t-O  : Overwrite logfile (default append)\n");
   printf("\t-Sn : Number of records skip (default 1)\n");
   printf("\t-T  : Load tax files\n");
   printf("\t-U  : Update roll file using old S01 file\n");
   printf("\t-Ur : Update roll file replace current values in S01 file\n");
   printf("\t-Ua : Update Assr roll using old S01 file\n");
   printf("\t-Uc : Update PQ4 R01 file using Char file\n");
   printf("\t-Us : Update PQ4 R01 file using Sale file\n");
   printf("\t-Ut : Update tax tables\n");
   printf("\t-Ux : Update PQ4 R01 file using Sale extract file\n");
   printf("\t-X8 : Extract Prop8 APN to be imported to SQL.\n");
   printf("\t-Xa : Extract attribute from attr file.\n");
   printf("\t-Xc : Extract sale (GrGr_Exp.dat) from GrGr file.\n");
   printf("\t-XC : Extract XC file for bulk customer\n");
   printf("\t-Xg : Extract GrGr data.\n");
   printf("\t-Xl : Extract lien value from lien file.\n");
   printf("\t-Xr : Extract region file (for LAX).\n");
   printf("\t-Xs : Extract sale data for MergeAdr from sale file.\n");
   printf("\t-Xsi: Extract sale data for R01 and SQL import.\n");
   printf("\t-Xv : Extract values from value file.\n");
   printf("\t-Xf : Extract final values from equalized/tax file.\n");
   printf("\t-X1 : Extract Situs address.\n");
   printf("\t-Ynnnn: Process year.  This overwrites default year in CountyInfo.csv\n");
   exit(1);
}

/********************************** ParseCmd() *******************************
 *
 *
 *****************************************************************************/

void ParseCmd(int argc, char* argv[])
{
   char  chOpt;               // gotten option character
   char *pszParam;

   bGrGrAvail = true;
   bEnCode = true;
   iSkip = 1;
   iLoadFlag = 0;
   lLienYear = 0;

   bOverwriteLogfile = false;
   bSendMail = false;
   bMergeOthers = false;
   bClearSales = false;
   bReplValue = false;
   bConvSale = false;
   bConvGrGr = false;
   iLoadTax = 0;
   bCreateUpd = false;
   bDontUpd = false;
   bRemTmpFile = true;
   bEbc2Asc = true;
   bExpSql = false;

   if (argv[1][0] != '-')
   {
      strcpy(myCounty.acCntyCode, argv[1]);
      return;
   }

   while (1)
   {
      chOpt = GetOption(argc, argv, "AB:C:D:E:F:G:I:L:M:N:OP:S:T:U:X:Y:?", &pszParam);
      if (chOpt > 1)
      {
         // chOpt is valid argument
         switch (chOpt)
         {
            case 'A':                           // Merge attribute data
               iLoadFlag |= MERG_ATTR;
               break;

            case 'B':
               if (pszParam != NULL)
               {
                  if (*pszParam == '8')         // set Prop8 flag
                     lOptProp8 |= MYOPT_SET;
               }
               break;

            case 'C':                           // county code
               if (pszParam != NULL)
                  strcpy(myCounty.acCntyCode, pszParam);
               else
               {
                  printf("Missing county code\n");
                  Usage();
               }
               break;

            case 'D':                           
               if (pszParam != NULL)
               {
                  if (!strcmp(pszParam, "8"))
                     lOptProp8 |= MYOPT_UDB;    // Update Prop8 flag in DB
                  else if (*pszParam == '1')    // Convert cum sale to SCSAL_REC
                     bConvSale = true;
                  else if (*pszParam == '2')    // Convert cum grgr to SCSAL_REC
                     bConvGrGr = true;
                  else if (*pszParam == 'n')
                     bDontUpd = true;
                  else if (*pszParam == 'c')
                     bCreateUpd = true;
                  else if (*pszParam == 'r')
                     bRemTmpFile = false;
                  else if (*pszParam == 't')    // Don't translate EBCII to ASCII
                     bEbc2Asc = false;
               } 
               break;

            case 'E':                           
               if (pszParam != NULL)
               {
                  if (!strcmp(pszParam, "xt"))
                     bExpSql = true;            // Export SQL command for import
               } else
                  bSendMail = true;             // Email if error occurs
               break;

            case 'F':                           // Fix data field
               if (pszParam != NULL)
               {
                  if (*pszParam == 't')
                     lOptMisc |= M_OPT_FIXTRA;
                  else if (*pszParam == 'n')
                     lOptMisc |= M_OPT_FIXOWNER;
                  else if (*pszParam == 's')
                     lOptMisc |= M_OPT_FIXSAMT; // Fix sale amt
                  else if (*pszParam == 'e')
                     lOptMisc |= M_OPT_FIXFEFLG;
                  else if (*pszParam == 'c')
                     lOptMisc |= M_OPT_UCITYZIP;// Update CityZip file
                  else if (*pszParam == 'z')
                     lOptMisc |= M_OPT_UPDZIP;
                  else if (*pszParam == 'x')
                     lOptMisc |= M_OPT_FIXDOCT; // Fix DocType in cumsale
                  else if (*pszParam == 'y')
                     lOptMisc |= M_OPT_FIXDOCN; // Fix DocNum in cumsale
               }
               break;

            case 'G':                           // Load GrGr data
               iLoadFlag |= LOAD_GRGR;
               if (pszParam != NULL)
               {
                  if (*pszParam == 'i')                     
                     iLoadFlag |= EXTR_IGRGR;
               }

               break;

            case 'I':                           // Generate import file
               if (pszParam != NULL)
               {
                  if (*pszParam == 's')         // Import sale
                     iLoadFlag |= EXTR_ISAL;
                  else if (*pszParam == 'g')    // Import Grgr
                     iLoadFlag |= EXTR_IGRGR;
               }
               break;

            case 'L':                           // Default load lien date roll
               if (pszParam != NULL)
               {
                  if (*pszParam == 's')
                     iLoadFlag |= LOAD_SALE;
                  else if (*pszParam == 'a')
                     iLoadFlag |= LOAD_ASSR;
                  else if (*pszParam == 'l')
                     iLoadFlag |= LOAD_LAND;
                  else if (*pszParam == 'g')
                     iLoadFlag |= LOAD_GRGR;
                  else if (*pszParam == 'c')
                     iLoadFlag |= LOAD_ATTR;    // Load or convert Characteristic/Attribute
                  else if (*pszParam == 't')    // Load tax file
                  {
                     iLoadTax = TAX_LOADING;
                     bRemTmpFile = false;
                  } 
                  else if (*pszParam == 'u')    // Load all update files (VEN)
                  {
                     iLoadFlag |= LOAD_ALL;
                  }
                  else 
                  {
                     lProcDate = atol(pszParam); // -Lyyyymmdd
                     if (lProcDate > lLienDate && lProcDate <= lToday && !iLoadTax)
                        iLoadFlag |= LOAD_DAILY; // Load daily update file
                     else if (*pszParam == 'd')  // -Ld
                     {
                        lProcDate = lToday;
                        iLoadFlag |= LOAD_DAILY; // Load daily update file
                     } else if (!iLoadTax)
                        printf("***** Unknown option -L%s (ignore)\n", pszParam);
                  }
               } else
               {
                  iLoadFlag |= LOAD_LIEN;
               }
               break;

            case 'M':                           // Merge 
               if (pszParam != NULL)
               {
                  if (*pszParam == 's')
                  {
                     iLoadFlag |= MERG_CSAL;    // Merge cummulative sale
                     if (*(pszParam+1) == 'i')
                        iLoadFlag |= EXTR_ISAL;
                     else if (*(pszParam+1) == 'x')
                        lOptMisc |= M_OPT_REMXFER;
                  } 
                  else if (*pszParam == 'g')
                  {
                     iLoadFlag |= MERG_GRGR;    // Merge cummulative GrGr
                     if (*(pszParam+1) == 'i')
                        iLoadFlag |= EXTR_IGRGR;
                  } 
                  else if (*pszParam == '1')
                     iLoadFlag |= MERG_SADR;    // Merge situs addr1 in ORG
                  else if (*pszParam == 'a')
                     iLoadFlag |= MERG_ATTR;    // Merge attribute 
                  else if (*pszParam == 'l')
                     iLoadFlag |= MERG_LEGAL;   // Merge legal file in SCL
                  else if (*pszParam == 'p')
                     iLoadFlag |= MERG_PUBL;    // Merge public parcel
                  else if (*pszParam == 'r')
                     iLoadFlag |= MERG_GISA;    // Merge acreage from GIS basemap 
                  else if (*pszParam == 'o')
                     bMergeOthers = true;       // Merge other values
                  else if (*pszParam == 'n')
                     iLoadFlag |= UPDT_XSAL;    // Merge NDC sale UPDT_XSAL
                  else if (*pszParam == 'z')
                     iLoadFlag |= MERG_ZONE;    // Merge PQZoning file
                  else if (*pszParam == 'f')
                     lOptMisc |= M_OPT_MERGVAL; // Merge final value
               } else
                  iLoadFlag |= MERG_GRGR;
               break;

            case 'N':                           // Do not encode
               if (pszParam != NULL)
               {
                  if (*pszParam == 'm')         // Do not send mail             
                     bSendMail = false;
               } else
                  bEnCode = false;
               break;

            case 'O':                           // Overwrite logfile
               bOverwriteLogfile = true;
               break;

            case 'P':                           // Purge old data
               if (pszParam && *pszParam == 's')// clear sale data
                  bClearSales = true;
               break;

            case 'S':                           // Skip records
               if (pszParam != NULL)
                  iSkip = atoi(pszParam);
               else
                  Usage();
               break;

            case 'T':   // Load tax files
               iLoadTax = TAX_LOADING;
               bRemTmpFile = false;
               break;

            case 'U':                           // Update roll file
               if (pszParam != NULL)
               {
                  if (*pszParam == 's')
                  {
                     iLoadFlag |= UPDT_SALE;
                     if (*(pszParam+1) == 'i')
                        iLoadFlag |= EXTR_ISAL;
                  }
                  else if (*pszParam == 'a')
                     iLoadFlag |= UPDT_ASSR;
                  else if (*pszParam == 'x')
                     iLoadFlag |= UPDT_XSAL;
                  else if (*pszParam == 't')
                     iLoadTax = TAX_UPDATING;
                  else if (*pszParam == 'u')
                     iLoadFlag |= UPDT_SUSE;
                  else if (*pszParam == 'r')
                  {
                     bReplValue = true;
                     iLoadFlag |= LOAD_UPDT;
                  }
                  else if (*pszParam == 'c')
                     lOptMisc |= M_OPT_ROLLCOR;
               } else
                  iLoadFlag |= LOAD_UPDT;
               break;

            case 'X':                            // Extract data
               if (pszParam != NULL)
               {
                  if (*pszParam == 'l')          // lower case letter L
                     iLoadFlag |= EXTR_LIEN;
                  else if (*pszParam == 'a')
                     iLoadFlag |= EXTR_ATTR;
                  else if (*pszParam == 'C')
                     lOptExtr |= EXTR_XCHAR;
                  else if (*pszParam == 'c')
                     iLoadFlag |= EXTR_CSAL;
                  else if (*pszParam == 'd')
                     iLoadFlag |= EXTR_DESC;
                  else if (*pszParam == 'r')
                     iLoadFlag |= EXTR_REGN;
                  else if (*pszParam == 'n')
                     iLoadFlag |= EXTR_NRSAL;
                  else if (*pszParam == 's')
                  {
                     iLoadFlag |= EXTR_SALE;
                     if (*(pszParam+1) == 'i')
                        iLoadFlag |= EXTR_ISAL;
                  } 
                  else if (*pszParam == 'o')
                     lOptExtr |= EXTR_OWNER;
                  else if (*pszParam == 'h')
                     lOptExtr |= EXTR_MH;
                  else if (*pszParam == 'z')
                     lOptExtr |= EXTR_ZONE;
                  else if (*pszParam == '8')
                     lOptProp8 |= MYOPT_EXT;     // Extract prop8 flag
                  else if (*pszParam == 'v')
                  {
                     iLoadFlag |= EXTR_VALUE;
                     if (*(pszParam+1) == 'i')
                        iLoadFlag |= EXTR_IVAL;
                  } 
                  else if (*pszParam == 'f')    // Extract final value
                     lOptExtr |= EXTR_FVAL;
                  else
                  {
                     printf("***** Invalid extract option %s\n", pszParam);
                     Usage();
                  }
               } else
                  Usage();
               break;
            case 'Y':
               if (pszParam && *pszParam > '0')
               {
                  lLienYear = atol(pszParam);
                  if (lLienYear > lToyear || lLienYear < 2000)
                  {
                     printf("***** Invalid LDR year option %s\n", pszParam);
                     Usage();
                  }
               } else
                  Usage();
               break;

            case '?':   // usage info
            default:
               Usage();
               break;
         }
      }
      if (chOpt == 0)
      {
         // end of argument list
         break;
      }
      if ((chOpt == 1) || (chOpt == -1))
      {
         // standalone param or error
         printf("***** Argument [%s] not recognized\n", pszParam);
         Usage();
      }
   }

   if (iLoadFlag & LOAD_LIEN)
   {
      bCreateUpd = false;
      bDontUpd = true;
   }
}

/************************************** main *********************************
 *
 *
 *****************************************************************************/

int _tmain(int argc, TCHAR* argv[], TCHAR* envp[])
{
   int   iRet=1, iTmp;
   char  acTmp[256], acLogPath[256], acVersion[64];
   char  acSubj[256], acBody[512], acTmpFile[_MAX_PATH], acOutFile[_MAX_PATH];


   // initialize MFC and print and error on failure
   if (!AfxWinInit(::GetModuleHandle(NULL), NULL, ::GetCommandLine(), 0))
   {
      cerr << _T("Fatal Error: MFC initialization failed") << endl;
      return -1;
   }

   LoadString(theApp.m_hInstance, APP_VERSION_INFO, acVersion, 64);
   printf("%s\n\n", acVersion);
   sprintf(acTmp, "%s Options: ", acVersion);
   for (iTmp = 1; iTmp < argc; iTmp++)
   {
      strcat(acTmp, argv[iTmp]);
      strcat(acTmp, " ");
   }

   if (argc < 2)
      Usage();

   // Get today date - yyyymmdd
   getCurDate(acToday);
   lToday = atoi(acToday);
   lToyear= atoin(acToday, 4);

   // Parse command line
   ParseCmd(argc, argv);
   if (myCounty.acCntyCode[0] < 'A')
   {
      printf("ERROR: No county specified for processing.  Need -C<Countycode> option\n");
      return -2;
   }

   _getcwd(acIniFile, _MAX_PATH);
   strcat(acIniFile, "\\LoadOne.ini");

   // If not found INI file in working folder, check default location
   if (_access(acIniFile, 0))
      strcpy(acIniFile, "C:\\Tools\\LoadOne.ini");

   // Get log path
   iRet = GetIniString("System", "LogPath", "", acLogPath, _MAX_PATH, acIniFile);

   // Initialize
   printf("Initializing %s\n", acIniFile);
   lRecCnt = 0;
   lLastRecDate = 0;
   lLDRRecCount = 0;
   iMaxLegal = 0;
   alRegCnt[0]=alRegCnt[1]=alRegCnt[2]=alRegCnt[3]=0;

   // open log file
   if (iLoadFlag & LOAD_LIEN)
   {
      if (lLienYear > 0)
         sprintf(acLogFile, "%s\\Load_%s_%d.ldr", acLogPath, myCounty.acCntyCode, lLienYear);
      else
         sprintf(acLogFile, "%s\\Load_%s_%d.ldr", acLogPath, myCounty.acCntyCode, lToyear);
   } else if (iLoadTax == TAX_LOADING && !iLoadFlag)
      sprintf(acLogFile, "%s\\LoadTax_%s_%s.log", acLogPath, myCounty.acCntyCode, acToday);
   else if (iLoadTax == TAX_UPDATING)
      sprintf(acLogFile, "%s\\UpdtTax_%s_%s.log", acLogPath, myCounty.acCntyCode, acToday);
   else
      sprintf(acLogFile, "%s\\Load_%s_%s.log", acLogPath, myCounty.acCntyCode, acToday);

   if (bOverwriteLogfile)
      open_log(acLogFile, "w");
   else
      open_log(acLogFile, "a+");

   // Start processing
   LogMsg(acTmp);
   LogMsgD("Loading %s\n", myCounty.acCntyCode);

   iTmp = MergeInit(myCounty.acCntyCode);
   if (iTmp <= 0)
   {
      LogMsg("***** Error initializing LoadOne");
      if (iTmp < 0)
         sprintf(acBody, "File system might have problem.  You may need to reboot production server.");
      else
         sprintf(acBody, "Please review log file \"%s\" for more info", acLogFile);

      sprintf(acSubj, "*** %s - Error initializing LoadOne.", myCounty.acCntyCode);
      mySendMail(acIniFile, acSubj, acBody, NULL);

      goto LoadOne_Exit;
   }

   // Fix owner names
   if (lOptMisc & M_OPT_FIXOWNER)
   {
      iRet = fixOwners(myCounty.acCntyCode);
      goto LoadOne_Exit;
   }

   // Fix Full_Exe_Flg 
   if (lOptMisc & M_OPT_FIXFEFLG)
   {
      iRet = PQ_FixR01(myCounty.acCntyCode, PQ_FIX_FULLEXE); 
      if (!iLoadFlag)
         goto LoadOne_Exit;
   }

   //if (bClearSales)
   //   iTmp = PQ_FixR01(myCounty.acCntyCode, PQ_REM_XFER);

   if (iLoadFlag & LOAD_LIEN)
      LogMsg("LDR %s processing.", myCounty.acYearAssd);
   else if (iLoadFlag & LOAD_UPDT)
      LogMsg("Regular update using %s base year.", myCounty.acYearAssd);
   else if (iLoadTax & (TAX_LOADING|TAX_UPDATING))
      LogMsg("Loading %d tax file.", lLienYear);

   // Reset automation flag to 'W' for working
   if (iLoadFlag & (LOAD_LIEN|LOAD_UPDT|LOAD_GRGR|EXTR_SALE|MERG_CSAL|MERG_GRGR))
   {
      sprintf(acTmp, "UPDATE County SET Status='W' WHERE (CountyCode='%s')", myCounty.acCntyCode);
      updateTable(acTmp);
   }
   if (iLoadFlag & (LOAD_LIEN|LOAD_UPDT))
   {
      // Open Usecode log
      if (!openUsecodeLog(myCounty.acCntyCode, acIniFile))
         LogMsg("***** %s", getLastErrorLog());

      sprintf(acTmp, "UPDATE Products SET State='W' WHERE (Prefix='S%s')", myCounty.acCntyCode);
      updateTable(acTmp);
   }
   if (iLoadFlag & (LOAD_ASSR|UPDT_ASSR))
   {
      sprintf(acTmp, "UPDATE Products SET State='W' WHERE (Prefix='%s')", myCounty.acCntyCode);
      updateTable(acTmp);
   }

   // Retrieve last record count
   iTmp = getCountyInfo() ;
   if (iTmp < 0)
   {
      close_log();
      return 1;
   }
   if (myCounty.iMaxUpdate > 0)
      iMaxChgAllowed = myCounty.iMaxUpdate;

   if (lOptExtr & EXTR_ZONE)
   {                              
      GetIniString("Data", "ZoneExp", "", acTmp, _MAX_PATH, acIniFile);
      sprintf(acOutFile, acTmp, myCounty.acCntyCode, myCounty.acCntyCode, "dat");

      iRet = extrZoning(myCounty.acCntyCode, acOutFile);   
      goto LoadOne_Exit;
   }

   // Open export file
   if (bExpSql && (iLoadTax == TAX_LOADING || iLoadTax == TAX_UPDATING))
   {
      iRet = OpenTaxExpSql(myCounty.acCntyCode, "TaxUpd", "w");
      if (iRet)
         return iRet;
   } else
      bExpSql = false;

   // Special process
   if (!_stricmp(myCounty.acCntyCode, "ALA"))
   {
      iRet = loadAla(1);
   } else if (!_stricmp(myCounty.acCntyCode, "CCX"))
   {
      iRet = loadCcx(1);
   //} else if (!_stricmp(myCounty.acCntyCode, "EDX"))
   //{
   //   iRet = loadEdx(1);
   } else if (!_stricmp(myCounty.acCntyCode, "FRE"))
   {
      iRet = loadFre(1);
   } else if (!_stricmp(myCounty.acCntyCode, "KER"))
   {
      iRet = loadKer(1);
   //} else if (!_stricmp(myCounty.acCntyCode, "KIN"))
   //{
   //   iRet = loadKin(1);
   } else if (!_stricmp(myCounty.acCntyCode, "LAX"))
   {
      iRet = loadLax(1);
   //} else if (!_stricmp(myCounty.acCntyCode, "MEN"))
   //{
   //   iRet = loadMen(1);
   //} else if (!_stricmp(myCounty.acCntyCode, "MPA"))
   //{
   //   iRet = loadMpa(1);
   } else if (!_stricmp(myCounty.acCntyCode, "MRN"))
   {
      iRet = loadMrn(1);
   } else if (!_stricmp(myCounty.acCntyCode, "ORG"))
   {
      iRet = loadOrg(1);
   } else if (!_stricmp(myCounty.acCntyCode, "RIV"))
   {
      iRet = loadRiv(1);
   } else if (!_stricmp(myCounty.acCntyCode, "SAC"))
   {
      iRet = loadSac(1);
   } else if (!_stricmp(myCounty.acCntyCode, "SBD"))
   {
      iRet = loadSbd(1);
   } else if (!_stricmp(myCounty.acCntyCode, "SBX"))
   {
      iRet = loadSbx(1);
   } else if (!_stricmp(myCounty.acCntyCode, "SCL"))
   {
      iRet = loadScl(1);
   } else if (!_stricmp(myCounty.acCntyCode, "SCR"))
   {
      iRet = loadScr(1);
   } else if (!_stricmp(myCounty.acCntyCode, "SDX"))
   {
      iRet = loadSdx(1);
   } else if (!_stricmp(myCounty.acCntyCode, "SFX"))
   {
      iRet = loadSfx(1);
   } else if (!_stricmp(myCounty.acCntyCode, "SLO"))
   {
      iRet = loadSlo(1);
   } else if (!_stricmp(myCounty.acCntyCode, "SMX"))
   {
      iRet = loadSmx(1);
   } else if (!_stricmp(myCounty.acCntyCode, "SOL"))
   {
      iRet = loadSol(1);
   //} else if (!_stricmp(myCounty.acCntyCode, "SUT"))
   //{
   //   iRet = loadSut(1);
   //} else if (!_stricmp(myCounty.acCntyCode, "TUL"))
   //{
   //   iRet = loadTul(1);
   } else if (!_stricmp(myCounty.acCntyCode, "VEN"))
   {
      iRet = loadVen(1);
   } else
   {
      iRet = -1;
      LogMsgD("***** Invalid county code.  Please check with Sony for correct version *****");
   }

   if (bExpSql)
   {
      char sFile1[_MAX_PATH], sFile2[_MAX_PATH], sRootDir1[64], sRootDir2[64];
      
      CloseTaxExpSql();

      sprintf(sFile1, sTaxSqlTmpl, myCounty.acCntyCode,  "TaxUpd");
      iRet = getFileSize(sFile1);
      if (iRet < 100)
      {
         // Remove empty file and clear tax loading flag
         LogMsg("Remove empty tax sql file: %s", sFile1);
         DeleteFile(sFile1);
         iLoadTax = 0;
         iRet = 0;
      } else
      {
         iRet = GetIniString("Data", "WRoot", "", sRootDir1, 256, acIniFile);
         iRet = GetIniString("Data", "LRoot", "", sRootDir2, 256, acIniFile);
         if (iRet > 1)
         {
            sprintf(sFile2, sTaxSqlTmpl, myCounty.acCntyCode,  "TaxUpdL");
            iRet = ConvertTaxSqlFile(sFile1, sFile2, sRootDir1, sRootDir2, "L");
         }

         iRet = GetIniString("Data", "BRoot", "", sRootDir2, 256, acIniFile);
         if (iRet > 0)
         {
            sprintf(sFile2, sTaxSqlTmpl, myCounty.acCntyCode,  "TaxUpdB");
            iRet = ConvertTaxSqlFile(sFile1, sFile2, sRootDir1, sRootDir2, "B");
         }
      }
   }

   // Set state='F' if fail, 'P' for data processed and ready for product build
   // When load failed, stop all other events by setting iLoadFlag=0
   acTmp[0] = 0;
   if (iRet)
   {
      if (iLoadFlag & (LOAD_ASSR|UPDT_ASSR))
      {
         sprintf(acTmp, "UPDATE Products SET State='F' WHERE (Prefix='%s')", myCounty.acCntyCode);
         updateTable(acTmp);
      }

      if (iLoadFlag & (LOAD_LIEN|LOAD_UPDT))
      {
         sprintf(acTmp, "UPDATE Products SET State='F' WHERE (Prefix='S%s')", myCounty.acCntyCode);
         updateTable(acTmp);
      }

      if (!bGrGrAvail)
      {
         // send email
         if (bSendMail)
         {
            if (iLoadFlag & LOAD_DAILY)
               sprintf(acSubj, "*** %s - Daily file is not available.", myCounty.acCntyCode);
            else
               sprintf(acSubj, "*** %s - No new GRGR file.", myCounty.acCntyCode);
            sprintf(acBody, "Please review log file \"%s\" for more info", acLogFile);
            mySendMail(acIniFile, acSubj, acBody, NULL);
         }
      } else if (bSendMail)
      {
         if (iRet == 99)
         {
            sprintf(acSubj, "*** %s TaxDist needs update", myCounty.acCntyCode);
            sprintf(acBody, "%s_TaxDist.txt file is not up to date.  Please check log file and have it updated", myCounty.acCntyCode);
         } else if (iRet == CNTY_MPA)
         {
            sprintf(acSubj, "*** %s: Tax files need conversion before processing", myCounty.acCntyCode);
            sprintf(acBody, "Current LOADONE can not process XLS.  Please convert it to XLSX before processing.", myCounty.acCntyCode);
         } else
         {
            sprintf(acSubj, "*** Error loading %s", myCounty.acCntyCode);
            sprintf(acBody, "%s\nSee %s for more info", getLastErrorLog(), acLogFile);
         }
         mySendMail(acIniFile, acSubj, acBody, NULL);
         iLoadFlag = 0;
      }
   } else if (iLoadTax == TAX_LOADING || iLoadTax == TAX_UPDATING)
   {
      GetIniString("Mail", "MailChgTo", "", acTmp, 256, acIniFile);
      if (iLoadTax == TAX_LOADING)
         sprintf(acSubj, "Loading county tax for %s", myCounty.acCntyCode);
      else
         sprintf(acSubj, "Updating county tax for %s", myCounty.acCntyCode);
      sprintf(acBody, "Tax has been loaded.");
      if (bSendMail)
         mySendMail(acIniFile, acSubj, acBody, acTmp);
   } else
   {
      // Populate PQZoning with county zoning
      GetIniString(myCounty.acCntyCode, "SetZoning", "N", acTmp, _MAX_PATH, acIniFile);
      if (acTmp[0] == 'Y')
         PQ_SetDefaultPQZoning(myCounty.acCntyCode);

      // Merge zoning
      if (iLoadFlag & MERG_ZONE)                      // -Mz
      {
         iRet = MergeZoning(myCounty.acCntyCode, iSkip);
      }

      // Merge final value
      if (lOptMisc & M_OPT_MERGVAL)                   // -Mf
         iRet = PQ_MergeValueExt(myCounty.acCntyCode, 0, iSkip);

      // Update acreage using GIS data
      if (iLoadFlag & MERG_GISA)                      // -Mr
      {
         GetPrivateProfileString(myCounty.acCntyCode, "OverwriteSqft", "N", acTmp, 10, acIniFile);
         if (acTmp[0] == 'Y')
            iRet = PQ_MergeLotArea(myCounty.acCntyCode, true);
         else
            iRet = PQ_MergeLotArea(myCounty.acCntyCode, false);
      }

      if (iLoadFlag & (LOAD_LIEN|LOAD_UPDT|MERG_GRGR|LOAD_DAILY))
      {
         // Check for bad character
         GetPrivateProfileString(myCounty.acCntyCode, "FixBadChar", "Y", acTmp, 10, acIniFile);
         if (acTmp[0] == 'Y')
            iTmp = PQ_ChkBadR01(myCounty.acCntyCode, acRawTmpl, iRecLen, ' ');

         // Copy file for assessor
         sprintf(acOutFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "R01");
         GetIniString(myCounty.acCntyCode, "AsrFile", "", acTmpFile, _MAX_PATH, acIniFile);
         if (acTmpFile[0] > ' ')
         {
            LogMsg("Copying file %s ==> %s", acOutFile, acTmpFile);
            iTmp = CopyFile(acOutFile, acTmpFile, false);
            if (!iTmp)
            {
               LogMsg("***** Fail copying file %s ==> %s", acOutFile, acTmpFile);
               iLoadFlag = 0;
            } else
            {
               iLoadFlag |= UPDT_ASSR;
               lAssrRecCnt = lRecCnt;
            }
         }

         // Copy roll for assessor
         GetIniString(myCounty.acCntyCode, "CopyRoll", "", acOutFile, _MAX_PATH, acIniFile);
         if (acOutFile[0] > ' ')
         {
            LogMsg("Copying file %s ==> %s", acRollFile, acOutFile);
            iTmp = CopyFile(acRollFile, acOutFile, false);
            if (!iTmp)
            {
               LogMsg("***** Fail copying file %s ==> %s", acRollFile, acOutFile);
               iLoadFlag = 0;
            } else
            {
               iLoadFlag |= UPDT_ASSR;
               lAssrRecCnt = lRecCnt;
            }
         }

         if ((lRecCnt > (myCounty.iLastRecCnt-((myCounty.iLastRecCnt/100)*5)))
            || (iLoadFlag & LOAD_LIEN) )
         {
            int iRecChg=-1;

            // Generate update file
            if ((!bDontUpd || bCreateUpd) && (iLoadFlag & (LOAD_UPDT|LOAD_DAILY)))
            {
               GetPrivateProfileString(myCounty.acCntyCode, "CreateAPNFile", "N", acTmp, _MAX_PATH, acIniFile);
               // Force comparing full 14 bytes of APN
               if (acTmp[0] == 'Y')
                  iRecChg = chkS01R01(myCounty.acCntyCode, acRawTmpl, iRecLen, 14, bCreateUpd, true);
               else
                  iRecChg = chkS01R01(myCounty.acCntyCode, acRawTmpl, iRecLen, 14, bCreateUpd, false);

               if (iRecChg > 0)
               {
                  // Store number of recs changed to County table
                  sprintf(acTmp, "UPDATE County SET LastRecsChg=%d WHERE (CountyCode='%s')", iRecChg, myCounty.acCntyCode);
                  updateTable(acTmp);

                  // If number of changed records greater than allowed, we have to import manually
                  if (iRecChg > iMaxChgAllowed)
                  {
                     // Email production
                     LogMsg("*** WARNING: Number of changed records is too big for %s (%d) ***", myCounty.acCntyCode, iRecChg);
                     LogMsg("***          Please run CDDEXTR -Q -TWEBIMPORT -C%s ***", myCounty.acCntyCode);

                     GetIniString("Mail", "MailChgTo", "", acTmp, 256, acIniFile);
                     //LogMsg("Send email to %s", acTmp);

                     sprintf(acSubj, "LoadOne [%s] - %d recs changed.", myCounty.acCntyCode, iRecChg);
                     sprintf(acBody, "Msg from %s.\nSee log file Load_%s.log for more info.", acVersion, myCounty.acCntyCode);
                     if (bSendMail)
                        mySendMail(acIniFile, acSubj, acBody, acTmp);
                  }
               } 
            }

            if (iRecChg)
            {
               // Update Products table
               sprintf(acTmp, "UPDATE Products SET LastRecsCount=%d,State='P' WHERE (Prefix='S%s')", lRecCnt, myCounty.acCntyCode);
               updateTable(acTmp);

               // Update CtyProfiles
               sprintf(acTmp, "UPDATE ctyProfiles SET LastBldDate=%s,LastRecCount=%d,LastRecDate=%d,State='P' WHERE (CountyCode='%s')", acToday, lRecCnt, lLastRecDate, myCounty.acCntyCode);
               updateTable(acTmp);
            }

            // Update county table
            char sGrGrDt[32], sRollFileDt[32], sTaxFileDt[32];
            sGrGrDt[0] = 0;
            sRollFileDt[0] = 0;
            sTaxFileDt[0] = 0;

            if (lLastFileDate > 0)
               sprintf(sRollFileDt, ",LastFileDate=%d", lLastFileDate);
            if (lLastGrGrDate > 0)
               sprintf(sGrGrDt, ",LastGrGrDate=%d", lLastGrGrDate);

            // LastFileDate is set by ChkCnty.  But we provide option to do it here for manual run
            if (iLoadFlag & LOAD_LIEN)
               sprintf(acTmp, "UPDATE County SET LastBldDate=%s,LastRecCount=%d,LDRRecCount=%d,LastRecDate=%d %s %s %s WHERE (CountyCode='%s')", 
                  acToday, lRecCnt, lLDRRecCount, lLastRecDate, sGrGrDt, sRollFileDt, sTaxFileDt, myCounty.acCntyCode);
            else if (bSetLastFileDate)
               sprintf(acTmp, "UPDATE County SET LastBldDate=%s,LastRecCount=%d,LastRecDate=%d %s %s %s WHERE (CountyCode='%s')", 
                  acToday, lRecCnt, lLastRecDate, sGrGrDt, sRollFileDt, sTaxFileDt, myCounty.acCntyCode);
            else
               sprintf(acTmp, "UPDATE County SET LastBldDate=%s,LastRecCount=%d,LastRecDate=%d WHERE (CountyCode='%s')", 
                  acToday, lRecCnt, lLastRecDate, myCounty.acCntyCode);
            updateTable(acTmp);

            if (bDupApn && bSendMail)
            {
               sprintf(acSubj, "*** LoadOne [%s] - Duplicate parcel found.", myCounty.acCntyCode);
               sprintf(acBody, "Please review log file \"%s\" for more info", acLogFile);
               mySendMail(acIniFile, acSubj, acBody, NULL);
            }
         } else if (lRecCnt > 0)
         {
            sprintf(acSubj, "*** LoadOne WARNING: Number of records is too small for %s (%d ~ %d) ***", myCounty.acCntyCode, lRecCnt, myCounty.iLastRecCnt);
            LogMsg(acSubj);
            GetIniString("Mail", "PhoneTech", "Sony 714-247-9732", acTmp, 64, acIniFile);
            sprintf(acBody, "Msg from %s\nInput file may be corrupted.  Please check FTP folder to verify file size or call %s", acVersion, acTmp);

            // Send mail
            if (bSendMail)
            {
               bSendMail = false;
               mySendMail(acIniFile, acSubj, acBody, NULL);
            }
            bDontUpd = true;
         }

         if (!bGrGrAvail)
         {
            // send email
            if (bSendMail)
            {
               if (iLoadFlag & LOAD_DAILY)
                  sprintf(acSubj, "*** LoadOne [%s] - Daily file is not available.", myCounty.acCntyCode);
               else
                  sprintf(acSubj, "*** LoadOne [%s] - No new GRGR file.", myCounty.acCntyCode);
               sprintf(acBody, "Please review log file \"%s\" for more info", acLogFile);
               mySendMail(acIniFile, acSubj, acBody, NULL);
            }
         }

         if (iMaxLegal > 0)
            LogMsg("Max legal length:           %u", iMaxLegal);
      }

      if (iLoadTax > 0 && lLastTaxFileDate > 0)
      {
         LogMsg("Update Tax loading info");
         if (bTaxImport)
            sprintf(acTmp, "UPDATE County SET LastTaxImpDate=%s,LastTaxFileDate=%d WHERE (CountyCode='%s')", acToday, lLastTaxFileDate, myCounty.acCntyCode);
         else
            sprintf(acTmp, "UPDATE County SET LastTaxFileDate=%d WHERE (CountyCode='%s')", lLastTaxFileDate, myCounty.acCntyCode);
         updateTable(acTmp);
      }

      if (iLoadFlag & (LOAD_ASSR|UPDT_ASSR))
      {
         if (lAssrRecCnt > 0)
         {
            sprintf(acTmp, "UPDATE Products SET LastRecsCount=%d,State='P' WHERE (Prefix='%s')", lAssrRecCnt, myCounty.acCntyCode);
            updateTable(acTmp);
         } else
         {
            iTmp = 0;
            while (asRecCnt[iTmp].RecCnt > 0 && iTmp < MAX_REC_CNT)
            {
               sprintf(acTmp, "UPDATE Products SET LastRecsCount=%d,State='P' WHERE (Prefix='%s')", asRecCnt[iTmp].RecCnt, asRecCnt[iTmp].Prefix);
               updateTable(acTmp);
               iTmp++;
            }
         }
      }

      if (iLoadFlag & EXTR_REGN)
      {
         if (alRegCnt[1] > 0)
         {
            sprintf(acTmp, "UPDATE Products SET LastRecsCount=%d,State='P' WHERE (Prefix='S%.2s1')", alRegCnt[1], myCounty.acCntyCode);
            updateTable(acTmp);
         }
         if (alRegCnt[2] > 0)
         {
            sprintf(acTmp, "UPDATE Products SET LastRecsCount=%d,State='P' WHERE (Prefix='S%.2s2')", alRegCnt[2], myCounty.acCntyCode);
            updateTable(acTmp);
         }
         if (alRegCnt[3] > 0)
         {
            sprintf(acTmp, "UPDATE Products SET LastRecsCount=%d,State='P' WHERE (Prefix='S%.2s3')", alRegCnt[3], myCounty.acCntyCode);
            updateTable(acTmp);
         }
      }
   }

   // Reset county status
   if (!iLoadTax)
   {
      sprintf(acTmp, "UPDATE County SET Status='R' WHERE (CountyCode='%s')", myCounty.acCntyCode);
      updateTable(acTmp);
   }

   // Create flag file
   if (!iRet && CreateFlgFile(myCounty.acCntyCode, acIniFile))
      LogMsg("***** Error creating flag file *****");

   // Import sale file
   if (iLoadFlag & EXTR_ISAL)
   {
      char sDbName[64];

      if (iLoadFlag & LOAD_LIEN)
         sprintf(sDbName, "LDR%d", lLienYear);
      else
         sprintf(sDbName, "UPD%d", lLienYear);

      GetIniString("Data", "SqlSaleFile", "", acTmp, _MAX_PATH, acIniFile);
      sprintf(acOutFile, acTmp, sDbName, myCounty.acCntyCode);
      GetIniString(myCounty.acCntyCode, "UseDoc", "N", acTmp, _MAX_PATH, acIniFile);

      if (acTmp[0] == 'Y')
      {
         if (!memcmp(myCounty.acCntyCode, "FRE", 3))
            iTmp = createSaleImport(Fre_MakeDocLink, myCounty.acCntyCode, acCSalFile, acOutFile, 1, false);
         else if (!memcmp(myCounty.acCntyCode, "KER", 3))
            iTmp = createSaleImport(Ker_MakeDocLink, myCounty.acCntyCode, acCSalFile, acOutFile, 1, false);
      } else
         iTmp = createSaleImport(myCounty.acCntyCode, acCSalFile, acOutFile, 1, false);

      if (iTmp > 0 && bSaleImport)
         iTmp = doSaleImportEx(myCounty.acCntyCode, sDbName, 1);
      else
         iTmp = 0;

      // send email
      if (iTmp && bSendMail)
      {
         sprintf(acSubj, "*** LoadOne [%s] - Error importing sale file.", myCounty.acCntyCode);
         sprintf(acBody, "Please review log file \"%s\" for more info", acLogFile);
         mySendMail(acIniFile, acSubj, acBody, NULL);
      }
   }

   // Import GrGr file
   if (iLoadFlag & EXTR_IGRGR)
   {
      char sDbName[64];

      if (iLoadFlag & LOAD_LIEN)
         sprintf(sDbName, "LDR%d", lLienYear);
      else
         sprintf(sDbName, "UPD%d", lLienYear);

      GetIniString("Data", "SqlGrgrFile", "", acTmp, _MAX_PATH, acIniFile);
      sprintf(acOutFile, acTmp, sDbName, myCounty.acCntyCode);
      sprintf(acCSalFile, acGrGrTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "sls");
      GetIniString(myCounty.acCntyCode, "GrgrDef", "N", acTmp, _MAX_PATH, acIniFile);
      if (acTmp[0] == 'Y')
         iTmp = createSaleImport(NULL, myCounty.acCntyCode, acCSalFile, acOutFile, TYPE_GRGR_DEF, false);
      else if (!memcmp(myCounty.acCntyCode, "ORG", 3))
         iTmp = createSaleImport(NULL, myCounty.acCntyCode, acCSalFile, acOutFile, TYPE_GRGR_ORG, false);
      else
         iTmp = createSaleImport(NULL, myCounty.acCntyCode, acCSalFile, acOutFile, TYPE_GRGR_DOC, false);

      if (iTmp > 0 && bGrgrImport)
         iTmp = doSaleImportEx(myCounty.acCntyCode, sDbName, 2);
         //iTmp = doSaleImport(myCounty.acCntyCode, sDbName, acOutFile, 2);
      else
         iTmp = 0;
   
      // send email
      if (iTmp && bSendMail)
      {
         sprintf(acSubj, "*** LoadOne [%s] - Error importing GrGr file.", myCounty.acCntyCode);
         sprintf(acBody, "Please review log file \"%s\" for more info", acLogFile);
         mySendMail(acIniFile, acSubj, acBody, NULL);
      }
   }

   // Import Value file - Remove 7/4/2020 (spn) since value table can be updated using UV?.CMD
   if (iLoadFlag & EXTR_IVAL)
   {
      LogMsg0("*** Please use UV<n>.CMD command to update value table. ***");
      //iTmp = doValueImport(myCounty.acCntyCode, acValueFile);

      //// send email
      //if (iTmp && bSendMail)
      //{
      //   sprintf(acSubj, "*** LoadOne [%s] - Error importing Value file.", myCounty.acCntyCode);
      //   sprintf(acBody, "Please review log file \"%s\" for more info", acLogFile);
      //   mySendMail(acIniFile, acSubj, acBody, NULL);
      //}
   }

LoadOne_Exit:
   // Remove temp files
   if (!iRet && bRemTmpFile)
      iRet = RemoveTempFiles(myCounty.acCntyCode, acTmpPath);

   // Clean up memory allocation
   if (iNumUseCodes > 0 && pUseTable)
      delete pUseTable;

   // Close log
   closeUsecodeLog();
   close_log();

   return iRet;
}
