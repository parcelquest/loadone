/*****************************************************************************
 *
 * Notes: I have gone through the document number processing for CCX.  I ended
 *        up using two formats.  One format is for the old style recordings
 *        (Book/Page); although I had to fix that logic.  The second is for
 *        the new style recordings (document number).  The break point occurs
 *        in 1994; prior to that year the old system was used.
 *
 *        - Use Deed Ref from roll/lien file to populate TRANSFER.  Sale data is to 
 *          apply to sale1-3 only.
 *        - Extract CHAR from sale file and apply to R01 where CHAR file is missing.
 *        - Use -Xa option to extract convert CHAR file to STDCHAR format.
 *        - New roll file SRM2.TXT is the same format as lien file. 12/12/2013
 *        - CCX uses chars from both pdrfile.txt & salfile.txt.  Chars from salfile.txt
 *          overwrites data from pdrfile.txt
 *
 * Problems:
 * 07/13/2011 Some how first byte of acESalTmpl is overwritten, need investigation
 *
 * History:
 * 01/09/2006 1.1.8.1   First CCX production release
 * 07/19/2006 1.2.27.5  Adding BsmtSqft to BldgSqft to make it matched with
 *                      Fernando calculation.
 * 02/14/2008 1.5.5     Adding support for Stanrd Usecode
 * 03/24/2008 1.5.7.2   Format IMAPLINK.
 * 08/07/2008 8.0.8     Fix Ccx_ExtrLien()
 * 01/17/2009 8.5.6     Fix TRA
 * 02/12/2009 8.5.10    Adding -Mo option to merge Other Values to R01 file.  
 *                      Update Ccx_MergeRoll() to populate PP_Val field too.
 * 03/16/2009 8.7.0     Format STORIES as 99.0.  We no longer multiply by 10.
 * 05/08/2009 8.7.1     Set full exemption flag
 * 07/08/2009 9.1.1     Add Ccx_CreateHistSale() to create history sale file ccx_sale_cum.srt
 * 12/10/2009 9.3.5     Add Ccx_MergeProp8() to merge prop8 data into roll file
 * 12/23/2009 9.3.6     Add Ccx_SetProp8() to update prop8 flag in county table
 *                      and Ccx_ExtractProp8() to extract all prop8 APN for import into SQL
 *                      to set Prop8_Flg in county table.  This method is much faster but
 *                      require some manual labor (import then update).
 *                      i.e. update ccx_200904 set prop8_flg='Y' where exists (select prop8 from ccx_prop8 where apn=prop8)
 * 02/06/2010 9.4.0     Extending LEGAL to LEGAL1
 * 05/17/2010 9.6.1     Store Tax_CodeA & Tax_CodeB in TAX_CODE.
 * 07/18/2010 10.1.4    Add CCX_MergeLien(), Ccx_MergeLdrSAdr(), Ccx_MergeLdrMAdr(); 
 *                      Modify Ccx_Load_LDR(), Ccx_Load_Roll(), Ccx_MergeRoll(), Ccx_MergeOwner()
 *                      due to the new LDR file format.  Change logic to update TRANSFER
 *                      using roll data only.
 * 08/03/2010 10.1.9    Change logic that set FULL_EXEMPT flag.  Now if GROSS=TOTAL_EXE, set flag to 'Y'
 * 10/29/2010 10.2.0    Ignore all saleamt that is less than $10.
 * 02/02/2011 10.4.2    Add Ccx_LdrUpdateSale() to update sale data for specific LDR year.
 *                      Clear old sales before applying history sale.
 * 02/05/2011 10.4.2.1  Fix bug in CCX_Load_Roll(), do ClearOldSales() before calling
 *                      Ccx_LoadRoll() since this function is also called ClearOldSales().
 * 02/06/2011 10.4.2.2  Change merge sale logic:
 *                         - Use CUR_DEED from roll file to update TRANSFER.  Use roll data
 *                           to update sale for compatible with prev. version
 *                         - When there is history sale, remove all cursales before applying.
 *
 * 02/14/2011 10.4.2.3  Add function CCX_ConvertHistSale() to convert sac_sale_newcum.srt to SCSALE format
 * 03/14/2011 10.4.5    Extract sale1 from roll file and append to cum sale file.  Use ApplyCumSale()
 *                      to apply sales.
 * 03/15/2011 10.4.5.3  Calling ApplyCumSale() with ClearSaleFlg=2 to clear roll sale only
 *                      if there is sales from cum sale file.
 * 04/18/2011 10.5.5    Add Ccx_MatchSaleApn() to populate cum sale with roll's APN.
 * 04/21/2011 10.5.6    Remove Ccx_MatchSaleApn() and use Sale_MatchRoll() instead.
 * 05/09/2011 10.5.8    Change param in ApplyCumSale() to resort cum sale file before merged.
 * 05/23/2011 10.5.13   Fix bad character in Ccx_MergeOwner().
 * 06/17/2011 10.6.0    Modify Ccx_MergeSAdr() and Ccx_MergeLdrSAdr() to add long StrNum.
 * 06/20/2011 10.6.1.1  If S_STRNUM is not available and mailing has same StrName and HO_FL=1, copy M_STRNUM to S_STRNUM.
 * 07/13/2011 11.0.3    Modify Ccx_MergeCChr() to loop through all entries (sorted by descending entry date).
 *                      Modify Ccx_FormatSaleChar() and add SaleDate to CCX_CCHR, Ccx_UpdateCumSale() to
 *                      allow multiple entries of same APN in CCHAR file. Remove sale update
 *                      from Ccx_Load_LDR(). Modify Ccx_ExtrSaleFromRoll() to keep full APN and
 *                      set ApnMatched flag to 'Y'. Modify Ccx_ExtrRSale() to take output file from
 *                      caller. Do Sale_MatchRoll() only after successful update to cum sales file.
 *                      Remove sort flag from ApplyCumSale() params.
 * 08/18/2011 11.1.10   Add log msg to Ccx_ExtractProp8().
 * 03/07/2012 11.6.8    Sort update roll before processing.  Scan for bad char in legal.
 * 07/19/2012 12.1.2    Modify Ccx_MergeOwner() to keep original owner in NAME1.
 * 07/13/2013 13.0.1    Adding BsmtSqft to R01.  Remove unprintable char from Owner and Legal.
 * 10/02/2013 13.10.0   Use updateVesting() to update Vesting and Etal flag.
 * 10/19/2013 13.11.5   Remove unused function Ccx_UpdateCumChar() and add Ccx_ConvStdChar() for -Xa option.
 * 10/21/2013 13.11.5.1 Fix Ccx_FmtChar() to format APN with 9 digits instead of 10.
 * 12/12/2013 13.11.12  Modify Ccx_MergeSAdr() & Ccx_MergeMAdr() to work with new & old roll file
 *                      and to replace Ccx_MergeLdrSAdr & Ccx_MergeLdrMAdr().  Modify Ccx_MergeRoll()
 *                      and Ccx_MergeLien() to use new params to merge addr.  Add Ccx_MergeSrm()  
 *                      and Ccx_ExtrSaleFromSrm() to work with new roll format SRM version 2.
 * 05/21/2014 13.14.5   Add comments and reorganize code in loadCcx().
 * 03/12/2015 14.12.4   Fix BSMT_SQFT & GAR_SQFT bug in Ccx_MergeChar() and Ccx_FormatChar()
 * 07/07/2015 15.0.2    Add comments
 * 01/12/2016 15.4.2    Add comment, modify Suffix_Xlat.txt to fix CCX suffix problem.
 * 03/26/2016 15.8.1    Move dispError() to Logs.cpp
 * 07/19/2016 16.1.0    Modify Ccx_MergeMAdr() to remove known bad char.  
 *                      Modify Ccx_UpdateCumSale() to merge cum char output to keep history of change.
 * 08/09/2016 16.2.0    Turn ON MERG_CSAL after successfully load sale file.
 * 11/15/2016 16.7.0    Modify Ccx_UpdateCumSale() to update multi-sale flag and rename to Ccx_ExtrSaleFromSalFile()
 *                      Modify Ccx_FormatSale() to set SaleCode to "F"ull or "P"artial and set 
 *                      MultiSale_Flg='Y' if reject_code='3'
 * 12/19/2016 16.8.3    Set VIEW='A' when HasView='Y'
 * 07/04/2017 17.1.1    Modify Ccx_MergeOwner() to update DBA.  Fix BldgSqft in Ccx_FmtChar() by
 *                      adding BsmtSqft to BldgSqft.
 * 10/15/2017 17.3.1    Add tax loading functions.
 * 11/21/2017 17.5.0    Remove unused code.
 * 01/15/2018 17.6.1    Change Sort comand in import Agency to remove empty entry.
 * 04/07/2018 17.8.3    Add Ccx_UpdatePst() to process daily update tax file.  Modify Ccx_Load_TaxBase()
 *                      to allow daily update.  Modify Ccx_Load_TaxDelq() to remove duplicate.
 *                      Add Ccx_UnzipTaxUpdate() to unzip daily info file.
 * 05/07/2018 17.10.8   Modify loadCcx() to get CHAR definition regardless -Xa is used or not.
 * 05/14/2018 17.10.9   Ignore records with unknown TaxCode.
 *            17.10.10  Fix bug in Ccx_Load_TaxBase() that didn't populate paid date.
 * 06/12/2018 17.12.1   Modify Ccx_UnzipTaxUpdate() to unzip previous day file if current day is not available.
 * 10/14/2018 18.5.3    Add Ccx_Load_TaxBase_PTS() to load tax base from daily PTS file instead of Cortac.
 *                      Replace BSrchTaxDist() with BSrchTaxDistName() to search on exact name.
 *                      Modify Ccx_UnzipTaxUpdate() to unzip file only when there is new file.
 * 12/08/2018 18.6.9    Modify Ccx_ParsePTS() to add isSecd & isSupp to TaxBase.
 * 04/12/2019 18.11.2   Modify Ccx_MergeLdrMAdr() & Ccx_MergeMAdr() to populate M_UNITNOX
 * 04/21/2019 18.11.6   Fix UNITNO in Ccx_MergeSAdr() & Ccx_MergeMAdr()
 * 11/05/2019 19.4.1    Modify Ccx_Load_TaxDetail() to update tb_TotalRate.
 * 03/04/2020 19.8.0    Add -Mz to merge zoning data and modify Ccx_MergeSAdr() to handle special case
 *                      for situs suffix "PARK" where it is not a suffix, but part of street name.
 * 06/18/2020 20.0.0    Add Exemption codes to R01 file
 *                      Modify Ccx_MergeSrm() & Ccx_CreateLienRec() to copy exemption code even without exe amt.
 * 07/21/2020 20.2.2    Modify Ccx_MergeSrm() to clear out ZONE before apply new one.
 * 08/10/2020 20.2.8    Fix null char in Ccx_MergeLien(), replacing known bad char.
 * 10/15/2020 20.3.0    Move -Mz processing to LoadOne.cpp
 * 10/28/2020 20.3.6    Modify Ccx_MergeChar(), Ccx_MergeSrm(), & Ccx_MergeLien() to populate PQZoning.
 * 03/22/2021 20.7.11   Remove output folder check in Ccx_Load_TaxDelq().
 * 06/26/2021 21.0.0    Modify Ccx_MergeLien() & Ccx_MergeRoll() to add ALT_APN.
 * 02/27/2023 22.4.7    Modify Ccx_Load_TaxBase_PTS() to support new PTS file which
 *                      may have header and first row stick together.
 * 03/23/2023 22.6.0    Use NameTaxCsvFile() to form output tax file names for import.
 * 04/14/2023 22.6.2    Fix UNIT# in Ccx_MergeMAdr().
 * 05/25/2023 22.8.1    Modify Ccx_Load_TaxDetail() to check for input file.  If not exist, skip it.
 *                      Modify Ccx_Load_TaxDelq() to check for input file.  If not exist, skip it.
 *                      Add Ccx_Load_TaxSupp_SPT() & Ccx_ParseSPT() to load tax supplement.
 * 12/15/2023 23.4.5    Modify Ccx_ParseSPT() to fix DueDate.
 * 12/16/2023 23.4.6    Add -Xn and -Mn to support ATTOM sale.
 * 01/31/2024 23.5.8    Modify Ccx_MergeMAdr() to fix mail unit issue.
 * 02/01/2024 23.6.0    Use ApplyCumSaleNR() to update NDC sale instead of ApplyCumSale().
 * 02/20/2024 23.6.2    Modify Ccx_ExtrSaleFromSalFile() to display last sale date from county sale file.
 * 07/06/2024 24.0.0    Modify Ccx_MergeLien(), Ccx_MergeSrm(), Ccx_MergeRoll() to add exemption type.
 *
 *****************************************************************************/

#include "stdafx.h"
#include "CountyInfo.h"
#include "Prodlib.h"
#include "R01.h"
#include "RecDef.h"
#include "Tables.h"
#include "Logs.h"
#include "LoadDef.h"
#include "Utils.h"
#include "XlatTbls.h"
#include "doOwner.h"
#include "doSort.h"
#include "doGrGr.h"
#include "doZip.h"
#include "formatApn.h"
#include "SaleRec.h"
#include "LoadOne.h"
#include "MergeCcx.h"
#include "UseCode.h"
#include "LOExtrn.h"
#include "PQ.h"
#include "hlAdo.h"
#include "CharRec.h"
#include "Tax.h"
#include "NdcExtr.h"

static   FILE     *fdChar, *fdCChr;
static   long     lCharSkip, lSaleSkip, lCChrSkip;
static   long     lCharMatch, lSaleMatch, lCChrMatch, lPstMatch, iSaleApnLen;
static   int      iSrmVersion;
extern   hlAdo    dbLoadConn;

/******************************* Ccx_ExtrTaxDist *****************************
 *
 * Return number of district if successful, <0 if error
 *
 *****************************************************************************/

int Ccx_ExtrTaxDist(char *pCnty)
{
   char  acDistMaster[_MAX_PATH], acTaxDist[_MAX_PATH], acTmpDist[_MAX_PATH], acInbuf[256], acOutbuf[256], *pTmp;
   int   iRet, iCnt=0;

   FILE *fdInput, *fdOutput;
   DISTMSTR *pDistMstr = (DISTMSTR *)acInbuf;

   GetIniString(pCnty, "DistMstr", "", acDistMaster, _MAX_PATH, acIniFile);
   if (_access(acDistMaster, 0))
   {
      LogMsg("***** Missing Dist Master file: %s", acDistMaster);
      return -1;
   }
   sprintf(acTmpDist, "%s\\%s\\%s_TaxDist.txt", acTmpPath, pCnty, pCnty);

   fdInput = fopen(acDistMaster, "r");
   fdOutput = fopen(acTmpDist, "w");
   fputs("Code|Agency|Phone|TaxRate|TaxAmt|TCFlg\n", fdOutput);

   while (!feof(fdInput))
   {
      pTmp = fgets(acInbuf, 256, fdInput);
      if (!pTmp)
         break;

      sprintf(acOutbuf, "%.*s|%.*s||||0\n", SIZ_DM_DIST, pDistMstr->Dist, SIZ_DM_DESC, pDistMstr->Desc);
      fputs(acOutbuf, fdOutput);
      iCnt++;
   }

   fclose(fdInput);
   fclose(fdOutput);

   // Sort & dedup output
   if (iCnt > 0)
   {
      GetIniString(pCnty, "TaxDist", "", acTaxDist, _MAX_PATH, acIniFile);
      iCnt = sortFile(acTmpDist, acTaxDist, "S(#1,C,A) DUPO DEL(124)", &iRet);
   }

   return iCnt;
}

/******************************** Ccx_MergeOwner *****************************
 *
 * Return 0 if successful, >0 if error
 *
 *****************************************************************************/

void Ccx_MergeOwner(char *pOutbuf, char *pName)
{
   int   iTmp;
   char  acSaveName[100], acOwner1[100], acOwner2[100], acTmp[100], acVesting[8];
   char  *pTmp;

   OWNER    myOwner;
   CCX_NAME *pRec = (CCX_NAME *)pName;

   // Clear output buffer if needed
   removeNames(pOutbuf, true);

   // Initialize
   memcpy(acOwner1, pRec->Name1, RSIZ_PRIME_OWNER_NAME);
   myTrim(acOwner1, RSIZ_PRIME_OWNER_NAME);

   // Replace known bad characters
   replChar(acOwner1, 0x9F, 'O');
   replChar(acOwner1, 0xFE, 'O');
   if (iTmp = replUnPrtChar(acOwner1))
   {
      blankRem(acOwner1);
      LogMsg("*** Bad character in Owner1 at %.12s", pOutbuf);
   }

   // Check for special char
   if (pTmp = strchr(acOwner1, '/'))
   {
      *pTmp = 0;
      sprintf(acTmp, "%s & %s", acOwner1, ++pTmp);
      strcpy(acOwner1, acTmp);
   }
   strcpy(acSaveName, acOwner1);


   // Copy Name2 if present
   //   A  	Agreement of Sale
   //   B  	Second Owner Name
   //   C  	Care of Name - for mailing
   //   D  	Doing Business As
   //   E  	Et-al
   //   R  	Remainder for Life Estate
   if (pRec->Name2[0] > ' ')
   {
      memcpy(acOwner2, pRec->Name2, RSIZ_SECOND_OWNER_NAME);
      myTrim(acOwner2, RSIZ_SECOND_OWNER_NAME);
      if (iTmp = replUnPrtChar(acOwner2))
         LogMsg("*** Bad character in Owner2 at %.12s", pOutbuf);

      // Check for careof
      if (pRec->OwnerCode2 == 'C')
      {
         updateCareOf(pOutbuf, acOwner2, strlen(acOwner2));
         acOwner2[0] = 0;
      } else if (pRec->OwnerCode2 == 'D')
      {
         vmemcpy(pOutbuf+OFF_DBA, acOwner2, SIZ_DBA);
         acOwner2[0] = 0;
      } else if (pRec->OwnerCode2 == 'A')
         acOwner2[0] = 0;
   } else
      acOwner2[0] = 0;

   // Vesting
   acVesting[0] = 0;
   switch (pRec->OwnerCode1)
   {
      case 'C':
         strcpy(acVesting, "CP");
         break;
      case 'D':
         strcpy(acVesting, "DV");
         break;
      case 'I':
         strcpy(acVesting, "CO");
         break;
      case 'J':
         strcpy(acVesting, "JT");
         break;
      case 'L':
         strcpy(acVesting, "LE");
         break;
      case 'P':
         strcpy(acVesting, "PS");
         break;
      case 'S':
         strcpy(acVesting, "SE");
         break;
      case 'T':
         strcpy(acVesting, "TC");
         break;
   }

   // Check for TRUSTEE
   if (pTmp=strstr(acOwner1, " TRE"))
   {
      if (strlen(pTmp) == 4)
         *pTmp = 0;
   } else if (pTmp=strstr(acOwner1, " EST OF"))
   {
      strcpy(acVesting, "ES");
      *pTmp = 0;
   } else if (pTmp=strstr(acOwner1, "ET AL"))
   {
      strcpy(acVesting, "EA");
      pTmp--;
      if (*pTmp == ' ' || *pTmp == 'X')
         *pTmp = 0;
   }

#ifdef _DEBUG
   //if (!memcmp(pRollRec, "001081009", 9))
   //   iTmp = 1;
#endif
   // If Owner1 and Owner2 has the same last name, combine them
   if (acOwner2[0])
   {
      if (pTmp=strstr(acOwner2, " TRE"))
      {
         if (strlen(pTmp) == 4)
            *pTmp = 0;
      } else if (!memcmp(acOwner2, "ET AL", 5))
      {
         strcpy(acVesting, "EA");
         acOwner2[0] = 0;
      }
      if (strcmp(acOwner1, acOwner2))
      {
         // If Name1 is not a combined name, it might be merged with Name2
         if (!strchr(acOwner1, '&') && !strchr(acOwner2, '&'))
         {
            for (iTmp = 0; iTmp < strlen(acOwner1); iTmp++)
            {
               if (acOwner1[iTmp] != acOwner2[iTmp])
                  break;
            }
            if (acOwner1[iTmp-1] == ' ')
            {
               // Merge names
               sprintf(acTmp, " & %s", (char *)&acOwner2[iTmp]);
               acOwner2[0] = 0;
               strcat(acOwner1, acTmp);
            }
         }
      } else
         acOwner2[0] = 0;
   }

   // Do not parse if ownercode1 = 'P' or 'I'.  These are company names
   iTmp = strlen(acSaveName);
   memcpy(pOutbuf+OFF_NAME1, acSaveName, iTmp);
   if (pRec->OwnerCode1 == 'P' || pRec->OwnerCode1 == 'I')
   {
      memcpy(pOutbuf+OFF_NAME_SWAP, acSaveName, iTmp);
   } else
   {
      // Now parse owners
      iTmp = splitOwner(acOwner1, &myOwner, 3);
      if (iTmp >= 0)
         vmemcpy(pOutbuf+OFF_NAME_SWAP, myOwner.acSwapName, SIZ_NAME_SWAP);
      else
         vmemcpy(pOutbuf+OFF_NAME_SWAP, acSaveName, SIZ_NAME_SWAP);
   }

   if (acOwner2[0])
      vmemcpy(pOutbuf+OFF_NAME2, acOwner2, SIZ_NAME2);

   if (acVesting[0])
      memcpy(pOutbuf+OFF_VEST, acVesting, strlen(acVesting));
   else
      updateVesting(myCounty.acCntyCode, acSaveName, pOutbuf+OFF_VEST, pOutbuf+OFF_ETAL_FLG);
}

/********************************* Ccx_MergeSAdr *****************************
 *
 * Return 0 if successful, >0 if error
 *
 *****************************************************************************/

void Ccx_MergeSAdr(char *pOutbuf, char *pAdrRec, char *pZip, char *pZip4)
{
   CCX_SADR *pRec;
   char     *pTmp, acTmp[256], acAddr1[64], acAddr2[64], acCode[32], acHseNo[32], acUnitNo[32];
   int      iTmp, iSfxCode;
   long     lTmp;

   ADR_REC  sAdrRec;
   pRec = (CCX_SADR *)pAdrRec;

   // Situs
   acHseNo[0] = 0;
   acAddr1[0] = 0;
   acAddr2[0] = 0;
   acUnitNo[0] = 0;
   memset((void *)&sAdrRec, 0, sizeof(ADR_REC));
   removeSitus(pOutbuf);

   // Copy StrNum
   sAdrRec.lStrNum = atoin(pRec->S_StrNum, RSIZ_S_STRNUM);
   if (pRec->S_StrSfx[0] > ' ' && memcmp(pRec->S_StrSfx, "PARK", 4))
      memcpy(sAdrRec.strSfx, pRec->S_StrSfx, RSIZ_S_STRSFX);

   if (pRec->S_Unit[0] > ' ')
      memcpy(sAdrRec.Unit, pRec->S_Unit, RSIZ_S_APT_NBR);

   if (pRec->S_StrName[0] > ' ' && memcmp(pRec->S_StrName, "NO ADDRESS", 10))
   {
      // Copy street name
      memcpy(acTmp, pRec->S_StrName, RSIZ_S_STRNAME);
      myTrim(acTmp,RSIZ_S_STRNAME);

      if (strchr(acTmp, '&'))
         strcpy(sAdrRec.strName, acTmp);
      else
         parseAdrND(&sAdrRec, acTmp);

      // Special case CAITLIN PARK
      if (!memcmp(pRec->S_StrSfx, "PARK", 4) && sAdrRec.strSfx[0] < 'A')
      {
         sprintf(acTmp, "%s PARK", sAdrRec.strName);
         strcpy(sAdrRec.strName, acTmp);
      }

      if (sAdrRec.lStrNum > 0)
      {
         iTmp = sprintf(acAddr1, "%d", sAdrRec.lStrNum);
         memcpy(pOutbuf+OFF_S_STRNUM, acAddr1, iTmp);
         strcpy(acHseNo, acAddr1);
      } else if (*(pOutbuf+OFF_M_STRNUM) > '0' && *(pOutbuf+OFF_HO_FL) == '1' &&
           !memcmp(pOutbuf+OFF_M_STREET, sAdrRec.strName, strlen(sAdrRec.strName)))
      {  
         sAdrRec.lStrNum = atoin(pOutbuf+OFF_M_STRNUM, SIZ_M_STRNUM);
         iTmp = sprintf(acAddr1, "%d", sAdrRec.lStrNum);
         memcpy(pOutbuf+OFF_S_STRNUM, acAddr1, iTmp);
         strcpy(acHseNo, acAddr1);
      }

      if (sAdrRec.strDir[0] > ' ')
         *(pOutbuf+OFF_S_DIR) = sAdrRec.strDir[0];
      if (sAdrRec.strName[0] > ' ')
         vmemcpy(pOutbuf+OFF_S_STREET, sAdrRec.strName, SIZ_S_STREET);
      if (sAdrRec.strSfx[0] > ' ')
      {
         // Translate to code
         iSfxCode = GetSfxCode(myTrim(sAdrRec.strSfx));
         if (iSfxCode)
         {
            iTmp = sprintf(acTmp, "%d", iSfxCode);
            memcpy(pOutbuf+OFF_S_SUFF, acTmp, iTmp);
         } else
            LogMsg0("*** Unknown suffix %s [%.10s]", sAdrRec.strSfx, pOutbuf);
      }

      // Copy unit#
      memcpy(acTmp, pRec->S_Unit, RSIZ_S_APT_NBR);
      pTmp = myLTrim(acTmp, RSIZ_S_APT_NBR);
      if (*pTmp > ' ')
      {
         if (*pTmp != '#')
         {
            acUnitNo[0] = '#';
            strcpy(&acUnitNo[1], pTmp);
         } else
            strcpy(acUnitNo, pTmp);

         memcpy(pOutbuf+OFF_S_UNITNOX, acUnitNo, strlen(acUnitNo));
         strcpy(acTmp, acUnitNo);
         acTmp[SIZ_S_UNITNO] = 0;
         memcpy(pOutbuf+OFF_S_UNITNO, acTmp, strlen(acTmp));
      } else if (pRec->S_StrFra[0] > '0')
      {   // In CCX, StrFract is sometimes being used to store UnitNo
         if (pRec->S_StrFra[1] == '/')
            memcpy(pOutbuf+OFF_S_STR_SUB, pRec->S_StrFra, RSIZ_S_FRACTION);
         else
         {
            iTmp = sprintf(acHseNo, "%d-%.*s", sAdrRec.lStrNum, RSIZ_S_FRACTION, pRec->S_StrFra);
#ifdef _DEBUG
            lRecCnt++;
#endif
         }
      } else if (pRec->S_Unit[0] > ' ')
         iTmp = 0;         // Debug only

      // Save original house number
      memset(pOutbuf+OFF_S_HSENO, ' ', SIZ_S_HSENO);
      memcpy(pOutbuf+OFF_S_HSENO, acHseNo, strlen(acHseNo));

      sprintf(acAddr1, "%s %.3s %s %s %s %s", acHseNo, pOutbuf+OFF_S_STR_SUB, sAdrRec.strDir,
         sAdrRec.strName, sAdrRec.strSfx, acUnitNo);
      blankRem(acAddr1);
      memcpy(pOutbuf+OFF_S_ADDR_D, acAddr1, strlen(acAddr1));
   }

   // state
   memcpy(pOutbuf+OFF_S_ST, "CA", 2);

   // City
   if (pRec->S_CityAbbr[0] > ' ')
   {
      memcpy(acTmp, pRec->S_CityAbbr, RSIZ_S_CITYABBR);
      myTrim(acTmp, RSIZ_S_CITYABBR);
      Abbr2Code(acTmp, acCode, acAddr2);
      if (acCode[0])
      {
         memcpy(pOutbuf+OFF_S_CITY, acCode, strlen(acCode));
         strcat(acAddr2, " CA");
      }

      blankRem(acAddr2);
      memcpy(pOutbuf+OFF_S_CTY_ST_D, acAddr2, strlen(acAddr2));
   }

   // Zipcode
   lTmp = atoin(pZip, RSIZ_S_ZIP);
   if (lTmp > 90000)
   {
      memcpy(pOutbuf+OFF_S_ZIP, pZip, SIZ_S_ZIP);
      lTmp = atoin(pZip4, 4);
      if (lTmp > 0)
         memcpy(pOutbuf+OFF_S_ZIP4, pZip4, 4);
   }
}

void Ccx_MergeLdrSAdr(char *pOutbuf, char *pRollRec, char *pZip, char *pZip4)
{
   char     acTmp[256], acAddr1[64], acAddr2[64], acCode[32], acHseNo[32];
   int      iTmp, iSfxCode;
   long     lTmp;

   ADR_REC  sAdrRec;
   CCX_SADR *pRec = (CCX_SADR *)pRollRec;

   // Situs
   acHseNo[0] = 0;
   acAddr1[0] = 0;
   acAddr2[0] = 0;
   memset((void *)&sAdrRec, 0, sizeof(ADR_REC));
   removeSitus(pOutbuf);

   // Copy StrNum
   sAdrRec.lStrNum = atoin(pRec->S_StrNum, RSIZ_S_STRNUM);
   if (pRec->S_StrSfx[0] > ' ')
      memcpy(sAdrRec.strSfx, pRec->S_StrSfx, RSIZ_S_STRSFX);

   if (pRec->S_Unit[0] > ' ')
      memcpy(sAdrRec.Unit, pRec->S_Unit, RSIZ_S_APT_NBR);

   if (pRec->S_StrName[0] > ' ' && memcmp(pRec->S_StrName, "NO ADDRESS", 10))
   {
      // Copy street name
      memcpy(acTmp, pRec->S_StrName, RSIZ_S_STRNAME);
      myTrim(acTmp,RSIZ_S_STRNAME);

      if (strchr(acTmp, '&'))
         strcpy(sAdrRec.strName, acTmp);
      else
         parseAdrND(&sAdrRec, acTmp);

      if (sAdrRec.lStrNum > 0)
      {
         iTmp = sprintf(acAddr1, "%d", sAdrRec.lStrNum);
         memcpy(pOutbuf+OFF_S_STRNUM, acAddr1, iTmp);
         strcpy(acHseNo, acAddr1);
      }
      if (sAdrRec.strDir[0] > ' ')
         *(pOutbuf+OFF_S_DIR) = sAdrRec.strDir[0];
      if (sAdrRec.strName[0] > ' ')
      {
         iTmp = strlen(sAdrRec.strName);
         if (iTmp > SIZ_S_STREET)
            iTmp = SIZ_S_STREET;
         memcpy(pOutbuf+OFF_S_STREET, sAdrRec.strName, iTmp);
      }
      if (sAdrRec.strSfx[0] > ' ')
      {
         // Translate to code
         iSfxCode = GetSfxCode(myTrim(sAdrRec.strSfx));
         if (iSfxCode)
         {
            iTmp = sprintf(acTmp, "%d", iSfxCode);
            memcpy(pOutbuf+OFF_S_SUFF, acTmp, iTmp);
         } else
            LogMsg0("*** Unknown suffix %s", sAdrRec.strSfx);
      }

      // Copy unit#
      if (memcmp(pRec->S_Unit, "          ", RSIZ_S_APT_NBR))
      {
         if (pRec->S_Unit[0] != '#')
         {
            acTmp[0] = '#';
            memcpy((char *)&acTmp[1], pRec->S_Unit, RSIZ_S_APT_NBR);
         } else
            memcpy((char *)&acTmp[0], pRec->S_Unit, RSIZ_S_APT_NBR);
         acTmp[RSIZ_S_APT_NBR+1] = 0;
         blankRem((char *)&acTmp[1]);
         memcpy(pOutbuf+OFF_S_UNITNO, acTmp, strlen(acTmp));
      } else if (pRec->S_StrFra[0] > '0')
      {   // In CCX, StrFract is sometimes being used to store UnitNo
         if (pRec->S_StrFra[1] == '/')
            memcpy(pOutbuf+OFF_S_STR_SUB, pRec->S_StrFra, RSIZ_S_FRACTION);
         else
         {
            // To StrNum (Fernando put this in strName)
            // Don't put it in here because it will mess up geocoding process.  We need to find
            // a way to put it in product.

            //acToNum[0] = '-';
            //memcpy((char *)&acToNum[1], pRec->S_StrFra, RSIZ_S_FRACTION);
            //acToNum[RSIZ_S_FRACTION+1] = 0;
            //strcat(acAddr1, acToNum);
            iTmp = sprintf(acHseNo, "%d-%.*s", sAdrRec.lStrNum, RSIZ_S_FRACTION, pRec->S_StrFra);
         }
      }

      // Save original house number
      memcpy(pOutbuf+OFF_S_HSENO, acHseNo, strlen(acHseNo));

      sprintf(acTmp, " %.3s %s %s %s %.6s", pOutbuf+OFF_S_STR_SUB, sAdrRec.strDir,
         sAdrRec.strName, sAdrRec.strSfx, pOutbuf+OFF_S_UNITNO);
      strcat(acAddr1, acTmp);
      blankRem(acAddr1);
      memcpy(pOutbuf+OFF_S_ADDR_D, acAddr1, strlen(acAddr1));
   }

   // state
   memcpy(pOutbuf+OFF_S_ST, "CA", 2);

   // City
   if (pRec->S_CityAbbr[0] > ' ')
   {
      memcpy(acTmp, pRec->S_CityAbbr, RSIZ_S_CITYABBR);
      myTrim(acTmp, RSIZ_S_CITYABBR);
      Abbr2Code(acTmp, acCode, acAddr2);
      if (acCode[0])
      {
         memcpy(pOutbuf+OFF_S_CITY, acCode, strlen(acCode));
         strcat(acAddr2, " CA");
      }

      blankRem(acAddr2);
      memcpy(pOutbuf+OFF_S_CTY_ST_D, acAddr2, strlen(acAddr2));
   }

   // Zipcode
   lTmp = atoin(pZip, RSIZ_S_ZIP);
   if (lTmp > 90000)
   {
      memcpy(pOutbuf+OFF_S_ZIP, pZip, SIZ_S_ZIP);
      lTmp = atoin(pZip4, 4);
      if (lTmp > 0)
         memcpy(pOutbuf+OFF_S_ZIP4, pZip4, 4);
   }
}

/********************************* Ccx_MergeMAdr *****************************
 *
 * Mail address has similar problem as situs, but worse due to inconsistency.
 * Now we have to check for direction both pre and post strname.
 *
 *****************************************************************************/

void Ccx_MergeMAdr(char *pOutbuf, char *pRollRec, char *pZip4)
{
   CCX_MADR *pRec;
   char     *pTmp, acTmp[256], acAddr1[64], acAddr2[64], acToNum[16], acUnitNo[32];
   int      iTmp, iIdx;
   long     lTmp;

   ADR_REC  sAdrRec;

   pRec = (CCX_MADR *)pRollRec;

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "0116301490", 9))
   //   iTmp = 0;
#endif

   // Initialize
   acAddr1[0] = 0;
   acAddr2[0] = 0;
   acUnitNo[0] = 0;
   memset((void *)&sAdrRec, 0, sizeof(ADR_REC));
   removeMailing(pOutbuf, false);

   // Copy StrNum
   sAdrRec.lStrNum = atoin(pRec->M_StrNum, RSIZ_M_STRNUM);
   if (pRec->M_StrSfx[0] > ' ')
      memcpy(sAdrRec.strSfx, pRec->M_StrSfx, RSIZ_M_STRSFX);

   // Skip unwanted character
   iIdx = 0;
   if (pRec->M_StrName[0] < ' ' || pRec->M_StrName[0] > 'Z')
      iIdx++;

   if (pRec->M_StrName[iIdx] > ' ')
   {
      // Copy street name
      memcpy(acTmp, (char *)&pRec->M_StrName[iIdx], RSIZ_M_STRNAME-iIdx);
      
      // Remove known bad char 07/18/2016
      if ((unsigned char)acTmp[11] == 0x8D)
         acTmp[11] = 'P';              // APT 412
      blankRem(acTmp, RSIZ_M_STRNAME);
      quoteRem(acTmp);

      if (strchr(acTmp, '&'))
         strcpy(sAdrRec.strName, acTmp);
      else
      {
         if (!strcmp(acTmp, "AVE"))
         {
            if (!memcmp(pRec->M_StrFra, "61/4", 4))
               strcpy(sAdrRec.strName, "6 1/4");
            else
               memcpy(sAdrRec.strName, pRec->M_StrFra, RSIZ_M_FRACTION);
            memcpy(sAdrRec.strSfx, "AVE", 3);
         } else if (!strcmp(acTmp, "ST"))
         {
            memcpy(sAdrRec.strName, pRec->M_StrFra, RSIZ_M_FRACTION);
            memcpy(sAdrRec.strSfx, "ST", 2);
         } else if (!strcmp(acTmp, "1/2") )
         {
            memcpy(sAdrRec.strSub, "1/2", 3);
            memcpy(sAdrRec.strName, pRec->M_StrFra, RSIZ_M_FRACTION);
         } else if (!memcmp(acTmp, "1/2 ", 4) )
         {
            memcpy(sAdrRec.strSub, "1/2", 3);
            strcpy(acTmp, (char *)&acTmp[4]);
            parseAdrND(&sAdrRec, acTmp);
         } else
         {
            if (acTmp[0] == '&')
            {
               // If strnum is present, skip next number
               if (sAdrRec.lStrNum)
               {
                  iTmp = 2;
                  while (acTmp[iTmp] && acTmp[iTmp] < 'A') iTmp++;
                  while (iTmp > 0 && acTmp[iTmp-1] > ' ') iTmp--;    // & 123 1/2 20TH
               } else
                  iTmp = 2;
               strcpy(acTmp, (char *)&acTmp[iTmp]);
            }

            parseAdrND(&sAdrRec, acTmp);
         }
      }

      if (sAdrRec.lStrNum > 0)
      {
         iTmp = sprintf(acAddr1, "%d", sAdrRec.lStrNum);
         memcpy(pOutbuf+OFF_M_STRNUM, acAddr1, iTmp);
      }
      if (sAdrRec.strDir[0] > ' ')
         *(pOutbuf+OFF_M_DIR) = sAdrRec.strDir[0];
      if (sAdrRec.strName[0] > ' ')
         vmemcpy(pOutbuf+OFF_M_STREET, sAdrRec.strName, SIZ_M_STREET);
      if (sAdrRec.strSfx[0] > ' ')
         vmemcpy(pOutbuf+OFF_M_SUFF, sAdrRec.strSfx, SIZ_M_SUFF);

      // Copy UnitNo
      memcpy(acTmp, pRec->M_Unit, RSIZ_M_APT_NBR);
      pTmp = myLTrim(acTmp, RSIZ_M_APT_NBR);
      if (*pTmp > ' ')
      {
         if (*pTmp != '#')
         {
            acUnitNo[0] = '#';
            strcpy(&acUnitNo[1], pTmp);
         } else
            strcpy(acUnitNo, pTmp);

         vmemcpy(pOutbuf+OFF_M_UNITNOX, acUnitNo, SIZ_M_UNITNOX);
         vmemcpy(pOutbuf+OFF_M_UNITNO, acUnitNo, SIZ_M_UNITNO);
      } else if (sAdrRec.Unit[0] > ' ')
      {
         strcpy(acUnitNo, sAdrRec.Unit);
         vmemcpy(pOutbuf+OFF_M_UNITNOX, acUnitNo, SIZ_M_UNITNOX);
         vmemcpy(pOutbuf+OFF_M_UNITNO, acUnitNo, SIZ_M_UNITNO);
      }

      if (pRec->M_StrFra[0] > '0')
      {   // In CCX, StrFract is sometimes being used to store UnitNo
         if (pRec->M_StrFra[1] == '/')
            memcpy(pOutbuf+OFF_M_STR_SUB, pRec->M_StrFra, RSIZ_M_FRACTION);
         else
         {
            // To StrNum (Fernando put this in strName)
            acToNum[0] = '-';
            memcpy((char *)&acToNum[1], pRec->M_StrFra, RSIZ_M_FRACTION);
            acToNum[RSIZ_M_FRACTION+1] = 0;
            strcat(acAddr1, acToNum);
         }
      }

      sprintf(acTmp, " %.3s %s %s %s %s", pOutbuf+OFF_M_STR_SUB, sAdrRec.strDir,
         sAdrRec.strName, sAdrRec.strSfx, acUnitNo);
      strcat(acAddr1, acTmp);
      blankRem(acAddr1);
      vmemcpy(pOutbuf+OFF_M_ADDR_D, acAddr1, SIZ_M_ADDR_D);
   }

   // city state
   if (pRec->M_CitySt[0] > ' ')
   {
      memcpy(acAddr2, pRec->M_CitySt, RSIZ_M_CITYST);
      myTrim(acAddr2, RSIZ_M_CITYST);
      pTmp = strrchr(acAddr2, ' ');
      if (pTmp && strlen(pTmp) == 3)
      {
         memcpy(pOutbuf+OFF_M_ST, pTmp+1, SIZ_M_ST);
         *pTmp = 0;
         memcpy(pOutbuf+OFF_M_CITY, acAddr2, strlen(acAddr2));
         *pTmp = ' ';
      } else
         memcpy(pOutbuf+OFF_M_CITY, acAddr2, strlen(acAddr2));

      memcpy(pOutbuf+OFF_M_CTY_ST_D, acAddr2, strlen(acAddr2));
   }

   // Zipcode
   lTmp = atoin(pRec->M_Zip, RSIZ_M_ZIP);
   if (lTmp > 400)
   {
      memcpy(pOutbuf+OFF_M_ZIP, pRec->M_Zip, SIZ_M_ZIP);
      lTmp = atoin(pZip4, 4);
      if (lTmp > 0)
         memcpy(pOutbuf+OFF_M_ZIP4, pZip4, 4);
   }

}

void Ccx_MergeLdrMAdr(char *pOutbuf, char *pRollRec)
{
   char     *pTmp, acTmp[256], acAddr1[64], acAddr2[64], acToNum[16], acUnitNo[32];
   int      iTmp, iIdx;
   long     lTmp;

   ADR_REC  sAdrRec;
   CCX_MADR *pRec = (CCX_MADR *)pRollRec;

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "011291065", 9))
   //   iTmp = 0;
#endif

   // Initialize
   acAddr1[0] = 0;
   acAddr2[0] = 0;
   acUnitNo[0] = 0;
   memset((void *)&sAdrRec, 0, sizeof(ADR_REC));
   removeMailing(pOutbuf, false);

   // Copy StrNum
   sAdrRec.lStrNum = atoin(pRec->M_StrNum, RSIZ_M_STRNUM);
   if (pRec->M_StrSfx[0] > ' ')
      memcpy(sAdrRec.strSfx, pRec->M_StrSfx, RSIZ_M_STRSFX);

   if (pRec->M_Unit[0] > ' ')
      memcpy(acUnitNo, pRec->M_Unit, RSIZ_M_APT_NBR);

   // Skip unwanted character
   iIdx = 0;
   if (pRec->M_StrName[0] < ' ' || pRec->M_StrName[0] > 'Z')
      iIdx++;

   if (pRec->M_StrName[iIdx] > ' ')
   {
      // Copy street name
      memcpy(acTmp, (char *)&pRec->M_StrName[iIdx], RSIZ_M_STRNAME-iIdx);
      acTmp[RSIZ_M_STRNAME] = 0;
      blankRem(acTmp);
      quoteRem(acTmp);

      if (strchr(acTmp, '&'))
         strcpy(sAdrRec.strName, acTmp);
      else
      {
         if (!strcmp(acTmp, "AVE"))
         {
            if (!memcmp(pRec->M_StrFra, "61/4", 4))
               strcpy(sAdrRec.strName, "6 1/4");
            else
               memcpy(sAdrRec.strName, pRec->M_StrFra, RSIZ_M_FRACTION);
            memcpy(sAdrRec.strSfx, "AVE", 3);
         } else if (!strcmp(acTmp, "ST"))
         {
            memcpy(sAdrRec.strName, pRec->M_StrFra, RSIZ_M_FRACTION);
            memcpy(sAdrRec.strSfx, "ST", 2);
         } else if (!strcmp(acTmp, "1/2") )
         {
            memcpy(sAdrRec.strSub, "1/2", 3);
            memcpy(sAdrRec.strName, pRec->M_StrFra, RSIZ_M_FRACTION);
         } else if (!memcmp(acTmp, "1/2 ", 4) )
         {
            memcpy(sAdrRec.strSub, "1/2", 3);
            strcpy(acTmp, (char *)&acTmp[4]);
            parseAdrND(&sAdrRec, acTmp);
         } else
         {
            if (acTmp[0] == '&')
            {
               // If strnum is present, skip next number
               if (sAdrRec.lStrNum)
               {
                  iTmp = 2;
                  while (acTmp[iTmp] && acTmp[iTmp] < 'A') iTmp++;
                  while (iTmp > 0 && acTmp[iTmp-1] > ' ') iTmp--;    // & 123 1/2 20TH
               } else
                  iTmp = 2;
               strcpy(acTmp, (char *)&acTmp[iTmp]);
            }
            parseAdrND(&sAdrRec, acTmp);
         }
      }

      if (sAdrRec.lStrNum > 0)
      {
         iTmp = sprintf(acAddr1, "%d", sAdrRec.lStrNum);
         memcpy(pOutbuf+OFF_M_STRNUM, acAddr1, iTmp);
      }
      if (sAdrRec.strDir[0] > ' ')
         *(pOutbuf+OFF_M_DIR) = sAdrRec.strDir[0];
      if (sAdrRec.strName[0] > ' ')
         vmemcpy(pOutbuf+OFF_M_STREET, sAdrRec.strName, SIZ_M_STREET);
      if (sAdrRec.strSfx[0] > ' ')
         memcpy(pOutbuf+OFF_M_SUFF, sAdrRec.strSfx, strlen(sAdrRec.strSfx));

      // Copy unit#
      acToNum[0] = 0;
      if (acUnitNo[0] > ' ' && memcmp(acUnitNo, "          ", RSIZ_M_APT_NBR))
      {
         if (acUnitNo[0] != '#')
         {
            acTmp[0] = '#';
            memcpy((char *)&acTmp[1], acUnitNo, RSIZ_M_APT_NBR);
         } else
            memcpy((char *)&acTmp[0], acUnitNo, RSIZ_M_APT_NBR);

         iTmp = blankRem(acTmp, RSIZ_M_APT_NBR);
         strcpy(acUnitNo, acTmp);
         memcpy(pOutbuf+OFF_M_UNITNOX, acTmp, iTmp);
         acTmp[SIZ_M_UNITNO] = 0;
         memcpy(pOutbuf+OFF_M_UNITNO, acTmp, strlen(acTmp));
      } else if (pRec->M_StrFra[0] > '0')
      {   // In CCX, StrFract is sometimes being used to store UnitNo
         if (pRec->M_StrFra[1] == '/')
            memcpy(pOutbuf+OFF_M_STR_SUB, pRec->M_StrFra, RSIZ_M_FRACTION);
         else
         {
            // To StrNum (Fernando put this in strName)
            acToNum[0] = '-';
            memcpy((char *)&acToNum[1], pRec->M_StrFra, RSIZ_M_FRACTION);
            acToNum[RSIZ_M_FRACTION+1] = 0;
            strcat(acAddr1, acToNum);
         }
      }

      sprintf(acTmp, " %.3s %s %s %s %s", pOutbuf+OFF_M_STR_SUB, sAdrRec.strDir,
         sAdrRec.strName, sAdrRec.strSfx, acUnitNo);
      strcat(acAddr1, acTmp);
      blankRem(acAddr1);
      vmemcpy(pOutbuf+OFF_M_ADDR_D, acAddr1, SIZ_M_ADDR_D);
   }

   // city state
   if (pRec->M_CitySt[0] > ' ')
   {
      memcpy(acAddr2, pRec->M_CitySt, RSIZ_M_CITYST);
      myTrim(acAddr2, RSIZ_M_CITYST);
      pTmp = strrchr(acAddr2, ' ');
      if (pTmp && strlen(pTmp) == 3)
      {
         memcpy(pOutbuf+OFF_M_ST, pTmp+1, SIZ_M_ST);
         *pTmp = 0;
         memcpy(pOutbuf+OFF_M_CITY, acAddr2, strlen(acAddr2));
         *pTmp = ' ';
      } else
         memcpy(pOutbuf+OFF_M_CITY, acAddr2, strlen(acAddr2));

      memcpy(pOutbuf+OFF_M_CTY_ST_D, acAddr2, strlen(acAddr2));
   }

   // Zipcode
   lTmp = atoin(pRec->M_Zip, RSIZ_M_ZIP);
   if (lTmp > 400)
   {
      memcpy(pOutbuf+OFF_M_ZIP, pRec->M_Zip, SIZ_M_ZIP);
      //lTmp = atoin(pRec->M_Zip4, 4);
      //if (lTmp > 0)
      //   memcpy(pOutbuf+OFF_M_ZIP4, pRec->M_Zip4, 4);
   }

}

/********************************* Ccx_MergeChar *****************************
 *
 * This function updates some characteristics (pdrfile.txt) to roll file.  Others
 * will come from salfile.txt
 *
 * 1) Stories is counted if BldgSqft2 > 0.
 * 2) Don't use LotSqft unless roll record is blank
 * 3) On parcel with multiple buildings, add them up (bldgsqft, beds, baths, ...)
 *    - If their is building 0, use that record only ignoring the rest.
 *    - Use YrBlt and EffYr from BldgNum 1.
 *
 *
 *****************************************************************************/

int Ccx_MergeChar(char *pOutbuf)
{

   static   char acRec[1024], *pRec=NULL;
   char     acTmp[256];
   long     lTmp, lBldgSqft, lGarSqft, lBsmtSqft;
   int      iRet, iTmp, iLoop, iBeds, iFBath, iHBath, iRooms, iBldgNum;
   CCX_CHAR *pChar;

   iRet=iBeds=iFBath=iHBath=iRooms=0;
   lBldgSqft=lGarSqft=lBsmtSqft=0;

   // Get first Char rec for first call
   if (!pRec && !lCharMatch)
   {
      pRec = fgets(acRec, 1024, fdChar);
   }
   pChar = (CCX_CHAR *)pRec;

   do
   {
      if (!pRec)
      {
         fclose(fdChar);
         fdChar = NULL;
         return 1;      // EOF
      }

      // Compare Apn
      iLoop = memcmp(pOutbuf, pChar->Apn, CSIZ_APN);
      if (iLoop > 0)
      {
         if (bDebug)
            LogMsg0("Skip Char rec  %.*s", CSIZ_APN, pChar->Apn);
         pRec = fgets(acRec, 1024, fdChar);
         lCharSkip++;
      }
   } while (iLoop > 0);

   // If not match, return
   if (iLoop)
      return 1;

   while (!iLoop)
   {
      // YrEff
      lTmp = atoin(pChar->EffYear, CSIZ_EFF_YR);
      if (lTmp > 1700)
         memcpy(pOutbuf+OFF_YR_EFF, pChar->EffYear, SIZ_YR_BLT);

      // YrBlt
      lTmp = atoin(pChar->YearBuilt, CSIZ_EFF_YR);
      if (lTmp > 1700)
         memcpy(pOutbuf+OFF_YR_BLT, pChar->YearBuilt, SIZ_YR_BLT);

      // BldgSqft
      lBldgSqft += atoin(pChar->BldgSqft, CSIZ_BLDG_SQFT);
      iTmp = 0;
      if (lBldgSqft > 10)
      {
         iTmp = 1;
         lTmp = atoin(pChar->BldgSqft2, CSIZ_BLDG_SQFT);
         if (lTmp > 0)
         {
            iTmp++;
            lBldgSqft += lTmp;

            // Stories
            if (iTmp > 0 && iTmp < 99)
            {
               if (iTmp > 9 && bDebug)
                  LogMsg("%.*s : %d", iApnLen, pOutbuf, iTmp);
            sprintf(acTmp, "%d.0  ", iTmp);
            memcpy(pOutbuf+OFF_STORIES, acTmp, SIZ_STORIES);
            }
         }

         // Adding BasementSqft to BldgSqft to make it matched with Fernando calculation
         lBldgSqft += atoin(pChar->BsmtSqft, CSIZ_BASEMENT_SQFT);
         sprintf(acTmp, "%*u", SIZ_BLDG_SF, lBldgSqft);
         memcpy(pOutbuf+OFF_BLDG_SF, acTmp, SIZ_BLDG_SF);
      }

      // Garage Sqft
      lGarSqft += atoin(pChar->GarSqft, CSIZ_GARAGE_SQFT);
      if (lGarSqft > 10)
      {
         sprintf(acTmp, "%*u", SIZ_GAR_SQFT, lGarSqft);
         memcpy(pOutbuf+OFF_GAR_SQFT, acTmp, SIZ_GAR_SQFT);
         // This is to cover where Garage_Spcs is 0
         *(pOutbuf+OFF_PARK_TYPE) = 'Z';
      }

      // Park spaces

      // Beds
      iBeds += atoin(pChar->Beds, CSIZ_BEDROOMS);
      if (iBeds > 0)
      {
         sprintf(acTmp, "%*u", SIZ_BEDS, iBeds);
         memcpy(pOutbuf+OFF_BEDS, acTmp, SIZ_BEDS);
      }

      // Baths
      iFBath += atoin(pChar->Baths, CSIZ_BATHSROOMS-1);
      if (iFBath > 0)
      {
         sprintf(acTmp, "%*u", SIZ_BATH_F, iFBath);
         memcpy(pOutbuf+OFF_BATH_F, acTmp, SIZ_BATH_F);

         // Half bath
         iTmp = pChar->Baths[CSIZ_BATHSROOMS-1] & 0x0F;
         if (iTmp > 0)
         {
            iHBath++;
            sprintf(acTmp, "%*u", SIZ_BATH_H, iHBath);
            memcpy(pOutbuf+OFF_BATH_H, acTmp, SIZ_BATH_H);
         }
      }

      // Rooms
      iRooms += atoin(pChar->Rooms, CSIZ_UNITS_ROOMS);
      if (iRooms > 0)
      {
         if (pChar->PdrType == 'O')       // Commercial
         {
            sprintf(acTmp, "%*u", SIZ_UNITS, iRooms);
            memcpy(pOutbuf+OFF_UNITS, acTmp, SIZ_UNITS);
         } else
         {
            sprintf(acTmp, "%*u", SIZ_ROOMS, iRooms);
            memcpy(pOutbuf+OFF_ROOMS, acTmp, SIZ_ROOMS);
         }
      }

      // Fireplace

      // Floor

      // Pool/Spa
      if (pChar->Pool == 'Y')
         *(pOutbuf+OFF_POOL) = 'P';    // Pool

#ifdef _DEBUG
      //if (!memcmp(pOutbuf, "003120007", 9))
      //   lTmp = 0;
#endif
      // Lot sqft - Lot Acres: only update if current roll value not present
      lTmp = atoin(pChar->LotSqft, CSIZ_LOT_SQFT);
      if (lTmp > 100 && *(pOutbuf+OFF_LOT_SQFT+SIZ_LOT_SQFT-2) == ' ')
      {
         sprintf(acTmp, "%*u", SIZ_LOT_SQFT, lTmp);
         memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);
         lTmp = (long)(((double)lTmp*ACRES_FACTOR)/SQFT_PER_ACRE);
         sprintf(acTmp, "%*u", SIZ_LOT_SQFT, lTmp);
         memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_SQFT);
      }

      // Zoning
      if (*(pOutbuf+OFF_ZONE) == ' ')
      {
         if (pChar->Zoning[0] >= 'A')
         {
            memcpy(acTmp, pChar->Zoning, CSIZ_ZONE);
            iTmp = blankRem(acTmp, CSIZ_ZONE);
            memcpy(pOutbuf+OFF_ZONE, acTmp, iTmp);

            if (*(pOutbuf+OFF_ZONE_X1) == ' ')
               vmemcpy(pOutbuf+OFF_ZONE_X1, pChar->Zoning, SIZ_ZONE_X1, CSIZ_ZONE);
         }
      }

      // Usecode

      // Basement Sqft 
      lBsmtSqft += atoin(pChar->BsmtSqft, CSIZ_BASEMENT_SQFT);
      if (lBsmtSqft > 100)
      {
         sprintf(acTmp, "%*u", SIZ_BSMT_SF, lBsmtSqft);
         memcpy(pOutbuf+OFF_BSMT_SF, acTmp, SIZ_BSMT_SF);
      } else
         memset(pOutbuf+OFF_BSMT_SF, ' ', SIZ_BSMT_SF);

      // Quality/class

      // View
      if (pChar->View == 'Y')
         *(pOutbuf+OFF_VIEW) = 'A';    // 12/19/2016 sn

      // Units - See rooms
      // Buildings - if

      // Others
      iBldgNum = atoin(pChar->BldgNum, CSIZ_BLDG_NUMBER);

      // Get next Char rec
      pRec = fgets(acRec, 1024, fdChar);
      if (!pRec)
         break;

      if (!iBldgNum)
         break;

      iLoop = memcmp(pOutbuf, pChar->Apn, CSIZ_APN);
   }

   lCharMatch++;
   return 0;
}

/********************************* Ccx_MergeCChr *****************************
 *
 * This function updates some characteristics from sale file. Update only when
 * current is empty.
 *
 *****************************************************************************/

int Ccx_MergeCChr(char *pOutbuf)
{
   static   char acRec[1024], *pRec=NULL;
   char     acTmp[256];
   long     lTmp, lBldgSqft;
   int      iLoop, iBeds, iFBath, iHBath, iRooms;
   CCX_CCHR *pChar;

   // Get first Char rec for first call
   if (!pRec && !lCChrMatch)
   {
      pRec = fgets(acRec, 1024, fdCChr);
   }
   pChar = (CCX_CCHR *)pRec;

   do
   {
      if (!pRec)
      {
         fclose(fdCChr);
         fdCChr = NULL;
         return 1;      // EOF
      }

      // Compare Apn
      iLoop = memcmp(pOutbuf, pChar->Apn, CSIZ_APN);
      if (iLoop > 0)
      {
         if (bDebug)
            LogMsg0("Skip Char rec  %.*s", CSIZ_APN, pChar->Apn);
         pRec = fgets(acRec, 1024, fdCChr);
         lCChrSkip++;
      }
   } while (iLoop > 0);

   // If not match, return
   if (iLoop)
      return 1;

   while (!iLoop)
   {
      // Total Living Area
      lBldgSqft = atoin(pChar->Total_Tot_Liv_Area, SSIZ_TOTAL_LIV_AREA);
      lTmp = atoin(pOutbuf+OFF_BLDG_SF, SIZ_BLDG_SF);
      if (lBldgSqft > 10 && !lTmp)
      {
         sprintf(acTmp, "%*u", SIZ_BLDG_SF, lBldgSqft);
         memcpy(pOutbuf+OFF_BLDG_SF, acTmp, SIZ_BLDG_SF);
      }

      // YrEff
      lTmp = atoin(pChar->EffYear, CSIZ_EFF_YR);
      if (lTmp > 1700 && *(pOutbuf+OFF_YR_EFF) < '1')
         memcpy(pOutbuf+OFF_YR_EFF, pChar->EffYear, SIZ_YR_BLT);

      // Beds
      iBeds = atoin(pOutbuf+OFF_BEDS, SIZ_BEDS);
      if (!iBeds)
      {
         iBeds = atoin(pChar->Beds, CSIZ_BEDROOMS);
         if (iBeds > 0)
         {
            sprintf(acTmp, "%*u", SIZ_BEDS, iBeds);
            memcpy(pOutbuf+OFF_BEDS, acTmp, SIZ_BEDS);
         }
      }

      // Baths
      iFBath = atoin(pOutbuf+OFF_BATH_F, SIZ_BATH_F);
      if (!iFBath)
      {
         iFBath = atoin(pChar->Baths, CSIZ_BATHSROOMS-1);
         if (iFBath > 0)
         {
            sprintf(acTmp, "%*u", SIZ_BATH_F, iFBath);
            memcpy(pOutbuf+OFF_BATH_F, acTmp, SIZ_BATH_F);

            // Half bath
            iHBath = pChar->Baths[CSIZ_BATHSROOMS-1] & 0x0F;
            if (iHBath > 0)
            {
               iHBath = 1;
               sprintf(acTmp, "%*u", SIZ_BATH_H, iHBath);
               memcpy(pOutbuf+OFF_BATH_H, acTmp, SIZ_BATH_H);
            }
         }
      }

      // Rooms or Units
      if (pChar->PdrType == 'O')
      {
         iRooms = atoin(pOutbuf+OFF_UNITS, SIZ_UNITS);
         if (!iRooms)
         {
            iRooms = atoin(pChar->Units, SIZ_CCHAR_UNITS);
            if (iRooms > 0)
            {
               sprintf(acTmp, "%*u", SIZ_UNITS, iRooms);
               memcpy(pOutbuf+OFF_UNITS, acTmp, SIZ_UNITS);
            }
         }
      } else
      {
         iRooms = atoin(pOutbuf+OFF_ROOMS, SIZ_ROOMS);
         if (!iRooms)
         {
            iRooms = atoin(pChar->Rooms, CSIZ_UNITS_ROOMS);
            if (iRooms > 0)
            {
               sprintf(acTmp, "%*u", SIZ_ROOMS, iRooms);
               memcpy(pOutbuf+OFF_ROOMS, acTmp, SIZ_ROOMS);
            }
         }
      }

      // Garage/Carport
      if (pChar->Garage == 'G')
      {
         if (pChar->AttachedGarage == 'Y')
            *(pOutbuf+OFF_PARK_TYPE) = 'I';
         else
            *(pOutbuf+OFF_PARK_TYPE) = 'Z';
      } else if (pChar->CarPort == 'C')
         *(pOutbuf+OFF_PARK_TYPE) = 'C';

      // Park spaces
      lTmp = atoin(pChar->ParkStalls, SSIZ_PARKING_STALLS);
      if (lTmp > 0)
      {
         sprintf(acTmp, "%d      ", lTmp);
         memcpy(pOutbuf+OFF_PARK_SPACE, acTmp, SIZ_PARK_SPACE);
      }

#ifdef _DEBUG
      //if (!memcmp(pChar->Apn, "004113013",9))
      //   lTmp = 0;
#endif

      // Condition
      if (pChar->Condition > ' ')
         *(pOutbuf+OFF_IMPR_COND) = pChar->Condition;

      // Central Air/Heat
      if (pChar->Central_AirHeat == 'A')
         *(pOutbuf+OFF_AIR_COND) = 'C';
      if (pChar->Central_AirHeat == 'H')
         *(pOutbuf+OFF_HEAT) = 'Z';

      // Others

      // Get next Char rec
      pRec = fgets(acRec, 1024, fdCChr);
      if (!pRec)
      {
         fclose(fdCChr);
         fdCChr = NULL;
         iLoop = 1;
      } else
         iLoop = memcmp(pOutbuf, pChar->Apn, CSIZ_APN);        
   }

   lCChrMatch++;
   return 0;
}

/********************************* Ccx_FormatDoc *****************************
 *
 *  - DocNum format: input as VVVVVPPPPP
 *    + Before 1994 : V*-P*
 *    + 1994-present: VVVPPP  Drop first 2 in vol and first 2 in page
 *
 *
 *****************************************************************************/

int Ccx_FormatDoc(char *pDocNum, char *pDocVol, char *pDocPage, char *pDate)
{
   long  lTmp, lDocVol, lDocPage;
   int   iRet;

   lTmp = atoin(pDate, 8);
   if (pDocPage)
   {
      if (lTmp > 19931231)
      {  // 1994-present
         lDocVol = atoin(pDocVol+2, SSIZ_DEED_REF_VOL-2);
      } else
      {  // Before 1994
         lDocVol = atoin(pDocVol, SSIZ_DEED_REF_VOL);
      }
      lDocPage= atoin(pDocPage, SSIZ_DEED_REF_PAGE);
   } else
   {
      if (lTmp > 19931231)
      {  // 1994-present
         lDocVol = atoin(pDocVol+2, SSIZ_DEED_REF_VOL-2);
         lDocPage= atoin(pDocVol+SSIZ_DEED_REF_VOL, 3);
      } else
      {  // Before 1994
         lDocVol = atoin(pDocVol, SSIZ_DEED_REF_VOL);
         lDocPage= atoin(pDocVol+SSIZ_DEED_REF_VOL, 3);
      }
   }

   if (lDocVol || lDocPage)
   {
      iRet = 0;
      if (lDocVol)
      {
         if (lTmp > 19931231)
            sprintf(pDocNum, "%d%.3d          ", lDocVol, lDocPage);
         else
            sprintf(pDocNum, "%d-%d          ", lDocVol, lDocPage);
      } else
         sprintf(pDocNum, "%d            ", lDocPage);
   } else
   {
      iRet = 1;
      memset(pDocNum, ' ', SIZ_TRANSFER_DOC);
   }
   *(pDocNum+SIZ_TRANSFER_DOC) = 0;

   return iRet;
}

/********************************* Ccx_MergeSale *****************************
 *
 * Could you please suppress the publication of sales price information or
 * transfer tax information on parcels that start with 900 or 901 in Contra
 * Costa County data?
 *
 * We are researching the confidentiality of this information.  These parcels
 * are stock co-operative units that do not have recorded ownership documents.
 *
 *****************************************************************************/

int Ccx_MergeSale1(char *pOutbuf, char *pSaleRec, int iCnt)
{
   CCX_CSAL *pRec;
   long     lDttAmt, lRecDate;
   int      iRet;
   char     *pDate, *pAmt, *pDoc, *pDocType, *pSaleDate;
   char     acTmp[32], acDocNum[32];

   pRec = (CCX_CSAL *)pSaleRec;
   pSaleDate = pRec->SaleDate;

   // Check for date
   lRecDate = atoin(pSaleDate, SIZ_SALE1_DT);
   if (lRecDate <= 0 || lRecDate >= lToday)
      return 1;

   switch (iCnt)
   {
      case 1:
         pAmt    = pOutbuf+OFF_SALE1_AMT;
         pDate   = pOutbuf+OFF_SALE1_DT;
         pDoc    = pOutbuf+OFF_SALE1_DOC;
         pDocType= pOutbuf+OFF_SALE1_DOCTYPE;
         break;
      case 2:
         pAmt    = pOutbuf+OFF_SALE2_AMT;
         pDate   = pOutbuf+OFF_SALE2_DT;
         pDoc    = pOutbuf+OFF_SALE2_DOC;
         pDocType= pOutbuf+OFF_SALE2_DOCTYPE;
         break;
      case 3:
         pAmt    = pOutbuf+OFF_SALE3_AMT;
         pDate   = pOutbuf+OFF_SALE3_DT;
         pDoc    = pOutbuf+OFF_SALE3_DOC;
         pDocType= pOutbuf+OFF_SALE3_DOCTYPE;
         break;
      default:
         return 1;
   }

#ifdef _DEBUG
   //if (!memcmp(pRec->Apn, "90",2))
   //   iRet = 0;
#endif

   lDttAmt = atoin(pRec->PriceFromStamp, SSIZ_PRICE_FROM_STAMPS);
   iRet = Ccx_FormatDoc(acDocNum, pRec->DeedVol, pRec->DeedPage, pSaleDate);

   // 10/29/2010 -SN - Strip out all sale price < 10 for hiding request 
   if (lDttAmt > 9 && memcmp(pOutbuf, "900", 3) && memcmp(pOutbuf, "901", 3))
   {
      sprintf(acTmp, "%*u", SIZ_SALE1_AMT, lDttAmt);
      memcpy(pAmt, acTmp, SIZ_SALE1_AMT);
   } else
      memset(pAmt, ' ', SIZ_SALE1_AMT);

   memcpy(pDate, pSaleDate, SIZ_SALE1_DT);
   memcpy(pDoc, acDocNum, SIZ_SALE1_DOC);

   // Translate doctype
   if (pRec->DeedType == 'G')
      memcpy(pDocType, "1    ", 5);
   else if (pRec->DeedType == 'Q')
      memcpy(pDocType, "4    ", 5);
   else
      memcpy(pDocType, "     ", 5);

   // Update transfer on first sale
   /* Use Deed Ref from roll file only
   if (1 == iCnt)
   {
      iRet = atoin(pOutbuf+OFF_TRANSFER_DT, SIZ_TRANSFER_DT);
      if (iRet > lToday || iRet < lRecDate)
      {
         memcpy(pOutbuf+OFF_TRANSFER_DT, pSaleDate, SIZ_TRANSFER_DT);
         memcpy(pOutbuf+OFF_TRANSFER_DOC, acDocNum, SIZ_SALE1_DOC);
      }
   }
   */

   return 0;
}

// Cum sale file is a fixed length 144-byte record
int Ccx_MergeSale(char *pOutbuf)
{
   static   char  acRec[1024], *pRec=NULL;
   static   int   iSaleLen;

   char     acSave[1024];
   int      iRet, iLoop, iSaleCnt, iInc;
   CCX_CSAL *pSale;

   // Get first Char rec for first call
   if (!pRec && !lSaleMatch)
   {
      iSaleLen = fread(acRec, 1, iCSalLen, fdSale);
      pRec = acRec;
      //pRec = fgets(acRec, 1024, fdSale);
   }

   pSale = (CCX_CSAL *)&acRec[0];
   do
   {
      if (iSaleLen != iCSalLen)
      {
         fclose(fdSale);
         fdSale = NULL;
         return 1;      // EOF
      }

      // Compare Apn
      iLoop = memcmp(pOutbuf, pSale->Apn, SSIZ_APN);
      if (iLoop > 0)
      {
         if (bDebug)
            LogMsg0("Skip Sale rec  %.*s", SSIZ_APN, pSale->Apn);
         iSaleLen = fread(acRec, 1, iCSalLen, fdSale);
         lSaleSkip++;
      }
   } while (iLoop > 0);

   // If not match, return
   if (iLoop)
      return 1;

   iSaleCnt = 0;
   acSave[0] = 0;

   // Remove all old sales before apply cum sales
   ClearOldSale(pOutbuf);

   do
   {
#ifdef _DEBUG
      //if (!memcmp(pOutbuf, "0021020037", 10))
      //   iRet = 0;
#endif
      // Populate sale using Grand deed only
      if (pSale->DeedType == 'G')
      {
         // If same APN and Date, update data only.  Do not move sale
         if (!iSaleCnt || memcmp(acRec, acSave, 20))
         {
            iSaleCnt++;
            iInc = 1;
         } else
            iInc = 0;

         iRet = Ccx_MergeSale1(pOutbuf, acRec, iSaleCnt);
         if (iRet)
         {
            if (iInc)
               iSaleCnt--;
         } else
            *(pOutbuf+OFF_AR_CODE1+iSaleCnt-1) = 'A';           // Assessor-Recorder code
         memcpy(acSave, acRec, iCSalLen);
      }

      // Get next sale record
      iSaleLen = fread(acRec, 1, iCSalLen, fdSale);
      if (iSaleLen != iCSalLen)
      {
         fclose(fdSale);
         fdSale = NULL;
         break;
      }

   } while (!memcmp(pOutbuf, pSale->Apn, SSIZ_APN) && iSaleCnt < 3);

   // Clear other sale left on record
   /* No need since all sales have been removed before calling this function.
   while (iSaleCnt < 3)
   {
      iSaleCnt++;
      ClearOneSale(pOutbuf, iSaleCnt);
   } */

   lSaleMatch++;

   return 0;
}

/********************************* Ccx_MergeRoll *****************************
 *
 * Return 0 if successful, >0 if error
 *
 * Notes: The iLen parameter is not used.
 *
 *****************************************************************************/

void Ccx_MergeRoll(char *pOutbuf, char *pRollRec, int iLen, int iFlag)
{
   CCX_ROLL *pRec;
   char     acTmp[256], acTmp1[256], acDocNum[32], *pRet;
   long     lTmp, lExe;
   double   dTmp;
   int      iRet;

   pRec = (CCX_ROLL *)pRollRec;
   //iRet = sizeof(CCX_ROLL);
   if (iFlag & CREATE_R01)
   {
      // Clear output buffer
      memset(pOutbuf, ' ', iRecLen);

      // Start copying data
      memcpy(pOutbuf, pRec->Apn, RSIZ_APN);
      memcpy(pOutbuf+OFF_CO_NUM, "07CCX", 5);

      // Format APN
      iRet = formatApn(pRec->Apn, acTmp, &myCounty);
      memcpy(pOutbuf+OFF_APN_D, acTmp, iRet);

      // Create MapLink
      iRet = formatMapLink(pRec->Apn, acTmp, &myCounty);
      memcpy(pOutbuf+OFF_MAPLINK, acTmp, iRet);

      // Create index map link
      if (getIndexPage(acTmp, acTmp1, &myCounty))
         memcpy(pOutbuf+OFF_IMAPLINK, acTmp1, iRet);

      // Year assessed
      memcpy(pOutbuf+OFF_YR_ASSD, myCounty.acYearAssd, 4);

      // Land
      long lLand = atoin(pRec->Land, RSIZ_LAND_VALUE);
      if (lLand > 0)
      {
         sprintf(acTmp, "%*u", SIZ_LAND, lLand);
         memcpy(pOutbuf+OFF_LAND, acTmp, SIZ_LAND);
      }

      // Improve
      long lImpr = atoin(pRec->Impr, RSIZ_LAND_VALUE);
      if (lImpr > 0)
      {
         sprintf(acTmp, "%*u", SIZ_IMPR, lImpr);
         memcpy(pOutbuf+OFF_IMPR, acTmp, SIZ_IMPR);
      }

      // Other value
      long lPers = atoin(pRec->PP_Val, RSIZ_LAND_VALUE);
      if (lPers > 0)
      {
         sprintf(acTmp, "%*u", SIZ_OTHER, lPers);
         memcpy(pOutbuf+OFF_OTHER, acTmp, SIZ_OTHER);
         sprintf(acTmp, "%u         ", lPers);
         memcpy(pOutbuf+OFF_PERSPROP, acTmp, SIZ_PERSPROP);
      }

      // Gross total
      LONGLONG lGross = lLand+lImpr+lPers;
      if (lGross > 0)
      {
         sprintf(acTmp, "%*u", SIZ_GROSS, lGross);
         memcpy(pOutbuf+OFF_GROSS, acTmp, SIZ_GROSS);

         // Ratio
         if (lImpr > 0)
         {
            sprintf(acTmp, "%*u", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
            memcpy(pOutbuf+OFF_RATIO, acTmp, SIZ_RATIO);
         }
      }

      // HO Exempt
      memcpy(pOutbuf+OFF_EXE_CD1, pRec->Exe_Code1, RSIZ_EXE_CODE);
      memcpy(pOutbuf+OFF_EXE_CD2, pRec->Exe_Code2, RSIZ_EXE_CODE);
      memcpy(pOutbuf+OFF_EXE_CD3, pRec->Exe_Code3, RSIZ_EXE_CODE);

      // Create exemption type
      pRet = makeExeType(pOutbuf+OFF_EXE_TYPE, pOutbuf+OFF_EXE_CD1, pOutbuf+OFF_EXE_CD2, pOutbuf+OFF_EXE_CD3, (IDX_TBL4 *)&CCX_Exemption);

      if (!memcmp(pRec->Exe_Code1, "HO", 2))
         *(pOutbuf+OFF_HO_FL) = '1';      // 'Y'
      else
         *(pOutbuf+OFF_HO_FL) = '2';      // 'N'

      lExe = atoin(pRec->Exe_Amt1, RSIZ_EXE_AMT);
      lExe += atoin(pRec->Exe_Amt2, RSIZ_EXE_AMT);
      lExe += atoin(pRec->Exe_Amt3, RSIZ_EXE_AMT);
      if (lExe > 0)
      {
         sprintf(acTmp, "%*u", SIZ_EXE_TOTAL, lExe);
         memcpy(pOutbuf+OFF_EXE_TOTAL, acTmp, SIZ_EXE_TOTAL);
      }

      // Set full exempt flag
      *(pOutbuf+OFF_TAX_CODE)    = pRec->Tax_CodeA;
      *(pOutbuf+OFF_TAX_CODE+1)  = pRec->Tax_CodeB;
      if (pRec->Tax_CodeB > ' ' || lExe >= lGross)
         *(pOutbuf+OFF_FULL_EXEMPT) = 'Y';
      else
         *(pOutbuf+OFF_FULL_EXEMPT) = ' ';

      // Set Prop8 flag
      if (pRec->ValueResonCode[0] == 'P' && pRec->ValueResonCode[1] == '8')
         *(pOutbuf+OFF_PROP8_FLG) = 'Y';
   }

   // TRA
   if (pRec->TRA[0] > ' ')
   {
      iRet = sprintf(acTmp, "0%.*s", RSIZ_TRA, pRec->TRA);
      memcpy(pOutbuf+OFF_TRA, acTmp, iRet);
   }

#ifdef _DEBUG
   //if (!memcmp(pRollRec, "740303222", 9))
   //   iRet = 1;
#endif

   // Mailing
   Ccx_MergeMAdr(pOutbuf, &pRec->M_Adr.M_StrName[0], &pRec->M_Zip4[0]);

   // Situs
   Ccx_MergeSAdr(pOutbuf, &pRec->S_Adr.S_StrName[0], &pRec->S_Zip[0], &pRec->S_Zip4[0]);

   // UseCode
   if (pRec->UseCode[0] > ' ')
   {
      memcpy(pOutbuf+OFF_USE_CO, pRec->UseCode, RSIZ_USE_CODE);
      updateStdUse(pOutbuf+OFF_USE_STD, pRec->UseCode, RSIZ_USE_CODE, pOutbuf);
   } else
      memcpy(pOutbuf+OFF_USE_STD, USE_UNASGN, SIZ_USE_STD);

   // Owner
   try {
      Ccx_MergeOwner(pOutbuf, &pRec->OwnerCode1[0]);
   } catch (...)
   {
      iRet = 1;
   }

   // Legal
   if (pRec->Description[0] > ' ')
   {
      memcpy(acTmp, pRec->Description, RSIZ_DESCRIPTION);
      acTmp[RSIZ_DESCRIPTION] = 0;
      if (replChar(acTmp, 0xBA, 'C', 0))
         LogMsg("Fix bad char in Legal: %s", acTmp);
      updateLegal(pOutbuf, acTmp);
   }

   // Acreage: Lot sqft - Lot Acres
   lTmp = atoin(pRec->Acreage, RSIZ_ACREAGE);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*u", SIZ_LOT_ACRES, lTmp);
      memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);

      dTmp = (((double)lTmp/1000)*SQFT_PER_ACRE + 0.5);
      sprintf(acTmp, "%*u", SIZ_LOT_SQFT, (unsigned long)dTmp);
      memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);
   }

   // Zoning
   if (pRec->Zoning[0] > ' ')
      memcpy(pOutbuf+OFF_ZONE, pRec->Zoning, SIZ_ZONE);

   // Transfer - YYMMDD
   if (memcmp(pRec->CurDeedDate, "000000", 6) > 0)
   {
      // Clear all old sale info in current roll record
      ClearOldSale(pOutbuf);

      /*
      - DocNum format:
        + Before 1994 : 99999-999
        + 1994-present: YY999-999
      */
      // Apply transfer
      if (memcmp(pRec->CurDeedDate, &acToday[2], 2) <= 0)
         sprintf(acTmp, "20%.6s", pRec->CurDeedDate);
      else
         sprintf(acTmp, "19%.6s", pRec->CurDeedDate);
      memcpy(pOutbuf+OFF_TRANSFER_DT, acTmp, SIZ_TRANSFER_DT);
      iRet = Ccx_FormatDoc(acDocNum, pRec->CurDeedNum, NULL, acTmp);
      if (!iRet)
         memcpy(pOutbuf+OFF_TRANSFER_DOC, acDocNum, SIZ_TRANSFER_DOC);

      // Ignore sales if no DocNum, keep transfer date only
      if (!iRet)
      {
         // Sale 1
         memcpy(pOutbuf+OFF_SALE1_DOC, acDocNum, SIZ_TRANSFER_DOC);
         memcpy(pOutbuf+OFF_SALE1_DT, acTmp, SIZ_TRANSFER_DT);
         lTmp = atoin(pRec->SalePrice, RSIZ_SALE_PRICE);
         // Ignore sale price on book 900 & 901
         if (lTmp > 0 && !(!memcmp(pOutbuf, "90", 2) && *(pOutbuf+2) < '2'))
         {
            sprintf(acTmp, "%*u", SIZ_SALE1_AMT, lTmp);
            memcpy(pOutbuf+OFF_SALE1_AMT, acTmp, SIZ_SALE1_AMT);
         } else
            memset(pOutbuf+OFF_SALE1_AMT, ' ', SIZ_SALE1_AMT);

         // Sale 2
         if (memcmp(pRec->PriDeedDate, "000000", 6) > 0)
         {
            if (memcmp(pRec->PriDeedDate, "30", 2) < 0)
               sprintf(acTmp, "20%.6s", pRec->PriDeedDate);
            else
               sprintf(acTmp, "19%.6s", pRec->PriDeedDate);
            memcpy(pOutbuf+OFF_SALE2_DT, acTmp, SIZ_TRANSFER_DT);
            iRet = Ccx_FormatDoc(acDocNum, pRec->PriDeedNum, NULL, acTmp);
            if (!iRet)
               memcpy(pOutbuf+OFF_SALE2_DOC, acDocNum, SIZ_TRANSFER_DOC);
         }

         // Sale 3
         if (memcmp(pRec->PriDeedDate2, "000000", 6) > 0)
         {
            if (memcmp(pRec->PriDeedDate2, "30", 2) < 0)
               sprintf(acTmp, "20%.6s", pRec->PriDeedDate2);
            else
               sprintf(acTmp, "19%.6s", pRec->PriDeedDate2);
            memcpy(pOutbuf+OFF_SALE3_DT, acTmp, SIZ_TRANSFER_DT);
            iRet = Ccx_FormatDoc(acDocNum, pRec->PriDeedNum2, NULL, acTmp);
            if (!iRet)
               memcpy(pOutbuf+OFF_SALE3_DOC, acDocNum, SIZ_TRANSFER_DOC);
         }
      }
   }
}

/********************************* Ccx_MergeSrm ******************************
 *
 * Return 0 if successful, >0 if error
 *
 * Notes: The iLen parameter is not used.
 *
 *****************************************************************************/

void Ccx_MergeSrm(char *pOutbuf, char *pRollRec, int iLen, int iFlag)
{
   CCX_LIEN *pRec;
   char     acTmp[256], acTmp1[256], acDocNum[32], *pRet;
   long     lTmp, lExe;
   double   dTmp;
   int      iRet;

   pRec = (CCX_LIEN *)pRollRec;
#ifdef _DEBUG
   //iRet = sizeof(CCX_LIEN);
#endif

   if (iFlag & CREATE_R01)
   {
      // Clear output buffer
      memset(pOutbuf, ' ', iRecLen);

      // Start copying data
      memcpy(pOutbuf, pRec->Apn, RSIZ_APN);
      memcpy(pOutbuf+OFF_CO_NUM, "07CCX", 5);

      // Format APN
      iRet = formatApn(pRec->Apn, acTmp, &myCounty);
      memcpy(pOutbuf+OFF_APN_D, acTmp, iRet);

      // Create MapLink
      iRet = formatMapLink(pRec->Apn, acTmp, &myCounty);
      memcpy(pOutbuf+OFF_MAPLINK, acTmp, iRet);

      // Create index map link
      if (getIndexPage(acTmp, acTmp1, &myCounty))
         memcpy(pOutbuf+OFF_IMAPLINK, acTmp1, iRet);

      // Year assessed
      memcpy(pOutbuf+OFF_YR_ASSD, myCounty.acYearAssd, 4);

      // Land
      long lLand = atoin(pRec->Land, RSIZ_LAND_VALUE);
      if (lLand > 0)
      {
         sprintf(acTmp, "%*u", SIZ_LAND, lLand);
         memcpy(pOutbuf+OFF_LAND, acTmp, SIZ_LAND);
      }

      // Improve
      long lImpr = atoin(pRec->Impr, RSIZ_LAND_VALUE);
      if (lImpr > 0)
      {
         sprintf(acTmp, "%*u", SIZ_IMPR, lImpr);
         memcpy(pOutbuf+OFF_IMPR, acTmp, SIZ_IMPR);
      }

      // Other value
      long lPers = atoin(pRec->PP_Val, RSIZ_LAND_VALUE);
      if (lPers > 0)
      {
         sprintf(acTmp, "%*u", SIZ_OTHER, lPers);
         memcpy(pOutbuf+OFF_OTHER, acTmp, SIZ_OTHER);
         sprintf(acTmp, "%d         ", lPers);
         memcpy(pOutbuf+OFF_PERSPROP, acTmp, SIZ_PERSPROP);
      }

      // Gross total
      LONGLONG lGross = lLand+lImpr+lPers;
      if (lGross > 0)
      {
         sprintf(acTmp, "%*u", SIZ_GROSS, lGross);
         memcpy(pOutbuf+OFF_GROSS, acTmp, SIZ_GROSS);

         // Ratio
         if (lImpr > 0)
         {
            sprintf(acTmp, "%*u", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
            memcpy(pOutbuf+OFF_RATIO, acTmp, SIZ_RATIO);
         }
      }

      // Exemptions:     
      //    CG - College
      //    CH - Church
      //    CM - Cemetery
      //    DV - Disable veteran
      //    HO - Howmowners
      //    HP - Hospital 
      //    LC - School less then collegiate
      //    PL - Public Library
      //    PM - Public Museum
      //    PS - Public School
      //    RE - Religion
      //    RV - Regular veteran
      //    SC - Parochial school
      //    WE - Welfare

      // HO Exempt
      if (!memcmp(pRec->Exe_Code1, "HO", 2))
         *(pOutbuf+OFF_HO_FL) = '1';      // 'Y'
      else
         *(pOutbuf+OFF_HO_FL) = '2';      // 'N'

      // Exemption code
      memcpy(pOutbuf+OFF_EXE_CD1, pRec->Exe_Code1, RSIZ_EXE_CODE);
      memcpy(pOutbuf+OFF_EXE_CD2, pRec->Exe_Code2, RSIZ_EXE_CODE);
      memcpy(pOutbuf+OFF_EXE_CD3, pRec->Exe_Code3, RSIZ_EXE_CODE);

      // Create exemption type
      pRet = makeExeType(pOutbuf+OFF_EXE_TYPE, pOutbuf+OFF_EXE_CD1, pOutbuf+OFF_EXE_CD2, pOutbuf+OFF_EXE_CD3, (IDX_TBL4 *)&CCX_Exemption);

      lExe = atoin(pRec->Exe_Amt1, RSIZ_EXE_AMT);
      lExe += atoin(pRec->Exe_Amt2, RSIZ_EXE_AMT);
      lExe += atoin(pRec->Exe_Amt3, RSIZ_EXE_AMT);
      if (lExe > 0)
      {
         sprintf(acTmp, "%*u", SIZ_EXE_TOTAL, lExe);
         memcpy(pOutbuf+OFF_EXE_TOTAL, acTmp, SIZ_EXE_TOTAL);
      }

      // Set full exempt flag
      *(pOutbuf+OFF_TAX_CODE)    = pRec->Tax_CodeA;
      *(pOutbuf+OFF_TAX_CODE+1)  = pRec->Tax_CodeB;
      if (pRec->Tax_CodeB > ' ' || lExe >= lGross)
         *(pOutbuf+OFF_FULL_EXEMPT) = 'Y';
      else
         *(pOutbuf+OFF_FULL_EXEMPT) = ' ';

      // Set Prop8 flag
      if (pRec->ValueResonCode[0] == 'P' && pRec->ValueResonCode[1] == '8')
         *(pOutbuf+OFF_PROP8_FLG) = 'Y';
   }

#ifdef _DEBUG
   //if (!memcmp(pRollRec, "0351000245", 9))
   //   iRet = 1;
#endif

   // Alt_Apn
   if (*(pOutbuf+OFF_ALT_APN) < '0')
      memcpy(pOutbuf+OFF_ALT_APN, pRec->Apn, CSIZ_APN);

   // TRA
   if (pRec->TRA[0] > ' ')
   {
      iRet = sprintf(acTmp, "0%.*s", RSIZ_TRA, pRec->TRA);
      memcpy(pOutbuf+OFF_TRA, acTmp, iRet);
   }

   // Mailing
   Ccx_MergeMAdr(pOutbuf, &pRec->M_Adr.M_StrName[0], &pRec->M_Zip4[0]);

   // Situs
   Ccx_MergeSAdr(pOutbuf, &pRec->S_Adr.S_StrName[0], &pRec->S_Zip[0], &pRec->S_Zip4[0]);

   // UseCode
   if (pRec->UseCode[0] > ' ')
   {
      memcpy(pOutbuf+OFF_USE_CO, pRec->UseCode, RSIZ_USE_CODE);
      updateStdUse(pOutbuf+OFF_USE_STD, pRec->UseCode, RSIZ_USE_CODE, pOutbuf);
   } else
      memcpy(pOutbuf+OFF_USE_STD, USE_UNASGN, SIZ_USE_STD);

   // Owner
   try {
      Ccx_MergeOwner(pOutbuf, &pRec->OwnerCode1[0]);
   } catch (...)
   {
      iRet = 1;
   }

   // Legal
   if (pRec->Description[0] > ' ')
   {
      memcpy(acTmp, pRec->Description, RSIZ_DESCRIPTION);
      acTmp[RSIZ_DESCRIPTION] = 0;
      if (replChar(acTmp, 0xBA, 'C', 0) || replChar(acTmp, 0x8D, 'C', 0))
         LogMsg("Fix bad char in Legal: %s", acTmp);
      updateLegal(pOutbuf, acTmp);
   }

   // Acreage: Lot sqft - Lot Acres
   lTmp = atoin(pRec->Acreage, RSIZ_ACREAGE);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*u", SIZ_LOT_ACRES, lTmp);
      memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);

      dTmp = (((double)lTmp/1000)*SQFT_PER_ACRE + 0.5);
      sprintf(acTmp, "%*u", SIZ_LOT_SQFT, (unsigned long)dTmp);
      memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);
   }

   // Zoning
   memset(pOutbuf+OFF_ZONE, ' ', SIZ_ZONE);
   if (pRec->Zoning[0] > ' ')
   {
      memcpy(pOutbuf+OFF_ZONE, pRec->Zoning, SIZ_ZONE);
      if (*(pOutbuf+OFF_ZONE_X1) == ' ')
         vmemcpy(pOutbuf+OFF_ZONE_X1, pRec->Zoning, SIZ_ZONE_X1, SIZ_CCX_ZONING);
   }

   // Transfer - YYMMDD
   if (memcmp(pRec->CurDeedDate, "000000", 6) > 0)
   {
      // Clear all old sale info in current roll record
      ClearOldSale(pOutbuf);

      // Apply transfer
      memcpy(pOutbuf+OFF_TRANSFER_DT, pRec->CurDeedDate, SIZ_TRANSFER_DT);
      iRet = Ccx_FormatDoc(acDocNum, pRec->CurDeedNum, NULL, pRec->CurDeedDate);
      if (!iRet)
         memcpy(pOutbuf+OFF_TRANSFER_DOC, acDocNum, SIZ_TRANSFER_DOC);

      // Ignore sales if no DocNum, keep transfer date only
      if (!iRet)
      {
         // Sale 1
         memcpy(pOutbuf+OFF_SALE1_DOC, acDocNum, SIZ_TRANSFER_DOC);
         memcpy(pOutbuf+OFF_SALE1_DT, pRec->CurDeedDate, SIZ_TRANSFER_DT);
         lTmp = atoin(pRec->SalePrice, RSIZ_SALE_PRICE);
         // Ignore sale price on book 900 & 901
         if (lTmp > 0 && !(!memcmp(pOutbuf, "90", 2) && *(pOutbuf+2) < '2'))
         {
            sprintf(acTmp, "%*u", SIZ_SALE1_AMT, lTmp);
            memcpy(pOutbuf+OFF_SALE1_AMT, acTmp, SIZ_SALE1_AMT);
         } else
            memset(pOutbuf+OFF_SALE1_AMT, ' ', SIZ_SALE1_AMT);

         // Sale 2
         if (memcmp(pRec->PriDeedDate, "000000", 6) > 0)
         {
            memcpy(pOutbuf+OFF_SALE2_DT, pRec->PriDeedDate, SIZ_TRANSFER_DT);
            iRet = Ccx_FormatDoc(acDocNum, pRec->PriDeedNum, NULL, pRec->PriDeedDate);
            if (!iRet)
               memcpy(pOutbuf+OFF_SALE2_DOC, acDocNum, SIZ_TRANSFER_DOC);
         }

         // Sale 3
         if (memcmp(pRec->PriDeedDate2, "000000", 6) > 0)
         {
            memcpy(pOutbuf+OFF_SALE3_DT, pRec->PriDeedDate2, SIZ_TRANSFER_DT);
            iRet = Ccx_FormatDoc(acDocNum, pRec->PriDeedNum2, NULL, pRec->PriDeedDate2);
            if (!iRet)
               memcpy(pOutbuf+OFF_SALE3_DOC, acDocNum, SIZ_TRANSFER_DOC);
         }
      }
   }
}

/********************************* Ccx_Load_Roll ******************************
 *
 *
 ******************************************************************************/

int Ccx_Load_Roll(int iSkip)
{
   char     *pTmp, acRec[MAX_RECSIZE], acBuf[MAX_RECSIZE];
   char     acRawFile[_MAX_PATH], acOutFile[_MAX_PATH], acRollRec[MAX_RECSIZE];

   HANDLE   fhIn, fhOut;

   int      iRet, iTmp, iRollUpd=0, iNewRec=0, iRetiredRec=0;
   DWORD    nBytesRead;
   DWORD    nBytesWritten;
   BOOL     bRet, bEof;
   long     lRet=0, lCnt=0;

   LogMsg0("Load CCX Roll update ...");
   lLastRecDate = 0;

   sprintf(acRawFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "S01");
   sprintf(acOutFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "R01");

   // Rename files for processing
   if (_access(acRawFile, 0))
   {
      if (!_access(acOutFile, 0))
         rename(acOutFile, acRawFile);
      else
      {
         LogMsg("***** Missing input file %s.  Please recheck!", acRawFile);
         return 1;
      }
   }

   // Open Roll file
   LogMsg("Open Roll file %s", acRollFile);
   fdRoll = fopen(acRollFile, "r");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", acRollFile);
      return 2;
   }

   // Open Char file
   if (!_access(acCharFile, 0))
   {
      LogMsg("Open Char file %s", acCharFile);
      fdChar = fopen(acCharFile, "r");
      if (fdChar == NULL)
      {
         LogMsg("***** Error opening Char file: %s\n", acCharFile);
         return 2;
      }
   }

   // Open Cum Char file
   if (!_access(acCChrFile, 0))
   {
      LogMsg("Open cumulative Char file %s", acCChrFile);
      fdCChr = fopen(acCChrFile, "r");
      if (fdCChr == NULL)
      {
         LogMsg("***** Error opening Cum Char file: %s\n", acCChrFile);
         return 2;
      }
   }

   // Open Input file
   LogMsg("Open input file %s", acRawFile);
   fhIn = CreateFile(acRawFile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);
   if (fhIn == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening input file: %s\n", acRawFile);
      return 2;
   }

   // Open Output file
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening output file: %s\n", acOutFile);
      return 2;
   }

   // Get first RollRec
   pTmp = fgets((char *)&acRollRec[0], MAX_RECSIZE, fdRoll);
   bEof = (pTmp ? false:true);

   // Copy skip record
   memset(acBuf, ' ', iRecLen);
   while (iSkip-- > 0)
   {
      ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);
      // Ignore record start with 99999999
      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
   }

   iNoMatch=iBadCity=iBadSuffix=0;

   // Merge loop
   while (!bEof)
   {
      bRet = ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);

      // Check for EOF
      if (!bRet)
      {
         LogMsg("***** Error reading input file %s (%f)", acRawFile, GetLastError());
         break;
      }

      // EOF ?
      if (iRecLen != nBytesRead)
         break;

      NextRollRec:
#ifdef _DEBUG
      //if (!memcmp(acBuf, "004020015", 9))
      //   iRet = 1;
#endif
      iTmp = memcmp(acBuf, acRollRec, iApnLen);
      if (!iTmp)
      {
         // Merge roll data
         if (iSrmVersion == 1)
            Ccx_MergeRoll(acBuf, acRollRec, iRollLen, UPDATE_R01);
         else
            Ccx_MergeSrm(acBuf, acRollRec, iRollLen, UPDATE_R01);        
         iRollUpd++;

         // Merge Char
         if (fdChar)
            iRet = Ccx_MergeChar(acBuf);

         // Merge Cum Char
         if (fdCChr)
            iRet = Ccx_MergeCChr(acBuf);

         // Get last recording date
         iTmp = atoin((char *)&acBuf[OFF_TRANSFER_DT], SIZ_TRANSFER_DT);
         if (iTmp >= lToday)
            LogMsg0("*** WARNING: Bad last recording date %d at record# %d -->%.10s", iTmp, lCnt, acBuf);
         else if (lLastRecDate < iTmp)
            lLastRecDate = iTmp;

         // Write output
         bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);

         // Read next roll record
         pTmp = fgets(acRollRec, MAX_RECSIZE, fdRoll);

         if (!pTmp)
            bEof = true;    // Signal to stop
      } else if (iTmp > 0)       // Roll not match, new roll record?
      {
         LogMsg0("*** New roll record : %.*s (%d) ***", iApnLen, acRollRec, lCnt);

         // Create new R01 record
         if (iSrmVersion == 1)
            Ccx_MergeRoll(acRec, acRollRec, iRollLen, CREATE_R01);
         else
            Ccx_MergeSrm(acRec, acRollRec, iRollLen, CREATE_R01);        
         iNewRec++;

         // Merge Char
         if (fdChar)
            iRet = Ccx_MergeChar(acRec);

         // Merge Cum Char
         if (fdCChr)
            iRet = Ccx_MergeCChr(acRec);

         // Get last recording date
         iTmp = atoin((char *)&acRec[OFF_TRANSFER_DT], SIZ_TRANSFER_DT);
         if (iTmp >= lToday)
            LogMsg0("*** WARNING: Bad last recording date %d at record# %d -->%.10s", iTmp, lCnt, acRec);
         else if (lLastRecDate < iTmp)
            lLastRecDate = iTmp;

         bRet = WriteFile(fhOut, acRec, iRecLen, &nBytesWritten, NULL);
         lCnt++;

         // Get next roll record
         pTmp = fgets(acRollRec, MAX_RECSIZE, fdRoll);

         if (!pTmp)
            bEof = true;    // Signal to stop
         else
            goto NextRollRec;
      } else
      {
         // Record may be retired
         if (bDebug)
            LogMsg0("*** Roll not match (retired record?) : R01->%.*s < Roll->%.*s (%d) ***", iApnLen, acBuf, iApnLen, acRollRec, lCnt);
         iRetiredRec++;
         continue;
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);

      if (!bRet)
      {
         LogMsg("Error occurs: %d\n", GetLastError());
         lRet = WRITE_ERR;
         break;
      }
   }

   // Do the rest of the file
   while (!bEof)
   {
      LogMsg0("*** New roll record : %.*s (%d) ***", iApnLen, acRollRec, lCnt);

      // Create new R01 record
      if (iSrmVersion == 1)
         Ccx_MergeRoll(acRec, acRollRec, iRollLen, CREATE_R01);
      else
         Ccx_MergeSrm(acRec, acRollRec, iRollLen, CREATE_R01);        
      iNewRec++;

      // Merge Char
      if (fdChar)
         iRet = Ccx_MergeChar(acRec);

      // Merge Cum Char
      if (fdCChr)
         iRet = Ccx_MergeCChr(acRec);

      // Get last recording date
      iTmp = atoin((char *)&acRec[OFF_TRANSFER_DT], SIZ_TRANSFER_DT);
      if (iTmp >= lToday)
         LogMsg0("*** WARNING: Bad last recording date %d at record# %d -->%.10s", iTmp, lCnt, acRec);
      else if (lLastRecDate < iTmp)
         lLastRecDate = iTmp;

      bRet = WriteFile(fhOut, acRec, iRecLen, &nBytesWritten, NULL);
      lCnt++;

      // Get next roll record
      pTmp = fgets(acRollRec, MAX_RECSIZE, fdRoll);

      if (!pTmp)
         bEof = true;    // Signal to stop
   }

   // Close files
   if (fdRoll)
      fclose(fdRoll);
   if (fdChar)
      fclose(fdChar);
   if (fdCChr)
      fclose(fdCChr);

   if (fhOut)
      CloseHandle(fhOut);
   if (fhIn)
      CloseHandle(fhIn);

   LogMsg("Total output records:       %u", lCnt);
   LogMsg("Total new records:          %u", iNewRec);
   LogMsg("Total retired records:      %u", iRetiredRec);
   LogMsg("Total Char matched:         %u", lCharMatch);
   LogMsg("Total Char skipped:         %u", lCharSkip);
   LogMsg("Total Sale Char matched:    %u", lCChrMatch);
   LogMsg("Total Sale Char skipped:    %u", lCChrSkip);

   LogMsg("Total bad-city records:     %u", iBadCity);
   LogMsg("Total bad-suffix records:   %u\n", iBadSuffix);

   printf("\nTotal output records: %u\n", lCnt);

   lRecCnt = lCnt;
   return 0;
}

/********************************* Ccx_MergeLien *****************************
 *
 * Return 0 if successful, >0 if error
 *
 * Notes: The iLen parameter is not used.
 *
 *****************************************************************************/

void Ccx_MergeLien(char *pOutbuf, char *pRollRec, int iLen, int iFlag)
{
   CCX_LIEN *pRec;
   char     acTmp[256], acTmp1[256], acDocNum[32];
   long     lTmp;
   double   dTmp;
   int      iRet;

   pRec = (CCX_LIEN *)pRollRec;

   // Clear output buffer
   memset(pOutbuf, ' ', iRecLen);

   // Start copying data
   memcpy(pOutbuf, pRec->Apn, RSIZ_APN);
   memcpy(pOutbuf+OFF_CO_NUM, "07CCX", 5);

   // Format APN
   iRet = formatApn(pRec->Apn, acTmp, &myCounty);
   memcpy(pOutbuf+OFF_APN_D, acTmp, iRet);

   // Alt_Apn
   memcpy(pOutbuf+OFF_ALT_APN, pRec->Apn, CSIZ_APN);

   // Create MapLink
   iRet = formatMapLink(pRec->Apn, acTmp, &myCounty);
   memcpy(pOutbuf+OFF_MAPLINK, acTmp, iRet);

   // Create index map link
   if (getIndexPage(acTmp, acTmp1, &myCounty))
      memcpy(pOutbuf+OFF_IMAPLINK, acTmp1, iRet);

   // Year assessed
   memcpy(pOutbuf+OFF_YR_ASSD, myCounty.acYearAssd, 4);

   // Land
   long lLand = atoin(pRec->Land, RSIZ_LAND_VALUE);
   if (lLand > 0)
   {
      sprintf(acTmp, "%*u", SIZ_LAND, lLand);
      memcpy(pOutbuf+OFF_LAND, acTmp, SIZ_LAND);
   }

   // Improve
   long lImpr = atoin(pRec->Impr, RSIZ_LAND_VALUE);
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*u", SIZ_IMPR, lImpr);
      memcpy(pOutbuf+OFF_IMPR, acTmp, SIZ_IMPR);
   }

   // Other value
   long lPers = atoin(pRec->PP_Val, RSIZ_LAND_VALUE);
   if (lPers > 0)
   {
      sprintf(acTmp, "%*u", SIZ_OTHER, lPers);
      memcpy(pOutbuf+OFF_OTHER, acTmp, SIZ_OTHER);
      sprintf(acTmp, "%u         ", lPers);
      memcpy(pOutbuf+OFF_PERSPROP, acTmp, SIZ_PERSPROP);
   }

   // Gross total
   LONGLONG lGross = lLand+lImpr+lPers;
   if (lGross > 0)
   {
      sprintf(acTmp, "%*u", SIZ_GROSS, lGross);
      memcpy(pOutbuf+OFF_GROSS, acTmp, SIZ_GROSS);

      // Ratio
      if (lImpr > 0)
      {
         sprintf(acTmp, "%*u", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
         memcpy(pOutbuf+OFF_RATIO, acTmp, SIZ_RATIO);
      }
   }

   // HO Exempt
   memcpy(pOutbuf+OFF_EXE_CD1, pRec->Exe_Code1, SIZ_CCX_EXEMPTION_CODE_1);
   memcpy(pOutbuf+OFF_EXE_CD2, pRec->Exe_Code2, SIZ_CCX_EXEMPTION_CODE_1);
   memcpy(pOutbuf+OFF_EXE_CD3, pRec->Exe_Code3, SIZ_CCX_EXEMPTION_CODE_1);

   // Create exemption type
   makeExeType(pOutbuf+OFF_EXE_TYPE, pOutbuf+OFF_EXE_CD1, pOutbuf+OFF_EXE_CD2, pOutbuf+OFF_EXE_CD3, (IDX_TBL4 *)&CCX_Exemption);

   if (!memcmp(pRec->Exe_Code1, "HO", 2))
      *(pOutbuf+OFF_HO_FL) = '1';      // 'Y'
   else
      *(pOutbuf+OFF_HO_FL) = '2';      // 'N'
   long lExe = atoin(pRec->Exe_Amt1, RSIZ_EXE_AMT);
   lExe += atoin(pRec->Exe_Amt2, RSIZ_EXE_AMT);
   lExe += atoin(pRec->Exe_Amt3, RSIZ_EXE_AMT);
   if (lExe > 0)
   {
      sprintf(acTmp, "%*u", SIZ_EXE_TOTAL, lExe);
      memcpy(pOutbuf+OFF_EXE_TOTAL, acTmp, SIZ_EXE_TOTAL);
   }

   // TRA
   if (pRec->TRA[0] > ' ')
   {
      iRet = sprintf(acTmp, "0%.*s", RSIZ_TRA, pRec->TRA);
      memcpy(pOutbuf+OFF_TRA, acTmp, iRet);
   }

   // Set full exempt flag
   *(pOutbuf+OFF_TAX_CODE)    = pRec->Tax_CodeA;
   *(pOutbuf+OFF_TAX_CODE+1)  = pRec->Tax_CodeB;
   if (pRec->Tax_CodeB > ' ' || lExe == lGross)
      *(pOutbuf+OFF_FULL_EXEMPT) = 'Y';
   else
      *(pOutbuf+OFF_FULL_EXEMPT) = ' ';

   // Set Prop8 flag
   if (pRec->ValueResonCode[0] == 'P' && pRec->ValueResonCode[1] == '8')
      *(pOutbuf+OFF_PROP8_FLG) = 'Y';

   // Situs
   //Ccx_MergeLdrSAdr(pOutbuf, pRollRec);

   // Mailing
   //Ccx_MergeLdrMAdr(pOutbuf, pRollRec);
   Ccx_MergeMAdr(pOutbuf, &pRec->M_Adr.M_StrName[0], &pRec->M_Zip4[0]);

   // Situs
   Ccx_MergeSAdr(pOutbuf, &pRec->S_Adr.S_StrName[0], &pRec->S_Zip[0], &pRec->S_Zip4[0]);

   // UseCode
   if (pRec->UseCode[0] > ' ')
   {
      memcpy(pOutbuf+OFF_USE_CO, pRec->UseCode, RSIZ_USE_CODE);
      updateStdUse(pOutbuf+OFF_USE_STD, pRec->UseCode, RSIZ_USE_CODE, pOutbuf);
   } else
      memcpy(pOutbuf+OFF_USE_STD, USE_UNASGN, SIZ_USE_STD);

#ifdef _DEBUG
   //if (!memcmp(pRollRec, "003120007", 9))
   //   iRet = 1;
#endif

   // Owner
   try {
      Ccx_MergeOwner(pOutbuf, &pRec->OwnerCode1[0]);
   } catch (...)
   {
      iRet = 1;
   }

   // Legal
   if (pRec->Description[0] > ' ')
   {
      memcpy(acTmp, pRec->Description, RSIZ_DESCRIPTION);
      acTmp[RSIZ_DESCRIPTION] = 0;
      if ((iRet=strlen(acTmp)) < RSIZ_DESCRIPTION)
      {
         if (acTmp[iRet+1] == 'I' &&  acTmp[iRet+2] == 'G')
            acTmp[iRet] = 'R';
         else if (iRet > 5 && !memcmp(&acTmp[iRet-5], "JUNTA", 5))
            acTmp[iRet] = 'S';
         else
            acTmp[iRet] = ' ';
         LogMsg("*** Null character in legal at %.12s (pos:%d).  Replace with %c.", pOutbuf, iRet, acTmp[iRet]);
      }

      // Replace known bad char
      replChar(acTmp, 0x8D, 'C');
      iRet = remUnPrtChar(acTmp);
      if (iRet < RSIZ_DESCRIPTION)
         LogMsg("*** Bad character in legal at %.12s (pos:%d)", pOutbuf, iRet);

      updateLegal(pOutbuf, acTmp);
   }

   // Acreage: Lot sqft - Lot Acres
   lTmp = atoin(pRec->Acreage, RSIZ_ACREAGE);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*u", SIZ_LOT_ACRES, lTmp);
      memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);

      dTmp = (((double)lTmp/1000)*SQFT_PER_ACRE + 0.5);
      sprintf(acTmp, "%*u", SIZ_LOT_SQFT, (unsigned long)dTmp);
      memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);
   }

   // Zoning
   if (pRec->Zoning[0] > ' ')
   {
      memcpy(pOutbuf+OFF_ZONE, pRec->Zoning, SIZ_ZONE);
      vmemcpy(pOutbuf+OFF_ZONE_X1, pRec->Zoning, SIZ_ZONE_X1, SIZ_CCX_ZONING);
   }

   // Transfer - YYYYMMDD
   if (memcmp(pRec->CurDeedDate, "000000", 6) > 0)
   {
      /*
      - DocNum format:
        + Before 1994 : 99999-999
        + 1994-present: YY999-999
      */
      // Apply transfer
      memcpy(pOutbuf+OFF_TRANSFER_DT, pRec->CurDeedDate, SIZ_TRANSFER_DT);
      iRet = Ccx_FormatDoc(acDocNum, pRec->CurDeedNum, NULL, pRec->CurDeedDate);
      if (!iRet)
         memcpy(pOutbuf+OFF_TRANSFER_DOC, acDocNum, SIZ_TRANSFER_DOC);

      // Sale 1
      lTmp = atoin(pRec->SalePrice, RSIZ_SALE_PRICE);
      // Ignore sale price on book 900 & 901
      if (lTmp > 0 && !(!memcmp(pOutbuf, "90", 2) && *(pOutbuf+2) < '2'))
      {
         memcpy(pOutbuf+OFF_SALE1_DT, pRec->CurDeedDate, SIZ_TRANSFER_DT);
         memcpy(pOutbuf+OFF_SALE1_DOC, acDocNum, SIZ_TRANSFER_DOC);
         sprintf(acTmp, "%*u", SIZ_SALE1_AMT, lTmp);
         memcpy(pOutbuf+OFF_SALE1_AMT, acTmp, SIZ_SALE1_AMT);
      }
   }
}

/********************************* Ccx_Load_LDR *****************************
 *
 *
 ****************************************************************************/

int Ccx_Load_LDR(int iFirstRec /* 1=create header rec */)
{
   char     *pTmp, acBuf[MAX_RECSIZE], acRollRec[MAX_RECSIZE];
   char     acOutFile[_MAX_PATH];

   HANDLE   fhOut;

   DWORD    nBytesWritten;
   BOOL     bRet;
   long     lRet=0, lCnt=0;
   int      iRet, iRollUpd=0;

   LogMsg0("Loading LDR file ...");
   sprintf(acOutFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "R01");

   // Open Roll file
   LogMsg("Open ldr file %s", acLienFile);
   fdRoll = fopen(acLienFile, "r");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening ldr file: %s\n", acLienFile);
      return 2;
   }

   // Open Char file
   if (!_access(acCharFile, 0))
   {
      LogMsg("Open Char file %s", acCharFile);
      fdChar = fopen(acCharFile, "r");
      if (fdChar == NULL)
      {
         LogMsg("***** Error opening Char file: %s\n", acCharFile);
         return 2;
      }
   }

   // Open Cum Char file
   if (!_access(acCChrFile, 0))
   {
      LogMsg("Open cum Char file %s", acCChrFile);
      fdCChr = fopen(acCChrFile, "r");
      if (fdCChr == NULL)
      {
         LogMsg("***** Error opening Cum Char file: %s\n", acCChrFile);
         return 2;
      }
   }

   // Open Output file
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error creating output file %s", acOutFile);
      return 4;
   }

   // Create header record
   if (iFirstRec)
   {
      memset(acBuf, '9', iRecLen);
      // Ignore record start with 99999999
      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
   }

   iNoMatch=iBadCity=iBadSuffix=0;

   // Get first RollRec
   pTmp = fgets((char *)&acRollRec[0], MAX_RECSIZE, fdRoll);
   memset(acBuf, ' ', iRecLen);

   // Merge loop
   while (pTmp)
   {
#ifdef _DEBUG
      //if (!memcmp(acRollRec, "133473029", 9))
      //   iRet = 1;
#endif

      // Create new R01 record
      Ccx_MergeLien(acBuf, acRollRec, iRollLen, CREATE_R01);

      // Merge Char
      if (fdChar)
         iRet = Ccx_MergeChar(acBuf);

      // Merge Cum Char
      if (fdCChr)
         iRet = Ccx_MergeCChr(acBuf);

      // Get last recording date
      iRet = atoin((char *)&acBuf[OFF_TRANSFER_DT], SIZ_TRANSFER_DT);
      if (iRet >= lToday)
         LogMsg0("*** WARNING: Bad last recording date %d at record# %d -->%.10s", iRet, lCnt, acBuf);
      else if (lLastRecDate < iRet)
         lLastRecDate = iRet;

      lLDRRecCount++;
      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
      if (!bRet)
      {
         LogMsg("***** Error writing to output file: %d\n", GetLastError());
         lRet = WRITE_ERR;
         break;
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);

      // Get next roll record
      pTmp = fgets(acRollRec, MAX_RECSIZE, fdRoll);
   }

   // Close files
   if (fdRoll)
      fclose(fdRoll);
   if (fdChar)
      fclose(fdChar);
   if (fdCChr)
      fclose(fdCChr);

   if (fhOut)
      CloseHandle(fhOut);

   LogMsg("Total input records:        %u", lCnt);
   LogMsg("Total output records:       %u", lLDRRecCount);
   LogMsg("Total Char matched:         %u", lCharMatch);
   LogMsg("Total Char skipped:         %u", lCharSkip);
   LogMsg("Total Sale Char matched:    %u", lCChrMatch);
   LogMsg("Total Sale Char skipped:    %u", lCChrSkip);

   LogMsg("Total bad-city records:     %u", iBadCity);
   LogMsg("Total bad-suffix records:   %u\n", iBadSuffix);

   printf("\nTotal output records: %u\n", lCnt);

   lRecCnt = lLDRRecCount;
   return 0;
}

/********************************* formatCSale *******************************
 *
 * Return -1 if bad record.  0 if OK.
 *
 *****************************************************************************/

int Ccx_FormatCSale(char *pFmtSale, char *pSaleRec)
{

   CCX_SALE *pSale = (CCX_SALE *)pSaleRec;
   CCX_CSAL *pHSale= (CCX_CSAL *)pFmtSale;
   long     lTmp;
   int      iDeedVol, iDeedPage;
   char     acTmp[32];

   // Inititalize
   memset(pFmtSale, ' ', sizeof(CCX_CSAL));

   // Deed type & Pdr type
   pHSale->DeedType = pSale->DeedType;
   pHSale->PdrType = pSale->PdrType;
   iDeedVol  = atoin(pSale->DeedVol, SSIZ_DEED_REF_VOL);
   iDeedPage = atoin(pSale->DeedPage, SSIZ_DEED_REF_PAGE);
   if ((pSale->DeedType == ' ')  ||
       (!iDeedVol && !iDeedPage) ||
       (iDeedVol == 99999 && iDeedPage == 99999))
      return -1;

   // SaleDate
   if (pSale->SaleDate[0] > '0')
      memcpy(pHSale->SaleDate, pSale->SaleDate, SSIZ_SALE_DATE);

   // APN
   memcpy(pHSale->Apn, pSale->Apn, SSIZ_APN);

   // BldgNum
   memcpy(pHSale->BldgNum, pSale->BldgNum,  SSIZ_BLDG_NUMBER);

   // RecPage - DocNum format:
   //             + Before 1994 : 99999-999
   //             + 1994-present: YY999-999
   sprintf(acTmp, "%*u", SSIZ_DEED_REF_VOL, iDeedVol);
   memcpy(pHSale->DeedVol, acTmp, SSIZ_DEED_REF_VOL);
   sprintf(acTmp, "%*u", SSIZ_DEED_REF_PAGE, iDeedPage);
   memcpy(pHSale->DeedPage, acTmp, SSIZ_DEED_REF_PAGE);

   // PriceFromStamp
   lTmp = atoin(pSale->PriceFromStamp, SSIZ_PRICE_FROM_STAMPS);
   if (lTmp > 9)
   {
      sprintf(acTmp, "%*u", SSIZ_PRICE_FROM_STAMPS, lTmp);
      memcpy(pHSale->PriceFromStamp, acTmp, SSIZ_PRICE_FROM_STAMPS);
   }

   // Confirmed Price
   lTmp = atoin(pSale->Conf_Price, SSIZ_CONFIRMED_PRICE);
   if (lTmp > 9)
   {
      sprintf(acTmp, "%*u", SSIZ_CONFIRMED_PRICE, lTmp);
      memcpy(pHSale->Conf_Price, acTmp, SSIZ_CONFIRMED_PRICE);
   }

   // Confirmation Code
   pHSale->Conf_Code = pSale->Conf_Code;

   // Appraisal Code
   pHSale->Appr_Code = pSale->Appr_Code;

   // Appraisal Base Year
   pHSale->Appr_BaseYr = pSale->Appr_BaseYr;

   // Appraisal Percentage
   lTmp = atoin(pSale->Appr_Pct, 3);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%3d.%c", lTmp, pSale->Appr_Pct[3]);
      memcpy(pHSale->Appr_Pct, acTmp, 5);
   }

   // Appraisal value
   memcpy(pHSale->Appr_Val, pSale->Appr_Val, sizeof(pHSale->Appr_Val));

   // Activity number
   memcpy(pHSale->Activity_Num, pSale->Activity_Num, sizeof(pHSale->Activity_Num));

   // Sale comments
   memcpy(pHSale->Remark1, pSale->Remark1, SSIZ_REMARK_1+SSIZ_REMARK_2);

   return 0;
}

/********************************* Ccx_FormatSale ****************************
 *
 * Return -1 if bad record.  0 if OK.
 *
 *****************************************************************************/

int Ccx_FormatSale(char *pFmtSale, char *pSale)
{
   SCSAL_REC *pCSale = (SCSAL_REC *)pFmtSale;
   CCX_SALE  *pSaleRec = (CCX_SALE *)pSale;
   char      acTmp[32];
	int		 iRet, lPrice;

   memset(pFmtSale, ' ', sizeof(SCSAL_REC));

   memcpy(pCSale->Apn, pSaleRec->Apn, SSIZ_APN);
   iRet = Ccx_FormatDoc(acTmp, pSaleRec->DeedVol, pSaleRec->DeedPage, pSaleRec->SaleDate);
	memcpy(pCSale->DocNum, acTmp, SIZ_SALE1_DOC);

   memcpy(pCSale->DocDate, pSaleRec->SaleDate, SSIZ_SALE_DATE);
   // Translate doctype
   if (pSaleRec->DeedType == 'G')
      pCSale->DocType[0]= '1';
   else if (pSaleRec->DeedType == 'Q')
      pCSale->DocType[0]= '4';
   else if (pSaleRec->DeedType == 'U')
      memcpy(pCSale->DocType, "76", 2);
   else
      pCSale->DocType[0]= pSaleRec->DeedType;


   // Strip out all sale price < 10 for hiding request 
   lPrice = atoin(pSaleRec->PriceFromStamp, SSIZ_PRICE_FROM_STAMPS);
   if (lPrice > 10)
   {
      sprintf(acTmp, "%*u", SIZ_SALE1_AMT, lPrice);
      memcpy(pCSale->SalePrice, acTmp, SIZ_SALE1_AMT);
   }

   lPrice = atoin(pSaleRec->Conf_Price, SSIZ_PRICE_FROM_STAMPS);
   if (lPrice > 10)
   {
      sprintf(acTmp, "%*u", SIZ_SALE1_AMT, lPrice);
      memcpy(pCSale->ConfirmedSalePrice, acTmp, SIZ_SALE1_AMT);
   }

   // Set full/partial sale flag
   iRet = atoin(pSaleRec->Appr_Pct, 4);
   if (pSaleRec->Appr_Code == 'R' && iRet > 0) 
   {
      if (iRet == 1000)
         pCSale->SaleCode[0] = 'F';
      else
         pCSale->SaleCode[0] = 'P';

      if (iRet > 0)
      {
         iRet = sprintf(acTmp, "%d", (int)((iRet+5)/10.0));
         memcpy(pCSale->PctXfer, acTmp, iRet);
      }
   }

   // Set multi-parcel flag
   if (pSaleRec->Rej_Code == '3')
      pCSale->MultiSale_Flg = 'Y';

   pCSale->CRLF[0] = '\n';
   pCSale->CRLF[1] = 0;

   return 0;
}

/****************************** Ccx_FormatPdrChar ****************************
 *
 * Return -1 if bad record.  0 if OK.
 *
 *****************************************************************************/

int Ccx_FormatPdrChar(char *pCharRec, char *pPdrRec, bool initFlg=true)
{
   CCX_CHAR *pPdr  = (CCX_CHAR *)pPdrRec;
   CCX_CCHR *pChar = (CCX_CCHR *)pCharRec;

   char     acTmp[256];
   long     lTmp, lBldgSqft, lGarSqft;
   int      iRet, iTmp, iBeds, iFBath, iHBath, iRooms, iBldgNum;

   iRet=iBeds=iFBath=iHBath=iRooms=0;
   lBldgSqft=lGarSqft=0;

   // Inititalize
   if (initFlg)
   {
      memset(pCharRec, ' ', sizeof(CCX_CCHR));
      memcpy(pChar->Apn, pPdr->Apn, CSIZ_APN);

      iBldgNum = atoin(pPdr->BldgNum, CSIZ_BLDG_NUMBER);
      memcpy(pChar->BldgNum, pPdr->BldgNum, CSIZ_BLDG_NUMBER);

      lTmp = atoin(pPdr->EffYear, CSIZ_EFF_YR);
      if (lTmp > 1700)
         memcpy(pChar->EffYear, pPdr->EffYear, SIZ_YR_BLT);

      // YrBlt
      lTmp = atoin(pPdr->YearBuilt, CSIZ_EFF_YR);
      if (lTmp > 1700)
         memcpy(pChar->YearBuilt, pPdr->YearBuilt, SIZ_YR_BLT);

      // BldgSqft
      lBldgSqft += atoin(pPdr->BldgSqft, CSIZ_BLDG_SQFT);
      iTmp = 0;
      if (lBldgSqft > 10)
      {
         iTmp = 1;
         lTmp = atoin(pPdr->BldgSqft2, CSIZ_BLDG_SQFT);
         if (lTmp > 0)
         {
            iTmp++;
            lBldgSqft += lTmp;

            // Stories
            sprintf(acTmp, "%*u", SIZ_STORIES, iTmp*10);
            memcpy(pChar->Stories, acTmp, SIZ_STORIES);
         }
         sprintf(acTmp, "%*u", SIZ_BLDG_SF, lBldgSqft);
         memcpy(pChar->BldgSqft, acTmp, SIZ_BLDG_SF);
      }

      // Garage Sqft
      lGarSqft += atoin(pPdr->GarSqft, CSIZ_GARAGE_SQFT);
      if (lGarSqft > 10)
      {
         sprintf(acTmp, "%*u", SIZ_GAR_SQFT, lGarSqft);
         memcpy(pChar->GarSqft, acTmp, SIZ_GAR_SQFT);
         // This is to cover where Garage_Spcs is 0
         pChar->Garage = 'G';
      }

      // Beds
      iBeds += atoin(pPdr->Beds, CSIZ_BEDROOMS);
      if (iBeds > 0)
      {
         sprintf(acTmp, "%*u", SIZ_BEDS, iBeds);
         memcpy(pChar->Beds, acTmp, SIZ_BEDS);
      }

      // Baths
      iFBath += atoin(pPdr->Baths, CSIZ_BATHSROOMS-1);
      if (iFBath > 0)
      {
         sprintf(acTmp, "%*u", SIZ_BATH_F, iFBath);
         memcpy(pChar->FBaths, acTmp, SIZ_BATH_F);

         // Half bath
         iTmp = pPdr->Baths[CSIZ_BATHSROOMS-1] & 0x0F;
         if (iTmp > 0)
         {
            iHBath++;
            sprintf(acTmp, "%*u", SIZ_BATH_H, iHBath);
            memcpy(pChar->HBaths, acTmp, SIZ_BATH_H);
         }
      }

      // Rooms
      iRooms += atoin(pPdr->Rooms, CSIZ_UNITS_ROOMS);
      if (iRooms > 0)
      {
         if (pPdr->PdrType == 'O')       // Commercial
         {
            sprintf(acTmp, "%*u", SIZ_UNITS, iRooms);
            memcpy(pChar->Units, acTmp, SIZ_UNITS);
         } else
         {
            sprintf(acTmp, "%*u", SIZ_ROOMS, iRooms);
            memcpy(pChar->Rooms, acTmp, SIZ_ROOMS);
         }
      }

      // Pool/Spa
      if (pPdr->Pool == 'Y')
         pChar->Pool = 'P';    // Pool

      // Lot sqft - Lot Acres: only update if current roll value not present
      lTmp = atoin(pPdr->LotSqft, CSIZ_LOT_SQFT);
      if (lTmp > 100)
      {
         sprintf(acTmp, "%*u", SIZ_LOT_SQFT, lTmp);
         memcpy(pChar->LotSqft, acTmp, SIZ_LOT_SQFT);
         lTmp = (lTmp*ACRES_FACTOR)/SQFT_PER_ACRE;
         sprintf(acTmp, "%*u", SIZ_LOT_ACRES, lTmp);
         memcpy(pChar->Acres, acTmp, SIZ_LOT_ACRES);
      }

      // Zoning
      memcpy(pChar->Zoning, pPdr->Zoning, CSIZ_ZONE);

      // Usecode
      memcpy(pChar->UseCode, pPdr->UseCode, CSIZ_USE_CODE);

      // Basement Sqft
      memcpy(pChar->BsmtSqft, pPdr->BsmtSqft, CSIZ_BASEMENT_SQFT);

      // View
      if (pPdr->View == 'Y')
         pChar->View = 'Y';

      // Others
      iBldgNum = atoin(pPdr->BldgNum, CSIZ_BLDG_NUMBER);
   } else
   {
      iBldgNum = atoin(pPdr->BldgNum, CSIZ_BLDG_NUMBER);
      memcpy(pChar->BldgNum, pPdr->BldgNum, CSIZ_BLDG_NUMBER);

      lTmp = atoin(pPdr->EffYear, CSIZ_EFF_YR);
      if (lTmp > 1700)
         memcpy(pChar->EffYear, pPdr->EffYear, SIZ_YR_BLT);

      // YrBlt
      lTmp = atoin(pPdr->YearBuilt, CSIZ_EFF_YR);
      if (lTmp > 1700)
         memcpy(pChar->YearBuilt, pPdr->YearBuilt, SIZ_YR_BLT);

      // BldgSqft
      lBldgSqft += atoin(pPdr->BldgSqft, CSIZ_BLDG_SQFT);
      iTmp = 0;
      if (lBldgSqft > 10)
      {
         iTmp = 1;
         lTmp = atoin(pPdr->BldgSqft2, CSIZ_BLDG_SQFT);
         if (lTmp > 0)
         {
            iTmp++;
            lBldgSqft += lTmp;

            // Stories
            sprintf(acTmp, "%*u", SIZ_STORIES, iTmp*10);
            memcpy(pChar->Stories, acTmp, SIZ_STORIES);
         }
         sprintf(acTmp, "%*u", SIZ_BLDG_SF, lBldgSqft);
         memcpy(pChar->BldgSqft, acTmp, SIZ_BLDG_SF);
      }

      // Garage Sqft
      lGarSqft += atoin(pPdr->GarSqft, CSIZ_GARAGE_SQFT);
      if (lGarSqft > 10)
      {
         sprintf(acTmp, "%*u", SIZ_GAR_SQFT, lGarSqft);
         memcpy(pChar->GarSqft, acTmp, SIZ_GAR_SQFT);
         // This is to cover where Garage_Spcs is 0
         pChar->Garage = 'G';
      }

      // Park spaces

      // Beds
      iBeds += atoin(pPdr->Beds, CSIZ_BEDROOMS);
      if (iBeds > 0)
      {
         sprintf(acTmp, "%*u", SIZ_BEDS, iBeds);
         memcpy(pChar->Beds, acTmp, SIZ_BEDS);
      }

      // Baths
      iFBath += atoin(pPdr->Baths, CSIZ_BATHSROOMS-1);
      if (iFBath > 0)
      {
         sprintf(acTmp, "%*u", SIZ_BATH_F, iFBath);
         memcpy(pChar->FBaths, acTmp, SIZ_BATH_F);

         // Half bath
         iTmp = pPdr->Baths[CSIZ_BATHSROOMS-1] & 0x0F;
         if (iTmp > 0)
         {
            iHBath++;
            sprintf(acTmp, "%*u", SIZ_BATH_H, iHBath);
            memcpy(pChar->HBaths, acTmp, SIZ_BATH_H);
         }
      }

      // Rooms
      iRooms += atoin(pPdr->Rooms, CSIZ_UNITS_ROOMS);
      if (iRooms > 0)
      {
         if (pPdr->PdrType == 'O')       // Commercial
         {
            sprintf(acTmp, "%*u", SIZ_UNITS, iRooms);
            memcpy(pChar->Units, acTmp, SIZ_UNITS);
         } else
         {
            sprintf(acTmp, "%*u", SIZ_ROOMS, iRooms);
            memcpy(pChar->Rooms, acTmp, SIZ_ROOMS);
         }
      }

      // Pool/Spa
      if (pPdr->Pool == 'Y')
         pChar->Pool = 'P';    // Pool

      // Lot sqft - Lot Acres: only update if current roll value not present
      lTmp = atoin(pPdr->LotSqft, CSIZ_LOT_SQFT);
      if (lTmp > 100)
      {
         sprintf(acTmp, "%*u", SIZ_LOT_SQFT, lTmp);
         memcpy(pChar->LotSqft, acTmp, SIZ_LOT_SQFT);
         lTmp = (lTmp*ACRES_FACTOR)/SQFT_PER_ACRE;
         sprintf(acTmp, "%*u", SIZ_LOT_ACRES, lTmp);
         memcpy(pChar->Acres, acTmp, SIZ_LOT_ACRES);
      }

      // Zoning
      memcpy(pChar->Zoning, pPdr->Zoning, CSIZ_ZONE);

      // Usecode
      memcpy(pChar->UseCode, pPdr->UseCode, CSIZ_USE_CODE);

      // Basement Sqft
      memcpy(pChar->BsmtSqft, pPdr->BsmtSqft, CSIZ_BASEMENT_SQFT);

      // View
      if (pPdr->View == 'Y')
         pChar->View = 'Y';

      // Units - See rooms

      // Others
      iBldgNum = atoin(pPdr->BldgNum, CSIZ_BLDG_NUMBER);
   }

   return 0;
}

/****************************** Ccx_FormatSaleChar ***************************
 *
 * Return -1 if bad record.  0 if OK.
 *
 *****************************************************************************/

int Ccx_FormatSaleChar(char *pCharRec, char *pSaleRec, bool initFlg=true)
{

   CCX_SALE *pSale = (CCX_SALE *)pSaleRec;
   CCX_CCHR *pChar = (CCX_CCHR *)pCharRec;

   int      iTmp;
   char     acTmp[32];

   // Inititalize
   if (initFlg)
      memset(pCharRec, ' ', sizeof(CCX_CCHR));

   if (pSale->PdrType == ' ')
      return -1;

   // APN
   memcpy(pChar->Apn, pSale->Apn, SSIZ_APN);

   // BldgNum
   memcpy(pChar->BldgNum, pSale->BldgNum,  SSIZ_BLDG_NUMBER);

   // Activity number
   memcpy(pChar->Activity_Num, pSale->Activity_Num, sizeof(pChar->Activity_Num));

   pChar->PdrType = pSale->PdrType;
   memcpy(pChar->TRA, pSale->TRA, SSIZ_TRA);
   memcpy(pChar->UseCode, pSale->UseCode, SSIZ_USE_CODE);
   memcpy(pChar->MapModel, pSale->MapModel, SSIZ_MAP_MODEL);
   memcpy(pChar->Tot_Liv_Area, pSale->Tot_Liv_Area, SSIZ_TOT_LIV_AREA);

   memcpy(pChar->ClassType, pSale->ClassType, SSIZ_CLASS_TYPE);
   memcpy(pChar->EffYear, pSale->EffYear, SSIZ_EFF_YR+1);         // Include Rej_Code
   memcpy(pChar->Model, pSale->Model, SSIZ_MODEL);
   memcpy(pChar->Beds, pSale->Beds, SSIZ_BEDROOMS);
   memcpy(pChar->Baths, pSale->Baths, SSIZ_BATHROOMS);

   // Rooms or Units
   iTmp = atoin(pSale->Rooms, SSIZ_ROOMS);
   if (iTmp > 0)
   {
      // Commercial or office building
      if (pSale->PdrType == 'O')
      {
         sprintf(acTmp, "%*u", SIZ_CCHAR_UNITS, iTmp);
         memcpy(pChar->Units, acTmp, SIZ_CCHAR_UNITS);
      } else
         memcpy(pChar->Rooms, pSale->Rooms, SSIZ_ROOMS);
   }

   // Condition
   pChar->Condition = pSale->Condition;
   pChar->Garage = pSale->Garage;
   pChar->CarPort = pSale->CarPort;
   memcpy(pChar->ParkStalls, pSale->ParkStalls, SSIZ_PARKING_STALLS);
   pChar->GarConv = pSale->GarConv;
   pChar->AttachedGarage = pSale->AttachedGarage;
   pChar->Addition = pSale->Addition;
   pChar->Architecture = pSale->Architecture;
   pChar->Central_AirHeat = pSale->Central_AirHeat;
   pChar->FloorLevel = pSale->FloorLevel;
   pChar->Level_Access = pSale->Level_Access;
   pChar->Back2Back = pSale->Back2Back;
   pChar->EndUnit = pSale->EndUnit;

   // Proximities
   memcpy(pChar->Proximities, pSale->Proximities, SSIZ_PROXIMITES);
   memcpy(pChar->PoolSqft, pSale->PoolSqft, SSIZ_POOL_SQFT);
   memcpy(pChar->PatioSqft, pSale->PatioSqft, SSIZ_PATIO_DECK_SQFT);
   memcpy(pChar->Resp_Code, pSale->Resp_Code, SSIZ_RESPONSIBLITY_CODE);

   // Land problem
   pChar->LandProblem = pSale->LandProblem;
   pChar->Topog = pSale->Topog;

   // Settings
   memcpy(pChar->Settings, pSale->Settings, SSIZ_SETTINGS);
   memcpy(pChar->Nuisances, pSale->Nuisances, SSIZ_NUISANCES);

   // Pool
   // Bldg
   // Acres/LotSqft
   // Zoning
   // VacantLot

   // Appraisal Code
   pChar->Appr_Code = pSale->Appr_Code;
   memcpy(pChar->Appr_Pct, pSale->Appr_Pct, SSIZ_APPRAISAL_PCT);

   // Appraisal Base Year
   pChar->Appr_BaseYr = pSale->Appr_BaseYr;

   // Appraisal value
   memcpy(pChar->Appr_Val, pSale->Appr_Val, SSIZ_APPRAISED_VAL);

   // Situs
   memcpy(pChar->S_StrNum, pSale->S_StrNum, SSIZ_S_STRNUM+SSIZ_S_STRNAME+SSIZ_S_CITYCODE);

   // Rclnd
   memcpy(pChar->Rclnd_BldgSqft, pSale->Rclnd_BldgSqft, SSIZ_TOT_LIV_AREA*4);

   // Sale comments
   memcpy(pChar->Remark1, pSale->Remark1, SSIZ_REMARK_1+SSIZ_REMARK_2);

   // Total living area
   memcpy(pChar->Total_Tot_Liv_Area, pSale->Total_Tot_Liv_Area, SSIZ_TOTAL_LIV_AREA);

   // View

   // Remark
   memcpy(pChar->Bldg, pSale->Bldg, SSIZ_BUILDING);
   memcpy(pChar->Remark1, pSale->Remark1, SSIZ_REMARK_1+SSIZ_REMARK_2);

   // YrBlt, BldgSqft2, BsmtSqft, GarSqft

   // Sale date
   memcpy(pChar->SaleDate, pSale->SaleDate, SSIZ_SALE_DATE);

   // CRLF
   pChar->CRLF[0] = '\n';
   pChar->CRLF[1] = 0;

   return 0;
}

/****************************** Ccx_UpdateSaleHist ***************************
 *
 * Update history sale.  This function adds new sale record to cumulative file.
 *
 * Return 0 if successful, otherwise error
 *
 *****************************************************************************/
//
//int Ccx_CreateSaleHist(void)
//{
//   CCX_SALE *pCurSale;
//   CCX_CSAL *pHistSale;
//
//   char     acNewSale[512];
//   char     *pTmp, acHSaleRec[512], acCSaleRec[1024], acSaleDate[32], acTmp[256];
//   char     acOutFile[_MAX_PATH], acCharRec[512], acLastApn[16];
//
//   HANDLE   fhOut;
//   FILE     *fdChar;
//
//   int      iRet, iTmp, iUpdateSale=0, iNewSale=0;
//   DWORD    nBytesWritten;
//   BOOL     bRet, bEof;
//   long     lCnt=0;
//
//   // Check current sale file
//   if (_access(acSaleFile, 0))
//   {
//      LogMsg("***** Missing current sale file %s", acSaleFile);
//      return -1;
//   }
//
//   // Create Cum Char output file
//   strcpy(acOutFile, acCChrFile);
//   if (pTmp = strrchr(acOutFile, '.'))
//      strcpy(pTmp, ".dat");
//   else
//      strcat(acOutFile, ".dat");
//   if (!(fdChar = fopen(acOutFile, "w")))
//   {
//      LogMsg("***** Error creating Char output file %s", acOutFile);
//      return 2;
//   }
//
//   // Create tmp output file
//   strcpy(acOutFile, acCSalFile);
//   if (pTmp = strrchr(acOutFile, '.'))
//      strcpy(pTmp, ".tmp");
//   else
//      strcat(acOutFile, ".tmp");
//
//   // Open current sale
//   LogMsg("Open current sale file %s", acSaleFile);
//   fdCSale = fopen(acSaleFile, "r");
//   if (fdCSale == NULL)
//   {
//      LogMsg("***** Error opening current sale file: %s\n", acSaleFile);
//      return 2;
//   }
//
//   // Open Output file
//   LogMsg("Open output file %s", acOutFile);
//   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
//          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);
//
//   if (fhOut == INVALID_HANDLE_VALUE)
//   {
//      LogMsg("***** Error open output file: %s", acOutFile);
//      return 4;
//   }
//
//   acHSaleRec[0] = 0;
//
//   pCurSale  = (CCX_SALE *)&acCSaleRec[0];
//   pHistSale = (CCX_CSAL *)&acHSaleRec[0];
//   bEof = false;
//   acSaleDate[0] = 0;
//   acLastApn[0] = 0;
//   acLastApn[iSaleApnLen] = 0;
//
//   // Merge loop
//   while (!bEof)
//   {
//      // Get current sale
//      pTmp = fgets(acCSaleRec, 1024, fdCSale);
//      if (!pTmp)
//         break;
//
//      // Update char file.  Like sale file, this output file needs resort
//      // and update with latest info. 7/13/2011 sn
//      if (!Ccx_FormatSaleChar(acCharRec, acCSaleRec))
//      {
//         memcpy(acLastApn, pCurSale->Apn, iSaleApnLen);
//         fputs(acCharRec, fdChar);
//      }
//
//      // Format cumsale record
//      iRet = Ccx_FormatCSale(acNewSale, acCSaleRec);
//      if (!iRet)
//      {
//         if (bDebug)
//            LogMsg("** New sale: %.9s->%.8s", pCurSale->Apn, pCurSale->SaleDate);
//
//         // Output current sale record
//         bRet = WriteFile(fhOut, acNewSale, iCSalLen, &nBytesWritten, NULL);
//         iNewSale++;
//      }
//
//      if (!(++lCnt % 1000))
//         printf("\r%u", lCnt);
//
//      if (!bRet)
//      {
//         LogMsg("***** Error occurs: %d\n", GetLastError());
//         break;
//      }
//   }
//
//   // Close files
//   if (fdCSale)
//      fclose(fdCSale);
//   if (fdChar)
//      fclose(fdChar);
//   if (fhOut)
//      CloseHandle(fhOut);
//
//   // Sort output file - APN (asc), Saledate (des), DocNum (des)
//   sprintf(acTmp, "S(%d,%d,C,A,%d,8,C,D,%d,10,C,D) F(FIX,%d)", OFF_CSAL_APN,
//      SSIZ_APN, OFF_CSAL_SALEDATE, OFF_CSAL_DOCNUM, iCSalLen);
//   iTmp = sortFile(acOutFile, acCSalFile, acTmp);
//
//   LogMsg("Total sale records processed:   %u", lCnt);
//   LogMsg("Total cumulative sale records:  %u", iTmp);
//   LogMsg("Total new sale records:         %u", iNewSale);
//
//   LogMsg("Update Sale History completed.");
//
//   return 0;
//}

//int Ccx_UpdateSaleHist(void)
//{
//   CCX_SALE *pCurSale;
//   CCX_CSAL *pHistSale;
//
//   char     acNewSale[512];
//   char     *pTmp, acHSaleRec[512], acCSaleRec[1024], acSaleDate[32], acTmp[256];
//   char     acOutFile[_MAX_PATH], acCharRec[512], acLastApn[16];
//
//   HANDLE   fhIn, fhOut;
//   FILE     *fdChar;
//
//   int      iRet, iTmp, iUpdateSale=0, iNewSale=0;
//   DWORD    nBytesRead;
//   DWORD    nBytesWritten;
//   BOOL     bRet, bEof;
//   long     lCnt=0;
//
//   // Check current sale file
//   if (_access(acSaleFile, 0))
//   {
//      LogMsg("***** Missing current sale file %s", acSaleFile);
//      return -1;
//   }
//
//   // Check cumulative sale file
//   if (_access(acCSalFile, 0))
//   {
//      LogMsg("***** Missing cumulative file %s.", acCSalFile);
//      return -1;
//   }
//
//   // Create Cum Char output file
//   strcpy(acOutFile, acCChrFile);
//   if (pTmp = strrchr(acOutFile, '.'))
//      strcpy(pTmp, ".dat");
//   else
//      strcat(acOutFile, ".dat");
//   if (!(fdChar = fopen(acOutFile, "w")))
//   {
//      LogMsg("***** Error creating Char output file %s", acOutFile);
//      return 2;
//   }
//
//   // Create tmp output file
//   strcpy(acOutFile, acCSalFile);
//   if (pTmp = strrchr(acOutFile, '.'))
//      strcpy(pTmp, ".tmp");
//   else
//      strcat(acOutFile, ".tmp");
//
//   // Open current sale
//   LogMsg("Open current sale file %s", acSaleFile);
//   fdCSale = fopen(acSaleFile, "r");
//   if (fdCSale == NULL)
//   {
//      LogMsg("***** Error opening current sale file: %s\n", acSaleFile);
//      return 2;
//   }
//
//   // Open Input file - cumulative sale file
//   LogMsg("Open cumulative sale file %s", acCSalFile);
//   fhIn = CreateFile(acCSalFile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
//          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);
//
//   if (fhIn == INVALID_HANDLE_VALUE)
//   {
//      LogMsg("***** Error open current sale file: %s", acCSalFile);
//      return 3;
//   }
//
//   // Open Output file
//   LogMsg("Open output file %s", acOutFile);
//   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
//          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);
//
//   if (fhOut == INVALID_HANDLE_VALUE)
//   {
//      LogMsg("***** Error open output file: %s", acOutFile);
//      return 4;
//   }
//
//   // Read first history sale
//   bRet = ReadFile(fhIn, acHSaleRec, iCSalLen, &nBytesRead, NULL);
//   acHSaleRec[iCSalLen] = 0;
//
//   pCurSale  = (CCX_SALE *)&acCSaleRec[0];
//   pHistSale = (CCX_CSAL *)&acHSaleRec[0];
//   bEof = false;
//   acSaleDate[0] = 0;
//   acLastApn[iSaleApnLen] = acLastApn[0] = 0;
//
//   // Merge loop
//   while (!bEof)
//   {
//      // Get current sale
//      pTmp = fgets(acCSaleRec, 1024, fdCSale);
//      if (!pTmp)
//         break;
//
//      // Update char file
//      if (memcmp(acLastApn, pCurSale->Apn, iSaleApnLen))
//      {
//         if (!Ccx_FormatSaleChar(acCharRec, acCSaleRec))
//         {
//            memcpy(acLastApn, pCurSale->Apn, iSaleApnLen);
//            fputs(acCharRec, fdChar);
//         }
//      }
//
//      iTmp = memcmp(pCurSale->Apn, pHistSale->Apn, iSaleApnLen);
//
//      NextSaleRec:
//
//      // Found matched APN ?
//      if (!iTmp)
//      {
//         // If current sale is newer, add to out file
//         if (memcmp(pCurSale->SaleDate, pHistSale->SaleDate, SIZ_SALE1_DT) > 0)
//         {
//            // Format cumsale record
//            iRet = Ccx_FormatCSale(acNewSale, acCSaleRec);
//            if (!iRet)
//            {
//               if (bDebug)
//                  LogMsg("** New sale: %.9s->%.8s", pCurSale->Apn, pCurSale->SaleDate);
//
//               // Output current sale record
//               bRet = WriteFile(fhOut, acNewSale, iCSalLen, &nBytesWritten, NULL);
//               iUpdateSale++;
//            }
//         }
//      } else if (iTmp > 0)
//      {
//         // Write out all history sale record up to current APN
//         while (iTmp > 0)
//         {
//            bRet = WriteFile(fhOut, acHSaleRec, iCSalLen, &nBytesWritten, NULL);
//            bRet = ReadFile(fhIn, acHSaleRec, iCSalLen, &nBytesRead, NULL);
//            if (!bRet || !nBytesRead)
//            {
//               bEof = true;
//               break;
//            }
//            iTmp = memcmp(pCurSale->Apn, pHistSale->Apn, iSaleApnLen);
//         }
//
//         if (!bEof)
//            goto NextSaleRec;
//      } else // CurApn < HistApn
//      {
//         // Format cumsale record
//         iRet = Ccx_FormatCSale(acNewSale, acCSaleRec);
//
//         if (!iRet)
//         {
//            if (bDebug)
//               LogMsg("** First sale: %.9s->%.8s", pCurSale->Apn, pCurSale->SaleDate);
//
//            // New sale record for this APN
//            bRet = WriteFile(fhOut, acNewSale, iCSalLen, &nBytesWritten, NULL);
//            iNewSale++;
//         }
//      }
//
//      if (!(++lCnt % 1000))
//         printf("\r%u", lCnt);
//
//      if (!bRet)
//      {
//         LogMsg("***** Error occurs: %d\n", GetLastError());
//         break;
//      }
//   }
//
//   // Check what file has broken out of the loop
//   if (bEof)
//   {
//      // Do the rest of Char file
//      do
//      {
//         iRet = Ccx_FormatCSale(acNewSale, acCSaleRec);
//         if (!iRet)
//         {
//            // New char record for this APN
//            bRet = WriteFile(fhOut, acNewSale, iCSalLen, &nBytesWritten, NULL);
//            iNewSale++;
//         }
//         pTmp = fgets(acCSaleRec, 1024, fdCSale);
//
//         // Update char file
//         if (pTmp && memcmp(acLastApn, pCurSale->Apn, iSaleApnLen))
//         {
//            if (!Ccx_FormatSaleChar(acCharRec, acCSaleRec))
//            {
//               memcpy(acLastApn, pCurSale->Apn, iSaleApnLen);
//               fputs(acCharRec, fdChar);
//            }
//         }
//      } while (pTmp);
//   } else
//   {
//      // Do the rest of the file
//      while (nBytesRead > 0)
//      {
//         bRet = WriteFile(fhOut, acHSaleRec, iCSalLen, &nBytesWritten, NULL);
//         bRet = ReadFile(fhIn, acHSaleRec, iCSalLen, &nBytesRead, NULL);
//
//         if (!bRet)
//            break;
//         if (!(++lCnt % 1000))
//            printf("\r%u", lCnt);
//      }
//   }
//
//   // Close files
//   if (fdCSale)
//      fclose(fdCSale);
//   if (fdChar)
//      fclose(fdChar);
//   if (fhOut)
//      CloseHandle(fhOut);
//   if (fhIn)
//      CloseHandle(fhIn);
//
//   // Rename output file
//   strcpy(acTmp, acCSalFile);
//   if (pTmp = strrchr(acTmp, '.'))
//   {
//      strcpy(pTmp, ".sav");
//      if (!_access(acTmp, 0))
//         remove(acTmp);
//   }
//
//   iRet = rename(acCSalFile, acTmp);
//   if (iRet)
//   {
//      LogMsg("***** Error renaming %s to %s: %d", acCSalFile, acTmp, errno);
//   } else
//   {
//      // Sort output file - APN (asc), Saledate (des), DocNum (des)
//      sprintf(acTmp, "S(%d,%d,C,A,%d,8,C,D,%d,10,C,D) F(FIX,%d)", OFF_CSAL_APN,
//         SSIZ_APN, OFF_CSAL_SALEDATE, OFF_CSAL_DOCNUM, iCSalLen);
//      iTmp = sortFile(acOutFile, acCSalFile, acTmp);
//   }
//
//   LogMsg("Total sale records processed:   %u", lCnt);
//   LogMsg("Total cumulative sale records:  %u", iTmp);
//   LogMsg("Total new sale records:         %u", iNewSale);
//   LogMsg("Total sale records updated:     %u", iUpdateSale);
//
//   LogMsg("Update Sale History completed.");
//
//   return 0;
//}

/**************************** Ccx_ExtrSaleFromSalFile *************************
 *
 * Extract sales to cum sale file and chars to cum char file
 *
 ******************************************************************************/

int Ccx_ExtrSaleFromSalFile(void)
{
   CCX_SALE  *pSale;
   SCSAL_REC *pCSale;

   char     *pTmp, acSaleRec[512], acCSaleRec[600], acSaleDate[32], acTmp[256];
   char     acOutFile[_MAX_PATH], acSaleChrFile[_MAX_PATH], acTmpSale[_MAX_PATH], acCharRec[600], acLastApn[16];

   FILE     *fdChar;

   int      iRet, iTmp, iUpdateSale=0, iUpdateChar=0, iLastCountySaleDate=0;
   long     lCnt=0;

   LogMsg0("Update cum sale file");

   // Check current sale file
   if (_access(acSaleFile, 0))
   {
      LogMsg("***** Missing current sale file %s", acSaleFile);
      return -1;
   }

   // Create Cum Char output file
   strcpy(acSaleChrFile, acCChrFile);
   if (pTmp = strrchr(acSaleChrFile, '.'))
      strcpy(pTmp, ".tmp");
   else
      strcat(acSaleChrFile, ".tmp");

   LogMsg("Create char file (from sale): %s", acSaleChrFile);
   if (!(fdChar = fopen(acSaleChrFile, "w")))
   {
      LogMsg("***** Error creating Char output file %s", acSaleChrFile);
      return -3;
   }

   // Create tmp sale file
   sprintf(acTmpSale, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "tmp");
   LogMsg("Create updated sale file %s", acTmpSale);
   fdCSale = fopen(acTmpSale, "w");
   if (fdCSale == NULL)
   {
      LogMsg("***** Error creating extract sale file: %s\n", acTmpSale);
      return -3;
   }

   // Open current sale
   LogMsg("Open current sale file %s", acSaleFile);
   fdSale = fopen(acSaleFile, "r");
   if (fdSale == NULL)
   {
      LogMsg("***** Error opening current sale file: %s\n", acSaleFile);
      return -2;
   }

   pSale  = (CCX_SALE *)&acSaleRec[0];
   pCSale = (SCSAL_REC *)&acCSaleRec[0];
   acSaleDate[0] = 0;
   acLastApn[iSaleApnLen] = acLastApn[0] = 0;

   // Merge loop
   while (fgets(acSaleRec, 1024, fdSale))
   {
      // Update char file.  Like sale file, this output file needs resort
      // and update with latest info. 7/13/2011 sn
      if (!Ccx_FormatSaleChar(acCharRec, acSaleRec))
      {
         fputs(acCharRec, fdChar);
         iUpdateChar++;
      }

      // Format cumsale record
      iRet = Ccx_FormatSale(acCSaleRec, acSaleRec);
      if (!iRet)
      {
         iTmp = atoln(pCSale->DocDate, SSIZ_SALE_DATE);
         if (iTmp > iLastCountySaleDate)
            iLastCountySaleDate = (pCSale->DocDate, SSIZ_SALE_DATE);

         // Output current sale record
         fputs(acCSaleRec, fdCSale);
         iUpdateSale++;
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdSale)
      fclose(fdSale);
   if (fdCSale)
      fclose(fdCSale);
   if (fdChar)
      fclose(fdChar);

   // Append to cum sale file
   sprintf(acOutFile,  acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "dat");

   // Sort/merge sale file - APN (asc), Saledate (asc), DocNum (asc)
   sprintf(acTmp, "S(1,%d,C,A,27,8,C,A,15,%d,C,A,57,10,C,D) DUPO(1,34)", iApnLen, SALE_SIZ_DOCNUM);
   iTmp = sortFile(acTmpSale, acOutFile, acTmp);
   if (iTmp > 0)
   {
      strcpy(acSaleFile, acOutFile);
      if (!_access(acCSalFile, 0))
      {
         sprintf(acSaleRec, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "srt");
         sprintf(acTmpSale, "%s+%s", acOutFile, acCSalFile);
         iTmp = sortFile(acTmpSale, acSaleRec, acTmp);
         if (iTmp > 0)
         {
            DeleteFile(acCSalFile);
      
            // Rename srt to SLS file
            rename(acSaleRec, acCSalFile);
         }
      } else
      {
         CopyFile(acOutFile, acCSalFile, false);
      }
   }

   // Prepare output file
   strcpy(acOutFile, acCChrFile);
   pTmp = strrchr(acOutFile, '.');
   strcpy(pTmp+1, "Sls");
   sprintf(acTmp, "%s+%s", acSaleChrFile, acOutFile);

   // Sort Cum Char file 
   iTmp = sortFile(acTmp, acCChrFile, "S(1,9,C,A,10,3,C,A,371,8,C,D) DUPOUT", &iRet);
   if (iTmp > 0)
      CopyFile(acCChrFile, acOutFile, false);

   LogMsg("Total sale records processed:   %u", lCnt);
   LogMsg("Total cumulative sale records:  %u", iTmp);
   LogMsg("Total char records extracted:   %u", iUpdateChar);
   LogMsg("Total sale records extracted:   %u", iUpdateSale);

   LogMsg("Update Sale History completed.  Last sale date: ", iLastCountySaleDate);
   

   return iRet;
}

/********************************** CreateCcxRec *****************************
 *
 * Return 0 if successful, >0 if error
 *
 *****************************************************************************/

int Ccx_CreateLienRec(char *pOutbuf, char *pRollRec)
{
   char     acTmp[256];
   long     lTmp, iTmp;
   LIENEXTR *pLienRec = (LIENEXTR *)pOutbuf;
   CCX_LIEN *pRec     = (CCX_LIEN *)pRollRec;

   // Clear output buffer
   memset(pOutbuf, ' ', sizeof(LIENEXTR));

   // Start copying data
   memcpy(pLienRec->acApn, pRec->Apn, RSIZ_APN);

   // TRA
   lTmp = atoin(pRec->TRA, RSIZ_TRA);
   if (lTmp > 0)
   {
      iTmp = sprintf(acTmp, "%.6d", lTmp);
      memcpy(pLienRec->acTRA, acTmp, iTmp);
   }

   // Year assessed
   memcpy(pLienRec->acYear, myCounty.acYearAssd, 4);

   // Exempt
   long lExe1 = atoin(pRec->Exe_Amt1, RSIZ_EXE_AMT);
   long lExe2 = atoin(pRec->Exe_Amt2, RSIZ_EXE_AMT);
   long lExe3 = atoin(pRec->Exe_Amt3, RSIZ_EXE_AMT);
   long lTotalExe = lExe1+lExe2+lExe3;
   memcpy(pLienRec->extra.Ccx.Exe_Code1, pRec->Exe_Code1, RSIZ_EXE_CODE);
   memcpy(pLienRec->extra.Ccx.Exe_Code2, pRec->Exe_Code2, RSIZ_EXE_CODE);
   memcpy(pLienRec->extra.Ccx.Exe_Code3, pRec->Exe_Code3, RSIZ_EXE_CODE);

   pLienRec->acHO[0] = '2';            // 'N'
   if (lTotalExe > 0)
   {
      memcpy(pLienRec->acExCode, pRec->Exe_Code1, RSIZ_EXE_CODE);      
      if (pRec->Exe_Code1[0] == 'H')
         pLienRec->acHO[0] = '1';      // 'Y'

      iTmp = sprintf(acTmp, "%*u", sizeof(pLienRec->acExAmt), lTotalExe);
      memcpy(pLienRec->acExAmt, acTmp, iTmp);

      if (lExe1 > 0)
      {
         iTmp = sprintf(acTmp, "%*u", sizeof(pLienRec->extra.Ccx.Exe_Amt1), lExe1);
         memcpy(pLienRec->extra.Ccx.Exe_Amt1, acTmp, iTmp);
      }
      if (lExe2 > 0)
      {
         iTmp = sprintf(acTmp, "%*u", sizeof(pLienRec->extra.Ccx.Exe_Amt1), lExe2);
         memcpy(pLienRec->extra.Ccx.Exe_Amt2, acTmp, iTmp);
      }
      if (lExe3 > 0)
      {
         iTmp = sprintf(acTmp, "%*u", sizeof(pLienRec->extra.Ccx.Exe_Amt1), lExe3);
         memcpy(pLienRec->extra.Ccx.Exe_Amt3, acTmp, iTmp);
      }
   }

   // Land
   long lLand = atoin(pRec->Land, RSIZ_LAND_VALUE);
   if (lLand > 0)
   {
      sprintf(acTmp, "%*u", SIZ_LIEN_LAND, lLand);
      memcpy(pLienRec->acLand, acTmp, SIZ_LIEN_LAND);
   }

   // Improve
   long lImpr = atoin(pRec->Impr, RSIZ_LAND_VALUE);
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*u", SIZ_LIEN_IMPR, lImpr);
      memcpy(pLienRec->acImpr, acTmp, SIZ_LIEN_IMPR);
   }

   // Other value
   long lPers = atoin(pRec->PP_Val, RSIZ_LAND_VALUE);
   if (lPers > 0)
   {
      sprintf(acTmp, "%*u", SIZ_LIEN_PERS, lPers);
      memcpy(pLienRec->acPP_Val, acTmp, SIZ_LIEN_PERS);
      memcpy(pLienRec->acOther, acTmp, SIZ_LIEN_PERS);
   }

   // Gross
   LONGLONG lGross = lPers + lLand + lImpr;
   if (lGross > 0)
   {
      sprintf(acTmp, "%*u", SIZ_LIEN_GROSS, lGross);
      memcpy(pLienRec->acGross, acTmp, SIZ_LIEN_GROSS);
   }

   // Impr - Ratio
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*u", SIZ_LIEN_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
      memcpy(pLienRec->acRatio, acTmp, SIZ_LIEN_RATIO);
   }

   // Set full exempt flag
   if (pRec->Tax_CodeB > ' ')
   {
      pLienRec->SpclFlag = LX_FULLEXE_FLG;
      pLienRec->extra.Ccx.Tax_CodeA = pRec->Tax_CodeA;
      pLienRec->extra.Ccx.Tax_CodeB = pRec->Tax_CodeB;
   } else if (lTotalExe == lGross)
      pLienRec->SpclFlag = LX_FULLEXE_FLG;

   // Set Prop8 flag
   //if (memcmp(pRec->ValueResonCode, "    ", 4))
   if (pRec->ValueResonCode[0] == 'P' && pRec->ValueResonCode[1] == '8')
      pLienRec->SpclFlag |= LX_PROP8_FLG;

   pLienRec->LF[0] = '\n';
   pLienRec->LF[1] = 0;

   return 0;
}

/********************************* Ccx_ExtrLien *****************************
 *
 *
 ****************************************************************************/

int Ccx_ExtrLien()
{
   char     *pTmp, acBuf[MAX_RECSIZE], acRollRec[MAX_RECSIZE];
   char     acOutFile[_MAX_PATH];
   FILE     *fdLien;
   int      iRet;
   long     lCnt=0;

   LogMsg0("Extract LDR value... ");

   // Open roll file
   LogMsg("Open Roll file %s", acLienFile);
   fdRoll = fopen(acLienFile, "r");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", acLienFile);
      return -1;
   }

   // Open Output file
   sprintf(acOutFile, acLienTmpl, myCounty.acCntyCode, myCounty.acCntyCode);
   LogMsg("Open lien extract file %s", acOutFile);
   fdLien = fopen(acOutFile, "w");
   if (fdLien == NULL)
   {
      LogMsg("***** Error creating lien extract file: %s\n", acOutFile);
      return -2;
   }

   // Merge loop
   while (!feof(fdRoll))
   {
      // Get roll rec
      pTmp = fgets(acRollRec, iRollLen, fdRoll);

      if (!pTmp)
         break;

      // Create new R01 record
      iRet = Ccx_CreateLienRec(acBuf, acRollRec);

      // Write to output
      fputs(acBuf, fdLien);
      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdRoll)
      fclose(fdRoll);
   if (fdLien)
      fclose(fdLien);

   LogMsg("Total output records:       %u", lCnt);
   printf("\nTotal output records: %u", lCnt);

   lRecCnt = lCnt;
   return 0;
}

/****************************** Ccx_CreateSitusRec() ******************************
 *
 *
 **********************************************************************************/

//void Ccx_CreateSitusRec(ADREXT *pAdrbuf, char *pRollRec)
//{
//   CCX_ROLL *pRec;
//   char     acTmp[256], acAddr1[64], acAddr2[64], acCode[32], acToNum[16];
//   int      iTmp;
//   long     lTmp;
//
//   ADR_REC  sSitusAdr;
//
//   pRec = (CCX_ROLL *)pRollRec;
//
//
//   // Situs
//   acAddr1[0] = 0;
//   acAddr2[0] = 0;
//   memset((void *)&sSitusAdr, 0, sizeof(ADR_REC));
//
//   // Copy APN
//   memcpy(pAdrbuf->Apn, pRec->Apn, RSIZ_APN);
//
//   // Copy StrNum
//   sSitusAdr.lStrNum = atoin(pRec->S_StrNum, RSIZ_S_STRNUM);
//   if (pRec->S_StrSfx[0] > ' ')
//      memcpy(sSitusAdr.strSfx, pRec->S_StrSfx, RSIZ_S_STRSFX);
//
//   if (pRec->S_StrName[0] > ' ' && memcmp(pRec->S_StrName, "NO ADDRESS", 10))
//   {
//      // Copy street name
//      memcpy(acTmp, pRec->S_StrName, RSIZ_S_STRNAME);
//      myTrim(acTmp,RSIZ_S_STRNAME);
//
//      if (strchr(acTmp, '&'))
//         strcpy(sSitusAdr.strName, acTmp);
//      else
//         parseAdrND(&sSitusAdr, acTmp);
//
//      if (sSitusAdr.lStrNum > 0)
//      {
//         iTmp = sprintf(acAddr1, "%d", sSitusAdr.lStrNum);
//         memcpy(pAdrbuf->StrNum, acAddr1, iTmp);
//      }
//      if (sSitusAdr.strDir[0] > ' ')
//         memcpy(pAdrbuf->StrDir, sSitusAdr.strDir, strlen(sSitusAdr.strDir));
//      if (sSitusAdr.strName[0] > ' ')
//         memcpy(pAdrbuf->StrName, sSitusAdr.strName, strlen(sSitusAdr.strName));
//      if (sSitusAdr.strSfx[0] > ' ')
//         memcpy(pAdrbuf->StrSfx, sSitusAdr.strSfx, strlen(sSitusAdr.strSfx));
//
//      // Copy unit#
//      acToNum[0] = 0;
//      if (memcmp(pRec->S_Unit, "          ", RSIZ_S_APT_NBR))
//      {
//         memcpy(acTmp, pRec->S_Unit, RSIZ_S_APT_NBR);
//         acTmp[RSIZ_S_APT_NBR] = 0;
//         blankRem(acTmp);
//         pAdrbuf->Unit[0] = '#';
//         memcpy((char *)&pAdrbuf->Unit[1], acTmp, strlen(acTmp));
//      } else if (pRec->S_StrFra[0] > '0')
//      {   // In CCX, StrFract is sometimes being used to store UnitNo
//         if (pRec->S_StrFra[1] == '/')
//            memcpy(pAdrbuf->StrFra, pRec->S_StrFra, RSIZ_S_FRACTION);
//         else
//         {
//            // To StrNum (Fernando put this in strName)
//            // Don't put it in here because it will mess up geocoding process.  We need to find
//            // a way to put it in product.
//
//            //acToNum[0] = '-';
//            //memcpy((char *)&acToNum[1], pRec->S_StrFra, RSIZ_S_FRACTION);
//            //acToNum[RSIZ_S_FRACTION+1] = 0;
//            //strcat(acAddr1, acToNum);
//         }
//      }
//
//      sprintf(acTmp, " %.3s %s %s %s %.6s", pAdrbuf->StrFra, sSitusAdr.strDir, sSitusAdr.strName, sSitusAdr.strSfx, pAdrbuf->Unit);
//      strcat(acAddr1, acTmp);
//      blankRem(acAddr1);
//      memcpy(pAdrbuf->Addr1, acAddr1, strlen(acAddr1));
//   }
//
//   // state
//   memcpy(pAdrbuf->State, "CA", 2);
//
//   // City
//   if (pRec->S_CityAbbr[0] > ' ')
//   {
//      memcpy(acTmp, pRec->S_CityAbbr, RSIZ_S_CITYABBR);
//      myTrim(acTmp, RSIZ_S_CITYABBR);
//      Abbr2City(acTmp, acCode);
//      if (acCode[0])
//         memcpy(pAdrbuf->City, acCode, strlen(acCode));
//   }
//
//   // Zipcode
//   lTmp = atoin(pRec->S_Zip, RSIZ_S_ZIP);
//   if (lTmp > 90000)
//   {
//      memcpy(pAdrbuf->Zip, pRec->S_Zip, SIZ_S_ZIP);
//      lTmp = atoin(pRec->S_Zip4, 4);
//      if (lTmp > 0)
//         memcpy(pAdrbuf->Zip4, pRec->S_Zip4, 4);
//   }
//
//   sprintf(acAddr2, "%s CA %.5s", acCode, pAdrbuf->Zip);
//   blankRem(acAddr2);
//   memcpy(pAdrbuf->Addr2, acAddr2, strlen(acAddr2));
//}

/******************************** Ccx_ExtrSitus *****************************
 *
 * Extract situs from roll file
 *
 ****************************************************************************/

//int Ccx_ExtrSitus()
//{
//   char     *pTmp, acRollRec[MAX_RECSIZE];
//   char     acOutFile[_MAX_PATH];
//
//   HANDLE   fhOut;
//   DWORD    nBytesWritten;
//   BOOL     bRet;
//   long     lCnt=0;
//   ADREXT   sAdrExt;
//
//   // Open roll file
//   LogMsg("Open Roll file %s", acRollFile);
//   fdRoll = fopen(acRollFile, "r");
//   if (fdRoll == NULL)
//   {
//      printf("Error opening roll file: %s\n", acRollFile);
//      return 2;
//   }
//
//   // Open Output file
//   sprintf(acOutFile, acAddrTmpl, myCounty.acCntyCode);
//   LogMsg("Open output file %s", acOutFile);
//   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
//          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);
//
//   if (fhOut == INVALID_HANDLE_VALUE)
//      return 4;
//
//   // Set output record length
//   iRecLen = SIZ_ADREXT;
//
//   // Merge loop
//   while (!feof(fdRoll))
//   {
//      // Get roll rec
//      pTmp = fgets(acRollRec, iRollLen, fdRoll);
//      if (!pTmp)
//         break;
//
//      // Create new R01 record
//      memset((void *)&sAdrExt, ' ', iRecLen);
//      Ccx_CreateSitusRec(&sAdrExt, acRollRec);
//
//      bRet = WriteFile(fhOut, (char *)&sAdrExt, iRecLen, &nBytesWritten, NULL);
//      if (!(++lCnt % 1000))
//         printf("\r%u", lCnt);
//
//      if (!bRet)
//      {
//         LogMsg("Error writing to output file at record %d\n", lCnt);
//         break;
//      }
//   }
//
//   // Close files
//   if (fdRoll)
//      fclose(fdRoll);
//
//   if (fhOut)
//      CloseHandle(fhOut);
//
//   LogMsg("Total output records:       %u", lCnt);
//   printf("\nTotal output records: %u", lCnt);
//
//   lRecCnt = lCnt;
//   return 0;
//}

/******************************* Ccx_MergeProp8 *******************************
 *
 *
 ******************************************************************************/

int Ccx_MergeProp8(char *pProp8File, int iSkip)
{
   char     *pTmp, acBuf[MAX_RECSIZE], acProp8Rec[MAX_RECSIZE];
   char     acRawFile[_MAX_PATH], acOutFile[_MAX_PATH];

   HANDLE   fhIn, fhOut;
   FILE     *fdProp8;

   int      iTmp, iProp8Match=0, iProp8Drop=0;
   DWORD    nBytesRead, nBytesWritten;
   BOOL     bRet, bEof;
   long     lRet=0, lCnt=0;

   sprintf(acRawFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "R01");
   sprintf(acOutFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "M01");

   // Rename files for processing
   if (_access(acRawFile, 0))
   {
      LogMsg("Missing input file %s.  Please recheck!", acRawFile);
      return -1;
   }

   // Open Prop8 file
   LogMsg("Open Prop8 file %s", pProp8File);
   fdProp8 = fopen(pProp8File, "r");
   if (fdProp8 == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", pProp8File);
      return -2;
   }

   // Open Input file
   LogMsg("Open input file %s", acRawFile);
   fhIn = CreateFile(acRawFile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhIn == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening raw file: %s\n", acRawFile);
      return -3;
   }

   // Open Output file
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error creating output file: %s\n", acOutFile);
      return -4;
   }

   // Get first lien rec
   pTmp = fgets((char *)&acProp8Rec[0], 1024, fdProp8);
   if (acProp8Rec[1] > '9')
      pTmp = fgets((char *)&acProp8Rec[0], 1024, fdProp8);

   bEof = (pTmp ? false:true);

   // Copy skip record
   memset(acBuf, ' ', iRecLen);
   while (iSkip-- > 0)
   {
      ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);
      WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
   }

   // Merge loop
   while (!bEof)
   {
      bRet = ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);

      // Check for EOF
      if (!bRet)
      {
         LogMsg("Error reading input file %s (%f)", acRawFile, GetLastError());
         break;
      }

      // EOF ?
      if (!nBytesRead)
         break;

NextProp8Rec:
      iTmp = memcmp(acBuf, acProp8Rec, iApnLen);
      if (!iTmp)
      {
         // Set prop8 flag - RVC found in special LDR copy.
         if (acProp8Rec[513] == 'P' && acProp8Rec[514] == '8')
         {
            iProp8Match++;
            acBuf[OFF_PROP8_FLG] = 'Y';
         }

         // Read next prop8 record
         pTmp = fgets(acProp8Rec, 1024, fdProp8);

         if (!pTmp)
            bEof = true;    // Signal to stop
      } else if (iTmp > 0)       
      {
         // Get next prop8 record
         pTmp = fgets(acProp8Rec, 1024, fdProp8);
         iProp8Drop++;

         if (!pTmp)
            bEof = true;    // Signal to stop
         else
            goto NextProp8Rec;
      }

      WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);

      if (!bRet)
      {
         LogMsg("***** Error occurs: %d\n", GetLastError());
         lRet = -1;
         break;
      }
   }

   // Close files
   if (fdProp8)
      fclose(fdProp8);
   if (fhOut)
      CloseHandle(fhOut);
   if (fhIn)
      CloseHandle(fhIn);

   // Rename output file
   iTmp = remove(acRawFile);
   if (!iTmp)
   {
      iTmp = rename(acOutFile, acRawFile);
      if (iTmp < 0)
      {
         LogMsgD("***** Error renaming file %s to %s", acOutFile, acRawFile);
         iTmp = -1;
      }
   } else
   {
      LogMsgD("***** Error removing file %s", acRawFile);
      iTmp = -1;
   }

   LogMsgD("Total output records:       %u", lCnt);
   LogMsgD("Total records matched:      %u", iProp8Match);
   LogMsgD("Total records dropped:      %u", iProp8Drop);

   lRecCnt = lCnt;
   return iTmp;
}

/******************************* Ccx_SetProp8 *********************************
 *
 *
 ******************************************************************************/

int Ccx_SetProp8(char *pProp8File)
{
   char     *pTmp, acTmp[256], acProp8Rec[MAX_RECSIZE];
   FILE     *fdProp8;

   int      iProp8Match=0, iProp8Drop=0;
   bool     bRet;
   long     lCnt=0;

   // Open Prop8 file
   LogMsg("Open Prop8 file %s", pProp8File);
   fdProp8 = fopen(pProp8File, "r");
   if (fdProp8 == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", pProp8File);
      return -2;
   }

   // Open DB table
   LogMsg("Open DB table %s", acCntyTbl);
   try
   {
      // open the database connection
      bRet = dbLoadConn.Connect(acMdbProvider);
   } AdoCatch(e)
   {
      LogMsg("***** Error connecting to server: %s", acMdbProvider);
      LogMsgD("%s\n", ComError(e));
      return -1;
   }

   // Get first lien rec
   pTmp = fgets((char *)&acProp8Rec[0], 1024, fdProp8);
   if (acProp8Rec[1] > '9')
      pTmp = fgets((char *)&acProp8Rec[0], 1024, fdProp8);

   // Merge loop
   while (pTmp)
   {
      // Set prop8 flag - RVC found in special LDR copy.
      if (acProp8Rec[513] == 'P' && acProp8Rec[514] == '8')
      {
         // Update flag
         sprintf(acTmp, "UPDATE %s SET Prop8_Flg='Y' WHERE Apn='%.*s'", acCntyTbl, RSIZ_APN, acProp8Rec);
         try
         {
            dbLoadConn.ExecuteCommand(acTmp);
            iProp8Match++;
         } AdoCatch (e)
         {
            LogMsg("%s", ComError(e));
            iProp8Drop++;
         }        
      }

      // Read next prop8 record
      pTmp = fgets(acProp8Rec, 1024, fdProp8);

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdProp8)
      fclose(fdProp8);

   LogMsgD("Total records processed:    %u", lCnt);
   LogMsgD("Total records matched:      %u", iProp8Match);
   LogMsgD("Total records dropped:      %u", iProp8Drop);

   lRecCnt = lCnt;
   return 0;
}

/******************************* Ccx_ExtractProp8 *****************************
 *
 * Reason code is at pos 514, check for "P8"
 *
 ******************************************************************************/

int Ccx_ExtractProp8(char *pProp8File)
{
   char     *pTmp, acTmp[256], acProp8Rec[MAX_RECSIZE];
   char     acOutFile[_MAX_PATH];
   int      iProp8Match=0;
   long     lCnt=0;
   FILE     *fdProp8, *fdExt;

   LogMsg0("Extract Prop8 APN from %s LDR file for %s", myCounty.acYearAssd, myCounty.acCntyCode);

   sprintf(acTmp, "%s_%sP8", myCounty.acCntyCode, myCounty.acYearAssd);
   sprintf(acOutFile, acRawTmpl, myCounty.acCntyCode, acTmp, "txt");

   // Open Prop8 file
   LogMsg("Open LDR with VRC file %s", pProp8File);
   fdProp8 = fopen(pProp8File, "r");
   if (fdProp8 == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", pProp8File);
      return -2;
   }

   // Open Output file
   LogMsg("Open output file %s", acOutFile);
   fdExt = fopen(acOutFile, "w");
   if (fdExt == NULL)
   {
      LogMsg("***** Error creating output file: %s\n", acOutFile);
      return -4;
   }

   // Output header
   fputs("APN\n", fdExt);

   // Get first lien rec
   pTmp = fgets((char *)&acProp8Rec[0], 1024, fdProp8);
   if (acProp8Rec[1] > '9')
      pTmp = fgets((char *)&acProp8Rec[0], 1024, fdProp8);

   // Merge loop
   while (pTmp)
   {
      // Set prop8 flag - RVC found in special LDR copy.
      if (acProp8Rec[513] == 'P' && acProp8Rec[514] == '8')
      {
         // Update flag
         sprintf(acTmp, "%.*s\n", RSIZ_APN, acProp8Rec);
         fputs(acTmp, fdExt);
         iProp8Match++;
      }

      // Read next prop8 record
      pTmp = fgets(acProp8Rec, 1024, fdProp8);

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdProp8)
      fclose(fdProp8);
   if (fdExt)
      fclose(fdExt);

   LogMsgD("Total records processed:      %u", lCnt);
   LogMsgD("Total records extracted:      %u", iProp8Match);
   return 0;
}

/******************************* Ccx_LdrUpdateSale ****************************
 *
 * Update sale for a specific year.
 *
 ******************************************************************************/

int Ccx_LdrUpdateSale(char *pYear, bool bFixTRA, int iSkip=1)
{
   char     acTmp[_MAX_PATH], acBuf[MAX_RECSIZE];
   char     acRawFile[_MAX_PATH], acOutFile[_MAX_PATH];

   HANDLE   fhIn, fhOut;

   int      iRet, iTmp, iRollUpd=0;
   DWORD    nBytesRead;
   DWORD    nBytesWritten;
   BOOL     bRet, bEof;
   long     lRet=0, lCnt=0;

   LogMsgD("Merge CCX sale for LDR %s...", pYear);

   sprintf(acOutFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "Tmp");

   GetIniString("Data", "LdrCSal", "", acTmp, _MAX_PATH, acIniFile);
   sprintf(acCSalFile, acTmp, myCounty.acCntyCode, myCounty.acCntyCode, pYear);

   GetIniString("Data", "LdrRoll", "", acTmp, _MAX_PATH, acIniFile);
   sprintf(acRawFile, acTmp, myCounty.acCntyCode, myCounty.acCntyCode, pYear, "R01");

   // Check raw file
   if (_access(acRawFile, 0))
   {
      LogMsg("***** Missing input file %s.  Please recheck!", acRawFile);
      return 1;
   }

   // Open Sales file
   LogMsg("Open Sales file %s", acCSalFile);
   fdSale = fopen(acCSalFile, "r");
   if (fdSale == NULL)
   {
      LogMsg("***** Error opening Sales file: %s\n", acCSalFile);
      return 2;
   }

   // Open Input file
   LogMsg("Open input file %s", acRawFile);
   fhIn = CreateFile(acRawFile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);
   if (fhIn == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening input file: %s\n", acRawFile);
      return 2;
   }

   // Open Output file
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening output file: %s\n", acOutFile);
      return 2;
   }

   // Copy skip record
   memset(acBuf, ' ', iRecLen);
   while (iSkip-- > 0)
   {
      ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);
      // Ignore record start with 99999999
      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
   }

   // Merge loop
   bEof = false;
   while (!bEof)
   {
      bRet = ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);

      // Check for EOF
      if (!bRet)
      {
         LogMsg("***** Error reading input file %s (%f)", acRawFile, GetLastError());
         break;
      }

      // EOF ?
      if (iRecLen != nBytesRead)
         break;

#ifdef _DEBUG
      //if (!memcmp(acBuf, "4056400874", 9))
      //   iRet = 1;
#endif

      // Fix TRA
      if (bFixTRA)
      {
         iTmp = atoin(&acBuf[OFF_TRA], 6);
         if (iTmp > 0)
         {
            iRet = sprintf(acTmp, "%.6d", iTmp);
            memcpy(&acBuf[OFF_TRA], acTmp, iRet);
         }
      }

      // Merge Sales
      ClearOldSale(acBuf);
      if (fdSale)
         iRet = Ccx_MergeSale(acBuf);

      // Get last recording date
      iTmp = atoin((char *)&acBuf[OFF_TRANSFER_DT], SIZ_TRANSFER_DT);
      if (iTmp >= lToday)
         LogMsg0("*** WARNING: Bad last recording date %d at record# %d -->%.10s", iTmp, lCnt, acBuf);
      else if (lLastRecDate < iTmp)
         lLastRecDate = iTmp;

      // Write output
      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);

      if (!bRet)
      {
         LogMsg("Error occurs: %d\n", GetLastError());
         lRet = WRITE_ERR;
         break;
      }
   }

   // Close files
   if (fdSale)
      fclose(fdSale);
   if (fhOut)
      CloseHandle(fhOut);
   if (fhIn)
      CloseHandle(fhIn);

   LogMsg("Total output records:       %u", lCnt);
   LogMsg("Total Sale matched:         %u", lSaleMatch);
   LogMsg("Total Sale skipped:         %u", lSaleSkip);

   printf("\nTotal output records: %u\n", lCnt);

   // Rename output file
   sprintf(acTmp, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "Sav");
   if (!_access(acTmp, 0))
      iRet = DeleteFile(acTmp);
   iRet = rename(acRawFile, acTmp);
   iRet = rename(acOutFile, acRawFile);

   lRecCnt = lCnt;
   return 0;
}

/*****************************************************************************/

int CCX_ConvertHistSale(char *pCnty)
{
   char     acInbuf[1024], acOutbuf[1024], acOutFile[_MAX_PATH], acTmp[32];
   long     lCnt=0, lOut=0, iRet, lPrice, lDrop=0;
   FILE     *fdOut;

   SCSAL_REC *pCSale = (SCSAL_REC *)&acOutbuf[0];
   CCX_CSAL  *pSale  = (CCX_CSAL  *)&acInbuf[0];

   if (_access(acCSalFile, 0))
   {
      LogMsg("***** ConvertSaleData(): Missing input file: %s", acCSalFile);
      return -1;
   }

   sprintf(acOutFile, acESalTmpl, pCnty, pCnty, "Sls");
   LogMsg("Convert %s to %s.", acCSalFile, acOutFile);

   // Open input file
   LogMsg("Open cumsale file %s", acCSalFile);
   fdSale = fopen(acCSalFile, "r");
   if (fdSale == NULL)
   {
      LogMsg("***** Error opening cumsale file: %s\n", acCSalFile);
      return -2;
   }

   // Open output file
   LogMsg("Create output sale file %s", acOutFile);
   fdOut = fopen(acOutFile, "w");
   if (fdOut == NULL)
   {
      LogMsg("***** Error creating output sale file: %s\n", acOutFile);
      return -3;
   }

   // Convert loop
   while (!feof(fdSale))
   {
      iRet = fread((char *)&acInbuf[0], 1, sizeof(CCX_CSAL), fdSale);
      if (iRet != sizeof(CCX_CSAL))
         break;

      // Drop bad record
      iRet = atoin(pSale->Apn, 6);
      if (acInbuf[0] == ' ' || iRet == 0 || iRet > 999998 || pSale->DeedType != 'G')
      {
         lCnt++;
         lDrop++;
         continue;
      }

      // Reformat sale
      memset(acOutbuf, ' ', sizeof(SCSAL_REC));
      memcpy(pCSale->Apn, pSale->Apn, SSIZ_APN);

      iRet = Ccx_FormatDoc(acTmp, pSale->DeedVol, pSale->DeedPage, pSale->SaleDate);
      memcpy(pCSale->DocNum, acTmp, SALE_SIZ_DOCNUM);

      memcpy(pCSale->DocDate, pSale->SaleDate, SALE_SIZ_DOCDATE);

      // Doctype
      pCSale->DocType[0] = pSale->DeedType;

      // Strip out all sale price < 10 for hiding request 
      lPrice = atoin(pSale->PriceFromStamp, SSIZ_PRICE_FROM_STAMPS);
      if (lPrice > 9 && memcmp(pSale->Apn, "900", 3) && memcmp(pSale->Apn, "901", 3))
      {
         sprintf(acTmp, "%*u", SIZ_SALE1_AMT, lPrice);
         memcpy(pCSale->SalePrice, acTmp, SIZ_SALE1_AMT);
      }

      // Write to output file
      if (!iRet)
      {
         pCSale->CRLF[0] = '\n';
         pCSale->CRLF[1] = 0;

         fputs(acOutbuf, fdOut);
         lOut++;
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdSale)
      fclose(fdSale);
   if (fdOut)
      fclose(fdOut);

   LogMsg("Total input records:        %u", lCnt);
   LogMsg("Total output records:       %u", lOut);
   LogMsg("Total records dropped:      %u", lDrop);

   printf("\nTotal output records: %u\n", lOut);
   return lOut;
}

/***************************** Ccx_ExtrSaleFromRoll **************************
 *
 * Create sale record with full APN and set ApnMatched flag to 'Y'.
 *
 * Return 0 if successful, otherwise error
 *
 *****************************************************************************/

int Ccx_ExtrSaleFromRoll(char *pOutbuf, char *pRollRec)
{
   char     acTmp[256], acDocNum[32];
   long     lTmp;
   int      iRet;

   CCX_ROLL  *pRec = (CCX_ROLL *)pRollRec;
   SCSAL_REC *pSale= (SCSAL_REC *)pOutbuf;

   // Clear output buffer
   memset(pOutbuf, ' ', sizeof(SCSAL_REC));

      // Start copying data
   memcpy(pOutbuf, pRec->Apn, iApnLen);
   iRet = -1;

   // Transfer - YYMMDD
   if (memcmp(pRec->CurDeedDate, "000000", 6) > 0)
   {
      // Sale price
      lTmp = atoin(pRec->SalePrice, RSIZ_SALE_PRICE);
      // Ignore sale price on book 900 & 901
      if (lTmp > 1000 && !(!memcmp(pRec->Apn, "90", 2) && pRec->Apn[2] < '2'))
      {
         iRet = sprintf(acTmp, "%*u", SIZ_SALE1_AMT, lTmp);
         memcpy(pSale->SalePrice, acTmp, iRet);

         /*
         - DocNum format:
           + Before 1994 : 99999-999
           + 1994-present: YY999-999
         */
         // Apply transfer
         if (memcmp(pRec->CurDeedDate, &acToday[2], 2) <= 0)
            sprintf(acTmp, "20%.6s", pRec->CurDeedDate);
         else
            sprintf(acTmp, "19%.6s", pRec->CurDeedDate);
         memcpy(pSale->DocDate, acTmp, SIZ_TRANSFER_DT);

         iRet = Ccx_FormatDoc(acDocNum, pRec->CurDeedNum, NULL, acTmp);
         if (!iRet)
            memcpy(pSale->DocNum, acDocNum, SIZ_TRANSFER_DOC);

         // Mailing

         // Owner
         memcpy(pSale->Name1, pRec->Name1, sizeof(pRec->Name1));
         memcpy(pSale->Name2, pRec->Name1, sizeof(pRec->Name2));

         // Seller

         pSale->ApnMatched = 'Y';
         strcpy(pSale->CRLF, "\n");
      }
   }

   return iRet;
}

/***************************** Ccx_ExtrSaleFromSrm ***************************
 *
 * Create sale record with full APN and set ApnMatched flag to 'Y'.
 *
 * Return 0 if successful, otherwise error
 *
 *****************************************************************************/

int Ccx_ExtrSaleFromSrm(char *pOutbuf, char *pRollRec)
{
   char     acTmp[256], acDocNum[32];
   long     lTmp;
   int      iRet;

   CCX_LIEN  *pRec = (CCX_LIEN *)pRollRec;
   SCSAL_REC *pSale= (SCSAL_REC *)pOutbuf;

   // Clear output buffer
   memset(pOutbuf, ' ', sizeof(SCSAL_REC));

      // Start copying data
   memcpy(pOutbuf, pRec->Apn, iApnLen);
   iRet = -1;

   // Transfer - YYMMDD
   if (memcmp(pRec->CurDeedDate, "000000", 6) > 0)
   {
      // Sale price
      lTmp = atoin(pRec->SalePrice, RSIZ_SALE_PRICE);
      // Ignore sale price on book 900 & 901
      if (lTmp > 1000 && !(!memcmp(pRec->Apn, "90", 2) && pRec->Apn[2] < '2'))
      {
         iRet = sprintf(acTmp, "%*u", SIZ_SALE1_AMT, lTmp);
         memcpy(pSale->SalePrice, acTmp, iRet);

         // Apply transfer
         memcpy(pSale->DocDate, pRec->CurDeedDate, SIZ_TRANSFER_DT);

         iRet = Ccx_FormatDoc(acDocNum, pRec->CurDeedNum, NULL, pRec->CurDeedDate);
         if (!iRet)
            memcpy(pSale->DocNum, acDocNum, SIZ_TRANSFER_DOC);

         // Mailing

         // Owner
         memcpy(pSale->Name1, pRec->Name1, sizeof(pRec->Name1));
         memcpy(pSale->Name2, pRec->Name1, sizeof(pRec->Name2));

         // Seller

         pSale->ApnMatched = 'Y';
         strcpy(pSale->CRLF, "\n");
      }
   }

   return iRet;
}

/********************************* Ccx_ExtrRSale ******************************
 *
 * Extract sale1 from roll file then merge/update cum sale.
 *
 ******************************************************************************/

int Ccx_ExtrRSale(char *pRSale)
{
   char     acBuf[1024], acRollRec[1024];
   int      iRet, iUpdateSale=0;
   long     lCnt=0;

   LogMsg0("Extract sale1 from CCX Roll ...");

   // Open Roll file
   LogMsg("Open Roll file %s", acRollFile);
   fdRoll = fopen(acRollFile, "r");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", acRollFile);
      return -2;
   }

   // Create output file
   LogMsg("Create updated sale file %s", pRSale);
   fdSale = fopen(pRSale, "w");
   if (fdSale == NULL)
   {
      LogMsg("***** Error creating extract sale file: %s\n", pRSale);
      return -3;
   }
   
   // Collect all records that has sale1 with sale price
   while (fgets((char *)&acRollRec[0], MAX_RECSIZE, fdRoll))
   {
#ifdef _DEBUG
      //if (!memcmp(acRollRec, "001102037", 9))
      //   iRet = 1;
#endif
      // Merge roll data
      if (iSrmVersion == 1)
         iRet = Ccx_ExtrSaleFromRoll(acBuf, acRollRec);
      else
         iRet = Ccx_ExtrSaleFromSrm(acBuf, acRollRec);
      if (!iRet)
      {
         fputs(acBuf, fdSale);
         iUpdateSale++;
      }

		if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdRoll)
      fclose(fdRoll);
   if (fdSale)
      fclose(fdSale);

   printf("\n");
   LogMsgD("Total input records:        %u", lCnt);
   LogMsg("Total Sale extracted:       %u", iUpdateSale);

   return 0;
}

/*****************************************************************************
 *
 * Notes: Output file is sorted
 *
 * Return 0 if success, -1 if file not found
 *
 *****************************************************************************/

int Ccx_AppendToCumSale(char *pSaleFile)
{
   int iRet;
   char  acOutFile[_MAX_PATH], acTmpFile[_MAX_PATH], acSortCtl[_MAX_PATH];

   LogMsg0("Append %s to cumsale file.", pSaleFile);

   // Append to cum sale file
   sprintf(acCSalFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "sls");
   if (!_access(acCSalFile, 0))
   {
      // Sort/merge sale file - APN (asc), Saledate (asc), DocNum (asc)
      sprintf(acSortCtl, "S(1,%d,C,A,27,8,C,A,15,%d,C,A,57,10,C,D) DUPO(1,34)", iApnLen, SALE_SIZ_DOCNUM);
      sprintf(acOutFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "srt");
      sprintf(acTmpFile, "%s+%s", pSaleFile, acCSalFile);
      iRet = sortFile(acTmpFile, acOutFile, acSortCtl);
      if (iRet > 0)
      {
         sprintf(acTmpFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "sl1");
         if (!_access(acTmpFile, 0))
            DeleteFile(acTmpFile);

         LogMsg("Rename %s to %s", acCSalFile, acTmpFile);
         iRet = rename(acCSalFile, acTmpFile);

         // Rename srt to SLS file
         LogMsg("Rename %s to %s", acOutFile, acCSalFile);
         iRet = rename(acOutFile, acCSalFile);
      }
   } else
   {
      iRet = -1;
      LogMsg("***** Error: History sale file missing: %s", acCSalFile);
   }

   return iRet;
}

/********************************* Ccx_MatchApn *******************************
 *
 * Match sale APN to roll APN and use roll APN as primary APN.
 *
 ******************************************************************************

int Ccx_MatchSaleApn(void)
{
   char     acTmpSale[MAX_RECSIZE], acBuf[MAX_RECSIZE], acSaleRec[1024];
   int      iRet, iUpdateSale=0;
   long     lCnt=0;

   LogMsgD("Match sale APN ...");

   // Check roll file
   sprintf(acBuf, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "R01");
   if (iRet = locateRecInR01(acBuf, "", 0, R01_OPEN))
   {
      if (iRet == -1)
         LogMsg("***** Missing input file %s", acBuf);
      else
         LogMsg("***** Error opening %s", acBuf);
      return iRet;
   }

   // Open history sale file
   LogMsg("Open cum sale file %s", acCSalFile);
   fdCSale = fopen(acCSalFile, "r");
   if (fdCSale == NULL)
   {
      LogMsg("***** Error opening cum sale file (%d): %s\n", _errno, acCSalFile);
      return -2;
   }

   // Create output file
   sprintf(acTmpSale, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "tmp");
   LogMsg("Create updated sale file %s", acTmpSale);
   fdSale = fopen(acTmpSale, "w");
   if (fdSale == NULL)
   {
      LogMsg("***** Error creating temp sale file: %s\n", acTmpSale);
      return -3;
   }
   
   // Collect all records that has sale1 with sale price
   while (fgets((char *)&acSaleRec[0], 1024, fdCSale))
   {
#ifdef _DEBUG
      //if (!memcmp(acSaleRec, "002040036", 9))
      //   iRet = 0;
#endif
      // Update sale APN if matched roll APN
      if (iRet = memcmp(acBuf, acSaleRec, iSaleApnLen))
         iRet = locateRecInR01(acBuf, acSaleRec, iSaleApnLen, R01_SRCHAPN);
      if (!iRet)
      {
         memcpy(acSaleRec, acBuf, iApnLen);
         iUpdateSale++;
      }

      fputs(acSaleRec, fdSale);
		if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdCSale)
      fclose(fdCSale);
   if (fdSale)
      fclose(fdSale);

   // Close roll file
   locateRecInR01("", "", 0, R01_CLOSE);

   // Rename sale file
   sprintf(acBuf, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "old");
   if (!_access(acBuf, 0))
      DeleteFile(acBuf);

   LogMsg("Rename %s to %s", acCSalFile, acBuf);
   iRet = rename(acCSalFile, acBuf);
   LogMsg("Rename %s to %s", acTmpSale, acCSalFile);
   iRet = rename(acTmpSale, acCSalFile); 

   printf("\n");
   LogMsg("Total input records:        %u", lCnt);
   LogMsg("Total Sale APN updated:     %u", iUpdateSale);

   return iRet;
}

/****************************** Ccx_FmtChar **********************************
 *
 *
 *****************************************************************************/

int Ccx_FmtChar(char *pOutbuf, char *pInbuf)
{
   char     acTmp[_MAX_PATH];
   int      iTmp, lTmp, lTmp2;

   STDCHAR  *pCharRec=(STDCHAR *)pOutbuf;
   CCX_CHAR *pRawRec =(CCX_CHAR *)pInbuf;
   
   // Init output buffer
   memset(pOutbuf, ' ', sizeof(STDCHAR));

   // APN
   memcpy(pCharRec->Apn, pRawRec->Apn, CSIZ_APN);

   // Format APN
   iTmp = sprintf(acTmp, "%.3s-%.3s-%.3s", pRawRec->Apn, &pRawRec->Apn[3], &pRawRec->Apn[6]); 
   memcpy(pCharRec->Apn_D, acTmp, iTmp);

#ifdef _DEBUG
   //if (!memcmp(pCharRec->Apn, "139040027", 9) )
   //   iTmp = 0;
#endif

   // Acreage
   lTmp = atoin(pRawRec->LotSqft, CSIZ_LOT_SQFT);
   if (lTmp > 100)
   {
      iTmp = sprintf(acTmp, "%d", lTmp);
      memcpy(pCharRec->LotSqft, acTmp, iTmp);

      lTmp2 = (long)(((double)lTmp*ACRES_FACTOR)/SQFT_PER_ACRE);
      iTmp = sprintf(acTmp, "%d", lTmp2);
      memcpy(pCharRec->LotAcre, acTmp, iTmp);
   } else if (pRawRec->PdrType == 'O')
   {
      lTmp = atoin(pRawRec->Acres, CSIZ_LOT_SQFT);
      if (lTmp > 0)
      {
         if (lTmp > 500)
         {
            iTmp = sprintf(acTmp, "%d", lTmp);
            memcpy(pCharRec->LotSqft, acTmp, iTmp);
         } else
         {
            iTmp = sprintf(acTmp, "%d", lTmp);
            memcpy(pCharRec->LotAcre, acTmp, iTmp);
         }
      }
   }

   // Bldg#
   lTmp = atoin(pRawRec->BldgNum, CSIZ_BLDG_NUMBER);
   if (lTmp > 0)
   {
      iTmp = sprintf(acTmp, "%d", lTmp);
      memcpy(pCharRec->BldgSeqNo, acTmp, iTmp);
   }

   // Year Built
   lTmp = atoin(pRawRec->YearBuilt, CSIZ_EFF_YR);
   if (lTmp > 0)
   {
      iTmp = sprintf(acTmp, "%d", lTmp);
      memcpy(pCharRec->YrBlt, acTmp, iTmp);
   }

   // Eff Year
   lTmp = atoin(pRawRec->EffYear, CSIZ_EFF_YR);
   if (lTmp > 0)
   {
      iTmp = sprintf(acTmp, "%d", lTmp);
      memcpy(pCharRec->YrEff, acTmp, iTmp);
   }

   // BldgSqft
   lTmp = atoin(pRawRec->BldgSqft, CSIZ_BLDG_SQFT);
   if (lTmp > 0)
   {
      iTmp = sprintf(acTmp, "%d", lTmp);
      memcpy(pCharRec->Sqft_1stFl, acTmp, iTmp);

      lTmp2 = atoin(pRawRec->BldgSqft2, CSIZ_BLDG_SQFT);
      if (lTmp2 > 0)
      {
         iTmp = sprintf(acTmp, "%d", lTmp2);
         memcpy(pCharRec->Sqft_2ndFl, acTmp, iTmp);
         lTmp += lTmp2;

         memcpy(pCharRec->Stories, "2.0", 3);
      } else
         memcpy(pCharRec->Stories, "1.0", 3);

      // BsmtSqft
      lTmp2 = atoin(pRawRec->BsmtSqft, CSIZ_BASEMENT_SQFT);
      if (lTmp2 > 0)
      {
         iTmp = sprintf(acTmp, "%d", lTmp2);
         memcpy(pCharRec->BsmtSqft, acTmp, iTmp);
         lTmp += lTmp2;
      }

      iTmp = sprintf(acTmp, "%d", lTmp);
      memcpy(pCharRec->BldgSqft, acTmp, iTmp);
   }

   // GarSqft
   lTmp = atoin(pRawRec->GarSqft, CSIZ_GARAGE_SQFT);
   if (lTmp > 0)
   {
      iTmp = sprintf(acTmp, "%d", lTmp);
      memcpy(pCharRec->GarSqft, acTmp, iTmp);
   }

   // Beds
   lTmp = atoin(pRawRec->Beds, CSIZ_BEDROOMS);
   if (lTmp > 0)
   {
      iTmp = sprintf(acTmp, "%d", lTmp);
      memcpy(pCharRec->Beds, acTmp, iTmp);
   }

   // Baths
   lTmp = atoin(pRawRec->Baths, CSIZ_BATHSROOMS-1);
   if (lTmp > 0)
   {
      iTmp = sprintf(acTmp, "%d", lTmp);
      memcpy(pCharRec->FBaths, acTmp, iTmp);
      memcpy(pCharRec->Bath_4Q, acTmp, iTmp);

      // Half bath
      iTmp = pRawRec->Baths[CSIZ_BATHSROOMS-1] & 0x0F;
      if (iTmp > 0)
      {
         pCharRec->HBaths[0] = '1';
         if (iTmp == 5)
            pCharRec->Bath_2Q[0] = '1';
         else if (iTmp < 5)
            pCharRec->Bath_1Q[0] = '1';
         else
            pCharRec->Bath_3Q[0] = '1';
      }
   }

   // Rooms
   lTmp = atoin(pRawRec->Rooms, CSIZ_UNITS_ROOMS);
   if (lTmp > 0)
   {
      iTmp = sprintf(acTmp, "%d", lTmp);
      if (pRawRec->PdrType == 'O')              // Business office
         memcpy(pCharRec->Units, acTmp, iTmp);
      else
         memcpy(pCharRec->Rooms, acTmp, iTmp);
   }

   // Pool
   if (pRawRec->Pool == 'Y')
      pCharRec->Pool[0] = 'P';

   // View
   if (pRawRec->View == 'Y' || pRawRec->View == 'N')
      pCharRec->View[0] = pRawRec->View;

   // Zoning
   if (pRawRec->Zoning[0] > ' ')
      memcpy(pCharRec->Zoning, pRawRec->Zoning, CSIZ_ZONE);

   // UseCode
   if (pRawRec->UseCode[0] > ' ')
      memcpy(pCharRec->LandUse, pRawRec->UseCode, CSIZ_USE_CODE);

   if (pRawRec->VacantLot == 'V')
      pCharRec->VacantLot = 'Y';

   pCharRec->CRLF[0] = '\n';
   pCharRec->CRLF[1] = '\0';

   return 0;
}

/***************************** Ccx_ConvStdChar *******************************
 *
 * Extract characteristics from pdffile.txt to Ccx_Char.dat
 *
 * Return 0 if successful, otherwise error
 *
 *****************************************************************************/

int Ccx_ConvStdChar(char *pInfile, char *pOutfile)
{
   char  *pTmp, acCharRec[2048], acInRec[MAX_RECSIZE], acTmpFile[_MAX_PATH];
   long  lCnt=0, lChars=0, iRet;
   FILE  *fdIn;

   LogMsg0("Extracting Chars from %s", pInfile);

   // Check current sale file
   if (_access(pInfile, 0))
   {
      LogMsg("***** Missing input file %s", pInfile);
      return -1;
   }

   // Open char file
   LogMsg("Open char file to extract CHARS %s", pInfile);
   fdIn = fopen(pInfile, "r");
   if (fdIn == NULL)
   {
      LogMsg("***** Error opening char file: %s\n", pInfile);
      return -2;
   }

   // Create output file
   sprintf(acTmpFile, "%s\\%s\\%s_Char.tmp", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode); 
   LogMsg("Create output chars file %s", acTmpFile);
   fdChar = fopen(acTmpFile, "w");
   if (fdChar == NULL)
   {
      LogMsg("***** Error creating char file: %s\n", acTmpFile);
      return -3;
   }

   // Merge loop
   while (!feof(fdIn))
   {
      // Get next record
      pTmp = fgets(acInRec, MAX_RECSIZE, fdIn);
      if (!pTmp)
         break;

      iRet = Ccx_FmtChar(acCharRec, acInRec);

      if (!iRet)
      {
         fputs(acCharRec, fdChar);
         lChars++;
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);

   }

   // Close files
   if (fdChar)
      fclose(fdChar);
   if (fdIn)
      fclose(fdIn);

   LogMsg("Total input records processed: %u", lCnt);
   LogMsg("Total char records output:     %u\n", lChars);

   // Sort output
   sprintf(acInRec, "S(1,%d,C,A) ", iApnLen);
   iRet = sortFile(acTmpFile, pOutfile, acInRec);
   LogMsg("Output CHAR file: %s", pOutfile);

   return iRet;
}

/***************************** Ccx_ParseTaxDetail ****************************
 *
 * Type A: In the future, we may need to break the penalty amt into defferent entry.
 *         Currently it's included in TaxAmt.
 *
 * Return 0 if success
 *
 *****************************************************************************/

int Ccx_ParseTaxLevyA(char *pItems, char *pAgency, char *pInbuf)
{
   TAXDETAIL  *pItemsRec  = (TAXDETAIL *)pItems;
   TAXAGENCY  *pAgencyRec = (TAXAGENCY *)pAgency;
   TAXAGENCY  *pResult;
   LEVYREC    *pInRec     = (LEVYREC *)  pInbuf;
   double      dTmp;

   // Clear output buffer
   memset(pItems, 0, sizeof(TAXDETAIL));
   memset(pAgency, 0, sizeof(TAXAGENCY));

   // APN
   memcpy(pItemsRec->Apn, pInRec->Apn, SIZ_LV_APN);

   // Tax Year
   memcpy(pItemsRec->TaxYear, pInRec->TaxYear, SIZ_LV_YEAR);

   // BillNum
   sprintf(pItemsRec->BillNum, "%.4s%.*s", pInRec->TaxYear, SIZ_LV_BILLNUM, pInRec->BillNum);

   // Tax Desc
   if (pInRec->Spec.RecA.LevyDesc[0] > ' ')
   {
      strcpy(pItemsRec->TaxDesc, myTrim(pInRec->Spec.RecA.LevyDesc, SIZ_LV_LEVYDESC));
      strcpy(pAgencyRec->Agency, pItemsRec->TaxDesc);

      pResult = BSrchTaxDistName(pItemsRec->TaxDesc);
      if (pResult)
      {
         strcpy(pAgencyRec->Code, pResult->Code);
         strcpy(pAgencyRec->Phone, pResult->Phone);
         strcpy(pItemsRec->TaxCode, pResult->Code);
         pAgencyRec->TC_Flag[0] = pResult->TC_Flag[0];
         pItemsRec->TC_Flag[0] = pResult->TC_Flag[0];
      } else
      {
         LogMsg("+++ Unknown Agency-A: %.*s|%s", SIZ_LV_FUND, pInRec->Spec.RecA.Fund, pItemsRec->TaxDesc);
         //memcpy(pItemsRec->TaxCode, pInRec->Spec.RecA.Fund, SIZ_LV_FUND);
         //memcpy(pAgencyRec->Code, pInRec->Spec.RecA.Fund, SIZ_LV_FUND);
      }
   } 

   // Tax Rate
   dTmp = atoin(pInRec->Spec.RecA.TaxRate, SIZ_LV_TAXRATE) / 10000.0;
   if (dTmp > 0.0)
   {
      sprintf(pItemsRec->TaxRate, "%.4f", dTmp);
      //strcpy(pAgencyRec->TaxRate, pItemsRec->TaxRate);
   }

   // Tax Amount
   dTmp = atoin(pInRec->Spec.RecA.TaxAmt, SIZ_LV_TAXAMT) / 100.0;
   if (dTmp > 0.0)
   {
      sprintf(pItemsRec->TaxAmt, "%.2f", dTmp);
      return 0;
   } else
      return 3;
}

// Mello Roo
int Ccx_ParseTaxLevyB(char *pItems, char *pAgency, char *pInbuf)
{
   TAXDETAIL  *pItemsRec  = (TAXDETAIL *)pItems;
   TAXAGENCY  *pAgencyRec = (TAXAGENCY *)pAgency;
   TAXAGENCY  *pResult;
   LEVYREC    *pInRec     = (LEVYREC *)  pInbuf;
   double      dTmp;

   // Clear output buffer
   memset(pItems, 0, sizeof(TAXDETAIL));
   memset(pAgency, 0, sizeof(TAXAGENCY));

   // APN
   memcpy(pItemsRec->Apn, pInRec->Apn, SIZ_LV_APN);

   // Tax Year
   memcpy(pItemsRec->TaxYear, pInRec->TaxYear, SIZ_LV_YEAR);

   // BillNum
   sprintf(pItemsRec->BillNum, "%.4s%.*s", pInRec->TaxYear, SIZ_LV_BILLNUM, pInRec->BillNum);

   // Tax Desc
   if (pInRec->Spec.RecB.LevyDesc[0] > ' ')
   {
      strcpy(pItemsRec->TaxDesc, myTrim(pInRec->Spec.RecB.LevyDesc, SIZ_LV_LEVYDESC));
      strcpy(pAgencyRec->Agency, pItemsRec->TaxDesc);

      pResult = BSrchTaxDistName(pItemsRec->TaxDesc);
      if (pResult)
      {
         strcpy(pAgencyRec->Code, pResult->Code);
         strcpy(pAgencyRec->Phone, pResult->Phone);
         strcpy(pItemsRec->TaxCode, pResult->Code);
         pAgencyRec->TC_Flag[0] = pResult->TC_Flag[0];
         pItemsRec->TC_Flag[0] = pResult->TC_Flag[0];
      } else
      {
         LogMsg("+++ Unknown Agency-B: %.*s|%s", SIZ_LV_FUND, pInRec->Spec.RecB.Fund, pItemsRec->TaxDesc);
         memcpy(pItemsRec->TaxCode, pInRec->Spec.RecB.Fund, SIZ_LV_FUND);
         memcpy(pAgencyRec->Code, pInRec->Spec.RecB.Fund, SIZ_LV_FUND);
      }
   } 

   // Tax Amount
   dTmp = atoin(pInRec->Spec.RecB.TaxAmt, SIZ_LV_TAXAMT) / 100.0;
   if (dTmp > 0.0)
   {
      sprintf(pItemsRec->TaxAmt, "%.2f", dTmp);
      return 0;
   } else
      return 3;
}

// Special Assessment
int Ccx_ParseTaxLevyC(char *pItems, char *pAgency, char *pInbuf)
{
   TAXDETAIL  *pItemsRec  = (TAXDETAIL *)pItems;
   TAXAGENCY  *pAgencyRec = (TAXAGENCY *)pAgency;
   TAXAGENCY  *pResult;
   LEVYREC    *pInRec     = (LEVYREC *)  pInbuf;
   double      dTmp;

   // Clear output buffer
   memset(pItems, 0, sizeof(TAXDETAIL));
   memset(pAgency, 0, sizeof(TAXAGENCY));

   // APN
   memcpy(pItemsRec->Apn, pInRec->Apn, SIZ_LV_APN);

   // Tax Year
   memcpy(pItemsRec->TaxYear, pInRec->TaxYear, SIZ_LV_YEAR);

   // BillNum
   sprintf(pItemsRec->BillNum, "%.4s%.*s", pInRec->TaxYear, SIZ_LV_BILLNUM, pInRec->BillNum);

   // Tax Desc
   if (pInRec->Spec.RecC.LevyDesc[0] > ' ')
   {
      strcpy(pItemsRec->TaxDesc, myTrim(pInRec->Spec.RecC.LevyDesc, SIZ_LV_LEVYDESC));
      strcpy(pAgencyRec->Agency, pItemsRec->TaxDesc);

      pResult = BSrchTaxDistName(pItemsRec->TaxDesc);
      if (pResult)
      {
         strcpy(pAgencyRec->Code, pResult->Code);
         strcpy(pAgencyRec->Phone, pResult->Phone);
         strcpy(pItemsRec->TaxCode, pResult->Code);
         pAgencyRec->TC_Flag[0] = pResult->TC_Flag[0];
         pItemsRec->TC_Flag[0] = pResult->TC_Flag[0];
      } else
      {
         LogMsg("+++ Unknown Agency-C: %.*s|%s", SIZ_LV_FUND, pInRec->Spec.RecC.Fund, pItemsRec->TaxDesc);
         memcpy(pItemsRec->TaxCode, pInRec->Spec.RecC.Fund, SIZ_LV_FUND);
         memcpy(pAgencyRec->Code, pInRec->Spec.RecC.Fund, SIZ_LV_FUND);
      }
   } 

   // Tax Rate
   dTmp = atoin(pInRec->Spec.RecC.TaxRate, SIZ_LV_TAXRATE) / 10000.0;
   if (dTmp > 0.0)
   {
      sprintf(pItemsRec->TaxRate, "%.4f", dTmp);
      //strcpy(pAgencyRec->TaxRate, pItemsRec->TaxRate);
   }

   // Tax Amount
   dTmp = atoin(pInRec->Spec.RecC.TaxAmt, SIZ_LV_TAXAMT) / 100.0;
   if (dTmp > 0.0)
   {
      sprintf(pItemsRec->TaxAmt, "%.2f", dTmp);
      return 0;
   } else
      return 3;
}

// Weed Abatement (clean up weed on street/property under specific juridiction)
int Ccx_ParseTaxLevyD(char *pItems, char *pAgency, char *pInbuf)
{
   TAXDETAIL  *pItemsRec  = (TAXDETAIL *)pItems;
   TAXAGENCY  *pAgencyRec = (TAXAGENCY *)pAgency;
   TAXAGENCY  *pResult;
   LEVYREC    *pInRec     = (LEVYREC *)  pInbuf;
   double      dTmp;

   // Clear output buffer
   memset(pItems, 0, sizeof(TAXDETAIL));
   memset(pAgency, 0, sizeof(TAXAGENCY));

   // APN
   memcpy(pItemsRec->Apn, pInRec->Apn, SIZ_LV_APN);

   // Tax Year
   memcpy(pItemsRec->TaxYear, pInRec->TaxYear, SIZ_LV_YEAR);

   // BillNum
   sprintf(pItemsRec->BillNum, "%.4s%.*s", pInRec->TaxYear, SIZ_LV_BILLNUM, pInRec->BillNum);

   // Tax Desc
   if (pInRec->Spec.RecD.LevyDesc[0] > ' ')
   {
      strcpy(pItemsRec->TaxDesc, myTrim(pInRec->Spec.RecD.LevyDesc, SIZ_LV_LEVYDESC));
      strcpy(pAgencyRec->Agency, pItemsRec->TaxDesc);

      pResult = BSrchTaxDistName(pItemsRec->TaxDesc);
      if (pResult)
      {
         strcpy(pAgencyRec->Code, pResult->Code);
         strcpy(pAgencyRec->Phone, pResult->Phone);
         strcpy(pItemsRec->TaxCode, pResult->Code);
         pAgencyRec->TC_Flag[0] = pResult->TC_Flag[0];
         pItemsRec->TC_Flag[0] = pResult->TC_Flag[0];
      } else
      {
         LogMsg("+++ Unknown Agency-D: %.*s|%s", SIZ_LV_FUND, pInRec->Spec.RecD.Fund, pItemsRec->TaxDesc);
         memcpy(pItemsRec->TaxCode, pInRec->Spec.RecD.Fund, SIZ_LV_FUND);
         memcpy(pAgencyRec->Code, pInRec->Spec.RecD.Fund, SIZ_LV_FUND);
      }
   } 

   // Tax Amount
   dTmp = atoin(pInRec->Spec.RecD.TaxAmt, SIZ_LV_TAXAMT) / 100.0;
   if (dTmp > 0.0)
   {
      sprintf(pItemsRec->TaxAmt, "%.2f", dTmp);
      return 0;
   } else
      return 3;
}

// Special Levy - Prorated Esc tax, TAX CORR, ESCAPE ASMT TAX
int Ccx_ParseTaxLevyE(char *pItems, char *pAgency, char *pInbuf)
{
   TAXDETAIL  *pItemsRec  = (TAXDETAIL *)pItems;
   TAXAGENCY  *pAgencyRec = (TAXAGENCY *)pAgency;
   TAXAGENCY  *pResult;
   LEVYREC    *pInRec     = (LEVYREC *)  pInbuf;
   double      dTmp;

   // Clear output buffer
   memset(pItems, 0, sizeof(TAXDETAIL));
   memset(pAgency, 0, sizeof(TAXAGENCY));

   // APN
   memcpy(pItemsRec->Apn, pInRec->Apn, SIZ_LV_APN);

   // Tax Year
   memcpy(pItemsRec->TaxYear, pInRec->TaxYear, SIZ_LV_YEAR);

   // BillNum
   sprintf(pItemsRec->BillNum, "%.4s%.*s", pInRec->TaxYear, SIZ_LV_BILLNUM, pInRec->BillNum);

   // Tax Desc
   if (pInRec->Spec.RecE.LevyDesc[0] > ' ')
   {
      strcpy(pItemsRec->TaxDesc, myTrim(pInRec->Spec.RecE.LevyDesc, SIZ_LV_LEVYDESC));
      strcpy(pAgencyRec->Agency, pItemsRec->TaxDesc);

      pResult = BSrchTaxDistName(pItemsRec->TaxDesc);
      if (pResult)
      {
         strcpy(pAgencyRec->Code, pResult->Code);
         strcpy(pAgencyRec->Phone, pResult->Phone);
         strcpy(pItemsRec->TaxCode, pResult->Code);
         pAgencyRec->TC_Flag[0] = pResult->TC_Flag[0];
         pItemsRec->TC_Flag[0] = pResult->TC_Flag[0];
      } else
      {
         LogMsg("+++ Unknown Agency-E: %.*s|%s", SIZ_LV_FUND, pInRec->Spec.RecE.Fund, pItemsRec->TaxDesc);
         memcpy(pItemsRec->TaxCode, pInRec->Spec.RecE.Fund, SIZ_LV_FUND);
         memcpy(pAgencyRec->Code, pInRec->Spec.RecE.Fund, SIZ_LV_FUND);
      }
   } 

   // Tax Amount
   dTmp = atoin(pInRec->Spec.RecE.TaxAmt, SIZ_LV_TAXAMT) / 100.0;
   if (dTmp > 0.0)
   {
      sprintf(pItemsRec->TaxAmt, "%.2f", dTmp);
      return 0;
   } else
      return 3;
}
int Ccx_ParseTaxLevyF(char *pItems, char *pAgency, char *pInbuf)
{
   TAXDETAIL  *pItemsRec  = (TAXDETAIL *)pItems;
   TAXAGENCY  *pAgencyRec = (TAXAGENCY *)pAgency;
   TAXAGENCY  *pResult;
   LEVYREC    *pInRec     = (LEVYREC *)  pInbuf;
   double      dTmp;

   // Clear output buffer
   memset(pItems, 0, sizeof(TAXDETAIL));
   memset(pAgency, 0, sizeof(TAXAGENCY));

   // APN
   memcpy(pItemsRec->Apn, pInRec->Apn, SIZ_LV_APN);

   // Tax Year
   memcpy(pItemsRec->TaxYear, pInRec->TaxYear, SIZ_LV_YEAR);

   // BillNum
   sprintf(pItemsRec->BillNum, "%.4s%.*s", pInRec->TaxYear, SIZ_LV_BILLNUM, pInRec->BillNum);

   // Tax Desc
   if (pInRec->Spec.RecF.LevyDesc[0] > ' ')
   {
      strcpy(pItemsRec->TaxDesc, myTrim(pInRec->Spec.RecF.LevyDesc, SIZ_LV_LEVYDESC));
      strcpy(pAgencyRec->Agency, pItemsRec->TaxDesc);

      pResult = BSrchTaxDistName(pItemsRec->TaxDesc);
      if (pResult)
      {
         strcpy(pAgencyRec->Code, pResult->Code);
         strcpy(pAgencyRec->Phone, pResult->Phone);
         strcpy(pItemsRec->TaxCode, pResult->Code);
         pAgencyRec->TC_Flag[0] = pResult->TC_Flag[0];
         pItemsRec->TC_Flag[0] = pResult->TC_Flag[0];
      } else
      {
         LogMsg("+++ Unknown Agency-F: %.*s|%s", SIZ_LV_FUND, pInRec->Spec.RecF.Fund, pItemsRec->TaxDesc);
         memcpy(pItemsRec->TaxCode, pInRec->Spec.RecF.Fund, SIZ_LV_FUND);
         memcpy(pAgencyRec->Code, pInRec->Spec.RecF.Fund, SIZ_LV_FUND);
      }
   } 

   // Tax Amount
   dTmp = atoin(pInRec->Spec.RecF.TaxAmt, SIZ_LV_TAXAMT) / 100.0;
   if (dTmp > 0.0)
   {
      sprintf(pItemsRec->TaxAmt, "%.2f", dTmp);
      return 0;
   } else
      return 3;
}

int Ccx_ParseTaxLevyG(char *pItems, char *pAgency, char *pInbuf)
{
   return 2;
}

int Ccx_ParseTaxLevyZ(char *pItems, char *pAgency, char *pInbuf)
{
   return 2;
}
/**************************** Ccx_Load_TaxDetail ******************************
 *
 * Create base import file and import into SQL if specified
 * Input:  ccc.pts.levyfile.$YYMMDD
 * Output: CCX_Items.csv, CCX_Agency.csv
 *
 * Return 0 if success.
 *
 *****************************************************************************/

int Ccx_Load_TaxDetail(bool bImport)
{
   char     *pTmp, acItemsRec[MAX_RECSIZE], acAgencyRec[512], acRec[512];
   char     acItemsFile[_MAX_PATH], acAgencyFile[_MAX_PATH], acTmpFile[_MAX_PATH], acInFile[_MAX_PATH];

   int      iRet;
   long     lItems=0, lCnt=0, lAgency=0;
   FILE     *fdItems, *fdAgency, *fdIn;
   TAXDETAIL *pTax   = (TAXDETAIL *)&acItemsRec[0];
   LEVYREC   *pInRec = (LEVYREC *)&acRec[0];

   LogMsg0("Loading Detail file");

   GetIniString(myCounty.acCntyCode, "LevyFile", "", acInFile, _MAX_PATH, acIniFile);
   if (_access(acInFile, 0))
   {
      LogMsg("*** Detail file is not available, skip it");
      return 0;
   }
   NameTaxCsvFile(acItemsFile, myCounty.acCntyCode, "Items");
   NameTaxCsvFile(acAgencyFile, myCounty.acCntyCode, "Agency");
   sprintf(acTmpFile, "%s\\%s\\%s_Agency.tmp", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);

   // Open input file
   LogMsg("Open Detail file %s", acInFile);
   fdIn = fopen(acInFile, "r");
   if (fdIn == NULL)
   {
      LogMsg("***** Error opening Detail file: %s\n", acInFile);
      return -2;
   }  

   // Open Items file
   LogMsg("Create Items file %s", acItemsFile);
   fdItems = fopen(acItemsFile, "w");
   if (fdItems == NULL)
   {
      LogMsg("***** Error creating Items file: %s\n", acItemsFile);
      return -4;
   }

   // Open Agency file
   LogMsg("Create Agency file %s", acTmpFile);
   fdAgency = fopen(acTmpFile, "w");
   if (fdAgency == NULL)
   {
      LogMsg("***** Error creating Agency file: %s\n", acTmpFile);
      return -4;
   }

   // Init variables

   // Merge loop 
   while (!feof(fdIn))
   {
      pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdIn);
      if (!pTmp)
         break;

      // Create Items & Agency record
      switch (pInRec->RecType)
      {
         case 'A':
            iRet = sizeof(LEVY_A);
            iRet = Ccx_ParseTaxLevyA(acItemsRec, acAgencyRec, acRec);
            break;
         case 'B':
            iRet = sizeof(LEVY_B);
            iRet = Ccx_ParseTaxLevyB(acItemsRec, acAgencyRec, acRec);
            break;
         case 'C':
            iRet = sizeof(LEVY_C);
            iRet = Ccx_ParseTaxLevyC(acItemsRec, acAgencyRec, acRec);
            break;
         case 'D':
            iRet = sizeof(LEVY_D);
            iRet = Ccx_ParseTaxLevyD(acItemsRec, acAgencyRec, acRec);
            break;
         case 'E':
            iRet = sizeof(LEVY_E);
            iRet = Ccx_ParseTaxLevyE(acItemsRec, acAgencyRec, acRec);
            break;
         case 'F':
            iRet = sizeof(LEVY_F);
            iRet = Ccx_ParseTaxLevyF(acItemsRec, acAgencyRec, acRec);
            break;
         case 'G':
            iRet = sizeof(LEVY_G);
            iRet = Ccx_ParseTaxLevyG(acItemsRec, acAgencyRec, acRec);
            break;
         case 'Z':
            iRet = sizeof(LEVY_Z);
            iRet = Ccx_ParseTaxLevyZ(acItemsRec, acAgencyRec, acRec);
            break;
         default:
            iRet = 1;
            break;
      }

      if (!iRet)
      {
         // Create TaxBase record
         Tax_CreateDetailCsv(acRec, (TAXDETAIL *)&acItemsRec);
         lItems++;
         fputs(acRec, fdItems);

         // Create Agency record
         Tax_CreateAgencyCsv(acRec, (TAXAGENCY *)&acAgencyRec);
#ifdef _DEBUG
         //if (acRec[0] < '0')
         //   iRet = 0;
#endif
         fputs(acRec, fdAgency);
      } else if (iRet == 1)
      {
         LogMsg0("---> Drop detail record type %c %d [%.80s]", pInRec->RecType, lCnt, acRec); 
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdIn)
      fclose(fdIn);
   if (fdItems)
      fclose(fdItems);
   if (fdAgency)
      fclose(fdAgency);

   // Dedup Agency file before import
   LogMsg("Dedup Agency file %s to %s", acTmpFile, acAgencyFile);
   iRet = sortFile(acTmpFile, acAgencyFile, "S(#1,C,A,#2,C,A) DEL(124) DUPOUT F(TXT) OMIT(1,1,C,EQ,\"|\")");

   LogMsg("Total records processed:    %u", lCnt);
   LogMsg("Total Items records:        %u", lItems);

   // Import into SQL
   if (bImport)
   {
      iRet = doTaxImport(myCounty.acCntyCode, TAX_DETAIL);
      if (!iRet)
      {
         // Populate TotalRate - EXEC spUpdateTotalRate 'EDX'
         iRet = doUpdateTotalRate(myCounty.acCntyCode);

         iRet = doTaxImport(myCounty.acCntyCode, TAX_AGENCY);
      }
   } else
      iRet = 0;

   return iRet;
}

/***************************** Ccx_ParseTaxBase ******************************
 *
 * Parse CORTAC record into TAXBASE
 *
 * CCX doesn't provide penalty amt.  If paid amt > tax amt, difference is penalty.
 *
 *****************************************************************************/

int Ccx_ParseTaxBase(char *pBase, char *pInbuf)
{
   TAXBASE  *pBaseRec = (TAXBASE *)pBase;
   CORTAC   *pInRec   = (CORTAC  *)pInbuf;
   double   dTotalTax, dTotalDue, dTax1, dTax2;

   // Clear output buffer
   memset(pBase, 0, sizeof(TAXBASE));

   // APN
   memcpy(pBaseRec->Apn, pInRec->Apn, SIZ_CT_APN);

#ifdef _DEBUG
   //int      iRet;
   //if (!memcmp(pBaseRec->Apn, "00405003", 8))
   //   iRet = 0;
#endif

   // TRA

   // Tax Year
   sprintf(pBaseRec->TaxYear, "%d", lTaxYear);

   // Bill Number
   memcpy(pBaseRec->BillNum, pInRec->BillNum, SIZ_CT_BILLNUM);

   // Check for Tax amount
   dTax1 = atof(pInRec->InstAmt_1);
   dTax2 = atof(pInRec->InstAmt_2);

   dTotalTax = dTax1+dTax2;
   dTotalDue = 0.0;

   InstDueDate(pBaseRec->DueDate1, 1, lTaxYear);
   InstDueDate(pBaseRec->DueDate2, 2, lTaxYear);

   if (dTotalTax > 0.0)
   {
      sprintf(pBaseRec->TotalTaxAmt, "%.2f", dTotalTax);
      if (dTax1 > 0.0)
         sprintf(pBaseRec->TaxAmt1, "%.2f", dTax1);
      if (dTax2 > 0.0)
         sprintf(pBaseRec->TaxAmt2, "%.2f", dTax2);
   }

   // Paid
   if (pInRec->PaidFlg_1 == 'P')
   {
      sprintf(pBaseRec->PaidAmt1, "%.2f", dTax1);
      pBaseRec->Inst1Status[0] = TAX_BSTAT_PAID;
   } else
   {
      if (dTax1 == 0.0)
         pBaseRec->Inst1Status[0] = TAX_BSTAT_NOTAX;
      else
      {
         pBaseRec->Inst1Status[0] = TAX_BSTAT_UNPAID;
         dTotalDue = dTax1;
      }
   }

   if (pInRec->PaidFlg_2 == 'P')
   {
      sprintf(pBaseRec->PaidAmt2, "%.2f", dTax2);
      pBaseRec->Inst2Status[0] = TAX_BSTAT_PAID;
   } else
   {
      if (dTax2 == 0.0)
         pBaseRec->Inst2Status[0] = TAX_BSTAT_NOTAX;
      else
      {
         pBaseRec->Inst2Status[0] = TAX_BSTAT_UNPAID;
         dTotalDue += dTax2;
      }
   }

   // Total due
   if (dTotalDue > 0)
   {
      sprintf(pBaseRec->TotalDue, "%.2f", dTotalDue);
   }

   // Tax rate

   // Default

   pBaseRec->BillType[0] = BILLTYPE_SECURED;
   pBaseRec->isSecd[0] = '1';
   pBaseRec->isSupp[0] = '0';

   return 0;
}

/****************************** Ccx_UpdatePst *********************************
 *
 * Use data from PST file to update TAXBASE record
 *
 ******************************************************************************/

int Ccx_UpdatePst(char *pOutbuf, FILE *fd)
{
   static   char  acRec[2048], *pRec=NULL;
   int      iRet, iLoop;
   bool     bPaidSp;
   double   dTaxAmt1, dTaxAmt2, dPaidAmt1, dPaidAmt2, dTotalDue, dTotalTax;

   TAXBASE  *pTaxBase = (TAXBASE *)pOutbuf;

   // Get first Char rec for first call
   if (!pRec)
   {
      // Drop header
      pRec = fgets(acRec, 2048, fd);

      // Get first rec
      pRec = fgets(acRec, 2048, fd);
      iRet = ParseStringNQ(acRec, ',', 64, apTokens);
      if (iRet < PTS_BILL_SUFFIX)
      {
         LogMsg("***** UpdatePst -->Bad tax record at 1 (%d)", iRet);
         return -1;
      }

      // Remove hyphen from APN
      remChar(apTokens[PTS_APN], '-');

      lPstMatch = 0;
   }

   do
   {
      // Compare Apn
      iLoop = strcmp(pTaxBase->Apn, apTokens[PTS_APN]);
      if (iLoop > 0)
      {
         pRec = fgets(acRec, 2048, fd);
         if (!pRec)
         {
            fclose(fd);
            fd = NULL;
            break;
         }

         iRet = ParseStringNQ(acRec, ',', 64, apTokens);
         if (iRet < PTS_BILL_SUFFIX)
         {
            LogMsg("***** UpdatePst -->Bad tax record at 1 (%d)", iRet);
            return -1;
         }

         // Remove hyphen from APN
         remChar(apTokens[PTS_APN], '-');
      }
   } while (iLoop > 0);

   // If not match, return
   if (iLoop)
      return 1;

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "0884510314", 10))
   //   iRet = 0;
#endif

   lPstMatch++;

   dTotalDue=dTaxAmt1=dTaxAmt2=dPaidAmt1=dPaidAmt2 = 0.0;
   bPaidSp = false;
   if (!memcmp(apTokens[PTS_PAID_STATUS], "PA", 2))
   {
      pTaxBase->Inst1Status[0] = TAX_BSTAT_PAID;
      dPaidAmt1 = atof(apTokens[PTS_TAX_AMT]);

      // If paid date not available, use due date
      if (isdigit(*apTokens[PTS_PAID_DATE]))
         strcpy(pTaxBase->PaidDate1, apTokens[PTS_PAID_DATE]);
      else if (!memcmp(apTokens[PTS_PAID_DATE], "PAIDSP", 6))
      {
         dateConversion(apTokens[PTS_DUE_DATE], pTaxBase->PaidDate1, MM_DD_YYYY_1);
         bPaidSp = true;
      } else
         LogMsg("*** Invalid date %s [%s]", apTokens[PTS_PAID_DATE], apTokens[PTS_APN]);
   } else if (!memcmp(apTokens[PTS_PAID_STATUS], "PEND", 2))
   {
      pTaxBase->Inst1Status[0] = TAX_BSTAT_PENDING;
      dTaxAmt1 = atof(apTokens[PTS_TAX_AMT]);
   } else if (!memcmp(apTokens[PTS_PAID_STATUS], "NOT PAID", 6))
   {
      pTaxBase->Inst1Status[0] = TAX_BSTAT_UNPAID;
      dTaxAmt1 = atof(apTokens[PTS_TAX_AMT]);
   } else
      LogMsg("*** Unknown paid status: %s [%s]", apTokens[PTS_PAID_STATUS], apTokens[PTS_APN]);

   // Get 2nd bill
   pRec = fgets(acRec, 2048, fd);
   if (pRec)
   {
      iRet = ParseStringNQ(acRec, ',', 64, apTokens);
      if (iRet < PTS_BILL_SUFFIX)
      {
         LogMsg("***** UpdatePst -->Bad tax record at 1 (%d)", iRet);
         return -1;
      }

      if (*apTokens[PTS_INST_NBR] == '2')
      {
         if (!memcmp(apTokens[PTS_PAID_STATUS], "PA", 2))
         {
            pTaxBase->Inst2Status[0] = TAX_BSTAT_PAID;
            dPaidAmt2 = atof(apTokens[PTS_TAX_AMT]);

            // If paid date not available, use due date
            if (isdigit(*apTokens[PTS_PAID_DATE]))
               strcpy(pTaxBase->PaidDate2, apTokens[PTS_PAID_DATE]);
            else
               dateConversion(apTokens[PTS_DUE_DATE], pTaxBase->PaidDate2, MM_DD_YYYY_1);
         } else if (!memcmp(apTokens[PTS_PAID_STATUS], "PEND", 2))
         {
            pTaxBase->Inst2Status[0] = TAX_BSTAT_PENDING;
            dTaxAmt2 = atof(apTokens[PTS_TAX_AMT]);
         } else if (!memcmp(apTokens[PTS_PAID_STATUS], "NOT PAID", 6))
         {
            pTaxBase->Inst2Status[0] = TAX_BSTAT_UNPAID;
            dTaxAmt2 = atof(apTokens[PTS_TAX_AMT]);
         } else
            LogMsg("*** Unknown paid status: %s [%s]", apTokens[PTS_PAID_STATUS], apTokens[PTS_APN]);
      } else
         LogMsg("*** Installment out of sync APN=%s", apTokens[PTS_APN]);
   }

   // Update amount
   dTotalTax = atof(pTaxBase->TotalTaxAmt);

   // If inst1 includes supplemental, then due amt is tax amt2
   if (bPaidSp && !dPaidAmt2)
      dTotalDue = dTaxAmt2;
   else
   {
      dTotalDue = dTotalTax - (dPaidAmt1+dPaidAmt2);
      if (dTotalDue < 0) dTotalDue = 0;
   }

   if (dPaidAmt1 > 0.0)
      sprintf(pTaxBase->PaidAmt1, "%.2f", dPaidAmt1);
   if (dPaidAmt2 > 0.0)
   {
      sprintf(pTaxBase->PaidAmt2, "%.2f", dPaidAmt2);
      dTotalDue = 0;
   }
   sprintf(pTaxBase->TotalDue, "%.2f", dTotalDue);

   // Get next bill
   if (!feof(fd))
   {
      pRec = fgets(acRec, 2048, fd);
      if (pRec)
      {
         iRet = ParseStringNQ(acRec, ',', 64, apTokens);
         if (iRet < PTS_BILL_SUFFIX)
         {
            LogMsg("***** UpdatePst -->Bad tax record at 1 (%d)", iRet);
            return -1;
         }

         // Remove hyphen from APN
         remChar(apTokens[PTS_APN], '-');
      }
   }

   if (feof(fd))
   {
      fclose(fd);
      fd = NULL;
   }

   return 0;
}

/************************* Ccx_Load_TaxBase_Cortac ****************************
 *
 * Loading Tax Base using Cortac file
 *
 * Create import file for Tax_Base table
 *
 * Return 0 if success.
 *
 *****************************************************************************/

int Ccx_Load_TaxBase_Cortac(bool bImport)
{
   char     *pTmp, acBuf[MAX_RECSIZE], acRec[MAX_RECSIZE], acBaseFile[_MAX_PATH], acCortacFile[_MAX_PATH],
            acPtsFile[_MAX_PATH];
   int      iRet;
   long     lOut=0, lCnt=0;
   FILE     *fdBase, *fdSecRoll, *fdR01, *fdPst;
   TAXBASE  *pTaxBase = (TAXBASE *)&acBuf[0];

   LogMsg("Loading Current tax file");

   GetIniString(myCounty.acCntyCode, "CortacFile", "", acCortacFile, _MAX_PATH, acIniFile);
   //lLastTaxFileDate = getFileDate(acCortacFile);
   //// Only process if new tax file
   //iRet = isNewTaxFile(NULL, myCounty.acCntyCode);
   //if (iRet <= 0)
   //{
   //   lLastTaxFileDate = 0;
   //   return iRet;
   //}
   lLastTaxFileDate = lProcDate;

   // Prepare output files
   NameTaxCsvFile(acBaseFile, myCounty.acCntyCode, "Base");

   pTmp = strrchr(acBaseFile, '\\');
   *pTmp = 0;
   if (_access(acBaseFile, 0))
      _mkdir(acBaseFile);
   *pTmp = '\\';

   // Open input file
   LogMsg("Open Cortac file %s", acCortacFile);
   fdSecRoll = fopen(acCortacFile, "r");
   if (fdSecRoll == NULL)
   {
      LogMsg("***** Error opening Cortac file: %s\n", acCortacFile);
      return -2;
   }  

   // Open base file
   LogMsg("Open Base file %s", acBaseFile);
   fdBase = fopen(acBaseFile, "w");
   if (fdBase == NULL)
   {
      LogMsg("***** Error creating base file: %s\n", acBaseFile);
      return -4;
   }

   // Open input
   GetIniString(myCounty.acCntyCode, "PTSDaily", "", acBuf, _MAX_PATH, acIniFile);
   sprintf(acPtsFile, acBuf, lProcDate);
   if (!_access(acPtsFile, 0))
   {
      LogMsg("Open PTS daily file %s", acPtsFile);
      fdPst = fopen(acPtsFile, "r");
   } else
      fdPst = NULL;

   // Open raw file to update TRA
   fdR01 = OpenR01(myCounty.acCntyCode);

   // Init variables

   // Merge loop 
   while (!feof(fdSecRoll))
   {
      pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdSecRoll);
      if (!pTmp)
         break;

      // Create TaxBase record
      iRet = Ccx_ParseTaxBase(acBuf, acRec);
      if (!iRet)
      {
         // Update daily file
         if (fdPst) // && pTaxBase->Inst2Status[0] != TAX_BSTAT_PAID)
            Ccx_UpdatePst(acBuf, fdPst);

         // Update TRA
         if (fdR01)
            UpdateTRA(acBuf, fdR01);

         // Create TaxBase record
         Tax_CreateTaxBaseCsv(acRec, (TAXBASE *)&acBuf);
         lOut++;
         fputs(acRec, fdBase);
      } else
      {
         if (iRet == 1)
            LogMsg("---> Drop cancel record %d [%.40s]", lCnt, acRec); 
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdSecRoll)
      fclose(fdSecRoll);
   if (fdBase)
      fclose(fdBase);
   if (fdR01)
      fclose(fdR01);
   if (fdPst)
      fclose(fdPst);

   LogMsg("Total records processed:    %u", lCnt);
   LogMsg("Total output records:       %u", lOut);
   LogMsg("Total matched records:      %u", lPstMatch);

   // Import into SQL 
   if (bImport)
   {
      iRet = doTaxImport(myCounty.acCntyCode, TAX_BASE);
   } else
      iRet = 0;

   return iRet;
}

/****************************** Ccx_ParsePTS **********************************
 *
 * Use data from PST file to update TAXBASE record
 *
 ******************************************************************************/

int Ccx_ParsePTS(char *pOutbuf, char *pRec1, char *pRec2)
{
   char     *apItem1[32], *apItem2[32], acTmp[32], *pTmp;
   int      iTem1, iTem2;
   bool     bPaidSp;
   double   dTaxAmt1, dTaxAmt2, dPaidAmt1, dPaidAmt2, dTotalDue, dTotalTax;

   TAXBASE  *pTaxBase = (TAXBASE *)pOutbuf;

   // Get first Char rec for first call
   iTem1 = ParseStringNQ(pRec1, ',', 32, apItem1);
   if (iTem1 < PTS_BILL_SUFFIX)
   {
      LogMsg("***** ParsePTS -->Bad tax record 1: %s (%d)", pRec1, iTem1);
      return -1;
   }
   iTem2 = ParseStringNQ(pRec2, ',', 32, apItem2);
   if (iTem2 < PTS_BILL_SUFFIX)
   {
      LogMsg("***** ParsePTS -->Bad tax record 2: %s (%d)", pRec2, iTem2);
      return -1;
   }

   if  (strcmp(apItem1[PTS_APN], apItem2[PTS_APN]))
   {
      LogMsg("***** Mismatch APN: %s <> %s", apItem1[PTS_APN], apItem2[PTS_APN]);
      return 1;
   }

   // Init buffer
   memset(pOutbuf, 0, sizeof(TAXBASE));

   // Remove hyphen from APN
   remChar(apItem1[PTS_APN], '-');
   strcpy(pTaxBase->Apn, apItem1[PTS_APN]);

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "0884510314", 10))
   //   iRet = 0;
#endif

   dTotalDue=dPaidAmt1=dPaidAmt2 = 0.0;

   // Tax Year
   sprintf(acTmp, "20%.2s", apItem1[PTS_TAX_YEAR]);
   iTaxYear = atol(acTmp);
   if (iTaxYear != lTaxYear)
   {
      LogMsg("*** Drop rec due to bad tax year: APN=%s, TaxYear=%s", pTaxBase->Apn, acTmp);
      return iTaxYear;
   }
   strcpy(pTaxBase->TaxYear, acTmp);

   // Installment 1
   if (*apItem1[PTS_INST_NBR] == '1')
   {

      // BillNum
      strcpy(pTaxBase->BillNum, apItem1[PTS_BILL_NO]);

      // Tax Amt
      strcpy(pTaxBase->TaxAmt1, apItem1[PTS_TAX_AMT]);
      dTaxAmt1 = atof(apItem1[PTS_TAX_AMT]);

      // Due date
      pTmp = dateConversion(apItem1[PTS_DUE_DATE], pTaxBase->DueDate1, MM_DD_YYYY_1);
      if (!pTmp)
         LogMsg("*** Invalid due date: APN=%s, DueDate1=%s", pTaxBase->Apn, pTaxBase->DueDate1);

      if (!memcmp(apItem1[PTS_PAID_STATUS], "PA", 2))
      {
         pTaxBase->Inst1Status[0] = TAX_BSTAT_PAID;
         dPaidAmt1 = dTaxAmt1;

         // If paid date not available, use due date
         if (isdigit(*apItem1[PTS_PAID_DATE]))
            strcpy(pTaxBase->PaidDate1, apItem1[PTS_PAID_DATE]);
         else if (memcmp(acToday, pTaxBase->DueDate1, 8) < 0)
            strcpy(pTaxBase->PaidDate1, acToday);
         else
            strcpy(pTaxBase->PaidDate1, pTaxBase->DueDate1);
      } else if (!memcmp(apItem1[PTS_PAID_STATUS], "PEND", 2))
      {
         pTaxBase->Inst1Status[0] = TAX_BSTAT_PENDING;
         dPaidAmt1 = dTaxAmt1;
      } else if (!memcmp(apItem1[PTS_PAID_STATUS], "NOT PAID", 6))
         pTaxBase->Inst1Status[0] = TAX_BSTAT_UNPAID;
      else
         LogMsg("*** Unknown paid status: %s [%s]", apItem1[PTS_PAID_STATUS], apItem1[PTS_APN]);
   } else
      LogMsg("*** Installment out of sync APN=%s", apItem1[PTS_APN]);

   bPaidSp = false;
   // Installment 2
   dTaxAmt2 = atof(apItem2[PTS_TAX_AMT]);
   strcpy(pTaxBase->TaxAmt2, apItem2[PTS_TAX_AMT]);
   pTmp = dateConversion(apItem2[PTS_DUE_DATE], pTaxBase->DueDate2, MM_DD_YYYY_1, iTaxYear+1);
   if (!pTmp)
      LogMsg("*** Invalid due date: APN=%s, DueDate2=%s", pTaxBase->Apn, pTaxBase->DueDate2);

   if (!memcmp(apItem2[PTS_PAID_STATUS], "PA", 2))
   {
      pTaxBase->Inst2Status[0] = TAX_BSTAT_PAID;
      dPaidAmt2 = dTaxAmt2;

      // If paid date not available, use due date
      if (isdigit(*apItem2[PTS_PAID_DATE]))
         strcpy(pTaxBase->PaidDate2, apItem2[PTS_PAID_DATE]);
      else if (!memcmp(apItem2[PTS_PAID_DATE], "PAIDSP", 6))
      {
         if (memcmp(acToday, pTaxBase->DueDate2, 8) < 0)
            strcpy(pTaxBase->PaidDate1, acToday);
         else
            strcpy(pTaxBase->PaidDate2, pTaxBase->DueDate2);
         bPaidSp = true;
      } else
         LogMsg("*** Invalid paid date: APN=%s PaidDate2=%s", apItem2[PTS_APN], apItem2[PTS_PAID_DATE]);
   } else if (!memcmp(apItem2[PTS_PAID_STATUS], "PEND", 2))
      pTaxBase->Inst2Status[0] = TAX_BSTAT_PENDING;
   else if (!memcmp(apItem2[PTS_PAID_STATUS], "NOT PAID", 6))
      pTaxBase->Inst2Status[0] = TAX_BSTAT_UNPAID;
   else
      LogMsg("*** Unknown paid status: %s [%s]", apItem2[PTS_PAID_STATUS], apItem2[PTS_APN]);

   // Update amount
   dTotalTax = dTaxAmt1+dTaxAmt2;
   sprintf(pTaxBase->TotalTaxAmt, "%.2f", dTotalTax);

   // If inst1 includes supplemental, then due amt is tax amt2
   if (bPaidSp && !dPaidAmt2)
      dTotalDue = dTaxAmt2;
   else
   {
      dTotalDue = dTotalTax - (dPaidAmt1+dPaidAmt2);
      if (dTotalDue < 0) dTotalDue = 0;
   }

   if (dPaidAmt1 > 0.0)
      sprintf(pTaxBase->PaidAmt1, "%.2f", dPaidAmt1);
   if (dPaidAmt2 > 0.0)
   {
      sprintf(pTaxBase->PaidAmt2, "%.2f", dPaidAmt2);
      dTotalDue = 0;
   }
   sprintf(pTaxBase->TotalDue, "%.2f", dTotalDue);

   pTaxBase->BillType[0] = BILLTYPE_SECURED;
   pTaxBase->isSecd[0] = '1';
   pTaxBase->isSupp[0] = '0';

   lPstMatch++;

   return 0;
}

/**************************** Ccx_Load_TaxBase_PTS ****************************
 *
 * Loading Tax Base using daily PTS file
 *
 * Create import file for Tax_Base, Tax_Items, & Tax_Agency tables
 *    - Header may include first record.  Make sure check for it on first row.
 *
 * Return 0 if success.
 *
 *****************************************************************************/

int Ccx_Load_TaxBase_PTS(bool bImport)
{
   char     *pRec1, *pRec2, acBaseFile[_MAX_PATH], acPtsFile[_MAX_PATH],
            acBuf[MAX_RECSIZE], acRec1[MAX_RECSIZE], acRec2[MAX_RECSIZE];
   int      iRet;
   long     lOut=0, lCnt=0;
   FILE     *fdBase, *fdR01, *fdPst;
   TAXBASE  *pTaxBase = (TAXBASE *)&acBuf[0];

   LogMsg0("Loading Current tax file");

   lLastTaxFileDate = lProcDate;
   pRec2 = pRec1 = NULL;

   // Open input
   GetIniString(myCounty.acCntyCode, "PTSDaily", "", acBuf, _MAX_PATH, acIniFile);
   sprintf(acPtsFile, acBuf, lProcDate);
   if (!_access(acPtsFile, 0))
   {
      LogMsg("Open PTS daily file %s", acPtsFile);
      fdPst = fopen(acPtsFile, "r");
      // Drop header
      if (fdPst)
         fgets((char *)&acRec1[0], MAX_RECSIZE, fdPst);
      else
         return -1;

      // Check for first record
      pRec1 = strstr(acRec1, "PTS");
      if (pRec1) pRec1--;
   } else
   {
      LogMsg("***** ERROR: Missing daily input file: %s\n", acPtsFile);
      return -2;
   }  

   // Open base output file
   NameTaxCsvFile(acBaseFile, myCounty.acCntyCode, "Base");
   LogMsg("Open Base file %s", acBaseFile);
   fdBase = fopen(acBaseFile, "w");
   if (fdBase == NULL)
   {
      LogMsg("***** Error creating base file: %s\n", acBaseFile);
      return -4;
   }

   // Open raw file to update TRA
   fdR01 = OpenR01(myCounty.acCntyCode);

   // Merge loop 
   while (!feof(fdPst))
   {
      if (!pRec1)
         if (!(pRec1 = fgets((char *)&acRec1[0], MAX_RECSIZE, fdPst)))
            break;
      if (!(pRec2 = fgets((char *)&acRec2[0], MAX_RECSIZE, fdPst)))
         break;

      // Create TaxBase record
      iRet = Ccx_ParsePTS(acBuf, pRec1, pRec2);
      if (!iRet)
      {
         // Update TRA
         if (fdR01)
            UpdateTRA(acBuf, fdR01);

         // Create TaxBase record
         Tax_CreateTaxBaseCsv(acRec1, (TAXBASE *)&acBuf);
         lOut++;
         fputs(acRec1, fdBase);
      } else
      {
         if (iRet == 1)
            LogMsg("---> Drop cancel record %d [%.40s]", lCnt, acRec1); 
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
      pRec1 = NULL;
   }

   // Close files
   if (fdBase)
      fclose(fdBase);
   if (fdR01)
      fclose(fdR01);
   if (fdPst)
      fclose(fdPst);

   LogMsg("Total records processed:    %u", lCnt);
   LogMsg("Total output records:       %u", lOut);
   LogMsg("Total matched records:      %u", lPstMatch);

   // Import into SQL 
   if (bImport)
   {
      iRet = doTaxImport(myCounty.acCntyCode, TAX_BASE);
   } else
      iRet = 0;

   return iRet;
}

/****************************** Ccx_ParseSPT **********************************
 *
 * Use data from SPT file to update TAXBASE record
 *
 ******************************************************************************/

int Ccx_ParseSPT(char *pOutbuf, char *pRec1, char *pRec2)
{
   char     *apItem1[32], *apItem2[32], acTmp[32], *pTmp;
   int      iTem1, iTem2;
   bool     bPaidSp;
   double   dTaxAmt1, dTaxAmt2, dPaidAmt1, dPaidAmt2, dTotalDue, dTotalTax;

   TAXBASE  *pTaxBase = (TAXBASE *)pOutbuf;

   // Get first Char rec for first call
   iTem1 = ParseStringNQ(pRec1, ',', 32, apItem1);
   if (iTem1 < PTS_BILL_SUFFIX)
   {
      LogMsg("***** ParseSPT -->Bad tax record 1: %s (%d)", pRec1, iTem1);
      return -1;
   }
   iTem2 = ParseStringNQ(pRec2, ',', 32, apItem2);
   if (iTem2 < PTS_BILL_SUFFIX)
   {
      LogMsg("***** ParseSPT -->Bad tax record 2: %s (%d)", pRec2, iTem2);
      return -1;
   }

   if  (strcmp(apItem1[PTS_APN], apItem2[PTS_APN]))
   {
      LogMsg("***** Mismatch APN: %s <> %s", apItem1[PTS_APN], apItem2[PTS_APN]);
      return 1;
   }

   // Init buffer
   memset(pOutbuf, 0, sizeof(TAXBASE));

   // Remove hyphen from APN
   remChar(apItem1[PTS_APN], '-');
   strcpy(pTaxBase->Apn, apItem1[PTS_APN]);

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "4073120026", 10))
   //   iTem1 = 0;
#endif

   dTotalDue=dPaidAmt1=dPaidAmt2 = 0.0;

   // Tax Year
   if (*apItem1[PTS_TAX_YEAR] >= '2')
   {
      sprintf(acTmp, "20%.2s", apItem1[PTS_TAX_YEAR]);
      iTaxYear = atol(acTmp);
      if (iTaxYear != lTaxYear)
      {
         LogMsg("*** Drop rec due to bad tax year: APN=%s, TaxYear=%s", pTaxBase->Apn, acTmp);
         return iTaxYear;
      }
   } else
      sprintf(acTmp, "%d", lTaxYear);
   strcpy(pTaxBase->TaxYear, acTmp);

   // Installment 1
   if (*apItem1[PTS_INST_NBR] == '3')
   {
      // BillNum
      strcpy(pTaxBase->BillNum, apItem1[PTS_BILL_NO]);

      // Tax Amt
      strcpy(pTaxBase->TaxAmt1, apItem1[PTS_TAX_AMT]);
      dTaxAmt1 = atof(apItem1[PTS_TAX_AMT]);

      // Due date
      pTmp = dateConversion(apItem1[PTS_DUE_DATE], pTaxBase->DueDate1, MM_DD_YYYY_1, iTaxYear);
      if (!pTmp)
         LogMsg("*** Invalid due date: APN=%s, DueDate1=%s", pTaxBase->Apn, pTaxBase->DueDate1);

      if (!memcmp(apItem1[PTS_PAID_STATUS], "PA", 2))
      {
         pTaxBase->Inst1Status[0] = TAX_BSTAT_PAID;
         dPaidAmt1 = dTaxAmt1;

         // If paid date not available, use due date
         if (isdigit(*apItem1[PTS_PAID_DATE]))
            strcpy(pTaxBase->PaidDate1, apItem1[PTS_PAID_DATE]);
         else if (memcmp(acToday, pTaxBase->DueDate1, 8) < 0)
            strcpy(pTaxBase->PaidDate1, acToday);
         else
            strcpy(pTaxBase->PaidDate1, pTaxBase->DueDate1);
      } else if (!memcmp(apItem1[PTS_PAID_STATUS], "PEND", 2))
      {
         pTaxBase->Inst1Status[0] = TAX_BSTAT_PENDING;
         dPaidAmt1 = dTaxAmt1;
      } else if (!memcmp(apItem1[PTS_PAID_STATUS], "NOT PAID", 6))
         pTaxBase->Inst1Status[0] = TAX_BSTAT_UNPAID;
      else
         LogMsg("*** Unknown paid status: %s [%s]", apItem1[PTS_PAID_STATUS], apItem1[PTS_APN]);
   } else
      LogMsg("*** Installment out of sync APN=%s", apItem1[PTS_APN]);

   bPaidSp = false;
   // Installment 2
   dTaxAmt2 = atof(apItem2[PTS_TAX_AMT]);
   strcpy(pTaxBase->TaxAmt2, apItem2[PTS_TAX_AMT]);
   pTmp = dateConversion(apItem2[PTS_DUE_DATE], pTaxBase->DueDate2, MM_DD_YYYY_1, iTaxYear+1);
   if (!pTmp)
      LogMsg("*** Invalid due date: APN=%s, DueDate2=%s", pTaxBase->Apn, pTaxBase->DueDate2);

   if (!memcmp(apItem2[PTS_PAID_STATUS], "PA", 2))
   {
      pTaxBase->Inst2Status[0] = TAX_BSTAT_PAID;
      dPaidAmt2 = dTaxAmt2;

      // If paid date not available, use due date
      if (isdigit(*apItem2[PTS_PAID_DATE]))
         strcpy(pTaxBase->PaidDate2, apItem2[PTS_PAID_DATE]);
      else if (!memcmp(apItem2[PTS_PAID_DATE], "PAIDSP", 6))
      {
         if (memcmp(acToday, pTaxBase->DueDate2, 8) < 0)
            strcpy(pTaxBase->PaidDate1, acToday);
         else
            strcpy(pTaxBase->PaidDate2, pTaxBase->DueDate2);
         bPaidSp = true;
      } else
         LogMsg("*** Invalid paid date: APN=%s PaidDate2=%s", apItem2[PTS_APN], apItem2[PTS_PAID_DATE]);
   } else if (!memcmp(apItem2[PTS_PAID_STATUS], "PEND", 2))
      pTaxBase->Inst2Status[0] = TAX_BSTAT_PENDING;
   else if (!memcmp(apItem2[PTS_PAID_STATUS], "NOT PAID", 6))
      pTaxBase->Inst2Status[0] = TAX_BSTAT_UNPAID;
   else
      LogMsg("*** Unknown paid status: %s [%s]", apItem2[PTS_PAID_STATUS], apItem2[PTS_APN]);

   // Update amount
   dTotalTax = dTaxAmt1+dTaxAmt2;
   sprintf(pTaxBase->TotalTaxAmt, "%.2f", dTotalTax);

   // If inst1 includes supplemental, then due amt is tax amt2
   if (bPaidSp && !dPaidAmt2)
      dTotalDue = dTaxAmt2;
   else
   {
      dTotalDue = dTotalTax - (dPaidAmt1+dPaidAmt2);
      if (dTotalDue < 0) dTotalDue = 0;
   }

   if (dPaidAmt1 > 0.0)
      sprintf(pTaxBase->PaidAmt1, "%.2f", dPaidAmt1);
   if (dPaidAmt2 > 0.0)
   {
      sprintf(pTaxBase->PaidAmt2, "%.2f", dPaidAmt2);
      dTotalDue = 0;
   }
   sprintf(pTaxBase->TotalDue, "%.2f", dTotalDue);

   pTaxBase->BillType[0] = BILLTYPE_SECURED_SUPPL;
   pTaxBase->isSecd[0] = '0';
   pTaxBase->isSupp[0] = '1';

   lPstMatch++;

   return 0;
}

/**************************** Ccx_Load_TaxSupp_SPT ****************************
 *
 * Loading Tax Supplement using daily SPT file
 *
 * Return 0 if success.
 *
 *****************************************************************************/

int Ccx_Load_TaxSupp_SPT(bool bImport)
{
   char     *pRec1, *pRec2, acBaseFile[_MAX_PATH], acSptFile[_MAX_PATH],
            acBuf[MAX_RECSIZE], acRec1[MAX_RECSIZE], acRec2[MAX_RECSIZE];
   int      iRet;
   long     lOut=0, lCnt=0;
   FILE     *fdBase, *fdR01, *fdSpt;
   TAXBASE  *pTaxBase = (TAXBASE *)&acBuf[0];

   LogMsg0("Loading tax supplement file");

   lLastTaxFileDate = lProcDate;
   pRec2 = pRec1 = NULL;

   // Open input
   GetIniString(myCounty.acCntyCode, "SPTDaily", "", acBuf, _MAX_PATH, acIniFile);
   sprintf(acSptFile, acBuf, lProcDate);
   if (!_access(acSptFile, 0))
   {
      // SPT file needs resort before processing
      sprintf(acBuf, "%s\\%s\\%s_Supp.txt", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
      iRet = sortFile(acSptFile, acBuf, "S(#4,C,A,#3,C,A,#5,C,A) F(TXT)  DUPOUT BYPASS(249,R) DELIM(44)");
      
      LogMsg("Open SPT daily file %s", acBuf);
      fdSpt = fopen(acBuf, "r");
      // Drop header
      if (fdSpt)
         fgets((char *)&acRec1[0], MAX_RECSIZE, fdSpt);
      else
         return -1;
   } else
   {
      LogMsg("***** ERROR: Missing daily supplement file: %s\n", acSptFile);
      return -2;
   }  

   // Open base output file
   NameTaxCsvFile(acBaseFile, myCounty.acCntyCode, "Base");
   LogMsg("Open Base file %s", acBaseFile);
   fdBase = fopen(acBaseFile, "a+");
   if (fdBase == NULL)
   {
      LogMsg("***** Error opening base file: %s\n", acBaseFile);
      return -4;
   }

   // Open raw file to update TRA
   fdR01 = OpenR01(myCounty.acCntyCode);

   // Reset buffer in UpdateTRA()
   UpdateTRA(acBuf, fdR01, true);

   // Merge loop 
   while (!feof(fdSpt))
   {
      if (!(pRec1 = fgets((char *)&acRec1[0], MAX_RECSIZE, fdSpt)))
         break;
      if (!(pRec2 = fgets((char *)&acRec2[0], MAX_RECSIZE, fdSpt)))
         break;

      // Create TaxBase record
      iRet = Ccx_ParseSPT(acBuf, pRec1, pRec2);
      if (!iRet)
      {
         // Update TRA
         if (fdR01)
            UpdateTRA(acBuf, fdR01);

         // Create TaxBase record
         Tax_CreateTaxBaseCsv(acRec1, (TAXBASE *)&acBuf);
         lOut++;
         fputs(acRec1, fdBase);
      } else
      {
         if (iRet == 1)
            LogMsg("---> Drop cancel record %d [%.40s]", lCnt, acRec1); 
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
      pRec1 = NULL;
   }

   // Close files
   if (fdBase)
      fclose(fdBase);
   if (fdR01)
      fclose(fdR01);
   if (fdSpt)
      fclose(fdSpt);

   LogMsg("Total records processed:    %u", lCnt);
   LogMsg("Total output records:       %u", lOut);
   LogMsg("Total matched records:      %u", lPstMatch);

   // Import into SQL 
   if (bImport)
   {
      iRet = doTaxImport(myCounty.acCntyCode, TAX_BASE);
   } else
      iRet = 0;

   return iRet;
}

/****************************** Ccx_ParseTaxDelq *****************************
 *
 * Return number of records output
 *
 *****************************************************************************/

int Ccx_ParseTaxDelq15(char *pOutbuf, char *pInbuf)
{
   //char     acTmp[256];
   //int      iTmp, iCnt;
   TAXDELQ  *pOutRec = (TAXDELQ *)pOutbuf;
   SR5515   *pInRec  = (SR5515 *) pInbuf;

   return 0;
}

int Ccx_ParseTaxDelq0(char *pOutbuf, char *pInbuf)
{
   int      iRet= 0;
   TAXDELQ  *pOutRec = (TAXDELQ *)pOutbuf;
   SR5500   *pInRec  = (SR5500 *) pInbuf;

   memset(pOutbuf, 0, sizeof(TAXDELQ));

   if (pInRec->Apn[0] > ' ')
      memcpy(pOutRec->Apn, pInRec->Apn, SIZ_DSL_APN);

   memcpy(pOutRec->BillNum, pInRec->DSL_Sft_SaleNbr, SIZ_DSL_SALE_NBR);
   switch(pInRec->DSL_Sft_Status)
   {
      case ' ':
      case 'A':
         pOutRec->DelqStatus[0] = TAX_STAT_UNPAID;
         break;
      case 'R':
         pOutRec->DelqStatus[0] = TAX_STAT_REDEEMED;
         break;
      case 'C':
         pOutRec->DelqStatus[0] = TAX_STAT_CANCEL;
         break;
      case 'X':   // Ignore this record
         iRet = -1;     
         break;
      default:
         iRet = -2;     
         break;
   }

   return iRet;
}

// Payment plan
int Ccx_ParseTaxDelq1(char *pOutbuf, char *pInbuf)
{
   TAXDELQ  *pOutRec = (TAXDELQ *)pOutbuf;
   SR5501   *pInRec  = (SR5501 *) pInbuf;
   double   dRedAmt=0;
   long     lTmp;

   // If not redeemed, ignore it
   if (pOutRec->DelqStatus[0] != TAX_STAT_REDEEMED)
      return 0;

   // Add up installment payment
   if (pOutRec->Red_Amt[0] > ' ')
      dRedAmt = atol(pOutRec->Red_Amt);

   // Redeemed date - Keep first one only
   if (pOutRec->Red_Date[0] < '0')
   {
      lTmp = atoin(pInRec->PayDate, SIZ_BATCH_DATE);
      if (lTmp > 19000000)
         memcpy(pOutRec->Red_Date, pInRec->PayDate+1, 8);
   }

   dRedAmt += atofn(pInRec->PayAmt, SIZ_DPY_PAYAMT)/100.0;
   sprintf(pOutRec->Red_Amt, "%.2f", dRedAmt);

   return 0;
}

// Delq data
int Ccx_ParseTaxDelq2(char *pOutbuf, char *pInbuf)
{
   static char sLastRec[32];
   int      iRet;
   long     lTmp;
   double   dTmp, dDelqAmt;

   TAXDELQ  *pOutRec = (TAXDELQ *)pOutbuf;
   SR5502   *pInRec  = (SR5502 *) pInbuf;

#ifdef _DEBUG
   //if (!memcmp(pInRec->Apn, "0312200108", 10))
   //   iRet = 0;
#endif

   // If duplicate record, ignore it
   if (!memcmp(sLastRec, pInbuf, 16))
      return 1;
   memcpy(sLastRec, pInbuf, 16);

   // Record fully paid, ignore
   if (pInRec->PaidStatus == 'P')
      return 1;

   if (pOutRec->Apn[0] < '0')
      memcpy(pOutRec->Apn, pInRec->Apn, SIZ_DSL_APN);
   else if (memcmp(pOutRec->Apn, pInRec->Apn, SIZ_DSL_APN))
   {
      LogMsg("*** ParseTaxDelq2: Questionable APN from: %.50s [Base APN: %.10s] - ignore", pInbuf, pOutRec->Apn);
      return -2;
   }

   // Tax year
   lTmp = atoin(pInRec->TaxYear, SIZ_LV_YEAR);
   if (lTmp > 1900 && lTmp < lToyear)
   {
      memcpy(pOutRec->TaxYear, pInRec->TaxYear, SIZ_LV_YEAR);
      sprintf(pOutRec->Def_Date, "%d0630", lTmp+1);
   } else
      iRet = 0;

   // Delq Amt
   dDelqAmt = atofn(pInRec->CntyTaxDelqAmt, SIZ_DPY_PAYAMT, true);

   // Other levy
   dDelqAmt += atofn(pInRec->LevyDelqAmt, SIZ_DPY_PAYAMT, true);
   sprintf(pOutRec->Def_Amt, "%.2f", dDelqAmt/100.0);

   // Cost Amt
   dTmp = atofn(pInRec->CostDelqAmt, SIZ_COST_AMT, true)/100.0;
   sprintf(pOutRec->Fee_Amt, "%.2f", dTmp);

   // Pen Amt
   dTmp = atofn(pInRec->PenDelqAmt, SIZ_DPY_PAYAMT, true)/100.0;
   sprintf(pOutRec->Pen_Amt, "%.2f", dTmp);

   // Paid Status
   if (pOutRec->DelqStatus[0] != TAX_STAT_REDEEMED)
   {
      switch (pInRec->PaidStatus)
      {
         case 'P':   // Fully Paid
         case 'W':   // Write off
            pOutRec->DelqStatus[0] = TAX_STAT_REDEEMED;
            break;
         case 'F':   // First Installment paid
            pOutRec->DelqStatus[0] = TAX_STAT_ONPAYMENT;
            break;
         case 'C':
            pOutRec->DelqStatus[0] = TAX_STAT_CANCEL;
            break;
         default:
            break;
      }
   }

   if (dDelqAmt > 0.0)
      return 0;
   else
      return 1;
}

int Ccx_ParseTaxDelq6(char *pOutbuf, char *pInbuf)
{
   //char     acTmp[256];
   //int      iTmp, iCnt;
   //long     lTmp;
   TAXDELQ  *pOutRec = (TAXDELQ *)pOutbuf;
   SR5506   *pInRec  = (SR5506 *) pInbuf;

   return 0;
}

// Installment plan
int Ccx_ParseTaxDelq8(char *pOutbuf, char *pInbuf)
{
   //char     acTmp[256];
   //int      iTmp, iCnt;
   //long     lTmp;
   TAXDELQ  *pOutRec = (TAXDELQ *)pOutbuf;
   SR5508   *pInRec  = (SR5508 *) pInbuf;

   //if (pOutRec->DelqStatus[0] != TAX_STAT_ONPAYMENT)
   //   pOutRec->DelqStatus[0] = TAX_STAT_ONPAYMENT;

   return 0;
}

/**************************** Ccx_Load_Delq **********************************
 *
 * Delq record consists of a group of record type 1 thru 15.  00 is always the
 * first entry of every parcel.  If multiple type 2 record found, create multiple Delq rec.
 *
 * Return 0 if success.
 *
 *****************************************************************************/

int Ccx_Load_TaxDelq(bool bImport)
{
   char     *pTmp, acBuf[MAX_RECSIZE], acRec[MAX_RECSIZE], acTmp[MAX_RECSIZE];
   char     acTmpFile[_MAX_PATH], acOutFile[_MAX_PATH], acInFile[_MAX_PATH];

   int      iRet, iRecType;
   long     lOut=0, lCnt=0;
   FILE     *fdOut, *fdIn;
   TAXDELQ  *pOutRec = (TAXDELQ *)acBuf;

   LogMsg0("Load tax delinquent");

   GetIniString(myCounty.acCntyCode, "DelqFile", "", acInFile, _MAX_PATH, acIniFile);
   if (_access(acInFile, 0))
   {
      LogMsg("*** DelqFile %s is not available, skip it", acInFile);
      return -1;
   }

   lLastFileDate = getFileDate(acInFile);

   // Open input file
   LogMsg("Open delinquent tax file %s", acInFile);
   fdIn = fopen(acInFile, "r");
   if (fdIn == NULL)
   {
      LogMsg("***** Error opening delinquent tax file: %s\n", acInFile);
      return -2;
   }  

   // Open Output file
   sprintf(acTmpFile, "%s\\%s\\%s_Delq.Tmp", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   LogMsg("Open output file %s", acTmpFile);
   fdOut = fopen(acTmpFile, "w");
   if (fdOut == NULL)
   {
      LogMsg("***** Error creating output file: %s\n", acTmpFile);
      return -4;
   }

   // Merge loop    
   iRet = 0;
   acBuf[0] = 0;
   while (!feof(fdIn))
   {
      pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdIn);
      if (!pTmp || *pTmp < ' ')
         break;
#ifdef _DEBUG      
      //if (!memcmp(acRec, "00", 2) && !memcmp(&acRec[16], "0151000528", 10))
      //{
      //   iRecType = 0;
      //}
#endif

      iRecType = atoin(acRec, 2);
      // Skip deleted record
      while (iRet == -1 && iRecType > 0)
      {
         pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdIn);
         if (!pTmp)
         {
            iRecType = -1;
            acBuf[0] = 0;
            break;
         }
         iRecType = atoin(acRec, 2);
      }

      switch (iRecType)
      {
         case 0:
            if (acBuf[0] > ' ')
            {
               if (pOutRec->DelqStatus[0] != TAX_STAT_REDEEMED && pOutRec->DelqStatus[0] != TAX_STAT_CANCEL)
                  pOutRec->isDelq[0] = '1';

               sprintf(pOutRec->Upd_Date, "%d", lLastFileDate);

               // Output last record
               Tax_CreateDelqCsv(acTmp, (TAXDELQ *)pOutRec);

               // Output record			
               fputs(acTmp, fdOut);
               lOut++;
            }

            iRet = Ccx_ParseTaxDelq0(acBuf, acRec);
            break;

         case 1:
            iRet = Ccx_ParseTaxDelq1(acBuf, acRec);
            break;

         case 2:
            iRet = Ccx_ParseTaxDelq2(acBuf, acRec);
            if (!iRet)
            {
               if (pOutRec->DelqStatus[0] != TAX_STAT_REDEEMED && pOutRec->DelqStatus[0] != TAX_STAT_CANCEL)
                  pOutRec->isDelq[0] = '1';

               sprintf(pOutRec->Upd_Date, "%d", lLastFileDate);

               // Output last record
               Tax_CreateDelqCsv(acTmp, (TAXDELQ *)pOutRec);

               // Output record			
               fputs(acTmp, fdOut);
               lOut++;
            }
            acBuf[0] = 0;
            break;

         case 6:
            iRet = Ccx_ParseTaxDelq6(acBuf, acRec);
            break;
         case 8:
            iRet = Ccx_ParseTaxDelq8(acBuf, acRec);
            break;
         case 15:
            iRet = Ccx_ParseTaxDelq15(acBuf, acRec);
            break;
         default:
            iRet = 1;
            break;
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdIn)
      fclose(fdIn);
   if (fdOut)
      fclose(fdOut);

   // Sort output to dedup entry
   NameTaxCsvFile(acOutFile, myCounty.acCntyCode, "Delq");
   iRet = sortFile(acTmpFile, acOutFile, "S(#1,C,A,#3,C,A,#5,C,A) DUPOUT DEL(124)");

   LogMsg("Total records processed:          %u", lCnt);
   LogMsg("Total delinquent records output:  %u", lOut);

   printf("\nTotal output records: %u", lCnt);

   // Import into SQL
   if (bImport && lOut > 0)
   {
      iRet = doTaxImport(myCounty.acCntyCode, TAX_DELINQUENT);
   } else
      iRet = 0;

   return iRet;
}

 /***************************** Ccx_ParseTaxUns ******************************
 *
 *
 *****************************************************************************/

int Ccx_ParseTaxUns(char *pOutbuf, char *pInbuf)
{
   //char     acTmp[256];
   //int      iTmp, lTmp;

   TAXBASE  *pOutRec = (TAXBASE *) pOutbuf;
   //CCX_UNS *pInRec  = (CCX_UNS *)pInbuf;

   // Clear output buffer
   memset(pOutbuf, 0, sizeof(TAXBASE));


   return 0;
}

/***************************** Ccx_Load_TaxUns *******************************
 *
 * Create import file for "BUD Unsecured Properties.d170906.txt"
 * Use TAX_BASE as output record
 *
 * Return 0 if success.
 *
 *****************************************************************************/

int Ccx_Load_TaxUns(bool bImport)
{
   char     *pTmp, acBuf[MAX_RECSIZE], acRec[MAX_RECSIZE];
   char     acOutFile[_MAX_PATH], acInFile[_MAX_PATH];

   int      iRet;
   long     lOut=0, lCnt=0;
   FILE     *fdOut, *fdIn;

   NameTaxCsvFile(acOutFile, myCounty.acCntyCode, "Supp");
   pTmp = strrchr(acOutFile, '\\');
   *pTmp = 0;
   if (_access(acOutFile, 0))
      _mkdir(acOutFile);
   *pTmp = '\\';

   GetIniString(myCounty.acCntyCode, "UnsFile", "", acInFile, _MAX_PATH, acIniFile);

   // Open input file
   LogMsg("Open unsecured tax file %s", acInFile);
   fdIn = fopen(acInFile, "r");
   if (fdIn == NULL)
   {
      LogMsg("***** Error opening unsecured tax file: %s\n", acInFile);
      return -2;
   }  

   lLastFileDate = getFileDate(acInFile);

   // Open Output file
   LogMsg("Open output file %s", acOutFile);
   fdOut = fopen(acOutFile, "w");
   if (fdOut == NULL)
   {
      LogMsg("***** Error creating output file: %s\n", acOutFile);
      return -4;
   }

   // Merge loop 
   while (!feof(fdIn))
   {
      pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdIn);
      if (!pTmp || *pTmp < ' ')
         break;

      // Create new R01 record
      iRet = Ccx_ParseTaxUns(acBuf, acRec);
      if (!iRet)
      {
         // Create delimited record
         Tax_CreateTaxBaseCsv(acRec, (TAXBASE *)&acBuf);

         // Output record			
         lOut++;
         fputs(acRec, fdOut);
      } else
      {
         LogMsg("---> Drop Supp record %d [%.40s]", lCnt, acRec); 
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdIn)
      fclose(fdIn);
   if (fdOut)
      fclose(fdOut);

   LogMsg("Total records processed:          %u", lCnt);
   LogMsg("Total delinquent records output:  %u", lOut);

   printf("\nTotal output records: %u", lCnt);

   // Import into SQL
   if (bImport)
   {
      iRet = doTaxImport(myCounty.acCntyCode, TAX_SUPPLEMENT);
   } else
      iRet = 0;

   return iRet;
}

/****************************** Ccx_UnzipTaxUpdate ******************************
 *
 * Return number of records output, <0 if error
 *
 ********************************************************************************/

int Ccx_UnzipTaxUpdate(void)
{
   char     *pTmp, acRec[MAX_RECSIZE];
   char     acInFile[_MAX_PATH], acPtsFile[_MAX_PATH], acUnzipFolder[_MAX_PATH],
            acTmpFile[_MAX_PATH];

   int      iRet;
   BOOL     bRet;

   GetIniString(myCounty.acCntyCode, "DailyInfo", "", acRec, _MAX_PATH, acIniFile);
   UnzipTax:
   sprintf(acInFile, acRec, lProcDate);
   if (_access(acInFile, 0))
   {
      if (lProcDate == lToday)
      {
         lProcDate = lToday-1;
         goto UnzipTax;
      }
      return -2;
   }

   // Find unzip folder
   GetIniString(myCounty.acCntyCode, "PTSDaily", "", acUnzipFolder, _MAX_PATH, acIniFile);
   sprintf(acPtsFile, acUnzipFolder, lProcDate);

   sprintf(acTmpFile, "%s\\CCX\\CCX_PTS.TXT", acTmpPath);

   // Check file date, if nothing new do not unzip
   if (!_access(acTmpFile, 0) && !_access(acPtsFile, 0))
      iRet = chkFileDate(acPtsFile, acTmpFile);
   else
      iRet = 1;

   if (iRet == 1)
   {
      pTmp = strrchr(acUnzipFolder, '\\');
      *pTmp = 0;

      // Unzip daily file
      if (iRet = unzipOne(acInFile, acUnzipFolder, true))
         return iRet;

      // Sort input file
      sprintf(acTmpFile, "%s\\CCX\\CCX_PTS.TXT", acTmpPath);
      iRet = sortFile(acPtsFile, acTmpFile, "S(#4,C,A,#5,C,A) B(248,R)");
   } else
      iRet = 1;

   if (iRet > 0)
   {
      bRet = CopyFile(acTmpFile, acPtsFile, false);
      if (!bRet)
      {
         LogMsg("***** Error copying file %s to %s", acTmpFile, acPtsFile);
         iRet = -1;
      }
   }

   return iRet;
}

/*********************************** loadCcx ********************************
 *
 * Input files:
 *    - srmfile.txt              (roll file, 600-byte ebcdic)
 *    - pdrfile.txt              (char file, 100-byte ebcdic)
 *    - salfile.txt              (sale file, 346-byte ebcdic)
 *    - srmeqrol 05 wUC.txt      (lien date roll file, 552-byte ascii)
 *    - In 2006, lien date roll is srmfile.txt
 *    - in 2010, ldr is srmeqrol.txt in ASCII
 *
 * Usage:
 *    - Normal update: LoadOne -CCCX -U [-Usi|-Ms] [-Xa] [-T|-Lt] [-Lyyyymmdd]
 *    - Load Lien:     LoadOne -CCCX -L -Xl [-Usi|-Ms] [-X8|B8] -Xa
 *    - Update situs city using MergeAdr -CCCX -A
 *    - Only use -X8 or -B8 when needed.
 *
 ****************************************************************************/

int loadCcx(int iSkip)
{
   int   iRet, iSrcDate, iDstDate;
   char  *pTmp, acDefFile[_MAX_PATH], acTmpFile[_MAX_PATH], acSrtFile[_MAX_PATH];
   char  acTmp[_MAX_PATH], acOutFile[_MAX_PATH];

   // Load tax
   if (iLoadTax == TAX_LOADING)                    // -T or -Lt
   {
      if (!lProcDate)
         lProcDate = lToday;

      // Call this once at the beginning of the tax year to create TaxDist file
      //iRet = Ccx_ExtrTaxDist(myCounty.acCntyCode);
     
      // Load Tax Agency table
      iRet = GetIniString(myCounty.acCntyCode, "TaxDist", "", acTmpFile, _MAX_PATH, acIniFile);
      if (iRet > 0)
         iRet = LoadTaxCodeTable(acTmpFile);

      // Unzip daily update file
      iRet = Ccx_UnzipTaxUpdate();
      if (iRet < 0)
      {
         LogMsg("*** No new Tax file");
         iLoadFlag = 0;
         return 0;
      }

      iRet = Ccx_Load_TaxBase_PTS(false);
      if (!iRet && lLastTaxFileDate > 0)
      {
         iRet = Ccx_Load_TaxSupp_SPT(bTaxImport);

         iRet = Ccx_Load_TaxDetail(bTaxImport);

         iRet = Ccx_Load_TaxDelq(bTaxImport);
         if (!iRet)
            iRet = updateDelqFlag(myCounty.acCntyCode, false, true);
      }

      return iRet;
   }

   // Preset APN length
   iApnLen = myCounty.iApnLen;
   iSaleApnLen = SSIZ_APN;
   iRet = 0;

   if (lOptProp8 & MYOPT_EXT)                      // -X8 Extract prop8 flag to text file
   {
      GetIniString(myCounty.acCntyCode, "Prop8File", "", acTmpFile, _MAX_PATH, acIniFile);
      iRet = Ccx_ExtractProp8(acTmpFile);
   }

   // Extract Attom sales 
   if (iLoadFlag & EXTR_NRSAL)                      // -Xn
   {
      iRet = GetIniString(myCounty.acCntyCode, "NdcSale", "", acTmpFile, _MAX_PATH, acIniFile);
      if (!_access(acTmpFile, 0))
      {
         iRet = NR_CreateSCSale(myCounty.acCntyCode, acTmpFile, iApnLen-1);
         if (iRet)
            return iRet;
      } else
      {
         LogMsg("*** Sale file not available.  Please verify: %s", acTmpFile);
         return -1;
      }
   }

   // Update sale for specific year
   //iRet = Ccx_LdrUpdateSale("2010", false);
   //iRet = Ccx_LdrUpdateSale("2009", false);
   //iRet = Ccx_LdrUpdateSale("2008", true);
   //iRet = CCX_ConvertHistSale(myCounty.acCntyCode);

   // This has to be done before roll update
   // Input:  salfile.txt
   // Output: salfile.asc, CCX_Sale.dat, Ccx_char_cum.dat
   if (iLoadFlag & (UPDT_SALE|EXTR_SALE))          // -Us or -Xs
   {
      // Translate sale file ebcdic to ascii
      GetIniString(myCounty.acCntyCode, "SaleDef", "", acDefFile, _MAX_PATH, acIniFile);
      if (_access(acDefFile, 0))
      {
         LogMsg("***** Error: missing definition file %s", acDefFile);
         return 1;
      }

      // Translate EBCDIC to ASCII
      strcpy(acTmpFile, acSaleFile);
      strcpy(acSrtFile, acSaleFile);
      pTmp = strrchr(acSaleFile, '.');
      strcpy(++pTmp, "ASC");
      iSrcDate = getFileDate(acTmpFile);
      iDstDate = getFileDate(acSaleFile);
      if (iDstDate <= iSrcDate)
      {
         LogMsg("Translate %s to Ascii %s", acTmpFile, acSaleFile);
         iRet = F_Ebc2Asc(acTmpFile, acSaleFile, acDefFile, 0);
         if (iRet < 0)
         {
            dispError(iRet, acTmpFile, acSaleFile, acDefFile);
            return iRet;
         }
      }

      // Extract sale data
      iRet = Ccx_ExtrSaleFromSalFile();

      // Replace Sale APN with Roll APN if matched
      if (!iRet)
      {
         iRet = Sale_MatchRoll(acCSalFile, iSaleApnLen);
         if (iRet > 0)
            iRet = 0;
      } 

      if (!iRet)
         iLoadFlag |= MERG_CSAL;
      else
         return iRet;
   }

   GetIniString(myCounty.acCntyCode, "CharDef", "", acDefFile, _MAX_PATH, acIniFile);
   if (_access(acDefFile, 0))
   {
      LogMsg("***** Error: missing definition file %s", acDefFile);
      return 1;
   }

   if (iLoadFlag & EXTR_ATTR)                      // -Xa
   {
      // Translate char file ebcdic to ascii
      sprintf(acTmpFile, "%s\\%s\\Ccx_Char.asc", acTmpPath, myCounty.acCntyCode);
      iDstDate = getFileDate(acTmpFile);
      iSrcDate = getFileDate(acCharFile);

      if (iDstDate <= iSrcDate)
      {
         LogMsg("Translate %s to Ascii %s", acCharFile, acTmpFile);
         iRet = F_Ebc2Asc(acCharFile, acTmpFile, acDefFile, 0);
         if (iRet < 0)
         {
            dispError(iRet, acTmpFile, acCharFile, acDefFile);
            return 1;
         }
      }

      // Extract to STDCHAR format - output CCX_Char.dat
      GetIniString("Data", "CChrFile", "", acTmp, _MAX_PATH, acIniFile);
      sprintf(acOutFile, acTmp, myCounty.acCntyCode, myCounty.acCntyCode);
      iRet = Ccx_ConvStdChar(acTmpFile, acOutFile);
   }

   // Translate to ascii
   if (iLoadFlag & (LOAD_LIEN|LOAD_UPDT|EXTR_LIEN))
   {
      // Translate char file ebcdic to ascii if not done yet
      sprintf(acTmpFile, "%s\\%s\\Ccx_Char.asc", acTmpPath, myCounty.acCntyCode);
      if (_access(acTmpFile, 0))
      {
         LogMsg("Translate %s to Ascii %s", acCharFile, acTmpFile);
         iRet = F_Ebc2Asc(acCharFile, acTmpFile, acDefFile, 0);
         if (iRet < 0)
         {
            dispError(iRet, acTmpFile, acCharFile, acDefFile);
            return 1;
         }
      }
      strcpy(acCharFile, acTmpFile);

      if (iLoadFlag & LOAD_UPDT)
      {
         // Translate Roll file ebcdic to ascii 600-byte record
         if (strstr(acRollFile, "srmfile"))
         {
            iSrmVersion = 1;

            GetIniString(myCounty.acCntyCode, "RollDef", "", acDefFile, _MAX_PATH, acIniFile);
            if (_access(acDefFile, 0))
            {
               LogMsg("***** Error: missing definition file %s", acDefFile);
               return 1;
            }

            strcpy(acTmpFile, acRollFile);
            pTmp = strrchr(acRollFile, '.');
            strcpy(++pTmp, "ASC");
            LogMsg("Translate %s to Ascii %s", acTmpFile, acRollFile);
            iRet = F_Ebc2Asc(acTmpFile, acRollFile, acDefFile, 0);
            if (iRet < 0)
            {
               dispError(iRet, acTmpFile, acRollFile, acDefFile);
               return 1;
            }
         } else
            iSrmVersion = 2;

         // Resort input file
         sprintf(acTmpFile, "%s\\%s\\%s_Roll.txt", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
         iRet = sortFile(acRollFile, acTmpFile, "S(1,10,C,A) F(TXT)");
         if (iRet > 0)
            strcpy(acRollFile, acTmpFile);
         else
            return -1;
      }

      /*
      // Translate Roll file ebcdic to ascii - 551-byte record
      pTmp = strrchr(acRollFile, '\\');
      sprintf(acTmpFile, "%s\\%s%s", acTmpPath, myCounty.acCntyCode, pTmp);
      LogMsg("Translate %s to Ascii %s", acRollFile, acTmpFile);
      iRet = doEBC2ASCAddCR(acRollFile, acTmpFile, iRollLen);

      if (iRet < 0)
      {
         iLoadFlag = 0;
         iRet = -1;
         LogMsg("***** ERROR sorting roll file %s to %s", acRollFile, acTmpFile);
      }
      strcpy(acRollFile, acTmpFile);
      */

   }

   // Extract Lien value
   if (iLoadFlag & EXTR_LIEN)                      // -Xl
      iRet = Ccx_ExtrLien();
   //if (iLoadFlag & EXTR_SADR)          
   //   iRet = Ccx_ExtrSitus();

   // Merge roll file
   if (iLoadFlag & LOAD_LIEN)                      // -L
      iRet = Ccx_Load_LDR(iSkip);
   else if (iLoadFlag & LOAD_UPDT)                 // -U
   {
      sprintf(acTmpFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "rol");

      // Extract sale1 from roll file (Ccx_Sale.rol) then merge to cum sale file
      iRet = Ccx_ExtrRSale(acTmpFile);
      if (!iRet)
      {
         // Merge with cum sale file and output to Ccx_Sale.sls
         iRet = Ccx_AppendToCumSale(acTmpFile);
      }

      // Load roll update
      iRet = Ccx_Load_Roll(iSkip);
   }

   // Apply cum sale file to R01
   if (!iRet && (iLoadFlag & MERG_CSAL) )          // -Ms
      iRet = ApplyCumSale(iSkip, acCSalFile, false, SALE_USE_SCUPDXFR, CLEAR_UPD_SALE /* clear sales only if sale update available */);

   // Apply NDC sales
   if (!iRet && iLoadFlag & UPDT_XSAL)             // -Mn
   {
      GetIniString("Data", "ASH_File", acESalTmpl, acESalTmpl, _MAX_PATH, acIniFile);
      sprintf(acCSalFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "Sls");

      // Apply Sfx_ash.sls to R01 file
      // Reset APN length to match with NDC data.
      myCounty.iApnLen = 9;
      //iRet = ApplyCumSale(iSkip, acCSalFile, false, SALE_USE_SCUPDXFR, 0);
      iRet = ApplyCumSaleNR(iSkip, acCSalFile, SALE_USE_SCUPDXFR);
      myCounty.iApnLen = 10;
      if (!iRet)
         iLoadFlag |= LOAD_UPDT;
   }

   // Fix TRA
   if (!iRet && (lOptMisc & M_OPT_FIXTRA))
      iRet = PQ_FixR01(myCounty.acCntyCode, PQ_FIX_TRA, 0, true);

   if (!iRet && bMergeOthers)
   {
      // CCX only has PP_Val value
      sprintf(acTmpFile, acLienTmpl, myCounty.acCntyCode, myCounty.acCntyCode);
      if (!_access(acTmpFile, 0))
         iRet = PQ_MergeOthers(myCounty.acCntyCode, acTmpFile, 0, iSkip);
      else
         LogMsg("***** Lien extract is missing: %s.  Please locate the file and retry", acTmpFile);
   }

   GetIniString(myCounty.acCntyCode, "Prop8File", "", acTmpFile, _MAX_PATH, acIniFile);
   if (lOptProp8 & MYOPT_UDB)          // -D8 Set prop8 flag in DB
   {
      GetIniString(myCounty.acCntyCode, "CntyProvider", "", acMdbProvider, _MAX_PATH, acIniFile);
      GetIniString(myCounty.acCntyCode, "CntyTable", "", acCntyTbl, _MAX_PATH, acIniFile);
      iRet = Ccx_SetProp8(acTmpFile);
   } else if (!iRet && (lOptProp8 & MYOPT_SET))    // -B8 Set prop8 flag in R01 file
      iRet = Ccx_MergeProp8(acTmpFile, iSkip);

   if (!iRet && (lOptMisc & M_OPT_FIXSAMT))
      PQ_FixR01(myCounty.acCntyCode, PQ_REM_AMT9);

   return iRet;
}