/**************************************************************************
 *
 * Notes: 
 *    - spMergeTaxBase requires BillNum.  It won't work properly if it's NULL.
 *
 * 10/13/2015 Add Tax_CreateTaxOwnerCsv(), importTaxFile(), and simplify doTaxImport().
 * 10/15/2015 Modify Tax_CreateTaxOwnerCsv() due to TAXOWNER changed
 * 01/04/2016 Add Load_TC() and TC_*() to load scrape tax data.
 * 01/05/2016 Add TC_SetDateFmt() to specify date format in TC file. 
 *            Modify TC_ParseTaxBase() to set default due date if not provided.
 * 03/07/2016 Rename "Detail" to "Items"
 * 03/09/2016 Update lLastTaxFileDate in Load_TC()
 * 03/16/2016 Change sort parameters in TC_LoadTaxAgencyAndDetail() to dedup agency file.
 * 04/06/2016 Modify TC_ParseTaxDist() to fix bug when tax rate is fixed.
 * 04/08/2016 Modify TC_ParseTaxBase() to add default DueDate.
 * 04/12/2016 Add Tax_CreatePaidCsv().  Modify doTaxImport() to support loading TAXPAID.
 * 04/14/2016 Add DelqStatus to Tax_CreateDelqCsv()
 * 04/20/2016 Add updateTaxAgency() for RIV
 * 05/03/2016 Modify TC_ParseTaxBase() to populate PaidDate1 when PaidDate2 is avail.
 * 08/04/2016 Modify Tax_CreateTaxBaseCsv() to add delqYear and UpdatedDate
 *            Modify Tax_CreateAgencyCsv() to add TaxAmt
 * 08/17/2016 Add LoadTaxCodeTable() and findTaxDist(). Modify Tax_CreateDetailCsv() to include TaxRate
 *            in Tax_Items since same Agency may have multiple rate.
 * 08/18/2016 Move iNumTaxDist to Tax.h
 * 08/19/2016 Modify TC_ParseTaxDist() to support external TaxDist list
 * 08/24/2016 Modify Tax_CreateDelqCsv() to fill BillNum with DefaultNo if available.
 * 08/25/2016 Add doTaxPrep() to create empty table.
 * 08/29/2016 Add findTaxAgency() to find agency name using TaxCode
 * 09/01/2016 Modify prototype of findTaxAgency() to support different starting point, default is 0.
 * 09/07/2016 Modify Tax_CreateTaxBaseCsv() to add option to use Upd_Date & findTaxDist() to add 
 *            option to start search at any given position.
 * 09/14/2016 Use global lTaxYear in TC_ParseTaxBase() and TC_ParseTaxDist()
 * 09/15/2016 Add iTaxYear in Tax.h
 * 10/12/2016 Modify LoadTaxCodeTable() to add option for tax code translation.
 * 10/14/2016 Modify Load_TC() to load TaxBase first so the TaxYear is set correctly.
 *            Modify TC_ParseTaxBase() to use iTaxYear instead of lLienYear to format DueDate.
 * 10/15/2016 Modify TC_ParseTaxBase(): If TaxYear < 1900, drop that record
 *            Modify Tax_CreateAgencyCsv() to force uppercase on Agency
 * 10/19/2016 Add InstDueDate() and modify TC_ParseTaxBase() and TC_ParseTaxDist()
 *            since co3DIST.TC layout has been changed.  TC_ParseTaxDist now will
 *            pick up phone if available.  Add comment.
 * 10/21/2016 Fix PaidAmt1 in TC_ParseTaxBase().  Modify TC_ParseTaxDist() to add BillNum.
 * 10/23/2016 Modify Tax_CreatePaidCsv() to use Default_No if present (ORG).
 * 10/24/2016 Add update_TI_BillNum() to update Tax_Items.BillNum using Tax_Base
 * 10/25/2016 Add Tax_CreateAgencyTable() to load district file into SQL table TAX_AGENCY
 * 11/24/2016 Modify TC_LoadTaxAgencyAndDetail() to add Phone number to Agency record
 * 11/29/2016 Add TC_LoadTaxBaseAndDelq() to create Tax_Delq if data available.
 * 01/09/2017 Add m_bUseFeePrcl to turn ON/OFF the use of FeePrcl in place of BillNum when BillNum is not available.
 *            Add ChkDueDate() function to check whether updated date has passed certain due date.
 * 01/11/2017 Modify TC_ParseTaxDist() to ignore item that has no tax amt.
 * 01/18/2017 Modify TC_ParseTaxBase() to add check for DEF_DATE in new TC layout.  
 *            If PaidStatus is CANCEL, do not set DueDate.  If ROLL_CAT=SEC, set isSecd=1.
 * 01/24/2017 Modify TC_ParseTaxBase() to set isSecd & isSupp based on ROLL_CAT.  Also check
 *            date format amd use correct formating template. Set default date based in bill type (sec or supp).
 * 01/27/2017 Add bInclTaxRate to include TaxRate in Agency table.  Modify Load_TC() to use
 *            external Agency table via LoadTaxCodeTable().
 * 02/08/2017 Modify TC_ParseTaxBase() to update DEF_DATE & RED_DATE to Delq record.
 * 02/10/2017 Add updateTaxDelq()
 * 02/14/2017 Rename updateTaxDelq() to updateDelqFlag().
 * 02/16/2017 Modify TC_ParseTaxDist() to remove TaxAmt from Agency table since this is not unique (SOL).
 * 02/17/2017 Modify Load_TC() to get TaxYear from INI file if not found in TC file.
 * 03/03/2017 Modify Tax_CreateAgencyCsv(), Tax_CreateDetailCsv(), TC_ParseTaxDist(), LoadTaxCodeTable() to add TC_Flag.
 * 03/04/2017 Add findExactTaxDist() in addition to findTaxDist()
 * 03/07/2017 Add new version of ChkDueDate().  Add Add Inst1Status & Inst2Status (MEN)
 *            to replace PaidStatus.
 * 03/08/2017 Modify TC_ParseTaxBase() to populate DueDate only if tax has not been paid.
 * 03/09/2017 Modify Tax_CreateTaxBaseCsv() to add inst1Status, inst2Status, billType to taxBase.
 * 03/13/2017 Fix potential error in Tax_CreateAgencyCsv() & Tax_CreateDetailCsv()
 * 03/15/2017 Fix TC_LoadTaxBaseAndDelq() that first record is missing.
 *            Modify TC_ParseTaxDist() to use phone number from TaxDist file.
 *            Modify sort command in TC_LoadTaxAgencyAndDetail() to dupout on Agency only.
 * 03/16/2017 Modify TC_ParseTaxBase() to insert due date if bill was paid.
 * 03/22/2017 Modify Tax_CreateTaxBaseCsv() to add tb_altApn
 * 03/24/2017 Modify Load_TC() to check for new file before processing.  Add isNewTaxFile()
 * 03/26/2017 Fix bug in isNewTaxFile(), add iNewCodeIdx.  Modify TC_ParseTaxDist() to 
 *            auto add new Agency into table.  Modify TC_LoadTaxAgencyAndDetail() to allow
 *            caller option to ignore item without taxamt.
 * 04/02/2017 Modify TC_LoadTaxBaseAndDelq() to update TRA using R01 file.
 *            Add OpenR01() and UpdateTRA().
 * 04/03/2017 Fix bug in Tax_CreatePaidCsv().  Remove back slash from agency name.
 *            Modify isNewTaxFile() to allow passing file date as params.
 * 04/09/2017 Remove Delq file if it's too smale (<10 bytes)
 * 04/13/2017 Modify TC_ParseTaxDist() to support future adding fields in district file.
 * 04/16/2017 Add doTaxImpExt() & doTaxPrepExt() to support update on multiple server automatically
 *            Modify updateDelqFlag() & update_TI_BillNum() to support multiple servers update
 * 04/19/2017 Add TC_ParseTaxBase_S1(), TC_ParseTaxDist_S1() & TC_ParseTaxBase_S2() to support SOL & TUL
 * 04/25/2017 Modify TC_ParseTaxBase() to set Inst1Status=PAID if payment2 is paid.  Also 
 *            set Upd_Date using TC_UPD_DATE if available.
 * 04/26/2017 Fix TC_ParseTaxBase() since the format of TC_UPD_DATE was changed to mm/dd/yyyy
 * 04/30/2017 Add TC_ParseTaxDist_ORG() and new version of findTaxDist()
 * 05/03/2017 Add exception for ORG not to delete DELQ table in Load_TC() when no delinq data available.
 *            This is temporary until ORG.TC includes delinq data.
 * 05/19/2017 Fix updateBaseDelq().  Add iTaxDelq to count Delq records to import
 *            Modify Load_TC() not to delete Delq file even it's empty.
 * 05/23/2017 Fix TaxRate and TaxAmt in TC_ParseTaxDist*() when TaxRate < 0.0
 * 06/02/2017 Add BillNum to TC_ParseTaxBase() & TC_ParseTaxDist() using APN-99 when BillNum not available.
 *            Fix UpdateTRA() for SCR.
 * 06/08/2017 Add Load_TCC() to process special case of SAC where Detail file doesn't include AV.
 *            Add TC_ParseTaxData(), TC_CreateAgencyAndDetail(), TC_LoadTaxFiles() to support Load_TCC().
 *            Modify TC_ParseTaxBase() to check for SECURED CORRECTED record and check for DELQ in PAID_STAT1.
 * 06/09/2017 Modidy InstDueDate() to check on whether county is using extended due date the format accordingly.
 *            Modify ParseTaxBase function to fix "Bad date" bug due to wrong format.
 *            Modify TC_SetDateFmt() to set extended due date flag
 * 06/12/2017 Modify TC_ParseTaxDist() to support county that has tax code but no description (SFX)
 * 06/17/2017 Add TC_ParseTaxDist_S2() & BSrchTaxDist() to handle special case in SBD where TaxDist file  
 *            is too long and has to use binary search.  Add variable iNewAgency.
 * 06/19/2017 Modify TC_ParseTaxDist_S2() to return 1 if no tax amt. This item won't show in items list.
 * 06/20/2017 Move some global variables to Tax.h and make iNewAgency public.
 * 06/27/2017 Rename TC_ParseTaxDist_ORG() to TC_ParseTaxDist_S3().  Add TC_ParseTaxDist_S4() to parse
 *            SAC district file since it has similar agency name.  Modify TC_CreateAgencyAndDetail()
 *            to call TC_ParseTaxDist_S3() instead of TC_ParseTaxDist().
 * 07/06/2017 Fix bug in TC_ParseTaxDist() when running with old data that doesn't have ROLL_CAT.
 * 07/23/2017 Fix bug in TC_LoadTaxFiles() - Dedup agency file.
 * 09/20/2017 Add Upd_Date to Tax_CreateDelqCsv() if not specified.
 * 09/24/2017 Modify updateBaseDelq() to use spUpdateBaseDelq to update both tb_isDelq flag and tb_DelqYear
 * 10/19/2017 Modify TC_ParseTaxBase() to fix RED_DATE & DEF_DATE in diff format.
 *            Modify TC_ParseTaxDist_S2() to add TaxYear to detail record.
 * 11/10/2017 Modify TC_ParseTaxBase() to support UNSECURED.
 * 11/19/2017 Modify Load_TC() to call updateBaseDelq() up on successful load Tax Delq.
 * 11/29/2017 Fix DelqYear in TC_ParseTaxData()
 * 01/10/2018 Modify TC_ParseTaxBase*() to update TaxYear for Delq record.
 * 01/11/2018 Modify TC_ParseTaxBase() & TC_ParseTaxData() to update iTaxYear 
 *            based on TaxYear on record for later used.
 * 01/25/2018 Modify Load_TC() to check for output folder.  If not exist, create it.
 * 03/26/2018 Add Update_TC(), TCU_LoadTaxBaseAndDelq(), updateUBaseDelq(), updateUDelqFlag()
 *            to update tax table creating a temp table then merge to main table.
 * 03/27/2018 Modify Update_TC() to merge update tax data to Tax_Base & Tax_Delq.
 * 03/31/2018 Make lTaxYear global. Add doSortTC().  Modify Tax_CreateDetailCsv() to add 
 *            process date. Modify TC_ParseTaxBase() & TC_ParseTaxData() to process predefined 
 *            TaxYear only. Modify Load_TC(), Load_TCC(), Update_TC() to sort input file.
 *            Add mergeTaxItems().
 * 04/02/2018 Fix bug in doSortTC() to remove BYPASS header.
 *            Modify doTaxMerge() to allow bypass update to Tax_Items & Tax_Delq tables.
 * 04/05/2018 Rewrite Update_TC() to allow option to update Items & Delq.
 *            Make use iTaxYear and populate Tax_Base using current year only.
 *            Modify Tax_CreateAgencyCsv() to add ta_search.
 * 04/08/2018 Modify TC_ParseTaxBase() to check TaxYear only when lTaxYear > 0
 *            Modify Load_TC(), Load_TCC(), Update_TC() to check for "IgnoreNoValue" on INI file.
 * 04/17/2018 Fix bug in TC_LoadTaxAgencyAndDetail() by check for tax amt > 0.00
 * 04/25/2018 Add log msg when fail to open R01 file in TC_LoadTaxBaseAndDelq()
 * 05/08/2018 Add updateBaseDelqX1() and modify updateDelqFlag() to handle special case.
 * 05/11/2018 Add TC_CreateTaxRecord() (for ALA)
 * 05/16/2018 Modify Load_TC(), Load_TCC(), Update_TC()  to use input file with .TCF & .TCP extensions, not .TC
 *            Add Update_TCC() for SAC.
 * 05/18/2018 Modify TC_LoadTaxAgencyAndDetail() to allow import into Items & Agency tables.
 *            Add TC_LoadTaxBase() to create Tax Base import file only.
 * 05/19/2018 Modify TC_ParseTaxBase_S2() to set DefaultAmt=TotalDue which includes penalty & interest
 * 05/20/2018 Modify TC_ParseTaxBase_S1() to allow supplemental from prior year.
 * 05/21/2018 Modify TC_ParseTaxDist_S4() to use TaxYear based on BillNum.  
 *            Modify S4_CreateAgencyAndDetail() to match up both APN & BillNum to generate 
 *            COUNTY GENERAL TAX correctly. Modify TC_ParseTaxData() to catch prior year supplemental.
 *            Modify Update_TCC() to import detail into TMP table.
 * 06/10/2018 Fix problem of reloading old tax file.
 * 06/25/2018 Add addTaxAgency() to update asTaxDist[] list on the fly.
 * 07/16/2018 Fix TC_ParseTaxBase_S1() to process delq rec when TaxYear<=LienYear
 *            Modify TC_LoadTaxAgencyAndDetail() to identify no tax items and bad tax items correctly. 
 * 09/12/2018 Add doTaxUpdate() & updateTaxBase() to support individual TaxBase record update.
 * 10/04/2018 Add TC_ParseTaxDist_S5() to parse MRN detail file
 * 10/14/2018 Add BSrchTaxDistName() to complement BSrchTaxDist() for searching exact name.
 * 10/15/2018 Check for MAX_TAX_DISTRICT to avoid buffer overrun.  Add SetTaxRateIncl()
 * 10/21/2018 Add TC_ParseTaxDist_S6() to support SCL.  Increase MAX_TAX_DISTRICT to 32000.
 * 10/24/2018 Modify TC_ParseTaxBase_S1() to set paiddate2=paiddate1 if paiddate2 is a future date 
 *            (Inst1 & Inst2 may be paid at the same time)
 * 11/12/2018 Modify TC_ParseTaxBase*() to handle case when Inst2 is prepaid.
 * 11/13/2018 Add MAX_TRA, asTRADist[], findTRADist(), LoadTRADistTable(), iNumTRADist. (SDX)
 * 11/14/2018 Modify TC_ParseTaxDist_S5() to fix special case for MRN.
 * 12/06/2018 Add TC_CreateR01()
 * 02/01/2019 Modify updateBaseDelq() & updateBaseDelqX1() to handle special case for MPA.
 * 03/20/2019 Modify isNewTaxFile() to log error if input file does not exist.
 * 05/08/2019 Modify Update_TC(), Update_TCC(), Load_TC(), Load_TCC() to ignore input file < 1000 bytes.
 * 06/06/2019 Modify doTaxUpdate().  Need more testing
 * 06/11/2019 Add mergeTaxTbl() and modify doMergeTax() to support supplemental (ORG). Add SetCounty() to set defeult county.
 *            Modify TC_ParseTaxData() & TC_ParseTaxDist_S3() to set BillNum=APN when UseFeePrcl=N
 * 08/27/2019 Modify TC_LoadTaxBaseAndDelq() to skip future tax year.
 *            Skip import if number of records is < 100.
 * 10/29/2019 Modify TC_ParseTaxDist_S4() to fix SAC bug.
 * 10/30/2019 Add doUpdateTotalRate() to populate TaxBase.tb_TotalRate by sum up TaxItems.ti_rate
 *            per APN and update its record on TaxBase.
 * 10/31/2019 Modify TC_ParseTaxBase() to ignore record without Due/Paid date.
 * 11/12/2019 Modify doUpdateTotalRate() to support multi version of spUpdateTotalRate.
 * 11/24/2019 Modify update_TI_BillNum() to support multi version of spUpdateBillNum.
 * 01/09/2020 Modify TC_ParseTaxDist_S?() to limit phone number length to 14 bytes
 * 01/15/2020 Modify TC_ParseTaxBase() to fix MRN & TC_ParseTaxBase_S2() to keep supplemental record of previous tax year.
 * 01/28/2020 Modify TC_ParseTaxDist_S5() to set tax year based on first two digits of bill number.
 * 04/04/2020 Add LoadTaxRateTable(). Modify Load_TCC() & TC_ParseTaxData() to include TotalTaxRate if supplied.
 * 04/17/2020 Modify TC_ParseTaxBase_S1() to keep DelqYr same as default tax year.  Add refund amount as paid amount.
 * 04/27/2020 Add TC_LoadTax() to handle both the loading of full and partial tax file (any county that used Load_TC() & Update_TC()).
 *            This allows the load program automatically picks which file to load when -T is used.
 * 05/05/2020 Modify Update_TCC(), Update_TC(), Load_TC() to check for iNumTRADist before updating TotalRate
 *            Add TC_ParseTaxBase_S3() to parse SMX data.
 * 07/01/2020 Modify doTaxImpExt(), doTaxImport() and add importTaxFileL() to support update on Linux server.
 * 07/02/2020 Fix bug in TC_LoadTaxBaseAndDelq() when neither R01 nor S01 file is available.
 * 10/12/2020 Modify TC_ParseTaxBase_S1() to bypass date check on future date.
 *            Modify Load_TC(), Update_TC(), Load_TCC(), Update_TCC() & TC_LoadTax() to look for input files 
 *            on county section first then [Data] section.
 * 10/13/2020 Reverse change on Load_TC(), Update_TC(), Load_TCC(), Update_TCC() & TC_LoadTax().
 * 10/19/2020 Fix TC_ParseTaxBase() & TC_ParseTaxBase_S3() to pick up tax values < 1.0
 * 11/08/2020 Add TC_ParseTaxBase_S4() for SFX.
 * 12/20/2020 Modify TC_ParseTaxBase_S1() to allow future date on refund bill.
 * 02/02/2021 Add "UpdateDetail=Y|N" option to INI file to skip update Items table when detail file is in question (SMX).
 * 02/06/2021 Modify TC_ParseTaxBase_S3() to fill in when Amt or Amt2 is 0.
 * 02/25/2021 Fix LoadTaxRateTable() to skip header record.
 * 03/24/2021 Add feature to reset local buffer in UpdateTRA().
 * 04/02/2021 Modify findTaxAgency() to use _memicmp() to compare tax code.
 * 05/29/2021 Add TC_ParseTaxBase_S5() for ALA since they have DUE_DATE and PAID_DATE in different format.
 * 03/03/2022 Add updateTotalTaxRate() to update tb_TotalRate in Tax_Base table (ORG, SCR).
 * 08/30/2022 Add updateBaseDelqX2() to set tb_updatedDate=td_updatedDate when update partial delq file.
 * 10/25/2022 Modify TC_ParseTaxBase_S2() to support TUL new data.
 * 03/19/2023 Add NameTaxCsvFile() to form output tax file names for import.
 * 03/23/2023 Add *fdExpSql, NameTaxSqlFile(), OpenTaxExpSql() & CloseTaxExpSql().
 *            Modify importTaxFile() & doUpdateTotalRate() to output SQL command to file.
 *            Remove updateTotalTaxRate() since ORG & SCR now use doUpdateTotalRate().
 *            Modify updateBaseDelq() to output SQL command.
 * 03/25/2023 Remove special case for MPA in updateBaseDelq(). Remove mergeTaxBase, Items, Delq.
 * 04/11/2023 Modify importTaxFile() to flush buffer after writing to export file.
 * 04/19/2023 Modify ConvertTaxSqlFile() & doTaxImpExt() to support blob storage type.
 * 05/24/2023 Fix bug in updateBaseDelq() that returns non-zero when -Ext option is used.
 *            Remove TaxPrep Owner from Load_TC().
 * 08/10/2023 Modify importTaxFile() & Update_TC() to support general Tmp table.
 * 10/12/2023 Modify LoadTaxRateTable() to support new rate file format from SAC.
 * 10/16/2023 Modify importTaxFileX() to bypass deleting log file when output to sql file.
 * 10/29/2023 Modify updateDelqFlag() and add updateBaseDelqX3() to support new case of SUT.
 * 11/14/2024 Modify TC_ParseTaxDist_S5() to ignore mismatch TAX_CODE on "PENALTY".
 * 02/01/2025 Increase MAX_TRA to 16000 to support LAX.
 *
 **************************************************************************/

#include "stdafx.h"
#include "Logs.h"
#include "Utils.h"
#include "hlAdo.h"
#include "Prodlib.h"
#include "doSort.h"
#include "R01.h"

#define  _TAX_CPP    1
#include "Tax.h"

extern char acIniFile[], sImportLogTmpl[], sTaxOutTmpl[], sTaxSqlTmpl[], acTmpPath[], acToday[], acRawTmpl[];
extern int  execSqlCmd(LPCTSTR strCmd, hlAdo *phDb=NULL);
extern char *apTokens[], cDelim;
extern int  iTokens, iApnLen, iRecLen;
extern long lLastTaxFileDate, lLienYear, lTaxYear, lToday;
extern bool bExpSql, bDebug;

#define  MAX_TAX_DISTRICT     32000
#define  MAX_TRA              16000

TAXAGENCY asTaxDist[MAX_TAX_DISTRICT];
TRADIST   asTRADist[MAX_TRA];
bool      m_bUseFeePrcl, m_bExtDueDate;
char      m_sCnty[32];
FILE      *fdExpSql;

/*****************************************************************************/

void SetCounty(char *pCnty)
{
   if (pCnty)
      strcpy(m_sCnty, pCnty);
   else
      m_sCnty[0] = 0;
}

/***************************** NameTaxCsvFile ********************************
 *
 *
 *****************************************************************************/

void NameTaxCsvFile(char *pOutFileName, char *pCnty, char *Type)
{
   sprintf(pOutFileName, sTaxOutTmpl, pCnty, Type);
}

/***************************** NameTaxSqlFile ********************************
 *
 *
 *****************************************************************************/

void NameTaxSqlFile(char *pOutFileName, char *pCnty, char *pType)
{
   sprintf(pOutFileName, sTaxSqlTmpl, pCnty, pType);
}

/***************************** OpenTaxExpSql *********************************
 *
 * Open sql file for output with pMode="w" for create or "a" for append.
 *
 *****************************************************************************/

int OpenTaxExpSql(char *pCnty, char *pType, char *pMode)
{
   char  sTaxSqlFile[_MAX_PATH];
   int   iRet = 0;

   NameTaxSqlFile(sTaxSqlFile, pCnty, pType);
   fdExpSql = fopen(sTaxSqlFile, pMode);
   if (!fdExpSql)
      iRet = -1;

   return 0;
}

void CloseTaxExpSql()
{
   if (fdExpSql)
   {
      fputs("PRINT 'Job Done!: ' + CONVERT(varchar,GETDATE(),21);\n", fdExpSql);
      fclose(fdExpSql);
      fdExpSql = NULL;
   }
}

/***************************** doUpdateTotalRate *****************************
 *
 * Update tb_TotalRate using Tax_Items.ti_rate
 *
 *****************************************************************************/

int doUpdateTotalRate(char *pCnty, char *pVersion)
{
   int   iRet;
   char  acServerName[64], acTmp[64];

   iRet = GetIniString("Database", "TaxSvr1", "", acServerName, _MAX_PATH, acIniFile);
   if (iRet > 1)
   {
      LogMsg0("Update TotalRate for %s on %s", pCnty, acServerName);
      if (pVersion)
         sprintf(acTmp, "EXEC [dbo].[spUpdateTotalRate%s] '%s'", pVersion, pCnty);
      else
         sprintf(acTmp, "EXEC [dbo].[spUpdateTotalRate] '%s'", pCnty);
      if (bExpSql)
      {
         fputs("PRINT 'UpdateTotalRate: ' + CONVERT(varchar,GETDATE(),21);\n", fdExpSql);
         strcat(acTmp, ";\n");
         fputs(acTmp, fdExpSql);
         iRet = 0;
      } else
         iRet = updTaxDB(acTmp, acServerName);
   }

   if (bExpSql)
      return iRet;

   iRet = GetIniString("Database", "TaxSvr2", "", acServerName, _MAX_PATH, acIniFile);
   if (iRet > 1)
   {
      LogMsg0("Update TotalRate for %s on %s", pCnty, acServerName);
      if (pVersion)
         sprintf(acTmp, "EXEC [dbo].[spUpdateTotalRate%s] '%s'", pVersion, pCnty);
      else
         sprintf(acTmp, "EXEC [dbo].[spUpdateTotalRate] '%s'", pCnty);
      iRet = updTaxDB(acTmp, acServerName);
   }

   iRet = GetIniString("Database", "TaxSvr3", "", acServerName, _MAX_PATH, acIniFile);
   if (iRet > 1)
   {
      LogMsg0("Update TotalRate for %s on %s", pCnty, acServerName);
      if (pVersion)
         sprintf(acTmp, "EXEC [dbo].[spUpdateTotalRate%s] '%s'", pVersion, pCnty);
      else
         sprintf(acTmp, "EXEC [dbo].[spUpdateTotalRate] '%s'", pCnty);
      iRet = updTaxDB(acTmp, acServerName);
   }

   return iRet;
}
/********************************* doSortTC **********************************
 *
 * Sort CO3.TC file to dedup
 * Return number of output records if successful
 *
 *****************************************************************************/

int doSortTC(char *pInfile, char *pOutfile)
{
   int iRet;

   //iRet = sortFile(pInfile, pOutfile, "S(#1,C,A,#5,C,A,#15,C,A,#13,C,A) F(TXT)  OMIT(#5,C,LT,\"0\") DUPOUT(B5000,1,100) BYPASS(422,R) DELIM(124)");
   iRet = sortFile(pInfile, pOutfile, "S(#1,C,A,#5,C,A,#15,C,A,#13,C,A) F(TXT)  OMIT(#5,C,LT,\"0\") DUPOUT(B5000,1,100) DELIM(124)");

   return iRet;
}

/**************************** TC_SetDateFmt **********************************
 *
 *
 *****************************************************************************/

void TC_SetDateFmt(int iFmt, bool bUseExtendedDueDate)
{
   if (iFmt > 0)
      iDateFmt = iFmt;
   m_bExtDueDate = bUseExtendedDueDate;
}

/**************************** Tax_CreateTaxOwner ***************************
 *
 *
 *****************************************************************************/

char *Tax_CreateTaxOwnerCsv(char *pOutbuf, TAXOWNER *pInrec)
{
   sprintf(pOutbuf, "%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s\n",
      pInrec->Apn,pInrec->BillNum,pInrec->TaxYear,pInrec->Name1,pInrec->Name2,pInrec->CareOf,pInrec->Dba,
      pInrec->MailAdr[0],pInrec->MailAdr[1],pInrec->MailAdr[2],pInrec->MailAdr[3]);

   return pOutbuf;
}

/**************************** Tax_CreateTaxBaseCsv ***************************
 *
 * If BillNum is not available, try Assmnt_No
 *
 *****************************************************************************/

char *Tax_CreateTaxBaseCsv(char *pOutbuf, TAXBASE *pInrec)
{
   if (pInrec->Upd_Date[0] < '1')
      sprintf(pInrec->Upd_Date, "%d", lLastTaxFileDate);

   if (pInrec->BillNum[0] > ' ')
      sprintf(pOutbuf, "%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s\n",
         pInrec->Apn,pInrec->BillNum,pInrec->TRA,pInrec->TaxYear,pInrec->DelqYear,pInrec->TaxAmt1,pInrec->TaxAmt2,
         pInrec->PenAmt1,pInrec->PenAmt2,pInrec->PaidAmt1,pInrec->PaidAmt2,
         pInrec->DueDate1,pInrec->DueDate2,pInrec->PaidDate1,pInrec->PaidDate2,
         pInrec->TotalTaxAmt,pInrec->TotalDue,pInrec->TotalFees,pInrec->TotalRate,
         pInrec->isDelq,pInrec->isSecd,pInrec->isSupp, pInrec->Upd_Date,
         pInrec->Inst1Status,pInrec->Inst2Status,pInrec->BillType,pInrec->Assmnt_No);
   else
      sprintf(pOutbuf, "%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s\n",
         pInrec->Apn,pInrec->Assmnt_No,pInrec->TRA,pInrec->TaxYear,pInrec->DelqYear,pInrec->TaxAmt1,pInrec->TaxAmt2,
         pInrec->PenAmt1,pInrec->PenAmt2,pInrec->PaidAmt1,pInrec->PaidAmt2,
         pInrec->DueDate1,pInrec->DueDate2,pInrec->PaidDate1,pInrec->PaidDate2,
         pInrec->TotalTaxAmt,pInrec->TotalDue,pInrec->TotalFees,pInrec->TotalRate,
         pInrec->isDelq,pInrec->isSecd,pInrec->isSupp, pInrec->Upd_Date,
         pInrec->Inst1Status,pInrec->Inst2Status,pInrec->BillType,pInrec->Assmnt_No);

   return pOutbuf;
}

/******************************** Tax_CreateDelqCsv **************************
 *
 * If DefaultNo is available, use it.  Otherwise use BillNum
 *
 *****************************************************************************/

char *Tax_CreateDelqCsv(char *pOutbuf, TAXDELQ *pInrec)
{
   if (pInrec->Upd_Date[0] < '1')
      sprintf(pInrec->Upd_Date, "%d", lLastTaxFileDate);

   if (pInrec->Default_No[0] > ' ')
      sprintf(pOutbuf, "%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s\n",
         pInrec->Apn,pInrec->Default_No,pInrec->TaxYear,pInrec->Def_Date,pInrec->Def_Amt,
         pInrec->Red_Date,pInrec->Red_Amt,pInrec->Upd_Date,pInrec->PrclType,pInrec->InstDel,pInrec->DelqStatus,
         pInrec->isDelq,pInrec->isSupp);
   else
      sprintf(pOutbuf, "%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s\n",
         pInrec->Apn,pInrec->BillNum,pInrec->TaxYear,pInrec->Def_Date,pInrec->Def_Amt,
         pInrec->Red_Date,pInrec->Red_Amt,pInrec->Upd_Date,pInrec->PrclType,pInrec->InstDel,pInrec->DelqStatus,
         pInrec->isDelq,pInrec->isSupp);

   return pOutbuf;
}

/******************************** Tax_CreateSuppCsv **************************
 *
 *
 *****************************************************************************/

//char *Tax_CreateSuppCsv(char *pOutbuf, TAXSUPP *pInrec)
//{ 
//   if (pInrec->Upd_Date[0] < '1')
//      sprintf(pInrec->Upd_Date, "%d", lLastTaxFileDate);
//
//   sprintf(pOutbuf, "%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s\n",
//      pInrec->Apn,pInrec->ApnSeq,pInrec->CurPri,pInrec->Prorate,pInrec->BillNum,
//      pInrec->RollYear,pInrec->RollFctr,pInrec->NetAmt,pInrec->PenAmt,
//      pInrec->Status1,pInrec->TaxAmt1,pInrec->PenAmt1,pInrec->DelqDate1,pInrec->PaidDate1,
//      pInrec->Status2,pInrec->TaxAmt2,pInrec->PenAmt2,pInrec->DelqDate2,pInrec->PaidDate2,
//      pInrec->NoteDate,pInrec->EventDate,pInrec->Type,pInrec->RollSupp);
//   
//   return pOutbuf;
//}

/****************************** Tax_CreateAgencyCsv **************************
 *
 * Add ta_search 4/5/2018
 *
 *****************************************************************************/

char *Tax_CreateAgencyCsv(char *pOutbuf, TAXAGENCY *pInrec)
{
   char  TC_Flag[2];

   // Default
   if (pInrec->TC_Flag[0])
      TC_Flag[0] = pInrec->TC_Flag[0];
   else
      TC_Flag[0] = '0';

   TC_Flag[1] = 0;

   if (TC_Flag[0] == '0')
      sprintf(pOutbuf, "%s|%s|%s|%s|%s|%s|%s (%s)\n",
         pInrec->Code,_strupr(pInrec->Agency),pInrec->Phone,pInrec->TaxRate,pInrec->TaxAmt,TC_Flag, pInrec->Agency, pInrec->Code);
   else
      sprintf(pOutbuf, "%s|%s|%s|%s|%s|%s|%s\n",
         pInrec->Code,_strupr(pInrec->Agency),pInrec->Phone,pInrec->TaxRate,pInrec->TaxAmt,TC_Flag, pInrec->Agency);

   return pOutbuf;
}

/**************************** Tax_CreateDetailCsv ****************************
 *
 * If BillNum is not available, try Assmnt_No
 * Return 0 if success
 *
 *****************************************************************************/

char *Tax_CreateDetailCsv(char *pOutbuf, TAXDETAIL *pInrec)
{
   char  TC_Flag[2];

   // Default
   if (pInrec->TC_Flag[0])
      TC_Flag[0] = pInrec->TC_Flag[0];
   else
      TC_Flag[0] = '0';

   TC_Flag[1] = 0;
   if (pInrec->BillNum[0] > ' ')
      sprintf(pOutbuf, "%s|%s|%s|%s|%s|%s|%s|%s\n",
         pInrec->Apn,pInrec->BillNum,pInrec->TaxYear,pInrec->TaxAmt,pInrec->TaxCode,pInrec->TaxRate,TC_Flag, acToday);
   else
      sprintf(pOutbuf, "%s|%s|%s|%s|%s|%s|%s|%s\n",
         pInrec->Apn,pInrec->Assmnt_No,pInrec->TaxYear,pInrec->TaxAmt,pInrec->TaxCode,pInrec->TaxRate,TC_Flag, acToday);
   return pOutbuf;
}

/******************************** Tax_CreatePaidCsv **************************
 *
 *
 *****************************************************************************/

char *Tax_CreatePaidCsv(char *pOutbuf, TAXPAID *pInrec)
{
   if (pInrec->Default_No[0] > ' ')
      sprintf(pOutbuf, "%s|%s|%s|%s|%s|%s|%s|%s\n",
         pInrec->Apn,pInrec->Default_No,pInrec->PaidDate,pInrec->PaidAmt,pInrec->TaxAmt,
         pInrec->PenAmt,pInrec->FeeAmt,pInrec->PaidFlg);
   else
      sprintf(pOutbuf, "%s|%s|%s|%s|%s|%s|%s|%s\n",
         pInrec->Apn,pInrec->BillNum,pInrec->PaidDate,pInrec->PaidAmt,pInrec->TaxAmt,
         pInrec->PenAmt,pInrec->FeeAmt,pInrec->PaidFlg);

   return pOutbuf;
}

/******************************** updateTaxDB *******************************
 *
 * This function will call store procedure to do bulk insert
 *
 * Return TRUE if success
 *
 ****************************************************************************/

int updateTaxDB(char *strCmd)
{                 
   static bool  bConnected=false;
   static hlAdo hDB;

   char acServer[256];
   int  iRet;

   iRet = GetIniString("Database", "TaxProvider", "", acServer, 128, acIniFile);
   if (!iRet)
   {
      LogMsg("***** Missing TaxProvider in INI file.  Please review %s", acIniFile);
      return -1;
   }

   LogMsg("Execute command on: %s", acServer);
   LogMsg(strCmd);
   try
   {
      // open the database connection
      if (!bConnected)
         bConnected = hDB.Connect(acServer);
   } catch(_com_error &e)
   {
      LogMsg("%s", ComError(e));
      return -2;
   }

   // Update county profile table
   iRet = execSqlCmd(strCmd, &hDB);
   return iRet;
}

int updTaxDB(char *strCmd, char*pServerName)
{                 
   static hlAdo hDB;

   char acServer[256], acTmp[256];
   int  iRet;
   bool bConnected;

   iRet = GetIniString("Database", "TaxProvider", "", acServer, 128, acIniFile);
   if (!iRet)
   {
      LogMsg("***** Missing TaxProvider in INI file.  Please review %s", acIniFile);
      return -1;
   }

   if (pServerName && *pServerName)
   {
      sprintf(acTmp, acServer, pServerName);
      strcpy(acServer, acTmp);
      bConnected = false;
   } else
      bConnected = true;


   LogMsg("Execute command on: %s", acServer);
   LogMsg(strCmd);
   try
   {
      // open the database connection
      if (!bConnected)
      {
         LogMsg("Connect to DB server: %s", pServerName);
         bConnected = hDB.Connect(acServer);
      }
   } catch(_com_error &e)
   {
      LogMsg("%s", ComError(e));
      return -2;
   }

   // Execute requested command
   iRet = execSqlCmd(strCmd, &hDB);

   return iRet;
}

/******************************** doTaxImport *******************************
 *
 * This function will call store procedure to do bulk insert
 *
 * iType values: see tax.h
 *
 * Return 0 if success
 *
 ****************************************************************************/

int importTaxFile(char *pCnty, char *pType, char *pServerName)
{
   char sInFile[128], sErrFile[128], sTblName[32], sImportTmpl[_MAX_PATH];
   char sFmtFile[128], acTmp[1024];
   int  iRet=1;

   GetIniString("Data", "TaxImportTmpl", "", sImportTmpl, _MAX_PATH, acIniFile);
   sprintf(sFmtFile, sImportTmpl, pType);
   sprintf(sErrFile, sImportLogTmpl, "Tax", pCnty, pType);
   sprintf(sTblName, "%.3s_Tax_%s", pCnty, pType);
   NameTaxCsvFile(sInFile, pCnty, pType);

   LogMsg0("Import %s table", sTblName);
   if (!bExpSql)
   {
      if (!_access(sErrFile, 0))
      {
         LogMsg("Delete error log %s", sErrFile);
         DeleteFile(sErrFile);
      }
      sprintf(acTmp, "%s.Error.txt", sErrFile);
      if (!_access(acTmp, 0))
      {
         LogMsg("Delete error log %s", acTmp);
         DeleteFile(acTmp);
      }
   }

   if (_access(sInFile, 0))
   {
      LogMsg("***** Missing import file: %s", sInFile);
      return -1;
   }
   if (_access(sFmtFile, 0))
   {
      LogMsg("***** Missing import format file: %s", sFmtFile);
      return -1;
   }

   // Prepare to import - Create/truncate table
   sprintf(acTmp, "EXEC [dbo].[spPrep_Tax%s] '%.3s'", pType, pCnty);
   if (bExpSql)
   {
      strcat(acTmp, "\n");
      iRet = fputs(acTmp, fdExpSql);
      if (iRet != EOF)
         iRet = 0;
   } else
      iRet = updTaxDB(acTmp, pServerName);

   if (!iRet)
   {
      // Calling store procedure
      if (bExpSql)
      {
         char sCmd[256];
         sprintf(sCmd, "PRINT 'Bulk Import %s: ' + CONVERT(varchar,GETDATE(),21);\n", sTblName);
         fputs(sCmd, fdExpSql);
         sprintf(acTmp, "EXEC [dbo].[spBulkImport] '%s', '%s', '%s', '', 1\n", sTblName, sInFile, sFmtFile);
         fputs(acTmp, fdExpSql);
         fflush(fdExpSql);
      } else
      {
         sprintf(acTmp, "EXEC [dbo].[spBulkImport] '%s', '%s', '%s', '%s', 1", sTblName, sInFile, sFmtFile, sErrFile);
         iRet = updTaxDB(acTmp, NULL);
      }
   }

   return iRet;
}

// Import tax file on Linux server
int importTaxFileL(char *pCnty, char *pType, char *pServerName)
{
   char sInFile[128], sErrFile[128], sTblName[32], sImportTmpl[_MAX_PATH];
   char sFmtFile[128], acTmp[1024];
   int  iRet=1;

   GetIniString("Data", "TaxImportTmpl", "", sImportTmpl, _MAX_PATH, acIniFile);
   sprintf(sFmtFile, sImportTmpl, pType);
   sprintf(sErrFile, sImportLogTmpl, "Tax", pCnty, pType);
   sprintf(sTblName, "%s_Tax_%s", pCnty, pType);
   NameTaxCsvFile(sInFile, pCnty, pType);

   LogMsg0("Import %s table", sTblName);
   if (!bExpSql)
   {
      if (!_access(sErrFile, 0))
      {
         LogMsg("Delete error log %s", sErrFile);
         DeleteFile(sErrFile);
      }
      sprintf(acTmp, "%s.Error.txt", sErrFile);
      if (!_access(acTmp, 0))
      {
         LogMsg("Delete error log %s", acTmp);
         DeleteFile(acTmp);
      }
   }

   if (_access(sInFile, 0))
   {
      LogMsg("***** Missing import file: %s", sInFile);
      return -1;
   }
   if (_access(sFmtFile, 0))
   {
      LogMsg("***** Missing import format file: %s", sFmtFile);
      return -1;
   }

   GetIniString("Data", "LTaxImportTmpl", "", sImportTmpl, _MAX_PATH, acIniFile);
   GetIniString("System", "LImportLog", "", sImportLogTmpl, _MAX_PATH, acIniFile);
   GetIniString("Data", "LTaxOut", "", sTaxOutTmpl, _MAX_PATH, acIniFile);
   sprintf(sFmtFile, sImportTmpl, pType);
   sprintf(sErrFile, sImportLogTmpl, "Tax", pCnty, pType);
   NameTaxCsvFile(sInFile, pCnty, pType);

   // Prepare to import - Create/truncate table
   sprintf(acTmp, "EXEC [dbo].[spPrep_Tax%s] '%s'", pType, pCnty);
   if (bExpSql)
   {
      strcat(acTmp, "\n");
      iRet = fputs(acTmp, fdExpSql);
      if (iRet != EOF)
         iRet = 0;
   } else
      iRet = updTaxDB(acTmp, pServerName);

   if (!iRet)
   {
      // Calling store procedure
      sprintf(acTmp, "EXEC [dbo].[spBulkImport] '%s', '%s', '%s', '%s', 1", sTblName, sInFile, sFmtFile, sErrFile);
      if (bExpSql)
      {
         char sCmd[256];
         sprintf(sCmd, "PRINT 'Bulk Import %s: ' + CONVERT(varchar,GETDATE(),21);\n", sTblName);
         fputs(sCmd, fdExpSql);
         strcat(acTmp, ";\n");
         fputs(acTmp, fdExpSql);
      } else
         iRet = updTaxDB(acTmp, NULL);
   }

   // Reset sTaxOutTmpl
   GetIniString("Data", "TaxOut", "", sTaxOutTmpl, _MAX_PATH, acIniFile);

   return iRet;
}

// Import tax file on blob storage
int importTaxFileC(char *pCnty, char *pType, char *pServerName)
{
   char sInFile[128], sErrFile[128], sTblName[32], sImportTmpl[_MAX_PATH];
   char sFmtFile[128], acTmp[1024];
   int  iRet=1;

   GetIniString("Data", "TaxImportTmpl", "", sImportTmpl, _MAX_PATH, acIniFile);
   sprintf(sFmtFile, sImportTmpl, pType);
   sprintf(sErrFile, sImportLogTmpl, "Tax", pCnty, pType);
   sprintf(sTblName, "%s_Tax_%s", pCnty, pType);
   NameTaxCsvFile(sInFile, pCnty, pType);

   LogMsg0("Import %s table", sTblName);
   if (!bExpSql)
   {
      if (!_access(sErrFile, 0))
      {
         LogMsg("Delete error log %s", sErrFile);
         DeleteFile(sErrFile);
      }
      sprintf(acTmp, "%s.Error.txt", sErrFile);
      if (!_access(acTmp, 0))
      {
         LogMsg("Delete error log %s", acTmp);
         DeleteFile(acTmp);
      }
   }

   if (_access(sInFile, 0))
   {
      LogMsg("***** Missing import file: %s", sInFile);
      return -1;
   }
   if (_access(sFmtFile, 0))
   {
      LogMsg("***** Missing import format file: %s", sFmtFile);
      return -1;
   }

   GetIniString("Data", "CTaxImportTmpl", "", sImportTmpl, _MAX_PATH, acIniFile);
   GetIniString("System", "CImportLog", "", sImportLogTmpl, _MAX_PATH, acIniFile);
   GetIniString("Data", "CTaxOut", "", sTaxOutTmpl, _MAX_PATH, acIniFile);
   sprintf(sFmtFile, sImportTmpl, pType);
   sprintf(sErrFile, sImportLogTmpl, "Tax", pCnty, pType);
   NameTaxCsvFile(sInFile, pCnty, pType);

   // Prepare to import - Create/truncate table
   sprintf(acTmp, "EXEC [dbo].[spPrep_Tax%s] '%s'", pType, pCnty);
   if (bExpSql)
   {
      strcat(acTmp, "\n");
      iRet = fputs(acTmp, fdExpSql);
      if (iRet != EOF)
         iRet = 0;
   } else
      iRet = updTaxDB(acTmp, pServerName);

   if (!iRet)
   {
      // Calling store procedure
      sprintf(acTmp, "EXEC [dbo].[spBulkImportB] '%s', '%s', '%s', '%s', 1", sTblName, sInFile, sFmtFile, sErrFile);
      if (bExpSql)
      {
         char sCmd[256];
         sprintf(sCmd, "PRINT 'Bulk Import %s: ' + CONVERT(varchar,GETDATE(),21);\n", sTblName);
         fputs(sCmd, fdExpSql);
         strcat(acTmp, ";\n");
         fputs(acTmp, fdExpSql);
      } else
         iRet = updTaxDB(acTmp, NULL);
   }

   // Reset sTaxOutTmpl
   GetIniString("Data", "TaxOut", "", sTaxOutTmpl, _MAX_PATH, acIniFile);

   return iRet;
}

int doTaxImpExt(char *pCnty, int iType, char *pServerName, char *pServerType)
{
   int  iRet=1;

   //LogMsg("Import Tax data to: %s", pCnty);
   switch (iType)
   {
      case TAX_OWNER:
         if (*pServerType == 'L')
            iRet = importTaxFileL(pCnty, "Owner", pServerName);
         else if (*pServerType == 'C')
            iRet = importTaxFileC(pCnty, "Owner", pServerName);
         else
            iRet = importTaxFile(pCnty, "Owner", pServerName);
         break;

      case TAX_BASE:
         if (*pServerType == 'L')
            iRet = importTaxFileL(pCnty, "Base", pServerName);
         else if (*pServerType == 'C')
            iRet = importTaxFileC(pCnty, "Base", pServerName);
         else
            iRet = importTaxFile(pCnty, "Base", pServerName);
         break;

      case TAX_DETAIL:
         if (*pServerType == 'L')
            iRet = importTaxFileL(pCnty, "Items", pServerName);
         else if (*pServerType == 'C')
            iRet = importTaxFileC(pCnty, "Items", pServerName);
         else
            iRet = importTaxFile(pCnty, "Items", pServerName);
         break;

      case TAX_AGENCY:
         if (*pServerType == 'L')
            iRet = importTaxFileL(pCnty, "Agency", pServerName);
         else if (*pServerType == 'C')
            iRet = importTaxFileC(pCnty, "Agency", pServerName);
         else
            iRet = importTaxFile(pCnty, "Agency", pServerName);
         break;

      case TAX_DELINQUENT:
         if (*pServerType == 'L')
            iRet = importTaxFileL(pCnty, "Delq", pServerName);
         else if (*pServerType == 'C')
            iRet = importTaxFileC(pCnty, "Delq", pServerName);
         else
            iRet = importTaxFile(pCnty, "Delq", pServerName);
         break;
 
      case TAX_SUPPLEMENT:
         if (*pServerType == 'L')
            iRet = importTaxFileL(pCnty, "Supp", pServerName);
         else if (*pServerType == 'C')
            iRet = importTaxFileC(pCnty, "Supp", pServerName);
         else
            iRet = importTaxFile(pCnty, "Supp", pServerName);
         break;

      case TAX_PAID:
         if (*pServerType == 'L')
            iRet = importTaxFileL(pCnty, "Paid", pServerName);
         else if (*pServerType == 'C')
            iRet = importTaxFileC(pCnty, "Paid", pServerName);
         else
            iRet = importTaxFile(pCnty, "Paid", pServerName);
         break;

      case TAX_UBASE:
         if (*pServerType == 'L')
            iRet = importTaxFileL(pCnty, "Base", pServerName);
         else if (*pServerType == 'C')
            iRet = importTaxFileC(pCnty, "Base", pServerName);
         else
            iRet = importTaxFile(pCnty, "Base", pServerName);
         break;

      case TAX_UDETAIL:
         if (*pServerType == 'L')
            iRet = importTaxFileL(pCnty, "Items", pServerName);
         else if (*pServerType == 'C')
            iRet = importTaxFileC(pCnty, "Items", pServerName);
         else
            iRet = importTaxFile(pCnty, "Items", pServerName);
         break;

      case TAX_UDELINQUENT:
         if (*pServerType == 'L')
            iRet = importTaxFileL(pCnty, "Delq", pServerName);
         else if (*pServerType == 'C')
            iRet = importTaxFileC(pCnty, "Delq", pServerName);
         else
            iRet = importTaxFile(pCnty, "Delq", pServerName);
         break;

      default:
         LogMsg("*** Import file type [%d] has not been implemented.  Please check calling function", iType);
         break;
   }

   return iRet;
}

/*********************************** doTaxImport ***********************************
 *
 * Support multiple server update.  Server name can be defined in INI file
 *
 ***********************************************************************************/

int doTaxImport(char *pCnty, int iType)
{
   int   iRet;
   char  acServerName[64], acServerType[32];

   iRet = GetIniString("Database", "Svr1Type", "W", acServerType, _MAX_PATH, acIniFile);
   iRet = GetIniString("Database", "TaxSvr1", "", acServerName, _MAX_PATH, acIniFile);

   if (iRet > 0)
      iRet = doTaxImpExt(pCnty, iType, acServerName, acServerType);
   
   // Only process server1 if SQL export flag is true
   if (bExpSql)
      return iRet;

   if (!iRet)
   {
      iRet = GetIniString("Database", "Svr2Type", "W", acServerType, _MAX_PATH, acIniFile);
      iRet = GetIniString("Database", "TaxSvr2", "", acServerName, _MAX_PATH, acIniFile);
      if (iRet > 1)
         iRet = doTaxImpExt(pCnty, iType, acServerName, acServerType);
   }

   if (!iRet)
   {
      iRet = GetIniString("Database", "Svr3Type", "W", acServerType, _MAX_PATH, acIniFile);
      iRet = GetIniString("Database", "TaxSvr3", "", acServerName, _MAX_PATH, acIniFile);
      if (iRet > 1)
         iRet = doTaxImpExt(pCnty, iType, acServerName, acServerType);
   }

   if (!iRet)
   {
      iRet = GetIniString("Database", "Svr4Type", "W", acServerType, _MAX_PATH, acIniFile);
      iRet = GetIniString("Database", "TaxSvr4", "", acServerName, _MAX_PATH, acIniFile);
      if (iRet > 1)
         iRet = doTaxImpExt(pCnty, iType, acServerName, acServerType);
   }

   return iRet;
}

/********************************** doTaxUpdExt ************************************
 *
 * Update tax DB by looping through all update statement in a file
 * Return -1 if error.
 *
 ***********************************************************************************/

int doTaxUpdExt(char *pCnty, int iType, char *pServerName)
{
   char sInFile[128], sTmp[4000], *pTmp, sType[32];
   int  iRet=1;
   FILE  *fdInFile;

   switch (iType)
   {
      case TAX_BASE:
         strcpy(sType, "Base");
         break;

      case TAX_DELINQUENT:
         strcpy(sType, "Delq");
         break;
 
      case TAX_SUPPLEMENT:
         strcpy(sType, "Supp");
         break;

      default:
         LogMsg("*** Import file type [%d] has not been implemented.  Please check calling function", iType);
         return -1;
   }
   NameTaxCsvFile(sInFile, pCnty, sType);

   if (_access(sInFile, 0))
   {
      LogMsg("***** Missing import file: %s", sInFile);
      return -1;
   }

   if (!(fdInFile = fopen(sInFile, "r")))
   {
      LogMsg("***** Error opening %s", sInFile);
      return -1;
   }

   while (!feof(fdInFile))
   {
      pTmp = fgets(sTmp, 4000, fdInFile);
      if (!pTmp)
         break;

      // Calling store procedure
      iRet = updTaxDB(sTmp, NULL);
   }

   return iRet;
}

// This function is developed to update partial tax file.  Not ready to use yet....
//int doTaxUpdate(char *pCnty, int iType)
//{
//   int   iRet;
//   char  acServerName[64];
//
//   iRet = GetIniString("Database", "TaxSvr1", "", acServerName, _MAX_PATH, acIniFile);
//   if (iRet > 1)
//      iRet = doTaxUpdExt(pCnty, iType, acServerName);
//
//   if (!iRet)
//   {
//      iRet = GetIniString("Database", "TaxSvr2", "", acServerName, _MAX_PATH, acIniFile);
//      if (iRet > 1)
//         iRet = doTaxUpdExt(pCnty, iType, acServerName);
//   }
//
//   if (!iRet)
//   {
//      iRet = GetIniString("Database", "TaxSvr3", "", acServerName, _MAX_PATH, acIniFile);
//      if (iRet > 1)
//         iRet = doTaxUpdExt(pCnty, iType, acServerName);
//   }
//
//   return iRet;
//}

/************************************ doTaxPrep ************************************
 *
 * Create empty table
 *
 ***********************************************************************************/

int doTaxPrepExt(char *pCnty, int iType, char *pServerName)
{
   int  iRet=1;
   char  acTmp[64], acType[16];

   acTmp[0] = 0;
   switch (iType)
   {
      case TAX_OWNER:
         strcpy(acType, "Owner");
         break;

      case TAX_BASE:
         strcpy(acType, "Base");
         break;

      case TAX_DETAIL:
         strcpy(acType, "Items");
         break;

      case TAX_AGENCY:
         strcpy(acType, "Agency");
         break;

      case TAX_DELINQUENT:
         strcpy(acType, "Delq");
         break;
 
      case TAX_SUPPLEMENT:
         strcpy(acType, "Supp");
         break;

      case TAX_PAID:
         strcpy(acType, "Paid");
         break;

      default:
         acType[0] = 0;
         LogMsg("*** Table type [%d] has not been implemented.  Please check calling function", iType);
         break;
   }

   if (acType[0] > ' ')
   {
      LogMsg("Create empty table %s_Tax_%s", pCnty, acType);
      sprintf(acTmp, "EXEC [dbo].[spPrep_Tax%s] '%s'", acType, pCnty);
      if (bExpSql)
      {
         fputs("PRINT 'Create empty table: ' + CONVERT(varchar,GETDATE(),21);\n", fdExpSql);
         strcat(acTmp, ";\n");
         fputs(acTmp, fdExpSql);
         iRet = 0;
      } else
         iRet = updTaxDB(acTmp, pServerName);
   }

   return iRet;
}

int doTaxPrep(char *pCnty, int iType)
{
   int   iRet;
   char  acServerName[64];

   iRet = GetIniString("Database", "TaxSvr1", "", acServerName, _MAX_PATH, acIniFile);
   if (iRet > 1)
      iRet = doTaxPrepExt(pCnty, iType, acServerName);

   if (bExpSql)
      return iRet;

   iRet = GetIniString("Database", "TaxSvr2", "", acServerName, _MAX_PATH, acIniFile);
   if (iRet > 1)
      iRet = doTaxPrepExt(pCnty, iType, acServerName);

   iRet = GetIniString("Database", "TaxSvr3", "", acServerName, _MAX_PATH, acIniFile);
   if (iRet > 1)
      iRet = doTaxPrepExt(pCnty, iType, acServerName);

   return iRet;
}

/*************************** TC_ParseTaxBase() ********************************
 *
 * DUE_PAID_DATE1 & DUE_PAID_DATE2 may have different format.  Watch out
 *
 * Return < 0 if record is not good or no tax
 *
 ******************************************************************************/

int TC_ParseTaxBase(char *pOutbuf, char *pInbuf, char cDelim)
{
   char     acTmp[256], bBillType;
   int      iRet=0, iTmp, iFmt;
   double	dTmp, dTax1, dTax2, dPen1, dPen2, dTaxTotal;
   TAXBASE  *pOutBase = (TAXBASE *)pOutbuf;
   TAXDELQ  *pOutDelq = (TAXDELQ *)pOutbuf;

#ifdef _DEBUG
   //if (!memcmp(pInbuf, "00103217", 8))
   //   iTmp = 0;
#endif
   // Parse input tax data
   iTokens = ParseStringIQ(pInbuf, cDelim, 128, apTokens);
   if (iTokens < TC_NET_VAL)
   {
      LogMsg("***** Error: bad Tax record for APN=%.*s (#tokens=%d)", iApnLen, pInbuf, iTokens);
      return -1;
   }

   iTaxYear = atol(apTokens[TC_TAX_YR]);
   if (iTaxYear > lTaxYear)
      return -1;

   // Clear output buffer
   memset(pOutBase, 0, sizeof(TAXBASE));

   // Default BillType SECURED ANNUAL
   bBillType = BILLTYPE_SECURED;
   pOutBase->isSecd[0] = '1';
   pOutBase->isSupp[0] = 0;
   if (!memcmp(apTokens[TC_ROLL_CAT], "SECURED CORRECTED", 10))
   {
      bBillType = BILLTYPE_ROLL_CORRECTION;
   } else if (!memcmp(apTokens[TC_ROLL_CAT], "SUP", 3))
   {
      pOutBase->isSecd[0] = 0;
      pOutBase->isSupp[0] = '1';
      bBillType = BILLTYPE_SECURED_SUPPL;
   } else if (!memcmp(apTokens[TC_ROLL_CAT], "UNSEC", 5))
   {
      pOutBase->isSecd[0] = 0;
      pOutBase->isSupp[0] = 0;
      bBillType = BILLTYPE_UNSECURED;
   } 
   pOutBase->BillType[0] = bBillType;

   // Verify date format
   if (strchr(apTokens[TC_DUE_PAID_DT2], '/'))
      iFmt = MM_DD_YYYY_1;
   else
      iFmt = iDateFmt;

   if (!memcmp(apTokens[TC_PAID_STAT1], "DEL", 3))
   {
      int iDelqYear;

      // Delq Year
      iDelqYear = atol(apTokens[TC_TAX_YR]) + 1;
      if (iDelqYear < 1900)
      {
         LogMsg("*** WARNING: No tax data APN=%s", apTokens[TC_APN_S]);
         return -2;
      }

      // Tax year delq
      sprintf(pOutDelq->TaxYear, "%.4d", iDelqYear-1);

      if (iTokens > TC_DEF_DATE && *apTokens[TC_DEF_DATE] > ' ')
      {
         if (strchr(apTokens[TC_DEF_DATE], '/'))
            iTmp = MM_DD_YYYY_1;
         else
            iTmp = iFmt;

         if (dateConversion(apTokens[TC_DEF_DATE], acTmp, iTmp))
         {
            strcpy(pOutDelq->Def_Date, acTmp);
            iTmp = atoin(acTmp, 4);
            sprintf(pOutDelq->TaxYear, "%d", iTmp-1);
         } else
            strcpy(pOutDelq->Def_Date, apTokens[TC_DEF_DATE]);
      } else
         sprintf(pOutDelq->Def_Date, "%d", iDelqYear);

      // APN
      strcpy(pOutDelq->Apn, apTokens[TC_APN_S]);

      // BillNum
      if (*apTokens[TC_BILL_NUM] >= '0')
         strcpy(pOutDelq->Default_No, apTokens[TC_BILL_NUM]);
      else if (m_bUseFeePrcl)
         strcpy(pOutDelq->BillNum, apTokens[TC_FEE_PCL]);

      // Default amt
      dTaxTotal = atof(apTokens[TC_TOTAL_DUE]);
      if (dTaxTotal > 0.0)
      {
         sprintf(pOutDelq->Def_Amt, "%.2f", dTaxTotal);
         pOutDelq->isDelq[0] = '1';

         // Paid amt
         dTmp = atof(apTokens[TC_TOTAL_PAID]);
         if (dTmp > 0.0)
         {
            iTmp = sprintf(pOutDelq->Red_Amt, "%.2f", dTmp);
            if (dTaxTotal == dTmp)
            {
               pOutDelq->DelqStatus[0] = TAX_STAT_REDEEMED;       // Redeemed
               pOutDelq->isDelq[0] = '0';
               if (iTokens >= TC_DEF_DATE)
               {
                  if (strchr(apTokens[TC_RED_DATE], '/'))
                     iFmt = MM_DD_YYYY_1;
                  else
                     iTmp = iFmt;

                  if (*apTokens[TC_RED_DATE] >= '0' && dateConversion(apTokens[TC_RED_DATE], acTmp, iTmp))
                     strcpy(pOutDelq->Red_Date, acTmp);
               }
            } else if (dTaxTotal > dTmp)
               pOutDelq->DelqStatus[0] = TAX_STAT_ONPAYMENT;      // On payment plan
         } else
         {
            pOutDelq->DelqStatus[0] = TAX_STAT_UNPAID;            // Unpaid
         }
      }

      iRet = 2;
   } else
   {
      // Tax Year - ignore if it's not current year
      if (lTaxYear > 0 && iTaxYear != lTaxYear && pOutBase->isSecd[0] == '1')
         return 0;

      sprintf(pOutBase->TaxYear, "%d", iTaxYear);

      // APN
      strcpy(pOutBase->Apn, apTokens[TC_APN_S]);

      // BillNum
      if (*apTokens[TC_BILL_NUM] >= '0')
         strcpy(pOutBase->BillNum, apTokens[TC_BILL_NUM]);
      else if (m_bUseFeePrcl)
      {
         if (!pOutBase->isSecd[0] || pOutBase->isSecd[0] == '1')  
            sprintf(pOutBase->BillNum, "%s-01", apTokens[TC_APN_S]);    // Secured or Unsecured
         else
            sprintf(pOutBase->BillNum, "%s-02", apTokens[TC_APN_S]);    // Supplemental
      } 

      // TRA
      iTmp = atol(apTokens[TC_TRA]);
      if (iTmp > 0)
         sprintf(pOutBase->TRA, "%0.6d", iTmp);

      // Check for Tax amount
      dTax1 = dTax2 = 0.0;
      if (*apTokens[TC_TAX_DUE1] > ' ')
         dTax1 = atof(apTokens[TC_TAX_DUE1]);

      if (*apTokens[TC_TAX_DUE2] > ' ')
         dTax2 = atof(apTokens[TC_TAX_DUE2]);


      if (dTax1 > 0.0)
         sprintf(pOutBase->TaxAmt1, "%.2f", dTax1);
      if (dTax2 > 0.0)
         sprintf(pOutBase->TaxAmt2, "%.2f", dTax2);

      if (*apTokens[TC_TOTAL_TAX] > '0')
         dTaxTotal = atof(apTokens[TC_TOTAL_TAX]);
      else
         dTaxTotal = dTax1+dTax2;

      if (dTaxTotal > 0.0)
         sprintf(pOutBase->TotalTaxAmt, "%.2f", dTaxTotal);

      // Penalty
      dPen1 = atof(apTokens[TC_PEN_DUE1]);
      if (dPen1 > 0.0)
         sprintf(pOutBase->PenAmt1, "%.2f", dPen1);
      dPen2 = atof(apTokens[TC_PEN_DUE2]);
      if (dPen2 > 0.0)
         sprintf(pOutBase->PenAmt2, "%.2f", dPen2);

      // Paid/Due - SFX: PAID/UNPAID/PENDING/DUE/CANCELED/OUTSTANDING
      if (!memcmp(apTokens[TC_PAID_STAT2], "PA", 2))
      {
         InstDueDate(pOutBase->DueDate2, 2, iTaxYear);
         strcpy(pOutBase->PaidAmt2, pOutBase->TaxAmt2);
         if (*apTokens[TC_DUE_PAID_DT2] >= '0' && dateConversion(apTokens[TC_DUE_PAID_DT2], acTmp, iFmt))
            strcpy(pOutBase->PaidDate2, acTmp);
         else if (*apTokens[TC_DUE_PAID_DT1] >= '0' && dateConversion(apTokens[TC_DUE_PAID_DT1], acTmp, iFmt))
            strcpy(pOutBase->PaidDate2, acTmp);                // Inst1 & Inst2 may be paid at the same time
         else if (*apTokens[TC_DUE_PAID_DT2] < '0')
            return 0;
         else
            LogMsg("*** Bad PaidDate 2: %s [%s]", apTokens[TC_DUE_PAID_DT2], apTokens[TC_APN_S]);

         pOutBase->Inst2Status[0] = TAX_BSTAT_PAID;
         pOutBase->Inst1Status[0] = TAX_BSTAT_PAID;
      } else if (!memcmp(apTokens[TC_PAID_STAT2], "CA", 2))    // Cancel
         pOutBase->Inst2Status[0] = TAX_BSTAT_CANCEL;
      else if (!dTax2)                                         // No tax
         pOutBase->Inst2Status[0] = TAX_BSTAT_NOTAX;
      else
      {
         if (*apTokens[TC_DUE_PAID_DT2] >= '0' && dateConversion(apTokens[TC_DUE_PAID_DT2], acTmp, iFmt))
            strcpy(pOutBase->DueDate2, acTmp);
         else
            InstDueDate(pOutBase->DueDate2, 2, iTaxYear);

         if (!memcmp(apTokens[TC_PAID_STAT2], "LA", 2) || ChkDueDate(2, pOutBase->DueDate2))
            pOutBase->Inst2Status[0] = TAX_BSTAT_PASTDUE;
         else
            pOutBase->Inst2Status[0] = TAX_BSTAT_UNPAID;
      } 

      // Verify date format
      if (*(apTokens[TC_DUE_PAID_DT1]+2) == '/' && strlen(apTokens[TC_DUE_PAID_DT1]) == 10)
         iFmt = MM_DD_YYYY_1;
      else
         iFmt = iDateFmt;

      // Paid Inst1
      if (!memcmp(apTokens[TC_PAID_STAT1], "PA", 2))
      {  
         InstDueDate(pOutBase->DueDate1, 1, iTaxYear);
         strcpy(pOutBase->PaidAmt1, pOutBase->TaxAmt1);
         if (*apTokens[TC_DUE_PAID_DT1] >= '0' && dateConversion(apTokens[TC_DUE_PAID_DT1], acTmp, iFmt))
            strcpy(pOutBase->PaidDate1, acTmp);
         else if (pOutBase->PaidDate2[0] >= '0')
            // Sometimes when both installments are paid at the same time, people only keys in date2
            strcpy(pOutBase->PaidDate1, pOutBase->PaidDate2);
         else if (*apTokens[TC_DUE_PAID_DT1] < '0')
            return 0;
         else 
            LogMsg("*** Bad PaidDate 1: %s [%s]", apTokens[TC_DUE_PAID_DT1], apTokens[TC_APN_S]);

         pOutBase->Inst1Status[0] = TAX_BSTAT_PAID;
      } else if (!memcmp(apTokens[TC_PAID_STAT1], "CA", 2))    // Cancel
         pOutBase->Inst1Status[0] = TAX_BSTAT_CANCEL;
      else if (!dTax1)                                         // No tax
         pOutBase->Inst1Status[0] = TAX_BSTAT_NOTAX;
      else 
      {
         if (*apTokens[TC_DUE_PAID_DT1] >= '0' && dateConversion(apTokens[TC_DUE_PAID_DT1], acTmp, iFmt))
            strcpy(pOutBase->DueDate1, acTmp);
         //else if (pOutBase->isSupp[0] == '1')
         //   strcpy(pOutBase->DueDate1, "20170228");
         else
            InstDueDate(pOutBase->DueDate1, 1, iTaxYear);

         // Late or past due
         if (!memcmp(apTokens[TC_PAID_STAT1], "LA", 2) || ChkDueDate(1, pOutBase->DueDate1))
            pOutBase->Inst1Status[0] = TAX_BSTAT_PASTDUE;
         else
            pOutBase->Inst1Status[0] = TAX_BSTAT_UNPAID;
      } 

      // Fee Paid
      dTmp = atof(apTokens[TC_OTHER_FEES]);
      if (dTmp > 0.0)
         iTmp = sprintf(pOutBase->TotalFees, "%.2f", dTmp);

      // Calculate unpaid amt
      dTmp = atof(apTokens[TC_TOTAL_DUE]);
      if (dTmp > 0.0)
         iTmp = sprintf(pOutBase->TotalDue, "%.2f", dTmp);

      iRet = 1;
   }

   // Update date
   if (iTokens > TC_UPD_DATE && *apTokens[TC_UPD_DATE] > ' ' && dateConversion(apTokens[TC_UPD_DATE], acTmp, MM_DD_YYYY_1))
   {
      if (iRet == 1)
         strcpy(pOutBase->Upd_Date, acTmp);
      else
         strcpy(pOutDelq->Upd_Date, acTmp);
   } else
   {
      if (iRet == 1)
         sprintf(pOutBase->Upd_Date, "%d", lLastTaxFileDate);
      else
         sprintf(pOutDelq->Upd_Date, "%d", lLastTaxFileDate);
   }

   return iRet;
}

/*************************** TC_ParseTaxBase_S1() *****************************
 *
 * Special version for SOL where we generate BillNum using APN and occurence number
 * For delq record, use delq year as is.  Do not subtract 1.
 *
 * Return < 0 if record is not good or no tax
 * 2 = Delq/Redeem
 * 1 = Reg tax bill (include secured/supplemental, refund/due)
 *
 ******************************************************************************/

int TC_ParseTaxBase_S1(char *pOutbuf, char *pInbuf, char cDelim)
{
   static   char sLastApn[20];
   static   int  iCnt=0;

   char     acTmp[256];
   int      iRet=0, iTmp, iFmt;
   double	dTmp, dTax1, dTax2, dPen1, dPen2, dTaxTotal;
   TAXBASE  *pOutBase = (TAXBASE *)pOutbuf;
   TAXDELQ  *pOutDelq = (TAXDELQ *)pOutbuf;

   // Parse input tax data
   iTokens = ParseStringIQ(pInbuf, cDelim, 128, apTokens);
   if (iTokens < TC_NET_VAL)
   {
      LogMsg("***** Error: bad Tax record for APN=%.*s (#tokens=%d)", iApnLen, pInbuf, iTokens);
      return -1;
   }

   // Clear output buffer
   memset(pOutBase, 0, sizeof(TAXBASE));
#ifdef _DEBUG
   //if (!memcmp(apTokens[TC_APN_S], "0038190100", 10))
   //   iTmp = 0;
#endif
   
   if (!memcmp(apTokens[TC_ROLL_CAT], "SEC", 3))
   {
      pOutBase->isSecd[0] = '1';
   } else if (!memcmp(apTokens[TC_ROLL_CAT], "SUP", 3))
   {
      pOutBase->isSupp[0] = '1';
   } else if (*apTokens[TC_ROLL_CAT] < '0' || *apTokens[TC_TAX_YR] < '0')
   {
      return -2;
   }

   // Tax Year
   iTaxYear = atol(apTokens[TC_TAX_YR]);
   if (iTaxYear < 1900)
   {
      LogMsg("*** WARNING: Bad tax year APN=%s.   Ignore!", apTokens[TC_APN_S]);
      return -2;
   }

   // Do not take future year
   if (iTaxYear > lTaxYear)
      return -1;

   // Verify date format
   if (*(apTokens[TC_DUE_PAID_DT2]+2) == '/' && strlen(apTokens[TC_DUE_PAID_DT2]) == 10)
      iFmt = MM_DD_YYYY_1;
   else
      iFmt = iDateFmt;

   // APN
   strcpy(pOutBase->Apn, apTokens[TC_APN_S]);

   // Check for DELQ
   if (!memcmp(apTokens[TC_PAID_STAT1], "DEL", 3) && iTaxYear <= lLienYear)
   {
      if (iTokens >= TC_DEF_DATE && *apTokens[TC_DEF_DATE] > ' ' && dateConversion(apTokens[TC_DEF_DATE], acTmp, iFmt))
      {
         strcpy(pOutDelq->Def_Date, acTmp);
         sprintf(pOutDelq->TaxYear, "%.4s", acTmp);
      } else
      {
         sprintf(pOutDelq->TaxYear, "%d", iTaxYear+1);
         sprintf(pOutDelq->Def_Date, "%d", iTaxYear);
      }

      // Default amt
      dTaxTotal = atof(apTokens[TC_TOTAL_DUE]);
      if (dTaxTotal > 0.0)
      {
         iTmp = sprintf(pOutDelq->Def_Amt, "%.2f", dTaxTotal);
         pOutDelq->isDelq[0] = '1';

         // Paid amt
         dTmp = atof(apTokens[TC_TOTAL_PAID]);
         if (dTmp > 0.0)
         {
            iTmp = sprintf(pOutDelq->Red_Amt, "%.2f", dTmp);
            if (dTaxTotal == dTmp)
            {
               pOutDelq->DelqStatus[0] = TAX_STAT_REDEEMED;       // Redeemed
               pOutDelq->isDelq[0] = '0';
               if (iTokens >= TC_RED_DATE)
               {
                  if (*apTokens[TC_RED_DATE] >= '0' && dateConversion(apTokens[TC_RED_DATE], acTmp, iFmt))
                     strcpy(pOutDelq->Red_Date, acTmp);
               }
            } else if (dTaxTotal > dTmp)
               pOutDelq->DelqStatus[0] = TAX_STAT_ONPAYMENT;      // On payment plan
         } else
         {
            pOutDelq->DelqStatus[0] = TAX_STAT_UNPAID;            // Unpaid
         }
      }
      iRet = 2;
   } else if (!memcmp(apTokens[TC_PAID_STAT1], "RED", 3))
   {
      pOutDelq->DelqStatus[0] = TAX_STAT_REDEEMED;                // Redeemed
      pOutDelq->isDelq[0] = '0';

      if (iTokens >= TC_RED_DATE && *apTokens[TC_RED_DATE] > ' ' && dateConversion(apTokens[TC_RED_DATE], acTmp, iFmt))
         strcpy(pOutDelq->Red_Date, acTmp);
      else if (*apTokens[TC_DUE_PAID_DT1] > ' ' && dateConversion(apTokens[TC_DUE_PAID_DT1], acTmp, iFmt))
         strcpy(pOutDelq->Red_Date, acTmp);
            
      if (*apTokens[TC_DEF_DATE] > ' ' && dateConversion(apTokens[TC_DEF_DATE], acTmp, iFmt))
      {
         strcpy(pOutDelq->Def_Date, acTmp);
         iTmp = atoin(acTmp, 4);
         sprintf(pOutDelq->TaxYear, "%d", iTmp-1);
      } 

      // Paid amt
      dTmp = atof(apTokens[TC_TOTAL_PAID]);
      if (dTmp > 0.0)
      {
         iTmp = sprintf(pOutDelq->Red_Amt, "%.2f", dTmp);
         if (iTokens >= TC_RED_DATE)
         {
            if (*apTokens[TC_RED_DATE] >= '0' && dateConversion(apTokens[TC_RED_DATE], acTmp, iFmt))
               strcpy(pOutDelq->Red_Date, acTmp);
         }
      }

      iRet = 2;
   } else
   {
      // Tax Year - ignore if it's not current year
      if (lTaxYear > 0 && iTaxYear != lTaxYear && pOutBase->isSecd[0] == '1')
         return 0;

      // Tax Year
      sprintf(pOutBase->TaxYear, "%d", iTaxYear);

      // Save APN
      if (memcmp(sLastApn, pOutBase->Apn, iApnLen))
      {
         strcpy(sLastApn, apTokens[TC_APN_S]);
         iCnt = 1;
      } else
         iCnt++;

      // BillNum
      if (pOutBase->isSecd[0] == '1')
         sprintf(pOutBase->BillNum, "%s-01", sLastApn);
      else
         sprintf(pOutBase->BillNum, "%s-02", sLastApn);

      // BillType
      if (pOutBase->isSecd[0] == '1')
         pOutBase->BillType[0] = BILLTYPE_SECURED;
      else if (pOutBase->isSupp[0] == '1')
         pOutBase->BillType[0] = BILLTYPE_SECURED_SUPPL;
      else
         pOutBase->BillType[0] = '?';

      // TRA
      iTmp = atol(apTokens[TC_TRA]);
      if (iTmp > 0)
         sprintf(pOutBase->TRA, "%0.6d", iTmp);

      // Check for Tax amount
      dTax1 = dTax2 = 0.0;
      if (*apTokens[TC_TAX_DUE1] > ' ')
         dTax1 = atof(apTokens[TC_TAX_DUE1]);

      if (*apTokens[TC_TAX_DUE2] > ' ')
         dTax2 = atof(apTokens[TC_TAX_DUE2]);

      if (dTax1)
         sprintf(pOutBase->TaxAmt1, "%.2f", dTax1);
      if (dTax2)
         sprintf(pOutBase->TaxAmt2, "%.2f", dTax2);

      if (*apTokens[TC_TOTAL_TAX] > ' ')
         dTaxTotal = atof(apTokens[TC_TOTAL_TAX]);
      else
         dTaxTotal = dTax1+dTax2;

      if (dTaxTotal)
         sprintf(pOutBase->TotalTaxAmt, "%.2f", dTaxTotal);

      // Penalty
      dPen1 = atof(apTokens[TC_PEN_DUE1]);
      if (dPen1 > 0.0)
         sprintf(pOutBase->PenAmt1, "%.2f", dPen1);
      dPen2 = atof(apTokens[TC_PEN_DUE2]);
      if (dPen2 > 0.0)
         sprintf(pOutBase->PenAmt2, "%.2f", dPen2);

      // INST2 - PAID/UNPAID/PENDING/DUE/CANCELED/OUTSTANDING/REFUND PAID/REFUND DUE
      if (!memcmp(apTokens[TC_PAID_STAT2], "PA", 2))
      {
         InstDueDate(pOutBase->DueDate2, 2, iTaxYear);
         strcpy(pOutBase->PaidAmt2, pOutBase->TaxAmt2);
         if (*apTokens[TC_DUE_PAID_DT2] >= '0' && dateConversion(apTokens[TC_DUE_PAID_DT2], acTmp, iFmt))
            strcpy(pOutBase->PaidDate2, acTmp);
         else if (*apTokens[TC_DUE_PAID_DT1] >= '0' && dateConversion(apTokens[TC_DUE_PAID_DT1], acTmp, iFmt))
         {
            // Inst1 & Inst2 may be paid at the same time
            strcpy(pOutBase->PaidDate2, acTmp);                
         } else
            LogMsg("*** Bad PaidDate 2: %s [%s]", apTokens[TC_DUE_PAID_DT2], apTokens[TC_APN_S]);

         pOutBase->Inst2Status[0] = TAX_BSTAT_PAID;
         pOutBase->Inst1Status[0] = TAX_BSTAT_PAID;
      } else if (!memcmp(apTokens[TC_PAID_STAT2], "CA", 2))    // Cancel
         pOutBase->Inst2Status[0] = TAX_BSTAT_CANCEL;
      else if (!memcmp(apTokens[TC_PAID_STAT2], "REF", 2))     // Refund
      {
         if (*apTokens[TC_AMT_PAID2] > ' ')
         {
            strcpy(pOutBase->PaidAmt2, apTokens[TC_AMT_PAID2]);
            if (*apTokens[TC_DUE_PAID_DT2] >= '0' && dateConversion(apTokens[TC_DUE_PAID_DT2], acTmp, iFmt, iTaxYear))
               strcpy(pOutBase->PaidDate2, acTmp);
            else
               LogMsg("*** Bad RefDate2: %s [%s]", apTokens[TC_DUE_PAID_DT2], apTokens[TC_APN_S]);
         }
         pOutBase->Inst2Status[0] = TAX_BSTAT_REFUND;
      } else if (!dTax2)                                         // No tax
         pOutBase->Inst2Status[0] = TAX_BSTAT_NOTAX;
      else
      {
         if (*apTokens[TC_DUE_PAID_DT2] >= '0' && dateConversion(apTokens[TC_DUE_PAID_DT2], acTmp, iFmt))
            strcpy(pOutBase->DueDate2, acTmp);
         //else if (pOutBase->isSupp[0] == '1')
         //   strcpy(pOutBase->DueDate2, "20170630");
         else
            InstDueDate(pOutBase->DueDate2, 2, iTaxYear);

         if (ChkDueDate(2, pOutBase->DueDate2))
            pOutBase->Inst2Status[0] = TAX_BSTAT_PASTDUE;
         else
            pOutBase->Inst2Status[0] = TAX_BSTAT_UNPAID;
      } 

      // Verify date format
      if (*(apTokens[TC_DUE_PAID_DT1]+2) == '/' && strlen(apTokens[TC_DUE_PAID_DT1]) == 10)
         iFmt = MM_DD_YYYY_1;
      else
         iFmt = iDateFmt;

      // Paid Inst1
      if (!memcmp(apTokens[TC_PAID_STAT1], "PA", 2))
      {  
         InstDueDate(pOutBase->DueDate1, 1, iTaxYear);
         strcpy(pOutBase->PaidAmt1, pOutBase->TaxAmt1);
         if (*apTokens[TC_DUE_PAID_DT1] >= '0' && dateConversion(apTokens[TC_DUE_PAID_DT1], acTmp, iFmt))
            strcpy(pOutBase->PaidDate1, acTmp);
         else if (pOutBase->PaidDate2[0] >= '0')
            // Sometimes when both installments are paid at the same time, people only keys in date2
            strcpy(pOutBase->PaidDate1, pOutBase->PaidDate2);
         else 
            LogMsg("*** Bad PaidDate 1: %s [%s]", apTokens[TC_DUE_PAID_DT1], apTokens[TC_APN_S]);

         pOutBase->Inst1Status[0] = TAX_BSTAT_PAID;
      } else if (!memcmp(apTokens[TC_PAID_STAT1], "CA", 2))    // Cancel
         pOutBase->Inst1Status[0] = TAX_BSTAT_CANCEL;
      else if (!memcmp(apTokens[TC_PAID_STAT1], "REF", 2))     // Refund
      {
         pOutBase->Inst1Status[0] = TAX_BSTAT_REFUND;
         if (*apTokens[TC_AMT_PAID1] > ' ')
         {
            strcpy(pOutBase->PaidAmt1, apTokens[TC_AMT_PAID1]);
            if (*apTokens[TC_DUE_PAID_DT1] >= '0' && dateConversion(apTokens[TC_DUE_PAID_DT1], acTmp, iFmt, iTaxYear))
               strcpy(pOutBase->PaidDate1, acTmp);
            else
               LogMsg("*** Bad RefDate1: %s [%s]", apTokens[TC_DUE_PAID_DT1], apTokens[TC_APN_S]);
         }
      } else if (!dTax1)                                         // No tax
         pOutBase->Inst1Status[0] = TAX_BSTAT_NOTAX;
      else 
      {
         if (*apTokens[TC_DUE_PAID_DT1] >= '0' && dateConversion(apTokens[TC_DUE_PAID_DT1], acTmp, iFmt))
            strcpy(pOutBase->DueDate1, acTmp);
         //else if (pOutBase->isSupp[0] == '1')
         //   strcpy(pOutBase->DueDate1, "20170228");
         else
            InstDueDate(pOutBase->DueDate1, 1, iTaxYear);

         if (ChkDueDate(1, pOutBase->DueDate1))
            pOutBase->Inst1Status[0] = TAX_BSTAT_PASTDUE;
         else
            pOutBase->Inst1Status[0] = TAX_BSTAT_UNPAID;
      } 

      // Fee Paid
      dTmp = atof(apTokens[TC_OTHER_FEES]);
      if (dTmp > 0.0)
         iTmp = sprintf(pOutBase->TotalFees, "%.2f", dTmp);

      // Calculate unpaid amt
      dTmp = atof(apTokens[TC_TOTAL_DUE]);
      if (dTmp > 0.0)
         iTmp = sprintf(pOutBase->TotalDue, "%.2f", dTmp);

      iRet = 1;
   }

   // Update date
   if (iTokens > TC_UPD_DATE && *apTokens[TC_UPD_DATE] > ' ' && dateConversion(apTokens[TC_UPD_DATE], acTmp, MM_DD_YYYY_1))
   {
      if (iRet == 1)
         strcpy(pOutBase->Upd_Date, acTmp);
      else
         strcpy(pOutDelq->Upd_Date, acTmp);
   } else
   {
      if (iRet == 1)
         sprintf(pOutBase->Upd_Date, "%d", lLastTaxFileDate);
      else
         sprintf(pOutDelq->Upd_Date, "%d", lLastTaxFileDate);
   }

   return iRet;
}

/*************************** TC_ParseTaxBase_S2() *****************************
 *
 * Special version for TUL where we generate BillNum using APN and occurence number
 * DELQ record has separate TaxAmt, Penalty, and other fee.  Use TotalDue for DefaultAmt.
 *
 * Return < 0 if record is not good or no tax
 *
 ******************************************************************************/

int TC_ParseTaxBase_S2(char *pOutbuf, char *pInbuf, char cDelim)
{
   static   char sLastApn[20];
   static   int  iCnt=0;

   char     acTmp[256];
   int      iRet=0, iTmp, iFmt;
   double	dTmp, dTax1, dTax2, dPen1, dPen2, dTaxTotal;
   TAXBASE  *pOutBase = (TAXBASE *)pOutbuf;
   TAXDELQ  *pOutDelq = (TAXDELQ *)pOutbuf;

   // Parse input tax data
   iTokens = ParseStringIQ(pInbuf, cDelim, 128, apTokens);
   if (iTokens < TC_PMT_PLAN)
   {
      LogMsg("***** Error: bad Tax record for APN=%.*s (#tokens=%d)", iApnLen, pInbuf, iTokens);
      return -1;
   }

   // Clear output buffer
   memset(pOutBase, 0, sizeof(TAXBASE));
#ifdef _DEBUG
   //if (!memcmp(apTokens[TC_APN_S], "0052455070", 10))
   //   iTmp = 0;
#endif
   
   if (!memcmp(apTokens[TC_ROLL_CAT], "SEC", 3))
   {
      pOutBase->isSecd[0] = '1';
   } else if (!memcmp(apTokens[TC_ROLL_CAT], "SUP", 3))
   {
      pOutBase->isSupp[0] = '1';
   } else if (*apTokens[TC_ROLL_CAT] < '0' || *apTokens[TC_TAX_YR] < '0')
   {
      return -2;
   }

   // Tax Year
   iTaxYear = atol(apTokens[TC_TAX_YR]);
   if (iTaxYear < 1900)
   {
      LogMsg("*** WARNING: Bad tax year APN=%s.   Ignore!", apTokens[TC_APN_S]);
      return -2;
   } 

   // Do not take future year
   if (iTaxYear > lTaxYear)
      return -1;


   // Verify date format
   if (*(apTokens[TC_DUE_PAID_DT2]+2) == '/' && strlen(apTokens[TC_DUE_PAID_DT2]) == 10)
      iFmt = MM_DD_YYYY_1;
   else
      iFmt = iDateFmt;

   // APN
   strcpy(pOutBase->Apn, apTokens[TC_APN_S]);

   // Check for DELQ
   if (!memcmp(apTokens[TC_PAID_STAT1], "DEL", 3) && iTaxYear <= lLienYear)
   {
      if (iTokens >= TC_DEF_DATE && *apTokens[TC_DEF_DATE] > ' ' && dateConversion(apTokens[TC_DEF_DATE], acTmp, iFmt))
      {
         strcpy(pOutDelq->Def_Date, acTmp);
         iTmp = atoin(acTmp, 4);
         sprintf(pOutDelq->TaxYear, "%d", iTmp-1);
      } else
      {
         sprintf(pOutDelq->Def_Date, "%d", iTaxYear+1);
         sprintf(pOutDelq->TaxYear, "%d", iTaxYear);
      }

      // Default amt
      // 10/25/2022 - TOTAL_TAX not included

      //dTaxTotal = atof(apTokens[TC_TOTAL_TAX]);
      //if (dTaxTotal > 0.0)
      //{
      //   dTmp = atof(apTokens[TC_TOTAL_DUE]);
      //   if (dTmp > 0.0)
      //   {
      //      iTmp = sprintf(pOutDelq->Def_Amt, "%.2f", dTmp);
      //      pOutDelq->DelqStatus[0] = TAX_STAT_UNPAID;            // Unpaid
      //      pOutDelq->isDelq[0] = '1';
      //
      //      // BillNum
      //      strcpy(pOutDelq->BillNum, apTokens[TC_BILL_NUM]);
      //   } else if (dTaxTotal > 0.0)
      //      iTmp = sprintf(pOutDelq->Def_Amt, "%.2f", dTaxTotal);
      //
      //   // Total Penalty
      //   dTmp = atof(apTokens[TC_TOTAL_PEN]);
      //   if (dTmp > 0.0)
      //      sprintf(pOutDelq->Pen_Amt, "%.2f", dTmp);
      //
      //   // Other fee
      //   dTmp = atof(apTokens[TC_OTHER_FEES]);
      //   if (dTmp > 0.0)
      //      sprintf(pOutDelq->Fee_Amt, "%.2f", dTmp);
      //
      //   // Paid amt
      //   dTmp = atof(apTokens[TC_TOTAL_PAID]);
      //   if (dTmp > 0.0)
      //   {
      //      iTmp = sprintf(pOutDelq->Red_Amt, "%.2f", dTmp);
      //      if (dTaxTotal == dTmp)
      //      {
      //         pOutDelq->DelqStatus[0] = TAX_STAT_REDEEMED;       // Redeemed
      //         pOutDelq->isDelq[0] = '0';
      //         if (iTokens >= TC_RED_DATE)
      //         {
      //            if (*apTokens[TC_RED_DATE] >= '0' && dateConversion(apTokens[TC_RED_DATE], acTmp, iFmt))
      //               strcpy(pOutDelq->Red_Date, acTmp);
      //         }
      //      } else if (dTaxTotal > dTmp)
      //         pOutDelq->DelqStatus[0] = TAX_STAT_ONPAYMENT;      // On payment plan
      //   }
      //}
      dTmp = atof(apTokens[TC_TOTAL_DUE]);
      if (dTmp > 0.0)
      {
         iTmp = sprintf(pOutDelq->Def_Amt, "%.2f", dTmp);
         pOutDelq->DelqStatus[0] = TAX_STAT_UNPAID;            // Unpaid
         pOutDelq->isDelq[0] = '1';

         // BillNum
         strcpy(pOutDelq->BillNum, apTokens[TC_BILL_NUM]);
      }
      iRet = 2;
   } else if (!memcmp(apTokens[TC_PAID_STAT1], "RED", 3))
   {
      pOutDelq->DelqStatus[0] = TAX_STAT_REDEEMED;       // Redeemed
      pOutDelq->isDelq[0] = '0';

      if (iTokens >= TC_RED_DATE && *apTokens[TC_RED_DATE] > ' ' && dateConversion(apTokens[TC_RED_DATE], acTmp, iFmt))
         strcpy(pOutDelq->Red_Date, acTmp);
      else if (*apTokens[TC_DUE_PAID_DT1] > ' ' && dateConversion(apTokens[TC_DUE_PAID_DT1], acTmp, iFmt))
         strcpy(pOutDelq->Red_Date, acTmp);
            
      if (*apTokens[TC_DEF_DATE] > ' ' && dateConversion(apTokens[TC_DEF_DATE], acTmp, iFmt))
      {
         strcpy(pOutDelq->Def_Date, acTmp);
         iTmp = atoin(acTmp, 4);
         sprintf(pOutDelq->TaxYear, "%d", iTmp-1);
      }

      // Paid amt
      dTmp = atof(apTokens[TC_TOTAL_PAID]);
      if (dTmp > 0.0)
      {
         iTmp = sprintf(pOutDelq->Red_Amt, "%.2f", dTmp);
         if (iTokens >= TC_RED_DATE)
         {
            if (*apTokens[TC_RED_DATE] >= '0' && dateConversion(apTokens[TC_RED_DATE], acTmp, iFmt))
               strcpy(pOutDelq->Red_Date, acTmp);
         }
      }

      iRet = 2;
   } else
   {
      // Tax Year - ignore if it's not current year
      if (lTaxYear > 0 && iTaxYear != lTaxYear && pOutBase->isSecd[0] == '1')
         return 0;

      // Tax Year
      sprintf(pOutBase->TaxYear, "%d", iTaxYear);

      // Save APN
      if (memcmp(sLastApn, pOutBase->Apn, iApnLen))
      {
         strcpy(sLastApn, apTokens[TC_APN_S]);
         iCnt = 1;
      } else
         iCnt++;

      // BillType-BillNum
      if (pOutBase->isSecd[0] == '1')
      {
         pOutBase->BillType[0] = BILLTYPE_SECURED;
         if (iCnt > 2)
            sprintf(pOutBase->BillNum, "%s-%.2d", apTokens[TC_APN_S], iCnt);
         else
            sprintf(pOutBase->BillNum, "%s-01", apTokens[TC_APN_S]);
      } else if (pOutBase->isSupp[0] == '1')
      {
         pOutBase->BillType[0] = BILLTYPE_SECURED_SUPPL;
         if (iCnt > 2)
            sprintf(pOutBase->BillNum, "%s-%.2d", apTokens[TC_APN_S], iCnt);
         else
            sprintf(pOutBase->BillNum, "%s-02", apTokens[TC_APN_S]);
      } else
         pOutBase->BillType[0] = '?';

      // TRA
      iTmp = atol(apTokens[TC_TRA]);
      if (iTmp > 0)
         sprintf(pOutBase->TRA, "%0.6d", iTmp);

      // Check for Tax amount
      dTax1 = dTax2 = 0.0;
      if (*apTokens[TC_TAX_DUE1] > ' ')
         dTax1 = atof(apTokens[TC_TAX_DUE1]);

      if (*apTokens[TC_TAX_DUE2] > ' ')
         dTax2 = atof(apTokens[TC_TAX_DUE2]);

      if (dTax1)
         sprintf(pOutBase->TaxAmt1, "%.2f", dTax1);
      if (dTax2)
         sprintf(pOutBase->TaxAmt2, "%.2f", dTax2);

      if (*apTokens[TC_TOTAL_TAX] > ' ')
         dTaxTotal = atof(apTokens[TC_TOTAL_TAX]);
      else
         dTaxTotal = dTax1+dTax2;

      if (dTaxTotal)
         sprintf(pOutBase->TotalTaxAmt, "%.2f", dTaxTotal);

      // Penalty
      dPen1 = atof(apTokens[TC_PEN_DUE1]);
      if (dPen1 > 0.0)
         sprintf(pOutBase->PenAmt1, "%.2f", dPen1);
      dPen2 = atof(apTokens[TC_PEN_DUE2]);
      if (dPen2 > 0.0)
         sprintf(pOutBase->PenAmt2, "%.2f", dPen2);

      // INST2 - PAID/UNPAID/PENDING/DUE/CANCELED/OUTSTANDING/REFUND PAID/REFUND DUE
      if (!memcmp(apTokens[TC_PAID_STAT2], "PA", 2))
      {
         InstDueDate(pOutBase->DueDate2, 2, iTaxYear);
         strcpy(pOutBase->PaidAmt2, pOutBase->TaxAmt2);
         if (*apTokens[TC_DUE_PAID_DT2] >= '0' && dateConversion(apTokens[TC_DUE_PAID_DT2], acTmp, iFmt))
            strcpy(pOutBase->PaidDate2, acTmp);
         else if (*apTokens[TC_DUE_PAID_DT1] >= '0' && dateConversion(apTokens[TC_DUE_PAID_DT1], acTmp, iFmt))
         {
            // Inst1 & Inst2 may be paid at the same time
            strcpy(pOutBase->PaidDate2, acTmp);                
         } else
            LogMsg("*** Bad PaidDate 2: %s [%s]", apTokens[TC_DUE_PAID_DT2], apTokens[TC_APN_S]);

         pOutBase->Inst2Status[0] = TAX_BSTAT_PAID;
         pOutBase->Inst1Status[0] = TAX_BSTAT_PAID;
      } else if (!memcmp(apTokens[TC_PAID_STAT2], "CA", 2))    // Cancel
         pOutBase->Inst2Status[0] = TAX_BSTAT_CANCEL;
      else if (!memcmp(apTokens[TC_PAID_STAT2], "REF", 2))     // Refund
         pOutBase->Inst2Status[0] = TAX_BSTAT_REFUND;
      else if (!dTax2)                                         // No tax
         pOutBase->Inst2Status[0] = TAX_BSTAT_NOTAX;
      else
      {
         if (*apTokens[TC_DUE_PAID_DT2] >= '0' && dateConversion(apTokens[TC_DUE_PAID_DT2], acTmp, iFmt))
            strcpy(pOutBase->DueDate2, acTmp);
         //else if (pOutBase->isSupp[0] == '1')
         //   strcpy(pOutBase->DueDate2, "20170630");
         else
            InstDueDate(pOutBase->DueDate2, 2, iTaxYear);

         if (ChkDueDate(2, pOutBase->DueDate2))
            pOutBase->Inst2Status[0] = TAX_BSTAT_PASTDUE;
         else
            pOutBase->Inst2Status[0] = TAX_BSTAT_UNPAID;
      } 

      // Verify date format
      if (*(apTokens[TC_DUE_PAID_DT1]+2) == '/' && strlen(apTokens[TC_DUE_PAID_DT1]) == 10)
         iFmt = MM_DD_YYYY_1;
      else
         iFmt = iDateFmt;

      // Paid Inst1
      if (!memcmp(apTokens[TC_PAID_STAT1], "PA", 2))
      {  
         InstDueDate(pOutBase->DueDate1, 1, iTaxYear);
         strcpy(pOutBase->PaidAmt1, pOutBase->TaxAmt1);
         if (*apTokens[TC_DUE_PAID_DT1] >= '0' && dateConversion(apTokens[TC_DUE_PAID_DT1], acTmp, iFmt))
            strcpy(pOutBase->PaidDate1, acTmp);
         else if (pOutBase->PaidDate2[0] >= '0')
            // Sometimes when both installments are paid at the same time, people only keys in date2
            strcpy(pOutBase->PaidDate1, pOutBase->PaidDate2);
         else 
            LogMsg("*** Bad PaidDate 1: %s [%s]", apTokens[TC_DUE_PAID_DT1], apTokens[TC_APN_S]);

         pOutBase->Inst1Status[0] = TAX_BSTAT_PAID;
      } else if (!memcmp(apTokens[TC_PAID_STAT1], "CA", 2))    // Cancel
         pOutBase->Inst1Status[0] = TAX_BSTAT_CANCEL;
      else if (!memcmp(apTokens[TC_PAID_STAT1], "REF", 2))     // Refund
         pOutBase->Inst1Status[0] = TAX_BSTAT_REFUND;
      else if (!dTax1)                                         // No tax
         pOutBase->Inst1Status[0] = TAX_BSTAT_NOTAX;
      else 
      {
         if (*apTokens[TC_DUE_PAID_DT1] >= '0' && dateConversion(apTokens[TC_DUE_PAID_DT1], acTmp, iFmt))
            strcpy(pOutBase->DueDate1, acTmp);
         //else if (pOutBase->isSupp[0] == '1')
         //   strcpy(pOutBase->DueDate1, "20170228");
         else
            InstDueDate(pOutBase->DueDate1, 1, iTaxYear);

         if (ChkDueDate(1, pOutBase->DueDate1))
            pOutBase->Inst1Status[0] = TAX_BSTAT_PASTDUE;
         else
            pOutBase->Inst1Status[0] = TAX_BSTAT_UNPAID;
      } 

      // Fee Paid
      dTmp = atof(apTokens[TC_OTHER_FEES]);
      if (dTmp > 0.0)
         iTmp = sprintf(pOutBase->TotalFees, "%.2f", dTmp);

      // Calculate unpaid amt
      dTmp = atof(apTokens[TC_TOTAL_DUE]);
      if (dTmp > 0.0)
         iTmp = sprintf(pOutBase->TotalDue, "%.2f", dTmp);

      iRet = 1;
   }

   // Update date
   if (iTokens > TC_UPD_DATE && *apTokens[TC_UPD_DATE] > ' ' && dateConversion(apTokens[TC_UPD_DATE], acTmp, MM_DD_YYYY_1))
   {
      if (iRet == 1)
         strcpy(pOutBase->Upd_Date, acTmp);
      else
         strcpy(pOutDelq->Upd_Date, acTmp);
   } else
   {
      if (iRet == 1)
         sprintf(pOutBase->Upd_Date, "%d", lLastTaxFileDate);
      else
         sprintf(pOutDelq->Upd_Date, "%d", lLastTaxFileDate);
   }

   return iRet;
}

/*************************** TC_ParseTaxBase_S3() *****************************
 *
 * Special version for SMX.  Use TotalDue for DefaultAmt.
 * Verify TaxAmt1 & TaxAmt2 for incomplete information (i.e. missing due/paid date).
 * Use TC_TAX_YR as delq year and default on 6/30 the year after
 *
 * Return < 0 if record is not good or no tax
 *
 ******************************************************************************/

int TC_ParseTaxBase_S3(char *pOutbuf, char *pInbuf, char cDelim)
{
   char     acTmp[256], bBillType;
   int      iRet=0, iTmp, iFmt;
   double	dTmp, dTax1, dTax2, dPen1, dPen2, dTaxTotal;
   TAXBASE  *pOutBase = (TAXBASE *)pOutbuf;
   TAXDELQ  *pOutDelq = (TAXDELQ *)pOutbuf;

#ifdef _DEBUG
   //if (!memcmp(pInbuf, "045202120", 8))
   //   iTmp = 0;
#endif
   // Parse input tax data
   iTokens = ParseStringIQ(pInbuf, cDelim, 128, apTokens);
   if (iTokens < TC_NET_VAL)
   {
      LogMsg("***** Error: bad Tax record for APN=%.*s (#tokens=%d)", iApnLen, pInbuf, iTokens);
      return -1;
   }

   iTaxYear = atol(apTokens[TC_TAX_YR]);
   if (iTaxYear > lTaxYear)
      return -1;

   // Clear output buffer
   memset(pOutBase, 0, sizeof(TAXBASE));

   // Default BillType SECURED ANNUAL
   bBillType = BILLTYPE_SECURED;
   pOutBase->isSecd[0] = '1';
   pOutBase->isSupp[0] = 0;
   if (!memcmp(apTokens[TC_ROLL_CAT], "SECURED CORRECTED", 10))
   {
      bBillType = BILLTYPE_ROLL_CORRECTION;
   } else if (!memcmp(apTokens[TC_ROLL_CAT], "SUP", 3))
   {
      pOutBase->isSecd[0] = 0;
      pOutBase->isSupp[0] = '1';
      bBillType = BILLTYPE_SECURED_SUPPL;
   } else if (!memcmp(apTokens[TC_ROLL_CAT], "UNSEC", 5))
   {
      pOutBase->isSecd[0] = 0;
      pOutBase->isSupp[0] = 0;
      bBillType = BILLTYPE_UNSECURED;
   } 
   pOutBase->BillType[0] = bBillType;

   // Verify date format
   if (strchr(apTokens[TC_DUE_PAID_DT2], '/'))
      iFmt = MM_DD_YYYY_1;
   else
      iFmt = iDateFmt;

   if (!memcmp(apTokens[TC_PAID_STAT1], "DEL", 3))
   {
      int iDelqYear;

      // Delq Year
      iDelqYear = atol(apTokens[TC_TAX_YR]) ;
      if (iDelqYear < 1900)
      {
         LogMsg("*** WARNING: No tax data APN=%s", apTokens[TC_APN_S]);
         return -2;
      }

      // Tax year delq
      sprintf(pOutDelq->TaxYear, "%.4d", iDelqYear);
      sprintf(pOutDelq->Def_Date, "%d0630", iDelqYear+1);

      // APN
      strcpy(pOutDelq->Apn, apTokens[TC_APN_S]);

      // BillNum
      if (*apTokens[TC_BILL_NUM] >= '0')
         strcpy(pOutDelq->Default_No, apTokens[TC_BILL_NUM]);
      else if (m_bUseFeePrcl)
         strcpy(pOutDelq->BillNum, apTokens[TC_FEE_PCL]);

      // Default amt
      dTaxTotal = atof(apTokens[TC_TOTAL_DUE]);
      if (dTaxTotal > 0.0)
      {
         sprintf(pOutDelq->Def_Amt, "%.2f", dTaxTotal);
         pOutDelq->isDelq[0] = '1';

         // Paid amt
         dTmp = atof(apTokens[TC_TOTAL_PAID]);
         if (dTmp > 0.0)
         {
            iTmp = sprintf(pOutDelq->Red_Amt, "%.2f", dTmp);
            if (dTaxTotal == dTmp)
            {
               pOutDelq->DelqStatus[0] = TAX_STAT_REDEEMED;       // Redeemed
               pOutDelq->isDelq[0] = '0';
               if (iTokens >= TC_DEF_DATE)
               {
                  if (strchr(apTokens[TC_RED_DATE], '/'))
                     iFmt = MM_DD_YYYY_1;
                  else
                     iTmp = iFmt;

                  if (*apTokens[TC_RED_DATE] >= '0' && dateConversion(apTokens[TC_RED_DATE], acTmp, iTmp))
                     strcpy(pOutDelq->Red_Date, acTmp);
               }
            } else if (dTaxTotal > dTmp)
               pOutDelq->DelqStatus[0] = TAX_STAT_ONPAYMENT;      // On payment plan
         } else
         {
            pOutDelq->DelqStatus[0] = TAX_STAT_UNPAID;            // Unpaid
         }
      }

      iRet = 2;
   } else
   {
      // Tax Year - ignore if it's not current year
      if (lTaxYear > 0 && iTaxYear != lTaxYear && pOutBase->isSecd[0] == '1')
         return 0;

      sprintf(pOutBase->TaxYear, "%d", iTaxYear);

      // APN
      strcpy(pOutBase->Apn, apTokens[TC_APN_S]);

      // BillNum
      if (*apTokens[TC_BILL_NUM] >= '0')
         strcpy(pOutBase->BillNum, apTokens[TC_BILL_NUM]);
      else if (m_bUseFeePrcl)
      {
         if (!pOutBase->isSecd[0] || pOutBase->isSecd[0] == '1')  
            sprintf(pOutBase->BillNum, "%s-01", apTokens[TC_APN_S]);    // Secured or Unsecured
         else
            sprintf(pOutBase->BillNum, "%s-02", apTokens[TC_APN_S]);    // Supplemental
      } 

      // TRA
      iTmp = atol(apTokens[TC_TRA]);
      if (iTmp > 0)
         sprintf(pOutBase->TRA, "%0.6d", iTmp);

      // Check for Tax amount
      dTax1 = dTax2 = 0.0;
      if (*apTokens[TC_TAX_DUE1] > ' ')
         dTax1 = atof(apTokens[TC_TAX_DUE1]);

      if (*apTokens[TC_TAX_DUE2] > ' ')
         dTax2 = atof(apTokens[TC_TAX_DUE2]);

      if (dTax1 > 0.0)
         sprintf(pOutBase->TaxAmt1, "%.2f", dTax1);
      if (dTax2 > 0.0)
         sprintf(pOutBase->TaxAmt2, "%.2f", dTax2);

      if (*apTokens[TC_TOTAL_TAX] > '0')
         dTaxTotal = atof(apTokens[TC_TOTAL_TAX]);
      else
         dTaxTotal = dTax1+dTax2;

      // Penalty
      dPen1 = atof(apTokens[TC_PEN_DUE1]);
      if (dPen1 > 0.0)
         sprintf(pOutBase->PenAmt1, "%.2f", dPen1);
      dPen2 = atof(apTokens[TC_PEN_DUE2]);
      if (dPen2 > 0.0)
         sprintf(pOutBase->PenAmt2, "%.2f", dPen2);

      // Paid/Due - SAC: PAID/UNPAID/PENDING/DUE/CANCELED/OUTSTANDING
      if (!memcmp(apTokens[TC_PAID_STAT2], "PA", 2))
      {
         InstDueDate(pOutBase->DueDate2, 2, iTaxYear);
         strcpy(pOutBase->PaidAmt2, pOutBase->TaxAmt2);
         if (*apTokens[TC_DUE_PAID_DT2] >= '0' && dateConversion(apTokens[TC_DUE_PAID_DT2], acTmp, iFmt))
            strcpy(pOutBase->PaidDate2, acTmp);
         else if (*apTokens[TC_DUE_PAID_DT1] >= '0' && dateConversion(apTokens[TC_DUE_PAID_DT1], acTmp, iFmt))
            strcpy(pOutBase->PaidDate2, acTmp);                // Inst1 & Inst2 may be paid at the same time
         else if (*apTokens[TC_DUE_PAID_DT2] < '0')
            return 0;
         else
            LogMsg("*** Bad PaidDate 2: %s [%s]", apTokens[TC_DUE_PAID_DT2], apTokens[TC_APN_S]);

         pOutBase->Inst2Status[0] = TAX_BSTAT_PAID;
         pOutBase->Inst1Status[0] = TAX_BSTAT_PAID;
      } else if (!memcmp(apTokens[TC_PAID_STAT2], "CA", 2))    // Cancel
         pOutBase->Inst2Status[0] = TAX_BSTAT_CANCEL;
      else if (!memcmp(apTokens[TC_PAID_STAT2], "DU", 2))      // Due
      {
         pOutBase->Inst2Status[0] = TAX_BSTAT_UNPAID;
         if (*apTokens[TC_DUE_PAID_DT2] >= '0' && dateConversion(apTokens[TC_DUE_PAID_DT2], acTmp, iFmt))
            strcpy(pOutBase->DueDate2, acTmp);
         else if (iTaxYear == 2019)
            strcpy(pOutBase->DueDate2, "20200504");             // Special case COVID-19
         else
            InstDueDate(pOutBase->DueDate2, 2, iTaxYear);
      } else if (!dTax2)                                        // No tax
         pOutBase->Inst2Status[0] = TAX_BSTAT_NOTAX;
      else
      {
         if (*apTokens[TC_DUE_PAID_DT2] >= '0' && dateConversion(apTokens[TC_DUE_PAID_DT2], acTmp, iFmt))
            strcpy(pOutBase->DueDate2, acTmp);
         else if (iTaxYear == 2019)
            strcpy(pOutBase->DueDate2, "20200504");             // Special case COVID-19
         else
            InstDueDate(pOutBase->DueDate2, 2, iTaxYear);

         if (!memcmp(apTokens[TC_PAID_STAT2], "LA", 2) || ChkDueDate(2, pOutBase->DueDate2))
            pOutBase->Inst2Status[0] = TAX_BSTAT_PASTDUE;
         else
            pOutBase->Inst2Status[0] = TAX_BSTAT_UNPAID;
      } 

      // Verify date format
      if (*(apTokens[TC_DUE_PAID_DT1]+2) == '/' && strlen(apTokens[TC_DUE_PAID_DT1]) == 10)
         iFmt = MM_DD_YYYY_1;
      else
         iFmt = iDateFmt;

      // Paid Inst1
      if (!memcmp(apTokens[TC_PAID_STAT1], "PA", 2))
      {  
         InstDueDate(pOutBase->DueDate1, 1, iTaxYear);
         strcpy(pOutBase->PaidAmt1, pOutBase->TaxAmt1);
         if (*apTokens[TC_DUE_PAID_DT1] >= '0' && dateConversion(apTokens[TC_DUE_PAID_DT1], acTmp, iFmt))
            strcpy(pOutBase->PaidDate1, acTmp);
         else if (pOutBase->PaidDate2[0] >= '0')
            // Sometimes when both installments are paid at the same time, people only keys in date2
            strcpy(pOutBase->PaidDate1, pOutBase->PaidDate2);
         else if (*apTokens[TC_DUE_PAID_DT1] < '0')
            return 0;
         else 
            LogMsg("*** Bad PaidDate 1: %s [%s]", apTokens[TC_DUE_PAID_DT1], apTokens[TC_APN_S]);

         pOutBase->Inst1Status[0] = TAX_BSTAT_PAID;
      } else if (!memcmp(apTokens[TC_PAID_STAT1], "CA", 2))    // Cancel
         pOutBase->Inst1Status[0] = TAX_BSTAT_CANCEL;
      else if (!memcmp(apTokens[TC_PAID_STAT1], "DU", 2))      // Due
      {
         pOutBase->Inst1Status[0] = TAX_BSTAT_UNPAID;
         if (*apTokens[TC_DUE_PAID_DT1] >= '0' && dateConversion(apTokens[TC_DUE_PAID_DT1], acTmp, iFmt))
            strcpy(pOutBase->DueDate1, acTmp);
         else
            InstDueDate(pOutBase->DueDate1, 1, iTaxYear);
      } else if (!dTax1)                                         // No tax
         pOutBase->Inst1Status[0] = TAX_BSTAT_NOTAX;
      else 
      {
         if (*apTokens[TC_DUE_PAID_DT1] >= '0' && dateConversion(apTokens[TC_DUE_PAID_DT1], acTmp, iFmt))
            strcpy(pOutBase->DueDate1, acTmp);
         else
            InstDueDate(pOutBase->DueDate1, 1, iTaxYear);

         // Late or past due
         if (!memcmp(apTokens[TC_PAID_STAT1], "LA", 2) || ChkDueDate(1, pOutBase->DueDate1))
            pOutBase->Inst1Status[0] = TAX_BSTAT_PASTDUE;
         else
            pOutBase->Inst1Status[0] = TAX_BSTAT_UNPAID;
      } 

      // Fee Paid
      dTmp = atof(apTokens[TC_OTHER_FEES]);
      if (dTmp > 0.0)
         iTmp = sprintf(pOutBase->TotalFees, "%.2f", dTmp);

      // Calculate unpaid amt
      dTmp = atof(apTokens[TC_TOTAL_DUE]);
      if (dTmp > 0.0)
         iTmp = sprintf(pOutBase->TotalDue, "%.2f", dTmp);

      // Verify missing items
      if (dTaxTotal > 0.0)
      {
         sprintf(pOutBase->TotalTaxAmt, "%.2f", dTaxTotal);
         if ((!dTax1 || !dTax2) && (dTaxTotal > (dTax1+dTax2)) && pOutBase->isSupp[0] != '1')
         {
            LogMsg("*** Recheck: %s - Amt1 or Amt2 is zero.  Set Amt1=Total-Amt2 or Amt2=Total-Amt1.", pOutBase->Apn);
            if (!dTax1)
            {
               dTax1 = dTaxTotal - dTax2;
               sprintf(pOutBase->TaxAmt1, "%.2f", dTax1);
               if (pOutBase->DueDate1[0] < '0')
                  InstDueDate(pOutBase->DueDate1, 1, iTaxYear);
            } else if (!dTax2)
            {
               dTax2 = dTaxTotal - dTax1;
               sprintf(pOutBase->TaxAmt2, "%.2f", dTax2);
               if (pOutBase->DueDate2[0] < '0')
                  InstDueDate(pOutBase->DueDate2, 2, iTaxYear);
            }
         }
      }

      iRet = 1;
   }

   // Update date
   if (iTokens > TC_UPD_DATE && *apTokens[TC_UPD_DATE] > ' ' && dateConversion(apTokens[TC_UPD_DATE], acTmp, MM_DD_YYYY_1))
   {
      if (iRet == 1)
         strcpy(pOutBase->Upd_Date, acTmp);
      else
         strcpy(pOutDelq->Upd_Date, acTmp);
   } else
   {
      if (iRet == 1)
         sprintf(pOutBase->Upd_Date, "%d", lLastTaxFileDate);
      else
         sprintf(pOutDelq->Upd_Date, "%d", lLastTaxFileDate);
   }

   return iRet;
}

/*************************** TC_ParseTaxBase_S4() ******************************
 *
 * DUE_PAID_DATE2 - SFX
 *
 * Return < 0 if record is not good or no tax
 *
 ******************************************************************************/

int TC_ParseTaxBase_S4(char *pOutbuf, char *pInbuf, char cDelim)
{
   char     acTmp[256], bBillType;
   int      iRet=0, iTmp, iFmt;
   double	dTmp, dTax1, dTax2, dPen1, dPen2, dTaxTotal;
   TAXBASE  *pOutBase = (TAXBASE *)pOutbuf;
   TAXDELQ  *pOutDelq = (TAXDELQ *)pOutbuf;

#ifdef _DEBUG
   //if (!memcmp(pInbuf, "3746 008", 8))
   //   iTmp = 0;
#endif
   // Parse input tax data
   iTokens = ParseStringIQ(pInbuf, cDelim, 128, apTokens);
   if (iTokens < TC_NET_VAL)
   {
      LogMsg("***** Error: bad Tax record for APN=%.*s (#tokens=%d)", iApnLen, pInbuf, iTokens);
      return -1;
   }

   iTaxYear = atol(apTokens[TC_TAX_YR]);
   if (iTaxYear > lTaxYear)
      return -1;

   // Clear output buffer
   memset(pOutBase, 0, sizeof(TAXBASE));

   // Default BillType SECURED ANNUAL
   bBillType = BILLTYPE_SECURED;
   pOutBase->isSecd[0] = '1';
   pOutBase->isSupp[0] = 0;
   if (!memcmp(apTokens[TC_ROLL_CAT], "SECURED CORRECTED", 10))
   {
      bBillType = BILLTYPE_ROLL_CORRECTION;
   } else if (!memcmp(apTokens[TC_ROLL_CAT], "SUP", 3))
   {
      pOutBase->isSecd[0] = 0;
      pOutBase->isSupp[0] = '1';
      bBillType = BILLTYPE_SECURED_SUPPL;
   } else if (!memcmp(apTokens[TC_ROLL_CAT], "UNSEC", 5))
   {
      pOutBase->isSecd[0] = 0;
      pOutBase->isSupp[0] = 0;
      bBillType = BILLTYPE_UNSECURED;
   } 
   pOutBase->BillType[0] = bBillType;

   // Verify date format
   if (strchr(apTokens[TC_DUE_PAID_DT2], '/'))
      iFmt = MM_DD_YYYY_1;
   else
      iFmt = iDateFmt;

   if (!memcmp(apTokens[TC_PAID_STAT1], "DEL", 3))
   {
      int iDelqYear;

      // Delq Year
      iDelqYear = atol(apTokens[TC_TAX_YR]) + 1;
      if (iDelqYear < 1900)
      {
         LogMsg("*** WARNING: No tax data APN=%s", apTokens[TC_APN_S]);
         return -2;
      }

      // Tax year delq
      sprintf(pOutDelq->TaxYear, "%.4d", iDelqYear-1);

      if (iTokens > TC_DEF_DATE && *apTokens[TC_DEF_DATE] > ' ')
      {
         if (strchr(apTokens[TC_DEF_DATE], '/'))
            iTmp = MM_DD_YYYY_1;
         else
            iTmp = iFmt;

         if (dateConversion(apTokens[TC_DEF_DATE], acTmp, iTmp))
         {
            strcpy(pOutDelq->Def_Date, acTmp);
            iTmp = atoin(acTmp, 4);
            sprintf(pOutDelq->TaxYear, "%d", iTmp-1);
         } else
            strcpy(pOutDelq->Def_Date, apTokens[TC_DEF_DATE]);
      } else
         sprintf(pOutDelq->Def_Date, "%d", iDelqYear);

      // APN
      strcpy(pOutDelq->Apn, apTokens[TC_APN_S]);

      // BillNum
      if (*apTokens[TC_BILL_NUM] >= '0')
         strcpy(pOutDelq->Default_No, apTokens[TC_BILL_NUM]);
      else if (m_bUseFeePrcl)
         strcpy(pOutDelq->BillNum, apTokens[TC_FEE_PCL]);

      // Default amt
      dTaxTotal = atof(apTokens[TC_TOTAL_DUE]);
      if (dTaxTotal > 0.0)
      {
         sprintf(pOutDelq->Def_Amt, "%.2f", dTaxTotal);
         pOutDelq->isDelq[0] = '1';

         // Paid amt
         dTmp = atof(apTokens[TC_TOTAL_PAID]);
         if (dTmp > 0.0)
         {
            iTmp = sprintf(pOutDelq->Red_Amt, "%.2f", dTmp);
            if (dTaxTotal == dTmp)
            {
               pOutDelq->DelqStatus[0] = TAX_STAT_REDEEMED;       // Redeemed
               pOutDelq->isDelq[0] = '0';
               if (iTokens >= TC_DEF_DATE)
               {
                  if (strchr(apTokens[TC_RED_DATE], '/'))
                     iFmt = MM_DD_YYYY_1;
                  else
                     iTmp = iFmt;

                  if (*apTokens[TC_RED_DATE] >= '0' && dateConversion(apTokens[TC_RED_DATE], acTmp, iTmp))
                     strcpy(pOutDelq->Red_Date, acTmp);
               }
            } else if (dTaxTotal > dTmp)
               pOutDelq->DelqStatus[0] = TAX_STAT_ONPAYMENT;      // On payment plan
         } else
         {
            pOutDelq->DelqStatus[0] = TAX_STAT_UNPAID;            // Unpaid
         }
      }

      iRet = 2;
   } else
   {
      // Tax Year - ignore if it's not current year
      if (lTaxYear > 0 && iTaxYear != lTaxYear && pOutBase->isSecd[0] == '1')
         return 0;

      sprintf(pOutBase->TaxYear, "%d", iTaxYear);

      // APN
      strcpy(pOutBase->Apn, apTokens[TC_APN_S]);

      // BillNum
      if (*apTokens[TC_BILL_NUM] >= '0')
         strcpy(pOutBase->BillNum, apTokens[TC_BILL_NUM]);
      else if (m_bUseFeePrcl)
      {
         if (!pOutBase->isSecd[0] || pOutBase->isSecd[0] == '1')  
            sprintf(pOutBase->BillNum, "%s-01", apTokens[TC_APN_S]);    // Secured or Unsecured
         else
            sprintf(pOutBase->BillNum, "%s-02", apTokens[TC_APN_S]);    // Supplemental
      } 

      // TRA
      vmemcpy(pOutBase->TRA,  apTokens[TC_TRA], 6);
      //iTmp = atol(apTokens[TC_TRA]);
      //if (iTmp > 0)
      //   sprintf(pOutBase->TRA, "%0.6d", iTmp);

      // Check for Tax amount
      dTax1 = dTax2 = 0.0;
      if (*apTokens[TC_TAX_DUE1] > ' ')
         dTax1 = atof(apTokens[TC_TAX_DUE1]);

      if (*apTokens[TC_TAX_DUE2] > ' ')
         dTax2 = atof(apTokens[TC_TAX_DUE2]);


      if (dTax1 > 0.0)
         sprintf(pOutBase->TaxAmt1, "%.2f", dTax1);
      if (dTax2 > 0.0)
         sprintf(pOutBase->TaxAmt2, "%.2f", dTax2);

      if (*apTokens[TC_TOTAL_TAX] > '0')
         dTaxTotal = atof(apTokens[TC_TOTAL_TAX]);
      else
         dTaxTotal = dTax1+dTax2;

      if (dTaxTotal > 0.0)
         sprintf(pOutBase->TotalTaxAmt, "%.2f", dTaxTotal);

      // Penalty
      dPen1 = atof(apTokens[TC_PEN_DUE1]);
      if (dPen1 > 0.0)
         sprintf(pOutBase->PenAmt1, "%.2f", dPen1);
      dPen2 = atof(apTokens[TC_PEN_DUE2]);
      if (dPen2 > 0.0)
         sprintf(pOutBase->PenAmt2, "%.2f", dPen2);

      // Paid/Due - SFX: PAID/UNPAID/PENDING/DUE/CANCELED/OUTSTANDING
      if (!memcmp(apTokens[TC_PAID_STAT2], "PA", 2))
      {
         InstDueDate(pOutBase->DueDate2, 2, iTaxYear);
         strcpy(pOutBase->PaidAmt2, pOutBase->TaxAmt2);
         if (*apTokens[TC_DUE_PAID_DT2] >= '0' && dateConversion(apTokens[TC_DUE_PAID_DT2], acTmp, iFmt, iTaxYear))
            strcpy(pOutBase->PaidDate2, acTmp);
         else if (*apTokens[TC_DUE_PAID_DT1] >= '0' && dateConversion(apTokens[TC_DUE_PAID_DT1], acTmp, iFmt))
            strcpy(pOutBase->PaidDate2, acTmp);                // Inst1 & Inst2 may be paid at the same time
         else if (*apTokens[TC_DUE_PAID_DT2] < '0')
            return 0;
         else
            LogMsg("*** Bad PaidDate 2: %s [%s]", apTokens[TC_DUE_PAID_DT2], apTokens[TC_APN_S]);

         pOutBase->Inst2Status[0] = TAX_BSTAT_PAID;
         pOutBase->Inst1Status[0] = TAX_BSTAT_PAID;
      } else if (!memcmp(apTokens[TC_PAID_STAT2], "CA", 2))    // Cancel
         pOutBase->Inst2Status[0] = TAX_BSTAT_CANCEL;
      else if (!dTax2)                                         // No tax
         pOutBase->Inst2Status[0] = TAX_BSTAT_NOTAX;
      else
      {
         if (*apTokens[TC_DUE_PAID_DT2] >= '0' && dateConversion(apTokens[TC_DUE_PAID_DT2], acTmp, iFmt, iTaxYear))
            strcpy(pOutBase->DueDate2, acTmp);
         else
            InstDueDate(pOutBase->DueDate2, 2, iTaxYear);

         if (!memcmp(apTokens[TC_PAID_STAT2], "LA", 2) || ChkDueDate(2, pOutBase->DueDate2))
            pOutBase->Inst2Status[0] = TAX_BSTAT_PASTDUE;
         else
            pOutBase->Inst2Status[0] = TAX_BSTAT_UNPAID;
      } 

      // Verify date format
      if (*(apTokens[TC_DUE_PAID_DT1]+2) == '/' && strlen(apTokens[TC_DUE_PAID_DT1]) == 10)
         iFmt = MM_DD_YYYY_1;
      else
         iFmt = iDateFmt;

      // Paid Inst1
      if (!memcmp(apTokens[TC_PAID_STAT1], "PA", 2))
      {  
         InstDueDate(pOutBase->DueDate1, 1, iTaxYear);
         strcpy(pOutBase->PaidAmt1, pOutBase->TaxAmt1);
         if (*apTokens[TC_DUE_PAID_DT1] >= '0' && dateConversion(apTokens[TC_DUE_PAID_DT1], acTmp, iFmt))
            strcpy(pOutBase->PaidDate1, acTmp);
         else if (pOutBase->PaidDate2[0] >= '0')
            // Sometimes when both installments are paid at the same time, people only keys in date2
            strcpy(pOutBase->PaidDate1, pOutBase->PaidDate2);
         else if (*apTokens[TC_DUE_PAID_DT1] < '0')
            return 0;
         else 
            LogMsg("*** Bad PaidDate 1: %s [%s]", apTokens[TC_DUE_PAID_DT1], apTokens[TC_APN_S]);

         pOutBase->Inst1Status[0] = TAX_BSTAT_PAID;
      } else if (!memcmp(apTokens[TC_PAID_STAT1], "CA", 2))    // Cancel
         pOutBase->Inst1Status[0] = TAX_BSTAT_CANCEL;
      else if (!dTax1)                                         // No tax
         pOutBase->Inst1Status[0] = TAX_BSTAT_NOTAX;
      else 
      {
         if (*apTokens[TC_DUE_PAID_DT1] >= '0' && dateConversion(apTokens[TC_DUE_PAID_DT1], acTmp, iFmt))
            strcpy(pOutBase->DueDate1, acTmp);
         //else if (pOutBase->isSupp[0] == '1')
         //   strcpy(pOutBase->DueDate1, "20170228");
         else
            InstDueDate(pOutBase->DueDate1, 1, iTaxYear);

         // Late or past due
         if (!memcmp(apTokens[TC_PAID_STAT1], "LA", 2) || ChkDueDate(1, pOutBase->DueDate1))
            pOutBase->Inst1Status[0] = TAX_BSTAT_PASTDUE;
         else
            pOutBase->Inst1Status[0] = TAX_BSTAT_UNPAID;
      } 

      // Fee Paid
      dTmp = atof(apTokens[TC_OTHER_FEES]);
      if (dTmp > 0.0)
         iTmp = sprintf(pOutBase->TotalFees, "%.2f", dTmp);

      // Calculate unpaid amt
      dTmp = atof(apTokens[TC_TOTAL_DUE]);
      if (dTmp > 0.0)
         iTmp = sprintf(pOutBase->TotalDue, "%.2f", dTmp);

      iRet = 1;
   }

   // Update date
   if (iTokens > TC_UPD_DATE && *apTokens[TC_UPD_DATE] > ' ' && dateConversion(apTokens[TC_UPD_DATE], acTmp, MM_DD_YYYY_1))
   {
      if (iRet == 1)
         strcpy(pOutBase->Upd_Date, acTmp);
      else
         strcpy(pOutDelq->Upd_Date, acTmp);
   } else
   {
      if (iRet == 1)
         sprintf(pOutBase->Upd_Date, "%d", lLastTaxFileDate);
      else
         sprintf(pOutDelq->Upd_Date, "%d", lLastTaxFileDate);
   }

   return iRet;
}

/*************************** TC_ParseTaxBase_S5() ******************************
 *
 * If PAID, paid date format is MMM_DD_YYYY (Feb 01, 2016). Otherwise MM_DD_YYYY_1 (04/10/2021)
 *
 * Return < 0 if record is not good or no tax
 *
 ******************************************************************************/

int TC_ParseTaxBase_S5(char *pOutbuf, char *pInbuf, char cDelim)
{
   char     acTmp[256], bBillType;
   int      iRet=0, iTmp, iFmt;
   double	dTmp, dTax1, dTax2, dPen1, dPen2, dTaxTotal;
   TAXBASE  *pOutBase = (TAXBASE *)pOutbuf;
   TAXDELQ  *pOutDelq = (TAXDELQ *)pOutbuf;

#ifdef _DEBUG
   //if (!memcmp(pInbuf, "00103217", 8))
   //   iTmp = 0;
#endif
   // Parse input tax data
   iTokens = ParseStringIQ(pInbuf, cDelim, 128, apTokens);
   if (iTokens < TC_NET_VAL)
   {
      LogMsg("***** Error: bad Tax record for APN=%.*s (#tokens=%d)", iApnLen, pInbuf, iTokens);
      return -1;
   }

   iTaxYear = atol(apTokens[TC_TAX_YR]);
   if (iTaxYear > lTaxYear)
      return -1;

   // Clear output buffer
   memset(pOutBase, 0, sizeof(TAXBASE));

   // Default BillType SECURED ANNUAL
   bBillType = BILLTYPE_SECURED;
   pOutBase->isSecd[0] = '1';
   pOutBase->isSupp[0] = 0;
   if (!memcmp(apTokens[TC_ROLL_CAT], "SECURED CORRECTED", 10))
   {
      bBillType = BILLTYPE_ROLL_CORRECTION;
   } else if (!memcmp(apTokens[TC_ROLL_CAT], "SUP", 3))
   {
      pOutBase->isSecd[0] = 0;
      pOutBase->isSupp[0] = '1';
      bBillType = BILLTYPE_SECURED_SUPPL;
   } else if (!memcmp(apTokens[TC_ROLL_CAT], "UNSEC", 5))
   {
      pOutBase->isSecd[0] = 0;
      pOutBase->isSupp[0] = 0;
      bBillType = BILLTYPE_UNSECURED;
   } 
   pOutBase->BillType[0] = bBillType;

   // Verify date format
   if (strchr(apTokens[TC_DUE_PAID_DT2], '/'))
      iFmt = MM_DD_YYYY_1;
   else
      iFmt = MMM_DD_YYYY;

   if (!memcmp(apTokens[TC_PAID_STAT1], "DEL", 3))
   {
      int iDelqYear;

      // Delq Year
      iDelqYear = atol(apTokens[TC_TAX_YR]) + 1;
      if (iDelqYear < 1900)
      {
         LogMsg("*** WARNING: No tax data APN=%s", apTokens[TC_APN_S]);
         return -2;
      }

      // Tax year delq
      sprintf(pOutDelq->TaxYear, "%.4d", iDelqYear-1);

      if (iTokens > TC_DEF_DATE && *apTokens[TC_DEF_DATE] > ' ')
      {
         if (strchr(apTokens[TC_DEF_DATE], '/'))
            iTmp = MM_DD_YYYY_1;
         else
            iTmp = iFmt;

         if (dateConversion(apTokens[TC_DEF_DATE], acTmp, iTmp))
         {
            strcpy(pOutDelq->Def_Date, acTmp);
            iTmp = atoin(acTmp, 4);
            sprintf(pOutDelq->TaxYear, "%d", iTmp-1);
         } else
            strcpy(pOutDelq->Def_Date, apTokens[TC_DEF_DATE]);
      } else
         sprintf(pOutDelq->Def_Date, "%d", iDelqYear);

      // APN
      strcpy(pOutDelq->Apn, apTokens[TC_APN_S]);

      // BillNum
      if (*apTokens[TC_BILL_NUM] >= '0')
         strcpy(pOutDelq->Default_No, apTokens[TC_BILL_NUM]);
      else if (m_bUseFeePrcl)
         strcpy(pOutDelq->BillNum, apTokens[TC_FEE_PCL]);

      // Default amt
      dTaxTotal = atof(apTokens[TC_TOTAL_DUE]);
      if (dTaxTotal > 0.0)
      {
         sprintf(pOutDelq->Def_Amt, "%.2f", dTaxTotal);
         pOutDelq->isDelq[0] = '1';

         // Paid amt
         dTmp = atof(apTokens[TC_TOTAL_PAID]);
         if (dTmp > 0.0)
         {
            iTmp = sprintf(pOutDelq->Red_Amt, "%.2f", dTmp);
            if (dTaxTotal == dTmp)
            {
               pOutDelq->DelqStatus[0] = TAX_STAT_REDEEMED;       // Redeemed
               pOutDelq->isDelq[0] = '0';
               if (iTokens >= TC_DEF_DATE)
               {
                  if (strchr(apTokens[TC_RED_DATE], '/'))
                     iFmt = MM_DD_YYYY_1;
                  else
                     iTmp = iFmt;

                  if (*apTokens[TC_RED_DATE] >= '0' && dateConversion(apTokens[TC_RED_DATE], acTmp, iTmp))
                     strcpy(pOutDelq->Red_Date, acTmp);
               }
            } else if (dTaxTotal > dTmp)
               pOutDelq->DelqStatus[0] = TAX_STAT_ONPAYMENT;      // On payment plan
         } else
         {
            pOutDelq->DelqStatus[0] = TAX_STAT_UNPAID;            // Unpaid
         }
      }

      iRet = 2;
   } else
   {
      // Tax Year - ignore if it's not current year
      if (lTaxYear > 0 && iTaxYear != lTaxYear && pOutBase->isSecd[0] == '1')
         return 0;

      sprintf(pOutBase->TaxYear, "%d", iTaxYear);

      // APN
      strcpy(pOutBase->Apn, apTokens[TC_APN_S]);

      // BillNum
      if (*apTokens[TC_BILL_NUM] >= '0')
         strcpy(pOutBase->BillNum, apTokens[TC_BILL_NUM]);
      else if (m_bUseFeePrcl)
      {
         if (!pOutBase->isSecd[0] || pOutBase->isSecd[0] == '1')  
            sprintf(pOutBase->BillNum, "%s-01", apTokens[TC_APN_S]);    // Secured or Unsecured
         else
            sprintf(pOutBase->BillNum, "%s-02", apTokens[TC_APN_S]);    // Supplemental
      } 

      // TRA
      iTmp = atol(apTokens[TC_TRA]);
      if (iTmp > 0)
         sprintf(pOutBase->TRA, "%0.6d", iTmp);

      // Check for Tax amount
      dTax1 = dTax2 = 0.0;
      if (*apTokens[TC_TAX_DUE1] > ' ')
         dTax1 = atof(apTokens[TC_TAX_DUE1]);

      if (*apTokens[TC_TAX_DUE2] > ' ')
         dTax2 = atof(apTokens[TC_TAX_DUE2]);


      if (dTax1 > 0.0)
         sprintf(pOutBase->TaxAmt1, "%.2f", dTax1);
      if (dTax2 > 0.0)
         sprintf(pOutBase->TaxAmt2, "%.2f", dTax2);

      if (*apTokens[TC_TOTAL_TAX] > '0')
         dTaxTotal = atof(apTokens[TC_TOTAL_TAX]);
      else
         dTaxTotal = dTax1+dTax2;

      if (dTaxTotal > 0.0)
         sprintf(pOutBase->TotalTaxAmt, "%.2f", dTaxTotal);

      // Penalty
      dPen1 = atof(apTokens[TC_PEN_DUE1]);
      if (dPen1 > 0.0)
         sprintf(pOutBase->PenAmt1, "%.2f", dPen1);
      dPen2 = atof(apTokens[TC_PEN_DUE2]);
      if (dPen2 > 0.0)
         sprintf(pOutBase->PenAmt2, "%.2f", dPen2);

      // Paid/Due - SFX: PAID/UNPAID/PENDING/DUE/CANCELED/OUTSTANDING
      if (!memcmp(apTokens[TC_PAID_STAT2], "PA", 2))
      {
         iFmt = MMM_DD_YYYY;
         InstDueDate(pOutBase->DueDate2, 2, iTaxYear);
         strcpy(pOutBase->PaidAmt2, pOutBase->TaxAmt2);
         if (*apTokens[TC_DUE_PAID_DT2] >= '0' && dateConversion(apTokens[TC_DUE_PAID_DT2], acTmp, iFmt))
            strcpy(pOutBase->PaidDate2, acTmp);
         else if (*apTokens[TC_DUE_PAID_DT1] >= '0' && dateConversion(apTokens[TC_DUE_PAID_DT1], acTmp, iFmt))
            strcpy(pOutBase->PaidDate2, acTmp);                // Inst1 & Inst2 may be paid at the same time
         else if (*apTokens[TC_DUE_PAID_DT2] < '0')
            return 0;
         else
            LogMsg("*** Bad PaidDate 2: %s [%s]", apTokens[TC_DUE_PAID_DT2], apTokens[TC_APN_S]);

         pOutBase->Inst2Status[0] = TAX_BSTAT_PAID;
         pOutBase->Inst1Status[0] = TAX_BSTAT_PAID;
      } else if (!memcmp(apTokens[TC_PAID_STAT2], "CA", 2))    // Cancel
         pOutBase->Inst2Status[0] = TAX_BSTAT_CANCEL;
      else if (!dTax2)                                         // No tax
         pOutBase->Inst2Status[0] = TAX_BSTAT_NOTAX;
      else
      {
         iFmt = MM_DD_YYYY_1;
         if (*apTokens[TC_DUE_PAID_DT2] >= '0' && dateConversion(apTokens[TC_DUE_PAID_DT2], acTmp, iFmt))
            strcpy(pOutBase->DueDate2, acTmp);
         else
            InstDueDate(pOutBase->DueDate2, 2, iTaxYear);

         if (!memcmp(apTokens[TC_PAID_STAT2], "LA", 2) || ChkDueDate(2, pOutBase->DueDate2))
            pOutBase->Inst2Status[0] = TAX_BSTAT_PASTDUE;
         else
            pOutBase->Inst2Status[0] = TAX_BSTAT_UNPAID;
      } 

      // Verify date format
      if (*(apTokens[TC_DUE_PAID_DT1]+2) == '/' && strlen(apTokens[TC_DUE_PAID_DT1]) == 10)
         iFmt = MM_DD_YYYY_1;
      else
         iFmt = iDateFmt;

      // Paid Inst1
      if (!memcmp(apTokens[TC_PAID_STAT1], "PA", 2))
      {  
         InstDueDate(pOutBase->DueDate1, 1, iTaxYear);
         strcpy(pOutBase->PaidAmt1, pOutBase->TaxAmt1);
         iFmt = MMM_DD_YYYY;
         if (*apTokens[TC_DUE_PAID_DT1] >= '0' && dateConversion(apTokens[TC_DUE_PAID_DT1], acTmp, iFmt))
            strcpy(pOutBase->PaidDate1, acTmp);
         else if (pOutBase->PaidDate2[0] >= '0')
            // Sometimes when both installments are paid at the same time, people only keys in date2
            strcpy(pOutBase->PaidDate1, pOutBase->PaidDate2);
         else if (*apTokens[TC_DUE_PAID_DT1] < '0')
            return 0;
         else 
            LogMsg("*** Bad PaidDate 1: %s [%s]", apTokens[TC_DUE_PAID_DT1], apTokens[TC_APN_S]);

         pOutBase->Inst1Status[0] = TAX_BSTAT_PAID;
      } else if (!memcmp(apTokens[TC_PAID_STAT1], "CA", 2))    // Cancel
         pOutBase->Inst1Status[0] = TAX_BSTAT_CANCEL;
      else if (!dTax1)                                         // No tax
         pOutBase->Inst1Status[0] = TAX_BSTAT_NOTAX;
      else 
      {
         if (*apTokens[TC_DUE_PAID_DT1] >= '0' && dateConversion(apTokens[TC_DUE_PAID_DT1], acTmp, iFmt))
            strcpy(pOutBase->DueDate1, acTmp);
         //else if (pOutBase->isSupp[0] == '1')
         //   strcpy(pOutBase->DueDate1, "20170228");
         else
            InstDueDate(pOutBase->DueDate1, 1, iTaxYear);

         // Late or past due
         if (!memcmp(apTokens[TC_PAID_STAT1], "LA", 2) || ChkDueDate(1, pOutBase->DueDate1))
            pOutBase->Inst1Status[0] = TAX_BSTAT_PASTDUE;
         else
            pOutBase->Inst1Status[0] = TAX_BSTAT_UNPAID;
      } 

      // Fee Paid
      dTmp = atof(apTokens[TC_OTHER_FEES]);
      if (dTmp > 0.0)
         iTmp = sprintf(pOutBase->TotalFees, "%.2f", dTmp);

      // Calculate unpaid amt
      dTmp = atof(apTokens[TC_TOTAL_DUE]);
      if (dTmp > 0.0)
         iTmp = sprintf(pOutBase->TotalDue, "%.2f", dTmp);

      iRet = 1;
   }

   // Update date
   if (iTokens > TC_UPD_DATE && *apTokens[TC_UPD_DATE] > ' ' && dateConversion(apTokens[TC_UPD_DATE], acTmp, MM_DD_YYYY_1))
   {
      if (iRet == 1)
         strcpy(pOutBase->Upd_Date, acTmp);
      else
         strcpy(pOutDelq->Upd_Date, acTmp);
   } else
   {
      if (iRet == 1)
         sprintf(pOutBase->Upd_Date, "%d", lLastTaxFileDate);
      else
         sprintf(pOutDelq->Upd_Date, "%d", lLastTaxFileDate);
   }

   return iRet;
}

/*************************** TC_LoadTaxBaseAndDelq ****************************
 *
 *
 ******************************************************************************/

int TC_LoadTaxBaseAndDelq(char *pCnty, char *pTaxFile, char *pBaseFile, char *pDelqFile)
{
   char     *pTmp, acBuf[2048], acRec[2048], sDelqYear[64], sApn[64], acR01File[_MAX_PATH];
   int      iRet, iParseType, iDrop=0;
   long     lBase=0, lCnt=0;
   FILE     *fdIn, *fdBase, *fdDelq, *fdR01;
   TAXBASE  *pTaxBase = (TAXBASE *)&acBuf[0];
   TAXDELQ  *pTaxDelq = (TAXDELQ *)&acBuf[0];

   // Open input file
   LogMsg("Open Tax file %s", pTaxFile);
   fdIn = fopen(pTaxFile, "r");
   if (fdIn == NULL)
   {
      LogMsg("***** Error opening Tax file: %s\n", pTaxFile);
      return -2;
   }  

   // Open R01 file
   fdR01 = NULL;
   iRet = GetIniString("Data", "RawFile", "", acRec, 128, acIniFile);
   sprintf(acR01File, acRec, pCnty, pCnty, "R01");
   LogMsg("Open R01 file %s", acR01File);
   if (_access(acR01File, 0))
      sprintf(acR01File, acRec, pCnty, pCnty, "S01");
   try {
      if (!_access(acR01File, 0))
         fdR01 = fopen(acR01File, "rb");
      else
         LogMsg("*** Missing input file: %s", acR01File);
   } catch (...)
   {
      LogMsg("***** Error opening R01 file: %s", acR01File);
   }

   // Open Base Output file
   LogMsg("Open Base output file %s", pBaseFile);
   fdBase = fopen(pBaseFile, "w");
   if (fdBase == NULL)
   {
      LogMsg("***** Error creating output file: %s\n", pBaseFile);
      return -3;
   }

   // Open Delq Output file
   LogMsg("Open Delq output file %s", pDelqFile);
   fdDelq = fopen(pDelqFile, "w");
   if (fdDelq == NULL)
   {
      LogMsg("***** Error creating output file: %s\n", pDelqFile);
      return -4;
   }
   iTaxDelq = 0;

   // Set which parser to use
   if (!_memicmp(pCnty, "SOL", 3))
      iParseType = 1;
   else if (!_memicmp(pCnty, "TUL", 3))
      iParseType = 2;
   else if (!_memicmp(pCnty, "SMX", 3))
      iParseType = 3;
   else if (!_memicmp(pCnty, "SFX", 3))
      iParseType = 4;
   else if (!_memicmp(pCnty, "ALA", 3))
      iParseType = 5;
   else
      iParseType = 0;

   // Skip header record - file is sorted, no header to skip
   //pTmp = fgets((char *)&acRec[0], 2048, fdIn);

   // Merge loop 
   while (!feof(fdIn))
   {
      pTmp = fgets((char *)&acRec[0], 2048, fdIn);
      if (!pTmp || *pTmp > '9')
         break;

#ifdef _DEBUG
      //if (!memcmp(acRec, "140680110", 9))
      //   iRet = 0;
#endif

      // Create new TAXBASE record
      if (iParseType == 1)       // SOL
         iRet = TC_ParseTaxBase_S1(acBuf, acRec, '|');
      else if (iParseType == 2)  // TUL
         iRet = TC_ParseTaxBase_S2(acBuf, acRec, '|');
      else if (iParseType == 3)  // SMX
         iRet = TC_ParseTaxBase_S3(acBuf, acRec, '|');
      else if (iParseType == 4)  // SFX
         iRet = TC_ParseTaxBase_S4(acBuf, acRec, '|');
      else if (iParseType == 5)  // ALA
         iRet = TC_ParseTaxBase_S5(acBuf, acRec, '|');
      else
         iRet = TC_ParseTaxBase(acBuf, acRec, '|');

      if (iRet == 1)
      {
         // Update TRA if not there yet
         if (pTaxBase->TRA[0] < '0' && fdR01)
            UpdateTRA(acBuf, fdR01);

         if (!strcmp(sApn, pTaxBase->Apn))
         {
            strcpy(pTaxBase->DelqYear, sDelqYear);
            pTaxBase->isDelq[0] = '1';
            sDelqYear[0] = 0;
         }
         Tax_CreateTaxBaseCsv(acRec, (TAXBASE *)&acBuf);
         fputs(acRec, fdBase);
         lBase++;
         sApn[0] = 0;
      } else if (iRet == 2)
      {
         Tax_CreateDelqCsv(acRec, (TAXDELQ *)&acBuf);
         fputs(acRec, fdDelq);
         iTaxDelq++;

         // Save DelqYear
         strcpy(sDelqYear, pTaxDelq->TaxYear);
         strcpy(sApn, pTaxDelq->Apn);
      } else
         iDrop++;

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdIn)
      fclose(fdIn);
   if (fdBase)
      fclose(fdBase);
   if (fdDelq)
      fclose(fdDelq);
   if (fdR01)
      fclose(fdR01);

   LogMsg("Total records processed: %u", lCnt);
   LogMsg("Total records dropped:   %u", iDrop);
   LogMsg("Total Base output:       %u", lBase);
   LogMsg("Total Delq output:       %u", iTaxDelq);

   printf("\nTotal output records: %u", lCnt);

   if (lBase > 0)
      iRet = 0;
   else
      iRet = -10;

   return iRet;
}

/*************************** TC_ParseTaxDist() ********************************
 *
 *
 ******************************************************************************/

int TC_ParseTaxDist(char *pTaxDetail, char *pTaxAgency, char *pInbuf, char cDelim)
{
   int      iTmp;
   double	dTax, dRate;

   TAXDETAIL *pOutDetail = (TAXDETAIL *)pTaxDetail;
   TAXAGENCY *pOutAgency = (TAXAGENCY *)pTaxAgency;

   // Parse input tax data
   iTokens = ParseStringIQ(pInbuf, cDelim, MAX_FLD_TOKEN, apTokens);
   if (iTokens < TC_TAX_BILLNUM)
   {
      LogMsg("***** Error: bad Tax record for APN=%.*s (#tokens=%d)", iApnLen, pInbuf, iTokens);
      return -1;
   }

   // Clear output buffer
   memset(pTaxDetail, 0, sizeof(TAXDETAIL));
   memset(pTaxAgency, 0, sizeof(TAXAGENCY));

#ifdef _DEBUG
   //if (!memcmp(apTokens[TC_APN_S], "002061440", 9))
   //   iTmp = 0;
#endif
   
   // APN
   strcpy(pOutDetail->Apn, apTokens[TC_APN_S]);

   // BillNum
   if (*apTokens[TC_TAX_BILLNUM] >= '0')
      strcpy(pOutDetail->BillNum, remCrLf(apTokens[TC_TAX_BILLNUM]));
   else if (iTokens > TC_TAX_ROLLCAT && m_bUseFeePrcl)
   {
      if (!memcmp(apTokens[TC_TAX_ROLLCAT], "SE", 2) || !memcmp(apTokens[TC_TAX_ROLLCAT], "UNSE", 4))
         sprintf(pOutDetail->BillNum, "%s-01", apTokens[TC_APN_S]);
      else if (!memcmp(apTokens[TC_TAX_ROLLCAT], "SU", 2))
         sprintf(pOutDetail->BillNum, "%s-02", apTokens[TC_APN_S]);
   } 

   // Tax Year
   sprintf(pOutDetail->TaxYear, "%d", iTaxYear);

   // Tax Rate - In SOL, FIXED amt known to be varied by community
   //if (!_memicmp(apTokens[TC_TAX_RATE], "FIX", 3))
   //   strcpy(pOutAgency->TaxAmt, pOutDetail->TaxAmt);
   //else 
   if (*apTokens[TC_TAX_RATE] > ' ')
   {
      dRate = atof(apTokens[TC_TAX_RATE]);
      // Tax rate - may have negative value
      if (dRate)
      {
         strcpy(pOutDetail->TaxRate, apTokens[TC_TAX_RATE]);
         if (bInclTaxRate)
            strcpy(pOutAgency->TaxRate, apTokens[TC_TAX_RATE]);
      }
   } else
      dRate = 0.0;

   // Tax amount
   dTax = atof(apTokens[TC_TAX_TOTAL]);
   if (dRate < 0.0 && dTax > 0.0)
      dTax = -dTax;                             // Quick fix
   sprintf(pOutDetail->TaxAmt, "%.2f", dTax);

   // Tax Agency - remove special char
   iTmp = remChar(apTokens[TC_TAX_DIST], '\\');
   strcpy(pOutDetail->TaxDesc, apTokens[TC_TAX_DIST]);
   strcpy(pOutAgency->Agency, apTokens[TC_TAX_DIST]);

   // Tax code
   if (iNumTaxDist > 1)
   {
      replStrAll(apTokens[TC_TAX_DIST], "&QUOT;", "\"");
      replStr(apTokens[TC_TAX_DIST], "&#39;", "'");
      TAXAGENCY *pAgency = findTaxDist(apTokens[TC_TAX_DIST]);
      if (pAgency)
      {
#ifdef _DEBUG
         if (*apTokens[TC_TAX_CODE] > ' ' && strcmp(apTokens[TC_TAX_CODE], pAgency->Code))
            LogMsg("??? Tax code mismatched.  Please research the problem APN=%s, TaxCode=%s <> %s (%s)", 
            pOutDetail->Apn, apTokens[TC_TAX_CODE], pAgency->Code, apTokens[TC_TAX_DIST]);
#endif
         strcpy(pOutDetail->TaxCode, pAgency->Code);
         strcpy(pOutAgency->Code, pAgency->Code);
         strcpy(pOutAgency->Agency, pAgency->Agency);
         strcpy(pOutAgency->Phone, pAgency->Phone);
         pOutAgency->TC_Flag[0] = pAgency->TC_Flag[0];
         pOutDetail->TC_Flag[0] = pAgency->TC_Flag[0];
      } else if (*apTokens[TC_TAX_DIST] > ' ')
      {
         char sDesc[256], sCode[32];

         // Update dist table
         if (*apTokens[TC_TAX_CODE] > ' ')
         {
            strcpy(sCode, apTokens[TC_TAX_CODE]);
            asTaxDist[iNumTaxDist].iCodeLen = strlen(sCode);
            asTaxDist[iNumTaxDist].TC_Flag[0] = '0';
            pOutAgency->TC_Flag[0] = '0';
         } else
         {
            asTaxDist[iNumTaxDist].iCodeLen = sprintf(sCode, "X%.3d", ++iNewCodeIdx);
            asTaxDist[iNumTaxDist].TC_Flag[0] = '1';
            pOutAgency->TC_Flag[0] = '1';
         }
         asTaxDist[iNumTaxDist].iNameLen = sprintf(sDesc, "%s", apTokens[TC_TAX_DIST]);
         strcpy(asTaxDist[iNumTaxDist].Agency, sDesc);
         strcpy(asTaxDist[iNumTaxDist].Code, sCode);
         strcpy(asTaxDist[iNumTaxDist].Phone, apTokens[TC_TAX_PHONE]);
         iNumTaxDist++;
         iNewAgency++;

         strcpy(pOutAgency->Agency, sDesc);
         strcpy(pOutAgency->Code, sCode);
         LogMsg("*** New Tax Agency: %s, TaxCode: %s", apTokens[TC_TAX_DIST], sCode);
      } else if (*apTokens[TC_TAX_CODE] > ' ')
      {
         char sDesc[256], sCode[32];

         pAgency = findTaxAgency(apTokens[TC_TAX_CODE]);
         if (pAgency)
         {
            strcpy(pOutDetail->TaxCode, pAgency->Code);
            strcpy(pOutAgency->Code, pAgency->Code);
            strcpy(pOutAgency->Agency, pAgency->Agency);
            strcpy(pOutAgency->Phone, pAgency->Phone);
            pOutAgency->TC_Flag[0] = pAgency->TC_Flag[0];
            pOutDetail->TC_Flag[0] = pAgency->TC_Flag[0];
         } else
         {
            // Update dist table
            strcpy(sCode, apTokens[TC_TAX_CODE]);
            asTaxDist[iNumTaxDist].iCodeLen = strlen(sCode);
            asTaxDist[iNumTaxDist].iNameLen = sprintf(sDesc, "Special Assessment %s", sCode);
            strcpy(asTaxDist[iNumTaxDist].Agency, sDesc);
            strcpy(asTaxDist[iNumTaxDist].Code, sCode);
            strcpy(asTaxDist[iNumTaxDist].Phone, apTokens[TC_TAX_PHONE]);
            asTaxDist[iNumTaxDist].TC_Flag[0] = '1';
            iNumTaxDist++;
            iNewAgency++;

            strcpy(pOutAgency->Agency, sDesc);
            strcpy(pOutAgency->Code, sCode);
            pOutAgency->TC_Flag[0] = '1';
            LogMsg("*** New Tax code: %s", sCode);
         }
      } else
         LogMsg("*** Blank Tax Agency.  Please research APN=%s", pOutDetail->Apn);
   } else if (*apTokens[TC_TAX_CODE] > ' ')
   {
      strcpy(pOutDetail->TaxCode, apTokens[TC_TAX_CODE]);
      strcpy(pOutAgency->Code, apTokens[TC_TAX_CODE]);
      pOutAgency->TC_Flag[0] = '0';
      pOutDetail->TC_Flag[0] = '0';
   } else if (*apTokens[TC_TAX_DIST] > ' ')
   {
      strcpy(pOutAgency->Agency, apTokens[TC_TAX_DIST]);
      pOutAgency->TC_Flag[0] = '0';
   }

   if (*apTokens[TC_TAX_PHONE] > ' ' && pOutAgency->Phone[0] <= ' ')
      strcpy(pOutAgency->Phone, apTokens[TC_TAX_PHONE]);

   return 0;
}

/*************************** TC_ParseTaxDist_S1() *****************************
 *
 * Handle negative tax rate and positive tax amount
 * For SOL & TUL
 *
 ******************************************************************************/

int TC_ParseTaxDist_S1(char *pTaxDetail, char *pTaxAgency, char *pInbuf, char cDelim)
{
   static   char  sLastRollCat[32];
   static   int   iCnt=0;

   int      iTmp;
   double	dTax, dRate;

   TAXDETAIL *pOutDetail = (TAXDETAIL *)pTaxDetail;
   TAXAGENCY *pOutAgency = (TAXAGENCY *)pTaxAgency;

   // Parse input tax data
   iTmp = ParseStringIQ(pInbuf, cDelim, MAX_FLD_TOKEN, apTokens);
   if (iTmp < TC_TAX_BILLNUM)
   {
      LogMsg("***** Error: bad Tax record for APN=%.*s (#tokens=%d)", iApnLen, pInbuf, iTmp);
      return -1;
   }

   // Clear output buffer
   memset(pTaxDetail, 0, sizeof(TAXDETAIL));
   memset(pTaxAgency, 0, sizeof(TAXAGENCY));

#ifdef _DEBUG
   //if (!memcmp(apTokens[TC_APN_S], "005235002", 9))
   //   iTmp = 0;
#endif
   
   // APN
   strcpy(pOutDetail->Apn, apTokens[TC_APN_S]);
   
   // BillNum
   if (*apTokens[TC_TAX_BILLNUM] >= '0')
      strcpy(pOutDetail->BillNum, remCrLf(apTokens[TC_TAX_BILLNUM]));
   else
   {
      if (!memcmp(apTokens[TC_TAX_ROLLCAT], "SUP", 3))
         sprintf(pOutDetail->BillNum, "%s-02", apTokens[TC_APN_S]);
      else
         sprintf(pOutDetail->BillNum, "%s-01", apTokens[TC_APN_S]);
   }

   // Tax Year
   sprintf(pOutDetail->TaxYear, "%d", iTaxYear);

   // Tax Rate - In SOL, FIXED amt known to be varied by community
   //if (!_memicmp(apTokens[TC_TAX_RATE], "FIX", 3))
   //   strcpy(pOutAgency->TaxAmt, pOutDetail->TaxAmt);
   //else 
   if (*apTokens[TC_TAX_RATE] > ' ')
   {
      dRate = atof(apTokens[TC_TAX_RATE]);
      // Tax rate - may have negative value
      if (dRate)
      {
         strcpy(pOutDetail->TaxRate, apTokens[TC_TAX_RATE]);
         if (bInclTaxRate)
            strcpy(pOutAgency->TaxRate, apTokens[TC_TAX_RATE]);
      }
   } else
      dRate = 0.0;

   // Tax amount
   dTax = atof(apTokens[TC_TAX_TOTAL]);
   if (dRate < 0.0 && dTax > 0.0)
      dTax = -dTax;                             // Quick fix
   sprintf(pOutDetail->TaxAmt, "%.2f", dTax);

   // Tax Agency - remove special char
   iTmp = remChar(apTokens[TC_TAX_DIST], '\\');
   strcpy(pOutDetail->TaxDesc, apTokens[TC_TAX_DIST]);
   strcpy(pOutAgency->Agency, apTokens[TC_TAX_DIST]);

   // Tax code
   if (iNumTaxDist > 1)
   {
      replStrAll(apTokens[TC_TAX_DIST], "&QUOT;", "\"");
      replStr(apTokens[TC_TAX_DIST], "&#39;", "'");
      TAXAGENCY *pAgency = findTaxDist(apTokens[TC_TAX_DIST]);
      if (pAgency)
      {
         strcpy(pOutDetail->TaxCode, pAgency->Code);
         strcpy(pOutAgency->Code, pAgency->Code);
         strcpy(pOutAgency->Agency, pAgency->Agency);
         strcpy(pOutAgency->Phone, pAgency->Phone);
         pOutAgency->TC_Flag[0] = pAgency->TC_Flag[0];
         pOutDetail->TC_Flag[0] = pAgency->TC_Flag[0];
      } else
      {
         char sDesc[256], sCode[32];

         // Update dist table
         asTaxDist[iNumTaxDist].iCodeLen = sprintf(sCode, "X%.3d", ++iNewCodeIdx);
         asTaxDist[iNumTaxDist].iNameLen = sprintf(sDesc, "%s", apTokens[TC_TAX_DIST]);
         strcpy(asTaxDist[iNumTaxDist].Agency, sDesc);
         strcpy(asTaxDist[iNumTaxDist].Code, sCode);
         strcpy(asTaxDist[iNumTaxDist].Phone, apTokens[TC_TAX_PHONE]);
         asTaxDist[iNumTaxDist].TC_Flag[0] = '1';
         if (iNumTaxDist < MAX_TAX_DISTRICT-1)
            iNumTaxDist++;
         iNewAgency++;

         strcpy(pOutAgency->Agency, sDesc);
         strcpy(pOutAgency->Code, sCode);
         pOutAgency->TC_Flag[0] = '1';
         LogMsg("*** New Tax Agency: %s", apTokens[TC_TAX_DIST]);
      }
   } else
   {
      strcpy(pOutDetail->TaxCode, apTokens[TC_TAX_CODE]);
      strcpy(pOutAgency->Code, apTokens[TC_TAX_CODE]);
      pOutAgency->TC_Flag[0] = '0';
      pOutDetail->TC_Flag[0] = '0';
   }

   if (*apTokens[TC_TAX_PHONE] > ' ' && pOutAgency->Phone[0] <= ' ')
   {
      strncpy(pOutAgency->Phone, apTokens[TC_TAX_PHONE], 14);
   }

   return 0;
}

/*************************** TC_ParseTaxDist_S2() *****************************
 *
 * Search Dist name using binary search.
 * For SBD since it has a long list
 * Return 0 if success, -1 if bad record, 1 if no tax amt
 *
 ******************************************************************************/

int TC_ParseTaxDist_S2(char *pTaxDetail, char *pTaxAgency, char *pInbuf, char cDelim)
{
   int      iTmp, iRet = 0;
   double	dTax, dRate;

   TAXDETAIL *pOutDetail = (TAXDETAIL *)pTaxDetail;
   TAXAGENCY *pOutAgency = (TAXAGENCY *)pTaxAgency;

   // Parse input tax data
   iTmp = ParseStringIQ(pInbuf, cDelim, MAX_FLD_TOKEN, apTokens);
   if (iTmp < TC_TAX_BILLNUM)
   {
      LogMsg("***** Error: bad Tax record for APN=%.*s (#tokens=%d)", iApnLen, pInbuf, iTmp);
      return -1;
   }

   // Clear output buffer
   memset(pTaxDetail, 0, sizeof(TAXDETAIL));
   memset(pTaxAgency, 0, sizeof(TAXAGENCY));

#ifdef _DEBUG
   //if (!memcmp(apTokens[TC_APN_S], "002061440", 9))
   //   iTmp = 0;
#endif
   
   // APN
   strcpy(pOutDetail->Apn, apTokens[TC_APN_S]);

   // BillNum
   if (*apTokens[TC_TAX_BILLNUM] >= '0')
   {
      strcpy(pOutDetail->BillNum, remCrLf(apTokens[TC_TAX_BILLNUM]));
      iTaxYear = atoin(pOutDetail->BillNum, 4);
   } else if (!memcmp(apTokens[TC_TAX_ROLLCAT], "SE", 2))
      sprintf(pOutDetail->BillNum, "%s-01", apTokens[TC_APN_S]);
   else if (!memcmp(apTokens[TC_TAX_ROLLCAT], "SU", 2))
      sprintf(pOutDetail->BillNum, "%s-02", apTokens[TC_APN_S]);

   // Tax Year
   sprintf(pOutDetail->TaxYear, "%d", iTaxYear);

   // Tax Rate - In SOL, FIXED amt known to be varied by community
   //if (!_memicmp(apTokens[TC_TAX_RATE], "FIX", 3))
   //   strcpy(pOutAgency->TaxAmt, pOutDetail->TaxAmt);
   //else 
   if (*apTokens[TC_TAX_RATE] > ' ')
   {
      dRate = atof(apTokens[TC_TAX_RATE]);
      // Tax rate - may have negative value
      if (dRate)
      {
         strcpy(pOutDetail->TaxRate, apTokens[TC_TAX_RATE]);
         if (bInclTaxRate)
            strcpy(pOutAgency->TaxRate, apTokens[TC_TAX_RATE]);
      }
   } else
      dRate = 0.0;

   // Tax amount
   dTax = atof(apTokens[TC_TAX_TOTAL]);
   if (!dTax)
      iRet = 1;
   else if (dRate < 0.0 && dTax > 0.0)
      dTax = -dTax;                             // Quick fix
   sprintf(pOutDetail->TaxAmt, "%.2f", dTax);

   // Tax Agency - remove special char
   iTmp = remChar(apTokens[TC_TAX_DIST], '\\');
   strcpy(pOutDetail->TaxDesc, apTokens[TC_TAX_DIST]);
   strcpy(pOutAgency->Agency, apTokens[TC_TAX_DIST]);

   // Tax code
   if (iNumTaxDist > 1)
   {
      replStrAll(apTokens[TC_TAX_DIST], "&QUOT;", "\"");
      replStr(apTokens[TC_TAX_DIST], "&#39;", "'");
      TAXAGENCY *pAgency = BSrchTaxDist(apTokens[TC_TAX_DIST]);
      if (pAgency)
      {
#ifdef _DEBUG
         if (*apTokens[TC_TAX_CODE] > ' ' && strcmp(apTokens[TC_TAX_CODE], pAgency->Code))
            LogMsg("??? Tax code mismatched.  Please research the problem APN=%s, TaxCode=%s <> %s", 
            pOutDetail->Apn, apTokens[TC_TAX_CODE], pAgency->Code);
#endif
         strcpy(pOutDetail->TaxCode, pAgency->Code);
         strcpy(pOutAgency->Code, pAgency->Code);
         strcpy(pOutAgency->Agency, pAgency->Agency);
         strcpy(pOutAgency->Phone, pAgency->Phone);
         pOutAgency->TC_Flag[0] = pAgency->TC_Flag[0];
         pOutDetail->TC_Flag[0] = pAgency->TC_Flag[0];
      } else if (*apTokens[TC_TAX_DIST] > ' ')
      {
         //char sDesc[256], sCode[32];

         // Update dist table
         //asTaxDist[iNumTaxDist].iCodeLen = sprintf(sCode, "Z%.3d", ++iNewCodeIdx);
         //asTaxDist[iNumTaxDist].iNameLen = sprintf(sDesc, "%s", apTokens[TC_TAX_DIST]);
         //strcpy(asTaxDist[iNumTaxDist].Agency, sDesc);
         //strcpy(asTaxDist[iNumTaxDist].Code, sCode);
         //strcpy(asTaxDist[iNumTaxDist].Phone, apTokens[TC_TAX_PHONE]);
         //asTaxDist[iNumTaxDist].TC_Flag[0] = '1';
         //iNumTaxDist++;
         iNewAgency++;

         //strcpy(pOutAgency->Agency, sDesc);
         //strcpy(pOutAgency->Code, sCode);
         //pOutAgency->TC_Flag[0] = '1';
         LogMsg("*** New Tax Agency: %s Phone=%s", apTokens[TC_TAX_DIST], apTokens[TC_TAX_PHONE]);
      } else
      {
         LogMsg("*** Blank Tax Agency.  Please research APN=%s", pOutDetail->Apn);
      }
   } else if (*apTokens[TC_TAX_DIST] > ' ')
   {
      strcpy(pOutAgency->Agency, apTokens[TC_TAX_DIST]);
      pOutAgency->TC_Flag[0] = '0';
   }

   if (*apTokens[TC_TAX_PHONE] > ' ' && pOutAgency->Phone[0] <= ' ')
   {
      strncpy(pOutAgency->Phone, apTokens[TC_TAX_PHONE], 14);
   }

   return iRet;
}

/*************************** TC_ParseTaxDist_S3() *****************************
 *
 * Use 01 extension of BillNum for Secd and 02 for Supp
 * Special version for ORG
 *
 ******************************************************************************/
 
int TC_ParseTaxDist_S3(char *pTaxDetail, char *pTaxAgency, char *pInbuf, char cDelim)
{
   static   char  sLastRollCat[32];
   static   int   iCnt=0;

   int      iTmp;
   double	dTax, dRate;

   TAXDETAIL *pOutDetail = (TAXDETAIL *)pTaxDetail;
   TAXAGENCY *pOutAgency = (TAXAGENCY *)pTaxAgency;
   TAXAGENCY *pAgency;

   // Parse input tax data
   iTmp = ParseStringIQ(pInbuf, cDelim, MAX_FLD_TOKEN, apTokens);
   if (iTmp < TC_TAX_BILLNUM)
   {
      LogMsg("***** Error: bad Tax record for APN=%.*s (#tokens=%d)", iApnLen, pInbuf, iTmp);
      return -1;
   }

   // Clear output buffer
   memset(pTaxDetail, 0, sizeof(TAXDETAIL));
   memset(pTaxAgency, 0, sizeof(TAXAGENCY));

#ifdef _DEBUG
   //if (!memcmp(apTokens[TC_APN_S], "002061440", 9))
   //   iTmp = 0;
#endif
   
   // APN
   strcpy(pOutDetail->Apn, apTokens[TC_APN_S]);
   
   // BillNum
   if (*apTokens[TC_TAX_BILLNUM] >= '0')
      strcpy(pOutDetail->BillNum, remCrLf(apTokens[TC_TAX_BILLNUM]));
   else if (m_bUseFeePrcl)
   {
      if (!memcmp(apTokens[TC_TAX_ROLLCAT], "SUP", 3))
         sprintf(pOutDetail->BillNum, "%s-02", apTokens[TC_APN_S]);
      else
         sprintf(pOutDetail->BillNum, "%s-01", apTokens[TC_APN_S]);
   } else
      sprintf(pOutDetail->BillNum, "%s", apTokens[TC_APN_S]);

   // Tax Year
   sprintf(pOutDetail->TaxYear, "%d", iTaxYear);

   // Tax rate - may have negative value
   if (*apTokens[TC_TAX_RATE] > ' ')
   {
      dRate = atof(apTokens[TC_TAX_RATE]);
      if (dRate)
      {
         strcpy(pOutDetail->TaxRate, apTokens[TC_TAX_RATE]);
         if (bInclTaxRate)
            strcpy(pOutAgency->TaxRate, apTokens[TC_TAX_RATE]);
      }
   } else
      dRate = 0.0;

   // Tax amount
   dTax = atof(apTokens[TC_TAX_TOTAL]);
   if (dRate < 0.0 && dTax > 0.0)
      dTax = -dTax;                             // Quick fix
   sprintf(pOutDetail->TaxAmt, "%.2f", dTax);

   // Tax Agency - remove special char
   strcpy(pOutDetail->TaxDesc, apTokens[TC_TAX_DIST]);
   strcpy(pOutAgency->Agency, apTokens[TC_TAX_DIST]);

   // Tax code
   if (iNumTaxDist > 1)
   {
      if (*apTokens[TC_TAX_CODE] > ' ')
      {
         pAgency = findTaxDist(apTokens[TC_TAX_DIST], apTokens[TC_TAX_CODE]);
         if (pAgency)
         {
            strcpy(pOutDetail->TaxCode, pAgency->Code);
            strcpy(pOutAgency->Code, pAgency->Code);
            strcpy(pOutAgency->Agency, pAgency->Agency);
            strcpy(pOutAgency->Phone, pAgency->Phone);
            pOutAgency->TC_Flag[0] = pAgency->TC_Flag[0];
            pOutDetail->TC_Flag[0] = pAgency->TC_Flag[0];
         } else
         {
            strcpy(pOutAgency->Code, apTokens[TC_TAX_CODE]);
            strcpy(pOutDetail->TaxCode, apTokens[TC_TAX_CODE]);
            pOutAgency->TC_Flag[0] = '0';
            pOutDetail->TC_Flag[0] = '0';
            LogMsg("*** New Tax Agency: %s", apTokens[TC_TAX_DIST]);
            iNewAgency++;
         }
      } else
      {
            strcpy(pOutAgency->Code, apTokens[TC_TAX_CODE]);
            strcpy(pOutDetail->TaxCode, pOutAgency->Code);
            strcpy(pOutDetail->TaxDesc, apTokens[TC_TAX_DIST]);
            pOutAgency->TC_Flag[0] = '1';
            pOutDetail->TC_Flag[0] = '1';
            LogMsg("*** New Tax Agency: %s", apTokens[TC_TAX_DIST]);
            iNewAgency++;
      }
   } else
   {
      strcpy(pOutDetail->TaxCode, apTokens[TC_TAX_CODE]);
      strcpy(pOutAgency->Code, apTokens[TC_TAX_CODE]);
      pOutAgency->TC_Flag[0] = '0';
      pOutDetail->TC_Flag[0] = '0';
   }

   if (*apTokens[TC_TAX_PHONE] > ' ' && pOutAgency->Phone[0] <= ' ')
   {
      strncpy(pOutAgency->Phone, apTokens[TC_TAX_PHONE], 14);
   }

   return 0;
}

/*************************** TC_ParseTaxDist_S4() *****************************
 *
 * Search Tax Code using binary search
 * For SAC
 * Return 0 if success
 *
 ******************************************************************************/

int TC_ParseTaxDist_S4(char *pTaxDetail, char *pTaxAgency, char *pInbuf, char cDelim)
{
   int      iTmp;
   double	dTax, dRate;

   TAXDETAIL *pOutDetail = (TAXDETAIL *)pTaxDetail;
   TAXAGENCY *pOutAgency = (TAXAGENCY *)pTaxAgency;
   TAXAGENCY *pAgency;

   // Parse input tax data
   iTmp = ParseStringIQ(pInbuf, cDelim, MAX_FLD_TOKEN, apTokens);
   if (iTmp < TC_TAX_BILLNUM)
   {
      LogMsg("***** Error: bad Tax record for APN=%.*s (#tokens=%d)", iApnLen, pInbuf, iTmp);
      return -1;
   }

   // Clear output buffer
   memset(pTaxDetail, 0, sizeof(TAXDETAIL));
   memset(pTaxAgency, 0, sizeof(TAXAGENCY));

#ifdef _DEBUG
   //if (!memcmp(apTokens[TC_APN_S], "0010020064", 10))
   //   iTmp = 0;
#endif
   
   // APN
   strcpy(pOutDetail->Apn, apTokens[TC_APN_S]);

   // BillNum
   if (*apTokens[TC_TAX_BILLNUM] >= '0')
   {
      strcpy(pOutDetail->BillNum, remCrLf(apTokens[TC_TAX_BILLNUM]));
      iTaxYear = 2000 + atoin(pOutDetail->BillNum, 2);
   } else if (!memcmp(apTokens[TC_TAX_ROLLCAT], "SE", 2))
      sprintf(pOutDetail->BillNum, "%s-01", apTokens[TC_APN_S]);
   else if (!memcmp(apTokens[TC_TAX_ROLLCAT], "SU", 2))
      sprintf(pOutDetail->BillNum, "%s-02", apTokens[TC_APN_S]);

   // Tax Year
   sprintf(pOutDetail->TaxYear, "%d", iTaxYear);

   // Tax Rate - In SOL, FIXED amt known to be varied by community
   //if (!_memicmp(apTokens[TC_TAX_RATE], "FIX", 3))
   //   strcpy(pOutAgency->TaxAmt, pOutDetail->TaxAmt);
   //else 
   if (*apTokens[TC_TAX_RATE] > ' ')
   {
      dRate = atof(apTokens[TC_TAX_RATE]);
      // Tax rate - may have negative value
      if (dRate)
      {
         strcpy(pOutDetail->TaxRate, apTokens[TC_TAX_RATE]);
         if (bInclTaxRate)
            strcpy(pOutAgency->TaxRate, apTokens[TC_TAX_RATE]);
      }
   } else
      dRate = 0.0;

   // Tax amount
   dTax = atof(apTokens[TC_TAX_TOTAL]);
   if (dRate < 0.0 && dTax > 0.0)
      dTax = -dTax;                             // Quick fix
   sprintf(pOutDetail->TaxAmt, "%.2f", dTax);

   // Tax Agency - remove special char
   iTmp = remChar(apTokens[TC_TAX_DIST], '\\');
   strcpy(pOutDetail->TaxDesc, apTokens[TC_TAX_DIST]);
   strcpy(pOutAgency->Agency, apTokens[TC_TAX_DIST]);

#ifdef _DEBUG
   //if (!memcmp(apTokens[TC_TAX_CODE], "0161", 4))
   //   iTmp = 0;
#endif

   // Tax code
   if (iNumTaxDist > 1)
   {
      pAgency = findTaxAgency(apTokens[TC_TAX_CODE]);
      if (pAgency)
      {
#ifdef _DEBUG
         if (*apTokens[TC_TAX_CODE] > ' ' && strcmp(apTokens[TC_TAX_CODE], pAgency->Code))
            LogMsg("??? Tax code mismatched.  Please research the problem APN=%s, TaxCode=%s <> %s", 
            pOutDetail->Apn, apTokens[TC_TAX_CODE], pAgency->Code);
#endif
         strcpy(pOutDetail->TaxCode, pAgency->Code);
         strcpy(pOutAgency->Code, pAgency->Code);
         strcpy(pOutAgency->Agency, pAgency->Agency);
         strcpy(pOutAgency->Phone, pAgency->Phone);
         pOutAgency->TC_Flag[0] = pAgency->TC_Flag[0];
         pOutDetail->TC_Flag[0] = pAgency->TC_Flag[0];
      } else if (iNumTaxDist < MAX_TAX_DISTRICT-1)
      {
         // Update dist table
         //asTaxDist[iNumTaxDist].iCodeLen = strlen(apTokens[TC_TAX_CODE]);
         //asTaxDist[iNumTaxDist].iNameLen = strlen(apTokens[TC_TAX_DIST]);
         //strcpy(asTaxDist[iNumTaxDist].Agency, apTokens[TC_TAX_DIST]);
         //strcpy(asTaxDist[iNumTaxDist].Code, apTokens[TC_TAX_CODE]);
         //strcpy(asTaxDist[iNumTaxDist].Phone, apTokens[TC_TAX_PHONE]);
         //asTaxDist[iNumTaxDist].TC_Flag[0] = '0';
         //iNumTaxDist++;
         //iNewAgency++;

         strcpy(pOutAgency->Agency, apTokens[TC_TAX_DIST]);
         strcpy(pOutAgency->Code, apTokens[TC_TAX_CODE]);
         pOutAgency->TC_Flag[0] = '0';
         LogMsg("*** New Tax code: %s", apTokens[TC_TAX_CODE]);
      } else
         LogMsg("*** New Tax code: %s", apTokens[TC_TAX_CODE]);
   } else if (*apTokens[TC_TAX_CODE] > ' ')
   {
      strcpy(pOutDetail->TaxCode, apTokens[TC_TAX_CODE]);
      strcpy(pOutAgency->Code, apTokens[TC_TAX_CODE]);
      pOutAgency->TC_Flag[0] = '0';
      pOutDetail->TC_Flag[0] = '0';
   } else if (*apTokens[TC_TAX_DIST] > ' ')
   {
      strcpy(pOutAgency->Agency, apTokens[TC_TAX_DIST]);
      pOutAgency->TC_Flag[0] = '0';
   }

   if (*apTokens[TC_TAX_PHONE] > ' ' && pOutAgency->Phone[0] <= ' ')
   {
      strncpy(pOutAgency->Phone, apTokens[TC_TAX_PHONE], 14);
   }

   return 0;
}

/*************************** TC_ParseTaxDist_S5() ******************************
 *
 * Special case for MRN where '|' is in district name
 *
 ******************************************************************************/

int TC_ParseTaxDist_S5(char *pTaxDetail, char *pTaxAgency, char *pInbuf, char cDelim)
{
   int      iTmp;
   double	dTax, dRate;
   char     *pTmp;

   TAXDETAIL *pOutDetail = (TAXDETAIL *)pTaxDetail;
   TAXAGENCY *pOutAgency = (TAXAGENCY *)pTaxAgency;

   // Check special case
   if ((pTmp = strstr(pInbuf, "1915|CLEAN")) || (pTmp = strstr(pInbuf, "1915|ANRG")) || (pTmp = strstr(pInbuf, "1915|B-A")))
      *(pTmp+4) = ' ';

   // Parse input tax data
   iTokens = ParseStringIQ(pInbuf, cDelim, MAX_FLD_TOKEN, apTokens);
   if (iTokens < TC_TAX_ROLLCAT)
   {
      LogMsg("***** Error: bad Tax record for APN=%.*s (#tokens=%d)", iApnLen, pInbuf, iTokens);
      return -1;
   } else if (iTokens > TC_TAX_ROLLCAT+1)
      LogMsg("*** Warning: check for bad record APN=%.*s (#tokens=%d)", iApnLen, pInbuf, iTokens);

   // Clear output buffer
   memset(pTaxDetail, 0, sizeof(TAXDETAIL));
   memset(pTaxAgency, 0, sizeof(TAXAGENCY));

#ifdef _DEBUG
   //if (!memcmp(apTokens[TC_APN_S], "002061440", 9))
   //   iTmp = 0;
#endif
   
   // APN
   strcpy(pOutDetail->Apn, apTokens[TC_APN_S]);

   // BillNum
   if (*apTokens[TC_TAX_BILLNUM] >= '0')
      strcpy(pOutDetail->BillNum, remCrLf(apTokens[TC_TAX_BILLNUM]));
   else if (iTokens > TC_TAX_ROLLCAT && m_bUseFeePrcl)
   {
      if (!memcmp(apTokens[TC_TAX_ROLLCAT], "SE", 2) || !memcmp(apTokens[TC_TAX_ROLLCAT], "UNSE", 4))
         sprintf(pOutDetail->BillNum, "%s-01", apTokens[TC_APN_S]);
      else if (!memcmp(apTokens[TC_TAX_ROLLCAT], "SU", 2))
         sprintf(pOutDetail->BillNum, "%s-02", apTokens[TC_APN_S]);
   }

   // Tax Year
   iTmp = atoin(pOutDetail->BillNum, 2);
   sprintf(pOutDetail->TaxYear, "20%d", iTmp);

   // Tax Rate - In SOL, FIXED amt known to be varied by community
   //if (!_memicmp(apTokens[TC_TAX_RATE], "FIX", 3))
   //   strcpy(pOutAgency->TaxAmt, pOutDetail->TaxAmt);
   //else 
   if (*apTokens[TC_TAX_RATE] > ' ')
   {
      dRate = atof(apTokens[TC_TAX_RATE]);
      // Tax rate - may have negative value
      if (dRate)
      {
         strcpy(pOutDetail->TaxRate, apTokens[TC_TAX_RATE]);
         if (bInclTaxRate)
            strcpy(pOutAgency->TaxRate, apTokens[TC_TAX_RATE]);
      }
   } else
      dRate = 0.0;

   // Tax amount
   dTax = atof(apTokens[TC_TAX_TOTAL]);
   if (dRate < 0.0 && dTax > 0.0)
      dTax = -dTax;                             // Quick fix
   sprintf(pOutDetail->TaxAmt, "%.2f", dTax);

   // Tax Agency - remove special char
   iTmp = remChar(apTokens[TC_TAX_DIST], '\\');
   strcpy(pOutDetail->TaxDesc, apTokens[TC_TAX_DIST]);
   strcpy(pOutAgency->Agency, apTokens[TC_TAX_DIST]);

   // Tax code
   if (iNumTaxDist > 1)
   {
      replStrAll(apTokens[TC_TAX_DIST], "&QUOT;", "\"");
      replStr(apTokens[TC_TAX_DIST], "&#39;", "'");
      TAXAGENCY *pAgency = findTaxDist(apTokens[TC_TAX_DIST]);
      if (pAgency)
      {
#ifdef _DEBUG
         if (*apTokens[TC_TAX_CODE] > ' ' && strcmp(apTokens[TC_TAX_CODE], pAgency->Code) && strcmp(apTokens[TC_TAX_DIST], "PENALTY"))
            LogMsg("??? Tax code mismatched.  Please research the problem APN=%s, TaxCode=%s <> %s (%s)", 
            pOutDetail->Apn, apTokens[TC_TAX_CODE], pAgency->Code, apTokens[TC_TAX_DIST]);
#endif
         strcpy(pOutDetail->TaxCode, pAgency->Code);
         strcpy(pOutAgency->Code, pAgency->Code);
         strcpy(pOutAgency->Agency, pAgency->Agency);
         strcpy(pOutAgency->Phone, pAgency->Phone);
         pOutAgency->TC_Flag[0] = pAgency->TC_Flag[0];
         pOutDetail->TC_Flag[0] = pAgency->TC_Flag[0];
      } else if (*apTokens[TC_TAX_DIST] > ' ')
      {
         char sDesc[256], sCode[32];

         // Update dist table
         asTaxDist[iNumTaxDist].iCodeLen = sprintf(sCode, "X%.3d", ++iNewCodeIdx);
         asTaxDist[iNumTaxDist].iNameLen = sprintf(sDesc, "%s", apTokens[TC_TAX_DIST]);
         strcpy(asTaxDist[iNumTaxDist].Agency, sDesc);
         strcpy(asTaxDist[iNumTaxDist].Code, sCode);
         strcpy(asTaxDist[iNumTaxDist].Phone, apTokens[TC_TAX_PHONE]);
         asTaxDist[iNumTaxDist].TC_Flag[0] = '1';
         iNumTaxDist++;
         iNewAgency++;

         strcpy(pOutAgency->Agency, sDesc);
         strcpy(pOutAgency->Code, sCode);
         pOutAgency->TC_Flag[0] = '1';
         LogMsg("*** New Tax Agency: %s", apTokens[TC_TAX_DIST]);
      } else if (*apTokens[TC_TAX_CODE] > ' ')
      {
         char sDesc[256], sCode[32];

         pAgency = findTaxAgency(apTokens[TC_TAX_CODE]);
         if (pAgency)
         {
            strcpy(pOutDetail->TaxCode, pAgency->Code);
            strcpy(pOutAgency->Code, pAgency->Code);
            strcpy(pOutAgency->Agency, pAgency->Agency);
            strcpy(pOutAgency->Phone, pAgency->Phone);
            pOutAgency->TC_Flag[0] = pAgency->TC_Flag[0];
            pOutDetail->TC_Flag[0] = pAgency->TC_Flag[0];
         } else if (iNumTaxDist < MAX_TAX_DISTRICT-1)
         {
            // Update dist table
            asTaxDist[iNumTaxDist].iCodeLen = sprintf(sCode, "X%.3d", ++iNewCodeIdx);
            asTaxDist[iNumTaxDist].iNameLen = sprintf(sDesc, "Special Assessment %s", sCode);
            strcpy(asTaxDist[iNumTaxDist].Agency, sDesc);
            strcpy(asTaxDist[iNumTaxDist].Code, sCode);
            strcpy(asTaxDist[iNumTaxDist].Phone, apTokens[TC_TAX_PHONE]);
            asTaxDist[iNumTaxDist].TC_Flag[0] = '1';
            iNumTaxDist++;
            iNewAgency++;

            strcpy(pOutAgency->Agency, sDesc);
            strcpy(pOutAgency->Code, sCode);
            pOutAgency->TC_Flag[0] = '1';
            LogMsg("*** New Tax code1: %s", sCode);
         } else
            LogMsg("*** New Tax code2: %s", sCode);
      } else
         LogMsg("*** Blank Tax Agency.  Please research APN=%s", pOutDetail->Apn);
   } else if (*apTokens[TC_TAX_CODE] > ' ')
   {
      strcpy(pOutDetail->TaxCode, apTokens[TC_TAX_CODE]);
      strcpy(pOutAgency->Code, apTokens[TC_TAX_CODE]);
      pOutAgency->TC_Flag[0] = '0';
      pOutDetail->TC_Flag[0] = '0';
   } else if (*apTokens[TC_TAX_DIST] > ' ')
   {
      strcpy(pOutAgency->Agency, apTokens[TC_TAX_DIST]);
      pOutAgency->TC_Flag[0] = '0';
   }

   if (*apTokens[TC_TAX_PHONE] > ' ' && pOutAgency->Phone[0] <= ' ')
   {
      strncpy(pOutAgency->Phone, apTokens[TC_TAX_PHONE], 14);
   }

   return 0;
}

/*************************** TC_ParseTaxDist_S6() *****************************
 *
 * For SCL
 *
 ******************************************************************************/

int TC_ParseTaxDist_S6(char *pTaxDetail, char *pTaxAgency, char *pInbuf, char cDelim)
{
   int      iTmp;
   double	dTax, dRate;

   TAXDETAIL *pOutDetail = (TAXDETAIL *)pTaxDetail;
   TAXAGENCY *pOutAgency = (TAXAGENCY *)pTaxAgency;
   TAXAGENCY *pAgency;

   // Parse input tax data
   iTokens = ParseStringIQ(pInbuf, cDelim, MAX_FLD_TOKEN, apTokens);
   if (iTokens < TC_TAX_BILLNUM)
   {
      LogMsg("***** Error: bad Tax record for APN=%.*s (#tokens=%d)", iApnLen, pInbuf, iTokens);
      return -1;
   }

   // Clear output buffer
   memset(pTaxDetail, 0, sizeof(TAXDETAIL));
   memset(pTaxAgency, 0, sizeof(TAXAGENCY));

#ifdef _DEBUG
   //if (!memcmp(apTokens[TC_APN_S], "002061440", 9))
   //   iTmp = 0;
#endif
   
   // APN
   strcpy(pOutDetail->Apn, apTokens[TC_APN_S]);

   // BillNum
   if (*apTokens[TC_TAX_BILLNUM] >= '0')
      strcpy(pOutDetail->BillNum, remCrLf(apTokens[TC_TAX_BILLNUM]));
   else if (iTokens > TC_TAX_ROLLCAT && m_bUseFeePrcl)
   {
      if (!memcmp(apTokens[TC_TAX_ROLLCAT], "SE", 2) || !memcmp(apTokens[TC_TAX_ROLLCAT], "UNSE", 4))
         sprintf(pOutDetail->BillNum, "%s-01", apTokens[TC_APN_S]);
      else if (!memcmp(apTokens[TC_TAX_ROLLCAT], "SU", 2))
         sprintf(pOutDetail->BillNum, "%s-02", apTokens[TC_APN_S]);
   }

   // Tax Year
   sprintf(pOutDetail->TaxYear, "%d", iTaxYear);

   // Tax Rate - In SOL, FIXED amt known to be varied by community
   if (*apTokens[TC_TAX_RATE] > ' ')
   {
      dRate = atof(apTokens[TC_TAX_RATE]);
      // Tax rate - may have negative value
      if (dRate)
      {
         strcpy(pOutDetail->TaxRate, apTokens[TC_TAX_RATE]);
         if (bInclTaxRate)
            strcpy(pOutAgency->TaxRate, apTokens[TC_TAX_RATE]);
      }
   } else
      dRate = 0.0;

   // Tax amount
   dTax = atof(apTokens[TC_TAX_TOTAL]);
   if (dRate < 0.0 && dTax > 0.0)
      dTax = -dTax;                             // Quick fix
   sprintf(pOutDetail->TaxAmt, "%.2f", dTax);

   // Tax Agency - remove special char
   iTmp = remChar(apTokens[TC_TAX_DIST], '\\');
   strcpy(pOutDetail->TaxDesc, apTokens[TC_TAX_DIST]);
   strcpy(pOutAgency->Agency, apTokens[TC_TAX_DIST]);

   // Tax code
   if (iNumTaxDist > 1)
   {
      replStrAll(apTokens[TC_TAX_DIST], "&QUOT;", "\"");
      replStr(apTokens[TC_TAX_DIST], "&#39;", "'");

      if (*apTokens[TC_TAX_CODE] > ' ')
         pAgency = findTaxAgency(apTokens[TC_TAX_CODE]);
      else
         pAgency = findTaxDist(apTokens[TC_TAX_DIST]);

      if (pAgency)
      {
#ifdef _DEBUG
         if (*apTokens[TC_TAX_CODE] > ' ' && strcmp(apTokens[TC_TAX_CODE], pAgency->Code))
            LogMsg("??? Tax code mismatched.  Please research the problem APN=%s, TaxCode=%s <> %s (%s)", 
            pOutDetail->Apn, apTokens[TC_TAX_CODE], pAgency->Code, apTokens[TC_TAX_DIST]);
#endif
         strcpy(pOutDetail->TaxCode, pAgency->Code);
         strcpy(pOutAgency->Code, pAgency->Code);
         strcpy(pOutAgency->Agency, pAgency->Agency);
         strcpy(pOutAgency->Phone, pAgency->Phone);
         pOutAgency->TC_Flag[0] = pAgency->TC_Flag[0];
         pOutDetail->TC_Flag[0] = pAgency->TC_Flag[0];
      } else if (*apTokens[TC_TAX_DIST] > ' ')
      {
         char sDesc[256], sCode[32];

         // Update dist table
         if (*apTokens[TC_TAX_CODE] > ' ')
         {
            strcpy(sCode, apTokens[TC_TAX_CODE]);
            asTaxDist[iNumTaxDist].iCodeLen = strlen(sCode);
            asTaxDist[iNumTaxDist].TC_Flag[0] = '0';
            pOutAgency->TC_Flag[0] = '0';
         } else
         {
            asTaxDist[iNumTaxDist].iCodeLen = sprintf(sCode, "X%.3d", ++iNewCodeIdx);
            asTaxDist[iNumTaxDist].TC_Flag[0] = '1';
            pOutAgency->TC_Flag[0] = '1';
         }
         asTaxDist[iNumTaxDist].iNameLen = sprintf(sDesc, "%s", apTokens[TC_TAX_DIST]);
         strcpy(asTaxDist[iNumTaxDist].Agency, sDesc);
         strcpy(asTaxDist[iNumTaxDist].Code, sCode);
         strcpy(asTaxDist[iNumTaxDist].Phone, apTokens[TC_TAX_PHONE]);
         //iNumTaxDist++;
         //iNewAgency++;

         strcpy(pOutAgency->Agency, sDesc);
         strcpy(pOutAgency->Code, sCode);
         LogMsg("*** New Tax Agency: %s|%s|%s", sCode, apTokens[TC_TAX_DIST], apTokens[TC_TAX_PHONE]);
      } else if (*apTokens[TC_TAX_CODE] > ' ')
      {
         char sDesc[256], sCode[32];

         pAgency = findTaxAgency(apTokens[TC_TAX_CODE]);
         if (pAgency)
         {
            strcpy(pOutDetail->TaxCode, pAgency->Code);
            strcpy(pOutAgency->Code, pAgency->Code);
            strcpy(pOutAgency->Agency, pAgency->Agency);
            strcpy(pOutAgency->Phone, pAgency->Phone);
            pOutAgency->TC_Flag[0] = pAgency->TC_Flag[0];
            pOutDetail->TC_Flag[0] = pAgency->TC_Flag[0];
         } else
         {
            // Update dist table
            strcpy(sCode, apTokens[TC_TAX_CODE]);
            asTaxDist[iNumTaxDist].iCodeLen = strlen(sCode);
            asTaxDist[iNumTaxDist].iNameLen = sprintf(sDesc, "Special Assessment %s", sCode);
            strcpy(asTaxDist[iNumTaxDist].Agency, sDesc);
            strcpy(asTaxDist[iNumTaxDist].Code, sCode);
            strcpy(asTaxDist[iNumTaxDist].Phone, apTokens[TC_TAX_PHONE]);
            asTaxDist[iNumTaxDist].TC_Flag[0] = '1';
            iNewAgency++;

            strcpy(pOutAgency->Agency, sDesc);
            strcpy(pOutAgency->Code, sCode);
            pOutAgency->TC_Flag[0] = '1';
            LogMsg("*** New Tax code: %s", sCode);
         }
      } else
         LogMsg("*** Blank Tax Agency.  Please research APN=%s", pOutDetail->Apn);
   } else if (*apTokens[TC_TAX_CODE] > ' ')
   {
      strcpy(pOutDetail->TaxCode, apTokens[TC_TAX_CODE]);
      strcpy(pOutAgency->Code, apTokens[TC_TAX_CODE]);
      pOutAgency->TC_Flag[0] = '0';
      pOutDetail->TC_Flag[0] = '0';
   } else if (*apTokens[TC_TAX_DIST] > ' ')
   {
      strcpy(pOutAgency->Agency, apTokens[TC_TAX_DIST]);
      pOutAgency->TC_Flag[0] = '0';
   }

   if (*apTokens[TC_TAX_PHONE] > ' ' && pOutAgency->Phone[0] <= ' ')
   {
      strncpy(pOutAgency->Phone, apTokens[TC_TAX_PHONE], 14);
   }

   return 0;
}

/*************************** Load_TaxAgencyAndDetail() ************************
 *
 * Use ???DIST.TC to load Tax_Agency, Tax_Detail
 *
 ******************************************************************************/

int TC_LoadTaxAgencyAndDetail(char *pCnty, char *pDistFile, char *pAgencyFile, char *pDetailFile, bool bImport, bool bIgnoreNoValue)
{
   char     *pTmp, acTmpAgency[256], acAgency[1024], acDetail[1024], acRec[1024];
   int      iRet, iParseType=0;
   long     lOut=0, lCnt=0, lDrop=0, lBad=0, lNoTax=0;
   FILE     *fdIn, *fdDetail, *fdAgency;
   TAXDETAIL *pDetail = (TAXDETAIL *)acDetail;

   // Open input file
   LogMsg("Open DIST file %s", pDistFile);
   fdIn = fopen(pDistFile, "r");
   if (!fdIn)
   {
      LogMsg("***** Error opening Tax file: %s\n", pDistFile);
      return -2;
   }  

   // Open Detail output
   if (pDetailFile)
   {
      strcpy(acDetail, pDetailFile);
      strcpy(acAgency, pAgencyFile);
   } else
   {
      NameTaxCsvFile(acDetail, pCnty, "Items");
      NameTaxCsvFile(acAgency, pCnty, "Agency");
   }

   LogMsg("Open Detail file %s", acDetail);
   fdDetail = fopen(acDetail, "w");
   if (!fdDetail)
   {
      LogMsg("***** Error creating Detail file: %s\n", acDetail);
      return -4;
   }

   // Open Agency output
   sprintf(acTmpAgency, "%s\\%s\\%s_Agency.tmp", acTmpPath, pCnty, pCnty);
   LogMsg("Open Agency file %s", acTmpAgency);
   fdAgency = fopen(acTmpAgency, "w");
   if (!fdAgency)
   {
      LogMsg("***** Error creating temp Agency file: %s\n", acTmpAgency);
      return -4;
   }

   // Set which parser to use
   if (!_memicmp(pCnty, "SOL", 3) || !_memicmp(pCnty, "TUL", 3))
      iParseType = 1;
   else if (!_memicmp(pCnty, "SBD", 3))
      iParseType = 2;
   else if (!_memicmp(pCnty, "ORG", 3))
      iParseType = 3;
   else if (!_memicmp(pCnty, "SAC", 3))
      iParseType = 4;
   else if (!_memicmp(pCnty, "MRN", 3))
      iParseType = 5;
   else if (!_memicmp(pCnty, "SCL", 3))
      iParseType = 6;

   if (!iTaxYear)
      iTaxYear = lTaxYear;

   // Skip header record
   pTmp = fgets((char *)&acRec[0], 1024, fdIn);
   iNewCodeIdx = 0;
   iNewAgency = 0;

   // Merge loop 
   while (!feof(fdIn))
   {
      pTmp = fgets((char *)&acRec[0], 1024, fdIn);
      if (!pTmp)
         break;

      if (*pTmp < '0')
         continue;

#ifdef _DEBUG
      //if (!memcmp(acRec, "06009106", 8))
      //   iRet = 0;
#endif

      // Create new Detail record
      if (iParseType == 1)
         iRet = TC_ParseTaxDist_S1(acDetail, acAgency, acRec, '|');
      else if (iParseType == 2)
         iRet = TC_ParseTaxDist_S2(acDetail, acAgency, acRec, '|');
      else if (iParseType == 3)
         iRet = TC_ParseTaxDist_S3(acDetail, acAgency, acRec, '|');
      else if (iParseType == 4)
         iRet = TC_ParseTaxDist_S4(acDetail, acAgency, acRec, '|');
      else if (iParseType == 5)
         iRet = TC_ParseTaxDist_S5(acDetail, acAgency, acRec, '|');
      else if (iParseType == 6)
         iRet = TC_ParseTaxDist_S6(acDetail, acAgency, acRec, '|');
      else
         iRet = TC_ParseTaxDist(acDetail, acAgency, acRec, '|');
      if (!iRet)
      {
         double dTmp = atof(pDetail->TaxAmt);
         if (dTmp > 0.0 || !bIgnoreNoValue)
         {
            // Create delimited record
            Tax_CreateDetailCsv(acRec, (TAXDETAIL *)&acDetail);
            fputs(acRec, fdDetail);

            Tax_CreateAgencyCsv(acRec, (TAXAGENCY *)&acAgency);
            fputs(acRec, fdAgency);
            lOut++;
         } else
         {
            if (bDebug)
               LogMsg("*** Ignore items without tax amt: %s - %s", pDetail->Apn, pDetail->TaxCode);
            lDrop++;
         }
      } else if (iRet == 1)
         lNoTax++;
      else
         lBad++;
         //LogMsg("---> Drop detail record %d [%s] (%d)", lCnt, acRec, iRet); 

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdIn)
      fclose(fdIn);
   if (fdDetail)
      fclose(fdDetail);
   if (fdAgency)
      fclose(fdAgency);

   LogMsg("Total records processed:      %u", lCnt);
   LogMsg("      Detail records output:  %u", lOut);
   LogMsg("      Detail records dropped: %u", lDrop);
   LogMsg("      No Tax records:         %u", lNoTax);
   LogMsg("      Bad Detail records:     %u", lBad);
   LogMsg("      New tax agency:         %u", iNewAgency);

   printf("\nTotal output records: %u", lCnt);

   // Dedup Agency file
   if (pAgencyFile)
      strcpy(acAgency, pAgencyFile);
   else
      NameTaxCsvFile(acAgency, pCnty, "Agency");

   iRet = sortFile(acTmpAgency, acAgency, "S(#1,C,A,#2,C,A) DUPOUT(#1) F(TXT) DEL(124)");

   // Import into SQL
   if (bImport)
   {
      iRet = doTaxImport(pCnty, TAX_DETAIL);
      if (!iRet)
         iRet = doTaxImport(pCnty, TAX_AGENCY);
   } else
      iRet = 0;

   if (iNewAgency > 0)
      iRet = 99;        // signal send mail

   return iRet;
}

/******************************** Load_TC() ***********************************
 *
 * Load scrape data from county tax website
 *
 * Use ???.TC to load Tax_Base, Tax_Delq
 *     ???DIST.TC to load Tax_Agency, Tax_Detail
 *     S???.S01 to load Tax_Owner
 *
 *   - CAL,DNX,IMP,MAD,MER,MNO,MON,MRN,NAP,ORG,PLA,SBX,SCL,SFX,SMX,SOL,SON,STA,TUL,YOL,YUB
 *
 ******************************************************************************/

int Load_TC(char *pCnty, bool bImport)
{
   char  sTaxFile[_MAX_PATH], sDistFile[_MAX_PATH], sDetailFile[_MAX_PATH], sTmp[_MAX_PATH];
   char  sAgencyFile[_MAX_PATH], sBaseFile[_MAX_PATH], sDelqFile[_MAX_PATH], sSortFile[_MAX_PATH];
   int   iRet;
   INT64 lTmp;

   LogMsg0("Loading TC files for %s", pCnty);

   // Set flag to include tax rate in Agency table
   iRet = GetIniString(pCnty, "TaxRate", "Y", sTmp, _MAX_PATH, acIniFile);
   if (sTmp[0] == 'Y')
      bInclTaxRate = true;
   else
      bInclTaxRate = false;

   // Set flag to ignore zero value
   iRet = GetIniString(pCnty, "IgnoreNoValue", "Y", sTmp, _MAX_PATH, acIniFile);
   if (sTmp[0] == 'Y')
      bIgnoreZero = true;
   else
      bIgnoreZero = false;

   // Get input file names
   GetIniString("Data", "Main_TC", "", sTmp, _MAX_PATH, acIniFile);
   sprintf(sTaxFile, sTmp, pCnty, pCnty, 'F');
   sprintf(sSortFile, sTmp, pCnty, "Sort", 'F');

   // Check file size, ignore small file
   lTmp = getFileSize(sTaxFile);
   if (lTmp < 1000)
   {
      LogMsg("***** File size is too small (%d).  Skip processing %s", lTmp, sTaxFile);
      return 0;
   }

   // Save last file date to update County table
   lLastTaxFileDate = getFileDate(sTaxFile);

   // Only process if new tax file
   iRet = isNewTaxFile(sTaxFile, pCnty);
   if (iRet <= 0)
   {
      LogMsg("*** Loading old file %s.  Reset Tax date and continue (or set ChkFileDate=N)", sTaxFile);
      lLastTaxFileDate = 0;      // Signal not to update Tax info in County table
      return iRet;
   }

   // Sort TC file
   iRet = doSortTC(sTaxFile, sSortFile);
   if (iRet > 0)
      strcpy(sTaxFile, sSortFile);
   else
      return -2;

   // Load Tax Dist
   iRet = GetIniString(pCnty, "TaxDist", "", sTmp, _MAX_PATH, acIniFile);
   if (iRet > 0)
      iRet = LoadTaxCodeTable(sTmp);

   // Dedup if needed
   // S(#1,C,A,#7,C,A,#8,C,A,#2,C,A) DUPOUT F(TXT) DEL(124) BYPASS(77,R)
   GetIniString("Data", "Dist_TC", "", sTmp, _MAX_PATH, acIniFile);
   sprintf(sDistFile, sTmp, pCnty, pCnty, 'F');
   if (_access(sDistFile, 0))
   {
      LogMsg("***** File not found: %s", sDistFile);
      return -1;
   }

   // Init global
   GetPrivateProfileString(pCnty, "UseFeePrcl", "Y", sTmp, _MAX_PATH, acIniFile);
   if (sTmp[0] == 'Y')
      m_bUseFeePrcl = true;
   else
      m_bUseFeePrcl = false;

   // Verify temp folder
   sprintf(sTmp, "%s\\%s", acTmpPath, pCnty);
   if (_access(sTmp, 0))
      _mkdir(sTmp);

   // Verify import folder
   NameTaxCsvFile(sTmp, pCnty, "Base");
   char *pTmp = strrchr(sTmp, '\\');
   *pTmp = 0;
   if (_access(sTmp, 0))
      _mkdir(sTmp);

   // Create output file names
   NameTaxCsvFile(sBaseFile, pCnty, "Base");
   NameTaxCsvFile(sDetailFile, pCnty, "Items");
   NameTaxCsvFile(sDelqFile, pCnty, "Delq");
   NameTaxCsvFile(sAgencyFile, pCnty, "Agency");

   // Load Base & Delq
   iRet = TC_LoadTaxBaseAndDelq(pCnty, sTaxFile, sBaseFile, sDelqFile);
   if (iRet < 0)
      return iRet;

   if (!iTaxYear)
      iTaxYear = lTaxYear;

   // Load Agency & Detail
   iRet = TC_LoadTaxAgencyAndDetail(pCnty, sDistFile, sAgencyFile, sDetailFile, false, bIgnoreZero);
   if (iRet < 0)
      return iRet;

   // Import into SQL
   if (bImport)
   {
      iRet = doTaxImport(pCnty, TAX_BASE);
      if (!iRet)
      {
         iRet = doTaxImport(pCnty, TAX_DETAIL);
         if (!iRet)
         {
            // Populate TotalRate - EXEC spUpdateTotalRate 'EDX'
            if (!iNumTRADist)
               iRet = doUpdateTotalRate(pCnty);

            if (iTaxDelq > 0)
            {
               iRet = doTaxImport(pCnty, TAX_DELINQUENT);
               // Update Delq flag in Tax_Base
               if (!iRet)
                  iRet = updateDelqFlag(pCnty);
            } else
               doTaxPrep(pCnty, TAX_DELINQUENT);         // Create empty Tax_Delq table

            iRet = doTaxImport(pCnty, TAX_AGENCY);

            // Create empty table
            //iRet = doTaxPrep(pCnty, TAX_OWNER);
         }
      }
   } else
      iRet = 0;

   if (iNewAgency > 0)
      iRet = 99;        // signal send mail
   return iRet;
}

int Update_TC(char *pCnty, bool bImport, bool bLoadDetail)
{
   char  sTaxFile[_MAX_PATH], sDistFile[_MAX_PATH], sDetailFile[_MAX_PATH], sTmp[_MAX_PATH], sTmpCnty[8];
   char  sAgencyFile[_MAX_PATH], sBaseFile[_MAX_PATH], sDelqFile[_MAX_PATH], sSortFile[_MAX_PATH];
   int   iRet;

   LogMsg0("Update TC files for %s", pCnty);

   // Set flag to include tax rate in Agency table
   iRet = GetIniString(pCnty, "TaxRate", "Y", sTmp, _MAX_PATH, acIniFile);
   if (sTmp[0] == 'Y')
      bInclTaxRate = true;
   else
      bInclTaxRate = false;

   // Set flag to ignore zero value
   iRet = GetIniString(pCnty, "IgnoreNoValue", "Y", sTmp, _MAX_PATH, acIniFile);
   if (sTmp[0] == 'Y')
      bIgnoreZero = true;
   else
      bIgnoreZero = false;

   // Get input file names
   GetIniString("Data", "Main_TC", "", sTmp, _MAX_PATH, acIniFile);
   sprintf(sTaxFile, sTmp, pCnty, pCnty, 'P');
   sprintf(sSortFile, sTmp, pCnty, "Sort", 'P');

   // Check file size, ignore small file
   if (getFileSize(sTaxFile) < 500)
      return 0;

   // Save last file date to update County table
   lLastTaxFileDate = getFileDate(sTaxFile);

   // Only process if new tax file
   iRet = isNewTaxFile(sTaxFile, pCnty);
   if (iRet <= 0)
   {
      lLastTaxFileDate = 0;
      return iRet;
   }

   // Sort TC fille if needed
   iRet = doSortTC(sTaxFile, sSortFile);
   if (iRet > 0)
      strcpy(sTaxFile, sSortFile);
   else
      return -2;

   // Load Tax Dist
   iRet = GetIniString(pCnty, "TaxDist", "", sTmp, _MAX_PATH, acIniFile);
   if (iRet > 0)
      iRet = LoadTaxCodeTable(sTmp);

   if (bLoadDetail)
   {
      GetIniString("Data", "Dist_TC", "", sTmp, _MAX_PATH, acIniFile);
      sprintf(sDistFile, sTmp, pCnty, pCnty, 'P');
      if (_access(sDistFile, 0))
      {
         LogMsg("***** File not found: %s", sDistFile);
         return -1;
      }
   }

   // Init global
   GetPrivateProfileString(pCnty, "UseFeePrcl", "Y", sTmp, _MAX_PATH, acIniFile);
   if (sTmp[0] == 'Y')
      m_bUseFeePrcl = true;
   else
      m_bUseFeePrcl = false;

   // Verify temp folder
   sprintf(sTmp, "%s\\%s", acTmpPath, pCnty);
   if (_access(sTmp, 0))
      _mkdir(sTmp);

   // Verify import folder
   NameTaxCsvFile(sTmp, pCnty, "Base");
   char *pTmp = strrchr(sTmp, '\\');
   *pTmp = 0;
   if (_access(sTmp, 0))
      _mkdir(sTmp);

   // Create output file names
   sprintf(sTmpCnty, "Tmp%s", pCnty);
   NameTaxCsvFile(sBaseFile, sTmpCnty, "Base");
   NameTaxCsvFile(sDelqFile, sTmpCnty, "Delq");

   if (!iTaxYear)
      iTaxYear = lTaxYear;

   // Load Base & Delq
   iRet = TC_LoadTaxBaseAndDelq(pCnty, sTaxFile, sBaseFile, sDelqFile);
   if (iRet < 0)
      return iRet;

   // Load Agency & Detail
   if (bLoadDetail)
   {
      NameTaxCsvFile(sDetailFile, sTmpCnty, "Items");
      NameTaxCsvFile(sAgencyFile, sTmpCnty, "Agency");
      iRet = TC_LoadTaxAgencyAndDetail(pCnty, sDistFile, sAgencyFile, sDetailFile, false, bIgnoreZero);
      if (iRet < 0)
         return iRet;

      if (bImport)
         iRet = doTaxImport(sTmpCnty, TAX_UDETAIL);
   }

   // Import into SQL
   if (bImport)
   {
      iRet = doTaxImport(sTmpCnty, TAX_BASE);
      if (!iRet)
      {
         // Update Tax_Base & Tax_Delq 
         if (iTaxDelq > 0)
         {
            iRet = doTaxImport(sTmpCnty, TAX_DELINQUENT);
            // Update Delq flag in Tax_Base
            if (!iRet)
               iRet = updateDelqFlag("Tmp");
            if (!iRet)
               iRet = doTaxMerge(pCnty, bLoadDetail, true);
         } else
            iRet = doTaxMerge(pCnty, bLoadDetail, false);

         // Update TotalRate
         if (!iNumTRADist)
            iRet = doUpdateTotalRate(pCnty);
      }
   } else
      iRet = 0;

   return iRet;
}

/************************* S4_CreateAgencyAndDetail **************************
 *
 * Special case to parse Detail record for SAC
 *
 *****************************************************************************/

int S4_CreateAgencyAndDetail(char *pBaseBuf, FILE *fdDist, FILE *fdDetail, FILE *fdAgency)
{
   static   char acRec[1024], *pRec=NULL;
   char     acDetail[2048], acAgency[1024];
   int      iLoop, iRet;
   double   dTmp, dTax = 0.0;
   TAXBASE   *pOutBase = (TAXBASE *)pBaseBuf;
   TAXDETAIL *pDetail = (TAXDETAIL *)&acDetail[0];

   // Get first rec
   if (!pRec)
   {
      pRec = fgets(acRec, 1024, fdDist);  // Skip header
      pRec = fgets(acRec, 1024, fdDist);  // Get 1st record
   }

   do
   {
      if (!pRec)
      {
         fclose(fdDist);
         fdDist = NULL;
         return 1;      // EOF
      }

      // Compare Apn
      iLoop = memcmp(pBaseBuf, pRec, iApnLen);
      if (iLoop > 0)
      {
         pRec = fgets(acRec, 1024, fdDist);
      }
   } while (iLoop > 0);

#ifdef _DEBUG
   //if (!memcmp(pRec, "0010020064", 10))
   //   iRet = 0;
#endif

   // If not match, return
   if (iLoop  || !strstr(pRec, pOutBase->BillNum))
      return 0;

   do
   {
      iRet = TC_ParseTaxDist_S4(acDetail, acAgency, acRec, '|');
      if (!iRet)
      {
         if (pDetail->TaxAmt[0] > '0')
         {
            dTax += atof(pDetail->TaxAmt);

            // Create delimited record
            Tax_CreateDetailCsv(acRec, (TAXDETAIL *)&acDetail);
            fputs(acRec, fdDetail);

            Tax_CreateAgencyCsv(acRec, (TAXAGENCY *)&acAgency);
            fputs(acRec, fdAgency);
         }
      } else
      {
         LogMsg("---> Drop detail record [%.40s] (error=%d)", acRec, iRet); 
      }

      // Get next  rec
      pRec = fgets(acRec, 1024, fdDist);
   } while (pRec && !memcmp(pBaseBuf, pRec, iApnLen));
   if (!pRec)
   {
      fclose(fdDist);
      fdDist = NULL;
   }

   // Create AV record
   if (dTax > 0.0)
   {
      dTmp = atof(pOutBase->TaxAmt1);
      dTmp += atof(pOutBase->TaxAmt2);
      if (dTmp > dTax)
      {
         sprintf(pDetail->TaxAmt, "%.2f", dTmp - dTax);
         strcpy(pDetail->TaxCode, "0001");
         strcpy(pDetail->TaxDesc, "COUNTY GENERAL TAX");
         pDetail->TC_Flag[0] = '1';
         Tax_CreateDetailCsv(acAgency, (TAXDETAIL *)&acDetail);
         fputs(acAgency, fdDetail);
      }
   }
   return 0;
}

/*************************** TC_ParseTaxData() ********************************
 *
 * DUE_PAID_DATE1 & DUE_PAID_DATE2 may have different format.  Watch out
 *
 * Return < 0 if record is not good or no tax
 *
 ******************************************************************************/

int TC_ParseTaxData(char *pOutbuf, char *pInbuf, char cDelim)
{
   char     acTmp[256], bBillType;
   int      iRet=0, iTmp, iFmt;
   double	dTmp, dTax1, dTax2, dPen1, dPen2, dTaxTotal;
   TAXBASE  *pOutBase = (TAXBASE *)pOutbuf;
   TAXDELQ  *pOutDelq = (TAXDELQ *)pOutbuf;
   TRADIST  *pResult;

   // Parse input tax data
   iTokens = ParseStringIQ(pInbuf, cDelim, 128, apTokens);
   if (iTokens < TC_NET_VAL)
   {
      LogMsg("***** Error: bad Tax record for APN=%.*s (#tokens=%d)", iApnLen, pInbuf, iTokens);
      return -1;
   }

#ifdef _DEBUG
   //if (!memcmp(apTokens[TC_APN_S], "00100200640000", 12))
   //   iTmp = 0;
#endif
   // In no tax year, ignore it
   if (*apTokens[TC_TAX_YR] < '2')
      return 0;

   // Clear output buffer
   memset(pOutBase, 0, sizeof(TAXBASE));
   
   // Default BillType SECURED ANNUAL
   bBillType = BILLTYPE_SECURED;
   pOutBase->isSecd[0] = '1';
   pOutBase->isSupp[0] = 0;
   if (!memcmp(apTokens[TC_ROLL_CAT], "SECURED CORRECTED", 10))
   {
      bBillType = BILLTYPE_ROLL_CORRECTION;
   } else if (!memcmp(apTokens[TC_ROLL_CAT], "SUP", 3))
   {
      pOutBase->isSecd[0] = '0';
      pOutBase->isSupp[0] = '1';
      bBillType = BILLTYPE_SECURED_SUPPL;
   } 

   // Verify date format
   if (*(apTokens[TC_DUE_PAID_DT2]+2) == '/' && strlen(apTokens[TC_DUE_PAID_DT2]) == 10)
      iFmt = MM_DD_YYYY_1;
   else
      iFmt = iDateFmt;

   if (!memcmp(apTokens[TC_PAID_STAT1], "DEL", 3))
   {
      int iDelqYear;

      // Delq Year
      iDelqYear = atol(apTokens[TC_TAX_YR]);
      if (iDelqYear < 1900)
      {
         LogMsg("*** WARNING: No tax data APN=%s", apTokens[TC_APN_S]);
         return -2;
      }

      // Tax year delq
      strcpy(pOutDelq->TaxYear, apTokens[TC_TAX_YR]);

      // APN
      strcpy(pOutDelq->Apn, apTokens[TC_APN_S]);

      // TRA
      iTmp = atol(apTokens[TC_TRA]);
      if (iTmp > 0)
      {
         sprintf(pOutBase->TRA, "%0.6d", iTmp);
         if (iNumTRADist > 0)
         {
            pResult = findTRADist(pOutBase->TRA);
            if (pResult)
               strcpy(pOutBase->TotalRate, pResult->TaxRate);
            else
               LogMsg("Invalid TRA: %s", pOutBase->TRA);
         }
      }

      // BillNum
      if (*apTokens[TC_BILL_NUM] >= '0')
         strcpy(pOutDelq->Default_No, apTokens[TC_BILL_NUM]);
      else if (m_bUseFeePrcl)
         strcpy(pOutDelq->BillNum, apTokens[TC_FEE_PCL]);

      // Default amt
      dTaxTotal = atof(apTokens[TC_TOTAL_DUE]);
      if (dTaxTotal > 0.0)
      {
         iTmp = sprintf(pOutDelq->Def_Amt, "%.2f", dTaxTotal);
         pOutDelq->isDelq[0] = '1';
         if (iTokens >= TC_DEF_DATE)
         {
            if (*apTokens[TC_DEF_DATE] >= '0' && dateConversion(apTokens[TC_DEF_DATE], acTmp, iFmt))
            {
               strcpy(pOutDelq->Def_Date, acTmp);
               iDelqYear = atoin(acTmp, 4)-1;
               sprintf(pOutDelq->TaxYear, "%d", iDelqYear);
            }
         }

         // Paid amt
         dTmp = atof(apTokens[TC_TOTAL_PAID]);
         if (dTmp > 0.0)
         {
            iTmp = sprintf(pOutDelq->Red_Amt, "%.2f", dTmp);
            if (dTaxTotal == dTmp)
            {
               pOutDelq->DelqStatus[0] = TAX_STAT_REDEEMED;       // Redeemed
               pOutDelq->isDelq[0] = '0';
               if (iTokens >= TC_DEF_DATE)
               {
                  if (*apTokens[TC_RED_DATE] >= '0' && dateConversion(apTokens[TC_RED_DATE], acTmp, iFmt))
                     strcpy(pOutDelq->Red_Date, acTmp);
               }
            } else if (dTaxTotal > dTmp)
               pOutDelq->DelqStatus[0] = TAX_STAT_ONPAYMENT;      // On payment plan
         } else
         {
            pOutDelq->DelqStatus[0] = TAX_STAT_UNPAID;            // Unpaid
         }
      }

      iRet = 2;
   } else
   {
      // Tax Year
      iTaxYear = atol(apTokens[TC_TAX_YR]);
      if (iTaxYear != lTaxYear && pOutBase->isSecd[0] == '1')
         return 0;
      sprintf(pOutBase->TaxYear, "%d", iTaxYear);

      // APN
      strcpy(pOutBase->Apn, apTokens[TC_APN_S]);

      // BillNum
      if (*apTokens[TC_BILL_NUM] >= '0')
         strcpy(pOutBase->BillNum, apTokens[TC_BILL_NUM]);
      else if (m_bUseFeePrcl)
      {
         if (pOutBase->isSecd[0] == '1')
            sprintf(pOutBase->BillNum, "%s-01", apTokens[TC_APN_S]);
         else
            sprintf(pOutBase->BillNum, "%s-02", apTokens[TC_APN_S]);
      } else
         sprintf(pOutBase->BillNum, "%s", apTokens[TC_APN_S]);

      // BillType
      pOutBase->BillType[0] = bBillType;

      // TRA
      iTmp = atol(apTokens[TC_TRA]);
      if (iTmp > 0)
      {
         sprintf(pOutBase->TRA, "%0.6d", iTmp);
         if (iNumTRADist > 0)
         {
            pResult = findTRADist(pOutBase->TRA);
            if (pResult)
               strcpy(pOutBase->TotalRate, pResult->TaxRate);
            else
               LogMsg("Invalid TRA: %s", pOutBase->TRA);
         }
      }

      // Check for Tax amount
      dTax1 = dTax2 = 0.0;
      if (*apTokens[TC_TAX_DUE1] > '0')
         dTax1 = atof(apTokens[TC_TAX_DUE1]);

      if (*apTokens[TC_TAX_DUE2] > '0')
         dTax2 = atof(apTokens[TC_TAX_DUE2]);

      if (dTax1 > 0.0)
         sprintf(pOutBase->TaxAmt1, "%.2f", dTax1);
      if (dTax2 > 0.0)
         sprintf(pOutBase->TaxAmt2, "%.2f", dTax2);

      if (*apTokens[TC_TOTAL_TAX] > '0')
         dTaxTotal = atof(apTokens[TC_TOTAL_TAX]);
      else
         dTaxTotal = dTax1+dTax2;

      if (dTaxTotal > 0.0)
         sprintf(pOutBase->TotalTaxAmt, "%.2f", dTaxTotal);

      // Penalty
      dPen1 = atof(apTokens[TC_PEN_DUE1]);
      if (dPen1 > 0.0)
         sprintf(pOutBase->PenAmt1, "%.2f", dPen1);
      dPen2 = atof(apTokens[TC_PEN_DUE2]);
      if (dPen2 > 0.0)
         sprintf(pOutBase->PenAmt2, "%.2f", dPen2);

      // Paid/Due - SAC: PAID/UNPAID/PENDING/DUE/CANCELED/OUTSTANDING
      if (!memcmp(apTokens[TC_PAID_STAT2], "PA", 2))
      {
         InstDueDate(pOutBase->DueDate2, 2, iTaxYear);
         strcpy(pOutBase->PaidAmt2, pOutBase->TaxAmt2);
         if (*apTokens[TC_DUE_PAID_DT2] >= '0' && dateConversion(apTokens[TC_DUE_PAID_DT2], acTmp, iFmt))
            strcpy(pOutBase->PaidDate2, acTmp);
         else
            LogMsg("*** Bad PaidDate 2: %s [%s]", apTokens[TC_DUE_PAID_DT2], apTokens[TC_APN_S]);

         pOutBase->Inst2Status[0] = TAX_BSTAT_PAID;
         pOutBase->Inst1Status[0] = TAX_BSTAT_PAID;
      } else if (!memcmp(apTokens[TC_PAID_STAT2], "CA", 2))    // Cancel
         pOutBase->Inst2Status[0] = TAX_BSTAT_CANCEL;
      else if (!dTax2)                                         // No tax
         pOutBase->Inst2Status[0] = TAX_BSTAT_NOTAX;
      else
      {
         if (*apTokens[TC_DUE_PAID_DT2] >= '0' && dateConversion(apTokens[TC_DUE_PAID_DT2], acTmp, iFmt))
            strcpy(pOutBase->DueDate2, acTmp);
         //else if (pOutBase->isSupp[0] == '1')
         //   strcpy(pOutBase->DueDate2, "20170630");
         else
            InstDueDate(pOutBase->DueDate2, 2, iTaxYear);

         if (ChkDueDate(2, pOutBase->DueDate2))
            pOutBase->Inst2Status[0] = TAX_BSTAT_PASTDUE;
         else
            pOutBase->Inst2Status[0] = TAX_BSTAT_UNPAID;
      } 

      // Verify date format
      if (*(apTokens[TC_DUE_PAID_DT1]+2) == '/' && strlen(apTokens[TC_DUE_PAID_DT1]) == 10)
         iFmt = MM_DD_YYYY_1;
      else
         iFmt = iDateFmt;

      // Paid Inst1
      if (!memcmp(apTokens[TC_PAID_STAT1], "PA", 2))
      {  
         InstDueDate(pOutBase->DueDate1, 1, iTaxYear);
         strcpy(pOutBase->PaidAmt1, pOutBase->TaxAmt1);
         if (*apTokens[TC_DUE_PAID_DT1] >= '0' && dateConversion(apTokens[TC_DUE_PAID_DT1], acTmp, iFmt))
            strcpy(pOutBase->PaidDate1, acTmp);
         else if (pOutBase->PaidDate2[0] >= '0')
            // Sometimes when both installments are paid at the same time, people only keys in date2
            strcpy(pOutBase->PaidDate1, pOutBase->PaidDate2);
         else 
            LogMsg("*** Bad PaidDate 1: %s [%s]", apTokens[TC_DUE_PAID_DT1], apTokens[TC_APN_S]);

         pOutBase->Inst1Status[0] = TAX_BSTAT_PAID;
      } else if (!memcmp(apTokens[TC_PAID_STAT1], "CA", 2))    // Cancel
         pOutBase->Inst1Status[0] = TAX_BSTAT_CANCEL;
      else if (!dTax1)                                         // No tax
         pOutBase->Inst1Status[0] = TAX_BSTAT_NOTAX;
      else 
      {
         if (*apTokens[TC_DUE_PAID_DT1] >= '0' && dateConversion(apTokens[TC_DUE_PAID_DT1], acTmp, iFmt))
            strcpy(pOutBase->DueDate1, acTmp);
         //else if (pOutBase->isSupp[0] == '1')
         //   strcpy(pOutBase->DueDate1, "20170228");
         else
            InstDueDate(pOutBase->DueDate1, 1, iTaxYear);

         if (ChkDueDate(1, pOutBase->DueDate1))
            pOutBase->Inst1Status[0] = TAX_BSTAT_PASTDUE;
         else
            pOutBase->Inst1Status[0] = TAX_BSTAT_UNPAID;
      } 

      // Fee Paid
      dTmp = atof(apTokens[TC_OTHER_FEES]);
      if (dTmp > 0.0)
         iTmp = sprintf(pOutBase->TotalFees, "%.2f", dTmp);

      // Calculate unpaid amt
      dTmp = atof(apTokens[TC_TOTAL_DUE]);
      if (dTmp > 0.0)
         iTmp = sprintf(pOutBase->TotalDue, "%.2f", dTmp);

      iRet = 1;
   }

   // Update date
   if (iTokens > TC_UPD_DATE && *apTokens[TC_UPD_DATE] > ' ' && dateConversion(apTokens[TC_UPD_DATE], acTmp, MM_DD_YYYY_1))
   {
      if (iRet == 1)
         strcpy(pOutBase->Upd_Date, acTmp);
      else
         strcpy(pOutDelq->Upd_Date, acTmp);
   } else
   {
      if (iRet == 1)
         sprintf(pOutBase->Upd_Date, "%d", lLastTaxFileDate);
      else
         sprintf(pOutDelq->Upd_Date, "%d", lLastTaxFileDate);
   }

   if (dTaxTotal == 0)
      iRet = 0;
   return iRet;
}

/******************************* TC_LoadTaxFiles ******************************
 *
 * Load scraped files and create all tax import files.  This varsion do not import
 *
 * 7/23/2017 Sort output agency file
 *
 ******************************************************************************/

int TC_LoadTaxFiles(char *pCnty, char *pTaxFile, char *pDistFile)
{
   char      sDelqYear[64], sApn[64], sDetailFile[_MAX_PATH], sAgencyFile[_MAX_PATH], sDelqFile[_MAX_PATH], 
             sBaseFile[_MAX_PATH], sTmpAgency[_MAX_PATH], *pTmp, acBuf[2048], acRec[2048], acDetail[1024];
   int       iRet, iParseType=0, iDrop=0;
   long      lBase=0, lCnt=0;
   FILE      *fdTax, *fdDist, *fdBase, *fdDelq, *fdDetail, *fdAgency;
   TAXBASE   *pTaxBase = (TAXBASE *)acBuf;
   TAXDELQ   *pTaxDelq = (TAXDELQ *)acBuf;
   TAXDETAIL *pDetail  = (TAXDETAIL *)acDetail;

   // Open input file
   LogMsg("Open Tax file %s", pTaxFile);
   fdTax = fopen(pTaxFile, "r");
   if (fdTax == NULL)
   {
      LogMsg("***** Error opening Tax file: %s\n", pTaxFile);
      return -2;
   }  

   // Open input file
   LogMsg("Open DIST file %s", pDistFile);
   fdDist = fopen(pDistFile, "r");
   if (!fdDist)
   {
      LogMsg("***** Error opening Tax file: %s\n", pDistFile);
      return -2;
   }  

   // Create output file names
   NameTaxCsvFile(sBaseFile, pCnty, "Base");
   NameTaxCsvFile(sDetailFile, pCnty, "Items");
   NameTaxCsvFile(sDelqFile, pCnty, "Delq");
   NameTaxCsvFile(sAgencyFile, pCnty, "Agency");

   // Open Detail output
   LogMsg("Open Detail file %s", sDetailFile);
   fdDetail = fopen(sDetailFile, "w");
   if (!fdDetail)
   {
      LogMsg("***** Error creating Detail file: %s\n", sDetailFile);
      return -4;
   }

   // Open Agency output
   sprintf(sTmpAgency, "%s\\%s\\%s_Agency.tmp", acTmpPath, pCnty, pCnty);
   LogMsg("Open Agency file %s", sTmpAgency);
   fdAgency = fopen(sTmpAgency, "w");
   if (!fdAgency)
   {
      LogMsg("***** Error creating temp Agency file: %s\n", sTmpAgency);
      return -4;
   }

   // Open Base Output file
   LogMsg("Open Base output file %s", sBaseFile);
   fdBase = fopen(sBaseFile, "w");
   if (fdBase == NULL)
   {
      LogMsg("***** Error creating output file: %s\n", sBaseFile);
      return -3;
   }

   // Open Delq Output file
   LogMsg("Open Delq output file %s", sDelqFile);
   fdDelq = fopen(sDelqFile, "w");
   if (fdDelq == NULL)
   {
      LogMsg("***** Error creating output file: %s\n", sDelqFile);
      return -4;
   }
   iTaxDelq = 0;

   // Skip header record - file is sorted, no header to skip
   pTmp = fgets((char *)&acRec[0], 2048, fdTax);
   
   sApn[0] = 0;
   sDelqYear[0] = 0;

   // Merge loop 
   while (!feof(fdTax))
   {
      pTmp = fgets((char *)&acRec[0], 2048, fdTax);
      if (!pTmp)
         break;

#ifdef _DEBUG
      //if (!memcmp(acRec, "00100200100000", 12))
      //   iRet = 0;
#endif

      // Create new TAXBASE record
      iRet = TC_ParseTaxData(acBuf, acRec, '|');

      if (iRet == 1)          // Secured
      {
         if (!strcmp(sApn, pTaxBase->Apn))
         {
            strcpy(pTaxBase->DelqYear, sDelqYear);
            pTaxBase->isDelq[0] = '1';
            sDelqYear[0] = 0;
         }
         Tax_CreateTaxBaseCsv(acRec, (TAXBASE *)&acBuf);
         fputs(acRec, fdBase);
         lBase++;
         sApn[0] = 0;

         // Parse detail & agency
         if (fdDist)
            iRet = S4_CreateAgencyAndDetail(acBuf, fdDist, fdDetail, fdAgency);

      } else if (iRet == 2)   // Delq 
      {
         Tax_CreateDelqCsv(acRec, (TAXDELQ *)&acBuf);
         fputs(acRec, fdDelq);
         iTaxDelq++;

         // Save DelqYear
         strcpy(sDelqYear, pTaxDelq->TaxYear);
         strcpy(sApn, pTaxDelq->Apn);
      } else
         iDrop++;

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Add general agency
   TAXAGENCY *pAgency  = (TAXAGENCY *)&acBuf[0];
   memset(acBuf, 0, sizeof(TAXAGENCY));
   strcpy(pAgency->Code, "0001");
   strcpy(pAgency->Agency, "COUNTY GENERAL TAX");
   pAgency->TC_Flag[0] = '1';

   Tax_CreateAgencyCsv(acRec, pAgency);
   fputs(acRec, fdAgency);

   // Close files
   if (fdTax)
      fclose(fdTax);
   if (fdDist)
      fclose(fdDist);
   if (fdBase)
      fclose(fdBase);
   if (fdDelq)
      fclose(fdDelq);
   if (fdDetail)
      fclose(fdDetail);
   if (fdAgency)
      fclose(fdAgency);

   // Dedup Agency file
   iRet = sortFile(sTmpAgency, sAgencyFile, "S(#1,C,A,#2,C,A) DUPOUT(#1) F(TXT) DEL(124)");

   LogMsg("Total records processed: %u", lCnt);
   LogMsg("Total records droped:    %u", iDrop);
   LogMsg("Total Base output:       %u", lBase);
   LogMsg("Total Delq output:       %u", iTaxDelq);
   LogMsg("Total new agency:        %u", iNewAgency);

   printf("\nTotal output records: %u", lCnt);

   if (lBase > 100)
      iRet = 0;
   else
      iRet = -10;

   return iRet;
}

/******************************* TC_LoadTaxBase *******************************
 *
 * Load Main_TC and create Tax Base only.
 *
 * For ORG, SLO
 *
 ******************************************************************************/

int TC_LoadTaxBase(char *pCnty, char *pTaxFile, bool bImport)
{
   char      sBaseFile[_MAX_PATH],  *pTmp, acBuf[2048], acRec[2048];
   int       iRet, iParseType=0, iDrop=0;
   long      lBase=0, lCnt=0;
   FILE      *fdTax, *fdBase;
   TAXBASE   *pTaxBase = (TAXBASE *)acBuf;

   // Open input file
   LogMsg0("Open Tax file %s", pTaxFile);
   fdTax = fopen(pTaxFile, "r");
   if (fdTax == NULL)
   {
      LogMsg("***** Error opening Tax file: %s\n", pTaxFile);
      return -2;
   }  

   // Create output file names
   NameTaxCsvFile(sBaseFile, pCnty, "Base");

   // Open Base Output file
   LogMsg("Open Base output file %s", sBaseFile);
   fdBase = fopen(sBaseFile, "w");
   if (fdBase == NULL)
   {
      LogMsg("***** Error creating output file: %s\n", sBaseFile);
      return -3;
   }

   // Skip header record - file is sorted, no header to skip
   pTmp = fgets((char *)&acRec[0], 2048, fdTax);
   
   // Merge loop 
   while (!feof(fdTax))
   {
      pTmp = fgets((char *)&acRec[0], 2048, fdTax);
      if (!pTmp)
         break;

#ifdef _DEBUG
      //if (!memcmp(acRec, "00100200100000", 12))
      //   iRet = 0;
#endif

      // Create new TAXBASE record
      iRet = TC_ParseTaxData(acBuf, acRec, '|');
      if (iRet == 1)          // Secured
      {
         Tax_CreateTaxBaseCsv(acRec, (TAXBASE *)&acBuf);
         fputs(acRec, fdBase);
         lBase++;
      } else
         iDrop++;

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdTax)
      fclose(fdTax);
   if (fdBase)
      fclose(fdBase);

   if (lBase > 1000 && bImport)
   {
      iRet = doTaxImport(pCnty, TAX_BASE);
   } else
   {
      LogMsg("***** Number of output records is too small.  Please check input file: %s", pTaxFile);
      iRet = -10;
   }

   LogMsg("Total records processed: %u", lCnt);
   LogMsg("Total records droped:    %u", iDrop);
   LogMsg("Total Base output:       %u", lBase);

   printf("\nTotal output records: %u", lCnt);

   return 0;
}

/******************************* TC_UpdateTaxBase *****************************
 *
 * Load Main_TC into Tmp tables then update Tax Base only

 * TO be tested .......

 * For ORG
 *
 ******************************************************************************/

int TC_UpdateTaxBase(char *pCnty, char *pTaxFile, bool bImport)
{
   char      sBaseFile[_MAX_PATH],  *pTmp, acBuf[2048], acRec[2048];
   int       iRet, iParseType=0, iDrop=0;
   long      lBase=0, lCnt=0;
   FILE      *fdTax, *fdBase;
   TAXBASE   *pTaxBase = (TAXBASE *)acBuf;

   // Open input file
   LogMsg0("Open Tax file %s", pTaxFile);
   fdTax = fopen(pTaxFile, "r");
   if (fdTax == NULL)
   {
      LogMsg("***** Error opening Tax file: %s\n", pTaxFile);
      return -2;
   }  

   // Create output file names
   NameTaxCsvFile(sBaseFile, "Tmp", "Base");
   NameTaxCsvFile(sBaseFile, "Tmp", "Base");

   // Open Base Output file
   LogMsg("Open Base output file %s", sBaseFile);
   fdBase = fopen(sBaseFile, "w");
   if (fdBase == NULL)
   {
      LogMsg("***** Error creating output file: %s\n", sBaseFile);
      return -3;
   }

   // Skip header record - file is sorted, no header to skip
   pTmp = fgets((char *)&acRec[0], 2048, fdTax);
   
   // Merge loop 
   while (!feof(fdTax))
   {
      pTmp = fgets((char *)&acRec[0], 2048, fdTax);
      if (!pTmp)
         break;

#ifdef _DEBUG
      //if (!memcmp(acRec, "00100200100000", 12))
      //   iRet = 0;
#endif

      // Create new TAXBASE record
      iRet = TC_ParseTaxData(acBuf, acRec, '|');
      if (iRet == 1)          // Secured
      {
         Tax_CreateTaxBaseCsv(acRec, (TAXBASE *)&acBuf);
         fputs(acRec, fdBase);
         lBase++;
      } else
         iDrop++;

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdTax)
      fclose(fdTax);
   if (fdBase)
      fclose(fdBase);

   if (bImport)
   {
      iRet = doTaxImport("Tmp", TAX_BASE);
      if (!iRet)
      {
         if (!iRet)
            iRet = doTaxMerge(pCnty, false, false);    // Merge Tax Base only
      }
   }

   LogMsg("Total records processed: %u", lCnt);
   LogMsg("Total records droped:    %u", iDrop);
   LogMsg("Total Base output:       %u", lBase);

   printf("\nTotal output records: %u", lCnt);

   return 0;
}

/******************************** Load_TCC() ***********************************
 *
 * Load scrape data from county tax website.  This version specifically deals with
 * corrected data.
 *
 * Use ???.TC to load Tax_Base, Tax_Delq
 *     ???DIST.TC to load Tax_Agency, Tax_Detail
 *     S???.S01 to load Tax_Owner
 *
 *   - Design for SAC, but not in use since 2021
 *
 ******************************************************************************/

int Load_TCC(char *pCnty, bool bImport)
{
   char  sTaxFile[_MAX_PATH], sDistFile[_MAX_PATH], sTmp[_MAX_PATH], sSortFile[_MAX_PATH];
   int   iRet;
   INT64 lTmp;

   LogMsg0("Loading TC files for %s", pCnty);

   // Set flag to include tax rate in Agency table
   iRet = GetIniString(pCnty, "TaxRate", "Y", sTmp, _MAX_PATH, acIniFile);
   if (sTmp[0] == 'Y')
      bInclTaxRate = true;
   else
      bInclTaxRate = false;

   // Set flag to ignore zero value
   iRet = GetIniString(pCnty, "IgnoreNoValue", "Y", sTmp, _MAX_PATH, acIniFile);
   if (sTmp[0] == 'Y')
      bIgnoreZero = true;
   else
      bIgnoreZero = false;

   // Get input file names
   GetIniString("Data", "Main_TC", "", sTmp, _MAX_PATH, acIniFile);
   sprintf(sTaxFile, sTmp, pCnty, pCnty, 'F');
   sprintf(sSortFile, sTmp, pCnty, "Sort", 'F');
   if (_access(sTaxFile, 0))
   {
      LogMsg("***** File not found: %s", sTaxFile);
      return -1;
   }

   // Check file size, ignore small file
   lTmp = getFileSize(sTaxFile);
   if (lTmp < 1000)
   {
      LogMsg("***** File size is too small (%d).  Skip processing %s", lTmp, sTaxFile);
      return 0;
   }

   // Save last file date to update County table
   lLastTaxFileDate = getFileDate(sTaxFile);

   // Only process if new tax file
   iRet = isNewTaxFile(sTaxFile, pCnty);
   if (iRet <= 0)
   {
      LogMsg("*** Loading old file %s.  Reset Tax date and continue (or set ChkFileDate=N)", sTaxFile);
      lLastTaxFileDate = 0;      // Signal not to update Tax info in County table
      return iRet;
   }

   GetIniString("Data", "Dist_TC", "", sTmp, _MAX_PATH, acIniFile);
   sprintf(sDistFile, sTmp, pCnty, pCnty, 'F');
   if (_access(sDistFile, 0))
   {
      LogMsg("***** File not found: %s", sDistFile);
      return -1;
   }

   // Init global
   GetPrivateProfileString(pCnty, "UseFeePrcl", "Y", sTmp, _MAX_PATH, acIniFile);
   if (sTmp[0] == 'Y')
      m_bUseFeePrcl = true;
   else
      m_bUseFeePrcl = false;

   // Load TC file
   iRet = TC_LoadTaxFiles(pCnty, sTaxFile, sDistFile);

   // Import into SQL
   if (bImport)
   {
      iRet = doTaxImport(pCnty, TAX_BASE);
      if (!iRet)
      {
         if (iTaxDelq > 0)
         {
            iRet = doTaxImport(pCnty, TAX_DELINQUENT);
         }

         iRet = doTaxImport(pCnty, TAX_DETAIL);
         if (!iRet)
         {
            iRet = doTaxImport(pCnty, TAX_AGENCY);

            // Update TotalRate
            if (!iNumTRADist)
               iRet = doUpdateTotalRate(pCnty);
         }
      }
   } else
      iRet = 0;

   return iRet;
}

// For SAC
int Update_TCC(char *pCnty, bool bImport, bool bLoadDetail)
{
   char  sTaxFile[_MAX_PATH], sDistFile[_MAX_PATH], sSortFile[_MAX_PATH], sTmp[_MAX_PATH];
   int   iRet;

   LogMsg0("Update TC files for %s", pCnty);

   // Set flag to include tax rate in Agency table
   iRet = GetIniString(pCnty, "TaxRate", "Y", sTmp, _MAX_PATH, acIniFile);
   if (sTmp[0] == 'Y')
      bInclTaxRate = true;
   else
      bInclTaxRate = false;

   // Set flag to ignore zero value
   iRet = GetIniString(pCnty, "IgnoreNoValue", "Y", sTmp, _MAX_PATH, acIniFile);
   if (sTmp[0] == 'Y')
      bIgnoreZero = true;
   else
      bIgnoreZero = false;

   // Get input file names
   GetIniString("Data", "Main_TC", "", sTmp, _MAX_PATH, acIniFile);
   sprintf(sTaxFile, sTmp, pCnty, pCnty, 'P');
   sprintf(sSortFile, sTmp, pCnty, "Sort", 'P');
   if (_access(sTaxFile, 0))
   {
      LogMsg("***** File not found: %s", sTaxFile);
      return -1;
   }

   // Check file size, ignore small file
   if (getFileSize(sTaxFile) < 1000)
      return 0;

   // Save last file date to update County table
   lLastTaxFileDate = getFileDate(sTaxFile);

   // Only process if new tax file
   iRet = isNewTaxFile(sTaxFile, pCnty);
   if (iRet <= 0)
   {
      lLastTaxFileDate = 0;
      return iRet;
   }

   // Sort TC fille if needed
   iRet = doSortTC(sTaxFile, sSortFile);
   if (iRet > 0)
      strcpy(sTaxFile, sSortFile);
   else
      return -2;

   // Load Tax Dist
   iRet = GetIniString(pCnty, "TaxDist", "", sTmp, _MAX_PATH, acIniFile);
   if (iRet > 0)
      iRet = LoadTaxCodeTable(sTmp);

   if (bLoadDetail)
   {
      GetIniString("Data", "Dist_TC", "", sTmp, _MAX_PATH, acIniFile);
      sprintf(sDistFile, sTmp, pCnty, pCnty, 'P');
      if (_access(sDistFile, 0))
      {
         LogMsg("***** File not found: %s", sDistFile);
         return -1;
      }
   }

   // Init global
   GetPrivateProfileString(pCnty, "UseFeePrcl", "Y", sTmp, _MAX_PATH, acIniFile);
   if (sTmp[0] == 'Y')
      m_bUseFeePrcl = true;
   else
      m_bUseFeePrcl = false;

   // Verify temp folder
   sprintf(sTmp, "%s\\%s", acTmpPath, pCnty);
   if (_access(sTmp, 0))
      _mkdir(sTmp);

   // Verify import folder
   NameTaxCsvFile(sTmp, "Tmp", "Base");
   char *pTmp = strrchr(sTmp, '\\');
   *pTmp = 0;
   if (_access(sTmp, 0))
      _mkdir(sTmp);

   if (!iTaxYear)
      iTaxYear = lTaxYear;

   // Loading tax files
   char *pTmpCnty = "Tmp";
   iRet = TC_LoadTaxFiles(pTmpCnty, sTaxFile, sDistFile);

   // Import into SQL
   if (bImport)
   {
      iRet = doTaxImport(pTmpCnty, TAX_BASE);
      if (!iRet)
      {
         // Import detail
         if (bLoadDetail)
            iRet = doTaxImport(pTmpCnty, TAX_DETAIL);

         // Update Tax_Base & Tax_Delq 
         if (iTaxDelq > 0)
         {
            iRet = doTaxImport(pTmpCnty, TAX_DELINQUENT);
            // Update Delq flag in Tax_Base
            if (!iRet)
               iRet = updateDelqFlag(pTmpCnty);
            if (!iRet)
               iRet = doTaxMerge(pCnty, bLoadDetail, true);
         } else
            iRet = doTaxMerge(pCnty, bLoadDetail, false);

         // Update TotalRate
         if (!iNumTRADist)
            iRet = doUpdateTotalRate(pCnty);
      }
   } else
      iRet = 0;

   return iRet;
}

 /***************************** updateTaxAgency ********************************
 *
 * Merge tax agencies from county agency list table to Tax_Agency table
 *
 * Return 0 if successful, else error
 *
 ******************************************************************************/

//int updateTaxAgency(char *pCnty)
//{
//   char acTmp[128];
//   int  iRet;
//
//   // Update CO3_Tax_Agency table
//   sprintf(acTmp, "EXEC [dbo].[spUpdateAgencies] '%s'", pCnty);
//   iRet = updateTaxDB(acTmp);
//
//   return iRet;
//}

/******************************** updateDelqFlag *******************************
 *
 * Update tb_isDelq to be the same as td_isDelq.
 * if bChkRedeem=true, reset tb_isDelq to 0 if td_isDelq is 0
 * if bChkDelq=true, set tb_isDelq & tb_DelqYear if td_isDelq is 1
 *
 * 03/25/2023 Remove special case for MPA
 *
 ******************************************************************************/

int updateBaseDelq(char *pCnty, bool bChkDelq, char *pServerName)
{
   char acTmp[256];
   int  iRet;

   if (bChkDelq)
   {
      sprintf(acTmp, "EXEC [dbo].[spUpdateBaseDelq] '%s'", pCnty);
      if (bExpSql)
      {
         fputs("PRINT 'UpdateBaseDelq: ' + CONVERT(varchar,GETDATE(),21);\n", fdExpSql);
         strcat(acTmp, ";\n");
         fputs(acTmp, fdExpSql);
         iRet = 0;
      } else
         iRet = updTaxDB(acTmp, pServerName);
   }

   return iRet;
}

// MPA
int updateBaseDelqX1(char *pCnty, bool bChkDelq, char *pServerName)
{
   char acTmp[128];
   int  iRet;

   if (bChkDelq)
   {
      sprintf(acTmp, "EXEC [dbo].[spUpdateBaseDelqX1] '%s'", pCnty);
      if (bExpSql)
      {
         fputs("PRINT 'UpdateBaseDelqX1: ' + CONVERT(varchar,GETDATE(),21);\n", fdExpSql);
         strcat(acTmp, ";\n");
         fputs(acTmp, fdExpSql);
         iRet = 0;
      } else
         iRet = updTaxDB(acTmp, pServerName);
   }

   return iRet;
}

int updateBaseDelqX2(char *pCnty, bool bChkDelq, char *pServerName)
{
   char acTmp[128];
   int  iRet;

   if (bChkDelq)
   {
      sprintf(acTmp, "EXEC [dbo].[spUpdateBaseDelqX2] '%s'", pCnty);
      if (bExpSql)
      {
         fputs("PRINT 'UpdateBaseDelqX2: ' + CONVERT(varchar,GETDATE(),21);\n", fdExpSql);
         strcat(acTmp, ";\n");
         fputs(acTmp, fdExpSql);
         iRet = 0;
      } else
         iRet = updTaxDB(acTmp, pServerName);
   }

   return iRet;
}

// SUT
int updateBaseDelqX3(char *pCnty, bool bChkDelq, char *pServerName)
{
   char acTmp[128];
   int  iRet;

   if (bChkDelq)
   {
      sprintf(acTmp, "EXEC [dbo].[spUpdateBaseDelqX3] '%s'", pCnty);
      if (bExpSql)
      {
         fputs("PRINT 'UpdateBaseDelqX3: ' + CONVERT(varchar,GETDATE(),21);\n", fdExpSql);
         strcat(acTmp, ";\n");
         fputs(acTmp, fdExpSql);
         iRet = 0;
      } else
         iRet = updTaxDB(acTmp, pServerName);
   }

   return iRet;
}

int updateDelqFlag(char *pCnty, bool bChkRedeem, bool bChkDelq, int iSpecialCase)
{
   char acServerName[64], acTmp[256];
   int  iRet;

   LogMsg0("Update Delq flag on Base table using TaxDelq");

   iRet = GetIniString("Database", "TaxSvr1", "", acServerName, _MAX_PATH, acIniFile);
   if (iRet > 1)
   {
      if (bChkRedeem)
      {
         sprintf(acTmp, "EXEC [dbo].[spResetBaseDelq] '%s'", pCnty);
         if (bExpSql)
         {
            fputs("PRINT 'ResetBaseDelq: ' + CONVERT(varchar,GETDATE(),21);\n", fdExpSql);
            strcat(acTmp, ";\n");
            fputs(acTmp, fdExpSql);
            iRet = 0;
         } else
            iRet = updTaxDB(acTmp, acServerName);
      }

      switch (iSpecialCase)
      {
         case 1:
            iRet = updateBaseDelqX1(pCnty, bChkDelq, acServerName);
            break;
         case 2:
            iRet = updateBaseDelqX2(pCnty, bChkDelq, acServerName);
            break;
         case 3:
            iRet = updateBaseDelqX3(pCnty, bChkDelq, acServerName);
            break;
         default:
            iRet = updateBaseDelq(pCnty, bChkDelq, acServerName);
      }
   }

   if (bExpSql)
      return iRet;

   iRet = GetIniString("Database", "TaxSvr2", "", acServerName, _MAX_PATH, acIniFile);
   if (iRet > 1)
   {
      if (bChkRedeem)
      {
         sprintf(acTmp, "EXEC [dbo].[spResetBaseDelq] '%s'", pCnty);
         if (bExpSql)
         {
            fputs("PRINT 'ResetBaseDelq: ' + CONVERT(varchar,GETDATE(),21);\n", fdExpSql);
            strcat(acTmp, ";\n");
            fputs(acTmp, fdExpSql);
            iRet = 0;
         } else
            iRet = updTaxDB(acTmp, acServerName);
      }

      switch (iSpecialCase)
      {
         case 1:
            iRet = updateBaseDelqX1(pCnty, bChkDelq, acServerName);
            break;
         case 2:
            iRet = updateBaseDelqX2(pCnty, bChkDelq, acServerName);
            break;
         case 3:
            iRet = updateBaseDelqX3(pCnty, bChkDelq, acServerName);
            break;
         default:
            iRet = updateBaseDelq(pCnty, bChkDelq, acServerName);
      }
   }

   iRet = GetIniString("Database", "TaxSvr3", "", acServerName, _MAX_PATH, acIniFile);
   if (iRet > 1)
   {
      if (bChkRedeem)
      {
         sprintf(acTmp, "EXEC [dbo].[spResetBaseDelq] '%s'", pCnty);
         if (bExpSql)
         {
            fputs("PRINT 'ResetBaseDelq: ' + CONVERT(varchar,GETDATE(),21);\n", fdExpSql);
            strcat(acTmp, ";\n");
            fputs(acTmp, fdExpSql);
            iRet = 0;
         } else
            iRet = updTaxDB(acTmp, acServerName);
      }

      switch (iSpecialCase)
      {
         case 1:
            iRet = updateBaseDelqX1(pCnty, bChkDelq, acServerName);
            break;
         case 2:
            iRet = updateBaseDelqX2(pCnty, bChkDelq, acServerName);
            break;
         case 3:
            iRet = updateBaseDelqX3(pCnty, bChkDelq, acServerName);
            break;
         default:
            iRet = updateBaseDelq(pCnty, bChkDelq, acServerName);
      }
   }

   return iRet;
}

/***************************** update_TI_BillNum ******************************
 *
 * Update BillNum on Tax_Items using data from Tax_Base
 *
 * Return 0 if successful, else error
 *
 ******************************************************************************/

int update_TI_BillNum(char *pCnty, char *pVersion)
{
   char acServerName[64], acTmp[128];
   int  iRet;

   LogMsg0("Update Bill Number on Tax_Items using Tax_Base");

   if (pVersion && *pVersion > '0')
      sprintf(acTmp, "EXEC [dbo].[spUpdateBillNum%s] '%s'", pVersion, pCnty);
   else
      sprintf(acTmp, "EXEC [dbo].[spUpdateBillNum] '%s'", pCnty);

   if (bExpSql)
   {
      fputs("PRINT 'UpdateBillNum: ' + CONVERT(varchar,GETDATE(),21);\n", fdExpSql);
      strcat(acTmp, ";\n");
      fputs(acTmp, fdExpSql);
      return 0;
   }

   iRet = GetIniString("Database", "TaxSvr1", "", acServerName, _MAX_PATH, acIniFile);
   if (iRet > 1)
      iRet = updTaxDB(acTmp, acServerName);

   iRet = GetIniString("Database", "TaxSvr2", "", acServerName, _MAX_PATH, acIniFile);
   if (iRet > 1)
      iRet = updTaxDB(acTmp, acServerName);

   iRet = GetIniString("Database", "TaxSvr3", "", acServerName, _MAX_PATH, acIniFile);
   if (iRet > 1)
      iRet = updTaxDB(acTmp, acServerName);

   return iRet;
}

/****************************** LoadTaxCodeTable *****************************
 *
 * Load a delimited file into TAXAGENCY table for translation
 * File format: TaxCode|Desc|Phone|TaxRate|TaxAmt|TCFlag
 * or           TaxCode|CodeXL|Desc|Phone|TaxRate|TaxAmt|TCFlag
 *  
 * Use TaxGet*() functions to access and retrieve data.
 *
 *****************************************************************************/

int LoadTaxCodeTable(char *pXrefFile, int iCodeXL)
{
   char      acTmp[_MAX_PATH], *pTmp, *apItems[16];
   int       iCnt;
   FILE      *fd;
   TAXAGENCY *pXrefTbl;

   LogMsg0("Loading TaxCode table: %s", pXrefFile);
   iNumTaxDist = 0;
   fd = fopen(pXrefFile, "r");
   if (fd)
   {
      pXrefTbl = &asTaxDist[0];
      // Skip header
      pTmp = fgets(acTmp, _MAX_PATH, fd);

      // Loading table
      while (!feof(fd))
      {
         pTmp = fgets(acTmp, _MAX_PATH, fd);
         if (!pTmp)
            break;

         iCnt = ParseStringIQ(acTmp, '|', 15, apItems);

         if (iCnt > 5)
         {
            strcpy(pXrefTbl->Code, apItems[0]);
            pXrefTbl->iCodeLen = strlen(apItems[0]);

            strcpy(pXrefTbl->CodeXL, apItems[iCodeXL+0]);
            strcpy(pXrefTbl->Agency, myTrim(apItems[iCodeXL+1]));
            pXrefTbl->iNameLen = strlen(apItems[iCodeXL+1]);
            strcpy(pXrefTbl->Phone, apItems[iCodeXL+2]);
            strcpy(pXrefTbl->TaxRate, apItems[iCodeXL+3]);
            strcpy(pXrefTbl->TaxAmt, apItems[iCodeXL+4]);
            strcpy(pXrefTbl->TC_Flag, apItems[iCodeXL+5]);

            iNumTaxDist++;
            pXrefTbl++;
            if (iNumTaxDist >= MAX_TAX_DISTRICT)
            {
               pXrefTbl->Code[0] = 0;
               pXrefTbl->iNameLen = 0;
               pXrefTbl->iCodeLen = 0;
               break;
            }
         } else
         {
            LogMsg0("*** Invalid entries at: %s (%d)", pTmp, iNumTaxDist);
            break;
         }
      }

      fclose(fd);
   } else
      LogMsg("***** Error opening Tax district file %s", pXrefFile);

   LogMsg("Number of district loaded: %d", iNumTaxDist);

   return iNumTaxDist;
}

/****************************** LoadTRADistTable *****************************
 *
 * Load a delimited file into TRADIST table for translation
 * File format: TRA|District Code|District Count
 *  
 * Use findTaxAgency() to access and retrieve data.
 *
 *****************************************************************************/

int LoadTRADistTable(char *pXrefFile)
{
   char     acTmp[1024], acCode[1024], *pTmp, *apItems[100];
   int      iIdx, iCnt, iMaxCnt;
   FILE     *fd;
   TRADIST  *pXrefTbl;

   LogMsg0("Loading TRA_Dist table: %s", pXrefFile);
   iNumTRADist = 0;
   fd = fopen(pXrefFile, "r");
   if (fd)
   {
      pXrefTbl = &asTRADist[0];

      // Loading table
      while (!feof(fd))
      {
         pTmp = fgets(acTmp, 1024, fd);
         if (!pTmp)
            break;
         if (*pTmp > '9')
            continue;

         iCnt = ParseStringIQ(acTmp, '|', 15, apItems);

         if (iCnt == 3)
         {
            strcpy(pXrefTbl->TRA, apItems[0]);
            strcpy(acCode, apItems[1]);
            iMaxCnt = atol(apItems[2]);

            iCnt = ParseStringIQ(acCode, ',', 100, apItems);
            if (iCnt != iMaxCnt)
               LogMsg("*** Number of entries mismatched: TRA=%s, %s %d != %d", pXrefTbl->TRA, acCode, iCnt, iMaxCnt);
            if (iCnt > MAX_TRA_DIST)
            {
               LogMsg("***** ERROR: Number of entries is larger than storage: %d > %d.  Plese contact Sony.", iCnt, MAX_TRA_DIST);
               break;
            }

            for (iIdx = 0; iIdx < iCnt; iIdx++)
            {
               strcpy(pXrefTbl->Code[iIdx], apItems[iIdx]);
            }

            pXrefTbl->iCount = iCnt;
            iNumTRADist++;
            pXrefTbl++;
         } else
         {
            LogMsg0("*** Invalid entries at: %s (%d)", pTmp, iNumTRADist);
            break;
         }
      }

      fclose(fd);
   } else
      LogMsg("***** Error opening Tax district file %s", pXrefFile);

   LogMsg("Number of TRA loaded: %d", iNumTRADist);

   return iNumTRADist;
}

/****************************** LoadTaxRateTable *****************************
 *
 * Load a delimited file "Rate Book 19-20.txt" into TRADIST table for translation
 * File format: TRA|Tax Rate
 *  
 *****************************************************************************/

int LoadTaxRateTable(char *pXrefFile)
{
   char     acTmp[128], *pTmp, *apItems[4];
   int      iCnt;
   FILE     *fd;
   TRADIST  *pXrefTbl;

   LogMsg0("Loading Tax Rate table: %s", pXrefFile);
   iNumTRADist = 0;
   fd = fopen(pXrefFile, "r");
   if (fd)
   {
      pXrefTbl = &asTRADist[0];

      // Loading table
      while (!feof(fd))
      {
         pTmp = fgets(acTmp, 128, fd);
         if (!pTmp)
            break;
         if (*pTmp > '9')
            continue;

         iCnt = ParseStringIQ(acTmp, 9, 4, apItems);

         if (iCnt > 1)
         {
            remChar(apItems[0], '-');
            strcpy(pXrefTbl->TRA, apItems[0]);
            strcpy(pXrefTbl->TaxRate, myTrim(apItems[1]));
            iNumTRADist++;
            pXrefTbl++;
         } else
         {
            LogMsg0("*** Invalid entries at: %s (%d)", pTmp, iNumTRADist);
            break;
         }
      }

      fclose(fd);
   } else
      LogMsg("***** Error opening Tax district file %s", pXrefFile);

   LogMsg("Number of TRA loaded: %d", iNumTRADist);

   return iNumTRADist;
}

/*************************** Tax_CreateAgencyTable ***************************
 *
 * Load a delimited file into SQL table CO3_TAX_AGENCY
 * File format: TaxCode|Desc|Phone|TaxRate|TaxAmt
 * or           TaxCode|CodeXL|Desc|Phone|TaxRate|TaxAmt
 *  
 *****************************************************************************/

int Tax_CreateAgencyTable(char *pCnty, char *pXrefFile, int iCodeXL)
{
   char      acTmp[512], acAgencyFile[_MAX_PATH], acAgencyRec[512], *pTmp, *apItems[16];
   int       iCnt;
   FILE      *fd, *fdAgency;
   TAXAGENCY *pAgency = (TAXAGENCY *)&acAgencyRec[0];

   LogMsg("Loading Tax_Agency table for %s using %s", pCnty, pXrefFile);

   NameTaxCsvFile(acAgencyFile, pCnty, "Agency");
   LogMsg("Create Agency file %s", acAgencyFile);
   fdAgency = fopen(acAgencyFile, "w");
   if (fdAgency == NULL)
   {
      LogMsg("***** Error creating Agency file: %s\n", acAgencyFile);
      return -4;
   }

   iNumTaxDist = 0;
   fd = fopen(pXrefFile, "r");
   if (fd)
   {
      // Skip header
      pTmp = fgets(acTmp, _MAX_PATH, fd);

      // Loading table
      while (!feof(fd))
      {
         pTmp = fgets(acTmp, _MAX_PATH, fd);
         if (!pTmp)
            break;

         iCnt = ParseString(acTmp, '|', 15, apItems);

         if (iCnt > 4)
         {
            memset(acAgencyRec, 0, sizeof(TAXAGENCY));
            strcpy(pAgency->Code, apItems[0]);
            strcpy(pAgency->Agency, myTrim(apItems[iCodeXL+1]));
            strcpy(pAgency->Phone, apItems[iCodeXL+2]);
            strcpy(pAgency->TaxRate, apItems[iCodeXL+3]);

            Tax_CreateAgencyCsv(acTmp, (TAXAGENCY *)&acAgencyRec);
            fputs(acTmp, fdAgency);

            iNumTaxDist++;
         } else
         {
            LogMsg0("*** Invalid entries at: %s (%d)", pTmp, iNumTaxDist);
            break;
         }
      }

      fclose(fd);
   } else
      LogMsg("***** Error opening Tax district file %s", pXrefFile);

   fclose(fdAgency);

   // Import into SQL
   doTaxImport(pCnty, TAX_AGENCY);

   LogMsg("Number of district loaded: %d", iNumTaxDist);

   return iNumTaxDist;
}

/********************************* findTaxDist ********************************
 *
 * Find agency sequentially through unsorted list.
 *
 ******************************************************************************/

TAXAGENCY *findTaxDist(LPCSTR pDist, int iStart)
{
   TAXAGENCY  *pXrefTbl, *pRet=NULL;
   
   if (iStart < iNumTaxDist)
   {
      pXrefTbl = &asTaxDist[iStart];   
      for (; pXrefTbl->iNameLen > 0; pXrefTbl++)
      {
         if (!_memicmp(pDist, pXrefTbl->Agency, pXrefTbl->iNameLen))
         {
            pRet = pXrefTbl;
            break;
         }
      }
   }

   return pRet;
}

/********************************* findTaxDist ********************************
 *
 * Find agency sequentially through sorted list on TaxCode, TaxDist.
 * First search on TaxCode, then TaxDist
 *
 ******************************************************************************/

TAXAGENCY *findTaxDist(LPCSTR pDist, LPCSTR pCode, int iStart)
{
   TAXAGENCY  *pXrefTbl, *pRet=NULL;
   int         iLen=strlen(pCode);

   if (iStart < iNumTaxDist)
   {
      pXrefTbl = &asTaxDist[iStart];   
      for (; pXrefTbl->iNameLen > 0; pXrefTbl++)
      {
         if (!_memicmp(pCode, pXrefTbl->Code, iLen))
         {
            pRet = pXrefTbl;
            break;
         }
      }

      // Now search on Agency
      for (; pXrefTbl->iNameLen > 0; pXrefTbl++)
      {
         if (!_memicmp(pDist, pXrefTbl->Agency, pXrefTbl->iNameLen))
         {
            pRet = pXrefTbl;
            break;
         }
      }
   }

   return pRet;
}

/******************************** findTaxDist *********************************
 *
 * Find Tax district through sorted list using binary search.
 *
 ******************************************************************************/

TAXAGENCY *BSrchTaxDist(LPCSTR pDist)     // Use memcmp()
{
   TAXAGENCY   *pRet=NULL;
   int         iTmp, beg, end, mid;

   beg = 0;
   end = iNumTaxDist-1;
   mid = (beg+end)/2;                       // Find Mid Location of Array

   while (beg <= end && (iTmp = memcmp(asTaxDist[mid].Agency, pDist, asTaxDist[mid].iNameLen)))      // Compare Item and Value of Mid
   {
      if (iTmp < 0)
         beg = mid+1;
      else
         end = mid-1;

      mid = (beg+end)/2;
   }

   if (!iTmp)
   {
      pRet = &asTaxDist[mid];
   }

   return pRet;
}

TAXAGENCY *BSrchTaxDistName(LPCSTR pDist)    // Use strcmp()
{
   TAXAGENCY   *pRet=NULL;
   int         iTmp, beg, end, mid;

   beg = 0;
   end = iNumTaxDist-1;
   mid = (beg+end)/2;                       // Find Mid Location of Array

   while (beg <= end && (iTmp = strcmp(asTaxDist[mid].Agency, pDist)))      // Compare Item and Value of Mid
   {
      if (iTmp < 0)
         beg = mid+1;
      else
         end = mid-1;

      mid = (beg+end)/2;
   }

   if (!iTmp)
   {
      pRet = &asTaxDist[mid];
   }

   return pRet;
}

/******************************* findExactTaxDist *****************************
 *
 * Find exact agency sequentially through unsorted list.
 *
 ******************************************************************************/

TAXAGENCY *findExactTaxDist(LPCSTR pDist, int iStart)
{
   TAXAGENCY  *pXrefTbl, *pRet=NULL;
   
   if (iStart < iNumTaxDist)
   {
      pXrefTbl = &asTaxDist[iStart];   
      for (; pXrefTbl->iNameLen > 0; pXrefTbl++)
      {
         if (!_stricmp(pDist, pXrefTbl->Agency))
         {
            pRet = pXrefTbl;
            break;
         }
      }
   }

   return pRet;
}

/******************************** findTaxAgency *******************************
 *
 * Find Tax code through sorted list using binary search.
 *
 ******************************************************************************/

TAXAGENCY *findTaxAgency(LPCSTR pCode, int iStart)
{
   TAXAGENCY   *pRet=NULL;
   int         iTmp, beg, end, mid;

   if (iStart >= iNumTaxDist)
      return pRet;

   beg = iStart;
   end = iNumTaxDist-1;
   mid = (beg+end)/2;                       // Find Mid Location of Array

   while (beg <= end && (iTmp = _memicmp(asTaxDist[mid].Code, pCode, asTaxDist[mid].iCodeLen)))      // Compare Item and Value of Mid
   {
      if (iTmp < 0)
         beg = mid+1;
      else
         end = mid-1;

      mid = (beg+end)/2;
   }

   if (!iTmp)
   {
      pRet = &asTaxDist[mid];
   }

   return pRet;
}

/********************************* InstDueDate ********************************
 *
 * Find installment due date for specific installment.
 *
 ******************************************************************************/

int InstDueDate(char *pResult, int iInstNum, int iRollYear)
{
   int   iDate=10, iDay, iMonth, iYear;

   if (iInstNum == 1)
   {
      iMonth = 12;
      iYear = iRollYear;
   } else if (iInstNum == 2)
   {
      iMonth = 4;
      iYear = iRollYear+1;
   } else
      return 0;

   if (m_bExtDueDate)
   {
      iDay = DayOfWeek(iYear, iMonth, iDate);
      if (iDay == 7)
         iDate += 2;
      else if (iDay == 1)
         iDate += 1;
   }
   sprintf(pResult, "%d%.2d%d", iYear, iMonth, iDate);

   iDate = atol(pResult);
   return iDate;
}

/********************************* ChkDueDate *********************************
 *
 * Check last update vs installment due date
 * Return 1 if past due date, 0 otherwise.
 *
 ******************************************************************************/

int ChkDueDate(int iInstNum, int iRollYear)
{
   int   iDate=10, iDay, iMonth, iYear, iRet;
   long  lDueDate;
   char  sDueDate[16];

   if (iInstNum == 1)
   {
      iMonth = 12;
      iYear = iRollYear;
   } else if (iInstNum == 2)
   {
      iMonth = 4;
      iYear = iRollYear+1;
   } else
      return 0;

   if (m_bExtDueDate)
   {
      iDay = DayOfWeek(iYear, iMonth, iDate);
      if (iDay == 7)
         iDate += 2;
      else if (iDay == 1)
         iDate += 1;
   }
   sprintf(sDueDate, "%d%.2d%d", iYear, iMonth, iDate);

   lDueDate = atol(sDueDate);
   if (lLastTaxFileDate > lDueDate)
      iRet = 1;
   else 
      iRet = 0;

   return iRet;
}

// pDueDate = yyyymmdd
int ChkDueDate(int iInstNum, char *pDueDate)
{
   int   iRet;
   long  lDueDate;

   lDueDate = atoin(pDueDate, 8);
   if (lLastTaxFileDate > lDueDate)
      iRet = 1;
   else 
      iRet = 0;

   return iRet;
}

/**************************** isNewTaxFile ***********************************
 *
 * Check file modification date against last tax file date in county table
 * Return >0 if input file is newer, 0 if not, <0 if error.
 *
 *****************************************************************************/

int isNewTaxFile(char *pInFile, char *pCnty, int iChkDate)
{
   char  acTmp[256], acServer[256];
   int   iRet, iFileDate;

   GetIniString("Database", "Provider", "", acServer, 128, acIniFile);
   if (acServer[0] == '\0')
      return -1;

   // Ignore date checking when debug
   GetIniString("Debug", "ChkFileDate", "Y", acTmp, 128, acIniFile);
   if (acTmp[0] == 'N')
      return 1;

   if (iChkDate > 0)
      iFileDate = iChkDate;
   else
      iFileDate = lLastTaxFileDate;

   if (!iFileDate && pInFile)
   {
      if (_access(pInFile, 0))
      {
         LogMsg("*** Missing input file %s.  Please verify and rerun.", pInFile);
         return -2;
      }
      iFileDate = getFileDate(pInFile);
   }

   try
   {
      // open the database connection
      if (!m_bConnected)
         m_bConnected = hl.Connect(acServer);

      sprintf(acTmp, "SELECT * FROM County WHERE CountyCode='%s'", pCnty);
      m_AdoRs.Open(hl, acTmp);

      if (m_AdoRs.next())
      {
         CString	sTmp = m_AdoRs.GetItem("LastTaxFileDate");
         m_AdoRs.Close();

         if (sTmp > "20000000")
         {
            iRet = atoi(sTmp);
            if (iRet >= iFileDate)
               iRet = 0;
         } else
            iRet = 1;
      }
   } catch(_com_error &e)
   {
      LogMsg("***** Error exec cmd: %s", acServer);
      LogMsgD("%s\n", ComError(e));
      iRet = -1;
   }

   return iRet;
}

/*********************************** OpenR01 **********************************
 *
 * Open R01 file.  If R01 file nor available, try S01
 * Input: R01 data.
 *
 ******************************************************************************/

FILE *OpenR01(char *pCnty)
{
   FILE  *fd;
   char  acR01File[_MAX_PATH], acTmp[_MAX_PATH];
   int   iRet;

   iRet = GetIniString("Data", "RawFile", "", acTmp, _MAX_PATH, acIniFile);
   sprintf(acR01File, acTmp, pCnty, pCnty, "R01");
   if (_access(acR01File, 0))
      sprintf(acR01File, acTmp, pCnty, pCnty, "S01");
   try 
   {
      LogMsg("Open R01 file: %s", acR01File);
      fd = fopen(acR01File, "rb");
   } catch (...)
   {
      LogMsg("***** Error opening R01 file: %s", acR01File);
      fd = NULL;
   }

   return fd;
}

/********************************* UpdateTRA **********************************
 *
 * Input: R01 data.
 *
 ******************************************************************************/

int UpdateTRA(char *pOutbuf, FILE *fd, bool bResetBuffer)
{
   static   char  acRec[2048], *pRec=NULL;
   int      iRet, iLoop;
   TAXBASE  *pTaxBase = (TAXBASE *)pOutbuf;

   if (bResetBuffer)
   {
      pRec = NULL;
      acRec[0] = 0;
      return 0;
   }

   // Get first Char rec for first call
   if (!pRec)
   {
      // Drop header
      iRet = fread(acRec, iRecLen, 1, fd);
      iRet = fread(acRec, iRecLen, 1, fd);
      pRec = acRec;
   }

   do
   {
      // Compare Apn
      iLoop = memcmp(pTaxBase->Apn, pRec, strlen(pTaxBase->Apn));
      if (iLoop > 0)
      {
         iRet = fread(pRec, 1, iRecLen, fd);
         if (iRet != iRecLen)
         {
            fclose(fd);
            fd = NULL;
            break;
         }
      }
   } while (iLoop > 0);

   // If not match, return
   if (iLoop)
      return 1;

#ifdef _DEBUG
//      if (!memcmp(pOutbuf, "10145008", 8))
//         iRet = 0;
#endif

   memcpy(pTaxBase->TRA, pRec+OFF_TRA, 6);
   iApnMatch++;

   // Get next roll record
   //iRet = fread(acRec, iRecLen, 1, fd);

   return 0;
}

/******************************************************************************
 *
 * Merge updated tax into Tax_Base & Tax_Delq table
 *
 ******************************************************************************/

//int mergeTaxBase(char *pCnty, char *pServerName)
//{
//   char *pSvrName, acTmp[128];
//   int  iRet;
//
//   pSvrName = pServerName;
//   sprintf(acTmp, "EXEC [dbo].[spMergeTaxBase] '%s'", pCnty);
//   if (bExpSql)
//   {
//      fputs("PRINT 'MergeTaxBase: ' + CONVERT(varchar,GETDATE(),21);\n", fdExpSql);
//      strcat(acTmp, ";\n");
//      fputs(acTmp, fdExpSql);
//      iRet = 0;
//   } else
//      iRet = updTaxDB(acTmp, pSvrName);
//
//   return iRet;
//}

//int mergeTaxDelq(char *pCnty, char *pServerName)
//{
//   char *pSvrName, acTmp[128];
//   int  iRet;
//
//   pSvrName = pServerName;
//   sprintf(acTmp, "EXEC [dbo].[spMergeTaxDelq] '%s'", pCnty);
//   if (bExpSql)
//   {
//      fputs("PRINT 'MergeTaxDelq: ' + CONVERT(varchar,GETDATE(),21);\n", fdExpSql);
//      strcat(acTmp, ";\n");
//      fputs(acTmp, fdExpSql);
//      iRet = 0;
//   } else
//      iRet = updTaxDB(acTmp, pSvrName);
//
//   return iRet;
//}

//int mergeTaxItems(char *pCnty, char *pServerName)
//{
//   char *pSvrName, acTmp[128];
//   int  iRet;
//
//   pSvrName = pServerName;
//   sprintf(acTmp, "EXEC [dbo].[spMergeTaxItems] '%s'", pCnty);
//   if (bExpSql)
//   {
//      fputs("PRINT 'MergeTaxItems: ' + CONVERT(varchar,GETDATE(),21);\n", fdExpSql);
//      strcat(acTmp, ";\n");
//      fputs(acTmp, fdExpSql);
//      iRet = 0;
//   } else
//      iRet = updTaxDB(acTmp, pSvrName);
//
//   return iRet;
//}

int mergeTaxTbl(char *pCnty, char *pServerName, char *pTblType)
{
   char *pSvrName, acTmp[128];
   int  iRet;

   pSvrName = pServerName;
   sprintf(acTmp, "EXEC [dbo].[spMergeTax%s] '%s'", pTblType, pCnty);
   if (bExpSql)
   {
      fputs("PRINT 'MergeTaxTbl: ' + CONVERT(varchar,GETDATE(),21);\n", fdExpSql);
      strcat(acTmp, ";\n");
      fputs(acTmp, fdExpSql);
      iRet = 0;
   } else
      iRet = updTaxDB(acTmp, pSvrName);

   return iRet;
}

int doTaxMerge(char *pCnty, bool bMergeItems, bool bMergeDelq, bool bMergeSupp)
{
   int   iRet;
   char  acServerName[64];

   iRet = GetIniString("Database", "TaxSvr1", "", acServerName, _MAX_PATH, acIniFile);
   if (iRet > 1)
   {
      LogMsg0("Merge tax on %s", acServerName);
      if (bMergeSupp)
      {
         LogMsg("Merge %s.%s_Tax_Supp", acServerName, pCnty);
         iRet = mergeTaxTbl(pCnty, acServerName, "Supp");
      } else
      {
         LogMsg("Merge %s.%s_Tax_Base", acServerName, pCnty);
         iRet = mergeTaxTbl(pCnty, acServerName, "Base");
         if (!iRet && bMergeDelq)
         {
            LogMsg("Merge %s.%s_Tax_Delq", acServerName, pCnty);
            iRet = mergeTaxTbl(pCnty, acServerName, "Delq");
         }
         if (!iRet && bMergeItems)
         {
            LogMsg("Merge %s.%s_Tax_Items", acServerName, pCnty);
            iRet = mergeTaxTbl(pCnty, acServerName, "Items");
         }
      }
   }

   if (bExpSql)
      return iRet;

   if (!iRet)
   {
      iRet = GetIniString("Database", "TaxSvr2", "", acServerName, _MAX_PATH, acIniFile);
      if (iRet > 1)
      {
         LogMsg0("Merge tax on %s", acServerName);
         if (bMergeSupp)
         {
            LogMsg("Merge %s.%s_Tax_Supp", acServerName, pCnty);
            iRet = mergeTaxTbl(pCnty, acServerName, "Supp");
         } else
         {
            LogMsg("Merge %s.%s_Tax_Base", acServerName, pCnty);
            iRet = mergeTaxTbl(pCnty, acServerName, "Base");
            if (!iRet && bMergeDelq)
            {
               LogMsg("Merge %s.%s_Tax_Delq", acServerName, pCnty);
               iRet = mergeTaxTbl(pCnty, acServerName, "Delq");
            }
            if (!iRet && bMergeItems)
            {
               LogMsg("Merge %s.%s_Tax_Items", acServerName, pCnty);
               iRet = mergeTaxTbl(pCnty, acServerName, "Items");
            }
         }
      }
   }

   if (!iRet)
   {
      iRet = GetIniString("Database", "TaxSvr3", "", acServerName, _MAX_PATH, acIniFile);
      if (iRet > 1)
      {
         LogMsg0("Merge tax on %s", acServerName);
         if (bMergeSupp)
         {
            LogMsg("Merge %s.%s_Tax_Supp", acServerName, pCnty);
            iRet = mergeTaxTbl(pCnty, acServerName, "Supp");
         } else
         {
            LogMsg("Merge %s.%s_Tax_Base", acServerName, pCnty);
            iRet = mergeTaxTbl(pCnty, acServerName, "Base");
            if (!iRet && bMergeDelq)
            {
               LogMsg("Merge %s.%s_Tax_Delq", acServerName, pCnty);
               iRet = mergeTaxTbl(pCnty, acServerName, "Delq");
            }
            if (!iRet && bMergeItems)
            {
               LogMsg("Merge %s.%s_Tax_Items", acServerName, pCnty);
               iRet = mergeTaxTbl(pCnty, acServerName, "Items");
            }
         }
      }
   }

   return iRet;
}

/**************************** TC_CreateTaxRecord() ****************************
 *
 * Data is already parsed in apTokens[].  Output buffer can be either Base or Delq record.
 *
 * DUE_PAID_DATE1 & DUE_PAID_DATE2 may have different format.  Watch out
 *
 * Return < 0 if record is not good or no tax
 *
 ******************************************************************************/

int TC_CreateTaxRecord(char *pOutbuf)
{
   char     acTmp[256], bBillType;
   int      iRet=0, iTmp, iFmt;
   double	dTmp, dTax1, dTax2, dPen1, dPen2, dTaxTotal;
   TAXBASE  *pOutBase = (TAXBASE *)pOutbuf;
   TAXDELQ  *pOutDelq = (TAXDELQ *)pOutbuf;

   // Clear output buffer
   memset(pOutBase, 0, sizeof(TAXBASE));
#ifdef _DEBUG
   //if (!memcmp(apTokens[TC_APN_S], "002017032000", 12))
   //   iTmp = 0;
#endif

   // Default BillType SECURED ANNUAL
   bBillType = BILLTYPE_SECURED;
   pOutBase->isSecd[0] = '1';
   pOutBase->isSupp[0] = 0;
   if (!memcmp(apTokens[TC_ROLL_CAT], "SECURED CORRECTED", 10))
   {
      bBillType = BILLTYPE_ROLL_CORRECTION;
   } else if (!memcmp(apTokens[TC_ROLL_CAT], "SUP", 3))
   {
      pOutBase->isSecd[0] = 0;
      pOutBase->isSupp[0] = '1';
      bBillType = BILLTYPE_SECURED_SUPPL;
   } else if (!memcmp(apTokens[TC_ROLL_CAT], "UNSEC", 5))
   {
      pOutBase->isSecd[0] = 0;
      pOutBase->isSupp[0] = 0;
      bBillType = BILLTYPE_UNSECURED;
   } 
   pOutBase->BillType[0] = bBillType;

   // Verify date format
   if (strchr(apTokens[TC_DUE_PAID_DT2], '/'))
      iFmt = MM_DD_YYYY_1;
   else
      iFmt = iDateFmt;

   if (!memcmp(apTokens[TC_PAID_STAT1], "DEL", 3))
   {
      int iDelqYear;

      // Delq Year
      iDelqYear = atol(apTokens[TC_TAX_YR]) + 1;
      if (iDelqYear < 1900)
      {
         LogMsg("*** WARNING: No tax data APN=%s", apTokens[TC_APN_S]);
         return -2;
      }

      // Tax year delq
      sprintf(pOutDelq->TaxYear, "%.4d", iDelqYear-1);

      if (iTokens > TC_DEF_DATE && *apTokens[TC_DEF_DATE] > ' ')
      {
         if (strchr(apTokens[TC_DEF_DATE], '/'))
            iTmp = MM_DD_YYYY_1;
         else
            iTmp = iFmt;

         if (dateConversion(apTokens[TC_DEF_DATE], acTmp, iTmp))
         {
            strcpy(pOutDelq->Def_Date, acTmp);
            iTmp = atoin(acTmp, 4);
            sprintf(pOutDelq->TaxYear, "%d", iTmp-1);
         } else
            strcpy(pOutDelq->Def_Date, apTokens[TC_DEF_DATE]);
      } else
         sprintf(pOutDelq->Def_Date, "%d", iDelqYear);

      // APN
      strcpy(pOutDelq->Apn, apTokens[TC_APN_S]);

      // BillNum
      if (*apTokens[TC_BILL_NUM] >= '0')
         strcpy(pOutDelq->Default_No, apTokens[TC_BILL_NUM]);
      else if (m_bUseFeePrcl)
         strcpy(pOutDelq->BillNum, apTokens[TC_FEE_PCL]);

      // Default amt
      dTaxTotal = atof(apTokens[TC_TOTAL_DUE]);
      if (dTaxTotal > 0.0)
      {
         sprintf(pOutDelq->Def_Amt, "%.2f", dTaxTotal);
         pOutDelq->isDelq[0] = '1';

         // Paid amt
         dTmp = atof(apTokens[TC_TOTAL_PAID]);
         if (dTmp > 0.0)
         {
            iTmp = sprintf(pOutDelq->Red_Amt, "%.2f", dTmp);
            if (dTaxTotal == dTmp)
            {
               pOutDelq->DelqStatus[0] = TAX_STAT_REDEEMED;       // Redeemed
               pOutDelq->isDelq[0] = '0';
               if (iTokens >= TC_DEF_DATE)
               {
                  if (strchr(apTokens[TC_RED_DATE], '/'))
                     iFmt = MM_DD_YYYY_1;
                  else
                     iTmp = iFmt;

                  if (*apTokens[TC_RED_DATE] >= '0' && dateConversion(apTokens[TC_RED_DATE], acTmp, iTmp))
                     strcpy(pOutDelq->Red_Date, acTmp);
               }
            } else if (dTaxTotal > dTmp)
               pOutDelq->DelqStatus[0] = TAX_STAT_ONPAYMENT;      // On payment plan
         } else
         {
            pOutDelq->DelqStatus[0] = TAX_STAT_UNPAID;            // Unpaid
         }
      }

      iRet = 2;
   } else
   {
      // Tax Year - ignore if it's not current year
      iTaxYear = atol(apTokens[TC_TAX_YR]);
      if (lTaxYear > 0 && iTaxYear != lTaxYear)
         return 0;

      sprintf(pOutBase->TaxYear, "%d", iTaxYear);

      // APN
      strcpy(pOutBase->Apn, apTokens[TC_APN_S]);

      // BillNum
      if (*apTokens[TC_BILL_NUM] >= '0')
         strcpy(pOutBase->BillNum, apTokens[TC_BILL_NUM]);
      else if (m_bUseFeePrcl)
      {
         if (!pOutBase->isSecd[0] || pOutBase->isSecd[0] == '1')  
            sprintf(pOutBase->BillNum, "%s-01", apTokens[TC_APN_S]);    // Secured or Unsecured
         else
            sprintf(pOutBase->BillNum, "%s-02", apTokens[TC_APN_S]);    // Supplemental
      }

      // TRA
      iTmp = atol(apTokens[TC_TRA]);
      if (iTmp > 0)
         sprintf(pOutBase->TRA, "%0.6d", iTmp);

      // Check for Tax amount
      dTax1 = dTax2 = 0.0;
      if (*apTokens[TC_TAX_DUE1] > '0')
         dTax1 = atof(apTokens[TC_TAX_DUE1]);

      if (*apTokens[TC_TAX_DUE2] > '0')
         dTax2 = atof(apTokens[TC_TAX_DUE2]);

      if (dTax1 > 0.0)
         sprintf(pOutBase->TaxAmt1, "%.2f", dTax1);
      if (dTax2 > 0.0)
         sprintf(pOutBase->TaxAmt2, "%.2f", dTax2);

      if (*apTokens[TC_TOTAL_TAX] > '0')
         dTaxTotal = atof(apTokens[TC_TOTAL_TAX]);
      else
         dTaxTotal = dTax1+dTax2;

      if (dTaxTotal > 0.0)
         sprintf(pOutBase->TotalTaxAmt, "%.2f", dTaxTotal);

      // Penalty
      dPen1 = atof(apTokens[TC_PEN_DUE1]);
      if (dPen1 > 0.0)
         sprintf(pOutBase->PenAmt1, "%.2f", dPen1);
      dPen2 = atof(apTokens[TC_PEN_DUE2]);
      if (dPen2 > 0.0)
         sprintf(pOutBase->PenAmt2, "%.2f", dPen2);

      // Paid/Due - SAC: PAID/UNPAID/PENDING/DUE/CANCELED/OUTSTANDING
      if (!memcmp(apTokens[TC_PAID_STAT2], "PA", 2))
      {
         InstDueDate(pOutBase->DueDate2, 2, iTaxYear);
         strcpy(pOutBase->PaidAmt2, pOutBase->TaxAmt2);
         if (*apTokens[TC_DUE_PAID_DT2] >= '0' && dateConversion(apTokens[TC_DUE_PAID_DT2], acTmp, iFmt))
            strcpy(pOutBase->PaidDate2, acTmp);
         else
            LogMsg("*** Bad PaidDate 2: %s [%s]", apTokens[TC_DUE_PAID_DT2], apTokens[TC_APN_S]);

         pOutBase->Inst2Status[0] = TAX_BSTAT_PAID;
         pOutBase->Inst1Status[0] = TAX_BSTAT_PAID;
      } else if (!memcmp(apTokens[TC_PAID_STAT2], "CA", 2))    // Cancel
         pOutBase->Inst2Status[0] = TAX_BSTAT_CANCEL;
      else if (!dTax2)                                         // No tax
         pOutBase->Inst2Status[0] = TAX_BSTAT_NOTAX;
      else
      {
         if (*apTokens[TC_DUE_PAID_DT2] >= '0' && dateConversion(apTokens[TC_DUE_PAID_DT2], acTmp, iFmt))
            strcpy(pOutBase->DueDate2, acTmp);
         //else if (pOutBase->isSupp[0] == '1')
         //   strcpy(pOutBase->DueDate2, "20170630");
         else
            InstDueDate(pOutBase->DueDate2, 2, iTaxYear);

         if (!memcmp(apTokens[TC_PAID_STAT2], "LA", 2) || ChkDueDate(2, pOutBase->DueDate2))
            pOutBase->Inst2Status[0] = TAX_BSTAT_PASTDUE;
         else
            pOutBase->Inst2Status[0] = TAX_BSTAT_UNPAID;
      } 

      // Verify date format
      if (*(apTokens[TC_DUE_PAID_DT1]+2) == '/' && strlen(apTokens[TC_DUE_PAID_DT1]) == 10)
         iFmt = MM_DD_YYYY_1;
      else
         iFmt = iDateFmt;

      // Paid Inst1
      if (!memcmp(apTokens[TC_PAID_STAT1], "PA", 2))
      {  
         InstDueDate(pOutBase->DueDate1, 1, iTaxYear);
         strcpy(pOutBase->PaidAmt1, pOutBase->TaxAmt1);
         if (*apTokens[TC_DUE_PAID_DT1] >= '0' && dateConversion(apTokens[TC_DUE_PAID_DT1], acTmp, iFmt))
            strcpy(pOutBase->PaidDate1, acTmp);
         else if (pOutBase->PaidDate2[0] >= '0')
            // Sometimes when both installments are paid at the same time, people only keys in date2
            strcpy(pOutBase->PaidDate1, pOutBase->PaidDate2);
         else 
            LogMsg("*** Bad PaidDate 1: %s [%s]", apTokens[TC_DUE_PAID_DT1], apTokens[TC_APN_S]);

         pOutBase->Inst1Status[0] = TAX_BSTAT_PAID;
      } else if (!memcmp(apTokens[TC_PAID_STAT1], "CA", 2))    // Cancel
         pOutBase->Inst1Status[0] = TAX_BSTAT_CANCEL;
      else if (!dTax1)                                         // No tax
         pOutBase->Inst1Status[0] = TAX_BSTAT_NOTAX;
      else 
      {
         if (*apTokens[TC_DUE_PAID_DT1] >= '0' && dateConversion(apTokens[TC_DUE_PAID_DT1], acTmp, iFmt))
            strcpy(pOutBase->DueDate1, acTmp);
         //else if (pOutBase->isSupp[0] == '1')
         //   strcpy(pOutBase->DueDate1, "20170228");
         else
            InstDueDate(pOutBase->DueDate1, 1, iTaxYear);

         // Late or past due
         if (!memcmp(apTokens[TC_PAID_STAT1], "LA", 2) || ChkDueDate(1, pOutBase->DueDate1))
            pOutBase->Inst1Status[0] = TAX_BSTAT_PASTDUE;
         else
            pOutBase->Inst1Status[0] = TAX_BSTAT_UNPAID;
      } 

      // Fee Paid
      dTmp = atof(apTokens[TC_OTHER_FEES]);
      if (dTmp > 0.0)
         iTmp = sprintf(pOutBase->TotalFees, "%.2f", dTmp);

      // Calculate unpaid amt
      dTmp = atof(apTokens[TC_TOTAL_DUE]);
      if (dTmp > 0.0)
         iTmp = sprintf(pOutBase->TotalDue, "%.2f", dTmp);

      iRet = 1;
   }

   // Update date
   if (iTokens > TC_UPD_DATE && *apTokens[TC_UPD_DATE] > ' ' && dateConversion(apTokens[TC_UPD_DATE], acTmp, MM_DD_YYYY_1))
   {
      if (iRet == 1)
         strcpy(pOutBase->Upd_Date, acTmp);
      else
         strcpy(pOutDelq->Upd_Date, acTmp);
   } else
   {
      if (iRet == 1)
         sprintf(pOutBase->Upd_Date, "%d", lLastTaxFileDate);
      else
         sprintf(pOutDelq->Upd_Date, "%d", lLastTaxFileDate);
   }

   return iRet;
}

/******************************** addTaxAgency *********************************
 *
 * Add new agency to asTaxDist list on the fly
 *
 ******************************************************************************/

void addTaxAgency(char *pTaxCode, char *pTaxAgency, char *pTaxRate, char *pPhone)
{
   memset(&asTaxDist[iNumTaxDist], 0, sizeof(TAXAGENCY));
   strcpy(asTaxDist[iNumTaxDist].Code, pTaxCode);
   asTaxDist[iNumTaxDist].iCodeLen = strlen(pTaxCode);

   strcpy(asTaxDist[iNumTaxDist].Agency, pTaxAgency);
   asTaxDist[iNumTaxDist].iNameLen = strlen(pTaxAgency);

   if (pTaxRate && *pTaxRate > ' ')
      strcpy(asTaxDist[iNumTaxDist].TaxRate, pTaxRate);
   if (pPhone && *pPhone > ' ')
      strcpy(asTaxDist[iNumTaxDist].Phone, pPhone);

   iNumTaxDist++;
}

/******************************* updateTaxBase ********************************
 *
 * Update TaxBase record
 *
 ******************************************************************************/

int updateTaxBase(char *pCnty, char *pBaseRec, char *pOutbuf)
{
   //static bool  bConnected=false;
   //static hlAdo hDB;

   char     acTmp[4096], acTmp1[1024], acTmp2[1024], acTmp3[1024];
   int      iRet=0;
   double   dTmp;
   TAXBASE  *pTaxBase = (TAXBASE *)pBaseRec;

   //CString  sTmp;
   //hlAdoRs  myRs;

   //if (!bConnected)
   //{
   //   iRet = GetIniString("Database", "TaxProvider", "", acTmp, 128, acIniFile);
   //   if (!iRet)
   //   {
   //      LogMsg("***** Missing TaxProvider in INI file.  Please review %s", acIniFile);
   //      return -1;
   //   }

   //   LogMsg("Execute command on: %s", acTmp);
   //   LogMsg(strCmd);
   //   try
   //   {
   //      // open the database connection
   //      bConnected = hDB.Connect(acTmp);
   //   } catch(_com_error &e)
   //   {
   //      LogMsg("%s", ComError(e));
   //      return -2;
   //   }
   //}

   //// Retrieve 
   //sprintf(strCmd, "SELECT * FROM %s_Tax_Base WHERE tb_apn=%s", pCnty, pTaxBase->Apn);
   //try
   //{
   //   myRs.Open(hDB, strCmd);
   //   if (myRs.next())
   //   {
   //      // Update PaidAmt, PaidDate
   //      
   //   }
   //} catch(_com_error &e)
   //{
   //   LogMsg("***** Error accessing sql [%s] : %s", strCmd, ComError(e));
   //   iRet = -3;
   //}

   acTmp1[0] = 0;
   acTmp2[0] = 0;
   acTmp3[0] = 0;
   dTmp = 0;
   if (pTaxBase->PaidAmt1[0] > '0')
   {
      sprintf(acTmp1, "tb_amtPaid1=%s, tb_inst1Status='P', ", pTaxBase->PaidAmt1);
      if (pTaxBase->PenAmt1[0] > '0')
      {
         sprintf(acTmp, "tb_amtPen1=%s, ", pTaxBase->PenAmt1);
         strcat(acTmp1, acTmp);
      }
      if (pTaxBase->PaidDate1[0] > '0')
      {
         sprintf(acTmp, "tb_paidDate1=%s, ", pTaxBase->PaidDate1);
         strcat(acTmp1, acTmp);
      }
   }
   if (pTaxBase->PaidAmt2[0] > '0')
   {
      dTmp = atof(pTaxBase->PaidAmt2);
      sprintf(acTmp2, "tb_amtPaid2=%s, tb_inst2Status='P', ", pTaxBase->PaidAmt2);
      if (pTaxBase->PenAmt2[0] > '0')
      {
         sprintf(acTmp, "tb_amtPen2=%s, ", pTaxBase->PenAmt2);
         strcat(acTmp2, acTmp);
      }
      if (pTaxBase->PaidDate2[0] > '0')
      {
         sprintf(acTmp, "tb_paidDate2=%s, ", pTaxBase->PaidDate2);
         strcat(acTmp2, acTmp);
      }
   }

   sprintf(acTmp3, "tb_updatedDate=%d WHERE tb_apn='%s' AND tb_taxYear=%s",
      lLastTaxFileDate,pTaxBase->Apn, pTaxBase->TaxYear);

   sprintf(acTmp, "UPDATE %s_Tax_Base SET ", pCnty);
   if (acTmp1[0] > ' ')
      strcat(acTmp, acTmp1);
   if (acTmp2[0] > ' ')
      strcat(acTmp, acTmp2);

   if (acTmp1[0] > ' ' || acTmp2[0] > ' ')
   {
      strcat(acTmp, acTmp3);

      if (pOutbuf)
         strcpy(pOutbuf, acTmp);

      //iRet = execSqlCmd(strCmd, &hDB);
      iRet = 0;
   } else
      iRet = 1;

   return iRet;
}

/****************************** SetTaxRateIncl *******************************
 *
 *****************************************************************************/

void SetTaxRateIncl(char *pCnty)
{
   char  sTmp[_MAX_PATH];

   // Set flag to include tax rate in Agency table
   GetIniString(pCnty, "TaxRate", "Y", sTmp, _MAX_PATH, acIniFile);
   if (sTmp[0] == 'Y')
      bInclTaxRate = true;
   else
      bInclTaxRate = false;
}

/******************************** findTRADist *********************************
 *
 * Find TRA through sorted list using binary search.
 *
 ******************************************************************************/

TRADIST *findTRADist(LPCSTR pCode, int iStart)
{
   TRADIST  *pRet=NULL;
   int      iTmp, beg, end, mid;

   if (iStart >= iNumTRADist)
      return pRet;

   if (!memcmp(asTRADist[0].TRA, pCode, 6))
   {
      pRet = &asTRADist[0];
      return pRet;
   }

   beg = iStart;
   end = iNumTRADist-1;
   mid = (beg+end)/2;                       // Find Mid Location of Array

   while (beg <= end && (iTmp = memcmp(asTRADist[mid].TRA, pCode, 6)))      // Compare Item and Value of Mid
   {
      if (iTmp < 0)
         beg = mid+1;
      else
         end = mid-1;

      mid = (beg+end)/2;
   }

   if (!iTmp)
   {
      pRet = &asTRADist[mid];
   }

   return pRet;
}

/******************************** TC_CreateR01 *******************************
 *
 * Extract special book from TC file and create R01 record
 *
 *****************************************************************************/

int TC_CreateR01(char *pCnty, char *pBook)
{
   char  sTaxFile[_MAX_PATH], sOutFile[_MAX_PATH], sInbuf[2048], sOutbuf[2048], sTmp[512], *pTmp, *pOutbuf;
   int   iCmpLen, lCnt=0;
   FILE  *fdTax, *fdR01;

   LogMsg0("Creating R01 parcels from TC files for %s", pCnty);

   // Get input file names
   GetIniString("Data", "Main_TC", "", sTmp, _MAX_PATH, acIniFile);
   sprintf(sTaxFile, sTmp, pCnty, pCnty, 'F');
   if (_access(sTaxFile, 0))
   {
      LogMsg("***** File not found: %s", sTaxFile);
      return -1;
   }

   // Open input file
   LogMsg("Open Tax file %s", sTaxFile);
   fdTax = fopen(sTaxFile, "r");
   if (fdTax == NULL)
   {
      LogMsg("***** Error opening Tax file: %s\n", sTaxFile);
      return -2;
   }  

   // Create output file names
   sprintf(sOutFile, acRawTmpl, pCnty, pCnty, "XT1");

   // Open Base Output file
   LogMsg("Open Base output file %s", sOutFile);
   fdR01 = fopen(sOutFile, "w");
   if (fdR01 == NULL)
   {
      LogMsg("***** Error creating output file: %s\n", sOutFile);
      return -3;
   }

   // Skip header record - file is sorted, no header to skip
   pTmp = fgets((char *)&sInbuf[0], 2048, fdTax);

   // Initialize
   pOutbuf = &sOutbuf[0];
   iCmpLen = strlen(pBook);

   // Loop through whole file
   while (!feof(fdTax))
   {
      pTmp = fgets((char *)&sInbuf[0], 2048, fdTax);
      if (!pTmp)
         break;
      
      if (memcmp(sInbuf, pBook, iCmpLen))
         continue;

#ifdef _DEBUG
      //if (!memcmp(acRec, "00100200100000", 12))
      //   iRet = 0;
#endif

      // Parse input tax data
      iTokens = ParseStringIQ(sInbuf, '|', 128, apTokens);
      if (iTokens < TC_NET_VAL)
      {
         LogMsg("***** Error: bad Tax record for APN=%.*s (#tokens=%d)", iApnLen, sInbuf, iTokens);
         continue;
      }

      // Create new R01 record
      memset(pOutbuf, ' ', iRecLen);
      memcpy(pOutbuf+OFF_APN_S, apTokens[TC_APN_S], strlen(apTokens[TC_APN_S]));
      memcpy(pOutbuf+OFF_APN_D, apTokens[TC_APN_D], strlen(apTokens[TC_APN_D]));
      memcpy(pOutbuf+OFF_ALT_APN, apTokens[TC_FEE_PCL], strlen(apTokens[TC_FEE_PCL]));

      memcpy(pOutbuf+OFF_YR_ASSD, apTokens[TC_TAX_YR], strlen(apTokens[TC_TAX_YR]));
      memcpy(pOutbuf+OFF_TAX_AMT, apTokens[TC_TOTAL_TAX], strlen(apTokens[TC_TOTAL_TAX]));

      memcpy(pOutbuf+OFF_CO_NUM, apTokens[TC_CO_NUM], strlen(apTokens[TC_CO_NUM]));
      memcpy(pOutbuf+OFF_CO_ID, apTokens[TC_CO_ID], strlen(apTokens[TC_CO_ID]));

      if (*apTokens[TC_S_ADDR] > ' ')
         memcpy(pOutbuf+OFF_S_ADDR_D, apTokens[TC_S_ADDR], strlen(apTokens[TC_S_ADDR]));
      if (*apTokens[TC_M_ADDR] > ' ')
         memcpy(pOutbuf+OFF_M_ADDR_D, apTokens[TC_M_ADDR], strlen(apTokens[TC_M_ADDR]));
      if (*apTokens[TC_LEGAL] > ' ')
         vmemcpy(pOutbuf+OFF_NAME1, apTokens[TC_LEGAL], SIZ_NAME1+SIZ_NAME2);
      if (*apTokens[TC_TRA] > ' ')
         memcpy(pOutbuf+OFF_TRA, apTokens[TC_TRA], strlen(apTokens[TC_TRA]));

      fwrite(sOutbuf, 1, iRecLen, fdR01);
      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdTax)
      fclose(fdTax);
   if (fdR01)
      fclose(fdR01);

   LogMsg("Total records extracted: %d", lCnt);

   return lCnt;
}

/******************************** TC_LoadTax *********************************
 *
 * Load tax scrape file.  This function will load full or partial data
 * based on which one is newer.
 *
 * Return 0 if success
 *
 *****************************************************************************/

int TC_LoadTax(char *pCnty, bool bImport, bool bLoadDetail)
{
   char  sFullTaxFile[_MAX_PATH], sPartialTaxFile[_MAX_PATH], sTmp[_MAX_PATH];
   int   iRet=-1;
   long  lFullDate=0, lPartialDate=0;

   // Get input file names
   GetIniString("Data", "Main_TC", "", sTmp, _MAX_PATH, acIniFile);
   sprintf(sFullTaxFile, sTmp, pCnty, pCnty, 'F');
   if (!_access(sFullTaxFile, 0))
      lFullDate = getFileDate(sFullTaxFile);

   sprintf(sPartialTaxFile, sTmp, pCnty, pCnty, 'P');
   if (!_access(sPartialTaxFile, 0))
      lPartialDate = getFileDate(sPartialTaxFile);

   if (lFullDate > 0 && lFullDate >= lPartialDate)
   {
      iRet = Load_TC(pCnty, bImport);
   } else if (lPartialDate > 0)
   {
      GetIniString(pCnty, "UpdateDetail", "Y", sTmp, _MAX_PATH, acIniFile);
      if (sTmp[0] == 'N')
         iRet = Update_TC(pCnty, bImport, false);
      else
         iRet = Update_TC(pCnty, bImport, bLoadDetail);
   }

   return iRet;
}

/****************************** ConvertTaxSqlFile ****************************
 *
 *
 *****************************************************************************/

int ConvertTaxSqlFile(char *pFile1, char *pFile2, char *pRoot1, char *pRoot2, char *pSvrType)
{
   char     *pTmp, acInRec[256], acOutRec[256], acMachine[64];

   FILE     *fdIn, *fdOut;
   long     lCnt=0;

   // Setup file names
   LogMsg0("Convert tax update file");

   // Open Lien extract file
   LogMsg("Open file %s", pFile1);
   fdIn = fopen(pFile1, "r");
   if (fdIn == NULL)
   {
      LogMsg("***** Error opening file: %s\n", pFile1);
      return 2;
   }

   // Open Output file
   LogMsg("Open output file %s", pFile2);
   fdOut = fopen(pFile2, "w");
   if (fdOut == NULL)
   {
      LogMsg("***** Error creating output file: %s\n", pFile2);
      return 4;
   }

   strcpy(acMachine, getenv("COMPUTERNAME"));

   // Merge loop
   while (!feof(fdIn))
   {
      // Read next roll record
      pTmp = fgets(acInRec, 255, fdIn);
      if (pTmp)
      {
         if (strstr(acInRec, acMachine))
         {
            strcpy(acOutRec, acInRec);
            replStrAll(acOutRec, pRoot1, pRoot2);
            replStrAll(acOutRec, "\\", "/");
            if (*pSvrType == 'B')
               replStrAll(acOutRec, "[spBulkImport]", "[spBulkImportB]");

            fputs(acOutRec, fdOut);
         } else
            fputs(acInRec, fdOut);
         if (!(++lCnt % 1000))
            printf("\r%u", lCnt);
      }
   }

   // Close files
   if (fdIn)
      fclose(fdIn);
   if (fdOut)
      fclose(fdOut);

   LogMsg("Total output records:       %u", lCnt);

   return 0;
}

