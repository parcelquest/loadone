/*****************************************************************************
 *
 * Input files:
 *    - KIN_LIEN.BIN             (lien date roll, 2550-byte ebcdic)
 *    - FIL645E.BIN              (roll file, 2550-byte ebcdic) - may contain blank APN records
 *    - F02101K.BIN              (char file, 250-byte ebcdic)
 *    - F02151K.BIN              (sale file, 198-byte ebcdic)
 *    - Kin_sale_cum.srt         (cum sale,  180-byte ascii)
 *
 * Notes: - KIN sale file has group APN for every record. This info may not be used.
 *        - Legal also contains situs.  In case situs not avail, parse legal for situs
 *
 *    - Normal update: LoadOne -U -Xs[i] -Mp -Mr
 *    - Load Lien: LoadOne -L -Xl -Xs -Mp -Mr
 *
 * 1/11/2006  1.1.8.2  Start coding
 * 03/24/2008 1.5.7.2  Format IMAPLINK.
 * 07/23/2008 8.0.4    Rename CreateKinRoll() to Kin_Load_LDR() and MergeKinRoll()
 *                     to Kin_Load_Roll().  Modify Kin_Load_LDR() to accept only
 *                     records without TaxYear and BOC values.
 * 08/12/2008 8.1.2	  Fix bug in LoadKin() by setting iApnLen and reorganize code
 *                     to process all temp file in CO_OUT folder.
 * 03/16/2009 8.7.0    Format STORIES as 99.0.  Devive stories by 10.  Also fix mail
 *                     address bug that we put wrong fields to addr1 & 2 when all 
 *                     4 mail addr input fields are populated.
 * 07/21/2009 9.1.4    Ignore record with blank APN in Kin_Load_Roll() & Kin_Load_LDR().
 *                     Remove dup sales in Kin_UpdateSaleHist().
 * 02/16/2010 9.4.2    Merge long LEGAL into LEGAL1.
 * 02/23/2010 9.4.3    Fix CARE_OF length issue.
 * 08/01/2010 10.1.8   Use updateLegal() to handle legal update.
 * 08/02/2011 11.0.6   Add S_HSENO
 * 10/05/2011 11.4.11  Change cumsale file to standard format and use ApplyCumSale()
 *                     to apply sales to R01 file.
 * 01/29/2012 11.6.4   Fix M_ZIP bug in Kin_MergeMAdr().
 * 03/08/2012 11.6.8.0 Add Kin_MergePubl() to merge tax exempt parcels.
 *            11.6.8.1 Update Zoning & UseCode if current roll doesn't have.
 * 06/27/2012 12.0.1   Fix bug in Kin_FormatSCSale() that crashes when TaxAmt > 10000000
 * 08/31/2012 12.2.5   Add Kin_ExtractProp8()
 * 09/19/2012 12.2.8   Fix Kin_MergeOwner() to keep Owner1 the same as county provided.
 * 10/09/2012 12.2.10  Remove comma from owner1.
 * 10/25/2012 12.5.2   Modify Kin_ChkCond() to ignore book 800-899 for unsecured parcels.
 * 07/17/2013 13.0.4   Modify Kin_Load_LDR() to exclude APN 800-899. Fix bug in calculation
 *                     large value by using __int64 instead of long.
 * 08/21/2013 13.6.11  Add Heat/Cool, BsmtSqft, and MiscImpr.
 * 10/08/2013 13.10.4  Use updateVesting() to update Vesting and Etal flag.
 *                     Modify MergeChar() to prepare to add 1Q & 3Q bath.
 * 10/11/2013 13.11.0  Add all QBaths to R01
 * 10/22/2013 13.11.6  Drop supplemental APN from book 980-998
 * 03/08/2016 15.7.1   Fix bug in Kin_Load_Roll() not to use lRet as temp variable.
 * 03/26/2016 15.8.1   Move dispError() to Logs.cpp
 * 04/24/2016 15.9.2   Modify Kin_MergeMAdr() to capture DBA.  Add -T option to load tax data.
 * 07/20/2016 16.0.1   Fix bug in Kin_MergeMAdr()
 * 09/27/2016 16.2.8   Fix SalePrice and save DocTax in Kin_FormatSCSale().  Filter out
 *                     sales with DocType other than 'R'.
 * 09/29/2016 16.2.9   Bug fix Kin_ParseTaxDetail() to remove interest from Agency record since
 *                     one agency may have multiple TaxRate.  Load TaxCode table before processing.
 * 10/14/2016 16.3.4   Modify ParseTaxBase() and ParseTaxDetail() to use default TaxYear (set in INI)
 *                     If both TaxYear & Apportionment_Year are not present.
 * 10/26/2016 16.5.3   Modify Kin_ChkCond() to accept rec with status 'C', ' ', and 'A' only
 *                     Modify Kin_ParseTaxBase() to populate Tax_Base.DelqYear & Tax_Delq.TaxYear
 * 11/01/2016 16.5.4   Remove redeemed, penalty, and fee calculation from Kin_ParseTaxBase() until further notice.
 * 04/01/2017 16.13.4  Fix bug in Kin_ParseTaxDetail() and update status in Kin_ParseTaxBase()
 *                     Add Kin_UpdateDelq() to update Tax_Delq using redemption file.  Modify Kin_Load_TaxBase()
 *                     to add Redemption update.
 * 04/02/2017 16.13.5  Modify Kin_UpdateDelq() to populate default amt.
 * 11/21/2017 17.5.0   Call updateDelqFlg() to populate delqYear & defaultAmt in Tax_Base table.  Modify 
 *                     Kin_UpdateDelq() & Kin_ParseTaxBase() to set Delq.isDelq flag.
 * 01/18/2018 17.6.2   Modify Kin_FormatSCSale() to set Multi_Apn flag.
 * 04/05/2018 17.8.2   Modify Kin_MergeRoll() to seperate values into indidual field.
 *                     Modify Kin_CreatePublR01() to add Taxability code. KIN_PUB layout has been changed.
 * 04/11/2018 17.9.1   Modify Kin_ChkCond() to include book 906.
 * 05/02/2018 17.10.7  Modify Kin_Load_TaxBase() to putt due date on even if it's paid.
 * 11/13/2018 18.6.1   Fix Other Value in Kin_MergeRoll().
 * 08/28/2019 19.2.1   Modify Kin_Load_TaxBase() to skip import if number of records < 30000
 * 04/01/2020 19.8.6   Add special case to fix acreage for 050010001 in Kin_MergeRoll() & Kin_CreatePublR01()
 * 06/19/2020 20.0.0   Modify Kin_MergeRoll() & Kin_CreateLienRec() to add Exemption codes
 * 10/28/2020 20.3.6   Modify Kin_CreatePublR01() & Kin_MergePubl() to populate PQZoning.
 * 08/18/2021 21.1.5   Add Kin_ConvStdChar()
 *
 *****************************************************************************/

#include "stdafx.h"
#include "CountyInfo.h"
#include "Prodlib.h"
#include "R01.h"
#include "RecDef.h"
#include "Tables.h"
#include "Logs.h"
#include "LoadDef.h"
#include "Utils.h"
#include "XlatTbls.h"
#include "doSort.h"
#include "doOwner.h"
#include "formatApn.h"
#include "CharRec.h"
#include "SaleRec.h"
#include "LoadOne.h"
#include "MergeKin.h"
#include "UseCode.h"
#include "LOExtrn.h"
#include "Tax.h"

static   LU_ENTRY tblAttr[MAX_ATTR_ENTRIES];
static   FILE     *fdChar, *fdCChr;
static   long     lCharSkip, lSaleSkip, lCChrSkip;
static   long     lCharMatch, lSaleMatch, lCChrMatch, lRdmptFileDate, lDelqMatch;

/******************************** Kin_MergeOwner *****************************
 *
 * Return 0 if successful, >0 if error

 * Notes: wait to be tested
 *
 *****************************************************************************/

void Kin_MergeOwner(char *pOutbuf, char *pRollRec)
{
   int   iTmp, iRet;
   char  acTmp1[128], acTmp[128], acSave1[64], *pTmp, *pTmp1;
   char  acOwner1[64], acName1[64], acName2[64];

   KIN_ROLL *pRec;
   OWNER    myOwner;

   pRec = (KIN_ROLL *)pRollRec;

   // Clear output buffer if needed
   removeNames(pOutbuf, false, false);
   memset(acTmp, 0, 128);

   // Initialize
   memcpy(acTmp1, pRec->OwnerName, RSIZ_NAME);
   myTrim(acTmp1, RSIZ_NAME);

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "0026T038A", 9))
   //   iTmp = 0;
#endif

   // Remove multiple spaces
   iTmp = 0;
   pTmp = acTmp1;
   while (*pTmp)
   {
      // skip unwanted chars at beginning of name
      if (*pTmp < '1' && iTmp == 0)
      {
         pTmp++;
         continue;
      }

      // Fix problem as RAMON&PURISIMA
      if (*pTmp == '&' && acTmp[iTmp-1] != ' ')
      {
         acTmp[iTmp++] = ' ';
         acTmp[iTmp++] = *pTmp;
         if (*(pTmp+1) != ' ')
            acTmp[iTmp++] = ' ';
      } else
         acTmp[iTmp++] = toupper(*pTmp);

      // Remove multi-space
      if (*pTmp == ' ')
         while (*pTmp == ' ') pTmp++;
      else
         pTmp++;

      // Remove unwanted characters
      if (*pTmp == '`' || *pTmp == ':' || *pTmp == 39)
         pTmp++;
   }
   if (acTmp[iTmp-1] == ' ')
      iTmp -= 1;
   acTmp[iTmp] = 0;

   // Update vesting
   iTmp = updateVesting(myCounty.acCntyCode, acTmp, pOutbuf+OFF_VEST, pOutbuf+OFF_ETAL_FLG);

   // Remove % of ownership
   if (pTmp=strchr((char *)&acTmp[0], '%'))
   {
      do
      {
         pTmp--;
      } while (*pTmp != ' ');
      *pTmp = 0;
   }

   // Check for vesting - temporary remove, will reinstated later
   char acVest[16];
   acVest[0] = 0;
   acSave1[0] = 0;
   if (pTmp=strstr(acTmp, " H/W"))
   {
      *pTmp = 0;
      pTmp1 = pTmp+4;
      if (!*pTmp1)
         strcpy(acVest, "HW");
      else
      {
         if (pTmp=strstr(pTmp1, "JT"))
            strcpy(acVest, "HWJT");
         else if (pTmp=strstr(pTmp1, "CP"))
            strcpy(acVest, "HWCP");
         else
         {
            strcpy(acVest, "HW");
         }
      }
   } else if (pTmp=strstr(acTmp, " W/H"))
   {
      strcpy(acVest, "WH");
      *pTmp = 0;
   } else if (pTmp=strstr(acTmp, " T/C"))
   {
      strcpy(acVest, "TC");
      *pTmp = 0;
   } else if (pTmp=strstr(acTmp, " S/M"))
   {
      strcpy(acVest, "SM");
      *pTmp = 0;
   } else if (pTmp=strstr(acTmp, " M/M"))
   {
      strcpy(acVest, "MM");
      *pTmp = 0;
   } else if (pTmp=strstr(acTmp, " JT"))
   {
      *pTmp = 0;
      strcpy(acVest, "JT");
   } else if (pTmp=strstr(acTmp, " CP"))
   {
      strcpy(acVest, "CP");
      *pTmp = 0;
   }

   // If there is no comma in name, don't parse
   if (!strchr(acTmp, ','))
   {
      if (pTmp = strstr(acTmp, " THE"))
         *pTmp = 0;

      iTmp = strlen(acTmp);
      if (iTmp > SIZ_NAME1)
         iTmp = SIZ_NAME1;
      memcpy(pOutbuf+OFF_NAME1, acTmp, iTmp);
      memcpy(pOutbuf+OFF_NAME_SWAP, acTmp, iTmp);
      return;
   }

   // Save Name1
   strcpy(acOwner1, acTmp);
   remChar(acOwner1, ',');

   // Remove suffixes to format swap name
   if ((pTmp = strchr(acTmp, '('))      || (pTmp=strstr(acTmp, " CO TR")) ||
      (pTmp=strstr(acTmp, " CO-TR"))    || (pTmp=strstr(acTmp, " COTR"))  ||
      (pTmp=strstr(acTmp, ",ET AL"))    || (pTmp=strstr(acTmp, " ET AL")) ||
      (pTmp=strstr(acTmp, " ETAL"))     || (pTmp=strstr(acTmp, " ET-AL")) ||
      (pTmp=strstr(acTmp, ",TRSTE"))    || (pTmp=strstr(acTmp, " TSTE"))  ||
      (pTmp=strstr(acTmp, ",TRUSTEE"))  || (pTmp=strstr(acTmp, " TTEE"))  ||
      (pTmp=strstr(acTmp, " TRST"))     || (pTmp=strstr(acTmp, " TRES")) )
      *pTmp = 0;

   if ((pTmp=strstr(acTmp, " RVC TR")) || (pTmp=strstr(acTmp, " REVOC")) ||
       (pTmp=strstr(acTmp, " REV TR")) || (pTmp=strstr(acTmp, " REV LIV")) ||
       (pTmp=strstr(acTmp, " REVC TR"))|| (pTmp=strstr(acTmp, " LIFE EST")) ||
       (pTmp=strstr(acTmp, " LVG TR")) || (pTmp=strstr(acTmp, " REV LVNG")) ||
       (pTmp=strstr(acTmp, " LIV TR")) || (pTmp=strstr(acTmp, " RVC LVG")) ||
       (pTmp=strstr(acTmp, " LVNG TR"))|| (pTmp=strstr(acTmp, " BYPASS TR")) ||
       (pTmp=strstr(acTmp, " FAM TR")) || (pTmp=strstr(acTmp, " FAMILY ")) ||
       (pTmp=strstr(acTmp, " EST OF")) || (pTmp=strstr(acTmp, " ESTS OF")) ||
       (pTmp=strstr(acTmp, " LF EST")) || (pTmp=strstr(acTmp, " LIVING ")) ||
       (pTmp=strstr(acTmp, " ESTATE")) || (pTmp=strstr(acTmp, " SURVIVOR")) ||
       (pTmp=strstr(acTmp, " TESTAMENTARY")) || (pTmp=strstr(acTmp, " IRREVOCABLE")) ||
       (pTmp=strstr(acTmp, " TRUST"))
       )
   {  // KLOTZ GENE L & JANE G FAMILY TRUST TESTAMENTARY
      if (isdigit(*(pTmp-1)) )
         while (isdigit(*(--pTmp)));
      strcpy(acSave1, pTmp);
      *pTmp = 0;
      strcpy(acName1, acTmp);
   } else
      strcpy(acName1, acTmp);

   if (pTmp = strstr(acSave1, " THE"))
      *pTmp = 0;

   // We keep first two name only, drop the rest
   // TAYLOR JAMES H & MERRIEL & LEFEVRE PATRICIA M & RE
   if (pTmp = strchr(acName1, '&'))
   {
      if (pTmp1 = strchr(pTmp+1, '&'))
      {
         *pTmp1++ = 0;
         if (*pTmp1 == ' ') pTmp1++;
         strcpy(acName2, pTmp1);
      }
   }

   // Now parse owners
   iRet = 0;
   if (strchr(acName1, ' '))
   {
      iRet = splitOwner(acName1, &myOwner, 6);
      // If parsable, use it.  Otherwise, keep the original
      if (iRet >= 0)
      {
         strcpy(acName1, myOwner.acName1);
         strcpy(acTmp1, myOwner.acSwapName);
      } else
      {
         // Use Name1 for swapname
         strcpy(acTmp1, acName1);
      }

      // Check for saving?
      if (acSave1[0])
      {
         myTrim(acName1);
         strcat(acName1, acSave1);
      }

      iTmp = strlen(acTmp1);
      if (iTmp > SIZ_NAME_SWAP)
         iTmp = SIZ_NAME_SWAP;
      memcpy(pOutbuf+OFF_NAME_SWAP, acTmp1, iTmp);
   } else
   {
      if (acSave1[1] > ' ')
         strcat(acName1, acSave1);
      else
         strcpy(acName1, acTmp1);

      iTmp = strlen(acName1);
      if (iTmp > SIZ_NAME1)
         iTmp = SIZ_NAME1;
      memcpy(pOutbuf+OFF_NAME_SWAP, acName1, iTmp);
   }

   // Update owner1
   iTmp = strlen(acOwner1);
   if (iTmp > SIZ_NAME1)
      iTmp = SIZ_NAME1;
   memcpy(pOutbuf+OFF_NAME1, acOwner1, iTmp);

   // Vesting code
   //if (acVest[0] > ' ')
   //   memcpy(pOutbuf+OFF_VEST, acVest, strlen(acVest));
}

/********************************* Kin_MergeSAdr *****************************
 *
 * Return 0 if successful, >0 if error
 *
 *****************************************************************************/

void Kin_MergeSAdr(char *pOutbuf, char *pAdrRec)
{
   KINSITUS *pRec;
   char     acTmp[256], acAddr1[64], acAddr2[64], acCode[32];
   int      iTmp, iSfxCode;
   long     lTmp;

   ADR_REC  sAdrRec;
   pRec = (KINSITUS *)pAdrRec;

   // Situs
   acAddr1[0] = 0;
   acAddr2[0] = 0;
   memset((void *)&sAdrRec, 0, sizeof(ADR_REC));
   removeSitus(pOutbuf);

   // Copy StrNum
   lTmp = atoin(pRec->S_StrNum, RSIZ_S_STRNUM);
   if (lTmp > 0)
   {
      iTmp = sprintf(acAddr1, "%d", lTmp);
      memcpy(pOutbuf+OFF_S_STRNUM, acAddr1, iTmp);
      memcpy(pOutbuf+OFF_S_HSENO,  acAddr1, iTmp);

      if (pRec->S_StrDir[0] > ' ')
         *(pOutbuf+OFF_S_DIR) = pRec->S_StrDir[0];

      if (pRec->S_StrName[0] > ' ')
      {
         // Copy street name
         memcpy(acTmp, pRec->S_StrName, RSIZ_S_STRNAME);
         myTrim(acTmp, RSIZ_S_STRNAME);
         iTmp = strlen(acTmp);
         if (iTmp > SIZ_S_STREET)
            iTmp = SIZ_S_STREET;
         memcpy(pOutbuf+OFF_S_STREET, acTmp, iTmp);
      }

      if (pRec->S_StrSfx[0] > ' ')
      {
         memcpy(acTmp, pRec->S_StrSfx, RSIZ_S_STRSFX);
         // Translate to code
         iSfxCode = GetSfxCode(myTrim(acTmp, RSIZ_S_STRSFX));
         if (iSfxCode)
         {
            iTmp = sprintf(acCode, "%d", iSfxCode);
            memcpy(pOutbuf+OFF_S_SUFF, acCode, iTmp);
         } else
            LogMsg0("*** Unknown suffix %s [%.*s]", acTmp, iApnLen, pOutbuf);
      }

      // Copy unit#
      if (pRec->S_Unit[0] > ' ')
      {
         if (pRec->S_Unit[0] != '#')
         {
            acTmp[0] = '#';
            memcpy((char *)&acTmp[1], pRec->S_Unit, RSIZ_S_UNIT);
         } else
            memcpy((char *)&acTmp[0], pRec->S_Unit, RSIZ_S_UNIT);
         myTrim(acTmp, RSIZ_S_UNIT+1);
         memcpy(pOutbuf+OFF_S_UNITNO, acTmp, strlen(acTmp));
      }

      sprintf(acTmp, " %c %.24s %.4s %.6s", pRec->S_StrDir[0],
         pRec->S_StrName, pRec->S_StrSfx, pOutbuf+OFF_S_UNITNO);
      strcat(acAddr1, acTmp);
      blankRem(acAddr1);
      memcpy(pOutbuf+OFF_S_ADDR_D, acAddr1, strlen(acAddr1));
   }

   // state
   memcpy(pOutbuf+OFF_S_ST, "CA", 2);

   // City
   if (pRec->S_CityAbbr[0] > ' ')
   {
      memcpy(acTmp, pRec->S_CityAbbr, RSIZ_S_CITY);
      myTrim(acTmp, RSIZ_S_CITY);
      Abbr2Code(acTmp, acCode, acAddr2);
      if (acCode[0])
      {
         memcpy(pOutbuf+OFF_S_CITY, acCode, strlen(acCode));
         strcat(acAddr2, " CA");
      }

      //blankRem(acAddr2);
      memcpy(pOutbuf+OFF_S_CTY_ST_D, acAddr2, strlen(acAddr2));
   }

   // Zipcode
   //lTmp = atoin(pRec->S_Zip, RSIZ_S_ZIP);
   //if (lTmp > 90000)
   //{
   //   memcpy(pOutbuf+OFF_S_ZIP, pRec->S_Zip, SIZ_S_ZIP);
   //   lTmp = atoin(pRec->S_Zip4, 4);
   //   if (lTmp > 0)
   //      memcpy(pOutbuf+OFF_S_ZIP4, pRec->S_Zip4, 4);
   //}

}

/********************************* Kin_MergeMAdr *****************************
 *
 * Mail address has similar problem as situs, but worse due to inconsistency.
 * Now we have to check for direction both pre and post strname.
 *
 *****************************************************************************/

void Kin_MergeMAdr(char *pOutbuf, char *pMailRec)
{
   KINMAIL  *pRec;
   char     acTmp[256], acAddr1[64], acAddr2[64];
   char     *p1, *p2, *p3, *p4;
   int      iTmp, iLen;
   long     lTmp;

   ADR_REC  sAdrRec;

   pRec = (KINMAIL *)pMailRec;

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "800037786", 9))
   //   iTmp = 0;
#endif

   acAddr1[0] = 0;
   acAddr2[0] = 0;
   memset((void *)&sAdrRec, 0, sizeof(ADR_REC));
   removeMailing(pOutbuf, true, true);

   if (pRec->M_Addr1[0] <= ' ' || pRec->M_Addr2[0] <= ' ')
      return;

   p1 = pRec->M_Addr1;
   p2 = pRec->M_Addr2;
   p3 = pRec->M_Addr3;
   p4 = pRec->M_Addr4;
   iLen = RSIZ_M_ADDR_1;

   if (!memcmp(pRec->M_Addr1, "C/O", 3) || !memcmp(pRec->M_Addr1, "DBA ", 4))
   {
      vmemcpy(acTmp, pRec->M_Addr1, RSIZ_M_ADDR_1);
      iTmp = blankRem(acTmp, RSIZ_M_ADDR_1);
      if (!memcmp(acTmp, "DBA", 3))
         vmemcpy(pOutbuf+OFF_DBA, acTmp, SIZ_DBA);
      else
         updateCareOf(pOutbuf, acTmp, iTmp);
      p1 = pRec->M_Addr2;
      p2 = pRec->M_Addr3;
      p3 = pRec->M_Addr4;
      p4 = "";
      iLen = RSIZ_M_ADDR_2;
   }
   if (!memcmp(pRec->M_Addr2, "C/O", 3) || !memcmp(pRec->M_Addr2, "DBA ", 4))
   {
      memcpy(acTmp, pRec->M_Addr2, RSIZ_M_ADDR_2);
      iTmp = blankRem(acTmp, RSIZ_M_ADDR_2);
      if (!memcmp(acTmp, "DBA", 3))
         vmemcpy(pOutbuf+OFF_DBA, acTmp, SIZ_DBA);
      else
         updateCareOf(pOutbuf, acTmp, iTmp);
      p1 = pRec->M_Addr3;
      p2 = pRec->M_Addr4;
      p3 = p4 = "";
      iLen = RSIZ_M_ADDR_2;
   }

   if (*p4 > ' ')
   {
      p2 = pRec->M_Addr4;
      p1 = pRec->M_Addr3;
      iLen = RSIZ_M_ADDR_2;
   } else if (*p3 > ' ')
   {
      p2 = pRec->M_Addr3;
      p1 = pRec->M_Addr2;
      iLen = RSIZ_M_ADDR_2;
   }
   memcpy(acAddr1, p1, iLen);
   blankRem(acAddr1, iLen);
   memcpy(acAddr2, p2, RSIZ_M_ADDR_2);
   blankRem(acAddr2, RSIZ_M_ADDR_2);

   // Parse address
   parseMAdr1(&sAdrRec, acAddr1);
   parseMAdr2(&sAdrRec, acAddr2);
   memcpy(pOutbuf+OFF_M_ADDR_D, acAddr1, strlen(acAddr1));
   memcpy(pOutbuf+OFF_M_CTY_ST_D, acAddr2, strlen(acAddr2));

   if (sAdrRec.lStrNum > 0)
   {
      iTmp = sprintf(acTmp, "%d", sAdrRec.lStrNum);
      memcpy(pOutbuf+OFF_M_STRNUM, acTmp, iTmp);
   }

   if (sAdrRec.strSub[0] > ' ')
      memcpy(pOutbuf+OFF_M_STR_SUB, sAdrRec.strSub, strlen(sAdrRec.strSub));

   if (sAdrRec.strDir[0] > ' ')
      *(pOutbuf+OFF_M_DIR) = sAdrRec.strDir[0];
   if (sAdrRec.strName[0] > ' ')
      vmemcpy(pOutbuf+OFF_M_STREET, sAdrRec.strName, SIZ_M_STREET);

   if (sAdrRec.strSfx[0] > ' ')
      vmemcpy(pOutbuf+OFF_M_SUFF, sAdrRec.strSfx, SIZ_M_SUFF);

   // Copy unit#
   if (sAdrRec.Unit[0] > ' ')
      vmemcpy(pOutbuf+OFF_M_UNITNO, sAdrRec.Unit, SIZ_M_UNITNO);


   // city state
   if (sAdrRec.City[0] > ' ')
   {
      vmemcpy(pOutbuf+OFF_M_CITY, sAdrRec.City, SIZ_M_CITY);
      vmemcpy(pOutbuf+OFF_M_ST, sAdrRec.State, SIZ_M_ST);
   }

   // Zipcode
   lTmp = atoin(sAdrRec.Zip, SIZ_M_ZIP);
   if (lTmp > 400)
   {
      iTmp = sprintf(acTmp, "%.5d", lTmp);
      memcpy(pOutbuf+OFF_M_ZIP, acTmp, iTmp);
   }
}

/********************************* Kin_MergeChar *****************************
 *
 * This function updates some characteristics (pdrfile.txt) to roll file.  Others
 * will come from salfile.txt
 *
 * 1) Stories is counted if BldgSqft2 > 0.
 * 2) Don't use LotSqft unless roll record is blank
 * 3) On parcel with multiple buildings, add them up (bldgsqft, beds, baths, ...)
 *    - If their is building 0, use that record only ignoring the rest.
 *    - Use YrBlt and EffYr from BldgNum 1.
 *
 *
 *****************************************************************************/

int Kin_MergeChar(char *pOutbuf)
{

   static   char acRec[1024], *pRec=NULL;
   char     acTmp[256], acCode[32];
   long     lTmp, lBldgSqft, lGarSqft;
   int      iRet, iTmp, iLoop, iBeds, iFBath, iHBath, iRooms;
   KIN_CHAR *pChar;

   iRet=iBeds=iFBath=iHBath=iRooms=0;
   lBldgSqft=lGarSqft=0;

   // Get first Char rec for first call
   if (!pRec && !lCharMatch)
   {
      pRec = fgets(acRec, 1024, fdChar);
   }
   pChar = (KIN_CHAR *)pRec;

   do
   {
      if (!pRec)
      {
         fclose(fdChar);
         fdChar = NULL;
         return 1;      // EOF
      }

      // Compare Apn
      iLoop = memcmp(pOutbuf, pChar->Apn, CSIZ_APN);
      if (iLoop > 0)
      {
         if (bDebug)
            LogMsg0("Skip Char rec  %.*s", CSIZ_APN, pChar->Apn);
         pRec = fgets(acRec, 1024, fdChar);
         lCharSkip++;
      }
   } while (iLoop > 0);

   // If not match, return
   if (iLoop)
      return 1;

   // Quality Class
   if (isalpha(pChar->BldgClass[0]))
   {  // D045A
      *(pOutbuf+OFF_BLDG_CLASS) = pChar->BldgClass[0];
      iTmp = atoin((char *)&pChar->BldgClass[1], 2);
      if (iTmp > 0)
      {
         sprintf(acTmp, "%d.%c", iTmp, pChar->BldgClass[3]);
         iRet = Value2Code(acTmp, acCode, NULL);
         blankPad(acCode, SIZ_BLDG_QUAL);
         memcpy(pOutbuf+OFF_BLDG_QUAL, acCode, SIZ_BLDG_QUAL);
      }
   }

   // YrEff
   lTmp = atoin(pChar->EffYear, CSIZ_EFF_YR);
   if (lTmp > 1700)
      memcpy(pOutbuf+OFF_YR_EFF, pChar->EffYear, SIZ_YR_BLT);

   // YrBlt
   lTmp = atoin(pChar->YrBlt, CSIZ_EFF_YR);
   if (lTmp > 1700)
      memcpy(pOutbuf+OFF_YR_BLT, pChar->YrBlt, SIZ_YR_BLT);

   // BldgSqft
   lBldgSqft = atoin(pChar->BldgSqft, CSIZ_LIVING_AREA);
   if (lBldgSqft > 10)
   {
      sprintf(acTmp, "%*u", SIZ_BLDG_SF, lBldgSqft);
      memcpy(pOutbuf+OFF_BLDG_SF, acTmp, SIZ_BLDG_SF);
   }

   // Beds
   iBeds = atoin(pChar->Beds, CSIZ_BEDS);
   if (iBeds > 0)
   {
      sprintf(acTmp, "%*u", SIZ_BEDS, iBeds);
      memcpy(pOutbuf+OFF_BEDS, acTmp, SIZ_BEDS);
   }

   // Baths - 99V9
   iFBath = atoin(pChar->Baths, CSIZ_BATHS-1);
   if (iFBath > 0)
   {
      // Clear old data
      *(pOutbuf+OFF_BATH_1Q) = ' ';
      *(pOutbuf+OFF_BATH_2Q) = ' ';
      *(pOutbuf+OFF_BATH_3Q) = ' ';
      *(pOutbuf+OFF_BATH_4Q) = ' ';

      sprintf(acTmp, "%*u", SIZ_BATH_F, iFBath);
      memcpy(pOutbuf+OFF_BATH_F, acTmp, SIZ_BATH_F);
      *(pOutbuf+OFF_BATH_4Q) = pChar->Baths[0];

      // Half bath: 3,5,7
      iTmp = pChar->Baths[CSIZ_BATHS-1] & 0x0F;
      if (iTmp > 0)
      {
         memcpy(pOutbuf+OFF_BATH_H, " 1", 2);
         if (iTmp >= 6)
            *(pOutbuf+OFF_BATH_3Q) = '1';
         else if (iTmp >= 4)
            *(pOutbuf+OFF_BATH_2Q) = '1';
         else 
            *(pOutbuf+OFF_BATH_1Q) = '1';
      }
   }

   // Rooms
   iRooms = atoin(pChar->Rooms, CSIZ_ROOMS);
   if (iRooms > 0)
   {
      sprintf(acTmp, "%*u", SIZ_ROOMS, iRooms);
      memcpy(pOutbuf+OFF_ROOMS, acTmp, SIZ_ROOMS);
   }

   // Garage Sqft
   lGarSqft = atoin(pChar->GarSqft, CSIZ_GARAGE_AREA);
   if (lGarSqft > 10)
   {
      sprintf(acTmp, "%*u", SIZ_GAR_SQFT, lGarSqft);
      memcpy(pOutbuf+OFF_GAR_SQFT, acTmp, SIZ_GAR_SQFT);
      // This is to cover where Garage_Spcs is 0
      *(pOutbuf+OFF_PARK_TYPE) = 'Z';
   }

   // Carport?
   if (pChar->Carport == 'Y')
      *(pOutbuf+OFF_PARK_TYPE) = 'C';

   // Heating
   if (pChar->Heating[0] == 'C')
      *(pOutbuf+OFF_HEAT) = '1';          // Conventional
   else if (pChar->Heating[0] == 'M')
      *(pOutbuf+OFF_HEAT) = '2';          // Modern
   else if (!memcmp(pChar->Heating, "FD", 2))
      *(pOutbuf+OFF_HEAT) = 'B';          // Down Flow
   else if (!memcmp(pChar->Heating, "FU", 2))
      *(pOutbuf+OFF_HEAT) = 'B';          // Up Flow
   else if (!memcmp(pChar->Heating, "WF", 2))
      *(pOutbuf+OFF_HEAT) = 'M';          // Wall or Floor
   else if (pChar->Heating[0] > ' ')
      iTmp = 0;

   // Cooling
   if (pChar->Cooling[0] == 'C')
      *(pOutbuf+OFF_AIR_COND) = '1';      // Conventional
   else if (pChar->Cooling[0] == 'M')
      *(pOutbuf+OFF_AIR_COND) = '2';      // Modern
   else if (!memcmp(pChar->Cooling, "O ", 2))
      *(pOutbuf+OFF_AIR_COND) = 'X';      // Other
   else if (!memcmp(pChar->Cooling, "R ", 2))
      *(pOutbuf+OFF_AIR_COND) = 'A';      // Refrigeration 
   else if (!memcmp(pChar->Cooling, "EC", 2))
      *(pOutbuf+OFF_AIR_COND) = 'E';      // Evaporator Cool
   else if (!memcmp(pChar->Cooling, "TW", 2))
      *(pOutbuf+OFF_AIR_COND) = 'L';      // Thru Wall
   else if (pChar->Cooling[0] > ' ')
      iTmp = 0;

   // Pool/Spa
   if (pChar->Pool_Spa == 'P')
      *(pOutbuf+OFF_POOL) = 'P';    // Pool
   else if (pChar->Pool_Spa == 'S')
      *(pOutbuf+OFF_POOL) = 'S';    // Spa
   else if (pChar->Pool_Spa == 'B')
      *(pOutbuf+OFF_POOL) = 'C';    // Pool/Spa

   // Fireplace
   if (pChar->Fireplace > '0')
   {
      *(pOutbuf+OFF_FIRE_PL) = ' ';
      *(pOutbuf+OFF_FIRE_PL+1) = pChar->Fireplace;
   }

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "050010001", 9))
   //   iTmp = 0;
#endif
   // Lot sqft - Lot Acres: only update if current roll value not present
   lTmp = atoin(pChar->LotSqft, CSIZ_LOT_AREA);
   if (lTmp > 100 && *(pOutbuf+OFF_LOT_ACRES+SIZ_LOT_ACRES-1) == ' ')
   {
      sprintf(acTmp, "%*u", SIZ_LOT_SQFT, lTmp);
      memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);

      lTmp = (long)((((double)lTmp*ACRES_FACTOR)/SQFT_PER_ACRE)+0.5);
      sprintf(acTmp, "%*u", SIZ_LOT_SQFT, lTmp);
      memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_SQFT);
   }

   // Stories 99V9
   iTmp = atoin(pChar->Stories, CSIZ_STORIES);
   if (iTmp > 9)
   {
      sprintf(acTmp, "%.1f  ", (float)(iTmp/10));
      memcpy(pOutbuf+OFF_STORIES, acTmp, SIZ_STORIES);
   }

   // Park spaces

   // Floor

   // Misc Impr
   *(pOutbuf+OFF_MISC_IMPR) = pChar->MiscImpr;    


#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "003120007", 9))
   //   lTmp = 0;
#endif
   // Zoning

   // Usecode

   // Basement Sqft
   iTmp = atoin(pChar->BsmtSqft, CSIZ_BASEMENT_SQFT);
   if (iTmp > 50)
   {
      sprintf(acTmp, "%d     ", iTmp);
      memcpy(pOutbuf+OFF_BSMT_SF, acTmp, SIZ_BSMT_SF);
   }

   // View

   // Others
   lCharMatch++;

   // Get next Char rec
   pRec = fgets(acRec, 1024, fdChar);

   return 0;
}

/********************************* Kin_MergeSale *****************************
 *
 *
 *
 *****************************************************************************

int Kin_MergeSale1(char *pOutbuf, char *pSalebuf)
{
   long  lCurSaleDt, lLstSaleDt, lPrice, lLastAmt;
   int   iTmp;
   char  acTmp[32];
   bool  bUpdtAll=true;
   KIN_CSAL *pSaleRec = (KIN_CSAL *)pSalebuf;

   // Check case #1
   iTmp = memcmp(pSaleRec->DocNum, pOutbuf+OFF_SALE1_DOC, SSIZ_DOC_NUM);
   if (iTmp <= 0)
      return -1;

   lCurSaleDt = atoin(pSaleRec->DocDate, SIZ_SALE1_DT);
   lLstSaleDt = atoin(pOutbuf+OFF_SALE1_DT, SIZ_SALE1_DT);
   lPrice = atoin(pSaleRec->SalePrice, SSIZ_SALE_PRICE);
   lLastAmt = atoin(pOutbuf+OFF_SALE1_AMT, SIZ_SALE1_AMT);

   if (lCurSaleDt == lLstSaleDt)
   {
      // Check case #2
      if (!lPrice && lLastAmt)
         return 0;

      // Check case #3
      if (!lPrice && !lLastAmt)
         bUpdtAll = false;    // Update docnumonly

      // case #4: update current sale
      iTmp = 0;

   } else // case #5
   {
      // Move sale2 to sale3
      memcpy(pOutbuf+OFF_SALE3_DOC, pOutbuf+OFF_SALE2_DOC, SIZ_SALE3_DOC);
      memcpy(pOutbuf+OFF_SALE3_DT, pOutbuf+OFF_SALE2_DT, SIZ_SALE3_DT);
      memcpy(pOutbuf+OFF_SALE3_AMT, pOutbuf+OFF_SALE2_AMT, SIZ_SALE3_AMT);
      *(pOutbuf+OFF_AR_CODE1+2) = *(pOutbuf+OFF_AR_CODE1+1);

      // Move sale1 to sale2
      memcpy(pOutbuf+OFF_SALE2_DOC, pOutbuf+OFF_SALE1_DOC, SIZ_SALE3_DOC);
      memcpy(pOutbuf+OFF_SALE2_DT, pOutbuf+OFF_SALE1_DT, SIZ_SALE3_DT);
      memcpy(pOutbuf+OFF_SALE2_AMT, pOutbuf+OFF_SALE1_AMT, SIZ_SALE3_AMT);
      *(pOutbuf+OFF_AR_CODE1+1) = *(pOutbuf+OFF_AR_CODE1);
      iTmp = 1;
   }

   // Update current sale
   memcpy(pOutbuf+OFF_SALE1_DOC, pSaleRec->DocNum, SSIZ_DOC_NUM);
   if (bUpdtAll)
   {
      if (lPrice > 0)
      {
         sprintf(acTmp, "%*u", SIZ_SALE1_AMT, lPrice);
         memcpy(pOutbuf+OFF_SALE1_AMT, acTmp, SIZ_SALE1_AMT);
      } else
         memset(pOutbuf+OFF_SALE1_AMT, ' ', SIZ_SALE1_AMT);

      memcpy(pOutbuf+OFF_SALE1_DT, pSaleRec->DocDate, SIZ_SALE1_DT);
      memcpy(pOutbuf+OFF_SELLER, pSaleRec->Seller, SIZ_SELLER);

      memcpy(pOutbuf+OFF_TRANSFER_DOC, pSaleRec->DocNum, SSIZ_DOC_NUM);
      memcpy(pOutbuf+OFF_TRANSFER_DT, pSaleRec->DocDate, SIZ_TRANSFER_DT);
   }

   *(pOutbuf+OFF_AR_CODE1) = 'A';    // Assessor

   return iTmp;
}

int Kin_MergeSale2(char *pOutbuf, char *pSalebuf)
{
   long  lCurSaleDt, lLstSaleDt, lPrice, lLastAmt;
   int   iTmp;
   char  acTmp[32];
   bool  bUpdtAll=true;
   KIN_CSAL *pSaleRec = (KIN_CSAL *)pSalebuf;

   lCurSaleDt = atoin(pSaleRec->DocDate, SIZ_SALE1_DT);
   lLstSaleDt = atoin(pOutbuf+OFF_SALE1_DT, SIZ_SALE1_DT);
   if (lCurSaleDt < lLstSaleDt)
      return -1;

   lPrice = atoin(pSaleRec->SalePrice, SSIZ_SALE_PRICE);
   lLastAmt = atoin(pOutbuf+OFF_SALE1_AMT, SIZ_SALE1_AMT);
   if (lCurSaleDt == lLstSaleDt)
   {
      // Case #1
      iTmp = memcmp(pSaleRec->DocNum, pOutbuf+OFF_SALE1_DOC, SSIZ_DOC_NUM);
      if (iTmp <= 0)
         return -1;

      // Check case #2
      if (!lPrice && lLastAmt)
         return 0;

      // Check case #3
      if (!lPrice && !lLastAmt)
         bUpdtAll = false;    // Update docnumonly

      // case #4: update current sale
      iTmp = 0;

   } else // case #5
   {
      // Move sale2 to sale3
      memcpy(pOutbuf+OFF_SALE3_DOC, pOutbuf+OFF_SALE2_DOC, SIZ_SALE3_DOC);
      memcpy(pOutbuf+OFF_SALE3_DT, pOutbuf+OFF_SALE2_DT, SIZ_SALE3_DT);
      memcpy(pOutbuf+OFF_SALE3_AMT, pOutbuf+OFF_SALE2_AMT, SIZ_SALE3_AMT);
      *(pOutbuf+OFF_AR_CODE1+2) = *(pOutbuf+OFF_AR_CODE1+1);

      // Move sale1 to sale2
      memcpy(pOutbuf+OFF_SALE2_DOC, pOutbuf+OFF_SALE1_DOC, SIZ_SALE3_DOC);
      memcpy(pOutbuf+OFF_SALE2_DT, pOutbuf+OFF_SALE1_DT, SIZ_SALE3_DT);
      memcpy(pOutbuf+OFF_SALE2_AMT, pOutbuf+OFF_SALE1_AMT, SIZ_SALE3_AMT);
      *(pOutbuf+OFF_AR_CODE1+1) = *(pOutbuf+OFF_AR_CODE1);
      iTmp = 1;
   }

   // Update current sale
   memcpy(pOutbuf+OFF_SALE1_DOC, pSaleRec->DocNum, SSIZ_DOC_NUM);
   if (bUpdtAll)
   {
      if (lPrice > 0)
      {
         sprintf(acTmp, "%*u", SIZ_SALE1_AMT, lPrice);
         memcpy(pOutbuf+OFF_SALE1_AMT, acTmp, SIZ_SALE1_AMT);
      } else
         memset(pOutbuf+OFF_SALE1_AMT, ' ', SIZ_SALE1_AMT);

      memcpy(pOutbuf+OFF_SALE1_DT, pSaleRec->DocDate, SIZ_SALE1_DT);
      memcpy(pOutbuf+OFF_SELLER, pSaleRec->Seller, SIZ_SELLER);

      memcpy(pOutbuf+OFF_TRANSFER_DOC, pSaleRec->DocNum, SSIZ_DOC_NUM);
      memcpy(pOutbuf+OFF_TRANSFER_DT, pSaleRec->DocDate, SIZ_TRANSFER_DT);
   }

   *(pOutbuf+OFF_AR_CODE1) = 'A';    // Assessor

   return iTmp;
}

// Cum sale file is a fixed length 144-byte record
// iFlg = 1 for LDR and 2 for roll update
int Kin_MergeSale(char *pOutbuf, int iFlg)
{
   static   char  acRec[1024], *pRec=NULL;
   static   int   iSaleLen;

   int      iRet, iLoop;
   KIN_CSAL *pSale;

   // Get first Char rec for first call
   if (!pRec && !lSaleMatch)
   {
      iSaleLen = fread(acRec, 1, iCSalLen, fdSale);
      pRec = acRec;
   }

   pSale = (KIN_CSAL *)&acRec[0];
   do
   {
      if (iSaleLen != iCSalLen)
      {
         fclose(fdSale);
         fdSale = NULL;
         return 1;      // EOF
      }

      // Compare Apn
      iLoop = memcmp(pOutbuf, pSale->Apn, SSIZ_APN);
      if (iLoop > 0)
      {
         if (bDebug)
            LogMsg0("Skip Sale rec  %.*s", SSIZ_APN, pSale->Apn);
         iSaleLen = fread(acRec, 1, iCSalLen, fdSale);
         lSaleSkip++;
      }
   } while (iLoop > 0);

   // If not match, return
   if (iLoop)
      return 0;

   do
   {
#ifdef _DEBUG
      //if (!memcmp(pOutbuf, "014202004", 9))
      //   iRet = 0;
#endif
      if (iFlg == 1)
         iRet = Kin_MergeSale1(pOutbuf, acRec);
      else
         iRet = Kin_MergeSale2(pOutbuf, acRec);

      // Get next sale record
      iSaleLen = fread(acRec, 1, iCSalLen, fdSale);
      if (iSaleLen != iCSalLen)
      {
         fclose(fdSale);
         fdSale = NULL;
         break;
      }
   } while (!memcmp(pOutbuf, pSale->Apn, SSIZ_APN));

   lSaleMatch++;

   return 0;
}
*/

/********************************* Kin_MergeRoll *****************************
 *
 * Return 0 if successful, >0 if error
 *
 *****************************************************************************/

void Kin_MergeRoll(char *pOutbuf, char *pRollRec, int iLen, int iFlag)
{
   KIN_ROLL *pRec;
   char     acTmp[256], acTmp1[256];
   long     lTmp;
   double   dTmp;
   int      iRet;

   pRec = (KIN_ROLL *)pRollRec;
   if (iFlag & CREATE_R01)
   {
      // Clear output buffer
      memset(pOutbuf, ' ', iRecLen);

      // Start copying data
      memcpy(pOutbuf, pRec->Apn, RSIZ_APN);
      memcpy(pOutbuf+OFF_CO_NUM, "16KINA", 6);

      // Format APN
      iRet = formatApn(pRec->Apn, acTmp, &myCounty);
      memcpy(pOutbuf+OFF_APN_D, acTmp, iRet);

      // Create MapLink
      iRet = formatMapLink(pRec->Apn, acTmp, &myCounty);
      memcpy(pOutbuf+OFF_MAPLINK, acTmp, iRet);

      // Create index map link
      if (getIndexPage(acTmp, acTmp1, &myCounty))
         memcpy(pOutbuf+OFF_IMAPLINK, acTmp1, iRet);

      // Year assessed
      memcpy(pOutbuf+OFF_YR_ASSD, myCounty.acYearAssd, 4);

      // Land
      long lLand = atoin(pRec->Land, RSIZ_LAND);
      if (lLand > 0)
      {
         sprintf(acTmp, "%*u", SIZ_LAND, lLand);
         memcpy(pOutbuf+OFF_LAND, acTmp, SIZ_LAND);
      }

      // Improve
      long lImpr = atoin(pRec->Struct_Impr, RSIZ_LAND);
      if (lImpr > 0)
      {
         sprintf(acTmp, "%*u", SIZ_IMPR, lImpr);
         memcpy(pOutbuf+OFF_IMPR, acTmp, SIZ_IMPR);
      }

      // Other value - Personal property
      long lPers = atoin(pRec->PP_Val, RSIZ_PERS_PROP);
      if (lPers > 0)
      {
         sprintf(acTmp, "%*u", SIZ_PERSPROP, lPers);
         memcpy(pOutbuf+OFF_PERSPROP, acTmp, SIZ_PERSPROP);
      }

      // Growing impr - Tree/vine
      long lGrow = atoin(pRec->Growing_Impr, RSIZ_GROWING_IMP);
      if (lGrow > 0)
      {
         sprintf(acTmp, "%*u", SIZ_GR_IMPR, lGrow);
         memcpy(pOutbuf+OFF_GR_IMPR, acTmp, SIZ_GR_IMPR);
      }

      // Fixture
      long lFixtr = atoin(pRec->Fixed_Impr, RSIZ_FIXED_IMPROV);
      if (lFixtr > 0)
      {
         sprintf(acTmp, "%*u", SIZ_FIXTR, lFixtr);
         memcpy(pOutbuf+OFF_FIXTR, acTmp, SIZ_FIXTR);
      }

      // Total other
      LONGLONG lOthers = lPers + lGrow + lFixtr;
      if (lOthers > 0)
      {
         sprintf(acTmp, "%*u", SIZ_OTHER, lOthers);
         memcpy(pOutbuf+OFF_OTHER, acTmp, SIZ_OTHER);
      }

      // Gross total
      LONGLONG lGross = lOthers+lLand+lImpr;
      if (lGross > 0)
      {
         sprintf(acTmp, "%*u", SIZ_GROSS, lGross);
         memcpy(pOutbuf+OFF_GROSS, acTmp, SIZ_GROSS);
#ifdef _DEBUG
         //if (!memcmp(pOutbuf, "906000025000", 12))
         //   lTmp = 0;
#endif
         // Ratio
         lTmp = lImpr+lPers;
         if (lTmp > 0)
         {
            __int64 lBig;

            lBig = (__int64)lTmp*100+50;
            sprintf(acTmp, "%*u", SIZ_RATIO, lBig/lGross);
            memcpy(pOutbuf+OFF_RATIO, acTmp, SIZ_RATIO);
         }
      }

      // HO Exempt
      if (pRec->asExeItems[0].Code == 'H')
         *(pOutbuf+OFF_HO_FL) = '1';      // 'Y'
      else
         *(pOutbuf+OFF_HO_FL) = '2';      // 'N'

      // Exe Code
      long lTotalExe=0;
      if (pRec->asExeItems[0].Code > ' ')
      {
         *(pOutbuf+OFF_EXE_CD1) = pRec->asExeItems[0].Code;
         *(pOutbuf+OFF_EXE_CD2) = pRec->asExeItems[1].Code;
         *(pOutbuf+OFF_EXE_CD3) = pRec->asExeItems[2].Code;
         lTotalExe = atoin(pRec->asExeItems[0].Amt, RSIZ_EXE_AMT);
         lTotalExe += atoin(pRec->asExeItems[1].Amt, RSIZ_EXE_AMT);
         lTotalExe += atoin(pRec->asExeItems[2].Amt, RSIZ_EXE_AMT);

         // Exemp amount
         if (lTotalExe > 0)
         {
            sprintf(acTmp, "%*u", SIZ_EXE_TOTAL, lTotalExe);
            memcpy(pOutbuf+OFF_EXE_TOTAL, acTmp, SIZ_EXE_TOTAL);
         }
      }
   }

   // TRA
   memcpy(pOutbuf+OFF_TRA, pRec->TRA, RSIZ_TRA);

   // Taxability
   iRet = atoin(pRec->Taxability, RSIZ_TAXABILITY);
   if (iRet > 0)
   {
      sprintf(acTmp, "%.*d", SIZ_TAX_CODE, iRet);
      memcpy(pOutbuf+OFF_TAX_CODE, acTmp, SIZ_TAX_CODE);

      if (iRet > 799 && iRet < 900)
         *(pOutbuf+OFF_PROP8_FLG) = 'Y';     // Set Prop8 flag
      else if (iRet == 3)   
         *(pOutbuf+OFF_FULL_EXEMPT) = 'Y';   // Set FullExempt
   }

   // Situs - Legal also contains situs
   Kin_MergeSAdr(pOutbuf, &pRec->Situs.S_StrNum[0]);

   // Mailing
   Kin_MergeMAdr(pOutbuf, &pRec->Mail.M_Addr1[0]);

   // Ag Preserved
   if (pRec->AgPreserve == 'Y')
      *(pOutbuf+OFF_AG_PRE) = 'Y';      // 'Y'

   // UseCode
   if (pRec->UseCode[0] > ' ')
   {
      memset(pOutbuf+OFF_USE_STD, ' ', SIZ_USE_STD);
      memcpy(pOutbuf+OFF_USE_CO, pRec->UseCode, RSIZ_USE_CODE);
      updateStdUse(pOutbuf+OFF_USE_STD, pRec->UseCode, RSIZ_USE_CODE, pOutbuf);
   } else
      memcpy(pOutbuf+OFF_USE_STD, USE_UNASGN, SIZ_USE_STD);

   // Owner
   Kin_MergeOwner(pOutbuf, pRollRec);

#ifdef _DEBUG
   //if (!memcmp(pRollRec, "002010053000", 9))
   //   iRet = 1;
#endif

   // Legal
   memcpy(acTmp, pRec->Desc_Line1, RSIZ_DESC_LINE_1*2);
   acTmp[RSIZ_DESC_LINE_1*2] = 0;
   if (acTmp[0] > ' ')
   {
      // Ignore situs description
      if (!isdigit(acTmp[0]) || strchr(acTmp, '.') || strchr(acTmp, '/') || strstr(acTmp, " SEC ") || strstr(acTmp, " TWP "))
      {
         iRet = updateLegal(pOutbuf, _strupr(acTmp));
         if (iRet > iMaxLegal)
            iMaxLegal = iRet;
      }
   }

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "050010001", 9))
   //   lTmp = 0;
#endif

   // Acreage: Lot sqft - Lot Acres
   lTmp = atoin(pRec->Acreage, RSIZ_ACRES);
   if (!memcmp(pOutbuf, "050010001", 9))              // Special case
      lTmp = 11888;

   if (lTmp > 0)
   {
      sprintf(acTmp, "%*u", SIZ_LOT_ACRES, lTmp*10);
      memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);

      dTmp = (((double)lTmp/100)*SQFT_PER_ACRE + 0.5);
      sprintf(acTmp, "%*u", SIZ_LOT_SQFT, (unsigned long)dTmp);
      memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);
   }

   // Property tax
   iRet = atoin(pRec->NumTaxes, RSIZ_TAX_ENTRIES);
   if (iRet > 0)
   {
      KIN_TAX *pTax = &pRec->asTaxItems[0];
      long     lTaxAmt=0;

      for (int iCnt=0; iCnt < iRet; iCnt++, pTax++)
      {
         lTaxAmt += atoin(pTax->Inst_Amt1, RSIZ_INST_AMT);
         lTaxAmt += atoin(pTax->Inst_Amt2, RSIZ_INST_AMT);
      }

      sprintf(acTmp, "%*u         ", SIZ_TAX_AMT, lTaxAmt);
      memcpy(pOutbuf+OFF_TAX_AMT, acTmp, SIZ_TAX_AMT);
   }

   // Transfer - YYYYMMDD
   // Only take date from recorded document with valid date
   if (memcmp(pRec->DeedDate, "0000", 4) > 0 && pRec->DeedNum[4] == 'R')
   {
      if (memcmp(pOutbuf+OFF_TRANSFER_DT, pRec->DeedDate, SIZ_TRANSFER_DT) < 0)
      {
         // Apply transfer
         memcpy(pOutbuf+OFF_TRANSFER_DT, pRec->DeedDate, SIZ_TRANSFER_DT);
         memcpy(pOutbuf+OFF_TRANSFER_DOC, (char *)&pRec->DeedNum[5], 7);
      }
   }

   // Delinquent year
   lTmp = atoin(pRec->Default_Date, 4);
   if (lTmp > 1900)
      memcpy(pOutbuf+OFF_DEL_YR, pRec->Default_Date, 4);

}

/********************************* Kin_ChkCond ********************************
 *
 * 04/11/2018: Include book 900 and up
 * Check special case.
 *
 ******************************************************************************/

int Kin_ChkCond(char *pRec, bool bLog=false)
{
   int   iRet;

   iRet = atoin(pRec+ROFF_APN, 3);
   // Ignore unsecured properties
   if ((iRet > 50 && iRet < 900) || (iRet >= 980) )
   {
      iRet = -2;
      if (bLog)
         LogMsg("Skip unsecured rec: %.25s", pRec);
   }
   // Ignore corrected records
   else if (*(pRec+ROFF_TAX_YEAR) > ' ' || *(pRec+ROFF_BOC) > ' ' )
   {
      iRet = -1;
      if (bLog)
         LogMsg("Skip BOC rec: %.25s", pRec);
   } else if (*(pRec+ROFF_RECORD_STATUS) == 'I' ||
              *(pRec+ROFF_RECORD_STATUS) == 'P' ||
              *(pRec+ROFF_RECORD_STATUS) == 'N')
   {
      iRet = -3;
      if (bLog)
         LogMsg("Skip (Status=%c) APN= %.25s", *(pRec+ROFF_RECORD_STATUS), pRec);
   } 
   else if (*(pRec+ROFF_RECORD_STATUS) != 'C' && *(pRec+ROFF_RECORD_STATUS) != ' ' && *(pRec+ROFF_RECORD_STATUS) != 'A')
      LogMsg("*** Unknown Status: %c", *(pRec+ROFF_RECORD_STATUS));
      
   return iRet;
}

/********************************* Kin_Load_Roll ******************************
 *
 * Notes:
 *    - There will be multiple records for the same parcel in the roll file.
 *      Ignore the one with taxyear < current year or record status is pending.
 *
 ******************************************************************************/

int Kin_Load_Roll(int iSkip)
{
   char     *pTmp, acRec[MAX_RECSIZE], acBuf[MAX_RECSIZE], acRollRec[MAX_RECSIZE];
   char     acRawFile[_MAX_PATH], acOutFile[_MAX_PATH];
   int      iRet, iTmp, iRollUpd=0, iNewRec=0, iRetiredRec=0;
   DWORD    nBytesRead, nBytesWritten;
   BOOL     bRet, bEof;
   long     lRet=0, lCnt=0, lBadRecs=0, lSkipRecs=0, lRead;

   HANDLE   fhIn, fhOut;

   LogMsg0("Load roll update for KIN");

   sprintf(acRawFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "S01");
   sprintf(acOutFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "R01");

   // Rename files for processing
   if (_access(acRawFile, 0))
   {
      if (!_access(acOutFile, 0))
         rename(acOutFile, acRawFile);
      else
      {
         LogMsg("***** Missing input file %s.  Please recheck!", acRawFile);
         return -1;
      }
   }

   // Open Roll file
   LogMsg("Open Roll file %s", acRollFile);
   fdRoll = fopen(acRollFile, "r");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", acRollFile);
      return -2;
   }

   // Open Char file
   if (!_access(acCharFile, 0))
   {
      LogMsg("Open Char file %s", acCharFile);
      fdChar = fopen(acCharFile, "r");
      if (fdChar == NULL)
      {
         LogMsg("***** Error opening Char file: %s\n", acCharFile);
         return -2;
      }
   }

   /*
   // Open Sales file
   if (!_access(acCSalFile, 0))
   {
      LogMsg("Open Sales file %s", acCSalFile);
      fdSale = fopen(acCSalFile, "r");
      if (fdSale == NULL)
      {
         LogMsg("***** Error opening Sales file: %s\n", acCSalFile);
         return -2;
      }
   }
   */

   // Open Input file
   LogMsg("Open input file %s", acRawFile);
   fhIn = CreateFile(acRawFile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhIn == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening last month input file: %s\n", acRawFile);
      return -3;
   }

   // Open Output file
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error creating output file: %s\n", acOutFile);
      return -4;
   }

   // Get first RollRec
   pTmp = fgets((char *)&acRollRec[0], MAX_RECSIZE, fdRoll);
   bEof = (pTmp ? false:true);
   lRead = 1;

   // Copy skip record
   memset(acBuf, ' ', iRecLen);
   while (iSkip-- > 0)
   {
      ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);
      // Ignore record start with 99999999
      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
   }

   iNoMatch=iBadCity=iBadSuffix=0;

   // Merge loop
   while (!bEof)
   {
      bRet = ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);

      // Check for EOF
      if (!bRet)
      {
         LogMsg("***** Error reading input file %s (%f)", acRawFile, GetLastError());
         break;
      }

      // EOF ?
      if (iRecLen != nBytesRead)
         break;

      NextRollRec:
      iTmp = memcmp(acBuf, acRollRec, iApnLen);
      if (!iTmp)
      {
         // Merge roll data
         Kin_MergeRoll(acBuf, acRollRec, iRollLen, UPDATE_R01);
         iRollUpd++;

         // Merge Char
         if (fdChar)
            iRet = Kin_MergeChar(acBuf);

         // Merge Sales
         //if (fdSale)
         //   iRet = Kin_MergeSale(acBuf, 2);

         // Get next roll record
         do 
         {
            pTmp = fgets(acRollRec, MAX_RECSIZE, fdRoll);
            lRead++;
            if (acRollRec[0] < '0')
            {
               lBadRecs++;
               continue;
            }
            if ((iRet = Kin_ChkCond(acRollRec)) < 0)
               lSkipRecs++;
            iTmp = memcmp(acBuf, acRollRec, iApnLen);
         } while (pTmp && (!iTmp || iRet < 0) );

         if (!pTmp)
            bEof = true;    // Signal to stop
      } else if (iTmp > 0)       // Roll not match, new roll record?
      {
         if (bDebug)
            LogMsg0("*** New roll record : %.*s (%d) ***", iApnLen, acRollRec, lCnt);

         // Create new R01 record
         Kin_MergeRoll(acRec, acRollRec, iRollLen, CREATE_R01);
         iNewRec++;

         // Merge Char
         if (fdChar)
            iRet = Kin_MergeChar(acRec);

         // Merge Sales
         //if (fdSale)
         //   iRet = Kin_MergeSale(acRec, 2);

         // Save last recording date
         iRet = atoin((char *)&acRec[OFF_TRANSFER_DT], 8);
         if (iRet > lLastRecDate && iRet < lToday)
            lLastRecDate = iRet;

         bRet = WriteFile(fhOut, acRec, iRecLen, &nBytesWritten, NULL);
         lCnt++;

         // Get next roll record
         do
         {
            pTmp = fgets(acRollRec, MAX_RECSIZE, fdRoll);
            lRead++;
            if (acRollRec[0] < '0')
            {
               lBadRecs++;
               continue;
            }
            if ((iRet = Kin_ChkCond(acRollRec)) < 0)
               lSkipRecs++;
            iTmp = memcmp(acRec, acRollRec, iApnLen);
         } while (pTmp && (!iTmp || iRet < 0) );

         if (!pTmp)
            bEof = true;    // Signal to stop
         else
            goto NextRollRec;
      } else
      {
         // Record may be retired
         if (bDebug)
            LogMsg0("*** Roll not match (retired record?) : R01->%.*s < Roll->%.*s (%d) ***", iApnLen, acBuf, iApnLen, acRollRec, lCnt);
         iRetiredRec++;
         continue;
      }

      // Save last recording date
      iRet = atoin((char *)&acBuf[OFF_TRANSFER_DT], 8);
      if (iRet > lLastRecDate && iRet < lToday)
         lLastRecDate = iRet;

      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);

      if (!bRet)
      {
         LogMsg("***** Error occurs: %d\n", GetLastError());
         lRet = WRITE_ERR;
         break;
      }
   }

   // Do the rest of the file
   while (!bEof)
   {
      if (bDebug)
         LogMsg0("*** New roll record : %.*s (%d) ***", iApnLen, acRollRec, lCnt);

      // Create new R01 record
      Kin_MergeRoll(acRec, acRollRec, iRollLen, CREATE_R01);
      iNewRec++;

      // Merge Char
      if (fdChar)
         iRet = Kin_MergeChar(acRec);

      // Merge Sales
      //if (fdSale)
      //   iRet = Kin_MergeSale(acRec, 2);

      // Save last recording date
      iRet = atoin((char *)&acRec[OFF_TRANSFER_DT], 8);
      if (iRet > lLastRecDate && iRet < lToday)
         lLastRecDate = iRet;

      bRet = WriteFile(fhOut, acRec, iRecLen, &nBytesWritten, NULL);
      lCnt++;

      // Get next roll record
      do 
      {
         pTmp = fgets(acRollRec, MAX_RECSIZE, fdRoll);
         lRead++;
         if (acRollRec[0] < '0')
         {
            lBadRecs++;
            continue;
         }

         if ((iRet = Kin_ChkCond(acRollRec)))
            lSkipRecs++;
         iTmp = memcmp(acRec, acRollRec, iApnLen);
      } while (pTmp && (!iTmp || iRet < 0) );

      if (!pTmp)
         bEof = true;    // Signal to stop
   }

   // Close files
   if (fdRoll)
      fclose(fdRoll);
   if (fdChar)
      fclose(fdChar);
   //if (fdSale)
   //   fclose(fdSale);

   if (fhOut)
      CloseHandle(fhOut);
   if (fhIn)
      CloseHandle(fhIn);

   printf("\n");
   LogMsg("Total records processed:    %u", lRead-1);
   LogMsg("Total records skiped:       %u", lSkipRecs);
   LogMsg("Total records output:       %u", lCnt);
   LogMsg("Total new records:          %u", iNewRec);
   LogMsg("Total bad input records:    %u", lBadRecs);
   LogMsg("Total retired records:      %u", iRetiredRec);
   LogMsg("Total updated records:      %u", iRollUpd);
   //LogMsg("Total Sale matched:         %u", lSaleMatch);
   LogMsg("Total Char matched:         %u", lCharMatch);
   LogMsg("Total bad-city records:     %u", iBadCity);
   LogMsg("Total bad-suffix records:   %u\n", iBadSuffix);
   LogMsg("Last recording date:        %u", lLastRecDate);

   lRecCnt = lCnt;
   return lRet;
}

/********************************* Kin_Load_LDR *****************************
 *
 * Ignore records that has Tax Year correction or Board Of Order correction code.
 *
 ****************************************************************************/

int Kin_Load_LDR(int iFirstRec /* 1=create header rec */)
{
   char     *pTmp, acBuf[MAX_RECSIZE], acRollRec[MAX_RECSIZE];
   char     acOutFile[_MAX_PATH];

   HANDLE   fhOut;
   DWORD    nBytesWritten;
   BOOL     bRet;
   long     lRet=0, lCnt, iRet;

   LogMsg0("Load LDR for KIN");

   sprintf(acOutFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "R01");

   // Open Roll file
   LogMsg("Open Roll file %s", acRollFile);
   fdRoll = fopen(acRollFile, "r");
   if (fdRoll == NULL)
   {
      printf("***** Error opening roll file: %s\n", acRollFile);
      return -1;
   }

   // Open Char file
   if (!_access(acCharFile, 0))
   {
      LogMsg("Open Char file %s", acCharFile);
      fdChar = fopen(acCharFile, "r");
      if (fdChar == NULL)
      {
         printf("***** Error opening Char file: %s\n", acCharFile);
         return -2;
      }
   }

   // Open Output file
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error creating output file: %s\n", acOutFile);
      return -4;
   }

   // Create header record
   if (iFirstRec)
   {
      memset(acBuf, '9', iRecLen);
      // Ignore record start with 99999999
      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
   }

   iNoMatch=iBadCity=iBadSuffix=0;

   // Get first RollRec
   pTmp = fgets((char *)&acRollRec[0], MAX_RECSIZE, fdRoll);
   memset(acBuf, ' ', iRecLen);
   lCnt = 1;

   // Merge loop
   while (pTmp)
   {
      // Create new R01 record
      Kin_MergeRoll(acBuf, acRollRec, iRollLen, CREATE_R01);

      // Merge Char
      if (fdChar)
         iRet = Kin_MergeChar(acBuf);

      // Save last recording date
      iRet = atoin((char *)&acBuf[OFF_TRANSFER_DT], 8);
      if (iRet > lLastRecDate && iRet < lToday)
         lLastRecDate = iRet;

      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
      if (!bRet)
      {
         LogMsg("***** Error occurs: %d\n", GetLastError());
         lRet = WRITE_ERR;
         break;
      }

      if (!(++lLDRRecCount % 1000))
         printf("\r%u", lLDRRecCount);

      // Get next roll record
      do 
      {
         pTmp = fgets(acRollRec, MAX_RECSIZE, fdRoll);
         lCnt++;
         if (acRollRec[0] < '0')
            continue;
         iRet = Kin_ChkCond(acRollRec);
         // Testing
         if (!memcmp(acRollRec, "070", 3))
            bRet = true;
      } while (pTmp && iRet < 0);
   }

   // Close files
   if (fdRoll)
      fclose(fdRoll);
   if (fdChar)
      fclose(fdChar);

   if (fhOut)
      CloseHandle(fhOut);

   LogMsg("Total input records:        %u", lCnt);
   LogMsg("Total output records:       %u", lLDRRecCount);
   LogMsg("Total Char matched:         %u", lCharMatch);
   LogMsg("Total Char skipped:         %u", lCharSkip);

   LogMsg("Total bad-city records:     %u", iBadCity);
   LogMsg("Total bad-suffix records:   %u\n", iBadSuffix);

   printf("\nTotal output records: %u\n", lCnt);

   lRecCnt = lLDRRecCount;
   return lRet;
}

/********************************* formatCSale *******************************
 *
 * Return -1 if bad record.  0 if OK.
 *
 *****************************************************************************/

int Kin_FormatCSale(char *pFmtSale, char *pSaleRec)
{

   KIN_SALE *pSale = (KIN_SALE *)pSaleRec;
   KIN_CSAL *pHSale= (KIN_CSAL *)pFmtSale;
   long     lTmp;
   char     acTmp[32];

   // Inititalize
   memset(pFmtSale, ' ', sizeof(KIN_CSAL));

   // APN
   memcpy(pHSale->Apn, pSale->Apn, SSIZ_APN);

   // Deed type
   pHSale->DocType = pSale->DocType;

   // DocNum
   memcpy(pHSale->DocNum, pSale->DocNum, SSIZ_DOC_NUM);

   // SaleDate
   if (pSale->DocDate[0] > '0')
      memcpy(pHSale->DocDate, pSale->DocDate, SSIZ_DOC_DATE);

   // Buyer
   memcpy(pHSale->Buyer, pSale->Transferee, SSIZ_TRANSFEREE);

   // Seller
   memcpy(pHSale->Seller, pSale->Transferor, SSIZ_TRANSFEREE);

#ifdef _DEBUG
   //if (!memcmp(pSale->Apn, "002120075", 9))
   //   lTmp = 0;
#endif
   // Sale price
   lTmp = atoin(pSale->SalePrice, SSIZ_SALE_PRICE);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*u", SSIZ_SALE_PRICE, lTmp);
      memcpy(pHSale->SalePrice, acTmp, SSIZ_SALE_PRICE);
   } else
   {
      lTmp = atoin(pSale->Doc_Tax, SSIZ_DOC_TAX);
      if (lTmp > 0)
      {
         double dTmp;

         // If DocTax is greater than 100000, this probably saleprice
         if (lTmp > 10000000)
            dTmp = lTmp / 100;
         else
            dTmp = (double)lTmp/0.11;
         sprintf(acTmp, "%*u", SSIZ_SALE_PRICE, (long)dTmp);
         memcpy(pHSale->SalePrice, acTmp, SSIZ_SALE_PRICE);
      }
   }

   // Situs address
   memcpy(pHSale->S_StrNum, pSale->S_StrNum, SSIZ_S_STRNUM);
   memcpy(pHSale->S_StrDir, pSale->S_StrDir, SSIZ_S_STRDIR);
   memcpy(pHSale->S_StrName, pSale->S_StrName, SSIZ_S_STRNAME);
   memcpy(pHSale->S_StrSfx, pSale->S_StrSfx, SSIZ_S_STRSFX);
   memcpy(pHSale->S_City, pSale->S_City, SSIZ_S_CITY);


   return 0;
}

/********************************* formatSCSale ******************************
 *
 * Return -1 if bad record.  0 if OK.
 *
 *****************************************************************************/

int Kin_FormatSCSale(char *pFmtSale, char *pSaleRec)
{
   KIN_SALE  *pSale  = (KIN_SALE *)pSaleRec;
   SCSAL_REC *pSCSale= (SCSAL_REC *)pFmtSale;
   long     lTmp;
   char     acTmp[32];

   if (pSale->Apn[0] < '0')
      return -1;

   // Inititalize
   memset(pFmtSale, ' ', sizeof(SCSAL_REC));

   // APN
   memcpy(pSCSale->Apn, pSale->Apn, iApnLen);

   // Group APN - It's hard to identify since every record has group APN
   if (memcmp(pSale->GroupApn, "0000000000", 10) && memcmp(pSale->GroupApn, pSale->Apn, iApnLen))
   {
      pSCSale->MultiSale_Flg = 'Y';
      memcpy(pSCSale->PrimaryApn, pSale->GroupApn, iApnLen);
   }

   // Deed type
   pSCSale->DocCode[0] = pSale->DocType;

   // DocNum
   memcpy(pSCSale->DocNum, pSale->DocNum, SSIZ_DOC_NUM);

   // SaleDate
   if (pSale->DocDate[0] > '0')
   {
      lTmp = atoin(pSale->DocDate, SSIZ_DOC_DATE);
      if (lTmp > lLastRecDate)
         lLastRecDate = lTmp;

      memcpy(pSCSale->DocDate, pSale->DocDate, SSIZ_DOC_DATE);
   }

   // Buyer
   memcpy(pSCSale->Name1, pSale->Transferee, SSIZ_TRANSFEREE);

   // Seller
   memcpy(pSCSale->Seller1, pSale->Transferor, SSIZ_TRANSFEREE);

#ifdef _DEBUG
   //if (!memcmp(pSale->Apn, "012114014", 9))
   //   lTmp = 0;
#endif
   // Sale price
   lTmp = atoin(pSale->SalePrice, SSIZ_SALE_PRICE);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*u", SALE_SIZ_SALEPRICE, lTmp);
      memcpy(pSCSale->SalePrice, acTmp, SALE_SIZ_SALEPRICE);
   } else
   {
      lTmp = atoin(pSale->Doc_Tax, SSIZ_DOC_TAX);
      if (lTmp > 0)
      {
         double dTmp;

         // If DocTax is greater than 100000, this probably saleprice
         if (lTmp > 10000000)
         {
            dTmp = lTmp / 100;
            LogMsg("??? Questionable doc tax amt %d (%.*s)", lTmp, iApnLen, pSale->Apn);
         } else
         {
            dTmp = (double)lTmp/0.11;
            sprintf(acTmp, "%*u", SALE_SIZ_SALEPRICE, lTmp);
            memcpy(pSCSale->StampAmt, acTmp, SALE_SIZ_SALEPRICE);
         }
         sprintf(acTmp, "%*u", SALE_SIZ_SALEPRICE, (long)dTmp);
         memcpy(pSCSale->SalePrice, acTmp, SALE_SIZ_SALEPRICE);
      }
   }

   // Situs address
   /*
   memcpy(pSCSale->S_StrNum, pSale->S_StrNum, SSIZ_S_STRNUM);
   memcpy(pSCSale->S_StrDir, pSale->S_StrDir, SSIZ_S_STRDIR);
   memcpy(pSCSale->S_StrName, pSale->S_StrName, SSIZ_S_STRNAME);
   memcpy(pSCSale->S_StrSfx, pSale->S_StrSfx, SSIZ_S_STRSFX);
   memcpy(pSCSale->S_City, pSale->S_City, SSIZ_S_CITY);
   */

   pSCSale->CRLF[0] = '\n';
   pSCSale->CRLF[1] = 0;

   return 0;
}

/*****************************************************************************
 *
 * Converting Fernando Cumulative sale format to Sony format.
 *
 *****************************************************************************/

void reformatDocNum(char *pSaleRec)
{
   char      acTmp[32];
   int       iTmp, iTmp1;
   KIN_CSAL *pHistSale = (KIN_CSAL *)pSaleRec;

   if (pHistSale->DocNum[0] == ' ')
      pHistSale->DocType = 'R';
   else
      pHistSale->DocType = 'I';

   iTmp = 0;
   while (iTmp < SSIZ_DOC_NUM && pHistSale->DocNum[iTmp] == ' ')
      iTmp++;

   iTmp1 = 0;
   while (iTmp < SSIZ_DOC_NUM)
      acTmp[iTmp1++] = pHistSale->DocNum[iTmp++];

   strcpy((char *)&acTmp[iTmp1], "           ");
   memcpy(pHistSale->DocNum, acTmp, SSIZ_DOC_NUM);
}

/****************************** Kin_UpdateSaleHist ***************************
 *
 * Update history sale.  This function adds new sale record to cumulative file.
 *
 * Return 0 if successful, otherwise error
 *
 *****************************************************************************

int Kin_UpdateSaleHist(void)
{
   KIN_SALE *pCurSale;
   KIN_CSAL *pHistSale;
   KIN_CSAL *pNewSale;

   char     acNewSale[512];
   char     *pTmp, acHSaleRec[512], acCSaleRec[1024], acSaleDate[32], acTmp[256];
   char     acOutFile[_MAX_PATH], acLastApn[16];

   HANDLE   fhIn, fhOut;
   FILE     *fdCSale;

   int      iRet, iTmp, iUpdateSale=0, iNewSale=0;
   DWORD    nBytesRead;
   DWORD    nBytesWritten;
   BOOL     bRet, bEof;
   long     lCnt=0, lTmp;

   // Check current sale file
   if (_access(acSaleFile, 0))
   {
      LogMsg("***** Missing current sale file %s", acSaleFile);
      return -1;
   }

   // Check cumulative sale file
   if (_access(acCSalFile, 0))
   {
      LogMsg("***** Missing cumulative file %s.", acCSalFile);
      return -1;
   }

   // Open current sale
   LogMsg("Open current sale file %s", acSaleFile);
   fdCSale = fopen(acSaleFile, "r");
   if (fdCSale == NULL)
   {
      LogMsg("***** Error opening current sale file: %s\n", acSaleFile);
      return 2;
   }

   // Open Input file - cumulative sale file
   LogMsg("Open cumulative sale file %s", acCSalFile);
   fhIn = CreateFile(acCSalFile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhIn == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error open current sale file: %s", acCSalFile);
      return 3;
   }

   // Create tmp output file
   strcpy(acOutFile, acCSalFile);
   if (pTmp = strrchr(acOutFile, '.'))
      strcpy(pTmp, ".tmp");
   else
      strcat(acOutFile, ".tmp");
   LogMsg("Create output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error Create output file: %s.  Please check disk space.", acOutFile);
      return 4;
   }

   // Read first history sale
   bRet = ReadFile(fhIn, acHSaleRec, iCSalLen, &nBytesRead, NULL);
   acHSaleRec[iCSalLen] = 0;
#ifdef _DEBUG_KIN
   reformatDocNum(acHSaleRec);
#endif

   pCurSale  = (KIN_SALE *)&acCSaleRec[0];
   pHistSale = (KIN_CSAL *)&acHSaleRec[0];
   pNewSale  = (KIN_CSAL *)&acNewSale[0];
   bEof = false;
   acSaleDate[0] = 0;
   acLastApn[iApnLen] = acLastApn[0] = 0;

   // Merge loop
   while (!bEof)
   {
      // Get current sale
      pTmp = fgets(acCSaleRec, 1024, fdCSale);
      if (!pTmp)
         break;
      if (pCurSale->DocType != 'R')
         continue;

      iTmp = memcmp(pCurSale->Apn, pHistSale->Apn, iApnLen);

      NextSaleRec:

#ifdef _DEBUG
      //if (!memcmp(pCurSale->Apn, "002010031", 9))
      //   iRet = 0;
#endif
      // Found matched APN ?
      if (!iTmp)
      {
         // Format cumsale record
         iRet = Kin_FormatCSale(acNewSale, acCSaleRec);
         iTmp = memcmp(pCurSale->DocDate, pHistSale->DocDate, SIZ_SALE1_DT);

         // If current sale is newer, add to out file
         if (iTmp > 0)
         {
            if (bDebug)
               LogMsg("** New sale: %.9s->%.8s", pCurSale->Apn, pCurSale->DocDate);

            // Output current sale record
            bRet = WriteFile(fhOut, acNewSale, iCSalLen, &nBytesWritten, NULL);
            iUpdateSale++;
         } else if (iTmp == 0 && !memcmp(pCurSale->DocNum, pHistSale->DocNum, SSIZ_DOC_NUM))
         {  // Update history sale record to correct Fernando mistake
            memcpy(pHistSale->SalePrice, pNewSale->SalePrice, SSIZ_SALE_PRICE);
            memcpy(pHistSale->Seller, pNewSale->Seller, SSIZ_TRANSFEROR);

            bRet = WriteFile(fhOut, acHSaleRec, iCSalLen, &nBytesWritten, NULL);
            bRet = ReadFile(fhIn, acHSaleRec, iCSalLen, &nBytesRead, NULL);
            if (!bRet || !nBytesRead)
               bEof = true;
         } else
         {
            if (bDebug)
               LogMsg0("*** Skip sale %.10s:%.17s", pCurSale->Apn, pCurSale->DocNum);
         }
      } else if (iTmp > 0)
      {
         // Write out all history sale record up to current APN
         while (iTmp > 0)
         {
            // Blank out sale price in history if it is smaller than 5000
            // since it might be invalid
            lTmp = atoin(pHistSale->SalePrice, SSIZ_SALE_PRICE);
            if (lTmp > 0 && lTmp < 5000)
               memset(pHistSale->SalePrice, ' ', SSIZ_SALE_PRICE);
            bRet = WriteFile(fhOut, acHSaleRec, iCSalLen, &nBytesWritten, NULL);
            bRet = ReadFile(fhIn, acHSaleRec, iCSalLen, &nBytesRead, NULL);
            if (!bRet || !nBytesRead)
            {
               bEof = true;
               break;
            }
#ifdef _DEBUG_KIN
            reformatDocNum(acHSaleRec);
#endif
            iTmp = memcmp(pCurSale->Apn, pHistSale->Apn, iApnLen);
         }

         if (!bEof)
            goto NextSaleRec;
      } else // CurApn < HistApn
      {
         // Format cumsale record
         iRet = Kin_FormatCSale(acNewSale, acCSaleRec);

         if (!iRet)
         {
            if (bDebug)
               LogMsg("** First sale: %.9s->%.8s", pCurSale->Apn, pCurSale->DocDate);

            // New sale record for this APN
            bRet = WriteFile(fhOut, acNewSale, iCSalLen, &nBytesWritten, NULL);
            iNewSale++;
         }
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);

      if (!bRet)
      {
         LogMsg("***** Error occurs: %d\n", GetLastError());
         break;
      }
   }

   // Check what file has broken out of the loop
   if (bEof)
   {
      // Do the rest of current sale file
      do
      {
         iRet = Kin_FormatCSale(acNewSale, acCSaleRec);
         if (!iRet)
         {
            // New sale record for this APN
            bRet = WriteFile(fhOut, acNewSale, iCSalLen, &nBytesWritten, NULL);
            iNewSale++;
         }
         pTmp = fgets(acCSaleRec, 1024, fdCSale);
      } while (pTmp);
   } else
   {
      // Do the rest of the cum sale file
      while (nBytesRead > 0)
      {
         bRet = WriteFile(fhOut, acHSaleRec, iCSalLen, &nBytesWritten, NULL);
         bRet = ReadFile(fhIn, acHSaleRec, iCSalLen, &nBytesRead, NULL);

         if (!bRet)
            break;

#ifdef _DEBUG_KIN
         reformatDocNum(acHSaleRec);
#endif

         if (!(++lCnt % 1000))
            printf("\r%u", lCnt);
      }
   }

   // Close files
   if (fdCSale)
      fclose(fdCSale);
   if (fhOut)
      CloseHandle(fhOut);
   if (fhIn)
      CloseHandle(fhIn);

   // Rename output file
   strcpy(acTmp, acCSalFile);
   if (pTmp = strrchr(acTmp, '.'))
   {
      strcpy(pTmp, ".sav");
      if (!_access(acTmp, 0))
         remove(acTmp);
   }

   iRet = rename(acCSalFile, acTmp);
   if (iRet)
   {
      LogMsg("***** Error renaming %s to %s: %d", acCSalFile, acTmp, errno);
   } else
   {
      // Sort output file - APN (asc), Saledate (des), DocNum (des)
      // S(1,12,C,A,23,8,C,D,14,9,C,D) DUPO(1,30) F(FIX,%d)
      sprintf(acTmp, "S(%d,%d,C,A,%d,8,C,A,%d,%d,C,A) F(FIX,%d) DUPO(1,30) ", OFF_CSAL_APN,
         SSIZ_APN, OFF_CSAL_RECDATE, OFF_CSAL_DOCNUM, SSIZ_DOC_NUM, iCSalLen);
      iTmp = sortFile(acOutFile, acCSalFile, acTmp);
   }

   LogMsg("Total sale records processed:   %u", lCnt);
   LogMsg("Total cumulative sale records:  %u", iTmp);
   LogMsg("Total new sale records:         %u", iNewSale);
   LogMsg("Total sale records updated:     %u", iUpdateSale);

   LogMsg("Update Sale History completed.");

   return 0;
}

/****************************** Kin_ExtrSale *********************************
 *
 * Extract sale and format to standard cum sale format SCSAL_REC.
 *
 * Return 0 if successful, otherwise error
 *
 *****************************************************************************/

int Kin_ExtrSale(char *pSaleFile)
{
   KIN_SALE    *pCurSale;
   SCSAL_REC   *pNewSale;

   char     *pTmp, acSaleRec[512], acTmp[256], acOutFile[_MAX_PATH], acNewSale[1024];
   FILE     *fdOut;

   int      iRet, iNewSale=0, lCnt=0;
   BOOL     bEof;

   LogMsg0("Extract sale file %s", pSaleFile);

   // Check current sale file
   if (_access(pSaleFile, 0))
   {
      LogMsg("***** Missing current sale file %s", pSaleFile);
      return -1;
   }

   // Open current sale
   LogMsg("Open current sale file %s", pSaleFile);
   fdSale = fopen(pSaleFile, "r");
   if (fdSale == NULL)
   {
      LogMsg("***** Error opening current sale file: %s\n", pSaleFile);
      return -2;
   }

   // Create output file
   sprintf(acOutFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "DAT");
   LogMsg("Create updated sale file %s", acOutFile);
   fdOut = fopen(acOutFile, "w");
   if (fdOut == NULL)
   {
      LogMsg("***** Error create updated sale file: %s", acOutFile);
      return -3;
   }

   pCurSale  = (KIN_SALE *)&acSaleRec[0];
   pNewSale  = (SCSAL_REC *)&acNewSale[0];
   bEof = false;
   lLastRecDate = 0;

   // Merge loop
   while (!bEof)
   {
      // Get current sale
      pTmp = fgets(acSaleRec, 1024, fdSale);
      if (!pTmp)
         break;

      // Drop all except type 'R'
      //if (pCurSale->DocType != 'R')
      //   continue;

#ifdef _DEBUG
      //if (!memcmp(pCurSale->Apn, "002010031", 9))
      //   iRet = 0;
#endif

      if (pCurSale->DocType == 'R')
      {
         iRet = Kin_FormatSCSale(acNewSale, acSaleRec);
         if (!iRet)
         {
            fputs(acNewSale, fdOut);
            iNewSale++;
         }
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdSale)
      fclose(fdSale);
   if (fdOut)
      fclose(fdOut);

   LogMsg("Total sale records processed: %u", lCnt);
   LogMsg("   Total sale records output: %u", iNewSale);
   LogMsg("       Most recent sale date: %u\n", lLastRecDate);

   LogMsg("Merge sale output with cum sale file");

   // Sort on APN, SALEDATE, SALEPRICE, DOCNUM, DOCTAX since sale price can be in any of the doc.
   sprintf(acTmp, "S(1,10,C,A,27,8,C,A,57,10,C,D,15,12,C,A,117,10,C,D) DUPO(1,34)");
   sprintf(acCSalFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "sls");
   if (_access(acCSalFile, 0))
   {
      iRet = sortFile(acOutFile, acCSalFile, acTmp);
   } else
   {  // Append to history sale
      strcpy(acSaleRec, acCSalFile);
      strcat(acSaleRec, "+");
      strcat(acSaleRec, acOutFile);
      sprintf(acNewSale, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "Srt");

      // Sort sale file
      iRet = sortFile(acSaleRec, acNewSale, acTmp);
      if (iRet >= iNewSale)
      {
         sprintf(acOutFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "sav");
         if (!_access(acOutFile, 0))
            DeleteFile(acOutFile);

         // Rename old cum sale file to .sav
         rename(acCSalFile, acOutFile);
         // Rename .srt file to .sls
         rename(acNewSale, acCSalFile);
      }
   }

   LogMsg("Total cumulative sale records:  %u", iRet);
   LogMsg("Update Sale History completed.");
   if (iRet > 0)
      iRet = 0;

   return iRet;
}

/********************************** CreateKinRec *****************************
 *
 * Return 0 if successful, >0 if error
 *
 *****************************************************************************/

int Kin_CreateLienRec(char *pOutbuf, char *pRollRec)
{
   char     acTmp[256];
   LONGLONG lTmp;
   int      iTmp;
   LIENEXTR *pLienRec = (LIENEXTR *)pOutbuf;
   KIN_ROLL *pRec = (KIN_ROLL *)pRollRec;

   // Clear output buffer
   memset(pOutbuf, ' ', sizeof(LIENEXTR));

   // Start copying data
   memcpy(pLienRec->acApn, pRec->Apn, RSIZ_APN);
   memcpy(pLienRec->acTRA, pRec->TRA, RSIZ_TRA);

   // Year assessed
   memcpy(pLienRec->acYear, myCounty.acYearAssd, 4);

   // Land
   long lLand = atoin(pRec->Land, RSIZ_LAND);
   if (lLand > 0)
   {
      iTmp = sprintf(acTmp, "%*u", sizeof(pLienRec->acLand), lLand);
      memcpy(pLienRec->acLand, acTmp, iTmp);
   }

   // Improve 
   long lImpr = atoin(pRec->Struct_Impr, RSIZ_STRUCTURE_IMP);
   if (lImpr > 0)
   {
      iTmp = sprintf(acTmp, "%*u", sizeof(pLienRec->acImpr), lImpr);
      memcpy(pLienRec->acImpr, acTmp, iTmp);

      __int64 lBig;
      lBig = (__int64)lImpr*100;
      iTmp = sprintf(acTmp, "%*u", sizeof(pLienRec->acRatio), lBig/(lLand+lImpr));
      memcpy(pLienRec->acRatio, acTmp, iTmp);
   }

   // Fixed Equipment
   long lFixt = atoin(pRec->Fixed_Impr, RSIZ_FIXED_IMPROV);
   if (lFixt > 0)
   {
      iTmp = sprintf(acTmp, "%*u", SIZ_LIEN_FIXT, lFixt);
      memcpy(pLienRec->acME_Val, acTmp, iTmp);
   }

   // Pers Prop
   long lPers = atoin(pRec->PP_Val, RSIZ_PERS_PROP);
   if (lPers > 0)
   {
      iTmp = sprintf(acTmp, "%*u", SIZ_LIEN_PERS, lPers);
      memcpy(pLienRec->acPP_Val, acTmp, iTmp);
   }

   // Growing impr
   long lGrow = atoin(pRec->Growing_Impr, RSIZ_GROWING_IMP);
   if (lGrow > 0)
   {
      iTmp = sprintf(acTmp, "%*u", SIZ_LIEN_FIXT, lGrow);
      memcpy(pLienRec->extra.Kin.GrowImpr, acTmp, iTmp);
   }

   // Total other
   LONGLONG lOthers = lPers + lGrow + lFixt;
   if (lOthers > 0)
   {
      iTmp = sprintf(acTmp, "%*u", sizeof(pLienRec->acOther), lOthers);
      memcpy(pLienRec->acOther, acTmp, iTmp);
   }

   // Gross
   lTmp = lOthers + lLand + lImpr;
   if (lTmp > 0)
   {
      iTmp = sprintf(acTmp, "%*u", sizeof(pLienRec->acGross), lTmp);
      memcpy(pLienRec->acGross, acTmp, iTmp);
   }

   // HO Exempt
   if (pRec->asExeItems[0].Code == 'H')
   {
      pLienRec->acHO[0] = '1';      // 'Y'
   } else
      pLienRec->acHO[0] = '2';      // 'N'

   // Exe Code
   LONGLONG lTotalExe=0;
   pLienRec->extra.Kin.Exe_Code1[0] = pRec->asExeItems[0].Code;
   pLienRec->extra.Kin.Exe_Code2[0] = pRec->asExeItems[1].Code;
   pLienRec->extra.Kin.Exe_Code3[0] = pRec->asExeItems[2].Code;
   lTmp = atoin(pRec->asExeItems[0].Amt, RSIZ_EXE_AMT);
   if (lTmp > 0)
   {
      iTmp = sprintf(acTmp, "%d", lTmp);
      memcpy(pLienRec->extra.Kin.Exe_Amt1, acTmp, iTmp);
      lTotalExe = lTmp;
   }
   lTmp = atoin(pRec->asExeItems[1].Amt, RSIZ_EXE_AMT);
   if (lTmp > 0)
   {
      iTmp = sprintf(acTmp, "%d", lTmp);
      memcpy(pLienRec->extra.Kin.Exe_Amt2, acTmp, iTmp);
      lTotalExe += lTmp;
   }
   lTmp = atoin(pRec->asExeItems[2].Amt, RSIZ_EXE_AMT);
   if (lTmp > 0)
   {
      iTmp = sprintf(acTmp, "%d", lTmp);
      memcpy(pLienRec->extra.Kin.Exe_Amt3, acTmp, iTmp);
      lTotalExe += lTmp;
   }

   if (lTotalExe > 0)
   {
      iTmp = sprintf(acTmp, "%d", lTotalExe);
      memcpy(pLienRec->acExAmt, acTmp, iTmp);
   }

#ifdef _DEBUG
   //memcpy(pLienRec->filler, "1234567890", 10);
   //pLienRec->acTaxCode[0] = 'T';
#endif

   pLienRec->LF[0] = '\n';
   pLienRec->LF[1] = 0;
   
   return 0;
}

/********************************* Kin_ExtrLien *****************************
 *
 *
 ****************************************************************************/

int Kin_ExtrLien()
{
   char     *pTmp, acBuf[MAX_RECSIZE], acRollRec[MAX_RECSIZE];
   char     acOutFile[_MAX_PATH];
   int      iRet;
   long     lCnt=0;

   FILE     *fdLien;

   LogMsg0("Extract lien for KIN");

   // Open roll file
   LogMsg("Open Roll file %s", acRollFile);
   fdRoll = fopen(acRollFile, "r");
   if (fdRoll == NULL)
   {
      printf("Error opening roll file: %s\n", acRollFile);
      return 2;
   }

   // Open Output file
   sprintf(acOutFile, acLienTmpl, myCounty.acCntyCode, myCounty.acCntyCode);
   LogMsg("Open output file %s", acOutFile);
   fdLien = fopen(acOutFile, "w");
   if (fdLien == NULL)
   {
      LogMsg("***** Error opening Lien extract file: %s\n", acOutFile);
      return 4;
   }

   // Extract loop
   while (!feof(fdRoll))
   {
      // Get roll rec
      do {
         pTmp = fgets(acRollRec, MAX_RECSIZE, fdRoll);
      } while (pTmp && (Kin_ChkCond(acRollRec) < 0));
      if (!pTmp)
         break;

      // Create new lien record
      iRet = Kin_CreateLienRec(acBuf, acRollRec);

      fputs(acBuf, fdLien);
      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdRoll)
      fclose(fdRoll);

   if (fdLien)
      fclose(fdLien);

   LogMsg("Total output records:       %u", lCnt);
   printf("\nTotal output records: %u", lCnt);

   lRecCnt = lCnt;
   return 0;
}

/******************************* ConvertSaleData() *********************************
 *
 * Convert sale file from KIN_CSAL format to SCSAL_REC (Kin_Sale.sls)
 *
 * Return 0 if success, otherwise error
 *
 ***********************************************************************************/

static int Kin_convertSaleType(char *pInbuf, char *pOutbuf)
{
   KIN_CSAL  *pSale  = (KIN_CSAL *)pInbuf;
   SCSAL_REC *pCSale = (SCSAL_REC *)pOutbuf;
   int       iTmp;
   char      sTmp[32];
   
   memset(pOutbuf, ' ', sizeof(SCSAL_REC));
   memcpy(pCSale->Apn, pSale->Apn, SSIZ_APN);
   memcpy(pCSale->DocNum, pSale->DocNum, SSIZ_DOC_NUM);
   memcpy(pCSale->DocDate, pSale->DocDate, SSIZ_DOC_DATE);
   pCSale->DocCode[0] = pSale->DocType;

   iTmp = atoin(pSale->SalePrice, SSIZ_SALE_PRICE);
   if (iTmp > 0)
   {
      sprintf(sTmp, "%*u", SALE_SIZ_SALEPRICE, iTmp);
      memcpy(pCSale->SalePrice, sTmp, SALE_SIZ_SALEPRICE);
   }

   memcpy(pCSale->Seller1, pSale->Seller, SSIZ_TRANSFEROR);
   memcpy(pCSale->Name1, pSale->Buyer, SSIZ_TRANSFEREE);

   pCSale->CRLF[0] = '\n';
   pCSale->CRLF[1] = 0;

   return 0;
}

// This function is used to convert old sale format to SCSAL_REC format
// Remove this after all sales are converted
int Kin_convertSaleData(char *pCnty)
{
   char     acInbuf[1024], acOutbuf[1024];
   char     acOutFile[_MAX_PATH], acSrtFile[_MAX_PATH];
   long     lCnt=0, lOut=0, iRet;
   FILE     *fdOut;

	HANDLE	fhCSale;

   DWORD    nBytesRead;
   BOOL     bRet, bEof;

   if (_access(acCSalFile, 0))
   {
      LogMsg("***** ConvertSaleData(): Missing input file: %s", acCSalFile);
      return -1;
   }

   sprintf(acOutFile, acESalTmpl, pCnty, pCnty, "Tmp");
   LogMsg("Convert %s to %s.", acCSalFile, acOutFile);

   // Open input file
   LogMsg("Open input sale file %s", acCSalFile);

   fhCSale = CreateFile(acCSalFile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
										  FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);
   if (fhCSale == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error open current sale file: %s", acCSalFile);
      return -2;
   }

   // Open output file
   LogMsg("Create output sale file %s", acOutFile);
   fdOut = fopen(acOutFile, "w");
   if (fdOut == NULL)
   {
      LogMsg("***** Error creating output sale file: %s\n", acOutFile);
      return -3;
   }

   bRet = ReadFile(fhCSale, acInbuf, iCSalLen, &nBytesRead, NULL);
   bEof = false;

   // Convert loop
   while (!bEof)
   {
		acInbuf[iCSalLen] = 0;
      iRet = Kin_convertSaleType(acInbuf, acOutbuf);

      // Write to output file
      if (!iRet)
      {
         fputs(acOutbuf, fdOut);
         lOut++;
      }

      bRet = ReadFile(fhCSale, acInbuf, iCSalLen, &nBytesRead, NULL);
      if (!bRet || !nBytesRead)
         bEof = true;

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fhCSale)
      CloseHandle(fhCSale);
   if (fdOut)
      fclose(fdOut);

   // Sort sale file - APN (asc), Saledate (asc), DocNum (asc), Sale price (desc)
   sprintf(acInbuf, "S(1,12,C,A,27,8,C,A,57,10,C,D,15,12,C,A) DUPO(1,34)");
   sprintf(acSrtFile, acESalTmpl, pCnty, pCnty, "sls");
   iRet = sortFile(acOutFile, acSrtFile, acInbuf);
   if (!iRet)
      iRet = 1;
   else
      iRet = 0;

   LogMsg("Total input records:        %u", lCnt);
   LogMsg("Total output records:       %u", lOut);

   printf("\nTotal output records: %u\n", lCnt);
   return iRet;
}

/****************************** Kin_CreatePublR01 ****************************
 *
 * Return 0 if successful, >0 if error
 *
 *****************************************************************************/

int Kin_CreatePublR01(char *pOutbuf, char *pRollRec)
{
   char     acTmp[256], acTmp1[256];
   int      iRet, iCnt;
   long     lTmp;
   KIN_PUB  *pInrec;

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "0101100240", 9))
   //   iRet = 0;
#endif
   // Clear output buffer
   memset(pOutbuf, ' ', iRecLen);
   pInrec = (KIN_PUB *)pRollRec;

   // Start copying data
   memcpy(pOutbuf, pInrec->Apn, iApnLen);

   // Format APN
   memcpy(acTmp1, pInrec->Apn, iApnLen);
   acTmp1[iApnLen] = 0;
   iRet = formatApn(acTmp1, acTmp, &myCounty);
   memcpy(pOutbuf+OFF_APN_D, acTmp, iRet);

   // Create MapLink and output new record
   iRet = formatMapLink(acTmp1, acTmp, &myCounty);
   memcpy(pOutbuf+OFF_MAPLINK, acTmp, iRet);

   // Create index map link
   if (getIndexPage(acTmp, acTmp1, &myCounty))
      memcpy(pOutbuf+OFF_IMAPLINK, acTmp1, iRet);

   memcpy(pOutbuf+OFF_CO_NUM, "16KIN", 5);

   // Set public parcel flag
   *(pOutbuf+OFF_PUBL_FLG) = 'Y';

   // Year assessed
   //memcpy(pOutbuf+OFF_YR_ASSD, pInrec->TaxYear, 4);
   memcpy(pOutbuf+OFF_YR_ASSD, myCounty.acYearAssd, 4);

   // Owner
   memcpy(pOutbuf+OFF_NAME_SWAP, pInrec->Owner, PSIZ_OWNER);
   memcpy(pOutbuf+OFF_NAME1, pInrec->Owner, PSIZ_OWNER);

   // TRA
   lTmp = atoin(pInrec->Tra, PSIZ_TRA);
   if (lTmp > 0)
   {
      iRet = sprintf(acTmp, "%.6d", lTmp);
      memcpy(pOutbuf+OFF_TRA, acTmp, iRet);
   }

   // Taxability
   if (pInrec->Taxability[0] > ' ')
   {
      if (memcmp(pInrec->Taxability, "000", 3))
         memcpy(pOutbuf+OFF_TAX_CODE, pInrec->Taxability, PSIZ_TAXABILITY);
   }

   // LandUse
   if (pInrec->UseCode[0] > ' ')
   {
      memset(pOutbuf+OFF_USE_STD, ' ', SIZ_USE_STD);
      memcpy(pOutbuf+OFF_USE_CO, pInrec->UseCode, PSIZ_LANDUSE);
      updateStdUse(pOutbuf+OFF_USE_STD, pInrec->UseCode, PSIZ_LANDUSE, pOutbuf);
   } else
      memcpy(pOutbuf+OFF_USE_STD, USE_UNASGN, SIZ_USE_STD);

   // Legal
   if (pInrec->Section[0] > ' ')
   {
      sprintf(acTmp, "SECTION %.2s TOWNSHIP %.2s RANGE %.2s", pInrec->Section, pInrec->Township, pInrec->Range);
      iCnt = updateLegal(pOutbuf, acTmp);
      if (iCnt > iMaxLegal)
         iMaxLegal = iCnt;
   }

   // Zoning
   memcpy(pOutbuf+OFF_ZONE, pInrec->Zoning, PSIZ_ZONING);
   if (*(pOutbuf+OFF_ZONE_X1) == ' ')
      vmemcpy(pOutbuf+OFF_ZONE_X1, pInrec->Zoning, SIZ_ZONE_X1, PSIZ_ZONING);

   // Beds, Baths, Rooms
   iCnt = atoin(pInrec->Beds, PSIZ_BEDS);
   if (iCnt > 0)
   {
      sprintf(acTmp, "%*u", SIZ_BEDS, iCnt);
      memcpy(pOutbuf+OFF_BEDS, acTmp, SIZ_BEDS);
   }

   // Baths 99V9
   iCnt = atoin(pInrec->Baths, PSIZ_BATHS-1);
   if (iCnt > 0)
   {
      // Make .75 as full bath
      if (pInrec->Baths[2] > '5')
      {
         iCnt++;
         pInrec->Baths[2] = '0';
      }
      sprintf(acTmp, "%*u", SIZ_BATH_F, iCnt);
      memcpy(pOutbuf+OFF_BATH_F, acTmp, SIZ_BATH_F);

      if (pInrec->Baths[2] > '0')
         *(pOutbuf+OFF_BATH_H) = '1';
   }

   // Rooms
   iCnt = atoin(pInrec->Rooms, PSIZ_ROOMS);
   if (iCnt > 0)
   {
      sprintf(acTmp, "%*u", SIZ_ROOMS, iCnt);
      memcpy(pOutbuf+OFF_ROOMS, acTmp, SIZ_ROOMS);
   }

   // Pool - P/S/B
   if (pInrec->Pool[0] == 'B')
      *(pOutbuf+OFF_POOL) = 'C';
   else if (pInrec->Pool[0] == 'P')
      *(pOutbuf+OFF_POOL) = 'P';
   else if (pInrec->Pool[0] == 'S')
      *(pOutbuf+OFF_POOL) = 'S';
   else
      *(pOutbuf+OFF_POOL) = ' ';

   // Yrblt
   iCnt = atoin(pInrec->YrBlt, PSIZ_YRBLT);
   if (iCnt > 0)
   {
      iRet = sprintf(acTmp, "%d", iCnt);
      memcpy(pOutbuf+OFF_YR_BLT, acTmp, iRet);
   }

   // Units
   iCnt = atoin(pInrec->Units, PSIZ_UNITS);
   if (iCnt > 0)
   {
      iRet = sprintf(acTmp, "%d", iCnt);
      memcpy(pOutbuf+OFF_UNITS, acTmp, iRet);
   }

   // Stories 99V9
   iCnt = atoin(pInrec->Stories, CSIZ_STORIES);
   if (iCnt > 9)
   {
      sprintf(acTmp, "%.1f  ", (float)(iCnt/10));
      memcpy(pOutbuf+OFF_STORIES, acTmp, SIZ_STORIES);
   }

   long lSqFtLand=0;
   double dTmp = atof(pInrec->Acres);

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "050010001", 9))
   //   lTmp = 0;
#endif
   // LotSize
   lTmp = atoin(pInrec->Acres, PSIZ_ACRES);
   if (!memcmp(pOutbuf, "050010001", 9))              // Special case
      lTmp = 11888;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*u", SIZ_LOT_ACRES, lTmp*10);
      memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);

      dTmp = (((double)lTmp/100)*SQFT_PER_ACRE + 0.5);
      sprintf(acTmp, "%*u", SIZ_LOT_SQFT, (unsigned long)dTmp);
      memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);
   } else
   {
      lTmp = atoin(pInrec->LotSize, PSIZ_LOTSIZE);
      if (lTmp > 0)
      {
         sprintf(acTmp, "%*u", SIZ_LOT_SQFT, lTmp);
         memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);

         dTmp = ((double)lTmp*SQFT_MF_1 + 0.005);
         sprintf(acTmp, "%*u", SIZ_LOT_ACRES, (unsigned long)dTmp);
         memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);
      }
   }

   // BldgSqft
   lTmp = atoin(pInrec->BldgSqft, PSIZ_BLDGSQFT);
   if (lTmp > 10)
   {
      sprintf(acTmp, "%*u", SIZ_BLDG_SF, lTmp);
      memcpy(pOutbuf+OFF_BLDG_SF, acTmp, SIZ_BLDG_SF);
   }

   // Situs
   Kin_MergeSAdr(pOutbuf, &pInrec->Situs.S_StrNum[0]);

   // Mailing
   Kin_MergeMAdr(pOutbuf, &pInrec->Mail.M_Addr1[0]);

   return 0;
}

/********************************* Kin_MergePubl ****************************
 *
 * Merge supplemental parcel file to roll file
 * Use LotSize when LotAcres is empty.
 *
 ****************************************************************************/

int Kin_MergePubl(int iSkip)
{
   char     acRec[MAX_RECSIZE], acBuf[MAX_RECSIZE], acRollRec[1024];
   char     acRawFile[_MAX_PATH], acOutFile[_MAX_PATH], *pRoll;

   HANDLE   fhIn, fhOut;
   KIN_PUB  *pInrec;

   int      iRet, iTmp, iNewRec=0;
   DWORD    nBytesRead;
   DWORD    nBytesWritten;
   BOOL     bRet;
   long     lRet=0, lCnt=0;

   sprintf(acRawFile, acRawTmpl, "Kin", "Kin", "Tmp");
   sprintf(acOutFile, acRawTmpl, "Kin", "Kin", "R01");

   // Remove tmp file
   if (!_access(acRawFile, 0))
      remove(acRawFile);

   // Make sure R01 file is avail.
   if (_access(acOutFile, 0))
   {
      LogMsg("***** Missing input file %s.  Please recheck!", acOutFile);
      return 1;
   }

   // Rename current R01 to TMP file
   rename(acOutFile, acRawFile);

   // Open unassessed file
   LogMsg("Open unassessed file %s", acPubParcelFile);
   fdRoll = fopen(acPubParcelFile, "r");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening unassessed file: %s\n", acPubParcelFile);
      return 2;
   }

   // Open Input file
   LogMsg("Open input file %s", acRawFile);
   fhIn = CreateFile(acRawFile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhIn == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening input file: %s\n", acRawFile);
      return 3;
   }

   // Open Output file
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, FILE_SHARE_READ, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening output file: %s\n", acOutFile);
      return 4;
   }

   // Copy skip record
   memset(acBuf, ' ', iRecLen);
   while (iSkip-- > 0)
   {
      ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);
      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
   }

   iNoMatch=iBadCity=iBadSuffix=0;

   // Get first rec
   pRoll = fgets(acRollRec, 1024, fdRoll);
   pInrec = (KIN_PUB *)&acRollRec[0];

   // Merge loop
   while (pRoll)
   {
      bRet = ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);
      if (!bRet)
      {
         LogMsg("***** Error reading input file %s (%f)", acRawFile, GetLastError());
         break;
      }

      if (iRecLen != nBytesRead)
         break;

      NextRollRec:
#ifdef _DEBUG
      //if (!memcmp(acRollRec, "014760037", 9))
      //   iTmp = 0;
#endif

      iTmp = memcmp(acBuf, acRollRec, iApnLen);
      if (iTmp <= 0)
      {
         // Update Zoning and Usecode if available
         if (acBuf[OFF_ZONE] == ' ' && pInrec->Zoning[0] > ' ')
            memcpy(&acBuf[OFF_ZONE], pInrec->Zoning, PSIZ_ZONING);
         if (acBuf[OFF_ZONE_X1] == ' ' && pInrec->Zoning[0] > ' ')
            memcpy(&acBuf[OFF_ZONE_X1], pInrec->Zoning, PSIZ_ZONING);

         if (acBuf[OFF_USE_CO] == ' ')
         {
            if (pInrec->UseCode[0] > ' ')
            {
               memcpy(&acBuf[OFF_USE_CO], pInrec->UseCode, PSIZ_ZONING);
               updateStdUse(&acBuf[OFF_USE_STD], pInrec->UseCode, PSIZ_LANDUSE, acBuf);
            } else 
               memcpy(&acBuf[OFF_USE_STD], USE_UNASGN, SIZ_USE_STD);         
         }

         // Taxability
         if (pInrec->Taxability[0] > ' ')
         {
            if (memcmp(pInrec->Taxability, "000", 3))
               memcpy(&acBuf[OFF_TAX_CODE], pInrec->Taxability, PSIZ_TAXABILITY);
         }

         // Write existing rec out
         bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
         lCnt++;

         if (!iTmp)
            pRoll = fgets(acRollRec, 1024, fdRoll);
      } else 
      {
         // Create new R01 record from unassessed record
         iRet = Kin_CreatePublR01(acRec, acRollRec);
         iNewRec++;

         bRet = WriteFile(fhOut, acRec, iRecLen, &nBytesWritten, NULL);
         if (!(++lCnt % 1000))
            printf("\r%u", lCnt);

         pRoll = fgets(acRollRec, 1024, fdRoll);
         if (!pRoll)
            break;
         else
            goto NextRollRec;
      }

      if (!(lCnt % 1000))
         printf("\r%u", lCnt);

      if (!bRet)
      {
         LogMsg("***** Error occurs: %d\n", GetLastError());
         lRet = WRITE_ERR;
         break;
      }
   }

   while (bRet && (iRecLen == nBytesRead))
   {
      // Write existing rec out
      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
      lCnt++;

      bRet = ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);
   }

   // If there are more new records, add them
   while (pRoll)
   {
      // Create new R01 record
      iRet = Kin_CreatePublR01(acRec, acRollRec);
      iNewRec++;

      bRet = WriteFile(fhOut, acRec, iRecLen, &nBytesWritten, NULL);
      lCnt++;

      // Get next roll record
      pRoll = fgets(acRollRec, 1024, fdRoll);
   }

   // Close files
   if (fdRoll)
      fclose(fdRoll);
   if (fhOut)
      CloseHandle(fhOut);
   if (fhIn)
      CloseHandle(fhIn);

   //
   LogMsg("Total output records:       %u", lCnt);
   LogMsg("Total unassessed records:   %u", iNewRec);
   LogMsg("Total bad-city records:     %u", iBadCity);
   LogMsg("Total bad-suffix records:   %u", iBadSuffix);

   printf("\nTotal output records: %u\n", lCnt);

   lRecCnt = lCnt;

   return 0;
}

/******************************* Kin_ExtractProp8 *****************************
 *
 * Extract  Prop8 flag from roll file.
 *
 ******************************************************************************/

int Kin_ExtractProp8(char *pProp8File)
{
   char     *pTmp, acTmp[256], acProp8Rec[MAX_RECSIZE];
   char     acOutFile[_MAX_PATH];

   FILE     *fdProp8, *fdExt;
   KIN_ROLL *pRoll = (KIN_ROLL *)&acProp8Rec[0];

   int      iProp8Match=0;
   long     lCnt=0;

   if (lLienYear > 1900)
      sprintf(acTmp, "%d", lLienYear);
   else
      strcpy(acTmp, "yyyy");
   sprintf(acOutFile, acProp8Tmpl, myCounty.acCntyCode, myCounty.acCntyCode, acTmp, "P8");

   // Open Prop8 file
   LogMsg("Open Prop8 file %s", pProp8File);
   fdProp8 = fopen(pProp8File, "r");
   if (fdProp8 == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", pProp8File);
      return -2;
   }

   // Open Output file
   LogMsg("Open output file %s", acOutFile);
   fdExt = fopen(acOutFile, "w");
   if (fdExt == NULL)
   {
      LogMsg("***** Error creating output file: %s\n", acOutFile);
      return -4;
   }

   // Output header
   fputs("APN\n", fdExt);

   // Get first lien rec
   pTmp = fgets((char *)&acProp8Rec[0], 1024, fdProp8);

   // Merge loop
   while (pTmp)
   {
      // Set prop8 flag 
      if (pRoll->Taxability[0] == '8')
      {
         // Update flag
         sprintf(acTmp, "%.*s\n", RSIZ_APN, acProp8Rec);
         fputs(acTmp, fdExt);
         iProp8Match++;
      }

      // Read next prop8 record
      pTmp = fgets(acProp8Rec, 1024, fdProp8);

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdProp8)
      fclose(fdProp8);
   if (fdExt)
      fclose(fdExt);

   LogMsgD("Total records processed:      %u", lCnt);
   LogMsgD("Total records extracted:      %u", iProp8Match);
   return 0;
}

/***************************** Kin_ParseTaxDetail ****************************
 *
 * Use KIN_ROLL to generate TAXDETAIL & TAXAGENCY
 *
 * Return 0 if success
 *
 *****************************************************************************/

int Kin_ParseTaxDetail(FILE *fdDetail, FILE *fdAgency, char *pInbuf)
{
   char     acTmp[256], acDetail[512], acAgency[512];
   int      iIdx, iCnt;
   long     lTmp;
   TAXDETAIL *pDetail = (TAXDETAIL *)acDetail;
   TAXAGENCY *pAgency = (TAXAGENCY *)acAgency, *pResult;
   KIN_ROLL  *pInRec  = (KIN_ROLL  *)pInbuf;

   // Clear output buffer
   memset(acDetail, 0, sizeof(TAXDETAIL));
   memset(acAgency, 0, sizeof(TAXAGENCY));

   // APN
   memcpy(pDetail->Apn, pInRec->Apn, iApnLen);

   // Assessment number
   memcpy(pDetail->Assmnt_No, pInRec->Fee_Asmt, RSIZ_FEE_ASSMT);
#ifdef _DEBUG
   //if (!memcmp(pInRec->Apn, "002220072000", 9))
   //   lTmp = 0;
#endif

   // BillNumber

   // Tax Year
   if (pInRec->TaxYear[0] > '0')
      memcpy(pDetail->TaxYear, pInRec->TaxYear, RSIZ_TAX_YEAR);
   else if (pInRec->Apportionment_Year[0] > '0')
      memcpy(pDetail->TaxYear, pInRec->Apportionment_Year, RSIZ_TAX_YEAR);
   else
      sprintf(pDetail->TaxYear, "%d", lTaxYear);

   // Loop through tax items
   iCnt = atoin(pInRec->NumTaxes, RSIZ_TAX_ENTRIES);
   for (iIdx = 0; iIdx < iCnt; iIdx++)
   {      
      // Tax Amt
      lTmp =  atoin(pInRec->asTaxItems[iIdx].Inst_Amt1, RSIZ_INST_AMT);
      lTmp += atoin(pInRec->asTaxItems[iIdx].Inst_Amt2, RSIZ_INST_AMT);
      if (lTmp > 0)
         sprintf(pDetail->TaxAmt, "%.2f", (double)lTmp/100.0);

      // Tax Rate
      lTmp =  atoin(pInRec->asTaxItems[iIdx].Rate, RSIZ_TAX_RATE);
      if (lTmp > 0)
         sprintf(pDetail->TaxRate, "%.6f", (double)lTmp/1000000.0);

      // Agency 
      memcpy(pDetail->TaxCode, pInRec->asTaxItems[iIdx].Code, RSIZ_TAX_CODE);
      memcpy(pAgency->Code, pInRec->asTaxItems[iIdx].Code, RSIZ_TAX_CODE);
      pResult = findTaxAgency(pAgency->Code);
      if (pResult)
      {
         strcpy(pAgency->Agency, pResult->Agency);
      } else
      {
         sprintf(pAgency->Agency, "TAX CODE %s", pAgency->Code);
         LogMsg("+++ Unknown TaxCode: %s [%s]", pAgency->Code, pDetail->Apn);
      }

      // Generate csv line and write to file
      Tax_CreateDetailCsv(acTmp, pDetail);
      fputs(acTmp, fdDetail);

      // Generate Agency record
      Tax_CreateAgencyCsv(acTmp, pAgency);
      fputs(acTmp, fdAgency);
   }

   return 0;
}

/***************************** Kin_ParseTaxBase ******************************
 *
 * Parse roll record to Base, Detail, Delq, Agency, Owner
 *
 *****************************************************************************/

int Kin_ParseTaxBase(char *pBaseBuf, char *pDelqBuf, char *pInbuf)
{
   long     lTax1, lTax2, lTmp, lTotalFee, lTotalPen, lTotalTax;
   int      iTmp;
   char     acTmp[256], *p1, *p2, *p3, *p4;

   TAXBASE  *pBaseRec = (TAXBASE *)pBaseBuf;
   TAXDELQ  *pDelqRec = (TAXDELQ *)pDelqBuf;
   KIN_ROLL *pInRec  = (KIN_ROLL *)pInbuf;

   // Clear output buffer
   memset(pBaseBuf, 0, sizeof(TAXBASE));
   memset(pDelqBuf, 0, sizeof(TAXDELQ));

   // APN
   memcpy(pBaseRec->Apn, pInRec->Apn, iApnLen);
   memcpy(pBaseRec->OwnerInfo.Apn, pInRec->Apn, iApnLen);

   // Assessment No
   memcpy(pBaseRec->Assmnt_No, pInRec->Fee_Asmt, RSIZ_FEE_ASSMT);

   // TRA
   memcpy(pBaseRec->TRA, pInRec->TRA, RSIZ_TRA);

   // BillNum

   // Tax Year
   if (pInRec->Apportionment_Year[0] > '0')
      memcpy(pBaseRec->TaxYear, pInRec->Apportionment_Year, RSIZ_TAX_YEAR);
   else
      sprintf(pBaseRec->TaxYear, "%d", lTaxYear);
   strcpy(pBaseRec->OwnerInfo.TaxYear, pBaseRec->TaxYear);

   // Check for Tax amount - V99
   lTax1 = atoin(pInRec->Inst_Tot_Amt1, RSIZ_INST_AMT);
   lTax2 = atoin(pInRec->Inst_Tot_Amt2, RSIZ_INST_AMT);
   lTotalTax = lTax1+lTax2;
   if ((lTax1+lTax2) > 10)
   {
      sprintf(pBaseRec->TotalTaxAmt, "%.2f", (double)(lTotalTax/100.0));

      sprintf(pBaseRec->TaxAmt1, "%.2f", (double)(lTax1/100.0));
      sprintf(pBaseRec->TaxAmt2, "%.2f", (double)(lTax2/100.0));
   }

   // Paid 
   lTax1 = atoin(pInRec->Inst_Paid_Amt1, RSIZ_INST_AMT);
   if (lTax1 > 0)
   {
      pBaseRec->Inst1Status[0] = TAX_BSTAT_PAID;
      sprintf(pBaseRec->PaidAmt1, "%.2f", (double)(lTax1/100.0));
      memcpy(pBaseRec->PaidDate1, pInRec->Inst_Paid_Date1, RSIZ_DATE8);
      lTax2 = atoin(pInRec->Inst_Paid_Amt2, RSIZ_INST_AMT);
      if (lTax2 > 0)
      {
         sprintf(pBaseRec->PaidAmt2, "%.2f", (double)(lTax2/100.0));
         memcpy(pBaseRec->PaidDate2, pInRec->Inst_Paid_Date2, RSIZ_DATE8);
         pBaseRec->Inst2Status[0] = TAX_BSTAT_PAID;
      } else
         pBaseRec->Inst2Status[0] = TAX_BSTAT_UNPAID;
   } else
   {
      lTax2 = 0;
      pBaseRec->Inst1Status[0] = TAX_BSTAT_UNPAID;
      pBaseRec->Inst2Status[0] = TAX_BSTAT_UNPAID;
   }

   // Penalty
   lTotalPen = 0;
   if (pInRec->Inst_Pen_Code1 == 'P')
   {
      lTmp = atoin(pInRec->Inst_Pen_Amt1, RSIZ_PEN_AMT);
      sprintf(pBaseRec->PenAmt1, "%.2f", (double)(lTmp/100.0));
      lTotalPen = lTmp;
   }

   if (pInRec->Inst_Pen_Code2 == 'P')
   {
      lTmp = atoin(pInRec->Inst_Pen_Amt2, RSIZ_PEN_AMT);
      sprintf(pBaseRec->PenAmt2, "%.2f", (double)(lTmp/100.0));
      lTotalPen += lTmp;
   }

   // Delinquent cost
   //lTotalFee = atoin(pInRec->Delq_Cost, RSIZ_DELINQUENT_COST);
   lTotalFee = 0;
   // Total fee
   if (lTotalFee > 0)
      sprintf(pBaseRec->TotalFees, "%.2f", (double)(lTotalFee/100.0));

   // Due Date
   if (pInRec->Inst_Due_Date1[0] > '0')
      memcpy(pBaseRec->DueDate1, pInRec->Inst_Due_Date1, RSIZ_DATE8);
   if (pInRec->Inst_Due_Date2[0] > '0')
      memcpy(pBaseRec->DueDate2, pInRec->Inst_Due_Date2, RSIZ_DATE8);         

   // Total due
   if (lTotalTax > (lTax1+lTax2))
   {
      lTmp = lTotalTax - (lTax1+lTax2) + lTotalFee + lTotalPen;
      sprintf(pBaseRec->TotalDue, "%.2f", (double)(lTmp/100.0));
   }

#ifdef _DEBUG
   //if (!memcmp(pInRec->Apn, "002220072000", 9))
   //   iTmp = 0;
#endif

   // Is delinquent - 002010059000
   if (pInRec->Default_Date[0] > '0')
   {
      memcpy(pBaseRec->Def_Date, pInRec->Default_Date, RSIZ_DATE8);

      // Create DELQ record
      strcpy(pDelqRec->Apn, pBaseRec->Apn);
      strcpy(pDelqRec->Assmnt_No, pBaseRec->Assmnt_No);

      iTmp = atoin(pBaseRec->Def_Date, 4);
      if (iTmp > 1990 && iTmp <= lTaxYear)
      {
         memcpy(pBaseRec->DelqYear, pBaseRec->Def_Date, 4);
         sprintf(pDelqRec->TaxYear, "%d", iTmp-1);
         strcpy(pDelqRec->Def_Date, pBaseRec->Def_Date);
      } else
         LogMsg("*** Invalid Default date [%s]: %s", pBaseRec->Apn, pBaseRec->Def_Date);
      memcpy(pDelqRec->Default_No, pInRec->Default_No, RSIZ_DEFAULT_NO);

      // Redeem date & amt
      if (pInRec->Redeemed_Date[0] > '0')
      {
         memcpy(pDelqRec->Red_Date, pInRec->Redeemed_Date, RSIZ_DATE8);
         pDelqRec->DelqStatus[0] = TAX_STAT_REDEEMED;
      } else
      {
         pDelqRec->DelqStatus[0] = TAX_STAT_UNPAID;
         pBaseRec->isDelq[0] = '1';
         pDelqRec->isDelq[0] = '1';
         //strcpy(pDelqRec->Tax_Amt, pBaseRec->TotalTaxAmt);
         //sprintf(pDelqRec->Pen_Amt, "%.2f", lTotalPen);
         //sprintf(pDelqRec->Fee_Amt, "%.2f", lTotalFee);
      }
      sprintf(pDelqRec->Upd_Date, "%d", lLastTaxFileDate);
   } else
      pBaseRec->isDelq[0] = '0';

   // Supplemental record?
   if (!memcmp(pInRec->TaxType, "SB", 2))
      pBaseRec->isSupp[0] = '1';
   else
      pBaseRec->isSupp[0] = '0';

   pBaseRec->isSecd[0] = '1';
   pBaseRec->BillType[0] = BILLTYPE_SECURED;
   sprintf(pBaseRec->Upd_Date, "%d", lLastTaxFileDate);

   // Owner info
   memcpy(acTmp, pInRec->Assessee, RSIZ_NAME);
   blankRem(acTmp, RSIZ_NAME);
   strcpy(pBaseRec->OwnerInfo.Name1, acTmp);
   p1 = pInRec->Mail.M_Addr1;
   p2 = pInRec->Mail.M_Addr2;
   p3 = pInRec->Mail.M_Addr3;
   p4 = pInRec->Mail.M_Addr4;
   iTmp = RSIZ_M_ADDR_1;

   if (!memcmp(pInRec->Mail.M_Addr1, "C/O", 3) || !memcmp(pInRec->Mail.M_Addr1, "DBA ", 4))
   {
      memcpy(acTmp, pInRec->Mail.M_Addr1, RSIZ_M_ADDR_1);
      blankRem(acTmp, RSIZ_M_ADDR_1);
      if (!memcmp(acTmp, "DBA", 3))
         strcpy(pBaseRec->OwnerInfo.Dba, &acTmp[4]);
      else
         strcpy(pBaseRec->OwnerInfo.CareOf, &acTmp[4]);
      p1 = pInRec->Mail.M_Addr2;
      p2 = pInRec->Mail.M_Addr3;
      p3 = pInRec->Mail.M_Addr4;
      p4 = "";
      iTmp = RSIZ_M_ADDR_2;
   }
   if (!memcmp(pInRec->Mail.M_Addr2, "C/O", 3) || !memcmp(pInRec->Mail.M_Addr2, "DBA ", 4))
   {
      memcpy(acTmp, pInRec->Mail.M_Addr2, RSIZ_M_ADDR_2);
      blankRem(acTmp, RSIZ_M_ADDR_2);
      if (!memcmp(acTmp, "DBA", 3))
         strcpy(pBaseRec->OwnerInfo.Dba, &acTmp[4]);
      else
         strcpy(pBaseRec->OwnerInfo.CareOf, &acTmp[4]);
      p1 = pInRec->Mail.M_Addr3;
      p2 = pInRec->Mail.M_Addr4;
      p3 = p4 = "";
      iTmp = RSIZ_M_ADDR_2;
   }

   memcpy(pBaseRec->OwnerInfo.MailAdr[0], p1, iTmp);
   blankRem(pBaseRec->OwnerInfo.MailAdr[0], iTmp);
   memcpy(pBaseRec->OwnerInfo.MailAdr[1], p2, RSIZ_M_ADDR_2);
   blankRem(pBaseRec->OwnerInfo.MailAdr[1], RSIZ_M_ADDR_2);

   if (*p3 > ' ')
   {
      //LogMsg("MailAdr 3: %.12s: %s|%s|%.20s", pBaseRec->Apn, pBaseRec->OwnerInfo.MailAdr[0], pBaseRec->OwnerInfo.MailAdr[1], p3);
      memcpy(pBaseRec->OwnerInfo.MailAdr[2], p3, RSIZ_M_ADDR_2);
      blankRem(pBaseRec->OwnerInfo.MailAdr[2], RSIZ_M_ADDR_2);
      if (*p4 > ' ')
      {
         //LogMsg("MailAdr4: %.12s: %s|%s|%.20s|%.20s", pBaseRec->Apn, pBaseRec->OwnerInfo.MailAdr[0], pBaseRec->OwnerInfo.MailAdr[1], p3, p4);
         memcpy(pBaseRec->OwnerInfo.MailAdr[3], p4, RSIZ_M_ADDR_2);
         blankRem(pBaseRec->OwnerInfo.MailAdr[3], RSIZ_M_ADDR_2);
      }
   }

   return 0;
}

/****************************** Kin_UpdateDelq ********************************
 * 
 * Update redeem info and populate delq amt
 *
 ******************************************************************************/

int Kin_UpdateDelq(char *pBaseBuf, char *pDelqBuf, FILE *fd)
{
   static   char  acRec[MAX_RECSIZE], *pRec=NULL;

   int      iRet, iLoop;
   double   dTmp;
   bool     bDontRead=false;

   TAXBASE   *pBaseRec = (TAXBASE *)pBaseBuf;
   TAXDELQ   *pDelqRec = (TAXDELQ *)pDelqBuf;
   KIN_RDMPT *pRdmpt   = (KIN_RDMPT *)acRec;

   // Get first rec
   if (!pRec)
      pRec = fgets(acRec, MAX_RECSIZE, fd);        

#ifdef _DEBUG
   //if (!memcmp(pDelqRec->Apn, "004050131000", 9) )
   //   iRet = 0;
#endif

   do
   {
      // Compare Apn
      iLoop = memcmp(pDelqRec->Apn, pRdmpt->Apn, iApnLen);
      if (iLoop > 0 || pRdmpt->Status[0] == 'I')   // Ignore inactive rec
      {
         if (bDebug)
            LogMsg("*** Skip rec %.80s", acRec);
         pRec = fgets(acRec, MAX_RECSIZE, fd);
         if (!pRec)
         {
            fclose(fd);
            fd = NULL;
            return 1;      // EOF
         }
      }
   } while (iLoop > 0);

   if (iLoop)
      return 1;

   // Matching up billNum before update
   if (pDelqRec->Default_No[0] > ' ' && memcmp(pDelqRec->Default_No, pRdmpt->Delq_No, TSIZ_DELQ_NO))
   {
      LogMsg("*** Skip update APN=%s, Delq.BillNum=%s, Rdmpt.BillNum=%.6s", pDelqRec->Apn, pDelqRec->Default_No, pRdmpt->Delq_No);
      return 1;
   }

   if (pRdmpt->Redeemed_Flg[0] == 'R')
   {
      if (pRdmpt->Redeemed_Date[0] == '2')
      {
         memcpy(pDelqRec->Red_Date, pRdmpt->Redeemed_Date, TSIZ_DELQ_DATE);
         dTmp = atofn(pRdmpt->Full_Rdmpt_Amount, TSIZ_FULL_RDMPT_AMOUNT);
         sprintf(pDelqRec->Red_Amt, "%.2f", dTmp);
         pBaseRec->isDelq[0] = '0';
         pDelqRec->isDelq[0] = '0';
         pDelqRec->DelqStatus[0] = TAX_STAT_REDEEMED;
         sprintf(pDelqRec->Upd_Date, "%d", lRdmptFileDate);
      } else
         iRet = 1;
   } else if (pRdmpt->Pay_Plan_Account_No[0] > ' ')
   {
      pBaseRec->isDelq[0] = '0';
      pDelqRec->isDelq[0] = '0';
      pDelqRec->DelqStatus[0] = TAX_STAT_ONPAYMENT;
      sprintf(pDelqRec->Upd_Date, "%d", lRdmptFileDate);
   } else if (pDelqRec->DelqStatus[0] != TAX_STAT_REDEEMED)
   {
      // Delq unpaid - get next record for default info
      do {
         pRec = fgets(acRec, MAX_RECSIZE, fd);     
      } while (pRec && pRdmpt->Status[0] == 'I');

      if (!memcmp(pDelqRec->Apn, pRdmpt->Apn, iApnLen))
      {
         dTmp = atofn(pRdmpt->TaxAmt1, TSIZ_TAXAMT1);
         dTmp += atofn(pRdmpt->TaxAmt2, TSIZ_TAXAMT1);
         if (dTmp > 0.0)
            sprintf(pDelqRec->Def_Amt, "%.2f", dTmp);
         else
            sprintf(pDelqRec->Def_Amt, "%.2f", dTmp);

         pDelqRec->DelqStatus[0] = TAX_STAT_UNPAID;
         pBaseRec->isDelq[0] = '1';
         pDelqRec->isDelq[0] = '1';
      } else
         bDontRead = true;
      sprintf(pDelqRec->Upd_Date, "%d", lRdmptFileDate);
   }

   lDelqMatch++;

   // Get next record
   if (!bDontRead)
      pRec = fgets(acRec, MAX_RECSIZE, fd);
   if (!pRec)
   {
      fclose(fd);
      fd = NULL;
   }

   return 0;
}

/**************************** Kin_Load_TaxBase *******************************
 *
 * Create base import file and import into SQL if specified
 *
 * Return 0 if success.
 *
 *****************************************************************************/

int Kin_Load_TaxBase(bool bImport)
{
   char      *pTmp, acBase[MAX_RECSIZE], acDelq[512], acRec[MAX_RECSIZE], acTmp[MAX_RECSIZE];
   char      acBaseFile[_MAX_PATH], acDetailFile[_MAX_PATH], acTmpFile[_MAX_PATH], 
             acAgencyFile[_MAX_PATH], acDelqFile[_MAX_PATH], acRdmptFile[_MAX_PATH], acOwnerFile[_MAX_PATH];

   int       iRet;
   long      lOut=0, lCnt=0;
   FILE      *fdDelq, *fdBase, *fdDetail, *fdAgency, *fdIn, *fdOwner, *fdRdmpt;
   TAXBASE   *pBase = (TAXBASE *)acBase;
   TAXDELQ   *pDelq = (TAXDELQ *)acDelq;
   KIN_ROLL  *pInRec = (KIN_ROLL *)acRec;

   LogMsg0("Loading tax base");

   sprintf(acBaseFile, sTaxOutTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "Base");
   sprintf(acOwnerFile, sTaxOutTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "Owner");
   sprintf(acDetailFile, sTaxOutTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "Items");
   sprintf(acAgencyFile, sTaxOutTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "Agency");
   sprintf(acDelqFile, sTaxOutTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "Delq");
   pTmp = strrchr(acBaseFile, '\\');
   *pTmp = 0;
   if (_access(acBaseFile, 0))
      _mkdir(acBaseFile);
   *pTmp = '\\';

   // Open input file
   LogMsg("Open current roll file %s", acRollFile);
   // Only process if new tax file
   iRet = isNewTaxFile(NULL, myCounty.acCntyCode);
   if (iRet <= 0)
   {
      lLastTaxFileDate = 0;
      return iRet;
   }

   fdIn = fopen(acRollFile, "r");
   if (fdIn == NULL)
   {
      LogMsg("***** Error opening Current tax file: %s\n", acRollFile);
      return -2;
   }  

   // Open redemption file
   iRet = GetIniString(myCounty.acCntyCode, "Redemption", "", acRdmptFile, _MAX_PATH, acIniFile);
   if (iRet > 10 && !_access(acRdmptFile, 0))
   {
      lRdmptFileDate = getFileDate(acRdmptFile);
      
      // This file needs resort on APN, TaxYear
      sprintf(acTmp, "%s\\%s\\%s_Rdmpt.txt", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
      iRet = sortFile(acRdmptFile, acTmp, "S(13,12,C,A,25,4,C,A) F(TXT)");
      if (!iRet)
         return -1;

      LogMsg("Open redemption file: %s", acTmp);
      fdRdmpt = fopen(acTmp, "r");
      if (fdRdmpt == NULL)
      {
         LogMsg("***** Error opening redemption tax file: %s\n", acTmp);
         return -2;
      }  
#ifdef _DEBUG
      //int iTmp = 2978;
      //char *pTest;
      //do
      //{
      //   if (pTmp = fgets(acRec, 3000, fdRdmpt))
      //   {
      //      iRet = strlen(pTmp);
      //      if (iRet < iTmp)
      //         iRet = iTmp;
      //      if (pTest=strchr(pTmp, 12))
      //         lOut++;
      //      lCnt++;
      //   }
      //} while (pTmp);
#endif
   } else
   {
      LogMsg("***** Redemption file not found: %s", acRdmptFile);
      fdRdmpt = NULL;
   }

   // Open base file
   LogMsg("Open Base file %s", acBaseFile);
   fdBase = fopen(acBaseFile, "w");
   if (fdBase == NULL)
   {
      LogMsg("***** Error creating base file: %s\n", acBaseFile);
      return -4;
   }

   // Open Owner file
   LogMsg("Open Owner output file %s", acOwnerFile);
   fdOwner = fopen(acOwnerFile, "w");
   if (fdOwner == NULL)
   {
      LogMsg("***** Error creating Owner output file: %s\n", acOwnerFile);
      return -4;
   }

   // Open Detail file
   LogMsg("Open Detail output file %s", acDetailFile);
   fdDetail = fopen(acDetailFile, "w");
   if (fdDetail == NULL)
   {
      LogMsg("***** Error creating Detail output file: %s\n", acDetailFile);
      return -4;
   }

   // Open delq file
   LogMsg("Open Delq file %s", acDelqFile);
   fdDelq = fopen(acDelqFile, "w");
   if (fdDelq == NULL)
   {
      LogMsg("***** Error creating base file: %s\n", acDelqFile);
      return -4;
   }

   // Open Agency file
   sprintf(acTmpFile, "%s\\%s\\%s_Agency.tmp", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   LogMsg("Open Agency file %s", acTmpFile);
   fdAgency = fopen(acTmpFile, "w");
   if (fdAgency == NULL)
   {
      LogMsg("***** Error creating Agency file: %s\n", acTmpFile);
      return -4;
   }

   // Init variables

   // Merge loop 
   while (!feof(fdIn))
   {
      pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdIn);
      if (!pTmp)
         break;

      // Skip correction records
      //if (pInRec->BOC[0] != ' ' || pInRec->Status != 'C')
      if (Kin_ChkCond(acRec) < 0)
         continue;

      // Create new R01 record
      iRet = Kin_ParseTaxBase(acBase, acDelq, acRec);
      if (!iRet)
      {
         // Create TaxBase record
         if (acDelq[0] > ' ')
         {
            // Update redemption data
            if (fdRdmpt)
            {
               iRet = Kin_UpdateDelq(acBase, acDelq, fdRdmpt);
            }

            Tax_CreateDelqCsv(acTmp, (TAXDELQ *)&acDelq);
            fputs(acTmp, fdDelq);
         }

         // Create detail & agency records
         Kin_ParseTaxDetail(fdDetail, fdAgency, acRec);

         // Create TaxBase record
         Tax_CreateTaxBaseCsv(acTmp, (TAXBASE *)&acBase);
         fputs(acTmp, fdBase);

         // Create owner record
         Tax_CreateTaxOwnerCsv(acTmp, &pBase->OwnerInfo);
         fputs(acTmp, fdOwner);

         lOut++;
      } else
      {
         LogMsg("---> Drop record %d [%.40s]", lCnt, acRec); 
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdIn)
      fclose(fdIn);
   if (fdBase)
      fclose(fdBase);
   if (fdDetail)
      fclose(fdDetail);
   if (fdAgency)
      fclose(fdAgency);
   if (fdDelq)
      fclose(fdDelq);
   if (fdOwner)
      fclose(fdOwner);
   if (fdRdmpt)
      fclose(fdRdmpt);

   LogMsg("Total records processed:    %u", lCnt);
   LogMsg("Total output records:       %u", lOut);
   LogMsg("Total Redemption matched:   %u", lDelqMatch);

   // Dedup Agency file
   iRet = sortFile(acTmpFile, acAgencyFile, "S(#1,C,A) DUPO F(TXT)");

   // Import into SQL
   if (lOut > 30000 && bImport)
   {
      iRet = doTaxImport(myCounty.acCntyCode, TAX_BASE);
      if (!iRet)
         iRet = doTaxImport(myCounty.acCntyCode, TAX_OWNER);
      if (!iRet)
         iRet = doTaxImport(myCounty.acCntyCode, TAX_DETAIL);
      if (!iRet)
         iRet = doTaxImport(myCounty.acCntyCode, TAX_AGENCY);
      if (!iRet)
         iRet = doTaxImport(myCounty.acCntyCode, TAX_DELINQUENT);
   } else
   {
      LogMsg("***** Number of output records is too small.  Please check input file: %s", acRollFile);
      iRet = -10;
   }

   return iRet;
}

/******************************** Kin_ConvStdChar ****************************
 *
 *
 *****************************************************************************/

int Kin_ConvStdChar(char *pInfile)
{
   FILE     *fdIn, *fdOut;
   char     acBuf[MAX_RECSIZE], acTmpFile[256], acTmp[256], acCode[4], *pRec;
   int      iRet, iTmp, iCnt=0;
   ULONG    lTmp;

   KIN_CHAR *pChar = (KIN_CHAR *)&acBuf[0];
   STDCHAR  myCharRec;

   LogMsg0("Extracting Chars");
   LogMsg("Open char file %s", pInfile);
   if (!(fdIn = fopen(pInfile, "r")))
      return -1;

   sprintf(acTmpFile, "%s\\%s\\%s_char.tmp", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   if (!(fdOut = fopen(acTmpFile, "w")))
   {
      fclose(fdIn);
      LogMsg("***** Error creating output file %s", acTmpFile);
      return -2;
   }

   // Start loop
   while (!feof(fdIn))
   {
      pRec = fgets(acBuf, MAX_RECSIZE, fdIn);

      if (!pRec)
         break;

      if (acBuf[0] <= ' ')
         continue;

      memset((void *)&myCharRec, ' ', sizeof(STDCHAR));
      
      // Format APN
      memcpy(myCharRec.Apn, pChar->Apn, CSIZ_APN);
      iRet = formatApn(pChar->Apn, acTmp, &myCounty);
      memcpy(myCharRec.Apn_D, acTmp, iRet);

      // Quality Class
      if (isalpha(pChar->BldgClass[0]))
      {  // D045A
         myCharRec.BldgClass = pChar->BldgClass[0];
         iTmp = atoin((char *)&pChar->BldgClass[1], 2);
         if (iTmp > 0)
         {
            sprintf(acTmp, "%d.%c", iTmp, pChar->BldgClass[3]);
            if ((iRet = Value2Code(acTmp, acCode, NULL)) >= 0)
               myCharRec.BldgQual = acCode[0];
         }
      }

      // YrEff
      lTmp = atoin(pChar->EffYear, CSIZ_EFF_YR);
      if (lTmp > 1700)
         memcpy(myCharRec.YrEff, pChar->EffYear, SIZ_YR_BLT);

      // YrBlt
      lTmp = atoin(pChar->YrBlt, CSIZ_EFF_YR);
      if (lTmp > 1700)
         memcpy(myCharRec.YrBlt, pChar->YrBlt, SIZ_YR_BLT);

      // BldgSqft
      lTmp = atoin(pChar->BldgSqft, CSIZ_LIVING_AREA);
      if (lTmp > 10)
      {
         iTmp = sprintf(acTmp, "%u", lTmp);
         memcpy(myCharRec.BldgSqft, acTmp, iTmp);
      }

      // Beds
      lTmp = atoin(pChar->Beds, CSIZ_BEDS);
      if (lTmp > 0)
      {
         iTmp = sprintf(acTmp, "%u", lTmp);
         memcpy(myCharRec.Beds, acTmp, iTmp);
      }

      // Baths - 99V9
      int iFBath = atoin(pChar->Baths, CSIZ_BATHS-1);
      if (iFBath > 0)
      {
         iTmp = sprintf(acTmp, "%u", iFBath);
         memcpy(myCharRec.FBaths, acTmp, iTmp);
         memcpy(myCharRec.Bath_4Q, acTmp, iTmp);

         // Half bath: 3,5,7
         iTmp = pChar->Baths[CSIZ_BATHS-1] & 0x0F;
         if (iTmp > 0)
         {
            myCharRec.HBaths[0] = '1';
            if (iTmp >= 6)
               myCharRec.Bath_3Q[0] = '1';
            else if (iTmp >= 4)
               myCharRec.Bath_2Q[0] = '1';
            else 
               myCharRec.Bath_1Q[0] = '1';
         }
      }

      // Rooms
      int iRooms = atoin(pChar->Rooms, CSIZ_ROOMS);
      if (iRooms > 0)
      {
         iTmp = sprintf(acTmp, "%u", iRooms);
         memcpy(myCharRec.Rooms, acTmp, iTmp);
      }

      // Garage Sqft
      lTmp = atoin(pChar->GarSqft, CSIZ_GARAGE_AREA);
      if (lTmp > 10)
      {
         iTmp = sprintf(acTmp, "%u", lTmp);
         memcpy(myCharRec.GarSqft, acTmp, iTmp);

         // This is to cover where Garage_Spcs is 0
         myCharRec.ParkType[0] = 'Z';
      }

      // Carport?
      if (pChar->Carport == 'Y')
         myCharRec.ParkType[0] = 'C';

      // Heating
      if (pChar->Heating[0] == 'C')
         myCharRec.Heating[0] = '1';          // Conventional
      else if (pChar->Heating[0] == 'M')
         myCharRec.Heating[0] = '2';          // Modern
      else if (!memcmp(pChar->Heating, "FD", 2))
         myCharRec.Heating[0] = 'B';          // Down Flow
      else if (!memcmp(pChar->Heating, "FU", 2))
         myCharRec.Heating[0] = 'B';          // Up Flow
      else if (!memcmp(pChar->Heating, "WF", 2))
         myCharRec.Heating[0] = 'M';          // Wall or Floor
      else if (pChar->Heating[0] > ' ')
         LogMsg("*** Unknown Heating code: %.2s [%.12s]", pChar->Heating, pChar->Apn);

      // Cooling
      if (pChar->Cooling[0] == 'C')
         myCharRec.Cooling[0] = '1';         // Conventional
      else if (pChar->Cooling[0] == 'M')
         myCharRec.Cooling[0] = '2';         // Modern
      else if (!memcmp(pChar->Cooling, "O ", 2))
         myCharRec.Cooling[0] = 'X';         // Other
      else if (!memcmp(pChar->Cooling, "R ", 2))
         myCharRec.Cooling[0] = 'A';         // Refrigeration 
      else if (!memcmp(pChar->Cooling, "EC", 2))
         myCharRec.Cooling[0] = 'E';         // Evaporator Cool
      else if (!memcmp(pChar->Cooling, "TW", 2))
         myCharRec.Cooling[0] = 'L';         // Thru Wall
      else if (pChar->Cooling[0] > ' ')
         LogMsg("*** Unknown cooling code: %.2s [%.12s]", pChar->Cooling, pChar->Apn);

      // Pool/Spa
      if (pChar->Pool_Spa == 'P')
         myCharRec.Pool[0] = 'P';            // Pool
      else if (pChar->Pool_Spa == 'S')
         myCharRec.Pool[0] = 'S';            // Spa
      else if (pChar->Pool_Spa == 'B')
         myCharRec.Pool[0] = 'C';            // Pool/Spa

      // Fireplace
      if (pChar->Fireplace > '0')
         myCharRec.Fireplace[0] = pChar->Fireplace;

#ifdef _DEBUG
      //if (!memcmp(pOutbuf, "050010001", 9))
      //   iTmp = 0;
#endif
      // Lot sqft - Lot Acres: only update if current roll value not present
      lTmp = atoin(pChar->LotSqft, CSIZ_LOT_AREA);
      if (lTmp > 100)
      {
         iTmp = sprintf(acTmp, "%u", lTmp);
         memcpy(myCharRec.LotSqft, acTmp, iTmp);

         lTmp = (long)((((double)lTmp*ACRES_FACTOR)/SQFT_PER_ACRE)+0.5);
         iTmp = sprintf(acTmp, "%u", lTmp);
         memcpy(myCharRec.LotAcre, acTmp, iTmp);
      }

      // Stories 99V9
      iTmp = atoin(pChar->Stories, CSIZ_STORIES);
      if (iTmp > 9)
      {
         iTmp = sprintf(acTmp, "%.1f", (float)(iTmp/10));
         memcpy(myCharRec.Stories, acTmp, iTmp);
      }

      // Park spaces

      // Floor

      // Misc Impr
      myCharRec.MiscImpr_Code[0] = pChar->MiscImpr;    

      // Zoning

      // Usecode

      // Basement Sqft
      lTmp = atoin(pChar->BsmtSqft, CSIZ_BASEMENT_SQFT);
      if (lTmp > 50)
      {
         iTmp = sprintf(acTmp, "%d", lTmp);
         memcpy(myCharRec.BsmtSqft, acTmp, iTmp);
      }

      myCharRec.CRLF[0] = '\n';
      myCharRec.CRLF[1] = '\0';
      fputs((char *)&myCharRec.Apn[0], fdOut);

      if (!(++iCnt % 1000))
         printf("\r%u", iCnt);
   }

   if (fdIn) fclose(fdIn);
   if (fdOut) fclose(fdOut);

   LogMsg("Number of records processed: %d", iCnt);

   // Sort output on ASMT
   if (iCnt > 100)
   {
      // Rename old CHAR file if needed
      if (!_access(acCChrFile, 0))
      {
         strcpy(acBuf, acCChrFile);
         replStr(acBuf, ".dat", ".sav");
         if (!_access(acBuf, 0))
         {
            LogMsg("Delete old %s", acBuf);
            DeleteFile(acBuf);
         }
         LogMsg("Rename %s to %s", acCChrFile, acBuf);
         rename(acCChrFile, acBuf);
      } 

      LogMsgD("\nSorting char file %s --> %s", acTmpFile, acCChrFile);
      // Sort on APN, YrBlt to keep the latest record on top
      iRet = sortFile(acTmpFile, acCChrFile, "S(1,14,C,A,53,4,C,D) F(TXT) ");
   } else
   {
      printf("\n");
      iRet = 0;
   }

   return iRet;
}

/*********************************** loadKin ********************************
 *
 * Input files:
 *    - KIN_LIEN.BIN             (lien date roll, 2550-byte ebcdic)
 *    - FIL645E.BIN              (roll file, 2550-byte ebcdic)
 *    - F02101K.BIN              (char file, 250-byte ebcdic)
 *    - F02151K.BIN              (sale file, 198-byte ebcdic)
 *    - Kin_sale_cum.srt         (cum sale,  180-byte ascii)
 *
 * Following are tasks to be done here:
 *    - Convert ebcdic to ascii.  The above .BIN files become:
 *         + FIL645E.ASC
 *         + F02101K.ASC
 *         + F02151K.ASC
 *         + KIN_LIEN.ASC (for lien date processing only)
 *    - Normal update: LoadOne -U -Us|-Ms [-Mp]
 *    - Load Lien:     LoadOne -L -Us -Xl -Mp [-X8]
 *    - Load Tax:      LoadOne -T
 *
 ****************************************************************************/

int loadKin(int iSkip)
{
   int   iRet;
   long  lFileDate;
   char  acDefFile[_MAX_PATH], acTmpFile[_MAX_PATH];

   iApnLen = myCounty.iApnLen;

   // Translate Roll file ebcdic to ascii
   // Input:  FIL645E.BIN
   // Output: Kin_Roll.asc/Kin_Lien.asc (go to G:\CO_OUT\KIN folder)
   if (iLoadTax || (iLoadFlag & (LOAD_LIEN|LOAD_UPDT|EXTR_LIEN)) || (lOptProp8 & MYOPT_EXT))
   {
      sprintf(acTmpFile, "%s\\%s\\%s_Roll.asc", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
      if (!_access(acTmpFile, 0))
         lFileDate = getFileDate(acTmpFile);
      else
         lFileDate = 0;
      lLastTaxFileDate = getFileDate(acRollFile);
      if (lLastTaxFileDate > lFileDate)
      {
         GetIniString(myCounty.acCntyCode, "RollDef", "", acDefFile, _MAX_PATH, acIniFile);
         if (_access(acDefFile, 0))
         {
            LogMsg("***** Error: missing definition file %s", acDefFile);
            return 1;
         }

         LogMsg("Translate %s to Ascii %s", acRollFile, acTmpFile);
         iRet = F_Ebc2Asc(acRollFile, acTmpFile, acDefFile, 0);
         if (iRet < 0)
         {
            dispError(iRet, acRollFile, acTmpFile, acDefFile);
            return 1;
         } else
            strcpy(acRollFile, acTmpFile);
      } else
         strcpy(acRollFile, acTmpFile);
   }

   // Loading Tax
   if (iLoadTax)
   {
      iRet = GetIniString(myCounty.acCntyCode, "TaxDist", "", acTmpFile, _MAX_PATH, acIniFile);
      if (iRet > 0)
         iRet = LoadTaxCodeTable(acTmpFile);

      // KIN has base, delq, detail, 
      iRet = Kin_Load_TaxBase(bTaxImport);
      if (!iRet)
         iRet = updateDelqFlag(myCounty.acCntyCode);
   }

   if (!iLoadFlag)
      return iRet;

    // Load tables
   //iRet = LoadLUTable((char *)&acLookupTbl[0], "[Quality]", NULL, MAX_ATTR_ENTRIES);
   //if (!iRet)
   //{
   //   LogMsg("*** Error Looking for table [Quality] in %s", acLookupTbl);
   //   return 1;
   //}

   // Convert old cum sale format to standard format
   //iRet = Kin_convertSaleData(myCounty.acCntyCode);

   // Translate to ascii
   if (lOptProp8 & MYOPT_EXT)                      // -X8 Extract prop8 flag to text file
   {
      iRet = Kin_ExtractProp8(acTmpFile);
   } else if (iLoadFlag & (LOAD_LIEN|LOAD_UPDT))
   {
      // Translate char file ebcdic to ascii
      // Input:  F02101K.BIN
      // Output: Kin_Char.asc
      GetIniString(myCounty.acCntyCode, "CharDef", "", acDefFile, _MAX_PATH, acIniFile);
      if (_access(acDefFile, 0))
      {
         LogMsg("***** Error: missing definition file %s", acDefFile);
         return 1;
      }

      sprintf(acTmpFile, "%s\\%s\\%s_Char.asc", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
      LogMsg0("Translate %s to Ascii %s", acCharFile, acTmpFile);
      iRet = F_Ebc2Asc(acCharFile, acTmpFile, acDefFile, 0);
      if (iRet < 0)
      {
         dispError(iRet, acCharFile, acTmpFile, acDefFile);
         return 1;
      } else
         strcpy(acCharFile, acTmpFile);
   }

   // Extract CHAR
   if (iLoadFlag & EXTR_ATTR)                      // -Xa
   {
      // Load char file
      iRet = Kin_ConvStdChar(acCharFile);
   }

   // This has to be done before roll update
   // Input:  F02151K.BIN
   // Output: Kin_Sale.asc, Kin_Sale.dat & KinSale.sls
   if (iLoadFlag & (UPDT_SALE|EXTR_SALE))          // -Us or -Xs
   {
      // Translate sale file ebcdic to ascii
      GetIniString(myCounty.acCntyCode, "SaleDef", "", acDefFile, _MAX_PATH, acIniFile);
      if (_access(acDefFile, 0))
      {
         LogMsg("***** Error: missing definition file %s", acDefFile);
         return 1;
      }

      sprintf(acTmpFile, "%s\\%s\\%s_Sale.asc", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
      LogMsg0("Translate %s to Ascii %s", acSaleFile, acTmpFile);
      iRet = F_Ebc2Asc(acSaleFile, acTmpFile, acDefFile, 0);
      if (iRet < 0)
      {
         dispError(iRet, acSaleFile, acTmpFile, acDefFile);
         return 1;
      }

      LogMsg("Update %s sale history file", myCounty.acCntyCode);
      iRet = Kin_ExtrSale(acTmpFile);
      if (iRet)
         iLoadFlag = 0;          // Stop all operation
      else
         iLoadFlag |= MERG_CSAL;
   }

   // Extract lien
   if (iLoadFlag & EXTR_LIEN)                      // -Xl
      iRet = Kin_ExtrLien();

   // Merge roll file
   if (iLoadFlag & LOAD_LIEN)                      // -L
      iRet = Kin_Load_LDR(iSkip);
   else if (iLoadFlag & LOAD_UPDT)                 // -U
      iRet = Kin_Load_Roll(iSkip);

   // Merge public file
   if (!iRet && (iLoadFlag & MERG_PUBL))           // -Mp
   {
      LogMsg0("Merge public parcel file %s", acPubParcelFile);
      if (!_access(acPubParcelFile, 0))
      {
         iRet = Kin_MergePubl(iSkip);
      } else
         LogMsg("***** Public parcel file <%s> is missing", acPubParcelFile);
   }

   // Apply cum sale file to R01
   if (!iRet && (iLoadFlag & MERG_CSAL) )          // -Ms
   {
      // Apply Kin_Sale.sls to R01 file
      sprintf(acCSalFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "sls");
      iRet = ApplyCumSale(iSkip, acCSalFile, false, SALE_USE_SCUPDXFR);
   }

   return iRet;
}