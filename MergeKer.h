#ifndef _MERGEKER_H
#define  _MERGEKER_H

// 2023 - 2023 Current Tax Year.txt
#define  KER_L_ID                0
#define  KER_L_TENO              1
#define  KER_L_ASSESSE_NO        2
#define  KER_L_TRA               3
#define  KER_L_ATN               4
#define  KER_L_FILENO            5
#define  KER_L_USE               6
#define  KER_L_EXECODE           7
#define  KER_L_ASSESSEE          8
#define  KER_L_MADDR1            9
#define  KER_L_MADDR2            10
#define  KER_L_ZIPCODE           11
#define  KER_L_ACRES             12
#define  KER_L_CITRUS_ACRES      13
#define  KER_L_MINERAL           14
#define  KER_L_LAND              15
#define  KER_L_IMPR              16
#define  KER_L_OTHER_IMP         17
#define  KER_L_PERSPROP          18
#define  KER_L_EXEAMT            19
#define  KER_L_NETVALUE          20
#define  KER_L_ROLL              21
#define  KER_L_DIST_NO           22
#define  KER_L_SITUS             23
#define  KER_L_LEGAL             24
#define  KER_L_CAREOF            25
#define  KER_L_DBA               26
#define  KER_L_AGPRESERVE        27
#define  KER_L_MADDR3            28
#define  KER_L_FLD36             29
#define  KER_L_FLDS              30

// 2020
//#define  KER_L_ROLL              0
//#define  KER_L_TRA               1
//#define  KER_L_ATN               2
//#define  KER_L_FILENO            3
//#define  KER_L_APN               4
//#define  KER_L_USE               5
//#define  KER_L_EX                6
//#define  KER_L_ASSESSEE          7
//#define  KER_L_ACRES             8
//#define  KER_L_CITRUS            9
//#define  KER_L_AGPRESERVE        10
//#define  KER_L_MINERAL           11
//#define  KER_L_LAND              12
//#define  KER_L_IMPR              13
//#define  KER_L_OTHER_IMP         14
//#define  KER_L_PERSPROP          15
//#define  KER_L_EXEMPTIONS        16
//#define  KER_L_NETVALUE          17
//#define  KER_L_P8                18
//#define  KER_L_P8_BYVAG_BYV      19
//#define  KER_L_DIST              20
//#define  KER_L_BPS               21
//#define  KER_L_PERSPROPTYPE      22
//#define  KER_L_SITUS             23
//#define  KER_L_LEGALTYPE         24
//#define  KER_L_LEGAL             25
//#define  KER_L_CAREOF            26
//#define  KER_L_DBA               27
//#define  KER_L_MADDR1            28
//#define  KER_L_MADDR2            29
//#define  KER_L_MADDR3            30
//#define  KER_L_ZIPCODE           31
//#define  KER_L_BILLKEY           32
//#define  KER_L_BILLAMT           33
//#define  KER_L_TENO              34
//#define  KER_L_ASSESSE_NO        35
//#define  KER_L_AE_INFO1          36
//#define  KER_L_AE_INFO2          37
//#define  KER_L_AE_INFO3          38
//#define  KER_L_PRIOR_LD_VALUE    39

#define  ROLL_ATN                   0
#define  ROLL_TENO                  1
#define  ROLL_OWNERNAME             2
#define  ROLL_APN                   3
#define  ROLL_USECODE               4
#define  ROLL_AGPRESERVEDCODE       5     // Y/N
#define  ROLL_LEGALACRE             6
#define  ROLL_LEGALTYPECODE         7
#define  ROLL_CITYABBREVIATION      8
#define  ROLL_BLOCK                 9
#define  ROLL_CITYLOT               10
#define  ROLL_PARCELMAP             11
#define  ROLL_PARCELLOT             12
#define  ROLL_SECTION               13
#define  ROLL_TOWNSHIP              14
#define  ROLL_RANGE                 15
#define  ROLL_QUARTER               16
#define  ROLL_TRACTMAP              17
#define  ROLL_TRACTBLOCK            18
#define  ROLL_TRACTLOT              19
#define  ROLL_ISOILGASPRODUCING     20
#define  ROLL_UNFORMAT_LEGAL        21
#define  ROLL_ROLLTYPECODE          22
/*
#define  ROLL_TAXYEARROLL           22
#define  ROLL_TRAFORMATTED          23
#define  ROLL_TAXBILLNUMBER         24
#define  ROLL_LANDVAL               25
#define  ROLL_MINERALVAL            26
#define  ROLL_OTHERIMPFIXTUREVAL    27
#define  ROLL_IMPVAL                28
#define  ROLL_PERSONALPROPVAL       29
#define  ROLL_EXEMPTVAL             30
#define  ROLL_DATEBILLED            31
#define  ROLL_DATEBILLCANCELED      32
#define  ROLL_BILLAMOUNT            33
#define  ROLL_GENERALTAXAMTDUE      34
#define  ROLL_SA_AMTDUE             35
#define  ROLL_SA_AND_TR_AMTDUE      36
#define  ROLL_SUPPFULLYEARAMOUNT    37
#define  ROLL_TAXRATEGENERAL        38
#define  ROLL_TAXRATESPECIAL        39
#define  ROLL_ROLLCORRSTATUSCODE    40
#define  ROLL_DATEDELQFIRST         41
#define  ROLL_DATEDELQSECOND        42
#define  ROLL_DATEPAIDFIRST         43
#define  ROLL_DATEPAIDSECOND        44
#define  ROLL_INSTDUEFIRST          45
#define  ROLL_INSTDUESECOND         46
#define  ROLL_PENDUEFIRST           47
#define  ROLL_PENDUESECOND          48
#define  ROLL_AMTPAIDFIRST          49
#define  ROLL_AMTPAIDSECOND         50
#define  ROLL_BILLSTATUSCODE        51
#define  ROLL_BILLSUBTYPECODE       52
*/

#define  VALUE_ATN                  0
#define  VALUE_TENO                 1
#define  VALUE_TAXYEARROLL          2
#define  VALUE_TRAFORMATTED         3
#define  VALUE_TAXBILLNUMBER        4
#define  VALUE_LANDVAL              5
#define  VALUE_MINERALVAL           6
#define  VALUE_OTHERIMPFIXTUREVAL   7
#define  VALUE_IMPVAL               8
#define  VALUE_PERSONALPROPVAL      9
#define  VALUE_EXEMPTVAL            10
#define  VALUE_DATEBILLED           11
#define  VALUE_DATEBILLCANCELED     12
#define  VALUE_BILLAMOUNT           13
#define  VALUE_GENERALTAXAMTDUE     14
#define  VALUE_SA_AMTDUE            15
#define  VALUE_SA_AND_TR_AMTDUE     16
#define  VALUE_SUPPFULLYEARAMOUNT   17
#define  VALUE_TAXRATEGENERAL       18
#define  VALUE_TAXRATESPECIAL       19
#define  VALUE_ROLLCORRSTATUSCODE   20
#define  VALUE_DATEDELQFIRST        21
#define  VALUE_DATEDELQSECOND       22
#define  VALUE_DATEPAIDFIRST        23
#define  VALUE_DATEPAIDSECOND       24
#define  VALUE_INSTDUEFIRST         25
#define  VALUE_INSTDUESECOND        26
#define  VALUE_PENDUEFIRST          27
#define  VALUE_PENDUESECOND         28
#define  VALUE_AMTPAIDFIRST         29
#define  VALUE_AMTPAIDSECOND        30
#define  VALUE_BILLSTATUSCODE       31
#define  VALUE_BILLSUBTYPECODE      32
   
#define  SALE_ATN                   0
#define  SALE_TENO                  1
#define  SALE_TAXENTITYID           2
#define  SALE_BUYER                 3
#define  SALE_SELLER                4
#define  SALE_DOCNUM                5
#define  SALE_DOCDATE               6
#define  SALE_DOCTAX                7
#define  SALE_DOCTYPE               8
#define  SALE_DOCDESC               9
#define  SALE_SALEPRICE             10
#define  SALE_PCTXFER               11
#define  SALE_MULTIPARCELS          12
#define  SALE_MULTIBUYERS           13
#define  SALE_MULTISELLERS          14

#define  SITUS_ATN                  0
#define  SITUS_TENO                 1
#define  SITUS_STRNO                2
#define  SITUS_STRFRA               3
#define  SITUS_STRDIR               4
#define  SITUS_STRNAME              5
#define  SITUS_STRSFX               6
#define  SITUS_UNITTYPE             7
#define  SITUS_UNITNO               8
#define  SITUS_ADDR1                9
#define  SITUS_CITY                 10

#define  MAIL_ATN                   0
#define  MAIL_TENO                  1
#define  MAIL_MAIL_ID               2
#define  MAIL_DTPRIMEOWNER          3
#define  MAIL_CAREOF                4
#define  MAIL_ADDR1                 5
#define  MAIL_ADDR2                 6
#define  MAIL_ISFREEFORM            7
#define  MAIL_STRNO                 8
#define  MAIL_STRFRA                9
#define  MAIL_STRNAME               10
#define  MAIL_STRDIR                11
#define  MAIL_STRSFX                12
#define  MAIL_UNITTYPE              13
#define  MAIL_UNITNO                14
#define  MAIL_CITY                  15
#define  MAIL_STATE                 16
#define  MAIL_ZIPCODE               17
#define  MAIL_LINE1                 18
#define  MAIL_LINE2                 19
#define  MAIL_LINE3                 20
#define  MAIL_LINE4                 21

#define  BLDG_ATN                   0
#define  BLDG_TENO                  1
#define  BLDG_CHAR_ID               2
#define  BLDG_FLDNAME               3
#define  BLDG_FLDVALUE              4


#define  LOFF_TE_NO              1  -1 
#define  LOFF_ASSESSEE_NO        11 -1 
#define  LOFF_TRA_NO             18 -1
#define  LOFF_ATN                24 -1
#define  LOFF_FILE_NO            35 -1
#define  LOFF_USE_CODE           48 -1
#define  LOFF_EXMPT_TYPE_CODE    52 -1
#define  LOFF_OWNER              54 -1
#define  LOFF_MADDR1             98 -1
#define  LOFF_MADDR2             138-1
#define  LOFF_MZIP               178-1
#define  LOFF_LAND_ACRES         187-1
#define  LOFF_LAND_PET_ACRES     194-1
#define  LOFF_MINERAL_VALUE      201-1
#define  LOFF_LAND_VALUE         211-1
#define  LOFF_IMP_VALUE          221-1
#define  LOFF_OTH_IMP_VALUE      231-1
#define  LOFF_PERS_PROP_VAL      241-1
#define  LOFF_EXE_VALUE          251-1
#define  LOFF_NET_VALUE          261-1
#define  LOFF_ROLL_TYPE_CODE     271-1
#define  LOFF_SUPERV_DIST_CODE   272-1
#define  LOFF_SITUS_ADDR         273-1
#define  LOFF_LEGAL              333-1
#define  LOFF_CAREOF             393-1
#define  LOFF_DBA                433-1
#define  LOFF_AG_PRESERVE_CD     483-1
#define  LOFF_MADDR3             498-1
#define  LOFF_FILLER             528-1

#define  LSIZ_TE_NO              10
#define  LSIZ_ASSESSEE_NO        7
#define  LSIZ_TRA_NO             6
#define  LSIZ_ATN                11
#define  LSIZ_FILE_NO            13
#define  LSIZ_USE_CODE           4
#define  LSIZ_EXMPT_TYPE_CODE    2
#define  LSIZ_OWNER              44
#define  LSIZ_MADDR1             40
#define  LSIZ_MADDR2             40
#define  LSIZ_MZIP               9
#define  LSIZ_LAND_ACRES         7
#define  LSIZ_LAND_PET_ACRES     7
#define  LSIZ_MINERAL_VALUE      10
#define  LSIZ_LAND_VALUE         10
#define  LSIZ_IMP_VALUE          10
#define  LSIZ_OTH_IMP_VALUE      10
#define  LSIZ_PERS_PROP_VAL      10
#define  LSIZ_EXE_VALUE          10
#define  LSIZ_NET_VALUE          10
#define  LSIZ_ROLL_TYPE_CODE     1
#define  LSIZ_SUPERV_DIST_CODE   1
#define  LSIZ_SITUS_ADDR         60
#define  LSIZ_LEGAL              60
#define  LSIZ_CAREOF             40
#define  LSIZ_DBA                50
#define  LSIZ_AG_PRESERVE_CD     15
#define  LSIZ_MADDR3             30
#define  LSIZ_FILLER             11

typedef  struct _tKer_Lien
{
   char Te_No[LSIZ_TE_NO];
   char Assessee_No[LSIZ_ASSESSEE_NO];
   char TRA[LSIZ_TRA_NO];
   char Atn[LSIZ_ATN];
   char FileNo[LSIZ_FILE_NO];
   char UseCode[LSIZ_USE_CODE];
   char Exmpt_Type_Code[LSIZ_EXMPT_TYPE_CODE];
   char OwnerName[LSIZ_OWNER];
   char M_Addr1[LSIZ_MADDR1];
   char M_Addr2[LSIZ_MADDR2];
   char M_Zip[LSIZ_MZIP];
   char Land_Acres[LSIZ_LAND_ACRES];
   char Land_Pet_Acres[LSIZ_LAND_ACRES];
   char Mineral[LSIZ_MINERAL_VALUE];
   char Land[LSIZ_LAND_VALUE];
   char Impr[LSIZ_IMP_VALUE];
   char Fixt[LSIZ_OTH_IMP_VALUE];
   char PP_Val[LSIZ_PERS_PROP_VAL];
   char Exe_Amt[LSIZ_EXE_VALUE];
   char Net_Val[LSIZ_NET_VALUE];
   char RollType[LSIZ_ROLL_TYPE_CODE];
   char Superv_Dist_No[LSIZ_SUPERV_DIST_CODE];
   char Situs[LSIZ_SITUS_ADDR];
   char Legal[LSIZ_LEGAL];
   char CareOf[LSIZ_CAREOF];
   char Dba[LSIZ_DBA];
   char AgPreserve[LSIZ_AG_PRESERVE_CD];
   char M_Addr3[LSIZ_MADDR3];
   char Filler[LSIZ_FILLER];
   char CrLf[2];
} KER_LIEN;

#define  ROFF_ATN                1-1
#define  ROFF_FILE_NO            12-1
#define  ROFF_APN                25-1
#define  ROFF_TE_NO              34-1
#define  ROFF_NAME_LIENHOLDER    44-1
#define  ROFF_ROLL_TYPE_CODE     64-1
#define  ROFF_STATUS_CODE        65-1
#define  ROFF_USE_CODE           67-1
#define  ROFF_OWNER              71-1
#define  ROFF_CAREOF             115-1
#define  ROFF_NAMETYPE           159-1
#define  ROFF_LAND_VALUE         160-1
#define  ROFF_MINERAL_VALUE      171-1
#define  ROFF_IMP_VALUE          182-1
#define  ROFF_OI_FIX_VALUE       193-1
#define  ROFF_PERS_PROP_VAL      204-1
#define  ROFF_EXMPT_VAL_1        215-1
#define  ROFF_EXMPT_TYPE_1       226-1
#define  ROFF_EXMPT_VAL_2        228-1
#define  ROFF_EXMPT_TYPE_2       239-1
#define  ROFF_EXMPT_VAL_3        241-1
#define  ROFF_EXMPT_TYPE_3       252-1
#define  ROFF_TRA_NO             254-1
#define  ROFF_AG_PRESERVE_CD     260-1
#define  ROFF_LN_PENALTY_IND     261-1
#define  ROFF_DT_PROP_8          262-1
#define  ROFF_DT_VAL_UPDT        270-1
#define  ROFF_EVT_TYPE_CD_CHG    278-1
#define  ROFF_FREE_FORM_IND      280-1
#define  ROFF_STR_NO             281-1
#define  ROFF_STR_NO_FRACTION    287-1
#define  ROFF_STR_NAME           288-1
#define  ROFF_STR_DIR            308-1
#define  ROFF_STR_TYPE_CODE      310-1
#define  ROFF_CITY               312-1
#define  ROFF_STATE_CODE         327-1
#define  ROFF_ZIP_CODE           329-1
#define  ROFF_AD_UNIT_TYPE       338-1
#define  ROFF_AD_UNIT_ID         341-1
#define  ROFF_RDMP_STATUS        345-1
#define  ROFF_FUTURE_OWN_IND     346-1
#define  ROFF_PHONE_NO           347-1
#define  ROFF_LAND_VAL_BASE      357-1
#define  ROFF_MIN_VAL_BASE       368-1
#define  ROFF_IMP_VAL_BASE       379-1
#define  ROFF_FIX_VAL_BASE       390-1
#define  ROFF_PP_VAL_BASE        401-1
#define  FILLER                  412-1

#define  RSIZ_ATN                11
#define  RSIZ_FILE_NO            13
#define  RSIZ_APN                9
#define  RSIZ_TE_NO              10
#define  RSIZ_NAME_LIENHOLDER    20
#define  RSIZ_ROLL_TYPE_CODE     1
#define  RSIZ_STATUS_CODE        2
#define  RSIZ_USE_CODE           4
#define  RSIZ_OWNER              44
#define  RSIZ_CAREOF             44
#define  RSIZ_NAMETYPE           1

#define  RSIZ_LAND               11
#define  RSIZ_MINERAL            11
#define  RSIZ_IMPR               11
#define  RSIZ_FIXT               11
#define  RSIZ_PP_VAL             11
#define  RSIZ_EXMPT_VAL          11
#define  RSIZ_EXMPT_TYPE         2
#define  RSIZ_LIEN_VAL           (RSIZ_LAND*8)+(RSIZ_EXMPT_TYPE*3)

#define  RSIZ_TRA                6
#define  RSIZ_AG_PRESERVE_CD     1
#define  RSIZ_LN_PENALTY_IND     1
#define  RSIZ_DT_PROP_8          8
#define  RSIZ_DT_VAL_UPDT        8
#define  RSIZ_EVT_TYPE_CD_CHG    2
#define  RSIZ_FREE_FORM_IND      1
#define  RSIZ_STR_NO             6
#define  RSIZ_STR_NO_FRACTION    1
#define  RSIZ_STR_NAME           20
#define  RSIZ_STR_DIR            2
#define  RSIZ_STR_TYPE_CODE      2
#define  RSIZ_CITY               15
#define  RSIZ_STATE_CODE         2
#define  RSIZ_ZIP_CODE           9
#define  RSIZ_AD_UNIT_TYPE       3
#define  RSIZ_AD_UNIT_ID         4
#define  RSIZ_RDMP_STATUS        1
#define  RSIZ_FUTURE_OWN_IND     1
#define  RSIZ_PHONE_NO           10
#define  RSIZ_LAND_VAL_BASE      11
#define  RSIZ_MIN_VAL_BASE       11
#define  RSIZ_IMP_VAL_BASE       11
#define  RSIZ_FIX_VAL_BASE       11
#define  RSIZ_PP_VAL_BASE        11
#define  RSIZ_FILLER             9

typedef  struct _tKer_Roll
{
   char Atn[RSIZ_ATN];
   char FileNo[RSIZ_FILE_NO];
   char AltApn[RSIZ_APN];
   char Te_No[RSIZ_TE_NO];
   char LienOwner[RSIZ_NAME_LIENHOLDER];
   char RollType[RSIZ_ROLL_TYPE_CODE];
   char ParcelStatus[RSIZ_STATUS_CODE];
   char UseCode[RSIZ_USE_CODE];
   char OwnerName[RSIZ_OWNER];
   char CareOf[RSIZ_CAREOF];
   char NameType;
   char Land[RSIZ_LAND];
   char Mineral[RSIZ_MINERAL];
   char Impr[RSIZ_IMPR];
   char Fixt[RSIZ_FIXT];
   char PP_Val[RSIZ_PP_VAL];
   char Exe_Amt[RSIZ_EXMPT_VAL];
   char Exe_Code1[RSIZ_EXMPT_TYPE];
   char Exe_Amt1[RSIZ_EXMPT_VAL];
   char Exe_Code2[RSIZ_EXMPT_TYPE];
   char Exe_Amt2[RSIZ_EXMPT_VAL];
   char Exe_Code3[RSIZ_EXMPT_TYPE];
   char TRA[RSIZ_TRA];
   char AgPreserve;
   char Penalty_Flg;
   char Prop8_Date[RSIZ_DT_PROP_8];
   char ValUpdt_Date[RSIZ_DT_VAL_UPDT];
   char EventType[RSIZ_EVT_TYPE_CD_CHG];
   char MAdr_Flg;             // Y=free form on UFAR.DSS
   char M_StrNum[RSIZ_STR_NO];
   char M_StrFra[RSIZ_STR_NO_FRACTION];
   char M_StrName[RSIZ_STR_NAME];
   char M_StrDir[RSIZ_STR_DIR];
   char M_StrSfx[RSIZ_STR_TYPE_CODE];
   char M_City[RSIZ_CITY];
   char M_St[RSIZ_STATE_CODE];
   char M_Zip[RSIZ_ZIP_CODE];
   char M_UnitType[RSIZ_AD_UNIT_TYPE];
   char M_UnitNo[RSIZ_AD_UNIT_ID];
   char Rdmp_Status;
   char Future_Own_Ind;
   char Phone[RSIZ_PHONE_NO];
   char Base_Land[RSIZ_LAND_VAL_BASE];
   char Base_Mineral[RSIZ_MIN_VAL_BASE];
   char Base_Impr[RSIZ_IMP_VAL_BASE];
   char Base_Fixt[RSIZ_FIX_VAL_BASE];
   char Base_Pers[RSIZ_PP_VAL_BASE];
   char Filler[RSIZ_FILLER];
} KER_ROLL;

/*
typedef struct _t_KerLien
{
   char Te_No[RSIZ_TE_NO];
   char Land[RSIZ_LAND];
   char Mineral[RSIZ_MINERAL];
   char Impr[RSIZ_IMPR];
   char Fixt[RSIZ_FIXT];
   char PP_Val[RSIZ_PP_VAL];
   char Exe_Amt[RSIZ_EXMPT_VAL];
   char Exe_Code1[RSIZ_EXMPT_TYPE];
   char Exe_Amt1[RSIZ_EXMPT_VAL];
   char Exe_Code2[RSIZ_EXMPT_TYPE];
   char Exe_Amt2[RSIZ_EXMPT_VAL];
   char Exe_Code3[RSIZ_EXMPT_TYPE];
   char Apn[SIZ_APN_S];
   char CRLF[2];
} KER_LIEN;
*/
#define  CHAR_ATN                0
#define  CHAR_ENTITY_NO          1
#define  CHAR_DESIGN             2
#define  CHAR_QC_CNSTR           3
#define  CHAR_QC_RATING          4
#define  CHAR_QC_SHAPE           5
#define  CHAR_CONDITION          6
#define  CHAR_YEAR_BLT           7
#define  CHAR_YEAR_EFF           8
#define  CHAR_SF_1ST_FL          9
#define  CHAR_SF_2ND_FL          10
#define  CHAR_SF_ABV_2ND         11
#define  CHAR_SF_BSMNT           12
#define  CHAR_SF_PORCH           13
#define  CHAR_SF_GARAGE          14
#define  CHAR_SF_GARAGE2         15
#define  CHAR_SF_PATIO           16
#define  CHAR_SF_CARPORT         17
#define  CHAR_SF_ADDITION        18
#define  CHAR_SF_TOTAL           19
#define  CHAR_AM_2ND_FL          20
#define  CHAR_AM_ABV_2ND         21
#define  CHAR_AM_BSMNT           22
#define  CHAR_AM_PORCH           23
#define  CHAR_AM_GARAGE          24
#define  CHAR_AM_GARAGE2         25
#define  CHAR_AM_PATIO           26
#define  CHAR_AM_CARPORT         27
#define  CHAR_AM_ADDITION        28
#define  CHAR_CV_ADDITION        29
#define  CHAR_CV_PORCH           30
#define  CHAR_CV_PATIO           31
#define  CHAR_CV_GARAGE          32
#define  CHAR_CV_GARAGE2         33
#define  CHAR_DET_GARAGE         34
#define  CHAR_DET_GARAGE2        35
#define  CHAR_ADD_DATE           36
#define  CHAR_ADD_FACTOR         37
#define  CHAR_FMLY_ROOM          38
#define  CHAR_DINING_ROOM        39
#define  CHAR_STORIES            40
#define  CHAR_BEDROOMS           41
#define  CHAR_BATHS              42
#define  CHAR_FIREPLACES         43
#define  CHAR_AIRCOND            44
#define  CHAR_POOL               45
#define  CHAR_SPA                46
#define  CHAR_LST_INSP_DATE      47
#define  CHAR_COMMENTS           48
#define  CHAR_INSERTED_DATE      49
#define  CHAR_UPDATED_DATE       50
#define  CHAR_EMP_INSERTED       51
#define  CHAR_EMP_UPDATED        52
#define  CHAR_SEQNUM             53

#define  COFF_ATN                1
#define  COFF_ENTITY_NO          11
#define  COFF_DESIGN             13
#define  COFF_QC_CNSTR           14
#define  COFF_QC_RATING          15
#define  COFF_QC_SHAPE           21
#define  COFF_CONDITION          22
#define  COFF_YEAR_BLT           23
#define  COFF_YEAR_EFF           27
#define  COFF_SF_1ST_FL          31
#define  COFF_SF_2ND_FL          41
#define  COFF_SF_ABV_2ND         51
#define  COFF_SF_BSMNT           61
#define  COFF_SF_PORCH           71
#define  COFF_SF_GARAGE          81
#define  COFF_SF_GARAGE2         91
#define  COFF_SF_PATIO           101
#define  COFF_SF_CARPORT         111
#define  COFF_SF_ADDITION        121
#define  COFF_SF_TOTAL           131
#define  COFF_AM_2ND_FL          141
#define  COFF_AM_ABV_2ND         142
#define  COFF_AM_BSMNT           143
#define  COFF_AM_PORCH           144
#define  COFF_AM_GARAGE          145
#define  COFF_AM_GARAGE2         146
#define  COFF_AM_PATIO           147
#define  COFF_AM_CARPORT         148
#define  COFF_AM_ADDITION        149
#define  COFF_CV_ADDITION        150
#define  COFF_CV_PORCH           151
#define  COFF_CV_PATIO           152
#define  COFF_CV_GARAGE          153
#define  COFF_CV_GARAGE2         154
#define  COFF_DET_GARAGE         155
#define  COFF_DET_GARAGE2        156
#define  COFF_ADD_DATE           157
#define  COFF_ADD_FACTOR         165
#define  COFF_FMLY_ROOM          173
#define  COFF_DINING_ROOM        174
#define  COFF_STORIES            175
#define  COFF_BEDROOMS           179
#define  COFF_BATHS              183
#define  COFF_FIREPLACES         189
#define  COFF_AIRCOND            193
#define  COFF_POOL               194
#define  COFF_SPA                195
#define  COFF_LST_INSP_DATE      196

#define  CSIZ_ATN                10
#define  CSIZ_ENTITY_NO          2
#define  CSIZ_DESIGN             1
#define  CSIZ_QC_CNSTR           1
#define  CSIZ_QC_RATING          6
#define  CSIZ_QC_SHAPE           1
#define  CSIZ_CONDITION          1
#define  CSIZ_YEAR               4
#define  CSIZ_SQFT               10
#define  CSIZ_AM_2ND_FL          1
#define  CSIZ_AM_ABV_2ND         1
#define  CSIZ_AM_BSMNT           1
#define  CSIZ_AM_PORCH           1
#define  CSIZ_AM_GARAGE          1
#define  CSIZ_AM_GARAGE2         1
#define  CSIZ_AM_PATIO           1
#define  CSIZ_AM_CARPORT         1
#define  CSIZ_AM_ADDITION        1
#define  CSIZ_CV_ADDITION        1
#define  CSIZ_CV_PORCH           1
#define  CSIZ_CV_PATIO           1
#define  CSIZ_CV_GARAGE          1
#define  CSIZ_CV_GARAGE2         1
#define  CSIZ_DET_GARAGE         1
#define  CSIZ_DET_GARAGE2        1
#define  CSIZ_DATE               8
#define  CSIZ_ADD_FACTOR         8
#define  CSIZ_FMLY_ROOM          1
#define  CSIZ_DINING_ROOM        1
#define  CSIZ_STORIES            4
#define  CSIZ_BEDROOMS           4
#define  CSIZ_BATHS              6
#define  CSIZ_FIREPLACES         4
#define  CSIZ_AIRCOND            1
#define  CSIZ_POOL               1
#define  CSIZ_SPA                1

typedef struct _tKerChar
{
   char Atn            [CSIZ_ATN];
   char Entity_No      [CSIZ_ENTITY_NO];
   char Design         [CSIZ_DESIGN];
   char Qc_Cnstr       [CSIZ_QC_CNSTR];
   char Qc_Rating      [CSIZ_QC_RATING];
   char Qc_Shape       [CSIZ_QC_SHAPE];
   char Condition      [CSIZ_CONDITION];
   char Year_Blt       [CSIZ_YEAR];
   char Year_Eff       [CSIZ_YEAR];
   char Sf_1St_Fl      [CSIZ_SQFT];
   char Sf_2Nd_Fl      [CSIZ_SQFT];
   char Sf_Abv_2Nd     [CSIZ_SQFT];
   char Sf_Bsmnt       [CSIZ_SQFT];
   char Sf_Porch       [CSIZ_SQFT];
   char Sf_Garage      [CSIZ_SQFT];
   char Sf_Garage2     [CSIZ_SQFT];
   char Sf_Patio       [CSIZ_SQFT];
   char Sf_Carport     [CSIZ_SQFT];
   char Sf_Addition    [CSIZ_SQFT];
   char Sf_Total       [CSIZ_SQFT];
   char Am_2Nd_Fl;
   char Am_Abv_2nd;
   char Am_Bsmnt;
   char Am_Porch;
   char Am_Garage;
   char Am_Garage2;
   char Am_Patio;
   char Am_Carport;
   char Am_Addition;
   char Cv_Addition;
   char Cv_Porch;
   char Cv_Patio;
   char Cv_Garage;
   char Cv_Garage2;
   char Det_Garage;
   char Det_Garage2;
   char Add_Date       [CSIZ_DATE];
   char Add_Factor     [CSIZ_ADD_FACTOR];
   char Fmly_Room;
   char Dining_Room;
   char Stories        [CSIZ_STORIES];
   char Bedrooms       [CSIZ_BEDROOMS];
   char Baths          [CSIZ_BATHS];
   char Fireplaces     [CSIZ_FIREPLACES];
   char Aircond;
   char Pool;
   char Spa;
   char isComm;
   char isLodging;
   char isRes;

   char Lst_Insp_Date  [CSIZ_DATE];
   char CRLF[2];
} KER_CHAR;


/*
typedef struct _tKerCval
{
   char Atn            [CSIZ_ATN];
   char Entity_No      [CSIZ_ENTITY_NO];
   char Design         [CSIZ_DESIGN];
   char Qc_Cnstr       [CSIZ_QC_CNSTR];
   char Qc_Rating      [CSIZ_QC_RATING];
   char Qc_Shape       [CSIZ_QC_SHAPE];
   char Condition      [CSIZ_CONDITION];
   char Year_Blt       [CSIZ_YEAR_BLT];
   char Year_Eff       [CSIZ_YEAR_EFF];
   char Sf_1St_Fl      [CSIZ_SF_1ST_FL];
   char Sf_2Nd_Fl      [CSIZ_SF_2ND_FL];
   char Sf_Abv_2Nd     [CSIZ_SF_ABV_2ND];
   char Sf_Bsmnt       [CSIZ_SF_BSMNT];
   char Sf_Porch       [CSIZ_SF_PORCH];
   char Sf_Garage      [CSIZ_SF_GARAGE];
   char Sf_Garage2     [CSIZ_SF_GARAGE2];
   char Sf_Patio       [CSIZ_SF_PATIO];
   char Sf_Carport     [CSIZ_SF_CARPORT];
   char Sf_Addition    [CSIZ_SF_ADDITION];
   char Sf_Total       [CSIZ_SF_TOTAL];
   char Am_2Nd_Fl;
   char Am_Abv_2nd;
   char Am_Bsmnt;
   char Am_Porch;
   char Am_Garage;
   char Am_Garage2;
   char Am_Patio;
   char Am_Carport;
   char Am_Addition;
   char Cv_Addition;
   char Cv_Porch;
   char Cv_Patio;
   char Cv_Garage;
   char Cv_Garage2;
   char Det_Garage;
   char Det_Garage2;
   char Add_Date       [CSIZ_DATE];
   char Add_Factor     [CSIZ_ADD_FACTOR];
   char Fmly_Room;
   char Dining_Room;
   char Stories        [CSIZ_STORIES];
   char Bedrooms       [CSIZ_BEDROOMS];
   char Baths          [CSIZ_BATHS];
   char Fireplaces     [CSIZ_FIREPLACES];
   char Aircond;
   char Pool;
   char Spa;
   char Lst_Insp_Date  [CSIZ_DATE];
   char CRLF[2];
} KER_CVAL;
*/
#define  LGL_TE_NO               10
#define  LGL_LEGAL_ACRE          8
#define  LGL_PRODUCING_IND       1
#define  LGL_LEGAL_TYPE          2
#define  LGL_LEGAL_FORMAT        13
#define  LGL_UNFORM_DESC         72
#define  LGL_FILLER              45

typedef struct _t_Legal
{
   char  Te_No[LGL_TE_NO];
   char  Acres[LGL_LEGAL_ACRE];
   char  Producing_Ind;
   char  LegalType[LGL_LEGAL_TYPE];
   char  LegalFormat[LGL_LEGAL_FORMAT];
   char  Desc[LGL_UNFORM_DESC];
   char  Filler[LGL_FILLER];
} KER_LEGAL;

#define  SIT_TE_NO               10
#define  SIT_STR_NO              6
#define  SIT_STR_FRACT           1
#define  SIT_STR_NAME            20
#define  SIT_STR_DIR             2
#define  SIT_STR_SFX             2
#define  SIT_CITY                15
#define  SIT_AD_UNIT_TYPE        3
#define  SIT_AD_UNIT_ID          4
#define  SIT_STATE               2
#define  SIT_ZIP_CODE            9

typedef struct _t_Situs
{
   char  Te_No[SIT_TE_NO];
   char  StrNum[SIT_STR_NO];
   char  StrFra[SIT_STR_FRACT];
   char  StrName[SIT_STR_NAME];
   char  StrDir[SIT_STR_DIR];
   char  StrSfx[SIT_STR_SFX];
   char  City[SIT_CITY];
   char  UnitType[SIT_AD_UNIT_TYPE];
   char  UnitNo[SIT_AD_UNIT_ID];
   char  State[SIT_STATE];
   char  ZipCode[SIT_ZIP_CODE];
} KER_SITUS;

#define  MAIL_TE_NO              10
#define  MAIL_ADDR_LINE1         30
#define  MAIL_ADDR_LINE2         30
#define  MAIL_ADDR_LINE3         30
#define  MAIL_COUNTRY_CDE        2

typedef struct _t_MailAdr
{
   char  Te_No[MAIL_TE_NO];
   char  Addr1[MAIL_ADDR_LINE1];
   char  Addr2[MAIL_ADDR_LINE1];
   char  Addr3[MAIL_ADDR_LINE1];
   char  CountyCode[MAIL_COUNTRY_CDE];
} KER_MADR;

#define  SOFF_TE_NO              1  -1
#define  SOFF_EVENT_NO           11 -1
#define  SOFF_EVENT_TYPE         21 -1
#define  SOFF_REC_DOCNO          23 -1
#define  SOFF_PROCESSED_CODE     32 -1
#define  SOFF_DOC_FEEAMT         34 -1
#define  SOFF_DOC_TYPE           42 -1
#define  SOFF_DOC_VALUE          45 -1
#define  SOFF_REC_DATE           55 -1
#define  SOFF_REC_BKPG           63 -1
#define  SOFF_DOCTAX             72 -1
#define  SOFF_PCOR_AMT           81 -1
#define  SOFF_ASSMT_AMT          92 -1
#define  SOFF_PCOR_RECEIVED      104-1
#define  SOFF_TRANSFEROR         105-1
#define  SOFF_TRANSFEREE         149-1
#define  SOFF_MULTI_TRANSFEREES  193-1
#define  SOFF_MULTI_TRANSFERORS  194-1
#define  SOFF_MULTI_PARCEL_SALE  195-1
#define  SOFF_TRANSFER_DATE      196-1
#define  SOFF_DEATH_DATE         204-1
#define  SOFF_LEASE_START_DATE   212-1
#define  SOFF_LEASE_TERM_YEARS   220-1
#define  SOFF_LEASE_YEARS_REM    223-1
#define  SOFF_FULL_PART          226-1
#define  SOFF_PCT_TRANSFER       227-1
#define  SOFF_TRANSFER_TYPE      233-1
#define  SOFF_APN                234-1
#define  SOFF_CRLF               243-1

#define  SSIZ_TE_NO              10
#define  SSIZ_EVENT_NO           10
#define  SSIZ_EVENT_TYPE         2 
#define  SSIZ_REC_DOCNO          9 
#define  SSIZ_PROCESSED_CODE     2 
#define  SSIZ_DOC_FEEAMT         8 
#define  SSIZ_DOC_TYPE           3 
#define  SSIZ_DOC_VALUE          10
#define  SSIZ_REC_DATE           8 
#define  SSIZ_REC_BKPG           9 
#define  SSIZ_DOCTAX             9 
#define  SSIZ_PCOR_AMT           11
#define  SSIZ_ASSMT_AMT          12
#define  SSIZ_PCOR_RECEIVED      1 
#define  SSIZ_TRANSFEROR         44
#define  SSIZ_TRANSFEREE         44
#define  SSIZ_MULTI_TRANSFEREES  1 
#define  SSIZ_MULTI_TRANSFERORS  1 
#define  SSIZ_MULTI_PARCEL_SALE  1 
#define  SSIZ_TRANSFER_DATE      8 
#define  SSIZ_DEATH_DATE         8 
#define  SSIZ_LEASE_START_DATE   8 
#define  SSIZ_LEASE_TERM_YEARS   3 
#define  SSIZ_LEASE_YEARS_REM    3 
#define  SSIZ_FULL_PART          1 
#define  SSIZ_PCT_TRANSFER       6 
#define  SSIZ_TRANSFER_TYPE      1 
#define  SSIZ_APN                9 

typedef struct _t_KerSale
{  // 242 bytes
   char Te_No            [SSIZ_TE_NO];
   char Event_No         [SSIZ_EVENT_NO];
   char Event_Type       [SSIZ_EVENT_TYPE];
   char Rec_Docno        [SSIZ_REC_DOCNO];      // Same as Rec_BkPg
   char Processed_Code   [SSIZ_PROCESSED_CODE];
   char Doc_FeeAmt       [SSIZ_DOC_FEEAMT];
   char Doc_Type         [SSIZ_DOC_TYPE];
   char Doc_Value        [SSIZ_DOC_VALUE];
   char Rec_Date         [SSIZ_REC_DATE];
   char Rec_BkPg         [SSIZ_REC_BKPG];
   char Doctax           [SSIZ_DOCTAX];
   char Pcor_Amt         [SSIZ_PCOR_AMT];
   char Assmt_Amt        [SSIZ_ASSMT_AMT];
   char Pcor_Received;
   char Transferor       [SSIZ_TRANSFEROR];
   char Transferee       [SSIZ_TRANSFEREE];
   char Multi_Transferees;
   char Multi_Transferors;
   char Multi_Parcel_Sale;
   char Transfer_Date    [SSIZ_TRANSFER_DATE];
   char Death_Date       [SSIZ_DEATH_DATE];
   char Lease_Start_Date [SSIZ_LEASE_START_DATE];
   char Lease_Term_Years [SSIZ_LEASE_TERM_YEARS];
   char Lease_Years_Rem  [SSIZ_LEASE_YEARS_REM];
   char Full_Part;
   char Pct_Transfer     [SSIZ_PCT_TRANSFER];
   char Transfer_Type    [SSIZ_TRANSFER_TYPE];
   char Apn              [SSIZ_APN];
   char CRLF[2];
} KER_SALE;

/* 305-byte format
#define  SSIZ_TE_NO              10
#define  SSIZ_UNKNOWN_1          10
#define  SSIZ_EVENT_NO           10
#define  SSIZ_UNKNOWN_2          10
#define  SSIZ_EVENT_TYPE         2 
#define  SSIZ_REC_DOCNO          9 
#define  SSIZ_PROCESSED_CODE     2 
#define  SSIZ_DOC_FEE_AMT        8 
#define  SSIZ_UNKNOWN_3          12
#define  SSIZ_DOC_TYPE           3 
#define  SSIZ_DOC_VALUE          10
#define  SSIZ_UNKNOWN_4          10
#define  SSIZ_REC_DATE           8 
#define  SSIZ_REC_DOC            9 
#define  SSIZ_DOCTAX             9 
#define  SSIZ_PCOR_AMT           11
#define  SSIZ_UNKNOWN_5          9 
#define  SSIZ_ASSMT_AMT          12
#define  SSIZ_UNKNOWN_6          8 
#define  SSIZ_PCOR_RECEIVED      1 
#define  SSIZ_TRANSFEROR         44
#define  SSIZ_TRANSFEREE         44
#define  SSIZ_MULTI_TRANSFEREES  1 
#define  SSIZ_MULTI_TRANSFERORS  1 
#define  SSIZ_MULTI_PARCEL_SALE  1 
#define  SSIZ_TRANSFER_DATE      8 
#define  SSIZ_DEATH_DATE         8 
#define  SSIZ_LEASE_START_DATE   8 
#define  SSIZ_LEASE_TERM_YEARS   3 
#define  SSIZ_UNKNOWN_7          1 
#define  SSIZ_LEASE_YEARS_REM    3 
#define  SSIZ_UNKNOWN_8          1 
#define  SSIZ_FULL_PART          1 
#define  SSIZ_PCT_TRANSFER       6 
#define  SSIZ_TRANSFER_TYPE      1 
#define  SSIZ_APN                9 

#define  SOFF_TE_NO               1  -1
#define  SOFF_UNKNOWN_1           11 -1
#define  SOFF_EVENT_NO            21 -1
#define  SOFF_UNKNOWN_2           31 -1
#define  SOFF_EVENT_TYPE          41 -1
#define  SOFF_REC_DOCNO           43 -1
#define  SOFF_PROCESSED_CODE      52 -1
#define  SOFF_DOC_FEE_AMT         54 -1
#define  SOFF_UNKNOWN_3           62 -1
#define  SOFF_DOC_TYPE            74 -1
#define  SOFF_DOC_VALUE           77 -1
#define  SOFF_UNKNOWN_4           87 -1
#define  SOFF_REC_DATE            97 -1
#define  SOFF_REC_REC_DOC         105-1
#define  SOFF_DOCTAX              114-1
#define  SOFF_PCOR_AMT            123-1
#define  SOFF_UNKNOWN_5           134-1
#define  SOFF_ASSMT_AMT           143-1
#define  SOFF_UNKNOWN_6           155-1
#define  SOFF_PCOR_RECEIVED       163-1
#define  SOFF_TRANSFEROR          164-1
#define  SOFF_TRANSFEREE          208-1
#define  SOFF_MULTI_TRANSFEREES   252-1
#define  SOFF_MULTI_TRANSFERORS   253-1
#define  SOFF_MULTI_PARCEL_SALE   254-1
#define  SOFF_TRANSFER_DATE       255-1
#define  SOFF_DEATH_DATE          263-1
#define  SOFF_LEASE_START_DATE    271-1
#define  SOFF_LEASE_TERM_YEARS    279-1
#define  SOFF_UNKNOWN_7           282-1
#define  SOFF_LEASE_YEARS_REM     283-1
#define  SOFF_UNKNOWN_8           286-1
#define  SOFF_FULL_PART           287-1
#define  SOFF_PCT_TRANSFER        288-1
#define  SOFF_TRANSFER_TYPE       294-1
#define  SOFF_APN                 295-1
#define  SOFF_CRLF                304-1

typedef struct _t_KerSale
{  // 305 bytes
   char Te_No            [SSIZ_TE_NO];
   char Unknown_1        [SSIZ_UNKNOWN_1];
   char Event_No         [SSIZ_EVENT_NO];
   char Unknown_2        [SSIZ_UNKNOWN_2];
   char Event_Type       [SSIZ_EVENT_TYPE];
   char Rec_Docno        [SSIZ_REC_DOCNO];      // Same as Rec_Doc
   char Processed_Code   [SSIZ_PROCESSED_CODE];
   char Doc_Fee_Amt      [SSIZ_DOC_FEE_AMT];
   char Unknown_3        [SSIZ_UNKNOWN_3];
   char Doc_Type         [SSIZ_DOC_TYPE];
   char Doc_Value        [SSIZ_DOC_VALUE];
   char Unknown_4        [SSIZ_UNKNOWN_4];
   char Rec_Date         [SSIZ_REC_DATE];
   char Rec_Doc          [SSIZ_REC_DOC];
   char Doctax           [SSIZ_DOCTAX];
   char Pcor_Amt         [SSIZ_PCOR_AMT];
   char Unknown_5        [SSIZ_UNKNOWN_5];
   char Assmt_Amt        [SSIZ_ASSMT_AMT];
   char Unknown_6        [SSIZ_UNKNOWN_6];
   char Pcor_Received    [SSIZ_PCOR_RECEIVED];
   char Transferor       [SSIZ_TRANSFEROR];
   char Transferee       [SSIZ_TRANSFEREE];
   char Multi_Transferees[SSIZ_MULTI_TRANSFEREES];
   char Multi_Transferors[SSIZ_MULTI_TRANSFERORS];
   char Multi_Parcel_Sale[SSIZ_MULTI_PARCEL_SALE];
   char Transfer_Date    [SSIZ_TRANSFER_DATE];
   char Death_Date       [SSIZ_DEATH_DATE];
   char Lease_Start_Date [SSIZ_LEASE_START_DATE];
   char Lease_Term_Years [SSIZ_LEASE_TERM_YEARS];
   char Unknown_7        [SSIZ_UNKNOWN_7];
   char Lease_Years_Rem  [SSIZ_LEASE_YEARS_REM];
   char Unknown_8        [SSIZ_UNKNOWN_8];
   char Full_Part        [SSIZ_FULL_PART];
   char Pct_Transfer     [SSIZ_PCT_TRANSFER];
   char Transfer_Type    [SSIZ_TRANSFER_TYPE];
   char Apn              [SSIZ_APN];
   char CRLF[2];
} KER_SALE;
*/

// CHAR layout from sql extract file (Ker_Char.txt & Ker_Bldg.txt)
#define  CVAL_ATN                0
#define  CVAL_TE_NO              1
#define  CVAL_PARCELID           2
#define  CVAL_CHARID             3
#define  CVAL_FLDNAME            4
#define  CVAL_FLDVAL             5

#define  VSIZ_ATN                12
#define  VSIZ_TE_NO              10
#define  VSIZ_ID                 8
#define  VSIZ_BLDGNAME           60
#define  VSIZ_CONDITION          10
#define  VSIZ_ZONING             24
#define  VSIZ_YEAR               4
#define  VSIZ_DATE               8
#define  VSIZ_ADD_FACTOR         8
#define  VSIZ_SQFT               12
#define  VSIZ_FLAG               2
#define  VSIZ_COUNT              8
#define  VSIZ_STR10              10
#define  VSIZ_STR20              20
#define  VSIZ_STR40              40
#define  VSIZ_FILLER1            66
#define  VSIZ_FILLER2            354

#define  VOFF_ATN                0
#define  VOFF_TE_NO              12
#define  VOFF_CHARID             22
#define  VOFF_CONDITION          30
#define  VOFF_YRBLT              40
#define  VOFF_YREFF              44
#define  VOFF_BLDGS              48
#define  VOFF_UNITS              56
#define  VOFF_LOTACRES           64
#define  VOFF_LOTSQFT            76
#define  VOFF_BLDGSQFT           88
#define  VOFF_ZONING             100
#define  VOFF_POOL               124
#define  VOFF_SPA                126
#define  VOFF_ISGATED            128
#define  VOFF_HASVIEW            130
#define  VOFF_HASWATERFRONT      132
#define  VOFF_HASGOLFCOURSE      134
#define  VOFF_ISFLOODZONE        136

#define  VOFF_FILLER1            138
#define  VOFF_BLDGID             204
#define  VOFF_BLDGNAME           212
#define  VOFF_STORIES            272
#define  VOFF_BATHS              280
#define  VOFF_BEDS               288
#define  VOFF_FIREPLACES         296
#define  VOFF_AIRCONDTYPE        304
#define  VOFF_HEATTYPE           344
#define  VOFF_COMMQUALTYPE       384
#define  VOFF_CONSTTYPE          394
#define  VOFF_SHAPERESTYPE       396
#define  VOFF_ADDSQFT            400
#define  VOFF_ADDDATE            410
#define  VOFF_ADDFACTOR          418
#define  VOFF_BSMTSQFT           426
#define  VOFF_1STFLRSQFT         438
#define  VOFF_2NDFLRSQFT         450
#define  VOFF_ABOVE2NDFLRSQFT    462
#define  VOFF_TOTALRESSQFT       474
#define  VOFF_RENTABLESQFT       486
#define  VOFF_TOTALCOMMSQFT      500
#define  VOFF_OTHERSQFT          510
#define  VOFF_PATIOSQFT          522
#define  VOFF_POOLHOUSESQFT      534
#define  VOFF_PORCHSQFT          546
#define  VOFF_CARPORTSQFT        558
#define  VOFF_GARSQFT            570
#define  VOFF_2NDGARSQFT         582
#define  VOFF_ISGARDET           594
#define  VOFF_ELEVATORS          596
#define  VOFF_HASREMODELD        604
#define  VOFF_HASCANOPY          606
#define  VOFF_HASESCALATOR       608
#define  VOFF_HASFIRESPRINKLER   610
#define  VOFF_HASMEZZANINE       612
#define  VOFF_HASDININGROOM      614
#define  VOFF_HASFAMILYROOM      616
#define  VOFF_HASSOLAR           618
#define  VOFF_HASPKLOT           620
#define  VOFF_HASPKSTR           622
#define  VOFF_HASCLBHSE          624
#define  VOFF_HASFIREPLACE       626
#define  VOFF_HASFITNESS         628
#define  VOFF_HASLAUNDRY         630
#define  VOFF_HASPLAYGRND        632
#define  VOFF_HASTENNIS          634
#define  VOFF_HASWSHDRY          636

#define  VOFF_COVEREDSPACE       638
#define  VOFF_UNCOVEREDSPACE     646      // Parking lot

#define  VOFF_REMODELYEAR        654
#define  VOFF_RESQUALTYPE        658
#define  VOFF_FILLER2            668

typedef struct _tKerCval
{
   char Atn             [VSIZ_ATN];          // 0
   char Te_No           [VSIZ_TE_NO];        // 12
   char CharID          [VSIZ_ID];           // 22
   char Condition       [VSIZ_CONDITION];    // 30
   char YrBlt           [VSIZ_YEAR];         // 40
   char YrEff           [VSIZ_YEAR];         // 44
   char Bldgs           [VSIZ_COUNT];        // 48
   char Units           [VSIZ_COUNT];        // 56
   char LotAcres        [VSIZ_SQFT];         // 64
   char LotSqft         [VSIZ_SQFT];         // 76
   char BldgSqft        [VSIZ_SQFT];         // 88
   char Zoning          [VSIZ_ZONING];       // 100
   char HasPool         [VSIZ_FLAG];         // 124
   char HasSpa          [VSIZ_FLAG];         // 126
   char IsGated         [VSIZ_FLAG];         // 128
   char HasView         [VSIZ_FLAG];         // 130

   char HasWaterFront   [VSIZ_FLAG];         // 132
   char HasGolfCourse   [VSIZ_FLAG];         // 134
   char IsFloodZone     [VSIZ_FLAG];         // 136

   char filler1         [VSIZ_FILLER1];      // 138
   char BldgID          [VSIZ_ID];           // 204
   char BldgName        [VSIZ_BLDGNAME];     // 212
   char Stories         [VSIZ_COUNT];        // 272
   char Baths           [VSIZ_COUNT];        // 280
   char Beds            [VSIZ_COUNT];        // 288
   char Fireplaces      [VSIZ_COUNT];        // 296
   char AirCond         [VSIZ_STR40];        // 304
   char Heat            [VSIZ_STR40];        // 344
   char C_QualityType   [VSIZ_CONDITION];    // 384
   char ConstType       [VSIZ_FLAG];         // 394
   char ShapeType       [VSIZ_FLAG];         // 396
   char Add_Sqft        [VSIZ_SQFT];         // 400
   char Add_Date        [VSIZ_DATE];         // 410
   char Add_Factor      [VSIZ_ADD_FACTOR];   // 418
   char BsmtSqft        [VSIZ_SQFT];         // 426
   char FirstFlrSqft    [VSIZ_SQFT];         // 438
   char SecondFlrSqft   [VSIZ_SQFT];         // 450
   char Above2ndFlrSqft [VSIZ_SQFT];         // 462
   char ResAreaSqft     [VSIZ_SQFT];         // 474
   char RentableSqft    [VSIZ_SQFT];         // 486
   char TotalCommSqft   [VSIZ_SQFT];         // 500
   char OtherAreaSqft   [VSIZ_SQFT];         // 510
   char PatioAreaSqft   [VSIZ_SQFT];         // 522
   char PoolAreaSqft    [VSIZ_SQFT];         // 534
   char PorchAreaSqft   [VSIZ_SQFT];         // 546
   char CarportSqft     [VSIZ_SQFT];         // 558
   char GarageSqft      [VSIZ_SQFT];         // 570
   char GarageSqft2     [VSIZ_SQFT];         // 582
   char GarageDetached  [VSIZ_FLAG];         // 594
   char Elevators       [VSIZ_COUNT];        // 596
   char HasRemodeled    [VSIZ_FLAG];         // 604
   char HasCanopy       [VSIZ_FLAG];         // 606
   char HasEscalator    [VSIZ_FLAG];         // 608
   char HasFireSplr     [VSIZ_FLAG];         // 610
   char HasMezzanine    [VSIZ_FLAG];         // 612
   char HasDiningRoom   [VSIZ_FLAG];         // 614
   char HasFamilyRoom   [VSIZ_FLAG];         // 616
   char HasSolarSystem  [VSIZ_FLAG];         // 618

   char HasParkingLot   [VSIZ_FLAG];         // 620
   char HasParkingStr   [VSIZ_FLAG];         // 622

   char HasClubHouse    [VSIZ_FLAG];         // 624
   char HasFLInUnits    [VSIZ_FLAG];         // 626
   char HasFitness      [VSIZ_FLAG];         // 628
   char HasLaundry      [VSIZ_FLAG];         // 630
   char HasPlayground   [VSIZ_FLAG];         // 632
   char HasTennisCourt  [VSIZ_FLAG];         // 634
   char HasWasherDryer  [VSIZ_FLAG];         // 636
   char CoveredSpaces   [VSIZ_COUNT];        // 638
   char UnCoveredSpaces [VSIZ_COUNT];        // 646

   char RemodelYear     [VSIZ_YEAR];         // 654
   char R_QualityType   [VSIZ_CONDITION];    // 658
   char filler2         [VSIZ_FILLER2];      // 668
   char CRLF[2];                             // 1022
} KER_CVAL;

FLD_IDX tblBldgVal[] = 
{
   "tblBuilding.BuildingName"                         ,0, 22, VSIZ_BLDGNAME,  VOFF_BLDGNAME,
   "tblBuilding.BuildingID"                           ,0, 22, VSIZ_ID,        VOFF_BLDGID,
   "tblBuilding.EffectiveYear"                        ,1, 22, VSIZ_YEAR,      VOFF_YREFF,
   "tblBuilding.YearBuilt"                            ,1, 22, VSIZ_YEAR,      VOFF_YRBLT,
   "tblBuilding.PropertyConditionTypeID"              ,0, 22, VSIZ_CONDITION, VOFF_CONDITION,
   "tblBuilding.StoriesCount"                         ,1, 22, VSIZ_COUNT,     VOFF_STORIES,
   "tblBuilding.TotalBuildingSF"                      ,1, 22, VSIZ_SQFT,      VOFF_BLDGSQFT,    // single bldg
   "tblBuilding.UnitsCount"                           ,1, 22, VSIZ_COUNT,     VOFF_UNITS,       // single bldg
   "tblBuildingComm.AdditionAreaSF"                   ,1, 22, VSIZ_SQFT,      VOFF_ADDSQFT,
   "tblBuildingComm.AirConditioningCommTypeID"        ,0, 22, VSIZ_STR40,     VOFF_AIRCONDTYPE,
   "tblBuildingComm.BasementAreaSF"                   ,1, 22, VSIZ_SQFT,      VOFF_BSMTSQFT,
   "tblBuildingComm.ElevatorsCount"                   ,1, 22, VSIZ_COUNT,     VOFF_ELEVATORS,
   "tblBuildingComm.HasBeenRemodeled"                 ,0, 22, VSIZ_FLAG,      VOFF_HASREMODELD,
   "tblBuildingComm.HasCanopy"                        ,0, 22, VSIZ_FLAG,      VOFF_HASCANOPY,
   "tblBuildingComm.HasEscalators"                    ,0, 22, VSIZ_FLAG,      VOFF_HASESCALATOR,
   "tblBuildingComm.HasFireSprinklers"                ,0, 22, VSIZ_FLAG,      VOFF_HASFIRESPRINKLER,
   "tblBuildingComm.HasMezzanine"                     ,0, 22, VSIZ_FLAG,      VOFF_HASMEZZANINE,
   "tblBuildingComm.HeatingCommTypeID"                ,0, 22, VSIZ_STR40,     VOFF_HEATTYPE,
   "tblBuildingComm.MostRecentRemodelYear"            ,1, 22, VSIZ_YEAR,      VOFF_REMODELYEAR,
   "tblBuildingComm.QualityCommTypeID"                ,0, 22, VSIZ_CONDITION, VOFF_COMMQUALTYPE,
   "tblBuildingComm.RentableAreaSF"                   ,1, 22, VSIZ_SQFT,      VOFF_RENTABLESQFT,
   "tblBuildingComm.TotalCommAreaSF"                  ,1, 22, VSIZ_SQFT,      VOFF_TOTALCOMMSQFT,
   "tblBuildingRes.AboveSecondFloorAreaSF"            ,1, 22, VSIZ_SQFT,      VOFF_ABOVE2NDFLRSQFT,
   "tblBuildingRes.AdditionAreaSF"                    ,1, 24, VSIZ_SQFT,      VOFF_ADDSQFT,
   "tblBuildingRes.AdditionDate"                      ,3, 24, VSIZ_DATE,      VOFF_ADDDATE,
   "tblBuildingRes.AdditionFactor"                    ,0, 24, VSIZ_ADD_FACTOR,VOFF_ADDFACTOR,
   "tblBuildingRes.AirConditioningResTypeID"          ,0, 22, VSIZ_STR40,     VOFF_AIRCONDTYPE,
   "tblBuildingRes.BasementAreaSF"                    ,1, 22, VSIZ_SQFT,      VOFF_BSMTSQFT,
   "tblBuildingRes.BathroomCount"                     ,0, 22, VSIZ_COUNT,     VOFF_BATHS,
   "tblBuildingRes.BedroomCount"                      ,1, 22, VSIZ_COUNT,     VOFF_BEDS,
   "tblBuildingRes.CarportAreaSF"                     ,1, 22, VSIZ_SQFT,      VOFF_CARPORTSQFT,
   "tblBuildingRes.ConstructionResTypeID"             ,0, 22, VSIZ_FLAG,      VOFF_CONSTTYPE,
   "tblBuildingRes.FireplaceCount"                    ,1, 22, VSIZ_COUNT,     VOFF_FIREPLACES,
   "tblBuildingRes.FirstFloorAreaSF"                  ,1, 22, VSIZ_SQFT,      VOFF_1STFLRSQFT,
   "tblBuildingRes.GarageAreaSF"                      ,1, 22, VSIZ_SQFT,      VOFF_GARSQFT,
   "tblBuildingRes.HasDiningRoom"                     ,0, 22, VSIZ_FLAG,      VOFF_HASDININGROOM,
   "tblBuildingRes.HasFamilyRoom"                     ,0, 22, VSIZ_FLAG,      VOFF_HASFAMILYROOM,
   "tblBuildingRes.HasSolarSystem"                    ,0, 22, VSIZ_FLAG,      VOFF_HASSOLAR,
   "tblBuildingRes.IsGarageDetached"                  ,0, 22, VSIZ_FLAG,      VOFF_ISGARDET,
   "tblBuildingRes.OtherAreaSF"                       ,1, 22, VSIZ_SQFT,      VOFF_OTHERSQFT,
   "tblBuildingRes.PatioAreaSF"                       ,1, 22, VSIZ_SQFT,      VOFF_PATIOSQFT,
   "tblBuildingRes.PoolHouseAreaSF"                   ,1, 22, VSIZ_SQFT,      VOFF_POOLHOUSESQFT,
   "tblBuildingRes.PorchAreaSF"                       ,1, 22, VSIZ_SQFT,      VOFF_PORCHSQFT,
   "tblBuildingRes.QualityResTypeID"                  ,0, 22, VSIZ_COUNT,     VOFF_RESQUALTYPE,
   "tblBuildingRes.SecondFloorAreaSF"                 ,1, 22, VSIZ_SQFT,      VOFF_2NDFLRSQFT,
   "tblBuildingRes.SecondGarageAreaSF"                ,1, 22, VSIZ_SQFT,      VOFF_2NDGARSQFT,
   "tblBuildingRes.ShapeResTypeID"                    ,0, 22, VSIZ_FLAG,      VOFF_SHAPERESTYPE,
   "tblBuildingRes.TotalResAreaSF"                    ,1, 22, VSIZ_SQFT,      VOFF_TOTALRESSQFT,
   "",0,0,0,0
};  

FLD_IDX tblCharVal[] = 
{
   "tblCharacteristics.CharacteristicsID"             ,1, 28, VSIZ_ID,     VOFF_CHARID,
   "tblCharacteristics.AllBuildingsSF"                ,1, 28, VSIZ_SQFT,   VOFF_BLDGSQFT,
   "tblCharacteristics.BuildingCount"                 ,1, 28, VSIZ_COUNT,  VOFF_BLDGS,
   "tblCharacteristics.LandAreaAcres"                 ,0, 28, VSIZ_SQFT,   VOFF_LOTACRES,
   "tblCharacteristics.ZoningCode"                    ,0, 28, VSIZ_ZONING, VOFF_ZONING,
   "tblCharacteristics.TotalUnitsCount"               ,1, 28, VSIZ_COUNT,  VOFF_UNITS,
   "tblCharacteristicsRes.IsGated"                    ,0, 28, VSIZ_FLAG,   VOFF_ISGATED,
   "tblCharacteristicsRes.HasPool"                    ,0, 28, VSIZ_FLAG,   VOFF_POOL,
   "tblCharacteristicsRes.HasSpa"                     ,0, 28, VSIZ_FLAG,   VOFF_SPA,
   "tblCharacteristicsRes.HasScenicView"              ,0, 28, VSIZ_FLAG,   VOFF_HASVIEW,
   "tblCharacteristicsRes.HasWaterfront"              ,0, 28, VSIZ_FLAG,   VOFF_HASWATERFRONT,
   "tblCharacteristicsRes.HasGolfCourse"              ,0, 28, VSIZ_FLAG,   VOFF_HASGOLFCOURSE,
   "tblCharacteristicsRes.IsFloodZone"                ,0, 28, VSIZ_FLAG,   VOFF_ISFLOODZONE,
   "tblCharacteristicsComm.HasSolar"                  ,0, 28, VSIZ_FLAG,   VOFF_HASSOLAR,
   "tblCharacteristicsComm.HasParkingLot"             ,0, 35, VSIZ_FLAG,   VOFF_HASPKLOT,
   "tblCharacteristicsComm.HasParkingStructure"       ,0, 35, VSIZ_FLAG,   VOFF_HASPKSTR,
   "tblCharacteristicsComm.CoveredSpacesCount"        ,1, 32, VSIZ_COUNT,  VOFF_COVEREDSPACE,
   "tblCharacteristicsComm.UncoveredSpacesCount"      ,1, 32, VSIZ_COUNT,  VOFF_UNCOVEREDSPACE,
   "tblCharacteristicsApartment.HasClubHouse"         ,0, 38, VSIZ_FLAG,   VOFF_HASCLBHSE,
   "tblCharacteristicsApartment.HasFireplaceInUnits"  ,0, 38, VSIZ_FLAG,   VOFF_HASFIREPLACE,
   "tblCharacteristicsApartment.HasFitnessFacilities" ,0, 38, VSIZ_FLAG,   VOFF_HASFITNESS,
   "tblCharacteristicsApartment.HasLaundryFacilities" ,0, 38, VSIZ_FLAG,   VOFF_HASLAUNDRY,
   "tblCharacteristicsApartment.HasPlayground"        ,0, 38, VSIZ_FLAG,   VOFF_HASPLAYGRND,
   "tblCharacteristicsApartment.HasTennisCourt"       ,0, 38, VSIZ_FLAG,   VOFF_HASTENNIS,
   "tblCharacteristicsApartment.HasWasherDryerHookups",0, 38, VSIZ_FLAG,   VOFF_HASWSHDRY,
   "",0,0,0,0
};

// Tax file CURB.TXT
#define  TOFF_TAXYEAR                  1-1
#define  TOFF_BILL_NUMBER              5-1
#define  TOFF_BILL_STATUS              15-1
#define  TOFF_BILL_SUBTYPE             16-1
#define  TOFF_ATN                      18-1
#define  TOFF_FILE_NO                  29-1
#define  TOFF_APN                      42-1
#define  TOFF_TE_NO                    51-1
#define  TOFF_ROLL_YEAR                61-1
#define  TOFF_ROLL_NO                  65-1
#define  TOFF_ROLL_TYPE                72-1
#define  TOFF_USE_CODE                 73-1
#define  TOFF_TRA                      77-1
#define  TOFF_LAND                     83-1
#define  TOFF_MINERAL                  94-1
#define  TOFF_IMPR                     105-1
#define  TOFF_FIXTR                    116-1
#define  TOFF_PPVAL                    127-1
#define  TOFF_EXMPT_VAL                138-1
#define  TOFF_EXMPT_TYPE               149-1
#define  TOFF_EXMPT_VAL_2              151-1
#define  TOFF_EXMPT_TYPE_2             162-1
#define  TOFF_EXMPT_VAL_3              164-1
#define  TOFF_EXMPT_TYPE_3             175-1
#define  TOFF_ASSESSEE                 177-1
#define  TOFF_NAME_BILLING             221-1
#define  TOFF_ADDR_FRMTTD_CD           265-1
#define  TOFF_M_STR_NO                 266-1
#define  TOFF_M_STR_NO_FRACTION        272-1
#define  TOFF_M_STR_NAME               273-1
#define  TOFF_M_STR_DIR                293-1
#define  TOFF_M_STR_TYPE               295-1
#define  TOFF_M_CITY                   297-1
#define  TOFF_M_STATE                  312-1
#define  TOFF_M_ZIP                    314-1
#define  TOFF_M_UNIT_TYPE              323-1
#define  TOFF_M_UNIT                   326-1
#define  TOFF_UNDELIVER_IND            330-1
#define  TOFF_ADDR_CHG_IND             331-1
#define  TOFF_DT_BILLED                332-1
#define  TOFF_DT_BILL_CANCEL           340-1
#define  TOFF_BILL_AMT                 348-1
#define  TOFF_GEN_AMT_DUE              361-1
#define  TOFF_SA_AMT_DUE               374-1
#define  TOFF_SPCL_AMT_DUE             387-1
#define  TOFF_SUP_FULL_YR_AMT          400-1
#define  TOFF_TAX_RATE                 413-1
#define  TOFF_TAX_RATE_SPECL           423-1
#define  TOFF_KRNTX_CORTAC_NO          433-1
#define  TOFF_INSUFF_AMT_IND           444-1
#define  TOFF_ROLL_CORR_CD             445-1
#define  TOFF_TB_COND_OVR_IND          446-1
#define  TOFF_PRINT_CODE               447-1
#define  TOFF_DT_DELINQ_2ND            448-1
#define  TOFF_DT_DELINQ_1ST            456-1
#define  TOFF_DT_PAID_1ST              464-1
#define  TOFF_DT_PAID_2ND              472-1
#define  TOFF_INSTAL_DUE_1ST           480-1
#define  TOFF_INSTAL_DUE_2ND           493-1
#define  TOFF_PNLTY_DUE_1ST            506-1
#define  TOFF_PNLTY_DUE_2ND            519-1
#define  TOFF_AMOUNT_PAID_1            532-1
#define  TOFF_AMOUNT_PAID_2            545-1
#define  TOFF_DT_PROP_8                558-1
#define  TOFF_DATE_EVENT               566-1
#define  TOFF_EVENT_NO                 574-1
#define  TOFF_EVENT_TYPE               584-1
#define  TOFF_SUPPL_FACTOR             586-1
#define  TOFF_SUPPL_PERIOD             595-1
#define  TOFF_SUPPL_PERIOD_CD          598-1
#define  TOFF_SUPPL_UNSEC_IND          599-1
#define  TOFF_DT_SUPPL_PRO_BEG         600-1
#define  TOFF_DT_SUPPL_PRO_END         608-1
#define  TOFF_VALUA_REDUCTION          616-1
#define  TOFF_DOLLR_REDUCTION          629-1
#define  TOFF_GROSS_TAX_PRIOR          642-1
#define  TOFF_ROLL_REC_TYPE            655-1
#define  TOFF_AG_PRESERVE_CD           656-1
#define  TOFF_LN_PENALTY_IND           657-1
#define  TOFF_BAD_CHECK_IND            658-1
#define  TOFF_ESC_INS_PLN_IND          659-1
#define  TOFF_BANKRUPTCY_IND           660-1
#define  TOFF_TAX_POSTPON_IND          661-1
#define  TOFF_REDEMPTION_NO            662-1

#define  TSIZ_TAXYEAR                  4
#define  TSIZ_BILL_NUMBER              10
#define  TSIZ_BILL_STATUS              1
#define  TSIZ_BILL_SUBTYPE             2
#define  TSIZ_ATN                      11
#define  TSIZ_FILE_NO                  13
#define  TSIZ_APN                      9
#define  TSIZ_TE_NO                    10
#define  TSIZ_ROLL_YEAR                4
#define  TSIZ_ROLL_NO                  7
#define  TSIZ_ROLL_TYPE                1
#define  TSIZ_USE_CODE                 4
#define  TSIZ_TRA                      6
#define  TSIZ_LAND                     11
#define  TSIZ_MINERAL                  11
#define  TSIZ_IMPR                     11
#define  TSIZ_FIXTR                    11
#define  TSIZ_PPVAL                    11
#define  TSIZ_EXMPT_VAL                11
#define  TSIZ_EXMPT_TYPE               2
#define  TSIZ_NAME                     44
#define  TSIZ_ADDR_FRMTTD_CD           1
#define  TSIZ_M_STR_NO                 6
#define  TSIZ_M_STR_NO_FRACTION        1
#define  TSIZ_M_STR_NAME               20
#define  TSIZ_M_STR_DIR                2
#define  TSIZ_M_STR_TYPE               2
#define  TSIZ_M_CITY                   15
#define  TSIZ_M_STATE                  2
#define  TSIZ_M_ZIP                    5
#define  TSIZ_M_ZIP4                   4
#define  TSIZ_M_UNIT_TYPE              3
#define  TSIZ_M_UNIT                   4
#define  TSIZ_UNDELIVER_IND            1
#define  TSIZ_ADDR_CHG_IND             1
#define  TSIZ_DATE                     8
#define  TSIZ_TAX_AMT                  13
#define  TSIZ_TAX_RATE                 10
#define  TSIZ_TAX_RATE_SPECL           10
#define  TSIZ_KRNTX_CORTAC_NO          11
#define  TSIZ_EVENT_NO                 10
#define  TSIZ_EVENT_TYPE               2
#define  TSIZ_SUPPL_FACTOR             9
#define  TSIZ_SUPPL_PERIOD             3
#define  TSIZ_REDEMPTION_NO            7

// CURB.TXT layout
typedef struct _tKerTaxBill
{  // 722-bytes
   char  TaxYear[TSIZ_TAXYEAR];                   //1
   char  BillNum[TSIZ_BILL_NUMBER];               //5
   //char  ChkDigit;                                //14
   char  Bill_Status;                             //15 - �B� � Billed
				                                      //     �C� � Cancelled
				                                      //     �N� � Not Billable
				                                      //     �P� � Paid in Full
				                                      //     �R� � Refund
				                                      //     �T� � To be billed

   char  Bill_SubType[TSIZ_BILL_SUBTYPE];         //16 - 'S ' = SECURED
                                                  //     'SS' = SECURED SUPPLEMENTAL
                                                  //     'SE' = SECURED ESCAPE
                                                  //     'U ' = UNSECURED
 				                                      //     'US' = UNSECURED SUPPLEMENTAL
 				                                      //     'UE' = UNSECURED ESCAPE
                                                  //     'UI' = UNSECURED INTERRIM OWNER
                                                  //     'RC' = ROLL CORRECTION

   char  Atn[TSIZ_ATN];                           //18
   char  FileNo[TSIZ_FILE_NO];                    //29
   char  Apn[TSIZ_APN];                           //42
   char  TeNo[TSIZ_TE_NO];                        //51
   char  RollYear[TSIZ_ROLL_YEAR];                //61
   char  RollNo[TSIZ_ROLL_NO];                    //65
   char  RollType;                                //72    
   /* Roll-Type-CD contains the code for the type of roll (i.e.     
      Secured, Unsecured, Government, Mineral, Mobile Home, etc.).  The following table details the roll type:
      RollType Code    Sec/Unsec Ind       Description
       	 1                S                 SECURED
      	 2   	            S                 OIL AND GAS
       	 3                S          	      UTILITY
       	 4                U                 UNSECURED
  			 5                S                 GOVERNMENT		      
          6                                     ?        388321580
   */
   char  UseCode[TSIZ_USE_CODE];                  //73
   char  TRA[TSIZ_TRA];                           //77
   char  Land[TSIZ_LAND];                         //83
   char  Mineral[TSIZ_MINERAL];                   //94
   char  Impr[TSIZ_IMPR];                         //105
   char  Fixtr[TSIZ_FIXTR];                       //116
   char  PPVal[TSIZ_PPVAL];                       //127
   char  ExeVal[TSIZ_EXMPT_VAL];                  //138
   char  ExeType[TSIZ_EXMPT_TYPE];                //149
   char  ExeVal2[TSIZ_EXMPT_VAL];                 //151
   char  ExeType2[TSIZ_EXMPT_TYPE];               //162
   char  ExeVal3[TSIZ_EXMPT_VAL];                 //164
   char  ExeType3[TSIZ_EXMPT_TYPE];               //175
   char  Assessee[TSIZ_NAME];                     //177
   char  NameBilling[TSIZ_NAME];                  //221
   char  M_FmtAddr_Cd;                            //265
   char  M_StrNum[TSIZ_M_STR_NO];                 //266
   char  M_StrFra[TSIZ_M_STR_NO_FRACTION];        //272
   char  M_StrName[TSIZ_M_STR_NAME];              //273
   char  M_StrDir[TSIZ_M_STR_DIR];                //293
   char  M_StrSfx[TSIZ_M_STR_TYPE];               //295
   char  M_City[TSIZ_M_CITY];                     //297
   char  M_State[TSIZ_M_STATE];                   //312
   char  M_Zip[TSIZ_M_ZIP];                       //314
   char  M_Zip4[TSIZ_M_ZIP4];                     //319
   char  M_UnitType[TSIZ_M_UNIT_TYPE];            //323
   char  M_Unit[TSIZ_M_UNIT];                     //326
   char  M_BadAdr;                                //330
   char  M_AdrChg;                                //331
   char  DtBilled[TSIZ_DATE];                     //332
   char  DtBillCancel[TSIZ_DATE];                 //340
   char  TotalTax[TSIZ_TAX_AMT];                  //348
   char  TotalDue[TSIZ_TAX_AMT];                  //361
   char  Total_SA_AmtDue[TSIZ_TAX_AMT];           //374
   char  Spcl_AmtDue[TSIZ_TAX_AMT];               //387
   char  Supp_FullYrAmt[TSIZ_TAX_AMT];            //400
   char  Gen_TaxRate[TSIZ_TAX_RATE];              //413
   char  Spcl_TaxRate[TSIZ_TAX_RATE];             //423
   char  CortacNo[TSIZ_KRNTX_CORTAC_NO];          //433
   char  Insuf_Ind;                               //444  - If the bill amount due is zero or below the minimum amount due this will be set to �y�
   char  RollCorr_Cd;                             //445  - I=roll correction in progress, R=roll correction occurred
   char  TB_Cond_Ovr;                             //446
   char  Print_Cd;                                //447
   char  DueDate2[TSIZ_DATE];                     //448
   char  DueDate1[TSIZ_DATE];                     //456
   char  PaidDate1[TSIZ_DATE];                    //464
   char  PaidDate2[TSIZ_DATE];                    //472
   char  DueAmt1[TSIZ_TAX_AMT];                   //480
   char  DueAmt2[TSIZ_TAX_AMT];                   //493
   char  PenAmt1[TSIZ_TAX_AMT];                   //506
   char  PenAmt2[TSIZ_TAX_AMT];                   //519
   char  PaidAmt1[TSIZ_TAX_AMT];                  //532
   char  PaidAmt2[TSIZ_TAX_AMT];                  //545
   char  Prop8_Dt[TSIZ_DATE];                     //558
   char  Event_Dt[TSIZ_DATE];                     //566
   char  Event_No[TSIZ_EVENT_NO];                 //574
   char  Event_Type[TSIZ_EVENT_TYPE];             //584
   char  Supp_Factor[TSIZ_SUPPL_FACTOR];          //586
   char  Supp_Period[TSIZ_SUPPL_PERIOD];          //595
   char  Supp_Period_Cd;                          //598
   char  Supp_Unsec;                              //599
   char  Supp_Pro_Beg_Dt[TSIZ_DATE];              //600
   char  Supp_Pro_End_Dt[TSIZ_DATE];              //608
   char  Value_Reduction[TSIZ_TAX_AMT];           //616
   char  Dollar_Reduction[TSIZ_TAX_AMT];          //629
   char  Gross_Tax_Prior[TSIZ_TAX_AMT];           //642
   char  RollRec_Type;                            //655
   char  Ag_Preserve;                             //656
   char  Lien_Penalty;                            //657
   char  Bad_Check;                               //658
   char  Esc_Ins_Pln_Ind;                         //659
   char  Bankruptcy_Ind;                          //660
   char  Postpone_Ind;                            //661
   char  RedemptionNo[TSIZ_REDEMPTION_NO];        //662
   char  filler[52];                              //669
   char  CRLF[2];
} KER_TAX;

// TaxSrvc.txt
#define  OSVC_TAX_YEAR              1
#define  OSVC_BILL_NO               3
#define  OSVC_BILL_STATUS_CD        13
#define  OSVC_ATN                   14
#define  OSVC_TRA_NO                25
#define  OSVC_KRNTX_CORTAC_NO       31
#define  OSVC_KRN_COR_SECOND        36
#define  OSVC_NAME_ASSE             42
#define  OSVC_NAME_BILLING          86
#define  OSVC_BILL_SUBTYPE_CD       130
#define  OSVC_TAX_YEAR_ROLL         132
#define  OSVC_ROLL_TYPE_CODE        136
#define  OSVC_ZONE_CODE             137
#define  OSVC_USE_CODE              142
#define  OSVC_ADDR_FRMTTD_CD        146
#define  OSVC_M_STR_NO              147
#define  OSVC_M_FRACTION            153
#define  OSVC_M_STR_NAME            154
#define  OSVC_M_STR_DIR             174
#define  OSVC_M_STR_TYPE_CODE       176
#define  OSVC_M_CITY                178
#define  OSVC_M_STATE_CODE          193
#define  OSVC_M_ZIP_CODE            195
#define  OSVC_M_AD_UNIT_TYPE        204
#define  OSVC_M_AD_UNIT_ID          207
#define  OSVC_FILLER1               211
#define  OSVC_ADDRESS_LINE1         147
#define  OSVC_ADDRESS_LINE2         177
#define  OSVC_ADDRESS_LINE3         207
#define  OSVC_S_STR_NO              237
#define  OSVC_S_FRACTION            243
#define  OSVC_S_STR_NAME            244
#define  OSVC_S_STR_DIR             264
#define  OSVC_S_STR_TYPE_CODE       266  
#define  OSVC_S_CITY                268  
#define  OSVC_S_AD_UNIT_TYPE        283  
#define  OSVC_S_AD_UNIT_ID          286  
#define  OSVC_LEGAL_TYPE_CODE       290
#define  OSVC_METES_BOUNDS          292
#define  OSVC_CITY_ABBR             292
#define  OSVC_BLOCK                 298
#define  OSVC_CITY_LOT              301
#define  OSVC_PARCEL_MAP            305
#define  OSVC_PARCEL_LOT            311
#define  OSVC_SECTION               315
#define  OSVC_TOWNSHIP              318
#define  OSVC_RANGE                 321
#define  OSVC_QUARTER               324
#define  OSVC_TRACT_MAP             326
#define  OSVC_TRACT_BLOCK           332
#define  OSVC_TRACT_LOT             335
#define  OSVC_FILLER2               339
#define  OSVC_REC_DOC_NO            352
#define  OSVC_RECORDER_BK           352
#define  OSVC_RECORDER_PG           357
#define  OSVC_NET_ASSE_VAL          361
#define  OSVC_LAND_VALUE            371
#define  OSVC_MINERAL_VALUE         381
#define  OSVC_OI_FIX_VALUE          391
#define  OSVC_IMP_VALUE             401
#define  OSVC_PERS_PROP_VAL         411
#define  OSVC_EXMPT_VAL             421
#define  OSVC_EXMPT_TYPE_CODE       431
#define  OSVC_NET_ASSE_VAL_S        433
#define  OSVC_LAND_VALUE_S          443
#define  OSVC_MINERAL_VALUE_S       453
#define  OSVC_OI_FIX_VALUE_S        463
#define  OSVC_IMP_VALUE_S           473
#define  OSVC_PERS_PROP_VAL_S       483
#define  OSVC_EXMPT_VAL_S           493
#define  OSVC_EXMPT_TYPE_CODE_S     503
#define  OSVC_BILL_AMT              505
#define  OSVC_GEN_AMT_DUE           516
#define  OSVC_SPCL_AMT_DUE          527
#define  OSVC_SA_AMT_DUE            538
#define  OSVC_SUP_FULL_YR_AMT       549
#define  OSVC_DT_BILL_CANCEL        560
#define  OSVC_TAX_RATE_GENERL       568
#define  OSVC_TAX_RATE_SPECL        576
#define  OSVC_DELINQ_FINAL          584
#define  OSVC_DT_RECORDED           592
#define  OSVC_SUPPL_FACTOR          600
#define  OSVC_DT_EVENT              607
#define  OSVC_EVENT_TYPE_CODE       615
#define  OSVC_SUPPL_PERIOD_CD       617
#define  OSVC_SUPPL_PERIOD          618
#define  OSVC_DT_SUP_PRO_BEG        621
#define  OSVC_DT_SUP_PRO_END        629
#define  OSVC_AMOUNT_DUE_1          637
#define  OSVC_AMOUNT_DUE_PEN_1      648
#define  OSVC_DT_DELINQUENT_1       659
#define  OSVC_AMOUNT_COLL_1         667
#define  OSVC_DT_PAID_1             678
#define  OSVC_AMOUNT_DUE_2          686
#define  OSVC_AMOUNT_DUE_PEN_2      697
#define  OSVC_DT_DELINQUENT_2       708
#define  OSVC_AMOUNT_COLL_2         716
#define  OSVC_DT_PAID_2             727
#define  OSVC_LN_PENALTY_IND        735
#define  OSVC_UNDELIVER_IND         736
#define  OSVC_INSUFF_AMT_IND        737
#define  OSVC_BAD_CHECK_IND         738
#define  OSVC_ESC_INS_PLN_IND       739
#define  OSVC_BANKRUPTCY_IND        740
#define  OSVC_TAX_POSTPON_IND       741
#define  OSVC_SPEC_ASMT_NUMBER      742
#define  OSVC_SA_FUND_NO            744
#define  OSVC_SA_DIST_TYPE_CD       749
#define  OSVC_SA_AMOUNT_1           751
#define  OSVC_SA_AMOUNT_2           762

#define  SSVC_TAX_YEAR              2
#define  SSVC_BILL_NO               10
#define  SSVC_BILL_STATUS_CD        1
#define  SSVC_ATN                   11
#define  SSVC_TRA_NO                6
#define  SSVC_KRNTX_CORTAC_NO       5
#define  SSVC_KRN_COR_SECOND        6
#define  SSVC_NAME_ASSE             44
#define  SSVC_NAME_BILLING          44
#define  SSVC_BILL_SUBTYPE_CD       2
#define  SSVC_TAX_YEAR_ROLL         4
#define  SSVC_ROLL_TYPE_CODE        1
#define  SSVC_ZONE_CODE             5
#define  SSVC_USE_CODE              4
#define  SSVC_ADDR_FRMTTD_CD        1
#define  SSVC_STR_NO                6
#define  SSVC_STR_NO_FRACTION       1
#define  SSVC_STR_NAME              20
#define  SSVC_STR_DIR               2
#define  SSVC_STR_TYPE_CODE         2
#define  SSVC_CITY                  15
#define  SSVC_STATE_CODE            2
#define  SSVC_ZIP_CODE              9
#define  SSVC_AD_UNIT_TYPE          3
#define  SSVC_AD_UNIT_ID            4
#define  SSVC_FILLER1               26
#define  SSVC_ADDRESS_LINE1         30
#define  SSVC_ADDRESS_LINE2         30
#define  SSVC_ADDRESS_LINE3         30
#define  SSVC_SIT_STR_NO            6
#define  SSVC_STR_NO_FRACTION       1
#define  SSVC_SIT_STR_NAME          20
#define  SSVC_SIT_STR_DIR           2
#define  SSVC_SIT_STR_TYPE_CODE     2
#define  SSVC_SIT_CITY              15
#define  SSVC_SIT_AD_UNIT_TYPE      3
#define  SSVC_SIT_AD_UNIT_ID        4
#define  SSVC_LEGAL_TYPE_CODE       2
#define  SSVC_METES_BOUNDS          60
#define  SSVC_CITY_ABBR             6
#define  SSVC_BLOCK                 3
#define  SSVC_CITY_LOT              4
#define  SSVC_PARCEL_MAP            6
#define  SSVC_PARCEL_LOT            4
#define  SSVC_SECTION               3
#define  SSVC_TOWNSHIP              3
#define  SSVC_RANGE                 3
#define  SSVC_QUARTER               2
#define  SSVC_TRACT_MAP             6
#define  SSVC_TRACT_BLOCK           3
#define  SSVC_TRACT_LOT             4
#define  SSVC_FILLER2               13
#define  SSVC_REC_DOC_NO            9
#define  SSVC_RECORDER_BK           5
#define  SSVC_RECORDER_PG           4
#define  SSVC_NET_ASSE_VAL          10
#define  SSVC_LAND_VALUE            10
#define  SSVC_MINERAL_VALUE         10
#define  SSVC_OI_FIX_VALUE          10
#define  SSVC_IMP_VALUE             10
#define  SSVC_PERS_PROP_VAL         10
#define  SSVC_EXMPT_VAL             10
#define  SSVC_EXMPT_TYPE_CODE       2
#define  SSVC_NET_ASSE_VAL_S        10
#define  SSVC_LAND_VALUE_S          10
#define  SSVC_MINERAL_VALUE_S       10
#define  SSVC_OI_FIX_VALUE_S        10
#define  SSVC_IMP_VALUE_S           10
#define  SSVC_PERS_PROP_VAL_S       10
#define  SSVC_EXMPT_VAL_S           10
#define  SSVC_EXMPT_TYPE_CODE_S     2
#define  SSVC_DT_BILL_CANCEL        8
#define  SSVC_TAX_RATE_GENERL       8
#define  SSVC_TAX_RATE_SPECL        8
#define  SSVC_DELINQ_FINAL          8
#define  SSVC_DT_RECORDED           8
#define  SSVC_SUPPL_FACTOR          7
#define  SSVC_DT_EVENT              8
#define  SSVC_EVENT_TYPE_CODE       2
#define  SSVC_SUPPL_PERIOD_CD       1
#define  SSVC_SUPPL_PERIOD          3
#define  SSVC_DT_SUP_PRO_BEG        8
#define  SSVC_DT_SUP_PRO_END        8
#define  SSVC_DT_DELINQUENT         8
#define  SSVC_DT_PAID               8
#define  SSVC_LN_PENALTY_IND        1
#define  SSVC_UNDELIVER_IND         1
#define  SSVC_INSUFF_AMT_IND        1
#define  SSVC_BAD_CHECK_IND         1
#define  SSVC_ESC_INS_PLN_IND       1
#define  SSVC_BANKRUPTCY_IND        1
#define  SSVC_TAX_POSTPON_IND       1
#define  SSVC_SPEC_ASMT_NUMBER      2
#define  SSVC_SA_FUND_NO            5
#define  SSVC_SA_DIST_TYPE_CD       2
#define  SSVC_REAL_AMOUNT           11    // V99
#define  SSVC_MAX_AGENCY            50

typedef struct _tKerLegal
{
   char  City_Abbr           [SSVC_CITY_ABBR  ];
   char  Block               [SSVC_BLOCK      ];
   char  City_Lot            [SSVC_CITY_LOT   ];
   char  Parcel_Map          [SSVC_PARCEL_MAP ];
   char  Parcel_Lot          [SSVC_PARCEL_LOT ];
   char  Section             [SSVC_SECTION    ];
   char  Township            [SSVC_TOWNSHIP   ];
   char  Range               [SSVC_RANGE      ];
   char  Quarter             [SSVC_QUARTER    ];
   char  Tract_Map           [SSVC_TRACT_MAP  ];
   char  Tract_Block         [SSVC_TRACT_BLOCK];
   char  Tract_Lot           [SSVC_TRACT_LOT  ];
   char  Filler2             [SSVC_FILLER2    ];
} KER_LGL;

typedef struct _tKerFund
{
   char  Fund_No             [SSVC_SA_FUND_NO];
   char  Dist_Type_Cd        [SSVC_SA_DIST_TYPE_CD];
   char  Amount1             [SSVC_REAL_AMOUNT];         // V99
   char  Amount2             [SSVC_REAL_AMOUNT];         // V99
} KER_FUND;

typedef struct _tKerAdrItem
{
   char  M_Str_No            [SSVC_STR_NO          ];
   char  M_Fraction          [SSVC_STR_NO_FRACTION ];
   char  M_Str_Name          [SSVC_STR_NAME        ];
   char  N_Str_Dir           [SSVC_STR_DIR         ];
   char  M_Str_Type_Code     [SSVC_STR_TYPE_CODE   ];
   char  M_City              [SSVC_CITY            ];
   char  M_State_Code        [SSVC_STATE_CODE      ];
   char  M_Zip_Code          [SSVC_ZIP_CODE        ];
   char  M_Ad_Unit_Type      [SSVC_AD_UNIT_TYPE    ];
   char  M_Ad_Unit_Id        [SSVC_AD_UNIT_ID      ];
   char  Filler1             [SSVC_FILLER1         ];
} ADR_ITEM;

typedef struct _tKerAdrLine
{
   char  Address_Line1       [SSVC_ADDRESS_LINE1   ];
   char  Address_Line2       [SSVC_ADDRESS_LINE2   ];
   char  Address_Line3       [SSVC_ADDRESS_LINE3   ];
} ADR_LINE;

typedef struct _tKerTaxSvc
{
   char  Tax_Year            [SSVC_TAX_YEAR        ];
   char  Bill_No             [SSVC_BILL_NO         ];
   //char  Check_Digit         [SSVC_CHECK_DIGIT     ];
   char  Bill_Status_Cd      [SSVC_BILL_STATUS_CD  ];
   char  Atn                 [SSVC_ATN             ];
   char  TRA                 [SSVC_TRA_NO          ];
   char  Krntx_Cortac_No     [SSVC_KRNTX_CORTAC_NO ];
   char  Krn_Cor_Second      [SSVC_KRN_COR_SECOND  ];
   char  Name_Asse           [SSVC_NAME_ASSE       ];
   char  Name_Billing        [SSVC_NAME_BILLING    ];
   char  Bill_Subtype_Cd     [SSVC_BILL_SUBTYPE_CD ];
   char  Tax_Year_Roll       [SSVC_TAX_YEAR_ROLL   ];
   char  Roll_Type_Code      [SSVC_ROLL_TYPE_CODE  ];
   char  Zone_Code           [SSVC_ZONE_CODE       ];
   char  Use_Code            [SSVC_USE_CODE        ];
   char  Addr_Frmttd_Cd      [SSVC_ADDR_FRMTTD_CD  ];

   union
   {
      ADR_ITEM Items;
      ADR_LINE Lines;
   } M_Addr;

   char  S_Str_No            [SSVC_STR_NO          ];
   char  S_Fraction          [SSVC_STR_NO_FRACTION ];
   char  S_Str_Name          [SSVC_STR_NAME        ];
   char  S_Str_Dir           [SSVC_STR_DIR         ];
   char  S_Str_Type_Code     [SSVC_STR_TYPE_CODE   ];
   char  S_City              [SSVC_CITY            ];
   char  S_Ad_Unit_Type      [SSVC_AD_UNIT_TYPE    ];
   char  S_Ad_Unit_Id        [SSVC_AD_UNIT_ID      ];
   char  Legal_Type_Code     [SSVC_LEGAL_TYPE_CODE ];

   union
   {
      char     Metes_Bounds  [SSVC_METES_BOUNDS];
      KER_LGL  Legal;
   } LglDesc;

   char  Rec_Doc_No          [SSVC_REC_DOC_NO       ];
   char  Net_Asse_Val        [SSVC_NET_ASSE_VAL     ];
   char  Land_Value          [SSVC_LAND_VALUE       ];
   char  Mineral_Value       [SSVC_MINERAL_VALUE    ];
   char  Oi_Fix_Value        [SSVC_OI_FIX_VALUE     ];
   char  Imp_Value           [SSVC_IMP_VALUE        ];
   char  Pers_Prop_Val       [SSVC_PERS_PROP_VAL    ];
   char  Exmpt_Val           [SSVC_EXMPT_VAL        ];
   char  Exmpt_Type_Code     [SSVC_EXMPT_TYPE_CODE  ];
   char  Net_Asse_Val_S      [SSVC_NET_ASSE_VAL_S   ];
   char  Land_Value_S        [SSVC_LAND_VALUE_S     ];
   char  Mineral_Value_S     [SSVC_MINERAL_VALUE_S  ];
   char  Oi_Fix_Value_S      [SSVC_OI_FIX_VALUE_S   ];
   char  Imp_Value_S         [SSVC_IMP_VALUE_S      ];
   char  Pers_Prop_Val_S     [SSVC_PERS_PROP_VAL_S  ];
   char  Exmpt_Val_S         [SSVC_EXMPT_VAL_S      ];
   char  Exmpt_Type_Code_S   [SSVC_EXMPT_TYPE_CODE_S];
   char  Bill_Amt            [SSVC_REAL_AMOUNT      ];
   char  Gen_Amt_Due         [SSVC_REAL_AMOUNT      ];
   char  Spcl_Amt_Due        [SSVC_REAL_AMOUNT      ];
   char  Sa_Amt_Due          [SSVC_REAL_AMOUNT      ];
   char  Sup_Full_Yr_Amt     [SSVC_REAL_AMOUNT      ];
   char  Dt_Bill_Cancel      [SSVC_DT_BILL_CANCEL   ];
   char  Tax_Rate_Generl     [SSVC_TAX_RATE_GENERL  ];      // V9(6)
   char  Tax_Rate_Specl      [SSVC_TAX_RATE_SPECL   ];      // V9(6)
   char  Delinq_Final        [SSVC_DELINQ_FINAL     ];
   char  Dt_Recorded         [SSVC_DT_RECORDED      ];
   char  Suppl_Factor        [SSVC_SUPPL_FACTOR     ];      // V9(6)
   char  Dt_Event            [SSVC_DT_EVENT         ];
   char  Event_Type_Code     [SSVC_EVENT_TYPE_CODE  ];
   char  Suppl_Period_Cd     [SSVC_SUPPL_PERIOD_CD  ];
   char  Suppl_Period        [SSVC_SUPPL_PERIOD     ];
   char  Dt_Sup_Pro_Beg      [SSVC_DT_SUP_PRO_BEG   ];
   char  Dt_Sup_Pro_End      [SSVC_DT_SUP_PRO_END   ];
   char  Amount_Due_1        [SSVC_REAL_AMOUNT      ];
   char  Amount_Due_Pen_1    [SSVC_REAL_AMOUNT      ];
   char  Dt_Delinquent_1     [SSVC_DT_DELINQUENT    ];
   char  Amount_Coll_1       [SSVC_REAL_AMOUNT      ];
   char  Dt_Paid_1           [SSVC_DT_PAID          ];
   char  Amount_Due_2        [SSVC_REAL_AMOUNT      ];
   char  Amount_Due_Pen_2    [SSVC_REAL_AMOUNT      ];
   char  Dt_Delinquent_2     [SSVC_DT_DELINQUENT    ];
   char  Amount_Coll_2       [SSVC_REAL_AMOUNT      ];
   char  Dt_Paid_2           [SSVC_DT_PAID          ];
   char  Ln_Penalty_Ind      [SSVC_LN_PENALTY_IND   ];
   char  Undeliver_Ind       [SSVC_UNDELIVER_IND    ];
   char  Insuff_Amt_Ind      [SSVC_INSUFF_AMT_IND   ];
   char  Bad_Check_Ind       [SSVC_BAD_CHECK_IND    ];
   char  Esc_Ins_Pln_Ind     [SSVC_ESC_INS_PLN_IND  ];
   char  Bankruptcy_Ind      [SSVC_BANKRUPTCY_IND   ];
   char  Tax_Postpon_Ind     [SSVC_TAX_POSTPON_IND  ];
   char  Spec_Asmt_Number    [SSVC_SPEC_ASMT_NUMBER ];      // 742
   KER_FUND Agency           [SSVC_MAX_AGENCY       ];
} KER_TAXSVC;


#define  LIC_BILLKEY             17
#define  LIC_TAXTYPE             1
#define  LIC_FUNDNO              5
#define  LIC_DISTCTLNO           6
#define  LIC_TAXRATE             10
#define  LIC_TAXAMT              13
#define  LIC_SA_IND              1

// Line items for AV
typedef struct _t_Txli
{
   char  BillKey[LIC_BILLKEY];      // 1
   char  filler1;                
   char  TaxType[LIC_TAXTYPE];      // 19
   char  filler2;
   char  FundNo[LIC_FUNDNO];        // 21
   char  filler3;
   char  DistCtlNo[LIC_DISTCTLNO];  // 27
   char  filler4;
   char  TaxRate[LIC_TAXRATE];      // 34
   char  filler5;
   char  TaxAmt[LIC_TAXAMT];        // 45
   char  filler6;
   char  SaInd[LIC_SA_IND];         // 59
} KER_TXLI;

#define  PRIB_TAX_YEAR              4
#define  PRIB_BILL_NUM              10
#define  PRIB_BILL_NO               7
#define  PRIB_BILL_NO_EXT           2
#define  PRIB_BILL_NO_CHK           1
#define  PRIB_BILL_STATUS_CD        1
#define  PRIB_BILL_SUBTYPE_CD       2
#define  PRIB_ATN                   11
#define  PRIB_FILE_NO               13
#define  PRIB_APN                   11
#define  PRIB_TE_NO                 10
#define  PRIB_TAX_YEAR_ROLL         4
#define  PRIB_ROLL_NO               7
#define  PRIB_ROLL_TYPE_CODE        1
#define  PRIB_USE_CODE              4
#define  PRIB_TRA_NO                6
#define  PRIB_LAND_VAL              10
#define  PRIB_MINERAL_VAL           10
#define  PRIB_IMP_VAL               10
#define  PRIB_OI_FIX_VAL            10
#define  PRIB_PERS_PROP_VAL         10
#define  PRIB_EXMPT_VAL             10
#define  PRIB_EXMPT_TYPE            2
#define  PRIB_EXMPT_VAL2            10
#define  PRIB_EXMPT_TYPE2           2
#define  PRIB_EXMPT_VAL3            10
#define  PRIB_EXMPT_TYPE3           2
#define  PRIB_ASSESSEE              44
#define  PRIB_MAIL_NAME             44
#define  PRIB_ADDR_FRMTTD_CD        1
#define  PRIB_STR_NO                6
#define  PRIB_STR_NO_FRACTION       1
#define  PRIB_STR_NAME              20
#define  PRIB_STR_DIR               2
#define  PRIB_STR_TYPE_CODE         2
#define  PRIB_CITY                  15
#define  PRIB_STATE_CODE            2
#define  PRIB_ZIP_CODE              9
#define  PRIB_AD_UNIT_TYPE          3
#define  PRIB_AD_UNIT_ID            4
#define  PRIB_UNDELIVER_IND         1
#define  PRIB_ADDR_CHG_IND          1
#define  PRIB_DT_BILLED             8
#define  PRIB_DT_BILL_CANCEL        8
#define  PRIB_BILL_AMT              13
#define  PRIB_GEN_AMT_DUE           13
#define  PRIB_SA_AMT_DUE            13
#define  PRIB_SPCL_AMT_DUE          13
#define  PRIB_SUP_FULL_YR_AMT       13
#define  PRIB_TAX_RATE_GENERL       10
#define  PRIB_TAX_RATE_SPECL        10
#define  PRIB_KRNTX_CORTAC_NO       11
#define  PRIB_INSUFF_AMT_IND        1
#define  PRIB_ROLL_CORR_CD          1
#define  PRIB_TB_COND_OVR_IND       1
#define  PRIB_PRINT_CODE            1
#define  PRIB_DT_DELINQ_2ND         8
#define  PRIB_DT_DELINQ_1ST         8
#define  PRIB_DT_PAID_1ST           8
#define  PRIB_DT_PAID_2ND           8
#define  PRIB_INSTAL_DUE_1ST        13
#define  PRIB_INSTAL_DUE_2ND        13
#define  PRIB_PNLTY_DUE_1ST         13
#define  PRIB_PNLTY_DUE_2ND         13
#define  PRIB_AMOUNT_PAID_1         13
#define  PRIB_AMOUNT_PAID_2         13
#define  PRIB_DT_PROP_8             8
#define  PRIB_DATE_EVENT            8
#define  PRIB_EVENT_KEY             12
#define  PRIB_SUPPL_FACTOR          9
#define  PRIB_SUPPL_PERIOD          3
#define  PRIB_SUPPL_PERIOD_CD       1
#define  PRIB_SUPPL_UNSEC_IND       1
#define  PRIB_DT_SUPPL_PRO_BEG      8
#define  PRIB_DT_SUPPL_PRO_END      8
#define  PRIB_VALUE_REDUCTION       13
#define  PRIB_DOLLR_REDUCTION       13
#define  PRIB_GROSS_TAX_PRIOR       13
#define  PRIB_ROLL_REC_TYPE         1
#define  PRIB_AG_PRESERVE_CD        1
#define  PRIB_LN_PENALTY_IND        1
#define  PRIB_BAD_CHECK_IND         1
#define  PRIB_ESC_INS_PLN_IND       1
#define  PRIB_BANKRUPTCY_IND        1
#define  PRIB_TAX_POSTPON_IND       1
#define  PRIB_REDEMPTION_NO         7
/*
Bill-Subtype-Cd      SUBTYPE OF BILL FOR BILLING
      'S ' = SECURED
      'SS' = SECURED SUPPLEMENTAL
      'SE' = SECURED ESCAPE
      'U ' = UNSECURED
      'US' = UNSECURED SUPPLEMENTAL
      'UE' = UNSECURED ESCAPE
      'UI' = UNSECURED INTERRIM OWNER
      'RC' = ROLL CORRECTION 
*/

typedef struct _tKerTaxDelq
{
   char  Tax_Year         [PRIB_TAX_YEAR        ];  // 1
   char  Bill_No          [PRIB_BILL_NO         ];  // 5
   char  Bill_No_Ext      [PRIB_BILL_NO_EXT     ];  // 12
   char  Bill_No_Chk      [PRIB_BILL_NO_CHK     ];  // 14
   char  Bill_Status_Cd   [PRIB_BILL_STATUS_CD  ];  // 15 B=Billed, C=Cancelled, N=Not Billable, P=Paid in Full, R=Refund, T=To be billed
   char  Bill_Subtype_Cd  [PRIB_BILL_SUBTYPE_CD ];  // 16
   char  Atn              [PRIB_ATN             ];  // 18
   char  File_No          [PRIB_FILE_NO         ];  // 29
   char  Apn              [PRIB_APN             ];  // 42
   char  Te_No            [PRIB_TE_NO           ];  // 51
   char  Tax_Year_Roll    [PRIB_TAX_YEAR_ROLL   ];  // 61
   char  Roll_No          [PRIB_ROLL_NO         ];  // 65
   char  Roll_Type_Code   [PRIB_ROLL_TYPE_CODE  ];  // 72 1=Secd, 2=Secd Oil & Gas, 3=Secd Util, 4=Unsecd, 5=Secd Government
   char  Use_Code         [PRIB_USE_CODE        ];  // 73
   char  Tra_No           [PRIB_TRA_NO          ];  // 77
   char  Land_Val         [PRIB_LAND_VAL        ];  // 83
   char  Mineral_Val      [PRIB_MINERAL_VAL     ];  // 94
   char  Imp_Val          [PRIB_IMP_VAL         ];  // 105
   char  Oi_Fix_Val       [PRIB_OI_FIX_VAL      ];  // 116
   char  Pers_Prop_Val    [PRIB_PERS_PROP_VAL   ];  // 127
   char  Exmpt_Val        [PRIB_EXMPT_VAL       ];  // 138
   char  Exmpt_Type       [PRIB_EXMPT_TYPE      ];  // 149
   char  Exmpt_Val2       [PRIB_EXMPT_VAL2      ];  // 151
   char  Exmpt_Type2      [PRIB_EXMPT_TYPE2     ];  // 162
   char  Exmpt_Val3       [PRIB_EXMPT_VAL3      ];  // 164
   char  Exmpt_Type3      [PRIB_EXMPT_TYPE3     ];  // 175
   char  Assessee         [PRIB_ASSESSEE        ];  // 177
   char  Mail_Name        [PRIB_MAIL_NAME       ];  // 221
   char  Addr_Frmttd_Cd   [PRIB_ADDR_FRMTTD_CD  ];  // 265
   char  Str_No           [PRIB_STR_NO          ];  // 266
   char  Str_No_Fraction  [PRIB_STR_NO_FRACTION ];  // 272
   char  Str_Name         [PRIB_STR_NAME        ];  // 273
   char  Str_Dir          [PRIB_STR_DIR         ];  // 293
   char  Str_Type_Code    [PRIB_STR_TYPE_CODE   ];  // 295
   char  City             [PRIB_CITY            ];  // 297
   char  State_Code       [PRIB_STATE_CODE      ];  // 312
   char  Zip_Code         [PRIB_ZIP_CODE        ];  // 314
   char  Ad_Unit_Type     [PRIB_AD_UNIT_TYPE    ];  // 323
   char  Ad_Unit_Id       [PRIB_AD_UNIT_ID      ];  // 326
   char  Undeliver_Ind    [PRIB_UNDELIVER_IND   ];  // 330
   char  Addr_Chg_Ind     [PRIB_ADDR_CHG_IND    ];  // 331
   char  Dt_Billed        [PRIB_DT_BILLED       ];  // 332
   char  Dt_Bill_Cancel   [PRIB_DT_BILL_CANCEL  ];  // 340
   char  Bill_Amt         [PRIB_BILL_AMT        ];  // 348
   char  Gen_Amt_Due      [PRIB_GEN_AMT_DUE     ];  // 361
   char  Sa_Amt_Due       [PRIB_SA_AMT_DUE      ];  // 374
   char  Spcl_Amt_Due     [PRIB_SPCL_AMT_DUE    ];  // 387
   char  Sup_Full_Yr_Amt  [PRIB_SUP_FULL_YR_AMT ];  // 400
   char  Tax_Rate_Generl  [PRIB_TAX_RATE_GENERL ];  // 413
   char  Tax_Rate_Specl   [PRIB_TAX_RATE_SPECL  ];  // 423
   char  Krntx_Cortac_No  [PRIB_KRNTX_CORTAC_NO ];  // 433
   char  Insuff_Amt_Ind   [PRIB_INSUFF_AMT_IND  ];  // 444
   char  Roll_Corr_Cd     [PRIB_ROLL_CORR_CD    ];  // 445 space=No roll corr, I=roll corr in progress, R=roll corr occured
   char  Tb_Cond_Ovr_Ind  [PRIB_TB_COND_OVR_IND ];  // 446 Tax Bill Conditional Override Indicator
   char  Print_Code       [PRIB_PRINT_CODE      ];  // 447
   char  Dt_Delinq_2nd    [PRIB_DT_DELINQ_2ND   ];  // 448
   char  Dt_Delinq_1st    [PRIB_DT_DELINQ_1ST   ];  // 456
   char  Dt_Paid_1st      [PRIB_DT_PAID_1ST     ];  // 464
   char  Dt_Paid_2nd      [PRIB_DT_PAID_2ND     ];  // 472
   char  Instal_Due_1st   [PRIB_INSTAL_DUE_1ST  ];  // 480
   char  Instal_Due_2nd   [PRIB_INSTAL_DUE_2ND  ];  // 493
   char  Pnlty_Due_1st    [PRIB_PNLTY_DUE_1ST   ];  // 506
   char  Pnlty_Due_2nd    [PRIB_PNLTY_DUE_2ND   ];  // 519
   char  Amount_Paid_1    [PRIB_AMOUNT_PAID_1   ];  // 532
   char  Amount_Paid_2    [PRIB_AMOUNT_PAID_2   ];  // 545
   char  Dt_Prop_8        [PRIB_DT_PROP_8       ];  // 558
   char  Date_Event       [PRIB_DATE_EVENT      ];  // 566
   char  Event_Key        [PRIB_EVENT_KEY       ];  // 574
   char  Suppl_Factor     [PRIB_SUPPL_FACTOR    ];  // 586
   char  Suppl_Period     [PRIB_SUPPL_PERIOD    ];  // 595
   char  Suppl_Period_Cd  [PRIB_SUPPL_PERIOD_CD ];  // 598
   char  Suppl_Unsec_Ind  [PRIB_SUPPL_UNSEC_IND ];  // 599
   char  Dt_Suppl_Pro_Beg [PRIB_DT_SUPPL_PRO_BEG];  // 600
   char  Dt_Suppl_Pro_End [PRIB_DT_SUPPL_PRO_END];  // 608
   char  Valua_Reduction  [PRIB_VALUE_REDUCTION ];  // 616
   char  Dollr_Reduction  [PRIB_DOLLR_REDUCTION ];  // 629
   char  Gross_Tax_Prior  [PRIB_GROSS_TAX_PRIOR ];  // 642
   char  Roll_Rec_Type    [PRIB_ROLL_REC_TYPE   ];  // 655 P=penalty, C=roll corr, space=original or supplemental
   char  Ag_Preserve_Cd   [PRIB_AG_PRESERVE_CD  ];  // 656
   char  Ln_Penalty_Ind   [PRIB_LN_PENALTY_IND  ];  // 657 Lien Penalty Indicator
   char  Bad_Check_Ind    [PRIB_BAD_CHECK_IND   ];  // 658 Y=paid with bad check
   char  Esc_Ins_Pln_Ind  [PRIB_ESC_INS_PLN_IND ];  // 659
   char  Bankruptcy_Ind   [PRIB_BANKRUPTCY_IND  ];  // 660
   char  Tax_Postpon_Ind  [PRIB_TAX_POSTPON_IND ];  // 661 Y=yes, there is a postponement
   char  Redemption_No    [PRIB_REDEMPTION_NO   ];  // 662
} KER_PRIB;

#define SECR_REDEMPTION_NO     7
#define SECR_DT_DEFAULT        8
#define SECR_TAX_YEAR          4
#define SECR_BILL_NO           7
#define SECR_BILL_NO_EXT       2
#define SECR_DEFAULT_AMT       13
#define SECR_ATN_DEFAULT       11
#define SECR_DT_PWR_TO_SELL    8
#define SECR_REC_DOC_NO_PTS    9
#define SECR_DT_PTS_RECORDED   8
#define SECR_DT_RCSN_LN_FEE    8
#define SECR_DT_RECISN_RECRD   8
#define SECR_REC_DOC_NO_RCSN   9
#define SECR_RCSN_LIEN_FEE     7
#define SECR_SEC_RDMP_FEE      7
#define SECR_TE_NO             10
#define SECR_DT_REDEEMED       8
#define SECR_SEARCH_FEE        7
#define SECR_RDMP_STATUS       1
#define SECR_RDMP_STATUS_PRI   1
#define SECR_INSTALL_STATUS    1
#define SECR_DT_INST_PLN_STR   8
#define SECR_DT_INST_PLN_DFL   8
#define SECR_EVENT_KEY         12
#define SECR_DT_SLDBID_OFFRD   8
#define SECR_DT_SLDBID_SOLD    8
#define SECR_DT_PUBAUC_OFFRD   8
#define SECR_DT_PUBAUC_SOLD    8
#define SECR_DT_TAX_SALE_RCD   8
#define SECR_REC_DOC_TAX_SAL   9
#define SECR_TAX_SALE_AMT      13
#define SECR_DEFAULT_NO        13
#define SECR_RDMP_PN_AMT_01    13
#define SECR_RDMP_PN_AMT_02    13
#define SECR_RDMP_PN_AMT_03    13
#define SECR_RDMP_PN_AMT_04    13
#define SECR_RDMP_PN_AMT_05    13
#define SECR_RDMP_PN_AMT_06    13
#define SECR_RDMP_PN_AMT_07    13
#define SECR_RDMP_PN_AMT_08    13
#define SECR_RDMP_PN_AMT_09    13
#define SECR_RDMP_PN_AMT_10    13
#define SECR_RDMP_PN_AMT_11    13
#define SECR_RDMP_PN_AMT_12    13
#define SECR_RDMP_PN_AMT_13    13
#define SECR_RDMP_PN_AMT_14    13
#define SECR_RDMP_PN_AMT_15    13
#define SECR_AMOUNT_COLL       13
#define SECR_UNDELIVER_IND     1
#define SECR_BAD_CHECK_IND     1
#define SECR_BANKRUPTCY_IND    1
#define SECR_BNKR_CRTCASE_NO   18
#define SECR_DT_BNKRPTCY_FLD   8
#define SECR_DT_BNKRPTCY_CLR   8
#define SECR_DT_BNK_FNDS_RCV   8
#define SECR_AMT_BNK_FND_RCV   13
#define SECR_DT_CRTSY_NTC_ST   8
#define SECR_DT_PTS_NOTC_PRT   8
#define SECR_DT_POSTED         8
#define SECR_AMT_DUE_GENERAL   13
#define SECR_AMT_DUE_SPECIAL   13
#define SECR_AMT_DUE_SPASMTS   13
#define SECR_AMT_DUE_DLQ_PEN   13
#define SECR_AMT_DUE_DLQ_CST   13
#define SECR_AMT_VALID_IND     1
#define SECR_RDMP_FICHE_NO     5
#define SECR_FICHE_ROW         1
#define SECR_FICHE_COL         2
#define SECR_FICHE_UPD_IND     3
#define SECR_INST_PYMNT_AMT    10
#define SECR_INST_DFL_REA_CD   1
#define SECR_DT_TAX_DFLT_PUB   8
#define SECR_DFLT_PUB_AD_IND   1
#define SECR_EXCLUDE_CD_RDMP   2
#define SECR_TAX_YR_RDMP_AMT   13
#define SECR_NAME_ASSE         44
#define SECR_NAME_BILLING      44
#define SECR_ADDR_FRMTTD_CD    1
#define SECR_M_STR_NO          6
#define SECR_M_STR_FRACTION    1
#define SECR_M_STR_NAME        20
#define SECR_M_STR_DIR         2
#define SECR_M_STR_TYPE_CODE   2
#define SECR_M_CITY            15
#define SECR_M_STATE_CODE      2
#define SECR_M_ZIP_CODE        9
#define SECR_M_AD_UNIT_TYPE    3
#define SECR_M_AD_UNIT_ID      4
#define SECR_S_STR_NO          6
#define SECR_S_STR_FRACTION    1
#define SECR_S_STR_NAME        20
#define SECR_S_STR_DIR         2
#define SECR_S_STR_TYPE_CODE   2
#define SECR_S_CITY            15
#define SECR_S_AD_UNIT_TYPE    3
#define SECR_S_AD_UNIT_ID      4
#define SECR_S_STATE_CODE      2
#define SECR_S_ZIP_CODE        9
#define SECR_ATN               11
#define SECR_TAX_SALE_FEE      7
#define SECR_INT_AD_FEE        7
#define SECR_NEWS_AD_FEE       7
#define SECR_PERS_CON_FEE      7
#define SECR_BAD_CHK_FEE       7
#define SECR_FILLER            296

// SECR.TXT
typedef struct _tKerSecr
{
   char  Redemption_No   [SECR_REDEMPTION_NO    ]; // 1
   char  Dt_Default      [SECR_DT_DEFAULT       ]; // 8
   char  Tax_Year        [SECR_TAX_YEAR         ]; // 16
   char  Bill_No         [SECR_BILL_NO          ]; // 20
   char  Bill_No_Ext     [SECR_BILL_NO_EXT      ]; // 27
   char  Default_Amt     [SECR_DEFAULT_AMT      ]; // 29
   char  Atn_Default     [SECR_ATN_DEFAULT      ]; // 42
   char  Dt_Pwr_To_Sell  [SECR_DT_PWR_TO_SELL   ]; // 53
   char  Rec_Doc_No_Pts  [SECR_REC_DOC_NO_PTS   ]; // 61
   char  Dt_Pts_Recorded [SECR_DT_PTS_RECORDED  ]; // 70
   char  Dt_Rcsn_Ln_Fee  [SECR_DT_RCSN_LN_FEE   ]; // 78
   char  Dt_Recisn_Recrd [SECR_DT_RECISN_RECRD  ]; // 86
   char  Rec_Doc_No_Rcsn [SECR_REC_DOC_NO_RCSN  ]; // 94
   char  Rcsn_Lien_Fee   [SECR_RCSN_LIEN_FEE    ]; // 103
   char  Sec_Rdmp_Fee    [SECR_SEC_RDMP_FEE     ]; // 110
   char  Te_No           [SECR_TE_NO            ]; // 117
   char  Dt_Redeemed     [SECR_DT_REDEEMED      ]; // 127
   char  Search_Fee      [SECR_SEARCH_FEE       ]; // 135
   char  Rdmp_Status     [SECR_RDMP_STATUS      ]; // 142
         // ' ' = NO DELINQUENCY.
         // 'A' = NORMAL ACTIVE DELINQUENCY.
         // 'R' = REDEEMED.
         // 'S' = SOLD AT TAX AUCTION.
         // 'P' = PARCEL SPLIT.
         // 'D' = REDEMPTION DELETED.
         // 'N' = DUAL ABSTRACT REDEMPTION.
   char  Rdmp_Status_Pri [SECR_RDMP_STATUS_PRI  ]; // 143
   char  Install_Status  [SECR_INSTALL_STATUS   ]; // 144
         // ' ' = NO INSTALLMENT PLAN.
         // 'A' = ACTIVE INSTALLMENT PLAN.
         // 'D' = INSTALLMENT PLAN DEFAULTED.
         // 'B' = BANKRUPTCY.
         // 'I' = INSTALLMENT PLAN INVALID - BAD CHECK.
         // 'R' = INSTALLMENT PLAN REMOVED.
   char  Dt_Inst_Pln_Str [SECR_DT_INST_PLN_STR  ]; // 145
   char  Dt_Inst_Pln_Dfl [SECR_DT_INST_PLN_DFL  ]; // 153
   char  Event_Key       [SECR_EVENT_KEY        ]; // 161
   char  Dt_Sldbid_Offrd [SECR_DT_SLDBID_OFFRD  ]; // 173
   char  Dt_Sldbid_Sold  [SECR_DT_SLDBID_SOLD   ]; // 181
   char  Dt_Pubauc_Offrd [SECR_DT_PUBAUC_OFFRD  ]; // 189
   char  Dt_Pubauc_Sold  [SECR_DT_PUBAUC_SOLD   ]; // 197
   char  Dt_Tax_Sale_Rcd [SECR_DT_TAX_SALE_RCD  ]; // 205
   char  Rec_Doc_Tax_Sal [SECR_REC_DOC_TAX_SAL  ]; // 213
   char  Tax_Sale_Amt    [SECR_TAX_SALE_AMT     ]; // 222
   char  Default_No      [SECR_DEFAULT_NO       ]; // 235
   char  Rdmp_Pn_Amt_01  [SECR_RDMP_PN_AMT_01   ]; // 248
   char  Rdmp_Pn_Amt_02  [SECR_RDMP_PN_AMT_02   ]; // 261
   char  Rdmp_Pn_Amt_03  [SECR_RDMP_PN_AMT_03   ]; // 274
   char  Rdmp_Pn_Amt_04  [SECR_RDMP_PN_AMT_04   ]; // 287
   char  Rdmp_Pn_Amt_05  [SECR_RDMP_PN_AMT_05   ]; // 300
   char  Rdmp_Pn_Amt_06  [SECR_RDMP_PN_AMT_06   ]; // 313
   char  Rdmp_Pn_Amt_07  [SECR_RDMP_PN_AMT_07   ]; // 326
   char  Rdmp_Pn_Amt_08  [SECR_RDMP_PN_AMT_08   ]; // 339
   char  Rdmp_Pn_Amt_09  [SECR_RDMP_PN_AMT_09   ]; // 352
   char  Rdmp_Pn_Amt_10  [SECR_RDMP_PN_AMT_10   ]; // 365
   char  Rdmp_Pn_Amt_11  [SECR_RDMP_PN_AMT_11   ]; // 378
   char  Rdmp_Pn_Amt_12  [SECR_RDMP_PN_AMT_12   ]; // 391
   char  Rdmp_Pn_Amt_13  [SECR_RDMP_PN_AMT_13   ]; // 404
   char  Rdmp_Pn_Amt_14  [SECR_RDMP_PN_AMT_14   ]; // 417
   char  Rdmp_Pn_Amt_15  [SECR_RDMP_PN_AMT_15   ]; // 430
   char  Amount_Coll     [SECR_AMOUNT_COLL      ]; // 443 - The amount collected as payment of the redemption
   char  Undeliver_Ind   [SECR_UNDELIVER_IND    ]; // 456
   char  Bad_Check_Ind   [SECR_BAD_CHECK_IND    ]; // 457
   char  Bankruptcy_Ind  [SECR_BANKRUPTCY_IND   ]; // 458
   char  Bnkr_Crtcase_No [SECR_BNKR_CRTCASE_NO  ]; // 459
   char  Dt_Bnkrptcy_Fld [SECR_DT_BNKRPTCY_FLD  ]; // 477
   char  Dt_Bnkrptcy_Clr [SECR_DT_BNKRPTCY_CLR  ]; // 485
   char  Dt_Bnk_Fnds_Rcv [SECR_DT_BNK_FNDS_RCV  ]; // 493
   char  Amt_Bnk_Fnd_Rcv [SECR_AMT_BNK_FND_RCV  ]; // 501
   char  Dt_Crtsy_Ntc_St [SECR_DT_CRTSY_NTC_ST  ]; // 514
   char  Dt_Pts_Notc_Prt [SECR_DT_PTS_NOTC_PRT  ]; // 522
   char  Dt_Posted       [SECR_DT_POSTED        ]; // 530
   char  Amt_Due_General [SECR_AMT_DUE_GENERAL  ]; // 538
   char  Amt_Due_Special [SECR_AMT_DUE_SPECIAL  ]; // 551
   char  Amt_Due_Spasmts [SECR_AMT_DUE_SPASMTS  ]; // 564
   char  Amt_Due_Dlq_Pen [SECR_AMT_DUE_DLQ_PEN  ]; // 577
   char  Amt_Due_Dlq_Cst [SECR_AMT_DUE_DLQ_CST  ]; // 590
   char  Amt_Valid_Ind   [SECR_AMT_VALID_IND    ]; // 603
   char  Rdmp_Fiche_No   [SECR_RDMP_FICHE_NO    ]; // 604
   char  Fiche_Row       [SECR_FICHE_ROW        ]; // 609
   char  Fiche_Col       [SECR_FICHE_COL        ]; // 610
   char  Fiche_Upd_Ind   [SECR_FICHE_UPD_IND    ]; // 612
   char  Inst_Pymnt_Amt  [SECR_INST_PYMNT_AMT   ]; // 615
   char  Inst_Dfl_Rea_Cd [SECR_INST_DFL_REA_CD  ]; // 625 Default reason: 1=current taxes not paid, 2=installment payment not paid, 3=Other
   char  Dt_Tax_Dflt_Pub [SECR_DT_TAX_DFLT_PUB  ]; // 626 Date of publication of notice of default of taxes
   char  Dflt_Pub_Ad_Ind [SECR_DFLT_PUB_AD_IND  ]; // 634
   char  Exclude_Cd_Rdmp [SECR_EXCLUDE_CD_RDMP  ]; // 635
   char  Tax_Yr_Rdmp_Amt [SECR_TAX_YR_RDMP_AMT  ]; // 637
   char  Name_Asse       [SECR_NAME_ASSE        ]; // 641
   char  Name_Billing    [SECR_NAME_BILLING     ]; // 685
   char  Addr_Frmttd_Cd  [SECR_ADDR_FRMTTD_CD   ]; // 729
   char  M_Str_No        [SECR_M_STR_NO         ]; // 730
   char  M_Str_Fraction  [SECR_M_STR_FRACTION   ]; // 736
   char  M_Str_Name      [SECR_M_STR_NAME       ]; // 737
   char  M_Str_Dir       [SECR_M_STR_DIR        ]; // 757
   char  M_Str_Type_Code [SECR_M_STR_TYPE_CODE  ]; // 759
   char  M_City          [SECR_M_CITY           ]; // 761
   char  M_State_Code    [SECR_M_STATE_CODE     ]; // 776
   char  M_Zip_Code      [SECR_M_ZIP_CODE       ]; // 778
   char  M_Ad_Unit_Type  [SECR_M_AD_UNIT_TYPE   ]; // 787
   char  M_Ad_Unit_Id    [SECR_M_AD_UNIT_ID     ]; // 790
   char  S_Str_No        [SECR_S_STR_NO         ]; // 794
   char  S_Str_Fraction  [SECR_S_STR_FRACTION   ]; // 800
   char  S_Str_Name      [SECR_S_STR_NAME       ]; // 801
   char  S_Str_Dir       [SECR_S_STR_DIR        ]; // 821
   char  S_Str_Type_Code [SECR_S_STR_TYPE_CODE  ]; // 823
   char  S_City          [SECR_S_CITY           ]; // 825
   char  S_Ad_Unit_Type  [SECR_S_AD_UNIT_TYPE   ]; // 840
   char  S_Ad_Unit_Id    [SECR_S_AD_UNIT_ID     ]; // 843
   char  S_State_Code    [SECR_S_STATE_CODE     ]; // 847
   char  S_Zip_Code      [SECR_S_ZIP_CODE       ]; // 849
   char  Atn             [SECR_ATN              ]; // 858
   char  Tax_Sale_Fee    [SECR_TAX_SALE_FEE     ]; // 869
   char  Int_Ad_Fee      [SECR_INT_AD_FEE       ]; // 876
   char  News_Ad_Fee     [SECR_NEWS_AD_FEE      ]; // 883
   char  Pers_Con_Fee    [SECR_PERS_CON_FEE     ]; // 890
   char  Bad_Chk_Fee     [SECR_BAD_CHK_FEE      ]; // 897
   char  Filler          [SECR_FILLER           ]; // 904
} KER_SECR;

#define SIZ_DIST_SIZ_DIST_CTL_NO    6
#define SIZ_DIST_FUND_NO            5
#define SIZ_DIST_SIZ_DIST_TYPE_CODE 2
#define SIZ_DIST_SCHOOL_SIZ_DIST_CD 2
#define SIZ_DIST_COMM_COLL_CODE     1
#define SIZ_DIST_STR_NO             6
#define SIZ_DIST_STR_NO_FRACTION    1
#define SIZ_DIST_STR_NAME           20
#define SIZ_DIST_STR_DIR            2
#define SIZ_DIST_STR_TYPE_CODE      2
#define SIZ_DIST_CITY               15
#define SIZ_DIST_STATE_CODE         2
#define SIZ_DIST_ZIP_CODE           9
#define SIZ_DIST_AD_UNIT_TYPE       3
#define SIZ_DIST_AD_UNIT_ID         4
#define SIZ_DIST_NAME_NOTIFY        44
#define SIZ_DIST_TAX_TYPE_CD        1
#define SIZ_DIST_PHONE_NO           10
#define SIZ_DIST_WRNT_ISSUE_IND     1
#define SIZ_DIST_DESCRIPT           30
#define SIZ_DIST_DT_DEBT_APRVD      8
#define SIZ_DIST_DT_INACTIVE        8
#define SIZ_DIST_FUND_NO_OPER       5
#define SIZ_DIST_RESP_ORG_CD        2
#define SIZ_DIST_RESP_LOC_CD        3
#define SIZ_DIST_BOND_YEARS         2
#define SIZ_DIST_SP_ASMT_IND        1
#define SIZ_DIST_TOTAL_BOND_AMT     6
#define SIZ_DIST_SA_PURGE_IND       1
#define SIZ_DIST_FUND_TYPE_CODE     2
#define SIZ_DIST_TRAN_ID_NO_FRST    10
#define SIZ_DIST_TRAN_ID_NO_LCHG    10
#define SIZ_DIST_TEETER_IND         1
#define SIZ_DIST_DEPT_NO            4
#define SIZ_DIST_VENDOR_NO          6

#define OFF_DIST_OFF_DIST_CTL_NO    1
#define OFF_DIST_FUND_NO            7-1
#define OFF_DIST_OFF_DIST_TYPE_CODE 12
#define OFF_DIST_SCHOOL_OFF_DIST_CD 14
#define OFF_DIST_COMM_COLL_CODE     16
#define OFF_DIST_STR_NO             17
#define OFF_DIST_STR_NO_FRACTION    23
#define OFF_DIST_STR_NAME           24
#define OFF_DIST_STR_DIR            44
#define OFF_DIST_STR_TYPE_CODE      46
#define OFF_DIST_CITY               48 
#define OFF_DIST_STATE_CODE         63 
#define OFF_DIST_ZIP_CODE           65 
#define OFF_DIST_AD_UNIT_TYPE       74 
#define OFF_DIST_AD_UNIT_ID         77 
#define OFF_DIST_NAME_NOTIFY        81 
#define OFF_DIST_TAX_TYPE_CD        125 
#define OFF_DIST_PHONE_NO           126-1 
#define OFF_DIST_WRNT_ISSUE_IND     136 
#define OFF_DIST_DESCRIPT           137-1 
#define OFF_DIST_DT_DEBT_APRVD      167 
#define OFF_DIST_DT_INACTIVE        175 
#define OFF_DIST_FUND_NO_OPER       183 
#define OFF_DIST_RESP_ORG_CD        188 
#define OFF_DIST_RESP_LOC_CD        190 
#define OFF_DIST_BOND_YEARS         193 
#define OFF_DIST_SP_ASMT_IND        195 
#define OFF_DIST_TOTAL_BOND_AMT     196 // ???
#define OFF_DIST_SA_PURGE_IND       202 
#define OFF_DIST_FUND_TYPE_CODE     203 
#define OFF_DIST_TRAN_ID_NO_FRST    205 
#define OFF_DIST_TRAN_ID_NO_LCHG    215 
#define OFF_DIST_TEETER_IND         225 
#define OFF_DIST_DEPT_NO            226 
#define OFF_DIST_VENDOR_NO          230 

// Standard codes
// H=Homeowners
// D=Disabled Veterans
// V=Veterans
// C=Church
// L=Library
// M=Museums
// R=Religion
// T=Tribal Housing
// W=Welfare
// U=College
// P=Public School
// E=Cemetery
// I=Hospital
// S=School
// X=Misc exempt
// Y=Military

IDX_TBL4 KER_Exemption[] = 
{
   "AH", "X", 2, 1,    // ?
   "CE", "E", 2, 1,    // 
   "CM", "E", 2, 1,    // 
   "CO", "X", 2, 1,    // ?
   "C",  "C", 1, 1,    // 
   "DA", "D", 2, 1,    // DIS VET - TOTINC 
   "DB", "D", 2, 1,    // DIS VET - BLIND 
   "DI", "D", 2, 1,    // DIS VET - BLDINC 
   "DV", "D", 2, 1,    // DIS VET - TOTAL
   "H",  "H", 1, 1,    // 
   "LE", "X", 1, 1,    // ?
   "L",  "L", 1, 1,    // 
   "M",  "M", 1, 1,    // 
   "R",  "R", 1, 1,    // 
   "S",  "S", 1, 1,    // 
   "SS", "Y", 2, 1,    // SOLDIER/SAILOR 
   "VE", "V", 2, 1,    // 
   "WH", "I", 2, 1,    // WELFARE - HOSPITAL  
   "WL", "X", 2, 1,    // ?
   "WR", "R", 2, 1,    // WELFARE - RELIGIOUS 
   "WS", "S", 2, 1,    // WELFARE - SCHOOL  
   "W",  "W", 1, 1,    // 
   "","",0,0
};

#endif
