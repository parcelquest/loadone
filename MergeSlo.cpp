/**************************************************************************
 *
 * To do: 1) Add supplemental
 *
 * Notes:
 *    - Load lien (-L) and load roll (-U) should be run independently
 *    - LDR input file is in MDB format for 2010 & 2011.  This may change next year.
 *    - New roll layout effective 07/2011
 *
 *    Prop 8
 *      We can get pretty close using a couple calculations according to Steve Wolfinger. 
 *      Prop 8's are all those properties where:
 *      Taxable Value < Base Year Value AND Use Code is NOT equal to 022
 *          Use Code = 022 = Williamson Act Open Space Contracts whose taxable value is reduced 
 *          by the contract, but for whom the base year value is still tracked in case the contract
 *          is cancelled.
 *      The above calculation will give us all Prop 8's plus a few disaster properties whose 
 *      taxable value is also less than its base year value.  (Presumably, the disaster properties
 *      are tracked this way because the assumption is that whatever structure was destroyed will
 *      eventually be rebuilt and will be given the original base year value even though the 
 *      replacement structure is new.  We wouldn't want to penalize disaster victims.)
 *
 * To load lien date data:
 *    -CSLO -L -Lc [-Xl] [-X8] [-Us|Ms]
 * To load update roll:
 *    -CSLO -U -Xs[i] -Xa [-X8]
 * To load tax file:
 *    -CSLO -T|-Ut
 *
 * 02/12/2009 8.5.11    First version in CSV format.  Original version is in MDB.
 * 02/25/2009 8.5.12    Final testing version.
 * 03/23/2009 8.7.1.1   Format STORIES as 99.9.  We no longer multiply by 10.
 * 07/29/2009 9.1.4     Add Slo_ExtrLien() and -Xl option.
 * 02/06/2010 9.4.0     Extending LEGAL to LEGAL1
 * 02/23/2010 9.4.3     Fix CARE_OF issue.
 * 04/15/2010 9.5.4     If Usecode is 022, turn ON Ag Preserve
 * 07/03/2010 10.0.1    Rename Slo_CreatePQ4() to Slo_Load_LDR()
 * 07/08/2010 10.1.1    Copy Slo_CreatePQ4() and related functions from LoadMdb project
 *                      to process LDR file in MDB format.
 * 08/10/2010 10.1.10   Modify Slo_MergeRoll() to add OTHEXE to TOTAL_EXE due to new
 *                      record layout. Modify Slo_MergeSAdr() to fix situs city.
 * 12/17/2010 10.3.7    Add Slo_ExtrProp8() to extract Prop8 file.  Modify Slo_MergeLien()
 *                      to apply Prop8 flag when ValueCode3=41 or ValueCode4=43.
 * 04/10/2011 10.5.4    Modify Prop8 flag setting by checking for value 41 or 43 in both Code3 & Code4.
 * 07/01/2011 11.0.0    Populate S_HSENO. Add value code 31 (mobile home), 33 for MH prop8.
 *                      Modify Slo_MergeLien() to check for all possible prop8 from code 2 thru 4.
 * 08/12/2011 11.1.8    Modify Slo_ExtrChar()
 * 04/19/2012 11.9.2    Convert SLO_SALE to SCSAL_REC format for SQL import.
 *                      Remove all non-sale transactions from sale history section in R01.
 * 04/30/2012 11.9.2.2  Modify Slo_ExtrSale1() to set NoneSale_Flg='Y' only when DocType is "QC".
 * 07/17/2012 12.1.2    Check ValueCode4-ValueCode7 for Prop8 value.
 * 07/22/2012 12.2.2    Fix Zoning bug in Slo_MergeRoll().
 * 07/10/2013 13.2.3    Modify Slo_MergeLien() to check for all possible value in the
 *                      first 7.  8 throu 15 are currently empty.
 * 10/15/2013 13.11.2.1 Use updateVesting() to update Vesting and Etal flag.
 * 03/03/2016 15.6.0    Modify Slo_MergeRoll() to drop off unsecured parcels.  Also
 *                      assign ALTAPN and create maplink for M/H and Time Share.
 *                      Rename Slo_MergePQ4() to Slo_Load_Roll().
 * 05/09/2016 15.9.4    Modify Slo_ExtrChar() and Slo_MergeRoll() to spot bad input record and prevent memory corruption.
 * 05/13/2016 15.9.4.1  Add UpdateTaxBase()
 * 05/18/2016 15.9.4.2  Sort both Cortac and TC file on APN & BillNum for best matching.
 * 07/12/2016 16.0.5    Modify Slo_MergeLien() to remove secondary ZONING to make it the same as 
 *                      Slo_MergeRoll().  Update getSloValue() to add value code 55 & 57.
 * 10/07/2016 16.3.1    Modify Slo_ParseTaxDetail() to create Agency record by using Tax_CreateAgencyCsv()
 * 01/11/2017 16.9.2    Modify Slo_UpdateTaxBase() to ignore header record and compare BillNum for matching update.
 *                      Modify Slo_ParseCortac() and Slo_ParseTaxDetail() to format BillNum to match with county website.
 * 01/12/2017 16.9.3    Turn all globals static.
 * 03/16/2017 16.12.3   Modify Slo_UpdateTaxBase() & Slo_ParseCortac() to set InstStatus & DueDate.
 *                      Modify Slo_ParseTaxDetail() to add phone number to Agency table and to add special
                        assessment to Items table.
 * 03/27/2017 16.13.1   Bug fix Slo_UpdateTaxBase().  Modify Slo_Load_Cortac() to check for new file before update.
 * 06/18/2017 16.15.2   Modify Slo_Load_Cortac() & Slo_ParseTaxDetail() to replace comma in BillNum with hyphen
 * 11/21/2017 17.5.0    Modify Slo_UpdateTaxBase() to update Tax_Base.TaxYear.  
 * 01/09/2018 17.5.5    Modify Slo_UpdateTaxBase() to skip TC records that are older than tax roll.
 *                      Modify Slo_ParseCortac() to set TotalDue. Modify Slo_Load_Cortac() to skip TC file if it's older than tax roll.
 * 04/05/2018 17.8.1    Add -Ut option to update tax using TC file.  Remove dup record in TC file in Slo_Load_Cortac()
 * 04/16/2018 17.10.1   Remove situs before update in Slo_MergeSAdr() to fix a situs bug.
 * 04/30/2018 17.10.5   Fix TotalDue in Slo_UpdateTaxBase() and SUBTOTAL in Slo_ParseTaxDetail()
 * 08/17/2018 18.3.2    Fix HSENO with zero prefix in Slo_MergeSAdr() & Slo_MergeSAdrX()
 * 01/14/2019 18.8.2    Add new Slo_ExtrChar() & Slo_MergeStdChar() to create standard char file in preparation for MI extract.
 *                      Modify Slo_Load_Roll() to use new Slo_MergeStdChar() instead of Slo_MergeChar().
 * 02/18/2019 18.9.6    Modify Slo_ExtrChar() to add BldgSeq.
 * 06/11/2019 18.12.8   Use SLO.TCF to load Tax_Base instead of Cortax file.  Modify Slo_ParseTaxDetail() to drop items 
 *                      that is not in the current year and populate TaxYear with Bill Year.  
 * 07/11/2019 19.0.2    Add new functions Slo_MergeLien2(), Slo_MergeSAdr2(), Slo_MergeMAdr2(), Slo_CreateLienRec2(),
 *                      Slo_ExtrLien2() and modify Slo_Load_LDR() to support new LDR CSV format.  Use Slo_CityZip.txt
 *                      (extracted from SLO_ROLL table) to populate city name.
 * 08/30/2019 19.2.2    Fix LAND & IMPR in Slo_MergeLien2() & Slo_CreateLienRec2()
 *                      Modify getSloValue() to use only code 1 & 95 for LAND and 21 & 97 for IMPR
 * 11/20/2019 19.5.4    Modify Slo_UpdateTaxBase() to skip update of TC file is older than cortac.
 *                      Modify Slo_ParseCortac() & Slo_ParseTaxDetail() to skip records based on billed year instead 
 *                      of roll year since they might have escape bill from previous year but bill on current year with
 *                      different bill numbers. Modify Slo_Load_Cortac() to change sort command on both cortac and TC file.
 *                      Modify Slo_Load_TaxDetail() to skip unsecured records and to update tax base TotalRate.
 * 06/17/2020 19.10.2   Add -xn & -Mn option.
 * 07/18/2020 20.1.6    Fix bug when -Xa and -L on the same command.
 * 07/20/2020 20.2.1    Fix Slo_MergeSAdr() & Slo_MergeSAdr2() to use city/Zip from GIS extract as needed.
 * 09/10/2020 20.2.12   Modify Slo_ExtrChar() & Slo_MergeRoll() to used cDelim instead of hard code delimiter.
 * 10/15/2020 20.2.18   Add Slo_Load_Supplemental() to add supplemental bill to TaxBase.
 *            20.2.19   Modify Slo_ParseTaxDetail() to ignore detail record of supplemental bill.
 * 10/30/2020 20.4.1    Modify Slo_MergeLien2() & Slo_MergeRoll() to populate PQZoning.
 * 01/28/2021 20.7.2    Add Slo_Update_Tax() to process data based on TCF file, ignore extra records from Cortac file.
 *                      Check file date of Cortac and TCF then deciding on which function to use.
 * 05/07/2021 20.7.18   Set FirePlace='M' when more than 9 in Slo_ExtrChar().
 * 08/24/2021 21.1.6    Modify Slo_MergeSAdr() for special case of "PARK AVE" and b-p "005162".
 * 10/18/2021 21.2.8    Fix Paid Status in Slo_UpdateTaxBase().
 * 10/20/2021 21.2.9    Add Slo_Load_TaxDelq() to process CortacTaxDataPrior.txt
 * 12/01/2021 21.4.2    Fix LotSqft Slo_Extr_SLOChar().
 * 10/09/2022 22.2.6    Modify tax loading process to check for TCF and supplemental files.
 *                      If sup file not avail, import tax base only.
 * 07/17/2024 24.0.5    Modify Slo_Load_LDR() to update UseCode & Zoning from old R01 file.
 *                      Modify Slo_MergeLien2() to support LDR 2024 where some fields have been removed.
 *
 ***************************************************************************/

#include "stdafx.h"
#include "Prodlib.h"
#include "hlAdo.h"
#include "CountyInfo.h"
#include "R01.h"
#include "RecDef.h"
#include "Logs.h"
#include "Utils.h"
#include "Tables.h"
#include "XlatTbls.h"
#include "doOwner.h"
#include "doSort.h"
#include "formatApn.h"
#include "SaleRec.h"
#include "LoadOne.h"
#include "MergeSlo.h"
#include "UseCode.h"
#include "LOExtrn.h"
#include "PQ.h"
#include "Tax.h"
#include "CharRec.h"
#include "Situs.h"
#include "NdcExtr.h"

 static XLAT_CODE  asTopo[] =
{
   // Value, lookup code, value length
   "LEV", "L", 3,               // LEVEL
   "GEN", "G", 3,               // GENTLE
   "HIL", "H", 2,               // HILLY
   "MOD", "M", 2,               // MODERATE
   "ROL", "R", 2,               // ROLLING
   "SLO", "S", 2,               // SLOPE
   "STE", "T", 2,               // STEEP
   "VAR", "V", 2,               // VARIED
   "UNK", "U", 2,               // UNKNOWN
   "",   "",  0
};

static XLAT_CODE  asAccess[] =
{
   // Value, lookup code, value length
   "PAVED PUBLIC",      "N", 8,  // PAVED PUBLIC
   "PAVED EASEMENT",    "L", 8,  // PAVED EASEMENT
   "PAVED PRIVATE",     "M", 8,  // PAVED PRIVATE
   "PAVED",             "K", 2,  // PAVED
   "UNKNOWN",           "U", 2,  // UNKNOWN
   "DIRT EASEMENT",     "A", 8,  // DIRT EASEMENT
   "DIRT PRIVATE",      "B", 8,  // DIRT PRIVATE
   "DIRT PUBLIC",       "C", 8,  // DIRT PUBLIC
   "GRAVEL EASEMENT",   "D", 9,  // GRAVEL EASEMENT
   "GRAVEL PRIVATE",    "E", 9,  // GRAVEL PRIVATE
   "GRAVEL PUBLIC",     "F", 9,  // GRAVEL PUBLIC
   "LANDLOCKED",        "G", 8,  // LANDLOCKED
   "NOT DEVELOPED EA",  "H", 16, // NOT DEVELOPED EASEMENT
   "NOT DEVELOPED PR",  "I", 16, // NOT DEVELOPED PRIVATE
   "NOT DEVELOPED PU",  "J", 16, // NOT DEVELOPED PUBLIC
   "SEASONAL EASEMENT", "P", 11, // SEASONAL EASEMENT
   "SEASONAL PRIVATE",  "Q", 11, // SEASONAL PRIVATE
   "SEASONAL PUBLIC",   "R", 11, // SEASONAL PUBLIC
   "SEASONAL",          "O", 2,  // SEASONAL
   "","",0
};

hlAdo    hSloRoll;
hlAdoRs  rsSloRoll;

static long    lCharMatch, lCharSkip, lSaleMatch, lSaleSkip, iApnOffset, lTaxSkip, lTaxMatch, lTaxRollDate;
static char    acPublicFile[_MAX_PATH], acPublicTbl[_MAX_PATH], acPublicKey[_MAX_PATH], acLastApn[32];
static char    acRollKey[_MAX_PATH], acRollTbl[_MAX_PATH], acSecuredMdb[_MAX_PATH];
static FILE    *fdChar;
extern FILE    *fdSale;

/******************************** Slo_XlatUseCode ****************************
 *
 * Return pointer to the correct use code.
 *
 *****************************************************************************/

char *Slo_XlatUseCode(char *pCntyUse)
{
   int   iTmp, iCmp;

   iTmp = 0;
   while (Slo_UseTbl[iTmp].acCntyUse[0])
   {
      iCmp = memcmp(Slo_UseTbl[iTmp].acCntyUse, pCntyUse, 3);
      if (!iCmp)
         return (char *)&Slo_UseTbl[iTmp].acStdUse[0];
      else if (iCmp > 0)
         break;
      else
         iTmp++;
   }

   return NULL;
}

/********************************* Slo_ExtrChar *****************************
 *
 * Extract characteristic from roll file
 *
 ****************************************************************************/

int Slo_ExtrChar(char *pRollFile, char *pCharFile)
{
   FILE     *fdRoll;
   STDCHAR  myCharRec;
   CString  strFld;

   char     *pRoll, acTmp[_MAX_PATH], acRollRec[MAX_RECSIZE], acCode[16], *pTmp;
   int      iRet, iTmp, iBldgNum;
   long     lCnt=0, lTmp, lLotAcres;
   __int64  lLotSqft;
   double   dTmp;

   LogMsgD("Extract STDCHAR from SLO Roll ...");

   // Open Roll file
   LogMsg("Open Roll file %s", pRollFile);
   fdRoll = fopen(pRollFile, "r");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", pRollFile);
      return -1;
   }

   // Open Char file
   LogMsg("Open Char file %s", pCharFile);
   fdChar = fopen(pCharFile, "w");
   if (fdChar == NULL)
   {
      LogMsg("***** Error creating Char file: %s\n", pCharFile);
      return -2;
   }

   // Loop to EOF  
   while (true)
   {
      if (!(pRoll = fgets((char *)&acRollRec[0], MAX_RECSIZE, fdRoll)))
         break;
      iTmp = replUnPrtChar(acRollRec, ' ');
      lCnt++;

      //iRet = ParseStringNQ1(pRoll, ',', MAX_FLD_TOKEN, apTokens);
      iRet = ParseStringNQ(pRoll, cDelim, MAX_FLD_TOKEN, apTokens);
      if (iRet != SLO_ROLL_ALTAPN+1)
      {
         LogMsg("***** Error: bad input APN=%s (%d)", apTokens[SLO_ROLL_APN], lCnt);
         continue;
      }

      // Ignore removed record
      if (!memcmp(apTokens[SLO_ROLL_STATUS], "REM", 3))
         continue;

#ifdef _DEBUG
      //if (strchr(apTokens[SLO_ROLL_LEGAL], 34))
      //   LogMsg("-----> Check Legal APN=%s", apTokens[SLO_ROLL_APN]);
#endif

      // Clear buffer
      memset((void *)&myCharRec, ' ', sizeof(STDCHAR));

      // APN
      memcpy(myCharRec.Apn, apTokens[SLO_ROLL_APN], strlen(apTokens[SLO_ROLL_APN]));

      // Zoning
      memcpy(myCharRec.Zoning, apTokens[SLO_ROLL_PRI_ZONE], strlen(apTokens[SLO_ROLL_PRI_ZONE]));

      // LotSqft/acres
      dTmp = atof(apTokens[SLO_ROLL_LOTSIZE]);
      if (dTmp > 0.0)
      {
         // This field sometimes called SizeCode
         if (*apTokens[SLO_ROLL_SIZE_TYPE] == 'A')
         {
            iTmp = atol(apTokens[SLO_ROLL_PRI_LUC]);

            // Code might be wrong.  So verify!
            if (dTmp > 500 && iTmp >= 110 && iTmp <= 129)
            {
               // Lot Sqft
               iTmp = sprintf(acTmp, "%u", (long)dTmp);
               memcpy(myCharRec.LotSqft, acTmp, iTmp);
               lLotAcres = (long)((dTmp / SQFT_FACTOR_1000)+0.5);
               iTmp = sprintf(acTmp, "%u", lLotAcres);
               memcpy(myCharRec.LotAcre, acTmp, iTmp);
            } else if (dTmp < 10000.0)
            {
               // Lot Acres
               lLotAcres = (long)(dTmp*1000.0);
               iTmp = sprintf(acTmp, "%u", lLotAcres);
               memcpy(myCharRec.LotAcre, acTmp, iTmp);
               lLotSqft = (__int64)(dTmp*SQFT_PER_ACRE);
               iTmp = sprintf(acTmp, "%u", lLotSqft);
               memcpy(myCharRec.LotSqft, acTmp, iTmp);
            } else
            {
               LogMsg("*** Huge LotAcres: %.2f, LUC=%d [%s]", dTmp, iTmp, apTokens[SLO_ROLL_APN]);
               lLotAcres = (long)(dTmp*1000.0);
               iTmp = sprintf(acTmp, "%u", lLotAcres);
               memcpy(myCharRec.LotAcre, acTmp, iTmp);
               lLotSqft = (__int64)(dTmp*SQFT_PER_ACRE);
               iTmp = sprintf(acTmp, "%u", lLotSqft);
               memcpy(myCharRec.LotSqft, acTmp, iTmp);
            }
         } else if (*apTokens[SLO_ROLL_SIZE_TYPE] == 'S' && dTmp > 100.0)
         {
            iTmp = sprintf(acTmp, "%u", (long)dTmp);
            memcpy(myCharRec.LotSqft, acTmp, iTmp);
            lLotAcres = (long)((dTmp / SQFT_FACTOR_1000)+0.5);
            iTmp = sprintf(acTmp, "%u", lLotAcres);
            memcpy(myCharRec.LotAcre, acTmp, iTmp);
         }
      }

      // Quality class
      if (*apTokens[SLO_ROLL_CLASS] > ' ' && memcmp(apTokens[SLO_ROLL_CLASS], "00", 2) )
      {
         memcpy(myCharRec.QualityClass, apTokens[SLO_ROLL_CLASS], strlen(apTokens[SLO_ROLL_CLASS]));

         if (isdigit(myCharRec.QualityClass[0]))
         {
            dTmp = atof(&myCharRec.QualityClass[0]);
         } else
         {
            myCharRec.BldgClass = myCharRec.QualityClass[0];
            dTmp = atof(&myCharRec.QualityClass[1]);
         }
         if (dTmp > 0.0)
         {
            sprintf(acTmp, "%.1f", dTmp);
            if (-1 != (iRet = Value2Code(acTmp, acCode, NULL)))
               myCharRec.BldgQual = acCode[0];
         }
      }

      // Year built
      lTmp = atol(apTokens[SLO_ROLL_YRBLT]);
      if (lTmp > 1800)
         memcpy(myCharRec.YrBlt, apTokens[SLO_ROLL_YRBLT], strlen(apTokens[SLO_ROLL_YRBLT]));

      // BldgSqft
      lTmp = atol(apTokens[SLO_ROLL_BLDGSQFT]);
      if (lTmp > 0)
      {
         iTmp = sprintf(acTmp, "%d", lTmp);
         memcpy(myCharRec.BldgSqft, acTmp, iTmp);
      }

      lTmp = atol(apTokens[SLO_ROLL_ADDLSQFT]);
      if (lTmp > 0)
      {
         iTmp = sprintf(acTmp, "%d", lTmp);
         memcpy(myCharRec.MiscSqft, acTmp, iTmp);
      }

      // Parking type
      if (*apTokens[SLO_ROLL_PARKTYPE] == 'C')
         myCharRec.ParkType[0] = 'C';
      else if (*apTokens[SLO_ROLL_PARKTYPE] == 'G')
         myCharRec.ParkType[0] = 'Z';

      // Parking spaces
      lTmp = atol(apTokens[SLO_ROLL_PARKSPACE]);
      if (lTmp > 0)
      {
         iTmp = sprintf(acTmp, "%d", lTmp);
         memcpy(myCharRec.ParkSpace, acTmp, iTmp);
      }

      // Heat/Cool
      if (!memcmp(apTokens[SLO_ROLL_HEATING], "FORCE", 5))
         myCharRec.Heating[0] = 'B';
      else if (!memcmp(apTokens[SLO_ROLL_HEATING], "HEAT N", 6))
         myCharRec.Heating[0] = 'Y';

      if (!memcmp(apTokens[SLO_ROLL_COOLING], "AIR C", 5))
         myCharRec.Heating[0] = 'Y';

      // Rooms
      lTmp = atol(apTokens[SLO_ROLL_ROOMS]);
      if (lTmp > 0)
      {
         iTmp = sprintf(acTmp, "%d", lTmp);
         memcpy(myCharRec.Rooms, acTmp, iTmp);
      }

      // Beds
      lTmp = atol(apTokens[SLO_ROLL_BEDS]);
      if (lTmp > 0)
      {
         iTmp = sprintf(acTmp, "%d", lTmp);
         memcpy(myCharRec.Beds, acTmp, iTmp);
      }

      // Baths
      dTmp = atof(apTokens[SLO_ROLL_BATHS]);
      if (dTmp > 0.0)
      {
         if (strlen(apTokens[SLO_ROLL_BATHS]) > 3)
         {
            strFld = apTokens[SLO_ROLL_BATHS];
            // If decimal is > 5, make it full bath
            if (strFld.GetAt(3) == '5')
               myCharRec.HBaths[0] = '1';
            else if (strFld.GetAt(3) > '5')
               dTmp += 0.5;
            else if (strFld.GetAt(3) > '0')
               myCharRec.HBaths[0] = '1';
         }
         iTmp = sprintf(acTmp, "%d", (long)dTmp);
         memcpy(myCharRec.FBaths, acTmp, iTmp);
      }

#ifdef _DEBUG
      //if (!memcmp(myCharRec.Apn, "001064020", 9))
      //   lTmp = 0;
#endif

      // Levels/Stories
      lTmp = atol(apTokens[SLO_ROLL_LEVELS]);
      if (lTmp > 0)
      {
         if (bDebug && lTmp > 9)
            LogMsg("*** Please verify number of stories for %.*s (%d)", apTokens[SLO_ROLL_APN], lTmp);

         sprintf(acTmp, "%d.0  ", lTmp);
         memcpy(myCharRec.Stories, acTmp, SIZ_STORIES);
      }

      // Fire place
      iTmp = atol(apTokens[SLO_ROLL_FP]);
      if (iTmp > 9)
         myCharRec.Fireplace[0] = 'M';
      else if (iTmp > 0)
         myCharRec.Fireplace[0] = *apTokens[SLO_ROLL_FP];
      //else
      //   LogMsg("*** Bad FirePlace %s [%s]", apTokens[SLO_ROLL_FP], apTokens[SLO_ROLL_APN]);

      // Water/Sewer/Gas/Elec
      strFld = apTokens[SLO_ROLL_UTILITY];
      if (strFld.Find("WATER") != -1)
         myCharRec.HasWater = 'Y';
      if (strFld.Find("SEWER") != -1)
         myCharRec.HasSewer = 'Y';
      if (strFld.Find("GAS") != -1)
         myCharRec.HasGas = 'Y';
      if (strFld.Find("ELEC") != -1)
         myCharRec.HasElectric = 'Y';

      // Topography
      if (*apTokens[SLO_ROLL_TOPOGRAPHY] > ' ')
      {
         pTmp = findXlatCodeA(apTokens[SLO_ROLL_TOPOGRAPHY], &asTopo[0]);
         if (pTmp)
            myCharRec.Topo[0] = *pTmp;
      }

      // Access
      if (*apTokens[SLO_ROLL_ACCESS] > ' ')
      {
         pTmp = findXlatCodeA(apTokens[SLO_ROLL_ACCESS], &asAccess[0]);
         if (pTmp)
            myCharRec.Access[0] = *pTmp;
      }

      // Resident - multi unit indicator
      iBldgNum = atol(apTokens[SLO_ROLL_RESIDENT]);
      if (iBldgNum > 0 && iBldgNum < 100)
      {
         iTmp = sprintf(acTmp, "%d", iBldgNum);
         memcpy(myCharRec.BldgSeqNo, acTmp, iTmp);
      }

      myCharRec.CRLF[0] = '\n';
      myCharRec.CRLF[1] = '\0';
      fputs((char *)&myCharRec, fdChar);

      if (!(lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdRoll)
      fclose(fdRoll);
   if (fdChar)
      fclose(fdChar);

   LogMsg("Total CHAR records output:   %u\n", lCnt);

   return 0;
}

int Slo_Extr_SLOChar(char *pRollFile, char *pCharFile)
{
   FILE     *fdRoll;
   SLO_CHAR myCharRec;
   CString  strFld;

   char     *pRoll, acTmp[_MAX_PATH], acRollRec[MAX_RECSIZE];
   int      iRet, iTmp;
   long     lCnt=0, lTmp, lLotAcres;
   __int64  lLotSqft;

   double   dTmp;

   LogMsgD("Extract CHAR from SLO Roll ...");

   // Open Roll file
   LogMsg("Open Roll file %s", pRollFile);
   fdRoll = fopen(pRollFile, "r");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", pRollFile);
      return -1;
   }

   // Open Char file
   LogMsg("Open Char file %s", pCharFile);
   fdChar = fopen(pCharFile, "w");
   if (fdChar == NULL)
   {
      LogMsg("***** Error creating Char file: %s\n", pCharFile);
      return -2;
   }

   // Loop to EOF  
   while (true)
   {
      if (!(pRoll = fgets((char *)&acRollRec[0], MAX_RECSIZE, fdRoll)))
         break;
      iTmp = replUnPrtChar(acRollRec, ' ');
      lCnt++;

      iRet = ParseStringNQ1(pRoll, ',', MAX_FLD_TOKEN, apTokens);
      if (iRet != SLO_ROLL_ALTAPN+1)
      {
         LogMsg("***** Error: bad input APN=%s (%d)", apTokens[SLO_ROLL_APN], lCnt);
         continue;
      }

      // Clear buffer
      memset((void *)&myCharRec, ' ', sizeof(SLO_CHAR));

      // APN
      memcpy(myCharRec.Apn, apTokens[SLO_ROLL_APN], strlen(apTokens[SLO_ROLL_APN]));

      // Usecode
      memcpy(myCharRec.UseCode1, apTokens[SLO_ROLL_PRI_LUC], strlen(apTokens[SLO_ROLL_PRI_LUC]));
      memcpy(myCharRec.UseCode2, apTokens[SLO_ROLL_2ND_LUC], strlen(apTokens[SLO_ROLL_2ND_LUC]));

      // Zoning
      memcpy(myCharRec.Zoning, apTokens[SLO_ROLL_PRI_ZONE], strlen(apTokens[SLO_ROLL_PRI_ZONE]));

      // LotSqft/acres
      dTmp = atof(apTokens[SLO_ROLL_LOTSIZE]);
      if (dTmp > 0.0)
      {
         // This field sometimes called SizeCode
         if (*apTokens[SLO_ROLL_SIZE_TYPE] == 'A')
         {
            iTmp = atol(apTokens[SLO_ROLL_PRI_LUC]);

            // Code might be wrong.  So verify!
            if (dTmp > 500 && iTmp >= 110 && iTmp <= 129)
            {
               // Lot Sqft
               iTmp = sprintf(acTmp, "%u", (long)dTmp);
               memcpy(myCharRec.LotSqft, acTmp, iTmp);
               lLotAcres = (long)((dTmp / SQFT_FACTOR_1000)+0.5);
               iTmp = sprintf(acTmp, "%u", lLotAcres);
               memcpy(myCharRec.LotAcres, acTmp, iTmp);
            } else if (dTmp < 10000.0)
            {
               // Lot Acres
               lLotAcres = (long)(dTmp*1000.0);
               iTmp = sprintf(acTmp, "%u", lLotAcres);
               memcpy(myCharRec.LotAcres, acTmp, iTmp);
               lLotSqft = (long)(dTmp*SQFT_PER_ACRE);
               iTmp = sprintf(acTmp, "%u", lLotSqft);
               memcpy(myCharRec.LotSqft, acTmp, iTmp);
            }
         } else if (*apTokens[SLO_ROLL_SIZE_TYPE] == 'S' && dTmp > 100.0)
         {
            iTmp = sprintf(acTmp, "%u", (long)dTmp);
            memcpy(myCharRec.LotSqft, acTmp, iTmp);
            lLotAcres = (long)((dTmp / SQFT_FACTOR_1000)+0.5);
            iTmp = sprintf(acTmp, "%u", lLotAcres);
            memcpy(myCharRec.LotAcres, acTmp, iTmp);
         }
      }

      // Qual class
      if (memcmp(apTokens[SLO_ROLL_CLASS], "00", 2) )
         memcpy(myCharRec.QualityClass, apTokens[SLO_ROLL_CLASS], strlen(apTokens[SLO_ROLL_CLASS]));

      // Year built
      lTmp = atol(apTokens[SLO_ROLL_YRBLT]);
      if (lTmp > 1800)
         memcpy(myCharRec.Year_Blt, apTokens[SLO_ROLL_YRBLT], strlen(apTokens[SLO_ROLL_YRBLT]));

      // BldgSqft
      lTmp = atol(apTokens[SLO_ROLL_TOTALSQFT]);
      if (lTmp > 0)
      {
         iTmp = sprintf(acTmp, "%d", lTmp);
         memcpy(myCharRec.BuildingSize, acTmp, iTmp);

         lTmp = atol(apTokens[SLO_ROLL_BLDGSQFT]);
         if (lTmp > 0)
         {
            iTmp = sprintf(acTmp, "%d", lTmp);
            memcpy(myCharRec.BldgSqft, acTmp, iTmp);
         }

         lTmp = atol(apTokens[SLO_ROLL_ADDLSQFT]);
         if (lTmp > 0)
         {
            iTmp = sprintf(acTmp, "%d", lTmp);
            memcpy(myCharRec.AddlSqft, acTmp, iTmp);
         }
      }

      // Parking type
      if (*apTokens[SLO_ROLL_PARKTYPE] == 'C')
         myCharRec.ParkType = 'C';
      else if (*apTokens[SLO_ROLL_PARKTYPE] == 'G')
         myCharRec.ParkType = 'Z';

      // Parking spaces
      lTmp = atol(apTokens[SLO_ROLL_PARKSPACE]);
      if (lTmp > 0)
         myCharRec.ParkSpaces[0] = *apTokens[SLO_ROLL_PARKSPACE];

      // Heat/Cool
      if (!memcmp(apTokens[SLO_ROLL_HEATING], "FORCE", 5))
         myCharRec.Heating = 'B';
      else if (!memcmp(apTokens[SLO_ROLL_HEATING], "HEAT N", 6))
         myCharRec.Heating = 'Y';

      if (!memcmp(apTokens[SLO_ROLL_COOLING], "AIR C", 5))
         myCharRec.Heating = 'Y';

      // Rooms
      lTmp = atol(apTokens[SLO_ROLL_ROOMS]);
      if (lTmp > 0)
      {
         iTmp = sprintf(acTmp, "%d", lTmp);
         memcpy(myCharRec.Rooms, acTmp, iTmp);
      }

      // Beds
      lTmp = atol(apTokens[SLO_ROLL_BEDS]);
      if (lTmp > 0)
      {
         iTmp = sprintf(acTmp, "%d", lTmp);
         memcpy(myCharRec.Bedrooms, acTmp, iTmp);
      }

      // Baths
      dTmp = atof(apTokens[SLO_ROLL_BATHS]);
      if (dTmp > 0.0)
      {
         if (strlen(apTokens[SLO_ROLL_BATHS]) > 3)
         {
            strFld = apTokens[SLO_ROLL_BATHS];
            // If decimal is > 5, make it full bath
            if (strFld.GetAt(3) == '5')
               myCharRec.HalfBaths[0] = '1';
            else if (strFld.GetAt(3) > '5')
               dTmp += 0.5;
            else if (strFld.GetAt(3) > '0')
               myCharRec.HalfBaths[0] = '1';
         }
         iTmp = sprintf(acTmp, "%d", (long)dTmp);
         memcpy(myCharRec.FullBaths, acTmp, iTmp);
      }

      // Fire place
      iTmp = atol(apTokens[SLO_ROLL_FP]);
      if (iTmp > 9)
         myCharRec.Fireplaces[0] = 'M';
      else if (iTmp > 0)
         myCharRec.Fireplaces[0] = *apTokens[SLO_ROLL_FP];

      // Removed?
      if (!memcmp(apTokens[SLO_ROLL_STATUS], "REM", 3))
         myCharRec.Removed = 'R';

      // Water/Sewer/Gas/Elec
      strFld = apTokens[SLO_ROLL_UTILITY];
      if (strFld.Find("WATER") != -1)
         myCharRec.HasWater = 'Y';
      if (strFld.Find("SEWER") != -1)
         myCharRec.HasSewer = 'Y';
      if (strFld.Find("GAS") != -1)
         myCharRec.HasGas = 'Y';
      if (strFld.Find("ELEC") != -1)
         myCharRec.HasElec = 'Y';

      // Topography
      memcpy(myCharRec.Topography, apTokens[SLO_ROLL_TOPOGRAPHY], strlen(apTokens[SLO_ROLL_TOPOGRAPHY]));
      // Access
      memcpy(myCharRec.Access, apTokens[SLO_ROLL_ACCESS], strlen(apTokens[SLO_ROLL_ACCESS]));
      // Resident - multi unit indicator
      memcpy(myCharRec.Resident, apTokens[SLO_ROLL_RESIDENT], strlen(apTokens[SLO_ROLL_RESIDENT]));
      // Levels
      memcpy(myCharRec.Levels, apTokens[SLO_ROLL_LEVELS], strlen(apTokens[SLO_ROLL_LEVELS]));

      myCharRec.CRLF[0] = '\n';
      myCharRec.CRLF[1] = '\0';
      fputs((char *)&myCharRec, fdChar);

      if (!(lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdRoll)
      fclose(fdRoll);
   if (fdChar)
      fclose(fdChar);

   LogMsg("\nTotal CHAR records output:   %u", lCnt);

   return 0;
}

/****************************** Slo_MergeStdChar *****************************
 *
 * Some parcel has more than one entries.  We total up Beds, Baths, Sqft.  Other
 * fields are from the first entity only.
 *
 *****************************************************************************/

int Slo_MergeStdChar(char *pOutbuf)
{
   static   char acRec[2048], *pRec=NULL;
   char     acTmp[256];
   long     lTmp, lBldgSqft, lParkSpc, lYrBlt, lLotSqft, lLotAcres, lAddSqft;
   int      iRet, iLoop, iBeds, iFBath, iHBath, iBldgNum, iFp, iRooms;

   STDCHAR *pChar;

   // Get first Char rec for first call
   if (!pRec && !lCharMatch)
      pRec = fgets(acRec, 2048, fdChar);

   pChar = (STDCHAR *)pRec;

   do
   {
      if (!pRec)
      {
         fclose(fdChar);
         fdChar = NULL;
         return -1;      // EOF
      }

      // Compare Apn
      iLoop = memcmp(pOutbuf, pChar->Apn, iApnLen);
      if (iLoop > 0)
      {
         //if (bDebug)
         //   LogMsg0("Skip Char rec  %.*s", CSIZ_APN, pChar->Apn);
         pRec = fgets(acRec, 2048, fdChar);
         lCharSkip++;
      }
   } while (iLoop > 0);

   // If not match, return
   if (iLoop)
      return 1;

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "056322001", 9))
   //   lTmp = 0;
#endif

   // Init variables
   iRooms=iRet=iBeds=iFBath=iHBath=iBldgNum=iFp=0;
   lAddSqft=lLotSqft=lLotAcres=lBldgSqft=lParkSpc=lYrBlt=0;
   memset(pOutbuf+OFF_YR_BLT, ' ', SIZ_YR_BLT);
   memset(pOutbuf+OFF_FIRE_PL, ' ', SIZ_FIRE_PL);
   memset(pOutbuf+OFF_BLDG_SF, ' ', SIZ_BLDG_SF);
   memset(pOutbuf+OFF_LOT_SQFT, ' ', SIZ_LOT_SQFT);
   memset(pOutbuf+OFF_LOT_ACRES, ' ', SIZ_LOT_ACRES);
   memset(pOutbuf+OFF_PARK_SPACE, ' ', SIZ_PARK_SPACE);
   memset(pOutbuf+OFF_ROOMS, ' ', SIZ_ROOMS);
   memset(pOutbuf+OFF_BEDS, ' ', SIZ_BEDS);
   memset(pOutbuf+OFF_BATH_F, ' ', SIZ_BATH_F);
   memset(pOutbuf+OFF_BATH_H, ' ', SIZ_BATH_H);

   while (!iLoop)
   {
      // YrBlt
      lTmp = atoin(pChar->YrBlt, SIZ_YR_BLT);
      if (lTmp > 1800 && lTmp > lYrBlt)
      {
         lYrBlt = lTmp;
         memcpy(pOutbuf+OFF_YR_BLT, pChar->YrBlt, SIZ_YR_BLT);
      }

      iBldgNum++;
      // Quality/class
      if (iBldgNum < 2)
      {
         if (pChar->BldgClass > ' ')
         {
            *(pOutbuf+OFF_BLDG_CLASS) = pChar->BldgClass;
            *(pOutbuf+OFF_BLDG_QUAL) = pChar->BldgQual;
         }

         // Air Cond
         *(pOutbuf+OFF_AIR_COND) = pChar->Cooling[0];
         // Heat
         *(pOutbuf+OFF_HEAT) = pChar->Heating[0];
      } else
      {
         sprintf(acTmp, "%*u", SIZ_BLDGS, iBldgNum);
         memcpy(pOutbuf+OFF_BLDGS, acTmp, SIZ_BLDGS);
      }

#ifdef _DEBUG
      //if (iBldgNum > 1)
      //   lTmp = 0;
#endif
      // Fireplace
      *(pOutbuf+OFF_FIRE_PL) = pChar->Fireplace[0];

      // BldgSqft
      lBldgSqft += atoin(pChar->BldgSqft, SIZ_CHAR_SQFT);
      lAddSqft += atoin(pChar->MiscSqft, SIZ_CHAR_SQFT);
      lTmp = lBldgSqft+lAddSqft;
      if (lTmp > 10)
      {
         sprintf(acTmp, "%*u", SIZ_BLDG_SF, lTmp);
         memcpy(pOutbuf+OFF_BLDG_SF, acTmp, SIZ_BLDG_SF);
      }

      // LotSqft
      lTmp = atoin(pChar->LotSqft, SIZ_LOT_SQFT);
      if (lTmp > 0 && lTmp != lLotSqft)
      {
         lLotSqft += lTmp;
         sprintf(acTmp, "%*u", SIZ_LOT_SQFT, lLotSqft);
         memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);

         lLotAcres += atoin(pChar->LotAcre, SIZ_LOT_ACRES);
         if (lLotAcres > 10)
         {
            sprintf(acTmp, "%*u", SIZ_LOT_ACRES, lLotAcres);
            memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);
         }
      }

      // Parking
      lTmp = atoin(pChar->ParkSpace, SIZ_PARK_SPACE);
      if (lTmp > 0 && lTmp != lParkSpc)
      {
         lParkSpc += lTmp;
         sprintf(acTmp, "%d      ", lParkSpc);
         memcpy(pOutbuf+OFF_PARK_SPACE, acTmp, SIZ_PARK_SPACE);
         *(pOutbuf+OFF_PARK_TYPE) = pChar->ParkType[0];
      }

      // Rooms
      iRooms += atoin(pChar->Rooms, SIZ_ROOMS);
      if (iRooms > 0)
      {
         sprintf(acTmp, "%*u", SIZ_ROOMS, iRooms);
         memcpy(pOutbuf+OFF_ROOMS, acTmp, SIZ_ROOMS);
      }

      // Beds
      iBeds += atoin(pChar->Beds, SIZ_BEDS);
      if (iBeds > 0)
      {
         sprintf(acTmp, "%*u", SIZ_BEDS, iBeds);
         memcpy(pOutbuf+OFF_BEDS, acTmp, SIZ_BEDS);
      }

      // Baths
      iFBath += atoin(pChar->FBaths, SIZ_BATH_F);
      if (iFBath > 0)
      {
         sprintf(acTmp, "%*u", SIZ_BATH_F, iFBath);
         memcpy(pOutbuf+OFF_BATH_F, acTmp, SIZ_BATH_F);
      }
      iHBath += atoin(pChar->HBaths, SIZ_BATH_H);
      if (iHBath > 0)
      {
         sprintf(acTmp, "%*u", SIZ_BATH_H, iHBath);
         memcpy(pOutbuf+OFF_BATH_H, acTmp, SIZ_BATH_H);
      }

      // Levels/Stories
      memcpy(pOutbuf+OFF_STORIES, pChar->Stories, SIZ_STORIES);

      // Others
      if (pChar->HasSewer > ' ')
         *(pOutbuf+OFF_SEWER) = pChar->HasSewer;
      if (pChar->HasWater > ' ')
         *(pOutbuf+OFF_WATER) = pChar->HasWater;

      // Get next Char rec
      pRec = fgets(acRec, 1024, fdChar);
      if (!pRec)
         break;

      iLoop = memcmp(pOutbuf, pChar->Apn, iApnLen);
   }

   lCharMatch++;
   return 0;
}

// Merge CHAR to R01 using SLO_CHAR records
int Slo_MergeChar(char *pOutbuf)
{
   static   char acRec[1024], *pRec=NULL;
   char     acTmp[256], acCode[32];
   long     lTmp, lBldgSqft, lParkSpc, lYrBlt, lLotSqft, lLotAcres;
   int      iRet, iTmp, iLoop, iBeds, iFBath, iHBath, iBldgNum, iFp, iRooms;

   SLO_CHAR *pChar;

   // Get first Char rec for first call
   if (!pRec && !lCharMatch)
      pRec = fgets(acRec, 1024, fdChar);

   pChar = (SLO_CHAR *)pRec;

   do
   {
      if (!pRec)
      {
         fclose(fdChar);
         fdChar = NULL;
         return -1;      // EOF
      }

      // Compare Apn
      iLoop = memcmp(pOutbuf, pChar->Apn, iApnLen);
      if (iLoop > 0)
      {
         //if (bDebug)
         //   LogMsg0("Skip Char rec  %.*s", CSIZ_APN, pChar->Apn);
         pRec = fgets(acRec, 1024, fdChar);
         lCharSkip++;
      }
   } while (iLoop > 0);

   // If not match, return
   if (iLoop)
      return 1;

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "056322001", 9))
   //   lTmp = 0;
#endif

   // Init variables
   iRooms=iRet=iBeds=iFBath=iHBath=iBldgNum=iFp=0;
   lLotSqft=lLotAcres=lBldgSqft=lParkSpc=lYrBlt=0;
   while (!iLoop)
   {
      if (pChar->Removed != 'R')
      {
         // YrBlt
         lTmp = atoin(pChar->Year_Blt, SIZ_YR_BLT);
         if (lTmp > 1800 && lTmp > lYrBlt)
         {
            lYrBlt = lTmp;
            memcpy(pOutbuf+OFF_YR_BLT, pChar->Year_Blt, SIZ_YR_BLT);
         }

         iBldgNum++;
         // Quality/class
         if (iBldgNum < 2)
         {
            if (pChar->QualityClass[0] > ' ')
            {
               *(pOutbuf+OFF_BLDG_CLASS) = pChar->QualityClass[0];
               iTmp = atoin(&pChar->QualityClass[1], 2);
               if (iTmp > 0)
               {
                  if (pChar->QualityClass[1] == '0')        // D05.0A
                  {
                     memcpy(acTmp, &pChar->QualityClass[2], 3);
                     acTmp[3] = 0;
                  } else if (pChar->QualityClass[2] >= 'A') // M7A
                  {
                     sprintf(acTmp, "%d.0", iTmp);
                  } else                                    // M5.5A
                  {
                     memcpy(acTmp, &pChar->QualityClass[1], 3);
                     acTmp[3] = 0;
                  }
                  if (-1 != (iRet = Value2Code(acTmp, acCode, NULL)))
                     *(pOutbuf+OFF_BLDG_QUAL) = acCode[0];
               }
            }

            // Air Cond
            *(pOutbuf+OFF_AIR_COND) = pChar->Cooling;
            // Heat
            *(pOutbuf+OFF_HEAT) = pChar->Heating;
         } else
         {
            sprintf(acTmp, "%*u", SIZ_BLDGS, iBldgNum);
            memcpy(pOutbuf+OFF_BLDGS, acTmp, SIZ_BLDGS);
         }


#ifdef _DEBUG
         //if (iBldgNum > 1)
         //   lTmp = 0;
#endif
         // Fireplace
         iFp += atoin(pChar->Fireplaces, SIZ_FIRE_PL);
         if (iFp > 0 && iFp < 99)
         {
            sprintf(acTmp, "%*u", SIZ_FIRE_PL, iFp);
            memcpy(pOutbuf+OFF_FIRE_PL, acTmp, SIZ_FIRE_PL);
         }

         // BldgSqft
         lBldgSqft += atoin(pChar->BuildingSize, SIZ_BLDG_SF);
         if (lBldgSqft > 10)
         {
            sprintf(acTmp, "%*u", SIZ_BLDG_SF, lBldgSqft);
            memcpy(pOutbuf+OFF_BLDG_SF, acTmp, SIZ_BLDG_SF);
         }

         // LotSqft
         lTmp = atoin(pChar->LotSqft, SIZ_LOT_SQFT);
         if (lTmp > 0 && lTmp != lLotSqft)
         {
            lLotSqft += lTmp;
            sprintf(acTmp, "%*u", SIZ_LOT_SQFT, lLotSqft);
            memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);

            lLotAcres += atoin(pChar->LotAcres, SIZ_LOT_ACRES);
            if (lLotAcres > 10)
            {
               sprintf(acTmp, "%*u", SIZ_LOT_ACRES, lLotAcres);
               memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);
            }
         }

         // Parking
         lTmp = atoin(pChar->ParkSpaces, SIZ_PARK_SPACE);
         if (lTmp > 0 && lTmp != lParkSpc)
         {
            lParkSpc += lTmp;
            sprintf(acTmp, "%d      ", lParkSpc);
            memcpy(pOutbuf+OFF_PARK_SPACE, acTmp, SIZ_PARK_SPACE);
            *(pOutbuf+OFF_PARK_TYPE) = pChar->ParkType;
         }

         // Rooms
         iRooms += atoin(pChar->Rooms, SIZ_ROOMS);
         if (iRooms > 0)
         {
            sprintf(acTmp, "%*u", SIZ_ROOMS, iRooms);
            memcpy(pOutbuf+OFF_ROOMS, acTmp, SIZ_ROOMS);
         }

         // Beds
         iBeds += atoin(pChar->Bedrooms, SIZ_BEDS);
         if (iBeds > 0)
         {
            sprintf(acTmp, "%*u", SIZ_BEDS, iBeds);
            memcpy(pOutbuf+OFF_BEDS, acTmp, SIZ_BEDS);
         }

         // Baths
         iFBath += atoin(pChar->FullBaths, SIZ_BATH_F);
         if (iFBath > 0)
         {
            sprintf(acTmp, "%*u", SIZ_BATH_F, iFBath);
            memcpy(pOutbuf+OFF_BATH_F, acTmp, SIZ_BATH_F);
         }
         iHBath += atoin(pChar->HalfBaths, SIZ_BATH_H);
         if (iHBath > 0)
         {
            sprintf(acTmp, "%*u", SIZ_BATH_H, iHBath);
            memcpy(pOutbuf+OFF_BATH_H, acTmp, SIZ_BATH_H);
         }

         // Levels/Stories
         lTmp = atoin(pChar->Levels, SLO_CSIZ_LEVELS);
         if (lTmp > 0)
         {
            if (bDebug && lTmp > 9)
               LogMsg("*** Please verify number of stories for %.*s (%d)", iApnLen, pOutbuf, lTmp);

            sprintf(acTmp, "%d.0  ", lTmp);
            memcpy(pOutbuf+OFF_STORIES, acTmp, SIZ_STORIES);
         }

         // Others
         if (pChar->HasSewer > ' ')
            *(pOutbuf+OFF_SEWER) = pChar->HasSewer;
         if (pChar->HasWater > ' ')
            *(pOutbuf+OFF_WATER) = pChar->HasWater;
      }

      // Get next Char rec
      pRec = fgets(acRec, 1024, fdChar);
      if (!pRec)
         break;

      iLoop = memcmp(pOutbuf, pChar->Apn, iApnLen);
   }

   lCharMatch++;
   return 0;
}

/******************************** Slo_MergeOwner *****************************
 *
 * Return 0 if successful, >0 if error
 *
 *****************************************************************************/

void Slo_MergeOwner(char *pOutbuf, LPCSTR pNames, LPCSTR pCareOf)
{
   int   iTmp;
   char  acTmp[128], acName1[128];
   char  *pTmp;

   OWNER myOwner;

   // Clear output buffer if needed
   removeNames(pOutbuf, true);

   iTmp = 0;
   strcpy(acTmp, pNames);
   quoteRem(acTmp);
   blankRem(acTmp);

   // Remove parenthesis
   if (pTmp=strchr(acTmp, '('))
      *pTmp = 0;

   // Save name1
   strcpy(acName1, acTmp);

   // Update vesting
   updateVesting(myCounty.acCntyCode, acTmp, pOutbuf+OFF_VEST, pOutbuf+OFF_ETAL_FLG);

   // Remove filtered words
   if (pTmp=strstr(acTmp, " TRE ETAL"))
      *pTmp = 0;
   else if (pTmp=strstr(acTmp, " ETUX"))
      *pTmp = 0;
   else if (pTmp=strstr(acTmp, " ETAL"))
      *pTmp = 0;
   else if (pTmp=strstr(acTmp, " LIFE EST"))
      *pTmp = 0;

   iTmp = strlen(acTmp);
   if (!memcmp((char *)&acTmp[iTmp-3], "TRE", 3))
      acTmp[iTmp-4] = 0;

   // Now parse owners
   splitOwner(acTmp, &myOwner, 0);
   if (myOwner.acName2[0] && strcmp(myOwner.acName1, myOwner.acName2))
   {
      iTmp = strlen(myOwner.acName1);
      memcpy(pOutbuf+OFF_NAME1, myOwner.acName1, (iTmp < SIZ_NAME1 ? iTmp:SIZ_NAME1));

      iTmp = strlen(myOwner.acName2);
      memcpy(pOutbuf+OFF_NAME2, myOwner.acName2, (iTmp < SIZ_NAME2 ? iTmp:SIZ_NAME2));
   } else
   {
      iTmp = strlen(acName1);
      memcpy(pOutbuf+OFF_NAME1, acName1, (iTmp < SIZ_NAME1 ? iTmp:SIZ_NAME1));
   }

   iTmp = strlen(myOwner.acSwapName);
   memcpy(pOutbuf+OFF_NAME_SWAP, myOwner.acSwapName, (iTmp < SIZ_NAME_SWAP ? iTmp:SIZ_NAME_SWAP));

   // Care of
   if (*pCareOf > ' ' && *pCareOf != '#' && *pCareOf != '(' && *pCareOf != '&')
   {
      iTmp = blankRem((char *)pCareOf);
      updateCareOf(pOutbuf, (char *)pCareOf, iTmp);
   }

}

/********************************* Slo_MergeAdr ******************************
 *
 * Return 0 if successful, >0 if error
 *
 *****************************************************************************/

void Slo_MergeMAdr(char *pOutbuf, LPCSTR pAdr1, LPCSTR pAdr2, LPCSTR pAdr3, LPCSTR pCity, 
                   LPCSTR pState, LPCSTR pZip, LPCSTR pZip4)
{
   char     *p3, *p0, *p1, *p2, *pCareOf, acCity[32];
   char     acAddr1[64], acAddr2[64], acAddr3[64], acTmp[64], acMailAdr[64], acCareOf[64];
   int      iTmp, iZip4;
   ADR_REC  sMailAdr;
   CString  strFld;

   // Clear old Mailing
   removeMailing(pOutbuf, false);
   memset((void *)&sMailAdr, 0, sizeof(ADR_REC));

   pCareOf = acCareOf;
   *pCareOf = 0;

   // Mail address
   if (*pAdr1 <= ' ')
      return;
   strcpy(acAddr1, pAdr1);
   strcpy(acAddr2, pAdr2);
   strcpy(acAddr3, pAdr3);
   if (!memcmp(pCity, "SLO", 3))
      strcpy(acCity, "SAN LUIS OBISPO");
   else
      strcpy(acCity, pCity);
   myTrim(acAddr1);
   myTrim(acAddr2);
   myTrim(acAddr3);
   myTrim(acCity);

   if (*pState == ' ')
   {
      if (!memcmp(acAddr1, "C/O", 3))
      {
         if (*(pOutbuf+OFF_CARE_OF) == ' ')
            updateCareOf(pOutbuf, acAddr1, strlen(acAddr1));
         sprintf(acMailAdr, "%s %s", acAddr2, acAddr3);
      } else
         sprintf(acMailAdr, "%s %s %s", acAddr1, acAddr2, acAddr3);

      vmemcpy(pOutbuf+OFF_M_STREET, acMailAdr, SIZ_M_STREET);
      vmemcpy(pOutbuf+OFF_M_ADDR_D, acMailAdr, SIZ_M_ADDR_D);

      memcpy(pOutbuf+OFF_M_CITY, acCity, strlen(acCity));
      memcpy(pOutbuf+OFF_M_ST, "  ", SIZ_M_ST);
      memcpy(pOutbuf+OFF_M_CTY_ST_D, acCity, strlen(acCity));

      return;
   }


   // Check for Care Of
   if (acAddr1[0] == '%' || !memcmp(acAddr1, "ATTN", 4) || !memcmp(acAddr1, "C/O", 3))
   {
      strcpy(pCareOf, acAddr1);
      acAddr1[0] = 0;
      p1 = acAddr2;
      p2 = acAddr3;
      p3 = NULL;
   } else if (acAddr2[0] && (acAddr2[0] == '%' || !memcmp(acAddr2, "ATTN", 4) || !memcmp(acAddr2, "C/O", 3)))
   {
      strcpy(pCareOf, acAddr2);
      acAddr2[0] = 0;
      p1 = acAddr1;
      p2 = acAddr3;
      p3 = NULL;
   } else if (acAddr3[0] && (acAddr3[0] == '%' || !memcmp(acAddr3, "ATTN", 4) || !memcmp(acAddr3, "C/O", 3)))
   {
      strcpy(pCareOf, acAddr3);
      acAddr3[0] = 0;
      p1 = acAddr1;
      p2 = acAddr2;
      p3 = NULL;
   } else
   {
      p1 = acAddr1;
      p2 = p3 = NULL;
      if (acAddr2[0] > ' ') p2 = acAddr2;
      if (acAddr3[0] > ' ') p3 = acAddr3;
   }

   p0 = NULL;
   strcpy(acMailAdr, p1);
   if (*pCareOf)
   {
      if (p2 && (atoi(p2) > 0 || !memcmp(p2, "PO BOX", 6)))
      {
         strcpy(acMailAdr, p2);
         p0 = p1;
      } else
      {
         strcpy(acMailAdr, p1);
         p0 = p2;
      }
   } else if (p3)
   {
      if (p3 && (atoi(p3) > 0 || !memcmp(p3, "PO BOX", 6)))
      {
         strcpy(acMailAdr, p3);
         p0 = p2;
      } else if (p2 && (atoi(p2) > 0 || !memcmp(p2, "PO BOX", 6)))
      {
         strcpy(acMailAdr, p2);
         p0 = p3;
      }
   } else if (p2)
   {
      if (p1 && (atoi(p1) > 0)  )
      {
         strcpy(acMailAdr, p1);
         p0 = p2;
      } else if (p2 && (atoi(p2) > 0 || !memcmp(p2, "PO BOX", 6)))
      {
         strcpy(acMailAdr, p2);
         p0 = p1;
      }
   }

   if (p0 && *p0)
   {
      if (*p0 == '#' || !memcmp(p0, "SUITE", 5) || !memcmp(p0, "APT", 3) || !memcmp(p0, "STE", 3)
         || !memcmp(p0, "SP ", 3) || !memcmp(p0, "SP#", 3))
      {
         strcat(acMailAdr, " ");
         strcat(acMailAdr, p0);
      }
   }

   // Remove multi-space
   blankRem(acMailAdr);
   strcpy(acTmp, acMailAdr);
   if (acTmp[0] >= '0' && acTmp[0] <= '9')
   {
      parseAdr1(&sMailAdr, acTmp);
      memcpy(pOutbuf+OFF_M_STRNUM, sMailAdr.strNum, strlen(sMailAdr.strNum));
      memcpy(pOutbuf+OFF_M_DIR, sMailAdr.strDir, strlen(sMailAdr.strDir));
      memcpy(pOutbuf+OFF_M_STREET, sMailAdr.strName, strlen(sMailAdr.strName));
      memcpy(pOutbuf+OFF_M_SUFF, sMailAdr.strSfx, strlen(sMailAdr.strSfx));
      memcpy(pOutbuf+OFF_M_UNITNO, sMailAdr.Unit, strlen(sMailAdr.Unit));
      memcpy(pOutbuf+OFF_M_ADDR_D, acAddr1, strlen(acAddr1));
   } else
   {
      iTmp = strlen(acTmp);
      if (iTmp > SIZ_M_STREET) iTmp = SIZ_M_STREET;
      memcpy(pOutbuf+OFF_M_STREET, acTmp, iTmp);
      memcpy(pOutbuf+OFF_M_ADDR_D, acTmp, iTmp);
   }

   // Mail city & state
   memcpy(pOutbuf+OFF_M_CITY, acCity, strlen(acCity));
   memcpy(pOutbuf+OFF_M_ST, pState, SIZ_M_ST);
   sprintf(acAddr2, "%s %s ", acCity, pState);

   // Mail zip
   iTmp = atol(pZip);
   if (iTmp > 99999)
   {
      sprintf(sMailAdr.Zip, "%.5s-%.4s", pZip, pZip+5);
      memcpy(pOutbuf+OFF_M_ZIP, pZip, SIZ_M_ZIP);
      memcpy(pOutbuf+OFF_M_ZIP4, pZip+5, SIZ_M_ZIP4);
      strcat(acAddr2, sMailAdr.Zip);
   } else if (iTmp > 0)
   {
      memcpy(pOutbuf+OFF_M_ZIP, pZip, SIZ_M_ZIP);

      iZip4 = atol(pZip4);
      if (iZip4 > 0)
      {
         sprintf(sMailAdr.Zip4, "%.4d", iZip4);
         memcpy(pOutbuf+OFF_M_ZIP4, sMailAdr.Zip4, SIZ_M_ZIP4);
         sprintf(sMailAdr.Zip, "%.5d-%.4d", iTmp, iZip4);
      } else
         sprintf(sMailAdr.Zip, "%.5d", iTmp);
      strcat(acAddr2, sMailAdr.Zip);
   }

   iTmp = strlen(acAddr2);
   if (iTmp > SIZ_M_CTY_ST_D) iTmp = SIZ_M_CTY_ST_D;
   memcpy(pOutbuf+OFF_M_CTY_ST_D, acAddr2, iTmp);

   // Check for Care Of
   if (*(pOutbuf+OFF_CARE_OF) == ' ' && *pCareOf > ' ')
      updateCareOf(pOutbuf, pCareOf, strlen(pCareOf));

}

/******************************** Slo_MergeSAdr ******************************
 *
 * Return 0 if successful, >0 if error
 *
 *****************************************************************************/

void Slo_MergeSAdr(char *pOutbuf)
{
   char     acAddr1[64], acAddr2[64], acTmp[64], acTmp1[64], cTmp;
   int      iTmp, iStrNo;
   CString  strFld, strName;

   // Situs
   acAddr1[0] = 0;
   acAddr2[0] = 0;

   if (*apTokens[SLO_ROLL_S_STREET] <= ' ' || *apTokens[SLO_ROLL_S_STREET] == '*')
      return;

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "005162045", 9))
   //   iTmp =  0;
#endif


   iStrNo = atol(apTokens[SLO_ROLL_S_HSENO]);
   if (iStrNo > 0)
   {
      // Clean up old situs to avoid left over from prev one
      removeSitus(pOutbuf);

      // Make situs strnum left justified so we can compare with mailing
      iTmp = sprintf(acAddr1, "%d", iStrNo);
      memcpy(pOutbuf+OFF_S_STRNUM, acAddr1, iTmp);
      // Save original HSENO
      memcpy(pOutbuf+OFF_S_HSENO, acAddr1, iTmp);

      cTmp = *apTokens[SLO_ROLL_S_DIR];
      if (cTmp == 'N' || cTmp == 'S' || cTmp == 'E' || cTmp == 'W')
      {
         sprintf(acTmp, " %c", cTmp);
         strcat(acAddr1, acTmp);
         *(pOutbuf+OFF_S_DIR) = cTmp;
      }

      if (*apTokens[SLO_ROLL_S_STREET] > ' ')
      {
         strcat(acAddr1, " ");
         strcat(acAddr1, myTrim(apTokens[SLO_ROLL_S_STREET]));
         memcpy(pOutbuf+OFF_S_STREET, apTokens[SLO_ROLL_S_STREET], strlen(apTokens[SLO_ROLL_S_STREET]));
      }

      // Special case
      if (!memcmp(apTokens[SLO_ROLL_S_STREET], "PARK", 4) && !memcmp(pOutbuf, "005162", 6))
      {
         strcat(acAddr1, " AVE");
         memcpy(pOutbuf+OFF_S_SUFF, "3  ", 3);
      } else if (*apTokens[SLO_ROLL_S_TYPE] > ' ')
      {
         strcat(acAddr1, " ");
         strcat(acAddr1, myTrim(apTokens[SLO_ROLL_S_TYPE]));

         iTmp = GetSfxCodeX(apTokens[SLO_ROLL_S_TYPE], acTmp);
         if (iTmp > 0)
         {
            Sfx2Code(acTmp, acTmp1);
            memcpy(pOutbuf+OFF_S_SUFF, acTmp1, SIZ_S_SUFF);
         }
      }

      // Unit #
      iTmp = atol(apTokens[SLO_ROLL_S_UNIT]);
      acTmp[0] = 0;
      if (iTmp > 0)
         sprintf(acTmp, " %d", iTmp);
      else if (*apTokens[SLO_ROLL_S_UNIT] >= 'A')
         sprintf(acTmp, " %s", apTokens[SLO_ROLL_S_UNIT]);
      if (acTmp[0])
      {
         strcat(acAddr1, acTmp);
         memcpy(pOutbuf+OFF_S_UNITNO, &acTmp[1], strlen(acTmp)-1);
      }
   } else
   {
      if (*apTokens[SLO_ROLL_S_STREET] > '0')
      {
         strcpy(acAddr1, myTrim(apTokens[SLO_ROLL_S_STREET]));
         memcpy(pOutbuf+OFF_S_STREET, apTokens[SLO_ROLL_S_STREET], strlen(apTokens[SLO_ROLL_S_STREET]));
      }
   }

   blankRem(acAddr1);
   memcpy(pOutbuf+OFF_S_ADDR_D, acAddr1, strlen(acAddr1));
   memcpy(pOutbuf+OFF_S_ST, "CA", 2);

   // Look up for city & zipcode
   if ((iStrNo > 0) &&
       (*(pOutbuf+OFF_S_STRNUM) == *(pOutbuf+OFF_M_STRNUM)) &&
       !memcmp(pOutbuf+OFF_S_STREET, pOutbuf+OFF_M_STREET, SIZ_M_STREET) )
   {
      // With the same street name & number, they are more likely the same address
      memcpy(acAddr2, pOutbuf+OFF_M_CITY, SIZ_M_CITY);
      myTrim(acAddr2, SIZ_M_CITY);

      City2Code(acAddr2, acTmp, pOutbuf);
      if (acTmp[0] > ' ')
      {
         memcpy(pOutbuf+OFF_S_CITY, acTmp, strlen(acTmp));
         memcpy(pOutbuf+OFF_S_ZIP, pOutbuf+OFF_M_ZIP, SIZ_S_ZIP);

         iTmp = sprintf(acAddr2, "%s CA %.5s", GetCityName(atol(acTmp)), pOutbuf+OFF_M_ZIP);
         memcpy(pOutbuf+OFF_S_CTY_ST_D, acAddr2, iTmp);
      } else if (*apTokens[SLO_ROLL_S_COMMUNITY] >= 'A' )
      {
         _strupr(apTokens[SLO_ROLL_S_COMMUNITY]);
         Abbr2Code(apTokens[SLO_ROLL_S_COMMUNITY], acTmp, acAddr2, pOutbuf);
         if (acTmp[0] > ' ')
         {
            memcpy(pOutbuf+OFF_S_CITY, acTmp, strlen(acTmp));
            strcat(acAddr2, " CA");
            iTmp = blankRem(acAddr2);
            memcpy(pOutbuf+OFF_S_CTY_ST_D, acAddr2, iTmp);
         }
      } 
   } else if (*apTokens[SLO_ROLL_S_COMMUNITY] >= 'A' )
   {
      _strupr(apTokens[SLO_ROLL_S_COMMUNITY]);
      Abbr2Code(apTokens[SLO_ROLL_S_COMMUNITY], acTmp, acAddr2, pOutbuf);
      if (acTmp[0] > ' ')
      {
         memcpy(pOutbuf+OFF_S_CITY, acTmp, strlen(acTmp));
         strcat(acAddr2, " CA");
         iTmp = blankRem(acAddr2);
         memcpy(pOutbuf+OFF_S_CTY_ST_D, acAddr2, iTmp);
      }
   } else
   // If not found, use GIS extract
   {
      // Look up for city & zipcode
      char acCity[64], acZip[32], acCode[32];
      if (fdCity && !getCityZip(pOutbuf, acCity, acZip, acCode, iApnLen))
      {
         // Get City & Zip from GIS
         //City2Code(acCity, acCode, pOutbuf);
         memcpy(pOutbuf+OFF_S_CITY, acCode, strlen(acCode));
         memcpy(pOutbuf+OFF_S_ZIP, acZip, strlen(acZip));
         memcpy(pOutbuf+OFF_S_ST, "CA", 2);
         iTmp = sprintf(acAddr2, "%s CA %s", acCity, acZip);
         memcpy(pOutbuf+OFF_S_CTY_ST_D, acAddr2, iTmp);  
      }
   }

}

/******************************* Slo_MergeSAdr2 ******************************
 *
 * This function is used to parse situs from new LDR file "1920SecRollDataFull.txt"
 * This file doesn't include city & zipcode
 *
 *****************************************************************************/

void Slo_MergeSAdr2(char *pOutbuf)
{
   char     acAddr1[64], acAddr2[64], acTmp[64], acTmp1[64], cTmp;
   int      iTmp, iStrNo;
   CString  strFld, strName;

   // Situs
   acAddr1[0] = 0;
   acAddr2[0] = 0;

   if (*apTokens[SLO_L_S_STREET] <= ' ' || *apTokens[SLO_L_S_STREET] == '*')
      return;

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "091714042", 9))
   //   iTmp =  0;
#endif


   iStrNo = atol(apTokens[SLO_L_S_STRNUM]);
   if (iStrNo > 0)
   {
      // Clean up old situs to avoid left over from prev one
      removeSitus(pOutbuf);

      // Make situs strnum left justified so we can compare with mailing
      iTmp = sprintf(acAddr1, "%d", iStrNo);
      memcpy(pOutbuf+OFF_S_STRNUM, acAddr1, iTmp);
      // Save original HSENO
      memcpy(pOutbuf+OFF_S_HSENO, acAddr1, iTmp);

      cTmp = *apTokens[SLO_L_S_DIR];
      if (cTmp == 'N' || cTmp == 'S' || cTmp == 'E' || cTmp == 'W')
      {
         sprintf(acTmp, " %c", cTmp);
         strcat(acAddr1, acTmp);
         *(pOutbuf+OFF_S_DIR) = cTmp;
      }

      if (*apTokens[SLO_L_S_STREET] > ' ')
      {
         strcat(acAddr1, " ");
         strcat(acAddr1, myTrim(apTokens[SLO_L_S_STREET]));
         memcpy(pOutbuf+OFF_S_STREET, apTokens[SLO_L_S_STREET], strlen(apTokens[SLO_L_S_STREET]));
      }

      if (*apTokens[SLO_L_S_TYPE] > ' ')
      {
         strcat(acAddr1, " ");
         strcat(acAddr1, myTrim(apTokens[SLO_L_S_TYPE]));

         iTmp = GetSfxCodeX(apTokens[SLO_L_S_TYPE], acTmp);
         if (iTmp > 0)
         {
            Sfx2Code(acTmp, acTmp1);
            memcpy(pOutbuf+OFF_S_SUFF, acTmp1, SIZ_S_SUFF);
         }
      }

      // Unit #
      iTmp = atol(apTokens[SLO_L_S_UNIT]);
      acTmp[0] = 0;
      if (iTmp > 0)
         sprintf(acTmp, " %d", iTmp);
      else if (*apTokens[SLO_L_S_UNIT] >= 'A')
         sprintf(acTmp, " %s", apTokens[SLO_L_S_UNIT]);
      if (acTmp[0] > ' ')
      {
         strcat(acAddr1, acTmp);
         memcpy(pOutbuf+OFF_S_UNITNO, &acTmp[1], strlen(acTmp)-1);
      }
   } else
   {
      if (*apTokens[SLO_L_S_STREET] > '0')
      {
         strcpy(acAddr1, myTrim(apTokens[SLO_L_S_STREET]));
         memcpy(pOutbuf+OFF_S_STREET, apTokens[SLO_L_S_STREET], strlen(apTokens[SLO_L_S_STREET]));
      }
   }

   blankRem(acAddr1);
   memcpy(pOutbuf+OFF_S_ADDR_D, acAddr1, strlen(acAddr1));

   // Look up for city & zipcode
   char acCity[64], acZip[32], acCode[32];
   if (fdCity && !getCityZip(pOutbuf, acCity, acZip, acCode, iApnLen))
   {
      // Get City & Zip from GIS
      //City2Code(acCity, acCode, pOutbuf);
      memcpy(pOutbuf+OFF_S_CITY, acCode, strlen(acCode));
      memcpy(pOutbuf+OFF_S_ZIP, acZip, strlen(acZip));
      memcpy(pOutbuf+OFF_S_ST, "CA", 2);
      iTmp = sprintf(acAddr2, "%s CA %s", acCity, acZip);
      memcpy(pOutbuf+OFF_S_CTY_ST_D, acAddr2, iTmp);  
   }
}

void Slo_MergeSAdrX(char *pOutbuf)
{
   char     acAddr1[64], acAddr2[64], acTmp[64], acTmp1[64], cTmp;
   int      iTmp, iStrNo;
   CString  strFld, strName;

   // Situs
   acAddr1[0] = 0;
   acAddr2[0] = 0;

   strName = rsSloRoll.GetItem("SITUS_STREET");
   if (strName.IsEmpty() || strName.GetAt(0) == '*')
      return;

   strcpy(acAddr1, rsSloRoll.GetItem("SITUS_NUM"));
   iStrNo = atoi(myTrim(acAddr1));
   if (iStrNo > 0)
   {
      // Save original HSENO
      memset(pOutbuf+OFF_S_HSENO, ' ', SIZ_S_HSENO);

      // Make situs strnum left justified so we can compare with mailing
      iTmp = sprintf(acAddr1, "%d", iStrNo);
      memcpy(pOutbuf+OFF_S_STRNUM, acAddr1, iTmp);
      memcpy(pOutbuf+OFF_S_HSENO, acAddr1, iTmp);

      strFld = rsSloRoll.GetItem("SITUS_DIRECTION");
      strFld.TrimRight();
      if (!strFld.IsEmpty())
      {
         cTmp = strFld.GetAt(0);
         if (cTmp == 'N')
            strFld = " N";
         else if (cTmp == 'S')
            strFld = " S";
         else if (cTmp == 'E')
            strFld = " E";
         else if (cTmp == 'W')
            strFld = " W";
         else
            strFld = "";

         strcat(acAddr1, strFld);
         memcpy(pOutbuf+OFF_S_DIR, strFld, strFld.GetLength());
      }

      strFld = rsSloRoll.GetItem("SITUS_STREET");
      strFld.TrimRight();
      if (!strFld.IsEmpty())
      {
         strcat(acAddr1, " ");
         strcat(acAddr1, strFld);
         memcpy(pOutbuf+OFF_S_STREET, strFld, strFld.GetLength());
      }

      strFld = rsSloRoll.GetItem("SITUS_TYPE");
      strFld.TrimRight();
      if (!strFld.IsEmpty())
      {
         strcat(acAddr1, " ");
         strcat(acAddr1, strFld);

         iTmp = GetSfxCodeX(strFld.GetBuffer(0), acTmp);
         if (iTmp > 0)
         {
            Sfx2Code(acTmp, acTmp1);
            memcpy(pOutbuf+OFF_S_SUFF, acTmp1, SIZ_S_SUFF);
         }
      }

   } else
   {
      strFld = rsSloRoll.GetItem("SITUS_STREET");
      strFld.TrimRight();
      if (!strFld.IsEmpty() && strFld.GetAt(0) > '0')
      {
         strcpy(acAddr1, strFld);
         memcpy(pOutbuf+OFF_S_STREET, strFld, strFld.GetLength());
      }
   }

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "009779016", 9))
   //   iTmp =  0;
#endif

   memcpy(pOutbuf+OFF_S_ADDR_D, acAddr1, strlen(acAddr1));
  
   // Look up for city & zipcode
   if ((iStrNo > 0) && 
       (*(pOutbuf+OFF_S_STRNUM) == *(pOutbuf+OFF_M_STRNUM)) &&
       !memcmp(pOutbuf+OFF_S_STREET, pOutbuf+OFF_M_STREET, SIZ_M_STREET) )
   {
      // With the same street name & number, they are more likely the same address
      memcpy(acAddr2, pOutbuf+OFF_M_CITY, SIZ_M_CITY);
      myTrim(acAddr2, SIZ_M_CITY);

      City2Code(acAddr2, acTmp, pOutbuf);   
      if (acTmp[0] > ' ')
      {
         memcpy(pOutbuf+OFF_S_CITY, acTmp, strlen(acTmp));
         memcpy(pOutbuf+OFF_S_ST, "CA", 2);
         memcpy(pOutbuf+OFF_S_ZIP, pOutbuf+OFF_M_ZIP, SIZ_S_ZIP);

         iTmp = sprintf(acAddr2, "%s, CA %.5s", GetCityName(atol(acTmp)), pOutbuf+OFF_M_ZIP);
         memcpy(pOutbuf+OFF_S_CTY_ST_D, acAddr2, iTmp);
      }
   }
}

/********************************** getSloValue ******************************
 *
 * Decode values
 *    33: MH   - Prop8 value
 *    41: Land - Prop8 value
 *    43: Impr - Prop8 value
 *    47: TRACK OSC SITE FACT
 *    49: TRACK OSC IMPS FACT 
 *    73: POSTED IMPS ON UNSEC          
 *
 *****************************************************************************/

void getSloValue(LPCSTR strCode, LPCSTR strAmt, long alValues[], LPSTR strApn)
{
   long  lCode;

   if (!strCode || !*strCode || !strAmt || !*strAmt)
      return;

   lCode = atol(strCode);
   if (!lCode)
      return;

   switch (lCode)
   {
      case 1:     // Land to be factored
      case 95:    // Mineral value treated as LAND (OSC)
         if (alValues[SLO_LAND] > 0)
            LogMsg("+++ Recheck Land value for %.12s", strApn);
         alValues[SLO_LAND] += atol(strAmt);
         break;
      case 21:    // Impr to be factored
      case 97:    // OSC Impr
         if (alValues[SLO_IMPR] > 0)
            LogMsg("+++ Recheck Impr value for %.9s", strApn);
         alValues[SLO_IMPR] += atol(strAmt);
         break;
      case 75:    // PP
         alValues[SLO_PP] += atol(strAmt);
         break;
      case 77:    // Fixture
         alValues[SLO_FE] += atol(strAmt);
         break;
      case 31:    // IMPR - Mobile home
         alValues[SLO_IMPR] = atol(strAmt);
         alValues[SLO_MH]   = atol(strAmt);
         break;
      case 33:    // MH Prop8 values
      case 41:    // Land Prop8 values
      case 43:    // Impr Prop8 values
      case 47:    // TRACK OSC SITE FACT           
      case 49:    // TRACK OSC IMPR FACT   
      case 73:    // POSTED IMPS ON UNSEC          
      case 9:     // Section 11 Land
      case 11:    // PI Land factored
      case 13:    // Lease land - land value fact
      case 15:
      case 55:    // Mills Act Land (factored)
      case 81:
      case 83:    // Oil value treated as Land
      case 27:    // Leasehold Impr to be factored
      case 57:    // Mills Act Impr (factored)
         break;
      default:
         LogMsg("++Unknown value code %d, amt=%s, APN=%.9s", lCode, strAmt, strApn);
   }
}

/********************************** Slo_MergeRoll *****************************
 *
 * Exclude parcels with book > 100
 *   - Except 902: Time share
 *            910: Mobilehomes not on fee
 *            911: Mobilehomes on fee owned park
 *            912: Mobilehome separately assessed
 *            913: Mobilehomes on fee parcels
 *
 * Return 0 if successful, -1 if error, 1 if skip
 *
 ******************************************************************************/

int Slo_MergeRoll(char *pOutbuf, char *pRollRec, int iLen, int iFlag)
{
   char     acTmp[256], acTmp1[256];
   LONGLONG lTmp;
   int      iRet, iTmp;

   iRet = ParseStringNQ1(pRollRec, cDelim, MAX_FLD_TOKEN, apTokens);
   if (iRet != SLO_ROLL_ALTAPN+1)
   {
      LogMsg("***** Error: bad input record for APN=%s", apTokens[SLO_ROLL_APN]);
      return -1;
   }

   iTmp = atoin(apTokens[SLO_ROLL_APN], 3);
   if (iTmp >= 99 && iTmp != 902 && iTmp != 910 && iTmp != 911 && iTmp != 912 && iTmp != 913)
      return 1;
   if (!strcmp(apTokens[SLO_ROLL_APN], acLastApn))
      return 1;
   else
      strcpy(acLastApn, apTokens[SLO_ROLL_APN]);

   if (iFlag & CREATE_R01)
   {
      // Clear output buffer
      memset(pOutbuf, ' ', iRecLen);

      // Start copying data
      memcpy(pOutbuf, apTokens[SLO_ROLL_APN], strlen(apTokens[SLO_ROLL_APN]));
      *(pOutbuf+OFF_STATUS) = 'A';

      // AltApn - Physical APN
      memcpy(pOutbuf+OFF_ALT_APN, apTokens[SLO_ROLL_ALTAPN], strlen(apTokens[SLO_ROLL_ALTAPN]));

      // Format APN
      iRet = formatApn(apTokens[SLO_ROLL_APN], acTmp, &myCounty);
      memcpy(pOutbuf+OFF_APN_D, acTmp, iRet);

      // Create MapLink and output new record
      iRet = formatMapLink(apTokens[SLO_ROLL_ALTAPN], acTmp, &myCounty);
      memcpy(pOutbuf+OFF_MAPLINK, acTmp, iRet);

      // Create index map link
      if (getIndexPage(acTmp, acTmp1, &myCounty))
         memcpy(pOutbuf+OFF_IMAPLINK, acTmp1, iRet);

      // County code
      memcpy(pOutbuf+OFF_CO_NUM, "40SLO", 5);

      // Year assessed
      memcpy(pOutbuf+OFF_YR_ASSD, myCounty.acYearAssd, 4);

      // Land
      long lLand = atol(apTokens[SLO_ROLL_LAND]);
      if (lLand > 0)
      {
         sprintf(acTmp, "%*u", SIZ_LAND, lLand);
         memcpy(pOutbuf+OFF_LAND, acTmp, SIZ_LAND);
      }

      // Improve
      long lImpr = atol(apTokens[SLO_ROLL_IMPR]);
      if (lImpr > 0)
      {
         sprintf(acTmp, "%*u", SIZ_IMPR, lImpr);
         memcpy(pOutbuf+OFF_IMPR, acTmp, SIZ_IMPR);
      }

      // Other value: FixtureRealProperty, PPBusiness, PPMH
      long lFixt = atol(apTokens[SLO_ROLL_FIXTURE]);
      long lPers = atol(apTokens[SLO_ROLL_PERSPROP]);
      lTmp = lPers + lFixt;
      if (lTmp > 0)
      {
         sprintf(acTmp, "%*u", SIZ_OTHER, lTmp);
         memcpy(pOutbuf+OFF_OTHER, acTmp, SIZ_OTHER);

         if (lFixt > 0)
         {
            sprintf(acTmp, "%u        ", lFixt);
            memcpy(pOutbuf+OFF_FIXTR, acTmp, SIZ_FIXTR);
         }
         if (lPers > 0)
         {
            sprintf(acTmp, "%u        ", lPers);
            memcpy(pOutbuf+OFF_PERSPROP, acTmp, SIZ_PERSPROP);
         }
      }

      // Gross total
      lTmp += (lLand+lImpr);
      if (lTmp > 0)
      {
         sprintf(acTmp, "%*u", SIZ_GROSS, lTmp);
         memcpy(pOutbuf+OFF_GROSS, acTmp, SIZ_GROSS);
      }

      // Ratio
      if (lImpr > 0)
      {
         sprintf(acTmp, "%*u", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
         memcpy(pOutbuf+OFF_RATIO, acTmp, SIZ_RATIO);
      }

      // HO Exempt
      long lHOExe = atol(apTokens[SLO_ROLL_HOEXE]);
      if (lHOExe > 0)
         *(pOutbuf+OFF_HO_FL) = '1';      // 'Y'
      else
         *(pOutbuf+OFF_HO_FL) = '2';      // 'N'

      // Other exempt
      long lOtherExe = atol(apTokens[SLO_ROLL_OTHEXE]);
      lTmp = lHOExe + lOtherExe;
      if (lTmp > 0)
      {
         sprintf(acTmp, "%*u", SIZ_EXE_TOTAL, lTmp);
         memcpy(pOutbuf+OFF_EXE_TOTAL, acTmp, SIZ_EXE_TOTAL);
      }
   } else if (*pOutbuf == '9')
   {
      // Reformat MapLink
      iRet = formatMapLink(apTokens[SLO_ROLL_ALTAPN], acTmp, &myCounty);
      memcpy(pOutbuf+OFF_MAPLINK, acTmp, iRet);

      // Create index map link
      if (getIndexPage(acTmp, acTmp1, &myCounty))
         memcpy(pOutbuf+OFF_IMAPLINK, acTmp1, iRet);
   }

   // TRA
   memcpy(pOutbuf+OFF_TRA, apTokens[SLO_ROLL_TRA], strlen(apTokens[SLO_ROLL_TRA]));

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "004251062", 9))
   //   iRet = 0;
#endif
   // Zoning
   if (*apTokens[SLO_ROLL_PRI_ZONE] > ' ')
   {
      memset(pOutbuf+OFF_ZONE, ' ', SIZ_ZONE);
      iTmp = blankRem(apTokens[SLO_ROLL_PRI_ZONE]);
      vmemcpy(pOutbuf+OFF_ZONE, apTokens[SLO_ROLL_PRI_ZONE], SIZ_ZONE, iTmp);
      if (*(pOutbuf+OFF_ZONE_X1) == ' ')
         vmemcpy(pOutbuf+OFF_ZONE_X1, apTokens[SLO_ROLL_PRI_ZONE], SIZ_ZONE_X1, iTmp);
   }

   // Legal
   if (*apTokens[SLO_ROLL_LEGAL] > ' ')
   {
      iRet = updateLegal(pOutbuf, apTokens[SLO_ROLL_LEGAL]);
      if (iRet > iMaxLegal)
         iMaxLegal = iRet;
   }

   // UseCode
   iTmp = sprintf(acTmp, "%s %s", apTokens[SLO_ROLL_PRI_LUC], apTokens[SLO_ROLL_2ND_LUC]);
   memcpy(pOutbuf+OFF_USE_CO, acTmp, iTmp);
   updateStdUse(pOutbuf+OFF_USE_STD, acTmp, iTmp, pOutbuf);

   //if (iNumUseCodes > 0)
   //{
   //   if (acTmp[0] > ' ')
   //   {
   //      pTmp = Cnty2StdUse(acTmp);
   //      if (pTmp)
   //         memcpy(pOutbuf+OFF_USE_STD, pTmp, 3);
   //      else
   //      {
   //         memcpy(pOutbuf+OFF_USE_STD, USE_MISC, 3);
   //         logUsecode(acTmp, pOutbuf);
   //      }
   //   }
   //}

   // Check for Wil Act - 04/15/2010 spn
   if (!memcmp(apTokens[SLO_ROLL_PRI_LUC], "022", 3))
      *(pOutbuf+OFF_AG_PRE) = 'Y';

   // Owner
   Slo_MergeOwner(pOutbuf, apTokens[SLO_ROLL_ASSESSEE], apTokens[SLO_ROLL_CAREOF]);

   // Situs & Mailing addr
   Slo_MergeMAdr(pOutbuf, apTokens[SLO_ROLL_M_ADDR1], apTokens[SLO_ROLL_M_ADDR2], 
      apTokens[SLO_ROLL_M_ADDR3], apTokens[SLO_ROLL_M_CITY], 
      apTokens[SLO_ROLL_M_ST], apTokens[SLO_ROLL_M_ZIP], apTokens[SLO_ROLL_M_ZIP4]);
   Slo_MergeSAdr(pOutbuf);

   // Characteristics - will be merged separately due to multiple entries

   return 0;
}

/***************************** Slo_MergeSaleRec ******************************
 *
 * Merge new sale data into current sale.  Move other sales accordingly.
 * Only update seller if that is the sale from assessor.
 * This is the modified version of MergeSale() which will not update sale
 * without SaleAmt.  Only transfer is updated.
 *
 *****************************************************************************/

int Slo_MergeSaleRec(char *pSalebuf, char *pOutbuf, bool bSaleFlag)
{
   long  lCurSaleDt, lLstSaleDt, lPrice, lLastAmt, iTmp;
   char  acTmp[64], *pTmp;
   SLO_SALE *pSaleRec = (SLO_SALE *) pSalebuf;

   lCurSaleDt = atoin(pSaleRec->DocDate, SIZ_SALE1_DT);
   lLstSaleDt = atoin(pOutbuf+OFF_SALE1_DT, SIZ_SALE1_DT);

   if (lCurSaleDt < lLstSaleDt)
      return -1;

   lPrice = atoin(pSaleRec->SalePrice, SIZ_SALE1_AMT);
   if (lCurSaleDt == lLstSaleDt)
   {
      lLastAmt = atoin(pOutbuf+OFF_SALE1_AMT, SIZ_SALE1_AMT);
      if (!lPrice && lLastAmt > 0)
         return 0;
   } else 
   {
      // Move sale2 to sale3
      memcpy(pOutbuf+OFF_SALE3_DOC, pOutbuf+OFF_SALE2_DOC, SIZ_SALE3_DOC);
      memcpy(pOutbuf+OFF_SALE3_DT, pOutbuf+OFF_SALE2_DT, SIZ_SALE3_DT);
      memcpy(pOutbuf+OFF_SALE3_DOCTYPE, pOutbuf+OFF_SALE2_DOCTYPE, SIZ_SALE3_DOCTYPE);
      memcpy(pOutbuf+OFF_SALE3_AMT, pOutbuf+OFF_SALE2_AMT, SIZ_SALE3_AMT);
      memcpy(pOutbuf+OFF_SALE3_CODE, pOutbuf+OFF_SALE2_CODE, SIZ_SALE3_CODE);
      *(pOutbuf+OFF_AR_CODE3) = *(pOutbuf+OFF_AR_CODE2);

      // Move sale1 to sale2
      memcpy(pOutbuf+OFF_SALE2_DOC, pOutbuf+OFF_SALE1_DOC, SIZ_SALE3_DOC);
      memcpy(pOutbuf+OFF_SALE2_DT, pOutbuf+OFF_SALE1_DT, SIZ_SALE3_DT);
      memcpy(pOutbuf+OFF_SALE2_DOCTYPE, pOutbuf+OFF_SALE1_DOCTYPE, SIZ_SALE3_DOCTYPE);
      memcpy(pOutbuf+OFF_SALE2_AMT, pOutbuf+OFF_SALE1_AMT, SIZ_SALE3_AMT);
      memcpy(pOutbuf+OFF_SALE2_CODE, pOutbuf+OFF_SALE1_CODE, SIZ_SALE3_CODE);
      *(pOutbuf+OFF_AR_CODE2) = *(pOutbuf+OFF_AR_CODE1);
   }

   // Update current sale
   memcpy(pOutbuf+OFF_SALE1_DOC, pSaleRec->DocNum, SIZ_SALE3_DOC);
   memcpy(pOutbuf+OFF_SALE1_DT, pSaleRec->DocDate, SIZ_SALE3_DT);

   // Translate DocType
   iTmp = XrefCode2Idx((XREFTBL *)&asDeed[0], pSaleRec->DocType, iNumDeeds);
   if (iTmp > 0)
   {
      iTmp = sprintf(acTmp, "%d  ", iTmp);
      memcpy(pOutbuf+OFF_SALE1_DOCTYPE, acTmp, iTmp);
   }

   // Remove sale code -
   // Notes: SALE_SIZ_SALECODE is smaller than  SIZ_SALE1_CODE.  Don't swap them.
   switch(pSaleRec->SaleCode[0])
   {
      case 'F':   // Full value
         *(pOutbuf+OFF_SALE1_CODE) = pSaleRec->SaleCode[0];
         break;
      case 'L':   // Less liens
         *(pOutbuf+OFF_SALE1_CODE) = 'N';
         break;
      case 'N':   // NO CONSIDERATION
         *(pOutbuf+OFF_SALE1_CODE) = ' ';
         break;
      case 'M':   // Multi Parcel
         *(pOutbuf+OFF_MULTI_APN) = 'Y';
         if (pSaleRec->SaleCode[1] == 'F' || pSaleRec->SaleCode[1] == 'L')
            *(pOutbuf+OFF_SALE1_CODE) = pSaleRec->SaleCode[1];
         else
            *(pOutbuf+OFF_SALE1_CODE) = ' ';
         break;
      default:
         *(pOutbuf+OFF_SALE1_CODE) = ' ';
   }

   // Check for questionable sale amt
   if (lPrice > 1000)
   {
      memcpy(pOutbuf+OFF_SALE1_AMT, pSaleRec->SalePrice, SIZ_SALE1_AMT);

      // Seller - Remove things in parenthesis
      memcpy(acTmp, pSaleRec->Grantor[0], SIZ_SELLER);
      acTmp[SIZ_SELLER] = 0;
      if (pTmp = strchr(acTmp, '('))
      {
         *pTmp = 0;
         blankPad(acTmp, SIZ_SELLER);
      }
      if (bSaleFlag && acTmp[0] > ' ')
         memcpy(pOutbuf+OFF_SELLER, acTmp, SIZ_SELLER);
      else
         memset(pOutbuf+OFF_SELLER, ' ', SIZ_SELLER);
   } else
   {
      memset(pOutbuf+OFF_SALE1_AMT, ' ', SIZ_SALE1_AMT);
      memset(pOutbuf+OFF_SELLER, ' ', SIZ_SELLER);
   }

   // Update transfers
   lLstSaleDt = atoin(pOutbuf+OFF_TRANSFER_DT, SIZ_TRANSFER_DT);
   if (lCurSaleDt >= lLstSaleDt)
   {
      memcpy(pOutbuf+OFF_TRANSFER_DOC, pSaleRec->DocNum, SIZ_TRANSFER_DOC);
      memcpy(pOutbuf+OFF_TRANSFER_DT, pSaleRec->DocDate, SIZ_TRANSFER_DT);
   }

   if (bSaleFlag)
      *(pOutbuf+OFF_AR_CODE1) = 'A';
   else
      *(pOutbuf+OFF_AR_CODE1) = 'R';

   return 0;
}

/******************************** Slo_MergeSale ******************************
 *
 *
 *****************************************************************************/

int Slo_MergeSale(char *pOutbuf)
{
   static   char  acRec[1024], *pRec=NULL;
   int      iRet, iLoop;
   SLO_SALE *pSale;

   // Get first Char rec for first call
   if (!pRec)
      pRec = fgets(acRec, 1024, fdSale);

   pSale = (SLO_SALE *)&acRec[0];
   do
   {
      if (!pRec)
      {
         fclose(fdSale);
         fdSale = NULL;
         return -1;      // EOF
      }

      // Compare Apn
      iLoop = memcmp(pOutbuf, pSale->Apn, iApnLen);
      if (iLoop > 0)
      {
         if (bDebug)
            LogMsg0("Skip Sale rec  %.*s", iApnLen, pSale->Apn);
         pRec = fgets(acRec, 1024, fdSale);
         lSaleSkip++;
      }
   } while (iLoop > 0);

   // If not match, return
   if (iLoop)
      return 1;

   do
   {
#ifdef _DEBUG
      //if (!memcmp(pOutbuf, "001013007", 9))
      //   iRet = 0;
#endif

      // Skip Quit Claim
      if (memcmp(pSale->DocType, "QC", 2))
         iRet = Slo_MergeSaleRec(acRec, pOutbuf, true);

      // Get next sale record
      pRec = fgets(acRec, 1024, fdSale);
      if (!pRec)
      {
         fclose(fdSale);
         fdSale = NULL;
         break;
      }

   } while (!memcmp(pOutbuf, pSale->Apn, iApnLen));

   lSaleMatch++;

   return 0;
}

/******************************* Slo_MergeMAdr2 ******************************
 *
 * Return 0 if successful, >0 if error
 *
 *****************************************************************************/

void Slo_MergeMAdr2(char *pOutbuf, LPCSTR pAdr1, LPCSTR pAdr2, LPCSTR pAdr3, LPCSTR pCity, LPCSTR pState, LPCSTR pZip)
{
   char     *pTmp, *p3, *p0, *p1, *p2, *pCareOf, acCity[32];
   char     acAddr1[64], acAddr2[64], acAddr3[64], acTmp[64], acMailAdr[64], acCareOf[64];
   int      iTmp;
   ADR_REC  sMailAdr;
   CString  strFld;

   // Clear old Mailing
   removeMailing(pOutbuf, false);
   memset((void *)&sMailAdr, 0, sizeof(ADR_REC));

   pCareOf = acCareOf;
   *pCareOf = 0;

   // Mail address
   if (*pAdr1 <= ' ')
      return;
   strcpy(acAddr1, pAdr1);
   strcpy(acAddr2, pAdr2);
   strcpy(acAddr3, pAdr3);
   if (!memcmp(pCity, "SLO", 3))
      strcpy(acCity, "SAN LUIS OBISPO");
   else
      strcpy(acCity, pCity);
   myTrim(acAddr1);
   myTrim(acAddr2);
   myTrim(acAddr3);
   myTrim(acCity);

   if (*pState == ' ')
   {
      if (!memcmp(acAddr1, "C/O", 3))
      {
         if (*(pOutbuf+OFF_CARE_OF) == ' ')
         {
            //memcpy(pOutbuf+OFF_CARE_OF, (char *)&acAddr1[4], strlen(acAddr1)-4);
            updateCareOf(pOutbuf, acAddr1, strlen(acAddr1));
         }
         sprintf(acMailAdr, "%s %s", acAddr2, acAddr3);
      } else
      {
         sprintf(acMailAdr, "%s %s %s", acAddr1, acAddr2, acAddr3);
      }

      iTmp = strlen(acMailAdr);
      if (iTmp > SIZ_M_STREET) iTmp = SIZ_M_STREET;
      memcpy(pOutbuf+OFF_M_STREET, acMailAdr, iTmp);
      memcpy(pOutbuf+OFF_M_ADDR_D, acMailAdr, iTmp);

      memcpy(pOutbuf+OFF_M_CITY, acCity, strlen(acCity));
      memcpy(pOutbuf+OFF_M_ST, "  ", SIZ_M_ST);
      memcpy(pOutbuf+OFF_M_CTY_ST_D, acCity, strlen(acCity));

      return;
   }


   // Check for Care Of
   if (acAddr1[0] == '%' || !memcmp(acAddr1, "ATTN", 4) || !memcmp(acAddr1, "C/O", 3))
   {
      strcpy(pCareOf, acAddr1);
      acAddr1[0] = 0;
      p1 = acAddr2;
      p2 = acAddr3;
      p3 = NULL;
   } else if (acAddr2[0] && (acAddr2[0] == '%' || !memcmp(acAddr2, "ATTN", 4) || !memcmp(acAddr2, "C/O", 3)))
   {
      strcpy(pCareOf, acAddr2);
      acAddr2[0] = 0;
      p1 = acAddr1;
      p2 = acAddr3;
      p3 = NULL;
   } else if (acAddr3[0] && (acAddr3[0] == '%' || !memcmp(acAddr3, "ATTN", 4) || !memcmp(acAddr3, "C/O", 3)))
   {
      strcpy(pCareOf, acAddr3);
      acAddr3[0] = 0;
      p1 = acAddr1;
      p2 = acAddr2;
      p3 = NULL;
   } else
   {
      p1 = acAddr1;
      p2 = p3 = NULL;
      if (acAddr2[0] > ' ') p2 = acAddr2;
      if (acAddr3[0] > ' ') p3 = acAddr3;
   }

   p0 = NULL;
   strcpy(acMailAdr, p1);
   if (*pCareOf)
   {
      if (p2 && (atoi(p2) > 0 || !memcmp(p2, "PO BOX", 6)))
      {
         strcpy(acMailAdr, p2);
         p0 = p1;
      } else
      {
         strcpy(acMailAdr, p1);
         p0 = p2;
      }
   } else if (p3)
   {
      if (p3 && (atoi(p3) > 0 || !memcmp(p3, "PO BOX", 6)))
      {
         strcpy(acMailAdr, p3);
         p0 = p2;
      } else if (p2 && (atoi(p2) > 0 || !memcmp(p2, "PO BOX", 6)))
      {
         strcpy(acMailAdr, p2);
         p0 = p3;
      }
   } else if (p2)
   {
      if (p1 && (atoi(p1) > 0)  )
      {
         strcpy(acMailAdr, p1);
         p0 = p2;
      } else if (p2 && (atoi(p2) > 0 || !memcmp(p2, "PO BOX", 6)))
      {
         strcpy(acMailAdr, p2);
         p0 = p1;
      }
   }

   if (p0 && *p0)
   {
      if (*p0 == '#' || !memcmp(p0, "SUITE", 5) || !memcmp(p0, "APT", 3) || !memcmp(p0, "STE", 3)
         || !memcmp(p0, "SP ", 3) || !memcmp(p0, "SP#", 3))
      {
         strcat(acMailAdr, " ");
         strcat(acMailAdr, p0);
      }
   }


   // Check for valid string
   iTmp = 0;
   pTmp = (char *)&acMailAdr[0];
   while (*pTmp)
   {
      acTmp[iTmp++] = *pTmp;
      // Remove multi-space
      if (*pTmp == ' ')
         while (*pTmp == ' ') pTmp++;
      else
         pTmp++;
   }
   if (acTmp[iTmp-1] == ' ')
      iTmp -= 1;
   acTmp[iTmp] = 0;

   if (acTmp[0] >= '0' && acTmp[0] <= '9')
   {
      parseAdr1(&sMailAdr, acTmp);
      memcpy(pOutbuf+OFF_M_STRNUM, sMailAdr.strNum, strlen(sMailAdr.strNum));
      memcpy(pOutbuf+OFF_M_DIR, sMailAdr.strDir, strlen(sMailAdr.strDir));
      memcpy(pOutbuf+OFF_M_STREET, sMailAdr.strName, strlen(sMailAdr.strName));
      memcpy(pOutbuf+OFF_M_SUFF, sMailAdr.strSfx, strlen(sMailAdr.strSfx));
      memcpy(pOutbuf+OFF_M_UNITNO, sMailAdr.Unit, strlen(sMailAdr.Unit));
      memcpy(pOutbuf+OFF_M_ADDR_D, acAddr1, strlen(acAddr1));
   } else
   {
      iTmp = strlen(acTmp);
      if (iTmp > SIZ_M_STREET) iTmp = SIZ_M_STREET;
      memcpy(pOutbuf+OFF_M_STREET, acTmp, iTmp);
      memcpy(pOutbuf+OFF_M_ADDR_D, acTmp, iTmp);
   }

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "012342059", 9))
   //   iTmp = 0;
#endif
   
   // Mail city & state
   memcpy(pOutbuf+OFF_M_CITY, acCity, strlen(acCity));
   memcpy(pOutbuf+OFF_M_ST, pState, strlen(pState));
   sprintf(acAddr2, "%s %s", acCity, pState);

   // Mail zip
   iTmp = atoi(pZip);
   if (strlen(pZip) == 9)
   {
      sprintf(sMailAdr.Zip, "%.5s-%.4s", pZip, pZip+5);
      vmemcpy(pOutbuf+OFF_M_ZIP, sMailAdr.Zip, SIZ_M_ZIP);
      vmemcpy(pOutbuf+OFF_M_ZIP4, &sMailAdr.Zip[6], SIZ_M_ZIP4);
   } else if (iTmp > 400)
   {
      sprintf(sMailAdr.Zip, "%.5d", iTmp);
      memcpy(pOutbuf+OFF_M_ZIP, sMailAdr.Zip, SIZ_M_ZIP);
   } 

   if (iTmp > 0)
   {
      strcat(acAddr2, " ");
      strcat(acAddr2, sMailAdr.Zip);
   }
   iTmp = strlen(acAddr2);
   if (iTmp > SIZ_M_CTY_ST_D) iTmp = SIZ_M_CTY_ST_D;
   memcpy(pOutbuf+OFF_M_CTY_ST_D, acAddr2, iTmp);

   // Check for Care Of
   if (*(pOutbuf+OFF_CARE_OF) == ' ' && *pCareOf > ' ')
   {
      /*
      iTmp = strlen(pCareOf);
      if (iTmp > SIZ_CARE_OF) iTmp = SIZ_CARE_OF;
      memcpy(pOutbuf+OFF_CARE_OF, pCareOf, iTmp);
      */
      updateCareOf(pOutbuf, pCareOf, strlen(pCareOf));
   }

}

/******************************** Slo_MergeLien2 *****************************
 *
 * For 2019 LDR file 1920SecRollDataFull.txt
 * Return 0 if successful, < 0 if error
 *        1 retired record, not use
 *
 *****************************************************************************/

int Slo_MergeLien2(char *pOutbuf, char *pRollRec)
{
   char     acTmp[256], acTmp1[64], *pTmp;
   LONGLONG lTmp;
   int      iRet, iTmp;

   // Replace null char with space
   iRet = replNull(pRollRec, ' ', 0);

   // Parse input rec
   iTokens = ParseStringIQ(pRollRec, 9, MAX_FLD_TOKEN, apTokens);
   if (iTokens < SLO_L_FLDS)
   {
      LogMsg("***** Error: bad input record for APN=%s", apTokens[SLO_L_APN]);
      return -1;
   }

   // Clear output buffer
   memset(pOutbuf, ' ', iRecLen);

   // Start copying data
   memcpy(pOutbuf, apTokens[SLO_L_APN], strlen(apTokens[SLO_L_APN]));

   // Copy ALT_APN
   pTmp = myTrim(apTokens[SLO_L_FEE_ASSMT]);
   memcpy(pOutbuf+OFF_ALT_APN, apTokens[SLO_L_FEE_ASSMT], strlen(apTokens[SLO_L_FEE_ASSMT]));

   // Format APN
   iRet = formatApn(apTokens[SLO_L_APN], acTmp, &myCounty);
   memcpy(pOutbuf+OFF_APN_D, acTmp, iRet);

   // Create MapLink and output new record
   iRet = formatMapLink(apTokens[SLO_L_APN], acTmp, &myCounty);
   memcpy(pOutbuf+OFF_MAPLINK, acTmp, iRet);

   // Create index map link
   if (getIndexPage(acTmp, acTmp1, &myCounty))
      memcpy(pOutbuf+OFF_IMAPLINK, acTmp1, iRet);

   // County code
   memcpy(pOutbuf+OFF_CO_NUM, "40SLO", 5);

   // status
   *(pOutbuf+OFF_STATUS) = 'A';

   // TRA
   memcpy(pOutbuf+OFF_TRA, apTokens[SLO_L_TRA], strlen(apTokens[SLO_L_TRA]));

   // Year assessed
   memcpy(pOutbuf+OFF_YR_ASSD, myCounty.acYearAssd, 4);

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "006380014000", 9))
   //   iTmp = 0;
#endif

   // Values
   long alValues[8], iIdx;
   memset((void *)&alValues[0], 0, sizeof(long)*8);
   // Remove 08/30/2019
   //alValues[SLO_LAND] = atol(apTokens[SLO_L_LAND]);
   //alValues[SLO_IMPR] = atol(apTokens[SLO_L_IMPR]);

   iIdx = SLO_L_VALUE_CODE1;
   int iValueCode1 = atol(apTokens[iIdx]);
   getSloValue(apTokens[iIdx], apTokens[iIdx+1], alValues, pOutbuf);
   iIdx += 3;     // Point to code 2
   int iValueCode2 = atol(apTokens[iIdx]);
   getSloValue(apTokens[iIdx], apTokens[iIdx+1], alValues, pOutbuf);
   iIdx += 3;     // Point to code 3
   int iValueCode3 = atol(apTokens[iIdx]);
   getSloValue(apTokens[iIdx], apTokens[iIdx+1], alValues, pOutbuf);
   iIdx += 3;     // Point to code 4
   int iValueCode4 = atol(apTokens[iIdx]);
   getSloValue(apTokens[iIdx], apTokens[iIdx+1], alValues, pOutbuf);
   iIdx += 3;     // Point to code 5
   int iValueCode5 = atol(apTokens[iIdx]);
   getSloValue(apTokens[iIdx], apTokens[iIdx+1], alValues, pOutbuf);
   iIdx += 3;     // Point to code 6
   int iValueCode6 = atol(apTokens[iIdx]);
   getSloValue(apTokens[iIdx], apTokens[iIdx+1], alValues, pOutbuf);
   iIdx += 3;     // Point to code 7
   int iValueCode7 = atol(apTokens[iIdx]);
   getSloValue(apTokens[iIdx], apTokens[iIdx+1], alValues, pOutbuf);

   // Land
   if (alValues[SLO_LAND] > 0)
   {
      sprintf(acTmp, "%*u", SIZ_LAND, alValues[SLO_LAND]);
      memcpy(pOutbuf+OFF_LAND, acTmp, SIZ_LAND);
   }

   // Impr
   if (alValues[SLO_IMPR] > 0)
   {
      sprintf(acTmp, "%*u", SIZ_IMPR, alValues[SLO_IMPR]);
      memcpy(pOutbuf+OFF_IMPR, acTmp, SIZ_IMPR);
   }

   // Other values
   lTmp = alValues[SLO_PP] + alValues[SLO_FE];
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*u", SIZ_OTHER, lTmp);
      memcpy(pOutbuf+OFF_OTHER, acTmp, SIZ_OTHER);

      if (alValues[SLO_FE] > 0)
      {
         iTmp = sprintf(acTmp, "%u", alValues[SLO_FE]);
         memcpy(pOutbuf+OFF_FIXTR, acTmp, iTmp);
      }
      if (alValues[SLO_PP] > 0)
      {
         iTmp = sprintf(acTmp, "%u", alValues[SLO_PP]);
         memcpy(pOutbuf+OFF_PERSPROP, acTmp, iTmp);
      }
   }

   // Gross total
   lTmp += alValues[SLO_LAND]+alValues[SLO_IMPR];
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*u", SIZ_GROSS, lTmp);
      memcpy(pOutbuf+OFF_GROSS, acTmp, SIZ_GROSS);
   }

   // Ratio
   if (alValues[SLO_IMPR] > 0)
   {
      sprintf(acTmp, "%*u", SIZ_RATIO, (LONGLONG)alValues[SLO_IMPR]*100/(alValues[SLO_LAND]+alValues[SLO_IMPR]));
      memcpy(pOutbuf+OFF_RATIO, acTmp, SIZ_RATIO);
   }

   // Prop8 
   if (iValueCode2 == 41 || iValueCode2 == 43 || iValueCode2 == 33 ||
         iValueCode3 == 41 || iValueCode3 == 43 || iValueCode3 == 33 ||
         iValueCode4 == 41 || iValueCode4 == 43 || iValueCode4 == 33 ||
         iValueCode5 == 41 || iValueCode5 == 43 || iValueCode5 == 33 ||
         iValueCode6 == 41 || iValueCode6 == 43 || iValueCode6 == 33 ||
         iValueCode7 == 41 || iValueCode7 == 43 || iValueCode7 == 33 ||
         iValueCode1 == 41 || iValueCode1 == 43 || iValueCode1 == 33 )
      *(pOutbuf+OFF_PROP8_FLG) = 'Y';

   // Exemption
   iIdx = SLO_L_EXEMP_CODE1;
   if (!memcmp(apTokens[SLO_L_EXEMP_CODE1], "01", 2))
      *(pOutbuf+OFF_HO_FL) = '1';      // 'Y'
   else
      *(pOutbuf+OFF_HO_FL) = '2';      // 'N'

   // Save Exempt code.  We only have room for 3
   if (*apTokens[iIdx] > ' ')
      vmemcpy(pOutbuf+OFF_EXE_CD1, apTokens[iIdx], SIZ_EXE_CD1);
   if (*apTokens[iIdx+2] > ' ')
      vmemcpy(pOutbuf+OFF_EXE_CD2, apTokens[iIdx+2], SIZ_EXE_CD1);
   if (*apTokens[iIdx+4] > ' ')
      vmemcpy(pOutbuf+OFF_EXE_CD3, apTokens[iIdx+4], SIZ_EXE_CD1);

   // Create exemption type
   makeExeType(pOutbuf+OFF_EXE_TYPE, pOutbuf+OFF_EXE_CD1, pOutbuf+OFF_EXE_CD2, pOutbuf+OFF_EXE_CD3, (IDX_TBL4 *)&SLO_Exemption);

   // Exempt amt
   iIdx = SLO_L_EXEMP_AMT1;
   lTmp = atol(apTokens[iIdx]);
   lTmp += atol(apTokens[iIdx+2]);
   lTmp += atol(apTokens[iIdx+4]);
   lTmp += atol(apTokens[iIdx+6]);
   lTmp += atol(apTokens[iIdx+8]);
   lTmp += atol(apTokens[iIdx+10]);     
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*u", SIZ_EXE_TOTAL, lTmp);
      memcpy(pOutbuf+OFF_EXE_TOTAL, acTmp, SIZ_EXE_TOTAL);
   }

   // Legal
   iRet = updateLegal(pOutbuf, apTokens[SLO_L_LEGAL_DESC]);
   if (iRet > iMaxLegal)
      iMaxLegal = iRet;

   // AG preserved - Check for Wil Act - 04/15/2010 spn
   //if (!memcmp(apTokens[SLO_L_LAND_USE], "022", 3))
   //   *(pOutbuf+OFF_AG_PRE) = 'Y';

   // Zoning
   //vmemcpy(pOutbuf+OFF_ZONE, apTokens[SLO_L_PRIM_ZONING], SIZ_ZONE);
   //vmemcpy(pOutbuf+OFF_ZONE_X1, apTokens[SLO_L_PRIM_ZONING], SIZ_ZONE_X1);

   // Owner
   Slo_MergeOwner(pOutbuf, apTokens[SLO_L_ASSESSEE], apTokens[SLO_L_CARE_OF]);

   // Mailing addr
   Slo_MergeMAdr2(pOutbuf, apTokens[SLO_L_M_ADDR_1], apTokens[SLO_L_M_ADDR_2], apTokens[SLO_L_M_ADDR_3],
      apTokens[SLO_L_M_CITY], apTokens[SLO_L_M_STATE], apTokens[SLO_L_M_ZIP] );

   // Situs 
   Slo_MergeSAdr2(pOutbuf);

   // UseCode
   //sprintf(acTmp, "%s %s", apTokens[SLO_L_LAND_USE], apTokens[SLO_L_2ND_LAND_USE_1]);
   //iTmp = blankRem(acTmp);
   //memcpy(pOutbuf+OFF_USE_CO, acTmp, iTmp);
   //updateStdUse(pOutbuf+OFF_USE_STD, acTmp, iTmp, pOutbuf);

   return 0;
}

/********************************* Slo_Load_LDR ****************************
 *
 * Load LDR file 1920SecRollDataFull.txt
 * Since this file doesn't include situs city, use community code from updated roll
 * to convert to city name.
 *
 ****************************************************************************/

int Slo_Load_LDR(int iFirstRec)
{
   char     *pRoll, acBuf[MAX_RECSIZE], acOutFile[_MAX_PATH], acRollRec[MAX_RECSIZE];
   long     iRet, lCnt=0;

   HANDLE   fhOut, fhOld;
   DWORD    nBytesWritten;
   BOOL     bRet, bEof;

   LogMsg0("Loading LDR Roll ...");

   // Open Lien file
   LogMsg("Open Lien file %s", acLienFile);
   fdRoll = fopen(acLienFile, "r");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", acLienFile);
      return -1;
   }

   // Open CityZip file
   iRet = initSitus(acIniFile, myCounty.acCntyCode);
   if (iRet < 0)
      return -3;

   // Open Char file
   if (!_access(acCChrFile, 0))
   {
      LogMsg("Open Char file %s", acCChrFile);
      fdChar = fopen(acCChrFile, "r");
      if (fdChar == NULL)
      {
         LogMsg("***** Error opening Char file: %s\n", acCChrFile);
         return -2;
      }
   }
  
   // Open old .O01 file to get UseCode & Legal
   sprintf(acOutFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "O01");
   LogMsg("Open old file %s", acOutFile);
   fhOld = CreateFile(acOutFile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOld == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening O01 file: %s\n", acOutFile);
      return -3;
   }

   // Open Output file
   sprintf(acOutFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "R01");
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening output file: %s\n", acOutFile);
      return -4;
   }

   // Get first RollRec
   pRoll = fgets((char *)&acRollRec[0], MAX_RECSIZE, fdRoll);
   if (*pRoll > '9')
      pRoll = fgets((char *)&acRollRec[0], MAX_RECSIZE, fdRoll);
   bEof = (pRoll ? false:true);

   // create header record
   if (iFirstRec)
   {
      memset(acBuf, '9', iRecLen);
      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
   }

   iNoMatch=iBadCity=iBadSuffix=0;
   acLastApn[0] = 0;

   // Do the rest of the file
   while (!bEof)
   {
      // Create new R01 record
      iRet = Slo_MergeLien2(acBuf, pRoll);
      if (!iRet)
      {
         // Merge Chars
         if (fdChar)
            iRet = Slo_MergeStdChar(acBuf);

         // 2024 file doesn't include UseCode & Zoning.  Get them from current roll
         if (fhOld)
         {
            // Copy UseCode & Zoning from O01 to current roll since new LDR file doesn't include it.
            iRet = PQ_CopyOldR01(acBuf, fhOld, PQ_UPD_USECODE|PQ_UPD_ZONING);
            if (iRet < 0)
            {
               CloseHandle(fhOld);
               fhOld = 0;
            } 
         }

         lLDRRecCount++;
         bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
      }

      // Get next roll record
      pRoll = fgets(acRollRec, MAX_RECSIZE, fdRoll);
      if (!pRoll)
         bEof = true;    // Signal to stop
      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   //if (fdLien)
   //   fclose(fdLien);
   if (fdRoll)
      fclose(fdRoll);
   if (fdChar)
      fclose(fdChar);

   if (fhOld)
      CloseHandle(fhOld);
   if (fhOut)
      CloseHandle(fhOut);
   closeSitus();

   LogMsg("Total output records:       %u", lLDRRecCount);
   LogMsg("Total Char matched:         %u", lCharMatch);
   LogMsg("Total Char skipped:         %u", lCharSkip);
   LogMsg("Total bad-city records:     %u", iBadCity);
   LogMsg("Total bad-suffix records:   %u\n", iBadSuffix);

   lRecCnt = lLDRRecCount;
   return 0;
}

/********************************* Slo_Load_Roll ******************************
 *
 * Rename Slo_MergePQ4 to Slo_Load_Roll
 *
 ******************************************************************************/

int Slo_Load_Roll(int iSkip)
{
   char     *pRoll, acRec[MAX_RECSIZE], acBuf[MAX_RECSIZE];
   char     acRawFile[_MAX_PATH], acOutFile[_MAX_PATH], acRollRec[MAX_RECSIZE];

   HANDLE   fhIn, fhOut;
   FILE     *fdRoll;

   int      iRet, iApnLen, iTmp, iRollUpd=0, iNewRec=0, iRetiredRec=0, iSkipRec=0;
   DWORD    nBytesRead;
   DWORD    nBytesWritten;
   BOOL     bRet, bEof;
   long     lRet=0, lCnt=0;

   LogMsg0("Loading SLO Roll update ...");

   iApnLen = myCounty.iApnLen;
   sprintf(acRawFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "S01");
   sprintf(acOutFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "R01");

   // Rename files for processing
   if (_access(acRawFile, 0))
   {
      if (!_access(acOutFile, 0))
         rename(acOutFile, acRawFile);
      else
      {
         LogMsg("***** Missing input file %s.  Please recheck!", acRawFile);
         return 1;
      }
   }

   // Open Roll file
   LogMsg("Open Roll file %s", acRollFile);
   fdRoll = fopen(acRollFile, "r");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", acRollFile);
      return 2;
   }

   // Open CityZip file
   iRet = initSitus(acIniFile, myCounty.acCntyCode);
   if (iRet < 0)
      return -3;

   // Open Char file
   if (!_access(acCChrFile, 0))
   {
      LogMsg("Open Char file %s", acCChrFile);
      fdChar = fopen(acCChrFile, "r");
      if (fdChar == NULL)
      {
         LogMsg("***** Error opening Char file: %s\n", acCChrFile);
         return 2;
      }
   }

   // Open Input file
   LogMsg("Open input file %s", acRawFile);
   fhIn = CreateFile(acRawFile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);
   if (fhIn == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening input file: %s\n", acRawFile);
      return 2;
   }

   // Open Output file
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening output file: %s\n", acOutFile);
      return 2;
   }

   // Get first RollRec
   pRoll = fgets((char *)&acRollRec[0], MAX_RECSIZE, fdRoll);
   bEof = (pRoll ? false:true);
   iTmp = replUnPrtChar(acRollRec, ' ', iRollLen-2);

   // Copy skip record
   memset(acBuf, ' ', iRecLen);
   while (iSkip-- > 0)
   {
      ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);
      // Ignore record start with 99999999
      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
   }

   iNoMatch=iBadCity=iBadSuffix=0;
   acLastApn[0] = 0;

   // Merge loop
   while (!bEof)
   {
      bRet = ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);

      // Check for EOF
      if (!bRet)
      {
         LogMsg("***** Error reading input file %s (%f)", acRawFile, GetLastError());
         break;
      }

      // EOF ?
      if (iRecLen != nBytesRead)
         break;

      NextRollRec:
#ifdef _DEBUG
      //if (!memcmp(acBuf, "004020015", 9))
      //   iRet = 1;
#endif
      iTmp = memcmp(acBuf, pRoll+iApnOffset, iApnLen);
      if (!iTmp)
      {
         // Merge roll data
         iRet = Slo_MergeRoll(acBuf, pRoll, iRollLen, UPDATE_R01);
         if (!iRet)
         {
            // Merge Chars
            if (fdChar)
               iRet = Slo_MergeStdChar(acBuf);

            // Get last recording date
            iTmp = atoin((char *)&acBuf[OFF_TRANSFER_DT], SIZ_TRANSFER_DT);
            if (iTmp >= lToday)
               LogMsg0("*** WARNING: Bad last recording date %d at record# %d -->%.10s", iTmp, lCnt, acBuf);
            else if (lLastRecDate < iTmp)
               lLastRecDate = iTmp;

            bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
            iRollUpd++;
         } else if (iRet == -1)
         {
            lRet = iRet;
            bEof = true;
            break;
         } else
            iSkipRec++;

         // Read next roll record
         pRoll = fgets(acRollRec, MAX_RECSIZE, fdRoll);
         if (!pRoll)
            bEof = true;    // Signal to stop
         else
         {
            iTmp = replUnPrtChar(acRollRec, ' ', iRollLen-2);
            if (iTmp > 0)
               LogMsg0("*** Invalid char at %d (%.33s)", iTmp, pRoll);
         }
      } else if (iTmp > 0)       // Roll not match, new roll record?
      {
         // Create new R01 record
         iRet = Slo_MergeRoll(acRec, pRoll, iRollLen, CREATE_R01);
         if (!iRet)
         {
            LogMsg0("*** New roll record : %.*s (%d) ***", iApnLen, pRoll+iApnOffset, lCnt);

            // Merge Chars
            if (fdChar)
               iRet = Slo_MergeStdChar(acRec);

            // Get last recording date
            iTmp = atoin((char *)&acRec[OFF_TRANSFER_DT], SIZ_TRANSFER_DT);
            if (iTmp >= lToday)
               LogMsg0("*** WARNING: Bad last recording date %d at record# %d -->%.10s", iTmp, lCnt, acRec);
            else if (lLastRecDate < iTmp)
               lLastRecDate = iTmp;

            bRet = WriteFile(fhOut, acRec, iRecLen, &nBytesWritten, NULL);
            iNewRec++;
            lCnt++;
         } else
            iSkipRec++;

         // Get next roll record
         pRoll = fgets(acRollRec, MAX_RECSIZE, fdRoll);

         if (!pRoll)
            bEof = true;    // Signal to stop
         else
         {
            iTmp = replUnPrtChar(acRollRec, ' ', iRollLen-2);
            if (iTmp > 0)
               LogMsg0("*** Invalid char at %d (%.33s)", iTmp, pRoll);
            goto NextRollRec;
         }
      } else
      {
         // Record may be retired
         if (bDebug)
            LogMsg0("*** Roll not match (retired record?) : R01->%.*s < Roll->%.*s (%d) ***", iApnLen, acBuf, iApnLen, pRoll+iApnOffset, lCnt);
         iRetiredRec++;
         continue;
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);

      if (!bRet)
      {
         LogMsg("Error occurs: %d\n", GetLastError());
         lRet = WRITE_ERR;
         break;
      }
   }

   // Do the rest of the file
   while (!bEof)
   {
      // Create new R01 record
      iRet = Slo_MergeRoll(acRec, pRoll, iRollLen, CREATE_R01);
      if (!iRet)
      {
         LogMsg0("*** New roll record : %.*s (%d) ***", iApnLen, pRoll+iApnOffset, lCnt);

         // Merge Chars
         if (fdChar)
            iRet = Slo_MergeStdChar(acRec);

         // Get last recording date
         iTmp = atoin((char *)&acRec[OFF_TRANSFER_DT], SIZ_TRANSFER_DT);
         if (iTmp >= lToday)
            LogMsg0("*** WARNING: Bad last recording date %d at record# %d -->%.10s", iTmp, lCnt, acRec);
         else if (lLastRecDate < iTmp)
            lLastRecDate = iTmp;

         bRet = WriteFile(fhOut, acRec, iRecLen, &nBytesWritten, NULL);
         iNewRec++;
         lCnt++;
      } else if (iRet == -1)
      {
         lRet = iRet;
         break;
      } else
         iSkipRec++;

      // Get next roll record
      pRoll = fgets(acRollRec, MAX_RECSIZE, fdRoll);

      if (!pRoll)
         bEof = true;    // Signal to stop
      else
      {
         iTmp = replUnPrtChar(acRollRec, ' ', iRollLen-2);
         if (iTmp > 0)
            LogMsg0("*** Invalid char at %d (%.33s)", iTmp, pRoll);
      }
   }

   // Close files
   if (fdRoll)
      fclose(fdRoll);
   if (fhOut)
      CloseHandle(fhOut);
   if (fhIn)
      CloseHandle(fhIn);

   LogMsg("Total output records:       %u", lCnt);
   LogMsg("Total new records:          %u", iNewRec);
   LogMsg("Total skipped(dup) records: %u", iSkipRec);
   LogMsg("Total retired records:      %u", iRetiredRec);
   LogMsg("Total Char matched:         %u", lCharMatch);
   LogMsg("Total Char skipped:         %u", lCharSkip);

   LogMsg("Total bad-city records:     %u", iBadCity);
   LogMsg("Total bad-suffix records:   %u\n", iBadSuffix);

   lRecCnt = lCnt;
   return lRet;
}

/********************************** Slo_MergePQ4 ******************************
 *
 *
 ******************************************************************************

int Slo_MergePublicParcels(int iSkip)
{
   char     acRec[MAX_RECSIZE], acBuf[MAX_RECSIZE], *pBuf;
   char     acRawFile[_MAX_PATH], acOutFile[_MAX_PATH], acTmp[256];

   HANDLE   fhIn, fhOut;

   DWORD    nBytesRead;
   DWORD    nBytesWritten;
   BOOL     bRet, bCont;
   long     lTmp, lCnt=0, lRApn, lS01Apn;
   int      iRet, iNewRec=0;
   CString  strRApn;

   sprintf(acRawFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "S01");
   sprintf(acOutFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "R01");

   // Rename files for processing
   if (_access(acRawFile, 0))
   {
      if (!_access(acOutFile, 0))
         rename(acOutFile, acRawFile);
      else
      {
         LogMsg("Missing input file %s.  Please recheck!", acRawFile);
         return 1;
      }
   }

   // Open public file
   LogMsg("Open Public file %s", acPublicFile);
   try
   {
      strcpy(acTmp, acMdbProvider);
      strcat(acTmp, acPublicFile);

      bRet = hSloRoll.Connect(acTmp);
      if (bRet)
      {
         sprintf(acTmp, "SELECT * FROM %s ORDER BY %s", acPublicTbl, acPublicKey);
         LogMsg("%s", acTmp);
         rsSloRoll.Open(hSloRoll, acTmp);
         bCont = rsSloRoll.next();
         strRApn = rsSloRoll.GetItem(acPublicKey);
         lRApn = atol(strRApn);
      }
   }
   AdoCatch(e)
   {
      LogMsg("***** Error open SecMstr db: %s", ComError(e));
      return -1;
   }

   // Open Char file
   LogMsg("Open Char file %s", acCChrFile);
   fdChar = fopen(acCChrFile, "r");
   if (fdChar == NULL)
   {
      LogMsg("***** Error opening Char file: %s\n", acCChrFile);
      return 2;
   }

   // Open Input file
   LogMsg("Open input file %s", acRawFile);
   fhIn = CreateFile(acRawFile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhIn == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error in opening input file: %s", acRawFile);
      return 3;
   }

   // Open Output file
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error in creating output file: %s", acOutFile);
      return 4;
   }

   // Copy skip record
   memset(acBuf, ' ', iRecLen);
   while (iSkip-- > 0)
   {
      ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);
      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
   }

   lCharMatch=iBadCity=iBadSuffix=0;

   // Merge loop
   while (bCont)
   {
      bRet = ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);

      // Check for EOF
      if (!bRet)
      {
         LogMsg("Error reading input file %s (%f)", acRawFile, GetLastError());
         break;
      }

      if (!nBytesRead)
         break;         // EOF

      lS01Apn = atoin(acBuf, iApnLen);

NextPublicParcel:

      if (lS01Apn > lRApn)
      {
         // Create new R01 record
         iRet = Slo_MergeRoll(acRec, CREATE_R01);
         iNewRec++;

         // Merge Char
         if (fdChar)
            iRet = Slo_MergeChar(acRec);

         // Set public parcel flag
         acRec[OFF_PUBL_FLG] = 'Y';

         lTmp = atoin((char *)&acRec[OFF_TRANSFER_DT], 8);
         if (lTmp > lLastRecDate && lLastRecDate < lToday)
            lLastRecDate = lTmp;

         // Write to output file
         bRet = WriteFile(fhOut, acRec, iRecLen, &nBytesWritten, NULL);
         lCnt++;

         // Read next record
         bCont = rsSloRoll.next();
         if (bCont)
         {
            strRApn = rsSloRoll.GetItem(acPublicKey);
            lRApn = atol(strRApn);
            goto NextPublicParcel;
         }
      }

      // Write to output file
      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);

      if (!bRet)
      {
         LogMsg("***** Error writing to output file: %d\n", GetLastError());
         nBytesRead = 0;
         break;
      }
   }

   // Do the rest of the file
   while (nBytesRead>0 && ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL))
   {
      if (!nBytesRead)
         break;

      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
      lCnt++;
   }

   // Do the rest of the file
   while (bCont)
   {
      iRet = Slo_MergeRoll(acRec, CREATE_R01);
      pBuf = acRec;
      iNewRec++;

      // Merge Char
      if (fdChar)
         iRet = Slo_MergeChar(acRec);

      // Set public flag
      acRec[OFF_PUBL_FLG] = 'Y';

      // Save last rec date
      lTmp = atoin((char *)&acRec[OFF_TRANSFER_DT], 8);
      if (lTmp > lLastRecDate && lLastRecDate < lToday)
         lLastRecDate = lTmp;

      // Write to output file
      bRet = WriteFile(fhOut, acRec, iRecLen, &nBytesWritten, NULL);

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);

      // Read next roll record
      bCont = rsSloRoll.next();
      if (bCont)
         strRApn = rsSloRoll.GetItem(acPublicKey);
   }

   // Close record set
   rsSloRoll.Close();

   if (fhOut)
      CloseHandle(fhOut);
   if (fhIn)
      CloseHandle(fhIn);
   if (fdChar)
      fclose(fdChar);

   LogMsg("Total output records:       %u", lCnt);
   LogMsg("Total new records:          %u", iNewRec);
   LogMsg("Total Char matched:         %u", lCharMatch);
   LogMsg("Total bad-city records:     %u", iBadCity);
   LogMsg("Total bad-suffix records:   %u", iBadSuffix);

   printf("\nTotal output records: %u", lCnt);

   lRecCnt = lCnt;
   return 0;
}

/****************************** Slo_ExtrSale *********************************
 *
 * Return 0 if successful, >0 if error
 *
 *****************************************************************************/

int Slo_ExtrSale(char *pSaleFile)
{
   SLO_SALE  SaleRec;
   char      acOutFile[_MAX_PATH], acSaleOut[_MAX_PATH];
   char      acTmp[256], acBuf[1024], *pTmp;
   FILE      *fdOut, *fdIn;
   long      lCnt=0, lTmp, iRet, lPrice;
   double    dTmp;

   LogMsg("Extracting Sale for %s ...", myCounty.acCntyCode);

   LogMsg("Open Sale input file %s", pSaleFile);
   fdIn = fopen(pSaleFile, "r");
   if (fdIn == NULL)
   {
      LogMsg("***** Error opening sale input file: %s\n", pSaleFile);
      return -2;
   }

   // Open Output file
   sprintf(acSaleOut, acSaleTmpl, myCounty.acCntyCode, "TMP");
   LogMsg("Open output file %s", acSaleOut);
   fdOut = fopen(acSaleOut, "w");
   if (fdOut == NULL)
   {
      LogMsg("***** Error creating GrGr output file: %s\n", acSaleOut);
      return -2;
   }

   // Loop through record set
   while (!feof(fdIn))
   {
      pTmp = fgets(acBuf, 1024, fdIn);
      if (!pTmp)
         break;

      // Reset output record
      memset((void *)&SaleRec, ' ', sizeof(SLO_SALE));

      iRet = ParseStringNQ1(acBuf, ',', MAX_FLD_TOKEN, apTokens);
      if (iRet < SLO_SALE_GTEE_PCT6)
      {
         LogMsg("***** Error: bad input record for APN=%s", apTokens[SLO_SALE_APN]);
         return -1;
      }

      memcpy(SaleRec.Apn, apTokens[SLO_SALE_APN], strlen(apTokens[SLO_SALE_APN]));

      // Drop records without DocNum, DocDate, and Buyer
      if (*apTokens[SLO_SALE_DOCNUM] > ' ' && *apTokens[SLO_SALE_RECDATE] > ' ' &&
          *apTokens[SLO_SALE_GTEE_NAME1] > ' ')
      {
         remChar(apTokens[SLO_SALE_DOCNUM], '-');
         memcpy(SaleRec.DocNum, apTokens[SLO_SALE_DOCNUM],strlen(apTokens[SLO_SALE_DOCNUM]));
         memcpy(SaleRec.DocDate, apTokens[SLO_SALE_RECDATE], 8);
         memcpy(SaleRec.DocType, apTokens[SLO_SALE_DOCTYPE], strlen(apTokens[SLO_SALE_DOCTYPE]));
         memcpy(SaleRec.SaleCode, apTokens[SLO_SALE_CODE], strlen(apTokens[SLO_SALE_CODE]));

         remChar(apTokens[SLO_SALE_SALEPRICE], ',');
         lPrice = atol(apTokens[SLO_SALE_SALEPRICE]);
         if (lPrice > 0)
         {
            sprintf(acTmp, "%*u", SALE_SIZ_SALEPRICE, lPrice);
            memcpy(SaleRec.SalePrice, acTmp, SALE_SIZ_SALEPRICE);
         }

         remChar(apTokens[SLO_SALE_DOCTAX], ',');
         dTmp = atof(apTokens[SLO_SALE_DOCTAX]);
         if (dTmp > 0.0)
         {
            lTmp = (long)(dTmp * 100.0);
            iRet = sprintf(acTmp, "%*u", SALE_SIZ_STAMPAMT, lTmp);
            if (iRet > SALE_SIZ_STAMPAMT)
               LogMsg("*** Bad Tax amt: %s [APN=%s]", acTmp, apTokens[SLO_SALE_APN]);
            else
               memcpy(SaleRec.DocTax, acTmp, SALE_SIZ_STAMPAMT);

            // Check to see if lPrice is the same as Stamp amount
            /* Do not use Stamp Amt to calculate sale price. -SN
            lTmp = (double)lTmp * SALE_FACTOR_100;
            if (lPrice != lTmp)
            {
               LogMsg("*** Sale price mismatches Stamp amount: %d <> %d (%s).  Recalc sale price", lPrice, lTmp, apTokens[SLO_SALE_APN]);  
               sprintf(acTmp, "%*u", SALE_SIZ_SALEPRICE, lTmp);
               memcpy(SaleRec.SalePrice, acTmp, SALE_SIZ_SALEPRICE);
            }
            */
         }

#ifdef _DEBUG
         //if (!memcmp(apTokens[SLO_SALE_APN], "001012029", 9))
         //   lTmp = 0;
#endif
         // Sellers
         if (*apTokens[SLO_SALE_GTOR_NAME1] > ' ')
            memcpy(SaleRec.Grantor[0], apTokens[SLO_SALE_GTOR_NAME1], strlen(apTokens[SLO_SALE_GTOR_NAME1]));
         if (*apTokens[SLO_SALE_GTOR_NAME2] > ' ')
            memcpy(SaleRec.Grantor[1], apTokens[SLO_SALE_GTOR_NAME2], strlen(apTokens[SLO_SALE_GTOR_NAME2]));
         if (*apTokens[SLO_SALE_GTOR_NAME3] > ' ')
            memcpy(SaleRec.Grantor[2], apTokens[SLO_SALE_GTOR_NAME3], strlen(apTokens[SLO_SALE_GTOR_NAME3]));
         if (*apTokens[SLO_SALE_GTOR_NAME4] > ' ')
            memcpy(SaleRec.Grantor[3], apTokens[SLO_SALE_GTOR_NAME4], strlen(apTokens[SLO_SALE_GTOR_NAME4]));
         if (*apTokens[SLO_SALE_GTOR_NAME5] > ' ')
            memcpy(SaleRec.Grantor[4], apTokens[SLO_SALE_GTOR_NAME5], strlen(apTokens[SLO_SALE_GTOR_NAME5]));
         if (*apTokens[SLO_SALE_GTOR_NAME6] > ' ')
            memcpy(SaleRec.Grantor[5], apTokens[SLO_SALE_GTOR_NAME6], strlen(apTokens[SLO_SALE_GTOR_NAME6]));

         // Buyers
         if (*apTokens[SLO_SALE_GTEE_NAME1] > ' ')
            memcpy(SaleRec.Grantee[0], apTokens[SLO_SALE_GTEE_NAME1], strlen(apTokens[SLO_SALE_GTEE_NAME1]));
         if (*apTokens[SLO_SALE_GTEE_NAME2] > ' ')
            memcpy(SaleRec.Grantee[1], apTokens[SLO_SALE_GTEE_NAME2], strlen(apTokens[SLO_SALE_GTEE_NAME2]));
         if (*apTokens[SLO_SALE_GTEE_NAME3] > ' ')
            memcpy(SaleRec.Grantee[2], apTokens[SLO_SALE_GTEE_NAME3], strlen(apTokens[SLO_SALE_GTEE_NAME3]));
         if (*apTokens[SLO_SALE_GTEE_NAME4] > ' ')
            memcpy(SaleRec.Grantee[3], apTokens[SLO_SALE_GTEE_NAME4], strlen(apTokens[SLO_SALE_GTEE_NAME4]));
         if (*apTokens[SLO_SALE_GTEE_NAME5] > ' ')
            memcpy(SaleRec.Grantee[4], apTokens[SLO_SALE_GTEE_NAME5], strlen(apTokens[SLO_SALE_GTEE_NAME5]));
         if (*apTokens[SLO_SALE_GTEE_NAME6] > ' ')
            memcpy(SaleRec.Grantee[5], apTokens[SLO_SALE_GTEE_NAME6], strlen(apTokens[SLO_SALE_GTEE_NAME6]));

         SaleRec.CRLF[0] = '\n';
         SaleRec.CRLF[1] = 0;
         fputs((char *)&SaleRec,fdOut);
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   fclose(fdIn);
   fclose(fdOut);

   // Sort output file
   LogMsg("Total sale records output: %u\n", lCnt);

   // Sort output file and dedup
   //sprintf(acOutFile, acSaleTmpl, myCounty.acCntyCode, "DAT");
   //LogMsg("Sorting %s to %s.", acSaleOut, acOutFile);

   // Update accumulated sale file
   if (lCnt > 0)
   {
      sprintf(acOutFile, acSaleTmpl, myCounty.acCntyCode, "sls");      // cummulative sale file
      if (!_access(acCSalFile, 0))
      {
         if (!_access(acOutFile, 0))
            DeleteFile(acOutFile);
         iRet = MoveFile(acCSalFile, acOutFile);
      }
      LogMsg("Append %s to %s.", acSaleOut, acOutFile);
      //iRet = appendTxtFile(acSaleOut, acOutFile);
      sprintf(acBuf, "%s+%s", acOutFile, acSaleOut);

      // Sort on APN asc, DocDate asc, DocNum asc, Sale price desc
      sprintf(acTmp, "S(1,14,C,A,27,8,C,A,15,12,C,A,67,10,C,D) F(TXT) DUPO(B1024,1,77) ");
      lCnt = sortFile(acBuf, acCSalFile, acTmp);
      if (!lCnt)
      {
         LogMsg("***** Error sorting sale file %s to %s", acOutFile, acCSalFile);
         iRet = -2;
      }
   } else
      iRet = -1;

   LogMsg("#Sale records extracted: %d", lCnt);
   return iRet;
}

int Slo_ExtrSale1(char *pSaleFile)
{
   SCSAL_REC SaleRec;
   char      acOutFile[_MAX_PATH], acSaleOut[_MAX_PATH];
   char      acTmp[256], acBuf[1024], *pTmp;
   FILE      *fdOut, *fdIn;
   long      lCnt=0, lTmp, iRet, lPrice;
   double    dTmp;

   LogMsg0("Extracting county Sale");

   LogMsg("Open Sale input file %s", pSaleFile);
   fdIn = fopen(pSaleFile, "r");
   if (fdIn == NULL)
   {
      LogMsg("***** Error opening sale input file: %s\n", pSaleFile);
      return -2;
   }

   // Open Output file
   sprintf(acSaleOut, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "tmp"); 
   LogMsg("Open output file %s", acSaleOut);
   fdOut = fopen(acSaleOut, "w");
   if (fdOut == NULL)
   {
      LogMsg("***** Error creating GrGr output file: %s\n", acSaleOut);
      return -2;
   }

   lLastRecDate = 0;
   // Loop through record set
   while (!feof(fdIn))
   {
      pTmp = fgets(acBuf, 1024, fdIn);
      if (!pTmp)
         break;

      // Reset output record
      memset((void *)&SaleRec, ' ', sizeof(SCSAL_REC));

      iRet = ParseStringNQ1(acBuf, ',', MAX_FLD_TOKEN, apTokens);
      if (iRet < SLO_SALE_GTEE_PCT6)
      {
         LogMsg("***** Error: bad input record for APN=%s", apTokens[SLO_SALE_APN]);
         return -1;
      }

      memcpy(SaleRec.Apn, apTokens[SLO_SALE_APN], strlen(apTokens[SLO_SALE_APN]));

      // Drop records without DocNum, DocDate
      if (*apTokens[SLO_SALE_DOCNUM] > ' ' && *apTokens[SLO_SALE_RECDATE] > ' ')
      {
#ifdef _DEBUG
         //if (!memcmp(apTokens[SLO_SALE_APN], "001012029", 9))
         //   lTmp = 0;
#endif
         remChar(apTokens[SLO_SALE_SALEPRICE], ',');
         lPrice = atol(apTokens[SLO_SALE_SALEPRICE]);
         if (lPrice > 0)
         {
            sprintf(acTmp, "%*u", SALE_SIZ_SALEPRICE, lPrice);
            memcpy(SaleRec.SalePrice, acTmp, SALE_SIZ_SALEPRICE);
         }

         remChar(apTokens[SLO_SALE_DOCTAX], ',');
         dTmp = atof(apTokens[SLO_SALE_DOCTAX]);
         if (dTmp > 0.0)
         {
            lTmp = (long)(dTmp * 100.0);
            iRet = sprintf(acTmp, "%.2f", dTmp);
            if (iRet > SALE_SIZ_STAMPAMT)
               LogMsg("*** Bad Tax amt: %s [APN=%s]", acTmp, apTokens[SLO_SALE_APN]);
            else
               memcpy(SaleRec.StampAmt, acTmp, iRet);
         }

         // Sellers
         if (*apTokens[SLO_SALE_GTOR_NAME1] > ' ')
            memcpy(SaleRec.Seller1, apTokens[SLO_SALE_GTOR_NAME1], strlen(apTokens[SLO_SALE_GTOR_NAME1]));
         if (*apTokens[SLO_SALE_GTOR_NAME2] > ' ')
            memcpy(SaleRec.Seller2, apTokens[SLO_SALE_GTOR_NAME2], strlen(apTokens[SLO_SALE_GTOR_NAME2]));

         // Buyers
         if (*apTokens[SLO_SALE_GTEE_NAME1] > ' ')
            memcpy(SaleRec.Name1, apTokens[SLO_SALE_GTEE_NAME1], strlen(apTokens[SLO_SALE_GTEE_NAME1]));
         if (*apTokens[SLO_SALE_GTEE_NAME2] > ' ')
            memcpy(SaleRec.Name2, apTokens[SLO_SALE_GTEE_NAME2], strlen(apTokens[SLO_SALE_GTEE_NAME2]));
         if (*apTokens[SLO_SALE_GTEE_NAME3] > ' ')
            SaleRec.Etal = 'Y';

         remChar(apTokens[SLO_SALE_DOCNUM], '-');
         memcpy(SaleRec.DocNum, apTokens[SLO_SALE_DOCNUM],strlen(apTokens[SLO_SALE_DOCNUM]));
         memcpy(SaleRec.DocDate, apTokens[SLO_SALE_RECDATE], 8);
         lTmp = atol(apTokens[SLO_SALE_RECDATE]);
         if (lTmp  > lLastRecDate)
            lLastRecDate = lTmp;

         // Translate DocType
         iRet = XrefCode2Idx((XREFTBL *)&asDeed[0], apTokens[SLO_SALE_DOCTYPE], iNumDeeds);
         if (iRet > 0)
         {
            iRet = sprintf(acTmp, "%d", iRet);
            memcpy(SaleRec.DocType, acTmp, iRet);
         } else if (!memcmp(apTokens[SLO_SALE_DOCTYPE], "AF", 2))
         {
            SaleRec.DocType[0] = '6';
         } else if (!memcmp(apTokens[SLO_SALE_DOCTYPE], "GD", 2))
         {
            SaleRec.DocType[0] = '1';
         } else if (!memcmp(apTokens[SLO_SALE_DOCTYPE], "QC", 2))
         {  
            SaleRec.DocType[0] = '4';
         } else if (lPrice > 0)
            LogMsg("Unknown DocTitle: %s [APN=%s] Price=%s, Tax=%s", apTokens[SLO_SALE_DOCTYPE], 
            apTokens[SLO_SALE_APN], apTokens[SLO_SALE_SALEPRICE], apTokens[SLO_SALE_DOCTAX]);

         pTmp = apTokens[SLO_SALE_CODE];
         switch(*pTmp)
         {
            case 'F':   // Full value
               SaleRec.SaleCode[0] = *pTmp;
               break;
            case 'L':   // Less liens
               SaleRec.SaleCode[0] = 'N';
               break;
            case 'N':   // NO CONSIDERATION
               if (SaleRec.DocType[0] == '4')
                  SaleRec.NoneSale_Flg = 'Y';
               pTmp++;
               if (*pTmp != 'C')
                  SaleRec.SaleCode[0] = *pTmp;
               break;
            case 'M':   // Multi Parcel
               pTmp++;
               SaleRec.MultiSale_Flg = 'Y';
               if (*pTmp == 'F' || *pTmp == 'L')
                  SaleRec.SaleCode[0] = *pTmp;
               break;
         }

         SaleRec.CRLF[0] = '\n';
         SaleRec.CRLF[1] = 0;
         fputs((char *)&SaleRec,fdOut);
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   fclose(fdIn);
   fclose(fdOut);

   // Sort output file
   LogMsg("Total sale records output: %u\n", lCnt);

   // Update accumulated sale file
   if (lCnt > 0)
   {
      if (!_access(acCSalFile, 0))
      {
         // Backup cum sale file
         sprintf(acOutFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "sav"); 
         if (!_access(acOutFile, 0))
            DeleteFile(acOutFile);
         iRet = MoveFile(acCSalFile, acOutFile);
      }
      LogMsg("Append %s to %s.", acSaleOut, acOutFile);
      sprintf(acBuf, "%s+%s", acOutFile, acSaleOut);

      // Sort on APN asc, DocDate asc, DocNum asc, Sale price desc
      sprintf(acTmp, "S(1,14,C,A,27,8,C,A,15,12,C,A,67,10,C,D) F(TXT) DUPO(B1024,1,77) ");
      lCnt = sortFile(acBuf, acCSalFile, acTmp);
      if (!lCnt)
      {
         LogMsg("***** Error sorting sale file %s to %s", acOutFile, acCSalFile);
         iRet = -2;
      }
   } else
      iRet = -1;

   LogMsg("#Sale records extracted: %d", lCnt);
   LogMsg("    Last recording date: %d\n", lLastRecDate);

   if (iRet > 0)
      iRet = 0;
   return iRet;
}

/***************************** Slo_CreateLienRec ****************************
 *
 * Format lien extract record.  
 * Input string may contain double quote in LEGAL.  Use ParseStringNQ1() to parse.
 *
 ****************************************************************************/

int Slo_CreateLienRec(char *pOutbuf, char *pRollRec)
{
   int      iRet, lTmp;
   char     acTmp[64];
   LIENEXTR *pLienExtr = (LIENEXTR *)pOutbuf;

   iRet = ParseStringNQ1(pRollRec, ',', MAX_FLD_TOKEN, apTokens);
   if (iRet < SLO_ROLL_PARKSPACE)
   {
      LogMsg("***** Error: bad input record for APN=%s", apTokens[SLO_ROLL_APN]);
      return -1;
   }

#ifdef _DEBUG
   //if (!memcmp(apTokens[L_ASMT], "003-351-003", 11))
   //   iRet = 0;
#endif

   // Clear output buffer
   memset(pOutbuf, ' ', sizeof(LIENEXTR));

   // Start copying data - avoid duplicate record
   if (!strcmp(apTokens[SLO_ROLL_APN], acLastApn))
      return 1;
   else
      strcpy(acLastApn, apTokens[SLO_ROLL_APN]);

   memcpy(pLienExtr->acApn, apTokens[SLO_ROLL_APN], strlen(apTokens[SLO_ROLL_APN]));

   // TRA
   lTmp = atol(apTokens[SLO_ROLL_TRA]);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%.6d", lTmp);
      memcpy(pLienExtr->acTRA, acTmp, 6);
   }

   // Year assessed
   memcpy(pLienExtr->acYear, myCounty.acYearAssd, 4);

   // Land
   long lLand = atoi(apTokens[SLO_ROLL_LAND]);
   if (lLand > 0)
   {
      sprintf(acTmp, "%*u", SIZ_LIEN_FIXT, lLand);
      memcpy(pLienExtr->acLand, acTmp, SIZ_LIEN_FIXT);
   }

   // Improve
   long lImpr = atoi(apTokens[SLO_ROLL_IMPR]);
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*u", SIZ_LIEN_FIXT, lImpr);
      memcpy(pLienExtr->acImpr, acTmp, SIZ_LIEN_FIXT);
   }

   // Fixtures
   long lFixt   = atoi(apTokens[SLO_ROLL_FIXTURE]);
   // Personal Property
   long lPP   = atoi(apTokens[SLO_ROLL_PERSPROP]);

   lTmp = lFixt+lPP;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*u", SIZ_OTHER, lTmp);
      memcpy(pLienExtr->acOther, acTmp, SIZ_OTHER);

      if (lFixt > 0)
      {
         sprintf(acTmp, "%*u", SIZ_LIEN_FIXT, lFixt);
         memcpy(pLienExtr->acME_Val, acTmp, SIZ_LIEN_FIXT);
      }
      if (lPP > 0)
      {
         sprintf(acTmp, "%*u", SIZ_LIEN_FIXT, lPP);
         memcpy(pLienExtr->acPP_Val, acTmp, SIZ_LIEN_FIXT);
      }
   }

   // Gross total
   lTmp += (lLand+lImpr);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*u", SIZ_GROSS, lTmp);
      memcpy(pLienExtr->acGross, acTmp, SIZ_GROSS);
   }

   // Ratio
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*u", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
      memcpy(pLienExtr->acRatio, acTmp, SIZ_RATIO);
   }

   // Exemption
   lTmp = atol(apTokens[SLO_ROLL_HOEXE]);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*u", SIZ_LIEN_EXEAMT, lTmp);
      memcpy(pLienExtr->acExAmt, acTmp, SIZ_LIEN_EXEAMT);
      pLienExtr->acHO[0] = '1';      // 'Y'
   } else
      pLienExtr->acHO[0] = '2';      // 'N'

   // Prop 8
   //   We can get pretty close using a couple calculations according to Steve Wolfinger. 
   //   Prop 8's are all those properties where:
   //   Taxable Value < Base Year Value AND Use Code is NOT equal to 022
   //     Use Code = 022 = Williamson Act Open Space Contracts whose taxable value is reduced 
   //     by the contract, but for whom the base year value is still tracked in case the contract
   //     is cancelled.
   //   The above calculation will give us all Prop 8's plus a few disaster properties whose 
   //   taxable value is also less than its base year value.  (Presumably, the disaster properties
   //   are tracked this way because the assumption is that whatever structure was destroyed will
   //   eventually be rebuilt and will be given the original base year value even though the 
   //   replacement structure is new.  We wouldn't want to penalize disaster victims.)

   // Testing
   //if (*apTokens[SLO_ROLL_TAXCODE] > ' ' && memcmp(apTokens[SLO_ROLL_TAXCODE], "000", 3))
   //   lTmp = 0;

   pLienExtr->LF[0] = 10;
   pLienExtr->LF[1] = 0;

   return 0;
}

/******************************** Slo_ExtrLien ******************************
 *
 * Extract lien data from ???_lien.csv
 *
 ****************************************************************************/

int Slo_ExtrLien(void)
{
   char     *pTmp, acBuf[1024], acRec[MAX_RECSIZE];
   char     acOutFile[_MAX_PATH], acTmpFile[_MAX_PATH];
   int      iRet, iNewRec=0, lCnt=0;
   FILE     *fdRoll, *fdLienExt;

   LogMsg("\nExtract lien roll for %s", myCounty.acCntyCode);

   // Open lien file
   LogMsg("Open Lien Date Roll file %s", acRollFile);
   sprintf(acTmpFile, "%s\\%s\\%s_lien.srt", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);

   // Sort on SLO_ROLL_APN
   iRet = sortFile(acRollFile, acTmpFile, "S(#2,C,A)");
   fdRoll = fopen(acTmpFile, "r");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening sorted roll file: %s\n", acTmpFile);
      return 2;
   }

   // Create lien extract
   sprintf(acOutFile, acLienTmpl, myCounty.acCntyCode, myCounty.acCntyCode);
   LogMsg("Open lien extract file %s", acOutFile);
   fdLienExt = fopen(acOutFile, "w");
   if (fdLienExt == NULL)
   {
      LogMsg("***** Error creating lien extract file: %s\n", acOutFile);
      return -2;
   }

   // Get 1st rec
   pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdRoll);
   acLastApn[0] = 0;

   // Merge loop
   while (pTmp && !feof(fdRoll))
   {
      // Create new record
      iRet = Slo_CreateLienRec(acBuf, acRec);
    
      if (!iRet)
      {
         // Write to output
         fputs(acBuf, fdLienExt);

         iNewRec++;
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);

      // Get next roll record
      pTmp = fgets(acRec, MAX_RECSIZE, fdRoll);
      if (!isdigit(acRec[1]))
         break;      // EOF
   }

   // Close files
   if (fdRoll)
      fclose(fdRoll);
   if (fdLienExt)
      fclose(fdLienExt);

   LogMsg("Total output records:       %u", iNewRec);
   LogMsg("Total records processed:    %u", lCnt);
   printf("\nTotal output records: %u\n", lCnt);

   return 0;
}

/**************************** Slo_CreateLienRec2 ****************************
 *
 * Parse LDR record for 2019  
 * Input string may contain double quote in LEGAL.  Use ParseStringNQ1() to parse.
 *
 ****************************************************************************/

int Slo_CreateLienRec2(char *pOutbuf, char *pRollRec)
{
   int      iRet;
   LONGLONG lTmp;
   char     acTmp[64];
   LIENEXTR *pLienExtr = (LIENEXTR *)pOutbuf;

   iRet = ParseStringNQ1(pRollRec, 9, MAX_FLD_TOKEN, apTokens);
   if (iRet < SLO_L_FLDS)
   {
      LogMsg("***** Error: bad input record for APN=%s", apTokens[0]);
      return -1;
   }

#ifdef _DEBUG
   //if (!memcmp(apTokens[L_ASMT], "003-351-003", 11))
   //   iRet = 0;
#endif

   // Clear output buffer
   memset(pOutbuf, ' ', sizeof(LIENEXTR));

   // Start copying data 
   memcpy(pLienExtr->acApn, apTokens[SLO_L_APN], strlen(apTokens[SLO_L_APN]));

   // TRA
   lTmp = atol(apTokens[SLO_L_TRA]);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%.6d", lTmp);
      memcpy(pLienExtr->acTRA, acTmp, 6);
   }

   // Year assessed
   memcpy(pLienExtr->acYear, myCounty.acYearAssd, 4);

   // Values
   long alValues[8], iIdx;
   memset((void *)&alValues[0], 0, sizeof(long)*8);
   // Remove 08/30/2019
   //alValues[SLO_LAND] = atol(apTokens[SLO_L_LAND]);
   //alValues[SLO_IMPR] = atol(apTokens[SLO_L_IMPR]);

   iIdx = SLO_L_VALUE_CODE1;
   int iValueCode1 = atol(apTokens[iIdx]);
   getSloValue(apTokens[iIdx], apTokens[iIdx+1], alValues, pOutbuf);
   iIdx += 3;     // Point to code 2
   int iValueCode2 = atol(apTokens[iIdx]);
   getSloValue(apTokens[iIdx], apTokens[iIdx+1], alValues, pOutbuf);
   iIdx += 3;     // Point to code 3
   int iValueCode3 = atol(apTokens[iIdx]);
   getSloValue(apTokens[iIdx], apTokens[iIdx+1], alValues, pOutbuf);
   iIdx += 3;     // Point to code 4
   int iValueCode4 = atol(apTokens[iIdx]);
   getSloValue(apTokens[iIdx], apTokens[iIdx+1], alValues, pOutbuf);
   iIdx += 3;     // Point to code 5
   int iValueCode5 = atol(apTokens[iIdx]);
   getSloValue(apTokens[iIdx], apTokens[iIdx+1], alValues, pOutbuf);
   iIdx += 3;     // Point to code 6
   int iValueCode6 = atol(apTokens[iIdx]);
   getSloValue(apTokens[iIdx], apTokens[iIdx+1], alValues, pOutbuf);
   iIdx += 3;     // Point to code 7
   int iValueCode7 = atol(apTokens[iIdx]);
   getSloValue(apTokens[iIdx], apTokens[iIdx+1], alValues, pOutbuf);

   // Land
   if (alValues[SLO_LAND] > 0)
   {
      sprintf(acTmp, "%*u", SIZ_LAND, alValues[SLO_LAND]);
      memcpy(pLienExtr->acLand, acTmp, SIZ_LAND);
   }

   // Impr
   if (alValues[SLO_IMPR] > 0)
   {
      sprintf(acTmp, "%*u", SIZ_IMPR, alValues[SLO_IMPR]);
      memcpy(pLienExtr->acImpr, acTmp, SIZ_IMPR);
   }

   // Other values
   lTmp = alValues[SLO_PP] + alValues[SLO_FE];
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*u", SIZ_OTHER, lTmp);
      memcpy(pLienExtr->acOther, acTmp, SIZ_OTHER);

      if (alValues[SLO_FE] > 0)
      {
         iRet = sprintf(acTmp, "%u", alValues[SLO_FE]);
         memcpy(pLienExtr->acME_Val, acTmp, iRet);
      }
      if (alValues[SLO_PP] > 0)
      {
         iRet = sprintf(acTmp, "%u", alValues[SLO_PP]);
         memcpy(pLienExtr->acPP_Val, acTmp, iRet);
      }
   }

   // Gross total
   lTmp += alValues[SLO_LAND]+alValues[SLO_IMPR];
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*u", SIZ_GROSS, lTmp);
      memcpy(pLienExtr->acGross, acTmp, SIZ_GROSS);
   }

   // Ratio
   if (alValues[SLO_IMPR] > 0)
   {
      sprintf(acTmp, "%*u", SIZ_RATIO, (LONGLONG)alValues[SLO_IMPR]*100/(alValues[SLO_LAND]+alValues[SLO_IMPR]));
      memcpy(pLienExtr->acRatio, acTmp, SIZ_RATIO);
   }

   // Prop8 
   if (iValueCode2 == 41 || iValueCode2 == 43 || iValueCode2 == 33 ||
         iValueCode3 == 41 || iValueCode3 == 43 || iValueCode3 == 33 ||
         iValueCode4 == 41 || iValueCode4 == 43 || iValueCode4 == 33 ||
         iValueCode5 == 41 || iValueCode5 == 43 || iValueCode5 == 33 ||
         iValueCode6 == 41 || iValueCode6 == 43 || iValueCode6 == 33 ||
         iValueCode7 == 41 || iValueCode7 == 43 || iValueCode7 == 33 ||
         iValueCode1 == 41 || iValueCode1 == 43 || iValueCode1 == 33 )
      pLienExtr->SpclFlag = LX_PROP8_FLG;

   // Exemption
   iIdx = SLO_L_EXEMP_CODE1;
   if (!memcmp(apTokens[SLO_L_EXEMP_CODE1], "01", 2))
      pLienExtr->acHO[0] = '1';      // 'Y'
   else
      pLienExtr->acHO[2] = '2';      // 'N'

   // Save Exempt code
   if (*apTokens[iIdx] > ' ')
   {
      memcpy(pLienExtr->extra.Slo.Exe_Code1, apTokens[iIdx], strlen(apTokens[iIdx]));
      memcpy(pLienExtr->extra.Slo.Exe_Amt1, apTokens[iIdx+1], strlen(apTokens[iIdx+1]));
   }
   iIdx+= 2;
   if (*apTokens[iIdx] > ' ')
   {
      memcpy(pLienExtr->extra.Slo.Exe_Code2, apTokens[iIdx], strlen(apTokens[iIdx]));
      memcpy(pLienExtr->extra.Slo.Exe_Amt2, apTokens[iIdx+1], strlen(apTokens[iIdx+1]));
   }
   iIdx+= 2;
   if (*apTokens[iIdx] > ' ')
   {
      memcpy(pLienExtr->extra.Slo.Exe_Code3, apTokens[iIdx], strlen(apTokens[iIdx]));
      memcpy(pLienExtr->extra.Slo.Exe_Amt3, apTokens[iIdx+1], strlen(apTokens[iIdx+1]));
   }
   iIdx+= 2;
   if (*apTokens[iIdx] > ' ')
   {
      memcpy(pLienExtr->extra.Slo.Exe_Code4, apTokens[iIdx], strlen(apTokens[iIdx]));
      memcpy(pLienExtr->extra.Slo.Exe_Amt4, apTokens[iIdx+1], strlen(apTokens[iIdx+1]));
   }
   iIdx+= 2;
   if (*apTokens[iIdx] > ' ')
   {
      memcpy(pLienExtr->extra.Slo.Exe_Code5, apTokens[iIdx], strlen(apTokens[iIdx]));
      memcpy(pLienExtr->extra.Slo.Exe_Amt5, apTokens[iIdx+1], strlen(apTokens[iIdx+1]));
   }
   iIdx+= 2;
   if (*apTokens[iIdx] > ' ')
   {
      memcpy(pLienExtr->extra.Slo.Exe_Code6, apTokens[iIdx], strlen(apTokens[iIdx]));
      memcpy(pLienExtr->extra.Slo.Exe_Amt6, apTokens[iIdx+1], strlen(apTokens[iIdx+1]));
   }

   // Exempt amt
   iIdx = SLO_L_EXEMP_AMT1;
   lTmp = atol(apTokens[iIdx]);
   lTmp += atol(apTokens[iIdx+2]);
   lTmp += atol(apTokens[iIdx+4]);
   lTmp += atol(apTokens[iIdx+6]);
   lTmp += atol(apTokens[iIdx+8]);
   lTmp += atol(apTokens[iIdx+10]);     
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*u", SIZ_EXE_TOTAL, lTmp);
      memcpy(pLienExtr->acExAmt, acTmp, SIZ_EXE_TOTAL);
   }

   pLienExtr->LF[0] = 10;
   pLienExtr->LF[1] = 0;

   return 0;
}

/******************************* Slo_ExtrLien2 ******************************
 *
 * Extract lien data from 2021SecRollDataFull.txt
 *
 ****************************************************************************/

int Slo_ExtrLien2(void)
{
   char     *pTmp, acBuf[1024], acRec[MAX_RECSIZE];
   char     acOutFile[_MAX_PATH], acTmpFile[_MAX_PATH];
   int      iRet, iNewRec=0, lCnt=0;
   FILE     *fdRoll, *fdLienExt;

   LogMsg0("Extract lien value for %s", myCounty.acCntyCode);

   // Sort on SLO_ROLL_APN
   sprintf(acTmpFile, "%s\\%s\\%s_lien.srt", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   iRet = sortFile(acRollFile, acTmpFile, "S(#2,C,A)");
   LogMsg("Open Lien file %s", acTmpFile);
   fdRoll = fopen(acTmpFile, "r");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening sorted roll file: %s\n", acTmpFile);
      return 2;
   }

   // Create lien extract
   sprintf(acOutFile, acLienTmpl, myCounty.acCntyCode, myCounty.acCntyCode);
   LogMsg("Open lien extract file %s", acOutFile);
   fdLienExt = fopen(acOutFile, "w");
   if (fdLienExt == NULL)
   {
      LogMsg("***** Error creating lien extract file: %s\n", acOutFile);
      return -2;
   }

   // Get 1st rec
   pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdRoll);
   if (*pTmp > '9')
      pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdRoll);

   // Merge loop
   while (pTmp && !feof(fdRoll))
   {
      // Create new record
      iRet = Slo_CreateLienRec2(acBuf, acRec);
    
      if (!iRet)
      {
         // Write to output
         fputs(acBuf, fdLienExt);

         iNewRec++;
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);

      // Get next roll record
      pTmp = fgets(acRec, MAX_RECSIZE, fdRoll);
      if (!isdigit(acRec[1]))
         break;      // EOF
   }

   // Close files
   if (fdRoll)
      fclose(fdRoll);
   if (fdLienExt)
      fclose(fdLienExt);

   LogMsg("Total output records:       %u", iNewRec);
   LogMsg("Total records processed:    %u", lCnt);
   printf("\nTotal output records: %u\n", lCnt);

   return 0;
}

/******************************** Slo_ExtrProp8 *****************************
 *
 * The way to identify if a property has Proposition 8 assessed value is if the 
 * property has a 41 and/or 43 code in the Value_Code3/Value_Code4 field.
 * These are internal tracking codes for the Factored Base Year Value. 
 * The Factored Base Year Value amount follows in the Value_AMT3 field and/or the Value_AMT4 field.
 *
 * The Proposition 8 assessed value amounts appear in the Value_AMT1 (Land) and 
 * the Value_AMT2 (Improvements) fields. They are the "taxable" values of which the 
 * property taxes are based on.
 *
 ****************************************************************************/

int Slo_ExtrProp8(void)
{
   char     acOutFile[_MAX_PATH], acTmp[256];

   int      iValueCode3, iValueCode4;
   long     lCnt=0, lRApn;
   bool     bRet;
   CString  strRApn, strFld;

   FILE     *fdProp8;

   LogMsg("Extract Prop8 for %s %s", myCounty.acCntyCode, myCounty.acYearAssd);
   sprintf(acOutFile, acProp8Tmpl, myCounty.acCntyCode, myCounty.acCntyCode, myCounty.acYearAssd, "P8");

   // Open roll file
   LogMsg("Open Roll file %s", acSecuredMdb);
   try
   {
      strcpy(acTmp, acMdbProvider);
      strcat(acTmp, acSecuredMdb);

      bRet = hSloRoll.Connect(acTmp);
      if (bRet)
      {
         sprintf(acTmp, "SELECT * FROM %s ORDER BY %s", acRollTbl, acRollKey);
         LogMsg("%s", acTmp);
         rsSloRoll.Open(hSloRoll, acTmp);
      }
   } catch(_com_error &e)
   {
      LogMsg("***** Error open SecMstr db: %s ", e.ErrorMessage());
      return -1;
   }

   // Open Char file
   LogMsg("Open Prop8 file %s", acOutFile);
   fdProp8 = fopen(acOutFile, "w");
   if (fdProp8 == NULL)
   {
      LogMsg("***** Error opening Prop8 file: %s\n", acOutFile);
      return 2;
   }

   // Output header
   fputs("APN\n", fdProp8);

   // Loop through and bill every one of them
   while (rsSloRoll.next())
   {
      lLDRRecCount++;

      strRApn = rsSloRoll.GetItem(acRollKey);
      lRApn = atol(strRApn);

      if (lRApn == 99999)
         continue;

      // Exempt Code
      strFld = rsSloRoll.GetItem("VALUE_CODE3");
      iValueCode3 = atol(strFld);
      strFld = rsSloRoll.GetItem("VALUE_CODE4");
      iValueCode4 = atol(strFld);

      if (iValueCode3 == 41 || iValueCode3 == 43 || iValueCode4 == 41 || iValueCode4 == 43)
      {
         sprintf(acTmp, "%s\n", strRApn);
         fputs(acTmp, fdProp8);
         lCnt++;
      }

      if (!(lLDRRecCount % 1000))
         printf("\r%u", lLDRRecCount);
   }

   // Close record set
   rsSloRoll.Close();
   
   // Close files
   if (fdProp8)
      fclose(fdProp8);

   printf("\n");
   LogMsgD("Total input records:        %u", lLDRRecCount);
   LogMsgD("Total Prop8 records output: %u", lCnt);
   
   return 0;
}

/********************************** Slo_MergeLien *****************************
 *
 * Return 0 if successful, >0 if error
 *
 *****************************************************************************/

int Slo_MergeLien(char *pOutbuf, int iFlag)
{
   char     acTmp[1024], acTmp1[256];
   long     lTmp; 
   int      iRet, iTmp, iYrAssd;
   CString  strFld, strTmp;

   iYrAssd = atoi(myCounty.acYearAssd);
   if (iFlag & CREATE_R01)
   {
      // Clear output buffer
      memset(pOutbuf, ' ', iRecLen);

      // Start copying data
      strFld = rsSloRoll.GetItem(acRollKey);
      iTmp = strFld.GetLength();
      if (iTmp < iApnLen)
      {
         iTmp = sprintf(acTmp, "%.*s%s", iApnLen-iTmp, "000", strFld);
         strFld = acTmp;
      }  
      memcpy(pOutbuf, strFld, iTmp);

      // Format APN
      iRet = formatApn(strFld.GetBuffer(0), acTmp, &myCounty);
      memcpy(pOutbuf+OFF_APN_D, acTmp, iRet);

      // Create MapLink
      iRet = formatMapLink(strFld.GetBuffer(0), acTmp, &myCounty);
      memcpy(pOutbuf+OFF_MAPLINK, acTmp, iRet);

      // Create index map link
      if (getIndexPage(acTmp, acTmp1, &myCounty))
         memcpy(pOutbuf+OFF_IMAPLINK, acTmp1, iRet);

      // County code and Parcel Status
      memcpy(pOutbuf+OFF_CO_NUM, "40SLOA", 6);

      // Year assessed
      memcpy(pOutbuf+OFF_YR_ASSD, myCounty.acYearAssd, 4);

      // HO Exempt
      strFld = rsSloRoll.GetItem("EXEMP_CODE1");
      if (strFld == "01")
      {
         *(pOutbuf+OFF_HO_FL) = '1';      // 'Y'
      } else
      {
         *(pOutbuf+OFF_HO_FL) = '2';      // 'N'
      } 

      // Save Exempt code
      memcpy(pOutbuf+OFF_EXE_CD1, strFld, strFld.GetLength());
      strFld = rsSloRoll.GetItem("EXEMP_CODE2");
      memcpy(pOutbuf+OFF_EXE_CD2, strFld, strFld.GetLength());
      strFld = rsSloRoll.GetItem("EXEMP_CODE3");
      if (strFld == "")
         strFld = rsSloRoll.GetItem("EXEMP_CODE4");
      memcpy(pOutbuf+OFF_EXE_CD3, strFld, strFld.GetLength());

      // Exempt amt
      lTmp = 0;
      strFld = rsSloRoll.GetItem("EXEMP_AMT1");
      lTmp = atol(strFld);
      strFld = rsSloRoll.GetItem("EXEMP_AMT2");
      lTmp += atol(strFld);
      strFld = rsSloRoll.GetItem("EXEMP_AMT3");
      lTmp += atol(strFld);
      strFld = rsSloRoll.GetItem("EXEMP_AMT4");
      lTmp += atol(strFld);
      strFld = rsSloRoll.GetItem("EXEMP_AMT5");
      lTmp += atol(strFld);
      strFld = rsSloRoll.GetItem("EXEMP_AMT6");
      lTmp += atol(strFld);     
      if (lTmp > 0)
      {
         sprintf(acTmp, "%*u", SIZ_EXE_TOTAL, lTmp);
         memcpy(pOutbuf+OFF_EXE_TOTAL, acTmp, SIZ_EXE_TOTAL);
      }

      // Based on value code to determine values
      long alValues[8];
      memset((void *)&alValues[0], 0, sizeof(long)*8);
      strFld = rsSloRoll.GetItem("VALUE_CODE1");
      int iValueCode1 = atol(strFld);
      getSloValue(strFld, rsSloRoll.GetItem("VALUE_AMT1"), alValues, pOutbuf);
      strFld = rsSloRoll.GetItem("VALUE_CODE2");
      int iValueCode2 = atol(strFld);
      getSloValue(strFld, rsSloRoll.GetItem("VALUE_AMT2"), alValues, pOutbuf);
      strFld = rsSloRoll.GetItem("VALUE_CODE3");
      int iValueCode3 = atol(strFld);
      getSloValue(strFld, rsSloRoll.GetItem("VALUE_AMT3"), alValues, pOutbuf);
      strFld = rsSloRoll.GetItem("VALUE_CODE4");
      int iValueCode4 = atol(strFld);
      getSloValue(strFld, rsSloRoll.GetItem("VALUE_AMT4"), alValues, pOutbuf);
      strFld = rsSloRoll.GetItem("VALUE_CODE5");
      int iValueCode5 = atol(strFld);
      getSloValue(strFld, rsSloRoll.GetItem("VALUE_AMT5"), alValues, pOutbuf);
      strFld = rsSloRoll.GetItem("VALUE_CODE6");
      int iValueCode6 = atol(strFld);
      getSloValue(strFld, rsSloRoll.GetItem("VALUE_AMT6"), alValues, pOutbuf);
      strFld = rsSloRoll.GetItem("VALUE_CODE7");
      int iValueCode7 = atol(strFld);
      getSloValue(strFld, rsSloRoll.GetItem("VALUE_AMT7"), alValues, pOutbuf);

      if (alValues[SLO_LAND] > 0)
      {
         sprintf(acTmp, "%*u", SIZ_LAND, alValues[SLO_LAND]);
         memcpy(pOutbuf+OFF_LAND, acTmp, SIZ_LAND);
      }
      if (alValues[SLO_IMPR] > 0)
      {
         sprintf(acTmp, "%*u", SIZ_IMPR, alValues[SLO_IMPR]);
         memcpy(pOutbuf+OFF_IMPR, acTmp, SIZ_IMPR);
      }
      lTmp = alValues[SLO_PP] + alValues[SLO_FE];
      if (lTmp > 0)
      {
         sprintf(acTmp, "%*u", SIZ_OTHER, lTmp);
         memcpy(pOutbuf+OFF_OTHER, acTmp, SIZ_OTHER);
      }

      // County considers this value as IMPR
      //if (alValues[SLO_MH] > 0)
      //{
      //   sprintf(acTmp, "%*u", SIZ_IMPR, alValues[SLO_MH]);
      //   memcpy(pOutbuf+OFF_PP_MH, acTmp, SIZ_IMPR);
      //}

      // Gross total
      lTmp += alValues[SLO_LAND]+alValues[SLO_IMPR];
      if (lTmp > 0)
      {
         sprintf(acTmp, "%*u", SIZ_GROSS, lTmp);
         memcpy(pOutbuf+OFF_GROSS, acTmp, SIZ_GROSS);
      }

      // Ratio
      if (alValues[SLO_IMPR] > 0)
      {
         sprintf(acTmp, "%*u", SIZ_RATIO, alValues[SLO_IMPR]*100/(alValues[SLO_LAND]+alValues[SLO_IMPR]));
         memcpy(pOutbuf+OFF_RATIO, acTmp, SIZ_RATIO);
      }

      // Prop8 
      if (iValueCode2 == 41 || iValueCode2 == 43 || iValueCode2 == 33 ||
          iValueCode3 == 41 || iValueCode3 == 43 || iValueCode3 == 33 ||
          iValueCode4 == 41 || iValueCode4 == 43 || iValueCode4 == 33 ||
          iValueCode5 == 41 || iValueCode5 == 43 || iValueCode5 == 33 ||
          iValueCode6 == 41 || iValueCode6 == 43 || iValueCode6 == 33 ||
          iValueCode7 == 41 || iValueCode7 == 43 || iValueCode7 == 33 ||
          iValueCode1 == 41 || iValueCode1 == 43 || iValueCode1 == 33 )
         *(pOutbuf+OFF_PROP8_FLG) = 'Y';
   }

   // TRA
   strFld = rsSloRoll.GetItem("TRA");
   memcpy(pOutbuf+OFF_TRA, strFld, strFld.GetLength());

   // AG preserved - Check for Wil Act - 04/15/2010 spn
   strFld = rsSloRoll.GetItem("PRIMARY_LAND_USE");
   if (strFld == "022")
      *(pOutbuf+OFF_AG_PRE) = 'Y';

   // Zoning
   strFld = rsSloRoll.GetItem("PRIM_ZONING");
   strFld.TrimRight();
   // 07/12/2016 - Ignore secondary to make it compatible with monthly update data
   //strTmp = rsSloRoll.GetItem("SECND_ZONING");
   //if (strTmp.Left(1) != "." && strTmp.Left(1) != " ")
   //{
   //   strTmp.TrimRight();
   //   if (strFld.IsEmpty())
   //      strFld = strTmp;
   //   else
   //   {
   //      iTmp = strFld.FindOneOf(" ");
   //      if (iTmp >= 0)
   //         strFld += strTmp;
   //      else
   //      {
   //         strFld += " ";
   //         strFld += strTmp;
   //      }
   //   }
   //}
   memcpy(pOutbuf+OFF_ZONE, strFld, strFld.GetLength());

   // Legal 
   strFld = rsSloRoll.GetItem("LEGAL_DESC");
   strcpy(acTmp, strFld);
   iRet = updateLegal(pOutbuf, acTmp);
   if (iRet > iMaxLegal)
      iMaxLegal = iRet;

   // Owner
   strFld = rsSloRoll.GetItem("ASSESSEE");
   strTmp = rsSloRoll.GetItem("IN_CARE_OF");
   Slo_MergeOwner(pOutbuf, strFld, strTmp);

   // Situs & Mailing addr
   Slo_MergeMAdr2(pOutbuf, rsSloRoll.GetItem("ADDRESS_1"), rsSloRoll.GetItem("ADDRESS_2"), rsSloRoll.GetItem("ADDRESS_3"),
      rsSloRoll.GetItem("CITY"), rsSloRoll.GetItem("STATE"), rsSloRoll.GetItem("ZIP") );
   Slo_MergeSAdrX(pOutbuf);

   // UseCode
   strFld = rsSloRoll.GetItem("PRIMARY_LAND_USE");
   strFld.TrimRight();
   strTmp = rsSloRoll.GetItem("SECND_LAND_USE_1");
   if (!strTmp.IsEmpty())
      strTmp.TrimRight();
   iTmp = sprintf(acTmp, "%s %s", strFld, strTmp);
   memcpy(pOutbuf+OFF_USE_CO, acTmp, iTmp);
   updateStdUse(pOutbuf+OFF_USE_STD, acTmp, iTmp, pOutbuf);

   return 0;
}

/********************************* Slo_CreatePQ4 ****************************
 *
 * This function is used to support MDB format
 *
 ****************************************************************************/

int Slo_CreatePQ4(int iFirstRec /* 1=create header rec */)
{
   char     acBuf[MAX_RECSIZE];
   char     acOutFile[_MAX_PATH], acTmp[256];

   HANDLE   fhOut;

   int      iRet, iTmp;
   long     lCnt=0, lRApn;
   DWORD    nBytesWritten;
   BOOL     bRet;
   CString  strRApn, strSApn;

   sprintf(acOutFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "R01");

   // Open roll file
   LogMsg("Open Roll file %s", acSecuredMdb);
   try
   {
      strcpy(acTmp, acMdbProvider);
      strcat(acTmp, acSecuredMdb);

      bRet = hSloRoll.Connect(acTmp);
      if (bRet)
      {
         sprintf(acTmp, "SELECT * FROM %s ORDER BY %s", acRollTbl, acRollKey);
         LogMsg("%s", acTmp);
         rsSloRoll.Open(hSloRoll, acTmp);
      }
   }
   AdoCatch(e)
   {
      LogMsg("***** Error open SecMstr db: %s", ComError(e));
      return -1;
   }

   // Open Char file
   LogMsg("Open Char file %s", acCChrFile);
   fdChar = fopen(acCChrFile, "r");
   if (fdChar == NULL)
   {
      LogMsg("***** Error opening Char file: %s\n", acCChrFile);
      return -2;
   }

   // Open Sales file
   if (!_access(acCSalFile, 0))
   {
      LogMsg("Open Sales file %s", acCSalFile);
      fdSale = fopen(acCSalFile, "r");
      if (fdSale == NULL)
      {
         LogMsg("***** Error opening Sales file: %s\n", acCSalFile);
         return -2;
      }
   }
   
   // Open Output file
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error creating output file: %s\n", acOutFile);
      return -4;
   }

   // Output first header record
   if (iFirstRec > 0)
   {
      memset(acBuf, '9', iRecLen);
      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
   }

   // Init variables
   iNoMatch=iBadCity=iBadSuffix=0;

   // Loop through and bill every one of them
   while (rsSloRoll.next())
   {
      lLDRRecCount++;

      strRApn = rsSloRoll.GetItem(acRollKey);
      lRApn = atol(strRApn);

      if (lRApn == 99999)
         continue;

      // Create new R01 record
      iTmp = Slo_MergeLien(acBuf, CREATE_R01);

      // Merge Char
      if (fdChar)
         iRet = Slo_MergeChar(acBuf);

      // Merge Sales
      if (fdSale)
         iRet = Slo_MergeSale(acBuf);

      iTmp = atoin((char *)&acBuf[OFF_TRANSFER_DT], 8);
      if (iTmp > lLastRecDate && iTmp < lToday)
         lLastRecDate = iTmp;

      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);

      if (!bRet)
      {
         LogMsg("***** Error writing to output file at record %d\n", lCnt);
         iRet = WRITE_ERR;
         break;
      }
   }

   // Close record set
   rsSloRoll.Close();
   
   // Close files
   if (fhOut)
      CloseHandle(fhOut);
   if (fdChar)
      fclose(fdChar);
   if (fdSale)
      fclose(fdSale);

   LogMsg("Total input records:        %u", lLDRRecCount);
   LogMsg("Total output records:       %u", lCnt);
   LogMsg("Total Char matched:         %u", lCharMatch);
   LogMsg("Total bad-city records:     %u", iBadCity);
   LogMsg("Total bad-suffix records:   %u", iBadSuffix);
   LogMsg("Last recording date:        %u", lLastRecDate);
   printf("\nTotal output records: %u\n", lCnt);
   
   lRecCnt = lCnt;

   if (iRet != WRITE_ERR)
      iRet = 0;
   return iRet;
}

/****************************** Slo_UpdateTaxBase *****************************
 *
 * Update Tax Base using SLO.TC
 *
 ******************************************************************************/

int Slo_UpdateTaxBase(char *pBaseBuf, FILE *fd)
{
   static   char acRec[MAX_RECSIZE], *pRec=NULL, acBillNum[64];
   static   bool bDontParse=false;
   int      iRet, iLoop;
   char     acTmp[256], acUpdDate[16], *pTmp;
   double   dTmp;

   TAXBASE   *pBaseRec = (TAXBASE *)pBaseBuf;

   if (!pRec)
      pRec = fgets(acRec, MAX_RECSIZE, fd);        // Get first rec

#ifdef _DEBUG
   //if (!memcmp(pBaseRec->Apn, "045357007", 9))
   //   iRet = 0;
#endif

   do
   {
      // Compare Apn
      iLoop = memcmp(pBaseRec->Apn, acRec, iApnLen);
      if (iLoop > 0)
      {
         lTaxSkip++;
         if (strstr(acRec, "SEC"))
         {
            // Insert new tax bill
            LogMsg("*** Skip TC record: %.128s", acRec);
         }

         pRec = fgets(acRec, MAX_RECSIZE, fd);
         if (!pRec)
         {
            fclose(fd);
            fd = NULL;
            return 1;      // EOF
         }
      }
   } while (iLoop > 0);

   if (iLoop)
      return 1;

   // Parse input string
   if (!bDontParse)
   {
      iRet = ParseStringIQ(acRec, '|', 128, apTokens);
      if (iRet < TC_NET_VAL)
      {
         LogMsg("***** Error: bad TC record for APN=%.100s (#tokens=%d)", acRec, iRet);
         return -1;
      }
   }

   // Check if new data then update
   pTmp = dateConversion(apTokens[TC_UPD_DATE], acUpdDate, MM_DD_YYYY_1);
   if (memcmp(acUpdDate, pBaseRec->Upd_Date, 8) < 0)
   {
      lTaxSkip++;
      goto GetNextRec;
   }

   // Matching up billNum before update
   if (strcmp(pBaseRec->BillNum, apTokens[TC_BILL_NUM]))
   {
      bDontParse=true;
      LogMsg("*** Skip update APN=%s, Base.BillNum=%s, TC.BillNum=%s", pBaseRec->Apn, pBaseRec->BillNum, apTokens[TC_BILL_NUM]);
      return 1;
   }

   // Update Tax year
   strcpy(pBaseRec->TaxYear, apTokens[TC_TAX_YR]);

   // Update paid info - PAID_STAT can be PAID or DUE
   if (*apTokens[TC_PAID_STAT1] == 'P')
   {
      pBaseRec->Inst1Status[0] = TAX_BSTAT_PAID;
      strcpy(pBaseRec->PaidAmt1, apTokens[TC_AMT_PAID1]);
      if (*apTokens[TC_DUE_PAID_DT1] >= '0' && dateConversion(apTokens[TC_DUE_PAID_DT1], acTmp, MM_DD_YYYY_1))
         strcpy(pBaseRec->PaidDate1, acTmp);
      else
         LogMsg("*** Bad PaidDate 1: %s [%s]", apTokens[TC_DUE_PAID_DT1], apTokens[TC_APN_S]);
   } else if (*apTokens[TC_PAID_STAT1] == 'D' || *apTokens[TC_PAID_STAT1] == 'U')
   {
      if (*apTokens[TC_DUE_PAID_DT1] >= '0' && dateConversion(apTokens[TC_DUE_PAID_DT1], acTmp, MM_DD_YYYY_1))
         strcpy(pBaseRec->DueDate1, acTmp);

      // TOTAL_DUE can be "Pending" or actual due value.
      // Pending means county is waiting for the check to be clear.
      if (*apTokens[TC_TOTAL_DUE] == 'P')
         pBaseRec->Inst1Status[0] = TAX_BSTAT_PENDING;
      else
         pBaseRec->Inst1Status[0] = TAX_BSTAT_UNPAID;
   } 

   // Is it paid?
   if (*apTokens[TC_PAID_STAT2] == 'P')
   {
      pBaseRec->Inst2Status[0] = TAX_BSTAT_PAID;
      strcpy(pBaseRec->PaidAmt2, apTokens[TC_AMT_PAID2]);
      if (*apTokens[TC_DUE_PAID_DT2] >= '0' && dateConversion(apTokens[TC_DUE_PAID_DT2], acTmp, MM_DD_YYYY_1))
         strcpy(pBaseRec->PaidDate2, acTmp);
      else
         LogMsg("*** Bad PaidDate 2: %s [%s]", apTokens[TC_DUE_PAID_DT2], apTokens[TC_APN_S]);
   } else if (*apTokens[TC_PAID_STAT2] == 'D' || *apTokens[TC_PAID_STAT2] == 'U')
   {
      if (*apTokens[TC_DUE_PAID_DT2] >= '0' && dateConversion(apTokens[TC_DUE_PAID_DT2], acTmp, MM_DD_YYYY_1))
         strcpy(pBaseRec->DueDate2, acTmp);
      if (*apTokens[TC_TOTAL_DUE] == 'P')
         pBaseRec->Inst2Status[0] = TAX_BSTAT_PENDING;
      else
         pBaseRec->Inst2Status[0] = TAX_BSTAT_UNPAID;
   } 

   // Penalty
   dTmp = atof(apTokens[TC_PEN_DUE1]);
   if (dTmp > 0.0)
      sprintf(pBaseRec->PenAmt1, "%.2f", dTmp);
   else
      pBaseRec->PenAmt1[0] = 0;

   dTmp = atof(apTokens[TC_PEN_DUE2]);
   if (dTmp > 0.0)
      sprintf(pBaseRec->PenAmt2, "%.2f", dTmp);
   else
      pBaseRec->PenAmt2[0] = 0;

   // Fee Paid
   dTmp = atof(apTokens[TC_OTHER_FEES]);
   if (dTmp > 0.0)
      sprintf(pBaseRec->TotalFees, "%.2f", dTmp);

   // Calculate unpaid amt
   dTmp = atof(apTokens[TC_TOTAL_DUE]);
   if (dTmp > 0.0)
      sprintf(pBaseRec->TotalDue, "%.2f", dTmp);
   else
      pBaseRec->TotalDue[0] = 0;

   strcpy(pBaseRec->Upd_Date, acUpdDate);

   lTaxMatch++;

   // Get next record
GetNextRec:
   pRec = fgets(acRec, MAX_RECSIZE, fd);
   if (!pRec)
   {
      fclose(fd);
      fd = NULL;
   }
   bDontParse=false;

   return 0;
}

/***************************** Slo_ParseCortac ******************************
 *
 * Prepare for SLO
 *
 * Return 0 if success
 *
 ****************************************************************************/

int Slo_ParseCortac(char *pOutbuf, char *pInbuf)
{
   int      iRet=0, iTmp;
   double	dTax1, dTax2, dTaxTotal, dPen1, dPen2, dDue1, dDue2;

   TAXBASE     *pOutRec = (TAXBASE *)pOutbuf;
   SLO_CORTAC  *pInRec  = (SLO_CORTAC *)pInbuf;

   // Clear output buffer
   memset(pOutbuf, 0, sizeof(TAXBASE));

   // APN
   memcpy(pOutRec->Apn, pInRec->Apn, TSIZ_APN);

   // Bill Number
   char *pTmp = pInRec->BillNum;
   sprintf(pOutRec->BillNum, "%.4s/%.2s %.3s,%.3s,%.3s", pInRec->BillYear, pInRec->BillYear+4, pTmp, pTmp+3, pTmp+6);

   // TRA
   memcpy(pOutRec->TRA, pInRec->TRA, TSIZ_TRA);

   // Keep current year only
   iTmp = atoin(pInRec->BillYear, 4);
   if (iTmp != lTaxYear)
      return -1;

   // Tax Year
   memcpy(pOutRec->TaxYear, pInRec->RollYear, 4);

#ifdef _DEBUG
   //if (!memcmp(pOutRec->Apn, "005241067", 9))
   //   iTmp = 0;
#endif

   // Check for Tax amount
   dDue1 = atofn(pInRec->TaxAmt1, TSIZ_TAXAMT);
   dDue2 = atofn(pInRec->TaxAmt2, TSIZ_TAXAMT);
   dPen1 = dPen2 = 0.0;
   if (pInRec->PenStatus1 == 'Y')
   {
      dPen1 = dDue1 / 11.0;
      dTax1 = (dDue1 - dPen1) + 0.4;
      sprintf(pOutRec->PenAmt1, "%.2f", dPen1/100.0);
   } else
      dTax1 = dDue1;
   if (pInRec->PenStatus2 == 'Y')
   {
      dPen2 = dDue2 / 11.0;
      dTax2 = (dDue2 - dPen2) + 0.4;
      sprintf(pOutRec->PenAmt2, "%.2f", dPen2/100.0);
   } else
      dTax2 = dDue2;

   // Paid?
   if (pInRec->PaidStatus1 == 'N')
      pOutRec->Inst1Status[0] = TAX_BSTAT_NOTAX;
   else
   {
      if (pInRec->PaidStatus1 == 'P')
      {
         sprintf(pOutRec->PaidAmt1, "%.2f", dDue1/100.0);
         pOutRec->Inst1Status[0] = TAX_BSTAT_PAID;
         dDue1 = 0;
      } else
         pOutRec->Inst1Status[0] = TAX_BSTAT_UNPAID;
   }

   if (pInRec->PaidStatus2 == 'N')
      pOutRec->Inst2Status[0] = TAX_BSTAT_NOTAX;
   else
   {
      if (pInRec->PaidStatus2 == 'P')
      {
         sprintf(pOutRec->PaidAmt2, "%.2f", dDue2/100.0);
         pOutRec->Inst2Status[0] = TAX_BSTAT_PAID;
         dDue2 = 0;
      } else
         pOutRec->Inst2Status[0] = TAX_BSTAT_UNPAID;
   }

   // Total tax
   dTaxTotal = dTax1+dTax2;
   if (dTaxTotal > 0.0)
   {
      sprintf(pOutRec->TaxAmt1, "%.2f", dTax1/100.0);
      sprintf(pOutRec->TaxAmt2, "%.2f", dTax2/100.0);
      sprintf(pOutRec->TotalTaxAmt, "%.2f", dTaxTotal/100.0);

      // DueDate
      memcpy(pOutRec->DueDate1, pInRec->DueDate1, TSIZ_DATE);
      memcpy(pOutRec->DueDate2, pInRec->DueDate2, TSIZ_DATE);
   }

   // Total due
   dTaxTotal = dDue1+dDue2;
   if (dTaxTotal > 0.0)
   {
      sprintf(pOutRec->TotalDue, "%.2f", dTaxTotal/100.0);
   }

   sprintf(pOutRec->Upd_Date, "%d", lTaxRollDate);
   pOutRec->isSecd[0] = '1';
   pOutRec->BillType[0] = BILLTYPE_SECURED;
   return 0;
}

/****************************** Slo_Load_Cortac ******************************
 *
 * Create import file from CortacFile and import into SQL Tax Base table
 *
 * Return 0 if success.
 *
 *****************************************************************************/

int Slo_Load_Cortac(bool bImport)
{
   char     *pTmp, acBase[1024], acRec[MAX_RECSIZE], acTCFile[_MAX_PATH];
   char     acOutFile[_MAX_PATH], acInFile[_MAX_PATH], acOutbuf[MAX_RECSIZE];

   int      iRet, iDrop=0;
   long     lBase=0, lCnt=0;
   FILE     *fdBase, *fdIn, *fdTC;
   TAXBASE  *pBase = (TAXBASE *)acBase;

   LogMsg0("Loading Tax Base");
   NameTaxCsvFile(acOutFile, myCounty.acCntyCode, "Base");

   GetIniString(myCounty.acCntyCode, "TaxRollInfo", "", acInFile, _MAX_PATH, acIniFile);
   lTaxRollDate = getFileDate(acInFile);
   GetIniString(myCounty.acCntyCode, "Main_TC", "", acTCFile, _MAX_PATH, acIniFile);
   if (acTCFile[0] > ' ')
   {
      lLastTaxFileDate = getFileDate(acTCFile);
      if (lLastTaxFileDate < lTaxRollDate)
      {
         lLastTaxFileDate = lTaxRollDate;
         // Ignore TC file
         LogMsg("*** %s is older than tax roll file, ignore this update", acTCFile);
         acTCFile[0] = 0;        
      }
   } else
   {
      LogMsg("*** TCF file (%s) not present.  Load Cortac only.", acTCFile);
      lLastTaxFileDate = lTaxRollDate;
   }

   // Only process if new tax file
   iRet = isNewTaxFile(NULL, myCounty.acCntyCode);
   if (iRet <= 0)
   {
      lLastTaxFileDate = 0;
      return iRet;
   }

   // Sort & Open Cortac file
   sprintf(acBase, "%s\\%s\\%s_Cortac.srt", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   // Sort on APN, BillNum
   iRet = sortFile(acInFile, acBase, "S(32,9,C,A,17,9,C,A)");   
   if (iRet < 100)
      return -1;

   LogMsg("Open Secured tax file %s", acBase);
   fdIn = fopen(acBase, "r");
   if (fdIn == NULL)
   {
      LogMsg("***** Error opening Secured tax file: %s\n", acBase);
      return -2;
   }  

   // Sort & open TC file
   fdTC = NULL;
   if (acTCFile[0] > ' ')
   {
      sprintf(acBase, "%s\\%s\\%s_TC.srt", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
      iRet = sortFile(acTCFile, acBase, "S(#1,C,A,#5,C,D,#6,C,A) DUPOUT(B5000,1,110)");   
      if (iRet > 0)
      {
         LogMsg("Open TC file %s (%d records)", acBase, iRet-1);
         fdTC = fopen(acBase, "r");
         if (fdTC == NULL)
         {
            LogMsg("***** Error opening Current TC file: %s\n", acBase);
            return -2;
         }  
      }
   }

   // Open Output file
   LogMsg("Create Base file %s", acOutFile);
   fdBase = fopen(acOutFile, "w");
   if (fdBase == NULL)
   {
      LogMsg("***** Error creating output file: %s\n", acOutFile);
      return -4;
   }

   // Merge loop 
   lTaxSkip=lTaxMatch=0;
   while (!feof(fdIn))
   {
      pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdIn);
      if (!pTmp || *pTmp < '0')
         break;

      // Create new R01 record
      iRet = Slo_ParseCortac(acBase, acRec);
      if (!iRet)
      {
         // Update TaxBase record using SLO.TCF
         if (fdTC)
            iRet = Slo_UpdateTaxBase(acBase, fdTC);

         // Replace comma with hyphen on BillNum
         //replChar(pBase->BillNum, ',', '-');

         // Create delimited record
         Tax_CreateTaxBaseCsv(acOutbuf, (TAXBASE *)&acBase);

         // Output record			
         lBase++;
         fputs(acOutbuf, fdBase);
      } else
      {
         LogMsg("---> Drop record %d [%.40s]", lCnt, acRec); 
         iDrop++;
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdIn)
      fclose(fdIn);
   if (fdTC)
      fclose(fdTC);
   if (fdBase)
      fclose(fdBase);

   printf("\nTotal records processed:     %u\n", lCnt);
   LogMsg("Total records processed:   %u", lCnt);
   LogMsg("    base records output:   %u", lBase);
   LogMsg("        Skip TC records:   %u", lTaxSkip);
   LogMsg("       TC records match:   %u", lTaxMatch);
   LogMsg("  Total records dropped:   %u", iDrop);

   // Import into SQL
   if (bImport)
      iRet = doTaxImport(myCounty.acCntyCode, TAX_BASE);
   else
      iRet = 0;

   return iRet;
}

/******************************* Slo_Update_Tax ******************************
 *
 * Create import file from CortacFile and import into SQL Tax Base table.
 * This function only insert records found in TCF file.
 *
 * Return 0 if success.
 *
 *****************************************************************************/

int Slo_Update_Tax(bool bImport)
{
   char     *pTmp, acBase[1024], acRec[MAX_RECSIZE], acTCFile[_MAX_PATH];
   char     acOutFile[_MAX_PATH], acInFile[_MAX_PATH], acOutbuf[MAX_RECSIZE];

   int      iRet, iDrop=0;
   long     lBase=0, lCnt=0;
   FILE     *fdBase, *fdIn, *fdTC;
   TAXBASE  *pBase = (TAXBASE *)acBase;

   LogMsg0("Loading Tax Base");
   NameTaxCsvFile(acOutFile, myCounty.acCntyCode, "Base");

   GetIniString(myCounty.acCntyCode, "Main_TC", "", acTCFile, _MAX_PATH, acIniFile);
   lLastTaxFileDate = getFileDate(acTCFile);
   GetIniString(myCounty.acCntyCode, "TaxRollInfo", "", acInFile, _MAX_PATH, acIniFile);
   lTaxRollDate = getFileDate(acInFile);
   if (lLastTaxFileDate < lTaxRollDate)
   {
      lLastTaxFileDate = lTaxRollDate;
      // Ignore TC file
      LogMsg("*** %s is older than tax roll file, ignore this update", acTCFile);
      acTCFile[0] = 0;        
   }

   // Only process if new tax file
   iRet = isNewTaxFile(NULL, myCounty.acCntyCode);
   if (iRet <= 0)
   {
      lLastTaxFileDate = 0;
      return iRet;
   }

   // Sort & Open Cortac file
   sprintf(acBase, "%s\\%s\\%s_Cortac.srt", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   // Sort on APN, BillNum
   iRet = sortFile(acInFile, acBase, "S(32,9,C,A,17,9,C,A)");   
   LogMsg("Open Secured tax file %s", acBase);
   fdIn = fopen(acBase, "r");
   if (fdIn == NULL)
   {
      LogMsg("***** Error opening Secured tax file: %s\n", acBase);
      return -2;
   }  

   // Sort & open TC file
   if (acTCFile[0] > ' ')
   {
      sprintf(acBase, "%s\\%s\\%s_TC.srt", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
      iRet = sortFile(acTCFile, acBase, "S(#1,C,A,#5,C,D,#6,C,A) DUPOUT(B5000,1,110)");   
      LogMsg("Open TC file %s (%d records)", acBase, iRet-1);
      fdTC = fopen(acBase, "r");
      if (fdTC == NULL)
      {
         LogMsg("***** Error opening Current TC file: %s\n", acBase);
         return -2;
      }  
   } else
      fdTC = NULL;

   // Open Output file
   LogMsg("Create Base file %s", acOutFile);
   fdBase = fopen(acOutFile, "w");
   if (fdBase == NULL)
   {
      LogMsg("***** Error creating output file: %s\n", acOutFile);
      return -4;
   }

   // Merge loop 
   lTaxSkip=lTaxMatch=0;
   while (!feof(fdIn))
   {
      pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdIn);
      if (!pTmp || *pTmp < '0')
         break;

      // Create new R01 record
      iRet = Slo_ParseCortac(acBase, acRec);
      if (!iRet)
      {
         // Update TaxBase record using SLO.TCF
         if (fdTC)
         {
            iRet = Slo_UpdateTaxBase(acBase, fdTC);
            if (!iRet)
            {
               // Create delimited record
               Tax_CreateTaxBaseCsv(acOutbuf, (TAXBASE *)&acBase);

               // Output record			
               lBase++;
               fputs(acOutbuf, fdBase);
            } else
            {
               LogMsg("---> Ignore Cortac record: Apn=%s BillNum=%s", pBase->Apn, pBase->BillNum);
               iDrop++;
            }
         }
      } else
      {
         LogMsg("---> Drop record %d Apn=%s BillNum=%s", lCnt, pBase->Apn, pBase->BillNum); 
         iDrop++;
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdIn)
      fclose(fdIn);
   if (fdTC)
      fclose(fdTC);
   if (fdBase)
      fclose(fdBase);

   printf("\nTotal records processed:     %u\n", lCnt);
   LogMsg("Total records processed:   %u", lCnt);
   LogMsg("    base records output:   %u", lBase);
   LogMsg("        Skip TC records:   %u", lTaxSkip);
   LogMsg("       TC records match:   %u", lTaxMatch);
   LogMsg("  Total records dropped:   %u", iDrop);

   // Import into SQL
   if (bImport)
      iRet = doTaxImport(myCounty.acCntyCode, TAX_BASE);
   else
      iRet = 0;

   return iRet;
}

/***************************** Slo_ParseTaxDetail ****************************
 *
 * Generate TAXDETAIL & TAXAGENCY
 * Ignore detail records of supplemental bill.
 *
 * Return number of detail records created if success
 *
 *****************************************************************************/

int Slo_ParseTaxDetail(FILE *fdDetail, FILE *fdAgency)
{
   char     *pTmp, acTmp[256], acOutbuf[512];
   int      iIdx, iCnt, iTmp;
   double   dTmp;
   TAXDETAIL *pDetail = (TAXDETAIL *)acOutbuf;
   TAXAGENCY sAgency, *pAgency, *pResult;

   // Clear output buffer
   memset(acOutbuf, 0, sizeof(TAXDETAIL));
   memset(&sAgency, 0, sizeof(TAXAGENCY));
   pAgency = &sAgency;

   // Don't need supp detail records
   if (strcmp(apTokens[SLO_T_APN], apTokens[SLO_T_BILL_NUMBER]))
      return 1;

   // APN
   strcpy(pDetail->Apn, apTokens[SLO_T_APN]);

   // BillNum
   pTmp = apTokens[SLO_T_BILL_NUMBER];
   sprintf(pDetail->BillNum, "%s %.3s,%.3s,%.3s", apTokens[SLO_T_BILL_YEAR], pTmp, pTmp+3, pTmp+6);

   // Use billing year for tax year
   iTmp = atoin(apTokens[SLO_T_BILL_YEAR], 4);
   if (iTmp != lTaxYear && pDetail->Apn[0] != '9')
      return -1;

   // Tax Year
   sprintf(pDetail->TaxYear, "%.4s", apTokens[SLO_T_BILL_YEAR]);
   iIdx = SLO_T_FUND_NUMBER_1;

#ifdef _DEBUG
   //if (!memcmp(pDetail->Apn, "910002402", 9))
   //   iCnt = 0;
#endif

   // Loop through tax items
   for (iCnt = 0; iCnt < SLO_T_MAXFUNDS && *apTokens[iIdx+1] > '0'; iCnt++)
   {      
      // Agency 
      sprintf(pDetail->TaxCode, "%.4d", atol(apTokens[iIdx]));
      strcpy(pAgency->Code, pDetail->TaxCode);

      // Tax Agency
      pResult = findTaxAgency(pAgency->Code);
      if (pResult)
      {
         strcpy(pAgency->Agency, pResult->Agency);
         strcpy(pAgency->Phone, pResult->Phone);
         pAgency->TC_Flag[0] = pResult->TC_Flag[0];
      } else
      {
         blankRem(apTokens[iIdx+1]);
         strcpy(pAgency->Agency, apTokens[iIdx+1]);
         pAgency->TC_Flag[0] = '0';
         LogMsg("---> Unknown Agency: %s-%s", apTokens[iIdx], apTokens[iIdx+1]);
      }

      // Tax Rate
      strcpy(pDetail->TaxRate, apTokens[iIdx+2]);

      // Tax Amt
      dTmp =  atof(apTokens[iIdx+3]);
      if (dTmp > 0.0)
         sprintf(pDetail->TaxAmt, "%.2f", dTmp);

      // Replace comma with hyphen on BillNum
      //replChar(pDetail->BillNum, ',', '-');

      // Generate csv line and write to file
      Tax_CreateDetailCsv(acTmp, pDetail);
      fputs(acTmp, fdDetail);

      // Forming Agency record
      Tax_CreateAgencyCsv(acTmp, pAgency);
      fputs(acTmp, fdAgency);

      iIdx += 5;
      // Bypass sub total
      if (strstr(apTokens[iIdx+1], "SUBTOTAL"))
         iIdx += 5;
   }

   return iCnt;
}

/**************************** Slo_Load_TaxDetail ******************************
 *
 * Create detail file and import into SQL
 *
 * Return 0 if success.
 *
 *****************************************************************************/

int Slo_Load_TaxDetail(char *pInFile, bool bImport)
{
   char     *pTmp, acRec[MAX_RECSIZE];
   char     acItemsFile[_MAX_PATH], acAgencyFile[_MAX_PATH], acTmpFile[_MAX_PATH];

   int      iRet;
   long     lItems=0, lCnt=0, lAgency=0, lDrop=0;
   FILE     *fdItems, *fdAgency, *fdIn;

   LogMsg0("Loading Detail file");

   cDelim = 9;
   NameTaxCsvFile(acItemsFile, myCounty.acCntyCode, "Items");
   NameTaxCsvFile(acAgencyFile, myCounty.acCntyCode, "Agency");
   sprintf(acTmpFile, "%s\\%s\\%s_Agency.tmp", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);

   // Open input file
   LogMsg("Open Detail file %s", pInFile);
   fdIn = fopen(pInFile, "r");
   if (fdIn == NULL)
   {
      LogMsg("***** Error opening Detail file: %s\n", pInFile);
      return -2;
   }  

   // Open Items file
   LogMsg("Create Items file %s", acItemsFile);
   fdItems = fopen(acItemsFile, "w");
   if (fdItems == NULL)
   {
      LogMsg("***** Error creating Items file: %s\n", acItemsFile);
      return -4;
   }

   // Open Agency file
   LogMsg("Create Agency file %s", acTmpFile);
   fdAgency = fopen(acTmpFile, "w");
   if (fdAgency == NULL)
   {
      LogMsg("***** Error creating Agency file: %s\n", acTmpFile);
      return -4;
   }

   // Remove header
   pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdIn);

   // Merge loop 
   while (!feof(fdIn))
   {
      pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdIn);
      if (!pTmp)
         break;

      // Parse input
      iTokens = ParseStringIQ(acRec, cDelim, MAX_FLD_TOKEN, apTokens);
      if (iTokens < SLO_T_TOTAL_FUND_AMOUNT)
      {
         if (iTokens > SLO_T_APN)
            LogMsg("*** Bad record: %s", apTokens[SLO_T_APN]);
         continue;
      }

      // Do not take unsecured record
      if (memcmp(apTokens[SLO_T_ROLL_TYPE], "UNS", 3))
      {
         // Create Items & Agency record
         iRet = Slo_ParseTaxDetail(fdItems, fdAgency);
         if (iRet > 0)
            lItems += iRet;
      } else
         lDrop++;

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdIn)
      fclose(fdIn);
   if (fdItems)
      fclose(fdItems);
   if (fdAgency)
      fclose(fdAgency);

   // Import into SQL
   if (bImport)
   {
      iRet = doTaxImport(myCounty.acCntyCode, TAX_DETAIL);
      if (!iRet)
      {
         // Update TotalRate
         iRet = doUpdateTotalRate(myCounty.acCntyCode, "B");

         // Dedup Agency file before import
         LogMsg("Dedup Agency file %s to %s", acTmpFile, acAgencyFile);
         lAgency = sortFile(acTmpFile, acAgencyFile, "S(#1,C,A) DUPOUT F(TXT)");
         if (lAgency > 0)
            iRet = doTaxImport(myCounty.acCntyCode, TAX_AGENCY);
      }
   } else
      iRet = 0;

   LogMsg("  Total records processed:  %u", lCnt);
   LogMsg("            Items records:  %u", lItems);
   LogMsg("           Agency records:  %u", lAgency);
   LogMsg("Unsecured records dropped:  %u\n", lDrop);

   return iRet;
}

/************************** Slo_ParseSupplemental ***************************
 *
 * Parse supplemental record and append to tax base
 *
 * Return 0 if success
 *
 ****************************************************************************/

int Slo_ParseSupplemental(char *pOutbuf)
{
   int      iRet=0, iTmp;
   double	dTax1, dTax2, dTaxTotal, dPaidTotal, dPen1, dPen2, dPaid1, dPaid2, dTotalDue;
   char     sTmp[256], *pTmp;

   TAXBASE     *pOutRec = (TAXBASE *)pOutbuf;

   // Clear output buffer
   memset(pOutbuf, 0, sizeof(TAXBASE));

   // APN
   strcpy(pOutRec->Apn, apTokens[SLO_SUP_ROLLASSMNT]);
   remChar(pOutRec->Apn, ',');

   // Bill Number
   sprintf(pOutRec->BillNum, "%s %s", apTokens[SLO_SUP_BILLYR], apTokens[SLO_SUP_BILLASSMNT]);

   // TRA
   strcpy(pOutRec->TRA, apTokens[SLO_SUP_TAXAREA]);
   remChar(pOutRec->TRA, '-');

   // Tax Year
   memcpy(pOutRec->TaxYear, pOutRec->BillNum, 4);

#ifdef _DEBUG
   //if (!memcmp(pOutRec->Apn, "005241067", 9))
   //   iTmp = 0;
#endif

   // Check for Tax refund - ignore this bill
   dTaxTotal = atof(apTokens[SLO_SUP_TOTBILLED]);
   if (dTaxTotal <= 0)
   {
      LogMsg("---> Drop supplemental Apn=%s, BillNum=%s Refund=%.2f", pOutRec->Apn, pOutRec->BillNum, dTaxTotal); 
      return 1;
   }

   // Check for Tax amount
   dPaidTotal= atof(apTokens[SLO_SUP_TOTPAID]);
   dTax1  = dTax2 = dTaxTotal / 2.0;
   dPaid1 = atof(apTokens[SLO_SUP_PAID1]);
   dPaid2 = atof(apTokens[SLO_SUP_PAID2]);
   dPen1  = atof(apTokens[SLO_SUP_PENALTY1]);
   dPen2  = atof(apTokens[SLO_SUP_PENALTY2]);

   // Total tax
   if (dTaxTotal > 0.0)
   {
      sprintf(pOutRec->TaxAmt1, "%.2f", dTax1);
      sprintf(pOutRec->TaxAmt2, "%.2f", dTax2);
      sprintf(pOutRec->TotalTaxAmt, "%.2f", dTaxTotal);

      // DueDate
      iTmp = InstDueDate(sTmp, 1, lTaxYear);
      strcpy(pOutRec->DueDate1, sTmp);
      if (pTmp=dateConversion(apTokens[SLO_SUP_DELQNTDT2], sTmp, MM_DD_YYYY_2, lTaxYear))
         strcpy(pOutRec->DueDate2, sTmp);
      else
         LogMsg("*** Invalid DueDate2: %s [%s]", apTokens[SLO_SUP_DELQNTDT2], pOutRec->Apn);
   }

   // Paid?
   if (dPaid1 > 0.0)
   {
      pOutRec->Inst1Status[0] = TAX_BSTAT_PAID;
      sprintf(pOutRec->PaidAmt1, "%.2f", dPaid1);
      if (pTmp=dateConversion(apTokens[SLO_SUP_PAYDT1], sTmp, MM_DD_YYYY_2))
      {
         strcpy(pOutRec->PaidDate1, sTmp);
      } else
         LogMsg("*** Invalid paid1 date: %s [%s]", apTokens[SLO_SUP_PAYDT1], pOutRec->Apn);
   } else
   {
      pOutRec->Inst1Status[0] = TAX_BSTAT_UNPAID;
   }

   if (dPaid2 > 0.0)
   {
      pOutRec->Inst2Status[0] = TAX_BSTAT_PAID;
      sprintf(pOutRec->PaidAmt2, "%.2f", dPaid2);
      if (pTmp=dateConversion(apTokens[SLO_SUP_PAYDT2], sTmp, MM_DD_YYYY_2))
         strcpy(pOutRec->PaidDate2, sTmp);
      else
         LogMsg("*** Invalid paid1 date: %s [%s]", apTokens[SLO_SUP_PAYDT2], pOutRec->Apn);
   } else
   {
      pOutRec->Inst2Status[0] = TAX_BSTAT_UNPAID;
   }

   // Total due
   dTotalDue= atof(apTokens[SLO_SUP_TOTBALDUE]);
   if (dTotalDue > 0.0)
   {
      sprintf(pOutRec->TotalDue, "%.2f", dTotalDue);
   }

   if (dPen1 > 0.0)
   {
      sprintf(pOutRec->PenAmt1, "%.2f", dPen1);
   }
   if (dPen2 > 0.0)
   {
      sprintf(pOutRec->PenAmt2, "%.2f", dPen2);
   }

   sprintf(pOutRec->Upd_Date, "%d", lTaxRollDate);
   pOutRec->isSupp[0] = '1';
   pOutRec->BillType[0] = BILLTYPE_SECURED_SUPPL;
   return 0;
}

/************************** Slo_Load_Supplemental ****************************
 *
 * Create import file from Supp.txt and import into SQL Tax Base table
 *
 * Return 0 if success.
 *
 *****************************************************************************/

int Slo_Load_Supplemental(bool bImport)
{
   char     *pTmp, acBase[1024], acRec[MAX_RECSIZE];
   char     acOutFile[_MAX_PATH], acInFile[_MAX_PATH], acOutbuf[MAX_RECSIZE];

   int      iRet, iDrop=0;
   long     lBase=0, lCnt=0;
   FILE     *fdBase, *fdIn;
   TAXBASE  *pBase = (TAXBASE *)acBase;

   LogMsg0("Loading Tax Supplemental");
   NameTaxCsvFile(acOutFile, myCounty.acCntyCode, "Base");

   GetIniString(myCounty.acCntyCode, "SuppTax", "", acInFile, _MAX_PATH, acIniFile);
   lLastTaxFileDate = getFileDate(acInFile);

   // Sort & Open Cortac file
   sprintf(acBase, "%s\\%s\\%s_Supp.srt", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   // Sort on APN, BillNum
   GetIniString(myCounty.acCntyCode, "SuppSortCmd", "S(#7,C,A,#4,C,A,#2,C,D) F(TXT) DELIM(9) OMIT(#4,C,GTE,\"UNS\")", acRec, _MAX_PATH, acIniFile);
   iRet = sortFile(acInFile, acBase, acRec);   
   if (iRet < 100)
      return -1;

   LogMsg("Open Supplemental tax file %s", acBase);
   fdIn = fopen(acBase, "r");
   if (fdIn == NULL)
   {
      LogMsg("***** Error opening Supplemental tax file (%d): %s\n", _errno, acBase);
      return -2;
   }  

   // Open Output file
   LogMsg("Open Base file %s", acOutFile);
   fdBase = fopen(acOutFile, "a+");
   if (fdBase == NULL)
   {
      LogMsg("***** Error opening output file for append (%d): %s\n", _errno, acOutFile);
      return -4;
   }

   // Merge loop 
   lTaxSkip=lTaxMatch=0;
   while (!feof(fdIn))
   {
      pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdIn);
      if (!pTmp || *pTmp < '0')
         break;

      // Parse input
      iTokens = ParseStringIQ(acRec, 9, MAX_FLD_TOKEN, apTokens);
      if (iTokens < SLO_SUP_FLDS)
      {
         if (iTokens > SLO_T_APN)
            LogMsg("*** Bad record: %s", apTokens[SLO_SUP_ROLLASSMNT]);
         continue;
      }

      // Create base record
      iRet = Slo_ParseSupplemental(acBase);
      if (!iRet)
      {
         // Create delimited record
         Tax_CreateTaxBaseCsv(acOutbuf, (TAXBASE *)&acBase);

         // Output record			
         lBase++;
         fputs(acOutbuf, fdBase);
      } else
      {
         iDrop++;
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdIn)
      fclose(fdIn);
   if (fdBase)
      fclose(fdBase);

   printf("\nTotal records processed:     %u\n", lCnt);
   LogMsg("Total records processed:   %u", lCnt);
   LogMsg("    base records output:   %u", lBase);
   LogMsg("  Total records dropped:   %u", iDrop);

   // Import into SQL
   if (bImport)
      iRet = doTaxImport(myCounty.acCntyCode, TAX_BASE);
   else
      iRet = 0;

   return iRet;
}

/*************************** Slo_Load_TaxDelq ********************************
 *
 * Create import file from Supp.txt and import into SQL Tax Base table
 *
 * Return 0 if success.
 *
 *****************************************************************************/

int Slo_ParseDelq(char *pOutbuf, char *pInbuf)
{
   double   dTaxAmt, dTmp;
   TAXDELQ  *pOutRec= (TAXDELQ *)pOutbuf;
   SLO_DELQ *pInRec = (SLO_DELQ *)pInbuf;

   // Clear output buffer
   memset(pOutbuf, 0, sizeof(TAXDELQ));

   // APN
   memcpy(pOutRec->Apn, pInRec->Apn, TSIZ_APN);

   // TDN
   memcpy(pOutRec->Default_No, pInRec->DefaultNumber, TSIZ_DEFNUM);

   // Tracer
   memcpy(pOutRec->BillNum, pInRec->BillNum, TSIZ_BILLNUM);

   // Supplemental delq
   if (pInRec->RollType[1] == 'E')
      pOutRec->isSecd[0] = '1';
   else if (pInRec->RollType[1] == 'U')
      pOutRec->isSupp[0] = '1';
   else
      return 1;

   // Updated date
   sprintf(pOutRec->Upd_Date, "%d", lLastTaxFileDate);

   // Tax Year
   memcpy(pOutRec->TaxYear, pInRec->BillYear, 4);

   // Default date
   memcpy(pOutRec->Def_Date, pInRec->DefaultDate, TSIZ_DATE);

   // Default amount
   dTaxAmt  = atoln(pInRec->OrgTaxAmt, TSIZ_TAXAMT)/100.0;
   if (dTaxAmt > 0.0)
   {
      sprintf(pOutRec->Def_Amt, "%.2f", dTaxAmt);
      if (pInRec->InstPlanStatus[0] == 'G')
         pOutRec->DelqStatus[0] = TAX_STAT_ONPAYMENT;
      else
      {
         pOutRec->isDelq[0] = '1';
         if (pInRec->InstPlan[0] == 'D')
            pOutRec->DelqStatus[0] = TAX_STAT_DFLTINST;  // Default installment plan
         else
            pOutRec->DelqStatus[0] = TAX_STAT_UNPAID;
      }
   }

   // Recission fee
   dTmp = atoln(pInRec->RecissionFee, TSIZ_FEEAMT);

   // Misc fee
   dTmp += atoln(pInRec->MiscFee, TSIZ_TAXAMT);
   if (dTmp > 0.0)
      sprintf(pOutRec->Fee_Amt, "%.2f", dTmp/100.0);

   return 0;
}

int Slo_Load_TaxDelq(char *pInFile, bool bImport)
{
   char     *pTmp, acDelq[1024], acRec[MAX_RECSIZE];
   char     acOutFile[_MAX_PATH], acOutbuf[MAX_RECSIZE];

   int      iRet, iDrop=0;
   long     lOut=0, lCnt=0;
   FILE     *fdOut, *fdIn;
   TAXDELQ  *pDelq = (TAXDELQ *)acDelq;
   SLO_DELQ *pDefRec = (SLO_DELQ *)&acRec[0];

   LogMsg0("Loading Tax Delinquent");

   // Sort & Open Delq file
   sprintf(acDelq, "%s\\%s\\%s_Delq.srt", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   // Sort on APN, Bill year
   GetIniString(myCounty.acCntyCode, "DelqSortCmd", "S(32,9,C,A,11,4,C,A) OMIT(86,3,C,EQ,\"UTL\",OR,11,6,C,LT,\"202021\")", acRec, _MAX_PATH, acIniFile);
   iRet = sortFile(pInFile, acDelq, acRec);   
   if (iRet < 100)
      return -1;

   LogMsg("Open Delq tax file %s", acDelq);
   fdIn = fopen(acDelq, "r");
   if (fdIn == NULL)
   {
      LogMsg("***** Error opening Delq tax file (%d): %s\n", _errno, acDelq);
      return -2;
   }  

   // Open Output file
   NameTaxCsvFile(acOutFile, myCounty.acCntyCode, "Delq");
   LogMsg("Open output file %s", acOutFile);
   fdOut = fopen(acOutFile, "w");
   if (fdOut == NULL)
   {
      LogMsg("***** Error creating output file: %s\n", acOutFile);
      return -4;
   }

   // Merge loop 
   lTaxSkip=lTaxMatch=0;
   while (!feof(fdIn))
   {
      pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdIn);
      if (!pTmp || *pTmp < ' ')
         break;

#ifdef _DEBUG
      //if (!memcmp(pDefRec->Apn, "30218200", 8) || !memcmp(pDefRec->Apn, "00127306", 8) || !memcmp(pDefRec->Apn, "06657107", 8))
      //   iRet = 0;
#endif
      // Create new Delq record
      iRet = Slo_ParseDelq(acDelq, acRec);
      if (!iRet)
      {
         // Create delimited record
         Tax_CreateDelqCsv(acOutbuf, (TAXDELQ *)&acDelq);

         // Output record			
         lOut++;
         fputs(acOutbuf, fdOut);
      } else 
      {
         LogMsg("---> Drop Delq record %d [%.9s], DeftNum=%.6s", lCnt, pDefRec->Apn, pDefRec->DefaultNumber); 
         pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdIn);
         iDrop++;
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdIn)
      fclose(fdIn);
   if (fdOut)
      fclose(fdOut);

   printf("\nTotal records processed:     %u\n", lCnt);
   LogMsg("Total records processed:   %u", lCnt);
   LogMsg("    Delq records output:   %u", lOut);
   LogMsg("  Total records dropped:   %u", iDrop);

   // Import into SQL
   if (bImport)
      iRet = doTaxImport(myCounty.acCntyCode, TAX_DELINQUENT);
   else
      iRet = 0;

   return iRet;
}

/*********************************** loadSlo ********************************
 *
 * Notes: Should use VEN sale update scheme
 *
 ****************************************************************************/

int loadSlo(int iSkip)
{
   int   iRet=0;
   char  acTmp[_MAX_PATH], acTmpFile[_MAX_PATH];

   iApnLen = myCounty.iApnLen;
   iApnOffset = GetPrivateProfileInt(myCounty.acCntyCode, "ApnOffset", 0, acIniFile);

   // Load tax
   if (iLoadTax == TAX_LOADING)                    // -T or -Lt
   {
      iRet = GetIniString(myCounty.acCntyCode, "TaxDist", "", acTmp, _MAX_PATH, acIniFile);
      if (iRet > 0)
         iRet = LoadTaxCodeTable(acTmp);

      // Load Tax_Base
      iRet = GetIniString(myCounty.acCntyCode, "Main_TC", "", acTmp, _MAX_PATH, acIniFile);
      GetIniString(myCounty.acCntyCode, "TaxRollInfo", "", acTmpFile, _MAX_PATH, acIniFile);
      lTaxRollDate = getFileDate(acTmpFile);
      lLastTaxFileDate = getFileDate(acTmp);
      if (lLastTaxFileDate > lTaxRollDate)
      {      
         // Create tax base using records from TCF file
         iRet = Slo_Update_Tax(false);
      } else
      {
         // Create tax base using records from Cortac file
         iRet = Slo_Load_Cortac(false);
      }

      if (!iRet && lLastTaxFileDate > 0)
      {
         // Load Supplemental
         iRet = Slo_Load_Supplemental(bTaxImport);
         // If no supplemental data, import tax base
         if (iRet < 0 && bTaxImport)
            iRet = doTaxImport(myCounty.acCntyCode, TAX_BASE);

         // Only load detail file if input file available
         int iTmp = GetIniString(myCounty.acCntyCode, "TaxDetail", "", acTmp, _MAX_PATH, acIniFile);
         if (iTmp > 0 && !_access(acTmp, 0))
            iRet = Slo_Load_TaxDetail(acTmp, bTaxImport);
      }

      if (!iRet)
      {
         // Load tax delq
         int iTmp = GetIniString(myCounty.acCntyCode, "TaxDelq", "", acTmp, _MAX_PATH, acIniFile);
         if (iTmp > 0 && !_access(acTmp, 0))
         {
            iRet = Slo_Load_TaxDelq(acTmp, bTaxImport);
            if (!iRet)
               iRet = updateDelqFlag(myCounty.acCntyCode);
         }
      }

      //if (!iRet)
      //   iRet = Slo_Load_TaxOwner(bTaxImport);
   } else if (iLoadTax == TAX_UPDATING)            // -Ut
   {
      iRet = Update_TC(myCounty.acCntyCode, bTaxImport, false);
   }

   // Exit if load/update tax only
   if (!iLoadFlag)
      return iRet;

   // Load quality tables
   //iRet = LoadLUTable((char *)&acLookupTbl[0], "[Quality]", NULL, MAX_ATTR_ENTRIES);
   //if (!iRet)
   //{
   //   LogMsg("*** Error Looking for table [Quality] in %s", acLookupTbl);
   //   return 1;
   //}

   // Setup file name for LDR extract - 07/11/2019
   //GetIniString(myCounty.acCntyCode, "LienTbl", "", acRollTbl, _MAX_PATH, acIniFile);
   //GetIniString(myCounty.acCntyCode, "LienKey", "", acRollKey, _MAX_PATH, acIniFile);
   //GetIniString(myCounty.acCntyCode, "LienDB", "", acSecuredMdb, _MAX_PATH, acIniFile);

   // Extract lien
   if (iLoadFlag & EXTR_LIEN)                      // -Xl
      iRet = Slo_ExtrLien2();

   // Extract Prop8
   if (lOptProp8 == MYOPT_EXT)
      iRet = Slo_ExtrProp8();

   // SLO pages have been merged.  There is no need to do this
   //iRet = loadMapIndex(myCounty.acCntyCode);

   // Load or Convert CHAR file
   //if (iLoadFlag & LOAD_ATTR)                      // -Lc
   //{
   //   LogMsg("Loading CHAR file");
   //   iRet = Slo_Extr_SLOChar(acRollFile, acCChrFile);
   //   if (iRet < 0)
   //   {
   //      LogMsg("***** Error in extracting CHAR from %s", acRollFile);
   //      return iRet;
   //   } 
   //}

   // Extract CHAR from roll file
   if (iLoadFlag & EXTR_ATTR)                      // -Xa
   {
      GetIniString(myCounty.acCntyCode, "RollFile", "", acTmpFile, _MAX_PATH, acIniFile);
      LogMsg("Extracting CHAR from roll file");
      iRet = Slo_ExtrChar(acTmpFile, acCChrFile);
      if (iRet < 0)
      {
         LogMsg("***** Error in extracting CHAR from %s", acTmpFile);
         return iRet;
      } 
   }

   // Extract Sale 
   if (iLoadFlag & EXTR_SALE)                      // -Xs
   {
      // Load special Deed xref table 
      GetIniString(myCounty.acCntyCode, "DeedXref", "", acTmp, _MAX_PATH, acIniFile);
      iNumDeeds = LoadXrefTable(acTmp, (XREFTBL *)&asDeed[0], MAX_DEED_ENTRIES);

      LogMsg("Loading Sale file");
      iRet = Slo_ExtrSale1(acSaleFile);
      if (iRet < 0)
      {
         LogMsg("***** Error in extracting SALE from %s", acSaleFile);
         return iRet;
      } 

      iLoadFlag |= MERG_CSAL;
   }

      // Extract NDC recorder sale 
   if (iLoadFlag & EXTR_NRSAL)                     // -Xn
   {
      iRet = GetIniString(myCounty.acCntyCode, "NdcSale", "", acSaleFile, _MAX_PATH, acIniFile);
      if (!_access(acSaleFile, 0))
      {
         iRet = NR_CreateSCSale(myCounty.acCntyCode, acSaleFile, iApnLen);
         if (iRet)
            return iRet;
      } else
      {
         LogMsg("*** Sale file not available.  Please verify: %s", acSaleFile);
         return -1;
      }
   }

   // Load roll file (from secured master)
   if (iLoadFlag & LOAD_LIEN)                      // -L
   {
      // CSV version - 2019
      iRet = Slo_Load_LDR(iSkip);
      // MDB version - before 2019
      //iRet = Slo_CreatePQ4(iSkip);
   } else if (iLoadFlag & LOAD_UPDT)               // -U
   {
      // Create update roll R01
      iRet = Slo_Load_Roll(iSkip);
   }

   if (!iRet && bMergeOthers)
   {
      sprintf(acTmpFile, acLienTmpl, myCounty.acCntyCode, myCounty.acCntyCode);
      if (!_access(acTmpFile, 0))
         iRet = PQ_MergeOthers(myCounty.acCntyCode, acTmpFile, 0, iSkip);
      else
         LogMsg("***** Lien extract is missing: %s.  Please locate the file and retry", acTmpFile);
   }

   // Apply cum sale file to R01
   if (!iRet && (iLoadFlag & MERG_CSAL) )          // -Ms
   {
      // Apply Slo_Sale.sls to R01 file
      iRet = ApplyCumSale(iSkip, acCSalFile, false, SALE_USE_SCUPDXFR, CLEAR_UPD_SALE);
   }

   // Apply NDC sale file to R01
   if (!iRet && (iLoadFlag & UPDT_XSAL))           // -Mn
   {
      // Apply Slo_Ash.sls to R01 file
      GetIniString("Data", "ASH_File", acESalTmpl, acESalTmpl, _MAX_PATH, acIniFile);
      sprintf(acTmpFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "sls");
      iRet = ApplyCumSaleNR(iSkip, acTmpFile, SALE_USE_SCUPDXFR);
      if (!iRet)
         iLoadFlag |= LOAD_UPDT;
   }

   return iRet;
}

