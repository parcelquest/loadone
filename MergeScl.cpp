/*********************************** loadScl ********************************
 *
 * Input files:
 *    - Scl_Lien        (lien file, fixed length 402 /w CRLF)
 *    - Scl_Roll        (roll file, fixed length 402 /w CRLF)
 *    - Scl_Situs       (Situs file, fixed length 106 /w CRLF)
 *
 * Fields not used:
 *    - ExeType, Well, SecuredImpr, UnsecuredImpr, Parcel Flag.
 *
 * Following are tasks to be done here:
 *    - To load lien, run -CSCL -L -O [-Mg|-Ms]
 *      Rename MF901.txt to Scl_Lien and SF902.txt to Scl_Situs before running
 *    - To update std usecode, run -CSCL -Uu 
 *    - For monthly update, run -SCL -U -O
 *    - No update is setup for SCL yet because county is update annually
 *
 * 05/29/2007  1.4.14   Adding GrGr
 * 06/21/2007  1.4.16   Bug fix in merge mail addr in Scl_MergeSale()
 * 08/08/2007  1.4.24   Change SALE_REC to SALE_REC1 avoid using special case.
 *                      Also adding more log message.
 * 11/07/2007  1.4.29   Fix Owner name problem when name1 is a number.
 * 11/18/2007  1.4.31   Rename Scl_MergeSale() to Scl_MergeGrGrRec().  Fix problem 
 *                      that remove mail addr of all owner name when update cum sale.
 *                      Use MergeCumGrGr() to update sale at LDR.
 * 12/21/2007  1.5.1    Reorganize and renaming files in GRGR processing
 * 01/15/2008  1.5.3    Fix bug in Scl_FormatOwner() that wipes out lien values
 * 01/16/2008  1.5.3.8  Remove /TRT from owner name before parsing. Drop sales that 
 *                      occurs on the same day without sale price.  Update mail addr 
 *                      only if sale date > lien date and mail addr <> situs addr.
 *                      If DocType is a number, copy as is.  Otherwise, call findDocType()
 *                      to translate it.  Add flag to Scl_MergeGrGrRec() allows user to 
 *                      specify option to copy all records regardless of DocType and SalePrice.
 *                      This option is specified in INI file.
 * 03/21/2008 1.5.7.1   Use standard function to update usecode.  
 * 03/25/2008 1.5.8     Remove retired parcels
 *                      Adding -Uu option to update std use.
 * 06/10/2008 1.6.4     Only update owner name if recdate > liendate.  
 * 06/11/2008 1.6.4.1   Modify Scl_MergeGrGrRec() to fix problem when updating
 *                      same sale record (caused duplicate)
 * 12/11/2008 8.5.1     Make StrNum left justified
 * 01/17/2009 8.5.6     Fix TRA
 * 05/11/2009 8.8       Set full exemption flag
 * 06/02/2009 8.8.2     Save ParcelFlags to LIENEXTR file.
 * 07/21/2009 9.1.3     Change logic to compute IMPR value.  The value from the county
 *                      includes Structure & Fixture.  We have to subtract Fixtue from IMPR.
 *                      Remove -Ms option and use -Mg instead.
 * 12/06/2009 9.3.4     Force return error if GRGR data not available.
 * 08/17/2010 10.1.11   Add ParcelFlg
 * 08/25/2010 10.1.12   Force EXE_TOTAL to be <= GROSS.  Populate EXE_CD1=EXE_TYPE.
 * 07/12/2011 11.0.2    Add S_HSENO.
 * 06/04/2012 11.9.9    Create GrGr import file if load successful.
 * 10/14/2012 12.3.0    Remove import GRGR code that didn't work correctly.  Let _tmain()
 *                      do it globally by using -Gi option and set ImportGrgr=Y in LOADONE.INI file
 * 12/11/2012 12.3.7    Use SCSAL_REC for GrGr_Exp.???. Modify Scl_ExtrSaleMatched() to output
 *                      all records that has sale price.
 * 03/12/2013 12.5.0    Remove redundant code
 * 03/19/2013 12.5.1.1  Filter bad zipcode for CA mailing addr.
 * 10/15/2013 13.11.2   Use updateVesting() to update Vesting and Etal flag.
 * 10/17/2013 13.11.4   Modify Scl_MergeGrGrFile() to use Grgr_exp.sls file if Grgr_exp.dat file not avail.
 *                      Modify Scl_FormatOwner() & Scl_MergeGrGrRec() to check for vesting from owner name.
 * ---                  Keep name as is 00301049
 * 08/28/2015 15.1.3    Add -Mr option to add lot sqft & lot area
 * 09/13/2015 15.2.2    Standardize MergeArea.
 * 04/06/2016 15.8.3    Load Tax data.
 * 06/27/2016 16.0.1    Fix M_Zip bug in Scl_MergeMAdr()
 * 07/14/2016 16.1.0    Add -Lc option. Add new functiona Scl_ExtrChar() and Scl_MergeChar()
 *                      There are more CHAR fields to be added when request.
 * 07/29/2016 16.1.3    Fix bug in Scl_MergeGrGrFile() that mistakenly uses M01 instead on S01 for input.
 * 11/10/2016 16.6.1    Modify LoadGrGr() and related functions to process new GRGR csv format.
 *                      We now allow NOTICE OF DEFAULT to be in transfer if APN is available.
 * 01/05/2017 16.8.6    Modify Scl_ParseGrGrName() to fix bad char in NAMES.
 *                      Modify Scl_LoadGrGrCsv() & Scl_ExtrSaleMatched() to support multi APN records.
 * 01/27/2017 16.9.9    Modify Scl_ExtrSaleMatched() and Scl_MergeGrGrFile to merge GrGr based on defined
 *                      CSalFile in INI file.
 * 06/19/2017 16.15.4   Fix bug in Scl_ParseGrGrName() to stop copying names at MAX_NAMES
 * 07/26/2017 17.1.5    Modify Scl_MergeGrGrRec() to support update owner option. Fix bug in Scl_MergeMAdr()
 *                      Modify Scl_MergeRoll() to update XFER only when DOCDATE is valid.
 * 09/09/2017 17.2.3    Modify Scl_MergeRoll() to fix USE_CODE issue.
 * 03/05/2018 17.6.8    Adding -Xs & -Ms options to use sales data from Attom data extract.  
 *                      Modify Scl_MergeGrGrRec() to update only when update record is newer than last
 *                      and a sale transaction with sale price > 0.  Modify Scl_MergeGrGrFile() to
 *                      allow caller to specify input sasle file.
 * 03/07/2018           Add -Ma option and fix bug in Scl_MergeChar().
 * 03/09/2018           Fix bug in Scl_MergeGrGrFile()
 * 03/22/2018 17.6.9    Add -Ps option to purge old sale.  Modify Scl_MergeGrGrFile() to clear old sales.
 *                      Remove setting MERG_CSAL when extract sales from loadScl()
 * 04/25/2018 17.10.2   Modify Scl_MergeGrGrRec() to merge owner info on GD only
 * 07/18/2018 18.2.1    Check for known bad zipcode in Scl_MergeMAdr() and populate situs zip in Scl_MergeSitus()
 * 08/09/2018 18.3.0    Add -Mn to merge sale from NDC.
 * 08/14/2018 18.4.0    Replace R01.MergeArea() with PQ_MergeLotArea() using new SCL_Basemap.txt
 * 09/29/2018 18.5.1    Move -Mr option to LoadOne.cpp
 * 11/21/2018 18.6.3    Modify Scl_LoadGrGrCsv() to accept delimiter "GrDelim" from INI file.
 * 05/15/2019 18.12.2   Update CI_FormatStdChar(), MF_FormatStdChar(), SF_FormatStdChar().
 *                      Modify Scl_ExtrSaleMatched() to output GrGr_Exp.dat in SCSAL_REC format. 
 *                      Modify Scl_MergeGrGrFile() to append output to SCL_GRGR.SLS and sort it before calling Scl_ExtrSaleMatched().
 *                      Call convertSaleData() to generate SALE_EXP.SLS in SCSAL_EXT format after successfully load GrGr.
 *                      Modify Scl_LoadGrGrCsv() to store DocTax as %.2f
 * 06/25/2019 19.0.0    Clear old sales before applying GRGR.
 * 03/21/2020 19.8.4    Stop creating "no longer used" SALE_EXP.SLS and update log msg.
 * 04/02/2020 19.8.7    Remove -Xs and replace it with -Xn option to format and merge NDC recorder data.
 * 04/27/2020 19.9.0    Remove -Ut and use -T to load both full & partial tax file with TC_LoadTax().
 * 07/22/2020 20.2.3    Modify -Mg option to check for new Scl_GrGr.sls before processing.
 * 07/29/2020 20.2.4    Fix sale price in Scl_LoadGrGrCsv()
 * 10/28/2020 20.3.6    Modify Scl_MergeRoll() to populate PQZoning.
 * 11/06/2021 21.3.0    Add Scl_ExtrLegal() & Scl_MergeLegalFile() to load and merge legal file.
 * 04/26/2022 21.9.0    Use acGrBkTmpl as backup folder for GrGr files. 
 * 05/25/2022 21.9.1    Add Scl_Load_TaxBase() & Scl_Load_TaxItem() to support new tax files.
 * 07/27/2022 22.1.1    Add Scl_ExtrLegalCsv() to suppot new legal format. Modify Scl_Load_LDR()
 *                      to merge legal using extracted legal file.
 * 09/01/2022 22.2.0    Add Scl_Update_TaxBase() & Scl_Load_TaxDelq() to support -Ut option.
 * 11/05/2022 22.3.1    Process redemption file and populate Tax_Delq table.
 * 07/27/2023 23.0.7    Add Scl_MergeSitusCsv() and add special cases to Scl_MergeMAdr().
 * 08/09/2023 23.1.3    Modify loadScl() to remove code that uses TC files.  Check TaxRoll file size
 *                      to determine whether to do full load or just update Base table.
 *                      Modify Scl_Update_TaxBase() to generate county specific Tmp???_Base file.
 * 02/02/2024 23.6.0    Modify Scl_MergeMAdr() to populate UnitNox.
 * 07/23/2024 24.0.8    Modify Scl_MergeRoll() to add ExeType.
 * 08/24/2024 24.1.3    Modify Scl_MergeMAdr() to extend M_CITY and fix UK city name.
 * 08/31/2024 24.1.5    Clean up Scl_MergeMAdr().
 * 10/08/2024 24.1.6    Modify Scl_ParseTaxBase() to keep all base records (even prior tax year)
 *
 ****************************************************************************/

#include "stdafx.h"
#include "CountyInfo.h"
#include "hlAdo.h"
#include "Prodlib.h"
#include "R01.h"
#include "Tables.h"
#include "Logs.h"
#include "LoadDef.h"
#include "Utils.h"
#include "XlatTbls.h"
#include "doSort.h"
#include "doOwner.h"
#include "doGrgr.h"
#include "formatApn.h"
#include "LoadOne.h"
#include "RecDef.h"
#include "CharRec.h"

#include "SaleRec.h"
#include "MergeScl.h"
#include "UseCode.h"
#include "LOExtrn.h"
#include "PQ.h"
#include "Tax.h"
#include "NdcExtr.h"

static   FILE  *fdSitus, *fdChar, *fdLegal;
static   long  lSitusSkip, lSitusMatch, lCharMatch, lCharSkip, lLegalSkip, lLegalMatch;

/******************************** Scl_MergeChar ******************************
 *
 *
 *****************************************************************************/

int Scl_MergeChar(char *pOutbuf)
{
   static char    acRec[2048], *pRec=NULL;
   char           acTmp[256];
   long           lTmp, lBldgSqft, lGarSqft;
   int            iRet, iLoop, iBeds, iFBath, iHBath, iUnits;
   STDCHAR       *pChar;

   iRet=iBeds=iUnits=0;
   lBldgSqft=lGarSqft=0;

   // Get first Char rec for first call
   if (!pRec)
      pRec = fgets(acRec, 1024, fdChar);

   do
   {
      // Compare Apn
      iLoop = memcmp(pOutbuf, acRec, iApnLen);
      if (iLoop > 0)
      {
         if (bDebug)
            LogMsg0("Skip Char rec  %.*s", iApnLen, pRec);

         pRec = fgets(acRec, 1024, fdChar);
         if (!pRec)
         {
            fclose(fdChar);
            fdChar = NULL;
            return 1;      // EOF
         }
         lCharSkip++;
      }
   } while (iLoop > 0);

   // If not match, return
   if (iLoop)
      return 1;

   pChar = (STDCHAR *)acRec;

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "0119009000", 10) )
   //   lTmp = 0;
#endif

   // Zoning
   memcpy(pOutbuf+OFF_ZONE, pChar->Zoning, SIZ_ZONE);

   // Lot sqft
   lTmp = atoin(pChar->LotSqft, SIZ_CHAR_SQFT);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*u", SIZ_LOT_SQFT, lTmp);
      memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);
   } else
      memset(pOutbuf+OFF_LOT_SQFT, ' ', SIZ_LOT_SQFT);

   lTmp = atoin(pChar->LotAcre, SIZ_CHAR_SQFT);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*u", SIZ_LOT_ACRES, lTmp);
      memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);
   } else
      memset(pOutbuf+OFF_LOT_ACRES, ' ', SIZ_LOT_ACRES);

   // BldgSqft
   lBldgSqft = atoin(pChar->BldgSqft, SIZ_CHAR_SQFT);
   if (lBldgSqft > 0)
   {
      sprintf(acTmp, "%*u", SIZ_BLDG_SF, lBldgSqft);
      memcpy(pOutbuf+OFF_BLDG_SF, acTmp, SIZ_BLDG_SF);
   } else
      memset(pOutbuf+OFF_BLDG_SF, ' ', SIZ_BLDG_SF);

   // Yrblt
   lTmp = atoin(pChar->YrBlt, SIZ_CHAR_YRBLT);
   if (lTmp > 0)
      memcpy(pOutbuf+OFF_YR_BLT, pChar->YrBlt, SIZ_YR_BLT);
   else
      memset(pOutbuf+OFF_YR_BLT, ' ', SIZ_YR_BLT); // Clean up bad data

   // YrEff
   lTmp = atoin(pChar->YrEff, SIZ_CHAR_YRBLT);
   if (lTmp > 0)
      memcpy(pOutbuf+OFF_YR_EFF, pChar->YrEff, SIZ_YR_BLT);
   else
      memset(pOutbuf+OFF_YR_EFF, ' ', SIZ_YR_BLT); // Clean up bad data

   // Stories   
   if (pChar->Stories[0] > ' ')
      memcpy(pOutbuf+OFF_STORIES, pChar->Stories, SIZ_STORIES);
   
   // Beds
   iBeds = atoin(pChar->Beds, SIZ_CHAR_BEDS);
   if (iBeds > 0)
   {
      sprintf(acTmp, "%*u", SIZ_BEDS, iBeds);
      memcpy(pOutbuf+OFF_BEDS, acTmp, SIZ_BEDS);
   }

   // Bath
   iFBath = atoin(pChar->FBaths, SIZ_CHAR_BATHS);
   if (iFBath > 0)
   {
      sprintf(acTmp, "%*u", SIZ_BATH_F, iFBath);
      memcpy(pOutbuf+OFF_BATH_F, acTmp, SIZ_BATH_F);
   }

   iHBath = atoin(pChar->HBaths, SIZ_CHAR_BATHS);
   if (iHBath > 0)
   {
      sprintf(acTmp, "%*u", SIZ_BATH_H, iHBath);
      memcpy(pOutbuf+OFF_BATH_H, acTmp, SIZ_BATH_H);
   }

   if (pChar->Bath_1Q[0] > '0')
      *(pOutbuf+OFF_BATH_1Q) = pChar->Bath_1Q[0];
   if (pChar->Bath_2Q[0] > '0')
      *(pOutbuf+OFF_BATH_2Q) = pChar->Bath_2Q[0];
   if (pChar->Bath_3Q[0] > '0')
      *(pOutbuf+OFF_BATH_3Q) = pChar->Bath_3Q[0];
   if (pChar->Bath_4Q[0] > '0')
      *(pOutbuf+OFF_BATH_4Q) = pChar->Bath_4Q[0];

   // Unit
   iUnits = atoin(pChar->Units, SIZ_CHAR_UNITS);
   if (iUnits > 0)
   {
      sprintf(acTmp, "%*u", SIZ_UNITS, iUnits);
      memcpy(pOutbuf+OFF_UNITS, acTmp, SIZ_UNITS);
   }

   // Rooms
   iRet = atoin(pChar->Rooms, SIZ_CHAR_ROOMS);
   if (iRet > 0)
   {
      sprintf(acTmp, "%*u", SIZ_ROOMS, iRet);
      memcpy(pOutbuf+OFF_ROOMS, acTmp, SIZ_ROOMS);
   }

   // Gargage Size
   iRet = atoin(pChar->GarSqft, SIZ_CHAR_SQFT);
   if (iRet > 0)
   {
      sprintf(acTmp, "%*u", SIZ_GAR_SQFT, iRet);
      memcpy(pOutbuf+OFF_GAR_SQFT, acTmp, SIZ_GAR_SQFT);
   }

   // Garage
   *(pOutbuf+OFF_PARK_TYPE) = pChar->ParkType[0];

   // Air/Heating
   *(pOutbuf+OFF_AIR_COND) = pChar->Cooling[0];
   *(pOutbuf+OFF_HEAT) = pChar->Heating[0];    

   // Fireplace
   *(pOutbuf+OFF_FIRE_PL) = pChar->Fireplace[0];

   // Pool/Spa
   *(pOutbuf+OFF_POOL) = pChar->Pool[0];

   // View
   *(pOutbuf+OFF_VIEW) = pChar->View[0];

   // Elevator
   *(pOutbuf+OFF_ELEVATOR) = pChar->HasElevator;

   // Quality class
   *(pOutbuf+OFF_BLDG_CLASS) = pChar->BldgClass;
   *(pOutbuf+OFF_BLDG_QUAL)  = pChar->BldgQual;

   // Impr condition
   *(pOutbuf+OFF_IMPR_COND)  = pChar->ImprCond[0];

   // Water/Sewer
   *(pOutbuf+OFF_SEWER) = pChar->HasSewer;
   *(pOutbuf+OFF_WATER) = pChar->HasWater;

   // Floor 1, 2, 3
   lTmp = atoin(pChar->Sqft_1stFl, SIZ_CHAR_SQFT);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*u", SIZ_FLOOR_SF, lTmp);
      memcpy(pOutbuf+OFF_FLOOR1_SF, acTmp, SIZ_FLOOR_SF);
   }
   lTmp = atoin(pChar->Sqft_2ndFl, SIZ_CHAR_SQFT);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*u", SIZ_FLOOR_SF, lTmp);
      memcpy(pOutbuf+OFF_FLOOR2_SF, acTmp, SIZ_FLOOR_SF);
   }
   lTmp = atoin(pChar->Sqft_Above2nd, SIZ_CHAR_SQFT);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*u", SIZ_FLOOR_SF, lTmp);
      memcpy(pOutbuf+OFF_FLOOR3_SF, acTmp, SIZ_FLOOR_SF);
   }

   // Basement
   lTmp = atoin(pChar->BsmtSqft, SIZ_CHAR_SQFT);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*u", SIZ_BSMT_SF, lTmp);
      memcpy(pOutbuf+OFF_BSMT_SF, acTmp, SIZ_BSMT_SF);
   }

   // Misc Impr
   lTmp = atoin(pChar->MiscSqft, SIZ_CHAR_SQFT);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*u", SIZ_MISCIMPR_SF, lTmp);
      memcpy(pOutbuf+OFF_MISCIMPR_SF, acTmp, SIZ_MISCIMPR_SF);
   }

   lCharMatch++;

   // Get next Char rec
   pRec = fgets(acRec, 1024, fdChar);

   return 0;
}

/******************************** Scl_MergeLegal *****************************
 *
 *
 *****************************************************************************/

int Scl_MergeLegal(char *pOutbuf)
{
   static char acRec[256], *pRec=NULL;
   int         iLoop;

   // Get first rec for first call
   if (!pRec)
      pRec = fgets(acRec, 256, fdLegal);

   do
   {
      // Compare Apn
      iLoop = memcmp(pOutbuf, acRec, iApnLen);
      if (iLoop > 0)
      {
         if (bDebug)
            LogMsg0("Skip Legal rec  %.*s", iApnLen, pRec);

         pRec = fgets(acRec, 256, fdLegal);
         if (!pRec)
         {
            fclose(fdLegal);
            fdLegal = NULL;
            return 1;      // EOF
         }
         lLegalSkip++;
      }
   } while (iLoop > 0);

   // If not match, return
   if (iLoop)
      return 1;


#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "0119009000", 10) )
   //   lTmp = 0;
#endif

   updateLegal(pOutbuf, pRec+16);
   //memcpy(pOutbuf+OFF_LEGAL, pRec+16, strlen(pRec)-17);
   lLegalMatch++;

   // Get next Char rec
   pRec = fgets(acRec, 256, fdLegal);

   return 0;
}

// Not currently used
//int Scl_MergeLegalCsv(char *pOutbuf)
//{
//   static char acRec[256], *pRec=NULL;
//   int         iLoop;
//
//   // Get first rec for first call
//   if (!pRec)
//      pRec = fgets(acRec, 256, fdLegal);
//
//   do
//   {
//      // Compare Apn
//      iLoop = memcmp(pOutbuf, acRec, iApnLen);
//      if (iLoop > 0)
//      {
//         if (bDebug)
//            LogMsg0("Skip Legal rec  %.*s", iApnLen, pRec);
//
//         pRec = fgets(acRec, 256, fdLegal);
//         if (!pRec)
//         {
//            fclose(fdLegal);
//            fdLegal = NULL;
//            return 1;      // EOF
//         }
//         lLegalSkip++;
//      }
//   } while (iLoop > 0);
//
//   // If not match, return
//   if (iLoop)
//      return 1;
//
//
//#ifdef _DEBUG
//   //if (!memcmp(pOutbuf, "0119009000", 10) )
//   //   lTmp = 0;
//#endif
//
//   updateLegal(pOutbuf, pRec+16);
//   //memcpy(pOutbuf+OFF_LEGAL, pRec+16, strlen(pRec)-17);
//   lLegalMatch++;
//
//   // Get next Char rec
//   pRec = fgets(acRec, 256, fdLegal);
//
//   return 0;
//}
   
/***************************** Scl_MergeLegalFile *****************************
 *
 *
 ******************************************************************************/

int Scl_MergeLegalFile(int iSkip)
{
   char     acLglFile[_MAX_PATH], acRawFile[_MAX_PATH], acOutFile[_MAX_PATH], acBuf[MAX_RECSIZE];

   HANDLE   fhIn, fhOut;
   DWORD    nBytesRead;
   DWORD    nBytesWritten;
   BOOL     bRet;
   long     lRet=0, lCnt=0;

   sprintf(acRawFile, acRawTmpl, "SCL", "SCL", "S01");
   sprintf(acOutFile, acRawTmpl, "SCL", "SCL", "R01");

   // Rename files for processing
   if (_access(acRawFile, 0))
   {
      if (!_access(acOutFile, 0))
         rename(acOutFile, acRawFile);
      else
      {
         LogMsg("Missing input file %s.  Please recheck!", acRawFile);
         return 1;
      }
   }

   // Open Input file
   LogMsg("Open input file %s", acRawFile);
   fhIn = CreateFile(acRawFile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhIn == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening raw file: %s\n", acRawFile);
      return 3;
   }

   // Open Legal file
   GetIniString("Data", "LegalFile", "", acBuf, _MAX_PATH, acIniFile);
   sprintf(acLglFile, acBuf, myCounty.acCntyCode, myCounty.acCntyCode, "Dat");
   fdLegal = fopen(acLglFile, "r");
   if (fdLegal == NULL)
   {
      LogMsg("***** Error opening Legal file: %s\n", acLglFile);
      return -2;
   }

   // Open Output file
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error creating output file: %s\n", acOutFile);
      return 4;
   }

   // Copy skip record
   while (iSkip-- > 0)
   {
      ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);
      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
   }

   // Merge loop
   while (bRet)
   {
      bRet = ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);

      // Check for EOF
      if (!bRet)
      {
         LogMsg("Error reading input file %s (%f)", acRawFile, GetLastError());
         break;
      }

      // EOF ?
      if (!nBytesRead)
         break;

      // Merge Char
      if (fdLegal)
         lRet = Scl_MergeLegal(acBuf);

      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);

      if (!bRet)
      {
         LogMsg("Error occurs: %d\n", GetLastError());
         lRet = WRITE_ERR;
         break;
      }
   }

   // Close files
   if (fdLegal)
      fclose(fdLegal);
   if (fhOut)
      CloseHandle(fhOut);
   if (fhIn)
      CloseHandle(fhIn);

   LogMsg("Total output records:       %u", lCnt);
   LogMsg("Total Legal matched:        %u", lLegalMatch);
   LogMsg("Total Legal skipped:        %u\n", lLegalSkip);

   printf("\nTotal output records: %u", lCnt);

   lRecCnt = lCnt;
   return 0;
}


/***************************** Scl_ExtrLegal **********************************
 *
 * Load legal file LD852PC-2021-07-06.txt and create legal file. If legal is longer than 40 bytes,
 * abbr BOOK to BK, PAGE to PG, ...
 * Notes: In the Assessor's master parcel number file the recorded BOOK number and 
 *        recorded PAGE number are not included if a TRACT number and LOT number are available.
 * - Output string: "TR <track> BLK <block> LOT <lot> BK <book> PG <page>"
 *                  "TR <track> BLK <block> LOT <lot> UNIT <unit>
 *                  "UNIT <unit> BOOK <book> PAGE <page>"
 *
 ******************************************************************************/

void Scl_FormatLegal(char *pInbuf, char *pOutbuf)
{
   char  sLegal[255], sTR[16], sBLK[16], sLOT[16], sUNIT[16], sBOOK[16], sPAGE[16];
   SCL_LEGAL *pLegal = (SCL_LEGAL *)pInbuf;

   sTR[0]=sBLK[0]=sLOT[0]=sUNIT[0]=sBOOK[0]=sPAGE[0]=0;
   if (pLegal->Tract[0] > ' ')
      sprintf(sTR, "TR %.6s", pLegal->Tract);
   if (pLegal->Block[0] > ' ')
      sprintf(sBLK, "BLK %.4s", pLegal->Block);
   if (pLegal->Lot[0] > ' ')
      sprintf(sLOT, "LOT %.4s", pLegal->Lot);
   if (pLegal->Unit[0] > ' ')
      sprintf(sUNIT, "UNIT %.4s", pLegal->Unit);
   if (pLegal->Book[0] > ' ')
      sprintf(sBOOK, "BOOK %.4s", pLegal->Book);
   if (pLegal->Page[0] > ' ')
      sprintf(sPAGE, "PAGE %.4s", pLegal->Page);

   sprintf(sLegal, "%s %s %s %s %s %s", sTR, sBLK, sLOT, sUNIT, sBOOK, sPAGE);
   blankRem(sLegal);
   sprintf(pOutbuf, "%.10s      %s\n", pLegal->Apn, sLegal);
}

int Scl_ExtrLegal()
{
   char     acTmp[_MAX_PATH], acOutFile[_MAX_PATH], acInbuf[1024], acOutbuf[1024], *pTmp;
   long     iRet, lCnt=0;
   FILE     *fdIn, *fdOut;

   LogMsg0("Loading legal file");

   // Open legal file
   GetIniString(myCounty.acCntyCode, "LegalFile", "", acTmp, _MAX_PATH, acIniFile);
   LogMsg("Open legal file: %s", acTmp);
   fdIn = fopen(acTmp, "r");
   if (fdIn == NULL)
   {
      LogMsg("***** Error opening input file: %s (errno=%d)\n", acTmp, _errno);
      return -2;
   }

   // Open Output file
   GetIniString("Data", "LegalFile", "", acTmp, _MAX_PATH, acIniFile);
   sprintf(acOutFile, acTmp, myCounty.acCntyCode, myCounty.acCntyCode, "Dat");
   LogMsg("Open output file %s", acOutFile);
   fdOut = fopen(acOutFile, "w");
   if (fdIn == NULL)
   {
      LogMsg("***** Error opening output file: %s (errno=%d)\n", acOutFile, _errno);
      return -2;
   }

   // Merge loop
   while (!feof(fdIn))
   {
      pTmp = fgets(acInbuf, 1024, fdIn);
      // Check for EOF
      if (!pTmp)
         break;

      // Format legal string
      Scl_FormatLegal(acInbuf, acOutbuf);
      iRet = fputs(acOutbuf, fdOut);
      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdIn)
      fclose(fdIn);
   if (fdOut)
      fclose(fdOut);

   LogMsgD("Total output records:       %u", lCnt);
   return 0;
}

/***************************** Scl_ExtrLegalCsv *******************************
 *
 * Load legal file LE852CSV-2022-07-05.txt and create legal file. If legal is longer than 40 bytes,
 * abbr BOOK to BK, PAGE to PG, ...
 * Notes: In the Assessor's master parcel number file the recorded BOOK number and 
 *        recorded PAGE number are not included if a TRACT number and LOT number are available.
 * - Output string: "TR <track> BLK <block> LOT <lot> BK <book> PG <page>"
 *                  "TR <track> BLK <block> LOT <lot> UNIT <unit>
 *                  "UNIT <unit> BOOK <book> PAGE <page>"
 *
 ******************************************************************************/

void Scl_FormatLegalCsv(char *pInbuf, char *pOutbuf)
{
   char  sLegal[255], sTR[16], sBLK[16], sLOT[16], sUNIT[16], sBOOK[16], sPAGE[16];

   sTR[0]=sBLK[0]=sLOT[0]=sUNIT[0]=sBOOK[0]=sPAGE[0]=0;
   if (*apTokens[SCL_LGL_TRACT] > ' ')
      sprintf(sTR, "TR %s", apTokens[SCL_LGL_TRACT]);
   if (*apTokens[SCL_LGL_BLOCK] > ' ')
      sprintf(sBLK, "BLK %s", apTokens[SCL_LGL_BLOCK]);
   if (*apTokens[SCL_LGL_LOT] > ' ')
      sprintf(sLOT, "LOT %s", apTokens[SCL_LGL_LOT]);
   if (*apTokens[SCL_LGL_UNIT] > ' ')
      sprintf(sUNIT, "UNIT %s", apTokens[SCL_LGL_UNIT]);
   if (*apTokens[SCL_LGL_BOOK] > ' ')
      sprintf(sBOOK, "BOOK %s", apTokens[SCL_LGL_BOOK]);
   if (*apTokens[SCL_LGL_PAGE] > ' ')
      sprintf(sPAGE, "PAGE %s", apTokens[SCL_LGL_PAGE]);

   sprintf(sLegal, "%s %s %s %s %s %s", sTR, sBLK, sLOT, sUNIT, sBOOK, sPAGE);
   blankRem(sLegal);
   sprintf(pOutbuf, "%s        %s\n", apTokens[SCL_LGL_APN], sLegal);
}

int Scl_ExtrLegalCsv()
{
   char     acTmp[_MAX_PATH], acOutFile[_MAX_PATH], acInbuf[1024], acOutbuf[1024], *pTmp;
   long     iRet, lCnt=0;
   FILE     *fdIn, *fdOut;

   LogMsg0("Loading legal file");

   // Open legal file
   GetIniString(myCounty.acCntyCode, "LegalFile", "", acTmp, _MAX_PATH, acIniFile);
   LogMsg("Open legal file: %s", acTmp);
   fdIn = fopen(acTmp, "r");
   if (fdIn == NULL)
   {
      LogMsg("***** Error opening input file: %s (errno=%d)\n", acTmp, _errno);
      return -2;
   }

   // Open Output file
   GetIniString("Data", "LegalFile", "", acTmp, _MAX_PATH, acIniFile);
   sprintf(acOutFile, acTmp, myCounty.acCntyCode, myCounty.acCntyCode, "Dat");
   LogMsg("Open output file %s", acOutFile);
   fdOut = fopen(acOutFile, "w");
   if (fdIn == NULL)
   {
      LogMsg("***** Error opening output file: %s (errno=%d)\n", acOutFile, _errno);
      return -2;
   }

   // Skip header
   pTmp = fgets(acInbuf, 1024, fdIn);

   // Merge loop
   while (!feof(fdIn))
   {
      pTmp = fgets(acInbuf, 1024, fdIn);
      // Check for EOF
      if (!pTmp)
         break;

      iTokens = ParseStringNQ(acInbuf, cDelim, SCL_LGL_PAGE+2, apTokens);
      if (iTokens < SCL_LGL_FLDS)
      {
         LogMsg("*** Bad CHAR record (%d): %s", iTokens, acInbuf);
         continue;
      }

      // Format legal string
      Scl_FormatLegalCsv(acInbuf, acOutbuf);
      iRet = fputs(acOutbuf, fdOut);
      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdIn)
      fclose(fdIn);
   if (fdOut)
      fclose(fdOut);

   LogMsgD("Total output records:       %u", lCnt);
   return 0;
}

/******************************** Scl_GrGrMergeOwner *****************************
 *
 * Return 0 if successful, >0 if error
 *
 *****************************************************************************/

void Scl_GrGrMergeOwner(char *pOutbuf, char *pName1, char *pName2)
{
   int   iTmp;
   char  acSave1[64], acSave2[64];
   char  acOwner1[64], acOwner2[64];
   bool  bNoMerge=false, bKeep=false, bSameLast=false;

   OWNER myOwner1, myOwner2;

   // Clear output buffer if needed
   removeNames(pOutbuf, true);

   acSave1[0]=acSave2[0] = 0;

   // Remove multiple spaces
   memcpy(acOwner1, pName1, RSIZ_SCL_NAME1);
   blankRem(acOwner1, RSIZ_SCL_NAME1);

   memcpy(acOwner2, pName2, RSIZ_SCL_NAME2);
   blankRem(acOwner2, RSIZ_SCL_NAME2);

   // Update vesting - If no vesting in name1, try name2
   iTmp = updateVesting(myCounty.acCntyCode, acOwner1, pOutbuf+OFF_VEST, pOutbuf+OFF_ETAL_FLG);
   if (!iTmp && acOwner2[0] > ' ')
      iTmp = updateVesting(myCounty.acCntyCode, acOwner2, pOutbuf+OFF_VEST, pOutbuf+OFF_ETAL_FLG);

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "00305004", 8))
   //   iTmp = 0;
#endif
   // If name is a trust, keep them as is
   if (!strchr(acOwner1, ',') || strstr(acOwner1, " TRUST ") || strstr(acOwner1, " TR ") ||
       strstr(acOwner1, "/TR ") )
   {
      bNoMerge = true;
      replChar(acOwner1, ',', ' ');
      blankRem(acOwner1);
   }

   if (acOwner2[0] > ' ')
   {
      if (!strchr(acOwner2, ',') || strstr(acOwner2, " TRUST ") || strstr(acOwner2, " TR ") ||
          strstr(acOwner2, "/TR ") )
      {
         bNoMerge = true;
         replChar(acOwner2, ',', ' ');
         blankRem(acOwner2);
      }
   }

   if (!bNoMerge)
   {
      replChar(acOwner1, ',', ' ');
      blankRem(acOwner1);
      iTmp = splitOwner(acOwner1, &myOwner1, 2);
      // Check for mergable names
      if (acOwner2[0] > ' ')
      {
         replChar(acOwner2, ',', ' ');
         blankRem(acOwner2);
         iTmp = splitOwner(acOwner2, &myOwner2, 2);
         if (!strcmp(myOwner1.acOL, myOwner2.acOL))
         {
            strcat(acOwner1, " & ");
            strcat(acOwner1, myOwner2.acOF);
            strcat(acOwner1, " ");
            strcat(acOwner1, myOwner2.acOM);
            acOwner2[0] = 0;
         }
      } else
         acOwner1[SIZ_NAME1] = 0;
   }

   memcpy(pOutbuf+OFF_NAME1, acOwner1, strlen(acOwner1));
   if (acOwner2[0] > ' ')
   {
      acOwner2[SIZ_NAME2] = 0;
      memcpy(pOutbuf+OFF_NAME2, acOwner2, strlen(acOwner2));
   }

   // to be done - create swap name
   if (!bNoMerge)
   {
      iTmp = splitOwner(acOwner1, &myOwner1, 2);
      if (iTmp == -1)
         strcpy(myOwner1.acSwapName, acOwner1);

      // Swapped name
      if (myOwner1.acSwapName[0] > ' ')
      {
         iTmp = strlen(myOwner1.acSwapName);
         if (iTmp > SIZ_NAME_SWAP)
            iTmp = SIZ_NAME_SWAP;
         memcpy(pOutbuf+OFF_NAME_SWAP, myOwner1.acSwapName, iTmp);
      }
   } else
      memcpy(pOutbuf+OFF_NAME_SWAP, acOwner1, strlen(acOwner1));
}

/******************************** Scl_FormatOwner ****************************
 *
 * Formating owner name
 *
 *****************************************************************************/

void Scl_FormatOwner(OWNER *pOwner, char *pName1, char *pName2)
{
   int   iTmp;
   char  acSave1[64], acSave2[64];
   char  acOwner1[64], acOwner2[64], *pTmp;
   bool  bNoMerge=false, bKeep=false, bSameLast=false;

   OWNER myOwner1, myOwner2;

   // Clear output buffer if needed
   memset(pOwner, ' ', sizeof(OWNER));
   acSave1[0]=acSave2[0] = 0;

   // Remove multiple spaces
   strcpy(acOwner1, pName1);
   strcpy(acOwner2, pName2);

   pTmp = findVesting(acOwner1, pOwner->acVest);
   if (pTmp)
      bNoMerge = true;
   else if (*pName2 > ' ' && (pTmp = findVesting(acOwner2, pOwner->acVest)))
      bNoMerge = true;

   // Drop /TRT (trustee)
   if (pTmp = strstr(acOwner1, "TRT "))
   {
      pTmp--;
      if (*pTmp == '/' || *pTmp == -90)
      {
         *pTmp++ = ' ';
         *pTmp = 0;
      }
   }
   if (pTmp = strstr(acOwner2, "/TRT ")) 
   {
      *pTmp++ = ' ';
      *pTmp = 0;
   }

   if (!bNoMerge)
   {
      replChar(acOwner1, ',', ' ');
      blankRem(acOwner1);
      iTmp = splitOwner(acOwner1, pOwner, 2);
      // Check for mergable names
      if (acOwner2[0] > ' ')
      {
         replChar(acOwner2, ',', ' ');
         blankRem(acOwner2);
         iTmp = splitOwner(acOwner2, &myOwner2, 2);
         if (!strcmp(pOwner->acOL, myOwner2.acOL))
         {
            strcat(acOwner1, " & ");
            strcat(acOwner1, myOwner2.acOF);
            strcat(acOwner1, " ");
            strcat(acOwner1, myOwner2.acOM);

            strcpy(pOwner->acName1, acOwner1);
            acOwner2[0] = 0;
         }
      } 
   } else
      strcpy(pOwner->acName1, acOwner1);

   if (acOwner2[0] > ' ')
   {
      acOwner2[SIZ_NAME2] = 0;
      strcpy(pOwner->acName2, acOwner2);
   } else
      pOwner->acName2[0] = 0;

   // to be done - create swap name
   if (!bNoMerge)
   {
      iTmp = splitOwner(acOwner1, &myOwner1, 2);
      if (iTmp == -1)
         strcpy(myOwner1.acSwapName, acOwner1);

      // Swapped name
      if (myOwner1.acSwapName[0] > ' ')
      {
         iTmp = strlen(myOwner1.acSwapName);
         if (iTmp > SIZ_NAME_SWAP)
            iTmp = SIZ_NAME_SWAP;
         strncpy(pOwner->acSwapName, myOwner1.acSwapName, iTmp);
      } 
   } else
      strcpy(pOwner->acSwapName, acOwner1);
}

/****************************** Scl_MergeGrGrRec *****************************
 *
 * GRGR record sometimes calls QUIT CLAIM as a DEED.  We have to check if a DEED
 * without sale price, don't consider it a sale. SN - 20180416
 *
 *****************************************************************************/

int Scl_MergeGrGrRec(char *pOutbuf, char *pSaleRec, bool bAllGrgr, bool bUpdOwner)
{
   static char curOwner[64];

   int       iTmp, iDeedType, iRet=0;
   long      lCurSaleDt, lLstSaleDt, lPrice, lTmp;
   char      *pSaleDate;
   char      *pTmp, acTmp[64], acName1[64], acName2[64], *pSfx;
   SCSAL_REC *pRec;
   OWNER     myOwner;

   pRec = (SCSAL_REC *)pSaleRec;
   pSaleDate = pRec->DocDate;

   lPrice = atoin(pRec->SalePrice, SALE_SIZ_SALEPRICE);
   lCurSaleDt = atoin(pRec->DocDate, SALE_SIZ_DOCDATE);

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "00301009", 8))
   //   iTmp = 0;
#endif

   // Update transfers 
   lLstSaleDt = atoin(pOutbuf+OFF_TRANSFER_DT, SIZ_SALE1_DT);
   if (lCurSaleDt >= lLstSaleDt)
   {
      if (lCurSaleDt == lLstSaleDt)
      {
         if (memcmp(pOutbuf+OFF_TRANSFER_DOC, pRec->DocNum, SIZ_TRANSFER_DOC) < 0)
            memcpy(pOutbuf+OFF_TRANSFER_DOC, pRec->DocNum, SIZ_TRANSFER_DOC);
      } else
      {
         memcpy(pOutbuf+OFF_TRANSFER_DOC, pRec->DocNum, SIZ_TRANSFER_DOC);
         memcpy(pOutbuf+OFF_TRANSFER_DT, pRec->DocDate, SIZ_TRANSFER_DT);
      }
   }

   // Skip none sale transaction
   if (pRec->NoneSale_Flg == 'Y' || !lPrice)
      return -1;

   iDeedType = atoin(pRec->DocType, 3);
   // Ignore doc that is not deed, grant deed and has no sale price
   if (!bAllGrgr && !lPrice && !(iDeedType == 13 || iDeedType == 1))
      return -1;

   // If current sale is older than last sale, return
   lLstSaleDt = atoin(pOutbuf+OFF_SALE1_DT, SIZ_SALE1_DT);
   if (lCurSaleDt < lLstSaleDt)
      return -1;
   else if (lCurSaleDt == lLstSaleDt)
   {
      iTmp = memcmp(pRec->DocNum, pOutbuf+OFF_SALE1_DOC, SIZ_SALE1_DOC);
      if (iTmp < 0)
         return -1;
      else if (iTmp > 0 && lPrice > 0)
      {  
         // Update Sale1 - Multi transfer in same day
         memcpy(pOutbuf+OFF_SALE1_DOC, pRec->DocNum, sizeof(pRec->DocNum));
         memcpy(pOutbuf+OFF_SALE1_DOCTYPE, pRec->DocType, sizeof(pRec->DocType));
         if (pRec->Seller1[0] > ' ')
            memcpy(pOutbuf+OFF_SELLER, pRec->Seller1, SIZ_SELLER);
         memcpy(pOutbuf+OFF_SALE1_AMT, pRec->SalePrice, SIZ_SALE1_AMT);
      }
      return 1;
   }

   // Update Owner
   if (bUpdOwner && pRec->Name1[0] > ' ' && lCurSaleDt > lLienDate)
   {
      // If name2 exists, merge name1 and name2 if they have the same last name
      // Otherwise just remove comma and copy to Name1 and Name2 as appropriate.

      // Remove comma
      memcpy(acName1, pRec->Name1, SALE_SIZ_NAME);
      replChar(acName1, ',', ' ', SALE_SIZ_NAME);
      blankRem(acName1, SALE_SIZ_NAME);

      memcpy(acName2, pRec->Name2, SALE_SIZ_NAME);
      replChar(acName2, ',', ' ', SALE_SIZ_NAME);
      blankRem(acName2, SALE_SIZ_NAME);

      //Scl_GrGrMergeOwner(pOutbuf, acName1, acName2);
      Scl_FormatOwner(&myOwner, acName1, acName2);

      // If new owner, update mail addr
      iTmp = strlen(myOwner.acName1);
      if (memcmp(myOwner.acName1, pOutbuf+OFF_NAME1, iTmp))
      {
         // Clear output buffer
         removeNames(pOutbuf, true);
         
         // Update vesting 
         if (myOwner.acVest[0] >= 'A')
         {
            memcpy(pOutbuf+OFF_VEST, myOwner.acVest, strlen(myOwner.acVest));
            if (!memcmp(myOwner.acVest, "EA", 2))
               *(pOutbuf+OFF_ETAL_FLG) = 'Y';
         }

         // Update owner name
         memcpy(pOutbuf+OFF_NAME1, myOwner.acName1, strlen(myOwner.acName1));
         if (myOwner.acName2[0] > ' ')
            memcpy(pOutbuf+OFF_NAME2, myOwner.acName2, strlen(myOwner.acName2));
         memcpy(pOutbuf+OFF_NAME_SWAP, myOwner.acSwapName, strlen(myOwner.acSwapName));

         // Update mailing
         if (memcmp(pOutbuf+OFF_M_ADDR_D, pOutbuf+OFF_S_ADDR_D, SIZ_M_ADDR_D))
         {
            // Clear old Mailing
            removeMailing(pOutbuf, false);

            // Replace mail addr with situs addr
            lTmp = atoin(pOutbuf+OFF_S_STRNUM, SIZ_S_STRNUM);
            if (lTmp > 0)
            {
               iTmp = sprintf(acTmp, "%d ", lTmp);
               memcpy(pOutbuf+OFF_M_STRNUM, acTmp, iTmp);
            }

            memcpy(pOutbuf+OFF_M_STR_SUB, pOutbuf+OFF_S_STR_SUB, SIZ_M_STR_SUB);
            memcpy(pOutbuf+OFF_M_DIR, pOutbuf+OFF_S_DIR, SIZ_M_DIR);
            memcpy(pOutbuf+OFF_M_STREET, pOutbuf+OFF_S_STREET, SIZ_M_STREET);

            iTmp = atoin(pOutbuf+OFF_S_SUFF, SIZ_S_SUFF);
            pSfx = GetSfxStr(iTmp);
            if (pSfx)
               memcpy(pOutbuf+OFF_M_SUFF, pSfx, strlen(pSfx));

            memcpy(pOutbuf+OFF_M_UNITNO, pOutbuf+OFF_S_UNITNO, SIZ_S_UNITNO);
            memcpy(pOutbuf+OFF_M_ADDR_D, pOutbuf+OFF_S_ADDR_D, SIZ_M_ADDRB_D);

            // City state
            iTmp = atoin(pOutbuf+OFF_S_CITY, SIZ_S_CITY);
            pTmp = GetCityName(iTmp);
            if (pTmp)
               memcpy(pOutbuf+OFF_M_CITY, pTmp, strlen(pTmp));

            memcpy(pOutbuf+OFF_M_ST, pOutbuf+OFF_S_ST, SIZ_M_ST);

            if (*(pOutbuf+OFF_S_ZIP) == '9')
            {
               memcpy(pOutbuf+OFF_M_ZIP, pOutbuf+OFF_S_ZIP, SIZ_M_ZIP);
               memcpy(pOutbuf+OFF_M_ZIP4, pOutbuf+OFF_S_ZIP4, SIZ_M_ZIP4);
            } else if (*(pOutbuf+OFF_S_ZIP) != ' ')
               LogMsg("*** Bad situs zipcode: %.5s on APN=%.*s", pOutbuf+OFF_S_ZIP, iApnLen, pOutbuf);

            memcpy(pOutbuf+OFF_M_CTY_ST_D, pOutbuf+OFF_S_CTY_ST_D, SIZ_M_CTY_ST_D);
         }
      }
   }

   // Move sale2 to sale3
   memcpy(pOutbuf+OFF_SALE3_DOC, pOutbuf+OFF_SALE2_DOC, SIZ_SALE1_DOC);
   memcpy(pOutbuf+OFF_SALE3_DT, pOutbuf+OFF_SALE2_DT, SIZ_SALE1_DT);
   memcpy(pOutbuf+OFF_SALE3_DOCTYPE, pOutbuf+OFF_SALE2_DOCTYPE, SIZ_SALE1_DOCTYPE);
   memcpy(pOutbuf+OFF_SALE3_AMT, pOutbuf+OFF_SALE2_AMT, SIZ_SALE1_AMT);
   *(pOutbuf+OFF_AR_CODE3) = *(pOutbuf+OFF_AR_CODE2);

   // Move sale1 to sale2
   memcpy(pOutbuf+OFF_SALE2_DOC, pOutbuf+OFF_SALE1_DOC, SIZ_SALE1_DOC);
   memcpy(pOutbuf+OFF_SALE2_DT, pOutbuf+OFF_SALE1_DT, SIZ_SALE1_DT);
   memcpy(pOutbuf+OFF_SALE2_DOCTYPE, pOutbuf+OFF_SALE1_DOCTYPE, SIZ_SALE1_DOCTYPE);
   memcpy(pOutbuf+OFF_SALE2_AMT, pOutbuf+OFF_SALE1_AMT, SIZ_SALE1_AMT);
   *(pOutbuf+OFF_AR_CODE2) = *(pOutbuf+OFF_AR_CODE1);

   // Clear sale1
   ClearOneSale(pOutbuf, 1);

   // Seller
   memcpy(pOutbuf+OFF_SELLER, pRec->Seller1, SIZ_SELLER);

   // Update DocNum
   if (pRec->DocNum[0] > ' ')
      memcpy(pOutbuf+OFF_SALE1_DOC, pRec->DocNum, SIZ_SALE1_DOC);

   // Update current sale
   memcpy(pOutbuf+OFF_SALE1_DT, pRec->DocDate, SIZ_SALE1_DT);
   if (lPrice > 0)
      memcpy(pOutbuf+OFF_SALE1_AMT, pRec->SalePrice, SIZ_SALE1_AMT);

   if (isdigit(pRec->DocType[0]))
      memcpy(pOutbuf+OFF_SALE1_DOCTYPE, pRec->DocType, sizeof(pRec->DocType));
   else if (findDocType(pRec->DocType, acTmp))
      memcpy(pOutbuf+OFF_SALE1_DOCTYPE, acTmp, strlen(acTmp));

   *(pOutbuf+OFF_AR_CODE1) = 'R';

   return 1;
}

// Old version used with RP format
int Scl_ExtrSaleMatched_Old(char *pGrGrFile, char *pOutFile=NULL)
{
   char      acTmp[256], acTmpFile[256], acBuf[2048], acSaleRec[1024];
   long      lCnt=0, lTax, iDocType;
   char      *pTmp;

   CString   sTmp, sApn, sType;
   SCSAL_REC *pSaleRec = (SCSAL_REC *)&acSaleRec;
   GRGR_DEF  *pGrGr = (GRGR_DEF *)&acBuf[0];

   // Open output file
   if (pOutFile)
      strcpy(acTmpFile, pOutFile);
   else
      sprintf(acTmpFile, acEGrGrTmpl, myCounty.acCntyCode, "DAT");

   LogMsg("Extract matched grgr from %s to %s", pGrGrFile, acTmpFile);

   // Create output file
   if (!(fdSale = fopen(acTmpFile, "w")))
   {
      LogMsg("***** Error creating %s file", acTmpFile);
      return -2;
   }

   // Open input file
   if (!(fdGrGr = fopen(pGrGrFile, "r")))
   {
      LogMsg("***** Error creating %s file", pGrGrFile);
      return -1;
   }

   // Loop through
   while (!feof(fdGrGr))
   {
      // Get input rec
      pTmp = fgets(acBuf, 2048, fdGrGr);
      if (!pTmp)
         break;         // EOF

      // Drop all record without APN matched
      if (pGrGr->APN_Matched == 'Y')
      {
         lTax = atoin(pGrGr->Tax, SIZ_GR_TAX);

         if (!memcmp(pGrGr->DocTitle, "DEED   ", 7))
            iDocType = 13;
         else if (!memcmp(pGrGr->DocTitle, "QCDD", 4))
            iDocType = 4; 
         else if (!memcmp(pGrGr->DocTitle, "TRUSTEES D", 10))
            iDocType = 27;
         else 
         {
            if (lTax > 550)      // SALEPRICE > 5000
            {
               if (!memcmp(pGrGr->DocTitle, "ASSIGNMENT  ", 12))
                  iDocType = 7;
               else if (!memcmp(pGrGr->DocTitle, "CORRECTION DEED", 15))
                  iDocType = 9; 
               else if (!memcmp(pGrGr->DocTitle, "DEED OF TRUST/MORTGAGE/SECURIT", 30))
                  iDocType = 13; 
               else if (!memcmp(pGrGr->DocTitle, "NOTICE TRUSTEE SALE", 19))
                  iDocType = 27; 
               else if (!memcmp(pGrGr->DocTitle, "SHERIFFS DEED EXECUTION", 23))
                  iDocType = 25; 
            } else
            {
               if (bDebug)
                  LogMsg0("??? Skip Doc type: %.30s APN=%.10s DocNum=%.10s Price=%.10s", pGrGr->DocTitle, pGrGr->APN, pGrGr->DocNum, pGrGr->SalePrice); 
               iDocType = 0;
            }
         }

         if (iDocType > 0)
         {
            memset(acSaleRec, 32, sizeof(SCSAL_REC));

            memcpy(pSaleRec->Apn, pGrGr->APN, SALE_SIZ_APN);
            memcpy(pSaleRec->DocDate, pGrGr->DocDate, SALE_SIZ_DOCDATE);
            memcpy(pSaleRec->DocNum, pGrGr->DocNum, SALE_SIZ_DOCNUM);

            // Translate Doctype to index code
            sprintf(acTmp, "%d  ", iDocType);
            memcpy(pSaleRec->DocType, acTmp, 3);

            if (lTax > 0)
            {
               memcpy(pSaleRec->StampAmt, pGrGr->Tax, SIZ_GR_TAX);
               memcpy(pSaleRec->SalePrice, pGrGr->SalePrice, SIZ_GR_SALE);
            }

            // Get grantors
            memcpy(pSaleRec->Seller1, pGrGr->Grantors[0].Name, SALE_SIZ_SELLER);
            memcpy(pSaleRec->Seller2, pGrGr->Grantors[1].Name, SALE_SIZ_SELLER);

            // Get grantees
            memcpy(pSaleRec->Name1, pGrGr->Grantees[0].Name, SALE_SIZ_NAME);
            memcpy(pSaleRec->Name2, pGrGr->Grantees[1].Name, SALE_SIZ_NAME);

            pSaleRec->CRLF[0] = '\n';
            pSaleRec->CRLF[1] = 0;
            fputs(acSaleRec, fdSale);

            if (!(++lCnt % 1000))
               printf("\r%u", lCnt);
         }
      }
   }  // EOLoop

   fclose(fdGrGr);
   fclose(fdSale);

   // Append to cumulative file if no output file specified
   if (!pOutFile)
   {
      char acESalFile[256], acGrGrSrt[256];

      sprintf(acESalFile, acEGrGrTmpl, myCounty.acCntyCode, "SLS");
      //LogMsg("Append to %s", acTmp);
      //if (appendTxtFile(acTmpFile, acTmp) < 0)
      //   LogMsg("*** Error appending %s to cum sale file %s", acTmpFile, acTmp);

      sprintf(acBuf, "S(1,%d,C,A,27,8,C,A,15,12,C,A) DUPO(1,34)", iApnLen);
      sprintf(acTmp, "%s+%s", acTmpFile, acESalFile);
      sprintf(acGrGrSrt, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "srt");
      lCnt = sortFile(acTmp, acGrGrSrt, acBuf);
      if (lCnt > 0)
      {
         sprintf(acTmp, acGrGrTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "sav");
         if (!_access(acTmp, 0))
            DeleteFile(acTmp);

         // Rename sls to sav
         rename(acESalFile, acTmp);
   
         // Rename srt to SLS file
         rename(acGrGrSrt, acESalFile);
      }
   }

   LogMsg("Total number of output matched records: %ld", lCnt);
   return 0;
}

/***************************** Scl_ExtrSaleMatched ***************************
 *
 * Input:  Scl_GrGr.dat or Scl_GrGr.sls
 * Output: GrGr_Exp.dat
 *
 * Return 0 if successful, else error
 *
 *****************************************************************************/

int Scl_ExtrSaleMatched(char *pGrGrFile, char *pOutFile=NULL)
{
   char      acTmp[256], acTmpFile[256], acBuf[2048], acSaleRec[1024];
   long      lCnt=0, lTax, iDocType;
   char      *pTmp;

   CString   sTmp, sApn, sType;
   SCSAL_REC *pSaleRec = (SCSAL_REC *)&acSaleRec;
   GRGR_DEF  *pGrGr = (GRGR_DEF *)&acBuf[0];

   // Open output file
   if (pOutFile)
      strcpy(acTmpFile, pOutFile);
   else
      sprintf(acTmpFile, acEGrGrTmpl, myCounty.acCntyCode, "Dat");         // Grgr_Exp.dat

   LogMsg0("Extract matched grgr from %s to %s", pGrGrFile, acTmpFile);

   // Create output file
   if (!(fdSale = fopen(acTmpFile, "w")))
   {
      LogMsg("***** Error creating %s file", acTmpFile);
      return -2;
   }

   // Open input file
   if (!(fdGrGr = fopen(pGrGrFile, "r")))
   {
      LogMsg("***** Error creating %s file", pGrGrFile);
      return -1;
   }

   // Loop through
   while (!feof(fdGrGr))
   {
      // Get input rec
      pTmp = fgets(acBuf, 2048, fdGrGr);
      if (!pTmp)
         break;         // EOF

      // Only take in certain DocTitle
      lTax = atoin(pGrGr->Tax, SIZ_GR_TAX);

      if (!memcmp(pGrGr->DocTitle, "DEED   ", 7))
         iDocType = 13;
      else if (!memcmp(pGrGr->DocTitle, "NOTICE OF DEF", 13))
         iDocType = 52; 
      else if (!memcmp(pGrGr->DocTitle, "TRUSTEES D", 10) || !memcmp(pGrGr->DocTitle, "NOTICE TRUSTEE SALE", 18))
         iDocType = 27;
      else if (!memcmp(pGrGr->DocTitle, "CERTIFICATE SALE", 16))
         iDocType = 67;       // Tax Deed
      else 
      {
         if (lTax > 550)      // SALEPRICE > 5000
         {
            pSaleRec->NoneSale_Flg = 'Y';
            if (!memcmp(pGrGr->DocTitle, "ASSIGNMENT  ", 12))
               iDocType = 7;
            else if (!memcmp(pGrGr->DocTitle, "CORRECTION DEED", 15))
               iDocType = 9; 
            else if (!memcmp(pGrGr->DocTitle, "DEED OF TRUST", 13))
               iDocType = 65; 
            else if (!memcmp(pGrGr->DocTitle, "SHERIFFS DEED EXECUTION", 23))
               iDocType = 25; 
            else if (!memcmp(pGrGr->DocTitle, "QCDD", 4) || !memcmp(pGrGr->DocTitle, "DEED/QUITCLAIM", 9))
               iDocType = 4; 
            else if (!memcmp(pGrGr->DocTitle, "EASEMENT", 4))
               iDocType = 40; 
            else
               LogMsg("*** Unknown DocTitle %.30s [%.10s] Tax=%.2f", pGrGr->DocTitle, pGrGr->APN, (double)lTax/100.0);
         } else
         {
            if (bDebug)
               LogMsg0("??? Skip Doc type: %.30s APN=%.10s DocNum=%.10s Price=%.10s", pGrGr->DocTitle, pGrGr->APN, pGrGr->DocNum, pGrGr->SalePrice); 
            iDocType = 0;
         }
      }

      if (iDocType > 0)
      {
         memset(acSaleRec, 32, sizeof(SCSAL_REC));

         memcpy(pSaleRec->Apn, pGrGr->APN, SALE_SIZ_APN);
         memcpy(pSaleRec->DocDate, pGrGr->DocDate, SALE_SIZ_DOCDATE);
         memcpy(pSaleRec->DocNum, pGrGr->DocNum, SALE_SIZ_DOCNUM);

         // Translate Doctype to index code
         sprintf(acTmp, "%d  ", iDocType);
         memcpy(pSaleRec->DocType, acTmp, 3);

         if (lTax > 0)
         {
            memcpy(pSaleRec->StampAmt, pGrGr->Tax, SIZ_GR_TAX);
            memcpy(pSaleRec->SalePrice, pGrGr->SalePrice, SIZ_GR_SALE);
         }

         // Get grantors
         memcpy(pSaleRec->Seller1, pGrGr->Grantors[0].Name, SALE_SIZ_SELLER);
         memcpy(pSaleRec->Seller2, pGrGr->Grantors[1].Name, SALE_SIZ_SELLER);

         // Get grantees
         memcpy(pSaleRec->Name1, pGrGr->Grantees[0].Name, SALE_SIZ_NAME);
         memcpy(pSaleRec->Name2, pGrGr->Grantees[1].Name, SALE_SIZ_NAME);

         if (pGrGr->ParcelCount[0] > '1')
            pSaleRec->MultiSale_Flg = 'Y';

         pSaleRec->CRLF[0] = '\n';
         pSaleRec->CRLF[1] = 0;
         fputs(acSaleRec, fdSale);

         if (!(++lCnt % 1000))
            printf("\r%u", lCnt);
      }
   }  // EOLoop

   fclose(fdGrGr);
   fclose(fdSale);

   LogMsg("Total number of output matched records: %ld", lCnt);
   return 0;
}

/********************************* Scl_MergeGrGrFile *****************************
 *
 * Merge GrGr
 *
 *****************************************************************************/

int Scl_MergeGrGrFile(char *pInfile, bool bAllGrgr, bool bUpdOwner)
{
   char     *pTmp, acBuf[MAX_RECSIZE], acSaleRec[1024];
   char     acRawFile[_MAX_PATH], acOutFile[_MAX_PATH], acGrGrFile[_MAX_PATH], acGrGrExp[_MAX_PATH];
   char     *pCnty = myCounty.acCntyCode;

   HANDLE    fhIn, fhOut;
   SCSAL_REC *pSaleRec = (SCSAL_REC *)acSaleRec;

   int      iTmp, iSaleUpd=0;
   DWORD    nBytesRead;
   DWORD    nBytesWritten;
   BOOL     bRet, bEof;
   long     lRet=0, lCnt=0;

   LogMsg0("Apply GrGr data %s to R01 file", pInfile);

   // Check input file
   if (pInfile && !_access(pInfile, 0))
      strcpy(acGrGrExp, pInfile);
   else
   {
      sprintf(acGrGrFile, acGrGrTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "Sls");
      sprintf(acGrGrExp, acEGrGrTmpl, myCounty.acCntyCode, "Dat");
      iTmp = Scl_ExtrSaleMatched(acGrGrFile, acGrGrExp);
      if (iTmp)
         return iTmp;
   }

   sprintf(acOutFile, acRawTmpl, pCnty, pCnty, "R01");
   sprintf(acRawFile, acRawTmpl, pCnty, pCnty, "S01");
   if (!_access(acOutFile, 0))
   {
      if (!MoveFileEx(acOutFile, acRawFile, MOVEFILE_REPLACE_EXISTING))
      {
         LogMsg("***** Error renaming %s to %s [%d]", acOutFile, acRawFile, GetLastError());
         return -1;
      }
   }

   // Open GrGr_Exp.dat
   LogMsg("Open GrGr file %s", acGrGrExp);
   fdSale = fopen(acGrGrExp, "r");
   if (fdSale == NULL)
   {
      LogMsg("***** Error opening sale file: %s\n", acGrGrExp);
      return -2;
   }

   // Get first record
   pTmp = fgets(acSaleRec, 1024, fdSale);
   if (!pTmp || feof(fdSale))
   {
      LogMsg("*** No GrGr data available");
      return 0;
   }

   // Open Input file
   LogMsg("Open raw file %s", acRawFile);
   fhIn = CreateFile(acRawFile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhIn == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening input file: %s\n", acRawFile);
      return -3;
   }

   // Open Output file
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening output file: %s\n", acOutFile);
      return -4;
   }

   // Copy skip record
   ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);
   bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);

   iNoMatch=0;
   bEof = false;

   // Merge loop
   while (!bEof)
   {
      bRet = ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);
      // Check for EOF
      if (!bRet)
      {
         LogMsg("Error reading input file %s (%f)", acRawFile, GetLastError());
         break;
      }

      if (!nBytesRead)
         break;

      if (bClearSales)
         ClearOldSale(acBuf, true);

      // Update sale
GrGr_ReLoad:
#ifdef _DEBUG
      //if (!memcmp(acBuf, "13714171", 8))
      //   iTmp = 0;
#endif
      iTmp = memcmp(acBuf, pSaleRec->Apn, iApnLen);
      if (!iTmp)
      {
         // Merge sale data
         iTmp = Scl_MergeGrGrRec(acBuf, acSaleRec, bAllGrgr, bUpdOwner);
         if (iTmp > 0)
            iSaleUpd++;

         // Read next sale record
         pTmp = fgets(acSaleRec, 1024, fdSale);
         if (!pTmp)
            bEof = true;    // Signal to stop sale update
         else
            goto GrGr_ReLoad;
      } else
      {
         if (iTmp > 0)        // Sale not match, advance to next sale record
         {
            if (bDebug)
               LogMsg0("*** Sale not match : %.12s > %.12s (%d) ", acBuf, pSaleRec->Apn, lCnt);
            iNoMatch++;
            pTmp = fgets(acSaleRec, 1024, fdSale);
            if (!pTmp)
               bEof = true;    // Signal to stop sale update
            else
               goto GrGr_ReLoad;
         }
      }

      // Save last recording date
      lRet = atoin((char *)&acBuf[OFF_TRANSFER_DT], 8);
      if (lRet > lLastRecDate && lRet < lToday)
         lLastRecDate = lRet;

      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);

      if (!bRet)
      {
         LogMsg("Error occurs: %d\n", GetLastError());
         lRet = WRITE_ERR;
         break;
      }
   }

   // Do the rest of the file
   while (nBytesRead>0 && ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL))
   {
      if (!nBytesRead)
         break;

      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
      lCnt++;
   }

   // Close files
   if (fdSale)
      fclose(fdSale);
   if (fhOut)
      CloseHandle(fhOut);
   if (fhIn)
      CloseHandle(fhIn);

   lRecCnt = lCnt;
   LogMsg("Total output records:       %u", lCnt);
   LogMsg("Total unmatched records:    %u", iNoMatch);
   LogMsg("Total GrGr records updated: %u", iSaleUpd);
   LogMsg("       Last recording date: %u", lLastRecDate);

   return 0;
}

/****************************** Scl_CreateGrGrRec ****************************
 *
 * Create SCL GrGr record.
 *
 * Return 0 if successful, >0 if error
 *
 *****************************************************************************/

int Scl_CreateGrGrRec(GRGR_DEF *pCurGrGr, char *pRec, int iCnt /* entry number for same document */)
{
   static   int iGrtor, iGrtee;
   char     acTmp[256], acDate[16];
   int      lTax, iTmp;
   double   dTax;
   SCL_GRGR *pGrGr = (SCL_GRGR *)pRec;

   if (iCnt == 1)
   {
      // Assume buffer is clear on first entry
      memcpy(pCurGrGr->APN, pGrGr->Apn, SIZ_SCL_GR_APN);
      memcpy(acTmp, pGrGr->Docno, SIZ_SCL_GR_DOCNO);
      blankRem(acTmp, SIZ_SCL_GR_DOCNO);
      memcpy(pCurGrGr->DocNum, acTmp, strlen(acTmp));
      memcpy(pCurGrGr->DocTitle, pGrGr->Title, SIZ_SCL_GR_TITLE);

      dTax = atofn(pGrGr->Transfer_Tax, SIZ_SCL_GR_TRANSFER_TAX);
      if (dTax > 0.0)
      {
         lTax = (long)(dTax * 100);
         sprintf(acTmp, "%*u", SIZ_GR_TAX, lTax);
         memcpy(pCurGrGr->Tax, acTmp ,SIZ_GR_TAX);

         lTax = (long)(dTax * SALE_FACTOR);
         sprintf(acTmp, "%*u", SIZ_GR_SALE, lTax);
         memcpy(pCurGrGr->SalePrice, acTmp ,SIZ_GR_SALE);
      }

      memcpy(acTmp, pGrGr->Date, SIZ_SCL_GR_DATE);
      acTmp[SIZ_SCL_GR_DATE] = 0;
      remChar(acTmp, 32);

      dateConversion(acTmp, acDate, MM_DD_YYYY_1);
      memcpy(pCurGrGr->DocDate, acDate ,SIZ_GR_DOCDATE);
      iGrtor = iGrtee = 0;
   }

   if (pGrGr->Typecode == 'E')
   {
      if (iGrtee < MAX_NAMES)
      {
         pCurGrGr->Grantees[iGrtee].NameType[0] = 'E';
         memcpy(pCurGrGr->Grantees[iGrtee].Name, pGrGr->Grgr, SIZ_GR_NAME);
      } else
      {
         pCurGrGr->MoreName = 'Y';
      }
      iTmp = sprintf(acTmp, "%d", ++iGrtee);
      memcpy(pCurGrGr->NameCnt, acTmp, iTmp);
   } else if (pGrGr->Typecode == 'R')
   {
      if (iGrtor < MAX_NAMES)
      {
         pCurGrGr->Grantors[iGrtor].NameType[0] = 'O';
         memcpy(pCurGrGr->Grantors[iGrtor++].Name, pGrGr->Grgr, SIZ_GR_NAME);
      }
   } else
      LogMsg("*** Unknown name type on: %.8s - %.20s", pGrGr->Apn, pGrGr->Grgr);

   return 0;
}

/******************************* Scl_LoadGrGrRP ******************************
 *
 * Return number of process records
 *
 *****************************************************************************/

int Scl_LoadGrGrRP(char *pInfile, FILE *fdOut)
{
   char     *pTmp, *pSrtCmd, acTmp[256], acRec[2048], acSavRec[2048];
   int      iRet, iCnt, lCnt;

   FILE     *fdIn;
   GRGR_DEF  curGrGrRec;
   SCL_GRGR  *pGrGr, *pSavGrGr;

   // Create output folder
   sprintf(acTmp, "%s\\SCL", acTmpPath);
   if (_access(acTmp, 0))
      _mkdir(acTmp);

   // Sort input file, skip header and records without APN
   pSrtCmd = "s(206,8,C,A,197,8,C,A,1,1,C,A)  OM(206,1,C,LT,\"0\",OR,206,1,C,GT,\"9\") B(227,D)";
   pTmp = strrchr(pInfile, '\\');
   strcat(acTmp, pTmp);
   printf("Sorting %s --> %s\n", pInfile, acTmp);
   lCnt = sortFile(pInfile, acTmp, pSrtCmd);
   if (lCnt <= 0)
      return 0;

   LogMsg("Process GrGr file %s", acTmp);
   if (!(fdIn = fopen(acTmp, "r")))
   {
      LogMsg("*** Error opening file: %s", acTmp);
      return 0;
   }

   // Update GrGr file date
   iRet = getFileDate(acTmp);
   if (iRet > lLastGrGrDate)
      lLastGrGrDate = iRet;

   // Initialize pointers
   iCnt = lCnt = 0;
   pSavGrGr = (SCL_GRGR *)&acSavRec;
   pGrGr = (SCL_GRGR *)&acRec;

   while (fdIn && !feof(fdIn))
   {
      pTmp = fgets(acRec, 2048, fdIn);
      if (!pTmp) break;

      // If same doc, increase iCnt
      if (!memcmp(pGrGr->Docno, pSavGrGr->Docno, SIZ_SCL_GR_DOCNO) &&
          !memcmp(pGrGr->Apn, pSavGrGr->Apn, SIZ_SCL_GR_APN))
         iCnt++;
      else
      {
         if (iCnt)
         {
            // Output GrGr record
            curGrGrRec.CRLF[0] = '\n';
            curGrGrRec.CRLF[1] = 0;

            iRet = fputs((char *)&curGrGrRec, fdOut);
         }

         strcpy(acSavRec, acRec);
         iCnt = 1;      // reset counter
         memset(&curGrGrRec, ' ', sizeof(GRGR_DEF));
      }

      iRet = Scl_CreateGrGrRec(&curGrGrRec, acRec, iCnt);

      lCnt++;
      if (!(lCnt % 1000))
         printf("\r%d", lCnt);
   }

   if (iCnt)
   {
      // Output GrGr record
      curGrGrRec.CRLF[0] = '\n';
      curGrGrRec.CRLF[1] = 0;

      iRet = fputs((char *)&curGrGrRec, fdOut);
   }

   fclose(fdIn);
   LogMsg("Total processed records  : %u", lCnt);

   return lCnt;
}

/********************************* Scl_LoadGrGr *****************************
 *
 * If successful, return 0.  Otherwise error.
 *    -5 : Data not available
 *    -4 : Input file is empty
 *    -3 : Error sorting output file
 *    -2 : Cannot create output file Scl_GrGr.dat
 *    -1 : Error in Scl_ExtrSaleMatched()
 *
 ****************************************************************************/

int Scl_LoadGrGr_Old(char *pCnty)
{
   char     *pTmp, acRec[512];
   char     acTmp[256], cFileCnt=1;
   char     acGrGrIn[_MAX_PATH], acGrGrOut[_MAX_PATH], acGrGrBak[_MAX_PATH];
   char     acCsvFile[_MAX_PATH];
   struct   _finddata_t  sFileInfo;

   FILE     *fdOut;

   int      iCnt, iRet;
   long     lCnt, lHandle, lTmp;

   // Get raw file name
   GetIniString(pCnty, "GrGrIn", "", acGrGrIn, _MAX_PATH, acIniFile);
   strcpy(acGrGrBak, acGrGrIn);
   pTmp = strrchr(acGrGrBak, '\\');
   if (pTmp)
      *++pTmp = 0;
   dateString(acRec, 0);
   sprintf(acTmp, "GrGr_%s", acRec);
   strcat(acGrGrBak, acTmp);

   // Open Input file
   lHandle = _findfirst(acGrGrIn, &sFileInfo);
   if (lHandle > 0)
   {
      pTmp = strrchr(acGrGrIn, '\\');
      if (pTmp)
         *pTmp = 0;
      iRet = 0;
   } else
   {
      bGrGrAvail = false;
      LogMsg("*** No new file avail for processing: %s", acGrGrIn);
      return -5;
   }

   // Create Output file - Scl_GrGr.Tmp
   sprintf(acGrGrOut, acGrGrTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "Tmp");
   LogMsg("Open output file %s", acGrGrOut);
   fdOut = fopen(acGrGrOut, "w");
   if (fdOut == NULL)
   {
      LogMsgD("***** Error creating GrGr output file: %s\n", acGrGrOut);
      _findclose(lHandle);
      return -2;
   }

   // Create backup folder
   if (_access(acGrGrBak, 0))
      _mkdir(acGrGrBak);

   iCnt = lCnt = 0;
   while (!iRet)
   {
      sprintf(acCsvFile, "%s\\%s", acGrGrIn, sFileInfo.name);

      // Update GrGr file date
      lTmp = getFileDate(acCsvFile);
      if (lTmp > lLastGrGrDate)
         lLastGrGrDate = lTmp;

      // Parse input file
      iRet = Scl_LoadGrGrRP(acCsvFile, fdOut);
      if (iRet <= 0)
         LogMsg("*** Skip %s", acCsvFile);
      else
      {
         lCnt += iRet;

         // Move input file to backup folder
         sprintf(acTmp, "%s\\%s", acGrGrBak, sFileInfo.name);
         rename(acCsvFile, acTmp);
      }

      iCnt++;
      // Find next file
      iRet = _findnext(lHandle, &sFileInfo);
   }

   // Close handle
   _findclose(lHandle);
   iRet = 0;

   // Close files
   if (fdOut)
      fclose(fdOut);

   LogMsg("Number of GRGR files have been processed: %d", iCnt);
   LogMsg("Total processed records  : %u", lCnt);
   printf("\nTotal processed records  : %u\n", lCnt);

   // Sort output
   if (lCnt > 0)
   {
      // Sort output file and dedup if same docdate and docnum
      // Sort on APN asc, DocNum asc, RecDate asc
      strcpy(acTmp,"S(17,8,C,A,1,10,C,A,37,8,C,A) F(TXT) DUPO(B2000, 1,44) ");
      strcpy(acGrGrIn, acGrGrOut);
      sprintf(acGrGrOut, acGrGrTmpl, pCnty, pCnty, "Dat");

      // Sort Scl_GrGr.Tmp into Scl_GrGr.Dat
      printf("Sort %s --> %s\n", acGrGrIn, acGrGrOut);
      lCnt = sortFile(acGrGrIn, acGrGrOut, acTmp);

      // Update cumulative sale file
      if (lCnt > 0)
      {
         // Match with roll file, set APN_Match and Owner_Match in Scl_GrGr.dat
         LogMsgD("\nMatch roll file");
         iRet = GrGr_MatchRoll(acGrGrOut, iApnLen);

         // Extract GrGr_Exp.dat from Scl_GrGr.dat
         iRet = Scl_ExtrSaleMatched(acGrGrOut);

         // Append Scl_GrGr.srt to Scl_GrGr.sls for backup
         //sprintf(acTmp, "S(1,14,C,A,27,8,C,A,15,12,C,A,57,10,C,D) DUPO(1,36)");
         sprintf(acGrGrIn, acGrGrTmpl, pCnty, pCnty, "Sls");
         sprintf(acGrGrBak, acGrGrTmpl, pCnty, pCnty, "Srt");
         sprintf(acRec, "%s+%s", acGrGrIn, acGrGrOut);
         iRet = sortFile(acRec, acGrGrBak, acTmp);
         if (iRet > 1000)
         {
            remove(acGrGrIn);
            iRet = rename(acGrGrBak, acGrGrIn);
         } else
            iRet = -1;

      } else
         iRet = -3;
   } else
      iRet = -4;

   LogMsg("Total output records after dedup: %u", lCnt);
   printf("\nTotal output records after dedup: %u\n", lCnt);

   return iRet;
}

/****************************** Scl_ParseGrGrName ****************************
 *
 * Parse names into an array up to MAX_NAMES. Return number of names.  
 *
 *****************************************************************************/

int Scl_ParseGrGrName(NREC asNames[], char *pNames, char cNameType)
{
   char  sTmp[2048], *apItems[50];
   int   iCnt, iIdx;

   strcpy(sTmp, pNames);
   // Remove "\," so we can parse on ","
   replStrAll(sTmp, "\\,", "");
   iCnt = ParseString(sTmp, ',', 50, apItems);
   for (iIdx = 0; iIdx < iCnt; iIdx++)
   {
      if (iIdx < MAX_NAMES)
      {
         // Ignore bad names
         if (*apItems[iIdx] < 'A' || *apItems[iIdx] > 'Z')
            break;
         vmemcpy(asNames[iIdx].Name, apItems[iIdx], SIZ_GR_NAME);
         asNames[iIdx].NameType[0] = cNameType;
      } else
         break;
   }

   return iCnt;
}

/****************************** Scl_LoadGrGrCsv ******************************
 *
 * Fix sale price on 7/29/2020 (a mistake was made on 5/16/2019 causing sale price too low)
 *
 * Return number of process records
 *
 *****************************************************************************/

#define SCL_GRGR_COLS      7
#define SCL_GRGR_GRTOR     0
#define SCL_GRGR_GRTEE     1
#define SCL_GRGR_DOCTITLE  2
#define SCL_GRGR_DOCDATE   3
#define SCL_GRGR_DOCNUM    4
#define SCL_GRGR_APN       5
#define SCL_GRGR_DOCTAX    6

int Scl_LoadGrGrCsv(char *pInfile, FILE *fdOut)
{
   FILE     *fdIn; 
   char     acBuf[MAX_RECSIZE], acTmp[256], acDate[32], *apApn[10], *pRec, cGrDelim;
   int      iApnCnt, iApnIdx, iRet, iTmp, iCnt=0;
   GRGR_DEF curGrGrRec;

   LogMsg("Loading %s ...", pInfile);
   if (!(fdIn = fopen(pInfile, "r")))
   {
      LogMsg("***** Error opening GrGr file %s", pInfile);
      return -1;
   }

   GetPrivateProfileString(myCounty.acCntyCode, "GrDelim", "|", acTmp, 32, acIniFile);
   cGrDelim = acTmp[0];

   // Drop header
   pRec = fgets(acBuf, MAX_RECSIZE, fdIn);

   // Loop through EOF
   while (!feof(fdIn))
   {
      pRec = fgets(acBuf, MAX_RECSIZE, fdIn);
      if (!pRec)
         break;
#ifdef _DEBUG
      //if (iCnt >= 920)
      //   iRet = 0;
#endif

      iRet = ParseStringNQ(acBuf, cGrDelim, SCL_GRGR_COLS, apTokens);
      if (iRet < SCL_GRGR_COLS)
      {
         LogMsg("Invalid number of columns: %s", acBuf);
         continue;
      }

      // Check for APN
      if (!apTokens[SCL_GRGR_APN] || !isdigit(*apTokens[SCL_GRGR_APN]))
         continue;

      memset((void *)&curGrGrRec, ' ', sizeof(GRGR_DEF));
      iApnCnt = ParseString(apTokens[SCL_GRGR_APN], ',', 10, apApn);
      iApnIdx = 0;
      iRet = strlen(apApn[iApnIdx]);
      if (iRet != iApnLen)
      {
         LogMsg("*** Bad APN: %s.  Please verify", apTokens[SCL_GRGR_APN]);

         if (iRet < iApnLen)
            continue;
         iRet = iApnLen;
      }
      vmemcpy(curGrGrRec.APN, apApn[iApnIdx], iRet);

#ifdef _DEBUG
      //if (!memcmp(curGrGrRec.APN, "14750085", 8))
      //   iRet = 0;
#endif

      if (iApnCnt > 1 && iApnCnt < 10)
         curGrGrRec.ParcelCount[0] = iApnCnt + '0';
      else if (iApnCnt > 9)
         LogMsg("*** APN Count is greater than 9 [%d].  Please check!!!", iApnCnt);

      // Grantor - parse grantor into multiple names
      iRet = Scl_ParseGrGrName(curGrGrRec.Grantors, apTokens[SCL_GRGR_GRTOR], 'O');

      // Grantee - parse grantee into multiple names
      iRet = Scl_ParseGrGrName(curGrGrRec.Grantees, apTokens[SCL_GRGR_GRTEE], 'E');
      if (iRet > 0)
      {
         iTmp = sprintf(acTmp, "%d", iRet);
         memcpy(curGrGrRec.NameCnt, acTmp, iTmp);

         if (iRet > 2)
            curGrGrRec.MoreName = 'Y';
      }

      // DocType
      vmemcpy(curGrGrRec.DocTitle, apTokens[SCL_GRGR_DOCTITLE], SIZ_GR_TITLE);

      // DocTax
      double   dTax;
      dTax = atof(apTokens[SCL_GRGR_DOCTAX]);
      if (dTax > 0.0)
      {
         iTmp = sprintf(acTmp, "%.2f", dTax);
         memcpy(curGrGrRec.Tax, acTmp, iTmp);

         iTmp = sprintf(acTmp, "%d", (long)(dTax * SALE_FACTOR));
         memcpy(curGrGrRec.SalePrice, acTmp, iTmp);
      }

      // DocDate
      if (*apTokens[SCL_GRGR_DOCDATE] >= '0')
      {
         strcpy(acTmp, apTokens[SCL_GRGR_DOCDATE]);
         if (dateConversion(acTmp, acDate, MM_DD_YYYY_1))
         {
            memcpy(curGrGrRec.DocDate, acDate ,SIZ_GR_DOCDATE);
            iTmp = atol(acTmp);
            if (iTmp > lLastRecDate)
               lLastRecDate = iTmp;
         }
      }

      // DocNum
      vmemcpy(curGrGrRec.DocNum, apTokens[SCL_GRGR_DOCNUM], SIZ_GR_DOCNUM);

      curGrGrRec.CRLF[0] = '\n';
      curGrGrRec.CRLF[1] = '\0';
      iRet = fputs((char *)&curGrGrRec, fdOut);

      // Multi APN
      while (++iApnIdx < iApnCnt)
      {
         vmemcpy(curGrGrRec.APN, apApn[iApnIdx], iApnLen);
         iRet = fputs((char *)&curGrGrRec, fdOut);
      }
      memset(acBuf,0,MAX_RECSIZE);

      if (!(++iCnt % 1000))
         printf("\r%u", iCnt);
   }

   if (fdIn) fclose(fdIn);

   printf("\n");
   LogMsgD("Number of GrGr record output:  %d\n", iCnt);

   return iCnt;
}

/********************************* Scl_LoadGrGr *****************************
 *
 * If successful, return 0.  Otherwise error.
 *    -5 : Data not available
 *    -4 : Input file is empty
 *    -3 : Error sorting output file
 *    -2 : Cannot create output file Scl_GrGr.dat
 *    -1 : Error in Scl_ExtrSaleMatched()
 *
 ****************************************************************************/

int Scl_LoadGrGr(char *pCnty)
{
   char     *pTmp, acRec[512];
   char     acTmp[256], cFileCnt=1;
   char     acGrGrIn[_MAX_PATH], acGrGrOut[_MAX_PATH], acGrGrBak[_MAX_PATH];
   char     acCsvFile[_MAX_PATH];
   struct   _finddata_t  sFileInfo;

   FILE     *fdOut;

   int      iCnt, iRet;
   long     lCnt, lHandle, lTmp;

   // Get raw file name
   GetIniString(pCnty, "GrGrIn", "", acGrGrIn, _MAX_PATH, acIniFile);

   // Prepare backup folder
   sprintf(acGrGrBak, acGrBkTmpl, pCnty, acToday);
   if (_access(acGrGrBak, 0))
      _mkdir(acGrGrBak);

   // Open Input file
   lHandle = _findfirst(acGrGrIn, &sFileInfo);
   if (lHandle > 0)
   {
      pTmp = strrchr(acGrGrIn, '\\');
      if (pTmp)
         *pTmp = 0;
      iRet = 0;
   } else
   {
      bGrGrAvail = false;
      LogMsg("*** No new file avail for processing: %s", acGrGrIn);
      return -5;
   }

   // Create Output file - Scl_GrGr.Tmp
   sprintf(acGrGrOut, acGrGrTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "Tmp");
   LogMsg("Open output file %s", acGrGrOut);
   fdOut = fopen(acGrGrOut, "w");
   if (fdOut == NULL)
   {
      LogMsgD("***** Error creating GrGr output file: %s\n", acGrGrOut);
      _findclose(lHandle);
      return -2;
   }

   // Create backup folder
   if (_access(acGrGrBak, 0))
      _mkdir(acGrGrBak);

   iCnt = lCnt = 0;
   while (!iRet)
   {
      sprintf(acCsvFile, "%s\\%s", acGrGrIn, sFileInfo.name);

      // Update GrGr file date
      lTmp = getFileDate(acCsvFile);
      if (lTmp > lLastGrGrDate)
         lLastGrGrDate = lTmp;

      // Parse input file
      iRet = Scl_LoadGrGrCsv(acCsvFile, fdOut);
      if (iRet <= 0)
         LogMsg("*** Skip %s", acCsvFile);
      else
      {
         lCnt += iRet;

         // Move input file to backup folder
         sprintf(acTmp, "%s\\%s", acGrGrBak, sFileInfo.name);
         rename(acCsvFile, acTmp);
      }

      iCnt++;
      // Find next file
      iRet = _findnext(lHandle, &sFileInfo);
   }

   // Close handle
   _findclose(lHandle);
   iRet = 0;

   // Close files
   if (fdOut)
      fclose(fdOut);

   LogMsg("Number of GRGR files have been processed: %d\n", iCnt);
   LogMsg("Total processed records : %u", lCnt);
   LogMsg("    Last recording date : %u\n", lLastRecDate);

   // Sort output
   if (lCnt > 0)
   {
      // Sort output file and dedup if same docdate and docnum
      // Sort on APN asc, DocNum asc, RecDate asc
      strcpy(acTmp,"S(17,8,C,A,1,10,C,A,37,8,C,A) F(TXT) DUPO(B2000, 1,44) ");
      sprintf(acGrGrIn, acGrGrTmpl, pCnty, pCnty, "Sls");
      sprintf(acGrGrBak, acGrGrTmpl, pCnty, pCnty, "Dat");
      sprintf(acRec, "%s+%s", acGrGrOut, acGrGrIn);

      // Sort Scl_GrGr.Tmp+Scl_GrGr.Sls into Scl_GrGr.dat
      printf("Sort %s ---> %s\n", acRec, acGrGrBak);
      lCnt = sortFile(acRec, acGrGrBak, acTmp);

      // Update cumulative sale file
      if (lCnt > 0)
      {
         sprintf(acTmp, acGrGrTmpl, pCnty, pCnty, "sav");
         if (!_access(acTmp, 0))
            DeleteFile(acTmp);

         // Rename Scl_GrGr.Sls to Scl_GrGr.sav
         LogMsg("Rename %s ---> %s", acGrGrIn, acTmp);
         rename(acGrGrIn, acTmp);
   
         // Rename Scl_GrGr.DAT to Scl_GrGr.SLS file
         LogMsg("Rename %s ---> %s\n", acGrGrBak, acGrGrIn);
         iRet = rename(acGrGrBak, acGrGrIn);

         // Extract GrGr_Exp.dat from Scl_GrGr.sls
         iRet = Scl_ExtrSaleMatched(acGrGrIn);
      } else
         iRet = -3;
   } else
      iRet = -4;

   return iRet;
}

/******************************** Sbx_MergeOwner *****************************
 *
 * Return 0 if successful, >0 if error
 *
 *****************************************************************************/

void Scl_MergeOwner(char *pOutbuf, char *pName1, char *pName2)
{
   int   iTmp;
   char  acTmp[128], acSave1[64], acSave2[64];
   char  acOwner1[64], acOwner2[64];
   char  *pTmp, *pTmp1;
   bool  bNoMerge=false, bKeep=false;

   OWNER myOwner1, myOwner2;

   // Clear output buffer if needed
   removeNames(pOutbuf, true);
   acSave1[0]=acSave2[0] = 0;

   // Remove multiple spaces
   memcpy(acOwner1, pName1, RSIZ_SCL_NAME1);
   acOwner1[RSIZ_SCL_NAME1] = 0;
   if (*pName2 > ' ')
   {
      memcpy(acOwner2, pName2, RSIZ_SCL_NAME1);
      acOwner2[RSIZ_SCL_NAME1] = 0;
      if (*pName2 == '*')
      {
         // Copy Name1 to seller
         memcpy(pOutbuf+OFF_SELLER, acOwner1, SIZ_SELLER);

         strcpy(acOwner1, &acOwner2[1]);
         acOwner2[0] = 0;
      } else if (!memcmp(acOwner2, "C/O", 3))
      {
         updateCareOf(pOutbuf, acOwner2, RSIZ_SCL_NAME1);
         acOwner2[0] = 0;
      }
   } else
      acOwner2[0] = 0;

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "00301055", iApnLen))
   //   iTmp = 0;
#endif

   // Save owner1
   strcpy(acSave1, acOwner1);

   // Update vesting - If no vesting in name1, try name2
   iTmp = updateVesting(myCounty.acCntyCode, acOwner1, pOutbuf+OFF_VEST, pOutbuf+OFF_ETAL_FLG);
   if (!iTmp && acOwner2[0] > ' ')
      iTmp = updateVesting(myCounty.acCntyCode, acOwner2, pOutbuf+OFF_VEST, pOutbuf+OFF_ETAL_FLG);

   // Do not merge if vesting is set
   if (*(pOutbuf+OFF_VEST) > ' ')
      bKeep = true;

   if (acOwner2[0] > ' ')
   {
      strcpy(acSave2, acOwner2);
      if (pTmp = strstr(acOwner2, " TRUSTE"))
         *pTmp = 0;
      if ((pTmp = strstr(acOwner2, " ET AL")) || (pTmp = strstr(acOwner2, " ETAL")) )
         *pTmp = 0;

      pTmp = isNumIncluded(acOwner2, 0);
      if (pTmp || strstr(acOwner2, " TRUST ") || strstr(acOwner2, " AND ")  )
         bNoMerge = true;
   }

   // Drop TRUSTEE and ET AL
   if (pTmp = strstr(acOwner1, " TRUSTE"))
      *pTmp = 0;
   if ((pTmp = strstr(acOwner1, " & ET AL")) ||
       (pTmp = strstr(acOwner1, " ET AL")) ||
       (pTmp = strstr(acOwner1, " ETAL")) ||
       (pTmp = strstr(acOwner1, " R/O")))
      *pTmp = 0;

   pTmp = &acOwner1[RSIZ_SCL_NAME1-4];
   if (!memcmp(pTmp, "TRUS", 4) || !memcmp(pTmp, " TRU", 4) )
      *pTmp = 0;

   // If name starts with a digit, do not parse
   if (isdigit(acOwner1[0]))
   {
      memcpy(pOutbuf+OFF_NAME1, acSave1, strlen(acSave1));

      iTmp = strlen(acSave2);
      if (iTmp > 0)
         memcpy(pOutbuf+OFF_NAME2, acSave2, (iTmp < SIZ_NAME2 ? iTmp:SIZ_NAME2));

      iTmp = strlen(acOwner1);
      memcpy(pOutbuf+OFF_NAME_SWAP, acOwner1, (iTmp < SIZ_NAME_SWAP ? iTmp:SIZ_NAME_SWAP));

      return;
   }

   if (!strcmp(acOwner1, acOwner2))
      acOwner2[0] = 0;

   pTmp = isNumIncluded(acOwner1, 0);
   if (pTmp || strstr(acOwner1, " TRUST ") || strstr(acOwner1, " AND ")  )
      bNoMerge = true;

   // Check for trust
   if ((pTmp = strstr(acOwner1, " FAMILY")) ||
       (pTmp = strstr(acOwner1, " LIFE ESTATE")) ||
       (pTmp = strstr(acOwner1, " TRUST")) ||
       (pTmp = strstr(acOwner1, " EST OF"))
      )
   {
      *pTmp = 0;
      bKeep = true;
   }

   // Now remove '.' then parse owners
   if (pTmp = strchr(acOwner1, '.'))
   {
      *pTmp = ' ';
      if (pTmp1 = strchr(pTmp, '.'))
         *pTmp1 = ' ';
      blankRem(acOwner1);
   }

   iTmp = splitOwner(acOwner1, &myOwner1, 2);
   if (iTmp == -1)
   {
      strcpy(myOwner1.acSwapName, acSave1);
      bNoMerge = true;
   } else if (strchr(myOwner1.acOM, '/'))
   {
      pTmp = strchr(acSave1, '/');
      if (pTmp && *(pTmp-1) == ' ' && *(pTmp+1) == ' ')
      {
         *pTmp = '&';
         if (pTmp = strchr(myOwner1.acSwapName, '/'))
            *pTmp = '&';
      }
   }

   if (!strcmp(myOwner1.acOL, myOwner1.acSM) && !strcmp(myOwner1.acOF, myOwner1.acSL))
      myOwner1.acName2[0] = 0;

   if (acOwner2[0])
   {
      if (!bNoMerge)
      {
         // Parse name2 - if they have same last name, merge them
         iTmp = splitOwner(acOwner2, &myOwner2, 2);
         if (iTmp == 1 && !strcmp(myOwner1.acOL, myOwner2.acOL))
         {
            // Ignore name2 if name1 and name2 have the same first and last name
            if (strcmp(myOwner1.acOF, myOwner2.acOF))
            {
               sprintf(acTmp, "%s %s & %s", myOwner1.acOF, myOwner1.acOM, myOwner2.acSwapName);
               blankRem(acTmp);
               strcpy(myOwner1.acSwapName, acTmp);

               // Merge name1 if not protected
               if (!bKeep)
               {
                  sprintf(acTmp, "%s & %s %s", myOwner1.acName1, myOwner2.acOF, myOwner2.acOM);
                  blankRem(acTmp);
                  strcpy(myOwner1.acName1, acTmp);
                  acSave1[0] = 0;
               } else
                  strcpy(myOwner1.acName2, acSave2);
            }
         } else
            strcpy(myOwner1.acName2, acSave2);
      } else
         strcpy(myOwner1.acName2, acSave2);
   } else
      myOwner1.acName2[0] = 0;

   // Owner1
   if (acSave1[0])
      memcpy(pOutbuf+OFF_NAME1, acSave1, strlen(acSave1));
   else
      memcpy(pOutbuf+OFF_NAME1, myOwner1.acName1, strlen(myOwner1.acName1));

   // Owner2
   if (myOwner1.acName2[0] > ' ')
   {
      iTmp = strlen(myOwner1.acName2);
      if (iTmp > SIZ_NAME2)
         iTmp = SIZ_NAME2;
      memcpy(pOutbuf+OFF_NAME2, myOwner1.acName2, iTmp);
   }

   // Swapped name
   if (myOwner1.acSwapName[0] > ' ')
   {
      iTmp = strlen(myOwner1.acSwapName);
      if (iTmp > SIZ_NAME_SWAP)
         iTmp = SIZ_NAME_SWAP;
      memcpy(pOutbuf+OFF_NAME_SWAP, myOwner1.acSwapName, iTmp);
   }

}

/********************************* Scl_MergeAdr ******************************
 *
 * Return 0 if successful, >0 if error
 *
 *****************************************************************************/

void Scl_MergeMAdr(char *pOutbuf, char *pRollRec)
{
   SCL_ROLL *pRec;
   char     acTmp[256], acAddr1[64], acAddr2[64], *pTmp;
   int      iTmp;
   ADR_REC  sMailAdr;

   pRec = (SCL_ROLL *)pRollRec;

   // Clear old Mailing
   removeMailing(pOutbuf, false);

   // Check for blank address
   if (memcmp(pRec->M_Addr1, "     ", 5))
   {
      memcpy(acAddr1, pRec->M_Addr1, RSIZ_SCL_M_ADDR1);
      blankRem(acAddr1, RSIZ_SCL_M_ADDR1);

      // Parsing mail address
      memset((void *)&sMailAdr, 0, sizeof(ADR_REC));
      parseMAdr1(&sMailAdr, acAddr1);

      if (sMailAdr.lStrNum > 0)
      {
         iTmp = sprintf(acTmp, "%d", sMailAdr.lStrNum);
         memcpy(pOutbuf+OFF_M_STRNUM, acTmp, iTmp);
         if (sMailAdr.strSub[0] > '0')
         {
            sprintf(acTmp, "%s  ", sMailAdr.strSub);
            memcpy(pOutbuf+OFF_M_STR_SUB, acTmp, SIZ_M_STR_SUB);
         }
      }
      memcpy(pOutbuf+OFF_M_DIR, sMailAdr.strDir, strlen(sMailAdr.strDir));
      memcpy(pOutbuf+OFF_M_STREET, sMailAdr.strName, strlen(sMailAdr.strName));
      memcpy(pOutbuf+OFF_M_SUFF, sMailAdr.strSfx, strlen(sMailAdr.strSfx));

      if (sMailAdr.Unit[0] > ' ')
      {
         vmemcpy(pOutbuf+OFF_M_UNITNO, sMailAdr.Unit, SIZ_M_UNITNO);
         vmemcpy(pOutbuf+OFF_M_UNITNOX, sMailAdr.UnitNox, SIZ_M_UNITNOX);
      }
      memcpy(pOutbuf+OFF_M_ADDR_D, acAddr1, strlen(acAddr1));

#ifdef _DEBUG
      //if (!memcmp(pOutbuf, "00309033", 8))
      //   iTmp = 0;
#endif

      // City state
      if (pRec->M_CitySt[0] > ' ')
      {
         memcpy(acAddr2, pRec->M_CitySt, RSIZ_SCL_M_ADDR2+RSIZ_SCL_M_ZIP);
         acAddr2[RSIZ_SCL_M_ADDR2+RSIZ_SCL_M_ZIP] = 0;
         //blankRem(acAddr2, RSIZ_SCL_M_ADDR2);
         if (pTmp = strstr(acAddr2, " UK "))
         {
            strcpy(sMailAdr.State, "UK");
            strcpy(sMailAdr.Zip, pTmp+5);
            *pTmp = 0;
            if (pTmp = strrchr(acAddr2, ','))
            {
               if (*(pTmp+1) == ' ')
                  strcpy(sMailAdr.City, pTmp+2);
               else
                  strcpy(sMailAdr.City, pTmp+1);
            } else
               vmemcpy(sMailAdr.City, acAddr2, SIZ_S_CITY-1);
         } else
         {
            acAddr2[RSIZ_SCL_M_ADDR2] = 0;
            parseMAdr2(&sMailAdr, acAddr2);
         }

         iTmp = iTrim(sMailAdr.City);
         if (iTmp > SIZ_M_CITY)
         {
            memcpy(pOutbuf+OFF_M_CITY, sMailAdr.City, SIZ_M_CITY);
            vmemcpy(pOutbuf+OFF_M_CITYX, &sMailAdr.City[SIZ_M_CITY], SIZ_M_CITYX);
            LogMsg("*** Long mail city: %s [%.*s]", sMailAdr.City, iApnLen, pOutbuf);
         } else
            vmemcpy(pOutbuf+OFF_M_CITY, sMailAdr.City, SIZ_M_CITY);

         if (sMailAdr.State[0] >= 'A')
            vmemcpy(pOutbuf+OFF_M_ST, sMailAdr.State, SIZ_M_ST);

         iTmp = atoin(pRec->M_Zip, SIZ_M_ZIP);
         if (iTmp > 500)
         {
            myLTrim(pRec->M_Zip, RSIZ_SCL_M_ZIP);
            if (pRec->M_Zip[0] != '9' && !memcmp(pOutbuf+OFF_M_ST, "CA", 2))
            {
               // Check for known error
               if ((!memcmp(&pRec->M_Zip[1], "4063", 4) && !memcmp(pOutbuf+OFF_M_CITY, "REDWOOD CITY", 6)) ||
                  (!memcmp(&pRec->M_Zip[1], "4555", 4) && !memcmp(pOutbuf+OFF_M_CITY, "FREMONT", 6)) ||
                  (!memcmp(&pRec->M_Zip[1], "4568", 4) && !memcmp(pOutbuf+OFF_M_CITY, "DUBLIN", 6)) ||
                  (!memcmp(&pRec->M_Zip[1], "4404", 4) && !memcmp(pOutbuf+OFF_M_CITY, "FOSTER CITY", 8)) ||
                  (!memcmp(&pRec->M_Zip[1], "5008", 4) && !memcmp(pOutbuf+OFF_M_CITY, "CAMPBELL", 6)) ||
                  (!memcmp(&pRec->M_Zip[1], "5013", 3) && !memcmp(pOutbuf+OFF_M_CITY, "COYOTE", 6)) ||
                  (!memcmp(&pRec->M_Zip[1], "5030", 3) && !memcmp(pOutbuf+OFF_M_CITY, "LOS GATOS", 6)) ||
                  (!memcmp(&pRec->M_Zip[1], "5120", 4) && !memcmp(pOutbuf+OFF_M_CITY, "SAN JOSE", 6)) ||
                  (!memcmp(&pRec->M_Zip[1], "5363", 4) && !memcmp(pOutbuf+OFF_M_CITY, "PATTERSON", 8)) ||
                  (!memcmp(&pRec->M_Zip[1], "5404", 4) && !memcmp(pOutbuf+OFF_M_CITY, "SANTA ROSA", 8)) ||
                  (!memcmp(&pRec->M_Zip[1], "4127", 4) && !memcmp(pOutbuf+OFF_M_CITY, "SAN FRANCISCO", 8)) )
                  pRec->M_Zip[0] = '9';
               else 
               if (!memcmp(pRec->M_Zip, "39350", 5) && !memcmp(pOutbuf+OFF_M_CITY, "PACIFIC GROVE", 10))
                  strcpy(pRec->M_Zip, "93950");
               else
               if (!memcmp(pRec->M_Zip, "5110", 4) && !memcmp(pOutbuf+OFF_M_CITY, "SAN JOSE", 6))
                  strcpy(pRec->M_Zip, "95110");
               else
               if (!memcmp(pRec->M_Zip, "5038", 4) && !memcmp(pOutbuf+OFF_M_CITY, "MORGAN HILL", 6))
                  strcpy(pRec->M_Zip, "95038");
               else
               if (!memcmp(pOutbuf+OFF_M_CITY, "PALO ALTO", 6))
                  strcpy(pRec->M_Zip, "94301");
               else
               if (!memcmp(pRec->M_Zip, "85014", 3) || !memcmp(pRec->M_Zip, "85361", 5) || !memcmp(pRec->M_Zip, "85261", 5) ||
                   !memcmp(pRec->M_Zip, "85210", 3) || !memcmp(pRec->M_Zip, "86323", 5) || !memcmp(pRec->M_Zip, "85283", 5) ||
                   !memcmp(pRec->M_Zip, "85743", 5) || !memcmp(pRec->M_Zip, "85382", 5) || !memcmp(pRec->M_Zip, "85208", 5) ||
                   !memcmp(pRec->M_Zip, "85051", 5))
               {
                  memcpy(pOutbuf+OFF_M_ST, "AZ", 2);
                  strcpy(sMailAdr.State, "AZ");
               } else
               if (!memcmp(pRec->M_Zip, "80249", 5) || !memcmp(pRec->M_Zip, "80303", 5) || !memcmp(pRec->M_Zip, "80601", 5) || 
                   !memcmp(pRec->M_Zip, "80504", 5) || !memcmp(pRec->M_Zip, "80924", 5))
               {
                  memcpy(pOutbuf+OFF_M_ST, "CO", 2);
                  strcpy(sMailAdr.State, "CO");
               } else
               if (!memcmp(pRec->M_Zip, "33305", 5) || !memcmp(pRec->M_Zip, "33487", 5) || !memcmp(pRec->M_Zip, "34208", 5) || 
                   !memcmp(pRec->M_Zip, "33401", 5) || !memcmp(pRec->M_Zip, "33141", 5) )
               {
                  memcpy(pOutbuf+OFF_M_ST, "FL", 2);
                  strcpy(sMailAdr.State, "FL");
               } else
               if (!memcmp(pRec->M_Zip, "30601", 5) || !memcmp(pRec->M_Zip, "30560", 5) || !memcmp(pRec->M_Zip, "30062", 5))
               {
                  memcpy(pOutbuf+OFF_M_ST, "GA", 2);
                  strcpy(sMailAdr.State, "GA");
               } else
               if (!memcmp(pRec->M_Zip, "83616", 5) || !memcmp(pRec->M_Zip, "83353", 5) || !memcmp(pRec->M_Zip, "83709", 5) ||
                   !memcmp(pRec->M_Zip, "83619", 5) || !memcmp(pRec->M_Zip, "83644", 5))
               {
                  memcpy(pOutbuf+OFF_M_ST, "ID", 2);
                  strcpy(sMailAdr.State, "ID");
               } else
               if (!memcmp(pRec->M_Zip, "60690", 5) || !memcmp(pRec->M_Zip, "61607", 5) || !memcmp(pRec->M_Zip, "60173", 5) || 
                   !memcmp(pRec->M_Zip, "61254", 5) || !memcmp(pRec->M_Zip, "60523", 5))
               {
                  memcpy(pOutbuf+OFF_M_ST, "IL", 2);
                  strcpy(sMailAdr.State, "IL");
               } else
               if (!memcmp(pRec->M_Zip, "21613", 5) || !memcmp(pRec->M_Zip, "20878", 5) || !memcmp(pRec->M_Zip, "20905", 5))
               {
                  memcpy(pOutbuf+OFF_M_ST, "MD", 2);
                  strcpy(sMailAdr.State, "MD");
               } else
               if (!memcmp(pRec->M_Zip, "55127", 5))
               {
                  memcpy(pOutbuf+OFF_M_ST, "MN", 2);
                  strcpy(sMailAdr.State, "MN");
               } else
               if (!memcmp(pRec->M_Zip, "64086", 5) || !memcmp(pRec->M_Zip, "65014", 5) || !memcmp(pRec->M_Zip, "65682", 5))
               {
                  memcpy(pOutbuf+OFF_M_ST, "MO", 2);
                  strcpy(sMailAdr.State, "MO");
               } else
               if (!memcmp(pRec->M_Zip, "59870", 5))
               {
                  memcpy(pOutbuf+OFF_M_ST, "MT", 2);
                  strcpy(sMailAdr.State, "MT");
               } else
               if (!memcmp(pRec->M_Zip, "69161", 5) )
               {
                  memcpy(pOutbuf+OFF_M_ST, "NE", 2);
                  strcpy(sMailAdr.State, "NE");
               } else
               if (!memcmp(pRec->M_Zip, "89135", 3) || !memcmp(pRec->M_Zip, "89431", 5) || !memcmp(pRec->M_Zip, "89450", 5) || 
                   !memcmp(pRec->M_Zip, "89523", 3) || !memcmp(pRec->M_Zip, "89515", 5) || !memcmp(pRec->M_Zip, "89015", 5))
               {
                  memcpy(pOutbuf+OFF_M_ST, "NV", 2);
                  strcpy(sMailAdr.State, "NV");
               } else
               if (!memcmp(pRec->M_Zip, "10010", 5) || !memcmp(pRec->M_Zip, "11249", 5) )
               {
                  memcpy(pOutbuf+OFF_M_ST, "NY", 2);
                  strcpy(sMailAdr.State, "NY");
               } else
               if (!memcmp(pRec->M_Zip, "44131", 5) )
               {
                  memcpy(pOutbuf+OFF_M_ST, "OH", 2);
                  strcpy(sMailAdr.State, "OH");
               } else
               if (!memcmp(pRec->M_Zip, "73107", 5))
               {
                  memcpy(pOutbuf+OFF_M_ST, "OK", 2);
                  strcpy(sMailAdr.State, "OK");
               } else
               if (!memcmp(pRec->M_Zip, "16947", 5) || !memcmp(pRec->M_Zip, "16161", 5) )
               {
                  memcpy(pOutbuf+OFF_M_ST, "PA", 2);
                  strcpy(sMailAdr.State, "PA");
               } else
               if (!memcmp(pRec->M_Zip, "37027", 5))
               {
                  memcpy(pOutbuf+OFF_M_ST, "TN", 2);
                  strcpy(sMailAdr.State, "TN");
               } else
               if (!memcmp(pRec->M_Zip, "75240", 5) || !memcmp(pRec->M_Zip, "75052", 5) || !memcmp(pRec->M_Zip, "78613", 5) || 
                   !memcmp(pRec->M_Zip, "75771", 5) || !memcmp(pRec->M_Zip, "77056", 5) || !memcmp(pRec->M_Zip, "78724", 5) ||
                   !memcmp(pRec->M_Zip, "78660", 5) || !memcmp(pRec->M_Zip, "78681", 5))
               {
                  memcpy(pOutbuf+OFF_M_ST, "TX", 2);
                  strcpy(sMailAdr.State, "TX");
               } else
               if (!memcmp(pRec->M_Zip, "84093", 5) || !memcmp(pRec->M_Zip, "84060", 5) )
               {
                  memcpy(pOutbuf+OFF_M_ST, "UT", 2);
                  strcpy(sMailAdr.State, "UT");
               } else
               if (!memcmp(pRec->M_Zip, "22102", 5) || !memcmp(pRec->M_Zip, "22033", 5) || !memcmp(pRec->M_Zip, "24061", 5) )
               {
                  memcpy(pOutbuf+OFF_M_ST, "VA", 2);
                  strcpy(sMailAdr.State, "VA");
               } else
               if (!memcmp(pRec->M_Zip, "53703", 5) )
               {
                  memcpy(pOutbuf+OFF_M_ST, "WI", 2);
                  strcpy(sMailAdr.State, "WI");
               } else
               if (!memcmp(pRec->M_Zip, "82001", 5) )
               {
                  memcpy(pOutbuf+OFF_M_ST, "WY", 2);
                  strcpy(sMailAdr.State, "WY");
               } else
               {
                  LogMsg("*** Bad CA zipcode: %.10s [%.*s]", pRec->M_Zip, iApnLen, pOutbuf);
                  pRec->M_Zip[0] = 0;
               }
            }

            // 03/05/2022 - spn
            if (pRec->M_Zip[0] > ' ' && pRec->M_Zip[4] >= '0')
            {
               vmemcpy(pOutbuf+OFF_M_ZIP, pRec->M_Zip, SIZ_M_ZIP);
               iTmp = atoin(&pRec->M_Zip[6], SIZ_M_ZIP4);
               if (iTmp > 0)
                  vmemcpy(pOutbuf+OFF_M_ZIP4, &pRec->M_Zip[6], SIZ_M_ZIP4);
            }
            sprintf(acAddr2, "%s, %s %.10s", myTrim(sMailAdr.City), sMailAdr.State, pRec->M_Zip);
         } else
            sprintf(acAddr2, "%s, %s", myTrim(sMailAdr.City), sMailAdr.State);

         vmemcpy(pOutbuf+OFF_M_CTY_ST_D, acAddr2, SIZ_M_CTY_ST_D);
      }
   }
}

/******************************** Scl_MergeSitus *****************************
 *
 * Merge situs using "SF902PC.txt" & "SF902PC-2022-07-05.txt"
 *
 * Return 0 if successful, >0 if error
 *
 *****************************************************************************/

int Scl_MergeSitus(char *pOutbuf)
{
   static   char acRec[1024], *pRec=NULL;
   char     acTmp[256], acAddr1[64], acCity[32], acCode[32], *pSfx;
   long     lTmp;
   int      iTmp, iLoop, iSfx;

   SCL_SITUS   *pSitus = (SCL_SITUS *)&acRec[0];

   // Get first rec for first call
   if (!pRec)
      pRec = fgets(acRec, 1024, fdSitus);

   do
   {
      if (!pRec)
      {
         fclose(fdSitus);
         fdSitus = NULL;
         return 1;      // EOF
      }

      // Compare Apn
      iLoop = memcmp(pOutbuf, pSitus->Apn, RSIZ_SCL_APN);
      if (iLoop > 0)
      {
         if (bDebug)
            LogMsg0("Skip situs Addr rec  %.*s", RSIZ_SCL_APN, pSitus->Apn);
         pRec = fgets(acRec, 1024, fdSitus);
         lSitusSkip++;
      }
   } while (iLoop > 0);

   // If not match, return
   if (iLoop)
      return 1;

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "02314101", 9))
   //   lTmp = 0;
#endif

   // Initialize
   acAddr1[0] = 0;
   lTmp = atoin(pSitus->strNum, ASIZ_SCL_S_STRNUM);
   if (lTmp > 0)
   {
      sprintf(acAddr1, "%d       ", lTmp);
      memcpy(pOutbuf+OFF_S_STRNUM, acAddr1, SIZ_S_STRNUM);
      memcpy(pOutbuf+OFF_S_HSENO, acAddr1, SIZ_S_STRNUM);
      if (pSitus->strFra[0] > ' ')
         memcpy(pOutbuf+OFF_S_STR_SUB, pSitus->strFra, SIZ_S_STR_SUB);

      if (pSitus->strDir[0] > ' ')
         memcpy(pOutbuf+OFF_S_DIR, pSitus->strDir, ASIZ_SCL_S_STRDIR);

      if (pSitus->strName[0] > '0')
         memcpy(pOutbuf+OFF_S_STREET, pSitus->strName, ASIZ_SCL_S_STRNAME);

      pSfx = NULL;
      acCode[0] = 0;
      if (pSitus->strSfx[0] > ' ')
      {
         memcpy(acCode, pSitus->strSfx, ASIZ_SCL_S_STRSFX);
         acCode[ASIZ_SCL_S_STRSFX] = 0;
         iSfx = GetSfxDev(acCode);
         if (iSfx > 0)
         {
            iTmp = sprintf(acTmp, "%d", iSfx);
            memcpy(pOutbuf+OFF_S_SUFF, acTmp, iTmp);

            pSfx = GetSfxStr(iSfx);
            strcpy(acCode, pSfx);
         }
      }

      if (pSitus->Unit[0] > ' ')
         memcpy(pOutbuf+OFF_S_UNITNO, pSitus->Unit, ASIZ_SCL_S_UNIT);

      if (pSitus->Unit[0] > ' ')
         sprintf(acAddr1, "%d %.3s %.2s %.*s %s #%.4s", lTmp, pSitus->strFra, pSitus->strDir, ASIZ_SCL_S_STRNAME, pSitus->strName, acCode, pSitus->Unit);
      else
         sprintf(acAddr1, "%d %.3s %.2s %.*s %s", lTmp, pSitus->strFra, pSitus->strDir, ASIZ_SCL_S_STRNAME, pSitus->strName, acCode);
      blankRem(acAddr1);
      memcpy(pOutbuf+OFF_S_ADDR_D, acAddr1, strlen(acAddr1));
   }

   // City
   if (pSitus->City[0] > ' ')
   {
      memcpy(acTmp, pSitus->City, ASIZ_SCL_S_CITY);
      acTmp[ASIZ_SCL_S_CITY] = 0;
      Abbr2Code(acTmp, acCode, acCity);
      if (acCode[0] > ' ')
      {
         memcpy(pOutbuf+OFF_S_CITY, acCode, strlen(acCode));
         strcat(acCity, " CA");
         blankRem(acCity);
         memcpy(pOutbuf+OFF_S_CTY_ST_D, acCity, strlen(acCity));
      }
   }

   // State
   memcpy(pOutbuf+OFF_S_ST, "CA", 2);

   // Zipcode
   if (pSitus->Zip[0] == '9')
      memcpy(pOutbuf+OFF_S_ZIP, pSitus->Zip, SIZ_S_ZIP);

   // Legal
   if (pSitus->Tract[0] > ' ')
   {
      iTmp = sprintf(acTmp, "TRACT %.*s", ASIZ_SCL_TRACTNO, pSitus->Tract);
      memcpy(pOutbuf+OFF_LEGAL, acTmp, iTmp);
      // ASIZ_SCL_TRACTNO = SIZ_TRACT = 6
      memcpy(pOutbuf+OFF_TRACT, pSitus->Tract, ASIZ_SCL_TRACTNO);
   }
   lSitusMatch++;

   // Get next situs rec
   pRec = fgets(acRec, 1024, fdSitus);
   return 0;
}

/****************************** Scl_MergeSitusCsv ****************************
 *
 * Merge situs using "SF902CSV-2023-07-05.txt"
 *
 * Return 0 if successful, >0 if error
 *
 *****************************************************************************/

int Scl_MergeSitusCsv(char *pOutbuf)
{
   static   char acRec[1024], *pRec=NULL;
   char     acTmp[256], acAddr1[64], acAddr2[64], acCode[32], *apItems[20];
   long     lTmp;
   int      iTmp, iLoop, iCnt;

   // Get first rec for first call
   if (!pRec)
   {
      pRec = fgets(acRec, 1024, fdSitus);
      if (acRec[1] > '0')
         pRec = fgets(acRec, 1024, fdSitus);
   }

   do
   {
      if (!pRec)
      {
         fclose(fdSitus);
         fdSitus = NULL;
         return 1;      // EOF
      }

      // Compare Apn
      iLoop = memcmp(pOutbuf, pRec+1, RSIZ_SCL_APN);
      if (iLoop > 0)
      {
         if (bDebug)
            LogMsg0("Skip situs Addr rec  %.*s", RSIZ_SCL_APN, pRec+1);
         pRec = fgets(acRec, 1024, fdSitus);
         lSitusSkip++;
      }
   } while (iLoop > 0);

   // If not match, return
   if (iLoop)
      return 1;

   // Parse input
   iCnt = ParseStringNQ(pRec, cDelim, 20, apItems);
   if (iCnt < SCL_SIT_FLDS)
   {
      LogMsg("***** Error: bad situs record for APN=%.*s (#tokens=%d)", iApnLen, pOutbuf, iCnt);
      return -1;
   }

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "02314101", 9))
   //   lTmp = 0;
#endif

   // Initialize
   acAddr1[0] = 0;

   lTmp = atol(apItems[SCL_SIT_STRNUM]);
   if (lTmp > 0)
   {
      iTmp = sprintf(acTmp, "%d", lTmp);
      memcpy(pOutbuf+OFF_S_STRNUM, acTmp, iTmp);

      strcpy(acAddr1, apItems[SCL_SIT_STRNUM]);
      if (*apItems[SCL_SIT_STRSUB] > ' ')
      {
         vmemcpy(pOutbuf+OFF_S_STR_SUB, apItems[SCL_SIT_STRSUB], SIZ_S_STR_SUB);
         strcat(acAddr1, apItems[SCL_SIT_STRSUB]);
      }
      vmemcpy(pOutbuf+OFF_S_HSENO, acAddr1, SIZ_S_HSENO);

      if (*apItems[SCL_SIT_STRDIR] > ' ')
      {
         strcat(acAddr1, " ");
         strcat(acAddr1, apItems[SCL_SIT_STRDIR]);
         vmemcpy(pOutbuf+OFF_S_DIR, apItems[SCL_SIT_STRDIR], SIZ_S_DIR);
      }

      if (*apItems[SCL_SIT_STRNAME] > ' ')
      {
         // Make "WW" strType part of strName since we don't have it Suffix table
         if (!memcmp(apItems[SCL_SIT_STRTYPE], "WW", 2))
         {           
            sprintf(acTmp, "%s WALKWAY", apItems[SCL_SIT_STRNAME]);
            *apItems[SCL_SIT_STRTYPE] = 0;
         } else
            strcpy(acTmp, apItems[SCL_SIT_STRNAME]);

         vmemcpy(pOutbuf+OFF_S_STREET, acTmp, SIZ_S_STREET);
         strcat(acAddr1, " ");
         strcat(acAddr1, acTmp);
      }

      if (*apItems[SCL_SIT_STRTYPE] >= 'A')
      {
         strcat(acAddr1, " ");
         strcat(acAddr1, apItems[SCL_SIT_STRTYPE]);

         iTmp = GetSfxCodeX(apItems[SCL_SIT_STRTYPE], acTmp);
         if (iTmp > 0)
            Sfx2Code(acTmp, acCode);
         else
         {
            LogMsg0("*** Invalid suffix: %s, APN=%.12s", apItems[SCL_SIT_STRTYPE], pOutbuf);
            iBadSuffix++;
            memset(acCode, ' ', SIZ_S_SUFF);
         }
         memcpy(pOutbuf+OFF_S_SUFF, acCode, SIZ_S_SUFF);
      }

      if (*apItems[SCL_SIT_UNITNO] > ' ')
      {
         strcat(acAddr1, " ");
         strcat(acAddr1, apItems[SCL_SIT_UNITNO]);
         vmemcpy(pOutbuf+OFF_S_UNITNO, apItems[SCL_SIT_UNITNO], SIZ_S_UNITNO);
         vmemcpy(pOutbuf+OFF_S_UNITNOX, apItems[SCL_SIT_UNITNO], SIZ_S_UNITNOX);
      }

      vmemcpy(pOutbuf+OFF_S_ADDR_D, acAddr1, SIZ_S_ADDR_D);
   }


   // City
   acAddr2[0] = 0;
   if (*apItems[SCL_SIT_CITY_CD] > ' ')
   {
      Abbr2Code(apItems[SCL_SIT_CITY_CD], acCode, acAddr2);
      if (acCode[0] > ' ')
      {
         memcpy(pOutbuf+OFF_S_CITY, acCode, strlen(acCode));
         strcat(acAddr2, " CA");
      } else
         LogMsg("*** Unknown city: %s [%s]", apItems[SCL_SIT_CITY_CD], apItems[SCL_SIT_APN]);
   }

   // State
   memcpy(pOutbuf+OFF_S_ST, "CA", 2);

   // Zipcode
   if (*apItems[SCL_SIT_ZIP] == '9')
   {
      vmemcpy(pOutbuf+OFF_S_ZIP, apItems[SCL_SIT_ZIP], SIZ_S_ZIP);
      strcat(acAddr2, " ");
      strcat(acAddr2, apItems[SCL_SIT_ZIP]);
   }

   blankRem(acAddr2);
   vmemcpy(pOutbuf+OFF_S_CTY_ST_D, acAddr2, SIZ_S_CTY_ST_D);

   if (*apItems[SCL_SIT_TRACT] > ' ')
      vmemcpy(pOutbuf+OFF_TRACT, apItems[SCL_SIT_TRACT], SIZ_TRACT);

   lSitusMatch++;

   // Get next situs rec
   pRec = fgets(acRec, 1024, fdSitus);
   return 0;
}

/******************************** Scl_ExtrLien ******************************
 *
 * 
 ****************************************************************************/

int Scl_ExtrLien()
{
   char  acRec[2400], acOutbuf[256], acTmp[256], acTmpFile[_MAX_PATH];
   char  acLienExtr[_MAX_PATH], acPrevApn[16], *pTmp;

   FILE     *fdExtr;
   LIENEXTR *pLienExtr = (LIENEXTR *)&acOutbuf;
   SCL_ROLL *pRec = (SCL_ROLL *)acRec;

   LogMsg("Extract lien values");

   // Open Roll file
   LogMsg("Open Roll file %s", acRollFile);
   fdRoll = fopen(acRollFile, "r");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", acRollFile);
      return -1;
   }

   // Open extr file
   sprintf(acTmpFile, acLienTmpl, myCounty.acCntyCode, "tmp");
   LogMsg("Open Lien extract file %s", acTmpFile);
   fdExtr = fopen(acTmpFile, "w");
   if (fdExtr == NULL)
   {
      LogMsg("***** Error creating extract file: %s\n", acTmpFile);
      return -2;
   }

   long     lLand, lImpr, lPP, lFixt, lTmp, iTmp, lCnt, lSkip, lSameApn;
   LONGLONG lGross, lOther, lTotalExe;

   acPrevApn[0] = 0;
   lSameApn = lSkip = lCnt = 0;

   while (!feof(fdRoll))
   {
      pTmp = fgets(acRec, 2400, fdRoll);
      if (!pTmp)
         break;

      if (!memcmp(acPrevApn, pRec->Apn, iApnLen))
      {
         LogMsg("Same APN: %.12s", acRec);
         lSameApn++;
         continue;
      }

      // Reset output buffer
      memset(acOutbuf, ' ', sizeof(LIENEXTR));

      // Save current APN
      memcpy(acPrevApn, pRec->Apn, iApnLen);
      memcpy(pLienExtr->acApn, pRec->Apn, iApnLen);

      // TRA
      lTmp = atoin(pRec->TRA, RSIZ_SCL_TRA);
      if (lTmp > 0)
      {
         iTmp = sprintf(acTmp, "%.6d", lTmp);
         memcpy(pLienExtr->acTRA, acTmp, iTmp);
      }

      // Land
      lLand = atoin(pRec->Land, RSIZ_SCL_LAND);
      if (lLand > 0)
      {
         sprintf(acTmp, "%*u", SIZ_LAND, lLand);
         memcpy(pLienExtr->acLand, acTmp, SIZ_LAND);
      }
#ifdef _DEBUG
      //if (!memcmp(pLienExtr->acApn, "01534101", 8))
      //   lTmp = 1;
#endif

      // Structure impr - Fixt included
      lImpr = atoin(pRec->Impr, RSIZ_SCL_IMPR);
      // Personal property/Business inventory
      lPP   = atoin(pRec->PP_Val, RSIZ_SCL_PP_VAL);
      // Fixture - Not use.  This value is already included in Impr
      lFixt = atoin(pRec->Fixt, RSIZ_SCL_FIXT);

      if (lImpr > lFixt)
      {
         lImpr -= lFixt;
         sprintf(acTmp, "%*u", SIZ_LAND, lImpr);
         memcpy(pLienExtr->acImpr, acTmp, SIZ_LAND);
      } else 
         lImpr = 0;
      
      // Other impr
      lOther = lPP + lFixt;
      if (lOther > 0)
      {
         sprintf(acTmp, "%*u", SIZ_LIEN_LAND, lOther);
         memcpy(pLienExtr->acOther, acTmp, SIZ_LIEN_LAND);
         if (lPP > 0)
         {
            sprintf(acTmp, "%*u", SIZ_LIEN_LAND, lPP);
            memcpy(pLienExtr->acPP_Val, acTmp, SIZ_LIEN_LAND);
         }
         if (lFixt > 0)
         {
            sprintf(acTmp, "%*u", SIZ_LIEN_LAND, lFixt);
            memcpy(pLienExtr->acME_Val, acTmp, SIZ_LIEN_LAND);
         }
      }

      // Gross
      lGross = lOther + lLand + lImpr;
      if (lGross > 0)
      {
         sprintf(acTmp, "%*u", SIZ_LIEN_LAND, lGross);
         memcpy(pLienExtr->acGross, acTmp, SIZ_LIEN_LAND);
      }

      // Impr - Ratio
      if (lImpr > 0)
      {
         sprintf(acTmp, "%*u", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
         memcpy(pLienExtr->acRatio, acTmp, SIZ_RATIO);
      }

      // HO Exempt
      lTmp = atoin(pRec->HOExe, RSIZ_SCL_HO_EXE);
      if (lTmp > 0)
         pLienExtr->acHO[0] = '1';      // 'Y'
      else
         pLienExtr->acHO[0] = '2';      // 'N'

      // Parcel status
      pLienExtr->extra.Scl.ParcelFlag1 = pRec->Parcel_Flag1;
      pLienExtr->extra.Scl.ParcelFlag2 = pRec->Parcel_Flag2;

      if (pRec->Parcel_Flag2 == 'P' || pRec->Parcel_Flag2 == 'S')
         pLienExtr->SpclFlag = LX_FULLEXE_FLG;

      // Exempt total
      lTotalExe = atoin(pRec->TotalExe, RSIZ_SCL_EXEAMT);
      if (lTotalExe > 0)
      {
         if (lTotalExe > lGross)
            lTotalExe = lGross;
         sprintf(acTmp, "%*u", SIZ_EXE_TOTAL, lTotalExe);
         memcpy(pLienExtr->acExAmt, acTmp, SIZ_EXE_TOTAL);
      }

      pLienExtr->LF[0] = '\n';
      pLienExtr->LF[1] = 0;

      // Write to file
      fputs(acOutbuf, fdExtr);

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdRoll)
      fclose(fdRoll);
   if (fdExtr)
      fclose(fdExtr);

   LogMsg("Total records output:     %d", lCnt);
   LogMsg("Total records same APN:   %d", lSameApn);
   LogMsg("Total records skip:       %d", lSkip);

   // Sort output file
   sprintf(acLienExtr, acLienTmpl, myCounty.acCntyCode, myCounty.acCntyCode);

   /* No need to sort since lien roll always sorted - remove this comment when needed
   sprintf(acTmp, "S(1,12,C,A) F(TXT) DUPO(1,12)");
   LogMsg("Sort lien extract output file %s --> %s", acTmpFile, acLienExtr);
   int iRet = sortFile(acTmpFile, acLienExtr, acTmp);
   if (iRet <= 0)
   {
      LogMsg("***** ERROR sorting lien extract file %s to %s", acTmpFile, acLienExtr);
      iRet = -1;
   } else
      LogMsg("Total records after sort: %d", iRet);
   */
   // Rename instead
   if (!_access(acLienExtr, 0))
      DeleteFile(acLienExtr);
   iTmp = rename(acTmpFile, acLienExtr);

   return iTmp;
}

/********************************** CreateSclRec *****************************
 *
 * Return 0 if successful, >0 if error
 *
 *****************************************************************************/

int Scl_MergeRoll(char *pOutbuf, char *pRollRec, int iLen, int iFlag)
{
   SCL_ROLL *pRec;
   char     *pTmp, acTmp[256], acTmp1[256];
   LONGLONG lTmp, lGross;
   int      iRet;

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "00301041", 8))
   //   lTmp = 0;
#endif
   // Replace tab char with 0
   pTmp = pRollRec;

   pRec = (SCL_ROLL *)pRollRec;
   if (iFlag & CREATE_R01)
   {
      // Clear output buffer
      memset(pOutbuf, ' ', iRecLen);

      // Start copying data
      memcpy(pOutbuf, pRec->Apn, RSIZ_SCL_APN);
      memcpy(pOutbuf+OFF_CO_NUM, "43SCLA", 6);

      // Format APN
      iRet = formatApn(pRec->Apn, acTmp, &myCounty);
      memcpy(pOutbuf+OFF_APN_D, acTmp, iRet);

      // Create MapLink
      iRet = formatMapLink(pRec->Apn, acTmp, &myCounty);
      memcpy(pOutbuf+OFF_MAPLINK, acTmp, iRet);

      // Create index map link
      if (getIndexPage(acTmp, acTmp1, &myCounty))
         memcpy(pOutbuf+OFF_IMAPLINK, acTmp1, iRet);

      // Year assessed
      memcpy(pOutbuf+OFF_YR_ASSD, myCounty.acYearAssd, 4);

      // Land
      long lLand = atoin(pRec->Land, RSIZ_SCL_LAND);
      // Improve
      long lImpr = atoin(pRec->Impr, RSIZ_SCL_IMPR);
      // Personal prop value
      long lPers = atoin(pRec->PP_Val, RSIZ_SCL_PP_VAL);
      // Fixture
      long lFixt = atoin(pRec->Fixt, RSIZ_SCL_FIXT);

      if (lLand > 0)
      {
         sprintf(acTmp, "%*u", SIZ_LAND, lLand);
         memcpy(pOutbuf+OFF_LAND, acTmp, SIZ_LAND);
      }

      if (lImpr > lFixt)
      {
         lImpr -= lFixt;
         sprintf(acTmp, "%*u", SIZ_IMPR, lImpr);
         memcpy(pOutbuf+OFF_IMPR, acTmp, SIZ_IMPR);
      } else
         lImpr = 0;

      lTmp = lPers + lFixt;
      if (lTmp > 0)
      {
         sprintf(acTmp, "%*u", SIZ_OTHER, lTmp);
         memcpy(pOutbuf+OFF_OTHER, acTmp, SIZ_OTHER);

         if (lPers > 0)
         {
            sprintf(acTmp, "%u         ", lPers);
            memcpy(pOutbuf+OFF_PERSPROP, acTmp, SIZ_PERSPROP);
         }
         if (lFixt > 0)
         {
            sprintf(acTmp, "%u         ", lFixt);
            memcpy(pOutbuf+OFF_FIXTR, acTmp, SIZ_FIXTR);
         }
      }

      // Gross total
      lGross = lTmp+lLand+lImpr;
      if (lGross > 0)
      {
         sprintf(acTmp, "%*u", SIZ_GROSS, lGross);
         memcpy(pOutbuf+OFF_GROSS, acTmp, SIZ_GROSS);
      }

      // Ratio
      if (lImpr > 0)
      {
         double dTmp = (LONGLONG)lImpr*100/(lLand+lImpr)+0.5;
         sprintf(acTmp, "%*u", SIZ_RATIO, (long)dTmp);

         memcpy(pOutbuf+OFF_RATIO, acTmp, SIZ_RATIO);
      }

      // HO Exempt
      ULONG lExe1 = atoln(pRec->HOExe, RSIZ_SCL_HO_EXE);
      if (lExe1 > 0)
      {
         *(pOutbuf+OFF_HO_FL) = '1';      // 'Y'
         *(pOutbuf+OFF_EXE_CD1) = '7';
      } else
      {
         *(pOutbuf+OFF_HO_FL) = '2';      // 'N'
         if (pRec->ExeType > ' ')
            *(pOutbuf+OFF_EXE_CD1) = pRec->ExeType;
      }

      // Exempt total
      lTmp = atoin(pRec->TotalExe, RSIZ_SCL_EXEAMT);
      if (lTmp > 0)
      {
         // Total exempt cannot be greater than total assessed
         if (lTmp > lGross)
         {
            LogMsg("* Overwrite EXE_TOTAL of %d with %d", lTmp, lGross);
            lTmp = lGross;
         }
         sprintf(acTmp, "%*u", SIZ_EXE_TOTAL, lTmp);
         memcpy(pOutbuf+OFF_EXE_TOTAL, acTmp, SIZ_EXE_TOTAL);
      }

      // Create exemption type
      makeExeType(pOutbuf+OFF_EXE_TYPE, pOutbuf+OFF_EXE_CD1, pOutbuf+OFF_EXE_CD2, pOutbuf+OFF_EXE_CD3, (IDX_TBL4 *)&SCL_Exemption);
   }

   // TRA
   lTmp = atoin(pRec->TRA, RSIZ_SCL_TRA);
   if (lTmp > 0)
   {
      iRet = sprintf(acTmp, "%.6d", lTmp);
      memcpy(pOutbuf+OFF_TRA, acTmp, iRet);
   }

   // Set full exemption flag
   if (pRec->Parcel_Flag2 == 'P' || pRec->Parcel_Flag2 == 'S')
      *(pOutbuf+OFF_FULL_EXEMPT) = 'Y';
   else
      *(pOutbuf+OFF_FULL_EXEMPT) = ' ';

   // Parcel Flags
   *(pOutbuf+OFF_PARCEL_FLG1) = pRec->Parcel_Flag1;
   *(pOutbuf+OFF_PARCEL_FLG2) = pRec->Parcel_Flag2;

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "72513009", iApnLen))
   //   lTmp = 0;
#endif
   // UseCode
   if (memcmp(pRec->UseCode, "00", 2) > 0)
   {
      memcpy(pOutbuf+OFF_USE_CO, pRec->UseCode, RSIZ_SCL_USECODE);
      updateStdUse(pOutbuf+OFF_USE_STD, pRec->UseCode, RSIZ_SCL_USECODE, pOutbuf);
   } else
      memcpy(pOutbuf+OFF_USE_STD, USE_UNASGN, SIZ_USE_STD);

   // Mailing
   Scl_MergeMAdr(pOutbuf, pRollRec);

   // Owner
   Scl_MergeOwner(pOutbuf, pRec->Name1, pRec->Name2);

   // Care Of
   if (pRec->CareOf[0] > ' ')
   {
      memcpy(acTmp, pRec->CareOf, SIZ_CARE_OF);
      acTmp[SIZ_CARE_OF] = 0;
      if ((pTmp = strchr(acTmp, '/')) || (pTmp = strchr(acTmp, '(')) )
         *pTmp = 0;
      //memcpy(pOutbuf+OFF_CARE_OF, acTmp, strlen(acTmp));
      updateCareOf(pOutbuf, acTmp, strlen(acTmp));
   }

   // YrDelq
   if (isdigit(pRec->Sold2St[0]) )
   {
      char acYear[8];

      dateConversion(pRec->Sold2St, acYear, YY2YYYY);
      memcpy(pOutbuf+OFF_DEL_YR, acYear, SIZ_DEL_YR);
   }

   // Acres

   // Zoning
   if (pRec->Zoning[0] > ' ')
   {
      memcpy(acTmp, pRec->Zoning, RSIZ_SCL_ZONING);
      acTmp[RSIZ_SCL_ZONING] = 0;
      memcpy(pOutbuf+OFF_ZONE, _strupr(acTmp), RSIZ_SCL_ZONING);
      if (*(pOutbuf+OFF_ZONE_X1) == ' ')
         memcpy(pOutbuf+OFF_ZONE_X1, acTmp, RSIZ_SCL_ZONING);
   }

   // Transfer
   if (pRec->DocNum[0] > ' ')
   {
      char acDate[16];

      pTmp = dateConversion(pRec->DocDate, acDate, MMDDYY2);
      if (pTmp)
      {
         memcpy(pOutbuf+OFF_TRANSFER_DT, acDate, SIZ_TRANSFER_DT);
         memcpy(pOutbuf+OFF_TRANSFER_DOC, pRec->DocNum, RSIZ_SCL_DOCNUM);
      }
   }

   return 0;
}

/********************************** Scl_Load_Roll ******************************
 *
 *
 ******************************************************************************/

int Scl_Load_Roll(int iSkip)
{
   char     *pTmp, acRec[MAX_RECSIZE], acBuf[MAX_RECSIZE], acRollRec[MAX_RECSIZE];
   char     acRawFile[_MAX_PATH], acOutFile[_MAX_PATH];
   char     acSitusFile[_MAX_PATH];

   HANDLE   fhIn, fhOut;

   int      iRet, iTmp, iRollUpd=0, iNewRec=0, iRetiredRec=0;
   DWORD    nBytesRead;
   DWORD    nBytesWritten;
   BOOL     bRet, bEof;
   long     lRet=0, lCnt=0;

   sprintf(acRawFile, acRawTmpl, "SOL", "SOL", "S01");
   sprintf(acOutFile, acRawTmpl, "SOL", "SOL", "R01");

   // Rename files for processing
   if (_access(acRawFile, 0))
   {
      if (!_access(acOutFile, 0))
         rename(acOutFile, acRawFile);
      else
      {
         LogMsg("Missing input file %s.  Please recheck!", acRawFile);
         return 1;
      }
   }

   // Open Roll file
   LogMsg("Open Roll file %s", acRollFile);
   fdRoll = fopen(acRollFile, "r");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", acRollFile);
      return 2;
   }

   // Open Situs file
   GetIniString(myCounty.acCntyCode, "SitusFile", "", acSitusFile, _MAX_PATH, acIniFile);
   if (_access(acSitusFile, 0))
   {
      LogMsg("***** Error missing Situs file: %s\n", acSitusFile);
      return 1;
   }

   LogMsg("Open Situs file %s", acSitusFile);
   fdSitus = fopen(acSitusFile, "r");
   if (fdSitus == NULL)
   {
      LogMsg("***** Error opening Situs file: %s\n", acSitusFile);
      return 2;
   }

   // Open Input file
   LogMsg("Open input file %s", acRawFile);
   fhIn = CreateFile(acRawFile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhIn == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening raw file: %s\n", acRawFile);
      return 3;
   }

   // Open Output file
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error creating output file: %s\n", acOutFile);
      return 4;
   }

   // Get first RollRec
   pTmp = fgets((char *)&acRollRec[0], iRollLen, fdRoll);
   bEof = (pTmp ? false:true);

   // Copy skip record
   memset(acBuf, ' ', iRecLen);
   while (iSkip-- > 0)
   {
      ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);
      // Ignore record start with 99999999
      //if (memcmp(acBuf, "99999999", 8))
         bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
   }

   iNoMatch=iBadCity=iBadSuffix=0;

   // Merge loop
   while (!bEof)
   {
      bRet = ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);

      // Check for EOF
      if (!bRet)
      {
         LogMsg("Error reading input file %s (%f)", acRawFile, GetLastError());
         break;
      }

      // EOF ?
      if (!nBytesRead)
         break;

      NextRollRec:
      iTmp = memcmp(acBuf, acRollRec, iApnLen);
      if (!iTmp)
      {
         // Merge roll data
         iRet = Scl_MergeRoll(acBuf, acRollRec, iRollLen, UPDATE_R01);
         iRollUpd++;

         // Merge Situs
         if (fdSitus)
            iRet = Scl_MergeSitus(acBuf);

         // Read next roll record
         pTmp = fgets(acRollRec, iRollLen, fdRoll);

         if (!pTmp)
            bEof = true;    // Signal to stop
      } else if (iTmp > 0)       // Roll not match, new roll record?
      {
         LogMsg0("*** New roll record : %.*s (%d)", iApnLen, acRollRec, lCnt);

         // Create new R01 record
         iRet = Scl_MergeRoll(acRec, acRollRec, iRollLen, CREATE_R01);
         iNewRec++;

         // Merge Situs
         if (fdSitus)
            iRet = Scl_MergeSitus(acRec);

         bRet = WriteFile(fhOut, acRec, iRecLen, &nBytesWritten, NULL);
         lCnt++;

         // Get next roll record
         pTmp = fgets(acRollRec, iRollLen, fdRoll);

         if (!pTmp)
            bEof = true;    // Signal to stop
         else
            goto NextRollRec;
      } else
      {
         // Record may be retired
         if (bDebug)
            LogMsg0("***** Roll not match (retired record?) : R01->%.*s < Roll->%.*s (%d) *****", iApnLen, acBuf, iApnLen, acRollRec, lCnt);
         iRetiredRec++;
         continue;
      }

      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);

      if (!bRet)
      {
         LogMsg("Error occurs: %d\n", GetLastError());
         lRet = WRITE_ERR;
         break;
      }
   }

   // If there are more new records, add them
   while (!bEof)
   {
      LogMsg0("*** New roll record : %.*s (%d)", iApnLen, acRollRec, lCnt);

      // Create new R01 record
      iRet = Scl_MergeRoll(acRec, acRollRec, iRollLen, CREATE_R01);
      iNewRec++;

      // Merge Situs
      if (fdSitus)
         iRet = Scl_MergeSitus(acRec);

      bRet = WriteFile(fhOut, acRec, iRecLen, &nBytesWritten, NULL);
      lCnt++;

      // Get next roll record
      pTmp = fgets(acRollRec, iRollLen, fdRoll);

      if (!pTmp)
         bEof = true;    // Signal to stop
   }

   // Close files
   if (fdRoll)
      fclose(fdRoll);
   if (fdSitus)
      fclose(fdSitus);
   if (fhOut)
      CloseHandle(fhOut);
   if (fhIn)
      CloseHandle(fhIn);

   LogMsg("Total output records:       %u", lCnt);
   LogMsg("Total new records:          %u", iNewRec);
   LogMsg("Total retired records:      %u", iRetiredRec);
   LogMsg("Total bad-city records:     %u", iBadCity);
   LogMsg("Total bad-suffix records:   %u", iBadSuffix);

   printf("\nTotal output records: %u", lCnt);

   lRecCnt = lCnt;
   return 0;
}

/********************************* Scl_Load_LDR ****************************
 *
 *
 ****************************************************************************/

int Scl_Load_LDR(int iFirstRec /* 1=create header rec */)
{
   char     *pTmp, acBuf[MAX_RECSIZE], acRec[MAX_RECSIZE], acLglFile[MAX_RECSIZE],
            acSitusFile[_MAX_PATH], acOutFile[_MAX_PATH];

   HANDLE   fhOut;

   int      iRet;
   DWORD    nBytesWritten;
   BOOL     bRet;
   long     lRet, lCnt=0;

   // Init variables
   iNoMatch=iBadCity=iBadSuffix=0;

   // Open roll file
   LogMsg("Open Roll file %s", acRollFile);
   fdRoll = fopen(acRollFile, "r");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", acRollFile);
      return 2;
   }

   // Open Situs file
   GetIniString(myCounty.acCntyCode, "SitusFile", "", acSitusFile, _MAX_PATH, acIniFile);
   if (_access(acSitusFile, 0))
   {
      LogMsg("***** Error missing Situs file: %s\n", acSitusFile);
      return 1;
   }

   LogMsg("Open Situs file %s", acSitusFile);
   fdSitus = fopen(acSitusFile, "r");
   if (fdSitus == NULL)
   {
      LogMsg("***** Error opening Situs file: %s\n", acSitusFile);
      return 2;
   }

   // Open Char file
   fdChar = fopen(acCChrFile, "r");
   if (fdChar == NULL)
   {
      LogMsg("***** Error opening Char file: %s\n", acCChrFile);
      return -2;
   }

   // Open Legal file
   GetIniString("Data", "LegalFile", "", acBuf, _MAX_PATH, acIniFile);
   sprintf(acLglFile, acBuf, myCounty.acCntyCode, myCounty.acCntyCode, "Dat");
   LogMsg("Open Legal file %s", acLglFile);
   fdLegal = fopen(acLglFile, "r");
   if (fdLegal == NULL)
   {
      LogMsg("***** Error opening Legal file: %s\n", acLglFile);
      return -2;
   }

   // Open Output file
   sprintf(acOutFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "R01");
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error creating output file: %s\n", acOutFile);
      return 4;
   }

   // Output first header record
   if (iFirstRec > 0)
   {
      memset(acBuf, '9', iRecLen);
      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
   }

   // Get 1st rec
   pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdRoll);
   iRet = replCharEx(acRec, 31, ' ', iRollLen);

   // Merge loop
   while (pTmp && !feof(fdRoll))
   {
      // Create new R01 record
      iRet = Scl_MergeRoll(acBuf, acRec, iRollLen, CREATE_R01);

      if (!iRet)
      {
         // Merge Situs
         if (fdSitus)
            iRet = Scl_MergeSitusCsv(acBuf);

         // Merge Char
         if (fdChar)
            lRet = Scl_MergeChar(acBuf);

         // Merge Char
         if (fdLegal)
            lRet = Scl_MergeLegal(acBuf);

         // Save last recording date
         lRet = atoin((char *)&acBuf[OFF_TRANSFER_DT], 8);
         if (lRet > lLastRecDate && lRet < lToday)
            lLastRecDate = lRet;

         lLDRRecCount++;
         bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
         if (!bRet)
         {
            LogMsg("Error writing to output file at record %d\n", lCnt);
            lRet = WRITE_ERR;
            break;
         }
      }

      // Get next roll record
      pTmp = fgets(acRec, MAX_RECSIZE, fdRoll);
      iRet = replCharEx(acRec, 31, ' ', iRollLen);
      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdLegal)
      fclose(fdLegal);
   if (fdRoll)
      fclose(fdRoll);
   if (fdSitus)
      fclose(fdSitus);
   if (fdChar)
      fclose(fdChar);
   if (fhOut)
      CloseHandle(fhOut);

   LogMsg("Total input records:        %u", lCnt);
   LogMsg("Total output records:       %u", lLDRRecCount);
   LogMsg("Total bad-city records:     %u", iBadCity);
   LogMsg("Total bad-suffix records:   %u\n", iBadSuffix);
   LogMsg("Total Char matched:         %u", lCharMatch);
   LogMsg("Total Char skipped:         %u\n", lCharSkip);
   LogMsg("Total Situs matched:        %u", lSitusMatch);
   LogMsg("Total Situs skipped:        %u\n", lSitusSkip);
   LogMsg("Total Legal matched:        %u", lLegalMatch);
   LogMsg("Total Legal skipped:        %u\n", lLegalSkip);

   LogMsg("Last recording date:        %u", lLastRecDate);
   printf("\nTotal output records: %u", lLDRRecCount);

   lRecCnt = lLDRRecCount;
   return 0;
}

/******************************** Scl_ExtrChar ******************************
 *
 ****************************************************************************/

int SF_FormatStdChar(char *pCharbuf)
{
   char     *pTmp, acTmp[_MAX_PATH], acCode[16];
   double   dTmp;
   int      iTmp, iSqft;
   STDCHAR  *pCharRec=(STDCHAR *)pCharbuf;

   // APN
   memcpy(pCharRec->Apn, apTokens[SF_APN], strlen(apTokens[SF_APN]));

#ifdef _DEBUG
   //if (!memcmp(pCharRec->Apn, "1858506000", 10) )
   //   iTmp = 0;
#endif

   // Building#
   iTmp = atol(apTokens[SF_BUILDING_NUM]);
   if (iTmp > 0)
   {
      iTmp = sprintf(acTmp, "%d", iTmp);
      memcpy(pCharRec->BldgSeqNo, acTmp, iTmp);
   }

   // Date updated
   memcpy(pCharRec->Date_Updated, apTokens[SF_DATE_UPDATED], strlen(apTokens[SF_DATE_UPDATED]));

   // Zoning
   if (*apTokens[SF_ZONING_CODE] > ' ')
      vmemcpy(pCharRec->Zoning, apTokens[SF_ZONING_CODE], SIZ_CHAR_ZONING);

   // Acreage
   dTmp = atof(apTokens[SF_LAND_ACRES]);
   if (dTmp > 0.0)
   {
      iTmp = sprintf(acTmp, "%d", (long)(dTmp*1000));
      memcpy(pCharRec->LotAcre, acTmp, iTmp);
   }

   // LotSqft
   iSqft = atol(apTokens[SF_USABLE_SQ_FEET]);
   if (iSqft > 0)
   {
      iTmp = sprintf(acTmp, "%d", iSqft);
      memcpy(pCharRec->LotSqft, acTmp, iTmp);
   }

   // YrBlt
   iTmp = atol(apTokens[SF_YEAR_BUILT]);
   if (iTmp > 1900 && iTmp <= lToyear)
      memcpy(pCharRec->YrBlt, apTokens[SF_YEAR_BUILT], 4);

   // Eff year
   iTmp = atol(apTokens[SF_EFFECTIVE_YEAR]);
   if (iTmp > 1900 && iTmp <= lToyear)
      memcpy(pCharRec->YrEff, apTokens[SF_EFFECTIVE_YEAR], 4);

   // Rooms
   iTmp = atol(apTokens[SF_TOTAL_ROOMS]);
   if (iTmp > 0)
      memcpy(pCharRec->Rooms, apTokens[SF_TOTAL_ROOMS], strlen(apTokens[SF_TOTAL_ROOMS]));

   // Beds
   iTmp = atol(apTokens[SF_BEDROOM]);
   if (iTmp > 0)
      memcpy(pCharRec->Beds, apTokens[SF_BEDROOM], strlen(apTokens[SF_BEDROOM]));

   // Baths - 1.5, 9.9, ...
   int iBath = atol(apTokens[SF_BATH_ROOMS]);
   if ((pTmp = strchr(apTokens[SF_BATH_ROOMS], '.')))
   {     
      pCharRec->Bath_4Q[0] = iBath|0x30;

      // After decimal point: 1, 2, 3, 4, 5, 6, 7, 8, 9
      // We can make BATH_1Q <=3, Bath_2Q <= 6, BATH_3Q <=9
      if (*(pTmp+1) > '0' && *(pTmp+1) <= '9')
      {
         pCharRec->HBaths[0] = '1';
         if (*(pTmp+1) <= '3')
            pCharRec->Bath_1Q[0] = '1';
         else if (*(pTmp+1) <= '6')
            pCharRec->Bath_2Q[0] = '1';
         else 
            pCharRec->Bath_3Q[0] = '1';
      }

      if (iBath > 0)
      {
         iTmp = sprintf(acTmp, "%d", iBath);
         memcpy(pCharRec->FBaths, acTmp, iTmp);
      }
   }

   // Dining room
   // Family room
   // Util room

   // Water
   // Sewer

   // View
   //if (*apTokens[SF_HILLSIDE_FLAG] == 'Y')
   //   pCharRec->View[0] = 'Y';

   // Quality - BldgCls
   pTmp = apTokens[SF_QUALITY_CLASS];
   if (isalpha(*pTmp))
   {  // D45A
      memcpy(pCharRec->QualityClass, pTmp, strlen(pTmp));

      pCharRec->BldgClass = *pTmp++;
      iTmp = atoin(pTmp, 3);
      if (iTmp > 0)
      {
         sprintf(acTmp, "%.1f", (double)iTmp/10.0);
         //sprintf(acTmp, "%d", iTmp);
         iTmp = Value2Code(acTmp, acCode, NULL);
         if (iTmp >= 0)
            pCharRec->BldgQual = acCode[0]; 
         else
            iTmp = 0;      // Testing
      }
   }

   // Condition
   if (*apTokens[SF_CONDITION_CODE] > ' ')
   {
      if (strchr("AGFEPN", *apTokens[SF_CONDITION_CODE]))
         pCharRec->ImprCond[0] = *apTokens[SF_CONDITION_CODE];
      else
         LogMsg("SF - Unknown Condition [%s] APN=%.10s", apTokens[SF_CONDITION_CODE], apTokens[SF_APN]);
   }

   // Fire place
   //memcpy(pCharRec->Fireplace, apTokens[SF_FIREPL], strlen(apTokens[SF_FIREPL]));

   // Air/Heating
   switch (*apTokens[SF_HEAT_AIR_COND])
   {
      case 'A':
         pCharRec->Cooling[0] = 'U';
         break;
      case 'H':
         pCharRec->Heating[0] = 'Y';
         break;
      case 'B':   // Both
         pCharRec->Cooling[0] = 'U';
         pCharRec->Heating[0] = 'Y';
         break;
      case 'N':   // None
         pCharRec->Cooling[0] = 'N';
         pCharRec->Heating[0] = 'L';
         break;
   }

   // Pool/Spa
   switch (*apTokens[SF_POOL_SAP_CODE])
   {
      case 'P':   // Pool
         pCharRec->Pool[0] = 'P';
         break;
      case 'S':   // Spa
         pCharRec->Pool[0] = 'S';
         break;
      case 'B':   // Both
         pCharRec->Pool[0] = 'C';
         break;
      case 'N':   // None
         pCharRec->Pool[0] = 'N';
         break;
   }

   // Bldg Sqft
   iSqft = atol(apTokens[SF_TOTAL_AREA]);
   if (iSqft > 0)
      memcpy(pCharRec->BldgSqft, apTokens[SF_TOTAL_AREA], strlen(apTokens[SF_TOTAL_AREA]));

   // SqftFlr1
   iTmp = 0;
   int iFlr1 = atol(apTokens[SF_FIRST_FLOOR_AREA]);
   if (iFlr1 > 0)
   {
      memcpy(pCharRec->Sqft_1stFl, apTokens[SF_FIRST_FLOOR_AREA], strlen(apTokens[SF_FIRST_FLOOR_AREA]));
      iTmp = 1;
   }

   // SqftFlr2
   int iFlr2 = atol(apTokens[SF_SECOND_FLR_AREA]);
   if (iFlr2 > 0)
   {
      memcpy(pCharRec->Sqft_2ndFl, apTokens[SF_SECOND_FLR_AREA], strlen(apTokens[SF_SECOND_FLR_AREA]));
      iTmp = 2;
   }

   // SqftFlr3
   int iFlr3 = atol(apTokens[SF_THIRD_FLOOR_AREA]);
   if (iFlr3 > 0)
   {
      memcpy(pCharRec->Sqft_Above2nd, apTokens[SF_THIRD_FLOOR_AREA], strlen(apTokens[SF_THIRD_FLOOR_AREA]));
      iTmp = 3;
   }

   // Stories
   if (iTmp > 0)
   {
      iTmp = sprintf(acTmp, "%d.0", iTmp);
      memcpy(pCharRec->Stories, acTmp, iTmp);
   }

   // Misc Sqft
   iSqft = atol(apTokens[SF_ADDITION_AREA]);
   if (iSqft > 0)
      memcpy(pCharRec->MiscSqft, apTokens[SF_ADDITION_AREA], strlen(apTokens[SF_ADDITION_AREA]));

   // Basement Sqft
   iSqft = atol(apTokens[SF_BASEMENT_AREA]);
   if (iSqft > 0)
      memcpy(pCharRec->BsmtSqft, apTokens[SF_BASEMENT_AREA], strlen(apTokens[SF_BASEMENT_AREA]));

   // Garage Sqft
   iSqft = atol(apTokens[SF_GARAGE_AREA]);
   if (iSqft > 0)
      memcpy(pCharRec->GarSqft, apTokens[SF_GARAGE_AREA], strlen(apTokens[SF_GARAGE_AREA]));

   // Garage type
   switch (*apTokens[SF_GARAGE_PORT])
   {
      case 'G':   // Garage
         pCharRec->ParkType[0] = 'Z';
         break;
      case 'P':   // Carport
         pCharRec->ParkType[0] = 'C';
         break;
      case 'B':   // Both
         pCharRec->ParkType[0] = '2';
         break;
      case 'N':   // None
         pCharRec->ParkType[0] = 'H';
         break;
   }

   pCharRec->CRLF[0] = '\n';
   pCharRec->CRLF[1] = '\0';

   return 0;
}

int MF_FormatStdChar(char *pCharbuf)
{
   char     *pTmp, acTmp[_MAX_PATH], acCode[16];
   double   dTmp;
   int      iTmp, iSqft;
   STDCHAR  *pCharRec=(STDCHAR *)pCharbuf;

   // APN
   memcpy(pCharRec->Apn, apTokens[MF_APN], strlen(apTokens[MF_APN]));

#ifdef _DEBUG
   //if (!memcmp(pCharRec->Apn, "1858506000", 10) )
   //   iTmp = 0;
#endif

   // Building#
   iTmp = atol(apTokens[MF_BUILDING_NUM]);
   if (iTmp > 0)
   {
      iTmp = sprintf(acTmp, "%d", iTmp);
      memcpy(pCharRec->BldgSeqNo, acTmp, iTmp);
   }

   // Date updated
   memcpy(pCharRec->Date_Updated, apTokens[MF_DATE_UPDATED], strlen(apTokens[MF_DATE_UPDATED]));

   // Acreage
   dTmp = atof(apTokens[MF_LAND_ACRES]);
   if (dTmp > 0.0)
   {
      iTmp = sprintf(acTmp, "%d", (long)(dTmp*1000));
      memcpy(pCharRec->LotAcre, acTmp, iTmp);
   }

   // LotSqft
   iSqft = atol(apTokens[MF_USABLE_SQ_FEET]);
   if (iSqft > 0)
   {
      iTmp = sprintf(acTmp, "%d", iSqft);
      memcpy(pCharRec->LotSqft, acTmp, iTmp);
   }

   // Zoning
   if (*apTokens[MF_ZONING_CODE] > ' ')
      vmemcpy(pCharRec->Zoning, apTokens[MF_ZONING_CODE], SIZ_CHAR_ZONING);

   // Gas
   if (*apTokens[MF_GAS_FLAG] > ' ')
      pCharRec->HasGas = *apTokens[MF_GAS_FLAG];

   // Electric
   if (*apTokens[MF_ELECTRIC_FLAG] > ' ')
      pCharRec->HasElectric = *apTokens[MF_ELECTRIC_FLAG];

   // Water
   if (*apTokens[MF_WATER_FLAG] > ' ')
      pCharRec->HasWater = *apTokens[MF_WATER_FLAG];

   // Garbage
   // Cable TV
   // Patio/Balcony
   // Laundry
   // Dishwasher
   // Sauna
   // Tennis
   // Rec room
   // Lake/Stream

   // Cover parking
   int iCoverSpace = atol(apTokens[MF_COV_PARKING]);
   if (iCoverSpace > 0)
   {
      vmemcpy(pCharRec->ParkSpace, apTokens[MF_COV_PARKING], SIZ_CHAR_SIZE4);
      pCharRec->ParkType[0] = 'Z';     // GARAGE
   }
   
   // Open parking
   int iOpenSpace = atol(apTokens[MF_OPEN_PARKING]);
   if (iOpenSpace > 0)
   {
      iTmp = sprintf(acTmp, "%d", iCoverSpace+iOpenSpace);
      vmemcpy(pCharRec->ParkSpace, acTmp, SIZ_CHAR_SIZE4, iTmp);
      if (!iCoverSpace)
         pCharRec->ParkType[0] = 'C';  // CARPORT
      else
         pCharRec->ParkType[0] = '2';  // GARAGE/CARPORT
   }

   // YrBlt
   iTmp = atol(apTokens[MF_YEAR_BUILT]);
   if (iTmp > 1900 && iTmp <= lToyear)
      memcpy(pCharRec->YrBlt, apTokens[MF_YEAR_BUILT], 4);

   // Eff year
   iTmp = atol(apTokens[MF_EFFECTIVE_YEAR]);
   if (iTmp > 1900 && iTmp <= lToyear)
      memcpy(pCharRec->YrEff, apTokens[MF_EFFECTIVE_YEAR], 4);

   // Quality - BldgCls
   pTmp = apTokens[MF_QUALITY_CLASS];
   if (isalpha(*pTmp))
   {  // D45A
      memcpy(pCharRec->QualityClass, pTmp, strlen(pTmp));

      pCharRec->BldgClass = *pTmp++;
      iTmp = atoin(pTmp, 3);
      if (iTmp > 0)
      {
         sprintf(acTmp, "%.1f", (double)iTmp/10.0);
         iTmp = Value2Code(acTmp, acCode, NULL);
         if (iTmp >= 0)
            pCharRec->BldgQual = acCode[0]; 
         else
            iTmp = 0;      // Testing
      }
   }

   // Condition
   if (*apTokens[MF_CONDITION_CODE] > ' ')
   {
      if (strchr("AGFEPN", *apTokens[MF_CONDITION_CODE]))
         pCharRec->ImprCond[0] = *apTokens[MF_CONDITION_CODE];
      else
         LogMsg("MF - Unknown Condition [%s] APN=%.10s", apTokens[SF_CONDITION_CODE], apTokens[MF_APN]);
   }

   // Fire place
   if (*apTokens[MF_FIREPLACE_FLAG] > ' ')
      pCharRec->Fireplace[0] = *apTokens[MF_FIREPLACE_FLAG];

   // Elevator
   if (*apTokens[MF_ELEVATOR_FLAG] > ' ')
      pCharRec->HasElevator = *apTokens[MF_ELEVATOR_FLAG];

   // Heating
   if (*apTokens[MF_CENT_HEAT_FLAG] == 'Y')
      pCharRec->Heating[0] = 'Z';
   else if (*apTokens[MF_CENT_HEAT_FLAG] == 'N')
      pCharRec->Heating[0] = 'L';

   // Cooling
   if (*apTokens[MF_AIR_COND_FLAG] == 'Y')
      pCharRec->Cooling[0] = 'U';
   else if (*apTokens[MF_AIR_COND_FLAG] == 'N')
      pCharRec->Cooling[0] = 'N';

   // Pool
   if (*apTokens[MF_POOL_FLAG] == 'Y')
      pCharRec->Pool[0] = 'P';
   else if (*apTokens[MF_POOL_FLAG] == 'N')
      pCharRec->Pool[0] = 'N';

   // Bldg Sqft
   iSqft = atol(apTokens[MF_TOTAL_AREA]);
   if (iSqft > 0)
      memcpy(pCharRec->BldgSqft, apTokens[MF_TOTAL_AREA], strlen(apTokens[MF_TOTAL_AREA]));

   // Units
   iTmp = atol(apTokens[MF_NUMBER_UNITS]);
   if (iTmp > 0)
   {
      iTmp = sprintf(acTmp, "%d", iTmp);
      memcpy(pCharRec->Units, acTmp, iTmp);
   }

   // Lease area
   if (*apTokens[MF_LEASE_AREA] > '0')
      memcpy(pCharRec->LeaseArea, apTokens[MF_LEASE_AREA], strlen(apTokens[MF_LEASE_AREA]));

   // Stories
   iTmp = atol(apTokens[MF_NUMBER_FLOORS]);
   if (iTmp > 0)
   {
      iTmp = sprintf(acTmp, "%d.0", iTmp);
      memcpy(pCharRec->Stories, acTmp, iTmp);
   }

   // Remark
   iTmp = blankRem(apTokens[MF_REMARKS]);
   vmemcpy(pCharRec->Misc.Comment, apTokens[MF_REMARKS], SIZ_CHAR_COMMENTS, iTmp);

   pCharRec->CRLF[0] = '\n';
   pCharRec->CRLF[1] = '\0';

   return 0;
}

int CI_FormatStdChar(char *pCharbuf)
{
   char     *pTmp, acTmp[_MAX_PATH], acCode[16];
   double   dTmp;
   int      iTmp, iSqft;
   STDCHAR  *pCharRec=(STDCHAR *)pCharbuf;

   // APN
   memcpy(pCharRec->Apn, apTokens[CI_APN], strlen(apTokens[CI_APN]));

#ifdef _DEBUG
   //if (!memcmp(pCharRec->Apn, "1858506000", 10) )
   //   iTmp = 0;
#endif

   // Building#
   iTmp = atol(apTokens[CI_BUILDING_NUM]);
   if (iTmp > 0)
   {
      iTmp = sprintf(acTmp, "%d", iTmp);
      memcpy(pCharRec->BldgSeqNo, acTmp, iTmp);
   }
   
   // Date updated
   memcpy(pCharRec->Date_Updated, apTokens[CI_DATE_UPDATED], strlen(apTokens[CI_DATE_UPDATED]));

   // Acreage
   dTmp = atof(apTokens[CI_LAND_ACRES]);
   if (dTmp > 0.0)
   {
      iTmp = sprintf(acTmp, "%d", (long)(dTmp*1000));
      memcpy(pCharRec->LotAcre, acTmp, iTmp);
   }

   // LotSqft
   iSqft = atol(apTokens[CI_USABLE_SQ_FEET]);
   if (iSqft > 0)
   {
      iTmp = sprintf(acTmp, "%d", iSqft);
      memcpy(pCharRec->LotSqft, acTmp, iTmp);
   }

   // Units
   iTmp = atol(apTokens[CI_NUMBER_UNITS]);
   if (iTmp > 0)
   {
      iTmp = sprintf(acTmp, "%d", iTmp);
      memcpy(pCharRec->Units, acTmp, iTmp);
   }

   // Lease area
   if (*apTokens[CI_LEASE_AREA] > '0')
      memcpy(pCharRec->LeaseArea, apTokens[CI_LEASE_AREA], strlen(apTokens[CI_LEASE_AREA]));

   // Net rental
   if (*apTokens[CI_RENTABLE_AREA] > '0')
      memcpy(pCharRec->NetRental, apTokens[CI_RENTABLE_AREA], strlen(apTokens[CI_RENTABLE_AREA]));

   // Zoning
   if (*apTokens[CI_ZONING_CODE] > ' ')
      vmemcpy(pCharRec->Zoning, apTokens[CI_ZONING_CODE], SIZ_CHAR_ZONING);

   // Quality - BldgCls
   pTmp = apTokens[CI_QUALITY_CLASS];
   if (isalpha(*pTmp))
   {  // D45A
      memcpy(pCharRec->QualityClass, pTmp, strlen(pTmp));

      pCharRec->BldgClass = *pTmp++;
      iTmp = atoin(pTmp, 3);
      if (iTmp > 0)
      {
         sprintf(acTmp, "%.1f", (double)iTmp/10.0);
         iTmp = Value2Code(acTmp, acCode, NULL);
         if (iTmp >= 0)
            pCharRec->BldgQual = acCode[0]; 
         else
            iTmp = 0;      // Testing
      }
   }

   // YrBlt
   iTmp = atol(apTokens[CI_YEAR_BUILT]);
   if (iTmp > 1900 && iTmp <= lToyear)
      memcpy(pCharRec->YrBlt, apTokens[CI_YEAR_BUILT], 4);

   // Eff year
   iTmp = atol(apTokens[CI_EFFECTIVE_YEAR]);
   if (iTmp > 1900 && iTmp <= lToyear)
      memcpy(pCharRec->YrEff, apTokens[CI_EFFECTIVE_YEAR], 4);

   // Condition
   if (*apTokens[CI_CONDITION_CODE] > ' ')
   {
      if (strchr("AGFEPN", *apTokens[CI_CONDITION_CODE]))
         pCharRec->ImprCond[0] = *apTokens[CI_CONDITION_CODE];
      else
         LogMsg("CI - Unknown Condition [%s] APN=%.10s", apTokens[SF_CONDITION_CODE], apTokens[CI_APN]);
   }

   // Cooling
   if (*apTokens[CI_AIR_COND_FLAG] == 'Y')
      pCharRec->Cooling[0] = 'U';
   else if (*apTokens[CI_AIR_COND_FLAG] == 'N')
      pCharRec->Cooling[0] = 'N';

   // Elevator
   if (*apTokens[CI_ELEVATOR_FLAG] > ' ')
      pCharRec->HasElevator = *apTokens[CI_ELEVATOR_FLAG];

   // Sprinkler

   // Const Class

   // MS Class

   // Wall Height

   // Number of tenants

   // Vacancy percent

   // Gross income

   // Expense percent
   
   // Office percent

   // Warehouse percent

   // Parking ratio

   // Bldg Sqft
   iSqft = atol(apTokens[CI_TOTAL_AREA]);
   if (iSqft > 0)
      memcpy(pCharRec->BldgSqft, apTokens[CI_TOTAL_AREA], strlen(apTokens[CI_TOTAL_AREA]));

   // Stories
   iTmp = atol(apTokens[CI_NUMBER_FLOORS]);
   if (iTmp > 0)
   {
      iTmp = sprintf(acTmp, "%d.0", iTmp);
      memcpy(pCharRec->Stories, acTmp, iTmp);
   }

   // Remark
   iTmp = blankRem(apTokens[CI_REMARKS]);
   vmemcpy(pCharRec->Misc.Comment, apTokens[CI_REMARKS], SIZ_CHAR_COMMENTS, iTmp);

   pCharRec->CRLF[0] = '\n';
   pCharRec->CRLF[1] = '\0';

   return 0;
}

int AG_FormatStdChar(char *pCharbuf)
{
   char     acTmp[_MAX_PATH];
   double   dTmp;
   int      iTmp, iSqft;
   STDCHAR  *pCharRec=(STDCHAR *)pCharbuf;

   // APN
   memcpy(pCharRec->Apn, apTokens[AG_APN], strlen(apTokens[AG_APN]));

#ifdef _DEBUG
   //if (!memcmp(pCharRec->Apn, "1858506000", 10) )
   //   iTmp = 0;
#endif

   pCharRec->AGP = 'Y';

   // Acreage
   dTmp = atof(apTokens[AG_LAND_ACRES]);
   if (dTmp > 0.0)
   {
      iTmp = sprintf(acTmp, "%d", (long)(dTmp*1000));
      memcpy(pCharRec->LotAcre, acTmp, iTmp);
   }

   // LotSqft
   iSqft = atol(apTokens[AG_USABLE_SQ_FEET]);
   if (iSqft > 0)
   {
      iTmp = sprintf(acTmp, "%d", iSqft);
      memcpy(pCharRec->LotSqft, acTmp, iTmp);
   }

   // Zoning
   if (*apTokens[AG_ZONING_CODE] > ' ')
      vmemcpy(pCharRec->Zoning, apTokens[AG_ZONING_CODE], SIZ_CHAR_ZONING);

   // Date updated
   iTmp = atol(apTokens[AG_DATE_UPDATED]);
   if (iTmp > 19000101 && iTmp < lToday)
      memcpy(pCharRec->Date_Updated, apTokens[AG_DATE_UPDATED], SIZ_CHAR_DATE);

   pCharRec->CRLF[0] = '\n';
   pCharRec->CRLF[1] = '\0';

   return 0;
}

int Scl_ExtrChar(char *pType, int iType, bool bOverWrite, bool bSortOutput)
{
   char  *pTmp, acCharRec[2048], acInRec[MAX_RECSIZE], acTmpFile[_MAX_PATH];
   long  lCnt=0, lChars=0;
   int   iRet;
   FILE  *fdIn;
   STDCHAR  *pCharRec=(STDCHAR *)&acCharRec[0];
   
   GetIniString(myCounty.acCntyCode, pType, "", acCharFile, _MAX_PATH, acIniFile);

   // Open char file
   LogMsg("Open char file %s", acCharFile);
   fdIn = fopen(acCharFile, "r");
   if (fdIn == NULL)
   {
      LogMsg("***** Error opening char file: %s\n", acCharFile);
      return -2;
   }

   // Drop header record
   pTmp = fgets(acInRec, MAX_RECSIZE, fdIn);

   // Create output file
   sprintf(acTmpFile, "%s\\%s\\%s_Char.Dat", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode); 
   LogMsg("Create output chars file %s", acTmpFile);
   if (bOverWrite)
      fdChar = fopen(acTmpFile, "w");
   else
      fdChar = fopen(acTmpFile, "a+");
   if (fdChar == NULL)
   {
      LogMsg("***** Error creating char file: %s\n", acTmpFile);
      return -3;
   }

   // Merge loop
   while (!feof(fdIn))
   {
      // Get next record
      pTmp = fgets(acInRec, MAX_RECSIZE, fdIn);
      if (!pTmp)
         break;

      replNull(acInRec);
      memset(acCharRec, ' ', sizeof(STDCHAR));
      iRet = 1;
      switch (iType)
      {
         case SCL_CHAR_SF: 
            iRet = ParseStringNQ(acInRec, ',', SF_REMARKS+2, apTokens);
            if (iRet >= SF_REMARKS)
               iRet = SF_FormatStdChar(acCharRec);
            else
               iRet = 96;
            break;
         case SCL_CHAR_MF: 
            iRet = ParseStringNQ(acInRec, ',', MF_REMARKS+2, apTokens);
            if (iRet >= MF_REMARKS)
               iRet = MF_FormatStdChar(acCharRec);
            else
               iRet = 97;
            break;
         case SCL_CHAR_CI: 
            iRet = ParseStringNQ(acInRec, ',', CI_REMARKS+2, apTokens);
            if (iRet >= CI_REMARKS)
               iRet = CI_FormatStdChar(acCharRec);
            else
               iRet = 98;
            break;
         case SCL_CHAR_AG: 
            iRet = ParseStringNQ(acInRec, ',', AG_USABLE_SQ_FEET+2, apTokens);
            if (iRet >= AG_USABLE_SQ_FEET)
               iRet = AG_FormatStdChar(acCharRec);
            else
               iRet = 99;
            break;
      }

      if (!iRet)
      {
         fputs(acCharRec, fdChar);
         lChars++;
      } else if (iRet == 99)
         LogMsg("Bad input record (%d): %s", lCnt, apTokens[0]);

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdChar)
      fclose(fdChar);
   if (fdIn)
      fclose(fdIn);

   LogMsg("Total input records processed: %u", lCnt);
   LogMsg("Total char records output:     %u", lChars);

   // Sort output
   if (bSortOutput)
   {
      sprintf(acInRec, "S(1,%d,C,A) ", iApnLen);
      iRet = sortFile(acTmpFile, acCChrFile, acInRec, &iRet);
   }

   return iRet;
}

/***************************** Scl_MergeCharFile ******************************
 *
 *
 ******************************************************************************/

int Scl_MergeCharFile(int iSkip)
{
   char     acRawFile[_MAX_PATH], acOutFile[_MAX_PATH], acBuf[MAX_RECSIZE];

   HANDLE   fhIn, fhOut;
   DWORD    nBytesRead;
   DWORD    nBytesWritten;
   BOOL     bRet;
   long     lRet=0, lCnt=0;

   sprintf(acRawFile, acRawTmpl, "SCL", "SCL", "S01");
   sprintf(acOutFile, acRawTmpl, "SCL", "SCL", "R01");

   // Rename files for processing
   if (_access(acRawFile, 0))
   {
      if (!_access(acOutFile, 0))
         rename(acOutFile, acRawFile);
      else
      {
         LogMsg("Missing input file %s.  Please recheck!", acRawFile);
         return 1;
      }
   }

   // Open Input file
   LogMsg("Open input file %s", acRawFile);
   fhIn = CreateFile(acRawFile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhIn == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening raw file: %s\n", acRawFile);
      return 3;
   }

   // Open Char file
   fdChar = fopen(acCChrFile, "r");
   if (fdChar == NULL)
   {
      LogMsg("***** Error opening Char file: %s\n", acCChrFile);
      return -2;
   }

   // Open Output file
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error creating output file: %s\n", acOutFile);
      return 4;
   }

   // Copy skip record
   while (iSkip-- > 0)
   {
      ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);
      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
   }

   // Merge loop
   while (bRet)
   {
      bRet = ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);

      // Check for EOF
      if (!bRet)
      {
         LogMsg("Error reading input file %s (%f)", acRawFile, GetLastError());
         break;
      }

      // EOF ?
      if (!nBytesRead)
         break;

      // Merge Char
      if (fdChar)
         lRet = Scl_MergeChar(acBuf);

      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);

      if (!bRet)
      {
         LogMsg("Error occurs: %d\n", GetLastError());
         lRet = WRITE_ERR;
         break;
      }
   }

   // Close files
   if (fdChar)
      fclose(fdChar);
   if (fhOut)
      CloseHandle(fhOut);
   if (fhIn)
      CloseHandle(fhIn);

   LogMsg("Total output records:       %u", lCnt);
   LogMsg("Total Char matched:         %u", lCharMatch);
   LogMsg("Total Char skipped:         %u\n", lCharSkip);

   printf("\nTotal output records: %u", lCnt);

   lRecCnt = lCnt;
   return 0;
}

/***************************** Scl_UpdateTaxExt *****************************
 *
 * Use tax extended file to update DueDate.
 *
 * Return 0 if success
 *
 ****************************************************************************/

int Scl_UpdateTaxExt(char *pOutbuf, FILE *fdTaxExt)
{
   static char acRec[1200], *pRec=NULL;
   int         iLoop;
   TAXBASE     *pOutRec = (TAXBASE *)pOutbuf;
   SEC_TAXEXT  *pInRec = (SEC_TAXEXT *)&acRec;

   // Get first rec for first call
   if (!pRec)
      pRec = fgets(acRec, 1200, fdTaxExt);

   do
   {
      // Compare Apn
      iLoop = memcmp(pOutRec->Apn, pInRec->APN, iApnLen);
      if (iLoop > 0)
      {
         if (bDebug)
            LogMsg0("Skip ext rec  %.*s", iApnLen, pRec);

         pRec = fgets(acRec, 1200, fdTaxExt);
         if (!pRec)
         {
            fclose(fdTaxExt);
            fdTaxExt = NULL;
            return 1;      // EOF
         }
         lLegalSkip++;
      }
   } while (iLoop > 0);

   // If not match, return
   if (iLoop)
      return 1;

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "0119009000", 10) )
   //   lTmp = 0;
#endif

   sprintf(pOutRec->DueDate1, "%.4s%.4s%", &pInRec->DueDate1[4], pInRec->DueDate1);
   sprintf(pOutRec->DueDate2, "%.4s%.4s%", &pInRec->DueDate2[4], pInRec->DueDate2);
   lLegalMatch++;

   // Get next rec
   pRec = fgets(acRec, 1200, fdTaxExt);

   return 0;
}

/***************************** Scl_ParseTaxBase *****************************
 *
 * Create TaxBase output
 * Use TaxAmt to create detail record for general county tax
 *
 * Return 0 if success
 *
 ****************************************************************************/

int Scl_ParseTaxBase(char *pOutbuf, char *pDetail, char *pInbuf)
{
   int      iTmp;
   double	dTaxTotal;
   char     sTmp[256];

   TAXBASE  *pOutRec = (TAXBASE *)pOutbuf;
   SEC_TAX  *pInRec  = (SEC_TAX *)pInbuf;

   // Get tax year 
   iTaxYear = atoin(pInRec->TaxYear, 4) - 1;
   // 10/08/2024 - Keep all records
   //if (iTaxYear < lTaxYear && !memcmp(pInRec->APN_Suffix, "00", 2))
   //   return 1;

   // Clear output buffer
   memset(pOutbuf, 0, sizeof(TAXBASE));

   // APN
   memcpy(pOutRec->Apn, pInRec->APN, iApnLen);

   // Tax Year
   iTmp = sprintf(sTmp, "%d", iTaxYear);
   memcpy(pOutRec->TaxYear, sTmp, iTmp);

   // Defaulted year

   // Tax rate

   // Bill Number 
   sprintf(pOutRec->BillNum, "%s-%.2s", myTrim(pInRec->APN, iApnLen), pInRec->APN_Suffix);

   // TRA - Drop hyphen
   memcpy(pOutRec->TRA, pInRec->TRA, 3);
   memcpy(&pOutRec->TRA[3], &pInRec->TRA[4], 3);

#ifdef _DEBUG
   //if (!memcmp(pOutRec->Apn, "00301008", 8))
   //   iTmp = 0;
#endif

   if (!memcmp(pInRec->APN_Suffix, "00", 2))
   {
      pOutRec->BillType[0] = BILLTYPE_SECURED;
      pOutRec->isSecd[0] = '1';
      pOutRec->isSupp[0] = '0';
   } else 
   {
      pOutRec->BillType[0] = BILLTYPE_SECURED_SUPPL;
      pOutRec->isSecd[0] = '0';
      pOutRec->isSupp[0] = '1';
   } 

   // Due date
   //memcpy(pOutRec->DueDate1, pInRec->Bill.Inst1.DueDate, TBSIZ_YYYYMMDD);
   //memcpy(pOutRec->DueDate2, pInRec->Bill.Inst2.DueDate, TBSIZ_YYYYMMDD);

   // Check for Tax amount
   double dTax1 = atofn(pInRec->Inst1_40, TSIZ_AMT);
   double dTax2 = atofn(pInRec->Inst2_40, TSIZ_AMT);
   double dTax1_60 = atofn(pInRec->Inst1_60, TSIZ_AMT);
   double dTax2_60 = atofn(pInRec->Inst2_60, TSIZ_AMT);
   double dTax1_70 = atofn(pInRec->Inst1_70, TSIZ_AMT);
   double dTax2_70 = atofn(pInRec->Inst2_70, TSIZ_AMT);
   double dTax1_SAT = atofn(pInRec->Inst1_SAT, TSIZ_AMT);
   double dTax2_SAT = atofn(pInRec->Inst2_SAT, TSIZ_AMT);
   double dInt1 = atofn(pInRec->Int1, TSIZ_AMT);
   double dInt2 = atofn(pInRec->Int2, TSIZ_AMT);
   double dPen1 = atofn(pInRec->Pen1, TSIZ_AMT);
   double dPen2 = atofn(pInRec->Pen2, TSIZ_AMT);
   double dCost1 = atofn(pInRec->Cost1, TSIZ_AMT);
   double dCost2 = atofn(pInRec->Cost2, TSIZ_AMT);
   double dOther1 = atofn(pInRec->Other1, TSIZ_AMT);
   double dOther2 = atofn(pInRec->Other2, TSIZ_AMT);

   dTax1 += dTax1_60+dTax1_70;
   dTax2 += dTax2_60+dTax2_70;
   dTaxTotal = dTax1+dTax2;
   // Only create detail record for secured tax bill
   if (dTaxTotal > 0.0 && pOutRec->isSecd[0] == '1')
   {
      TAXDETAIL  *pDetailRec = (TAXDETAIL *)pDetail;
      memset(pDetail, 0, sizeof(TAXDETAIL));

      // Create detail record
      strcpy(pDetailRec->Apn, pInRec->APN);
      strcpy(pDetailRec->BillNum, pOutRec->BillNum);

      memcpy(pDetailRec->TaxYear, pOutRec->TaxYear, 4);
      sprintf(pDetailRec->TaxAmt, "%.2f", dTaxTotal);
      strcpy(pDetailRec->TaxCode, "0001");
      pDetailRec->TC_Flag[0] = '1';
   } else
      *pDetail = 0;

   double dDue1=0.0,dDue2=0.0;

   dTax1 += dTax1_SAT;
   dTax2 += dTax2_SAT;
   if (pInRec->PaidDate2[0] >= '0')
   {
      pOutRec->Inst1Status[0] = TAX_BSTAT_PAID;
      pOutRec->Inst2Status[0] = TAX_BSTAT_PAID;
      sprintf(pOutRec->PaidAmt1, "%.2f", dTax1);
      sprintf(pOutRec->PaidAmt2, "%.2f", dTax2);
      sprintf(pOutRec->PaidDate1, "%.4s%.4s%", &pInRec->PaidDate1[4], pInRec->PaidDate1);
      sprintf(pOutRec->PaidDate2, "%.4s%.4s%", &pInRec->PaidDate2[4], pInRec->PaidDate2);
   } else
   if (pInRec->PaidDate1[0] >= '0')
   {
      pOutRec->Inst1Status[0] = TAX_BSTAT_PAID;
      pOutRec->Inst2Status[0] = TAX_BSTAT_UNPAID;
      sprintf(pOutRec->PaidAmt1, "%.2f", dTax1);
      sprintf(pOutRec->PaidDate1, "%.4s%.4s%", &pInRec->PaidDate1[4], pInRec->PaidDate1);
      dDue2 = dTax2;
   } else
   {
      // Both inst not paid
      dDue1 = dTax1;
      dDue2 = dTax2;
      pOutRec->Inst1Status[0] = TAX_BSTAT_UNPAID;
      pOutRec->Inst2Status[0] = TAX_BSTAT_UNPAID;
   }

#ifdef _DEBUG
   //if ((int)dTaxTotal != (int)(dTax1+dTax2))
   //   iRet = 0;
#endif
   dTaxTotal = dTax1+dTax2;
   if (dTaxTotal > 0.0)
   {
      sprintf(pOutRec->TaxAmt1, "%.2f", dTax1);
      sprintf(pOutRec->TaxAmt2, "%.2f", dTax2);
      sprintf(pOutRec->TotalTaxAmt, "%.2f", dTaxTotal);
   }

   if (dPen1 > 0)
      sprintf(pOutRec->PenAmt1, "%.2f", dPen1);
   if (dPen2 > 0)
      sprintf(pOutRec->PenAmt2, "%.2f", dPen2);
   if (dCost1 > 0 || dCost2 > 0)
      sprintf(pOutRec->TotalFees, "%.2f", dCost1+dCost2);

   // Total due
   dTaxTotal = dDue1+dDue2+dPen1+dPen2+dInt1+dInt2+dOther1+dOther2+dCost1+dCost2;
   if (dTaxTotal > 0.0)
      sprintf(pOutRec->TotalDue, "%.2f", dTaxTotal);

   return 0;
}

/***************************** Scl_Load_TaxBase ******************************
 *
 * Create import file from tax base extract Base_Extr.txt and import into SQL 
 *
 * Return 0 if success.
 *
 *****************************************************************************/

int Scl_Load_TaxBase(bool bImport)
{
   char     *pTmp, acBase[1024], acDetail[1024], acRec[512], acOutbuf[MAX_RECSIZE],
            acBaseFile[_MAX_PATH], acItemsFile[_MAX_PATH], acTaxRoll[_MAX_PATH], 
            acTaxRollExt[_MAX_PATH];
            
   int      iRet, iDrop=0;
   long     lBase=0, lCnt=0;
   FILE     *fdBase, *fdItems, *fdTaxRoll, *fdTaxRollExt;
   TAXBASE  *pBase = (TAXBASE *)acBase;

   LogMsg0("Loading Tax Base for SCL");
   NameTaxCsvFile(acBaseFile, myCounty.acCntyCode, "Base");
   pTmp = strrchr(acBaseFile, '\\');
   *pTmp = 0;
   if (_access(acBaseFile, 0))
      _mkdir(acBaseFile);
   *pTmp = '\\';

   // Check file date 
   GetIniString(myCounty.acCntyCode, "TaxRoll", "", acTaxRoll, _MAX_PATH, acIniFile);
   lLastTaxFileDate = getFileDate(acTaxRoll);
   // Only process if new tax file
   iRet = isNewTaxFile(NULL, myCounty.acCntyCode);
   if (iRet <= 0)
   {
      LogMsg("*** Skip loading Tax Base. Set ChkFileDate=N to bypass new file check");
      lLastTaxFileDate = 0;
      return iRet;
   }

   // Sort tax roll
   sprintf(acRec, "%s\\%s\\%s_TaxRoll.srt", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   lCnt = sortFile(acTaxRoll, acRec, "S(5,8,C,A,1,4,C,D,20,2,C,A) F(FIX,335)");
   if (lCnt < 400000)
   {
      LogMsg("***** Input file is bad,  Please check %s", acTaxRoll);
      return -1;
   }
   strcpy(acTaxRoll, acRec);

   // Open input file
   LogMsg("Open tax file %s", acTaxRoll);
   fdTaxRoll = fopen(acTaxRoll, "r");
   if (fdTaxRoll == NULL)
   {
      LogMsg("***** Error opening tax file: %s (errno=%d)\n", acTaxRoll, _errno);
      return -2;
   }  

   // Open extended tax roll
   GetIniString(myCounty.acCntyCode, "TaxRollExt", "", acRec, _MAX_PATH, acIniFile);
   sprintf(acTaxRollExt, "%s\\%s\\%s_TaxRollExt.srt", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   lCnt = sortFile(acRec, acTaxRollExt, "S(5,8,C,A,1,4,C,D,20,2,C,A) F(FIX,1090)");
   LogMsg("Open extended tax roll file %s", acTaxRollExt);
   fdTaxRollExt = fopen(acTaxRollExt, "r");
   if (fdTaxRollExt == NULL)
   {
      LogMsg("***** Error opening extended tax file: %s (errno=%d)\n", acTaxRollExt, _errno);
      return -2;
   }  

   // Open Output base file
   LogMsg("Create Base file %s", acBaseFile);
   fdBase = fopen(acBaseFile, "w");
   if (fdBase == NULL)
   {
      LogMsg("***** Error creating output file: %s\n", acBaseFile);
      return -4;
   }

   // Open Items file
   NameTaxCsvFile(acItemsFile, myCounty.acCntyCode, "Items");
   LogMsg("Create Items file %s", acItemsFile);
   fdItems = fopen(acItemsFile, "w");
   if (fdItems == NULL)
   {
      LogMsg("***** Error creating Items file: %s\n", acItemsFile);
      return -4;
   }

   // Merge loop 
   while (!feof(fdTaxRoll))
   {
      pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdTaxRoll);
      if (!pTmp)
         break;

      // Create new R01 record
      iRet = Scl_ParseTaxBase(acBase, acDetail, acRec);
      if (!iRet)
      {
         // Update DueDate
         if (fdTaxRollExt)
            iRet = Scl_UpdateTaxExt(acBase, fdTaxRollExt);

         // Create TaxBase record
         Tax_CreateTaxBaseCsv(acOutbuf, (TAXBASE *)&acBase);

         // Output record			
         lBase++;
         fputs(acOutbuf, fdBase);

         if (acDetail[0] >= '0')
         {
            // Create TaxDetail record
            Tax_CreateDetailCsv(acRec, (TAXDETAIL *)&acDetail);
            fputs(acRec, fdItems);
         }
      } else
      {
         LogMsg("---> Drop record %d [%.40s]", lCnt, acRec); 
         iDrop++;
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdTaxRoll)
      fclose(fdTaxRoll);
   if (fdTaxRollExt)
      fclose(fdTaxRollExt);
   if (fdBase)
      fclose(fdBase);
   if (fdItems)
      fclose(fdItems);

   LogMsg("Total records processed:   %u", lCnt);
   LogMsg("    base records output:   %u", lBase);
   LogMsg("       ext roll matched:   %u", lLegalMatch);
   LogMsg("       ext roll skipped:   %u", lLegalSkip);

   if (iDrop > 5)
   {
      LogMsg("Total records dropped: %u", iDrop);
      return -5;
   }

   // Import into SQL
   if (bImport && lBase > 100000)
   {
      iRet = doTaxImport(myCounty.acCntyCode, TAX_BASE);
   } else
      iRet = 0;

   return iRet;
}

/**************************** Scl_Update_TaxBase *****************************
 *
 * Create import file from "MF_Secured_Taxes_Receivable.txt" and import into Tmp_Tax_Base. 
 * Then merge it into Scl_Tax_Base.
 *
 * Return 0 if success.
 *
 *****************************************************************************/

int Scl_Update_TaxBase(bool bImport)
{
   char     *pTmp, acBase[1024], acDetail[1024], acRec[512], acOutbuf[MAX_RECSIZE],
            acBaseFile[_MAX_PATH], acTaxRoll[_MAX_PATH], acTaxRollExt[_MAX_PATH];
            
   int      iRet, iDrop=0;
   long     lBase, lCnt;
   FILE     *fdBase, *fdTaxRoll, *fdTaxRollExt;
   TAXBASE  *pBase = (TAXBASE *)acBase;

   LogMsg0("Updating Tax Base for SCL");

   // Check file date 
   GetIniString(myCounty.acCntyCode, "TaxRoll", "", acTaxRoll, _MAX_PATH, acIniFile);
   lLastTaxFileDate = getFileDate(acTaxRoll);
   // Only process if new tax file
   iRet = isNewTaxFile(NULL, myCounty.acCntyCode);
   if (iRet <= 0)
   {
      LogMsg("*** Skip loading Tax Base. Set ChkFileDate=N to bypass new file check");
      lLastTaxFileDate = 0;
      return iRet;
   }

   // Sort tax roll
   sprintf(acRec, "%s\\%s\\%s_TaxRoll.srt", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   lCnt = sortFile(acTaxRoll, acRec, "S(5,8,C,A,1,4,C,D,20,2,C,A) F(FIX,335)");
   if (lCnt < 100)
   {
      LogMsg("***** Input file is bad,  Please check %s", acTaxRoll);
      return -1;
   }
   strcpy(acTaxRoll, acRec);

   // Open input file
   LogMsg("Open tax file %s", acTaxRoll);
   fdTaxRoll = fopen(acTaxRoll, "r");
   if (fdTaxRoll == NULL)
   {
      LogMsg("***** Error opening tax file: %s (errno=%d)\n", acTaxRoll, _errno);
      return -2;
   }  

   // Open extended tax roll
   GetIniString(myCounty.acCntyCode, "TaxRollExt", "", acRec, _MAX_PATH, acIniFile);
   sprintf(acTaxRollExt, "%s\\%s\\%s_TaxRollExt.srt", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   lCnt = sortFile(acRec, acTaxRollExt, "S(5,8,C,A,1,4,C,D,20,2,C,A) F(FIX,1090)");
   LogMsg("Open extended tax roll file %s", acTaxRollExt);
   fdTaxRollExt = fopen(acTaxRollExt, "r");
   if (fdTaxRollExt == NULL)
   {
      LogMsg("***** Error opening extended tax file: %s (errno=%d)\n", acTaxRollExt, _errno);
      return -2;
   }  

   // Open Output base file
   NameTaxCsvFile(acBaseFile, "TmpScl", "Base");
   LogMsg("Create Base file %s", acBaseFile);
   fdBase = fopen(acBaseFile, "w");
   if (fdBase == NULL)
   {
      LogMsg("***** Error creating output file: %s\n", acBaseFile);
      return -4;
   }

   // Merge loop 
   lBase=lCnt = 0;
   while (!feof(fdTaxRoll))
   {
      pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdTaxRoll);
      if (!pTmp)
         break;

      // Create new R01 record
      iRet = Scl_ParseTaxBase(acBase, acDetail, acRec);
      if (!iRet)
      {
         // Update DueDate
         if (fdTaxRollExt)
            iRet = Scl_UpdateTaxExt(acBase, fdTaxRollExt);

         // Create TaxBase record
         Tax_CreateTaxBaseCsv(acOutbuf, (TAXBASE *)&acBase);

         // Output record			
         lBase++;
         fputs(acOutbuf, fdBase);
      } else
      {
         LogMsg("---> Drop record %d [%.40s]", lCnt, acRec); 
         iDrop++;
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdTaxRoll)
      fclose(fdTaxRoll);
   if (fdTaxRollExt)
      fclose(fdTaxRollExt);
   if (fdBase)
      fclose(fdBase);

   LogMsg("Total records processed:   %u", lCnt);
   LogMsg("    base records output:   %u", lBase);
   LogMsg("       ext roll matched:   %u", lLegalMatch);
   LogMsg("       ext roll skipped:   %u", lLegalSkip);

   if (iDrop > 5)
   {
      LogMsg("Total records dropped: %u", iDrop);
      return -5;
   }

   // Import into SQL
   if (bImport && lBase > 10)
   {
      iRet = doTaxImport("TmpScl", TAX_BASE);
   } else
      iRet = 0;

   return iRet;
}

/***************************** Scl_ParseTaxDetail ****************************
 *
 * Create detail and angency records
 *
 * Return 0 if there is tax amt, otherwise return 1
 *
 *****************************************************************************/

int Scl_ParseTaxDetail(char *pDetailBuf, char *pAgencyBuf, char *pInbuf)
{
   SPC_ASMT   *pInRec   = (SPC_ASMT *)pInbuf;
   TAXDETAIL  *pDetail  = (TAXDETAIL *)pDetailBuf;
   TAXAGENCY  *pResult, *pAgency = (TAXAGENCY *)pAgencyBuf;
   double      dTax;
   int         iRet;
   char        acTmp[32];

   // Clear output buffer
   memset(pDetailBuf, 0, sizeof(TAXDETAIL));
   memset(pAgencyBuf, 0, sizeof(TAXAGENCY));

   // APN
   memcpy(pDetail->Apn, pInRec->APN, iApnLen);

   // Make up bill number
   memcpy(pDetail->BillNum, pInRec->APN, iApnLen);
   memcpy(&pDetail->BillNum[iApnLen], "-00", 3);

   // Tax Year
   sprintf(acTmp, "%d", lTaxYear);
   memcpy(pDetail->TaxYear, acTmp, 4);

   // Tax Code
   memcpy(pDetail->TaxCode, pInRec->TaxCode, 4);
   memcpy(pAgency->Code,    pInRec->TaxCode, 4);

   // Tax Desc
   pResult = findTaxAgency(pDetail->TaxCode);
   if (pResult)
   {
      strcpy(pAgency->Agency,pResult->Agency);
      strcpy(pAgency->Phone,pResult->Phone);
      pAgency->TC_Flag[0] = pResult->TC_Flag[0];
   } else
   {
      LogMsg0("*** Unknown Agency: %s [%s]", pDetail->TaxCode, pDetail->Apn);
      sprintf(pAgency->Agency, "%s - SPECIAL ASSESSMENT", pDetail->TaxCode);
   }

   // Tax Amt
   dTax  = atofn(pInRec->AsmtAmt, TSIZ_AMT);
   if (dTax > 0.0)
   {
      sprintf(pDetail->TaxAmt, "%.2f", dTax);
      iRet = 0;
   } else
      iRet = 1;

   return iRet;
}

/**************************** Scl_Load_TaxItems ******************************
 *
 * Create detail import file and import into SQL if specified
 *
 * Return 0 if success.
 *
 *****************************************************************************/

int Scl_Load_TaxItems(bool bImport)
{
   char     *pTmp, acRec[512], acTmpFile[_MAX_PATH], acDetailFile[_MAX_PATH],
            acItemsRec[MAX_RECSIZE], acAgencyRec[512], acItemsFile[_MAX_PATH], acAgencyFile[_MAX_PATH];
            
   int      iRet, iDrop=0;
   long     lItems=0, lCnt=0, lAgency=0;
   FILE     *fdItems, *fdTaxDetail, *fdAgency;

   TAXDETAIL *pTax = (TAXDETAIL *)&acItemsRec[0];
   TAXAGENCY *pAgency = (TAXAGENCY *)&acAgencyRec[0];
   SPC_ASMT  *pSpecAsmt = (SPC_ASMT *)&acRec[0];

   LogMsg0("Loading Tax Detail file");

   // Open special assessment file
   GetIniString(myCounty.acCntyCode, "TaxDetail", "", acDetailFile, _MAX_PATH, acIniFile);
   LogMsg("Open special assessment tax file %s", acDetailFile);
   fdTaxDetail = fopen(acDetailFile, "r");
   if (fdTaxDetail == NULL)
   {
      LogMsg("***** Error opening special assessment tax file: %s (errno=%d)\n", acDetailFile, _errno);
      return -2;
   }  

   // Open Items file - append to output file from Load_TaxBase()
   NameTaxCsvFile(acItemsFile, myCounty.acCntyCode, "Items");
   LogMsg("Create Items file %s", acItemsFile);
   fdItems = fopen(acItemsFile, "a+");
   if (fdItems == NULL)
   {
      LogMsg("***** Error creating Items file: %s\n", acItemsFile);
      return -4;
   }

   // Open Agency file
   NameTaxCsvFile(acAgencyFile, myCounty.acCntyCode, "Agency");
   sprintf(acTmpFile, "%s\\%s\\%s_Agency.tmp", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   LogMsg("Create Agency file %s", acTmpFile);
   fdAgency = fopen(acTmpFile, "w");
   if (fdAgency == NULL)
   {
      LogMsg("***** Error creating Agency file: %s\n", acTmpFile);
      return -4;
   }

   // Init Agency table
   memset(acAgencyRec, 0, sizeof(TAXAGENCY));
   strcpy(pAgency->Code, "0001");
   strcpy(pAgency->Agency, "GENERAL TAX");
   pAgency->TC_Flag[0] = '1';
   Tax_CreateAgencyCsv(acRec, (TAXAGENCY *)&acAgencyRec);
   fputs(acRec, fdAgency);
   strcpy(pAgency->Code, "0002");
   strcpy(pAgency->Agency, "GENERAL SUPPLEMENTAL TAX");
   Tax_CreateAgencyCsv(acRec, (TAXAGENCY *)&acAgencyRec);
   fputs(acRec, fdAgency);

   // Start loop 
   while (!feof(fdTaxDetail))
   {
      pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdTaxDetail);
      if (!pTmp)
         break;

      // Ignore SBE tax bill
      //if (!memcmp(pSpecAsmt->RollType, "SB", 2))
      //   continue;

      // Create Items & Agency record
      iRet = Scl_ParseTaxDetail(acItemsRec, acAgencyRec, acRec);
      if (!iRet)
      {
         // Create TaxBase record
         Tax_CreateDetailCsv(acRec, (TAXDETAIL *)&acItemsRec);
         lItems++;
         fputs(acRec, fdItems);

         // Create Agency record
         Tax_CreateAgencyCsv(acRec, (TAXAGENCY *)&acAgencyRec);
         fputs(acRec, fdAgency);
      } else
      {
         LogMsg0("---> Drop record %d [%.80s]", lCnt, acRec); 
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   if (fdTaxDetail)
      fclose(fdTaxDetail);
   if (fdItems)
      fclose(fdItems);
   if (fdAgency)
      fclose(fdAgency);

   LogMsg("Total records processed:    %u", lCnt);
   LogMsg("Total Items records:        %u", lItems);

   LogMsg("Dedup Agency file %s to %s", acTmpFile, acAgencyFile);
   iRet = sortFile(acTmpFile, acAgencyFile, "S(#1,C,A) DUPOUT F(TXT)");

   // Import into SQL
   if (bImport)
   {
      iRet = doTaxImport(myCounty.acCntyCode, TAX_DETAIL);
      if (!iRet)
         iRet = doTaxImport(myCounty.acCntyCode, TAX_AGENCY);
   } else
      iRet = 0;

   return iRet;
}

/*************************** Scl_ParseTaxDelq ********************************
 *
 * Create TaxDelq record using MF_Redemption_Abstracts.txt file. 
 *
 * Return 0 if success.
 *
 *****************************************************************************/

int Scl_ParseTaxDelq(char *pInBuf, char *pDelqBuf)
{
   double   dTmp, dTotal;

   TAXDELQ *pDelqRec = (TAXDELQ *)pDelqBuf;
   SCL_TAXDELQ *pInRec   = (SCL_TAXDELQ *)pInBuf;

   memset(pDelqRec, 0, sizeof(TAXDELQ));

#ifdef _DEBUG
   //if (!memcmp(pBaseRec->Apn, "00102000400000", iApnLen))
   //   iTmp = 0;
#endif

   memcpy(pDelqRec->Apn, pInRec->Apn, iApnLen);
   memcpy(pDelqRec->TaxYear, pInRec->DefaultedYear, 4);
   memcpy(pDelqRec->Def_Date, pInRec->DefaultedYear, 4);

   // Updated date
   sprintf(pDelqRec->Upd_Date, "%d", lLastTaxFileDate);

   // Update DefAmt
   dTotal = atofn(pInRec->PayoffAmt, TSIZ_AMT)+atofn(pInRec->SpclAsmnt, TSIZ_AMT);
   if (dTotal > 0)
      sprintf(pDelqRec->Def_Amt, "%.2f", dTotal);

   // Penalty
   dTmp = atofn(pInRec->Redemption_Penalty, TSIZ_AMT)+atofn(pInRec->Penalty, TSIZ_AMT);
   if (dTmp > 0)
      sprintf(pDelqRec->Pen_Amt, "%.2f", dTmp);
   dTotal += dTmp;

   // Fee
   dTmp  = atofn(pInRec->Redemption_Fee, TSIZ_AMT);
   dTmp += atofn(pInRec->Cost, TSIZ_AMT);
   dTmp += atofn(pInRec->RetChkChrg, TSIZ_AMT);
   dTmp += atofn(pInRec->Other_Charge, TSIZ_AMT);
   if (dTmp > 0)
      sprintf(pDelqRec->Fee_Amt, "%.2f", dTmp);
   dTotal += dTmp;

   double dPaidAmt = atofn(pInRec->Paid_Amt, TSIZ_AMT);
   if (dPaidAmt > 0)
   {
      sprintf(pDelqRec->Red_Amt, "%.2f", dPaidAmt);
      memcpy(pDelqRec->Red_Date, pInRec->Redeemed_Date, 8);
   }

   // Status
   pDelqRec->isDelq[0] = '0';
   if (dPaidAmt >= dTotal)
   {
      pDelqRec->DelqStatus[0] = TAX_STAT_REDEEMED;       // redeemed
   } else if (dPaidAmt > 0)
   {
      pDelqRec->DelqStatus[0] = TAX_STAT_ONPAYMENT;      // on payment plan
   } else
   {
      pDelqRec->DelqStatus[0] = TAX_STAT_UNPAID;
      pDelqRec->isDelq[0] = '1';
   }

   return 0;
}

/**************************** Scl_Load_TaxDelq *******************************
 *
 * Load TaxDelq file 20220723_RDMJ3410.txt
 * Return 0 if success.
 *
 *****************************************************************************/

int Scl_Load_TaxDelq(bool bImport)
{
   char     *pTmp, acDelqFile[_MAX_PATH], acInFile[_MAX_PATH], acOutbuf[MAX_RECSIZE], acRec[MAX_RECSIZE], acDelq[1024];
   int      iRet;
   long     lDelq=0, lCnt=0;
   FILE     *fdRdm, *fdDelq;

   LogMsg0("Loading Tax Delq for SCL");
   NameTaxCsvFile(acDelqFile, myCounty.acCntyCode, "Delq");

   // Delinquent file
   GetIniString(myCounty.acCntyCode, "Redemption", "", acInFile, _MAX_PATH, acIniFile);
   lLastTaxFileDate = getFileDate(acInFile);

   LogMsg("Open Redemption file %s", acInFile);
   fdRdm = fopen(acInFile, "r");
   if (fdRdm == NULL)
   {
      LogMsg("***** Error opening delinquent file: %s\n", acInFile);
      return -2;
   }  

   // Create import Delq file
   LogMsg("Open Delq file %s", acDelqFile);
   fdDelq = fopen(acDelqFile, "w");
   if (fdDelq == NULL)
   {
      LogMsg("***** Error creating Delq file: %s\n", acDelqFile);
      return -4;
   }

   // Merge loop 
   while (!feof(fdRdm))
   {
      pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdRdm);
      if (!pTmp || *pTmp < '0')
         break;

      // Create Delq record
      if (fdRdm)
      {
         iRet = Scl_ParseTaxDelq(acRec, acDelq);
         if (!iRet)
         {
            Tax_CreateDelqCsv(acOutbuf, (TAXDELQ *)&acDelq);
            fputs(acOutbuf, fdDelq);
            lDelq++;
         }
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdRdm)
      fclose(fdRdm);
   if (fdDelq)
      fclose(fdDelq);

   LogMsg("Total records processed:       %u", lCnt);
   LogMsg("Total delq records output:     %u", lDelq);

   // Import into SQL
   if (bImport)
      iRet = doTaxImport(myCounty.acCntyCode, TAX_DELINQUENT);
   else
      iRet = 0;

   return iRet;
}

/*********************************** loadScl ********************************
 *
 * Input files:
 *    - Scl_Lien        (lien file, fixed length 402 /w CRLF)
 *    - Scl_Roll        (roll file, fixed length 402 /w CRLF)
 *    - Scl_Situs       (Situs file, fixed length 106 /w CRLF)
 *    - Sale_Exp.sls    cummulative sale file
 *
 * Notes: Make sure cum sale file Sale_exp.sls is avail. when doing LDR.
 *        
 *    - On lien date, run -L -Xl -Mg -Xc
 *    - Normal monthly update, run -U -G
 *    - Daily update, run -G
 *    - Use -Uu to update standard usecode as needed
 *
 ****************************************************************************/

int loadScl(int iSkip)
{
   int   iRet=0;
   char  acTmp[256], acTmpFile[256];
   bool  bUpdOwner=true;

   iApnLen = myCounty.iApnLen;

   // Convert DocTax to %.2f - 05/16/2019
   //sprintf(acTmpFile, acGrGrTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "SLS");
   //FixGrGrDef(acTmpFile, SALE_FLD_STAMPAMT);

   // Load tax
   if (iLoadTax == TAX_LOADING)                    // -T or -Lt
   {
      // Init DueDate format
      TC_SetDateFmt(0, true);

      iRet = GetIniString(myCounty.acCntyCode, "TaxRoll", "", acTmpFile, _MAX_PATH, acIniFile);
      iRet = (UINT)getFileSize(acTmpFile);
      if (iRet > 150000000)
      {
         iRet = GetIniString(myCounty.acCntyCode, "TaxDist", "", acTmpFile, _MAX_PATH, acIniFile);
         if (iRet > 0)
            iRet = LoadTaxCodeTable(acTmpFile);

         // Load Tax Base and Delq
         iRet = Scl_Load_TaxBase(bTaxImport);
         if (!iRet && lLastTaxFileDate > 0)
         {
            // Load Items and Agency
            iRet = Scl_Load_TaxItems(bTaxImport); 

            if (iRet != 0)
               return iRet;
         }

         iRet = Scl_Load_TaxDelq(bTaxImport);
         // Upate Delq flag in Base table
         if (!iRet)
            iRet = updateDelqFlag(myCounty.acCntyCode);
      } else
      {
UpdateTax:         
         // Update tax base using MF_Secured_Taxes_Receivable.txt
         iRet = Scl_Update_TaxBase(bTaxImport);

         // Update tax delq using MF_Redemption_Abstracts.txt
         iRet = Scl_Load_TaxDelq(bTaxImport);

         // Merge Tmp table to Base & Delq
         if (!iRet)
            iRet = doTaxMerge(myCounty.acCntyCode, false, false, false);

         if (!iRet)
            iRet = updateDelqFlag(myCounty.acCntyCode, true, true, 2);
      }
   } else if (iLoadTax == TAX_UPDATING) // -Ut
      goto UpdateTax;

   // Exit if load/update tax only
   if (!iLoadFlag && !lOptMisc)
      return iRet;

   if (iLoadFlag & EXTR_NRSAL)                     // -Xn
   {
      iRet = GetIniString(myCounty.acCntyCode, "NdcSale", "", acSaleFile, _MAX_PATH, acIniFile);
      if (!_access(acSaleFile, 0))
      {
         iRet = NR_CreateSCSale(myCounty.acCntyCode, acSaleFile, iApnLen);
         if (iRet)
            return iRet;
      } else
      {
         LogMsg("*** Sale file not available.  Please verify: %s", acSaleFile);
         return -1;
      }
   }

   // Load GrGr file
   if (iLoadFlag & LOAD_GRGR)                      // -G 
   {
      // Create Scl_GrGr.dat, append to Scl_GrGr.sls, 
      // then extract to GrGr_Exp.dat 
      LogMsg0("Load %s GrGr file", myCounty.acCntyCode);
      iRet = Scl_LoadGrGr(myCounty.acCntyCode);
      if (!iRet)
      {
         // Unused code 3/21/2020 spn
         // Convert SCL_GRGR.SLS to SCL_SALE.TMP then SALE_EXP.SLS
         //sprintf(acTmpFile, acGrGrTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "SLS");
         //sprintf(acSaleFile, acSaleTmpl, myCounty.acCntyCode, "SLS");
         //iRet = convertSaleData(myCounty.acCntyCode, acTmpFile, CONV_GRGR_DEF, acSaleFile);
         iLoadFlag |= MERG_GRGR;
      }
   }

   // Extract cum sale from GRGR file.  Do not use this option on the same commandline with -G
   if (iLoadFlag & EXTR_CSAL)                      // -Xc
   {
      // Convert SCL_GRGR.SLS to GRGR_EXP.SLS
      sprintf(acTmpFile, acGrGrTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "Sls");
      iRet = Scl_ExtrSaleMatched(acTmpFile, acCSalFile);
   }

   // Load CHAR files
   if (iLoadFlag & EXTR_ATTR)                      // -Xa
   {
      //char acLUFile[_MAX_PATH];

      // Get Lookup file name
      //GetIniString("System", "LookUpTbl", "", acLUFile, _MAX_PATH, acIniFile);

      //// Load tables
      //iRet = LoadLUTable((char *)&acLUFile[0], "[Quality]", NULL, MAX_ATTR_ENTRIES);
      //if (!iRet)
      //{
      //   LogMsg("***** Error Looking for table [Quality] in %s", acLUFile);
      //   return 1;
      //}

      LogMsg0("Extract CHAR file");
      iRet = Scl_ExtrChar("SF_CHAR", SCL_CHAR_SF, true, false);
      if (!iRet)
         iRet = Scl_ExtrChar("MF_CHAR", SCL_CHAR_MF, false, false);
      if (!iRet)
         iRet = Scl_ExtrChar("CI_CHAR", SCL_CHAR_CI, false, false);
      if (!iRet)
         iRet = Scl_ExtrChar("AG_CHAR", SCL_CHAR_AG, false, true);
      if (iRet < 0)
         return iRet;
   }

   if (iLoadFlag & EXTR_DESC)                      // -Xd
   {
      //iRet = Scl_ExtrLegal();                      // 2021 - Fix length
      iRet = Scl_ExtrLegalCsv();                   // 2022 - CSV
   }

   if (iLoadFlag & EXTR_LIEN)                      // -Xl
   {
      LogMsg0("Extract %s Lien file", myCounty.acCntyCode);
      iRet = Scl_ExtrLien();
   }

   if (iLoadFlag & LOAD_LIEN)                      // -L
   {
      // Create Lien file
      LogMsg0("Load %s Lien file", myCounty.acCntyCode);
      iRet = Scl_Load_LDR(iSkip);
      bUpdOwner = false;

      // Turn off MergeLegal since it's already done in Load_LDR()
      if (iLoadFlag & MERG_LEGAL)
         iLoadFlag ^= MERG_LEGAL;
   } else if (iLoadFlag & LOAD_UPDT)               // -U
   {
      LogMsg0("Load %s roll update file", myCounty.acCntyCode);
      iRet = Scl_Load_Roll(iSkip);
   } else if (iLoadFlag & MERG_ATTR)               // -Ma
   {
      iRet = Scl_MergeCharFile(iSkip);
   } else if (iLoadFlag & MERG_LEGAL)              // -Ml
   {
      LogMsg0("Merge Legal file");
      iRet = Scl_MergeLegalFile(iSkip);
   }

   // Apply cum sale file to R01
   if (!iRet && (iLoadFlag & UPDT_XSAL) )          // -Mn
   {
      GetIniString("Data", "ASH_File", acESalTmpl, acESalTmpl, _MAX_PATH, acIniFile);
      sprintf(acCSalFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "Sls");

      // Apply Sfx_ash.sls to R01 file
      iRet = ApplyCumSale(iSkip, acCSalFile, false, GRGR_UPD_OWNER, CLEAR_OLD_SALE|CLEAR_OLD_XFER);
      if (!iRet)
         iLoadFlag |= LOAD_UPDT;
   }

   // Merge sale & GrGr data
   if (!iRet && (iLoadFlag & MERG_GRGR) )          // -Mg
   {
      // Clear old sales before applying GRGR
      if (!(iLoadFlag & UPDT_XSAL))
         bClearSales = true;

      // Check for new file
      sprintf(acTmp, acGrGrTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "Sls");
      sprintf(acTmpFile, acEGrGrTmpl, myCounty.acCntyCode, "Dat");
      iRet = chkFileDate(acTmp, acTmpFile);
      if (iRet == 1)
      {
         // Scl_GrGr.sls file is newer, extract GrGr_Exp.dat from Scl_GrGr.sls
         iRet = Scl_ExtrSaleMatched(acTmp);
      }

      // Merge sale data from GrGr_Exp.dat
      GetIniString(myCounty.acCntyCode, "AllGrGr", "", acTmp, _MAX_PATH, acIniFile);
      if (acTmp[0] == 'Y')
         iRet = Scl_MergeGrGrFile(acTmpFile, true, bUpdOwner);
      else
         iRet = Scl_MergeGrGrFile(acTmpFile, false, bUpdOwner);

      if (!iRet && !(iLoadFlag & LOAD_LIEN))
      {
         iLoadFlag |= LOAD_UPDT;                   // To signal production update
      }
   } 

   // Update Usecode
   if (iLoadFlag & UPDT_SUSE)
      iRet = updateR01StdUse(myCounty.acCntyCode, iSkip);

   return iRet;
}