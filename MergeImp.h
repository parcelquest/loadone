#ifndef  _MERGEIMP_H_
#define  _MERGEIMP_H_

/* 2005 layout
#define  IMP_ROLL_APN                  0
#define  IMP_ROLL_OLDAPN               0
#define  IMP_ROLL_NEWAPN               1
#define  IMP_ROLL_STATUS               2
#define  IMP_ROLL_PRCLCODE             3
#define  IMP_ROLL_TRA                  4
#define  IMP_ROLL_OWNER                5
#define  IMP_ROLL_CAREOF               6
#define  IMP_ROLL_M_ADDR               7
#define  IMP_ROLL_M_CITYST             8
#define  IMP_ROLL_M_ZIP                9 
#define  IMP_ROLL_S_ADDR               10
#define  IMP_ROLL_LAND                 11
#define  IMP_ROLL_IMPR                 12
#define  IMP_ROLL_PPMOBILHOME          13
#define  IMP_ROLL_PPVALUE              14
#define  IMP_ROLL_MISCEXEMPT           15
#define  IMP_ROLL_HOEXEMPT             16
#define  IMP_ROLL_BUSEXEMPT            17
#define  IMP_ROLL_EXEMPTCODE           18
#define  IMP_ROLL_USECODE              19
#define  IMP_ROLL_DOCNUM               20
#define  IMP_ROLL_REACHG               21
#define  IMP_ROLL_YRCHG                22
#define  IMP_ROLL_LEGAL                23
*/

/* 2006 layout 
#define  IMP_ROLL_FEEPARCEL      0
#define  IMP_ROLL_APN            1
#define  IMP_ROLL_TRA            2
#define  IMP_ROLL_USECODE        3
#define  IMP_ROLL_ACRES          4
#define  IMP_ROLL_OWNER          5
#define  IMP_ROLL_CAREOF         6
#define  IMP_ROLL_DBA            7
#define  IMP_ROLL_M_ADDR         8
#define  IMP_ROLL_M_CITY         9
#define  IMP_ROLL_M_STATE        10
#define  IMP_ROLL_M_ZIP          11
#define  IMP_ROLL_M_ADDR1        12
#define  IMP_ROLL_M_ADDR2        13
#define  IMP_ROLL_M_ADDR3        14
#define  IMP_ROLL_M_ADDR4        15
#define  IMP_ROLL_S_ADDR1        16
#define  IMP_ROLL_S_ADDR2        17
#define  IMP_ROLL_S_STRNUM       18
#define  IMP_ROLL_S_STREET       19
#define  IMP_ROLL_S_ZIP          20
#define  IMP_ROLL_LEGAL          21
#define  IMP_ROLL_LAND           22
#define  IMP_ROLL_IMPR           23
#define  IMP_ROLL_PPVALUE        24
#define  IMP_ROLL_S_CITY         25
#define  IMP_ROLL_S_DIR          26
#define  IMP_ROLL_S_UNIT         27
#define  IMP_ROLL_STATUS         28
*/

/* 2008 layout */
#define  IMP_ROLL_FEEPARCEL      0
#define  IMP_ROLL_APN            1
#define  IMP_ROLL_TRA            2
#define  IMP_ROLL_USECODE1       3
#define  IMP_ROLL_USECODE2       4
#define  IMP_ROLL_ACRES          5
#define  IMP_ROLL_OWNER          6
#define  IMP_ROLL_CAREOF         7
#define  IMP_ROLL_DBA            8
#define  IMP_ROLL_M_ADDR         9
#define  IMP_ROLL_M_CITY         10
#define  IMP_ROLL_M_STATE        11
#define  IMP_ROLL_M_ZIP          12
#define  IMP_ROLL_M_ADDR1        13
#define  IMP_ROLL_M_ADDR2        14
#define  IMP_ROLL_M_ADDR3        15
#define  IMP_ROLL_M_ADDR4        16
#define  IMP_ROLL_S_ADDR1        17
#define  IMP_ROLL_S_ADDR2        18
#define  IMP_ROLL_S_STRNUM       19
#define  IMP_ROLL_S_STREET       20
#define  IMP_ROLL_S_ZIP          21
#define  IMP_ROLL_LEGAL          22
#define  IMP_ROLL_LAND           23
#define  IMP_ROLL_IMPR           24
#define  IMP_ROLL_PPVALUE        25
#define  IMP_ROLL_S_CITY         26
#define  IMP_ROLL_S_DIR          27
#define  IMP_ROLL_S_UNIT         28
#define  IMP_ROLL_STATUS         29
#define  IMP_ROLL_DOCNUM         30
#define  IMP_ROLL_DOCDATE        31
// 2008 LDR
//#define  IMP_ROLL_VALSETTYPE     32
//#define  IMP_ROLL_SEQ            33
//#define  IMP_ROLL_TAXABILITY     34
// 2009 LDR
#define  IMP_ROLL_TAXABILITY     32
#define  IMP_ROLL_PPMH           33

// 2010 doesn't have PPMH

/* 07/27/2011
// Exemption file
#define  IMP_EXE_APN             0
#define  IMP_EXE_EXECODE         1
#define  IMP_EXE_EXEAMT          2
// 2008
//#define  IMP_EXE_HOEXE           3
//#define  IMP_EXE_ASMTCAT         4
#define  IMP_EXE_ASMTSTATUS      5
// 2009
#define  IMP_EXE_PCT             3
#define  IMP_EXE_HOEXE           4
*/

// 2010 & 2012 has 2 more fields DATE & USERID
#define  IMP_EXE_APN             0
#define  IMP_EXE_EXECODE         1
#define  IMP_EXE_EXEAMT          2
#define  IMP_EXE_PCT             3
#define  IMP_EXE_HOEXE           4
#define  IMP_EXE_DATE            5
#define  IMP_EXE_USERID          6


/* 2011 
#define  IMP_EXE_APN             0
#define  IMP_EXE_EXECODE         1
#define  IMP_EXE_PCT             2
#define  IMP_EXE_EXEAMT          3
#define  IMP_EXE_DATE            4
#define  IMP_EXE_HOEXE           5
#define  IMP_EXE_ID              6
#define  IMP_EXE_USERID          7
*/

typedef struct _tTraCityTbl
{
   int   iLoTra;
   int   iHiTra;
   char  *pCity;
   char  *pZip;
} IMP_CITY;

IMP_CITY asImpCity[] =
{
   1000,  1999,  "BRAWLEY",      "92227",
   2000,  2999,  "CALEXICO",     "92231",
   3000,  3999,  "CALIPATRIA",   "92233",
   4000,  4999,  "EL CENTRO",    "92243",
   5000,  5999,  "HOLTVILLE",    "92250",
   6000,  6999,  "IMPERIAL",     "92251",
   7000,  7999,  "WESTMORLAND",  "92281",
   56000, 56003, "BRAWLEY",      "92227",
   57002, 57005, "CALEXICO",     "92231",
   58000, 58000, "CALIPATRIA",   "92233",
   58002, 58002, "NILAND",       "92257",
   58003, 58003, "CALIPATRIA",   "92233",
   58004, 58010, "NILAND",       "92257",
   62000, 62000, "EL CENTRO",    "92243",
   66001, 66004, "HEBER",        "92249",
   68001, 68001, "HOLTVILLE",    "92250",
   68002, 68002, "BRAWLEY",      "92227",
   68003, 68005, "HOLTVILLE",    "92250",
   68006, 68006, "BRAWLEY",      "92227",
   68007, 68015, "HOLTVILLE",    "92250",
   68018, 68018, "HOLTVILLE",    "92250",
   69000, 69000, "BRAWLEY",      "92227",
   69001, 69004, "IMPERIAL",     "92251",
   69007, 69007, "IMPERIAL",     "92251",
   69008, 69009, "OCOTILLO",     "92259",
   69011, 69011, "BRAWLEY",      "92227",
   73000, 73000, "BRAWLEY",      "92227",
   74000, 74004, "EL CENTRO",    "92243",
   74005, 74005, "CALEXICO",     "92231",
   75001, 75002, "EL CENTRO",    "92243",
   79000, 79000, "BRAWLEY",      "92227",
   82003, 82006, "THERMAL",      "92274",
   85000, 85005, "EL CENTRO",    "92243",
   90001, 90002, "BRAWLEY",      "92227",
   94002, 94002, "WINTERHAVEN",  "92283",
   94005, 94005, "PALO VERDE",   "92266",
   94006, 94010, "WINTERHAVEN",  "92283",
   95000, 99999, "", ""
};



#endif