 * Notes: MPA has 9-digit APN. The 10th digit is reserved for special case like
 *        a property with 2 separate tax bills.  This has no meaning no meaning
 *        other than assessor internal use.  But here we display all 10 digits
 *        as we received from the county.

1) DocNum format:
   Pre 1987:   9999999YY or 999999YY or 999999 (Book/Page)
               028826586 08/15/86
               290429 10/08/86
               016510176  11/12/76
               150294 10/02/74
   1987-1998:  YY9999
               873967 09/18/87
               983103 07/15/98
   1999-2010:  YYY9999 (i.e. 1990102=0102 1999, 2030405=0405 2003)
               1994548 10/08/99
               2044711 08/02/04
   2011-now:   YYYY9999
               20125034 12/26/12
               20110095 01/11/2011
   * We need to reformat DocNum for 1999 till now. Leave older documents alone
     since no DocLink available.

2) Possible DocRef:
   *0041400370 20110095 01/11/2011
   *0042300070 20140396 1/31/14
   *0042500320 975471 12/30/97 981245
   *0070500260 2056918 10/13/05 2062142
   *0041100110 2002445&46 06/21/00
   *0151600650 2053776 & 80 06/02/05
   *0081700450 2056466-67 9/26/05
   *0081700850 20122897-98 07/30/12
   *0104100120 2012778,79 06/18/01
   *0041100150 971100,971101 03/24/97
   *0175000110 2004200 (01, 02) 9/29/00
   *0041400380 2102824 CERT OF COMP
   *0051100100 2085063 12/17/08 CERT COM
   *0083470130 2080262 01/22/08CERT COMP
   *0082000470 2057719 CERT 11/16/05
   *0082400610 027224985  CERT 2003090
   *0083100030 2057222 CERT10/26/05
   *0084400300 903174  CERT 2046751
   *0151800560 2012503 05/13/01 (C OF C)
   *0082300440 015330675 (CC 2054108)
   *0082800530 2084526 10/31/08 CC
   *0083470010 2022118 4/19/02 CC2026241
   *0132600240 2038593 12/5/03 COC
   *0140100640 025261983 COC 2039021
   *0060100130 2012232 5/11/01 (2012281)
   *0071000260 2061887 03/30/06(2074836)
   *0081700870 2033363 05/20/03  2061782
   *0060800550 2083070 07/23/2008
   *0060900420 2056414(WRG MAP #)9/23/05
   *0061600190 922631,973871, 1993861
   *0112100150 028349686 & 982563
   *0070500060 028861786,1991652 4/21/99
   *0083900030 944751 & 944752 09/12/94
   *0071100590 940387 983042 7/10/98
   *0101000830 982344 982345 06/03/98
   *0070800080 017527577 2032227 2038247
   -0102700180 20301 98611 78338             Drop this case
   *0070800660 2/1/00 2000388
   *0070800670 11/21/84 266/250 196/290
   *0080700250 936697/965782 12/24/96
   *0083300180 2057025/2052221 3/30/05
   ?0102800100 0017242/43
   *0202400240 1991415 & 1991416 4/5/99
   *0153200310 2025481 (2026279 10/24/02
   *0080800440 277/685 936697
   *0081700450 2056466-67 9/26/05
   *0081800220 026067484 06/01/84
   *0082400350 2001494(2001581) 4/19/00
   *0082900290 2103446 09/23/10 (9/2/10)
   *0083500730 (982011 CORR LEGL)2036968
   *0112900070 20133842 9/6/13 INC LEG
   *0090300020 00NONE
   ?0091200010 1995380(1992094 5/14/99) --> process in parenthesis
   *0100950100 027328585 06/25/85 R/W
   *0101400320 2064072 07/12/0669
   -0120500660 PM 29/31  04/03/08 --> drop
   -0120600070 020360079 11/05/81 --> invalid docnum or docdate
   -0131520090 51/89 LESS SEE MSTR FILE
   -0133000010 R/S 2695
   -0141000540 PM 28/27
   *0152600660 870785 87/
   -0173400070 S�NE� LESS VOL 103 PG 198
   *0173500330 166/74 2003961/62 9/15/00
   *0174400390 164/058 9/76 936460 11/93
   -0802700160 ON 013-050-0420
   -0802703950 POSS INT #56
   -0802704200 PI #53 20124537 11/21/12
   *0032400120 982653 06/17/983
   -0060800280 20134309 10/18/03 ???
   -0061430040 201031627 04/17/13 ???
   *0040600100 928422   2000815
   *0105500230 912604 052191
   -0043000180 027604885 09/18/95

"Improved Sales_be1.mdb"

OK Field name      Usable
---   ----------      ------
   APN               Y
   DATE              Y
   LAND VALUE        Y/N
   ACRES             Y/N
   YB                Y
   SQ FT             Y
   BED               Y
   BATH              Y
   FP                Y
   DECK              Y
   POOL              Y
   ADDRESS           Y/N
   SALE PRICE        Y
   ZONING            Y
   CLASS             Y
n  DOLLAR SF         N
   HEAT              Y
   GAR               Y
n  DN                F
n  RATE              F
n  TERM              F
n  LENDER            F
   PROPTYPE          Y
n  REMARKS           F
   SPA               Y
   WHEN              F
   OTHER IMP VALUE   Y/N
   BOOK              Y
   PAGE              Y
   PARCEL            Y
   select            N

   "Land 2000_be.mdb"
   Field name      Usable
   ----------      ------
   APN               Y
   SALE DATE         Y
   SALE PRICE        Y
   ACRES             Y/N
   ZONING            Y/N
   UTILITIES ACCESS  N
   TOPOGRAPHY        Y
n  DOWN PAYMENT      F
n  RATE              F
n  TERM              F
n  REMARKS           F
   WHEN              F
   ADDRESS-AREA      F
   PROPERTY TYPE     Y
   BOOK              Y
   PAGE              Y
   PARCEL            Y
   PICK              N


FP
1     2WS
887   WD STV
1     3FP
2     NONE
13    2 FP
13    2 WD STV
1144  FP
1     FORCED
1033
1     3FP & WD STV
280   WS
77    FP & WD STV

HEAT
419
2     BSBRD
1     CENT
11    EVAP
1     FORC,H/C
600   FORCED
1     FORCED W
1     FORCED/EVAP
1     FORCED/WALL
1     H-EVAP
1     H.C
1218  H/C
1     H/CF
348   H/EVAP
1     H/RVAP
3     HEAT/EVAP
1     HH/EVAP
29    N
27    NO
108   NONE
1     ONE
1     SOLAR
671   WALL
5     WALL/EVAP

Expr1 GAR
104
1     0
1     400
1     48000
100   A1
1009  A2
100   A3
2     A4
1     B
326   CP
1     CP & D2
115   D1
611   D2
81    D3
1     D4
263   NO
736   NONE

USE_ZONE cnt
220      4693
210      3163
330      1247
240      615
N/A      510
171      286
161      213
141      165
220B     154
320      148
211A     146
167E     138
150      106
165      102
162      93
220C     92
160B     92
310      91
230      87
120      85
125      72
115      72
600      71
231      61
360      60
230G     54
163      51
130A     48
134A     43
320B     36
240D     34
174A     34
164      33
166      32
310B     31
250      29
167C     27
330C     25
136B     22
340      22
150A     20
143      19
160C     18
168E     17
350      17
230H     16
310D     16
167H     16
168A     15
221B     15
331B     14
222      14
220E     12
132A     12
330B     11
165D     11
162A     10
164C     9
610A     9
145      8
180      8
210B     8
137      7
168B     7
230F     7
146      7
240A     7
320A     7
242      7
220F     6
167      6
220G     6
172      6
331      5
450      5
210A     5
174      5
450A     5
142      5
139F     5
420      4
161D     4
510      4
320F     4
168C     4
430      4
180B     3
330A     3
221      3
210C     3
164B     3
165C     3
242A     3
331A     3
230E     3
132D     3
139D     3
220A     3
161A     2
170B     2
167F     2
140      2
165B     2
221A     2
332      2
166E     2
167B     2
330E     2
163B     2
320C     2
331D     2
221C     2
131B     2
138      2
136A     2
165A     2
130C     2
132C     2
168D     2
161B     2
320G     2
132B     2
125A     1
165E     1
166C     1
211      1
162E     1
241      1
320D     1
221D     1
140C     1
400A     1
241A     1
120A     1
310A     1
230A     1
133A     1
139E     1
240C     1
320I     1
351      1
133C     1
137A     1
241D     1
222A     1
310C     1
340A     1
230D     1
131D     1
230C     1
241B     1
330D     1
140A     1
230B     1
140B     1
242B     1
136C     1
166F     1
700      1
134B     1
210E     1
340B     1
160A     1
160      1
320E     1
166D     1
135B     1
220D     1
150B     1
241C     1
167G     1
166G     1
163A     1
240B     1
139C     1
163D     1
167A     1
320H     1