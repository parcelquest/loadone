/*********************************** loadAla ********************************
 *
 * Run LoadOne with following options:
 *    -L : to load lien
 *    -U : to load update roll
 *    -Us: to load sale update file
 *    -G : to process GrGr and create sale file for MergeAdr
 *    -Xc: to extract Sale_Exp.dat from SALA.O01.  This  should be done on LDR only
 *    -Ms: to merge cumsale
 *
 * Load lien:   -CALA -O -L -Ms -Ma (use -Xc and -Xa as needed separately)
 * Load update: -CALA -O -U -G
 * Notes:
 *    - Do not use -Ms and -Xc together in one run
 *    - If county doesn't provide attribute data, run Load Lien with -Xa
 *    - Contact Sony if the output doesn't look right.
 *
 * 06/22/2006 1.2.21.2  Update transfer doc/date only if it is newer.
 * 07/07/2006 1.2.24.3  Add -Xc option for LDR processing.
 * 11/30/2006 1.3.7.0   Fix ALA Owner problem.  Allowing all TRUST abbr in Name field
 * 06/13/2007 1.4.15    Take full UseCode.  Don't know why it was chopped at 3.
 * 07/18/2007 1.4.20    Change needed for 2007 LDR.  Change are made to many place
 *                      to handle merge CHAR and merge SALE.
 * 07/27/2007 1.4.21.1  Fix OUTREC in Ala_Grgr2Sale() and CreateAlaGrGrFile() to
 *                      output SALE_REC format.
 * 02/14/2008 1.5.5     Adding support for Standard Usecode.  Change Lien format to 
 *                      standard format.
 * 03/24/2008 1.5.7.2   Format IMAPLINK.  
 * 12/11/2008 8.5.1     Make StrNum left justified
 * 01/17/2009 8.5.6     Fix TRA
 * 02/02/2009 8.5.10    Adding -Mo option to merge Other Values to R01 file.  It now includes
 *                      both PP_Val, Fixture, CLCA Land, CLCA Impr and Hpp (or Bus_Inv) values.
 *                      Adding Ala_ExtrLien() and support -Xl option.
 * 05/08/2009 8.8       Set full exemption flag
 * 07/09/2009 9.1.2     Auto merge characteristics whwen loading LDR.  Modify Ala_MergeRoll()
 *                      and Ala_CreateLienRec() to populate other values.
 * 09/15/2009 9.2.1     Add Ala_MergeChar() to merge char data from "ALACO_VARIABLE_UPDATE.TXT"
 *                      to roll file.  Rename CreateAlaRoll() to Ala_CreateRoll(), 
 *                      MergeAlaRoll() to Ala_UpdateRoll(), loading Quality table.
 * 02/02/2010 9.3.9     Fix Ala_MergeRoll() to populate S_ADDR_D.
 *                      Add Ala_UpdateCumSale() to update cum sale.
 * 02/03/2010 9.3.9.3   Add -Us option to update sale transfer file.  Bug fix on -Ms
 * 04/03/2010 9.5.2     Auto sort Sale_Exp.sls when -G is used.  So when -Mg is used,
 *                      update R01 with Sale_exp.sls instead of Sale_exp.dat.
 * 04/16/2010 9.5.4.1   Set NumOfPrclXfer='M' instead of actual number when multi prcl involved.
 * 07/12/2010 10.1.1    Modify Ala_CreateRoll() to update chars.
 * 03/14/2011 10.4.5    Open roll file in binary to avoid program interruption by bad chars.
 *                      Check for bad chars in M_STR and M_CITY.  Remove CLCA Land and CLCA Impr
 *                      from OTHER_VAL.
 * 03/25/2011 10.5.0    Process GRGR only if data available.
 * 04/02/2011 10.5.1    Fix search problem caused by bad character in roll file.
 * 04/21/2011 10.5.6    Fix LotAcres & LotSqft data problem in Ala_MergeChar().
 *                      Remove CLCA Land & Impr from OtherValue since it's not assessed value.
 * 07/12/2011 11.0.2    Add S_HSENO.  Modify Ala_CreateRoll(), open input roll file in binary
 *                      mode since there is embeded chr(26) which causes early EOF.
 * 08/16/2011 11.1.9    When there is no new GRGR, use history file GrGr_Exp.sls to merge.
 * 09/30/2011 11.4.10   Signal to extract sale import file when history sale is updated.
 * 01/13/2012 11.6.4    Change -G option to convert GRGR_EXP.SLS (ALAGRGR) to ALA_GRGR.SLS (SCSAL_REC).
 *                      It will also import GrGr into Ala_GrGr table. 
 *                      Rename CreateAlaGrGrFile() to Ala_LoadGrGr().
 * 06/04/2012 11.9.9    Call createSaleImport() with type 2 for GrGr import.
 * 09/19/2012 12.2.8    Fix Ala_MergeOwner() to keep Owner1 the same as county provided.
 * 10/17/2012 12.3.1    Populate BLDGS.
 * 12/14/2012 12.3.8    Add change to Ala_MergeOwner() to update legal in Common Area.
 * 01/02/2013 12.4.0.1  Check error on GrGr import to SQL.
 * 07/06/2013 13.0.1    Add Elevator and Topo to R01 record. Remove bad char in Name1 and CareOf.
 * 07/10/2013 13.0.3    Modify Ala_CreateRoll() to support option to rebuild prior year roll.
 *                      Add Ala_MergeHChar() to merge CHAR to prior R01 file using -Ma option.
 *                      Remove -Xa option since it's no longer needed.
 * 08/06/2013 13.3.10   Remove NAME_TYPE and use updateCareOf()
 * 10/02/2013 13.10.0   Use updateVesting() to update Vesting and Etal flag.
 * 10/03/2013 13.10.1   Fix bug that assigns ELEVATOR value to VIEW. Also remove bad chars in owner name.
 * 10/23/2013 13.11.6   Modify Ala_MergeRoll() to fix Situs & Mailing address parsing error.
 *                      This also involves change to parseAdr1_2() in R01.cpp
 * 10/24/2013 13.11.7   Fix special case "460 B TAYLOR AVE" for both situs and mailing.
 * 12/12/2013 13.11.12  Fix situs addr parsing bug in Ala_MergeRoll().
 * 12/19/2013 13.11.12.1 Fix situs addr in Ala_MergeRoll().
 * 02/06/2014 13.11.15  Modify Ala_FormatSale() to add MultiSale_Flg and set actual NumOfPrclXfer.
 * 03/03/2014 13.12.2   Modify Ala_LoadGrGr() to return -1 if no data avail. Use ApplyCumSale()
 *                      to merge GRGR data.
 * 10/29/2014 14.8.0    Fix Ala_MergeOwner() to remove known bad char and memory access violation.
 * 01/04/2015 14.10.2   Fix Unit# in Ala_MergeRoll().
 * 02/03/2015 14.6.0    Use Ala_GrGr.sls to apply GRGR instead of Sale_Exp.Sls
 * 02/16/2016 15.6.0    Remove bad char in owner name.
 * 03/26/2016 15.8.1    Fix IMAPLINK in Ala_MergeRoll()
 * 05/05/2016 15.9.3    Add Ala_Load_TaxBase() to load tax file and import into SQL.
 * 05/09/2016 15.9.4    Add Ala_UpdateTaxBase() to update TaxBase using data grabbed from the web.
 * 05/13/2016 15.9.4.1  Modify UpdateTaxBase()
 * 07/01/2016 16.0.3    Add iCharRecSize.  Modify Ala_MergeChar() to read input as fixed length
 *                      since data in CHAR file contains null character.  Also fix known data issue in YrBlt.
 * 08/30/2016 16.4.3    Call doTaxPrep() to create empty DELQ table.
 * 10/26/2016 16.5.2    Modify Ala_UpdateTaxBase() to update only if same BillNum.  Modify Ala_ParseTaxDetail()
 *                      to convert negative value correctly, use lTaxYear instead of lLienYear, and generate 
 *                      Tax_Agency record. Modify Ala_Load_TaxBase() to create Tax_Agency.
 * 01/09/2017 16.8.8    Modify Ala_ParseTaxDetail() to add net assessed tax.
 * 01/11/2017 16.9.1    Modify Ala_UpdateTaxBase() fix paid date and ignore cancelled records.
 *                      Modify Ala_ParseTaxBase() to add BillStatus which can be used in the future
 *                      MOdify Ala_Load_TaxBase() to open input file in binary to avoid EOF embedded char.
 * 04/02/2017 16.13.5   Reformat TRA in Ala_ParseTaxBase()
 * 05/30/2017 16.14.14  Add Ala_UpdateTaxBase() & Ala_LoadTaxDelq().  Load Tax Delq.
 * 05/07/2018 17.10.7.1 Cosmetic change
 * 05/11/2018 17.10.9   Fix bug in Ala_UpdateTaxBase() when new tax bill found
 * 10/30/2018 18.5.10   Fix bug when Main_TC not available
 * 12/07/2018 18.6.8    Fix bug in Ala_UpdateTaxBase() to remove blank records in TaxBase.
 *            18.6.8.1  Modify Ala_Load_TaxBase() to ignore book "000"
 * 05/09/2019 18.12.1   Auto select TCF or TCP to load tax.
 * 07/11/2019 19.0.2    Change SALE_USE_SCSALREC to SALE_USE_SCUPDXFR when calling ApplyCumSale()
 * 11/14/2019 19.5.2    Fix bill status of 2nd bill in Ala_ParseTaxBase()
 * 07/05/2020 20.1.1    Remove automatic import GrGr after successful load.  Use -Gi instead.
 * 07/06/2020 20.1.1.1  Remove automatic import Sale after successful load.  Use -Xsi instead.
 * 07/18/2020 20.1.6    Modify Ala_MergeRoll() to verify direction before assign to S_DIR or M_DIR.
 * 10/14/2020 20.2.17   Add -Xa and remove -Ma option. Add Ala_ConvStdChar().  
 *                      Modify Ala_MergeChar() to update MISCIMPR_SF and add Amenities to R01.
 * 10/15/2020 20.2.19   Modify Ala_Load_TaxDelq() to allow processing without paid delq file.
 * 10/17/2020 20.3.1    Fix overflow in Ala_CreateLienRec() and Ala_MergeRoll() when GROSS is more than 2 billion.
 * 10/26/2020 20.3.6    Clean up Ala_MergeOwner().
 * 05/27/2021 20.8.2    Remove process owner info from Ala_Load_TaxBase() since we don't use it.
 * 05/29/2021 20.8.3    Modify tax loading process to allow update using TCP file.
 * 06/06/2021 20.9.1    Remove call to LoadLUTable() in LoadAla() since it's already loaded in MergeInit().
 * 07/06/2021 21.0.2    Modify Ala_MergeOwner() to fix known bad char and Ala_MergeChar() to populate QualityClass.
 * 04/26/2022 21.9.0    Use acGrBkTmpl as backup folder for GrGr files. 
 * 05/15/2022 21.9.1    Add -Xf to extract final values.
 * 06/20/2022 21.9.4    Fix Ala_UpdateTaxBase() set status to NOTAX if taxamt2 = 0.
 * 07/02/2022 22.0.0    Fix bug in Ala_ConvStdChar().
 * 08/25/2022 22.1.5    Modify Ala_MergeRoll() to add CLCA values to OTHER_VAL as county website display.
 * 02/06/2024 23.6.1    Fix HO_FL in Ala_MergeRoll().
 * 08/31/2024 24.1.5    Modify Ala_MergeRoll() to add Add ExeType.
 * 01/31/2025 24.4.9    Modify Ala_ParseTaxDetail() to correct TCFlg.
 *
 ****************************************************************************/

#include "stdafx.h"
#include "Prodlib.h"
#include "Utils.h"
#include "CountyInfo.h"
#include "R01.h"
#include "RecDef.h"
#include "Tables.h"
#include "XlatTbls.h"
#include "SaleRec.h"
#include "Logs.h"
#include "LoadOne.h"
#include "Utils.h"
#include "doOwner.h"
#include "doSort.h"
#include "doGrgr.h"
#include "FormatApn.h"
#include "CharRec.h"
#include "UseCode.h"
#include "Update.h"
#include "LOExtrn.h"
#include "PQ.h"
#include "MergeAla.h"
#include "Outrecs.h"
#include "Tax.h"

static   char *gpCondition = "AEFGP";
static   long  lCharMatch, lCharSkip, lSaleSkip, lSaleMatch, lTaxSkip, lTaxMatch, lTaxAdd;
static   int   iCharRecSize;
static   FILE  *fdChar;
extern   FILE  *fdSale;

/********************************** ConvertSale ******************************
 *
 * Convert DocNum format from YY NNNNNN to YYYYNNNNNN.
 *
 ******************************************************************************/

int Ala_ConvertSale(char *pSaleIn, char *pSaleOut)
{
   char     acBuf[512], acTmp[32];
   char     *pTmp;

   FILE     *fdSaleIn, *fdSaleOut;
   SALE_REC *pSale = (SALE_REC *)&acBuf[0];

   long     lCnt=0, iTmp;

   // Open cumulative file
   LogMsg("Open cumulative sale file %s", pSaleIn);
   fdSaleIn = fopen(pSaleIn, "r");
   if (fdSaleIn == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", pSaleIn);
      return 2;
   }

   LogMsg("Open output sale file %s", pSaleOut);
   fdSaleOut = fopen(pSaleOut, "w");
   if (fdSaleOut == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", pSaleOut);
      return 2;
   }

   while (!feof(fdSaleIn))
   {
      if (!(pTmp = fgets(acBuf, 512, fdSaleIn)))
         break;

      // Reformat DocNum
      if (pSale->acDocNum[2] == ' ')
      {
         // If first two-digit is DocYear, convert to 4-digit year
         if (!memcmp(pSale->acDocNum, (char *)&pSale->acDocDate[2], 2))
         {
            iTmp = sprintf(acTmp, "%.4s%.6s ", pSale->acDocDate, (char *)&pSale->acDocNum[3]);
            memcpy(pSale->acDocNum, acTmp, iTmp);
         }
      }

      // Write to output file
      fputs(acBuf, fdSaleOut);

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);

   }

   // Close files
   fclose(fdSaleOut);
   fclose(fdSaleIn);

   LogMsg("Total output records:       %u", lCnt);
   printf("\nTotal output records: %u", lCnt);

   lRecCnt = lCnt;
   return 0;
}

/******************************** Ala_MergeOwner *****************************
 *
 * Return 0 if successful, >0 if error
 *
 *****************************************************************************/

void Ala_MergeOwner(char *pOutbuf, char *pNames)
{
   int   iTmp, iTmp1, iRet;
   char  acOwners[128], acTmp[128], acSave[64], acSave1[64];
   char  acName2[64];
   char  *pTmp, *pTmp1;

   OWNER myOwner;

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "042 4287005", 11))
   //   iTmp = 0;
#endif

   // Clear output buffer if needed
   removeNames(pOutbuf, false);

   // Remove multiple spaces
   pTmp = strcpy(acOwners, pNames);
   iTmp = iTmp1 = 0;
   while (*pTmp)
   {
      // Replace known bad character
      if (*pTmp == -84)
      {
         LogMsg("*** Replace bad char (%#X) in Ala_MergeOwner(): APN=%.12s", *pTmp, pOutbuf);
         *pTmp = 'N';
      } else if (*pTmp < 0)
      {
         LogMsg("*** Bad char %0.2x found in Ala_MergeOwner(): APN=%.12s", *pTmp, pOutbuf);
         *pTmp = '?';
      }


      // Mark name with numeric value
      if (isdigit(*pTmp))
         iTmp1++;

      // skip unwanted chars at beginning of name
      if (*pTmp < '1' && iTmp == 0)
      {
         pTmp++;
         continue;
      }

      acTmp[iTmp++] = *pTmp++;

      // Remove '.' too
      if (*pTmp == '.' || *pTmp == '`')
         pTmp++;
   }
   if (acTmp[iTmp-1] == ' ')
      iTmp -= 1;
   acTmp[iTmp] = 0;

   // Check for COMMON AREA; if present, copy Owner to Legal Desc
   if (!memcmp(acOwners, "COMMON AREA ", 12) ||
       !memcmp(acOwners, "COMON AREA ",  11) ||
       !memcmp(acOwners, "COM AREA ",    9)
      )
   {
      iRet = updateLegal(pOutbuf, acTmp);
      if (iRet > iMaxLegal)
         iMaxLegal = iRet;
      iTmp = strlen(acTmp);
   }

   acName2[0] = 0;
   acSave1[0] = 0;
   acSave[0] = 0;

   // Replace known bad chars
   replChar(acTmp, 181, '&');

   // Terminate name if found char 26
   if (pTmp = strchr(acTmp, 26))
      *pTmp = 0;

   // Save owner
   strcpy(acOwners, acTmp);
   iTmp = blankRem(acOwners);
   vmemcpy(pOutbuf+OFF_NAME1, acOwners, SIZ_NAME1, iTmp);

   // Update vesting
   updateVesting(myCounty.acCntyCode, acOwners, pOutbuf+OFF_VEST, pOutbuf+OFF_ETAL_FLG);

   // Leave name as is if number present
   if (iTmp1)
   {
      vmemcpy(pOutbuf+OFF_NAME_SWAP, acOwners, SIZ_NAME_SWAP);
      return;
   }

   // Remove ETAL
   if (pTmp=strstr(acTmp, " ETAL")) 
      memset(pTmp, ' ', 5);
   else if (pTmp=strstr(acTmp, " ET AL")) 
      memset(pTmp, ' ', 6);

   // We keep first two name only, put the rest in name2
   // TAYLOR JAMES H & MERRIEL & LEFEVRE PATRICIA M & RE   
   // YEUNG NORMAN K & ROSA S TRS & YEUNG ROSA S TR
   if (pTmp = strchr(acTmp, '&'))
   {
      if (pTmp1 = strchr(pTmp+1, '&'))
      {
         *pTmp1++ = 0;
         //if (*pTmp1 == ' ') pTmp1++;
         //strcpy(acName2, pTmp1);
      }
   }

   // Save TR, CO-TR, TRS, CO-TRS too
   if ((pTmp=strstr(acTmp, " TR &"))  || 
       (pTmp=strstr(acTmp, " TRS &")) || 
       (pTmp=strstr(acTmp, " TRUST &")))
   {
      pTmp1 = strchr(pTmp, '&');
      *pTmp1 = 0;                            // Remove '&'
      //strcpy(acSave1, pTmp);                 
      memset(pTmp, ' ', strlen(pTmp));
      *pTmp1 = '&';                          // Restore '&'
   }

   // Save data to append later
   if (pTmp1=strstr(acTmp, "TRUST"))
   {
      if (pTmp=strstr(acTmp, " FAM"))
      {
         //strcpy(acSave, pTmp);
         *pTmp = 0;
      } else if (pTmp=strstr(acTmp, " REVOC"))
      {
         //strcpy(acSave, pTmp);
         *pTmp = 0;
      } else if (pTmp=strstr(acTmp, " INDIV"))
      {
         //strcpy(acSave, pTmp);
         *pTmp = 0;
      } else if (pTmp=strstr(acTmp, " LIV"))
      {
         //strcpy(acSave, pTmp);
         *pTmp = 0;
      } else if (pTmp=strstr(acTmp, " AMENDED "))
      {
         //strcpy(acSave, pTmp);
         *pTmp = 0;
      } else
      {
         if (pTmp1 > &acTmp[0])        
            *(--pTmp1) = 0;
      }
   } else if ((pTmp=strstr(acTmp, " TR ")) || (pTmp=strstr(acTmp, " TRS ")) )
   {
      pTmp1 = strchr(pTmp+1, ' ');
      //*pTmp1 = 0;                            // Remove '&'
      //strcpy(acSave, pTmp);                 
      *pTmp = 0;
   }

   blankRem(acTmp);

   // Now parse owners
   acTmp[63] = 0;
   splitOwner(acTmp, &myOwner, 0);  
   vmemcpy(pOutbuf+OFF_NAME_SWAP, myOwner.acSwapName, SIZ_NAME_SWAP);
}

/********************************** Ala_MergeRoll *****************************
 *
 * Return 0 if successful, otherwise error
 *
 *****************************************************************************/

int Ala_MergeRoll(char *pOutbuf, char *pRollRec, int iLen, int iFlag)
{
   ALA_ROLL *pRec;
   char     acTmp[256], acTmp1[256], *pTmp;
   int      iRet, iTmp, iTmp1;
   unsigned long lTmp; //, lLand, lImpr, lPers, lHpp, lFixture, lCLCA_Land, lCLCA_Impr;

   // Replace tab char with 0
   iTmp = replChar(pRollRec, 9, 0);
   if (iTmp < ALA_ROLL_FLDS)
      return -1;

   pRec = (ALA_ROLL *)pRollRec;
   iRet = 0;

   iTmp = strlen(pRec->Apn);
   if (iTmp < 13 || iTmp > RSIZ_APN)
      LogMsg("??? Questionable APN=%s", pRec->Apn);

   if (iFlag & CREATE_R01)
   {
      // Clear output buffer
      memset(pOutbuf, ' ', iRecLen);

      // Start copying data
      memcpy(pOutbuf, pRec->Apn, strlen(pRec->Apn));
      memcpy(pOutbuf+OFF_CO_NUM, "01ALA", 5);

      // Year assessed
      memcpy(pOutbuf+OFF_YR_ASSD, myCounty.acYearAssd, 4);

      // Format APN
      iTmp = formatApn(pRec->Apn, acTmp, &myCounty);
      memcpy(pOutbuf+OFF_APN_D, acTmp, iTmp);

      // Create MapLink and output new record
      iTmp = formatMapLink(pRec->Apn, acTmp, &myCounty);
      memcpy(pOutbuf+OFF_MAPLINK, acTmp, iTmp);

      // Create index map link
      if (getIndexPage(acTmp, acTmp1, &myCounty))
         memcpy(pOutbuf+OFF_IMAPLINK, acTmp1, iTmp);

      // HO Exempt
      long lExe1 = atol(pRec->HOEX);
      long lExe2 = atol(pRec->OTEX);
      if (lExe1 > 0)
      {
         *(pOutbuf+OFF_HO_FL) = '1';      // 'Y'
         memcpy(pOutbuf+OFF_EXE_CD1, "HO", 2);
      } else
      {
         *(pOutbuf+OFF_HO_FL) = '2';      // 'N'
         if (lExe2 > 0)
            memcpy(pOutbuf+OFF_EXE_CD1, "99", 2);
      }

      // Exemp total
      lTmp = lExe1+lExe2;
      if (lTmp > 0)
      {
         sprintf(acTmp, "%*d", SIZ_EXE_TOTAL, lTmp);
         memcpy(pOutbuf+OFF_EXE_TOTAL, acTmp, SIZ_EXE_TOTAL);
      }

      // Create exemption type
      pTmp = makeExeType(pOutbuf+OFF_EXE_TYPE, pOutbuf+OFF_EXE_CD1, pOutbuf+OFF_EXE_CD2, pOutbuf+OFF_EXE_CD3, (IDX_TBL4 *)&ALA_Exemption);

      // Land
      long lLand = atol(pRec->Land);
      if (lLand > 0)
      {
         sprintf(acTmp, "%*d", SIZ_LAND, lLand);
         memcpy(pOutbuf+OFF_LAND, acTmp, SIZ_LAND);
      }

      // Improve
      long lImpr = atol(pRec->Impr);
      if (lImpr > 0)
      {
         sprintf(acTmp, "%*d", SIZ_IMPR, lImpr);
         memcpy(pOutbuf+OFF_IMPR, acTmp, SIZ_IMPR);
      }

      // Other value
      long lCLCA_Impr = atol(pRec->CLCA_Impr);
      long lCLCA_Land = atol(pRec->CLCA_Land);
      long lFixture = atol(pRec->Fixt);
      long lPers = atol(pRec->Pers);
      long lHpp = atol(pRec->Hpp);

      // Conservation values are not consider property value
      //lTmp = lPers + lHpp + lFixture;
      // 08/25/2022 - CLCA is taxable, put them back into OTHER_VAL
      lTmp = lPers + lHpp + lFixture + lCLCA_Land + lCLCA_Impr;
      if (lTmp > 0)
      {
         sprintf(acTmp, "%*u", SIZ_OTHER, lTmp);
         memcpy(pOutbuf+OFF_OTHER, acTmp, SIZ_OTHER);

         if (lFixture > 0)
         {
            sprintf(acTmp, "%u         ", lFixture);
            memcpy(pOutbuf+OFF_FIXTR, acTmp, SIZ_FIXTR);
         }
         if (lPers > 0)
         {
            sprintf(acTmp, "%u         ", lPers);
            memcpy(pOutbuf+OFF_PERSPROP, acTmp, SIZ_PERSPROP);
         }
         if (lHpp > 0)
         {
            sprintf(acTmp, "%u         ", lHpp);
            memcpy(pOutbuf+OFF_HOUSEHOLD_PP, acTmp, SIZ_HOUSEHOLD_PP);
         }
      }

      if (lCLCA_Impr > 0)
      {
         sprintf(acTmp, "%u         ", lCLCA_Impr);
         memcpy(pOutbuf+OFF_CLCA_IMPR, acTmp, SIZ_CLCA_IMPR);
      }
      if (lCLCA_Land > 0)
      {
         sprintf(acTmp, "%u         ", lCLCA_Land);
         memcpy(pOutbuf+OFF_CLCA_LAND, acTmp, SIZ_CLCA_LAND);
      }

#ifdef _DEBUG
      //if (!memcmp(pOutbuf, "519 174701101", iApnLen) )
      //   iRet = 0;
#endif

      // Gross total
      lTmp += lLand+lImpr;
      if (lTmp > 0)
         sprintf(acTmp, "%*u", SIZ_GROSS, lTmp);
      else
         strcpy(acTmp, BLANK32);
      memcpy(pOutbuf+OFF_GROSS, acTmp, SIZ_GROSS);

      // Ratio
      if (lImpr > 0)
      {
         sprintf(acTmp, "%*d", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
         memcpy(pOutbuf+OFF_RATIO, acTmp, SIZ_RATIO);
      }
   }

   // TRA
   iRet = sprintf(acTmp, "0%s%s", pRec->TRA_Prime, pRec->TRA_Sec);
   memcpy(pOutbuf+OFF_TRA, acTmp, iRet);

   // Situs - StrNum field can be number street name
   ADR_REC  sAdr;
   bool     bNoStrNum = false;
   char     sUnitNo[64];

   // Clear old data
   removeSitus(pOutbuf);

   if (pTmp=strchr(myBTrim(pRec->S_StreetNum), ' ') )
   {
      if (*(pTmp+1) > '1' && (*(pTmp+1) <= '9'))
         *pTmp = '-';
      else if (pRec->S_StreetNum[strlen(pRec->S_StreetNum)-1] == '/')
         strcat(pRec->S_StreetNum, "2");
      else if (*(pTmp+1) == '1')
         strcat(pRec->S_StreetNum, "/2");
      else if (isalpha(*(pTmp+1)))
      {
         *pTmp = '-';
      } else
         *pTmp = 0;
   }

   if (pRec->S_StreetNum[0] > ' ')
      sprintf(acTmp, "%s %s", pRec->S_StreetNum, pRec->S_StreetName);
   else
   {
      strcpy(acTmp, pRec->S_StreetName);
      bNoStrNum = true;
   }

   iTmp = blankRem(acTmp);
   memcpy(pOutbuf+OFF_S_ADDR_D, acTmp, iTmp);

   iTmp1 = blankRem(pRec->S_UnitNo, SIZ_S_UNITNO);
   if (iTmp1 > 0)
   {
      if (pRec->S_UnitNo[0] == '#')
         strcpy(sUnitNo, pRec->S_UnitNo);
      else
      {
         sUnitNo[0] = '#';
         strcpy(&sUnitNo[1], pRec->S_UnitNo);
      }

      iTmp1 = strlen(sUnitNo);
      memcpy(pOutbuf+OFF_S_UNITNO, sUnitNo, iTmp1);
      memcpy(pOutbuf+OFF_S_ADDR_D+iTmp+1, sUnitNo, iTmp1);
   }

   if (bNoStrNum)
   {
      strcpy(acTmp1, "0 ");
      strcat(acTmp1, acTmp);
      strcpy(acTmp, acTmp1);
   }
   parseAdr1_2(&sAdr, acTmp);

   if (!memcmp(pRec->S_StreetName, "ST  ", 4))
   {
      strcpy(sAdr.strName, pRec->S_StreetNum);
      strcpy(sAdr.strSfx, "1");
   } else if (!memcmp(pRec->S_StreetName, "AV  ", 4))
   {
      strcpy(sAdr.strName, pRec->S_StreetNum);
      strcpy(sAdr.strSfx, "3");
      iTmp = sprintf(acTmp1, "%s AVE", sAdr.strName);
      memcpy(pOutbuf+OFF_S_ADDR_D, acTmp1, iTmp);
   } else if (!memcmp(pRec->S_StreetName, "RD  ", 4))
   {
      strcpy(sAdr.strName, pRec->S_StreetNum);
      strcpy(sAdr.strSfx, "2");
   } else if (!memcmp(pRec->S_StreetName, "E ST  ", 6) )
   {
      //LogMsg("%.13s - %s - %s", pRec->Apn, pRec->S_StreetNum, pRec->S_StreetName);
      if (pRec->S_StreetNum[2] > '9')
      {
         strcpy(sAdr.strName, pRec->S_StreetNum);
         strcpy(sAdr.strDir, "E");
         iTmp = sprintf(acTmp1, "E %s ST       ", sAdr.strName);
      } else if (pRec->S_StreetNum[0] > '0')
      {
         iTmp = strlen(pRec->S_StreetNum);
         memcpy(pOutbuf+OFF_S_STRNUM, pRec->S_StreetNum, iTmp);
         memcpy(pOutbuf+OFF_S_HSENO, pRec->S_StreetNum, iTmp);
         iTmp = sprintf(acTmp1, "%s %s", pRec->S_StreetNum, pRec->S_StreetName);
      } else
         iTmp = sprintf(acTmp1, "%s", pRec->S_StreetName);
      memcpy(pOutbuf+OFF_S_ADDR_D, acTmp1, iTmp);
   } else if (sAdr.lStrNum > 0)
   {
      iTmp = sprintf(acTmp, "%d", sAdr.lStrNum);
      memcpy(pOutbuf+OFF_S_STRNUM, acTmp, iTmp);
      if ((pTmp = strchr(pRec->S_StreetNum, '-')) && *(pTmp+1) > '0')
         vmemcpy(pOutbuf+OFF_S_HSENO, pRec->S_StreetNum, SIZ_S_HSENO);
      else
      {
         if (sAdr.strSub[0] > ' ')
            vmemcpy(pOutbuf+OFF_S_STR_SUB, sAdr.strSub, SIZ_S_STR_SUB);
         memcpy(pOutbuf+OFF_S_HSENO, acTmp, iTmp);
      }
   }

   if (isDir(sAdr.strDir))
      memcpy(pOutbuf+OFF_S_DIR, sAdr.strDir, strlen(sAdr.strDir));
   vmemcpy(pOutbuf+OFF_S_STREET, sAdr.strName, SIZ_S_STREET);
   vmemcpy(pOutbuf+OFF_S_SUFF, sAdr.strSfx, SIZ_S_SUFF);

   acTmp1[0] = 0;
   if (pRec->S_City[0] > ' ')
   {
      City2Code(pRec->S_City, acTmp, pOutbuf);
      if (acTmp[0] != ' ')
      {
         memcpy(pOutbuf+OFF_S_CITY, acTmp, SIZ_S_CITY);
         strcpy(acTmp1, GetCityName(atoi(acTmp)));
      } else
         strcpy(acTmp1, pRec->S_City);
   }

   memcpy(pOutbuf+OFF_S_ST, "CA", 2);
   if (acTmp1[0])
   {
      sprintf(acTmp, "%s, CA ", acTmp1);
      if (pRec->S_Zip[0] > '0')
      {
         memcpy(pOutbuf+OFF_S_ZIP, pRec->S_Zip, SIZ_S_ZIP);
         strcat(acTmp, pRec->S_Zip);
      }
      vmemcpy(pOutbuf+OFF_S_CTY_ST_D, acTmp, SIZ_S_CTY_ST_D);
   }

   // Doc date - update only if xfer date is newer than current.  This
   //            prevents update over GrGr data which is more up to date.
   long lPrevXfer;

   lPrevXfer = atoin(pOutbuf+OFF_TRANSFER_DT, SIZ_TRANSFER_DT);
   lTmp = atol(pRec->Doc_Dt);
   if (lTmp > lPrevXfer && lTmp < lToday)
   {
      memcpy(pOutbuf+OFF_TRANSFER_DT, pRec->Doc_Dt, SIZ_TRANSFER_DT);

      // Doc#
      strcpy(acTmp, pRec->Doc_Prefix);
      if (acTmp[0] > ' ')
      {
         if (acTmp[2] == ' ')
         {
            acTmp[2] = '*';
            acTmp[3] = 0;
         }
         strcat(acTmp, pRec->Doc_Series);
         vmemcpy(pOutbuf+OFF_TRANSFER_DOC, acTmp, SIZ_TRANSFER_DOC);
      }
   }

   // UseCode
   if (pRec->UseCode[0] > ' ')
   {
      strcpy(acTmp, pRec->UseCode);
      blankPad(acTmp, SIZ_USE_CO);
      memcpy(pOutbuf+OFF_USE_CO, acTmp, SIZ_USE_CO);
      updateStdUse(pOutbuf+OFF_USE_STD, acTmp, RSIZ_USECODE, pOutbuf);
   } else
      memcpy(pOutbuf+OFF_USE_STD, USE_UNASGN, SIZ_USE_STD);

   // Set full exemption flag
   iTmp = atoin(acTmp, 2);
   if (iTmp > 2 && iTmp < 6)
      *(pOutbuf+OFF_FULL_EXEMPT) = 'Y';
   else
      *(pOutbuf+OFF_FULL_EXEMPT) = ' ';

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "048A7086028", 11))
   //   iTmp = 0;
#endif

   // Remove old mailing
   removeMailing(pOutbuf, true);

   // Care Of
   iTmp = remUChar((unsigned char *)&pRec->M_CareOf[0], (unsigned char)(0x8D), RSIZ_OWNERS-1);
   if (iTmp < RSIZ_OWNERS-1)
      LogMsg("??? Remove known bad char in CARE_OF, apn=%s", pRec->Apn);

   iTmp = replUnPrtChar(pRec->M_CareOf, ' ', 0);
   if (iTmp > 0)
      LogMsg("??? Bad char found in CARE_OF, apn=%s", pRec->Apn);
   updateCareOf(pOutbuf, pRec->M_CareOf, RSIZ_OWNERS);

   // Scan bad char in M_Street
   iTmp = replChar(pRec->M_Street, (unsigned char)(0xE4), 'O', RSIZ_MSTRNAME-1);
   if (iTmp > 0)
      LogMsg("??? Replace known bad char in M_STREET, apn=%s", pRec->Apn);

   iTmp = remUChar((unsigned char *)&pRec->M_Street[0], (unsigned char)(0x8D), RSIZ_MSTRNAME-1);
   if (iTmp < RSIZ_MSTRNAME-1)
      LogMsg("??? Remove known bad char in M_STREET, apn=%s", pRec->Apn);

   iTmp = replUnPrtChar(pRec->M_Street, ' ', RSIZ_MSTRNAME-1);
   if (iTmp > 0)
      LogMsg("??? Bad char found in M_STREET field, apn=%s", pRec->Apn);

   // Scan bad char in M_City
   iTmp = replChar(pRec->M_City, (unsigned char)(0xE4), 'O', RSIZ_MCITY-1);
   if (iTmp > 0)
      LogMsg("??? Replace known bad char in M_CITY, apn=%s", pRec->Apn);

   iTmp = remUChar((unsigned char *)&pRec->M_City[0], (unsigned char)(0x1A), RSIZ_MCITY-1);
   if (iTmp < RSIZ_MCITY-1)
      LogMsg("??? Remove known bad char in M_CITY, apn=%s", pRec->Apn);

   iTmp = replUnPrtChar(pRec->M_City, ' ', RSIZ_MCITY-1);
   if (iTmp > 0)
      LogMsg("??? Bad char found in M_CITY field, apn=%s", pRec->Apn);

   // Special case  10/24/2013
   if (!memcmp(pRec->M_Street, "460 B TAYLOR", 8))
      pRec->M_Street[3] = '-';

   // Mailing
   sUnitNo[0] = 0;
   if (pRec->M_UnitNo[0] > ' ')
   {
      if (pRec->M_UnitNo[0] == '#')
         strcpy(sUnitNo, pRec->M_UnitNo);
      else
      {
         sUnitNo[0] = '#';
         strcpy(&sUnitNo[1], pRec->M_UnitNo);
      }
   } else if (pTmp=strchr(pRec->M_Street, '#'))
   {
      strcpy(sUnitNo, pTmp);
      *pTmp = 0;
      if (pTmp=strchr(sUnitNo, '-'))
         *pTmp = 0;
   }
   iTmp = blankRem(sUnitNo, SIZ_M_UNITNO);
   if (iTmp > 1)
      memcpy(pOutbuf+OFF_M_UNITNO, sUnitNo, iTmp);

   // Check for 1/2 that attached to strnum, then insert a space in between
   if ((pTmp = strstr(pRec->M_Street, "1/2")) && isdigit(*(pTmp-1)))
   {
      strcpy(acTmp, pTmp);
      *pTmp = ' ';
      strcpy(pTmp+1, acTmp);
   }

   ADR_REC sMailAdr;
   iTmp = blankRem(pRec->M_Street);
   if (memcmp(pRec->M_Street, "     ", 5))
   {
      parseAdr1(&sMailAdr, pRec->M_Street);
      parseAdr2(&sMailAdr, pRec->M_City);
      if (sMailAdr.lStrNum > 0)
      {
         iTmp = sprintf(acTmp, "%d", sMailAdr.lStrNum);
         memcpy(pOutbuf+OFF_M_STRNUM, acTmp, iTmp);
         vmemcpy(pOutbuf+OFF_M_STR_SUB, sMailAdr.strSub, SIZ_M_STR_SUB);
      }
      if (isDir(sAdr.strDir))
         memcpy(pOutbuf+OFF_M_DIR, sMailAdr.strDir, strlen(sMailAdr.strDir));
      memcpy(pOutbuf+OFF_M_STREET, sMailAdr.strName, strlen(sMailAdr.strName));
      memcpy(pOutbuf+OFF_M_SUFF, sMailAdr.strSfx, strlen(sMailAdr.strSfx));
      memcpy(pOutbuf+OFF_M_CITY, sMailAdr.City, strlen(sMailAdr.City));
      memcpy(pOutbuf+OFF_M_ST, sMailAdr.State, strlen(sMailAdr.State));
      iRet = 0;
   } else
      iRet |= 2;

   // Format ADDR_D 
   iTmp = sprintf(acTmp, "%s %s", pRec->M_Street, sUnitNo);
   vmemcpy(pOutbuf+OFF_M_ADDR_D, acTmp, SIZ_M_ADDR_D, iTmp);

   iTmp = blankRem(pRec->M_City);
   vmemcpy(pOutbuf+OFF_M_CTY_ST_D, pRec->M_City, SIZ_M_CTY_ST_D, iTmp);
   lTmp = atol(pRec->M_Zip);
   if (isdigit(pRec->M_Zip[0]))
   {
      memcpy(pOutbuf+OFF_M_ZIP, pRec->M_Zip, SIZ_M_ZIP);
      memcpy(pOutbuf+OFF_M_ZIP4, pRec->M_Zip4, SIZ_M_ZIP4);
   }

   // Owner
   if (memcmp(pRec->Owners, "     ", 5))
   {
      Ala_MergeOwner(pOutbuf, pRec->Owners);
   } else
      iRet |= 1;

   return iRet;
}

/******************************* Ala_Grgr2Sale *******************************
 *
 * Convert Grgr_Exp.sls Sale_Exp.dat then append to cum sale file
 * Return number of output records if successful, <0 if error
 *
 * This function should be used when loading LDR only
 *
 *****************************************************************************

long Ala_Grgr2Sale(char *pCnty)
{
   long  lCnt, iTmp;
   char  acRec[512], acTmp[512], acInfile[_MAX_PATH], acOutfile[_MAX_PATH];

   sprintf(acInfile, acGrGrTmpl, pCnty, "SLS");
   sprintf(acOutfile, acSaleTmpl, pCnty, "DAT");
   LogMsg("Convert %s to %s", acInfile, acOutfile);

   if (!_access(acInfile, 0))
   {
      // Sort output file and dedup
      sprintf(acRec, "OUTREC(%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%%REP,50,\"&H20\",%%REP,14,\"&H20\",CRLF)",
         OFF_GR_APN+1, SALE_SIZ_APN,
         OFF_GR_DOCNUM+1, SALE_SIZ_DOCNUM,
         OFF_GR_DOCDATE+1, SALE_SIZ_DOCDATE,
         OFF_GR_TITLE+1, SALE_SIZ_DOCTYPE,
         OFF_GR_SALE+1, SALE_SIZ_SALEPRICE);

      strcpy(acTmp,"S(17,16,C,A,1,12,C,A) F(TXT) DUPO(1,46,125,10) ");
      strcat(acTmp, acRec);
      lCnt = sortFile(acInfile, acOutfile, acTmp);
      if (lCnt > 0)
      {
         // Append Sale_Exp.dat to cum sale file
         iTmp = appendTxtFile(acOutfile, acCSalFile);
         if (iTmp)
         {
            LogMsg("***** Error appending %s to %s", acOutfile, acCSalFile);
            lCnt = -3;
         }
      } else
      {
         LogMsg("***** Error creating %s", acOutfile);
         lCnt = -2;
      }
   } else
      lCnt = -1;
   return lCnt;
}
*/

/******************************** Ala_LoadGrGr *******************************
 *
 * Return 0 if successful, >0 if error
 *
 *****************************************************************************/

//int CreateAlaGrGrFile(char *pCnty)
int Ala_LoadGrGr(char *pCnty)
{
   char     *pTmp, acRec[MAX_RECSIZE];
   char     acTmp[256], cFileCnt=1;
   char     acGrGrIn[_MAX_PATH], acGrGrOut[_MAX_PATH], acGrGrBak[_MAX_PATH];
   struct   _finddata_t  sFileInfo;

   FILE     *fdIn, *fdOut;
   ALA_DOC  *pDoc;
   ALA_NAME *pName;
   ALA_APN  *pApn;
   ALAGRGR  curGrGrRec, savGrGrRec;

   int      iRet, iGrGrLen, iTmp, iMatched=0, iNotMatched=0;
   int      iGrantors, iGrantees, iApn1, iApn2, iApn3, iApn4;
   BOOL     bEof=false;
   long     lCnt, lHandle, lSalePrice, lTax, lTmp;

   // Get raw file name
   GetIniString(pCnty, "GrGrIn", "", acGrGrIn, _MAX_PATH, acIniFile);
   
   // Prepare backup folder
   sprintf(acGrGrBak, acGrBkTmpl, pCnty, acToday);
   if (_access(acGrGrBak, 0))
      _mkdir(acGrGrBak);

   iGrGrLen = GetPrivateProfileInt(pCnty, "GrGrRecSize", 102, acIniFile);

   // Open Input file
   lHandle = _findfirst(acGrGrIn, &sFileInfo);
   if (lHandle > 0)
   {
      pTmp = strrchr(acGrGrIn, '\\');
      if (pTmp)
         *pTmp = 0;
      iRet = 0;
   } else
   {
      bGrGrAvail = false;
      LogMsg("*** No new GrGr file found.");
      return -1;
   }

   // Create Output file
   sprintf(acGrGrOut, acEGrGrTmpl, pCnty, "dat");

   LogMsg("Open output file %s", acGrGrOut);
   fdOut = fopen(acGrGrOut, "w");
   if (fdOut == NULL)
   {
      printf("Error creating GrGr output file: %s\n", acGrGrOut);
      return -2;
   }

   // Create backup folder
   if (_access(acGrGrBak, 0))
      _mkdir(acGrGrBak);

   lCnt = 0;  
   while (!iRet)
   {
      sprintf(acTmp, "%s\\%s", acGrGrIn, sFileInfo.name);
      LogMsg("Open input file %s", acTmp);
      fdIn = fopen(acTmp, "r");

      // Update GrGr file date
      lTmp = getFileDate(acTmp);
      if (lTmp > lLastGrGrDate)
         lLastGrGrDate = lTmp;

      while (!feof(fdIn))
      {
         pTmp = fgets(acRec, iGrGrLen, fdIn);
         if (!pTmp) break;

         pTmp = myTrim(acRec);
         switch (*pTmp)
         {
            case '1':
               pDoc = (ALA_DOC *)&acRec;

               // Initialize new record
               memset((void *)&curGrGrRec, ' ', sizeof(ALAGRGR));

               // Process GD only
               if (!memcmp(pDoc->DocType, "A03", 3))
               //if (pDoc->DocType[2] == '3')
               {
                  lTax = atoin(pDoc->CountyTax, 10);
                  lSalePrice = (long)(lTax*100)/11;
                  if (lSalePrice > 0)
                  {
                     sprintf(acTmp, "%*d", SIZ_GR_SALE, lSalePrice);
                     memcpy(curGrGrRec.SalePrice, acTmp, SIZ_GR_SALE);

                     // swapping sale date from MMDDYYYY to YYYYMMDD
                     memcpy((char *)&acTmp[4], pDoc->DocDate, SIZ_GR_DOCDATE);
                     memcpy((char *)&acTmp[0], (char *)&acTmp[8], 4);
                     memcpy(curGrGrRec.DocDate, acTmp, SIZ_GR_DOCDATE);

                     sprintf(acTmp, "%.10s      ", pDoc->DocNumber);
                     memcpy(curGrGrRec.DocNum, acTmp, SIZ_GR_DOCNUM);
                     //memcpy(acTmp, pDoc->DocType, SIZ_GR_DOCTYPE);
                     //acTmp[SIZ_GR_DOCTYPE] = 0;
                     //blankPad(acTmp, SIZ_GR_TITLE);
                     //memcpy(curGrGrRec.DocTitle, acTmp, SIZ_GR_TITLE);
                     strcpy(acTmp, "1");                          // Assume GD
                     blankPad(acTmp, SIZ_GR_TITLE);
                     memcpy(curGrGrRec.DocTitle, acTmp, SIZ_GR_TITLE);
                     iGrantors = iGrantees = 0;
                  }
               }
               break;

            case '2':
               // Merge Grantor
               pName = (ALA_NAME *)&acRec;

               if (lSalePrice > 0 && !memcmp(curGrGrRec.DocNum, pName->DocNumber, 10))
               {
                  iGrantors++;
                  iTmp = atoin(pName->SeqNo, 4);
                  if (iTmp > 0 && iTmp < 3)
                  {
                     if (SIZ_GR_NAME > strlen(pName->Name))
                     {
                        memcpy(curGrGrRec.Grantor[iTmp-1], pName->Name, strlen(pName->Name));
                        //blankPad(curGrGrRec.Grantor[iTmp-1], SIZ_GR_NAME);
                     } else
                        memcpy(curGrGrRec.Grantor[iTmp-1], pName->Name, SIZ_GR_NAME);
                  }
               }
               break;

            case '3':
               // Merge Grantee
               pName = (ALA_NAME *)&acRec;

               if (lSalePrice > 0 && !memcmp(curGrGrRec.DocNum, pName->DocNumber, 10))
               {
                  iGrantees++;
                  iTmp = atoin(pName->SeqNo, 4);
                  if (iTmp > 2)
                     curGrGrRec.MoreName = 'Y';
                  else
                  {
                     if (SIZ_GR_NAME > strlen(pName->Name))
                     {
                        memcpy(curGrGrRec.Grantee[iTmp-1], pName->Name, strlen(pName->Name));
                        //blankPad(curGrGrRec.Grantee[iTmp-1], SIZ_GR_NAME);
                     } else
                        memcpy(curGrGrRec.Grantee[iTmp-1], pName->Name, SIZ_GR_NAME);
                  }

               }
               break;

            case '4':
               // Merge APN
               pApn = (ALA_APN *)&acRec;

               char  bBookAlpha;
               if (lSalePrice > 0 && !memcmp(curGrGrRec.DocNum, pApn->DocNumber, 10))
               {
                  // Only process record with APN
                  if (pApn->Apn2[0] != ' ')
                  {
                     if (pApn->Apn1[3] > '9')
                     {
                        bBookAlpha = toupper(pApn->Apn1[3]);
                        iApn1 = atoin(pApn->Apn1, 3);
                        if (bBookAlpha == 'O' && iApn1 == 0)
                           bBookAlpha = '0';
                     } else
                     {
                        iApn1 = atoin(pApn->Apn1, 4);
                        if (iApn1 == 0)
                           bBookAlpha = '0';
                        else
                           bBookAlpha = ' ';
                     }

                     iApn2 = atoin(pApn->Apn2, 4);
                     iApn3 = atoin(pApn->Apn3, 3);
                     iApn4 = atoin(pApn->Apn4, 2);

                     // Check for book alpha present
                     sprintf(acTmp, "%.3d%c%.4d%.3d%.2d       ", iApn1, bBookAlpha, iApn2, iApn3, iApn4);

                     // Save current record
                     memcpy(curGrGrRec.APN, acTmp, SIZ_GR_APN);
                     memcpy((void *)&savGrGrRec, (void *)&curGrGrRec, sizeof(ALAGRGR));
                     curGrGrRec.CRLF[0] = 10;
                     curGrGrRec.CRLF[1] = 0;
                     curGrGrRec.filler = 0;
                     fputs((char *)&curGrGrRec,fdOut);
                     iMatched++;
                  }
               } else
               {
                  if (curGrGrRec.DocNum[0] != ' ')
                     LogMsg("Doc#=%.10s/%.10s Sale Price=%d", curGrGrRec.DocNum, pApn->DocNumber, lSalePrice);
               }
               break;
         }
         lCnt++;
         if (!(lCnt % 1000))
            printf("\r%d", lCnt);
      }

      LogMsg("Number of matched records: %d", iMatched);
      LogMsg("Total records so far     : %d", lCnt);

      fclose(fdIn);

      // Move file
      sprintf(acTmp, "%s\\%s", acGrGrBak, sFileInfo.name);
      sprintf(acRec, "%s\\%s", acGrGrIn, sFileInfo.name);
      rename(acRec, acTmp);

      // Find next file
      iRet = _findnext(lHandle, &sFileInfo);
   }

   // Close handle
   _findclose(lHandle);

   // Close files
   if (fdOut)
      fclose(fdOut);

   LogMsg("Total matched records    : %d", iMatched);
   LogMsg("Total processed records  : %u", lCnt);
   iRet = 0;

   // Create sale file if there is new GrGr record
   if (lCnt > 0)
   {
      // Sort output file and dedup
      sprintf(acRec, "OUTREC(%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%%REP,50,\"&H20\",%%REP,14,\"&H20\",CRLF)",
         OFF_GR_APN+1, SALE_SIZ_APN,
         OFF_GR_DOCNUM+1, SALE_SIZ_DOCNUM,
         OFF_GR_DOCDATE+1, SALE_SIZ_DOCDATE,
         OFF_GR_TITLE+1, SALE_SIZ_DOCTYPE,
         OFF_GR_SALE+1, SALE_SIZ_SALEPRICE);

      strcpy(acTmp,"S(17,16,C,A,1,12,C,A) F(TXT) DUPO(1,46,125,10) ");
      strcat(acTmp, acRec);

      // Create Sale_Exp.dat from GrGr_Exp.dat
      strcpy(acGrGrIn, acGrGrOut);
      sprintf(acGrGrOut, acSaleTmpl, pCnty, "DAT");
      lCnt = sortFile(acGrGrIn, acGrGrOut, acTmp);

      // Update accumulated sale file
      if (lCnt > 0)
      {
         // Append Sale_Exp.dat to Sale_Exp.sls
         sprintf(acGrGrBak, acSaleTmpl, pCnty, "SLS");
         iRet = appendTxtFile(acGrGrOut, acGrGrBak);
         if (iRet)
         {
            LogMsg("***** Error appending %s to %s", acGrGrOut, acGrGrBak);
            iRet = -3;
         } else
         {
            // Sort Sale_Exp.sls
            sprintf(acGrGrOut, acSaleTmpl, pCnty, "SRT");
            strcpy(acTmp,"S(1,14,C,A,27,8,C,A,15,12,C,A,57,10,C,D) F(TXT) DUPO(1,34)");
            lCnt = sortFile(acGrGrBak, acGrGrOut, acTmp);
            if (lCnt > 0)
            {
               iRet = remove(acGrGrBak);
               if (!iRet)
                  iRet = rename(acGrGrOut, acGrGrBak);
               else
                  LogMsg("***** Unable to remove old file %s", acGrGrBak);
            }
         }

         // Append Grgr_Exp.dat to Grgr_Exp.sls
         sprintf(acGrGrBak, acEGrGrTmpl, pCnty, "SLS");
         iRet = appendTxtFile(acGrGrIn, acGrGrBak);
         if (iRet)
         {
            LogMsg("***** Error appending %s to %s", acGrGrIn, acGrGrBak);
            iRet = -3;
         }
      } else
      {
         LogMsg("***** Error creating %s", acGrGrOut);
         iRet = -3;
      }
   } else
      LogMsg("*** No new GrGr file found.");

   LogMsg("Total output records after dedup: %u", lCnt);
   printf("\nTotal output records after dedup: %u", lCnt);
   return iRet;
}

/********************************** MergeAlaRoll ****************************
 *
 *
 ****************************************************************************/

int MergeAlaRoll(int iSkip)
{
   char     *pTmp, acRec[MAX_RECSIZE], acBuf[MAX_RECSIZE], acRollRec[MAX_RECSIZE];
   char     cFileCnt=1;
   char     acRawFile[_MAX_PATH], acOutFile[_MAX_PATH];

   HANDLE   fhIn, fhOut;

   int      iRet, iApnLen, iTmp, iRollUpd=0, iNewRec=0, iRetiredRec=0;
   DWORD    nBytesRead;
   DWORD    nBytesWritten;
   BOOL     bRet, bEof;
   long     lRet=0, lCnt=0;

   iApnLen = myCounty.iApnLen;
   sprintf(acRawFile, acRawTmpl, "ALA", "ALA", "S01");
   sprintf(acOutFile, acRawTmpl, "ALA", "ALA", "R01");

   // Rename files for processing
   if (_access(acRawFile, 0))
   {
      if (!_access(acOutFile, 0))
         rename(acOutFile, acRawFile);
      else
      {
         LogMsg("Missing input file %s.  Please recheck!");
         return 1;
      }
   }

   // Open roll file
   LogMsg("Open Roll file %s", acRollFile);
   fdRoll = fopen(acRollFile, "r");
   if (fdRoll == NULL)
   {
      printf("Error opening roll file: %s\n", acRollFile);
      return 2;
   }

   // Open Input file
   LogMsg("Open input file %s", acRawFile);
   fhIn = CreateFile(acRawFile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhIn == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening input file: %s\n", acRawFile);
      return 2;
   }

   // Open Output file
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening output file: %s\n", acOutFile);
      return 2;
   }

   // Get first RollRec
   pTmp = fgets((char *)&acRollRec[0], iRollLen, fdRoll);
   bEof = (pTmp ? false:true);

   // Copy skip record
   memset(acBuf, ' ', iRecLen);
   while (iSkip-- > 0)
   {
      ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);
      // Ignore record start with 99999999
      //if (memcmp(acBuf, "99999999", 8))
         bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
   }

   iNoMatch=iBadCity=iBadSuffix=0;

   // Merge loop
   while (!bEof)
   {
      bRet = ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);

      // Check for EOF
      if (!bRet)
      {
         LogMsg("Error reading input file %s (%f)", acRawFile, GetLastError());
         break;
      }

      if (iRecLen != nBytesRead)
         break;

      NextRollRec:
      // Overwrite 'O' with '0'
      if (acRollRec[3] == 'O')
         acRollRec[3] = '0';
      iTmp = memcmp(acBuf, acRollRec, iApnLen);
      if (!iTmp)
      {
         // Merge roll data
         iRet = Ala_MergeRoll(acBuf, acRollRec, iRollLen, UPDATE_R01);
         iRollUpd++;

         bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);

         // Read next roll record
         pTmp = fgets(acRollRec, iRollLen, fdRoll);

         if (!pTmp)
            bEof = true;    // Signal to stop
      } else
      {
         if (iTmp > 0)       // Roll not match, new roll record
         {
            LogMsg0("*** New roll record : %.*s (%d) ***", iApnLen, acRollRec, lCnt);

            // Create new R01 record
            iRet = Ala_MergeRoll(acRec, acRollRec, iRollLen, CREATE_R01);
            iNewRec++;

            bRet = WriteFile(fhOut, acRec, iRecLen, &nBytesWritten, NULL);
            lCnt++;

            // Get next roll record
            pTmp = fgets(acRollRec, iRollLen, fdRoll);

            if (!pTmp)
               bEof = true;    // Signal to stop
            else
               goto NextRollRec;
         } else
         {
            // Record may be retired - drop it
            if (bDebug)
               LogMsg0("*** Roll not match (retired record?) : R01->%.*s ***", iApnLen, acBuf);
            iRetiredRec++;
            continue;
         }
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);

      if (!bRet)
      {
         LogMsg("Error occurs: %d\n", GetLastError());
         lRet = WRITE_ERR;
         break;
      }
   }

   // Do the rest of the file
   while (!bEof)
   {
      LogMsg0("*** New roll record : %.*s (%d) ***", iApnLen, acRollRec, lCnt);

      // Create new R01 record
      iRet = Ala_MergeRoll(acRec, acRollRec, iRollLen, CREATE_R01);
      iNewRec++;

      bRet = WriteFile(fhOut, acRec, iRecLen, &nBytesWritten, NULL);
      lCnt++;

      // Get next roll record
      pTmp = fgets(acRollRec, MAX_RECSIZE, fdRoll);

      if (!pTmp)
         bEof = true;    // Signal to stop
   }

   // Close files
   if (fdRoll)
      fclose(fdRoll);
   if (fhOut)
      CloseHandle(fhOut);
   if (fhIn)
      CloseHandle(fhIn);

   LogMsg("Total output records:       %u", lCnt);
   LogMsg("Total new records:          %u", iNewRec);
   LogMsg("Total retired records:      %u", iRetiredRec);
   LogMsg("Total bad-city records:     %u", iBadCity);
   LogMsg("Total bad-suffix records:   %u", iBadSuffix);

   printf("\nTotal output records: %u", lCnt);

   lRecCnt = lCnt;
   return 0;
}

/****************************** Ala_ConvStdChar ******************************
 *
 * Convert char file "ALACO_VARIABLE_UPDATE.TXT" to STDCHAR format.
 *
 *****************************************************************************/

int Ala_ConvStdChar(char *pInfile)
{
   FILE     *fdIn, *fdOut;
   char     acInbuf[1024], acOutbuf[2048], acTmpFile[256], acTmp[256], acCode[32], *pRec;
   int      iRet, iTmp, iCnt=0, lLotSqft, lLotAcres;
   STDCHAR  *pCharRec=(STDCHAR *)&acOutbuf[0];

   LogMsg0("Converting char file %s", pInfile);
   if (!(fdIn = fopen(pInfile, "r")))
      return -1;

   sprintf(acTmpFile, "%s\\%s\\%s_char.tmp", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   if (!(fdOut = fopen(acTmpFile, "w")))
   {
      fclose(fdIn);
      LogMsg("***** Error creating output file %s", acTmpFile);
      return -2;
   }

   while (!feof(fdIn))
   {
      pRec = fgets(acInbuf, 1024, fdIn);

      if (!pRec)
         break;

      replNull(acInbuf);
#ifdef _DEBUG
      //if (!memcmp(acInbuf, "002 007301800", 12))
      //   iRet = 0;
#endif

      iRet = ParseStringIQ(pRec, 9, ALA_CHAR_FLDS, apTokens);
      if (iRet < ALA_CHAR_FLDS)
      {
         LogMsg("*** Bad CHAR record (%d): %s", iCnt, pRec);
         continue;
      }

      memset(acOutbuf, ' ', sizeof(STDCHAR));

      // Apn
      memcpy(pCharRec->Apn, apTokens[ALA_CHAR_APN], strlen(apTokens[ALA_CHAR_APN]));
      
      // UseCode
      vmemcpy(pCharRec->LandUse, apTokens[ALA_CHAR_USECODE], SIZ_CHAR_SIZE8);

      // Quality/class
      vmemcpy(pCharRec->QualityClass, apTokens[ALA_CHAR_CLASS], SIZ_CHAR_QCLS);
      iTmp = 0;
      if (*apTokens[ALA_CHAR_CLASS] >= 'A')
         pCharRec->BldgClass = *apTokens[ALA_CHAR_CLASS];
      if (*apTokens[ALA_CHAR_CLASS] >= '0' && *apTokens[ALA_CHAR_CLASS] <= '9')
         iTmp = atol(apTokens[ALA_CHAR_CLASS]);
      else  if (*(apTokens[ALA_CHAR_CLASS]+1) >= '0' && *(apTokens[ALA_CHAR_CLASS]+1) <= '9')
         iTmp = atol(apTokens[ALA_CHAR_CLASS]+1);
      if (iTmp > 0)
      {
         sprintf(acTmp, "%d", iTmp);
         iRet = Quality2Code(acTmp, acCode, NULL);
         if (iRet >= 0)
            pCharRec->BldgQual = acCode[0];
      }

      // YrBlt
      iTmp = atoi(apTokens[ALA_CHAR_YRBLT]);
      if (iTmp > 1600 && iTmp != 1900)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(pCharRec->YrBlt, acTmp, iRet);
      }

      // YrEff
      iTmp = atoi(apTokens[ALA_CHAR_EFFYR]);
      if (iTmp > 1600 && iTmp != 1900)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(pCharRec->YrEff, acTmp, iRet);
      }

      // Units
      iTmp = atol(apTokens[ALA_CHAR_UNITS]);
      if (iTmp > 0)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(pCharRec->Units, acTmp, iRet);
      }

      // Lot Sqft - 9999999V9
      lLotSqft = atol(apTokens[ALA_CHAR_LOTSIZE]);
      if (lLotSqft > 0)
      {
         if (*apTokens[ALA_CHAR_ACRE] == 'A')
         {
            if (lLotSqft < 100000)
            {
               lLotAcres = lLotSqft*100;
               lLotSqft *= SQFT_FACTOR_10;
            } else
            {
               lLotSqft /= 10;
               lLotAcres = (long)((double)lLotSqft*SQFT_MF_1000);
               LogMsg("??? Questionable lot acreage: %u acres, APN=%s", lLotSqft, apTokens[ALA_CHAR_APN]);
            }
         } else
         {
            lLotSqft /= 10;
            lLotAcres = (long)((double)lLotSqft*SQFT_MF_1000);
         }

         iRet = sprintf(acTmp, "%d", lLotSqft);
         memcpy(pCharRec->LotSqft, acTmp, iRet);
         iRet = sprintf(acTmp, "%d", lLotAcres);
         memcpy(pCharRec->LotAcre, acTmp, iRet);
      }

      // BldgSqft
      iTmp = atol(apTokens[ALA_CHAR_BLDGAREA]);
      if (iTmp > 100)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(pCharRec->BldgSqft, acTmp, iRet);
      }

      // Addition
      iTmp = atol(apTokens[ALA_CHAR_ADDITIONS]);
      if (iTmp > 100)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(pCharRec->MiscSqft, acTmp, iRet);
      }

      // Misc area - Skip until request

      // Rentable space
      iTmp = atol(apTokens[ALA_CHAR_RENTABLE]);
      if (iTmp > 100)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(pCharRec->NetRental, acTmp, iRet);
      }

      // Parking
      if (*apTokens[ALA_CHAR_PARKING] > '0')
      {
         if (*apTokens[ALA_CHAR_PARKING] == 'G')
            pCharRec->ParkType[0] = 'Z';
         else if (*apTokens[ALA_CHAR_PARKING] == 'C')
            pCharRec->ParkType[0] = 'C';
         else if (*apTokens[ALA_CHAR_PARKING] == 'X')
            pCharRec->ParkType[0] = '2';              // GARAGE/CARPORT

         if (*apTokens[ALA_CHAR_PARKING] > 'A')
            iTmp = atol(apTokens[ALA_CHAR_PARKING]+1);
         else
            iTmp = atol(apTokens[ALA_CHAR_PARKING]);
         if (iTmp > 0)
         {
            iRet = sprintf(acTmp, "%d", iTmp);
            memcpy(pCharRec->ParkSpace, acTmp, iRet);
         }
      }

      // Stories 99V9
      iTmp = atol(apTokens[ALA_CHAR_STORIES]);
      if (iTmp > 0)
      {
         iRet = sprintf(acTmp, "%.1f    ", (double)(iTmp/10.0));
         memcpy(pCharRec->Stories, acTmp, iRet);
      }

      // Rooms
      iTmp = atol(apTokens[ALA_CHAR_ROOMS]);
      if (iTmp > 0)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(pCharRec->Rooms, acTmp, iRet);
      }

      // Beds
      iTmp = atoi(apTokens[ALA_CHAR_BEDS]);
      if (iTmp > 0)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(pCharRec->Beds, acTmp, iRet);
      }

      // Baths - 9999V9
      int iFBath = atoi(apTokens[ALA_CHAR_BATHS]);
      if (iFBath > 0 && iFBath < 9999)
      {
         iRet = sprintf(acTmp, "%d", (long)(iFBath/10.0));
         memcpy(pCharRec->FBaths, acTmp, iRet);

         // Half bath
         iTmp = iFBath % 10;
         if (iFBath < 100 && iTmp > 0)
            pCharRec->HBaths[0] = '1';
      } else if (iFBath > 9998)
         LogMsg("*** Questionable number of bath rooms: %s [%s] - Ignored", apTokens[ALA_CHAR_BATHS], apTokens[ALA_CHAR_APN]);

      // Pool
      if (*apTokens[ALA_CHAR_POOL] == 'Y')
         pCharRec->Pool[0] = 'P';    // Pool

      // Condition - A/E/G/F/P
      for (iTmp = 0; iTmp < 5; iTmp++)
      {
         if (*apTokens[ALA_CHAR_CONDITION] == *(gpCondition+iTmp))
         {
            pCharRec->ImprCond[0] = *apTokens[ALA_CHAR_CONDITION];
            break;
         }
      }

      // View 
      switch (*apTokens[ALA_CHAR_VIEW])
      {
         case 'G':
            pCharRec->View[0] = '4';      // Good
            break;
         case 'A':
            pCharRec->View[0] = '3';      // Average
            break;
         case 'F':
            pCharRec->View[0] = '2';      // Fair
            break;
         case 'E':
            pCharRec->View[0] = '5';      // Excellent
            break;
         case 'P':
            pCharRec->View[0] = '9';      // Negative
            break;
      }

      // Elevator
      if (*apTokens[ALA_CHAR_ELEVATOR] == 'Y')
         pCharRec->HasElevator = *apTokens[ALA_CHAR_ELEVATOR];  // Y/N

      // Topo - (F)lat, (H)illy, (L)evel, (M)oderate, (S)teep 
      if (isalpha(*apTokens[ALA_CHAR_TOPO]))
         pCharRec->Topo[0] = *apTokens[ALA_CHAR_TOPO];  

      // Amenities - (T)ennis court, (H)ot tub, (S)ecurity, (R)ecreation/exercise room
      if (isalpha(*apTokens[ALA_CHAR_AMENITIES]))
         pCharRec->Amenities[0] = *apTokens[ALA_CHAR_AMENITIES];  

      // Slope
      // Unit Floor
      // Hazards

      // Bldgs
      iRet = atol(apTokens[ALA_CHAR_BLDGS]);
      if (iRet > 0 && iRet <= 99)
         vmemcpy(pCharRec->Bldgs, apTokens[ALA_CHAR_BLDGS], SIZ_CHAR_SIZE2);

      // Change date
      if (dateConversion(apTokens[ALA_CHAR_CHG_DATE], acTmp, MM_DD_YYYY_1))
         memcpy(pCharRec->Date_Updated, acTmp, 8);

      try {
         pCharRec->CRLF[0] = '\n';
         pCharRec->CRLF[1] = 0;
         fputs(acOutbuf, fdOut);
      } catch (...)
      {
         LogMsg("??? Exception occurs in Ala_ConvStdChar(),  APN=%s", apTokens[0]);
      }

      if (!(++iCnt % 1000))
         printf("\r%u", iCnt);
   }

   if (fdIn) fclose(fdIn);
   if (fdOut) fclose(fdOut);

   // Sort output on ASMT
   if (iCnt > 100)
   {
      LogMsg("Sorting char file %s --> %s", acTmpFile, acCChrFile);
      iRet = sortFile(acTmpFile, acCChrFile, "S(1,12,C,A) F(TXT)");
   } else
   {
      printf("\n");
      iRet = 0;
   }
   return iRet;
}

/********************************* Ala_MergeChar *****************************
 *
 * Do not use fgets() to read CHAR file since it might embed null char in record.
 *
 *****************************************************************************/

int Ala_MergeChar(char *pOutbuf)
{
   static   char acRec[1024], *pRec=NULL;
   char     acTmp[256], acCode[32];
   long     lBldgSqft, lLotAcres, lLotSqft, lTmp, lMiscSqft, lAddSqft;
   int      iRet, iTmp, iLoop, iBeds, iFBath, iRooms;
   ALA_CHAR *pChar;

   // Get first Char rec for first call
   if (!pRec)
   {
      iRet = fread(acRec, 1, iCharRecSize, fdChar);
      if (iRet == iCharRecSize)
         pRec = acRec;
   }

   pChar = (ALA_CHAR *)pRec;

   do
   {
      if (!pRec)
      {
         fclose(fdChar);
         fdChar = NULL;
         return 1;      // EOF
      }

      // Replace 'O' with '0'
      if (pChar->Apn[3] == 'O')
         pChar->Apn[3] = '0';

      // Compare Apn
      iLoop = memcmp(pOutbuf, pChar->Apn, CSIZ_APN);
      if (iLoop > 0)
      {
         if (bDebug)
            LogMsg0("Skip Char rec  %.*s", CSIZ_APN, pChar->Apn);
         iRet = fread(acRec, 1, iCharRecSize, fdChar);
         if (iRet != iCharRecSize)
         {
            pRec = NULL;
            break;
         }
         lCharSkip++;
      }
   } while (iLoop > 0);

   // If not match, return
   if (iLoop)
      return 1;

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "033 215802201", iApnLen))
   //   iTmp = 0;
#endif

   // Replace TAB char with 0
   iTmp = replChar(acRec, 9, 0, iCharRecSize);

   // Units
   iTmp = atoin(pChar->Units, CSIZ_UNITS);
   if (iTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_UNITS, iTmp);
      memcpy(pOutbuf+OFF_UNITS, acTmp, SIZ_UNITS);
   }

   // Quality/class
   if (isalpha(pChar->BldgCls[0]))
   {
      *(pOutbuf+OFF_BLDG_CLASS) = pChar->BldgCls[0];
      memcpy(pOutbuf+OFF_QUALITYCLASS, pChar->BldgCls, CSIZ_BLDGCLS);
   }
   iTmp = atoin(&pChar->BldgCls[1], 2);
   if (iTmp > 0)
   {
      sprintf(acTmp, "%c.%c", pChar->BldgCls[1], pChar->BldgCls[2]);
      iRet = Value2Code(acTmp, acCode, NULL);
      if (iRet >= 0)
         *(pOutbuf+OFF_BLDG_QUAL) = acCode[0];
   }

   // YrEff
   int iYrEff = atol(pChar->YrEff);
   if (iYrEff > 0 && iYrEff <= lToyear)
      memcpy(pOutbuf+OFF_YR_EFF, pChar->YrEff, SIZ_YR_BLT);
   else if (iYrEff > 0)
      LogMsg("*** Bad effective year: %d [%s]", iYrEff, pChar->Apn);

   // YrBlt
   lTmp = atol(pChar->YrBlt);
   if (lTmp > 0 && lTmp <= lToyear)
      memcpy(pOutbuf+OFF_YR_BLT, pChar->YrBlt, SIZ_YR_BLT);
   else if (lTmp > 0)
   {
      // Fix known bug
      if (iYrEff > 1900 && iYrEff <= lToyear)
         memcpy(pOutbuf+OFF_YR_BLT, pChar->YrEff, SIZ_YR_BLT);
      else
         LogMsg("*** Bad year built: %d [%s]", lTmp, pChar->Apn);
   }

   // Addition
   lAddSqft  = atoin(pChar->Add_Area, CSIZ_ADD_AREA);
   if (lAddSqft > 0)
   {
      sprintf(acTmp, "%*d", SIZ_MISCIMPR_SF, lAddSqft);
      memcpy(pOutbuf+OFF_MISCIMPR_SF, acTmp, SIZ_MISCIMPR_SF);
   }

   // To be used later
   lMiscSqft = atoin(pChar->Misc_Area,CSIZ_MISC_AREA);

   // BldgSqft
   lBldgSqft = atoin(pChar->BldgSqft, CSIZ_BLDGAREA);
   if (lBldgSqft > 0)
   {
      sprintf(acTmp, "%*d", SIZ_BLDG_SF, lBldgSqft);
      memcpy(pOutbuf+OFF_BLDG_SF, acTmp, SIZ_BLDG_SF);
   }

   // Lot Sqft - 9999999V9
   lLotSqft = atol(pChar->LotSize);
   if (lLotSqft > 0)
   {
      if (pChar->Acre_Flg[0] == 'A')
      {
         if (lLotSqft < 100000)
         {
            lLotAcres = lLotSqft*100;
            lLotSqft *= SQFT_FACTOR_10;
         } else
         {
            lLotSqft /= 10;
            lLotAcres = (long)((double)lLotSqft*SQFT_MF_1000);
            LogMsg("??? Questionable lot acreage: %u acres, APN=%s", lLotSqft, pChar->Apn);
         }
      } else
      {
         lLotSqft /= 10;
         lLotAcres = (long)((double)lLotSqft*SQFT_MF_1000);
      }

      sprintf(acTmp, "%*d", SIZ_LOT_SQFT, lLotSqft);
      memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);
      sprintf(acTmp, "%*d", SIZ_LOT_ACRES, lLotAcres);
      memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);
   }

   // Stories
   iTmp = atoin(pChar->Stories, 2);
   if (iTmp > 0)
   {
      sprintf(acTmp, "%d.%c    ", iTmp, pChar->Stories[2]);
      memcpy(pOutbuf+OFF_STORIES, acTmp, SIZ_STORIES);
   }

   // Rooms
   iRooms = atoin(pChar->Rooms, CSIZ_ROOMS);
   if (iRooms > 0)
   {
      sprintf(acTmp, "%*d", SIZ_ROOMS, iRooms);
      memcpy(pOutbuf+OFF_ROOMS, acTmp, SIZ_ROOMS);
   }

   // Beds
   iBeds = atoin(pChar->Beds, CSIZ_BEDS);
   if (iBeds > 0)
   {
      if (iBeds > 99)
         iBeds = 99;
      sprintf(acTmp, "%*d", SIZ_BEDS, iBeds);
      memcpy(pOutbuf+OFF_BEDS, acTmp, SIZ_BEDS);
   }

   // Baths - 9999V9
   iFBath = atoin(pChar->Baths, CSIZ_BATHS-1);
   if (iFBath > 0 && iFBath < 999)
   {
      iTmp = sprintf(acTmp, "%*d", SIZ_BATH_F, iFBath);
      memcpy(pOutbuf+OFF_BATH_F, acTmp, iTmp);

      // Half bath
      if (iFBath < 100 && pChar->Baths[CSIZ_BATHS-1] > '0')
         memcpy(pOutbuf+OFF_BATH_H, " 1", 2);
   } else if (iFBath > 998)
      LogMsg("*** Questionable number of bath rooms: %s [%s] - Ignored", pChar->Baths, pChar->Apn);

   // Pool
   if (pChar->Pool_Code[0] == 'Y')
      *(pOutbuf+OFF_POOL) = 'P';    // Pool
   else
      *(pOutbuf+OFF_POOL) = ' ';

   // Condition - A/E/G/F/P
   for (iTmp = 0; iTmp < 5; iTmp++)
   {
      if (pChar->Condition_Code[0] == *(gpCondition+iTmp))
      {
         *(pOutbuf+OFF_IMPR_COND) = pChar->Condition_Code[0];
         break;
      }
   }

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "014 126607200", 11))
   //   iRet = 0;
#endif
   // Parking
   if (memcmp(pChar->Parking_Code, "   ", 3))
   {
      if (pChar->Parking_Code[0] == 'G')
         *(pOutbuf+OFF_PARK_TYPE) = 'Z';
      else if (pChar->Parking_Code[0] == 'C')
         *(pOutbuf+OFF_PARK_TYPE) = 'C';
      else if (pChar->Parking_Code[0] == 'X')
         *(pOutbuf+OFF_PARK_TYPE) = '2';        // GARAGE/CARPORT

      if (pChar->Parking_Code[0] > 'A')
         iTmp = atol(&pChar->Parking_Code[1]);
      else
         iTmp = atol(pChar->Parking_Code);
      if (iTmp > 0)
      {
         sprintf(acTmp, "%d      ", iTmp);
         memcpy(pOutbuf+OFF_PARK_SPACE, acTmp, SIZ_PARK_SPACE);
      }
   }

   // View 
   switch (pChar->View_Code[0])
   {
      case 'G':
         *(pOutbuf+OFF_VIEW) = '4';    // Good
         break;
      case 'A':
         *(pOutbuf+OFF_VIEW) = '3';    // Average
         break;
      case 'F':
         *(pOutbuf+OFF_VIEW) = '2';    // Fair
         break;
      case 'E':
         *(pOutbuf+OFF_VIEW) = '5';    // Excellent
         break;
      case 'P':
         *(pOutbuf+OFF_VIEW) = '9';    // Negative
         break;
      default:
         *(pOutbuf+OFF_VIEW) = ' ';
   }

   // Elevator
   *(pOutbuf+OFF_ELEVATOR) = pChar->Elevator_Flg[0];  // Y/N

   // Topo - (F)lat, (H)illy, (L)evel, (M)oderate, (S)teep 
   *(pOutbuf+OFF_TOPO) = pChar->Topography_Code[0];   

   // Amenities
   *(pOutbuf+OFF_AMENITIES) = pChar->Amenities_Code[0];   

   // Slope
   // Unit Floor
   // Hazards

   // Bldgs
   iRet = atoi(pChar->Bldgs);
   if (iRet > 0 && iRet <= 99)
   {
      iTmp = sprintf(acTmp, "%*d", SIZ_BLDGS, iRet);
      memcpy(pOutbuf+OFF_BLDGS, acTmp, iTmp);
   } else if (iRet > 0)
      LogMsg("*** Questionable number of bldgs: %s [%s]", pChar->Bldgs, pChar->Apn);

   lCharMatch++;

   // Get next Char rec
   iRet = fread(acRec, 1, iCharRecSize, fdChar);
   if (iRet != iCharRecSize)
   {
      pRec = NULL;
      fclose(fdChar);
   }

   return 0;
}

/******************************** Ala_UpdateRoll ****************************
 *
 *
 ****************************************************************************/

int Ala_UpdateRoll(int iSkip)
{
   char     *pTmp, acRec[MAX_RECSIZE], acBuf[MAX_RECSIZE], acRollRec[MAX_RECSIZE];
   char     acRawFile[_MAX_PATH], acOutFile[_MAX_PATH];

   HANDLE   fhIn, fhOut;

   int      iRet, iTmp, iRollUpd=0, iNewRec=0, iRetiredRec=0;
   DWORD    nBytesRead;
   DWORD    nBytesWritten;
   BOOL     bRet, bEof;
   long     lRet=0, lCnt=0;

   sprintf(acRawFile, acRawTmpl, "ALA", "ALA", "S01");
   sprintf(acOutFile, acRawTmpl, "ALA", "ALA", "R01");

   // Rename files for processing
   if (_access(acRawFile, 0))
   {
      if (!_access(acOutFile, 0))
         rename(acOutFile, acRawFile);
      else
      {
         LogMsg("Missing input file %s.  Please recheck!", acRawFile);
         return -1;
      }
   }

   // Open roll file
   LogMsg("Open Roll file %s", acRollFile);
   if (iRollLen > 0)
      fdRoll = fopen(acRollFile, "rb");
   else
      fdRoll = fopen(acRollFile, "r");
   if (fdRoll == NULL)
   {
      LogMsg("Error opening roll file: %s\n", acRollFile);
      return -2;
   }

   // Open Char file
   LogMsg("Open Char file %s", acCharFile);
   fdChar = fopen(acCharFile, "r");
   if (fdChar == NULL)
   {
      LogMsg("Error opening Char file: %s\n", acCharFile);
      return -3;
   }

   // Open Input file
   LogMsg("Open input file %s", acRawFile);
   fhIn = CreateFile(acRawFile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhIn == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening input file: %s\n", acRawFile);
      return -4;
   }

   // Open Output file
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening output file: %s\n", acOutFile);
      return -5;
   }

   // Get first RollRec
   if (iRollLen > 0)
   {
      iTmp = fread(acRollRec, 1, iRollLen, fdRoll);
      bEof = (iTmp == iRollLen ? false:true);
   } else
   {
      pTmp = fgets(acRollRec, MAX_RECSIZE, fdRoll);
      bEof = (pTmp ? false:true);
   }

   // Copy skip record
   memset(acBuf, ' ', iRecLen);
   while (iSkip-- > 0)
   {
      ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);
      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
   }

   iNoMatch=iBadCity=iBadSuffix=0;

   // Merge loop
   while (!bEof)
   {
      bRet = ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);

      // Check for EOF
      if (!bRet)
      {
         LogMsg("Error reading input file %s (%f)", acRawFile, GetLastError());
         break;
      }

      if (iRecLen != nBytesRead)
         break;

      NextRollRec:
      // Overwrite 'O' with '0'
      if (acRollRec[3] == 'O')
         acRollRec[3] = '0';
      iTmp = memcmp(acBuf, acRollRec, iApnLen);
      if (!iTmp)
      {
         // Merge roll data
         iRet = Ala_MergeRoll(acBuf, acRollRec, iRollLen, UPDATE_R01);

         if (!iRet)
         {
            iRollUpd++;

            iRet = Ala_MergeChar(acBuf);
            bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
         }

         // Read next roll record
         if (iRollLen > 0)
         {
            iTmp = fread(acRollRec, 1, iRollLen, fdRoll);
            bEof = (iTmp == iRollLen ? false:true);
         } else
         {
            pTmp = fgets(acRollRec, MAX_RECSIZE, fdRoll);
            bEof = (pTmp ? false:true);
         }
      } else
      {
         if (iTmp > 0)       // Roll not match, new roll record
         {
            if (bDebug)
               LogMsg0("*** New roll record : %.*s (%d) ***", iApnLen, acRollRec, lCnt);

            // Create new R01 record
            iRet = Ala_MergeRoll(acRec, acRollRec, iRollLen, CREATE_R01);
            if (!iRet)
            {
               iNewRec++;

               iRet = Ala_MergeChar(acRec);
               bRet = WriteFile(fhOut, acRec, iRecLen, &nBytesWritten, NULL);
               lCnt++;
            }

            // Get next roll record
            if (iRollLen > 0)
            {
               iTmp = fread(acRollRec, 1, iRollLen, fdRoll);
               bEof = (iTmp == iRollLen ? false:true);
            } else
            {
               pTmp = fgets(acRollRec, MAX_RECSIZE, fdRoll);
               bEof = (pTmp ? false:true);
            }

            if (!bEof)
               goto NextRollRec;
         } else
         {
            // Record may be retired - drop it
            if (bDebug)
               LogMsg0("*** Roll not match (retired record?) : R01->%.*s ***", iApnLen, acBuf);
            iRetiredRec++;
            continue;
         }
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);

      if (!bRet)
      {
         LogMsg("Error occurs: %d\n", GetLastError());
         lRet = WRITE_ERR;
         break;
      }
   }

   // Do the rest of the file
   while (!bEof)
   {
      if (bDebug)
         LogMsg0("*** New roll record : %.*s (%d) ***", iApnLen, acRollRec, lCnt);

      // Create new R01 record
      iRet = Ala_MergeRoll(acRec, acRollRec, iRollLen, CREATE_R01);

      if (!iRet)
      {
         iNewRec++;

         iRet = Ala_MergeChar(acRec);

         bRet = WriteFile(fhOut, acRec, iRecLen, &nBytesWritten, NULL);
         lCnt++;
      }

      // Get next roll record
      if (iRollLen > 0)
      {
         iTmp = fread(acRollRec, 1, iRollLen, fdRoll);
         bEof = (iTmp == iRollLen ? false:true);
      } else
      {
         pTmp = fgets(acRollRec, MAX_RECSIZE, fdRoll);
         bEof = (pTmp ? false:true);
      }
   }

   // Close files
   if (fdRoll)
      fclose(fdRoll);
   if (fdChar)
      fclose(fdChar);
   if (fhOut)
      CloseHandle(fhOut);
   if (fhIn)
      CloseHandle(fhIn);

   LogMsg("Total output records:       %u", lCnt);
   LogMsg("Total new records:          %u", iNewRec);
   LogMsg("Total retired records:      %u", iRetiredRec);
   LogMsg("Total char matched:         %u", lCharMatch);
   LogMsg("Total char skiped:          %u", lCharSkip);
   LogMsg("Total bad-city records:     %u", iBadCity);
   LogMsg("Total bad-suffix records:   %u", iBadSuffix);

   printf("\nTotal output records: %u", lCnt);

   lRecCnt = lCnt;
   return 0;
}

/******************************** Ala_CreateRoll ****************************
 *
 *
 ****************************************************************************/

int Ala_CreateRoll(int iFirstRec /* 1=create header rec */)
{
   char     acBuf[MAX_RECSIZE], acRollRec[MAX_RECSIZE];
   char     acOutFile[_MAX_PATH];

   HANDLE   fhOut;

   int      iRet;
   DWORD    nBytesWritten;
   BOOL     bRet, bEof;
   long     lRet=0, lCnt=0;

   if (lLienYear >= lToyear-1)
      sprintf(acOutFile, acRawTmpl, "ALA", "ALA", "R01");
   else
   {
      sprintf(acBuf, "ALA_LDR%d", lLienYear);
      sprintf(acOutFile, acRawTmpl, "ALA", acBuf, "R01");

      GetIniString("ALA", "H1CharFile", "", acBuf, _MAX_PATH, acIniFile);    
      sprintf(acCharFile, acBuf, lLienYear, lLienYear);
   }

   // Open roll file
   sprintf(acBuf, acRollFile, lLienYear);
   strcpy(acRollFile, acBuf);
   LogMsg("Open Roll file %s", acRollFile);
   fdRoll = fopen(acRollFile, "rb");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", acRollFile);
      return -2;
   }

   // Open Char file
   LogMsg("Open Char file %s", acCharFile);
   fdChar = fopen(acCharFile, "r");
   if (fdChar == NULL)
   {
      LogMsg("Error opening Char file: %s\n", acCharFile);
      return -3;
   }

   // Open Output file
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error creating output file: %s\n", acOutFile);
      return -4;
   }

   // Output first header record
   if (iFirstRec > 0)
   {
      memset(acBuf, '9', iRecLen);
      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
   }

   // Get first RollRec
   //pTmp = fgets((char *)&acRollRec[0], iRollLen, fdRoll);
   //bEof = (pTmp ? false:true);
   iRet = fread(acRollRec, 1, iRollLen, fdRoll);
   bEof = ((iRet != iRollLen) ? true:false);

   // Init variables
   iNoMatch=iBadCity=iBadSuffix=0;

   // Merge loop
   while (!bEof)
   {
      // Overwrite 'O' with '0'
      if (acRollRec[3] == 'O')
         acRollRec[3] = '0';

#ifdef _DEBUG
      //if (!memcmp(acRollRec, "432 0108015", 11) )
      //   iRet = 0;
#endif

      // Create new R01 record
      iRet = Ala_MergeRoll(acBuf, acRollRec, iRollLen, CREATE_R01);
      if (!iRet)
      {
         if (fdChar)
            iRet = Ala_MergeChar(acBuf);
         bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
         lLDRRecCount++;
      } else 
      {
         if (iRet & 1)
            LogMsg0("*** Missing owner: %.20s", acRollRec);
         if (iRet & 2)
            LogMsg0("*** Missing MailAddr: %.20s", acRollRec);
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);

      if (!bRet)
      {
         LogMsg("Error writing to output file at record %d\n", lCnt);
         lRet = WRITE_ERR;
         break;
      }

      // Get next roll record
      //pTmp = fgets(acRollRec, iRollLen, fdRoll);
      iRet = fread(acRollRec, 1, iRollLen, fdRoll);
      bEof = ((iRet != iRollLen) ? true:false);
   }

   // Close files
   if (fdRoll)
      fclose(fdRoll);
   if (fdChar)
      fclose(fdChar);
   if (fhOut)
      CloseHandle(fhOut);

   LogMsg("Total input records:        %u", lCnt);
   LogMsg("Total output records:       %u", lLDRRecCount);
   LogMsg("Total char matched:         %u", lCharMatch);
   LogMsg("Total char skiped:          %u", lCharSkip);
   LogMsg("Total bad-city records:     %u", iBadCity);
   LogMsg("Total bad-suffix records:   %u", iBadSuffix);
   //LogMsg("Last recording date:        %u", lLastRecDate);
   printf("\nTotal output records: %u\n", lCnt);

   lRecCnt = lLDRRecCount;
   return 0;
}

/******************************* Ala_CreateLienRec ***************************
 *
 * Return 0 if successful, >0 if error
 *
 *****************************************************************************/

int Ala_CreateLienRec(char *pOutbuf, char *pRollRec)
{
   char     acTmp[256];
   int      iTmp;

   unsigned long  lTmp;
   ALA_ROLL *pRec = (ALA_ROLL *)pRollRec;
   LIENEXTR *pLienRec = (LIENEXTR *)pOutbuf;

   // Clear output buffer
   memset(pOutbuf, ' ', sizeof(LIENEXTR));

#ifdef _DEBUG
   //if (!memcmp(pRec->Apn, "519 174701101", iApnLen) )
   //   iTmp = 0;
#endif

   // Overwrite 'O' with '0'
   if (pRec->Apn[3] == 'O')
      pRec->Apn[3] = '0';
   memcpy(pLienRec->acApn, pRec->Apn, iApnLen);

   // Year assessed
   memcpy(pLienRec->acYear, myCounty.acYearAssd, 4);

   // HO Exe - Exemp total
   long lExe = atol(pRec->HOEX);
   if (lExe > 0)
   {
      iTmp = sprintf(acTmp, "%*d", sizeof(pLienRec->acExAmt), lExe);
      memcpy(pLienRec->acExAmt, acTmp, iTmp);
   }

   if (lExe == 7000)
      pLienRec->acHO[0] = '1';         // Y
   else
      pLienRec->acHO[0] = '2';         // N

   // Land
   long lLand = atol(pRec->Land);
   if (lLand > 0)
   {
      iTmp = sprintf(acTmp, "%*u", sizeof(pLienRec->acLand), lLand);
      memcpy(pLienRec->acLand, acTmp, iTmp);
   }

   // Improve - Ratio
   long lImpr = atol(pRec->Impr);
   if (lImpr > 0)
   {
      iTmp = sprintf(acTmp, "%*u", sizeof(pLienRec->acImpr), lImpr);
      memcpy(pLienRec->acImpr, acTmp, iTmp);

      iTmp = sprintf(acTmp, "%*d", sizeof(pLienRec->acRatio), (LONGLONG)lImpr*100/(lLand+lImpr));
      memcpy(pLienRec->acRatio, acTmp, iTmp);
   }

   // PP value
   long lPers = atol(pRec->Pers);
   // Fixture
   long lFixt = atol(pRec->Fixt);
   // Other values
   long lCLCA_Impr = atol(pRec->CLCA_Impr);
   long lCLCA_Land = atol(pRec->CLCA_Land);
   long lHpp = atol(pRec->Hpp);

   // Total other
   lTmp = lPers + lHpp + lFixt + lCLCA_Land + lCLCA_Impr;
   if (lTmp > 0)
   {
      iTmp = sprintf(acTmp, "%*u", sizeof(pLienRec->acOther), lTmp);
      memcpy(pLienRec->acOther, acTmp, iTmp);

      if (lPers > 0)
      {
         iTmp = sprintf(acTmp, "%*u", sizeof(pLienRec->acPP_Val), lPers);
         memcpy(pLienRec->acPP_Val, acTmp, iTmp);
      }
      if (lFixt > 0)
      {
         iTmp = sprintf(acTmp, "%*u", sizeof(pLienRec->acME_Val), lFixt);
         memcpy(pLienRec->acME_Val, acTmp, iTmp);
      }
      if (lCLCA_Land > 0)
      {
         iTmp = sprintf(acTmp, "%*u", sizeof(pLienRec->extra.Ala.CLCA_Land), lCLCA_Land);
         memcpy(pLienRec->extra.Ala.CLCA_Land, acTmp, iTmp);
      }
      if (lCLCA_Impr > 0)
      {
         iTmp = sprintf(acTmp, "%*u", sizeof(pLienRec->extra.Ala.CLCA_Impr), lCLCA_Impr);
         memcpy(pLienRec->extra.Ala.CLCA_Impr, acTmp, iTmp);
      }
      if (lHpp > 0)
      {
         iTmp = sprintf(acTmp, "%*u", sizeof(pLienRec->extra.Ala.Hpp), lHpp);
         memcpy(pLienRec->extra.Ala.Hpp, acTmp, iTmp);
      }
   }

   // Gross
   lTmp +=  lLand + lImpr;
   if (lTmp > 0)
   {
      iTmp = sprintf(acTmp, "%*u", sizeof(pLienRec->acGross), lTmp);
      memcpy(pLienRec->acGross, acTmp, iTmp);
   }

   // TRA
   iTmp = sprintf(acTmp, "0%s%s", pRec->TRA_Prime, pRec->TRA_Sec);
   memcpy(pLienRec->acTRA, acTmp, iTmp);

   pLienRec->LF[0] = '\n';
   pLienRec->LF[1] = 0;
   return 0;
}

/********************************* Ala_ExtrLien *****************************
 *
 *
 ****************************************************************************/

int Ala_ExtrLien()
{
   char  acBuf[512], acRollRec[MAX_RECSIZE], acOutFile[_MAX_PATH];
   FILE  *fdLien;
   long  lCnt=0, iTmp;

   // Open roll file
   LogMsg("Open Roll file %s", acRollFile);
   fdRoll = fopen(acRollFile, "rb");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", acRollFile);
      return 2;
   }

   // Open Output file
   sprintf(acOutFile, acLienTmpl, myCounty.acCntyCode, myCounty.acCntyCode);
   LogMsg("Open output file %s", acOutFile);
   fdLien = fopen(acOutFile, "w");
   if (fdLien == NULL)
   {
      LogMsg("***** Error opening Lien extract file: %s\n", acOutFile);
      return 4;
   }

   // Merge loop
   while (!feof(fdRoll))
   {
      // Get roll rec
      //if (!(pTmp = fgets(acRollRec, MAX_RECSIZE, fdRoll) ))
      //   break;
      iTmp = fread(acRollRec, 1, iRollLen, fdRoll);
      if (iTmp != iRollLen)
         break;

      //acRollRec[iTmp] = 0;

#ifdef _DEBUG
      //if (!memcmp(acRollRec, "041 41980370", 12))
      //   iTmp = 0;
#endif

      // Replace tab char with 0
      iTmp = replChar(acRollRec, 9, 0);
      if (iTmp > 38)
      {
         // Create new lien record
         Ala_CreateLienRec(acBuf, acRollRec);

         fputs(acBuf, fdLien);
         if (!(++lCnt % 1000))
            printf("\r%u", lCnt);
      } else
         LogMsg("*** Bad record %.13s", acRollRec);
   }

   // Close files
   if (fdRoll)
      fclose(fdRoll);
   if (fdLien)
      fclose(fdLien);

   LogMsgD("Total records output:     %d\n", lCnt);
   lRecCnt = lCnt;
   return 0;
}

/********************************* formatCSale *******************************
 *
 * Return -1 if bad record.  0 if OK.
 *
 *****************************************************************************/

int Ala_FormatSale(char *pFmtSale, bool bCreate)
{
   SCSAL_REC *pCSale= (SCSAL_REC *)pFmtSale;
   long     lTmp, iTmp;
   char     acTmp[32], *pTmp;

   // Inititalize
   if (bCreate)
   {
      memset(pFmtSale, ' ', sizeof(SCSAL_REC));
      memcpy(pCSale->Apn, apTokens[ALA_SALE_APN_S], strlen(apTokens[ALA_SALE_APN_S]));

      // SaleDate
      pTmp = dateConversion(apTokens[ALA_SALE_DOC_DT], acTmp, MM_DD_YYYY_1);
      if (pTmp)
         memcpy(pCSale->DocDate, acTmp, SALE_SIZ_DOCDATE);

      // Sale Doc
      lTmp = atol(apTokens[ALA_SALE_DOC_SER]);
      iTmp = sprintf(acTmp, "%s%.6d", myTrim(apTokens[ALA_SALE_DOC_PRE]), lTmp);
      memcpy(pCSale->DocNum, acTmp, iTmp);
      lTmp = atol(apTokens[ALA_SALE_SALEAMT]);
      if (lTmp > 0)
      {
         iTmp = sprintf(acTmp, "%*d", SALE_SIZ_SALEPRICE, lTmp);
         memcpy(pCSale->SalePrice, acTmp, iTmp);
      }

      lTmp = atol(apTokens[ALA_SALE_PARCEL_CNT]);
      if (lTmp > 1)
      {
         pCSale->MultiSale_Flg = 'Y';
         //pCSale->NumOfPrclXfer[0] = 'M';
         iTmp = sprintf(acTmp, "%d", lTmp);
         if (iTmp > SALE_SIZ_NOPRCLXFR)
            iTmp = SALE_SIZ_NOPRCLXFR;
         memcpy(pCSale->NumOfPrclXfer, acTmp, iTmp);
      }

      // DocType

      // Sale code
   }

   if (*(apTokens[ALA_SALE_NAMETYPE]+ALA_SALE_NAMETYPECD) == 'E')
   {
      iTmp = strlen(apTokens[ALA_SALE_NAME]);
      if (iTmp > SALE_SIZ_BUYER)
         iTmp = SALE_SIZ_BUYER;

      // Buyer - keep 2 names only
      if (pCSale->Name1[0] == ' ')
      {
         memcpy(pCSale->Name1, apTokens[ALA_SALE_NAME], iTmp); 
      } else if (pCSale->Name2[0] == ' ')
      {
         memcpy(pCSale->Name2, apTokens[ALA_SALE_NAME], iTmp); 
      }
   } else if (*(apTokens[ALA_SALE_NAMETYPE]+ALA_SALE_NAMETYPECD) == 'R')
   {
      iTmp = strlen(apTokens[ALA_SALE_NAME]);
      if (iTmp > SALE_SIZ_SELLER)
         iTmp = SALE_SIZ_SELLER;

      // Seller
      if (pCSale->Seller1[0] == ' ')
         memcpy(pCSale->Seller1, apTokens[ALA_SALE_NAME], iTmp); 
      else if (pCSale->Seller2[0] == ' ')
         memcpy(pCSale->Seller2, apTokens[ALA_SALE_NAME], iTmp);          
   }

   pCSale->CRLF[0] = '\n';
   pCSale->CRLF[1] = 0;

   return 0;
}

/****************************** Ala_UpdateCumSale ****************************
 *
 * Update history sale.  This function adds new sale record to cumulative file.
 *
 * Return 0 if successful, otherwise error
 *
 *****************************************************************************/

int Ala_UpdateCumSale(void)
{
   char     *pTmp, acCSalRec[1024], acSaleRec[1024], acTmp[256];
   char     acLastDoc[16];
   char     acOutFile[_MAX_PATH], acHistSale[_MAX_PATH], acTmpSale[_MAX_PATH];

   int      iRet, iTmp, iUpdateSale=0;
   long     lLstSaleDt=0, lCnt=0;
   SCSAL_REC *pSaleRec=(SCSAL_REC *)&acCSalRec[0];

   // Check current sale file
   if (_access(acSaleFile, 0))
   {
      LogMsg("***** Missing current sale file %s", acSaleFile);
      return -1;
   }

   // Open current sale
   LogMsg("Open current sale file %s", acSaleFile);
   fdSale = fopen(acSaleFile, "r");
   if (fdSale == NULL)
   {
      LogMsg("***** Error opening current sale file: %s\n", acSaleFile);
      return -2;
   }
   // Drop header record
   pTmp = fgets(acSaleRec, 1024, fdSale);

   // Create output file
   sprintf(acTmpSale, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "tmp");
   LogMsg("Create updated sale file %s", acTmpSale);
   fdCSale = fopen(acTmpSale, "w");
   if (fdCSale == NULL)
   {
      LogMsg("***** Error creating updated sale file: %s\n", acTmpSale);
      return -3;
   }

   // Read first history sale
   acCSalRec[0] = 0;

   // Merge loop
   while (!feof(fdSale))
   {
      // Get current sale
      pTmp = fgets(acSaleRec, 1024, fdSale);
      if (!pTmp)
      {
         // Output last record
         if (acCSalRec[0] > 0)
         {
            fputs(acCSalRec, fdCSale);
            iUpdateSale++;
            iTmp = atoin(pSaleRec->DocDate, 8);
            if (iTmp > lLstSaleDt && iTmp < lToday)
               lLstSaleDt = iTmp;
         }
         break;
      }

      iRet = ParseString(acSaleRec, 9, ALA_SALE_TOKENS, apTokens);
      if (iRet >= ALA_SALE_TOKENS)
      {
         // If same APN, update current sale record
         if (!strcmp(apTokens[ALA_SALE_DOC_SER], acLastDoc))
         {
            // Update current sale
            iRet = Ala_FormatSale(acCSalRec, false);
         } else
         {
            // Output current record
            if (acCSalRec[0] > 0)
            {
               fputs(acCSalRec, fdCSale);
               iUpdateSale++;
               iTmp = atoin(pSaleRec->DocDate, 8);
               if (iTmp > lLstSaleDt && iTmp < lToday)
                  lLstSaleDt = iTmp;
            }

            // Save current doc#
            strcpy(acLastDoc, apTokens[ALA_SALE_DOC_SER]);

            // Create new record
            iRet = Ala_FormatSale(acCSalRec, true);
         }
      } else
         LogMsg("Bad input record (%d): %s", iRet, apTokens[ALA_SALE_APN_S]);

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);

   }

   // Close files
   if (fdCSale)
      fclose(fdCSale);
   if (fdSale)
      fclose(fdSale);

   // Update sale history file
   sprintf(acHistSale, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "sls");
   sprintf(acOutFile,  acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "dat");

   // Sort/merge sale file - APN (asc), Saledate (asc), DocNum (asc)
   sprintf(acTmp, "S(1,%d,C,A,27,8,C,A,15,%d,C,A,57,10,C,D) DUPO(1,34)", iApnLen, SALE_SIZ_DOCNUM);
   iTmp = sortFile(acTmpSale, acOutFile, acTmp);
   if (iTmp > 0)
   {
      strcpy(acSaleFile, acOutFile);
      if (!_access(acHistSale, 0))
      {
         sprintf(acSaleRec, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "srt");
         sprintf(acTmpSale, "%s+%s", acOutFile, acHistSale);
         iTmp = sortFile(acTmpSale, acSaleRec, acTmp);
         if (iTmp > 0)
         {
            DeleteFile(acHistSale);
      
            // Rename srt to SLS file
            rename(acSaleRec, acHistSale);
            //strcpy(acSaleFile, acHistSale);
         }
      } else
      {
         rename(acOutFile, acHistSale);
      }
   }

   LogMsg("Total sale records processed:   %u", lCnt);
   LogMsg("Total cumulative sale records:  %u", iTmp);
   LogMsg("Total sale records updated:     %u", iUpdateSale);
   LogMsg("Latest recording date:          %u", lLstSaleDt);

   LogMsg("Update Sale History completed.");

   return 0;
}

/******************************* Ala_MergeHChar *****************************
 *
 * Update historical characteristics to old roll
 * Output: SALA.yyyy
 *
 ****************************************************************************/

int Ala_MergeHChar(char *pRawFile, char *pCharFile, int iSkip)
{
   HANDLE   fhIn, fhOut;
   DWORD    nBytesRead;
   DWORD    nBytesWritten;
   BOOL     bRet;
   long     lRet=0, lCnt=0;
   char     acBuf[MAX_RECSIZE], acOutFile[MAX_RECSIZE];

   LogMsg("Merge %s to %s", pCharFile, pRawFile);
   sprintf(acBuf, "%d", lLienYear);
   sprintf(acOutFile, acRawTmpl, "ALA", "ALA", acBuf);

   // Check inpur files
   if (_access(pRawFile, 0))
   {
      LogMsg("Missing input file %s.  Please recheck!", pRawFile);
      return -1;
   }
   if (_access(pCharFile, 0))
   {
      LogMsg("Missing input file %s.  Please recheck!", pCharFile);
      return -2;
   }

   // Open Char file
   LogMsg("Open Char file %s", pCharFile);
   fdChar = fopen(acCharFile, "r");
   if (fdChar == NULL)
   {
      LogMsg("Error opening Char file: %s\n", pCharFile);
      return -3;
   }

   // Open Input file
   LogMsg("Open input file %s", pRawFile);
   fhIn = CreateFile(pRawFile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhIn == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening input file: %s\n", pRawFile);
      return -4;
   }

   // Open Output file
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening output file: %s\n", acOutFile);
      return -5;
   }

   // Copy skip record
   memset(acBuf, ' ', iRecLen);
   bRet = true;
   while (iSkip-- > 0)
   {
      ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);
      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
   }

   // Merge loop
   while (bRet)
   {
      bRet = ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);

      // Check for EOF
      if (!bRet)
      {
         LogMsg("Error reading input file %s (%f)", pRawFile, GetLastError());
         break;
      }

      if (iRecLen != nBytesRead)
         break;

      if (fdChar)
         Ala_MergeChar(acBuf);
      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);

      if (!bRet)
      {
         LogMsg("Error occurs: %d\n", GetLastError());
         lRet = WRITE_ERR;
      }
   }

   // Close files
   if (fdChar)
      fclose(fdChar);
   if (fhOut)
      CloseHandle(fhOut);
   if (fhIn)
      CloseHandle(fhIn);

   LogMsg("Total output records:       %u", lCnt);
   LogMsg("Total char matched:         %u", lCharMatch);
   LogMsg("Total char skiped:          %u", lCharSkip);

   printf("\nTotal output records: %u", lCnt);

   lRecCnt = lCnt;
   return lRet;
}

/***************************** Ala_ParseTaxDetail ****************************
 *
 * Generate TAXDETAIL
 *
 * Return 0 if success
 *
 *****************************************************************************/

int Ala_ParseTaxDetail(char *pInBuf, FILE *fdDetail, FILE *fdAgency)
{
   char     acTmp[512], acOutbuf[512], acAgencyRec[512];
   int      iIdx;
   double   dVal1, dVal2, dRate;

   TAXDETAIL *pDetail= (TAXDETAIL *)&acOutbuf[0];
   TAXAGENCY *pAgency= (TAXAGENCY *)&acAgencyRec[0], *pResult;
   ALCO_SEC  *pInRec = (ALCO_SEC *)pInBuf;

   // Clear output buffer
   memset(acOutbuf, 0, sizeof(TAXDETAIL));

   // APN
   memcpy(acTmp, pInRec->APN, TSIZ_APN);
   strcpy(pDetail->Apn, myTrim(acTmp, TSIZ_APN));

   // Tracer number
   memcpy(pDetail->BillNum, pInRec->Tracer_Number, TSIZ_TRACER_NUMBER);

   // Tax Year
   sprintf(pDetail->TaxYear, "%d", lTaxYear);

#ifdef _DEBUG
   //if (!memcmp(pInRec->APN, "001 011503700", iApnLen))
   //   iIdx = 0;
#endif

   // Assessed value
   dVal1  = atofn(pInRec->Tax_Av_1St, TSIZ_TAX_AV_1ST);
   dVal1 += atofn(pInRec->Tax_Av_2Nd, TSIZ_TAX_AV_1ST);
   if (dVal1 > 0.0)
   {
      sprintf(pDetail->TaxAmt, "%.2f", dVal1);
      memcpy(pDetail->TaxCode, "001", 3);
      dRate = atofn(pInRec->Taxrate_Av, TSIZ_TAXRATE_FLOOD);
      sprintf(pDetail->TaxRate, "%.4f", dRate);
      pDetail->TC_Flag[0] = '1';

      pResult = findTaxAgency(pDetail->TaxCode);
      if (pResult)
      {
         pDetail->TC_Flag[0] = pResult->TC_Flag[0];

         memset(acAgencyRec, 0, sizeof(TAXAGENCY));
         strcpy(pAgency->Code,  pResult->Code);
         strcpy(pAgency->Agency,pResult->Agency);
         strcpy(pAgency->Phone, pResult->Phone);
         // Same tax code may have different rate.  Do not put it in Agency table.
         //strcpy(pAgency->TaxRate, pDetail->TaxRate);
         pAgency->TC_Flag[0] = pResult->TC_Flag[0];
         Tax_CreateAgencyCsv(acTmp, (TAXAGENCY *)&acAgencyRec);
         fputs(acTmp, fdAgency);
      }

      // Generate detail sql insert and write to file
      Tax_CreateDetailCsv(acTmp, pDetail);
      fputs(acTmp, fdDetail);
   }

   // Flood assessment - currently there is no data 11/14/2019
   dVal2  = atofn(pInRec->Tax_Flood_1St, TSIZ_TAX_FLOOD_1ST);
   dVal2 += atofn(pInRec->Tax_Flood_2Nd, TSIZ_TAX_FLOOD_1ST);
   if (dVal2 > 0.0)
   {
      sprintf(pDetail->TaxAmt, "%.2f", dVal2);
      memcpy(pDetail->TaxCode, "002", 3);
      dRate = atofn(pInRec->Taxrate_Flood, TSIZ_TAXRATE_FLOOD);
      sprintf(pDetail->TaxRate, "%.4f", dRate);
      pDetail->TC_Flag[0] = '1';

      pResult = findTaxAgency(pDetail->TaxCode);
      if (pResult)
      {
         pDetail->TC_Flag[0] = pResult->TC_Flag[0];

         memset(acAgencyRec, 0, sizeof(TAXAGENCY));
         strcpy(pAgency->Code,  pResult->Code);
         strcpy(pAgency->Agency,pResult->Agency);
         strcpy(pAgency->Phone, pResult->Phone);
         // Same tax code may have different rate.  Do not put it in Agency table.
         //strcpy(pAgency->TaxRate, pDetail->TaxRate);
         pAgency->TC_Flag[0] = pDetail->TC_Flag[0];
         Tax_CreateAgencyCsv(acTmp, (TAXAGENCY *)&acAgencyRec);
         fputs(acTmp, fdAgency);
      } else
         LogMsg("+++ Unknown Flood TaxCode: %s [%s]", pDetail->TaxCode, pDetail->Apn);

      // Generate csv line and write to file
      Tax_CreateDetailCsv(acTmp, pDetail);
      fputs(acTmp, fdDetail);
   }
   memset(pDetail->TaxRate, 0, TAX_RATE);

   // Loop through tax items
   for (iIdx = 0; iIdx < SEC_MAX_SPCTAX; iIdx++)
   {      
      if (!memcmp(pInRec->asSpcTax[iIdx].Special_Legend, "000", TSIZ_SPECIAL_LEGEND))
         break;

      // Agency 
      memcpy(pDetail->TaxCode, pInRec->asSpcTax[iIdx].Special_Legend, TSIZ_SPECIAL_LEGEND);

      // Tax Desc

      // Tax Amt
      dVal1 =  atof(pInRec->asSpcTax[iIdx].Tax_Special_1St);
      dVal2 =  atof(pInRec->asSpcTax[iIdx].Tax_Special_2Nd);
      sprintf(pDetail->TaxAmt, "%.2f", dVal1+dVal2);

      // Generate Agency rec
      if (pDetail->TaxCode[0] > ' ')
      {
         pResult = findTaxAgency(pDetail->TaxCode);
         if (pResult)
         {
            pDetail->TC_Flag[0] = pResult->TC_Flag[0];

            memset(acAgencyRec, 0, sizeof(TAXAGENCY));
            strcpy(pAgency->Code,  pResult->Code);
            strcpy(pAgency->Agency,pResult->Agency);
            strcpy(pAgency->Phone, pResult->Phone);
            strcpy(pAgency->TaxRate, pResult->TaxRate);
            pAgency->TC_Flag[0] = pResult->TC_Flag[0];

            Tax_CreateAgencyCsv(acTmp, (TAXAGENCY *)&acAgencyRec);
            fputs(acTmp, fdAgency);
         } else
            LogMsg("+++ Unknown special TaxCode: %s [%s]", pDetail->TaxCode, pDetail->Apn);
      } 

      // Generate csv line and write to file
      Tax_CreateDetailCsv(acTmp, pDetail);
      fputs(acTmp, fdDetail);
   }

   return 0;
}

/***************************** Ala_ParseTaxBase ******************************
 *
 * Parse record to Base & Owner
 *
 *****************************************************************************/

int Ala_ParseTaxBase(char *pBaseBuf, char *pInBuf)
{
   double   dTmp;
   char     sTmp[256];

   TAXBASE  *pBaseRec = (TAXBASE *)pBaseBuf;
   ALCO_SEC *pInRec = (ALCO_SEC *)pInBuf;

   // Clear output buffer
   memset(pBaseBuf, 0, sizeof(TAXBASE));

   // APN
   memcpy(sTmp, pInRec->APN, TSIZ_APN);
   strcpy(pBaseRec->Apn, myTrim(sTmp, TSIZ_APN));
   //strcpy(pBaseRec->OwnerInfo.Apn, pBaseRec->Apn);

   // Tracer number
   memcpy(pBaseRec->BillNum, pInRec->Tracer_Number, TSIZ_TRACER_NUMBER);
   //memcpy(pBaseRec->OwnerInfo.BillNum, pInRec->Tracer_Number, TSIZ_TRACER_NUMBER);

   // TRA
   sprintf(pBaseRec->TRA, "%.6d", atoin(pInRec->TRA, TSIZ_CODEAREA));

   // Tax Year
   sprintf(pBaseRec->TaxYear, "%d", lTaxYear);
   //strcpy(pBaseRec->OwnerInfo.TaxYear, pBaseRec->TaxYear);

   // Check for Tax amount - V99
   double dTax1 = atof(pInRec->Tax_Total_1St);
   double dTax2 = atof(pInRec->Tax_Total_2Nd);
   double dTotalTax = dTax1+dTax2;
   if (dTotalTax > 1.0)
   {
      sprintf(pBaseRec->TotalTaxAmt, "%.2f", dTotalTax);

      sprintf(pBaseRec->TaxAmt1, "%.2f", dTax1);
      sprintf(pBaseRec->TaxAmt2, "%.2f", dTax2);
   }

   // Tax Rate
   double dRate = atofn(pInRec->Taxrate_Av, TSIZ_TAXRATE_FLOOD);
   dRate += atofn(pInRec->Taxrate_Flood, TSIZ_TAXRATE_FLOOD);
   if (dRate > 0.0)
      sprintf(pBaseRec->TotalRate, "%.4f", dRate);

   // Paid status
   pBaseRec->Inst1Status[0] = TAX_BSTAT_UNPAID;
   pBaseRec->Inst2Status[0] = TAX_BSTAT_UNPAID;
   if (pInRec->Instlmt1_Pmt_Date[0] > '0')
   {
      pBaseRec->Inst1Status[0] = TAX_BSTAT_PAID;
      sprintf(pBaseRec->PaidDate1, "20%.6s", pInRec->Instlmt1_Pmt_Date);
      if (pInRec->Instlmt2_Pmt_Date[0] > '0')
      {
         sprintf(pBaseRec->PaidDate2, "20%.6s", pInRec->Instlmt2_Pmt_Date);
         pBaseRec->Inst2Status[0] = TAX_BSTAT_PAID;
      } 
   }

   // Total fee
   double dCost = atof(pInRec->Tax_Delinquent_Cost);
   if (dCost > 0.0)
      sprintf(pBaseRec->TotalFees, "%.2f", dCost);

#ifdef _DEBUG
   //if (!memcmp(pInRec->Apn, "002010059000", 9))
   //   iTmp = 0;
#endif

   // Penalty
   double dPen1 = atof(pInRec->Tax_Delinq_Penalty_1St);
   double dPen2 = atof(pInRec->Tax_Delinq_Penalty_2Nd);
   double dDue1=0, dDue2=0;
   if (dPen1 > 0.0)
   {
      sprintf(pBaseRec->PenAmt1, "%.2f", dPen1);
      dDue1 = dTax1 + dPen1 + dCost;
   }
   if (dPen2 > 0.0)
   {
      sprintf(pBaseRec->PenAmt2, "%.2f", dPen2);
      dDue2 = dTax2 + dPen2 + dCost;
   }

   // Is delinquent 
   //if (dPen1 > 0.0 || dPen2 > 0.0)
   //   pBaseRec->isDelq[0] = '1';

   // Due
   memcpy(pBaseRec->DueDate1, pInRec->Install1_Delq_Due_Date, TSIZ_INSTALL1_DELQ_DUE_DATE);
   memcpy(pBaseRec->DueDate2, pInRec->Install2_Delq_Due_Date, TSIZ_INSTALL2_DELQ_DUE_DATE);
   dTmp = dDue1+dDue2;
   if (dTmp > 0.0)
      sprintf(pBaseRec->TotalDue, "%.2f", dTmp);

   // Supplemental record? This flag tells us there is supp record, but not in this file
   //if (pInRec->Supplemental_Assess_Flag[0] == 'Y')
   //   pBaseRec->isSupp[0] = '1';

   pBaseRec->isSecd[0] = '1';
   pBaseRec->BillType[0] = BILLTYPE_SECURED;

   // Bill Status
   if (pInRec->Cancel_Date_Posted[0] > ' ')
   {
      pBaseRec->Inst1Status[0] = TAX_BSTAT_CANCEL;
      pBaseRec->Inst2Status[0] = TAX_BSTAT_CANCEL;
   }

   /* Remove owner info since we don't use it 05/27/2021 spn
   // Owner info
   int   iTmp;
   memcpy(sTmp, pInRec->Current_Owners_Name, TSIZ_CURRENT_OWNERS_NAME);
   iTmp = blankRem(sTmp, TSIZ_CURRENT_OWNERS_NAME);
   if (iTmp > 0)
      strcpy(pBaseRec->OwnerInfo.Name1, sTmp);
   else
   {
      memcpy(sTmp, pInRec->Liendate_Owners_Name, TSIZ_LIENDATE_OWNERS_NAME);
      iTmp = blankRem(sTmp, TSIZ_LIENDATE_OWNERS_NAME);
      if (iTmp > 0)
         strcpy(pBaseRec->OwnerInfo.Name1, sTmp);
   }

   // CareOf
   if (pInRec->Mail_Care_Of_Name[0] > ' ')
   {
      memcpy(sTmp, pInRec->Mail_Care_Of_Name, TSIZ_MAIL_CARE_OF_NAME);
      iTmp = blankRem(sTmp, TSIZ_MAIL_CARE_OF_NAME);
      strcpy(pBaseRec->OwnerInfo.Name1, sTmp);
   }

   // Mail addr
   if (pInRec->Mail_Street_Address[0] > ' ' && memcmp(pInRec->Mail_Street_Address, "CODE AREA", 9))
   {
      if (pInRec->Mail_Unit_No[0] > ' ')
         iTmp = sprintf(sTmp, "%.*s #%.*s", TSIZ_MAIL_STREET_ADDRESS, pInRec->Mail_Street_Address, TSIZ_MAIL_UNIT_NO, pInRec->Mail_Unit_No);
      else
         iTmp = sprintf(sTmp, "%.*s", TSIZ_MAIL_STREET_ADDRESS, pInRec->Mail_Street_Address);
      blankRem(sTmp, iTmp);
      strcpy(pBaseRec->OwnerInfo.MailAdr[0], sTmp);

      iTmp = sprintf(sTmp, "%.*s %.*s", TSIZ_MAIL_CITY_STATE, pInRec->Mail_City_State, TSIZ_MAIL_ZIP, pInRec->Mail_Zip);
      blankRem(sTmp, iTmp);
      strcpy(pBaseRec->OwnerInfo.MailAdr[1], sTmp);
   }
   */
   return 0;
}

/*************************** Ala_ParseCancel() ********************************
 *
 * Create Cancelled Base record
 * Return < 0 if record is not good or no tax
 *
 ******************************************************************************/

int Ala_ParseCancel(char *pOutbuf, char *pInbuf, char cDelim)
{
   char     acTmp[256];
   int      iTmp, iFmt;
   double	dTmp, dTax1, dTax2, dPen1, dPen2, dTaxTotal;
   TAXBASE  *pOutBase = (TAXBASE *)pOutbuf;

   // Parse input tax data
   iTokens = ParseStringIQ(pInbuf, cDelim, 128, apTokens);
   if (iTokens < TC_NET_VAL)
   {
      LogMsg("***** Error: bad Cancel record for APN=%.*s (#tokens=%d)", iApnLen, pInbuf, iTokens);
      return -1;
   }

   // Clear output buffer
   memset(pOutBase, 0, sizeof(TAXBASE));
#ifdef _DEBUG
   //if (!memcmp(apTokens[TC_APN_S], "24201130060000", 12))
   //   iTmp = 0;
#endif
   
   if (!memcmp(apTokens[TC_ROLL_CAT], "SEC", 3))
   {
      pOutBase->isSecd[0] = '1';
      pOutBase->BillType[0] = BILLTYPE_SECURED;
   } else if (!memcmp(apTokens[TC_ROLL_CAT], "SUP", 3))
   {
      pOutBase->isSupp[0] = '1';
      pOutBase->BillType[0] = BILLTYPE_SECURED_SUPPL;
   } else
   {
      pOutBase->isSecd[0] = '0';
      pOutBase->isSupp[0] = '0';
   }

   // Tax Year
   iTaxYear = atol(apTokens[TC_TAX_YR]);
   if (iTaxYear < 1900)
   {
      LogMsg("*** WARNING: No tax base data APN=%s", apTokens[TC_APN_S]);
      return -2;
   }
   sprintf(pOutBase->TaxYear, "%d", iTaxYear);

   // APN
   strcpy(pOutBase->Apn, apTokens[TC_APN_S]);

   // BillNum
   if (*apTokens[TC_BILL_NUM] >= '0')
      strcpy(pOutBase->BillNum, apTokens[TC_BILL_NUM]);

   // TRA
   iTmp = atol(apTokens[TC_TRA]);
   if (iTmp > 0)
      sprintf(pOutBase->TRA, "%0.6d", iTmp);

   // Check for Tax amount
   dTax1 = dTax2 = 0.0;
   if (*apTokens[TC_TAX_DUE1] > '0')
      dTax1 = atof(apTokens[TC_TAX_DUE1]);

   if (*apTokens[TC_TAX_DUE2] > '0')
      dTax2 = atof(apTokens[TC_TAX_DUE2]);

   if (dTax1 > 0.0)
      sprintf(pOutBase->TaxAmt1, "%.2f", dTax1);
   if (dTax2 > 0.0)
      sprintf(pOutBase->TaxAmt2, "%.2f", dTax2);

   if (*apTokens[TC_TOTAL_TAX] > '0')
      dTaxTotal = atof(apTokens[TC_TOTAL_TAX]);
   else
      dTaxTotal = dTax1+dTax2;

   if (dTaxTotal > 0.0)
      sprintf(pOutBase->TotalTaxAmt, "%.2f", dTaxTotal);

   // Penalty
   dPen1 = atof(apTokens[TC_PEN_DUE1]);
   if (dPen1 > 0.0)
      sprintf(pOutBase->PenAmt1, "%.2f", dPen1);
   dPen2 = atof(apTokens[TC_PEN_DUE2]);
   if (dPen2 > 0.0)
      sprintf(pOutBase->PenAmt2, "%.2f", dPen2);

   // Verify date format
   if (*(apTokens[TC_DUE_PAID_DT2]+2) == '/')
      iFmt = MM_DD_YYYY_1;    // 4/8/2017 or 04/08/2017
   else
      iFmt = MMM_DD_YYYY;     // Nov 3, 2016

   if (*apTokens[TC_DUE_PAID_DT2] >= '0' && dateConversion(apTokens[TC_DUE_PAID_DT2], acTmp, iFmt))
      strcpy(pOutBase->PaidDate2, acTmp);

   // Verify date format
   if (*(apTokens[TC_DUE_PAID_DT1]+2) == '/')
      iFmt = MM_DD_YYYY_1;
   else
      iFmt = MMM_DD_YYYY;

   if (*apTokens[TC_DUE_PAID_DT1] >= '0' && dateConversion(apTokens[TC_DUE_PAID_DT1], acTmp, iFmt))
      strcpy(pOutBase->PaidDate1, acTmp);

   // Fee Paid
   dTmp = atof(apTokens[TC_OTHER_FEES]);
   if (dTmp > 0.0)
      iTmp = sprintf(pOutBase->TotalFees, "%.2f", dTmp);

   pOutBase->Inst2Status[0] = TAX_BSTAT_CANCEL;
   pOutBase->Inst1Status[0] = TAX_BSTAT_CANCEL;

   // Update date
   if (iTokens > TC_UPD_DATE && *apTokens[TC_UPD_DATE] > ' ' && dateConversion(apTokens[TC_UPD_DATE], acTmp, MM_DD_YYYY_1))
      strcpy(pOutBase->Upd_Date, acTmp);
   else
      sprintf(pOutBase->Upd_Date, "%d", lLastTaxFileDate);

   return 0;
}

/****************************** Ala_UpdateTaxBase *****************************
 *
 * Update TaxBase using scraped data
 *
 ******************************************************************************/

int Ala_UpdateTaxBase(char *pOutbuf, FILE *fd, FILE *fdBase)
{
   static   char  acRec[MAX_RECSIZE], *pRec=NULL, sLastApn[16], sLastTRA[16];
   //static   bool  bNoParse=false;

   int      iRet, iLoop;
   char     *pTmp, acTmp[256], acBuf[2048];
   double   dTmp;

   TAXBASE   *pBaseRec = (TAXBASE *)pOutbuf;
   TAXBASE   *pLocalRec =(TAXBASE *)acBuf;

   if (!pRec)
   {
      pRec = fgets(acRec, MAX_RECSIZE, fd);        // Skip header
      pRec = fgets(acRec, MAX_RECSIZE, fd);        // Get first rec
      TC_SetDateFmt(MMM_DD_YYYY, true);
   }

#ifdef _DEBUG
   //if (!memcmp(acRec, "001 011703400", 13))
   //   iRet = 0;
#endif
   GetNextTCRec:
   do
   {
      // Compare Apn
      iLoop = memcmp(pBaseRec->Apn, acRec, iApnLen);
      if (iLoop > 0)
      {
         // Add new bill
         iRet = TC_ParseTaxBase(acBuf, acRec, '|');
         if (iRet >= 0)
         {
            lTaxAdd++;
            if (!strcmp(sLastApn, pLocalRec->Apn) && pLocalRec->TRA[0] < '0')
               strcpy(pLocalRec->TRA, sLastTRA);
            LogMsg0("+++ Add new rec %.80s", acRec);
            Tax_CreateTaxBaseCsv(acRec, (TAXBASE *)&acBuf);
            fputs(acRec, fdBase);
         }
         pRec = fgets(acRec, MAX_RECSIZE, fd);        // Get next rec
         if (!pRec)
         {
            fclose(fd);
            fd = NULL;
            return 1;      // EOF
         }
         //bNoParse = false;
         iLoop = 1;
      }
   } while (iLoop > 0);

   if (iLoop)
      return 1;

   // Parse input string
   //if (!bNoParse)
   //{
      iRet = ParseStringIQ(acRec, '|', 128, apTokens);
      if (iRet < TC_NET_VAL)
      {
         LogMsg("***** Error: bad TC record for APN=%.100s (#tokens=%d)", acTmp, iRet);
         return -1;
      }
   //}

   // Matching up billNum before update
   if (strcmp(pBaseRec->BillNum, apTokens[TC_BILL_NUM]))
   {
      if (*apTokens[TC_PAID_STAT1] == 'C' || *apTokens[TC_PAID_STAT2] == 'C')
      {
         // Save APN & TRA
         strcpy(sLastApn, pBaseRec->Apn);
         strcpy(sLastTRA, pBaseRec->TRA);
         lTaxSkip++;

         LogMsg("*** Skip update APN=%s, Base.BillNum=%s, TC.BillNum=%s", pBaseRec->Apn, pBaseRec->BillNum, apTokens[TC_BILL_NUM]);
         // Ignore this record, get next one
         pRec = fgets(acRec, MAX_RECSIZE, fd);
      } else
      {
         // Add new bill
         iRet = TC_CreateTaxRecord(acBuf);
         if (iRet >= 0)
         {
            lTaxAdd++;
            if (!strcmp(sLastApn, pLocalRec->Apn) && pLocalRec->TRA[0] < '0')
               strcpy(pLocalRec->TRA, sLastTRA);
            LogMsg0("+++ Add new rec %.80s", acRec);
            Tax_CreateTaxBaseCsv(acRec, (TAXBASE *)&acBuf);
            fputs(acRec, fdBase);
         }
         pRec = fgets(acRec, MAX_RECSIZE, fd);        // Get next rec
      }
      goto GetNextTCRec;
   }

   // Update bill number
   //if (pBaseRec->BillNum[0] <= ' ')
   //   strcpy(pBaseRec->BillNum, apTokens[TC_BILL_NUM]);

   if (*apTokens[TC_PAID_STAT1] == 'P')
   {
      pBaseRec->Inst1Status[0] = TAX_BSTAT_PAID;
      strcpy(pBaseRec->PaidAmt1, apTokens[TC_AMT_PAID1]);
      if (*apTokens[TC_DUE_PAID_DT1] >= '0')
      {
         if (*apTokens[TC_DUE_PAID_DT1] > '9')
            pTmp = dateConversion(apTokens[TC_DUE_PAID_DT1], acTmp, MMM_DD_YYYY);
         else
            pTmp = dateConversion(apTokens[TC_DUE_PAID_DT1], acTmp, MM_DD_YYYY_2);
         if (pTmp)
            strcpy(pBaseRec->PaidDate1, acTmp);
      } else
         LogMsg("*** Bad PaidDate 1: %s [%s]", apTokens[TC_DUE_PAID_DT1], apTokens[TC_APN_S]);
   } else if (*apTokens[TC_PAID_STAT1] == 'D' || *apTokens[TC_PAID_STAT1] == 'U')
   {
      pBaseRec->Inst1Status[0] = TAX_BSTAT_UNPAID;
      if (*apTokens[TC_DUE_PAID_DT1] >= '0')
      {
         if (*apTokens[TC_DUE_PAID_DT1] > '9')
            pTmp = dateConversion(apTokens[TC_DUE_PAID_DT1], acTmp, MMM_DD_YYYY);
         else
            pTmp = dateConversion(apTokens[TC_DUE_PAID_DT1], acTmp, MM_DD_YYYY_2);
         if (pTmp)
            strcpy(pBaseRec->DueDate1, acTmp);
      }
   } else if (*apTokens[TC_PAID_STAT1] == 'C')     // Cancelled
   {
      pBaseRec->Inst1Status[0] = TAX_BSTAT_CANCEL;
      if (*apTokens[TC_DUE_PAID_DT1] >= '0')
      {
         if (*apTokens[TC_DUE_PAID_DT1] > '9')
            pTmp = dateConversion(apTokens[TC_DUE_PAID_DT1], acTmp, MMM_DD_YYYY);
         else
            pTmp = dateConversion(apTokens[TC_DUE_PAID_DT1], acTmp, MM_DD_YYYY_2);
         if (pTmp)
            strcpy(pBaseRec->DueDate1, acTmp);
      }
   } 

   if (*apTokens[TC_PAID_STAT2] == 'P')
   {
      pBaseRec->Inst2Status[0] = TAX_BSTAT_PAID;
      strcpy(pBaseRec->PaidAmt2, apTokens[TC_AMT_PAID2]);
      if (*apTokens[TC_DUE_PAID_DT2] >= '0')
      {
         if (*apTokens[TC_DUE_PAID_DT2] > '9')
            pTmp = dateConversion(apTokens[TC_DUE_PAID_DT2], acTmp, MMM_DD_YYYY);
         else
            pTmp = dateConversion(apTokens[TC_DUE_PAID_DT2], acTmp, MM_DD_YYYY_2);
         if (pTmp)
            strcpy(pBaseRec->PaidDate2, acTmp);
      } else
         LogMsg("*** Bad PaidDate 2: %s [%s]", apTokens[TC_DUE_PAID_DT2], apTokens[TC_APN_S]);
   } else if (*apTokens[TC_PAID_STAT2] == 'D' || *apTokens[TC_PAID_STAT2] == 'U')
   {
      dTmp = atof(apTokens[TC_TAX_DUE2]);
      if (dTmp > 0.0)
      {
         pBaseRec->Inst2Status[0] = TAX_BSTAT_UNPAID;
         if (*apTokens[TC_DUE_PAID_DT2] >= '0')
         {
            if (*apTokens[TC_DUE_PAID_DT2] > '9')
               pTmp = dateConversion(apTokens[TC_DUE_PAID_DT2], acTmp, MMM_DD_YYYY);
            else
               pTmp = dateConversion(apTokens[TC_DUE_PAID_DT2], acTmp, MM_DD_YYYY_2);
            if (pTmp)
               strcpy(pBaseRec->DueDate2, acTmp);
         }
      } else
         pBaseRec->Inst2Status[0] = TAX_BSTAT_NOTAX;
   } else if (*apTokens[TC_PAID_STAT2] == 'C')     // Cancelled
   {
      pBaseRec->Inst2Status[0] = TAX_BSTAT_CANCEL;
      if (*apTokens[TC_DUE_PAID_DT2] >= '0')
      {
         if (*apTokens[TC_DUE_PAID_DT2] > '9')
            pTmp = dateConversion(apTokens[TC_DUE_PAID_DT2], acTmp, MMM_DD_YYYY);
         else
            pTmp = dateConversion(apTokens[TC_DUE_PAID_DT2], acTmp, MM_DD_YYYY_2);
         if (pTmp)
            strcpy(pBaseRec->DueDate2, acTmp);
      }
   } 

   // Penalty
   pBaseRec->isDelq[0] = 0;
   dTmp = atof(apTokens[TC_PEN_DUE1]);
   if (dTmp > 0.0)
   {
      pBaseRec->isDelq[0] = '1';
      sprintf(pBaseRec->PenAmt1, "%.2f", dTmp);
   } else
      memset(pBaseRec->PenAmt1, 0, TAX_AMT);

   dTmp = atof(apTokens[TC_PEN_DUE2]);
   if (dTmp > 0.0)
   {
      pBaseRec->isDelq[0] = '1';
      sprintf(pBaseRec->PenAmt2, "%.2f", dTmp);
   } else
      memset(pBaseRec->PenAmt2, 0, TAX_AMT);

   // Fee Paid
   dTmp = atof(apTokens[TC_OTHER_FEES]);
   if (dTmp > 0.0)
      sprintf(pBaseRec->TotalFees, "%.2f", dTmp);

   // Calculate unpaid amt
   dTmp = atof(apTokens[TC_TOTAL_DUE]);
   if (dTmp > 0.0)
      sprintf(pBaseRec->TotalDue, "%.2f", dTmp);

   lTaxMatch++;

   // Get next record
   pRec = fgets(acRec, MAX_RECSIZE, fd);

#ifdef _DEBUG
   //if (!memcmp(acRec, "006 002100400", 13))
   //   iRet = 0;
#endif

   // Save last TRA
   strcpy(sLastApn, pBaseRec->Apn);
   strcpy(sLastTRA, pBaseRec->TRA);

   while (pRec && strstr(acRec, "SUPPLEMENTAL"))
   {
      // Create supp record
      iRet = TC_ParseTaxBase(acBuf, acRec, '|');
      if (iRet > 0)
      {
         if (!strcmp(sLastApn, pLocalRec->Apn) && pLocalRec->TRA[0] < '0')
            strcpy(pLocalRec->TRA, sLastTRA);

         Tax_CreateTaxBaseCsv(acRec, (TAXBASE *)&acBuf);
         fputs(acRec, fdBase);
      } else
         LogMsg0("--- Drop TC supp rec %.80s", acRec);
      pRec = fgets(acRec, MAX_RECSIZE, fd);        // Get next rec
   }

   if (!pRec)
   {
      fclose(fd);
      fd = NULL;
   }
   //bNoParse = false;

   return 0;
}

/**************************** Ala_Load_TaxBase *****************************
 *
 * Loading ALCO-SECUR.txt
 * Create TaxBase, TaxDetail, TaxOwner
 *
 ***************************************************************************/

int Ala_Load_TaxBase(bool bImport)
{
   char      *pTmp, acBase[1024], acDetail[1024], acRec[MAX_RECSIZE], acAgencyFile[_MAX_PATH], acTCFile[_MAX_PATH],
             acBaseFile[_MAX_PATH], acDetailFile[_MAX_PATH], acTmpFile[_MAX_PATH], acOwnerFile[_MAX_PATH];

   int       iRet, iRecSize, lTmp;
   long      lOut=0, lCnt=0;
   FILE      *fdBase, *fdDetail, *fdIn, *fdTC, *fdAgency; 
   TAXBASE   *pBase = (TAXBASE *)acBase;
   TAXDETAIL *pDetail = (TAXDETAIL *)acDetail;
   ALCO_SEC  *pInRec= (ALCO_SEC *)acRec;

   LogMsg0("Loading tax base");

   NameTaxCsvFile(acBaseFile, myCounty.acCntyCode, "Base");
   NameTaxCsvFile(acDetailFile, myCounty.acCntyCode, "Items");
   NameTaxCsvFile(acOwnerFile, myCounty.acCntyCode, "Owner");
   NameTaxCsvFile(acAgencyFile, myCounty.acCntyCode, "Agency");
   pTmp = strrchr(acBaseFile, '\\');
   *pTmp = 0;
   if (_access(acBaseFile, 0))
      _mkdir(acBaseFile);
   *pTmp = '\\';

   // Prepare input file - pick the latest one
   GetIniString(myCounty.acCntyCode, "Main_TC", "", acTmpFile, _MAX_PATH, acIniFile);
   sprintf(acRec, acTmpFile, 'P');
   sprintf(acBase, acTmpFile, 'F');
   iRet = chkFileDate(acRec, acBase);
   if (iRet == 1 || iRet == -2)
      strcpy(acTCFile, acRec);
   else if (iRet == 0 || iRet == 2 || iRet == -1)
      strcpy(acTCFile, acBase);
   else
      acTCFile[0] = 0;

   GetIniString(myCounty.acCntyCode, "TaxRollInfo", "", acBase, _MAX_PATH, acIniFile);
   lLastTaxFileDate = getFileDate(acBase);
   lTmp = getFileDate(acTCFile);
   if (lTmp > lLastTaxFileDate)
      lLastTaxFileDate = lTmp;

   // Only process if new tax file
   iRet = isNewTaxFile(acBase, myCounty.acCntyCode);
   if (iRet <= 0)
   {
      lLastTaxFileDate = 0;
      if (!iRet)
      {
         LogMsg("*** No new tax file ***");
         iRet = 1;
      }
      return iRet;
   }

   LogMsg("Open Tax Roll Info file %s", acBase);
   fdIn = fopen(acBase, "rb");
   if (fdIn == NULL)
   {
      LogMsg("***** Error opening tax roll file: %s\n", acBase);
      return -2;
   }  
   iRecSize = GetPrivateProfileInt(myCounty.acCntyCode, "TaxRecSize", 3067, acIniFile);

   // Open TC file
   if (acTCFile[0] && !_access(acTCFile, 0))
   {
      LogMsg("Open TC file %s", acTCFile);
      fdTC = fopen(acTCFile, "r");
      if (fdTC == NULL)
      {
         LogMsg("***** Error opening Current TC file: %s\n", acTCFile);
         return -2;
      }  
   } else
   {
      fdTC = NULL;
      LogMsg("*** Missing TC file (%s).  No current data available.", acTCFile);
   }

   // Open base file
   LogMsg("Open Base file %s", acBaseFile);
   fdBase = fopen(acBaseFile, "w");
   if (fdBase == NULL)
   {
      LogMsg("***** Error creating base file: %s\n", acBaseFile);
      return -4;
   }

   // Open Detail file
   LogMsg("Open Detail output file %s", acDetailFile);
   fdDetail = fopen(acDetailFile, "w");
   if (fdDetail == NULL)
   {
      LogMsg("***** Error creating Detail output file: %s\n", acDetailFile);
      return -4;
   }

   // Open Owner file
   //LogMsg("Open Owner file %s", acOwnerFile);
   //fdOwner = fopen(acOwnerFile, "w");
   //if (fdOwner == NULL)
   //{
   //   LogMsg("***** Error creating Agency file: %s\n", acOwnerFile);
   //   return -4;
   //}

   // Open Agency file
   sprintf(acTmpFile, "%s\\%s\\%s_Agency.tmp", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   LogMsg("Create Agency file %s", acTmpFile);
   fdAgency = fopen(acTmpFile, "w");
   if (fdAgency == NULL)
   {
      LogMsg("***** Error creating Agency file: %s\n", acTmpFile);
      return -4;
   }

   // Merge loop 
   lTaxSkip=lTaxMatch=0;
   while (!feof(fdIn))
   {
      //pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdIn);
      //if (!pTmp)
      //   break;
      iRet = fread(acRec, 1, iRecSize, fdIn);
      if (iRet < iRecSize)
      {
         if (iRet > 2)
            LogMsg("*** Bad record: %.40s", acRec);
         lCnt++;
         continue;
      }

      // Skip unknown parcel
      if (!memcmp(pInRec->APN, "000", 3))
         continue;

#ifdef _DEBUG
      //if (!memcmp(pInRec->APN, "001 011703400", iApnLen))
      //   iRet = 0;
#endif
      iRet = Ala_ParseTaxBase(acBase, acRec);
      if (!iRet)
      {
         // No need to update cancelled bill
         if (pBase->Inst1Status[0] != TAX_BSTAT_CANCEL)
         {
            // Update TaxBase record using Ala.TC
            if (fdTC)
               Ala_UpdateTaxBase(acBase, fdTC, fdBase);
         }

         // Create detail & agency records
         Ala_ParseTaxDetail(acRec, fdDetail, fdAgency);

         // Create TaxBase record
         Tax_CreateTaxBaseCsv(acRec, (TAXBASE *)&acBase);
         fputs(acRec, fdBase);

         // Create Owner record
         //Tax_CreateTaxOwnerCsv(acRec, &pBase->OwnerInfo);
         //fputs(acRec, fdOwner);

         lOut++;
      } else
         LogMsg("*** Drop Base record %.40s (%d)", acRec, lCnt);

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdIn)
      fclose(fdIn);
   if (fdBase)
      fclose(fdBase);
   if (fdDetail)
      fclose(fdDetail);
   //if (fdOwner)
   //   fclose(fdOwner);
   if (fdTC)
      fclose(fdTC);
   if (fdAgency)
      fclose(fdAgency);

   LogMsg("Total records processed:    %u", lCnt);
   LogMsg("         output records:    %u", lOut);
   LogMsg("        Skip TC records:    %u", lTaxSkip);
   LogMsg("         Add TC records:    %u", lTaxAdd);
   LogMsg("       TC records match:    %u", lTaxMatch);

   // Import into SQL
   if (bImport)
   {
      iRet = doTaxImport(myCounty.acCntyCode, TAX_BASE);
      if (!iRet)
         iRet = doTaxImport(myCounty.acCntyCode, TAX_DETAIL);
      //if (!iRet)
      //   iRet = doTaxImport(myCounty.acCntyCode, TAX_OWNER);
      if (!iRet)
      {
         // Dedup Agency file before import
         LogMsg("Dedup Agency file %s to %s", acTmpFile, acAgencyFile);
         iRet = sortFile(acTmpFile, acAgencyFile, "S(#1,C,A) DUPOUT F(TXT)");
         if (iRet > 0)
            iRet = doTaxImport(myCounty.acCntyCode, TAX_AGENCY);
      }
   } else
      iRet = 0;

   return iRet;
}

/**************************** Ala_Load_TaxDelq *****************************
 *
 * Create Delq record
 *
 ***************************************************************************/

int Ala_ParseDelq(char *pOutbuf, char *pInbuf)
{
   int      iTmp, lRedDate, lDefDate;
   double   dTaxAmt, dTmp;
   TAXDELQ  *pOutRec= (TAXDELQ *)pOutbuf;
   TDTCM    *pInRec = (TDTCM *)pInbuf;

   // Process 'D' record only
   if (pInRec->Roll_Id[0] != 'D')
      return -1;

   // Clear output buffer
   memset(pOutbuf, 0, sizeof(TAXDELQ));
   lRedDate=lDefDate = 0;

   // APN
   memcpy(pOutRec->Apn, pInRec->Apn, TSIZ_TCM_PARCEL);

   // TDN
   memcpy(pOutRec->Default_No, pInRec->Acct_Dflt_Num, TSIZ_TCM_ACCT_DFLT_NUM);

   // Tracer
   memcpy(pOutRec->BillNum, pInRec->Tracer, TSIZ_TCM_TRACER);

   // Supplemental delq
   if (pInRec->Acct_Source_Num[1] == '1')
      pOutRec->isSecd[0] = '1';
   else if (pInRec->Acct_Source_Num[1] == '2')
      pOutRec->isSupp[0] = '1';

   // Updated date
   sprintf(pOutRec->Upd_Date, "%d", lLastTaxFileDate);

   // Tax Year
   iTmp = atoin(pInRec->Acct_Yr, 2);
   if (iTmp >= 50)
      sprintf(pOutRec->TaxYear, "19%.2d", iTmp);
   else
      sprintf(pOutRec->TaxYear, "20%.2d", iTmp);

   //// Default amount
   dTaxAmt  = atof(pInRec->AV_Tax);
   dTaxAmt += atof(pInRec->FL_Tax);
   for (iTmp = 0; iTmp < TCM_MAX_ITEMS; iTmp++)
   {
      dTmp = atof(pInRec->SP_Tax[iTmp]);
      if (!dTmp)
         break;
      dTaxAmt += dTmp;
   }

   if (dTaxAmt > 0.0)
   {
      sprintf(pOutRec->Def_Amt, "%.2f", dTaxAmt);
      pOutRec->isDelq[0] = '1';
      pOutRec->DelqStatus[0] = TAX_STAT_UNPAID;
   }

   //// Default date
   //strcpy(pOutRec->Def_Date, apTokens[T01_DELQYEAR]);


   // Delq Pen Amt
   dTmp = atof(pInRec->Del_Pen);
   if (dTmp > 0.0)
      sprintf(pOutRec->Pen_Amt, "%.2f", dTmp);

   // Delq Cost Amt
   dTmp = atof(pInRec->Del_Cst);
   if (dTmp > 0.0)
      sprintf(pOutRec->Fee_Amt, "%.2f", dTmp);

   return 0;
}

/********************************* Ala_UpdateDelq *****************************
 *
 ******************************************************************************/

int Ala_UpdateDelq(char *pOutbuf, FILE *fd)
{
   static   char  acRec[MAX_RECSIZE], *pRec=NULL;

   int      iLoop;
   double   dRedAmt, dFeeAmt, dCstAmt;

   TAXDELQ  *pOutRec  = (TAXDELQ *)pOutbuf;
   TDPF0    *pPaidRec = (TDPF0 *)acRec;

   if (!pRec)
   {
      do {
         pRec = fgets(acRec, MAX_RECSIZE, fd);        // Get first rec
      } while (*pRec < '0');
   }
#ifdef _DEBUG
   //if (!memcmp(acRec, "006 002100400", 13))
   //   iLoop = 0;
#endif
   do
   {
      // Compare Apn
      iLoop = memcmp(pOutRec->Apn, pPaidRec->Apn, iApnLen);
      if (iLoop > 0)
      {
         pRec = fgets(acRec, MAX_RECSIZE, fd);        // Get next paid delq rec
         if (!pRec)
         {
            fclose(fd);
            fd = NULL;
            return 1;      // EOF
         }
      }
   } while (iLoop > 0);

   if (iLoop)
      return 1;

   lTaxMatch++;

#ifdef _DEBUG
   //if (!memcmp(acRec, "006 002100400", 13))
   //   iRet = 0;
#endif

   while (!iLoop)
   {
      // If payment accepted
      if (pPaidRec->Pymt_Code[0] == 'A')
      {
         if (pPaidRec->Pymt_Type[0] == 'F')        // Paid in full
            pOutRec->DelqStatus[0] = TAX_STAT_REDEEMED;
         else if (pPaidRec->Pymt_Type[0] == 'P')   // Partial
            pOutRec->DelqStatus[0] = TAX_STAT_ONPAYMENT;

         pOutRec->isDelq[0] = '0';
         dRedAmt = atofn(pPaidRec->Pymt_Amt, TSIZ_PAY_PYMT_AMT);
         dFeeAmt = atofn(pPaidRec->Rdm_Fee_Pre_84, TSIZ_PAY_PYMT_AMT)+atofn(pPaidRec->Rdm_Fee_All_Oth, TSIZ_PAY_PYMT_AMT);
         dCstAmt = atofn(pPaidRec->Del_Cst, TSIZ_PAY_PYMT_AMT);
         dFeeAmt += dCstAmt;
         if (dRedAmt > 0.0)
            sprintf(pOutRec->Red_Amt, "%.2f", dRedAmt);
         if (dFeeAmt > 0.0)
            sprintf(pOutRec->Fee_Amt, "%.2f", dFeeAmt);
         memcpy(pOutRec->Red_Date, pPaidRec->Post_Date, 8);
      }

      // Get next record
      pRec = fgets(acRec, MAX_RECSIZE, fd);
      if (!pRec)
      {
         fclose(fd);
         fd = NULL;
      } else
         iLoop = memcmp(pOutRec->Apn, pPaidRec->Apn, iApnLen);
   }

   return 0;
}

int Ala_Load_TaxDelq(bool bImport)
{
   char     acOutFile[_MAX_PATH], acDelqFile[_MAX_PATH], acPaidFile[_MAX_PATH], acBuf[1024], acRec[1024], *pTmp;
   char     acTmp[512];
   int      iRet, lCnt=0, lOut=0, lLastPaidFileDate;
   FILE     *fdDelq, *fdPaid, *fdOut;
   TDTCM    *pDefRec = (TDTCM *)&acRec[0];

   LogMsg0("Loading tax delinquent");

   // Master defaulted file
   GetIniString(myCounty.acCntyCode, "TaxDelq", "", acDelqFile, _MAX_PATH, acIniFile);
   GetIniString(myCounty.acCntyCode, "PaidDelq", "", acPaidFile, _MAX_PATH, acIniFile);

   // Sort Delq file
   sprintf(acTmp, "%s\\Ala\\Delq.srt", acTmpPath);
   iRet = sortFile(acDelqFile, acTmp, "S(71,13,C,A,1617,4,C,D) F(TXT)");

   // Open Delq file
   LogMsg("Open tax delinquent file %s", acTmp);
   fdDelq = fopen(acTmp, "r");
   if (fdDelq == NULL)
   {
      LogMsg("***** Error opening Secured tax file: %s\n", acTmp);
      return -2;
   }  
   lLastTaxFileDate = getFileDate(acDelqFile);

   if (!_access(acPaidFile, 0))
   {
      // Sort Paid Delq file
      sprintf(acTmp, "%s\\Ala\\PaidDelq.srt", acTmpPath);
      iRet = sortFile(acPaidFile, acTmp, "S(1,13,C,A,86,2,C,A,34,8,C,A) F(TXT)");

      // Open Paid Delq file
      LogMsg("Open paid delq file %s", acTmp);
      fdPaid = fopen(acTmp, "r");
      if (fdPaid == NULL)
      {
         LogMsg("***** Error opening paid delq file: %s\n", acTmp);
         return -2;
      }  
      lLastPaidFileDate = getFileDate(acPaidFile);
   } else
   {
      LogMsg("*** Missing paid Delq file: %s", acPaidFile);
      fdPaid = NULL;
   }

   // Open output file
   NameTaxCsvFile(acOutFile, myCounty.acCntyCode, "Delq");
   LogMsg("Open output file %s", acOutFile);
   fdOut = fopen(acOutFile, "w");
   if (fdOut == NULL)
   {
      LogMsg("***** Error creating output file: %s\n", acOutFile);
      return -4;
   }

   // Merge loop 
   while (!feof(fdDelq))
   {
      pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdDelq);
      if (!pTmp || *pTmp < ' ')
         break;


#ifdef _DEBUG
      //if (!memcmp(pDefRec->Apn, "30218200", 8) || !memcmp(pDefRec->Apn, "00127306", 8) || !memcmp(pDefRec->Apn, "06657107", 8))
      //   iRet = 0;
#endif
      // Create new Delq record
      iRet = Ala_ParseDelq(acBuf, acRec);
      if (!iRet)
      {
         if (fdPaid)
            iRet = Ala_UpdateDelq(acBuf, fdPaid);

         // Create delimited record
         Tax_CreateDelqCsv(acTmp, (TAXDELQ *)&acBuf);

         // Output record			
         lOut++;
         fputs(acTmp, fdOut);
      } else 
      {
         LogMsg("---> Drop Delq record %d [%.13s]. Roll_Id=%c", lCnt, pDefRec->Apn, pDefRec->Roll_Id[0]); 
         pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdDelq);
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdDelq)
      fclose(fdDelq);
   if (fdPaid)
      fclose(fdPaid);
   if (fdOut)
      fclose(fdOut);

   LogMsg("Total records processed:    %u", lCnt);
   LogMsg("Total output records:       %u", lOut);

   // Import into SQL
   if (bImport)
      iRet = doTaxImport(myCounty.acCntyCode, TAX_DELINQUENT);
   else
      iRet = 0;

   return iRet;
}

/********************************* Ala_ExtrVal ******************************
 *
 * Extract values from ALCO-SECUR.txt
 *
 ****************************************************************************/

int Ala_CreateValueRec(char *pOutbuf, char *pInbuf)
{
   char     acTmp[256], acApn[256];
   LONGLONG lTmp;
   int      iTmp;
   LIENEXTR *pLienRec = (LIENEXTR *)pOutbuf;
   ALCO_SEC *pInRec = (ALCO_SEC *)pInbuf;

   // Clear output buffer
   memset(pOutbuf, ' ', sizeof(LIENEXTR));

   // Format APN
   memcpy(acApn, pInRec->APN, TSIZ_APN);
   iTmp = iTrim(acApn, TSIZ_APN);
   memcpy(pLienRec->acApn, acApn, iTmp);

   // Format TRA
   iTmp = sprintf(acTmp, "%.6d", atoin(pInRec->TRA, TSIZ_CODEAREA));
   memcpy(pLienRec->acTRA, acTmp, iTmp);

#ifdef _DEBUG
   //if (!memcmp(pLienRec->acApn, "011 0859004", 11) )
   //   iTmp = 0;
#endif

   // Year assessed
   memcpy(pLienRec->acYear, myCounty.acYearAssd, 4);

   // Land
   ULONG lLand = atoln(pInRec->Value_Land, TSIZ_VALUE_LAND);
   if (lLand > 0)
   {
      iTmp = sprintf(acTmp, "%*u", sizeof(pLienRec->acLand), lLand);
      memcpy(pLienRec->acLand, acTmp, iTmp);
   }

   // Improve - Ratio
   ULONG lImpr = atoln(pInRec->Value_Improvements, TSIZ_VALUE_IMPROVEMENTS);
   if (lImpr > 0)
   {
      iTmp = sprintf(acTmp, "%*u", sizeof(pLienRec->acImpr), lImpr);
      memcpy(pLienRec->acImpr, acTmp, iTmp);

      iTmp = sprintf(acTmp, "%*u", sizeof(pLienRec->acRatio), (LONGLONG)lImpr*100/(lLand+lImpr));
      memcpy(pLienRec->acRatio, acTmp, iTmp);
   }

   // PP value
   ULONG lPers = atoln(pInRec->Value_Business_Persprop, TSIZ_VALUE_BUSINESS_PERSPROP);

   // Fixture
   ULONG lFixt = atoln(pInRec->Value_Machine_Equip, TSIZ_VALUE_MACHINE_EQUIP);

   // Total other
   ULONG lOthers = lPers + lFixt + atoln(pInRec->Value_Househld_Persprop, TSIZ_VALUE_MACHINE_EQUIP);
   if (lOthers > 0)
   {
      iTmp = sprintf(acTmp, "%*u", sizeof(pLienRec->acOther), lOthers);
      memcpy(pLienRec->acOther, acTmp, iTmp);

      if (lPers > 0)
      {
         iTmp = sprintf(acTmp, "%u", lPers);
         memcpy(pLienRec->acPP_Val, acTmp, iTmp);
      }
      if (lFixt > 0)
      {
         iTmp = sprintf(acTmp, "%u", lFixt);
         memcpy(pLienRec->acME_Val, acTmp, iTmp);
      }
   }

   // Gross
   lTmp = lOthers + lLand + lImpr;
   if (lTmp > 0)
   {
      iTmp = sprintf(acTmp, "%*u", sizeof(pLienRec->acGross), lTmp);
      memcpy(pLienRec->acGross, acTmp, iTmp);
   }

   // HO Exe 
   long lHo_Exe = atoin(pInRec->Value_Homeowner_Exemption, TSIZ_VALUE_HOMEOWNER_EXEMPTION);
   if (lHo_Exe > 0)
   {
      iTmp = sprintf(acTmp, "%d", lHo_Exe);
      memcpy(pLienRec->acExAmt, acTmp, iTmp);
      pLienRec->acHO[0] = '1';         // Y
   } else
      pLienRec->acHO[0] = '2';         // N

   // Total Exemption
   lTmp = atoin(pInRec->Value_Other_Exemption, TSIZ_VALUE_HOMEOWNER_EXEMPTION);
   lTmp += lHo_Exe;
   if (lTmp > 0)
   {
      iTmp = sprintf(acTmp, "%u", lTmp);
      memcpy(pLienRec->acExAmt, acTmp, iTmp);
   } 

   pLienRec->LF[0] = '\n';
   pLienRec->LF[1] = 0;
   return 0;
}

int Ala_ExtrVal()
{
   char      acBuf[1024], acRec[MAX_RECSIZE], acTmpFile[_MAX_PATH];
   int       iRet, iRecSize;
   long      lOut=0, lCnt=0;
   FILE      *fdOut, *fdIn;
   ALCO_SEC  *pInRec = (ALCO_SEC *)acRec;

   LogMsg0("Extracting final values from equalized file.");

   // Open input file
   GetIniString(myCounty.acCntyCode, "EQRollFile", "", acTmpFile, _MAX_PATH, acIniFile);
   LogMsg("Open %s for input", acTmpFile);
   fdIn = fopen(acTmpFile, "rb");
   if (fdIn == NULL)
   {
      LogMsg("***** Error opening input file: %s\n", acTmpFile);
      return -2;
   }  

   iRecSize = GetPrivateProfileInt(myCounty.acCntyCode, "TaxRecSize", 3066, acIniFile);

   // Open out file
   sprintf(acTmpFile, acValueTmpl, myCounty.acCntyCode, myCounty.acCntyCode);
   LogMsg("Open file %s for output", acTmpFile);
   fdOut = fopen(acTmpFile, "w");
   if (fdOut == NULL)
   {
      LogMsg("***** Error creating output file: %s\n", acTmpFile);
      return -4;
   }

   // Start loop 
   while (!feof(fdIn))
   {
      iRet = fread(acRec, 1, iRecSize, fdIn);
      if (iRet < iRecSize)
      {
         if (iRet > 2)
            LogMsg("*** Bad record: %.40s", acRec);
         lCnt++;
         continue;
      }

      // Skip unknown parcel
      if (!memcmp(pInRec->APN, "000", 3))
         continue;

      // Create new base record
      iRet = Ala_CreateValueRec(acBuf, acRec);
      if (!iRet)
      {
         fputs(acBuf, fdOut);
         lOut++;
      } else
      {
         LogMsg("---> Drop record %s (%d) ", acRec, lCnt); 
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   LogMsg("Total records processed: %u", lCnt);
   LogMsg("Total records output:    %u", lOut);

   // Close files
   if (fdIn)
      fclose(fdIn);
   if (fdOut)
      fclose(fdOut);

   return iRet;
}

/*********************************** loadAla ********************************
 *
 * Run LoadOne with following options:
 *    -L : to load lien
 *    -U : to load update roll
 *    -G : to process GrGr and create sale file for MergeAdr
 *    -Xa: extract attribute from O01.  ALA is no longer sending us char file.
 *    -Xc: extract cum sale from O01 file.
 *    -Ms: merge cum sale file
 *    -Ma: merge attibute file (Ala_Attr.dat), output from -Xa option
 *
 * Load lien:   -CALA -O -L -Xl [-Mg|-G] [-Usi|-Ms]
 * Load update: -CALA -O -U [-G] [-Usi] [-D2] [-T] [-Dr]
 * Notes:
 *    - Do not use -Ms and -Xc together in one run
 *
 ****************************************************************************/

int loadAla(int iSkip)
{
   int  iRet=0;
   char acTmpFile[_MAX_PATH], acGrGrFile[256], acTCF[_MAX_PATH], acTCP[_MAX_PATH];

   iApnLen = myCounty.iApnLen;
 
   // Loading Tax
   if (iLoadTax == TAX_LOADING)                    // -T 
   {
      TC_SetDateFmt();
      iRet = GetIniString(myCounty.acCntyCode, "TaxDist", "", acTmpFile, _MAX_PATH, acIniFile);
      if (iRet > 0)
         iRet = LoadTaxCodeTable(acTmpFile);

      GetIniString(myCounty.acCntyCode, "Main_TC", "", acTmpFile, _MAX_PATH, acIniFile);
      sprintf(acTCP, acTmpFile, 'P');
      sprintf(acTCF, acTmpFile, 'F');
      iRet = chkFileDate(acTCP, acTCF);
      if (iRet == 1 || iRet == -2)
         iRet = Update_TC(myCounty.acCntyCode, true, false);
      else 
      {
         // ALA has base, detail, agency
         iRet = Ala_Load_TaxBase(bTaxImport);
         if (!iRet)
         {
            iRet = Ala_Load_TaxDelq(bTaxImport);
            if (!iRet)
            {
               iRet = updateDelqFlag(myCounty.acCntyCode);
            }
         }
      }
   }

   if (!iLoadFlag && !lOptExtr)
      return iRet;

   // Extract final value
   if (lOptExtr & EXTR_FVAL)                       // -Xf
      iRet = Ala_ExtrVal();

   // Load tables - remove 6/6/2021
   //if (!LoadLUTable((char *)&acLookupTbl[0], "[Quality]", NULL, MAX_ATTR_ENTRIES))
   //{
   //   LogMsg("*** Error Looking for table [Quality] in %s", acLookupTbl);
   //   return 1;
   //}

   // Fix DocNum - uncomment this block to run as needed
   /*
   sprintf(acRawFile, acSaleTmpl, myCounty.acCntyCode, "dat");
   sprintf(acXferFile, acSaleTmpl, myCounty.acCntyCode, "new");
   iRet = Ala_ConvertSale(acRawFile, acXferFile);
   if (!iRet)
   {
      sprintf(acTmpFile, acSaleTmpl, myCounty.acCntyCode, "old");
      if (!_access(acTmpFile, 0))
         remove(acTmpFile);
      rename(acRawFile, acTmpFile);
      rename(acXferFile, acRawFile);
   }
   */

 	// Load char file
   if (iLoadFlag & EXTR_ATTR)                      // -Xa
   {
      iRet = GetIniString(myCounty.acCntyCode, "XCharFile", "", acTmpFile, _MAX_PATH, acIniFile);
      if (iRet > 20)
         strcpy(acCharFile, acTmpFile);
      iRet = Ala_ConvStdChar(acCharFile);
   }

   // Load 2-year transfer file
   if (iLoadFlag & (EXTR_SALE|UPDT_SALE))          // -Us -Xs
   {
      iRet = Ala_UpdateCumSale();
      if (!iRet)
      {
         // Signal merge sale to R01
         iLoadFlag |= MERG_CSAL;
         // Signal extract sale import file
         //iLoadFlag |= EXTR_ISAL;
      }
   }

   // Merge historical CHAR file
   //if (iLoadFlag & MERG_ATTR)                // -Ma
   //{
   //   char sLdrYear[8];

   //   if (lLienYear > 2004 && lLienYear < 2009)
   //      GetIniString(myCounty.acCntyCode, "H1CharFile", "", acTmp, _MAX_PATH, acIniFile);    
   //   else
   //      GetIniString(myCounty.acCntyCode, "H2CharFile", "", acTmp, _MAX_PATH, acIniFile);    
   //   sprintf(acTmpFile, acTmp, lLienYear, lLienYear);

   //   GetIniString("Data", "LdrRoll", "", acTmp, _MAX_PATH, acIniFile); 
   //   sprintf(sLdrYear, "%d", lLienYear);
   //   sprintf(acRawFile, acTmp, myCounty.acCntyCode, myCounty.acCntyCode, sLdrYear, "R01");
   //   iRet = Ala_MergeHChar(acRawFile, acTmpFile, iSkip);
   //   return iRet;
   //} 

   if (!iRet && (iLoadFlag & LOAD_GRGR))           // -G
   {
      // Move GrGr files to its subfolder
      iRet = moveGrGrFiles(acIniFile, myCounty.acCntyCode, false);
      if (iRet < 0)
      {
         bGrGrAvail = false;
         if (iRet < 0)
         {
            if (iRet == -1)
               LogMsg("***** Bad GrGrSrc file.  Please verify %s file", acIniFile);  
            else 
               LogMsg("***** Error renaming GrGr file");  

            return 1;
         }
      } else
      {
         LogMsg0("Load %s GrGr file", myCounty.acCntyCode);
         iRet = Ala_LoadGrGr(myCounty.acCntyCode);
         if (!iRet)
         {
            // Convert GRGR_EXP.SLS to ALA_GRGR.SLS
            sprintf(acGrGrFile, acEGrGrTmpl, myCounty.acCntyCode, "SLS");
            sprintf(acTmpFile, acGrGrTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "SLS");
            iRet = convertSaleData(myCounty.acCntyCode, acGrGrFile, CONV_GRGR_ALA, acTmpFile);

            // Do this until LoadOne can handle it properly
            if (!iRet)
            {
               //char sDbName[16];

               // Turn ON -Mg
               iLoadFlag |= MERG_GRGR;

               // Use -Gi to import GrGr
               //if (iLoadFlag & LOAD_LIEN)
               //   sprintf(sDbName, "LDR%d", lLienYear);
               //else
               //   sprintf(sDbName, "UPD%d", lLienYear);

               //GetIniString("Data", "SqlGrgrFile", "", acTmp, _MAX_PATH, acIniFile);
               //sprintf(acXferFile, acTmp, sDbName, myCounty.acCntyCode);
               //iRet = createSaleImport(myCounty.acCntyCode, acTmpFile, acXferFile, 2, false);

               //if (iRet > 0 && bGrgrImport)
               //{
               //   iRet = doSaleImportEx(myCounty.acCntyCode, sDbName, 2);
               //   if (iRet)
               //   {
               //      LogMsg("***** Error importing GRGR file %s", acXferFile);
               //      return iRet;
               //   }
               //}
            }
         }
      }
   } 

   // Extract lien
   if (iLoadFlag & EXTR_LIEN)                      // -Xl
      iRet = Ala_ExtrLien();

   iCharRecSize = GetPrivateProfileInt("ALA", "CharRecSize", 0, acIniFile);
   if (iLoadFlag & LOAD_LIEN)                      // -L
   {
      // Create Lien file
      LogMsg0("Load %s Lien file", myCounty.acCntyCode);
      iRet = Ala_CreateRoll(iSkip);
   } else if (iLoadFlag & LOAD_UPDT)               // -U
   {
      LogMsg0("Load %s roll update file", myCounty.acCntyCode);
      iRet = Ala_UpdateRoll(iSkip);
   }

   // Apply cum sale file to R01
   if (!iRet && (iLoadFlag & MERG_CSAL) )          // -Ms
   {
      // Apply Ala_Sale.sls to R01 file
      sprintf(acCSalFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "sls");
      iRet = ApplyCumSale(iSkip, acCSalFile, false, SALE_USE_SCUPDXFR);
   }

   // Apply cum sale file to R01
   if (!iRet && (iLoadFlag & MERG_GRGR) )          // -Mg
   {
      // Apply Ala_Grgr.sls to R01 file
      sprintf(acTmpFile, acGrGrTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "sls");
      iRet = ApplyCumSale(iSkip, acTmpFile, false, GRGR_UPD_XFR, 0);
      iLoadFlag |= LOAD_UPDT; // Force compare
   }

   // Fix TRA
   if (!iRet && (lOptMisc & M_OPT_FIXTRA))
      iRet = PQ_FixR01(myCounty.acCntyCode, PQ_FIX_TRA);

   if (!iRet && bMergeOthers)
   {
      // ALA has Fixture, PP_Val, Hpp, CLCA Land & CLCA Impr values
      sprintf(acTmpFile, acLienTmpl, myCounty.acCntyCode, myCounty.acCntyCode);
      if (!_access(acTmpFile, 0))
         iRet = PQ_MergeOthers(myCounty.acCntyCode, acTmpFile, GRP_ALA, iSkip);
      else
         LogMsg("***** Lien extract is missing: %s.  Please locate the file and retry", acTmpFile);
   }

   return iRet;
}