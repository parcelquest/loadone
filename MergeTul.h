#ifndef  _MERGE_TUL_H
#define  _MERGE_TUL_H

// Roll file - Ascii version
#define  ROFF_APN                   1
#define  ROFF_APN2                  13
#define  ROFF_APN3                  25
#define  ROFF_PARCEL_DOCNO          37
#define  ROFF_UNUSED0               49
#define  ROFF_APN4                  50
#define  ROFF_UNUSED1               62
#define  ROFF_STATUS                63
#define  ROFF_STATUS_DATE           64
#define  ROFF_ASSESSEE_REF_TYPE     72
#define  ROFF_ASSESSEE_REF_NUMBER   73
#define  ROFF_SECURED_TRA           85
#define  ROFF_UNSECURED_TRA         91
#define  ROFF_CURR_DOCNUM           97
#define  ROFF_UNUSED2               109
#define  ROFF_TAX_PARCEL            110
#define  ROFF_LEGAL                 112
#define  ROFF_TAX_CODE              162
#define  ROFF_OWNER_TOTAL           165
#define  ROFF_BASE_DATE             172
#define  ROFF_RETIRED_OWNER         180
#define  ROFF_LETTER_611            181
#define  ROFF_NOTICE_TYPE           182
#define  ROFF_CURR_DOCDATE          183
#define  ROFF_VALUE_INDEX           191
#define  ROFF_VALUE_1_CODE          196
#define  ROFF_VALUE_1               199
#define  ROFF_VALUE_1_APPR_DATE     208
#define  ROFF_VALUE_2_CODE          216
#define  ROFF_VALUE_2               219
#define  ROFF_VALUE_2_APPR_DATE     228
#define  ROFF_VALUE_3_CODE          236
#define  ROFF_VALUE_3               239
#define  ROFF_VALUE_3_APPR_DATE     248
#define  ROFF_VALUE_4_CODE          256
#define  ROFF_VALUE_4               259
#define  ROFF_VALUE_4_APPR_DATE     268
#define  ROFF_VALUE_5_CODE          276
#define  ROFF_VALUE_5               279
#define  ROFF_VALUE_5_APPR_DATE     288
#define  ROFF_VALUE_6_CODE          296
#define  ROFF_VALUE_6               299
#define  ROFF_VALUE_6_APPR_DATE     308
#define  ROFF_VALUE_7_CODE          316
#define  ROFF_VALUE_7               319
#define  ROFF_VALUE_7_APPR_DATE     328
#define  ROFF_VALUE_8_CODE          336
#define  ROFF_VALUE_8               339
#define  ROFF_VALUE_8_APPR_DATE     348
#define  ROFF_VALUE_9_CODE          356
#define  ROFF_VALUE_9               359
#define  ROFF_VALUE_9_APPR_DATE     368
#define  ROFF_VALUE_10_CODE         376
#define  ROFF_VALUE_10              379
#define  ROFF_VALUE_10_APPR_DATE    388
#define  ROFF_EXEMP_INDEX           396
#define  ROFF_EXEMP_1_CODE          401
#define  ROFF_EXEMP_1_AMOUNT        404
#define  ROFF_EXEMP_1_PCT           413
#define  ROFF_EXEMP_2_CODE          416
#define  ROFF_EXEMP_2_AMOUNT        419
#define  ROFF_EXEMP_2_PCT           428
#define  ROFF_EXEMP_3_CODE          431
#define  ROFF_EXEMP_3_AMOUNT        434
#define  ROFF_EXEMP_3_PCT           443
#define  ROFF_EXEMP_4_CODE          446
#define  ROFF_EXEMP_4_AMOUNT        449
#define  ROFF_EXEMP_4_PCT           458
#define  ROFF_EXEMP_5_CODE          461
#define  ROFF_EXEMP_5_AMOUNT        464
#define  ROFF_EXEMP_5_PCT           473
#define  ROFF_EXEMP_6_CODE          476
#define  ROFF_EXEMP_6_AMOUNT        479
#define  ROFF_EXEMP_6_PCT           488
#define  ROFF_EXEMP_7_CODE          491
#define  ROFF_EXEMP_7_AMOUNT        494
#define  ROFF_EXEMP_7_PCT           503
#define  ROFF_EXEMP_8_CODE          506
#define  ROFF_EXEMP_8_AMOUNT        509
#define  ROFF_EXEMP_8_PCT           518
#define  ROFF_EXEMP_9_CODE          521
#define  ROFF_EXEMP_9_AMOUNT        524
#define  ROFF_EXEMP_9_PCT           533
#define  ROFF_EXEMP_10_CODE         536
#define  ROFF_EXEMP_10_AMOUNT       539
#define  ROFF_EXEMP_10_PCT          548
#define  ROFF_ZONING                551
#define  ROFF_LAND_USE              561
#define  ROFF_SECONDARY_LAND_USE    565
#define  ROFF_SECONDARY_LAND_USE2   569
#define  ROFF_COMMENTS_PT           573
#define  ROFF_COMMENTS_PT_1         573
#define  ROFF_COMMENTS_PT_2         597
#define  ROFF_COMMENTS_PT_3         605
#define  ROFF_TOPS_ASSESSMENT       620
#define  ROFF_OTHER_BASE_YEAR_FLAG  633
#define  ROFF_CREATE_DOCDATE        634
#define  ROFF_SB813_SUPPL_FLAG      642
#define  ROFF_SUPPL_BILLING_COUNT   643
#define  ROFF_KILLING_DOCNO         646
#define  ROFF_UNUSED3               658
#define  ROFF_KILL_DATE             659
#define  ROFF_OWNER_SEQUENCE        667
#define  ROFF_FEE_TRANSFER          670
#define  ROFF_PENALTY_APPLIED       671
#define  ROFF_AUDITOR_CODE_AREA     672
#define  ROFF_ACRES                 678
#define  ROFF_ORIGINAL_APN          685
#define  ROFF_BASE_VALUE_INDEX      696
#define  ROFF_BVALUE_1_CODE         701
#define  ROFF_BAMOUNT_1             704
#define  ROFF_BVALUE_2_CODE         713
#define  ROFF_BAMOUNT_2             716
#define  ROFF_BVALUE_3_CODE         725
#define  ROFF_BAMOUNT_3             728
#define  ROFF_BVALUE_4_CODE         737
#define  ROFF_BAMOUNT_4             740
#define  ROFF_CITRUS_CODE           749
#define  ROFF_CITRUS_VALUE          752
#define  ROFF_REPORT_CODES          761
#define  ROFF_INFLATION_FLAG        765
#define  ROFF_ADJUSTMENT_AMOUNT     766
#define  ROFF_UNUSED4               775
#define  ROFF_UFO_DELETE_FLAG       784

#define  RSIZ_APN                   12
#define  RSIZ_APN2                  12
#define  RSIZ_APN3                  12
#define  RSIZ_PARCEL_DOCNO          12
#define  RSIZ_UNUSED0               1
#define  RSIZ_APN4                  12
#define  RSIZ_UNUSED1               1
#define  RSIZ_STATUS                1
#define  RSIZ_STATUS_DATE           8
#define  RSIZ_ASSESSEE_REF_TYPE     1
#define  RSIZ_ASSESSEE_REF_NUMBER   12
#define  RSIZ_TRA                   6
#define  RSIZ_CURR_DOCNUM           12
#define  RSIZ_UNUSED2               1
#define  RSIZ_TAX_PARCEL            2
#define  RSIZ_LEGAL                 50
#define  RSIZ_TAX_CODE              3
#define  RSIZ_OWNER_TOTAL           7
#define  RSIZ_BASE_DATE             8
#define  RSIZ_RETIRED_OWNER         1
#define  RSIZ_LETTER_611            1
#define  RSIZ_NOTICE_TYPE           1
#define  RSIZ_CURR_DOCDATE          8
#define  RSIZ_VALUE_INDEX           5
#define  RSIZ_VALUE_CODE            3
#define  RSIZ_VALUE_AMT             9
#define  RSIZ_VALUE_DATE            8
#define  RSIZ_EXEMP_INDEX           5
#define  RSIZ_EXEMP_CODE            3
#define  RSIZ_EXEMP_AMT             9
#define  RSIZ_EXEMP_PCT             3
#define  RSIZ_ZONING                10
#define  RSIZ_LAND_USE              4
#define  RSIZ_SECONDARY_LAND_USE    4
#define  RSIZ_SECONDARY_LAND_USE2   4
#define  RSIZ_COMMENTS_PT           47
#define  RSIZ_COMMENTS_PT_1         24
#define  RSIZ_COMMENTS_PT_2         8
#define  RSIZ_COMMENTS_PT_3         15
#define  RSIZ_TOPS_ASSESSMENT       13
#define  RSIZ_OTHER_BASE_YEAR_FLAG  1
#define  RSIZ_CREATE_DOCDATE        8
#define  RSIZ_SB813_SUPPL_FLAG      1
#define  RSIZ_SUPPL_BILLING_COUNT   3
#define  RSIZ_KILLING_DOCNO         12
#define  RSIZ_UNUSED3               1
#define  RSIZ_KILL_DATE             8
#define  RSIZ_OWNER_SEQUENCE        3
#define  RSIZ_FEE_TRANSFER          1
#define  RSIZ_PENALTY_APPLIED       1
#define  RSIZ_AUDITOR_CODE_AREA     6
#define  RSIZ_ACRES                 7
#define  RSIZ_ORIGINAL_APN          11
#define  RSIZ_BASE_VALUE_INDEX      5
#define  RSIZ_CITRUS_CODE           3
#define  RSIZ_CITRUS_VALUE          9
#define  RSIZ_REPORT_CODES          4
#define  RSIZ_INFLATION_FLAG        1
#define  RSIZ_ADJUSTMENT_AMOUNT     9
#define  RSIZ_UNUSED4               9
#define  RSIZ_UFO_DELETE_FLAG       1
#define  MAX_TUL_VAL                10

typedef struct _tTulVal
{
   char  Code[RSIZ_VALUE_CODE];
   char  Amount[RSIZ_VALUE_AMT];
   char  Date[RSIZ_VALUE_DATE];
} TUL_VAL;
typedef struct _tTulExe
{
   char  Code[RSIZ_EXEMP_CODE];
   char  Amount[RSIZ_EXEMP_AMT];
   char  Percent[RSIZ_EXEMP_PCT];
} TUL_EXE;

// Roll Record
typedef struct _tTulRoll
{  // 784-bytes unpacked
   char  Apn[RSIZ_APN];
   char  Apn2[RSIZ_APN];
   char  Apn3[RSIZ_APN];
   char  Parcel_Docno[RSIZ_PARCEL_DOCNO];
   char  Unused0;
   char  Apn4[RSIZ_APN];
   char  Unused1;
   char  Status;                    // A=ACTIVE,I=INACT,D=DEFFERED INACT,P=POSTPONED ACTIVE
   char  Status_Date[RSIZ_STATUS_DATE];
   char  Assessee_Ref_Type;
   char  Assessee_Ref_Number[RSIZ_ASSESSEE_REF_NUMBER];
   char  Tra_Sec[RSIZ_TRA];
   char  Tra_Unsec[RSIZ_TRA];
   char  Curr_DocNum[RSIZ_CURR_DOCNUM];
   char  Unused2;
   char  Tax_Parcel[RSIZ_TAX_PARCEL];
   char  Legal[RSIZ_LEGAL];
   char  Tax_Code[RSIZ_TAX_CODE];
   char  Owner_Total[RSIZ_OWNER_TOTAL];
   char  Base_Date[RSIZ_BASE_DATE];
   char  Retired_Owner;             // Y/N
   char  Letter_611;                // Y/N
   char  Notice_Type;
   char  Curr_Docdate[RSIZ_CURR_DOCDATE];
   char  Value_Index[RSIZ_VALUE_INDEX];
   TUL_VAL Value[MAX_TUL_VAL];
   char  Exemp_Index[RSIZ_EXEMP_INDEX];
   TUL_EXE Exemp[MAX_TUL_VAL];
   char  Zoning[RSIZ_ZONING];       // PRI - 4, SEC - 6 OR ZONE - 6, YR - 4
   char  Primary_Land_Use[RSIZ_LAND_USE];
   char  Secondary_Land_Use[RSIZ_LAND_USE];
   char  Secondary_Land_Use2[RSIZ_LAND_USE];
   char  Comments[RSIZ_COMMENTS_PT];
   char  Tops_Assessment[RSIZ_TOPS_ASSESSMENT];
   char  Other_Base_Year_Flag;
   char  Create_Docdate[RSIZ_CREATE_DOCDATE];
   char  SB813_Suppl_Flag;
   char  Suppl_Billing_Count[RSIZ_SUPPL_BILLING_COUNT];
   char  Killing_Docno[RSIZ_KILLING_DOCNO];
   char  Unused3;
   char  Kill_Date[RSIZ_KILL_DATE];
   char  Owner_Sequence[RSIZ_OWNER_SEQUENCE];
   char  Fee_Transfer;              // Y/N
   char  Penalty_Applied;           // Y/N
   char  Auditor_Code_Area[RSIZ_AUDITOR_CODE_AREA];
   char  Acres[RSIZ_ACRES];         // V99
   char  Original_Apn[RSIZ_ORIGINAL_APN];
   char  Base_Value_Index[RSIZ_BASE_VALUE_INDEX];
   char  Value_1_Code[RSIZ_VALUE_CODE];
   char  Amount_1[RSIZ_VALUE_AMT];
   char  Value_2_Code[RSIZ_VALUE_CODE];
   char  Amount_2[RSIZ_VALUE_AMT];
   char  Value_3_Code[RSIZ_VALUE_CODE];
   char  Amount_3[RSIZ_VALUE_AMT];
   char  Value_4_Code[RSIZ_VALUE_CODE];
   char  Amount_4[RSIZ_VALUE_AMT];
   char  Citrus_Code[RSIZ_CITRUS_CODE];
   char  Citrus_Value[RSIZ_CITRUS_VALUE];
   char  Report_Codes[RSIZ_REPORT_CODES];
   char  Inflation_Flag;
   char  Adj_Amt[RSIZ_ADJUSTMENT_AMOUNT];
   char  Unused4[RSIZ_UNUSED4];
   char  Ufo_Delete_Flag;
} TUL_ROLL;

// CHAR file - EBCDIC and Ascii version
#define  CSIZ_APN                 12
#define  CSIZ_PARCEL_TYPE         1
#define  CSIZ_LOT_SQFT            7
#define  CSIZ_IREGGULAR_SHAPE     1
#define  CSIZ_TOTAL_BLDG_SQFT     7
#define  CSIZ_ZONE                10
#define  CSIZ_NUMBER_OF_BLDGS     2
#define  CSIZ_TOTAL_ROOMS         3
#define  CSIZ_NUMBER_OF_UNITS     3
#define  CSIZ_BATHROOMS           4
#define  CSIZ_OUTBUILDING_COUNT   2
#define  CSIZ_OUTBUILDING_SIZE    7
#define  CSIZ_HEAT_COOL_FLG       1
#define  CSIZ_HEAT_COOL_DESC      25
#define  CSIZ_MAIN_BLDG_SQFT      7
#define  CSIZ_CONST_YEAR          4
#define  CSIZ_CONST_TYPE          1
#define  CSIZ_CLASS               3
#define  CSIZ_UNUSED              1
#define  CSIZ_STORIES             2
#define  CSIZ_MISC_CODES          10
#define  CSIZ_WATER_SERVICE       4
#define  CSIZ_SANITATION          4
#define  CSIZ_BEDROOMS            2
#define  CSIZ_GARAGE_SIZE         6
#define  CSIZ_CARPORT_SIZE        6
#define  CSIZ_POOL                1
#define  CSIZ_SPA                 1
#define  CSIZ_FIREPLACE           2
#define  CSIZ_DECK_SIZE           6
#define  CSIZ_PATIO_SIZE          6
#define  CSIZ_RENTABLE_SPACE      7
#define  CSIZ_OFFICE_SPACE        7
#define  CSIZ_PARKING_LOT_SIZE    7
#define  CSIZ_FIRE_SPRINKLERS     1
#define  CSIZ_WALL_HEIGHT         2
#define  CSIZ_NUMBER_OF_FLOORS    2
#define  CSIZ_NOTES               64
#define  CSIZ_LAST_UPDATED        8
#define  CSIZ_UFO_DELETE          1

#define  COFF_APN                 1
#define  COFF_PARCEL_TYPE         13
#define  COFF_LOT_SQFT            14
#define  COFF_IREGGULAR_SHAPE     21
#define  COFF_TOTAL_BLDG_SQFT     22   // Do not use, no data
#define  COFF_ZONE                29
#define  COFF_NUMBER_OF_BLDGS     39
#define  COFF_TOTAL_ROOMS         41
#define  COFF_NUMBER_OF_UNITS     44
#define  COFF_BATHROOMS           47
#define  COFF_OUTBUILDING_COUNT   51
#define  COFF_OUTBUILDING_SIZE    53
#define  COFF_HEAT_COOL_FLG       60
#define  COFF_HEAT_COOL_DESC      61
#define  COFF_MAIN_BLDG_SQFT      86
#define  COFF_CONST_YEAR          93
#define  COFF_CONST_TYPE          97
#define  COFF_CLASS               98
#define  COFF_UNUSED              101
#define  COFF_STORIES             102
#define  COFF_MISC_CODES          104
#define  COFF_WATER_SERVICE       114
#define  COFF_SANITATION          118
#define  COFF_BEDROOMS            122
#define  COFF_GARAGE_SIZE         124
#define  COFF_CARPORT_SIZE        130
#define  COFF_POOL                136
#define  COFF_SPA                 137
#define  COFF_FIREPLACE           138
#define  COFF_DECK_SIZE           140
#define  COFF_PATIO_SIZE          146
#define  COFF_RENTABLE_SPACE      152
#define  COFF_OFFICE_SPACE        159
#define  COFF_PARKING_LOT_SIZE    166
#define  COFF_FIRE_SPRINKLERS     173
#define  COFF_WALL_HEIGHT         174
#define  COFF_NUMBER_OF_FLOORS    176
#define  COFF_NOTES               178
#define  COFF_LAST_UPDATED        242
#define  COFF_UFO_DELETE          250

// Characteristic record
typedef struct _tTulChar
{
   char  Apn              [CSIZ_APN];
   char  Parcel_Type;
   char  LotSqft          [CSIZ_LOT_SQFT];
   char  Ireggular_Shape;
   char  Total_BldgSqft   [CSIZ_TOTAL_BLDG_SQFT];  // No data
   char  Zoning           [CSIZ_ZONE];
   char  Number_Of_Bldgs  [CSIZ_NUMBER_OF_BLDGS];
   char  Total_Rooms      [CSIZ_TOTAL_ROOMS];
   char  Number_Of_Units  [CSIZ_NUMBER_OF_UNITS];
   char  Bathrooms        [CSIZ_BATHROOMS];
   char  Outbuilding_Count[CSIZ_OUTBUILDING_COUNT];
   char  Outbuilding_Size [CSIZ_OUTBUILDING_SIZE];
   char  Heat_Cool_Flg;
   char  Heat_Cool_Desc   [CSIZ_HEAT_COOL_DESC];
   char  Main_Bldg_Sqft   [CSIZ_MAIN_BLDG_SQFT];
   char  Const_Year       [CSIZ_CONST_YEAR];
   char  Const_Type;
   char  QualClass        [CSIZ_CLASS];
   char  Unused;
   char  Stories          [CSIZ_STORIES];
   char  Misc_Codes       [CSIZ_MISC_CODES];
   char  Water_Service    [CSIZ_WATER_SERVICE];
   char  Sanitation       [CSIZ_SANITATION];
   char  Bedrooms         [CSIZ_BEDROOMS];
   char  Garage_Size      [CSIZ_GARAGE_SIZE];
   char  Carport_Size     [CSIZ_CARPORT_SIZE];
   char  Pool;
   char  Spa;
   char  FirePlace        [CSIZ_FIREPLACE];
   char  Deck_Size        [CSIZ_DECK_SIZE];
   char  Patio_Size       [CSIZ_PATIO_SIZE];
   char  Rentable_Space   [CSIZ_RENTABLE_SPACE];
   char  Office_Space     [CSIZ_OFFICE_SPACE];
   char  Parking_Lot_Size [CSIZ_PARKING_LOT_SIZE];
   char  Fire_Sprinklers;
   char  Wall_Height      [CSIZ_WALL_HEIGHT];
   char  Number_Of_Floors [CSIZ_NUMBER_OF_FLOORS];
   char  Notes            [CSIZ_NOTES];
   char  Last_Updated     [CSIZ_LAST_UPDATED];
   char  Ufo_Delete;
} TUL_CHAR;

// Situs file - Ebcdic & Ascii version
#define  SIOFF_S_STREET              1
#define  SIOFF_S_STRNO              25
#define  SIOFF_S_STRTYPE            30
#define  SIOFF_S_STRDIR             34
#define  SIOFF_S_UNIT               36
#define  SIOFF_S_CITY               40
#define  SIOFF_APN                  44
#define  SIOFF_AGENCY_ID            56
#define  SIOFF_PROJECT_ID           60
#define  SIOFF_SEQNUM2              65
#define  SIOFF_FEE_PARCEL           69
#define  SIOFF_SEQNUM               81
#define  SIOFF_UNUSED1              85
#define  SIOFF_REC_TYPE             99
#define  SIOFF_PERMIT_NUMBER       101
#define  SIOFF_COMPLETION_DATE     106
#define  SIOFF_START_DATE          114
#define  SIOFF_SITUS_TRA           122
#define  SIOFF_UNUSED2             128
#define  SIOFF_UFO_DELETE_FLAG     134

#define  SISIZ_S_STREET            24
#define  SISIZ_S_STRNO             5
#define  SISIZ_S_STRTYPE           4
#define  SISIZ_S_STRDIR            2
#define  SISIZ_S_UNIT              4
#define  SISIZ_S_CITY              4
#define  SISIZ_APN                 12
#define  SISIZ_AGENCY_ID           4
#define  SISIZ_PROJECT_ID          5
#define  SISIZ_SEQNUM2             4
#define  SISIZ_FEE_PARCEL          12
#define  SISIZ_SEQNUM              4
#define  SISIZ_UNUSED1             14
#define  SISIZ_REC_TYPE            2
#define  SISIZ_PERMIT_NUMBER       5
#define  SISIZ_COMPLETION_DATE     8
#define  SISIZ_START_DATE          8
#define  SISIZ_SITUS_TRA           6
#define  SISIZ_UNUSED2             6
#define  SISIZ_UFO_DELETE_FLAG     1

// Situs record
typedef struct _tTulSitus
{  //
   char  StrName         [SISIZ_S_STREET       ];
   char  StrNum          [SISIZ_S_STRNO        ];
   char  StrType         [SISIZ_S_STRTYPE      ];
   char  StrDir          [SISIZ_S_STRDIR       ];
   char  Unit            [SISIZ_S_UNIT         ];
   char  City            [SISIZ_S_CITY         ];
   char  Apn             [SISIZ_APN            ];
   char  Agency_Id       [SISIZ_AGENCY_ID      ];
   char  Project_Id      [SISIZ_PROJECT_ID     ];
   char  Seqnum2         [SISIZ_SEQNUM2        ];
   char  Fee_Parcel      [SISIZ_FEE_PARCEL     ];
   char  Seqnum          [SISIZ_SEQNUM         ];
   char  Unused1         [SISIZ_UNUSED1        ];
   char  Rec_Type        [SISIZ_REC_TYPE       ];
   char  Permit_Number   [SISIZ_PERMIT_NUMBER  ];
   char  Completion_Date [SISIZ_COMPLETION_DATE];
   char  Start_Date      [SISIZ_START_DATE     ];
   char  Situs_Tra       [SISIZ_SITUS_TRA      ];
   char  Unused2         [SISIZ_UNUSED2        ];
   char  Ufo_Delete_Flag;
} TUL_SITUS;

// NAD file Ebcdic & Ascii version
#define  NOFF_REC_TYPE           1
#define  NOFF_APN                2
#define  NOFF_NAME               14
#define  NOFF_ADDR_LINE_1        55
#define  NOFF_ADDR_LINE_2        96
#define  NOFF_ADDR_LINE_3        126
#define  NOFF_ADDR_LINE_4        156
#define  NOFF_ADDR_FMT_FLAG      186
#define  NOFF_UNUSED             187

#define  NSIZ_REC_TYPE           1
#define  NSIZ_APN                12
#define  NSIZ_NAME               41
#define  NSIZ_ADDR_LINE_1        41
#define  NSIZ_ADDR_LINE_2        30
#define  NSIZ_ADDR_LINE_3        30
#define  NSIZ_ADDR_LINE_4        30
#define  NSIZ_ADDR_FMT_FLAG      1
#define  NSIZ_UNUSED             64

typedef struct _tTulNad
{  // 250-bytes + CRLF
   char  Rec_Type       [NSIZ_REC_TYPE     ];
   char  Apn            [NSIZ_APN          ];
   char  Name           [NSIZ_NAME         ];
   char  Addr_Line_1    [NSIZ_ADDR_LINE_1  ];
   char  Addr_Line_2    [NSIZ_ADDR_LINE_2  ];
   char  Addr_Line_3    [NSIZ_ADDR_LINE_3  ];
   char  Addr_Line_4    [NSIZ_ADDR_LINE_4  ];
   char  Addr_Fmt_Flag  [NSIZ_ADDR_FMT_FLAG];
   char  Unused         [NSIZ_UNUSED       ];
   char  CrLf[2];
} TUL_NAD;

// Sale file Ebcdic & Ascii version
#define  SSIZ_APN                12
#define  SSIZ_DATE_SORT          8
#define  SSIZ_TIME_SORT          6
#define  SSIZ_DOCNUM             12
#define  SSIZ_UNUSED1            1
#define  SSIZ_DOCDATE            8
#define  SSIZ_SELLER             41
#define  SSIZ_OWNER              41
#define  SSIZ_TRANSFER_DATE      8
#define  SSIZ_TRANSFER_TIME      6
#define  SSIZ_ACRES              7
#define  SSIZ_SALE_AMT           9
#define  SSIZ_DTT_AMT            11
#define  SSIZ_GROUP_SALE_FLAG    1
#define  SSIZ_GROUP_APN          12
#define  SSIZ_USE_CODE           4
#define  SSIZ_TRANSFER_TYPE      2
#define  SSIZ_NOTES_FLAG         1
#define  SSIZ_ENTER_DATE         8
#define  SSIZ_UNUSED2            11
#define  SSIZ_UFO_DELETE_FLAG    1

#define  SOFF_APN                1
#define  SOFF_DATE_SORT          13
#define  SOFF_TIME_SORT          21
#define  SOFF_DOCNUM             27
#define  SOFF_UNUSED1            39
#define  SOFF_DOCDATE            40
#define  SOFF_SELLER             48
#define  SOFF_OWNER              89
#define  SOFF_TRANSFER_DATE      130
#define  SOFF_TRANSFER_TIME      138
#define  SOFF_ACRES              144
#define  SOFF_SALE_AMT           151
#define  SOFF_DTT_AMT            160
#define  SOFF_GROUP_SALE_FLAG    171
#define  SOFF_GROUP_APN          172
#define  SOFF_USE_CODE           184
#define  SOFF_TRANSFER_TYPE      188
#define  SOFF_NOTES_FLAG         190
#define  SOFF_ENTER_DATE         191
#define  SOFF_UNUSED2            199
#define  SOFF_UFO_DELETE_FLAG    210

// Sale record
typedef struct _tTulSale
{  // 212-bytes
   char  Apn             [SSIZ_APN            ];
   char  Date_Sort       [SSIZ_DATE_SORT      ];
   char  Time_Sort       [SSIZ_TIME_SORT      ];
   char  DocNum          [SSIZ_DOCNUM         ];
   char  Unused1;
   char  DocDate         [SSIZ_DOCDATE        ];
   char  Seller          [SSIZ_SELLER         ];
   char  Owner           [SSIZ_OWNER          ];
   char  Transfer_Date   [SSIZ_TRANSFER_DATE  ];
   char  Transfer_Time   [SSIZ_TRANSFER_TIME  ];
   char  Acres           [SSIZ_ACRES          ];
   char  SalePrice       [SSIZ_SALE_AMT       ];
   char  Dtt_Amt         [SSIZ_DTT_AMT        ];
   char  Group_Sale_Flag;
   char  Group_Apn       [SSIZ_GROUP_APN      ];
   char  Use_Code        [SSIZ_USE_CODE       ];
   char  Transfer_Type   [SSIZ_TRANSFER_TYPE  ]; // Sale flag
   char  Notes_Flag;
   char  Enter_Date      [SSIZ_ENTER_DATE     ];
   char  Unused2         [SSIZ_UNUSED2        ];
   char  Ufo_Delete_Flag;
   char  CrLf[2];
} TUL_SALE;

// Owner file - Ascii version
#define  OOFF_APN                1
#define  OOFF_NAME               13
#define  OOFF_APN2               54
#define  OOFF_OWNER_SEQ          66
#define  OOFF_OWNERSHIP_PCT      69
#define  OOFF_DOCNUM             76
#define  OOFF_UNUSED1            88
#define  OOFF_VESTING            89
#define  OOFF_OWNER_NUMBER       91
#define  OOFF_OWNER_APPRDATE     94
#define  OOFF_OWNER_DOCDATE      102
#define  OOFF_UFO_DUMMY_CODE     110
#define  OOFF_OWNER_COMMENT      111
#define  OOFF_UNUSED2            135
#define  OOFF_UFO_DELETE_CODE    138

#define  OSIZ_APN                12
#define  OSIZ_NAME               41
#define  OSIZ_APN2               12
#define  OSIZ_OWNER_SEQ          3
#define  OSIZ_OWNERSHIP_PCT      7
#define  OSIZ_DOCNUM             12
#define  OSIZ_UNUSED1            1
#define  OSIZ_VESTING            2
#define  OSIZ_OWNER_NUMBER       3
#define  OSIZ_OWNER_APPRDATE     8
#define  OSIZ_OWNER_DOCDATE      8
#define  OSIZ_UFO_DUMMY_CODE     1
#define  OSIZ_OWNER_COMMENT      24
#define  OSIZ_UNUSED2            3
#define  OSIZ_UFO_DELETE_CODE    1

typedef struct _tTulOwner
{
   char  Apn            [OSIZ_APN            ];
   char  Name           [OSIZ_NAME           ];
   char  Apn2           [OSIZ_APN2           ];
   char  Owner_Seq      [OSIZ_OWNER_SEQ      ];
   char  Ownership_Pct  [OSIZ_OWNERSHIP_PCT  ];
   char  Docnum         [OSIZ_DOCNUM         ];
   char  Unused1;
   char  Vesting        [OSIZ_VESTING        ];
   char  Owner_Number   [OSIZ_OWNER_NUMBER   ];
   char  Owner_Apprdate [OSIZ_OWNER_APPRDATE ];
   char  Owner_Docdate  [OSIZ_OWNER_DOCDATE  ];
   char  Ufo_Dummy_Code;
   char  Owner_Comment  [OSIZ_OWNER_COMMENT  ];
   char  Unused2        [OSIZ_UNUSED2        ];
   char  Ufo_Delete_Code;
} TUL_OWNER;

// Lien extract layout
/*
#define  LOFF_APN                1
#define  LOFF_APN2               13
#define  LOFF_MKTLAND            25
#define  LOFF_RSTLAND            35
#define  LOFF_IMPR               45
#define  LOFF_MKTIMPR            55
#define  LOFF_RSTIMPR            65
#define  LOFF_FIXTURE            75
#define  LOFF_PERSPROP1          85
#define  LOFF_PERSPROP2          95
#define  LOFF_OTHERVAL           105
#define  LOFF_GROSSVAL           115
#define  LOFF_TOTALEXE           125
#define  LOFF_RATIO              135
#define  LOFF_TAXCODE            138
#define  LOFF_HOFLG              141
#define  LOFF_STATUS             142

typedef struct _tTulLien
{
   char  Apn[RSIZ_APN];
   char  Apn2[RSIZ_APN];
   char  MktLand[SIZ_LAND];        // V01 - Land market value
   char  RstLand[SIZ_LAND];        // V02 - Land restricted value
   char  StrImpr[SIZ_LAND];        // V11 - Structure Impr
   char  MktGrowImpr[SIZ_LAND];    // V12 - Market grow impr
   char  RstGrowImpr[SIZ_LAND];    // V13 - Restricted grow impr
   char  Fixture[SIZ_LAND];        // V14 - Fixture or manufacturing equipment
   char  PersProp1[SIZ_LAND];      // V21 -
   char  PersProp2[SIZ_LAND];      // V31 - Other
   char  Land[SIZ_LAND];
   char  Impr[SIZ_LAND];
   char  OtherVal[SIZ_LAND];       // Total excluding Land and Impr
   char  GrossVal[SIZ_LAND];       // Total values
   char  TotalExe[SIZ_EXE_TOTAL];  // Total Exe
   char  Ratio[SIZ_RATIO];
   char  TaxCode[RSIZ_TAX_CODE];
   char  HO_Flg;                   // E01 - Y/N
   char  Status;
   char  CRLF[2];
} TUL_LIEN;
*/
// Lien ebcdic
#define  LOFF_APN                   1
#define  LOFF_TAXYEAR               13
#define  LOFF_BOC_NO                17
#define  LOFF_ROLL_TYPE             22
#define  LOFF_TAX_TYPE              23
#define  LOFF_RECORD_STATUS         25
#define  LOFF_TRA                   27
#define  LOFF_CORTAC_NO             33
#define  LOFF_CORTAC_CUSTNO         37
#define  LOFF_LOAN_ID               42
#define  LOFF_CURR_OWNER            67
#define  LOFF_ALT_APN               108
#define  LOFF_BOC_ENDDATE           127
#define  LOFF_BOC_VALUE             135   // Pack dec (9)
#define  LOFF_ASSESSEE              140
#define  LOFF_MAILADDR1             181
#define  LOFF_MAILADDR2             222
#define  LOFF_MAILADDR3             252
#define  LOFF_MAILADDR4             282   // Used for UNFORMATTED addr
#define  LOFF_MAIL_FMT_FLG          312
#define  LOFF_SITUSADDR1            313
#define  LOFF_SITUSADDR2            345
#define  LOFF_SOLD_TO_ST            377
#define  LOFF_SOLD_TO_ST_NO         385
#define  LOFF_UNUSED0               391
#define  LOFF_DATE_REDEEMED_TO_ST   397
#define  LOFF_ACRES                 405   // Pack dec (7)
#define  LOFF_LAND                  409
#define  LOFF_FIXT_IMPR             414
#define  LOFF_GROWING_IMPR          419
#define  LOFF_STRUCTURE_IMPR        424
#define  LOFF_PERS_IMPR             429
#define  LOFF_BUSINESS_INV          434
#define  LOFF_NET                   439
#define  LOFF_PENALTY               444
#define  LOFF_UNUSED1               445
#define  LOFF_INT_DATE              450
#define  LOFF_AG_PRESERVE_CODE      458
#define  LOFF_USECODE               459
#define  LOFF_BUSCODE               463
#define  LOFF_REDEMPTION_CK_FLG     465
#define  LOFF_PRIOR_UNSEC_DISCHRG   466
#define  LOFF_NAMECHG_FLG           467
#define  LOFF_EXEMP_IND             468
#define  LOFF_EXEMP_CODE            470
#define  LOFF_EXEMP_AMT             472   // Pack dec (9)
#define  LOFF_UNUSED2               477
#define  LOFF_TAXCODE_IND           491
#define  LOFF_TAXCODE               493
#define  LOFF_TAXRATE               496   // Pack dec (8) 99V999999
#define  LOFF_TAXCODE_TYPE          501
#define  LOFF_TAX_1ST_AMT           502   // 999999999V99 (11)
#define  LOFF_TAX_1ST_PEN           508   // 9999999V99 (9)
#define  LOFF_TAX_2ND_AMT           513   // 999999999V99 (11)
#define  LOFF_TAX_2ND_PEN           519   // 9999999V99 (9)
#define  LOFF_FILLER1               524
#define  LOFF_TAX_1ST_DUE_DATE      989
#define  LOFF_TAX_1ST_TOTAL         997   // 999999999V99 (11)
#define  LOFF_TAX_1ST_PEN_AMT       1003  // 9999999V99 (9)
#define  LOFF_TAX_1ST_PEN_CODE      1008
#define  LOFF_TAX_1ST_COL_NO        1009
#define  LOFF_TAX_1ST_COL_DATE      1019
#define  LOFF_CO_1ST_PAID_FLG       1027
#define  LOFF_BASE_APN_ROLLOVER_FLG 1028
#define  LOFF_UNUSED3               1029
#define  LOFF_TAX_2ND_DUE_DATE      1031
#define  LOFF_TAX_2ND_TOTAL         1039  // 999999999V99 (11)
#define  LOFF_TAX_2ND_PEN_AMT       1045  // 9999999V99 (9)
#define  LOFF_TAX_COST              1050  // 99999V99 (7)
#define  LOFF_TAX_2ND_PEN_CODE      1054
#define  LOFF_TAX_2ND_COL_NO        1055
#define  LOFF_TAX_2ND_COL_DATE      1065
#define  LOFF_REFUND_AMT            1073  // 999999999V99 (11)
#define  LOFF_REFUND_REFNO          1079
#define  LOFF_MISC_COST             1084  // 9999999V99 (9)
#define  LOFF_ROLLCHG_INDEX         1089
#define  LOFF_ROLLCHG_DATA          1091
#define  LOFF_TRAN_DATE_1ST         1523
#define  LOFF_TRAN_DATE_2ND         1531
#define  LOFF_UNUSED4               1539
#define  LOFF_TAX_1ST_AMT_PAID      1542  // 999999999V99 (11)
#define  LOFF_TAX_2ND_AMT_PAID      1548  // 999999999V99 (11)
#define  LOFF_1ST_INSTL_CANCL_DATE  1554
#define  LOFF_2ND_INSTL_CANCL_DATE  1562
#define  LOFF_APPORTION_YR          1570
#define  LOFF_UNUSED5               1574
#define  LOFF_REFUND_VENDOR         1580
#define  LOFF_INT_ON_REF_AMT        1708  // 999999999V99 (11)
#define  LOFF_UNUSED6               1714
#define  LOFF_REFUND_WARRANT_NO     1717
#define  LOFF_REFUND_DATE           1724
#define  LOFF_SUPPL_BILL_CNT        1732
#define  LOFF_TRANSFER_COMPL_DATE   1735
#define  LOFF_PRORATION_FACTOR      1743  // 999V999 (6)
#define  LOFF_PRORATION_PCT         1749  // 999V999 (6)
#define  LOFF_DAYS_OWNED            1755
#define  LOFF_OWNERSHP_PERIOD       1758
#define  LOFF_UNUSED7               1761
#define  LOFF_UNSEC_DELQ_DATA       1764
#define  LOFF_FEE_ASMT_NO           1868
#define  LOFF_ORIG_ASMT_NO          1880
#define  LOFF_UNUSED8               1892
#define  LOFF_ALT_SORT_KEY          1910
#define  LOFF_UFO_USES              1927
#define  LOFF_REFUND_AUTH_DATE      1935
#define  LOFF_ORG_DUE_DATE          1943
#define  LOFF_BANKRUPTCY_DATA       1951
#define  LOFF_POWER_TO_SELL_DATA    1977
#define  LOFF_XREF_APN              2041
#define  LOFF_DEED_DATE             2053
#define  LOFF_DEED_NUMBER           2061
#define  LOFF_BANKRUPTCY_REM_DATE   2073
#define  LOFF_ZIP_SORT              2081
#define  LOFF_TAXBILL_PRINT_DATE    2091
#define  LOFF_APPORT_INS1_DATE      2099
#define  LOFF_APPORT_INS2_DATE      2107
#define  LOFF_UNUSED9               2115
#define  LOFF_SITUS_STRNUM          2128
#define  LOFF_SITUS_STRDIR          2133
#define  LOFF_SITUS_STRNAME         2135
#define  LOFF_SITUS_STRTYPE         2159
#define  LOFF_SITUS_UNIT            2163
#define  LOFF_SITUS_COMMUNITY       2167
#define  LOFF_SITUS_MULTI           2171
#define  LOFF_UNUSED10              2172

#define  LSIZ_APN                   12
#define  LSIZ_TAXYEAR               4
#define  LSIZ_BOC_NO                5
#define  LSIZ_ROLL_TYPE             1
#define  LSIZ_TAX_TYPE              2
#define  LSIZ_RECORD_STATUS         2
#define  LSIZ_TRA                   6
#define  LSIZ_CORTAC_NO             4
#define  LSIZ_CORTAC_CUSTNO         5
#define  LSIZ_LOAN_ID               25
#define  LSIZ_CURR_OWNER            41
#define  LSIZ_ALT_APN               19
#define  LSIZ_BOC_ENDDATE           8
#define  LSIZ_BOC_VALUE             5     // Pack dec (9)
#define  ASIZ_BOC_VALUE             9
#define  LSIZ_ASSESSEE              41
#define  LSIZ_MAILADDR1             41
#define  LSIZ_MAILADDR2             30
#define  LSIZ_MAILADDR3             30
#define  LSIZ_MAILADDR4             30    // UNFORMATTED
#define  LSIZ_MAIL_FMT_FLG          1
#define  LSIZ_SITUSADDR1            32
#define  LSIZ_SITUSADDR2            32
#define  LSIZ_SOLD_TO_ST            8
#define  LSIZ_SOLD_TO_ST_NO         6
#define  LSIZ_UNUSED0               6
#define  LSIZ_DATE_REDEEMED_TO_ST   8
#define  LSIZ_ACRES                 4     // Pack dec (7)
#define  ASIZ_ACRES                 7
#define  LSIZ_LAND                  5
#define  LSIZ_FIXT_IMPR             5
#define  LSIZ_GROWING_IMPR          5
#define  LSIZ_STRUCTURE_IMPR        5
#define  LSIZ_PERS_IMPR             5
#define  LSIZ_BUSINESS_INV          5
#define  LSIZ_NET                   5
#define  LSIZ_PENALTY               1
#define  LSIZ_UNUSED1               5
#define  LSIZ_INT_DATE              8
#define  LSIZ_AG_PRESERVE_CODE      1
#define  LSIZ_USECODE               4
#define  LSIZ_BUSCODE               2
#define  LSIZ_REDEMPTION_CK_FLG     1
#define  LSIZ_PRIOR_UNSEC_DISCHRG   1
#define  LSIZ_NAMECHG_FLG           1
#define  LSIZ_EXEMP_IND             2
#define  ASIZ_EXEMP_IND             5
#define  LSIZ_EXEMP_CODE            2     // 1-byte code, 1-byte filler
#define  LSIZ_EXEMP_AMT             5     // Pack dec (9)
#define  ASIZ_EXEMP_AMT             9
#define  LSIZ_UNUSED2               14
#define  LSIZ_TAXCODE_IND           2
#define  ASIZ_TAXCODE_IND           5
#define  LSIZ_TAXCODE               3
#define  LSIZ_TAXRATE               5     // Pack dec (8) 99V999999
#define  ASIZ_TAXRATE               9
#define  LSIZ_TAXCODE_TYPE          1
#define  LSIZ_TAX_1ST_AMT           6     // 999999999V99 (11)
#define  LSIZ_TAX_1ST_PEN           5     // 9999999V99 (9)
#define  LSIZ_TAX_2ND_AMT           6     // 999999999V99 (11)
#define  LSIZ_TAX_2ND_PEN           5     // 9999999V99 (9)
#define  ASIZ_TAX_1ST_AMT           11
#define  ASIZ_TAX_1ST_PEN           9
#define  ASIZ_TAX_2ND_AMT           11
#define  ASIZ_TAX_2ND_PEN           9
#define  LSIZ_FILLER1               465
#define  LSIZ_TAX_1ST_DUE_DATE      8
#define  LSIZ_TAX_1ST_TOTAL         6     // 999999999V99 (11)
#define  LSIZ_TAX_1ST_PEN_AMT       5     // 9999999V99 (9)
#define  ASIZ_TAX_1ST_TOTAL         11
#define  ASIZ_TAX_1ST_PEN_AMT       9
#define  LSIZ_TAX_1ST_PEN_CODE      1
#define  LSIZ_TAX_1ST_COL_NO        10
#define  LSIZ_TAX_1ST_COL_DATE      8
#define  LSIZ_CO_1ST_PAID_FLG       1
#define  LSIZ_BASE_APN_ROLLOVER_FLG 1
#define  LSIZ_UNUSED3               2
#define  LSIZ_TAX_2ND_DUE_DATE      8
#define  LSIZ_TAX_2ND_TOTAL         6     // 999999999V99 (11)
#define  LSIZ_TAX_2ND_PEN_AMT       5     // 9999999V99 (9)
#define  LSIZ_TAX_COST              4     // 99999V99 (7)
#define  ASIZ_TAX_2ND_TOTAL         11
#define  ASIZ_TAX_2ND_PEN_AMT       9
#define  ASIZ_TAX_COST              7
#define  LSIZ_TAX_2ND_PEN_CODE      1
#define  LSIZ_TAX_2ND_COL_NO        10
#define  LSIZ_TAX_2ND_COL_DATE      8
#define  LSIZ_REFUND_AMT            6     // 999999999V99 (11)
#define  ASIZ_REFUND_AMT            11
#define  LSIZ_REFUND_REFNO          5
#define  LSIZ_MISC_COST             5     // 9999999V99 (9)
#define  ASIZ_MISC_COST             9
#define  LSIZ_ROLLCHG_INDEX         2
#define  ASIZ_ROLLCHG_INDEX         5
#define  LSIZ_ROLLCHG_DATA          432
#define  LSIZ_TRAN_DATE_1ST         8
#define  LSIZ_TRAN_DATE_2ND         8
#define  LSIZ_UNUSED4               3
#define  LSIZ_TAX_1ST_AMT_PAID      6     // 999999999V99 (11)
#define  LSIZ_TAX_2ND_AMT_PAID      6     // 999999999V99 (11)
#define  ASIZ_TAX_1ST_AMT_PAID      11
#define  ASIZ_TAX_2ND_AMT_PAID      11
#define  LSIZ_1ST_INSTL_CANCL_DATE  8
#define  LSIZ_2ND_INSTL_CANCL_DATE  8
#define  LSIZ_APPORTION_YR          4
#define  LSIZ_UNUSED5               6
#define  LSIZ_REFUND_VENDOR         128
#define  LSIZ_INT_ON_REF_AMT        6     // 999999999V99 (11)
#define  ASIZ_INT_ON_REF_AMT        11
#define  LSIZ_UNUSED6               3
#define  LSIZ_REFUND_WARRANT_NO     7
#define  LSIZ_REFUND_DATE           8
#define  LSIZ_SUPPL_BILL_CNT        3
#define  LSIZ_TRANSFER_COMPL_DATE   8
#define  LSIZ_PRORATION_FACTOR      6     // 999V999 (6)
#define  LSIZ_PRORATION_PCT         6     // 999V999 (6)
#define  LSIZ_DAYS_OWNED            3
#define  LSIZ_OWNERSHP_PERIOD       3
#define  LSIZ_UNUSED7               3
#define  LSIZ_UNSEC_DELQ_DATA       104
#define  LSIZ_FEE_ASMT_NO           12
#define  LSIZ_ORIG_ASMT_NO          12
#define  LSIZ_UNUSED8               18
#define  LSIZ_ALT_SORT_KEY          17
#define  LSIZ_UFO_USES              8
#define  LSIZ_REFUND_AUTH_DATE      8
#define  LSIZ_ORG_DUE_DATE          8
#define  LSIZ_BANKRUPTCY_DATA       26
#define  LSIZ_POWER_TO_SELL_DATA    64
#define  LSIZ_XREF_APN              12
#define  LSIZ_DEED_DATE             8
#define  LSIZ_DEED_NUMBER           12
#define  LSIZ_BANKRUPTCY_REM_DATE   8
#define  LSIZ_ZIP_SORT              10
#define  LSIZ_TAXBILL_PRINT_DATE    8
#define  LSIZ_APPORT_INS1_DATE      8
#define  LSIZ_APPORT_INS2_DATE      8
#define  LSIZ_UNUSED9               13
#define  LSIZ_SITUS_STRNUM          5
#define  LSIZ_SITUS_STRDIR          2
#define  LSIZ_SITUS_STRNAME         24
#define  LSIZ_SITUS_STRTYPE         4
#define  LSIZ_SITUS_UNIT            4
#define  LSIZ_SITUS_COMMUNITY       4
#define  LSIZ_SITUS_MULTI           1
#define  LSIZ_UNUSED10              9

// LDR 2015
#define  LOFF_1_APN                     1-1
#define  LOFF_1_TAX_YEAR                13-1
#define  LOFF_1_BOC_NO                  17-1
#define  LOFF_1_ROLL_TYPE               22-1
#define  LOFF_1_TAX_TYPE                23-1
#define  LOFF_1_RECORD_STATUS_CD        25-1
#define  LOFF_1_FILLER1                 26-1
#define  LOFF_1_TAX_RATE_AREA           27-1
#define  LOFF_1_CORTAC_NO               33-1
#define  LOFF_1_CORTAC_CUST_NO          37-1
#define  LOFF_1_CORTAC_LOAN_ID          42-1
#define  LOFF_1_CURR_OWNER_NAME         67-1
#define  LOFF_1_ALT_APN_KEY             108-1
#define  LOFF_1_BOC_END_DATE            127-1
#define  LOFF_1_BOC_VALUE               135-1
#define  LOFF_1_BOC_VALUE_SB            144-1
#define  LOFF_1_ASSESSEE_NAME           145-1
#define  LOFF_1_MAIL_ADDRESS_1          186-1
#define  LOFF_1_MAIL_ADDRESS_2          227-1
#define  LOFF_1_MAIL_ADDRESS_3          257-1
#define  LOFF_1_MAIL_ADDRESS_4          287-1
#define  LOFF_1_FORMATTED_ADDR          317-1
#define  LOFF_1_PROPDESC                318-1
//#define  LOFF_1_SITUS_LN_1              318-1
//#define  LOFF_1_SITUS_LN_2              350-1
#define  LOFF_1_DATE_SOLD               382-1
#define  LOFF_1_SOLD_TO_STATE_NO        390-1
#define  LOFF_1_FILLER2                 396-1
#define  LOFF_1_DATE_REDEEMED           402-1
#define  LOFF_1_ACRES                   410-1
#define  LOFF_1_ACRES_SB                417-1
#define  LOFF_1_MARKET_LAND_VAL         418-1
#define  LOFF_1_MARKET_LAND_VAL_SB      427-1
#define  LOFF_1_FIXED_IMPR_VAL          428-1
#define  LOFF_1_FIXED_IMPR_VAL_SB       437-1
#define  LOFF_1_GROWING_IMPR_VAL        438-1
#define  LOFF_1_GROWING_IMPR_VAL_SB     447-1
#define  LOFF_1_STRUCT_IMPR_VAL         448-1
#define  LOFF_1_STRUCT_IMPR_VAL_SB      457-1
#define  LOFF_1_PERSON_PROP_VAL         458-1
#define  LOFF_1_PERSON_PROP_VAL_SB      467-1
#define  LOFF_1_BUSINESS_INV            468-1
#define  LOFF_1_BUSINESS_INV_SB         477-1
#define  LOFF_1_NET_VALUE               478-1
#define  LOFF_1_NET_VALUE_SB            487-1
#define  LOFF_1_10_PERCENT_PENALTY      488-1
#define  LOFF_1_FILLER3                 489-1
#define  LOFF_1_INTEREST_DATE           494-1
#define  LOFF_1_AG_PRESERVE_FLAG        502-1
#define  LOFF_1_USE_CODE                503-1
#define  LOFF_1_BUSINESS_CODE           507-1
#define  LOFF_1_REDEMPTION_CK_FLAG      509-1
#define  LOFF_1_PRIOR_UNSEC_DISCHRGE    510-1
#define  LOFF_1_OWNER_NAME_CHANGEFLG    511-1
#define  LOFF_1_PROPERTY_EXMPT_IND      512-1
#define  LOFF_1_EXMPT_CODE_1            514-1
#define  LOFF_1_FILLER4_1               515-1
#define  LOFF_1_PROP_EXMPT_AMT_1        516-1
#define  LOFF_1_PROP_EXMPT_AMT_1_SB     525-1
#define  LOFF_1_EXMPT_CODE_2            526-1
#define  LOFF_1_FILLER4_2               527-1
#define  LOFF_1_PROP_EXMPT_AMT_2        528-1
#define  LOFF_1_PROP_EXMPT_AMT_2_SB     537-1
#define  LOFF_1_EXMPT_CODE_3            538-1
#define  LOFF_1_FILLER4_3               539-1
#define  LOFF_1_PROP_EXMPT_AMT_3        540-1
#define  LOFF_1_PROP_EXMPT_AMT_3_SB     549-1
#define  LOFF_1_TAX_CODE_IND            550-1
#define  LOFF_1_TAX_CODE_DATA_ARRAY     552-1
#define  LOFF_1_1ST_INSTL_DUE_DATE      1480-1
#define  LOFF_1_1ST_INSTL_TOTAL_TAX     1488-1
#define  LOFF_1_1ST_INSTL_TOTAL_TAX_SB  1499-1
#define  LOFF_1_1ST_INSTL_PEN_AMT       1500-1
#define  LOFF_1_1ST_INSTL_PEN_AMT_SB    1509-1
#define  LOFF_1_1ST_INSTL_PEN_CDE       1510-1
#define  LOFF_1_1ST_INSTL_REG_NO        1511-1
#define  LOFF_1_1ST_INSTL_COL_NO        1514-1
#define  LOFF_1_1ST_INSTL_SEQ_NO        1517-1
#define  LOFF_1_1ST_INSTL_COL_DATE      1521-1
#define  LOFF_1_CARRYOVER_1ST_PAID_FLAG 1529-1
#define  LOFF_1_BASE_APN_ROLLOVER_FLAG  1530-1
#define  LOFF_1_FILLER5                 1531-1
#define  LOFF_1_2ND_INSTL_DUE_DATE      1535-1
#define  LOFF_1_2ND_INSTL_TOTAL_TAX     1543-1
#define  LOFF_1_2ND_INSTL_TOTAL_TAX_SB  1554-1
#define  LOFF_1_2ND_INSTL_PEN_AMT       1555-1
#define  LOFF_1_2ND_INSTL_PEN_AMT_SB    1564-1
#define  LOFF_1_COST                    1565-1
#define  LOFF_1_COST_SB                 1572-1
#define  LOFF_1_2ND_INSTL_PEN_CDE       1573-1
#define  LOFF_1_2ND_INSTL_REG_NO        1574-1
#define  LOFF_1_2ND_INSTL_COL_NO        1577-1
#define  LOFF_1_2ND_INSTL_SEQ_NO        1580-1
#define  LOFF_1_2ND_INSTL_COL_DATE      1584-1
#define  LOFF_1_REFUND_AMOUNT           1592-1
#define  LOFF_1_REFUND_AMOUNT_SB        1603-1
#define  LOFF_1_REFUND_REF_NO           1604-1
#define  LOFF_1_MISC_COSTS              1609-1
#define  LOFF_1_MISC_COSTS_SB           1618-1
#define  LOFF_1_ROLL_CHANGE_INDEX       1619-1
#define  LOFF_1_ROLL_CHANGE_DATA_ARRAY  1621-1
#define  LOFF_1_TRAN_DATE_1ST           2053-1
#define  LOFF_1_TRAN_DATE_2ND           2061-1
#define  LOFF_1_FILLER7                 2069-1
#define  LOFF_1_AMT_PAID_1ST_INSTL      2080-1
#define  LOFF_1_AMT_PAID_1ST_INSTL_SB   2091-1
#define  LOFF_1_AMT_PAID_2ND_INSTL      2092-1
#define  LOFF_1_AMT_PAID_2ND_INSTL_SB   2103-1
#define  LOFF_1_1ST_INSTL_CANCL_DTE     2104-1
#define  LOFF_1_2ND_INSTL_CANCL_DTE     2112-1
#define  LOFF_1_APPORTION_YR            2120-1
#define  LOFF_1_FILLER8                 2124-1
#define  LOFF_1_VENDOR_NUMBER           2130-1
#define  LOFF_1_VENDOR_SUFFIX           2135-1
#define  LOFF_1_VEND_NAME               2136-1
#define  LOFF_1_VEND_STR_ADDR           2177-1
#define  LOFF_1_VEND_CITY_STATE         2207-1
#define  LOFF_1_VEND_CITY               2207-1
#define  LOFF_1_FILLER9                 2224-1
#define  LOFF_1_VEND_STATE              2225-1
#define  LOFF_1_FILLER10                2227-1
#define  LOFF_1_VEND_ZIP                2228-1
#define  LOFF_1_VEND_ZIP_4              2233-1
#define  LOFF_1_CLAIM_NOTICE_PR         2237-1
#define  LOFF_1_PAYMENT_VOUCHER         2238-1
#define  LOFF_1_LGR_RECORD_CODE         2248-1
#define  LOFF_1_REF_INT_DATE            2250-1
#define  LOFF_1_INT_ON_REF_AMT          2258-1
#define  LOFF_1_INT_ON_REF_AMT_SB       2269-1
#define  LOFF_1_FILLER11                2270-1
#define  LOFF_1_REFUND_WARRANT_NO       2273-1
#define  LOFF_1_REFUND_DATE             2280-1
#define  LOFF_1_SUPPLMENTAL_BILL_CNT    2288-1
#define  LOFF_1_TRANSFER_COMPLETION_DTE 2291-1
#define  LOFF_1_PRORATION_FACTOR        2299-1
#define  LOFF_1_PRORATION_FACTOR_SB     2305-1
#define  LOFF_1_PRO_PERCENT             2306-1
#define  LOFF_1_PRO_PERCENT_SB          2312-1
#define  LOFF_1_DAYS_OWNED              2313-1
#define  LOFF_1_OWNERSHIP_PERIOD        2316-1
#define  LOFF_1_NU_FILLER               2319-1
#define  LOFF_1_UNSEC_DELINQ_DATA       2322-1
#define  LOFF_1_FEE_ASSMT_X             2426-1
#define  LOFF_1_FEE_BOOK                2426-1
#define  LOFF_1_FEE_PAGE_BLK            2429-1
#define  LOFF_1_FEE_PARCEL              2432-1
#define  LOFF_1_FEE_PARCEL_SUB          2435-1
#define  LOFF_1_ORIG_ASSMT_X            2438-1
#define  LOFF_1_FILLER_NUMERIC          2450-1
#define  LOFF_1_ALT_SORT_KEY            2468-1
#define  LOFF_1_UFO_USES                2485-1
#define  LOFF_1_REFUND_AUTHORIZED_DATE  2493-1
#define  LOFF_1_ORIGINAL_DUE_DATE2      2501-1
#define  LOFF_1_COURT                   2509-1
#define  LOFF_1_BANKRUPTCY_NO           2513-1
#define  LOFF_1_BANKRUPTCY_DATE         2525-1
#define  LOFF_1_BANKRUPTCY_TYPE         2533-1
#define  LOFF_1_SELL_NOTICE_DATE        2535-1
#define  LOFF_1_SELL_DOC_NO             2543-1
#define  LOFF_1_FILLER16                2550-1
#define  LOFF_1_SELL_NOTICE_RECD_NO     2555-1
#define  LOFF_1_FILLER17                2562-1
#define  LOFF_1_RECISION_DATE           2567-1
#define  LOFF_1_RECISION_DOC_NO         2575-1
#define  LOFF_1_FILLER18                2582-1
#define  LOFF_1_RECISION_RECD_NO        2587-1
#define  LOFF_1_FILLER19                2594-1
#define  LOFF_1_XREF_APN_NO             2599-1
#define  LOFF_1_CURRENT_DEED_DATE       2611-1
#define  LOFF_1_CURRENT_DEED_NUM        2619-1
#define  LOFF_1_BANKRUPTCY_REMOVED_DATE 2631-1
#define  LOFF_1_ZIP_CODE_SORT           2639-1
#define  LOFF_1_TAX_BILL_PRINT_DATE     2649-1
#define  LOFF_1_APPORT_INS1_DATE        2657-1
#define  LOFF_1_APPORT_INS2_DATE        2665-1
#define  LOFF_1_FILLER20                2673-1
#define  LOFF_1_STRUCTURED_SITUS        2686-1
#define  LOFF_1_S_HSENUM                2686-1
#define  LOFF_1_S_STRDIR                2691-1
#define  LOFF_1_S_STRNAME               2693-1
#define  LOFF_1_S_STRTYPE               2717-1
#define  LOFF_1_S_UNITNO                2721-1
#define  LOFF_1_S_COMM                  2725-1
#define  LOFF_1_REFUND_DELINQ_TAX_FLAG  2730-1
#define  LOFF_1_FILLER21                2731-1

#define  LSIZ_1_APN                     12
#define  LSIZ_1_TAX_YEAR                4
#define  LSIZ_1_BOC_NO                  5
#define  LSIZ_1_ROLL_TYPE               1
#define  LSIZ_1_TAX_TYPE                2
#define  LSIZ_1_RECORD_STATUS_CD        1
#define  LSIZ_1_FILLER1                 1
#define  LSIZ_1_TAX_RATE_AREA           6
#define  LSIZ_1_CORTAC_NO               4
#define  LSIZ_1_CORTAC_CUST_NO          5
#define  LSIZ_1_CORTAC_LOAN_ID          25
#define  LSIZ_1_CURR_OWNER_NAME         41
#define  LSIZ_1_ALT_APN_KEY             19
#define  LSIZ_1_BOC_END_DATE            8
#define  LSIZ_1_BOC_VALUE               9
#define  LSIZ_1_BOC_VALUE_SB            1
#define  LSIZ_1_ASSESSEE_NAME           41
#define  LSIZ_1_MAIL_ADDRESS_1          41
#define  LSIZ_1_MAIL_ADDRESS_2          30
#define  LSIZ_1_MAIL_ADDRESS_3          30
#define  LSIZ_1_MAIL_ADDRESS_4          30
#define  LSIZ_1_FORMATTED_ADDR          1
#define  LSIZ_1_PROPDESC                64
//#define  LSIZ_1_SITUS_LN_1              32
//#define  LSIZ_1_SITUS_LN_2              32
#define  LSIZ_1_DATE_SOLD               8
#define  LSIZ_1_SOLD_TO_STATE_NO        6
#define  LSIZ_1_FILLER2                 6
#define  LSIZ_1_DATE_REDEEMED           8
#define  LSIZ_1_ACRES                   7
#define  LSIZ_1_ACRES_SB                1
#define  LSIZ_1_MARKET_LAND_VAL         9
#define  LSIZ_1_MARKET_LAND_VAL_SB      1
#define  LSIZ_1_FIXED_IMPR_VAL          9
#define  LSIZ_1_FIXED_IMPR_VAL_SB       1
#define  LSIZ_1_GROWING_IMPR_VAL        9
#define  LSIZ_1_GROWING_IMPR_VAL_SB     1
#define  LSIZ_1_STRUCT_IMPR_VAL         9
#define  LSIZ_1_STRUCT_IMPR_VAL_SB      1
#define  LSIZ_1_PERSON_PROP_VAL         9
#define  LSIZ_1_PERSON_PROP_VAL_SB      1
#define  LSIZ_1_BUSINESS_INV            9
#define  LSIZ_1_BUSINESS_INV_SB         1
#define  LSIZ_1_NET_VALUE               9
#define  LSIZ_1_NET_VALUE_SB            1
#define  LSIZ_1_10_PERCENT_PENALTY      1
#define  LSIZ_1_FILLER3                 5
#define  LSIZ_1_INTEREST_DATE           8
#define  LSIZ_1_AG_PRESERVE_FLAG        1
#define  LSIZ_1_USE_CODE                4
#define  LSIZ_1_BUSINESS_CODE           2
#define  LSIZ_1_REDEMPTION_CK_FLAG      1
#define  LSIZ_1_PRIOR_UNSEC_DISCHRGE    1
#define  LSIZ_1_OWNER_NAME_CHANGEFLG    1
#define  LSIZ_1_PROPERTY_EXMPT_IND      2
#define  LSIZ_1_EXMPT_CODE_1            1
#define  LSIZ_1_FILLER4_1               1
#define  LSIZ_1_PROP_EXMPT_AMT_1        9
#define  LSIZ_1_PROP_EXMPT_AMT_1_SB     1
#define  LSIZ_1_EXMPT_CODE_2            1
#define  LSIZ_1_FILLER4_2               1
#define  LSIZ_1_PROP_EXMPT_AMT_2        9
#define  LSIZ_1_PROP_EXMPT_AMT_2_SB     1
#define  LSIZ_1_EXMPT_CODE_3            1
#define  LSIZ_1_FILLER4_3               1
#define  LSIZ_1_PROP_EXMPT_AMT_3        9
#define  LSIZ_1_PROP_EXMPT_AMT_3_SB     1
#define  LSIZ_1_TAX_CODE_IND            2
#define  LSIZ_1_TAX_CODE_DATA_ARRAY     928
#define  LSIZ_1_1ST_INSTL_DUE_DATE      8
#define  LSIZ_1_1ST_INSTL_TOTAL_TAX     11
#define  LSIZ_1_1ST_INSTL_TOTAL_TAX_SB  1
#define  LSIZ_1_1ST_INSTL_PEN_AMT       9
#define  LSIZ_1_1ST_INSTL_PEN_AMT_SB    1
#define  LSIZ_1_1ST_INSTL_PEN_CDE       1
#define  LSIZ_1_1ST_INSTL_REG_NO        3
#define  LSIZ_1_1ST_INSTL_COL_NO        3
#define  LSIZ_1_1ST_INSTL_SEQ_NO        4
#define  LSIZ_1_1ST_INSTL_COL_DATE      8
#define  LSIZ_1_CARRYOVER_1ST_PAID_FLAG 1
#define  LSIZ_1_BASE_APN_ROLLOVER_FLAG  1
#define  LSIZ_1_FILLER5                 4
#define  LSIZ_1_2ND_INSTL_DUE_DATE      8
#define  LSIZ_1_2ND_INSTL_TOTAL_TAX     11
#define  LSIZ_1_2ND_INSTL_TOTAL_TAX_SB  1
#define  LSIZ_1_2ND_INSTL_PEN_AMT       9
#define  LSIZ_1_2ND_INSTL_PEN_AMT_SB    1
#define  LSIZ_1_COST                    7
#define  LSIZ_1_COST_SB                 1
#define  LSIZ_1_2ND_INSTL_PEN_CDE       1
#define  LSIZ_1_2ND_INSTL_REG_NO        3
#define  LSIZ_1_2ND_INSTL_COL_NO        3
#define  LSIZ_1_2ND_INSTL_SEQ_NO        4
#define  LSIZ_1_2ND_INSTL_COL_DATE      8
#define  LSIZ_1_REFUND_AMOUNT           11
#define  LSIZ_1_REFUND_AMOUNT_SB        1
#define  LSIZ_1_REFUND_REF_NO           5
#define  LSIZ_1_MISC_COSTS              9
#define  LSIZ_1_MISC_COSTS_SB           1
#define  LSIZ_1_ROLL_CHANGE_INDEX       2
#define  LSIZ_1_ROLL_CHANGE_DATA_ARRAY  432
#define  LSIZ_1_TRAN_DATE_1ST           8
#define  LSIZ_1_TRAN_DATE_2ND           8
#define  LSIZ_1_FILLER7                 11
#define  LSIZ_1_AMT_PAID_1ST_INSTL      11
#define  LSIZ_1_AMT_PAID_1ST_INSTL_SB   1
#define  LSIZ_1_AMT_PAID_2ND_INSTL      11
#define  LSIZ_1_AMT_PAID_2ND_INSTL_SB   1
#define  LSIZ_1_1ST_INSTL_CANCL_DTE     8
#define  LSIZ_1_2ND_INSTL_CANCL_DTE     8
#define  LSIZ_1_APPORTION_YR            4
#define  LSIZ_1_FILLER8                 6
#define  LSIZ_1_VENDOR_NUMBER           5
#define  LSIZ_1_VENDOR_SUFFIX           1
#define  LSIZ_1_VEND_NAME               41
#define  LSIZ_1_VEND_STR_ADDR           30
#define  LSIZ_1_VEND_CITY_STATE         30
#define  LSIZ_1_VEND_CITY               17
#define  LSIZ_1_FILLER9                 1
#define  LSIZ_1_VEND_STATE              2
#define  LSIZ_1_FILLER10                1
#define  LSIZ_1_VEND_ZIP                5
#define  LSIZ_1_VEND_ZIP_4              4
#define  LSIZ_1_CLAIM_NOTICE_PR         1
#define  LSIZ_1_PAYMENT_VOUCHER         10
#define  LSIZ_1_LGR_RECORD_CODE         2
#define  LSIZ_1_REF_INT_DATE            8
#define  LSIZ_1_INT_ON_REF_AMT          11
#define  LSIZ_1_INT_ON_REF_AMT_SB       1
#define  LSIZ_1_FILLER11                3
#define  LSIZ_1_REFUND_WARRANT_NO       7
#define  LSIZ_1_REFUND_DATE             8
#define  LSIZ_1_SUPPLMENTAL_BILL_CNT    3
#define  LSIZ_1_TRANSFER_COMPLETION_DTE 8
#define  LSIZ_1_PRORATION_FACTOR        6
#define  LSIZ_1_PRORATION_FACTOR_SB     1
#define  LSIZ_1_PRO_PERCENT             6
#define  LSIZ_1_PRO_PERCENT_SB          1
#define  LSIZ_1_DAYS_OWNED              3
#define  LSIZ_1_OWNERSHIP_PERIOD        3
#define  LSIZ_1_NU_FILLER               3
#define  LSIZ_1_UNSEC_DELINQ_DATA       104
#define  LSIZ_1_FEE_ASSMT_X             12
#define  LSIZ_1_FEE_BOOK                3
#define  LSIZ_1_FEE_PAGE_BLK            3
#define  LSIZ_1_FEE_PARCEL              3
#define  LSIZ_1_FEE_PARCEL_SUB          3
#define  LSIZ_1_ORIG_ASSMT_X            12
#define  LSIZ_1_FILLER_NUMERIC          18
#define  LSIZ_1_ALT_SORT_KEY            17
#define  LSIZ_1_UFO_USES                8
#define  LSIZ_1_REFUND_AUTHORIZED_DATE  8
#define  LSIZ_1_ORIGINAL_DUE_DATE2      8
#define  LSIZ_1_COURT                   4
#define  LSIZ_1_BANKRUPTCY_NO           12
#define  LSIZ_1_BANKRUPTCY_DATE         8
#define  LSIZ_1_BANKRUPTCY_TYPE         2
#define  LSIZ_1_SELL_NOTICE_DATE        8
#define  LSIZ_1_SELL_DOC_NO             7
#define  LSIZ_1_FILLER16                5
#define  LSIZ_1_SELL_NOTICE_RECD_NO     7
#define  LSIZ_1_FILLER17                5
#define  LSIZ_1_RECISION_DATE           8
#define  LSIZ_1_RECISION_DOC_NO         7
#define  LSIZ_1_FILLER18                5
#define  LSIZ_1_RECISION_RECD_NO        7
#define  LSIZ_1_FILLER19                5
#define  LSIZ_1_XREF_APN_NO             12
#define  LSIZ_1_CURRENT_DEED_DATE       8
#define  LSIZ_1_CURRENT_DEED_NUM        12
#define  LSIZ_1_BANKRUPTCY_REMOVED_DATE 8
#define  LSIZ_1_ZIP_CODE_SORT           10
#define  LSIZ_1_TAX_BILL_PRINT_DATE     8
#define  LSIZ_1_APPORT_INS1_DATE        8
#define  LSIZ_1_APPORT_INS2_DATE        8
#define  LSIZ_1_FILLER20                13
#define  LSIZ_1_S_HSENUM                5
#define  LSIZ_1_S_STRDIR                2
#define  LSIZ_1_S_STRNAME               24
#define  LSIZ_1_S_STRTYPE               4
#define  LSIZ_1_S_UNITNO                4
#define  LSIZ_1_S_COMM                  5
#define  LSIZ_1_REFUND_DELINQ_TAX_FLAG  1
#define  LSIZ_1_FILLER21                7

#define  AOFF_APN                   1-1
#define  AOFF_TAXYEAR               13-1
#define  AOFF_BOC_NO                17-1
#define  AOFF_ROLL_TYPE             22-1
#define  AOFF_TAX_TYPE              23-1
#define  AOFF_RECORD_STATUS         25-1
#define  AOFF_TRA                   27-1
#define  AOFF_CORTAC_NO             33-1
#define  AOFF_CORTAC_CUSTNO         37-1
#define  AOFF_LOAN_ID               42-1
#define  AOFF_CURR_OWNER            67-1
#define  AOFF_ALT_APN               108-1
#define  AOFF_BOC_ENDDATE           127-1
#define  AOFF_BOC_VALUE             135-1
#define  AOFF_ASSESSEE              144-1
#define  AOFF_MAILADDR1             185-1
#define  AOFF_MAILADDR2             226-1
#define  AOFF_MAILADDR3             256-1
#define  AOFF_MAILADDR4             286-1   // Used for UNFORMATTED addr
#define  AOFF_MAIL_FMT_FLG          316-1
#define  AOFF_SITUSADDR1            317-1
#define  AOFF_SITUSADDR2            349-1
#define  AOFF_SOLD_TO_ST            381-1
#define  AOFF_SOLD_TO_ST_NO         389-1
#define  AOFF_UNUSED0               395-1
#define  AOFF_DATE_REDEEMED_TO_ST   401-1
#define  AOFF_ACRES                 409-1
#define  AOFF_LAND                  416-1
#define  AOFF_FIXT_IMPR             425-1
#define  AOFF_GROWING_IMPR          434-1
#define  AOFF_STRUCTURE_IMPR        443-1
#define  AOFF_PERS_IMPR             452-1
#define  AOFF_BUSINESS_INV          461-1
#define  AOFF_NET                   470-1
#define  AOFF_PENALTY               479-1
#define  AOFF_UNUSED1               480-1
#define  AOFF_INT_DATE              485-1
#define  AOFF_AG_PRESERVE_CODE      493-1
#define  AOFF_USECODE               494-1
#define  AOFF_BUSCODE               498-1
#define  AOFF_REDEMPTION_CK_FLG     500-1
#define  AOFF_PRIOR_UNSEC_DISCHRG   501-1
#define  AOFF_NAMECHG_FLG           502-1
#define  AOFF_EXEMP_IND             503-1
#define  AOFF_EXEMP_CODE            508-1
#define  AOFF_EXEMP_AMT             510-1
#define  AOFF_UNUSED2               519-1
#define  AOFF_TAXCODE_IND           533-1
#define  AOFF_TAXCODE               538-1
#define  AOFF_TAXRATE               541-1   // 999V999999
#define  AOFF_TAXCODE_TYPE          550-1
#define  AOFF_TAX_1ST_AMT           551-1   // 999999999V99 (11)
#define  AOFF_TAX_1ST_PEN           562-1   // 9999999V99 (9)
#define  AOFF_TAX_2ND_AMT           571-1   // 999999999V99 (11)
#define  AOFF_TAX_2ND_PEN           582-1   // 9999999V99 (9)
#define  AOFF_FILLER1               591-1
#define  AOFF_TAX_1ST_DUE_DATE      1056-1
#define  AOFF_TAX_1ST_TOTAL         1064-1  // 999999999V99 (11)
#define  AOFF_TAX_1ST_PEN_AMT       1075-1  // 9999999V99 (9)
#define  AOFF_TAX_1ST_PEN_CODE      1084-1
#define  AOFF_TAX_1ST_COL_NO        1085-1
#define  AOFF_TAX_1ST_COL_DATE      1095-1
#define  AOFF_CO_1ST_PAID_FLG       1103-1
#define  AOFF_BASE_APN_ROLLOVER_FLG 1104-1
#define  AOFF_UNUSED3               1105-1
#define  AOFF_TAX_2ND_DUE_DATE      1107-1
#define  AOFF_TAX_2ND_TOTAL         1115-1  // 999999999V99 (11)
#define  AOFF_TAX_2ND_PEN_AMT       1126-1  // 9999999V99 (9)
#define  AOFF_TAX_COST              1135-1  // 99999V99 (7)
#define  AOFF_TAX_2ND_PEN_CODE      1142-1
#define  AOFF_TAX_2ND_COL_NO        1143-1
#define  AOFF_TAX_2ND_COL_DATE      1153-1
#define  AOFF_REFUND_AMT            1161-1  // 999999999V99 (11)
#define  AOFF_REFUND_REFNO          1172-1
#define  AOFF_MISC_COST             1177-1  // 9999999V99 (9)
#define  AOFF_ROLLCHG_INDEX         1186-1
#define  AOFF_ROLLCHG_DATA          1191-1
#define  AOFF_TRAN_DATE_1ST         1623-1
#define  AOFF_TRAN_DATE_2ND         1631-1
#define  AOFF_UNUSED4               1639-1
#define  AOFF_TAX_1ST_AMT_PAID      1642-1  // 999999999V99 (11)
#define  AOFF_TAX_2ND_AMT_PAID      1653-1  // 999999999V99 (11)
#define  AOFF_1ST_INSTL_CANCL_DATE  1664-1
#define  AOFF_2ND_INSTL_CANCL_DATE  1672-1
#define  AOFF_APPORTION_YR          1680-1
#define  AOFF_UNUSED5               1684-1
#define  AOFF_REFUND_VENDOR         1690-1
#define  AOFF_INT_ON_REF_AMT        1818-1  // 999999999V99 (11)
#define  AOFF_UNUSED6               1829-1
#define  AOFF_REFUND_WARRANT_NO     1832-1
#define  AOFF_REFUND_DATE           1839-1
#define  AOFF_SUPPL_BILL_CNT        1847-1
#define  AOFF_TRANSFER_COMPL_DATE   1850-1
#define  AOFF_PRORATION_FACTOR      1858-1  // 999V999 (6)
#define  AOFF_PRORATION_PCT         1864-1  // 999V999 (6)
#define  AOFF_DAYS_OWNED            1870-1
#define  AOFF_OWNERSHP_PERIOD       1873-1
#define  AOFF_UNUSED7               1876-1
#define  AOFF_UNSEC_DELQ_DATA       1879-1
#define  AOFF_FEE_ASMT_NO           1983-1
#define  AOFF_ORIG_ASMT_NO          1995-1
#define  AOFF_UNUSED8               2007-1
#define  AOFF_ALT_SORT_KEY          2025-1
#define  AOFF_UFO_USES              2042-1
#define  AOFF_REFUND_AUTH_DATE      2050-1
#define  AOFF_ORG_DUE_DATE          2058-1
#define  AOFF_BANKRUPTCY_DATA       2066-1
#define  AOFF_POWER_TO_SELL_DATA    2092-1
#define  AOFF_XREF_APN              2156-1
#define  AOFF_DEED_DATE             2168-1
#define  AOFF_DEED_NUMBER           2176-1
#define  AOFF_BANKRUPTCY_REM_DATE   2188-1
#define  AOFF_ZIP_SORT              2196-1
#define  AOFF_TAXBILL_PRINT_DATE    2206-1
#define  AOFF_APPORT_INS1_DATE      2214-1
#define  AOFF_APPORT_INS2_DATE      2222-1
#define  AOFF_UNUSED9               2230-1
#define  AOFF_SITUS_STRNUM          2243-1
#define  AOFF_SITUS_STRDIR          2248-1
#define  AOFF_SITUS_STRNAME         2250-1
#define  AOFF_SITUS_STRTYPE         2274-1
#define  AOFF_SITUS_UNIT            2278-1
#define  AOFF_SITUS_COMMUNITY       2282-1
#define  AOFF_SITUS_MULTI           2286-1
#define  AOFF_UNUSED10              2287-1

typedef struct _tTulLien
{  // 2297-bytes unpacked
   char  Apn[LSIZ_APN];
   char  TaxYear[LSIZ_TAXYEAR];
   char  BocNo[LSIZ_BOC_NO];
   char  RollType[LSIZ_ROLL_TYPE];
   char  TaxType[LSIZ_TAX_TYPE];
   char  Status[LSIZ_RECORD_STATUS];
   char  Tra[LSIZ_TRA];
   char  Cortac_No[LSIZ_CORTAC_NO];
   char  Cortac_CustNo[LSIZ_CORTAC_CUSTNO];
   char  Loan_ID[LSIZ_LOAN_ID];
   char  Curr_Owner[LSIZ_CURR_OWNER];
   char  Alt_Apn[LSIZ_ALT_APN];
   char  Boc_Enddate[LSIZ_BOC_ENDDATE];
   char  Boc_Value[ASIZ_BOC_VALUE];
   char  Assessee[LSIZ_ASSESSEE];
   char  MailAdr1[LSIZ_MAILADDR1];
   char  MailAdr2[LSIZ_MAILADDR2];
   char  MailAdr3[LSIZ_MAILADDR3];
   char  MailAdr4[LSIZ_MAILADDR4];
   char  MailFmt_Flg;
   char  SitusAdr1[LSIZ_SITUSADDR1];
   char  SitusAdr2[LSIZ_SITUSADDR2];
   char  Sold_To_St[LSIZ_SOLD_TO_ST];
   char  Sold_To_St_No[LSIZ_SOLD_TO_ST_NO];
   char  Unused0[LSIZ_UNUSED0];
   char  Redeem_Date[LSIZ_DATE_REDEEMED_TO_ST];
   char  Acres[ASIZ_ACRES];
   char  Land[RSIZ_VALUE_AMT];
   char  Fixture[RSIZ_VALUE_AMT];
   char  Growing[RSIZ_VALUE_AMT];
   char  Structure[RSIZ_VALUE_AMT];
   char  PersProp[RSIZ_VALUE_AMT];
   char  BusProp[RSIZ_VALUE_AMT];
   char  Net[RSIZ_VALUE_AMT];
   char  Penalty;
   char  Unused1[LSIZ_UNUSED1];
   char  Int_Date[LSIZ_INT_DATE];
   char  Ag_Preserve;
   char  UseCode[LSIZ_USECODE];
   char  BusCode[LSIZ_BUSCODE];
   char  Redemption_Chk_Flg;
   char  Prior_Unsec_Dischrg;
   char  NameChg_Flg;
   char  Exe_Ind[ASIZ_EXEMP_IND];
   char  Exe_Code[LSIZ_EXEMP_CODE];
   char  Exe_Amt[ASIZ_EXEMP_AMT];
   char  Unused2[LSIZ_UNUSED2];
   char  TaxCode_Ind[ASIZ_TAXCODE_IND];
   char  TaxCode[LSIZ_TAXCODE];
   char  TaxRate[ASIZ_TAXRATE];
   char  TaxCode_Type;
   char  Tax_1st_Amt[ASIZ_TAX_1ST_AMT];
   char  Tax_1st_Pen[ASIZ_TAX_1ST_PEN];
   char  Tax_2nd_Amt[ASIZ_TAX_1ST_AMT];
   char  Tax_2nd_Pen[ASIZ_TAX_1ST_PEN];
   char  filler1[LSIZ_FILLER1];
   char  Tax_1st_DueDate[LSIZ_TAX_1ST_DUE_DATE];
   char  Tax_1st_Total[ASIZ_TAX_1ST_TOTAL];
   char  Tax_1st_PenAmt[ASIZ_TAX_1ST_PEN_AMT];
   char  Tax_1st_PenCode;
   char  Tax_1st_ColNo[LSIZ_TAX_1ST_COL_NO];
   char  Tax_1st_ColDate[LSIZ_TAX_1ST_COL_DATE];
   char  CO_1st_Paid_Flg;
   char  Base_Apn_Rollover_Flg;
   char  Unused3[LSIZ_UNUSED3];
   char  Tax_2nd_DueDate[LSIZ_TAX_2ND_DUE_DATE];
   char  Tax_2nd_Total[ASIZ_TAX_2ND_TOTAL];
   char  Tax_2nd_PenAmt[ASIZ_TAX_2ND_PEN_AMT];
   char  Tax_Cost[ASIZ_TAX_COST];
   char  Tax_2nd_PenCode;
   char  Tax_2nd_ColNo[LSIZ_TAX_2ND_COL_NO];
   char  Tax_2nd_ColDate[LSIZ_TAX_2ND_COL_DATE];
   char  Refund_Amt[ASIZ_REFUND_AMT];
   char  Refund_RefNo[LSIZ_REFUND_REFNO];
   char  MiscCost[ASIZ_MISC_COST];
   char  RollChg_Index[ASIZ_ROLLCHG_INDEX];
   char  RollChg_Data[LSIZ_ROLLCHG_DATA];
   char  Tran_Date_1st[LSIZ_TRAN_DATE_1ST];
   char  Tran_Date_2nd[LSIZ_TRAN_DATE_2ND];
   char  Unused4[LSIZ_UNUSED4];
   char  Tax_1st_Amt_Paid[ASIZ_TAX_1ST_AMT_PAID];
   char  Tax_2nd_Amt_Paid[ASIZ_TAX_1ST_AMT_PAID];
   char  Tax_1st_Inst_Cancel_Date[LSIZ_1ST_INSTL_CANCL_DATE];
   char  Tax_2nd_Inst_Cancel_Date[LSIZ_1ST_INSTL_CANCL_DATE];
   char  Apportion_Year[LSIZ_APPORTION_YR];
   char  Unused5[LSIZ_UNUSED5];
   char  Refund_Vendor[LSIZ_REFUND_VENDOR];
   char  Int_On_Ref_Amt[ASIZ_INT_ON_REF_AMT];
   char  Unused6[LSIZ_UNUSED6];
   char  Refund_Warrant_No[LSIZ_REFUND_WARRANT_NO];
   char  Refund_Date[LSIZ_REFUND_DATE];
   char  Suppl_Bill_Count[LSIZ_SUPPL_BILL_CNT];
   char  Transfer_Compl_Date[LSIZ_TRANSFER_COMPL_DATE];
   char  Proration_Factor[LSIZ_PRORATION_FACTOR];
   char  Proration_Pct[LSIZ_PRORATION_PCT];
   char  Days_Owned[LSIZ_DAYS_OWNED];
   char  Ownership_Period[LSIZ_OWNERSHP_PERIOD];
   char  Unused7[LSIZ_UNUSED7];
   char  Unsec_Delq_Date[LSIZ_UNSEC_DELQ_DATA];
   char  Fee_Asmt_No[LSIZ_FEE_ASMT_NO];
   char  Orig_Asmt_No[LSIZ_ORIG_ASMT_NO];
   char  Unused8[LSIZ_UNUSED8];
   char  Alt_Sort_Key[LSIZ_ALT_SORT_KEY];
   char  UFO_Uses[LSIZ_UFO_USES];
   char  Refund_Auth_Date[LSIZ_REFUND_AUTH_DATE];
   char  Org_Due_Date[LSIZ_ORG_DUE_DATE];
   char  Bankrupcy_Data[LSIZ_BANKRUPTCY_DATA];
   char  Power_To_Sell_Data[LSIZ_POWER_TO_SELL_DATA];
   char  Xref_Apn[LSIZ_XREF_APN];
   char  DocDate[LSIZ_DEED_DATE];
   char  DocNum[LSIZ_DEED_NUMBER];
   char  Bankrupcy_Rem_Date[LSIZ_BANKRUPTCY_REM_DATE];
   char  ZipSort[LSIZ_ZIP_SORT];
   char  Taxbill_Print_Date[LSIZ_TAXBILL_PRINT_DATE];
   char  Apport_Ins1_Date[LSIZ_APPORT_INS1_DATE];
   char  Apport_Ins2_Date[LSIZ_APPORT_INS2_DATE];
   char  Unused9[LSIZ_UNUSED9];
   char  StrNum[LSIZ_SITUS_STRNUM];
   char  StrDir[LSIZ_SITUS_STRDIR];
   char  StrName[LSIZ_SITUS_STRNAME];
   char  StrType[LSIZ_SITUS_STRTYPE];
   char  Unit[LSIZ_SITUS_UNIT];
   char  City[LSIZ_SITUS_COMMUNITY];
   char  Multi;
   char  Unused10[LSIZ_UNUSED10];
   char  CrLf[2];
} TUL_LIEN;

IDX_TBL  Tul_HeatCool1[] =
{
   "DUO PAK                  ","WD",
   "HUM                      "," F",
   "DUO PACK                 ","WD",
   "DUOPAK                   ","WD",
   "A/C                      ","ZX",
   "FA/REF                   ","BA",
   "DUAL PAK                 ","WD",
   "DUO-PAK                  ","WD",
   "REF                      "," A",
   "FAU/AC                   "," X",
   "A/C & FAH                ","BX",
   "FAU & HUMID COOLER       ","BF",
   "DUO PAC                  ","WD",
   "DUOPAC                   ","WD",
   "ZONE UNIT                ","T ",
   "CENTRAL                  ","ZC",
   "FLOOR UNIT               ","C ",
   "",""
};

IDX_TBL  Tul_HeatCool2[] =
{
   "REF/ F/A                 ","BA",
   "FORCED                   ","B ",
   "FORCED/HUMID             ","BF",
   "WALL                     ","D ",
   "COOLER/WALL HEATER       ","DG",
   "COOLER                   "," G",
   "CENTRAL H & C            ","ZC",
   "COOLER/FAH               ","BG",
   "FORCED / HUMID           ","BF",
   "FLOOR                    ","C ",
   "",""
};

IDX_TBL  Tul_HeatCool[] =
{
	"   FA/AC                 ","B ",
	"   WALLHT/EVAP           ","DE",
	"  WALL HT. EVAP DUO PAC  ","DE",
	" (3) DUO-PAKS            ","WD",
	" 2 DUO-PAK UNITS         ","WD",
	" 2 DUOPACS               ","WD",
	" 3 1/2 TON               "," A",
	" 3 DUO PAK               ","WD",
	" 4TON                    "," A",
	" A/C                     ","ZX",
	" CENTRAL                 ","ZC",
	" COOLER/FLOOR HEATER     ","CG",
	" COOLING/ FLOOR UNIT     ","CG",
	" DOU PAK                 ","WD",
	" DP                      ","WD",
	" DUAK PAK                ","WD",
	" DUAL  AK                ","WD",
	" DUAL PACK               ","WD",
	" DUAL PAK                ","WD",
	" DUAL PAKS               ","WD",
	" DUO PAC                 ","WD",
	" DUO PACK                ","WD",
	" DUO PAK                 ","WD",
	" DUO PAK (2)             ","WD",
	" DUO PAK @ OFFICE        ","WD",
	" DUO PAKS                ","WD",
	" DUO-PAK                 ","WD",
	" DUOPAC                  ","WD",
	" DUOPAK                  ","WD",
	" DUPAC                   ","WD",
	" EST 3 TON               "," A",
	" EVAP                    "," E",
	" EVAP/HEATER             ","YE",
	" F/A HEAT & EVAP         ","BE",
	" F/A HEAT & PUMP         ","BH",
	" FA & REF                ","BA",
	" FA / EVAP               ","BE",
	" FA / REF                ","BA",
	" FA /AC                  ","BX",
	" FA /EVAP                ","BE",
	" FA /REF                 ","BA",
	" FA HEAT  A/C COOL       ","BG",
	" FA HEAT  EVAP COOL      ","BE",
	" FA HEAT & COOL          ","BG",
	" FA HEAT A/C COOL        ","BG",
	" FA HEAT/COOL            ","BG",
	" FA HT/AC COOL           ","BG",
	" FA REF                  ","BA",
	" FA/ REF                 ","BA",
	" FA/HUMID                ","BF",
	" FA/REF                  ","BA",
	" FA/RET                  ","BA",
	" FAU &                   ","B ",
	" FAU & HUMID COOL        ","BF",
	" FAU & HUMID COOLER      ","BF",
	" FAU & REFRIG.           ","BA",
	" FAU/2 TON               ","BA",
	" FAU/AC                  ","BX",
	" FFA HEAT & COOL         ","BG",
	" FLOOR                   ","C ",
	" FLOOR FURNANCE          ","C ",
	" FLOOR HEAT EVAP COOL    ","CE",
	" FLOOR HEATER            ","C ",
	" FLOOR HT/EVAP           ","CE",
	" FLRHT/EVAP COOL         ","CE",
	" FORC'D/COOLER           ","BG",
	" FORCED / HUMID          ","BF",
	" FORCED / REF            ","BA",
	" FORCED HEAT A/C COOL    ","BG",
	" FORCED HEAT EVAP COOL   ","BE",
	" FORCED REF              ","B ",
	" FORCED/ COOLER          ","BG",
	" FORCED/ HUMID           ","BF",
	" FORCED/HUMID            ","BF",
	" FRC HEAT/REF            ","BA",
	" FRCD HEAT/HUM           ","BF",
	" FRCD HEAT/HUMID         ","BF",
	" FRCD HEAT/REF           ","BA",
	" FRCD HEAT/WINDOW REF    ","BA",
	" FRCD/REF                ","BA",
	" FRCED/ HUMID            ","BF",
	" GRAVITY                 ","A ",
	" HEAT PUMP               ","  ",
	" HEATING/FLOOR UNIT      ","C ",
	" HEATING/WALL UNIT       ","D ",
	" HUM                     ","D ",
	" HUMID, WALL UNIT        ","D ",
	" PERIMETER - CENTRAL     ","  ",
	" REF                     "," A",
	" REF/FORCD               ","BA",
	" REF/GRAVITY HEATER      ","AA",
	" STOVE                   ","S ",
	" W F EVAP                ","DE",
	" WALL HEAT & (EST.) EVAP ","DE",
	" WALL HEAT / EVAP        ","DE",
	" WALL HEAT EVAP COOL     ","DE",
	" WALL HEAT-HUMID COOL    ","DF",
	" WALL HEAT/HUMID         ","DF",
	" WALL HEATER/ COOLERS    ","DE",
	" WALL HT/EVAP            ","DE",
	" WALL UNIT               ","D ",
	" WALL UNIT, HUMID        ","DF",
	" WALL UNIT/ HUMID        ","DF",
	" WALL UNIT/HUMID         ","DF",
	" WALL UNIT/ZONE UNIT     ","DT",
	" WALL/ HUMID             ","DF",
	" WALLHT/AC               ","DX",
	" WOOD STOVE/HUMID        ","SF",
	" ZONE / HUMID            ","TF",
	" ZONE AND SMALL DP       ","T ",
	" ZONE UNIT               ","T ",
	"PACKAGE UNIT FIRST FLR   ","  ",
	"#1FAH/EVAP,#2ZONE,#3UKN  ","BE",
	"(1) DUO PAK (1) WALL/EVAP","WD",
	"(1) DUO PAK (2) EVAP     ","WD",
	"(1)5TON & (2)2TON        ","A ",
	"(2)                      ","D ",
	"(2) A/C'S; FAU           ","ZX",
	"(2) DUAL-PAKS            ","WD",
	"(2) DUO PAKS             ","WD",
	"(2) DUO PAKS (1) COOLER  ","WD",
	"(2) DUO-PAK'S            ","WD",
	"(2) DUO-PAKS             ","WD",
	"(2) DUOPAKS              ","WD",
	"(2) FAU/AC               ","BX",
	"(2) FAU/AC UNITS         ","BX",
	"(2) HUM                  ","  ",
	"(2) WALL HEATER/COOLER   ","DG",
	"(2) WALL HEATERS         ","D ",
	"(2) WALL HT/ EVAP        ","DE",
	"(2) WALL/EVAP            ","DE",
	"(2)DUO PAK               ","WD",
	"(2)DUOPAKS, (1)WALL/EVAP ","WD",
	"(2)FAU/(2)AC             ","BX",
	"(2)FAU/AC                ","BX",
	"(2)FLOOR UNIT-HUMID      ","BF",
	"(2)WALL                  ","D ",
	"(2)WALL UNITS,(2)C-EVAPS ","DE",
	"(2)WALL/(2)EVAP'S        ","DE",
	"(2)WALL/EVAP             ","DE",
	"(3) DUO PAK              ","WD",
	"(3) DUO PAKS             ","WD",
	"(3) DUO-PAKS             ","WD",
	"(4) FAU/AC'S             "," X",
	"(4)DUO PAKS              ","WD",
	"(EST) FLOOR/EVAP         ","CE",
	"(EST.) DUO-PAK           ","WD",
	"(EST.) WALL HT. + EVAP   ","DE",
	"/                        ","  ",
	"/C                       "," X",
	"/C & FAH                 ","BX",
	"0                        ","  ",
	"00                       ","  ",
	"0UOPAC                   ","WD",
	"1 DUO PAC EACH UNIT      ","WD",
	"1 FA/REF, 2 DUOPAKS      ","WD",
	"1 HUM 5 WIN A/C          "," W",
	"1 WALL HEATER (BATH)     ","D ",
	"1 WALL HT & 1 EACH UNIT  ","DE",
	"1 WALL HT. 1 EVAP EA.UNIT","DE",
	"1 WALL HT.1 EVAP PER UNIT","DE",
	"1-DUO PAK 2-WALLHT/EVAP  ","WD",
	"1-DUOPAK 1-WALL/EVAP     ","WE",
	"1-EVP-FLR 2 CNTRL 3 CTR93","DE",
	"10 DUO PAKS              ","WD",
	"10 DUO-PAK UNITS         ","WD",
	"10 DUO-PAKS              ","WD",
	"11 DUO-PAKS              ","WD",
	"12 DUO-PAK UNITS         ","WD",
	"12 DUO-PAKS              ","WD",
	"13 DUO-PAK UNITS         ","WD",
	"15 EVAP; A/C IN OFFICE   "," O",
	"16 DUO-PAK, 4 EVAP       ","WD",
	"175                      ","  ",
	"18 DUO PAKS              ","WD",
	"2 (WALL HEATER)/COOLER   ","DG",
	"2 - DPS                  ","  ",
	"2 - DUO PAKS             ","WD",
	"2 A/C UNITS              "," X",
	"2 A/C UNITS - 2 EVAP     "," E",
	"2 A/C'A; FAU             "," X",
	"2 A/C'S                  "," X",
	"2 A/C'S FAU              "," X",
	"2 A/C'S, FAU             "," X",
	"2 A/C'S-FAU              "," X",
	"2 COOLERS                "," G",
	"2 DP                     ","WD",
	"2 DU0 PAKS               ","WD",
	"2 DUAL PAKS              ","WD",
	"2 DUAL-PAK'S             ","WD",
	"2 DUO PAC'S              ","WD",
	"2 DUO PACK UNITS         ","WD",
	"2 DUO PACKS              ","WD",
	"2 DUO PACS               ","WD",
	"2 DUO PAK                ","WD",
	"2 DUO PAKS               ","WD",
	"2 DUO PAKS/EVAP          ","WD",
	"2 DUO-PAC,6 REZNOR,6 EVAP","WD",
	"2 DUO-PAK                ","WD",
	"2 DUO-PAK UNITS          ","WD",
	"2 DUO-PAK UNITS, 2 EVAP  ","WE",
	"2 DUO-PAK'S              ","WD",
	"2 DUO-PAKS               ","WD",
	"2 DUO-PAKS & 1 EVAP      ","WD",
	"2 DUO-PAKS + 3 EVAP      ","WD",
	"2 DUOPACS                ","WD",
	"2 DUOPACS (2.5 TON EACH) ","WD",
	"2 DUOPAK'S               ","WD",
	"2 DUOPAKS                ","WD",
	"2 F.P. FOR HEAT/EVAP     ","FE",
	"2 F/A REF                "," A",
	"2 F/A UNITS              ","B ",
	"2 FA/REF'S               "," A",
	"2 FAU/AC'S               "," X",
	"2 FLOOR COOLER           "," G",
	"2 FLOOR HTRS. + EVAP     ","CE",
	"2 FLOOR UNITS            ","BE",
	"2 FORC'D & REF'S         ","BA",
	"2 H&C; 1 F/A HEAT + EVAP ","BE",
	"2 HEAT PUMP UNITS        ","GH",
	"2 HEAT PUMPS             ","GH",
	"2 HUM                    "," F",
	"2 HUMID BASE             "," F",
	"2 HUMID WALL UNITS       ","DF",
	"2 HUMS                   "," F",
	"2 MASTER COOL UNITS      "," G",
	"2 REF & 1 EVAP           "," A",
	"2 REZ. HEATERS           ","B ",
	"2 ROOF COOLERS           "," R",
	"2 TON                    "," X",
	"2 TON AIR                "," X",
	"2 TON REF                "," A",
	"2 TON REF UNIT           "," A",
	"2 TONS                   ","  ",
	"2 UNITS                  ","  ",
	"2 WALL                   ","  ",
	"2 WALL COOLER            ","DL",
	"2 WALL HEAT / COOLER     ","DL",
	"2 WALL HEAT 2 EVAP COOL  ","DL",
	"2 WALL HEAT EVAP COOL    ","DL",
	"2 WALL HEATER/COOLER     ","DL",
	"2 WALL HEATERS           ","D ",
	"2 WALL HEATERS EVAP COOL ","DL",
	"2 WALL HEATERS/COOLER    ","DL",
	"2 WALL HT+EVAP 1 DUO PAC ","WD",
	"2 WALL HT. 2 EVAP COOL   ","DL",
	"2 WALL HTRS              ","D ",
	"2 WALL UNIT              ","  ",
	"2 WALL UNIT / HUMID      ","DF",
	"2 WALL UNIT-COOLER       ","DL",
	"2 WALL UNIT/180          ","D ",
	"2 WALL UNIT/HUMID        ","DF",
	"2 WALL UNITS             ","D ",
	"2 WALL UNITS/2 COOLERS   ","DL",
	"2 WALL UNITS/HUM         ","DF",
	"2 WALL-HUM               ","DF",
	"2 WALL-HUMID             ","DF",
	"2 WALL/ 2 HUMID          ","DF",
	"2 WALL/ REF 2 ONE TON    ","DA",
	"2 WALL/EVAP              ","DE",
	"2 WALL/HUMID             ","DF",
	"2 WALL/REF               ","DA",
	"2 WALL/WALL AC & EVAP    ","DX",
	"2 WALLHT/2 AC UNITS      ","DX",
	"2 WINDOW A/C             "," W",
	"2 WOODSTOVES             ","S ",
	"2- DPS                   ","WD",
	"2- DUO PAKS              ","WD",
	"2-3 TON A/C              "," X",
	"2-DPS.                   ","WD",
	"2-DUA-PAK                ","WD",
	"2-DUO PAK'S              ","WD",
	"2-DUO PAKS,  8 EVAPS     ","WD",
	"2-DUO-PAK                ","WD",
	"2-DUO-PAKS               ","WD",
	"2-EVAPS, 1-200000 BTU HTR","XE",
	"2.5 TON                  "," X",
	"2.5 TON H&C              ","ZD",
	"20 DUO-PAK UNITS         ","WD",
	"24 DUO-PAK UNITS         ","WD",
	"2DUAL PACK               ","WD",
	"2DUO-PAKS,REZNOR & EVAP  ","WE",
	"2DUOPACS                 ","WD",
	"2EA. FORCED HEAT A/C COOL","ZX",
	"2FRCD/AC                 ","BX",
	"2T REF/                  "," A",
	"2TON                     "," X",
	"2TON SPLIT UNIT          "," S",
	"2WALL - EVAP             ","DE",
	"2WALL HT/EVAP COOL       ","DE",
	"2WALL HTRS/EVP COOLER    ","DE",
	"2WALL HTS/EVAP COOL      ","DE",
	"2WALL UNITS-2COOLERS     ","DE",
	"2WALLHT/EVAP COOLER      ","DE",
	"2WALLS                   "," D",
	"2WALLUNITS               "," D",
	"3 1/2 TON                "," X",
	"3 DUO PAC'S              ","WD",
	"3 DUO PACS               ","WD",
	"3 DUO PAK                ","WD",
	"3 DUO PAKS               ","WD",
	"3 DUO-PAK UNITS          ","WD",
	"3 DUO-PAK UNITS IN OFFICE","WD",
	"3 DUO-PAKS & 101 HT.PUMPS","WD",
	"3 DUO-PAKS,3 RADIANT HEAT","WD",
	"3 DUOPACS                ","WD",
	"3 DUOPAKS                ","WD",
	"3 EVAP & OFFICE H&C      ","YE",
	"3 FOR'D, 3 REF.          ","BA",
	"3 HEAT PUMP UNITS        ","GH",
	"3 ROOF EVAP & CEILING HT ","XE",
	"3 SPLIT UNITS            ","XS",
	"3 T HEAT PUMP            ","GH",
	"3 TON                    "," A",
	"3 TON - FAH/CENTRAL      ","ZC",
	"3 TON A/C                ","ZA",
	"3 TON A/C UNIT           ","ZA",
	"3 TON AC                 ","ZA",
	"3 TON AC / PERIMETER HEAT","XA",
	"3 TON AIR                ","BA",
	"3 TON AIR COND           ","BA",
	"3 TON H&C                ","BA",
	"3 TON H-C                ","BA",
	"3 TON H.P                ","BA",
	"3 TON HP                 ","GA",
	"3 TON REF                "," A",
	"3 TON REFRIG             "," A",
	"3 TON REFRIG/FORCED HEAT "," A",
	"3 TON ROOF               "," A",
	"3 TON-WALL               "," A",
	"3 UNITS                  ","U ",
	"3 WALL/EVAP              ","DE",
	"3-HVAC                   ","33",
	"3.5 TON A/C / FORCED     ","BA",
	"30 EVAP & 9 REZNOR UNITS ","WE",
	"3T                       "," X",
	"3T / COOLER              "," G",
	"3T CARRIER               "," A",
	"3T REF                   "," A",
	"3T WALL UNIT             ","DL",
	"3TON                     "," X",
	"3TON DUAL PAK            ","WD",
	"3TON H&C                 ","BA",
	"3TON REF                 "," A",
	"3TON REF/H&C             ","BA",
	"3TON REFR                ","BA",
	"4 DUO PAC'S              ","WD",
	"4 DUO PAK                ","WD",
	"4 DUO PAKS               ","WD",
	"4 DUO-PAK UNITS          ","WD",
	"4 DUO-PAK, 6 EVAP        ","WD",
	"4 DUO-PAKS               ","WD",
	"4 DUO-PAKS; WALL/EVAP    ","WD",
	"4 DUOPACS                ","WD",
	"4 FAU'S/4 A/C'S          ","BX",
	"4 FRCD/REF UNITS         ","B ",
	"4 HEAT & A/C COOL UNITS  ","XX",
	"4 HEATERS                ","X ",
	"4 REF-FORCED             ","BA",
	"4 TON                    "," X",
	"4 TON A/C                ","ZA",
	"4 TON AC                 ","ZA",
	"4 TON DUO PAK            ","WD",
	"4 TON DUO-PAK            ","WD",
	"4 TON HEAT & COOL        ","BD",
	"4 TON HEAT PUMP          ","GH",
	"4 UNITS                  ","B   ",
	"4 WALL HEAT'S 4 EVAPS    ","DE",
	"4 WALL HEATERS           ","D ",
	"4 WINDOW A/C             ","WX",
	"4 X DUO-PAKS, 6 X EVAP   ","WD",
	"4 ZONE UNITS             "," T",
	"4REF.,                   "," A",
	"4TON AC;F/A HEAT         ","BA",
	"5 DUO PAK UNITS          ","WD",
	"5 DUO-PAK UNITS          ","WD",
	"5 DUO-PAKS               ","WD",
	"5 EVAP + A/C IN OFFICE   "," E",
	"5 FLOOR UNITS/HUMID      ","C ",
	"5 TON                    "," X",
	"5 TON A/C                ","ZA",
	"50                       ","  ",
	"5T OFFICE H&C            ","XO",
	"5TON                     "," X",
	"5TON-2TON                ","ZA",
	"6 DUO-PAK UNITS          ","WD",
	"6 DUO-PAKS               ","WD",
	"6 WALL HT.,6 WINDOW A/C  ","DW",
	"7 DUO PAKS               ","WD",
	"7 DUO-PAK UNITS          ","WD",
	"7 DUO-PAKS               ","WD",
	"7 DUO-PAKSF              ","WD",
	"7DUO-PAK;8EVAP;6SUSP.HEAT","WD",
	"8 DUO PAC                ","WD",
	"8 DUO-PAK UNITS          ","WD",
	"8 DUO-PAKS               ","WD",
	"8 HUMS                   ","  ",
	"8 TON-FORCED             ","B ",
	"80 WINDOW UNITS+ 2 X 5T  "," X",
	"85 TON AC                ","BA",
	"9                        ","  ",
	"9 CENTRAL H/C UNITS      "," C",
	"9 DUO-PAK UNITS          ","WD",
	"9 HEAT PUMPS             ","  ",
	"924D50                   ","  ",
	"941D65                   ","  ",
	"979D80                   ","  ",
	"980D65                   ","  ",
	"987                      ","  ",
	"990D65                   ","  ",
	"998D70                   ","  ",
	"?                        ","  ",
	"A HEAT/COOL              ","XX",
	"A.C & FAH                ","BX",
	"A/C                      ","ZX",
	"A/C   HEAT PUMP          ","GH",
	"A/C  & FAH               ","BX",
	"A/C  /  FORCED           ","BX",
	"A/C  FAH                 ","BX",
	"A/C & AFH                ","BX",
	"A/C & EVAP.COOLER        ","ZE",
	"A/C & FA                 ","BX",
	"A/C & FAH                ","BX",
	"A/C & FAK                ","BX",
	"A/C & FAN                ","ZX",
	"A/C & FAS, COOLERS       ","BF",
	"A/C & FLOOR HEATER       ","CX",
	"A/C & FORCED             ","BX",
	"A/C & FORCED AIR HEAT    ","BX",
	"A/C & HUMID COOL         ","ZF",
	"A/C & HUMID COOLER       ","ZF",
	"A/C & HUMID COOLERS      ","ZF",
	"A/C & REF                ","ZA",
	"A/C & WALL               ","DX",
	"A/C &FAH                 ","BX",
	"A/C (2)                  ","ZX",
	"A/C (4)                  ","ZX",
	"A/C + EVAP               ","ZE",
	"A/C - 3                  ","ZX",
	"A/C - 4                  ","ZX",
	"A/C - COOLER             ","ZG",
	"A/C - FA HEAT            ","BX",
	"A/C - FAH                ","BX",
	"A/C - FORCED AIR         ","BX",
	"A/C - HUM - FORCED       ","ZF",
	"A/C - HUMID              ","ZF",
	"A/C / 1 PER UNIT         ","ZX",
	"A/C / FAH                ","BX",
	"A/C / WALL HEAT          ","DX",
	"A/C /FORCED              ","BX",
	"A/C 3 1/2 TON            ","ZA",
	"A/C 3 TON                ","ZA",
	"A/C 4 TON                ","ZA",
	"A/C 4TON                 ","ZA",
	"A/C 7 FAH                ","BX",
	"A/C @ OFFICES            ","ZA",
	"A/C AND EVAP COOLER      ","ZE",
	"A/C FAH                  ","BX",
	"A/C FAU                  ","BX",
	"A/C FLOOR UNIT           ","ZC",
	"A/C IN EACH RM.          ","ZO",
	"A/C IN MAIN OFFICE       ","ZO",
	"A/C IN OFFICE            ","ZO",
	"A/C IN OFFICE AREA       ","ZO",
	"A/C IN OFFICE AREAS      ","ZO",
	"A/C IN OFFICE ONLY       ","ZO",
	"A/C IN OFFICE; REZ + EVAP","BE",
	"A/C IN OFFICE;EVAP & REZ ","BE",
	"A/C IN OFFICES           ","ZO",
	"A/C IN OFFICES; EVAP/RAD.","IE",
	"A/C IN OFFICES; REZ HEAT ","BO",
	"A/C IN OFFICES; REZ. HEAT","BO",
	"A/C IN RES               ","BO",
	"A/C IN STORE AREA        ","ZX",
	"A/C OFFICE               ","ZO",
	"A/C OFFICE;HUMID SHOP    ","ZO",
	"A/C ON ROOF              ","ZR",
	"A/C ROOF                 ","ZA",
	"A/C UNIT                 ","ZA",
	"A/C WALL UNIT            ","ZA",
	"A/C& F/A                 ","BX",
	"A/C& FAH                 ","BX",
	"A/C& FLOOR FURNACE       ","CX",
	"A/C&FAH                  ","BX",
	"A/C&FLOOR FURNACE        ","CX",
	"A/C(OFFICE);EVAP&SUSPHEAT","JE",
	"A/C, EVAP, & WALL HEAT   ","DE",
	"A/C, FORCED AIR          ","BX",
	"A/C, RF                  ","ZR",
	"A/C, WALL HEAT           ","ZA",
	"A/C-FLOOR FURNACE        ","CX",
	"A/C-FORCED               ","ZB",
	"A/C-OFFICES COOLERS-SHOP ","ZX",
	"A/C/ FORCED AIR          ","BX",
	"A/C/FAH                  ","BX",
	"A/C7FAH                  ","BX",
	"A/C; EVAP IN W'HSE.AREA  ","ZE",
	"A/C; FAN                 ","ZX",
	"A/REF                    ","ZA",
	"A?C                      ","ZX",
	"AA/C                     ","ZX",
	"AA/C & FAH               ","ZX",
	"AARCOLA/FLOOR            ","C ",
	"AC                       ","ZX",
	"AC & FAH                 ","BX",
	"AC & FLOOR HEAT          ","CX",
	"AC & FLOOR HEATER        ","CX",
	"AC / FA                  ","BX",
	"AC / FAH                 ","BX",
	"AC FAH                   ","BX",
	"AC IN OFFICE             "," O",
	"AC IN OFFICE AREAS       "," O",
	"AC IN OFFICES; REZ HEAT  "," O",
	"AC UNIT                  ","ZA",
	"AC/ FAH                  ","BX",
	"AC/EVAP                  "," E",
	"AC/FA                    ","BX",
	"AC/FAH                   ","BX",
	"AC/FAU                   ","BX",
	"AC/FLOOR FURN            ","CX",
	"AC/FLOOR UNIT            ","CX",
	"AC/FORCD                 ","BX",
	"AF/AC                    "," 4",
	"AIR COND                 "," X",
	"AIR COOLING HEAT WALL UNT","DG",
	"AIR DUAL PAK             ","BD",
	"AIR ROOF HEAT-FLOOR UNIT ","CR",
	"ALL HEAT                 ","D ",
	"ALL HEAT-HUMID COOL      ","DF",
	"ALL HEAT-HUMID COOLER    ","DF",
	"ALL HEAT/HUM             ","DF",
	"ALL HEATER-HUMID COOLER  ","DF",
	"ALL UNIT                 ","DL",
	"ALL; EVAP                ","DE",
	"ARCOLA                   ","  ",
	"AU & EVAP COOLER         ","BE",
	"AU & HUMID COOLE         ","BF",
	"AU & HUMID COOLER        ","BF",
	"AU & NHUMID COOLER       ","BF",
	"AU & REFRIG              ","BA",
	"AU/AC                    ","ZC",
	"AU/CENT                  ","ZC",
	"A\\C                     ","ZX",
	"BARN                     ","  ",
	"BASE                     ","FX",
	"BASE / HUMID             "," F",
	"BASE BOARD HEAT EVAP COOL","FE",
	"BASE ELECTRIC            ","  ",
	"BASE HEAT / WINDOW AIR   ","FW",
	"BASE/EVAP                ","FE",
	"BASE/REF                 ","FA",
	"BASEBD HT/ AC            ","FX",
	"BASEBOARD                ","FX",
	"BASEBOARD / REFRIGERATION","FA",
	"BASEBOARD HEAT           ","F ",
	"BASEBOARD HEAT  AC COOL  ","FX",
	"BASEBOARD HEAT / COOLER  ","FG",
	"BASEBOARD HEAT EVAP COOL ","FE",
	"BASEBOARD HEAT-WINDOW A/C","FW",
	"BASEBOARD HEATING        ","F ",
	"BASEBOARD HT/EVAP COOL   ","FE",
	"BASEBOARD, AC            ","FX",
	"BASEBOARD/EVAP           ","FE",
	"BUTANE / HUMID           ","X ",
	"BUTANE HEAT & EVAP COOL  ","X ",
	"C-EVAP, FAU,2 TON A/C    ","BE",
	"CENRAL COOL/EVAP,ZONE HT ","ZE",
	"CENTERAL                 ","ZC",
	"CENTRA H/C               ","ZC",
	"CENTRAL                  ","ZC",
	"CENTRAL & TON            ","ZC",
	"CENTRAL (2)              ","ZC",
	"CENTRAL - FORCED         ","ZC",
	"CENTRAL - HUM            ","ZF",
	"CENTRAL - REF - FORCED   ","ZA",
	"CENTRAL / 1 PER UNIT     ","ZC",
	"CENTRAL / 1 TON REF      ","ZA",
	"CENTRAL / FORCED         ","ZC",
	"CENTRAL / HUM            ","ZF",
	"CENTRAL / YORK 2 TON     ","ZC",
	"CENTRAL 3                ","ZC",
	"CENTRAL 4TON             ","ZC",
	"CENTRAL 5 TON            ","ZC",
	"CENTRAL A/ C & FAH.      ","ZC",
	"CENTRAL A/C              ","ZC",
	"CENTRAL A/C & FAH        ","ZC",
	"CENTRAL A/C & FAH HEATING","ZC",
	"CENTRAL AC               ","ZC",
	"CENTRAL AC/FAH           ","BC",
	"CENTRAL AIR              ","ZC",
	"CENTRAL AIR / WALL UNIT  ","DZ",
	"CENTRAL EVAP             ","ZE",
	"CENTRAL EVAP, ZONE       ","ZE",
	"CENTRAL EVAP/WALL HEATER ","ZE",
	"CENTRAL FAU A/C          ","ZC",
	"CENTRAL FORCED           ","ZC",
	"CENTRAL H & C            ","ZC",
	"CENTRAL H & C IN OFFICE  ","ZO",
	"CENTRAL H & C/DUOPAC     ","WD",
	"CENTRAL H 7 C            ","ZG",
	"CENTRAL H& C             ","ZC",
	"CENTRAL H&C              ","ZC",
	"CENTRAL H&C & EVAP       ","ZE",
	"CENTRAL H&C (BOTH BLDGS) ","ZC",
	"CENTRAL H&C (OFFICE)     ","ZC",
	"CENTRAL H&C (RESTAURANT) ","ZC",
	"CENTRAL H&C IN MORTUARY  ","ZC",
	"CENTRAL H&C IN OFFICE    ","ZO",
	"CENTRAL H&C IN SHOWROOM  ","ZC",
	"CENTRAL H&C OFFICE/STORE ","ZC",
	"CENTRAL H&C REC BLDG.    ","ZC",
	"CENTRAL H&C UNITS        ","ZC",
	"CENTRAL H&C-RESIDENCE    ","ZC",
	"CENTRAL HC 2 UNITS       ","ZC",
	"CENTRAL HEAT             ","Z ",
	"CENTRAL HEAT & AIR       ","ZC",
	"CENTRAL HEAT & COOL      ","ZG",
   "CENTRAL HEAT & REFRIG    ","ZC",
	"CENTRAL HEAT/AIR         ","ZC",
	"CENTRAL HEAT/COOLING     ","ZC",
	"CENTRAL HEAT/EVAP        ","ZE",
	"CENTRAL HVAC             ","Z ",
	"CENTRAL LAND FAU         ","Z ",
	"CENTRAL REF & HEAT UNIT  ","ZA",
	"CENTRAL UNIT             ","Z ",
	"CENTRAL, 3TON HP         ","ZH",
	"CENTRAL-5 TON            ","ZC",
	"CENTRAL/ ROOF EVAP       ","ZE",
	"CENTRAL/AC               ","ZC",
	"CENTRAL/DUAL             ","ZC",
	"CENTRAL/EVAP & REF       ","ZE",
	"CENTRAL/HUM              ","ZF",
	"CENTRAL/REF/WALL UNIT    ","ZA",
	"CENTRAL/REZ/EVAP         ","ZE",
	"CENTRAL; RESNOR/EVAP     ","ZE",
	"CENTRL                   ","ZC",
	"CHILLER & BOILER         "," Z",
	"CIRCULATE                "," Z",
	"COLD STORAGE             ","  ",
	"COLER/FLOOR HEATER       ","CG",
	"COLLER/FAH               ","BG",
	"COLLING,HUMID,ZONE UNIT,W","TG",
	"COMB.                    ","  ",
	"COMBINATION 3 TON        ","WD",
	"COOELR/FAH               ","BG",
	"COOER/FLOOR HEATER       ","CG",
	"COOERS/AIR COND.         ","GZ",
	"COOL                     "," G",
	"COOL PAK                 "," X",
	"COOL REF                 "," X",
	"COOL/FLOOR HEATER        ","CG",
	"COOL/FORCD               ","BG",
	"COOL/HUMID/FLR UNIT      ","CG",
	"COOL/HUMID/ZONE UNIT     ","TG",
	"COOL/WALL HEATER         ","DG",
	"COOL/WALL UNIT           ","DG",
	"COOL/WALL UNIT/HUMID     ","DG",
	"COOL;HUMID;WALL UNIT     ","DG",
	"COOLER                   "," G",
	"COOLER & A/C @OFFICE     ","ZG",
	"COOLER & FAH             ","BG",
	"COOLER & FAN             "," G",
	"COOLER & FLOOR HEATER    ","CG",
	"COOLER & FORCD AIR HEAT  ","BG",
	"COOLER & FORCD HEAT      ","BG",
	"COOLER & WALL HEATER     ","DG",
	"COOLER & ZONE HEATER     ","TG",
	"COOLER + '06 DUOPAC      ","WD",
	"COOLER - FORCED          ","BG",
	"COOLER - WALL HEAT       ","DG",
	"COOLER - WALL UNIT       ","DL",
	"COOLER / F. AIR          ","XG",
	"COOLER / FA HEAT         ","BG",
	"COOLER / FAH             ","BG",
	"COOLER / FLOOR HEATER    ","CG",
	"COOLER / FORCED AIR HEAT ","BG",
	"COOLER / HEAT            ","XG",
	"COOLER / RADIANT HEATING ","IG",
	"COOLER / WALL            ","DL",
	"COOLER / WALL HEAT       ","DG",
	"COOLER / WALL HEATER     ","DG",
	"COOLER / WALL HEATOR     ","DG",
	"COOLER / WALL HTR        ","DG",
	"COOLER / WALL UNIT       ","DL",
	"COOLER / ZONE HEATER     ","TG",
	"COOLER /FA               ","BG",
	"COOLER /FAH              ","BG",
	"COOLER AND DUO PAK       ","WD",
	"COOLER FAH               ","BG",
	"COOLER FLOOR HEATER      ","CG",
	"COOLER FORCED HEATING    ","BG",
	"COOLER ONLY              ","LG",
	"COOLER WALL HEATER       ","DG",
	"COOLER,ZONE UNIT         ","TG",
	"COOLER- WALL UNIT        ","DG",
	"COOLER-2 WALL UNITS      ","DG",
	"COOLER-FORCED            ","BG",
	"COOLER-HUMID             "," F",
	"COOLER-HUNID`            "," F",
	"COOLER/                  "," G",
	"COOLER/ 2 WALL HEATERS   ","DG",
	"COOLER/ FAH              ","BG",
	"COOLER/ FLOOR HEATER     ","CG",
	"COOLER/ FLOOR HTR        ","CG",
	"COOLER/ NO HEATER        ","LG",
	"COOLER/ NO HEATING       ","LG",
	"COOLER/ WALL HEATER      ","DG",
	"COOLER/ WALL HT          ","DG",
	"COOLER/ WALL UNIT        ","DG",
	"COOLER/2 WALL HEATERS    ","DG",
	"COOLER/FAH               ","BG",
	"COOLER/FLOOR FURNACE     ","CG",
	"COOLER/FLOOR HEAT        ","CG",
	"COOLER/FLOOR HEAT/DUO PAK","CG",
	"COOLER/FLOOR HEATER      ","CG",
	"COOLER/FLOOR HEATER/ZONE ","CG",
	"COOLER/FLOOR HRT         ","CG",
	"COOLER/FLOOR HT          ","CG",
	"COOLER/FLOOR HTR         ","CG",
	"COOLER/FLOOR WALL HEATER ","CG",
	"COOLER/FLOR HEATER       ","CG",
	"COOLER/FLORR HEATER      ","CG",
	"COOLER/GRAVITY HEATING   ","CG",
	"COOLER/HEATER            ","CG",
	"COOLER/HUM               "," F",
	"COOLER/HUMID/WALL UNIT   ","  ",
	"COOLER/NO HEATING        ","LG",
	"COOLER/REZNOR            ","WG",
	"COOLER/REZNOR SUSPENDED  ","WG",
	"COOLER/REZNOR, W. A/C    ","WG",
	"COOLER/RF                "," R",
	"COOLER/W. A/C & W. HEATER","LG",
	"COOLER/W. REF W. HEATERS ","LG",
	"COOLER/WAL HEATER        ","DG",
	"COOLER/WALL              ","DG",
	"COOLER/WALL A/C          "," L",
	"COOLER/WALL AC/FAH       ","BL",
	"COOLER/WALL HEAT         ","DG",
	"COOLER/WALL HEATE        ","DG",
	"COOLER/WALL HEATER       ","DG",
	"COOLER/WALL HEATER/A/C   ","DG",
	"COOLER/WALL HEATERS      ","DG",
	"COOLER/WALL HEATR        ","DG",
	"COOLER/WALL HT           ","DG",
	"COOLER/WALL HTR          ","DG",
	"COOLER/WALL HTR/WINDOW AC","DW",
	"COOLER/WALL UNIT         ","DL",
	"COOLER/WALL/HEATER       ","DG",
	"COOLER/WD STOVE          ","SG",
	"COOLER/WD. STOVE         ","SG",
	"COOLER/ZONE              ","TG",
	"COOLER/ZONE HAET. A/C.   ","TG",
	"COOLER/ZONE HEAT         ","TG",
	"COOLER/ZONE HEATER       ","TG",
	"COOLER/ZONE HEATING      ","TG",
	"COOLER/ZONE HT           ","TG",
	"COOLERS                  "," G",
	"COOLERS & ZONE HEATER    "," G",
	"COOLERS - WALL UNITS     "," L",
	"COOLERS, NO HEAT.        "," G",
	"COOLERS/WALL & FLOOR HTR ","CG",
	"COOLING                  "," G",
	"COOLING - A/C            "," X",
	"COOLING WALL UNIT 3TON RE","CG",
	"COOLING WALL UNIT HUMID  ","FG",
	"COOLING+HUMID            ","FG",
	"COOLING,HUMID,ZONE UNIT  ","TF",
	"COOLING,WALL UNIT,HUMID  ","FG",
	"COOLING,WIND             ","FG",
	"COOLING/ HUMID.          ","FG",
	"COOLING/DUO PAK          ","WD",
	"COOLING/HEATING/WALL UNIT","CG",
	"COOLING/HUMID            "," F",
	"COOLING/HUMID/FLOOR UNIT ","CF",
	"COOLING/HUMID/ZONE UNIT  ","CF",
	"COOLING/WALL UNIT        ","CF",
	"COOLING;HUMID;WALL UNIT  ","CF",
	"COOLLER/FAH              ","BG",
	"COOLR/FAH                ","BG",
	"COOLR/FLOOR HEATER       ","CG",
	"COOLR/WALL HEATER        ","DG",
	"COOLRE/WALL HEATER       ","DG",
	"COOLWALL HEATER          ","DG",
	"COOLWE/FAH               ","BG",
	"CPPLER/ FLOOR HTR        ","CG",
	"D65                      ","  ",
	"DAL PAKS                 ","WD",
	"DAUL-PAC                 ","WD",
	"DAUL-PAK                 ","WD",
	"DFUAL PAK                ","WD",
	"DFUAL PAKS               ","WD",
	"DO PAK                   ","WD",
	"DO PAKS                  ","WD",
	"DOP PAK @ OFFICE         ","WD",
	"DOPAK                    ","WD",
	"DOU PAK                  ","WD",
	"DOU-PAK                  ","WD",
	"DOUPAK                   ","WD",
	"DOWN FLOW                "," A",
	"DOWN FLOW - EVAP         ","AE",
	"DOWN FLOW HEAT/EVAP      ","AE",
	"DOWN FLOW/REFRIG         ","AA",
	"DP                       ","WD",
	"DP & COOLERS             ","WD",
	"DP - EST. 3 TON          ","WD",
	"DP AND EVAP              ","WE",
	"DP-2                     ","WD",
	"DPS                      ","WD",
	"DPS -4                   ","WD",
	"DRCD/REF                 ","BA",
	"DU OPAK                  ","WD",
	"DU PAK                   ","WD",
	"DU-PAK                   ","WD",
	"DU0 PAK                  ","WD",
	"DU0-PAK                  ","WD",
	"DUAK PAKS                ","WD",
	"DUAL                     ","WD",
	"DUAL  AK                 ","WD",
	"DUAL  PAK                ","WD",
	"DUAL - HUM - WALL        ","WF",
	"DUAL - WALL - HUM        ","WF",
	"DUAL 3 TON               ","WD",
	"DUAL AK                  ","WD",
	"DUAL APCK                ","WD",
	"DUAL APK                 ","WD",
	"DUAL DUO PAKS            ","WD",
   "DUAL FLOOR UNIT          ","WD",
	"DUAL P AK                ","WD",
	"DUAL PAC                 ","WD",
	"DUAL PAC (2)             ","WD",
	"DUAL PACK                ","WD",
	"DUAL PACK & WOODSTOVE    ","SD",
	"DUAL PACK, HUMID         ","WD",
	"DUAL PAK                 ","WD",
	"DUAL PAK / WALL HTR      ","DD",
	"DUAL PAKS                ","WD",
	"DUAL PAKS & COOLERS      ","WD",
	"DUAL PAKS & HUMID COOLERS","WF",
	"DUAL P�AK                ","WD",
	"DUAL WALL                ","DD",
	"DUAL WALL / COOLER       ","DD",
	"DUAL WALL / HUMID        ","FD",
	"DUAL WALL HEAT & EVAP    ","DE",
	"DUAL WALL HEAT EVAP COOL ","DE",
	"DUAL WALL HTR/COOLER     ","DD",
	"DUAL WALL UNIT           ","DD",
	"DUAL WALL/EVAP           ","DD",
	"DUAL WALL/HUMID          ","DF",
	"DUAL WALLHT/ EVAP COOL   ","DE",
	"DUAL WALLHT/EVAP         ","DE",
	"DUAL �AK                 ","WD",
	"DUAL-PAC                 ","WD",
	"DUAL-PACK                ","WD",
	"DUAL-PAK                 ","WD",
	"DUAL-PAK'S               ","WD",
	"DUAL-PAK, WALL/ AC       ","WD",
	"DUAL-PAKS                ","WD",
	"DUAL-WALL                ","WD",
	"DUAL-WALL HEATER         ","DD",
	"DUAL-WALLHEAT/COOLER     ","DD",
	"DUAL/PAK                 ","WD",
	"DUALPACK                 ","WD",
	"DUALPAK                  ","WD",
	"DUAPAK                   ","WD",
	"DUCTED HUMID             "," F",
	"DUIO PAK                 ","WD",
	"DUIOPAC                  ","WD",
	"DUL PAK                  ","WD",
	"DUO                      ","WD",
	"DUO  PAC                 ","WD",
	"DUO  PACK                ","WD",
	"DUO  PAK                 ","WD",
	"DUO -PAK                 ","WD",
	"DUO ACK                  ","WD",
	"DUO PAC                  ","WD",
	"DUO PAC (2)              ","WD",
	"DUO PAC / WOODSTOVE      ","WD",
	"DUO PAC HEAT & COOL      ","WD",
	"DUO PAC IN OFFICE AREAS  ","WD",
	"DUO PAC IN RES, OFFICE ON","WD",
	"DUO PAC PREM             ","WD",
	"DUO PAC'S                ","WD",
	"DUO PAC/ PREM H&C        ","WD",
	"DUO PAC/HUM              ","WF",
	"DUO PACK                 ","WD",
	"DUO PACK & EVAP          ","WE",
	"DUO PACK & EVAP UNITS    ","WE",
	"DUO PACK / 3T            ","WD",
	"DUO PACK / WOODSTOVE     ","WD",
	"DUO PACK HT/COOL         ","WD",
	"DUO PACK IN OFFICE       ","WD",
	"DUO PACK IN RES/OFFICE   ","WD",
	"DUO PACK/ WALL           ","WD",
	"DUO PACK/EVAP            ","WE",
	"DUO PACK/EVAP & REZNOR   ","WE",
	"DUO PACK/FLOOR           ","WD",
	"DUO PACK/FORCED          ","WD",
	"DUO PACK/ZONE            ","WD",
	"DUO PACS                 ","WD",
	"DUO PAK                  ","WD",
	"DUO PAK              `   ","WD",
	"DUO PAK & COLLER         ","WD",
	"DUO PAK & COOLER         ","WD",
	"DUO PAK & COOLERTER      ","WD",
	"DUO PAK & EVAP           ","WE",
	"DUO PAK & EVAP.          ","WE",
	"DUO PAK & HUM            ","WD",
	"DUO PAK & WALL/HUMID     ","WF",
	"DUO PAK & ZONEHT/EVAPCOOL","WE",
	"DUO PAK (12)             ","WD",
	"DUO PAK (17)             ","WD",
	"DUO PAK (2)              ","WD",
	"DUO PAK (2TON) HUMID     ","WF",
	"DUO PAK (3)              ","WD",
	"DUO PAK (4)              ","WD",
	"DUO PAK (5)              ","WD",
	"DUO PAK (6)              ","WD",
	"DUO PAK (8)              ","WD",
	"DUO PAK - 2              ","WD",
	"DUO PAK - 3              ","WD",
	"DUO PAK - HUM            ","WF",
	"DUO PAK - PELLET STOVE   ","WD",
	"DUO PAK - ZONE           ","WD",
	"DUO PAK -2               ","WD",
	"DUO PAK -3               ","WD",
	"DUO PAK / 3T             ","WD",
	"DUO PAK / 3T HEAT PUMP   ","WD",
	"DUO PAK / COOLER         ","WD",
	"DUO PAK / EVAP           ","WE",
	"DUO PAK / HEAT PUMP      ","WD",
	"DUO PAK / HUM            ","WF",
	"DUO PAK / HUMID          ","WF",
	"DUO PAK / REFRIG         ","WA",
	"DUO PAK 2 EA             ","WD",
	"DUO PAK 2 TON            ","WD",
	"DUO PAK 3 EA             ","WD",
	"DUO PAK A/C COOL         ","WD",
	"DUO PAK IN OFFICE        ","WD",
	"DUO PAK IN OFFICE AREA   ","WD",
	"DUO PAK IN OFFICES       ","WD",
	"DUO PAK IN RES & OFFICE  ","WD",
	"DUO PAK IN SFR           ","WD",
	"DUO PAK IN STORE         ","WD",
	"DUO PAK(1)/ HUMID (3)    ","WD",
	"DUO PAK(2)               ","WD",
	"DUO PAK(2)/ HUMID (2)    ","WD",
	"DUO PAK, EVAP, REZ       ","WE",
	"DUO PAK, PREM H&C        ","WD",
	"DUO PAK,2-WALL/2-EVAP    ","WE",
	"DUO PAK-DUPLEX           ","WD",
	"DUO PAK/ HUMID           ","WF",
	"DUO PAK/ ZONEHT/EVAP     ","WE",
	"DUO PAK/1 PER UNIT       ","WD",
	"DUO PAK/3T HEAT PUMP     ","WD",
	"DUO PAK/EVAP             ","WE",
	"DUO PAK/EVAP/SP.HEATERS  ","WE",
	"DUO PAK/FLOOR            ","WD",
	"DUO PAK/HEATPUMP         ","WD",
	"DUO PAK/HUM              ","WF",
	"DUO PAK/HUMID            ","WF",
	"DUO PAK/HUMID/FORCD      ","WF",
	"DUO PAK/SOLAR            ","WD",
	"DUO PAK/WALLHT/EVAP      ","WD",
	"DUO PAK/ZONE W/ EVAP     ","WD",
	"DUO PAK2                 ","WD",
	"DUO PAKS                 ","WD",
	"DUO PAKS (11)            ","WD",
	"DUO PAKS (15)            ","WD",
	"DUO PAKS (2)             ","WD",
	"DUO PAKS (3)             ","WD",
	"DUO PAKS (4)             ","WD",
	"DUO PAKS (5)             ","WD",
	"DUO PAKS (7)             ","WD",
	"DUO PAKS - 5             ","WD",
	"DUO PAKS / WALL/EVAP     ","WE",
	"DUO PAKS/1 PER UNIT      ","WD",
	"DUO PAKS/EVAP            ","WE",
	"DUO PAK_(4)              ","WD",
	"DUO PAL                  ","WD",
	"DUO PASK                 ","WD",
	"DUO PCK                  ","WD",
	"DUO PK                   ","WD",
	"DUO PKA                  ","WD",
	"DUO PPAK                 ","WD",
	"DUO PQK                  ","WD",
	"DUO PSK                  ","WD",
	"DUO-APK                  ","WD",
	"DUO-PAC                  ","WD",
	"DUO-PAC IN OFFICE        ","WD",
	"DUO-PAC IN OFFICE/RES    ","WD",
	"DUO-PACK                 ","WD",
	"DUO-PAK                  ","WD",
	"DUO-PAK  (7)             ","WD",
	"DUO-PAK  3 TON           ","WD",
	"DUO-PAK  5 TON           ","WD",
	"DUO-PAK & EVAP           ","WE",
	"DUO-PAK & EVAP (BAR)     ","WE",
	"DUO-PAK (BOTH BLDGS.)    ","WD",
	"DUO-PAK (RES); 2 EVAP    ","WE",
	"DUO-PAK + EVAP           ","WE",
	"DUO-PAK + EVAP; REZNOR   ","WE",
	"DUO-PAK + REF/EVAP       ","WE",
	"DUO-PAK + SP. HEATERS    ","WD",
	"DUO-PAK BASEBOARD;EVAP   ","WE",
	"DUO-PAK IN CHURCH BLDG.  ","WD",
	"DUO-PAK IN COFFEE KIOSK  ","WD",
	"DUO-PAK IN MAIN BLDG.    ","WD",
	"DUO-PAK IN MAIN OFFICE   ","WD",
	"DUO-PAK IN OFFICE        ","WD",
	"DUO-PAK IN OFFICE AREA   ","WD",
	"DUO-PAK IN OFFICES       ","WD",
	"DUO-PAK IN RES           ","WD",
	"DUO-PAK SYSTEM           ","WD",
	"DUO-PAK SYSTEMS          ","WD",
	"DUO-PAK UNITS            ","WD",
	"DUO-PAK'S                ","WD",
	"DUO-PAK(MAIN RES.)       ","WD",
	"DUO-PAK, 5 EVAP UNITS    ","WE",
	"DUO-PAK, EVAP            ","WE",
	"DUO-PAK, EVAP & REZNOR   ","WE",
	"DUO-PAK, EVAP, & RADIANT ","WE",
	"DUO-PAK, EVAP, ZONE HEAT ","WE",
	"DUO-PAK, EVAP/RADIANT    ","WE",
	"DUO-PAK, ZONE            ","WE",
	"DUO-PAK, ZONE/EVAP       ","WE",
	"DUO-PAK,2 EVAP,GRAVITY HT","WE",
	"DUO-PAK,4 EVAP,GR'V'TY HT","WE",
	"DUO-PAK,EVAP,SUSP HT.    ","WE",
	"DUO-PAK/WINDOW A/C       ","WD",
	"DUO-PAK: 7 UNITS         ","WD",
	"DUO-PAK;RADIANT;EVAP     ","WE",
	"DUO-PAK?                 ","WD",
	"DUO-PAKK                 ","WD",
	"DUO-PAKS                 ","WD",
	"DUO-PAKS & EVAP          ","WE",
	"DUO-PAKS IN SOME UNITS   ","WE",
	"DUO-PAKS, EVAP, REZ HEAT ","WE",
	"DUO-PAK_(3)              ","WD",
	"DUO-PAR                  ","WD",
	"DUO/PAK                  ","WD",
	"DUO=PAK                  ","WD",
	"DUOAK                    ","WD",
	"DUOL PAK                 ","WD",
	"DUOM PAK                 ","WD",
	"DUOP ACK                 ","WD",
	"DUOP AK                  ","WD",
	"DUOP AK (2)              ","WD",
	"DUOP PACK                ","WD",
	"DUOPA                    ","WD",
	"DUOPAC                   ","WD",
	"DUOPAC & COOLER          ","WE",
	"DUOPAC & EVAP            ","WE",
	"DUOPAC & FLOOR/EVAP      ","WE",
	"DUOPAC + WALL HT. +EVAP  ","WE",
	"DUOPAC 5 TON             ","WD",
	"DUOPAC IN OFFICE         ","WD",
	"DUOPAC PLUS EVAP         ","WE",
	"DUOPAC'S                 ","WD",
	"DUOPAC/EVAP              ","WE",
	"DUOPACK                  ","WD",
	"DUOPACS                  ","WD",
	"DUOPAK                   ","WD",
	"DUOPAK & FA/REF          ","WD",
	"DUOPAK & ZONE            ","WD",
	"DUOPAK '06               ","WD",
	"DUOPAK '08               ","WD",
	"DUOPAK (2)               ","WE",
	"DUOPAK (FORCED +HUMID)   ","WD",
	"DUOPAK 3.5T              ","WD",
	"DUOPAK 4T                ","WD",
	"DUOPAK AND EVAP          ","WD",
	"DUOPAK IN SALES/OFFICES  ","WD",
	"DUOPAK(2)                ","WD",
	"DUOPAK(4)                ","WD",
	"DUOPAK+HUM               ","WF",
	"DUOPAK-DUPLEX/WL/EVAP RES","WE",
	"DUOPAK-MAIN/WALL/EVAP-2ND","WE",
	"DUOPAK-RES/WALLHT/EVAP-DU","WE",
	"DUOPAK/WALL              ","WD",
	"DUOPAKS                  ","WD",
	"DUOPAKS (2)              ","WD",
	"DUOPAKS-2                ","WD",
	"DUOPAK{                  ","WD",
	"DUOPK                    ","WD",
	"DUOPPAK                  ","WD",
	"DUOU-PAK                 ","WD",
	"DUO_PAK                  ","WD",
	"DUP PAK                  ","WD",
	"DUP-PAK                  ","WD",
	"DUPAC                    ","WD",
	"DUPLEX Y / SFR HEAT ONLY ","Y ",
	"DUPO PAK                 ","WD",
	"DUQL PAK                 ","WD",
	"DUUO PAK                 ","WD",
	"DUUO-PAK                 ","WD",
	"DUYO PAK                 ","WD",
	"E H&C IN OFFICE AREA     ","WO",
	"E WALL HEAT & EVAP       ","FE",
	"E WALL HEAT + EVAP       ","FE",
	"E WALL HT. & EVAP        ","FE",
	"E. WALL HT. + EVAP       ","FE",
	"EF-EVAP                  ","FE",
	"EF-FORCED                ","FE",
	"ELEC / HUMID             ","FF",
	"ELEC BASEBOARD/EVAP      ","FE",
	"ELEC WALL HEAT           ","F ",
	"ELEC WALL UNIT           ","FL",
	"ELECT / REF              ","FA",
	"ELECT. RADIANT/EVAP      ","FE",
	"ELECT/ZONE               ","FE",
	"ELECTRIC / WINDOW        ","FW",
	"ELECTRIC HEAT EVAP COOL  ","FE",
	"ELECTRIC HEAT ONLY       ","FE",
	"ES                       ","  ",
	"EST 3 1/2 TON            "," A",
	"EST 3 TON                "," A",
	"EST AC IN OFFICE         "," A",
	"EST CENTRAL H&C          ","XC",
	"EST DUO PAK              ","WD",
	"EST DUOPAC               ","WD",
	"EST FA HEAT EVAP COOL    ","BE",
	"EST FLOOR                ","C ",
	"EST FLR, EVAP&ZONE       ","EF",
	"EST FLR/EVAP             ","EF",
	"EST FLR/WINDOW           ","WF",
	"EST FLR/ZONE             ","FE",
	"EST HEAT PUMP            ","GH",
	"EST WALL/EVAP            ","DE",
	"EST WALL/ZONE            ","FE",
	"EST WALLHT/EVAP          ","DE",
	"EST. 2 DUO-PAK UNITS     ","WD",
	"EST. A/C IN OFFICE       "," O",
	"EST. CENTRAL H&C         ","ZC",
	"EST. DUO-PAK             ","WD",
	"EST. EVAP & WD. STOVES(2)","SE",
	"EST. EVAP IN OFFICE      "," E",
	"EST. F/A HEAT & EVAP     ","BE",
	"EST. FLR. HT. & EVAP     ","CE",
	"EST. H&C IN OFFICE AREA  ","XO",
	"EST. WALL HEAT + EVAP    ","DE",
	"EST. WALL HT. & EVAP     ","DE",
	"EST. ZONE HEAT & EVAP    ","TE",
	"EUAL PAK                 ","WD",
	"EVAP                     ","LE",
	"EVAP & 2 DUO-PAK UNITS   ","WE",
	"EVAP & DUO PACK          ","WE",
	"EVAP & DUO-PAK           ","WE",
	"EVAP & FLOOR UNIT        ","CE",
	"EVAP & REFR. COOL        ","WE",
	"EVAP & REZNOR            ","BE",
	"EVAP (HEAT UNKNOWN)      "," E",
	"EVAP + A/C IN OFFICE     "," E",
	"EVAP + DUO-PAK(IN OFFICE)","WE",
	"EVAP + WALL HEAT         ","DE",
	"EVAP + WINDOW A/C        ","WE",
	"EVAP - HEAT UNKNOWN      "," E",
	"EVAP - WALL              ","DE",
	"EVAP / FORCED AIR        ","BE",
	"EVAP / HEAT VAK          "," E",
	"EVAP / WALL              ","DE",
	"EVAP / WALL HEAT         ","BE",
	"EVAP / WALL HT           ","DE",
	"EVAP COOL                "," E",
	"EVAP COOL & AC IN OFFICE "," E",
	"EVAP COOL & DUO PAK OFF. ","WD",
	"EVAP COOL & RADIANT HEAT ","WE",
	"EVAP COOL - FLOOR HEAT   ","CE",
	"EVAP COOL - WALL HEAT    ","DE",
	"EVAP COOL-FAU            ","BE",
	"EVAP COOL-HEATER         ","XE",
	"EVAP COOL-WALL HEAT      ","DE",
	"EVAP COOL/DUOPACK IN OFFC","WE",
	"EVAP COOL/FOLLR HEAT     ","CE",
	"EVAP COOL/FORCED HT      ","BE",
	"EVAP COOL/WALL HEAT      ","DE",
	"EVAP COOL; EST. WALL HEAT","DE",
	"EVAP COOL; PORTABLE HEAT ","BE",
	"EVAP COOLER              "," E",
	"EVAP COOLER & FAU        ","BE",
	"EVAP COOLER & FOLLR HEAT ","CE",
	"EVAP COOLER - FAU HEAT   ","BE",
	"EVAP COOLERS             "," E",
	"EVAP COOLING             "," E",
	"EVAP ONLY                "," E",
	"EVAP UNITS               "," E",
	"EVAP \\ WALL              "," E",
	"EVAP, A/C IN OFFICE      "," E",
	"EVAP, A/C IN OFFICES     "," E",
	"EVAP, REZNOR             "," E",
	"EVAP, WINDOW A/C,FLR.HEAT","TE",
	"EVAP, WINDOW A/C,ZONE HT.","TE",
	"EVAP,FLOOR HEAT          ","CE",
	"EVAP.                    ","CE",
	"EVAP. COOLER             ","CE",
	"EVAP/ WALL?              ","CE",
	"EVAP/2 FLOOR UNITS       ","CE",
	"EVAP/CENTRAL HEAT        ","CE",
	"EVAP/DAUL WALL UNIT      ","WE",
	"EVAP/FA                  ","BE",
	"EVAP/FAU                 ","BE",
	"EVAP/FAU(EST)            ","BE",
	"EVAP/FIREPLACE           ","XE",
	"EVAP/FLFRN               ","CE",
	"EVAP/FLOOR               ","CE",
	"EVAP/FLOOR FURN.         ","CE",
	"EVAP/FLOOR UNIT          ","CE",
	"EVAP/FORCED AIR          ","BE",
	"EVAP/GAS FURNACE         ","NE",
	"EVAP/HEATER              ","XE",
	"EVAP/REZ & DUO-PAK       ","WD",
	"EVAP/REZ/DUO-PAK         ","WD",
	"EVAP/WALL                ","DE",
	"EVAP/WALL FURN.          ","DE",
	"EVAP/WALL HEAT           ","DE",
	"EVAP/WALL UNIT           ","DE",
	"EVAP/WALL/DUO PACK       ","WE",
	"EVAP/WF                  ","DE",
	"EVAP/WINDOW A/C          ","ZE",
	"EVAP/WINDOW HT           ","DE",
	"EVAP/ZONE                ","TE",
	"EVAP; A/C IN OFFICE AREAS"," E",
	"EVAPO COOL FA HEAT       ","  ",
	"EVAPO COOL, SOOD STOVE   ","  ",
	"EVAPO COOL-WALL HEAT     ","DE",
	"EVAPORATIVE              "," E",
	"EVAPS/FLFURNS            ","DE",
	"EVAPS/WF                 ","DE",
	"F A                      ","B ",
	"F A / HUMID              ","BF",
	"F A HEAT                 ","BF",
	"F A HEAT/HUMID COOLL     ","BF",
	"F A HUMID COOL           ","BF",
	"F A REF                  ","BA",
	"F A U                    ","B ",
	"F A/REF                  ","BA",
	"F.A-HUMID                ","BF",
	"F.A.                     ","B ",
	"F.A. / HUMID             ","BF",
	"F.A. REF                 ","BA",
	"F.A./REF                 ","BA",
	"F.A/HUMID                ","BF",
	"F.A/REF.                 ","BA",
	"F/A                      ","B ",
	"F/A & A/C                ","BX",
	"F/A & EVAP               ","BE",
	"F/A & HUM                ","BF",
	"F/A & HUMID              ","BF",
	"F/A & REF                ","BA",
	"F/A & REFR. A/C          ","BA",
	"F/A & REZ HT.; EVAP COOL ","BE",
	"F/A (2) & REF (2)        ","BA",
	"F/A + EVAP               ","BE",
	"F/A - A/C                ","BX",
	"F/A - CENTRAL            ","BC",
	"F/A - EVAP               ","BE",
	"F/A - EVAP COOLER        ","BE",
	"F/A - HUM                ","BF",
	"F/A - REF                ","BA",
	"F/A / A/C                ","BX",
	"F/A / CENTRAL AIR        ","BC",
	"F/A / EVAP               ","BE",
	"F/A / HUM                ","BF",
	"F/A / REF                ","BA",
	"F/A /CENTRAL AIR         ","ZC",
	"F/A /EVAP                ","BE",
	"F/A /HUM                 ","BF",
	"F/A A/C                  "," X",
	"F/A COOLER               "," G",
	"F/A EVA  & ZONE/EVAP     ","BE",
	"F/A EVAP                 "," E",
	"F/A HEAT                 ","B ",
	"F/A HEAT & 4 EVAP UNITS  ","BE",
	"F/A HEAT & A/C           ","BX",
	"F/A HEAT & AC            ","BX",
	"F/A HEAT & EVAP          ","BE",
	"F/A HEAT & EVAP COOL     ","BE",
	"F/A HEAT & PUMP          ","BH",
	"F/A HEAT & REF. A/C      ","BA",
	"F/A HEAT & REFR. A/C     ","BA",
	"F/A HEAT & REFR. AIR     ","BA",
	"F/A HEAT & REFR. COOL    ","BA",
	"F/A HEAT & REFRIG. AIR   ","BA",
	"F/A HEAT + EVAP          ","BE",
	"F/A HEAT + EVAP(EST)     ","BE",
	"F/A HEAT + REFRIG. AIR   ","BA",
	"F/A HEAT + WALL A/C      ","BL",
	"F/A HEAT,EVAP            ","BE",
	"F/A HEAT/EVAP            ","BE",
	"F/A HUM                  ","BF",
	"F/A REF                  ","BA",
	"F/A, EVAP                ","BE",
	"F/A/ CENTRAL AIR         ","ZC",
	"F/A/A/C                  ","BX",
	"F/A/EVAP                 ","BE",
	"F/AC                     ","ZC",
	"FA                       ","B ",
	"FA  & HUMID COOLER       ","BF",
	"FA & DISP                ","B ",
	"FA & EVAP                ","BE",
	"FA & HUMID               ","BF",
	"FA & HUMID COOLER        ","BF",
	"FA & REF                 ","BA",
	"FA & RF                  ","BR",
	"FA - A/C                 ","BX",
	"FA - EVAP                ","BE",
	"FA / AC                  ","BX",
	"FA / COOLER              ","BG",
	"FA / EVAP                ","BE",
	"FA / HUM                 ","BF",
	"FA / HUMID               ","BF",
	"FA / REF                 ","BA",
	"FA / REFF                ","BA",
	"FA / REFIG               ","BA",
	"FA / REFRIG              ","BA",
	"FA / REFRIGERATION       ","BA",
	"FA / REF____________     ","BA",
	"FA / REGRIG              ","BA",
	"FA / REGRIG.             ","BA",
	"FA / RF                  ","BA",
	"FA / WINDOW              ","BW",
	"FA /AC                   ","BX",
	"FA /REF                  ","BA",
	"FA /REFRIG               ","BA",
	"FA A/C                   ","BX",
	"FA AC                    ","BX",
	"FA AC / EVAP             ","BE",
	"FA CAC                   ","ZC",
	"FA CENTRAL A/C           ","ZC",
	"FA D50T EVAP COOL        ","BE",
	"FA EVAP                  ","BE",
	"FA HEAT                  ","B ",
	"FA HEAT  A/C COOL        ","BX",
	"FA HEAT  COOL            ","B4",
	"FA HEAT  EVAP COOL       ","BE",
	"FA HEAT & COOL           ","BG",
	"FA HEAT & COOLING        ","BG",
	"FA HEAT & EVAP           ","BE",
	"FA HEAT & EVAP COOL      ","BE",
	"FA HEAT + A/C COOL       ","BX",
	"FA HEAT + EVAP + A/C     ","BE",
	"FA HEAT / AC COOL        ","BX",
	"FA HEAT / EVAP           ","BE",
	"FA HEAT / EVAP COOL      ","BE",
	"FA HEAT / HUMID          ","BF",
	"FA HEAT / REF            ","BA",
	"FA HEAT / REFRIG         ","BA",
	"FA HEAT A/C + EVAP       ","BE",
	"FA HEAT A/C + EVAP COOL  ","BE",
	"FA HEAT A/C COOL         ","BX",
	"FA HEAT A/C COOLCOOL     ","BX",
	"FA HEAT A/C_COOL         ","BX",
	"FA HEAT AC COOL          ","BX",
	"FA HEAT COOL             ","B4",
	"FA HEAT EVAP & COOL      ","BE",
	"FA HEAT EVAP COOL        ","BE",
	"FA HEAT EVAP COOL{       ","BE",
	"FA HEAT ONLY             ","BN",
	"FA HEAT, WINDOW REF      ","BW",
	"FA HEAT, WOOD STOVE      ","B ",
	"FA HEAT/ 4 TON           ","BX",
	"FA HEAT/COOL             ","B4",
	"FA HEAT/EVAP COOL        ","BE",
	"FA HEAT/EVAP COOLER      ","BE",
	"FA HEATING               "," B",
	"FA HT & COOL             ","B4",
	"FA HT / AC COOL          ","BX",
	"FA HT / EVAP COOL        ","BE",
	"FA HT /AC COOL           ","BX",
	"FA HT /EVAP COOL         ","BE",
	"FA HT A/C COOL           ","BX",
	"FA HT AC COOL            ","BX",
	"FA HT EVAP COOL          ","BE",
	"FA HT/ AC COOL           ","BX",
	"FA HT/ EVAP COOL         ","BE",
	"FA HT/AC COOL            ","BX",
	"FA HT/EVAP COOL          ","BE",
	"FA HT/HUMID AIR          ","BF",
	"FA HUMID                 ","BF",
	"FA REF                   ","BA",
	"FA REFRIG                ","BA",
	"FA& REF                  ","BA",
	"FA&REF                   ","BA",
	"FA,HUM                   ","BF",
	"FA- HUM                  ","BF",
	"FA-CFE                   ","B ",
	"FA-CFE__2 UNITS          ","B ",
	"FA-HUM                   ","BF",
	"FA-HUMID                 ","BF",
	"FA-REF                   ","BA",
	"FA-REF,2 SPLIT UNITS     ","BA",
	"FA.AC                    ","BX",
	"FA.CFE                   ","BA",
	"FA.REF                   ","BA",
	"FA/ COOLER               ","B4",
	"FA/ EVAP                 ","BE",
	"FA/ HUM                  ","BF",
	"FA/ HUMID                ","BF",
	"FA/ REF                  ","BA",
	"FA//REF                  ","BA",
	"FA/AC                    ","BX",
	"FA/AC/SOLAR              ","KX",
	"FA/CENTRAL               ","ZC",
	"FA/CENTRAL AIR           ","ZC",
	"FA/CFE                   ","B ",
	"FA/CFE 5T                ","B ",
	"FA/COLLER                ","B4",
	"FA/COOLER                ","B4",
	"FA/EVAP                  ","BE",
	"FA/EVAP COOLER           ","BE",
	"FA/EVAP/AC               ","BE",
	"FA/EVAP/WALL AC          ","BL",
	"FA/EVAP/WINDOW AC        ","BW",
	"FA/HEAT & EVAP           ","BE",
	"FA/HSM                   ","EB",
	"FA/HT & EVAP             ","BE",
	"FA/HUM                   ","BF",
	"FA/HUM.                  ","BF",
	"FA/HUMID                 ","BF",
	"FA/HUMID AIR             ","BF",
	"FA/HUMID.                ","BF",
	"FA/REF                   ","BA",
	"FA/REF  2 UNITS          ","BA",
	"FA/REF (X4)              ","BA",
	"FA/REF -2 UNITS          ","BA",
	"FA/REF- 2 UNITS          ","BA",
	"FA/REF/EVAP              ","BA",
	"FA/REF/HUMID             ","BA",
	"FA/REF/ZONE              ","BA",
	"FA/REFF                  ","BA",
	"FA/REFMP                 ","BA",
	"FA/REFRIG                ","BA",
	"FA/REF`                  ","BA",
	"FA/REF`                  ","BA",

   "FA/REG                   ","BA",
	"FA/RET                   ","BA",
	"FA/ROOF                  ","BR",
	"FA/ROOF AC               ","BR",
	"FA/WALL AC               ","BL",
	"FA/WALL/HUM              ","BL",
	"FA/WINDOW AC             ","BW",
	"FA/ZONE                  ","BA",
	"FA; REF                  ","BA",
	"FA;U & HUMID COOLER      ","BF",
	"FA? EVAP                 ","BE",
	"FAA/REF                  ","BA",
	"FAAAU & HUMID COOLER     ","BF",
	"FAAC                     ","BX",
	"FAF / REF                ","BA",
   "FAF/REF                  ","BA",
	"FAH                      ","B ",
	"FAH & COOLER             ","BG",
	"FAH & EVAP               ","BE",
	"FAH - EVAP COOLER        ","BE",
	"FAH / CENTRAL            ","ZC",
	"FAH 1 EVAP COOL.         ","BE",
	"FAH A/C                  ","BX",
	"FAH AC                   ","BX",
	"FAH CENT                 ","ZC",
	"FAH EVAP COOL            ","BE",
	"FAH EVAP COOLER          ","BE",
	"FAH ONLY                 ","B ",
	"FAH REFRIG               ","BA",
	"FAH/ CENTRAL             ","ZC",
	"FAH/1EVAP COOL.          ","BE",
	"FAH/AC                   ","BX",
	"FAH/CENT                 ","ZC",
	"FAH/CENTRAL              ","ZC",
	"FAH/EVAP                 ","BE",
	"FAH/EVAP COOL            ","BE",
	"FAH/EVAP COOLER          ","BE",
	"FAH/HUMID.               ","BF",
   "FAH/REF                  ","BA",
	"FAHEAT A/C COOL          ","BX",
	"FAHEAT EVAP COOL         ","BE",
	"FAHT / EVAP COOL         ","BE",
	"FAHT/AC COOL             ","BX",
	"FAHT/EVAP COOL           ","BE",
	"FALL AC                  "," L",
	"FAN  AC                  "," X",
	"FAN  EVAP                "," E",
	"FAN / AC                 "," X",
	"FAN / EVAP COOL          "," E",
	"FAN A/C                  "," X",
	"FAN AC                   "," X",
	"FAN EVAP                 "," E",
	"FAN EVAP COOL            "," E",
	"FAN-HUMID COOL           "," F",
	"FAN/AC                   "," X",
	"FAN/EVAP                 "," E",
	"FAN/EVAP COOL            "," E",
	"FAREF                    ","BA",
	"FAU                      ","B ",
	"FAU  & HUMID COOLER      ","BF",
	"FAU &  REFRIG.           ","BA",
	"FAU & COOLER             ","BG",
	"FAU & COOLER  WOODSTOVE  ","SG",
	"FAU & EVAP COOL          ","BE",
	"FAU & EVAP COOLER        ","BE",
	"FAU & EVAPCOOLER         ","BE",
	"FAU & HEATER             ","B ",
	"FAU & HHUMID COOLER      ","BF",
	"FAU & HUMID              ","BF",
	"FAU & HUMID CCOOLER      ","BF",
	"FAU & HUMID COL          ","BF",
	"FAU & HUMID COOL         ","BF",
	"FAU & HUMID COOL/WOODSTVE","SF",
	"FAU & HUMID COOL;ER      ","BF",
	"FAU & HUMID COOLE        ","BF",
	"FAU & HUMID COOLER       ","BF",
	"FAU & HUMID COOLERS      ","BF",
	"FAU & HUMID COOOLER      ","BF",
	"FAU & HUMIDCOOLER        ","BF",
	"FAU & HUN=MID COOLER     ","BF",
	"FAU & REFRIG             ","BA",
	"FAU & REFRIG.            ","BA",
	"FAU & REFRIGEATION       ","BA",
	"FAU & REFRIGERATION      ","BA",
	"FAU & REGRIG             ","BA",
	"FAU & REGRIG.            ","BA",
	"FAU & WINDOW A/C         ","BW",
	"FAU & WINDOW COOLER      ","BW",
	"FAU & WVAP COOLER        ","BE",
	"FAU &EVAP COOLER         ","BE",
	"FAU &HUMID COOL          ","BF",
	"FAU &HUMID COOLER        ","BF",
	"FAU - A/C                ","BX",
	"FAU - EVAP               ","BE",
	"FAU - EVAP COOL          ","BE",
	"FAU / A/C                ","BX",
	"FAU / A/C WALL EVAP      ","BE",
	"FAU / AC                 ","BX",
	"FAU / EVAP               ","BE",
	"FAU / EVAP COOLER        ","BE",
	"FAU / HUMID COOL         ","BF",
	"FAU / HUMID COOLER       ","BF",
	"FAU 7 HUMID COOL         ","BF",
	"FAU 7 HUMID COOLER       ","BF",
	"FAU A/C                  ","BX",
	"FAU AC                   ","BX",
	"FAU CENT                 ","ZC",
	"FAU HEAT - HUM COOL      ","BF",
	"FAU HT/ AC               ","BX",
	"FAU HT/ EVAP             ","BE",
	"FAU HT/ EVAP COOL        ","BE",
	"FAU HT/ WALL AC          ","BL",
	"FAU HT/AC                ","BX",
	"FAU HTG/AC               ","BX",
	"FAU �AC                  ","BX",
	"FAU&WALL REFRIG UNITS    ","BA",
	"FAU'S & REFRIG.          ","BA",
	"FAU'S; 3 EVAP; 1 REFRIG. ","BA",
	"FAU, EVAP COOL, WOODSTOVE","SE",
	"FAU,ZONE                 ","T ",
	"FAU-A/C                  ","BX",
	"FAU-AC                   ","BX",
	"FAU-EVAP-DUOPAC          ","WD",
	"FAU-HUMID COOL           ","BF",
	"FAU-PAK                  ","B ",
	"FAU-REFRIGERATION        ","BA",
	"FAU/                     "," B",
	"FAU/ (2) AC'S            ","BX",
	"FAU/ 2 AC'S              ","BX",
	"FAU/ A/C                 ","BX",
	"FAU/ AC                  ","BX",
	"FAU/ CENT                ","ZC",
	"FAU/ EVAP COOLER         ","BE",
	"FAU/(2) AC'S             ","BX",
	"FAU/2 AC                 ","BX",
	"FAU/2 TON & 3 TON        ","BX",
	"FAU/3T AC                ","BX",
	"FAU/A/C                  ","BX",
	"FAU/AC                   ","BX",
	"FAU/AC  WALL HT          ","DX",
	"FAU/AC  WALL/EVAP-WND AC ","DX",
	"FAU/AC & EVAP.           ","BE",
	"FAU/AC (2)               ","BX",
	"FAU/AC EACH UNIT         ","BX",
	"FAU/AC&EVAP.             ","DX",
	"FAU/AC'S                 ","BX",
	"FAU/AC(1)                ","BX",
	"FAU/AC, & EVAP.          ","BE",
	"FAU/AC-SFR WALLHT/EVAP/DU","DE",
	"FAU/AC/2 WHOLE HOUSE FANS","ZX",
	"FAU/AC/COOLER            ","BX",
	"FAU/AC/EVAP              ","ZE",
	"FAU/AC/EVAP AND FLRHT    ","CE",
	"FAU/C.EVAP               ","ZE",
	"FAU/CAC                  ","  ",
	"FAU/CAC +EVAP            ","  ",
	"FAU/CAC/EVAP             ","  ",
	"FAU/CENREF               ","ZA",
	"FAU/CENREF-EVAP          ","ZA",
	"FAU/CENT                 ","ZC",
	"FAU/CENTRAL              ","ZC",
	"FAU/CENTRAL EVAP         ","ZA",
	"FAU/EVAP                 ","BE",
	"FAU/EVAP  WALL/EVAP      ","BE",
	"FAU/EVAP COOL            ","BE",
	"FAU/EVAP COOLER          ","BE",
	"FAU/EVAP COOLING         ","BE",
	"FAU/EVAP COOOLER         ","BE",
	"FAU/EVAP.                ","BE",
	"FAU/EVAP. WALL/EVAP      ","BE",
	"FAU/EVAP/                ","BE",
	"FAU/EVAP/A/C             ","BE",
	"FAU/EVAPORATIVE          ","BE",
	"FAU/HUM                  ","BF",
	"FAU/HUM/A/C              ","BF",
	"FAU/HUMID                ","BF",
	"FAU/HUMID COOL           ","BF",
	"FAU/HUMID COOLER         ","BF",
	"FAU/NONE                 ","  ",
	"FAU/REF                  ","ZA",
	"FAU/REF.                 ","ZA",
	"FAU/REFRIG               ","ZA",
	"FAU/RF                   ","ZA",
	"FAU/WALL                 ","Z ",
	"FAU/WALL HT/EVAP COOL    ","ZE",
	"FAU/WALL/EVAP            ","ZE",
	"FAU/WINDOW               ","ZX",
	"FAUCAC                   ","ZD",
	"FAULCENT                 ","ZD",
	"FAULCENT (2)             ","ZD",
	"FAUPAK                   ","WD",
	"FAUU-HUMID COOL          ","BF",
	"FAV                      ","B ",
	"FAX/AC                   ","BX",
	"FE/REF                   ","C ",
	"FF                       ","C ",
	"FF - EVAP                ","CE",
	"FF / AC                  ","CX",
	"FF / EVAP                ","CE",
	"FF / EVAP / AC           ","CE",
	"FF /EVAP                 ","CE",
	"FF EVAP                  ","CE",
	"FF/AC                    ","CX",
	"FF/AC/EVAP               ","CE",
	"FF/EVAP                  ","CE",
	"FF/FA/EVAP               ","CE",
	"FF/WAC                   ","CL",
	"FH/CENTRAL               ","ZC",
	"FIREPLACE                ","X ",
	"FL FRN/EVA               ","CE",
	"FL. & WALL HT. EVAP COOL ","CE",
	"FL. + WALL HEAT EVAP COOL","CE",
	"FL.& WALL HEAT EVAP COOL ","CE",
	"FL/EVAP                  ","CE",
	"FLF/EVAP                 ","CE",
	"FLFRN                    ","C ",
	"FLFRN/EVAP               ","CE",
	"FLLOR FURNACE/COOLER     ","CG",
	"FLOOD FURNACE/COOLER     ","CG",
	"FLOOE/EVAP.              ","CE",
	"FLOOF EVAP               ","CE",
	"FLOOR                    ","C ",
	"FLOOR & 2 EVAP'S         ","CE",
	"FLOOR & EVAP             ","CE",
	"FLOOR & HUMID            ","CF",
	"FLOOR & WALL HEAT EVAP   ","CF",
	"FLOOR & WALL HEAT-HUMID C","CF",
	"FLOOR & WALL HT. EVAP    ","CE",
	"FLOOR (1) / ZONE (1)     ","C ",
	"FLOOR (2) / HUMID        ","CF",
	"FLOOR (4)                ","C ",
	"FLOOR + WALL HEAT + EVAP ","CE",
	"FLOOR + WALL HEAT EVAP   ","CE",
	"FLOOR + WALL HT EVAP COOL","CE",
	"FLOOR + WALL HT. EVAP    ","CE",
	"FLOOR + ZONE HEAT EVAP   ","  ",
	"FLOOR - A/C              ","CX",
	"FLOOR - EVAP             ","CE",
	"FLOOR - HUM              ","CF",
	"FLOOR - REF - WALL UNIT  ","CL",
	"FLOOR / AC               ","CX",
	"FLOOR / CENTRAL / EVAP   ","CE",
	"FLOOR / CENTRAL AC       ","CC",
	"FLOOR / COOLEER          ","CG",
	"FLOOR / COOLER           ","CG",
	"FLOOR / COOLERS          ","CG",
	"FLOOR / EVAP             ","CE",
	"FLOOR / HUM              ","CF",
	"FLOOR / HUMID            ","CF",
	"FLOOR / HUMID (2)        ","CF",
	"FLOOR / REF              ","CA",
	"FLOOR / REFRIG           ","CA",
	"FLOOR / ROOF COOLER      ","CR",
	"FLOOR / WALL             ","CL",
	"FLOOR / WIN A/C          ","CW",
	"FLOOR COOLER             ","C ",
	"FLOOR EVAP               ","CE",
	"FLOOR FER/COOLER         ","CG",
	"FLOOR FUNCACE AND COOLER ","  ",
	"FLOOR FUR                ","C ",
	"FLOOR FURN / COOLER      ","CG",
	"FLOOR FURN / WATER COOLER","CZ",
	"FLOOR FURN.              ","C ",
	"FLOOR FURNACE            ","C ",
	"FLOOR FURNACE & COOLER   ","CG",
	"FLOOR FURNACE & EVAP COOL","CE",
	"FLOOR FURNACE / COOLER   ","CG",
	"FLOOR FURNACE / ROOF A/C ","CR",
	"FLOOR FURNACE AND COOLER ","CG",
	"FLOOR FURNACE EVAP COOL  ","CG",
	"FLOOR FURNACE HEAT       ","CG",
	"FLOOR FURNACE/ COOLER    ","CG",
	"FLOOR FURNACE/ HUMID     ","CF",
	"FLOOR FURNACE/2 COOLERS  ","CG",
	"FLOOR FURNACE/AC         ","CX",
	"FLOOR FURNACE/COOLER     ","CG",
	"FLOOR FURNACE/HUMID      ","CF",
	"FLOOR FURNACE/HUMID COOL ","CF",
	"FLOOR FURNACE/WALL COOLER","CL",
	"FLOOR FURNACE/WINDOW AC  ","CW",
	"FLOOR FURNANCE           ","C ",
	"FLOOR FURNANCE / COOLER  ","CG",
	"FLOOR FURNANCE /COOLER   ","CG",
	"FLOOR FURNANCE A/C       ","CX",
	"FLOOR FURNANCE/ HUMID    ","CF",
	"FLOOR FURNANCE/AC        ","CX",
	"FLOOR FURNANCE/COOLER    ","CG",
	"FLOOR FURNANCE/HUMID     ","CF",
	"FLOOR FURNS AND EVAPS    ","CG",
	"FLOOR HAEA- HUMID COOL   ","CF",
	"FLOOR HEAT               ","C ",
	"FLOOR HEAT  EVAP COOL    ","CE",
	"FLOOR HEAT  EVAP COOLER  ","CE",
	"FLOOR HEAT  EVAP COOLING ","CE",
	"FLOOR HEAT & (EST.) EVAP ","CE",
	"FLOOR HEAT & EVAP        ","CE",
	"FLOOR HEAT & EVAP COOL   ","CE",
	"FLOOR HEAT & WINDOW A/C  ","CW",
	"FLOOR HEAT + EVAP        ","CE",
	"FLOOR HEAT + EVAP (EST.) ","CE",
	"FLOOR HEAT + WINDOW A/C  ","CW",
	"FLOOR HEAT - EVAP        ","CE",
	"FLOOR HEAT / COOLER      ","CG",
	"FLOOR HEAT / EVAP        ","CE",
	"FLOOR HEAT / EVAP COOL   ","CE",
	"FLOOR HEAT / EVAP COOLER ","CE",
	"FLOOR HEAT / HUMID       ","CF",
	"FLOOR HEAT / REFR WALL   ","CL",
	"FLOOR HEAT / WALL A/C    ","CL",
	"FLOOR HEAT /EVAP COOL    ","CE",
	"FLOOR HEAT A/C COOL      ","CX",
	"FLOOR HEAT EVAP COOL     ","CE",
	"FLOOR HEAT WINDOW EVAP   ","CE",
	"FLOOR HEAT,HUMID         ","CF",
	"FLOOR HEAT- WALL COOLER  ","CL",
	"FLOOR HEAT-EVAP COOL     ","CE",
	"FLOOR HEAT-EVAP COOLER   ","CE",
	"FLOOR HEAT-HUMID & REFRIG","CA",
	"FLOOR HEAT-HUMID COLER   ","CF",
	"FLOOR HEAT-HUMID COOL    ","CF",
	"FLOOR HEAT-HUMID COOLER  ","CF",
	"FLOOR HEAT-REFRIG & HUMID","CA",
	"FLOOR HEAT-REFRIG UNIT   ","CA",
	"FLOOR HEAT-REFRIGERATION ","CA",
	"FLOOR HEAT-WALL REFRIG   ","CA",
	"FLOOR HEAT-WINDOW A/C    ","CW",
	"FLOOR HEAT-WINDOW REFRIG ","CA",
	"FLOOR HEAT/              ","C ",
	"FLOOR HEAT/ HUMID        ","CF",
	"FLOOR HEAT/AC            ","CX",
	"FLOOR HEAT/COOLER        ","CG",
	"FLOOR HEAT/EVAP          ","CE",
	"FLOOR HEAT/EVAP COOL     ","CE",
	"FLOOR HEAT/EVAP COOLER   ","CE",
	"FLOOR HEAT/EVAP/A/C      ","CE",
	"FLOOR HEAT/HUM           ","CF",
	"FLOOR HEAT/HUMID         ","CF",
	"FLOOR HEAT/HUMID COOL    ","CF",
	"FLOOR HEAT/NONE          ","C ",
	"FLOOR HEAT/WALL A/C      ","CZ",
	"FLOOR HEAT/WINDOW A/C    ","CW",
	"FLOOR HEAT=HUMID COOL    ","CF",
	"FLOOR HEATED             ","C ",
	"FLOOR HEATER             ","C ",
	"FLOOR HEATER & HUMID COOL","CF",
	"FLOOR HEATER-COOLER      ","CG",
	"FLOOR HEATER-HUMID COOLER","CF",
	"FLOOR HEATER/ HUMID      ","CF",
	"FLOOR HEATER/COOLER      ","CG",
	"FLOOR HEATER/EVAP COOL   ","CE",
	"FLOOR HEATER/HUMID       ","CF",
	"FLOOR HEATER/HUMID COOL  ","CF",
	"FLOOR HEATER/HUMID COOLER","CF",
	"FLOOR HEATERS            ","C ",
	"FLOOR HEATING            ","C ",
	"FLOOR HEATING/HUMID      ","CF",
	"FLOOR HT                 ","C ",
	"FLOOR HT & EVAP          ","CE",
	"FLOOR HT / EVAP COOL     ","CE",
	"FLOOR HT EVAP            ","CE",
	"FLOOR HT EVAP COOL       ","CE",
	"FLOOR HT-EVAP            ","CE",
	"FLOOR HT/ EVAP           ","CE",
	"FLOOR HT/ EVAP COOL      ","CE",
	"FLOOR HT/ ROOF/WALL EVAP ","CE",
	"FLOOR HT/ WINDOW COOLER  ","CW",
	"FLOOR HT/EVAP            ","CE",
	"FLOOR HT/EVAP & AC       ","CE",
	"FLOOR HT/EVAP COOL       ","CE",
	"FLOOR HT/WINDOW COOLER   ","CW",
	"FLOOR HTR                ","C ",
	"FLOOR HTR/COOLER         ","CG",
	"FLOOR HTR/EVAP           ","CE",
	"FLOOR HUMID              ","CF",
	"FLOOR REF                ","CA",
	"FLOOR UNIT               ","C ",
	"FLOOR UNIT & HUMID       ","CF",
	"FLOOR UNIT - COOLER      ","CG",
	"FLOOR UNIT - HUM         ","CF",
	"FLOOR UNIT - REF         ","CA",
	"FLOOR UNIT / 2 TON REF   ","CA",
	"FLOOR UNIT / COOLER      ","CG",
	"FLOOR UNIT / EVAP        ","CE",
	"FLOOR UNIT / HUMID       ","CF",
	"FLOOR UNIT / RF          ","CR",
	"FLOOR UNIT / WALL UNIT   ","CL",
	"FLOOR UNIT A/C           ","CX",
	"FLOOR UNIT AND COOLER    ","CG",
	"FLOOR UNIT COOLING HUMID ","CF",
	"FLOOR UNIT EVAP          ","CE",
	"FLOOR UNIT H/C           ","  ",
	"FLOOR UNIT, EVAP         ","CE",
	"FLOOR UNIT,COOLING,HUMID ","CF",
	"FLOOR UNIT-COOLER        ","CG",
	"FLOOR UNIT-HUMID         ","CF",
	"FLOOR UNIT-HUMID.        ","CF",
	"FLOOR UNIT/ A/C WINDOW   ","CW",
	"FLOOR UNIT/ COOLER       ","CG",
	"FLOOR UNIT/ HUM          ","CF",
	"FLOOR UNIT/ HUMID        ","CF",
	"FLOOR UNIT/ WALL UNIT    ","CL",
	"FLOOR UNIT/AC            ","CX",
	"FLOOR UNIT/COOLER        ","CG",
	"FLOOR UNIT/COOLER DUCT   ","CG",
	"FLOOR UNIT/EVAP          ","CE",
	"FLOOR UNIT/HUM           ","CF",
	"FLOOR UNIT/HUMID         ","CF",
	"FLOOR UNIT/HUMID.        ","CF",
	"FLOOR UNIT/REF           ","CA",
	"FLOOR UNIT/REFRIG        ","CA",
	"FLOOR UNIT/ROOF COOLER   ","CR",
	"FLOOR UNIT/WALL          ","CL",
	"FLOOR UNIT/WALL AC       ","CL",
	"FLOOR UNIT/WALL REFRIG   ","CL",
	"FLOOR UNIT/WIN REF       ","CA",
	"FLOOR UNIT/WIN-REF       ","CA",
	"FLOOR UNIT/WIND AC       ","CZ",
	"FLOOR UNIT/WINDOW A/C    ","CW",
	"FLOOR UNIT/WINDOW AC     ","CW",
	"FLOOR UNIT/ZONE          ","C ",
	"FLOOR UNIT/ZONE UNIT     ","C ",
	"FLOOR UNITS              ","C ",
	"FLOOR UNITS/COOLERS      ","CG",
	"FLOOR WALL EVAP          ","CE",
	"FLOOR WALL HEAT & EVAP   ","CE",
	"FLOOR& WALL HEAT EVAP COO","CG",
	"FLOOR+HUMID              ","CF",
	"FLOOR, EVAP              ","CE",
	"FLOOR, HUMID.            ","CF",
	"FLOOR, ZONE; EVAP        ","CE",
	"FLOOR,HUMID              ","CF",
	"FLOOR,WALL;EVAP          ","CE",
	"FLOOR- REF               ","CA",
	"FLOOR-EVAP               ","CE",
	"FLOOR/                   ","C ",
	"FLOOR/ 2 HUMID           ","CF",
	"FLOOR/ AC                ","CX",
	"FLOOR/ COOLER            ","CG",
	"FLOOR/ COOLING           ","CG",
	"FLOOR/ EVAP              ","CE",
	"FLOOR/ HUMID             ","CF",
	"FLOOR/ REF 2 TON         ","CA",
	"FLOOR/2 HUMID            ","CF",
	"FLOOR/5 HEAT PUMP        ","  ",
	"FLOOR/AC                 ","CX",
	"FLOOR/AC; DUO PAK        ","WD",
	"FLOOR/CENT               ","CC",
	"FLOOR/COLER              ","CG",
	"FLOOR/COOLER             ","CG",
	"FLOOR/EVAO.              ","CE",
	"FLOOR/EVAP               ","CE",
	"FLOOR/EVAP AC            ","CE",
	"FLOOR/EVAP COOLER        ","CE",
	"FLOOR/EVAP ROOF          ","CE",
	"FLOOR/EVAP.              ","CE",
	"FLOOR/EVSAP/ZONE         ","CE",
	"FLOOR/HEAT               ","C ",
	"FLOOR/HUM                ","CF",
	"FLOOR/HUMID              ","CF",
	"FLOOR/NONE               ","  ",
	"FLOOR/REF                ","CA",
	"FLOOR/ROOF AC            ","CX",
	"FLOOR/ROOM A/C           ","CX",
	"FLOOR/WALL               ","CL",
	"FLOOR/WALL HT/EVAPS      ","CE",
	"FLOOR/WALL REF.          ","CL",
	"FLOOR/WINDOW             ","CW",
	"FLOOR/WINDOW REF.        ","CW",
	"FLOOR/WOODSTOVE          ","  ",
	"FLOOR/ZONE UNIT          ","  ",
	"FLOOR; DUO PAK           ","WD",
	"FLOOR; EVAP              ","CE",
	"FLOOR;AC                 ","CX",
	"FLOOR;EVAP               ","CE",
	"FLOOR;EVAP/DUO PAC       ","WD",
	"FLOORHEAT EVAP COOL      ","CE",
	"FLOORHT                  ","C ",
	"FLOORHT/ EVAP            ","CE",
	"FLOORHT/AC               ","CX",
	"FLOORHT/EVAP             ","CE",
	"FLOORHT/EVAP COOL        ","CE",
	"FLOORHT/WINDOW EVAP      ","CE",
	"FLOORING HUMID UNIT      ","CF",
	"FLOR HT/EVAP COOLER      ","CE",
	"FLORR HT/EVAP COOL       ","CE",
	"FLORR/WINDOW A/C         ","CX",
	"FLR                      ","C ",
	"FLR & WALL HT/EVAP COOL  ","CE",
	"FLR HEAT                 ","C ",
	"FLR HEAT & EVAP          ","CE",
	"FLR HEAT - EVAP - RM A/C ","CE",
	"FLR HEAT / EVAP          ","CE",
	"FLR HEAT/EVAP            ","  ",
	"FLR HEAT/HUM             ","CF",
	"FLR HEAT/HUMID           ","CF",
	"FLR HEAT/REF             ","CA",
	"FLR HT & EVAP            ","CE",
	"FLR HT EVAP COOLER       ","CE",
	"FLR HT,ZN HT, EVAP COOL  ","CE",
	"FLR HT/ AC               ","CX",
	"FLR HT/ WINDOW AC        ","CW",
	"FLR HT/AC                ","CX",
	"FLR HT/EVAP              ","CE",
	"FLR HT/EVAP COOL         ","CE",
	"FLR HT/EVAP COOL.        ","CE",
	"FLR HT/EVAP COOLER       ","CE",
	"FLR HT/WINDOW AC         ","CX",
	"FLR HT/WINDOW EVAP       ","CE",
	"FLR UNIT                 ","C ",
	"FLR UNIT / WALL R & F    ","C ",
	"FLR UNIT, CENTERAL       ","CC",
	"FLR UNIT, HUMID          ","CF",
	"FLR UNIT/ HUM            ","CF",
	"FLR UNIT/COOLER          ","CG",
	"FLR UNIT/HUM             ","CF",
	"FLR UNIT/HUM/COOLER      ","CF",
	"FLR UNIT/HUMID           ","CF",
	"FLR UNIT/HUMID/COOLER    ","CF",
	"FLR. HEAT & EVAP         ","CE",
	"FLR. HEAT & WALL A/C     ","CL",
	"FLR. HEAT + CENTRAL A/C  ","CC",
	"FLR. HT. & EVAP          ","CE",
	"FLR. HT. + EVAP (EST)    ","CE",
	"FLR./WALL HEAT & EVAP    ","CE",
	"FLR.HEAT, ROOF COOLERS,AC","CR",
	"FLR.HT. & EVAP           ","CE",
	"FLR.HT.& WINDOW REFR.    ","CA",
	"FLR/EVAP                 ","CE",
	"FLR/EVAP  DUO PAK        ","WD",
	"FLR/EVAP (2)DUO PAKS     ","WD",
	"FLR/EVAP/DUOPK ON ADDN   ","CD",
	"FLR/HUM                  ","CF",
	"FLR/WALL HT/ EVAP        ","CE",
	"FLR/ZONE                 ","  ",
	"FLRHT 1ST FLOR ONLY/AC   ","CX",
	"FLRHT/COOLER             ","CG",
	"FLRHT/EVAP               ","CE",
	"FLRHT/EVAP COOL          ","CE",
	"FLRHT/WALLHT/EVAP COOL   ","CE",
	"FOC'D & REF              ","BA",
	"FOCED / HUMID            ","BF",
	"FOCED HT/ REF            ","BA",
	"FOCED/HUMID              ","BF",
	"FOOLR UNIT               ","C ",
	"FOOR                     ","C ",
	"FOOR; EVAP               ","CE",
	"FOR'D & REF              ","BA",
	"FORC  REF                ","BA",
	"FOR./REF                 ","BA",
	"FOR./REF.                ","BA",
	"FOR/REF                  ","BA",
	"FORC / HUMID             ","BF",
	"FORC'D                   ","B ",
	"FORC'D & A/C             ","BX",
	"FORC'D /  A/C            ","BX",
	"FORC'D / A/C             ","BX",
	"FORC'D / REF             ","BA",
	"FORC'D / REF.            ","BA",
	"FORC'D AIR & REF         ","BA",
	"FORC'D REF               ","BA",
	"FORC'D,REF.              ","BA",
	"FORC'D/ HUMID.           ","BF",
	"FORC'D/ REF.             ","BA",
	"FORC'D/HUMID             ","BF",
	"FORC'D/HUMID/CEILING VENT","BF",
	"FORC'D/REF               ","BA",
	"FORC'D/REF.              ","BA",
	"FORC'D/REFRIG            ","BA",
	"FORC'D/REG.              ","BA",
	"FORC/REF                 ","BA",
	"FORCD                    ","BA",
	"FORCD   REF              ","BA",
	"FORCD  REF               ","BA",
	"FORCD & REF.             ","BA",
	"FORCD AIR HEAT HUMID COOL","BF",
	"FORCD HEAT/AC            ","BX",
	"FORCD/COOLER             ","BG",
	"FORCD/EVAP               ","BE",
	"FORCD/HUMID              ","BF",
	"FORCD/REF                ","BA",
	"FORCD/REF/WINDOW/WALL    ","BA",
	"FORCD/REFIG COOL         ","CG",
	"FORCD; 3 EVAP; 1 REFRIG. ","CE",
	"FORCE                    ","B ",
	"FORCE AIR & EVAP         ","BE",
	"FORCE HT / REF COOL      ","BA",
	"FORCE/WALLUNIT/COOLING/  ","BG",
	"FORCED                   ","B ",
	"FORCED & EVAP            ","BE",
	"FORCED & REF             ","BA",
	"FORCED & REFRIG          ","BA",
	"FORCED & SOLAR           ","BJ",
	"FORCED (2)               ","B ",
	"FORCED + EVAP            ","BE",
	"FORCED + HUMID           ","BF",
	"FORCED + REF             ","BA",
	"FORCED - A/C             ","BX",
	"FORCED - COOLER          ","BG",
	"FORCED - EVAP            ","BE",
	"FORCED - HUM             ","BF",
	"FORCED - REF             ","BA",
	"FORCED - ROOM A/C        ","BX",
	"FORCED / 2 1/2 TON       ","BA",
	"FORCED / 2 TON WINDOW    ","BW",
	"FORCED / 3 T GAS         ","B ",
	"FORCED / A/C             ","BX",
	"FORCED / AC              ","BX",
	"FORCED / COOLER          ","BG",
	"FORCED / COOLERS         ","BG",
	"FORCED / COOLING         ","BG",
	"FORCED / EVAP            ","BE",
	"FORCED / HEAT PUMP       ","GH",
	"FORCED / HUM             ","BF",
	"FORCED / HUMID           ","BF",
	"FORCED / HUMID (2 EA)    ","BF",
	"FORCED / HUMID (2)       ","BF",
	"FORCED / HUMID RF        ","BF",
	"FORCED / HUMID/ZONE      ","BF",
	"FORCED / REF             ","BA",
	"FORCED / REF (2 EA)      ","BA",
	"FORCED / REF (2)         ","BA",
	"FORCED / REF 2 1/2 TON   ","BA",
	"FORCED / REFRIG          ","BA",
	"FORCED / RF              ","BR",
	"FORCED / ROOF COOLER     ","BR",
	"FORCED / ROOF COOLING    ","BR",
	"FORCED /REF              ","BA",
	"FORCED 3 TON             ","BA",
	"FORCED 5 T LENNOX        ","BA",
	"FORCED A/C               ","BX",
	"FORCED A/C COOL          ","BX",
	"FORCED AC                ","BX",
	"FORCED AC/HUMID          ","BF",
	"FORCED AIR               ","BX",
	"FORCED AIR & EVAP        ","BE",
	"FORCED AIR & REFRIG      ","BA",
	"FORCED AIR / A C         ","BX",
	"FORCED AIR / A/C         ","BX",
	"FORCED AIR / AC          ","BX",
	"FORCED AIR / COOLER      ","BG",
	"FORCED AIR / EVAP        ","BE",
	"FORCED AIR / REF         ","BA",
	"FORCED AIR / REF / COOLER","BA",
	"FORCED AIR / REFIG       ","BA",
	"FORCED AIR / REFRIG      ","BA",
	"FORCED AIR / REFRIG 4T   ","BA",
	"FORCED AIR AND EVAP      ","BE",
	"FORCED AIR HEAT          ","B ",
	"FORCED AIR HEAT EVAP COOL","BE",
	"FORCED AIR HEAT/EVAP.    ","BE",
	"FORCED AIR HEAT/EVAPCOOL ","BE",
	"FORCED AIR REF           ","BA",
	"FORCED AIR REF.          ","BA",
	"FORCED AIR, EVAP         ","BE",
	"FORCED AIR, REFRIG       ","BE",
	"FORCED AIR,EVAP          ","BE",
	"FORCED AIR-EVAP          ","BE",
	"FORCED AIR/ AC COOL      ","BX",
	"FORCED AIR/ EVAP         ","BE",
	"FORCED AIR/ EVAP COOL    ","BE",
	"FORCED AIR/AC            ","BX",
	"FORCED AIR/CENREF        ","ZA",
	"FORCED AIR/EVAP          ","BE",
	"FORCED AIR/EVAP COOL     ","BE",
	"FORCED AIR/EVAP/FP       ","BE",
	"FORCED AIR/HUM           ","BF",
	"FORCED AIR/HUMID         ","BF",
   "FORCED AIR/HUMIDIFIER    ","BF",
	"FORCED AIR/REF           ","BA",
	"FORCED AIR/REF COOL      ","BA",
	"FORCED AIR/REFRIG        ","BA",
	"FORCED CENTRAL COOLER    ","BA",
	"FORCED COOLER            ","BG",
	"FORCED EVAP              ","BE",
	"FORCED GRAVITY + HUMID   ","BA",
	"FORCED H & C             ","BX",
	"FORCED H&C               ","BX",
	"FORCED HE/EVAP COOL      ","BE",
	"FORCED HEAT              ","B ",
	"FORCED HEAT & AIR        ","BX",
	"FORCED HEAT & COOL       ","BG",
	"FORCED HEAT & EVAP       ","BE",
	"FORCED HEAT + A/C COOL   ","BX",
	"FORCED HEAT / WOOD STOVE ","B ",
	"FORCED HEAT A/C          ","BX",
	"FORCED HEAT A/C COOL     ","BX",
	"FORCED HEAT EVAP COOL    ","BE",
	"FORCED HEAT EVAPCOOL     ","BE",
	"FORCED HEAT WINDOW A/C   ","BX",
	"FORCED HEAT/ HUMID       ","BF",
	"FORCED HEAT/EVAP COOL    ","BE",
	"FORCED HEAT/EVAP COOLER  ","BE",
	"FORCED HEAT/HUMID        ","BF",
	"FORCED HEAT/REF          ","BA",
	"FORCED HEAT/REF COOL     ","BA",
	"FORCED HEAT/ROOF COOL    ","BG",
	"FORCED HEAT;WALL;ROOF MT ","  ",
	"FORCED HEATING           ","B ",
	"FORCED HT                ","B ",
	"FORCED HT & EVAP         ","BE",
	"FORCED HT / EVAP COOL    ","BE",
	"FORCED HT / REF COOL     ","BA",
	"FORCED HT. A/C COOL      ","BX",
	"FORCED HT/EVAP COOL      ","BE",
	"FORCED HT/HUM COOL       ","BF",
	"FORCED HT/HUMID          ","BF",
	"FORCED HT/HUMID COOL     ","BF",
	"FORCED HT/REF COOL       ","BA",
	"FORCED HTG/AC            ","BX",
	"FORCED HUMID             ","BF",
	"FORCED RED               ","BA",
	"FORCED REF               ","BA",
	"FORCED REF (2)           ","BA",
	"FORCED REF.              ","BA",
	"FORCED REFRIG            ","BA",
	"FORCED UNIT              ","BA",
	"FORCED'EVAP              ","BE",
	"FORCED+HUMID             ","BF",
	"FORCED, EVAP             ","BE",
	"FORCED, EVAP.            ","BE",
	"FORCED,CENTRAL           ","ZC",
	"FORCED,COOLING,DUAL PAK  ","WD",
	"FORCED,HUMID             ","BF",
	"FORCED-3 TON             ","BA",
	"FORCED-COOLER            ","BG",
	"FORCED-COOLING           ","BG",
	"FORCED-DOWN FLOW         ","  ",
	"FORCED-DUO PAK           ","WD",
	"FORCED-EVAP              ","BE",
	"FORCED-HUM               ","BF",
	"FORCED-HUMID             ","BF",
	"FORCED-REF               ","BA",
	"FORCED-REFRIG.           ","BA",
	"FORCED-WALL UNIT         ","B ",
	"FORCED/ 2 COOLERS        ","BG",
	"FORCED/ A/C              ","BX",
	"FORCED/ COOLER           ","BG",
	"FORCED/ EVAP             ","BE",
	"FORCED/ HUM              ","BF",
	"FORCED/ HUMID            ","BF",
	"FORCED/ HUMID (2)        ","BF",
	"FORCED/ REF              ","BA",
	"FORCED/ ROOF COOLER      ","BR",
	"FORCED/ WALL DUAL        ","WL",
	"FORCED/3 SM UNITS        ","  ",
	"FORCED/3 TON             ","BA",
	"FORCED/3T A/C            ","BA",
	"FORCED/3TON              ","BA",
	"FORCED/AC                ","ZX",
	"FORCED/CENT              ","ZC",
	"FORCED/CENTRAL           ","ZC",
	"FORCED/CENTRAL AIR       ","ZC",
	"FORCED/COOELR            ","BG",
	"FORCED/COOL/HUMID        ","BG",
	"FORCED/COOLER            ","BG",
	"FORCED/COOLING           ","BG",
	"FORCED/COOLING/SWAMP     ","BG",
	"FORCED/DAY & NIGHT       ","B ",
	"FORCED/EVAP              ","BE",
	"FORCED/FP                ","  ",
	"FORCED/HEAT PUMP COOLER  ","BH",
	"FORCED/HEAT PUMP/REF     ","GA",
	"FORCED/HUM               ","BF",
	"FORCED/HUMID             ","BF",
	"FORCED/HUMID 3T&2T IN OF ","BF",
	"FORCED/HUMID A/C         ","BF",
	"FORCED/HUMOD/COOLING     ","BG",
	"FORCED/HUNID             ","BF",
	"FORCED/REF               ","BA",
	"FORCED/REF 3TON          ","BA",
	"FORCED/REF RF            ","BA",
	"FORCED/REF WINDOW        ","BA",
	"FORCED/REF.              ","BA",
	"FORCED/REFR              ","BA",
	"FORCED/REFRIG            ","BA",
	"FORCED/REFRIGERATION     ","BA",
	"FORCED/RF                ","BR",
	"FORCED/ROOF COOLER       ","BR",
	"FORCED/ROOM A/C          ","BX",
	"FORCED/SWAMP             ","BE",
	"FORCED/WALL              ","BL",
	"FORCED/WALL COOLER       ","BL",
	"FORCED/WALL REF          ","BA",
	"FORCED/WALL UNIT/ A/C    ","BA",
	"FORCED/WALL;CENTRAL      ","BZ",
	"FORCED/ZONE; EVAP        ","TE",
	"FORCED: REF              ","BA",
	"FORCED;  REF             ","BA",
	"FORCED; AC               ","BX",
	"FORCED; CENTRAL          ","BZ",
	"FORCED; CENTRAL AC       ","BZ",
	"FORCED; EVAP             ","BE",
	"FORCED; EVAP; CENTRAL    ","ZE",
	"FORCED; EVP              ","BE",
	"FORCED; REF              ","BA",
	"FORCED; REF.             ","BA",
	"FORCED; REFG             ","BA",
	"FORCED; REFG.            ","BA",
	"FORCED; REFIG.           ","BA",
	"FORCED; REFRG.           ","BA",
	"FORCED; REFRIG           ","BA",
	"FORCED; REFRIG.          ","BA",
	"FORCED; REFRIGERATION    ","BA",
	"FORCED; REGRIG           ","BA",
	"FORCED;AC                ","ZX",
	"FORCED;AC/EVAP           ","ZX",
	"FORCED;CENTRAL           ","ZX",
	"FORCED;EVAP              ","BE",
	"FORCED;REF               ","BA",
	"FORCED;REF DUO PAK       ","WD",
	"FORCED;REF.              ","BA",
	"FORCED;REFRIG            ","BA",
	"FORCEDHEAT A/C COOL      ","BX",
	"FORCEDHEAT/ROOF COOL     ","BG",
	"FORCEDL AC               ","BX",
	"FORCEED                  ","B ",
	"FORCEED/REF              ","BA",
	"FORD'D, HUMID            ","BF",
	"FORECED; AC              ","BX",
	"FORED / EVAP             ","BE",
	"FORED / REF              ","BA",
	"FORED HEAT               ","B ",
	"FOUR FAU/AC ONE PER UNIT ","BX",
	"FPLC/A/C UNIT            ","XX",
	"FR HT / AK               ","BX",
	"FRC HEAT/REF, WH FAN     ","BA",
	"FRC'D                    ","B ",
	"FRCD                     ","B ",
	"FRCD & REF               ","BA",
	"FRCD & REFRIG            ","BA",
	"FRCD /EVAP               ","BE",
	"FRCD HEAT                ","B ",
	"FRCD HEAT,HUM            ","BF",
	"FRCD HEAT,REF            ","BA",
	"FRCD HEAT/EVAP           ","BE",
	"FRCD HEAT/HUM            ","BF",
	"FRCD HEAT/HUMID          ","BF",
	"FRCD HEAT/REF            ","BA",
	"FRCD HEAT/REF & HUMID    ","BA",
	"FRCD HEAT/REF/HUMID      ","BA",
	"FRCD HET/REF             ","BA",
	"FRCD HT/HUMID            ","BF",
	"FRCD HT/REF              ","BA",
	"FRCD REF                 ","BA",
	"FRCD'REF                 ","BA",
	"FRCD/                    ","B ",
	"FRCD/ REF                ","BA",
	"FRCD/2REF                ","BA",
	"FRCD/4TON AC             ","BX",
	"FRCD/?                   ","B ",
	"FRCD/AC                  ","BX",
	"FRCD/EST EVAP            ","BE",
	"FRCD/EVAP                ","BE",
	"FRCD/HEAT PUMP           ","GH",
	"FRCD/HTR/COOLING-HUMID   ","BF",
	"FRCD/REF                 ","BA",
	"FRCD/REF/EVAP            ","BA",
	"FRCE/REF                 ","BA",
	"FRCH HEAT/REF            ","BA",
	"FROCED; REF              ","BA",
	"FUA-PAK                  ","WD",
	"FUA/AC                   ","BX",
	"FUA/EVAP                 ","BE",
	"FURNACE                  ","M ",
	"FURNACE-EVP COOLER       ","ME",
	"FURNANCE/ 5 TON AC       ","MX",
	"FWA/COOLER               "," G",
	"GAS DUL                  ","WD",
	"GAS HEAT - A/C           ","NX",
	"GAS HEAT/EVAP            ","NE",
	"GAS STOVE                ","N ",
	"GAS WALL HT. ONLY        ","D ",
	"GAS/EVAP                 ","  ",
	"GENERAL AIR              ","Z ",
	"GFWA                     ","A ",
	"GFWA / AC                ","ZX",
	"GFWA / CENTRAL COOLING   ","ZC",
	"GFWA / COOLER            ","AG",
	"GFWA / ROOF A/C          ","AR",
	"GFWA / ROOF COOLER       ","AR",
	"GFWA / WATER COOLER      ","AZ",
	"GFWA/ A/C                ","ZX",
	"GFWA/ AC                 ","ZX",
	"GFWA/ COOLER             ","AG",
	"GFWA/AC                  ","ZX",
	"GFWA/ATTIC AC            ","ZX",
	"GFWA/CENTRAL A/C         ","ZC",
	"GFWA/COOLER              ","AG",
	"GFWA/WALL AC             ","AL",
	"GFWA/WALL COOLER         ","AL",
	"GFWAL/AC                 ","AX",
	"GRAVITY                  ","A ",
	"GRAVITY / COOLING        ","AG",
	"GRAVITY / HUMID          ","AF",
	"GRAVITY FLOOR UNIT       ","A ",
	"GRAVITY HEAT             ","A ",
	"GRAVITY HEAT/REF         ","AA",
	"GRAVITY/COOLER           ","AG",
	"GRAVITY/EVAP             ","AE",
	"GRAVITY/HUM              ","AF",
	"GRAVITY/HUMID            ","AF",
	"GRAVITY/WALL COOL        ","AL",
	"GRAVITY/WIN A/C          ","AW",
	"GRAVITY/ZONE UNIT        ","A ",
	"GRAVITY; EVAP            ","AE",
	"GROUND MOUNTED DUO PAK   ","BD",
	"H & C IN OFFICE, MO/HO   ","ZX",
	"H&C                      ","ZX",
	"H&C 4 RES/OFF/ & CARD RM ","ZX",
	"H&C IN INTERIOR OFFICES  ","ZX",
	"H&C IN OFFICE            ","ZX",
	"H&C IN OFFICE / SHOWROOM ","ZX",
	"H&C IN OFFICE AREA       ","ZX",
	"H&C IN OFFICE AREAS      ","ZX",
	"H&C IN OFFICE CONV.      ","ZX",
	"H&C IN OFFICE/SALES AREA ","  ",
	"H&C IN OFFICE; SHOP EVAP ","ZX",
	"H&C IN OFFICES           ","ZX",
	"H&C IN OFFICES & % WHSE. ","ZX",
	"H&C IN RES, OFFICE       ","  ",
	"H&C IN RETAIL/OFFICE AREA","ZX",
	"H&C SYSTEM & EVAP        ","ZE",
	"H/C                      ","ZX",
	"H/C DESCR HEAT PUMP      ","GH",
	"H;UMID COOL-WALL HEAT    ","DF",
	"H;UMID COOLER            "," F",
	"HALL HEAT EVAP COOL      ","  ",
	"HCAT PUMP                ","GH",
	"HEAT                     ","Y ",
	"HEAT  PUMP               ","  ",
	"HEAT & AC                ","YX",
	"HEAT & COOL              ","YX",
	"HEAT & HUMID COOL        ","YF",
	"HEAT + EVAP              ","YE",
	"HEAT FLOOR               ","  ",
	"HEAT ONLY                ","Y ",
	"HEAT ONLY - FLOOR        ","  ",
	"HEAT ONLY PLUS WOOD STOVE","  ",
	"HEAT PAK                 ","  ",
	"HEAT PUMP                ","GH",
	"HEAT PUMP & DUO-PAK UNITS","GD",
	"HEAT PUMP & EVAP         ","GE",
	"HEAT PUMP (2)            ","GH",
	"HEAT PUMP + EVAP         ","GE",
	"HEAT PUMP - A/C          ","GX",
	"HEAT PUMP / 4 TON REF    ","GA",
	"HEAT PUMP / REF          ","GA",
	"HEAT PUMP 3HP            ","GH",
	"HEAT PUMP A/C            ","GX",
	"HEAT PUMP AND A/C        ","GX",
	"HEAT PUMP FORCD          ","GH",
	"HEAT PUMP IN OFFICE AREA ","GH",
	"HEAT PUMP REF/FA         ","BA",
	"HEAT PUMP UNITS          ","GH",
	"HEAT PUMP,ON ROOF        ","  ",
	"HEAT PUMP/AC             ","GX",
	"HEAT PUMP/EVAP           ","GE",
	"HEAT PUMP/FA/RET         ","GA",
	"HEAT PUMP/FORCD          ","BH",
	"HEAT PUMP/REF            ","GA",
	"HEAT PUMP/REFRIG         ","GA",
	"HEAT PUMPS               ","GH",
	"HEAT, COOL               ","YX",
	"HEAT,COOL                ","YX",
	"HEAT,HUMID,WALL          ","  ",
	"HEAT,WALL,COOL,HUMID     ","  ",
	"HEAT-COOLING             ","  ",
	"HEAT-FLOOR /AIR-BUILT IN ","CX",
	"HEAT-FLR / AIR-ROOF      ","CR",
	"HEAT-PUMP                ","GH",
	"HEAT-WALL UNIT/ AIR-ROOF ","DR",
	"HEAT/;COOL/FORCD/CEN     ","  ",
	"HEAT/COOL                ","YX",
	"HEAT/COOL+ZONE UNIT      ","  ",
	"HEAT/COOL/(2)WALL UNIT   ","  ",
	"HEAT/COOL/FORCD          ","  ",
	"HEAT/COOL/FORCD/HUMI     ","  ",
	"HEAT/COOL/FORCD/HUMID    ","  ",
	"HEAT/COOL/FORCED         ","  ",
	"HEAT/COOL/FORCED/HUMID   ","  ",
	"HEAT/COOL/HUMID          ","  ",
	"HEAT/COOL/HUMID/WALL UNIT","  ",
	"HEAT/COOL/HUMID/ZONE UNIT","  ",
	"HEAT/COOL/REF.           ","  ",
	"HEAT/COOL/SERVEL         ","  ",
	"HEAT/COOL/WALL UNIT      ","  ",
	"HEAT/COOL/WALL UNIT/HUMID","  ",
	"HEAT/EVAP                ","YE",
	"HEAT/FORCD/HUMID/CENTRL  ","  ",
	"HEAT/FORCED/HUMID        ","  ",
	"HEAT/ZONE UNIT           ","  ",
	"HEAT; COOL; FORCD        ","  ",
	"HEAT; COOL; WALL UNIT;HUM","  ",
	"HEATED FLOOR PIPES/CENREF","  ",
	"HEATER                   ","X ",
	"HEATER & HUMID COOLER    ","XF",
	"HEATER . HUMID           ","XF",
	"HEATER AND HUMID COOLER  ","XF",
	"HEATER-WALL / AIR-ROOF   ","DR",
	"HEATER/HUMID COOLER      ","XF",
	"HEATERS & HUMID COOL     ","XF",
	"HEATING                  ","  ",
	"HEATING COOLING          ","  ",
	"HEATING COOLING FLOOR UNI","  ",
	"HEATING COOLING HUMID WAL","  ",
	"HEATING COOLING WALL UNIT","  ",
	"HEATING COOLING,FORCED,RE","  ",
	"HEATING FLOOR UNIT       ","  ",
	"HEATING GRAVITY          ","A ",
	"HEATING HUMID FORCD AIR/H","  ",
	"HEATING,COOLING          ","  ",
	"HEATING,COOLING,FORCED,CE","  ",
	"HEATING,COOLING,FORCED,HU","  ",
	"HEATING,COOLING,HUMID    ","  ",
	"HEATING,COOLING,HUMID,FLO","  ",
	"HEATING,COOLING,HUMID,FLR","  ",
	"HEATING,FORCED,          ","  ",
	"HEATING,WALL,HUMID       ","  ",
	"HEATING-FLR / AIR-ROOF   ","CR",
	"HEATING-WALL/AIR-ROOF    ","DR",
	"HEATING-ZONE UNIT        ","  ",
	"HEATING/COOLER/FLOOR UNIT","  ",
	"HEATING/COOLING          ","  ",
	"HEATING/COOLING/AC       ","  ",
	"HEATING/COOLING/DUAL PAK ","WD",
	"HEATING/COOLING/DUO PAK  ","WD",
	"HEATING/COOLING/FLOOR UNI","  ",
	"HEATING/COOLING/FLRUNIT/W","  ",
	"HEATING/COOLING/FORCED/CE","  ",
	"HEATING/COOLING/FORCED/HU","  ",
	"HEATING/COOLING/FORCED/RE","  ",
	"HEATING/COOLING/HEAT PUMP","  ",
	"HEATING/COOLING/HUMID    ","  ",
	"HEATING/COOLING/HUMID/WAL","  ",
	"HEATING/COOLING/WALLUNIT ","  ",
	"HEATING/FLOORUNIT        ","  ",
	"HEATING/FORCED/CENTRAL   ","  ",
	"HEATING/WALLUNIT/HUMID   ","  ",
	"HEATING/ZONE UNIT        ","  ",
	"HEATPUMP                 ","GH",
	"HEATPUMP HEAT & COOL     ","  ",
	"HEATPUMPS                ","GH",
	"HGUMID / F.A.            ","B ",
	"HIMID COOL               "," F",
	"HIMID COOL FORCED HEAT   ","  ",
	"HIMID/WALLUNIT           ","D ",
	"HOT WATER PIPES IN FLOOR ","E ",
	"HP                       ","GH",
	"HP - A/C                 ","GH",
	"HP/AC                    ","  ",
	"HRST PUMP                ","GH",
	"HT PUMP                  ","GH",
	"HT PUMP / COOLER         ","GG",
	"HT PUMP FA / REF         ","GA",
	"HT PUMP FA.REF           ","GA",
	"HT PUMP FA/REF           ","GA",
	"HT PUMP FA/RET           ","GA",
	"HT PUMP REF/ FA          ","GA",
	"HT PUMP REF/FA           ","GA",
	"HT PUMP RET/FA           ","GA",
	"HT PUMP/FA / RET         ","GA",
	"HT PUMP/FORCED           ","GH",
	"HT PUMP/REF/FA           ","GA",
	"HT.PUMPS;B'BOARD/WALL REF","  ",
	"HT/EVAP                  ","XE",
	"HTG-WALL UNIT/AIR-ROOF   ","DR",
	"HTPUMP FA / REF          ","GA",
	"HTPUMP FA/REF            ","GA",
	"HTR-FLR UNIT / A/C-ROOF  ","CR",
	"HTR-FLR UNIT / AIR-ROOF  ","CR",
	"HTR-WAL UNIT / COOL-ROOF ","DR",
	"HUM                      "," F",
	"HUM & DUO PAK            ","WD",
	"HUM & F/A                ","B ",
	"HUM & WIN A/C            "," W",
	"HUM & ZONE UNIT          ","T ",
	"HUM (2)                  ","  ",
	"HUM (4)                  ","  ",
	"HUM (5)                  ","  ",
	"HUM - 3                  ","  ",
	"HUM - DUO PAK            ","WD",
	"HUM - FLOOR              ","C ",
	"HUM - FLOOR UNIT         ","C ",
	"HUM - FORCED             ","B ",
	"HUM - HAH                ","B ",
	"HUM - RADIATORS          ","H ",
	"HUM - WALL               ","D ",
	"HUM - WALL UNIT          ","  ",
	"HUM - WINDOW             ","  ",
	"HUM - ZONE               ","T ",
	"HUM - ZONE UNIT          ","T ",
	"HUM / FLOOR              ","C ",
	"HUM / FLOOR UNIT         ","C ",
	"HUM / FORCED             ","B ",
	"HUM / WALL               ","  ",
	"HUM / WALL HEATER        ","D ",
	"HUM / WINDOW A/C         "," W",
	"HUM /FLOOR HEATER        ","C ",
	"HUM COOL - WALL HEAT     ","DF",
	"HUM FLOOR                ","C ",
	"HUM REF                  ","  ",
	"HUM WALL                 ","  ",
	"HUM WALL HEAT            ","D ",
	"HUM&FLOOR FURNACE        ","C ",
	"HUM&WALL HEATER          ","D ",
	"HUM+WALL                 ","D ",
	"HUM+WALL AC+FLOOR HEAT   ","CL",
	"HUM, FA                  ","  ",
	"HUM- FLOOR UNIT          ","C ",
	"HUM-FA                   ","B ",
	"HUM-FLOOR                ","  ",
	"HUM-FORCED               ","B ",
	"HUM/ A/C                 "," X",
	"HUM/ FA                  ","B ",
	"HUM/ WALL HEATER         ","  ",
	"HUM/ WALL HTR            ","  ",
	"HUM/ WIN A/C             "," W",
	"HUM/COOLER               ","  ",
	"HUM/COOLER/FA            ","BG",
	"HUM/FA                   ","B ",
	"HUM/FA/RF                ","  ",
	"HUM/FAH                  ","B ",
	"HUM/FLOOR                ","C ",
	"HUM/FLOOR FURNACE        ","C ",
	"HUM/FLOOR FURNANCE       ","C ",
	"HUM/FLOOR UNIT           ","C ",
	"HUM/FLR UNIT             ","C ",
	"HUM/REF                  "," A",
	"HUM/WALL                 ","D ",
	"HUM/WALL A/C             "," L",
	"HUM/WALL HEATER          ","D ",
	"HUM/WALL HTR             ","D ",
	"HUM/WALL UNIT            ","D ",
	"HUM/ZONE                 ","T ",
	"HUM/ZONE UNIT            ","T ",
	"HUM/ZONE UNITS/FLOOR UNIT","C ",
	"HUMI                     ","  ",
	"HUMID                    ","  ",
	"HUMID  COOLER            "," F",
	"HUMID (2)                ","  ",
	"HUMID (5)                ","  ",
	"HUMID / F.A.             ","B ",
	"HUMID / FP               ","  ",
	"HUMID COLERS             "," F",
	"HUMID COOL               "," F",
	"HUMID COOL & FAU         ","BF",
	"HUMID COOL WALL HEAT     ","  ",
	"HUMID COOL- HEATERS      ","XF",
	"HUMID COOL- WALL HEATER  ","DF",
	"HUMID COOL-FAU           ","BF",
	"HUMID COOL-FAU HEAT      ","BF",
	"HUMID COOL-FLOOR HEAT    ","CF",
	"HUMID COOL-GRAVITY HEAT  ","AF",
	"HUMID COOL-SPACE HEATERS ","JF",
	"HUMID COOL-SUSPENDED HEAT","JF",
	"HUMID COOL-WALL HEAT     ","DF",
	"HUMID COOL-WALL HEATER   ","DF",
	"HUMID COOL/ ZONE HEAT    ","TF",
	"HUMID COOL/FLOOR HEAT    ","CF",
	"HUMID COOL/FLOOR HEATER  ","CF",
	"HUMID COOL/WALL HEAT     ","DF",
	"HUMID COOL=WALL HEAT     ","DF",
	"HUMID COOLER             "," F",
	"HUMID COOLER & FAU       ","BF",
	"HUMID COOLER-WALL HEAT   ","DF",
	"HUMID COOLER-WALL HEATER ","DF",
	"HUMID COOLER-WALL HEATERS","DF",
	"HUMID COOLER/FA          ","  ",
	"HUMID COOLER/ZONE HEATER ","TF",
	"HUMID COOLERS            "," F",
	"HUMID FORCED             ","  ",
	"HUMID RF / WALL UNIT     ","D ",
	"HUMID WALL UN            ","  ",
	"HUMID WALL UNIT          ","  ",
	"HUMID WIND               ","  ",
	"HUMID ZONE UNIT          ","T ",
	"HUMID(WALL+FLOOR)        ","  ",
	"HUMID, FLRUNIT           ","C ",
	"HUMID,FORC'D             ","  ",
	"HUMID,ZONE               ","T ",
	"HUMID,ZONE UNIT          ","T ",
	"HUMID- WALL UNIT         ","  ",
	"HUMID-2 WALL UNIT        ","  ",
	"HUMID-2 WALL UNITS       ","  ",
	"HUMID-COOLING            ","  ",
	"HUMID-FA                 ","  ",
	"HUMID-FLOOR              ","C ",
	"HUMID-FLOOR UNIT         ","C ",
	"HUMID-FLOORUNIT          ","  ",
	"HUMID-FOOLR UNIT         ","  ",
	"HUMID-FORCED             ","B ",
	"HUMID-FORDCED            ","  ",
	"HUMID-WALL               ","D ",
	"HUMID-WALL UNIT          ","D ",
	"HUMID-WALLUNIT           ","  ",
	"HUMID-WINDOW             ","  ",
	"HUMID-ZONE UNIT          ","T ",
	"HUMID.                   ","  ",
	"HUMID.,F.A.              ","  ",
	"HUMID./WINDOW AC         ","  ",
	"HUMID/ FLOOR UNIT        ","C ",
	"HUMID/ FORC'D            ","  ",
	"HUMID/ FORCD             ","B ",
	"HUMID/ WALL HEATER       ","  ",
	"HUMID/COOL/HEAT/WALL UNIT","  ",
	"HUMID/F.A.               ","B ",
	"HUMID/FA                 ","  ",
	"HUMID/FLOOD UNIT         ","  ",
	"HUMID/FLOOR              ","  ",
	"HUMID/FLOOR FURNACE      ","  ",
	"HUMID/FLOOR FURNANCE     ","  ",
	"HUMID/FLOOR UNIT         ","  ",
	"HUMID/FLR UNIT           ","C ",
	"HUMID/FORC'D             ","B ",
	"HUMID/FORCD              ","B ",
	"HUMID/FORCD (ALL 3 HSE)  ","  ",
	"HUMID/FORCDE             ","  ",
	"HUMID/FORCED             ","B ",
	"HUMID/FORCED AIR         ","B ",
	"HUMID/FORCED AIR/ROOF    ","  ",
	"HUMID/REF                ","ZA",
	"HUMID/REFRIG             ","ZA",
	"HUMID/WALL               ","D ",
	"HUMID/WALL HEAT          ","D ",
	"HUMID/WALL UNIT          ","D ",
	"HUMID/WALL UNIT DUAL     ","  ",
	"HUMID/ZONE               ","  ",
	"HUMID/ZONE UNIT          ","T ",
	"HUMID/ZONE UNITS         ","  ",
	"HUMID/ZONE/ROOF          ","  ",
	"HUMIDFIER                ","  ",
	"HUMIDIFIER (ZONE)        ","  ",
	"HUMIS COOL-FAU           ","BF",
	"HUMOD COOLER             "," F",
	"HUNMID                   ","  ",
	"HVAC                     ","  ",
	"HYDRONIC/REF             ","  ",
	"IN OFFICE                ","  ",
	"IN OFFICE AREAS          ","  ",
	"INDIVIDUAL H&C/UNIT      ","XX",
	"INDIVIDUAL HEAT PUMPS    ","  ",
	"INDIVIDUAL RM. UNITS     ","U ",
	"JEN AIR                  ","  ",
	"KA HEAT & COOL           "," G",
	"KING AIR/AMMONIA COIL SYS"," A",
	"LOOR / HUMID             ","CF",
	"LOOR HEAT                ","C ",
	"LOOR HEAT-HUMID COOL     ","CF",
	"LOOR HEATER-HUMID COOLER ","CF",
	"LOOR HEATER-HUMID COOLERS","CF",
	"LOOR/EVAP                ","  ",
	"LOOR/EVAP.               ","  ",
	"MAIN FAH/CENTRAL         ","ZC",
	"MAIN HOUSE FAU/CENT      ","ZC",
	"MANAGERS UNIT ONLY       ","  ",
	"MH INCL. F/A HT. & REFR. ","BA",
	"MOTEL WALL UNITS         ","  ",
	"MULTIPLE DUO-PAKS & REZ. ","WD",
	"N                        ","L ",
	"N/A                      ","L ",
	"NA                       ","L ",
	"NO COOLING/ WALL HEATER  ","DN",
	"NO HEAT/ ROOF AC         ","LR",
	"NO HEAT/EVAP             ","  ",
	"NO HEAT;EVAP             ","  ",
	"NONE                     ","L ",
	"NONE/HUMID               ","  ",
	"NOT MARKED               ","L ",
	"OFFICE                   ","  ",
	"OFFICE A/C               "," O",
	"OFFICE A/C & SHOP EVAP   ","  ",
	"OFFICE A/C + 58 ROOF EVAP","  ",
	"OFFICE A/C; EVAP + REZNOR"," E",
	"OFFICE A/C; EVAP IN WHSE "," E",
	"OFFICE AC; 6 EVAP, 5 ZONE","  ",
	"OFFICE AREA              "," O",
	"OFFICE DUO-PAK;EVAP&HEAT ","WD",
	"OFFICE H&C               ","  ",
	"OFFICE ONLY              "," O",
	"OFFICES ONLY             "," O",
	"OIL HEATER               ","X ",
	"ONE;WALL                 ","  ",
	"OOLER/FAH                ","BG",
	"OOLER/FLOOR HEATER       ","CG",
	"OOLER/WALL HEATER        ","DG",
	"PANEL RAY / COOLER       ","IG",
	"PANEL/HUMID              ","I ",
	"PELLET STOVE/FANS        ","  ",
	"PERIMETER                ","X ",
	"PERIMETER/REF            ","XA",
	"PERM                     ","X ",
	"POR. = CENTRAL H&C       ","  ",
	"PP COOLER/(3) HEATERS    ","  ",
	"PREM                     ","X ",
	"PREM H&C                 ","XX",
	"PRIMETER                 ","X ",
	"PROPANE WOOD STOVE       ","  ",
	"PUO PAC                  ","WD",
	"PUO PAK                  ","  ",
	"PUO PAK/EVAP             ","  ",
	"Q/C                      ","ZX",
	"QALL HEATER-HUMID COOLER ","DF",
	"RAD'NT/EVAP              ","  ",
	"RAD'NT/WALL A/C          ","  ",
	"RAD/ROOF ACS             ","  ",
	"RAD;EVAP                 ","  ",
	"RADIANT FLOOR/REF        ","  ",
	"RADIANT HEAT & EVAP COOL ","  ",
	"RADIANT HEAT & WINDOW A/C","IW",
	"RADIANT HEAT-HUMID COOLER","IF",
	"RADIANT HT. & EVAP. COOL ","IE",
	"RADIANT/EVAP             ","IE",
	"RADIANT/EVAP/REFR.       ","IE",
	"RADIENT HEAT             ","I ",
	"RADN,T;EVAP              ","  ",
	"RCD HEAT/HUM             ","  ",
	"RCD/REF                  ","  ",
	"RE-FORCED                ","  ",
	"REF                      "," A",
	"REF & HUM                "," B",
	"REF (2)                  "," A",
	"REF - DUO PAK            ","WA",
	"REF - FORCED             ","BA",
	"REF - HUM - WLL          ","DA",
	"REF - WALL               ","DA",
	"REF - WALL UNIT          ","DA",
	"REF / F/A                ","BA",
	"REF / FAH                ","BA",
	"REF / FLOOR              ","CA",
	"REF / FORC'D             ","BA",
	"REF / FORCED             ","BA",
	"REF / HUMID              "," B",
	"REF 3 TON                "," A",
	"REF 4 TON                ","  ",
	"REF A/C /FORCED          ","BA",
	"REF A/C WALL HEAT        ","  ",
	"REF AIR                  ","ZA",
	"REF COOL                 "," A",
	"REF COOL-FORCED          ","  ",
	"REF COOL/ FORCED AIR HT  ","BA",
	"REF COOLING              "," A",
	"REF F/A                  ","BA",
	"REF FA                   ","BA",
	"REF FAH                  ","BA",
	"REF FORCED               ","BA",
	"REF-FA                   ","BA",
	"REF-FLOOR UNIT           ","  ",
	"REF-FORCED               ","BA",
	"REF-FORCED AIR           ","BA",
	"REF-FORCEDQ              "," A",
	"REF-FROCED               "," A",
	"REF-HUMID                "," B",
	"REF-WALL UNIT            "," A",
	"REF.                     "," A",
	"REF. & FAH               ","BA",
	"REF./6 TON               "," A",
	"REF./FORCD               ","BA",
	"REF./HOT WTR PIPES IN FLR","EA",
	"REF./FOR.                ","BA",
	"REF/ CENTRAL             ","ZA",
	"REF/ F/A                 ","BA",
	"REF/ FORCED              ","BA",
	"REF/3 TON                "," A",
	"REF/DUO PAK              ","WA",
	"REF/F/A                  ","BA",
	"REF/FA                   ","BA",
	"REF/FAH                  ","BA",
	"REF/FAU                  ","BA",
	"REF/FOR                  ","BA",
	"REF/FORCD                ","BA",
	"REF/FORCED               ","BA",
	"REF/FORCED AIR           ","BA",
	"REF/WIN                  "," A",
	"REFG/HUMID               "," A",
	"REFIG                    "," A",
	"REFIG & EVAP             "," B",
	"REFIG& FAN               "," A",
	"REFIG, FORCED, WD STOVE  ","SA",
	"REFR                     "," A",
	"REFR.                    "," A",
	"REFR. & EVAP COOL        "," B",
	"REFR.A/C, WALL HEAT      "," A",
	"REFRG.                   "," A",
	"REFRI. & FAH             ","BA",
	"REFRI./FAH               ","BA",
	"REFRI/FAH                ","BA",
	"REFRIG                   "," A",
	"REFRIG & FAH             ","BA",
	"REFRIG & FLOOR HEAT      ","CA",
	"REFRIG & FORCD AIR HEAT  ","BA",
	"REFRIG & FORCED AIR      ","BA",
	"REFRIG & FORCED AIR HEAT ","BA",
	"REFRIG & FORCED HEAT     ","BA",
	"REFRIG / FORCED AIR      ","BA",
	"REFRIG, FORCED AIR       ","BA",
	"REFRIG.                  "," A",
	"REFRIG. & FAH            ","BA",
	"REFRIG. AND FAU          ","BA",
	"REFRIG., WALL; EVAP      ","  ",
	"REFRIG./FAH              ","BA",
	"REFRIG./HEATING          ","XA",
	"REFRIG/FA                ","BA",
	"REFRIG/FAH               ","BA",
	"REFRIG/FAH/COOLER        ","BA",
	"REFRIG/FAU               ","BA",
	"REFRIG/FORCED            ","BA",
	"REFRIG/FORED AIR         ","BA",
	"REFRIG/FORCED AIR        ","BA",
	"REFRIGERATED COOLING     "," A",
	"REFRIGERATION            "," A",
	"REFRIGERATION 3 TON      "," A",
	"REFRIGERATION-WALL HEAT  ","DA",
	"REFRIGERATION/FLOOR HEAT ","CA",
	"REFRIGERATION/HUMID      "," A",
	"REFRIGERATON             "," A",
	"RES & OFFICE ONLY        ","  ",
	"RESNOR/AC AND EVAP       ","  ",
	"RESNOR/EVAP              ","  ",
	"RESNOR/WALL; EVAP        ","  ",
	"RETI FA                  ","B ",
	"REZ HEAT & EVAP          ","TE",
	"REZ HEAT & EVAP COOL     ","  ",
	"REZ HEAT/EVAP            ","  ",
	"REZ HEAT/EVAP/DUO-PAKS   ","  ",
	"REZ./F/A HEAT + EVAP     ","TE",
	"REZ/EVAP/DUO-PAK         ","  ",
	"REZNOR HEAT & 2 EVAP     ","TE",
	"REZNOR HEAT & EVAP COOL  ","TE",
	"REZNOR HEAT + EVAP COOL  ","TE",
	"REZNOR HT. & EVAP        ","TE",
	"REZNOR HT. & EVAP;AC     ","  ",
	"REZNOR, EVAP & DUO-PAK   ","  ",
	"REZNOR/EVAP WHSE/AC OFFIC","TE",
	"REZNOR/EVAP;DUO-PAK      ","WD",
	"REZNOR;EVAP              ","  ",
	"RF-HUMID                 ","  ",
	"RFRIG.                   ","  ",
	"ROOF                     "," R",
	"ROOF A/C                 "," R",
	"ROOF A/C WALL HEAT       ","DR",
	"ROOF AC                  "," R",
	"ROOF COOLER              "," R",
	"ROOF EVAP/WALL GAS HEAT  ","DR",
	"ROOF HEAT PUMP           ","GR",
	"ROOF HEATING & COOLING   ","XR",
	"ROOF MTN                 ","  ",
	"ROOF REF                 "," A",
	"ROOF REF/ HUMID          "," B",
	"ROOF UNIT                ","  ",
	"ROOF/HUMID               "," F",
	"ROOF/WALL                ","XX",
	"ROOM                     ","  ",
	"ROOM COOLER              "," G",
	"SALL HEAT & HUMID COOLER ","DF",
	"SALL HEAT-HUMID COOL     ","DF",
	"SALL HEAT-HUMID COOLER   ","DF",
	"SEE NOTES                ","  ",
	"SERVEL                   ","  ",
	"SERVEL H&C 3TON          ","XX",
	"SFWD/ A/C                ","XX",
	"SHOP                     ","  ",
	"SOLAR                    ","  ",
	"SOLAR H&C                ","KJ",
	"SOLAR HEAT               ","K ",
	"SOLAR HEAT/REF           ","KA",
	"SP. HEAT & EVAP          ","JE",
	"SPACE HEAT / EVAP        ","JE",
	"SPACE HEAT EVAP COOL     ","JE",
	"SPACE HEAT EVAP COPOL    ","JE",
	"SPACE HEAT/EVAP COOL     ","JE",
	"SPACE HEATER             ","J ",
	"SPACE HEATER & EVAP      ","JE",
	"SPACE HEATER - EVAP      ","JE",
	"SPACE HEATERS            ","J ",
	"SPACE/EVAP.              ","JE",
	"SPLIT                    ","  ",
	"SPLIT A/C SYSTEM         ","XS",
	"SPLIT H & C SYSTEM       ","XS",
	"SPLIT SYS                ","  ",
	"STORAGE                  ","  ",
	"STOVE                    ","S ",
	"STOVE / COOLER           ","SG",
	"STOVE FOR HEAT           ","S ",
	"STOVE/ COOLER            ","SG",
	"STOVE/COOLER             ","SG",
	"STOVES / WALL COOLER     ","SL",
	"SUAL PAK                 ","WD",
	"SUSP. HEAT               ","O ",
	"SUSP. HEAT & 3 EVAP UNITS","OE",
	"SUSP. HEAT & EVAP        ","OE",
	"SUSP. HEAT/EVAP/WALL A/C ","OE",
	"SUSP. HEATER/EVAP        ","OE",
	"SUSP.HEATERS-HUMID COOL  ","OF",
	"SUSP;EVAP/WINDOW AC      ","OE",
	"SUSPENDED                ","O ",
	"SUSPENDED HEAT-HUMID COOL","OF",
	"SUSPENDED HEATERS        ","O ",
	"SUSPENED; EVAP           ","OE",
	"SWAMP                    "," E",
	"T                        ","  ",
	"T PUMP / 4 TON REF COOL  ","GA",
	"TON MULLER               ","  ",
	"TRIPLEX ONLY             ","  ",
	"TWO DUO PACS             "," D",
	"TWO DUO PAKS             "," D",
	"TWO DUOPAKS              "," D",
	"TWO LARGER UNITS HAVE H&C","33",
	"TWO WALL HEAT EVAP COOL  ","DE",
	"TWO WALL HEAT TWO EVAP   ","DE",
	"TWO WINDOW A/CONDS       "," W",
	"U                        ","  ",
	"UAL PAK                  "," D",
	"UDO PACK                 "," D",
	"UDO PAK                  "," D",
	"UFAU & HUMID COOLER      ","BF",
	"UK                       ","  ",
	"UMID/FLOOR UNIT          ","  ",
	"UNITS ON ROOF            ","  ",
	"UNK                      ","  ",
	"UNK.                     ","  ",
	"UNK./EVAP.               "," E",
	"UNK/EVAP.                "," E",
	"UNKNOWN                  ","  ",
	"UNKNOWN HT/THRUWALL AC   ","YL",
	"UNKNOWN/ALSO WOODSTOVE   ","S ",
	"UNKNOWN/SWAMP            "," E",
	"UNKOWN                   ","  ",
	"UNKWN                    ","  ",
	"UO PAK                   ","WD",
	"UOPAK                    ","  ",
	"UP FLOOR                 ","  ",
	"VARIOUS                  ","Y ",
	"VARIOUS H&C SYSTEMS      ","YX",
	"VARIOUS(3SEPERATE BUILD) ","  ",
	"W F EVAP                 "," E",
	"WA;; HEAT-HUMID COOLER   ","DF",
	"WA;; HEATER-HUMID COOLER ","DF",
	"WAA HT/EVAP              ","DE",
	"WAAL / COOLER            ","DG",
	"WAAL FURN/EVAP           ","  ",
	"WAAL-HUMID               ","  ",
	"WAAL/EVAP                ","  ",
	"WAALL UNIT/ HUMID        ","DF",
	"WAKK HEAT-HUMID COOLER   ","DF",
	"WAL EVAP                 "," E",
	"WAL HEAT EVAP COOL       ","DE",
	"WAL HEAT/HUM             ","DF",
	"WAL HEAT/HUMID           ","DF",
	"WAL; EVAP                ","DE",
	"WALL                     ","D ",
	"WALL  HEAT-HUMID COOL    ","DF",
	"WALL  HEATER             ","D ",
	"WALL & EVAP              ","DE",
	"WALL & F/A               ","D ",
	"WALL & FL. HEAT EVAP COOL","  ",
	"WALL & FL. HT. EVAP COOL ","  ",
	"WALL & FLOOR HEAT        ","C ",
	"WALL & FLOOR HEAT & EVAP ","  ",
	"WALL & FLOOR HT EVAP COOL","  ",
	"WALL & FLOOR HT. EVAP C. ","  ",
	"WALL & FLOOR UNIT/HUMID  ","CF",
	"WALL & FLR HT & EVAP     ","CE",
	"WALL & FLR. HEAT, EVAP   ","CE",
	"WALL & HUM               ","DF",
	"WALL & REF               ","DA",
	"WALL & SUSP. HEAT, EVAP  ","  ",
	"WALL & WINDOW AC/HUMID   ","  ",
	"WALL & ZONE HEAT; EVAP;AC","DE",
	"WALL (2)                 ","  ",
	"WALL (2) / HUMID         "," F",
	"WALL (8)                 ","  ",
	"WALL + FL HEAT EVAP COOL ","CE",
	"WALL + HUMID             ","  ",
	"WALL + WOODSTOVE/COOLER  ","  ",
	"WALL - A/C               "," X",
	"WALL - COOLER            "," G",
	"WALL - EVAP              "," E",
	"WALL - EVAPS             "," E",
	"WALL - FLOOR             ","C ",
	"WALL - FLOOR - EVAP      ","CE",
	"WALL - HUM               "," F",
	"WALL - REF               ","DA",
	"WALL - ROOF              ","DR",
	"WALL - UNIT              ","  ",
	"WALL . HUMID             "," F",
	"WALL / CENT              ","ZC",
	"WALL / COOLER            ","DG",
	"WALL / COOLERS           ","  ",
	"WALL / DUO PAK           ","WD",
	"WALL / EVAP              ","DE",
	"WALL / EVAP/ DUO PAK     ","WE",
	"WALL / FLOOR / HUMID     ","CF",
	"WALL / HUM               ","DF",
	"WALL / HUMID             "," F",
	"WALL / HUMID (2)         "," F",
	"WALL / HYMID             ","  ",
	"WALL / REF               "," A",
	"WALL / REFRIG            "," A",
	"WALL / WINDOW            "," W",
	"WALL / ZONE HEAT         ","D ",
	"WALL /EVAP               "," E",
	"WALL A/C                 "," L",
	"WALL A/C & FLOOR HEATER  ","CL",
	"WALL A/C & WALL HEATER   ","DL",
	"WALL A/C & WALL HEATERS  ","DL",
	"WALL A/C FLOOR UNIT      ","CL",
	"WALL A/C IN OFFICE,RDN'T "," L",
	"WALL A/C-FLOOR FURNACE   ","CL",
	"WALL A/C. FAH BROKEN.    ","  ",
	"WALL AC                  ","  ",
	"WALL AC & HEATER         ","DL",
	"WALL AC & WALL HEATER    ","DL",
	"WALL AC UNITS            "," L",
	"WALL AC/ZONE HEAT        ","  ",
	"WALL COOL                "," L",
	"WALL COOLER              "," L",
	"WALL EVAP                "," E",
	"WALL FRN/WALL AC         ","  ",
	"WALL FURN (2)/EVAP       ","  ",
	"WALL FURNACE             ","D ",
	"WALL FURNACE/COOLER      ","DG",
	"WALL FURNACE/HUMID       ","DF",
	"WALL H /COOLER           ","  ",
	"WALL H&C UNITS           ","  ",
	"WALL H&C; 1 HEAT PUMP    ","  ",
	"WALL H/ HUMID            ","  ",
	"WALL HEAAT-HUMID COOL    ","DF",
	"WALL HEAAT-HUMID COOLER  ","DF",
	"WALL HEAR/EVAP           ","DE",
	"WALL HEAT                ","D ",
	"WALL HEAT  EVAP COOL     ","DE",
	"WALL HEAT & (EST.) EVAP  ","DE",
	"WALL HEAT & EVAP         ","DE",
	"WALL HEAT & EVAP (EST.)  ","DE",
	"WALL HEAT & EVAP A/C     ","DE",
	"WALL HEAT & EVAP COOL    ","DE",
	"WALL HEAT & EVAP COOLER  ","DE",
	"WALL HEAT & HUMID COOL   ","DF",
	"WALL HEAT & HUMID COOLER ","DF",
	"WALL HEAT & PUMP         ","DH",
	"WALL HEAT & REF.         ","DA",
	"WALL HEAT & ROOF EVAP    ","DE",
	"WALL HEAT & WALL A/C     ","  ",
	"WALL HEAT & WINDOW A/C   ","  ",
	"WALL HEAT & WINDOW EVAP  ","DE",
	"WALL HEAT &EVAP COOL     ","DE",
	"WALL HEAT + (EST) WALL AC","DL",
	"WALL HEAT + 5T A/C       ","  ",
	"WALL HEAT + EVAP         ","DE",
	"WALL HEAT + EVAP (EST.)  ","DE",
	"WALL HEAT - EVAP         ","DE",
	"WALL HEAT - HUMID COOL   ","DF",
	"WALL HEAT - WIN A/C      ","DW",
	"WALL HEAT /  EVAP COOLER ","DE",
	"WALL HEAT / A/C          ","DX",
	"WALL HEAT / COOLER       ","DG",
	"WALL HEAT / EVAP         ","DE",
	"WALL HEAT / EVAP COOL    ","DE",
	"WALL HEAT / EVAP COOLER  ","DE",
	"WALL HEAT / HUMID        ","DF",
	"WALL HEAT / ROOF A/C     ","DR",
	"WALL HEAT /COOLER        ","DG",
	"WALL HEAT /EVAP          ","DE",
	"WALL HEAT A/C COOL       ","DX",
	"WALL HEAT AC COOL        ","DX",
	"WALL HEAT EEVAP COOL     ","  ",
	"WALL HEAT EVAP           ","DE",
	"WALL HEAT EVAP  COOL     ","DE",
	"WALL HEAT EVAP & DUO PAC ","  ",
	"WALL HEAT EVAP + A/C     ","  ",
	"WALL HEAT EVAP + A/C COOL","  ",
	"WALL HEAT EVAP COL       ","  ",
	"WALL HEAT EVAP COLL      ","DE",
	"WALL HEAT EVAP COOL      ","DE",
	"WALL HEAT EVAP DUO PAC   ","  ",
	"WALL HEAT EVAQP COOL     ","  ",
	"WALL HEAT HUMID          ","  ",
	"WALL HEAT HUMID COOLER   ","DF",
	"WALL HEAT ONLY           ","  ",
	"WALL HEAT PUMPS          ","  ",
	"WALL HEAT VENTED TO ROOMS","D ",
	"WALL HEAT WALL A/C       ","  ",
	"WALL HEAT WINDOW AC      ","  ",
	"WALL HEAT ZONE HEAT EVAP ","  ",
	"WALL HEAT&HUM            ","  ",
	"WALL HEAT, EVAP COOL     ","DE",
	"WALL HEAT, HUMID         ","DF",
	"WALL HEAT, WALL A/C      ","DL",
	"WALL HEAT, WALL AC       ","DL",
	"WALL HEAT,EVAP,DUO PAC   ","  ",
	"WALL HEAT,EVAP/AC IN OFFC","DE",
	"WALL HEAT,WALL A/C,&HUMID","DL",
	"WALL HEAT- HUMID COOL    ","DF",
	"WALL HEAT- HUMID COOLER  ","DF",
	"WALL HEAT-=HUMID COOLER  ","DF",
	"WALL HEAT-EVAP COOL      ","DE",
	"WALL HEAT-EVAP COOLER    ","DE",
	"WALL HEAT-H UMID COOL    ","DF",
	"WALL HEAT-HUMID CCOL     ","DF",
	"WALL HEAT-HUMID COL      ","DF",
	"WALL HEAT-HUMID COOL     ","DF",
	"WALL HEAT-HUMID COOLER   ","DF",
	"WALL HEAT-HUMID COOLERS  ","DF",
	"WALL HEAT-HUMID COOL\\    ","DF",
	"WALL HEAT-REFRIG.        ","DA",
	"WALL HEAT-REFRIGERATION  ","DA",
	"WALL HEAT-WALL REFRIG    ","DA",
	"WALL HEAT-WALLCOOLER     ","DL",
	"WALL HEAT-WINDOW REFRIG  ","DA",
	"WALL HEAT/ COOLER        ","  ",
	"WALL HEAT/AC             ","DX",
	"WALL HEAT/COLLER         ","DG",
	"WALL HEAT/COOLER         ","DG",
	"WALL HEAT/EVAP           ","DE",
	"WALL HEAT/EVAP COOL      ","DE",
	"WALL HEAT/HUM            ","DF",
	"WALL HEAT/HUMID          ","DF",
	"WALL HEAT/HUMID AIR      ","DF",
	"WALL HEAT/HUMID COOL     ","DF",
	"WALL HEAT/HUMID.         ","DF",
	"WALL HEAT/HUMID/WALL AC  ","  ",
	"WALL HEAT/IN HOUSE COOLER","DG",
	"WALL HEAT/REF            ","DA",
	"WALL HEAT/ROOF COOLER    ","DR",
	"WALL HEAT/ROOM EVAP      ","DE",
	"WALL HEAT/WALL A/C       ","  ",
	"WALL HEAT/WALL REF       ","  ",
	"WALL HEAT/WINDOW EVAP    ","DE",
	"WALL HEAT/WINDOW REF.    ","  ",
	"WALL HEAT=HUMID COOLER   ","DF",
	"WALL HEATE-HUMID COOL    ","DF",
	"WALL HEATE-HUMID COOLER  ","DF",
	"WALL HEATE4R             ","D ",
	"WALL HEATER              ","D ",
	"WALL HEATER & COOLER     ","DG",
	"WALL HEATER & EVAP       ","DE",
	"WALL HEATER & HUMID COOL ","DF",
	"WALL HEATER & WALL A/C   ","DL",
	"WALL HEATER + HUMIDIFIER ","DF",
	"WALL HEATER - EVAP       ","DE",
	"WALL HEATER / 2 EVAP COOL","  ",
	"WALL HEATER / EVAP       ","DE",
	"WALL HEATER / EVAP COOLER","DE",
	"WALL HEATER / HUMID COOL ","DF",
	"WALL HEATER / WOODSTOVE  ","D ",
	"WALL HEATER /EVAP COOLING","  ",
	"WALL HEATER AND 2 COOLERS","DG",
	"WALL HEATER AND COOLER   ","DG",
	"WALL HEATER AND HUMID    ","DF",
	"WALL HEATER EVAP         ","DE",
	"WALL HEATER EVAP COOLER  ","DE",
	"WALL HEATER ONLY         ","D ",
	"WALL HEATER-EVAP COOLER  ","DE",
	"WALL HEATER-HUMID COOL   ","DF",
	"WALL HEATER-HUMID COOLE  ","DF",
	"WALL HEATER-HUMID COOLER ","DF",
	"WALL HEATER/ COOLER      ","DG",
	"WALL HEATER/ EVAP        ","DE",
	"WALL HEATER/ HUMID       ","DF",
	"WALL HEATER/2 COOLERS    ","DG",
	"WALL HEATER/AC           ","DX",
	"WALL HEATER/COLLER       ","DG",
	"WALL HEATER/COOLER       ","DG",
	"WALL HEATER/COOLERS      ","DG",
	"WALL HEATER/EVAP         ","DE",
	"WALL HEATER/EVAP COOL    ","DE",
	"WALL HEATER/EVAP COOLER  ","DE",
	"WALL HEATER/HUM          ","DF",
	"WALL HEATER/HUMID        ","DF",
	"WALL HEATER/HUMID COOL   ","DF",
	"WALL HEATER/HUMID.       ","  ",
	"WALL HEATER/HUMIDIFIER   ","DF",
	"WALL HEATER/REF          ","  ",
	"WALL HEATER/WALL AC      ","DL",
	"WALL HEATER/WALL REFRIG  ","DA",
	"WALL HEATERS             ","D ",
	"WALL HEATERS& EVAP COOLER","DE",
	"WALL HEATERS-HUMID COOL  ","DF",
	"WALL HEATERS-HUMID COOLER","DF",
	"WALL HEATERS/ COOLERS    ","  ",
	"WALL HEATERS/COOLERS     ","  ",
	"WALL HR / AC             ","DX",
	"WALL HT                  ","D ",
	"WALL HT & EVAP           ","DE",
	"WALL HT & EVAP COOL      ","DE",
	"WALL HT &EVAP            ","DE",
	"WALL HT / COOLER         ","DG",
	"WALL HT / EVAP           ","DE",
	"WALL HT / EVAP COOL      ","DE",
	"WALL HT / HUMID          ","DF",
	"WALL HT / HUMID COOL     ","DF",
	"WALL HT /EVAP COOL       ","DE",
	"WALL HT EVAP             ","DE",
	"WALL HT EVAP COOL        ","DE",
	"WALL HT EVAP DUO PAC     ","  ",
	"WALL HT, EVAP COOL       ","  ",
	"WALL HT-EVAP COOL        ","DE",
	"WALL HT-EVAP COOLER      ","DE",
	"WALL HT. EVAP EACH UNIT  ","  ",
	"WALL HT., EVAP, WINDOW AC","DE",
	"WALL HT./WINDOW AC/EVAP  ","DE",
	"WALL HT/ EVAP            ","DE",
	"WALL HT/ EVAP COOL       ","DE",
	"WALL HT/ EVAP COOLER     ","DE",
	"WALL HT/ EVAP/AC         ","DE",
	"WALL HT/ SMALL AC        ","DX",
	"WALL HT/ WALL AC         ","DL",
	"WALL HT/2COOLERS         ","DG",
	"WALL HT/AC               ","DX",
	"WALL HT/COOLER           ","DG",
	"WALL HT/EVAP             ","DE",
	"WALL HT/EVAP & A/C       ","DE",
	"WALL HT/EVAP COOL        ","DE",
	"WALL HT/EVAP COOL.       ","DE",
	"WALL HT/EVAP COOLER      ","DE",
	"WALL HT/EVAP COOLERS     ","DE",
	"WALL HT/EVAP/DUOPAK      ","DE",
	"WALL HT/EVAPCOOL         ","DE",
	"WALL HT/EVAPCOOLER       ","DE",
	"WALL HT/GFWA             ","D ",
	"WALL HT/HUMID COOL       ","DF",
	"WALL HT/SMALL WALL AC    ","DL",
	"WALL HT/WINDOW AC        ","DW",
	"WALL HT/WINDOW EVAP      ","DE",
	"WALL HTR                 ","D ",
	"WALL HTR / A/C           ","DX",
	"WALL HTR / EVAP          ","DE",
	"WALL HTR / EVAP COOL     ","DE",
	"WALL HTR/ AC             ","  ",
	"WALL HTR/ COOLER         ","DG",
	"WALL HTR/ HUM.           ","  ",
	"WALL HTR/ HUMID          ","DF",
	"WALL HTR/ HUMID.         ","  ",
	"WALL HTR/COOLER          ","DG",
	"WALL HTR/EVAP            ","DE",
	"WALL HTR/EVAP COOL       ","DE",
	"WALL HTR/HUMID           ","DF",
	"WALL HTR/ROOF AC         ","DR",
	"WALL HTR/ROOF HUMID.     ","  ",
	"WALL HUM                 ","  ",
	"WALL HUMID               ","  ",
	"WALL OR ZONE HEAT & EVAP ","DE",
	"WALL REF                 "," A",
	"WALL REF - FLOOR UNIT    ","CA",
	"WALL REF-FLOOR HEAT      ","CA",
	"WALL REF/FLOOR FURNACE   ","  ",
	"WALL REFRIG & HEAT       ","XA",
	"WALL REFRIG-ZONE HEAT    ","TA",
	"WALL UN, COOLER          ","  ",
	"WALL UNIT                ","  ",
	"WALL UNIT & COOLER       ","XG",
	"WALL UNIT & EVAP         ","XE",
	"WALL UNIT & HUM          ","XF",
	"WALL UNIT (2)            ","  ",
	"WALL UNIT (2), HUMID     "," F",
	"WALL UNIT (6)            ","  ",
	"WALL UNIT + HUMID        ","  ",
	"WALL UNIT - ATMOSPHERE   ","  ",
	"WALL UNIT - COOLER       "," G",
	"WALL UNIT - HUM          "," F",
	"WALL UNIT / COOLER       "," G",
	"WALL UNIT / DUAL         ","WD",
	"WALL UNIT / HUMID        "," F",
	"WALL UNIT AND CENTRAL AIR","  ",
	"WALL UNIT COOLING HUMID  ","  ",
	"WALL UNIT DBL/COOLER ROOF","  ",
	"WALL UNIT DUAL           ","WD",
	"WALL UNIT DUAL / HUMID   ","WD",
	"WALL UNIT DUAL/HUMID     ","WD",
	"WALL UNIT HEAT / NO COOL ","DN",
	"WALL UNIT HT             ","D ",
	"WALL UNIT REF            "," A",
	"WALL UNIT(4), HUMID(4)   ","DF",
	"WALL UNIT+HUMID          ","DF",
	"WALL UNIT, COOLER,DUAL RF"," X",
	"WALL UNIT, HUMID.        "," F",
	"WALL UNIT, REFRIG.       "," A",
	"WALL UNIT,COLER,COOLING,H","DG",
	"WALL UNIT-2TON           "," X",
	"WALL UNIT-COOLER         "," X",
	"WALL UNIT-COOLING        "," X",
	"WALL UNIT-DUAL           ","WD",
	"WALL UNIT-HUMID          "," F",
	"WALL UNIT-WINDOW COOLER  "," W",
	"WALL UNIT/ A/C           "," L",
	"WALL UNIT/ COOLER        "," L",
	"WALL UNIT/ DUAL          ","WD",
	"WALL UNIT/ EVAP COOLER   ","DE",
	"WALL UNIT/ HUMID         ","DF",
	"WALL UNIT/ ROOF COOLER   ","DR",
	"WALL UNIT/ ROOF EVAP     ","DE",
	"WALL UNIT/ ROOM EVAP     ","DE",
	"WALL UNIT/A.C.           "," L",
	"WALL UNIT/AC             "," L",
	"WALL UNIT/CENTRAL EVAP   ","DE",
	"WALL UNIT/COOLER         ","DX",
	"WALL UNIT/COOLER ROOF    ","DR",
	"WALL UNIT/COOLER-ROOF    ","DR",
	"WALL UNIT/COOLING        ","DX",
	"WALL UNIT/COOLING/HUMID  ","DX",
	"WALL UNIT/DUAL           ","WD",
	"WALL UNIT/DUO PAK        ","WD",
	"WALL UNIT/EVAP           ","DE",
	"WALL UNIT/EVAP.          ","DE",
	"WALL UNIT/FLOOR UNIT     ","DM",
	"WALL UNIT/HUM            ","DF",
	"WALL UNIT/HUMID          ","DF",
	"WALL UNIT/HUMID.         ","DF",
	"WALL UNIT/HUMID/DUAL     ","DD",
	"WALL UNIT/R C            ","DR",
	"WALL UNIT/RF             ","DR",
	"WALL UNIT/RF COOL        ","DR",
	"WALL UNIT/RF COOLER      ","DR",
	"WALL UNIT/ROOF COOLER    ","DR",
	"WALL UNIT/WIN COOLER     ","DW",
	"WALL UNIT/WINDOW COOL    ","DW",
	"WALL UNIT; EVAP          ","DE",
	"WALL UNITS               ","DL",
	"WALL UNITS (4)/HUMID (4) ","DF",
	"WALL UNITS H&C           ","DL",
	"WALL UNITS/EVAPORATIVE   ","DE",
	"WALL& WINDOW UNIT        ","DW",
	"WALL&FLOOR HEAT EVAP COOL","CE",
	"WALL(2), EVAP            ","DE",
	"WALL(HUMID+EVAP.)        ","DL",
	"WALL+ HUMID              ","DF",
	"WALL+FLOOR HEAT EVAP COOL","ME",
	"WALL+HUMID               ","DF",
	"WALL, EVAP               ","DE",
	"WALL, HUM                ","DF",
	"WALL, HUMID              ","DF",
	"WALL, HUMID.             ","DF",
	"WALL, REF                ","DA",
	"WALL,EVAP                ","DE",
	"WALL,HUM                 ","DF",
	"WALL,HUMID               ","DF",
	"WALL,HUMID,2WALL UNITS   ","DE",
	"WALL,REF                 ","DA",
	"WALL- COOL + HUMID       ","DX",
	"WALL-A/C                 ","DX",
	"WALL-COOL + HUMID        ","DX",
	"WALL-EVAP                ","DE",
	"WALL-EVAPS               ","DE",
	"WALL-FLOOR UNIT          ","DM",
	"WALL-HUM                 ","DF",
	"WALL-HUMID               ","DF",
	"WALL-HUMID`              ","DF",
	"WALL-WILL ISTL ZONE      ","DX",
	"WALL-ZONE                ","DX",
	"WALL-ZONE UNIT           ","DX",
	"WALL/                    ","D ",
	"WALL/ 2 EVAPS            ","DE",
	"WALL/ A/C                ","DX",
	"WALL/ DUAL               ","WD",
	"WALL/ HUM                ","DF",
	"WALL/ HUMID              ","DF",
	"WALL/ HUMID.             ","DF",
	"WALL/ REF                ","DA",
	"WALL/ REF 1 TON          ","DA",
	"WALL/ ZONE               ","D ",
	"WALL/2 EVAP              ","DE",
	"WALL/2 HUM               ","DF",
	"WALL/2 WINDOW AC         ","DW",
	"WALL/3 TON               ","DA",
	"WALL/?                   ","D ",
	"WALL/A/C                 ","DX",
	"WALL/AC                  ","DX",
	"WALL/AC/EVAP             ","DX",
	"WALL/CAC                 ","DX",
	"WALL/CENT EVAP           ","DE",
	"WALL/CENTRAL EVAP        ","DE",
	"WALL/COOLER              ","DG",
	"WALL/COOLERS             ","DG",
	"WALL/EVAP                ","DE",
	"WALL/EVAP & AC           ","DE",
	"WALL/EVAP & DUO PAK      ","DD",
	"WALL/EVAP ESTIMATED      ","DE",
	"WALL/EVAP.               ","DE",
	"WALL/EVAP/WD STOVE       ","DE",
	"WALL/EVAP/WINDOW AC      ","DW",
	"WALL/EVAP/ZONE           ","DE",
	"WALL/EVP COOL            ","DE",
	"WALL/FLOOR               ","DM",
	"WALL/FLOOR HEAT-EVAP COOL","ME",
	"WALL/FLOOR HEATER& COOLER","MX",
	"WALL/FLOOR UNIT          ","M ",
	"WALL/FLOOR/EVAP          ","ME",
	"WALL/FLOOR/HUM           ","MF",
	"WALL/FLOORHTR/EVAP       ","ME",
	"WALL/FLR.HEAT & EVAP     ","ME",
	"WALL/FORCED; REF/AC      ","DB",
	"WALL/HEATALATOR          ","D ",
	"WALL/HUM                 ","DF",
	"WALL/HUMD                ","DF",
	"WALL/HUMID               ","DF",
	"WALL/HUMID.              ","DF",
	"WALL/HUMID/              ","DF",
	"WALL/HUMID/FLOOR         ","DM",
	"WALL/HUMID/ZONE          ","DX",
	"WALL/HUMID`              ","DF",
	"WALL/NONE                ","DN",
	"WALL/REF                 ","DA",
	"WALL/REF WALL            ","DA",
	"WALL/REFRIG              ","DA",
	"WALL/RF                  ","DR",
	"WALL/ROOF A.C.           ","WR",
	"WALL/ROOF COOLER         ","DR",
	"WALL/WALL                ","DL",
	"WALL/WALL AC             ","DL",
	"WALL/WALL REF            ","DL",
	"WALL/WALL REFRIDGE       ","DL",
	"WALL/WIN AC              ","DW",
	"WALL/WINDOW              ","DW",
	"WALL/WINDOW AC           ","DW",
	"WALL/WINDOW COOLER       ","DW",
	"WALL/WINDOW EVAP         ","DE",
	"WALL/WINDOW UNIT         ","DW",
	"WALL/ZONE                ","DX",
	"WALL/ZONE HEAT & EVAP    ","DE",
	"WALL/ZONE HT/ EVAP       ","DE",
	"WALL/ZONE/EVAP           ","DE",
	"WALL/ZONE;EVAP,REF       ","DE",
	"WALL;                    ","D ",
	"WALL; BASE, FAN/EVAP     ","DE",
	"WALL; EVAP               ","DE",
	"WALL; EVAP.              ","DE",
	"WALL; EVAP/ DUO PAK      ","DD",
	"WALL; WINDOW AC          ","DW",
	"WALL;AC                  ","DX",
	"WALL;AC/DUO PAK          ","DD",
	"WALL;EVAP                ","DE",
	"WALL;FLOOR/EVAP          ","DL",
	"WALL;REF                 ","DA",
	"WALL;REFRIG.             ","DA",
	"WALL?/HUMID              ","DF",
	"WALLHEAT                 ","D ",
	"WALLHEAT HUMID COOLER    ","DF",
	"WALLHEAT/COOLER          ","DX",
	"WALLHEAT/EVAP            ","DE",
	"WALLHEATER-HUMID COOLER  ","DF",
	"WALLHEATER/COOLER        ","DG",
	"WALLHT                   ","D ",
	"WALLHT/ EVAP COOL        ","DE",
	"WALLHT/EVAP              ","DE",
	"WALLHT/EVAP COOL         ","DE",
	"WALLHT/EVAP COOL EST.    ","DE",
	"WALLHT/EVAP COOLER       ","DE",
	"WALLHT/EVAP ESTIMATED    ","DE",
	"WALLHT/EVAP/AC           ","DE",
	"WALLHT/EVAPCOOL          ","DE",
	"WALLHT/WINDOWEVAP        ","DE",
	"WALLHUMID                ","DF",
	"WALLL HEATER             ","D ",
	"WALLUNIT-HUMID           ","DF",
	"WALLUNIT-HUMID`          ","DF",
	"WALL\\EVAP                ","DE",
	"WAQLL HEAT EVAP COOL     ","DE",
	"WATER HEATER/HUMID       ","EF",
	"WD STOVE                 ","S ",
	"WD STOVE/EVAP            ","SE",
	"WD. STOVE/EVAP           ","SE",
	"WDW A/C, COOLER & FLR HTR","CW",
	"WF                       ","D ",
	"WF & EVAP                ","DE",
	"WF - EVAP                ","DE",
	"WF / EVAP                ","DE",
	"WF AC                    ","DX",
	"WF EVAP                  ","DE",
	"WF/COOLER                "," W",
	"WF/EVAP                  ","DE",
	"WF/WALL AC               ","DL",
	"WIN                      "," W",
	"WIN A/C                  "," W",
	"WIN A/C & HUM            "," W",
	"WIN A/C (2)              "," W",
	"WIN A/C - DUO PAKS       ","WD",
	"WIN A/C - FLOOR - HUM    ","CW",
	"WIN A/C - WALL           ","DW",
	"WIN A/C - ZONE UNIT      ","TW",
	"WIN A/C / WALL UNIT      ","DW",
	"WIN COOLER               "," W",
	"WIN HUM                  "," W",
	"WIN REF                  "," A",
	"WIN REF/ WALL            ","DA",
	"WIN REF/WALL HEATER      ","DA",
	"WIN-A/C                  "," W",
	"WINDIW/WALL UNITS        "," W",
	"WINDOW                   "," W",
	"WINDOW A/C               "," W",
	"WINDOW A/C & FLOOR HEATER","CW",
	"WINDOW A/C & WALL HEATER ","DW",
	"WINDOW A/C & ZONE HEATER ","TW",
	"WINDOW A/C IN OFFICE     "," W",
	"WINDOW A/C NO HEATING    "," W",
	"WINDOW A/C UNIT          "," W",
	"WINDOW A/C/ FLOOR FURNACE"," W",
	"WINDOW A/C/ZONE UNIT     ","TW",
	"WINDOW A/C;EVAP;F/A      "," W",
	"WINDOW AC                "," W",
	"WINDOW AC & FLOOR HTR    ","CW",
	"WINDOW AC IN OFFICE AREAS"," W",
	"WINDOW COOLER            "," W",
	"WINDOW COOLERS           "," W",
	"WINDOW H&C IN OFFICE     ","  ",
	"WINDOW H&C UNITS         ","  ",
	"WINDOW H/C               ","  ",
	"WINDOW HEAT PUMPS        ","GW",
	"WINDOW HUMID COOL        "," W",
	"WINDOW REF               "," A",
	"WINDOW REF & WALL HEATER ","DA",
	"WINDOW REF/RADIATORS     ","IA",
	"WINDOW REF/ZONE UNIT     ","  ",
	"WINDOW REFRIGERATION     "," A",
	"WINDOW UNIT              "," W",
	"WLL A/C & WALL HTR       ","  ",
	"WLL HEAT EVAP COOL       ","  ",
	"WLLL 2-HUMID             ","  ",
	"WOOD STOVE               ","S ",
	"WOOD STOVE - EVAP COOLER ","SE",
	"WOOD STOVE - HUMID COOL  ","SF",
	"WOOD STOVE HEAT ONLY     ","  ",
	"WOOD STOVE/COOLER        ","SG",
	"WOOD STOVE/EVAP          ","SE",
	"WOOD STOVE; EVAP         ","SE",
	"WOODSTOVE                ","S ",
	"WOODSTOVE / EVAP         ","SE",
	"WOODSTOVE, FA HEAT       ","  ",
	"WOODSTOVE-HUMID COOLER   ","SF",
	"WQALL HEAT-HUMID COOL    ","DF",
	"Y                        ","  ",
	"Y ZONE UNIT              ","T ",
	"YDUO PAK                 "," D",
	"YDUOPAK                  "," D",
	"YFORCED.REF              "," A",
	"YZONE UNIT & EVAP        ","TE",
	"ZON                      ","T ",
	"ZONE                     ","T ",
	"ZONE  UNIT               ","T ",
	"ZONE & EVAP              ","TE",
	"ZONE & FAU/EVAP COOL     ","  ",
	"ZONE + WALL HT. EVAP C00L","DE",
	"ZONE +WALL HEAT EVAP     ","DE",
	"ZONE - EVAP              ","TE",
	"ZONE - FLOOR/2 WIN A/C   ","CW",
	"ZONE - HUM               ","TF",
	"ZONE - HUMID             ","TF",
	"ZONE / COOLER            ","TG",
	"ZONE / EVAP              ","TE",
	"ZONE / HUMID             ","TF",
	"ZONE / REF               ","TA",
	"ZONE / WALL / EVAP       ","DE",
	"ZONE / WINDOW            ","TW",
	"ZONE / WINDOW A/C        ","TW",
	"ZONE AIR                 ","TX",
	"ZONE EVAP                ","TE",
	"ZONE FLOOR HALL HT EVAP  ","  ",
	"ZONE FURNACE/EVP COOLER  ","  ",
	"ZONE H/EVAP COOL.        ","TE",
	"ZONE HE/EVAP COOL        ","TE",
	"ZONE HEAT                ","T ",
	"ZONE HEAT  EVAP COOL     ","TE",
	"ZONE HEAT & (EST.) EVAP  ","TE",
	"ZONE HEAT & (EST.)EVAP   ","TE",
	"ZONE HEAT & EVAP         ","TE",
	"ZONE HEAT & EVAP (EST.)  ","TE",
	"ZONE HEAT & EVAP COOL    ","TE",
	"ZONE HEAT & HUMID COOL   ","TF",
	"ZONE HEAT & WALL A/C     ","TL",
	"ZONE HEAT & WINDOW A/C   ","TW",
	"ZONE HEAT + EVAP         ","TE",
	"ZONE HEAT - HUMID        ","TF",
	"ZONE HEAT / EVAP         ","TE",
	"ZONE HEAT / HUMID        ","TF",
	"ZONE HEAT 7 EVAP         ","TE",
	"ZONE HEAT EVAP           ","TE",
	"ZONE HEAT EVAP COOL      ","TE",
	"ZONE HEAT EVAP COOL`     ","TE",
	"ZONE HEAT HUMID COOL     ","TF",
	"ZONE HEAT ONLY           ","T ",
	"ZONE HEAT WINDOW EVAP    ","TE",
	"ZONE HEAT,WINDOW AC      ","TW",
	"ZONE HEAT/ AC            ","TX",
	"ZONE HEAT/EVAP           ","TE",
	"ZONE HEAT/EVAP & WALL A/C","TE",
	"ZONE HEAT/HUMID          ","TF",
	"ZONE HEAT/WINDOW EVAP    ","TE",
	"ZONE HEATER              ","T ",
	"ZONE HEATER & COOLER     ","TG",
	"ZONE HEATER/ EVAP        ","TE",
	"ZONE HEATER/NO COOLING   ","T ",
	"ZONE HEATER/WINDOW COOLER","TW",
	"ZONE HEATING             ","T ",
	"ZONE HEATING/EVAP        ","TE",
	"ZONE HR / EVAP COOLER    ","TE",
	"ZONE HT                  ","T ",
	"ZONE HT / EVAP           ","TE",
	"ZONE HT / WINDOW EVAP    ","TE",
	"ZONE HT EVAP COOL        ","TE",
	"ZONE HT/ DUO PAK         ","WD",
	"ZONE HT/ EVAP            ","TE",
	"ZONE HT/ EVAP COOL       ","TE",
	"ZONE HT/ WINDOW EVAP     ","  ",
	"ZONE HT/COOLER           ","TG",
	"ZONE HT/EVAP             ","TE",
	"ZONE HT/EVAP COOL        ","TE",
	"ZONE HT/EVAP COOLER      ","TE",
	"ZONE HT/HUMID            ","TF",
	"ZONE HT/HUMID AIR        ","TF",
	"ZONE HTR                 ","T ",
	"ZONE HUMID               "," F",
	"ZONE HY/EVAP COOLER      ","TE",
	"ZONE PER ROOM            ","T ",
	"ZONE UN.                 ","  ",
	"ZONE UNIT                ","T ",
	"ZONE UNIT #1             ","T ",
	"ZONE UNIT - 5            ","T ",
	"ZONE UNIT - A/C          ","TX",
	"ZONE UNIT - FLOOR        ","C ",
	"ZONE UNIT - WALL A/C     ","TL",
	"ZONE UNIT - WALL AIR     ","TL",
	"ZONE UNIT - WINDOW UNIT  ","TW",
	"ZONE UNIT / HUMID        ","TF",
	"ZONE UNIT / HUMID AIR    ","TF",
	"ZONE UNIT / WOODSTOVE    ","S ",
	"ZONE UNIT /HUMID AIR     ","TF",
	"ZONE UNIT AND COOLER     ","TG",
	"ZONE UNIT COOLING HUMID  ","  ",
	"ZONE UNIT HEAT           ","T ",
	"ZONE UNIT HUMID          ","  ",
	"ZONE UNIT, HUM           ","TF",
	"ZONE UNIT-HEAT           ","  ",
	"ZONE UNIT-HEATING        ","  ",
	"ZONE UNIT-HUM            ","TF",
	"ZONE UNIT-HUMID          ","TF",
	"ZONE UNIT/ COOLING       ","  ",
	"ZONE UNIT/ HUMID         ","TF",
	"ZONE UNIT/ HUMID.        ","TF",
	"ZONE UNIT/ WINDOW COOLER ","TW",
	"ZONE UNIT/2ND DUO PAK    ","  ",
	"ZONE UNIT/COOLING        ","TR",
	"ZONE UNIT/COOLING ROOF   ","TR",
	"ZONE UNIT/EAVP           ","TE",
	"ZONE UNIT/EVAP           ","TE",
	"ZONE UNIT/FLOOR          ","C ",
	"ZONE UNIT/HUM            ","TF",
	"ZONE UNIT/HUMID          ","TF",
	"ZONE UNIT/HUMID.         ","TF",
	"ZONE UNIT/WINDOW A/C     ","TW",
	"ZONE UNIT/WINDOW AC      ","TW",
	"ZONE UNIT/WINDOW COOLER  ","TW",
	"ZONE UNITS               ","T ",
	"ZONE, 2 WINDOW REFRIG.   ","TA",
	"ZONE, HEAT PUMP          ","GH",
	"ZONE,FLOOR; EVAP         ","  ",
	"ZONE,FLRHT/EVAP          ","  ",
	"ZONE,HUMID               ","TF",
	"ZONE,WALL,EVAP           ","DE",
	"ZONE-HUMID               ","TF",
	"ZONE/ EVAP               ","TE",
	"ZONE/ HUIMID.            ","TF",
	"ZONE/ HUMID              ","TF",
	"ZONE/ REF. 1 TON         ","TA",
	"ZONE/CEN. REF/EVAP       ","  ",
	"ZONE/COOLER              ","TG",
	"ZONE/COOLING             ","TG",
	"ZONE/DUCTED EVAP         ","TE",
	"ZONE/EVAP                ","TE",
	"ZONE/EVAP BOTH UNITS     ","  ",
	"ZONE/EVAP COOL           ","TE",
	"ZONE/EVAP COOLER         ","TE",
	"ZONE/EVAP.               ","TE",
	"ZONE/EVAP/WINDOW A/C     ","TE",
	"ZONE/FLR. HEAT & EVAP    ","CE",
	"ZONE/FLR. HEAT, EVAP     ","CE",
	"ZONE/HUM                 ","TF",
	"ZONE/HUMID               ","TF",
	"ZONE/REF                 ","TA",
	"ZONE/UNK                 ","  ",
	"ZONE/WAL EVAP            ","TE",
	"ZONE/WALL                ","  ",
	"ZONE/WALL AC             ","TL",
	"ZONE/WALL EVAP           ","  ",
	"ZONE/WALL HEAT & EVAP    ","DE",
	"ZONE/WALL HT. & EVAP     ","  ",
	"ZONE/WALL REF            ","TA",
	"ZONE/WALL UNIT           ","  ",
	"ZONE/WALL; EVAP          ","TE",
	"ZONE/WINDOW              ","TW",
	"ZONE/WINDOW A/C          ","TW",
	"ZONE/WINDOW AC           ","TW",
	"ZONE/WINDOW COOLER/HUMID ","TW",
	"ZONE/WINDOW REF          ","TA",
	"ZONE/WINDOW REF. UNIT    ","TA",
	"ZONE; EVAP               ","TE",
	"ZONE; EVAP; REF          ","TE",
	"ZONE; EVAP;DUO-PAK       ","WD",
	"ZONE; EVSP               ","  ",
	"ZONE; REF                ","TA",
	"ZONE; WINDOW AC          ","TW",
	"ZONE;EVAP                ","TE",
	"ZONE?                    ","T ",
	"ZONEHEAT                 ","T ",
	"ZONEHT                   ","T ",
	"ZONEHT/                  ","T ",
	"ZONEHT/EVAP              ","TE",
	"ZONEHT/WINDOW AC         ","TW",
	"ZONEUNIT, WALL A/C       ","TL",
	"ZONEWALL                 ","T ",
	"ZONW / HUMID             ","TF",
	"\\                        ","  ",
	"",""
};

IDX_TBL  Tul_Heat[] =
{
   "FORCED",               "B",
   "WALL",                 "D",
   "FAH",                  "B",
   "FLOOR",                "C",
   "ZONE",                 "T",
   "",""
};

IDX_TBL  Tul_Cool[] =
{
   "HUMID",                "F",
   "EVAP",                 "E",
   "COOLER",               "G",
   "",""
};

#define  TUL_HC_COUNT   3714
#endif