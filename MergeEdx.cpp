/******************************************************************************
 *
 *    - Do not need to parse sale file since new sale is in roll file at every
 *      update.  When load lien, use -Ms to merge cum sale to new file.  When
 *      running update, if there is new sale recorded, append it to cum sale file
 *    - New sales are appended to cum sale file then applied back to R01 later.
 *    - LDR file does not contain government parcels.  To update them, we have
 *      to run update with -U option right after LDR version approved.
 *
 * Input:
 *    - Events.txt      (lien file 547-bytes ebcdic)
 *    - Vendor.txt      (roll file 547-bytes ebcdic)
 *    - Charac.txt      (char file 70-bytes ebcdic)
 *    - Zipcode.txt     (zipcode file 53-bytes ebcdic)
 *    - Subdiv.txt      (subdiv table 21-bytes ebcdic)
 *    - Salesrep.txt    (sale file 133-bytes ebcdic)
 *
 * Options:
 *    -CEDX -L -Ms -Xl -Lz    : yearly update
 *    -CEDX -U -Xsi [-T] [-Lz]: monthly update (current sales are updated automatically)
 *                              (use -Xsi to update sale db, not for extract sale)
 *    -CEDX -L[yyyymmdd]      : daily update.  If the date not provided, today file will be run
 *    -Lz                     : load zipcode table
 *    -Ms                     : Merge cumulative sale to R01 file
 *
 * Revision
 * 07/18/2006 1.2.27.3  Test version
 * 11/09/2007 1.4.29    Add Edx_MergeDaily() to support daily update.  The daily update
 *                      file contains only records that was updated for the day. This
 *                      version also fix situs display bug.
 * 12/13/2007 1.4.34    Fix name2 overflow problem, get correct mail city and remove
 *                      code that populates mailaddr using situs.  Add importZipcode()
 *                      to update zip table.
 * 01/15/2008 1.5.2     Add code to support new standard UseCode.
 * 01/23/2008 1.5.3.12  If unable to translate primary usecode, try secondary.
 * 01/23/2008 1.5.3.13  Clear old use code if new use code not assigned
 * 01/29/2008 1.5.4     Adding code to produce update file
 * 03/24/2008 1.5.7.2   Format IMAPLINK.  Remove custom code and use standard code
 *                      to update std use.  Remove retired parcels
 * 07/30/2008 8.0.6     Modify Edx_MergeXferFile() to update sale for 2008 LDR
 * 08/07/2008 8.0.7     Fix bug in Edx_MergeOwner() that crashes if C/O has no space
 * 02/18/2009 8.5.11    Fix Unit#, some of them are actually range street number.
 *                      Data still not in perfect situation, but better than what the county provided.
 * 03/16/2009 8.7.0     Format STORIES as 99.0.  We no longer multiply by 10.
 * 07/17/2009 9.1.3     Add Edx_ExtrLien()
 * 08/05/2009 9.1.5     Populating other values to R01 record
 * 12/03/2009 9.2.8     Fix Edx_CreateLienRec() and Edx_ExtrLien() to take active records only.
 * 02/16/2010 9.4.2     Set Prop8 flag.
 * 02/18/2011 10.4.3    Process previous day if daily file not available.  Set LastGrGrDate
 *                      in County table using file date when load daily.
 * 04/27/2011 10.5.6    Modify Edx_FormatDoc() to correctly format DocNum.  Add -Ms to apply cum sale file
 *                      to R01 as needed (when process LDR).  Cum sale file can also
 *                      be used in ImportRaw. Rename MergeEdxRoll() to Edx_Load_Roll(),
 *                      Edx_MergeSale1() to Edx_MergeSale().  Modify Edx_MergeRoll() to return
 *                      new sale record to be added to cum sale file.
 *                      Remove code that move daily file to daily folder.
 * 05/25/2011 10.5.14   Fix Situs Unit# problem in Edx_MergeAdr().
 * 07/08/2011 11.0.2    Add S_HSENO.  Update TRANSFER using sale data.
 * 07/05/2012 12.1.1    Add comments.
 * 01/08/2013 12.4.1    Fix bug in loadEdx() that cancels sale import option.
 * 04/11/2013 12.5.4    Update R01 file only sales with doctax.
 * 04/16/2013 12.5.5    Fix Edx_MergeSale() that corrupts cum sale file. 
 * 09/10/2013 13.7.14   Modify Edx_MergeOwner() to set Etal flag.
 * 10/02/2013 13.10.0   Modify Edx_MergeOwner() and use updateVesting() to update Vesting and Etal flag.
 * 09/02/2014 14.4.2    Fix vesting bug in Edx_MergeOwner().
 * 08/10/2015 15.1.0    Remove -Xx option
 * 02/16/2016 15.6.0    Remove duplicate owner name.
 * 05/24/2016 15.9.4    Adding LoadTaxBase() and related functions to data to TaxBase, TaxDelq, TaxItems, TaxAgency tables
 * 09/06/2016 16.2.5    Modify tax loading process to get accurate info from TaxBill.
 * 11/19/2016 16.7.0    Add Edx_ExtrSale() to process monthly sale file and Edx_ExtrHistSale() to process
 *                      10yr history sale file.  Also set Full/Partial and MultiSale flags.
 * 12/09/2016 16.7.7    Fix infinite loop in Edx_UpdateTaxBase().
 * 12/12/2016 16.8.1    Fix TotalTax in Edx_ParseTaxBase(), use sum of Tax1+Tax2 instead of NetTax.
 * 01/14/2017 16.9.3    Fix DelqStatus.
 * 03/04/2017 16.11.0   Modify Edx_ParseTaxDetail() to add TC_Flag
 * 03/05/2017 16.11.1   Fix bug in Edx_UpdateTaxBase()
 * 04/14/2017 16.13.10  Modify Edx_UpdateTaxBase() to update TotalDue and PenAmt
 * 10/24/2017 17.4.1    Modify Edx_ParseTaxBase() to fix DelqRec.TaxYear
 * 11/21/2017 17.5.0    Call updateDelqFlg() to populate delqYear & defaultAmt in Tax_Base table.
 * 08/02/2018           Move to LoadMB
 *
 ******************************************************************************/

#include "stdafx.h"
#include "CountyInfo.h"
#include "Prodlib.h"
#include "R01.h"
#include "RecDef.h"
#include "Tables.h"
#include "Logs.h"
#include "LoadDef.h"
#include "Utils.h"
#include "XlatTbls.h"
#include "doSort.h"
#include "doOwner.h"
#include "formatApn.h"
#include "SaleRec.h"
#include "LoadOne.h"
#include "SqlExt.h"
#include "MergeEdx.h"
#include "UseCode.h"
#include "LOExtrn.h"
#include "Tax.h"

static LU_ENTRY tblAttr[MAX_ATTR_ENTRIES];
static FILE     *fdChar, *fdLand;
static long     lCharSkip, lSaleSkip, iNewSale;
static long     lCharMatch, lSaleMatch, lTaxSkip, lTaxMatch, lSold4TaxDate, lTaxBillDate, lInstallMTDate, lTaxRollInfoDate;

/********************************* Edx_FormatDoc *****************************
 *
 *  - DocNum format : 9999-999 or 99999
 *
 * Return output length.  0 means all blank
 *
 *****************************************************************************/

int Edx_FormatDoc(char *pResult, char *pDocNum, char *pDocDate)
{
   long  lTmp, lYear;
   int   iRet=0;

   lTmp = atoin(pDocNum, RSIZ_CURR_RECI);
   if (lTmp > 0)
   {
      lYear = atoin(pDocDate, 6);
      if (lYear < 199802)
         iRet = sprintf(pResult, "%.4s-%.3s", pDocNum, pDocNum+4);
      else if (*pDocNum > '0'  && lTmp < 9990990)
         iRet = sprintf(pResult, "%.7d  ", lTmp);
      else
         iRet = sprintf(pResult, "%.7d ", lTmp);
   } else
      strcpy(pResult, "        ");
   return iRet;
}

/******************************** Edx_FixSales *******************************
 *
 * Fix sale file by reformat DocNum and APN to current format.
 *
 * Return number of record output, negative number if error.
 *
 *****************************************************************************/

int Edx_FixSales(char *pInfile, char *pOutfile)
{
   char  acInbuf[1024], acTmp[64], *pTmp;
   long  lTmp, lYear, lCnt, iRet;
   FILE  *fdIn, *fdOut;
   SCSAL_REC *pRec = (SCSAL_REC *)&acInbuf[0];

   LogMsg("Fix sale file %s to %s", pInfile, pOutfile);

   if (!(fdIn = fopen(pInfile, "r")))
   {
      LogMsg("***** Error opening %s (%d)", pInfile, _errno);
      return -1;
   }

   if (!(fdOut = fopen(pOutfile, "w")))
   {
      LogMsg("***** Error creating %s (%d)", pOutfile, _errno);
      return -2;
   }

   lCnt = 0;
   while (!feof(fdIn))
   {
      if (!(pTmp = fgets(acInbuf, 1024, fdIn)))
         break;

      // Reformat APN
      if (pRec->Apn[9] == ' ')
         memset(&pRec->Apn[9], '0', 2);

      // Reformat DocNum
      lYear = atoin(pRec->DocDate, 6);
      memcpy(acTmp, pRec->DocNum, 8);
      if (lYear < 199802)
      {
         if (acTmp[4] == ' ')
            acTmp[4] = '-';
         iRet = 8;
      } else
      {
         acTmp[8] = 0;
         if (acTmp[4] == ' ')
            remChar(acTmp, ' ');
         lTmp = atol(acTmp);
         iRet = sprintf(acTmp, "%.7d  ", lTmp);
      }

      memcpy(pRec->DocNum, acTmp, iRet);
      fputs(acInbuf, fdOut);

      if (!(lCnt++ % 1000))
         printf("\r%u", lCnt);
   }

   fclose(fdIn);
   fclose(fdOut);

   LogMsg("Total records output: %d", lCnt);

   return lCnt;
}

/******************************** Edx_MergeOwner *****************************
 *
 * Return 0 if successful, >0 if error
 *
 *****************************************************************************/

void Edx_MergeOwner(char *pOutbuf, char *pRollRec)
{
   int   iTmp;
   char  acOwner1[128], acOwner2[128], acTmp[128], acVesting[8];
   char  acSave1[64], acSave2[64], acCareOf[64];
   char  *pTmp, *pName1, *pName2;
   bool  bNoMerge=false, bKeep=false;

   OWNER    myOwner1, myOwner2;
   EDX_ROLL *pRec = (EDX_ROLL *)pRollRec;

   // Clear output buffer if needed
   removeNames(pOutbuf, true);
   if (pRec->Etal_Flag == 'A')
      strcpy(acVesting, "EA");
   else 
      acVesting[0] = 0;

   acSave1[0]=acSave2[0]=acCareOf[0]=acOwner2[0] = 0;
   pName1 = pRec->Name1;
   if (!memcmp(pName1, "NO OWNER", 8))
   {
      memcpy(pOutbuf+OFF_NAME1, pName1, RSIZ_NAME1);
      memcpy(pOutbuf+OFF_NAME_SWAP, pName1, RSIZ_NAME1);
      return;
   }

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "02555211100", 9) )
   //   iTmp = 0;
#endif

   memcpy(acOwner2, pRec->Name2, RSIZ_NAME2);
   acOwner2[RSIZ_NAME2] = 0;
   pName2 = acOwner2;
   if (pRec->NameCode == 'A' || *pName2 == '%' || !memcmp(pName2, "C/O", 3) || !memcmp(pName2, "ATTN", 4))
   {
      strcpy(acCareOf, acOwner2);
      blankRem(acCareOf);
      *pName2 = 0;
   } else if (*pName2 > ' ')
   {
      if (acVesting[0] < 'A')
         findVesting(pName2, acVesting);

      if ((pTmp = strstr(acOwner2, " LE%")) || (pTmp = strstr(acOwner2, " LE %")) )
         *pTmp = 0;
      if ((pTmp = strchr(acOwner2, '%')) || (pTmp = strchr(acOwner2, '(')) )
         *pTmp = 0;
      strcpy(acSave2, acOwner2);
      blankRem(acSave2);
      if ((pTmp = isNumIncluded(acOwner2, 0)) ||
          strstr(acOwner2, " TRUST ")  ||
          strstr(acOwner2, " AKA ")  )
         bNoMerge = true;
      else
      {
         if ((pTmp=strstr(acOwner2, " SUCC CO")) ||
             (pTmp=strstr(acOwner2, " CO TR"))   ||
             (pTmp=strstr(acOwner2, " CO SUC"))  ||
             (pTmp=strstr(acOwner2, " SURV T"))  ||
             (pTmp=strstr(acOwner2, " MD TR"))   ||
             (pTmp=strstr(acOwner2, " SUC TR"))  ||
             (pTmp=strstr(acOwner2, " SUCC T")) )
         {
            *pTmp = 0;
         } else
         {
            pTmp = &acOwner2[strlen(acOwner2)-3];
            if (!memcmp(pTmp, " TR", 3) || (pTmp=strstr(acOwner2, " TR ")))
               *pTmp = 0;
         }
      }

      if ((pTmp = strstr(acOwner2, " TRUSTE")) )
         *pTmp = 0;

      if ((pTmp = strstr(acOwner2, "CORP")) ||
          (pTmp = strstr(acOwner2, "LLC")) )
         acOwner2[0] = 0;
      else
         blankRem(acOwner2);
   }

   // Process name1
   memcpy(acOwner1, pName1, RSIZ_NAME1);
   acOwner1[RSIZ_NAME1] = 0;
   if (acVesting[0] < 'A')
      findVesting(acOwner1, acVesting);

   if ((pTmp = strstr(acOwner1, " LE%"))  ||
       (pTmp = strstr(acOwner1, " LE %")) ||
       (pTmp = strstr(acOwner1, " AKA ")) )
      *pTmp = 0;
   if ((pTmp = strchr(acOwner1, '%')) || (pTmp = strchr(acOwner1, '(')) )
      *pTmp = 0;
   strcpy(acSave1, acOwner1);
   blankRem(acSave1);

   // Drop TR, TRUSTEE and ET AL
   if ((pTmp=strstr(acOwner1, " SUCC CO")) ||
       (pTmp=strstr(acOwner1, " CO TR"))   ||
       (pTmp=strstr(acOwner1, " CO SUC"))  ||
       (pTmp=strstr(acOwner1, " SURV T"))  ||
       (pTmp=strstr(acOwner1, " MD TR"))   ||
       (pTmp=strstr(acOwner1, " SUCC T"))  ||
       (pTmp=strstr(acOwner1, " SUC TR")))
   {
      *pTmp = 0;
      bKeep = true;
   } else
   {
      pTmp = &acOwner1[strlen(acOwner1)-3];
      if (!memcmp(pTmp, " TR", 3))
         *pTmp = 0;
      else if (!memcmp(--pTmp, " CO ", 4) || (pTmp=strstr(acOwner1, " TR ")))
         *pTmp = 0;
   }

   if (pTmp = strstr(acOwner1, " TRUSTE"))
      *pTmp = 0;

   // If name starts with a digit, do not parse
   if (isdigit(acOwner1[0]))
   {
      memcpy(pOutbuf+OFF_NAME1, acSave1, strlen(acSave1));
      memcpy(pOutbuf+OFF_NAME_SWAP, acSave1, strlen(acSave1));
      if (acSave2[0])
      {
         acSave2[SIZ_NAME2] = 0;
         memcpy(pOutbuf+OFF_NAME2, acSave2, strlen(acSave2));
      }
      return;
   }
   blankRem(acOwner1);

   // Check for trust
   if ((pTmp = strstr(acOwner1, " FAMILY")) ||
       (pTmp = strstr(acOwner1, " LIFE ESTATE")) ||
       (pTmp = strstr(acOwner1, " TRUST")) ||
       (pTmp = strstr(acOwner1, " EST OF"))
      )
   {
      *pTmp = 0;
      bKeep = true;
   }

   // Now parse owners
   iTmp = splitOwner(acOwner1, &myOwner1, 5);
   if (iTmp == -1)
   {
      strcpy(myOwner1.acName1, acSave1);
      strcpy(myOwner1.acSwapName, acSave1);
      bNoMerge = true;
   } else if (!strcmp(myOwner1.acOL, myOwner1.acSM) && !strcmp(myOwner1.acOF, myOwner1.acSL))
   {
      myOwner1.acName2[0] = 0;
   } else if (strchr(acOwner1, '/'))
   {
      strcpy(myOwner1.acSwapName, acOwner1);
      bNoMerge = true;
   }

   // Ignore Name2 if it's the same as Name1
   if (!strcmp(acOwner1, acOwner2))
      acOwner2[0] = 0;

   if (acOwner2[0])
   {
      if (!bNoMerge)
      {
         // Parse name2 - if they have same last name, merge them
         splitOwner(acOwner2, &myOwner2, 5);
         if (!strcmp(myOwner1.acOL, myOwner2.acOL))
         {
            // Ignore name2 if name1 and name2 have the same first and last name
            if (strcmp(myOwner1.acOF, myOwner2.acOF))
            {
               sprintf(acTmp, "%s %s & %s", myOwner1.acOF, myOwner1.acOM, myOwner2.acSwapName);
               blankRem(acTmp);
               strcpy(myOwner1.acSwapName, acTmp);

               // Merge name1 if not protected
               if (!bKeep)
               {
                  sprintf(acTmp, "%s & %s %s", myOwner1.acName1, myOwner2.acOF, myOwner2.acOM);
                  blankRem(acTmp);
                  strcpy(myOwner1.acName1, acTmp);
                  acSave1[0] = 0;
               } else
                  strcpy(myOwner1.acName2, acSave2);
            }
         } else
            strcpy(myOwner1.acName2, acSave2);
      } else
         strcpy(myOwner1.acName2, acSave2);
   }

   // Keep original name if no merge occured
   if (acSave1[0])
   {
      iTmp = strlen(acSave1);
      memcpy(pOutbuf+OFF_NAME1, acSave1, iTmp);
      if (strstr(acSave1, " A CA ") || strstr(acSave1, " UNTD "))
         memcpy(pOutbuf+OFF_NAME_SWAP, acSave1, iTmp);
      else
         memcpy(pOutbuf+OFF_NAME_SWAP, myOwner1.acSwapName, strlen(myOwner1.acSwapName));
   } else
   {
      memcpy(pOutbuf+OFF_NAME1, myOwner1.acName1, strlen(myOwner1.acName1));
      memcpy(pOutbuf+OFF_NAME_SWAP, myOwner1.acSwapName, strlen(myOwner1.acSwapName));
   }

   if (myOwner1.acName2[0] > ' ')
   {
      myOwner1.acName2[SIZ_NAME2] = 0;
      memcpy(pOutbuf+OFF_NAME2, myOwner1.acName2, strlen(myOwner1.acName2));
   }

   if (acCareOf[0])
      updateCareOf(pOutbuf, acCareOf, strlen(acCareOf));

   if (acVesting[0] > ' ')
   {
      memcpy(pOutbuf+OFF_VEST, acVesting, strlen(acVesting));
      if (!memcmp(acVesting, "EA", 2))
         *(pOutbuf+OFF_ETAL_FLG) = 'Y';
   } else
      iTmp = updateVesting(myCounty.acCntyCode, acOwner1, pOutbuf+OFF_VEST, pOutbuf+OFF_ETAL_FLG);
}

/********************************* Edx_MergeAdr ******************************
 *
 * Return 0 if successful, >0 if error
 *
 *****************************************************************************/

void Edx_MergeAdr(char *pOutbuf, char *pRollRec)
{
   EDX_ROLL *pRec;
   char     acTmp[256], acAddr1[256], acAddr2[256], acUnit[8], *pTmp;
   char     acCity[32], acState[4], acCountry[32];
   int      iTmp, iStrNo;
   bool     bNoMail = false;

   ADR_REC  sMailAdr;
   ADR_REC  sSitusAdr;

   pRec = (EDX_ROLL *)pRollRec;

   // Clear old Mailing
   removeMailing(pOutbuf, false);

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "00121112100", 9))
   //   iTmp = 0;
#endif
   // Check for blank address
   if (memcmp(pRec->M_Addr1, "     ", 5))
   {
      memcpy(acAddr1, pRec->M_Addr1, RSIZ_M_STREET);
      blankRem(acAddr1, RSIZ_M_STREET);
      pTmp = acAddr1;

      // Strip off CareOf
      if (acAddr1[0] == '%' || !memcmp(acAddr1, "C/O", 3))
      {
         pTmp = &acAddr1[1];
         while (*pTmp)
         {
            if (isdigit(*pTmp) || !memcmp(pTmp, "P O", 3) || !memcmp(pTmp, "PO BOX", 6))
               break;
            pTmp++;
         }
      }

      memcpy(pOutbuf+OFF_M_ADDR_D, pTmp, strlen(pTmp));

      // Parsing mail address
      memset((void *)&sMailAdr, 0, sizeof(ADR_REC));
      parseMAdr1(&sMailAdr, acAddr1);
      //parseAdr1U(&sMailAdr, pTmp, false);

      if (sMailAdr.lStrNum > 0)
      {
         sprintf(acTmp, "%d       ", sMailAdr.lStrNum);
         memcpy(pOutbuf+OFF_M_STRNUM, acTmp, SIZ_M_STRNUM);
         if (sMailAdr.strSub[0] > '0')
         {
            sprintf(acTmp, "%s  ", sMailAdr.strSub);
            memcpy(pOutbuf+OFF_M_STR_SUB, acTmp, SIZ_M_STR_SUB);
         }
      }
      memcpy(pOutbuf+OFF_M_DIR, sMailAdr.strDir, strlen(sMailAdr.strDir));
      memcpy(pOutbuf+OFF_M_STREET, sMailAdr.strName, strlen(sMailAdr.strName));
      memcpy(pOutbuf+OFF_M_SUFF, sMailAdr.strSfx, strlen(sMailAdr.strSfx));

      if (sMailAdr.Unit[0] > ' ')
      {
         if (sMailAdr.Unit[0] == '#')
            iTmp = sprintf(acTmp, " %s", sMailAdr.Unit);
         else
            iTmp = sprintf(acTmp, " #%s", sMailAdr.Unit);
         memcpy(pOutbuf+OFF_M_UNITNO, &acTmp[1], iTmp-1);
         strcat(acAddr1, acTmp);
      }

      iTmp = atoin(pRec->M_Zip, RSIZ_M_ZIP);
      if (iTmp > 500)
      {
         memcpy(pOutbuf+OFF_M_ZIP, pRec->M_Zip, SIZ_M_ZIP);
         if (pRec->M_ZipSfx != ' ')
            sprintf(acTmp, "%.5s%c", pRec->M_Zip, pRec->M_ZipSfx);
         else
            sprintf(acTmp, "%.5s", pRec->M_Zip);
         iTmp = locateCity(acTmp, acCity, acState, acCountry);
         if (iTmp > 0)
         {
            acCity[SIZ_M_CITY] = 0;
            memcpy(pOutbuf+OFF_M_CITY, acCity, strlen(acCity));

            if (acState[0] > ' ')
            {
               memcpy(pOutbuf+OFF_M_ST, acState, SIZ_M_ST);
               iTmp = sprintf(acAddr2, "%s %.2s %.5s", acCity, acState, pRec->M_Zip);
            } else
               iTmp = sprintf(acAddr2, "%s %s", acCity, acCountry);

            blankRem(acAddr2);
            memcpy(pOutbuf+OFF_M_CTY_ST_D, acAddr2, strlen(acAddr2));
         }
      }
   } else
      bNoMail = true;

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "0040084013", 10) )
   //   iTmp = 0;
#endif

   // Situs
   acUnit[0] = 0;
   acAddr1[0] = 0;
   acAddr2[0] = 0;
   memset((void *)&sSitusAdr, 0, sizeof(ADR_REC));

   // Fraction - may be in the wrong spot
   if (memcmp(pRec->S_UnitNum,  "0000", 4))
   {
      if (memcmp(pRec->S_UnitType, "TEMP", 4) 
          && memcmp(pRec->S_UnitNum,  "TEMP", 4)
          && memcmp(pRec->S_UnitType, "CELL", 4)
          && memcmp(pRec->S_UnitNum,  "CELL", 4)
          && memcmp(pRec->S_UnitNum,  "TOWE", 4)
          && memcmp(pRec->S_UnitNum,  "MAIN", 4)
          && memcmp(pRec->S_UnitType, "MAIN", 4)
          && memcmp(pRec->S_UnitNum,  "BELL", 4)
          && memcmp(pRec->S_UnitNum,  "LAND", 4)
          && memcmp(pRec->S_UnitNum,  "COMP", 4)
          && memcmp(pRec->S_UnitNum,  "FRNT", 4)
          && memcmp(pRec->S_UnitNum,  "TRLR", 4)
          && memcmp(pRec->S_UnitNum,  "WATE", 4)
          && memcmp(pRec->S_UnitType, "WATE", 4)
          && memcmp(pRec->S_UnitNum,  "PARK", 4)
          && memcmp(pRec->S_UnitType, "CARE", 4)
          && memcmp(pRec->S_UnitNum,  "STOR", 4)
          && memcmp(pRec->S_UnitNum,  "WEST", 4)
          && memcmp(pRec->S_UnitNum,  "CLUB", 4)
          && memcmp(pRec->S_UnitType, "CLUB", 4)
          && memcmp(pRec->S_UnitNum,  "THEA", 4)
          && memcmp(pRec->S_UnitNum,  "LIFT", 4)
          && memcmp(pRec->S_UnitType, "LIFT", 4)
          && memcmp(pRec->S_UnitNum,  "GATE", 4)
          && memcmp(pRec->S_UnitNum,  "ELEC", 4)
          && memcmp(pRec->S_UnitType, "OPEN", 4)
          && memcmp(pRec->S_UnitType, "REST", 4)
          && memcmp(pRec->S_UnitNum,  "METE", 4))
      {
         memcpy(acTmp, pRec->S_UnitNum, RSIZ_S_UNIT_NUMBER);
         acTmp[RSIZ_S_UNIT_NUMBER] = 0;
         pTmp = acTmp;
         while (*pTmp == '0')
            pTmp++;
         strcpy(acUnit, pTmp);
      } else
      {
         memset(pOutbuf+OFF_S_UNITNO, ' ', SIZ_S_UNITNO);   // Clear old unit#
      }
   }

   // Copy StrNum
   iStrNo = atoin(pRec->S_StrNum, RSIZ_S_STRNUM);
   if (iStrNo > 0)
   {
      sprintf(acAddr1, "%d       ", iStrNo);
      memcpy(pOutbuf+OFF_S_STRNUM, acAddr1, SIZ_S_STRNUM);

      if (pRec->S_StrFra[0] > ' ')
      {
         memcpy(pOutbuf+OFF_S_STR_SUB, pRec->S_StrFra, RSIZ_S_STRFRA);
         if (pRec->S_StrFra[0] == '/' || pRec->S_StrFra[0] == '-')
            sprintf(acAddr1, "%d%.3s ", iStrNo, pRec->S_StrFra);
         else
            sprintf(acAddr1, "%d-%.3s ", iStrNo, pRec->S_StrFra);
         memcpy(pOutbuf+OFF_S_HSENO, acAddr1, strlen(acAddr1));
         //LogMsg0("...%.10s...%s", pOutbuf, acAddr1);
      } else if (acUnit[0] == '/' || acUnit[0] == '-')
      {
         memcpy(pOutbuf+OFF_S_HSENO, acAddr1, SIZ_S_STRNUM);
         memcpy(pOutbuf+OFF_S_STR_SUB, acUnit, RSIZ_S_STRFRA);
         sprintf(acAddr1, "%d%.3s ", iStrNo, pRec->S_StrFra);
         //LogMsg0("...%.10s...%s", pOutbuf, acAddr1);
      } else
         memcpy(pOutbuf+OFF_S_HSENO, acAddr1, SIZ_S_STRNUM);
   }

   if (pRec->S_StrName[0] > ' ')
   {
      // Copy street name
      memcpy(pOutbuf+OFF_S_STREET, pRec->S_StrName, SIZ_S_STREET);

      if (pRec->S_Dir[0] >= 'E')
         memcpy(pOutbuf+OFF_S_DIR, pRec->S_Dir, SIZ_S_DIR);
      else
         memset(pOutbuf+OFF_S_DIR, ' ', SIZ_S_DIR);

      if (pRec->S_StrSfx[0] > ' ')
      {
         memcpy(acTmp, pRec->S_StrSfx, RSIZ_S_STRTYPE);
         blankRem(acTmp, RSIZ_S_STRTYPE);
         iTmp = GetSfxDev(acTmp);
         if (iTmp)
         {
            sprintf(acTmp, "%d      ", iTmp);
            memcpy(pOutbuf+OFF_S_SUFF, acTmp, SIZ_S_SUFF);
         }
      }

      // Format address line 1
      if (pRec->S_Dir[0] >= 'E')
         sprintf(acAddr1, "%s %.2s %.25s %.2s", acAddr1, pRec->S_Dir, pRec->S_StrName, pRec->S_StrSfx);
      else
         sprintf(acAddr1, "%s %.25s %.2s", acAddr1, pRec->S_StrName, pRec->S_StrSfx);
      blankRem(acAddr1);

      //memcpy(acUnit, pRec->S_UnitNum, RSIZ_S_UNIT_NUMBER);
      //acUnit[RSIZ_S_UNIT_NUMBER] = 0;
      //pTmp = acUnit;
      //while (*pTmp == '0')
      //   pTmp++;

      // Ignore certain unit type
      pTmp = acUnit;
      if (*pTmp)
      {
         if (*pTmp != '-' && *pTmp != '/')
         {
            if (*pTmp == '#')
               pTmp++;
            iTmp = sprintf(acTmp, " #%s", pTmp);
            memcpy(pOutbuf+OFF_S_UNITNO, &acTmp[1], iTmp-1);
            strcat(acAddr1, acTmp);
         }
      }
      blankPad(acAddr1, SIZ_S_ADDR_D);
      memcpy(pOutbuf+OFF_S_ADDR_D, acAddr1, SIZ_S_ADDR_D);
   }

   // state
   memcpy(pOutbuf+OFF_S_ST, "CA", 2);

   // City
   acCity[0] = 0;
   iTmp = atoin(pRec->TRA, 3);
   if (iTmp == 1)
   {
      strcpy(acCity, "PLACERVILLE");
      strcpy(sSitusAdr.City, "1");
      memcpy(pOutbuf+OFF_S_ZIP, "95667", SIZ_S_ZIP);
   } else if (iTmp == 2)
   {
      strcpy(acCity, "SOUTH LAKE TAHOE");
      strcpy(sSitusAdr.City, "2");
      memcpy(pOutbuf+OFF_S_ZIP, "96150", SIZ_S_ZIP);
   } else if (!bNoMail && sMailAdr.City[0] > ' ' &&
            !memcmp(pOutbuf+OFF_S_STRNUM, pOutbuf+OFF_M_STRNUM, SIZ_M_STRNUM) &&
            !memcmp(pOutbuf+OFF_S_STREET, pOutbuf+OFF_M_STREET, 8) )
   {
      // Use mail city and unit# for situs if they have the same addr
      strcpy(acCity, sMailAdr.City);
      City2Code(acCity, sSitusAdr.City);
      if (pRec->M_Zip[0] == '9' && sSitusAdr.City[0] > ' ')
         memcpy(pOutbuf+OFF_S_ZIP, pRec->M_Zip, SIZ_S_ZIP);
   }

   if (acCity[0] >= 'A')
   {
      iTmp = sprintf(acAddr2, "%s CA", acCity);
      memcpy(pOutbuf+OFF_S_CTY_ST_D, acAddr2, iTmp);
      memcpy(pOutbuf+OFF_S_CITY, sSitusAdr.City, strlen(sSitusAdr.City));

      /* Do this at MergeAdr
      // Populate mailing address with situs.
      if (bNoMail && pRec->S_StrName[0] > ' ')
      {
         // Do this at the MergeAdr step instead.
         memcpy(pOutbuf+OFF_M_STRNUM, pOutbuf+OFF_S_STRNUM, SIZ_S_STRNUM);
         memcpy(pOutbuf+OFF_M_STR_SUB, pOutbuf+OFF_S_STR_SUB, SIZ_M_STR_SUB);
         memcpy(pOutbuf+OFF_M_DIR, pOutbuf+OFF_S_DIR, SIZ_M_DIR);
         memcpy(pOutbuf+OFF_M_STREET, pOutbuf+OFF_S_STREET, SIZ_M_STREET);
         memcpy(pOutbuf+OFF_M_UNITNO, pOutbuf+OFF_S_UNITNO, SIZ_S_UNITNO);

         // Translate suffix
         iTmp = atoin(pOutbuf+OFF_S_SUFF, 3);
         if (iTmp > 0)
         {
            iTmp = sprintf(acTmp, "%s", GetSfxStr(iTmp));
            memcpy(pOutbuf+OFF_M_SUFF, acTmp, iTmp);
         }

         iTmp = strlen(acCity);
         memcpy(pOutbuf+OFF_M_CITY, acCity, iTmp);
         memcpy(pOutbuf+OFF_M_ST, pOutbuf+OFF_S_ST, SIZ_M_ST);
         memcpy(pOutbuf+OFF_M_ZIP, pOutbuf+OFF_S_ZIP, SIZ_M_ZIP);
         memcpy(pOutbuf+OFF_M_ADDR_D, pOutbuf+OFF_S_ADDR_D, SIZ_M_ADDR_D);
         memcpy(pOutbuf+OFF_M_CTY_ST_D, pOutbuf+OFF_S_CTY_ST_D, SIZ_M_CTY_ST_D);
      }
      */
   }
}

/********************************* Edx_MergeChar *****************************
 *
 *
 *
 *****************************************************************************/

int Edx_MergeChar(char *pOutbuf)
{
   static   char acRec[128], *pRec=NULL;
   char     acTmp[256], acCode[32];
   long     lTmp, lBldgSqft, lGarSqft;
   int      iRet, iTmp, iLoop, iBeds, iFBath, iHBath, iFp;
   EDX_CHAR *pChar;

   iRet=iBeds=iFBath=iHBath=iFp=0;
   lBldgSqft=lGarSqft=0;

   // Get first Char rec for first call
   if (!pRec && !lCharMatch)
   {
      pRec = fgets(acRec, 128, fdChar);
   }

   pChar = (EDX_CHAR *)pRec;

   do
   {
      if (!pRec)
      {
         fclose(fdChar);
         fdChar = NULL;
         return 1;      // EOF
      }

      // Compare Apn
      iLoop = memcmp(pOutbuf, pChar->Apn, myCounty.iApnLen);
      if (iLoop > 0)
      {
         if (bDebug)
            LogMsg0("Skip Char rec  %.*s", myCounty.iApnLen, pChar->Apn);
         pRec = fgets(acRec, 1024, fdChar);
         lCharSkip++;
      }
   } while (iLoop > 0);

   // If not match, return
   if (iLoop)
      return 0;

   // Quality Class
   iTmp = atoin((char *)&pChar->QualityClass[0], 3);
   if (iTmp > 0)
   {
      if (iTmp >= 100)
         sprintf(acTmp, "%.2s.%c", pChar->QualityClass, pChar->QualityClass[2]);
      else
         sprintf(acTmp, "%c.%c", pChar->QualityClass[1], pChar->QualityClass[2]);
      iRet = Value2Code(acTmp, acCode, NULL);
      blankPad(acCode, SIZ_BLDG_QUAL);
      memcpy(pOutbuf+OFF_BLDG_QUAL, acCode, SIZ_BLDG_QUAL);
   }

   // Stories
   iTmp = atoin(pChar->Num_Stories, CSIZ_STORIES);
   if (iTmp > 0 && iTmp < 99)
   {
      if (iTmp > 9 && bDebug)
         LogMsg("%.*s : %d", iApnLen, pOutbuf, iTmp);
      sprintf(acTmp, "%d.0  ", iTmp);
      memcpy(pOutbuf+OFF_STORIES, acTmp, SIZ_STORIES);
   }

   // Beds
   iBeds = atoin(pChar->NumBedrooms, CSIZ_BEDROOMS);
   if (iBeds > 0)
   {
      sprintf(acTmp, "%*u", SIZ_BEDS, iBeds);
      memcpy(pOutbuf+OFF_BEDS, acTmp, SIZ_BEDS);
   }

   // Bath
   iFBath = atoin(pChar->NumBaths, CSIZ_BATHS-1);
   if (iFBath > 0)
   {
      sprintf(acTmp, "%*u", SIZ_BATH_F, iFBath);
      memcpy(pOutbuf+OFF_BATH_F, acTmp, SIZ_BATH_F);

      // Half bath
      if (pChar->NumBaths[CSIZ_BATHS-1] == '5')
         memcpy(pOutbuf+OFF_BATH_H, " 1", 2);
   }

   // Rooms
   iTmp = atoin(pChar->Total_Rooms, CSIZ_ROOMS);
   if (iTmp > 0)
   {
      sprintf(acTmp, "%*u", SIZ_ROOMS, iTmp);
      memcpy(pOutbuf+OFF_ROOMS, acTmp, SIZ_ROOMS);
   }

   // BldgSqft
   lBldgSqft = atoin(pChar->LivArea, CSIZ_LIVAREA);
   if (lBldgSqft > 10)
   {
      sprintf(acTmp, "%*u", SIZ_BLDG_SF, lBldgSqft);
      memcpy(pOutbuf+OFF_BLDG_SF, acTmp, SIZ_BLDG_SF);
   }

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "001012201", 9))
   //   iTmp = 0;
#endif
   // Units
   iTmp = atoin(pChar->Units, CSIZ_UNITS);
   if (iTmp > 0)
   {
      sprintf(acTmp, "%*u", SIZ_UNITS, iTmp);
      memcpy(pOutbuf+OFF_UNITS, acTmp, SIZ_UNITS);
   }

   // YrBlt
   lTmp = atoin(pChar->YearBuilt, CSIZ_YRBLT);
   if (lTmp > 1700)
      memcpy(pOutbuf+OFF_YR_BLT, pChar->YearBuilt, SIZ_YR_BLT);

   // YrEff
   lTmp = atoin(pChar->EffYear, CSIZ_EFFYR);
   if (lTmp > 1700)
      memcpy(pOutbuf+OFF_YR_EFF, pChar->EffYear, SIZ_YR_BLT);

   // Condition
   if (pChar->Imprv_Condition_Code > '0')
      *(pOutbuf+OFF_IMPR_COND) = pChar->Imprv_Condition_Code;

   // Lot sqft - Lot Acres
   if (*(pOutbuf+OFF_LOT_SQFT+8) == ' ')
   {
      lTmp = atoin(pChar->Lot_Sqft, CSIZ_LOT_SQFT);
      if (lTmp > 0)
      {
         sprintf(acTmp, "%*u", SIZ_LOT_SQFT, lTmp);
         memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);
         lTmp = (lTmp*ACRES_FACTOR)/SQFT_PER_ACRE;
         sprintf(acTmp, "%*u", SIZ_LOT_SQFT, lTmp);
         memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_SQFT);
      }
   }

   // Water
   if (pChar->WaterSource > ' ')
      *(pOutbuf+OFF_WATER) = pChar->WaterSource;

   // Sewer.
   if (pChar->Sewer > ' ')
      *(pOutbuf+OFF_SEWER) = pChar->Sewer;

   // View .
   if (pChar->WaterFront == 'Y')
      *(pOutbuf+OFF_VIEW) = 'W';
   else if (pChar->View > ' ')
   {
      switch (pChar->View)
      {
         case 'A':
            *(pOutbuf+OFF_VIEW) = '3';
            break;
         case 'E':
            *(pOutbuf+OFF_VIEW) = '5';
            break;
         case 'F':
            *(pOutbuf+OFF_VIEW) = '2';
            break;
         case 'G':
            *(pOutbuf+OFF_VIEW) = '4';
            break;
      }
   }

   // Others

   lCharMatch++;

   // Get next Char rec
   pRec = fgets(acRec, 128, fdChar);

   return 0;
}

/******************************** Edx_MergeSale ******************************
 *
 * Return 0 - No update
 *        1 - New sale
 *
 *****************************************************************************/

int Edx_MergeSale(char *pOutbuf, char *pRollRec, char *pSaleRec)
{
   long     lCurSaleDt, lLstSaleDt, lPrice, lLastAmt, lTax;
   char     acTmp[32], acDocNum[16];
   int      iRet;

   EDX_ROLL  *pRec = (EDX_ROLL *)pRollRec;
   SCSAL_REC *pRec1= (SCSAL_REC *)pSaleRec;

   lCurSaleDt = atoin(pRec->Cur_DocDate, SIZ_SALE1_DT);
   lLstSaleDt = atoin(pOutbuf+OFF_SALE1_DT, SIZ_SALE1_DT);

   if (lCurSaleDt < lLstSaleDt)
      return 0;

   // Format DocNum
   iRet = Edx_FormatDoc(acDocNum, pRec->Cur_DocNum, pRec->Cur_DocDate);
   if (!iRet)
      return 0;        // No or invalid DocNum

   if (iRet > 8)
      LogMsg("??? Questionable DocNum: %s --> APN=%.12s", acDocNum, pOutbuf);

   // Calculate sale price
   lTax = atoin(pRec->DocTax, RSIZ_DTT_AMT);
   if (lTax > 0)
      lPrice = (long)((double)lTax * SALE_FACTOR_100);
   else
      lPrice = 0;

   if (lCurSaleDt == lLstSaleDt)
   {
      lLastAmt = atoin(pOutbuf+OFF_SALE1_AMT, SIZ_SALE1_AMT);
      if (!lTax || lLastAmt > 0)
         return 0;
      else
         LogMsg("* Update existing sale1 for %.14s", pOutbuf);
   }

   // Update current sale
   memset(pSaleRec, ' ', sizeof(SCSAL_REC));
   if (lPrice > 5000)
   {
      // Move sale2 to sale3
      memcpy(pOutbuf+OFF_SALE3_DOC, pOutbuf+OFF_SALE2_DOC, SIZ_SALE3_DOC);
      memcpy(pOutbuf+OFF_SALE3_DT, pOutbuf+OFF_SALE2_DT, SIZ_SALE3_DT);
      memcpy(pOutbuf+OFF_SALE3_DOCTYPE, pOutbuf+OFF_SALE2_DOCTYPE, SIZ_SALE3_DOCTYPE);
      memcpy(pOutbuf+OFF_SALE3_AMT, pOutbuf+OFF_SALE2_AMT, SIZ_SALE3_AMT);
      *(pOutbuf+OFF_AR_CODE3) = *(pOutbuf+OFF_AR_CODE2);

      // Move sale1 to sale2
      memcpy(pOutbuf+OFF_SALE2_DOC, pOutbuf+OFF_SALE1_DOC, SIZ_SALE3_DOC);
      memcpy(pOutbuf+OFF_SALE2_DT, pOutbuf+OFF_SALE1_DT, SIZ_SALE3_DT);
      memcpy(pOutbuf+OFF_SALE2_DOCTYPE, pOutbuf+OFF_SALE1_DOCTYPE, SIZ_SALE3_DOCTYPE);
      memcpy(pOutbuf+OFF_SALE2_AMT, pOutbuf+OFF_SALE1_AMT, SIZ_SALE3_AMT);
      *(pOutbuf+OFF_AR_CODE2) = *(pOutbuf+OFF_AR_CODE1);

      memcpy(pOutbuf+OFF_SALE1_DOC, acDocNum, 8);
      memcpy(pOutbuf+OFF_SALE1_DT, pRec->Cur_DocDate, SIZ_SALE1_DT);
      memset(pOutbuf+OFF_SALE1_CODE, ' ', SALE_SIZ_SALECODE);
      memcpy(pOutbuf+OFF_SALE1_DOCTYPE, "1  ", 3);
      sprintf(acTmp, "%*u", SIZ_SALE1_AMT, lPrice);
      memcpy(pOutbuf+OFF_SALE1_AMT, acTmp, SIZ_SALE1_AMT);

      // Update sale rec
      memcpy(pRec1->DocType,   "1  ", 3);
      memcpy(pRec1->SalePrice, acTmp, SIZ_SALE1_AMT);
      iRet = sprintf(acTmp, "%.2f", (double)lTax/100.0);
      memcpy(pRec1->StampAmt,  acTmp, iRet);
   }

   // Update transfers
   memcpy(pOutbuf+OFF_TRANSFER_DOC, acDocNum, 8);
   memcpy(pOutbuf+OFF_TRANSFER_DT, pRec->Cur_DocDate, SIZ_TRANSFER_DT);
   *(pOutbuf+OFF_AR_CODE1) = 'A';

   memcpy(pRec1->Apn,      pOutbuf+OFF_APN_S, SALE_SIZ_APN);
   memcpy(pRec1->DocNum,   acDocNum,          8);
   memcpy(pRec1->DocDate,  pRec->Cur_DocDate, SALE_SIZ_DOCDATE);

   pRec1->CRLF[0] = '\n';
   pRec1->CRLF[1] = '\0';

   return 1;
}

/******************************** Edx_XlatUseCode ****************************
 *
 * Return pointer to the correct use code.
 *
 *****************************************************************************/

char *Edx_XlatUseCode(char *pCntyUse)
{
   int   iTmp;

   iTmp = 0;
   while (Edx_UseTbl[iTmp].acCntyUse[0])
   {
      if (!strcmp(Edx_UseTbl[iTmp].acCntyUse, pCntyUse))
         return Edx_UseTbl[iTmp].acStdUse;
      else
         iTmp++;
   }
   return NULL;
}

/********************************* Edx_MergeRoll *****************************
 *
 * Return -1 if record is inactive
 *         1 if record has new sale update
 *         0 if record has no new sale
 *
 *****************************************************************************/

int Edx_MergeRoll(char *pOutbuf, char *pRollRec, char *pSaleRec, int iFlag)
{
   char     acTmp[256], acTmp1[256], *pTmp;
   LONGLONG lTmp;
   int      iRet, iStatus;

   EDX_ROLL *pRec = (EDX_ROLL *)pRollRec;

   // 00    = Active
   // 01-89 = Active nonbillable
   // 90-XX = deleted
   iStatus = atoin(pRec->Status, RSIZ_STATUS);
   if (pRec->RecordCode != '2' || iStatus > 89)
      return -1;

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "02555211100", 9) )
   //   iRet = 0;
#endif

   if (iFlag & CREATE_R01)
   {
      // Clear output buffer
      memset(pOutbuf, ' ', iRecLen);

      // Start copying data
      memcpy(pOutbuf, pRec->Apn, iApnLen);
      memcpy(pOutbuf+OFF_CO_NUM, "09EDX", 5);

      // Format APN
      iRet = formatApn(pRec->Apn, acTmp, &myCounty);
      memcpy(pOutbuf+OFF_APN_D, acTmp, iRet);

      // Create MapLink
      iRet = formatMapLink(pRec->Apn, acTmp, &myCounty);
      memcpy(pOutbuf+OFF_MAPLINK, acTmp, iRet);

      // Create index map link
      if (getIndexPage(acTmp, acTmp1, &myCounty))
         memcpy(pOutbuf+OFF_IMAPLINK, acTmp1, iRet);

      // Year assessed
      memcpy(pOutbuf+OFF_YR_ASSD, myCounty.acYearAssd, 4);

      // HO Exempt
      /*
        01 - HOMEOWNERS          02 - VETERANS
        03 - BUSINESS INVENTORY  04 - WELFARE
        05 - CHURCH              06 - COLLEGE
        07 - CEMETERY            08 - DISABLED VETERANS
        09 - CIVIC               10 - PUBLIC SCHOOLS
        11 - LESSEE/LESSOR       12 - RELIGIOUS
        13 - FREE MUSEUM/LIBRARY 14 - HISTORICAL AIRCRAFT
      */

      if (!memcmp(pRec->Exe1_Code, "01", 2) ||
          !memcmp(pRec->Exe2_Code, "01", 2) ||
          !memcmp(pRec->Exe3_Code, "01", 2) )
         *(pOutbuf+OFF_HO_FL) = '1';      // 'Y'
      else
         *(pOutbuf+OFF_HO_FL) = '2';      // 'N'

      // Exemp total
      lTmp = atoin(pRec->Exe1_Amt, RSIZ_EXEMP_AMT);
      lTmp += atoin(pRec->Exe2_Amt, RSIZ_EXEMP_AMT);
      lTmp += atoin(pRec->Exe3_Amt, RSIZ_EXEMP_AMT);
      if (lTmp > 0)
      {
         sprintf(acTmp, "%*u", SIZ_EXE_TOTAL, lTmp);
         memcpy(pOutbuf+OFF_EXE_TOTAL, acTmp, SIZ_EXE_TOTAL);
      }

      // Land
      long lLand = atoin(pRec->Land, RSIZ_LAND);
      if (lLand > 0)
      {
         sprintf(acTmp, "%*u", SIZ_LAND, lLand);
         memcpy(pOutbuf+OFF_LAND, acTmp, SIZ_LAND);
      }

      // Improve
      long lImpr = atoin(pRec->Structure, RSIZ_STRUCTURES);
      if (lImpr > 0)
      {
         sprintf(acTmp, "%*u", SIZ_IMPR, lImpr);
         memcpy(pOutbuf+OFF_IMPR, acTmp, SIZ_IMPR);
      }

      // Other value
      long lFixture = atoin(pRec->Fixt_Val, RSIZ_FIXED_EQUIP);
      long lPers    = atoin(pRec->Other_PP_Val, RSIZ_OTH_PERS_PROP);
      long lTimber  = atoin(pRec->Timber, RSIZ_OTH_PERS_PROP);
      long lMineral = atoin(pRec->Mineral, RSIZ_OTH_PERS_PROP);
      long lPlant   = atoin(pRec->Plants, RSIZ_OTH_PERS_PROP);
      long lBusInv  = atoin(pRec->Bus_Inv, RSIZ_OTH_PERS_PROP);
      long lOther   = atoin(pRec->OtherImpr, RSIZ_OTH_PERS_PROP);
      lTmp = lPers + lFixture + lTimber + lMineral + lPlant + lBusInv + lOther;
      if (lTmp > 0)
      {
         sprintf(acTmp, "%*u", SIZ_OTHER, lTmp);
         memcpy(pOutbuf+OFF_OTHER, acTmp, SIZ_OTHER);

         if (lPers > 0)
         {
            sprintf(acTmp, "%d         ", lPers);
            memcpy(pOutbuf+OFF_PERSPROP, acTmp, SIZ_PERSPROP);
         }
         if (lFixture > 0)
         {
            sprintf(acTmp, "%d         ", lFixture);
            memcpy(pOutbuf+OFF_FIXTR, acTmp, SIZ_FIXTR);
         }
         if (lTimber > 0)
         {
            sprintf(acTmp, "%d         ", lTimber);
            memcpy(pOutbuf+OFF_TIMBER_VAL, acTmp, SIZ_TIMBER_VAL);
         }
         if (lMineral > 0)
         {
            sprintf(acTmp, "%d         ", lMineral);
            memcpy(pOutbuf+OFF_MINERAL, acTmp, SIZ_MINERAL);
         }
         if (lPlant > 0)
         {
            sprintf(acTmp, "%d         ", lPlant);
            memcpy(pOutbuf+OFF_TREEVINES, acTmp, SIZ_TREEVINES);
         }
         if (lBusInv > 0)
         {
            sprintf(acTmp, "%d         ", lBusInv);
            memcpy(pOutbuf+OFF_BUSINV, acTmp, SIZ_BUSINV);
         }
         if (lOther > 0)
         {
            sprintf(acTmp, "%d         ", lOther);
            memcpy(pOutbuf+OFF_OTH_IMPR, acTmp, SIZ_OTH_IMPR);
         }
      }

      // Gross total
      lTmp += lLand+lImpr;
      if (lTmp > 0)
      {
         sprintf(acTmp, "%*u", SIZ_GROSS, lTmp);
         memcpy(pOutbuf+OFF_GROSS, acTmp, SIZ_GROSS);

         // Ratio
         if (lImpr > 0)
         {
            sprintf(acTmp, "%*u", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
            memcpy(pOutbuf+OFF_RATIO, acTmp, SIZ_RATIO);
         }
      }
   }

   // TRA
   memcpy(pOutbuf+OFF_TRA, pRec->TRA, RSIZ_TRA);

   // Status
   if (iStatus == 0)
      *(pOutbuf+OFF_STATUS) = 'A';
   else if (iStatus == 6)
      *(pOutbuf+OFF_STATUS) = 'S';
   else if (iStatus == 11)
      *(pOutbuf+OFF_STATUS) = 'I';
   else if (iStatus < 90)
      *(pOutbuf+OFF_STATUS) = 'O';
   else
      *(pOutbuf+OFF_STATUS) = 'R';

   // Merge Addr
   Edx_MergeAdr(pOutbuf, pRollRec);

   // UseCode
   memcpy(pOutbuf+OFF_USE_CO, pRec->Pri_UseCode, RSIZ_PRI_USECODE+RSIZ_SEC_USECODE);
   if (pRec->Pri_UseCode[0] > ' ')
   {
      if (iNumUseCodes > 0)
      {
         memcpy(acTmp, pRec->Pri_UseCode, RSIZ_PRI_USECODE);
         acTmp[RSIZ_PRI_USECODE] = 0;
         //pTmp = Edx_XlatUseCode(acTmp);
         pTmp = Cnty2StdUse(acTmp);
         if (pTmp)
            memcpy(pOutbuf+OFF_USE_STD, pTmp, 3);
         else if (pRec->Sec_UseCode[0] > ' ')
         {
            memcpy(acTmp, pRec->Sec_UseCode, RSIZ_SEC_USECODE);
            acTmp[RSIZ_SEC_USECODE] = 0;
            pTmp = Cnty2StdUse(acTmp);
            if (pTmp)
               memcpy(pOutbuf+OFF_USE_STD, pTmp, 3);
            else
            {
               memcpy(pOutbuf+OFF_USE_STD, USE_MISC, 3);
               LogMsg0("+++ Unknown Usecode: %s", acTmp);
            }
         } else
         {
            memcpy(pOutbuf+OFF_USE_STD, USE_MISC, 3);
            LogMsg0("+++ Unknown Usecode: %s", acTmp);
         }
      }
   } else if (pRec->Sec_UseCode[0] > ' ')
   {
      if (iNumUseCodes > 0)
      {
         memcpy(acTmp, pRec->Sec_UseCode, RSIZ_SEC_USECODE);
         acTmp[RSIZ_SEC_USECODE] = 0;
         pTmp = Cnty2StdUse(acTmp);
         if (pTmp)
            memcpy(pOutbuf+OFF_USE_STD, pTmp, 3);
      }
   } else
      memcpy(pOutbuf+OFF_USE_STD, USE_UNASGN, SIZ_USE_STD);

   // Owner
   Edx_MergeOwner(pOutbuf, pRollRec);

   // Legal
   if (pRec->LeadLine[0] > ' ')
   {
      memcpy(acTmp, pRec->LeadLine, RSIZ_LEAD_LINE);
      acTmp[RSIZ_LEAD_LINE] = 0;
      lTmp = atoin(pRec->SubdivNum, RSIZ_SUBDIV_NUMBER);
      if (lTmp > 0)
         sprintf(&acTmp[RSIZ_LEAD_LINE], " SUBDIV NMBR %d", lTmp);

      iRet = updateLegal(pOutbuf, acTmp);
      if (iRet > iMaxLegal)
         iMaxLegal = iRet;
   }

   // Zoning
   if (pRec->Zoning[6] > ' ')
   {
      // Do this to drop bad character from county file
      for (int i = 0; i < 4; i++)
      {
         if (isalnum(pRec->Zoning[i+6]))
            *(pOutbuf+OFF_ZONE+i) = pRec->Zoning[i+6];
      }
   }

   // Acres
   lTmp = atoin(pRec->Acres, RSIZ_ACRES);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*u", SIZ_LOT_ACRES, lTmp);
      memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);

      lTmp = (long)((double)lTmp * SQFT_FACTOR_1000 + 0.5);
      sprintf(acTmp, "%*u", SIZ_LOT_SQFT, lTmp);
      memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);
   } else
   {
      double dTmp;

      pTmp = strchr(pRec->LeadLine, ' ');
      dTmp = atof(pRec->LeadLine);
      if (*(pTmp+1) == 'A' && dTmp > 0.0)
      {
         lTmp = (long)(dTmp * 1000.0);
         sprintf(acTmp, "%*u", SIZ_LOT_ACRES, lTmp);
         memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);

         lTmp = (long)((double)lTmp * SQFT_FACTOR_1000 + 0.5);
         sprintf(acTmp, "%*u", SIZ_LOT_SQFT, lTmp);
         memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);
      }
   }

   // Transfer - YYYYMMDD
   if (memcmp(pRec->Cur_DocDate, "000000", 6))
      iRet = Edx_MergeSale(pOutbuf, pRollRec, pSaleRec);
   else
      iRet = 0;

   // Prop8
   if (pRec->Reappr_Code[0] == 'T')
      *(pOutbuf+OFF_PROP8_FLG) = 'Y';

   return iRet;
}

/********************************** Edx_Load_Roll *****************************
 *
 * Load roll file and append new sale to cumsale file.
 *
 ******************************************************************************/

int Edx_Load_Roll(int iSkip)
{
   char     acRec[MAX_RECSIZE], acBuf[MAX_RECSIZE], acRollRec[MAX_RECSIZE];
   char     acSaleRec[1024], acRawFile[_MAX_PATH], acOutFile[_MAX_PATH];

   HANDLE   fhIn, fhOut;
   EDX_ROLL *pRec;

   int      iRet, iTmp, iRollUpd, iNewRec, iRetiredRec;
   DWORD    nBytesRead;
   DWORD    nBytesWritten;
   BOOL     bRet, bEof;
   long     lRet, lCnt=0;

   iNewSale=iRollUpd=iNewRec=iRetiredRec=0;
   sprintf(acRawFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "S01");
   sprintf(acOutFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "R01");

   // Rename files for processing
   if (_access(acRawFile, 0))
   {
      if (!_access(acOutFile, 0))
         rename(acOutFile, acRawFile);
      else
      {
         LogMsgD("***** Missing input file %s.  Please recheck!", acRawFile);
         return 1;
      }
   }

   // Open Roll file
   LogMsg("Open Roll file %s", acRollFile);
   fdRoll = fopen(acRollFile, "r");
   if (fdRoll == NULL)
   {
      LogMsgD("***** Error opening roll file: %s", acRollFile);
      return 2;
   }

   // Open Char file
   if (!_access(acCharFile, 0))
   {
      LogMsg("Open Char file %s", acCharFile);
      fdChar = fopen(acCharFile, "r");
      if (fdChar == NULL)
      {
         LogMsgD("***** Error opening Char file: %s", acCharFile);
         return 2;
      }
   }

   // Open Sales file to append new sale record
   if (!_access(acCSalFile, 0))
   {
      LogMsg("Open Sales file %s", acCSalFile);
      fdSale = fopen(acCSalFile, "a");
      if (fdSale == NULL)
      {
         LogMsgD("***** Error opening Sales file: %s", acCSalFile);
         return 2;
      }
   }

   // Open Input file
   LogMsg("Open input file %s", acRawFile);
   fhIn = CreateFile(acRawFile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhIn == INVALID_HANDLE_VALUE)
   {
      LogMsgD("***** Error opening input file %s in MergeEdxRoll()", acRawFile);
      return 3;
   }

   // Open Output file
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsgD("***** Error creating output file %s in MergeEdxRoll()", acOutFile);
      return 4;
   }

   // Get first RollRec
   do
   {
      iRet = fread(acRollRec, 1, iRollLen, fdRoll);
   } while (iRet == iRollLen && acRollRec[0] < '2');
   bEof = (iRet == iRollLen ? false:true);

   // Copy skip record
   memset(acBuf, ' ', iRecLen);
   while (iSkip-- > 0)
   {
      ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);
      // Ignore record start with 99999999
      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
   }

   iNoMatch=iBadCity=iBadSuffix=0;
   pRec = (EDX_ROLL *)&acRollRec[0];

   // Merge loop
   while (!bEof)
   {
      bRet = ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);

      // Check for EOF
      if (!bRet)
      {
         LogMsgD("***** Error reading input file %s (%f)", acRawFile, GetLastError());
         break;
      }

      // EOF ?
      if (iRecLen != nBytesRead)
         break;

      NextRollRec:
      iTmp = memcmp(acBuf, pRec->Apn, iApnLen);
      if (!iTmp)
      {
         // Merge roll data
         iRet = Edx_MergeRoll(acBuf, acRollRec, acSaleRec, UPDATE_R01);
         if (iRet >= 0)
         {
            iRollUpd++;

            // Update cum Sales
            if (iRet == 1)
            {
               fputs(acSaleRec, fdSale);
               iNewSale++;
            }

            // Merge Char
            if (fdChar)
               iRet = Edx_MergeChar(acBuf);

            // Save last recording date
            lRet = atoin((char *)&acBuf[OFF_TRANSFER_DT], 8);
            if (lRet > lLastRecDate && lRet < lToday)
               lLastRecDate = lRet;

            bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
            lCnt++;
         }

         // Read next roll record
         iRet = fread(acRollRec, 1, iRollLen, fdRoll);
         bEof = (iRet == iRollLen ? false:true);
      } else if (iTmp > 0)       // Roll not match, new roll record?
      {
         // Create new R01 record
         iRet = Edx_MergeRoll(acRec, acRollRec, acSaleRec, CREATE_R01);
         if (iRet >= 0)
         {
            LogMsg0("*** New roll record : %.*s (%d) ***", iApnLen, pRec->Apn, lCnt);
            iNewRec++;

            // Update cum Sales
            if (iRet == 1)
            {
               fputs(acSaleRec, fdSale);
               iNewSale++;
            }

            // Merge Char
            if (fdChar)
               iRet = Edx_MergeChar(acRec);

            // Save last recording date
            lRet = atoin((char *)&acRec[OFF_TRANSFER_DT], 8);
            if (lRet > lLastRecDate && lRet < lToday)
               lLastRecDate = lRet;

            bRet = WriteFile(fhOut, acRec, iRecLen, &nBytesWritten, NULL);
            lCnt++;
         }

         // Get next roll record
         iRet = fread(acRollRec, 1, iRollLen, fdRoll);
         if (iRet != iRollLen)
            bEof = true;    // Signal to stop
         else
            goto NextRollRec;
      } else
      {
         // Record may be retired
         if (bDebug)
            LogMsg0("*** Roll not match (retired record?) : R01->%.*s < Roll->%.*s (%d) ***", iApnLen, acBuf, iApnLen, pRec->Apn, lCnt);
         iRetiredRec++;
         // Remove 20080324 - why keep retired record?
         //bRet = WriteFile(fhOut, acRec, iRecLen, &nBytesWritten, NULL);
         continue;
      }

      if (!(lCnt % 1000))
         printf("\r%u", lCnt);

      if (!bRet)
      {
         LogMsg("Error occurs: %d\n", GetLastError());
         lRet = WRITE_ERR;
         break;
      }
   }

   // Do the rest of the file
   while (!bEof)
   {
      // Create new R01 record
      iRet = Edx_MergeRoll(acRec, acRollRec, acSaleRec, CREATE_R01);
      if (iRet >= 0 && memcmp(pRec->Apn, "999999", 6))
      {
         if (bDebug)
            LogMsg0("*** New roll record : %.*s (%d) ***", iApnLen, pRec->Apn, lCnt);

         iNewRec++;

         // Update cum Sales
         if (iRet == 1)
         {
            fputs(acSaleRec, fdSale);
            iNewSale++;
         }

         // Merge Char
         if (fdChar)
            iRet = Edx_MergeChar(acRec);

         // Save last recording date
         lRet = atoin((char *)&acRec[OFF_TRANSFER_DT], 8);
         if (lRet > lLastRecDate && lRet < lToday)
            lLastRecDate = lRet;

         lCnt++;
         bRet = WriteFile(fhOut, acRec, iRecLen, &nBytesWritten, NULL);
      }

      if (!(lCnt % 1000))
         printf("\r%u", lCnt);

      // Get next roll record
      iRet = fread(acRollRec, 1, iRollLen, fdRoll);
      bEof = (iRet == iRollLen ? false:true);
   }

   // Close files
   if (fdRoll)
      fclose(fdRoll);
   if (fdChar)
      fclose(fdChar);
   if (fdSale)
      fclose(fdSale);

   if (fhOut)
      CloseHandle(fhOut);
   if (fhIn)
      CloseHandle(fhIn);

   LogMsgD("Total output records:       %u", lCnt);
   LogMsg("Total new records:          %u", iNewRec);
   LogMsg("Total retired records:      %u", iRetiredRec);
   LogMsg("Total bad-city records:     %u", iBadCity);
   LogMsg("Total bad-suffix records:   %u\n", iBadSuffix);
   LogMsg("Number of Char matched:     %u", lCharMatch);
   LogMsg("Number of Char skiped:      %u", lCharSkip);
   LogMsg("Number of new Sales:        %u", iNewSale);

   lRecCnt = lCnt;
   return 0;
}

/********************************* Edx_MergeDaily *****************************
 *
 * Update roll file using daily update file
 *
 ******************************************************************************/

int Edx_MergeDaily(int iSkip)
{
   char     acRec[MAX_RECSIZE], acBuf[MAX_RECSIZE], acRollRec[MAX_RECSIZE];
   char     acSaleRec[1024], acRawFile[_MAX_PATH], acOutFile[_MAX_PATH];

   HANDLE   fhIn, fhOut;
   EDX_ROLL *pRec;

   int      iRet, iTmp, iRollUpd, iNewRec, iRetiredRec;
   DWORD    nBytesRead;
   DWORD    nBytesWritten;
   BOOL     bRet, bEof;
   long     lRet=0, lCnt=0;

   iNewSale=iRollUpd=iNewRec=iRetiredRec=0;
   sprintf(acRawFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "R01");
   sprintf(acOutFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "TMP");

   // Rename files for processing
   // For daily processing, we check R01 first.  If not exist, use S01
   if (_access(acRawFile, 0))
   {
      sprintf(acRawFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "S01");
      if (_access(acRawFile, 0))
      {
         LogMsgD("***** Missing input file %s.  Please recheck!", acRawFile);
         return -1;
      }
   }

   // Open Roll file
   LogMsg("Open Daily update file %s", acRollFile);
   fdRoll = fopen(acRollFile, "r");
   if (fdRoll == NULL)
   {
      LogMsgD("***** Error opening roll file: %s", acRollFile);
      return -2;
   }

   // Open Sales file
   if (!_access(acCSalFile, 0))
   {
      LogMsg("Open Sales file %s", acCSalFile);
      fdSale = fopen(acCSalFile, "a");
      if (fdSale == NULL)
      {
         LogMsg("***** Error opening Sales file: %s\n", acCSalFile);
         return -2;
      }
   }

   // Open Input file
   LogMsg("Open input file %s", acRawFile);
   fhIn = CreateFile(acRawFile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhIn == INVALID_HANDLE_VALUE)
   {
      LogMsgD("***** Error opening input file %s in Edx_MergeDaily()", acRawFile);
      return -3;
   }

   // Open Output file
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsgD("***** Error creating output file %s in Edx_MergeDaily()", acOutFile);
      return -4;
   }

   // Get first RollRec
   do
   {
      iRet = fread(acRollRec, 1, iRollLen, fdRoll);
   } while (iRet == iRollLen && acRollRec[0] < '2');
   bEof = (iRet == iRollLen ? false:true);

   // Copy skip record
   memset(acBuf, ' ', iRecLen);
   while (iSkip-- > 0)
   {
      ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);
      // Ignore record start with 99999999
      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
   }

   iNoMatch=iBadCity=iBadSuffix=0;
   pRec = (EDX_ROLL *)&acRollRec[0];

   // Merge loop
   while (!bEof)
   {
      bRet = ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);

      // Check for EOF
      if (!bRet)
      {
         LogMsg("***** Error reading input file %s (%f)", acRawFile, GetLastError());
         break;
      }

      // EOF ?
      if (iRecLen != nBytesRead)
         break;

      NextRollRec:
      iTmp = memcmp(acBuf, pRec->Apn, iApnLen);
      if (!iTmp)
      {
         // Merge roll data
         iRet = Edx_MergeRoll(acBuf, acRollRec, acSaleRec, UPDATE_R01);
         if (iRet >= 0)
         {
            iRollUpd++;

            // Update cum Sales
            if (iRet == 1)
            {
               fputs(acSaleRec, fdSale);
               iNewSale++;
            }

            // Save last recording date
            lRet = atoin((char *)&acBuf[OFF_TRANSFER_DT], 8);
            if (lRet > lLastRecDate && lRet < lToday)
               lLastRecDate = lRet;
            lCnt++;
            bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
         }

         // Read next roll record
         iRet = fread(acRollRec, 1, iRollLen, fdRoll);
         bEof = (iRet == iRollLen ? false:true);
      } else if (iTmp > 0)       // Roll not match, new roll record?
      {
         // Create new R01 record
         iRet = Edx_MergeRoll(acRec, acRollRec, acSaleRec, CREATE_R01);
         if (!iRet)
         {
            LogMsg0("*** New roll record : %.*s (%d) ***", iApnLen, pRec->Apn, lCnt);
            iNewRec++;

            // Update cum Sales
            if (iRet == 1)
            {
               fputs(acSaleRec, fdSale);
               iNewSale++;
            }

            // Save last recording date
            lRet = atoin((char *)&acRec[OFF_TRANSFER_DT], 8);
            if (lRet > lLastRecDate && lRet < lToday)
               lLastRecDate = lRet;

            lCnt++;
            bRet = WriteFile(fhOut, acRec, iRecLen, &nBytesWritten, NULL);
         }

         // Get next roll record
         iRet = fread(acRollRec, 1, iRollLen, fdRoll);
         if (iRet != iRollLen)
            bEof = true;    // Signal to stop
         else
            goto NextRollRec;
      } else
      {
         lCnt++;
         bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
      }

      if (!(lCnt % 1000))
         printf("\r%u", lCnt);

      if (!bRet)
      {
         LogMsg("Error occurs: %d\n", GetLastError());
         lRet = WRITE_ERR;
         break;
      }
   }

   // Do the rest of the file
   while (!bEof)
   {
      // Create new R01 record
      iRet = Edx_MergeRoll(acRec, acRollRec, acSaleRec, CREATE_R01);
      if (iRet >= 0 && memcmp(pRec->Apn, "999999", 6))
      {
         // Update cum Sales
         if (iRet == 1)
         {
            fputs(acSaleRec, fdSale);
            iNewSale++;
         }

         if (bDebug)
            LogMsg0("*** New roll record : %.*s (%d) ***", iApnLen, pRec->Apn, lCnt);

         iNewRec++;

         // Save last recording date
         lRet = atoin((char *)&acRec[OFF_TRANSFER_DT], 8);
         if (lRet > lLastRecDate && lRet < lToday)
            lLastRecDate = lRet;

         lCnt++;
         bRet = WriteFile(fhOut, acRec, iRecLen, &nBytesWritten, NULL);
      }

      if (!(lCnt % 1000))
         printf("\r%u", lCnt);

      // Get next roll record
      iRet = fread(acRollRec, 1, iRollLen, fdRoll);
      bEof = (iRet == iRollLen ? false:true);
   }

   // Do the rest of the file
   while (nBytesRead>0 && ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL))
   {
      if (!nBytesRead)
         break;

      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
      lCnt++;
      if (!(lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdRoll)
      fclose(fdRoll);
   if (fdChar)
      fclose(fdChar);
   if (fdSale)
      fclose(fdSale);

   if (fhOut)
      CloseHandle(fhOut);
   if (fhIn)
      CloseHandle(fhIn);

   // If input is R01 file, delete it then rename TMP to R01
   if (acRawFile[strlen(acRawFile)-3] == 'R')
   {
      DeleteFile(acRawFile);
   } else
   {  // Raw file is S01, rename TMP to R01
      sprintf(acRawFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "R01");
   }

   // Rename TMP to R01 file
   rename(acOutFile, acRawFile);

   LogMsg("Total output records:       %u", lCnt);
   LogMsg("Total new records:          %u", iNewRec);
   LogMsg("Total bad-city records:     %u", iBadCity);
   LogMsg("Total bad-suffix records:   %u\n", iBadSuffix);
   LogMsg("Number of new Sales:        %u", iNewSale);

   printf("\nTotal output records: %u\n", lCnt);

   lRecCnt = lCnt;
   return 0;
}

/********************************* Edx_Load_LDR *****************************
 *
 * Load LDR file and update cum sale.
 *
 ****************************************************************************/

int Edx_Load_LDR(int iFirstRec /* 1=create header rec */)
{
   char     acBuf[MAX_RECSIZE], acRollRec[MAX_RECSIZE];
   char     acSaleRec[1024], acOutFile[_MAX_PATH];

   HANDLE   fhOut;

   int      iRet;
   DWORD    nBytesWritten;
   BOOL     bRet;
   long     lRet=0, lCnt=0;

   // Open roll file
   LogMsg("Open Roll file %s", acRollFile);
   fdRoll = fopen(acRollFile, "r");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", acRollFile);
      return 2;
   }

   // Open Char file
   if (!_access(acCharFile, 0))
   {
      LogMsg("Open Char file %s", acCharFile);
      fdChar = fopen(acCharFile, "r");
      if (fdChar == NULL)
      {
         LogMsg("***** Error opening Char file: %s\n", acCharFile);
         return 2;
      }
   }

   // Open Sales file
   if (!_access(acCSalFile, 0))
   {
      LogMsg("Open Sales file %s", acCSalFile);
      fdSale = fopen(acCSalFile, "a");
      if (fdSale == NULL)
      {
         LogMsg("***** Error opening Sales file: %s\n", acCSalFile);
         return 2;
      }
   }

   // Open Output file
   sprintf(acOutFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "R01");
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening output file: %s\n", acOutFile);
      return 4;
   }

   // Output first header record
   if (iFirstRec > 0)
   {
      memset(acBuf, '9', iRecLen);
      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
   }

   // Init variables
   iNewSale=iNoMatch=iBadCity=iBadSuffix=0;

   // Merge loop
   while (!feof(fdRoll))
   {
      // Get roll rec
      iRet = fread(acRollRec, 1, iRollLen, fdRoll);
      if (iRet != iRollLen)
         break;

      lLDRRecCount++;
      // Create new R01 record
      iRet = Edx_MergeRoll(acBuf, acRollRec, acSaleRec, CREATE_R01);
      if (iRet >= 0)
      {
         // Update cum Sales
         if (iRet == 1)
         {
            fputs(acSaleRec, fdSale);
            iNewSale++;
         }

         // Merge Char
         if (fdChar)
            iRet = Edx_MergeChar(acBuf);

         // Get last recording date
         iRet = atoin((char *)&acBuf[OFF_TRANSFER_DT], SIZ_TRANSFER_DT);
         if (iRet >= lToday)
            LogMsg0("*** WARNING: Bad last recording date %d at record# %d -->%.10s", iRet, lCnt, acBuf);
         else if (lLastRecDate < iRet)
            lLastRecDate = iRet;

         bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
         if (!(++lCnt % 1000))
            printf("\r%u", lCnt);

         if (!bRet)
         {
            LogMsg("Error writing to output file at record %d\n", lCnt);
            lRet = WRITE_ERR;
            break;
         }
      }
   }

   // Close files
   if (fdRoll)
      fclose(fdRoll);
   if (fdChar)
      fclose(fdChar);
   if (fdSale)
      fclose(fdSale);

   if (fhOut)
      CloseHandle(fhOut);

   LogMsg("Total input records:        %u", lLDRRecCount);
   LogMsg("Total output records:       %u", lCnt);
   LogMsg("Total bad-city records:     %u", iBadCity);
   LogMsg("Total bad-suffix records:   %u\n", iBadSuffix);
   LogMsg("Number of Char matched:     %u", lCharMatch);
   LogMsg("Number of Char skiped:      %u", lCharSkip);
   LogMsg("Number of new Sales:        %u", iNewSale);

   printf("\nTotal output records: %u\n", lCnt);

   lRecCnt = lCnt;
   return 0;
}

/******************************* Edx_FormatCSale *****************************
 *
 *****************************************************************************/

void Edx_FormatCSale(char *pFmtSale, char *pSaleRec)
{
/*
   EDX_SALE *pSale = (EDX_SALE *)pSaleRec;
   EDX_CSAL *pHSale= (EDX_CSAL *)pFmtSale;

   char     acTmp[32];

   // Inititalize
   memset(pFmtSale, ' ', sizeof(EDX_CSAL));

   // APN
   memcpy(pHSale->Apn, pSale->Apn, sizeof(pHSale->Apn));

   // ActualSalePrice
   memcpy(pHSale->ActualSalePrice, pSale->ActualSalePrice, sizeof(pHSale->ActualSalePrice));

   // RecDate
   if (pSale->RecDate[0] < '5')
      sprintf(acTmp, "20%.6s", pSale->RecDate);
   else
      sprintf(acTmp, "19%.6s", pSale->RecDate);
   memcpy(pHSale->RecDate, acTmp, sizeof(pHSale->RecDate));

   // RecPage
   memcpy(pHSale->RecPage, pSale->RecPage, sizeof(pHSale->RecPage));

   // ReliabilityCode
   memcpy(pHSale->ReliabilityCode, pSale->ReliabilityCode, sizeof(pHSale->ReliabilityCode));

   // SaleType
   memcpy(pHSale->SaleType, pSale->SaleType, sizeof(pHSale->SaleType));

   // AdjSalePrice
   memcpy(pHSale->AdjSalePrice, pSale->AdjSalePrice, sizeof(pHSale->AdjSalePrice));

   // NumParcels
   memcpy(pHSale->NumParcels, pSale->NumParcels, sizeof(pHSale->NumParcels));

   // DeedDate
   if (pSale->DeedDate[0] < '5')
      sprintf(acTmp, "20%.6s", pSale->DeedDate);
   else
      sprintf(acTmp, "19%.6s", pSale->DeedDate);
   memcpy(pHSale->DeedDate, acTmp, sizeof(pHSale->DeedDate));

   // PctTransfer
   memcpy(pHSale->PctTransfer, pSale->PctTransfer, sizeof(pHSale->PctTransfer));

   // DeedType
   memcpy(pHSale->DeedType, pSale->DeedType, sizeof(pHSale->DeedType));

   // StampAmt
   long lTmp = atoin(pSale->StampAmt, SSIZ_STAMP_AMOUNT);
   if (lTmp > 0)
   {
      double dTmp;
      dTmp = lTmp / 100;
      sprintf(acTmp, "%9.2f", dTmp);
      memcpy(pHSale->StampAmt, acTmp, 9);

      // SalePrice
      lTmp *= SALE_FACTOR_100;
      if (lTmp < 100)
         sprintf(acTmp, "%*u", SIZ_SALE1_AMT, lTmp);
      else
         sprintf(acTmp, "%*u00", SIZ_SALE1_AMT-2, lTmp/100);

      memcpy(pHSale->SalePrice, acTmp, SIZ_SALE1_AMT);
   }

   // StampCode
   pHSale->StampCode = pSale->StampCode;

   // Grantor
   memcpy(pHSale->Grantor, pSale->Grantor, sizeof(pHSale->Grantor));

   // Grantee
   memcpy(pHSale->Grantee, pSale->Grantee, sizeof(pHSale->Grantee));

   // S_Zip
   memcpy(pHSale->S_Zip, pSale->S_Zip, sizeof(pHSale->S_Zip));
*/
}

/****************************** Edx_UpdateSaleHist ***************************
 *
 * Update history sale.  This function adds new sale record to cumulative file.
 *
 * Return 0 if successful, >0 if error
 *
 *****************************************************************************/

int Edx_UpdateSaleHist(void)
{
   /*
   EDX_SALE *pCurSale;
   EDX_CSAL *pHistSale;
   char     acNewSale[512];
   char     *pTmp, acHSaleRec[512], acCSaleRec[1024], acSaleDate[32], acTmp[256];
   char     acOutFile[_MAX_PATH];

   HANDLE   fhIn, fhOut;
   FILE     *fdCSale;

   int      iRet, iTmp, iApnLen, iUpdateSale=0, iNewSale=0;
   DWORD    nBytesRead;
   DWORD    nBytesWritten;
   BOOL     bRet, bEof;
   long     lCnt=0;

   // Check current sale file
   if (_access(acSaleFile, 0))
   {
      LogMsg("***** Missing current sale file %s", acSaleFile);
      return -1;
   }

   // Check cumulative sale file
   if (_access(acCSalFile, 0))
   {
      LogMsg("*** Missing cumulative file %s.", acCSalFile);
      return -1;
   }

   // Create tmp output file
   strcpy(acOutFile, acCSalFile);
   if (pTmp = strrchr(acOutFile, '.'))
      strcpy(pTmp, ".tmp");
   else
      strcat(acOutFile, ".tmp");

   iApnLen = myCounty.iApnLen;

   // Open current sale
   LogMsg("Open current sale file %s", acSaleFile);
   fdCSale = fopen(acSaleFile, "r");
   if (fdCSale == NULL)
   {
      LogMsg("***** Error opening current sale file: %s\n", acSaleFile);
      return 2;
   }

   // Open Input file - cumulative sale file
   LogMsg("Open cumulative sale file %s", acCSalFile);
   fhIn = CreateFile(acCSalFile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhIn == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error open current sale file: %s", acCSalFile);
      return 3;
   }

   // Open Output file
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error open output file: %s", acOutFile);
      return 4;
   }

   // Read first history sale
   bRet = ReadFile(fhIn, acHSaleRec, iCSalLen, &nBytesRead, NULL);

   pCurSale  = (EDX_SALE *)&acCSaleRec[0];
   pHistSale = (EDX_CSAL *)&acHSaleRec[0];
   bEof = false;
   acSaleDate[0] = 0;

   // Merge loop
   while (!bEof)
   {
      // Get current sale
      pTmp = fgets(acCSaleRec, 1024, fdCSale);
      if (!pTmp)
         break;

      NextSaleRec:
      iTmp = memcmp(acCSaleRec, acHSaleRec, iApnLen);
      if (!iTmp)
      {
         // Check for duplicate sale
         if (!memcmp((char *)&acSaleDate[2], pCurSale->RecDate, 10))
         {
            LogMsg0("Dup sale: %.14s - %s", pCurSale->Apn, acSaleDate);
            continue;
         }

         // If current sale is newer, add to out file
         if (pCurSale->RecDate[0] < '5')
            sprintf(acSaleDate, "20%.10s", pCurSale->RecDate);
         else
            sprintf(acSaleDate, "19%.10s", pCurSale->RecDate);

         if (memcmp(acSaleDate, pHistSale->RecDate, SIZ_SALE1_DT) > 0)
         {
            // Format cumsale record
            Edx_FormatCSale(acNewSale, acCSaleRec);

            // Output current sale record
            bRet = WriteFile(fhOut, acNewSale, iCSalLen, &nBytesWritten, NULL);
            iUpdateSale++;
         }
      } else if (iTmp > 0)
      {
         acSaleDate[2] = 0;

         // Write out all history sale record for current APN
         while (iTmp > 0)
         {
            bRet = WriteFile(fhOut, acHSaleRec, iCSalLen, &nBytesWritten, NULL);
            bRet = ReadFile(fhIn, acHSaleRec, iCSalLen, &nBytesRead, NULL);
            if (!bRet || !nBytesRead)
            {
               bEof = true;
               break;
            }
            iTmp = memcmp(acCSaleRec, acHSaleRec, iApnLen);
         }

         if (!bEof)
            goto NextSaleRec;
      } else // CurApn < HistApn
      {
         // Format cumsale record
         Edx_FormatCSale(acNewSale, acCSaleRec);

         // New sale record for this APN
         bRet = WriteFile(fhOut, acNewSale, iCSalLen, &nBytesWritten, NULL);
         iNewSale++;
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);

      if (!bRet)
      {
         LogMsg("***** Error occurs: %d\n", GetLastError());
         break;
      }
   }

   // Do the rest of the file
   while (nBytesRead > 0)
   {
      bRet = WriteFile(fhOut, acHSaleRec, iCSalLen, &nBytesWritten, NULL);
      bRet = ReadFile(fhIn, acHSaleRec, iCSalLen, &nBytesRead, NULL);

      if (!bRet)
         break;
   }

   // Close files
   if (fdCSale)
      fclose(fdCSale);
   if (fhOut)
      CloseHandle(fhOut);
   if (fhIn)
      CloseHandle(fhIn);

   // Rename output file
   strcpy(acTmp, acCSalFile);
   if (pTmp = strrchr(acTmp, '.'))
   {
      strcpy(pTmp, ".sav");
      if (!_access(acTmp, 0))
         remove(acTmp);
   }

   iRet = rename(acCSalFile, acTmp);
   if (iRet)
   {
      LogMsg("***** Error renaming %s to %s: %d", acCSalFile, acTmp, errno);
   } else
   {
      // Sort output file
      sprintf(acTmp, "S(1,14,C,A,25,12,C,D) F(FIX,%d)", iCSalLen);
      iTmp = sortFile(acOutFile, acCSalFile, acTmp);
   }

   LogMsg("Total sale records processed:   %u", lCnt);
   LogMsg("Total cumulative sale records:  %u", iTmp);
   LogMsg("Total new sale records:         %u", iNewSale);
   LogMsg("Total sale records updated:     %u", iUpdateSale);

   LogMsg("Update Sale History completed.");
   return lCnt;
   */

return 0;
}

/******************************** MergeXferRec *******************************
 *
 * This function is designed to run 2006 LDR.  After this event, it may need
 * to be modified for later when DocNum has already been standardized. 2006/08/18 - spn
 * Remove Edx_FormatDoc() for 2008 LDR - spn
 *
 *****************************************************************************/

int Edx_MergeXferRec(char *pOutbuf)
{
   static   char  acRec[1024], *pRec=NULL;
   int      iLoop;
   XFER_REC *pSale;

   // Get first Char rec for first call
   if (!pRec)
      pRec = fgets(acRec, 1024, fdSale);

   pSale = (XFER_REC *)&acRec[0];
   do
   {
      if (!pRec)
      {
         fclose(fdSale);
         fdSale = NULL;
         return 1;      // EOF
      }

      // Compare Apn
      iLoop = memcmp(pOutbuf, pSale->acApn, iApnLen);
      if (iLoop > 0)
      {
         if (bDebug)
            LogMsg0("Skip Sale rec  %.*s", iApnLen, pSale->acApn);
         pRec = fgets(acRec, 1024, fdSale);
         lSaleSkip++;
      }
   } while (iLoop > 0);

   // If not match, return
   if (iLoop)
      return 1;

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "001012271", 9))
   //   iLoop = 0;
#endif


   // Merge sale rec
   if (pSale->acDoc3Num[0] > ' ')
   {
      //if (pSale->acDoc3Num[4] == ' ')
      //{
      //   sprintf(acTmp, "%.4s%.3s", pSale->acDoc3Num, &pSale->acDoc3Num[5]);
      //   iRet = Edx_FormatDoc(acDocNum, acTmp, pSale->acDoc3Date);
      //   memcpy(pOutbuf+OFF_SALE3_DOC, acDocNum, iRet);
      //} else
         memcpy(pOutbuf+OFF_SALE3_DOC,  pSale->acDoc3Num,    SIZ_SALE3_DOC);
      memcpy(pOutbuf+OFF_SALE3_DT,      pSale->acDoc3Date,   SIZ_SALE3_DT);
      memcpy(pOutbuf+OFF_SALE3_DOCTYPE, pSale->acDoc3Type,   SIZ_SALE3_DOCTYPE);
      memcpy(pOutbuf+OFF_SALE3_AMT,     pSale->acSale3Price, SIZ_SALE3_AMT);
   }

   if (pSale->acDoc2Num[0] > ' ')
   {
      //if (pSale->acDoc2Num[4] == ' ')
      //{
      //   sprintf(acTmp, "%.4s%.3s", pSale->acDoc2Num, &pSale->acDoc2Num[5]);
      //   iRet = Edx_FormatDoc(acDocNum, acTmp, pSale->acDoc2Date);
      //   memcpy(pOutbuf+OFF_SALE2_DOC, acDocNum, iRet);
      //} else
         memcpy(pOutbuf+OFF_SALE2_DOC,  pSale->acDoc2Num,    SIZ_SALE3_DOC);
      memcpy(pOutbuf+OFF_SALE2_DT,      pSale->acDoc2Date,   SIZ_SALE3_DT);
      memcpy(pOutbuf+OFF_SALE2_DOCTYPE, pSale->acDoc2Type,   SIZ_SALE3_DOCTYPE);
      memcpy(pOutbuf+OFF_SALE2_AMT,     pSale->acSale2Price, SIZ_SALE3_AMT);
   }

   if (pSale->acDoc1Num[0] > ' ')
   {
      //if (pSale->acDoc1Num[4] == ' ')
      //{
      //   sprintf(acTmp, "%.4s%.3s", pSale->acDoc1Num, &pSale->acDoc1Num[5]);
      //   iRet = Edx_FormatDoc(acDocNum, acTmp, pSale->acDoc1Date);
      //   memcpy(pOutbuf+OFF_SALE1_DOC, acDocNum,            iRet);
      //} else
         memcpy(pOutbuf+OFF_SALE1_DOC,  pSale->acDoc1Num,    SIZ_SALE3_DOC);
      memcpy(pOutbuf+OFF_SALE1_DT,      pSale->acDoc1Date,   SIZ_SALE3_DT);
      memcpy(pOutbuf+OFF_SALE1_DOCTYPE, pSale->acDoc1Type,   SIZ_SALE3_DOCTYPE);
      memcpy(pOutbuf+OFF_SALE1_AMT,     pSale->acSale1Price, SIZ_SALE3_AMT);

      // Seller
      //if (pSale->acSeller[0] > ' ')
      //   memcpy(pOutbuf+OFF_SELLER, pSale->acSeller, SIZ_SELLER);

   }

   // Get next sale record
   pRec = fgets(acRec, 1024, fdSale);
   if (!pRec)
   {
      fclose(fdSale);
      fdSale = NULL;
   }

   lSaleMatch++;

   return 0;
}

/******************************* MergeXferFile *******************************
 *
 * Merge extracted sale data into current sale.
 * Return < 0 if error.
 *        = 0 if sale file missing
 *        > 0 if success (number of updated records
 *
 *****************************************************************************/

int Edx_MergeXferFile(char *pInfile, char *pOutfile, char *pXferfile)
{
   int      iRet=0, iUpdCnt=0;
   long     lCnt=0;
   unsigned long nBytesRead, nBytesWritten;
   char     acBuf[2048], *pInbuf;

   HANDLE   fhIn, fhOut;

   // Check file is exist
   if (_access(pInfile, 0))
      return 0;

   LogMsg("Merge sale info from %s to %s", pXferfile, pInfile);

   // Open input file
   LogMsg("Open input file %s", pInfile);
   if (INVALID_HANDLE_VALUE == (fhIn = CreateFile(pInfile, GENERIC_READ, FILE_SHARE_READ,
       NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL)) )
   {
      LogMsg("***** Error opening input file: %s", pInfile);
      return -3;
   }

   // Open output file
   LogMsg("Open output file %s", pOutfile);
   if (INVALID_HANDLE_VALUE == (fhOut = CreateFile(pOutfile, GENERIC_WRITE, 0,
       NULL, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL)) )
   {
      LogMsg("***** Error creating output file: %s", pOutfile);
      return -4;
   }

   // Open sale file
   if (!(fdSale = fopen(pXferfile, "r")))
   {
      LogMsg("***** Error opening transfer file: %s", pXferfile);
      return -5;
   }

   // Read record, no sale update for this one
   ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);
   WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);

   pInbuf = acBuf;
   lSaleSkip=lSaleMatch=0;
   // Old APN length is only 9 bytes.  Next LDR 2007, remove following line
   iApnLen = 9;
   while (ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL))
   {
      if (!nBytesRead)
         break;

      // Update sale
      if (fdSale)
         if (!Edx_MergeXferRec(acBuf))
            iUpdCnt++;

      // Write output record
      WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   if (fhIn)
      CloseHandle(fhIn);
   if (fhOut)
      CloseHandle(fhOut);
   if (fdSale)
      fclose(fdSale);

   if (!iRet)
      iRet = iUpdCnt;

   LogMsg("Total records processed: %u", lCnt);
   LogMsg("      records updated:   %u", iRet);

   return iRet;
}

/******************************************************************************
 *
 *
 ******************************************************************************/

int Edx_CreateLienRec(char *pOutbuf, char *pRollRec)
{
   char     acTmp[256];
   long     lTmp, iTmp;

   LIENEXTR *pLienRec = (LIENEXTR *)pOutbuf;
   EDX_ROLL *pRec     = (EDX_ROLL *)pRollRec;

   // 00    = Active
   // 01-89 = Active nonbillable
   // 90-XX = deleted
   iTmp = atoin(pRec->Status, RSIZ_STATUS);
   if (pRec->RecordCode != '2' || iTmp > 89)
      return 1;

   // Clear output buffer
   memset(pOutbuf, ' ', sizeof(LIENEXTR));

   // Start copying data
   memcpy(pLienRec->acApn, pRec->Apn, RSIZ_APN);

   // TRA
   lTmp = atoin(pRec->TRA, RSIZ_TRA);
   if (lTmp > 0)
   {
      iTmp = sprintf(acTmp, "%.6d", lTmp);
      memcpy(pLienRec->acTRA, acTmp, iTmp);
   }

   // Year assessed
   memcpy(pLienRec->acYear, myCounty.acYearAssd, 4);

   // Land
   long lLand = atoin(pRec->Land, RSIZ_LAND);
   if (lLand > 0)
   {
      sprintf(acTmp, "%*u", SIZ_LIEN_LAND, lLand);
      memcpy(pLienRec->acLand, acTmp, SIZ_LIEN_LAND);
   }

   // Improve
   long lImpr = atoin(pRec->Structure, RSIZ_LAND);
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*u", SIZ_LIEN_IMPR, lImpr);
      memcpy(pLienRec->acImpr, acTmp, SIZ_LIEN_IMPR);
   }

   // Other value
   long lFixture = atoin(pRec->Fixt_Val, RSIZ_FIXED_EQUIP);
   long lPers    = atoin(pRec->Other_PP_Val, RSIZ_OTH_PERS_PROP);
   long lTimber  = atoin(pRec->Timber, RSIZ_OTH_PERS_PROP);
   long lMineral = atoin(pRec->Mineral, RSIZ_OTH_PERS_PROP);
   long lPlant   = atoin(pRec->Plants, RSIZ_OTH_PERS_PROP);
   long lBusInv  = atoin(pRec->Bus_Inv, RSIZ_OTH_PERS_PROP);
   long lOther   = atoin(pRec->OtherImpr, RSIZ_OTH_PERS_PROP);

   LONGLONG lGross = lPers + lFixture + lTimber + lMineral + lPlant + lBusInv + lOther;
   if (lGross > 0)
   {
      iTmp = sprintf(acTmp, "%*u", sizeof(pLienRec->acOther), lGross);
      memcpy(pLienRec->acOther, acTmp, iTmp);

      if (lPers > 0)
      {
         iTmp = sprintf(acTmp, "%*u", sizeof(pLienRec->acPP_Val), lPers);
         memcpy(pLienRec->acPP_Val, acTmp, iTmp);
      }
      if (lFixture > 0)
      {
         iTmp = sprintf(acTmp, "%*u", sizeof(pLienRec->acME_Val), lFixture);
         memcpy(pLienRec->acME_Val, acTmp, iTmp);
      }
      if (lTimber > 0)
      {
         iTmp = sprintf(acTmp, "%*u", sizeof(pLienRec->extra.Edx.Timber), lTimber);
         memcpy(pLienRec->extra.Edx.Timber, acTmp, iTmp);
      }
      if (lMineral > 0)
      {
         iTmp = sprintf(acTmp, "%*u", sizeof(pLienRec->extra.Edx.Mineral), lMineral);
         memcpy(pLienRec->extra.Edx.Mineral, acTmp, iTmp);
      }
      if (lPlant > 0)
      {
         iTmp = sprintf(acTmp, "%*u", sizeof(pLienRec->extra.Edx.Plants), lPlant);
         memcpy(pLienRec->extra.Edx.Plants, acTmp, iTmp);
      }
      if (lBusInv > 0)
      {
         iTmp = sprintf(acTmp, "%*u", sizeof(pLienRec->extra.Edx.BusInv), lBusInv);
         memcpy(pLienRec->extra.Edx.BusInv, acTmp, iTmp);
      }
      if (lOther > 0)
      {
         iTmp = sprintf(acTmp, "%*u", sizeof(pLienRec->extra.Edx.OthImpr), lOther);
         memcpy(pLienRec->extra.Edx.OthImpr, acTmp, iTmp);
      }
   }

   // Gross total
   lGross += lLand+lImpr;
   if (lGross > 0)
   {
      iTmp = sprintf(acTmp, "%*u", sizeof(pLienRec->acGross), lGross);
      memcpy(pLienRec->acGross, acTmp, iTmp);

      // Ratio
      if (lImpr > 0)
      {
         iTmp = sprintf(acTmp, "%*u", sizeof(pLienRec->acRatio), (LONGLONG)lImpr*100/(lLand+lImpr));
         memcpy(pLienRec->acRatio, acTmp, iTmp);
      }
   }


   // Exemp total
   pLienRec->acHO[0] = '2';      // 'N'
   long lExe1 = atoin(pRec->Exe1_Amt, RSIZ_EXEMP_AMT);
   long lExe2 = atoin(pRec->Exe2_Amt, RSIZ_EXEMP_AMT);
   long lExe3 = atoin(pRec->Exe3_Amt, RSIZ_EXEMP_AMT);
   lTmp = lExe1+lExe2+lExe3;
   if (lTmp > 0)
   {
      // HO Exempt
      if (!memcmp(pRec->Exe1_Code, "01", 2) ||
          !memcmp(pRec->Exe2_Code, "01", 2) ||
          !memcmp(pRec->Exe3_Code, "01", 2) )
         pLienRec->acHO[0] = '1';      // 'Y'

      // Exempt cannot be greater than total value
      if (lTmp > lGross)
         lTmp = (unsigned long)lGross;
      iTmp = sprintf(acTmp, "%*u", sizeof(pLienRec->acExAmt), lTmp);
      memcpy(pLienRec->acExAmt, acTmp, iTmp);

      if (lExe1 > 0)
      {
         memcpy(pLienRec->extra.Edx.Exe_Code1, pRec->Exe1_Code, RSIZ_EXEMP_CD);
         iTmp = sprintf(acTmp, "%*u", sizeof(pLienRec->extra.Edx.Exe_Amt1), lExe1);
         memcpy(pLienRec->extra.Edx.Exe_Amt1, acTmp, iTmp);
      }
      if (lExe2 > 0)
      {
         memcpy(pLienRec->extra.Edx.Exe_Code2, pRec->Exe2_Code, RSIZ_EXEMP_CD);
         iTmp = sprintf(acTmp, "%*u", sizeof(pLienRec->extra.Edx.Exe_Amt1), lExe2);
         memcpy(pLienRec->extra.Edx.Exe_Amt2, acTmp, iTmp);
      }
      if (lExe1 > 0)
      {
         memcpy(pLienRec->extra.Edx.Exe_Code3, pRec->Exe3_Code, RSIZ_EXEMP_CD);
         iTmp = sprintf(acTmp, "%*u", sizeof(pLienRec->extra.Edx.Exe_Amt1), lExe3);
         memcpy(pLienRec->extra.Edx.Exe_Amt3, acTmp, iTmp);
      }
   }

   // Prop 8
   if (pRec->Reappr_Code[0] == 'T')
      pLienRec->SpclFlag = LX_PROP8_FLG;

   pLienRec->LF[0] = 10;
   pLienRec->LF[1] = 0;
   return 0;
}

/********************************* Edx_ExtrLien *****************************
 *
 *
 ****************************************************************************/

int Edx_ExtrLien()
{
   char     acBuf[MAX_RECSIZE], acRollRec[MAX_RECSIZE];
   char     acOutFile[_MAX_PATH];
   FILE     *fdLien;
   int      iRet;
   long     lCnt=0, lDrop=0;

   // Open roll file
   LogMsg("Open Roll file %s", acRollFile);
   fdRoll = fopen(acRollFile, "rb");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", acRollFile);
      return -1;
   }

   // Open Output file
   sprintf(acOutFile, acLienTmpl, myCounty.acCntyCode, myCounty.acCntyCode);
   LogMsg("Open lien extract file %s", acOutFile);
   fdLien = fopen(acOutFile, "w");
   if (fdLien == NULL)
   {
      LogMsg("***** Error creating lien extract file: %s\n", acOutFile);
      return -2;
   }

   // Merge loop
   while (!feof(fdRoll))
   {
      // Get roll rec
      //pTmp = fgets(acRollRec, iRollLen, fdRoll);
      iRet = fread(acRollRec, 1, iRollLen, fdRoll);

      //if (!pTmp)
      if (iRet < iRollLen)
         break;

      // Create new R01 record
      iRet = Edx_CreateLienRec(acBuf, acRollRec);
      if (!iRet)
      {
         // Write to output
         fputs(acBuf, fdLien);
         if (!(++lCnt % 1000))
            printf("\r%u", lCnt);
      } else
         lDrop++;
   }

   // Close files
   if (fdRoll)
      fclose(fdRoll);
   if (fdLien)
      fclose(fdLien);

   LogMsgD("\nTotal output records:       %u", lCnt);
   LogMsgD(  "Number of records dropped:  %u", lDrop);

   lRecCnt = lCnt;
   return 0;
}

/***************************** Edx_ParseTaxDetail ****************************
 *
 * Create Detail and Agency records from Secured Tax Roll
 * Return 0 if success
 *
 *****************************************************************************/

int Edx_ParseTaxDetail(FILE *fdDetail, FILE *fdAgency, char *pInbuf)
{
   char     acTmp[2048], acDetail[512], acAgency[512], *pTmp;
   int      iIdx, iTmp, iTotalTax, iTaxAmt;
   double   dTmp;

   EDX_SECROLL *pInRec  = (EDX_SECROLL *)pInbuf;
   TAXDETAIL   *pDetail = (TAXDETAIL *)acDetail;
   TAXAGENCY   *pAgency = (TAXAGENCY *)acAgency, *pResult;

   // Clear output buffer
   memset(acDetail, 0, sizeof(TAXDETAIL));
   memset(acAgency, 0, sizeof(TAXAGENCY));

   // APN
   memcpy(acTmp, pInRec->APN_D, TSIZ_APN_D);
   acTmp[TSIZ_APN_D] = 0;
   remChar(acTmp, '-');
   strcpy(pDetail->Apn, acTmp);

   // BillNum
   memcpy(pDetail->BillNum, pInRec->BillNum, TSIZ_BILLNUM);

   // Tax Year
   iTmp = sprintf(pDetail->TaxYear, "%d", lTaxYear);
#ifdef _DEBUG
   //if (!memcmp(pDetail->Apn, "00081510010", 9) )
   //   iTmp = 0;
#endif

   // Handle county agency
   iTotalTax = 0;
   for (iIdx = 0; iIdx < EDX_MAX_AGENCY; iIdx++)
   {
      iTaxAmt = atoin(pInRec->Agency[iIdx].Amount, TSIZ_AMOUNT);
      if (iTaxAmt > 0)
      {
         // Ignore entries start with "TOTAL"
         if (memcmp(pInRec->Agency[iIdx].Agency, "TOTAL", 5))
         {
            // Tax amt
            sprintf(pDetail->TaxAmt, "%.2f", iTaxAmt/100.0);

            // Tax Rate
            memcpy(acTmp, pInRec->Agency[iIdx].Agency, TSIZ_AGENCY);
            acTmp[TSIZ_AGENCY] = 0;
            if (pTmp = strstr(acTmp, "  "))
            {
               *pTmp = 0;
               dTmp = atof(pTmp+2);
               sprintf(pDetail->TaxRate, "%.4f", dTmp);
            }

            // Tax desc
            strcpy(pDetail->TaxDesc, acTmp);

            // Create Agency record            
            pResult = findTaxDist(acTmp);
            if (pResult)
            {
               strcpy(pDetail->TaxCode, pResult->Code);
               strcpy(pAgency->Code, pResult->Code);
               strcpy(pAgency->Agency, pResult->Agency);
               pAgency->TC_Flag[0] = pResult->TC_Flag[0];
               pDetail->TC_Flag[0] = pResult->TC_Flag[0];
            } else
            {
               strcpy(pAgency->Code, "N/A");
               strcpy(pAgency->Agency, pInRec->Agency[iIdx].Agency);
               LogMsg0("+++ Unknown Tax Agency: %.80s", pInRec->Agency[iIdx].Agency);
            }

            // Phone
            strcpy(pDetail->Phone, myTrim(pInRec->Agency[iIdx].Phone, TSIZ_PHONE));

            // Generate csv line and write to file
            Tax_CreateDetailCsv(acTmp, pDetail);
            fputs(acTmp, fdDetail);

            strcpy(pAgency->TaxRate, pDetail->TaxRate);
            strcpy(pAgency->Phone, pDetail->Phone);
            Tax_CreateAgencyCsv(acTmp, pAgency);
            fputs(acTmp, fdAgency);
         }
      } else
         break;
   }

   return 0;
}

/***************************** Edx_ParseTaxBase ******************************
 *
 * Crete Base, Delq & Owner record
 *
 *****************************************************************************/

int Edx_ParseTaxBase(char *pBase, char *pDelq, char *pInbuf)
{
   TAXBASE     *pBaseRec = (TAXBASE *)pBase;
   TAXDELQ     *pDelqRec = (TAXDELQ *)pDelq;
   EDX_SECROLL *pInRec   = (EDX_SECROLL *)pInbuf;

   char acTmp[256];
   int  iTmp, iTax1, iTax2, iPen1, iPen2, iCost, iTotalTax, iTotalDue;

   // Clear output buffer
   memset(pBaseRec, 0, sizeof(TAXBASE));
   memset(pDelqRec, 0, sizeof(TAXDELQ));

   // APN
   memcpy(acTmp, pInRec->APN_D, TSIZ_APN_D);
   acTmp[TSIZ_APN_D] = 0;
   remChar(acTmp, '-');
   strcpy(pBaseRec->Apn, acTmp);
   strcpy(pBaseRec->OwnerInfo.Apn, acTmp);

   memcpy(pBaseRec->BillNum, pInRec->BillNum, TSIZ_BILLNUM);
   memcpy(pBaseRec->OwnerInfo.BillNum, pInRec->BillNum, TSIZ_BILLNUM);

   // TRA
   sprintf(pBaseRec->TRA, "%.3s%.3s", pInRec->TRA, &pInRec->TRA[4]);

   // Tax Year
   sprintf(pBaseRec->TaxYear, "%d", lTaxYear);
   strcpy(pBaseRec->OwnerInfo.TaxYear, pBaseRec->TaxYear);

   // Updated date
   sprintf(pBaseRec->Upd_Date, "%d", lTaxRollInfoDate);

   // Owner info
   memcpy(pBaseRec->OwnerInfo.MailAdr[0], pInRec->Street, TSIZ_STREET);
   myTrim(pBaseRec->OwnerInfo.MailAdr[0]);
   sprintf(pBaseRec->OwnerInfo.MailAdr[1], "%s, %.2s %.5s", myTrim(pInRec->City, TSIZ_CITY-1), pInRec->State, pInRec->Zip_Code);
   iTmp = blankRem(pInRec->CurName1, TSIZ_OWNERNAME);
   memcpy(pBaseRec->OwnerInfo.Name1, pInRec->CurName1, iTmp);
   if (!memcmp(pInRec->CurName2, "C/O", 3))
      strcpy(pBaseRec->OwnerInfo.CareOf, myTrim(pInRec->CurName2, TSIZ_OWNERNAME));
   else
      strcpy(pBaseRec->OwnerInfo.Name2,  myTrim(pInRec->CurName2, TSIZ_OWNERNAME));

   // Check for Tax amount
   iTax1 = atoin(pInRec->InstAmt1, TSIZ_AMOUNT);
   iTax2 = atoin(pInRec->InstAmt2, TSIZ_AMOUNT);
   iPen1 = atoin(pInRec->PenAmt1, TSIZ_AMOUNT);
   iPen2 = atoin(pInRec->PenAmt2, TSIZ_AMOUNT);
   iCost = atoin(pInRec->Cost, TSIZ_AMOUNT);

   //iTotalTax = atoin(pInRec->Net_Tax, TSIZ_AMOUNT);
   iTotalTax = iTax1+iTax2;
   iTotalDue = atoin(pInRec->Total_TaxDue, TSIZ_AMOUNT);

   if (pInRec->DelqDate1[0] >= '0' && dateConversion(pInRec->DelqDate1, acTmp, MM_DD_YYYY_1))
      strcpy(pBaseRec->DueDate1, acTmp);
   if (pInRec->DelqDate2[0] >= '0' && dateConversion(pInRec->DelqDate2, acTmp, MM_DD_YYYY_1))
      strcpy(pBaseRec->DueDate2, acTmp);

   if (iTotalTax > 0)
      sprintf(pBaseRec->TotalTaxAmt, "%.2f", iTotalTax/100.0);

   if (iCost > 0)
      sprintf(pBaseRec->TotalFees, "%.2f", iCost/100.0);

   // Tax Installment
   if (iTax1 > 0)
      sprintf(pBaseRec->TaxAmt1, "%.2f", iTax1/100.0);
   if (iTax2 > 0)
      sprintf(pBaseRec->TaxAmt2, "%.2f", iTax2/100.0);

#ifdef _DEBUG
   //if (!memcmp(pBaseRec->Apn, "00116130100", iApnLen))
   //   iTmp = 0;
#endif

   // Tax Penalty
   if (iPen1 > 0 && ChkDueDate(1, pBaseRec->DueDate1))
      sprintf(pBaseRec->PenAmt1, "%.2f", iPen1/100.0);
   if (iPen2 > 0 && ChkDueDate(2, pBaseRec->DueDate2))
      sprintf(pBaseRec->PenAmt2, "%.2f", iPen2/100.0);

   // Tax rate
   iTmp = atoin(pInRec->Tax_Rate, TSIZ_TAX_RATE);
   if (iTmp > 0)
      sprintf(pBaseRec->TotalRate, "%.5f", iTmp/10000.0);

   // Default
   if (pInRec->Sec_Def_Tax_Year[0] > '0')
   {
      strcpy(pDelqRec->Apn, pBaseRec->Apn);
      strcpy(pDelqRec->Upd_Date, pBaseRec->Upd_Date);
      // Delq year
      memcpy(pBaseRec->DelqYear, pInRec->Sec_Def_Tax_Year, 4);
      iTmp = atoin(pInRec->Sec_Def_Tax_Year, 4);
      sprintf(pDelqRec->TaxYear, "%d", iTmp-1);
      memcpy(pDelqRec->Default_No, pInRec->Sec_Def_Bill_Number, TSIZ_BILLNUM);
      memcpy(pDelqRec->BillNum, pInRec->BillNum, TSIZ_BILLNUM);

      pDelqRec->isDelq[0] = '1';
      pBaseRec->isDelq[0] = '1';
   } 
   pBaseRec->isSecd[0] = '1';
   return 0;
}

/****************************** Edx_UpdateTaxDelq *****************************
 *
 * Update Tax Delq using Sold4Tax.txt.  Update secured records only.  Will handle other
 * record type later.  If DefaultNo is different from Base, update Base record too.
 *
 * Update fields: DefaultDate, DefaultAmt, Redeem flag, DefaultNo
 *
 ******************************************************************************/

int Edx_UpdateTaxDelq1(char *pBaseBuf, char *pDelqBuf, FILE *fd)
{
   static   char  acRec[MAX_RECSIZE], *pRec=NULL;
   int      iLoop, iTmp;

   TAXBASE      *pBaseRec = (TAXBASE *)pBaseBuf;
   TAXDELQ      *pDelqRec = (TAXDELQ *)pDelqBuf;
   EDX_SOLD4TAX *pInRec = (EDX_SOLD4TAX *)acRec;

   if (!pRec)
      pRec = fgets(acRec, MAX_RECSIZE, fd);        // Get first rec
#ifdef _DEBUG
   //if (!memcmp(pDelqRec->Apn, "00101219100", iApnLen))
   //   iTmp = 0;
#endif

   do
   {
      // Compare Apn
      iLoop = memcmp(pDelqRec->Apn, pInRec->Apn, iApnLen);
      if (iLoop > 0 || pInRec->Rec_Code != '2')
      {
         pRec = fgets(acRec, MAX_RECSIZE, fd);
         if (!pRec)
         {
            fclose(fd);
            fd = NULL;
            return 1;      // EOF
         }
      }
   } while (iLoop > 0 || pInRec->Rec_Code != '2');

   if (iLoop)
      return 1;

   // Check DefaultNo
   if (memcmp(pDelqRec->Default_No, pInRec->DefaultNo, TSIZ_DEFAULTNO-1))
   {
      LogMsg0("*** Sold4Tax: DefaultNo not matched: %s vs %.7s [%s]", pDelqRec->Default_No, pInRec->DefaultNo, pDelqRec->Apn);
      memcpy(pDelqRec->Default_No, pInRec->DefaultNo, TSIZ_DEFAULTNO);
   }

   // Updated date
   sprintf(pDelqRec->Upd_Date, "%d", lSold4TaxDate);

   // Update DefAmt
   iTmp = atoin(pInRec->Def_Amt, TSIZ_DFLT_TAXAMT);
   if (iTmp > 0)
      sprintf(pDelqRec->Def_Amt, "%.2f", iTmp/100.0);

   // Update DefDate
   if (pInRec->Def_Date[0] > '0')
   {
      memcpy(pDelqRec->Def_Date, pInRec->Def_Date, TSIZ_DATE);
      if (pInRec->RedeemFlg != 'R')
      {
         memcpy(pBaseRec->Def_Date, pInRec->Def_Date, TSIZ_DATE);
         memcpy(pBaseRec->DelqYear, pInRec->Def_Date, 4);
         pBaseRec->isDelq[0] = '1';
         strcpy(pBaseRec->Upd_Date, pDelqRec->Upd_Date);
         lLastTaxFileDate = lSold4TaxDate;
      }
   }

   // Update status
   if (pInRec->RedeemFlg == 'R')
   {
      pDelqRec->DelqStatus[0] = '1';
      pBaseRec->DelqStatus[0] = '1';

      pDelqRec->isDelq[0] = 0;
      pBaseRec->isDelq[0] = 0;
   }

   // Get next record
   pRec = fgets(acRec, MAX_RECSIZE, fd);
   if (!pRec)
   {
      fclose(fd);
      fd = NULL;
   }

   return 0;
}

/****************************** Edx_UpdateTaxDelq *****************************
 *
 * Update Tax Delq using InstalMT.txt
 * Update fields: Redeem Date, Redeem Amt, Redeem flag
 *
 ******************************************************************************/

int Edx_UpdateTaxDelq2(char *pDelqBuf, FILE *fd)
{
   static   char  acRec[MAX_RECSIZE], *pRec=NULL;
   int      iLoop, iTmp;

   TAXDELQ  *pDelqRec = (TAXDELQ *)pDelqBuf;
   EDX_PAID *pInRec   = (EDX_PAID *)acRec;

   if (!pRec)
      pRec = fgets(acRec, MAX_RECSIZE, fd);        // Get first rec
#ifdef _DEBUG
   //if (!memcmp(pDelqRec->Apn, "00101219100", iApnLen))
   //   iTmp = 0;
#endif

   do
   {
      // Compare Apn
      iLoop = memcmp(pDelqRec->Apn, pInRec->Apn, iApnLen);
      if (iLoop > 0 || pInRec->Rec_Code != '2')
      {
         pRec = fgets(acRec, MAX_RECSIZE, fd);
         if (!pRec)
         {
            fclose(fd);
            fd = NULL;
            return 1;      // EOF
         }
      }
   } while (iLoop > 0 || pInRec->Rec_Code != '2');

   if (iLoop)
      return 1;

   // Check DefaultNo
   if (memcmp(pDelqRec->Default_No, pInRec->DefaultNo, TSIZ_DEFAULTNO-1))
   {
      LogMsg0("*** InstallMT: DefaultNo not matched: %s vs %.7s [%s]", pDelqRec->Default_No, pInRec->DefaultNo, pDelqRec->Apn);
      return 1;
   }

   // Update status
   if (pInRec->RedeemFlg == 'Q' || pInRec->RedeemFlg == 'T')
   {
      pDelqRec->isDelq[0] = 0;
      pDelqRec->DelqStatus[0] = '1';

      // Update Redeem Amt
      iTmp = atoin(pInRec->RedAmt, TSIZ_DFLT_TAXAMT);
      if (iTmp > 0)
         sprintf(pDelqRec->Red_Amt, "%.2f", iTmp/100.0);

      // Update Redeem Date
      if (pInRec->PaidDate[0] > '0')
         memcpy(pDelqRec->Red_Date, pInRec->PaidDate, TSIZ_DATE);

#ifdef _DEBUG
      //if (lInstallMTDate < lSold4TaxDate)
      //   iTmp = 0;
#endif
      sprintf(pDelqRec->Upd_Date, "%d", lInstallMTDate);
   }

   // Get next record
   pRec = fgets(acRec, MAX_RECSIZE, fd);
   if (!pRec)
   {
      fclose(fd);
      fd = NULL;
   }

   return 0;
}

/****************************** Edx_UpdateTaxBase *****************************
 *
 * Update Tax Base using TaxBill.txt
 * Find matching APN, if same tax year, update tax info. If newer tax year,
 * clear record and create new tax base.
 *
 ******************************************************************************/

int Edx_UpdateTaxBase(char *pBaseBuf, FILE *fdTaxBill)
{
   static   char  acRec[MAX_RECSIZE], *pRec=NULL;
   int      iLoop, iTmp;
   double   dDueAmt1, dDueAmt2;
   char     acTRA[8];

   TAXBASE     *pBaseRec = (TAXBASE *)pBaseBuf;
   EDX_TAXBILL *pTaxBill = (EDX_TAXBILL *)acRec;

   if (!pRec)
      pRec = fgets(acRec, MAX_RECSIZE, fdTaxBill);        // Get first rec
#ifdef _DEBUG
   //if (!memcmp(pBaseRec->Apn, "00116130100", 9))
   //   iTmp = 0;
#endif

   do
   {
      // Compare Apn
      iLoop = memcmp(pBaseRec->Apn, pTaxBill->Apn, iApnLen);
      if (iLoop > 0 || pTaxBill->RecType != '2')
      {
         do
         {
            lTaxSkip++;
            pRec = fgets(acRec, MAX_RECSIZE, fdTaxBill);
         } while (pRec && pTaxBill->Inst1.Status == 'D');

         if (!pRec)
         {
            fclose(fdTaxBill);
            fdTaxBill = NULL;
            return 1;      // EOF
         }
      }
   } while (iLoop > 0 || pTaxBill->RecType != '2');

   if (iLoop)
      return 1;

   long lTaxAmt1 = atoin(pTaxBill->Inst1.TaxAmt, TSIZ_AMOUNT);
   long lDueAmt1 = atoin(pTaxBill->Inst1.DueAmt, TSIZ_AMOUNT);
   long lTaxAmt2 = atoin(pTaxBill->Inst2.TaxAmt, TSIZ_AMOUNT);
   long lDueAmt2 = atoin(pTaxBill->Inst2.DueAmt, TSIZ_AMOUNT);

   dDueAmt1=dDueAmt2 = 0.0;

   // Refresh tax bill for new year
   if (memcmp(pTaxBill->TaxYear, pBaseRec->TaxYear, 4) > 0)
   {
      strcpy(acTRA, pBaseRec->TRA);
      memset(pBaseRec, 0, sizeof(TAXBASE));

      memcpy(pBaseRec->Apn, pTaxBill->Apn, iApnLen);
      memcpy(pBaseRec->OwnerInfo.Apn, pTaxBill->Apn, iApnLen);
      strcpy(pBaseRec->TRA, acTRA);

      // BillNum
      memcpy(pBaseRec->BillNum, pTaxBill->BillNum, TSIZ_BILLNUM);
      memcpy(pBaseRec->OwnerInfo.BillNum, pTaxBill->BillNum, TSIZ_BILLNUM);

      // Tax Year
      memcpy(pBaseRec->TaxYear, pTaxBill->TaxYear, 4);
      memcpy(pBaseRec->OwnerInfo.TaxYear, pTaxBill->TaxYear, 4);

      // Owner info
      if (!memcmp(pTaxBill->Name2, "C/O", 3))
         strcpy(pBaseRec->OwnerInfo.CareOf, myTrim(pTaxBill->Name2, TSIZ_OWNERNAME));
      else
         strcpy(pBaseRec->OwnerInfo.Name2,  myTrim(pTaxBill->Name2, TSIZ_OWNERNAME));

      iTmp = blankRem(pTaxBill->Name1, TSIZ_OWNERNAME);
      memcpy(pBaseRec->OwnerInfo.Name1, pTaxBill->Name1, iTmp);

      // Update bill number
      memcpy(pBaseRec->BillNum, pTaxBill->BillNum, TSIZ_BILLNUM);

      // Check for Tax amount
      sprintf(pBaseRec->TaxAmt1, "%.2f", lTaxAmt1/100.0);
      memcpy(pBaseRec->DueDate1, pTaxBill->Inst1.DelqDate, TSIZ_DATE);
      sprintf(pBaseRec->TaxAmt2, "%.2f", lTaxAmt2/100.0);
      memcpy(pBaseRec->DueDate2, pTaxBill->Inst2.DelqDate, TSIZ_DATE);

      // Check Penalty
      if (memcmp(pTaxBill->Inst1.PaidDate, pTaxBill->Inst1.DelqDate, TSIZ_DATE) > 0)
      {
         pBaseRec->Inst1Status[0] = TAX_BSTAT_PAID;
         sprintf(pBaseRec->PenAmt1, "%.2f", (lDueAmt1-lTaxAmt1)/100.0);
         sprintf(pBaseRec->PaidAmt1, "%.2f", lDueAmt1/100.0);
         lDueAmt1 = 0;
      } else if (pTaxBill->Inst1.Status == 'P')
      {
         pBaseRec->Inst1Status[0] = TAX_BSTAT_PAID;
         strcpy(pBaseRec->PaidAmt1, pBaseRec->TaxAmt1);
         memcpy(pBaseRec->PaidDate1, pTaxBill->Inst1.PaidDate, TSIZ_DATE);
         pBaseRec->PenAmt1[0] = 0;
         lDueAmt1 = 0;
      }  else if (pTaxBill->Inst1.Status == 'A')
      {
         pBaseRec->Inst1Status[0] = TAX_BSTAT_UNPAID;
         if (!ChkDueDate(1, pBaseRec->DueDate1))
         {
            pBaseRec->PenAmt1[0] = 0;
         }
      }

      if (memcmp(pTaxBill->Inst2.PaidDate, pTaxBill->Inst2.DelqDate, TSIZ_DATE) > 0)
      {
         pBaseRec->Inst1Status[0] = TAX_BSTAT_PAID;
         pBaseRec->Inst2Status[0] = TAX_BSTAT_PAID;
         sprintf(pBaseRec->PenAmt2, "%.2f", (lDueAmt2-lTaxAmt2)/100.0);
         sprintf(pBaseRec->PaidAmt2, "%.2f", lDueAmt2/100.0);
         lDueAmt2 = 0;
      } else if (pTaxBill->Inst2.Status == 'P')
      {
         pBaseRec->Inst2Status[0] = TAX_BSTAT_PAID;
         strcpy(pBaseRec->PaidAmt2, pBaseRec->TaxAmt2);
         memcpy(pBaseRec->PaidDate2, pTaxBill->Inst2.PaidDate, TSIZ_DATE);
         pBaseRec->PenAmt2[0] = 0;
         lDueAmt2 = 0;
      } else
      {
         pBaseRec->Inst2Status[0] = TAX_BSTAT_UNPAID;
         if (!ChkDueDate(2, pBaseRec->DueDate2))
            pBaseRec->PenAmt2[0] = 0;
      }

      pBaseRec->isSecd[0] = '1';
   } else
   {
      // Update corrected tax bill number
      if (pTaxBill->OrgBillNum[0] > ' ')
         memcpy(pBaseRec->BillNum, pTaxBill->BillNum, TSIZ_BILLNUM);

      // Update paid info
      if (memcmp(pTaxBill->Inst1.PaidDate, pTaxBill->Inst1.DelqDate, TSIZ_DATE) > 0)
      {
         pBaseRec->Inst1Status[0] = TAX_BSTAT_PAID;
         sprintf(pBaseRec->PenAmt1, "%.2f", (lDueAmt1-lTaxAmt1)/100.0);    
         sprintf(pBaseRec->PaidAmt1, "%.2f", lDueAmt1/100.0);
         memcpy(pBaseRec->PaidDate1, pTaxBill->Inst1.PaidDate, TSIZ_DATE);
      } else if (pTaxBill->Inst1.Status == 'P')
      {
         pBaseRec->Inst1Status[0] = TAX_BSTAT_PAID;
         strcpy(pBaseRec->PaidAmt1, pBaseRec->TaxAmt1);
         memcpy(pBaseRec->PaidDate1, pTaxBill->Inst1.PaidDate, TSIZ_DATE);
         pBaseRec->PenAmt1[0] = 0;
      }  else if (pTaxBill->Inst1.Status == 'A')
      {
         dDueAmt1 = atof(pBaseRec->TaxAmt1);
         pBaseRec->Inst1Status[0] = TAX_BSTAT_UNPAID;
         if (!ChkDueDate(1, pBaseRec->DueDate1))
            pBaseRec->PenAmt1[0] = 0;
         else
            dDueAmt1 += atof(pBaseRec->PenAmt1);
      }

      if (memcmp(pTaxBill->Inst2.PaidDate, pTaxBill->Inst2.DelqDate, TSIZ_DATE) > 0)
      {
         pBaseRec->Inst1Status[0] = TAX_BSTAT_PAID;
         pBaseRec->Inst2Status[0] = TAX_BSTAT_PAID;
         sprintf(pBaseRec->PenAmt2, "%.2f", (lDueAmt2-lTaxAmt2)/100.0);    
         sprintf(pBaseRec->PaidAmt2, "%.2f", lDueAmt2/100.0);
         memcpy(pBaseRec->PaidDate2, pTaxBill->Inst2.PaidDate, TSIZ_DATE);
         lDueAmt2 = 0;
      } else if (pTaxBill->Inst2.Status == 'P')
      {
         pBaseRec->Inst2Status[0] = TAX_BSTAT_PAID;
         pBaseRec->PenAmt2[0] = 0;
         lDueAmt2 = 0;
         strcpy(pBaseRec->PaidAmt2, pBaseRec->TaxAmt2);
         memcpy(pBaseRec->PaidDate2, pTaxBill->Inst2.PaidDate, TSIZ_DATE);
      } else
      {
         dDueAmt2 = atof(pBaseRec->TaxAmt2);
         pBaseRec->Inst2Status[0] = TAX_BSTAT_UNPAID;
         if (!ChkDueDate(2, pBaseRec->DueDate2))
            pBaseRec->PenAmt2[0] = 0;
         else
            dDueAmt2 += atof(pBaseRec->PenAmt2);
      }
   }

   // Due amount
   if (dDueAmt1 > 0 || dDueAmt2 > 0)
      sprintf(pBaseRec->TotalDue, "%.2f", dDueAmt1+dDueAmt2);

   lTaxMatch++;

   // Updated date
   sprintf(pBaseRec->Upd_Date, "%d", lTaxBillDate);

   // Get next record
   pRec = fgets(acRec, MAX_RECSIZE, fdTaxBill);
   if (!pRec)
   {
      fclose(fdTaxBill);
      fdTaxBill = NULL;
   }

   return 0;
}

/**************************** Edx_Load_TaxBase *******************************
 *
 * Create base import file and import into SQL if specified
 *
 * Return 0 if success.
 *
 *****************************************************************************/

int Edx_Load_TaxBase(bool bImport)
{
   char     *pTmp, acBaseRec[MAX_RECSIZE], acDelqRec[1024], acRec[MAX_RECSIZE], acOutBuf[512],
            acBaseFile[_MAX_PATH], acDelqFile[_MAX_PATH], acOwnerFile[_MAX_PATH], 
            acTaxRoll[_MAX_PATH], acTaxDelq[_MAX_PATH], acTaxBill[_MAX_PATH], acTaxRdmpt[_MAX_PATH], 
            acDetailFile[_MAX_PATH], acAgencyFile[_MAX_PATH], acTmpFile[_MAX_PATH];

   int      iRet, iTmp;
   long     lBase=0, lCnt=0, lDelq=0;
   FILE     *fdOwner, *fdBase, *fdDelq, *fdDetail, *fdAgency, *fdIn, *fdBill, *fdSold4Tax, *fdInstalMT;
   TAXBASE  *pTaxBase = (TAXBASE *)&acBaseRec[0];

   LogMsg("Loading Tax file");

   sprintf(acDetailFile, sTaxOutTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "Items");
   sprintf(acOwnerFile, sTaxOutTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "Owner");
   sprintf(acAgencyFile, sTaxOutTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "Agency");
   sprintf(acDelqFile, sTaxOutTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "Delq");
   sprintf(acBaseFile, sTaxOutTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "Base");
   pTmp = strrchr(acBaseFile, '\\');
   *pTmp = 0;
   if (_access(acBaseFile, 0))
      _mkdir(acBaseFile);
   *pTmp = '\\';

   // Prepare input files
   GetIniString(myCounty.acCntyCode, "TaxBill", "", acTaxBill, _MAX_PATH, acIniFile);
   lTaxBillDate = getFileDate(acTaxBill);
   GetIniString(myCounty.acCntyCode, "TaxRollInfo", "", acTaxRoll, _MAX_PATH, acIniFile);
   lTaxRollInfoDate = getFileDate(acTaxRoll);
   GetIniString(myCounty.acCntyCode, "Sold4Tax", "", acTaxDelq, _MAX_PATH, acIniFile);
   lSold4TaxDate = getFileDate(acTaxDelq);
   GetIniString(myCounty.acCntyCode, "InstalMT", "", acTaxRdmpt, _MAX_PATH, acIniFile);
   lInstallMTDate = getFileDate(acTaxRdmpt);
   lLastTaxFileDate = lTaxBillDate;
   if (lLastTaxFileDate < lTaxRollInfoDate)
      lLastTaxFileDate = lTaxRollInfoDate;

   // Only process if new tax file
   iRet = isNewTaxFile(NULL, myCounty.acCntyCode);
   if (iRet <= 0)
   {
      lLastTaxFileDate = 0;
      return iRet;
   }

   // Open annual tax roll file
   LogMsg("Open Tax file %s", acTaxRoll);
   fdIn = fopen(acTaxRoll, "r");
   if (fdIn == NULL)
   {
      LogMsg("***** Error opening Tax file: %s\n", acTaxRoll);
      return -2;
   }  

   // Delinquent file
   sprintf(acRec, "%s\\%s\\Sold4Tax.srt", acTmpPath, myCounty.acCntyCode);
   // Sort APN ascending, Default date descending
   iTmp = sortFile(acTaxDelq, acRec, "S(9,11,C,A,20,8,C,D) ", &iRet);
   if (iTmp > 0)
   {
      LogMsg("Open delinquent file #1 %s", acRec);
      fdSold4Tax = fopen(acRec, "r");
      if (fdSold4Tax == NULL)
      {
         LogMsg("***** Error opening delinquent file #1: %s\n", acRec);
         return -2;
      }  
   }

   // Redemption file
   sprintf(acRec, "%s\\%s\\InstalMT.srt", acTmpPath, myCounty.acCntyCode);
   // Sort APN ascending, Default date descending
   iTmp = sortFile(acTaxRdmpt, acRec, "S(11,11,C,A,22,8,C,D) ", &iRet);
   if (iTmp > 0)
   {
      LogMsg("Open delinquent file #2 %s", acRec);
      fdInstalMT = fopen(acRec, "r");
      if (fdInstalMT == NULL)
      {
         LogMsg("***** Error opening delinquent file #2: %s\n", acRec);
         return -2;
      }  
   }

   // Open monthly update file
   LogMsg("Open TaxBill file %s", acTaxBill);
   fdBill = fopen(acTaxBill, "r");
   if (fdBill == NULL)
   {
      LogMsg("***** Error opening TaxBill file: %s\n", acTaxBill);
      return -2;
   }  

   // Create import Delq file
   LogMsg("Open Delq file %s", acDelqFile);
   fdDelq = fopen(acDelqFile, "w");
   if (fdDelq == NULL)
   {
      LogMsg("***** Error creating Delq file: %s\n", acDelqFile);
      return -4;
   }

   // Create import Base file
   LogMsg("Open Base file %s", acBaseFile);
   fdBase = fopen(acBaseFile, "w");
   if (fdBase == NULL)
   {
      LogMsg("***** Error creating base file: %s\n", acBaseFile);
      return -4;
   }

   // Create import Detail file
   LogMsg("Open Detail output file %s", acDetailFile);
   fdDetail = fopen(acDetailFile, "w");
   if (fdDetail == NULL)
   {
      LogMsg("***** Error creating Detail output file: %s\n", acDetailFile);
      return -4;
   }

   // Create import Agency file
   sprintf(acTmpFile, "%s\\%s\\%s_Agency.tmp", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   LogMsg("Open Agency file %s", acTmpFile);
   fdAgency = fopen(acTmpFile, "w");
   if (fdAgency == NULL)
   {
      LogMsg("***** Error creating Agency file: %s\n", acTmpFile);
      return -4;
   }

   // Create import Owner file
   LogMsg("Open Owner output file %s", acOwnerFile);
   fdOwner = fopen(acOwnerFile, "w");
   if (fdOwner == NULL)
   {
      LogMsg("***** Error creating Owner output file: %s\n", acOwnerFile);
      return -4;
   }

   // Merge loop 
   while (!feof(fdIn))
   {
      pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdIn);
      if (!pTmp)
         break;

      // Create new tax base record
      iRet = Edx_ParseTaxBase(acBaseRec, acDelqRec, acRec);
      if (!iRet)
      {
         // Update tax base using current tax bill
         if (fdBill)
            Edx_UpdateTaxBase(acBaseRec, fdBill);

         // Create Delq record
         if (acDelqRec[0] > ' ')
         {
            if (fdSold4Tax)
               Edx_UpdateTaxDelq1(acBaseRec, acDelqRec, fdSold4Tax);

            if (fdInstalMT)
               Edx_UpdateTaxDelq2(acDelqRec, fdInstalMT);

            Tax_CreateDelqCsv(acOutBuf, (TAXDELQ *)&acDelqRec);
            fputs(acOutBuf, fdDelq);
            lDelq++;
         }

         // Create detail & agency records
         Edx_ParseTaxDetail(fdDetail, fdAgency, acRec);

         // Create TaxBase record
         Tax_CreateTaxBaseCsv(acOutBuf, (TAXBASE *)&acBaseRec);
         lBase++;
         fputs(acOutBuf, fdBase);

         // Create TaxOwner record
         Tax_CreateTaxOwnerCsv(acOutBuf, (TAXOWNER *)&pTaxBase->OwnerInfo);
         fputs(acOutBuf, fdOwner);
      } else
      {
         LogMsg0("---> Drop record %d [%.80s]", lCnt, acRec); 
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdIn)
      fclose(fdIn);
   if (fdBill)
      fclose(fdBill);
   if (fdSold4Tax)
      fclose(fdSold4Tax);
   if (fdInstalMT)
      fclose(fdInstalMT);
   
   if (fdBase)
      fclose(fdBase);
   if (fdDelq)
      fclose(fdDelq);
   if (fdDetail)
      fclose(fdDetail);
   if (fdAgency)
      fclose(fdAgency);
   if (fdOwner)
      fclose(fdOwner);

   LogMsg("Total records processed:   %u", lCnt);
   LogMsg("           Base records:   %u", lBase);
   LogMsg("           Delq records:   %u", lDelq);
   LogMsg("TaxBill records skipped:   %u", lTaxSkip);
   LogMsg("  TaxBill records match:   %u", lTaxMatch);

   // Dedup Agency file - sort on Agency
   iRet = sortFile(acTmpFile, acAgencyFile, "S(#2,C,A) DEL(124) DUPO F(TXT)");

   // Import into SQL
   if (bImport)
   {
      iRet = doTaxImport(myCounty.acCntyCode, TAX_BASE);
      if (!iRet)
         iRet = doTaxImport(myCounty.acCntyCode, TAX_DETAIL);
      if (!iRet)
         iRet = doTaxImport(myCounty.acCntyCode, TAX_AGENCY);
      if (!iRet)
         iRet = doTaxImport(myCounty.acCntyCode, TAX_DELINQUENT);
      if (!iRet)
         iRet = doTaxImport(myCounty.acCntyCode, TAX_OWNER);
   } else
      iRet = 0;

   return iRet;
}

/************************************** Edx_FormatSaleLine1 *******************
 *
 ******************************************************************************/

int Edx_FormatSaleLine1(char *pOutRec, char *pInRec)
{
   EDX_SALE1 *pSale = (EDX_SALE1 *)pInRec;
   SCSAL_REC *pSCSal= (SCSAL_REC *)pOutRec;
   char      *pTmp, acTmp[256];
   int        iPrice, iRet;

   memset(pOutRec, ' ', sizeof(SCSAL_REC));
   memcpy(acTmp, pSale->Apn, SSIZ_APN);
   remChar(acTmp, '-', SSIZ_APN);
   memcpy(pSCSal->Apn, acTmp, SSIZ_APN);

   // DocDate
   pTmp = dateConversion(pSale->DocDate, acTmp, MM_DD_YYYY_1);
   if (pTmp)
      vmemcpy(pSCSal->DocDate, acTmp, SALE_SIZ_DOCDATE);
   else
      return -1;

   // DocNum
   memcpy(pSCSal->DocNum, pSale->DocNum, SSIZ_DOCNUM);

   // Sale price
   iPrice = atoin(pSale->SalePrice, SSIZ_SALE_PRICE);
   if (iPrice > 0)
   {
      sprintf(acTmp, "%*u", SSIZ_SALE_PRICE, iPrice);
      memcpy(pSCSal->SalePrice, acTmp, SSIZ_SALE_PRICE);
   }

   // DocType
   if (pSale->RJ_Code == 'M')
      memcpy(pSCSal->DocType, "16", 2);      // Gifted deed
   else if (pSale->RJ_Code == 'C')
   {
      if (pSale->Conf_Level == 'C')
         memcpy(pSCSal->DocType, "27", 2);   // Trustee's Deed
      else
         memcpy(pSCSal->DocType, "78", 2);   // DEED IN LIEU OF FORCLOSURE
   } else if (iPrice > 0)
      pSCSal->DocType[0] = '1';

#ifdef _DEBUG
   if (pSale->AdjSalePrice[0] > ' ')
      memcpy(pSCSal->AdjSalePrice, pSale->AdjSalePrice, SSIZ_SALE_PRICE);
#endif

   // Full/Partial
   double dTmp = atof(pSale->PctXfer);
   if (dTmp == 100.0)
   {
      pSCSal->SaleCode[0] = 'F';
      memcpy(pSCSal->PctXfer, "100", 3);
   } else if (dTmp > 0.0)
   {
      iRet = sprintf(acTmp, "%d", (int)(dTmp+0.5));
      memcpy(pSCSal->PctXfer, acTmp, iRet);
      pSCSal->SaleCode[0] = 'P';
   } else if (pSale->RJ_Code == 'D')
      pSCSal->SaleCode[0] = 'P';

   return 0;
}

int Edx_FormatSaleLine2(char *pOutRec, char *pInRec)
{
   EDX_SALE2 *pSale = (EDX_SALE2 *)pInRec;
   SCSAL_REC *pSCSal= (SCSAL_REC *)pOutRec;
   int       iTmp1, iTmp2;
   char      acTmp[32];

   double dTmp = atof(pSale->DocTax);
   if (dTmp > 0.0)
   {
      memcpy(pSCSal->StampAmt, pSale->DocTax, SSIZ_DOCTAX);
      iTmp1 = (int)(dTmp*SALE_FACTOR);
      iTmp2 = atoin(pSCSal->SalePrice, SSIZ_SALE_PRICE);
      if (iTmp1 != iTmp2)
      {
         LogMsg("*** Use sale price from DocTax APN=%.11s (%d <> %d)", pSCSal->Apn, iTmp1, iTmp2);
         sprintf(acTmp, "%*u", SSIZ_SALE_PRICE, iTmp1);
         memcpy(pSCSal->SalePrice, acTmp, SSIZ_SALE_PRICE);
      }
   }

   // Check

   return 0;
}

/******************************** Edx_ExtrSale ********************************
 *
 * Extract sales from sale file SALESREP.TXT
 *
 ******************************************************************************/

int Edx_ExtrSale(void)
{
   EDX_SALE1 *pSale1;
   SCSAL_REC *pCSale;

   char     acSaleRec[512], acCSaleRec[600], acTmp[256];
   char     acOutFile[_MAX_PATH], acTmpSale[_MAX_PATH];

   int      iRet, iTmp, iUpdateSale=0, iUpdateChar=0;
   long     lCnt=0;

   LogMsg0("Extract sale file");

   // Check current sale file
   if (_access(acSaleFile, 0))
   {
      LogMsg("***** Missing current sale file %s", acSaleFile);
      return -1;
   }

   // Create tmp sale file
   sprintf(acTmpSale, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "tmp");
   LogMsg("Create updated sale file %s", acTmpSale);
   fdCSale = fopen(acTmpSale, "w");
   if (fdCSale == NULL)
   {
      LogMsg("***** Error creating extract sale file: %s\n", acTmpSale);
      return -3;
   }

   // Open current sale
   LogMsg("Open current sale file %s", acSaleFile);
   fdSale = fopen(acSaleFile, "r");
   if (fdSale == NULL)
   {
      LogMsg("***** Error opening current sale file: %s\n", acSaleFile);
      return -2;
   }

   pCSale = (SCSAL_REC *)&acCSaleRec[0];
   pSale1 = (EDX_SALE1 *)&acSaleRec;

   // Merge loop
   while (fgets(acSaleRec, 1024, fdSale))
   {
      if (isdigit(pSale1->Apn[0]))
      {
         // Format cumsale record
         iRet = Edx_FormatSaleLine1(acCSaleRec, acSaleRec);
         fgets(acSaleRec, 1024, fdSale);
         iRet |= Edx_FormatSaleLine2(acCSaleRec, acSaleRec);
         if (!iRet)
         {
            pCSale->CRLF[0] = '\n';
            pCSale->CRLF[1] = 0;

            // Output current sale record
            fputs(acCSaleRec, fdCSale);
            iUpdateSale++;
         }
      }
      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdSale)
      fclose(fdSale);
   if (fdCSale)
      fclose(fdCSale);

   // Set Multi-parcel sale flag
   iRet = SetMultiSale(acTmpSale, NULL, myCounty.acCntyCode);

   // Append to cum sale file
   sprintf(acOutFile,  acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "dat");

   // Sort/merge sale file - APN (asc), Saledate (asc), DocNum (asc)
   sprintf(acTmp, "S(1,%d,C,A,27,8,C,A,15,%d,C,A,57,10,C,D) DUPO(B8012,1,34)", iApnLen, SALE_SIZ_DOCNUM);
   iTmp = sortFile(acTmpSale, acOutFile, acTmp);
   if (iTmp > 0)
   {
      strcpy(acSaleFile, acOutFile);
      if (!_access(acCSalFile, 0))
      {
         sprintf(acSaleRec, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "srt");
         sprintf(acTmpSale, "%s+%s", acOutFile, acCSalFile);
         iTmp = sortFile(acTmpSale, acSaleRec, acTmp);
         if (iTmp > 0)
         {
            DeleteFile(acCSalFile);
      
            // Rename srt to SLS file
            rename(acSaleRec, acCSalFile);
         }
      } else
      {
         CopyFile(acOutFile, acCSalFile, false);
      }
   }

   LogMsg("Total sale lines read:         %u", lCnt);
   LogMsg("Total sale records extracted:  %u", iUpdateSale);

   return iRet;
}

/**************************** Edx_ExtrHistSale ********************************
 *
 * Extract sales from sale file edx_sale_10yr.csv
 *
 ******************************************************************************/

int Edx_FormatHistSaleRec(char *pOutRec, char *pInRec)
{
   SCSAL_REC *pSCSal= (SCSAL_REC *)pOutRec;
   char       acTmp[256];
   int        iPrice, iRet;
   double     dTmp;

   iTokens = ParseStringIQ(pInRec, '|', MAX_FLD_TOKEN, apTokens);
   if (iTokens < EDX_HS_APPR_DT)
   {
      LogMsg0("*** Error: bad sale record %.16s (#tokens=%d)", pInRec, iTokens);
      return -1;
   }

   memset(pOutRec, ' ', sizeof(SCSAL_REC));
   vmemcpy(pSCSal->Apn, apTokens[EDX_HS_APN], SALE_SIZ_APN);

   // DocDate
   vmemcpy(pSCSal->DocDate, apTokens[EDX_HS_DOCDATE], SALE_SIZ_DOCDATE);

   // DocNum
   vmemcpy(pSCSal->DocNum, apTokens[EDX_HS_DOCNUM], SALE_SIZ_DOCNUM);

   // DocTax
   dTmp = atof(apTokens[EDX_HS_DOCTAX]);
   if (dTmp > 0.1)
   {
      iPrice = (int)(dTmp*SALE_FACTOR);
      sprintf(acTmp, "%*u", SSIZ_SALE_PRICE, iPrice);
      memcpy(pSCSal->SalePrice, acTmp, SSIZ_SALE_PRICE);
      pSCSal->DocType[0] = '1';

      vmemcpy(pSCSal->StampAmt, apTokens[EDX_HS_DOCTAX], SALE_SIZ_STAMPAMT);
   }

   // DocType
   if (*apTokens[EDX_HS_REJ_CODE] == 'M')
      memcpy(pSCSal->DocType, "16", 2);      // Gifted deed
   else if (*apTokens[EDX_HS_REJ_CODE] == 'C')
   {
      if (*apTokens[EDX_HS_CONF_LEVL] == 'C')
         memcpy(pSCSal->DocType, "27", 2);   // Trustee's Deed
      else
         memcpy(pSCSal->DocType, "78", 2);   // DEED IN LIEU OF FORCLOSURE
   } 

   // Full/Partial
   dTmp = atof(apTokens[EDX_HS_PCT_REAPPR]);
   if (dTmp == 100.0)
   {
      pSCSal->SaleCode[0] = 'F';
      memcpy(pSCSal->PctXfer, "100", 3);
   } else if (dTmp > 0.0)
   {
      iRet = sprintf(acTmp, "%d", (int)(dTmp+0.5));
      memcpy(pSCSal->PctXfer, acTmp, iRet);
      pSCSal->SaleCode[0] = 'P';
   } else if (*apTokens[EDX_HS_REJ_CODE] == 'D')
      pSCSal->SaleCode[0] = 'P';

   return 0;
}

int Edx_ExtrHistSale(char *pInfile)
{
   SCSAL_REC *pCSale;

   char     acSaleRec[1024], acCSaleRec[600];
   char     acOutFile[_MAX_PATH], acTmpSale[_MAX_PATH];

   int      iRet, iTmp, iUpdateSale=0, iUpdateChar=0;
   long     lCnt=0;

   LogMsg0("Update cum sale file");

   // Open current sale
   LogMsg("Open history sale file %s", pInfile);
   fdSale = fopen(pInfile, "r");
   if (fdSale == NULL)
   {
      LogMsg("***** Error opening history sale file: %s\n", pInfile);
      return -2;
   }

   // Create tmp sale file
   sprintf(acTmpSale, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "dat");
   LogMsg("Create updated sale file %s", acTmpSale);
   fdCSale = fopen(acTmpSale, "w");
   if (fdCSale == NULL)
   {
      LogMsg("***** Error creating extract sale file: %s\n", acTmpSale);
      return -3;
   }

   pCSale = (SCSAL_REC *)&acCSaleRec[0];
   // Skip header
   fgets(acSaleRec, 1024, fdSale);

   // Merge loop
   while (fgets(acSaleRec, 1024, fdSale))
   {
      if (isdigit(acSaleRec[0]))
      {
         // Format cumsale record
         iRet = Edx_FormatHistSaleRec(acCSaleRec, acSaleRec);
         if (!iRet)
         {
            pCSale->CRLF[0] = '\n';
            pCSale->CRLF[1] = 0;

            // Output current sale record
            fputs(acCSaleRec, fdCSale);
            iUpdateSale++;
         }
      }
      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdSale)
      fclose(fdSale);
   if (fdCSale)
      fclose(fdCSale);

   // Set Multi-parcel sale flag
   iRet = SetMultiSale(acTmpSale, NULL, myCounty.acCntyCode);

   // Sort/merge sale file - APN (asc), Saledate (asc), DocNum (asc)
   if (!_access(acCSalFile, 0))
   {
      sprintf(acOutFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "srt");
      sprintf(acSaleRec, "%s+%s", acTmpSale, acCSalFile);
      iTmp = sortFile(acSaleRec, acOutFile, "S(1,12,C,A,27,8,C,A,15,12,C,A,57,10,C,D) DUPO(B8012,1,34)");
      if (iTmp > 0)
      {
         DeleteFile(acCSalFile);
      
         // Rename srt to SLS file
         rename(acOutFile, acCSalFile);
      }
   } else
   {
      CopyFile(acOutFile, acCSalFile, false);
   }

   LogMsg("Total sale lines read:         %u", lCnt);
   LogMsg("Total sale records extracted:  %u", iUpdateSale);

   return iRet;
}

/*********************************** loadEdx ********************************
 *
 * Input files:
 *    - events.txt        (lien file, 547-byte ebcdic)
 *    - vendor.txt        (roll file, 547-byte ebcdic)
 *    - charac.txt        (char file, 70-byte ebcdic)
 *    - Salesrep.txt      (sale file, 133-byte ebcdic)
 *      This is a report file in mainframe format.
 *
 * Following are tasks to be done here:
 *    - Convert ebcdic to ascii
 *    - Update sale history file.  Currently disable because roll file contains
 *      the same info.
 *    - Normal update: LoadOne -U [-Lz]
 *         + use -Xx to extract history sale and -Ux to apply it to R01 file
 *    - Load Lien: LoadOne -L -Ms -Xl -Lz
 *    - Load daily: -L[yyyymmdd]: Load the provided date
 *                  -Ld         : Load today file
 *                  -Xsi        : Extract sale data and import into sale table
 *                  -T          : Load tax data to SQL
 *
 ****************************************************************************/

int loadEdx(int iSkip)
{
   int   iRet, iSrcDate, iDstDate;
   char  acRawFile[_MAX_PATH], acTmpFile[_MAX_PATH], acXferFile[_MAX_PATH], *pTmp;

   iApnLen = myCounty.iApnLen;

   // Load tax
   if (iLoadTax == TAX_LOADING)              // -T or -Lt
   {
      iRet = GetIniString(myCounty.acCntyCode, "TaxDist", "", acTmpFile, _MAX_PATH, acIniFile);
      if (iRet > 0)
         iRet = LoadTaxCodeTable(acTmpFile);

      // Load Tax Base and Delq
      iRet = Edx_Load_TaxBase(bTaxImport);
      if (!iRet)
         iRet = updateDelqFlag(myCounty.acCntyCode);
   }

   if (!iLoadFlag)
      return iRet;

   // Loading history sale - One time
   iRet = GetIniString(myCounty.acCntyCode, "SaleHist", "", acTmpFile, _MAX_PATH, acIniFile);
   if (iRet > 0 && !_access(acTmpFile, 0))
      iRet = Edx_ExtrHistSale(acTmpFile);

   // Load tables
   iRet = LoadLUTable((char *)&acLookupTbl[0], "[Quality]", NULL, MAX_ATTR_ENTRIES);
   if (!iRet)
   {
      LogMsg("*** Error Looking for table [Quality] in %s", acLookupTbl);
      return 1;
   }

   // Load zipcode table
   //if (iLoadFlag & LOAD_ZIP)          // -Lz
   //{
   //   int iZipLen;

   //   GetIniString(myCounty.acCntyCode, "ZipFile", "", acTmpFile, _MAX_PATH, acIniFile);
   //   iZipLen = GetPrivateProfileInt(myCounty.acCntyCode, "ZipRecSize", 53, acIniFile);

   //   if (!_access(acTmpFile, 0))
   //   {
   //      LogMsgD("Import zipcode to SQL");

   //      // Convert ebcdic to ascii
   //      sprintf(acXferFile, "%s\\%s\\ZipCode.asc", acTmpPath, myCounty.acCntyCode);
   //      LogMsg("Translate %s to Ascii %s", acTmpFile, acXferFile);

   //      iRet = doEBC2ASCAddCR(acTmpFile, acXferFile, iZipLen);
   //      if (!iRet)
   //      {
   //         // Load data into SQL table
   //         iRet = importZipcode(acXferFile);
   //         if (iRet)
   //            iLoadFlag = 0;
   //      } else
   //      {
   //         iLoadFlag = 0;
   //         LogMsgD("***** ERROR converting EBCDIC to ASCII file %s to %s", acTmpFile, acXferFile);
   //      }
   //   }
   //}

   // Extract sale history from O01 file - remove 8/10/2015
   //if (iLoadFlag & EXTR_XSAL)          // -Xx
   //{
   //   sprintf(acRawFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "S01");
   //   GetIniString(myCounty.acCntyCode, "XSalFile", "", acXferFile, _MAX_PATH, acIniFile);
   //   iRet = ExtrXfer(acRawFile, acXferFile);
   //   if (iRet > 0 &&  (iLoadFlag & LOAD_LIEN))
   //      iLoadFlag |= UPDT_XSAL;
   //}

   // Convert Xfer data to cum sale
   /*
   strcpy(acXferFile, "H:\\CO_PROD\\SEDX_CD\\raw\\Edx_Xfer_2006.dat");
   sprintf(acTmpFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "2006");
   iRet = convertXferData(acXferFile, acTmpFile);
   strcpy(acXferFile, "H:\\CO_PROD\\SEDX_CD\\raw\\Edx_Xfer_2007.dat");
   sprintf(acTmpFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "2007");
   iRet = convertXferData(acXferFile, acTmpFile);
   strcpy(acXferFile, "H:\\CO_PROD\\SEDX_CD\\raw\\Edx_Xfer_2008.dat");
   sprintf(acTmpFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "2008");
   iRet = convertXferData(acXferFile, acTmpFile);
   strcpy(acXferFile, "H:\\CO_PROD\\SEDX_CD\\raw\\Edx_Xfer_2009.dat");
   sprintf(acTmpFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "2009");
   iRet = convertXferData(acXferFile, acTmpFile);
   strcpy(acXferFile, "H:\\CO_PROD\\SEDX_CD\\raw\\Edx_Xfer_2010.dat");
   sprintf(acTmpFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "2010");
   iRet = convertXferData(acXferFile, acTmpFile);
   strcpy(acXferFile, "H:\\CO_PROD\\SEDX_CD\\raw\\Edx_Xfer.dat");
   sprintf(acTmpFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "dat");
   iRet = convertXferData(acXferFile, acTmpFile);
   */

   //sprintf(acXferFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "srt");
   //sprintf(acTmpFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "sls");
   //iRet = Edx_FixSales(acXferFile, acTmpFile);

   // Set Multi-parcel sale flag
   //iRet = SetMultiSale(acCSalFile);

   if (iLoadFlag & (UPDT_SALE|EXTR_SALE))      // -Us or -Xs
   {
      // Translate EBCDIC to ASCII
      strcpy(acTmpFile, acSaleFile);
      pTmp = strrchr(acSaleFile, '.');
      if (pTmp)
         strcpy(pTmp, ".ASC");
      else
         strcat(acSaleFile, ".ASC");

      iSrcDate = getFileDate(acTmpFile);
      iDstDate = getFileDate(acSaleFile);
      if (iDstDate <= iSrcDate)
      {
         LogMsg("Translate %s to Ascii %s", acTmpFile, acSaleFile);
         iRet = doEBC2ASCAddCR(acTmpFile, acSaleFile, iSaleLen);
         if (iRet)
         {
            LogMsgD("***** ERROR converting EBCDIC to ASCII file %s to %s", acTmpFile, acSaleFile);
            return -1;
         }
      }

      // Extract sale data
      iRet = Edx_ExtrSale();
      if (!iRet)
         iLoadFlag |= MERG_CSAL;
      else
         return iRet;
    }

   if (iLoadFlag & (LOAD_LIEN|LOAD_UPDT))
   {
      iRet = 0;
      // Translate Roll file ebcdic to ascii
      if (iLoadFlag & LOAD_LIEN)
         sprintf(acTmpFile, "%s\\%s\\Edx_Lien.asc", acTmpPath, myCounty.acCntyCode);
      else
         sprintf(acTmpFile, "%s\\%s\\Edx_Roll.asc", acTmpPath, myCounty.acCntyCode);

      LogMsgD("Translate %s to Ascii %s", acRollFile, acTmpFile);

      iRet = doEBC2ASC(acRollFile, acTmpFile);
      if (iRet)
      {
         iLoadFlag = 0;
         LogMsgD("***** ERROR converting EBCDIC to ASCII file %s to %s", acRollFile, acTmpFile);
      }
      strcpy(acRollFile, acTmpFile);

      // Translate char file ebcdic to ascii
      sprintf(acTmpFile, "%s\\%s\\Edx_Char.asc", acTmpPath, myCounty.acCntyCode);
      LogMsgD("Translate %s to Ascii %s", acCharFile, acTmpFile);

      iRet = doEBC2ASCAddCR(acCharFile, acTmpFile, iCharLen);
      if (iRet)
      {
         iLoadFlag = 0;
         LogMsgD("***** ERROR converting EBCDIC to ASCII file %s to %s", acCharFile, acTmpFile);
      }
      strcpy(acCharFile, acTmpFile);
   }

   if (iLoadFlag & EXTR_LIEN)
      iRet = Edx_ExtrLien();

   // Merge roll file
   if (iLoadFlag & LOAD_LIEN)
      iRet = Edx_Load_LDR(iSkip);
   if (iLoadFlag & LOAD_UPDT)
      iRet = Edx_Load_Roll(iSkip);
   else if (iLoadFlag & LOAD_DAILY)
   {
      GetIniString(myCounty.acCntyCode, "DailyFile", "", acGrGrTmpl, _MAX_PATH, acIniFile);
      iRollLen = GetPrivateProfileInt(myCounty.acCntyCode, "DailyRecSize", 547, acIniFile);
      sprintf(acRollFile, acGrGrTmpl, lProcDate);
      if (_access(acRollFile, 0))
      {
         LogMsg(" ---> %s is not available.", acRollFile);
         // Get yesterday file
         lProcDate = getExtendDate(acTmpFile, -1);
         sprintf(acRollFile, acGrGrTmpl, lProcDate);
         LogMsg(" ---> Try to process %s instead.", acRollFile);
      }
      if (_access(acRollFile, 0))
      {
         LogMsg("*** Daily file is not available %s", acRollFile);
         bGrGrAvail = false;
      } else
      {
         sprintf(acTmpFile, "%s\\%s\\Edx_Daily.asc", acTmpPath, myCounty.acCntyCode);
         LogMsgD("Translate %s to Ascii %s", acRollFile, acTmpFile);

         iRet = doEBC2ASC(acRollFile, acTmpFile);
         if (iRet)
         {
            iLoadFlag = 0;
            LogMsg("***** ERROR converting EBCDIC to ASCII file %s to %s", acRollFile, acTmpFile);
         } else
         {
            lLastGrGrDate = getFileDate(acRollFile);

            strcpy(acRollFile, acTmpFile);
            LogMsg("Merge daily file %s ...", acRollFile);
            iRet = Edx_MergeDaily(iSkip);
            if (!iRet)
            {
               /* Do not move  4/27/2011 sn

               char *pTmp;

               // Move processed file to daily folder
               sprintf(acRollFile, acGrGrTmpl, lProcDate);
               pTmp = strrchr(acRollFile, '\\');
               *pTmp = 0;
               sprintf(acTmpFile, "%s\\Daily\\%s", acRollFile, pTmp+1);
               *pTmp = '\\';

               LogMsg("Moving %s to %s", acRollFile, acTmpFile);
               try
               {
                  rename(acRollFile, acTmpFile);
               } catch (...)
               {
                  LogMsg("***** Fail moving daily file %s to %s", acRollFile, acTmpFile);
               }
               */
               iLoadFlag |= LOAD_UPDT;           // To update production flag that initiates the BuildCda step
            }
         }
      }
   }

   // Apply cum sale file to R01
   if (!iRet && (iLoadFlag & MERG_CSAL) )          // -Ms
   {
      // Resort cum sale file before apply
      //iRet = ApplyCumSale(iSkip, acCSalFile, true, SALE_USE_SCUPDXFR);
      iRet = ApplyCumSaleWP(iSkip, acCSalFile, true, SALE_USE_SCUPDXFR, CLEAR_OLD_SALE);
   }
   // Merge extracted sales
   else if (iLoadFlag & UPDT_XSAL)                 // -Ux
   {
      GetIniString(myCounty.acCntyCode, "XSalFile", "", acXferFile, _MAX_PATH, acIniFile);
      sprintf(acRawFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "R01");
      sprintf(acTmpFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "N01");

      iRet = Edx_MergeXferFile(acRawFile, acTmpFile, acXferFile);
      if (iRet > 0)
      {
         if (bClean)
            remove(acRawFile);
         else
         {
            char  acTmp[_MAX_PATH];
            sprintf(acTmp, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "T01");
            try
            {
               if (!_access(acTmp, 0))
                  remove(acTmp);
               rename(acRawFile, acTmp);
            } catch (...)
            {
               LogMsg("***** Fail renaming output file %s to %s", acRawFile, acTmp);
            }
         }

         rename(acTmpFile, acRawFile);

         iRet = 0;
      }
   }

   return iRet;
}