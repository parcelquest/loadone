/*****************************************************************************
 *
 * Options:
 *    -CSOL -O -L -Xl -[Xs|Ms] -Ma : load lien.  
 *    -CSOL -O -U [-T] [-Xn] [-Mn] : monthly update
 *
 * 03/21/2008 1.5.7.1   Use standard function to update usecode.
 * 12/11/2008 8.5.1     Make StrNum left justified
 * 03/06/2009 8.6.0     Add -Ma option to merge chars data extracted from the web.
 * 07/16/2009 9.1.3     Add Sol_ExtrLien() and modify Sol_Ldr_MergeRoll() to merge
 *                      other values.
 * 02/06/2010 9.4.0     Extending LEGAL to LEGAL1
 * 07/23/2010 10.1.5    Cleanup code. Add more debug check.
 * 11/16/2010 10.3.3    Fix Sol_Ldr_MergeAdr().
 * 04/10/2011 10.5.4    Rename SOL_CCHR to EX_CHAR.
 * 07/19/2011 11.0.4    Add S_HSENO
 * 12/12/2012 12.3.8    Add option to merge GRGR extracted from the web.
 * 12/21/2012 12.3.9.1  Fix Sol_MergeCChr() and Sol_MergeDocRec().
 * 10/15/2013 13.11.2   Use updateVesting() to update Vesting and Etal flag.
 * 08/10/2015 15.1.0    Remove -Xx option
 * ---                  Keep name as is
 * 08/28/2015 15.1.3    Add -Mr option to add lot sqft & lot area
 * 09/13/2015 15.2.2    Standardize MergeArea.
 * 04/06/2016 15.8.3    Load Tax data.
 * 06/01/2016 15.9.4    Modify Sol_MergeCChrRec() and Sol_MergeCChr() to support new EX_CHAR layout.
 * 07/19/2016 16.1.0    Modify Sol_MergeDocRec() to merge "AFFIDAVIT" to TRANSFER
 * 08/16/2017 17.2.1    Add NR_CreateSCSale() to convert NDC sale extract to SCSAL_REC format.
 * 10/25/2017 17.4.2    Fix NR_CreateSCSale() to remove extra header row.
 * 10/28/2017 17.4.3    Move NR_CreateSCSale() to NdcExtr.cpp to support more counties.
 * 01/02/2018 17.5.4    When update sale, turn ON update flag to initiate indexing task.
 * 01/05/2018 17.5.5    Modify Sol_Ldr_MergeAdr() to add BldgNo to UnitNo and correct mailing addr.
 * 03/23/2018 17.6.9    Remove automatic sale update from loadSol().  To update sale, use -Ms.
 * 05/20/2018 17.10.13  When loading tax update, only tax base is updated, not tax items.
 * 05/22/2018 17.11.0   Modify -Xs option to use template ASH_File defined in INI for output sale file.
 * 05/23/2018 17.11.2   Fix "PO BOX" problem in mailing addr.
 * 08/09/2018 18.3.0    Add -Mn to merge sale from NDC.
 * 08/17/2018 18.3.2    Add special case for post direction in MILITARY WEST.  Add S_ADDR_D.
 * 08/14/2018 18.4.0    Replace R01.MergeArea() with PQ_MergeLotArea() using new SOL_Basemap.txt
 * 09/29/2018 18.5.1    Move -Mr option to LoadOne.cpp
 * 12/05/2019 19.5.9    Modify Sol_Ldr_MergeRoll() to add LatePen to R01 file.
 * 04/02/2020 19.8.7    Remove -Xs and replace it with -Xn option to format and merge NDC recorder data.
 * 04/27/2020 19.9.0    Remove -Ut and use -T to load both full & partial tax file with TC_LoadTax().
 * 08/04/2020 20.2.6    Add -Xa option and Sol_MergeStdChar(), Sol_ConvComChar(), Sol_ConvMultiChar(), & Sol_ConvResChar().
 *                      Modify Sol_MergeLien() & Sol_CreateLienRec() to add EXE_CODE.  Modify Sol_Load_LDR() to add char.
 * 08/13/2020 20.2.9    Modify Sol_Load_Roll() to add chars by calling Sol_MergeStdChar().
 * 10/09/2020 20.2.15   Fix bug in Sol_MergeLien() to include OTHER_VAL into GROSS.
 * 04/11/2023 22.6.1    Add Sol_LoadTax() and Sol_Load_TaxDelq() to process new tax files.
 * 04/14/2023 22.6.2    Fix UNIT# issue in Sol_MergeAdr().
 * 05/19/2023 22.7.1    New Aumentum file layout.  Add Sol_Load_RollCsv(), Sol_MergeLegalCsv(), 
 *                      Sol_MergeSAdr(), Sol_MergeMAdr(), Sol_MergeRollCsv(), Sol_ConvStdChar().
 * 06/01/2023 22.8.3    Fix legal desc in Sol_MergeLegalCsv().
 * 07/09/2023 23.0.1    Add Sol_MergeLegalCsv2(), Sol_MergeMAdr2(), Sol_MergeLienCsv(), Sol_CreateLienRecCsv().
 *                      Modify Sol_MergeSAdr(), Sol_Load_LDR(), Sol_ExtrLien() to support new layout.
 * 07/23/2023 23.0.6    Modify Sol_ParseTaxBase() to handle special case where record is shifted caused by bad char.
 * 08/30/2023 23.1.5    Modify Sol_MergeOwner() to filter out weird character 'N' in Owner Name.
 * 10/18/2023 23.3.5    Bug fix in Sol_LoadTax() that write extra record to Items file.
 * 01/06/2024 23.5.2    Fix bug in Sol_MergeOwner() by extending buffer for Owners.
 * 01/07/2024 23.5.3    Fix record misalignment in Sol_ParseTaxBase().
 * 02/01/2024 23.6.0    Use ApplyCumSaleNR() to update NDC sale instead of ApplyCumSale().
 * 02/02/2024 23.6.0    Modify Sol_MergeMAdr() & Sol_MergeSAdr() to populate UnitNox.
 * 02/20/2024 23.6.2    Fix cum sale file name in loadSol().
 * 06/11/2024 23.9.1    Modify Sol_MergeLegalCsv() to increase buffer size to avoid crashing.
 * 06/23/2024 23.9.5    Add Escape bill in Sol_ParseTaxBase().  In the past, we ignore it.
 * 07/06/2024 24.0.0    Modify Sol_MergeLienCsv() to add exemption type.
 * 07/12/2024 24.0.2    Add Sol_MergeMAdr3(), modify Sol_MergeLienCsv() to add ExeCode.
 *                      Increase buffer in Sol_MergeLegalCsv2() to avoid crash on long legal.
 * 07/13/2024 24.0.3    Check for dup APN in Sol_Load_LDR().
 * 10/23/2024 24.2.0    Modify Sol_ParseTaxBase() to ignore prior year tax bill.
 *
 *****************************************************************************/

#include "stdafx.h"
#include "Prodlib.h"
#include "CountyInfo.h"
#include "R01.h"
#include "RecDef.h"
#include "Tables.h"
#include "XlatTbls.h"
#include "CharRec.h"
#include "SaleRec.h"
#include "Logs.h"
#include "LoadOne.h"
#include "Utils.h"
#include "doOwner.h"
#include "formatApn.h"
#include "MergeSol.h"
#include "UseCode.h"
#include "FldDef.h"
#include "LOExtrn.h"
#include "Tax.h"
#include "PQ.h"
#include "SendMail.h"
#include "doSort.h"
#include "NdcExtr.h"

static   FILE  *fdChar;
static long    lCharSkip, lCharMatch; 

/******************************** Sol_MergeOwner *****************************
 *
 * Return 0 if successful, >0 if error
 *
 *****************************************************************************/

void Sol_MergeOwner(char *pOutbuf, char *pNames)
{
   char  acOwners[128], acTmp1[128], acSave[128];
   char  *pTmp, *pTmp1;

   OWNER myOwner;

   // Clear output buffer if needed
   removeNames(pOutbuf, false);

   // Process as two names
   strcpy(acOwners, myTrim(pNames));

   // Update vesting
   updateVesting(myCounty.acCntyCode, acOwners, pOutbuf+OFF_VEST, pOutbuf+OFF_ETAL_FLG);

   // Don't parse name start with number
   if (*pNames < 'A')
   {
      vmemcpy(pOutbuf+OFF_NAME1, pNames, SIZ_NAME1);
      vmemcpy(pOutbuf+OFF_NAME_SWAP, pNames, SIZ_NAME_SWAP);
      return;
   }

   vmemcpy(pOutbuf+OFF_NAME1, pNames, SIZ_NAME1);

   // Check for vesting
   pTmp1 = strrchr(acOwners, ' ');
   if (pTmp1)
   {
      strcpy(acSave, ++pTmp1);
      pTmp = getVestingCode_LAS(acSave, acTmp1);
      if (*pTmp)
         *pTmp1 = 0;
   }

   // Parse owner
   if (pTmp1 = strchr(acOwners, 0xD1))
   {
      LogMsg("*** Bad char found in Owner: %s [%.*s]", acOwners, iApnLen, pOutbuf);
      *pTmp1 = 'N';
   }
   splitOwner(acOwners, &myOwner, 2);

   //if (myOwner.acName2[0] && strcmp(myOwner.acName1, myOwner.acName2))
   //   memcpy(pOutbuf+OFF_NAME2, myOwner.acName2, strlen(myOwner.acName2));

   vmemcpy(pOutbuf+OFF_NAME_SWAP, myOwner.acSwapName, SIZ_NAME_SWAP);
}

// This version is used for fixed length record
void Sol_MergeOwner1(char *pOutbuf, char *pNames)
{
   int   iTmp;
   char  acOwners[128], acTmp1[128], acSave[128];
   char  *pTmp, *pTmp1;

   OWNER myOwner;

   // Clear output buffer if needed
   removeNames(pOutbuf, false);

   // Process as two names
   memcpy(acOwners, pNames, SIZ_ROLL_ASSESSEE);
   iTmp = blankRem(acOwners, SIZ_ROLL_ASSESSEE);

   // Update vesting
   updateVesting(myCounty.acCntyCode, acOwners, pOutbuf+OFF_VEST, pOutbuf+OFF_ETAL_FLG);

   // Don't parse name start with number
   if (*pNames < 'A')
   {
      memcpy(pOutbuf+OFF_NAME1, pNames, iTmp);
      memcpy(pOutbuf+OFF_NAME_SWAP, pNames, iTmp);
      return;
   }

   // Check for vesting
   pTmp1 = strrchr(myTrim(acOwners), ' ');
   if (pTmp1)
   {
      strcpy(acSave, ++pTmp1);
      pTmp = getVestingCode_LAS(acSave, acTmp1);
      if (*pTmp)
         *pTmp1 = 0;
   }

   // Parse owner
   splitOwner(acOwners, &myOwner, 2);
   memcpy(pOutbuf+OFF_NAME1, myOwner.acName1, strlen(myOwner.acName1));

   if (myOwner.acName2[0] && strcmp(myOwner.acName1, myOwner.acName2))
      memcpy(pOutbuf+OFF_NAME2, myOwner.acName2, strlen(myOwner.acName2));

   vmemcpy(pOutbuf+OFF_NAME_SWAP, myOwner.acSwapName, SIZ_NAME_SWAP);
}

/********************************* Sol_MergeAdr ******************************
 *
 * Return 0 if successful, >0 if error
 *
 *****************************************************************************/

void Sol_MergeAdr(char *pOutbuf, char *pRollRec)
{
   SOL_ROLL *pRec;
   char     acTmp[256], acAddr1[128], acAddr2[128];
   int      iTmp, iStrNo, iLen;
   ADR_REC  sMailAdr;
   ADR_REC  sSitusAdr;

   pRec = (SOL_ROLL *)pRollRec;

   // Clear old Mailing
   removeMailing(pOutbuf, false);

   // Check for blank address
   if (memcmp(pRec->M_Addr1, "     ", 5))
   {
      memcpy(acAddr1, pRec->M_Addr1, SIZ_ROLL_ADDR1);
      iLen = blankRem(acAddr1, SIZ_ROLL_ADDR1);

      // Check for Germany
      memcpy(acTmp, pRec->M_CitySt, SIZ_ROLL_CITYST);
      acTmp[SIZ_ROLL_CITYST] = 0;
      if (strstr(acTmp, "GERMANY"))
      {
         memcpy(acAddr2, pRec->M_Addr2, SIZ_ROLL_ADDR2);
         myTrim(acAddr2, SIZ_ROLL_ADDR2);
         strcat(acAddr2, " ");
         strcat(acAddr2, acTmp);

         memcpy(pOutbuf+OFF_M_ADDR_D, acAddr1, strlen(acAddr1));
         memcpy(pOutbuf+OFF_M_CTY_ST_D, acAddr2, strlen(acAddr2));

         acAddr1[SIZ_M_STREET] = 0;
         memcpy(pOutbuf+OFF_M_STREET, acAddr1, strlen(acAddr1));
         acAddr2[SIZ_M_CITY] = 0;
         memcpy(pOutbuf+OFF_M_CITY, acAddr2, strlen(acAddr2));

         return;
      }

      if (!memcmp(acAddr1, "CO", 2) || !memcmp(acAddr1, "C/O", 3) ||
          (acAddr1[0] > '9' && isdigit(pRec->M_Addr2[0])) 
         )
      {
         updateCareOf(pOutbuf, acAddr1, iLen);

         memcpy(acAddr1, pRec->M_Addr2, SIZ_ROLL_ADDR2);
         iLen = blankRem(acAddr1, SIZ_ROLL_ADDR2);
      } else if (!strchr(acAddr1, ' '))
      {
         // In case 0125-082-120 where strNo is in Addr1 and strName is in Addr2
         strcat(acAddr1, " ");
         strcat(acAddr1, myTrim(pRec->M_Addr2, SIZ_ROLL_ADDR2));
      }

      memcpy(acTmp, pRec->M_CitySt, SIZ_ROLL_CITYST);
      iTmp = blankRem(acTmp, SIZ_ROLL_CITYST);
      if (pRec->M_Zip[0] > ' ')
         sprintf(acAddr2, "%s %.5s", acTmp, pRec->M_Zip);
      else
         strcpy(acAddr2, acTmp);
      memcpy(pOutbuf+OFF_M_CTY_ST_D, acAddr2, strlen(acAddr2));

      memcpy(pOutbuf+OFF_M_ADDR_D, acAddr1, strlen(acAddr1));
      iTmp = atoin(pRec->M_Zip, 5);
      if (iTmp > 10001)
      {
         memcpy(pOutbuf+OFF_M_ZIP, pRec->M_Zip, SIZ_M_ZIP);
         memcpy(pOutbuf+OFF_M_ZIP4, (char *)&pRec->M_Zip[6], SIZ_M_ZIP4);
      } else
      {
         memcpy(pOutbuf+OFF_M_ZIP, BLANK32, SIZ_M_ZIP+SIZ_M_ZIP4);
      }

#ifdef _DEBUG
      //if (!memcmp(pOutbuf, "0032061400", 10))
      //   iTmp = 0;
#endif

      // Parsing mail address
      memset((void *)&sMailAdr, 0, sizeof(ADR_REC));
      parseAdr1_1(&sMailAdr, myTrim(acAddr1));

      parseAdr2(&sMailAdr, myTrim(acAddr2));

      if (sMailAdr.lStrNum > 0)
      {
         sprintf(acTmp, "%d      ", sMailAdr.lStrNum);
         memcpy(pOutbuf+OFF_M_STRNUM, acTmp, SIZ_M_STRNUM);
         if (sMailAdr.strSub[0] > '0')
         {
            sprintf(acTmp, "%s  ", sMailAdr.strSub);
            memcpy(pOutbuf+OFF_M_STR_SUB, acTmp, SIZ_M_STR_SUB);
         }
      }
      vmemcpy(pOutbuf+OFF_M_DIR, sMailAdr.strDir, SIZ_M_DIR);
      vmemcpy(pOutbuf+OFF_M_STREET, sMailAdr.strName, SIZ_M_STREET);
      vmemcpy(pOutbuf+OFF_M_SUFF, sMailAdr.strSfx, SIZ_M_SUFF);
      vmemcpy(pOutbuf+OFF_M_UNITNO, sMailAdr.Unit, SIZ_M_UNITNO);
      vmemcpy(pOutbuf+OFF_M_CITY, sMailAdr.City, SIZ_M_CITY);
      vmemcpy(pOutbuf+OFF_M_ST, sMailAdr.State, SIZ_M_ST);
   }

   // Situs
   iStrNo = atoin(pRec->S_StrNum, SIZ_ROLL_NUM);
   if (iStrNo > 0 && pRec->S_StrName[0] >= ' ')
   {
      sprintf(acAddr1, "%d       ", iStrNo);
      memcpy(pOutbuf+OFF_S_STRNUM, acAddr1, SIZ_S_STRNUM);
      memcpy(pOutbuf+OFF_S_HSENO, acAddr1, SIZ_S_STRNUM);

      // Copy strsub or unit#
      if (pRec->S_Unit[0] > ' ')
      {
         memcpy(acTmp, pRec->S_Unit, SIZ_ROLL_UNIT);
         acTmp[SIZ_ROLL_UNIT] = 0;
         if (strchr(acTmp, '/'))
            memcpy(pOutbuf+OFF_S_STR_SUB, acTmp, SIZ_S_STR_SUB);
         else
            memcpy(pOutbuf+OFF_S_UNITNO, acTmp, SIZ_ROLL_UNIT);
         strcat(acAddr1, acTmp);
      }

      // Copy street name
      memcpy(acTmp, pRec->S_StrName, SIZ_ROLL_ROAD);
      acTmp[SIZ_ROLL_ROAD] = 0;
      parseAdr1S(&sSitusAdr, myTrim(acTmp));

      if (sSitusAdr.strName[0] > ' ')
         memcpy(pOutbuf+OFF_S_STREET, sSitusAdr.strName, strlen(sSitusAdr.strName));
      if (sSitusAdr.strDir[0] > ' ')
         memcpy(pOutbuf+OFF_S_DIR, sSitusAdr.strDir, strlen(sSitusAdr.strDir));
      if (sSitusAdr.strSfx[0] > ' ')
         memcpy(pOutbuf+OFF_S_SUFF, sSitusAdr.strSfx, strlen(sSitusAdr.strSfx));

      sprintf(acTmp, "%s %s", acAddr1, acTmp);
      blankRem(acTmp);
      memcpy(pOutbuf+OFF_S_ST, "CA", 2);
      if (pRec->S_City[0] >= 'A')
      {
         // Translate city abbr to city code
         for (int iIdx = 0; asCityAbbr[iIdx]; iIdx++)
         {
            if (!memcmp(asCityAbbr[iIdx], pRec->S_City, SIZ_ROLL_CITY))
            {
               iTmp = sprintf(acTmp, "%d   ", iIdx+1);
               memcpy(pOutbuf+OFF_S_CITY, acTmp, iTmp);
               strcpy(sSitusAdr.City, asCityName[iIdx]);
               break;
            }
         }
      } else if (!strcmp(myTrim(sMailAdr.strName), myTrim(sSitusAdr.strName)) && sMailAdr.City[0] > ' ')
      {
         // Assuming that no situs city and we have to use mail city
         City2Code(sMailAdr.City, acTmp);
         if (acTmp[0] > ' ')
         {
            memcpy(pOutbuf+OFF_S_CITY, acTmp, strlen(acTmp));
            strcpy(sSitusAdr.City, sMailAdr.City);
         } else
            LogMsg("*** Bad city name %s in %.14s", sMailAdr.City, pOutbuf);
         memcpy(pOutbuf+OFF_S_ZIP, pOutbuf+OFF_M_ZIP, SIZ_S_ZIP+SIZ_S_ZIP4);
      } else
      {
         if ((sMailAdr.City[0] >= 'A') && (pRec->M_Zip[0] > '8') && pRec->HO_Type[0] == 'H')
         {
            // Get city code
            City2Code(sMailAdr.City, acTmp);
            if (acTmp[0] > ' ')
            {
               memcpy(pOutbuf+OFF_S_CITY, acTmp, strlen(acTmp));
               memcpy(pOutbuf+OFF_S_ZIP, pRec->M_Zip, 5);
               strcpy(sSitusAdr.City, sMailAdr.City);
            } else
               LogMsg(">>> Bad city name %s in %.14s", sMailAdr.City, pOutbuf);
         }
      }

      if (sSitusAdr.City[0] >= 'A')
      {
         sprintf(acAddr2, "%s CA", sSitusAdr.City);
         blankRem(acAddr2);
         memcpy(pOutbuf+OFF_S_CTY_ST_D, acAddr2, strlen(acAddr2));
      }
   }
}

void Sol_Ldr_MergeAdr(char *pOutbuf, char *pRollRec)
{
   SOL_LIEN *pRec;
   char     acTmp[256], acAddr1[64], acAddr2[64], acUnit[16];
   int      iTmp, iStrNo, iLen;
   ADR_REC  sMailAdr;
   ADR_REC  sSitusAdr;

   pRec = (SOL_LIEN *)pRollRec;

   // Clear old Mailing
   removeMailing(pOutbuf, false);

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "014210", 6))
   //   iTmp = 0;
#endif

   // Check for blank address
   if (memcmp(pRec->M_Addr1, "     ", 5))
   {
      if (isdigit(pRec->M_Addr1[0]) || !memcmp(pRec->M_Addr1, "PO BOX", 6))
         memcpy(acAddr1, pRec->M_Addr1, SIZ_ROLL_ADDR1);
      else
         memcpy(acAddr1, pRec->M_Addr2, SIZ_ROLL_ADDR2);

      iLen = blankRem(acAddr1, SIZ_ROLL_ADDR1);

      // Check for Germany
      memcpy(acTmp, pRec->M_CitySt, SIZ_ROLL_CITYST);
      acTmp[SIZ_ROLL_CITYST] = 0;
      if (strstr(acTmp, "GERMANY"))
      {
         memcpy(acAddr2, pRec->M_Addr2, SIZ_ROLL_ADDR2);
         myTrim(acAddr2, SIZ_ROLL_ADDR2);
         strcat(acAddr2, " ");
         strcat(acAddr2, acTmp);

         memcpy(pOutbuf+OFF_M_ADDR_D, acAddr1, strlen(acAddr1));
         memcpy(pOutbuf+OFF_M_CTY_ST_D, acAddr2, strlen(acAddr2));

         acAddr1[SIZ_M_STREET] = 0;
         memcpy(pOutbuf+OFF_M_STREET, acAddr1, strlen(acAddr1));
         acAddr2[SIZ_M_CITY] = 0;
         memcpy(pOutbuf+OFF_M_CITY, acAddr2, strlen(acAddr2));

         return;
      }

      iTmp = 0;
      if (!memcmp(pRec->M_Addr2, "SUITE", 5))
         iTmp = 1;
      if (!memcmp(pRec->M_Addr2, "PO", 2))
         iTmp = 2;
      if (!memcmp(acAddr1, "CO", 2))
         iTmp = 3;
      if (!memcmp(acAddr1, "C/O", 3))
         iTmp = 4;

      if (iTmp == 1)    // Append addr1 & addr2
      {
         memcpy(acAddr1, pRec->M_Addr1, SIZ_ROLL_ADDR1+SIZ_ROLL_ADDR2);
         iTmp = blankRem(acAddr1, SIZ_ROLL_ADDR1+SIZ_ROLL_ADDR2);
      } else if (iTmp > 0 || (acAddr1[0] > '9' && isdigit(pRec->M_Addr2[0])) )
      {
         updateCareOf(pOutbuf, acAddr1, iLen);

         memcpy(acAddr1, pRec->M_Addr2, SIZ_ROLL_ADDR2);
         iLen = blankRem(acAddr1, SIZ_ROLL_ADDR2);
      } else if (!strchr(acAddr1, ' '))
      {
         // In case 0125-082-120 where strNo is in Addr1 and strName is in Addr2
         strcat(acAddr1, " ");
         strcat(acAddr1, myTrim(pRec->M_Addr2, SIZ_ROLL_ADDR2));
      }

      memcpy(acAddr2, pRec->M_CitySt, SIZ_ROLL_CITYST);
      iTmp = blankRem(acAddr2, SIZ_ROLL_CITYST);
      memcpy(pOutbuf+OFF_M_CTY_ST_D, acAddr2, iTmp);
      memcpy(pOutbuf+OFF_M_ADDR_D, acAddr1, strlen(acAddr1));

      // Start processing
      iTmp = atoin(pRec->M_Zip, 5);
      if (iTmp > 10001)
      {
         memcpy(pOutbuf+OFF_M_ZIP, pRec->M_Zip, SIZ_M_ZIP);
         memcpy(pOutbuf+OFF_M_ZIP4, (char *)&pRec->M_Zip[6], SIZ_M_ZIP4);
      }

      // Parsing mail address
      memset((void *)&sMailAdr, 0, sizeof(ADR_REC));

      parseAdr1_1(&sMailAdr, myTrim(acAddr1));
      parseAdr2(&sMailAdr, myTrim(acAddr2));

      if (sMailAdr.lStrNum > 0)
      {
         sprintf(acTmp, "%d      ", sMailAdr.lStrNum);
         memcpy(pOutbuf+OFF_M_STRNUM, acTmp, SIZ_M_STRNUM);
         if (sMailAdr.strSub[0] > '0')
         {
            sprintf(acTmp, "%s  ", sMailAdr.strSub);
            memcpy(pOutbuf+OFF_M_STR_SUB, acTmp, SIZ_M_STR_SUB);
         }
      }
      blankPad(sMailAdr.strDir, SIZ_M_DIR);
      memcpy(pOutbuf+OFF_M_DIR, sMailAdr.strDir, SIZ_M_DIR);
      blankPad(sMailAdr.strName, SIZ_M_STREET);
      memcpy(pOutbuf+OFF_M_STREET, sMailAdr.strName, SIZ_M_STREET);
      blankPad(sMailAdr.strSfx, SIZ_M_SUFF);
      memcpy(pOutbuf+OFF_M_SUFF, sMailAdr.strSfx, SIZ_M_SUFF);
      blankPad(sMailAdr.Unit, SIZ_M_UNITNO);
      memcpy(pOutbuf+OFF_M_UNITNO, sMailAdr.Unit, SIZ_M_UNITNO);
      blankPad(sMailAdr.City, SIZ_M_CITY);
      memcpy(pOutbuf+OFF_M_CITY, sMailAdr.City, SIZ_M_CITY);
      blankPad(sMailAdr.State, SIZ_M_ST);
      memcpy(pOutbuf+OFF_M_ST, sMailAdr.State, SIZ_M_ST);
   }

   // Situs
   iStrNo = atoin(pRec->S_StrNum, SIZ_ROLL_NUM);
   if (iStrNo > 0 && pRec->S_StrName[0] >= ' ')
   {
      sprintf(acAddr1, "%d       ", iStrNo);
      memcpy(pOutbuf+OFF_S_STRNUM, acAddr1, SIZ_S_STRNUM);
      memcpy(pOutbuf+OFF_S_HSENO, acAddr1, SIZ_S_STRNUM);

      // Copy strsub or unit#
      memcpy(acTmp, pRec->S_Unit, SIZ_ROLL_UNIT);
      acTmp[SIZ_ROLL_UNIT] = 0;
      if (strchr(acTmp, '/'))
         memcpy(pOutbuf+OFF_S_STR_SUB, acTmp, SIZ_S_STR_SUB);
      else
      {
         if (pRec->S_Bldg[0] > ' ')
         {
            // Bldg only
            if (pRec->S_Unit[0] == ' ')
            {
               sprintf(acUnit, "BLDG%.2s", pRec->S_Bldg);
            } else
               sprintf(acUnit, "%.2s-%.4s", pRec->S_Bldg, pRec->S_Unit);
            iTmp = remChar(acUnit, ' ');
            if (iTmp > SIZ_S_UNITNO)
            {
               LogMsg("*** Change UnitNo from %s to %s", acUnit, &acUnit[1]);
               acUnit[SIZ_S_UNITNO+1] = 0;
               vmemcpy(pOutbuf+OFF_S_UNITNO, &acUnit[1], SIZ_S_UNITNO);
            } else
               vmemcpy(pOutbuf+OFF_S_UNITNO, acUnit, SIZ_S_UNITNO);
         } else
            memcpy(pOutbuf+OFF_S_UNITNO, acTmp, SIZ_ROLL_UNIT);
      }

      // Copy street name
      if (!memcmp(pRec->S_StrName, "WEST MILITARY", 10))
         strcpy(acTmp, "MILITARY WEST");
      else if (!memcmp(pRec->S_StrName, "EAST MILITARY", 10))
         strcpy(acTmp, "MILITARY EAST");
      else
         memcpy(acTmp, pRec->S_StrName, SIZ_ROLL_ROAD);
      acTmp[SIZ_ROLL_ROAD] = 0;

      strcat(acAddr1, acTmp);
      iTmp = blankRem(acAddr1);
      memcpy(pOutbuf+OFF_S_ADDR_D, acAddr1, iTmp);

      parseAdr1S(&sSitusAdr, myTrim(acTmp));

      if (sSitusAdr.strName[0] > ' ')
         memcpy(pOutbuf+OFF_S_STREET, sSitusAdr.strName, strlen(sSitusAdr.strName));
      if (sSitusAdr.strDir[0] > ' ')
         memcpy(pOutbuf+OFF_S_DIR, sSitusAdr.strDir, strlen(sSitusAdr.strDir));
      if (sSitusAdr.strSfx[0] > ' ')
         memcpy(pOutbuf+OFF_S_SUFF, sSitusAdr.strSfx, strlen(sSitusAdr.strSfx));

      memcpy(pOutbuf+OFF_S_ST, "CA", 2);
      if (pRec->S_City[0] >= 'A')
      {
         // Translate city abbr to city code
         for (int iIdx = 0; asCityAbbr[iIdx]; iIdx++)
         {
            if (!memcmp(asCityAbbr[iIdx], pRec->S_City, SIZ_ROLL_CITY))
            {
               iTmp = sprintf(acTmp, "%d   ", iIdx+1);
               memcpy(pOutbuf+OFF_S_CITY, acTmp, iTmp);
               strcpy(sSitusAdr.City, asCityName[iIdx]);
               break;
            }
         }
      } else if (!strcmp(myTrim(sMailAdr.strName), myTrim(sSitusAdr.strName)) && sMailAdr.City[0] > ' ')
      {
         // Assuming that no situs city and we have to use mail city
         City2Code(sMailAdr.City, acTmp);
         if (acTmp[0] > ' ')
         {
            memcpy(pOutbuf+OFF_S_CITY, acTmp, strlen(acTmp));
            strcpy(sSitusAdr.City, sMailAdr.City);
         } else
            LogMsg("*** Bad city name %s in %.14s", sMailAdr.City, pOutbuf);
         memcpy(pOutbuf+OFF_S_ZIP, pOutbuf+OFF_M_ZIP, SIZ_S_ZIP+SIZ_S_ZIP4);
      } else
      {
         if ((sMailAdr.City[0] >= 'A') && (pRec->M_Zip[0] > '8') && pRec->HO_ExeType[0] == 'H')
         {
            // Get city code
            City2Code(sMailAdr.City, acTmp);
            if (acTmp[0] > ' ')
            {
               memcpy(pOutbuf+OFF_S_CITY, acTmp, strlen(acTmp));
               memcpy(pOutbuf+OFF_S_ZIP, pRec->M_Zip, 5);
               strcpy(sSitusAdr.City, sMailAdr.City);
            } else
               LogMsg(">>> Bad city name %s in %.14s", sMailAdr.City, pOutbuf);
         }
      }

      if (sSitusAdr.City[0] > ' ')
      {
         sprintf(acAddr2, "%s CA %.5s", sSitusAdr.City, pOutbuf+OFF_S_ZIP);
         iTmp = blankRem(acAddr2);
         memcpy(pOutbuf+OFF_S_CTY_ST_D, acAddr2, iTmp);         
      }
   }
}

/********************************* Sol_MergeLegal ****************************
 *
 *    RM00550073    16  16  OAKWOOD ESTATES
 *    -> RM BK 55 PG 73 LT 16 OAKWOOD ESTATES
 *
 *
 *****************************************************************************/

void Sol_MergeLegal(char *pOutbuf, char *pRollRec)
{
   SOL_ROLL *pRec;
   char     *pTmp, acTmp[64], acLegal[256];
   int      iLen, iBook, iPage;

   pRec = (SOL_ROLL *)pRollRec;

   if (pRec->MapType[0] == ' ')
      return;

   iBook = atoin(pRec->MapBkPg, 4);
   iPage = atoin((char *)&pRec->MapBkPg[4], 4);

   sprintf(acLegal, "%.2s BK %d PG %d", pRec->MapType, iBook, iPage);
   if (pRec->MapBlock[0] != ' ')
   {
      sprintf(acTmp, " BLK %.4s", pRec->MapBlock);
      strcat(acLegal, myTrim(acTmp));
   }
   if (pRec->MapLot[0] != ' ')
   {
      sprintf(acTmp, " LT %.4s", pRec->MapLot);
      strcat(acLegal, myTrim(acTmp));
   }
   if (pRec->MapSubUnit[0] != ' ')
   {
      sprintf(acTmp, " UN %.4s", pRec->MapSubUnit);
      strcat(acLegal, myTrim(acTmp));
   }
   if (pRec->MapSubName[0] != ' ')
   {
      sprintf(acTmp, " %.30s", pRec->MapSubName);
      // Clean up different spelling of subdivision
      pTmp = strstr(acTmp, "SUBDIV");
      if (pTmp)
         strcpy(pTmp, "SUBD");
      strcat(acLegal, myTrim(acTmp));
   }

   if (acLegal[0] > ' ')
   {
      iLen = updateLegal(pOutbuf, acLegal);
      if (iLen > iMaxLegal)
         iMaxLegal = iLen;
   }
}

/******************************* Sol_MergeLegalCsv ***************************
 *
 * Create legal desc from map info
 *    0080171190: PM BK 33 PG 54 LT 16
 *    0031023020: RM BK 15 PG 23 BLK 21 LT 11 UN 8 BRANDEN
 *    0082432140: RM BK 49 PG 32 HIGHLANDS CONDOMINIUMS AMENDED
 *    0132455580: RM BK 39 PG 95 LT 18 GLENWOOD CREST
 *    0132455610: RM BK 35 PG 41 LT 17 UN 1 GLENWOOD ESTATES
 *
 *****************************************************************************/

void Sol_MergeLegalCsv(char *pOutbuf)
{
   char     acTmp[256], acLegal[512];
   int      iLen, iBook, iPage;

   if (*apTokens[SOL_R_MAPTYPE] < '0')
      return;

   iBook = atoin(apTokens[SOL_R_MAPBOOKPAGE], 4);
   iPage = atol(apTokens[SOL_R_MAPBOOKPAGE]+4);

   if (iBook > 0 && iPage > 0)
      sprintf(acLegal, "%.2s BK %d PG %d", apTokens[SOL_R_MAPTYPE], iBook, iPage);
   else if (iBook > 0)
      sprintf(acLegal, "%.2s BK %d", apTokens[SOL_R_MAPTYPE], iBook);
   else if (iPage > 0)
      sprintf(acLegal, "%.2s PG %d", apTokens[SOL_R_MAPTYPE], iPage);
   else
      sprintf(acLegal, "%.2s", apTokens[SOL_R_MAPTYPE]);

   if (*apTokens[SOL_R_MAPBLOCK] > ' ')
   {
      sprintf(acTmp, " BLK %s", apTokens[SOL_R_MAPBLOCK]);
      strcat(acLegal, myTrim(acTmp));
   }
   if (*apTokens[SOL_R_MAPLOT] > ' ')
   {
      if (*apTokens[SOL_R_MAPSUBLOT] > ' ')
         sprintf(acTmp, " LT %s-%s", apTokens[SOL_R_MAPLOT], apTokens[SOL_R_MAPSUBLOT]);
      else
         sprintf(acTmp, " LT %s", apTokens[SOL_R_MAPLOT]);
      strcat(acLegal, myTrim(acTmp));
   }
   if (*apTokens[SOL_R_SUBDIV_UNIT] > ' ')
   {
      sprintf(acTmp, " UN %s", apTokens[SOL_R_SUBDIV_UNIT]);
      strcat(acLegal, myTrim(acTmp));
   }
   if (*apTokens[SOL_R_SUBDIV_NAME] > ' ')
   {
      sprintf(acTmp, " %s", apTokens[SOL_R_SUBDIV_NAME]);
      // Clean up different spelling of subdivision
      //char *pTmp = strstr(acTmp, "SUBDIV");
      //if (pTmp)
      //   strcpy(pTmp, "SUBD");
      strcat(acLegal, myTrim(acTmp));
   }

   if (acLegal[0] > ' ')
   {
      iLen = updateLegal(pOutbuf, acLegal);
      if (iLen > iMaxLegal)
         iMaxLegal = iLen;
   }
}

void Sol_MergeLegalCsv2(char *pOutbuf)
{
   char     acTmp[512], acLegal[512];
   int      iLen, iBook, iPage;

   if (*apTokens[SOL_L_MAPTYPE] < '0')
      return;

   iBook = atoin(apTokens[SOL_L_MAPBOOKPAGE], 4);
   iPage = atol(apTokens[SOL_L_MAPBOOKPAGE]+4);

   if (iBook > 0 && iPage > 0)
      sprintf(acLegal, "%.2s BK %d PG %d", apTokens[SOL_L_MAPTYPE], iBook, iPage);
   else if (iBook > 0)
      sprintf(acLegal, "%.2s BK %d", apTokens[SOL_L_MAPTYPE], iBook);
   else if (iPage > 0)
      sprintf(acLegal, "%.2s PG %d", apTokens[SOL_L_MAPTYPE], iPage);
   else
      sprintf(acLegal, "%.2s", apTokens[SOL_L_MAPTYPE]);

   if (*apTokens[SOL_L_MAPBLOCK] > ' ')
   {
      sprintf(acTmp, " BLK %s", apTokens[SOL_L_MAPBLOCK]);
      strcat(acLegal, myTrim(acTmp));
   }
   if (*apTokens[SOL_L_MAPLOT] > ' ')
   {
      if (*apTokens[SOL_L_MAPSUBLOT] > ' ')
         sprintf(acTmp, " LT %s-%s", apTokens[SOL_L_MAPLOT], apTokens[SOL_L_MAPSUBLOT]);
      else
         sprintf(acTmp, " LT %s", apTokens[SOL_L_MAPLOT]);
      strcat(acLegal, myTrim(acTmp));
   }
   if (*apTokens[SOL_L_SUBDIV_UNIT] > ' ')
   {
      sprintf(acTmp, " UN %s", apTokens[SOL_L_SUBDIV_UNIT]);
      strcat(acLegal, myTrim(acTmp));
   }
   if (*apTokens[SOL_L_SUBDIV_NAME] > ' ')
   {
      sprintf(acTmp, " %s", apTokens[SOL_L_SUBDIV_NAME]);
      strcat(acLegal, myTrim(acTmp));
   }

   if (acLegal[0] > ' ')
   {
      iLen = updateLegal(pOutbuf, acLegal);
      if (iLen > iMaxLegal)
         iMaxLegal = iLen;
   }
}

/******************************* Sol_MergeStdChar ****************************
 *
 *
 *
 *****************************************************************************/

int Sol_MergeStdChar(char *pOutbuf)
{
   static   char acRec[2048], *pRec=NULL;
   char     acTmp[256];
   long     lTmp, lGarSqft;
   int      iTmp, iLoop, iBeds, iFBath;

   STDCHAR  *pChar;

   iBeds=iFBath=lGarSqft=0;

   // Get first Char rec for first call
   if (!pRec)
   {
      pRec = fgets(acRec, 2048, fdChar);
   }

   do
   {
      if (!pRec)
      {
         fclose(fdChar);
         fdChar = NULL;
         return 1;      // EOF
      }

      // Compare Apn
      iLoop = memcmp(pOutbuf, pRec, myCounty.iApnLen);
      if (iLoop > 0)
      {
         if (bDebug)
            LogMsg0("Skip Char rec  %.*s", myCounty.iApnLen, pRec);
         pRec = fgets(acRec, 2048, fdChar);
         lCharSkip++;
      }
   } while (iLoop > 0);

   // If not match, return
   if (iLoop)
      return 0;

   pChar = (STDCHAR *)pRec;

   // Quality Class
   *(pOutbuf+OFF_BLDG_CLASS) = pChar->BldgClass;
   *(pOutbuf+OFF_BLDG_QUAL)  = pChar->BldgQual;

   // Condition
   //if (pChar->ImprCond[0] > '0')
   //   *(pOutbuf+OFF_IMPR_COND) = pChar->ImprCond[0];

   // YrBlt
   if (pChar->YrBlt[0] > '0')
      memcpy(pOutbuf+OFF_YR_BLT, (char *)&pChar->YrBlt, SIZ_YR_BLT);

   // YrEff
   if (pChar->YrEff[0] > '0')
      memcpy(pOutbuf+OFF_YR_EFF, (char *)&pChar->YrEff, SIZ_YR_BLT);

   // Stories
   if (pChar->Stories[0] > '0')
      vmemcpy(pOutbuf+OFF_STORIES, pChar->Stories, SIZ_STORIES);

   // BldgSqft
   lTmp = atoin(pChar->BldgSqft, SIZ_CHAR_SQFT);
   if (lTmp > 10)
   {
      sprintf(acTmp, "%*u", SIZ_BLDG_SF, lTmp);
      memcpy(pOutbuf+OFF_BLDG_SF, acTmp, SIZ_BLDG_SF);
   }

   // FloorSqft
   lTmp = atoin(pChar->Sqft_1stFl, SIZ_CHAR_SQFT);
   if (lTmp > 1)
   {
      iTmp = sprintf(acTmp, "%d", lTmp);
      memcpy(pOutbuf+OFF_FLOOR1_SF, acTmp, iTmp);
   }
   lTmp = atoin(pChar->Sqft_2ndFl, SIZ_CHAR_SQFT);
   if (lTmp > 1)
   {
      iTmp = sprintf(acTmp, "%d", lTmp);
      memcpy(pOutbuf+OFF_FLOOR2_SF, acTmp, iTmp);
   }
   lTmp = atoin(pChar->Sqft_Above2nd, SIZ_CHAR_SQFT);
   if (lTmp > 1)
   {
      iTmp = sprintf(acTmp, "%d", lTmp);
      memcpy(pOutbuf+OFF_FLOOR3_SF, acTmp, iTmp);
   } 

   // Additional area - MiscSqft
   lTmp = atoin(pChar->MiscSqft, SIZ_CHAR_SQFT);
   if (lTmp > 1)
   {
      iTmp = sprintf(acTmp, "%d", lTmp);
      memcpy(pOutbuf+OFF_MISCIMPR_SF, acTmp, iTmp);
   }

   // Garage Sqft
   lGarSqft = atoin(pChar->GarSqft, SIZ_CHAR_SQFT);
   if (lGarSqft > 10)
   {
      sprintf(acTmp, "%*u", SIZ_GAR_SQFT, lGarSqft);
      memcpy(pOutbuf+OFF_GAR_SQFT, acTmp, SIZ_GAR_SQFT);
      // This is to cover where Garage_Spcs is 0
      *(pOutbuf+OFF_PARK_TYPE) = 'Z';
   }

   // Garage spaces 
   if (pChar->ParkSpace[0] > '0')
      memcpy(pOutbuf+OFF_PARK_SPACE, pChar->ParkSpace, sizeof(pChar->ParkSpace));

   // Central Heating-Cooling
   if (pChar->Heating[0] > ' ')
      *(pOutbuf+OFF_HEAT) = pChar->Heating[0];
   if (pChar->Cooling[0] > ' ')
      *(pOutbuf+OFF_AIR_COND) = pChar->Cooling[0];

   // Rooms
   iTmp = atoin(pChar->Rooms, SIZ_CHAR_ROOMS);
   if (iTmp > 0)
   {
      sprintf(acTmp, "%*u", SIZ_ROOMS, iTmp);
      memcpy(pOutbuf+OFF_ROOMS, acTmp, SIZ_ROOMS);
   }

   // Beds
   iBeds = atoin(pChar->Beds, SIZ_CHAR_BEDS);
   if (iBeds > 0)
   {
      sprintf(acTmp, "%*u", SIZ_BEDS, iBeds);
      memcpy(pOutbuf+OFF_BEDS, acTmp, SIZ_BEDS);
   }

   // Bath
   if (pChar->FBaths[0] > '0')
   {
      iFBath = atoin(pChar->FBaths, SIZ_CHAR_BATHS);
      sprintf(acTmp, "%*u", SIZ_BATH_F, iFBath);
      memcpy(pOutbuf+OFF_BATH_F, acTmp, SIZ_BATH_F);
      memcpy(pOutbuf+OFF_BATH_4Q, acTmp, SIZ_BATH_F);
   }
   if (pChar->HBaths[0] > '0')
   {
      memcpy(pOutbuf+OFF_BATH_H, " 1", 2);
      memcpy(pOutbuf+OFF_BATH_2Q, " 1", 2);
   } 

   // Fireplace
   *(pOutbuf+OFF_FIRE_PL) = pChar->Fireplace[0];

   // Roof
   if (pChar->RoofMat[0] > ' ')
      *(pOutbuf+OFF_ROOF_MAT) = pChar->RoofMat[0];

   // Pool/Spa
   *(pOutbuf+OFF_POOL) = pChar->Pool[0];

   // Lot sqft - Lot Acres
   if (pChar->LotSqft[0] > '0')
   {
      memcpy(pOutbuf+OFF_LOT_SQFT, pChar->LotSqft, SIZ_LOT_SQFT);
      memcpy(pOutbuf+OFF_LOT_ACRES, pChar->LotAcre, SIZ_LOT_SQFT);
   }

   // Units 
   iTmp = atoin(pChar->Units, SIZ_CHAR_UNITS);
   if (iTmp > 0)
      memcpy(pOutbuf+OFF_UNITS, pChar->Units, SIZ_CHAR_UNITS);

   // Neighborhood code
   //if (*(pOutbuf+OFF_NBH_CODE) == ' ' && pChar->Nbh_Code[0] > ' ')
   //   memcpy(pOutbuf+OFF_NBH_CODE, pChar->Nbh_Code, SIZ_NBH_CODE);

   // Zoning
   //if (*(pOutbuf+OFF_ZONE) == ' ' && pChar->Zoning[0] > ' ')
   //   memcpy(pOutbuf+OFF_ZONE, pChar->Zoning, SIZ_ZONE);

   // Basement Sqft
   //lTmp = atoin(pChar->BsmtSqft, SIZ_CHAR_SQFT);
   //if (lTmp > 50)
   //{
   //   iTmp = sprintf(acTmp, "%d", lTmp);
   //   memcpy(pOutbuf+OFF_BSMT_SF, acTmp, iTmp);
   //}

   // View - not avail.
   // Buildings - not avail

   // Others

   lCharMatch++;

   // Get next Char rec
   pRec = fgets(acRec, 2048, fdChar);

   return 0;
}

/********************************* Sol_MergeChar *****************************
 *
 *
 *
 *****************************************************************************/

void Sol_MergeChar(char *pOutbuf, char *pCharRec)
{
   char     acTmp[64];
   int      iTmp, iTmp1, iTmp2;
   SOL_CHAR *pRec;

   pRec = (SOL_CHAR *)pCharRec;

   // Beds
   if (pRec->Beds[0] == '0') pRec->Beds[0] = ' ';
   memcpy(pOutbuf+OFF_BEDS, pRec->Beds, SIZ_BEDS);

   // Baths
   if (pRec->Baths[0] != '0')
      *(pOutbuf+OFF_BATH_F+1) = pRec->Baths[0];
   if (pRec->Baths[1] != '0')
      *(pOutbuf+OFF_BATH_H+1) = '1';

   // Pool
   if (pRec->Pool[0] == 'P' && pRec->Pool[1] == 'L')
      *(pOutbuf+OFF_POOL) = 'P';
   else if (pRec->Pool[0] == 'C' && pRec->Pool[1] == 'N')
      *(pOutbuf+OFF_POOL) = 'A';
   else
      *(pOutbuf+OFF_POOL) = ' ';

   // Garage Sqft
   iTmp = 0;
   while (pRec->Gar_Sqft[iTmp] == '0' && iTmp < SIZ_GAR_SQFT)
      pRec->Gar_Sqft[iTmp++] = ' ';
   memcpy(pOutbuf+OFF_GAR_SQFT, pRec->Gar_Sqft, SIZ_GAR_SQFT);

   // Park type
   if (iTmp < SIZ_GAR_SQFT)
      *(pOutbuf+OFF_PARK_TYPE) = 'Z';

   // Quality
   char acQual[SIZ_BLDG_QUAL];
   iTmp = atoin(pRec->Qual_Cls, SOL_SIZCHAR_QUALCLS);
   sprintf(acTmp, "%.1f", (float)iTmp/10);
   iTmp = Value2Code(acTmp, acQual, NULL);
   blankPad(acQual, SIZ_BLDG_QUAL);
   memcpy(pOutbuf+OFF_BLDG_QUAL, acQual, SIZ_BLDG_QUAL);

   // Yr Built
   if (pRec->YrBlt[0] > '0')
      memcpy(pOutbuf+OFF_YR_BLT, pRec->YrBlt, SIZ_YR_BLT);

   // Fire Place
   if (pRec->Fp[0] == 'Y')
      *(pOutbuf+OFF_FIRE_PL+1) = '1';

   // Central Heat/Air/No
   if (pRec->Central_HA[0] == 'H')
      *(pOutbuf+OFF_HEAT) = 'Z';
   if (pRec->Central_HA[1] == 'A')
      *(pOutbuf+OFF_AIR_COND) = 'C';

   // Stories - check for sqft on FL2
   iTmp1 = atoin(pRec->Fl1_Sqft, SOL_SIZCHAR_FLXSQFT);
   iTmp2 = atoin(pRec->Fl2_Sqft, SOL_SIZCHAR_FLXSQFT);
   if (iTmp2 > 0)
      memcpy(pOutbuf+OFF_STORIES, " 2.0", SIZ_STORIES);
   else
   {
      if (iTmp1 > 0)
         memcpy(pOutbuf+OFF_STORIES, " 1.0", SIZ_STORIES);
   }

   // Bldg area
   iTmp = iTmp1+iTmp2;
   if (iTmp > 0)
   {
      iTmp += atoin(pRec->Add_Sqft, SOL_SIZCHAR_ADDSQFT);
      sprintf(acTmp, "%*u", SIZ_BLDG_SF, iTmp);
      memcpy(pOutbuf+OFF_BLDG_SF, acTmp, SIZ_BLDG_SF);
   }

   // Total Rooms
   int iRooms;
   iTmp = atoin(pRec->OthRooms, 2);
   iRooms = iTmp + atoin(pRec->Beds, 2) + (pRec->Baths[0] & 0x0F);
   if (pRec->Dining[0] == 'Y')
      iRooms++;
   if (pRec->Family[0] == 'Y')
      iRooms++;
   if (pRec->UtilRoom[0] == 'Y')
      iRooms++;
   sprintf(acTmp, "%3d", iRooms);
   memcpy(pOutbuf+OFF_ROOMS, acTmp, SIZ_ROOMS);

   // Remove this to make room for CAREOF & LEGAL 8/9/2009 spn
   // Other improvement
   //iTmp = atoin(pRec->BldgArea, SIZ_CHAR_BLDGAREA);
   //if (iTmp > 0)
   //{
   //   memcpy(pOutbuf+OFF_OTHER_ROOM, pRec->BldgDesc, SIZ_CHAR_BLDGDESC);
   //}
}

/****************************** Sol_MergeCChrRec *****************************
 *
 *
 *****************************************************************************/

void Sol_MergeCChrRec(char *pOutbuf, char *pCharRec)
{
   char     acTmp[64];
   int      iTmp;
   EX_CHAR *pRec;

   pRec = (EX_CHAR *)pCharRec;

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "0108202050", 10))
   //   iTmp = 0;
#endif
   
   // LotAcres/LotSqft
   double  dTmp = atof(pRec->LotAcres) * 1000.0;
   if (dTmp > 0.0)
   {
      sprintf(acTmp, "%*u", SIZ_LOT_ACRES, (long)(dTmp+0.01));
      memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);
      iTmp = atoin(pRec->LotSqft, SIZ_LOT_SQFT);
      sprintf(acTmp, "%*u", SIZ_LOT_SQFT, iTmp);
      memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);
   }

   // Quality
   char acQual[SIZ_BLDG_QUAL];
   blankRem(pRec->QualCls, 8);
   if (pRec->QualCls[0])
   {
      iTmp = Value2Code(pRec->QualCls, acQual, NULL);
      blankPad(acQual, SIZ_BLDG_QUAL);
      memcpy(pOutbuf+OFF_BLDG_QUAL, acQual, SIZ_BLDG_QUAL);
   }

   // Central Heat/Air/No
   *(pOutbuf+OFF_HEAT) = pRec->Heat[0];
   *(pOutbuf+OFF_AIR_COND) = pRec->AirCond[0];

   // Beds
   iTmp = atoin(pRec->Beds, 5);
   if (iTmp > 0)
   {
      sprintf(acTmp, "%*u", SIZ_BEDS, iTmp);
      memcpy(pOutbuf+OFF_BEDS, acTmp, SIZ_BEDS);
   }

   // Yr Built
   if (pRec->YrBlt[0] > '0')
      memcpy(pOutbuf+OFF_YR_BLT, pRec->YrBlt, SIZ_YR_BLT);

   // Yr Eff
   if (pRec->YrEff[0] > '0')
      memcpy(pOutbuf+OFF_YR_EFF, pRec->YrEff, SIZ_YR_BLT);

   // Baths
   iTmp = atoin(pRec->Baths, 5);
   if (iTmp > 0)
   {
      sprintf(acTmp, "%*u", SIZ_BATH_F, iTmp);
      memcpy(pOutbuf+OFF_BATH_F, acTmp, SIZ_BATH_F);

      if (pRec->Baths[2] > '0')
         *(pOutbuf+OFF_BATH_H+1) = '1';
   }

   // Fire Place
   *(pOutbuf+OFF_FIRE_PL) = pRec->Fp[0];

   // Pool
   *(pOutbuf+OFF_POOL) = pRec->Pool[0];

   // Stories - check for sqft on FL2
   if (pRec->Area2[0] > '0')
      memcpy(pOutbuf+OFF_STORIES, "2.0 ", 4);
   else
   {
      if (pRec->Area1[0] > '0')
         memcpy(pOutbuf+OFF_STORIES, "1.0 ", 4);
   }

   // Residential/Bldg area
   iTmp = atoin(pRec->Res_Area, 8, true);
   if (iTmp > 0)
   {
      sprintf(acTmp, "%*u", SIZ_BLDG_SF, iTmp);
      memcpy(pOutbuf+OFF_BLDG_SF, acTmp, SIZ_BLDG_SF);
   }

   // Total Rooms
   iTmp = atoin(pRec->Rooms, 5);
   if (iTmp > 0)
   {
      sprintf(acTmp, "%*u", SIZ_ROOMS, iTmp);
      memcpy(pOutbuf+OFF_ROOMS, acTmp, SIZ_ROOMS);
   }

   // Garage Sqft
   iTmp = atoin(pRec->Gar_Area, 8, true);
   if (iTmp > 0)
   {
      sprintf(acTmp, "%*u", SIZ_GAR_SQFT, iTmp);
      memcpy(pOutbuf+OFF_GAR_SQFT, acTmp, SIZ_GAR_SQFT);
      *(pOutbuf+OFF_PARK_TYPE) = 'Z';
   } else
   {
      iTmp = atoin(pRec->Carp_Area, 8, true);
      if (iTmp > 0)
      {
         sprintf(acTmp, "%*u", SIZ_GAR_SQFT, iTmp);
         memcpy(pOutbuf+OFF_GAR_SQFT, acTmp, SIZ_GAR_SQFT);
         *(pOutbuf+OFF_PARK_TYPE) = 'C';
      }
   }

   // Basement area
   iTmp = atoin(pRec->BsmtArea, 8, true);
   if (iTmp > 0)
   {
      sprintf(acTmp, "%*u", SIZ_BSMT_SF, iTmp);
      memcpy(pOutbuf+OFF_BSMT_SF, acTmp, SIZ_BSMT_SF);
   }

   // Ag/timber preserve
   if (pRec->AgStat[0] == 'A')
      *(pOutbuf+OFF_AG_PRE) = 'Y';
   else
      *(pOutbuf+OFF_AG_PRE) = ' ';

   if (pRec->TimbStat[0] == 'A')
      *(pOutbuf+OFF_TIMBER) = 'Y';
   else
      *(pOutbuf+OFF_TIMBER) = ' ';
}

/********************************* Sol_MergeCChr *****************************
 *
 * Merge current chars pull down from the county website
 *
 *****************************************************************************/

int Sol_MergeCChr(char *pCnty, int iSkip)
{
   char     *pTmp, acBuf[MAX_RECSIZE], acCharRec[MAX_RECSIZE];
   char     acRawFile[_MAX_PATH], acOutFile[_MAX_PATH];

   HANDLE   fhIn, fhOut;

   int      iRet, iRollUpd=0;
   DWORD    nBytesRead;
   DWORD    nBytesWritten;
   BOOL     bRet, bEof, bRename=true;
   long     lRet=0, lCnt=0;
   EX_CHAR  *pRec = (EX_CHAR *)&acCharRec[0];

   LogMsg("Merge EX_CHAR to R01 file");

   sprintf(acRawFile, acRawTmpl, pCnty, pCnty, "R01");
   if (_access(acRawFile, 0))
   {
      sprintf(acRawFile, acRawTmpl, pCnty, pCnty, "S01");
      bRename = false;
   }

   sprintf(acOutFile, acRawTmpl, pCnty, pCnty, "N01");

   // Open Chars file
   LogMsg("Open Chars file %s", acCChrFile);
   fdChar = fopen(acCChrFile, "r");
   if (fdChar == NULL)
   {
      LogMsg("***** Error opening Chars file: %s\n", acCChrFile);
      return -2;
   }

   // Open Input file
   LogMsg("Open input file %s", acRawFile);
   fhIn = CreateFile(acRawFile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);
   if (fhIn == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening input file: %s\n", acRawFile);
      return -3;
   }

   // Open Output file
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);
   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening output file: %s\n", acOutFile);
      return -4;
   }

   // Get first Chars rec
   pTmp = fgets((char *)&acCharRec[0], MAX_RECSIZE, fdChar);
   bEof = (pTmp ? false:true);

   // Copy skip record
   memset(acBuf, ' ', iRecLen);
   while (iSkip-- > 0)
   {
      ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);
      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
   }

   // Merge loop
   while (!bEof)
   {
      bRet = ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);

      // Check for EOF
      if (!bRet)
      {
         LogMsg("Error reading input file %s (%f)", acRawFile, GetLastError());
         break;
      }

      // EOF ?
      if (iRecLen != nBytesRead)
         break;

#ifdef _DEBUG
      //if (!memcmp(acBuf, "0183123320", 10))
      //   bRet = 0;
#endif
 
      if (fdChar)
      {
Sol_Char_ReLoad:
         // Merge Char
         iRet = memcmp(acBuf, acCharRec, iApnLen);
         if (!iRet)
         {
            // Merge data
            Sol_MergeCChrRec(acBuf, acCharRec);
            iRollUpd++;

            // Read next CHAR record
            pTmp = fgets((char *)&acCharRec[0], MAX_RECSIZE, fdChar);
            if (!pTmp)
            {
               fclose(fdChar);
               fdChar = NULL;
            }
         } else
         {
            if (iRet > 0)
            {
               // Char not match, advance to next char record
               if (bDebug) // && memcmp(acBuf, acCharRec, 4))
                  LogMsg("CHAR not match --> %.*s (R01) > %.*s (CHAR) ", iApnLen, acBuf, iApnLen, acCharRec);
               
               pTmp = fgets((char *)&acCharRec[0], MAX_RECSIZE, fdChar);
               if (pTmp)
                  goto Sol_Char_ReLoad;
               else
               {
                  fclose(fdChar);
                  fdChar = NULL;
               }
            } else
               // Char not match, advance to next char record
               if (bDebug) // && memcmp(acBuf, acCharRec, 4))
                  LogMsg("CHAR not match --> %.*s (R01) < %.*s (CHAR) ", iApnLen, acBuf, iApnLen, acCharRec);
         }
      }

      if (!isdigit(acCharRec[0]))
         LogMsg("Invalid CHARS at APN=%.*s (%d)", iApnLen, acBuf, lCnt);

      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);

      if (!bRet)
      {
         LogMsg("Error occurs: %d\n", GetLastError());
         lRet = WRITE_ERR;
         break;
      }
   }

   // Close files
   if (fdChar)
      fclose(fdChar);
   if (fhOut)
      CloseHandle(fhOut);
   if (fhIn)
   {
      CloseHandle(fhIn);
      // Rename outfile
      if (bRename)
         remove(acRawFile);
      else
         sprintf(acRawFile, acRawTmpl, pCnty, pCnty, "R01");

      LogMsg("Rename output file %s to %s", acOutFile, acRawFile);
      lRet = rename(acOutFile, acRawFile);
   }

   LogMsg("Total output records:       %u", lCnt);
   LogMsg("Total records updated:      %u", iRollUpd);

   lRecCnt = lCnt;
   return lRet;
}

/********************************* Sol_MergeRoll *****************************
 *
 * Return 0 if successful, >0 if error
 *
 *****************************************************************************/

int Sol_MergeRoll(char *pOutbuf, char *pRollRec, int iLen, int iFlag)
{
   SOL_ROLL *pRec;
   char     *pTmp, acTmp[256], acTmp1[256];
   long     lTmp; ;
   int      iRet;

   // Replace tab char with 0
   pTmp = pRollRec;

   pRec = (SOL_ROLL *)pRollRec;

   if (iFlag & CREATE_R01)
   {
      // Clear output buffer
      memset(pOutbuf, ' ', iRecLen);

      // Start copying data
      memcpy(pOutbuf, pRec->Apn, SIZ_ROLL_APN);
      memcpy(pOutbuf+OFF_CO_NUM, "48SOL", 5);

      // Format APN
      iRet = formatApn(pOutbuf, acTmp, &myCounty);
      memcpy(pOutbuf+OFF_APN_D, acTmp, iRet);

      // Create MapLink
      iRet = formatMapLink(pOutbuf, acTmp, &myCounty);
      memcpy(pOutbuf+OFF_MAPLINK, acTmp, iRet);

      // Create index map link
      if (getIndexPage(acTmp, acTmp1, &myCounty))
         memcpy(pOutbuf+OFF_IMAPLINK, acTmp1, iRet);

      // Year assessed
      memcpy(pOutbuf+OFF_YR_ASSD, myCounty.acYearAssd, 4);

      // HO Exempt
      if (pRec->HO_Type[0] == 'H')
         *(pOutbuf+OFF_HO_FL) = '1';      // 'Y'
      else
         *(pOutbuf+OFF_HO_FL) = '2';      // 'N'

      // Exemp total
      lTmp  = atoin(pRec->HO_Exe, SIZ_ROLL_VALUE);
      lTmp += atoin(pRec->Wel_Exe, SIZ_ROLL_VALUE);
      if (lTmp > 0)
      {
         sprintf(acTmp, "%*u", SIZ_EXE_TOTAL, lTmp);
         memcpy(pOutbuf+OFF_EXE_TOTAL, acTmp, SIZ_EXE_TOTAL);
      }

      // Land
      long lLand = atoin(pRec->Land, SIZ_ROLL_VALUE);
      if (lLand > 0)
      {
         sprintf(acTmp, "%*u", SIZ_LAND, lLand);
         memcpy(pOutbuf+OFF_LAND, acTmp, SIZ_LAND);
      }

      // Improve
      long lImpr = atoin(pRec->Impr, SIZ_ROLL_VALUE);
      if (lImpr > 0)
      {
         sprintf(acTmp, "%*u", SIZ_IMPR, lImpr);
         memcpy(pOutbuf+OFF_IMPR, acTmp, SIZ_IMPR);
      }
      // Other value
      long lOthers = atoin(pRec->TreeVine, SIZ_ROLL_VALUE);
      long lFixture = atoin(pRec->Fixt, SIZ_ROLL_VALUE);
      long lPers = atoin(pRec->Pers, SIZ_ROLL_VALUE);
      lTmp = lPers + lOthers + lFixture;
      long lGross = lTmp+lLand+lImpr;
      if (lTmp > 0)
      {
         sprintf(acTmp, "%*u", SIZ_OTHER, lTmp);
         memcpy(pOutbuf+OFF_OTHER, acTmp, SIZ_OTHER);
      }

      // Gross total
      if (lGross > 0)
      {
         sprintf(acTmp, "%*u", SIZ_GROSS, lGross);
         memcpy(pOutbuf+OFF_GROSS, acTmp, SIZ_GROSS);
      }

      // Ratio
      if (lImpr > 0)
      {
         sprintf(acTmp, "%*u", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
         memcpy(pOutbuf+OFF_RATIO, acTmp, SIZ_RATIO);
      }
   }

   // Mailing - Situs
   Sol_MergeAdr(pOutbuf, pRollRec);

   // UseCode
   if (pRec->UseCode[0] > ' ')
   {
      memcpy(pOutbuf+OFF_USE_CO, pRec->UseCode, SIZ_ROLL_USECODE);
      updateStdUse(pOutbuf+OFF_USE_STD, pRec->UseCode, SIZ_ROLL_USECODE, pOutbuf);
   } else
      memcpy(pOutbuf+OFF_USE_STD, USE_UNASGN, SIZ_USE_STD);

   // TRA
   memcpy(pOutbuf+OFF_TRA, pRec->TRA, SIZ_ROLL_TRA);

   // Owner
   Sol_MergeOwner(pOutbuf, pRec->Owners);

   // Legal
   Sol_MergeLegal(pOutbuf, pRollRec);

   // Lot sqft
   lTmp = atoin(pRec->LotSize, SIZ_ROLL_LOTSIZE);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*u", SIZ_LOT_SQFT, lTmp);
      memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);
   }

   // Acres
   long lAcres = 10*atoin(pRec->Acres, SIZ_ROLL_ACRES);
   if (!lAcres && lTmp)
      lAcres = (lTmp * 1000)/SQFT_PER_ACRE;

   if (lAcres > 0)
   {
      sprintf(acTmp, "%*u", SIZ_LOT_ACRES, lAcres);
      memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);
   }

   // Units
   lTmp = atoin(pRec->Units, SIZ_ROLL_NUMUNITS);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%d       ", lTmp);
      memcpy(pOutbuf+OFF_UNITS, acTmp, SIZ_UNITS);
   }

   return 0;
}

int Sol_MergeLien(char *pOutbuf, char *pRollRec, int iLen, int iFlag)
{
   SOL_LIEN *pRec;
   char     *pTmp, acTmp[256], acTmp1[256];
   long     lTmp, lGross;
   int      iRet, iTmp;

   // Replace tab char with 0
   pTmp = pRollRec;

   pRec = (SOL_LIEN *)pRollRec;

   // Clear output buffer
   memset(pOutbuf, ' ', iRecLen);

   // Start copying data
   memcpy(pOutbuf, pRec->Apn, SIZ_ROLL_APN);
   memcpy(pOutbuf+OFF_CO_NUM, "48SOL", 5);

   // Format APN
   iRet = formatApn(pOutbuf, acTmp, &myCounty);
   memcpy(pOutbuf+OFF_APN_D, acTmp, iRet);

   // Create MapLink
   iRet = formatMapLink(pOutbuf, acTmp, &myCounty);
   memcpy(pOutbuf+OFF_MAPLINK, acTmp, iRet);

   // Create index map link
   if (getIndexPage(acTmp, acTmp1, &myCounty))
      memcpy(pOutbuf+OFF_IMAPLINK, acTmp1, iRet);

   // Year assessed
   memcpy(pOutbuf+OFF_YR_ASSD, myCounty.acYearAssd, 4);

   // HO Exempt
   if (pRec->HO_ExeType[0] == 'H')
      *(pOutbuf+OFF_HO_FL) = '1';      // 'Y'
   else
      *(pOutbuf+OFF_HO_FL) = '2';      // 'N'

   // Exemp total
   long lHO_Exe  = atoin(pRec->HO_Exe, SIZ_ROLL_VALUE);
   long lWel_Exe = atoin(pRec->Wel_Exe, SIZ_ROLL_VALUE);
   long lMil_Exe = atoin(pRec->Mil_Exe, SIZ_ROLL_VALUE);

   // HO Exempt
   if (pRec->HO_ExeType[0] == 'H')
      *(pOutbuf+OFF_HO_FL) = '1';      // 'Y'
   else
      *(pOutbuf+OFF_HO_FL) = '2';      // 'N'

   lTmp = lHO_Exe+lWel_Exe+lMil_Exe;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*u", SIZ_EXE_TOTAL, lTmp);
      memcpy(pOutbuf+OFF_EXE_TOTAL, acTmp, SIZ_EXE_TOTAL);
      iTmp = OFF_EXE_CD1;
      if (lHO_Exe > 0)
      {
         if (memcmp(pRec->HO_ExeType, "NA", 2))
            memcpy(pOutbuf+OFF_EXE_CD1, pRec->HO_ExeType, 2);
         else
            memcpy(pOutbuf+OFF_EXE_CD1, "HO", 2);
         iTmp = OFF_EXE_CD2;
      }
      if (lWel_Exe > 0)
      {
         if (memcmp(pRec->HO_ExeType, "NA", 2))
            memcpy(pOutbuf+OFF_EXE_CD1, pRec->HO_ExeType, 2);
         else
            memcpy(pOutbuf+iTmp, "WE", 2);
         iTmp = OFF_EXE_CD3;
      }
      if (lMil_Exe > 0)
      {
         if (memcmp(pRec->HO_ExeType, "NA", 2))
            memcpy(pOutbuf+OFF_EXE_CD1, pRec->HO_ExeType, 2);
         else
            memcpy(pOutbuf+iTmp, "SS", 2);
      }
   }

   // Land
   long lLand = atoin(pRec->Land, SIZ_ROLL_VALUE);
   if (lLand > 0)
   {
      sprintf(acTmp, "%*u", SIZ_LAND, lLand);
      memcpy(pOutbuf+OFF_LAND, acTmp, SIZ_LAND);
   }

   // Improve
   long lImpr = atoin(pRec->Impr, SIZ_ROLL_VALUE);
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*u", SIZ_IMPR, lImpr);
      memcpy(pOutbuf+OFF_IMPR, acTmp, SIZ_IMPR);
   }

   // Other value
   long lTree  = atoin(pRec->TreeVine, SIZ_ROLL_VALUE);
   long lFixtr = atoin(pRec->Fixt, SIZ_ROLL_VALUE);
   long lPers  = atoin(pRec->Pers, SIZ_ROLL_VALUE);
   long lMinr  = atoin(pRec->MnrRight, SIZ_ROLL_VALUE);
   lTmp = lPers + lTree + lFixtr + lMinr;
   lGross = lLand+lImpr+lTmp;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*u", SIZ_OTHER, lTmp);
      memcpy(pOutbuf+OFF_OTHER, acTmp, SIZ_OTHER);

      if (lTree > 0)
      {
         sprintf(acTmp, "%d         ", lTree);
         memcpy(pOutbuf+OFF_TREEVINES, acTmp, SIZ_TREEVINES);
      }
      if (lFixtr > 0)
      {
         sprintf(acTmp, "%d         ", lFixtr);
         memcpy(pOutbuf+OFF_FIXTR, acTmp, SIZ_FIXTR);
      }
      if (lPers > 0)
      {
         sprintf(acTmp, "%d         ", lPers);
         memcpy(pOutbuf+OFF_PERSPROP, acTmp, SIZ_PERSPROP);
      }
      if (lMinr > 0)
      {
         sprintf(acTmp, "%d         ", lMinr);
         memcpy(pOutbuf+OFF_MINERAL, acTmp, SIZ_MINERAL);
      }

      lTmp  = atoin(pRec->Pen, SIZ_ROLL_VALUE);
      if (lTmp > 0)
      {
         sprintf(acTmp, "%d         ", lTmp);
         memcpy(pOutbuf+OFF_LATE_PENALTY, acTmp, SIZ_LATE_PENALTY);
      }

   }

   // Gross total
   if (lGross > 0)
   {
      sprintf(acTmp, "%*u", SIZ_GROSS, lGross);
      memcpy(pOutbuf+OFF_GROSS, acTmp, SIZ_GROSS);
   }
   // Ratio
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*u", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
      memcpy(pOutbuf+OFF_RATIO, acTmp, SIZ_RATIO);
   }

   // UseCode
   if (pRec->UseCode[0] > ' ')
   {
      memcpy(pOutbuf+OFF_USE_CO, pRec->UseCode, SIZ_ROLL_USECODE);
      updateStdUse(pOutbuf+OFF_USE_STD, pRec->UseCode, SIZ_ROLL_USECODE, pOutbuf);
   } else
      memcpy(pOutbuf+OFF_USE_STD, USE_UNASGN, SIZ_USE_STD);

   // TRA
   memcpy(pOutbuf+OFF_TRA, pRec->TRA, SIZ_ROLL_TRA);

   // Lot sqft
   lTmp = atoin(pRec->LotSize, SIZ_ROLL_LOTSIZE);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*u", SIZ_LOT_SQFT, lTmp);
      memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);
   }

   // Acres v99
   long lAcres = 10*atoin(pRec->Acres, SIZ_ROLL_ACRES);
   if (!lAcres && lTmp > 0)
      lAcres = (lTmp * 1000)/SQFT_PER_ACRE;

   if (lAcres > 0)
   {
      sprintf(acTmp, "%*u", SIZ_LOT_ACRES, lAcres);
      memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);
   } 
   if (!lTmp && lAcres > 0)
   {
      lTmp = (long)(lAcres * SQFT_FACTOR_1000);
      sprintf(acTmp, "%*u", SIZ_LOT_SQFT, lTmp);
      memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);
   }

   // Units
   lTmp = atoin(pRec->Units, SIZ_ROLL_NUMUNITS);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%d       ", lTmp);
      memcpy(pOutbuf+OFF_UNITS, acTmp, SIZ_UNITS);
   }

   // Owner
   Sol_MergeOwner(pOutbuf, pRec->Owners);

   // Legal
   Sol_MergeLegal(pOutbuf, pRollRec);

   // Mailing - Situs
   Sol_Ldr_MergeAdr(pOutbuf, pRollRec);

   return 0;
}

/******************************** Sol_MergeSAdr ******************************
 *
 * Merge Situs address from AssessmentRoll.csv
 * Return 0 if successful, otherwise error.
 *
 *****************************************************************************/

int Sol_MergeSAdr(char *pOutbuf, char *pStrNum, char *pStrName, char *pCityCode, char *pUnit)
{
   char     acTmp[256], acAddr1[256];
   long     lTmp;
   int      iTmp;

   // Remove old situs
   removeSitus(pOutbuf);

   // Merge data
   acAddr1[0] = 0;
   lTmp = atol(pStrNum);
   if (lTmp > 0)
   {
      memcpy(pOutbuf+OFF_S_HSENO, pStrNum, strlen(pStrNum));
      iTmp = sprintf(acTmp, "%d ", lTmp);
      memcpy(pOutbuf+OFF_S_STRNUM, acTmp, iTmp);
      strcpy(acAddr1, acTmp);
   }

   _strupr(pStrName);
   strcat(acAddr1, pStrName);

   ADR_REC  sSitusAdr;
   parseAdr1S(&sSitusAdr, pStrName);

   if (sSitusAdr.strName[0] > ' ')
      memcpy(pOutbuf+OFF_S_STREET, sSitusAdr.strName, strlen(sSitusAdr.strName));
   if (sSitusAdr.strDir[0] > ' ')
      memcpy(pOutbuf+OFF_S_DIR, sSitusAdr.strDir, strlen(sSitusAdr.strDir));
   if (sSitusAdr.strSfx[0] > ' ')
      memcpy(pOutbuf+OFF_S_SUFF, sSitusAdr.strSfx, strlen(sSitusAdr.strSfx));
   if (sSitusAdr.Unit[0] > ' ')
   {
      vmemcpy(pOutbuf+OFF_S_UNITNO, sSitusAdr.Unit, SIZ_S_UNITNO);
      vmemcpy(pOutbuf+OFF_S_UNITNOX, sSitusAdr.UnitNox, SIZ_S_UNITNOX);
   } else if (*pUnit > ' ')
   {
      vmemcpy(pOutbuf+OFF_S_UNITNO, pUnit, SIZ_S_UNITNO);
      vmemcpy(pOutbuf+OFF_S_UNITNOX, pUnit, SIZ_S_UNITNOX);
   }

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "616141018", 9))
   //   acTmp[0] = 0;
#endif

   iTmp = blankRem(acAddr1);
   memcpy(pOutbuf+OFF_S_ADDR_D, acAddr1, iTmp);
   memcpy(pOutbuf+OFF_S_ST, "CA", 2);

   // Situs city
   if (*pCityCode > ' ')
   {
      Abbr2Code(pCityCode, acTmp, acAddr1);
      vmemcpy(pOutbuf+OFF_S_CITY, acTmp, SIZ_S_CITY);
      memcpy(pOutbuf+OFF_S_ST, "CA", 2);
      iTmp = sprintf(acTmp, "%s CA", myTrim(acAddr1));
      memcpy(pOutbuf+OFF_S_CTY_ST_D, acTmp, iTmp);
   }

   return 0;
}

/******************************** Sol_MergeMAdr ******************************
 *
 * This version parse mail address from "SecuredAssessorRoll.txt"
 * Return 0 if successful, >0 if error
 *
 *****************************************************************************/

void Sol_MergeMAdr(char *pOutbuf)
{
   char        acTmp[256], acAddr1[64], *pAddr1;
   bool        bUnitAvail = false;
   ADR_REC     sMailAdr;

   // Clear old Mailing
   removeMailing(pOutbuf, true, true);

   if (*apTokens[SOL_R_M_ATTN] > ' ')
      updateCareOf(pOutbuf, apTokens[SOL_R_M_ATTN], 0);
   if (*apTokens[SOL_R_DBANAME] > ' ')
      vmemcpy(pOutbuf+OFF_DBA, apTokens[SOL_R_DBANAME], SIZ_DBA);

   // Check for blank address
   pAddr1 = apTokens[SOL_R_M_ADDR1];
   if (*pAddr1 > ' ' && memcmp(pAddr1, "UNKNOWN", 7))
   {
      strcpy(acAddr1, pAddr1);
      memset((void *)&sMailAdr, 0, sizeof(ADR_REC));

#ifdef _DEBUG
      //if (!memcmp(pOutbuf, "692460076", 9) )
      //   iTmp = 0;
#endif

      vmemcpy(pOutbuf+OFF_M_ADDR_D, apTokens[SOL_R_M_ADDR1], SIZ_M_ADDR_D);
      vmemcpy(pOutbuf+OFF_M_CTY_ST_D, apTokens[SOL_R_M_ADDR2], SIZ_M_CTY_ST_D);

      // Parsing mail address
      parseAdr1_1(&sMailAdr, apTokens[SOL_R_M_ADDR1]);
      parseAdr2(&sMailAdr, apTokens[SOL_R_M_ADDR2]);

      if (sMailAdr.lStrNum > 0)
      {
         sprintf(acTmp, "%d      ", sMailAdr.lStrNum);
         memcpy(pOutbuf+OFF_M_STRNUM, acTmp, SIZ_M_STRNUM);
         if (sMailAdr.strSub[0] > '0')
         {
            sprintf(acTmp, "%s  ", sMailAdr.strSub);
            memcpy(pOutbuf+OFF_M_STR_SUB, acTmp, SIZ_M_STR_SUB);
         }
      }
      vmemcpy(pOutbuf+OFF_M_DIR, sMailAdr.strDir, SIZ_M_DIR);
      vmemcpy(pOutbuf+OFF_M_STREET, sMailAdr.strName, SIZ_M_STREET);
      vmemcpy(pOutbuf+OFF_M_SUFF, sMailAdr.strSfx, SIZ_M_SUFF);
      vmemcpy(pOutbuf+OFF_M_UNITNO, sMailAdr.Unit, SIZ_M_UNITNO);
      vmemcpy(pOutbuf+OFF_M_UNITNOX, sMailAdr.UnitNox, SIZ_M_UNITNOX);
      vmemcpy(pOutbuf+OFF_M_CITY, sMailAdr.City, SIZ_M_CITY);
      vmemcpy(pOutbuf+OFF_M_ST, sMailAdr.State, SIZ_M_ST);
      vmemcpy(pOutbuf+OFF_M_ZIP, sMailAdr.Zip, SIZ_M_ZIP);
   }
}

/******************************* Sol_MergeMAdr2 ******************************
 *
 * This version parse mail address from "SecuredOfficialRoll.txt"
 * Return 0 if successful, >0 if error
 *
 *****************************************************************************/

void Sol_MergeMAdr2(char *pOutbuf)
{
   char        acTmp[256], acAddr1[64], *pAddr1;
   bool        bUnitAvail = false;
   ADR_REC     sMailAdr;

   // Clear old Mailing
   removeMailing(pOutbuf, true, true);

   if (*apTokens[SOL_L_M_ATTN] > ' ')
      updateCareOf(pOutbuf, apTokens[SOL_L_M_ATTN], 0);

   // Check for blank address
   pAddr1 = apTokens[SOL_L_M_ADDR1];
   if (*pAddr1 > ' ' && memcmp(pAddr1, "UNKNOWN", 7))
   {
      strcpy(acAddr1, pAddr1);
      memset((void *)&sMailAdr, 0, sizeof(ADR_REC));

#ifdef _DEBUG
      //if (!memcmp(pOutbuf, "692460076", 9) )
      //   iTmp = 0;
#endif

      vmemcpy(pOutbuf+OFF_M_ADDR_D, apTokens[SOL_L_M_ADDR1], SIZ_M_ADDR_D);
      vmemcpy(pOutbuf+OFF_M_CTY_ST_D, apTokens[SOL_L_M_ADDR2], SIZ_M_CTY_ST_D);

      // Parsing mail address
      parseAdr1_1(&sMailAdr, apTokens[SOL_L_M_ADDR1]);
      parseAdr2(&sMailAdr, apTokens[SOL_L_M_ADDR2]);

      if (sMailAdr.lStrNum > 0)
      {
         sprintf(acTmp, "%d      ", sMailAdr.lStrNum);
         memcpy(pOutbuf+OFF_M_STRNUM, acTmp, SIZ_M_STRNUM);
         if (sMailAdr.strSub[0] > '0')
         {
            sprintf(acTmp, "%s  ", sMailAdr.strSub);
            memcpy(pOutbuf+OFF_M_STR_SUB, acTmp, SIZ_M_STR_SUB);
         }
      }
      vmemcpy(pOutbuf+OFF_M_DIR, sMailAdr.strDir, SIZ_M_DIR);
      vmemcpy(pOutbuf+OFF_M_STREET, sMailAdr.strName, SIZ_M_STREET);
      vmemcpy(pOutbuf+OFF_M_SUFF, sMailAdr.strSfx, SIZ_M_SUFF);
      vmemcpy(pOutbuf+OFF_M_UNITNO, sMailAdr.Unit, SIZ_M_UNITNO);
      vmemcpy(pOutbuf+OFF_M_CITY, sMailAdr.City, SIZ_M_CITY);
      vmemcpy(pOutbuf+OFF_M_ST, sMailAdr.State, SIZ_M_ST);
      vmemcpy(pOutbuf+OFF_M_ZIP, sMailAdr.Zip, SIZ_M_ZIP);
   }
}

/******************************* Sol_MergeMAdr2 ******************************
 *
 * This version parse mail address from "SecuredOfficialRoll.txt"
 * Return 0 if successful, >0 if error
 *
 *****************************************************************************/

void Sol_MergeMAdr3(char *pOutbuf)
{
   char        acTmp[256], acAddr1[64], *pAddr1;
   bool        bUnitAvail = false;
   ADR_REC     sMailAdr;

   // Clear old Mailing
   removeMailing(pOutbuf, true, true);

   if (!memcmp(apTokens[SOL_L_M_ADDR2], "C/O", 3) || !memcmp(apTokens[SOL_L_M_ADDR2], "C/O", 3))
      updateCareOf(pOutbuf, apTokens[SOL_L_M_ADDR2], 0);

   // Check for blank address
   pAddr1 = apTokens[SOL_L_M_ADDR1];
   if (*pAddr1 > ' ' && memcmp(pAddr1, "UNKNOWN", 7))
   {
      strcpy(acAddr1, pAddr1);
      memset((void *)&sMailAdr, 0, sizeof(ADR_REC));

#ifdef _DEBUG
      //if (!memcmp(pOutbuf, "692460076", 9) )
      //   iTmp = 0;
#endif

      vmemcpy(pOutbuf+OFF_M_ADDR_D, apTokens[SOL_L_M_ADDR1], SIZ_M_ADDR_D);
      vmemcpy(pOutbuf+OFF_M_CTY_ST_D, apTokens[SOL_L_M_CITYST], SIZ_M_CTY_ST_D);

      // Parsing mail address
      parseAdr1_1(&sMailAdr, apTokens[SOL_L_M_ADDR1]);
      parseAdr2(&sMailAdr, apTokens[SOL_L_M_CITYST]);

      if (sMailAdr.lStrNum > 0)
      {
         sprintf(acTmp, "%d      ", sMailAdr.lStrNum);
         memcpy(pOutbuf+OFF_M_STRNUM, acTmp, SIZ_M_STRNUM);
         if (sMailAdr.strSub[0] > '0')
         {
            sprintf(acTmp, "%s  ", sMailAdr.strSub);
            memcpy(pOutbuf+OFF_M_STR_SUB, acTmp, SIZ_M_STR_SUB);
         }
      }
      vmemcpy(pOutbuf+OFF_M_DIR, sMailAdr.strDir, SIZ_M_DIR);
      vmemcpy(pOutbuf+OFF_M_STREET, sMailAdr.strName, SIZ_M_STREET);
      vmemcpy(pOutbuf+OFF_M_SUFF, sMailAdr.strSfx, SIZ_M_SUFF);
      vmemcpy(pOutbuf+OFF_M_UNITNO, sMailAdr.Unit, SIZ_M_UNITNO);
      vmemcpy(pOutbuf+OFF_M_CITY, sMailAdr.City, SIZ_M_CITY);
      vmemcpy(pOutbuf+OFF_M_ST, sMailAdr.State, SIZ_M_ST);
      vmemcpy(pOutbuf+OFF_M_ZIP, sMailAdr.Zip, SIZ_M_ZIP);
   }
}

/****************************** Sol_MergeRollCsv *****************************
 *
 * Return 0 if successful, < 0 if error
 *        1 retired record, not use
 *
 *****************************************************************************/

int Sol_MergeRollCsv(char *pOutbuf, char *pRollRec, int iLen, int iFlag)
{
   char     acTmp[256], acTmp1[256];
   LONGLONG lTmp;
   double   dTmp;
   int      iRet=0, iTmp;

   // Parse roll record
   iRet = ParseStringNQ(pRollRec, cDelim, MAX_FLD_TOKEN, apTokens);
   if (iRet < SOL_R_FLDS)
   {
      LogMsg("***** Error: bad input record for APN=%s", apTokens[SOL_R_APN]);
      return -1;
   }
#ifdef _DEBUG
   //if (!memcmp(apTokens[R_APN], "692460080", 9) )
   //   iTmp = 0;
#endif

   if (iFlag & CREATE_R01)
   {
      // Clear output buffer
      memset(pOutbuf, ' ', iRecLen);

      // Start copying data
      memcpy(pOutbuf, apTokens[SOL_R_APN], strlen(apTokens[SOL_R_APN]));
      *(pOutbuf+OFF_STATUS) = 'A';

      // Format APN
      iRet = formatApn(apTokens[SOL_R_APN], acTmp, &myCounty);
      memcpy(pOutbuf+OFF_APN_D, acTmp, iRet);

      // Create MapLink and output new record
      iRet = formatMapLink(apTokens[SOL_R_APN], acTmp, &myCounty);
      memcpy(pOutbuf+OFF_MAPLINK, acTmp, iRet);

      // Create index map link
      if (getIndexPage(acTmp, acTmp1, &myCounty))
         memcpy(pOutbuf+OFF_IMAPLINK, acTmp1, iRet);

      // County code
      memcpy(pOutbuf+OFF_CO_NUM, "48SOL", 5);

      // Year assessed
      memcpy(pOutbuf+OFF_YR_ASSD, myCounty.acYearAssd, 4);

      // Land
      long lLand = atoi(apTokens[SOL_R_LAND_VAL]);
      if (lLand > 0)
      {
         sprintf(acTmp, "%*u", SIZ_LAND, lLand);
         memcpy(pOutbuf+OFF_LAND, acTmp, SIZ_LAND);
      }

      // Improve
      long lImpr = atoi(apTokens[SOL_R_IMPR_VAL]);
      if (lImpr > 0)
      {
         sprintf(acTmp, "%*u", SIZ_IMPR, lImpr);
         memcpy(pOutbuf+OFF_IMPR, acTmp, SIZ_IMPR);
      }

      // Other values
      long lFixt  = atoi(apTokens[SOL_R_FIX_VAL]);
      long lPP    = atoi(apTokens[SOL_R_PP_VAL]);
      long lTree  = atoi(apTokens[SOL_R_TREE_VAL]);
      lTmp = lFixt+lPP+lTree;
      if (lTmp > 0)
      {
         sprintf(acTmp, "%*u", SIZ_OTHER, lTmp);
         memcpy(pOutbuf+OFF_OTHER, acTmp, SIZ_OTHER);

         if (lFixt > 0)
         {
            sprintf(acTmp, "%d         ", lFixt);
            memcpy(pOutbuf+OFF_FIXTR, acTmp, SIZ_FIXTR);
         }
         if (lPP > 0)
         {
            sprintf(acTmp, "%d         ", lPP);
            memcpy(pOutbuf+OFF_PERSPROP, acTmp, SIZ_PERSPROP);
         }
         if (lTree > 0)
         {
            sprintf(acTmp, "%d         ", lTree);
            memcpy(pOutbuf+OFF_TREEVINES, acTmp, SIZ_TREEVINES);
         }
      }

      // Gross total
      lTmp += (lLand+lImpr);
      if (lTmp > 0)
      {
         sprintf(acTmp, "%*u", SIZ_GROSS, lTmp);
         memcpy(pOutbuf+OFF_GROSS, acTmp, SIZ_GROSS);
      }

      // Ratio
      if (lImpr > 0)
      {
         sprintf(acTmp, "%*u", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
         memcpy(pOutbuf+OFF_RATIO, acTmp, SIZ_RATIO);
      }
   }

   // TRA
   iTmp = atol(apTokens[SOL_R_TAXAREACODE]);
   sprintf(acTmp, "%.6u", iTmp);
   memcpy(pOutbuf+OFF_TRA, acTmp, 6);

   // UseCode
   if (*apTokens[SOL_R_USECODE] > ' ')
   {
      vmemcpy(pOutbuf+OFF_USE_CO, apTokens[SOL_R_USECODE], SIZ_ROLL_USECODE);
      updateStdUse(pOutbuf+OFF_USE_STD, apTokens[SOL_R_USECODE], SIZ_ROLL_USECODE, pOutbuf);
   } else
      memcpy(pOutbuf+OFF_USE_STD, USE_UNASGN, SIZ_USE_STD);

   // Ag preserved
   if (*apTokens[SOL_R_AG_STATUS] == 'A')
      *(pOutbuf+OFF_AG_PRE) = 'Y';

   // Goverment owned
   if (!memcmp(apTokens[SOL_R_GOV_OWNED], "SB", 2))
      *(pOutbuf+OFF_FULL_EXEMPT) = 'Y';

   // Acres
   dTmp = atof(apTokens[SOL_R_ACRES]);
   if (dTmp > 0.0)
   {
      // Format Acres
      lTmp = (long)(dTmp * ACRES_FACTOR);
      sprintf(acTmp, "%*u", SIZ_LOT_ACRES, lTmp);
      memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);
   }
   // Lot Sqft
   lTmp = atol(apTokens[SOL_R_LOTSQFT]);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*u", SIZ_LOT_SQFT, lTmp);
      memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);
   }

   // Number of Units
   lTmp = atol(apTokens[SOL_R_NUM_UNITS]);
   if (lTmp > 0)
   {
      iRet = sprintf(acTmp, "%d", lTmp);
      memcpy(pOutbuf+OFF_UNITS, acTmp, iRet);
   }

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "0128080070", 10) )
   //   iTmp = 0;
#endif

   // Owner
   Sol_MergeOwner(pOutbuf, apTokens[SOL_R_ASSESSEE]);

   // Situs
   Sol_MergeSAdr(pOutbuf, apTokens[SOL_R_S_STRNUM], apTokens[SOL_R_S_STRNAME], apTokens[SOL_R_S_CITYCODE], apTokens[SOL_R_S_UNIT]);

   // Mailing
   Sol_MergeMAdr(pOutbuf);

   // Legal
   Sol_MergeLegalCsv(pOutbuf);

   // Block Lot
   if (IsCharAlphaNumeric(*apTokens[SOL_R_MAPBLOCK]))
      vmemcpy(pOutbuf+OFF_BLOCK, apTokens[SOL_R_MAPBLOCK], SIZ_BLOCK);
   if (IsCharAlphaNumeric(*apTokens[SOL_R_MAPLOT]))
      vmemcpy(pOutbuf+OFF_LOT, apTokens[SOL_R_MAPLOT], SIZ_LOT);

   return 0;
}

/****************************** Sol_Load_RollCsv ******************************
 *
 * Load updated roll file AssessmentRoll.csv (10/06/2019)
 *
 ******************************************************************************/

int Sol_Load_RollCsv(int iSkip)
{
   char     acRec[MAX_RECSIZE], acBuf[MAX_RECSIZE], acRollRec[MAX_RECSIZE], acLastApn[32];
   char     acRawFile[_MAX_PATH], acOutFile[_MAX_PATH], acSrtFile[_MAX_PATH], *pTmp;
   int      iRet, iTmp, iRollUpd=0, iNewRec=0, iRetiredRec=0, lCnt=0;
   DWORD    nBytesRead, nBytesWritten;
   BOOL     bRet, bEof;
   HANDLE   fhIn, fhOut;

   LogMsg0("Loading roll update");

   sprintf(acRawFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "S01");
   sprintf(acOutFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "R01");

   // Rename files for processing
   if (_access(acRawFile, 0))
   {
      if (!_access(acOutFile, 0))
         rename(acOutFile, acRawFile);
      else
      {
         LogMsg("***** Missing input file %s.  Please recheck!", acRawFile);
         return -1;
      }
   }

   // Sort input
   sprintf(acSrtFile, "%s\\SOL\\Sol_Roll.srt", acTmpPath);
   sprintf(acRec, "S(#1,C,A) DEL(%d) F(TXT)", cDelim);
   iTmp = sortFile(acRollFile, acSrtFile, acRec);

   // Open roll file
   LogMsg("Open Roll file %s", acSrtFile);
   fdRoll = fopen(acSrtFile, "r");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", acSrtFile);
      return 2;
   }

   LogMsg("Open Char file %s", acCChrFile);
   fdChar = fopen(acCChrFile, "r");
   if (fdChar == NULL)
   {
      LogMsg("***** Error opening Char file: %s\n", acCChrFile);
      return -2;
   }

   // Open Input file
   LogMsg("Open input file %s", acRawFile);
   fhIn = CreateFile(acRawFile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhIn == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening input file: %s\n", acRawFile);
      return -3;
   }

   // Open Output file
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening output file: %s\n", acOutFile);
      return 4;
   }

   // Get 1st rec
   pTmp = fgets((char *)&acRollRec[0], MAX_RECSIZE, fdRoll);
   if (*pTmp > '9')
      pTmp = fgets((char *)&acRollRec[0], MAX_RECSIZE, fdRoll);
   bEof = (pTmp ? false:true);

   // Init variables
   iNoMatch=iBadCity=iBadSuffix=0;
   strcpy(acLastApn, "000000000");

   // Copy skip record
   memset(acBuf, ' ', iRecLen);
   while (iSkip-- > 0)
   {
      ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);
      // Ignore record start with 99999999
      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
   }

   // Merge loop
   while (!bEof)
   {
      bRet = ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);

      // Check for EOF
      if (!bRet)
      {
         LogMsg("***** Error reading input file %s (%f)", acRawFile, GetLastError());
         break;
      }

      // EOF ?
      if (!nBytesRead)
         break;

NextRollRec:
#ifdef _DEBUG
      //if (!memcmp(acBuf, "0072231060", 10))
      //   iTmp = 0;
#endif

      iTmp = memcmp(acBuf, acRollRec, iApnLen);
      if (!iTmp)
      {
         // Create R01 record from roll data
         iRet = Sol_MergeRollCsv(acBuf, acRollRec, MAX_RECSIZE, UPDATE_R01);
         if (!iRet)
         {
            // Merge Char
            if (fdChar)
               iTmp = Sol_MergeStdChar(acBuf);

            iRollUpd++;
         }

         // Read next roll record
         pTmp = fgets(acRollRec, MAX_RECSIZE, fdRoll);

         if (iRet > 0)
            goto NextRollRec;
         if (!pTmp)
            bEof = true;         // Signal to stop
      } else if (iTmp > 0)       // Roll not match, new roll record?
      {
         if (bDebug)
            LogMsg0("New roll record : %.*s (%d)", iApnLen, acRollRec, lCnt);

         // Create new R01 record
         iRet = Sol_MergeRollCsv(acRec, acRollRec, MAX_RECSIZE, CREATE_R01);
         if (!iRet)
         {
            // Merge Char
            if (fdChar)
               iTmp = Sol_MergeStdChar(acRec);

            if (memcmp(acLastApn, acRec, 14))
            {
               memcpy(acLastApn, acRec, 14);
               bRet = WriteFile(fhOut, acRec, iRecLen, &nBytesWritten, NULL);
               iNewRec++;
            } else
            {
               LogMsg("---> Drop duplicate APN: %.14s (LandVal=%s)", acLastApn, apTokens[SOL_R_LAND_VAL]);
               //LogMsg("---> Drop duplicate APN: %.14s (IMPR_PARCEL=%s)", acLastApn, apTokens[SOL_R_IMPR_PARCEL]);
               bDupApn = true;
            }
         }
         lCnt++;

         // Get next roll record
         pTmp = fgets(acRollRec, MAX_RECSIZE, fdRoll);
         if (!pTmp)
            bEof = true;    // Signal to stop
         else
            goto NextRollRec;
      } else
      {
         // Record may be retired
         if (bDebug)
            LogMsg0("Retired record? : R01->%.*s < Roll->%.*s (%d)", iApnLen, acBuf, iApnLen, (char *)&acRollRec[1], lCnt);
         iRetiredRec++;

         continue;
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
      if (!iRet)
      {
         if (memcmp(acLastApn, acBuf, 14))
         {
            memcpy(acLastApn, acBuf, 14);
            bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
            if (!bRet)
            {
               LogMsg("Error occurs: %d\n", GetLastError());
               break;
            }
         }
      }
   }

   // Do the rest of the file
   while (!bEof && isdigit(acRollRec[0]))
   {
      // Create new R01 record
      iRet = Sol_MergeRollCsv(acRec, acRollRec, MAX_RECSIZE, CREATE_R01);

      if (!iRet)
      {
         if (bDebug)
            LogMsg0("New roll record : %.*s (%d) ", iApnLen, acRollRec, lCnt);

         // Merge Char
         if (fdChar)
            iRet = Sol_MergeStdChar(acRec);

         if (memcmp(acLastApn, acRec, 14))
         {
            memcpy(acLastApn, acRec, 14);
            bRet = WriteFile(fhOut, acRec, iRecLen, &nBytesWritten, NULL);
            iNewRec++;
         } else
         {
            LogMsg("---> Duplicate APN: %.14s (END)", acLastApn);
            bDupApn = true;
         }
      }
      lCnt++;

      // Get next roll record
      pTmp = fgets(acRollRec, MAX_RECSIZE, fdRoll);

      if (!pTmp)
         bEof = true;    // Signal to stop
   }

   // Close files
   if (fdRoll)
      fclose(fdRoll);
   if (fdChar)
      fclose(fdChar);
   if (fhOut)
      CloseHandle(fhOut);

   LogMsg("Total output records:       %u", lCnt);
   LogMsg("Total retired records:      %u", iRetiredRec);
   LogMsg("Total bad-city records:     %u", iBadCity);
   LogMsg("Total bad-suffix records:   %u\n", iBadSuffix);

   LogMsg("Number of Char matched:     %u", lCharMatch);
   LogMsg("Number of Char skiped:      %u\n", lCharSkip);

   printf("\nTotal output records: %u", lCnt);

   lRecCnt = lCnt;
   return 0;
}

/********************************* Sol_Load_Roll ******************************
 *
 * We only update roll info.  No need to update characteristics here.
 * 08/12/2020 Call Sol_MergeStdChar()
 *
 ******************************************************************************/

int Sol_Load_Roll(int iSkip)
{
   char     *pTmp, acRec[MAX_RECSIZE], acBuf[MAX_RECSIZE], acRollRec[MAX_RECSIZE];
   char     acRawFile[_MAX_PATH], acOutFile[_MAX_PATH];

   HANDLE   fhIn, fhOut;

   int      iRet, iTmp, iRollUpd=0, iNewRec=0, iRetiredRec=0;
   DWORD    nBytesRead;
   DWORD    nBytesWritten;
   BOOL     bRet, bEof;
   long     lRet=0, lCnt=0;

   iApnLen = myCounty.iApnLen;
   sprintf(acRawFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "S01");
   sprintf(acOutFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "R01");

   // Rename files for processing
   if (_access(acRawFile, 0))
   {
      if (!_access(acOutFile, 0))
         rename(acOutFile, acRawFile);
      else
      {
         LogMsg("***** Missing input file %s.  Please recheck!", acRawFile);
         return 1;
      }
   }

   // Open roll file
   LogMsg("Open Roll file %s", acRollFile);
   fdRoll = fopen(acRollFile, "r");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", acRollFile);
      return 2;
   }

   // Open combine CHAR file
   LogMsg("Open Char file %s", acCChrFile);
   fdChar = fopen(acCChrFile, "r");
   if (fdChar == NULL)
   {
      LogMsg("***** Error opening Char file: %s\n", acCChrFile);
      return 2;
   }

   // Open Input file
   LogMsg("Open input file %s", acRawFile);
   fhIn = CreateFile(acRawFile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhIn == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening output file: %s\n", acRawFile);
      return 3;
   }

   // Open Output file
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening output file: %s\n", acOutFile);
      return 4;
   }

   // Get first RollRec
   pTmp = fgets((char *)&acRollRec[0], iRollLen, fdRoll);
   bEof = (pTmp ? false:true);

   // Copy skip record
   while (iSkip-- > 0)
   {
      ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);
      WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
   }
   iNoMatch=iBadCity=iBadSuffix=0;

   // Merge loop
   while (!bEof)
   {
      bRet = ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);

      // Check for EOF
      if (!bRet)
      {
         LogMsg("***** Error reading input file %s (%f)", acRawFile, GetLastError());
         break;
      }

      // EOF ?
      if (!nBytesRead)
         break;

      NextRollRec:
      iTmp = memcmp(acBuf, acRollRec, iApnLen);
      if (!iTmp)
      {
         // Merge roll data
         iRet = Sol_MergeRoll(acBuf, acRollRec, iRollLen, UPDATE_R01);

         // Merge char
         if (fdChar)
            iRet = Sol_MergeStdChar(acBuf);

         iRollUpd++;

         // Read next roll record
         pTmp = fgets(acRollRec, MAX_RECSIZE, fdRoll);

         if (!pTmp)
            bEof = true;         // Signal to stop
      } else if (iTmp > 0)       
      {
         // Roll not match, new roll record?
         if (bDebug)
            LogMsg("***** New roll record : %.*s (%d) *****", iApnLen, acRollRec, lCnt);

         // Create new R01 record
         iRet = Sol_MergeRoll(acRec, acRollRec, iRollLen, CREATE_R01);
         iNewRec++;

         // Merge char
         if (fdChar)
            iRet = Sol_MergeStdChar(acRec);

         bRet = WriteFile(fhOut, acRec, iRecLen, &nBytesWritten, NULL);
         lCnt++;

         // Get next roll record
         pTmp = fgets(acRollRec, MAX_RECSIZE, fdRoll);

         if (!pTmp)
            bEof = true;    // Signal to stop
         else
            goto NextRollRec;
      } else
      {
         // Record may be retired
         if (bDebug)
            LogMsg("***** Roll not match (retired record?) : R01->%.*s < Roll->%.*s (%d) *****", iApnLen, acBuf, iApnLen, acRollRec, lCnt);
         iRetiredRec++;
         continue;
      }

      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);

      if (!bRet)
      {
         LogMsg("***** Error occurs: %d\n", GetLastError());
         lRet = WRITE_ERR;
         break;
      }
   }

   // Do the rest of the file
   while (!bEof)
   {
      LogMsg0("*** New roll record : %.*s (%d) ***", iApnLen, acRollRec, lCnt);

      // Create new R01 record
      Sol_MergeRoll(acRec, acRollRec, iRollLen, CREATE_R01);
      iNewRec++;

      // Merge char
      if (fdChar)
         iRet = Sol_MergeStdChar(acRec);

      bRet = WriteFile(fhOut, acRec, iRecLen, &nBytesWritten, NULL);
      lCnt++;

      // Get next roll record
      pTmp = fgets(acRollRec, MAX_RECSIZE, fdRoll);

      if (!pTmp)
         bEof = true;    // Signal to stop
   }

   // Close files
   if (fdRoll)
      fclose(fdRoll);
   if (fdChar)
      fclose(fdChar);
   if (fhOut)
      CloseHandle(fhOut);
   if (fhIn)
      CloseHandle(fhIn);

   LogMsg("Total output records:       %u", lCnt);
   LogMsg("Total new records:          %u", iNewRec);
   LogMsg("Total Char matched:         %u", lCharMatch);
   LogMsg("Total Char skipped:         %u\n", lCharSkip);
   LogMsg("Total retired records:      %u", iRetiredRec);
   LogMsg("Total bad-city records:     %u", iBadCity);
   LogMsg("Total bad-suffix records:   %u", iBadSuffix);

   printf("\nTotal output records: %u", lCnt);

   lRecCnt = lCnt;
   return 0;
}

/****************************** Sol_MergeLienCsv *****************************
 *
 * Return 0 if successful, < 0 if error
 *        1 retired record, not use
 *
 *****************************************************************************/

int Sol_MergeLienCsv(char *pOutbuf, char *pRollRec, int iLen, int iFlag)
{
   char     acTmp[256], acTmp1[256];
   ULONG    lTmp;
   double   dTmp;
   int      iRet=0, iTmp;

   // Parse roll record
   iRet = ParseStringIQ(pRollRec, cLdrSep, MAX_FLD_TOKEN, apTokens);
   if (iRet < SOL_L_FLDS)
   {
      LogMsg("***** Error: bad input record for APN=%s", apTokens[SOL_L_APN]);
      return -1;
   }

   // Clear output buffer
   memset(pOutbuf, ' ', iRecLen);

   // Start copying data
   memcpy(pOutbuf, apTokens[SOL_L_APN], strlen(apTokens[SOL_L_APN]));
   *(pOutbuf+OFF_STATUS) = 'A';

   // Format APN
   iRet = formatApn(apTokens[SOL_L_APN], acTmp, &myCounty);
   memcpy(pOutbuf+OFF_APN_D, acTmp, iRet);

   // Create MapLink and output new record
   iRet = formatMapLink(apTokens[SOL_L_APN], acTmp, &myCounty);
   memcpy(pOutbuf+OFF_MAPLINK, acTmp, iRet);

   // Create index map link
   if (getIndexPage(acTmp, acTmp1, &myCounty))
      memcpy(pOutbuf+OFF_IMAPLINK, acTmp1, iRet);

   // County code
   memcpy(pOutbuf+OFF_CO_NUM, "48SOL", 5);

   // Year assessed
   memcpy(pOutbuf+OFF_YR_ASSD, myCounty.acYearAssd, 4);

   // Land
   long lLand = atol(apTokens[SOL_L_LAND_VAL]);
   if (lLand > 0)
   {
      sprintf(acTmp, "%*u", SIZ_LAND, lLand);
      memcpy(pOutbuf+OFF_LAND, acTmp, SIZ_LAND);
   }

   // Improve
   long lImpr = atol(apTokens[SOL_L_IMPR_VAL]);
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*u", SIZ_IMPR, lImpr);
      memcpy(pOutbuf+OFF_IMPR, acTmp, SIZ_IMPR);
   }

   // Other values
   long lFixt  = atol(apTokens[SOL_L_FIX_VAL]);
   long lPP    = atol(apTokens[SOL_L_PP_VAL]);
   long lTree  = atol(apTokens[SOL_L_TREE_VAL]);
   long lMinr  = atol(apTokens[SOL_L_MINRIGHT_VAL]);
   lTmp = lFixt+lPP+lTree+lMinr;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*u", SIZ_OTHER, lTmp);
      memcpy(pOutbuf+OFF_OTHER, acTmp, SIZ_OTHER);

      if (lFixt > 0)
      {
         sprintf(acTmp, "%d         ", lFixt);
         memcpy(pOutbuf+OFF_FIXTR, acTmp, SIZ_FIXTR);
      }
      if (lPP > 0)
      {
         sprintf(acTmp, "%d         ", lPP);
         memcpy(pOutbuf+OFF_PERSPROP, acTmp, SIZ_PERSPROP);
      }
      if (lTree > 0)
      {
         sprintf(acTmp, "%d         ", lTree);
         memcpy(pOutbuf+OFF_TREEVINES, acTmp, SIZ_TREEVINES);
      }
      if (lMinr > 0)
      {
         sprintf(acTmp, "%d         ", lMinr);
         memcpy(pOutbuf+OFF_MINERAL, acTmp, SIZ_MINERAL);
      }
   }

   // Gross total
   lTmp += (lLand+lImpr);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*u", SIZ_GROSS, lTmp);
      memcpy(pOutbuf+OFF_GROSS, acTmp, SIZ_GROSS);
   }

   // Ratio
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*u", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
      memcpy(pOutbuf+OFF_RATIO, acTmp, SIZ_RATIO);
   }

   // HO_Exe
   if (*apTokens[SOL_L_EXE_TYPE] == 'H')
      *(pOutbuf+OFF_HO_FL) = '1';
   else
      *(pOutbuf+OFF_HO_FL) = '2';
   vmemcpy(pOutbuf+OFF_EXE_CD1, apTokens[SOL_L_EXE_TYPE], SIZ_EXE_CD1);

#ifdef _DEBUG
   //if (!memcmp(apTokens[SOL_L_APN], "0032273100", 9) )
   //   iTmp = 0;
#endif

   // Exemp total
   long lHO_Exe  = atol(apTokens[SOL_L_HOEXE]);
   long lWel_Exe = atol(apTokens[SOL_L_WFEXE]);
   long lVet_Exe = atol(apTokens[SOL_L_VETEXE]);
   lTmp = lHO_Exe+lWel_Exe+lVet_Exe;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*u", SIZ_EXE_TOTAL, lTmp);
      memcpy(pOutbuf+OFF_EXE_TOTAL, acTmp, SIZ_EXE_TOTAL);
      iTmp = OFF_EXE_CD1;
      if (!memcmp(apTokens[SOL_L_EXE_TYPE], "NA", 2) && lWel_Exe > 0)
         vmemcpy(pOutbuf+iTmp, "WE  ", SIZ_EXE_CD1);

      // Create exemption type
      makeExeType(pOutbuf+OFF_EXE_TYPE, pOutbuf+OFF_EXE_CD1, pOutbuf+OFF_EXE_CD2, pOutbuf+OFF_EXE_CD3, (IDX_TBL4 *)&SOL_Exemption);
   }

   // TRA
   iTmp = atol(apTokens[SOL_L_TAXAREACODE]);
   sprintf(acTmp, "%.6u", iTmp);
   memcpy(pOutbuf+OFF_TRA, acTmp, 6);

   // UseCode
   if (*apTokens[SOL_L_USECODE] > ' ')
   {
      vmemcpy(pOutbuf+OFF_USE_CO, apTokens[SOL_L_USECODE], SIZ_ROLL_USECODE);
      updateStdUse(pOutbuf+OFF_USE_STD, apTokens[SOL_L_USECODE], SIZ_ROLL_USECODE, pOutbuf);
   } else
      memcpy(pOutbuf+OFF_USE_STD, USE_UNASGN, SIZ_USE_STD);

   // Ag preserved
   //if (*apTokens[SOL_L_AG_STATUS] == 'A')
   //   *(pOutbuf+OFF_AG_PRE) = 'Y';

   // Goverment owned
   //if (!memcmp(apTokens[SOL_L_GOV_OWNED], "SB", 2))
   //   *(pOutbuf+OFF_FULL_EXEMPT) = 'Y';

   // Acres
   dTmp = atof(apTokens[SOL_L_ACRES]);
   if (dTmp > 0.0)
   {
      // Format Acres
      lTmp = (ULONG)(dTmp * ACRES_FACTOR);
      sprintf(acTmp, "%*u", SIZ_LOT_ACRES, lTmp);
      memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);
   }
   // Lot Sqft
   lTmp = atol(apTokens[SOL_L_LOTSQFT]);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*u", SIZ_LOT_SQFT, lTmp);
      memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);
   }

   // Number of Units
   lTmp = atol(apTokens[SOL_L_NUM_UNITS]);
   if (lTmp > 0)
   {
      iRet = sprintf(acTmp, "%d", lTmp);
      memcpy(pOutbuf+OFF_UNITS, acTmp, iRet);
   }

   // Owner
   try {
      Sol_MergeOwner(pOutbuf, apTokens[SOL_L_ASSESSEE]);
   } catch(...) {
      LogMsg("***** Exeception occured in Sol_MergeOwner()");
   }

   // Situs
   Sol_MergeSAdr(pOutbuf, apTokens[SOL_L_S_STRNUM], apTokens[SOL_L_S_STRNAME], apTokens[SOL_L_S_CITYCODE], apTokens[SOL_L_S_UNIT]);

   // Mailing
   // 2023 Sol_MergeMAdr2(pOutbuf);
   // 2024
   Sol_MergeMAdr3(pOutbuf);

#ifdef _DEBUG
   //if (!memcmp(apTokens[SOL_L_APN], "0128040440", 10) )
   //   iTmp = 0;
#endif

   // Legal
   Sol_MergeLegalCsv2(pOutbuf);

   // Block Lot
   if (IsCharAlphaNumeric(*apTokens[SOL_L_MAPBLOCK]))
      vmemcpy(pOutbuf+OFF_BLOCK, apTokens[SOL_L_MAPBLOCK], SIZ_BLOCK);
   if (IsCharAlphaNumeric(*apTokens[SOL_L_MAPLOT]))
      vmemcpy(pOutbuf+OFF_LOT, apTokens[SOL_L_MAPLOT], SIZ_LOT);

   return 0;
}

/********************************* Sol_Load_LDR *****************************
 *
 *
 ****************************************************************************/

int Sol_Load_LDR(int iFirstRec /* 1=create header rec */)
{
   char     *pTmp, acBuf[MAX_RECSIZE], acRollRec[MAX_RECSIZE];
   char     acOutFile[_MAX_PATH], acLastApn[32];
   char     *pCnty = myCounty.acCntyCode;

   HANDLE   fhOut;
   int      iRet;
   int      iCharUpd=0, iSfrUpd=0;
   DWORD    nBytesWritten;
   BOOL     bRet, bEof;
   long     lRet=0;

   iApnLen = myCounty.iApnLen;
   sprintf(acOutFile, acRawTmpl, pCnty, pCnty, "R01");

   // Open roll file
   LogMsg("Open Roll file %s", acRollFile);
   fdRoll = fopen(acRollFile, "r");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", acRollFile);
      return 2;
   }

   // Open combine CHAR file
   LogMsg("Open Char file %s", acCChrFile);
   fdChar = fopen(acCChrFile, "r");
   if (fdChar == NULL)
   {
      LogMsg("***** Error opening Char file: %s\n", acCChrFile);
      return 2;
   }

   // Open Output file
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening output file: %s\n", acOutFile);
      return 3;
   }

   // Output first header record
   if (iFirstRec > 0)
   {
      memset(acBuf, '9', iRecLen);
      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
   }

   // Get first RollRec
   pTmp = fgets((char *)&acRollRec[0], MAX_RECSIZE, fdRoll);
   if (*pTmp > '9')
      pTmp = fgets((char *)&acRollRec[0], MAX_RECSIZE, fdRoll);

   bEof = (pTmp ? false:true);

   // Init variables
   iNoMatch=iBadCity=iBadSuffix=0;
   acLastApn[0] = 0;

   // Merge loop
   while (pTmp)
   {
      // Create new R01 record
      //iRet = Sol_MergeLien(acBuf, acRollRec, iRollLen, CREATE_R01);
      // 2023
      iRet = Sol_MergeLienCsv(acBuf, acRollRec, iRollLen, CREATE_R01);

      if (memcmp(acLastApn, acBuf, 14))
      {
         memcpy(acLastApn, acBuf, 14);
         if (fdChar)
         {
            // Merge char
            iRet = Sol_MergeStdChar(acBuf);
         }

         bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
         if (!(++lLDRRecCount % 1000))
            printf("\r%u", lLDRRecCount);

         if (!bRet)
         {
            LogMsg("Error writing to output file at record %d\n", lLDRRecCount);
            lRet = WRITE_ERR;
            break;
         }
      } else
      {
         LogMsg("---> Drop duplicate APN: %.14s (LandVal=%s)", acLastApn, apTokens[SOL_R_LAND_VAL]);
      }

      // Get next roll record
      pTmp = fgets(acRollRec, MAX_RECSIZE, fdRoll);
   }

   // Close files
   if (fdRoll)
      fclose(fdRoll);
   if (fdChar)
      fclose(fdChar);

   if (fhOut)
      CloseHandle(fhOut);

   LogMsg("Total input records:        %u", lLDRRecCount);
   LogMsg("Total output records:       %u", lLDRRecCount);
   LogMsg("Total bad-city records:     %u", iBadCity);
   LogMsg("Total bad-suffix records:   %u", iBadSuffix);
   LogMsg("Total Char matched:         %u", lCharMatch);
   LogMsg("Total Char skipped:         %u", lCharSkip);
   printf("\nTotal output records: %u", lLDRRecCount);

   lRecCnt = lLDRRecCount;
   return 0;
}

/****************************** Sol_CreateLienRec ****************************
 *
 * Return 0 if successful, >0 if error
 *
 *****************************************************************************/

int Sol_CreateLienRec(char *pOutbuf, char *pRollRec)
{
   char     acTmp[256];
   LONGLONG lTmp;
   int      iTmp, iExeIdx=0;
   LIENEXTR *pLienRec = (LIENEXTR *)pOutbuf;
   SOL_LIEN *pRec     = (SOL_LIEN *)pRollRec;

   // Clear output buffer
   memset(pOutbuf, ' ', sizeof(LIENEXTR));

   // Start copying data
   memcpy(pLienRec->acApn, pRec->Apn, SIZ_ROLL_APN);

   // TRA
   lTmp = atoin(pRec->TRA, SIZ_ROLL_TRA);
   if (lTmp > 0)
   {
      iTmp = sprintf(acTmp, "%.6d", lTmp);
      memcpy(pLienRec->acTRA, acTmp, iTmp);
   }

   // Year assessed
   memcpy(pLienRec->acYear, myCounty.acYearAssd, 4);

   // HO Exempt
   long lHO_Exe  = atoin(pRec->HO_Exe, SIZ_ROLL_VALUE);
   long lWel_Exe = atoin(pRec->Wel_Exe, SIZ_ROLL_VALUE);
   long lMil_Exe = atoin(pRec->Mil_Exe, SIZ_ROLL_VALUE);
   lTmp = lHO_Exe+lWel_Exe+lMil_Exe;

   // HO Exempt
   if (pRec->HO_ExeType[0] == 'H')
      pLienRec->acHO[0] = '1';      // 'Y'
   else
      pLienRec->acHO[0] = '2';      // 'N'

   if (lTmp > 0)
   {
      iTmp = sprintf(acTmp, "%*u", sizeof(pLienRec->acExAmt), lTmp);
      memcpy(pLienRec->acExAmt, acTmp, iTmp);

      if (lHO_Exe > 0)
      {
         iTmp = sprintf(acTmp, "%*u", sizeof(pLienRec->extra.Sol.HO_Exe), lHO_Exe);
         memcpy(pLienRec->extra.Sol.HO_Exe, acTmp, iTmp);
         if (memcmp(pRec->HO_ExeType, "NA", 2))
            memcpy(pLienRec->extra.Sol.Exe_Codes[iExeIdx], pRec->HO_ExeType, 2);
         else
            memcpy(pLienRec->extra.Sol.Exe_Codes[iExeIdx], "HO", 2);
         iExeIdx++;
      }
      if (lWel_Exe > 0)
      {
         iTmp = sprintf(acTmp, "%*u", sizeof(pLienRec->extra.Sol.HO_Exe), lWel_Exe);
         memcpy(pLienRec->extra.Sol.Wel_Exe, acTmp, iTmp);
         if (memcmp(pRec->HO_ExeType, "NA", 2))
            memcpy(pLienRec->extra.Sol.Exe_Codes[iExeIdx], pRec->HO_ExeType, 2);
         else
            memcpy(pLienRec->extra.Sol.Exe_Codes[iExeIdx], "WE", 2);
         iExeIdx++;
      }
      if (lMil_Exe > 0)
      {
         iTmp = sprintf(acTmp, "%*u", sizeof(pLienRec->extra.Sol.HO_Exe), lMil_Exe);
         memcpy(pLienRec->extra.Sol.Mil_Exe, acTmp, iTmp);
         if (memcmp(pRec->HO_ExeType, "NA", 2))
            memcpy(pLienRec->extra.Sol.Exe_Codes[iExeIdx], pRec->HO_ExeType, 2);
         else
            memcpy(pLienRec->extra.Sol.Exe_Codes[iExeIdx], "SS", 2);
         iExeIdx++;
      }
   }

   // Land
   long lLand = atoin(pRec->Land, SIZ_ROLL_VALUE);
   if (lLand > 0)
   {
      sprintf(acTmp, "%*u", SIZ_LIEN_LAND, lLand);
      memcpy(pLienRec->acLand, acTmp, SIZ_LIEN_LAND);
   }

   // Improve
   long lImpr = atoin(pRec->Impr, SIZ_ROLL_VALUE);
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*u", SIZ_LIEN_IMPR, lImpr);
      memcpy(pLienRec->acImpr, acTmp, SIZ_LIEN_IMPR);
   }

   // Other value
   long lFixtr = atoin(pRec->Fixt, SIZ_ROLL_VALUE);
   long lPers  = atoin(pRec->Pers, SIZ_ROLL_VALUE);
   long lTree  = atoin(pRec->TreeVine, SIZ_ROLL_VALUE);
   long lMinr  = atoin(pRec->MnrRight, SIZ_ROLL_VALUE);
   lTmp = lPers + lTree + lFixtr + lMinr;
   LONGLONG lGross = lTmp + lLand + lImpr;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*u", SIZ_LIEN_FIXT, lTmp);
      memcpy(pLienRec->acOther, acTmp, SIZ_LIEN_FIXT);

      if (lFixtr > 0)
      {
         sprintf(acTmp, "%*u", SIZ_LIEN_FIXT, lFixtr);
         memcpy(pLienRec->acME_Val, acTmp, SIZ_LIEN_FIXT);
      }
      if (lPers > 0)
      {
         sprintf(acTmp, "%*u", SIZ_LIEN_FIXT, lPers);
         memcpy(pLienRec->acPP_Val, acTmp, SIZ_LIEN_FIXT);
      }
      if (lTree > 0)
      {
         sprintf(acTmp, "%*u", SIZ_LIEN_FIXT, lTree);
         memcpy(pLienRec->extra.Sol.Tree, acTmp, SIZ_LIEN_FIXT);
      }
      if (lMinr > 0)
      {
         sprintf(acTmp, "%*u", SIZ_LIEN_FIXT, lMinr);
         memcpy(pLienRec->extra.Sol.Mineral, acTmp, SIZ_LIEN_FIXT);
      }
   }

   // Gross
   if (lGross > 0)
   {
      sprintf(acTmp, "%*u", SIZ_LIEN_GROSS, lGross);
      memcpy(pLienRec->acGross, acTmp, SIZ_LIEN_GROSS);
   }

   // Impr - Ratio
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*u", SIZ_LIEN_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
      memcpy(pLienRec->acRatio, acTmp, SIZ_LIEN_RATIO);
   }

   pLienRec->LF[0] = '\n';
   pLienRec->LF[1] = 0;

   return 0;
}

int Sol_CreateLienRecCsv(char *pOutbuf, char *pRollRec)
{
   char     acTmp[256];
   LONGLONG lTmp;
   int      iTmp, iExeIdx=0;
   LIENEXTR *pLienRec = (LIENEXTR *)pOutbuf;

   // Parse roll record
   iTokens = ParseStringIQ(pRollRec, cLdrSep, MAX_FLD_TOKEN, apTokens);
   if (iTokens < SOL_L_FLDS)
   {
      LogMsg("***** Error: bad input record for APN=%s", apTokens[SOL_L_APN]);
      return -1;
   }

#ifdef _DEBUG
   //if (!memcmp(apTokens[SOL_L_APN], "0033052300", 9) )
   //   iTmp = 0;
#endif

   // Clear output buffer
   memset(pOutbuf, ' ', sizeof(LIENEXTR));

   // Start copying data
   memcpy(pLienRec->acApn, apTokens[SOL_L_APN], strlen(apTokens[SOL_L_APN]));

   // TRA
   lTmp = atol(apTokens[SOL_L_TAXAREACODE]);
   if (lTmp > 0)
   {
      iTmp = sprintf(acTmp, "%.6d", lTmp);
      memcpy(pLienRec->acTRA, acTmp, iTmp);
   }

   // Year assessed
   memcpy(pLienRec->acYear, myCounty.acYearAssd, 4);

   // HO Exempt
   long lHO_Exe  = atol(apTokens[SOL_L_HOEXE]);
   long lWel_Exe = atol(apTokens[SOL_L_WFEXE]);
   long lMil_Exe = atol(apTokens[SOL_L_VETEXE]);
   lTmp = lHO_Exe+lWel_Exe+lMil_Exe;

   // HO Exempt
   if (*apTokens[SOL_L_EXE_TYPE] == 'H')
      pLienRec->acHO[0] = '1';      // 'Y'
   else
      pLienRec->acHO[0] = '2';      // 'N'

   if (lTmp > 0)
   {
      iTmp = sprintf(acTmp, "%*u", sizeof(pLienRec->acExAmt), lTmp);
      memcpy(pLienRec->acExAmt, acTmp, iTmp);
      if (memcmp(apTokens[SOL_L_EXE_TYPE], "NA", 2))
         vmemcpy(pLienRec->extra.Sol.Exe_Codes[iExeIdx++], apTokens[SOL_L_EXE_TYPE], 2);

      if (lHO_Exe > 0)
      {
         iTmp = sprintf(acTmp, "%*u", sizeof(pLienRec->extra.Sol.HO_Exe), lHO_Exe);
         memcpy(pLienRec->extra.Sol.HO_Exe, acTmp, iTmp);
      }
      if (lWel_Exe > 0)
      {
         iTmp = sprintf(acTmp, "%*u", sizeof(pLienRec->extra.Sol.HO_Exe), lWel_Exe);
         memcpy(pLienRec->extra.Sol.Wel_Exe, acTmp, iTmp);
      }
      if (lMil_Exe > 0)
      {
         iTmp = sprintf(acTmp, "%*u", sizeof(pLienRec->extra.Sol.HO_Exe), lMil_Exe);
         memcpy(pLienRec->extra.Sol.Mil_Exe, acTmp, iTmp);
      }
   }

   // Land
   long lLand = atol(apTokens[SOL_L_LAND_VAL]);
   if (lLand > 0)
   {
      sprintf(acTmp, "%*u", SIZ_LIEN_LAND, lLand);
      memcpy(pLienRec->acLand, acTmp, SIZ_LIEN_LAND);
   }

   // Improve
   long lImpr = atol(apTokens[SOL_L_IMPR_VAL]);
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*u", SIZ_LIEN_IMPR, lImpr);
      memcpy(pLienRec->acImpr, acTmp, SIZ_LIEN_IMPR);
   }

   // Other value
   long lFixtr = atol(apTokens[SOL_L_FIX_VAL]);
   long lPers  = atol(apTokens[SOL_L_PP_VAL]);
   long lTree  = atol(apTokens[SOL_L_TREE_VAL]);
   long lMinr  = atol(apTokens[SOL_L_MINRIGHT_VAL]);
   long lPen   = atol(apTokens[SOL_L_PEN_VAL]);
   lTmp = lPers + lTree + lFixtr + lMinr;
   LONGLONG lGross = lTmp + lLand + lImpr;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*u", SIZ_LIEN_FIXT, lTmp);
      memcpy(pLienRec->acOther, acTmp, SIZ_LIEN_FIXT);

      if (lFixtr > 0)
      {
         sprintf(acTmp, "%*u", SIZ_LIEN_FIXT, lFixtr);
         memcpy(pLienRec->acME_Val, acTmp, SIZ_LIEN_FIXT);
      }
      if (lPers > 0)
      {
         sprintf(acTmp, "%*u", SIZ_LIEN_FIXT, lPers);
         memcpy(pLienRec->acPP_Val, acTmp, SIZ_LIEN_FIXT);
      }
      if (lTree > 0)
      {
         sprintf(acTmp, "%*u", SIZ_LIEN_FIXT, lTree);
         memcpy(pLienRec->extra.Sol.Tree, acTmp, SIZ_LIEN_FIXT);
      }
      if (lMinr > 0)
      {
         sprintf(acTmp, "%*u", SIZ_LIEN_FIXT, lMinr);
         memcpy(pLienRec->extra.Sol.Mineral, acTmp, SIZ_LIEN_FIXT);
      }
   }

   if (lPen > 0)
   {
      sprintf(acTmp, "%*u", SIZ_LIEN_LATEPEN, lMinr);
      memcpy(pLienRec->acLatePen, acTmp, SIZ_LIEN_LATEPEN);
   }

   // Gross
   if (lGross > 0)
   {
      sprintf(acTmp, "%*u", SIZ_LIEN_GROSS, lGross);
      memcpy(pLienRec->acGross, acTmp, SIZ_LIEN_GROSS);
   }

   // Impr - Ratio
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*u", SIZ_LIEN_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
      memcpy(pLienRec->acRatio, acTmp, SIZ_LIEN_RATIO);
   }

   pLienRec->LF[0] = '\n';
   pLienRec->LF[1] = 0;

   return 0;
}

/********************************* Sol_ExtrLien *****************************
 *
 *
 ****************************************************************************/

int Sol_ExtrLien()
{
   char     *pTmp, acBuf[MAX_RECSIZE], acRollRec[MAX_RECSIZE];
   char     acOutFile[_MAX_PATH];
   FILE     *fdLien;
   int      iRet;
   long     lCnt=0;

   LogMsg0("Extract lien value");

   // Open roll file
   LogMsg("Open Roll file %s", acRollFile);
   fdRoll = fopen(acRollFile, "r");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", acRollFile);
      return -1;
   }

   // Open Output file
   sprintf(acOutFile, acLienTmpl, myCounty.acCntyCode, myCounty.acCntyCode);
   LogMsg("Open lien extract file %s", acOutFile);
   fdLien = fopen(acOutFile, "w");
   if (fdLien == NULL)
   {
      LogMsg("***** Error creating lien extract file: %s\n", acOutFile);
      return -2;
   }

   // Get first rec
   pTmp = fgets((char *)&acRollRec[0], MAX_RECSIZE, fdRoll);
   if (*pTmp > '9')
      pTmp = fgets((char *)&acRollRec[0], MAX_RECSIZE, fdRoll);

   // Merge loop
   while (pTmp)
   {
      // Create new R01 record
      iRet = Sol_CreateLienRecCsv(acBuf, acRollRec);

      // Write to output
      fputs(acBuf, fdLien);
      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);

      // Get roll rec
      pTmp = fgets(acRollRec, MAX_RECSIZE, fdRoll);
   }

   // Close files
   if (fdRoll)
      fclose(fdRoll);
   if (fdLien)
      fclose(fdLien);

   LogMsg("Total output records:       %u", lCnt);
   printf("\nTotal output records: %u", lCnt);

   lRecCnt = lCnt;
   return 0;
}

/********************************* MergeDocRec *******************************
 *
 * 
 *****************************************************************************/

int Sol_MergeDocRec(char *pGrGrRec, char *pOutbuf)
{
   long  lCurSaleDt, lLstSaleDt;
   int   iTmp, iDocIdx;
   char  *pTmp, acTmp[128], acTmp1[128];

   GRGR_DOC *pSaleRec = (GRGR_DOC *)pGrGrRec;

   lCurSaleDt = atoin(pSaleRec->DocDate, SIZ_SALE1_DT);
   lLstSaleDt = atoin(pOutbuf+OFF_SALE1_DT, SIZ_SALE1_DT);
   if (lCurSaleDt < lLstSaleDt)
      return -1;

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "0030091200", 10))
   //   iTmp = 0;
#endif

   // Only take documents with predefined DocType
   pSaleRec->DocTitle[SIZ_GD_TITLE-1] = 0;

   iDocIdx = findDocType(pSaleRec->DocTitle, (IDX_TBL4 *)&SOL_DocType[0]);
   if (iDocIdx < 0)
   {
      if (!memcmp(pSaleRec->DocTitle, "AFFIDAVIT DEATH", 12))
      {
         // Update transfers
         memcpy(pOutbuf+OFF_TRANSFER_DOC, pSaleRec->DocNum, SIZ_TRANSFER_DOC);
         memcpy(pOutbuf+OFF_TRANSFER_DT, pSaleRec->DocDate, SIZ_TRANSFER_DT);
      }
      return 0;
   }

   // Update Owner and Mailing if xfer date > Lien date for DEED or GD only
   if ((iDocIdx < 3) &&
      (pSaleRec->Grantee[0][0] > ' ') && 
      (lCurSaleDt > lLienDate) &&
      memcmp(pSaleRec->Grantee[0], pOutbuf+OFF_NAME1, 12) )
   {
      memset(pOutbuf+OFF_NAME1, ' ', (SIZ_NAME1+SIZ_NAME2));
      memset(pOutbuf+OFF_NAME_SWAP, ' ', SIZ_NAME_SWAP);

      pSaleRec->Grantee[1][0] = 0;
      if (pTmp = strchr(pSaleRec->Grantee[0], '('))
         *pTmp = '\0';
      memcpy(pOutbuf+OFF_NAME1, pSaleRec->Grantee[0], strlen(pSaleRec->Grantee[0]));
      strcpy(acTmp, pSaleRec->Grantee[0]);

      if (pTmp = strstr(acTmp, " TR  "))
         *pTmp = 0;
      pTmp = swapName(acTmp, acTmp1, 0);
      if (pTmp)
         memcpy(pOutbuf+OFF_NAME_SWAP, acTmp1, strlen(acTmp1));
      else
         memcpy(pOutbuf+OFF_NAME_SWAP, acTmp, strlen(acTmp));
   }


   if (lCurSaleDt == lLstSaleDt)
   {
      if (*(pOutbuf+OFF_SALE1_DOC) == ' ' && pSaleRec->DocNum[0] > ' ')
         memcpy(pOutbuf+OFF_SALE1_DOC, pSaleRec->DocNum, SIZ_SALE1_DOC);
      if (*(pOutbuf+OFF_SALE1_DOCTYPE) == ' ' && iDocIdx < 3)
         memcpy(pOutbuf+OFF_SALE1_DOCTYPE, SOL_DocType[iDocIdx].pCode, SOL_DocType[iDocIdx].iCodeLen);

      if (*pSaleRec->Grantor[0] >= 'A' && memcmp(pOutbuf+OFF_SELLER, pSaleRec->Grantor[0], 10))
      {
         *pSaleRec->Grantor[1] = 0;
         if (pTmp = strchr(pSaleRec->Grantor[0], '('))
            *pTmp = '\0';
         iTmp = strlen(pSaleRec->Grantor[0]);
         if (iTmp > SIZ_SELLER)
            iTmp = SIZ_SELLER;
         memset(pOutbuf+OFF_SELLER, ' ', SIZ_SELLER);
         memcpy(pOutbuf+OFF_SELLER, pSaleRec->Grantor[0], iTmp);

         *(pOutbuf+OFF_AR_CODE1) = 'W';
      }
      return 1;
   } else if (*(pOutbuf+OFF_SALE1_DOC) > ' ')
   {
      // Move sale2 to sale3
      memcpy(pOutbuf+OFF_SALE3_DOC, pOutbuf+OFF_SALE2_DOC, SIZ_SALE3_DOC);
      memcpy(pOutbuf+OFF_SALE3_DT, pOutbuf+OFF_SALE2_DT, SIZ_SALE3_DT);
      memcpy(pOutbuf+OFF_SALE3_DOCTYPE, pOutbuf+OFF_SALE2_DOCTYPE, SIZ_SALE3_DOCTYPE);
      memcpy(pOutbuf+OFF_SALE3_AMT, pOutbuf+OFF_SALE2_AMT, SALE_SIZ_SALEPRICE);
      memcpy(pOutbuf+OFF_SALE3_CODE, pOutbuf+OFF_SALE2_CODE, SALE_SIZ_SALECODE);
      *(pOutbuf+OFF_AR_CODE3) = *(pOutbuf+OFF_AR_CODE2);

      // Move sale1 to sale2
      memcpy(pOutbuf+OFF_SALE2_DOC, pOutbuf+OFF_SALE1_DOC, SIZ_SALE3_DOC);
      memcpy(pOutbuf+OFF_SALE2_DT, pOutbuf+OFF_SALE1_DT, SIZ_SALE3_DT);
      memcpy(pOutbuf+OFF_SALE2_DOCTYPE, pOutbuf+OFF_SALE1_DOCTYPE, SIZ_SALE3_DOCTYPE);
      memcpy(pOutbuf+OFF_SALE2_AMT, pOutbuf+OFF_SALE1_AMT, SALE_SIZ_SALEPRICE);
      memcpy(pOutbuf+OFF_SALE2_CODE, pOutbuf+OFF_SALE1_CODE, SALE_SIZ_SALECODE);
      *(pOutbuf+OFF_AR_CODE2) = *(pOutbuf+OFF_AR_CODE1);
   }

   // Update current sale
   memcpy(pOutbuf+OFF_SALE1_DOC, pSaleRec->DocNum, SIZ_SALE3_DOC);
   memcpy(pOutbuf+OFF_SALE1_DT, pSaleRec->DocDate, SIZ_SALE3_DT);
   memcpy(pOutbuf+OFF_SALE1_DOCTYPE, SOL_DocType[iDocIdx].pCode, SOL_DocType[iDocIdx].iCodeLen);
   memset(pOutbuf+OFF_SALE1_AMT, ' ', SALE_SIZ_SALEPRICE);
   memset(pOutbuf+OFF_SALE1_CODE, ' ', SALE_SIZ_SALECODE);

   *pSaleRec->Grantor[1] = 0;
   if (pTmp = strchr(pSaleRec->Grantor[0], '('))
      *pTmp = '\0';
   iTmp = strlen(pSaleRec->Grantor[0]);
   if (iTmp > SIZ_SELLER)
      iTmp = SIZ_SELLER;
   memset(pOutbuf+OFF_SELLER, ' ', SIZ_SELLER);
   memcpy(pOutbuf+OFF_SELLER, pSaleRec->Grantor[0], iTmp);

   *(pOutbuf+OFF_AR_CODE1) = 'W';

   // Update transfers
   memcpy(pOutbuf+OFF_TRANSFER_DOC, pSaleRec->DocNum, SIZ_TRANSFER_DOC);
   memcpy(pOutbuf+OFF_TRANSFER_DT, pSaleRec->DocDate, SIZ_TRANSFER_DT);

   return 1;
}

/******************************** MergeDocExt ********************************
 *
 * Merge data grab from website to PQ4 product w/o sale price.
 *
 *****************************************************************************/

int Sol_MergeDocExt(char *pCnty, char *pDocFile)
{
   char     *pTmp, acBuf[MAX_RECSIZE];
   char     acRawFile[_MAX_PATH], acOutFile[_MAX_PATH];

   HANDLE   fhIn, fhOut;
   GRGR_DOC SaleRec;
   FILE     *fdDoc;

   int      iTmp, iSaleUpd=0;
   DWORD    nBytesRead;
   DWORD    nBytesWritten;
   BOOL     bRet, bEof, bRename=true;
   long     lRet=0, lCnt=0;

   memset(&SaleRec, ' ', sizeof(GRGR_DOC));

   sprintf(acRawFile, acRawTmpl, pCnty, pCnty, "R01");
   if (_access(acRawFile, 0))
   {
      sprintf(acRawFile, acRawTmpl, pCnty, pCnty, "S01");
      bRename = false;
   }

   sprintf(acOutFile, acRawTmpl, pCnty, pCnty, "N01");
   if (!iGrGrApnLen)
      iGrGrApnLen = myCounty.iApnLen;

   // Open GrGr file
   LogMsg("Merge Recorder data %s to R01 file", pDocFile);
   fdDoc = fopen(pDocFile, "r");
   if (fdDoc == NULL)
   {
      LogMsg("***** Error opening recorder extract file: %s\n", pDocFile);
      return -2;
   }

   // Get first record
   pTmp = fgets((char *)&SaleRec, sizeof(GRGR_DOC), fdDoc);
   if (!pTmp || feof(fdDoc))
   {
      LogMsg("*** No recorder data available");
      return 0;
   }

   // Open Input file
   LogMsg("Open input file %s", acRawFile);
   fhIn = CreateFile(acRawFile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhIn == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening input file to merge GrGr: %s\n", acRawFile);
      return -3;
   }

   // Open Output file
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening output file to merge GrGr: %s\n", acOutFile);
      return -4;
   }

   // Copy skip record
   ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);
   bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);

   iNoMatch=0;
   bEof = false;

   // Merge loop
   while (!bEof)
   {
      bRet = ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);

      // Check for EOF
      if (!bRet)
      {
         LogMsg("***** Error reading input file %s (%f)", acRawFile, GetLastError());
         break;
      }

      if (!nBytesRead)
         break;

      // Update sale
GrGr_ReLoad:
      iTmp = memcmp(acBuf, SaleRec.APN, iGrGrApnLen);
      if (!iTmp)
      {
#ifdef _DEBUG
         //if (!memcmp(acBuf, "014118611", 9))
         //   iTmp = 0;
#endif

         // Merge sale data
         iTmp = Sol_MergeDocRec((char *)&SaleRec, acBuf);
         if (iTmp > 0)
         {
            iSaleUpd++;

            // Get last recording date
            iTmp = atoin((char *)&acBuf[OFF_TRANSFER_DT], SIZ_TRANSFER_DT);
            if (iTmp >= lToday)
               LogMsg0("*** WARNING: Bad last recording date %d at record# %d -->%.10s", iTmp, lCnt, acBuf);
            else if (lLastRecDate < iTmp)
               lLastRecDate = iTmp;
         }
         // Read next sale record
         pTmp = fgets((char *)&SaleRec, sizeof(GRGR_DOC), fdDoc);
         if (!pTmp)
            bEof = false;    // Signal to stop sale update
         else
            goto GrGr_ReLoad;
      } else
      {
         if (iTmp > 0)        // Sale not match, advance to next sale record
         {
            if (!memcmp(acBuf, "99999999", 8))
            {
               nBytesRead = 0;
               continue;
            } else
            {
               iNoMatch++;
               if (bDebug)
                  LogMsg0("*** Sale not match : %.12s > %.12s (%d) ", acBuf, SaleRec.APN, lCnt);
               pTmp = fgets((char *)&SaleRec, sizeof(GRGR_DOC), fdDoc);
               if (!pTmp)
                  bEof = true;    // Signal to stop sale update
               else
                  goto GrGr_ReLoad;
            }
         }
      }

      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);

      if (!bRet)
      {
         LogMsg("***** Error occurs: %d\n", GetLastError());
         lRet = -99;
         break;
      }
   }

   // Do the rest of the file
   while (nBytesRead>0 && ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL))
   {
      if (!nBytesRead)
         break;

      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
      lCnt++;
   }


   // Close files
   if (fdDoc)
      fclose(fdDoc);
   if (fhOut)
      CloseHandle(fhOut);
   if (fhIn)
   {
      CloseHandle(fhIn);

      // Rename outfile
      if (bRename)
      {
         sprintf(acBuf, acRawTmpl, pCnty, pCnty, "M01");
         if (!_access(acBuf, 0))
            remove(acBuf);
         iTmp = rename(acRawFile, acBuf);
      } else
      {
         sprintf(acRawFile, acRawTmpl, pCnty, pCnty, "R01");
      }
      LogMsg("Rename output file %s to %s", acOutFile, acRawFile);
      iTmp = rename(acOutFile, acRawFile);
   }

   // Update output record count
   if (lCnt > 1000)
      lRecCnt = lCnt;

   LogMsg("Total output records:       %u", lCnt);
   LogMsg("Total GrGr records updated: %u", iSaleUpd);
   LogMsg("Total GrGr records skipped: %u", iNoMatch);
   return 0;
}

/****************************** Sol_ConvComChar *****************************
 *
 * Convert commercial char file sd117903.txt to standard char format
 *
 * Return number of output record.  <0 if error.
 *
 ****************************************************************************/

int Sol_ConvComChar(LPCSTR pInfile) 
{
   FILE     *fdIn, *fdOut;
   char     acBuf[4096], acTmpFile[256], acTmp[256], *pRec;
   int      iRet, iTmp, iCnt=0;
   STDCHAR  myCharRec;
   SOL_COMCHAR *pChar = (SOL_COMCHAR *)acBuf;

   LogMsg0("Converting commercial char file %s", pInfile);
   if (!(fdIn = fopen(pInfile, "r")))
      return -1;

   sprintf(acTmpFile, acAttrTmpl, myCounty.acCntyCode, "com");
   if (!(fdOut = fopen(acTmpFile, "w")))
   {
      fclose(fdIn);
      LogMsg("***** Error creating output file %s", acTmpFile);
      return -2;
   }

   // Skip first record - header
   pRec = fgets(acBuf, 4096, fdIn);

   while (!feof(fdIn))
   {
      pRec = fgets(acBuf, 4096, fdIn);
      if (!pRec)
         break;
      if (pChar->Apn[0] == ' ')
         continue;

      memset((void *)&myCharRec, ' ', sizeof(STDCHAR));
      memcpy(myCharRec.Apn, pChar->Apn, SOL_SIZCHAR_APN);
      // Format APN
      iRet = formatApn(pChar->Apn, acTmp, &myCounty);
      memcpy(myCharRec.Apn_D, acTmp, iRet);

      memcpy(myCharRec.Date_Updated, pChar->ChgDate, SOL_SIZCHAR_DATE);

      // YrBlt
      int iYrBlt = atoin(pChar->YrBlt, 4);
      if (iYrBlt > 1600 && iYrBlt <= lToyear)
      {
         iRet = sprintf(acTmp, "%d", iYrBlt);
         memcpy(myCharRec.YrBlt, acTmp, iRet);
      }

      // YrEff
      iTmp = atoin(pChar->EffYr, 4);
      if (iTmp > 1900 && iTmp >= iYrBlt && iTmp <= lToyear)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.YrEff, acTmp, iRet);
      }

#ifdef _DEBUG
      //if (!memcmp(myCharRec.Apn, "003351003000", 9))
      //   iRet = 0;
#endif

      // BldgSize
      int iBldgSize = atoin(pChar->BldgArea, SOL_SIZCHAR_BLDGAREA);
      if (iBldgSize > 100)
      {
         iRet = sprintf(acTmp, "%d", iBldgSize);
         memcpy(myCharRec.BldgSqft, acTmp, iRet);
      }

      // Net rental
      iTmp = atoin(pChar->NetArea, SOL_SIZCHAR_BLDGAREA);
      if (iTmp > 0)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.NetRental, acTmp, iRet);
      }

      myCharRec.CRLF[0] = '\n';
      myCharRec.CRLF[1] = '\0';
      fputs((char *)&myCharRec.Apn[0], fdOut);

      if (!(++iCnt % 1000))
         printf("\r%u", iCnt);
   }

   if (fdIn) fclose(fdIn);
   if (fdOut) fclose(fdOut);

   LogMsg("Number of records processed: %d", iCnt);

   // Sort output on ASMT
   if (iCnt > 100)
   {
      iRet = iCnt;
      // Rename old CHAR file if needed
      //if (!_access(acCChrFile, 0))
      //{
      //   strcpy(acBuf, acCChrFile);
      //   replStr(acBuf, ".dat", ".sav");
      //   if (!_access(acBuf, 0))
      //   {
      //      LogMsg("Delete old %s", acBuf);
      //      DeleteFile(acBuf);
      //   }
      //   LogMsg("Rename %s to %s", acCChrFile, acBuf);
      //   RenameToExt(acCChrFile, "sav");
      //} 

      //// Sort on APN, last change date to keep the latest record on top
      //iRet = sortFile(acTmp, acCChrFile, "S(1,10,C,A,511,8,C,D) F(TXT)");
   } else
   {
      printf("\n");
      iRet = 0;
   }
   return iRet;
}

/**************************** Sol_ConvMultiChar *****************************
 *
 * Convert multi residential char file sd117904.txt to standard char format
 *
 * Return number of output record.  <0 if error.
 *
 ****************************************************************************/

int Sol_ConvMultiChar(LPCSTR pInfile) 
{
   FILE     *fdIn, *fdOut;
   char     acBuf[4096], acTmpFile[256], acTmp[256], acCode[32], *pRec;
   int      iRet, iTmp, iCnt=0;
   STDCHAR  myCharRec;
   SOL_MCHAR *pChar = (SOL_MCHAR *)acBuf;

   LogMsg0("Converting multi res char file %s", pInfile);
   if (!(fdIn = fopen(pInfile, "r")))
      return -1;

   sprintf(acTmpFile, acAttrTmpl, myCounty.acCntyCode, "mul");
   if (!(fdOut = fopen(acTmpFile, "w")))
   {
      fclose(fdIn);
      LogMsg("***** Error creating output file %s", acTmpFile);
      return -2;
   }

   while (!feof(fdIn))
   {
      pRec = fgets(acBuf, 4096, fdIn);
      if (!pRec)
         break;
      if (pChar->Apn[0] == ' ')
         continue;

      memset((void *)&myCharRec, ' ', sizeof(STDCHAR));
      memcpy(myCharRec.Apn, pChar->Apn, SOL_SIZCHAR_APN);
      // Format APN
      iRet = formatApn(pChar->Apn, acTmp, &myCounty);
      memcpy(myCharRec.Apn_D, acTmp, iRet);

      memcpy(myCharRec.Date_Updated, pChar->ChgDate, SOL_SIZCHAR_DATE);

      // YrBlt
      int iYrBlt = atoin(pChar->YrBlt, 4);
      if (iYrBlt > 1600 && iYrBlt <= lToyear)
      {
         iRet = sprintf(acTmp, "%d", iYrBlt);
         memcpy(myCharRec.YrBlt, acTmp, iRet);
      }

      // YrEff
      iTmp = atoin(pChar->EffYr, 4);
      if (iTmp > 1900 && iTmp >= iYrBlt && iTmp <= lToyear)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.YrEff, acTmp, iRet);
      }

      // BldgSize
      int iBldgSize = atoin(pChar->BldgArea, SOL_SIZCHAR_TOTALAREA);
      if (iBldgSize > 100)
      {
         iRet = sprintf(acTmp, "%d", iBldgSize);
         memcpy(myCharRec.BldgSqft, acTmp, iRet);
      }

      // Recreation area
      iTmp = atoin(pChar->RecrArea, SOL_SIZCHAR_BLDGAREA);
      if (iTmp > 0)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.RecArea, acTmp, iRet);
      }

      // Number of stories
      iTmp = atoin(pChar->NumStories, SOL_SIZCHAR_CNT);
      if (iTmp > 0)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.Stories, acTmp, iRet);
      }

      // Pools - no data

#ifdef _DEBUG
      //if (!memcmp(myCharRec.Apn, "0132050060", 10))
      //   iRet = 0;
#endif

      // Quality Class
      if (pChar->Qual_Cls[0] > '0')
         iTmp = sprintf(acTmp, "%.3s", pChar->Qual_Cls);
      else if (pChar->Qual_Cls[1] > '0')
         iTmp = sprintf(acTmp, "%.3s", &pChar->Qual_Cls[1]);
      else
         iTmp = sprintf(acTmp, "%.2s", &pChar->Qual_Cls[2]);
      if (memcmp(acTmp, "00", 2))
      {
         memcpy(myCharRec.QualityClass, acTmp, iTmp);
         iRet = Quality2Code(acTmp, acCode, NULL);
         myCharRec.BldgQual = acCode[0];
      }

      // Total Units
      iTmp = atoin(pChar->TotalUnits, SOL_SIZCHAR_4CHAR);
      if (iTmp > 0)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.Units, acTmp, iRet);
      }

      // Number of studios
      iTmp = atoin(pChar->NumStudio, SOL_SIZCHAR_4CHAR);
      if (iTmp > 0)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.NumStudio, acTmp, iRet);
      }

      // Number of 1 bed room
      iTmp = atoin(pChar->NumOneBed, SOL_SIZCHAR_4CHAR);
      if (iTmp > 0)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.OneBed, acTmp, iRet);
      }

      // Number of 2 bed rooms
      iTmp = atoin(pChar->NumTwoBed, SOL_SIZCHAR_4CHAR);
      if (iTmp > 0)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.TwoBeds, acTmp, iRet);
      }

      // Number of other rooms
      iTmp = atoin(pChar->NumOtherRoom, SOL_SIZCHAR_4CHAR);
      if (iTmp > 0)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.OthRooms, acTmp, iRet);
      }

      myCharRec.CRLF[0] = '\n';
      myCharRec.CRLF[1] = '\0';
      fputs((char *)&myCharRec.Apn[0], fdOut);

      if (!(++iCnt % 1000))
         printf("\r%u", iCnt);
   }

   if (fdIn) fclose(fdIn);
   if (fdOut) fclose(fdOut);

   LogMsg("Number of records processed: %d", iCnt);

   // Sort output on ASMT
   if (iCnt > 100)
   {
      iRet = iCnt;
      // Rename old CHAR file if needed
      //if (!_access(acCChrFile, 0))
      //{
      //   strcpy(acBuf, acCChrFile);
      //   replStr(acBuf, ".dat", ".mchar");
      //   if (!_access(acBuf, 0))
      //   {
      //      LogMsg("Delete old %s", acBuf);
      //      DeleteFile(acBuf);
      //   }
      //   LogMsg("Rename %s to %s", acCChrFile, acBuf);
      //   RenameToExt(acCChrFile, "mchar");
      //   sprintf(acTmp, "%s+%s", acBuf, acTmpFile);
      //} else
      //   strcpy(acTmp, acTmpFile);

      //// Sort on APN, last change date to keep the latest record on top
      //iRet = sortFile(acTmp, acCChrFile, "S(1,10,C,A,511,8,C,D) F(TXT)");
   } else
   {
      printf("\n");
      iRet = 0;
   }
   return iRet;
}

/**************************** Sol_ConvResChar ******************************
 *
 * Convert residential char file sd117901.txt to standard char format
 *
 * Return number of output record.  <0 if error.
 *
 ****************************************************************************/

int Sol_ConvResChar(char *pInfile)
{
   FILE     *fdIn, *fdOut;
   char     acBuf[4096], acTmpFile[256], acTmp[256], *pTmp;
   int      iRet, iTmp, iCnt=0;
   STDCHAR  myCharRec;
   SOL_CHAR *pChar = (SOL_CHAR *)acBuf;

   LogMsg0("Converting residential char file %s", pInfile);
   if (!(fdIn = fopen(pInfile, "r")))
      return -1;

   sprintf(acTmpFile, acAttrTmpl, myCounty.acCntyCode, "res");
   if (!(fdOut = fopen(acTmpFile, "w")))
   {
      fclose(fdIn);
      LogMsg("***** Error creating output file %s", acTmpFile);
      return -2;
   }

   while (!feof(fdIn))
   {
      pTmp = fgets(acBuf, 4096, fdIn);
      if (!pTmp)
         break;
      if (pChar->Apn[0] == ' ')
         continue;

      memset((void *)&myCharRec, ' ', sizeof(STDCHAR));
      memcpy(myCharRec.Apn, pChar->Apn, SOL_SIZCHAR_APN);
      // Format APN
      iRet = formatApn(pChar->Apn, acTmp, &myCounty);
      memcpy(myCharRec.Apn_D, acTmp, iRet);

      // YrBlt
      int iYrBlt = atoin(pChar->YrBlt, 4);
      if (iYrBlt > 1600 && iYrBlt <= lToyear)
      {
         iRet = sprintf(acTmp, "%d", iYrBlt);
         memcpy(myCharRec.YrBlt, acTmp, iRet);
      }

      // YrEff

      // Beds
      iTmp = atoin(pChar->Beds, SOL_SIZCHAR_CNT);
      if (iTmp > 0)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.Beds, acTmp, iRet);
      }

      // Baths
      if (pChar->Baths[0] != '0')
         myCharRec.FBaths[0] = pChar->Baths[0];
      if (pChar->Baths[1] != '0')
         myCharRec.HBaths[0] = '1';

      // Pool
      if (pChar->Pool[0] == 'P' && pChar->Pool[1] == 'L')
         myCharRec.Pool[0] = 'P';
      else if (pChar->Pool[0] == 'C' && pChar->Pool[1] == 'N')
         myCharRec.Pool[0] = 'A';

      // Quality
      char acCode[SIZ_BLDG_QUAL];
      if (pChar->Qual_Cls[0] > '0')
         iTmp = sprintf(acTmp, "%.3s", pChar->Qual_Cls);
      else if (pChar->Qual_Cls[1] > '0')
         iTmp = sprintf(acTmp, "%.3s", &pChar->Qual_Cls[1]);
      else
         iTmp = sprintf(acTmp, "%.2s", &pChar->Qual_Cls[2]);
      if (memcmp(acTmp, "00", 2))
      {
         memcpy(myCharRec.QualityClass, acTmp, iTmp);
         iRet = Quality2Code(acTmp, acCode, NULL);
         myCharRec.BldgQual = acCode[0];
      }

      // Fire Place
      if (pChar->Fp[0] == 'Y' || pChar->Fp[0] == '1')
         myCharRec.Fireplace[0] = '1';

      // Central Heat/Air/No
      if (pChar->Central_HA[0] == 'H')
      {
         myCharRec.Heating[0] = 'Z';
         if (pChar->Central_HA[1] == 'A')
            myCharRec.Cooling[0] = 'C';
      } else if (pChar->Central_HA[0] == 'N')
      {
         // None
         myCharRec.Heating[0] = 'L';
         myCharRec.Cooling[0] = 'N';
      }

      // Garage Sqft
      iTmp = atoin(pChar->Gar_Sqft, SOL_SIZCHAR_FLXSQFT);
      if (iTmp > 0)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.GarSqft, acTmp, iRet);
         myCharRec.ParkType[0] = 'Z';
      }

      // Patio Sqft
      iTmp = atoin(pChar->Add_Sqft, SOL_SIZCHAR_ADDSQFT);
      if (iTmp > 0)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.PatioSqft, acTmp, iRet);
      }

#ifdef _DEBUG
      //if (!memcmp(myCharRec.Apn, "0123060500", 9))
      //   iRet = 0;
#endif
      // Stories - check for sqft
      int iFlr1 = atoin(pChar->Fl1_Sqft, SOL_SIZCHAR_FLXSQFT);
      int iFlr2 = atoin(pChar->Fl2_Sqft, SOL_SIZCHAR_FLXSQFT);
      if (iFlr2 > 0)
      {
         myCharRec.Stories[0] = '2';
         iRet = sprintf(acTmp, "%d", iFlr2);
         memcpy(myCharRec.Sqft_2ndFl, acTmp, iRet);
      } else if (iFlr1 > 0)
      {
         myCharRec.Stories[0] = '1';
         iRet = sprintf(acTmp, "%d", iFlr1);
         memcpy(myCharRec.Sqft_1stFl, acTmp, iRet);
      }

      // Add Sqft
      int iAddSqft = atoin(pChar->Add_Sqft, SOL_SIZCHAR_ADDSQFT);
      if (iAddSqft > 0)
      {
         iRet = sprintf(acTmp, "%d", iAddSqft);
         memcpy(myCharRec.MiscSqft, acTmp, iRet);
      //} else
      //{
      //   // Other structure Sqft
      //   iTmp = atoin(pChar->OtherArea, SOL_SIZCHAR_BLDGAREA);
      //   if (iTmp > 0)
      //   {
      //      iRet = sprintf(acTmp, "%d", iTmp);
      //      memcpy(myCharRec.MiscSqft, acTmp, iRet);
      //   } 
      }

      // Bldg sqft
      iTmp = iFlr1+iFlr2+iAddSqft;
      if (iTmp > 0)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.BldgSqft, acTmp, iRet);
      }

      // Total Rooms
      iTmp = atoin(pChar->OthRooms, 2) + atoin(pChar->Beds, 2) + (pChar->Baths[0] & 0x0F);
      if (pChar->Dining[0] == 'Y')
         iTmp++;
      if (pChar->Family[0] == 'Y')
         iTmp++;
      if (pChar->UtilRoom[0] == 'Y')
         iTmp++;
      iRet = sprintf(acTmp, "%d", iTmp);
      memcpy(myCharRec.Rooms, acTmp, iRet);

      myCharRec.CRLF[0] = '\n';
      myCharRec.CRLF[1] = '\0';
      fputs((char *)&myCharRec.Apn[0], fdOut);

      if (!(++iCnt % 1000))
         printf("\r%u", iCnt);
   }

   if (fdIn) fclose(fdIn);
   if (fdOut) fclose(fdOut);

   LogMsg("Number of records processed: %d", iCnt);

   // Sort output on ASMT
   if (iCnt > 100)
   {
      // Rename old CHAR file if needed
      if (!_access(acCChrFile, 0))
      {
         strcpy(acBuf, acCChrFile);
         replStr(acBuf, ".dat", ".sav");
         if (!_access(acBuf, 0))
         {
            LogMsg("Delete old %s", acBuf);
            DeleteFile(acBuf);
         }
         LogMsg("Rename %s to %s", acCChrFile, acBuf);
         RenameToExt(acCChrFile, "sav");
      }

      char acMultiFile[256], acComFile[256];
      sprintf(acComFile, acAttrTmpl, myCounty.acCntyCode, "com");
      sprintf(acMultiFile, acAttrTmpl, myCounty.acCntyCode, "mul");
      sprintf(acBuf, "%s+%s+%s", acTmpFile, acComFile, acMultiFile);

      // Sort on APN, last change date to keep the latest record on top
      iRet = sortFile(acBuf, acCChrFile, "S(1,10,C,A,511,8,C,D) F(TXT)");
   } else
   {
      printf("\n");
      iRet = 0;
   }

   return iRet;
}

/**************************** Sol_ConvStdChar ******************************
 *
 * Convert char file "PropertyCharacteristics_SingleFamilyResidences.txt" to STDCHAR format.
 *
 *****************************************************************************/

int Sol_ConvStdChar(char *pInfile)
{
   FILE     *fdIn, *fdOut;
   char     acInbuf[1024], acOutbuf[2048], acTmpFile[256], acTmp[256], acCode[32], *pRec, *pTmp;
   int      iRet, iTmp, iCnt=0, lLotSqft, lLotAcres;
   double   dTmp;
   STDCHAR  *pCharRec=(STDCHAR *)&acOutbuf[0];

   LogMsg0("Converting char file %s", pInfile);
   if (!(fdIn = fopen(pInfile, "r")))
      return -1;

   // Drop header
   pRec = fgets(acInbuf, 1024, fdIn);

   sprintf(acTmpFile, "%s\\%s\\%s_char.tmp", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   if (!(fdOut = fopen(acTmpFile, "w")))
   {
      fclose(fdIn);
      LogMsg("***** Error creating output file %s", acTmpFile);
      return -2;
   }

   while (!feof(fdIn))
   {
      pRec = fgets(acInbuf, 1024, fdIn);

      if (!pRec)
         break;

      //replNull(acInbuf);
#ifdef _DEBUG
      //if (!memcmp(acInbuf, "002 007301800", 12))
      //   iRet = 0;
#endif

      iRet = ParseStringIQ(pRec, cDelim, SOL_C_FLDS, apTokens);
      if (iRet < SOL_C_FLDS)
      {
         LogMsg("*** Bad CHAR record (%d): %s", iCnt, pRec);
         continue;
      }

      memset(acOutbuf, ' ', sizeof(STDCHAR));

      // Apn
      memcpy(pCharRec->Apn, apTokens[SOL_C_APN], strlen(apTokens[SOL_C_APN]));
      
      // UseCode
      vmemcpy(pCharRec->LandUse, apTokens[SOL_C_USECODE], SIZ_CHAR_SIZE8);

      // Quality/class
      memcpy(pCharRec->QualityClass, apTokens[SOL_C_CLASS], strlen(apTokens[SOL_C_CLASS]));
      dTmp = atof(apTokens[SOL_C_CLASS]);
      if (dTmp > 0.0)
      {
         iRet = Quality2Code(apTokens[SOL_C_CLASS], acCode, NULL);
         if (iRet >= 0)
            pCharRec->BldgQual = acCode[0];
      }

      // YrBlt
      iTmp = atoi(apTokens[SOL_C_YRBLT]);
      if (iTmp > 1600 && iTmp != 1900)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(pCharRec->YrBlt, acTmp, iRet);
      }

      // YrEff
      //iTmp = atoi(apTokens[SOL_C_EFFYR]);
      //if (iTmp > 1600 && iTmp != 1900)
      //{
      //   iRet = sprintf(acTmp, "%d", iTmp);
      //   memcpy(pCharRec->YrEff, acTmp, iRet);
      //}

      // Units
      //iTmp = atol(apTokens[SOL_C_UNITS]);
      //if (iTmp > 0)
      //{
      //   iRet = sprintf(acTmp, "%d", iTmp);
      //   memcpy(pCharRec->Units, acTmp, iRet);
      //}

      // Lot Sqft - 9999999V9
      lLotSqft = atol(apTokens[SOL_C_LOTSIZE]);
      dTmp = atof(apTokens[SOL_C_ACRES]);
      if (dTmp > 0.0)
      {
         lLotAcres = (long)(dTmp*1000.0);
         iRet = sprintf(acTmp, "%d", lLotAcres);
         memcpy(pCharRec->LotAcre, acTmp, iRet);
      }
      if (lLotSqft > 0)
      {
         iRet = sprintf(acTmp, "%d", lLotSqft);
         memcpy(pCharRec->LotSqft, acTmp, iRet);
      }

      // BldgSqft
      iTmp = atol(apTokens[SOL_C_TOTAL_AREA]);
      if (iTmp > 100)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(pCharRec->BldgSqft, acTmp, iRet);
      }

      // First floor
      iTmp = atol(apTokens[SOL_C_FIRST_AREA]);
      if (iTmp > 100)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(pCharRec->Sqft_1stFl, acTmp, iRet);
      }
      // Second floor
      long l2ndArea = atol(apTokens[SOL_C_SECOND_AREA]);
      if (l2ndArea > 10)
      {
         iRet = sprintf(acTmp, "%d", l2ndArea);
         memcpy(pCharRec->Sqft_2ndFl, acTmp, iRet);
      }
      // Third floor
      long l3rdArea = atol(apTokens[SOL_C_THIRD_AREA]);
      if (l3rdArea > 10 && l2ndArea > 10)
      {
         iRet = sprintf(acTmp, "%d", l3rdArea);
         memcpy(pCharRec->Sqft_Above2nd, acTmp, iRet);
         l3rdArea = 0;
      }

      // Other area - if l3rdArea > 0, put it in MiscSqft
      iTmp = atol(apTokens[SOL_C_OTHERAREA]) + l3rdArea;
      if (iTmp > 10)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(pCharRec->MiscSqft, acTmp, iRet);
      }

      // Garage area
      iTmp = atol(apTokens[SOL_C_GARAGE_AREA]);
      if (iTmp > 20)
      {
         memcpy(pCharRec->GarSqft, apTokens[SOL_C_GARAGE_AREA], strlen(apTokens[SOL_C_GARAGE_AREA]));
         pCharRec->ParkType[0] = 'Z';
      }

      // Parking
      //if (*apTokens[SOL_C_PARKING] > '0')
      //{
      //   if (*apTokens[SOL_C_PARKING] == 'G')
      //      pCharRec->ParkType[0] = 'Z';
      //   else if (*apTokens[SOL_C_PARKING] == 'C')
      //      pCharRec->ParkType[0] = 'C';
      //   else if (*apTokens[SOL_C_PARKING] == 'X')
      //      pCharRec->ParkType[0] = '2';              // GARAGE/CARPORT

      //   if (*apTokens[SOL_C_PARKING] > 'A')
      //      iTmp = atol(apTokens[SOL_C_PARKING]+1);
      //   else
      //      iTmp = atol(apTokens[SOL_C_PARKING]);
      //   if (iTmp > 0)
      //   {
      //      iRet = sprintf(acTmp, "%d", iTmp);
      //      memcpy(pCharRec->ParkSpace, acTmp, iRet);
      //   }
      //}

      // Stories 99V9
      dTmp = atof(apTokens[SOL_C_STORIES]);
      if (dTmp > 0.0 && dTmp <= 99)
      {
         iRet = sprintf(acTmp, "%.1f    ", dTmp);
         memcpy(pCharRec->Stories, acTmp, iRet);
      } else
      {
         if (l3rdArea > 0)
            dTmp = 3.0;
         else if (l2ndArea > 0)
            dTmp = 2.0;
         else if (pCharRec->Sqft_1stFl[0] > '0')
            dTmp = 1.0;
         iRet = sprintf(acTmp, "%.1f    ", dTmp);
         memcpy(pCharRec->Stories, acTmp, iRet);
      }

      // Rooms
      iTmp = atol(apTokens[SOL_C_TOTAL_ROOMS]);
      if (iTmp > 0)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(pCharRec->Rooms, acTmp, iRet);
      }

      // Beds
      iTmp = atoi(apTokens[SOL_C_BEDROOM]);
      if (iTmp > 0)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(pCharRec->Beds, acTmp, iRet);
      }

      // Baths
      if (pTmp = strchr(apTokens[SOL_C_BATHROOM], '.'))
      {
         *pTmp = 0;
         pCharRec->HBaths[0] = '1';
      }

      iTmp = atoi(apTokens[SOL_C_BATHROOM]);
      if (iTmp > 0)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(pCharRec->FBaths, acTmp, iRet);
      } 

      // Other rooms
      iTmp = atoi(apTokens[SOL_C_OTHERROOMS]);
      if (iTmp > 0)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(pCharRec->OthRooms, acTmp, iRet);
      }

      // Dining room Y/N
      if (*apTokens[SOL_C_DINING] == 'Y')
         pCharRec->HasDiningRoom = 'Y';

      // Family room Y/N
      if (*apTokens[SOL_C_FAMILY] == 'Y')
         pCharRec->HasFamilyRoom = 'Y';

      // Has Solar
      if (*apTokens[SOL_C_SOLAR] == 'Y')
         pCharRec->HasSolar = 'Y';

      // Has Utility
      if (*apTokens[SOL_C_UTILITY] == 'Y')
         pCharRec->HasUtilities = 'Y';

      // Pool
      if (*apTokens[SOL_C_POOL] == 'Y')
         pCharRec->Pool[0] = 'P';    // Pool
      else if (*apTokens[SOL_C_POOL] == 'N')
         pCharRec->Pool[0] = 'N';    // None

      // HVAC
      if (!memcmp(apTokens[SOL_C_HVAC], "HA", 2))
      {
         // Central
         pCharRec->Cooling[0] = 'C';
         pCharRec->Heating[0] = 'Z';
      } else if (*apTokens[SOL_C_HVAC] == 'H')
      {  // Heat only
         pCharRec->Cooling[0] = 'N';
         pCharRec->Heating[0] = 'Y';
      } else
      {
         pCharRec->Cooling[0] = 'N';
         pCharRec->Heating[0] = 'L';
      }

      // Fireplace
      if (*apTokens[SOL_C_FIREPLC] < '1')
         pCharRec->Fireplace[0] = 'N';
      else
         pCharRec->Fireplace[0] = *apTokens[SOL_C_FIREPLC];

      // Condition - A/E/G/F/P
      //for (iTmp = 0; iTmp < 5; iTmp++)
      //{
      //   if (*apTokens[SOL_C_CONDITION] == *(gpCondition+iTmp))
      //   {
      //      pCharRec->ImprCond[0] = *apTokens[SOL_C_CONDITION];
      //      break;
      //   }
      //}

      // View 

      pCharRec->CRLF[0] = '\n';
      pCharRec->CRLF[1] = 0;
      fputs(acOutbuf, fdOut);

      if (!(++iCnt % 1000))
         printf("\r%u", iCnt);
   }

   if (fdIn) fclose(fdIn);
   if (fdOut) fclose(fdOut);

   // Sort output on ASMT
   if (iCnt > 100)
   {
      LogMsg("Sorting char file %s --> %s", acTmpFile, acCChrFile);
      iRet = sortFile(acTmpFile, acCChrFile, "S(1,12,C,A) F(TXT)");
   } else
   {
      printf("\n");
      iRet = 0;
   }
   return iRet;
}

int Sol_ParseTaxBase(char *pOutbuf, char *pInbuf)
{
   double	dTax1, dTax2, dTotalTax, dTotalDue, dTotalIntr, dTotalPen, dtotalFees, dTotalPaid;
   long     lTmp;
   char     sInRec[2048];
   TAXBASE  *pOutRec = (TAXBASE *)pOutbuf;
   SD116301 *pInRec = (SD116301 *)sInRec;

   strcpy(sInRec, pInbuf);

   // Fix bad chars
   if ((UCHAR)sInRec[787] == 0xC3 && (UCHAR)sInRec[788] == 0x91)
   {
      sInRec[787] = 'N';
      strcpy(&sInRec[788], pInbuf+789);
   }
   if ((UCHAR)sInRec[737] == 0xC3 && (UCHAR)sInRec[738] == 0x91)
   {
      sInRec[737] = 'N';
      strcpy(&sInRec[738], &sInRec[739]);
   }

   memset(pOutbuf, 0, sizeof(TAXBASE));
   // APN
   memcpy(pOutRec->Apn, pInRec->APN, TSIZ_APN);

   // Tax Year
   iTaxYear = atoin(pInRec->TaxYear, 4);
   if (iTaxYear < lTaxYear)
      return 1;

   memcpy(pOutRec->TaxYear, pInRec->TaxYear, 4);

   // Bill Number 
   memcpy(pOutRec->BillNum, pInRec->BillNumber, TSIZ_BILLNUM);

#ifdef _DEBUG
   //if (!memcmp(pInRec->APN, "0072231060", 10))
   //   dTax1 = 0;
#endif
   
   // Due date - convert mmddyyyy to yyyymmdd
   if (pInRec->Inst1_Due_Date[0] == ' ')
   {
      sprintf(pOutRec->DueDate1, "%.4s%.4s", &pInRec->Inst1_Due_Date[5], &pInRec->Inst1_Due_Date[1]);
      sprintf(pOutRec->DueDate2, "%.4s%.4s", &pInRec->Inst2_Due_Date[5], &pInRec->Inst2_Due_Date[1]);
      //lTmp = atol(pOutRec->DueDate1);
      //if (lTmp < 20201210)
      //   LogMsg("***** Bad due date 1: %.10s [%.10s]", pInRec->Inst1_Due_Date, pInRec->APN);
      //lTmp = atol(pOutRec->DueDate2);
      //if (lTmp < 20210410)
      //   LogMsg("***** Bad due date 2: %.10s [%.10s]", pInRec->Inst2_Due_Date, pInRec->APN);
   } else
   {
      sprintf(pOutRec->DueDate1, "%.4s%.4s", &pInRec->Inst1_Due_Date[4], pInRec->Inst1_Due_Date);
      sprintf(pOutRec->DueDate2, "%.4s%.4s", &pInRec->Inst2_Due_Date[4], pInRec->Inst2_Due_Date);
   }

   dTotalDue=dTotalIntr=dTotalPen=dtotalFees = 0;

   // Check for Tax amount
   dTotalTax = (double)atoln(pInRec->TotalTax, TSIZ_TAXAMT) / 100.0;
   if (dTotalTax > 0.0)
   {
      dTax1 = dTotalTax / 2.0;
      dTax2 = dTax1;
      sprintf(pOutRec->TaxAmt1, "%.2f", dTax1);
      sprintf(pOutRec->TaxAmt2, "%.2f", dTax2);
      sprintf(pOutRec->TotalTaxAmt, "%.2f", dTotalTax);

      dTotalDue = (double)atoln(pInRec->BalanceDue, 10)/100.0;
      sprintf(pOutRec->TotalDue, "%.2f", dTotalDue);

      // Paid Amount
      dTotalPaid = (double)atoln(pInRec->Total_Paid, TSIZ_TAXAMT)/100.0;

      if (!dTotalDue)
      {
         pOutRec->Inst1Status[0] = TAX_BSTAT_PAID;
         pOutRec->Inst2Status[0] = TAX_BSTAT_PAID;
         sprintf(pOutRec->PaidDate2, "%.4s%.4s", &pInRec->Payment_Eff_Date[4], pInRec->Payment_Eff_Date);
         sprintf(pOutRec->PaidAmt2, "%.2f", dTotalPaid);
      } else if (dTotalDue > 0.0 && dTotalDue < dTotalTax)
      {
         pOutRec->Inst1Status[0] = TAX_BSTAT_PAID;
         pOutRec->Inst2Status[0] = TAX_BSTAT_UNPAID;
         sprintf(pOutRec->PaidDate1, "%.4s%.4s", &pInRec->Payment_Eff_Date[4], pInRec->Payment_Eff_Date);
         sprintf(pOutRec->PaidAmt1, "%.2f", dTotalPaid);
      } else
      {
         pOutRec->Inst1Status[0] = TAX_BSTAT_UNPAID;
         pOutRec->Inst2Status[0] = TAX_BSTAT_UNPAID;
      }
   }

   // Penalty
   dTotalPen = atoln(pInRec->Penalty, TSIZ_TAXAMT);
   if (dTotalPen > 0.0)
      sprintf(pOutRec->PenAmt1, "%.2f", dTotalDue);

   if (pInRec->RollCaste[0] == 'A')
   {
      // Bill type
      pOutRec->BillType[0] = BILLTYPE_SECURED;
      pOutRec->isSecd[0] = '1';
      pOutRec->isSupp[0] = '0';
   } else  if (pInRec->RollCaste[0] == 'S')
   {
      pOutRec->BillType[0] = BILLTYPE_SECURED_SUPPL;
      pOutRec->isSecd[0] = '0';
      pOutRec->isSupp[0] = '1';
   } else  if (pInRec->RollCaste[0] == 'E')
   {
      pOutRec->BillType[0] = BILLTYPE_SECURED_ESCAPE;
      pOutRec->isSecd[0] = '1';
      pOutRec->isSupp[0] = '0';
   } else
      LogMsg("*** Unknown roll type: %15s [%s]", pInRec->RollCaste, pOutRec->Apn);

   return 0;
}

// Return number of records output
int Sol_ParseTaxDetail(char *pInBuf, FILE *fdDetail, FILE *fdAgency)
{
   char     acTmp[512], acOutbuf[512], acAgencyRec[512];
   int      iIdx;
   double   dVal;

   TAXDETAIL *pDetail= (TAXDETAIL *)&acOutbuf[0];
   TAXAGENCY *pAgency= (TAXAGENCY *)&acAgencyRec[0], *pResult;
   SD116301  *pInRec = (SD116301 *)pInBuf;

   // Clear output buffer
   memset(acOutbuf, 0, sizeof(TAXDETAIL));

   // APN
   memcpy(pDetail->Apn, pInRec->APN, TSIZ_APN);

   // Tracer number
   memcpy(pDetail->BillNum, pInRec->BillNumber, TSIZ_BILLNUM);

   // Tax Year
   sprintf(pDetail->TaxYear, "%d", iTaxYear);

#ifdef _DEBUG
   //if (!memcmp(pInRec->APN, "001 011503700", iApnLen))
   //   iIdx = 0;
#endif

   // Loop through tax items
   for (iIdx = 0; iIdx < SOL_MAXFUNDS; iIdx++)
   {      
      if (pInRec->asTaxFunds[iIdx].TAF_Fund[0] < '0')
         break;

      // Agency 
      memcpy(pDetail->TaxCode, pInRec->asTaxFunds[iIdx].TAF_Fund, TSIZ_FUNDNUM-1);

      // Tax Amt
      dVal =  (double)atoln(pInRec->asTaxFunds[iIdx].TAF_Tax_Amt, TSIZ_TAXAMT) / 100.0;
      sprintf(pDetail->TaxAmt, "%.2f", dVal);

      // Generate csv line and write to file
      Tax_CreateDetailCsv(acTmp, pDetail);
      fputs(acTmp, fdDetail);

      // Generate Agency rec
      pResult = findTaxAgency(pDetail->TaxCode);
      if (pResult)
      {
         memset(acAgencyRec, 0, sizeof(TAXAGENCY));
         strcpy(pAgency->Code,  pResult->Code);
         strcpy(pAgency->Agency,pResult->Agency);
         strcpy(pAgency->Phone, pResult->Phone);
         strcpy(pAgency->TaxRate, pResult->TaxRate);
         pAgency->TC_Flag[0] = pResult->TC_Flag[0];

         Tax_CreateAgencyCsv(acTmp, (TAXAGENCY *)&acAgencyRec);
         fputs(acTmp, fdAgency);
      } else
         LogMsg0("+++ Unknown TaxCode: %s [%s]", pDetail->TaxCode, pDetail->Apn);
   }

   return iIdx;
}

int Sol_ParseTaxDelq(char *pOutbuf, char *pInbuf)
{
   double	dTmp;

   TAXDELQ  *pOutRec = (TAXDELQ *)pOutbuf;
   SD116301 *pInRec = (SD116301 *)pInbuf;

   memset(pOutbuf, 0, sizeof(TAXDELQ));
   // APN
   memcpy(pOutRec->Apn, pInRec->APN, TSIZ_APN);

   // Tax Year
   iTaxYear = atoin(pInRec->TaxYear, 4);
   memcpy(pOutRec->TaxYear, pInRec->TaxYear, 4);

   // Delq year
   sprintf(pOutRec->TaxYear, "%d", iTaxYear-1);
   if (pInRec->RollCaste[0] == 'A')
   {
      pOutRec->isSecd[0] = '1';     // Secured Annual
   } else if (pInRec->RollCaste[0] == 'S')
   {
      pOutRec->isSupp[0] = '1';
   }

   // Default amount
   dTmp = (double)atoln(pInRec->Red_Grp_Bal_Due, TSIZ_TAXAMT);
   if (dTmp > 0.0)
   {
      sprintf(pOutRec->Def_Amt, "%.2f", dTmp / 100.0);

      // Delq status
      pOutRec->DelqStatus[0] = TAX_STAT_UNPAID;
      pOutRec->isDelq[0] = '1';
   }

   return 0;
}

/***************************** Sol_Load_TaxBase ******************************
 *
 * Create import file from tax base extract Base_Extr.txt and import into SQL 
 *
 * Return 0 if success.
 *
 *****************************************************************************/

int Sol_LoadTax(bool bImport)
{
   char     *pTmp, acBase[1024], acRec[2048], acOutbuf[MAX_RECSIZE],
            acBaseFile[_MAX_PATH], acItemsFile[_MAX_PATH], acTaxRoll[_MAX_PATH], 
            acAgencyFile[_MAX_PATH], acTmpFile[_MAX_PATH]; //, acDelqFile[_MAX_PATH], acDelq[1024];
            
   int      iRet, iDrop=0;
   long     lBase=0, lCnt=0, lItems=0, lDelq=0;
   FILE     *fdBase, *fdItems, *fdTaxRoll, *fdAgency; //, *fdDelq;
   TAXBASE  *pBase = (TAXBASE *)acBase;
   SD116301 *pTaxRoll = (SD116301 *)acRec;

   LogMsg0("Loading Tax file for SOL");

   // Check file date 
   GetIniString(myCounty.acCntyCode, "TaxRoll", "", acTaxRoll, _MAX_PATH, acIniFile);
   lLastTaxFileDate = getFileDate(acTaxRoll);
   // Only process if new tax file
   iRet = isNewTaxFile(NULL, myCounty.acCntyCode);
   if (iRet <= 0)
   {
      LogMsg("*** Skip loading Tax Base. Set ChkFileDate=N to bypass new file check");
      lLastTaxFileDate = 0;
      return iRet;
   }

   // Open input file
   LogMsg("Open tax file %s", acTaxRoll);
   fdTaxRoll = fopen(acTaxRoll, "r");
   if (fdTaxRoll == NULL)
   {
      LogMsg("***** Error opening tax file: %s (errno=%d)\n", acTaxRoll, _errno);
      return -2;
   }  

   // Open Output base file
   NameTaxCsvFile(acBaseFile, myCounty.acCntyCode, "Base");
   LogMsg("Create Base file %s", acBaseFile);
   fdBase = fopen(acBaseFile, "w");
   if (fdBase == NULL)
   {
      LogMsg("***** Error creating output file: %s\n", acBaseFile);
      return -4;
   }

   // Open Items file
   NameTaxCsvFile(acItemsFile, myCounty.acCntyCode, "Items");
   LogMsg("Create Items file %s", acItemsFile);
   fdItems = fopen(acItemsFile, "w");
   if (fdItems == NULL)
   {
      LogMsg("***** Error creating Items file: %s\n", acItemsFile);
      return -4;
   }

   // Open Agency file
   NameTaxCsvFile(acAgencyFile, myCounty.acCntyCode, "Agency");
   sprintf(acTmpFile, "%s\\%s\\%s_Agency.tmp", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   LogMsg("Create Agency file %s", acTmpFile);
   fdAgency = fopen(acTmpFile, "w");
   if (fdAgency == NULL)
   {
      LogMsg("***** Error creating Agency file: %s\n", acTmpFile);
      return -4;
   }

   // Merge loop 
   while (!feof(fdTaxRoll))
   {
      pTmp = fgets((char *)&acRec[0], 2048, fdTaxRoll);
      if (!pTmp)
         break;

      // Create new R01 record
      iRet = Sol_ParseTaxBase(acBase, acRec);
      if (!iRet)
      {
         // Create TaxBase record
         Tax_CreateTaxBaseCsv(acOutbuf, (TAXBASE *)&acBase);

         // Output record			
         lBase++;
         fputs(acOutbuf, fdBase);

         lItems += Sol_ParseTaxDetail(acRec, fdItems, fdAgency);
      } else
      {
         if (bDebug)
            LogMsg("---> Drop base record %d [%.40s]", lCnt, acRec); 
         iDrop++;
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdTaxRoll)
      fclose(fdTaxRoll);
   if (fdBase)
      fclose(fdBase);
   if (fdItems)
      fclose(fdItems);
   if (fdAgency)
      fclose(fdAgency);

   LogMsg("Total records processed:   %u", lCnt);
   LogMsg("    base records output:   %u", lBase);
   LogMsg("  detail records output:   %u", lItems);
   LogMsg("        records dropped:   %u", iDrop);

   // Import into SQL
   if (bImport)
   {
      iRet = doTaxImport(myCounty.acCntyCode, TAX_BASE);
      if (!iRet)
         iRet = doTaxImport(myCounty.acCntyCode, TAX_DETAIL);
      if (!iRet)
      {
         // Dedup Agency file before import
         LogMsg("Dedup Agency file %s to %s", acTmpFile, acAgencyFile);
         iRet = sortFile(acTmpFile, acAgencyFile, "S(#1,C,A) DUPOUT F(TXT)");
         if (iRet > 0)
            iRet = doTaxImport(myCounty.acCntyCode, TAX_AGENCY);
      }
   } else
      iRet = 0;

   return iRet;
}

/******************************** Sol_ParseDelq *****************************
 *
 * Return 0 if success
 *
 *****************************************************************************/

int Sol_ParseDelq(char *pOutbuf, char *pInbuf)
{
   double	dTaxAmt, dDueAmt, dPenAmt, dFeeAmt;
   TAXDELQ  *pOutRec = (TAXDELQ *)pOutbuf;

   // Parse input tax data
   iTokens = ParseStringNQ(pInbuf, cDelim, MAX_FLD_TOKEN, apTokens);
   if (iTokens < TD_FLDS)
   {
      LogMsg("***** Error: bad Delq record for: %.50s (#tokens=%d)", pInbuf, iTokens);
      return -1;
   }

   // Clear output buffer
   memset(pOutbuf, 0, sizeof(TAXDELQ));

   // APN
   strcpy(pOutRec->Apn, apTokens[TD_APN]);
   strcpy(pOutRec->BillNum, apTokens[TD_BILLNUMBER]);

#ifdef _DEBUG
   //if (!memcmp(pOutRec->Apn, "2105001054", 10))
   //   iTmp = 0;
#endif

   // Updated date
   sprintf(pOutRec->Upd_Date, "%d", lLastTaxFileDate);

   // Default amount
   dTaxAmt = (double)atol(apTokens[TD_TAX_DUE]);
   if (!dTaxAmt)
   {
      if (bDebug)
         LogMsg("---> NO delq amt: APN=%s, BillNum=%s ==> Ignore", apTokens[TD_APN], apTokens[TD_BILLNUMBER]); 
      return -2;
   }

   sprintf(pOutRec->Def_Amt, "%.2f", dTaxAmt);
   pOutRec->isDelq[0] = '1';

   // Default date
   //strcpy(pOutRec->Def_Date, apTokens[TD_DELQYEAR]);

   // Tax Year
   strcpy(pOutRec->TaxYear, apTokens[TD_TAX_YEAR]);

   dDueAmt = (double)atol(apTokens[TD_RED_BAL_DUE])/100.0;
   sprintf(pOutRec->Def_Amt, "%.2f", dDueAmt);

   // Pen Amt
   if (*apTokens[TD_PEN_DUE] > '0')
   {
      dPenAmt = (double)atol(apTokens[TD_PEN_DUE])/100.0;
      sprintf(pOutRec->Pen_Amt, "%.2f", dPenAmt);
   }

   // Fee Amt
   if (*apTokens[TD_FEE_DUE] > '0')
   {
      dFeeAmt = (double)atol(apTokens[TD_FEE_DUE])/100.0;
      sprintf(pOutRec->Fee_Amt, "%.2f", dFeeAmt);
   }
   
   if (dDueAmt > 0.0)
   {
      pOutRec->DelqStatus[0] = TAX_STAT_UNPAID;
      pOutRec->isDelq[0] = '1';
   }

   return 0;
}

int Sol_Load_TaxDelq(bool bImport)
{
   char     acOutFile[_MAX_PATH], acDelqFile[_MAX_PATH];
   char     acBuf[512], acRec[512], *pTmp;
   int      iRet, lCnt=0, lOut=0, iDrop=0;
   FILE     *fdIn, *fdOut;

   LogMsg0("Loading Tax Delinquent");

   // Concat file1 & file2
   GetIniString(myCounty.acCntyCode, "TaxDelq", "", acDelqFile, _MAX_PATH, acIniFile);
   lLastTaxFileDate = getFileDate(acDelqFile);

   // Open input file
   LogMsg("Open tax delinquent file %s", acDelqFile);
   fdIn = fopen(acDelqFile, "r");
   if (fdIn == NULL)
   {
      LogMsg("***** Error opening Secured tax file: %s (%d)\n", acDelqFile, _errno);
      return -2;
   }  

   // Open output file
   NameTaxCsvFile(acOutFile, myCounty.acCntyCode, "Delq");
   LogMsg("Open output file %s", acOutFile);
   fdOut = fopen(acOutFile, "w");
   if (fdOut == NULL)
   {
      LogMsg("***** Error creating output file: %s\n", acOutFile);
      return -4;
   }

   // Merge loop 
   while (!feof(fdIn))
   {
      pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdIn);
      if (!pTmp || *pTmp < ' ')
         break;

      // Create new R01 record
      iRet = Sol_ParseDelq(acBuf, acRec);
      if (!iRet)
      {
         // Create delimited record
         Tax_CreateDelqCsv(acRec, (TAXDELQ *)&acBuf);

         // Output record			
         lOut++;
         fputs(acRec, fdOut);
      } else
         iDrop++;

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdIn)
      fclose(fdIn);
   if (fdOut)
      fclose(fdOut);

   LogMsg("Total records processed:    %u", lCnt);
   LogMsg("Total output records:       %u", lOut);
   LogMsg("      records dropped:      %u", iDrop);

   // Import into SQL
   if (bImport)
      iRet = doTaxImport(myCounty.acCntyCode, TAX_DELINQUENT);
   else
      iRet = 0;

   return iRet;
}

/*********************************** loadSol ********************************
 *
 * - Lien Process:  -CSOL -L -Xl -Ma [-Mn] [-Mr] 
 * - Normal Update: -CSOL -U -Ma [-Xn] [-Mn] [-T] [-Mr]
 *
 ****************************************************************************/

int loadSol(int iSkip)
{
   int   iRet=0;
   char  acTmpFile[_MAX_PATH];

   iApnLen = myCounty.iApnLen;

   // Load tax 
   if (iLoadTax == TAX_LOADING)                    // -T
   {
      iRet = GetIniString(myCounty.acCntyCode, "TaxDist", "", acTmpFile, _MAX_PATH, acIniFile);
      if (iRet > 0)
         iRet = LoadTaxCodeTable(acTmpFile);

      // SOL has base, detail, agency in one file
      iRet = Sol_LoadTax(bTaxImport);

      // Load delq file
      if (!iRet)
         iRet = Sol_Load_TaxDelq(bTaxImport);

      // Update Delq flag in Tax_Base
      if (!iRet)
         iRet = updateDelqFlag(myCounty.acCntyCode);
   } 

   // Exit if load/update tax only
   if (!iLoadFlag)
      return iRet;

	// Load char file
   if (iLoadFlag & EXTR_ATTR)                      // -Xa
   {
      // 05/03/2023
      iRet = Sol_ConvStdChar(acCharFile);
      //// Load comm char
      //GetIniString(myCounty.acCntyCode, "CommChar", "", acTmpFile, _MAX_PATH, acIniFile);
      //if (!_access(acTmpFile, 0))
      //   iRet = Sol_ConvComChar(acTmpFile);
      //else
      //   LogMsg("***** Missing input file: %s", acTmpFile);

      //// Load multi property char
      //GetIniString(myCounty.acCntyCode, "MultiFile", "", acTmpFile, _MAX_PATH, acIniFile);
      //if (!_access(acTmpFile, 0))
      //   iRet = Sol_ConvMultiChar(acTmpFile);
      //else
      //   LogMsg("***** Missing input file: %s", acTmpFile);

      //// Load residential char
      //GetIniString(myCounty.acCntyCode, "CharFile", "", acTmpFile, _MAX_PATH, acIniFile);
      //if (!_access(acTmpFile, 0))
      //   iRet = Sol_ConvResChar(acCharFile);
      //else
      //   LogMsg("***** Missing input file: %s", acTmpFile);
   }

   // Extract LDR values
   if (iLoadFlag & EXTR_LIEN)                      // -Xl
      iRet = Sol_ExtrLien();

   // Extract Attom sales 
   if (iLoadFlag & EXTR_NRSAL)                     // -Xn
   {
      iRet = GetIniString(myCounty.acCntyCode, "NdcSale", "", acSaleFile, _MAX_PATH, acIniFile);
      if (!_access(acSaleFile, 0))
      {
         iRet = NR_CreateSCSale(myCounty.acCntyCode, acSaleFile, iApnLen);
         if (iRet)
            return iRet;
      } else
      {
         LogMsg("*** Sale file not available.  Please verify: %s", acSaleFile);
         return -1;
      }
   }

   if (iLoadFlag & LOAD_LIEN)                      // -L
   {
      // Create Lien file
      LogMsg0("Load %s Lien file", myCounty.acCntyCode);
      iRet = Sol_Load_LDR(iSkip);
   } else if (iLoadFlag & LOAD_UPDT)               // -U
   {
      LogMsg0("Load %s roll update file", myCounty.acCntyCode);
      iRet = Sol_Load_RollCsv(iSkip);
      //iRet = Sol_Load_Roll(iSkip);
   }

   // Update chars extracted from the web
   if (!iRet && (iLoadFlag & MERG_ATTR))          // -Ma
   {
      if (!_access(acCChrFile, 0))
      {
         iRet = Sol_MergeCChr(myCounty.acCntyCode, iSkip);
         if (!iRet)
            iLoadFlag |= LOAD_UPDT;
      } else
         LogMsgD("Missing chars file: %s", acCChrFile);
   }

   // Apply cum sale file to R01
   if (!iRet && (iLoadFlag & UPDT_XSAL) )          // -Mn
   {
      GetIniString("Data", "ASH_File", acESalTmpl, acESalTmpl, _MAX_PATH, acIniFile);
      sprintf(acCSalFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "Sls");

      // Apply Sol_Sale.sls to R01 file
      //iRet = ApplyCumSale(iSkip, acCSalFile, false, SALE_USE_SCUPDXFR, CLEAR_OLD_SALE);
      iRet = ApplyCumSaleNR(iSkip, acCSalFile, SALE_USE_SCUPDXFR, CLEAR_OLD_SALE);

      if (!iRet)
         iLoadFlag |= LOAD_UPDT;
   }

   // Update usecode - manually run only
   //if (iLoadFlag & UPDT_SUSE)
   //   iRet = updateR01StdUse(myCounty.acCntyCode, iSkip);

   return iRet;
}