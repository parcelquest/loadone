#if !defined(_FLDDEF_PARSER_H_)
#define _FLDDEF_PARSER_H_

#define  GRP_PROPINFO   1
#define  GRP_PROPCHAR   2
#define  GRP_PROPZONE   4
#define  GRP_PROPVALU   8

#define  PI_APN         0x10
#define  PI_ACRES       0x11
#define  PI_EXE_AMT     0x12
#define  PI_LOTSIZE     0x13
#define  PI_CENSUS      0x14
#define  PI_TRA         0x15
#define  PI_ADDR        0x16
#define  PI_CITY        0x17
#define  PI_DOCDATE     0x18
#define  PI_DOCNUM      0x19
#define  PI_DOCTAX      0x1A
#define  PI_DOCTYPE     0x1B
#define  PI_DOCDESC     0x1C
#define  PI_PAGECNT     0x1D
#define  PI_SUBDIV      0x1E

#define  PC_QUALCLS     0x20
#define  PC_BEDS        0x21
#define  PC_FL1AREA     0x22
#define  PC_YRBLT       0x23
#define  PC_BATHS       0x24
#define  PC_FL2AREA     0x25
#define  PC_FIREPLACE   0x26
#define  PC_D_ROOM      0x27
#define  PC_OTH_RES     0x28
#define  PC_POOL        0x29
#define  PC_F_ROOM      0x2A
#define  PC_ADD_AREA    0x2B
#define  PC_U_ROOM      0x2C
#define  PC_TOTAL_RES   0x2D
#define  PC_O_ROOM      0x2E
#define  PC_ROOMS       0x2F
#define  PC_GAR_AREA    0x30
#define  PC_ADD_STRU    0x31
#define  PC_ACRES       0x32
#define  PC_BLDGSQFT    0x33
#define  PC_3RDAREA     0x34
#define  PC_BSMTAREA    0x35
#define  PC_YRBLTEFF    0x36
#define  PC_GAR_CARP    0x37

#define  PC_CEN_H_A     0x3A
#define  PC_CEN_HEAT    0x3B
#define  PC_CEN_AIR     0x3C
#define  PC_HA_NONE     0x3D

#define  PZ_AGP         0x40
#define  PZ_AGPSTAT     0x41
#define  PZ_TIMBER      0x42
#define  PZ_TIMBERSTAT  0x43
#define  PZ_ZONE_1      0x44
#define  PZ_ZONE_2      0x45
#define  PZ_ZONE_2AC    0x46

#define  PV_TAX_YR      0x80
#define  PV_STATUS      0x81
#define  PV_TAC         0x82
#define  PV_USE         0x83
#define  PV_EXE_STATUS  0x84
#define  PV_LAND        0x85
#define  PV_IMPR        0x86
#define  PV_TV          0x87
#define  PV_MINERAL     0x88
#define  PV_FIXTRS      0x89
#define  PV_PERSPROP    0x8A
#define  PV_PENALTIES   0x8B
#define  PV_HO_EXE      0x8C
#define  PV_OTHER_EXE   0x8D
#define  PV_NET_VAL     0x8E

typedef struct _flditem
{
   char  sFldName[32];     // Field name in Html
   char  sDbName[16];      // Import db name
   int   iLen;             // Length of field name
   int   iFldId;           // Unique field ID
} FLDITEM;

typedef struct _tEx_Char
{  // 1400 bytes
   char APN      [20];     // 1
   char LotAcres [20];     // 21
   char ExeAmt   [20];     // 41 - Total EXE
   char LotSqft  [20];     // 61
   char Census   [20];     // 81
   char QualCls  [20];     // 101
   char AirCond  [20];     // 121
   char Heat     [20];     // 141
   char Beds     [20];     // 161
   char Area1    [20];     // 181
   char YrBlt    [20];     // 201
   char Baths    [20];     // 221
   char Area2    [20];     // 241
   char Fp       [20];     // 261
   char Din_Room [20];     // 281
   char Oth_Res  [20];     // 301
   char Pool     [20];     // 321
   char Fam_Room [20];     // 341
   char Add_Area [20];     // 361
   char Util_Room[20];     // 381
   char Res_Area [20];     // 401
   char Oth_Room [20];     // 421
   char Rooms    [20];     // 441
   char Gar_Area [20];     // 461
   char Str_Area [20];     // 481
   char AgStat   [20];     // 501
   char Agp      [20];     // 521
   char TimbStat [20];     // 541
   char Timber   [20];     // 561
   char Status   [20];     // 581
   char TRA      [20];     // 601
   char LandUse  [20];     // 621
   char ExeCode  [20];     // 641
   char Land     [20];     // 661
   char Impr     [20];     // 681
   char TreeVines[20];     // 701
   char Mineral  [20];     // 721
   char Fixtr    [20];     // 741
   char PersProp [20];     // 761
   char Penalties[20];     // 781
   char HO_Exe   [20];     // 801
   char Oth_Exe  [20];     // 821
   char NetVal   [20];     // 841
   char S_Addr   [40];     // 861
   char S_City   [40];     // 901
   char DocDate  [10];     // 941
   char DocNum   [12];     // 951
   char DocTax   [12];     // 963
   char ExtDate  [8];      // 975   // Extract date
   char Zone_1   [8];      // 983
   char Zone_2   [8];      // 991
   char Zone_2AC [8];      // 999
   char BsmtArea [20];     // 1007
   char Area3    [20];     // 1027
   char SubDiv   [80];     // 1047
   char YrEff    [8];      // 1127
   char Carp_Area[20];     // 1135
   char Filler   [244];    // 1155
   char Crlf[2];           // 1399
} EX_CHAR;

#endif