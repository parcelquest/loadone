/*****************************************************************************
 *
 * To be done:
 *    - Parse Owner into name1 & name2 where "<LF>" is present
 *
 * Input files:
 *    - SDX_LIEN                 (LDR file, 554-byte EBCDIC)
 *      Check every year for possible LDR format.  If they send us the same
 *      regular update format, just modify Sdx_CreatePQ4() to call Sdx_MergeRoll().
 *      * Since 2008, LDR file is change to 628-byte ASCII.
 *
 *    - COSDMPR.DAT              (roll file, 649-byte fixed)
 *      This file is available weekly on Saturday @6:00pm
 *    - COSDPCHAR.DAT            (char file, 299-byte )
 *      This file is avail. monthly on first Friday of the month.
 *    - COSDSCHAR.DAT            (sale file, 222-byte )
 *      This file is avail. monthly on first Tuesday of the month.  Only contains last
 *      2 years of data. 
 *    - GRANTORGRANTEEADDS.DAT   (GrGr data, 132-byte ends with CRLF)
 *      This file is avail daily at night.  Data is sent when recorded day is
 *      done keying.
 *
 * Commands:
 *    -Xc   Extract cum sale from R01
 *    -Us   Create Sale_Exp.dat from county sale file COSDSCHAR.DAT
 *          Output file will be used in both -U and -Ua
 *    -U,Ua Monthly update
 *    -L    Lien update
 *    -Lg   Load GrGr file to create Xfer_exp.dat
 *    -Mg   Merge GrGr.  This flag is used for testing only
 *    -Mt   Update transfer
 *
 * Notes: - To capture all previous sales from mainframe, run -Xc first to collect
 *          history sale.  Or you can run -L -Xc at the same time but never
 *          run -Xc and -Us the same time.  Both -Xc and -Us will set acSaleFile
 *          to its output file.
 *        - If we have full history sale from the county, previous step is not needed.
 *        - If not, load GrGrFull and merge it to Lien Roll, then apply current
 *          sales if needed.
 * GrGr processing:
 *        - Drop all records that are older than 4 months.
 *        For Assr:
 *          - If GrGr RecDate <= CurRecDate, drop it.
 *          - Create new GDate and GAmt field that contains latest sale info from
 *            either current sale or GrGr.
 *
 * LDR command: -CSDX -L -Xl -Ms -Mg -Xv -Xa -X8 -Dr -Mr 
 * Update cmd:  -CSDX -U -Xs|-Ms -G|-Mg -Xa -Mr [-X8] [-Xv]
 *
 * Additional data:
 *        - New fields added: PREV_APN, TAX_CODE (Y/N)
 *
 * 08/15/2007 1.4.26    Add Sdx_ExtrLien() function.
 * 01/17/2008 1.5.3.8   Add code to support new standard UseCode.
 * 02/25/2008 1.5.5.1   Adding code to create update file
 * 03/24/2008 1.5.7.2   Format IMAPLINK.
 * 07/02/2008 8.0.1     Modify Sdx_MergeLien() to support MPR08 format.  Adding
 *                      Sdx_MergeSAdr_Mpr08() and Sdx_MergeMAdr_Mpr08() to support new format.
 * 12/04/2008 8.4.7     GrGr input record layout change.  SEQ_NUM increases from 4 to 6 bytes.
 * 01/17/2009 8.5.6     Fix TRA
 * 05/21/2009 8.8       Set full exemption flag
 * 05/29/2009 8.8.1.1   Remove update file creation from Sdx_MergePQ4() and set flag bCreateUpd
 *                      to use standard update function.
 * 06/06/2009 8.8.3     Add Prop8 flag.
 * 07/01/2009 9.1.0     Modify Sdx_CreateGrGrFile() to remove section that create
 *                      Sdx_GExp.dat.  Remove function Sdx_MergeGrGrFile() and create
 *                      Sdx_MergeGrGrExp() to merge GrGr.  Populate PERSPROP.
 * 02/04/2010 9.3.10    Stop update TRANSFER with GRGR data.  Fix DOCNUM problem by cleanup
 *                      garbage at the end of DOCNUM.  Reset first byte of DOCNUM to 0.
 * 02/06/2010 9.4.0     Extending LEGAL to LEGAL1
 * 02/11/2010 9.4.2     Adding Sdx_ExtractProp8()
 * 06/30/2010 10.0.0    Modify Sdx_MergeSaleRec() to make updating transfer an option.
 *                      Adding exemption code to R01 layout and update LDRRecCount.  Also
 *                      change params on Sdx_MergeGrGrExp()
 * 07/01/2010 10.0.1    Fix bug that puts 'Y' in ALT_APN in Sdx_MergeSaleRec().
 * 03/25/2011 10.5.0    Let program run even if GrGr data not available.  Send email notif instead.
 * 04/18/2011 10.5.5    Fix bug that generate missing file error when we intendedly rename output
 *                      file when update GrGr.
 * 05/24/2011 10.5.13   Fix bad character in Sdx_MergeMAdr().
 * 07/03/2011 11.0.0    Modify Sdx_MergeSAdr_Mpr08() and Sdx_MergeSAdr() to add S_HSENO.
 *                      Modify Sdx_MergeLien() due to input record realignment.
 * 10/06/2011 11.4.12   Add Sdx_ExtrSale() to update cum sale and Sdx_LoadSaleHist() to load
 *                      county history sale file (salehist) using standard SCSALE_REC layout.
 *                      Modify Sdx_Load_Roll() & Sdx_Load_LDR() to use new cum sale to merge into R01.
 * 01/17/2012 11.6.4    Add more error check loading GrGr.
 * 03/27/2012 11.8.12   Add Sdx_UpdateXfer() to merge transfer to cum sale file.
 * 04/01/2012 11.8.12.1 Modify Sdx_UpdateXfer() to force output fixed length APN and DOCNUM.
 * 04/03/2012 11.8.13   Fix Sdx_LoadSaleHist(), fix bug in Sdx_FormatSCSale().  
 * 06/04/2012 11.9.9    Create GrGr import file if load successful.
 * 06/29/2012 12.1.1    Modify Sdx_ExtractProp8() to correct extracted count.
 * 10/14/2012 12.3.0    Remove import GRGR code that didn't work correctly.  Let _tmain()
 *                      do it globally by using -Gi option and set ImportGrgr=Y in LOADONE.INI file
 * 03/14/2013 12.5.1    Modify Sdx_MergeGrec() to update sale price only if not present.
 *                      Modify Sdx_MergeSCSale() to clear old sales before update.
 * 03/19/2013 12.5.1.1  Change address matching algorithm for copying city name from mail to situs.
 *                      If m_str=s_str, copy city name & zipcode. Old method also compare strnum.
 * 04/11/2013 12.5.4    Fix Sdx_MergeMAdr() to correctly populate ADDR_D.
 * 04/28/2013 12.6.1    Modify Sdx_MergeSAdr() to add more way to guess situs city.
 * 05/05/2013 12.6.2    Adding GetCityZip() to find specific city/zip for a parcel
 * 05/06/2013 12.6.2.1  Stop using asTRA[] since it's not accurately at city level.
 * 07/06/2013 13.0.1    Set YrBlt=YrEff and fix bad char in situs and careof.
 * 08/14/2013 13.5.10   Fix bug in Sdx_MergeMAdr() with passing correct param to updateCareOf().
 *                      Call initSitus() to open CityZip file.
 * 09/25/2013 13.8.4    Modify Sdx_MergeSAdr() to add '#' to situs unit number.
 * 10/15/2013 13.11.2   Use removeName() to clear out old names. Use vesting from county.
 * 10/16/2013 13.11.3   Use updateVesting() if marital status is "NS".
 *                      Ignore Name2 if it is the same as Name1.
 * 07/08/2014 14.0.4    Fix CareOf bug in Sdx_MergeMAdr_Mpr08().
 * 07/23/2014 14.1.2    Change to getCityZip() call to specify APN length.
 * 01/08/2015 14.10.3   Modify Sdx_MergeOwner() not to swap owner if it is a corp name.
 *                      Add Sdx_LoadGrGr() to process new GRGR layout (from 12/16/2014).
 *                      Expand SDX_GREC to 512 bytes and adding new fields NoneSale, NoneXfer, 
 *                      MoreName, DocRef, and Legal.  Modify Sdx_MergeGrec() to update transfer.
 *                      Add option Sdx_GrGr_Xref to INI file to specify DocType translation table.
 * 02/14/2015 14.12.1   Fix DocTax format and realign SalePrice in Sdx_LoadGrGr()
 * 04/16/2015 14.14.1   Modify Sdx_MergeSAdr() to use city from cityzip list first.
 *                      If not found, check mail addr.
 * 12/10/2015 15.3.7    Add option to use MapXRef file to update maplink and ALT_APN.
 *                      Only update PREV_APN if Old_Apn > '0'
 * 01/07/2016 15.4.1    Modify Sdx_LoadGrGr() to save prior version of SLS file.
 * 02/17/2016 15.6.0    Fix swap name in Sdx_MergeOwner() and clean up Sdx_MergeMAdr().
 * 06/09/2016 15.9.5    Add -T option to load tax data.
 * 06/10/2016 15.9.6    Unzip tax file before processing. The mailing address from tax file
 *                      is badly formatted.  *** We should use data from roll file.
 * 08/24/2016 16.2.3    Modify Sdx_ParseTaxBase() to create Detail record for import. Modify Sdx_Load_TaxBase() 
 *                      to update DelqYear in TaxBase. Import Tax_Agency table from external file.
 * 09/29/2016 16.2.9    Fix bad char in LEGAL in Sdx_MergeRoll(), Sdx_MergeLien(), and Sdx_LoadGrGr()
 * 10/25/2016 16.5.1    SDX doesn't provide Tax Code & Agency, we hardcode these values when we see fit.
 *                      Use Tax_CreateAgencyTable() to load agency table from from file defined in INI [TaxDist].
 * 11/03/2016 16.6.0    Modify Sdx_MergeSAdr() to use M_City if matching StrNum & strName except when the entry
 *                      in the CityZip file is confirmed.  Add -Fc option to update CityZip file. 
 * 12/02/2016 16.7.5    Add error check for file copy in loadSdx()
 * 12/19/2016 16.8.3    Set VIEW='A' when HasView='Y'
 * 12/31/2016 16.8.4    Add doGrGr.h
 * 01/19/2016 16.9.7    Modify Sdx_ParseTaxBase() & Sdx_Load_TaxBase() and add Sdx_Load_TaxDetail() to load
 *                      Tax_Items and Tax_Agency.  Get BaseAmt from TaxRoll and the rest from TaxDetail.
 * 01/20/2017 16.9.7.1  Populate all items from TAX_RATE.DAT
 *            16.9.7.2  Rename zip file after unzip
 * 01/25/2017 16.9.9    Increase SaleRec buffer in Sdx_MergeGrGrExp() to allow future record size increase.
 *                      Modify Sdx_LoadGrGr() to use char buffer instead struct SDX_GREC in hope to catch memory leak.
 * 01/31/2017 16.10.1   Fix TaxAmt in Sdx_CreateTaxDetailOnTRA() and only output to Detail if TaxAmt > 0
 * 02/01/2017 16.10.2   Add verification in Sdx_LoadGrGr() to make sure GrGr output file is not corrupted.
 * 03/12/2017 16.12.0   Modify Sdx_ParseTaxBase() to add instStatus.
 * 03/18/2017 16.12.4   Modify loadSdx() to remove the copying S01 to R01 in Merge GrGr section.
 *                      Increase temp buffer size in Sdx_LoadGrGr() trying to find memory leak.
 * 05/03/2017 16.14.8   Add Sdx_ExtrEnrolledValues() & Sdx_ExtrFactorBaseYearValues() to load value files
 * 06/02/2017 16.14.15  Add BillNum & DueDate to Sdx_ParseTaxBase().
 * 06/18/2017 16.15.2   Modify Sdx_ParseTaxBase() to populate TaxAmt1 & 2 with original InstAmt.
 * 06/23/2017 17.0.1    Make call to updateDelqFlag() to update Delq flag in Tax_Base table.
 * 05/30/2018 17.11.3   Modify Sdx_CreateTaxDetailOnTRA() to log new AV items.  
 *                      Modify Sdx_ParseTaxDetail() to support CSV format.
 * 07/03/2018 18.0.1    Bug fix in Sdx_MergeLien().
 * 11/01/2018 18.5.11   Modify Sdx_MergeChar() to remove YrBlt due to county complain.
 * 11/13/2018 18.6.0    Add Sdx_CreateTaxDetailOnTRA_Csv() to replace Sdx_CreateTaxDetailOnTRA().
 *                      Add Sdx_ParseTaxRate(), Sdx_ParsePhoneList(), and Sdx_ParseSpecAsmtList().
 *                      Modify Sdx_ParseTaxBase() to fix case when user prepaid inst2 the year before.
 *                      Modify Sdx_Load_TaxDetail() to support different version of tax detail format.
 * 03/05/2019 18.9.8    Fix owner1 truncation error in MergeOwner().
 * 06/29/2019 19.0.1    Add Sdx_MergeProp8() & Sdx_ExtrLdrProp8() to create Prop8 extract file "SDX_2019P8.txt"
 *                      using "Prop8 2019.txt" file from county. Modify Sdx_Load_LDR() to use Sdx_MergeProp8().
 *                      Change -X8 option to extract prop8 either from "Prop8 2019.txt" or "COSDPCHAR.DAT".
 * 07/03/2019 19.0.2    Add Sdx_MergeOwner_LDR(), Sdx_MergeSAdr_LDR(), Sdx_MergeMAdr_LDR(), Sdx_ExtrLien_CSV().
 *                      Add new version of Sdx_MergeLien() to support new LDR file in CSV format.
 *                      Fix bug in Sdx_ParseEnrolledValue().
 * 07/11/2019           Fix Unit# problem in Sdx_MergeSAdr_LDR().
 * 07/15/2019 19.0.3    Fix LEGAL in Merge_Roll() by adding "TR" before tract number.
 * 07/17/2019 19.0.4    Fix HSENO in Sdx_MergeSAdr_LDR() to avoid duplicate HSESUB.  
 *                      Add Sdx_ParseAdr() to replace parseAdrNSD(). 
 *                      Modify Sdx_MergeGrec() to use only records that are newer than lien date.  
 * 07/24/2019 19.0.5    Modify Sdx_MergeSAdr_LDR() to fix UnitNo & Hseno issue.
 * 10/07/2019 19.2.3    Set TaxYear = Def_Date.  If Def_Date not available, set it to current lien year.
 * 10/30/2019 19.3.3    Add Sdx_CreateTaxDist() to create Sdx_TaxDist.txt
 * 12/08/2019 19.5.10   Modify Sdx_MergeOwner() to parse owner to name1 & name2 when "<LF>" is present
 * 03/09/2020 19.8.3    Remove MERG_XFER option since no longer needed
 * 06/09/2020 19.10.1   Fix bug in Sdx_MergeSAdr() that skips street name starts with a digit.
 * 07/13/2020 20.1.5    Modify Sdx_MergeLien() to bypass MergeOwner() if input is blank.
 * 07/21/2020 20.2.3    Add -Xa & Sdx_ConvStdChar() to generate MI module in CDEXTR
 * 10/08/2020 20.2.14   Add CreateTaxDist option in INI file to create Sdx_TaxDist.txt file
 * 10/30/2020 20.4.1    Modify Sdx_MergeRoll() & Sdx_MergeLien() to populate PQZoning.
 * 11/18/2020 20.5.2.1  Fix situs UNIT# in Sdx_MergeSAdr_LDR().  Fix LONGLONG issue in Sdx_MergeLien().
 * 12/08/2020 20.5.5    Fix bug in Sdx_MergeSAdr_LDR().
 * 02/19/2021 20.7.7    Modify Sdx_MergeRoll() to reset TRANSFER before updating it.
 * 08/11/2021 21.1.2    Modify Sdx_MergeChar() to set LotSqft=0 when greater than 999,999,999.
 * 12/02/2021 21.4.3    Modify Sdx_MergeMAdr() to include PMB in POBox mail addr.
 * 01/18/2022 21.4.9    Modify Sdx_MergeChar() to clear H_Bath if not present.
 * 03/20/2022 21.8.4    Modify Sdx_MergeMAdr() to move M_StrSub to M_UnitNo so it can be export to bulk file.
 *                      Bug fix in Sdx_MergeSAdr().
 * 03/22/2022 21.8.5    Remove -Fc option since it's no longer needed.
 * 04/26/2022 21.9.0    Use acGrBkTmpl as backup folder for GrGr files. 
 * 06/21/2022 21.9.4    Fix Sdx_MergeSAdr() to populate S_ZIP only if it starts with '9'
 * 06/30/2022 22.0.0    Modify Sdx_MergeOwner() to remove known bad char 0xAC.  Modify Sdx_Load_LDR()
 *                      to check for CSV input and call appropriate processing function.
 * 09/28/2022 22.2.5    Modify Sdx_MergeSAdr() to populate HSE_NO when update S_STRNUM with M_STRNUM.
 * 05/28/2023 22.8.2    Modify Sdx_Load_TaxBase(), Sdx_ParseTaxBase() to check for bad TRA and show it in log file.
 * 06/09/2023 22.8.4    Rename Sdx_GrGr_Xref to GrGr_Xref to conform with other counties.
 * 01/11/2024 23.5.5    Comment out all unused ASR related functions.  Fix M_UNITNOX in Sdx_MergeMAdr().
 * 05/21/2024 23.8.3    Modify Sdx_MergeMAdr() to use HseNo to populate M_STRNUM.
 * 07/09/2024 24.0.1    Modify Sdx_MergeLien() to add ExeType.
 * 02/05/2025 24.5.1    Modify Sdx_Load_Roll() to filter out "INFORMATION ONLY records.
 *
 *****************************************************************************/

#include "stdafx.h"
#include "CountyInfo.h"
#include "Prodlib.h"
#include "R01.h"
#include "RecDef.h"
#include "Tables.h"
#include "Logs.h"
#include "LoadDef.h"
#include "Utils.h"
#include "XlatTbls.h"
#include "doSort.h"
#include "doOwner.h"
#include "doGrGr.h"
#include "formatApn.h"
#include "SaleRec.h"
#include "CharRec.h"
#include "LoadOne.h"
#include "Usecode.h"
#include "Update.h"
#include "LOExtrn.h"
#include "PQ.h"
#include "doZip.h"
#include "Situs.h"
#include "MergeSdx.h"
#include "Tax.h"
#include "SqlExt.h"
#include "LoadValue.h"

static   LU_ENTRY tblAttr[MAX_ATTR_ENTRIES];
static   FILE     *fdChar, *fdCChr, *fdDetail, *fdAgency, *fdProp8;
static   long     lCharSkip, lSaleSkip, lCChrSkip, lGrGrSkip, lGrGrMatch, lProp8Skip, lBadTRA;
static   long     lCharMatch, lSaleMatch, lCChrMatch, lSeqNum, lUseMailCity, lUseGis, lProp8Match;
static   char     acProp8File[_MAX_PATH];

SDX_DTYPE asDocType[]=
{
   '0',"UNRESEARCHED        ",
   '1',"REGULAR OWNER CHANGE",
   '2',"QUIT CLAIM          ",
   '3',"UNRECORDED DEED     ",
   '4',"DEATH CERTIFICATE   ",
   '5',"UNRECORD DEATH CERT.",
   '6',"OTHER               ",
   '7',"UNKNOWN OR MULT.DOCS",
   '8',"RECORDED CONTRACT   ",
   '9',""
};

static   hlAdo    dbTra;
static   hlAdoRs  rsTra;

extern   hlAdo dbLoadConn;

/******************************************************************************/

int Sdx_GetTaxInfo(TAXAGENCY pAgency[], char *pTRA)
{
   char     acTmp[256];
   int      iIdx=0;

   CString  sFundNum, sAgency, sTaxRate;

   //sprintf(acTmp, "SELECT * FROM v_Sdx_Rate WHERE TRA=%s", pTRA);
   sprintf(acTmp, "SELECT * FROM Sdx_Rate WHERE TRA=%s", pTRA);
   try
   {
      rsTra.Open(dbTra, acTmp);

      while (rsTra.next())
      {
         sFundNum = rsTra.GetItem("FUNDNUM");
         sAgency = rsTra.GetItem("AGENCY");
         sTaxRate = rsTra.GetItem("RATE");
         strcpy(pAgency[iIdx].Code, sFundNum);
         strcpy(pAgency[iIdx].Agency, sAgency.TrimRight());
         strcpy(pAgency[iIdx].TaxRate, sTaxRate);
         iIdx++;
      }
   } AdoCatch(e)
   {
      LogMsg("***** Error searching %s (%s)", acTmp, ComError(e));
   }
   rsTra.Close();

   return iIdx;
}

/********************************* Sdx_FormatDoc *****************************
 *
 *  - DocNum format : input as 999999
 *    + Before 2002 : 0999999
 *    + 2002-present: X999999
 *      X = 1 if sale occurs after August and first digit of DocNum <= 5
 *      X = 0 otherwise
 *
 * Notes: This logic may need adjustment in a later date when SDX is growing
 *        bigger than 1.2 million parcels.  
 *        This will fail since 2009 has only about 40000 doc
 *
 *****************************************************************************/

int Sdx_FormatDoc(char *pResult, char *pDocNum, char *pDocDate)
{
   //long  lYear;

   memcpy(pResult+1, pDocNum, RSIZ_REC_DOCNUM);
   *pResult = '0';
   *(pResult+RSIZ_REC_DOCNUM+1) = 0;

   // Remove by SPN 02/04/2010
   //lYear = atoin(pDocDate, 4);
   //if ((lYear >= 2002) && (memcmp(pDocDate+4, "08", 2) > 0) )
   //{
   //   if (*pDocNum <= 5)
   //      *pResult = '1';
   //}

   return 0;
}

/******************************** Sdx_MergeOwner *****************************
 *
 * Names are separated by '*' and ended by '\'.  Within a name, actual name starts
 * after '#'.  Names are mostly last name first and seemed to be consistent.
 * If multi names are found, keep first two as Name1 and Name2 separate and
 * ignore the rest.
 * 1) Do not parse if MaritalStatus is "CO", "LF"
 * 2) Break owner into name1 & name2 if it contains "<LF>"
 *
 *****************************************************************************/

void Sdx_MergeOwner(char *pOutbuf, char *pRollRec)
{
   int   iTmp, iRet;
   char  acOwner1[128], acOwners[128], acTmp[128];
   char  *pTmp, *pTmp1, *pName2, *pName3;
   bool  bCorp=false;

   SDX_ROLL *pRec;
   SDX_NAME sNames[3];
   OWNER    myOwner;

   pRec = (SDX_ROLL *)pRollRec;

   // Clear output buffer if needed
   removeNames(pOutbuf, false);

   memset(&sNames, 0, sizeof(SDX_NAME)*3);

   // Initialize
   memcpy(acOwner1, pRec->OwnerName, RSIZ_OWNER_NAME);
   myTrim(acOwner1, RSIZ_OWNER_NAME);

   // Find name ending char and terminate it
   pTmp = strchr(acOwner1, '\\');
   if (pTmp)
      *pTmp = 0;

   // Adding space to '&' if needed
   iTmp = 0;
   pTmp = acOwner1;
   while (*pTmp)
   {
      if (*pTmp == '&' && acTmp[iTmp-1] != ' ')
      {
         acTmp[iTmp++] = ' ';
         acTmp[iTmp++] = '&';
         acTmp[iTmp++] = ' ';
      } else
         acTmp[iTmp++] = *pTmp;
      pTmp++;
   }
   acTmp[iTmp] = 0;

   // Find vesting
   updateVesting(myCounty.acCntyCode, acOwner1, pOutbuf+OFF_VEST, pOutbuf+OFF_ETAL_FLG);

   // Check for multiple names
   pTmp = strchr(acTmp, '*');
   if (pTmp)
   {
      *pTmp++ = 0;
      memcpy(sNames[1].M_Flag, pTmp, 4);
      pName2 = strchr(pTmp, '#');
      if (pName2)
      {
         pTmp = strchr(pName2, '*');
         if (pTmp)
         {
            *pTmp++ = 0;
            memcpy(sNames[2].M_Flag, pTmp, 4);
            pName3 = strchr(pTmp, '#');
            if (pName3)
               strcpy(sNames[2].Name, pName3+1);
         }
         strcpy(sNames[1].Name, pName2+1);
      }
   }

   // Find Name1
   pTmp = strchr(acTmp, '#');
   if (pTmp)
      strcpy(sNames[0].Name, pTmp+1);
   else
      strcpy(sNames[0].Name, acTmp);

   // Check for <LF>
   if (!memcmp(pRec->Matital_Status, "LF", 2))
   {
      if (pTmp = strstr(sNames[0].Name, "<LF>"))
      {
         *pTmp = 0;
         pTmp += 5;
         vmemcpy(pOutbuf+OFF_NAME2, pTmp, SIZ_NAME2);
      }
      vmemcpy(pOutbuf+OFF_NAME_SWAP, sNames[0].Name, SIZ_NAME_SWAP);
      vmemcpy(pOutbuf+OFF_NAME1, sNames[0].Name, SIZ_NAME1);
      memcpy(pOutbuf+OFF_VEST, pRec->Matital_Status, 2);
      return;
   } else if (pTmp = strstr(sNames[0].Name, "<LF>"))
   {
      *pTmp = 0;
      pTmp += 5;
      if (*pTmp > ' ' && strcmp(pTmp, "ET AL"))
         vmemcpy(pOutbuf+OFF_NAME2, pTmp, SIZ_NAME2);
      //else if (sNames[1].Name[0] > ' ')
      //   vmemcpy(pOutbuf+OFF_NAME2, sNames[1].Name, SIZ_NAME2);

      vmemcpy(pOutbuf+OFF_NAME_SWAP, sNames[0].Name, SIZ_NAME_SWAP);
      vmemcpy(pOutbuf+OFF_NAME1, sNames[0].Name, SIZ_NAME1);
      memcpy(pOutbuf+OFF_VEST, "LF", 2);
      return;
   }

#ifdef _DEBUG
   // 1031160600, 1082511700
   //if (!memcmp(pRec->Apn, "1082511700", 8))
   //   iTmp = 1;
#endif

   // Check for CO
   if (!memcmp(pRec->Matital_Status, "CO", 2))
   {
      // Remove DBA
      if (pTmp = strstr(sNames[0].Name, "<DBA"))
      {
         *pTmp++ = 0;
         iTmp = strlen(pTmp);
         vmemcpy(pOutbuf+OFF_DBA, pTmp, SIZ_DBA, iTmp-1);
      }

      vmemcpy(pOutbuf+OFF_NAME_SWAP, sNames[0].Name, SIZ_NAME_SWAP);
      vmemcpy(pOutbuf+OFF_NAME1, sNames[0].Name, SIZ_NAME1);
      memcpy(pOutbuf+OFF_VEST, pRec->Matital_Status, 2);
      return;
   }

   // Check for PA
   if (!memcmp(pRec->Matital_Status, "PA", 2) ||
       !memcmp(pRec->Matital_Status, "WE", 2) ||
       !memcmp(pRec->Matital_Status, "RE", 2))
   {
      vmemcpy(pOutbuf+OFF_NAME_SWAP, sNames[0].Name, SIZ_NAME_SWAP);
      vmemcpy(pOutbuf+OFF_NAME1, sNames[0].Name, SIZ_NAME1);
      memcpy(pOutbuf+OFF_VEST, pRec->Matital_Status, 2);
      return;
   }

   // Check for <PF> Purchase From- drop everything after this
   if (pTmp = strstr(sNames[0].Name, "<PF>"))
      *pTmp = 0;
   if (sNames[1].Name[0] > ' ' && (pTmp = strstr(sNames[1].Name, "<PF>")) )
      *pTmp = 0;

   // Check for <LE> Life Estate - drop everything after this
   if (pTmp = strstr(sNames[0].Name, "<LE>"))
      *pTmp = 0;
   if (sNames[1].Name[0] > ' ' && (pTmp = strstr(sNames[1].Name, "<LE>")) )
      *pTmp = 0;

   // Check for <LF> Fease From - drop everything after this
   if (pTmp = strstr(sNames[0].Name, "<LF>"))
      *pTmp = 0;
   if (sNames[1].Name[0] > ' ' && (pTmp = strstr(sNames[1].Name, "<LE>")) )
      *pTmp = 0;

   // Check for <
   if (pTmp = strchr(sNames[0].Name, '<'))
      *pTmp = 0;

   if ((pTmp = strstr(sNames[0].Name, " ET AL")) || (pTmp = strstr(sNames[0].Name, " ETAL")) )
      *pTmp = 0;
   if ((pTmp = strstr(sNames[1].Name, " ET AL")) || (pTmp = strstr(sNames[1].Name, " ETAL")) )
      *pTmp = 0;

   // Special case - remove comma at beginning of name
   if (sNames[0].Name[0] == ',')
   {
      strcpy(acOwners, &sNames[0].Name[1]);       
      strcpy(&sNames[0].Name[0], acOwners);
   } else
      strcpy(acOwners, sNames[0].Name);

   // Save Name1 - Make sure Name1 is not longer than max size
   // by removing one word at a time from the end.
   iTmp = strlen(acOwners);
   if (iTmp > SIZ_NAME1)
   {
      // First try to chop name 3 and beyond
      if (pTmp = strchr(acOwners, '&'))
      {
         if (pTmp1 = strchr(pTmp+1, '&'))
            *pTmp1 = 0;
      }

      // Then try to remove words
      iTmp = strlen(acOwners);
      if (iTmp > SIZ_NAME1)
      {
         for (iTmp=SIZ_NAME1; iTmp > 30; iTmp--)
            if (acOwners[iTmp] == ' ')
               break;
         acOwners[iTmp] = 0;
      }
      strcpy(sNames[0].Name, acOwners);
   }

   // Format swap name
   if (!memcmp(pRec->Matital_Status, "HW", 2))
   {
      splitOwner(sNames[0].Name, &myOwner, 3);
      vmemcpy(pOutbuf+OFF_NAME_SWAP, myOwner.acSwapName, SIZ_NAME_SWAP);
   } else if (!memcmp(pRec->Matital_Status, "NS", 2))
   {
      strcpy(acOwner1, sNames[0].Name);
      if (pTmp = strchr(acOwner1, '('))
         *pTmp = 0;

      if ((pTmp1=strstr(acOwner1, " SEPARATE"))  ||
          (pTmp1=strstr(acOwner1, " REVOCABLE")) ||
          (pTmp1=strstr(acOwner1, " FAMILY"))    ||
          (pTmp1=strstr(acOwner1, " RESIDUAL"))  ||
          (pTmp1=strstr(acOwner1, " LIVING"))    ||
          ((pTmp1=strstr(acOwner1, " INSURANCE")) && isdigit(*(pTmp1-1)) )
          )
      {
         //pTmp = acOwner1;
         //while (pTmp < pTmp1 && !isdigit(*(pTmp)))
         //   pTmp++;
         //if (pTmp < pTmp1)
         //{
         //   --pTmp;
         //   *pTmp = 0;
         //} else
         //   *pTmp1 = 0;
         bCorp = true;
      } else if ((pTmp1=strstr(acOwner1, " SURVIVORS")) ||
                 (pTmp1=strstr(acOwner1, " DECEDENTS")) ||
                 (pTmp1=strstr(acOwner1, " INTERVIVOS")) ||
                 (pTmp1=strstr(acOwner1, " INTER VIVOS")) )
      {
         *pTmp1 = 0;
      } else if ((pTmp1=strstr(acOwner1, " EST OF")) ||
                 (pTmp1=strstr(acOwner1, " ESTATE")) ||
                 (pTmp1=strstr(acOwner1, " TRUST")))
      {
         //pTmp = acOwner1;
         //while (pTmp < pTmp1 && !isdigit(*(pTmp)))
         //   pTmp++;
         //if (pTmp < pTmp1)
         //{
         //   --pTmp;
         //   *pTmp = 0;
         //} else
         //   *pTmp1 = 0;
         bCorp = true;
      } else if (pTmp1=strstr(acOwner1, " PROFIT SHARING"))
      {
         *pTmp1 = 0;
      }

      if (bCorp)
      {
         vmemcpy(pOutbuf+OFF_NAME_SWAP, sNames[0].Name, SIZ_NAME_SWAP);
      } else
      {
         iRet = splitOwner(acOwner1, &myOwner, 3);
         if (iRet < 0)
            vmemcpy(pOutbuf+OFF_NAME_SWAP, sNames[0].Name, SIZ_NAME_SWAP);
         else
            vmemcpy(pOutbuf+OFF_NAME_SWAP, myOwner.acSwapName, SIZ_NAME_SWAP);
      }
   } else
   {
      replChar(sNames[0].Name, 0xAC, 0x20);
      splitOwner(sNames[0].Name, &myOwner, 3);
      vmemcpy(pOutbuf+OFF_NAME_SWAP, myOwner.acSwapName, SIZ_NAME_SWAP);
   }

   // Check Name2
   if (sNames[1].Name[0])
   {
      if (strcmp(sNames[1].Name, acOwners))
         vmemcpy(pOutbuf+OFF_NAME2, sNames[1].Name, SIZ_NAME2);
      else
         lDupOwners++;
   }

   // Name1
   vmemcpy(pOutbuf+OFF_NAME1, acOwners, SIZ_NAME1);

   if (memcmp(pRec->Matital_Status, "NSNS", 4))
   {
      if (memcmp(pRec->Matital_Status, "NS", 2) && memcmp(pRec->Owner_Status, "NS", 2))
         memcpy(pOutbuf+OFF_VEST, pRec->Matital_Status, 4);
      else if (memcmp(pRec->Matital_Status, "NS", 2))
         memcpy(pOutbuf+OFF_VEST, pRec->Matital_Status, 2);
   }
}

void Sdx_MergeOwner_LDR(char *pOutbuf, char *pOwnerName, char *pOwnerStatus, char *pMaritalStatus)
{
   int   iTmp, iRet;
   char  acOwner1[128], acOwners[128], acTmp[128];
   char  *pTmp, *pTmp1, *pName2, *pName3;
   bool  bCorp=false;

   SDX_NAME sNames[3];
   OWNER    myOwner;

   // Clear output buffer if needed
   removeNames(pOutbuf, false);

   memset(&sNames, 0, sizeof(SDX_NAME)*3);

   // Initialize
   strcpy(acOwner1, pOwnerName);

   // Find name ending char and terminate it
   pTmp = strchr(acOwner1, '\\');
   if (pTmp)
      *pTmp = 0;

   // Adding space to '&' if needed
   iTmp = 0;
   pTmp = acOwner1;
   while (*pTmp)
   {
      if (*pTmp == '&' && acTmp[iTmp-1] != ' ')
      {
         acTmp[iTmp++] = ' ';
         acTmp[iTmp++] = '&';
         acTmp[iTmp++] = ' ';
      } else
         acTmp[iTmp++] = *pTmp;
      pTmp++;
   }
   acTmp[iTmp] = 0;

   // Find vesting
   if (!memcmp(pMaritalStatus, "CO", 2) || 
       !memcmp(pMaritalStatus, "PA", 2) ||
       !memcmp(pMaritalStatus, "WE", 2) ||
       !memcmp(pMaritalStatus, "RE", 2) ||
       !memcmp(pMaritalStatus, "LF", 2) )
      memcpy(pOutbuf+OFF_VEST, pMaritalStatus, 2);
   else
      updateVesting(myCounty.acCntyCode, acOwner1, pOutbuf+OFF_VEST, pOutbuf+OFF_ETAL_FLG);

   // Check for multiple names
   pTmp = strchr(acTmp, '*');
   if (pTmp)
   {
      *pTmp++ = 0;
      memcpy(sNames[1].M_Flag, pTmp, 4);
      pName2 = strchr(pTmp, '#');
      if (pName2)
      {
         pTmp = strchr(pName2, '*');
         if (pTmp)
         {
            *pTmp++ = 0;
            memcpy(sNames[2].M_Flag, pTmp, 4);
            pName3 = strchr(pTmp, '#');
            if (pName3)
               strcpy(sNames[2].Name, pName3+1);
         }
         strcpy(sNames[1].Name, pName2+1);
      }
   }

   // Find Name1
   pTmp = strchr(acTmp, '#');
   if (pTmp)
      strcpy(sNames[0].Name, pTmp+1);
   else
      strcpy(sNames[0].Name, acTmp);

   // Check for <LF> (lease from)
   if (!memcmp(pMaritalStatus, "LF", 2))
   {
      if (pTmp = strstr(sNames[0].Name, "<LF>"))
         *pTmp = 0;

      vmemcpy(pOutbuf+OFF_NAME_SWAP, sNames[0].Name, SIZ_NAME_SWAP);
      vmemcpy(pOutbuf+OFF_NAME1, sNames[0].Name, SIZ_NAME1);
      return;
   }

#ifdef _DEBUG
   // 1031160600, 1082511700
   //if (!memcmp(pRec->Apn, "1082511700", 8))
   //   iTmp = 1;
#endif

   // Check for CO
   if (!memcmp(pMaritalStatus, "CO", 2))
   {
      // Remove DBA
      if (pTmp = strstr(sNames[0].Name, "<DBA"))
      {
         *pTmp++ = 0;
         iTmp = strlen(pTmp);
         vmemcpy(pOutbuf+OFF_DBA, pTmp, SIZ_DBA, iTmp-1);
      }

      vmemcpy(pOutbuf+OFF_NAME_SWAP, sNames[0].Name, SIZ_NAME_SWAP);
      vmemcpy(pOutbuf+OFF_NAME1, sNames[0].Name, SIZ_NAME1);
      return;
   }

   // Check for PA
   if (!memcmp(pMaritalStatus, "PA", 2) ||
       !memcmp(pMaritalStatus, "WE", 2) ||
       !memcmp(pMaritalStatus, "RE", 2))
   {
      vmemcpy(pOutbuf+OFF_NAME_SWAP, sNames[0].Name, SIZ_NAME_SWAP);
      vmemcpy(pOutbuf+OFF_NAME1, sNames[0].Name, SIZ_NAME1);
      return;
   }

   // Check for <PF> Purchase From- drop everything after this
   if (pTmp = strstr(sNames[0].Name, "<PF>"))
      *pTmp = 0;
   if (sNames[1].Name[0] > ' ' && (pTmp = strstr(sNames[1].Name, "<PF>")) )
      *pTmp = 0;

   // Check for <LE> Life Estate - drop everything after this
   if (pTmp = strstr(sNames[0].Name, "<LE>"))
      *pTmp = 0;
   if (sNames[1].Name[0] > ' ' && (pTmp = strstr(sNames[1].Name, "<LE>")) )
      *pTmp = 0;

   // Check for <LF> Fease From - drop everything after this
   if (pTmp = strstr(sNames[0].Name, "<LF>"))
      *pTmp = 0;
   if (sNames[1].Name[0] > ' ' && (pTmp = strstr(sNames[1].Name, "<LE>")) )
      *pTmp = 0;

   // Check for <
   if (pTmp = strchr(sNames[0].Name, '<'))
      *pTmp = 0;

   if ((pTmp = strstr(sNames[0].Name, " ET AL")) || (pTmp = strstr(sNames[0].Name, " ETAL")) )
      *pTmp = 0;
   if ((pTmp = strstr(sNames[1].Name, " ET AL")) || (pTmp = strstr(sNames[1].Name, " ETAL")) )
      *pTmp = 0;

   // Special case - remove comma at beginning of name
   if (sNames[0].Name[0] == ',')
   {
      strcpy(acOwners, &sNames[0].Name[1]);       
      strcpy(&sNames[0].Name[0], acOwners);
   } else
      strcpy(acOwners, sNames[0].Name);

   // Save Name1 - Make sure Name1 is not longer than max size
   // by removing one word at a time from the end.
   iTmp = strlen(acOwners);
   if (iTmp > SIZ_NAME1)
   {
      // First try to chop name 3 and beyond
      if (pTmp = strchr(acOwners, '&'))
      {
         if (pTmp1 = strchr(pTmp+1, '&'))
            *pTmp1 = 0;
      }

      // Then try to remove words
      iTmp = strlen(acOwners);
      if (iTmp > SIZ_NAME1)
      {
         for (iTmp=SIZ_NAME1; iTmp > 30; iTmp--)
            if (acOwners[iTmp] == ' ')
               break;
         acOwners[iTmp] = 0;
      }
      strcpy(sNames[0].Name, acOwners);
   }

   // Format swap name
   if (!memcmp(pMaritalStatus, "HW", 2))
   {
      splitOwner(sNames[0].Name, &myOwner, 3);
      vmemcpy(pOutbuf+OFF_NAME_SWAP, myOwner.acSwapName, SIZ_NAME_SWAP);
   } else if (!memcmp(pMaritalStatus, "NS", 2))
   {
      strcpy(acOwner1, sNames[0].Name);
      if (pTmp = strchr(acOwner1, '('))
         *pTmp = 0;

      if ((pTmp1=strstr(acOwner1, " SEPARATE"))  ||
          (pTmp1=strstr(acOwner1, " REVOCABLE")) ||
          (pTmp1=strstr(acOwner1, " FAMILY"))    ||
          (pTmp1=strstr(acOwner1, " RESIDUAL"))  ||
          (pTmp1=strstr(acOwner1, " LIVING"))    ||
          ((pTmp1=strstr(acOwner1, " INSURANCE")) && isdigit(*(pTmp1-1)) )
          )
      {
         bCorp = true;
      } else if ((pTmp1=strstr(acOwner1, " SURVIVORS")) ||
                 (pTmp1=strstr(acOwner1, " DECEDENTS")) ||
                 (pTmp1=strstr(acOwner1, " INTERVIVOS")) ||
                 (pTmp1=strstr(acOwner1, " INTER VIVOS")) )
      {
         *pTmp1 = 0;
      } else if ((pTmp1=strstr(acOwner1, " EST OF")) ||
                 (pTmp1=strstr(acOwner1, " ESTATE")) ||
                 (pTmp1=strstr(acOwner1, " TRUST")))
      {
         bCorp = true;
      } else if (pTmp1=strstr(acOwner1, " PROFIT SHARING"))
      {
         *pTmp1 = 0;
      }

      if (bCorp)
      {
         vmemcpy(pOutbuf+OFF_NAME_SWAP, sNames[0].Name, SIZ_NAME_SWAP);
      } else
      {
         iRet = splitOwner(acOwner1, &myOwner, 3);
         if (iRet < 0)
            vmemcpy(pOutbuf+OFF_NAME_SWAP, sNames[0].Name, SIZ_NAME_SWAP);
         else
            vmemcpy(pOutbuf+OFF_NAME_SWAP, myOwner.acSwapName, SIZ_NAME_SWAP);
      }
   } else
   {
      splitOwner(sNames[0].Name, &myOwner, 3);
      vmemcpy(pOutbuf+OFF_NAME_SWAP, myOwner.acSwapName, SIZ_NAME_SWAP);
   }

   // Check Name2
   if (sNames[1].Name[0])
   {
      if (strcmp(sNames[1].Name, acOwners))
         vmemcpy(pOutbuf+OFF_NAME2, sNames[1].Name, SIZ_NAME2);
      else
         lDupOwners++;
   }

   // Name1
   vmemcpy(pOutbuf+OFF_NAME1, acOwners, SIZ_NAME1);

   if (*(pOutbuf+OFF_VEST) == ' ')
   {
      if (memcmp(pMaritalStatus, "NS", 2) && memcmp(pOwnerStatus, "NS", 2))
      {
         memcpy(pOutbuf+OFF_VEST, pMaritalStatus, 2);
         memcpy(pOutbuf+OFF_VEST+2, pOwnerStatus, 2);
      } else if (memcmp(pMaritalStatus, "NS", 2))
         memcpy(pOutbuf+OFF_VEST, pMaritalStatus, 2);
   }
}

/******************************************************************************/

//void Sdx_AsrMergeOwner(char *pOutbuf, char *pRollRec)
//{
//   int   iTmp, iRet;
//   char  acOwner1[128], acOwners[128], acTmp[128];
//   char  *pTmp, *pTmp1, *pName2, *pName3;
//
//   SDX_ROLL *pRec;
//   SDX_NAME sNames[3];
//   OWNER    myOwner;
//
//   pRec = (SDX_ROLL *)pRollRec;
//
//   // Clear output buffer if needed
//   memset(pOutbuf+AOFF_NAME1_MASSAGE, ' ', ASIZ_NAME1_MASSAGE*3);
//   *(pOutbuf+AOFF_NAME1_FLAG) = ' ';
//   *(pOutbuf+AOFF_NAME2_FLAG) = ' ';
//   memset(&sNames, 0, sizeof(SDX_NAME)*3);
//
//   // Initialize
//   memcpy(acOwner1, pRec->OwnerName, RSIZ_OWNER_NAME);
//   myTrim(acOwner1, RSIZ_OWNER_NAME);
//
//   // Find name ending char and terminate it
//   pTmp = strchr(acOwner1, '\\');
//   if (pTmp)
//      *pTmp = 0;
//
//   // Adding space to '&' if needed
//   iTmp = 0;
//   pTmp = acOwner1;
//   while (*pTmp)
//   {
//      if (*pTmp == '&' && acTmp[iTmp-1] != ' ')
//      {
//         acTmp[iTmp++] = ' ';
//         acTmp[iTmp++] = '&';
//         acTmp[iTmp++] = ' ';
//      } else
//         acTmp[iTmp++] = *pTmp;
//      pTmp++;
//   }
//   acTmp[iTmp] = 0;
//
//   // Check for multiple names
//   pTmp = strchr(acTmp, '*');
//   if (pTmp)
//   {
//      *pTmp++ = 0;
//      memcpy(sNames[1].M_Flag, pTmp, 4);
//      pName2 = strchr(pTmp, '#');
//      if (pName2)
//      {
//         pTmp = strchr(pName2, '*');
//         if (pTmp)
//         {
//            *pTmp++ = 0;
//            memcpy(sNames[2].M_Flag, pTmp, 4);
//            pName3 = strchr(pTmp, '#');
//            if (pName3)
//               strcpy(sNames[2].Name, pName3+1);
//         }
//         strcpy(sNames[1].Name, pName2+1);
//      }
//   }
//
//   // Find Name1
//   pTmp = strchr(acTmp, '#');
//   if (pTmp)
//      strcpy(sNames[0].Name, pTmp+1);
//   else
//      strcpy(sNames[0].Name, acTmp);
//
//   /*
//      NAME 1 FLAG:
//                        P=NM1 IS PERSON
//                        C=NM1 IS CORP/COMP
//                        E=NM1 IS LIFE ESTATE
//                        O=NM1 IS CONTR. PURCH
//      NAME 2 FLAG:
//                        P=NM2 IS PERSON
//                        C=NM2 IS CORP/COMP
//                        D=NM2 IS DBA
//                        L=NM2 IS LEASED FROM
//   */
//
//   // Remove DBA
//   if ((pTmp = strstr(sNames[0].Name, "<DBA")) || (pTmp = strstr(sNames[0].Name, "<DVA")) )
//   {
//      *pTmp = 0;
//      *(pOutbuf+AOFF_NAME2_FLAG) = 'D';
//   }
//
//   // Remove <AKA>
//   if (pTmp = strstr(sNames[0].Name, "<AKA"))
//      *pTmp = 0;
//
//   // Check for <PF> - drop everything after this
//   // <PF> is found on both Name1 and Name2
//   if (pTmp = strstr(sNames[0].Name, "<PF>"))
//   {
//      *pTmp = 0;
//      *(pOutbuf+AOFF_NAME1_FLAG) = 'O';
//   }
//   if (sNames[1].Name[0] > ' ' && (pTmp = strstr(sNames[1].Name, "<PF>")) )
//   {
//      *pTmp = 0;
//      *(pOutbuf+AOFF_NAME2_FLAG) = 'O';
//   }
//
//   // Check for <LE> - drop everything after this
//   if (pTmp = strstr(sNames[0].Name, "<LE>"))
//   {
//      *pTmp = 0;
//      *(pOutbuf+AOFF_NAME1_FLAG) = 'E';
//   }
//   if (sNames[1].Name[0] > ' ' && (pTmp = strstr(sNames[1].Name, "<LE>")) )
//   {
//      *pTmp = 0;
//      *(pOutbuf+AOFF_NAME2_FLAG) = 'E';
//   }
//
//   // Check for <LF> - drop everything after this
//   if (pTmp = strstr(sNames[0].Name, "<LF>"))
//   {
//      *pTmp = 0;
//      *(pOutbuf+AOFF_NAME1_FLAG) = 'L';
//   }
//   // So far <LF> only found in Name1, but check Name2 to be safe
//   if (sNames[1].Name[0] > ' ' && (pTmp = strstr(sNames[1].Name, "<LF>")) )
//   {
//      *pTmp = 0;
//      *(pOutbuf+AOFF_NAME2_FLAG) = 'L';
//   }
//
//   // Check for <
//   if (pTmp = strchr(sNames[0].Name, '<'))
//      *pTmp = 0;
//
//   if ((pTmp = strstr(sNames[0].Name, " ET AL")) || (pTmp = strstr(sNames[0].Name, " ETAL")) )
//      *pTmp = 0;
//   if ((pTmp = strstr(sNames[1].Name, " ET AL")) || (pTmp = strstr(sNames[1].Name, " ETAL")) )
//      *pTmp = 0;
//
//   // Check for CO
//   if (!memcmp(pRec->Matital_Status, "CO", 2))
//   {
//      *(pOutbuf+AOFF_NAME1_FLAG) = 'C';
//
//      iTmp = strlen(sNames[0].Name);
//      if (iTmp > ASIZ_NAME1_MASSAGE) iTmp = ASIZ_NAME1_MASSAGE;
//      memcpy(pOutbuf+AOFF_NAME1_MASSAGE, sNames[0].Name, iTmp);
//      memcpy(pOutbuf+AOFF_NAME1_NORM, sNames[0].Name, iTmp);
//      return;
//   }
//
//   // Check for PA
//   if (!memcmp(pRec->Matital_Status, "PA", 2) ||
//       !memcmp(pRec->Matital_Status, "WE", 2) ||
//       !memcmp(pRec->Matital_Status, "RE", 2))
//   {
//      iTmp = strlen(sNames[0].Name);
//      if (iTmp > ASIZ_NAME1_MASSAGE) iTmp = ASIZ_NAME1_MASSAGE;
//      memcpy(pOutbuf+AOFF_NAME1_MASSAGE, sNames[0].Name, iTmp);
//      memcpy(pOutbuf+AOFF_NAME1_NORM, sNames[0].Name, iTmp);
//      return;
//   }
//
//   // Populate Name1
//   strcpy(acOwners, sNames[0].Name);
//
//   // Format swap name
//   if (!memcmp(pRec->Matital_Status, "HW", 2))
//   {
//      splitOwner(sNames[0].Name, &myOwner, 3);
//      iTmp = strlen(myOwner.acName1);
//      if (iTmp > ASIZ_NAME1_NORM)
//         iTmp = ASIZ_NAME1_NORM;
//      memcpy(pOutbuf+AOFF_NAME1_NORM, myOwner.acName1, iTmp);
//      *(pOutbuf+AOFF_NAME1_FLAG) = 'P';
//   } else if (!memcmp(pRec->Matital_Status, "NS", 2))
//   {
//      strcpy(acOwner1, sNames[0].Name);
//      if (pTmp = strchr(acOwner1, '('))
//         *pTmp = 0;
//
//      if ((pTmp1=strstr(acOwner1, " FAMILY")) ||
//          (pTmp1=strstr(acOwner1, " REVOCABLE")) ||
//          (pTmp1=strstr(acOwner1, " RESIDUAL")) ||
//          (pTmp1=strstr(acOwner1, " LIVING")) ||
//          (pTmp1=strstr(acOwner1, " SEPARATE"))
//          )
//      {
//         pTmp = acOwner1;
//         while (pTmp < pTmp1 && !isdigit(*(pTmp)))
//            pTmp++;
//         if (pTmp < pTmp1)
//         {
//            --pTmp;
//            *pTmp = 0;
//         } else
//            *pTmp1 = 0;
//      } else if (pTmp1=strstr(acOwner1, " REVOC"))
//      {  // Sometimes REVOC goes before FAMILY
//         *pTmp1 = 0;
//      } else if ((pTmp1=strstr(acOwner1, " EST OF")) ||
//                 (pTmp1=strstr(acOwner1, " ESTATE")) ||
//                 (pTmp1=strstr(acOwner1, " SURVIVORS")) ||
//                 (pTmp1=strstr(acOwner1, " DECEDENTS")) ||
//                 (pTmp1=strstr(acOwner1, " INTERVIVOS")) ||
//                 (pTmp1=strstr(acOwner1, " INTER VIVOS")) )
//      {
//         *pTmp1 = 0;
//      } else if (pTmp1=strstr(acOwner1, " TRUST"))
//      {
//         pTmp = acOwner1;
//         while (pTmp < pTmp1 && !isdigit(*(pTmp)))
//            pTmp++;
//         if (pTmp < pTmp1)
//         {
//            --pTmp;
//            *pTmp = 0;
//         } else
//            *pTmp1 = 0;
//      } else if (pTmp1=strstr(acOwner1, " PROFIT SHARING"))
//      {
//         *pTmp1 = 0;
//      }
//
//      iRet = splitOwner(acOwner1, &myOwner, 3);
//      if (iRet < 0)
//      {
//         iTmp = strlen(sNames[0].Name);
//         if (iTmp > ASIZ_NAME1_NORM)
//            iTmp = ASIZ_NAME1_NORM;
//         memcpy(pOutbuf+AOFF_NAME1_NORM, sNames[0].Name, iTmp);
//      } else
//      {
//         iTmp = strlen(myOwner.acName1);
//         if (iTmp > ASIZ_NAME1_NORM)
//            iTmp = ASIZ_NAME1_NORM;
//         memcpy(pOutbuf+AOFF_NAME1_NORM, myOwner.acName1, iTmp);
//      }
//   } else
//   {
//      splitOwner(sNames[0].Name, &myOwner, 3);
//      iTmp = strlen(myOwner.acName1);
//      if (iTmp > ASIZ_NAME1_NORM)
//         iTmp = ASIZ_NAME1_NORM;
//      memcpy(pOutbuf+AOFF_NAME1_NORM, myOwner.acName1, iTmp);
//   }
//
//#ifdef _DEBUG
//   //if (!memcmp(pRollRec, "001081009", 9))
//   //   iTmp = 1;
//#endif
//
//   // Check Name2
//   if (sNames[1].Name[0])
//   {
//      iTmp = strlen(sNames[1].Name);
//      if (iTmp > ASIZ_NAME2_MASSAGE)
//         iTmp = ASIZ_NAME2_MASSAGE;
//      memcpy(pOutbuf+AOFF_NAME2_MASSAGE, sNames[1].Name, iTmp);
//   }
//
//   // Name1
//   iTmp = strlen(acOwners);
//   if (iTmp > ASIZ_NAME1_MASSAGE)
//      iTmp = ASIZ_NAME1_MASSAGE;
//   memcpy(pOutbuf+AOFF_NAME1_MASSAGE, acOwners, iTmp);
//}

/********************************* Sdx_MergeSAdr *****************************
 *
 * Notes:
 *    - Drop street name '0' or 'O' if strNum is 0.
 *    - SANDIA CREEK TER WEST
 *    - Since there is no situs city, copy it from mailing if same strname
 *      and strnum.  Otherwise, match TRA.
 *
 * Return 0 if successful, >0 if error
 *
 *****************************************************************************/

void Sdx_ParseAdr(ADR_REC *pAdrRec, char *pNum, char *pAdr)
{
   char  acAdr[128], acStr[64], *apItems[16], *pDir, *pTmp, *pTmp1;
   int   iCnt, iIdx, iSfxCode;

   // Copy addr
   strcpy(acAdr, pAdr);

   // Check for Unit#
   if (pTmp = strchr(acAdr, '#'))
   {
      // URANIA AVE #943A 943A
      if (pTmp1 = strchr(pTmp+1, ' '))
         *pTmp1 = 0;
      strcpy(pAdrRec->Unit, pTmp);
      strcpy(pAdrRec->UnitNox, pTmp);
      *pTmp = 0;
   } 

   if (pTmp = strchr(pNum, '-'))
   {
      if ((pTmp = strrchr(acAdr, ' ')) && isdigit(*(pTmp+1)))
      {
         if (*(pTmp+2) == '/')
         {
            sprintf(pAdrRec->HseNo, "%d", pAdrRec->lStrNum);
            sprintf(pAdrRec->strSub, "%.3s", pTmp+1);
         } else if (strlen(pTmp) < 6)
         {
            sprintf(pAdrRec->HseNo, "%d-%s", pAdrRec->lStrNum, pTmp+1);
         }
         *pTmp = 0;
      } else if (pTmp && *pTmp >= 'A')
      {
         strcpy(pAdrRec->Unit, pTmp);
         strcpy(pAdrRec->UnitNox, pTmp);
         *pTmp = 0;
      }
   }

   iCnt = ParseString(acAdr, ' ', 16, apItems);
   if (iCnt == 1)
   {
      acAdr[SIZ_M_STREET] = 0;
      strcpy(pAdrRec->strName, acAdr);
      return;
   }

   // Dir
   pDir = GetStrDir(apItems[iCnt-1]);
   if (pDir)
   {
      strcpy(pAdrRec->strDir, pDir);
      iCnt--;
   }

   // Check for suffix
   if (iCnt > 1)
   {
      // ex: STAGECOACH LN NORTH
      iSfxCode = GetSfxDev(apItems[iCnt-1]);
      if (iSfxCode)
      {
         strcpy(pAdrRec->strSfx, GetSfxStr(iSfxCode));
         sprintf(pAdrRec->SfxCode, "%d", iSfxCode);
         iCnt--;
      }
   }

   // Street name
   iIdx = 0;
   acStr[0] = 0;
   while (iIdx < iCnt)
   {
      strcat(acStr, apItems[iIdx++]);
      if (iIdx < iCnt)
         strcat(acStr, " ");
   }

   acStr[SIZ_M_STREET] = 0;
   strcpy(pAdrRec->strName, acStr);
}

void Sdx_MergeSAdr(char *pOutbuf, char *pRollRec)
{
   static char sLastStr[64], sLastCitySt[64], sLastCityCode[8], sLastZip[12];
   char     *pTmp, acTmp[256], acAddr1[64], acAddr2[64], acCode[32], acCity[32];
   char     sStreet[32], sUnit[32];
   int      iTmp, iCode;

   ADR_REC  sAdrRec;
   SDX_ROLL *pRec = (SDX_ROLL *)pRollRec;
   CITYZIP  sCityZip;

   // Check input
   if (pRec->S_StrName[0] < '1')
      return;

   // Situs
   acAddr2[0] = acAddr1[0] = sStreet[0] = sUnit[0] = 0;
   memset((void *)&sAdrRec, 0, sizeof(ADR_REC));
   removeSitus(pOutbuf);

   // Copy StrNum
   sAdrRec.lStrNum = atoin(pRec->S_StrNum, RSIZ_SITUS_NUMBER);
   if (sAdrRec.lStrNum > 0)
   {
      if (pRec->S_Unit[0] == '#')
         sprintf(sUnit, "%.3s", pRec->S_Unit);
      else if (pRec->S_Unit[0] >= '0')
         sprintf(sUnit, "#%.3s", pRec->S_Unit);
   }

   memcpy(acAddr1, pRec->S_StrName, RSIZ_SITUS_STREET);
   acAddr1[RSIZ_SITUS_STREET] = 0;
   if (pTmp = strchr(acAddr1, '*'))
      *pTmp = 0;
   else
      myTrim(acAddr1, RSIZ_SITUS_STREET);

   if (pTmp = strchr(acAddr1, '('))
   {
      if (*(pTmp-1) == ' ')
         *--pTmp = 0;
      else
         *pTmp = 0;
   }

   // Parse address
   //if (pTmp = strstr(acAddr1, "  "))
   //   *pTmp = 0;
   //parseAdrNSD(&sAdrRec, acAddr1);
   memcpy(acTmp, pRec->S_StrNum, RSIZ_SITUS_NUMBER);
   acTmp[RSIZ_SITUS_NUMBER] = 0;
   Sdx_ParseAdr(&sAdrRec, acTmp, acAddr1);
   strcpy(sStreet, acAddr1);

   // Check for invalid address
   if (!sAdrRec.lStrNum && (sAdrRec.strName[0] == '0' || sAdrRec.strName[0] == 'O'))
      iCode = 0;

   if (sAdrRec.lStrNum > 0)
   {
      iTmp = sprintf(acAddr1, "%ld ", sAdrRec.lStrNum);
      memcpy(pOutbuf+OFF_S_STRNUM, acAddr1, iTmp);

      // Check for range of strNum that extends into unit number
      if (pRec->S_Unit[0] == '-') 
      {
         if (isdigit(pRec->S_Unit[1]))
         {
            // 00118-20
            iTmp = sprintf(acAddr1, "%d%.3s ", sAdrRec.lStrNum, pRec->S_Unit);
         } else
         {
            // 101-292-21
            /* to be implemented
               1273003200 -> 31223-  VIA MARGARITA #1161*\
               1282904900 -> 09690-  COVEY LN 9818*\
               1291621000 -> 30460-  ANTHONY RD  1/2*\
               1320900400 -> 16160-  HIGHWAY 76       195C*\
               1361202200 -> 23463-  GRADE RD EAST 23877*\
               1431720930 -> 00904-  THE STRAND NORTH 43172092990*\
               1480151300 -> 01322-  LEMON ST  1322-221/2*\
               1500771000 -> 00318-  MYERS ST SOUTH  318-318 1/2*\
               1520111500 -> 01030-  TREMONT ST SOUTH  1030-30 1/2*\
               1520740700 -> 00100-  OCEANSIDE BLVD  110A-110B*\
               1552714000 -> 00870-  LAGUNA DR  870-872-874-876*\
               1606025700 -> 00469-  BENEVENTE DR 469*\
               1606801713 -> 04015   AVENIDA DE LA PLATA #H-1 #H-2*\
               1612314400 -> 00311-  LOS ANGELES DR WEST 1-10*\
               1617500701 -> 01205-  NATOMA WAY #WAYA*\
               1620307615 -> 00432-  EDGEHILL LN ##163*\
               1631211800 -> 00743-  OLIVE AVE  741A-743*\
               1733001600 -> 01223-  VISTA WAY EAST  1223-1385*\
               2060201600 -> 00240-  CHINQUAPIN AVE  240,242,244*\
               2161003600 -> 01748-  NOMA LN EAST CL-1748B*\
            if ((pTmp = strrchr(sStreet, ' ')) && isdigit(*(pTmp+1)))
            {
               if (*(pTmp+2) == '/')
               {
                  iTmp = sprintf(acAddr1, "%d", sAdrRec.lStrNum);
                  memcpy(pOutbuf+OFF_S_HSENO, acAddr1, iTmp);
                  vmemcpy(pOutbuf+OFF_S_STR_SUB, pTmp+1, SIZ_S_STR_SUB);
                  iTmp = sprintf(acAddr1, "%d%s ", sAdrRec.lStrNum, pTmp);
                  *pTmp = 0;
               } else if (strlen(pTmp) < 7)
               {
                  iTmp = sprintf(acAddr1, "%d-%s ", sAdrRec.lStrNum, pTmp+1);
                  *pTmp = 0;
                  // URANIA AVE #943A 943A
                  if (pTmp = strchr(sStreet, '#'))
                     *pTmp = 0;
               }
               //sAdrRec.strName[0] = 0;
            }
            */
         }
      }

      if (sAdrRec.HseNo[0] > ' ')
         vmemcpy(pOutbuf+OFF_S_HSENO, sAdrRec.HseNo, SIZ_S_HSENO);
      else
         vmemcpy(pOutbuf+OFF_S_HSENO, acAddr1, SIZ_S_HSENO);

   } else if (sAdrRec.strName[0] > '0' && *(pOutbuf+OFF_M_STREET) > '0' &&
      !memcmp(pOutbuf+OFF_M_STREET, sAdrRec.strName, strlen(sAdrRec.strName)) &&
      !memcmp(pOutbuf+OFF_M_SUFF, sAdrRec.strSfx, strlen(sAdrRec.strSfx)) )
   {
      memcpy(pOutbuf+OFF_S_STRNUM, pOutbuf+OFF_M_STRNUM, SIZ_S_STRNUM);
      memcpy(pOutbuf+OFF_S_HSENO, pOutbuf+OFF_S_STRNUM, SIZ_S_STRNUM);
      memcpy(acAddr1, pOutbuf+OFF_S_STRNUM, SIZ_S_STRNUM);
      blankRem(acAddr1, SIZ_S_STRNUM);
      strcat(acAddr1, " ");
   } else
      acAddr1[0] = 0;

   if (sAdrRec.strSub[0] > ' ')
   {
      vmemcpy(pOutbuf+OFF_S_STR_SUB, sAdrRec.strSub, SIZ_S_STR_SUB);
      strcat(acAddr1, sAdrRec.strSub);
      strcat(acAddr1, " ");
   }

   if (sAdrRec.strDir[0] > ' ')
   {
      *(pOutbuf+OFF_S_DIR) = sAdrRec.strDir[0];
      strcat(acAddr1, sAdrRec.strDir);
      strcat(acAddr1, " ");
   }

   // Street name
   if (sAdrRec.strName[0] > ' ')
   {
      vmemcpy(pOutbuf+OFF_S_STREET, sAdrRec.strName, SIZ_S_STREET);
      strcat(acAddr1, sAdrRec.strName);
      strcat(acAddr1, " ");
   //} else if (sStreet[0] > '0')
   //{
   //   vmemcpy(pOutbuf+OFF_S_STREET, sStreet, SIZ_S_STREET);
   //   strcat(acAddr1, sStreet);
   //   strcat(acAddr1, " ");
   //   LogMsg("Use: %s \tAPN=%.10s (dir=%s, Sfx=%s, Sub=%s)", sStreet, pOutbuf, sAdrRec.strDir, sAdrRec.strSfx, sAdrRec.strSub);
   //   sAdrRec.strDir[0] = 0;
   } 

   // Street suffix
   if (sAdrRec.strSfx[0] > ' ')
   {
      if (sAdrRec.SfxCode[0] > ' ')
         vmemcpy(pOutbuf+OFF_S_SUFF, sAdrRec.SfxCode, SIZ_S_SUFF);
      else
      {
         // Translate to code
         iCode = GetSfxCode(myTrim(sAdrRec.strSfx));
         if (iCode)
         {
            iTmp = sprintf(acTmp, "%d", iCode);
            memcpy(pOutbuf+OFF_S_SUFF, acTmp, iTmp);
         } else
            LogMsg0("*** Unknown suffix %s", sAdrRec.strSfx);
      }

      strcat(acAddr1, sAdrRec.strSfx);
      strcat(acAddr1, " ");
   }

   // Unit#
   if (sUnit[0] > ' ')
   {
      vmemcpy(pOutbuf+OFF_S_UNITNO,  sUnit, SIZ_S_UNITNO);
      vmemcpy(pOutbuf+OFF_S_UNITNOX, sUnit, SIZ_S_UNITNOX);
      strcat(acAddr1, sUnit);
   } else if (sAdrRec.Unit[0] > ' ')
   {
      vmemcpy(pOutbuf+OFF_S_UNITNO,  sAdrRec.Unit, SIZ_S_UNITNO);
      vmemcpy(pOutbuf+OFF_S_UNITNOX, sAdrRec.Unit, SIZ_S_UNITNOX);
      strcat(acAddr1, sAdrRec.Unit);
   } else
   {
      // Copy unit# - when no strnum
      if (pRec->S_Unit[0] > '0' || pRec->S_Unit[0] == '#')
      {
         if (pRec->S_Unit[0] == '#')
         {
            sprintf(acTmp, "%.3s", pRec->S_Unit);
         } else if (isdigit(pRec->S_Unit[0]))
         {
            // 1022221200 - 02006395OLD HWY
            sprintf(acTmp, "#%.3s", pRec->S_Unit);
         } else 
         {
            sprintf(sAdrRec.strDir, "%.3c", pRec->S_Unit[0]);
            if (pTmp = GetStrDir(myTrim(sAdrRec.strDir)))
            {
               vmemcpy(pOutbuf+OFF_S_DIR, pTmp, SIZ_S_DIR);
               acTmp[0] = 0;
            } else
            {
               acTmp[0] = 0;
            }
         }

         if (acTmp[0] > ' ')
         {
            vmemcpy(pOutbuf+OFF_S_UNITNO, &acTmp[0], SIZ_S_UNITNO);
            vmemcpy(pOutbuf+OFF_S_UNITNOX, &acTmp[0], SIZ_S_UNITNOX);
            strcat(acAddr1, acTmp);
         }
      }
   }
   memcpy(pOutbuf+OFF_S_ADDR_D, acAddr1, strlen(acAddr1));

   // state
   memcpy(pOutbuf+OFF_S_ST, "CA", 2);

#ifdef _DEBUG
   // 1012101100
   //if (!memcmp(pOutbuf, "1580102800", 8) )
   //   iTmp = 0;
#endif

   sCityZip.bVerify = 0;
   if (fdCity)
   {
      iTmp = getCityZip(pOutbuf, sCityZip.acCity, sCityZip.acZip, iApnLen);
      if (!iTmp)
      {
         City2Code(sCityZip.acCity, acCode, pOutbuf);
         memcpy(pOutbuf+OFF_S_CITY, acCode, SIZ_M_CITY);
         if (sCityZip.acZip[0] == '9')
            memcpy(pOutbuf+OFF_S_ZIP, sCityZip.acZip, strlen(sCityZip.acZip));
         iTmp = sprintf(acAddr2, "%s CA", sCityZip.acCity);
         //iTmp = sprintf(acAddr2, "%s, CA %.5s", sCityZip.acCity, pOutbuf+OFF_S_ZIP);
         vmemcpy(pOutbuf+OFF_S_CTY_ST_D, acAddr2, SIZ_S_CTY_ST_D, iTmp);  
         lUseGis++;
      }
   }

   // Matching up with mailing addr to get city name
   if (*(pOutbuf+OFF_S_STRNUM) > '0' && *(pOutbuf+OFF_S_STREET) > '0' &&
       !memcmp(pOutbuf+OFF_S_STRNUM, pOutbuf+OFF_M_STRNUM, SIZ_S_STRNUM) &&
       !memcmp(pOutbuf+OFF_S_STREET, pOutbuf+OFF_M_STREET, 15) &&
       *(pOutbuf+OFF_M_CITY) > ' ')
   {
      memcpy(acCity, pOutbuf+OFF_M_CITY, SIZ_M_CITY);
      myTrim(acCity, SIZ_M_CITY);
      City2Code(acCity, acCode, pOutbuf);

      if (sCityZip.bVerify != '1' && acCode[0] > '0')
      {
         memcpy(pOutbuf+OFF_S_CITY, acCode, SIZ_M_CITY);
         memcpy(pOutbuf+OFF_S_ZIP, pOutbuf+OFF_M_ZIP, SIZ_S_ZIP);
         iTmp = sprintf(acAddr2, "%s CA     ", acCity);
         //iTmp = sprintf(acAddr2, "%s, CA %.5s     ", acCity, pOutbuf+OFF_S_ZIP);
         vmemcpy(pOutbuf+OFF_S_CTY_ST_D, acAddr2, SIZ_S_CTY_ST_D, iTmp);  
         lUseMailCity++;
      } else if (acCode[0] > '0' && memcmp(pOutbuf+OFF_S_CITY, acCode, 3))
         LogMsg("*** Check situs city: %.3s(gis) <> %.3s(mail) for APN: %.14s", pOutbuf+OFF_S_CITY, acCode, pOutbuf);
   }
}

/*
void Sdx_MergeSAdr_Pre201305(char *pOutbuf, char *pRollRec)
{
   static char sLastStr[64], sLastCitySt[64], sLastCityCode[8], sLastZip[12];
   char     *pTmp, acTmp[256], acAddr1[64], acAddr2[64], acCode[32], acCity[32];
   int      iTmp, iCode;

   ADR_REC  sAdrRec;
   SDX_ROLL *pRec = (SDX_ROLL *)pRollRec;

   // Situs
   acAddr1[0] = 0;
   acAddr2[0] = 0;
   memset((void *)&sAdrRec, 0, sizeof(ADR_REC));
   memset(pOutbuf+OFF_S_STRNUM, ' ', SIZ_S_ADDR);
   memset(pOutbuf+OFF_S_ADDR_D, ' ', SIZ_S_ADDRB_D);

   // Copy StrNum
   sAdrRec.lStrNum = atoin(pRec->S_StrNum, RSIZ_SITUS_NUMBER);
   memcpy(acAddr1, pRec->S_StrName, RSIZ_SITUS_STREET);
   acAddr1[RSIZ_SITUS_STREET] = 0;
   if (pTmp = strchr(acAddr1, '*'))
      *pTmp = 0;
   else
      myTrim(acAddr1, RSIZ_SITUS_STREET);

   if (pTmp = strchr(acAddr1, '('))
   {
      if (*(pTmp-1) == ' ')
         *--pTmp = 0;
      else
         *pTmp = 0;
   }

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "46301010", 8))
   //   iTmp = 0;
#endif

   // Parse address
   if (pTmp = strstr(acAddr1, "  "))
   {
      *pTmp = 0;
   }
   parseAdrNSD(&sAdrRec, acAddr1);

   // Check for invalid address
   if (!sAdrRec.lStrNum && (sAdrRec.strName[0] == '0' || sAdrRec.strName[0] == 'O'))
      iCode = 0;

   if (sAdrRec.lStrNum > 0)
   {
      iTmp = sprintf(acAddr1, "%ld ", sAdrRec.lStrNum);
      memcpy(pOutbuf+OFF_S_STRNUM, acAddr1, iTmp);

      // Check for range of strNum that extends into unit number
      if (isCharIncluded(pRec->S_StrNum, '-', RSIZ_SITUS_NUMBER+RSIZ_SITUS_UNIT))
      {
         if (pRec->S_Unit[1] > ' ')
         {
            iTmp = sprintf(acAddr1, "%.*s ", RSIZ_SITUS_NUMBER+RSIZ_SITUS_UNIT, pRec->S_StrNum);
            memcpy(pOutbuf+OFF_S_HSENO, acAddr1, iTmp);
         } else
         {
            memcpy(pOutbuf+OFF_S_HSENO, acAddr1, iTmp);
         }
      } else
         memcpy(pOutbuf+OFF_S_HSENO, acAddr1, iTmp);
   } else if (sAdrRec.strName[0] > '0' && *(pOutbuf+OFF_M_STREET) > '0' &&
      !memcmp(pOutbuf+OFF_M_STREET, sAdrRec.strName, strlen(sAdrRec.strName)) )
   {
      memcpy(pOutbuf+OFF_S_STRNUM, pOutbuf+OFF_M_STRNUM, SIZ_S_STRNUM);
      memcpy(acAddr1, pOutbuf+OFF_S_STRNUM, SIZ_S_STRNUM);
      blankRem(acAddr1, SIZ_S_STRNUM);
      strcat(acAddr1, " ");
   } else
      acAddr1[0] = 0;

   if (sAdrRec.strDir[0] > ' ')
   {
      *(pOutbuf+OFF_S_DIR) = sAdrRec.strDir[0];
      strcat(acAddr1, sAdrRec.strDir);
      strcat(acAddr1, " ");
   }

   if (sAdrRec.strName[0] > ' ')
   {
      iTmp = strlen(sAdrRec.strName);
      if (iTmp > SIZ_S_STREET)
         iTmp = SIZ_S_STREET;
      memcpy(pOutbuf+OFF_S_STREET, sAdrRec.strName, iTmp);
      strcat(acAddr1, sAdrRec.strName);
      strcat(acAddr1, " ");
   }
   if (sAdrRec.strSfx[0] > ' ')
   {
      // Translate to code
      iCode = GetSfxCode(myTrim(sAdrRec.strSfx));
      if (iCode)
      {
         iTmp = sprintf(acTmp, "%d", iCode);
         memcpy(pOutbuf+OFF_S_SUFF, acTmp, iTmp);
      } else
         LogMsg0("*** Unknown suffix %s", sAdrRec.strSfx);

      strcat(acAddr1, sAdrRec.strSfx);
      strcat(acAddr1, " ");
   }

   // Copy unit#
   if (pRec->S_Unit[0] > '0' || pRec->S_Unit[0] == '#')
   {
      if (pRec->S_Unit[0] == '#')
      {
         memcpy(acTmp, pRec->S_Unit, RSIZ_SITUS_UNIT);
         acTmp[RSIZ_SITUS_UNIT] = ' ';
      } else
      {
         acTmp[0] = '#';
         memcpy(&acTmp[1], pRec->S_Unit, RSIZ_SITUS_UNIT);
      }

      acTmp[RSIZ_SITUS_UNIT+1] = 0;
      memcpy(pOutbuf+OFF_S_UNITNO, &acTmp[1], RSIZ_SITUS_UNIT);
      strcat(acAddr1, acTmp);
   }

   memcpy(pOutbuf+OFF_S_ADDR_D, acAddr1, strlen(acAddr1));

   // state
   memcpy(pOutbuf+OFF_S_ST, "CA", 2);

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "3585300501", 10))
   //   iTmp = 0;
#endif

   // Matching up with mailing addr to get city name
   if (*(pOutbuf+OFF_S_STRNUM) > '0' && *(pOutbuf+OFF_S_STREET) > '0' &&
       //!memcmp(pOutbuf+OFF_S_STRNUM, pOutbuf+OFF_M_STRNUM, SIZ_S_STRNUM) &&
       !memcmp(pOutbuf+OFF_S_STREET, pOutbuf+OFF_M_STREET, 15) &&
       *(pOutbuf+OFF_M_CITY) > ' ')
   {
      memcpy(acCity, pOutbuf+OFF_M_CITY, SIZ_M_CITY);
      myTrim(acCity, SIZ_M_CITY);
      City2Code(acCity, acCode);
      memcpy(pOutbuf+OFF_S_CITY, acCode, SIZ_M_CITY);
      memcpy(pOutbuf+OFF_S_ZIP, pOutbuf+OFF_M_ZIP, SIZ_S_ZIP);
      strcat(acCity, " CA");
      memcpy(pOutbuf+OFF_S_CTY_ST_D, acCity, strlen(acCity));  

      // Save city for next record
      memcpy(sLastCitySt, pOutbuf+OFF_S_CTY_ST_D, SIZ_S_CTY_ST_D);
      memcpy(sLastStr, pOutbuf+OFF_M_STREET, SIZ_S_STREET);
      memcpy(sLastCityCode, acCode, 4);
      memcpy(sLastZip, pOutbuf+OFF_M_ZIP, SIZ_S_ZIP);
   } else if (!memcmp(pOutbuf+OFF_S_STREET, sLastStr, 15) )
   {
      // Use situs city from last record
      memcpy(pOutbuf+OFF_S_CITY, sLastCityCode, 4);
      memcpy(pOutbuf+OFF_S_ZIP, sLastZip, SIZ_S_ZIP);
      memcpy(pOutbuf+OFF_S_CTY_ST_D, sLastCitySt, SIZ_S_CTY_ST_D);
      lUsePrev++;
#ifdef _DEBUG
      if (bDebug)
         LogMsg("Use last city: %.12s: %s - %s", pOutbuf, sLastStr, sLastCitySt);
#endif
   } else
   {
      memcpy(acTmp, pRec->TRA, 2);
      acTmp[2] = 0;
      iCode = XrefCode2Idx(&asTRA[0], acTmp, iNumTRA);
      if (iCode > 0)
      {
         iTmp = sprintf(acCode, "%d", iCode);
         memcpy(pOutbuf+OFF_S_CITY, acCode, iTmp);
         pTmp = XrefIdx2Name(&asTRA[0], iCode, iNumTRA);
         iTmp = sprintf(acAddr2, "%s CA", pTmp);
         memcpy(pOutbuf+OFF_S_CTY_ST_D, acAddr2, iTmp);
         lUseTRA++;
#ifdef _DEBUG
         if (bDebug)
            LogMsg("TRA-CITY: %.12s - TRA=%s - %s", pOutbuf, acTmp, pTmp);
#endif
      }
   }

}

/*****/
void Sdx_MergeSAdr_Mpr08(char *pOutbuf, char *pRollRec)
{
   SDX_LIEN *pRec;
   char     *pTmp, acTmp[256], acAddr1[64], acAddr2[64], acCode[32], acCity[32];
   int      iTmp, iCode;

   ADR_REC  sAdrRec;
   pRec = (SDX_LIEN *)pRollRec;

   // Situs
   acAddr1[0] = 0;
   acAddr2[0] = 0;
   memset((void *)&sAdrRec, 0, sizeof(ADR_REC));
   removeSitus(pOutbuf);

   // Copy StrNum
   sAdrRec.lStrNum = atoin(pRec->S_StrNum, LSIZ_SITUS_NUMBER);
   memcpy(acAddr1, pRec->S_StrName, LSIZ_SITUS_STREET);
   acAddr1[LSIZ_SITUS_STREET] = 0;
   if (pTmp = strchr(acAddr1, '*'))
      *pTmp = 0;
   else
      myTrim(acAddr1, LSIZ_SITUS_STREET);

   if (pTmp = strchr(acAddr1, '('))
   {
      if (*(pTmp-1) == ' ')
         *--pTmp = 0;
      else
         *pTmp = 0;
   }

   // Parse address
   if (pTmp = strstr(acAddr1, "  "))
   {
      *pTmp = 0;
   }
   parseAdrNSD(&sAdrRec, acAddr1);

   // Check for invalid address
   if (!sAdrRec.lStrNum && (sAdrRec.strName[0] == '0' || sAdrRec.strName[0] == 'O'))
      iCode = 0;

   if (sAdrRec.lStrNum > 0)
   {
      iTmp = sprintf(acAddr1, "%ld ", sAdrRec.lStrNum);
      memcpy(pOutbuf+OFF_S_STRNUM, acAddr1, iTmp);

      // Check for range of strNum that extends into unit number
      //if (isCharIncluded(pRec->S_StrNum, '-', RSIZ_SITUS_NUMBER+RSIZ_SITUS_UNIT))
      if (pRec->S_Unit[0] == '-')
      {
         if (pRec->S_Unit[1] > ' ')
         {
            //iTmp = sprintf(acAddr1, "%.*s ", RSIZ_SITUS_NUMBER+RSIZ_SITUS_UNIT, pRec->S_StrNum);
            iTmp = sprintf(acAddr1, "%d%.*s ", sAdrRec.lStrNum, RSIZ_SITUS_UNIT, pRec->S_Unit);
         } else
         {
            // 101-292-21
            /* to be implemented
               1273003200 -> 31223-  VIA MARGARITA #1161*\
               1282904900 -> 09690-  COVEY LN 9818*\
               1290607200 -> 31491-  VIA VISTA MEJOR O*\
               1291621000 -> 30460-  ANTHONY RD  1/2*\
               1293704200 -> 13474-  HILLTOP TERR Y*\
               1320900400 -> 16160-  HIGHWAY 76       195C*\
               1361202200 -> 23463-  GRADE RD EAST 23877*\
               1431720930 -> 00904-  THE STRAND NORTH 43172092990*\
               1480151300 -> 01322-  LEMON ST  1322-221/2*\
               1500771000 -> 00318-  MYERS ST SOUTH  318-318 1/2*\
               1520111500 -> 01030-  TREMONT ST SOUTH  1030-30 1/2*\
               1520740700 -> 00100-  OCEANSIDE BLVD  110A-110B*\
               1552714000 -> 00870-  LAGUNA DR  870-872-874-876*\
               1606025700 -> 00469-  BENEVENTE DR 469*\
               1606801713 -> 04015   AVENIDA DE LA PLATA #H-1 #H-2*\
               1612314400 -> 00311-  LOS ANGELES DR WEST 1-10*\
               1617500701 -> 01205-  NATOMA WAY #WAYA*\
               1620307615 -> 00432-  EDGEHILL LN ##163*\
               1631211800 -> 00743-  OLIVE AVE  741A-743*\
               1733001600 -> 01223-  VISTA WAY EAST  1223-1385*\
               2060201600 -> 00240-  CHINQUAPIN AVE  240,242,244*\
               2161003600 -> 01748-  NOMA LN EAST CL-1748B*\
            */
            //iTmp = sprintf(acAddr1, "%.*s ", RSIZ_SITUS_NUMBER+RSIZ_SITUS_UNIT, pRec->S_StrNum);
         }
         lRecCnt++;
      } 
      memcpy(pOutbuf+OFF_S_HSENO, acAddr1, iTmp);
   //} else if (sAdrRec.strName[0] > '0' && *(pOutbuf+OFF_M_STREET) > '0' &&
   //   !memcmp(pOutbuf+OFF_M_STREET, sAdrRec.strName, strlen(sAdrRec.strName)) )
   } else if (sAdrRec.strName[0] > '0' && *(pOutbuf+OFF_M_STREET) > '0' &&
      !memcmp(pOutbuf+OFF_M_STREET, sAdrRec.strName, strlen(sAdrRec.strName)) &&
      !memcmp(pOutbuf+OFF_M_SUFF, sAdrRec.strSfx, strlen(sAdrRec.strSfx)) )
   {
      memcpy(pOutbuf+OFF_S_STRNUM, pOutbuf+OFF_M_STRNUM, SIZ_S_STRNUM);
      memcpy(acAddr1, pOutbuf+OFF_S_STRNUM, SIZ_S_STRNUM);
      blankRem(acAddr1, SIZ_S_STRNUM);
      strcat(acAddr1, " ");
   } else
      acAddr1[0] = 0;

   if (sAdrRec.strDir[0] > ' ')
   {
      *(pOutbuf+OFF_S_DIR) = sAdrRec.strDir[0];
      strcat(acAddr1, sAdrRec.strDir);
      strcat(acAddr1, " ");
   }

   if (sAdrRec.strName[0] > ' ')
   {
      iTmp = strlen(sAdrRec.strName);
      if (iTmp > SIZ_S_STREET)
         iTmp = SIZ_S_STREET;
      memcpy(pOutbuf+OFF_S_STREET, sAdrRec.strName, iTmp);
      strcat(acAddr1, sAdrRec.strName);
      strcat(acAddr1, " ");
   }
   if (sAdrRec.strSfx[0] > ' ')
   {
      // Translate to code
      iCode = GetSfxCode(myTrim(sAdrRec.strSfx));
      if (iCode)
      {
         iTmp = sprintf(acTmp, "%d", iCode);
         memcpy(pOutbuf+OFF_S_SUFF, acTmp, iTmp);
      } else
         LogMsg0("*** Unknown suffix %s", sAdrRec.strSfx);

      strcat(acAddr1, sAdrRec.strSfx);
      strcat(acAddr1, " ");
   }

   // Copy unit#
   if (pRec->S_Unit[0] > '0' || pRec->S_Unit[0] == '#')
   {
      if (pRec->S_Unit[0] == '#')
      {
         memcpy(acTmp, pRec->S_Unit, LSIZ_SITUS_UNIT);
         acTmp[LSIZ_SITUS_UNIT] = ' ';
      } else
      {
         acTmp[0] = '#';
         memcpy(&acTmp[1], pRec->S_Unit, LSIZ_SITUS_UNIT);
      }

      acTmp[LSIZ_SITUS_UNIT+1] = 0;
      memcpy(pOutbuf+OFF_S_UNITNO, &acTmp[0], LSIZ_SITUS_UNIT+1);
      strcat(acAddr1, acTmp);
   }

   iTmp = blankRem(acAddr1);
   memcpy(pOutbuf+OFF_S_ADDR_D, acAddr1, iTmp);

   // state
   memcpy(pOutbuf+OFF_S_ST, "CA", 2);

   // Matching up with mailing addr to get city name
   if (*(pOutbuf+OFF_S_STRNUM) > '0' && *(pOutbuf+OFF_S_STREET) > '0' &&
       !memcmp(pOutbuf+OFF_S_STRNUM, pOutbuf+OFF_M_STRNUM, SIZ_S_STRNUM) &&
       !memcmp(pOutbuf+OFF_S_STREET, pOutbuf+OFF_M_STREET, SIZ_S_STREET) &&
       *(pOutbuf+OFF_M_CITY) > ' ')
   {
      memcpy(acCity, pOutbuf+OFF_M_CITY, SIZ_M_CITY);
      myTrim(acCity, SIZ_M_CITY);
      City2Code(acCity, acCode);
      memcpy(pOutbuf+OFF_S_CITY, acCode, SIZ_M_CITY);
      memcpy(pOutbuf+OFF_S_ZIP, pOutbuf+OFF_M_ZIP, SIZ_S_ZIP);
      strcat(acCity, " CA");
      memcpy(pOutbuf+OFF_S_CTY_ST_D, acCity, strlen(acCity));
      //iTmp = sprintf(acAddr2, "%s, CA %.5s     ", acCity, pOutbuf+OFF_S_ZIP);
      //vmemcpy(pOutbuf+OFF_S_CTY_ST_D, acAddr2, SIZ_S_CTY_ST_D, iTmp);  

      lUseMailCity++;
   } else if (fdCity)
   {
      char acZip[16];

      iTmp = getCityZip(pOutbuf, acCity, acZip, iApnLen);
      if (!iTmp)
      {
         City2Code(acCity, acCode);
         memcpy(pOutbuf+OFF_S_CITY, acCode, SIZ_M_CITY);
         memcpy(pOutbuf+OFF_S_ZIP, acZip, strlen(acZip));
         strcat(acCity, " CA");
         memcpy(pOutbuf+OFF_S_CTY_ST_D, acCity, strlen(acCity));  
         lUseGis++;
      }
   } else
   {
      /*
      memcpy(acTmp, pRec->TRA, 2);
      acTmp[2] = 0;
      iCode = XrefCode2Idx(&asTRA[0], acTmp, iNumTRA);
      if (iCode > 0)
      {
         iTmp = sprintf(acCode, "%d", iCode);
         memcpy(pOutbuf+OFF_S_CITY, acCode, iTmp);
         pTmp = XrefIdx2Name(&asTRA[0], iCode, iNumTRA);
         iTmp = sprintf(acAddr2, "%s CA", pTmp);
         memcpy(pOutbuf+OFF_S_CTY_ST_D, acAddr2, iTmp);
      }
      */
   }
}

void Sdx_MergeSAdr_LDR(char *pOutbuf, char *pStrNum, char *pStreet)
{
   char     *pTmp, acTmp[256], acAddr1[64], acAddr2[64], acCode[32], acCity[32];
   char     sUnit[64], sHseSub[64], sHseNo[64], sStreet[64], acZip[16];
   int      iTmp, iCode;

   ADR_REC  sAdrRec;

   // Situs
   acAddr2[0] = acAddr1[0] = sStreet[0] = sUnit[0] = sHseSub[0] = sHseNo[0] = 0;
   memset((void *)&sAdrRec, 0, sizeof(ADR_REC));
   removeSitus(pOutbuf);

   // Remove end street name
   if (pTmp = strchr(pStreet, '*'))
      *pTmp = 0;

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "1025302211", 10))
   //   iTmp = 0;
#endif
   // Copy StrNum - First 5 digits is StrNum, last 3 is Unit#
   // 01925196
   sAdrRec.lStrNum = atoin(pStrNum, 5);
   if (sAdrRec.lStrNum > 0)
   {
      if (*(pStrNum+5) == '#')
      {
         if (*(pStrNum+6) == '0')
            sprintf(sUnit, "#%s", pStrNum+7);
         else
            strcpy(sUnit, pStrNum+5);
      } else if (*(pStrNum+6) == '/')
      {
         iTmp = sprintf(sHseSub, "%s", pStrNum+5);
         memcpy(pOutbuf+OFF_S_STR_SUB, sHseSub, iTmp);
         iTmp = sprintf(sHseNo, "%d", sAdrRec.lStrNum);
         vmemcpy(pOutbuf+OFF_S_HSENO, sHseNo, iTmp);
      } else if (*(pStrNum+5) >= '0')
         sprintf(sUnit, "#%s", pStrNum+5);
   } 

   strcpy(acAddr1, pStreet);
   if (pTmp = strchr(acAddr1, '('))
   {
      if (*(pTmp-1) == ' ')
         *--pTmp = 0;
      else
         *pTmp = 0;
   }

   // Parse address
   parseAdrNSD(&sAdrRec, acAddr1);

   // Check for invalid address
   if (!sAdrRec.lStrNum && (sAdrRec.strName[0] == '0' || sAdrRec.strName[0] == 0))
      iCode = 0;

   if (sAdrRec.lStrNum > 0)
   {
      iTmp = sprintf(acAddr1, "%ld ", sAdrRec.lStrNum);
      memcpy(pOutbuf+OFF_S_STRNUM, acAddr1, iTmp);
      if (sHseSub[0] > ' ')
         iTmp = sprintf(acAddr1, "%ld %s ", sAdrRec.lStrNum, sHseSub);

      /* Check for range of strNum that extends into unit number
         00241A-D
         00118-20
         00641-   TUMBLE CREEK TER  1/2*\
         00463-   STAGE COACH LN SOUTH 529*\
         07551-   RANCHO AMIGOS RD NORTH 7607*\
         04652-   VALLE DEL SOL #4654  4654*\
         05704-   CAMINO DEL CIELO #1001*\
         30371-   COYOTE RUN T*\
         22104-   CRESTLINE RD Y*\
         00511-   MONTEREY DR N*\
         02004-   ST VINCENT DR #1299*\
         1290607200 -> 31491-  VIA VISTA MEJOR O*\
         1291621000 -> 30460-  ANTHONY RD  1/2*\
         1293704200 -> 13474-  HILLTOP TERR Y*\
         1320900400 -> 16160-  HIGHWAY 76       195C*\
         1361202200 -> 23463-  GRADE RD EAST 23877*\
         1431720930 -> 00904-  THE STRAND NORTH 43172092990*\
         1480151300 -> 01322-  LEMON ST  1322-221/2*\
         1500771000 -> 00318-  MYERS ST SOUTH  318-318 1/2*\
         1520111500 -> 01030-  TREMONT ST SOUTH  1030-30 1/2*\
         1520740700 -> 00100-  OCEANSIDE BLVD  110A-110B*\
         1552714000 -> 00870-  LAGUNA DR  870-872-874-876*\
         1606025700 -> 00469-  BENEVENTE DR 469*\
         1606801713 -> 04015   AVENIDA DE LA PLATA #H-1 #H-2*\
         1612314400 -> 00311-  LOS ANGELES DR WEST 1-10*\
         1617500701 -> 01205-  NATOMA WAY #WAYA*\
         1620307615 -> 00432-  EDGEHILL LN ##163*\
         1631211800 -> 00743-  OLIVE AVE  741A-743*\
         1733001600 -> 01223-  VISTA WAY EAST  1223-1385*\
         2161003600 -> 01748-  NOMA LN EAST CL-1748B*\
      */

#ifdef _DEBUG
      //if (!memcmp(pOutbuf, "1054213100", 10))
      //   iTmp = 0;
#endif

      if (pTmp = strchr(pStrNum, '-'))
      {
         if (isdigit(*(pTmp+1)))
         {
            // 00118-20
            iTmp = sprintf(acAddr1, "%d%s ", sAdrRec.lStrNum, pTmp);
         } else if (*(pTmp-1) >= 'A')
         {
            // 00241A-D
            strcpy(&sUnit[1], pTmp-1);
            sUnit[0] = '#';
            *(pTmp-1) = 0;
         } else
         {
            // Remove hyphen from StrNum
            *pTmp = 0;
            if ((pTmp = strrchr(pStreet, ' ')) && isdigit(*(pTmp+1)))
            {
               if (*(pTmp+2) == '/')
               {
                  iTmp = sprintf(acAddr1, "%d", sAdrRec.lStrNum);
                  memcpy(pOutbuf+OFF_S_HSENO, acAddr1, iTmp);
                  vmemcpy(pOutbuf+OFF_S_STR_SUB, pTmp+1, SIZ_S_STR_SUB);
                  iTmp = sprintf(acAddr1, "%d%s ", sAdrRec.lStrNum, pTmp);
               } else
                  iTmp = sprintf(acAddr1, "%d-%s ", sAdrRec.lStrNum, pTmp+1);
               *pTmp = 0;
               strcpy(sStreet, pStreet);
            }
         }
      } 

      if (*(pOutbuf+OFF_S_HSENO) == ' ')
         vmemcpy(pOutbuf+OFF_S_HSENO, acAddr1, SIZ_S_HSENO);
   } else
      acAddr1[0] = 0;

   if (sAdrRec.strDir[0] > ' ')
   {
      *(pOutbuf+OFF_S_DIR) = sAdrRec.strDir[0];
      strcat(acAddr1, sAdrRec.strDir);
      strcat(acAddr1, " ");
   }

   // Street name
   if (sStreet[0] > '0')
   {
      vmemcpy(pOutbuf+OFF_S_STREET, sStreet, SIZ_S_STREET);
      strcat(acAddr1, sStreet);
      strcat(acAddr1, " ");
   } else if (sAdrRec.strName[0] > ' ')
   {
      vmemcpy(pOutbuf+OFF_S_STREET, sAdrRec.strName, SIZ_S_STREET);
      strcat(acAddr1, sAdrRec.strName);
      strcat(acAddr1, " ");
   }

   if (sAdrRec.strSfx[0] > ' ')
   {
      // Translate to code
      iCode = GetSfxCode(myTrim(sAdrRec.strSfx));
      if (iCode)
      {
         iTmp = sprintf(acTmp, "%d", iCode);
         memcpy(pOutbuf+OFF_S_SUFF, acTmp, iTmp);
      } else
         LogMsg0("*** Unknown suffix %s", sAdrRec.strSfx);

      strcat(acAddr1, sAdrRec.strSfx);
      strcat(acAddr1, " ");
   }

   if (sAdrRec.Unit[0] > ' ')
   {
      vmemcpy(pOutbuf+OFF_S_UNITNO,  sAdrRec.Unit, SIZ_S_UNITNO);
      vmemcpy(pOutbuf+OFF_S_UNITNOX, sAdrRec.UnitNox, SIZ_S_UNITNOX);
      strcat(acAddr1, sAdrRec.UnitNox);
   } else if (sUnit[0] > ' ')
   {
      vmemcpy(pOutbuf+OFF_S_UNITNO,  sUnit, SIZ_S_UNITNO);
      vmemcpy(pOutbuf+OFF_S_UNITNOX, sUnit, SIZ_S_UNITNOX);
      strcat(acAddr1, sUnit);
   }

   iTmp = 0;
   while (acAddr1[iTmp] == '0')
      iTmp++;
   vmemcpy(pOutbuf+OFF_S_ADDR_D, &acAddr1[iTmp], SIZ_S_ADDR_D);

   // state
   memcpy(pOutbuf+OFF_S_ST, "CA", 2);

   // Matching up with mailing addr to get city name
   if (*(pOutbuf+OFF_S_STRNUM) > '0' && *(pOutbuf+OFF_S_STREET) > '0' &&
       !memcmp(pOutbuf+OFF_S_STRNUM, pOutbuf+OFF_M_STRNUM, SIZ_S_STRNUM) &&
       !memcmp(pOutbuf+OFF_S_STREET, pOutbuf+OFF_M_STREET, SIZ_S_STREET) &&
       *(pOutbuf+OFF_M_CITY) > ' ')
   {
      memcpy(acCity, pOutbuf+OFF_M_CITY, SIZ_M_CITY);
      myTrim(acCity, SIZ_M_CITY);
      iTmp = City2Code(acCity, acCode, pOutbuf);
      if (iTmp > 0)
      {
         lUseMailCity++;
         memcpy(pOutbuf+OFF_S_CITY, acCode, SIZ_M_CITY);
      }
      memcpy(pOutbuf+OFF_S_ZIP, pOutbuf+OFF_M_ZIP, SIZ_S_ZIP);
      strcat(acCity, " CA");
      memcpy(pOutbuf+OFF_S_CTY_ST_D, acCity, strlen(acCity));
   } else if (fdCity)
   {
      lSeqNum++;
      iTmp = getCityZip(pOutbuf, acCity, acZip, iApnLen);
      if (!iTmp)
      {
         iTmp = City2Code(acCity, acCode);
         if (!iTmp)
            LogMsg("***** Please update city table for %s APN=%.10s", acCity, pOutbuf);
         else
         {
            memcpy(pOutbuf+OFF_S_CITY, acCode, SIZ_M_CITY);
            if (acZip[0] == '9')
               memcpy(pOutbuf+OFF_S_ZIP, acZip, strlen(acZip));
            strcat(acCity, " CA");
            memcpy(pOutbuf+OFF_S_CTY_ST_D, acCity, strlen(acCity));  
            lUseGis++;
         }
      }
   } else
   {
      iTmp = 0;
      /*
      memcpy(acTmp, pRec->TRA, 2);
      acTmp[2] = 0;
      iCode = XrefCode2Idx(&asTRA[0], acTmp, iNumTRA);
      if (iCode > 0)
      {
         iTmp = sprintf(acCode, "%d", iCode);
         memcpy(pOutbuf+OFF_S_CITY, acCode, iTmp);
         pTmp = XrefIdx2Name(&asTRA[0], iCode, iNumTRA);
         iTmp = sprintf(acAddr2, "%s CA", pTmp);
         memcpy(pOutbuf+OFF_S_CTY_ST_D, acAddr2, iTmp);
      }
      */
   }

}

/*****************************************************************************/

//void Sdx_AsrMergeSAdr(char *pOutbuf, char *pRollRec)
//{
//   SDX_ROLL *pRec;
//   char     *pTmp, acAddr1[64], acAddr2[64];
//   int      iTmp;
//
//   ADR_REC  sAdrRec;
//   pRec = (SDX_ROLL *)pRollRec;
//
//   // Situs
//   acAddr1[0] = 0;
//   acAddr2[0] = 0;
//   memset((void *)&sAdrRec, 0, sizeof(ADR_REC));
//   memset(pOutbuf+AOFF_S_STRNUM, ' ', (AOFF_CAREOF)-(AOFF_S_STRNUM));
//
//   sAdrRec.lStrNum = atoin(pRec->S_StrNum, RSIZ_SITUS_NUMBER);
//   memcpy(acAddr1, pRec->S_StrName, RSIZ_SITUS_STREET);
//   acAddr1[RSIZ_SITUS_STREET] = 0;
//   if (pTmp = strchr(acAddr1, '*'))
//      *pTmp = 0;
//   else
//      myTrim(acAddr1, RSIZ_SITUS_STREET);
//
//   if (pTmp = strchr(acAddr1, '('))
//   {
//      if (*(pTmp-1) == ' ')
//         *--pTmp = 0;
//      else
//         *pTmp = 0;
//   }
//
//   // Parse address
//   parseAdrNSD(&sAdrRec, acAddr1);
//
//   // Check for invalid address
//   if (!sAdrRec.lStrNum && (sAdrRec.strName[0] == '0' || sAdrRec.strName[0] == 'O'))
//      return;
//
//   if (sAdrRec.lStrNum > 0)
//   {
//      iTmp = sprintf(acAddr1, "%ld ", sAdrRec.lStrNum);
//      memcpy(pOutbuf+AOFF_S_STRNUM, acAddr1, iTmp);
//   }
//
//   if (sAdrRec.strDir[0] > ' ')
//      *(pOutbuf+AOFF_S_STRDIR) = sAdrRec.strDir[0];
//
//   if (sAdrRec.strName[0] > ' ')
//   {
//      iTmp = strlen(sAdrRec.strName);
//      if (iTmp > ASIZ_S_STRNAME)
//         iTmp = ASIZ_S_STRNAME;
//      memcpy(pOutbuf+AOFF_S_STRNAME, sAdrRec.strName, iTmp);
//   }
//
//   if (sAdrRec.strSfx[0] > ' ')
//      memcpy(pOutbuf+AOFF_S_STRSFX, sAdrRec.strSfx, strlen(sAdrRec.strSfx));
//
//   // Copy unit#
//   if (pRec->S_Unit[0] > '0' || pRec->S_Unit[0] == '#')
//   {
//      if (pRec->S_Unit[0] == '#')
//         memcpy(pOutbuf+AOFF_S_UNIT, &pRec->S_Unit[1], RSIZ_SITUS_UNIT-1);
//      else
//         memcpy(pOutbuf+AOFF_S_UNIT, pRec->S_Unit, RSIZ_SITUS_UNIT);
//   }
//
//   // County doesn't supply city name, so we don't provide it
//   // Matching up with mailing addr to get city name
//   if (!memcmp(pOutbuf+AOFF_S_STRNUM,  pOutbuf+AOFF_M_STRNUM,  ASIZ_S_STRNUM) &&
//       !memcmp(pOutbuf+AOFF_S_STRNAME, pOutbuf+AOFF_M_STRNAME, ASIZ_S_STRNAME) &&
//       *(pOutbuf+AOFF_M_CITY) > ' ')
//   {
//      memcpy(pOutbuf+AOFF_S_CITY, pOutbuf+AOFF_M_CITY, ASIZ_M_CITY);
//   }
//}

/********************************* Sdx_MergeMAdr *****************************
 *
 * Mail address has similar problem as situs, but worse due to inconsistency.
 * Now we have to check for direction both pre and post strname.
 *
 * C/O ERIC ROMERO*2944 29TH ST*SAN DIEGO CA\
 *
 *****************************************************************************/

void Sdx_MergeMAdr(char *pOutbuf, char *pRollRec)
{
   SDX_ROLL *pRec;
   char     *pTmp, *pTmp1, acTmp[256];
   char     acAddr[4][64], acAddr1[64], acAddr2[64];
   int      iTmp, iIdx, iCnt;
   long     lTmp;

   ADR_REC  sMailAdr;

   pRec = (SDX_ROLL *)pRollRec;

   // Initialize
   *acAddr[0]=*acAddr[1]=*acAddr[2]=*acAddr[3]=0;
   removeMailing(pOutbuf, true);

   memcpy(acTmp, pRec->MailAddr, RSIZ_MAIL_ADDR);
   acTmp[RSIZ_MAIL_ADDR] = 0;

   // Special case
   if ((unsigned char)acTmp[0] == 0xBA && !memcmp(&acTmp[1], "REEN", 4))
      acTmp[0] = 'G';

   if (pTmp = strchr(acTmp, 92))
      *pTmp = 0;
   else
      return;

   // Parse addr lines
   pTmp1 = acTmp;
   pTmp = strchr(pTmp1, '*');
   iIdx=iCnt=0;
   while (pTmp && iCnt < 3)
   {
      *pTmp++ = 0;
      strcpy(acAddr[iCnt++], pTmp1);
      pTmp1 = pTmp;
      pTmp = strchr(pTmp1, '*');
   }
   strcpy(acAddr[iCnt++], pTmp1);

   // Check for CareOf
   if (!memcmp(acAddr[0], "C/O", 3))
   {
      iTmp = strlen(acAddr[0]);
      if (iTmp > 10)
         updateCareOf(pOutbuf, acAddr[0], iTmp);
      iIdx++;
   }

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "1630422200", 8))
   //   iTmp = 0;
#endif

   if (iCnt > 1)
   {
      strcpy(acAddr2, acAddr[--iCnt]);
      vmemcpy(pOutbuf+OFF_M_CTY_ST_D, acAddr2, SIZ_M_CTY_ST_D);
   } else
      return;     // bad address

   // Fix known bad data
   if (!memcmp(acAddr[0], "2727651A PAZ", 12) )
      memcpy(acAddr[0], "27651 LA PAZ", 12);

   // Save addr line 1
   strcpy(acAddr1, acAddr[--iCnt]);

   // Check for possible mail to/care of name
   if (iCnt == 1 && *(pOutbuf+OFF_CARE_OF) == ' ')
      updateCareOf(pOutbuf, acAddr[0], strlen(acAddr[0]));

   // Parse addr1
   memset((void *)&sMailAdr, 0, sizeof(ADR_REC));

   bool bPmb=false, bPob=false;
   if (!memcmp(acAddr1, "PO B", 4) || !memcmp(acAddr1, "P O B", 5))
      bPob = true;
   if (!memcmp(acAddr[0], "PMB", 3) || !memcmp(acAddr[0], "P M B", 5))
      bPmb = true;

   if (iCnt > 0 && bPob && bPmb)
   {
      iTmp = replStr(acAddr[0], "PMB LOT", "PMB");
      if (!iTmp)
         replStr(acAddr[0], "P M B", "PMB");
      replStr(acAddr[0], " #", " ");
      replStr(acAddr[1], "P O", "PO");
      iTmp = sprintf(acTmp, "%s*%s", acAddr[0], acAddr[1]);
      vmemcpy(pOutbuf+OFF_M_STREET, acTmp, SIZ_M_STREET, iTmp);
      vmemcpy(pOutbuf+OFF_M_ADDR_D, acTmp, SIZ_M_ADDR_D, iTmp);
   } else
   {
      vmemcpy(pOutbuf+OFF_M_ADDR_D, acAddr1, SIZ_M_ADDR_D);
      parseMAdr1_2(&sMailAdr, acAddr1);
      if (sMailAdr.lStrNum > 0)
      {
         //sprintf(acTmp, "%d       ", sMailAdr.lStrNum);
         vmemcpy(pOutbuf+OFF_M_STRNUM, sMailAdr.HseNo, SIZ_M_STRNUM);
         //if (sMailAdr.strSub[0] > '0')
         //{
         //   sprintf(acTmp, "%s  ", sMailAdr.strSub);
         //   memcpy(pOutbuf+OFF_M_STR_SUB, acTmp, SIZ_M_STR_SUB);
         //}
         memcpy(pOutbuf+OFF_M_DIR, sMailAdr.strDir, strlen(sMailAdr.strDir));
         memcpy(pOutbuf+OFF_M_STREET, sMailAdr.strName, strlen(sMailAdr.strName));
         memcpy(pOutbuf+OFF_M_SUFF, sMailAdr.strSfx, strlen(sMailAdr.strSfx));
         if (sMailAdr.Unit[0] > ' ')
         {
            memcpy(pOutbuf+OFF_M_UNITNO, sMailAdr.Unit, strlen(sMailAdr.Unit));
            memcpy(pOutbuf+OFF_M_UNITNOX, sMailAdr.UnitNox, strlen(sMailAdr.UnitNox));
            if (sMailAdr.strSub[0] > '0')
               memcpy(pOutbuf+OFF_M_STR_SUB, sMailAdr.strSub, strlen(sMailAdr.strSub));
         } else if (sMailAdr.strSub[0] > '0')
         {
            memcpy(pOutbuf+OFF_M_UNITNO, sMailAdr.strSub, strlen(sMailAdr.strSub));
            memcpy(pOutbuf+OFF_M_UNITNOX, sMailAdr.strSub, strlen(sMailAdr.strSub));
         }

#ifdef _DEBUG
         //if (sMailAdr.Unit[0] > ' ' && sMailAdr.strSub[0] > '0')
         //   iTmp = 0;
#endif
      } else
         memcpy(pOutbuf+OFF_M_STREET, acAddr1, strlen(acAddr1));
   }

   // Parse addr2
   pTmp = strrchr(acAddr2, ' ');
   if (pTmp && strlen(pTmp) == 3)
   {
      *pTmp = 0;
      memcpy(pOutbuf+OFF_M_ST, pTmp+1, 2);
   }

   iTmp = remChar(acAddr2, ',');
   vmemcpy(pOutbuf+OFF_M_CITY, acAddr2, SIZ_M_CITY, iTmp);

   // Zipcode
   lTmp = atoin(pRec->MailZip, RSIZ_MAIL_ZIP);
   if (lTmp > 400)
      memcpy(pOutbuf+OFF_M_ZIP, pRec->MailZip, SIZ_M_ZIP);
}

void Sdx_MergeMAdr_Mpr08(char *pOutbuf, char *pRollRec)
{
   SDX_LIEN *pRec;
   char     *pTmp, *pTmp1, acTmp[256];
   char     acAddr[4][64], acAddr1[64], acAddr2[64];
   int      iTmp, iIdx, iCnt;
   long     lTmp;

   ADR_REC  sMailAdr;

   pRec = (SDX_LIEN *)pRollRec;

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "4174000600", 10))
   //   iTmp = 0;
#endif

   // Initialize
   *acAddr[0]=*acAddr[1]=*acAddr[2]=*acAddr[3]=0;
   removeMailing(pOutbuf, true);

   memcpy(acTmp, pRec->MailAddr, LSIZ_MAIL_ADDR);
   acTmp[LSIZ_MAIL_ADDR] = 0;
   // Terminate at '\'
   if (pTmp = strchr(acTmp, 92))
      *pTmp = 0;
   else
      return;

   // Parse addr lines
   pTmp1 = acTmp;
   pTmp = strchr(pTmp1, '*');
   iIdx=iCnt=0;
   while (pTmp && iCnt < 3)
   {
      *pTmp++ = 0;
      strcpy(acAddr[iCnt++], pTmp1);
      pTmp1 = pTmp;
      pTmp = strchr(pTmp1, '*');
   }
   strcpy(acAddr[iCnt++], pTmp1);

   // Check for known bad char
   if ((unsigned char)*acAddr[0] == 0xA6)
      *acAddr[0] = 'G';

   // Check for CareOf
   iTmp = strlen(acAddr[0]);
   if (!memcmp(acAddr[0], "C/O", 3) && iTmp > 4) 
   {
      updateCareOf(pOutbuf, acAddr[0], iTmp);
      iIdx++;
   } else if (iCnt > 2 && isdigit(*acAddr[1]))
   {
      updateCareOf(pOutbuf, acAddr[0], iTmp);
      iIdx++;
   }

   if (iCnt > 1)
   {
      strcpy(acAddr2, acAddr[--iCnt]);
      vmemcpy(pOutbuf+OFF_M_CTY_ST_D, acAddr2, SIZ_M_CTY_ST_D);
   } else
      return;     // bad address

   strcpy(acAddr1, acAddr[iIdx++]);
   vmemcpy(pOutbuf+OFF_M_ADDR_D, acAddr1, SIZ_M_ADDR_D);

   // Parse addr1
   memset((void *)&sMailAdr, 0, sizeof(ADR_REC));
   parseMAdr1_2(&sMailAdr, acAddr1);
   if (sMailAdr.lStrNum > 0)
   {
      sprintf(acTmp, "%d       ", sMailAdr.lStrNum);
      memcpy(pOutbuf+OFF_M_STRNUM, acTmp, SIZ_M_STRNUM);
      if (sMailAdr.strSub[0] > '0')
      {
         sprintf(acTmp, "%s  ", sMailAdr.strSub);
         memcpy(pOutbuf+OFF_M_STR_SUB, acTmp, SIZ_M_STR_SUB);
      }
      memcpy(pOutbuf+OFF_M_DIR, sMailAdr.strDir, strlen(sMailAdr.strDir));
      memcpy(pOutbuf+OFF_M_STREET, sMailAdr.strName, strlen(sMailAdr.strName));
      memcpy(pOutbuf+OFF_M_SUFF, sMailAdr.strSfx, strlen(sMailAdr.strSfx));
      if (sMailAdr.Unit[0] > ' ')
         memcpy(pOutbuf+OFF_M_UNITNO, sMailAdr.Unit, strlen(sMailAdr.Unit));
   } else
      memcpy(pOutbuf+OFF_M_STREET, acAddr1, strlen(acAddr1));

   // Parse addr2
   pTmp = strrchr(acAddr2, ' ');
   if (pTmp && strlen(pTmp) == 3)
   {
      *pTmp = 0;
      memcpy(pOutbuf+OFF_M_ST, pTmp+1, 2);
   }

   vmemcpy(pOutbuf+OFF_M_CITY, acAddr2, SIZ_M_CITY);

   // Zipcode
   lTmp = atoin(pRec->MailZip, LSIZ_MAIL_ZIP);
   if (lTmp > 400)
      memcpy(pOutbuf+OFF_M_ZIP, pRec->MailZip, SIZ_M_ZIP);
}

void Sdx_MergeMAdr_LDR(char *pOutbuf, char *pAddr, char *pZipcode)
{
   char     *pTmp, *pTmp1, acTmp[256];
   char     acAddr[4][64], acAddr1[64], acAddr2[64];
   int      iTmp, iIdx, iCnt;
   long     lTmp;

   ADR_REC  sMailAdr;

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "4174000600", 10))
   //   iTmp = 0;
#endif

   // Initialize
   *acAddr[0]=*acAddr[1]=*acAddr[2]=*acAddr[3]=0;
   removeMailing(pOutbuf, true);

   // Terminate at '\'
   if (pTmp = strchr(pAddr, 92))
   {
      *pTmp = 0;
      strcpy(acTmp, pAddr);
   } else
      return;

   // Parse addr lines
   pTmp1 = acTmp;
   pTmp = strchr(pTmp1, '*');
   iIdx=iCnt=0;
   while (pTmp && iCnt < 3)
   {
      *pTmp++ = 0;
      strcpy(acAddr[iCnt++], pTmp1);
      pTmp1 = pTmp;
      pTmp = strchr(pTmp1, '*');
   }
   strcpy(acAddr[iCnt++], pTmp1);

   // Check for known bad char
   if ((unsigned char)*acAddr[0] == 0xA6)
      *acAddr[0] = 'G';

   // Check for CareOf
   iTmp = strlen(acAddr[0]);
   if (!memcmp(acAddr[0], "C/O", 3) && iTmp > 4) 
   {
      updateCareOf(pOutbuf, acAddr[0], iTmp);
      iIdx++;
   } else if (iCnt > 2 && isdigit(*acAddr[1]))
   {
      updateCareOf(pOutbuf, acAddr[0], iTmp);
      iIdx++;
   }

   if (iCnt > 1)
   {
      strcpy(acAddr2, acAddr[--iCnt]);
      vmemcpy(pOutbuf+OFF_M_CTY_ST_D, acAddr2, SIZ_M_CTY_ST_D);
   } else
   {
      vmemcpy(pOutbuf+OFF_M_ADDR_D, acAddr[0], SIZ_M_ADDR_D);
      return;     // bad address
   }

   strcpy(acAddr1, acAddr[iIdx++]);
   vmemcpy(pOutbuf+OFF_M_ADDR_D, acAddr1, SIZ_M_ADDR_D);

   // Parse addr1
   memset((void *)&sMailAdr, 0, sizeof(ADR_REC));
   parseMAdr1_2(&sMailAdr, acAddr1);
   if (sMailAdr.lStrNum > 0)
   {
      sprintf(acTmp, "%d       ", sMailAdr.lStrNum);
      memcpy(pOutbuf+OFF_M_STRNUM, acTmp, SIZ_M_STRNUM);
      if (sMailAdr.strSub[0] > '0')
      {
         sprintf(acTmp, "%s  ", sMailAdr.strSub);
         memcpy(pOutbuf+OFF_M_STR_SUB, acTmp, SIZ_M_STR_SUB);
      }
      memcpy(pOutbuf+OFF_M_DIR, sMailAdr.strDir, strlen(sMailAdr.strDir));
      memcpy(pOutbuf+OFF_M_STREET, sMailAdr.strName, strlen(sMailAdr.strName));
      memcpy(pOutbuf+OFF_M_SUFF, sMailAdr.strSfx, strlen(sMailAdr.strSfx));
      if (sMailAdr.Unit[0] > ' ')
      {
         memcpy(pOutbuf+OFF_M_UNITNO, sMailAdr.Unit, strlen(sMailAdr.Unit));
         memcpy(pOutbuf+OFF_M_UNITNOX, sMailAdr.UnitNox, strlen(sMailAdr.UnitNox));
      }
   } else
      memcpy(pOutbuf+OFF_M_STREET, acAddr1, strlen(acAddr1));

   // Parse addr2
   pTmp = strrchr(acAddr2, ' ');
   if (pTmp && strlen(pTmp) == 3)
   {
      *pTmp = 0;
      memcpy(pOutbuf+OFF_M_ST, pTmp+1, 2);
   }

   vmemcpy(pOutbuf+OFF_M_CITY, acAddr2, SIZ_M_CITY);

   // Zipcode
   lTmp = atol(pZipcode);
   if (lTmp > 400)
      vmemcpy(pOutbuf+OFF_M_ZIP, pZipcode, SIZ_M_ZIP);
}

/*****************************************************************************/

//void Sdx_AsrMergeMAdr(char *pOutbuf, char *pRollRec)
//{
//   SDX_ROLL *pRec;
//   char     *pTmp, *pTmp1, acTmp[256], acAddr1[64], acAddr2[64];
//   int      iTmp;
//   long     lTmp;
//
//   ADR_REC  sMailAdr;
//
//   pRec = (SDX_ROLL *)pRollRec;
//
//#ifdef _DEBUG
//   //if (!memcmp(pOutbuf, "011291065", 9))
//   //   iTmp = 0;
//#endif
//
//   // Initialize
//   acAddr1[0] = 0;
//   acAddr2[0] = 0;
//   memset((void *)&sMailAdr, 0, sizeof(ADR_REC));
//   memset(pOutbuf+AOFF_M_STRNUM, ' ', (AOFF_M_FLAG)-(AOFF_M_STRNUM));
//   memset(pOutbuf+AOFF_CAREOF, ' ', ASIZ_CAREOF);
//
//   memcpy(acTmp, pRec->MailAddr, RSIZ_MAIL_ADDR);
//   acTmp[RSIZ_MAIL_ADDR] = 0;
//   if (pTmp = strchr(acTmp, 92))
//      *pTmp = 0;
//   else
//      return;
//
//   // Check for CareOf
//   if (!memcmp(acTmp, "C/O", 3))
//   {
//      pTmp = strchr(acTmp, '*');
//      if (pTmp) *pTmp++ = 0;
//      iTmp = strlen(acTmp);
//      if (iTmp > ASIZ_CAREOF)
//         iTmp = ASIZ_CAREOF;
//      memcpy(pOutbuf+AOFF_CAREOF, acTmp, iTmp);
//   } else
//      pTmp = acTmp;
//
//   pTmp1 = strchr(pTmp, '*');
//   if (pTmp1)
//   {
//      *pTmp1++ = 0;
//      strcpy(acAddr2, pTmp1);
//   } else
//      return;     // bad address
//
//   strcpy(acAddr1, pTmp);
//
//   // Parse addr1
//   parseMAdr1_2(&sMailAdr, acAddr1);
//   if (sMailAdr.lStrNum > 0)
//   {
//      sprintf(acTmp, "%d       ", sMailAdr.lStrNum);
//      memcpy(pOutbuf+AOFF_M_STRNUM, acTmp, ASIZ_M_STRNUM);
//      if (sMailAdr.strSub[0] > '0')
//      {
//         sprintf(acTmp, "%s  ", sMailAdr.strSub);
//         memcpy(pOutbuf+AOFF_M_STRFRA, acTmp, ASIZ_M_STRFRA);
//      }
//   }
//   memcpy(pOutbuf+AOFF_M_STRDIR, sMailAdr.strDir, strlen(sMailAdr.strDir));
//   memcpy(pOutbuf+AOFF_M_STRNAME, sMailAdr.strName, strlen(sMailAdr.strName));
//   memcpy(pOutbuf+AOFF_M_STRSFX, sMailAdr.strSfx, strlen(sMailAdr.strSfx));
//   if (sMailAdr.Unit[0] > ' ')
//      memcpy(pOutbuf+AOFF_M_UNIT, sMailAdr.Unit, strlen(sMailAdr.Unit));
//
//   // Parse addr2
//   pTmp = strrchr(acAddr2, ' ');
//   *(pOutbuf+AOFF_M_FLAG) = '3';
//   if (pTmp && strlen(pTmp) == 3)
//   {
//      *pTmp = 0;
//      memcpy(pOutbuf+AOFF_M_STATE, pTmp+1, 2);
//      *(pOutbuf+AOFF_M_FLAG) = '1';
//   }
//
//   iTmp = strlen(acAddr2);
//   if (iTmp > 2)
//   {
//      if (iTmp > ASIZ_M_CITY) iTmp = ASIZ_M_CITY;
//      memcpy(pOutbuf+AOFF_M_CITY, acAddr2, iTmp);
//      *(pOutbuf+AOFF_M_FLAG) = '1';
//   }
//
//   // Zipcode
//   lTmp = atoin(pRec->MailZip, ASIZ_M_ZIP);
//   if (lTmp > 400)
//   {
//      memcpy(pOutbuf+AOFF_M_ZIP, pRec->MailZip, ASIZ_M_ZIP);
//      if (*(pOutbuf+AOFF_M_FLAG) == '3')
//         *(pOutbuf+AOFF_M_FLAG) = '2';
//   }
//
//}

/********************************* Sdx_MergeProp8 ****************************
 *
 * Set Prop8 flag using prop8 file from the county
 *
 *****************************************************************************/

int Sdx_MergeProp8(char *pOutbuf)
{

   static   char acRec[256], *pRec=NULL;
   int      iLoop;

   // Get first Char rec for first call
   if (!pRec && !lProp8Match)
   {
      // Skip header
      pRec = fgets(acRec, 256, fdProp8);
      if (acRec[0] > '9')
         pRec = fgets(acRec, 256, fdProp8);
   }

   do
   {
      if (!pRec)
      {
         fclose(fdProp8);
         fdProp8 = NULL;
         return 1;      // EOF
      }

      // Compare Apn
      iLoop = memcmp(pOutbuf, acRec, CSIZ_APN);
      if (iLoop > 0)
      {
         if (bDebug)
            LogMsg0("Skip Prop8 rec  %.*s", CSIZ_APN, acRec);
         pRec = fgets(acRec, 256, fdProp8);
         lProp8Skip++;
      }
   } while (iLoop > 0);

   // If not match, return
   if (iLoop)
      return 1;

   // Prop 8
   *(pOutbuf+OFF_PROP8_FLG) = 'Y';

   lProp8Match++;

   // Get next Char rec
   pRec = fgets(acRec, 256, fdProp8);

   return 0;
}

/********************************* Sdx_MergeChar *****************************
 *
 * 10/31/2018 Remove YrBlt
 * 01/18/2022 Clear H_Bath if not present
 *
 *****************************************************************************/

int Sdx_MergeChar(char *pOutbuf)
{

   static   char acRec[1024], *pRec=NULL;
   char     acTmp[256], *pTmp;
   ULONG    lBldgSqft, lLotAcres, lLotSqft, lTmp;
   int      iRet, iTmp, iLoop, iBeds, iFBath, iHBath;
   SDX_CHAR *pChar;

   iRet=iBeds=iFBath=iHBath=0;
   lLotSqft=lBldgSqft=lLotAcres=0;

   // Get first Char rec for first call
   if (!pRec && !lCharMatch)
   {
      pRec = fgets(acRec, 1024, fdChar);
   }
   pChar = (SDX_CHAR *)pRec;

   do
   {
      if (!pRec)
      {
         fclose(fdChar);
         fdChar = NULL;
         return 1;      // EOF
      }

      // Compare Apn
      iLoop = memcmp(pOutbuf, pChar->Apn, CSIZ_APN);
      if (iLoop > 0)
      {
         if (bDebug)
            LogMsg0("Skip Char rec  %.*s", CSIZ_APN, pChar->Apn);
         pRec = fgets(acRec, 1024, fdChar);
         lCharSkip++;
      }
   } while (iLoop > 0);

   // If not match, return
   if (iLoop)
      return 1;

   // TRA
   if (*(pOutbuf+OFF_TRA) == ' ' && pChar->Tra[0] > ' ')
   {
      //memcpy(pOutbuf+OFF_TRA, pChar->Tra, CSIZ_TRA);
      lTmp = atoin(pChar->Tra, CSIZ_TRA);
      if (lTmp > 0)
      {
         iRet = sprintf(acTmp, "%.6d", lTmp);
         memcpy(pOutbuf+OFF_TRA, acTmp, iRet);
      }
   }

   // Zoning
   if (pChar->Zoning > '0')
   {
      *(pOutbuf+OFF_ZONE) = pChar->Zoning;
      if (*(pOutbuf+OFF_ZONE_X1) == ' ')
         *(pOutbuf+OFF_ZONE_X1) = pChar->Zoning;
   }

   // LandUse 
   if (*(pOutbuf+OFF_USE_CO) == ' ')
   {
      if (pChar->LandUse[0] > '0')
      {
         memcpy(pOutbuf+OFF_USE_CO, pChar->LandUse, CSIZ_LAND_USE);
         updateStdUse(pOutbuf+OFF_USE_STD, pChar->LandUse, CSIZ_LAND_USE, pOutbuf);
      } else
         memcpy(pOutbuf+OFF_USE_STD, USE_UNASGN, SIZ_USE_STD);
   }

   // YrBlt/YrEff
   if (pChar->EffYear[0] > ' ')
   {
      pTmp = dateConversion(pChar->EffYear, acTmp, YY2YYYY);
      if (pTmp)
      {
         memcpy(pOutbuf+OFF_YR_EFF, pTmp, SIZ_YR_BLT);
         //memcpy(pOutbuf+OFF_YR_BLT, pTmp, SIZ_YR_BLT);
         memset(pOutbuf+OFF_YR_BLT, ' ', SIZ_YR_BLT);
      }
   }

   // BldgSqft
   lBldgSqft = atoin(pChar->BldgSqft, CSIZ_BLDG_SQFT);
   if (lBldgSqft == 99999)
      memset(pOutbuf+OFF_BLDG_SF, ' ', SIZ_BLDG_SF);
   else if (lBldgSqft > 10)
   {
      sprintf(acTmp, "%*u", SIZ_BLDG_SF, lBldgSqft);
      memcpy(pOutbuf+OFF_BLDG_SF, acTmp, SIZ_BLDG_SF);
   }

   // Beds
   iBeds = atoin(pChar->Beds, CSIZ_BEDROOMS);
   if (iBeds > 0)
   {
      sprintf(acTmp, "%*u", SIZ_BEDS, iBeds);
      memcpy(pOutbuf+OFF_BEDS, acTmp, SIZ_BEDS);
   }

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "4483330800", 10))
   //   iTmp = 0;
#endif

   // Baths
   iFBath = atoin(pChar->Baths, CSIZ_BATHS-1);
   if (iFBath > 0)
   {
      sprintf(acTmp, "%*u", SIZ_BATH_F, iFBath);
      memcpy(pOutbuf+OFF_BATH_F, acTmp, SIZ_BATH_F);

      // Half bath
      iTmp = pChar->Baths[CSIZ_BATHS-1] & 0x0F;
      if (iTmp > 0)
      {
         iHBath++;
         sprintf(acTmp, "%*u", SIZ_BATH_H, iHBath);
         memcpy(pOutbuf+OFF_BATH_H, acTmp, SIZ_BATH_H);
      } else
         memset(pOutbuf+OFF_BATH_H, ' ', SIZ_BATH_H);
   }

   // Garage spaces
   iTmp = atoin(pChar->GarStalls, CSIZ_GARAGE_STALLS);
   if (iTmp > 0)
   {
      sprintf(acTmp, "%d      ", iTmp);
      memcpy(pOutbuf+OFF_PARK_SPACE, acTmp, SIZ_PARK_SPACE);
   }

   // Pool/Spa
   if (pChar->Pool == 'Y')
      *(pOutbuf+OFF_POOL) = 'P';    // Pool

   // View - Y/N or blank
   if (pChar->View == 'Y')
      *(pOutbuf+OFF_VIEW) = 'A';    // HasView 12/19/2016 sn

   lLotSqft = atoln(pChar->LotSqft, CSIZ_LOT_SQFT);
   if (lLotSqft == 99999 || lLotSqft > 999999999)
      lLotSqft = 0;

   // Lot Acres - V99
   lLotAcres = atoln(pChar->Acres, CSIZ_ACREAGE);
   if (lLotAcres > 1)
   {
      if (!lLotSqft)
      {
         lLotSqft = (ULONG)((double)lLotAcres*SQFT_FACTOR_100);
         if (lLotSqft > 999999999)
            lLotSqft = 0;
      }
      lLotAcres *= 10;
   } else if (lLotSqft > 100)
      lLotAcres = (long)((double)lLotSqft*ACRES_FACTOR)/SQFT_PER_ACRE;

   if (lLotSqft > 100)
   {
      sprintf(acTmp, "%*u", SIZ_LOT_SQFT, lLotSqft);
      memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);
   }
   if (lLotAcres > 100)
   {
      sprintf(acTmp, "%*u", SIZ_LOT_ACRES, lLotAcres);
      memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);
   }

   // Units
   iTmp = atoin(pChar->Units, CSIZ_UNITS);
   if (iTmp > 0)
   {
      sprintf(acTmp, "%*u", SIZ_UNITS, iTmp);
      memcpy(pOutbuf+OFF_UNITS, acTmp, SIZ_UNITS);
   }

   // Rooms
   // Fireplace
   // Floor
   // Basement Sqft - not use
   // Quality/class
   // Others

   // Prop 8
   //if (pChar->ValChgCode == 'W')
   //   *(pOutbuf+OFF_PROP8_FLG) = 'Y';

   lCharMatch++;

   // Get next Char rec
   pRec = fgets(acRec, 1024, fdChar);

   return 0;
}

/***************************** Sdx_MergeSaleRec ******************************
 *
 * Merge new sale data into current sale.  Move other sales accordingly.
 * Only update seller if that is the sale from assessor.
 * This is the modified version of MergeSale() which will not update sale
 * without SaleAmt.  Only transfer is updated.
 *
 *****************************************************************************/

int Sdx_MergeSaleRec(char *pSCSale, char *pOutbuf, bool bSaleFlag, bool bUpdtXfer=true)
{
   long  lCurSaleDt, lLstSaleDt, lPrice, lLastAmt;

   SCSAL_REC *pSaleRec = (SCSAL_REC *)pSCSale;

   lCurSaleDt = atoin(pSaleRec->DocDate, SIZ_SALE1_DT);
   lLstSaleDt = atoin(pOutbuf+OFF_SALE1_DT, SIZ_SALE1_DT);

   // Ignore transfer
   if (pSaleRec->XferType == 'T')
      return 0;

   if (lCurSaleDt < lLstSaleDt)
      return -1;

   lPrice = atoin(pSaleRec->SalePrice, SIZ_SALE1_AMT);
   if (lCurSaleDt == lLstSaleDt)
   {
      lLastAmt = atoin(pOutbuf+OFF_SALE1_AMT, SIZ_SALE1_AMT);
      if (!lPrice && lLastAmt > 0)
         return 0;
   } else if (lPrice > 0)
   {
      // Move sale2 to sale3
      memcpy(pOutbuf+OFF_SALE3_DOC, pOutbuf+OFF_SALE2_DOC, SIZ_SALE3_DOC);
      memcpy(pOutbuf+OFF_SALE3_DT, pOutbuf+OFF_SALE2_DT, SIZ_SALE3_DT);
      memcpy(pOutbuf+OFF_SALE3_DOCTYPE, pOutbuf+OFF_SALE2_DOCTYPE, SIZ_SALE3_DOCTYPE);
      memcpy(pOutbuf+OFF_SALE3_AMT, pOutbuf+OFF_SALE2_AMT, SIZ_SALE3_AMT);
      *(pOutbuf+OFF_AR_CODE3) = *(pOutbuf+OFF_AR_CODE2);

      // Move sale1 to sale2
      memcpy(pOutbuf+OFF_SALE2_DOC, pOutbuf+OFF_SALE1_DOC, SIZ_SALE3_DOC);
      memcpy(pOutbuf+OFF_SALE2_DT, pOutbuf+OFF_SALE1_DT, SIZ_SALE3_DT);
      memcpy(pOutbuf+OFF_SALE2_DOCTYPE, pOutbuf+OFF_SALE1_DOCTYPE, SIZ_SALE3_DOCTYPE);
      memcpy(pOutbuf+OFF_SALE2_AMT, pOutbuf+OFF_SALE1_AMT, SIZ_SALE3_AMT);
      *(pOutbuf+OFF_AR_CODE2) = *(pOutbuf+OFF_AR_CODE1);
   }

   // Update current sale
   if (lPrice > 0)
   {
      memcpy(pOutbuf+OFF_SALE1_DOC, pSaleRec->DocNum, SIZ_SALE1_DOC);
      memcpy(pOutbuf+OFF_SALE1_DT, pSaleRec->DocDate, SIZ_SALE1_DT);
      if (lPrice > 100000)
         *(pOutbuf+OFF_SALE1_DOCTYPE) = '1';

      // Check for questionable sale amt
      if (lPrice > 1000)
      {
         memcpy(pOutbuf+OFF_SALE1_AMT, pSaleRec->SalePrice, SIZ_SALE1_AMT);

         // Seller
         if (bSaleFlag && pSaleRec->Seller1[0] > ' ')
            memcpy(pOutbuf+OFF_SELLER, pSaleRec->Seller1, SIZ_SELLER);
         else
            memset(pOutbuf+OFF_SELLER, ' ', SIZ_SELLER);

         *(pOutbuf+OFF_MULTI_APN) = pSaleRec->MultiSale_Flg;
      } else
      {
         memset(pOutbuf+OFF_SALE1_AMT, ' ', SIZ_SALE1_AMT);
         memset(pOutbuf+OFF_SELLER, ' ', SIZ_SELLER);
      }
   }

   // Update transfers
   if (bUpdtXfer)
   {
      lLstSaleDt = atoin(pOutbuf+OFF_TRANSFER_DT, SIZ_TRANSFER_DT);
      if (lCurSaleDt >= lLstSaleDt)
      {
         memcpy(pOutbuf+OFF_TRANSFER_DOC, pSaleRec->DocNum, SIZ_TRANSFER_DOC);
         memcpy(pOutbuf+OFF_TRANSFER_DT, pSaleRec->DocDate, SIZ_TRANSFER_DT);
      }
   }

   *(pOutbuf+OFF_AR_CODE1) = 'A';

   return 1;
}

int Sdx_MergeSCSale(char *pOutbuf, bool bUpdtXfer)
{
   static   char  acRec[1024], *pRec=NULL;
   int      iRet, iLoop;
   SCSAL_REC *pSale;

   // Get first Char rec for first call
   if (!pRec)
      pRec = fgets(acRec, 1024, fdSale);

   pSale = (SCSAL_REC *)&acRec[0];
   do
   {
      if (!pRec)
      {
         fclose(fdSale);
         fdSale = NULL;
         return 1;      // EOF
      }

      // Compare Apn
      iLoop = memcmp(pOutbuf, pSale->Apn, iApnLen);
      if (iLoop > 0)
      {
         if (bDebug)
            LogMsg0("Skip Sale rec  %.*s", iApnLen, pSale->Apn);
         pRec = fgets(acRec, 1024, fdSale);
         lSaleSkip++;
      }
   } while (iLoop > 0);

   // If not match, return
   if (iLoop)
      return 1;

   // Clear old sale before update
   ClearOldSale(pOutbuf);

   do
   {
#ifdef _DEBUG
//      if (!memcmp(pOutbuf, "10145008", 8))
//         iRet = 0;
#endif

      iRet = Sdx_MergeSaleRec(acRec, pOutbuf, true, bUpdtXfer);

      // Get next sale record
      pRec = fgets(acRec, 1024, fdSale);
      if (!pRec)
      {
         fclose(fdSale);
         fdSale = NULL;
         break;
      }

   } while (!memcmp(pOutbuf, pSale->Apn, iApnLen));

   lSaleMatch++;

   return 0;
}

/********************************* Sdx_MergeRoll *****************************
 *
 * Return 0 if successful, >0 if error
 *
 *****************************************************************************/

int Sdx_MergeRoll(char *pOutbuf, char *pRollRec, int iLen, int iFlag)
{
   SDX_ROLL *pRec;
   char     acTmp[256], acTmp1[256], acDocNum[32], *pTmp;
   LONGLONG lTmp;
   double   dTmp;
   int      iRet;

   pRec = (SDX_ROLL *)pRollRec;
   if (iFlag & CREATE_R01)
   {
      // Clear output buffer
      memset(pOutbuf, ' ', iRecLen);

      // Start copying data
      memcpy(pOutbuf, pRec->Apn, RSIZ_APN);
      memcpy(pOutbuf+OFF_CO_NUM, "37SDX", 5);

      // Format APN
      iRet = formatApn(pRec->Apn, acTmp, &myCounty);
      memcpy(pOutbuf+OFF_APN_D, acTmp, iRet);

      // Create MapLink
      iRet = formatMapLink(pRec->Apn, acTmp, &myCounty);
      memcpy(pOutbuf+OFF_MAPLINK, acTmp, iRet);

      // Create index map link
      if (getIndexPage(acTmp, acTmp1, &myCounty))
         memcpy(pOutbuf+OFF_IMAPLINK, acTmp1, iRet);

      // Year assessed
      memcpy(pOutbuf+OFF_YR_ASSD, myCounty.acYearAssd, 4);

      // Land
      long lLand = atoin(pRec->Land, RSIZ_LAND);
      if (lLand > 0)
      {
         sprintf(acTmp, "%*u", SIZ_LAND, lLand);
         memcpy(pOutbuf+OFF_LAND, acTmp, SIZ_LAND);
      }

      // Improve
      long lImpr = atoin(pRec->Impr, RSIZ_LAND);
      if (lImpr > 0)
      {
         sprintf(acTmp, "%*u", SIZ_IMPR, lImpr);
         memcpy(pOutbuf+OFF_IMPR, acTmp, SIZ_IMPR);
      }

      // Other value
      lTmp = atoin(pRec->PP_Val, RSIZ_LAND);
      if (lTmp > 0)
      {
         sprintf(acTmp, "%*u", SIZ_OTHER, lTmp);
         memcpy(pOutbuf+OFF_OTHER, acTmp, SIZ_OTHER);

         sprintf(acTmp, "%u          ", lTmp);
         memcpy(pOutbuf+OFF_PERSPROP, acTmp, SIZ_PERSPROP);
      }

      // Gross total
      lTmp += lLand+lImpr;
      if (lTmp > 0)
      {
         sprintf(acTmp, "%*u", SIZ_GROSS, lTmp);
         memcpy(pOutbuf+OFF_GROSS, acTmp, SIZ_GROSS);

         // Ratio
         if (lImpr > 0)
         {
            dTmp = (LONGLONG)lImpr*100/(lLand+lImpr)+0.5;
            sprintf(acTmp, "%*u", SIZ_RATIO, (long)dTmp);
            memcpy(pOutbuf+OFF_RATIO, acTmp, SIZ_RATIO);
         }
      }
   }

   // TRA
   lTmp = atoin(pRec->TRA, RSIZ_TRA);
   if (lTmp > 0)
   {
      iRet = sprintf(acTmp, "%.6d", lTmp);
      memcpy(pOutbuf+OFF_TRA, acTmp, iRet);
   }

   // Set full exemption flag
   if (pRec->Tax_Status == 'T')
      *(pOutbuf+OFF_FULL_EXEMPT) = ' ';
   else if (pRec->Tax_Status == 'N')
      *(pOutbuf+OFF_FULL_EXEMPT) = 'Y';

   *(pOutbuf+OFF_STATUS) = 'A';

   // Previous APN
   if (pRec->Old_Apn[0] > '0')
      memcpy(pOutbuf+OFF_PREV_APN, pRec->Old_Apn, RSIZ_APN);
   else
      memset(pOutbuf+OFF_PREV_APN, ' ', RSIZ_APN);

#ifdef _DEBUG
   //if (!memcmp(pRec->Apn, "1580102800", 8))
   //   iRet = 1;
#endif

   // Mailing
   Sdx_MergeMAdr(pOutbuf, pRollRec);

   // Situs
   Sdx_MergeSAdr(pOutbuf, pRollRec);

   // HO Exempt
   if (pRec->Exe_Code1 == '9')
      *(pOutbuf+OFF_HO_FL) = '1';      // 'Y'
   else
      *(pOutbuf+OFF_HO_FL) = '2';      // 'N'

   if (pRec->Exe_Code1 > '0')
   {
      lTmp = atoin(pRec->Exe_Amt1, RSIZ_EXE_AMT);
      lTmp += atoin(pRec->Exe_Amt2, RSIZ_EXE_AMT);
      lTmp += atoin(pRec->Exe_Amt3, RSIZ_EXE_AMT);
      sprintf(acTmp, "%*u", SIZ_EXE_TOTAL, lTmp);
      memcpy(pOutbuf+OFF_EXE_TOTAL, acTmp, SIZ_EXE_TOTAL);
   }

   // UseCode
   if (pRec->UseCode[0] > ' ')
   {
      memcpy(pOutbuf+OFF_USE_CO, pRec->UseCode, RSIZ_USE_CODE);
      updateStdUse(pOutbuf+OFF_USE_STD, pRec->UseCode, RSIZ_USE_CODE, pOutbuf);
   } else
      memcpy(pOutbuf+OFF_USE_STD, USE_UNASGN, SIZ_USE_STD);

   // Owner
   try {
      Sdx_MergeOwner(pOutbuf, pRollRec);
   } catch (...)
   {
      iRet = 1;
      LogMsg("***** Exception occured in Sdx_MergeOwner() APN=%.10s", pRec->Apn);
   }

   // Legal
   if (*(pOutbuf+OFF_LEGAL) == ' ')
   {
      lTmp = atoin(pRec->MapNum, RSIZ_MAP_NUMBER);
      if (lTmp > 0)
         sprintf(acTmp, "TR %d %.*s", (long)lTmp, RSIZ_DESCRIPTION, pRec->Description);
      else if (pRec->Description[0] >= 'A')
         sprintf(acTmp, "%.*s %.*s", RSIZ_MAP_NUMBER, pRec->MapNum, RSIZ_DESCRIPTION, pRec->Description);
      else
      {
         memcpy(acTmp, pRec->Description, RSIZ_DESCRIPTION);
         acTmp[RSIZ_DESCRIPTION] = 0;
      }

      if (acTmp[0] > ' ')
      {
         iRet = blankRem(acTmp);

         if (acTmp[iRet-1] == '\\')
            acTmp[iRet-1] = 0;
         if (iRet > 0)
         {
            replChar(acTmp, '|', '1');
            iRet = updateLegal(pOutbuf, acTmp);
            if (iRet > iMaxLegal)
               iMaxLegal = iRet;
         }
      }
   }

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "7602110500", 8))
   //   iRet = 0;
#endif

   // Acreage (V99): Lot sqft - Lot Acres
   lTmp = atoln(pRec->Acreage, RSIZ_ACREAGE);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*u", SIZ_LOT_ACRES, lTmp*10);
      memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);

      dTmp = (((double)lTmp/100)*SQFT_PER_ACRE + 0.5);
      sprintf(acTmp, "%*u", SIZ_LOT_SQFT, (unsigned long)dTmp);
      memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);
   } else
   {
      memset(pOutbuf+OFF_LOT_ACRES, ' ', SIZ_LOT_ACRES);
      memset(pOutbuf+OFF_LOT_SQFT, ' ', SIZ_LOT_SQFT);
   }

   // Zoning
   *(pOutbuf+OFF_ZONE) = pRec->Zoning;
   if (*(pOutbuf+OFF_ZONE_X1) == ' ')
      *(pOutbuf+OFF_ZONE_X1) = pRec->Zoning;

   // Transfer - MMDDYY
   if (memcmp(pRec->DocDate, "000000", 6) > 0)
   {
      // Apply transfer
      pTmp = dateConversion(pRec->DocDate, acTmp, MMDDYY2);
      if (pTmp)
      {
         memset(pOutbuf+OFF_TRANSFER_DT, ' ', SIZ_TRANSFER_DT);
         memset(pOutbuf+OFF_TRANSFER_DOC, ' ', RSIZ_REC_DOCNUM);
         memcpy(pOutbuf+OFF_TRANSFER_DT, acTmp, SIZ_TRANSFER_DT);
         if (pRec->DocNum[0] > ' ')
         {
            iRet = Sdx_FormatDoc(acDocNum, pRec->DocNum, acTmp);
            memcpy(pOutbuf+OFF_TRANSFER_DOC, acDocNum, RSIZ_REC_DOCNUM+1);
         }
      }
   }

   // Fix sale data - one time only
   //memset(pOutbuf+OFF_TRANSFER_DOC+7, ' ', 5);
   //memset(pOutbuf+OFF_SALE1_DOC+7, ' ', 5);
   //memset(pOutbuf+OFF_SALE2_DOC+7, ' ', 5);
   //memset(pOutbuf+OFF_SALE3_DOC+7, ' ', 5);

   return 0;
}

/******************************** Sdx_ExtrLien ******************************
 *
 * Extract lien values to LIENEXTR format:
 *
 ****************************************************************************/

int Sdx_ExtrLien()
{
   char  acRec[2400], acOutbuf[512], acTmp[256], acTmpFile[_MAX_PATH];
   char  acLienExtr[_MAX_PATH], acPrevApn[16], *pTmp;

   FILE     *fdExtr;
   LIENEXTR *pLienRec = (LIENEXTR *)&acOutbuf;
   SDX_LIEN *pRec = (SDX_LIEN *)acRec;

   LogMsg0("Extract lien values");

   // Open Roll file
   LogMsg("Open Roll file %s", acRollFile);
   fdRoll = fopen(acRollFile, "r");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", acRollFile);
      return -1;
   }

   // Open extr file
   sprintf(acTmpFile, acLienTmpl, myCounty.acCntyCode, "Sdx");
   LogMsg("Open Lien extract file %s", acTmpFile);
   fdExtr = fopen(acTmpFile, "w");
   if (fdExtr == NULL)
   {
      LogMsg("***** Error creating extract file: %s\n", acTmpFile);
      return -2;
   }

   long  lLand, lImpr, lPP, lTmp, iTmp;
   long  lGross, lOther, lTotalExe, lCnt, lSkip, lSameApn;
   long  lExe1, lExe2, lExe3;

   acPrevApn[0] = 0;
   lSameApn = lSkip = lCnt = 0;

   while (!feof(fdRoll))
   {
      pTmp = fgets(acRec, 2400, fdRoll);
      if (!pTmp)
         break;

      if (!memcmp(acPrevApn, pRec->Apn, iApnLen))
      {
         LogMsg("Same APN: %.12s", acRec);
         lSameApn++;
         continue;
      }

      // Reset output buffer
      memset(acOutbuf, ' ', sizeof(LIENEXTR));

      // Save current APN
      memcpy(acPrevApn, pRec->Apn, iApnLen);
      memcpy(pLienRec->acApn, pRec->Apn, iApnLen);

      // TRA
      lTmp = atoin(pRec->TRA, RSIZ_TRA);
      if (lTmp > 0)
      {
         iTmp = sprintf(acTmp, "%.6d", lTmp);
         memcpy(pLienRec->acTRA, acTmp, iTmp);
      }

      // Following fields are not populated, so ignore them 7/1/2009 spn
      //lTmp = atoin(pRec->BusInv, RSIZ_CURR_BUS_INV_VALUE);
      //lTmp = atoin(pRec->Fixt, RSIZ_LAND);
      //lTmp = atoin(pRec->Bus_PP, RSIZ_LAND);
      lLand = atoin(pRec->Land, LSIZ_LAND_VALUE);
      lImpr = atoin(pRec->Impr, LSIZ_IMPR_VALUE);
      lPP   = atoin(pRec->PP_Val, LSIZ_PP_VALUE);

      // Land
      if (lLand > 0)
      {
         sprintf(acTmp, "%*u", SIZ_LAND, lLand);
         memcpy(pLienRec->acLand, acTmp, SIZ_LAND);
      }

      // Structure impr
      if (lImpr > 0)
      {
         sprintf(acTmp, "%*u", SIZ_LAND, lImpr);
         memcpy(pLienRec->acImpr, acTmp, SIZ_LAND);
      }

      // Personal property/Business inventory
      if (lPP > 0)
      {
         sprintf(acTmp, "%*u", SIZ_LAND, lPP);
         memcpy(pLienRec->acPP_Val, acTmp, SIZ_LAND);
         memcpy(pLienRec->acOther, acTmp, SIZ_LAND);
      }

      // Other impr
      lOther = lPP;

      // Gross
      lGross = lOther + lLand + lImpr;
      if (lGross > 0)
      {
         sprintf(acTmp, "%*u", SIZ_LAND, lGross);
         memcpy(pLienRec->acGross, acTmp, SIZ_LAND);
      }

      // Impr - Ratio
      if (lImpr > 0)
      {
         sprintf(acTmp, "%*u", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
         memcpy(pLienRec->acRatio, acTmp, SIZ_RATIO);
      }

      // HO Exempt
      if (pRec->Exe_Code1 == '9')
         pLienRec->acHO[0] = '1';      // 'Y'
      else
         pLienRec->acHO[0] = '2';      // 'N'

      // Exempt
      lExe1 = atoin(pRec->Exe_Amt1, LSIZ_EXE_VALUE_1);
      lExe2 = atoin(pRec->Exe_Amt2, LSIZ_EXE_VALUE_1);
      lExe3 = atoin(pRec->Exe_Amt3, LSIZ_EXE_VALUE_1);
      lTotalExe = lExe1+lExe2+lExe3;
      if (lTotalExe > 0)
      {
         iTmp = sprintf(acTmp, "%*u", sizeof(pLienRec->acExAmt), lTotalExe);
         memcpy(pLienRec->acExAmt, acTmp, iTmp);

         if (lExe1 > 0)
         {
            pLienRec->extra.Sdx.Exe_Code1 = pRec->Exe_Code1;
            iTmp = sprintf(acTmp, "%*u", sizeof(pLienRec->extra.Sdx.Exe_Amt1), lExe1);
            memcpy(pLienRec->extra.Sdx.Exe_Amt1, acTmp, iTmp);
         }
         if (lExe2 > 0)
         {
            pLienRec->extra.Sdx.Exe_Code2 = pRec->Exe_Code2;
            iTmp = sprintf(acTmp, "%*u", sizeof(pLienRec->extra.Sdx.Exe_Amt1), lExe2);
            memcpy(pLienRec->extra.Sdx.Exe_Amt2, acTmp, iTmp);
         }
         if (lExe3 > 0)
         {
            pLienRec->extra.Sdx.Exe_Code3 = pRec->Exe_Code3;
            iTmp = sprintf(acTmp, "%*u", sizeof(pLienRec->extra.Sdx.Exe_Amt1), lExe3);
            memcpy(pLienRec->extra.Sdx.Exe_Amt3, acTmp, iTmp);
         }
      }

      // Taxability status
      if (pRec->Tax_Status == 'T')
         pLienRec->extra.Sdx.Tax_Status = 'Y';
      else if (pRec->Tax_Status == 'N')
      {
         pLienRec->extra.Sdx.Tax_Status = 'N';
         pLienRec->SpclFlag = LX_FULLEXE_FLG;
      }

      pLienRec->LF[0] = '\n';
      pLienRec->LF[1] = 0;

      // Write to file
      fputs(acOutbuf, fdExtr);

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdRoll)
      fclose(fdRoll);
   if (fdExtr)
      fclose(fdExtr);

   LogMsg("Total records output:     %d", lCnt);
   LogMsg("Total records same APN:   %d", lSameApn);
   LogMsg("Total records skip:       %d", lSkip);

   // Sort output file
   sprintf(acLienExtr, acLienTmpl, myCounty.acCntyCode, myCounty.acCntyCode);

   /* No need to sort since SDX lien roll always sorted - remove this comment when needed
   sprintf(acTmp, "S(1,12,C,A) F(TXT) DUPO(1,12)");
   LogMsg("Sort lien extract output file %s --> %s", acTmpFile, acLienExtr);
   int iRet = sortFile(acTmpFile, acLienExtr, acTmp);
   if (iRet <= 0)
   {
      LogMsg("***** ERROR sorting lien extract file %s to %s", acTmpFile, acLienExtr);
      iRet = -1;
   } else
      LogMsg("Total records after sort: %d", iRet);
   */
   // Rename instead
   rename(acTmpFile, acLienExtr);

   return 0;
}

int Sdx_ExtrLien_CSV()
{
   char  acRec[2400], acOutbuf[512], acTmp[256], acTmpFile[_MAX_PATH];
   char  acLienExtr[_MAX_PATH], *pTmp;

   FILE     *fdExtr;
   LIENEXTR *pLienRec = (LIENEXTR *)&acOutbuf;

   LogMsg0("Extract lien values");

   // Open Roll file
   LogMsg("Open Roll file %s", acRollFile);
   fdRoll = fopen(acRollFile, "r");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", acRollFile);
      return -1;
   }

   // Open extr file
   sprintf(acTmpFile, acLienTmpl, myCounty.acCntyCode, "Sdx");
   LogMsg("Open Lien extract file %s", acTmpFile);
   fdExtr = fopen(acTmpFile, "w");
   if (fdExtr == NULL)
   {
      LogMsg("***** Error creating extract file: %s\n", acTmpFile);
      return -2;
   }

   long  lLand, lImpr, lPP, lTmp, iTmp, iRet;
   long  lExe1, lExe2, lExe3, lCnt, lSkip, lSameApn;
   LONGLONG  lGross, lOther, lTotalExe;

   lSameApn = lSkip = lCnt = 0;

   while (!feof(fdRoll))
   {
      pTmp = fgets(acRec, 2400, fdRoll);
      if (!pTmp)
         break;

      iRet = ParseStringNQ1(acRec, cDelim, LDR_TRANSACTION_DATE+1, apTokens);
      if (iRet < LDR_TRANSACTION_DATE)
      {
         LogMsg("*** Bad LDR record (%d)", lCnt);
         continue;
      }

      // Reset output buffer
      memset(acOutbuf, ' ', sizeof(LIENEXTR));

      // Save current APN
      memcpy(pLienRec->acApn, apTokens[LDR_APN], iApnLen);

      // TRA
      lTmp = atoin(apTokens[LDR_TRA], RSIZ_TRA);
      if (lTmp > 0)
      {
         iTmp = sprintf(acTmp, "%.6d", lTmp);
         memcpy(pLienRec->acTRA, acTmp, iTmp);
      }

      // Value
      lLand = atol(apTokens[LDR_CURR_LAND_VALUE]);
      lImpr = atol(apTokens[LDR_CURR_IMPR_VALUE]);
      lPP   = atol(apTokens[LDR_CURR_PP_VALUE]);

      // Land
      if (lLand > 0)
      {
         sprintf(acTmp, "%*u", SIZ_LAND, lLand);
         memcpy(pLienRec->acLand, acTmp, SIZ_LAND);
      }

      // Structure impr
      if (lImpr > 0)
      {
         sprintf(acTmp, "%*u", SIZ_LAND, lImpr);
         memcpy(pLienRec->acImpr, acTmp, SIZ_LAND);
      }

      // Personal property/Business inventory
      if (lPP > 0)
      {
         sprintf(acTmp, "%*u", SIZ_LAND, lPP);
         memcpy(pLienRec->acPP_Val, acTmp, SIZ_LAND);
         memcpy(pLienRec->acOther, acTmp, SIZ_LAND);
      }

      // Other impr
      lOther = lPP;

      // Gross
      lGross = lOther + lLand + lImpr;
      if (lGross > 0)
      {
         sprintf(acTmp, "%*u", SIZ_LAND, lGross);
         memcpy(pLienRec->acGross, acTmp, SIZ_LAND);
      }

      // Impr - Ratio
      if (lImpr > 0)
      {
         sprintf(acTmp, "%*u", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
         memcpy(pLienRec->acRatio, acTmp, SIZ_RATIO);
      }

      // HO Exempt
      if (*apTokens[LDR_EXE_CODE_1] == '9')
         pLienRec->acHO[0] = '1';      // 'Y'
      else
         pLienRec->acHO[0] = '2';      // 'N'

      // Exempt
      lExe1 = atol(apTokens[LDR_EXE_VALUE_1]);
      lExe2 = atol(apTokens[LDR_EXE_VALUE_2]);
      lExe3 = atol(apTokens[LDR_EXE_VALUE_3]);
      lTotalExe = lExe1+lExe2+lExe3;
      if (lTotalExe > 0)
      {
         iTmp = sprintf(acTmp, "%*u", sizeof(pLienRec->acExAmt), lTotalExe);
         memcpy(pLienRec->acExAmt, acTmp, iTmp);

         if (lExe1 > 0)
         {
            pLienRec->extra.Sdx.Exe_Code1 = *apTokens[LDR_EXE_CODE_1];
            iTmp = sprintf(acTmp, "%*u", sizeof(pLienRec->extra.Sdx.Exe_Amt1), lExe1);
            memcpy(pLienRec->extra.Sdx.Exe_Amt1, acTmp, iTmp);
         }
         if (lExe2 > 0)
         {
            pLienRec->extra.Sdx.Exe_Code2 = *apTokens[LDR_EXE_CODE_2];
            iTmp = sprintf(acTmp, "%*u", sizeof(pLienRec->extra.Sdx.Exe_Amt1), lExe2);
            memcpy(pLienRec->extra.Sdx.Exe_Amt2, acTmp, iTmp);
         }
         if (lExe3 > 0)
         {
            pLienRec->extra.Sdx.Exe_Code3 = *apTokens[LDR_EXE_CODE_3];
            iTmp = sprintf(acTmp, "%*u", sizeof(pLienRec->extra.Sdx.Exe_Amt1), lExe3);
            memcpy(pLienRec->extra.Sdx.Exe_Amt3, acTmp, iTmp);
         }
      }

      // Taxability status
      if (*apTokens[LDR_TAX_STATUS] == 'T')
         pLienRec->extra.Sdx.Tax_Status = 'Y';
      else if (*apTokens[LDR_TAX_STATUS] == 'N')
      {
         pLienRec->extra.Sdx.Tax_Status = 'N';
         pLienRec->SpclFlag = LX_FULLEXE_FLG;
      }

      pLienRec->LF[0] = '\n';
      pLienRec->LF[1] = 0;

      // Write to file
      fputs(acOutbuf, fdExtr);

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdRoll)
      fclose(fdRoll);
   if (fdExtr)
      fclose(fdExtr);

   LogMsg("Total records output:     %d", lCnt);
   LogMsg("Total records same APN:   %d", lSameApn);
   LogMsg("Total records skip:       %d", lSkip);

   // Sort output file
   sprintf(acLienExtr, acLienTmpl, myCounty.acCntyCode, myCounty.acCntyCode);

   /* No need to sort since SDX lien roll always sorted - remove this comment when needed
   sprintf(acTmp, "S(1,12,C,A) F(TXT) DUPO(1,12)");
   LogMsg("Sort lien extract output file %s --> %s", acTmpFile, acLienExtr);
   int iRet = sortFile(acTmpFile, acLienExtr, acTmp);
   if (iRet <= 0)
   {
      LogMsg("***** ERROR sorting lien extract file %s to %s", acTmpFile, acLienExtr);
      iRet = -1;
   } else
      LogMsg("Total records after sort: %d", iRet);
   */
   // Rename instead
   rename(acTmpFile, acLienExtr);

   return 0;
}

/********************************* Sdx_MergeLien *****************************
 *
 * Return 0 if successful, >0 if error
 *
 *****************************************************************************

int Sdx_MergeLien(char *pOutbuf, char *pRollRec, int iLen, int iFlag)
{
   SDX_LIEN *pRec;
   char     acTmp[256], acTmp1[256], acDocNum[32], *pTmp;
   long     lTmp;
   double   dTmp;
   int      iRet;

   pRec = (SDX_LIEN *)pRollRec;
   // Clear output buffer
   memset(pOutbuf, ' ', iRecLen);

   // Start copying data
   memcpy(pOutbuf, pRec->Apn, RSIZ_APN);
   memcpy(pOutbuf+OFF_CO_NUM, "37SDX", 5);

   // Format APN
   iRet = formatApn(pRec->Apn, acTmp, &myCounty);
   memcpy(pOutbuf+OFF_APN_D, acTmp, iRet);

   // Create MapLink
   iRet = formatMapLink(pRec->Apn, acTmp, &myCounty);
   memcpy(pOutbuf+OFF_MAPLINK, acTmp, iRet);

   // Create index map link
   if (getIndexPage(acTmp, acTmp1, &myCounty))
      memcpy(pOutbuf+OFF_IMAPLINK, acTmp1, iRet);

   // Year assessed
   memcpy(pOutbuf+OFF_YR_ASSD, myCounty.acYearAssd, 4);

   // Land
   long lLand = atoin(pRec->Land, RSIZ_LAND);
   if (lLand > 0)
   {
      sprintf(acTmp, "%*u", SIZ_LAND, lLand);
      memcpy(pOutbuf+OFF_LAND, acTmp, SIZ_LAND);
   }

   // Improve
   long lImpr = atoin(pRec->Impr, RSIZ_LAND);
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*u", SIZ_IMPR, lImpr);
      memcpy(pOutbuf+OFF_IMPR, acTmp, SIZ_IMPR);
   }

   // Other value
   lTmp = atoin(pRec->PP_Val, RSIZ_LAND);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*u", SIZ_OTHER, lTmp);
      memcpy(pOutbuf+OFF_OTHER, acTmp, SIZ_OTHER);
   }

   // Gross total
   lTmp += lLand+lImpr;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*u", SIZ_GROSS, lTmp);
      memcpy(pOutbuf+OFF_GROSS, acTmp, SIZ_GROSS);

      // Ratio
      if (lImpr > 0)
      {
         dTmp = (LONGLONG)lImpr*100/(lLand+lImpr)+0.5;
         sprintf(acTmp, "%*u", SIZ_RATIO, (long)dTmp);
         memcpy(pOutbuf+OFF_RATIO, acTmp, SIZ_RATIO);
      }
   }

   // Taxability status
   if (pRec->Tax_Status == 'T')
      *(pOutbuf+OFF_TAX_CODE) = 'Y';
   if (pRec->Tax_Status == 'N')
      *(pOutbuf+OFF_TAX_CODE) = 'N';

   *(pOutbuf+OFF_STATUS) = 'A';

   // Previous APN
   if (pRec->Old_Apn[0] > ' ')
      memcpy(pOutbuf+OFF_PREV_APN, pRec->Old_Apn, RSIZ_APN);

   // Mailing
   Sdx_MergeMAdr(pOutbuf, pRollRec);

   // Situs
   Sdx_MergeSAdr(pOutbuf, pRollRec);

#ifdef _DEBUG
//   if (!memcmp(pRec->Apn, "10145008", 8))
//      iRet = 1;
#endif

   // HO Exempt
   if (pRec->Exe_Code1 == '9')
      *(pOutbuf+OFF_HO_FL) = '1';      // 'Y'
   else
      *(pOutbuf+OFF_HO_FL) = '2';      // 'N'

   if (pRec->Exe_Code1 > '0')
   {
      lTmp = atoin(pRec->Exe_Amt1, RSIZ_EXE_AMT);
      lTmp += atoin(pRec->Exe_Amt2, RSIZ_EXE_AMT);
      lTmp += atoin(pRec->Exe_Amt3, RSIZ_EXE_AMT);
      sprintf(acTmp, "%*u", SIZ_EXE_TOTAL, lTmp);
      memcpy(pOutbuf+OFF_EXE_TOTAL, acTmp, SIZ_EXE_TOTAL);
   }

   // UseCode
   memcpy(pOutbuf+OFF_USE_CO, pRec->UseCode, RSIZ_USE_CODE);

   // TRA
   memcpy(pOutbuf+OFF_TRA, pRec->TRA, RSIZ_TRA);

   // Owner
   try {
      Sdx_MergeOwner(pOutbuf, pRollRec);
   } catch (...)
   {
      iRet = 1;
      LogMsg("***** Exception occured in Sdx_MergeOwner() APN=%.10s", pRec->Apn);
   }

   // Legal
   memcpy(acTmp, pRec->MapNum, RSIZ_MAP_NUMBER);
   memcpy(&acTmp[RSIZ_MAP_NUMBER], pRec->Description, RSIZ_DESCRIPTION);
   blankRem(acTmp, RSIZ_MAP_NUMBER+RSIZ_DESCRIPTION);
   iRet = strlen(acTmp);
   if (acTmp[iRet-1] == '\\')
      iRet--;
   if (iRet > SIZ_LEGAL)
      iRet = SIZ_LEGAL;
   memcpy(pOutbuf+OFF_LEGAL, acTmp, iRet);

   // Acreage (V99): Lot sqft - Lot Acres
   lTmp = atoin(pRec->Acreage, RSIZ_ACREAGE);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*u", SIZ_LOT_ACRES, lTmp*10);
      memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);

      dTmp = (((double)lTmp/100)*SQFT_PER_ACRE + 0.5);
      sprintf(acTmp, "%*u", SIZ_LOT_SQFT, (unsigned long)dTmp);
      memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);
   }

   // Zoning
   *(pOutbuf+OFF_ZONE) = pRec->Zoning;

   // Transfer - MMDDYY
   if (memcmp(pRec->DocDate, "000000", 6) > 0)
   {
      // Apply transfer
      pTmp = dateConversion(pRec->DocDate, acTmp, MMDDYY2);
      if (pTmp)
      {
         memcpy(pOutbuf+OFF_TRANSFER_DT, acTmp, SIZ_TRANSFER_DT);
         if (pRec->DocNum[0] > ' ')
         {
            iRet = Sdx_FormatDoc(acDocNum, pRec->DocNum, acTmp);
            memcpy(pOutbuf+OFF_TRANSFER_DOC, acDocNum, RSIZ_REC_DOCNUM+1);
         }
      }

   }
   return 0;
}

/********************************* Sdx_MergeLien *****************************
 *
 * Merge lien record using MPR08 format
 *
 * Return 0 if successful, >0 if error
 *
 *****************************************************************************/

int Sdx_MergeLien(char *pOutbuf, char *pRollRec, int iLen, int iFlag)
{
   SDX_LIEN *pRec;
   char     acTmp[256], acTmp1[256], acDocNum[32], *pTmp;
   LONGLONG lTmp;
   double   dTmp;
   int      iRet;

   pRec = (SDX_LIEN *)pRollRec;
   // Clear output buffer
   memset(pOutbuf, ' ', iRecLen);

   // Start copying data
   memcpy(pOutbuf, pRec->Apn, RSIZ_APN);
   memcpy(pOutbuf+OFF_CO_NUM, "37SDX", 5);

   // Format APN
   iRet = formatApn(pRec->Apn, acTmp, &myCounty);
   memcpy(pOutbuf+OFF_APN_D, acTmp, iRet);

   // Create MapLink
   iRet = formatMapLink(pRec->Apn, acTmp, &myCounty);
   memcpy(pOutbuf+OFF_MAPLINK, acTmp, iRet);

   // Create index map link
   if (getIndexPage(acTmp, acTmp1, &myCounty))
      memcpy(pOutbuf+OFF_IMAPLINK, acTmp1, iRet);

   // Year assessed
   memcpy(pOutbuf+OFF_YR_ASSD, myCounty.acYearAssd, 4);

   // Land
   long lLand = atoin(pRec->Land, LSIZ_LAND_VALUE);
   if (lLand > 0)
   {
      sprintf(acTmp, "%*u", SIZ_LAND, lLand);
      memcpy(pOutbuf+OFF_LAND, acTmp, SIZ_LAND);
   }

   // Improve
   long lImpr = atoin(pRec->Impr, LSIZ_IMPR_VALUE);
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*u", SIZ_IMPR, lImpr);
      memcpy(pOutbuf+OFF_IMPR, acTmp, SIZ_IMPR);
   }

   // Other value
   lTmp = atoin(pRec->PP_Val, LSIZ_PP_VALUE);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*u", SIZ_OTHER, lTmp);
      memcpy(pOutbuf+OFF_OTHER, acTmp, SIZ_OTHER);

      sprintf(acTmp, "%u          ", lTmp);
      memcpy(pOutbuf+OFF_PERSPROP, acTmp, SIZ_PERSPROP);
   }

   // Gross total
   lTmp += lLand+lImpr;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*u", SIZ_GROSS, lTmp);
      memcpy(pOutbuf+OFF_GROSS, acTmp, SIZ_GROSS);

      // Ratio
      if (lImpr > 0)
      {
         dTmp = (LONGLONG)lImpr*100/(lLand+lImpr)+0.5;
         sprintf(acTmp, "%*u", SIZ_RATIO, (long)dTmp);
         memcpy(pOutbuf+OFF_RATIO, acTmp, SIZ_RATIO);
      }
   }

   // Set full exemption flag
   if (pRec->Tax_Status == 'N')
      *(pOutbuf+OFF_FULL_EXEMPT) = 'Y';

   *(pOutbuf+OFF_STATUS) = 'A';

   // Previous APN
   if (pRec->Old_Apn[0] > '0')
      memcpy(pOutbuf+OFF_PREV_APN, pRec->Old_Apn, LSIZ_OLD_PARCEL_NO);

   // Mailing
   Sdx_MergeMAdr_Mpr08(pOutbuf, pRollRec);

   // Situs
   Sdx_MergeSAdr_Mpr08(pOutbuf, pRollRec);

#ifdef _DEBUG
//   if (!memcmp(pRec->Apn, "10145008", 8))
//      iRet = 1;
#endif

   // HO Exempt
   if (pRec->Exe_Code1 == '9')
      *(pOutbuf+OFF_HO_FL) = '1';      // 'Y'
   else
      *(pOutbuf+OFF_HO_FL) = '2';      // 'N'

   if (pRec->Exe_Code1 > '0')
   {
      lTmp = atoin(pRec->Exe_Amt1, LSIZ_EXE_VALUE_1);
      lTmp += atoin(pRec->Exe_Amt2, LSIZ_EXE_VALUE_1);
      lTmp += atoin(pRec->Exe_Amt3, LSIZ_EXE_VALUE_1);
      sprintf(acTmp, "%*u", SIZ_EXE_TOTAL, lTmp);
      memcpy(pOutbuf+OFF_EXE_TOTAL, acTmp, SIZ_EXE_TOTAL);

      *(pOutbuf+OFF_EXE_CD1) = pRec->Exe_Code1;
      *(pOutbuf+OFF_EXE_CD2) = pRec->Exe_Code2;
      *(pOutbuf+OFF_EXE_CD3) = pRec->Exe_Code3;
   }

   // UseCode
   if (pRec->UseCode[0] > ' ')
   {
      memcpy(pOutbuf+OFF_USE_CO, pRec->UseCode, LSIZ_USE_CODE);
      updateStdUse(pOutbuf+OFF_USE_STD, pRec->UseCode, LSIZ_USE_CODE, pOutbuf);
   } else
      memcpy(pOutbuf+OFF_USE_STD, USE_UNASGN, SIZ_USE_STD);

   // TRA
   lTmp = atoin(pRec->TRA, LSIZ_TRA);
   if (lTmp > 0)
   {
      iRet = sprintf(acTmp, "%.6d", lTmp);
      memcpy(pOutbuf+OFF_TRA, acTmp, iRet);
   }
   //memcpy(pOutbuf+OFF_TRA, pRec->TRA, LSIZ_TRA);

   // Owner
   try {
      Sdx_MergeOwner(pOutbuf, pRollRec);
   } catch (...)
   {
      iRet = 1;
      LogMsg("***** Exception occured in Sdx_MergeOwner() APN=%.10s", pRec->Apn);
   }

   // Legal
   memcpy(acTmp, pRec->MapNum, LSIZ_MAP_NUMBER);
   memcpy(&acTmp[LSIZ_MAP_NUMBER], pRec->Description, LSIZ_PROP_DESCR);
   iRet =blankRem(acTmp, LSIZ_MAP_NUMBER+LSIZ_PROP_DESCR);
   if (iRet > 0)
   {
      if (acTmp[iRet-1] == '\\')
         acTmp[iRet-1] = 0;
      replChar(acTmp, '|', '1');
      iRet = updateLegal(pOutbuf, acTmp);
      if (iRet > iMaxLegal)
         iMaxLegal = iRet;
   }

   // Acreage (V99): Lot sqft - Lot Acres
   memcpy(acTmp, pRec->Acreage, LSIZ_ACREAGE);
   acTmp[LSIZ_ACREAGE] = 0;
   dTmp = atof(acTmp);
   if (dTmp > 0.0)
   {
      lTmp = (long)(dTmp*1000.0);
      sprintf(acTmp, "%*u", SIZ_LOT_ACRES, lTmp);
      memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);

      dTmp = (dTmp*SQFT_PER_ACRE) + 0.5;
      sprintf(acTmp, "%*u", SIZ_LOT_SQFT, (unsigned long)dTmp);
      memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);
   }

   // Zoning
   *(pOutbuf+OFF_ZONE) = pRec->Zoning;
   *(pOutbuf+OFF_ZONE_X1) = pRec->Zoning;

   // Transfer - MMDDYY
   lTmp = atoin(pRec->DocDate, LSIZ_REC_DOCDATE);
   if (lTmp > 0)
   {
      if (pRec->DocDate[0] == ' ')
         pRec->DocDate[0] = '0';

      // Apply transfer
      pTmp = dateConversion(&pRec->DocDate[0], acTmp, MMDDYY2);
      if (pTmp)
      {
         memcpy(pOutbuf+OFF_TRANSFER_DT, acTmp, SIZ_TRANSFER_DT);
         if (pRec->DocNum[0] > ' ')
         {
            iRet = Sdx_FormatDoc(acDocNum, pRec->DocNum, acTmp);
            memcpy(pOutbuf+OFF_TRANSFER_DOC, acDocNum, LSIZ_REC_DOCNUM+1);
         }
      }

   }
   return 0;
}

/********************************* Sdx_MergeLien *****************************
 *
 * Merge lien record using CSV format
 *
 * Return 0 if successful, >0 if error
 *
 *****************************************************************************/

int Sdx_MergeLien(char *pOutbuf, char *pRollRec)
{
   char     acTmp[256], acTmp1[256], *pTmp;
   LONGLONG lTmp;
   double   dTmp;
   int      iRet;

   iRet = ParseStringNQ1(pRollRec, cDelim, LDR_TRANSACTION_DATE+1, apTokens);
   if (iRet < LDR_TRANSACTION_DATE)
   {
      LogMsg("*** Bad LDR record APN=%s", apTokens[LDR_APN]);
      return -1;
   }

   // Clear output buffer
   memset(pOutbuf, ' ', iRecLen);

   // Start copying data
   vmemcpy(pOutbuf, apTokens[LDR_APN], RSIZ_APN);
   memcpy(pOutbuf+OFF_CO_NUM, "37SDX", 5);

   // Format APN
   iRet = formatApn(apTokens[LDR_APN], acTmp, &myCounty);
   memcpy(pOutbuf+OFF_APN_D, acTmp, iRet);

   // Create MapLink
   iRet = formatMapLink(apTokens[LDR_APN], acTmp, &myCounty);
   memcpy(pOutbuf+OFF_MAPLINK, acTmp, iRet);

   // Create index map link
   if (getIndexPage(acTmp, acTmp1, &myCounty))
      memcpy(pOutbuf+OFF_IMAPLINK, acTmp1, iRet);

   // Year assessed
   memcpy(pOutbuf+OFF_YR_ASSD, myCounty.acYearAssd, 4);

   // Land
   long lLand = atol(apTokens[LDR_CURR_LAND_VALUE]);
   if (lLand > 0)
   {
      sprintf(acTmp, "%*u", SIZ_LAND, lLand);
      memcpy(pOutbuf+OFF_LAND, acTmp, SIZ_LAND);
   }

   // Improve
   long lImpr = atol(apTokens[LDR_CURR_IMPR_VALUE]);
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*u", SIZ_IMPR, lImpr);
      memcpy(pOutbuf+OFF_IMPR, acTmp, SIZ_IMPR);
   }

   // Other value
   lTmp = atol(apTokens[LDR_CURR_PP_VALUE]);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*u", SIZ_OTHER, lTmp);
      memcpy(pOutbuf+OFF_OTHER, acTmp, SIZ_OTHER);

      sprintf(acTmp, "%u          ", lTmp);
      memcpy(pOutbuf+OFF_PERSPROP, acTmp, SIZ_PERSPROP);
   }

   // Gross total
   lTmp += lLand+lImpr;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*u", SIZ_GROSS, lTmp);
      memcpy(pOutbuf+OFF_GROSS, acTmp, SIZ_GROSS);

      // Ratio
      if (lImpr > 0)
      {
         dTmp = (LONGLONG)lImpr*100/(lLand+lImpr)+0.5;
         sprintf(acTmp, "%*u", SIZ_RATIO, (long)dTmp);
         memcpy(pOutbuf+OFF_RATIO, acTmp, SIZ_RATIO);
      }
   }

   // Set full exemption flag
   if (*apTokens[LDR_TAX_STATUS] == 'N')
      *(pOutbuf+OFF_FULL_EXEMPT) = 'Y';

   *(pOutbuf+OFF_STATUS) = 'A';

   // Previous APN
   if (*apTokens[LDR_OLD_PARCEL_NO] > '0')
      memcpy(pOutbuf+OFF_PREV_APN, apTokens[LDR_OLD_PARCEL_NO], strlen(apTokens[LDR_OLD_PARCEL_NO]));

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "1025302211", 10))
   //   iRet = 1;
#endif

   // Mailing
   Sdx_MergeMAdr_LDR(pOutbuf, apTokens[LDR_MAIL_ADDR], apTokens[LDR_MAIL_ZIP]);

   // Situs
   Sdx_MergeSAdr_LDR(pOutbuf, apTokens[LDR_SITUS_NUMBER], apTokens[LDR_SITUS_STREET]);

   // HO Exempt
   if (*apTokens[LDR_EXE_CODE_1] == '9')
      *(pOutbuf+OFF_HO_FL) = '1';      // 'Y'
   else
      *(pOutbuf+OFF_HO_FL) = '2';      // 'N'

   if (*apTokens[LDR_EXE_CODE_1] > '0')
   {
      lTmp  = atol(apTokens[LDR_EXE_VALUE_1]);
      lTmp += atol(apTokens[LDR_EXE_VALUE_2]);
      lTmp += atol(apTokens[LDR_EXE_VALUE_3]);
      sprintf(acTmp, "%*u", SIZ_EXE_TOTAL, lTmp);
      memcpy(pOutbuf+OFF_EXE_TOTAL, acTmp, SIZ_EXE_TOTAL);

      *(pOutbuf+OFF_EXE_CD1) = *apTokens[LDR_EXE_CODE_1];
      if (*apTokens[LDR_EXE_CODE_2] > ' ')
         *(pOutbuf+OFF_EXE_CD2) = *apTokens[LDR_EXE_CODE_2];
      if (*apTokens[LDR_EXE_CODE_3] > ' ')
         *(pOutbuf+OFF_EXE_CD3) = *apTokens[LDR_EXE_CODE_3];

      // Create exemption type
      makeExeType(pOutbuf+OFF_EXE_TYPE, pOutbuf+OFF_EXE_CD1, pOutbuf+OFF_EXE_CD2, pOutbuf+OFF_EXE_CD3, (IDX_TBL4 *)&SDX_Exemption);
   }

   // UseCode
   if (*apTokens[LDR_USE_CODE] > ' ')
   {
      vmemcpy(pOutbuf+OFF_USE_CO, apTokens[LDR_USE_CODE], SIZ_USE_CO);
      updateStdUse(pOutbuf+OFF_USE_STD, apTokens[LDR_USE_CODE], LSIZ_USE_CODE, pOutbuf);
   } else
      memcpy(pOutbuf+OFF_USE_STD, USE_UNASGN, SIZ_USE_STD);

   // TRA
   lTmp = atol(apTokens[LDR_TRA]);
   if (lTmp > 0)
   {
      iRet = sprintf(acTmp, "%.6d", lTmp);
      memcpy(pOutbuf+OFF_TRA, acTmp, iRet);
   }

   // Owner
   try {
      if (*apTokens[LDR_OWNER_NAME] > ' ')
         Sdx_MergeOwner_LDR(pOutbuf, apTokens[LDR_OWNER_NAME], apTokens[LDR_OWNER_STATUS], apTokens[LDR_MARITAL_STATUS]);
      else
         LogMsg("*** No owner name for APN: %s", apTokens[LDR_APN]);
   } catch (...)
   {
      iRet = 1;
      LogMsg("***** Exception occured in Sdx_MergeOwner() APN=%.10s", apTokens[LDR_APN]);
   }

   // Legal
   lTmp = atol(apTokens[LDR_MAP_NUMBER]);
   if (lTmp > 0)
   {
      // lTmp is LONGLONG so we have to cast it to long for sprintf() to work properly
      sprintf(acTmp, "TR %d %s", (long)lTmp, apTokens[LDR_PROP_DESCR]);
   } else if (*apTokens[LDR_MAP_NUMBER] >= 'A')
      sprintf(acTmp, "%s %s", apTokens[LDR_MAP_NUMBER], apTokens[LDR_PROP_DESCR]);
   else
      strcpy(acTmp, apTokens[LDR_PROP_DESCR]);

   iRet = blankRem(acTmp);
   if (iRet > 0)
   {
      if (acTmp[iRet-1] == '\\')
         acTmp[iRet-1] = 0;
      replChar(acTmp, '|', '1');
      iRet = updateLegal(pOutbuf, acTmp);
      if (iRet > iMaxLegal)
         iMaxLegal = iRet;
   }

   // Acreage (V99): Lot sqft - Lot Acres
   dTmp = atof(apTokens[LDR_ACREAGE]);
   if (dTmp > 0.0)
   {
      lTmp = (long)(dTmp*1000.0);
      sprintf(acTmp, "%*u", SIZ_LOT_ACRES, lTmp);
      memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);

      dTmp = (dTmp*SQFT_PER_ACRE) + 0.5;
      sprintf(acTmp, "%*u", SIZ_LOT_SQFT, (unsigned long)dTmp);
      memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);
   }

   // Zoning
   if (*apTokens[LDR_ZONING] > ' ')
   {
      *(pOutbuf+OFF_ZONE) = *apTokens[LDR_ZONING];
      *(pOutbuf+OFF_ZONE_X1) = *apTokens[LDR_ZONING];
   }

   // Transfer - MMDDYY
   lTmp = atol(apTokens[LDR_REC_DOCDATE]);
   if (lTmp > 0)
   {
      sprintf(acTmp1, "%.6d", lTmp);

      // Apply transfer
      pTmp = dateConversion(acTmp1, acTmp, MMDDYY2);
      if (pTmp)
      {
         memcpy(pOutbuf+OFF_TRANSFER_DT, acTmp, SIZ_TRANSFER_DT);
         lTmp = atol(apTokens[LDR_REC_DOCNUM]);
         if (lTmp > 0)
         {
            sprintf(acTmp, "%.7d", lTmp);
            memcpy(pOutbuf+OFF_TRANSFER_DOC, acTmp, 7);
         }
      }
   }

   return 0;
}

/********************************* Sdx_Load_Roll ******************************
 *
 *
 ******************************************************************************/

int Sdx_Load_Roll(int iSkip)
{
   char     *pTmp, acRec[MAX_RECSIZE], acBuf[MAX_RECSIZE], acRollRec[MAX_RECSIZE];
   char     acRawFile[_MAX_PATH], acOutFile[_MAX_PATH];

   HANDLE   fhIn, fhOut;
   SDX_ROLL *pRec;

   int      iRet, iTmp, iRollUpd, iNewRec, iRetiredRec, iLostRec;
   DWORD    nBytesRead;
   DWORD    nBytesWritten;
   BOOL     bRet, bEof;
   long     lRet, lCnt;

   char     acOrgRec[MAX_RECSIZE];
   int      iUpdRec=0, iInfoRec=0;

   iRollUpd=iNewRec=iRetiredRec=iLostRec=0;
   lRet=lCnt=0;

   LogMsg0("Load roll update file");
   sprintf(acRawFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "S01");
   sprintf(acOutFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "R01");

   // Rename files for processing
   if (_access(acRawFile, 0))
   {
      if (!_access(acOutFile, 0))
         rename(acOutFile, acRawFile);
      else
      {
         LogMsg("***** Missing input file %s.  Please recheck!", acRawFile);
         return -1;
      }
   }

   // Open Roll file
   LogMsg("Open Roll file %s", acRollFile);
   fdRoll = fopen(acRollFile, "r");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", acRollFile);
      return -2;
   }

   // Open Char file
   LogMsg("Open Char file %s", acCharFile);
   fdChar = fopen(acCharFile, "r");
   if (fdChar == NULL)
   {
      LogMsg("***** Error opening Char file: %s\n", acCharFile);
      return -2;
   }

   // Open Sale file
   LogMsg("Open sale file %s", acCSalFile);
   fdSale = fopen(acCSalFile, "r");
   if (fdSale == NULL)
   {
      LogMsg("***** Error opening cumulative sale file: %s\n", acCSalFile);
      return -2;
   }

   // Open CityZip file
   iRet = initSitus(acIniFile, myCounty.acCntyCode);
   if (iRet < 0)
      return iRet;

   // Open Input file
   LogMsg("Open input file %s", acRawFile);
   fhIn = CreateFile(acRawFile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhIn == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening input file: %s\n", acRawFile);
      return -2;
   }

   // Open Output file
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error creating output file: %s\n", acOutFile);
      return -2;
   }

   // Get first RollRec
   pTmp = fgets((char *)&acRollRec[0], MAX_RECSIZE, fdRoll);
   bEof = (pTmp ? false:true);
   pRec = (SDX_ROLL *)&acRollRec[0];

   // Copy skip record
   memset(acBuf, ' ', iRecLen);
   while (iSkip-- > 0)
   {
      ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);
      // Ignore record start with 99999999
      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
   }

   iNoMatch=iBadCity=iBadSuffix=0;
   lUseMailCity=lUseGis=0;

   // Merge loop
   while (!bEof)
   {
      bRet = ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);
      // Save original record
      memcpy(acOrgRec, acBuf, iRecLen);

      // Check for EOF
      if (!bRet)
      {
         LogMsg("***** Error reading input file %s (%f)", acRawFile, GetLastError());
         break;
      }

      // EOF ?
      if (iRecLen != nBytesRead)
         break;

      NextRollRec:
#ifdef _DEBUG
      //if (!memcmp(acBuf, "1250500001", 10))
      //   iRet = 1;
#endif
      iTmp = memcmp(acBuf, &acRollRec[ROFF_APN], iApnLen);
      if (!iTmp)
      {
         // Merge roll data
         iRet = Sdx_MergeRoll(acBuf, acRollRec, iRollLen, UPDATE_R01);
         iRollUpd++;

         // Merge Char
         if (fdChar)
            iRet = Sdx_MergeChar(acBuf);

         // Merge Sale
         if (fdSale)
            iRet = Sdx_MergeSCSale(acBuf, true);

         // Read next roll record
         do
         {
            pTmp = fgets(acRollRec, MAX_RECSIZE, fdRoll);
            bRet = memcmp(&pRec->OwnerName[1], "INFORMATION ONLY", 14);
            if (!bRet) 
               iInfoRec++;
         } while (pTmp && !bRet);

         if (!pTmp)
            bEof = true;    // Signal to stop
      } else if (iTmp > 0)       // Roll not match, new roll record?
      {
         if (bDebug)
            LogMsg0("*** New roll record : %.*s (%d) ***", iApnLen, acRollRec, lCnt);

         // Create new R01 record
         iRet = Sdx_MergeRoll(acRec, acRollRec, iRollLen, CREATE_R01);
         iNewRec++;

         // Merge Char
         if (fdChar)
            iRet = Sdx_MergeChar(acRec);

         // Merge Sale
         if (fdSale)
            iRet = Sdx_MergeSCSale(acRec, true);

         // Save last recording date
         lRet = atoin((char *)&acRec[OFF_TRANSFER_DT], 8);
         if (lRet > lLastRecDate && lRet < lToday)
            lLastRecDate = lRet;

         bRet = WriteFile(fhOut, acRec, iRecLen, &nBytesWritten, NULL);
         lCnt++;

         // Get next roll record
         do
         {
            pTmp = fgets(acRollRec, MAX_RECSIZE, fdRoll);
            bRet = memcmp(&pRec->OwnerName[1], "INFORMATION ONLY", 14);
            if (!bRet) 
               iInfoRec++;
         } while (pTmp && !bRet);


         if (!pTmp)
            bEof = true;    // Signal to stop
         else
            goto NextRollRec;
      } else
      {
         // Record may be retired
         if (bDebug)
            LogMsg0("*** Roll not match (retired record?) : R01->%.*s < Roll->%.*s (%d) ***", iApnLen, acBuf, iApnLen, acRollRec, lCnt);
         iRetiredRec++;
         continue;
      }

      // Save last recording date
      lRet = atoin((char *)&acBuf[OFF_TRANSFER_DT], 8);
      if (lRet > lLastRecDate && lRet < lToday)
         lLastRecDate = lRet;

      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);

      if (!bRet)
      {
         iRet = GetLastError();
         if (iRet == ERROR_DISK_FULL)
            LogMsg("***** Error writing to output file: Disk Full\n");
         else
            LogMsg("***** Error writing to output file: %d\n", iRet);
         lRet = WRITE_ERR;
         break;
      }
   }

   // Do the rest of the file
   while (!bEof)
   {
      if (bDebug)
         LogMsg0("*** New roll record : %.*s (%d) ***", iApnLen, acRollRec, lCnt);

      // Create new R01 record
      iRet = Sdx_MergeRoll(acRec, acRollRec, iRollLen, CREATE_R01);
      iNewRec++;

      // Merge Char
      if (fdChar)
         iRet = Sdx_MergeChar(acRec);

      // Merge Sale
      if (fdSale)
         iRet = Sdx_MergeSCSale(acRec, true);

      // Save last recording date
      lRet = atoin((char *)&acRec[OFF_TRANSFER_DT], 8);
      if (lRet > lLastRecDate && lRet < lToday)
         lLastRecDate = lRet;

      bRet = WriteFile(fhOut, acRec, iRecLen, &nBytesWritten, NULL);
      lCnt++;

      // Get next roll record
      do
      {
         pTmp = fgets(acRollRec, MAX_RECSIZE, fdRoll);
         bRet = memcmp(&pRec->OwnerName[1], "INFORMATION ONLY", 14);
         if (!bRet) 
            iInfoRec++;
      } while (pTmp && !bRet);

      if (!pTmp)
         bEof = true;    // Signal to stop
   }

   // Special case for SDX
   while (nBytesRead>0 && ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL))
   {
      if (!nBytesRead)
         break;

      // Merge Char
      if (fdChar)
         iRet = Sdx_MergeChar(acBuf);

      // Merge Sale
      if (fdSale)
         iRet = Sdx_MergeSCSale(acBuf, true);

      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
      iLostRec++;
   }

   // Close files
   closeSitus();
   if (fdRoll)
      fclose(fdRoll);
   if (fdChar)
      fclose(fdChar);
   if (fdSale)
      fclose(fdSale);
   if (fdCity)
      fclose(fdCity);

   if (fhOut)
      CloseHandle(fhOut);
   if (fhIn)
      CloseHandle(fhIn);

   LogMsg("Total output records from MergePQ4:  %u", lCnt);
   LogMsg("Total new records:                   %u", iNewRec);
   LogMsg("Total retired records:               %u", iRetiredRec);
   LogMsg("Total info only records:             %u", iInfoRec);
   LogMsg("Total Char matched:                  %u", lCharMatch);
   LogMsg("Total Sale matched:                  %u", lSaleMatch);
   LogMsg("Use GIS city:                        %u", lUseGis);
   LogMsg("Use Mail city:                       %u", lUseMailCity);

   if (iLostRec > 0)
      LogMsg("Total Lost or retired? records:      %u", iLostRec);
   if (lDupOwners > 0)
      LogMsg("Number of duplicate owners:          %u", lDupOwners);

   LogMsg("Total bad-city records:              %u", iBadCity);
   LogMsg("Total bad-suffix records:            %u\n", iBadSuffix);

   printf("\nTotal output records from MergePQ4: %u\n", lCnt);

   lRecCnt = lCnt;
   return 0;
}

/********************************* Sdx_Load_LDR *****************************
 *
 * Thiss function support both CSV and fixed length LDR format.
 *
 ****************************************************************************/

int Sdx_Load_LDR(int iFirstRec /* 1=create header rec */)
{
   char     *pTmp, acBuf[MAX_RECSIZE], acRollRec[MAX_RECSIZE];
   char     acOutFile[_MAX_PATH];

   HANDLE   fhOut;

   DWORD    nBytesWritten;
   BOOL     bRet, bCsv=false;
   long     lRet=0, lCnt=0;
   int      iRet;

   LogMsg0("Loading LDR file");
   sprintf(acOutFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "R01");

   // Open Roll file
   LogMsg("Open Roll file %s", acRollFile);
   fdRoll = fopen(acRollFile, "r");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", acRollFile);
      return -2;
   }
   
   // Check for delimited file type
   if (strstr(acRollFile, "csv"))
      bCsv = true;

   // Open Char file
   LogMsg("Open Char file %s", acCharFile);
   fdChar = fopen(acCharFile, "r");
   if (fdChar == NULL)
   {
      LogMsg("***** Error opening Char file: %s\n", acCharFile);
      return -2;
   }

   // Open Prop8 file
   sprintf(acProp8File, acProp8Tmpl, myCounty.acCntyCode, myCounty.acCntyCode, myCounty.acYearAssd, "P8");
   LogMsg("Open Prop8 file %s", acProp8File);
   fdProp8 = fopen(acProp8File, "r");
   if (fdProp8 == NULL)
   {
      LogMsg("***** Error opening Prop8 file: %s\n", acProp8File);
      return -2;
   }

   // Open Sale file
   LogMsg("Open sale file %s", acCSalFile);
   fdSale = fopen(acCSalFile, "r");
   if (fdSale == NULL)
   {
      LogMsg("***** Error opening cumulative sale file: %s\n", acCSalFile);
      return -2;
   }

   // Open CityZip file
   iRet = initSitus(acIniFile, myCounty.acCntyCode);
   if (iRet < 0)
      return iRet;

   // Open Output file
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error creating output file: %s\n", acOutFile);
      return -2;
   }

   // Create header record
   if (iFirstRec)
   {
      memset(acBuf, '9', iRecLen);
      // Ignore record start with 99999999
      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
   }

   lUseGis=iNoMatch=iBadCity=iBadSuffix=0;

   // Get first RollRec
   pTmp = fgets((char *)&acRollRec[0], MAX_RECSIZE, fdRoll);
   memset(acBuf, ' ', iRecLen);

   // Merge loop
   while (pTmp)
   {
#ifdef _DEBUG
      //if (!memcmp(acBuf, "1025302211", 10))
      //   iRet = 1;
#endif

      // Create new R01 record
      if (bCsv)
         iRet = Sdx_MergeLien(acBuf, acRollRec);
      else
         iRet = Sdx_MergeLien(acBuf, acRollRec, iRollLen, CREATE_R01);

      // Merge Char
      if (fdChar)
         iRet = Sdx_MergeChar(acBuf);

      // Merge Prop8
      if (fdProp8)
         iRet = Sdx_MergeProp8(acBuf);

      // Merge Sale
      if (fdSale)
         iRet = Sdx_MergeSCSale(acBuf, false);

      // Save last recording date
      iRet = atoin((char *)&acBuf[OFF_SALE1_DT], 8);
      if (iRet > lLastRecDate && iRet < lToday)
         lLastRecDate = iRet;

      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
      if (!bRet)
      {
         LogMsg("***** Error occurs: %d\n", GetLastError());
         lRet = WRITE_ERR;
         break;
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);

      // Get next roll record
      pTmp = fgets(acRollRec, MAX_RECSIZE, fdRoll);
   }

   // Close files
   closeSitus();

   if (fdRoll)
      fclose(fdRoll);
   if (fdChar)
      fclose(fdChar);
   if (fdSale)
      fclose(fdSale);
   if (fdProp8)
      fclose(fdProp8);

   if (fhOut)
      CloseHandle(fhOut);

   LogMsg("Total output records:       %u", lCnt);
   LogMsg("      Char matched:         %u", lCharMatch);
   LogMsg("      Char skipped:         %u", lCharSkip);
   LogMsg("      Sale matched:         %u", lSaleMatch);
   LogMsg("      Sale skipped:         %u", lSaleSkip);
   LogMsg("      Prop8 matched:        %u", lProp8Match);
   LogMsg("      Prop8 skipped:        %u", lProp8Skip);
   LogMsg("      Use mail city:        %u", lUseMailCity);
   LogMsg("  Use NDC extr file:        %u\n", lUseGis);
   LogMsg("Last recording date:        %u", lLastRecDate);
   
   LogMsg("Total bad-city records:     %u", iBadCity);
   LogMsg("Total bad-suffix records:   %u\n", iBadSuffix);

   printf("\nTotal output records: %u\n", lCnt);

   lRecCnt = lCnt;
   lLDRRecCount = lCnt;
   return lRet;
}

/********************************* formatCSale *******************************
 *
 * Return -1 if bad record.  0 if OK.
 *
 *****************************************************************************

int Sdx_FormatCSale1(char *pFmtSale, char *pSaleRec)
{

   SDX_HSALE *pSale = (SDX_HSALE *)pSaleRec;
   CSAL_REC  *pCSal = (CSAL_REC *)pFmtSale;
   long      lTmp, lYear, lMonth;
   char      acTmp[32];

#ifdef _DEBUG
   //if (!memcmp(pSale->Apn, "10129029", 8))
   //   lTmp = 0;
#endif
   // Inititalize
   memset(pFmtSale, ' ', sizeof(CSAL_REC));

   // APN
   if (pSale->Apn[0] < '0' || pSale->DocDate[0] < '0' || pSale->DocNum[0] < '0')
      return -1;

   memcpy(pCSal->Apn, pSale->Apn, HSIZ_APN);

   // SaleDate
   dateConversion(pSale->DocDate, acTmp, YYMMDD2);
   memcpy(pCSal->DocDate, acTmp, 8);

   lYear = atoin(acTmp, 4);
   lMonth= atoin(&acTmp[4], 2);

   // DocNum
   pCSal->DocNum[0] = '0';
   memcpy(&pCSal->DocNum[1], pSale->DocNum, HSIZ_DOCNUM);
   // 02/04/2010
   //lTmp = atoin(pSale->DocNum, HSIZ_DOCNUM);
   //if (lYear > 2000 && lMonth >= 8 && pSale->DocNum[0] < 0x33)
   //{
   //   pCSal->DocNum[0] = '1';
   //   memcpy(&pCSal->DocNum[1], pSale->DocNum, HSIZ_DOCNUM);
   //} else
   //{
   //   pCSal->DocNum[0] = '0';
   //   memcpy(&pCSal->DocNum[1], pSale->DocNum, HSIZ_DOCNUM);
   //}
   

   // DocType

   // SalePrice
   lTmp = atoin(pSale->SalePrice, HSIZ_SALEPRICE);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*u", SALE_SIZ_SALEPRICE, lTmp);
      memcpy(pCSal->SalePrice, acTmp, SALE_SIZ_SALEPRICE);
   }

   pCSal->SaleCode[0] = pSale->SaleFlg;    // M=Nulti parcel sale
   pCSal->CRLF[0] = '\n';
   pCSal->CRLF[1] = 0;
   return 0;
}

int Sdx_FormatCSale2(char *pFmtSale, char *pSaleRec)
{

   SDX_SALE *pSale = (SDX_SALE *)pSaleRec;
   CSAL_REC *pCSal= (CSAL_REC *)pFmtSale;
   long     lTmp;
   char     acTmp[32];

   // Inititalize
   memset(pFmtSale, ' ', sizeof(CSAL_REC));

   // APN
   if (pSale->Apn[0] < '0' || pSale->DocDate[0] < '0' || pSale->DocNum[0] < '0')
      return -1;

   memcpy(pCSal->Apn, pSale->Apn, SSIZ_APN);

   // SaleDate
   dateConversion(pSale->DocDate, acTmp, YYMMDD2);
   memcpy(pCSal->DocDate, acTmp, 8);

   // DocNum
   memcpy(pCSal->DocNum, pSale->DocNum, SSIZ_DOCNUM);

   // DocType

   // SalePrice
   lTmp = atoin(pSale->SalePrice, SSIZ_SALEPRICE);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*u", SALE_SIZ_SALEPRICE, lTmp);
      memcpy(pCSal->SalePrice, acTmp, SALE_SIZ_SALEPRICE);
   }

   // Buyer
   // ERSHIP NAME INFORMATION
   // 50% TAVELMAN JACK CONS50% M+L FINANCIAL PROPERTIES INC
   //  RENGGLI BALZ P UWJT CANNON KATHERINE
   //  RAYMUNDO ERNESTO MMSO RAYMUNDO JUAN M
   //  HANSEN TOMM+SANDRA
   //  KARDUM JOHN <DBA QUALITY PROPERTIES>
   // 1/2 MARTINEC JAMES C SMNS1/2 MAGGA DEAN G
   //  GONZALEZ GUSTAVO+GARZA-GONZALEZ ILSA G
   // 50% COVAS MARCUS A+GARRIOTT KIMBERLEY A HWJT50% GARRIOT
   // 50% ARTEAGA RAFAEL HWCP50% ALVAREZ JOSE+ROSA
   //  CAVANAGH JAMES P TR NSNS CAVANAGH DOROTHY C TR
   //  MEJIA JAIME G SMJT MEJIA MIGUEL A <AKA MEJIA  MIKE>
   // 50% SPERLING FAMILY TRUST 03-03-97 HWCP50% WILLIAMS KEN


   pCSal->CRLF[0] = '\n';
   pCSal->CRLF[1] = 0;
   return 0;
}

/****************************** Sdx_UpdateSaleHist ***************************
 *
 * Update history sale.  This function adds new sale record to cumulative file.
 *
 * Here we have two version of UpdateSaleHist().  Version #1 is to update using
 * old 250-byte fixed length format and version #2 is using new 202-byte DOS
 * file format (/w CRLF at EOL).
 *
 * Return 0 if successful, otherwise error
 *
 *****************************************************************************

int Sdx_UpdateSaleHist1(void)
{
   SDX_HSALE *pCurSale;
   CSAL_REC  *pCumSale;

   char      acCSalRec[512], acSaleRec[1024], acSaleDate[32], acTmp[256];
   char      acOutFile[_MAX_PATH], acLastApn[16];
   int       iRet, iUpdateSale=0, iNewSale;
   long      lCnt=0;

   iSaleLen = sizeof(SDX_HSALE);

   // Check history sale file
   GetIniString("SDX", "SaleHist", "", acTmp, _MAX_PATH, acIniFile);
   if (_access(acTmp, 0))
   {
      LogMsg("***** History sale file %s not found", acTmp);
      return -1;
   }

   // Open history sale
   LogMsg("Open History sale file %s", acTmp);
   fdSale = fopen(acTmp, "r");
   if (fdSale == NULL)
   {
      LogMsg("***** Error opening history sale file: %s\n", acTmp);
      return 2;
   }

   // Create output file
   sprintf(acOutFile, acSaleTmpl, myCounty.acCntyCode, "SLS");
   LogMsg("Create updated sale file %s", acOutFile);
   fdCSale = fopen(acOutFile, "w");
   if (fdCSale == NULL)
   {
      LogMsg("***** Error creating updated sale file: %s\n", acOutFile);
      return 2;
   }

   // Read first history sale - skip first one
   iRet = fread(acSaleRec, 1, iSaleLen, fdSale);

   pCurSale = (SDX_HSALE *)&acSaleRec[0];
   pCumSale = (CSAL_REC *)&acCSalRec[0];
   acSaleDate[0] = 0;
   acLastApn[iApnLen] = acLastApn[0] = 0;

   // Merge loop
   while (!feof(fdSale))
   {
      // Get current sale
      iRet = fread(acSaleRec, 1, iSaleLen, fdSale);
      if (iRet != iSaleLen)
         break;

      // Format cumsale record
      iRet = Sdx_FormatCSale1(acCSalRec, acSaleRec);
      if (!iRet)
      {
         // Output current sale record
         fputs(acCSalRec, fdCSale);
         iUpdateSale++;
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);

   }

   // Close files
   if (fdCSale)
      fclose(fdCSale);
   if (fdSale)
      fclose(fdSale);

   sprintf(acSaleFile, acSaleTmpl, myCounty.acCntyCode, "DAT");
   // Sort sale file - APN (asc), Saledate (asc), DocNum (asc)
   sprintf(acTmp, "S(1,10,C,A,27,8,C,A,15,7,C,A) DUPO(1,34)");
   iNewSale = sortFile(acOutFile, acSaleFile, acTmp);

   LogMsg("Total sale records processed:   %u", lCnt);
   LogMsg("Total cumulative sale records:  %u", iNewSale);
   LogMsg("Total sale records updated:     %u", iUpdateSale);

   LogMsg("Update Sale History completed.");

   return 0;
}

int Sdx_UpdateSaleHist2(void)
{
   SDX_SALE *pCurSale;
   CSAL_REC *pCumSale;

   char     *pTmp, acCSalRec[512], acSaleRec[1024], acSaleDate[32], acTmp[256];
   char     acOutFile[_MAX_PATH], acLastApn[16];
   int      iRet, iTmp, iUpdateSale=0, iNewSale=0;
   BOOL     bRet;
   long     lCnt=0;

   // Check current sale file
   if (_access(acSaleFile, 0))
   {
      LogMsg("***** Missing current sale file %s", acSaleFile);
      return -1;
   }

   // Open current sale
   LogMsg("Open current sale file %s", acSaleFile);
   fdSale = fopen(acSaleFile, "r");
   if (fdSale == NULL)
   {
      LogMsg("***** Error opening current sale file: %s\n", acSaleFile);
      return 2;
   }

   // Create output file
   sprintf(acOutFile, acSaleTmpl, myCounty.acCntyCode, "TMP");
   LogMsg("Create updated sale file %s", acOutFile);
   fdCSale = fopen(acOutFile, "w");
   if (fdCSale == NULL)
   {
      LogMsg("***** Error creating updated sale file: %s\n", acOutFile);
      return 2;
   }

   // Read first history sale
   pCurSale = (SDX_SALE *)&acSaleRec[0];
   pCumSale = (CSAL_REC *)&acCSalRec[0];
   acSaleDate[0] = 0;
   acLastApn[iApnLen] = acLastApn[0] = 0;

   // Merge loop
   while (!feof(fdSale))
   {
      // Get current sale
      pTmp = fgets(acSaleRec, 1024, fdSale);
      if (!pTmp)
         break;

      // Format cumsale record
      iRet = Sdx_FormatCSale2(acCSalRec, acSaleRec);
      if (!iRet)
      {
         // Output current sale record
         fputs(acCSalRec, fdCSale);
         iUpdateSale++;
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdCSale)
      fclose(fdCSale);
   if (fdSale)
      fclose(fdSale);

   char  acHistFile[256];

   sprintf(acHistFile, acSaleTmpl, myCounty.acCntyCode, "SLS");
   sprintf(acSaleFile, acSaleTmpl, myCounty.acCntyCode, "DAT");
   strcat(acHistFile, "+");
   strcat(acHistFile, acOutFile);

   // Sort sale file - APN (asc), Saledate (asc), DocNum (asc)
   sprintf(acTmp, "S(1,10,C,A,27,8,C,A,15,7,C,A) DUPO(1,34)");
   iTmp = sortFile(acHistFile, acSaleFile, acTmp);

   // Copy history file
   sprintf(acHistFile, acSaleTmpl, myCounty.acCntyCode, "SLS");
   bRet = CopyFile(acSaleFile, acHistFile, false);
   if (!bRet)
      LogMsg("***** Error copying %s to %s", acSaleFile, acHistFile);

   LogMsg("Total sale records processed:   %u", lCnt);
   //LogMsg("Total cumulative sale records:  %u", iTmp);
   LogMsg("Total sale records updated:     %u", iUpdateSale);

   LogMsg("Update Sale History completed.");

   return 0;
}

/****************************** Sdx_LoadSaleHist ****************************
 *
 * This function is used to load old sale history (SaleHist) from county.
 * It is one time county sent us the file back in 2006.
 *
 * Return 0 if successful, otherwise error
 *
 *****************************************************************************/

int Sdx_LoadSaleHist(void)
{
   SDX_HSALE *pCurSale;
   SCSAL_REC *pCumSale;
   FILE      *fdIn, *fdOut;

   char      acCSalRec[1024], acSaleRec[1024], acSaleDate[32], acTmp[256];
   char      acOutFile[_MAX_PATH], acLastApn[16], *pTmp;
   long      iTmp, iRet, lCnt=0, iUpdateSale=0;

   LogMsg0("Load SDX sale history file");

   // Check history sale file
   GetIniString("SDX", "SaleHist", "", acTmp, _MAX_PATH, acIniFile);
   if (_access(acTmp, 0))
   {
      LogMsg("***** History sale file %s not found", acTmp);
      return -1;
   }

   // Open history sale
   LogMsg("Open History sale file %s", acTmp);
   fdIn = fopen(acTmp, "r");
   if (fdIn == NULL)
   {
      LogMsg("***** Error opening history sale file: %s\n", acTmp);
      return -2;
   }

   // Create output file
   sprintf(acOutFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "tmp");
   LogMsg("Create updated sale file %s", acOutFile);
   fdOut = fopen(acOutFile, "w");
   if (fdOut == NULL)
   {
      LogMsg("***** Error creating updated sale file: %s\n", acOutFile);
      return -3;
   }

   // Skip header rec
   iSaleLen = sizeof(SDX_HSALE);
   iRet = fread(acSaleRec, 1, iSaleLen, fdIn);

   pCurSale = (SDX_HSALE *)&acSaleRec[0];
   pCumSale = (SCSAL_REC *)&acCSalRec[0];
   acSaleDate[0] = 0;
   acLastApn[iApnLen] = acLastApn[0] = 0;

   // Merge loop
   while (!feof(fdIn))
   {
      lCnt++;

      // Get current sale
      iRet = fread(acSaleRec, 1, iSaleLen, fdIn);
      if (iRet != iSaleLen)
         break;

      // Check for valid record
      if (pCurSale->Apn[0] < '0' || pCurSale->DocDate[0] < '0' || pCurSale->DocNum[0] < '0')
         continue;

      // Inititalize
      memset(acCSalRec, ' ', sizeof(SCSAL_REC));

      // APN
      memcpy(pCumSale->Apn, pCurSale->Apn, HSIZ_APN);

      // SaleDate
      dateConversion(pCurSale->DocDate, acTmp, YYMMDD2);
      memcpy(pCumSale->DocDate, acTmp, 8);

      // DocNum
      pCumSale->DocNum[0] = '0';
      memcpy(&pCumSale->DocNum[1], pCurSale->DocNum, HSIZ_DOCNUM);

      // DocType

      // SalePrice
      iRet = atoin(pCurSale->SalePrice, HSIZ_SALEPRICE);
      if (iRet > 0)
      {
         sprintf(acTmp, "%*u", SALE_SIZ_SALEPRICE, iRet);
         memcpy(pCumSale->SalePrice, acTmp, SALE_SIZ_SALEPRICE);
      }

      // Owner
      if (memcmp(pCurSale->Buyer, "NO O", 4))
      {
         pTmp = pCurSale->Buyer;
         iTmp = 0;
         while (*pTmp == ' ' && iTmp < 10)
         {
            pTmp++;
            iTmp++;
         }
         // For now, just keep name as in raw format for SDX
         memcpy(pCumSale->Name1, pTmp, HSIZ_NAME-iTmp);
      }

      // M=Multi parcel sale
      if (pCurSale->SaleFlg == 'M')
         pCumSale->MultiSale_Flg = 'Y';
      else
         pCumSale->SaleCode[0] = pCurSale->SaleFlg;    

      pCumSale->CRLF[0] = '\n';
      pCumSale->CRLF[1] = 0;

      // Output current sale record
      fputs(acCSalRec, fdOut);
      iUpdateSale++;

      if (!(lCnt % 1000))
         printf("\r%u", lCnt);

   }

   // Close files
   if (fdOut)
      fclose(fdOut);
   if (fdIn)
      fclose(fdIn);

   sprintf(acCSalFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "sls");
   // Sort/merge sale file - APN (asc), Saledate (asc), Saleprice (desc), DocNum (asc)
   sprintf(acTmp, "S(1,%d,C,A,27,8,C,A,57,10,C,D,15,12,C,A) DUPO(1,34)", iApnLen);
   iRet = sortFile(acOutFile, acCSalFile, acTmp);

   LogMsg("Total sale records processed:   %u", lCnt);
   LogMsg("Total cumulative sale records:  %u", iRet);
   LogMsg("Total sale records updated:     %u", iUpdateSale);

   LogMsg("Update Sale History completed.");

   return 0;
}

/*********************************** Sdx_ExtrSale *****************************
 *
 * This function update sale history by adding new sales to cum sale file.
 *
 ******************************************************************************/

int Sdx_FormatSCSale(char *pFmtSale, char *pSaleRec)
{

   SDX_SALE  *pSale = (SDX_SALE  *)pSaleRec;
   SCSAL_REC *pCSal = (SCSAL_REC *)pFmtSale;
   long     lTmp;
   char     acTmp[32], *pTmp;

   // Inititalize
   memset(pFmtSale, ' ', sizeof(SCSAL_REC));

   // APN
   if (pSale->Apn[0] < '0' || pSale->DocDate[0] < '0' || pSale->DocNum[0] < '0')
      return -1;

#ifdef _DEBUG
   //if (!memcmp(pSale->Apn, "1012102100", 8))
   //   lTmp = 0;
#endif
   
   memcpy(pCSal->Apn, pSale->Apn, SSIZ_APN);

   // SaleDate
   dateConversion(pSale->DocDate, acTmp, YYMMDD2);
   memcpy(pCSal->DocDate, acTmp, 8);

   // DocNum
   memcpy(pCSal->DocNum, pSale->DocNum, SSIZ_DOCNUM);

   // DocType

   // SalePrice
   lTmp = atoin(pSale->SalePrice, SSIZ_SALEPRICE);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*u", SALE_SIZ_SALEPRICE, lTmp);
      memcpy(pCSal->SalePrice, acTmp, SALE_SIZ_SALEPRICE);
   }

   // Buyer
   // ERSHIP NAME INFORMATION
   // 50% TAVELMAN JACK CONS50% M+L FINANCIAL PROPERTIES INC
   //  RENGGLI BALZ P UWJT CANNON KATHERINE
   //  RAYMUNDO ERNESTO MMSO RAYMUNDO JUAN M
   //  HANSEN TOMM+SANDRA
   //  KARDUM JOHN <DBA QUALITY PROPERTIES>
   // 1/2 MARTINEC JAMES C SMNS1/2 MAGGA DEAN G
   //  GONZALEZ GUSTAVO+GARZA-GONZALEZ ILSA G
   // 50% COVAS MARCUS A+GARRIOTT KIMBERLEY A HWJT50% GARRIOT
   // 50% ARTEAGA RAFAEL HWCP50% ALVAREZ JOSE+ROSA
   //  CAVANAGH JAMES P TR NSNS CAVANAGH DOROTHY C TR
   //  MEJIA JAIME G SMJT MEJIA MIGUEL A <AKA MEJIA  MIKE>
   // 50% SPERLING FAMILY TRUST 03-03-97 HWCP50% WILLIAMS KEN
   if (memcmp(pSale->Buyer, "ER", 2))
   {
      // For now, just keep name as in raw format for SDX
      if (pSale->Buyer[0] == ' ')
         memcpy(pCSal->Name1, &pSale->Buyer[1], SSIZ_NAME-1);
      else if (pSale->Buyer[0] > ' ')
      {
         char *pTmp1;
         pTmp = strchr(pSale->Buyer, ' ');
         pTmp1 = strstr(pTmp, "  ");
         if (pTmp1)
         {
            *pTmp1 = 0;
            pTmp++;
            memcpy(pCSal->Name1, pTmp, strlen(pTmp));
         }
      }
   }

   pCSal->CRLF[0] = '\n';
   pCSal->CRLF[1] = 0;
   return 0;
}

int Sdx_ExtrSale(char *pSaleFile)
{
   SDX_SALE  *pCurSale;
   SCSAL_REC *pCumSale;

   char     *pTmp, acCSalRec[1024], acSaleRec[1024], acTmp[256], acOutFile[_MAX_PATH];
   int      iRet, iUpdateSale=0;
   long     lCnt=0;

   LogMsg0("Load SDX sale history file");

   // Open current sale
   LogMsg("Open current sale file %s", pSaleFile);
   fdSale = fopen(pSaleFile, "r");
   if (fdSale == NULL)
   {
      LogMsg("***** Error opening current sale file: %s\n", pSaleFile);
      return -2;
   }

   // Create output file
   sprintf(acOutFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "tmp");
   LogMsg("Create updated sale file %s", acOutFile);
   fdCSale = fopen(acOutFile, "w");
   if (fdCSale == NULL)
   {
      LogMsg("***** Error creating updated sale file: %s\n", acOutFile);
      return -3;
   }

   // Initialize
   pCurSale = (SDX_SALE *)&acSaleRec[0];
   pCumSale = (SCSAL_REC *)&acCSalRec[0];

   // Merge loop
   while (!feof(fdSale))
   {
      // Get current sale
      pTmp = fgets(acSaleRec, 1024, fdSale);
      if (!pTmp)
         break;

      // Format cumsale record
      iRet = Sdx_FormatSCSale(acCSalRec, acSaleRec);
      if (!iRet)
      {
         if (strlen(acCSalRec) > 512)
            LogMsg("Bad sale rec: %.12s", acCSalRec);

         // Output current sale record
         fputs(acCSalRec, fdCSale);
         iUpdateSale++;
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdCSale)
      fclose(fdCSale);
   if (fdSale)
      fclose(fdSale);

   // Combine with old cum sale file and resort
   sprintf(acSaleRec, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "srt");
   sprintf(acCSalFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "sls");
   strcpy(acSaleFile, acCSalFile); 
   strcat(acSaleFile, "+");
   strcat(acSaleFile, acOutFile);

   // Sort sale file - APN (asc), Saledate (asc), sale price (desc), DocNum (asc)
   sprintf(acTmp, "S(1,10,C,A,27,8,C,A,57,10,C,D,15,12,C,A) DUPO(B2000,1,34)");
   iRet = sortFile(acSaleFile, acSaleRec, acTmp);
   if (iRet > 0)
   {
      LogMsg("Total cumulative sale records:  %u", iRet);

      // Rename old SDX_Sale.sls to SDX_Sale.sav then rename .srt to .sls
      sprintf(acOutFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "sav");
      if (!_access(acOutFile, 0))
         DeleteFile(acOutFile);

      iRet = rename(acCSalFile, acOutFile);
      if (!iRet)
      {
         iRet = rename(acSaleRec, acCSalFile);
         if (iRet)
         {
            sprintf(acTmp, "***** Error renaming %s to %s: ", acSaleRec, acCSalFile);
            perror(acTmp);
         }
      } else
      {
         sprintf(acTmp, "***** Error renaming %s to %s: ", acCSalFile, acOutFile);
         perror(acTmp);
      }
   } else
   {
      // Something is wrong here
      LogMsg("***** Error loading sale file.  Please check %s.", pSaleFile);
      iRet = -1;
   }

   LogMsg("Total sale records processed:   %u", lCnt);

   LogMsg("Update Sale History completed.");

   return iRet;
}

/******************************* Sdx_AsrMergeSaleData ****************************
 *
 * Merge new sale data into current sale.  Move other sales accordingly.
 * Only update seller if that is the sale from assessor.
 *
 *    1) If same date and docnum < doc1num, ignore this sale.
 *    2) If docdate < doc1date, ignore this sale
 *    3) If docdate = doc1date, update current sale regardless of sale price
 *    4) If docdate > doc1date, insert new sale
 *
 *****************************************************************************/

//int Sdx_AsrMergeSaleData(CSAL_REC *pSaleRec, char *pOutbuf, bool bSaleFlag)
//{
//   long  lCurSaleDt, lLstSaleDt, lPrice, lTmp;
//   int   iTmp;
//   char  acTmp[32];
//
//   lCurSaleDt = atoin(pSaleRec->DocDate, ASIZ_DOCDATE);
//   lLstSaleDt = atoin(pOutbuf+AOFF_S1_RECDATE, ASIZ_DOCDATE);
//
//   // Check case #2
//   if (lCurSaleDt < lLstSaleDt)
//      return -1;
//
//   // Check case #3
//   if (lCurSaleDt == lLstSaleDt)
//   {
//      // Check case #1
//      if (memcmp(pSaleRec->DocNum, pOutbuf+AOFF_S1_DOCNUM, ASIZ_DOCNUM) < 0)
//         return -1;
//
//      iTmp = 0;
//   } else // case #4
//   {
//      // Move sale2 to sale3
//      memcpy(pOutbuf+AOFF_S3_DOCNUM,  pOutbuf+AOFF_S2_DOCNUM,  ASIZ_DOCNUM);
//      memcpy(pOutbuf+AOFF_S3_RECDATE, pOutbuf+AOFF_S2_RECDATE, ASIZ_DOCDATE);
//      memcpy(pOutbuf+AOFF_S3_PRICE,   pOutbuf+AOFF_S2_PRICE,   ASIZ_S1_PRICE);
//      *(pOutbuf+AOFF_S3_MP_FLAG) = *(pOutbuf+AOFF_S2_MP_FLAG);
//
//      // Move sale1 to sale2
//      memcpy(pOutbuf+AOFF_S2_DOCNUM,  pOutbuf+AOFF_S1_DOCNUM,  ASIZ_DOCNUM);
//      memcpy(pOutbuf+AOFF_S2_RECDATE, pOutbuf+AOFF_S1_RECDATE, ASIZ_DOCDATE);
//      memcpy(pOutbuf+AOFF_S2_PRICE,   pOutbuf+AOFF_S1_PRICE,   ASIZ_S1_PRICE);
//      *(pOutbuf+AOFF_S2_MP_FLAG) = *(pOutbuf+AOFF_S1_MP_FLAG);
//      iTmp = 1;
//   }
//
//   // Update current sale
//   memcpy(pOutbuf+AOFF_S1_DOCNUM,  pSaleRec->DocNum,   ASIZ_DOCNUM);
//   memcpy(pOutbuf+AOFF_S1_RECDATE, pSaleRec->DocDate,  ASIZ_DOCDATE);
//   memcpy(pOutbuf+AOFF_S1_PRICE,   pSaleRec->SalePrice,ASIZ_S1_PRICE);
//   memcpy(pOutbuf+AOFF_SALEAMT,    pSaleRec->SalePrice,ASIZ_SALEAMT);
//
//   // SDX has no sale code, we use this field to store multi parcel sale flag
//   if (pSaleRec->SaleCode[0] == 'M')
//      *(pOutbuf+AOFF_S1_MP_FLAG) = 'Y';
//   else
//      *(pOutbuf+AOFF_S1_MP_FLAG) = ' ';
//
//   lPrice = atoin(pSaleRec->SalePrice, SALE_SIZ_SALEPRICE);
//
//   // Sale value per impr sqft
//   lTmp = atoin(pOutbuf+AOFF_LIV_AREA, ASIZ_LIV_AREA);
//   if (lTmp > 0 && lPrice > 0)
//   {
//      double dTmp;
//
//      dTmp = (double)lPrice/lTmp;
//      sprintf(acTmp, "%*.2f", ASIZ_SALEVAL_IMP_SQFT, dTmp);
//      memcpy(pOutbuf+AOFF_SALEVAL_IMP_SQFT, acTmp, ASIZ_SALEVAL_IMP_SQFT);
//   } else
//      memset(pOutbuf+AOFF_SALEVAL_IMP_SQFT, ' ', ASIZ_SALEVAL_IMP_SQFT);
//
//   // Sale less gross
//   lTmp = atoin(pOutbuf+AOFF_LAND_IMP, ASIZ_LAND_IMP);
//   if (lPrice > lTmp)
//   {
//      lTmp = lPrice - lTmp;
//      sprintf(acTmp, "%*u", ASIZ_SALE_LESS_GROSS, lTmp);
//      memcpy(pOutbuf+AOFF_SALE_LESS_GROSS, acTmp, ASIZ_SALE_LESS_GROSS);
//   } else
//      memset(pOutbuf+AOFF_SALE_LESS_GROSS, ' ', ASIZ_SALE_LESS_GROSS);
//
//   return iTmp;
//}

/********************************** Sdx_AsrMergeSale *****************************
 *
 * Input: Sale file is output from Sdx_UpdateSaleHist().
 *
 ******************************************************************************/

//int Sdx_AsrMergeSale(char *pOutbuf)
//{
//   static   char  acRec[1024], *pRec=NULL;
//   int      iRet, iLoop;
//   char     iCnt;
//   CSAL_REC *pSale;
//
//   // Get first Char rec for first call
//   if (!pRec && !lSaleMatch)
//   {
//      pRec = fgets(acRec, 1024, fdSale);
//   }
//
//   pSale = (CSAL_REC *)&acRec[0];
//   do
//   {
//      if (!pRec)
//      {
//         fclose(fdSale);
//         fdSale = NULL;
//         return 1;      // EOF
//      }
//
//      // Compare Apn
//      iLoop = memcmp(pOutbuf, pSale->Apn, SSIZ_APN);
//      if (iLoop > 0)
//      {
//         if (bDebug)
//            LogMsg0("Skip Sale rec  %.*s", SSIZ_APN, pSale->Apn);
//         pRec = fgets(acRec, 1024, fdSale);
//         lSaleSkip++;
//      }
//   } while (iLoop > 0);
//
//   // If not match, return
//   if (iLoop)
//      return 0;
//
//   //iCnt = '0';
//   do
//   {
//#ifdef _DEBUG
//      //if (!memcmp(pOutbuf, "004020015", 9))
//      //   iRet = 0;
//#endif
//
//      iRet = Sdx_AsrMergeSaleData((CSAL_REC *)&acRec[0], pOutbuf, false);
//      //iCnt++;
//
//      // Get next sale record
//      pRec = fgets(acRec, 1024, fdSale);
//      if (!pRec)
//      {
//         fclose(fdSale);
//         fdSale = NULL;
//         break;
//      }
//
//   } while (!memcmp(pOutbuf, acRec, SSIZ_APN));
//
//   iCnt = '1';
//   if (*(pOutbuf+AOFF_S2_RECDATE) > ' ')
//   {
//      iCnt++;
//      if (*(pOutbuf+AOFF_S3_RECDATE) > ' ')
//         iCnt++;
//   }
//   *(pOutbuf+AOFF_SALE_COUNT) = iCnt;
//
//   lSaleMatch++;
//
//   return 0;
//}

/********************************* Sdx_AsrMergeGrGr *****************************
 *
 * Return 0 if successful, >0 if error
 * Take last three sales
 *
 *****************************************************************************/

//int Sdx_AsrMergeGRec(SDX_GREC *pSaleRec, char *pOutbuf)
//{
//   long     lCurSaleDt, lLstSaleDt, lLstGrGrDt;
//
//   lCurSaleDt = atoin(pSaleRec->DocDate, ASIZ_DOCDATE);
//   lLstSaleDt = atoin(pOutbuf+AOFF_S1_RECDATE, ASIZ_DOCDATE);
//   lLstGrGrDt = atoin(pOutbuf+AOFF_1DT, ASIZ_DOCDATE);
//
//   if (lCurSaleDt <= lLstSaleDt || lCurSaleDt <= lLstGrGrDt)
//      return -1;
//
//   // Move sale2 to sale3
//   memcpy(pOutbuf+AOFF_3DT, pOutbuf+AOFF_2DT, ASIZ_GRGR_DOC);
//
//   // Move sale1 to sale2
//   memcpy(pOutbuf+AOFF_2DT, pOutbuf+AOFF_1DT, ASIZ_GRGR_DOC);
//
//   // Update current sale
//   memcpy(pOutbuf+AOFF_1DOC, pSaleRec->DocNum, ASIZ_DOCNUM);
//   memcpy(pOutbuf+AOFF_1DT, pSaleRec->DocDate, ASIZ_DOCDATE);
//   memcpy(pOutbuf+AOFF_1AMT, pSaleRec->SalePrice, ASIZ_S1_PRICE);
//   memcpy(pOutbuf+AOFF_1TYPE, "GRANT DEED", 10);
//
//   // DocType
//   //if (isdigit(pSaleRec->DocType[0]))
//   //   memcpy(pOutbuf+AOFF_1TYPE, pSaleRec->DocType, 3);
//   //else if (pSaleRec->DocType[0] > '0' && (pTmp=findDocType(pSaleRec->DocType, acTmp)))
//   //   memcpy(pOutbuf+AOFF_1TYPE, pTmp, strlen(pTmp));
//
//   // Grantor/Grantee
//   memcpy(pOutbuf+AOFF_1GRANTOR, pSaleRec->Grantor[0], GSIZ_GRANTOR*2);
//   memcpy(pOutbuf+AOFF_1GRANTEE, pSaleRec->Grantee[0], GSIZ_GRANTOR*2);
//
//   // Match flag
//   *(pOutbuf+AOFF_1APN_MATCH) = 'Y';
//   *(pOutbuf+AOFF_1NAME_MATCH) = pSaleRec->OwnerMatched;
//
//   return 1;
//}

//int Sdx_AsrMergeGrGr(char *pOutbuf)
//{
//   static   char  acRec[1024], *pRec=NULL;
//   int      iRet, iLoop;
//   char     iCnt;
//   SDX_GREC *pSale;
//
//   // Get first Char rec for first call
//   if (!pRec)
//   {
//      pRec = fgets(acRec, 1024, fdGrGr);
//   }
//
//   pSale = (SDX_GREC *)&acRec[0];
//   do
//   {
//      if (!pRec)
//      {
//         fclose(fdGrGr);
//         fdGrGr = NULL;
//         return 1;      // EOF
//      }
//
//      // Compare Apn
//      iLoop = memcmp(pOutbuf, acRec, SSIZ_APN);
//      if (iLoop > 0)
//      {
//         if (bDebug)
//            LogMsg0("Skip GrGr rec  %.*s", SSIZ_APN, pSale->Apn);
//         pRec = fgets(acRec, 1024, fdGrGr);
//         lGrGrSkip++;
//      }
//   } while (iLoop > 0);
//
//   // If not match, return
//   if (iLoop)
//      return 0;
//
//   do
//   {
//#ifdef _DEBUG
//      //if (!memcmp(pOutbuf, "004020015", 9))
//      //   iRet = 0;
//#endif
//
//      iRet = Sdx_AsrMergeGRec((SDX_GREC *)&acRec[0], pOutbuf);
//
//      // Get next sale record
//      pRec = fgets(acRec, 1024, fdGrGr);
//      if (!pRec)
//      {
//         fclose(fdGrGr);
//         fdGrGr = NULL;
//         break;
//      }
//
//   } while (!memcmp(pOutbuf, acRec, SSIZ_APN));
//
//   iCnt = '1';
//   if (*(pOutbuf+AOFF_2DT) > ' ')
//   {
//      iCnt++;
//      if (*(pOutbuf+AOFF_3DT) > ' ')
//         iCnt++;
//   }
//
//   *(pOutbuf+AOFF_GG_COUNT) = iCnt;
//
//   lGrGrMatch++;
//
//   return 0;
//}

/********************************* Sdx_MergeRoll *****************************
 *
 * Return 0 if successful, >0 if error
 *
 *****************************************************************************/

//int Sdx_AsrMergeRoll(char *pOutbuf, char *pRollRec, int iLen, int iFlag)
//{
//   SDX_ROLL *pRec;
//   char     acTmp[256], acDocNum[32], *pTmp;
//   long     lTmp;
//   double   dTmp;
//   int      iRet;
//
//   pRec = (SDX_ROLL *)pRollRec;
//   // Clear output buffer
//   memset(pOutbuf, ' ', iAsrRecLen);
//
//#ifdef _DEBUG
//   //if (!memcmp(pRec->Apn, "003120007", 9))
//   //   iRet = 1;
//#endif
//
//   // Start copying data
//   memcpy(pOutbuf, pRec->Apn, RSIZ_APN);
//
//   // TRA
//   memcpy(pOutbuf+AOFF_TRA, pRec->TRA, RSIZ_TRA);
//
//   // Contract code
//   *(pOutbuf+AOFF_CONTR_CODE) = pRec->ContractCode;
//
//   // Fractional interest
//   *(pOutbuf+AOFF_FRACT_INT) = pRec->FractIntCode;
//
//   // Status
//   memcpy(pOutbuf+AOFF_MARITAL_STATUS, pRec->Matital_Status, ASIZ_MARITAL_STATUS+ASIZ_OWNER_STATUS);
//
//   // Owner name
//   memcpy(pOutbuf+AOFF_OWNER_NAME, pRec->OwnerName, ASIZ_OWNER_NAME);
//
//   // Mail addr
//   //memcpy(pOutbuf+AOFF_MAIL_ADDR, pRec->MailAddr, ASIZ_MAIL_ADDR+ASIZ_MAIL_ZIP);
//   memcpy(pOutbuf+AOFF_MAIL_ADDR, pRec->MailAddr, ASIZ_MAIL_ADDR+ASIZ_MAIL_ZIP);
//   lTmp = atoin(pRec->MailZip, ASIZ_MAIL_ZIP);
//   if (lTmp > 400)
//      memcpy(pOutbuf+AOFF_MAIL_ZIP, pRec->MailZip, ASIZ_MAIL_ZIP);
//   else
//      memset(pOutbuf+AOFF_MAIL_ZIP, ' ', ASIZ_MAIL_ZIP);
//
//   // Convert MMDDYY to YYYYMMDD
//   pTmp = dateConversion(pRec->MailChgDate, acTmp, MMDDYY2);
//   if (pTmp)
//      memcpy(pOutbuf+AOFF_MAIL_CHGDATE, acTmp, ASIZ_MAIL_CHGDATE);
//
//   // Situs addr
//   memcpy(pOutbuf+AOFF_SITUS_ADDR, pRec->S_StrNum, ASIZ_SITUS_ADDR);
//
//   // Legal
//   *(pOutbuf+AOFF_DESC_CODE) = pRec->DescCode;
//   memcpy(pOutbuf+AOFF_LEGAL, pRec->Description, ASIZ_LEGAL);
//
//   // Net Values
//   long lNet = atoin(pRec->NetVal, RSIZ_LAND);
//   if (lNet > 0)
//   {
//      sprintf(acTmp, "%*u", ASIZ_NET, lNet);
//      memcpy(pOutbuf+AOFF_NET, acTmp, ASIZ_NET);
//   }
//
//   // Land
//   long lLand = atoin(pRec->Land, RSIZ_LAND);
//   if (lLand > 0)
//   {
//      sprintf(acTmp, "%*u", ASIZ_NET, lLand);
//      memcpy(pOutbuf+AOFF_LAND, acTmp, ASIZ_NET);
//   }
//
//   // Improve
//   long lImpr = atoin(pRec->Impr, RSIZ_LAND);
//   if (lImpr > 0)
//   {
//      sprintf(acTmp, "%*u", ASIZ_NET, lImpr);
//      memcpy(pOutbuf+AOFF_IMPR, acTmp, ASIZ_NET);
//   }
//
//   // Other value
//   long lFixt = atoin(pRec->Fixt, RSIZ_LAND);
//   long lPers = atoin(pRec->PP_Val, RSIZ_LAND);
//   lPers += lFixt;
//   if (lPers > 0)
//   {
//      sprintf(acTmp, "%*u", ASIZ_NET, lPers);
//      memcpy(pOutbuf+AOFF_PP_VAL, acTmp, ASIZ_NET);
//   }
//
//   // Gross total
//   lTmp = lLand+lImpr+lPers;
//   if (lTmp > 0)
//   {
//      sprintf(acTmp, "%*u", ASIZ_NET, lTmp);
//      memcpy(pOutbuf+AOFF_GROSS, acTmp, ASIZ_NET);
//
//      // Ratio
//      if (lImpr > 0)
//      {
//         dTmp = (LONGLONG)lImpr*100/(lLand+lImpr)+0.5;
//         sprintf(acTmp, "%*u", ASIZ_RATIO, (long)dTmp);
//         memcpy(pOutbuf+AOFF_RATIO, acTmp, ASIZ_RATIO);
//      }
//   }
//
//   // Land+Impr total
//   lTmp = lLand+lImpr;
//   if (lTmp > 0)
//   {
//      sprintf(acTmp, "%*u", ASIZ_NET, lTmp);
//      memcpy(pOutbuf+AOFF_LAND_IMP, acTmp, ASIZ_NET);
//   }
//
//   // Appeal year
//   if (pRec->AppealYr[1] > ' ')
//   {
//      pTmp = dateConversion(pRec->AppealYr, acTmp, YY2YYYY);
//      if (pTmp)
//         memcpy(pOutbuf+AOFF_APPEALS_YEAR, acTmp, ASIZ_APPEALS_YEAR);
//   }
//
//   // Exemptions
//   long lExeTotal;
//   if (pRec->Exe_Code1 > '0')
//   {
//      *(pOutbuf+AOFF_EXEMP1_CODE) = pRec->Exe_Code1;
//      lTmp = atoin(pRec->Exe_Amt1, RSIZ_EXE_AMT);
//      lExeTotal = lTmp;
//      sprintf(acTmp, "%*u", ASIZ_EXEMP1_VAL, lTmp);
//      memcpy(pOutbuf+AOFF_EXEMP1_VAL, acTmp, ASIZ_EXEMP1_VAL);
//      if (pRec->Exe_Code2 > '0')
//      {
//         *(pOutbuf+AOFF_EXEMP2_CODE) = pRec->Exe_Code2;
//         lTmp = atoin(pRec->Exe_Amt2, RSIZ_EXE_AMT);
//         lExeTotal += lTmp;
//         sprintf(acTmp, "%*u", ASIZ_EXEMP1_VAL, lTmp);
//         memcpy(pOutbuf+AOFF_EXEMP2_VAL, acTmp, ASIZ_EXEMP1_VAL);
//      }
//      if (pRec->Exe_Code3 > '0')
//      {
//         *(pOutbuf+AOFF_EXEMP3_CODE) = pRec->Exe_Code2;
//         lTmp = atoin(pRec->Exe_Amt3, RSIZ_EXE_AMT);
//         lExeTotal += lTmp;
//         sprintf(acTmp, "%*u", ASIZ_EXEMP1_VAL, lTmp);
//         memcpy(pOutbuf+AOFF_EXEMP3_VAL, acTmp, ASIZ_EXEMP1_VAL);
//      }
//
//      // Total
//      sprintf(acTmp, "%*u", ASIZ_EXEMP1_VAL, lExeTotal);
//      memcpy(pOutbuf+AOFF_EXEMP_TOTAL, acTmp, ASIZ_EXEMP1_VAL);
//   }
//
//   // Tract/Map number
//   memcpy(pOutbuf+AOFF_MAP_NUMBER, pRec->MapNum, ASIZ_MAP_NUMBER);
//
//   // Original Cut Number
//   lTmp = atoin(pRec->Orig_Cut_No, RSIZ_ORIG_CUT_NO);
//   if (lTmp > 0)
//   {
//      sprintf(acTmp, "%*u", ASIZ_ORG_CUT_NUMBER, lTmp);
//      memcpy(pOutbuf+AOFF_ORG_CUT_NUMBER, acTmp, ASIZ_ORG_CUT_NUMBER);
//
//      // Original Cut Date
//      pTmp = dateConversion(pRec->Orig_Cut_Date, acTmp, MMDDYY2);
//      if (pTmp)
//         memcpy(pOutbuf+AOFF_ORG_CUT_DATE, acTmp, ASIZ_ORG_CUT_DATE);
//   }
//
//   // TRA Cut Number
//   lTmp = atoin(pRec->Tra_Cut_No, RSIZ_ORIG_CUT_NO);
//   if (lTmp > 0)
//   {
//      sprintf(acTmp, "%*u", ASIZ_TRA_CUT_NUMBER, lTmp);
//      memcpy(pOutbuf+AOFF_TRA_CUT_NUMBER, acTmp, ASIZ_TRA_CUT_NUMBER);
//
//      // TRA Cut Date
//      pTmp = dateConversion(pRec->Tra_Cut_Date, acTmp, MMDDYY2);
//      if (pTmp)
//         memcpy(pOutbuf+AOFF_TRA_CUT_DATE, acTmp, ASIZ_TRA_CUT_DATE);
//   }
//
//   // Redraft Cut Number
//   lTmp = atoin(pRec->Redraft_Cut_No, RSIZ_ORIG_CUT_NO);
//   if (lTmp > 0)
//   {
//      sprintf(acTmp, "%*u", ASIZ_REDRAFT_CUT_NUMBER, lTmp);
//      memcpy(pOutbuf+AOFF_REDRAFT_CUT_NUMBER, acTmp, ASIZ_REDRAFT_CUT_NUMBER);
//
//      // Redraft Cut Date
//      pTmp = dateConversion(pRec->Redraft_Cut_Date, acTmp, MMDDYY2);
//      if (pTmp)
//         memcpy(pOutbuf+AOFF_REDRAFT_CUT_DATE, acTmp, ASIZ_REDRAFT_CUT_DATE);
//   }
//
//   // Old TRA
//   if (pRec->Old_Tra[0] > ' ')
//      memcpy(pOutbuf+AOFF_OLD_TRA, pRec->Old_Tra, ASIZ_OLD_TRA);
//
//   // Old APN
//   if (pRec->Old_Apn[0] > ' ')
//      memcpy(pOutbuf+AOFF_OLD_APN, pRec->Old_Apn, ASIZ_OLD_APN);
//
//   // Acreage (V99): Lot sqft - Lot Acres
//   lTmp = atoin(pRec->Acreage, RSIZ_ACREAGE);
//   if (lTmp > 0)
//   {
//      long lLotSqft;
//
//      sprintf(acTmp, "%*.2f", ASIZ_ACRES, (double)lTmp/100);
//      memcpy(pOutbuf+AOFF_ACRES, acTmp, ASIZ_ACRES);
//
//      lLotSqft = (long)((double)lTmp*SQFT_FACTOR_100+0.5);
//      sprintf(acTmp, "%*u", ASIZ_LOT_SQFT, lLotSqft);
//      memcpy(pOutbuf+AOFF_LOT_SQFT, acTmp, ASIZ_LOT_SQFT);
//
//      // Land value per acre
//      if (lLand > 0)
//      {
//         dTmp = (double)lLand*100/lTmp;
//         sprintf(acTmp, "%*.2f", ASIZ_LANDVAL_ACRE, dTmp);
//         memcpy(pOutbuf+AOFF_LANDVAL_ACRE, acTmp, ASIZ_LANDVAL_ACRE);
//
//         // Land value per lot sqft
//         dTmp = (double)lLand/lLotSqft;
//         sprintf(acTmp, "%*.2f", ASIZ_LANDVAL_LOT_SQFT, dTmp);
//         memcpy(pOutbuf+AOFF_LANDVAL_LOT_SQFT, acTmp, ASIZ_LANDVAL_LOT_SQFT);
//      }
//   }
//
//   // Units
//   lTmp = atoin(pRec->Units, RSIZ_UNITS);
//   if (lTmp > 0)
//   {
//      sprintf(acTmp, "%*u", ASIZ_UNITS, lTmp);
//      memcpy(pOutbuf+AOFF_UNITS, acTmp, ASIZ_UNITS);
//   }
//
//   // Zoning/UseCode
//   memcpy(pOutbuf+AOFF_ZONE_USE, &pRec->Zoning, ASIZ_ZONE_USE);
//
//   // Redeem flag
//   if (pRec->Sold_Red_Code > ' ')
//   {
//      *(pOutbuf+AOFF_REDEEMED_FLAG) = pRec->Sold_Red_Code;
//      if (pRec->Sold_Red_Year[0] > ' ')
//      {
//         pTmp = dateConversion(pRec->Sold_Red_Year, acTmp, YY2YYYY);
//         if (pTmp)
//            memcpy(pOutbuf+AOFF_REDEEMED_YEAR, acTmp, ASIZ_REDEEMED_YEAR);
//      }
//   }
//
//   // Tax status
//   *(pOutbuf+AOFF_TAX_STATUS) = pRec->Tax_Status;
//
//   // Assessment year
//   if (pRec->Asmnt_Yr[0] >= '0')
//   {
//      memcpy(pOutbuf+AOFF_ASSESSMENT_YEAR, "20", 2);
//      memcpy(pOutbuf+AOFF_ASSESSMENT_YEAR+2, pRec->Asmnt_Yr, 2);
//   }
//
//   // DocDate - MMDDYY
//   if (memcmp(pRec->DocDate, "000000", 6) > 0)
//   {
//      // Apply transfer
//      pTmp = dateConversion(pRec->DocDate, acTmp, MMDDYY2);
//      if (pTmp)
//         memcpy(pOutbuf+AOFF_DOCDATE, acTmp, ASIZ_DOCDATE);
//      if (pRec->DocNum[0] > ' ')
//      {
//         iRet = Sdx_FormatDoc(acDocNum, pRec->DocNum, acTmp);
//         memcpy(pOutbuf+AOFF_DOCNUM, acDocNum, ASIZ_DOCNUM);
//
//         // DocType
//         if (pRec->DocType > '0' && pRec->DocType < '9')
//         {
//            *(pOutbuf+AOFF_DOCTYPE_CODE) = pRec->DocType;
//            memcpy(pOutbuf+AOFF_DOCTYPE_TEXT, asDocType[pRec->DocType & 0x0F].DocTitle, ASIZ_DOCTYPE_TEXT);
//         }
//      }
//   }
//
//   // Transaction date
//   if (memcmp(pRec->Trans_Date, "000000", 6) > 0)
//   {
//      pTmp = dateConversion(pRec->Trans_Date, acTmp, MMDDYY2);
//      if (pTmp)
//         memcpy(pOutbuf+AOFF_TRANS_DATE, acTmp, 8);
//   }
//
//   // Parse Mailing
//   Sdx_AsrMergeMAdr(pOutbuf, pRollRec);
//
//   // Parse Situs
//   Sdx_AsrMergeSAdr(pOutbuf, pRollRec);
//
//   // ParseOwner
//   Sdx_AsrMergeOwner(pOutbuf, pRollRec);
//
//   // Seq number
//   lSeqNum++;
//   sprintf(acTmp, "%*u", ASIZ_HAS_SEQNUM, lSeqNum);
//   memcpy(pOutbuf+AOFF_HAS_SEQNUM, acTmp, ASIZ_HAS_SEQNUM);
//
//   return 0;
//}

/******************************* Sdx_AsrMergeChar ****************************
 *
 *
 *
 *****************************************************************************/

//int Sdx_AsrMergeChar(char *pOutbuf)
//{
//
//   static   char acRec[1024], *pRec=NULL;
//   char     acTmp[256], *pTmp;
//   long     lTmp, lBldgSqft, lLotAcres, lLotSqft;
//   int      iRet, iTmp, iLoop, iBeds, iFBath, iHBath;
//   double   dTmp;
//   SDX_CHAR *pChar;
//
//   iRet=iBeds=iFBath=iHBath=0;
//   lLotSqft=lBldgSqft=lLotAcres=0;
//
//   // Get first Char rec for first call
//   if (!pRec && !lCharMatch)
//   {
//      pRec = fgets(acRec, 1024, fdChar);
//   }
//   pChar = (SDX_CHAR *)pRec;
//
//   do
//   {
//      if (!pRec)
//      {
//         fclose(fdChar);
//         fdChar = NULL;
//         return 1;      // EOF
//      }
//
//      // Compare Apn
//      iLoop = memcmp(pOutbuf, pChar->Apn, CSIZ_APN);
//      if (iLoop > 0)
//      {
//         if (bDebug)
//            LogMsg0("Skip Char rec  %.*s", CSIZ_APN, pChar->Apn);
//         pRec = fgets(acRec, 1024, fdChar);
//         lCharSkip++;
//      }
//   } while (iLoop > 0);
//
//   // If not match, return
//   if (iLoop)
//      return 0;
//
//#ifdef _DEBUG
//   //if (!memcmp(pOutbuf, "003120007", 9))
//   //   lTmp = 0;
//#endif
//
//   // YrEff
//   if (pChar->EffYear[0] > ' ')
//   {
//      pTmp = dateConversion(pChar->EffYear, acTmp, YY2YYYY);
//      if (pTmp)
//         memcpy(pOutbuf+AOFF_EFF_YEAR, pTmp, ASIZ_EFF_YEAR);
//   }
//
//   // BldgSqft
//   lBldgSqft = atoin(pChar->BldgSqft, CSIZ_BLDG_SQFT);
//   if (lBldgSqft > 10)
//   {
//      sprintf(acTmp, "%*u", ASIZ_LIV_AREA, lBldgSqft);
//      memcpy(pOutbuf+AOFF_LIV_AREA, acTmp, ASIZ_LIV_AREA);
//
//      // Impr value per impr sqft
//      lTmp = atoin(pOutbuf+AOFF_IMPR, ASIZ_IMPR);
//      if (lTmp > 0)
//      {
//         dTmp = (double)lTmp/lBldgSqft;
//         sprintf(acTmp, "%*.2f", ASIZ_IMPRVAL_IMP_SQFT, dTmp);
//         memcpy(pOutbuf+AOFF_IMPRVAL_IMP_SQFT, acTmp, ASIZ_IMPRVAL_IMP_SQFT);
//      }
//   }
//
//   // Beds
//   iBeds += atoin(pChar->Beds, CSIZ_BEDROOMS);
//   if (iBeds > 0)
//   {
//      sprintf(acTmp, "%*u", ASIZ_BEDROOMS, iBeds);
//      memcpy(pOutbuf+AOFF_BEDROOMS, acTmp, ASIZ_BEDROOMS);
//   }
//
//   // Baths
//   iTmp = atoin(pChar->Baths, CSIZ_BATHS);
//   if (iTmp > 0)
//   {
//      sprintf(acTmp, "%*.1f", ASIZ_BATHS, (double)iTmp/10);
//      memcpy(pOutbuf+AOFF_BATHS, acTmp, ASIZ_BATHS);
//   }
//
//   // Garage spaces
//   iTmp = atoin(pChar->GarStalls, CSIZ_GARAGE_STALLS);
//   if (iTmp > 0)
//   {
//      sprintf(acTmp, "%*u", ASIZ_GARAGE_STALLS, iTmp);
//      memcpy(pOutbuf+AOFF_GARAGE_STALLS, acTmp, ASIZ_GARAGE_STALLS);
//   }
//
//   // Pool/Spa
//   *(pOutbuf+AOFF_POOL) = pChar->Pool;
//
//   // View - Y/N or blank
//   *(pOutbuf+AOFF_VIEW) = pChar->View;
//
//   // Acreage (V99): Lot sqft - Lot Acres
//   lTmp = atoin(pChar->Acres, CSIZ_ACREAGE);
//   if (lTmp > 0)
//   {
//      sprintf(acTmp, "%*.2f", ASIZ_ACRES, (double)lTmp/100);
//      memcpy(pOutbuf+AOFF_ACRES, acTmp, ASIZ_ACRES);
//
//      sprintf(acTmp, "%*u", ASIZ_LOT_SQFT, (long)((double)lTmp*SQFT_FACTOR_100));
//      memcpy(pOutbuf+AOFF_LOT_SQFT, acTmp, ASIZ_LOT_SQFT);
//   } else
//   {
//      // Lot sqft
//      lLotSqft = atoin(pChar->LotSqft, CSIZ_LOT_SQFT);
//      if (lLotSqft > 0)
//      {
//         sprintf(acTmp, "%*u", ASIZ_LOT_SQFT, lLotSqft);
//         memcpy(pOutbuf+AOFF_LOT_SQFT, acTmp, ASIZ_LOT_SQFT);
//
//         sprintf(acTmp, "%*.2f", ASIZ_ACRES, (double)lLotSqft/SQFT_PER_ACRE);
//         memcpy(pOutbuf+AOFF_ACRES, acTmp, ASIZ_ACRES);
//      }
//   }
//
//   lCharMatch++;
//
//   // Get next Char rec
//   pRec = fgets(acRec, 1024, fdChar);
//
//   return 0;
//}

/******************************** Sdx_CreateAssr *****************************
 *
 *
 ****************************************************************************/

//int Sdx_CreateAssr(int iFirstRec)
//{
//   char     *pTmp, acBuf[MAX_RECSIZE], acRec[MAX_RECSIZE];
//   char     acOutFile[_MAX_PATH], acTmpFile[_MAX_PATH], acGrGrFile[_MAX_PATH], acTmp[256];
//
//   HANDLE   fhOut=0;
//
//   int      iRet, iRollUpd=0, iNewRec=0, iRetiredRec=0;
//   DWORD    nBytesWritten;
//   BOOL     bRet, bEof;
//   long     lRet=0, lCnt=0;
//
//   LogMsg("Create ASSR file for %s", myCounty.acCntyCode);
//
//   if (!iAsrRecLen)
//      iAsrRecLen = 1900;
//   lCharMatch = 0;
//   lSeqNum = 0;
//
//   // Prepare GrGr file
//   sprintf(acGrGrFile, acGrGrTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "DAT");
//   sprintf(acTmpFile, acGrGrTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "SLS");
//   LogMsg("Sort GrGr file %s to %s", acTmpFile, acGrGrFile);
//
//   // Sort Sdx_GrGr.sls to Sdx_GrGr.dat
//   strcpy(acTmp,"S(1,28,C,A) F(TXT) DUPO(1,28,246,10) ");
//   lCnt = sortFile(acTmpFile, acGrGrFile, acTmp);
//
//   // Open GrGr file
//   if (!_access(acGrGrFile, 0))
//   {
//      LogMsg("Open GrGr file %s", acGrGrFile);
//      fdGrGr = fopen(acGrGrFile, "r");
//      if (fdGrGr == NULL)
//      {
//         LogMsg("***** Error opening GrGr file: %s\n", acGrGrFile);
//         return 2;
//      }
//   } else
//   {
//      LogMsg("*** Missing GrGr file %s", acGrGrFile);
//      fdGrGr = NULL;
//   }
//
//   // Prepare Sale file
//   sprintf(acSaleFile, acSaleTmpl, myCounty.acCntyCode, "DAT");
//   sprintf(acTmpFile, acSaleTmpl, myCounty.acCntyCode, "SLS");
//   LogMsg("Sort Sale file %s to %s", acTmpFile, acSaleFile);
//
//   // Sort sale file Sale_exp.sls to Sale_exp.dat on APN (asc), Saledate (asc), DocNum (asc)
//   sprintf(acTmp, "S(1,10,C,A,27,8,C,A,15,7,C,A) DUPO(1,34)");
//   lCnt = sortFile(acTmpFile, acSaleFile, acTmp);
//   if (!_access(acSaleFile, 0))
//   {
//      // Open Sales file - Need to be sorted on APN and EventDate
//      LogMsg("Open Sales file %s", acSaleFile);
//      fdSale = fopen(acSaleFile, "r");
//      if (fdSale == NULL)
//      {
//         LogMsg("***** Error opening Sales file: %s\n", acSaleFile);
//         return 2;
//      }
//   } else
//   {
//      LogMsg("*** Missing sale file %s", acSaleFile);
//      fdSale = NULL;
//   }
//
//   // Open roll file
//   LogMsg("Open Roll file %s", acRollFile);
//   fdRoll = fopen(acRollFile, "r");
//   if (fdRoll == NULL)
//   {
//      LogMsg("***** Error opening roll file: %s\n", acRollFile);
//      return 2;
//   }
//
//   // Open Char file
//   LogMsg("Open Char file %s", acCharFile);
//   fdChar = fopen(acCharFile, "r");
//   if (fdChar == NULL)
//   {
//      LogMsg("***** Error opening Char file: %s\n", acCharFile);
//      return 2;
//   }
//
//   // Open Output file
//   sprintf(acOutFile, acAsrTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "R01");
//   LogMsg("Open output file %s", acOutFile);
//   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
//          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);
//
//   if (fhOut == INVALID_HANDLE_VALUE)
//   {
//      LogMsg("***** Error creating output file: %s\n", acOutFile);
//      return 4;
//   }
//
//   // Output first header record
//   if (iFirstRec > 0)
//   {
//      memset(acBuf, '9', iAsrRecLen);
//      bRet = WriteFile(fhOut, acBuf, iAsrRecLen, &nBytesWritten, NULL);
//   }
//
//   // Get 1st rec
//   pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdRoll);
//   bEof = (pTmp ? false:true);
//
//   // Init variables
//   iNoMatch=iBadCity=iBadSuffix=0;
//   lCnt=lCharMatch=lSaleMatch = 0;
//
//   // Merge loop
//   while (pTmp && !feof(fdRoll))
//   {
//      // Create new R01 record
//      iRet = Sdx_AsrMergeRoll(acBuf, acRec, iAsrRecLen, CREATE_R01|CREATE_LIEN);
//
//      // Merge Char
//      if (fdChar)
//         lRet = Sdx_AsrMergeChar(acBuf);
//
//      // Merge Sales
//      if (fdSale)
//         lRet = Sdx_AsrMergeSale(acBuf);
//
//      // Merge GrGr - Turn this ON when data is ready
//      if (fdGrGr)
//         lRet = Sdx_AsrMergeGrGr(acBuf);
//
//      // Populate last sale
//      if (memcmp(&acBuf[AOFF_S1_RECDATE], &acBuf[AOFF_1DT], SIZ_SALE1_DT) >= 0)
//      {
//         memcpy(&acBuf[AOFF_LAST_RECDT], &acBuf[AOFF_S1_RECDATE], SIZ_SALE1_DT);
//         memcpy(&acBuf[AOFF_LAST_AMT], &acBuf[AOFF_S1_PRICE], SIZ_SALE1_AMT);
//      } else
//      {
//         memcpy(&acBuf[AOFF_LAST_RECDT], &acBuf[AOFF_1DT], SIZ_SALE1_DT);
//         memcpy(&acBuf[AOFF_LAST_AMT], &acBuf[AOFF_1AMT], SIZ_SALE1_AMT);
//      }
//
//      if (!iRet)
//      {
//         iNewRec++;
//#ifdef _DEBUG
//         //iRet = replChar(acBuf, 0, ' ', iAsrRecLen);
//         //if (iRet)
//         //   iRet = 0;
//#endif
//         // Save last recording date
//         lRet = atoin((char *)&acBuf[AOFF_DOCDATE], 8);
//         if (lRet > lLastRecDate && lRet < lToday)
//            lLastRecDate = lRet;
//
//         lCnt++;
//         bRet = WriteFile(fhOut, acBuf, iAsrRecLen, &nBytesWritten, NULL);
//         if (!(lCnt % 1000))
//            printf("\r%u", lCnt);
//
//         if (!bRet)
//         {
//            LogMsg("Error writing to output file at record %d\n", lCnt);
//            lRet = WRITE_ERR;
//            break;
//         }
//      }
//
//      // Get next roll record
//      pTmp = fgets(acRec, MAX_RECSIZE, fdRoll);
//   }
//
//   // Close files
//   if (fdGrGr)
//      fclose(fdGrGr);
//   if (fdRoll)
//      fclose(fdRoll);
//   if (fdChar)
//      fclose(fdChar);
//   if (fdSale)
//      fclose(fdSale);
//   if (fhOut)
//      CloseHandle(fhOut);
//
//   // Sort output file
//   //LogMsg("Sort output file %s to %s", acTmpFile, acOutFile);
//   //sprintf(acTmp, "S(1,14,C,A) F(FIX,%d) B(%d,R)", iAsrRecLen, iAsrRecLen);
//   //lRet = sortFile(acTmpFile, acOutFile, acTmp);
//
//   LogMsg("Total output records:       %u", lCnt);
//   LogMsg("Total retired records:      %u", iRetiredRec);
//   LogMsg("Total bad-city records:     %u", iBadCity);
//   LogMsg("Total bad-suffix records:   %u\n", iBadSuffix);
//
//   LogMsg("Number of Char matched:     %u", lCharMatch);
//   LogMsg("Number of Sale matched:     %u", lSaleMatch);
//   LogMsg("Number of GrGr matched:     %u\n", lGrGrMatch);
//
//   LogMsg("Number of Char skiped:      %u", lCharSkip);
//
//   LogMsg("Last recording date:        %u", lLastRecDate);
//   printf("\nTotal output records: %u", lCnt);
//
//   lAssrRecCnt = lCnt;
//   return 0;
//}

/********************************* Sdx_MergeAssr ******************************
 *
 * This function has not been tested.
 *
 ******************************************************************************

int Sdx_MergeAssr(int iSkip)
{
   char     *pTmp, acRec[MAX_RECSIZE], acBuf[MAX_RECSIZE], acRollRec[MAX_RECSIZE];
   char     cFileCnt=1;
   char     acRawFile[_MAX_PATH], acOutFile[_MAX_PATH], acTmpFile[_MAX_PATH];

   HANDLE   fhIn, fhOut;

   int      iRet, iTmp, iRollUpd=0, iNewRec=0, iRetiredRec=0;
   DWORD    nBytesRead;
   DWORD    nBytesWritten;
   BOOL     bRet, bEof;
   long     lRet=0, lCnt=0;

   iApnLen = myCounty.iApnLen;
   lSeqNum = 0;

   sprintf(acRawFile, acAsrTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "S01");
   sprintf(acOutFile, acAsrTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "R01");
   sprintf(acTmpFile, acGrGrTmpl, myCounty.acCntyCode, "DAT");

   // Open GrGr file
   if (!_access(acTmpFile, 0))
   {
      LogMsg("Open GrGr file %s", acTmpFile);
      fdGrGr = fopen(acTmpFile, "r");
      if (fdGrGr == NULL)
      {
         LogMsg("***** Error opening GrGr file: %s\n", acTmpFile);
         return 2;
      }
   } else
      fdGrGr = NULL;

   // Rename files for processing
   if (_access(acRawFile, 0))
   {
      if (!_access(acOutFile, 0))
         rename(acOutFile, acRawFile);
      else
      {
         LogMsg("Missing input file %s.  Please recheck!", acRawFile);
         return 1;
      }
   }

   // Open roll file
   LogMsg("Open Roll file %s", acRollFile);
   fdRoll = fopen(acRollFile, "r");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", acRollFile);
      return 2;
   }

   // Open Char file
   LogMsg("Open Char file %s", acCharFile);
   fdChar = fopen(acCharFile, "r");
   if (fdChar == NULL)
   {
      LogMsg("***** Error opening Char file: %s\n", acCharFile);
      return 2;
   }

   // Open Cum Sales file
   LogMsg("Open Sales file %s", acSaleFile);
   fdSale = fopen(acSaleFile, "r");
   if (fdSale == NULL)
   {
      LogMsg("***** Error opening Sales file: %s\n", acSaleFile);
      return 2;
   }

   // Open Input file
   LogMsg("Open input file %s", acRawFile);
   fhIn = CreateFile(acRawFile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhIn == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening input file: %s\n", acRawFile);
      return 3;
   }

   // Open Output file - use tmp file so we can sort at the end to R01 file
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening output file: %s\n", acOutFile);
      return 4;
   }

   // Get 1st rec
   pTmp = fgets((char *)&acRollRec[0], MAX_RECSIZE, fdRoll);
   bEof = (pTmp ? false:true);

   // Copy skip record
   memset(acBuf, ' ', iAsrRecLen);
   while (iSkip-- > 0)
   {
      ReadFile(fhIn, acBuf, iAsrRecLen, &nBytesRead, NULL);
      bRet = WriteFile(fhOut, acBuf, iAsrRecLen, &nBytesWritten, NULL);
   }

   iNoMatch=iBadCity=iBadSuffix=0;
   SDX_ROLL *pRoll = (SDX_ROLL *)&acRollRec[0];

   // Merge loop
   while (!bEof)
   {
      bRet = ReadFile(fhIn, acBuf, iAsrRecLen, &nBytesRead, NULL);

      // Check for EOF
      if (!bRet)
      {
         LogMsg("Error reading input file %s (%f)", acRawFile, GetLastError());
         break;
      }

      // EOF ?
      if (!nBytesRead)
         break;

NextRollRec:
      iTmp = memcmp((char *)&acBuf[AOFF_APN], (char *)&acRollRec[ROFF_APN], iApnLen);
      if (!iTmp)
      {
         // Merge roll data
         iRet = Sdx_AsrMergeRoll(acBuf, acRollRec, MAX_RECSIZE, UPDATE_R01);

         // Merge Char
         if (fdChar)
            lRet = Sdx_AsrMergeChar(acBuf);

         // Merge Sales
         if (fdSale)
            lRet = Sdx_AsrMergeSale(acBuf);

         iRollUpd++;

         // Read next roll record
         pTmp = fgets(acRollRec, MAX_RECSIZE, fdRoll);

         if (!pTmp)
            bEof = true;    // Signal to stop
      } else if (iTmp > 0)       // Roll not match, new roll record?
      {
         if (bDebug)
            LogMsg0("*** New roll record : %.*s (%d)", iApnLen, acRollRec, lCnt);

         // Create new R01 record
         iRet = Sdx_AsrMergeRoll(acRec, acRollRec, MAX_RECSIZE, CREATE_R01);

         // Merge Char
         if (fdChar)
            lRet = Sdx_AsrMergeChar(acRec);

         // Merge Sales
         if (fdSale)
            lRet = Sdx_AsrMergeSale(acRec);

         // Save last recording date
         lRet = atoin((char *)&acRec[AOFF_DOCDATE], 8);
         if (lRet > lLastRecDate && lRet < lToday)
            lLastRecDate = lRet;

         iNewRec++;
         bRet = WriteFile(fhOut, acRec, iAsrRecLen, &nBytesWritten, NULL);
         lCnt++;

         // Get next roll record
         pTmp = fgets(acRollRec, MAX_RECSIZE, fdRoll);

         if (!pTmp)
            bEof = true;    // Signal to stop
         else
            goto NextRollRec;
      } else
      {
         // Record may be retired
         if (bDebug)
            LogMsg0("*** Roll not match (retired record?) : R01->%.*s < Roll->%.*s (%d) ***", iApnLen, acBuf, iApnLen, (char *)&acRollRec[1], lCnt);
         iRetiredRec++;
         continue;
      }

      // Save last recording date
      lRet = atoin((char *)&acBuf[AOFF_DOCDATE], 8);
      if (lRet > lLastRecDate && lRet < lToday)
         lLastRecDate = lRet;

      bRet = WriteFile(fhOut, acBuf, iAsrRecLen, &nBytesWritten, NULL);
      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);

      if (!bRet)
      {
         LogMsg("Error occurs: %d\n", GetLastError());
         lRet = WRITE_ERR;
         break;
      }
   }

   // Finish remainder of roll file
   while (!bEof)
   {
      if (bDebug)
         LogMsg0("*** New roll record : %.*s (%d)", iApnLen, acRollRec, lCnt);

      // Create new R01 record
      iRet = Sdx_AsrMergeRoll(acRec, acRollRec, MAX_RECSIZE, CREATE_R01);

      // Merge Char
      if (fdChar)
         lRet = Sdx_AsrMergeChar(acRec);

      // Merge Sales
      if (fdSale)
         lRet = Sdx_AsrMergeSale(acRec);

      // Save last recording date
      lRet = atoin((char *)&acRec[AOFF_DOCDATE], 8);
      if (lRet > lLastRecDate && lRet < lToday)
         lLastRecDate = lRet;

      iNewRec++;
      bRet = WriteFile(fhOut, acRec, iAsrRecLen, &nBytesWritten, NULL);
      lCnt++;

      // Get next roll record
      pTmp = fgets(acRollRec, MAX_RECSIZE, fdRoll);

      if (!pTmp)
         bEof = true;    // Signal to stop
   }

   // Close files
   if (fdGrGr)
      fclose(fdGrGr);
   if (fdRoll)
      fclose(fdRoll);
   if (fdChar)
      fclose(fdChar);
   if (fdSale)
      fclose(fdSale);
   if (fhOut)
      CloseHandle(fhOut);
   if (fhIn)
      CloseHandle(fhIn);

   // Sort output file
   //sprintf(acTmp, "S(1,14,C,A) F(FIX,%d) B(%d,R)", iAsrRecLen, iAsrRecLen);
   //lRet = sortFile(acTmpFile, acOutFile, acTmp);

   LogMsg("Total output records:       %u", lCnt);
   LogMsg("Total new records:          %u", iNewRec);
   LogMsg("Total retired records:      %u", iRetiredRec);
   LogMsg("Total bad-city records:     %u", iBadCity);
   LogMsg("Total bad-suffix records:   %u\n", iBadSuffix);

   LogMsg("Number of Char matched:     %u", lCharMatch);
   LogMsg("Number of Sale matched:     %u\n", lSaleMatch);

   LogMsg("Number of Char skiped:      %u", lCharSkip);
   LogMsg("Number of Sale skiped:      %u\n", lSaleSkip);

   LogMsg("Last recording date:        %u", lLastRecDate);

   printf("\nTotal output records: %u", lCnt);

   lAssrRecCnt = lCnt;
   return 0;
}

/****************************** Sdx_CreateGrGrRec ****************************
 *
 * Create sale record from GrGr record.
 * Record type 1 = grantee
 *             2 = description
 *             3 = grantor
 *             F = ignore
 *
 * Return 0 if no update, 1 if updated
 *
 *****************************************************************************/

int Sdx_CreateGrGrRec(char *pGrGr, char *pSale)
{
   char     acTmp[256];
   long     lTmp;
   double   dTmp;

   SDX_GRGR *pGrGrRec=(SDX_GRGR *)pGrGr;
   SDX_GREC *pSaleRec=(SDX_GREC *)pSale;

   // Only accept up to 2 names (buyer) for each sale.  Ignore the rest.
   if (pSaleRec->Grantee[1][0] > ' ' && pGrGrRec->Record_Type == '1')
   {
      if (bDebug)
         LogMsg0("Ignore buyer %.24s from %.10s", pGrGrRec->Name, pGrGrRec->Apn);
      return 0;
   }

   // Keep no more than 2 sellers
   if (pSaleRec->Grantor[1][0] > ' ' && pGrGrRec->Record_Type == '3')
   {
      if (bDebug)
         LogMsg0("Ignore seller %.24s from %.10s", pGrGrRec->Name, pGrGrRec->Apn);
      return 0;
   }

   // Copy APN
   if (pSaleRec->Apn[0] < '0')
      memcpy(pSaleRec->Apn, pGrGrRec->Apn, iGrGrApnLen);

   // Copy recording date and doc number
   if (pSaleRec->DocDate[0] < '0')
   {
      memcpy(pSaleRec->DocNum, pGrGrRec->RecNum, GSIZ_RECNUMBER);
      memcpy(pSaleRec->DocDate, pGrGrRec->RecDate, GSIZ_RECDATE);
   }

   // Doc Tax
   lTmp = atoin(pGrGrRec->DocTax, GSIZ_DOCTAX);
   if (lTmp > 0 && pSaleRec->DocTax[GSIZ_DOCTAX-1] == ' ')
   {
      sprintf(acTmp, "%*.2f", GSIZ_DOCTAX, (double)lTmp/100);
      memcpy(pSaleRec->DocTax, acTmp, GSIZ_DOCTAX);

      // Calculating sale price
      if (lTmp >= 5500)
      {
         // We assume no sale is less than 50,000.  Lower than that would be
         // Tax Deed, County Deed, or Conveyance which is not a sale.
         dTmp = (double)lTmp*SALE_FACTOR_100;
         sprintf(acTmp, "%*u", SIZ_SALE1_AMT, (long)dTmp);
         memcpy(pSaleRec->SalePrice, acTmp, SIZ_SALE1_AMT);
      }
   }

   // Instrument Type
   memcpy(pSaleRec->InstType, pGrGrRec->Inst_Type, GSIZ_INST_TYPE);

   // Translate Inst type to Doc type
   /*
   int      iTmp, iIdx;

  memcpy(acTmp, pGrGrRec->Inst_Type, GSIZ_INST_TYPE);
   acTmp[GSIZ_INST_TYPE] = 0;
   iIdx = XrefCode2Idx(&asInst[0], acTmp, iNumInst);
   if (iIdx > 0)
   {
      iTmp = sprintf(acTmp, "%d", iIdx);
      memcpy(pSaleRec->DocType, acTmp, iTmp);
      //pTmp = XrefIdx2Name(&asInst[0], iIdx, iNumInst);
   } else if (lTmp > 5000)
   {
      LogMsg0("Inst type = %s, DocTax = %d", acTmp, lTmp);
   }
   */
   // Preset to GD since that's all we have
   pSaleRec->DocType[0] = '1';

   // Buyer
   if (pGrGrRec->Record_Type == '1')
   {
      memcpy(acTmp, pGrGrRec->Name, GSIZ_NAME);
      memcpy(&acTmp[GSIZ_NAME], pGrGrRec->Name_Cont, GSIZ_NAME_CONT);
      blankRem(acTmp, GSIZ_NAME+GSIZ_NAME_CONT);
      acTmp[GSIZ_GRANTOR] = 0;

      if (pSaleRec->Grantee[0][0] == ' ')
         memcpy(&pSaleRec->Grantee[0], acTmp, strlen(acTmp));
      else
         memcpy(&pSaleRec->Grantee[1], acTmp, strlen(acTmp));
   }

   // Seller
   if (pGrGrRec->Record_Type == '3')
   {
      memcpy(acTmp, pGrGrRec->Name, GSIZ_NAME);
      memcpy(&acTmp[GSIZ_NAME], pGrGrRec->Name_Cont, GSIZ_NAME_CONT);
      blankRem(acTmp, GSIZ_NAME+GSIZ_NAME_CONT);
      acTmp[GSIZ_GRANTOR] = 0;

      if (pSaleRec->Grantor[0][0] == ' ')
         memcpy(&pSaleRec->Grantor[0], acTmp, strlen(acTmp));
      else
         memcpy(&pSaleRec->Grantor[1], acTmp, strlen(acTmp));
   }

   return 1;
}

/***************************** Sdx_CreateGrGrFile ****************************
 *
 * Input record is variable length terminated by CRLF.  Append spaces to the
 * end to fill up record.
 *
 *
 *****************************************************************************/

int Sdx_CreateGrGrFile(char *pCnty)
{
   char     *pTmp, acOutRec[512], acGrGrRec[512], Blank99[100];
   char     acTmp[256], acDate[32];
   char     acGrGrIn[_MAX_PATH], acGrGrOut[_MAX_PATH], acGrGrBak[_MAX_PATH];
   char     acGrGrFile[_MAX_PATH];
   struct   _finddata_t  sFileInfo;

   FILE     *fdOut, *fdGrGr;
   SDX_GRGR *pGrGrRec=(SDX_GRGR *)&acGrGrRec[0];
   SDX_GREC *pOutRec =(SDX_GREC *)&acOutRec[0];

   int      iRet, iGrGrLen;
   BOOL     bEof=false;
   long     lCnt, lHandle, lTax, lDateLimit, lTmp;

   // Inititalize
   getPrevMonth(acTmp, 4);          // Get the date 4 months ago
   lDateLimit = atol(acTmp);

   memset(Blank99, ' ', 99);
   Blank99[99] = 0;
   memset(acOutRec, ' ', 512);
   pOutRec->CRLF[0] = '\n';
   pOutRec->CRLF[1] = 0;
   if (!iGrGrApnLen)
      iGrGrApnLen = myCounty.iApnLen;

   // Get raw file name
   GetIniString(pCnty, "GrGrIn", "", acGrGrIn, _MAX_PATH, acIniFile);
   strcpy(acGrGrBak, acGrGrIn);
   pTmp = strrchr(acGrGrBak, '\\');
   if (pTmp)
      *++pTmp = 0;
   dateString(acDate, 0);
   sprintf(acTmp, "GrGr_%s", acDate);
   strcat(acGrGrBak, acTmp);

   // Open Input file
   lHandle = _findfirst(acGrGrIn, &sFileInfo);
   if (lHandle > 0)
   {
      pTmp = strrchr(acGrGrIn, '\\');
      if (pTmp)
         *pTmp = 0;
      iRet = 0;
   } else
   {
      LogMsg("*** No new file avail for processing: %s", acGrGrIn);
      return -1;
   }

   iGrGrLen = GetPrivateProfileInt(pCnty, "GrGrRecSize", 102, acIniFile);

   // Create Output file
   sprintf(acGrGrOut, acGrGrTmpl, pCnty, pCnty, "TMP");

   LogMsg("Open output file %s", acGrGrOut);
   fdOut = fopen(acGrGrOut, "w");
   if (fdOut == NULL)
   {
      printf("Error creating GrGr output file: %s\n", acGrGrOut);
      _findclose(lHandle);
      return -2;
   }

   // Create backup folder
   if (_access(acGrGrBak, 0))
      _mkdir(acGrGrBak);

   lCnt = 0;
   while (!iRet)
   {
      // Open GrGr data file
      sprintf(acGrGrFile, "%s\\%s", acGrGrIn, sFileInfo.name);
      fdGrGr = fopen(acGrGrFile, "r");
      if (!fdGrGr)
      {
         LogMsg("***** Bad input file %s", acGrGrFile);
         break;
      }

      // Update GrGr file date
      lTmp = getFileDate(acGrGrFile);
      if (lTmp > lLastGrGrDate)
         lLastGrGrDate = lTmp;

      while (!feof(fdGrGr))
      {
         // Read Grgr rec
         pTmp = fgets(acGrGrRec, 512, fdGrGr);
         if (!pTmp)
         {
            // Output last record
            if (pOutRec->Apn[0] > ' ')
               fputs(acOutRec, fdOut);
            break;
         }

         lCnt++;
         if (!(lCnt % 1000))
            printf("\r%u", lCnt);

         // Ignore record type F, 2, or no APN
         if (pGrGrRec->Record_Type == 'F' || pGrGrRec->Record_Type == '2' ||
             pGrGrRec->Apn[0] < '1')
            continue;

         // Only take doc with InstType="001"
         if (memcmp(pGrGrRec->Inst_Type, "001", 3))
            continue;

         // Check for date limit.  This assumes SDX is sending us sale file quarterly.
         lTmp = atoin(pGrGrRec->RecDate, GSIZ_RECDATE);
         if (lTmp < lDateLimit)
            continue;

         // DocTax 55 dollar or less may not be a sale
         lTax = atoin(pGrGrRec->DocTax, GSIZ_DOCTAX);
         if (lTax < 5500)
            continue;

         // Fill up APN with 0
         for (iRet=iGrGrApnLen-1; iRet > 6; iRet--)
         {
            if (pGrGrRec->Apn[iRet] > ' ')
               break;
            else
               pGrGrRec->Apn[iRet] = '0';
         }

         // Append blank to prevent record shorter than defined
         if (pGrGrRec->termChar != 'Z')
            strcat(acGrGrRec, Blank99);

         // If same doc, increase iCnt
         if (pOutRec->Apn[0] > ' ' &&
            (memcmp(pGrGrRec->Apn, pOutRec->Apn, iGrGrApnLen) ||
             memcmp(pGrGrRec->RecNum, pOutRec->DocNum, GSIZ_RECNUMBER)) )
         {
            iRet = fputs(acOutRec, fdOut);
            memset(acOutRec, ' ', sizeof(SDX_GREC)-2);
         }

         iRet = Sdx_CreateGrGrRec(acGrGrRec, acOutRec);
      }

      LogMsg("Total records so far     : %d", lCnt);

      fclose(fdGrGr);

      // Move input file to backup folder
      sprintf(acTmp, "%s\\%s", acGrGrBak, sFileInfo.name);
      rename(acGrGrFile, acTmp);

      // Find next file
      iRet = _findnext(lHandle, &sFileInfo);
   }
   printf("\n%d\n", lCnt);


   // Close handle
   _findclose(lHandle);

   // Close files
   if (fdOut)
      fclose(fdOut);

   LogMsg("Total processed records  : %u", lCnt);

   if (lCnt > 0)
   {
      // Sort output file and dedup if same docdate and docnum
      // Sort on APN asc, RecDate asc, DocNum asc, Doc Type asc
      // For SDX, we sort GrGr file in ascending order so oldest sale goes first
      strcpy(acTmp,"S(1,28,C,A) F(TXT) DUPO(1,28,246,10) ");
      strcpy(acGrGrIn, acGrGrOut);
      sprintf(acGrGrOut, acGrGrTmpl, pCnty, pCnty, "DAT");
      // Sort Sdx_GrGr.tmp to Sdx_GrGr.dat
      LogMsg("Sorting %s to %s", acGrGrIn, acGrGrOut);
      lCnt = sortFile(acGrGrIn, acGrGrOut, acTmp);

      // Update cumulative GrGr file
      if (lCnt > 0)
      {
         // Sort input file - Append Sdx_GrGr.Tmp to Sdx_GrGr.sls for backup
         //sprintf(acTmp, "S(1,14,C,A,27,8,C,A,15,12,C,A,57,10,C,D) DUPO(1,36)");
         sprintf(acGrGrIn, acGrGrTmpl, pCnty, pCnty, "Sls");
         sprintf(acGrGrBak, acGrGrTmpl, pCnty, pCnty, "Srt");
         sprintf(acGrGrRec, "%s+%s", acGrGrIn, acGrGrOut);
         iRet = sortFile(acGrGrRec, acGrGrBak, acTmp);
         if (iRet > 1000)
         {
            remove(acGrGrIn);
            iRet = rename(acGrGrBak, acGrGrIn);
         } else
            iRet = -1;
      } else
         iRet = -5;
   } else
      iRet = -5;

   LogMsg("Total GrGr records output after dedup: %u", lCnt);
   printf("\nTotal GrGr records output after dedup: %u\n", lCnt);

   return iRet;
}

/******************************** Sdx_LoadGrGr *******************************
 *
 * Logic:
 *    - Load all docs
 *    - If there is tax amount, calculate sale price
 *    - Merge all docs with same doc#.
 * Return 0 if successful, otherwise error
 *
 *****************************************************************************/

int Sdx_LoadGrGr(char *pCnty)
{
   char     *pTmp, acRec[MAX_RECSIZE], acOutRec[1024], acTmp[512], acLastDoc[256], cGrGrDelim;
   char     acInFile[512], acGrGrIn[_MAX_PATH], acGrGrOut[_MAX_PATH], acGrGrBak[_MAX_PATH];
   int      iGrantors, iGrantees, iRet, iTmp;
   long     lCnt, lHandle, lSalePrice, lRecOut;
   double   dTax;

   FILE     *fdIn, *fdOut;
   SDX_GREC *pGrGrRec = (SDX_GREC *)&acOutRec[0] ;
   struct   _finddata_t  sFileInfo;
   
   LogMsg0("Loading GrGr file for %s", pCnty);

   // Get raw file name
   GetIniString(pCnty, "GrGrIn", "", acGrGrIn, _MAX_PATH, acIniFile);
   LogMsg("Load %s GrGr file: %s", pCnty, acGrGrIn);
   GetIniString(pCnty, "GrGrDelim", ",", acTmp, _MAX_PATH, acIniFile);
   cGrGrDelim = acTmp[0];

   // Prepare backup folder
   sprintf(acGrGrBak, acGrBkTmpl, pCnty, acToday);
   if (_access(acGrGrBak, 0))
      _mkdir(acGrGrBak);

   // Create temp output file
   sprintf(acGrGrOut, acGrGrTmpl, pCnty, pCnty, "TMP");

   LogMsg("Open output file %s", acGrGrOut);
   fdOut = fopen(acGrGrOut, "w");
   if (fdOut == NULL)
   {
      printf("Error creating GrGr output file: %s\n", acGrGrOut);
      return -1;
   }

   // Open Input file
   lHandle = _findfirst(acGrGrIn, &sFileInfo);
   if (lHandle > 0)
   {
      pTmp = strrchr(acGrGrIn, '\\');
      if (pTmp)
         *pTmp = 0;
      iRet = 0;
   } else
   {
      bGrGrAvail = false;
      LogMsg("*** No new GrGr file to process");
      fclose(fdOut);
      return 1;
   }

   // Create backup folder
   if (_access(acGrGrBak, 0))
      _mkdir(acGrGrBak);

   lCnt=lRecOut=0;
   while (!iRet)
   {
      // Create input name
      sprintf(acInFile, "%s\\%s", acGrGrIn, sFileInfo.name);

      // Update GrGr file date
      iRet = getFileDate(acInFile);
      if (iRet > lLastGrGrDate)
         lLastGrGrDate = iRet;

      LogMsg("Open input file %s", acInFile);
      if (!(fdIn = fopen(acInFile, "r")))
      {
         LogMsg("***** Error opening file %s", acInFile);
         break;
      }

      // Initialize new one
      memset(acOutRec, ' ', sizeof(SDX_GREC));
      lSalePrice = 0;
      iGrantors  = iGrantees = 0;
      acLastDoc[0] = 0;

      while (!feof(fdIn))
      {
         pTmp = fgets(acRec, 1024, fdIn);
         if (!pTmp)
            break;

         // Parse input string
         iRet = ParseString(acRec, cGrGrDelim, SDX_GR_DOCTAX+1, apTokens);
         if (iRet < SDX_GR_DOCTAX)
         {
            LogMsg("*** Bad input record: %s", acRec);
            continue;
         }

         // Is new doc?
         if (memcmp(apTokens[SDX_GR_DOCNUM], acLastDoc, strlen(apTokens[SDX_GR_DOCNUM])))
         {
            // Save last document
            if (pGrGrRec->Apn[0] >= '0')
            {
               pGrGrRec->CRLF[0] = 10;
               pGrGrRec->CRLF[1] = 0;
               fputs(acOutRec, fdOut);
               lRecOut++;
            }

            // Initialize new one
            memset(acOutRec, ' ', sizeof(SDX_GREC));            
            iGrantors = iGrantees = 0;
            lSalePrice = 0;

            // DocDate is in YYYYMMDD
            memcpy(pGrGrRec->DocDate, apTokens[SDX_GR_DOCDATE], strlen(apTokens[SDX_GR_DOCDATE]));
            strcpy(acLastDoc, apTokens[SDX_GR_DOCNUM]);
            iTmp = strlen(apTokens[SDX_GR_DOCNUM]);
            if (iTmp > GSIZ_RECNUMBER)
               memcpy(pGrGrRec->DocNum, apTokens[SDX_GR_DOCNUM]+4, iTmp-4);  // skip year
            else
               memcpy(pGrGrRec->DocNum, apTokens[SDX_GR_DOCNUM], iTmp);
            if (isdigit(*apTokens[SDX_GR_DOCTYPE]))
            {
               memcpy(pGrGrRec->InstType, apTokens[SDX_GR_DOCTYPE], strlen(apTokens[SDX_GR_DOCTYPE]));
               // Translate DocType
               iTmp = XrefCodeIndexEx((XREFTBL *)&asDeed[0], apTokens[SDX_GR_DOCTYPE], iNumDeeds);
               if (iTmp >= 0)
               {
                  pGrGrRec->NoneSale = asDeed[iTmp].acFlags[0];
                  pGrGrRec->NoneXfer = asDeed[iTmp].acOther[0];
                  iTmp = sprintf(acTmp, "%d", asDeed[iTmp].iIdxNum);
                  memcpy(pGrGrRec->DocType, acTmp, iTmp);
               } else if (acTmp[0] > ' ')
               {
                  LogMsg("*** SDX GRGR - Unknown Doc code: [%s] Record=%d", apTokens[SDX_GR_DOCTYPE], lCnt);
               }
            }            
         }

         // Tax Amt
         dTax = atof(apTokens[SDX_GR_DOCTAX]);
         lSalePrice = (long)(dTax*SALE_FACTOR);
         if (lSalePrice > 0)
         {
            sprintf(acTmp, "%*.2f", GSIZ_DOCTAX, dTax);
            memcpy(pGrGrRec->DocTax, acTmp, GSIZ_DOCTAX);
            sprintf(acTmp, "%*u", GSIZ_AMT, lSalePrice);
            memcpy(pGrGrRec->SalePrice, acTmp, GSIZ_AMT);
         }

         // What in Name?
         switch (*apTokens[SDX_GR_NAMETYPE])
         {
            case '4':      // APN
               pTmp = apTokens[SDX_GR_NAME];
               if (*pTmp == '-')
                  pTmp++;
               iTmp = strlen(pTmp);
               if (iTmp == GSIZ_APN)
                  memcpy(pGrGrRec->Apn, pTmp, iTmp);
               else
               {
                  LogMsg("*** Invalid APN length. Please verify: %s", apTokens[SDX_GR_NAME]);
                  memcpy(pGrGrRec->Apn, apTokens[SDX_GR_NAME], iTmp);
               }
               break;
            case '3':      // Grantor
               if (iGrantors < 2)
               {
                  iTmp = strlen(apTokens[SDX_GR_NAME]);
                  if (iTmp > GSIZ_GRANTOR) iTmp = GSIZ_GRANTOR;
                  memcpy(pGrGrRec->Grantor[iGrantors], apTokens[SDX_GR_NAME], iTmp);
               }
               iGrantors++;
               break;
            case '1':      // Grantee
               if (iGrantees < 2)
               {
                  iTmp = strlen(apTokens[SDX_GR_NAME]);
                  if (iTmp > GSIZ_GRANTOR) iTmp = GSIZ_GRANTOR;
                  memcpy(pGrGrRec->Grantee[iGrantees], apTokens[SDX_GR_NAME], iTmp);
               } else
                  pGrGrRec->MoreName = 'Y';
               iGrantees++;
               break;
            case '2':      // Legal Description
               replChar(apTokens[SDX_GR_NAME], '|', '1');
               vmemcpy(pGrGrRec->LegalDesc, apTokens[SDX_GR_NAME], GSIZ_LEGALDESC);
               break;
            case '5':      // Link to Document
               memcpy(pGrGrRec->DocRef, apTokens[SDX_GR_NAME], strlen(apTokens[SDX_GR_NAME]));
               break;
            case '6':      // secondary sequence number
               break;
            case '7':      // Book and Page numbers - put in the format BK##PG##
               break;
            case '8':      // Document number not used
               break;
            default:
               LogMsg("*** Unknown record type %s", apTokens[SDX_GR_NAMETYPE]);
               break;
         }

         lCnt++;
         if (!(lCnt % 1000))
            printf("\r%d", lCnt);
      }

      // Write out last record
      if (pGrGrRec->Apn[0] >= '0')
      {
         pGrGrRec->CRLF[0] = 10;
         pGrGrRec->CRLF[1] = 0;
         fputs(acOutRec, fdOut);
         lRecOut++;
      }

      LogMsg("Total records so far     : %d", lCnt);
      fclose(fdIn);

      // Move file
      sprintf(acTmp, "%s\\%s", acGrGrBak, sFileInfo.name);
      sprintf(acRec, "%s\\%s", acGrGrIn, sFileInfo.name);
      rename(acRec, acTmp);

      // Find next file
      iRet = _findnext(lHandle, &sFileInfo);
   }

   // Close handle
   _findclose(lHandle);

   // Close files
   if (fdOut)
      fclose(fdOut);

   // Sort output file
   LogMsg("Total processed records: %u", lCnt);
   LogMsg("         output records: %u\n", lRecOut);

   // Sort output file and dedup
   char acSrtFile[_MAX_PATH];
   sprintf(acSrtFile, acGrGrTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "Srt");
   sprintf(acRec, acGrGrTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "Sls");      // accummulated sale file
   if (!_access(acRec, 0))
      sprintf(acInFile, "%s+%s", acGrGrOut, acRec);
   else
      strcpy(acInFile, acGrGrOut);

   // Sort by APN, RecDate, DocNum
   LogMsg("Append to history file.");
   sprintf(acTmp, "S(1,%d,C,A,11,8,C,A,19,7,C,A,246,10,C,D) DUPO(B2048,1,256) ", iApnLen); 

   // Sort on APN asc, DocDate asc, DocNum asc
   lCnt = sortFile(acInFile, acSrtFile, acTmp);
   if (lCnt > 0)
   {
      sprintf(acTmp, acGrGrTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "Log");
      if (iRet = ChkBlankRow(acSrtFile, acTmp))
         LogMsg("***** ERROR merging GrGr file: %s.  Please see %s", acSrtFile, acTmp);
      else if (!_access(acRec, 0) && !iRet)
      {
         sprintf(acInFile, acGrGrTmpl, pCnty, pCnty, "sav");
         if (!_access(acInFile, 0))
            DeleteFile(acInFile);

         // Rename sls to sav
         LogMsg("Rename %s to %s", acRec, acInFile);
         rename(acRec, acInFile);
      }

      // Rename srt to SLS file
      LogMsg("Rename %s to %s", acSrtFile, acRec);
      iTmp = rename(acSrtFile, acRec);
      LogMsg("GRGR process completed: %d", lCnt);
      iRet = 0;
   } else
   {
      iRet = -1;
      LogMsg("***** Error sorting output file in Sdx_LoadGrGr()");
   }

   return iRet;
}

/********************************* MergeGrGrRec ******************************
 *
 * Merge GrGr data into current sale.  Move other sales accordingly.
 *
 *****************************************************************************

int Sdx_MergeGrGrRec(SDX_GREC *pSaleRec, char *pOutbuf)
{
   long  lCurSaleDt, lLstSaleDt;
   char  *pTmp, acTmp[32];

   lCurSaleDt = atoin(pSaleRec->DocDate, SIZ_SALE1_DT);
   lLstSaleDt = atoin(pOutbuf+OFF_SALE1_DT, SIZ_SALE1_DT);

   if (lCurSaleDt <= lLstSaleDt)
      return -1;

   // Move sale2 to sale3
   memcpy(pOutbuf+OFF_SALE3_DOC, pOutbuf+OFF_SALE2_DOC, SIZ_SALE3_DOC);
   memcpy(pOutbuf+OFF_SALE3_DT, pOutbuf+OFF_SALE2_DT, SIZ_SALE3_DT);
   memcpy(pOutbuf+OFF_SALE3_DOCTYPE, pOutbuf+OFF_SALE2_DOCTYPE, SIZ_SALE3_DOCTYPE);
   memcpy(pOutbuf+OFF_SALE3_AMT, pOutbuf+OFF_SALE2_AMT, SIZ_SALE3_AMT);
   memcpy(pOutbuf+OFF_SALE3_CODE, pOutbuf+OFF_SALE2_CODE, SIZ_SALE3_CODE);

   // Move sale1 to sale2
   memcpy(pOutbuf+OFF_SALE2_DOC, pOutbuf+OFF_SALE1_DOC, SIZ_SALE3_DOC);
   memcpy(pOutbuf+OFF_SALE2_DT, pOutbuf+OFF_SALE1_DT, SIZ_SALE3_DT);
   memcpy(pOutbuf+OFF_SALE2_DOCTYPE, pOutbuf+OFF_SALE1_DOCTYPE, SIZ_SALE3_DOCTYPE);
   memcpy(pOutbuf+OFF_SALE2_AMT, pOutbuf+OFF_SALE1_AMT, SIZ_SALE3_AMT);
   memcpy(pOutbuf+OFF_SALE2_CODE, pOutbuf+OFF_SALE1_CODE, SIZ_SALE3_CODE);

   // Update current sale
   memcpy(pOutbuf+OFF_SALE1_DOC, pSaleRec->DocNum, GSIZ_DOCNUM);
   memcpy(pOutbuf+OFF_SALE1_DT, pSaleRec->DocDate, SIZ_SALE1_DT);
   memcpy(pOutbuf+OFF_SALE1_AMT, pSaleRec->SalePrice, SIZ_SALE1_AMT);

   // Copy DocType if available
   if (isdigit(pSaleRec->DocType[0]))
      memcpy(pOutbuf+OFF_SALE1_DOCTYPE, pSaleRec->DocType, 3);
   else if (pSaleRec->DocType[0] > '0' && (pTmp=findDocType(pSaleRec->DocType, acTmp)))
      memcpy(pOutbuf+OFF_SALE1_DOCTYPE, pTmp, strlen(pTmp));

   // Remove sale code -
   memset(pOutbuf+OFF_SALE1_CODE, ' ', SALE_SIZ_SALECODE);

   // Remove seller
   memset(pOutbuf+OFF_SELLER, ' ', SIZ_SELLER);

   // Update transfers
   //memcpy(pOutbuf+OFF_TRANSFER_DOC, pSaleRec->DocNum, GSIZ_DOCNUM);
   //memcpy(pOutbuf+OFF_TRANSFER_DT, pSaleRec->DocDate, SIZ_TRANSFER_DT);

   return 1;
}

/********************************* MergeGrGrFile *****************************
 *
 * Initialize global variables
 *
 *****************************************************************************

int Sdx_MergeGrGrFile(char *pCnty)
{
   char     *pTmp, acBuf[MAX_RECSIZE];
   char     cFileCnt=1;
   char     acRawFile[_MAX_PATH], acOutFile[_MAX_PATH], acGrGrFile[_MAX_PATH];

   HANDLE   fhIn, fhOut;
   FILE     *fdSale;
   SDX_GREC SaleRec;

   int      iTmp, iSaleUpd=0;
   DWORD    nBytesRead;
   DWORD    nBytesWritten;
   BOOL     bRet, bEof;
   long     lRet=0, lCnt=0;

   memset(&SaleRec, ' ', sizeof(SDX_GREC));
   sprintf(acRawFile, acRawTmpl, pCnty, pCnty, "R01");
   sprintf(acOutFile, acRawTmpl, pCnty, pCnty, "N01");
   sprintf(acGrGrFile, acGrGrTmpl, pCnty, pCnty, "DAT");

   if (!iGrGrApnLen)
      iGrGrApnLen = myCounty.iApnLen;

   if (_access(acGrGrFile, 0))
      return 1;

   // Open Sale file
   LogMsg("Merge GrGr data %s to R01 file", acGrGrFile);
   fdSale = fopen(acGrGrFile, "r");
   if (fdSale == NULL)
   {
      LogMsg("***** Error opening sale file: %s\n", acGrGrFile);
      return 2;
   }

   // Get first record
   pTmp = fgets((char *)&SaleRec, sizeof(SDX_GREC), fdSale);
   if (!pTmp || feof(fdSale))
   {
      LogMsg("*** No GrGr data available");
      return 0;
   }

   // Open Input file
   LogMsg("Open input file %s", acRawFile);
   fhIn = CreateFile(acRawFile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhIn == INVALID_HANDLE_VALUE)
      return 3;

   // Open Output file
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
      return 4;

   // Copy skip record
   ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);
   bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);

   iNoMatch=0;
   bEof = false;

   // Merge loop
   while (!bEof)
   {
      bRet = ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);
      // Check for EOF
      if (!bRet)
      {
         LogMsg("Error reading input file %s (%f)", acRawFile, GetLastError());
         break;
      }

      if (!nBytesRead)
      {
         // EOF
         cFileCnt++;
         acRawFile[strlen(acRawFile)-1] = cFileCnt | 0x30;
         if (!_access(acRawFile, 0))
         {
            CloseHandle(fhIn);
            CloseHandle(fhOut);
            fhIn = 0;
            fhOut = 0;

            // Open next Input file
            LogMsg("Open input file %s", acRawFile);
            fhIn = CreateFile(acRawFile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
                  FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

            if (fhIn == INVALID_HANDLE_VALUE)
               break;
            bRet = ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);

            // Open Output file
            acOutFile[strlen(acRawFile)-1] = cFileCnt | 0x30;
            LogMsg("Open output file %s", acOutFile);
            fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
                  FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);
            if (fhOut == INVALID_HANDLE_VALUE)
               break;
         } else
            break;
      }

      // Update sale
GrGr_ReLoad:
      iTmp = memcmp(acBuf, SaleRec.Apn, iGrGrApnLen);
      if (!iTmp)
      {
         // Merge sale data
         iTmp = Sdx_MergeGrGrRec(&SaleRec, acBuf);
         if (iTmp > 0)
            iSaleUpd++;

         // Read next sale record
         pTmp = fgets((char *)&SaleRec, sizeof(SDX_GREC), fdSale);
         if (!pTmp)
            bEof = true;      // Signal to stop sale update
         else
            goto GrGr_ReLoad;
      } else
      {
         if (iTmp > 0)        // Sale not match, advance to next sale record
         {
            if (bDebug)
               LogMsg0("- Sale not match : %.12s > %.12s (%d) ", acBuf, SaleRec.Apn, lCnt);
            pTmp = fgets((char *)&SaleRec, sizeof(SDX_GREC), fdSale);
            if (!pTmp)
               bEof = true;    // Signal to stop sale update
            else
               goto GrGr_ReLoad;
         }
      }

      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);

      if (!bRet)
      {
         LogMsg("Error occurs: %d\n", GetLastError());
         lRet = WRITE_ERR;
         break;
      }
   }

   // Do the rest of the file
   while (nBytesRead>0 && ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL))
   {
      if (!nBytesRead)
         break;

      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
      lCnt++;
   }


   // Close files
   if (fdSale)
      fclose(fdSale);
   if (fhOut)
      CloseHandle(fhOut);
   if (fhIn)
      CloseHandle(fhIn);

   // Rename outfile
   LogMsg("Rename output file");
   sprintf(acBuf, acRawTmpl, pCnty, pCnty, "M01");
   if (!_access(acBuf, 0))
      remove(acBuf);
   sprintf(acRawFile, acRawTmpl, pCnty, pCnty, "R01");
   rename(acRawFile, acBuf);
   rename(acOutFile, acRawFile);

   LogMsg("Total output records:       %u", lCnt);
   LogMsg("Total unmatched records:    %u", iNoMatch);
   LogMsg("Total GrGr records updated: %u", iSaleUpd);

   return 0;
}

/******************************* Sdx_MergeGrec *******************************
 *
 * Merge new sale data into current sale.  Move other sales accordingly.
 * Only update seller if that is the sale from assessor.
 * This is the modified version of MergeSale() which will not update sale in 
 * the same date without SaleAmt.
 * 1/6/2015 Allow update XFER for new transaction.
 *
 *****************************************************************************/

int Sdx_MergeGrec(SDX_GREC *pSaleRec, char *pOutbuf)
{
   long  lCurSaleDt, lLstSaleDt, lPrice, lLastAmt;

   // Ignore non-xfer document
   if (pSaleRec->NoneXfer == 'Y')
      return 0;
   
   lCurSaleDt = atoin(pSaleRec->DocDate, SIZ_SALE1_DT);
   if (lCurSaleDt < lLienDate)
      return 0;

   // Update transfers
   lLstSaleDt = atoin(pOutbuf+OFF_TRANSFER_DT, SIZ_TRANSFER_DT);
   if (lCurSaleDt > lLstSaleDt)
   {
      memcpy(pOutbuf+OFF_TRANSFER_DOC, pSaleRec->DocNum, GSIZ_RECNUMBER);
      memcpy(pOutbuf+OFF_TRANSFER_DT, pSaleRec->DocDate, SIZ_TRANSFER_DT);
   }

   // Ignore non-sale document
   if (pSaleRec->NoneSale == 'Y')
      return 0;

   lLstSaleDt = atoin(pOutbuf+OFF_SALE1_DT, SIZ_SALE1_DT);
   if (lCurSaleDt < lLstSaleDt)
      return -1;

   lPrice = atoin(pSaleRec->SalePrice, SIZ_SALE1_AMT); 
   if (lCurSaleDt == lLstSaleDt)
   {
      lLastAmt = atoin(pOutbuf+OFF_SALE1_AMT, SIZ_SALE1_AMT);
      if (lLastAmt > 0 || (lPrice == 0))
         return 0;
   } else
   {
      // Move sale2 to sale3
      memcpy(pOutbuf+OFF_SALE3_DOC, pOutbuf+OFF_SALE2_DOC, SIZ_SALE3_DOC);
      memcpy(pOutbuf+OFF_SALE3_DT, pOutbuf+OFF_SALE2_DT, SIZ_SALE3_DT);
      memcpy(pOutbuf+OFF_SALE3_DOCTYPE, pOutbuf+OFF_SALE2_DOCTYPE, SIZ_SALE3_DOCTYPE);
      memcpy(pOutbuf+OFF_SALE3_AMT, pOutbuf+OFF_SALE2_AMT, SIZ_SALE3_AMT);
      memcpy(pOutbuf+OFF_SALE3_CODE, pOutbuf+OFF_SALE2_CODE, SIZ_SALE3_CODE);
      *(pOutbuf+OFF_AR_CODE3) = *(pOutbuf+OFF_AR_CODE2);

      // Move sale1 to sale2
      memcpy(pOutbuf+OFF_SALE2_DOC, pOutbuf+OFF_SALE1_DOC, SIZ_SALE3_DOC);
      memcpy(pOutbuf+OFF_SALE2_DT, pOutbuf+OFF_SALE1_DT, SIZ_SALE3_DT);
      memcpy(pOutbuf+OFF_SALE2_DOCTYPE, pOutbuf+OFF_SALE1_DOCTYPE, SIZ_SALE3_DOCTYPE);
      memcpy(pOutbuf+OFF_SALE2_AMT, pOutbuf+OFF_SALE1_AMT, SIZ_SALE3_AMT);
      memcpy(pOutbuf+OFF_SALE2_CODE, pOutbuf+OFF_SALE1_CODE, SIZ_SALE3_CODE);
      *(pOutbuf+OFF_AR_CODE2) = *(pOutbuf+OFF_AR_CODE1);
   }

   // Update current sale
   memcpy(pOutbuf+OFF_SALE1_DOC, pSaleRec->DocNum, GSIZ_RECNUMBER);
   memcpy(pOutbuf+OFF_SALE1_DT, pSaleRec->DocDate, SIZ_SALE3_DT);
   if (isdigit(pSaleRec->DocType[0]))
      memcpy(pOutbuf+OFF_SALE1_DOCTYPE, pSaleRec->DocType, 3);

   // Remove sale code - 
   memset(pOutbuf+OFF_SALE1_CODE, ' ', SALE_SIZ_SALECODE);

   if (lCurSaleDt > lLstSaleDt)
      memset(pOutbuf+OFF_SELLER, ' ', SIZ_SELLER);

   // Check for questionable price
   if (lPrice > 5000)
      memcpy(pOutbuf+OFF_SALE1_AMT, pSaleRec->SalePrice, SIZ_SALE1_AMT);
   else
      memset(pOutbuf+OFF_SALE1_AMT, ' ', SIZ_SALE1_AMT);

   *(pOutbuf+OFF_AR_CODE1) = 'R';

   return 1;
}

/****************************** Sdx_MergeGrGrExp *****************************
 *
 * Merge SDX_GREC output record from GrGr file SDX_GrGr.DAT into R01.
 *
 *****************************************************************************/

int Sdx_MergeGrGrExp(char *pCnty, char *pGrGrFile)
{
   char     *pTmp, acBuf[MAX_RECSIZE];
   char     acRawFile[_MAX_PATH], acOutFile[_MAX_PATH], acSaleRec[1024];

   HANDLE   fhIn, fhOut;
   SDX_GREC *pSaleRec=(SDX_GREC *)&acSaleRec[0];

   int      iTmp, iSaleUpd=0;
   DWORD    nBytesRead;
   DWORD    nBytesWritten;
   BOOL     bRet, bEof, bKeepS01=false;
   long     lRet=0, lCnt=0;

   LogMsg("Merge GrGr data using %s", pGrGrFile);
   memset(acSaleRec, ' ', sizeof(SDX_GREC));
   sprintf(acRawFile, acRawTmpl, pCnty, pCnty, "R01");
   if (_access(acRawFile, 0))
   {
      sprintf(acRawFile, acRawTmpl, pCnty, pCnty, "S01");
      bKeepS01 = true;
      if (_access(acRawFile, 0))
      {
         LogMsg("***** Error input file missing: %s\n", acRawFile);
         return -1;
      }
   }

   sprintf(acOutFile, acRawTmpl, pCnty, pCnty, "N01");

   if (!iGrGrApnLen)
      iGrGrApnLen = myCounty.iApnLen;

   if (_access(pGrGrFile, 0))
   {
      LogMsg("***** Error GrGr file missing: %s\n", pGrGrFile);
      return -1;
   }

   // Open Sale file
   LogMsg("Open GrGr data %s", pGrGrFile);
   fdSale = fopen(pGrGrFile, "r");
   if (fdSale == NULL)
   {
      LogMsg("***** Error opening GrGr file: %s\n", pGrGrFile);
      return -2;
   }

   // Get first record
   pTmp = fgets(acSaleRec, sizeof(SDX_GREC), fdSale);
   if (!pTmp || feof(fdSale))
   {
      LogMsg("*** No Sale data available");
      return 0;
   }

   // Open Input file
   LogMsg("Open input file %s", acRawFile);
   fhIn = CreateFile(acRawFile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhIn == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening input file: %s\n", acRawFile);
      return -3;
   }

   // Open Output file
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error creating output file: %s\n", acOutFile);
      return -4;
   }

   // Copy skip record
   ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);
   bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);

   iNoMatch=0;
   bEof = false;

   // Merge loop
   while (!bEof)
   {
      bRet = ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);
      // Check for EOF
      if (!bRet)
      {
         LogMsg("***** Error reading input file %s (%f)", acRawFile, GetLastError());
         break;
      }

      if (iRecLen != nBytesRead)
         break;   // EOF

      // Update sale
Sale_ReLoad:

#ifdef _DEBUG
      //if (!memcmp(acBuf, "1012720800", 10))
      //   iTmp = 0;
#endif

      iTmp = memcmp(acBuf, pSaleRec->Apn, iGrGrApnLen);
      if (!iTmp)
      {
         // Merge sale data
         iTmp = Sdx_MergeGrec(pSaleRec, acBuf);
         if (iTmp > 0)
            iSaleUpd++;

         // Read next sale record
         pTmp = fgets(acSaleRec, sizeof(SDX_GREC), fdSale);
         if (!pTmp)
            bEof = false;      // Signal to stop sale update
         else
            goto Sale_ReLoad;
      } else
      {
         if (iTmp > 0)         // Sale not match, advance to next sale record
         {
            iNoMatch++;
            if (bDebug)
               LogMsg0("==> Sale not match : %.*s > %.*s (%d) ", iGrGrApnLen, acBuf, iGrGrApnLen, pSaleRec->Apn, lCnt);
            pTmp = fgets(acSaleRec, sizeof(SDX_GREC), fdSale);
            if (!pTmp)
               bEof = true;    // Signal to stop sale update
            else
               goto Sale_ReLoad;
         }
      }

      // Get last recording date
      iTmp = atoin((char *)&acBuf[OFF_SALE1_DT], SIZ_SALE1_DT);
      // County sends daily records so it may include same day recording data.
      if (iTmp > lToday)
         LogMsg0("*** WARNING: Bad last recording date %d at record# %d -->%.10s", iTmp, lCnt, acBuf);
      else if (lLastRecDate < iTmp)
         lLastRecDate = iTmp;

      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);

      if (!bRet)
      {
         LogMsg("***** Error occurs: %d\n", GetLastError());
         lRet = WRITE_ERR;
         break;
      }
   }

   // Do the rest of the file
   while (nBytesRead>0 && ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL))
   {
      if (!nBytesRead)
         break;

      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
      lCnt++;
   }


   // Close files
   if (fdSale)
      fclose(fdSale);
   if (fhOut)
      CloseHandle(fhOut);
   if (fhIn)
      CloseHandle(fhIn);

   // Rename outfile
   LogMsg("Rename output file");
   sprintf(acBuf, acRawTmpl, pCnty, pCnty, "M01");
   if (!_access(acBuf, 0))
      remove(acBuf);
   sprintf(acRawFile, acRawTmpl, pCnty, pCnty, "R01");
   if (!bKeepS01)
   {
      LogMsg("Rename %s to %s", acRawFile, acBuf);
      rename(acRawFile, acBuf);
   }
   LogMsg("Rename %s to %s", acOutFile, acRawFile);
   rename(acOutFile, acRawFile);

   // Update output record count
   if (lCnt > 1000)
      lRecCnt = lCnt;

   LogMsg("Total output records:       %u", lCnt);
   LogMsg("Total GrGr records updated: %u", iSaleUpd);
   LogMsg("Total GrGr records skipped: %u", iNoMatch);

   return 0;
}

/******************************* Sdx_ExtractProp8 *****************************
 *
 *
 ******************************************************************************/

int Sdx_ExtractProp8(char *pProp8File)
{
   char     *pTmp, acTmp[256], acProp8Rec[MAX_RECSIZE];
   char     acOutFile[_MAX_PATH];

   FILE     *fdProp8, *fdExt;
   SDX_CHAR *pChar = (SDX_CHAR *)&acProp8Rec[0];

   int      iProp8Match=0;
   long     lCnt=0;

   if (lLienYear > 1900)
      sprintf(acTmp, "%d", lLienYear);
   else
      strcpy(acTmp, "yyyy");
   sprintf(acOutFile, acProp8Tmpl, myCounty.acCntyCode, myCounty.acCntyCode, acTmp, "P8");

   LogMsg0("Extract Prop8 APN from %s", pProp8File);

   // Open Prop8 file
   LogMsg("Open Prop8 file %s", pProp8File);
   fdProp8 = fopen(pProp8File, "r");
   if (fdProp8 == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", pProp8File);
      return -2;
   }

   // Open Output file
   LogMsg("Open output file %s", acOutFile);
   fdExt = fopen(acOutFile, "w");
   if (fdExt == NULL)
   {
      LogMsg("***** Error creating output file: %s\n", acOutFile);
      return -4;
   }

   // Output header
   fputs("APN\n", fdExt);

   // Get first lien rec
   pTmp = fgets((char *)&acProp8Rec[0], 1024, fdProp8);

   // Merge loop
   while (pTmp)
   {
      // Set prop8 flag 
      if (pChar->ValChgCode == 'W')
      {
         // Update flag
         sprintf(acTmp, "%.*s\n", RSIZ_APN, acProp8Rec);
         fputs(acTmp, fdExt);
         iProp8Match++;
      }

      // Read next prop8 record
      pTmp = fgets(acProp8Rec, 1024, fdProp8);

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdProp8)
      fclose(fdProp8);
   if (fdExt)
      fclose(fdExt);

   LogMsgD("Total records processed:      %u", lCnt);
   LogMsgD("Total records extracted:      %u", iProp8Match);
   return 0;
}

int Sdx_ExtrLdrProp8(char *pProp8File)
{
   char     *pTmp, acTmp[256], acProp8Rec[MAX_RECSIZE];
   int      lCnt=0;
   FILE     *fdExt;

   if (lLienYear > 1900)
      sprintf(acTmp, "%d", lLienYear);
   else
      strcpy(acTmp, "yyyy");
   sprintf(acProp8File, acProp8Tmpl, myCounty.acCntyCode, myCounty.acCntyCode, acTmp, "P8");

   LogMsg0("Extract Prop8 APN from %s", pProp8File);

   // Open Prop8 file
   LogMsg("Open Prop8 file %s", pProp8File);
   fdProp8 = fopen(pProp8File, "r");
   if (fdProp8 == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", pProp8File);
      return -2;
   }

   // Open Output file
   LogMsg("Open output file %s", acProp8File);
   fdExt = fopen(acProp8File, "w");
   if (fdExt == NULL)
   {
      LogMsg("***** Error creating output file: %s\n", acProp8File);
      return -4;
   }

   // Output header
   fputs("APN\n", fdExt);

   // Get first lien rec
   pTmp = fgets((char *)&acProp8Rec[0], 1024, fdProp8);

   // Merge loop
   while (pTmp)
   {
      // Set prop8 flag 
      sprintf(acTmp, "%.*s\n", RSIZ_APN, acProp8Rec);
      fputs(acTmp, fdExt);

      // Read next prop8 record
      pTmp = fgets(acProp8Rec, 1024, fdProp8);

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdProp8)
      fclose(fdProp8);
   if (fdExt)
      fclose(fdExt);

   LogMsgD("Total records processed:      %u", lCnt);
   return 0;
}

/********************************* Sdx_UpdateXfer *****************************
 *
 * Append transfers to existing file.
 *
 * Return 0 if successful, else error.
 *
 ******************************************************************************/

int Sdx_UpdateXfer(char *pCnty)
{
   int   iRet, lCnt=0, lOut=0;
   char  acXferFile[_MAX_PATH], acOutFile[_MAX_PATH];
   char  acRollRec[MAX_RECSIZE], acOutRec[512];
   char  *pTmp;
   FILE  *fdXfer;
   SDX_ROLL *pRec = (SDX_ROLL *)&acRollRec[0];

   LogMsg("Create xfer file for %s", pCnty);

   // Create transfer file
   sprintf(acXferFile, acXferTmpl, pCnty, pCnty, "txt");
   sprintf(acOutFile, acXferTmpl, pCnty, pCnty, "out");
   LogMsg("Open xfer file %s", acOutFile);
   fdXfer = fopen(acOutFile, "w");
   if (fdXfer == NULL)
   {
      LogMsg("***** Error opening Xfer file: %s\n", acOutFile);
      return -1;
   }
          
   // Open roll file      
   LogMsg("Open Roll file %s", acRollFile);
   fdRoll = fopen(acRollFile, "r");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", acRollFile);
      return -2;
   }

   char  sApn[16], sDocDate[16], sDocNum[16];
   memset(sApn, 0, 16);
   memset(sDocNum, 0, 16);

   while (!feof(fdRoll))
   {
      pTmp = fgets((char *)&acRollRec[0], MAX_RECSIZE, fdRoll);
      if (!pTmp)
         break;

      // Transfer - MMDDYY
      if (memcmp(pRec->DocDate, "000000", 6) > 0)
      {

         // Apply transfer
         pTmp = dateConversion(pRec->DocDate, sDocDate, MMDDYY2);
         if (pTmp)
         {
            if (pRec->DocNum[0] > ' ')
            {
               iRet = Sdx_FormatDoc(sDocNum, pRec->DocNum, sDocDate);
               memcpy(sApn, pRec->Apn, RSIZ_APN);
               blankPadz(sApn, SIZ_APN_S); 
               blankPadz(sDocNum, SIZ_TRANSFER_DOC); 
               sprintf(acOutRec, "%s|%s|%s|%s\n", sApn, sDocDate, sDocNum, acToday);
               fputs(acOutRec, fdXfer);
               lOut++;
            }
         }

      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   fclose(fdRoll);
   fclose(fdXfer);

   LogMsg("Total records processed:    %d", lCnt);
   LogMsg("              output:       %d", lOut);

   iRet = -99;
   if (lOut > 0)
   {
      // Sort output
      if (!_access(acXferFile, 0))
      {
         sprintf(acOutRec, acXferTmpl, pCnty, pCnty, "Srt");
         sprintf(acRollRec, "%s+%s", acXferFile, acOutFile); 
         iRet = sortFile(acRollRec, acOutRec, "S(#1,C,A,#2,C,A,#4,C,D,#3,C,D) DEL(124) OM(#2,C,LT,\"1700\",OR,#3,C,LT,\"0\") DUPO(#1,#2) ");
         if (iRet > 0)
         {                
            // Backup current file then rename output file to current
            sprintf(acOutFile, acXferTmpl, pCnty, pCnty, "bak");
            if (!_access(acOutFile, 0))
               DeleteFile(acOutFile);
            LogMsg("Save original xfer file to %s", acOutFile);
            rename(acXferFile, acOutFile);

            // Rename srt to SLS file
            iRet = rename(acOutRec, acXferFile);
         }
      } else
            iRet = rename(acOutFile, acXferFile);
   }

   return iRet;
}

/****************************************************************************
 * 
 * Convert old 281-bytes GREC to new 512-bytes GREC by appending extra spaces
 * to the end of the file.
 * Output Sdx_GrGr.New
 *
 ****************************************************************************/

void ConvertGRec(char *pCnty)
{
   FILE  *fdIn, *fdOut;
   char  acGrGrOut[_MAX_PATH], acGrGrIn[_MAX_PATH], acBuf[1024], *pTmp;
   SDX_GREC *pRec = (SDX_GREC *)&acBuf[0];
   int   iLen = sizeof(SDX_GREC);

   sprintf(acGrGrIn, acGrGrTmpl, pCnty, pCnty, "Sls");
   sprintf(acGrGrOut, acGrGrTmpl, pCnty, pCnty, "New");

   fdIn = fopen(acGrGrIn, "r");
   fdOut= fopen(acGrGrOut,"w");
   
   if (!fdIn)
   {
      LogMsg("***** Error opening input file: %s (%d)", acGrGrIn, _errno);
      return;
   }

   while (!feof(fdIn))
   {
      pTmp = fgets(acBuf, 1024, fdIn);
      if (!pTmp)
         break;
      blankPad_RemCRLF(acBuf, iLen);
      if (!memcmp(pRec->InstType, "001", 3))
         pRec->DocType[0] = '1';

      pRec->NoneSale = 'N';
      pRec->NoneXfer = 'N';
      pRec->CRLF[0] = 10;
      pRec->CRLF[1] = 0;
      fputs(acBuf, fdOut);
   }

   fclose(fdIn);
   fclose(fdOut);
}

/****************************** Sdx_MergeMapXRef *****************************
 *
 * Returns: 0 if updated, 1 if not found, -1 if eof
 *
 *****************************************************************************/

#define  SDX_MAPXREF_APN     0
#define  SDX_MAPXREF_LOW     12
#define  SDX_MAPXREF_HIGH    23

int Sdx_MergeMapXRef(FILE *fd, char *pOutbuf)
{
   static char    acRec[1024], *pRec=NULL;
   char           acTmp[32], acTmp1[32];
   int            iRet, iLoop;

   // Get first rec for first call
   if (!pRec)
      pRec = fgets(acRec, 1024, fd);

   do
   {
      // Compare Apn
      iLoop = memcmp(pOutbuf, pRec+SDX_MAPXREF_HIGH, iApnLen);
      if (iLoop > 0)
      {
         pRec = fgets(acRec, 1024, fd);
         if (!pRec)
            return -1;      // EOF
      }
   } while (iLoop > 0);

   // If smaller than low APN, return
   if (memcmp(pOutbuf, pRec+SDX_MAPXREF_LOW, iApnLen) < 0)
      return 1;

   // Update Alt-Apn
   memcpy(pOutbuf+OFF_ALT_APN, pRec+SDX_MAPXREF_APN, iApnLen);

   // Merge Maplink
   iRet = formatMapLink(pRec+SDX_MAPXREF_APN, acTmp, &myCounty);
   memcpy(pOutbuf+OFF_MAPLINK, acTmp, iRet);

   // Create index map link
   if (getIndexPage(acTmp, acTmp1, &myCounty))
      memcpy(pOutbuf+OFF_IMAPLINK, acTmp1, iRet);

   return 0;
}

/***************************** Sdx_UpdateMapXRef ******************************
 *
 *
 ******************************************************************************/

int Sdx_UpdateMapXRef(char *pCnty, char *pXRefFile, int iSkip)
{
   char     acBuf[MAX_RECSIZE], acRawFile[_MAX_PATH], acOutFile[_MAX_PATH];

   HANDLE   fhIn, fhOut;
   FILE     *fdMapXRef;

   int      iRet, iUpdated=0;
   DWORD    nBytesRead, nBytesWritten;
   BOOL     bRet;
   long     lCnt=0;

   LogMsgD("Update Map XRef on %s", pCnty);

   sprintf(acRawFile, acRawTmpl, pCnty, pCnty, "R01");
   sprintf(acOutFile, acRawTmpl, pCnty, pCnty, "X01");

   if (_access(acRawFile, 0))      // If R01 is not available
   {
      LogMsg("***** Error in Sdx_UpdateMapXRef(): Missing input file %s.  Please recheck!", acRawFile);
      return -1;
   }

   // Open XRef file
   if (pXRefFile && *pXRefFile > ' ')
   {
      LogMsg("Open MapXRef file %s", pXRefFile);
      fdMapXRef = fopen(pXRefFile, "r");
      if (fdMapXRef == NULL)
      {
         LogMsg("***** Error opening MapXRef file: %s\n", pXRefFile);
         return -2;
      }
   } else
      return -1;

   // Open Input file
   LogMsg("Open input file %s", acRawFile);
   fhIn = CreateFile(acRawFile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhIn == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening input file: %s\n", acRawFile);
      return -3;
   }

   // Open Output file
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening output file: %s\n", acOutFile);
      return -4;
   }

   // Copy skip record
   memset(acBuf, ' ', iRecLen);
   while (iSkip-- > 0)
   {
      ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);
      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
   }

   // Merge loop
   while (true)
   {
      bRet = ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);

      // Check for EOF
      if (!bRet)
      {
         LogMsg("***** Error reading input file %s (%f)", acRawFile, GetLastError());
         break;
      }

      // EOF ?
      if (iRecLen != nBytesRead)
         break;

      iRet = 1;
      if (fdMapXRef)
      {
         iRet = Sdx_MergeMapXRef(fdMapXRef, acBuf);
         if (iRet == -1)
         {
            fclose(fdMapXRef);
            fdMapXRef = (FILE *)NULL;
         } else if (!iRet)
            iUpdated++;
      }

      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);

      if (!bRet)
      {
         LogMsg("***** Error occurs: %d\n", GetLastError());
         break;
      }
   }

   // Close files
   if (fdMapXRef)
      fclose(fdMapXRef);
   if (fhOut)
      CloseHandle(fhOut);
   if (fhIn)
      CloseHandle(fhIn);

   sprintf(acBuf, acRawTmpl, pCnty, pCnty, "Tmp");
   if (!_access(acBuf, 0))
      remove(acBuf);
   rename(acRawFile, acBuf);
   rename(acOutFile, acRawFile);

   LogMsgD("Total output records:      %u", lCnt);
   LogMsg("Total records updated:     %u", iUpdated);

   return 0;
}

/************************** Sdx_CreateTaxDetailOnTRA *************************
 *
 * Look into SQL table for all fund related to specific TRA, then calculate
 * tax based on NetValue and create detail records.
 *
 *****************************************************************************/

int Sdx_CreateTaxDetailOnTRA_Sql(TAXDETAIL *pDetailRec, char *pTRA, int iNetTaxable)
{
   TAXAGENCY   asAgency[100], sAgency, *pResult;
   TAXDETAIL   sDetailRec;

   int         iCnt, iIdx;
   double      dTmp, dRate;
   char        acTmp[1024];

   // Look for list of fund
   iCnt = Sdx_GetTaxInfo(asAgency, pTRA);
   for (iIdx = 0; iIdx < iCnt; iIdx++)
   {
      dRate = atof(asAgency[iIdx].TaxRate);
      dTmp = (dRate * iNetTaxable) / 100.0;
      if (dTmp > 0.0)
      {
         pResult = findTaxAgency(asAgency[iIdx].Code);
         if (!pResult)
            LogMsg("New AV Code: %s|%s", asAgency[iIdx].Code, asAgency[iIdx].Agency);

         memcpy(&sDetailRec, (char *)pDetailRec, sizeof(TAXDETAIL));
         sprintf(sDetailRec.TaxRate, "%.5f", dRate);
         sprintf(sDetailRec.TaxAmt, "%.2f", dTmp);
         strcpy(sDetailRec.TaxCode, asAgency[iIdx].Code);
         Tax_CreateDetailCsv(acTmp, &sDetailRec);
         fputs(acTmp, fdDetail);

         memset((char *)&sAgency, 0, sizeof(TAXAGENCY));
         strcpy(sAgency.Code, sDetailRec.TaxCode);
         strcpy(sAgency.Agency,  asAgency[iIdx].Agency);
         strcpy(sAgency.TaxRate, sDetailRec.TaxRate);
         Tax_CreateAgencyCsv(acTmp, &sAgency);
         fputs(acTmp, fdAgency);
      }
   } 

   if (!iCnt)
      LogMsg("***** Sdx_CreateTaxDetailOnTRA_Sql->TRA not found: %s [%s]", pTRA, pDetailRec->Apn);

   return 0;
}

int Sdx_CreateTaxDetailOnTRA_Csv(TAXDETAIL *pDetailRec, char *pTRA, int iNetTaxable)
{
   TAXAGENCY   sAgency, *pResult;
   TAXDETAIL   sDetailRec;
   TRADIST     *pTRADist;

   int         iIdx, iRet = 0;
   double      dRate, dAmt;
   char        acTmp[1024];

   // Look for list of fund
   pTRADist = findTRADist(pTRA);
   if (pTRADist)
   {
      for (iIdx = 0; iIdx < pTRADist->iCount; iIdx++)
      {
         memset((char *)&sAgency, 0, sizeof(TAXAGENCY));
         pResult = findTaxAgency(pTRADist->Code[iIdx]);
         if (!pResult)
         {
            LogMsg0("*** New AV Code: %s (TRA=%s)", pTRADist->Code[iIdx], pTRA);
            sprintf(sAgency.Agency,  "Special Assessment %s", pTRADist->Code[iIdx]);
            sAgency.TC_Flag[0] = '1';
         } else
         {
            dRate = atof(pResult->TaxRate);
            dAmt = (dRate * iNetTaxable) / 100.0;
            strcpy(sAgency.Agency,  pResult->Agency);
            sAgency.TC_Flag[0] = pResult->TC_Flag[0];
            if (pResult->Phone[0] > ' ')
               strcpy(sAgency.Phone,  pResult->Phone);
         }

         memcpy(&sDetailRec, (char *)pDetailRec, sizeof(TAXDETAIL));
         sprintf(sDetailRec.TaxRate, "%.5f", dRate);
         sprintf(sDetailRec.TaxAmt, "%.2f", dAmt);
         strcpy(sDetailRec.TaxCode, pTRADist->Code[iIdx]);
         Tax_CreateDetailCsv(acTmp, &sDetailRec);
         fputs(acTmp, fdDetail);

         strcpy(sAgency.Code, sDetailRec.TaxCode);
         strcpy(sAgency.TaxRate, sDetailRec.TaxRate);
         Tax_CreateAgencyCsv(acTmp, &sAgency);
         fputs(acTmp, fdAgency);
      } 
   } else
   {
      LogMsg("*** Sdx_CreateTaxDetailOnTRA_Csv->TRA not found: %s [%s]", pTRA, pDetailRec->Apn);
      iRet = 1;
   }
   return iRet;
}

/***************************** Sdx_ParseTaxBase ******************************
 *
 * Parse roll record to Base, Delq, Owner
 *
 *****************************************************************************/

int Sdx_ParseTaxBase(char *pBaseBuf, char *pDelqBuf, char *pInbuf)
{
   double   dRate, dTax1, dTax2, dPen1, dPen2, dTotalFee, dTotalTax, dTotalDue;
   int      iTmp, iRollYear, iNetTaxable;

   TAXBASE    *pBaseRec = (TAXBASE *)pBaseBuf;
   TAXDELQ    *pDelqRec = (TAXDELQ *)pDelqBuf;
   TAXDETAIL  sDetailRec;
   SDX_SECTAX *pInRec   = (SDX_SECTAX *)pInbuf;

   // Clear output buffer
   memset(pBaseBuf, 0, sizeof(TAXBASE));
   memset(pDelqBuf, 0, sizeof(TAXDELQ));
   memset(&sDetailRec, 0, sizeof(TAXDETAIL));

   // APN
   memcpy(pBaseRec->Apn, pInRec->APN, iApnLen);
   memcpy(pDelqRec->Apn, pInRec->APN, iApnLen);
   memcpy(pBaseRec->OwnerInfo.Apn, pInRec->APN, iApnLen);
   memcpy(sDetailRec.Apn, pInRec->APN, iApnLen);

   // TRA
   sprintf(pBaseRec->TRA, "0%.5s", pInRec->TRA);

   // BillNum
   memcpy(pBaseRec->BillNum, pInRec->APN, iApnLen);
   memcpy(sDetailRec.BillNum, pInRec->APN, iApnLen);

   // Tax Year
   iRollYear = atoin(pInRec->RollYear, TSIZ_ROLLYEAR);
   memcpy(pBaseRec->TaxYear, pInRec->RollYear, TSIZ_ROLLYEAR);
   memcpy(pBaseRec->OwnerInfo.TaxYear, pInRec->RollYear, TSIZ_ROLLYEAR);
   memcpy(sDetailRec.TaxYear, pInRec->RollYear, TSIZ_ROLLYEAR);

   // Tax Amount
   dTax1 = atofn(pInRec->Inst1Amt, TSIZ_INST1AMT);
   dTax2 = atofn(pInRec->Inst2Amt, TSIZ_INST1AMT);

   // Tax Rate
   dRate = atofn(pInRec->TaxRate, TSIZ_TAXRATE);
   if (dRate > 0.0)
      sprintf(pBaseRec->TotalRate, "%.6f", dRate);

#ifdef _DEBUG
   //if (!memcmp(pInRec->APN, "1012905100", 10))
   //   iTmp = 0;
#endif

   // Penalty
   dPen1 = atofn(pInRec->Inst1Penalty, TSIZ_INST1PENALTY);
   if (dPen1 > 0.0)
      sprintf(pBaseRec->PenAmt1, "%.2f", dPen1);
   dPen2 = atofn(pInRec->Inst2Penalty, TSIZ_INST1PENALTY);
   if (dPen2 > 0.0)
      sprintf(pBaseRec->PenAmt2, "%.2f", dPen2);

   // Delinquent cost
   dTotalFee = atofn(pInRec->Delq_Cost, TSIZ_DELINQUENTCOST);
   if (dTotalFee > 0.0)
      sprintf(pBaseRec->TotalFees, "%.2f", dTotalFee);

   // Due Date
   sprintf(pBaseRec->DueDate1, "%.8s", pInRec->Inst1DueDate);
   sprintf(pBaseRec->DueDate2, "%.8s", pInRec->Inst2DueDate);

   // Paid 
   dTotalDue = 0;
   if (!memcmp(pInRec->Inst1Status, "PAID", 4))
   {
      pBaseRec->Inst1Status[0] = TAX_BSTAT_PAID;

      sprintf(pBaseRec->PaidAmt1, "%.2f", dTax1);
      iTmp = atoin(pInRec->Inst1PaidDate, 2);
      if (iTmp > 6)
         sprintf(pBaseRec->PaidDate1, "%d%.2s%.2s", iRollYear, pInRec->Inst1PaidDate, &pInRec->Inst1PaidDate[3]);
      else if (iTmp > 0)
      {
         if (dPen1 > 0.0 && lToyear == (iRollYear+1))
            sprintf(pBaseRec->PaidDate1, "%d%.2s%.2s", lToyear, pInRec->Inst1PaidDate, &pInRec->Inst1PaidDate[3]);
         else 
            sprintf(pBaseRec->PaidDate1, "%d%.2s%.2s", iRollYear, pInRec->Inst1PaidDate, &pInRec->Inst1PaidDate[3]);
      }
   } else if (pInRec->Inst1Status[0] == 'D' || pInRec->Inst1Status[0] == 'R')
   {  // DUE or RETURN BY POST OFFICE considered unpaid
      pBaseRec->Inst1Status[0] = TAX_BSTAT_UNPAID;
      dTotalDue = dTax1;
   } else if (pInRec->Inst1Status[0] == 'N' || pInRec->Inst1Status[0] == ' ')
   {
      pBaseRec->Inst1Status[0] = TAX_BSTAT_NOTAX;
      dTax1 = 0.0;
   } else
      LogMsg("??? Unknown installment #1 status: %.20s [%.10s]", pInRec->Inst1Status, pInRec->APN);

   if (!memcmp(pInRec->Inst2Status, "PAID", 4))
   {
      pBaseRec->Inst2Status[0] = TAX_BSTAT_PAID;

      sprintf(pBaseRec->PaidAmt2, "%.2f", dTax2);
      iTmp = atoin(pInRec->Inst2PaidDate, 2);
      if (iTmp > 6)
         sprintf(pBaseRec->PaidDate2, "%d%.2s%.2s", iRollYear, pInRec->Inst2PaidDate, &pInRec->Inst2PaidDate[3]);
      else if (iTmp > 0)
      {
         if (lToyear == (iRollYear+1))
            sprintf(pBaseRec->PaidDate2, "%d%.2s%.2s", lToyear, pInRec->Inst2PaidDate, &pInRec->Inst2PaidDate[3]);
         else
            sprintf(pBaseRec->PaidDate2, "%d%.2s%.2s", iRollYear, pInRec->Inst2PaidDate, &pInRec->Inst2PaidDate[3]);
      }
   } else if (pInRec->Inst2Status[0] == 'D' || pInRec->Inst2Status[0] == 'R')
   {
      pBaseRec->Inst2Status[0] = TAX_BSTAT_UNPAID;
      dTotalDue += dTax2;
   } else if (pInRec->Inst2Status[0] == 'N' || pInRec->Inst2Status[0] == ' ')
   {
      pBaseRec->Inst2Status[0] = TAX_BSTAT_NOTAX;
      dTax2 = 0.0;
   } else
      LogMsg("??? Unknown installment #2 status: %.20s [%.10s]", pInRec->Inst2Status, pInRec->APN);

   // Check for Tax amount - V99
   dTotalTax = atofn(pInRec->TotalTaxAmt, TSIZ_TOTALTAXAMT);
   if (dTotalTax > 0.0)
   {
      sprintf(pBaseRec->TotalTaxAmt, "%.2f", dTotalTax);
      
      double dOrgInstAmt = atofn(pInRec->OrgInstAmt, TSIZ_INST1AMT);
      sprintf(pBaseRec->TaxAmt1, "%.2f", dOrgInstAmt);
      sprintf(pBaseRec->TaxAmt2, "%.2f", dOrgInstAmt);
   }

   // Total due
   if (dTotalDue > 0.0)
      sprintf(pBaseRec->TotalDue, "%.2f", dTotalDue);

   pBaseRec->isSecd[0] = '1';
   pBaseRec->BillType[0] = BILLTYPE_SECURED;

   // Generate csv line for Detail and write to file.  This section
   // generate detail based on TRA in addition to detail from special_assessments_full_wits.dat
   double dBaseAmt = atofn(pInRec->BaseTaxAmt, TSIZ_BASETAXAMT);
   if (dBaseAmt > 1.0)
   {
      iNetTaxable = atoin(pInRec->NetTaxableValue, TSIZ_NETTAXABLEVALUE);
      if (pBaseRec->TRA[0] > ' ' && iNetTaxable > 0)
      {
         // Create detail record based on TRA
         //iTmp = Sdx_CreateTaxDetailOnTRA_Sql(&sDetailRec, &pBaseRec->TRA[0], iNetTaxable);
         iTmp = Sdx_CreateTaxDetailOnTRA_Csv(&sDetailRec, &pBaseRec->TRA[0], iNetTaxable);
         if (iTmp)
            lBadTRA++;
      }
   }

   /* Not use
   // Owner info
   char *pTmp, *pTmp1, acTmp[256];
   memcpy(acTmp, pInRec->OwnerName, TSIZ_OWNERNAME);
   blankRem(acTmp, TSIZ_OWNERNAME);
   strcpy(pBaseRec->OwnerInfo.Name1, acTmp);

   // Mailing addr
   iTmp = TSIZ_MAILINGADDRESS;
   if (!memcmp(pInRec->M_Addr, "0 ", 2))
   {
      iTmp -= 2;
      memcpy(acTmp, &pInRec->M_Addr[2], iTmp);
   } else
      memcpy(acTmp, pInRec->M_Addr, iTmp);
   blankRem(acTmp, iTmp);
   pTmp = acTmp;
   iTmp = 0;
   if (!memcmp(acTmp, "P O", 3) || !memcmp(acTmp, "P.O", 3) || !memcmp(acTmp, "PO BOX", 6) || 
      !memcmp(acTmp, "APT ", 4) || !memcmp(acTmp, "UNIT ", 5))
      iTmp = 0;
   else if (!memcmp(acTmp, "C/O", 2))
   {
      if ((pTmp1 = strstr(acTmp, "P O")) || (pTmp1 = strstr(acTmp, "P.O")))
         pTmp = pTmp1;
      else if ((pTmp1 = strstr(acTmp, " ONE ")))
      {
         pTmp = pTmp1+1;
      } else
      {
         pTmp += 5;
         while (*pTmp && !isdigit(*pTmp)) pTmp++;
      }
      *(pTmp-1) = 0;
      if (!memcmp(acTmp, "C/O", 2))
      {
         acTmp[50] = 0;
         strcpy(pBaseRec->OwnerInfo.CareOf, acTmp);
      } else
      {
         strcpy(pBaseRec->OwnerInfo.MailAdr[0], pTmp);
         iTmp++;
      }
   }

   if (strlen(pTmp) > SIZ_M_ADDR_D)
   {
      // Move last two words to next line
      char *pTmp2;

      if ((pTmp1 = strchr(pTmp, ',')))
      {
         *pTmp1++ = 0;
         strcpy(pBaseRec->OwnerInfo.MailAdr[iTmp++], pTmp);
         pTmp = pTmp1;
         if (*pTmp == ' ') pTmp++;
         if ((pTmp1 = strchr(pTmp, ',')))
         {
            *pTmp1++ = 0;
            strcpy(pBaseRec->OwnerInfo.MailAdr[iTmp++], pTmp);
            pTmp = pTmp1;
            if (*pTmp == ' ') pTmp++;
         } else
            pTmp1 = pTmp;
      } else if ((pTmp1 = strstr(pTmp+10, " P O")) || (pTmp1 = strstr(pTmp+10, " BRITISH")))
      {
         *pTmp1++ = 0;
         strcpy(pBaseRec->OwnerInfo.MailAdr[iTmp++], pTmp);
      } else if ((pTmp1 = strstr(pTmp, "ROAD ")) || (pTmp1 = strstr(pTmp, "BLVD ")) || 
         (pTmp1 = strstr(pTmp, " HWY ")) || (pTmp1 = strstr(pTmp, " WAY ")))
      {
         pTmp1 += 4;
         *pTmp1++ = 0;
         strcpy(pBaseRec->OwnerInfo.MailAdr[iTmp++], pTmp);
      } else if ((pTmp1 = strstr(pTmp, " LN ")) || (pTmp1 = strstr(pTmp, " DR ")) || 
         (pTmp1 = strstr(pTmp, " ST ")) || (pTmp1 = strstr(pTmp, " CT "))  || (pTmp1 = strstr(pTmp, " RD ")))
      {
         pTmp1 += 3;
         *pTmp1++ = 0;
         strcpy(pBaseRec->OwnerInfo.MailAdr[iTmp++], pTmp);
      } else if ((pTmp1 = strstr(pTmp, " DRIVE ")))
      {
         pTmp1 += 6;
         *pTmp1++ = 0;
         strcpy(pBaseRec->OwnerInfo.MailAdr[iTmp++], pTmp);
      } else if ((pTmp1 = strstr(pTmp, " CIRCLE ")))
      {
         pTmp1 += 7;
         *pTmp1++ = 0;
         strcpy(pBaseRec->OwnerInfo.MailAdr[iTmp++], pTmp);
      } else if ((pTmp1 = strstr(pTmp, " CHUNGNAM ")) || (pTmp1 = strstr(pTmp, " TOKYO ")) || (pTmp1 = strstr(pTmp, " BEIJING ")))
      {
         *pTmp1++ = 0;
         strcpy(pBaseRec->OwnerInfo.MailAdr[iTmp++], pTmp);
      } else if ((pTmp1 = strstr(pTmp, " UNITED ")) || (pTmp1 = strstr(pTmp, " NEW ")) || (pTmp1 = strstr(pTmp, " CANADA ")) ||
         (pTmp1 = strstr(pTmp, " LUXEMBOURG ")) || (pTmp1 = strstr(pTmp, " RANCHO ")) )
      {
         *pTmp1++ = 0;
         strcpy(pBaseRec->OwnerInfo.MailAdr[iTmp++], pTmp);
      } else
      {
         pTmp2 = strrchr(pTmp, ' ');
         *pTmp2 = 0;
         pTmp1 = strrchr(pTmp, ' ');
         *pTmp1++ = 0;
         *pTmp2 = ' ';
         strcpy(pBaseRec->OwnerInfo.MailAdr[iTmp++], pTmp);
      }
      pTmp = pTmp1;
      pBaseRec->OwnerInfo.MailAdr[iTmp-1][SIZ_M_ADDR_D] = 0;
      *(pTmp+SIZ_M_ADDR_D) = 0;
   }

   strcpy(pBaseRec->OwnerInfo.MailAdr[iTmp], pTmp);

   int iZip = atoin(pInRec->M_Zip, 5);
   if (iZip > 0 )
      memcpy(pBaseRec->OwnerInfo.MailAdr[iTmp+1], pInRec->M_Zip, TSIZ_MAILINGZIP);
   */

   return 0;
}

/******************************** Sdx_ParseTaxDelq ***************************
 *
 * 10/07/2019 Set TaxYear = Def_Date.  If Def_Date not available, set it to current lien year.
 *
 *****************************************************************************/

int Sdx_ParseTaxDelq(char *pOutbuf, FILE *fd)
{
   static   char acRec[1024], *pRec=NULL;
   int      iLoop;
   double   dTaxAmt, dTmp;

   SDX_DEFTAX *pTax;
   TAXDELQ    *pDelq = (TAXDELQ *)pOutbuf;

   // Get first Char rec for first call
   if (!pRec)
      pRec = fgets(acRec, 1024, fd);

   pTax = (SDX_DEFTAX *)pRec;

   do
   {
      if (!pRec)
      {
         fclose(fd);
         fd = NULL;
         return 1;      // EOF
      }

      // Compare Apn
      iLoop = memcmp(pDelq->Apn, pTax->Apn, iApnLen);
      if (iLoop > 0)
      {
         if (bDebug)
            LogMsg0("Skip rec  %.10s", pTax->Apn);
         pRec = fgets(acRec, 1024, fd);
      }
   } while (iLoop > 0);

#ifdef _DEBUG
   //if (!memcmp(pTax->Apn, "1012905100", 10))
   //   dTmp = 0;
#endif

   // If not match, return
   if (iLoop)
      return 1;

   // Updated date
   strcpy(pDelq->Upd_Date, acToday);

   // Default amount
   dTaxAmt = atof(pTax->DefAmt);
   if (dTaxAmt > 0.0)
      sprintf(pDelq->Def_Amt, "%.2f", dTaxAmt);

   // CANCELLED, PAID, DUE, DUE - POWER TO SALE
   if (pTax->BillStatus[0] == 'D')
      pDelq->isDelq[0] = '1';

   // Redemption status
   if (pTax->RedStatus[0] == 'R')
   {
      memcpy(pDelq->Red_Date, pTax->RedDate, TDSIZ_REDDATE);
      dTmp = atof(pTax->TotalAmtPaidReg);
      if (dTmp > 0.0)
         sprintf(pDelq->Red_Amt, "%.2f", dTmp);
      pDelq->DelqStatus[0] = TAX_STAT_REDEEMED;
   } else if (pTax->RedStatus[0] == 'S')
   {
      dTmp = atofn(pTax->BalanceDue, TDSIZ_BALANCEDUE);
      if (dTmp > 0.0)
         sprintf(pDelq->Red_Amt, "%.2f", dTmp);
      pDelq->DelqStatus[0] = TAX_STAT_SOLD2AUCTION;
   } else if (pTax->RedStatus[0] == 'P')
   {
      dTmp = atofn(pTax->BalanceDue5Yr, TDSIZ_BALANCEDUE);
      if (dTmp > 0.0)
         sprintf(pDelq->Red_Amt, "%.2f", dTmp);
      pDelq->DelqStatus[0] = TAX_STAT_ONPAYMENT;
   } else if (pTax->PowerToSaleStatus[0] == '3')
   {
      dTmp = atofn(pTax->BalanceDue, TDSIZ_BALANCEDUE);
      if (dTmp > 0.0)
         sprintf(pDelq->Red_Amt, "%.2f", dTmp);
      pDelq->DelqStatus[0] = TAX_STAT_POWER2SELL;       // Power to sell
   } else if (pTax->BillStatus[0] == 'C')
      pDelq->DelqStatus[0] = TAX_STAT_CANCEL;

   // Default date
   memcpy(pDelq->Def_Date, pTax->DefDate, TDSIZ_DEFDATE);

   // Delq Year
   if (pTax->DefDate[0] > '0')
      memcpy(pDelq->TaxYear, pTax->DefDate, TDSIZ_DEFDATE);
   else
      sprintf(pDelq->TaxYear, "%d", lLienYear);

   // Pen Amt
   dTmp = atofn(pTax->RedPenalty, TDSIZ_REDPENALTY);
   if (dTmp > 0.0)
      sprintf(pDelq->Pen_Amt, "%.2f", dTmp);

   // Fee Amt
   dTmp = atofn(pTax->RedFee, TDSIZ_REDFEE);
   dTmp += atofn(pTax->ReturnedCheckFees, TDSIZ_REDFEE);
   dTmp += atofn(pTax->CostRecoveryFee, TDSIZ_REDFEE);
   dTmp += atofn(pTax->TaxSalesFee, TDSIZ_REDFEE);
   if (dTmp > 0.0)
      sprintf(pDelq->Fee_Amt, "%.2f", dTmp);

   // Due date
   if (pTax->NextinstDueDate[0] > '0')
   {
      memcpy(pDelq->Pts_Date, pTax->NextinstDueDate, TDSIZ_NEXTINSTDUEDATE);
      // Inst due Amt
      dTmp = atofn(pTax->NextinstAmt, TDSIZ_NEXTINSTAMT);
      if (dTmp > 0.0)
         sprintf(pDelq->Tax_Amt, "%.2f", dTmp);
   }

   // Get next rec
   pRec = fgets(acRec, 1024, fd);

   return 0;
}

/******************************* Sdx_Load_TaxBase ****************************
 *
 * Create base import file and import into SQL if specified
 *
 * Return 0 if success.
 *
 *****************************************************************************/

int Sdx_Load_TaxBase(bool bImport)
{
   char     *pTmp, acBaseBuf[1024], acDelqBuf[1024], acRec[MAX_RECSIZE], acAgencyFile[_MAX_PATH];
   char     acBaseFile[_MAX_PATH], acDelqFile[_MAX_PATH], acInFile[_MAX_PATH],
            acDefaultFile[_MAX_PATH], acTCPath[_MAX_PATH], acZipFile[_MAX_PATH], acItemsFile[_MAX_PATH];

   int      iRet;
   long     lOut=0, lCnt=0;
   FILE     *fdBase, *fdDelq, *fdIn, *fdDeft; //, *fdOwner;
   TAXBASE  *pTaxBase = (TAXBASE *)&acBaseBuf[0];
   TAXDELQ  *pTaxDelq = (TAXDELQ *)&acDelqBuf[0];

   LogMsg0("Loading tax file");

   GetIniString(myCounty.acCntyCode, "TaxRollInfo", "", acInFile, _MAX_PATH, acIniFile);
   GetIniString(myCounty.acCntyCode, "SecDefault", "", acDefaultFile, _MAX_PATH, acIniFile);

   // Unzip file if needed
   strcpy(acTCPath, acInFile);
   pTmp = strrchr(acTCPath, '\\');
   *pTmp = 0;
   iRet = GetIniString(myCounty.acCntyCode, "TaxFile", "", acZipFile, _MAX_PATH, acIniFile);
   if (iRet > 20 && !_access(acZipFile, 0))
   {
      iRet = unzipAll(acZipFile, acTCPath);
      if (!iRet)
      {
         sprintf(acTCPath, "%s.%s", acZipFile, acToday);
         rename(acZipFile, acTCPath);
      }
   }

   NameTaxCsvFile(acBaseFile, myCounty.acCntyCode, "Base");
   NameTaxCsvFile(acItemsFile, myCounty.acCntyCode, "Items");
   NameTaxCsvFile(acAgencyFile, myCounty.acCntyCode, "Agency");
   NameTaxCsvFile(acDelqFile, myCounty.acCntyCode, "Delq");
   pTmp = strrchr(acBaseFile, '\\');
   *pTmp = 0;
   if (_access(acBaseFile, 0))
      _mkdir(acBaseFile);
   *pTmp = '\\';

   // Save file date
   lLastTaxFileDate = getFileDate(acInFile);
   // Only process if new tax file
   iRet = isNewTaxFile(NULL, myCounty.acCntyCode);
   if (iRet <= 0)
   {
      lLastTaxFileDate = 0;
      return iRet;
   }

   // Open input file
   LogMsg("Open current tax file %s", acInFile);
   fdIn = fopen(acInFile, "r");
   if (fdIn == NULL)
   {
      LogMsg("***** Error opening Current tax file: %s\n", acInFile);
      return -2;
   }  
   // Drop header record
   pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdIn);

   LogMsg("Open default tax file %s", acDefaultFile);
   fdDeft = fopen(acDefaultFile, "r");
   if (fdDeft == NULL)
   {
      LogMsg("***** Error opening default tax file: %s\n", acDefaultFile);
      return -2;
   }  
   // Drop header record
   pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdDeft);

   // Open Delq file
   LogMsg("Open Delq file %s", acDelqFile);
   fdDelq = fopen(acDelqFile, "w");
   if (fdDelq == NULL)
   {
      LogMsg("***** Error creating Delq output file: %s\n", acDelqFile);
      return -4;
   }

   // Open base file
   LogMsg("Open Base file %s", acBaseFile);
   fdBase = fopen(acBaseFile, "w");
   if (fdBase == NULL)
   {
      LogMsg("***** Error creating base file: %s\n", acBaseFile);
      return -4;
   }

   // Open Owner file
   //LogMsg("Open Owner file %s", acOwnerFile);
   //fdOwner = fopen(acOwnerFile, "w");
   //if (fdOwner == NULL)
   //{
   //   LogMsg("***** Error creating Owner file: %s\n", acOwnerFile);
   //   return -4;
   //}

   // Open Detail file
   LogMsg("Open Detail file %s", acItemsFile);
   fdDetail = fopen(acItemsFile, "w");
   if (fdDetail == NULL)
   {
      LogMsg("***** Error creating Detail file: %s\n", acItemsFile);
      return -4;
   }

   // Open Agency file
   LogMsg("Create Agency file %s", acAgencyFile);
   fdAgency = fopen(acAgencyFile, "w");
   if (fdAgency == NULL)
   {
      LogMsg("***** Error creating Agency file: %s\n", acAgencyFile);
      return -4;
   }

   // Merge loop 
   while (!feof(fdIn))
   {
      pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdIn);
      if (!pTmp)
         break;

      // Create base tax record
      iRet = Sdx_ParseTaxBase(acBaseBuf, acDelqBuf, acRec);
      if (!iRet)
      {
         // Update Delq record
         iRet = Sdx_ParseTaxDelq(acDelqBuf, fdDeft);
         if (!iRet)
         {
            Tax_CreateDelqCsv(acRec, (TAXDELQ *)&acDelqBuf);
            fputs(acRec, fdDelq);

            // Copy DelqYear from Delq record to Base record
            memcpy(pTaxBase->Def_Date, pTaxDelq->Def_Date, 8);
            memcpy(pTaxBase->DelqYear, pTaxDelq->Def_Date, 4);
         }

         // Create TaxBase record
         Tax_CreateTaxBaseCsv(acRec, (TAXBASE *)&acBaseBuf);
         lOut++;
         fputs(acRec, fdBase);

         // Create owner record
         //Tax_CreateTaxOwnerCsv(acRec, &((TAXBASE *)&acBaseBuf)->OwnerInfo);
         //fputs(acRec, fdOwner);
      } else
      {
         LogMsg("---> Drop record %d [%.40s]", lCnt, acRec); 
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdIn)
      fclose(fdIn);
   if (fdDeft)
      fclose(fdDeft);
   if (fdBase)
      fclose(fdBase);
   if (fdDelq)
      fclose(fdDelq);
   //if (fdOwner)
   //   fclose(fdOwner);
   if (fdDetail)
      fclose(fdDetail);
   if (fdAgency)
      fclose(fdAgency);

   LogMsg("Total records processed: %u", lCnt);
   LogMsg("         output records: %u", lOut);
   LogMsg("             Bad/No TRA: %u\n", lBadTRA);

   // Import into SQL
   if (bImport)
   {
      iRet = doTaxImport(myCounty.acCntyCode, TAX_BASE);
      if (!iRet)
         iRet = doTaxImport(myCounty.acCntyCode, TAX_DELINQUENT);
      //if (!iRet)
      //   iRet = doTaxImport(myCounty.acCntyCode, TAX_OWNER);
   } else
      iRet = 0;

   return iRet;
}

/***************************** Sdx_ParseTaxDetail ****************************
 *
 * Parsing CSV Special Assessment record (new CSV format 20180515)
 *
 *****************************************************************************/

int Sdx_ParseTaxDetail_2(char *pDetailBuf, char *pAgencyBuf, char *pInbuf)
{
   TAXDETAIL *pDetail = (TAXDETAIL *)pDetailBuf;
   TAXAGENCY *pAgency = (TAXAGENCY *)pAgencyBuf;
   TAXAGENCY *pTmpAgency;

   double     dTaxAmt;

   // Parse input tax data
   iTokens = ParseStringIQ(pInbuf, cDelim, MAX_FLD_TOKEN, apTokens);
   if (iTokens < SA_ID)
   {
      LogMsg("***** Error: bad Tax SA record for APN=%.*s (#tokens=%d)", iApnLen, pInbuf, iTokens);
      return -1;
   } 

   // Clear output buffer
   memset(pDetailBuf, 0, sizeof(TAXDETAIL));
   memset(pAgencyBuf, 0, sizeof(TAXAGENCY));

   // APN
   strcpy(pDetail->Apn, apTokens[SA_APN]);

   // Tax Year
   sprintf(pDetail->TaxYear, "%d", lTaxYear);

   // BillNum
   strcpy(pDetail->BillNum, apTokens[SA_APN]);

   // Agency 
   strcpy(pDetail->TaxCode, apTokens[SA_FUND_NUM]);
   strcpy(pAgency->Code, pDetail->TaxCode);
   if (iTokens > SA_ID+1)
   {
      sprintf(pAgency->Agency, "%s%s", apTokens[SA_FUND_DESC], apTokens[SA_FUND_DESC+1]);
      dTaxAmt = atof(apTokens[SA_FUND_AMT+1]);
   } else
   {
      strcpy(pAgency->Agency,  apTokens[SA_FUND_DESC]);
      dTaxAmt = atof(apTokens[SA_FUND_AMT]);
   }

   pTmpAgency = findTaxAgency(pAgency->Code);
   if (pTmpAgency)
   {
      strcpy(pAgency->Agency,  pTmpAgency->Agency);
      pAgency->TC_Flag[0] = pTmpAgency->TC_Flag[0];
      pDetail->TC_Flag[0] = pTmpAgency->TC_Flag[0];
   } else
   {
      LogMsg("New Agency: %s|%s", pAgency->Code, pAgency->Agency);
      pDetail->TC_Flag[0] = '0';
      pAgency->TC_Flag[0] = '0';
   }

   // Tax amt - accepting both pos and neg values
   sprintf(pDetail->TaxAmt, "%.2f", dTaxAmt);

   //if (pDetail->TaxCode[0] > ' ')
   //{
   //   strcpy(pAgency->Code, pDetail->TaxCode);
   //   pResult = findTaxAgency(pDetail->TaxCode);
   //   if (pResult)
   //   {
   //      strcpy(pAgency->Agency,  pResult->Agency);
   //      strcpy(pAgency->Phone,   pResult->Phone);
   //      strcpy(pAgency->TaxRate, pResult->TaxRate);
   //      strcpy(pDetail->TaxRate, pResult->TaxRate);
   //   } else
   //   {
   //      memcpy(pAgency->Agency, pInRec->FundDesc, FSIZ_FUND_DESC);
   //      iNewCode++;
   //      if (iNewCode < 1000)
   //         LogMsg("+++ New TaxCode: %s", pAgency->Code);
   //   }
   //} 

   return 0;
}

/***************************** Sdx_ParseTaxDetail ****************************
 *
 * Parsing fixed length Special Assessment record
 *
 *****************************************************************************/

int Sdx_ParseTaxDetail_1(char *pDetailBuf, char *pAgencyBuf, char *pInbuf)
{
   static int iNewCode=0;

   TAXDETAIL *pDetail = (TAXDETAIL *)pDetailBuf;
   TAXAGENCY *pAgency = (TAXAGENCY *)pAgencyBuf, *pResult;
   SPECASMT  *pInRec   = (SPECASMT *)pInbuf;
   double     dTmp;

   // Clear output buffer
   memset(pDetailBuf, 0, sizeof(TAXDETAIL));
   memset(pAgencyBuf, 0, sizeof(TAXAGENCY));

   // APN
   memcpy(pDetail->Apn, pInRec->Apn, FSIZ_APN);

   // Tax Year
   sprintf(pDetail->TaxYear, "%d", lTaxYear);

   // BillNum
   memcpy(pDetail->BillNum, pInRec->Apn, iApnLen);

   // Tax amt - accepting both pos and neg values
   dTmp = atofn(pInRec->FundAmt, FSIZ_FUND_AMT);
   sprintf(pDetail->TaxAmt, "%.2f", dTmp);

   // Agency 
   memcpy(pDetail->TaxCode, pInRec->FundNum, FSIZ_FUND_NUM);

   if (pDetail->TaxCode[0] > ' ')
   {
      strcpy(pAgency->Code,    pDetail->TaxCode);
      pResult = findTaxAgency(pDetail->TaxCode);
      if (pResult)
      {
         strcpy(pAgency->Agency,  pResult->Agency);
         strcpy(pAgency->Phone,   pResult->Phone);
         strcpy(pAgency->TaxRate, pResult->TaxRate);
         strcpy(pDetail->TaxRate, pResult->TaxRate);
      } else
      {
         memcpy(pAgency->Agency, pInRec->FundDesc, FSIZ_FUND_DESC);
         iNewCode++;
         if (iNewCode < 1000)
            LogMsg("+++ New TaxCode: %s", pAgency->Code);
      }
   } 

   return 0;
}

/**************************** Sdx_Load_TaxDetail ******************************
 *
 * Loading special_assessments_updt_wits_20160918_221754.dat to populate Tax_Items & Tax_Agency
 *
 * Return 0 if success.
 *
 *****************************************************************************/

int Sdx_Load_TaxDetail(bool bImport)
{
   char     *pTmp, acItemsRec[MAX_RECSIZE], acAgencyRec[512], acRec[512];
   char     acItemsFile[_MAX_PATH], acAgencyFile[_MAX_PATH], acTmpFile[_MAX_PATH], acInFile[_MAX_PATH];

   int      iRet, iVer;
   long     lItems=0, lCnt=0, lAgency=0;
   FILE     *fdItems, *fdIn;
   TAXDETAIL *pTax    = (TAXDETAIL *)&acItemsRec[0];
   TAXAGENCY *pAgency = (TAXAGENCY *)&acAgencyRec[0];
   SPECASMT  *pSpcAsmt= (SPECASMT *)&acRec[0];

   LogMsg0("Loading Detail file");

   GetIniString(myCounty.acCntyCode, "TaxDetail", "", acInFile, _MAX_PATH, acIniFile);
   iVer = GetPrivateProfileInt(myCounty.acCntyCode, "DetailVer", 1, acIniFile);
   NameTaxCsvFile(acItemsFile, myCounty.acCntyCode, "Items");
   NameTaxCsvFile(acAgencyFile, myCounty.acCntyCode, "Agency");

   // Open input file
   LogMsg("Open Detail file %s", acInFile);
   fdIn = fopen(acInFile, "r");
   if (fdIn == NULL)
   {
      LogMsg("***** Error opening Detail file: %s\n", acInFile);
      return -2;
   }  

   // Open Items file
   LogMsg("Append to Items file %s", acItemsFile);
   fdItems = fopen(acItemsFile, "a+");
   if (fdItems == NULL)
   {
      LogMsg("***** Error creating Items file: %s\n", acItemsFile);
      return -4;
   }

   // Open Agency file
   LogMsg("Append to Agency file %s", acAgencyFile);
   fdAgency = fopen(acAgencyFile, "a+");
   if (fdAgency == NULL)
   {
      LogMsg("***** Error Appending to Agency file: %s\n", acAgencyFile);
      return -4;
   }

   // Drop header rec & get first rec
   pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdIn);
   pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdIn);

   // Merge loop 
   while (!feof(fdIn))
   {
      // Create Items & Agency record
      if (iVer == 1)
         iRet = Sdx_ParseTaxDetail_1(acItemsRec, acAgencyRec, acRec);   // Fixed length format
      else
         iRet = Sdx_ParseTaxDetail_2(acItemsRec, acAgencyRec, acRec);   // CSV format

      if (!iRet)
      {
         // Create TaxBase record
         Tax_CreateDetailCsv(acRec, (TAXDETAIL *)&acItemsRec);
         lItems++;
         fputs(acRec, fdItems);

         // Create Agency record
         if (pAgency->Agency[0] > ' ')
         {
            Tax_CreateAgencyCsv(acRec, (TAXAGENCY *)&acAgencyRec);
            fputs(acRec, fdAgency);
         }
      } else
      {
         LogMsg0("---> Drop record %d [%.80s]", lCnt, acRec); 
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);

      pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdIn);
   }

   // Close files
   if (fdIn)
      fclose(fdIn);
   if (fdItems)
      fclose(fdItems);
   if (fdAgency)
      fclose(fdAgency);

   LogMsg("Total records processed:    %u", lCnt);
   LogMsg("Total Items records:        %u", lItems);

   // Import into SQL
   if (bImport)
   {
      iRet = doTaxImport(myCounty.acCntyCode, TAX_DETAIL);
      if (!iRet)
      {
         // Dedup Agency file before import
         sprintf(acTmpFile, "%s\\%s\\%s_Agency.tmp", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);

         LogMsg("Dedup Agency file %s", acAgencyFile);
         iRet = sortFile(acAgencyFile, acTmpFile, "S(#1,C,A) DUPOUT F(TXT)");
         if (iRet > 0)
         {
            CopyFile(acTmpFile, acAgencyFile, false);
            iRet = doTaxImport(myCounty.acCntyCode, TAX_AGENCY);
         }
      }
   } else
      iRet = 0;

   return iRet;
}

int Sdx_ParseEnrolledValue(LPSTR pInbuf, LPSTR pOutbuf)
{
   VALUE_REC   *pVal = (VALUE_REC *)pOutbuf;
   int         iRet = 0, lVal, lLand, lImpr, lOther;
   char        sTmp[64];

   // Parse input rec
   iTokens = ParseStringIQ(pInbuf, '|', 64, apTokens);

#ifdef _DEBUG
   //if (iTokens > 1 && !memcmp(apTokens[EV_APN], "5220421800", 9))
   //   iRet = 0;
#endif

   if (iTokens > EV_VALUE_CHNG_CODE && *apTokens[0] > '0')
   {
      memset(pOutbuf, ' ', sizeof(VALUE_REC));
      memcpy(pVal->Apn, apTokens[EV_APN], strlen(apTokens[EV_APN]));
      pVal->ValueSetType[0] = '1';
      if (*apTokens[EV_VALUE_CHNG_CODE] > ' ')
         pVal->Reason[0] = *apTokens[EV_VALUE_CHNG_CODE];
      memcpy(pVal->TaxYear, apTokens[EV_ASSESSMENT_YEAR], 4);
      lLand = atol(apTokens[EV_LAND_VALUE]);
      if (lLand > 0)
      {
         iRet = sprintf(sTmp, "%d", lLand);
         memcpy(pVal->Land, sTmp, iRet);
      }

      lImpr = atol(apTokens[EV_IMPR_VALUE]);
      if (lImpr > 0)
      {
         iRet = sprintf(sTmp, "%d", lImpr);
         memcpy(pVal->Impr, sTmp, iRet);
      }

      lVal = atol(apTokens[EV_FIXT_VALUE]);
      if (lVal > 0)
      {
         iRet = sprintf(sTmp, "%d", lVal);
         memcpy(pVal->Fixtr, sTmp, iRet);
      }
      lOther = lVal;

      lVal = atol(apTokens[EV_PP_VALUE]);
      if (lVal > 0)
      {
         iRet = sprintf(sTmp, "%d", lVal);
         memcpy(pVal->PersProp, sTmp, iRet);
      }
      lOther += lVal;

      if (lOther > 0)
      {
         iRet = sprintf(sTmp, "%d", lOther);
         memcpy(pVal->Other, sTmp, iRet);
      }

      // Exemption
      lVal = atol(apTokens[EV_TOTAL_EXE_VALUE]);
      if (lVal > 0)
      {
         iRet = sprintf(sTmp, "%d", lVal);
         memcpy(pVal->Exe, sTmp, iRet);
      }

      // Gross
      lVal = atol(apTokens[EV_TOTAL_ASSD_VALUE]);
      if (lVal > 0)
      {
         iRet = sprintf(sTmp, "%d", lVal);
         memcpy(pVal->TotalVal, sTmp, iRet);
      }
      pVal->CRLF[0] = '\n';
      pVal->CRLF[1] = 0;
      iRet = 0;
   } else
   {
      LogMsg("*** Bad input record: %s", pInbuf);
      iRet = -1;
   }

   return iRet;
}

int Sdx_ParseFactorBaseYearValue(LPSTR pInbuf, LPSTR pOutbuf)
{
   VALUE_REC   *pVal = (VALUE_REC *)pOutbuf;
   int         iRet = 0, lVal, lLand, lImpr;
   char        sTmp[32];

   // Parse input rec
   iTokens = ParseStringNQ(pInbuf, ',', 64, apTokens);

#ifdef _DEBUG
   //if (iTokens > 1 && !memcmp(apTokens[FV_APN], "5220421800", 9))
   //   iRet = 0;
#endif

   if (iTokens > FV_TOTAL_VALUE && *apTokens[0] > ' ')
   {
      memset(pOutbuf, ' ', sizeof(VALUE_REC));
      memcpy(pVal->Apn, apTokens[FV_APN], strlen(apTokens[FV_APN]));
      pVal->ValueSetType[0] = '2';
      memcpy(pVal->TaxYear, myCounty.acYearAssd, 4);
      lLand = atol(apTokens[FV_LAND_VALUE]);
      if (lLand > 0)
      {
         iRet = sprintf(sTmp, "%d", lLand);
         memcpy(pVal->Land, sTmp, iRet);
      }

      lImpr = atol(apTokens[FV_IMPR_VALUE]);
      if (lImpr > 0)
      {
         iRet = sprintf(sTmp, "%d", lImpr);
         memcpy(pVal->Impr, sTmp, iRet);
      }

      lVal = atol(apTokens[FV_TOTAL_VALUE]);
      if (lVal > 0)
      {
         iRet = sprintf(sTmp, "%d", lVal);
         memcpy(pVal->TotalVal, sTmp, iRet);
      }
      pVal->CRLF[0] = '\n';
      pVal->CRLF[1] = 0;
      iRet = 0;
   } else
   {
      LogMsg("*** Bad input record: %s", pInbuf);
      iRet = -1;
   }

   return iRet;
}

/******************************************************************************
 *
 * Extract base year values from enrolled value file
 *
 ******************************************************************************/

int Sdx_ExtrEnrolledValues(LPSTR pValueFile, LPSTR pOutFile, int iSkipRows)
{
   int   lCnt, lOut, iRet, iTmp;
   char  sInbuf[4096], sOutbuf[1024], *pTmp;
   FILE  *fdIn, *fdOut;

   LogMsg0("Extract base year values from Value file for %.3s", myCounty.acCntyCode);

   // Open input
   LogMsg("Opening ... %s", pValueFile);
   fdIn = fopen(pValueFile, "r");
   if (!fdIn)
   {
      LogMsg("***** Error opening %s (%d)", pValueFile, _errno);
      return -1;
   }

   // Create output file
   LogMsg("Creating ... %s", pOutFile);
   fdOut = fopen(pOutFile, "w");
   if (!fdOut)
   {
      LogMsg("***** Error creating %s (%d)", pOutFile, _errno);
      return -2;
   }

   // Skip header
   for (iTmp = 0; iTmp < iSkipRows; iTmp++)
      pTmp = fgets(sInbuf, 1024, fdIn);

   lCnt=lOut=0;
   // Loop through it
   while (!feof(fdIn))
   {
      if (!(pTmp = fgets(sInbuf, 1024, fdIn)))
         break;

      iRet = Sdx_ParseEnrolledValue(sInbuf, sOutbuf);
      if (!iRet)
      {
         fputs(sOutbuf, fdOut);
         lOut++;
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);

   }

   // CLosing
   fclose(fdIn);
   fclose(fdOut);

   LogMsg("Number of records processed: %d", lCnt);
   LogMsg("Number of records output:    %d", lOut);

   return 0;
}

int Sdx_ExtrFactorBaseYearValues(LPSTR pValueFile, LPSTR pOutFile, int iSkipRows)
{
   int   lCnt, lOut, iRet, iTmp;
   char  sInbuf[4096], sOutbuf[1024], *pTmp;
   FILE  *fdIn, *fdOut;

   LogMsg0("Extract factor base year values", myCounty.acCntyCode);

   // Open input
   LogMsg("Opening ... %s", pValueFile);
   fdIn = fopen(pValueFile, "r");
   if (!fdIn)
   {
      LogMsg("***** Error opening %s (%d)", pValueFile, _errno);
      return -1;
   }

   // Create output file
   LogMsg("Creating ... %s", pOutFile);
   fdOut = fopen(pOutFile, "a+");
   if (!fdOut)
   {
      LogMsg("***** Error creating %s (%d)", pOutFile, _errno);
      return -2;
   }

   // Skip header
   for (iTmp = 0; iTmp < iSkipRows; iTmp++)
      pTmp = fgets(sInbuf, 1024, fdIn);

   lCnt=lOut=0;
   // Loop through it
   while (!feof(fdIn))
   {
      if (!(pTmp = fgets(sInbuf, 1024, fdIn)))
         break;

      iRet = Sdx_ParseFactorBaseYearValue(sInbuf, sOutbuf);
      if (!iRet)
      {
         fputs(sOutbuf, fdOut);
         lOut++;
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);

   }

   // CLosing
   fclose(fdIn);
   fclose(fdOut);

   LogMsg("Number of records processed: %d", lCnt);
   LogMsg("Number of records output:    %d", lOut);

   return 0;
}

/****************************** Sdx_ParseTaxRate ****************************
 *
 * This function reads input from TAX_RATE.DAT and output TRA_Dist.txt & Sdx_Fund.txt.  
 * Each row consists of TRA and a list of fund numbers which belongs to that TRA.
 *
 ****************************************************************************/

int Sdx_ParseTaxRate(LPSTR pInfile, LPSTR pFundRate, LPSTR pTraDist)
{
   int   iIdx, iCnt, iTRA, iFund, iTmp;
   char  sOutbuf[1024], sInbuf[1024], sTRA[32], sFundNum[512], sTmpFile[_MAX_PATH], *pTmp;
   FILE  *fdIn, *fdFundRate, *fdTraDist;

   LogMsg0("Extract TRA & Fund number");

   // Open input file
   LogMsg("Open %s", pInfile);
   if (!(fdIn = fopen(pInfile, "r")))
   {
      LogMsg("***** Error opening %s", pInfile);
      return -1;
   }

   // Open output file
   LogMsg("Create %s", pTraDist);
   if (!(fdTraDist = fopen(pTraDist, "w")))
   {
      fclose(fdIn);
      LogMsg("***** Error creating %s", pTraDist);
      return -2;
   }

   sprintf(sTmpFile, "%s\\%s\\SDX_Fund.txt", acTmpPath, myCounty.acCntyCode);
   LogMsg("Create %s", sTmpFile);
   if (!(fdFundRate = fopen(sTmpFile, "w")))
   {
      fclose(fdIn);
      LogMsg("***** Error creating %s", sTmpFile);
      return -2;
   }
   strcpy(sOutbuf, "Code|Agency|Phone|TaxRate|TaxAmt|TCFlg\n");
   fputs(sOutbuf, fdFundRate);

   iIdx = iCnt = iTRA = iFund = 0;
   sTRA[0] = 0;
   sFundNum[0] = 0;
   while (!feof(fdIn))
   {
      pTmp = fgets(sInbuf, 1024, fdIn);
      if (!pTmp)
      {
         // Output last TRA
         sprintf(sOutbuf, "%s|%s|%d\n", sTRA, sFundNum, iIdx);
         fputs(sOutbuf, fdTraDist);
         iTRA++;
         break;
      }

      iTokens = ParseString(sInbuf, ';', 32, apTokens);
      if (iTokens >= TRA_CODE_ID)
      {
         iTmp = atoin(apTokens[TRA_FISCAL_YEAR], 4);
         if (iTmp == lLienYear)
         {
            // Create FundRate record
            sprintf(sOutbuf, "%s|%s||%.5f||0\n", apTokens[TRA_FUND_NUMBER], apTokens[TRA_AGENCY], atof(apTokens[TRA_RATE]));
            fputs(sOutbuf, fdFundRate);

            // If new TRA, write out last TRA
            if (strcmp(sTRA, apTokens[TRA_TRA_CODE]))
            {
               if (iIdx > 0)
               {
                  sprintf(sOutbuf, "%s|%s|%d\n", sTRA, sFundNum, iIdx);
                  fputs(sOutbuf, fdTraDist);
                  iTRA++;
               }
               iIdx = 1;

               // Init new TRA
               strcpy(sTRA, apTokens[TRA_TRA_CODE]);
               strcpy(sFundNum, apTokens[TRA_FUND_NUMBER]);
            } else
            {
               strcat(sFundNum, ",");
               strcat(sFundNum, apTokens[TRA_FUND_NUMBER]);
               iIdx++;
            }
         }
      } else
         LogMsg("*** Invalid entry: %s", sInbuf);

      iCnt++;
   }

   fclose(fdIn);
   fclose(fdTraDist);
   fclose(fdFundRate);

   // Sort and dedup
   LogMsg("Dedup Fund Rate file %s to %s", sTmpFile, pFundRate);
   iTRA = sortFile(sTmpFile, pFundRate, "S(#1,C,A) DEL(124) DUPOUT(1,30) B(38,R) F(TXT)");

   LogMsg("Number of record processed: %d", iCnt);
   LogMsg("                TRA output: %d", iTRA);

   return   iTRA;
}

/**************************** Sdx_ParsePhoneList ****************************
 *
 * This function reads input from phone_list.dat and output SDX_Phone.txt.  
 *
 ****************************************************************************/

int Sdx_ParsePhoneList(LPSTR pInfile, LPSTR pOutfile)
{
   int   iCnt, iIdx, iFromFund, iToFund;
   char  sOutbuf[1024], sInbuf[1024], sPhone[32], *pTmp;
   FILE  *fdIn, *fdOut;
   FUNDPHONE *pPhone = (FUNDPHONE *)&sInbuf;

   LogMsg0("Extract phone number");

   // Open input file
   LogMsg("Open %s", pInfile);
   if (!(fdIn = fopen(pInfile, "r")))
   {
      LogMsg("***** Error opening %s", pInfile);
      return -1;
   }
   // Skip first row
   pTmp = fgets(sInbuf, 1024, fdIn);

   // Open output file
   LogMsg("Create %s", pOutfile);
   if (!(fdOut = fopen(pOutfile, "w")))
   {
      fclose(fdIn);
      LogMsg("***** Error creating %s", pOutfile);
      return -2;
   }

   // Write header
   strcpy(sOutbuf, "FromFund|Phone\n");
   fputs(sOutbuf, fdOut);

   iCnt = 0;
   while (!feof(fdIn))
   {
      pTmp = fgets(sInbuf, 1024, fdIn);
      if (!pTmp)
         break;

      if (pPhone->Extension[0] > ' ')
         sprintf(sPhone, "(%.3s) %.3s-%.4s x%s", pPhone->Phone, &pPhone->Phone[3], &pPhone->Phone[6], myTrim(pPhone->Extension));
      else
         sprintf(sPhone, "(%.3s) %.3s-%.4s", pPhone->Phone, &pPhone->Phone[3], &pPhone->Phone[6]);

      iFromFund = atoin(pPhone->FromFund, FSIZ_FUND_NUM);
      iToFund   = atoin(pPhone->ToFund, FSIZ_FUND_NUM);
      for (iIdx = iFromFund;  iIdx <= iToFund; iIdx++)
      {
         sprintf(sOutbuf, "%.6d|%s\n", iIdx, sPhone);
         fputs(sOutbuf, fdOut);
         iCnt++;
      }
   }

   fclose(fdIn);
   fclose(fdOut);

   LogMsg("Number of record processed: %d", iCnt);

   return   iCnt;
}

/*************************** Sdx_ParseSpecAsmtList **************************
 *
 * This function reads input from Tax Detail special_assessments_full_wits.dat
 * and output SDX_Dist.txt.  
 *
 ****************************************************************************/

int Sdx_ParseSpecAsmtList(LPSTR pInfile, LPSTR pOutfile)
{
   int      iCnt;
   char     sOutbuf[1024], sInbuf[1024], sTmpFile[_MAX_PATH], *pTmp;
   FILE     *fdIn, *fdOut;
   SPECASMT *pDetail = (SPECASMT *)&sInbuf;

   LogMsg0("Extract special district info for SDX_Dist.txt");

   // Open input file
   LogMsg("Open %s", pInfile);
   if (!(fdIn = fopen(pInfile, "r")))
   {
      LogMsg("***** Error opening %s", pInfile);
      return -1;
   }
   // Skip first row
   pTmp = fgets(sInbuf, 1024, fdIn);

   // Open output file
   sprintf(sTmpFile, "%s\\%s\\Sdx_District.txt", acTmpPath, myCounty.acCntyCode);
   LogMsg("Create temp output %s", sTmpFile);
   if (!(fdOut = fopen(sTmpFile, "w")))
   {
      fclose(fdIn);
      LogMsg("***** Error creating %s", sTmpFile);
      return -2;
   }

   // Write header
   strcpy(sOutbuf, "Code|Agency|Phone|TaxRate|TaxAmt|TCFlg\n");
   fputs(sOutbuf, fdOut);

   iCnt = 0;
   while (!feof(fdIn))
   {
      pTmp = fgets(sInbuf, 1024, fdIn);
      if (!pTmp)
         break;

      pDetail->FundDesc[FSIZ_FUND_DESC] = 0;
      sprintf(sOutbuf, "%.6s|%s||||0\n", pDetail->FundNum, myTrim(pDetail->FundDesc));
      fputs(sOutbuf, fdOut);
      iCnt++;
   }

   fclose(fdIn);
   fclose(fdOut);

   // Sort and dedup
   LogMsg("Dedup District file %s to %s", sTmpFile, pOutfile);
   iCnt = sortFile(sTmpFile, pOutfile, "S(#1,C,A) BYPASS(38,R) DEL(124) DUPOUT F(TXT)");

   LogMsg("Number of record output: %d", iCnt);

   return   iCnt;
}

/****************************************************************************
 *
 * This function can generate TaxDist.txt file for current year only.  For 
 * supplemental that uses prior year tax code, we have to manually enter.
 *
 ****************************************************************************/

int Sdx_CreateTaxDist()
{
   int   iRet;
   char  acTmpFile[_MAX_PATH], acTDFile[_MAX_PATH], acSFFile[_MAX_PATH], acPhoneFile[_MAX_PATH],
         acDistFile[_MAX_PATH], acTRFile[_MAX_PATH], acTmp[256];

   LogMsg0("Process TRA file");

   // 1) Create TRA_Dist.txt: list of TRA and Fund number - this is bond fund only, not county agency
   //       and SDX_Fund.txt: list of fund number, fund name and tax rate

   //    - Sort "SdxRate" (TAX_RATE.DAT) to "TaxRate" (TRA_RATE.TXT) using Tax_Rate.ots to filter out unwanted entries
   GetIniString(myCounty.acCntyCode, "SdxRate", "", acTmpFile, _MAX_PATH, acIniFile);
   GetIniString(myCounty.acCntyCode, "TaxRate", "", acTRFile, _MAX_PATH, acIniFile);

   sprintf(acTmp, "S(#1,C,A,#3,C,A) DEL(59) OMIT(#2,C,LT,\"%d\")", lLienYear);
   iRet = sortFile(acTmpFile, acTRFile, acTmp);
   if (iRet > 0)
   {
      GetIniString(myCounty.acCntyCode, "TRADist", "", acTDFile, _MAX_PATH, acIniFile);
      GetIniString(myCounty.acCntyCode, "SdxFund", "", acSFFile, _MAX_PATH, acIniFile);
      iRet = Sdx_ParseTaxRate(acTRFile, acSFFile, acTDFile);
   }

   // 2) Create SDX_Dist.txt - this is county agency, not bond fund
   GetIniString(myCounty.acCntyCode, "TaxDetail", "", acTmpFile, _MAX_PATH, acIniFile);
   GetIniString(myCounty.acCntyCode, "SdxDist", "", acDistFile, _MAX_PATH, acIniFile);
   iRet = Sdx_ParseSpecAsmtList(acTmpFile, acDistFile);

   // 3) Create SDX_Phone.txt - This is not needed since website doesn't display phone number
   GetIniString(myCounty.acCntyCode, "Phonein", "", acTmpFile, _MAX_PATH, acIniFile);
   GetIniString(myCounty.acCntyCode, "Phoneout", "", acPhoneFile, _MAX_PATH, acIniFile);
   iRet = Sdx_ParsePhoneList(acTmpFile, acPhoneFile);
   //    - Merge SDX_Dist & SDX_Phone tables to update SDX_Dist table with phone number from SDX_Phone.
   //    - UPDATE SDX_Dist SET phone = Sdx_phone.phone FROM Sdx_phone WHERE Sdx_phone.fromfund = SDX_Dist.code

   // 4) Append acDistFile to acSFFile, resort it then copy to C:\Tools\Tax\Sdx_TaxDist.txt   
   sprintf(acTmp, "%s+%s", acDistFile, acSFFile);
   iRet = GetIniString(myCounty.acCntyCode, "TaxDist", "", acTmpFile, _MAX_PATH, acIniFile);

   iRet = sortFile(acTmp, acTmpFile, "S(#1,C,A) DEL(124) DUPO(#1) F(TXT) B(38,R)");

   return iRet;
}

/****************************** Sdx_FmtChar **********************************
 *
 *
 *****************************************************************************/

int Sdx_FmtChar(char *pOutbuf, char *pInbuf)
{
   char     acTmp[_MAX_PATH], *pTmp;
   int      iTmp, lTmp;

   STDCHAR  *pCharRec=(STDCHAR *)pOutbuf;
   SDX_CHAR *pInrec =(SDX_CHAR *)pInbuf;
   
   // Init output buffer
   memset(pOutbuf, ' ', sizeof(STDCHAR));

   // APN
   memcpy(pCharRec->Apn, pInrec->Apn, CSIZ_APN);

   iTmp = formatApn(pInrec->Apn, acTmp, &myCounty);
   memcpy(pCharRec->Apn_D, acTmp, iTmp);

   // TRA
   lTmp = atoin(pInrec->Tra, CSIZ_TRA);
   if (lTmp > 0)
   {
      iTmp = sprintf(acTmp, "%.6d", lTmp);
      memcpy(pCharRec->Misc.sExtra.TRA, acTmp, iTmp);
   }

#ifdef _DEBUG
   //if (!memcmp(pCharRec->Apn, "139040027", 9) )
   //   iTmp = 0;
#endif

   // Acreage
   ULONG lLotAcres, lLotSqft;

   // Lot sqft
   lLotSqft = atoln(pInrec->LotSqft, CSIZ_LOT_SQFT);
   if (lLotSqft == 99999)
      lLotSqft = 0;

   lLotAcres = atoln(pInrec->Acres, CSIZ_ACREAGE);
   if (lLotAcres > 0)
   {
      if (!lLotSqft)
         lLotSqft = (ULONG)((double)lLotAcres*SQFT_FACTOR_100);
      lLotAcres *= 10;
   } else if (lLotSqft > 100)
      lLotAcres = (ULONG)(double)(lLotSqft*SQFT_MF_1000);

   if (lLotSqft > 100)
   {
      iTmp = sprintf(acTmp, "%u", lLotSqft);
      memcpy(pCharRec->LotSqft, acTmp, iTmp);
   }
   if (lLotAcres > 100)
   {
      iTmp = sprintf(acTmp, "%u", lLotAcres);
      memcpy(pCharRec->LotAcre, acTmp, iTmp);
   }

   // Eff Year - Year Built
   lTmp = atoin(pInrec->EffYear, CSIZ_EFF_YR);
   if (lTmp > 0)
   {
      pTmp = dateConversion(pInrec->EffYear, acTmp, YY2YYYY);
      if (pTmp)
      {
         memcpy(pCharRec->YrEff, acTmp, 4);
         memcpy(pCharRec->YrBlt, acTmp, 4);
      }
   }

   // BldgSqft
   lTmp = atoin(pInrec->BldgSqft, CSIZ_BLDG_SQFT);
   if (lTmp > 0)
   {
      iTmp = sprintf(acTmp, "%d", lTmp);
      memcpy(pCharRec->BldgSqft, acTmp, iTmp);
   }

   // Parking space
   lTmp = atoin(pInrec->GarStalls, CSIZ_GARAGE_STALLS);
   if (lTmp > 0)
   {
      iTmp = sprintf(acTmp, "%d", lTmp);
      memcpy(pCharRec->ParkSpace, acTmp, iTmp);
   }

   // Beds
   lTmp = atoin(pInrec->Beds, CSIZ_BEDROOMS);
   if (lTmp > 0)
   {
      iTmp = sprintf(acTmp, "%d", lTmp);
      memcpy(pCharRec->Beds, acTmp, iTmp);
   }

   // Baths
   lTmp = atoin(pInrec->Baths, CSIZ_BATHS-1);
   if (lTmp > 0)
   {
      iTmp = sprintf(acTmp, "%d", lTmp);
      memcpy(pCharRec->FBaths, acTmp, iTmp);
      memcpy(pCharRec->Bath_4Q, acTmp, iTmp);

      // Half bath
      iTmp = pInrec->Baths[CSIZ_BATHS-1] & 0x0F;
      if (iTmp > 0)
      {
         pCharRec->HBaths[0] = '1';
         if (iTmp == 5)
            pCharRec->Bath_2Q[0] = '1';
         else if (iTmp < 5)
            pCharRec->Bath_1Q[0] = '1';
         else
            pCharRec->Bath_3Q[0] = '1';
      }
   }

   // Pool
   if (pInrec->Pool == 'Y')
      pCharRec->Pool[0] = 'P';

   // View
   if (pInrec->View == 'Y')
      pCharRec->View[0] = 'A';

   // Zoning
   if (pInrec->Zoning > '0')
      pCharRec->Zoning[0] = pInrec->Zoning;

   // UseCode
   if (pInrec->LandUse[0] > ' ')
      memcpy(pCharRec->LandUse, pInrec->LandUse, CSIZ_LAND_USE);

   pCharRec->CRLF[0] = '\n';
   pCharRec->CRLF[1] = '\0';

   return 0;
}

/***************************** Sdx_ConvStdChar *******************************
 *
 * Extract characteristics from COSDPCHAR.DAT to Sdx_Char.dat
 *
 * Return 0 if successful, otherwise error.
 *
 *****************************************************************************/

int Sdx_ConvStdChar(char *pInfile)
{
   char  *pTmp, acCharRec[2048], acInRec[MAX_RECSIZE], acTmpFile[_MAX_PATH];
   long  lCnt=0, lChars=0, iRet;
   FILE  *fdIn;

   LogMsg0("Extracting Chars from %s", pInfile);

   // Check current sale file
   if (_access(pInfile, 0))
   {
      LogMsg("***** Missing input file %s", pInfile);
      return -1;
   }

   // Open char file
   LogMsg("Open char file to extract CHARS %s", pInfile);
   fdIn = fopen(pInfile, "r");
   if (fdIn == NULL)
   {
      LogMsg("***** Error opening char file: %s\n", pInfile);
      return -2;
   }

   // Create output file
   sprintf(acTmpFile, "%s\\%s\\%s_Char.tmp", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode); 
   LogMsg("Create output chars file %s", acTmpFile);
   fdChar = fopen(acTmpFile, "w");
   if (fdChar == NULL)
   {
      LogMsg("***** Error creating char file: %s\n", acTmpFile);
      return -3;
   }

   // Merge loop
   while (!feof(fdIn))
   {
      // Get next record
      pTmp = fgets(acInRec, MAX_RECSIZE, fdIn);
      if (!pTmp)
         break;

      iRet = Sdx_FmtChar(acCharRec, acInRec);

      if (!iRet)
      {
         fputs(acCharRec, fdChar);
         lChars++;
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdChar)
      fclose(fdChar);
   if (fdIn)
      fclose(fdIn);

   LogMsg("Total input records processed: %u", lCnt);
   LogMsg("Total char records output:     %u\n", lChars);

   sprintf(acInRec, "S(1,%d,C,A) ", iApnLen);
   iRet = sortFile(acTmpFile, acCChrFile, acInRec);
   LogMsg("Output CHAR file: %s (%d)", acCChrFile, iRet);
   if (iRet > 0)
      iRet = 0;

   return iRet;
}

/*********************************** loadSdx ********************************
 *
 * Input files:
 *    - COSDMPR.DAT              (roll file, 649-byte fixed)
 *    - COSDPCHAR.DAT            (char file, 299-byte )
 *    - COSDSCHAR.DAT            (sale file, 222-byte )
 *
 * Commands:
 *    -Xc   Extract cum sale from R01
 *    -Us   Create Sale_Exp.dat from county sale file COSDSCHAR.DAT
 *    -U    Monthly update
 *    -L    Lien update
 *    -Lg   Load GrGr file
 *    -Mg   Update using Sdx_GrGr.dat
 *    -Mt   Merge transfer to sale file (do not use this with -L)
 *    -X8   Extract Prop8
 *    -Xl   Extract lien values
 *
 *    LDR command: -CSDX -L -Xl -Ms -Mg -Xv -Xa -X8 -Dr -Mr 
 *    Update cmd:  -CSDX -U -Xs|-Ms -G|-Mg -Xa -Mr [-X8] [-Xv]
 *
 * Notes: To capture all previous sales from mainframe, run -Xc first to collect
 *        history sale.  Or you can run -L -Xc at the same time but never
 *        run -Xc and -Us the same time.  Both -Xc and -Us will set acSaleFile
 *        to its output file.
 *        To run LDR, copy Sale_exp.sls and Sdx_GrGr.sls to local machine.
 *         
 *
 ****************************************************************************/

int loadSdx(int iSkip)
{
   int   iRet=0;
   char  acTmpFile[_MAX_PATH], acGrGrFile[_MAX_PATH];

   iApnLen = myCounty.iApnLen;

   // -Fc
   //if (lOptMisc & M_OPT_UCITYZIP)
   //{
   //   iRet = updateCityZip(acIniFile, myCounty.acCntyCode, iApnLen);
   //}

   // Loading Tax
   if (iLoadTax == TAX_LOADING)              // -T or -Lt
   {
      //if (!sqlConnEx(acTaxDBProvider, &dbTra))
      //   return -1;

      // Do this once a year to create Sdx_TaxDist.txt & TRA_Dist.txt
      iRet = GetIniString(myCounty.acCntyCode, "CreateTaxDist", "N", acTmpFile, _MAX_PATH, acIniFile);
      if (acTmpFile[0] == 'Y')
      {
         iRet = Sdx_CreateTaxDist();
         if (iRet <= 0)
            return iRet;
      }

      iRet = GetIniString(myCounty.acCntyCode, "TaxDist", "", acTmpFile, _MAX_PATH, acIniFile);
      if (iRet > 0)
         iRet = LoadTaxCodeTable(acTmpFile);

      iRet = GetIniString(myCounty.acCntyCode, "TRADist", "", acTmpFile, _MAX_PATH, acIniFile);
      if (iRet > 0)
         iRet = LoadTRADistTable(acTmpFile);

      // SDX has base, delq, detail, Owner
      iRet = Sdx_Load_TaxBase(bTaxImport);

      if (!iRet && lLastTaxFileDate > 0)
      {
         // Update Delq flag tb_isDelq
         iRet = updateDelqFlag(myCounty.acCntyCode);

         iRet = Sdx_Load_TaxDetail(bTaxImport);
      }
   }

   if (!iLoadFlag)
      return iRet;

   // Load Value file
   if (iLoadFlag & EXTR_VALUE)                     // -Xv
   {
      char sDbName[64], sBYVFile[_MAX_PATH], acTmp[_MAX_PATH], acTmp1[_MAX_PATH];

      // Extract enrolled value
      iRet = GetIniString(myCounty.acCntyCode, "EVFile", "", acTmpFile, _MAX_PATH, acIniFile);
      sprintf(sBYVFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "Value");
      iRet = Sdx_ExtrEnrolledValues(acTmpFile, sBYVFile, 0);
      if (!iRet)
      {
         GetIniString(myCounty.acCntyCode, "FBYVFile", "", acTmpFile, _MAX_PATH, acIniFile);
         iRet = Sdx_ExtrFactorBaseYearValues(acTmpFile, sBYVFile, 1);
      }

      if (!iRet)
      {
         sprintf(sDbName, "LDR%d", lLienYear);
         GetIniString("Data", "SqlValueFile", "", acTmp, _MAX_PATH, acIniFile);
         sprintf(acTmp1, acTmp, sDbName, myCounty.acCntyCode);
         lRecCnt = createValueImport(sBYVFile, acTmp1, false);
         if (lRecCnt > 0)
         {
            // Save output file for import
            strcpy(acValueFile, acTmp1);
            iRet = 0;
         } else if (iLoadFlag & EXTR_IVAL)
            iLoadFlag ^= EXTR_IVAL;                // Turn off import
      }
	}

   // Load tables
   //iRet = LoadLUTable((char *)&acLookupTbl[0], "[Quality]", NULL, MAX_ATTR_ENTRIES);
   //if (!iRet)
   //{
   //   LogMsg("*** Error Looking for table [Quality] in %s", acLookupTbl);
   //   return 1;
   //}

   // Count number of records that has same year or yyyymm or yyyymmdd
   //strcpy(acTmpFile, "H:\\CO_PROD\\SSDX_CD\\raw\\Sale_exp.sls");
   //iRet = countDate(acTmpFile, 2009, 26, 1);
   //iRet = resetOneChar(acTmpFile, 14, '0');
   //strcpy(acTmpFile, "H:\\CO_PROD\\SSDX_CD\\raw\\Sdx_grgr.sls");
   //iRet = resetOneChar(acTmpFile, 18, '0');

   // Convert old SDX_GREC to new 512-byte layout
   // ConvertGRec("SDX");

   // Extract Prop8 parcels
   if (lOptProp8 & MYOPT_EXT)                      // -X8 Extract prop8 flag to text file
   {
      GetIniString(myCounty.acCntyCode, "Prop8File", "", acTmpFile, _MAX_PATH, acIniFile);
      if (!strcmp(acTmpFile, acCharFile))
         iRet = Sdx_ExtractProp8(acTmpFile);
      else
         iRet = Sdx_ExtrLdrProp8(acTmpFile);
   }

   iRet = 0;

   // Extract history sales - use this only if needed - comment out to prevent accident
   if (iLoadFlag & LOAD_SALE)                      // -Ls
   {
      // Load history sale 
      iRet = Sdx_LoadSaleHist();
      /*
      char *pTmp;
      pTmp = "G:\\CO_DATA\\SDX\\COSDSCHAR_200606.DAT";
      iRet = Sdx_ExtrSale(pTmp);
      pTmp = "G:\\CO_DATA\\SDX\\COSDSCHAR_200801.DAT";
      iRet = Sdx_ExtrSale(pTmp);
      pTmp = "G:\\CO_DATA\\SDX\\COSDSCHAR_200811.DAT";
      iRet = Sdx_ExtrSale(pTmp);
      pTmp = "G:\\CO_DATA\\SDX\\COSDSCHAR_200906.DAT";
      iRet = Sdx_ExtrSale(pTmp);
      pTmp = "G:\\CO_DATA\\SDX\\COSDSCHAR_201006.DAT";
      iRet = Sdx_ExtrSale(pTmp);
      pTmp = "G:\\CO_DATA\\SDX\\COSDSCHAR_201101.DAT";
      iRet = Sdx_ExtrSale(pTmp);
      pTmp = "G:\\CO_DATA\\SDX\\COSDSCHAR_201109.DAT";
      iRet = Sdx_ExtrSale(pTmp);
      pTmp = "G:\\CO_DATA\\SDX\\COSDSCHAR_201203.DAT";
      iRet = Sdx_ExtrSale(pTmp);
      */
   }

   // Extract sales
   if (iLoadFlag & (EXTR_SALE|UPDT_SALE))          // -Xs or -Us
   {
      iRet = Sdx_ExtrSale(acSaleFile);
      if (!iRet)
         iLoadFlag |= MERG_CSAL;
   }

   // Load GrGr
   // Input:  GRANTORGRANTEEADDS.DAT
   // Output: Sdx_GrGr.dat
   if (iLoadFlag & LOAD_GRGR)                      // -Lg
   {
      LogMsg0("Prepare GrGr file for processing...");  

      // Move GrGr files to its subfolder
      iRet = moveGrGrFiles(acIniFile, myCounty.acCntyCode, true);
      if (iRet <= 0)
      {
         if (iRet < 0)
         {
            if (iRet == -1)
               LogMsg("***** Bad GrGrSrc file.  Please verify %s file", acIniFile);  
            else 
               LogMsg("***** Error renaming GrGr file.");  
         } else
               LogMsg("*** WARNING: No GrGr file at GrGrSrc location.  Will check GrGr folder.");  
      }

      // Load special table for GrGr
      GetIniString(myCounty.acCntyCode, "GrGr_Xref", "", acTmpFile, _MAX_PATH, acIniFile);
      iNumDeeds = LoadXrefTable(acTmpFile, (XREFTBL *)&asDeed[0], MAX_DEED_ENTRIES);

      // Loading Grgr
      iRet = Sdx_LoadGrGr(myCounty.acCntyCode);

      // Flag that GrGr is loaded
      if (!iRet)
      {
         iLoadFlag |= MERG_GRGR; 
      } else if (iRet < 0)
      {
         bGrGrAvail = false;
         if (iRet == -5)
            LogMsg("*** WARNING: GrGr file is empty.");  
      }
   }

   // Extract lien
   if (iLoadFlag & EXTR_LIEN)                      // -Xl
   {
      //iRet = Sdx_ExtrLien();
      iRet = Sdx_ExtrLien_CSV();
   }

   // Extract CHAR
   if (iLoadFlag & EXTR_ATTR)                      // -Xa
      iRet = Sdx_ConvStdChar(acCharFile);

   // Merge roll file
   if (!iRet && (iLoadFlag & LOAD_LIEN))           // -L
   {
      // Process lien date roll
      //iRet = Sdx_CreatePQ4(iSkip);
      iRet = Sdx_Load_LDR(iSkip);
   } else if (iLoadFlag & LOAD_UPDT)               // -U
   {
      //iRet = Sdx_MergePQ4(iSkip);
      iRet = Sdx_Load_Roll(iSkip);
   }

   // Check for map xref
   bool bUpdateMapXRef;
   GetIniString(myCounty.acCntyCode, "UpdateMapXRef", "", acTmpFile, _MAX_PATH, acIniFile);
   if (acTmpFile[0] == 'Y')
      bUpdateMapXRef = true;
   else
      bUpdateMapXRef = false;

   if (!iRet && (bUpdateMapXRef || (iLoadFlag & LOAD_LIEN)))
   {
      // Update condo/timeshare list
      GetIniString(myCounty.acCntyCode, "MapXRef", "", acTmpFile, _MAX_PATH, acIniFile);
      if (acTmpFile[0] > ' ')
      {
         if (!_access(acTmpFile, 0))
            iRet = Sdx_UpdateMapXRef(myCounty.acCntyCode, acTmpFile, iSkip);
         else
            LogMsgD("*** MapXRef list is missing: %s", acTmpFile);
      }
   }

   // Apply cum sale file to R01 - This option is not needed since it's already embeded in Load_Roll()
   //if (!iRet && (iLoadFlag & MERG_CSAL) )          // -Ms
   //{
      // Apply Sdx_Sale.sls to R01 file
   //   sprintf(acCSalFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "sls");
   //   iRet = ApplyCumSale(iSkip, acCSalFile, false, SALE_USE_SCUPDXFR);
   //}

   // Merge GrGr data into SSDX.R01 file
   if (!iRet && (iLoadFlag & MERG_GRGR))           // -Mg
   {
      LogMsg0("Merge %s GrGr", myCounty.acCntyCode);

      // Note: do not use Sdx_MergeGrGrFile() 7/1/2009 spn
      sprintf(acGrGrFile, acGrGrTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "SLS");
      iRet = Sdx_MergeGrGrExp(myCounty.acCntyCode, acGrGrFile);

      if (!iRet && !(iLoadFlag & LOAD_LIEN))
         iLoadFlag |= LOAD_UPDT;                   // Trigger build index
   }

   // Fix TRA
   if (!iRet && (lOptMisc & M_OPT_FIXTRA))
      iRet = PQ_FixR01(myCounty.acCntyCode, PQ_FIX_TRA, 0, true);

   // Combine sale & Xfer
   //if (!iRet && (iLoadFlag & MERG_XFER))
   //{
   //   iRet = Sdx_UpdateXfer(myCounty.acCntyCode);
   //   if (!iRet)
   //   {
   //      iRet = CombineSaleAndTransfer(myCounty.acCntyCode);
   //      if (iRet > 0)
   //         iRet = 0;
   //   }
   //}

   // Create assessor file
   //if (!iRet && (iLoadFlag & (LOAD_ASSR|UPDT_ASSR)))  // -La -Ua
   //   iRet = Sdx_CreateAssr(iSkip);

   return iRet;
}
