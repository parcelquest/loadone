#ifndef _myLoadExtrn_
#define _myLoadExtrn_
extern   char  acIniFile[], acRollFile[], acCSalFile[], acProp8Tmpl[], acXferTmpl[], acValueTmpl[], 
               acRawTmpl[], acSaleTmpl[], acGrGrTmpl[], acAttrTmpl[],sTaxOutTmpl[],sImportLogTmpl[],
               *apTokens[], acEGrGrTmpl[], acBPFile[], acLogFile[];
extern   int   iTokens, iRecLen, iApnLen, iGrGrApnLen, iRollLen, iSaleLen, iCharLen, iLoadTax,
               iLoadFlag, iNoMatch, iBadCity, iBadSuffix, iMaxLegal;
extern   bool  bDebug, bMergeOthers, bSetProp8, bConvSale, bTaxUpdtDetail, bUseConfSalePrice,
               bSendMail, bCreateUpd, bGrGrAvail, bSaleImport, bConvGrGr,
               bGrgrImport, bTaxImport, bUpdZip, bRemTmpFile, bExpSql;
extern   long  lRecCnt, lLastRecDate, lLastFileDate, lLastGrGrDate, lLastTaxFileDate,
               lToday, lToyear, lLienDate, lLDRRecCount, lDupOwners;

extern   FILE  *fdRoll;

extern   COUNTY_INFO myCounty;

// CCX
extern   char  acCChrFile[], acLienTmpl[], acAddrTmpl[], acCntyTbl[];
extern   char  acLookupTbl[], acCharFile[], acSaleFile[], acTmpPath[];
extern   int   iCSalLen;
extern   long  lOptProp8, lOptMisc;

// EDX
extern   long  lProcDate;
extern   bool  bClean, bSetLastFileDate;

// FRE
extern   char  acPubParcelFile[];

// IMP
extern   int   iApnFld;
extern   char  acExeFile[];

// KER
extern   char  acDocPath[];

// LAX
extern   char  acLienFile[], acGrGrTbl[];
extern   long  alRegCnt[], lAssrRecCnt, lOptExtr;
extern   int   iAsrRecLen;
extern   bool  m_bZipLoaded, bUseGrGr;

// NEV
extern   char  acValueFile[];

// MPA, SDX
extern   char  acMdbProvider[], acTaxDBProvider[];

// RIV, SDX, EDX
extern   long  lLienYear, lTaxYear;
extern   bool  bDupApn;

// SAC
extern   char     acESalTmpl[], acCumSalFile[];
extern   int      iNumDeeds;
extern   XREFTBL  asDeed[];
extern   bool     bClearSales, bReplValue, bEbc2Asc;

// SBT
extern   char     *asDir[], *asDir1[], *asDir2[];
extern   XREFTBL  asTRA[];
extern   int      iNumTRA;

// SBX
extern   char  acRollSale[];

// SDX
extern   char  acAsrTmpl[], acCityZip[];

// SFX
extern   char  acToday[];

// SMX
extern   char  acVacFile[];

// VEN
extern   char  acGrBkTmpl[];

// SCR   
extern   unsigned char  cDelim;
extern   int   iHdrRows;

// SOL
extern   unsigned char  cLdrSep;

// ORG, SOL, SBX, SFX, SCL, SBD
//extern   char  acAreaFile[];
#endif