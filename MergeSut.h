#ifndef _MERGESUT_H_
#define _MERGESUT_H_

#define SUT_SIZ_COUNTY_NO      		      2 
#define SUT_SIZ_ASSMT_NO		            5 
#define SUT_SIZ_STATUS		               1 
#define SUT_SIZ_ROLL_YEAR		            4 
#define SUT_SIZ_APN			               10
#define SUT_SIZ_NAME_1		               30
#define SUT_SIZ_CAREOF		               30
#define SUT_SIZ_DBA			               30
#define SUT_SIZ_LIEN_OWNER		            30
#define SUT_SIZ_MSTREET		               30
#define SUT_SIZ_MCITY_STATE		         25
#define SUT_SIZ_MZIP		                  9 
#define SUT_SIZ_SSTREET		               30
#define SUT_SIZ_SSTREET_NO		            8 
#define SUT_SIZ_LEGAL_UNIT		            3 
#define SUT_SIZ_LEGAL_BLOCK		         2 
#define SUT_SIZ_LEGAL_LOT		            3 
#define SUT_SIZ_SZIP		                  5 
#define SUT_SIZ_TRA			               5 
#define SUT_SIZ_LAND		                  9 
#define SUT_SIZ_STRUCTURES		            9 
#define SUT_SIZ_TREES		               9 
#define SUT_SIZ_FIXED_EQUIP		         9 
#define SUT_SIZ_PERS_PROP		            9 
#define SUT_SIZ_HOMEOWNERS_EXEMP	         9 
#define SUT_SIZ_MISC_EXEMP		            9 
#define SUT_SIZ_PENALTY_CODE	            2 
#define SUT_SIZ_PERS_PROP_PEN	            9 
#define SUT_SIZ_EXEMP_CODE		            2 
#define SUT_SIZ_USE_CODE		            10
#define SUT_SIZ_ACRES		               6 
#define SUT_SIZ_RECORDING_NO	            9 
#define SUT_SIZ_RECORDING_DATE	         8 
#define SUT_SIZ_BILL_DATE		            8 
#define SUT_SIZ_APPRAISAL_DATE	         8 
#define SUT_SIZ_LAST_MAINT_DATE	         8 
#define SUT_SIZ_STMT_TYPE		            1 
#define SUT_SIZ_STMT_DATE		            8 
#define SUT_SIZ_VALUE_CHANGE_CODE	      2 
#define SUT_SIZ_BUSINESS_CODE	            3 
#define SUT_SIZ_INDICATOR_CODES	         3 
#define SUT_SIZ_ASSMNT_STAT		         1 
#define SUT_SIZ_EXTEND_FLAG		         1 
#define SUT_SIZ_SUSPENSE_CODE	            1 
#define SUT_SIZ_ETAL_FLAG		            1 
#define SUT_SIZ_BYR_VAL_FL		            1 
#define SUT_SIZ_BYR_VAL_SEQ		         5 
#define SUT_SIZ_TREE_VAL_STMT_FL	         1 
#define SUT_SIZ_TREE_VAL_SEQ	            5 
#define SUT_SIZ_SUPPL_ROLL_FLAG	         1 
#define SUT_SIZ_DATE_ADDED		            8 
#define SUT_SIZ_DATE_RETIRED	            8 
#define SUT_SIZ_RETIRED_REASON	         40
#define SUT_SIZ_BOND_CODES		            12
#define SUT_SIZ_BENEFIT_CODES	            4 
#define SUT_SIZ_BENEFIT_EXE		         3 
#define SUT_SIZ_PRI_SQFT		            7 
#define SUT_SIZ_ATT_SQFT		            7 
#define SUT_SIZ_DET_SQFT		            7 
#define SUT_SIZ_FILLER1		               7 
#define SUT_SIZ_CORTAC_TBRA		         4 
#define SUT_SIZ_CALC_SHEET_STR	         9 
#define SUT_SIZ_REWORK_FLAG		         1 
#define SUT_SIZ_FORMAT		               1 
#define SUT_SIZ_INSTALL1_STAT	            1 
#define SUT_SIZ_INSTALL2_STAT	            1 
#define SUT_SIZ_BUS_SYS_IND		         4 
#define SUT_SIZ_FILLER2 		            46

#define SUT_OFF_COUNTY_NO			         1-1 
#define SUT_OFF_ASSMT_NO			         3-1 
#define SUT_OFF_STATUS			            8-1 
#define SUT_OFF_ROLL_YEAR			         9-1 
#define SUT_OFF_APN				            13-1 
#define SUT_OFF_NAME_1			            23-1 
#define SUT_OFF_CAREOF			            53-1 
#define SUT_OFF_DBA				            83-1 
#define SUT_OFF_LIEN_OWNER			         113-1 
#define SUT_OFF_MSTREET			            143-1 
#define SUT_OFF_MCITY_STATE			      173-1 
#define SUT_OFF_MZIP			               198-1 
#define SUT_OFF_SSTREET			            207-1 
#define SUT_OFF_SSTREET_NO			         237-1 
#define SUT_OFF_LEGAL_UNIT			         245-1 
#define SUT_OFF_LEGAL_BLOCK			      248-1 
#define SUT_OFF_LEGAL_LOT			         250-1 
#define SUT_OFF_SZIP			               253-1 
#define SUT_OFF_TRA				            258-1 
#define SUT_OFF_LAND			               263-1 
#define SUT_OFF_STRUCTURES			         272-1 
#define SUT_OFF_TREES			            281-1 
#define SUT_OFF_FIXED_EQUIP			      290-1 
#define SUT_OFF_PERS_PROP			         299-1 
#define SUT_OFF_HOMEOWNERS_EXEMP		      308-1 
#define SUT_OFF_MISC_EXEMP			         317-1 
#define SUT_OFF_PENALTY_CODE		         326-1 
#define SUT_OFF_PERS_PROP_PEN		         328-1 
#define SUT_OFF_EXEMP_CODE			         337-1 
#define SUT_OFF_USE_CODE			         339-1 
#define SUT_OFF_ACRES			            349-1 
#define SUT_OFF_RECORDING_NO		         355-1 
#define SUT_OFF_RECORDING_DATE		      364-1 
#define SUT_OFF_BILL_DATE			         372-1 
#define SUT_OFF_APPRAISAL_DATE		      380-1 
#define SUT_OFF_LAST_MAINT_DATE		      388-1 
#define SUT_OFF_STMT_TYPE			         396-1
#define SUT_OFF_STMT_DATE			         397-1 
#define SUT_OFF_VALUE_CHANGE_CODE		   405-1 
#define SUT_OFF_BUSINESS_CODE		         407-1 
#define SUT_OFF_INDICATOR_CODES		      410-1    // P=prop8 property
#define SUT_OFF_ASSMNT_STAT			      413-1
#define SUT_OFF_EXTEND_FLAG			      414-1
#define SUT_OFF_SUSPENSE_CODE		         415-1
#define SUT_OFF_ETAL_FLAG			         416-1
#define SUT_OFF_BYR_VAL_FL			         417-1
#define SUT_OFF_BYR_VAL_SEQ			      418-1 
#define SUT_OFF_TREE_VAL_STMT_FL		      423-1
#define SUT_OFF_TREE_VAL_SEQ		         424-1 
#define SUT_OFF_SUPPL_ROLL_FLAG		      429-1
#define SUT_OFF_DATE_ADDED			         430-1 
#define SUT_OFF_DATE_RETIRED		         438-1 
#define SUT_OFF_RETIRED_REASON		      446-1 
#define SUT_OFF_BOND_CODES			         486-1 
#define SUT_OFF_BENEFIT_CODES		         498-1 
#define SUT_OFF_BENEFIT_EXE			      502-1 
#define SUT_OFF_PRI_SQFT			         505-1    // These SQFT fields can be found
#define SUT_OFF_ATT_SQFT			         512-1    // in SUT_CHAR. Use the one in CHARS
#define SUT_OFF_DET_SQFT			         519-1    // file instead
#define SUT_OFF_FILLER1			            526-1 
#define SUT_OFF_CORTAC_TBRA			      533-1 
#define SUT_OFF_CALC_SHEET_STR		      537-1 
#define SUT_OFF_REWORK_FLAG			      546-1
#define SUT_OFF_FORMAT			            547-1
#define SUT_OFF_INSTALL1_STAT		         548-1
#define SUT_OFF_INSTALL2_STAT		         549-1
#define SUT_OFF_BUS_SYS_IND			      550-1 
#define SUT_OFF_FILLER2 			         554-1 

typedef struct _tSutRoll
{
   char County_No[SUT_SIZ_COUNTY_NO]; 
   char Assmt_No[SUT_SIZ_ASSMT_NO]; 
   char Status; 
   char Roll_Year[SUT_SIZ_ROLL_YEAR];
   char Apn[SUT_SIZ_APN];
   char Name_1[SUT_SIZ_NAME_1];
   char Careof[SUT_SIZ_CAREOF]; 
   char Dba[SUT_SIZ_DBA]; 
   char Lien_Owner[SUT_SIZ_LIEN_OWNER];
   char M_Street[SUT_SIZ_MSTREET]; 
   char M_CitySt[SUT_SIZ_MCITY_STATE];
   char M_Zip[SUT_SIZ_MZIP];
   char S_StrName[SUT_SIZ_SSTREET];
   char S_StrNum[SUT_SIZ_SSTREET_NO];
   char Legal_Unit[SUT_SIZ_LEGAL_UNIT];
   char Legal_Block[SUT_SIZ_LEGAL_BLOCK];
   char Legal_Lot[SUT_SIZ_LEGAL_LOT]; 
   char S_Zip[SUT_SIZ_SZIP]; 
   char Tra[SUT_SIZ_TRA]; 
   char Land[SUT_SIZ_LAND]; 
   char Structures[SUT_SIZ_STRUCTURES]; 
   char Trees[SUT_SIZ_TREES]; 
   char Fixed_Equip[SUT_SIZ_FIXED_EQUIP]; 
   char Pers_Prop[SUT_SIZ_PERS_PROP]; 
   char Homeowners_Exemp[SUT_SIZ_HOMEOWNERS_EXEMP];
   char Misc_Exemp[SUT_SIZ_MISC_EXEMP];
   char Penalty_Code[SUT_SIZ_PENALTY_CODE];
   char Pers_Prop_Pen[SUT_SIZ_PERS_PROP_PEN]; 
   char Exemp_Code[SUT_SIZ_EXEMP_CODE]; 
   char Use_Code[SUT_SIZ_USE_CODE]; 
   char Acres[SUT_SIZ_ACRES]; 
   char Recording_No[SUT_SIZ_RECORDING_NO];
   char Recording_Date[SUT_SIZ_RECORDING_DATE];
   char Bill_Date[SUT_SIZ_BILL_DATE];
   char Appraisal_Date[SUT_SIZ_APPRAISAL_DATE];
   char Last_Maint_Date[SUT_SIZ_LAST_MAINT_DATE];
   char Stmt_Type[SUT_SIZ_STMT_TYPE];
   char Stmt_Date[SUT_SIZ_STMT_DATE];
   char Value_Change_Code[SUT_SIZ_VALUE_CHANGE_CODE]; 
   char Business_Code[SUT_SIZ_BUSINESS_CODE]; 
   char Indicator_Codes[SUT_SIZ_INDICATOR_CODES];
   char Assmnt_Stat[SUT_SIZ_ASSMNT_STAT];
   char Extend_Flag[SUT_SIZ_EXTEND_FLAG];
   char Suspense_Code[SUT_SIZ_SUSPENSE_CODE];
   char Etal_Flag[SUT_SIZ_ETAL_FLAG];
   char Byr_Val_Fl[SUT_SIZ_BYR_VAL_FL];
   char Byr_Val_Seq[SUT_SIZ_BYR_VAL_SEQ];
   char Tree_Val_Stmt_Fl[SUT_SIZ_TREE_VAL_STMT_FL];
   char Tree_Val_Seq[SUT_SIZ_TREE_VAL_SEQ];
   char Suppl_Roll_Flag[SUT_SIZ_SUPPL_ROLL_FLAG];
   char Date_Added[SUT_SIZ_DATE_ADDED];
   char Date_Retired[SUT_SIZ_DATE_RETIRED];
   char Retired_Reason[SUT_SIZ_RETIRED_REASON];
   char Bond_Codes[SUT_SIZ_BOND_CODES];
   char Benefit_Codes[SUT_SIZ_BENEFIT_CODES];
   char Benefit_Exe[SUT_SIZ_BENEFIT_EXE];
   char Pri_Sqft[SUT_SIZ_PRI_SQFT];
   char Att_Sqft[SUT_SIZ_ATT_SQFT]; 
   char Det_Sqft[SUT_SIZ_DET_SQFT]; 
   char Filler1[SUT_SIZ_FILLER1]; 
   char Cortac_Tbra[SUT_SIZ_CORTAC_TBRA];
   char Calc_Sheet_Str[SUT_SIZ_CALC_SHEET_STR];
   char Rework_Flag[SUT_SIZ_REWORK_FLAG];
   char Format[SUT_SIZ_FORMAT];
   char Install1_Stat[SUT_SIZ_INSTALL1_STAT];
   char Install2_Stat[SUT_SIZ_INSTALL2_STAT];
   char Bus_Sys_Ind[SUT_SIZ_BUS_SYS_IND];
   char Filler2 [SUT_SIZ_FILLER2 ];
} SUT_ROLL;

// REC_SIZE = 1182
#define COFF_ROLLYR 			               1  -1
#define COFF_APN   			               5  -1
#define COFF_FMTAPN 			               15 -1
#define COFF_USECAT 			               27 -1
#define COFF_FILLER1			               42 -1
#define COFF_S_STRNUM		               43 -1
#define COFF_S_STREET		               51 -1
#define COFF_S_UNIT 			               81 -1
#define COFF_S_BLOCK			               84 -1
#define COFF_S_LOT 			               86 -1
#define COFF_USECODE1		               89 -1
#define COFF_USECODE2		               91 -1
#define COFF_USECODE3		               94 -1
#define COFF_USETYPE			               97 -1
#define COFF_FILLER2			               127-1
#define COFF_ACREAGE			               128-1
#define COFF_MH_DECAL		               135-1
#define COFF_YRBLT 			               145-1
#define COFF_CLASS1 			               149-1
#define COFF_FILLER3			               150-1
#define COFF_CLASS2 			               151-1
#define COFF_CLASS3 			               155-1
#define COFF_CONDITION		               156-1
#define COFF_FILLER4			               166-1
#define COFF_STORIES			               167-1
#define COFF_FILLER5			               169-1
#define COFF_UNITS			               170-1
#define COFF_FILLER6			               173-1
#define COFF_BEDS 			               174-1
#define COFF_FILLER7			               177-1
#define COFF_BATHS			               178-1
#define COFF_FILLER8			               181-1
#define COFF_HBATH			               182-1
#define COFF_HEAT_COOL		               185-1
#define COFF_FILLER9			               200-1
#define COFF_PSTOVES			               201-1
#define COFF_FILLER10		               203-1
#define COFF_FIREPLACES		               204-1
#define COFF_FILLER11		               207-1
#define COFF_WSTOVES			               208-1
#define COFF_FILLER12		               210-1
#define COFF_PRI_SQFT		               211-1       // BldgSqft
#define COFF_FILLER13		               218-1
#define COFF_GARAGES			               219-1
#define COFF_FILLER14		               222-1
#define COFF_GAR_SQFT		               223-1
#define COFF_FILLER15		               230-1
#define COFF_CARPORT 		               231-1
#define COFF_FILLER16		               234-1
#define COFF_CARPORT_SQFT	               235-1
#define COFF_FILLER17		               242-1
#define COFF_COV_AREA_SQFT	               243-1
#define COFF_FILLER18		               250-1
#define COFF_CANOPY_SQFT	               251-1
#define COFF_FILLER19		               258-1
#define COFF_BRZWAY_SQFT	               259-1
#define COFF_FILLER20		               266-1
#define COFF_STORAGE_SQFT	               267-1
#define COFF_FILLER21		               274-1
#define COFF_REC_ROOM_SQFT		            275-1
#define COFF_FILLER22		               282-1
#define COFF_LAUNDRY_ROOM_SQFT            283-1
#define COFF_FILLER23		               290-1
#define COFF_OTH_ROOM1_SQFT               291-1
#define COFF_OTH_ROOM1_DESC               298-1
#define COFF_FILLER24		               313-1
#define COFF_OTH_ROOM2_SQFT               314-1
#define COFF_OTH_ROOM2_DESC               321-1
#define COFF_LAUNDRY_HOOKUP               336-1
#define COFF_POOL		                     339-1
#define COFF_SPA		                     354-1
#define COFF_TENNIS	                     357-1
#define COFF_WATER	                     360-1
#define COFF_SEWER	                     375-1
#define COFF_ZONING                       385-1
#define COFF_NBHD_CODE                    389-1
#define COFF_DBA_NAME                     394-1
#define COFF_CUR_USE	                     424-1
#define COFF_NOTES	                     454-1
#define COFF_FILLER25                     854-1
#define COFF_OTH_BLDG_SQFT                855-1
#define COFF_OTH_BLDG_NOTES               862-1

#define CSIZ_ROLLYR                       4
#define CSIZ_APN                          10
#define CSIZ_FMTAPN                       12
#define CSIZ_USECAT                       15
#define CSIZ_FILLER1                      1
#define CSIZ_S_STRNUM                     8
#define CSIZ_S_STREET                     30
#define CSIZ_S_UNIT                       3
#define CSIZ_S_BLOCK                      2
#define CSIZ_S_LOT                        3
#define CSIZ_USECODE1                     2
#define CSIZ_USECODE2                     3
#define CSIZ_USECODE3                     3
#define CSIZ_USETYPE                      30
#define CSIZ_FILLER2                      1
#define CSIZ_ACREAGE                      7
#define CSIZ_MH_DECAL                     10
#define CSIZ_YRBLT                        4
#define CSIZ_CLASS1                       1
#define CSIZ_FILLER3                      1
#define CSIZ_CLASS2                       4
#define CSIZ_CLASS3                       1
#define CSIZ_CONDITION                    10
#define CSIZ_FILLER4                      1
#define CSIZ_STORIES                      2
#define CSIZ_FILLER5                      1
#define CSIZ_UNITS                        3
#define CSIZ_FILLER6                      1
#define CSIZ_BEDS                         3
#define CSIZ_FILLER7                      1
#define CSIZ_BATHS                        3
#define CSIZ_FILLER8                      1
#define CSIZ_HBATH                        3
#define CSIZ_HEAT_COOL                    15
#define CSIZ_FILLER9                      1
#define CSIZ_PSTOVES                      2
#define CSIZ_FILLER10                     1
#define CSIZ_FIREPLACES                   3
#define CSIZ_FILLER11                     1
#define CSIZ_WSTOVES                      2
#define CSIZ_FILLER12                     1
#define CSIZ_PRI_SQFT                     7
#define CSIZ_FILLER13                     1
#define CSIZ_GARAGES                      3
#define CSIZ_FILLER14                     1
#define CSIZ_GAR_SQFT                     7
#define CSIZ_FILLER15                     1
#define CSIZ_CARPORT                      3
#define CSIZ_FILLER16                     1
#define CSIZ_CARSQFT                      7
#define CSIZ_FILLER17                     1
#define CSIZ_COV_AREA_SQFT                7
#define CSIZ_FILLER18                     1
#define CSIZ_CANOPY_SQFT                  7
#define CSIZ_FILLER19                     1
#define CSIZ_BRZWAY_SQFT                  7
#define CSIZ_FILLER20                     1
#define CSIZ_STORAGE_SQFT                 7
#define CSIZ_FILLER21                     1
#define CSIZ_REC_ROOM_SQFT                7
#define CSIZ_FILLER22                     1
#define CSIZ_LAUNDRY_ROOM_SQFT            7
#define CSIZ_FILLER23                     1
#define CSIZ_OTH_ROOM1_SQFT               7
#define CSIZ_OTH_ROOM1_DESC               15
#define CSIZ_FILLER24                     1
#define CSIZ_OTH_ROOM2_SQFT               7
#define CSIZ_OTH_ROOM2_DESC               15
#define CSIZ_LAUNDRY_HOOKUP               3
#define CSIZ_POOL                         15
#define CSIZ_SPA                          3
#define CSIZ_TENNIS                       3
#define CSIZ_WATER                        15
#define CSIZ_SEWER                        10
#define CSIZ_ZONING                       4
#define CSIZ_NBHD_CODE                    5
#define CSIZ_DBA_NAME                     30
#define CSIZ_CUR_USE                      30
#define CSIZ_NOTES                        400
#define CSIZ_FILLER25                     1
#define CSIZ_OTH_BLDG_SQFT                7
#define CSIZ_OTH_BLDG_NOTES               320

typedef struct _tSutChar
{
   char Roll_Year[CSIZ_ROLLYR];
   char Apn[CSIZ_APN];
   char FmtApn[CSIZ_FMTAPN];
   char UseCat[CSIZ_USECAT];
   char Filler1[CSIZ_FILLER1];
   char S_StrNum[CSIZ_S_STRNUM];
   char S_StrName[CSIZ_S_STREET];
   char S_Unit[CSIZ_S_UNIT];
   char S_Block[CSIZ_S_BLOCK];
   char S_Lot[CSIZ_S_LOT];
   char Use_Code1[CSIZ_USECODE1];
   char Use_Code2[CSIZ_USECODE2];
   char Use_Code3[CSIZ_USECODE3];
   char Use_Type[CSIZ_USETYPE];
   char Filler2 [CSIZ_FILLER2];
   char Acres[CSIZ_ACREAGE];
   char MH_Decal[CSIZ_MH_DECAL];
   char YearBuilt[CSIZ_YRBLT];
   char Class1[CSIZ_CLASS1];
   char Filler3[CSIZ_FILLER3];
   char Class2[CSIZ_CLASS2];
   char Class3[CSIZ_CLASS3];
   char Condition[CSIZ_CONDITION];
   char Filler4[CSIZ_FILLER4];
   char Stories[CSIZ_STORIES];
   char Filler5[CSIZ_FILLER5];
   char Units[CSIZ_UNITS];
   char Filler6[CSIZ_FILLER6];
   char Beds[CSIZ_BEDS];
   char Filler7[CSIZ_FILLER7];
   char Baths[CSIZ_BATHS];
   char Filler8[CSIZ_FILLER8];
   char HBath[CSIZ_HBATH];
   char Heat_Cool[CSIZ_HEAT_COOL];
   char Filler9[CSIZ_FILLER9];
   char PStoves[CSIZ_PSTOVES];
   char Filler10[CSIZ_FILLER10];
   char FirePlaces[CSIZ_FIREPLACES];
   char Filler11[CSIZ_FILLER11];
   char WStoves[CSIZ_WSTOVES];
   char Filler12[CSIZ_FILLER12];
   char BldgSqft[CSIZ_PRI_SQFT];
   char Filler13[CSIZ_FILLER13];
   char Garages[CSIZ_GARAGES];
   char Filler14[CSIZ_FILLER14];
   char GarSqft[CSIZ_GAR_SQFT];
   char Filler15[CSIZ_FILLER15];
   char CarPorts[CSIZ_CARPORT];
   char Filler16[CSIZ_FILLER16];
   char CarSqft[CSIZ_GAR_SQFT];
   char Filler17[CSIZ_FILLER17];
   char CoverArea_Sqft[CSIZ_COV_AREA_SQFT];
   char Filler18[CSIZ_FILLER18];
   char Canopy_Sqft[CSIZ_CANOPY_SQFT];
   char Filler19[CSIZ_FILLER19];
   char Brzway_Sqft[CSIZ_BRZWAY_SQFT];
   char Filler20[CSIZ_FILLER20];
   char Storage_Sqft[CSIZ_STORAGE_SQFT];
   char Filler21[CSIZ_FILLER21];
   char Rec_Room_Sqft[CSIZ_REC_ROOM_SQFT];
   char Filler22[CSIZ_FILLER22];
   char Laundry_Room_Sqft[CSIZ_LAUNDRY_ROOM_SQFT];
   char Filler23[CSIZ_FILLER23];
   char OthRoom1_Sqft[CSIZ_OTH_ROOM1_SQFT];
   char OthRoom1_Desc[CSIZ_OTH_ROOM1_DESC];
   char Filler24[CSIZ_FILLER24];
   char OthRoom2_Sqft[CSIZ_OTH_ROOM2_SQFT];
   char OthRoom2_Desc[CSIZ_OTH_ROOM2_DESC];
   char Laundry_Hookup[CSIZ_LAUNDRY_HOOKUP];
   char Pool[CSIZ_POOL];
   char Spa[CSIZ_SPA];
   char Tennis[CSIZ_TENNIS];
   char Water[CSIZ_WATER];
   char Sewer[CSIZ_SEWER];
   char Zoning[CSIZ_ZONING];
   char Nbhd_Code[CSIZ_NBHD_CODE];
   char Dba_Name[CSIZ_DBA_NAME];
   char Cur_Use[CSIZ_CUR_USE];
   char Notes[CSIZ_NOTES];
   char Filler25[CSIZ_FILLER25];
   char OthBldg_Sqft[CSIZ_OTH_BLDG_SQFT];
   char OthBldg_Notes[CSIZ_OTH_BLDG_NOTES];
   char CrLf[2];
} SUT_CHAR;

// Secured Assessment Detail layout
#define OFF_SUT_SA_ROLLYEAR              1-1
#define OFF_SUT_SA_ASSMT_NO              5-1
#define OFF_SUT_SA_ASSMT_SUF             10-1
#define OFF_SUT_SA_RECTYPE               11-1
#define OFF_SUT_SA_ASSMTCODE             13-1
#define OFF_SUT_SA_APN                   15-1
#define OFF_SUT_SA_TAXAMT1               24-1
#define OFF_SUT_SA_TAXAMT2               35-1
#define OFF_SUT_SA_TAXRATE               46-1

#define SIZ_SUT_SA_ROLLYEAR              4
#define SIZ_SUT_SA_ASSMT_NO              5
#define SIZ_SUT_SA_ASSMT_SUF             1
#define SIZ_SUT_SA_RECTYPE               1
#define SIZ_SUT_SA_ASSMTCODE             2
#define SIZ_SUT_SA_APN                   10
#define SIZ_SUT_TAXAMT                   11
#define SIZ_SUT_TAXRATE                  9

typedef struct _SutAssmt
{
   char  RollYear         [SIZ_SUT_SA_ROLLYEAR];
   char  Assmt_No         [SIZ_SUT_SA_ASSMT_NO];
   char  Assm_Suf         [SIZ_SUT_SA_ASSMT_SUF];
   char  RecType          [SIZ_SUT_SA_RECTYPE];    // 1=tax, 2=bond, 3=direct assmnt
   char  Assmt_Code       [SIZ_SUT_SA_ASSMTCODE];
   char  Apn              [SIZ_SUT_SA_APN];
   char  TaxAmt1          [SIZ_SUT_TAXAMT];
   char  TaxAmt2          [SIZ_SUT_TAXAMT];
   char  TaxRate          [SIZ_SUT_TAXRATE];
   char  CrLf[2];
} SUT_DETAIL;

// Secured Paid/Unpaid layout
#define OFF_SUT_PU_ROLLYEAR              1
#define OFF_SUT_PU_ASSMT_NO              5
#define OFF_SUT_PU_APN                   11
#define OFF_SUT_PU_TRA                   21
#define OFF_SUT_PU_NAME                  26
#define OFF_SUT_PU_CAREOF                56
#define OFF_SUT_PU_DBA                   86
#define OFF_SUT_PU_LIENDATE_OWNER        116
#define OFF_SUT_PU_M_ADDR                146
#define OFF_SUT_PU_M_CITYST              176
#define OFF_SUT_PU_M_ZIP                 201
#define OFF_SUT_PU_M_ZIP4                206
#define OFF_SUT_PU_USECODE               210
#define OFF_SUT_PU_S_STRNAME             220
#define OFF_SUT_PU_S_STRNUM              250
#define OFF_SUT_PU_S_UNIT                258
#define OFF_SUT_PU_S_BLOCK               261
#define OFF_SUT_PU_S_LOT                 263
#define OFF_SUT_PU_S_ZIP                 266
#define OFF_SUT_PU_REMINDER_FLG          271
#define OFF_SUT_PU_TBRA                  272
#define OFF_SUT_PU_CUST                  276
#define OFF_SUT_PU_LOAN                  283
#define OFF_SUT_PU_RV_LAND               309
#define OFF_SUT_PU_RV_IMPR               319
#define OFF_SUT_PU_RV_PP                 329
#define OFF_SUT_PU_RV_FIXTR              339
#define OFF_SUT_PU_RV_OTHER              349
#define OFF_SUT_PU_HOEXE                 359
#define OFF_SUT_PU_MISCEXE               369
#define OFF_SUT_PU_EXE_CODE              379    // 44=HO
#define OFF_SUT_PU_EXE_VET               381
#define OFF_SUT_PU_MISC                  382
#define OFF_SUT_PU_ACRES                 384
#define OFF_SUT_PU_DEFAUT_DATE           392
#define OFF_SUT_PU_DEFAULT_SEQ           400
#define OFF_SUT_PU_SEG_FLG               404
#define OFF_SUT_PU_GROSS_TAX             405
#define OFF_SUT_PU_HO_REF                416
#define OFF_SUT_PU_MISC_REF              427
#define OFF_SUT_PU_NET_TAX               438
#define OFF_SUT_PU_TOTAL_BOND            449    // Total Bond
#define OFF_SUT_PU_TOTAL_DIR             460    // Total Direct Assessment
#define OFF_SUT_PU_TOTAL_PEN             471    // Total Penalty
#define OFF_SUT_PU_TOTAL_RATE            481
// Starting 9/2017 TOTAL_RATE increases one digit
#define OFF_SUT_PU_INST1_STATUS          489+1    // (P)aid, (C)ancel, (N)o tax, BLANK=not paid
#define OFF_SUT_PU_INST1_TAXAMT          490+1
#define OFF_SUT_PU_INST1_PENAMT          501+1    // PenAmt if not paid on due date
#define OFF_SUT_PU_INST1_DUE_DATE        511+1    // Delq after this date
#define OFF_SUT_PU_INST1_PAID_DATE       519+1    // Blank if not paid
#define OFF_SUT_PU_INST1_BATCH           527+1
#define OFF_SUT_PU_INST2_STATUS          533+1
#define OFF_SUT_PU_INST2_TAXAMT          534+1
#define OFF_SUT_PU_INST2_PENAMT          545+1
#define OFF_SUT_PU_INST2_COST            555+1
#define OFF_SUT_PU_INST2_DUE_DATE        561+1
#define OFF_SUT_PU_INST2_PAID_DATE       569+1
#define OFF_SUT_PU_INST2_BATCH           577+1
#define OFF_SUT_PU_CORR_TYPE             583+1
#define OFF_SUT_PU_BILL_ADJ              584+1
#define OFF_SUT_PU_ORIG_APN              585+1
#define OFF_SUT_PU_DEMAND_STMT_FLG       595+1
#define OFF_SUT_PU_DIRECT_ASSMT_RMVD     605+1
#define OFF_SUT_PU_LSCAPE                607+1
#define OFF_SUT_PU_ST_LTG_SUBDIV         609+1
#define OFF_SUT_PU_LAST_ACT_DATE         611+1
#define OFF_SUT_PU_PEN_CODE              619+1
#define OFF_SUT_PU_PEND_APPR             621+1
#define OFF_SUT_PU_BANKRUPT_NBR          622+1
#define OFF_SUT_PU_CNTY                  637+1
#define OFF_SUT_PU_REMARK                639+1
#define OFF_SUT_PU_FILLER                669+1

#define SIZ_SUT_PU_ROLLYEAR              4
#define SIZ_SUT_PU_ASSMT_NO              6
#define SIZ_SUT_PU_APN                   10
#define SIZ_SUT_PU_TRA                   5
#define SIZ_SUT_PU_NAME                  30
#define SIZ_SUT_PU_CAREOF                30
#define SIZ_SUT_PU_DBA                   30
#define SIZ_SUT_PU_LIENDATE_OWNER        30
#define SIZ_SUT_PU_M_ADDR                30
#define SIZ_SUT_PU_M_CITYST              25
#define SIZ_SUT_PU_M_ZIP                 5
#define SIZ_SUT_PU_M_ZIP4                4
#define SIZ_SUT_PU_USECODE               10
#define SIZ_SUT_PU_S_STRNAME             30
#define SIZ_SUT_PU_S_STRNUM              8
#define SIZ_SUT_PU_S_UNIT                3
#define SIZ_SUT_PU_S_BLOCK               2
#define SIZ_SUT_PU_S_LOT                 3
#define SIZ_SUT_PU_S_ZIP                 5
#define SIZ_SUT_PU_REMINDER_FLG          1
#define SIZ_SUT_PU_TBRA                  4
#define SIZ_SUT_PU_CUST                  7
#define SIZ_SUT_PU_LOAN                  26
#define SIZ_SUT_PU_RV_LAND               10
#define SIZ_SUT_PU_RV_IMPR               10
#define SIZ_SUT_PU_RV_PP                 10
#define SIZ_SUT_PU_RV_FIXTR              10
#define SIZ_SUT_PU_RV_OTHER              10
#define SIZ_SUT_PU_HOEXE                 10
#define SIZ_SUT_PU_MISCEXE               10
#define SIZ_SUT_PU_EXE_CODE              2
#define SIZ_SUT_PU_EXE_VET               1
#define SIZ_SUT_PU_MISC                  2
#define SIZ_SUT_PU_ACRES                 8
#define SIZ_SUT_PU_DEFAULT_SEQ           4
#define SIZ_SUT_PU_SEG_FLG               1
#define SIZ_SUT_PU_HO_REF                11
#define SIZ_SUT_PU_MISC_REF              11
#define SIZ_SUT_PU_TOTAL_BOND            11
#define SIZ_SUT_PU_TOTAL_DIR             11
#define SIZ_SUT_PU_TOTAL_PEN             10
#define SIZ_SUT_PU_TOTAL_RATE            9   // Increase to 9 in 9/2017
#define SIZ_SUT_PU_INST1_STATUS          1
#define SIZ_SUT_PU_INST1_PENAMT          10
#define SIZ_SUT_PU_INST1_BATCH           6
#define SIZ_SUT_PU_INST2_STATUS          1
#define SIZ_SUT_PU_INST2_PENAMT          10
#define SIZ_SUT_PU_INST2_COST            6
#define SIZ_SUT_PU_INST2_BATCH           6
#define SIZ_SUT_PU_CORR_TYPE             1
#define SIZ_SUT_PU_BILL_ADJ              11
#define SIZ_SUT_PU_ORIG_APN              10
#define SIZ_SUT_PU_DEMAND_STMT_FLG       1
#define SIZ_SUT_PU_DIRECT_ASSMT_RMVD     1
#define SIZ_SUT_PU_LSCAPE                2
#define SIZ_SUT_PU_ST_LTG_SUBDIV         2
#define SIZ_SUT_PU_PEN_CODE              2
#define SIZ_SUT_PU_PEND_APPR             1
#define SIZ_SUT_PU_BANKRUPT_NBR          15
#define SIZ_SUT_PU_CNTY                  2
#define SIZ_SUT_PU_REMARK                30
#define SIZ_SUT_PU_FILLER                17
#define SIZ_SUT_PU_DATE                  8

typedef struct _SutPaid
{
   char  RollYear         [SIZ_SUT_PU_ROLLYEAR         ];
   char  Assmt_No         [SIZ_SUT_PU_ASSMT_NO         ];
   char  Apn              [SIZ_SUT_PU_APN              ];
   char  TRA              [SIZ_SUT_PU_TRA              ];
   char  Name             [SIZ_SUT_PU_NAME             ];
   char  Careof           [SIZ_SUT_PU_CAREOF           ];
   char  Dba              [SIZ_SUT_PU_DBA              ];
   char  LienDate_Owner   [SIZ_SUT_PU_LIENDATE_OWNER   ];
   char  M_Addr           [SIZ_SUT_PU_M_ADDR           ];
   char  M_CitySt         [SIZ_SUT_PU_M_CITYST         ];
   char  M_Zip            [SIZ_SUT_PU_M_ZIP            ];
   char  M_Zip4           [SIZ_SUT_PU_M_ZIP4           ];
   char  UseCode          [SIZ_SUT_PU_USECODE          ];
   char  S_StrName        [SIZ_SUT_PU_S_STRNAME        ];
   char  S_StrNum         [SIZ_SUT_PU_S_STRNUM         ];
   char  S_Unit           [SIZ_SUT_PU_S_UNIT           ];
   char  S_Block          [SIZ_SUT_PU_S_BLOCK          ];
   char  S_Lot            [SIZ_SUT_PU_S_LOT            ];
   char  S_Zip            [SIZ_SUT_PU_S_ZIP            ];
   char  Reminder_Flg     [SIZ_SUT_PU_REMINDER_FLG     ];
   char  Tbra             [SIZ_SUT_PU_TBRA             ];
   char  Cust             [SIZ_SUT_PU_CUST             ];
   char  Loan             [SIZ_SUT_PU_LOAN             ];
   char  Rv_Land          [SIZ_SUT_PU_RV_LAND          ];
   char  Rv_Impr          [SIZ_SUT_PU_RV_IMPR          ];
   char  Rv_Pp            [SIZ_SUT_PU_RV_PP            ];
   char  Rv_Fixtr         [SIZ_SUT_PU_RV_FIXTR         ];
   char  Rv_Other         [SIZ_SUT_PU_RV_OTHER         ];
   char  HoExe            [SIZ_SUT_PU_HOEXE            ];
   char  MiscExe          [SIZ_SUT_PU_MISCEXE          ];
   char  Exe_Code         [SIZ_SUT_PU_EXE_CODE         ];
   char  Exe_Vet          [SIZ_SUT_PU_EXE_VET          ];
   char  Misc             [SIZ_SUT_PU_MISC             ];
   char  Acres            [SIZ_SUT_PU_ACRES            ];
   char  DefaultDate      [SIZ_SUT_PU_DATE             ];
   char  Default_Seq      [SIZ_SUT_PU_DEFAULT_SEQ      ];
   char  Seg_Flg          [SIZ_SUT_PU_SEG_FLG          ];
   char  Gross_Tax        [SIZ_SUT_TAXAMT              ];
   char  Ho_Ref           [SIZ_SUT_PU_HO_REF           ];
   char  Misc_Ref         [SIZ_SUT_PU_MISC_REF         ];
   char  Net_Tax          [SIZ_SUT_TAXAMT              ];
   char  Total_Bond       [SIZ_SUT_PU_TOTAL_BOND       ];
   char  Total_Dir        [SIZ_SUT_PU_TOTAL_DIR        ];
   char  Total_Pen        [SIZ_SUT_PU_TOTAL_PEN        ];
   char  Total_Rate       [SIZ_SUT_PU_TOTAL_RATE       ];
   char  Inst1_Status     [SIZ_SUT_PU_INST1_STATUS     ];
   char  Inst1_TaxAmt     [SIZ_SUT_TAXAMT              ];
   char  Inst1_PenAmt     [SIZ_SUT_PU_INST1_PENAMT     ];
   char  Inst1_DueDate    [SIZ_SUT_PU_DATE             ];
   char  Inst1_PaidDate   [SIZ_SUT_PU_DATE             ];
   char  Inst1_Batch      [SIZ_SUT_PU_INST1_BATCH      ];
   char  Inst2_Status     [SIZ_SUT_PU_INST2_STATUS     ];
   char  Inst2_TaxAmt     [SIZ_SUT_TAXAMT              ];
   char  Inst2_PenAmt     [SIZ_SUT_PU_INST2_PENAMT     ];
   char  Inst2_Cost       [SIZ_SUT_PU_INST2_COST       ];
   char  Inst2_DueDate    [SIZ_SUT_PU_DATE             ];
   char  Inst2_PaidDate   [SIZ_SUT_PU_DATE             ];
   char  Inst2_Batch      [SIZ_SUT_PU_INST2_BATCH      ];
   char  Corr_Type        [SIZ_SUT_PU_CORR_TYPE        ];
   char  Bill_Adj         [SIZ_SUT_PU_BILL_ADJ         ];
   char  Orig_Apn         [SIZ_SUT_PU_ORIG_APN         ];
   char  Demand_Stmt_Flg  [SIZ_SUT_PU_DEMAND_STMT_FLG  ];
   char  Direct_Assmt_Rmvd[SIZ_SUT_PU_DIRECT_ASSMT_RMVD];
   char  LandScape        [SIZ_SUT_PU_LSCAPE           ];
   char  St_Ltg_Subdiv    [SIZ_SUT_PU_ST_LTG_SUBDIV    ];
   char  Last_Act_Date    [SIZ_SUT_PU_DATE             ];
   char  Pen_Code         [SIZ_SUT_PU_PEN_CODE         ];
   char  Pend_Appr        [SIZ_SUT_PU_PEND_APPR        ];
   char  Bankrupt_Nbr     [SIZ_SUT_PU_BANKRUPT_NBR     ];
   char  Cnty             [SIZ_SUT_PU_CNTY             ];
   char  Remark           [SIZ_SUT_PU_REMARK           ];
   char  Filler           [SIZ_SUT_PU_FILLER           ];
} SUT_PAID;

#endif