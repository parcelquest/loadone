GRGR
1) What to do when one doc has two tax amt, one is Dtt and one is City tax?
2) Use only doc with dtt?


LAX Notes: LAX sends us roll file monthly and sale file quarterly (LA city).  We extract
           sale monthly even if that is old file and let MergeAdr take care the rest.

LAX Processes:
1. Load sale file and update cumulative file. (-Ls)
2. Load/update roll file with sale update. (-U)
3. Split into regions (-Xr)
4. Load/update assessor file with sale update

To be done:
1. Title & salutation



1) DocKey?
2) Should we use RecDate or Sale1Date? See 2004018029

3) NAME
   - If Name1_1 ends with "AND", append NAME1_2 to it.
   - If Name1_1 doesn't end with 'AND', ignore Name1_2.
   - If Name1_2 ends with 'AND', drop it
   - ???? Keep Name_2 as separate name even they may have the same last name
   - Convert space before comma to '_' then append name1_2 to name 1_1 if there is
     an 'AND' or '&' at the end of Name1_1.
   - If no comma found in Name1_1 or Name1_2, assume first word is last name
   - If Name1_2 is blank and Name1_1 has 'AND' at the end, strip off last 'AND' from Name1_1
   - Use ParseOwnerName4 and ParseOwnerName5 to handle this case.
   - Replace '_' with space after parsing.

     Ex:
     GREGG,ROLAND S,JR AND                                                                                GREGG,NORMA D
     BALDWIN,DAVID P AND                                                                                  HODGE,THANANYA
     THUY TYSON BAO PHAM AND         CADAVONA,KAREN S
     CILVA,JAMES L AND               EVELYN L
     MORGA,JOHN J AND                DE MIRA,MILKA
     DANCESCU,THEODORE AND           ANGELESCU AIDA
     DAFESH,ISAUM S AND              HASELTINE DAFESH,KARIN M
     HERNANDEZ,ALEX P AND TANYA A AND                                                                     WHETSTINE,DARLENE
     BOGDANOWICZ,JIM AND             BOGDANOWICZ,JILL
     WEYER,THOMAS E AND              D ACHILLE WEYER,MARY A
     FRIEDLANDER BERKOWITZ, BARBARA  AND BERKOWITZ,STEVEN
     HING CHEONG LAI AND             HUI CHEN LIU
     CUONG HUY VO AND
     FAITH BAPTIST CHURCH OF         CANOGA PARK
     TAKENOSHITA,NOBUYOSHI AND       MEIKO AND                                                            OKAZAKI,DANIEL M AND YUMIKO A

*Fernando check
2007026022 CHALSMA,HOWARD W AND            SMITH,TACY L AND                                                     CHALSMA,MELISSA C
2012001028 TAN,BIENVENIDO I JR AND         SCHULZ TAN,DAWN L
2012001079 LUNA,JOSE C AND NORMA AND                                                                            VALADEZ,XAVIER AND RAQUEL
2012012007 H D B FAMILY PROPERTIES ET AL                                                                        BERQUIST,H TR BERQUIST TRUST


   - If SPECIAL_NAME_TYPE="C/O", copy SPECIAL_NAME to CareOf
   -

OWNER_CODE
   11716
3  184235
4  1277
C  61
D  30
G  25
H  290
I  7
J  64
K  1060
L  8
M  20
N  3
P  177
S  367
T  38
V  28
W  1
X  189
Y  37
Z  18

DocKey in roll file
   11716
0  3
1  14558
2  128
3  161
4  219
5  22
6  30
7  21
8  690
9  140
A  87008
B  10
C  285
D  229
F  1765
G  90
I  42
K  190
M  6872
N  12
O  16
P  405
Q  9
R  16261
S  16238
T  26
U  33967
V  60
W  3534
Y  4186
Z  758

DocType in Sale file
   3149
2  1
4  646
B  268687            Quit Claim
C  2
D  882
H  48149
J  8
K  528
L  2753
P  1719
S  1980
U  33
V  5588
W  754
X  464
Y  250023
Z  23

DTT Type in Sale
   3183
1  544588
2  17307
3  5572
4  3050
5  1872
6  1302
7  891
8  687
9  6937
