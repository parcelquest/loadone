/**************************************************************************
 *
 * Notes:
 *    1) SALELISTS.TXT file contains last 2 years of transactions only
 *
 *    2) Available options:
 *       -CLAX -L          load lien
 *       -CLAX -La         load lien for assr
 *       -CLAX -Us -Xr     update PQ, sale history, and extract region
 *       -CLAX -Ua         update assr
 *       -CLAX -Xl         extract lien
 *       -CLAX -G          process GrGr data.  This option is on hold until
 *                         we know what the county will send us.
 *       -CLAX -U -Mg      Update roll with merge GrGr.
 *       -Xd               Extract legal
 *
 *    3) LAX has not finalize on GrGr format for us yet.  Until then, we can
 *       only use following command:
 *       Lien date:     -CLAX -L -O -Xl -Us|-Ms -Xr -La -Mr -Mg
 *       Normal update: -CLAX -U -O -Us|-Ms -Xr -Ua -G|-Mg -Mr
 *
 * 02/03/2007 1.4.2     Modify sale update logic.
 * 03/12/2007 1.4.4     Change sale update logic back to previous version with
 *                      slight modification
 * 03/21/2007 1.4.5.1   Fix bug that copies strname over suffix
 * 07/25/2007 1.4.21    Fix parseMaseg() by skipping design 2700 (parking lot)
 *                      and 3800 (???) segments.  This matches with the BldgSqft
 *                      calculation from the county.
 * 08/01/2007 1.4.23    Fixup sale update to pull sale from all sources (Saleslist.txt,
 *                      roll file, and grgr).  Order of sales used is sale file,
 *                      roll data, then grgr.
 * 12/11/2007 1.4.33.1  Remove first character of Name1 if it is not alpha numeric.
 * 01/04/2008 1.5.2     Add -Ll option to merge Lot Area file.  This should be used
 *                      in LDR or county rebuild only.
 * 03/14/2008 1.5.6     Use standard function to update usecode
 * 03/20/2008 1.5.7     Make all usecode ending with 'V' vacant.
 * 03/24/2008 1.5.7.2   Format IMAPLINK.
 * 06/02/2008 1.6.1     Process situs addr in regular monthly update
 * 07/18/2008 8.0.3     Skip test record in Lax_CreateAssr()
 * 10/04/2008 8.4.1     Modify Lax_LoadGrGr() to process new GrGr format. Rename myConn
 *                      to dbLoadConn.
 * 11/17/2008 8.4.3     Unzip AIN file before processing GrGr.  Recorder may not send us 
 *                      data in pair.  So we may have to monitor and copy back the old CSV
 *                      file to process new XML data.
 * 11/18/2008 8.4.3.1   Modify Lax_LoadGrGrXml() to fix bug where duplicate DocNum occurs.
 * 12/13/2008 8.4.6.1   Fix bug that translates Central Heat to Floor in parseMaseg() function.
 * 12/05/2008 8.4.7     Correct Heating translation in parseMaseg().
 * 12/11/2008 8.5.1     Make StrNum jeft justified
 * 12/22/2008 8.5.3     Modify Lax_LoadGrGr() to support new RDRENT format.  LoadGrGr now
 *                      take in both old and new format.
 * 01/07/2009 8.5.5     Modify LoadGrGr() to support new RDRENT format.  Since 11/2008 we
 *                      have received three different formats.
 * 01/26/2008 8.5.8     Create Lax_FixTRA() to translate TRARef to actual TRA.  Data 
 *                      is avail. in both SQL and mdb file.  Use SQL for speed.
 * 01/27/2009 8.5.8.1   Force build index when load GrGr successful.
 * 01/29/2009 8.5.9.2   Back out TRA code change.  Leave TRA as it is provided by county.
 *                      Format it to 5 digit.
 * 05/08/2009 8.8       Set full exemption flag
 * 06/18/2009 8.8.5     Modify Lax_MergeRoll() to populate Fixture, PP_Val, and Prop8 flag.
 * 06/29/2009 9.1.0     Fix Lax_CreateLienRec() to use in 2009 LDR production.
 * 10/29/2009 9.3.1.3   Fix Situs Unit# doesn't show up.
 * 01/22/2009 9.3.8.2   Add function Lax_ExtrProp8() to extract Prop8.
 * 02/06/2010 9.4.0     Extending LEGAL to LEGAL1 & LEGAL2 (240 bytes).
 * 02/23/2010 9.4.3     Fix CARE_OF issue.
 * 04/07/2010 9.5.2     Modify Lax_MergeOwner() to use Buyer if Name1 is not available.
 * 05/13/2010 9.5.8     Reformat DOCNUM to make data from sale file, roll file, and GRGR file
 *                      the same format 9999999.  Strip off year from GRGR.  Move extrGrGrDoc()
 *                      from LoadOne.cpp to here since it is specific to LAX.  Modify Lax_UpdateXferDoc()
 *                      to update Sale1 only if there is sale price avoiding transfer only transactions.
 *                      Add -Ps option to purge old sales when needed.  Modify Lax_LoadGrGrXml()
 *                      to remove all apostrophes from names in XML files.  Remove Lax_UpdateSaleHist()
 *                      and use Lax_ConvertSale() to update cum sale file. History sale now in
 *                      512-byte standard format.
 * 05/18/2010 9.6.1     Modify LoadGrGrXml() to fix SalePrice and inserting new record without
 *                      check for duplicate.  Change LoadGrGr() to use Recording Date from XML file
 *                      since it contains data for one day only.  This speeds up processing.
 * 05/31/2010 9.6.2.1   Modify CreateLaxRoll() to use CSalFile (new format) as sale input instead of SaleCum (old format).
 * 06/30/2010 10.0.0    Fix buffer overflow in Lax_LoadGrGrXml()
 * 07/01/2010 10.0.1    Stop updating TRANSFER with sale data.
 * 02/16/2011 10.4.2.2  Force return error if LoadGrGr() failed to clean up DB tables.  If GrGr
 *                      file not found, send email alert.
 * 05/23/2011 10.5.13   Fix bad character in Lax_MergeOwner() and Lax_MergeMAdr().
 * 06/03/2011 10.5.15   Remove Seller. This field actually Transferee that occurs after lien date.
 * 08/03/2011 11.0.7    Add S_HSENO
 * 08/05/2011 11.0.7.1  Fix Lax_UpdateXferDoc() to update SALE1 only if it is newer.
 * 10/02/2012 12.2.9    Modify Lax_UpdateXferDoc() to use only good transfer to update sale rec.
 *                      Remove bad chars in Legal.
 * 10/17/2012 12.3.1    Populate BLDGS.
 * 11/25/2012 12.3.6    Modify Lax_MergeMAdr() & Lax_MergeSAdr() to ignore Zip4 if it is 0.
 * 12/18/2012 12.3.9    Increase SIZ_LIEN_HO_CNT from 1 to 3.  This affect 2012 and later.
 *                      Populate exemption value for missing parcels.
 * 01/07/2013 12.4.1    Populate Vesting.
 * 06/24/2013 12.7.3    Add option -Xh to extract mobile home parcels and append them to roll
 *                      file for output. Use this option when receive new Xref file.
 * 07/23/2013 13.1.6    Remove known bad chars in CAREOF & ZONING.  Assign region 3 to MH.
 * 08/21/2013 13.6.11   Add acWorkDrive[] to sort using specific work drive, not temp path.
 * 08/23/2013 13.6.12   Modify Lax_ParseXrefRec() to extract all records from Cross Ref Roll.
 * 10/08/2013 13.10.4   Use removeNames() to remove owner name, careof, and Etal flag.
 *                      Existing code to update vesting is sufficient, may use updateVesting() in the future.
 * 06/12/2014 13.14.7   Fix bug in Lax_MergeSAdr() that didn't clear out old address.
 * 07/02/2014 14.0.1    Move MergeGrGr() and MergeGrGrDoc() from SaleRec.cpp to MergeLax.cpp
 *                      and modify MergeGrGr() to update transfers.
 * 07/07/2014 14.0.3    Modify MergeGrGrDoc() to use Grgr_exp.sls instead of Grgr_exp.dat.
 *                      Grgr_exp.sls is now always sorted on successful extract.
 * 08/05/2014 14.2.2    Modify Lax_MergeOwner() to remove bad characters, back slash and single quote.
 * 09/15/2014 14.5.1    Add Lax_LegalExtr() to extract legal from roll file. If a ParcelFile 
 *                      is defined in INI file, extract only parcels included in that file.
 * 09/26/2014 14.6.0    Move MergeGrGrDoc() and MergeGrGr() to doGrGr.cpp.
 * 10/29/2014 14.8.0    Fix memory violation in Lax_MergeOwner(), Lax_MergeMAdr() and Lax_MergeSAdr().
 * 11/06/2014 14.9.1    Fix UNIT# in Lax_MergeSAdr()
 * 11/12/2014 14.9.2    Fix memory violation in Lax_MergeOwner()
 * 12/09/2014 14.10.1   Modify LegalExtr() to take input & output file
 * 02/03/2015 14.6.0    Fix GrGrTmpl problem by using standard template ???_GrGr for cum grgr data.
 *                      Use ???_GrGr.* for acGrGrTmpl[] and GrGr_Exp.* for acEGrGrTmpl[].
 * 05/24/2015 14.14.4   Add Lax_FormatAltApn() to extract AltApn from narative and populate R01
 * 05/27/2015 14.14.4.1 Modify Lax_FormatAltApn() to output AltApn without hyphen.  Then create maplink
 *                      based on AltApn to link to parent parcel.
 * 05/29/2015 14.15.2   Add Lax_MergeXrefOwner() to replace Lax_MergeOwner() in Lax_ParseXrefRec()
 * 06/04/2015 14.15.3   Update DBA name in Lax_MergeOwner()
 * 08/17/2015 15.1.1    Modify Lax_MergeXSAdr() to pull situs from legal if exist.
 * 09/21/2015 15.2.3    Add iDbaCnt for debug.  Modify Lax_MergeXrefOwner() and Lax_MergeOwners() to 
 *                      fine tune process of collecting DBA which also appears in NAME1.
 *                      Modify Lax_ParseSitus() and parseSAdr1() to fine tune process of pulling
 *                      situs from legal, also add iDbaCnt for debug.
 * 02/16/2016 15.6.0    Remove duplicate owner name and bad chars in MergeOwners().
 * 03/02/2016 15.6.1    Cleanup situs in parseSAdr1(), Lax_MergeXSAdr() & Lax_ParseSitus().
 *                      Modify Lax_FormatAltApn() & Lax_ParseXrefRec() to pull AltApn correctly.
 * 05/04/2016 15.9.3    Add option to load tax data.
 * 07/13/2016 16.0.5    Modify Lax_Load_Roll() & Lax_LegalExtr() to fix bug when EOF character is in middle of record.
 *                      Modify Lax_MergeOwner() and Lax_MergeMAdr() to remove known bad char.
 * 07/20/2016 16.1.1    Modify Lax_ExtrLien() and Lax_Load_LDR() to read roll file in binary mode
 *                      to avoid loading interuption due to char 0x1A appears in the mid section of record
 * 07/28/2016 16.1.3    Allow -Xh option to run alone.
 * 08/21/2016 16.2.1    Modify Lax_ParseTaxDetail() to fix Agency record output.  Add DelqYear to TaxBase.
 * 10/19/2016 16.5.0    Use lLastTaxFileDate for Updated_Date.
 * 11/22/2016 16.7.1    Add comment to Lax_LoadGrGrXml() and set MultiSale flag in Lax_FormatSaleRec()
 * 12/27/2016 16.8.3    Call SetMultiSale() to set MultiSale flag in Lax_UpdateSaleHist()
 * 12/28/2016 16.8.3.1  Modify Lax_ConvertSale() to set MultiSale flag.
 * 12/31/2016 16.8.4    Fix bug in Lax_Load_TaxDelq() to concatenate two delinquent files before processing.
 *                      When finish, rename it to current day.
 * 01/04/2017 16.8.5    Modify Lax_ParseDelq() to populate Red_Date when paid in full with Red_Amt=PaidAmt.
 *                      If not paid in full, do not populate Red_Date and set Status to UNPAID or ONPAYMENTPLAN
 * 01/05/2017 16.8.6    Add GENERAL TAX.  Create BillNum using APN & SeqNum.  Add Suplemental record.
 * 03/29/2017 16.13.3   Modify Lax_FormatSaleRec() due to new sale record layout.
 * 04/05/2017 16.13.7   Modify Lax_FormatSaleRec() to reset back to old layout and fix SaleCode bug.
 * 05/20/2017 16.14.12  Set installment status.
 * 11/28/2017 17.5.1    Modify Lax_ParseDelq() to fix taxYear & defaultAmt. Use updateDelqFlag() to update Base delq.
 * 01/30/2018 17.6.4    Rename Lax_CreatePQ4Rec() to Lax_MergeRoll() to standardize function name.
 *                      Modify Lax_MergeSaleRec() & Lax_UpdateXferDoc() to set MultiApn flag.
 * 05/07/2018 17.10.8   Modify Lax_ParseTaxBase() to process current year tax bill only.
 * 07/24/2018 18.2.3    Increase number of records per output file to 900,000 from 800,000 in Lax_Load_LDR()
 * 08/03/2018 18.2.6    Fix bug that didn't add MH to R03 in loadLax()
 * 08/23/2018 18.3.3    Fix Situs & mailing Unit#
 * 08/24/2018 18.4.0    Replace R01.MergeArea() with PQ_MergeLotArea() using new LAX_Basemap.txt
 * 09/21/2018 18.5.0    Add Lax_Updt_TaxBase() to update tax base.
 * 09/29/2018 18.5.1    Move -Mr option to LoadOne.cpp
 * 01/11/2019 18.8.2    Add Lax_ExtrChar() & Lax_CreateCharRec() to generate Lax_Char.dat from DS04.TXT
 * 03/22/2019 18.10.3   Check TaxDetail file size.  If it is bigger than 100MB, then load normal, else use update medthod.
 * 04/09/2019 18.11.1   Modify Lax_MergeRoll() to update MULTIAPN flag.
 * 05/17/2019 18.12.3   Fix UnitNo in Lax_ExtrMH() by populating UNITNOX in Lax_MergeSAdr(), parseSAdr1(), Lax_MergeXSAdr()
 * 05/18/2019 18.12.4   Modify Lax_MergeRoll() to remove bad char in ZONING.
 * 05/27/2019 18.12.7   Fix UnitNo in Lax_MergeSAdr(), parseSAdr1(), Lax_ParseSitus(), Lax_MergeXSAdr()
 * 07/24/2019 19.0.5    Modify Lax_ParseXrefRec(), Lax_MergeRoll(), Lax_MergeSAdr() to remove '\' from LEGAL & StrName.
 * 10/04/2019 19.2.3    Modify Lax_ParseXrefRec() to populate OTHER_IMPR with OTHER_VAL to satisfy one bulk customer.
 * 04/02/2020 19.8.7    Add -Xn & -Mn option to format and merge NDC recorder data.
 * 06/20/2020 20.0.0    Modify Lax_MergeRoll() to copy ExclType to EXE_CD1.
 *                      Assign '10' to EXE_CD1 if HOE exist.
 * 07/31/2020 20.2.5    Clean up situs and owner name in Xref file.
 * 10/28/2020 20.3.6    Modify Lax_MergeRoll() & Lax_ParseXrefRec() to populate PQZoning.
 * 01/16/2021 20.7.1    Add Lax_MakeDocLink() and option to merge DocLinks.
 * 01/18/2021 20.7.1.2  Modify Lax_LoadGrGr() to support GRGR input as XML or zip file.
 * 02/11/2021 20.7.4    Modify Lax_LoadGrGr() to move XML file(s) to GrGr_yyyymmdd after processing successfully.
 *                      Also, this function now won't do merge GrGr.  User has to use -Mg to merge manually.
 * 05/11/2021 20.7.18   Fix S_SUFFIX in Lax_MergeSAdr() & Lax_MergeXSAdr() due to overwritten by S_STREET. 
 *                      Fix S_STRNUM cutoff and special case in Lax_ParseSitus() when parse cross ref file.
 * 02/25/2022 21.5.0    Cleanup Lax_ParseTaxBase().  Add Lax_ExtrVal() & Lax_CreateValueRec() and option -Xf & -Mf.
 * 04/26/2022 21.9.0    Use acGrBkTmpl as backup folder for GrGr files. 
 * 08/05/2022 22.1.3    Region table must be loaded before loading LDR.
 * 09/12/2022 22.2.2    Modify Lax_MergeOwner() to scan for bad chars in both Name1 & Name2.
 * 10/31/2022 22.2.11   Remove option to merge final value (it's moved to LoadOne.cpp).
 * 08/10/2023 23.1.3    Modify loadLax() to check TaxRoll file size to determine whether to do full load.
 *                      Modify Lax_Update_TaxBase() to generate county specific Tmp???_Base file.
 * 10/10/2023 23.3.2    Modify Lax_MergeRoll() & Lax_ParseXrefRec() to stop update ZONE_X1 with county ZONING.
 *                      This field is used for PQZoning only.
 * 10/16/2023 23.3.4    Modify Lax_Load_TaxBase() to update sort command for Agency file.
 * 01/12/2024 23.5.5    Fix M_UNITNOX in Lax_MergeMAdr() & Lax_MergeSAdr().
 * 01/17/2024 23.5.6    Modify Lax_ParseTaxDetail() & Lax_ParseTaxBase() to change BillNum to include tax year.
 *                      Also now we take all bill, not only current tax year.
 * 07/28/2024 24.0.8    Modify Lax_MergeRoll() to add ExeType.
 * 08/10/2024 24.1.0    Add Lax_Load_DefaultTaxRoll() && Lax_ParseLienSecuredTaxRoll() for new tax file format.
 * 10/17/2024 24.1.7    Create new Lax_ExtrVal(), Lax_CreateValueRec(), Lax_Load_TaxBaseCsv() & Lax_MergeTaxDetail  
 *                      to support new tax file TTC_Secured_*.csv.
 * 10/19/2024 24.1.8    Fix bug (new date format) in Lax_ParseDelq().  Remove header row in Lax_Load_TaxDelq().
 * 11/18/2024 24.3.5    Modify Lax_MergeRoll() & loadLax() to support new county files (the county is now sent
 *                      two files instead of one DS04.txt).  We need to combine them before processing.
 * 12/09/2024 24.4.1    Prepare DS04.txt for CHAR extract & ROLL update.
 * 12/11/2024 24.4.1.1  LAX is changing their mind and resend DS04 in single file.  We need to set it up to work both ways.
 * 12/31/2024 24.4.2    Modify Lax_ParseTaxBaseCsv() to ignore prior year record.
 * 01/16/2025 24.4.3    Modify Lax_MergeTaxDetail() to remove check on Prorated Amount.
 *                      Modify Lax_ParseTaxBaseCsv() to take based tax records only where the last three digits
 *                      of PARCEL_HISTORY_YRSEQ is "000".
 * 02/01/2025 24.4.10   Modify Lax_ParseTaxBaseCsv() add supplemental record, correct the 1% county tax amount.
 *                      Add tax rate table.
 *
 ***************************************************************************/

#include "stdafx.h"
#include "Prodlib.h"
#include "CountyInfo.h"
#include "R01.h"
#include "RecDef.h"
#include "Tables.h"
#include "doGrGr.h"
#include "doZip.h"
#include "SaleRec.h"
#include "Logs.h"
#include "LoadOne.h"
#include "Utils.h"
#include "XlatTbls.h"
#include "doSort.h"
#include "doOwner.h"
#include "formatApn.h"
#include "SaleRec.h"
#include "MergeLax.h"
#include "UseCode.h"
#include "Markup.h"
#include "SqlExt.h"
#include "LOExtrn.h"
#include "PQ.h"
#include "SendMail.h"
#include "Tax.h"
#include "CharRec.h"
#include "NdcExtr.h"

#define  MAX_LAX_BOOKS  64
REG_IDX  LaxRegs[MAX_LAX_BOOKS];
int      iRegBooks, iDbaCnt;

static   FILE  *fdLien;
static   int   lApnSkip, lApnMatch, lUpdXfer;
static   int   iCumSaleLen;
static   long  lSaleSkip, lGrGrSkip;
static   long  lSaleMatch, lGrGrMatch;
static   bool  bUseMdb;
static   char  acWorkDrive[64];

static   hlAdo    dbTra;
static   hlAdoRs  rsTra;

extern   hlAdo dbLoadConn;

/********************************** extrGrGr **********************************
 *
 * Extract GrGr data from SQL table.  Return -1 if error.
 * Otherwise, number of output records.  LAX only
 *
 ******************************************************************************/

int extrGrGrDoc(char *pOutfile)
{
   char     acTmp[256];
   int      iTmp;
   long     lCnt=0, lTmp;

   CString  sTmp, sApn, sType;
   hlAdoRs  myRs;
   FILE     *fdSale;
   GRGR_DOC GrGrRec;

   LogMsg("Extract grgr to %s", pOutfile);

   // Open output file
   if (!(fdSale = fopen(pOutfile, "w")))
   {
      LogMsg("***** Error creating %s file", pOutfile);
      return -1;
   }

   // Select statement
   sprintf(acTmp, "SELECT * FROM %s WHERE Apn > ' ' AND DocDate > 0 ORDER BY APN", acGrGrTbl);
   try
   {
      myRs.Open(dbLoadConn, acTmp);
   }
   AdoCatch(e)
   {
      LogMsg("***** Error executing command [%s] : %s", acTmp, ComError(e));
      fclose(fdSale);
      return -1;
   }

   // Loop through
   while (myRs.next())
   {
      
      memset((void *)&GrGrRec, 32, sizeof(GRGR_DOC));
      sTmp = myRs.GetItem("DocNum");
      // Strip off year from DocNum
      if (sTmp.GetLength() > 9)
         sTmp = sTmp.Mid(4);
      memcpy(GrGrRec.DocNum, sTmp, sTmp.GetLength());
      sTmp = myRs.GetItem("DocDate");
      memcpy(GrGrRec.DocDate, sTmp, sTmp.GetLength());
      sTmp = myRs.GetItem("DocTitle");
      iTmp = sTmp.GetLength();
      if (iTmp > OFF_GD_TITLE) iTmp = OFF_GD_TITLE;
      memcpy(GrGrRec.DocTitle, sTmp, iTmp);
      sApn = myRs.GetItem("Apn");
      memcpy(GrGrRec.APN, sApn, sApn.GetLength());

      sTmp = myRs.GetItem("DocTax");
      lTmp = atol(sTmp);
      if (lTmp > 1000)
      {
         sprintf(acTmp, "%*ld", SIZ_GD_TAX, lTmp);
         memcpy(GrGrRec.DocTax, acTmp, SIZ_GD_TAX);
      }

      sTmp = myRs.GetItem("SalePrice");
      lTmp = atol(sTmp);
      if (lTmp > 1000)
      {
         sprintf(acTmp, "%*ld", SIZ_GD_SALE, (lTmp/100)*100);
         memcpy(GrGrRec.SalePrice, acTmp, SIZ_GD_SALE);
      }

      // Grantees
      sTmp = myRs.GetItem("Grtee1");
      iTmp = sTmp.GetLength();
      if (iTmp > SIZ_GD_NAME) iTmp = SIZ_GD_NAME;
      memcpy(GrGrRec.Grantee[0], sTmp, iTmp);
      sTmp = myRs.GetItem("Grtee2");
      iTmp = sTmp.GetLength();
      if (iTmp > SIZ_GD_NAME) iTmp = SIZ_GD_NAME;
      memcpy(GrGrRec.Grantee[1], sTmp, iTmp);
      
      // Grantors
      sTmp = myRs.GetItem("Grtor1");
      iTmp = sTmp.GetLength();
      if (iTmp > SIZ_GD_NAME) iTmp = SIZ_GD_NAME;
      memcpy(GrGrRec.Grantee[0], sTmp, iTmp);
      sTmp = myRs.GetItem("Grtor2");
      iTmp = sTmp.GetLength();
      if (iTmp > SIZ_GD_NAME) iTmp = SIZ_GD_NAME;
      memcpy(GrGrRec.Grantee[1], sTmp, iTmp);

      GrGrRec.CRLF[0] = '\n';
      GrGrRec.CRLF[1] = 0;
      fputs((char *)&GrGrRec, fdSale);
      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }
   printf("\r%u\n", lCnt);

   // Close recordset and outfile
   myRs.Close();
   fclose(fdSale);

   LogMsg("Total number of extracted GrGr records: %ld", lCnt);
   return lCnt;
}

/********************************* Lax_FmtRecDoc *****************************
 *
 *  - County only provides latest DOC#, but provides last three SALEDT and SALEAMT.
 *  - Only use data if DOC# avail.
 *
 *****************************************************************************/
/*
void Lax_FmtRecDoc(char *pOutbuf, char *pRollRec)
{
   LAX_ROLL  *pRec;
   int      iTmp;
   char     acDoc[32], acTmp[32];
   bool     bMisAligned = false;
   long     lTmp, lDocNum, lDocDt, lDate1;

   pRec = (LAX_ROLL *)pRollRec;

   lDocDt  = atoin(pRec->DocDate, RSIZ_DOCDATE);
   lDocNum = atoin(pRec->LastDocNum, RSIZ_DOCNUM);
   lDate1  = atoin(pRec->Sale1Dt, RSIZ_DOCDATE);
   if (lDocNum > 0)
      sprintf(acDoc, "%d           ", lDocNum);
   else
      memset(acDoc, ' ', SIZ_TRANSFER_DOC);

   if (lDocDt > 0)
   {
      memcpy(pOutbuf+OFF_TRANSFER_DT, pRec->DocDate, RSIZ_DOCDATE);
      if (lDocNum > 0)
         memcpy(pOutbuf+OFF_TRANSFER_DOC, acDoc, SIZ_TRANSFER_DOC);
   }

   // If load GrGr, ignore sale data from assessor
   if (bUseGrGr)
      return;

   if (lDate1 > 0)
   {
      *(pOutbuf+OFF_AR_CODE1) = 'A';            // Set Assessor-Recorder code to sale1
      memcpy(pOutbuf+OFF_SALE1_DT, pRec->Sale1Dt, RSIZ_DOCDATE);

      lTmp = atoin(pRec->Sale1Amt, RSIZ_SALE1AMT);
      if (lTmp > 10)
      {
         iTmp = sprintf(acTmp, "%*u", SIZ_SALE1_AMT, lTmp);
         acTmp[iTmp-1] = '0';                   // Remove check digit from county
         memcpy(pOutbuf+OFF_SALE1_AMT, acTmp, iTmp);
         *(pOutbuf+OFF_SALE1_CODE) = 'F';
      }
      if (lDocDt == lDate1)
         memcpy(pOutbuf+OFF_SALE1_DOC, acDoc, SIZ_TRANSFER_DOC);
      //else
      //   memset(pOutbuf+OFF_SALE1_DOC, ' ', SIZ_TRANSFER_DOC);
   } else
   {
      memcpy(pOutbuf+OFF_SALE1_DT, pRec->DocDate, RSIZ_DOCDATE);
      if (lDocNum > 0)
         memcpy(pOutbuf+OFF_SALE1_DOC, acDoc, SIZ_TRANSFER_DOC);
   }

   lTmp = atoin(pRec->Sale2Dt, RSIZ_DOCDATE);
   if (lTmp > 0)
   {
      *(pOutbuf+OFF_AR_CODE2) = 'A';            // Set Assessor-Recorder code to sale1
      memcpy(pOutbuf+OFF_SALE2_DT, pRec->Sale2Dt, RSIZ_DOCDATE);

      lTmp = atoin(pRec->Sale2Amt, RSIZ_SALE2AMT);
      if (lTmp > 0)
      {
         iTmp = sprintf(acTmp, "%*u", SIZ_SALE2_AMT, lTmp);
         acTmp[iTmp-1] = '0';                   // Remove check digit from county
         memcpy(pOutbuf+OFF_SALE2_AMT, acTmp, iTmp);
         *(pOutbuf+OFF_SALE2_CODE) = 'F';
      }
   }
   lTmp = atoin(pRec->Sale3Dt, RSIZ_DOCDATE);
   if (lTmp > 0)
   {
      *(pOutbuf+OFF_AR_CODE3) = 'A';            // Set Assessor-Recorder code to sale1
      memcpy(pOutbuf+OFF_SALE3_DT, pRec->Sale3Dt, RSIZ_DOCDATE);

      lTmp = atoin(pRec->Sale3Amt, RSIZ_SALE3AMT);
      if (lTmp > 0)
      {
         iTmp = sprintf(acTmp, "%*u", SIZ_SALE3_AMT, lTmp);
         acTmp[iTmp-1] = '0';                   // Remove check digit from county
         memcpy(pOutbuf+OFF_SALE3_AMT, acTmp, iTmp);
         *(pOutbuf+OFF_SALE3_CODE) = 'F';
      }
   }

}
*/
/******************************* Lax_UpdateXferDoc ***************************
 *
 *  - County only provides latest DOC#, but provides last three SALEDT and SALEAMT.
 *  - Only use data if DOC# avail.
 *
 *****************************************************************************/

void Lax_UpdateXferDoc(char *pOutbuf, char *pRollRec)
{
   LAX_ROLL  *pRec;
   int      iTmp;
   char     acDoc[32], acTmp[32];
   bool     bMisAligned = false;
   long     lTmp, lDocNum, lDocDt, lDate1, lSaleDate, lPrice;

   pRec = (LAX_ROLL *)pRollRec;

   lDocDt  = atoin(pRec->DocDate, RSIZ_DOCDATE);
   lDocNum = atoin(pRec->LastDocNum, RSIZ_DOCNUM);
   if (lDocNum > 0)
      iTmp = sprintf(acDoc, "%.*u", RSIZ_DOCNUM, lDocNum);

   lTmp = atoin(pOutbuf+OFF_TRANSFER_DT, SIZ_TRANSFER_DT);
   if (lDocDt > lTmp && lDocNum > 0)
   {
      memcpy(pOutbuf+OFF_TRANSFER_DT, pRec->DocDate, RSIZ_DOCDATE);
      memcpy(pOutbuf+OFF_TRANSFER_DOC, acDoc, RSIZ_DOCNUM);

      // Multi APN/Partial transfer
      if (pRec->DocKey == 'M')
         *(pOutbuf+OFF_MULTI_APN) = 'Y';
      else 
         *(pOutbuf+OFF_MULTI_APN) = ' ';

      // Only update sale with good transfer
      if (pRec->DocKey != 'A')
         return;

      lSaleDate = atoin(pOutbuf+OFF_SALE1_DT, SIZ_TRANSFER_DT);
      lDate1 = atoin(pRec->Sale1Dt, RSIZ_DOCDATE);
      lPrice = atoin(pRec->Sale1Amt, RSIZ_SALE1AMT);
      if ((lDate1 == lDocDt) && (lDate1 > lSaleDate) && lPrice > 500)
      {
         lUpdXfer++;

         // Move sale2 to sale3
         memcpy(pOutbuf+OFF_SALE3_DOC, pOutbuf+OFF_SALE2_DOC, SIZ_SALE3_DOC);
         memcpy(pOutbuf+OFF_SALE3_DT, pOutbuf+OFF_SALE2_DT, SIZ_SALE3_DT);
         memcpy(pOutbuf+OFF_SALE3_DOCTYPE, pOutbuf+OFF_SALE2_DOCTYPE, SIZ_SALE3_DOCTYPE);
         memcpy(pOutbuf+OFF_SALE3_AMT, pOutbuf+OFF_SALE2_AMT, SIZ_SALE3_AMT);
         memcpy(pOutbuf+OFF_SALE3_CODE, pOutbuf+OFF_SALE2_CODE, SIZ_SALE3_CODE);
         *(pOutbuf+OFF_AR_CODE3) = *(pOutbuf+OFF_AR_CODE2);

         // Move sale1 to sale2
         memcpy(pOutbuf+OFF_SALE2_DOC, pOutbuf+OFF_SALE1_DOC, SIZ_SALE3_DOC);
         memcpy(pOutbuf+OFF_SALE2_DT, pOutbuf+OFF_SALE1_DT, SIZ_SALE3_DT);
         memcpy(pOutbuf+OFF_SALE2_DOCTYPE, pOutbuf+OFF_SALE1_DOCTYPE, SIZ_SALE3_DOCTYPE);
         memcpy(pOutbuf+OFF_SALE2_AMT, pOutbuf+OFF_SALE1_AMT, SIZ_SALE3_AMT);
         memcpy(pOutbuf+OFF_SALE2_CODE, pOutbuf+OFF_SALE1_CODE, SIZ_SALE3_CODE);
         *(pOutbuf+OFF_AR_CODE2) = *(pOutbuf+OFF_AR_CODE1);

         *(pOutbuf+OFF_AR_CODE1) = 'A';            // Set Assessor-Recorder code to sale1
         memcpy(pOutbuf+OFF_SALE1_DT, pRec->Sale1Dt, RSIZ_DOCDATE);
         memcpy(pOutbuf+OFF_SALE1_DOC, acDoc, RSIZ_DOCNUM);

         iTmp = sprintf(acTmp, "%*u", SIZ_SALE1_AMT, lPrice);
         acTmp[iTmp-1] = '0';                   // Remove check digit from county
         memcpy(pOutbuf+OFF_SALE1_AMT, acTmp, iTmp);
         //*(pOutbuf+OFF_SALE1_CODE) = 'F';

         if (lDate1 > lLastRecDate)
            lLastRecDate = lDate1;

      }
   }
}

/******************************** Lax_MergeOwner *****************************
 *
 * This version is to parse owner name from Roll file.
 * Return 0 if successful, >0 if error
 *
 *****************************************************************************/

void Lax_MergeOwner(char *pOutbuf, char *pName1, char *pName2, char *pCareOf)
{
   int   iTmp, iNameFlg=5;
   char  acTmp[128], acSave[128];
   char  acName1[128], acName2[128], acOwner1[128], acOwner2[128], acVest[8];
   char  *pTmp, *pTmp1, *pName1_1, *pName1_2;
   bool  bMarkName1=false, bMarkName2=false;

   OWNER myOwner, myOwner2;

   // Clear output buffer if needed
   removeNames(pOutbuf, true);

   // Remove known bad char
   if ((unsigned char)*(pName1+2) == 0x8D)
   {
      memcpy(acTmp, pName1, RSIZ_NAME2);
      memcpy(pName1+2, &acTmp[3], RSIZ_NAME2-3);
      *(pName1+(RSIZ_NAME2-1)) = ' ';
      LogMsg("*** Remove known bad char in Owner1 at %.10s", pOutbuf);
   } else if ((unsigned char)*(pName1+13) == 0x8D)
   {
      *(pName1+13) = ' ';
      LogMsg("*** Remove known bad char in Owner1 at %.10s", pOutbuf);
   }

   // Initialize
   pName1_1 = pName1;
   pName1_2 = pName1+RSIZ_NAME2;
   memset((void *)&myOwner, 0, sizeof(OWNER));
   acName2[0] = 0;
   acVest[0] = 0;
   acSave[0] = 0;

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "5144003035", 10))
   //   iTmp = 0;
#endif

   // Check DBA name in CareOf 
   memcpy(acTmp, pCareOf+RSIZ_SPCNAMETYPE, RSIZ_SPCNAME);
   iTmp = blankRem(acTmp, RSIZ_SPCNAME);
   if (!memcmp(pCareOf, "DBA", 3))
   {
      memcpy(pOutbuf+OFF_DBA, acTmp, iTmp);
      acTmp[0] = 0;
      iTmp = 0;
      iDbaCnt++;
   } else if ((pTmp = _strstrn(acTmp, "DBA ", iTmp)))
   {
      vmemcpy(pOutbuf+OFF_DBA, pTmp, strlen(pTmp));
      *pTmp = 0;
      iTmp = strlen(acTmp);
      iDbaCnt++;
   }

   // Check for CareOf
   if (!memcmp(pCareOf, "C/O", 3) && iTmp > 4)
   {
      // Remove known bad char
      iTmp = remUChar((unsigned char *)&acTmp[0], 0x9B, iTmp);
      updateCareOf(pOutbuf, acTmp, iTmp);
   } 

   // Search for DBA in Name1_2
   if (!memcmp(pName1_2, "DBA", 3))
   {
      memcpy(pOutbuf+OFF_DBA, pName1_2, RSIZ_NAME1_2);
      *pName1_2 = 0;
      iDbaCnt++;
   } else if (pTmp = _strstrn(pName1_1, " DBA", RSIZ_NAME1))
   {
      *pTmp++ = 0;
      int iNameLen = strlen(pName1_1);
      strcpy(acName1, pName1_1);
      blankRem(acName1);
      if (*(pOutbuf+OFF_DBA) == ' ')
      {
         iTmp = RSIZ_NAME1 - iNameLen;
         memcpy(acTmp, pTmp, --iTmp);
         iNameLen = blankRem(acTmp, iTmp);
         if (iNameLen == 3 && *pName2 > ' ')
         {
            sprintf(acTmp, "DBA %.*s", RSIZ_NAME2, pName2);
            iNameLen = blankRem(acTmp);
         }
         vmemcpy(pOutbuf+OFF_DBA, acTmp, SIZ_DBA, iNameLen);
         iDbaCnt++;
      }
      *pName2 = 0;
      *pName1_2 = 0;
   }

   // Save Name2
   if (*pName2 >= '0')
   {
      memcpy(acOwner2, pName2, RSIZ_NAME2);
      acOwner2[RSIZ_NAME2] = 0;
      memcpy(acName2, pName2, RSIZ_NAME2);
      bMarkName2 = markLastName(myTrim(acName2, RSIZ_NAME2));
   }

   // Mark name1
   memcpy(acOwner1, pName1_1, RSIZ_NAME2);
   acOwner1[RSIZ_NAME2] = 0;
   memcpy(acName1, pName1_1, RSIZ_NAME2);
   bMarkName1 = markLastName(myTrim(acName1, RSIZ_NAME2));

   // Looking for 'AND' in name1
   pTmp = (char *)&acName1[strlen(acName1)-4];
   if (!memcmp(pTmp, " AND", 4))
   {
      if (*pName1_2 > ' ')
      {
         memcpy(acTmp, pName1_2, RSIZ_NAME2);
         if (markLastName(myTrim(acTmp, RSIZ_NAME2)))
            iNameFlg = 4;   // Set this flag to remember that after AND there is another last name

         // Drop 'AND' at the end of Name1_2 then append to Name1_1
         iTmp = strlen(acTmp);
         if (iTmp > 4)
         {
            pTmp = (char *)&acTmp[iTmp-4];
            if (!memcmp(pTmp, " AND", 4))
               *pTmp = 0;
         }
         strcat(acName1, " ");
         strcat(acName1, acTmp);
      } else if (acName2[0] > ' ')
      {
         // If there are more than one names in Name2, process it separately
         // Name1 = LUNA,JOSE C AND NORMA AND  Name2=VALADEZ,XAVIER AND RAQUEL
         if (!strstr(acName2, " AND "))
         {
            // If Name1_1 has two 'AND' then we cannot append Name2 to Name1_1
            // Name1=HERNANDEZ,ALEX P AND TANYA A AND   Name2=WHETSTINE,DARLENE
            if (!strstr(acName1, " AND "))
            {
               // Append Name2 to Name1_1 if both names seem to contain a last name
               if (bMarkName1 && bMarkName2)
               {
                  strcat(acName1, " ");
                  strcat(acName1, acName2);
                  acName2[0] = 0;
                  if (bMarkName2)
                     iNameFlg = 4;
               } else
                  *pTmp = 0;           // Drop 'AND' from Name1_1
            } else
               *pTmp = 0;
         } else
            *pTmp = 0;  // Drop 'AND' from Name1_1
      } else
         *pTmp = 0;
   } else if (!memcmp(pName1_2, "AND ", 4))
   {  // FRIEDLANDER BERKOWITZ, BARBARA  AND BERKOWITZ,STEVEN
      memcpy(acTmp, pName1_2+4, RSIZ_NAME2-4);
      if (markLastName(myTrim(acTmp, RSIZ_NAME2-4)))
         iNameFlg = 4;

      // Drop 'AND' at the end of Name1_2 then append to Name1_1
      iTmp = strlen(acTmp);
      if (iTmp > 4)
      {
         pTmp = (char *)&acTmp[iTmp-4];
         if (!memcmp(pTmp, " AND", 4))
            *pTmp = 0;
      }
      strcat(acName1, " AND ");
      strcat(acName1, acTmp);
   } else if (acName2[0] <= ' ' && *pName1_2 > ' ')
   {
      // If this is a TRUST name, keep this as is, don't parse
      memcpy(acTmp, pName1_2, RSIZ_NAME2);
      myTrim(acTmp, RSIZ_NAME2);
      if (strstr(acTmp, " TRUST"))
      {
         strcpy(acSave, acTmp);
         iTmp = strlen(acSave);
         if (iTmp > 4)
         {
            pTmp = (char *)&acSave[iTmp-4];
            if (!memcmp(pTmp, " AND", 4))
               *pTmp = 0;
         }
      } else
      {
         strcat(acName1, " ");
         strcat(acName1, acTmp);
      }
   }

   // Check vesting
   if ( (pTmp=strstr(acName1, " ETAL")) || (pTmp=strstr(acName1, " ET AL")) ) 
   {
      *pTmp = 0;
      strcpy(acVest, "EA");
   }
   else if ( (pTmp=strstr(acName1, " FAMILY TRUST")) || (pTmp=strstr(acName1, " FAMTRUST")) ||
             (pTmp=strstr(acName1, " FAMILYTR")) || (pTmp=strstr(acName1, " FMLYTRUST")) )
   {
      strcpy(acVest, "FM");
   }
   else if ( (pTmp=strstr(acName1, " LVGTRUST")) || (pTmp=strstr(acName1, " REV LIVING TRUST")) || (pTmp=strstr(acName1, " LIVING TRUST")) ||
         (pTmp=strstr(acName1, " LIVTRUST")) || (pTmp=strstr(acName1, " LVTRUST")) )
   {
      strcpy(acVest, "LV");
   }
   else if (pTmp=strstr(acTmp, " REVOC TRUST"))
   {
      strcpy(acVest, "RT");
   }
   else if ( (pTmp=strstr(acName1, " TRS")) || (pTmp=strstr(acName1, " CO TR")) || (pTmp=strstr(acName1, " TR ")) )
   {
      *pTmp = 0;
      strcpy(acVest, "TR");
   }
   else if ( (pTmp=strstr(acName1, " LLC")) || (pTmp=strstr(acName1, " LLLP")) || (pTmp=strstr(acName1, " LLP")) ||
         (pTmp=strstr(acName1, " INC")) || (pTmp=strstr(acName1, " CORP")) || (pTmp=strstr(acName1, " RANCH LP")) || 
         (pTmp=strstr(acName1, "LLC.")) )
   {
      strcpy(acVest, "CO");
   }
   else if ( (pTmp=strstr(acName1, "CITY OF ")) || (pTmp=strstr(acName1, "COUNTY OF ")) || (pTmp=strstr(acName1, "L A COUNTY")) ||
         (pTmp=strstr(acName1, "LA COUNTY")) || (pTmp=strstr(acName1, "LOS ANGELES COUNTY")) || (pTmp=strstr(acName1, "STATE OF ")) || 
         (pTmp=strstr(acName1, "UNITED STATES ")) || (pTmp=strstr(acName1, "USLD ")) || (pTmp=strstr(acName1, "U S GOVT ")) )
   {
      strcpy(acVest, "GV");
   }
   else if ( (pTmp=strstr(acName1, " IRREVOCABLE TRUST")) || (pTmp=strstr(acName1, " IRRTRUST")) || (pTmp=strstr(acName1, " IRREVTRUST")) ||
         (pTmp=strstr(acName1, " IRREVOCTRUST")) )
   {
      strcpy(acVest, "IR");
   }
   else if (pTmp=strstr(acName1, " TRUST"))
   {
      strcpy(acVest, "TR");
   }
   else if (pTmp=strstr(acName1, " JT"))
   {
      strcpy(acVest, "JT");
   }
   else if ( (pTmp=strstr(acName1, " PARTNER")) || (pTmp=strstr(acName1, " PTNRSHP")) || (pTmp=strstr(acName1, " PTNSHIP")) ||
             (pTmp=strstr(acName1, " PTNRS")) || (pTmp=strstr(acName1, " PTNR")) )
   {
      strcpy(acVest, "PA");
   }

   // Check on name2
   if (acName2[0])
   {
      if (pTmp1=strstr(acName2, " ETAL"))
         *pTmp1 = 0;
      else if (pTmp1=strstr(acName2, " ET AL"))
         *pTmp1 = 0;
      else if (pTmp1=strstr(acName2, " CO TR"))
         *pTmp1 = 0;
      else if (pTmp1=strstr(acName2, " TR "))
         *pTmp1 = 0;
      else if (pTmp1=strstr(acName2, " TRS"))
         *pTmp1 = 0;
   }

   // Store vesting
   iTmp = strlen(acVest);
   memcpy(pOutbuf+OFF_VEST, acVest, iTmp);

   // Check for bad character
   strcpy(acTmp, acName1);
   iTmp = replSqlChar(acName1);
   if (bDebug && (iTmp = replCharEx(acName1, ".;`\"")))
      LogMsg("*** Bad character in Owner1 at %.12s [%s]", pOutbuf, acTmp);

   // Now parse owners
   splitOwner(acName1, &myOwner, iNameFlg);

   if (myOwner.acName2[0] <= ' ' && acName2[0] >= '0')
   {
      strcpy(acTmp, acName2);
      iTmp = replSqlChar(acName2);
      if (bDebug && (iTmp = replCharEx(acName2, ".;`\"")))
         LogMsg("*** Bad character in Owner2 at %.12s [%s]", pOutbuf, acTmp);
      splitOwner(acName2, &myOwner2, 5);
      strcpy(myOwner.acName2, myOwner2.acName1);
   }

   // Store Name1
   clearMarker(myOwner.acName1);
   if (acVest[0] > ' ')
   {
      iTmp = replSqlChar(acOwner1);
      iTmp = replCharEx(acOwner1, ",.;`");
      vmemcpy(pOutbuf+OFF_NAME1, acOwner1, SIZ_NAME1);
   } else
      vmemcpy(pOutbuf+OFF_NAME1, myOwner.acName1, SIZ_NAME1);

   // Store Name2
   if (myOwner.acName2[0] <= ' ' && acSave[0] >= '0')
      strcpy(myOwner.acName2, acSave);

   if (myOwner.acName2[0] > ' ')
   {
      // Ignore duplicate name
      if (strcmp(myOwner.acName1, myOwner.acName2))
      {
         clearMarker(myOwner.acName2);
         vmemcpy(pOutbuf+OFF_NAME2, myOwner.acName2, SIZ_NAME2);
      } else
         lDupOwners++;
   }

   if (myOwner.acSwapName[0] > ' ')
   {
      clearMarker(myOwner.acSwapName);
      vmemcpy(pOutbuf+OFF_NAME_SWAP, myOwner.acSwapName, SIZ_NAME_SWAP);
   }
}

/*******************************************************************************
 *
 * This version is to parse owner from Xref file.
 *
 *******************************************************************************/

void Lax_MergeXrefOwner(char *pOutbuf, char *pName1, char *pName2, char *pSpclName)
{
   int   iTmp, iNameLen, iNameFlg=5;
   char  acTmp[128], acSave[128];
   char  acName1[128], acName1_2[64], acName2[64], acVest[8];
   char  *pTmp, *pTmp1, *pName1_1, *pName1_2;
   bool  bMarkName1=false, bMarkName2=false;

   OWNER myOwner, myOwner2;

   // Clear output buffer if needed
   removeNames(pOutbuf, true);

   // Initialize
   pName1_1 = pName1;
   pName1_2 = pName1+XSIZ_NAME1_1;

   memset((void *)&myOwner, 0, sizeof(OWNER));

   acName1[0] = 0;
   acName2[0] = 0;
   acVest[0] = 0;
   acSave[0] = 0;

#ifdef _DEBUG
   // Need to combine company name when there is no comma after first word
   //if (!memcmp(pOutbuf, "8950036065", 10))
   //   iTmp = 0;
#endif

   // Check for CareOf
   if (!memcmp(pSpclName, "C/O", 3))
   {
      memcpy(acTmp, pSpclName, XSIZ_SPCNAME);

      // Remove known bad char
      remUChar((unsigned char *)&acTmp[0], 0x9B, XSIZ_SPCNAME);
      updateCareOf(pOutbuf, acTmp, RSIZ_SPCNAME);
   } else if (!memcmp(pSpclName, "ATTN", 4))
   {
      updateCareOf(pOutbuf, pSpclName, XSIZ_SPCNAME);
   } else if (!memcmp(pSpclName, "DBA", 3))
   {
      memcpy(pOutbuf+OFF_DBA, pSpclName, XSIZ_SPCNAME);
      iDbaCnt++;
   } 

   // Search for C/O in Name1_2
   if (!memcmp(pName1_2, "C/O", 3) )
   {  // If CareOf field is already populated, ignore this one
      if (*(pOutbuf+OFF_CARE_OF) == ' ')
         updateCareOf(pOutbuf, pName1_2, XSIZ_NAME1_2);
      *pName1_2 = 0;
   } else if (!memcmp(pName1_2, "DBA", 3))
   {  // Search for DBA in Name1_2
      memcpy(pOutbuf+OFF_DBA, pName1_2, XSIZ_NAME1_2);
      *pName1_2 = 0;
      iDbaCnt++;
   } else if (pTmp = _strstrn(pName1_1, " DBA", XSIZ_NAME1))
   {
      *pTmp++ = 0;
      iNameLen = strlen(pName1_1);
      strcpy(acName1, pName1_1);
      blankRem(acName1);
      if (*(pOutbuf+OFF_DBA) == ' ')
      {
         iTmp = (XSIZ_NAME1+XSIZ_NAME2)-iNameLen;
         memcpy(acTmp, pTmp, --iTmp);
         iNameLen = blankRem(acTmp, iTmp);
         vmemcpy(pOutbuf+OFF_DBA, acTmp, SIZ_DBA, iNameLen);
         iDbaCnt++;
      }
      *pName2 = 0;
      *pName1_2 = 0;
   }

   // Save Name2 
   if (*pName2 >= '0')
   {
      memcpy(acTmp, pName2, XSIZ_NAME2);
      acTmp[XSIZ_NAME2] = 0;
      if (!strchr(acTmp, '#'))
      {
         memcpy(acName2, pName2, XSIZ_NAME2);
         bMarkName2 = markLastName(myTrim(acName2, XSIZ_NAME2));
      }
   }

   // Mark name1
   if (!acName1[0])
   {
      strncpy(acName1, pName1_1, XSIZ_NAME1_1);
      myTrim(acName1, XSIZ_NAME1_1);
   }
   bMarkName1 = markLastName(acName1);

   // Save Name1_2
   memcpy(acName1_2, pName1_2, XSIZ_NAME1_2);
   myBTrim(acName1_2, XSIZ_NAME1_2);

   // Looking for 'AND' in name1
   pTmp = (char *)&acName1[strlen(acName1)-4];
   if (!memcmp(pTmp, " AND", 4))
   {
      if (*pName1_2 > ' ')
      {
         if (markLastName(acName1_2))
            iNameFlg = 4;   // Set this flag to remember that after AND there is another last name

         // Drop 'AND' at the end of Name1_2 then append to Name1_1
         iTmp = strlen(acName1_2);
         if (iTmp > 4)
         {
            pTmp = (char *)&acName1_2[iTmp-4];
            if (!memcmp(pTmp, " AND", 4))
               *pTmp = 0;
         }
         strcat(acName1, " ");
         strcat(acName1, acName1_2);
      } else if (acName2[0] > ' ')
      {
         // If there are more than one names in Name2, process it separately
         // Name1 = LUNA,JOSE C AND NORMA AND  Name2=VALADEZ,XAVIER AND RAQUEL
         if (!strstr(acName2, " AND "))
         {
            // If Name1_1 has two 'AND' then we cannot append Name2 to Name1_1
            // Name1=HERNANDEZ,ALEX P AND TANYA A AND   Name2=WHETSTINE,DARLENE
            if (!strstr(acName1, " AND "))
            {
               // Append Name2 to Name1_1 if both names seem to contain a last name
               if (bMarkName1 && bMarkName2)
               {
                  strcat(acName1, " ");
                  strcat(acName1, acName2);
                  acName2[0] = 0;
                  if (bMarkName2)
                     iNameFlg = 4;
               } else
               {
                  // BURDETTE,ROBERT AND                                             ASSOCIATES INC
                  //*pTmp = 0;           // Drop 'AND' from Name1_1 
                  strcat(acName1, " ");
                  strcat(acName1, acName2);
                  acName2[0] = 0;
                  bMarkName1 = false;
               }
            } else
            {
               *pTmp = 0;
            }
         } else
            *pTmp = 0;                 // Drop 'AND' from Name1_1 and keep two name separate
      } else
         *pTmp = 0;                    // Should not get here
   } else if (!memcmp(acName1_2, "AND ", 4))
   {  // FRIEDLANDER BERKOWITZ, BARBARA  AND BERKOWITZ,STEVEN
      strcpy(acTmp, &acName1_2[4]);
      if (markLastName(acTmp))
         iNameFlg = 4;

      // Drop 'AND' at the end of Name1_2 then append to Name1_1
      iTmp = strlen(acTmp);
      if (iTmp > 4)
      {
         pTmp = (char *)&acTmp[iTmp-4];
         if (!memcmp(pTmp, " AND", 4))
            *pTmp = 0;                    // 8950036065
      }
      strcat(acName1, " AND ");
      strcat(acName1, acTmp);
   } else if (!bMarkName1 && (strlen(acName1_2) > 0))
   {  // Company name - keep as is
      strcat(acName1, " ");
      strcat(acName1, acName1_2);
   } else if (acName2[0] <= ' ' && acName1_2[0] > ' ')
   {
      // If this is a TRUST name, keep this as is, don't parse
      if (!memcmp(acName1_2, "TRUST ", 6) || 
          !memcmp(acName1_2, "TRUSTE", 6) ||
          !memcmp(acName1_2, "REVOC", 5) ||
          !memcmp(acName1_2, "LIVING ", 7))
      {
         strcat(acName1, " ");
         strcat(acName1, acName1_2);
         acName1_2[0] = 0;
      } else if (strstr(acName1_2, " TRUST"))
      {
         strcpy(acSave, acName1_2);
         iTmp = strlen(acSave);
         if (iTmp > 4)
         {
            pTmp = (char *)&acSave[iTmp-4];
            if (!memcmp(pTmp, " AND", 4))
               *pTmp = 0;
         }
      } else if (acName2[0] < 'A')
      {
         strcpy(acName2, acName1_2);
         acName1_2[0] = 0;
      } else
      {
         strcat(acName1, " ");
         strcat(acName1, acName1_2);
         acName1_2[0] = 0;
      }
   }

   // Initialize the vesting field to blank.
   myOwner.acVest[0] = 0;

   if ( (pTmp=strstr(acName1, " ETAL")) || (pTmp=strstr(acName1, " ET AL")) ) 
   {
      *pTmp = 0;
      strcpy(myOwner.acVest, "EA");
   }
   else if ( (pTmp=strstr(acName1, " FAMILY TRUST")) || (pTmp=strstr(acName1, " FAMTRUST")) ||
             (pTmp=strstr(acName1, " FAMILYTR")) || (pTmp=strstr(acName1, " FMLYTRUST")) )
   {
      strcpy(myOwner.acVest, "FM");
   }
   else if ( (pTmp=strstr(acName1, " LVGTRUST")) || (pTmp=strstr(acName1, " REV LIVING TRUST")) || (pTmp=strstr(acName1, " LIVING TRUST")) ||
         (pTmp=strstr(acName1, " LIVTRUST")) || (pTmp=strstr(acName1, " LVTRUST")) )
   {
      strcpy(myOwner.acVest, "LV");
   }
   else if (pTmp=strstr(acName1, " REVOC TRUST"))
   {
      strcpy(myOwner.acVest, "RT");
   }
   else if ( (pTmp=strstr(acName1, " TRS")) || (pTmp=strstr(acName1, " CO TR")) || (pTmp=strstr(acName1, " TR ")) )
   {
      *pTmp = 0;
      strcpy(myOwner.acVest, "TR");
   }
   else if ( (pTmp=strstr(acName1, " LLC")) || (pTmp=strstr(acName1, " LLLP")) || (pTmp=strstr(acName1, " LLP")) ||
         (pTmp=strstr(acName1, " INC")) || (pTmp=strstr(acName1, " CORP")) || (pTmp=strstr(acName1, " LP")) || 
         (pTmp=strstr(acName1, "LLC.")) )
   {
      strcpy(myOwner.acVest, "CO");
   }
   else if ( (pTmp=strstr(acName1, "CITY OF ")) || (pTmp=strstr(acName1, "COUNTY OF ")) || (pTmp=strstr(acName1, "L A COUNTY")) ||
         (pTmp=strstr(acName1, "LA COUNTY")) || (pTmp=strstr(acName1, "LOS ANGELES COUNTY")) || (pTmp=strstr(acName1, "STATE OF ")) || 
         (pTmp=strstr(acName1, "UNITED STATES ")) || (pTmp=strstr(acName1, "USLD ")) || (pTmp=strstr(acName1, "U S GOVT ")) )
   {
      strcpy(myOwner.acVest, "GV");
   }
   else if ( (pTmp=strstr(acName1, " IRREVOCABLE TRUST")) || (pTmp=strstr(acName1, " IRRTRUST")) || (pTmp=strstr(acName1, " IRREVTRUST")) ||
         (pTmp=strstr(acName1, " IRREVOCTRUST")) )
   {
      strcpy(myOwner.acVest, "IR");
   }
   else if (pTmp=strstr(acName1, " TRUST"))
   {
      strcpy(myOwner.acVest, "TR");
   }
   else if (pTmp=strstr(acName1, " JT"))
   {
      strcpy(myOwner.acVest, "JT");
   }
   else if ( (pTmp=strstr(acName1, " PARTNER")) || (pTmp=strstr(acName1, " PTNRSHP")) || (pTmp=strstr(acName1, " PTNSHIP")) ||
             (pTmp=strstr(acName1, " PTNRS")) || (pTmp=strstr(acName1, " PTNR")) )
   {
      strcpy(myOwner.acVest, "PA");
   } 

   // Check on name2
   if (acName2[0])
   {
      if (pTmp1=strstr(acName2, " ETAL"))
         *pTmp1 = 0;
      else if (pTmp1=strstr(acName2, " ET AL"))
         *pTmp1 = 0;
      else if (pTmp1=strstr(acName2, " CO TR"))
         *pTmp1 = 0;
      else if (pTmp1=strstr(acName2, " TR "))
         *pTmp1 = 0;
      else if (pTmp1=strstr(acName2, " TRS"))
         *pTmp1 = 0;
   }

   // Store vesting
   iTmp = strlen(myOwner.acVest);
   if (iTmp > 1)
      memcpy(pOutbuf+OFF_VEST, myOwner.acVest, iTmp);

   // Check for bad character
   strcpy(acTmp, acName1);
   if (bDebug && (iTmp = replSqlChar(acName1)))
      LogMsg("*** Bad character in Owner1 at %.12s [%s]", pOutbuf, acTmp);

   // If corp, keep as is
   if (!bMarkName1)
   {
      iTmp = blankRem(acName1);
      vmemcpy(pOutbuf+OFF_NAME1, acName1, SIZ_NAME1);
      vmemcpy(pOutbuf+OFF_NAME_SWAP, acName1, SIZ_NAME1);
   } else
   {
      // Now parse owners
      splitOwner(acName1, &myOwner, iNameFlg);

      if (myOwner.acName2[0] <= ' ' && acName2[0] >= '0')
      {
         strcpy(acTmp, acName2);
         if (iTmp = replSqlChar(acName2))
            LogMsg("*** Bad character in Owner2 at %.12s [%s]", pOutbuf, acTmp);
         splitOwner(acName2, &myOwner2, 5);
         strcpy(myOwner.acName2, myOwner2.acName1);
      }

      // Store Name1
      clearMarker(myOwner.acName1);
      iTmp = blankRem(myOwner.acName1);
      vmemcpy(pOutbuf+OFF_NAME1, myOwner.acName1, iTmp);

      // Store Name2
      if (myOwner.acName2[0] > ' ')
      {
         clearMarker(myOwner.acName2);
         vmemcpy(pOutbuf+OFF_NAME2, myOwner.acName2, SIZ_NAME2);
      } else if (acSave[0] >= '0' && *(pOutbuf+OFF_NAME2) == ' ')
      {
         vmemcpy(pOutbuf+OFF_NAME2, acSave, SIZ_NAME2);
      }

      if (myOwner.acSwapName[0] > ' ')
      {
         clearMarker(myOwner.acSwapName);
         iTmp = blankRem(myOwner.acSwapName);
         vmemcpy(pOutbuf+OFF_NAME_SWAP, myOwner.acSwapName, SIZ_NAME_SWAP);
      }
   }
}

/*******************************************************************************
 *
 *******************************************************************************/

//void Lax_MergeOwners(OWNER *pOwners, char *pNames)
//{
//   int   iTmp, iNameFlg=5;
//   char  acTmp[128], acSave[128];
//   char  acName1[128], acName2[128], acVest[8];
//   char  *pTmp, *pTmp1, *pName1_1, *pName1_2, *pName2;
//   bool  bMarkName1=false, bMarkName2=false;
//
//   OWNER myOwner, myOwner2;
//
//   // Clear output buffer if needed
//   memset((void *)pOwners, ' ', sizeof(OWNER));
//
//   // Initialize
//   pName1_1 = pNames;
//   pName1_2 = pNames+RSIZ_NAME2;
//   pTmp1 = pNames+RSIZ_NAME1;
//   pName2 = pTmp1+RSIZ_SPCNAMETYPE+RSIZ_SPCNAME;
//
//   memset((void *)&myOwner, 0, sizeof(OWNER));
//
//   acName2[0] = 0;
//   acVest[0] = 0;
//   acSave[0] = 0;
//
//   // Check for CareOf
//   if (!memcmp(pTmp1, "C/O", 3))
//      memcpy(pOwners->acCareOf, pTmp1+RSIZ_SPCNAMETYPE, SIZ_CARE_OF);
//
//   // Search for DBA in Name1_2
//   if (!memcmp(pName1_2, "DBA", 3))
//   {
//      memcpy(pOwners->acDba, pName1_2, XSIZ_NAME1_2);
//      *pName1_2 = 0;
//      iDbaCnt++;
//   } else if (pTmp = _strstrn(pName1_1, " DBA", XSIZ_NAME1))
//   {
//      *pTmp++ = 0;
//      int iNameLen = strlen(pName1_1);
//      strcpy(acName1, pName1_1);
//      blankRem(acName1);
//      iTmp = (XSIZ_NAME1+XSIZ_NAME2)-iNameLen;
//      memcpy(acTmp, pTmp, --iTmp);
//      iNameLen = blankRem(acTmp, iTmp);
//      vmemcpy(pOwners->acDba, acTmp, SIZ_DBA, iNameLen);
//      iDbaCnt++;
//
//      *pName2 = 0;
//      *pName1_2 = 0;
//   }
//
//   // Save Name2
//   if (*pName2 >= '0')
//   {
//      memcpy(acName2, pName2, RSIZ_NAME2);
//      bMarkName2 = markLastName(myTrim(acName2, RSIZ_NAME2));
//   }
//
//   // Mark name1
//   memcpy(acName1, pName1_1, RSIZ_NAME2);
//   bMarkName1 = markLastName(myTrim(acName1, RSIZ_NAME2));
//
//   // Looking for 'AND' in name1
//   pTmp = (char *)&acName1[strlen(acName1)-4];
//   if (!memcmp(pTmp, " AND", 4))
//   {
//      if (*pName1_2 > ' ')
//      {
//         memcpy(acTmp, pName1_2, RSIZ_NAME2);
//         if (markLastName(myTrim(acTmp, RSIZ_NAME2)))
//            iNameFlg = 4;   // Set this flag to remember that after AND there is another last name
//
//         // Drop 'AND' at the end of Name1_2 then append to Name1_1
//         iTmp = strlen(acTmp);
//         if (iTmp > 4)
//         {
//            pTmp = (char *)&acTmp[iTmp-4];
//            if (!memcmp(pTmp, " AND", 4))
//               *pTmp = 0;
//         }
//         strcat(acName1, " ");
//         strcat(acName1, acTmp);
//      } else if (acName2[0] > ' ')
//      {
//         // If there are more than one names in Name2, process it separately
//         // Name1 = LUNA,JOSE C AND NORMA AND  Name2=VALADEZ,XAVIER AND RAQUEL
//         if (!strstr(acName2, " AND "))
//         {
//            // If Name1_1 has two 'AND' then we cannot append Name2 to Name1_1
//            // Name1=HERNANDEZ,ALEX P AND TANYA A AND   Name2=WHETSTINE,DARLENE
//            if (!strstr(acName1, " AND "))
//            {
//               // Append Name2 to Name1_1 if both names seem to contain a last name
//               if (bMarkName1 && bMarkName2)
//               {
//                  strcat(acName1, " ");
//                  strcat(acName1, acName2);
//                  acName2[0] = 0;
//                  if (bMarkName2)
//                     iNameFlg = 4;
//               } else
//                  *pTmp = 0;           // Drop 'AND' from Name1_1
//            } else
//               *pTmp = 0;
//         } else
//            *pTmp = 0;  // Drop 'AND' from Name1_1
//      } else
//         *pTmp = 0;
//   } else if (!memcmp(pName1_2, "AND ", 4))
//   {  // FRIEDLANDER BERKOWITZ, BARBARA  AND BERKOWITZ,STEVEN
//      memcpy(acTmp, pName1_2+4, RSIZ_NAME2-4);
//      if (markLastName(myTrim(acTmp, RSIZ_NAME2-4)))
//         iNameFlg = 4;
//
//      // Drop 'AND' at the end of Name1_2 then append to Name1_1
//      iTmp = strlen(acTmp);
//      if (iTmp > 4)
//      {
//         pTmp = (char *)&acTmp[iTmp-4];
//         if (!memcmp(pTmp, " AND", 4))
//            *pTmp = 0;
//      }
//      strcat(acName1, " AND ");
//      strcat(acName1, acTmp);
//   } else if (acName2[0] <= ' ' && *pName1_2 > ' ')
//   {
//      // If this is a TRUST name, keep this as is, don't parse
//      memcpy(acTmp, pName1_2, RSIZ_NAME2);
//      myTrim(acTmp, RSIZ_NAME2);
//      if (strstr(acTmp, " TRUST"))
//      {
//         strcpy(acSave, acTmp);
//         iTmp = strlen(acSave);
//         if (iTmp > 4)
//         {
//            pTmp = (char *)&acSave[iTmp-4];
//            if (!memcmp(pTmp, " AND", 4))
//               *pTmp = 0;
//         }
//      } else
//      {
//         strcat(acName1, " ");
//         strcat(acName1, acTmp);
//      }
//   }
//
//   if (pTmp=strstr(acName1, " ETAL"))
//      *pTmp = 0;
//   else if (pTmp=strstr(acName1, " ET AL"))
//      *pTmp = 0;
//   else if (pTmp=strstr(acName1, " TRS"))
//      *pTmp = 0;
//   else if (pTmp=strstr(acName1, " CO TR"))
//      *pTmp = 0;
//   else if (pTmp=strstr(acName1, " TR "))
//      *pTmp = 0;
//
//   // Check on name2
//   if (acName2[0])
//   {
//      if (pTmp1=strstr(acName2, " ETAL"))
//         *pTmp1 = 0;
//      else if (pTmp1=strstr(acName2, " ET AL"))
//         *pTmp1 = 0;
//      else if (pTmp1=strstr(acName2, " CO TR"))
//         *pTmp1 = 0;
//      else if (pTmp1=strstr(acName2, " TR "))
//         *pTmp1 = 0;
//      else if (pTmp1=strstr(acName2, " TRS"))
//         *pTmp1 = 0;
//   }
//
//   // Now parse owners
//   splitOwner(acName1, &myOwner, iNameFlg);
//
//   if (myOwner.acName2[0] <= ' ' && acName2[0] >= '0')
//   {
//      splitOwner(acName2, &myOwner2, 5);
//      strcpy(myOwner.acName2, myOwner2.acName1);
//   }
//
//   // Store Name1
//   clearMarker(myOwner.acName1);
//   iTmp = strlen(myOwner.acName1);
//   if (iTmp > SIZ_NAME1) iTmp = SIZ_NAME1;
//   memcpy(pOwners->acName1, myOwner.acName1, iTmp);
//
//   // Store Name2
//   if (myOwner.acName2[0] > ' ')
//   {
//      clearMarker(myOwner.acName2);
//      iTmp = strlen(myOwner.acName2);
//      if (iTmp > SIZ_NAME2) iTmp = SIZ_NAME2;
//      memcpy(pOwners->acName2, myOwner.acName2, iTmp);
//   } else if (acSave[0] >= '0')
//   {
//      iTmp = strlen(acSave);
//      if (iTmp > SIZ_NAME2) iTmp = SIZ_NAME2;
//      memcpy(pOwners->acName2, acSave, iTmp);
//   }
//
//   if (myOwner.acSwapName[0] > ' ')
//   {
//      clearMarker(myOwner.acSwapName);
//      iTmp = strlen(myOwner.acSwapName);
//      if (iTmp > SIZ_NAME_SWAP) iTmp = SIZ_NAME_SWAP;
//      memcpy(pOwners->acSwapName, myOwner.acSwapName, iTmp);
//   }
//
//   if (myOwner.acTitle[0] > '0')
//      strcpy(pOwners->acTitle, myOwner.acTitle);
//}

/********************************* Lax_MergeAdr ******************************
 *
 * Merge address into roll record
 *
 *****************************************************************************/

void Lax_MergeMAdr(char *pOutbuf, char *pRollRec)
{
   LAX_ROLL  *pRec;
   char     *pTmp, acTmp[256], acAddr1[128];
   char     acStr[64], acSfx[16], acCity[64], acState[4];
   int      iTmp, iStrNo;
   int      iIdx=0;

   pRec = (LAX_ROLL *)pRollRec;

   // Clear old Mailing
   removeMailing(pOutbuf, false);

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "4025018214", 10))
   //   iStrNo = 0;
#endif
   // StrNum - remove leading 0
   iStrNo = atoin(pRec->M_StrNo, RSIZ_MSTRNUM);
   acAddr1[0] = 0;
   if (iStrNo > 0)
   {
      // Right justify strNo
      iTmp = sprintf(acTmp, "%d", iStrNo);
      memcpy(pOutbuf+OFF_M_STRNUM, acTmp, iTmp);
      sprintf(acAddr1, "%d", iStrNo);

      // StrSub
      //if (pRec->M_StrSub[0] > ' ')
      if (isalnum(pRec->M_StrSub[0]))
      {
         memcpy(pOutbuf+OFF_M_STR_SUB, pRec->M_StrSub, RSIZ_MSTRSUB);
         memcpy(acTmp, pRec->M_StrSub, RSIZ_MSTRSUB);
         strcat(acAddr1, " ");
         strcat(acAddr1, myTrim(acTmp, RSIZ_MSTRSUB));
      }

      // StrDir
      if (pRec->M_Dir[0] > ' ')
      {
         *(pOutbuf+OFF_M_DIR) = pRec->M_Dir[0];
         sprintf(acTmp, " %c", pRec->M_Dir[0]);
         strcat(acAddr1, acTmp);
      }
      strcat(acAddr1, " ");
   }

   // Check for bad character
   if (iTmp = replUnPrtChar(pRec->M_StrName, ' ', RSIZ_MSTRNAME))
      LogMsg0("*** Bad character in M_StrName at %.12s", pOutbuf);

   // Mail street name & suffix
   memcpy(acTmp, pRec->M_StrName, RSIZ_MSTRNAME);
   myTrim(acTmp, RSIZ_MSTRNAME);
   strcat(acAddr1, acTmp);
   if (acTmp[0] > ' ')
   {
      parseStreet(acTmp, acStr, acSfx, NULL);

      iTmp = strlen(acStr);
      if (iTmp > SIZ_M_STREET) iTmp = SIZ_M_STREET;
      memcpy(pOutbuf+OFF_M_STREET, acStr, iTmp);
      memcpy(pOutbuf+OFF_M_SUFF, acSfx, strlen(acSfx));
   }

   // UnitNo
   iTmp = 0;
   for (iIdx = 0; iIdx < RSIZ_MUNITNO; iIdx++)
   {
      if (pRec->M_Unit[iIdx] != ' ')
         acTmp[iTmp++] = pRec->M_Unit[iIdx];
   }
   if (iTmp > 0)
   {
      acTmp[iTmp] = 0;
      if (!memcmp(acTmp, "NO.", 3) || !memcmp(acTmp, "APT", 3) || !memcmp(acTmp, "STE", 3))
      {
         pTmp = (char *)&acTmp[2];
         *pTmp = '#';
      } else if (!memcmp(acTmp, "NO", 2) || !memcmp(acTmp, "RM", 2))
      {
         pTmp = (char *)&acTmp[1];
         *pTmp = '#';
      } else if (!memcmp(acTmp, "UNIT", 4))
      {
         if (acTmp[4] == ' ')
            pTmp = (char *)&acTmp[4];
         else
            pTmp = (char *)&acTmp[3];
         *pTmp = '#';
      } else if (!memcmp(acTmp, "UNT", 3))
      {
         if (acTmp[3] == ' ')
            pTmp = (char *)&acTmp[3];
         else
            pTmp = (char *)&acTmp[2];
         *pTmp = '#';
      } else if (acTmp[0] == '#')
      {
         pTmp = (char *)&acTmp[0];
      } else
      {
         strcpy(&acStr[1], acTmp);
         acStr[0] = '#';
         pTmp = (char *)&acStr[0];
      }

      memcpy(pOutbuf+OFF_M_UNITNO, pTmp, strlen(pTmp));
      memcpy(pOutbuf+OFF_M_UNITNOX, pTmp, strlen(pTmp));
      strcat(acAddr1, " ");
      strcat(acAddr1, pTmp);
   }
   memcpy(pOutbuf+OFF_M_ADDR_D, acAddr1, strlen(acAddr1));

   // Replace known bad char
   if (pRec->M_City[6] == 0x1A)
      pRec->M_City[6] = 'U';
   else if (iTmp = replUnPrtChar(pRec->M_City, ' ', RSIZ_MCITYST))
      LogMsg0("*** Bad character in M_City at %.12s", pOutbuf);

   // Mailing city
   memcpy(pOutbuf+OFF_M_CTY_ST_D, pRec->M_City, RSIZ_MCITYST);
   if (pRec->M_City[0] > ' ')
   {
      memcpy(acTmp, pRec->M_City, RSIZ_MCITYST);
      parseCitySt(myTrim(acTmp, RSIZ_MCITYST), acCity, acState, NULL);
      memcpy(pOutbuf+OFF_M_CITY, acCity, strlen(acCity));
      memcpy(pOutbuf+OFF_M_ST, acState, strlen(acState));
   }

   // Mailing Zip
   if (pRec->M_Zip[0] >= '0' && pRec->M_Zip[0] <= '9' && memcmp(pRec->M_Zip, "99999", 5))
      memcpy(pOutbuf+OFF_M_ZIP, pRec->M_Zip, SIZ_M_ZIP);
}

void Lax_MergeSAdr(char *pOutbuf, char *pRollRec)
{
   LAX_ROLL  *pRec;
   char     *pTmp, acTmp[256], acAddr1[128];
   char     acStr[64], acSfx[16], acSfxCode[16], acCityCode[64], acState[4];
   int      iTmp, iStrNo;
   int      iIdx=0;

   pRec = (LAX_ROLL *)pRollRec;

#ifdef _DEBUG
   //if (!memcmp(pRec->APN, "2004002001", 10))
   //   iTmp = 0;
#endif

   // Remove old address
   removeSitus(pOutbuf);

   // StrNum - remove leading 0
   iStrNo = atoin(pRec->S_StrNo, RSIZ_SSTRNUM);
   acAddr1[0] = 0;
   if (iStrNo > 0)
   {
      // Right justify strNo
      iTmp = sprintf(acAddr1, "%d ", iStrNo);
      memcpy(pOutbuf+OFF_S_STRNUM, acAddr1, iTmp);
      memcpy(pOutbuf+OFF_S_HSENO,  acAddr1, iTmp);

      // StrSub
      if (isalnum(pRec->S_StrSub[0]))
      {
         memcpy(pOutbuf+OFF_S_STR_SUB, pRec->S_StrSub, RSIZ_SSTRSUB);
         memcpy(acTmp, pRec->S_StrSub, RSIZ_SSTRSUB);
         strcat(acAddr1, myTrim(acTmp, RSIZ_SSTRSUB));
         strcat(acAddr1, " ");
      } 

      // StrDir
      if (pRec->S_Dir[0] > 'A')
      {
         *(pOutbuf+OFF_S_DIR) = pRec->S_Dir[0];
         sprintf(acTmp, "%c ", pRec->S_Dir[0]);
         strcat(acAddr1, acTmp);
      }
   }

   // Situs street name & suffix
   memcpy(acTmp, pRec->S_StrName, RSIZ_SSTRNAME);
   myTrim(acTmp, RSIZ_SSTRNAME);
   if (acTmp[0] > ' ')
   {
      while (pTmp = strchr(acTmp, '\\'))
         *pTmp = '/';
      if (memcmp(acTmp, "VAC/", 4))
      {
         strcat(acAddr1, acTmp);
         if (acTmp[0] != '(')
            parseStreet(acTmp, acStr, acSfx, acSfxCode);
         else
         {
            strcpy(acStr, acTmp);
            acSfxCode[0] = 0;
         }

         vmemcpy(pOutbuf+OFF_S_STREET, acStr, SIZ_S_STREET);
         memcpy(pOutbuf+OFF_S_SUFF, acSfxCode, strlen(acSfxCode));
      } else
      {
         pTmp = (char *)&acTmp[4];
         strcat(acAddr1, pTmp);
         vmemcpy(pOutbuf+OFF_S_STREET, pTmp, SIZ_S_STREET);
      }
   }

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "4324007026", 10))
   //   iTmp = 0;
#endif

   // UnitNo
   if (!memcmp(pRec->S_Unit, "NO", 2))
   {
      memcpy(&acTmp[1], (char *)&pRec->S_Unit[2], RSIZ_SUNITNO-2);
      acTmp[RSIZ_SUNITNO-1] = 0;
      acTmp[0] = '#';
   } else if (!memcmp(pRec->S_Unit, "UNIT", 4))
   {
      memcpy(&acTmp[1], (char *)&pRec->S_Unit[4], RSIZ_SUNITNO-4);
      acTmp[RSIZ_SUNITNO-3] = 0;
      acTmp[0] = '#';
   } else if (!memcmp(pRec->S_Unit, "UNT", 3))
   {
      memcpy(&acTmp[1], (char *)&pRec->S_Unit[3], RSIZ_SUNITNO-3);
      acTmp[RSIZ_SUNITNO-2] = 0;
      acTmp[0] = '#';
   } else if (!memcmp(pRec->S_Unit, "STE", 3))
   {
      if (pRec->S_Unit[3] == '.' || pRec->S_Unit[3] == '#')
      {
         memcpy(&acTmp[1], (char *)&pRec->S_Unit[4], RSIZ_SUNITNO-4);
         acTmp[RSIZ_SUNITNO-3] = 0;
         acTmp[0] = '#';
      } else
      {
         iTmp = 3;
         while ((pRec->S_Unit[iTmp] == ' ' || pRec->S_Unit[iTmp] == '0') && iTmp < RSIZ_SUNITNO)
            iTmp++;
         if (iTmp < RSIZ_SUNITNO)
         {
            memcpy(&acTmp[1], &pRec->S_Unit[iTmp], RSIZ_SUNITNO-iTmp);
            acTmp[RSIZ_SUNITNO-iTmp+1] = 0;
            acTmp[0] = '#';
         }
      }
   } else if (!memcmp(pRec->S_Unit, "RM", 2))
   {
      iTmp = atoin(&pRec->S_Unit[2], RSIZ_SUNITNO-2);
      if (iTmp > 0)
         sprintf(acTmp, "RM %d", iTmp);
      else
      {
         iTmp = 2;
         while ((pRec->S_Unit[iTmp] == ' ' || pRec->S_Unit[iTmp] == '0') && iTmp < RSIZ_SUNITNO)
            iTmp++;
         if (iTmp < RSIZ_SUNITNO)
         {
            sprintf(acTmp, "RM %.*s", RSIZ_SUNITNO-iTmp, &pRec->S_Unit[iTmp]);
         }
      }
   } else if (!memcmp(pRec->S_Unit, "SPACE", 5))
   {
      iTmp = atoin(&pRec->S_Unit[5], RSIZ_SUNITNO-5);
      if (iTmp > 0)
         sprintf(acTmp, "SP %d", iTmp);
      else
      {
         iTmp = 5;
         while ((pRec->S_Unit[iTmp] == ' ' || pRec->S_Unit[iTmp] == '0') && iTmp < RSIZ_SUNITNO)
            iTmp++;
         if (iTmp < RSIZ_SUNITNO)
         {
            sprintf(acTmp, "SP %.*s", RSIZ_SUNITNO-iTmp, &pRec->S_Unit[iTmp]);
         }
      }
   } else if (!memcmp(pRec->S_Unit, "SPC", 3))
   {
      iTmp = atoin(&pRec->S_Unit[3], RSIZ_SUNITNO-3);
      if (iTmp > 0)
         sprintf(acTmp, "SP %d", iTmp);
      else
      {
         iTmp = 3;
         while ((pRec->S_Unit[iTmp] == ' ' || pRec->S_Unit[iTmp] == '0') && iTmp < RSIZ_SUNITNO)
            iTmp++;
         if (iTmp < RSIZ_SUNITNO)
         {
            sprintf(acTmp, "SP %.*s", RSIZ_SUNITNO-iTmp, &pRec->S_Unit[iTmp]);
         }
      }
   } else if (!memcmp(pRec->S_Unit, "APT", 3))
   {
      iTmp = atoin(&pRec->S_Unit[3], RSIZ_SUNITNO-3);
      if (iTmp > 0)
      {
         memcpy(acTmp, &pRec->S_Unit[3], RSIZ_SUNITNO-3);
         if (pTmp = isCharIncluded(acTmp, RSIZ_SUNITNO-3))
         {
            acTmp[RSIZ_SUNITNO-3] = 0;
            sprintf(acTmp, "#%d%s", iTmp, pTmp);
         } else
            sprintf(acTmp, "#%d", iTmp);
      } else
      {
         iTmp = 3;
         while ((pRec->S_Unit[iTmp] == ' ' || pRec->S_Unit[iTmp] == '0') && iTmp < RSIZ_SUNITNO)
            iTmp++;
         if (iTmp < RSIZ_SUNITNO)
         {
            memcpy(&acTmp[1], &pRec->S_Unit[iTmp], RSIZ_SUNITNO-iTmp);
            acTmp[RSIZ_SUNITNO-iTmp+1] = 0;
            acTmp[0] = '#';
         }
      }
   } else if (pRec->S_Unit[0] == '0')
   {
      iTmp = 0;
      while (pRec->S_Unit[iTmp] == '0' && iTmp < RSIZ_SUNITNO)
         iTmp++;
      if (iTmp < RSIZ_SUNITNO)
      {
         memcpy(&acTmp[1], &pRec->S_Unit[iTmp], RSIZ_SUNITNO-iTmp);
         acTmp[RSIZ_SUNITNO-iTmp+1] = 0;
         acTmp[0] = '#';
      } else
         acTmp[0] = 0;
   } else if (pRec->S_Unit[0] > ' ')
   {
      iTmp = atoin(pRec->S_Unit, RSIZ_SUNITNO);
      if (iTmp > 0)
         sprintf(acTmp, "#%d", iTmp);
      else
         memcpy(acTmp, pRec->S_Unit, RSIZ_SUNITNO);
      acTmp[RSIZ_SUNITNO] = 0;
   } else
      acTmp[0] = 0;

   iTmp = blankRem(acTmp);
   if (iTmp > 0)
   {
      if (memcmp(acTmp, "BLDG", 4) && memcmp(acTmp, "RM", 2) && memcmp(acTmp, "SP", 2))
      {
         // Remove all spaces in the middle of UnitNo
         iTmp = remChar(acTmp, ' ');
      }

      if (iTmp > 0)
      {
         vmemcpy(pOutbuf+OFF_S_UNITNO, acTmp, SIZ_S_UNITNO, iTmp);
         vmemcpy(pOutbuf+OFF_S_UNITNOX, acTmp, SIZ_S_UNITNOX, iTmp);
         strcat(acAddr1, " ");
         strcat(acAddr1, acTmp);
      } 
   }

   blankRem(acAddr1);
   memcpy(pOutbuf+OFF_S_ADDR_D, acAddr1, strlen(acAddr1));

   // Situs city
   memcpy(pOutbuf+OFF_S_CTY_ST_D, pRec->S_City, RSIZ_SCITYST);
   if (pRec->S_City[0] > ' ')
   {
      memcpy(acTmp, pRec->S_City, RSIZ_SCITYST);
      parseCitySt(myTrim(acTmp, RSIZ_SCITYST), NULL, acState, acCityCode);
      memcpy(pOutbuf+OFF_S_CITY, acCityCode, strlen(acCityCode));
      memcpy(pOutbuf+OFF_S_ST, "CA", SIZ_S_ST);
   } 

   // Situs Zip
   if (pRec->S_Zip[0] > '8')
   {
      if (!memcmp(&pRec->S_Zip[5], "0000", 4))
         iTmp = SIZ_S_ZIP;
      else
         iTmp = SIZ_S_ZIP+SIZ_S_ZIP4;
      memcpy(pOutbuf+OFF_S_ZIP, pRec->S_Zip, iTmp);
   } 
}

/******************************** parseMaseg *********************************
 *
 *   Design:   XXXX
 *             0XX1 = 'P' Pool
 *             0XX2 = Misc
 *             0XX3 = 'P' Pool & Misc
 *             0XX4 = 'S' Spa
 *             0XX5 = 'Z' Pool /w solar heating (0505, 0535 included)
 *             18X1 = Pool (Hotel, Motel)
 *   SFR (01)  0X1X = Heating 'C'  Floor or Wall
 *             0X2X = Heating 'Z'  Central heat (any type)
 *             0X3X = Heating 'Z', Cooling 'C'
 *             0X4X = Heating 'K'  Central Solar
 *   MRU (02+) 0X2X = Cooling 'C'
 *             0X3X = Cooling 'C'
 *   NR        11X1 = Cooling 'X'
 *             12X1
 *             14X1
 *             19X1
 *             21X1
 *             23X1
 *             24X1
 *             29X1
 *             2700 = parking lot
 *             3800 = ??? (not living space)
 * Use code:
 *    1st digit       2nd digit
 *    0 = res.        1-4 : number of units
 *                    5   : 5 or more units
 *    1 = commercial  1   : Stores
 *                    2   : Store/Office
 *                    3   : Dept. store
 *                    4   : Supermarket
 *                    5   : Shopping (N)
 *                    6   : Shopping (R)
 *                    7   : Office
 *                    8   : Hotel
 *                    9   : Professional
 *    2 = commercial  1   : Restaurant
 *                    2   : Wholesale/mfgr
 *                    3   : Banks
 *                    4   : Service
 *                    5   : Gas
 *                    6   : Auto
 *                    7   : Parking
 *    3 = Industrial
 *    4 = Agriculture
 *    5 = Dry Farm
 *    6 = Recreational
 *    7 = Institutional
 *    8 = Miscellaneous
 *
 *****************************************************************************/

void parseMaseg(char *pOutbuf, char *pRollRec)
{
   MASEG *Maseg;
   long  lSqft, lDesign;
   int   iTmp, lBaths, lBeds, lUnits, iBldgs;
   char  acTmp[16], acCode[16];

   Maseg = (MASEG *)(pRollRec+ROFF_SUBPT1);

   lSqft=0;
   lBaths=lBeds=lUnits=iBldgs=0;

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "2004024039", 10))
   //   iTmp = 0;
#endif

   for (iTmp = 0; iTmp < 5; iTmp++)
   {
      lDesign = atoin(Maseg->Design, RSIZ_DESIGN);
      if (!lDesign)
         break;

      // 2700 = parking lot
      if (lDesign != 2700 && lDesign != 3800)
      {
         int iVal;

         iVal = atoin(Maseg->Sqft, RSIZ_BLDGSQFT);
         if (iVal > 100)
            iBldgs++;

         lSqft += iVal;
         lBaths += atoin(Maseg->Baths, RSIZ_BATHS);
         lBeds += atoin(Maseg->Beds, RSIZ_BEDS);
         lUnits += atoin(Maseg->Units, RSIZ_UNITS);
      }
      Maseg++;
   }

   if (iBldgs > 0)
   {
      sprintf(acTmp, "%*u", SIZ_BLDGS, iBldgs);
      memcpy(pOutbuf+OFF_BLDGS, acTmp, SIZ_BLDGS);
      lAssrRecCnt++;
   }
   if (lSqft > 0)
   {
      sprintf(acTmp, "%*u", SIZ_BLDG_SF, lSqft);
      memcpy(pOutbuf+OFF_BLDG_SF, acTmp, SIZ_BLDG_SF);
   }
   if (lBaths > 0)
   {
      sprintf(acTmp, "%*u", SIZ_BATH_F, lBaths);
      memcpy(pOutbuf+OFF_BATH_F, acTmp, SIZ_BATH_F);
   }
   if (lBeds > 0)
   {
      sprintf(acTmp, "%*u", SIZ_BEDS, lBeds);
      memcpy(pOutbuf+OFF_BEDS, acTmp, SIZ_BEDS);
   }

   // Do not use NUMBER_OF_UNITS field
   if (lUnits > 0)
   {
      sprintf(acTmp, "%*u", SIZ_UNITS, lUnits);
      memcpy(pOutbuf+OFF_UNITS, acTmp, SIZ_UNITS);
   }

   // Reset to the first block
   Maseg = (MASEG *)(pRollRec+ROFF_SUBPT1);

   // Pool - Residential or hotel/motel
   if (Maseg->Design[0] == '0' || !memcmp(Maseg->Design, "18", 2))
   {
      switch (Maseg->Design[3])
      {
         case '1':
         case '3':
         case '5':
            *(pOutbuf+OFF_POOL) = 'P';
            break;
         case '4':
            *(pOutbuf+OFF_POOL) = 'S';
            break;
      }
   }

   // Cooling-Heating
   if (!memcmp(pRollRec+ROFF_USE, "01", 2))     // SFR
   {
      switch (Maseg->Design[2])
      {
         case '1':
            *(pOutbuf+OFF_HEAT) = 'C';          // Floor or Wall
            break;
         case '3':
            *(pOutbuf+OFF_AIR_COND) = 'C';      // Central A/C
         case '2':
            *(pOutbuf+OFF_HEAT) = 'Z';          // Central heat
            break;
         case '4':
            *(pOutbuf+OFF_HEAT) = 'K';
            break;
      }
   } else if (*(pRollRec+ROFF_USE) == '0')      // MR
   {
      if (Maseg->Design[2] == '2' || Maseg->Design[2] == '3')
         *(pOutbuf+OFF_AIR_COND) = 'C';
   } else if (Maseg->Design[3] == '1')          // NR
   {
      iTmp = atoin(Maseg->Design, 2);
      switch (iTmp)
      {
         case 11:
         case 12:
         case 14:
         case 19:
         case 21:
         case 23:
         case 24:
         case 29:
            *(pOutbuf+OFF_AIR_COND) = 'X';      // YES
            break;
      }
   }

   // Bldg Class
   if (Maseg->Class[0] >= 'A')
   {
      *(pOutbuf+OFF_BLDG_CLASS) = Maseg->Class[0];
      if (isdigit(Maseg->Class[1]))
      {
         acTmp[0] = Maseg->Class[1];
         acTmp[1] = '.';
         if (isdigit(Maseg->Class[2]))
            acTmp[2] = Maseg->Class[2];
         else
            acTmp[2] = '0';
         acTmp[3] = 0;

         iTmp = Value2Code(acTmp, acCode, NULL);
         memcpy(pOutbuf+OFF_BLDG_QUAL, acCode, strlen(acCode));
      }
   }
}

/******************************** formatMaseg ********************************
 *
 * To format output for Lax_CreateAssrRec()
 *
 *****************************************************************************/

void formatMaseg(char *pOutRec)
{
   MASEG *pMaseg;
   long  lTmp;
   char  acTmp[16];

   pMaseg = (MASEG *)pOutRec;

   lTmp = atoin(pMaseg->SubPt, RSIZ_SUBPT);
   if (!lTmp)
      memset(pMaseg->SubPt, ' ', RSIZ_SUBPT);
   else
   {
      sprintf(acTmp, "%*u", RSIZ_SUBPT, lTmp);
      memcpy(pMaseg->SubPt, acTmp, RSIZ_SUBPT);
   }

   lTmp = atoin(pMaseg->YrBlt, RSIZ_YRBLT);
   if (!lTmp)
      memset(pMaseg->YrBlt, ' ', RSIZ_YRBLT);
   else
   {
      sprintf(acTmp, "%*u", RSIZ_YRBLT, lTmp);
      memcpy(pMaseg->YrBlt, acTmp, RSIZ_YRBLT);
   }

   lTmp = atoin(pMaseg->Units, RSIZ_UNITS);
   if (!lTmp)
      memset(pMaseg->Units, ' ', RSIZ_UNITS);
   else
   {
      sprintf(acTmp, "%*u", RSIZ_UNITS, lTmp);
      memcpy(pMaseg->Units, acTmp, RSIZ_UNITS);
   }

   lTmp = atoin(pMaseg->Beds, RSIZ_BEDS);
   if (!lTmp)
      memset(pMaseg->Beds, ' ', RSIZ_BEDS);
   else
   {
      sprintf(acTmp, "%*u", RSIZ_BEDS, lTmp);
      memcpy(pMaseg->Beds, acTmp, RSIZ_BEDS);
   }

   lTmp = atoin(pMaseg->Baths, RSIZ_BATHS);
   if (!lTmp)
      memset(pMaseg->Baths, ' ', RSIZ_BATHS);
   else
   {
      sprintf(acTmp, "%*u", RSIZ_BATHS, lTmp);
      memcpy(pMaseg->Baths, acTmp, RSIZ_BATHS);
   }

   lTmp = atoin(pMaseg->Sqft, RSIZ_BLDGSQFT);
   if (!lTmp)
      memset(pMaseg->Sqft, ' ', RSIZ_BLDGSQFT);
   else
   {
      sprintf(acTmp, "%*u", RSIZ_BLDGSQFT, lTmp);
      memcpy(pMaseg->Sqft, acTmp, RSIZ_BLDGSQFT);
   }

}

/******************************** formatBldgChg ******************************
 *
 *
 *****************************************************************************/

void formatBldgChg(char *pOutRec)
{
   BLCHG *pBldgChg;
   long  lTmp;
   char  acTmp[16];

   pBldgChg = (BLCHG *)pOutRec;

   lTmp = atoin(pBldgChg->BldgYrChg, RSIZ_BLDGYEARCHG);
   if (!lTmp)
      memset(pBldgChg->BldgYrChg, ' ', RSIZ_BLDGYEARCHG);

   lTmp = atoin(pBldgChg->BldgRcn, RSIZ_BLDGRCN);
   if (!lTmp)
      memset(pBldgChg->BldgRcn, ' ', RSIZ_BLDGRCN);
   else
   {
      sprintf(acTmp, "%*u", RSIZ_BLDGRCN, lTmp);
      memcpy(pBldgChg->BldgRcn, acTmp, RSIZ_BLDGRCN);
   }

   lTmp = atoin(pBldgChg->BldgUnsec, RSIZ_BLDGCOST);
   if (!lTmp)
      memset(pBldgChg->BldgUnsec, ' ', RSIZ_BLDGCOST);
   else
   {
      sprintf(acTmp, "%*u", RSIZ_BLDGCOST, lTmp);
      memcpy(pBldgChg->BldgUnsec, acTmp, RSIZ_BLDGCOST);
   }
}

/******************************** loadRegionTbl ******************************
 *
 * Return number of entries if successful, <=0 if error
 *
 *****************************************************************************/

int loadRegionTbl(char *pRegTblFile)
{
   FILE  *fd;
   char  acTmp[256], *pTmp, *pTmp1;

   // Initialize list
   memset((void *)&LaxRegs[0], ' ', sizeof(REG_IDX)*MAX_LAX_BOOKS);

   // Open region file
   fd = fopen(pRegTblFile, "r");
   if (!fd)
   {
      LogMsg("***** Error opening %s", pRegTblFile);
      return -1;
   }

   pTmp = fgets(acTmp, 256, fd);
   iRegBooks = atoi(pTmp);
   if (iRegBooks > MAX_LAX_BOOKS)
   {
      fclose(fd);
      LogMsg("***** The number of entries in the %s (%d) is more than buffer can handle.  Please consult Sony.", pRegTblFile, iRegBooks);
      return -2;
   }

   iRegBooks = 0;
   while (!feof(fd))
   {
      pTmp = fgets(acTmp, 256, fd);
      if (pTmp)
      {
         if (pTmp1 = strchr(pTmp, ','))
         {
            *pTmp1++ = 0;
            LaxRegs[iRegBooks].iReg = *pTmp1;
            pTmp1 = strchr(pTmp, '-');
            if (pTmp1)
            {
               *pTmp1++ = 0;
               strcpy(LaxRegs[iRegBooks].acStartBook, pTmp);
               strcpy(LaxRegs[iRegBooks].acEndBook, pTmp1);
            } else
            {
               strcpy(LaxRegs[iRegBooks].acStartBook, pTmp);
               strcpy(LaxRegs[iRegBooks].acEndBook, pTmp);
            }
            iRegBooks++;
         } else
            LogMsg("***** Invalid entry %s at %d in %s", pTmp, iRegBooks, pRegTblFile);
      } else
         break;
   }

   fclose(fd);
   return iRegBooks;
}

/******************************** setRegion **********************************
 *
 * Return 0 if successful, Error otherwise
 *
 *****************************************************************************/

int setRegion(char *pOutbuf)
{
   static   int iRegIdx = 0;
   static   char bCurReg = '0';
   int      iRet, iRet1;

   // Look for book range
   while ((iRegIdx < iRegBooks) && ((iRet1=memcmp(pOutbuf, LaxRegs[iRegIdx].acEndBook, myCounty.iBookLen)) > 0))
      iRegIdx++;

   iRet=memcmp(pOutbuf, LaxRegs[iRegIdx].acStartBook, myCounty.iBookLen);
   if (iRet >= 0 && iRet1 <= 0)
   {
      *(pOutbuf+OFF_MAPDIV_LA) = LaxRegs[iRegIdx].iReg;
      iRet = 0;
   } else
   {
      iRet |= iRet1;
      LogMsg("*** Book without region assigned: %.*s", myCounty.iBookLen, pOutbuf);
   }

   return iRet;
}

/******************************* Lax_CreateLienRec ***************************
 *
 * Return 0 if successful, >0 if error
 *
 *****************************************************************************/

int Lax_CreateLienRec(char *pOutbuf, char *pRollRec)
{
   char     acTmp[256];
   long     iTmp;
   LIENEXTR *pLienRec = (LIENEXTR *)pOutbuf;
   LAX_ROLL *pRec = (LAX_ROLL *)pRollRec;
   LONGLONG lTmp;

   // Clear output buffer
   memset(pOutbuf, ' ', sizeof(LIENEXTR));

   // Start copying data
   memcpy(pLienRec->acApn, pRec->APN, RSIZ_APN);
   //memcpy(pLienRec->acTRA, pRec->TRA, RSIZ_TRA);
   // TRA
   lTmp = atoin(pRec->TRA, RSIZ_TRA);
   if (lTmp > 0)
   {
      iTmp = sprintf(acTmp, "%.6d", lTmp);
      memcpy(pLienRec->acTRA, acTmp, iTmp);
   }

   // Year assessed
   memcpy(pLienRec->acYear, myCounty.acYearAssd, 4);

   // Land
   long lLand = atoin(pRec->LandVal, RSIZ_LANDVAL);
   if (lLand > 0)
   {
      iTmp = sprintf(acTmp, "%*u", sizeof(pLienRec->acLand), lLand);
      memcpy(pLienRec->acLand, acTmp, iTmp);
   }

   // Improve
   long lImpr = atoin(pRec->ImprVal, RSIZ_IMPRVAL);
   if (lImpr > 0)
   {
      iTmp = sprintf(acTmp, "%*u", sizeof(pLienRec->acImpr), lImpr);
      memcpy(pLienRec->acImpr, acTmp, iTmp);
   }

   // Other values
   long lPers = atoin(pRec->PPVal, RSIZ_PPVAL);
   long lFixture = atoin(pRec->FixtVal, RSIZ_FIXTVAL);
   lTmp = lPers + lFixture;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*u", SIZ_LIEN_FIXT, lTmp);
      memcpy(pLienRec->acOther, acTmp, SIZ_OTHER);

      if (lPers > 0)
      {
         iTmp = sprintf(acTmp, "%*u", sizeof(pLienRec->acPP_Val), lPers);
         memcpy(pLienRec->acPP_Val, acTmp, iTmp);
      }

      if (lFixture > 0)
      {
         iTmp = sprintf(acTmp, "%*u", sizeof(pLienRec->acME_Val), lFixture);
         memcpy(pLienRec->acME_Val, acTmp, iTmp);
      }
   }

   // Gross total
   lTmp += (lLand+lImpr);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*u", SIZ_LIEN_FIXT, lTmp);
      memcpy(pLienRec->acGross, acTmp, SIZ_LIEN_FIXT);
   }

   // Ratio
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*u", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
      memcpy(pLienRec->acRatio, acTmp, SIZ_RATIO);
   }

   // HO Exempt
   long lExe = 0;
   iTmp = atoin(pRec->NumOfHOE, RSIZ_NUMOFHOE);
   if (iTmp > 0)
   {
      pLienRec->acHO[0] = '1';      // 'Y'
      lExe = atoin(pRec->HOExe, RSIZ_HOEXE);
      iTmp = sprintf(acTmp, "%*u", SIZ_LIEN_HO_EXE, lExe);
      memcpy(pLienRec->extra.Lax.acHOExe, acTmp, iTmp);
      memcpy(pLienRec->extra.Lax.Exe_Code1, "10", 2);
   } else
      pLienRec->acHO[0] = '2';      // 'N'

   long lRelExe = atoin(pRec->RelExe, RSIZ_RELEXE);
   if (lRelExe > 0)
   {
      iTmp = sprintf(acTmp, "%*u", SIZ_LIEN_REL_EXE, lRelExe);
      memcpy(pLienRec->extra.Lax.acRelExe, acTmp, iTmp);
   }
   memcpy(pLienRec->extra.Lax.acHOCnt, pRec->NumOfHOE, RSIZ_NUMOFHOE);
   if (pRec->ExclType > ' ')
   {
      pLienRec->extra.Lax.acExclType[0] = pRec->ExclType;
      if (pLienRec->extra.Lax.Exe_Code1[0] == ' ')
         pLienRec->extra.Lax.Exe_Code1[0] = pRec->ExclType;
      else
         pLienRec->extra.Lax.Exe_Code2[0] = pRec->ExclType;
   }
   pLienRec->extra.Lax.acPP_Key[0] = pRec->PPKey;

   // PP Exe
   long lPPExe = atoin(pRec->PPExe, RSIZ_PPEXE);
   if (lPPExe > 0)
   {
      iTmp = sprintf(acTmp, "%*u", SIZ_LIEN_PERS_EXE, lPPExe);
      memcpy(pLienRec->extra.Lax.acPP_Exe, acTmp, iTmp);
   }

   // Fixt Exe
   long lFixtExe = atoin(pRec->FixtExe, RSIZ_FIXTEXE);
   if (lFixtExe > 0)
   {
      iTmp = sprintf(acTmp, "%*u", SIZ_LIEN_FIXT_EXE, lFixtExe);
      memcpy(pLienRec->extra.Lax.acME_Exe, acTmp, iTmp);
   }

   // Total Exe
   lTmp = lExe+lFixtExe+lPPExe+lRelExe;
   if (lTmp > 0)
   {
      iTmp = sprintf(acTmp, "%*u", sizeof(pLienRec->acExAmt), lTmp);
      memcpy(pLienRec->acExAmt, acTmp, iTmp);
   }

   // Set full exempt flag
   iTmp = atoin(&pRec->APN[ROFF_PARCEL], 3);
   if ((iTmp >= 300 && iTmp < 400) || (iTmp >= 800))
      pLienRec->SpclFlag = LX_FULLEXE_FLG;

   // Prop8
   if (pRec->LandKey == '8')
      pLienRec->SpclFlag |= LX_PROP8_FLG;

   pLienRec->LF[0] = '\n';
   pLienRec->LF[1] = 0;
   return 0;
}

/******************************** Lax_ExtrProp8 *****************************
 *
 *
 ****************************************************************************/

int Lax_ExtrProp8()
{
   char  *pTmp, acBuf[MAX_RECSIZE], acRollRec[MAX_RECSIZE], acOutFile[_MAX_PATH];
   long  lCnt=0, iProp8Cnt=0;
   FILE  *fdOut;

   LogMsg("\nExtract Prop8 flag from lien roll %s", acRollFile);

   // Open roll file
   fdRoll = fopen(acRollFile, "r");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", acRollFile);
      return 2;
   }

   // Open Output file
   sprintf(acOutFile, acProp8Tmpl, myCounty.acCntyCode, myCounty.acCntyCode, myCounty.acYearAssd);
   LogMsg("Create prop8 output file %s", acOutFile);
   fdOut = fopen(acOutFile, "w");
   if (fdOut == NULL)
   {
      LogMsg("***** Error opening Prop8 extract file: %s\n", acOutFile);
      return 4;
   }

   // print header
   fputs("APN,Prop8_Flg\n", fdOut);

   // Merge loop
   while (!feof(fdRoll))
   {
      // Get roll rec
      pTmp = fgets(acRollRec, MAX_RECSIZE, fdRoll);
      if (!pTmp)
         break;

      // Prop8
      if (acRollRec[ROFF_LANDREASONKEY] == '8')
      {
         acRollRec[RSIZ_APN] = 0;
         sprintf(acBuf, "%s,Y\n", acRollRec);

         fputs(acBuf, fdOut);
         iProp8Cnt++;
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdRoll)
      fclose(fdRoll);
   if (fdOut)
      fclose(fdOut);

   LogMsgD("\nTotal prop8 records output: %u", iProp8Cnt);
   LogMsgD("Total records processed:    %u\n", lCnt);

   return 0;
}

/********************************* Lax_ExtrLien *****************************
 *
 *
 ****************************************************************************/

int Lax_ExtrLien()
{
   char  acBuf[MAX_RECSIZE], acRollRec[MAX_RECSIZE], acOutFile[_MAX_PATH];
   long  lCnt=0;
   int   iTmp;

   // Open roll file
   LogMsg("Open Roll file %s", acRollFile);
   fdRoll = fopen(acRollFile, "rb");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening roll file: %s (%d)\n", acRollFile, errno);
      return 2;
   }

   // Open Output file
   sprintf(acOutFile, acLienTmpl, myCounty.acCntyCode, myCounty.acCntyCode);
   LogMsg("Open output file %s", acOutFile);
   fdLien = fopen(acOutFile, "w");
   if (fdLien == NULL)
   {
      LogMsg("***** Error opening Lien extract file: %s\n", acOutFile);
      return 4;
   }

   // Merge loop
   while (!feof(fdRoll))
   {
      // Get roll rec
      iTmp = fread(acRollRec, 1, iRollLen, fdRoll);
      //pTmp = fgets(acRollRec, MAX_RECSIZE, fdRoll);
      if (iTmp < iRollLen)
         break;

      // Create new lien record
      Lax_CreateLienRec(acBuf, acRollRec);

      fputs(acBuf, fdLien);
      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdRoll)
      fclose(fdRoll);
   if (fdLien)
      fclose(fdLien);

   LogMsgD("Total records output:     %d\n", lCnt);

   lRecCnt = lCnt;
   return 0;
}

/******************************* Lax_MergeRoll ****************************
 *
 * Return 0 if successful, >0 if error
 *
 *****************************************************************************/

int Lax_MergeRoll(char *pOutbuf, char *pRollRec, int iFlag)
{
   LAX_ROLL *pRec;
   LONGLONG lTmp;
   char     acTmp[256], acTmp1[32];
   int      iTmp, iRet, iExeIdx;

   // Init input
   pRec = (LAX_ROLL *)pRollRec;

#ifdef _DEBUG
   //if (!memcmp(pRec->APN, "2004001009", 10))
   //   iTmp = 0;
#endif
   // Init output buffer
   if (iFlag & CREATE_R01)
   {
      // Clear output buffer
      memset(pOutbuf, ' ', iRecLen);

      // APN
      memcpy(pOutbuf+OFF_APN_S, pRec->APN, RSIZ_APN);
      memcpy(pOutbuf+OFF_CO_NUM, "19LAX", 5);

      // Format APN
      iRet = formatApn(pRec->APN, acTmp, &myCounty);
      memcpy(pOutbuf+OFF_APN_D, acTmp, iRet);

      // Create MapLink
      iRet = formatMapLink(pRec->APN, acTmp, &myCounty);
      memcpy(pOutbuf+OFF_MAPLINK, acTmp, iRet);

      // Create index map link
      if (getIndexPage(acTmp, acTmp1, &myCounty))
         memcpy(pOutbuf+OFF_IMAPLINK, acTmp1, iRet);

      // Year assessed
      memcpy(pOutbuf+OFF_YR_ASSD, myCounty.acYearAssd, 4);

      // HO Exempt
      iExeIdx = OFF_EXE_CD1;
      long lHOExe = atoin(pRec->HOExe, RSIZ_HOEXE);

      iTmp = atoin(pRec->NumOfHOE, RSIZ_NUMOFHOE);
      if (iTmp > 0)
      {
         *(pOutbuf+OFF_HO_FL) = '1';      // 'Y'
         memcpy(pOutbuf+OFF_EXE_CD1, "10", 2);
         iExeIdx = OFF_EXE_CD2;
      } else
         *(pOutbuf+OFF_HO_FL) = '2';      // 'N'

      // Exempt total
      long lRelExe = atoin(pRec->RelExe, RSIZ_RELEXE);
      long lPPExe  = atoin(pRec->PPExe, RSIZ_PPEXE);
      long lFixExe = atoin(pRec->FixtExe, RSIZ_FIXTEXE);
      lTmp = lHOExe+lRelExe+lPPExe+lFixExe;
      if (lTmp > 0)
      {
         sprintf(acTmp, "%*u", SIZ_EXE_TOTAL, lTmp);
         memcpy(pOutbuf+OFF_EXE_TOTAL, acTmp, SIZ_EXE_TOTAL);
      }

      // Exempt code (claim type)
      if (pRec->ExclType >= '0')
         *(pOutbuf+iExeIdx) = pRec->ExclType;

      // Create exemption type
      makeExeType(pOutbuf+OFF_EXE_TYPE, pOutbuf+OFF_EXE_CD1, pOutbuf+OFF_EXE_CD2, pOutbuf+OFF_EXE_CD3, (IDX_TBL4 *)&LAX_Exemption);

      // Land
      long lLand = atoin(pRec->LandVal, RSIZ_LANDVAL);
      if (lLand > 0)
         sprintf(acTmp, "%*u", SIZ_LAND, lLand);
      else
         strcpy(acTmp, BLANK32);
      memcpy(pOutbuf+OFF_LAND, acTmp, SIZ_LAND);

      // Improve
      long lImpr = atoin(pRec->ImprVal, RSIZ_IMPRVAL);
      if (lImpr > 0)
         sprintf(acTmp, "%*u", SIZ_IMPR, lImpr);
      else
         strcpy(acTmp, BLANK32);
      memcpy(pOutbuf+OFF_IMPR, acTmp, SIZ_IMPR);

      // Other value
      long lFixture = atoin(pRec->FixtVal, RSIZ_FIXTVAL);
      long lPers = atoin(pRec->PPVal, RSIZ_PPVAL);
      lTmp = lPers + lFixture;
      if (lTmp > 0)
      {
         sprintf(acTmp, "%*u", SIZ_OTHER, lTmp);
         memcpy(pOutbuf+OFF_OTHER, acTmp, SIZ_OTHER);

         if (lFixture > 0)
         {
            sprintf(acTmp, "%d         ", lFixture);
            memcpy(pOutbuf+OFF_FIXTR, acTmp, SIZ_FIXTR);
         }
         if (lPers > 0)
         {
            sprintf(acTmp, "%d         ", lPers);
            memcpy(pOutbuf+OFF_PERSPROP, acTmp, SIZ_PERSPROP);
         }
      }

      // Gross total
      lTmp += lLand+lImpr;
      if (lTmp > 0)
         sprintf(acTmp, "%*u", SIZ_GROSS, lTmp);
      else
         strcpy(acTmp, BLANK32);
      memcpy(pOutbuf+OFF_GROSS, acTmp, SIZ_GROSS);

      // Ratio
      if (lImpr > 0)
      {
         sprintf(acTmp, "%*u", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
         memcpy(pOutbuf+OFF_RATIO, acTmp, SIZ_RATIO);
      }
   }

   // TRA
   iTmp = sprintf(acTmp, "%.5d", atoin(pRec->TRA, RSIZ_TRA));
   memcpy(pOutbuf+OFF_TRA, acTmp, iTmp);
   /* No translation needed here - save for future use.
   iRet = atoin(pRec->TRA, RSIZ_TRA);
   if (iRet > 0)
   {
      sprintf(acTmp, "SELECT * FROM Lax_TraRef WHERE Xref=%d", iRet);
      try
      {
         rsTra.Open(dbTra, acTmp);

         CString sTra;
         if (rsTra.next())
         {
            sTra = rsTra.GetItem("TRA");
            memcpy(pOutbuf+OFF_TRA, sTra, sTra.GetLength());
            rsTra.Close();
         }
      } AdoCatch(e)
      {
         LogMsg("***** Error searching %s (%s)", acTmp, ComError(e));
      }
   } 
   */

   // Prop8
   if (pRec->LandKey == '8')
      *(pOutbuf+OFF_PROP8_FLG) = 'Y';

   iRet = 0;

   // Parcel Status

   // Set full exempt flag
   iTmp = atoin(pRollRec+ROFF_PARCEL, 3);
   if ((iTmp >= 300 && iTmp < 400) || (iTmp >= 800))
      *(pOutbuf+OFF_FULL_EXEMPT) = 'Y';
   else
      *(pOutbuf+OFF_FULL_EXEMPT) = ' ';

   // Legal Desc
   if (pRec->LglDesc[0] > ' ')
   {
      memcpy(acTmp, pRec->LglDesc, RSIZ_LGLDESC);
      myTrim(acTmp, RSIZ_LGLDESC);
      iTmp = replBadChar(acTmp, ' ');
      iTmp = updateLegal(pOutbuf, acTmp);
      if (iTmp > iMaxLegal)
         iMaxLegal = iTmp;
   } 

   // UseCode
   memcpy(acTmp, pRec->UseCode, RSIZ_USE);
   iTmp = strlen(myLTrim(acTmp, RSIZ_USE));
   if (iTmp > 0 && memcmp(acTmp, "0 ", 2))
   {
      memcpy(pOutbuf+OFF_USE_CO, acTmp, iTmp);

      strcpy(acTmp1, acTmp);
      myTrim(acTmp1);
      iTmp = strlen(acTmp1);
      if (acTmp1[iTmp-1] == 'V')
         memcpy(pOutbuf+OFF_USE_STD, USE_VACANT, 3);
      else
         updateStdUse(pOutbuf+OFF_USE_STD, acTmp1, iTmp, pOutbuf);
   } else
      memcpy(pOutbuf+OFF_USE_STD, USE_UNASGN, SIZ_USE_STD);

   // Recording info
   /* Use Lax_UpdateXferDoc() instead
   try {
      Lax_FmtRecDoc(pOutbuf, pRollRec);
   } catch (...)
   {
      iRet = 0;
      LogMsg("??? Error catched in Lax_FmtRecDoc() where APN=%.10s", pOutbuf);
   }
   */
   // Owners
   try {
      if (pRec->Name1[0] > ' ')
         Lax_MergeOwner(pOutbuf, pRec->Name1, pRec->Name2, pRec->SpcType);
      else if (pRec->Buyer1[0] > ' ')
      {
         LogMsg0("* Use Buyer %.12s", pOutbuf);
         Lax_MergeOwner(pOutbuf, pRec->Buyer1, pRec->Buyer2, pRec->SpcType);
      }
   } catch (...)
   {
      iRet = 0;
      LogMsg("??? Error catched in Lax_MergeOwner() where APN=%.10s", pOutbuf);
   }

   // Situs address
   Lax_MergeSAdr(pOutbuf, pRollRec);

   // Mailing address
   try {
      Lax_MergeMAdr(pOutbuf, pRollRec);
   } catch (...)
   {
      iRet = 0;
      LogMsg("??? Error catched in Lax_MergeMAdr() where APN=%.10s", pOutbuf);
   }

   // YrBlt
   iTmp = atoin(pRec->Maseg[0].YrBlt, RSIZ_YRBLT);
   if (iTmp > 1900)
      memcpy(pOutbuf+OFF_YR_BLT, pRec->Maseg[0].YrBlt, RSIZ_YRBLT);

   // Delinquent year
   iTmp = atoin(pRec->Sold2St, RSIZ_SOLDTOSTATE);
   if (iTmp > 1900)
      memcpy(pOutbuf+OFF_DEL_YR, pRec->Sold2St, SIZ_DEL_YR);

   // Acres
   // Lot Sqft
   // FP - 1
   // FL - # of floors  or stories
   // Garage

   // Bldg Sqft, Beds, Baths, Units, A/C, Heating, Pool, Spa, Bldg Class/Quality
   try {
      parseMaseg(pOutbuf, pRollRec);
   } catch (...)
   {
      iRet = 0;
      LogMsg("??? Error catched in parseMaseg() where APN=%.10s", pOutbuf);
   }

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "8235007001", 10))
   //   iTmp = 0;
#endif

   // Zoning
   if (pRec->Zone[0] > ' ')
   {
      // Remove known bad char
      remUChar((unsigned char *)&pRec->Zone[0], 0xFA, RSIZ_ZONE);
      iTmp = remChar(pRec->Zone, '|', RSIZ_ZONE);
      //if (*(pOutbuf+OFF_ZONE_X1) == ' ')
      //   vmemcpy(pOutbuf+OFF_ZONE_X1, pRec->Zone, SIZ_ZONE_X1, iTmp);

      memcpy(pOutbuf+OFF_ZONE, pRec->Zone, SIZ_ZONE);
   }

   // Remove old seller
   memset(pOutbuf+OFF_SELLER, ' ', SIZ_SELLER);

   // Multi APN/Partial transfer
   if (pRec->DocKey == 'M')
      *(pOutbuf+OFF_MULTI_APN) = 'Y';
   else 
      *(pOutbuf+OFF_MULTI_APN) = ' ';

   return iRet;
}

/***************************** LAX_MergeSaleRec ******************************
 *
 * Merge new sale data into current sale.  Move other sales accordingly.
 *
 *****************************************************************************/

int Lax_MergeSaleRec(char *pOutbuf, char *pSale)
{
   long     lCurSaleDt, lLstSaleDt, lPrice, lLastAmt, lDocNum;

   SCSAL_REC *pSaleRec = (SCSAL_REC *)pSale;

   lCurSaleDt = atoin(pSaleRec->DocDate, SIZ_SALE1_DT);
   lLstSaleDt = atoin(pOutbuf+OFF_SALE1_DT, SIZ_SALE1_DT);

   if (lCurSaleDt < lLstSaleDt)
      return -1;

   //if (lCurSaleDt > 20080731)
   //   return -1;

   // Format DocNo & sale price
   lDocNum = atoin(pSaleRec->DocNum, SSIZ_RECDOC_NO);
   lPrice = atoin(pSaleRec->SalePrice, SSIZ_DOCTAX_AMT);

   if (lCurSaleDt == lLstSaleDt)
   {
      lLastAmt = atoin(pOutbuf+OFF_SALE1_AMT, SIZ_SALE1_AMT);
      if (lPrice == lLastAmt || !lPrice)
         return 0;
   } else
   {
      // Move sale2 to sale3
      memcpy(pOutbuf+OFF_SALE3_DOC, pOutbuf+OFF_SALE2_DOC, SIZ_SALE3_DOC);
      memcpy(pOutbuf+OFF_SALE3_DT, pOutbuf+OFF_SALE2_DT, SIZ_SALE3_DT);
      memcpy(pOutbuf+OFF_SALE3_DOCTYPE, pOutbuf+OFF_SALE2_DOCTYPE, SIZ_SALE3_DOCTYPE);
      memcpy(pOutbuf+OFF_SALE3_AMT, pOutbuf+OFF_SALE2_AMT, SIZ_SALE3_AMT);
      memcpy(pOutbuf+OFF_SALE3_CODE, pOutbuf+OFF_SALE2_CODE, SIZ_SALE3_CODE);
      *(pOutbuf+OFF_AR_CODE3) = *(pOutbuf+OFF_AR_CODE2);

      // Move sale1 to sale2
      memcpy(pOutbuf+OFF_SALE2_DOC, pOutbuf+OFF_SALE1_DOC, SIZ_SALE3_DOC);
      memcpy(pOutbuf+OFF_SALE2_DT, pOutbuf+OFF_SALE1_DT, SIZ_SALE3_DT);
      memcpy(pOutbuf+OFF_SALE2_DOCTYPE, pOutbuf+OFF_SALE1_DOCTYPE, SIZ_SALE3_DOCTYPE);
      memcpy(pOutbuf+OFF_SALE2_AMT, pOutbuf+OFF_SALE1_AMT, SIZ_SALE3_AMT);
      memcpy(pOutbuf+OFF_SALE2_CODE, pOutbuf+OFF_SALE1_CODE, SIZ_SALE3_CODE);
      *(pOutbuf+OFF_AR_CODE2) = *(pOutbuf+OFF_AR_CODE1);
   }

   // Update current sale
   memcpy(pOutbuf+OFF_SALE1_DOC, pSaleRec->DocNum, SSIZ_RECDOC_NO);
   memcpy(pOutbuf+OFF_SALE1_DT, pSaleRec->DocDate, SIZ_SALE1_DT);
   memcpy(pOutbuf+OFF_SALE1_AMT, pSaleRec->SalePrice, SIZ_SALE1_AMT);

   // Sale code
   *(pOutbuf+OFF_SALE1_CODE) = pSaleRec->SaleCode[0];

   // Multi sale/partial interest
   *(pOutbuf+OFF_MULTI_APN) = pSaleRec->MultiSale_Flg;

   // Update transfers
   /*  07/01/2010
   lLstSaleDt = atoin(pOutbuf+OFF_TRANSFER_DT, SIZ_TRANSFER_DT);
   if (lCurSaleDt > lLstSaleDt)
   {
      memcpy(pOutbuf+OFF_TRANSFER_DOC, pSaleRec->DocNum, SSIZ_RECDOC_NO);
      memcpy(pOutbuf+OFF_TRANSFER_DT, pSaleRec->DocDate, SIZ_TRANSFER_DT);
   }
   */

   if (lCurSaleDt > lLastRecDate)
      lLastRecDate = lCurSaleDt;

   *(pOutbuf+OFF_AR_CODE1) = 'A';
   return 1;
}

/********************************* Lax_MergeSale ******************************
 *
 * Input: Sale file from the county.
 *
 ******************************************************************************/

int Lax_MergeSale(char *pOutbuf)
{
   static   char  acRec[1024], *pRec=NULL;
   int      iRet, iLoop;
   SCSAL_REC *pSale;

   // Get first Char rec for first call
   if (!pRec)
   {
      pRec = fgets((char *)&acRec[0], 1024, fdSale);
   }

   pSale = (SCSAL_REC *)&acRec;
   do
   {
      // Compare Apn
      iLoop = memcmp(pOutbuf, pSale->Apn, SSIZ_APN);
      if (iLoop > 0)
      {
         if (bDebug)
            LogMsg0("Skip Sale rec  %.*s", SSIZ_APN, pSale->Apn);

         pRec = fgets((char *)&acRec[0], 1024, fdSale);
         if (!pRec)
         {
            fclose(fdSale);
            fdSale = NULL;
            break;
         }
         lSaleSkip++;
      }
   } while (iLoop > 0);

   // If not match, return
   if (iLoop)
      return 1;

   do
   {
#ifdef _DEBUG
//      if (!memcmp(pOutbuf, "10145008", 8))
//         iRet = 0;
#endif

      iRet = Lax_MergeSaleRec(pOutbuf, acRec);

      // Get next sale record
      pRec = fgets((char *)&acRec[0], 1024, fdSale);
      if (!pRec)
      {
         fclose(fdSale);
         fdSale = NULL;
         break;
      }
   } while (!memcmp(pOutbuf, pSale->Apn, RSIZ_APN));

   lSaleMatch++;

   return 0;
}

/********************************** Lax_Load_Roll *****************************
 *
 * 07/12/2016 Change fgets() to fread() since roll file contains EOF character (1A)
 * within record which signals EOF in fgets().
 *
 ******************************************************************************/

int Lax_Load_Roll(int iSkip)
{
   char     acRec[MAX_RECSIZE], acBuf[MAX_RECSIZE], acRollRec[MAX_RECSIZE], acRawFile[_MAX_PATH], acOutFile[_MAX_PATH];
   char     cFileCnt=1;

   HANDLE   fhIn, fhOut;

   int      iRet, iTmp, iRollUpd=0, iNewRec=0, iRetiredRec=0;
   DWORD    nBytesRead;
   DWORD    nBytesWritten;
   BOOL     bRet, bEof;
   long     lRet=0, lCnt=0;

   //GetPrivateProfileString(myCounty.acCntyCode, "SaleCum", "", acSaleRec, _MAX_PATH, acIniFile);
   //iSaleLen = GetPrivateProfileInt(myCounty.acCntyCode, "CumSaleLen", 240, acIniFile);

   sprintf(acRawFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "S01");
   sprintf(acOutFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "R01");

   // Rename files for processing
   if (_access(acRawFile, 0))
   {
      if (!_access(acOutFile, 0))
         rename(acOutFile, acRawFile);
      else
      {
         LogMsg("***** Missing input file %s.  Please recheck!", acRawFile);
         return -1;
      }
   }

   // Open updated roll file
   LogMsg("Open Roll file %s", acRollFile);
   fdRoll = fopen(acRollFile, "rb");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", acRollFile);
      return -2;
   }

   // Open sale file
   LogMsg("Open Cum Sale file %s", acCSalFile);
   fdSale = fopen(acCSalFile, "r");
   if (fdSale == NULL)
   {
      LogMsg("***** Error opening cum sale file: %s\n", acCSalFile);
      return -3;
   }

   // Open Input file
   LogMsg("Open input file %s", acRawFile);
   fhIn = CreateFile(acRawFile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhIn == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error open input file: %s", acRawFile);
      return -4;
   }

   // Open Output file - use tmp file so we can sort at the end to R01 file
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error open output file: %s", acOutFile);
      return -4;
   }

   // Get first RollRec
   iTmp = fread(acRollRec, 1, iRollLen, fdRoll);
   bEof = (iTmp < iRollLen ? true:false);

   // Copy skip record
   memset(acBuf, ' ', iRecLen);
   while (iSkip-- > 0)
   {
      ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);
      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
   }

   iNoMatch=iBadCity=iBadSuffix=0;

   // Merge loop
   while (!bEof)
   {
      bRet = ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);

      // Check for EOF
      if (!bRet)
      {
         LogMsg("***** Error reading input file %s (%f)", acRawFile, GetLastError());
         break;
      }

      // EOF ?
      if (!nBytesRead)
      {
         // EOF
         cFileCnt++;
         acRawFile[strlen(acRawFile)-1] = cFileCnt | 0x30;
         acOutFile[strlen(acOutFile)-1] = cFileCnt | 0x30;
         if (_access(acRawFile, 0))
         {
            if (!_access(acOutFile, 0))
               rename(acOutFile, acRawFile);
            else
               break;
         }

         CloseHandle(fhIn);
         CloseHandle(fhOut);
         fhOut = 0;
         fhIn = 0;

         // Open next Input file
         LogMsg("Open input file %s", acRawFile);
         fhIn = CreateFile(acRawFile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
               FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

         if (fhIn == INVALID_HANDLE_VALUE)
            break;
         bRet = ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);

         // Open Output file
         LogMsg("Open output file %s", acOutFile);
         fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
               FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);
         if (fhOut == INVALID_HANDLE_VALUE)
            break;
      }

      NextRollRec:
#ifdef _DEBUG      
      //if ((iRet = findUnPrtChar(acRollRec, 390)) && iRet != 392 && (unsigned char)acRollRec[iRet] != 0x9B)
      //   iRet = 0;
#endif

      iTmp = memcmp(acBuf, acRollRec, iApnLen);
      if (!iTmp)
      {
         // Clear old sales
         if (bClearSales)
            ClearOldSale(acBuf, true);

         // Update roll data
         iRet = Lax_MergeRoll(acBuf, acRollRec, UPDATE_R01);
         iRollUpd++;

#ifdef _DEBUG
         //if (!memcmp(acBuf, "8765002017", 10))
         //   iRet = 0;
#endif

         // Clear old sales
         if (fdSale)
            iRet = Lax_MergeSale(acBuf);

         // Update transfer if roll file has newer sale
         Lax_UpdateXferDoc(acBuf, acRollRec);

         // Read next roll record
         iTmp = fread(acRollRec, 1, iRollLen, fdRoll);
         if (iTmp < iRollLen || !memcmp(acRollRec, "9999999", 7))
            bEof = true;         // Signal to stop - EOF
      } else if (iTmp > 0)       // Roll not match, new roll record?
      {
         if (bDebug)
            LogMsg0("New roll record : %.*s (%d)", iApnLen, acRollRec, lCnt);

         // Create new R01 record
         iRet = Lax_MergeRoll(acRec, acRollRec, CREATE_R01);
         iNewRec++;

         if (fdSale)
            iRet = Lax_MergeSale(acRec);

         iRet = setRegion(acRec);
         bRet = WriteFile(fhOut, acRec, iRecLen, &nBytesWritten, NULL);
         lCnt++;

         // Get next roll record
         iTmp = fread(acRollRec, 1, iRollLen, fdRoll);
         if (iTmp < iRollLen || !memcmp(acRollRec, "9999999", 7))
            bEof = true;    // Signal to stop
         else
            goto NextRollRec;
      } else
      {
         // Record may be retired
         if (bDebug)
            LogMsg0("Roll not match (retired record?) : R01->%.*s < Roll->%.*s (%d)", iApnLen, acBuf, iApnLen, acRollRec, lCnt);
         iRetiredRec++;
         continue;
      }

      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);

      if (!bRet)
      {
         LogMsg("***** Error occurs: %d\n", GetLastError());
         lRet = WRITE_ERR;
         break;
      }
   }

   // Do the rest of the file
   while (!bEof)
   {
      if (bDebug)
         LogMsg0("New roll record : %.*s (%d)", iApnLen, acRollRec, lCnt);

      // Create new R01 record
      iRet = Lax_MergeRoll(acRec, acRollRec, CREATE_R01);
      iNewRec++;

      if (fdSale)
         iRet = Lax_MergeSale(acRec);

      iRet = setRegion(acRec);
      bRet = WriteFile(fhOut, acRec, iRecLen, &nBytesWritten, NULL);
      lCnt++;

      // Get next roll record
      iTmp = fread(acRollRec, 1, iRollLen, fdRoll);
      if (iTmp < iRollLen || !memcmp(acRollRec, "9999999", 7))
         bEof = true;    // Signal to stop
   }

   // Close files
   if (fdRoll)
      fclose(fdRoll);
   if (fdSale)
      fclose(fdSale);
   if (fhOut)
      CloseHandle(fhOut);
   if (fhIn)
      CloseHandle(fhIn);

   LogMsg("Total output records:       %u", lCnt);
   LogMsg("Total new records:          %u", iNewRec);
   LogMsg("Total retired records:      %u", iRetiredRec);
   LogMsg("Total bad-city records:     %u", iBadCity);
   LogMsg("Total bad-suffix records:   %u", iBadSuffix);
   LogMsg("Total xfer updated  :       %u", lUpdXfer);
   LogMsg("Last recording date:        %u", lLastRecDate);
   LogMsg("Total records /w DBA:       %u", iDbaCnt);
   if (lDupOwners > 0)
      LogMsg("Total duplicate owners:     %u", lDupOwners);

   LogMsg("#Bldgs:                     %u\n", lAssrRecCnt);
   printf("\nTotal output records: %u\n", lCnt);

   lRecCnt = lCnt;

   return 0;
}

/********************************* Lax_Load_LDR *****************************
 *
 * Use -L
 *
 ****************************************************************************/

int Lax_Load_LDR(int iFirstRec /* 1=create header rec */)
{
   char     acBuf[MAX_RECSIZE], acRollRec[MAX_RECSIZE];
   char     acOutFile[_MAX_PATH];

   HANDLE   fhOut;
   int      iRet, iTmp;
   DWORD    nBytesWritten;
   BOOL     bRet, bEof;
   long     lRet=0, lCnt=0;

   sprintf(acOutFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "R01");

   // Open roll file
   LogMsg("Open Roll file %s", acRollFile);
   fdRoll = fopen(acRollFile, "rb");
   if (fdRoll == NULL)
   {
      printf("***** Error opening roll file: %s\n", acRollFile);
      return 2;
   }

   // Open sale file
   LogMsg("Open Cum Sale file %s", acCSalFile);
   fdSale = fopen(acCSalFile, "r");
   if (fdSale == NULL)
   {
      LogMsg("***** Error opening cum sale file: %s\n", acCSalFile);
      return -3;
   }

   // Open Output file
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
      return 4;

   // Output first header record
   if (iFirstRec > 0)
   {
      memset(acBuf, '9', iRecLen);
      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
   }

   // Get first RollRec
   //pTmp = fgets((char *)&acRollRec[0], iRollLen, fdRoll);
   //bEof = (pTmp ? false:true);
   iTmp = fread(acRollRec, 1, iRollLen, fdRoll);
   bEof = (iTmp < iRollLen ? true:false);

   // Init variables
   iNoMatch=iBadCity=iBadSuffix=0;

   // Merge loop
   while (!bEof)
   {
      // Skip test record
      if (!memcmp(acRollRec, "9999999", 7))
      {
         //pTmp = fgets(acRollRec, iRollLen, fdRoll);
         iTmp = fread(acRollRec, 1, iRollLen, fdRoll);
         bEof = (iTmp < iRollLen ? true:false);
         continue;
      }

      lLDRRecCount++;

      // Create new R01 record
      iRet = Lax_MergeRoll(acBuf, acRollRec, CREATE_R01);

      if (fdSale)
         iRet = Lax_MergeSale(acBuf);

      // Update transfer if roll file has newer sale
      Lax_UpdateXferDoc(acBuf, acRollRec);

#ifdef _DEBUG
      //iRet = replChar(acBuf, 0, ' ', iRecLen);
      //if (iRet)
      //{
      //   LogMsgD("***** Null character found at %d on APN: %.12s", iRet, acBuf);
      //   iRet = 0;
      //}
#endif

      // Get last recording date
      iRet = atoin((char *)&acBuf[OFF_SALE1_DT], SIZ_SALE1_DT);
      if (iRet >= lToday)
         LogMsg0("*** WARNING: Bad last recording date %d at record# %d -->%.10s", iRet, lCnt, acBuf);
      else if (lLastRecDate < iRet)
         lLastRecDate = iRet;

      iRet = setRegion(acBuf);
      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
      if (!(++lCnt % 1000))
      {
         printf("\r%u", lCnt);
         // Break output into 800000 record chunks

         if (!(lCnt % 900000))
         {
            if (fhOut) CloseHandle(fhOut);

            acOutFile[strlen(acOutFile)-1]++;
            LogMsg("Open output file %s", acOutFile);
            fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
                    FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

            if (fhOut == INVALID_HANDLE_VALUE)
            {
               fhOut = 0;
               break;
            }
         }
      }

      if (!bRet)
      {
         LogMsg("***** Error writing to output file at record %d\n", lCnt);
         lRet = WRITE_ERR;
         break;
      }

      // Get next roll record
      //pTmp = fgets(acRollRec, iRollLen, fdRoll);
      iTmp = fread(acRollRec, 1, iRollLen, fdRoll);
      if (iTmp < iRollLen || !memcmp(acRollRec, "9999999", 7))
         bEof = true;         // Signal to stop - EOF
   }

   // Close files
   if (fhOut)
      CloseHandle(fhOut);

   if (fdRoll)
      fclose(fdRoll);
   if (fdSale)
      fclose(fdSale);

   LogMsg("Total input records:        %u", lLDRRecCount);
   LogMsg("Total output records:       %u", lCnt);
   LogMsg("Total bad-city records:     %u", iBadCity);
   LogMsg("Total bad-suffix records:   %u", iBadSuffix);
   LogMsg("Total xfer updated:         %u", lUpdXfer);
   LogMsg("Last recording date:        %u\n", lLastRecDate);

   lRecCnt = lCnt;
   return lRet;
}

/******************************* Lax_CreateAssrRec ***************************
 *
 * Return 0 if successful, >0 if error
 *
 *****************************************************************************/

//int Lax_CreateAssrRec(char *pOutbuf, char *pRollRec, int iFlag)
//{
//   LAX_ROLL *pRec;
//   char     acTmp[256], *pTmp;
//   long     lTmp, lGross, lLand, lImpr, lNet, lSaleAmt, lSqft;
//   long     lFixture, lFixEx, lPers, lPerEx, lHomeEx, lRelEx, lTotalEx, lLI;
//   int      iTmp;
//
//   // Clean up bad data
//   replUnPrtChar(pRollRec, ' ', 0);
//
//   // Init input
//   pRec = (LAX_ROLL *)pRollRec;
//
//   lLand    = atoin(pRec->LandVal, RSIZ_LANDVAL);
//   lImpr    = atoin(pRec->ImprVal, RSIZ_IMPRVAL);
//   lFixture = atoin(pRec->FixtVal, RSIZ_FIXTVAL);
//   lFixEx   = atoin(pRec->FixtExe, RSIZ_FIXTEXE);
//   lPers    = atoin(pRec->PPVal, RSIZ_PPVAL);
//   lPerEx   = atoin(pRec->PPExe, RSIZ_PPEXE);
//   lHomeEx  = atoin(pRec->HOExe, RSIZ_HOEXE);
//   lRelEx   = atoin(pRec->RelExe, RSIZ_RELEXE);
//   lSaleAmt = atoin(pRollRec+ROFF_SALE1AMT, RSIZ_SALE1AMT);
//   lSqft    = atoin(pRollRec+ROFF_BLDGSQFT1, RSIZ_BLDGSQFT);
//   lSqft   += atoin(pRollRec+ROFF_BLDGSQFT2, RSIZ_BLDGSQFT);
//   lSqft   += atoin(pRollRec+ROFF_BLDGSQFT3, RSIZ_BLDGSQFT);
//   lSqft   += atoin(pRollRec+ROFF_BLDGSQFT4, RSIZ_BLDGSQFT);
//   lSqft   += atoin(pRollRec+ROFF_BLDGSQFT5, RSIZ_BLDGSQFT);
//
//   lTotalEx = lHomeEx+lFixEx+lPerEx+lRelEx;
//   lLI      = lLand+lImpr;
//   lGross   = lLI + lPers + lFixture;
//
//#ifdef _DEBUG
//   //if (!memcmp(pRec->APN, "10534116", 8))
//   //   iTmp = 0;
//#endif
//
//   // Init output buffer
//   if (iFlag & CREATE_R01)
//   {
//      // Clear output buffer
//      memset(pOutbuf, ' ', iRecLen);
//
//      // Land & Impr
//      if (lLand > 0)
//      {
//         sprintf(acTmp, "%*u", ASIZ_L_LAND, lLand);
//         memcpy(pOutbuf+AOFF_L_LAND, acTmp, ASIZ_L_LAND);
//      }
//      if (lImpr > 0)
//      {
//         sprintf(acTmp, "%*u", ASIZ_L_IMPR, lImpr);
//         memcpy(pOutbuf+AOFF_L_IMPR, acTmp, ASIZ_L_IMPR);
//      }
//
//      // Exe Type ???
//      *(pOutbuf+AOFF_L_EXTYPE) = '.';
//
//      *(pOutbuf+AOFF_L_PPKEY) = pRec->PPKey;
//
//      if (lPers > 0)
//      {
//         sprintf(acTmp, "%*u", ASIZ_L_PPVAL, lPers);
//         memcpy(pOutbuf+AOFF_L_PPVAL, acTmp, ASIZ_L_PPVAL);
//      }
//      if (lPerEx > 0)
//      {
//         sprintf(acTmp, "%*u", ASIZ_L_PPEX, lPerEx);
//         memcpy(pOutbuf+AOFF_L_PPEX, acTmp, ASIZ_L_PPEX);
//      }
//      if (lFixture > 0)
//      {
//         sprintf(acTmp, "%*u", ASIZ_L_FIXT, lFixture);
//         memcpy(pOutbuf+AOFF_L_FIXT, acTmp, ASIZ_L_FIXT);
//      }
//      if (lFixEx > 0)
//      {
//         sprintf(acTmp, "%*u", ASIZ_L_FIXTEX, lFixEx);
//         memcpy(pOutbuf+AOFF_L_FIXTEX, acTmp, ASIZ_L_FIXTEX);
//      }
//
//      *(pOutbuf+AOFF_L_HOECNT) = pRec->NumOfHOE[0];
//
//      if (lHomeEx > 0)
//      {
//         sprintf(acTmp, "%*u", ASIZ_L_HOEXE, lHomeEx);
//         memcpy(pOutbuf+AOFF_L_HOEXE, acTmp, ASIZ_L_HOEXE);
//      } else
//         *(pOutbuf+AOFF_L_HOECNT) = '0';
//
//      if (lRelEx > 0)
//      {
//         sprintf(acTmp, "%*u", ASIZ_L_RELEXE, lRelEx);
//         memcpy(pOutbuf+AOFF_L_RELEXE, acTmp, ASIZ_L_RELEXE);
//      }
//
//      if (lLI > 0)
//      {
//         sprintf(acTmp, "%*u", ASIZ_L_LI, lLI);
//         memcpy(pOutbuf+AOFF_L_LI, acTmp, ASIZ_L_LI);
//      }
//
//      if (lGross > 0)
//      {
//         sprintf(acTmp, "%*u", ASIZ_L_LIPF, lGross);
//         memcpy(pOutbuf+AOFF_L_LIPF, acTmp, ASIZ_L_LIPF);
//      }
//
//      lTmp = lLI - lHomeEx;
//      if (lTmp > 0)
//      {
//         sprintf(acTmp, "%*u", ASIZ_L_LIH, lTmp);
//         memcpy(pOutbuf+AOFF_L_LIH, acTmp, ASIZ_L_LIH);
//      }
//
//      lTmp = lGross - (lHomeEx+lFixEx+lPerEx);
//      if (lTmp > 0)
//      {
//         sprintf(acTmp, "%*u", ASIZ_L_LIPF_HFP, lTmp);
//         memcpy(pOutbuf+AOFF_L_LIPF_HFP, acTmp, ASIZ_L_LIPF_HFP);
//      }
//
//      lNet = lGross - lTotalEx;
//      if (lNet > 0)
//      {
//         sprintf(acTmp, "%*u", ASIZ_L_NET, lNet);
//         memcpy(pOutbuf+AOFF_L_NET, acTmp, ASIZ_L_NET);
//      }
//
//      lTmp = lLI - (lHomeEx+lRelEx);
//      if (lTmp > 0)
//      {
//         sprintf(acTmp, "%*u", ASIZ_L_LI_HR, lTmp);
//         memcpy(pOutbuf+AOFF_L_LI_HR, acTmp, ASIZ_L_LI_HR);
//      }
//
//      if (lTotalEx > 0)
//      {
//         sprintf(acTmp, "%*u", ASIZ_L_TOTEX, lTotalEx);
//         memcpy(pOutbuf+AOFF_L_TOTEX, acTmp, ASIZ_L_TOTEX);
//      }
//
//      // Ratio
//      if (lImpr > 0)
//      {
//         sprintf(acTmp, "%*u", SIZ_RATIO, lImpr*100/lLI);
//         memcpy(pOutbuf+AOFF_L_RATIO, acTmp, SIZ_RATIO);
//      }
//
//      // Impr Sqft
//      if (lSqft > 0)
//      {
//         sprintf(acTmp, "%*.2f", ASIZ_L_IV_SQFT, (double)lImpr/lSqft);
//         memcpy(pOutbuf+AOFF_L_IV_SQFT, acTmp, ASIZ_L_IV_SQFT);
//
//         if (lSaleAmt > 9)
//         {
//            sprintf(acTmp, "%*.2f", ASIZ_L_SV_SQFT, (double)lSaleAmt/lSqft);
//            memcpy(pOutbuf+AOFF_L_SV_SQFT, acTmp, ASIZ_L_SV_SQFT);
//         }
//      }
//
//      // Profit
//      if (lSaleAmt > lNet)
//      {
//         sprintf(acTmp, "%*u", ASIZ_L_PROFIT, lSaleAmt - lNet);
//         memcpy(pOutbuf+AOFF_L_PROFIT, acTmp, ASIZ_L_PROFIT);
//      }
//
//      // Sale > lien gross
//      if (lSaleAmt > lGross)
//         *(pOutbuf+AOFF_L_SBG) = 'E';
//   }
//
//   // Roll
//   pRec->ExclType = '.';
//   memcpy(pOutbuf, pRollRec, ASIZ_ROLL1);
//
//   // 20071211 - spn
//   if (isalnum(*(pRollRec+ROFF_NAME1)))
//      memcpy(pOutbuf+AOFF_ROLL2, pRollRec+ROFF_NAME1, ASIZ_ROLL2);
//   else
//   {
//      memcpy(pOutbuf+AOFF_NAME1, pRollRec+ROFF_NAME1+1, ASIZ_NAME1-1);
//      memcpy(pOutbuf+AOFF_NAME1_2, pRollRec+ROFF_NAME1_2, ASIZ_ROLL2_1);
//      if (*(pRollRec+ROFF_NAME1) != ' ')
//         LogMsg("Invalid character found (%c) in name1 at %.10s", *(pRollRec+ROFF_NAME1), pRollRec);
//   }
//   memcpy(pOutbuf+AOFF_ROLL3, pRollRec+ROFF_NUMOFHOE, ASIZ_ROLL3);
//   memcpy(pOutbuf+AOFF_ROLL4, pRollRec+ROFF_LANDYEAR2, ASIZ_ROLL4);
//
//   // Remove leading 0 for all numeric fields
//   lTmp = atoin(pOutbuf+ROFF_AGENCY, RSIZ_AGENCY);
//   if (!lTmp)
//      memset(pOutbuf+ROFF_AGENCY, ' ', RSIZ_AGENCY);
//
//   if (!lLand)
//      memset(pOutbuf+ROFF_LANDVAL, ' ', RSIZ_LANDVAL);
//   else
//   {
//      sprintf(acTmp, "%*u", RSIZ_LANDVAL, lLand);
//      memcpy(pOutbuf+ROFF_LANDVAL, acTmp, RSIZ_LANDVAL);
//   }
//   if (!lImpr)
//      memset(pOutbuf+ROFF_IMPRVAL, ' ', RSIZ_IMPRVAL);
//   else
//   {
//      sprintf(acTmp, "%*u", RSIZ_IMPRVAL, lImpr);
//      memcpy(pOutbuf+ROFF_IMPRVAL, acTmp, RSIZ_IMPRVAL);
//   }
//
//   lTmp = atoin(pOutbuf+ROFF_SSTRNUM, RSIZ_SSTRNUM);
//   if (!lTmp)
//      memset(pOutbuf+ROFF_SSTRNUM, ' ', RSIZ_SSTRNUM);
//   else
//   {
//      sprintf(acTmp, "%d     ", lTmp);
//      memcpy(pOutbuf+ROFF_SSTRNUM, acTmp, RSIZ_SSTRNUM);
//   }
//
//   lTmp = atoin(pOutbuf+ROFF_SZIP, 5);
//   if (lTmp >= 90000)
//   {
//      // Ignore zip4
//      sprintf(acTmp, "%d     ", lTmp);
//      memcpy(pOutbuf+ROFF_SZIP, acTmp, RSIZ_SZIP);
//   } else
//      memset(pOutbuf+ROFF_SZIP, ' ', RSIZ_SZIP);
//
//   lTmp = atoin(pOutbuf+ROFF_MSTRNUM, RSIZ_MSTRNUM);
//   if (!lTmp)
//      memset(pOutbuf+ROFF_MSTRNUM, ' ', RSIZ_MSTRNUM);
//   else
//   {
//      sprintf(acTmp, "%d     ", lTmp);
//      memcpy(pOutbuf+ROFF_MSTRNUM, acTmp, RSIZ_MSTRNUM);
//   }
//
//   // Delinquent year
//   lTmp = atoin(pRollRec+ROFF_SOLDTOSTATE, RSIZ_SOLDTOSTATE);
//   if (lTmp >= 1900)
//   {
//      sprintf(acTmp, "%d", lTmp);
//      memcpy(pOutbuf+AOFF_SOLDTOSTATE, acTmp, RSIZ_SOLDTOSTATE);
//   } else
//      memset(pOutbuf+AOFF_SOLDTOSTATE, ' ', RSIZ_SOLDTOSTATE);
//
//   lTmp = atoin(pRollRec+ROFF_WEED_ABAMNT, RSIZ_WEED_ABAMNT);
//   if (!lTmp)
//      memset(pOutbuf+AOFF_WEED_ABAMNT, ' ', RSIZ_WEED_ABAMNT);
//   else
//   {
//      sprintf(acTmp, "%*u", RSIZ_WEED_ABAMNT, lTmp);
//      memcpy(pOutbuf+AOFF_WEED_ABAMNT, acTmp, RSIZ_WEED_ABAMNT);
//   }
//
//   // PP Val
//   lTmp = atoin(pRec->PPVal, RSIZ_PPVAL);
//   if (!lTmp)
//      memset(pOutbuf+AOFF_PPVAL, ' ', RSIZ_PPVAL);
//   else
//   {
//      sprintf(acTmp, "%*u", RSIZ_PPVAL, lTmp);
//      memcpy(pOutbuf+AOFF_PPVAL, acTmp, RSIZ_PPVAL);
//   }
//
//   // PP Exe
//   lTmp = atoin(pRec->PPExe, RSIZ_PPVAL);
//   if (!lTmp)
//      memset(pOutbuf+AOFF_PPEXE, ' ', RSIZ_PPVAL);
//   else
//   {
//      sprintf(acTmp, "%*u", RSIZ_PPVAL, lTmp);
//      memcpy(pOutbuf+AOFF_PPEXE, acTmp, RSIZ_PPVAL);
//   }
//
//   // Fixt Val
//   lTmp = atoin(pRec->FixtVal, RSIZ_PPVAL);
//   if (!lTmp)
//      memset(pOutbuf+AOFF_FIXTVAL, ' ', RSIZ_PPVAL);
//   else
//   {
//      sprintf(acTmp, "%*u", RSIZ_PPVAL, lTmp);
//      memcpy(pOutbuf+AOFF_FIXTVAL, acTmp, RSIZ_PPVAL);
//   }
//
//   // Fixt Exe
//   lTmp = atoin(pRec->FixtExe, RSIZ_PPVAL);
//   if (!lTmp)
//      memset(pOutbuf+AOFF_FIXTEXE, ' ', RSIZ_PPVAL);
//   else
//   {
//      sprintf(acTmp, "%*u", RSIZ_PPVAL, lTmp);
//      memcpy(pOutbuf+AOFF_FIXTEXE, acTmp, RSIZ_PPVAL);
//   }
//
//   if (pRec->NumOfHOE[0] == '0')
//      *(pOutbuf+AOFF_NUMOFHOE) = ' ';
//
//   // HO Exe
//   lTmp = atoin(pRec->HOExe, RSIZ_PPVAL);
//   if (!lTmp)
//      memset(pOutbuf+AOFF_HOEXE, ' ', RSIZ_PPVAL);
//   else
//   {
//      sprintf(acTmp, "%*u", RSIZ_PPVAL, lTmp);
//      memcpy(pOutbuf+AOFF_HOEXE, acTmp, RSIZ_PPVAL);
//   }
//
//   // Rel Exe
//   lTmp = atoin(pRec->RelExe, RSIZ_PPVAL);
//   if (!lTmp)
//      memset(pOutbuf+AOFF_RELEXE, ' ', RSIZ_PPVAL);
//   else
//   {
//      sprintf(acTmp, "%*u", RSIZ_PPVAL, lTmp);
//      memcpy(pOutbuf+AOFF_RELEXE, acTmp, RSIZ_PPVAL);
//   }
//
//   // Sale1
//   lTmp = atoin(pRec->Sale1Amt, RSIZ_PPVAL);
//   if (lTmp > 9)
//   {
//      sprintf(acTmp, "%*u", RSIZ_PPVAL, lTmp);
//      memcpy(pOutbuf+AOFF_SALE1AMT, acTmp, RSIZ_PPVAL);
//   } else
//      memset(pOutbuf+AOFF_SALE1AMT, ' ', RSIZ_PPVAL);
//   lTmp = atoin(pRec->Sale1Dt, RSIZ_SALE1DATE);
//   if (!lTmp)
//      memset(pOutbuf+AOFF_SALE1DATE, ' ', RSIZ_SALE1DATE);
//
//   // Sale2
//   lTmp = atoin(pRec->Sale2Amt, RSIZ_PPVAL);
//   if (lTmp > 9)
//   {
//      sprintf(acTmp, "%*u", RSIZ_PPVAL, lTmp);
//      memcpy(pOutbuf+AOFF_SALE2AMT, acTmp, RSIZ_PPVAL);
//   } else
//      memset(pOutbuf+AOFF_SALE2AMT, ' ', RSIZ_PPVAL);
//   lTmp = atoin(pRec->Sale2Dt, RSIZ_SALE1DATE);
//   if (!lTmp)
//      memset(pOutbuf+AOFF_SALE2DATE, ' ', RSIZ_SALE1DATE);
//
//   // Sale3
//   lTmp = atoin(pRec->Sale3Amt, RSIZ_PPVAL);
//   if (lTmp > 9)
//   {
//      sprintf(acTmp, "%*u", RSIZ_PPVAL, lTmp);
//      memcpy(pOutbuf+AOFF_SALE3AMT, acTmp, RSIZ_PPVAL);
//   } else
//      memset(pOutbuf+AOFF_SALE3AMT, ' ', RSIZ_PPVAL);
//   lTmp = atoin(pRec->Sale3Dt, RSIZ_SALE1DATE);
//   if (!lTmp)
//      memset(pOutbuf+AOFF_SALE3DATE, ' ', RSIZ_SALE1DATE);
//
//   // Format MASEG
//   formatMaseg(pOutbuf+AOFF_MASEG1);
//   formatMaseg(pOutbuf+AOFF_MASEG2);
//   formatMaseg(pOutbuf+AOFF_MASEG3);
//   formatMaseg(pOutbuf+AOFF_MASEG4);
//   formatMaseg(pOutbuf+AOFF_MASEG5);
//
//   // ARoll4
//   lTmp = atoin(pOutbuf+AOFF_LANDYEAR2, RSIZ_LANDYEAR);
//   if (!lTmp)
//      memset(pOutbuf+AOFF_LANDYEAR2, ' ', RSIZ_LANDYEAR);
//   lTmp = atoin(pOutbuf+AOFF_IMPRYEAR2, RSIZ_IMPRYEAR);
//   if (!lTmp)
//      memset(pOutbuf+AOFF_IMPRYEAR2, ' ', RSIZ_IMPRYEAR);
//   lTmp = atoin(pOutbuf+AOFF_LAND2, RSIZ_LANDVAL);
//   if (lTmp > 0)
//   {
//      sprintf(acTmp, "%*u", RSIZ_LANDVAL, lTmp);
//      memcpy(pOutbuf+AOFF_LAND2, acTmp, RSIZ_LANDVAL);
//   } else
//      memset(pOutbuf+AOFF_LAND2, ' ', RSIZ_LANDVAL);
//   lTmp = atoin(pOutbuf+AOFF_IMPR2, RSIZ_LANDVAL);
//   if (lTmp > 0)
//   {
//      sprintf(acTmp, "%*u", RSIZ_LANDVAL, lTmp);
//      memcpy(pOutbuf+AOFF_IMPR2, acTmp, RSIZ_LANDVAL);
//   } else
//      memset(pOutbuf+AOFF_IMPR2, ' ', RSIZ_LANDVAL);
//
//   lTmp = atoin(pOutbuf+AOFF_DTTSALEAMT, RSIZ_LANDVAL);
//   if (lTmp > 0)
//   {
//      sprintf(acTmp, "%*u", RSIZ_LANDVAL, lTmp);
//      memcpy(pOutbuf+AOFF_DTTSALEAMT, acTmp, RSIZ_LANDVAL);
//   } else
//      memset(pOutbuf+AOFF_DTTSALEAMT, ' ', RSIZ_LANDVAL);
//
//   formatBldgChg(pOutbuf+AOFF_BLDGCHG1);
//   formatBldgChg(pOutbuf+AOFF_BLDGCHG2);
//   formatBldgChg(pOutbuf+AOFF_BLDGCHG3);
//   formatBldgChg(pOutbuf+AOFF_BLDGCHG4);
//   formatBldgChg(pOutbuf+AOFF_BLDGCHG5);
//
//   lTmp = atoin(pOutbuf+AOFF_REAPPRYEAR, RSIZ_REAPPRYEAR);
//   if (!lTmp)
//      memset(pOutbuf+AOFF_REAPPRYEAR, ' ', RSIZ_REAPPRYEAR);
//
//   lTmp = atoin(pOutbuf+AOFF_NUMUNITS, RSIZ_NUMUNITS);
//   if (lTmp > 0)
//   {
//      sprintf(acTmp, "%*u", RSIZ_NUMUNITS, lTmp);
//      memcpy(pOutbuf+AOFF_NUMUNITS, acTmp, RSIZ_NUMUNITS);
//   } else
//      memset(pOutbuf+AOFF_NUMUNITS, ' ', RSIZ_NUMUNITS);
//
//   lTmp = atoin(pOutbuf+AOFF_LASTDOCNUM, RSIZ_DOCNUM);
//   if (lTmp > 0)
//   {
//      sprintf(acTmp, "%*u", RSIZ_DOCNUM, lTmp);
//      memcpy(pOutbuf+AOFF_LASTDOCNUM, acTmp, RSIZ_DOCNUM);
//   } else
//      memset(pOutbuf+AOFF_LASTDOCNUM, ' ', RSIZ_DOCNUM);
//
//   // Flags - to be done
//   *(pOutbuf+AOFF_M_BRK) = 'B';           // B, N, or blank (no mailing)
//
//   char acCity[32], acState[16];
//   memcpy(acTmp, pRec->M_City, RSIZ_MCITYST);
//   parseCitySt(myTrim(acTmp, RSIZ_MCITYST), acCity, acState, NULL);
//   if (!memcmp(acState, "CA", 2))
//      *(pOutbuf+AOFF_M_CA) = 'Y';         // Y or N
//   else
//      *(pOutbuf+AOFF_M_CA) = 'N';         // Y or N
//
//   if (strlen(acState) == 2)
//      *(pOutbuf+AOFF_M_US) = 'Y';         // Y or N
//   else
//      *(pOutbuf+AOFF_M_US) = 'N';         // Y or N
//
//   lTmp = atoin(pRec->S_StrNo, RSIZ_SSTRNUM);
//   if (lTmp > 0)
//   {
//      *(pOutbuf+AOFF_S_YES) = 'S';        // S
//      *(pOutbuf+AOFF_S_BRK) = 'B';        // B
//   } else
//   {
//      *(pOutbuf+AOFF_S_YES) = ' ';        // S
//      *(pOutbuf+AOFF_S_BRK) = ' ';        // B
//   }
//
//   *(pOutbuf+AOFF_SBG) = ' ';
//
//   if (lSaleAmt > 9)
//   {
//      *(pOutbuf+AOFF_HAS_SALEAMT) = 'S';  // S
//      if (lSaleAmt > lGross)
//         *(pOutbuf+AOFF_L_SBG) = 'E';     // E = Sale > gross
//   } else
//      *(pOutbuf+AOFF_HAS_SALEAMT) = ' ';  // S
//
//   if (lLI > 0)
//   {
//      sprintf(acTmp, "%*u", ASIZ_LI, lLI);
//      memcpy(pOutbuf+AOFF_LI, acTmp, ASIZ_LI);
//   }
//
//   if (lGross > 0)
//   {
//      sprintf(acTmp, "%*u", ASIZ_LIPF, lGross);
//      memcpy(pOutbuf+AOFF_LIPF, acTmp, ASIZ_LIPF);
//   }
//
//   lTmp = lLI - lHomeEx;
//   if (lTmp > 0)
//   {
//      sprintf(acTmp, "%*u", ASIZ_LIH, lTmp);
//      memcpy(pOutbuf+AOFF_LIH, acTmp, ASIZ_LIH);
//   }
//
//   lTmp = lGross - (lHomeEx+lFixEx+lPerEx);
//   if (lTmp > 0)
//   {
//      sprintf(acTmp, "%*u", ASIZ_LIPF_HFP, lTmp);
//      memcpy(pOutbuf+AOFF_LIPF_HFP, acTmp, ASIZ_LIPF_HFP);
//   }
//
//   lNet = lGross - lTotalEx;
//   if (lNet > 0)
//   {
//      sprintf(acTmp, "%*u", ASIZ_NET, lNet);
//      memcpy(pOutbuf+AOFF_NET, acTmp, ASIZ_NET);
//   }
//
//   lTmp = lLI - (lHomeEx+lRelEx);
//   if (lTmp > 0)
//   {
//      sprintf(acTmp, "%*u", ASIZ_LI_HR, lTmp);
//      memcpy(pOutbuf+AOFF_LI_HR, acTmp, ASIZ_LI_HR);
//   }
//
//   if (lTotalEx > 0)
//   {
//      sprintf(acTmp, "%*u", ASIZ_TOTEX, lTotalEx);
//      memcpy(pOutbuf+AOFF_TOTEX, acTmp, ASIZ_TOTEX);
//   }
//
//   // Ratio
//   if (lImpr > 0)
//   {
//      sprintf(acTmp, "%*u", SIZ_RATIO, lImpr*100/lLI);
//      memcpy(pOutbuf+AOFF_RATIO, acTmp, SIZ_RATIO);
//   }
//
//   // Total sale
//   if (lSaleAmt > 9)
//   {
//      sprintf(acTmp, "%*u", ASIZ_SALEAMT, lSaleAmt);
//      memcpy(pOutbuf+AOFF_SALEAMT, acTmp, ASIZ_SALEAMT);
//   }
//
//   // Impr Sqft
//   if (lSqft > 0)
//   {
//      sprintf(acTmp, "%*u", ASIZ_ISQFT, lSqft);
//      memcpy(pOutbuf+AOFF_ISQFT, acTmp, ASIZ_ISQFT);
//
//      sprintf(acTmp, "%*.2f", ASIZ_IV_SQFT, (double)lImpr/lSqft);
//      memcpy(pOutbuf+AOFF_IV_SQFT, acTmp, ASIZ_IV_SQFT);
//
//      if (lSaleAmt > 9)
//      {
//         sprintf(acTmp, "%*.2f", ASIZ_SV_SQFT, (double)lSaleAmt/lSqft);
//         memcpy(pOutbuf+AOFF_SV_SQFT, acTmp, ASIZ_SV_SQFT);
//      }
//   }
//
//   // Profit
//   if (lSaleAmt > lNet)
//   {
//      sprintf(acTmp, "%*u", ASIZ_PROFIT, lSaleAmt - lNet);
//      memcpy(pOutbuf+AOFF_PROFIT, acTmp, ASIZ_PROFIT);
//   }
//
//   // Sale count
//   iTmp = 0;
//   if (pRec->Sale1Dt[0] > '0') iTmp++;
//   if (pRec->Sale2Dt[0] > '0') iTmp++;
//   if (pRec->Sale3Dt[0] > '0') iTmp++;
//   if (iTmp > 0)
//   {
//      sprintf(acTmp, "%*u", ASIZ_SALECNT, iTmp);
//      memcpy(pOutbuf+AOFF_SALECNT, acTmp, ASIZ_SALECNT);
//   }
//
//   // Mail zip
//   lTmp = atoin(pRec->M_Zip, 5);
//   if (lTmp > 0)
//   {
//      memcpy(pOutbuf+AOFF_M_ZIP, pRec->M_Zip, 5);
//
//      // Set mail flag
//      memcpy(acTmp, pRec->M_City, RSIZ_MCITYST);
//      myTrim(acTmp, RSIZ_MCITYST);
//      pTmp = strrchr(acTmp, ' ');
//      if (pTmp && strlen(pTmp) == 3)
//         *(pOutbuf+AOFF_M_FLG) = '1';
//      else
//         *(pOutbuf+AOFF_M_FLG) = '2';
//   } else
//      *(pOutbuf+AOFF_M_FLG) = '3';
//
//   // Names
//   OWNER sOwners;
//   Lax_MergeOwners(&sOwners, pRec->Name1);
//   memcpy(pOutbuf+AOFF_MSG_NAM1, sOwners.acName1, SIZ_NAME1);
//   memcpy(pOutbuf+AOFF_CLN_NAM1, sOwners.acName1, SIZ_NAME1);
//   memcpy(pOutbuf+AOFF_SWP_NAM1, sOwners.acSwapName, SIZ_NAME_SWAP);
//
//   // Title code
//
//   // Title
//
//   // Recording info - come from sale file
//
//   return 0;
//}

/********************************* CreateLaxAssr ****************************
 *
 *
 ****************************************************************************/

//int Lax_CreateAssr(int iFirstRec /* 1=create header rec */)
//{
//   char     *pTmp, acBuf[MAX_RECSIZE], acRollRec[MAX_RECSIZE];
//   char     acOutFile[_MAX_PATH], acTmp[256];
//
//   HANDLE   fhOut;
//
//   int      iRet, iRollUpd=0, iNewRec=0, iRetiredRec=0;
//   DWORD    nBytesWritten;
//   BOOL     bRet, bEof;
//   long     lRet=0, lCnt=0;
//
//   // Create raw file name
//   GetIniString(myCounty.acCntyCode, "AssrFile", "", acTmp, _MAX_PATH, acIniFile);
//   if (!iAsrRecLen)
//      iAsrRecLen = GetPrivateProfileInt(myCounty.acCntyCode, "ARecLen", 2065, acIniFile);
//   sprintf(acOutFile, acTmp, myCounty.acCntyCode, myCounty.acCntyCode, "R01");
//
//   // Open roll file
//   LogMsg("Open lien file %s", acLienFile);
//   fdRoll = fopen(acLienFile, "r");
//   if (fdRoll == NULL)
//   {
//      printf("***** Error opening roll file: %s\n", acLienFile);
//      return 2;
//   }
//
//   // Open Output file
//   LogMsg("Open output file %s", acOutFile);
//   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
//          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);
//
//   if (fhOut == INVALID_HANDLE_VALUE)
//      return 4;
//
//   // Output first header record
//   if (iFirstRec > 0)
//   {
//      memset(acBuf, '9', iAsrRecLen);
//      bRet = WriteFile(fhOut, acBuf, iAsrRecLen, &nBytesWritten, NULL);
//      lCnt = 1;
//   }
//
//   // Get first RollRec
//   pTmp = fgets((char *)&acRollRec[0], iRollLen, fdRoll);
//   bEof = (pTmp ? false:true);
//
//   // Init variables
//   iNoMatch=iBadCity=iBadSuffix=0;
//
//   // Merge loop
//   while (pTmp)
//   {
//      // Skip test record
//      if (!memcmp(acRollRec, "9999999", 7))
//      {
//         pTmp = fgets(acRollRec, iRollLen, fdRoll);
//         continue;
//      }
//
//      // Create new R01 record
//      iRet = Lax_CreateAssrRec(acBuf, acRollRec, CREATE_R01|CREATE_LIEN);
//
//      //if (fdGrGr)
//      //   Lax_AddGrGr(acBuf);
//
//      sprintf(acTmp, "%*u", ASIZ_HAS_SEQ, lCnt);
//      memcpy((char *)&acBuf[AOFF_HAS_SEQ], acTmp, ASIZ_HAS_SEQ);
//
//      bRet = WriteFile(fhOut, acBuf, iAsrRecLen, &nBytesWritten, NULL);
//      if (!(++lCnt % 1000))
//      {
//         printf("\r%u", lCnt);
//         // Break output into 800000 record chunks
//
//         if (!(lCnt % 525000))
//         {
//            if (fhOut) CloseHandle(fhOut);
//
//            acOutFile[strlen(acOutFile)-1]++;
//            LogMsg("Open output file %s", acOutFile);
//            fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
//                    FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);
//
//            if (fhOut == INVALID_HANDLE_VALUE)
//            {
//               fhOut = 0;
//               break;
//            }
//         }
//      }
//
//      if (!bRet)
//      {
//         LogMsg("***** Error writing to output file at record %d\n", lCnt);
//         lRet = WRITE_ERR;
//         break;
//      }
//
//      // Get next roll record
//      pTmp = fgets(acRollRec, iRollLen, fdRoll);
//   }
//
//   // Close files
//   if (fhOut)
//      CloseHandle(fhOut);
//
//   if (fdRoll)
//      fclose(fdRoll);
//
//   // Sort output file
//   //sprintf(acTmp, "S(1,14,C,A) F(FIX,%d) B(%d,R)", iRecLen, iRecLen);
//   //lRet = sortFile(acTmpFile, acOutFile, acTmp);
//
//   LogMsg("Total output records:       %u", lCnt);
//   LogMsg("Total bad-city records:     %u", iBadCity);
//   LogMsg("Total bad-suffix records:   %u", iBadSuffix);
//   LogMsg("Last recording date:        %u", lLastRecDate);
//   printf("\nTotal output records: %u\n", lCnt);
//
//   lAssrRecCnt = lCnt;
//   return 0;
//}

/********************************** MergeLaxAssr ******************************
 *
 *
 ******************************************************************************/

//int Lax_MergeAssr(int iSkip)
//{
//   char     *pTmp, acRec[MAX_RECSIZE], acBuf[MAX_RECSIZE], acRollRec[MAX_RECSIZE];
//   char     cFileCnt=1;
//   char     acRawFile[_MAX_PATH], acOutFile[_MAX_PATH], acTmp[256];
//
//   HANDLE   fhIn, fhOut;
//
//   int      iRet, iTmp, iRollUpd=0, iNewRec=0, iRetiredRec=0;
//   DWORD    nBytesRead;
//   DWORD    nBytesWritten;
//   BOOL     bRet, bEof;
//   long     lRet=0, lCnt=0;
//
//   GetIniString(myCounty.acCntyCode, "AssrFile", "", acTmp, _MAX_PATH, acIniFile);
//   if (!iAsrRecLen)
//      iAsrRecLen = GetPrivateProfileInt(myCounty.acCntyCode, "ARecLen", 2065, acIniFile);
//   sprintf(acOutFile, acTmp, myCounty.acCntyCode, myCounty.acCntyCode, "R01");
//   sprintf(acRawFile, acTmp, myCounty.acCntyCode, myCounty.acCntyCode, "S01");
//
//   // Rename files for processing
//   if (_access(acRawFile, 0))
//   {
//      if (!_access(acOutFile, 0))
//         rename(acOutFile, acRawFile);
//      else
//      {
//         LogMsg("***** Missing input file %s.  Please recheck!", acRawFile);
//         return 1;
//      }
//   }
//
//   // Open updated roll file
//   LogMsg("Open Roll file %s", acRollFile);
//   fdRoll = fopen(acRollFile, "r");
//   if (fdRoll == NULL)
//   {
//      LogMsg("***** Error opening roll file: %s\n", acRollFile);
//      return 2;
//   }
//
//   // Open Input file
//   LogMsg("Open input file %s", acRawFile);
//   fhIn = CreateFile(acRawFile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
//          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);
//
//   if (fhIn == INVALID_HANDLE_VALUE)
//   {
//      LogMsg("***** Error open input file: %s", acRawFile);
//      return 3;
//   }
//
//   // Open Output file - use tmp file so we can sort at the end to R01 file
//   LogMsg("Open output file %s", acOutFile);
//   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
//          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);
//
//   if (fhOut == INVALID_HANDLE_VALUE)
//   {
//      LogMsg("***** Error open output file: %s", acOutFile);
//      return 4;
//   }
//
//   // Get first RollRec
//   pTmp = fgets((char *)&acRollRec[0], iRollLen, fdRoll);
//   bEof = (pTmp ? false:true);
//
//   // Copy skip record
//   memset(acBuf, ' ', iAsrRecLen);
//   while (iSkip-- > 0)
//   {
//      ReadFile(fhIn, acBuf, iAsrRecLen, &nBytesRead, NULL);
//      // Ignore record start with 99999999
//      //if (memcmp(acBuf, "99999999", 8))
//         bRet = WriteFile(fhOut, acBuf, iAsrRecLen, &nBytesWritten, NULL);
//   }
//
//   iNoMatch=iBadCity=iBadSuffix=0;
//
//   // Merge loop
//   while (!bEof)
//   {
//      bRet = ReadFile(fhIn, acBuf, iAsrRecLen, &nBytesRead, NULL);
//
//      // Check for EOF
//      if (!bRet)
//      {
//         LogMsg("***** Error reading input file %s (%f)", acRawFile, GetLastError());
//         break;
//      }
//
//      // EOF ?
//      if (!nBytesRead)
//      {
//         // EOF
//         cFileCnt++;
//         acRawFile[strlen(acRawFile)-1] = cFileCnt | 0x30;
//         if (!_access(acRawFile, 0))
//         {
//            CloseHandle(fhIn);
//            CloseHandle(fhOut);
//            fhIn = 0;
//            fhOut = 0;
//
//            // Open next Input file
//            LogMsg("Open input file %s", acRawFile);
//            fhIn = CreateFile(acRawFile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
//                  FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);
//
//            if (fhIn == INVALID_HANDLE_VALUE)
//               break;
//            bRet = ReadFile(fhIn, acBuf, iAsrRecLen, &nBytesRead, NULL);
//
//            // Open Output file
//            acOutFile[strlen(acOutFile)-1] = cFileCnt | 0x30;
//            LogMsg("Open output file %s", acOutFile);
//            fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
//                  FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);
//            if (fhOut == INVALID_HANDLE_VALUE)
//               break;
//         } else
//            break;
//      }
//
//      NextRollRec:
//      iTmp = memcmp(acBuf, acRollRec, iApnLen);
//      if (!iTmp)
//      {
//         // Update roll data
//         iRet = Lax_CreateAssrRec(acBuf, acRollRec, UPDATE_R01);
//         iRollUpd++;
//
//         // Read next roll record
//         pTmp = fgets(acRollRec, iRollLen, fdRoll);
//
//         if (!pTmp || !memcmp(acRollRec, "9999999", 7))
//            bEof = true;    // Signal to stop
//      } else if (iTmp > 0)       // Roll not match, new roll record?
//      {
//         if (bDebug)
//            LogMsg0("New roll record : %.*s (%d)", iApnLen, acRollRec, lCnt);
//
//         // Create new R01 record
//         iRet = Lax_CreateAssrRec(acRec, acRollRec, CREATE_R01);
//         iNewRec++;
//
//         bRet = WriteFile(fhOut, acRec, iAsrRecLen, &nBytesWritten, NULL);
//         lCnt++;
//
//         // Get next roll record
//         pTmp = fgets(acRollRec, iRollLen, fdRoll);
//
//         if (!pTmp || !memcmp(acRollRec, "9999999", 7))
//            bEof = true;    // Signal to stop
//         else
//            goto NextRollRec;
//      } else
//      {
//         // Record may be retired
//         if (bDebug)
//            LogMsg0("Retired record? : R01->%.*s < Roll->%.*s (%d)", iApnLen, acBuf, iApnLen, acRollRec, lCnt);
//         iRetiredRec++;
//         continue;
//      }
//
//      bRet = WriteFile(fhOut, acBuf, iAsrRecLen, &nBytesWritten, NULL);
//      if (!(++lCnt % 1000))
//         printf("\r%u", lCnt);
//
//      if (!bRet)
//      {
//         LogMsg("***** Error occurs: %d\n", GetLastError());
//         lRet = WRITE_ERR;
//         break;
//      }
//   }
//
//   // Close files
//   if (fdRoll)
//      fclose(fdRoll);
//   if (fhOut)
//      CloseHandle(fhOut);
//   if (fhIn)
//      CloseHandle(fhIn);
//
//   LogMsg("Total output records:       %u", lCnt);
//   LogMsg("Total new records:          %u", iNewRec);
//   LogMsg("Total retired records:      %u", iRetiredRec);
//   LogMsg("Total bad-city records:     %u", iBadCity);
//   LogMsg("Total bad-suffix records:   %u", iBadSuffix);
//
//   printf("\nTotal output records: %u\n", lCnt);
//
//   lAssrRecCnt = lCnt;
//   return 0;
//}

/******************************* Lax_MergeSaleHist ***************************
 *
 * Update history sale.  This function adds new sale record to cumulative file.
 *
 * Return 0 if successful, >0 if error
 *
 * Removed 4/4/2017
 *
 *****************************************************************************/

/********************************** MergeLaxRoll ******************************
 *
 * Create LA Region files.
 * This function will split SLAX.R* into SLAX1.R*, SLAX2.R*, and SLAX3.R*
 *
 ******************************************************************************/

int Lax_CreateRegions(int iSkip)
{
   char     acBuf[MAX_RECSIZE], *pRegion;
   char     cFileCnt=1;
   char     acRawFile[_MAX_PATH], acReg1[_MAX_PATH], acReg2[_MAX_PATH], acReg3[_MAX_PATH];

   HANDLE   fhIn, fhReg1, fhReg2, fhReg3;

   DWORD    nBytesRead, nBytesWritten;
   BOOL     bRet;
   long     lCnt, lCnt0, lCnt1, lCnt2, lCnt3;

   sprintf(acRawFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "R01");
   sprintf(acReg1, acRawTmpl, "LA1", "LA1", "R01");
   sprintf(acReg2, acRawTmpl, "LA2", "LA2", "R01");
   sprintf(acReg3, acRawTmpl, "LA3", "LA3", "R01");

   // Open Input file
   LogMsg("Open input file %s", acRawFile);
   fhIn = CreateFile(acRawFile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhIn == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error open input file: %s", acRawFile);
      return 3;
   }

   // Open Output file
   LogMsg("Open LA1 file %s", acReg1);
   fhReg1 = CreateFile(acReg1, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhReg1 == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error open LA1 file: %s", acReg1);
      return 4;
   }
   LogMsg("Open LA2 file %s", acReg2);
   fhReg2 = CreateFile(acReg2, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhReg2 == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error open LA2 file: %s", acReg2);
      return 4;
   }
   LogMsg("Open LA3 file %s", acReg3);
   fhReg3 = CreateFile(acReg3, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhReg3 == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error open LA3 file: %s", acReg3);
      return 4;
   }

   // Copy skip record
   memset(acBuf, ' ', iRecLen);
   if (iSkip > 0)
   {
      ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);
      bRet = WriteFile(fhReg1, acBuf, iRecLen, &nBytesWritten, NULL);
      bRet = WriteFile(fhReg2, acBuf, iRecLen, &nBytesWritten, NULL);
      bRet = WriteFile(fhReg3, acBuf, iRecLen, &nBytesWritten, NULL);
   }

   lCnt=lCnt1=lCnt2=lCnt3=lCnt0 = 0;
   pRegion = (char *)&acBuf[OFF_MAPDIV_LA];

   // Merge loop
   while (true)
   {
      bRet = ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);

      // Check for EOF
      if (!bRet)
      {
         LogMsg("***** Error reading input file %s (%f)", acRawFile, GetLastError());
         break;
      }

      // EOF ?
      if (!nBytesRead)
      {
         // EOF
         cFileCnt++;
         acRawFile[strlen(acRawFile)-1] = cFileCnt | 0x30;
         if (!_access(acRawFile, 0))
         {
            CloseHandle(fhIn);
            fhIn = 0;

            // Open next Input file
            LogMsg("Open input file %s", acRawFile);
            fhIn = CreateFile(acRawFile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
                  FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

            if (fhIn == INVALID_HANDLE_VALUE)
               break;
            bRet = ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);

         } else
            break;
      }

      acBuf[OFF_CO_ID+2] = *pRegion;
      switch (*pRegion)
      {
         case '1':
            bRet = WriteFile(fhReg1, acBuf, iRecLen, &nBytesWritten, NULL);
            lCnt1++;
            break;
         case '2':
            bRet = WriteFile(fhReg2, acBuf, iRecLen, &nBytesWritten, NULL);
            lCnt2++;
            break;
         case '3':
            bRet = WriteFile(fhReg3, acBuf, iRecLen, &nBytesWritten, NULL);
            lCnt3++;
            break;
         default:
            lCnt0++;
            LogMsg("*** WARNING: record with unassigned region: %.*s", myCounty.iApnLen, acBuf);
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);

      if (!bRet)
      {
         LogMsg("***** Error writing to output file: %d\n", GetLastError());
         break;
      }
   }

   // Close files
   if (fhReg1)
      CloseHandle(fhReg1);
   if (fhReg2)
      CloseHandle(fhReg2);
   if (fhReg3)
      CloseHandle(fhReg3);
   if (fhIn)
      CloseHandle(fhIn);

   alRegCnt[1] = lCnt1;
   alRegCnt[2] = lCnt2;
   alRegCnt[3] = lCnt3;

   LogMsg("Total output records:       %u", lCnt);
   LogMsg("Total region 1 records:     %u", lCnt1);
   LogMsg("Total region 2 records:     %u", lCnt2);
   LogMsg("Total region 3 records:     %u", lCnt3);
   LogMsg("Total records /w no region: %u", lCnt0);

   printf("\nTotal output records: %u\n", lCnt);

   lRecCnt = lCnt;
   return 0;
}

/******************************** Lax_FindApn ********************************
 *
 * Return 0 if not found,
 *
 *****************************************************************************/

int Lax_FindApn(char *pApn, char *pDocNum, FILE *fdApn)
{
   static   char acRec[1024], *pRec=NULL;
   int      iRet;

   if (!fdApn)
      return 0;

   // Get first rec for first call
   if (!pRec)
   {
      // Only take record start with 2.  This may last for 1000 years
      do {
         pRec = fgets(acRec, 1024, fdApn);
      } while (*pRec != '2' && !feof(fdApn));
   }

   do
   {
      if (!pRec || feof(fdApn))
      {
         fclose(fdApn);
         fdApn = NULL;
         return 0;      // EOF
      }

      // Compare DocNum
      iRet = memcmp(pDocNum, pRec+4, GSIZ_DOCNUM);
      if (iRet > 0)
      {
         if (bDebug)
            LogMsg0("Skip APN rec: %.26s", pRec);
         do {
            pRec = fgets(acRec, 1024, fdApn);
         } while (!feof(fdApn) && *pRec != '2');
         lApnSkip++;
      }
   } while (iRet > 0);

   // If not match, return
   if (iRet)
      return 0;

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "003120007", 9))
   //   lTmp = 0;
#endif

   // token 0=DocNum, 1=Num Parcels, 2=APN, 3=DocDate
   iRet = ParseString(pRec, ',', 4, apTokens);
   if (iRet > 3 && strlen(apTokens[2]) == RSIZ_APN && isdigit(*apTokens[2]))
   {
      strcpy(pApn, apTokens[2]);
      lApnMatch++;
      iRet = 1;
   } else
   {
      if (bDebug)
         LogMsg0("* Bad APN for doc: %s (tokens=%d, APN=%s)", apTokens[0], iRet, apTokens[2]);
      iRet = 0;
   }

   // Get next rec
   do {
      pRec = fgets(acRec, 1024, fdApn);
   } while (!feof(fdApn) && *pRec != '2');

   return iRet;
}

/******************************************************************************
 *
 * Return true if success.  
 *
 ******************************************************************************/

bool findDoc(LPCSTR pDocNum)
{
   char     acTmp[256];
   bool     bRet=false;
   hlAdoRs  myRs;

   sprintf(acTmp, "SELECT DocDate FROM %s WHERE DocNum='%s'", acGrGrTbl, pDocNum);
   try
   {
      myRs.Open(dbLoadConn, acTmp);
      if (myRs.next())
         bRet = true;
      myRs.Close();
   } AdoCatch(e)
   {
      LogMsg("***** Error exec cmd: %s", acTmp);
      LogMsg("%s\n", ComError(e));
   }

   return bRet;
}

/******************************* Lax_LoadGrGrXml ****************************
 *
 * Load XML file into fixed length
 * Note that in order to pull data fields out correctly, they have to be pull
 * in the order they were put in the XML section.  See TransferTax section as example.
 *
 * TotalTax=CityTax+CountyTax.  If CityTax=CountyTax, use TotalTax to calculate sale price.
 * However, if CityTax and CountyTax are not the same, use CountyTax to calculate sale price.
 *
 * Return number of output record.  <0 if error.
 *
 ****************************************************************************/

int Lax_LoadGrGrXml(LPCSTR pXmlFile) 
{
   // Get pathname
   CString  csText, csNotes, csCity, csCityCode;
   CString  sDocTitle, sDocNum, sDocTax, sApn, sPageCnt, sPrclCnt, sGrtor[2], sGrtee[2];
   CFile    file;
   int      iRet, iGrtor, iGrtee, iNewCnt=0, iUpdCnt=0;
   char     acTmp[1024], acDocDate[16];

   LogMsgD("Load GrGr file: %s", pXmlFile);

#ifdef _DEBUG
   //if (!stricmp(pXmlFile, "F:\\CO_OUT\\LAX\\01272010-FullData.xml"))
   //   iRet = 0;
#endif

   if (!file.Open( pXmlFile, CFile::modeRead ) )
   {
      LogMsg("***** Error unable to open file: %s", pXmlFile);
      return -1;
   }
   int nFileLen = (int)file.GetLength();

   // Allocate buffer for binary file data
   unsigned char* pBuffer = new unsigned char[nFileLen + 2];
   nFileLen = file.Read( pBuffer, nFileLen );
   file.Close();
   pBuffer[nFileLen] = '\0';
   pBuffer[nFileLen+1] = '\0'; // in case 2-byte encoded

   // Windows Unicode file is detected if starts with FEFF
   if (pBuffer[0] == 0xff && pBuffer[1] == 0xfe )
   {
      // Contains byte order mark, so assume wide char content
      // non _UNICODE builds should perform UCS-2 (wide char) to UTF-8 conversion here
      csText = (LPCWSTR)(&pBuffer[2]);
      csNotes += _T("File starts with hex FFFE, assumed to be wide char format. ");
   } else
   {
      // _UNICODE builds should perform UTF-8 to UCS-2 (wide char) conversion here
      csText = (LPCSTR)pBuffer;
   }
   delete [] pBuffer;

   // If it is too short, assume it got truncated due to non-text content
   if (csText.GetLength() < nFileLen / 2 - 20 )
   {
      LogMsg("***** Error converting file to string (may contain binary data)");
      return -1;
   }

   // Parse
   CMarkup xml;
   BOOL bResult = xml.SetDoc( csText );

   acDocDate[0] = 0;
   if ( xml.FindChildElem(_T("Header")) )
   {
      if (!Check(xml.IntoElem(), "Look into Header"))
         return -2;

      // Get RecordingDate
      if (xml.FindChildElem( _T("RecordingTS") ))
      {
         csNotes = xml.GetChildData();
         sprintf(acDocDate, "%.4s%.2s%.2s", csNotes, csNotes.Mid(5, 5), csNotes.Mid(8, 2));
      }

      while (xml.FindChildElem( _T("DocumentHeader") ) )
      {
         Check(xml.IntoElem(), "Look into DocumentHeader");
         
         sPrclCnt = sPageCnt = sDocNum = sApn = "";
         // Get DocumentNumber
         if (xml.FindChildElem( _T("DocumentNumber") ))
         {
            sDocNum = xml.GetChildData();
         }

         // Page count
         if (xml.FindChildElem( _T("PageCount")))
            sPageCnt = xml.GetChildData();

         // Parcel count
         if (xml.FindChildElem( _T("ParcelCount")))
            sPrclCnt = xml.GetChildData();

         // Get AIN
         iRet = xml.FindChildElem( _T("AIN") );
         sApn = xml.GetChildData();
         if (iApnLen == sApn.GetLength())
         {
            // Find doc in table
            //if (findDoc(sDocNum))
            //{
            //   // Update SQL db
            //   sprintf(acTmp, "UPDATE %s SET Apn='%s', NumPages=%s, NumParcels=%s WHERE DocNum='%s'",
            //      acGrGrTbl, sApn, sPageCnt, sPrclCnt, sDocNum);
            //   iUpdCnt++;
            //} else
            {
               // Insert into SQL db
               sprintf(acTmp, "INSERT INTO %s (DocNum, DocDate, Apn, NumPages, NumParcels) VALUES('%s',%s,'%s',%s,%s)",
                  acGrGrTbl, sDocNum, acDocDate, sApn, sPageCnt, sPrclCnt);
               iNewCnt++;
            }
            iRet = execCmdEx(acTmp, &dbLoadConn);
         }

         Check(xml.OutOfElem(), "Out of DocumentHeader");
      }
      Check(xml.OutOfElem(), "Out of Header");
   }

   if (xml.FindChildElem(_T("DataHeader")) )
   {
      if (!Check(xml.IntoElem(), "Look into DataHeader"))
         return -4;

      while (xml.FindChildElem( _T("DocumentInfo") ) )
      {
         Check(xml.IntoElem(), "Look into DocumentInfo");

         sDocTitle = sDocNum = "";
         sGrtor[0] = sGrtor[1] = sGrtee[0] = sGrtee[1] = "";

         // Get DocumentNumber
         if (xml.FindChildElem( _T("DocumentNumber") ))
         {
            sDocNum = xml.GetChildData();
#ifdef _DEBUG
            //if (!memcmp(sDocNum, "20100116423", 11))
            //   iRet = 0;
#endif
         }

         // HasPCORSurcharge

         // Names
         iGrtor = iGrtee = 0;
         while (xml.FindChildElem( _T("IndexNames") ))
         {
            Check(xml.IntoElem(), "Look into IndexNames");

            iRet = xml.FindChildElem( _T("PartyTypeCode") );
            csNotes = xml.GetChildData();
            if (csNotes == "1")           // Grantee
            {
               iRet = xml.FindChildElem( _T("Name") );
               csNotes = xml.GetChildData();

               // Remove single quote
               while ((iRet = csNotes.Find(39)) >= 0)
                  csNotes.Delete(iRet, 1);

               if (iGrtee > 1)
               {
                  sprintf(acTmp, "INSERT INTO %s_Names (DocNum, DocDate, SeqNum, NameType, Name) VALUES('%s',%s,%d,'%s','%s')",
                     myCounty.acCntyCode, sDocNum, acDocDate, iGrtee-1, "E", csNotes);
                  iRet = execCmdEx(acTmp, &dbLoadConn);
               } else
                  sGrtee[iGrtee] = csNotes;
               iGrtee++;
            } else if (csNotes == "0")    // Grantor
            {
               iRet = xml.FindChildElem( _T("Name") );
               csNotes = xml.GetChildData();

               // Remove single quote
               while ((iRet = csNotes.Find(39)) >= 0)
                  csNotes.Delete(iRet, 1);

               if (iGrtor > 1)
               {
                  sprintf(acTmp, "INSERT INTO %s_Names (DocNum, DocDate, SeqNum, NameType, Name) VALUES('%s',%s,%d,'%s','%s')",
                     myCounty.acCntyCode, sDocNum, acDocDate, iGrtor-1, "O", csNotes);
                  iRet = execCmdEx(acTmp, &dbLoadConn);
               } else
                  sGrtor[iGrtor] = csNotes;
               iGrtor++;
            }
            Check(xml.OutOfElem(), "Out of IndexNames");
         }

         // Tax
         double   dTmp;
         long     lPrice, lCityTax, lCountyTax, lTotalTax;

#ifdef _DEBUG
         //if (!memcmp(sDocNum, "20161259555", 11))
         //   lPrice = 0;
#endif

         csCityCode = csCity = "";
         lPrice=lCityTax=lCountyTax=lTotalTax = 0;
         if (xml.FindChildElem( _T("TransferTax") ))
         {
            Check(xml.IntoElem(), "Look into TransferTax");

            // City code - CityCode
            if (xml.FindChildElem( _T("CityCode")))
               csCityCode = xml.GetChildData();

            // City name - CityName
            if (xml.FindChildElem( _T("CityName")))
               csCity = xml.GetChildData();

            // Total tax - TaxAmount
            if (xml.FindChildElem( _T("TaxAmount")))
            {
               csNotes = xml.GetChildData();

               // Format tax value
               dTmp = atof(csNotes);
               if (dTmp > 0.0)
                  lTotalTax = (long)((dTmp+0.001)*100.0);
            }          

            // City Tax - CityTaxAmount
            if (xml.FindChildElem( _T("CityTaxAmount")))
            {
               csNotes = xml.GetChildData();

               // Format tax value
               dTmp = atof(csNotes);
               if (dTmp > 0.0)
                  lCityTax = (long)((dTmp+0.001)*100.0);
            }

            // County Tax - CountytaxAmount
            if (xml.FindChildElem( _T("CountytaxAmount")))
            {
               csNotes = xml.GetChildData();

               // Format tax value
               dTmp = atof(csNotes);
               if (dTmp > 0.0)
                  lCountyTax = (long)((dTmp+0.001)*100.0);
            }

            // TotalTax=CityTax+CountyTax.  However, if CityTax and CountyTax are not the same, 
            // use CountyTax to calculate sale price.  Don't change the logic below.
            if (lCityTax == lCountyTax || lCityTax == lCountyTax+1 || lCityTax == lCountyTax+2 || lCountyTax == 0)
            {
               lPrice = (long)(lTotalTax * SALE_FACTOR_100);
               if (lCityTax != lCountyTax && (long)(lPrice/100)*100 != lPrice)
                  lPrice = (long)((lCountyTax+lCityTax) * SALE_FACTOR_100);
            } else
               lPrice = (long)(lCountyTax * SALE_FACTOR_100);

            Check(xml.OutOfElem(), "Out of TransferTax");
         }

         // Doc Title
         if (xml.FindChildElem( _T("Titles") ))
         {
            Check(xml.IntoElem(), "Look into Titles");

            iRet = xml.FindChildElem( _T("TitleDescription") );
            sDocTitle = xml.GetChildData();
            Check(xml.OutOfElem(), "Out of Titles");
         }

         // Update GrGr table
         sprintf(acTmp, "UPDATE %s SET DocTitle='%s', DocTax=%d, SalePrice=%d, NameCnt=%d, Grtee1='%s', Grtee2='%s', Grtor1='%s', Grtor2='%s', "
            "CityTax=%d, CountyTax=%d, City='%s', CityCode='%s' WHERE DocNum='%s'",
            acGrGrTbl, sDocTitle, lTotalTax, lPrice, iGrtee, sGrtee[0], sGrtee[1], sGrtor[0], sGrtor[1], 
            lCityTax, lCountyTax, csCity, csCityCode, sDocNum);
         iRet = execCmdEx(acTmp, &dbLoadConn);

         Check(xml.OutOfElem(), "Out of DocumentInfo");
      }

      Check(xml.OutOfElem(), "Out of DataHeader");
   }

   LogMsg("New Rec   : %d", iNewCnt);
   LogMsg("Update Rec: %d", iUpdCnt);

   return iNewCnt;
}

/********************************* Lax_LoadGrGr *****************************
 *
 * Load GrGr data into SQL table then extract to GrGr_Exp.dat
 * Use Lax_GrGr & Lax_Names tables to store data.
 *
 * If successful, return number of records processed.  Otherwise error.
 *
 ****************************************************************************/

int Lax_LoadGrGr(LPCSTR pCnty) 
{
   char     acGrGrIn[_MAX_PATH], acGrGrBak[_MAX_PATH], acUnzipDir[_MAX_PATH];
   char     *pTmp, acTmp[256], acXmlFile[_MAX_PATH], acZipFile[_MAX_PATH];
   struct   _finddata_t  sFileInfo;

   int      iCnt, iRet;
   long     lCnt, lHandle, lTmp;
   bool     bZipInput = false;
   FILE_ITEM   asFileList[64];

   // Get raw file name
   GetIniString(pCnty, "GrGrIn", "", acGrGrIn, _MAX_PATH, acIniFile);
   if (!_memicmp(&acGrGrIn[strlen(acGrGrIn)-3], "zip", 3) || !_memicmp(&acGrGrIn[strlen(acGrGrIn)-3], "dat", 3))
      bZipInput = true;

   // Open Input file
   lHandle = _findfirst(acGrGrIn, &sFileInfo);
   if (lHandle > 0)
   {
      pTmp = strrchr(acGrGrIn, '\\');
      if (pTmp)
         *pTmp = 0;
   } else
   {
      LogMsg("*** No new file avail for processing: %s", acGrGrIn);
      return 0;
   }

   // Create backup folder
   sprintf(acGrGrBak, acGrBkTmpl, pCnty, acToday);
   if (_access(acGrGrBak, 0))
      _mkdir(acGrGrBak);

   // Check for Zip input
   iRet = iCnt = 0;
   if (bZipInput)
   {
      // Initialize Zip server
      doZipInit();

      // Set unzip folder
      GetIniString(myCounty.acCntyCode, "UnzipFolder", acTmpPath, acUnzipDir, _MAX_PATH, acIniFile);
      setUnzipToFolder(acUnzipDir);
      setReplaceIfExist(true);

      while (!iRet)
      {
         // unzip file
         sprintf(acZipFile, "%s\\%s", acGrGrIn, sFileInfo.name);
         LogMsg("Unzip input file %s", acZipFile);

         // get file name
         iCnt += getZipFileContents(acZipFile, &asFileList[0], 64);

         // Unzip
         iRet = startUnzip(acZipFile);
         if (iRet)
         {
            LogMsg("***** Error unzipping %s.  Program terminated", acZipFile);
            return -2;
         }

         sprintf(acTmp, "%s\\%s", acGrGrBak, sFileInfo.name);
         rename(acZipFile, acTmp);

         // Find next file
         iRet = _findnext(lHandle, &sFileInfo);
      }

      // Close handle
      _findclose(lHandle);

      // Unload Zip DLL
      doZipShutdown();

      // Search for Input file
      sprintf(acTmp, "%s\\*.xml", acUnzipDir);
      lHandle = _findfirst(acTmp, &sFileInfo);
      if (!lHandle)
      {
         LogMsg("*** No new file avail for processing: %s", acTmp);
         return 0;
      }
   }

   // Delete old data
   sprintf(acTmp, "DELETE FROM %s", acGrGrTbl);
   iRet = execCmdEx(acTmp, &dbLoadConn);
   if (iRet)
      return iRet;

   sprintf(acTmp, "DELETE FROM %s_Names", pCnty);
   iRet = execCmdEx(acTmp, &dbLoadConn);
   if (iRet)
      return iRet;

   iCnt = lCnt = 0;
   while (!iRet)
   {
      if (bZipInput)
         sprintf(acXmlFile, "%s\\%s", acUnzipDir, sFileInfo.name);
      else
         sprintf(acXmlFile, "%s\\%s", acGrGrIn, sFileInfo.name);

      lTmp = getFileDate(acXmlFile);
      if (lTmp > lLastGrGrDate)
         lLastGrGrDate = lTmp;

      // Parse input file
      iRet = Lax_LoadGrGrXml(acXmlFile);
      if (iRet < 0)
         LogMsg("*** Skip %s", acXmlFile);
      else
      {
         lCnt += iRet;

         if (bZipInput)
            remove(acXmlFile);
         else
         {
            sprintf(acTmp, "%s\\%s", acGrGrBak, sFileInfo.name);
            rename(acXmlFile, acTmp);
         }

      }

      iCnt++;
      // Find next file
      iRet = _findnext(lHandle, &sFileInfo);
   }

   // Close handle
   _findclose(lHandle);

   /* Skip this section.  We no longer need this - 05/18/2010 sn
   // Update GrGr data using RDRENT*.CSV
   GetPrivateProfileString(pCnty, "GrGrDoc", "", acGrGrIn, 128, acIniFile);
   strcpy(acXmlFile, acGrGrIn);
   strcpy(&acXmlFile[strlen(acXmlFile)-3], "dat");

   // If DAT file is present, unzip it to the same folder
   lHandle = _findfirst(acXmlFile, &sFileInfo);
   if (lHandle > 0)
   {
      pTmp = strrchr(acXmlFile, '\\');
      *pTmp = 0;

      // Initialize Zip server
      doZipInit();

      // Set unzip folder
      setUnzipToFolder(acXmlFile);
      setReplaceIfExist(true);
      iCnt = iRet = 0;
      while (!iRet)
      {
         // unzip file
         sprintf(acZipFile, "%s\\%s", acXmlFile, sFileInfo.name);
         LogMsgD("Unzip input file %s", acZipFile);

         // get file name
         iCnt += getZipFileContents(acZipFile, &asFileList[0], 64);

         // Unzip
         iRet = startUnzip(acZipFile);
         if (iRet)
         {
            LogMsg("***** Error unzipping %s.  Program terminated", acZipFile);
            return -2;
         }

         sprintf(acTmp, "%s\\%s", acGrGrBak, sFileInfo.name);
         rename(acZipFile, acTmp);

         // Find next file
         iRet = _findnext(lHandle, &sFileInfo);
      }

      // Close handle
      _findclose(lHandle);

      // Unload Zip DLL
      doZipShutdown();
   }

   LogMsg("Loading %s into %s", acGrGrIn, acGrGrTbl);
   // Open Input file
   lHandle = _findfirst(acGrGrIn, &sFileInfo);
   if (lHandle > 0)
   {
      pTmp = strrchr(acGrGrIn, '\\');
      if (pTmp)
         *pTmp = 0;
   } else
   {
      LogMsg("*** No new file avail for processing: %s", acGrGrIn);
      return -1;
   }

   bool bMultiByte, bTestFld;
   int  iDocNum, iDocDate, iDateType, iMaxFlds;

   iCnt = iRet = 0;
   while (!iRet)
   {
      sprintf(acCsvFile, "%s\\%s", acGrGrIn, sFileInfo.name);
      lTmp = getFileDate(acCsvFile);
      if (lTmp > lLastGrGrDate)
         lLastGrGrDate = lTmp;

      LogMsgD("Process %s", acCsvFile);
      if (fdGrGr = fopen(acCsvFile, "r"))
      {
         hlAdoRs  myRs;
         char     sDocDate[32], sDocNum[32];

         bTestFld = false;
         iDocNum = LAX_RD2_DOCNUM;
         iDocDate = LAX_RD2_DOCDATE;
         iMaxFlds = LAX_RD2_NAME;

         pTmp = fgets(acRec, 1024, fdGrGr);
         if (*pTmp == -1)
         {
            bMultiByte = true;
            if (acRec[2] == 't')
            {
               iDocNum = LAX_RD1_DOCNUM;
               iDocDate = LAX_RD1_DOCDATE;
               iMaxFlds = LAX_RD1_NAME;
               bTestFld = true;
            }
            iDateType = MM_DD_YYYY_1;
            if (!isdigit(acRec[2]))
               pTmp = fgets(acRec, 512, fdGrGr);
         } else
         {
            bMultiByte = false;
            iDateType = YYYY_MM_DD;
         }

         do
         {
            if (!pTmp)
               break;

            if (!(iCnt % 1000))
               printf("%d\r", iCnt);
            iCnt++;

            if (bMultiByte)
            {
               for (iRet = 0, iTmp = 0; acRec[iRet] != 13; iRet++)
               {
                  if (acRec[iRet])
                     acRec[iTmp++] = acRec[iRet];
               }
               acRec[iTmp] = 0;
            }

            // Parse input string
            iRet = ParseString(acRec, ',', 12, apTokens);
            if (iRet < iMaxFlds)
            {
               LogMsg("*** Bad input record number %d (%d)", iCnt, iRet);
            } else
            {
               // Skip to next record
               if (strcmp(sDocNum, apTokens[iDocNum]))
               {
                  strcpy(sDocNum, apTokens[iDocNum]);

#ifdef _TESTING_
                  // Find doc in table
                  if (findDoc(sDocNum))
                  {
                     // Update record
                     pTmp = dateConversion(apTokens[iDocDate], sDocDate, iDateType);
                     if (pTmp)
                     {
                        sprintf(acTmp, "UPDATE %s SET DocDate=%s WHERE DocNum='%s'", acGrGrTbl, sDocDate, sDocNum);
                        iRet = execCmdEx(acTmp, &dbLoadConn);
                     }
                  }
#else
                  // Update record
                  pTmp = dateConversion(apTokens[iDocDate], sDocDate, iDateType);
                  if (pTmp)
                  {
                     sprintf(acTmp, "UPDATE %s SET DocDate=%s WHERE DocNum='%s'", acGrGrTbl, sDocDate, sDocNum);
                     iRet = execCmdEx(acTmp, &dbLoadConn);
                  }
               }
#endif         
            }
            pTmp = fgets(acRec, 1024, fdGrGr);
         } while (!feof(fdGrGr));

         fclose(fdGrGr);

         // Move input file to backup folder
         //sprintf(acTmp, "%s\\%s", acGrGrBak, sFileInfo.name);
         //rename(acCsvFile, acTmp);

         // Find next file
         iRet = _findnext(lHandle, &sFileInfo);
      } else
         LogMsg("***** Error: missing input file %s", acCsvFile);
   }
   _findclose(lHandle);

   */
   LogMsg("Total files processed  : %u", iCnt);
   LogMsg("Total records processed: %u", lCnt);

   return lCnt;
}

/******************************** Lax_LoadGrGr *******************************
 *
 * County provides us data in pair of files, yyyymmdd.data and yyyymmdd.txt.
 * The data file contains all info except APN and the txt file contain DocNum,
 * DocDate, and APN.  We need both to get a complete record.
 *
 * Merging county data into GRGR_DOC record.  This data later can be merge
 * into roll file.
 *
 * Creating output file only if GrGr input is available.
 *
 * Return 0 if successful, -1 if unable to create output file, 1 if no input file.
 *
 *****************************************************************************

int Lax_LoadGrGr()
{
   char     *pTmp, acRec[512];
   char     acTmp[_MAX_PATH];
   char     acGrGrIn[_MAX_PATH], acGrGrOut[_MAX_PATH], acGrGrBak[_MAX_PATH];
   char     acDataFile[_MAX_PATH], acTxtFile[_MAX_PATH];
   struct   _finddata_t  sFileInfo;
   char     *pCnty = "LAX";

   FILE        *fdIn, *fdOut, *fdApn;
   LAX_DOC     *pDoc;
   LAX_NAME    *pName;
   LAX_TAX     *pTax;
   GRGR_DOC    curGrGrRec;
   FILE_ITEM   asFileList[64];

   int      iRet, iTmp, iCnt, iMatched=0;
   int      iGrantors, iGrantees;
   BOOL     bEof=false;
   long     lCnt, lHandle, lSalePrice, lTax;

   // Get raw file name
   GetPrivateProfileString(pCnty, "GrGrIn", "", acGrGrIn, _MAX_PATH, acIniFile);
   strcpy(acGrGrBak, acGrGrIn);
   pTmp = strrchr(acGrGrBak, '\\');
   if (pTmp)
      *++pTmp = 0;
   dateString(acRec, 0);
   sprintf(acTmp, "GrGr_%s", acRec);
   strcat(acGrGrBak, acTmp);

   // Open Input file
   lHandle = _findfirst(acGrGrIn, &sFileInfo);
   if (lHandle > 0)
   {
      pTmp = strrchr(acGrGrIn, '\\');
      if (pTmp)
         *pTmp = 0;
      iRet = 0;

      // Create Output file
      GetPrivateProfileString(pCnty, "GrGrOut", "", acTmp, _MAX_PATH, acIniFile);
      sprintf(acGrGrOut, acTmp, pCnty, "TMP");

      LogMsg("Open output file %s", acGrGrOut);
      fdOut = fopen(acGrGrOut, "w");
      if (fdOut == NULL)
      {
         LogMsg("***** Error creating GrGr output file: %s\n", acGrGrOut);
         return -1;
      }
   } else
   {
      LogMsg("*** GrGr file not available: %s", acGrGrIn);
      return 1;
   }

   // Create backup folder
   if (_access(acGrGrBak, 0))
      mkdir(acGrGrBak);

   lCnt=lApnSkip=lApnMatch = 0;
   iCnt = 0;
   while (!iRet)
   {
      // Open .data file
      sprintf(acDataFile, "%s\\%s", acGrGrIn, sFileInfo.name);
      if (strstr(acDataFile, ".zip"))
      {
         // unzip file
         LogMsg("Unzip input file %s", acDataFile);

         if (!m_bZipLoaded)
         {
            // Initialize Zip server
            doZipInit();

            // Set unzip folder
            GetPrivateProfileString(myCounty.acCntyCode, "UnzipFolder", acTmpPath, acTmp, _MAX_PATH, acIniFile);
            setUnzipToFolder(acTmp);
            setReplaceIfExist(true);

            // get file name
            iCnt = getZipFileContents(acDataFile, &asFileList[0], 64);
         }

         iRet = startUnzip(acDataFile);
         if (iRet)
         {
            LogMsg("***** Error unzipping %s.  Program terminated", acDataFile);
            break;
         }

         if (iCnt > 0)
            sprintf(acDataFile, "%s\\%s", acTmp, asFileList[0].szName);
      }

      LogMsg("Open input file %s", acDataFile);
      fdIn = fopen(acDataFile, "r");

      // Update GrGr file date
      iTmp = getFileDate(acDataFile);
      if (iTmp > lLastGrGrDate)
         lLastGrGrDate = iTmp;

      // Open .txt file for APN
      strcpy(acTxtFile, acDataFile);
      pTmp = strrchr(acTxtFile, '.');
      strcpy(pTmp, ".txt");
      if (!(fdApn = fopen(acTxtFile, "r")))
      {
         LogMsg("*** Missing file : %s.  Skip processing.", acTxtFile);
         if (fdIn)
            fclose(fdIn);

         // Find next file
         iRet = _findnext(lHandle, &sFileInfo);
         continue;
      }

      // New file
      memset((void *)&curGrGrRec, ' ', sizeof(GRGR_DOC));
      lTax = 0;

      // Skip header record
      pTmp = fgets(acRec, 512, fdIn);
      while (!feof(fdIn))
      {
         pTmp = fgets(acRec, 512, fdIn);
         if (!pTmp) break;

         pName = (LAX_NAME *)pTmp;

         switch (pName->RecordType)
         {
            case '0':
               // Output current record - if doc tax avail.
               if (curGrGrRec.DocNum[0] > ' ' && lTax > 0)
               {
                  // Tax Amt
                  sprintf(acTmp, "%*u", SIZ_GR_SALE, lTax);
                  memcpy(curGrGrRec.DocTax, acTmp, SIZ_GR_SALE);

                  // Sale price
                  lSalePrice = (long)(lTax / 0.11);
                  sprintf(acTmp, "%*u", SIZ_GR_SALE, lSalePrice);
                  memcpy(curGrGrRec.SalePrice, acTmp, SIZ_GR_SALE);

                  // Owner counts
                  sprintf(acTmp, "%d    ", iGrantees);
                  memcpy(curGrGrRec.NameCnt, acTmp, SIZ_GR_NAMECNT);

                  curGrGrRec.CRLF[0] = '\n';
                  curGrGrRec.CRLF[1] = 0;
                  fputs((char *)&curGrGrRec, fdOut);
                  iMatched++;
               }

               // Initialize new record
               memset((void *)&curGrGrRec, ' ', sizeof(GRGR_DOC));
               lTax = 0;
               lCnt++;
               pDoc = (LAX_DOC *)&acRec;

               // Start a new record - taking Deed (Grant Deed or Trustees Deed) only
               if (pDoc->DocType[0] == 'D' || pDoc->DocType[0] == 'T')
               {
                  if (Lax_FindApn(acTmp, pDoc->DocNum, fdApn))
                  {
                     memcpy(curGrGrRec.APN, acTmp, RSIZ_APN);
                     memcpy(curGrGrRec.DocDate, pDoc->RecDate, GSIZ_RECDATE);
                     memcpy(curGrGrRec.DocNum, pDoc->DocNum, GSIZ_DOCNUM);
                     if (!memcmp(pDoc->DocTitle, "DECLARATION TAKING", 18))
                        memcpy(curGrGrRec.DocTitle, "GRANT DEED", 11);
                     else
                        memcpy(curGrGrRec.DocTitle, pDoc->DocTitle, GSIZ_DOCTITLE);

                     iGrantors = iGrantees = 0;
                  } else
                  {
                     if (bDebug)
                        LogMsg0("* No APN found for Doc# %.7s", pDoc->DocNum);
                  }
               }
               break;

            case '1':
               // Multiple title - ignore
               break;

            case '2':
               // Merge Grantor/Grantee
               if (pName->NameType == 'O')
               {
                  if (iGrantors < 2)
                  {  // Since SIZ_GR_NAME < len(pName->Name) we do straight copy
                     // No need to check for length here
                     memcpy(curGrGrRec.Grantor[iGrantors], pName->Name, SIZ_GR_NAME);
                  }
                  iGrantors++;
               } else
               {
                  if (iGrantees < 2)
                     memcpy(curGrGrRec.Grantee[iGrantees], pName->Name, SIZ_GR_NAME);
                  iGrantees++;
               }
               break;

            case '3':
               // Merge APN
               pTax = (LAX_TAX *)&acRec;

               if (!memcmp(curGrGrRec.DocNum, pTax->DocNum, GSIZ_DOCNUM))
               {
                  // Tax - When there are multiple tax amt, use the one with city code 80 (unincorporated)
                  if (lTax == 0 || !memcmp(pTax->CityCode, "80", 2))
                     lTax = atoin(pTax->TaxAmt, GSIZ_TAXAMT);
               } else
               {
                  if (curGrGrRec.DocNum[0] != ' ')
                     LogMsg0("** Unmatched Doc#=%.7s <> %.7s", curGrGrRec.DocNum, pTax->DocNum);
               }

               break;
         }
         if (!(lCnt % 1000))
            printf("\r%d", lCnt);
      }

      // Output last record - if doc tax avail.
      if (curGrGrRec.DocNum[0] > ' ' && lTax > 0)
      {
         // Tax Amt
         sprintf(acTmp, "%*u", SIZ_GR_SALE, lTax);
         memcpy(curGrGrRec.DocTax, acTmp, SIZ_GR_SALE);

         // Sale price
         lSalePrice = (long)(lTax / 0.11);
         sprintf(acTmp, "%*u", SIZ_GR_SALE, lSalePrice);
         memcpy(curGrGrRec.SalePrice, acTmp, SIZ_GR_SALE);

         // Owner counts
         sprintf(acTmp, "%d    ", iGrantees);
         memcpy(curGrGrRec.NameCnt, acTmp, SIZ_GR_NAMECNT);

         curGrGrRec.CRLF[0] = '\n';
         curGrGrRec.CRLF[1] = 0;
         fputs((char *)&curGrGrRec, fdOut);
         iMatched++;
      }


      LogMsg("Number of matched records: %d", iMatched);
      LogMsg("Total records so far     : %d", lCnt);

      fclose(fdIn);
      if (fdApn)
         fclose(fdApn);

      // Move files
      sprintf(acTmp, "%s\\%s", acGrGrBak, sFileInfo.name);
      rename(acDataFile, acTmp);
      pTmp = strrchr(acTmp, '.');
      strcpy(pTmp, ".txt");
      rename(acTxtFile, acTmp);

      // Find next file
      iRet = _findnext(lHandle, &sFileInfo);
   }

   // Close handle
   _findclose(lHandle);

   // Close files
   if (fdOut)
      fclose(fdOut);

   // Unload Zip DLL
   doZipShutdown();

   LogMsg("Total matched records    : %d", iMatched);
   LogMsg("Total Docs processed     : %u", lCnt);
   LogMsg("Total Doc with APN       : %d", lApnMatch);
   LogMsg("Total APN rec skipped    : %d", lApnSkip);

   // Sort output file and dedup - order APN, DocDate, DocNum
   strcpy(acTmp,"S(17,16,C,A,37,8,C,A,1,12,C,A) F(TXT) DUPO(1,44) ");
   strcpy(acGrGrIn, acGrGrOut);
   pTmp = strrchr(acGrGrOut, '.');
   strcpy(pTmp, ".DAT");
   lCnt = sortFile(acGrGrIn, acGrGrOut, acTmp);

   // Update cumulated sale file
   if (lCnt > 0)
   {
      pTmp = strrchr(acGrGrIn, '.');
      strcpy(pTmp, ".sls");         // accummulated sale file
      iTmp = appendTxtFile(acGrGrOut, acGrGrIn);

      // Sort sls file
      lCnt = sortFile(acGrGrIn, acGrGrOut, acTmp);

   }

   LogMsg("Total output records after dedup: %u", lCnt);
   printf("\nTotal output records after dedup: %u\n", lCnt);
   return 0;
}

/*********************************** Lax_FixTRA *****************************
 *
 * Use -Ft
 *
 ****************************************************************************/

int Lax_FixTRA(int iSkip)
{
   char     acBuf[MAX_RECSIZE], acTmp[_MAX_PATH];
   char     cFileCnt=1;
   char     acRawFile[_MAX_PATH], acOutFile[_MAX_PATH];

   HANDLE   fhIn, fhOut;

   int      iRet, iRollUpd=0;
   DWORD    nBytesRead;
   DWORD    nBytesWritten;
   BOOL     bRet;
   long     lRet=0, lCnt=0;

   sprintf(acRawFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "R01");
   sprintf(acOutFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "T01");

   // Rename files for processing
   if (_access(acRawFile, 0))
   {
      sprintf(acRawFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "S01");
      if (_access(acRawFile, 0))
      {
         LogMsg("***** Missing input file %s.  Please recheck!", acRawFile);
         return -1;
      }
   }

   // Open Input file
   LogMsg("Open input file %s", acRawFile);
   fhIn = CreateFile(acRawFile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhIn == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error open input file: %s", acRawFile);
      return -4;
   }

   // Open Output file - use tmp file so we can sort at the end to R01 file
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error open output file: %s", acOutFile);
      return -4;
   }

   // Copy skip record
   memset(acBuf, ' ', iRecLen);
   while (iSkip-- > 0)
   {
      ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);
      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
   }

   // Merge loop
   while (bRet)
   {
      bRet = ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);

      // Check for EOF
      if (!bRet)
      {
         LogMsg("***** Error reading input file %s (%f)", acRawFile, GetLastError());
         break;
      }

      // EOF ?
      if (!nBytesRead)
      {
         // EOF
         cFileCnt++;
         acRawFile[strlen(acRawFile)-1] = cFileCnt | 0x30;
         acOutFile[strlen(acOutFile)-1] = cFileCnt | 0x30;
         if (_access(acRawFile, 0))
         {
            if (!_access(acOutFile, 0))
               rename(acOutFile, acRawFile);
            else
               break;
         }

         CloseHandle(fhIn);
         CloseHandle(fhOut);
         fhOut = 0;
         fhIn = 0;

         // Open next Input file
         LogMsg("Open input file %s", acRawFile);
         fhIn = CreateFile(acRawFile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
               FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

         if (fhIn == INVALID_HANDLE_VALUE)
            break;
         bRet = ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);

         // Open Output file
         LogMsg("Open output file %s", acOutFile);
         fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
               FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);
         if (fhOut == INVALID_HANDLE_VALUE)
            break;
      }

      // Get last recording date
      iRet = atoin((char *)&acBuf[OFF_TRA], SIZ_TRA);
      if (iRet > 0)
      {
         sprintf(acTmp, "SELECT * FROM Lax_TraRef WHERE Xref=%d", iRet);
         try
         {
            rsTra.Open(dbTra, acTmp);

            CString sTra;
            if (rsTra.next())
            {
               sTra = rsTra.GetItem("TRA");
               memcpy((char *)&acBuf[OFF_TRA], sTra, sTra.GetLength());
               rsTra.Close();
               iRollUpd++;
            }
         } AdoCatch(e)
         {
            LogMsg("***** Error searching %s (%s)", acTmp, ComError(e));
            return -1;
         }
      }                       

      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);

      if (!bRet)
      {
         LogMsg("***** Error occurs: %d\n", GetLastError());
         lRet = WRITE_ERR;
         break;
      }
   }

   // Close files
   if (fhOut)
      CloseHandle(fhOut);
   if (fhIn)
      CloseHandle(fhIn);

   // Rename output files
   sprintf(acTmp, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "tmp");
   for (cFileCnt = 1; cFileCnt < 4; cFileCnt++)
   {
      acRawFile[strlen(acRawFile)-1] = cFileCnt | 0x30;
      acOutFile[strlen(acOutFile)-1] = cFileCnt | 0x30;
      rename(acRawFile, acTmp);
      rename(acOutFile, acRawFile);
      rename(acTmp, acOutFile);
   }

   LogMsgD("\nTotal updated records:      %u", iRollUpd);
   LogMsgD("Total output records:       %u", lCnt);

   return lRet;
}

/******************************* Lax_UpdateSaleHist **************************
 *
 * Update history sale.  This function adds new sale records to raw cumulative
 * sale file using LAX_SALE format.
 *
 * Return 0 if successful, >0 if error
 *
 *****************************************************************************

int Lax_UpdateSaleHist(void)
{
   LAX_SALE *pCurSale, *pHistSale;
   char     *pTmp, acHSaleRec[512], acCSaleRec[512];
   char     acSaleCur[_MAX_PATH], acSaleCum[_MAX_PATH], acOutFile[_MAX_PATH];

   HANDLE   fhIn, fhOut;

   int      iRet, iTmp, iUpdateSale=0, iNewSale=0, iCurSaleLen, iCumSaleLen;
   DWORD    nBytesRead;
   DWORD    nBytesWritten;
   BOOL     bRet, bEof;
   long     lCnt=0;

   GetPrivateProfileString(myCounty.acCntyCode, "SaleCur", "", acSaleCur, _MAX_PATH, acIniFile);
   GetPrivateProfileString(myCounty.acCntyCode, "SaleCum", "", acSaleCum, _MAX_PATH, acIniFile);
   iCurSaleLen = GetPrivateProfileInt(myCounty.acCntyCode, "CurSaleLen", 240, acIniFile);
   iCumSaleLen = GetPrivateProfileInt(myCounty.acCntyCode, "CumSaleLen", 240, acIniFile);

   if (_access(acSaleCur, 0))
   {
      LogMsg("*** Missing current sale file %s", acSaleCur);
      return -1;
   }
   if (_access(acSaleCum, 0))
   {
      LogMsg("*** Missing cumulative file %s.  Use current sale file as is.", acSaleCum);
      iTmp = doCopy(acSaleCur, acSaleCum);
      return iTmp;
   }

   // Create tmp output file
   strcpy(acOutFile, acSaleCum);
   if (pTmp = strrchr(acOutFile, '.'))
      strcpy(pTmp, ".tmp");
   else
      strcat(acOutFile, ".tmp");

   // Open current sale
   LogMsg("Open current sale file %s", acSaleCur);
   fdCSale = fopen(acSaleCur, "rb");
   if (fdCSale == NULL)
   {
      LogMsg("***** Error opening current sale file: %s\n", acSaleCur);
      return 2;
   }

   // Open Input file
   LogMsg("Open cumulative sale file %s", acSaleCum);
   fhIn = CreateFile(acSaleCum, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhIn == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error open current sale file: %s", acSaleCum);
      return 3;
   }

   // Open Output file
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error open output file: %s", acOutFile);
      return 4;
   }

   // Read first history sale
   bRet = ReadFile(fhIn, acHSaleRec, iCumSaleLen, &nBytesRead, NULL);

   pCurSale  = (LAX_SALE *)&acCSaleRec[0];
   pHistSale = (LAX_SALE *)&acHSaleRec[0];
   bEof = false;

   // Merge loop
   while (!bEof)
   {
      // Get current sale
      iRet = fread((char *)&acCSaleRec[0], 1, iCurSaleLen, fdCSale);
      if (iRet != iCurSaleLen)
         break;

      // Ignore bad APN
      if (!memcmp(pCurSale->Apn, "0000", 4))
         continue;

      NextSaleRec:
      iTmp = memcmp(acCSaleRec, acHSaleRec, iApnLen);
      if (!iTmp)
      {
         // If current sale is newer, add to out file
         if (memcmp(pCurSale->BuyerRecDate, pHistSale->BuyerRecDate, SSIZ_RECDATE) > 0)
         {
            bRet = WriteFile(fhOut, acCSaleRec, iCumSaleLen, &nBytesWritten, NULL);
            iUpdateSale++;
         }

         // Now output the rest cum sale rec to out file
         while (!iTmp)
         {
            bRet = WriteFile(fhOut, acHSaleRec, iCumSaleLen, &nBytesWritten, NULL);
            bRet = ReadFile(fhIn, acHSaleRec, iCumSaleLen, &nBytesRead, NULL);
            if (!bRet || !nBytesRead)
            {
               bEof = true;
               break;
            }
            iTmp = memcmp(acCSaleRec, acHSaleRec, iApnLen);
         }
      } else if (iTmp > 0)
      {
         // Write out all history sale record for current APN
         while (iTmp > 0)
         {
            bRet = WriteFile(fhOut, acHSaleRec, iCumSaleLen, &nBytesWritten, NULL);
            bRet = ReadFile(fhIn, acHSaleRec, iCumSaleLen, &nBytesRead, NULL);
            if (!bRet || !nBytesRead)
            {
               bEof = true;
               break;
            }
            iTmp = memcmp(acCSaleRec, acHSaleRec, iApnLen);
         }

         goto NextSaleRec;
      } else // CurApn < HistApn
      {
         // New sale record for this APN
         bRet = WriteFile(fhOut, acCSaleRec, iCumSaleLen, &nBytesWritten, NULL);
         iNewSale++;
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);

      if (!bRet)
      {
         LogMsg("***** Error occurs: %d\n", GetLastError());
         break;
      }
   }

   // Do the rest of the file
   while (nBytesRead > 0)
   {
      bRet = WriteFile(fhOut, acHSaleRec, iCumSaleLen, &nBytesWritten, NULL);
      bRet = ReadFile(fhIn, acHSaleRec, iCumSaleLen, &nBytesRead, NULL);

      if (!bRet)
         break;
   }

   // Close files
   if (fdCSale)
      fclose(fdCSale);
   if (fhOut)
      CloseHandle(fhOut);
   if (fhIn)
      CloseHandle(fhIn);

   // Rename output file
   strcpy(acSaleCur, acSaleCum);
   if (pTmp = strrchr(acSaleCur, '.'))
   {
      strcpy(pTmp, ".sav");
      if (!_access(acSaleCur, 0))
         remove(acSaleCur);
   }
   iRet = rename(acSaleCum, acSaleCur);
   if (iRet)
   {
      LogMsg("***** Error renaming %s to %s: %d", acSaleCum, acSaleCur, errno);
   } else
   {
      // Sort sale file
      char  acTmp[256];

      // Sort output file and dedup - order APN, DocDate, SaleAmt
      // Here we don't sort on DocNum since old xfer file is not reliable.
      sprintf(acTmp,"S(1,10,C,A,136,8,C,A,146,9,C,D) F(FIX,%d) OMIT(1,4,C,EQ,\"0000\") DUPO(1,10,136,8,146,9) ", iCumSaleLen);
      //sprintf(acTmp,"S(1,10,C,A,136,8,C,D,146,9,C,D) F(FIX,%d)  ", iCumSaleLen);
      lCnt = sortFile(acOutFile, acSaleCum, acTmp);
      if (!lCnt)
         LogMsg("***** Error merging sale history file.");
   }

   LogMsg("Total sale records processed:   %u", lCnt);
   LogMsg("Total new sale records:         %u", iNewSale);
   LogMsg("Total sale records updated:     %u", iUpdateSale);

   LogMsg("Update Sale History completed.\n");
   printf("\nUpdate Sale History completed.\n");

   return lCnt;
}

/**************************** Lax_FormatSaleRec ******************************
 *
 * Update history sale.  This function adds new sale record to cumulative file.
 *
 *
 * Return 0 if successful, otherwise error
 *
 *****************************************************************************/

int Lax_FormatSaleRec(char *pFmtSale, char *pSaleRec)
{
   LAX_SALE  *pSale = (LAX_SALE *)pSaleRec;
   SCSAL_REC *pCSal = (SCSAL_REC *)pFmtSale;
   long      lCurSaleDt, lTmp, lDocNum;
   char      acTmp[32];

   // Inititalize
   memset(pFmtSale, ' ', sizeof(SCSAL_REC));

   // APN
   lTmp = atoin(pSale->Apn, 4);
   if (!lTmp || lTmp == 9999)
      return -1;

   lCurSaleDt = atoin(pSale->SellerRecDate, 8);
   if (!lCurSaleDt)
      lCurSaleDt = atoin(pSale->BuyerRecDate, 8);

   if (lCurSaleDt < 19000101 || lCurSaleDt > lToday)
      return -1;

   lDocNum = atoin(pSale->RecDocNo, SSIZ_RECDOC_NO);
   if (!lDocNum)
      return -1;

#ifdef _DEBUG
   //if (!memcmp(pSale->Apn, "052382005", iApnLen))
   //   acTmp[0] = 0;
#endif
   // Assessment
   memcpy(pCSal->Apn, pSale->Apn, SSIZ_APN);

   // DocNum - format DocNum with zero prefix
   lTmp = atoin(pSale->RecDocNo, SSIZ_RECDOC_NO);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%.*u", SSIZ_RECDOC_NO, lTmp);
      memcpy(pCSal->DocNum, acTmp, SSIZ_RECDOC_NO);
   }

   // DocDate
   lTmp = sprintf(acTmp, "%d", lCurSaleDt);
   memcpy(pCSal->DocDate, acTmp, lTmp);

   // Sale price
   long lPrice = atoin(pSale->DttAmt, SSIZ_DOCTAX_AMT);
   if (lPrice > 0)
   {
      lTmp = sprintf(acTmp, "%*u", SALE_SIZ_SALEPRICE, lPrice);
      acTmp[lTmp-1] = '0';                   // Remove check digit from county
      memcpy(pCSal->SalePrice, acTmp, SALE_SIZ_SALEPRICE);
      pCSal->DocType[0] = '1';

      if (pSale->DttType > '1')
      {
         pCSal->SaleCode[0] = 'P';
         pCSal->MultiSale_Flg = 'Y';
      } else if (pSale->DocType == 'L')
         pCSal->SaleCode[0] = 'N';
      else
         pCSal->SaleCode[0] = 'F';
   }

   // Doc type - other than DEED
   pCSal->NoneSale_Flg = 'Y';
   switch (pSale->DocType)
   {
      case 'Y':   // Normal sale
      case 'L':   // Less lien
      case 'P':   // Probate sale
         pCSal->DocType[0] = '1';               
         pCSal->NoneSale_Flg = 'N';
         break;
      case 'A':   // Excluded transfer - No DTT
         memcpy(pCSal->DocType, "75", 2);       
         break;
      case 'C':   // Affidavit of Death of spouse
         pCSal->DocType[0] = '6';               
         break;
      case 'F':   // Foreclosure
         memcpy(pCSal->DocType, "77", 2);       
         break;
      default:
         memcpy(pCSal->DocType, "74", 2);       
         break;
   }

   // Buyers
   if (pSale->CurOwner[0] > ' ')
      memcpy(pCSal->Name1, pSale->CurOwner, SSIZ_BUYER);
   else
      memcpy(pCSal->Name1, pSale->Transferee, SSIZ_SELLER);

   // Sale flag 
   
   pCSal->CRLF[0] = '\n';
   pCSal->CRLF[1] = 0;
   return 0;
}

/**************************** Lax_UpdateSaleHist ******************************
 *
 * Update history sale.  This function adds new sale record to cumulative file.
 *
 *
 * Return 0 if successful, otherwise error
 *
 *****************************************************************************/

int Lax_UpdateSaleHist(void)
{
   SCSAL_REC *pCSale;

   char     *pTmp, acCSalRec[1024], acSaleRec[1024], acTmp[256];
   char     acOutFile[_MAX_PATH], acLatestSale[16];
   int      iRet, iTmp, iUpdateSale=0, iNewSale=0, iSkipSale=0;
   BOOL     bRet;
   long     lCnt=0;

   // Check current sale file
   if (_access(acSaleFile, 0))
   {
      LogMsg("***** Missing current sale file %s", acSaleFile);
      return -1;
   }

   // Open current sale
   LogMsg("Open current sale file %s", acSaleFile);
   fdSale = fopen(acSaleFile, "r");
   if (fdSale == NULL)
   {
      LogMsg("***** Error opening current sale file: %s\n", acSaleFile);
      return 2;
   }

   // Create output file
   sprintf(acOutFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "TMP");
   LogMsg("Create updated sale file %s", acOutFile);
   fdCSale = fopen(acOutFile, "w");
   if (fdCSale == NULL)
   {
      LogMsg("***** Error creating updated sale file: %s\n", acOutFile);
      return 2;
   }

   acLatestSale[0] =0;
   pCSale = (SCSAL_REC *)&acCSalRec[0];

   // Merge loop
   while (!feof(fdSale))
   {
      // Get current sale
      pTmp = fgets(acSaleRec, 1024, fdSale);
      if (!pTmp)
         break;

      // Skip bad records
      if (*pTmp < '2' || *pTmp > '9')
      {
         iSkipSale++;
         continue;
      }

      // Format cumsale record
      iRet = Lax_FormatSaleRec(acCSalRec, acSaleRec);
      if (!iRet)
      {
         // Output current sale record
         fputs(acCSalRec, fdCSale);
         iUpdateSale++;
         if (memcmp(acLatestSale, pCSale->DocDate, 8) < 0)
            memcpy(acLatestSale, pCSale->DocDate, 8);
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdCSale)
      fclose(fdCSale);
   if (fdSale)
      fclose(fdSale);

   // Set Multi-parcel sale flag
   iRet = SetMultiSale(acOutFile, NULL, myCounty.acCntyCode);

   char  acHistFile[256], acTmpFile[256];
   sprintf(acHistFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "SLS");
   sprintf(acTmpFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "DAT");
   strcat(acOutFile, "+");
   strcat(acOutFile, acHistFile);

   // Sort sale file - APN (asc), Saledate (asc), DocNum (asc)
   // S(1,9,C,A,28,8,C,A,21,7,C,A) DUPOUT(1,9,21,15)
   sprintf(acTmp, "S(1,%d,C,A,27,8,C,A,15,7,C,A) DUPOUT(1,34,57,10)", iApnLen);
   iTmp = sortFile(acOutFile, acTmpFile, acTmp, &iRet, acWorkDrive);

   // Rename old history file to SL1
   sprintf(acOutFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "SL1");
   if (!_access(acOutFile, 0))
   {
      LogMsg("Removing %s", acOutFile);
      bRet = DeleteFile(acOutFile);
      if (!bRet)
         LogMsg("***** Error deleting %s", acOutFile);
   }

   LogMsg("Renaming %s to %s", acHistFile, acOutFile);
   bRet = MoveFile(acHistFile, acOutFile);
   if (!bRet)
      LogMsg("***** Error renaming %s to %s", acHistFile, acOutFile);

   // Copy history file
   LogMsg("Copying %s to %s", acTmpFile, acHistFile);
   bRet = CopyFile(acTmpFile, acHistFile, false);
   if (!bRet)
      LogMsg("***** Error copying %s to %s", acTmpFile, acHistFile);

   LogMsg("Total sale records processed:   %u", lCnt);
   LogMsg("Total sale records updated:     %u", iUpdateSale);
   LogMsg("Total sale records skipped:     %u", iSkipSale);
   LogMsg("Total cumulative sale records:  %u", iTmp);

   acLatestSale[8] = 0;
   LogMsgD("Update Sale History completed.  Latest sale is %s", acLatestSale);

   return 0;
}

/******************************* Lax_ConvertSale ****************************
 *
 * Convert LAX_SALE format to standard cummulative sale 512-bytes format
 * pOutfile is used when bConvertOnly is true.
 *
 ****************************************************************************/

int Lax_ConvertSale(char *pCnty, char *pInfile, char *pOutfile, bool bConvertOnly=true)
{
   char     acInbuf[1024], acOutbuf[1024];
   char     acTmpFile[_MAX_PATH], acTmp[256];
   long     lCnt=0, lOut=0;
   int      iRet;
   FILE     *fdOut;

   LAX_SALE  *pSale  = (LAX_SALE *)&acInbuf[0];
   SCSAL_REC *pCSale = (SCSAL_REC *)&acOutbuf[0];

   LogMsg0("Load sale file %s", pInfile);
   if (_access(pInfile, 0))
   {
      LogMsg("***** Lax_ConvertSale(): Missing input file: %s", pInfile);
      return -1;
   }

   // Open input file
   LogMsg("Open input sale file %s", pInfile);
   fdSale = fopen(pInfile, "rb");
   if (fdSale == NULL)
   {
      LogMsg("***** Error opening sale file: %s\n", pInfile);
      return -2;
   }

   // Open output file
   sprintf(acTmpFile, acESalTmpl, pCnty, pCnty, "Tmp");
   LogMsg("Create output sale file %s", acTmpFile);
   fdOut = fopen(acTmpFile, "w");
   if (fdOut == NULL)
   {
      LogMsg("***** Error creating output sale file: %s\n", acTmpFile);
      return -3;
   }

   // Convert loop
   while (!feof(fdSale))
   {
      if (iSaleLen != fread(acInbuf, 1, iSaleLen, fdSale))
         break;

      // Format cumsale record
      iRet = Lax_FormatSaleRec(acOutbuf, acInbuf);
      if (!iRet)
      {
         // Output current sale record
         fputs(acOutbuf, fdOut);
         lOut++;
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdSale)
      fclose(fdSale);
   if (fdOut)
      fclose(fdOut);

   // Set Multi-parcel sale flag
   iRet = SetMultiSale(acTmpFile, NULL, pCnty);

   // Sort sale file - APN (asc), Saledate (asc), DocNum (asc), Sale price (desc)
   if (bConvertOnly)
   {
      sprintf(acInbuf, "S(1,%d,C,A,27,8,C,A,15,12,C,A,57,10,C,D) DUPO(1,34)", SSIZ_APN);
      lOut = sortFile(acTmpFile, pOutfile, acInbuf, &iRet, acWorkDrive);
      if (lOut > 0)
         iRet = 0;
   } else
   {
      // Append output file to CSalFile
      sprintf(acOutbuf, "%s+%s", acTmpFile, acCSalFile);
      sprintf(acTmpFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "DAT");

      // Sort sale file - APN (asc), Saledate (asc), DocNum (asc)
      sprintf(acTmp, "S(1,%d,C,A,27,8,C,A,15,12,C,A,57,10,C,D) DUPOUT(1,34)", SSIZ_APN);
      lOut = sortFile(acOutbuf, acTmpFile, acTmp, &iRet, acWorkDrive);
      if (lOut > 0)
      {
         // Rename old history file to SL1
         sprintf(acOutbuf, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "SL1");
         if (!_access(acOutbuf, 0))
         {
            LogMsg("Removing %s", acOutbuf);
            if (!DeleteFile(acOutbuf))
               LogMsg("***** Error deleting %s", acOutbuf);
         }

         LogMsg("Renaming %s to %s", acCSalFile, acOutbuf);
         if (!MoveFile(acCSalFile, acOutbuf))
            LogMsg("***** Error renaming %s to %s", acCSalFile, acOutbuf);
         else
         {
            // Rename new history file
            LogMsg("Renaming %s to %s", acTmpFile, acCSalFile);
            if (!MoveFile(acTmpFile, acCSalFile))
               LogMsg("***** Error renaming %s to %s", acTmpFile, acCSalFile);
         }
         iRet = 0;
      }
   }

   LogMsg("Total input records:  %u", lCnt);
   LogMsg("Total output records: %u\n", lOut);
   return iRet;
}

/*****************************************************************************
 *
 * Design for LAX
 *
 * Return number of tokens
 *
 *****************************************************************************/

int parseSAdr1(ADR_REC *psAdr, char *pAddr, char *pApn)
{
   char  acBuf[256], acAdr[256], acStr[256], acTmp[256], acSfx[64], acUnit[64], *apItems[16], acCity[64];
   char *pAdr, *pTmp, *pTmp1, *pSave, acSave[256];
   int   iCnt, iTmp, iIdx, iDir, iItems, iLastIdx, iStrNo;
   bool  bRet, bDir;
   ADR_REC *pAdrRec = (ADR_REC *)&acBuf[0];

   pAdr = strcpy(acAdr, pAddr);
   quoteRem(acAdr);
   acStr[0] = 0;
   acUnit[0] = 0;
   acSave[0] = 0;
   acCity[0] = 0;

#ifdef _DEBUG
   //if (!memcmp(pApn, "8940333023", 10))
   //   iTmp = 0;
#endif

   if (pSave = strstr(&acAdr[8], " IN "))
      *pSave++ = 0;
   //else if (pSave = strstr(&acAdr[8], " AT "))
   //   *pSave++ = 0;
   else
      pSave = NULL;

   // Drop unused word
   if (pTmp = strstr(acAdr, " LEASE"))
      *pTmp = 0;

   memset(acBuf, 0, sizeof(ADR_REC));
   if (pTmp = strstr(acAdr, " SUITE "))
   {
      if (pTmp1 = strchr(pTmp+7, ' '))
         *pTmp1 = 0;
      if (*(pTmp+7) != '#')
         sprintf(acUnit, "#%s", pTmp+7);
      else
         strcpy(acUnit, pTmp+7);
      *pTmp = 0;
   } else
   if (pTmp = strstr(acAdr, " SUITES "))
   {
      if (pTmp1 = strchr(pTmp+8, ' '))
         *pTmp1 = 0;
      if (*(pTmp+8) != '#')
         sprintf(acUnit, "#%s", pTmp+8);
      else
         strcpy(acUnit, pTmp+8);
      *pTmp = 0;
   } else
   if (pTmp = strstr(acAdr, " STE "))
   {
      if (pTmp1 = strchr(pTmp+5, ' '))
         *pTmp1 = 0;
      if (*(pTmp+5) != '#')
         sprintf(acUnit, "#%s", pTmp+5);
      else
         strcpy(acUnit, pTmp+5);
      *pTmp = 0;
      // 1800 CENTURY PARK E STE 210
      //if ((pTmp = strrchr(acAdr, ' ')) && strlen(pTmp) < 3)
      //   *pTmp = 0;
   } else
   if (pTmp = strstr(acAdr, " SP "))
   {
      if (pTmp1 = strchr(pTmp+4, ' '))
         *pTmp1 = 0;
      strcpy(acUnit, pTmp+1);
      *pTmp = 0;
   } else
   if (pTmp = strchr(acAdr, '#'))
   {
      if (pTmp1 = strstr(pTmp, "-S/"))
         *pTmp1 = 0;
      if (*(pTmp+1) == ' ')
      {
         *pTmp++ = 0;
         *pTmp = '#';
      }
      if (pTmp1 = strchr(pTmp, ' '))      // Keep first entry only
         if (strlen(pTmp) > 3)
            *pTmp1 = 0;

      // If Unit is too long, drop it
      if (strlen(pTmp) < 6)
         strcpy(acUnit, pTmp);
      else if (pTmp1 = strchr(pTmp, ','))
      {
         *pTmp1 = 0;
         strcpy(acUnit, pTmp);
      } else
         iTmp = 0;
      *pTmp = 0;
   }

   if (acUnit[0] > ' ')
   {
      strncpy(pAdrRec->Unit, acUnit, SIZ_S_UNITNO);
      strncpy(pAdrRec->UnitNox, acUnit, SIZ_S_UNITNOX);
   }

#ifdef _DEBUG
   // 2370 N LAGUNA CIR DR
   //if (!memcmp(pApn, "8940801051", 10))
   //   iTmp = 0;
   // 15701 E AVE M LANCASTER 
   //if (!memcmp(pApn, "8940388112", 10))
   //   iTmp = 0;
   // AT 11755 WILSHIRE BLVD,#50,LEASED
   //if (!memcmp(pApn, "8940436604", 10))
   //   iTmp = 0;
   // OFFICE SPACE AT 800 S  HOPE ST, #B28 LEASED
   //if (!memcmp(pApn, "8940436713", 10))
   //   iTmp = 0;
   // DESC AS 22036 E VALLEY BLVD
   //if (!memcmp(pApn, "8940149047", 10))
   //   iTmp = 0;
   // 07273 6239 010 002
   //if (!memcmp(pApn, "8950449408", 10) )
   //   iTmp = 0;
   
   //if (!memcmp(pApn, "8940759593", 10) )
   //   iTmp = 0;
#endif

   // Check for city 
   memset(acCity, 0, sizeof(acCity));
   if (pTmp = strchr(acAdr, ','))
   {
      if (!memcmp(pTmp+2, "LONG BEACH", 10))
      {
         memcpy(acCity, pTmp+2, 10);
         *pTmp = 0;
      } else
      if (!memcmp(pTmp+2, "LAKEWOOD", 8))
      {
         memcpy(acCity, pTmp+2, 8);
         *pTmp = 0;
      } else 
      if (pTmp = strstr(acAdr, "TEMPLE CITY"))
      {
         memcpy(acCity, pTmp, 11);
         *pTmp = 0;
      } else 
      if (pTmp = strstr(acAdr, "PASADENA"))
      {
         memcpy(acCity, pTmp, 8);
         *pTmp = 0;
      } else 
      if (pTmp = strstr(acAdr, "GARDENA"))
      {
         memcpy(acCity, pTmp, 7);
         *pTmp = 0;
      } else
         iTmp = 0;
   } 
   if (pTmp = strstr(acAdr, "CITY OF COMMERCE"))
   {
      memcpy(acCity, pTmp, 16);
      *pTmp = 0;
   } else 
   if (pTmp = strstr(&acAdr[10], "S MONICA"))
   {
      memcpy(acCity, "SANTA MONICA", 13);
      *pTmp = 0;
   }

   // Replace comma with space
   replChar(acAdr, ',', ' '); 
   remChar(acAdr, '.');
   blankRem(acAdr);

   // Fillter out multiple addresses
   if ((pTmp = strstr(acAdr, " BET ")) || (pTmp = strstr(acAdr, " BTWN ")) )
      *pTmp = 0;
   if (pTmp = strstr(acAdr, " N/O "))
      return 1;
   if (pTmp = strstr(acAdr, " E/O "))
      return 1;
   if (pTmp = strstr(acAdr, " W/O"))
      return 1;
   if (pTmp = strstr(acAdr, " W/F"))
      return 1;
   if (pTmp = strstr(acAdr, " SW OF "))
      return 1;
   if ((pTmp = strstr(acAdr, " S/LSD")) || (pTmp = strstr(acAdr, " S/LSE")) )
      *pTmp = 0;
   if ((pTmp = strchr(acAdr, '/')) && *(pTmp+1) >= 'A')
      *pTmp = 0;

   // Tokenize address
   iCnt = ParseString(acAdr, 32, 16, apItems);
   iItems = iCnt;
   iLastIdx = 0;

   if (!memcmp(apItems[iCnt-1], "SP", 2))
   {
      iCnt--;
      strcpy(pAdrRec->Unit, apItems[iCnt]);
      iLastIdx = iCnt+1;
   } 

   // 123 - 456 : keep first number only
   if (iCnt > 3 && *apItems[1] == '-')
   {
      for (iTmp = 1; iTmp < iCnt-2; iTmp++)
         apItems[iTmp] = apItems[iTmp+2];
      iCnt -= 2;
   }

   // 123 AND 456 : keep first number only
   if (iCnt > 3 && !memcmp(apItems[1], "AND", 3))
   {
      for (iTmp = 1; iTmp < iCnt-2; iTmp++)
         apItems[iTmp] = apItems[iTmp+2];
      iCnt -= 2;
   }

   // If strNum is 0, do not parse address
   iStrNo = atoi(apItems[0]);
   if (iStrNo == 0 || iCnt == 1)
      return iItems;
   else if (iCnt == 2 && strlen(apItems[1]) == 1)
      return 1;
   else
   {
      iIdx = 0;
      iTmp = strlen(apItems[0]);
      if (apItems[0][iTmp-1] <= '9')
      {
         pAdrRec->lStrNum = iStrNo;
      } else if (iTmp > 2 && apItems[0][iTmp-2] <= '9')
      {
         pAdrRec->lStrNum = iStrNo;
      }

      // 190TH ST
      if (isNumber(apItems[0]) || !strstr(apItems[0], "TH"))
      {
         strcpy(pAdrRec->strNum, apItems[0]);
         strcpy(pAdrRec->HseNo, apItems[0]);
         iIdx++;
      } 

      // Fraction - Make sure this is not the last token
      if (strchr(apItems[iIdx], '/') && (iCnt > (iIdx+1)) && (strlen(apItems[iIdx]) <= SIZ_S_STR_SUB))
         strcpy(pAdrRec->strSub, apItems[iIdx++]);

      // Dir
      iDir = 0;
      bDir = false;
      strcpy(acSfx, apItems[iIdx]);
      if (strlen(apItems[iIdx]) < 3)
      {
         if (acSfx[1] == '.')
            acSfx[1] = 0;

         while (iDir <= 7)
         {
            if (!strcmp(acSfx, asDir[iDir]))
            {
               bDir = true;
               break;
            }
            iDir++;
         }
      } else
      {
         // Check for long direction
         while (iDir < 4)
         {
            if (!strcmp(apItems[iIdx], asDir1[iDir]))
            {
               bDir = true;
               break;
            }
            iDir++;
         }
      }

      // Possible cases E ST or W RD or worst "E AVE M"
      // Check for suffix
      bRet = false;
      if (bDir && iCnt > 2)
      {
         // If next item is suffix, then this one is not direction
         // if (iTmp=GetSfxCode(apItems[iIdx+1]) && iCnt==iIdx+2)
         // The above statement won't work if Unit# is included.
         if (iTmp=GetSfxDev(apItems[iIdx+1]))
         {
            if (iCnt > iIdx+2 && (strlen(apItems[iIdx+2]) == 1 || !strcmp(apItems[iIdx+2], "K-4") || !strcmp(apItems[iIdx+2], "K4")))
            {
               // "E AVE M"
               strcpy(pAdrRec->strDir, asDir[iDir]);
               iIdx++;
            } else
            {
               strcpy(acStr, apItems[iIdx]);
               strcpy(pAdrRec->strSfx, GetSfxStr(iTmp));
               sprintf(pAdrRec->SfxCode, "%d", iTmp);
               bRet = true;         // Done
               if (iLastIdx < iIdx+2)  iLastIdx = iIdx+2;
            }
         } else
         {
            // This is direction
            strcpy(pAdrRec->strDir, asDir[iDir]);
            iIdx++;
         }
      }

      // Check for unit #
      if (iCnt > 3 && iCnt < 5 && apItems[iCnt-1][0] == '#')
      {
         strncpy(pAdrRec->Unit, (char *)&apItems[iCnt-1][0], sizeof(pAdrRec->Unit));
         strncpy(pAdrRec->UnitNox, (char *)&apItems[iCnt-1][0], sizeof(pAdrRec->UnitNox));
         iCnt--;
      } else if (!strcmp(apItems[iCnt-2], "STE") || !strcmp(apItems[iCnt-2], "SUITE")
         || !strcmp(apItems[iCnt-2], "UNIT") || !strcmp(apItems[iCnt-2], "UNIT#")
         || !strcmp(apItems[iCnt-2], "APT") || !strcmp(apItems[iCnt-2], "APT#")
         || !strcmp(apItems[iCnt-2], "RM") || (!strcmp(apItems[iCnt-2], "SP") && memcmp(apItems[iCnt-1], "AVE", 3)) )
      {
         if (*apItems[iCnt-1] == '#')
            sprintf(acTmp, "%s", apItems[iCnt-1]);
         else
            sprintf(acTmp, "#%s", apItems[iCnt-1]);
         strncpy(pAdrRec->Unit, acTmp, sizeof(pAdrRec->Unit));
         strncpy(pAdrRec->UnitNox, acTmp, sizeof(pAdrRec->UnitNox));
         iCnt -= 2;
      } else if (!memcmp(apItems[iCnt-1], "APT", 3))
      {
         sprintf(acTmp, "%s", apItems[iCnt-1]+3);
         strncpy(pAdrRec->Unit, acTmp, sizeof(pAdrRec->Unit));
         strncpy(pAdrRec->UnitNox, acTmp, sizeof(pAdrRec->UnitNox));
         iCnt--;
      } 

      // Not direction, token is str name
      if (!bRet)
      {
         // Check suffix
         if (iTmp=GetSfxDev(apItems[iCnt-1]))
         {
            strcpy(pAdrRec->strSfx, GetSfxStr(iTmp));
            sprintf(pAdrRec->SfxCode, "%d", iTmp);
            while (iIdx < iCnt-1)
            {
               // If multiple address, take the first one.
               // This loop break after first suffix found, ignore the rest.
               if ((iIdx < iCnt-2 && iIdx > 1) && (iTmp=GetSfxDev(apItems[iIdx])) )
               {
                  strcpy(pAdrRec->strSfx, GetSfxStr(iTmp));
                  sprintf(pAdrRec->SfxCode, "%d", iTmp);
                  break;
               }

               if (*apItems[iIdx] == '&' || *apItems[iIdx] == '-')
               {
                  iIdx++;
                  if (iIdx == 2 && iIdx < iCnt-1)
                     iIdx++;
               }
               strcat(acStr, apItems[iIdx++]);
               if (iIdx < iCnt)
                  strcat(acStr, " ");
            }
         } else if (iCnt > 2 && !strcmp(apItems[iCnt-2], "HWY"))
         {  // 123 HWY 70
            sprintf(acStr, "%s %s", apItems[iCnt-2], apItems[iCnt-1]);
         } else if (iCnt > iIdx+1 && !memcmp(apItems[iIdx], "AVE", 3) && *apItems[iIdx+1] != '#')
         {  // 15701 E AVE M
            sprintf(acStr, "%s %s ", apItems[iIdx], apItems[iIdx+1]);
            if (iLastIdx < iIdx+2 && iCnt > iIdx+2)
               iLastIdx = iIdx+2;
            if (iCnt > iIdx+2 && (!memcmp(apItems[iIdx+2], "EAST", 4) || !memcmp(apItems[iIdx+2], "WEST", 4)))
            {
               strcat(acStr, apItems[iIdx+2]);
               iLastIdx++;
            }
         } else
         {
            // Scan for suffix in multi address cases.  Keep the first one
            // 123 MAIN ST 540 N SANTA ANA BLVD
            while (iIdx < iCnt)
            {
               if (iIdx > 1 && (iTmp=GetSfxDev(apItems[iIdx])) )
               {
                  if (strcmp(apItems[iIdx], "LOOP"))
                  {
                     strcpy(pAdrRec->strSfx, GetSfxStr(iTmp));
                     sprintf(pAdrRec->SfxCode, "%d", iTmp);
                     if (iLastIdx < iIdx+1) iLastIdx = iIdx+1;

                     if (iCnt > (iIdx+2) && !memcmp(apItems[iIdx+1], "UNIT", 4))
                     {
                        strcpy(pAdrRec->Unit, apItems[iIdx+2]);
                        if (iLastIdx < iIdx+3) iLastIdx = iIdx+3;
                     } else if (strlen(apItems[iIdx+1]) == 1)
                     {
                        if (iCnt > iIdx+2  && *apItems[iIdx+1] == 'L'  && *apItems[iIdx+2] == 'A')
                        {
                           strcpy(acCity, "LOS ANGELES");
                           iLastIdx = -1;
                        } else if (iCnt > iIdx+2  && *apItems[iIdx+1] == 'B'  && !memcmp(apItems[iIdx+2], "HILL", 4))
                        {
                           strcpy(acCity, "BEVERLY HILLS");
                           iLastIdx = -1;
                        } else if (iCnt > iIdx+2  && *apItems[iIdx+1] == 'S'  && !memcmp(apItems[iIdx+2], "MONICA", 5))
                        {
                           strcpy(acCity, "SANTA MONICA");
                           iLastIdx = -1;
                        } else if (iCnt > iIdx+2  && *apItems[iIdx+1] == 'S'  && !memcmp(apItems[iIdx+2], "EL", 2))
                        {
                           strcpy(acCity, "SOUTH EL MONTE");
                           iLastIdx = -1;
                        } else if (iCnt > iIdx+2  && *apItems[iIdx+1] == 'S'  && !memcmp(apItems[iIdx+2], "PASA", 4))
                        {
                           strcpy(acCity, "SOUTH PASADENA");
                           iLastIdx = -1;
                        } else if (iCnt > iIdx+2  && *apItems[iIdx+1] == 'S'  && !memcmp(apItems[iIdx+2], "SAN", 3))
                        {
                           strcpy(acCity, "SOUTH SAN GABRIEL");
                           iLastIdx = -1;
                        } else if (iCnt > iIdx+2  && *apItems[iIdx+1] == 'E'  && (!memcmp(apItems[iIdx+2], "LA", 2) || !memcmp(apItems[iIdx+2], "LOS", 3)))
                        {
                           strcpy(acCity, "EAST LOS ANGELES");
                           iLastIdx = -1;
                        } else if (iCnt > iIdx+2  && *apItems[iIdx+1] == 'E'  && !memcmp(apItems[iIdx+2], "RNCHO", 5))
                        {
                           strcpy(acCity, "EAST RANCHO DOMINGUEZ");
                           iLastIdx = -1;
                        } else if (iCnt > iIdx+2  && *apItems[iIdx+1] == 'N'  && !memcmp(apItems[iIdx+2], "HOLLY", 5))
                        {
                           strcpy(acCity, "NORTH HOLLYWOOD");
                           iLastIdx = -1;
                        } else if (iCnt > iIdx+2  && *apItems[iIdx+1] == 'W'  && !memcmp(apItems[iIdx+2], "PALMDALE", 5))
                        {
                           strcpy(acCity, "PALMDALE");
                           iLastIdx = -1;
                        } else
                        {
                           sprintf(pAdrRec->Unit, "#%c", *apItems[iIdx+1]);
                           if (iLastIdx < iIdx+2) iLastIdx = iIdx+2;
                        }
                     } else if (*apItems[iIdx+1] == '#')
                     {
                        strcpy(pAdrRec->Unit, apItems[iIdx+1]);
                        if (iLastIdx < iIdx+2) iLastIdx = iIdx+2;
                     }
                     break;
                  }
               } else if (iCnt == iIdx+2  && !strcmp(apItems[iIdx+1], "LA")) // 5/26/2019
               {
                  strcpy(acCity, "LOS ANGELES");
                  iCnt = iIdx+1;
               }

               if (*apItems[iIdx] == '&' || *apItems[iIdx] == '-')
               {
                  iIdx++;
                  if (iIdx == 2 && iIdx < iCnt-1)
                     iIdx++;
               }

               strcat(acStr, apItems[iIdx++]);
               if (iIdx < iCnt)
                  strcat(acStr, " ");

               // special case
               if (iCnt > iIdx+1 && !memcmp(apItems[iIdx], "CIR", 3) && !memcmp(apItems[iIdx+1], "DR", 2))
               {
                  strcat(acStr, "CIRCLE ");
                  iIdx++;
               } else
               if (iCnt > iIdx+1 && !memcmp(apItems[iIdx], "CYN", 3) && GetSfxDev(apItems[iIdx]))
               {
                  //if (!memcmp(apItems[iIdx+1], "TR", 2) && !memcmp(apItems[iIdx+1], "BLVD", 4))
                  //   iTmp = 0;
                  strcat(acStr, "CANYON ");
                  iIdx++;
               } else
               if (iCnt > iIdx+1 && !strcmp(apItems[iIdx], "LOOP") && (!strcmp(apItems[iIdx+1], "S") || !strcmp(apItems[iIdx+1], "SOUTH") || !strcmp(apItems[iIdx+1], "S0UTH") ))
               {
                  strcat(acStr, "LOOP S ");
                  if (iLastIdx < iIdx+2 && iCnt > iIdx+2)
                     iLastIdx = iIdx+2;
                  break;
               } else
               if (iCnt > iIdx+1 && (!strcmp(apItems[iIdx], "ST") || !strcmp(apItems[iIdx], "STREET")) && 
                  (!strcmp(apItems[iIdx+1], "W") || !strcmp(apItems[iIdx+1], "E") || !strcmp(apItems[iIdx+1], "WEST") || !strcmp(apItems[iIdx+1], "EAST")) )
               {
                  sprintf(acTmp, "%s %s ", apItems[iIdx], apItems[iIdx+1]);
                  strcat(acStr, acTmp);
                  if (iLastIdx < iIdx+2 && iCnt > iIdx+2)
                     iLastIdx = iIdx+2;
                  break;
               } else
               if (iCnt > iIdx+1 && !strcmp(apItems[iIdx], "PARK") && 
                  (!strcmp(apItems[iIdx+1], "W") || !strcmp(apItems[iIdx+1], "E") || !strcmp(apItems[iIdx+1], "WEST") || !strcmp(apItems[iIdx+1], "EAST")) )
               {
                  sprintf(acTmp, "%s %s ", apItems[iIdx], apItems[iIdx+1]);
                  strcat(acStr, acTmp);
                  if (iLastIdx < iIdx+2 && iCnt > iIdx+2)
                     iLastIdx = iIdx+2;
                  break;
               }
            }
         }
      }
   }

   // Special case
   if (strstr(acStr, "ADJACENT TO"))
      acStr[0] = 0;
   else if (!strcmp(acStr, "050 "))
      strcpy(acStr, "50TH ");
   else if (!strcmp(acStr, "063 "))
      strcpy(acStr, "63RD ");
   else if (!strcmp(acStr, "022 "))
      strcpy(acStr, "22ND ");
   else if (acStr[0] == '0' && strlen(acStr) > 4)
   {
      // 015 ST WEST
      if (!strcmp(acStr, "015 ST WEST "))
         strcpy(acStr, "15TH ST WEST");
      else
         acStr[0] = 0;
   } else
   {
      // StrName is AltAPn
      iTmp = strlen(acStr);
      if (iTmp == 12 && acStr[4] == ' ' && acStr[8] == ' ' && isdigit(acStr[9]))
         acStr[0] = 0;
   }

   strncpy(pAdrRec->strName, acStr, SIZ_M_STREET);
   if (pSave)
   {
      strcpy(acAdr, pSave);
      iCnt = ParseString(acAdr, 32, 16, apItems);
      iLastIdx = 1;
   }

   // Check for city name
   if (iLastIdx == 0)
   {
      if (!memcmp(acStr, "CENTURY PARK", 12))
         strcpy(acCity, "CENTURY CITY");
      else if (!memcmp(acStr, "DONALD DOUGLAS LOOP", 17))
         strcpy(acCity, "SANTA MONICA");
   }

   if (iLastIdx > 0)
   {
      if (iLastIdx < iCnt-3 && !strcmp(apItems[iLastIdx], "THE") && (!strcmp(apItems[iLastIdx+1], "CITY") || !strcmp(apItems[iLastIdx+1], "CTY")))
      {
         sprintf(acCity, "%s %s %s", apItems[iLastIdx+1], apItems[iLastIdx+2], apItems[iLastIdx+3]);
      } else if (iLastIdx < iCnt-2 && !strcmp(apItems[iLastIdx], "CITY"))
      {
         sprintf(acCity, "%s %s %s", apItems[iLastIdx], apItems[iLastIdx+1], apItems[iLastIdx+2]);
      } else if (iLastIdx < iCnt-2 && !memcmp(apItems[iLastIdx+2], "EST", 3))
      {
         sprintf(acCity, "%s %s %s", apItems[iLastIdx], apItems[iLastIdx+1], apItems[iLastIdx+2]);
      } else if (iLastIdx < iCnt-1)
      {
         // Take 2 words after lastidx
         if (*apItems[iLastIdx+1] >= 'A')
            sprintf(acCity, "%s %s", apItems[iLastIdx], apItems[iLastIdx+1]);
         else
            strcpy(acCity, apItems[iLastIdx]);
      } else if (iLastIdx < iCnt && *apItems[iLastIdx] >= 'A')
      {
         // Take 1 word
         strcpy(acCity, apItems[iLastIdx]);
      } else if (iLastIdx < iCnt && *apItems[iLastIdx] == '#' && pAdrRec->Unit[0] < '0')
      {
         strcpy(pAdrRec->Unit, apItems[iLastIdx]);
         strcpy(pAdrRec->UnitNox, apItems[iLastIdx]);
      } 
   }

   if (acCity[0] < 'A' && !memcmp(pApn+OFF_M_STRNUM, pAdrRec->strNum, strlen(pAdrRec->strNum))
      && !memcmp(pApn+OFF_M_STREET, pAdrRec->strName, strlen(pAdrRec->strName)))
   {
      memcpy(acCity, pApn+OFF_M_CITY, SIZ_M_CITY);
      myTrim(acCity, SIZ_M_CITY);
   }  

   if (acCity[0] >= 'A')
   {
      if (iTmp = City2CodeEx(acCity, acStr, acTmp, pApn))
      {
         strcpy(pAdrRec->City, acTmp);
         strcpy(pAdrRec->CityIdx, acStr);
      } else if (acCity[0] > '9')
         LogMsg("*** Unknown city: %s", acCity);
   }

   if (pAdrRec->strNum[0] > '0' && pAdrRec->strName[0] > '0')
      memcpy((void *)psAdr, (void *)pAdrRec, sizeof(ADR_REC));
   else
      iItems = 0;

   return iItems;
}

/****************************** Lax_ParseSitus *******************************
 *
 * Extract situs from legal description. If found, populate pOutbuf and  
 * return > 0, Otherwise, error.
 *
 * Look for "LOCATED AT", " AT "
 * - CABIN LOC IN MALIBU LAKE MTN CLUB LOT   5 TR A AT 2200 E LAKESHORE DR AGOURA
 * - LOCATED AT           300 WORLD WAY LOS ANGELES
 * - LOCATED AT: 7821 ORION AVE LOS ANGELES  CA
 * - LOC AT 19778 COTTONWOOD DR IN SANTA
 * - LOC ABOUT 17508 S FIGUEROA ST
 *
 *****************************************************************************/

int Lax_ParseSitus(ADR_REC *psAdr, char *pApn, char *pLegal, char *pShortLegal)
{
   char     *pTmp, *pTmp1, *pInbuf, acTmp[256], acLegal[256];
   int      iRet, iTmp;

   // Copy input buffer before processing
   pInbuf = strcpy(acLegal, pLegal);

#ifdef _DEBUG
   //if (!memcmp(pApn, "8940435615", 10))
   //   iRet = 0;
#endif

   // Finding start of address
   if (!memcmp(pInbuf+XSIZ_LEGAL, "AT ", 3))
   {
      pTmp = pInbuf+XSIZ_LEGAL;
      iTmp = 3;
   } else
   {
      pTmp = strstr(pInbuf, " AT ");
      iTmp = 4;
   }

   if (!pTmp)
   {
      pTmp = strstr(pInbuf, " DESC AS ");
      if (pTmp)
      {
         iTmp = 9;
         if ((pTmp1 = strstr(pTmp, "SQ FT")) || 
             (pTmp1 = strstr(pTmp, " YRS ")) ||
             (pTmp1 = strstr(pTmp, " UNIT SEN")) )
            *pTmp1 = 0;
      }
   }
   if (!pTmp)
   {
      pTmp = strstr(pInbuf, " REAR OF ");
      if (pTmp)
         iTmp = 9;
   }
   if (!pTmp)
   {
      pTmp = strstr(pInbuf, " LOC ABOUT ");
      if (pTmp)
         iTmp = 11;
   }
   if (!pTmp)
   {
      pTmp = strstr(pInbuf, " LOC @ ");
      if (pTmp)
         iTmp = 7;
   }
   // ABT 8613 ARDENDALE AVE
   if (!pTmp)
   {
      pTmp = strstr(pInbuf, " ABT ");
      iTmp = 5;
   }

   if (!pTmp)
   {
      pTmp = strstr(pInbuf, " NEAR ");
      if (pTmp)
         iTmp = 6;
   }

   // Take care of NORTH OF, WEST OF, EAST OF, SOUTH OF ...
   // LOC NORTH OF 6252 TELEGRAPH ROAD
   // LOC WEST OF 3315 SAN PASQUAL ST
   if (!pTmp)
      pTmp = strstr(pInbuf, " OF ");

   // Check short legal if not found in full legal
   if (!pTmp && isdigit(*pShortLegal) && !strstr(pShortLegal, "ACRE FT"))
   {
      // Check for formatted APN 8900763701
      if ((*(pShortLegal+4) == '-' && *(pShortLegal+8) == '-') ||
          (*(pShortLegal+4) == ' ' && *(pShortLegal+8) == ' '))
      {
         iTmp = 0;
         pTmp = NULL;
      } else
      {
         pTmp = pShortLegal;
         iTmp = 0;
      }
   }

   // After AT or OF, addr must start with HSENO
   if (pTmp && (*(pTmp+iTmp) > '0' && *(pTmp+iTmp) <= '9') && isCharIncluded(pTmp+iTmp, 0))
   {
      // Finding the end of address and terminate it
      if (pTmp1 = strstr(pTmp, " ON LAND "))
         *pTmp1 = 0;
      else if (pTmp1 = strstr(pTmp, " OFFICE "))
         *pTmp1 = 0;
      else if ((pTmp1 = strstr(pTmp, " GROUND LEASE ")) || (pTmp1 = strstr(pTmp, " SUBLEASED ")) )
         *pTmp1 = 0;
      else if ((pTmp1 = strstr(pTmp, " LEASED ")) || (pTmp1 = strstr(pTmp, " S/LEASED ")) )
         *pTmp1 = 0;
      
      if ((pTmp1 = strstr(pTmp, " LSD ")) || (pTmp1 = strstr(pTmp, " LSE ")))
         *pTmp1 = 0;
      if ((pTmp1 = strstr(pTmp, " LEA SED")) || (pTmp1 = strstr(pTmp, " LEAS ED")) )
         *pTmp1 = 0;
      if (pTmp1 = strstr(pTmp, " OWNED "))
         *pTmp1 = 0;
      
      if (pTmp1 = strstr(pTmp, " FUTURE "))
         *pTmp1 = 0;      
      //if (pTmp1 = strstr(pTmp, " DESC AS "))
      //   *pTmp1 = 0;
      if (pTmp1 = strstr(pTmp, " FOR USE AS "))
         *pTmp1 = 0;
      if (pTmp1 = strstr(pTmp, " LAC_USC "))
         *pTmp1 = 0;
      if (pTmp1 = strstr(pTmp, " RESTAURANT "))
         *pTmp1 = 0;
      if (pTmp1 = strstr(pTmp, " FROM "))
         *pTmp1 = 0;
      if (pTmp1 = strstr(pTmp, " FOR "))
         *pTmp1 = 0;
      if (pTmp1 = strstr(pTmp, " INCLUDING "))
         *pTmp1 = 0;
      if ((pTmp1 = strstr(pTmp, " AND ")) && *(pTmp1-1) > '9')    // Avoid 123 AND 133 
         *pTmp1 = 0;
      if (pTmp1 = strstr(pTmp, " PARKING "))
         *pTmp1 = 0;

      if ((pTmp1 = strchr(pTmp, ';')) || (pTmp1 = strchr(pTmp, '(')) )
         *pTmp1 = 0;

      if (pTmp1 = strstr(pTmp, " FOR "))
         *pTmp1 = 0;
      if (pTmp1 = strstr(pTmp, " PROCESSED"))
         *pTmp1 = 0;

      // Skip known invalid address
      if (strstr(pTmp, "WWW"))
      {
         LogMsg("not situs: %s", pTmp+4);
         return 0;
      } else
      {
#ifdef _DEBUG
         // 8900763701, 8940090001
         //if (!memcmp(pApn, "8940435658", 10) )
         //   iRet = 0;
#endif
         iRet = parseSAdr1(psAdr, pTmp+iTmp, pApn);
         if (iRet > 1 )
         {
            // Good situs
            if (bDebug)
               LogMsg("situs: [%.*s] %s", iApnLen, pApn, pTmp+4);
            if ((pTmp=strstr(pLegal, " CABIN ")) && psAdr->Unit[0] <= ' ')
            {
               memcpy(acTmp, pTmp+7, 10);
               acTmp[7] = 0;
               if (pTmp=strchr(acTmp, ' '))
               {
                  *pTmp = 0;
                  strncpy(psAdr->Unit, acTmp, sizeof(psAdr->Unit));
               }
            }

            // Special case:
            if (!memcmp(pApn, "8940435658", 10) && psAdr->lStrNum == 337)
            {
               psAdr->lStrNum = 4337;
               strcpy(psAdr->HseNo, "4337");
               strcpy(psAdr->strNum, "4337");
            }
            if (psAdr->City[0] <= ' ')
            {
               if (pTmp=strstr(pLegal, " CITY OF ")) 
               {
                  memcpy(acTmp, pTmp+9, 20);
                  acTmp[20] = 0;
                  if (pTmp=strchr(acTmp, ' '))
                  {
                     char *apItems[16], acCity[64], acCode[16];
                     int   iCnt;

                     iCnt = ParseString(acTmp, 32, 16, apItems);
                     if (iCnt >= 3 && !isdigit(*apItems[2]))
                        sprintf(acCity, "%s %s %s", apItems[0], apItems[1], apItems[2]);
                     else if (iCnt >= 2 && !isdigit(*apItems[1]))
                        sprintf(acCity, "%s %s", apItems[0], apItems[1]);
                     else
                        strcpy(acCity, apItems[0]);

                     if (iTmp = City2CodeEx(acCity, acCode, acTmp, pApn))
                     {
                        strcpy(psAdr->City, acTmp);
                        strcpy(psAdr->CityIdx, acCode);
                     }
                  }
               }
            }
         } else
         {
            LogMsg("not situs: [%.*s] %s", iApnLen, pApn, pTmp+iTmp);
            iRet = 0;
         }
      }
   } else
      iRet = 0;

   return iRet;
}

/****************************** Lax_MergeXSAdr *******************************
 *
 * Merge situs from XREF_REC
 *
 *****************************************************************************/

void Lax_MergeXSAdr(char *pOutbuf, char *pInbuf, char *pLegalDesc)
{
   XREF_REC *pRec;
   ADR_REC  lglSitus;
   char     *pTmp, acTmp[256], acAddr1[64];
   char     acStr[32], acSfx[32], acSfxCode[32], acUnit[32];
   int      iTmp, iStrNo;
   int      iIdx=0;

   pRec = (XREF_REC *)pInbuf;
   memcpy(acTmp, pRec->Narrative, XSIZ_NARRATIVE);
   acTmp[XSIZ_NARRATIVE] = 0;

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "8940413032", 10) )//|| !memcmp(pOutbuf, "8940759593", 10))
   //   iTmp = 0;
#endif

   // If we can find addr in legal, use it.  Otherwise, use property situs.
   if (*pLegalDesc > ' ' && (iTmp = Lax_ParseSitus(&lglSitus, pOutbuf, pLegalDesc, acTmp)) > 0)
      iTmp = 0;
   else
      memset((void *)&lglSitus, 0, sizeof(ADR_REC));

   // If situs from legal is different from property's situs, use legal situs.
   acAddr1[0] = 0;

   // StrNum - remove leading 0
   iStrNo = atoin(pRec->S_StrNo, XSIZ_S_HSENO);
   if (iStrNo != lglSitus.lStrNum && lglSitus.lStrNum > 0 && lglSitus.strName[0] > '0')
   {
      // Use legal situs
      iTmp = sprintf(acAddr1, "%d ", lglSitus.lStrNum);
      memcpy(pOutbuf+OFF_S_STRNUM, acAddr1, iTmp);
      strcpy(acAddr1, lglSitus.strNum);
      strcat(acAddr1, " ");
      memcpy(pOutbuf+OFF_S_HSENO,  lglSitus.strNum, strlen(lglSitus.strNum));

      // StrSub
      if (isalnum(lglSitus.strSub[0]))
      {
         memcpy(pOutbuf+OFF_S_STR_SUB, lglSitus.strSub, strlen(lglSitus.strSub));
         strcat(acAddr1, lglSitus.strSub);
         strcat(acAddr1, " ");
      } 

      // StrDir
      if (lglSitus.strDir[0] >= 'E')
      {
         *(pOutbuf+OFF_S_DIR) = lglSitus.strDir[0];
         strcat(acAddr1, lglSitus.strDir);
         strcat(acAddr1, " ");
      }

      // Situs street name & suffix
      vmemcpy(pOutbuf+OFF_S_STREET, lglSitus.strName, SIZ_S_STREET);
      strcat(acAddr1, lglSitus.strName);
      if (lglSitus.SfxCode[0] > ' ')
      {
         strcat(acAddr1, lglSitus.strSfx);
         memcpy(pOutbuf+OFF_S_SUFF, lglSitus.SfxCode, strlen(lglSitus.SfxCode));
      }

      // UnitNo
      if (lglSitus.Unit[0] > ' ')
      {
         if (pTmp = strstr(lglSitus.UnitNox, "-S/"))     // Chk for ???-S/LSD
            *pTmp = 0;
         strcpy(acUnit, (char *)&lglSitus.UnitNox[0]);
         iTmp = strlen(acUnit);
         if (acUnit[iTmp-1] == ',')
            acUnit[iTmp-1] = 0;

         vmemcpy(pOutbuf+OFF_S_UNITNOX, acUnit, SIZ_S_UNITNOX);
         strcat(acAddr1, " ");
         strcat(acAddr1, acUnit);

         if (iTmp >= SIZ_S_UNITNO && ((pTmp = strchr(acUnit, '/')) || (pTmp = strchr(acUnit, ','))))
            *pTmp = 0;
         vmemcpy(pOutbuf+OFF_S_UNITNO, acUnit, SIZ_S_UNITNO);
      } 
   } else
   {
      if (iStrNo > 0)
      {
         // Right justify strNo
         sprintf(acAddr1, "%d       ", iStrNo);
         memcpy(pOutbuf+OFF_S_STRNUM, acAddr1, SIZ_S_STRNUM);
         memcpy(pOutbuf+OFF_S_HSENO,  acAddr1, SIZ_S_STRNUM);

         // StrSub
         if (isalnum(pRec->S_StrSub[0]))
         {
            memcpy(pOutbuf+OFF_S_STR_SUB, pRec->S_StrSub, XSIZ_S_FRA);
            memcpy(acTmp, pRec->S_StrSub, XSIZ_S_FRA);
            strcat(acAddr1, myTrim(acTmp, XSIZ_S_FRA));
            strcat(acAddr1, " ");
         } else
            // Clean up old garbage
            memset(pOutbuf+OFF_S_STR_SUB, ' ', SIZ_S_STR_SUB);

         // StrDir
         if (pRec->S_Dir[0] > 'A')
         {
            *(pOutbuf+OFF_S_DIR) = pRec->S_Dir[0];
            sprintf(acTmp, "%c ", pRec->S_Dir[0]);
            strcat(acAddr1, acTmp);
         }
      }

      // Situs street name & suffix
      memcpy(acTmp, pRec->S_StrName, XSIZ_S_STRNAME);
      myTrim(acTmp, XSIZ_S_STRNAME);

      if (acTmp[0] > ' ')
      {
         if (memcmp(acTmp, "VAC/", 4))
         {
            strcat(acAddr1, acTmp);
            parseStreet(acTmp, acStr, acSfx, acSfxCode);

            vmemcpy(pOutbuf+OFF_S_STREET, acStr, SIZ_S_STREET);
            memcpy(pOutbuf+OFF_S_SUFF, acSfxCode, strlen(acSfxCode));
         } else
         {
            pTmp = (char *)&acTmp[4];
            strcat(acAddr1, pTmp);
            vmemcpy(pOutbuf+OFF_S_STREET, pTmp, OFF_S_STREET);
         }
      }

#ifdef _DEBUG
   //if (!memcmp(pRec->APN, "8940412486", 10))
   //   iTmp = 0;
#endif

      // UnitNo
      if (pRec->S_Unit[0] > ' ')
      {
         if (!memcmp(pRec->S_Unit, "SPACE", 5))
         {
            iTmp = atoin((char *)&pRec->S_Unit[5], XSIZ_S_UNIT-5);
            sprintf(acUnit, "%d", iTmp);
         } else if (!memcmp(pRec->S_Unit, "SPC", 3))
         {
            iTmp = atoin((char *)&pRec->S_Unit[3], XSIZ_S_UNIT-3);
            sprintf(acUnit, "%d", iTmp);
         } else if (!memcmp(pRec->S_Unit, "SP NO", 5))
         {
            iTmp = atoin((char *)&pRec->S_Unit[5], XSIZ_S_UNIT-5);
            sprintf(acUnit, "%d", iTmp);
         } else if (!memcmp(pRec->S_Unit, "NO ", 3))
         {
            iTmp = atoin((char *)&pRec->S_Unit[3], XSIZ_S_UNIT-3);
            sprintf(acUnit, "%d", iTmp);
         } else if (!memcmp(pRec->S_Unit, "STE", 3))
         {
            iTmp = atoin((char *)&pRec->S_Unit[3], XSIZ_S_UNIT-3);
            sprintf(acUnit, "%d", iTmp);
         } else if (!memcmp(pRec->S_Unit, "SP", 2))
         {
            iTmp = 2;
            while ((pRec->S_Unit[iTmp] == '0' || pRec->S_Unit[iTmp] == ' ' || pRec->S_Unit[iTmp] == '.') && iTmp < RSIZ_SUNITNO)
               iTmp++;
            if (iTmp < RSIZ_SUNITNO)
            {
               memcpy(acUnit, &pRec->S_Unit[iTmp], RSIZ_SUNITNO-iTmp);
               acUnit[RSIZ_SUNITNO-iTmp] = 0;
               remChar(acUnit, ' ');
            } else
               acUnit[0] = 0;
         } else if (!memcmp(pRec->S_Unit, "UNIT", 4))
         {
            memcpy(acUnit, (char *)&pRec->S_Unit[0], XSIZ_S_UNIT);
            acUnit[XSIZ_S_UNIT] = 0;
            blankRem(acUnit);
         } else if (!memcmp(pRec->S_Unit, "AP", 2) || 
            !memcmp(pRec->S_Unit, "RD", 2) || 
            !memcmp(pRec->S_Unit, "P ", 2) || 
            !memcmp(pRec->S_Unit, "S^", 2) || 
            !memcmp(pRec->S_Unit, "SO", 2))
         {
            iTmp = atoin((char *)&pRec->S_Unit[2], XSIZ_S_UNIT-2);
            sprintf(acUnit, "%d", iTmp);
         } else if (pRec->S_Unit[0] == '#')
         {
            iTmp = atoin((char *)&pRec->S_Unit[1], XSIZ_S_UNIT-1);
            if (iTmp > 0)
               sprintf(acUnit, "%d", iTmp);
            else
            {
               memcpy(acUnit, (char *)&pRec->S_Unit[1], XSIZ_S_UNIT-1);
               acUnit[XSIZ_S_UNIT-1] = 0;
               remChar(acUnit, ' ');
            }
         } else if (pRec->S_Unit[0] != '(')
         {
            if (!memcmp(pRec->S_Unit, (char *)&pRec->S_Unit[3], 3))
            {  // 2353002097
               memcpy(acUnit, (char *)&pRec->S_Unit[0], 3);
               acUnit[3] = 0;
            } else
            {
               memcpy(acUnit, (char *)&pRec->S_Unit[0], XSIZ_S_UNIT);
               acUnit[XSIZ_S_UNIT] = 0;
            }
            remChar(acUnit, ' ');
         } else
            acUnit[0] = 0;

         if (strlen(acUnit) < 6 && acUnit[0] > ' ')
            sprintf(acTmp, "SP %s", acUnit);
         else
            strcpy(acTmp, acUnit);
         iTmp = blankRem(acTmp);
      } else
         acTmp[0] = 0;

      if (acTmp[0] > ' ' && iTmp > 0)
      {
         vmemcpy(pOutbuf+OFF_S_UNITNOX, acTmp, SIZ_S_UNITNOX);
         strcat(acAddr1, " ");
         strcat(acAddr1, acTmp);
         if (iTmp >= SIZ_S_UNITNO && ((pTmp = strchr(acTmp, '/')) || (pTmp = strchr(acTmp, ','))))
            *pTmp = 0;
         vmemcpy(pOutbuf+OFF_S_UNITNO, acTmp, SIZ_S_UNITNO);
      } 
   }

   iTmp = blankRem(acAddr1);
   if (iTmp > 5)
      vmemcpy(pOutbuf+OFF_S_ADDR_D, acAddr1, SIZ_S_ADDR_D);

#ifdef _DEBUG
   //if (!memcmp(pRec->APN, "8940388112", 10))
   //   iTmp = 0;
#endif

   // Situs city
   if (lglSitus.City[0] < 'A' && pRec->S_CitySt[0] >= 'A')
   {
      memcpy(acTmp, pRec->S_CitySt, XSIZ_M_CITYST);
      acTmp[XSIZ_M_CITYST] = 0;
      if (pTmp = strchr(acTmp, '*'))
         *pTmp = 0;
      else if (pTmp = strchr(acTmp, ';'))
         *pTmp = 0;
      else if (pTmp = strstr(acTmp, " CA"))
         *pTmp = 0;
      if (acTmp[0] > ' ')
      {
         iTmp = City2CodeEx(acTmp, &lglSitus.CityIdx[0], &lglSitus.City[0], pOutbuf);
      }
      
      // Situs Zip
      if (pRec->S_Zip[0] > '8')
      {
         if (!memcmp(&pRec->S_Zip[5], "0000", 4))
            iTmp = SIZ_S_ZIP;
         else
            iTmp = SIZ_S_ZIP+SIZ_S_ZIP4;
         memcpy(pOutbuf+OFF_S_ZIP, pRec->S_Zip, iTmp);
      } 
   }

   if (lglSitus.City[0] >= 'A')
   {
      memcpy(pOutbuf+OFF_S_CITY, lglSitus.CityIdx, strlen(lglSitus.CityIdx));
      memcpy(pOutbuf+OFF_S_ST, "CA", SIZ_S_ST);

      iTmp = sprintf(acTmp, "%s CA %.5s", lglSitus.City, pOutbuf+OFF_S_ZIP);
      memcpy(pOutbuf+OFF_S_CTY_ST_D, acTmp, iTmp);
   } 

}

/******************************* Lax_MergeXMAdr ******************************
 *
 * Merge mailing from XREF_REC
 * M_Key: (F)ormatted, (E) POBox, (H) No StrNo
 *
 *****************************************************************************/

void Lax_MergeXMAdr(char *pOutbuf, char *pInbuf)
{
   XREF_REC *pRec;
   char     *pTmp, acTmp[256], acAddr1[64];
   char     acStr[32], acSfx[16], acCity[32], acState[4];
   int      iTmp, iStrNo;
   int      iIdx=0;

   pRec = (XREF_REC *)pInbuf;

   if (pRec->M_Key[0] == ' ')
      return;

   // Clear old Mailing
   removeMailing(pOutbuf, false);

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "4001011015", 10))
   //   iStrNo = 0;
#endif
   // StrNum - remove leading 0
   iStrNo = atoin(pRec->M_StrNo, XSIZ_M_HSENO);
   acAddr1[0] = 0;
   if (iStrNo > 0)
   {
      // Right justify strNo
      iTmp = sprintf(acTmp, "%d", iStrNo);
      memcpy(pOutbuf+OFF_M_STRNUM, acTmp, iTmp);
      sprintf(acAddr1, "%d", iStrNo);

      // StrSub
      //if (pRec->M_StrSub[0] > ' ')
      if (isalnum(pRec->M_StrSub[0]))
      {
         memcpy(pOutbuf+OFF_M_STR_SUB, pRec->M_StrSub, XSIZ_M_FRA);
         memcpy(acTmp, pRec->M_StrSub, RSIZ_MSTRSUB);
         strcat(acAddr1, " ");
         strcat(acAddr1, myTrim(acTmp, RSIZ_MSTRSUB));
      }

      // StrDir
      if (pRec->M_Dir[0] > ' ')
      {
         *(pOutbuf+OFF_M_DIR) = pRec->M_Dir[0];
         sprintf(acTmp, " %c", pRec->M_Dir[0]);
         strcat(acAddr1, acTmp);
      }

      strcat(acAddr1, " ");
   }

   // Check for bad character
   if (iTmp = replUnPrtChar(pRec->M_StrName, ' ', XSIZ_M_STRNAME))
      LogMsg0("*** Bad character in M_StrName at %.12s", pOutbuf);

   // Mail street name & suffix
   memcpy(acTmp, pRec->M_StrName, XSIZ_M_STRNAME);
   myTrim(acTmp, XSIZ_M_STRNAME);
   strcat(acAddr1, acTmp);
   if (acTmp[0] > ' ')
   {
      if (pRec->M_Key[0] == 'E')    // PO Box
         strcpy(acStr, acTmp);
      else
      {
         parseStreet(acTmp, acStr, acSfx, NULL);
         memcpy(pOutbuf+OFF_M_SUFF, acSfx, strlen(acSfx));
      }
      iTmp = strlen(acStr);
      if (iTmp > SIZ_M_STREET) iTmp = SIZ_M_STREET;
      memcpy(pOutbuf+OFF_M_STREET, acStr, iTmp);
   }

   // UnitNo
   iTmp = 0;
   for (iIdx = 0; iIdx < XSIZ_M_UNIT; iIdx++)
   {
      if (pRec->M_Unit[iIdx] != ' ')
         acTmp[iTmp++] = pRec->M_Unit[iIdx];
   }
   if (iTmp > 0)
   {
      acTmp[iTmp] = 0;
      if (!memcmp(acTmp, "NO.", 3) || !memcmp(acTmp, "APT", 3) || !memcmp(acTmp, "STE", 3))
      {
         pTmp = (char *)&acTmp[2];
         *pTmp = '#';
      } else if (!memcmp(acTmp, "NO", 2) || !memcmp(acTmp, "RM", 2))
      {
         pTmp = (char *)&acTmp[1];
         *pTmp = '#';
      } else if (!memcmp(acTmp, "UNIT", 4))
      {
         pTmp = (char *)&acTmp[3];
         *pTmp = '#';
      } else
         pTmp = (char *)&acTmp[0];

      memcpy(pOutbuf+OFF_M_UNITNO, pTmp, strlen(pTmp));
      strcat(acAddr1, " ");
      strcat(acAddr1, pTmp);
   }
   memcpy(pOutbuf+OFF_M_ADDR_D, acAddr1, strlen(acAddr1));

   // Mailing city
   memcpy(pOutbuf+OFF_M_CTY_ST_D, pRec->M_CitySt, XSIZ_M_CITYST);
   if (pRec->M_CitySt[0] > ' ')
   {
      memcpy(acTmp, pRec->M_CitySt, XSIZ_M_CITYST);
      parseCitySt(myTrim(acTmp, RSIZ_MCITYST), acCity, acState, NULL);
      memcpy(pOutbuf+OFF_M_CITY, acCity, strlen(acCity));
      memcpy(pOutbuf+OFF_M_ST, acState, strlen(acState));
   }

   // Mailing Zip
   if (pRec->M_Zip[0] >= '0')
   {
      if (pRec->M_Zip[0] <= '9')
      {
         if (!memcmp(&pRec->M_Zip[5], "0000", 4))
            iTmp = SIZ_M_ZIP;
         else
            iTmp = SIZ_M_ZIP+SIZ_M_ZIP4;
         memcpy(pOutbuf+OFF_M_ZIP, pRec->M_Zip, iTmp);
      }
   }
}

/****************************** Lax_FormatAltApn ****************************
 *
 * 029 902
 * 7413-25-991
 * 5153 26 901
 * 7505002908 (this is APN)
 * COMPANY # 0142550002 (this is not APN)
 * 2856 010 901
 * 8061-035-900
 * 4291 012  000
 * 5722-027-910, 911
 * 5545-002-903, 904, 905
 * 8125 026 902 & 903
 * 8762 018 902/903
 * 5019 025 911-912
 * 24135-001-01 5220 034 000
 *                    2483 009 001&002
 *                      8341 008 912 & 914
 *                             8588 026 901 
 * 204-6372-1207               7502 017 001 
 * 2519 020 903 THRU 907
 * 7417 010 902 904 905 
 * SCH DIST 4156-004-902
 * OF ROLLINGS HILLS  7569 003 904
 * 4283 002 900 AND 4283 001 901
 * 4290 015 917 AND 920
 * 4249 026 270, 277 THROUGH 291
 * NORWALK LA MIRADA USD       8038 028 902
 * OF WEST HOLLYWOOD CA 90069  4336 008 911
 * LA CITY DEPT OF GEN SERVICES5161 012 903
 * GOV PCL                    #8379 020 901
 * LONG BEACH 7207 020 032     7207 020 033
 * 2307-025-900 (PORTION), 2307-020-902
 * SBE ID# 804-19-64           7426 001 815
 * L A CO SANITATION DIST #8 7406 026-911 
 * 7413 035 9163
 * 5713 037 0000
 * LBE2580 OO699 2849 021 018
 *
 ****************************************************************************/

int Lax_FormatAltApn(char *pOutbuf, char *pInbuf, char *pLegal, int iLen)
{
   char  *pTmp, *pNdx, sTmp[256], sApn[16];
   int   iRet, iLglLen=0;

   if (pLegal && *pLegal > ' ')
      iLglLen = strlen(pLegal);

   sApn[0] = 0;
   if (iLglLen > 20)
   {
      // If last 12 bytes is APN
      if (isdigit(*(pLegal+iLglLen-1)) && isdigit(*(pLegal+iLglLen-12)))
         strcpy(sApn, pLegal+iLglLen-12);
   } 

   // If byte position XSIZ_NARRATIVE is a digit, use last 12 bytes
   if (iLen >= XSIZ_NARRATIVE && isdigit(*(pInbuf+39)) && isdigit(*(pInbuf+28)) && !isdigit(*(pInbuf+32)) && !isdigit(*(pInbuf+36)))
   {
      memcpy(sApn, pInbuf+28, 12);
   } else if (!sApn[0])
   {
      memcpy(sTmp, pInbuf, iLen);
      memset(sApn, ' ', 16);

      // Trim both leading & trailing spaces
      myBTrim(sTmp, iLen);

      // Reduce double space to single one
      iRet = blankRem(sTmp);
      if (iRet < 12)
         return -1;              // Bad APN

      // If last 12 bytes is APN
      if (isdigit(sTmp[iRet-1]) && isdigit(sTmp[iRet-12]) && !isdigit(sTmp[iRet-8]) && !isdigit(sTmp[iRet-4]))
         memcpy(sApn, &sTmp[iRet-12], 12);

      // If first and 12th bytes are digit and third digit is space or '-'
      else if (isdigit(sTmp[0]) && isdigit(sTmp[11]) && !isdigit(sTmp[4]))
         memcpy(sApn, sTmp, 12);
      else
      {
         // Search for first token with digit
         pNdx = &sTmp[0];
         while (*pNdx >= '0' && (pTmp = strchr(pNdx, ' ')))
         {
            pTmp++;
            if (strlen(pTmp) < 12)
               break;
            if (isdigit(*pTmp) && isdigit(*(pTmp+5)) && isdigit(*(pTmp+9)))
            {
               memcpy(sApn, pTmp, 12);
               break;
            } 

            pNdx = pTmp;
         }
      }
   }

   // Find 9999 999 999
   if (isdigit(sApn[0]) && !isdigit(sApn[4]) && isdigit(sApn[5]) && !isdigit(sApn[8]) && isdigit(sApn[9]))
   {
      iRet = sprintf(pOutbuf, "%.4s%.3s%.3s", sApn, &sApn[5], &sApn[9]);
   } else if (((iRet=strlen(sApn)) == 10) && isdigit(sApn[0]))
   {
      //iRet = sprintf(pOutbuf, "%.4s%.3s%.3s", sApn, &sApn[4], &sApn[7]);
      strcpy(pOutbuf, sApn);
   } else
   {
      iRet = 0;
      *pOutbuf = 0;
   }
   return iRet;
}

/****************************** Lax_ParseXrefRec ****************************
 *
 * Extract all records from Cross Ref Roll (8/23/2013)
 *
 ****************************************************************************/

int Lax_ParseXrefRec(char *pOutbuf, char *pInbuf)
{
   XREF_REC *pRec;
   int      iRet, lTmp, iTmp;
   char     acTmp[256], acTmpLgl[64], acLegal[256], acAltApn[32], *pTmp;

   pRec = (XREF_REC *)pInbuf;

   lTmp = atoin(pRec->APN, 4);
   if (lTmp < 1000 || lTmp > 8999)
      return -1;

   // Clear output buffer
   memset(pOutbuf, ' ', iRecLen);

   // APN
   memcpy(pOutbuf+OFF_APN_S, pRec->APN, RSIZ_APN);
   memcpy(pOutbuf+OFF_CO_NUM, "19LAX", 5);

   // Format APN
   iRet = formatApn(pRec->APN, acTmp, &myCounty);
   memcpy(pOutbuf+OFF_APN_D, acTmp, iRet);

   // Legal Desc
   if (pRec->LglDesc1[0] > ' ')
   {
      sprintf(acLegal, "%.*s %.*s %.*s %.*s %.*s %.*s", XSIZ_LEGAL, pRec->LglDesc1, 
         XSIZ_LEGAL, pRec->LglDesc2, XSIZ_LEGAL, pRec->LglDesc3, XSIZ_LEGAL, pRec->LglDesc4, 
         XSIZ_LEGAL, pRec->LglDesc5, XSIZ_LEGAL, pRec->Narrative);
      iTmp = blankRem(acLegal);
      iTmp = replBadChar(acLegal, ' ', iTmp);
      iTmp = updateLegal(pOutbuf, acLegal);
      if (iTmp > iMaxLegal)
         iMaxLegal = iTmp;

      memcpy(acTmpLgl, pRec->LglDesc1, XSIZ_LEGAL);
      myTrim(acTmpLgl, XSIZ_LEGAL);
   } else
   {
      acLegal[0] = 0;
      acTmpLgl[0] = 0;
   }

   // Find Alt-Apn
   memcpy(acTmp, pRec->Narrative, XSIZ_NARRATIVE);
   acTmp[XSIZ_NARRATIVE] = 0;
   remUChar((unsigned char *)acTmp, 0x8D, XSIZ_NARRATIVE);
   remUChar((unsigned char *)acTmp, 0xD9, XSIZ_NARRATIVE);

   //if (pTmp = strchr(acTmp, ','))
   //   *pTmp = 0;

   //iRet = Lax_FormatAltApn(acAltApn, pRec->Narrative, NULL, XSIZ_NARRATIVE);
   iRet = Lax_FormatAltApn(acAltApn, acTmp, NULL, strlen(acTmp));
   if (iRet != 10 && acLegal[0] > ' ')
   {
      iTmp = strlen(acLegal);
      if (iTmp > XSIZ_NARRATIVE)
      {
         pTmp = strcpy(acTmp, &acLegal[iTmp-XSIZ_NARRATIVE]);
         //iTmp = XSIZ_NARRATIVE;
      } else
         pTmp = strcpy(acTmp, &acLegal[0]);
      replChar(acTmp, '-', ' ');
      iTmp = blankRem(acTmp);
      iRet = Lax_FormatAltApn(acAltApn, acTmp, acTmpLgl, iTmp);
   }
   if (iRet == 10)
   {
      memcpy(pOutbuf+OFF_ALT_APN, acAltApn, iRet);

      // Create MapLink
      iRet = formatMapLink(acAltApn, acTmp, &myCounty);
      memcpy(pOutbuf+OFF_MAPLINK, acTmp, iRet);
   } else if (iRet > 0)
      LogMsg("*** WARNING: Bad AltApn: ", acAltApn);       

   // Create index map link
   //if (getIndexPage(acTmp, acTmp1, &myCounty))
   //   memcpy(pOutbuf+OFF_IMAPLINK, acTmp1, iRet);

   // Year assessed
   memcpy(pOutbuf+OFF_YR_ASSD, myCounty.acYearAssd, 4);

   // HO Exempt
   lTmp = atoin(pRec->HO_Exe, XSIZ_HO_EXE);
   if (lTmp > 0)
      *(pOutbuf+OFF_HO_FL) = '1';      // 'Y'
   else
      *(pOutbuf+OFF_HO_FL) = '2';      // 'N'

   // Exemp total
   lTmp += atoin(pRec->RE_Exe, XSIZ_RE_EXE);
   lTmp += atoin(pRec->PP_Exe, XSIZ_PP_EXE);
   lTmp += atoin(pRec->Fixt_Exe, XSIZ_FIXT_EXE);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*u", SIZ_EXE_TOTAL, lTmp);
      memcpy(pOutbuf+OFF_EXE_TOTAL, acTmp, SIZ_EXE_TOTAL);
   }

   // Land
   long lLand = atoin(pRec->LandVal, XSIZ_LAND);
   if (lLand > 0)
      sprintf(acTmp, "%*u", SIZ_LAND, lLand);
   else
      strcpy(acTmp, BLANK32);
   memcpy(pOutbuf+OFF_LAND, acTmp, SIZ_LAND);

   // Improve
   long lImpr = atoin(pRec->ImprVal, XSIZ_IMPR);
   if (lImpr > 0)
      sprintf(acTmp, "%*u", SIZ_IMPR, lImpr);
   else
      strcpy(acTmp, BLANK32);
   memcpy(pOutbuf+OFF_IMPR, acTmp, SIZ_IMPR);

   // Other value
   long lOther = atoin(pRec->Other_Val, XSIZ_OTHER_VAL);
   long lFixture = atoin(pRec->Fixt_Val, XSIZ_FIXT_VAL);
   long lPers = atoin(pRec->PP_Val, XSIZ_PP_MH);
   lTmp = lPers + lFixture + lOther;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*u", SIZ_OTHER, lTmp);
      memcpy(pOutbuf+OFF_OTHER, acTmp, SIZ_OTHER);

      if (lOther > 0)
      {
         iTmp = sprintf(acTmp, "%d", lOther);
         memcpy(pOutbuf+OFF_OTH_IMPR, acTmp, iTmp);
      }
      if (lFixture > 0)
      {
         iTmp = sprintf(acTmp, "%d", lFixture);
         memcpy(pOutbuf+OFF_FIXTR, acTmp, iTmp);
      }
      if (lPers > 0)
      {
         iTmp = sprintf(acTmp, "%d", lPers);
         memcpy(pOutbuf+OFF_PERSPROP, acTmp, iTmp);
      }
   }

   // Gross total
   lTmp += lLand+lImpr;
   if (lTmp > 0)
      sprintf(acTmp, "%*u", SIZ_GROSS, lTmp);
   else
      strcpy(acTmp, BLANK32);
   memcpy(pOutbuf+OFF_GROSS, acTmp, SIZ_GROSS);

   // Ratio
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*u", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
      memcpy(pOutbuf+OFF_RATIO, acTmp, SIZ_RATIO);
   }

   // TRA
   memcpy(pOutbuf+OFF_TRA, pRec->TRA, XSIZ_TRA);

   // UseCode
   memcpy(acTmp, pRec->UseCode, XSIZ_USECODE);
   iTmp = strlen(myLTrim(acTmp, XSIZ_USECODE));
   if (iTmp > 0)
   {
      memcpy(pOutbuf+OFF_USE_CO, acTmp, iTmp);
      updateStdUse(pOutbuf+OFF_USE_STD, acTmp, iTmp, pOutbuf);
   } else
      memcpy(pOutbuf+OFF_USE_STD, USE_UNASGN, SIZ_USE_STD);

   // Recording info
   lTmp  = atoin(pRec->DocDate, XSIZ_DOCDATE);
   if (lTmp > 19000000)
      memcpy(pOutbuf+OFF_TRANSFER_DT, pRec->DocDate, RSIZ_DOCDATE);

   // Owners
   try 
   {
      if (pRec->Name1[0] > ' ')
         Lax_MergeXrefOwner(pOutbuf, pRec->Name1, pRec->Name2, pRec->Spcl_Name);
   } catch (...)
   {
      iRet = 0;
      LogMsg("??? Error catched in Lax_MergeXrefOwner() where APN=%.10s", pOutbuf);
   }

   // Mailing address
   Lax_MergeXMAdr(pOutbuf, pInbuf);

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "8940435615", 10))
   //   iTmp = 0;
#endif

   // Situs address
   Lax_MergeXSAdr(pOutbuf, pInbuf, acLegal);

   // Delinquent year
   iTmp = atoin(pRec->Sold2St, XSIZ_SOLD2ST);
   if (iTmp > 1900)
      memcpy(pOutbuf+OFF_DEL_YR, pRec->Sold2St, SIZ_DEL_YR);

   // Zoning
   if (pRec->Zone[0] > ' ')
   {
      memcpy(pOutbuf+OFF_ZONE, pRec->Zone, SIZ_ZONE);
      //if (*(pOutbuf+OFF_ZONE_X1) == ' ')
      //   vmemcpy(pOutbuf+OFF_ZONE_X1, pRec->Zone, SIZ_ZONE_X1, RSIZ_ZONE);
   }

   // BldgSqft
   lTmp = atoin(pRec->BldgSqft, XSIZ_BLDGSQFT);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*u", SIZ_BLDG_SF, lTmp);
      memcpy(pOutbuf+OFF_BLDG_SF, acTmp, SIZ_BLDG_SF);
   }

   // YrBlt
   iTmp = atoin(pRec->YrBlt, XSIZ_YRBLT);
   if (iTmp > 1900)
      memcpy(pOutbuf+OFF_YR_BLT, pRec->YrBlt, XSIZ_YRBLT);

   // YrEff
   iTmp = atoin(pRec->YrEff, XSIZ_YRBLT);
   if (iTmp > 1900)
      memcpy(pOutbuf+OFF_YR_BLT, pRec->YrEff, XSIZ_YRBLT);

   // Set region 3
   *(pOutbuf+OFF_MAPDIV_LA) = '3';

   return 0;
}

/******************************** Lax_LoadXref ********************************
 *
 * Extract MH record from Xref file and create R01 records to be appended to LAX roll
 * PI/Fixtures:      890xxxxxxx-894xxxxxxx
 * MH inside park:   8950001001-8950998999
 * MH outside park:  8950999001-8950999999
 * MH (lease):       8951927001-8951927999
 *
 ******************************************************************************/

int Lax_ExtrMH(char *pInfile)
{
   char  acTmpFile[_MAX_PATH], acInbuf[2048], acOutbuf[2048], *pTmp;
   FILE  *fdIn, *fdOut;
   long  lCnt=0, lOut=0, iRet;

   // Output file for MH parcels
   sprintf(acTmpFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "MBH");

   if (_access(pInfile, 0))
   {
      LogMsg("***** Lax_ExtrMH(): Missing input file: %s", pInfile);
      return -1;
   }

   // Open input file
   LogMsg("Open input file %s", pInfile);
   fdIn = fopen(pInfile, "r");
   if (fdIn == NULL)
   {
      LogMsg("***** Error opening input file: %s\n", pInfile);
      return -2;
   }

   // Open output file
   LogMsg("Create output MH file %s", acTmpFile);
   fdOut = fopen(acTmpFile, "wb");
   if (fdOut == NULL)
   {
      LogMsg("***** Error creating output file: %s\n", acTmpFile);
      return -3;
   }

   // Convert loop
   while (!feof(fdIn))
   {
      if (!(pTmp = fgets(acInbuf, 2048, fdIn)))
         break;

      // Format MH record - same as R01
      iRet = Lax_ParseXrefRec(acOutbuf, acInbuf);
      if (!iRet)
      {
         // Output record
         fwrite(acOutbuf, 1, iRecLen, fdOut);
         lOut++;
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdIn)
      fclose(fdIn);
   if (fdOut)
      fclose(fdOut);

   LogMsg("Total input records:        %u", lCnt);
   LogMsg("Total output records:       %u", lOut);
   LogMsg("Total records /w DBA:       %u", iDbaCnt);
   LogMsg("Total bad-city records:     %u", iBadCity);
   LogMsg("Total bad-suffix records:   %u", iBadSuffix);

   return 0;
}

/******************************** Lax_LegalExtr *****************************
 *
 * Extract legal for specific list of APN
 * Return number of records extracted. <0 if error.
 *
 ****************************************************************************/

int Lax_LegalExtr(char *pInfile, char *pOutfile)
{
   char     *pTmp, acBuf[512], acTmp[512], acApn[32], acRollRec[MAX_RECSIZE];
   char     acTmpFile[_MAX_PATH], acOutFile[_MAX_PATH];

   long     iRet, lCnt=0, lOutRecs=0, lNotFound=0;
   FILE     *fdParcel, *fdOut;
   LAX_ROLL *pRec = (LAX_ROLL *)&acRollRec[0];
   BOOL     bExtrAll;

   if (pInfile)
   {
      LogMsg0("Extract Legal for %s", pInfile);
      bExtrAll = false;

      // Sort input file
      sprintf(acTmpFile, "%s\\%s\\Parcels.srt", acTmpPath, myCounty.acCntyCode); 
      sprintf(acTmp, "S(1,%d,C,A) OMIT(1,1,C,GT,\"9\") DUPO", iApnLen);
      iRet = sortFile(pInfile, acTmpFile, acTmp);
      if (iRet <= 0)
         return 0;

      // Open parcels file
      LogMsg("Open parcel file %s", acTmpFile);
      fdParcel = fopen(acTmpFile, "r");
      if (fdParcel == NULL)
      {
         LogMsg("***** Error opening parcel file: %s\n", acTmpFile);
         return -1;
      }

      // Get first parcel rec
      pTmp = fgets((char *)&acBuf[0], 512, fdParcel);
      strcpy(acOutFile, pOutfile);
   } else
   {
      LogMsg0("Extract Legal for %s", myCounty.acCntyCode);
      bExtrAll = true;
      fdParcel = NULL;

      GetIniString("Data", "LegalTmpl", "", acTmp, _MAX_PATH, acIniFile);
      sprintf(acOutFile, acTmp, myCounty.acCntyName);
   }

   // Open roll file
   LogMsg("Open Roll file %s", acRollFile);
   fdRoll = fopen(acRollFile, "rb");
   if (fdRoll == NULL)
   {
      printf("***** Error opening roll file: %s\n", acRollFile);
      return -2;
   }

   // Create Output file
   LogMsg("Open output file %s", acOutFile);
   fdOut = fopen(acOutFile, "w");
   if (fdOut == NULL)
   {
      LogMsg("***** Error creating output legal file: %s\n", acOutFile);
      return -3;
   }

   // Output header - FIPS|APN_D|LEGAL1
   fputs("FIPS|APN_D|LEGAL\n", fdOut);

   // Merge loop
   while (true)
   {
      iRet = fread(acRollRec, 1, iRollLen, fdRoll);

      // Check for EOF
      if (iRet < iRollLen || !memcmp(acRollRec, "9999999", 7))
         break;
     
#ifdef _DEBUG      
      //if (!memcmp(acRollRec, "2079011016", 10))
      //   iRet = 0;
      //if ((iRet = findUnPrtChar(acRollRec, 390)) && iRet != 392 && (unsigned char)acRollRec[iRet] != 0x9B)
      //   iRet = 0;
#endif

      if (bExtrAll)
      {
         // Create output record
         memcpy(acTmp, pRec->LglDesc, RSIZ_LGLDESC);
         replUChar((unsigned char *)&acTmp[0], 0x9B, 0xB0, RSIZ_LGLDESC);
         blankRem(acTmp, RSIZ_LGLDESC);
         iRet = formatApn(pRec->APN, acApn, &myCounty);
         sprintf(acBuf, "06037|%s|%s\n", acApn, acTmp);   
         fputs(acBuf, fdOut);  
         lOutRecs++;
      } else
      {
ReCheck:
         iRet = memcmp(acBuf, pRec->APN, iApnLen);
         // If matched, get legal
         if (!iRet)
         {
            memcpy(acTmp, pRec->LglDesc, RSIZ_LGLDESC);
            replUChar((unsigned char *)&acTmp[0], 0x9B, 0xB0, RSIZ_LGLDESC);
            blankRem(acTmp, RSIZ_LGLDESC);
            iRet = formatApn(pRec->APN, acApn, &myCounty);
            sprintf(acBuf, "06037|%s|%s\n", acApn, acTmp);   
            fputs(acBuf, fdOut);  
            lOutRecs++;

            // Get new parcel
            pTmp = fgets((char *)&acBuf[0], 512, fdParcel);
            if (!pTmp)
               break;
         } else if (iRet < 0)
         {
            // No legal for this record
            acBuf[iApnLen] = 0;
            if (bDebug)
               LogMsg("No legal for: %s", acBuf);
            lNotFound++;

            // Get new parcel
            pTmp = fgets((char *)&acBuf[0], 512, fdParcel);
            if (!pTmp)
               break;

            lCnt++;
            goto ReCheck;
         }
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdRoll)
      fclose(fdRoll);
   if (fdParcel)
      fclose(fdParcel);
   if (fdOut)
      fclose(fdOut);

   LogMsg("Total input records:        %u", lCnt);
   LogMsg("Total output records:       %u", lOutRecs);
   LogMsg("Total records not found:    %u", lNotFound);

   return lOutRecs;
}

/******************************** Lax_ParseDelq *****************************
 *
 * Return 0 if success
 *
 *****************************************************************************/

int Lax_ParseDelq(char *pOutbuf, char *pInbuf)
{
   long     lRedDate, lDefDate;
   int      iTmp;
   double	dTaxAmt;
   char     *pTmp;

   TAXDELQ  *pOutRec = (TAXDELQ *)pOutbuf;

   // Parse input tax data
   iTokens = ParseStringIQ(pInbuf, cDelim, MAX_FLD_TOKEN, apTokens);
   if (iTokens < T04_FLDCNT)
   {
      LogMsg("***** Error: bad Delq record for: %.50s (#tokens=%d)", pInbuf, iTokens);
      return -1;
   }

   // Clear output buffer
   memset(pOutbuf, 0, sizeof(TAXDELQ));
   lRedDate=lDefDate = 0;

   // APN
   strcpy(pOutRec->Apn, apTokens[T01_APN]);

#ifdef _DEBUG
   //if (!memcmp(pOutRec->Apn, "2105001054", 10))
   //   iTmp = 0;
#endif

   // Updated date
   sprintf(pOutRec->Upd_Date, "%d", lLastTaxFileDate);

   // Default amount
   dTaxAmt = atof(apTokens[T01_BEGINBAL]);
   if (dTaxAmt > 0.0)
   {
      sprintf(pOutRec->Def_Amt, "%.2f", dTaxAmt);
      pOutRec->isDelq[0] = '1';
   }

   // Default date
   strcpy(pOutRec->Def_Date, apTokens[T01_DELQYEAR]);

   // Tax Year
   iTmp = atoin(apTokens[T01_DELQYEAR], 4);
   sprintf(pOutRec->TaxYear, "%d", iTmp-1);

   double   dDueAmt, dPaidAmt;
   dDueAmt = atof(apTokens[T01_REDAMTDUE]);
   dPaidAmt= atof(apTokens[T01_REDAMTPAID]);

   // Pen Amt
   if (*apTokens[T01_REDPEN] > '0')
   {
      dDueAmt += atof(apTokens[T01_REDPEN]);
      strcpy(pOutRec->Pen_Amt, apTokens[T01_REDPEN]);
   }

   // Fee Amt
   if (*apTokens[T01_REDFEEBAL] > '0')
   {
      dDueAmt += atof(apTokens[T01_REDFEEBAL]);
      strcpy(pOutRec->Fee_Amt, apTokens[T01_REDFEEBAL]);
   }

   // Redemption date
   if (*apTokens[T01_REDDATE] > '0')
   {
      // Redeemed
      pTmp = dateConversion(apTokens[T01_REDDATE], pOutRec->Red_Date, MM_DD_YYYY_1);
      if (!pTmp)
         LogMsg("***** Bad redemption date %s [%s]", apTokens[T01_REDDATE], pOutRec->Apn);

      pOutRec->isDelq[0] = '0';
      pOutRec->DelqStatus[0] = TAX_STAT_REDEEMED;
      strcpy(pOutRec->Red_Amt, apTokens[T01_REDAMTPAID]);      
   } else if (dPaidAmt > 0.0)
   {
      strcpy(pOutRec->Red_Amt, apTokens[T01_REDAMTPAID]);      
      if (dDueAmt == 0.0)
         pOutRec->DelqStatus[0] = TAX_STAT_REDEEMED;
      else
         pOutRec->DelqStatus[0] = TAX_STAT_ONPAYMENT;

      pOutRec->isDelq[0] = '0';
      if (*apTokens[T01_LASTPAIDDATE] > '0')
      {
         pTmp = dateConversion(apTokens[T01_LASTPAIDDATE], pOutRec->Red_Date, MM_DD_YYYY_1);
         if (!pTmp)
            LogMsg("***** Bad last paid date %s [%s]", apTokens[T01_LASTPAIDDATE], pOutRec->Apn);
      }
   } 
   
   if (dDueAmt > 0.0)
   {
      sprintf(pOutRec->Def_Amt, "%.2f", dDueAmt);
      if (dPaidAmt == 0.0)
      {
         pOutRec->DelqStatus[0] = TAX_STAT_UNPAID;
         pOutRec->isDelq[0] = '1';
      }
   } else if (dTaxAmt > 0.0 && dPaidAmt == 0.0)
   {
      sprintf(pOutRec->Def_Amt, "%.2f", dTaxAmt);
      pOutRec->DelqStatus[0] = TAX_STAT_UNPAID;
      pOutRec->isDelq[0] = '1';
   }

   // Due date
   if (*apTokens[T01_DUEDATE] > '0')
   {
      pTmp = dateConversion(apTokens[T01_DUEDATE], pOutRec->Pts_Date, MM_DD_YYYY_1);
      if (!pTmp)
         LogMsg("***** Bad last paid date %s [%s]", apTokens[T01_DUEDATE], pOutRec->Apn);
   }

   return 0;
}

/**************************** Lax_Load_TaxDelq *****************************
 *
 * Load TCTROA00.UNPACKED.FILE1 & TCTROA00.UNPACKED.FILE2
 * Create TaxDelq
 *
 ***************************************************************************/

int Lax_Load_TaxDelq(bool bImport)
{
   char     acFile1[_MAX_PATH], acFile2[_MAX_PATH], acOutFile[_MAX_PATH], acDelqFile[_MAX_PATH];
   char     acBuf[512], acRec[512], *pTmp;
   int      iRet, lCnt=0, lOut=0, iDrop=0;
   FILE     *fdIn, *fdOut;

   LogMsg0("Loading Tax Delinquent");

   // Concat file1 & file2
   GetIniString(myCounty.acCntyCode, "TaxDelq", "", acDelqFile, _MAX_PATH, acIniFile);
   sprintf(acRec, "%s.%s", acDelqFile, acToday);
   if (_access(acRec, 0))
   {
      sprintf(acFile1, "%s1", acDelqFile);
      sprintf(acFile2, "%s2", acDelqFile);
      LogMsg("Append %s+%s to %s", acFile1, acFile2, acDelqFile);

      lLastTaxFileDate = getFileDate(acFile1);
      iRet = CopyFile(acFile1, acDelqFile, false);
      iRet = appendTxtFile(acFile2, acDelqFile);
      iRet = touch(acFile1, acDelqFile);
   } else
   {
      strcpy(acDelqFile, acRec);
      lLastTaxFileDate = getFileDate(acDelqFile);
   }

   // Open input file
   LogMsg("Open tax delinquent file %s", acDelqFile);
   fdIn = fopen(acDelqFile, "r");
   if (fdIn == NULL)
   {
      LogMsg("***** Error opening Secured tax file: %s\n", acDelqFile);
      return -2;
   }  

   // Drop header
   pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdIn);

   // Open output file
   NameTaxCsvFile(acOutFile, myCounty.acCntyCode, "Delq");
   LogMsg("Open output file %s", acOutFile);
   fdOut = fopen(acOutFile, "w");
   if (fdOut == NULL)
   {
      LogMsg("***** Error creating output file: %s\n", acOutFile);
      return -4;
   }

   // Merge loop 
   while (!feof(fdIn))
   {
      pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdIn);
      if (!pTmp || *pTmp < ' ')
         break;
      lCnt++;

      // Take record type 1 only
      // 1 = Group record, summary of delinquency per parcel
      // 2 = Event
      // 3 = Assessment record
      // 4 = Subject to Power to sell
      // 9 = Summary record (county total)
      if (acRec[1] != '1')
      {
         iDrop++;
         continue;
      }

      // Create new R01 record
      iRet = Lax_ParseDelq(acBuf, acRec);
      if (!iRet)
      {
         // Create delimited record
         Tax_CreateDelqCsv(acRec, (TAXDELQ *)&acBuf);

         // Output record			
         lOut++;
         fputs(acRec, fdOut);
      } else
      {
         LogMsg("---> Drop Delq record %d [%.40s]", lCnt, acRec); 
         iDrop++;
      }

      if (!(lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdIn)
      fclose(fdIn);
   if (fdOut)
      fclose(fdOut);

   LogMsg("Total records processed:    %u", lCnt);
   LogMsg("      output records:       %u", lOut);
   LogMsg("     records dropped:       %u\n", iDrop);

   // Rename Delq file
   sprintf(acRec, "%s.%s", acDelqFile, acToday);
   if (!_access(acRec, 0))
      DeleteFile(acRec);

   LogMsg("Rename %s ===> %s", acDelqFile, acRec);
   rename(acDelqFile, acRec);

   // Import into SQL
   if (bImport)
      iRet = doTaxImport(myCounty.acCntyCode, TAX_DELINQUENT);
   else
      iRet = 0;

   return iRet;
}

/************************* Lax_Load_DefaultTaxRoll *************************
 *
 * Load "Secured Defaulted Tax Roll.csv"
 * Create TaxDelq
 *
 ***************************************************************************/

int Lax_ParseDefaultTaxRoll(char *pOutbuf, char *pInbuf)
{
   long     lRedDate, lDefDate;
   int      iTmp;
   double	dTaxAmt;
   TAXDELQ  *pOutRec = (TAXDELQ *)pOutbuf;

   // Parse input tax data
   iTokens = ParseStringIQ(pInbuf, cDelim, MAX_FLD_TOKEN, apTokens);
   if (iTokens < T04_FLDCNT)
   {
      LogMsg("***** Error: bad Delq record for: %.50s (#tokens=%d)", pInbuf, iTokens);
      return -1;
   }

   // Clear output buffer
   memset(pOutbuf, 0, sizeof(TAXDELQ));
   lRedDate=lDefDate = 0;

   // APN
   strcpy(pOutRec->Apn, apTokens[T01_APN]);

#ifdef _DEBUG
   //if (!memcmp(pOutRec->Apn, "2105001054", 10))
   //   iTmp = 0;
#endif

   // Updated date
   sprintf(pOutRec->Upd_Date, "%d", lLastTaxFileDate);

   // Default amount
   dTaxAmt = atof(apTokens[T01_BEGINBAL]);
   if (dTaxAmt > 0.0)
   {
      sprintf(pOutRec->Def_Amt, "%.2f", dTaxAmt);
      pOutRec->isDelq[0] = '1';
   }

   // Default date
   strcpy(pOutRec->Def_Date, apTokens[T01_DELQYEAR]);

   // Tax Year
   iTmp = atoin(apTokens[T01_DELQYEAR], 4);
   sprintf(pOutRec->TaxYear, "%d", iTmp-1);

   double   dDueAmt, dPaidAmt;
   dDueAmt = atof(apTokens[T01_REDAMTDUE]);
   dPaidAmt= atof(apTokens[T01_REDAMTPAID]);

   // Pen Amt
   if (*apTokens[T01_REDPEN] > '0')
   {
      dDueAmt += atof(apTokens[T01_REDPEN]);
      strcpy(pOutRec->Pen_Amt, apTokens[T01_REDPEN]);
   }

   // Fee Amt
   if (*apTokens[T01_REDFEEBAL] > '0')
   {
      dDueAmt += atof(apTokens[T01_REDFEEBAL]);
      strcpy(pOutRec->Fee_Amt, apTokens[T01_REDFEEBAL]);
   }

   // Redemption date
   if (*apTokens[T01_REDDATE] > '0')
   {
      // Redeemed
      dateConversion(apTokens[T01_REDDATE], pOutRec->Red_Date, MM_DD_YYYY_2);
      pOutRec->isDelq[0] = '0';
      pOutRec->DelqStatus[0] = TAX_STAT_REDEEMED;
      strcpy(pOutRec->Red_Amt, apTokens[T01_REDAMTPAID]);      
   } else if (dPaidAmt > 0.0)
   {
      strcpy(pOutRec->Red_Amt, apTokens[T01_REDAMTPAID]);      
      if (dDueAmt == 0.0)
         pOutRec->DelqStatus[0] = TAX_STAT_REDEEMED;
      else
         pOutRec->DelqStatus[0] = TAX_STAT_ONPAYMENT;

      pOutRec->isDelq[0] = '0';
      if (*apTokens[T01_LASTPAIDDATE] > '0')
      {
         dateConversion(apTokens[T01_LASTPAIDDATE], pOutRec->Red_Date, MM_DD_YYYY_2);
      }
   } 
   
   if (dDueAmt > 0.0)
   {
      sprintf(pOutRec->Def_Amt, "%.2f", dDueAmt);
      if (dPaidAmt == 0.0)
      {
         pOutRec->DelqStatus[0] = TAX_STAT_UNPAID;
         pOutRec->isDelq[0] = '1';
      }
   } else if (dTaxAmt > 0.0 && dPaidAmt == 0.0)
   {
      sprintf(pOutRec->Def_Amt, "%.2f", dTaxAmt);
      pOutRec->DelqStatus[0] = TAX_STAT_UNPAID;
      pOutRec->isDelq[0] = '1';
   }

   // Due date
   if (*apTokens[T01_DUEDATE] > '0')
   {
      dateConversion(apTokens[T01_DUEDATE], pOutRec->Pts_Date, MM_DD_YYYY_2);
      //strcpy(pOutRec->Pts_Date, apTokens[T01_DUEDATE]);
   }
   return 0;
}

int Lax_Load_DefaultTaxRoll(bool bImport)
{
   char     acOutFile[_MAX_PATH], acDelqFile[_MAX_PATH];
   char     acBuf[512], acRec[512], *pTmp;
   int      iRet, lCnt=0, lOut=0;
   FILE     *fdIn, *fdOut;

   LogMsg0("Loading Tax Delinquent");

   // Get tax delq file
   GetIniString(myCounty.acCntyCode, "TaxDelq", "", acDelqFile, _MAX_PATH, acIniFile);
   lLastTaxFileDate = getFileDate(acDelqFile);

   // Open input file
   LogMsg("Open tax delinquent file %s", acDelqFile);
   fdIn = fopen(acDelqFile, "r");
   if (fdIn == NULL)
   {
      LogMsg("***** Error opening Secured tax file: %s\n", acDelqFile);
      return -2;
   }  

   // Open output file
   NameTaxCsvFile(acOutFile, myCounty.acCntyCode, "Delq");
   LogMsg("Open output file %s", acOutFile);
   fdOut = fopen(acOutFile, "w");
   if (fdOut == NULL)
   {
      LogMsg("***** Error creating output file: %s\n", acOutFile);
      return -4;
   }

   // Drop header
   pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdIn);

   // Merge loop 
   while (!feof(fdIn))
   {
      pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdIn);
      if (!pTmp || *pTmp < ' ')
         break;

      // Take record type 1 only
      // 1 = Group record, summary of delinquency per parcel
      // 2 = Event
      // 3 = Assessment record
      // 4 = Subject to Power to sell
      // 9 = Summary record (county total)
      if (acRec[1] != '1')
         continue;

      // Create new R01 record
      iRet = Lax_ParseDefaultTaxRoll(acBuf, acRec);
      if (!iRet)
      {
         // Create delimited record
         Tax_CreateDelqCsv(acRec, (TAXDELQ *)&acBuf);

         // Output record			
         lOut++;
         fputs(acRec, fdOut);
      } else
      {
         LogMsg("---> Drop Delq record %d [%.40s]", lCnt, acRec); 
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdIn)
      fclose(fdIn);
   if (fdOut)
      fclose(fdOut);

   LogMsg("Total records processed:    %u", lCnt);
   LogMsg("Total output records:       %u", lOut);

   // Import into SQL
   if (bImport)
      iRet = doTaxImport(myCounty.acCntyCode, TAX_DELINQUENT);
   else
      iRet = 0;

   return iRet;
}

/***************************** Lax_ParseTaxBase ******************************
 *
 * Parse record to Base & Detail
 *
 *****************************************************************************/

int Lax_ParseTaxBase(char *pBaseBuf)
{
   int      iTmp;
   double   dTmp;

   TAXBASE   *pBaseRec = (TAXBASE *)pBaseBuf;

   // Clear output buffer
   memset(pBaseBuf, 0, sizeof(TAXBASE));

   // APN
   strcpy(pBaseRec->Apn, apTokens[SEC_APN]);

   // TRA
   strcpy(pBaseRec->TRA, apTokens[SEC_TRA]);

   // Tax Year
   iTmp = 2000 + atoin(apTokens[SEC_PRCLHIST], 2);
   // 20240117 - if (iTmp != lTaxYear)
   //   return 1;
   sprintf(pBaseRec->TaxYear, "%d", iTmp);

   // Updated date
   sprintf(pBaseRec->Upd_Date, "%d", lLastTaxFileDate);

   // Paid Status
   // 00 = Unpaid
   // 01 = Personal property paid
   // 02 = 1st installment only paid
   // 03 = Total paid, for both installments
   // 04 = 1st installment, 2nd adjusted
   // 05 = 1st and 2nd paid, 2nd adjusted
   // 06 = 1st paid, 2nd cancelled
   // 07 = Personal property paid, 2nd adjusted
   // 08 = 1st partially paid
   // 09 = 1st paid, 2nd partially paid 
   pBaseRec->Inst1Status[0] = TAX_BSTAT_UNPAID;
   pBaseRec->Inst2Status[0] = TAX_BSTAT_UNPAID;

   iTmp = atoi(apTokens[SEC_PAIDSTATUS]);
   switch (iTmp)
   {
      case 0:
         break;
      case 2:
         pBaseRec->Inst1Status[0] = TAX_BSTAT_PAID;
         break;
      case 4:
      case 6:
         pBaseRec->Inst1Status[0] = TAX_BSTAT_PAID;
         pBaseRec->Inst2Status[0] = TAX_BSTAT_CANCEL;
         break;
      case 8:
         pBaseRec->Inst1Status[0] = TAX_BSTAT_INSTPMNT;
         break;
      case 9:
         pBaseRec->Inst1Status[0] = TAX_BSTAT_PAID;
         pBaseRec->Inst2Status[0] = TAX_BSTAT_INSTPMNT;
         break;
      case 3:
      case 5:
         pBaseRec->Inst1Status[0] = TAX_BSTAT_PAID;
         pBaseRec->Inst2Status[0] = TAX_BSTAT_PAID;
         break;
   }

#ifdef _DEBUG
   //if (!memcmp(pBaseRec->Apn, "9014800001", 10))
   //   iTmp = 0;
#endif

   // Check for Tax amount - V99
   double dTax1 = atof(apTokens[SEC_INSTAMT1]);
   double dTax2 = atof(apTokens[SEC_INSTAMT2]);
   double dTotalTax = dTax1+dTax2;
   if (dTotalTax > 0.0)
   {
      sprintf(pBaseRec->TotalTaxAmt, "%.2f", dTotalTax);

      sprintf(pBaseRec->TaxAmt1, "%.2f", dTax1);
      sprintf(pBaseRec->TaxAmt2, "%.2f", dTax2);
   }

   // Due
   double dDue1 = atof(apTokens[SEC_DUEAMT1]);
   double dDue2 = atof(apTokens[SEC_DUEAMT2]);
   strcpy(pBaseRec->TotalDue, apTokens[SEC_TOTALTAXDUE]);

   // Penalty
   double dPen1 = atof(apTokens[SEC_PENAMT1]);
   double dPen2 = atof(apTokens[SEC_PENAMT2]);
   if (dPen1 > 0.0)
      strcpy(pBaseRec->PenAmt1, apTokens[SEC_PENAMT1]);
   if (dPen2 > 0.0)
      strcpy(pBaseRec->PenAmt2, apTokens[SEC_PENAMT2]);

   // Paid
   if (*apTokens[SEC_PAIDDATE1] > ' ')
   {
      sprintf(pBaseRec->PaidDate1, "20%s", apTokens[SEC_PAIDDATE1]);
      dPen1 = atof(apTokens[SEC_PAIDPENAMT1]);
      dTmp = dPen1 + dTax1;
      sprintf(pBaseRec->PaidAmt1, "%.2f", dTmp);
      if (*apTokens[SEC_PAIDDATE2] > ' ')
      {
         sprintf(pBaseRec->PaidDate2, "20%s", apTokens[SEC_PAIDDATE2]);
         dPen2 = atof(apTokens[SEC_PAIDPENAMT2]);
         dTmp = dPen2 + dTax2;
         sprintf(pBaseRec->PaidAmt2, "%.2f", dTmp);
      }
   } else
   {
      InstDueDate(pBaseRec->DueDate1, 1, lTaxYear);
      InstDueDate(pBaseRec->DueDate2, 2, lTaxYear);
   }

   // Total fee
   double dCost = atof(apTokens[SEC_COSTDUE]);
   if (dCost > 0.0)
      sprintf(pBaseRec->TotalFees, "%.2f", dCost);

   // Is delinquent 
   if (*apTokens[SEC_TAXSTATUS] > '0')
   {
      pBaseRec->isDelq[0] = '1';
      if (*apTokens[SEC_TAXSTATUS] == '1')
         pBaseRec->DelqStatus[0] = TAX_STAT_SOLD2AUCTION;
      else if (*apTokens[SEC_TAXSTATUS] == '2')
         pBaseRec->DelqStatus[0] = TAX_STAT_POWER2SELL;
      else
         pBaseRec->DelqStatus[0] = TAX_STAT_UNPAID;
   }

   // Delq year
   if (*apTokens[SEC_DELQYEAR] > '0')
   {
      // This should be ok for 1970-2069
      if (*apTokens[SEC_DELQYEAR] > '6')
         sprintf(pBaseRec->DelqYear, "19%s", apTokens[SEC_DELQYEAR]);
      else
         sprintf(pBaseRec->DelqYear, "20%s", apTokens[SEC_DELQYEAR]);
   } 

   // Supplemental record?
   //char *pTmp = apTokens[SEC_PRCLHIST]+2;

   if (!memcmp(apTokens[SEC_PRCLHIST]+2, "000", 3))
   {
      pBaseRec->isSecd[0] = '1';
      pBaseRec->BillType[0] = BILLTYPE_SECURED;
   } else 
   {
      pBaseRec->isSupp[0] = '1';

      if (!memcmp(apTokens[SEC_RCN], "000", 3))
         pBaseRec->BillType[0] = BILLTYPE_SECURED_SUPPL;
      else
         pBaseRec->BillType[0] = BILLTYPE_ROLL_CORRECTION;
   }

   // Bill number - LAX doesn't provide bill number, we use APN and Seq number instead
   sprintf(pBaseRec->BillNum, "%s-%s", apTokens[SEC_APN], apTokens[SEC_PRCLHIST]);

   return 0;
}

int Lax_ParseTaxDetail(FILE *fdDetail, FILE *fdAgency)
{
   char     acTmp[512], acOutbuf[512];
   int      iIdx, iCnt, iTmp;
   double   dTmp, dTmp1;
   TAXDETAIL *pDetail = (TAXDETAIL *)acOutbuf;
   TAXAGENCY sAgency;

   // Clear output buffer
   memset(acOutbuf, 0, sizeof(TAXDETAIL));
   memset((char *)&sAgency, 0, sizeof(TAXAGENCY));

   // APN
   strcpy(pDetail->Apn, apTokens[SEC_APN]);

   // Bill number - LAX doesn't provide bill number, we use APN and Seq number instead
   sprintf(pDetail->BillNum, "%s-%s", apTokens[SEC_APN], apTokens[SEC_PRCLHIST]);

   // Tax Year
   sprintf(pDetail->TaxYear, "20%.2s", apTokens[SEC_PRCLHIST]);

   // General tax
   dTmp  = atof(apTokens[SEC_TAXTYPE1]);
   dTmp += atof(apTokens[SEC_TAXTYPE2]);
   dTmp += atof(apTokens[SEC_TAXTYPE3]);
   dTmp += atof(apTokens[SEC_TAXTYPE4]);
   dTmp += atof(apTokens[SEC_TAXTYPE8]);
   if (dTmp > 0.0)
   {
      strcpy(pDetail->TaxCode, "00001");
      strcpy(pDetail->TaxDesc, "GENERAL TAX");
      sprintf(pDetail->TaxAmt, "%.2f", dTmp);
      pDetail->TC_Flag[0] = '1';

      Tax_CreateDetailCsv(acTmp, pDetail);
      fputs(acTmp, fdDetail);

      // Generate Agency record
      strcpy(sAgency.Agency, "GENERAL TAX");
      strcpy(sAgency.Code, pDetail->TaxCode);
      sAgency.TC_Flag[0] = '1';
      Tax_CreateAgencyCsv(acTmp, &sAgency);
      fputs(acTmp, fdAgency);
   }

   sAgency.TC_Flag[0] = 0;
   pDetail->TC_Flag[0] = 0;

   // Loop through tax items
   iIdx = SEC_TAXCODE1;
   for (iCnt = 0; iCnt < SEC_MAX_TAXITEMS; iCnt++)
   {      
      // Agency 
      strcpy(pDetail->TaxCode, apTokens[iIdx]);
      // Tax Amt
      dTmp  = atof(apTokens[iIdx+2]);

      // Tax Desc
      if (*apTokens[iIdx+1] > ' ')
      {
         iTmp = strlen(apTokens[iIdx+1]);
         if (isdigit(*(apTokens[iIdx+1]+iTmp-2) ))
         {
            iTmp -= 2;
            *(apTokens[iIdx+1]+iTmp) = 0;
         }
         strcpy(pDetail->TaxDesc, apTokens[iIdx+1]);
      } else
      {
         LogMsg("??? New Agency: %s APN=%s", apTokens[iIdx], pDetail->Apn);
         if (dTmp < 0)
            sprintf(pDetail->TaxDesc, "%s - EXEMPTION", apTokens[iIdx]);
         else
            sprintf(pDetail->TaxDesc, "%s - SPECIAL TAX", apTokens[iIdx]);
      }

      // Prorated Amt
      dTmp1 = atof(apTokens[iIdx+3]);
      if (dTmp)
      {
#ifdef _DEBUG
         //if (dTmp < 0)
         //   LogMsg(">>> Negative tax: %s - %s, %s", apTokens[SEC_APN], apTokens[iIdx], apTokens[iIdx+1]);
#endif
         sprintf(pDetail->TaxAmt, "%.2f", dTmp);
         if (bDebug && dTmp != dTmp1)
            LogMsg("??? TaxAmt=%.2f, ProratedAmt=%.2f APN=%s", dTmp, dTmp1, pDetail->Apn);

         // Generate csv line and write to file
         Tax_CreateDetailCsv(acTmp, pDetail);
         fputs(acTmp, fdDetail);

         // Generate Agency record
         strcpy(sAgency.Agency, pDetail->TaxDesc);
         strcpy(sAgency.Code, pDetail->TaxCode);
         Tax_CreateAgencyCsv(acTmp, &sAgency);
         fputs(acTmp, fdAgency);
      } else
         break;

      iIdx += 4;
   }

   return 0;
}

/**************************** Lax_Load_TaxBase *****************************
 *
 * Loading TCSTN960.UNPACKED.FILE
 * Create TaxBase, TaxDetail, TaxAgency
 *
 ***************************************************************************/

int Lax_Load_TaxBase(bool bImport)
{
   char      *pTmp, acBase[1024], acDetail[1024], acRec[MAX_RECSIZE],
             acBaseFile[_MAX_PATH], acDetailFile[_MAX_PATH], acTmpFile[_MAX_PATH], acAgencyFile[_MAX_PATH];

   int       iRet;
   long      lOut=0, lCnt=0, lDrop=0;
   FILE      *fdBase, *fdDetail, *fdAgency, *fdIn;
   TAXBASE   *pBase = (TAXBASE *)acBase;
   TAXDETAIL *pDetail = (TAXDETAIL *)acDetail;

   LogMsg0("Loading Tax Base & Detail");

   NameTaxCsvFile(acBaseFile, myCounty.acCntyCode, "Base");
   NameTaxCsvFile(acDetailFile, myCounty.acCntyCode, "Items");
   NameTaxCsvFile(acAgencyFile, myCounty.acCntyCode, "Agency");

   pTmp = strrchr(acBaseFile, '\\');
   *pTmp = 0;
   if (_access(acBaseFile, 0))
      _mkdir(acBaseFile);
   *pTmp = '\\';

   // Open input file
   GetIniString(myCounty.acCntyCode, "TaxDetail", "", acTmpFile, _MAX_PATH, acIniFile);
   lLastTaxFileDate = getFileDate(acTmpFile);
   // Only process if new tax file
   iRet = isNewTaxFile(NULL, myCounty.acCntyCode);
   if (iRet <= 0)
   {
      lLastTaxFileDate = 0;
      return iRet;
   }

   LogMsg("Open tax file %s for input", acTmpFile);
   fdIn = fopen(acTmpFile, "r");
   if (fdIn == NULL)
   {
      LogMsg("***** Error opening Current tax file: %s\n", acTmpFile);
      return -2;
   }  


   // Open base file
   LogMsg("Open Base file %s for output", acBaseFile);
   fdBase = fopen(acBaseFile, "w");
   if (fdBase == NULL)
   {
      LogMsg("***** Error creating base file: %s\n", acBaseFile);
      return -4;
   }

   // Open Detail file
   LogMsg("Open Detail file %s for output", acDetailFile);
   fdDetail = fopen(acDetailFile, "w");
   if (fdDetail == NULL)
   {
      LogMsg("***** Error creating Detail output file: %s\n", acDetailFile);
      return -4;
   }

   // Open Agency file
   sprintf(acTmpFile, "%s\\%s\\%s_Agency.tmp", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   LogMsg("Open Agency file %s for output", acTmpFile);
   fdAgency = fopen(acTmpFile, "w");
   if (fdAgency == NULL)
   {
      LogMsg("***** Error creating Agency file: %s\n", acTmpFile);
      return -4;
   }

   // Merge loop 
   while (!feof(fdIn))
   {
      pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdIn);
      if (!pTmp || *pTmp == '0')
         break;

      // Parse input record
      iTokens = ParseStringIQ(acRec, cDelim, 256, apTokens);
      if (iTokens < SEC_COMBINELIENKEY)
      {
         if (iTokens > SEC_APN)
            LogMsg("*** Bad record: %s (%d)", apTokens[SEC_APN], iTokens);
         continue;
      }

      // Create new base record
      iRet = Lax_ParseTaxBase(acBase);
      if (!iRet)
      {
         // Create detail & agency records
         Lax_ParseTaxDetail(fdDetail, fdAgency);

         // Create TaxBase record
         //if (*apTokens[SEC_AUTHORG] == 'C' || *apTokens[SEC_AUTHORG] == 'A')
         Tax_CreateTaxBaseCsv(acRec, (TAXBASE *)&acBase);
         fputs(acRec, fdBase);
         lOut++;
      } else if (iRet < 0)
      {
         lDrop++;
         LogMsg("---> Drop prior year record %d [APN=%s, TaxYear=20%.2s]", lCnt, apTokens[SEC_APN], apTokens[SEC_PRCLHIST]); 
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdIn)
      fclose(fdIn);
   if (fdBase)
      fclose(fdBase);
   if (fdDetail)
      fclose(fdDetail);
   if (fdAgency)
      fclose(fdAgency);

   LogMsg("Total records processed:  %u", lCnt);
   LogMsg("         output records:  %u", lOut);
   LogMsg("        records dropped:  %u", lDrop);

   // Dedup Agency file
   iRet = sortFile(acTmpFile, acAgencyFile, "S(#1,C,A) DUPOUT(B2048, #1) DEL(124) F(TXT)");

   // Import into SQL
   if (bImport)
   {
      iRet = doTaxImport(myCounty.acCntyCode, TAX_BASE);
      if (!iRet)
         iRet = doTaxImport(myCounty.acCntyCode, TAX_DETAIL);
      if (!iRet)
         iRet = doTaxImport(myCounty.acCntyCode, TAX_AGENCY);
   } else
      iRet = 0;

   return iRet;
}

/***************************** Lax_MergeTaxDetail ****************************
 *
 * Generate TAXDETAIL & TAXAGENCY from "Liens Secured Tax Roll.csv"
 * Return 0 if success
 *
 *****************************************************************************/

int Lax_MergeTaxDetail(char *pOutbuf, FILE *fdIn, FILE *fdDetail, FILE *fdAgency)
{
   static   char acRec[2048], *pRec=NULL, *apItems[128];
   char     acTmp[256];
   double   dTmp;
   int      iRet=0, iTmp, iCnt, iIdx;
   TAXDETAIL *pDetail = (TAXDETAIL *)pOutbuf;
   TAXAGENCY sAgency;

   // Get rec
   if (!pRec)
   {
      // Skip header record
      do {
         pRec = fgets(acRec, 2048, fdIn);
      } while (pRec && (*pRec < ' ' || *pRec > '9'));
   }

   do
   {
      if (!pRec)
      {
         fclose(fdIn);
         fdIn = NULL;
         return 1;      // EOF
      }

      iTmp = memcmp(pOutbuf, pRec, iApnLen);
      if (iTmp > 0)
      {
         if (bDebug)
            LogMsg0("Skip tax detail rec %.*s", iApnLen, pRec);
         pRec = fgets(acRec, 2048, fdIn);
      }
   } while (iTmp > 0);

   // If not match, return
   if (iTmp)
      return 1;

#ifdef _DEBUG
   //if (!memcmp(pRec, "2028015033", 10))
   //   iTmp = 0;
#endif

   // Parse input
   iTmp = ParseString(pRec, cDelim, LSTR_FLDS+1, apItems);
   if (iTmp < LSTR_FLDS)
   {
      LogMsg("***** Error: bad tax detail record for APN=%.*s (#tokens=%d)", iApnLen, pOutbuf, iTmp);
      return -1;
   }

   // Take current year only
   iTmp = atoin(apItems[LSTR_PRCLYEAR_SEQ], 2) + 2000;
   if (iTmp < lTaxYear)
      return -1;

   // Loop through tax items
   iIdx = LSTR_TAXCODE1;
   memset((char *)&sAgency, 0, sizeof(TAXAGENCY));
   sprintf(pDetail->BillNum, "%s-%s", apItems[LSTR_APN], apItems[LSTR_PRCLYEAR_SEQ]);

   for (iCnt = 0; iCnt < LSTR_MAX_TAXITEMS; iCnt++)
   {      
      // Agency 
      strcpy(pDetail->TaxCode, apItems[iIdx]);
      // Tax Amt
      dTmp  = atof(apItems[iIdx+2]);

      if (dTmp)
      {
         // Tax Desc
         if (*apItems[iIdx+1] > ' ')
         {
            iTmp = strlen(apItems[iIdx+1]);
            if (isdigit(*(apItems[iIdx+1]+iTmp-2) ))
            {
               iTmp -= 2;
               *(apItems[iIdx+1]+iTmp) = 0;
            }
            strcpy(pDetail->TaxDesc, apItems[iIdx+1]);
         } else
         {
            LogMsg("??? New Agency: %s APN=%s", apItems[iIdx], pDetail->Apn);
            if (dTmp < 0)
               sprintf(pDetail->TaxDesc, "%s - EXEMPTION", apItems[iIdx]);
            else
               sprintf(pDetail->TaxDesc, "%s - SPECIAL TAX", apItems[iIdx]);
         }

#ifdef _DEBUG
         //if (!memcmp(pDetail->Apn, "2833016072", 10))
         //   iTmp = 0;
#endif

         sprintf(pDetail->TaxAmt, "%.2f", dTmp);

         // Prorated Amt
         //double dTmp1 = atof(apItems[iIdx+3]);
         //if (bDebug && dTmp1 > 0 && dTmp != dTmp1)
         //   LogMsg("??? TaxAmt=%.2f, ProratedAmt=%.2f APN=%s", dTmp, dTmp1, pDetail->Apn);

         // Generate csv line and write to file
         Tax_CreateDetailCsv(acTmp, pDetail);
         fputs(acTmp, fdDetail);

         // Generate Agency record
         strcpy(sAgency.Agency, pDetail->TaxDesc);
         strcpy(sAgency.Code, pDetail->TaxCode);
         Tax_CreateAgencyCsv(acTmp, &sAgency);
         fputs(acTmp, fdAgency);
      } else
         break;

      iIdx += 4;
   }

   pRec = fgets(acRec, 2048, fdIn);

   return 0;
}

/************************* Lax_Load_TaxBaseCsv *****************************
 *
 * Loading Secured Tax Roll.csv 
 * Create TaxBase
 * Return: -1 if error, 1 if prior year
 *
 ***************************************************************************/

int Lax_ParseTaxBaseCsv(char *pBaseBuf, FILE *fdIn, FILE *fdDetail, FILE *fdAgency)
{
   int      iTmp;
   double   dTmp;
   char     acTmp[256], *pTmp;
   TAXBASE  *pBaseRec = (TAXBASE *)pBaseBuf;

   // Clear output buffer
   memset(pBaseBuf, 0, sizeof(TAXBASE));

   // APN
   strcpy(pBaseRec->Apn, apTokens[STR_APN]);

   // TRA
   strcpy(pBaseRec->TRA, apTokens[STR_TRA]);

   // Find tax rate
   if (iNumTRADist > 0)
   {
      TRADIST *pResult = findTRADist(pBaseRec->TRA);
      if (pResult)
         strcpy(pBaseRec->TotalRate, pResult->TaxRate);
      else
         LogMsg("Invalid TRA: %s", pBaseRec->TRA);
   }

   // Tax Year
   iTmp = 2000 + atoin(apTokens[STR_PRCLHIST], 2);
   if (iTmp < lTaxYear) // || memcmp(apTokens[STR_PRCLHIST]+2, "000", 3))
      return 1;

   sprintf(pBaseRec->TaxYear, "%d", iTmp);

   // Updated date
   sprintf(pBaseRec->Upd_Date, "%d", lLastTaxFileDate);

   // Bill type
   if (!memcmp(apTokens[STR_PRCLHIST]+2, "000", 3))
   {
      pBaseRec->isSecd[0] = '1';
      pBaseRec->BillType[0] = BILLTYPE_SECURED;
   } else 
   {
      pBaseRec->isSupp[0] = '1';

      if (!memcmp(apTokens[STR_RCN], "000", 3))
         pBaseRec->BillType[0] = BILLTYPE_SECURED_SUPPL;
      else
         pBaseRec->BillType[0] = BILLTYPE_ROLL_CORRECTION;
   }

   // Paid Status
   // 00 = Unpaid
   // 01 = Personal property paid
   // 02 = 1st installment only paid
   // 03 = Total paid, for both installments
   // 04 = 1st installment, 2nd adjusted
   // 05 = 1st and 2nd paid, 2nd adjusted
   // 06 = 1st paid, 2nd cancelled
   // 07 = Personal property paid, 2nd adjusted
   // 08 = 1st partially paid
   // 09 = 1st paid, 2nd partially paid 
   pBaseRec->Inst1Status[0] = TAX_BSTAT_UNPAID;
   pBaseRec->Inst2Status[0] = TAX_BSTAT_UNPAID;

   iTmp = atoi(apTokens[STR_PAIDSTATUS]);
   switch (iTmp)
   {
      case 0:
         break;
      case 2:
         pBaseRec->Inst1Status[0] = TAX_BSTAT_PAID;
         break;
      case 4:
      case 6:
         pBaseRec->Inst1Status[0] = TAX_BSTAT_PAID;
         pBaseRec->Inst2Status[0] = TAX_BSTAT_CANCEL;
         break;
      case 8:
         pBaseRec->Inst1Status[0] = TAX_BSTAT_INSTPMNT;
         break;
      case 9:
         pBaseRec->Inst1Status[0] = TAX_BSTAT_PAID;
         pBaseRec->Inst2Status[0] = TAX_BSTAT_INSTPMNT;
         break;
      case 3:
      case 5:
         pBaseRec->Inst1Status[0] = TAX_BSTAT_PAID;
         pBaseRec->Inst2Status[0] = TAX_BSTAT_PAID;
         break;
   }

   // Check for Tax amount - V99
   double dTax1 = atof(apTokens[STR_INSTAMT1]);
   double dTax2 = atof(apTokens[STR_INSTAMT2]);
   double dTotalTax = dTax1+dTax2;
   if (dTotalTax > 0.0)
   {
      sprintf(pBaseRec->TotalTaxAmt, "%.2f", dTotalTax);

      sprintf(pBaseRec->TaxAmt1, "%.2f", dTax1);
      sprintf(pBaseRec->TaxAmt2, "%.2f", dTax2);
   }

   // Due
   double dDue1 = atof(apTokens[STR_DUEAMT1]);
   double dDue2 = atof(apTokens[STR_DUEAMT2]);
   strcpy(pBaseRec->TotalDue, apTokens[STR_TOTALTAXDUE]);

   // Penalty
   double dPen1 = atof(apTokens[STR_PENAMT1]);
   double dPen2 = atof(apTokens[STR_PENAMT2]);
   if (dPen1 > 0.0)
      strcpy(pBaseRec->PenAmt1, apTokens[STR_PENAMT1]);
   if (dPen2 > 0.0)
      strcpy(pBaseRec->PenAmt2, apTokens[STR_PENAMT2]);

#ifdef _DEBUG
   //if (!memcmp(pBaseRec->Apn, "2833016072", 10))
   //   iTmp = 0;
#endif

   // Paid
   if (*apTokens[STR_PAIDDATE1] > ' ')
   {
      pTmp = dateConversion(apTokens[STR_PAIDDATE1], pBaseRec->PaidDate1, MM_DD_YYYY_1);
      if (!pTmp)
         LogMsg("***** Bad PaidDate1 %s [%s]", pBaseRec->PaidDate1, pBaseRec->Apn);
      dPen1 = atof(apTokens[STR_PAIDPENAMT1]);
      dTmp = dPen1 + dTax1;
      sprintf(pBaseRec->PaidAmt1, "%.2f", dTmp);
      if (*apTokens[STR_PAIDDATE2] > ' ')
      {
         pTmp = dateConversion(apTokens[STR_PAIDDATE2], pBaseRec->PaidDate2, MM_DD_YYYY_1);
         if (!pTmp)
            LogMsg("***** Bad PaidDate1 %s [%s]", pBaseRec->PaidDate1, pBaseRec->Apn);
         dPen2 = atof(apTokens[STR_PAIDPENAMT2]);
         dTmp = dPen2 + dTax2;
         sprintf(pBaseRec->PaidAmt2, "%.2f", dTmp);
      }
   } 
   if (pBaseRec->isSecd[0] == '1')
   {
      InstDueDate(pBaseRec->DueDate1, 1, lTaxYear);
      InstDueDate(pBaseRec->DueDate2, 2, lTaxYear);
   }

   // Total fee
   double dCost = atof(apTokens[STR_COSTDUE]);
   if (dCost > 0.0)
      sprintf(pBaseRec->TotalFees, "%.2f", dCost);

   // Is delinquent 
   if (*apTokens[STR_TAXSTATUS] > '0')
   {
      pBaseRec->isDelq[0] = '1';
      if (*apTokens[STR_TAXSTATUS] == '1')
         pBaseRec->DelqStatus[0] = TAX_STAT_SOLD2AUCTION;
      else if (*apTokens[STR_TAXSTATUS] == '2')
         pBaseRec->DelqStatus[0] = TAX_STAT_POWER2SELL;
      else
         pBaseRec->DelqStatus[0] = TAX_STAT_UNPAID;
   }

   // Delq year
   if (*apTokens[STR_DELQYEAR] > '0')
   {
      // This should be ok for 1970-2069
      if (*apTokens[STR_DELQYEAR] > '6')
         sprintf(pBaseRec->DelqYear, "19%s", apTokens[STR_DELQYEAR]);
      else
         sprintf(pBaseRec->DelqYear, "20%s", apTokens[STR_DELQYEAR]);
   } 

   // Bill number - LAX doesn't provide bill number, we use APN and Seq number instead
   sprintf(pBaseRec->BillNum, "%s-%s", apTokens[STR_APN], apTokens[STR_PRCLHIST]);

   if (pBaseRec->isSecd[0] == '1')
   {
      // Generate general tax detail record
      char      acDetail[512];
      double    dTotalVal, dCntyTax, dTotalExe;

      TAXDETAIL *pDetail = (TAXDETAIL *)acDetail;
      TAXAGENCY sAgency;

      // Clear output buffer
      memset(acDetail, 0, sizeof(TAXDETAIL));
      memset((char *)&sAgency, 0, sizeof(TAXAGENCY));

      // APN
      strcpy(pDetail->Apn, apTokens[STR_APN]);

      // Bill number - LAX doesn't provide bill number, we use APN and Seq number instead
      sprintf(pDetail->BillNum, "%s-%s", apTokens[STR_APN], apTokens[STR_PRCLHIST]);

      // Tax Year
      sprintf(pDetail->TaxYear, "20%.2s", apTokens[STR_PRCLHIST]);
      // General tax
      dTotalVal  = atof(apTokens[STR_LAND_VALUE]);
      dTotalVal  += atof(apTokens[STR_IMPROVEMENT_VALUE]);
      dTotalVal  += atof(apTokens[STR_FIXTURE_VALUE]);
      dTotalVal  += atof(apTokens[STR_PERSONAL_PROPERTY_VALUE]);

      dTotalExe  =  atof(apTokens[STR_OTHEXEVAL]);
      dTotalExe  += atof(apTokens[STR_MHOTHEXEVAL]);
      dTotalExe  += atof(apTokens[STR_HOEXEVAL]);
      dTotalExe  += atof(apTokens[STR_MHEXEVAL]);
      dTotalExe  += atof(apTokens[STR_FIXTEXEVAL]);
      dTotalExe  += atof(apTokens[STR_PPEXEVAL]);

      // We can generate 1% of total value for general tax
      dCntyTax  = (dTotalVal-dTotalExe)/100.0;

      // When following fields are populated, use them
      // Currently not all are populated, No TAXTYPE_2_AMOUNT, TAX_TYPE_4_AMOUNT
      //dTmp = atof(apTokens[STR_TAXTYPE1]);
      //dTmp += atof(apTokens[STR_TAXTYPE2]);
      //dTmp += atof(apTokens[STR_TAXTYPE3]);
      //dTmp += atof(apTokens[STR_TAXTYPE4]);
      //dTmp += atof(apTokens[STR_TAXTYPE8]);
      //if (dTmp > 0.0)
      //   dCntyTax += dTmp;
      if (dCntyTax > 0)
      {
         strcpy(pDetail->TaxCode, "00001");
         strcpy(pDetail->TaxDesc, "GENERAL TAX");
         sprintf(pDetail->TaxAmt, "%.2f", dCntyTax);
         pDetail->TC_Flag[0] = '1';

         Tax_CreateDetailCsv(acTmp, pDetail);
         fputs(acTmp, fdDetail);

         // Generate Agency record
         strcpy(sAgency.Agency, "GENERAL TAX");
         strcpy(sAgency.Code, pDetail->TaxCode);
         sAgency.TC_Flag[0] = '1';
         Tax_CreateAgencyCsv(acTmp, &sAgency);
         fputs(acTmp, fdAgency);
      }

      pDetail->TC_Flag[0] = 0;

      // Get more detail -this is direct assessment - voted indebtedness are not included (county not provided)
      Lax_MergeTaxDetail(&acDetail[0], fdIn, fdDetail, fdAgency);
   }
   return 0;
}

int Lax_Load_TaxBaseCsv(bool bImport)
{
   char      *pTmp, acBase[1024], acDetail[1024], acRec[MAX_RECSIZE],
             acBaseFile[_MAX_PATH], acDetailFile[_MAX_PATH], acTmpFile[_MAX_PATH], acAgencyFile[_MAX_PATH];

   int       iRet;
   long      lOut=0, lCnt=0, lDrop=0;
   FILE      *fdBase, *fdDetail, *fdAgency, *fdBaseIn, *fdDetailIn;
   TAXBASE   *pBase = (TAXBASE *)acBase;
   TAXDETAIL *pDetail = (TAXDETAIL *)acDetail;

   LogMsg0("Loading Tax Base & Detail");

   NameTaxCsvFile(acBaseFile, myCounty.acCntyCode, "Base");
   NameTaxCsvFile(acDetailFile, myCounty.acCntyCode, "Items");
   NameTaxCsvFile(acAgencyFile, myCounty.acCntyCode, "Agency");

   pTmp = strrchr(acBaseFile, '\\');
   *pTmp = 0;
   if (_access(acBaseFile, 0))
      _mkdir(acBaseFile);
   *pTmp = '\\';

   // Open input file
   GetIniString(myCounty.acCntyCode, "TaxBase", "", acTmpFile, _MAX_PATH, acIniFile);
   lLastTaxFileDate = getFileDate(acTmpFile);
   // Only process if new tax file
   iRet = isNewTaxFile(acTmpFile, myCounty.acCntyCode);
   if (iRet <= 0)
   {
      lLastTaxFileDate = 0;
      return iRet;
   }

   // Open Secured Tax Roll
   LogMsg("Open tax file %s for input", acTmpFile);
   fdBaseIn = fopen(acTmpFile, "r");
   if (fdBaseIn == NULL)
   {
      LogMsg("***** Error opening Current tax file: %s\n", acTmpFile);
      return -2;
   }  

   // Open Liens Secured Tax Roll
   GetIniString(myCounty.acCntyCode, "TaxDetail", "", acTmpFile, _MAX_PATH, acIniFile);
   LogMsg("Open tax file %s for input", acTmpFile);
   fdDetailIn = fopen(acTmpFile, "r");
   if (fdDetailIn == NULL)
   {
      LogMsg("***** Error opening tax file: %s\n", acTmpFile);
      return -2;
   }  

   // Open base file
   LogMsg("Open Base file %s for output", acBaseFile);
   fdBase = fopen(acBaseFile, "w");
   if (fdBase == NULL)
   {
      LogMsg("***** Error creating base file: %s\n", acBaseFile);
      return -4;
   }

   // Open Detail file
   LogMsg("Open Detail file %s for output", acDetailFile);
   fdDetail = fopen(acDetailFile, "w");
   if (fdDetail == NULL)
   {
      LogMsg("***** Error creating Detail output file: %s\n", acDetailFile);
      return -4;
   }

   // Open Agency file
   sprintf(acTmpFile, "%s\\%s\\%s_Agency.tmp", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   LogMsg("Open Agency file %s for output", acTmpFile);
   fdAgency = fopen(acTmpFile, "w");
   if (fdAgency == NULL)
   {
      LogMsg("***** Error creating Agency file: %s\n", acTmpFile);
      return -4;
   }

   // Skip header
   pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdBaseIn);

   // Merge loop 
   while (!feof(fdBaseIn))
   {
      pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdBaseIn);
      if (!pTmp || *pTmp == '0')
         break;

      // Parse input record
      iTokens = ParseString(acRec, cDelim, 256, apTokens);
      if (iTokens < STR_OBJECTID)
      {
         if (iTokens > SEC_APN)
            LogMsg("*** Bad record: %s (%d)", apTokens[STR_PARCEL_NUMBER], iTokens);
         continue;
      }

      // Create new base record
      iRet = Lax_ParseTaxBaseCsv(acBase, fdDetailIn, fdDetail, fdAgency);
      if (!iRet)
      {
         // Create TaxBase record
         Tax_CreateTaxBaseCsv(acRec, (TAXBASE *)&acBase);
         fputs(acRec, fdBase);
         lOut++;
      } else
      {
         lDrop++;
         if (bDebug)
            LogMsg("---> Drop prior year record [APN=%s, TaxYear=20%.2s]", apTokens[SEC_APN], apTokens[SEC_PRCLHIST]); 
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdBaseIn)
      fclose(fdBaseIn);
   if (fdBase)
      fclose(fdBase);
   if (fdDetail)
      fclose(fdDetail);
   if (fdAgency)
      fclose(fdAgency);

   LogMsg("Total records processed:  %u", lCnt);
   LogMsg("         output records:  %u", lOut);
   LogMsg("        records dropped:  %u", lDrop);

   // Dedup Agency file
   iRet = sortFile(acTmpFile, acAgencyFile, "S(#1,C,A) DUPOUT(B2048, #1) DEL(124) F(TXT)");

   // Import into SQL
   if (bImport)
   {
      iRet = doTaxImport(myCounty.acCntyCode, TAX_BASE);
      if (!iRet)
         iRet = doTaxImport(myCounty.acCntyCode, TAX_DETAIL);
      if (!iRet)
         iRet = doTaxImport(myCounty.acCntyCode, TAX_AGENCY);
   } else
      iRet = 0;

   return iRet;
}

/**************************** Lax_Updt_TaxBase *****************************
 *
 * Loading TCSTN960.UNPACKED.FILE.  This file contains only updated records after June
 * Update TaxBase
 *
 ***************************************************************************/

int Lax_Updt_TaxBase(bool bImport)
{
   char      *pTmp, acBase[1024], acRec[MAX_RECSIZE],
             acBaseFile[_MAX_PATH], acTmpFile[_MAX_PATH];

   int       iRet;
   long      lOut=0, lCnt=0;
   FILE      *fdBase, *fdIn;
   TAXBASE   *pBase = (TAXBASE *)acBase;

   LogMsg0("Updating Tax Base & Detail");

   if (bExpSql)
      NameTaxCsvFile(acBaseFile, "TmpLax", "Base");
   else
      NameTaxCsvFile(acBaseFile, "Tmp", "Base");

   // Open input file
   GetIniString(myCounty.acCntyCode, "TaxDetail", "", acTmpFile, _MAX_PATH, acIniFile);
   lLastTaxFileDate = getFileDate(acTmpFile);
   // Only process if new tax file
   iRet = isNewTaxFile(NULL, myCounty.acCntyCode);
   if (iRet <= 0)
   {
      lLastTaxFileDate = 0;
      return iRet;
   }

   LogMsg("Open tax file %s for input", acTmpFile);
   fdIn = fopen(acTmpFile, "r");
   if (fdIn == NULL)
   {
      LogMsg("***** Error opening Current tax file: %s\n", acTmpFile);
      return -2;
   }  

   // Open base file
   LogMsg("Open Base file %s for output", acBaseFile);
   fdBase = fopen(acBaseFile, "w");
   if (fdBase == NULL)
   {
      LogMsg("***** Error creating base file: %s\n", acBaseFile);
      return -4;
   }

   // Merge loop 
   while (!feof(fdIn))
   {
      pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdIn);
      if (!pTmp || *pTmp == '0')
         break;

      // Parse input record
      iTokens = ParseStringIQ(acRec, cDelim, 256, apTokens);
      if (iTokens < SEC_COMBINELIENKEY)
      {
         if (iTokens > SEC_APN)
            LogMsg("*** Bad record: %s", apTokens[SEC_APN]);
         continue;
      }

      // Create new base record
      iRet = Lax_ParseTaxBase(acBase);
      if (!iRet)
      {
         //if (pBase->PaidAmt1[0] > '0' || pBase->PaidAmt2[0] > '0')
         //{
         //   LogMsg0("Update %s, BillNum: %s, paid1: %s, Amt1: %s, paid2: %s, Amt2: %s", pBase->Apn, pBase->BillNum, pBase->PaidDate1,  pBase->PaidAmt1,  pBase->PaidDate2,  pBase->PaidAmt2);
         //   lOut++;
         //}

         // Create TaxBase record
         Tax_CreateTaxBaseCsv(acRec, (TAXBASE *)&acBase);
         fputs(acRec, fdBase);
         lOut++;
      } else
      {
         LogMsg("---> Drop Base record %d [%.40s]", lCnt, acRec); 
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdIn)
      fclose(fdIn);
   if (fdBase)
      fclose(fdBase);

   LogMsg("Total records processed:    %u", lCnt);
   LogMsg("Total output records:       %u", lOut);

   // Import into SQL
   if (bImport)
   {
      iRet = doTaxImport("TmpLax", TAX_BASE);
      if (!iRet)
         iRet = doTaxMerge(myCounty.acCntyCode, false, false);
   } else
      iRet = 0;

   return iRet;
}

/**************************** Lax_Updt_TaxBase *****************************
 *
 * Loading TCSTN960.UNPACKED.FILE.  This file contains only updated records after June
 * Update TaxBase
 *
 ***************************************************************************/

int Lax_ParseSecuredTaxRoll(char *pBaseBuf)
{
   int      iTmp;
   double   dTmp;

   TAXBASE   *pBaseRec = (TAXBASE *)pBaseBuf;

   // Clear output buffer
   memset(pBaseBuf, 0, sizeof(TAXBASE));

   // APN
   strcpy(pBaseRec->Apn, apTokens[SEC_APN]);

   // TRA
   strcpy(pBaseRec->TRA, apTokens[SEC_TRA]);

   // Tax Year
   iTmp = 2000 + atoin(apTokens[SEC_PRCLHIST], 2);
   // 20240117 - if (iTmp != lTaxYear)
   //   return 1;
   sprintf(pBaseRec->TaxYear, "%d", iTmp);

   // Updated date
   sprintf(pBaseRec->Upd_Date, "%d", lLastTaxFileDate);

   // Paid Status
   // 00 = Unpaid
   // 01 = Personal property paid
   // 02 = 1st installment only paid
   // 03 = Total paid, for both installments
   // 04 = 1st installment, 2nd adjusted
   // 05 = 1st and 2nd paid, 2nd adjusted
   // 06 = 1st paid, 2nd cancelled
   // 07 = Personal property paid, 2nd adjusted
   // 08 = 1st partially paid
   // 09 = 1st paid, 2nd partially paid 
   pBaseRec->Inst1Status[0] = TAX_BSTAT_UNPAID;
   pBaseRec->Inst2Status[0] = TAX_BSTAT_UNPAID;

   iTmp = atoi(apTokens[SEC_PAIDSTATUS]);
   switch (iTmp)
   {
      case 0:
         break;
      case 2:
         pBaseRec->Inst1Status[0] = TAX_BSTAT_PAID;
         break;
      case 4:
      case 6:
         pBaseRec->Inst1Status[0] = TAX_BSTAT_PAID;
         pBaseRec->Inst2Status[0] = TAX_BSTAT_CANCEL;
         break;
      case 8:
         pBaseRec->Inst1Status[0] = TAX_BSTAT_INSTPMNT;
         break;
      case 9:
         pBaseRec->Inst1Status[0] = TAX_BSTAT_PAID;
         pBaseRec->Inst2Status[0] = TAX_BSTAT_INSTPMNT;
         break;
      case 3:
      case 5:
         pBaseRec->Inst1Status[0] = TAX_BSTAT_PAID;
         pBaseRec->Inst2Status[0] = TAX_BSTAT_PAID;
         break;
   }

#ifdef _DEBUG
   //if (!memcmp(pBaseRec->Apn, "9014800001", 10))
   //   iTmp = 0;
#endif

   // Check for Tax amount - V99
   double dTax1 = atof(apTokens[SEC_INSTAMT1]);
   double dTax2 = atof(apTokens[SEC_INSTAMT2]);
   double dTotalTax = dTax1+dTax2;
   if (dTotalTax > 0.0)
   {
      sprintf(pBaseRec->TotalTaxAmt, "%.2f", dTotalTax);

      sprintf(pBaseRec->TaxAmt1, "%.2f", dTax1);
      sprintf(pBaseRec->TaxAmt2, "%.2f", dTax2);
   }

   // Due
   double dDue1 = atof(apTokens[SEC_DUEAMT1]);
   double dDue2 = atof(apTokens[SEC_DUEAMT2]);
   strcpy(pBaseRec->TotalDue, apTokens[SEC_TOTALTAXDUE]);

   // Penalty
   double dPen1 = atof(apTokens[SEC_PENAMT1]);
   double dPen2 = atof(apTokens[SEC_PENAMT2]);
   if (dPen1 > 0.0)
      strcpy(pBaseRec->PenAmt1, apTokens[SEC_PENAMT1]);
   if (dPen2 > 0.0)
      strcpy(pBaseRec->PenAmt2, apTokens[SEC_PENAMT2]);

   // Paid
   if (*apTokens[SEC_PAIDDATE1] > ' ')
   {
      dateConversion(apTokens[SEC_PAIDDATE1], pBaseRec->PaidDate1, MM_DD_YYYY_2);
      //sprintf(pBaseRec->PaidDate1, "20%s", apTokens[SEC_PAIDDATE1]);
      dPen1 = atof(apTokens[SEC_PAIDPENAMT1]);
      dTmp = dPen1 + dTax1;
      sprintf(pBaseRec->PaidAmt1, "%.2f", dTmp);
      if (*apTokens[SEC_PAIDDATE2] > ' ')
      {
         dateConversion(apTokens[SEC_PAIDDATE2], pBaseRec->PaidDate2, MM_DD_YYYY_2);
         //sprintf(pBaseRec->PaidDate2, "20%s", apTokens[SEC_PAIDDATE2]);
         dPen2 = atof(apTokens[SEC_PAIDPENAMT2]);
         dTmp = dPen2 + dTax2;
         sprintf(pBaseRec->PaidAmt2, "%.2f", dTmp);
      }
   } else
   {
      InstDueDate(pBaseRec->DueDate1, 1, lTaxYear);
      InstDueDate(pBaseRec->DueDate2, 2, lTaxYear);
   }

   // Total fee
   double dCost = atof(apTokens[SEC_COSTDUE]);
   if (dCost > 0.0)
      sprintf(pBaseRec->TotalFees, "%.2f", dCost);

   // Is delinquent 
   if (*apTokens[SEC_TAXSTATUS] > '0')
   {
      pBaseRec->isDelq[0] = '1';
      if (*apTokens[SEC_TAXSTATUS] == '1')
         pBaseRec->DelqStatus[0] = TAX_STAT_SOLD2AUCTION;
      else if (*apTokens[SEC_TAXSTATUS] == '2')
         pBaseRec->DelqStatus[0] = TAX_STAT_POWER2SELL;
      else
         pBaseRec->DelqStatus[0] = TAX_STAT_UNPAID;
   }

   // Delq year
   if (*apTokens[SEC_DELQYEAR] > '0')
   {
      // This should be ok for 1970-2069
      if (*apTokens[SEC_DELQYEAR] > '6')
         sprintf(pBaseRec->DelqYear, "19%s", apTokens[SEC_DELQYEAR]);
      else
         sprintf(pBaseRec->DelqYear, "20%s", apTokens[SEC_DELQYEAR]);
   } 

   // Supplemental record?
   //char *pTmp = apTokens[SEC_PRCLHIST]+2;

   if (!memcmp(apTokens[SEC_PRCLHIST]+2, "000", 3))
   {
      pBaseRec->isSecd[0] = '1';
      pBaseRec->BillType[0] = BILLTYPE_SECURED;
   } else 
   {
      pBaseRec->isSupp[0] = '1';

      if (!memcmp(apTokens[SEC_RCN], "000", 3))
         pBaseRec->BillType[0] = BILLTYPE_SECURED_SUPPL;
      else
         pBaseRec->BillType[0] = BILLTYPE_ROLL_CORRECTION;
   }

   // Bill number - LAX doesn't provide bill number, we use APN and Seq number instead
   sprintf(pBaseRec->BillNum, "%s-%s", apTokens[SEC_APN], apTokens[SEC_PRCLHIST]);

   return 0;
}

int Lax_ParseLienSecuredTaxRoll(char *pBaseBuf)
{
   int      iTmp;
   double   dTmp;

   TAXBASE   *pBaseRec = (TAXBASE *)pBaseBuf;

   // Clear output buffer
   memset(pBaseBuf, 0, sizeof(TAXBASE));

   // APN
   strcpy(pBaseRec->Apn, apTokens[SEC_APN]);

   // TRA
   strcpy(pBaseRec->TRA, apTokens[SEC_TRA]);

   // Tax Year
   iTmp = 2000 + atoin(apTokens[SEC_PRCLHIST], 2);
   // 20240117 - if (iTmp != lTaxYear)
   //   return 1;
   sprintf(pBaseRec->TaxYear, "%d", iTmp);

   // Updated date
   sprintf(pBaseRec->Upd_Date, "%d", lLastTaxFileDate);

   // Paid Status
   // 00 = Unpaid
   // 01 = Personal property paid
   // 02 = 1st installment only paid
   // 03 = Total paid, for both installments
   // 04 = 1st installment, 2nd adjusted
   // 05 = 1st and 2nd paid, 2nd adjusted
   // 06 = 1st paid, 2nd cancelled
   // 07 = Personal property paid, 2nd adjusted
   // 08 = 1st partially paid
   // 09 = 1st paid, 2nd partially paid 
   pBaseRec->Inst1Status[0] = TAX_BSTAT_UNPAID;
   pBaseRec->Inst2Status[0] = TAX_BSTAT_UNPAID;

   iTmp = atoi(apTokens[SEC_PAIDSTATUS]);
   switch (iTmp)
   {
      case 0:
         break;
      case 2:
         pBaseRec->Inst1Status[0] = TAX_BSTAT_PAID;
         break;
      case 4:
      case 6:
         pBaseRec->Inst1Status[0] = TAX_BSTAT_PAID;
         pBaseRec->Inst2Status[0] = TAX_BSTAT_CANCEL;
         break;
      case 8:
         pBaseRec->Inst1Status[0] = TAX_BSTAT_INSTPMNT;
         break;
      case 9:
         pBaseRec->Inst1Status[0] = TAX_BSTAT_PAID;
         pBaseRec->Inst2Status[0] = TAX_BSTAT_INSTPMNT;
         break;
      case 3:
      case 5:
         pBaseRec->Inst1Status[0] = TAX_BSTAT_PAID;
         pBaseRec->Inst2Status[0] = TAX_BSTAT_PAID;
         break;
   }

#ifdef _DEBUG
   //if (!memcmp(pBaseRec->Apn, "9014800001", 10))
   //   iTmp = 0;
#endif

   // Check for Tax amount - V99
   double dTax1 = atof(apTokens[SEC_INSTAMT1]);
   double dTax2 = atof(apTokens[SEC_INSTAMT2]);
   double dTotalTax = dTax1+dTax2;
   if (dTotalTax > 0.0)
   {
      sprintf(pBaseRec->TotalTaxAmt, "%.2f", dTotalTax);

      sprintf(pBaseRec->TaxAmt1, "%.2f", dTax1);
      sprintf(pBaseRec->TaxAmt2, "%.2f", dTax2);
   }

   // Due
   double dDue1 = atof(apTokens[SEC_DUEAMT1]);
   double dDue2 = atof(apTokens[SEC_DUEAMT2]);
   strcpy(pBaseRec->TotalDue, apTokens[SEC_TOTALTAXDUE]);

   // Penalty
   double dPen1 = atof(apTokens[SEC_PENAMT1]);
   double dPen2 = atof(apTokens[SEC_PENAMT2]);
   if (dPen1 > 0.0)
      strcpy(pBaseRec->PenAmt1, apTokens[SEC_PENAMT1]);
   if (dPen2 > 0.0)
      strcpy(pBaseRec->PenAmt2, apTokens[SEC_PENAMT2]);

   // Paid
   if (*apTokens[SEC_PAIDDATE1] > ' ')
   {
      sprintf(pBaseRec->PaidDate1, "20%s", apTokens[SEC_PAIDDATE1]);
      dPen1 = atof(apTokens[SEC_PAIDPENAMT1]);
      dTmp = dPen1 + dTax1;
      sprintf(pBaseRec->PaidAmt1, "%.2f", dTmp);
      if (*apTokens[SEC_PAIDDATE2] > ' ')
      {
         sprintf(pBaseRec->PaidDate2, "20%s", apTokens[SEC_PAIDDATE2]);
         dPen2 = atof(apTokens[SEC_PAIDPENAMT2]);
         dTmp = dPen2 + dTax2;
         sprintf(pBaseRec->PaidAmt2, "%.2f", dTmp);
      }
   } else
   {
      InstDueDate(pBaseRec->DueDate1, 1, lTaxYear);
      InstDueDate(pBaseRec->DueDate2, 2, lTaxYear);
   }

   // Total fee
   double dCost = atof(apTokens[SEC_COSTDUE]);
   if (dCost > 0.0)
      sprintf(pBaseRec->TotalFees, "%.2f", dCost);

   // Is delinquent 
   if (*apTokens[SEC_TAXSTATUS] > '0')
   {
      pBaseRec->isDelq[0] = '1';
      if (*apTokens[SEC_TAXSTATUS] == '1')
         pBaseRec->DelqStatus[0] = TAX_STAT_SOLD2AUCTION;
      else if (*apTokens[SEC_TAXSTATUS] == '2')
         pBaseRec->DelqStatus[0] = TAX_STAT_POWER2SELL;
      else
         pBaseRec->DelqStatus[0] = TAX_STAT_UNPAID;
   }

   // Delq year
   if (*apTokens[SEC_DELQYEAR] > '0')
   {
      // This should be ok for 1970-2069
      if (*apTokens[SEC_DELQYEAR] > '6')
         sprintf(pBaseRec->DelqYear, "19%s", apTokens[SEC_DELQYEAR]);
      else
         sprintf(pBaseRec->DelqYear, "20%s", apTokens[SEC_DELQYEAR]);
   } 

   // Supplemental record?
   //char *pTmp = apTokens[SEC_PRCLHIST]+2;

   if (!memcmp(apTokens[SEC_PRCLHIST]+2, "000", 3))
   {
      pBaseRec->isSecd[0] = '1';
      pBaseRec->BillType[0] = BILLTYPE_SECURED;
   } else 
   {
      pBaseRec->isSupp[0] = '1';

      if (!memcmp(apTokens[SEC_RCN], "000", 3))
         pBaseRec->BillType[0] = BILLTYPE_SECURED_SUPPL;
      else
         pBaseRec->BillType[0] = BILLTYPE_ROLL_CORRECTION;
   }

   // Bill number - LAX doesn't provide bill number, we use APN and Seq number instead
   sprintf(pBaseRec->BillNum, "%s-%s", apTokens[SEC_APN], apTokens[SEC_PRCLHIST]);

   return 0;
}

int Lax_Updt_SecuredTaxRoll(bool bImport)
{
   char      *pTmp, acBase[1024], acRec[MAX_RECSIZE],
             acBaseFile[_MAX_PATH], acTmpFile[_MAX_PATH];

   int       iRet;
   long      lOut=0, lCnt=0;
   FILE      *fdBase, *fdIn;
   TAXBASE   *pBase = (TAXBASE *)acBase;

   LogMsg0("Updating Tax Base");

   if (bExpSql)
      NameTaxCsvFile(acBaseFile, "TmpLax", "Base");
   else
      NameTaxCsvFile(acBaseFile, "Tmp", "Base");

   // Open input file
   GetIniString(myCounty.acCntyCode, "TaxBase", "", acTmpFile, _MAX_PATH, acIniFile);
   lLastTaxFileDate = getFileDate(acTmpFile);
   // Only process if new tax file
   iRet = isNewTaxFile(NULL, myCounty.acCntyCode);
   if (iRet <= 0)
   {
      lLastTaxFileDate = 0;
      return iRet;
   }

   LogMsg("Open tax file %s for input", acTmpFile);
   fdIn = fopen(acTmpFile, "r");
   if (fdIn == NULL)
   {
      LogMsg("***** Error opening Current tax file: %s\n", acTmpFile);
      return -2;
   }  

   // Open base file
   LogMsg("Open Base file %s for output", acBaseFile);
   fdBase = fopen(acBaseFile, "w");
   if (fdBase == NULL)
   {
      LogMsg("***** Error creating base file: %s\n", acBaseFile);
      return -4;
   }

   // Drop header
   pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdIn);

   // Merge loop 
   while (!feof(fdIn))
   {
      pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdIn);
      if (!pTmp || *pTmp == '0')
         break;

      // Parse input record
      iTokens = ParseStringIQ(acRec, cDelim, 256, apTokens);
      if (iTokens < STR_OBJECTID)
      {
         if (iTokens > SEC_APN)
            LogMsg("*** Bad record: %s (%d)", apTokens[STR_PARCEL_NUMBER], iTokens);
         continue;
      }

      // Create new base record
      iRet = Lax_ParseSecuredTaxRoll(acBase);
      if (!iRet)
      {
         // Create TaxBase record
         Tax_CreateTaxBaseCsv(acRec, (TAXBASE *)&acBase);
         fputs(acRec, fdBase);
         lOut++;
      } else
      {
         LogMsg("---> Drop Base record %d [%.40s]", lCnt, acRec); 
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdIn)
      fclose(fdIn);
   if (fdBase)
      fclose(fdBase);

   LogMsg("Total records processed:    %u", lCnt);
   LogMsg("Total output records:       %u", lOut);

   // Import into SQL
   if (bImport)
   {
      iRet = doTaxImport("TmpLax", TAX_BASE);
      if (!iRet)
         iRet = doTaxMerge(myCounty.acCntyCode, false, false);
   } else
      iRet = 0;

   return iRet;
}

/**************************** Lax_CreateCharRec ******************************
 *
 * Return number of records created
 *
 *****************************************************************************/

int Lax_CreateCharRec(char *pInbuf, FILE *fdOut)
{
   char     acTmp[_MAX_PATH], acBuf[2048], acCode[16];
   int      iTmp, lTmp, iIdx, iDesign, iBldgs=0;

   STDCHAR  *pCharRec =(STDCHAR *)&acBuf[0];
   LAX_ROLL *pRollRec =(LAX_ROLL *)pInbuf;
   
   // Init output buffer
   memset(acBuf, ' ', sizeof(STDCHAR));

   // APN
   memcpy(pCharRec->Apn, pRollRec->APN, RSIZ_APN);

   // Format APN
   iTmp = formatApn(pRollRec->APN, acTmp, &myCounty);
   memcpy(pCharRec->Apn_D, acTmp, iTmp);

#ifdef _DEBUG
   //if (!memcmp(pCharRec->Apn, "139040027", 9) )
   //   iTmp = 0;
#endif

   // Loop through 
   for (iIdx = 0; iIdx < 5; iIdx++)
   {
      iDesign = atoin(pRollRec->Maseg[iIdx].Design, RSIZ_DESIGN);
      if (!iDesign)
         break;

      // Reset record
      memset(&pCharRec->QualityClass[0], ' ', sizeof(STDCHAR)-(SIZ_CHAR_APN*2));

      // 2700 = parking lot
      if (iDesign != 2700 && iDesign != 3800)
      {
         // BldgSqft
         lTmp = atoin(pRollRec->Maseg[iIdx].Sqft, RSIZ_BLDGSQFT);
         if (lTmp > 0)
         {
            iTmp = sprintf(acTmp, "%d", lTmp);
            memcpy(pCharRec->BldgSqft, acTmp, iTmp);
         }

         // Beds
         lTmp = atoin(pRollRec->Maseg[iIdx].Beds, RSIZ_BEDS);
         if (lTmp > 0)
         {
            iTmp = sprintf(acTmp, "%d", lTmp);
            memcpy(pCharRec->Beds, acTmp, iTmp);
         }

         // Baths
         lTmp = atoin(pRollRec->Maseg[iIdx].Baths, RSIZ_BATHS);
         if (lTmp > 0)
         {
            iTmp = sprintf(acTmp, "%d", lTmp);
            memcpy(pCharRec->FBaths, acTmp, iTmp);
         } 

         // Units
         lTmp = atoin(pRollRec->Maseg[iIdx].Units, RSIZ_UNITS);
         if (lTmp > 0)
         {
            iTmp = sprintf(acTmp, "%d", lTmp);
            memcpy(pCharRec->Units, acTmp, iTmp);
         } 
      }

      // Pool - Residential or hotel/motel
      if (pRollRec->Maseg[iIdx].Design[0] == '0' || !memcmp(pRollRec->Maseg[iIdx].Design, "18", 2))
      {
         switch (pRollRec->Maseg[iIdx].Design[3])
         {
            case '1':
            case '3':
            case '5':
               pCharRec->Pool[0] = 'P';
               break;
            case '4':
               pCharRec->Pool[0] = 'S';
               break;
         }
      }

      // Cooling-Heating
      if (!memcmp(pRollRec->UseCode, "01", 2))        // SFR
      {
         switch (pRollRec->Maseg[iIdx].Design[2])
         {
            case '1':
               pCharRec->Heating[0] = 'C';            // Floor or Wall
               break;
            case '3':
               pCharRec->Cooling[0] = 'C';            // Central A/C
            case '2':
               pCharRec->Heating[0] = 'Z';            // Central heat
               break;
            case '4':
               pCharRec->Heating[0] = 'K';
               break;
         }
      } else if (pRollRec->UseCode[0] == '0')         // MR
      {
         if (pRollRec->Maseg[iIdx].Design[2] == '2' || pRollRec->Maseg[iIdx].Design[2] == '3')
            pCharRec->Cooling[0] = 'C';
      } else if (pRollRec->Maseg[iIdx].Design[3] == '1') // NR
      {
         iTmp = atoin(pRollRec->Maseg[iIdx].Design, 2);
         switch (iTmp)
         {
            case 11:
            case 12:
            case 14:
            case 19:
            case 21:
            case 23:
            case 24:
            case 29:
               pCharRec->Cooling[0] = 'X';            // YES
               break;
         }
      }

      // Year Built
      lTmp = atoin(pRollRec->Maseg[iIdx].YrBlt, RSIZ_YRBLT);
      if (lTmp > 0)
      {
         iTmp = sprintf(acTmp, "%d", lTmp);
         memcpy(pCharRec->YrBlt, acTmp, iTmp);
      } 

      // Quality Class
      memcpy(pCharRec->QualityClass, pRollRec->Maseg[iIdx].Class, RSIZ_CLASS);
      if (pCharRec->QualityClass[0] >= 'A')
      {
         pCharRec->BldgClass = pCharRec->QualityClass[0];
         if (isdigit(pCharRec->QualityClass[1]))
         {
            acTmp[0] = pCharRec->QualityClass[1];
            acTmp[1] = '.';
            if (isdigit(pCharRec->QualityClass[2]))
               acTmp[2] = pCharRec->QualityClass[2];
            else
               acTmp[2] = '0';
            acTmp[3] = 0;

            iTmp = Value2Code(acTmp, acCode, NULL);
            if (iTmp >= 0)
               pCharRec->BldgQual = acCode[0];
         }
      }

      pCharRec->CRLF[0] = '\n';
      pCharRec->CRLF[1] = '\0';

      // Write output
      fputs(acBuf, fdOut);
   }

   return iIdx;
}

/******************************** Lax_ExtrChar *******************************
 *
 * Extract characteristics from DS04.TXT to Lax_Char.dat
 *
 * Return 0 if successful, otherwise error
 *
 *****************************************************************************/

int Lax_ExtrChar(char *pInfile)
{
   char  *pTmp, acInRec[MAX_RECSIZE], acTmpFile[_MAX_PATH];
   long  lCnt=0, lChars=0, iRet;
   FILE  *fdIn, *fdOut;

   LogMsg0("Extracting Chars from %s", pInfile);

   // Open roll file
   LogMsg("Open roll file to extract CHARS %s", pInfile);
   fdIn = fopen(pInfile, "rb");
   if (fdIn == NULL)
   {
      LogMsg("***** Error opening char file: %s\n", pInfile);
      return -2;
   }

   // Create output file
   sprintf(acTmpFile, "%s\\%s\\%s_Char.tmp", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode); 
   LogMsg("Create output chars file %s", acTmpFile);
   fdOut = fopen(acTmpFile, "w");
   if (fdOut == NULL)
   {
      LogMsg("***** Error creating char file: %s\n", acTmpFile);
      return -3;
   }

   // Merge loop
   while (!feof(fdIn))
   {
      // Get next record
      pTmp = fgets(acInRec, MAX_RECSIZE, fdIn);
      if (!pTmp)
         break;

      lChars += Lax_CreateCharRec(acInRec, fdOut);

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);

   }

   // Close files
   if (fdOut)
      fclose(fdOut);
   if (fdIn)
      fclose(fdIn);

   LogMsg("Total input records processed: %u", lCnt);
   LogMsg("Total char records output:     %u", lChars);

   // Sort output
   sprintf(acInRec, "S(1,%d,C,A) ", iApnLen);
   iRet = sortFile(acTmpFile, acCChrFile, acInRec);

   return iRet;
}

int Lax_ExtrExe()
{
   char  acBuf[256], acRollRec[MAX_RECSIZE], acOutFile[_MAX_PATH];
   long  lCnt=0, lOut=0;
   int   iTmp;
   LAX_ROLL *pRec = (LAX_ROLL *)acRollRec;

   // Open roll file
   LogMsg("Open input file %s", acRollFile);
   fdRoll = fopen(acRollFile, "rb");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening roll file: %s (%d)\n", acRollFile, errno);
      return 2;
   }

   // Open Output file
   sprintf(acOutFile, acLienTmpl, myCounty.acCntyCode, "EXE");
   LogMsg("Open output file %s", acOutFile);
   fdLien = fopen(acOutFile, "w");
   if (fdLien == NULL)
   {
      LogMsg("***** Error opening Lien extract file: %s\n", acOutFile);
      return 4;
   }

   // Merge loop
   while (!feof(fdRoll))
   {
      // Get roll rec
      iTmp = fread(acRollRec, 1, iRollLen, fdRoll);
      if (iTmp < iRollLen)
         break;

      // HO Exempt
      long lExe = 0;
      iTmp = atoin(pRec->NumOfHOE, RSIZ_NUMOFHOE);
      if (iTmp > 0)
      {
         lExe = atoin(pRec->HOExe, RSIZ_HOEXE);
         iTmp = sprintf(acBuf, "%.10s|H|%d\n", pRec->APN, lExe);
         fputs(acBuf, fdLien);
         lOut++;
      } 

      long lRelExe = atoin(pRec->RelExe, RSIZ_RELEXE);
      if (lRelExe > 0)
      {
         iTmp = sprintf(acBuf, "%.10s|R|%d\n", pRec->APN, lRelExe);
         fputs(acBuf, fdLien);
         lOut++;
      }
      if (pRec->ExclType > 32)
      {
         iTmp = sprintf(acBuf, "%.10s|%c|%d\n", pRec->APN, pRec->ExclType, lExe+lRelExe);
         fputs(acBuf, fdLien);
         lOut++;
      }
      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdRoll)
      fclose(fdRoll);
   if (fdLien)
      fclose(fdLien);

   LogMsg("Total records processed:     %d", lCnt);
   LogMsg("         records output:     %d\n", lOut);

   return 0;
}

/******************************* Lax_MakeDocLink *******************************
 *
 * Format DocLink
 *
 ******************************************************************************/

void Lax_MakeDocLink(LPSTR pDocLink, LPSTR pDoc, LPSTR pDate)
{
   int   iDocNum;
   char  acTmp[256], acDocName[256], acDocNum[32];

   *pDocLink = 0;
   if (*pDoc > ' ' && *pDate > ' ')
   {
      iDocNum = atol((char *)pDoc);
      sprintf(acDocNum, "%.7d", iDocNum);       // Reformat DocNum
      sprintf(acDocName, "%.4s\\%.4s\\%.4s%s", pDate, acDocNum, pDate, acDocNum);
      sprintf(acTmp, "%s\\%s.pdf", acDocPath, acDocName);

      try
      {
         if (!_access(acTmp, 0))
            strcpy(pDocLink, &acDocName[0]);
      } catch (...)
      {
         LogMsg("*** Bad file name: %s", acTmp);
      }
   }
}

/********************************* Lax_ExtrVal *****************************
 *
 * Extract final values from TCSTN960.UNPACKED.FILE
 * For MH properties:
 *    OtherVal= SEC_MHVAL+SEC_MHOTHER
 *    OtherExe= SEC_OTHEXEVAL+SEC_FIXTEXEVAL+SEC_PPEXEVAL
 * Return number of records
 *
 ***************************************************************************/

int Lax_CreateValueRec_Old(char *pOutbuf)
{
   char     acTmp[256];
   long     iTmp;
   LIENEXTR *pLienRec = (LIENEXTR *)pOutbuf;
   LONGLONG lTmp;

   // Clear output buffer
   memset(pOutbuf, ' ', sizeof(LIENEXTR));

   // Start copying data
   vmemcpy(pLienRec->acApn, apTokens[SEC_APN], RSIZ_APN);

   // Year assessed
   memcpy(pLienRec->acYear, myCounty.acYearAssd, 4);

   // Land
   long lLand = atol(apTokens[SEC_LANDVAL]);
   if (lLand > 0)
   {
      iTmp = sprintf(acTmp, "%*u", sizeof(pLienRec->acLand), lLand);
      memcpy(pLienRec->acLand, acTmp, iTmp);
   }

   // Improve
   long lImpr = atol(apTokens[SEC_IMPRVAL]);
   if (lImpr > 0)
   {
      iTmp = sprintf(acTmp, "%*u", sizeof(pLienRec->acImpr), lImpr);
      memcpy(pLienRec->acImpr, acTmp, iTmp);
   }

   // MH Values
   long lMHVal = atol(apTokens[SEC_MHVAL]) + atol(apTokens[SEC_MHOTHER]);
   long lMHExeVal = atol(apTokens[SEC_MHEXEVAL]) + atol(apTokens[SEC_MHOTHEXEVAL]);

   // Other values
   long lPers = atol(apTokens[SEC_PPVAL]);
   long lFixture = atol(apTokens[SEC_FIXTVAL]);
   lTmp = lPers + lFixture;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*u", SIZ_LIEN_FIXT, lTmp);
      memcpy(pLienRec->acOther, acTmp, SIZ_OTHER);

      if (lPers > 0)
      {
         iTmp = sprintf(acTmp, "%*u", sizeof(pLienRec->acPP_Val), lPers);
         memcpy(pLienRec->acPP_Val, acTmp, iTmp);
      }

      if (lFixture > 0)
      {
         iTmp = sprintf(acTmp, "%*u", sizeof(pLienRec->acME_Val), lFixture);
         memcpy(pLienRec->acME_Val, acTmp, iTmp);
      }
   } else if (lMHVal > 0)
   {
      sprintf(acTmp, "%*u", SIZ_LIEN_FIXT, lMHVal);
      memcpy(pLienRec->acOther, acTmp, SIZ_OTHER);
   }

   // Gross total
   lTmp += (lLand+lImpr);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*u", SIZ_LIEN_FIXT, lTmp);
      memcpy(pLienRec->acGross, acTmp, SIZ_LIEN_FIXT);
   }

   // Ratio
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*u", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
      memcpy(pLienRec->acRatio, acTmp, SIZ_RATIO);
   }

   // HO Exempt
   long lExe = atol(apTokens[SEC_HOEXEVAL]);
   if (lExe > 0)
   {
      pLienRec->acHO[0] = '1';      // 'Y'
      iTmp = sprintf(acTmp, "%*u", SIZ_LIEN_HO_EXE, lExe);
      memcpy(pLienRec->extra.Lax.acHOExe, acTmp, iTmp);
      memcpy(pLienRec->extra.Lax.Exe_Code1, "10", 2);
   } else
      pLienRec->acHO[0] = '2';      // 'N'

   long lRelExe = atol(apTokens[SEC_OTHEXEVAL]);
   if (lRelExe > 0)
   {
      iTmp = sprintf(acTmp, "%*u", SIZ_LIEN_REL_EXE, lRelExe);
      memcpy(pLienRec->extra.Lax.acRelExe, acTmp, iTmp);
   }

   // PP Exe
   long lPPExe = atol(apTokens[SEC_PPEXEVAL]);
   if (lPPExe > 0)
   {
      iTmp = sprintf(acTmp, "%*u", SIZ_LIEN_PERS_EXE, lPPExe);
      memcpy(pLienRec->extra.Lax.acPP_Exe, acTmp, iTmp);
   }

   // Fixt Exe
   long lFixtExe = atol(apTokens[SEC_FIXTEXEVAL]);
   if (lFixtExe > 0)
   {
      iTmp = sprintf(acTmp, "%*u", SIZ_LIEN_FIXT_EXE, lFixtExe);
      memcpy(pLienRec->extra.Lax.acME_Exe, acTmp, iTmp);
   }

   // Total Exe
   lTmp = lExe+lFixtExe+lPPExe+lRelExe+lMHExeVal;
   if (lTmp > 0)
   {
      iTmp = sprintf(acTmp, "%*u", sizeof(pLienRec->acExAmt), lTmp);
      memcpy(pLienRec->acExAmt, acTmp, iTmp);
   }

   pLienRec->LF[0] = '\n';
   pLienRec->LF[1] = 0;
   return 0;
}

int Lax_ExtrVal_Old()
{
   char      *pTmp, acBuf[1024], acRec[MAX_RECSIZE], acTmpFile[_MAX_PATH], acYear[8];
   int       iRet;
   long      lOut=0, lCnt=0;
   FILE      *fdOut, *fdIn;

   LogMsg0("Extracting final values from Tax file.");

   // Open input file
   GetIniString(myCounty.acCntyCode, "TaxDetail", "", acTmpFile, _MAX_PATH, acIniFile);
   LogMsg("Open tax file %s for input", acTmpFile);
   fdIn = fopen(acTmpFile, "r");
   if (fdIn == NULL)
   {
      LogMsg("***** Error opening Current tax file: %s\n", acTmpFile);
      return -2;
   }  

   // Open out file
   sprintf(acTmpFile, acValueTmpl, myCounty.acCntyCode, myCounty.acCntyCode);
   LogMsg("Open file %s for output", acTmpFile);
   fdOut = fopen(acTmpFile, "w");
   if (fdOut == NULL)
   {
      LogMsg("***** Error creating output file: %s\n", acTmpFile);
      return -4;
   }

   // Set extract year
   sprintf(acYear, "%.2d000", lLienYear-2000);

   // Start loop 
   while (!feof(fdIn))
   {
      pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdIn);
      if (!pTmp)
         break;

      // Parse input record
      iTokens = ParseStringIQ(acRec, cDelim, 256, apTokens);
      if (iTokens < SEC_COMBINELIENKEY)
      {
         if (iTokens > SEC_APN)
            LogMsg("*** Bad record: %s", apTokens[SEC_APN]);
         continue;
      }

      if (memcmp(apTokens[SEC_PRCLHIST], acYear, 5))
         continue;

      // Create new base record
      iRet = Lax_CreateValueRec_Old(acBuf);
      if (!iRet)
      {
         fputs(acBuf, fdOut);
         lOut++;
      } else
      {
         LogMsg("---> Drop record %d [APN=%s, TaxYear=%s]", lCnt, apTokens[SEC_APN], apTokens[SEC_PRCLHIST]); 
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   LogMsg("Total records processed: %u", lCnt);
   LogMsg("Total records output:    %u", lOut);

   // Close files
   if (fdIn)
      fclose(fdIn);
   if (fdOut)
      fclose(fdOut);

   return iRet;
}

/********************************* Lax_ExtrVal *****************************
 *
 * Extract final values from TTC_Secured_Property_Taxes.csv
 * For MH properties:
 *    OtherVal= SEC_MHVAL+SEC_MHOTHER
 *    OtherExe= SEC_OTHEXEVAL+SEC_FIXTEXEVAL+SEC_PPEXEVAL
 * Return number of records
 *
 ***************************************************************************/

int Lax_CreateValueRec(char *pOutbuf)
{
   char     acTmp[256];
   long     iTmp;
   LIENEXTR *pLienRec = (LIENEXTR *)pOutbuf;
   LONGLONG lTmp;

   // Clear output buffer
   memset(pOutbuf, ' ', sizeof(LIENEXTR));

   // Start copying data
   vmemcpy(pLienRec->acApn, apTokens[STR_APN], RSIZ_APN);

   // Year assessed
   memcpy(pLienRec->acYear, myCounty.acYearAssd, 4);

   // Land
   long lLand = atol(apTokens[STR_LANDVAL]);
   if (lLand > 0)
   {
      iTmp = sprintf(acTmp, "%*u", sizeof(pLienRec->acLand), lLand);
      memcpy(pLienRec->acLand, acTmp, iTmp);
   }

   // Improve
   long lImpr = atol(apTokens[STR_IMPRVAL]);
   if (lImpr > 0)
   {
      iTmp = sprintf(acTmp, "%*u", sizeof(pLienRec->acImpr), lImpr);
      memcpy(pLienRec->acImpr, acTmp, iTmp);
   }

   // MH Values
   long lMHVal = atol(apTokens[STR_MHVAL]) + atol(apTokens[STR_MHOTHER]);
   long lMHExeVal = atol(apTokens[STR_MHEXEVAL]) + atol(apTokens[STR_MHOTHEXEVAL]);

   // Other values
   long lPers = atol(apTokens[STR_PPVAL]);
   long lFixture = atol(apTokens[STR_FIXTVAL]);
   lTmp = lPers + lFixture;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*u", SIZ_LIEN_FIXT, lTmp);
      memcpy(pLienRec->acOther, acTmp, SIZ_OTHER);

      if (lPers > 0)
      {
         iTmp = sprintf(acTmp, "%*u", sizeof(pLienRec->acPP_Val), lPers);
         memcpy(pLienRec->acPP_Val, acTmp, iTmp);
      }

      if (lFixture > 0)
      {
         iTmp = sprintf(acTmp, "%*u", sizeof(pLienRec->acME_Val), lFixture);
         memcpy(pLienRec->acME_Val, acTmp, iTmp);
      }
   } else if (lMHVal > 0)
   {
      sprintf(acTmp, "%*u", SIZ_LIEN_FIXT, lMHVal);
      memcpy(pLienRec->acOther, acTmp, SIZ_OTHER);
   }

   // Gross total
   lTmp += (lLand+lImpr);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*u", SIZ_LIEN_FIXT, lTmp);
      memcpy(pLienRec->acGross, acTmp, SIZ_LIEN_FIXT);
   }

   // Ratio
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*u", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
      memcpy(pLienRec->acRatio, acTmp, SIZ_RATIO);
   }

   // HO Exempt
   long lExe = atol(apTokens[STR_HOEXEVAL]);
   if (lExe > 0)
   {
      pLienRec->acHO[0] = '1';      // 'Y'
      iTmp = sprintf(acTmp, "%*u", SIZ_LIEN_HO_EXE, lExe);
      memcpy(pLienRec->extra.Lax.acHOExe, acTmp, iTmp);
      memcpy(pLienRec->extra.Lax.Exe_Code1, "10", 2);
   } else
      pLienRec->acHO[0] = '2';      // 'N'

   long lRelExe = atol(apTokens[STR_OTHEXEVAL]);
   if (lRelExe > 0)
   {
      iTmp = sprintf(acTmp, "%*u", SIZ_LIEN_REL_EXE, lRelExe);
      memcpy(pLienRec->extra.Lax.acRelExe, acTmp, iTmp);
   }

   // PP Exe
   long lPPExe = atol(apTokens[STR_PPEXEVAL]);
   if (lPPExe > 0)
   {
      iTmp = sprintf(acTmp, "%*u", SIZ_LIEN_PERS_EXE, lPPExe);
      memcpy(pLienRec->extra.Lax.acPP_Exe, acTmp, iTmp);
   }

   // Fixt Exe
   long lFixtExe = atol(apTokens[STR_FIXTEXEVAL]);
   if (lFixtExe > 0)
   {
      iTmp = sprintf(acTmp, "%*u", SIZ_LIEN_FIXT_EXE, lFixtExe);
      memcpy(pLienRec->extra.Lax.acME_Exe, acTmp, iTmp);
   }

   // Total Exe
   lTmp = lExe+lFixtExe+lPPExe+lRelExe+lMHExeVal;
   if (lTmp > 0)
   {
      iTmp = sprintf(acTmp, "%*u", sizeof(pLienRec->acExAmt), lTmp);
      memcpy(pLienRec->acExAmt, acTmp, iTmp);
   }

   pLienRec->LF[0] = '\n';
   pLienRec->LF[1] = 0;
   return 0;
}

int Lax_ExtrVal()
{
   char      *pTmp, acBuf[1024], acRec[MAX_RECSIZE], acTmpFile[_MAX_PATH], acYear[8];
   int       iRet;
   long      lOut=0, lCnt=0;
   FILE      *fdOut, *fdIn;

   LogMsg0("Extracting final values from Tax file.");

   // Open Secured Tax Roll
   GetIniString(myCounty.acCntyCode, "TaxBase", "", acTmpFile, _MAX_PATH, acIniFile);
   LogMsg("Open tax file %s for input", acTmpFile);
   fdIn = fopen(acTmpFile, "r");
   if (fdIn == NULL)
   {
      LogMsg("***** Error opening Current tax file: %s\n", acTmpFile);
      return -2;
   }  

   // Open out file
   sprintf(acTmpFile, acValueTmpl, myCounty.acCntyCode, myCounty.acCntyCode);
   LogMsg("Open file %s for output", acTmpFile);
   fdOut = fopen(acTmpFile, "w");
   if (fdOut == NULL)
   {
      LogMsg("***** Error creating output file: %s\n", acTmpFile);
      return -4;
   }

   // Set extract year
   sprintf(acYear, "%.2d000", lLienYear-2000);

   // Start loop 
   while (!feof(fdIn))
   {
      pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdIn);
      if (!pTmp)
         break;

      // Parse input record
      iTokens = ParseString(acRec, cDelim, 256, apTokens);
      if (iTokens < STR_FLDS)
      {
         if (iTokens > STR_APN)
            LogMsg("*** Bad record: %s", apTokens[STR_APN]);
         continue;
      }

      if (memcmp(apTokens[STR_PARCEL_HISTORY_YRSEQ], acYear, 5))
         continue;

      // Create new base record
      iRet = Lax_CreateValueRec(acBuf);
      if (!iRet)
      {
         fputs(acBuf, fdOut);
         lOut++;
      } else
      {
         LogMsg("---> Drop record %d [APN=%s, TaxYear=%s]", lCnt, apTokens[STR_APN], apTokens[STR_PARCEL_HISTORY_YRSEQ]); 
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   LogMsg("Total records processed: %u", lCnt);
   LogMsg("Total records output:    %u", lOut);

   // Close files
   if (fdIn)
      fclose(fdIn);
   if (fdOut)
      fclose(fdOut);

   return iRet;
}

/*********************************** loadLax ********************************
 *
 * LDR options:      -CLAX -Xl -Ms -L -O -Ll -Xr -Xh -Mg [-La]
 * Monthly update:   -CLAX -U -Us -O -Xr -Xd [-G|-Mg] [-Ua]
 * Testing           -CLAX -U -O -G
 *
 ****************************************************************************/

int loadLax(int iSkip)
{
   int   iRet=0;
   char  acLUFile[_MAX_PATH], acTmpFile[_MAX_PATH], acTmp[256];

   // Preset APN length for the whole process
   iApnLen = myCounty.iApnLen;

   // Prepare workdrive
   GetIniString("System", "WorkDrive", "", acWorkDrive, _MAX_PATH, acIniFile);

   // One time run
   //iRet = ResetSaleCode("H:\\CO_PROD\\SLAX_CD\\Raw\\LAX_Sale.Dat", acCSalFile);
   //strcpy(acTmp, "S(1,14,C,A,27,8,C,A,15,12,C,A,57,10,C,D) F(TXT) DUPO(B1024,1,34) ");
   //iRet = SetMultiSale(acCSalFile, acTmp, myCounty.acCntyCode);
   //iRet = Lax_ExtrExe();

   // Set extended due date
   TC_SetDateFmt(0, true);

   // Loading Tax
   if (iLoadTax == TAX_LOADING)                    // -T or -Lt
   {
      // Loading tax rate file if available
      iRet = GetIniString(myCounty.acCntyCode, "TaxRate", "", acTmpFile, _MAX_PATH, acIniFile);
      if (iRet > 0 && !_access(acTmpFile, 0))
      {
         iRet = LoadTaxRateTable(acTmpFile);
         if (iRet < 100)
            LogMsg("***** Bad tax rate table %s.  Please verify", acTmpFile);
      }

      // Check file size and determine how to load
      GetIniString(myCounty.acCntyCode, "TaxDetail", "", acTmpFile, _MAX_PATH, acIniFile);
      _int64 iTmp = getFileSize(acTmpFile);

      // LAX has base, delq, detail, agency
      if (iTmp > 100000000)
         iRet = Lax_Load_TaxBaseCsv(bTaxImport);
      else
      {
         LogMsg("*** Rerouting tax processing ...");
         iRet = Lax_Updt_TaxBase(bTaxImport);
      }

      if (!iRet && lLastTaxFileDate > 0)
      {
         iRet = Lax_Load_TaxDelq(bTaxImport);
         if (!iRet)
            iRet = updateDelqFlag(myCounty.acCntyCode);
      }
   } else if (iLoadTax == TAX_UPDATING)            // -Ut
   {
      // Load tax base to tmp table then merge into TaxBase
      //iRet = Lax_Updt_TaxBase(bTaxImport);
      iRet = Lax_Updt_SecuredTaxRoll(bTaxImport);

      if (!iRet)
         iRet = Lax_Load_DefaultTaxRoll(bTaxImport);
      if (!iRet)
         iRet = updateDelqFlag(myCounty.acCntyCode);
   }

   if (!iLoadFlag && !lOptExtr && !lOptMisc)
      return iRet;

   // Extract final value
   if (lOptExtr & EXTR_FVAL)                       // -Xf
      iRet = Lax_ExtrVal();

   // Extract legal.  Use as needed
   if (iLoadFlag & EXTR_DESC)                      // -Xd
   {
      // Get list of APN to extract legal
      iRet = GetIniString(myCounty.acCntyCode, "ParcelFile", "", acTmp, _MAX_PATH, acIniFile);
      if (iRet > 1)
      {
         if (!_access(acTmp, 0))
         {
            // Prepare output file
            iRet = GetIniString(myCounty.acCntyCode, "LegalFile", "", acTmpFile, _MAX_PATH, acIniFile);
            iRet = Lax_LegalExtr(acTmp, acTmpFile);
         } else
         {
            LogMsg("*** Missing parcel file: %s", acTmp);
            LogMsg("*** Ignore extracting legal");
            iRet = 0;
         }
      } else
         iRet = Lax_LegalExtr(NULL, NULL);
   }

   // Extract Prop8 data
   if (lOptProp8 == MYOPT_EXT)
   {
      iRet = Lax_ExtrProp8();
      return iRet;
   }

   // Convert old sale file to new format
   /*
   if (bConvSale)                                  // -D1
   {
      GetPrivateProfileString(myCounty.acCntyCode, "SaleCum", "", acTmpFile, _MAX_PATH, acIniFile);
      iSaleLen = GetPrivateProfileInt(myCounty.acCntyCode, "CumSaleLen", 240, acIniFile);
      iRet = Lax_ConvertSale(myCounty.acCntyCode, acTmpFile, acCSalFile, true);
   }
   */

   // Load Xref file
   if (lOptExtr & EXTR_MH)                         // -Xh
   {
      GetIniString(myCounty.acCntyCode, "XrefFile", "", acTmpFile, _MAX_PATH, acIniFile);
      LogMsg0("Extract %s MH records.", myCounty.acCntyCode);
      iRet = Lax_ExtrMH(acTmpFile);
   }

   // Extract sales and update sale history
   if (iLoadFlag & EXTR_SALE)                      // -Us or -Xs
   {
      // Update history sale using salefile.txt
      Load_Sale:
      LogMsg0("Update %s sale history file", myCounty.acCntyCode);
      iSaleLen = GetPrivateProfileInt(myCounty.acCntyCode, "CurSaleLen", 242, acIniFile);
      iRet = Lax_ConvertSale(myCounty.acCntyCode, acSaleFile, acCSalFile, false);
   } else if (!_access(acCSalFile, 0) && !_access(acSaleFile, 0))
   {
      long lSalefileDate, lCSalfileDate;

      lSalefileDate = getFileDate(acSaleFile);
      lCSalfileDate  = getFileDate(acCSalFile);

      if (lCSalfileDate < lSalefileDate)
         goto Load_Sale;
   }

   // Extract NDC recorder sale 
   if (iLoadFlag & EXTR_NRSAL)                     // -Xn
   {
      iRet = GetIniString(myCounty.acCntyCode, "NdcSale", "", acSaleFile, _MAX_PATH, acIniFile);
      if (!_access(acSaleFile, 0))
      {
         iRet = NR_CreateSCSale(myCounty.acCntyCode, acSaleFile, iApnLen);
         if (iRet)
            return iRet;
      } else
      {
         LogMsg("*** Sale file not available.  Please verify: %s", acSaleFile);
         return -1;
      }
   }

   // Load TRA xref table
   /* Save for future use
   bUseMdb = true;
   if (bFixTRA || (iLoadFlag & (LOAD_LIEN|LOAD_UPDT)) )
   {
      char acServer[_MAX_PATH];
      GetPrivateProfileString("LAX", "TRAXref", "", acServer, _MAX_PATH, acIniFile);
      if (!acServer[0])
      {
         GetPrivateProfileString("LAX", "CntyProvider", "", acServer, 128, acIniFile);
         if (!acServer[0])
            return -1;
         bUseMdb = false;
      }

      // Open char file
      LogMsg("Open TRA-Xref file %s", acServer);
      try
      {
         if (bUseMdb)
         {
            strcpy(acTmp, acMdbProvider);
            strcat(acTmp, acServer);
         } else
            strcpy(acTmp, acServer);

         dbTra.Connect(acTmp);
      } AdoCatch(e)
      {
         LogMsg("***** Error opening %s (%s)", acTmpFile, ComError(e));
         return -1;
      }
   }
   */

   // Load Deed xref table
   //GetPrivateProfileString("System", "DeedXref", "", acTmp, _MAX_PATH, acIniFile);
   //iNumDeeds = LoadXrefTable(acTmp, (XREFTBL *)&asDeed[0], MAX_DEED_ENTRIES);

   // Load GrGr
   bUseGrGr = false;
   if (iLoadFlag & LOAD_GRGR)                      // -G
   {
      // Connect to SQL
      iRet = GetIniString(myCounty.acCntyCode, "CntyProvider", "", acTmp, 128, acIniFile);
      if (iRet < 10)
         GetIniString("Database", "CntyProvider", "", acTmp, 128, acIniFile);
      sprintf(acTmpFile, acTmp, myCounty.acCntyCode);
      if (!sqlConnEx(acTmpFile, &dbLoadConn))
         return -1;

      LogMsg0("Load %s GrGr file", myCounty.acCntyCode);
      iRet = Lax_LoadGrGr(myCounty.acCntyCode);

      // Extract data and turn on MERG_GRGR flag
      if (iRet < 0)
      {
         bGrGrAvail = false;
         return iRet;
      } else if (iRet > 0)
      {
         // Extract GrGr data to file - LaxGrGr.dat
         sprintf(acTmpFile, acGrGrTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "Dat");
         iRet = extrGrGrDoc(acTmpFile);

         if (iRet > 0)
         {
            bool bAppend = true;

            // Append to GrGr_Exp.sls
            bUseGrGr = true;

            // Sort output file and dedup - order APN, DocDate, DocNum
            char  acCumGrGr[_MAX_PATH];
            sprintf(acCumGrGr, acGrGrTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "sls");
            if (!_access(acCumGrGr, 0))
               sprintf(acTmp, "%s+%s", acTmpFile, acCumGrGr);
            else
            {
               bAppend = false;
               strcpy(acTmp, acTmpFile);
            }
            sprintf(acTmpFile, acGrGrTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "srt");
            iRet = sortFile(acTmp, acTmpFile, "S(17,10,C,A,37,8,C,A,1,12,C,A) F(TXT) DUPO(1,44) ");
            if (iRet > 0)
            {
               if (bAppend)
               {
                  sprintf(acTmp, acGrGrTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "sav");
                  iRet = MoveFileEx(acCumGrGr, acTmp, MOVEFILE_REPLACE_EXISTING);
               }

               LogMsg("Rename %s to %s", acTmpFile, acCumGrGr);
               iRet = MoveFileEx(acTmpFile, acCumGrGr, MOVEFILE_REPLACE_EXISTING);
               if (iRet)
               {
                  iRet = 0;
                  //iLoadFlag |= MERG_GRGR;
               } else
                  iRet = -1;
            }
         }
      } else
      {  
         bGrGrAvail = false;
      }
   }

   // Load Region table
   GetIniString(myCounty.acCntyCode, "RegsFile", "", acLUFile, _MAX_PATH, acIniFile);
   iRet = loadRegionTbl(acLUFile);
   if (!iRet)
   {
      LogMsg("***** Error loading Region table %s", acLUFile);
      return 1;
   } else
      iRet = 0;

   // Concatenate input files
   if (strchr(acRollFile, '%'))
   {
      if (iLoadFlag & (LOAD_UPDT|EXTR_ATTR))
      {
         char sRoll1[_MAX_PATH], sRoll2[_MAX_PATH];

         // Combine roll file
         sprintf(sRoll1, acRollFile, "_1");
         sprintf(sRoll2, acRollFile, "_2");
         sprintf(acTmpFile, acRollFile, "");
         strcpy(acRollFile, acTmpFile);
         lLastFileDate = getFileDate(sRoll1);

         iRet = chkFileDateTime(sRoll1, acRollFile);
         if (iRet != 2)
         {
            try {
               CopyFile(sRoll1, acRollFile, false);
               iRet = appendTxtFile(sRoll2, acRollFile);
            } catch (...)
            {
               LogMsg("***** Error appending input file %s and %s", sRoll1, sRoll2);
               iRet = -1;
            }
            if (iRet)
               return iRet;
         }
      }
   }

   if (iLoadFlag & EXTR_LIEN)                      // -Xl
   {
      LogMsg0("Extract %s Lien file", myCounty.acCntyCode);
      iRet = Lax_ExtrLien();
   }

   if (iLoadFlag & EXTR_ATTR)                      // -Xa
   {
      LogMsg0("Extract %s CHARS file", myCounty.acCntyCode);
      iRet = Lax_ExtrChar(acRollFile);
   }

   lUpdXfer = 0;
   if (iLoadFlag & LOAD_LIEN)                      // -L
   {
      bUseGrGr = true;

      // Create Lien file
      LogMsg0("Load %s Lien file", myCounty.acCntyCode);
      iRet = Lax_Load_LDR(iSkip);
   } else if (iLoadFlag & LOAD_UPDT)               // -U
   {

      LogMsg0("Load %s roll update file", myCounty.acCntyCode);
      iRet = Lax_Load_Roll(iSkip);
   }

   if (!iRet && bMergeOthers)
   {
      sprintf(acTmpFile, acLienTmpl, myCounty.acCntyCode, myCounty.acCntyCode);
      if (!_access(acTmpFile, 0))
         iRet = PQ_MergeOthers(myCounty.acCntyCode, acTmpFile, GRP_LAX, iSkip);
      else
         LogMsg("***** Lien extract is missing: %s.  Please locate the file and retry", acTmpFile);
   }

   // Append Cross Ref Roll to R03 file
   if (!iRet && (iLoadFlag & (LOAD_LIEN|LOAD_UPDT)) )
   {
      char acOutFile[_MAX_PATH], acRawFile[_MAX_PATH], acMHFile[_MAX_PATH];

      sprintf(acMHFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "MBH");
      if (!_access(acMHFile, 0))
      {
         sprintf(acOutFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "N03");
         sprintf(acRawFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "R03");
         LogMsg0("Append %s to %s", acMHFile, acRawFile);

         // Use OTSort for this task
         sprintf(acTmp, "S(1,14,C,A) F(FIX,%d) ", iRecLen);
         sprintf(acTmpFile, "%s+%s", acRawFile, acMHFile);

         long lTmp;
         lTmp = sortFile(acTmpFile, acOutFile, acTmp, &iRet, acWorkDrive);
         if (lTmp > 400000)
         {
            LogMsg("Rename %s to %s", acOutFile, acRawFile);
            DeleteFile(acRawFile);
            iRet = rename(acOutFile, acRawFile);
         }
      }
   }

   // This section is not needed since -U already update county sales
   // Apply cum sale file to R01
   //if (!iRet && (iLoadFlag & MERG_CSAL) )          // -Ms
   //{
   //   // Apply Lax_Sale.sls to R01 file
   //   sprintf(acCSalFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "sls");
   //   iRet = ApplyCumSale(iSkip, acCSalFile, false, SALE_USE_SCUPDXFR, CLEAR_OLD_SALE);
   //}

   // Apply NDC sale file to R01
   if (!iRet && (iLoadFlag & UPDT_XSAL))           // -Mn
   {
      // Apply Lax_Ash.sls to R01 file
      GetIniString("Data", "ASH_File", acESalTmpl, acESalTmpl, _MAX_PATH, acIniFile);
      sprintf(acCSalFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "sls");
      iRet = ApplyCumSaleNR(iSkip, acCSalFile, SALE_USE_SCUPDXFR, 0, 3);
      if (!iRet)
         iLoadFlag |= LOAD_UPDT;
   }

   // Merge GrGr
   if (!iRet && (iLoadFlag & MERG_GRGR))           // -Mg
   {
      LogMsg0("Merge %s GrGr data", myCounty.acCntyCode);
      iRet = MergeGrGrDoc(myCounty.acCntyCode);
      if (iRet)
         iLoadFlag = 0;
      else
      {
         if (!(iLoadFlag & LOAD_LIEN))
            iLoadFlag |= LOAD_UPDT;
      }
   }

   if (iLoadFlag & (MERG_GRGR | UPDT_XSAL))
   {
      // Format DocLinks - Doclinks are concatenate fields separated by comma 
      if (!iRet)
      {
         iRet = updateDocLinks(Lax_MakeDocLink, myCounty.acCntyCode, iSkip, 0, 1);
         iRet = updateDocLinks(Lax_MakeDocLink, myCounty.acCntyCode, 0, 0, 2);
         iRet = updateDocLinks(Lax_MakeDocLink, myCounty.acCntyCode, 0, 0, 3);
      }
   }

   if (!iRet && (iLoadFlag & EXTR_REGN))           // -Xr
   {
      //GetIniString(myCounty.acCntyCode, "RegsFile", "", acLUFile, _MAX_PATH, acIniFile);
      //iRet = loadRegionTbl(acLUFile);
      //if (!iRet)
      //{
      //   LogMsg("***** Error loading Region table %s", acLUFile);
      //   return 1;
      //}

      LogMsg0("Create %s region files", myCounty.acCntyCode);
      iRet = Lax_CreateRegions(iSkip);
   }

   // Fix TRA
   //if (bFixTRA)
   //   iRet = Lax_FixTRA(iSkip);

  return iRet;
}