#ifdef _FORMAT_APN
typedef struct _BkNdx
{
   int   iBook;
   int   iStart;
   int   iEnd;
} BKNDX;

struct LakeApnBk
{
    char *pApn;
    int   iPageLen;
};
struct LakeApnBk gLakeApnBk[] = 
{  // default page length is 3 for APN <= first entry
   // If APN > defined APN, page length is changed to the one on same row  
   { "00603399", 2 },     // Start at APN 00603400, use first 2 page digits
   { "00699999", 3 },     // Currently only up to 00656999
   { "00806599", 2 },
   { "00899999", 3 },     // Currently only up to 00873999
   { "01400699", 2 },
   { "01499999", 3 },     // Currently only up to 01451999
   { "02399999", 2 },     // Currently only up to 02300599
   { "11499999", 3 },     // Currently only up to 11410999
   { "11599999", 2 },     // Currently only up to 11501899
   { "99999999", 3 }      // Currently only up to 62837999
};
#define MAXLAKEBLK      10

BKNDX    g_aBkNdx[100];
char     g_asBkPg[8000][8];

#endif

int   formatApn(char *pApn, char *pFmtApn, COUNTY_INFO *pCounty);
int   formatMapLink(char *pApn, char *pMapLink, COUNTY_INFO *pCounty);
int   loadMapIndex(char *pCntyCode, char *pMapList=NULL);
char *getIndexPage(char *pMapLink, char *pIndexPage, COUNTY_INFO *pCounty);
