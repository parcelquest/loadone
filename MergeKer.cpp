/*****************************************************************************
 *
 * Input files:
 *    - RBPR.TXT         (roll file, 422-byte fixed)
 *    - RESCHAR.TXT      (char file, comma delimited) - 2006
 *    - DOCM.TXT         (sale file, 242-byte )
 *    - LEGL.TXT         (legal file, 152-byte )
 *    - SITS.TXT         (situs file, 82-byte )
 *    - UFAR.TXT         (unformated mailadr file, 122-byte )
 *    - All_Rolls_9GBN2022.TXT (lien roll, 542-byte ascii) - 2009
 *    - char0709.txt     (new char file, delimited) - 2009
 *    - CURB.TXT         (current tax bill, 722 bytes) - 2016
 *
 * Notes: 
 *    1) ATN is used as APN and APN is named as AltApn.(for reference only)
 *    2) Need to remove extra space in LEGAL.    
 *    3) Ker_Bldg.txt contains multiple char records for a single APN.  
 *    4) Ker_Char.txt contains single APN only.
 *
 * Problems:
 *    1) CHAR.ASC is in ATN order, RBPR.DSS, LEGL.DSS, SITS.DSS, and UFAR.DSS
 *       are in TE_NO order.  DOCM.DSS is in DOC_NO order.  We have to sort DOCM
 *       on TE_NO order and process CHAR independently.
 *    2) RBPR.DSS may have record without ATN, we have to remove them.
 *    3) Lien values are increased right after lien date release.  SO Lien extract
 *       is needed on LDR for monthly update.
 *    4) Since input roll and output R01 are in different sort order, we have
 *       to generate two different version of Lien extract.  One in Te_No order (Ker_Lien.dat)
 *       and one in ATN order (Lien_exp.Ker)
 *       
 * Processing:
 *    1) Extract sale for cumulative file (to be done later)
 *    2) Load roll file with situs, legal, mailing and sale.  Sort result on ATN.
 *    3) On lien date, create lien extract for monthly update
 *    4) Convert CHAR file if needed then update R01 with CHAR data
 *
 * To be done:
 *    1) Populate mail addr using Ker_MergeMailing()
 *
 * Commands:
 *    -Xa   Extract characteristics
 *    -Xf   Extract final value
 *    -Xl   Extract lien file
 *    -Xs   Extract sale
 *    -Mf   Merge final value (create .V01 file)
 *    -Ms   Merge cum sale
 *    -Mz   Merge PQZoning
 *    -U    Monthly update
 *    -L    Load LDR
 *
 *    LDR command: -CKER -L -Xa -Xl -Dr -Ms -Mz -O
 *    Monthly cmd: -CKER -U -Xs -Xa [-Mz] [-Xf|-Mf] 
 *    Load tax:    -CKER -T
 *
 * Revision:
 * 07/21/2006 1.2.27.6  Rename convertChar() to Ker_ConverChar() and drop
 *                      the header record since they don't have it this year.
 * 08/22/2006 1.2.32.2  New sale format changes from 305 bytes to 242 bytes. Remove sale code.
 * 03/17/2008 1.5.6     Use standard function to update usecode
 * 03/24/2008 1.5.7.2   Format IMAPLINK.
 * 08/13/2008 8.1.3     Comment out Ker_MergeMailing() since this function need implementation
 *                      Change sorting order for sale file.
 * 03/16/2009 8.7.0     Format STORIES as 99.0.  We no longer multiply by 10.
 * 04/25/2009 8.7.2     Adding extract lien and merge other values.
 * 07/22/2009 9.1.3     Modify Ker_ExtrLien() to output two copies, one in ATN sort 
 *                      order and the other in TE_NO order.  Change the whole logic of
 *                      updating lien value in Ker_Load_Roll();
 * 02/16/2010 9.4.2     Merge long LEGAL into LEGAL1.
 * 02/23/2010 9.4.3     Extend CARE_OF field length.
 * 07/01/2010 10.0.1    Add Ker_ExtrRoll() to extract roll data from access file.
 *                      KER is now sending us data in ACCDB format.  Use Access 2007 to open
 *                      then save it to MDB 2000 format.  See INI file for appropriate filename.
 * 07/14/2010 10.1.3    Fix LEGAL in Ker_MergeLegal() and Ker_MergeLien().
 * 05/05/2011 10.5.7    Create cum sale file. Store TE_NO in OTHER_APN instead of ALT_APN.
 * 05/14/2011 10.5.9    Bug fix that Sale_MatchRoll() returns number of records but
 *                      thought it was error.  Reset to 0 before return to calling function.
 * 05/23/2011 10.5.13   Fix bad character in Ker_MergeMAdr().
 * 07/12/2011 11.0.2    Add S_HSENO.  If county supply LDR file in text file, we must sort
 *                      first before processing.
 * 03/12/2012 11.6.10   Modify Ker_ConvertChar() to support new char format.
 * 07/19/2012 12.1.2    Modify Ker_Load_LDR() to update legal using legal file, ignore
 *                      legal from legal field.  This will make data compatible with monthly update.
 * 07/29/2012 12.2.3    Fix swap name in Ker_MergeOwner(). Fix zipcode in Ker_MergeMAdr().
 * 02/14/2013 12.4.5    Update to support new data file format.  This version will support both new and old
 *                      layout from different data vendor.
 * 03/12/2013 12.4.9    Correct bad char in Ker_MergeOwner(), Ker_MergeLegal(), & Ker_FormatCSale().
 *                      Stop update sale in Ker_Load_Roll(), use ApplyCumSale() instead.
 *.04/10/2013 12.5.3    Add Ker_FmtDocLinks() and populate DocLink in the same manner as TUO.
 * 04/28/2013 12.6.1    Change parameter in Ker_MakeDocLink()
 * 06/19/2013 12.7.1    Work on new CHAR & BLDG files.
 * 07/21/2013 13.0.6    Made change to process LDR 2013 using old file format.
 * 09/11/2013 13.7.14   Modify Ker_MergeCVal() and Ker_MergeBldg() to add Heat & BsmtSqft.
 *                      Break Ker_Roll.txt into KerRoll.txt and Ker_Value.txt to better handle
 *                      data update while county has not update new value yet. Modify Ker_MergeRoll2()
 *                      to merge value separately and create Ker_MergeValue2() for this.
 * 09/12/2013 13.8.1    Fix Ker_ExtrSale() - merge new sales with history sale file since
 *                      new sale file might exclude some old ones.
 * 09/20/2013 13.8.2    Add Ker_CreateRBPRRec() & Ker_ExtrRBPR() to create lien extract using
 *                      RBPR.TXT file format. Clean up code in Ker_Load_Roll().
 * 09/24/2013 13.8.3    Fix bug in Ker_ExtrRBPR() that drops last digit in every value.
 * 09/26/2013 13.8.5    Add Patio & Porch sqft.
 * 10/04/2013 13.10.2.2 Modify Ker_MergeOwner2() to add Vesting.
 * 10/11/2013 13.11.0   Add all QBaths to R01
 * 10/24/2013 13.11.6.1 Filter out Units > 1000.
 * 07/30/2014 14.2.1    Modify Ker_ExtrLien(), Ker_Load_LDR() for new LDR format.
 *                      Modify Ker_MergeRoll2() to match legal wording of county and
 *                      set AG_PRE to 'Y' when ROLL_AGPRESERVEDCODE is not blank.
 * 09/04/2014 14.4.3    Add Ker_ValidateInput() to make sure data files are ready before process.
 * 12/02/2014 14.10.0   Modify Ker_MakeDocLink() to fix doc path, replace Ker_FmtDocLinks() with UpdateDocLinks().
 *                      Modify Ker_MergeOwner2() to fix known bad characters in OwnerNames.
 * 03/06/2015 14.12.3   Modify Ker_UpdateLien2() and Ker_MergeValue2() to upate full exemption flag
 *                      if EXE_VAL >= GROSS_VAL.
 * 03/13/2015 14.13.0   Change params in updateDocLinks()
 * 03/21/2015 14.13.1   Update Full exemption flag in Ker_MergeRoll()
 * 03/27/2015 14.13.2   Make all public parcel full exempt.
 * 07/24/2015 15.0.5    Fix known bad char in Ker_MergeOwner(), fix bug in Ker_MergeMAdr().
 *                      Use extracted Bldg file in Ker_Load_LDR(). Modify Ker_ValidateInput() to 
 *                      ignore some missing error file when processing LDR.
 * 09/11/2015 15.2.1    Add option to Ker_MergeCVal() to allow update Lot Acres as requested.
 * 11/13/2015 15.3.3    Populate FULLEXE_FLG even when loading LDR.
 * 12/02/2015 15.3.6    KER is reversed to old format. We have to make some adjustment.
 *                      Fix bad char in Ker_MergeOwner(). Fix matching in Ker_MergeSitus().
 *                      Modify Ker_FormatCSale() to set NoneSale_Flg for QUITCLAIM and 
 *                      change sort params. Modify Ker_ValidateInput(). Add Ker_UpdateChar2().
 * 03/16/2016 15.8.1    Load Tax data.  Use CURB.TXT to load Base & Owner tables.
 * 07/19/2016 16.1.0    Modify MergeOwner() to remove known bad char.  Use updateVesting() to update
 *                      vesting in LDR version of MergeOwner().
 * 08/24/2016 16.2.2    Remove TotalFee caculation in Ker_ParseTaxBase().
 * 11/14/2016 16.6.1    Add functions to load tax detail from TAXSRVC.TXT
 *                      Ignore all unsecured records and skip unassessed parcels.
 * 11/18/2016 16.7.0    Modify Ker_FormatCSale2() to update Full/Partial sale on Sale_Code
 * 11/22/2016 16.7.2    Fix bug in Ker_FormatCSale2() to set Full/Partial flag correctly
 * 12/11/2016 16.7.8    Modify Ker_Load_Roll2() to return -1 if there is any no match in roll update.
 * 12/12/2016 16.8.1    Modify Ker_Load_TaxDetail() to unzip TAXSRVC.ZIP if newer.
 *                      Modify Ker_ParseTaxBase() to ignore canceled tax bill and update BillType.
 *                      If Bill_SubType="SS" and set isSupp='1'.
 * 12/19/2016 16.8.3    Set VIEW='A' when HasView='Y'
 * 03/16/2017 16.12.3   Modify Ker_ParseTaxDetail() to keep detail record even no tax amt.
 * 03/20/2017 16.12.5   Modify Ker_ParseTaxBase() to keep Cancel bill but ignore "To be billed" bill.
 *                      Also populate PenAmt only if past due date.
 * 07/16/2017 17.1.2    Modify Ker_ParseTaxDetail() and Ker_Load_TaxDetail() to populate AV line items.
 * 07/23/2017 17.1.4    Add Ker_UpdateDelq() & Ker_Load_TaxDelq(). Modify Ker_Load_TaxBase() to update Delq info.
 * 11/21/2017 17.5.0    Call updateDelqFlg() to populate delqYear & defaultAmt in Tax_Base table.
 * 05/01/2018 17.10.6   Fix bug that left over TaxRate in Ker_ParseTaxDetail()
 * 10/25/2018 18.5.9.1  Modify Ker_ParseTaxDetail() to add phone number to Tax_Agency table.
 * 06/04/2019 18.12.8   Remove *fdLip from Ker_ParseTaxDetail().  Add Ker_MergeApn() to merge APN 
 *                      from roll file when loading LDR. Will be tested on LDR 2019.
 * 07/24/2019 19.0.5    Fix bug in Ker_MergeApn()
 * 10/23/2019 19.3.1    Add Ker_ExtrDist() to create Ker_TaxDist.txt from DIST.TXT
 * 03/05/2020 19.8.2    Modify Ker_ExtrCVal() & Ker_ExtrBldg() create separate CHAR record on APN & CharacterID
 *                      so we can pick the latest update one.  Modify Ker_MergeBldg() not to overwrite
 *                      values being updated by Ker_MergeCVal().
 * 03/10/2020 19.8.3    Modify Ker_ExtrCVal() & Ker_ExtrBldg() to sort output file on APN, CHAR_ID desc.
 * 06/19/2020 20.0.0    Modify Ker_UpdateLien2() to update EXE_CD1.   
 *                      Modify Ker_CreateLienRec() to populate both acExCode and extra.Ker.Exe_Code1.
 * 08/06/2020 20.2.7    Modify Ker_MergeSitus2() to use City2CodeEx() instead of City2Code() to replace
 *                      bad spelling with official city name.  Add Ker_MergeMAdrCsv(), Ker_MergeLienCsv(),
 *                      and Ker_Load_LDR_Csv() to support new LDR format.  Remove -Lc option.
 * 08/10/2020 20.2.8    Add Ker_ExtrLienCsv().
 * 10/28/2020 20.3.6    Modify Ker_MergeCVal() to populate PQZoning.
 * 07/25/2021 21.1.1    Modify loadKer() to support new LDR file fomat.
 * 09/07/2021 21.2.2    Modify Ker_MergeSitus2() to add special case for zipcode 93555.
 * 05/15/2022 21.9.1    Add -Xf to extract final values.
 * 07/14/2022 22.0.1    Remove unused function Ker_FmtDocLink().  Adjust loadKer() for LDR 2022.
 * 07/19/2023 23.0.6    Add Ker_CreateLienRecCsv1() & Ker_MergeLienCsv1() to handle new LDR layout.
 * 07/15/2024 24.0.4    Add [UseChar] to INI file and modify Ker_Load_Roll2() to choose whether 
 *                      to use LandAcres from characteristics file.
 * 07/18/2024 24.0.6    Modify Ker_MergeOwner2() to fix some known bad char.
 *                      Modify Ker_MergeLienCsv1() to add ExeType.
 *
 *****************************************************************************/

#include "stdafx.h"
#include "hlAdo.h"
#include "CountyInfo.h"
#include "Prodlib.h"
#include "R01.h"
#include "RecDef.h"
#include "Tables.h"
#include "Logs.h"
#include "LoadDef.h"
#include "Utils.h"
#include "XlatTbls.h"
#include "doSort.h"
#include "doOwner.h"
#include "doGrGr.h"
#include "formatApn.h"
#include "SaleRec.h"
#include "LoadOne.h"
#include "UseCode.h"
#include "LOExtrn.h"
#include "PQ.h"
#include "Tax.h"
#include "doZip.h"
#include "MergeKer.h"

static   LU_ENTRY tblAttr[MAX_ATTR_ENTRIES];
static   FILE     *fdBldg, *fdChar, *fdLegal, *fdSitus, *fdMAdr, *fdLien, *fdValue, *fdApn;
static   long     lCharSkip, lBldgSkip, lSaleSkip, lLegalSkip, lSitusSkip, lMAdrSkip, 
                  lLienSkip, lValueSkip, lApnSkip;
static   long     lCharMatch, lBldgMatch, lSaleMatch, lLegalMatch, lSitusMatch, lMAdrMatch, 
                  lLienMatch, lValueMatch, lTaxMatch, lApnMatch;
static   char     acSAdrFile[_MAX_PATH], acMAdrFile[_MAX_PATH], acLegalFile[_MAX_PATH], 
                  acBldgFile[_MAX_PATH], acCBldgFile[_MAX_PATH], acValueFile[_MAX_PATH], acApnFile[_MAX_PATH];
static   char     sCnty[32];
static   bool     bNewFormat;


/********************************* Ker_FormatDoc *****************************
 *
 *  - DocNum format : input as 999999
 *    + Before 2002 : 0999999
 *    + 2002-present: X999999
 *      X = 1 if sale occurs after August and first digit of DocNum <= 5
 *      X = 0 otherwise
 *
 * Notes: This logic may need adjustment in a later date when KER is growing
 *        bigger than 1.2 million parcels.
 *
 *****************************************************************************/

int Ker_FormatDoc(char *pResult, char *pDocNum, char *pDocDate)
{
/*
   long  lYear, lMonth;

   memcpy(pResult+1, pDocNum, RSIZ_REC_DOCNUM);
   *pResult = '0';
   *(pResult+RSIZ_REC_DOCNUM+1) = 0;

   lYear = atoin(pDocDate, 4);
   if ((lYear >= 2002) && (memcmp(pDocDate+4, "08", 2) > 0) )
   {
      if (*pDocNum <= 5)
         *pResult = '1';
   }
*/
   return 0;
}

/******************************** Ker_MergeOwner *****************************
 *
 * Multiple owners are separate by '/'
 *    SANDERS JASON/STEPHAN
 *    ROBLEDO JOSE/ GONZALES SUZETTE
 *    COMPTON EARL W/ MARLENA/ E
 *    BOWLING TERESA R/LEE ATHA DARLENE
 *    COMPTON EARL W/ MARLENA/ ET AL
 *    HUGHES MAXINE EST/58 FILED
 *
 *****************************************************************************/

void Ker_CleanOwner(char *pOrgName, char *pClnName)
{
   char  acOwner[128], acTmp[128];
   char  *pTmp, *pTmp1;

   strcpy(acOwner, pOrgName);
   pTmp = strchr(acOwner, '/');
   if (pTmp)
   {
      // If multiple '/' found, keep first one only
      pTmp1 = strchr(pTmp+1, '/');
      if (pTmp1)
         *pTmp1 = 0;

      // Ignore if EST is present
      if (strstr(acOwner, " EST"))
         strcpy(pClnName, acOwner);
      else
      {
         *pTmp = 0;
         if (strchr(acOwner, '&'))
         {
            // If there is an '&' before '/', leave all of them alone
            // COMPTON EARL W & MARLENA/ JONES EILEEN
            *pTmp = '/';
            strcpy(pClnName, acOwner);
         } else
         {
            sprintf(acTmp, "%s & %s", acOwner, pTmp+1);
            blankRem(acTmp);
            strcpy(pClnName, acTmp);
         }
      }
   } else
   {
      strcpy(pClnName, pOrgName);
   }
}

int Ker_CleanName(char *pSrcName, char *pDstName, char *pVesting)
{
   char  acOwner[128], acTmp[128];
   char  *pTmp, *pTmp1;

   strcpy(acOwner, pSrcName);
   pTmp = findVesting(acOwner, pVesting);
   if (pTmp)
      *pTmp = 0;
   else
      *pVesting = 0;

   pTmp = strchr(acOwner, '/');
   if (pTmp)
   {
      // If multiple '/' found, keep first one only
      pTmp1 = strchr(pTmp+1, '/');
      if (pTmp1)
         *pTmp1 = 0;

      // Ignore if EST is present
      if (*pVesting > ' ')
         strcpy(pDstName, acOwner);
      else
      {
         *pTmp = 0;
         if (strchr(acOwner, '&'))
         {
            // If there is an '&' before '/', leave all of them alone
            // COMPTON EARL W & MARLENA/ JONES EILEEN
            *pTmp = '/';
            strcpy(pDstName, acOwner);
         } else
         {
            sprintf(acTmp, "%s & %s", acOwner, pTmp+1);
            blankRem(acTmp);
            strcpy(pDstName, acTmp);
         }
      }
   } else
   {
      strcpy(pDstName, pSrcName);
   }

   if (*pVesting > ' ')
      return 99;
   else
      return 0;
}

void Ker_MergeOwner(char *pOutbuf, char *pRollRec)
{
   int   iTmp, iRet;
   char  acOwner1[128], acTmp[128], acSave[128], acVesting[8];
   char  *pTmp;

   KER_ROLL *pRec = (KER_ROLL *)pRollRec;

   // Clear output buffer if needed
   removeNames(pOutbuf, true);

   // Initialize
   memcpy(acTmp, pRec->OwnerName, RSIZ_OWNER);
   blankRem(acTmp, RSIZ_OWNER);
   if ((pTmp = strchr(acTmp, 0x9B)) || (pTmp = strchr(acTmp, 0xA2)) )
      if (*(pTmp-1) == ' ' && *(pTmp+1) == ' ')
         *pTmp = '&';

   // Clean up owner
   iTmp = Ker_CleanName(acTmp, acOwner1, acVesting);
   memcpy(pOutbuf+OFF_NAME1, acTmp, strlen(acTmp));

   // CareOf
   memcpy(acTmp, pRec->CareOf, RSIZ_CAREOF);
   acTmp[RSIZ_CAREOF] = 0;
   if (pTmp = strchr(acTmp, 0xAA))
      *pTmp = ' ';
   if (pTmp = strchr(acTmp, 0xF3))
      *pTmp = '&';
   if ((pTmp = strchr(acTmp, 0x9B)) || (pTmp = strchr(acTmp, 0xA2)) )
   {
      if (*(pTmp-1) == ' ' && *(pTmp+1) == ' ')
         *pTmp = '&';
   }
   iTmp = blankRem(acTmp, RSIZ_CAREOF);
   replUChar((unsigned char *)&acTmp[0], 0xA2, '&', iTmp);
   updateCareOf(pOutbuf, acTmp, iTmp);

   strcpy(acSave, acOwner1);

#ifdef _DEBUG
   //if (!memcmp(pRollRec, "001081009", 9))
   //   iTmp = 1;
#endif
   // Parse owner to get swap name
   if (strchr(acOwner1, ' '))
   {
      OWNER myOwner;
      iRet = splitOwner(acOwner1, &myOwner, 3);
      if (iRet >= 0)
         strcpy(acSave, myOwner.acSwapName);
   }
   vmemcpy(pOutbuf+OFF_NAME_SWAP, acSave, SIZ_NAME_SWAP);
}

void Ker_MergeOwner(char *pOutbuf, char *pOwner, char *pCareOf)
{
   int   iTmp, iRet;
   char  acOwner1[128], acTmp[128], acSave[128];
   char  *pTmp;

   // Clear output buffer if needed
   removeNames(pOutbuf, true);

   // Initialize
   memcpy(acTmp, pOwner, LSIZ_OWNER);
   blankRem(acTmp, LSIZ_OWNER);
   // Replace known bad char
   if (pTmp = strchr(acTmp, 0xA2))
      *pTmp = '&';

   updateVesting(myCounty.acCntyCode, acTmp, pOutbuf+OFF_VEST, pOutbuf+OFF_ETAL_FLG);

   Ker_CleanOwner(acTmp, acOwner1);
   memcpy(pOutbuf+OFF_NAME1, acOwner1, strlen(acOwner1));

   // CareOf
   memcpy(acTmp, pCareOf, LSIZ_CAREOF);
   iTmp = blankRem(acTmp, LSIZ_CAREOF);
   replUChar((unsigned char *)&acTmp[0], 0xA2, '&', iTmp);
   updateCareOf(pOutbuf, acTmp, iTmp);

   strcpy(acSave, acOwner1);

   // Filter out unused words
   if (pTmp=strstr(acOwner1, " ETAL"))
      *pTmp = 0;
   else if (pTmp=strstr(acOwner1, " ET AL"))
      *pTmp = 0;
   else if (pTmp=strstr(acOwner1, " TRS"))
      *pTmp = 0;
   else if (pTmp=strstr(acOwner1, " CO TR"))
      *pTmp = 0;
   else if (pTmp=strstr(acOwner1, " CO-TR"))
      *pTmp = 0;

   if ((pTmp=strstr(acOwner1, " REV ")) || (pTmp=strstr(acOwner1, " REVOC")) )
      *pTmp = 0;
   if ((pTmp=strstr(acOwner1, " FAMILY ")) || (pTmp=strstr(acOwner1, " FMLY ")) )
      *pTmp = 0;
   if ((pTmp=strstr(acOwner1, " LIVING ")) || (pTmp=strstr(acOwner1, " LIV ")) )
      *pTmp = 0;
   if ((pTmp=strstr(acOwner1, " IRR TR")) || (pTmp=strstr(acOwner1, " LIV ")) )
      *pTmp = 0;
   if ((pTmp=strstr(acOwner1, "-LIFE ESTATE")) || (pTmp=strstr(acOwner1, " LIFE ESTATE")) )
      *pTmp = 0;
   else if (pTmp=strstr(acOwner1, " SEP PROP TR"))
      *pTmp = 0;

   if ((pTmp=strstr(acOwner1, " TR-")) || (pTmp=strstr(acOwner1, " TR ")) )
      *pTmp = 0;
   else if (pTmp=strstr(acOwner1, " TRUST"))
      *pTmp = 0;

#ifdef _DEBUG
   //if (!memcmp(pRollRec, "001081009", 9))
   //   iTmp = 1;
#endif
   // Parse owner to get swap name
   if (strchr(acOwner1, ' '))
   {
      OWNER myOwner;

      try
      {
         iRet = splitOwner(acOwner1, &myOwner, 3);
         if (iRet >= 0)
            strcpy(acSave, myOwner.acSwapName);
      } catch (...)
      {
         LogMsg("***** Bad char found in Owner name on %.12s", pOutbuf);
      }
   }
   vmemcpy(pOutbuf+OFF_NAME_SWAP, acSave, SIZ_NAME_SWAP);
}

/******************************** Ker_MergeOwner2 *****************************
 *
 * Input: Owner name
 * 08/06/2020 Replace special char 0xA2 with '&'
 *
 ******************************************************************************/

void Ker_MergeOwner2(char *pOutbuf, char *pOwnerName)
{
   int   iTmp, iRet;
   char  acOwner1[128], acTmp[128], acSave[128], acVesting[8];
   char  *pTmp;

   // Initialize
   strcpy(acTmp, pOwnerName);
   if ((pTmp = strchr(acTmp, 0xF3)) || (pTmp = strchr(acTmp, 0xA2)))
      *pTmp = '&';

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "28505114000", 11))
   //   iTmp = 1;
#endif

   blankRem(acTmp, RSIZ_OWNER);
   iTmp = remChar(acTmp, 96);
   if (acTmp[0] < '0' || acTmp[0] > 'Z')
   {
      iTmp = replChar(acTmp, 0xBC, 'P');
      if (!iTmp)
      {
         if (acTmp[0] == 0x2E && !memcmp(pOutbuf, "28505114000", 11))
            acTmp[0] = 'P';
         else if (acTmp[0] == 0x2E && !memcmp(pOutbuf, "06708315006", 11))
            acTmp[0] = 'S';
         else if (acTmp[0] == 0x5C && !memcmp(pOutbuf, "21901405000", 11))
            acTmp[0] = 'H';
         else if (acTmp[0] == 0x5C && !memcmp(pOutbuf, "26217206002", 11))
            acTmp[0] = 'C';
         else 
         {
            strcpy(acSave, &acTmp[1]);
            strcpy(acTmp, acSave);
         }
      }
      LogMsg("*** Bad char found in: %s [%.11s]", pOwnerName, pOutbuf);
   }

   // Clean up owner name
   iTmp = Ker_CleanName(acTmp, acOwner1, acVesting);
   if (acVesting[0] > ' ')
      memcpy(pOutbuf+OFF_VEST, acVesting, strlen(acVesting));

   memcpy(pOutbuf+OFF_NAME1, acTmp, strlen(acTmp));
   strcpy(acSave, acOwner1);

   // Parse owner to get swap name
   if (strchr(acOwner1, ' '))
   {
      OWNER myOwner;
      iRet = splitOwner(acOwner1, &myOwner, 3);
      if (iRet >= 0)
         strcpy(acSave, myOwner.acSwapName);
   }
   vmemcpy(pOutbuf+OFF_NAME_SWAP, acSave, SIZ_NAME_SWAP);
}

/******************************** Ker_MergeSitus *****************************
 *
 * Notes:
 *    - Drop street name '0' or 'O' if strNum is 0.
 *    - SANDIA CREEK TER WEST
 *    - Since there is no situs city, copy it from mailing if same strname
 *      and strnum.  Otherwise, match TRA.
 *
 * Return 0 if successful, >0 if error
 *
 *****************************************************************************/

int Ker_MergeSitus(char *pOutbuf, char *pRollRec)
{
   static   char acRec[1024], *pRec=NULL;
   char     acTmp[256], acTmp1[64], acAddr1[64], acSfx[8];
   long     lTmp;
   int      iRet, iTmp, iIdx, iLoop;

   KER_SITUS *pSitus = (KER_SITUS *)&acRec[0];

   // Get first situs rec for first call
   //if (!pRec && !lSitusMatch)
   if (!pRec)
   {
      do
      {
         pRec = fgets(acRec, 1024, fdSitus);
      } while (pRec && !isdigit(*(pRec+9)));
   }
   do
   {
      if (!pRec)
      {
         fclose(fdSitus);
         fdSitus = NULL;
         return 1;      // EOF
      }

      // Compare Apn
      iLoop = memcmp(pOutbuf+OFF_OTHER_APN, pSitus->Te_No, SIT_TE_NO);
      if (iLoop > 0)
      {
         if (bDebug)
            LogMsg0("Skip situs Addr rec  %.*s", SIT_TE_NO, pSitus->Te_No);
         pRec = fgets(acRec, 1024, fdSitus);
         lSitusSkip++;
      }
   } while (iLoop > 0);

   // If not match, return
   if (iLoop)
      return 1;

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "21029002", 8))
   //   lTmp = 0;
#endif

   // Initialize
   acAddr1[0] = 0;
   lTmp = atoin(pSitus->StrNum, SIT_STR_NO);
   if (lTmp > 0)
   {
      sprintf(acAddr1, "%d       ", lTmp);
      memcpy(pOutbuf+OFF_S_STRNUM, acAddr1, SIZ_S_STRNUM);
      memcpy(pOutbuf+OFF_S_HSENO, acAddr1, SIZ_S_STRNUM);
      if (pSitus->StrFra[0] == '5')
      {
         memcpy(pOutbuf+OFF_S_STR_SUB, "1/2", 3);
         strcat(acAddr1, "1/2 ");
      }
   }

   memcpy(pOutbuf+OFF_S_DIR, pSitus->StrDir, SIT_STR_DIR);
   memcpy(pOutbuf+OFF_S_STREET, pSitus->StrName, SIT_STR_NAME);
   acSfx[0] = 0;
   if (pSitus->StrSfx[0] > ' ')
   {
      // Translate to code
      memcpy(acTmp, pSitus->StrSfx, SIT_STR_SFX);
      acTmp[SIT_STR_SFX] = 0;
      iTmp = GetSfxCodeX(acTmp, acSfx);
      if (iTmp)
      {
         iRet = sprintf(acTmp1, "%d", iTmp);
         memcpy(pOutbuf+OFF_S_SUFF, acTmp1, iRet);
      } else
         LogMsg0("*** Unknown suffix %s", acTmp);
   }

   sprintf(acTmp, "%.2s %.*s %s ", pSitus->StrDir, SIT_STR_NAME, pSitus->StrName, acSfx);
   strcat(acAddr1, acTmp);

   if (pSitus->UnitNo[0] > ' ')
   {
      for (iIdx=0,iTmp=0; iTmp < SIT_AD_UNIT_ID; iTmp++)
      {
         if (pSitus->UnitNo[iTmp] != ' ')
            acTmp[iIdx++] = pSitus->UnitNo[iTmp];
      }
      acTmp[iIdx] = 0;
      if (acTmp[0] != '#')
      {
         *(pOutbuf+OFF_S_UNITNO) = '#';
         memcpy(pOutbuf+OFF_S_UNITNO+1, acTmp, iIdx);
         strcat(acAddr1, "#");
      } else
         memcpy(pOutbuf+OFF_S_UNITNO, acTmp, iIdx);
      strcat(acAddr1, acTmp);
   }

   blankRem(acAddr1);
   memcpy(pOutbuf+OFF_S_ADDR_D, acAddr1, strlen(acAddr1));

   // City
   if (pSitus->City[0] > ' ')
   {
      memcpy(acTmp, pSitus->City, SIT_CITY);
      myTrim(acTmp, SIT_CITY);
      City2Code(acTmp, acTmp1, pOutbuf);
      memcpy(pOutbuf+OFF_S_CITY, acTmp1, SIZ_M_CITY);
      strcat(acTmp, " CA");
      memcpy(pOutbuf+OFF_S_CTY_ST_D, acTmp, strlen(acTmp));
   }

   // State
   memcpy(pOutbuf+OFF_S_ST, "CA", 2);

   // Zipcode
   lTmp = atoin(pSitus->ZipCode, SIT_ZIP_CODE);
   if (lTmp > 90000)
      memcpy(pOutbuf+OFF_S_ZIP, pSitus->ZipCode, SIT_ZIP_CODE);
   else if (!memcmp(pSitus->City, pOutbuf+OFF_M_CITY, 10))
   {
      lTmp = atoin(pOutbuf+OFF_M_ZIP, SIZ_M_ZIP);
      if (lTmp > 90000)
         memcpy(pOutbuf+OFF_S_ZIP, pOutbuf+OFF_M_ZIP, SIZ_M_ZIP);
   }

   lSitusMatch++;

   // Get next situs rec
   pRec = fgets(acRec, 1024, fdSitus);
   return 0;
}

/******************************* Ker_MergeSitus2 *****************************
 *
 * Notes:
 *    - Drop street name '0' or 'O' if strNum is 0.
 *    - SANDIA CREEK TER WEST
 *    - Since there is no situs city, copy it from mailing if same strname
 *      and strnum.  Otherwise, match TRA.
 *
 * Return 0 if successful, >0 if error
 *
 *****************************************************************************/

int Ker_MergeSitus2(char *pOutbuf)
{
   static   char acRec[256], *pRec=NULL;
   char     *pTmp, acBuf[256], acTmp[256], acTmp1[64], acAddr1[64], acSfx[8], *apFlds[SITUS_ADDR1+2];
   int      lTmp, iRet, iTmp, iLoop;

   // Get first situs rec for first call
   if (!pRec)
   {
      // Drop header
      pRec = fgets(acRec, 256, fdSitus);
      pRec = fgets(acRec, 256, fdSitus);

      // Get first rec
      pRec = fgets(acRec, 256, fdSitus);
   }

   pTmp = pRec;
   do
   {
      if (!pRec)
      {
         fclose(fdSitus);
         fdSitus = NULL;
         return 1;      // EOF
      }

      // Compare Apn
      iLoop = memcmp(pOutbuf, pRec, iApnLen);
      if (iLoop > 0)
      {
         if (bDebug)
            LogMsg0("Skip situs Addr rec  %.*s", iApnLen, pRec);
         pRec = fgets(acRec, 256, fdSitus);
         lSitusSkip++;
      }
   } while (iLoop > 0);

   // If not match, return
   if (iLoop)
      return 1;

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "00102001015", 11))
   //   lTmp = 0;
#endif
   // Parse input rec
   strcpy(acBuf, acRec);
   iRet = ParseString(acBuf, '|', SITUS_ADDR1+2, apFlds);
   if (iRet < SITUS_ADDR1+1)
   {
      LogMsg("***** Bad Situs record: %s", acRec);
      return -1;
   }

   // Initialize
   acAddr1[0] = 0;
   lTmp = atol(apFlds[SITUS_STRNO]);
   if (lTmp > 0)
   {
      iTmp = sprintf(acAddr1, "%d ", lTmp);
      memcpy(pOutbuf+OFF_S_STRNUM, acAddr1, iTmp);
      memcpy(pOutbuf+OFF_S_HSENO, apFlds[SITUS_STRNO], strlen(apFlds[SITUS_STRNO]));
      if (*apFlds[SITUS_STRFRA] == '5')
      {
         memcpy(pOutbuf+OFF_S_STR_SUB, "1/2", 3);
         strcat(acAddr1, "1/2 ");
      }
   }

   // DIR - N, S, E, W, NO, SO
   if (*apFlds[SITUS_STRDIR] > ' ')
   {
      *(pOutbuf+OFF_S_DIR) = *apFlds[SITUS_STRDIR];
      acTmp[0] = *apFlds[SITUS_STRDIR];
      acTmp[1] = ' ';
      acTmp[2] = 0;
      strcat(acAddr1, acTmp);
   }
   memcpy(pOutbuf+OFF_S_STREET, apFlds[SITUS_STRNAME], strlen(apFlds[SITUS_STRNAME]));
   acSfx[0] = 0;
   if (*apFlds[SITUS_STRSFX] > ' ')
   {
      // Translate to code
      strcpy(acTmp, apFlds[SITUS_STRSFX]);
      iTmp = GetSfxCodeX(acTmp, acSfx);
      if (iTmp)
      {
         iRet = sprintf(acTmp1, "%d", iTmp);
         memcpy(pOutbuf+OFF_S_SUFF, acTmp1, iRet);
      } else
         LogMsg0("*** Unknown suffix %s", acTmp);
   }

   sprintf(acTmp, "%s %s ", apFlds[SITUS_STRNAME], acSfx);
   strcat(acAddr1, acTmp);

   if (*apFlds[SITUS_UNITNO] > '0')
   {
      // Remove garbage
      if (pTmp = strchr(apFlds[SITUS_UNITNO], ')'))
         *pTmp = 0;

      if (*apFlds[SITUS_UNITNO] == '#')
         iTmp = 0;
      else 
      {
         iTmp = 1;
         *(pOutbuf+OFF_S_UNITNO) = '#';
         strcat(acAddr1, "#");
      }
      memcpy(pOutbuf+OFF_S_UNITNO+iTmp, apFlds[SITUS_UNITNO], strlen(apFlds[SITUS_UNITNO]));
      strcat(acAddr1, apFlds[SITUS_UNITNO]);
   }

   iTmp = blankRem(acAddr1);
   vmemcpy(pOutbuf+OFF_S_ADDR_D, acAddr1, SIZ_S_ADDR_D, iTmp);

   // Special cases
   if ((memcmp(pOutbuf, "0670810100", 10) >= 0 && memcmp(pOutbuf, "0670831000", 10) <= 0) ||
       (memcmp(pOutbuf, "0671020800", 10) >= 0 && memcmp(pOutbuf, "0671030700", 10) <= 0) )
   {
      memcpy(pOutbuf+OFF_S_ZIP, "93555", 5);
   }

   // City
   if (*apFlds[SITUS_CITY] > ' ')
   {
      //City2Code(apFlds[SITUS_CITY], acTmp1, pOutbuf);
      iRet = City2CodeEx(apFlds[SITUS_CITY], acTmp1, acTmp, pOutbuf);
      if (iRet > 0)
      {
         vmemcpy(pOutbuf+OFF_S_CITY, acTmp1, SIZ_S_CITY);
         iTmp = sprintf(acTmp, "%s, CA %.5s", acTmp, pOutbuf+OFF_S_ZIP);
         vmemcpy(pOutbuf+OFF_S_CTY_ST_D, acTmp, SIZ_S_CTY_ST_D, iTmp);
      }
   }

   // State
   memcpy(pOutbuf+OFF_S_ST, "CA", 2);

   // Zipcode
   /*
   lTmp = atoin(pSitus->ZipCode, SIT_ZIP_CODE);
   if (lTmp > 90000)
      memcpy(pOutbuf+OFF_S_ZIP, pSitus->ZipCode, SIT_ZIP_CODE);
   else if (!memcmp(pSitus->City, pOutbuf+OFF_M_CITY, 10))
   {
      lTmp = atoin(pOutbuf+OFF_M_ZIP, SIZ_M_ZIP);
      if (lTmp > 90000)
         memcpy(pOutbuf+OFF_S_ZIP, pOutbuf+OFF_M_ZIP, SIZ_M_ZIP);
   }
   */
   lSitusMatch++;

   // Get next situs rec
   pRec = fgets(acRec, 256, fdSitus);
   return 0;
}

/********************************* Ker_MergeMAdr *****************************
 *
 * Mail address has similar problem as situs, but worse due to inconsistency.
 * Now we have to check for direction both pre and post strname. This version 
 * used when loading LDR.
 *
 * 1) Line 3 is most likely country code for foreign addresses.  Sometimes it
 *    is zip code left over from Line 2.  In this case, PMZip is likely blank.
 * 2) Line 1 often contains regular addr or "P O BOX" or "PO BOX".
 * 3) If C/O or ATTN found, populate only if current CareOf field is blank.
 *
 *****************************************************************************/

void Ker_MergeMAdr(char *pOutbuf, char *pLine1, char *pLine2, char *pLine3, char *pMZip)
{
   ADR_REC  sMailAdr;
   char  acAddr1[256], acAddr2[256], acTmp[256], *pTmp, *pTerm;
   int   iTmp, iRet;

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "25201314002", 11))
   //   iTmp = 0;
#endif

   // Initialize
   removeMailing(pOutbuf, false);

   // Init output
   memset(&sMailAdr, 0, sizeof(ADR_REC));

   if (*pLine1 <= ' ' || *pLine1 == '0' || *pLine2 == '0')
      return;
   if (*pLine1 == '#')
      *pLine1 = ' ';

   acAddr1[0] = 0;

   pTerm = NULL;
   if (!memcmp(pLine1, "PMB ", 4))
   {  // Append line1 to line 2 if line 3 has data
      if (*pLine3 > ' ')
      {
         sprintf(acAddr1, "%.*s %.*s", LSIZ_MADDR2, pLine2, LSIZ_MADDR1, pLine1);
         iTmp = blankRem(acAddr1);
         pTerm = strstr(acAddr1, " PMB ");
         memcpy(acAddr2, pLine3, LSIZ_MADDR3);
         iTmp = blankRem(acAddr2, LSIZ_MADDR3);
      } else
      {
         memcpy(acTmp, pLine1, LSIZ_MADDR1);
         blankRem(acTmp, LSIZ_MADDR1);
         if (pTmp = strchr(&acTmp[5], ' '))
            pTmp++;
         else 
            pTmp = acTmp;
         strcpy(acAddr1, pTmp);
         memcpy(acAddr2, pLine2, LSIZ_MADDR2);
         iTmp = blankRem(acAddr2, LSIZ_MADDR2);
      }
   } else if (*pLine2 == '#' || !memcmp(pLine2, "BOX ", 4) || !memcmp(pLine2, "PMB ", 4) 
      || !memcmp(pLine2, "STE ", 4) || !memcmp(pLine2, "P O ", 4))
   {  // Append line2 to line 1
      sprintf(acAddr1, "%.*s %.*s", LSIZ_MADDR1, pLine1, LSIZ_MADDR2, pLine2);
      iTmp = blankRem(acAddr1);
      pTerm = strstr(acAddr1, " PMB ");
      if (!pTerm)
      {
         pTerm = strstr(acAddr1, " BOX ");
         if (!pTerm)
            pTerm = strstr(acAddr1, " P O ");
      }
      memcpy(acAddr2, pLine3, LSIZ_MADDR3);
      iTmp = blankRem(acAddr2, LSIZ_MADDR3);
   } else
   {
      memcpy(acAddr1, pLine1, LSIZ_MADDR1);
      iTmp = blankRem(acAddr1, LSIZ_MADDR1);

      if (!isdigit(*pLine1) && *pLine3 > ' ')
      {
         if (!memcmp(acAddr1, "C/O", 3) || !memcmp(acAddr1, "ATTN", 4) )
         {
            if (*(pOutbuf+OFF_CARE_OF) == ' ')
               updateCareOf(pOutbuf, acAddr1, iTmp);
            memcpy(acAddr1, pLine2, LSIZ_MADDR2);
            iTmp = blankRem(acAddr1, LSIZ_MADDR2);
            memcpy(acAddr2, pLine3, LSIZ_MADDR3);
            iRet = LSIZ_MADDR3;
         } else if (!memcmp(pLine3, "ATTN", 4) )
         {
            if (*(pOutbuf+OFF_CARE_OF) == ' ')
            {
               memcpy(acAddr2, pLine3, LSIZ_MADDR3);
               iTmp = blankRem(acAddr2, LSIZ_MADDR3);
               updateCareOf(pOutbuf, acAddr2, iTmp);
            }
            memcpy(acAddr2, pLine2, LSIZ_MADDR2);
            iRet = LSIZ_MADDR2;
         } else if (memcmp(pLine1, "P O ", 4) && memcmp(pLine1, "PO B", 4) && memcmp(pLine1, "P.O", 3) )
         {
            // Found SWEDEN, GERMANY in line 3, CANADA in line 2, should combine 2 & 3
            if (!memcmp(pLine3, "SWEDEN", 6) || !memcmp(pLine3, "GERMANY", 7) 
               || !memcmp(pLine3, "AUSTRIA", 7) || !memcmp(pLine3, "MEXICO", 6))
            {
               sprintf(acAddr2, "%.*s %.*s", LSIZ_MADDR2, pLine2, LSIZ_MADDR3, pLine3);
               iRet = blankRem(acAddr2, 0);
            } else
            {
               memcpy(acAddr1, pLine2, LSIZ_MADDR2);
               iTmp = blankRem(acAddr1, LSIZ_MADDR2);
               memcpy(acAddr2, pLine3, LSIZ_MADDR3);
               iRet = LSIZ_MADDR3;
            }
         } else
         { 
            // If foreign addr, combine 2 & 3.  Otherwise, drop line 2
            // Found CANADA, AUSTRALIA
            memcpy(acAddr2, pLine3, LSIZ_MADDR3);
            iRet = LSIZ_MADDR3;
         }
      } else if (*pMZip == ' ' && isdigit(*pLine3) && isdigit(*(pLine3+4)) && *(pLine3+5) == ' ')
      {
         // Combine 2 & 3 
         iRet = sprintf(acAddr2, "%.*s %.*s", LSIZ_MADDR2, pLine2, LSIZ_MADDR3, pLine3);
         replCharEx(acAddr2, ",_", ' ', iRet);

         //memcpy(sMailAdr.Zip, pLine3, 5);
         //memcpy(acAddr2, pLine2, LSIZ_MADDR2);
         //iRet = LSIZ_MADDR2;
      } else if (*pLine3 > ' ')
      {
         if (!memcmp(pLine2, "MAIL STOP", 9))
         {  // Ignore mail stop
            memcpy(acAddr2, pLine3, LSIZ_MADDR3);
            iRet = LSIZ_MADDR3;
         } else if (isdigit(*pLine2) || isdigit(*(pLine2+1)) || 
            !memcmp(pLine2, "APT ", 4) || !memcmp(pLine2, "ROOM ", 5) )
         {  // Append to line 1
            iRet = sprintf(acAddr1, "%.*s %.*s", LSIZ_MADDR1, pLine1, LSIZ_MADDR2, pLine2);
            iTmp = blankRem(acAddr1, iRet);
            memcpy(acAddr2, pLine3, LSIZ_MADDR3);
            iRet = LSIZ_MADDR3;
         } else
         {
            memcpy(acAddr2, pLine2, LSIZ_MADDR2);
            acAddr2[LSIZ_MADDR2] = 0;
            // Drop line 2 if containing BOX, BLD
            if (!strstr(acAddr2, " BOX") && !strstr(acAddr2, "BLD") &&
               memcmp(acAddr2, "RVW", 3) && memcmp(acAddr2, "SVW", 3) && 
               memcmp(acAddr2, "FORECLOSURE", 5) && memcmp(acAddr2, "DA AM", 5) &&
               memcmp(acAddr2, "CODE ", 5)  && memcmp(acAddr2, "IDC ", 4) )
               iRet = sprintf(acAddr2, "%.*s %.*s", LSIZ_MADDR2, pLine2, LSIZ_MADDR3, pLine3);
            else 
            {
               memcpy(acAddr2, pLine3, LSIZ_MADDR3);
               iRet = LSIZ_MADDR3;
            }
         }
         replChar(acAddr2, ',', ' ', iRet);
      } else
      {
         memcpy(acAddr2, pLine2, LSIZ_MADDR2);
         iRet = LSIZ_MADDR2;
      }
      iTmp = blankRem(acAddr2, iRet);
   }

   // Parse line 1
   replChar(acAddr1, ',', ' ', SIZ_M_ADDR_D);
   if (iTmp = replUnPrtChar(acAddr1))
      LogMsg("*** Bad character found in M_Addr1 at %.12s", pOutbuf);

   iTmp = blankRem(acAddr1);
   if (iTmp > SIZ_M_ADDR_D)
      iTmp = SIZ_M_ADDR_D;
   memcpy(pOutbuf+OFF_M_ADDR_D, acAddr1, iTmp);

   // Remove BOX or PMP to parse addr.
   if (pTerm)
      *pTerm = 0;
   parseMAdr1_2(&sMailAdr, acAddr1);
   if (sMailAdr.lStrNum > 0)
   {
      sprintf(acTmp, "%d       ", sMailAdr.lStrNum);
      memcpy(pOutbuf+OFF_M_STRNUM, acTmp, SIZ_M_STRNUM);
      if (sMailAdr.strSub[0] > '0')
      {
         sprintf(acTmp, "%s  ", sMailAdr.strSub);
         memcpy(pOutbuf+OFF_M_STR_SUB, acTmp, SIZ_M_STR_SUB);
      }
      if (sMailAdr.strDir[0] > '0')
         memcpy(pOutbuf+OFF_M_DIR, sMailAdr.strDir, strlen(sMailAdr.strDir));
      if (sMailAdr.strSfx[0] > '0')
         memcpy(pOutbuf+OFF_M_SUFF, sMailAdr.strSfx, strlen(sMailAdr.strSfx));
      if (sMailAdr.Unit[0] > ' ' && strlen(sMailAdr.Unit) <= SIZ_M_UNITNO)
         memcpy(pOutbuf+OFF_M_UNITNO, sMailAdr.Unit, strlen(sMailAdr.Unit));
      if (pTmp = strstr(sMailAdr.strName, " PMB"))
         *pTmp = 0;
      memcpy(pOutbuf+OFF_M_STREET, sMailAdr.strName, strlen(sMailAdr.strName));
   } else
   {
      iTmp = strlen(acAddr1);
      if (iTmp > SIZ_M_STREET)
         iTmp = SIZ_M_STREET;
      memcpy(pOutbuf+OFF_M_STREET, acAddr1, iTmp);
   }

   // Parse line 2
   if (pTmp = strrchr(acAddr2, ' '))
   {
      if (isdigit(*(pTmp-1)) && isdigit(*(pTmp+1)))
         *pTmp = '-';
   }

   iTmp = strlen(acAddr2);
   if (iTmp > SIZ_M_CTY_ST_D)
      iTmp = SIZ_M_CTY_ST_D;
   memcpy(pOutbuf+OFF_M_CTY_ST_D, acAddr2, iTmp);

   parseAdr2_1(&sMailAdr, acAddr2);
   if (sMailAdr.City[0] > ' ')
   {
      iTmp = strlen(sMailAdr.City);
      if (iTmp > SIZ_M_CITY) iTmp = SIZ_M_CITY;
      memcpy(pOutbuf+OFF_M_CITY, sMailAdr.City, iTmp);

      if (sMailAdr.State[0] > ' ')
         memcpy(pOutbuf+OFF_M_ST, sMailAdr.State, SIZ_M_ST);

      // Zipcode
      if (*pMZip > ' ')
         memcpy(pOutbuf+OFF_M_ZIP, pMZip, SIZ_M_ZIP);
      else
      {
         iTmp = strlen(sMailAdr.Zip);
         if (iTmp > SIZ_M_ZIP)
            iTmp = SIZ_M_ZIP;
         memcpy(pOutbuf+OFF_M_ZIP, sMailAdr.Zip, iTmp);
      }
   }
}

/********************************* Ker_MergeMAdr ******************************
 *
 * Use this version when loading monthly update
 *
 ******************************************************************************/

void Ker_MergeMAdr(char *pOutbuf, char *pRollRec)
{
   KER_ROLL *pRec;
   char     acTmp[256], acAddr1[128], acAddr2[128];
   int      iTmp, iIdx;
   long     lTmp;

   pRec = (KER_ROLL *)pRollRec;

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "011291065", 9))
   //   iTmp = 0;
#endif

   // Initialize
   acAddr1[0] = 0;
   removeMailing(pOutbuf, false);

   lTmp = atoin(pRec->M_StrNum, RSIZ_STR_NO);
   if (lTmp > 0)
   {
      iTmp = sprintf(acAddr1, "%d ", lTmp);
      memcpy(pOutbuf+OFF_M_STRNUM, acAddr1, iTmp);
      if (pRec->M_StrFra[0] == '5')
      {
         memcpy(pOutbuf+OFF_M_STR_SUB, "1/2", 3);
         strcat(acAddr1, "1/2 ");
      }
   }

   // Check for bad chars
   if (iTmp = replUnPrtChar(pRec->M_StrName, ' ', RSIZ_STR_NAME))
      LogMsg("*** Bad character found in M_StrName at %.12s", pOutbuf);

   memcpy(pOutbuf+OFF_M_DIR, pRec->M_StrDir, RSIZ_STR_DIR);
   memcpy(pOutbuf+OFF_M_STREET, pRec->M_StrName, RSIZ_STR_NAME);
   memcpy(pOutbuf+OFF_M_SUFF, pRec->M_StrSfx, RSIZ_STR_TYPE_CODE);
   sprintf(acTmp, "%.2s %.*s %.*s ", pRec->M_StrDir, RSIZ_STR_NAME, pRec->M_StrName, RSIZ_STR_TYPE_CODE, pRec->M_StrSfx);
   strcat(acAddr1, acTmp);

   if (pRec->M_UnitNo[0] > ' ')
   {
      for (iIdx=0,iTmp=0; iTmp < RSIZ_AD_UNIT_ID; iTmp++)
      {
         if (pRec->M_UnitNo[iTmp] != ' ')
            acTmp[iIdx++] = pRec->M_UnitNo[iTmp];
      }
      acTmp[iIdx] = 0;
      memcpy(pOutbuf+OFF_M_UNITNO, acTmp, iIdx);
      strcat(acAddr1, acTmp);
   }

   iTmp = blankRem(acAddr1);
   memcpy(pOutbuf+OFF_M_ADDR_D, acAddr1, iTmp);

   // City
   memcpy(acTmp, pRec->M_City, RSIZ_CITY);
   iTmp = blankRem(acTmp, RSIZ_CITY);
   memcpy(pOutbuf+OFF_M_CITY, acTmp, iTmp);

   // State
   if (pRec->M_St[0] > ' ')
   {
      memcpy(pOutbuf+OFF_M_ST, pRec->M_St, RSIZ_STATE_CODE);
      iTmp = sprintf(acAddr2, "%s %.2s", acTmp, pRec->M_St);
   } else
      strcpy(acAddr2, acTmp);

   // Zipcode
   lTmp = atoin(pRec->M_Zip, RSIZ_ZIP_CODE);
   if (lTmp > 400)
   {
      memcpy(pOutbuf+OFF_M_ZIP, pRec->M_Zip, RSIZ_ZIP_CODE);
      if (lTmp < 99999)
         sprintf(acTmp, "%.5d", lTmp);
      else if (isdigit(pRec->M_Zip[8]))
         sprintf(acTmp, "%.5s-%.4s", pRec->M_Zip, &pRec->M_Zip[5]);
      else
      {
         memcpy(acTmp, pRec->M_Zip, RSIZ_ZIP_CODE);
         acTmp[RSIZ_ZIP_CODE] = 0;
      }
   } else if (pRec->M_Zip[0] > ' ')
   {
      memcpy(acTmp, pRec->M_Zip, RSIZ_ZIP_CODE);
      acTmp[RSIZ_ZIP_CODE] = 0;
   } else
      acTmp[0] = 0;

   iTmp = sprintf(acAddr1, "%s %s", acAddr2, acTmp);
   memcpy(pOutbuf+OFF_M_CTY_ST_D, acAddr1, iTmp);
}

/******************************* Ker_MergeMAdr2 ******************************
 *
 * 
 *
 *****************************************************************************/

int Ker_MergeMAdr2(char *pOutbuf)
{
   static   char acRec[512], *pRec=NULL;
   char     *pTmp, acBuf[512], acTmp[64], acAddr1[128], *apFlds[MAIL_LINE4+2];
   int      iLoop, iRet, iTmp;

   // Get first Char rec for first call
   if (!pRec)
   {
      // Drop header
      pRec = fgets(acRec, 512, fdMAdr);
      pRec = fgets(acRec, 512, fdMAdr);

      // Get first rec
      pRec = fgets(acRec, 512, fdMAdr);
   }

   do
   {
      if (!pRec)
      {
         fclose(fdMAdr);
         fdMAdr = NULL;
         return 1;      // EOF
      }

      // Compare Apn
      iLoop = memcmp(pOutbuf, pRec, iApnLen);
      if (iLoop > 0)
      {
         if (bDebug)
            LogMsg0("Skip Mail Addr rec  %.*s", iApnLen, pRec);
         pRec = fgets(acRec, 512, fdMAdr);
         lMAdrSkip++;
      }
   } while (iLoop > 0);

   // If not match, return
   if (iLoop)
      return 1;

   if (strstr(pRec, "UNKNOWN"))
      return 1;

   // Parse address
#ifdef _DEBUG
   //if (!memcmp(pRec, "12218212001", 11))
   //   iRet = 0;
#endif

   // Parse input rec
   strcpy(acBuf, acRec);
   iRet = ParseStringIQ(acBuf, '|', MAIL_LINE4+2, apFlds);
   if (iRet < MAIL_LINE4+1)
   {
      LogMsg("***** Bad mailing record: %s", acRec);
      return -1;
   }

   // CareOf
   if (*apFlds[MAIL_CAREOF] > ' ')
   {
      if (pTmp = strchr(apFlds[MAIL_CAREOF], 0xF3))
         *pTmp = '&';
      if (pTmp = strchr(apFlds[MAIL_CAREOF], 0xBC))
         *pTmp = 'P';

      iRet = remChar(apFlds[MAIL_CAREOF], '"');
      replUChar((unsigned char *)apFlds[MAIL_CAREOF], 0xA2, '&', iRet);
      updateCareOf(pOutbuf, apFlds[MAIL_CAREOF], iRet);
   }

   // Initialize
   acAddr1[0] = 0;
   iRet = atol(apFlds[MAIL_STRNO]);
   if (iRet > 0)
   {
      iTmp = sprintf(acAddr1, "%d ", iRet);
      memcpy(pOutbuf+OFF_M_STRNUM, acAddr1, iTmp);
      if (*apFlds[MAIL_STRFRA] == '5')
      {
         memcpy(pOutbuf+OFF_M_STR_SUB, "1/2", 3);
         strcat(acAddr1, "1/2 ");
      }
   }

   // DIR 
   if (*apFlds[MAIL_STRDIR] > ' ')
      memcpy(pOutbuf+OFF_M_DIR, apFlds[MAIL_STRDIR], strlen(apFlds[MAIL_STRDIR]));

   // StrName - Check for bad chars
   if (pTmp = strchr(apFlds[MAIL_STRNAME], 188))
   {
      if ((unsigned char)*(pTmp+1) < 91)
      {
         remUnPrtChar(apFlds[MAIL_STRNAME]);
         remUnPrtChar(apFlds[MAIL_ADDR1]);
      } else
      {
         // Specific case for TRAPPER ST
         *(pTmp) = 0;
         pTmp += 2;
         sprintf(acTmp, "%sPP%s", apFlds[MAIL_STRNAME], pTmp);
         strcpy(apFlds[MAIL_STRNAME], acTmp);

         pTmp = strchr(apFlds[MAIL_ADDR1], 188);
         *(pTmp) = 0;
         pTmp += 2;
         sprintf(acTmp, "%sPP%s", apFlds[MAIL_ADDR1], pTmp);
         strcpy(apFlds[MAIL_ADDR1], acTmp);
      }

      LogMsg("*** Bad character found in M_StrName at %.12s", pOutbuf);
   } 
   memcpy(pOutbuf+OFF_M_STREET, apFlds[MAIL_STRNAME], strlen(apFlds[MAIL_STRNAME]));
   memcpy(pOutbuf+OFF_M_SUFF, apFlds[MAIL_STRSFX], strlen(apFlds[MAIL_STRSFX]));

   if (*apFlds[MAIL_UNITNO] > '0')
      memcpy(pOutbuf+OFF_M_UNITNO, apFlds[MAIL_UNITNO], strlen(apFlds[MAIL_UNITNO]));

   // City
   memcpy(pOutbuf+OFF_M_CITY, apFlds[MAIL_CITY], strlen(apFlds[MAIL_CITY]));

   // State
   if (*apFlds[MAIL_STATE] > ' ')
      memcpy(pOutbuf+OFF_M_ST, apFlds[MAIL_STATE], 2);

   // Zipcode
   iTmp = atol(apFlds[MAIL_ZIPCODE]);
   if (iTmp > 400)
      memcpy(pOutbuf+OFF_M_ZIP, apFlds[MAIL_ZIPCODE], strlen(apFlds[MAIL_ZIPCODE]));

   memcpy(pOutbuf+OFF_M_ADDR_D, apFlds[MAIL_ADDR1], strlen(apFlds[MAIL_ADDR1]));
   memcpy(pOutbuf+OFF_M_CTY_ST_D, apFlds[MAIL_ADDR2], strlen(apFlds[MAIL_ADDR2]));

   lMAdrMatch++;

   // Get next Mailing rec
   pRec = fgets(acRec, 512, fdMAdr);

   return 0;
}

/***************************** Ker_MergeMAdrCsv ******************************
 *
 * Merge mail address from LDR file
 *
 *****************************************************************************/

void Ker_MergeMAdrCsv(char *pOutbuf, char *pLine1, char *pLine2, char *pLine3, char *pMZip, char *pCareOf, char *pDba)
{
   ADR_REC  sMailAdr;
   char  acAddr1[256], acAddr2[256], acTmp[256], *pTmp, *pTerm;
   int   iTmp, iRet;

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "25201314002", 11))
   //   iTmp = 0;
#endif

   // Initialize
   removeMailing(pOutbuf, true, true);

   // DBA
   if (*pDba > ' ')
      vmemcpy(pOutbuf+OFF_DBA, pDba, SIZ_DBA);

   // CareOf
   if (*pCareOf > ' ')
   {
      if (pTmp = strchr(pCareOf, 0xA2))
         *pTmp = '&';
      if (pTmp = strchr(pCareOf, 0xAC))
      {
         *pTmp = 'P';
         if (pTmp = strchr(pCareOf, 0x3A))
         {
            *(pTmp-1) = 'U';
            *pTmp = 'L';
         }
      }
      remJSChar(pCareOf);
      vmemcpy(pOutbuf+OFF_CARE_OF, pCareOf, SIZ_CARE_OF);
   }
   // Init output
   memset(&sMailAdr, 0, sizeof(ADR_REC));

   if (*pLine1 <= ' ' || *pLine1 == '0' || *pLine2 == '0')
      return;
   if (*pLine1 == '#')
      *pLine1 = ' ';

   acAddr1[0] = 0;

   pTerm = NULL;
   if (!memcmp(pLine1, "PMB ", 4))
   {  // Append line1 to line 2 if line 3 has data
      if (*pLine3 > ' ')
      {
         sprintf(acAddr1, "%s %s", pLine1, pLine2);
         iTmp = blankRem(acAddr1);
         strcpy(acAddr2, pLine3);
      } else
      {
         strcpy(acAddr1, pLine1);
         strcpy(acAddr2, pLine2);
      }
   } else if (*pLine2 == '#' || !memcmp(pLine2, "BOX ", 4) || !memcmp(pLine2, "PMB ", 4) 
      || !memcmp(pLine2, "STE ", 4) || !memcmp(pLine2, "P O ", 4))
   {  // Append line2 to line 1
      sprintf(acAddr1, "%s %s", pLine1, pLine2);
      pTerm = strstr(acAddr1, " PMB ");
      if (!pTerm)
      {
         pTerm = strstr(acAddr1, " BOX ");
         if (!pTerm)
            pTerm = strstr(acAddr1, " P O ");
      }
      strcpy(acAddr2, pLine3);
   } else
   {
      strcpy(acAddr1, pLine1);
      if (!isdigit(*pLine1) && *pLine3 > ' ')
      {
         if (memcmp(pLine1, "P O ", 4) && memcmp(pLine1, "PO B", 4) && memcmp(pLine1, "P.O", 3) )
         {
            // Found SWEDEN, GERMANY in line 3, CANADA in line 2, should combine 2 & 3
            if (!memcmp(pLine3, "SWEDEN", 6) || !memcmp(pLine3, "GERMANY", 7) 
               || !memcmp(pLine3, "AUSTRIA", 7) || !memcmp(pLine3, "MEXICO", 6))
            {
               sprintf(acAddr2, "%s %s", pLine2, pLine3);
               iRet = blankRem(acAddr2, 0);
            } else
            {
               strcpy(acAddr1, pLine2);
               strcpy(acAddr2, pLine3);
               iRet = LSIZ_MADDR3;
            }
         } else
         { 
            // If foreign addr, combine 2 & 3.  Otherwise, drop line 2
            // Found CANADA, AUSTRALIA
            strcpy(acAddr2, pLine3);
            iRet = LSIZ_MADDR3;
         }
      } else if (*pMZip == ' ' && isdigit(*pLine3) && isdigit(*(pLine3+4)) && *(pLine3+5) == ' ')
      {
         // Combine 2 & 3 
         iRet = sprintf(acAddr2, "%s %s", pLine2, pLine3);
         replCharEx(acAddr2, ",_", ' ', iRet);
      } else if (*pLine3 > ' ')
      {
         if (!memcmp(pLine2, "MAIL STOP", 9))
         {  // Ignore mail stop
            strcpy(acAddr2, pLine3);
            iRet = LSIZ_MADDR3;
         } else if (isdigit(*pLine2) || isdigit(*(pLine2+1)) || 
            !memcmp(pLine2, "APT ", 4) || !memcmp(pLine2, "ROOM ", 5) )
         {  // Append to line 1
            iRet = sprintf(acAddr1, "%s %s", pLine1, pLine2);
            iTmp = blankRem(acAddr1, iRet);
            strcpy(acAddr2, pLine3);
            iRet = LSIZ_MADDR3;
         } else
         {
            strcpy(acAddr2, pLine2);
            acAddr2[LSIZ_MADDR2] = 0;
            // Drop line 2 if containing BOX, BLD
            if (!strstr(acAddr2, " BOX") && !strstr(acAddr2, "BLD") &&
               memcmp(acAddr2, "RVW", 3) && memcmp(acAddr2, "SVW", 3) && 
               memcmp(acAddr2, "FORECLOSURE", 5) && memcmp(acAddr2, "DA AM", 5) &&
               memcmp(acAddr2, "CODE ", 5)  && memcmp(acAddr2, "IDC ", 4) )
               iRet = sprintf(acAddr2, "%s %s", pLine2, pLine3);
            else 
            {
               strcpy(acAddr2, pLine3);
               iRet = LSIZ_MADDR3;
            }
         }
         replChar(acAddr2, ',', ' ', iRet);
      } else
      {
         strcpy(acAddr2, pLine2);
         iRet = LSIZ_MADDR2;
      }
      iTmp = blankRem(acAddr2, iRet);
   }

   // Parse line 1
   replChar(acAddr1, ',', ' ', SIZ_M_ADDR_D);
   if (iTmp = replUnPrtChar(acAddr1))
      LogMsg("*** Bad character found in M_Addr1 at %.12s", pOutbuf);

   iTmp = blankRem(acAddr1);
   vmemcpy(pOutbuf+OFF_M_ADDR_D, acAddr1, SIZ_M_ADDR_D, iTmp);

   // Remove BOX or PMP to parse addr.
   if (pTerm)
      *pTerm = 0;
   parseMAdr1_2(&sMailAdr, acAddr1);
   if (sMailAdr.lStrNum > 0)
   {
      sprintf(acTmp, "%d       ", sMailAdr.lStrNum);
      memcpy(pOutbuf+OFF_M_STRNUM, acTmp, SIZ_M_STRNUM);
      if (sMailAdr.strSub[0] > '0')
      {
         sprintf(acTmp, "%s  ", sMailAdr.strSub);
         memcpy(pOutbuf+OFF_M_STR_SUB, acTmp, SIZ_M_STR_SUB);
      }
      if (sMailAdr.strDir[0] > '0')
         memcpy(pOutbuf+OFF_M_DIR, sMailAdr.strDir, strlen(sMailAdr.strDir));
      if (sMailAdr.strSfx[0] > '0')
         memcpy(pOutbuf+OFF_M_SUFF, sMailAdr.strSfx, strlen(sMailAdr.strSfx));
      if (sMailAdr.Unit[0] > ' ' && strlen(sMailAdr.Unit) <= SIZ_M_UNITNO)
         memcpy(pOutbuf+OFF_M_UNITNO, sMailAdr.Unit, strlen(sMailAdr.Unit));
      if (pTmp = strstr(sMailAdr.strName, " PMB"))
         *pTmp = 0;
      vmemcpy(pOutbuf+OFF_M_STREET, sMailAdr.strName, SIZ_M_STREET);
   } else
   {
      vmemcpy(pOutbuf+OFF_M_STREET, acAddr1, SIZ_M_STREET);
   }

   // Parse line 2
   if (pTmp = strrchr(acAddr2, ' '))
   {
      if (isdigit(*(pTmp-1)) && isdigit(*(pTmp+1)))
         *pTmp = '-';
   }

   vmemcpy(pOutbuf+OFF_M_CTY_ST_D, acAddr2, SIZ_M_CTY_ST_D);

   parseAdr2_1(&sMailAdr, acAddr2);
   if (sMailAdr.City[0] > ' ')
   {
      iTmp = strlen(sMailAdr.City);
      if (iTmp > SIZ_M_CITY) iTmp = SIZ_M_CITY;
      memcpy(pOutbuf+OFF_M_CITY, sMailAdr.City, iTmp);

      if (sMailAdr.State[0] > ' ')
         memcpy(pOutbuf+OFF_M_ST, sMailAdr.State, SIZ_M_ST);

      // Zipcode
      if (*pMZip > ' ')
         vmemcpy(pOutbuf+OFF_M_ZIP, pMZip, SIZ_M_ZIP);
      else
         vmemcpy(pOutbuf+OFF_M_ZIP, sMailAdr.Zip, SIZ_M_ZIP);
   }
}

/******************************* Ker_ConvertChar ****************************
 *
 * char0709.txt is a text file, no header, no quote
 * residential_characteristics.csv is a text file with header and quote identify
 *
 * Return > 0 if successful.  < 0 is file open error, 0 is sort error.
 *
 ****************************************************************************/

int Ker_ConvertChar(char *pInfile, char *pOutfile)
{
   FILE     *fdIn, *fdOut;
   char     acBuf[1024], acTmpFile[256], acTmp[256], *pRec, *pTmp;
   int      iRet, iTmp, iCnt=0;
   int      iBeds, iFBaths, iHBaths, iStories, iFirePlaces;
   long     lBldgSqft, lGarSqft, lBsmntSqft;
   KER_CHAR myCharRec;

   if (!(fdIn = fopen(pInfile, "r")))
   {
      LogMsg("***** Error opening input file %s", pInfile);
      return -1;
   }

   strcpy(acTmpFile, pInfile);
   pRec = strrchr(acTmpFile, '.');
   if (pRec)
      strcpy(pRec, ".tmp");

   if (!(fdOut = fopen(acTmpFile, "w")))
   {
      fclose(fdIn);
      LogMsg("***** Error creating output file %s", acTmpFile);
      return -2;
   }

   // Skip first record - header
   pRec = fgets(acBuf, 1024, fdIn);
   iBeds=iFBaths=iHBaths=iStories=iFirePlaces = 0;
   lBldgSqft=lGarSqft=lBsmntSqft = 0;
   while (pRec = fgets(acBuf, 1024, fdIn))
   {
      iRet = ParseStringNQ(pRec, ',', CHAR_SEQNUM+1, apTokens);
      if (iRet < CHAR_UPDATED_DATE)
         break;

      memset((void *)&myCharRec, ' ', sizeof(KER_CHAR));
      memcpy(myCharRec.Atn, apTokens[CHAR_ATN], strlen(apTokens[CHAR_ATN]));
      // Right justified Entity_No for sorting
      iTmp = sprintf(acTmp, "%*s", CSIZ_ENTITY_NO, apTokens[CHAR_ENTITY_NO]);
      if (iTmp > 0)
         memcpy(myCharRec.Entity_No, acTmp, iTmp);
      memcpy(myCharRec.Design, apTokens[CHAR_DESIGN], strlen(apTokens[CHAR_DESIGN]));
      memcpy(myCharRec.Qc_Cnstr, apTokens[CHAR_QC_CNSTR], strlen(apTokens[CHAR_QC_CNSTR]));
      memcpy(myCharRec.Qc_Rating, apTokens[CHAR_QC_RATING], strlen(apTokens[CHAR_QC_RATING]));
      memcpy(myCharRec.Qc_Shape, apTokens[CHAR_QC_SHAPE], strlen(apTokens[CHAR_QC_SHAPE]));

      // Condition
      if (*apTokens[CHAR_CONDITION] == 'A' || *apTokens[CHAR_CONDITION] == 'E' ||
          *apTokens[CHAR_CONDITION] == 'F' || *apTokens[CHAR_CONDITION] == 'G' ||
          *apTokens[CHAR_CONDITION] == 'N' || *apTokens[CHAR_CONDITION] == 'P' )
         myCharRec.Condition[0] = *apTokens[CHAR_CONDITION];

      // Year built/eff
      if (*apTokens[CHAR_YEAR_BLT] > '0')
         memcpy(myCharRec.Year_Blt, apTokens[CHAR_YEAR_BLT], strlen(apTokens[CHAR_YEAR_BLT]));
      if (*apTokens[CHAR_YEAR_EFF] > '0')
         memcpy(myCharRec.Year_Eff, apTokens[CHAR_YEAR_EFF], strlen(apTokens[CHAR_YEAR_EFF]));

      // Sqft
      memcpy(myCharRec.Sf_1St_Fl, apTokens[CHAR_SF_1ST_FL], strlen(apTokens[CHAR_SF_1ST_FL]));
      memcpy(myCharRec.Sf_2Nd_Fl, apTokens[CHAR_SF_2ND_FL], strlen(apTokens[CHAR_SF_2ND_FL]));
      memcpy(myCharRec.Sf_Abv_2Nd, apTokens[CHAR_SF_ABV_2ND], strlen(apTokens[CHAR_SF_ABV_2ND]));
      memcpy(myCharRec.Sf_Bsmnt, apTokens[CHAR_SF_BSMNT], strlen(apTokens[CHAR_SF_BSMNT]));
      memcpy(myCharRec.Sf_Porch, apTokens[CHAR_SF_PORCH], strlen(apTokens[CHAR_SF_PORCH]));
      memcpy(myCharRec.Sf_Garage, apTokens[CHAR_SF_GARAGE], strlen(apTokens[CHAR_SF_GARAGE]));
      memcpy(myCharRec.Sf_Garage2, apTokens[CHAR_SF_GARAGE2], strlen(apTokens[CHAR_SF_GARAGE2]));
      memcpy(myCharRec.Sf_Patio, apTokens[CHAR_SF_PATIO], strlen(apTokens[CHAR_SF_PATIO]));
      memcpy(myCharRec.Sf_Carport, apTokens[CHAR_SF_CARPORT], strlen(apTokens[CHAR_SF_CARPORT]));
      memcpy(myCharRec.Sf_Addition, apTokens[CHAR_SF_ADDITION], strlen(apTokens[CHAR_SF_ADDITION]));
      // Fernando use this field for BldgSqft.
      memcpy(myCharRec.Sf_Total, apTokens[CHAR_SF_TOTAL], strlen(apTokens[CHAR_SF_TOTAL]));

      if (*apTokens[CHAR_AM_2ND_FL] > ' ')   myCharRec.Am_2Nd_Fl  = *apTokens[CHAR_AM_2ND_FL];
      if (*apTokens[CHAR_AM_ABV_2ND] > ' ')  myCharRec.Am_Abv_2nd = *apTokens[CHAR_AM_ABV_2ND];
      if (*apTokens[CHAR_AM_BSMNT] > ' ')    myCharRec.Am_Bsmnt   = *apTokens[CHAR_AM_BSMNT];
      if (*apTokens[CHAR_AM_PORCH] > ' ')    myCharRec.Am_Porch   = *apTokens[CHAR_AM_PORCH];
      if (*apTokens[CHAR_AM_GARAGE] > ' ')   myCharRec.Am_Garage  = *apTokens[CHAR_AM_GARAGE];
      if (*apTokens[CHAR_AM_GARAGE2] > ' ')  myCharRec.Am_Garage2 = *apTokens[CHAR_AM_GARAGE2];
      if (*apTokens[CHAR_AM_PATIO] > ' ')    myCharRec.Am_Patio   = *apTokens[CHAR_AM_PATIO];
      if (*apTokens[CHAR_AM_CARPORT] > ' ')  myCharRec.Am_Carport = *apTokens[CHAR_AM_CARPORT];
      if (*apTokens[CHAR_AM_ADDITION] > ' ') myCharRec.Am_Addition= *apTokens[CHAR_AM_ADDITION];
      if (*apTokens[CHAR_CV_ADDITION] > ' ') myCharRec.Cv_Addition= *apTokens[CHAR_CV_ADDITION];
      if (*apTokens[CHAR_CV_PORCH] > ' ')    myCharRec.Cv_Porch   = *apTokens[CHAR_CV_PORCH];
      if (*apTokens[CHAR_CV_PATIO] > ' ')    myCharRec.Cv_Patio   = *apTokens[CHAR_CV_PATIO];
      if (*apTokens[CHAR_CV_GARAGE] > ' ')   myCharRec.Cv_Garage  = *apTokens[CHAR_CV_GARAGE];
      if (*apTokens[CHAR_CV_GARAGE2] > ' ')  myCharRec.Cv_Garage2 = *apTokens[CHAR_CV_GARAGE2];
      if (*apTokens[CHAR_DET_GARAGE] > ' ')  myCharRec.Det_Garage = *apTokens[CHAR_DET_GARAGE];
      if (*apTokens[CHAR_DET_GARAGE2] > ' ') myCharRec.Det_Garage2= *apTokens[CHAR_DET_GARAGE2];

      if (*apTokens[CHAR_FMLY_ROOM] > ' ')   myCharRec.Fmly_Room  = *apTokens[CHAR_FMLY_ROOM];
      if (*apTokens[CHAR_DINING_ROOM] > ' ') myCharRec.Dining_Room= *apTokens[CHAR_DINING_ROOM];
      if (*apTokens[CHAR_AIRCOND] > ' ')     myCharRec.Aircond    = *apTokens[CHAR_AIRCOND];
      if (*apTokens[CHAR_POOL] > ' ')        myCharRec.Pool       = *apTokens[CHAR_POOL];
      if (*apTokens[CHAR_SPA] > ' ')         myCharRec.Spa        = *apTokens[CHAR_SPA];

      memcpy(myCharRec.Add_Date, apTokens[CHAR_ADD_DATE], strlen(apTokens[CHAR_ADD_DATE]));
      memcpy(myCharRec.Add_Factor, apTokens[CHAR_ADD_FACTOR], strlen(apTokens[CHAR_ADD_FACTOR]));
      memcpy(myCharRec.Stories, apTokens[CHAR_STORIES], strlen(apTokens[CHAR_STORIES]));
      memcpy(myCharRec.Bedrooms, apTokens[CHAR_BEDROOMS], strlen(apTokens[CHAR_BEDROOMS]));
      if (*apTokens[CHAR_BATHS] && strcmp(apTokens[CHAR_BATHS], "99.99") )
         memcpy(myCharRec.Baths, apTokens[CHAR_BATHS], strlen(apTokens[CHAR_BATHS]));
      memcpy(myCharRec.Fireplaces, apTokens[CHAR_FIREPLACES], strlen(apTokens[CHAR_FIREPLACES]));
      // Convert date to YYYYMMDD
      pTmp = dateConversion(apTokens[CHAR_LST_INSP_DATE], acTmp, YYYY_MM_DD);
      if (pTmp)
         memcpy(myCharRec.Lst_Insp_Date, acTmp, strlen(acTmp));

      myCharRec.CRLF[0] = '\n';
      myCharRec.CRLF[1] = '\0';
      fputs((char *)&myCharRec, fdOut);

      if (!(++iCnt % 1000))
         printf("\r%u", iCnt);
   }

   if (fdIn) fclose(fdIn);
   if (fdOut) fclose(fdOut);

   if (!pOutfile)
   {
      pTmp = strrchr(pInfile, '.');
      if (pTmp)
         strcpy(pTmp, ".dat");
      else
         strcat(pInfile, ".dat");
      pTmp = pInfile;
   } else
      pTmp = pOutfile;

   // Sort output on APN, Entity_No ascending
   printf("\nSorting %s\n", pTmp);
   iRet = sortFile(acTmpFile, pTmp, "S(1,12,C,A,) F(TXT)");
   // Remove temp file if sort success
   if (iRet > 0)
      remove(acTmpFile);

   printf("\n");
   return iRet;
}

/********************************* Ker_MergeChar *****************************
 *
 * Some parcel has more than one entries.  We total up Beds, Baths, Sqft.  Other
 * fields are from the first entity only.
 *
 *****************************************************************************/

int Ker_MergeChar(char *pOutbuf)
{
   static   char acRec[1024], *pRec=NULL;
   char     acTmp[256], acCode[32];
   long     lTmp, lBldgSqft, lGarSqft, lCarportSqft, lYrBlt, lYrEff;
   int      iRet, iTmp, iLoop, iBeds, iFBath, iHBath, iBldgNum, iFp;
   int      iBath_1Q, iBath_2Q, iBath_3Q, iBath_4Q;
   double   dTmp;

   KER_CHAR *pChar;

   iRet=iBeds=iFBath=iHBath=iBldgNum=iFp=iBath_1Q=iBath_2Q=iBath_3Q=iBath_4Q=0;
   lBldgSqft=lGarSqft=lCarportSqft=lYrBlt=lYrEff=0;

   // Get first Char rec for first call
   if (!pRec && !lCharMatch)
   {
      pRec = fgets(acRec, 1024, fdChar);
   }
   pChar = (KER_CHAR *)pRec;

   do
   {
      if (!pRec)
      {
         fclose(fdChar);
         fdChar = NULL;
         return 1;      // EOF
      }

      // Compare Apn
      iLoop = memcmp(pOutbuf, pChar->Atn, 8);
      if (iLoop > 0)
      {
         //if (bDebug)
         //   LogMsg0("Skip Char rec  %.*s", CSIZ_APN, pChar->Apn);
         pRec = fgets(acRec, 1024, fdChar);
         lCharSkip++;
      }
   } while (iLoop > 0);

   // If not match, return
   if (iLoop)
      return 0;

#ifdef _DEBUG
      //if (!memcmp(pOutbuf, "003120007", 9))
      //   lTmp = 0;
#endif
   while (!iLoop)
   {
      // YrEff
      lTmp = atoin(pChar->Year_Eff, CSIZ_YEAR);
      if (lTmp > lYrEff)
      {
         lYrEff = lTmp;
         memcpy(pOutbuf+OFF_YR_EFF, pChar->Year_Eff, SIZ_YR_BLT);
      }

      // YrBlt
      lTmp = atoin(pChar->Year_Blt, CSIZ_YEAR);
      if (lTmp > 1800 && !lYrBlt)
      {
         lYrBlt = lTmp;
         memcpy(pOutbuf+OFF_YR_BLT, pChar->Year_Blt, SIZ_YR_BLT);
      }

      // Quality/class
      if (iBldgNum < 2)
      {
         if (pChar->Qc_Cnstr[0] > ' ')
         {
            *(pOutbuf+OFF_BLDG_CLASS) = pChar->Qc_Cnstr[0];
            iTmp = atoin(pChar->Qc_Rating, CSIZ_QC_RATING);
            if (iTmp > 0)
            {
               // Prevent misspelling
               if (iTmp > 10)
               {
                  sprintf(acTmp, "%c.%c", pChar->Qc_Rating[0], pChar->Qc_Rating[1]);
               } else
                  sprintf(acTmp, "%.3s", pChar->Qc_Rating);
               iRet = Value2Code(acTmp, acCode, NULL);
               blankPad(acCode, SIZ_BLDG_QUAL);
               memcpy(pOutbuf+OFF_BLDG_QUAL, acCode, SIZ_BLDG_QUAL);
            }
         }

         // Condition
         *(pOutbuf+OFF_IMPR_COND) = pChar->Condition[0];

         // Stories
         iTmp = atoin(pChar->Stories, CSIZ_STORIES);
         if (iTmp > 0 && iTmp < 99)
         {
            if (iTmp > 9 && bDebug)
               LogMsg("%.*s : %d", iApnLen, pOutbuf, iTmp);
            sprintf(acTmp, "%d.0  ", iTmp);
            memcpy(pOutbuf+OFF_STORIES, acTmp, SIZ_STORIES);
         }

         // Air Cond - See Ker Notes.txt
         switch (pChar->Aircond)
         {
            case 'Y':
               *(pOutbuf+OFF_AIR_COND) = 'C';
               break;
            case 'C':
            case 'D':
            case 'E':
            case 'H':
            case 'O':
            case 'R':
            case 'W':
               *(pOutbuf+OFF_AIR_COND) = pChar->Aircond;
               break;
         }

         // Pool/Spa
         if (pChar->Pool == 'T' && pChar->Spa == 'T')
            *(pOutbuf+OFF_POOL) = 'C';    // Pool and Spa
         else if (pChar->Pool == 'T')
            *(pOutbuf+OFF_POOL) = 'P';    // Pool
         else if (pChar->Spa == 'T')
            *(pOutbuf+OFF_POOL) = 'S';    // Spa
      }

      // Fireplace
      iFp += atoin(pChar->Fireplaces, CSIZ_FIREPLACES);
      if (iFp > 0 && iFp < 99)
      {
         sprintf(acTmp, "%*u", SIZ_FIRE_PL, iFp);
         memcpy(pOutbuf+OFF_FIRE_PL, acTmp, SIZ_FIRE_PL);
      }

      // BldgSqft
      lBldgSqft += atoin(pChar->Sf_Total, CSIZ_SQFT);
      if (lBldgSqft > 10)
      {
         sprintf(acTmp, "%*u", SIZ_BLDG_SF, lBldgSqft);
         memcpy(pOutbuf+OFF_BLDG_SF, acTmp, SIZ_BLDG_SF);
      }

      // Garage Sqft - will decide later how to handle multi-building parcel
      lTmp = atoin(pChar->Sf_Garage, CSIZ_SQFT);
      lTmp += atoin(pChar->Sf_Garage2, CSIZ_SQFT);
      if (lTmp > 10)
      {
         sprintf(acTmp, "%*u", SIZ_GAR_SQFT, lTmp+lGarSqft);
         memcpy(pOutbuf+OFF_GAR_SQFT, acTmp, SIZ_GAR_SQFT);
         if (pChar->Det_Garage == 'T')
            *(pOutbuf+OFF_PARK_TYPE) = 'L';     // detached garage
         else
            *(pOutbuf+OFF_PARK_TYPE) = 'I';     // attached garage
         lGarSqft += lTmp;
      } else if (!lGarSqft && !lCarportSqft)
      {
         lCarportSqft = atoin(pChar->Sf_Carport, CSIZ_SQFT);
         if (lCarportSqft > 10)
         {
            sprintf(acTmp, "%*u", SIZ_GAR_SQFT, lCarportSqft);
            memcpy(pOutbuf+OFF_GAR_SQFT, acTmp, SIZ_GAR_SQFT);
            *(pOutbuf+OFF_PARK_TYPE) = 'C';
         }
      }

      // Beds
      iBeds += atoin(pChar->Bedrooms, CSIZ_BEDROOMS);
      if (iBeds > 0)
      {
         sprintf(acTmp, "%*u", SIZ_BEDS, iBeds);
         memcpy(pOutbuf+OFF_BEDS, acTmp, SIZ_BEDS);
      }

      // Baths
      dTmp = atofn(pChar->Baths, CSIZ_BATHS);
      if (dTmp > 0.0)
      {
         iTmp = (int) dTmp;
         iBath_4Q += iTmp;
         iFBath += iTmp;

         dTmp -= iTmp;
         if (dTmp > 0.0)
         {
            if (dTmp >= 0.6)
            {
               *(pOutbuf+OFF_BATH_3Q) = '1';
               iFBath++;
               iBath_3Q++;
            } else if (iTmp >= 0.4)
            {
               iBath_2Q++;
               iHBath++;
            } else 
            {
               iBath_1Q++;
               iHBath++;
            }
         }

         if (iFBath > 0)
         {
            sprintf(acTmp, "%*u", SIZ_BATH_F, iFBath);
            memcpy(pOutbuf+OFF_BATH_F, acTmp, SIZ_BATH_F);
         }
         if (iHBath > 0)
         {
            sprintf(acTmp, "%*u", SIZ_BATH_H, iHBath);
            memcpy(pOutbuf+OFF_BATH_H, acTmp, SIZ_BATH_H);
         }

         // Clear old data
         *(pOutbuf+OFF_BATH_1Q) = ' ';
         *(pOutbuf+OFF_BATH_2Q) = ' ';
         *(pOutbuf+OFF_BATH_3Q) = ' ';
         *(pOutbuf+OFF_BATH_4Q) = ' ';

         if (iBath_1Q > 0)
            *(pOutbuf+OFF_BATH_1Q) = 0x30|iBath_1Q;
         if (iBath_2Q > 0)
            *(pOutbuf+OFF_BATH_2Q) = 0x30|iBath_2Q;
         if (iBath_3Q > 0)
            *(pOutbuf+OFF_BATH_3Q) = 0x30|iBath_3Q;
         if (iBath_4Q > 0)
            *(pOutbuf+OFF_BATH_4Q) = 0x30|iBath_4Q;
      }


      // Buildings
      iBldgNum = atoin(pChar->Entity_No, CSIZ_ENTITY_NO);

      // Others

      // Get next Char rec
      pRec = fgets(acRec, 1024, fdChar);
      if (!pRec)
         break;

      iLoop = memcmp(pOutbuf, pChar->Atn, 8);
   }

   // Update number of buildings
   if (iBldgNum > 0)
   {
      sprintf(acTmp, "%*u", SIZ_BLDGS, iBldgNum);
      memcpy(pOutbuf+OFF_BLDGS, acTmp, SIZ_BLDGS);
   }

   lCharMatch++;
   return 0;
}

int Ker_MergeCVal(char *pOutbuf, bool bOverWriteAcre=true)
{
   static   char acRec[2048], *pRec=NULL;
   char     acTmp[256];
   long     lTmp;
   int      iLoop;
   double   dTmp;

   KER_CVAL *pChar=(KER_CVAL *)&acRec[0];

   // Get first Char rec for first call
   if (!pRec)
      pRec = fgets(acRec, 2048, fdChar);

   do
   {
      if (!pRec)
      {
         fclose(fdChar);
         fdChar = NULL;
         return 1;      // EOF
      }

      // Compare Apn
      iLoop = memcmp(pOutbuf, pChar->Atn, iApnLen);
      if (iLoop > 0)
      {
         if (bDebug)
            LogMsg0("Skip Char rec  %.*s", iApnLen, pChar->Atn);
         pRec = fgets(acRec, 2048, fdChar);
         lCharSkip++;
      }
   } while (iLoop > 0);

   // If not match, return
   if (iLoop)
      return 1;

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "11319225006", 11))
   //   lTmp = 0;
#endif

   // BldgSqft
   lTmp = atoin(pChar->BldgSqft, VSIZ_SQFT);
   if (lTmp > 10)
   {
      sprintf(acTmp, "%*u", SIZ_BLDG_SF, lTmp);
      memcpy(pOutbuf+OFF_BLDG_SF, acTmp, SIZ_BLDG_SF);
   }

   // Bldgs
   lTmp = atoin(pChar->Bldgs, VSIZ_COUNT);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*u", SIZ_BLDGS, lTmp);
      memcpy(pOutbuf+OFF_BLDGS, acTmp, SIZ_BLDGS);
   }

   // LotAcres
   dTmp = atofn(pChar->LotAcres, VSIZ_SQFT);
   lTmp = atoin(pOutbuf+OFF_LOT_SQFT, SIZ_LOT_SQFT);
   if (dTmp > 0.0 && (bOverWriteAcre || !lTmp))
   {
      sprintf(acTmp, "%*u", SIZ_LOT_ACRES, (long)(dTmp*1000));
      memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);

      lTmp = (long)(dTmp*SQFT_PER_ACRE);
      sprintf(acTmp, "%*u", SIZ_LOT_SQFT, lTmp);
      memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);
   }

   // Zoning
   if (pChar->Zoning[0] > ' ')
   {
      memcpy(pOutbuf+OFF_ZONE, pChar->Zoning, SIZ_ZONE);
      if (*(pOutbuf+OFF_ZONE_X1) == ' ')
         memcpy(pOutbuf+OFF_ZONE_X1, pChar->Zoning, SIZ_ZONE_X1);
   }

   // Units
   lTmp = atoin(pChar->Units, VSIZ_COUNT);
   if (lTmp > 1 && lTmp < 1000)
   {
      sprintf(acTmp, "%*u", SIZ_UNITS, lTmp);
      memcpy(pOutbuf+OFF_UNITS, acTmp, SIZ_UNITS);
   }

   // Basement
   lTmp = atoin(pChar->BsmtSqft, VSIZ_SQFT);
   if (lTmp > 10)
   {
      sprintf(acTmp, "%*u", SIZ_BSMT_SF, lTmp);
      memcpy(pOutbuf+OFF_BSMT_SF, acTmp, SIZ_BSMT_SF);
   }

   // Pool/Spa
   if (pChar->HasPool[0] == '1' && pChar->HasSpa[0] == '1')
      *(pOutbuf+OFF_POOL) = 'C';    // Pool and Spa
   else if (pChar->HasPool[0] == '1')
      *(pOutbuf+OFF_POOL) = 'P';    // Pool
   else if (pChar->HasSpa[0] == '1')
      *(pOutbuf+OFF_POOL) = 'S';    // Spa

   // HasView
   if (pChar->HasView[0] == '1')
      *(pOutbuf+OFF_VIEW) = 'A';    // 12/19/2016 sn

   // IsGated

   // Get next Char rec
   pRec = fgets(acRec, 2048, fdChar);

   lCharMatch++;
   return 0;
}

int Ker_MergeBldg(char *pOutbuf)
{
   static   char acRec[2048], *pRec=NULL;
   char     acTmp[256], acCode[32];
   long     lTmp, lBldgSqft, lGarSqft, lCarportSqft, lYrBlt, lYrEff;
   int      iRet, iTmp, iLoop, iBeds, iFBath, iHBath, iBldgNum, iFp;
   double   dTmp;

   KER_CVAL *pChar;

   iRet=iBeds=iFBath=iHBath=iBldgNum=iFp=0;
   lBldgSqft=lGarSqft=lCarportSqft=lYrBlt=lYrEff=0;

   // Get first Char rec for first call
   if (!pRec)
      pRec = fgets(acRec, 2048, fdBldg);

   pChar = (KER_CVAL *)&acRec[0];

   do
   {
      if (!pRec)
      {
         fclose(fdBldg);
         fdBldg = NULL;
         return 1;      // EOF
      }

      // Compare Apn
      iLoop = memcmp(pOutbuf, pChar->Atn, iApnLen);
      if (iLoop > 0)
      {
         if (bDebug)
            LogMsg0("Skip Char rec  %.*s", iApnLen, pChar->Atn);
         pRec = fgets(acRec, 2048, fdBldg);
         lBldgSkip++;
      }
   } while (iLoop > 0);

   // If not match, return
   if (iLoop)
      return 1;

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "46415011008", 11))
   //   lTmp = 0;
#endif
   //while (!iLoop)
   //{
      // YrEff
      lYrEff = atoin(pChar->YrEff, CSIZ_YEAR);
      if (lYrEff > 1800 && lYrEff <= lToyear)
         memcpy(pOutbuf+OFF_YR_EFF, pChar->YrEff, SIZ_YR_BLT);

      // YrBlt
      lYrBlt = atoin(pChar->YrBlt, CSIZ_YEAR);
      if (lYrBlt > 1800 && lYrBlt <= lToyear)
         memcpy(pOutbuf+OFF_YR_BLT, pChar->YrBlt, SIZ_YR_BLT);

      // Quality/class
      *(pOutbuf+OFF_BLDG_CLASS) = pChar->ConstType[0];
      dTmp = atof(pChar->R_QualityType);
      if (dTmp > 0.0)
      {
         sprintf(acTmp, "%.1f", dTmp);
         iRet = Value2Code(acTmp, acCode, NULL);
         blankPad(acCode, SIZ_BLDG_QUAL);
         memcpy(pOutbuf+OFF_BLDG_QUAL, acCode, SIZ_BLDG_QUAL);
      }

      // Condition
      *(pOutbuf+OFF_IMPR_COND) = pChar->Condition[0];

      // Stories
      iTmp = atoin(pChar->Stories, VSIZ_COUNT);
      if (iTmp > 0 && iTmp < 99)
      {
         if (iTmp > 9 && bDebug)
            LogMsg("%.*s : %d", iApnLen, pOutbuf, iTmp);
         sprintf(acTmp, "%d.0  ", iTmp);
         memcpy(pOutbuf+OFF_STORIES, acTmp, SIZ_STORIES);
      }

      // Basement
      iTmp = atoin(pChar->BsmtSqft, VSIZ_SQFT);
      if (iTmp > 10)
      {
         sprintf(acTmp, "%*u", SIZ_BSMT_SF, iTmp);
         memcpy(pOutbuf+OFF_BSMT_SF, acTmp, SIZ_BSMT_SF);
      }

      // Additional Sqft

      // Air Cond - See Ker Notes.txt
      /*
         Active Solar               405
         Central Refrigeration      35711
         Dual-Pack                  63614
         Evaporative With Ducts     29871
         Floor or Wall Heater       163678
         Forced Air & Refrigeration 99978
         Forced Heat                7617
         None                       526
         Not Used                   16370
         Passive Solare             7
         Wall or Window Unit        3615
         Y (Unknown)                3972

         Complete Engineered Cooling System  773
         Evaporative Coolers                 1423
         Heat Pump                           7
         Other                               3834
         Package Units                       4032
         Window Units                        209

      */
      switch (pChar->AirCond[0])
      {
         case 'A':
            *(pOutbuf+OFF_AIR_COND) = 'J';
            *(pOutbuf+OFF_HEAT) = 'K';
            break;
         case 'Y':
         case 'O':
            *(pOutbuf+OFF_AIR_COND) = 'X';
            *(pOutbuf+OFF_HEAT) = 'X';
            break;
         case 'C':
            if (!_memicmp(pChar->AirCond, "Central", 5))
            {
               *(pOutbuf+OFF_AIR_COND) = 'C';
               *(pOutbuf+OFF_HEAT) = 'Z';
            } else
            {
               *(pOutbuf+OFF_AIR_COND) = 'Y';
               *(pOutbuf+OFF_HEAT) = 'Q';
            }
            break;
         case 'D':
            *(pOutbuf+OFF_AIR_COND) = 'D';
            *(pOutbuf+OFF_HEAT) = 'X';
            break;
         case 'E':
            *(pOutbuf+OFF_AIR_COND) = 'E';
            *(pOutbuf+OFF_HEAT) = 'X';
            break;
         case 'F':
            if (!_memicmp(pChar->AirCond, "Floor", 5))
            {
               *(pOutbuf+OFF_AIR_COND) = 'M';
               *(pOutbuf+OFF_HEAT) = 'M';
            } else if (!_memicmp(pChar->AirCond, "Forced Air", 8))
            {
               *(pOutbuf+OFF_AIR_COND) = 'A';
               *(pOutbuf+OFF_HEAT) = 'B';
            } else if (!_memicmp(pChar->AirCond, "Forced Heat", 8))
            {
               *(pOutbuf+OFF_AIR_COND) = 'H';
               *(pOutbuf+OFF_HEAT) = 'B';
            }
            break;
         case 'H':
            *(pOutbuf+OFF_AIR_COND) = 'H';
            *(pOutbuf+OFF_HEAT) = 'G';
            break;
         case 'N':
            *(pOutbuf+OFF_AIR_COND) = 'N';
            *(pOutbuf+OFF_HEAT) = 'L';
            break;
         case 'P':
            if (!_memicmp(pChar->AirCond, "Pass", 4))
            {
               *(pOutbuf+OFF_AIR_COND) = 'J';
               *(pOutbuf+OFF_HEAT) = 'K';
            } else
            {
               *(pOutbuf+OFF_AIR_COND) = 'X';
               *(pOutbuf+OFF_HEAT) = 'X';
            }
            break;
         case 'W':
            *(pOutbuf+OFF_AIR_COND) = 'W';
            *(pOutbuf+OFF_HEAT) = 'W';
            break;
      }

      /*
         Electricity                93
         Floor Unit                 20
         Forced                     233
         Gas                        1243
         Other                      32
         Space Heaters              34
         Suspended Radiant Heaters  19
         Wall Unit                  89
      */
      switch (pChar->Heat[0])
      {
         case 'E':
            *(pOutbuf+OFF_HEAT) = 'F';       // Electric
            break;
         case 'F':
            if (!_memicmp(pChar->Heat, "Floor", 5))
               *(pOutbuf+OFF_HEAT) = 'C';
            else
               *(pOutbuf+OFF_HEAT) = 'B';    // Forced air
            break;
         case 'G':
            *(pOutbuf+OFF_HEAT) = 'N';       // Gas
            break;
         case 'O':
            *(pOutbuf+OFF_HEAT) = 'X';       // Other
            break;
         case 'S':
            if (!_memicmp(pChar->Heat, "Space", 5))
               *(pOutbuf+OFF_HEAT) = 'J';
            else
               *(pOutbuf+OFF_HEAT) = 'X';
            break;
         case 'W':
            *(pOutbuf+OFF_HEAT) = 'D';       // Wall
            break;
      }

      // Pool/Spa
      if (pChar->HasPool[0] == '1' && pChar->HasSpa[0] == '1')
         *(pOutbuf+OFF_POOL) = 'C';    // Pool and Spa
      else if (pChar->HasPool[0] == '1')
         *(pOutbuf+OFF_POOL) = 'P';    // Pool
      else if (pChar->HasSpa[0] == '1')
         *(pOutbuf+OFF_POOL) = 'S';    // Spa

      // Fireplace
      iFp = atoin(pChar->Fireplaces, VSIZ_COUNT);
      if (iFp > 0 && iFp < 99)
      {
         sprintf(acTmp, "%*u", SIZ_FIRE_PL, iFp);
         memcpy(pOutbuf+OFF_FIRE_PL, acTmp, SIZ_FIRE_PL);
      }

      // BldgSqft
      //lBldgSqft = atoin(pChar->BldgSqft, VSIZ_SQFT);
      //if (lBldgSqft > 10)
      //{
      //   sprintf(acTmp, "%*u", SIZ_BLDG_SF, lBldgSqft);
      //   memcpy(pOutbuf+OFF_BLDG_SF, acTmp, SIZ_BLDG_SF);
      //}

      // Garage Sqft - will decide later how to handle multi-building parcel
      lGarSqft = atoin(pChar->GarageSqft, VSIZ_SQFT);
      lGarSqft += atoin(pChar->GarageSqft2, VSIZ_SQFT);
      if (lGarSqft > 10)
      {
         sprintf(acTmp, "%*u", SIZ_GAR_SQFT, lGarSqft);
         memcpy(pOutbuf+OFF_GAR_SQFT, acTmp, SIZ_GAR_SQFT);
         if (pChar->GarageDetached[0] == '1')
            *(pOutbuf+OFF_PARK_TYPE) = 'L';     // detached garage
         else
            *(pOutbuf+OFF_PARK_TYPE) = 'I';     // attached garage
      } else 
      {
         lCarportSqft = atoin(pChar->CarportSqft, VSIZ_SQFT);
         if (lCarportSqft > 10)
         {
            sprintf(acTmp, "%*u", SIZ_GAR_SQFT, lCarportSqft);
            memcpy(pOutbuf+OFF_GAR_SQFT, acTmp, SIZ_GAR_SQFT);
            *(pOutbuf+OFF_PARK_TYPE) = 'C';
         }
      }

      // Beds
      iBeds = atoin(pChar->Beds, VSIZ_COUNT);
      if (iBeds > 0)
      {
         sprintf(acTmp, "%*u", SIZ_BEDS, iBeds);
         memcpy(pOutbuf+OFF_BEDS, acTmp, SIZ_BEDS);
      }

      // Baths
      dTmp = atofn(pChar->Baths, VSIZ_COUNT);
      if (dTmp > 0.0)
      {
         iTmp = (int) dTmp;
         dTmp -= iTmp;
         if (dTmp > 0.5)
            iFBath++;
         else if (dTmp > 0.1)
            iHBath++;

         iFBath += iTmp;
         if (iFBath > 0)
         {
            sprintf(acTmp, "%*u", SIZ_BATH_F, iFBath);
            memcpy(pOutbuf+OFF_BATH_F, acTmp, SIZ_BATH_F);
         }
         if (iHBath > 0)
         {
            sprintf(acTmp, "%*u", SIZ_BATH_H, iHBath);
            memcpy(pOutbuf+OFF_BATH_H, acTmp, SIZ_BATH_H);
         }
      }


      // Buildings
      iBldgNum = atoin(pChar->Bldgs, VSIZ_COUNT);

      // Basement
      lTmp = atoin(pChar->BsmtSqft, VSIZ_SQFT);
      if (lTmp > 10)
      {
         sprintf(acTmp, "%*u", SIZ_BSMT_SF, lTmp);
         memcpy(pOutbuf+OFF_BSMT_SF, acTmp, SIZ_BSMT_SF);
      }

      // Patio
      lTmp = atoin(pChar->PatioAreaSqft, VSIZ_SQFT);
      if (lTmp > 10)
      {
         sprintf(acTmp, "%*u", SIZ_PATIO_SF, lTmp);
         memcpy(pOutbuf+OFF_PATIO_SF, acTmp, SIZ_PATIO_SF);
      }

      // Porch
      lTmp = atoin(pChar->PorchAreaSqft, VSIZ_SQFT);
      if (lTmp > 10)
      {
         sprintf(acTmp, "%*u", SIZ_PORCH_SF, lTmp);
         memcpy(pOutbuf+OFF_PORCH_SF, acTmp, SIZ_PORCH_SF);
      }

      // Others

      // Get next Char rec
      pRec = fgets(acRec, 2048, fdBldg);
      //if (!pRec)
      //   break;

   //   iLoop = memcmp(pOutbuf, pChar->Atn, 8);
   //}

   // Update number of buildings
   //if (iBldgNum > 0)
   //{
   //   sprintf(acTmp, "%*u", SIZ_BLDGS, iBldgNum);
   //   memcpy(pOutbuf+OFF_BLDGS, acTmp, SIZ_BLDGS);
   //}

   lBldgMatch++;
   return 0;
}

/******************************** Ker_MergeLegal *****************************
 *
 * The following will assist in decoding the 13 byte formatted legal
 * based on the legal type code
 *
 * "C" =  CITY-BLOCK-LOT
 * CITY-ABBR           X       6
 * BLOCK               X       3
 * CITY-LOT            X       4         CTY XXXXXX B XXX L XXXX
 * ex: C HOMPK 1  PTN ==> CTY HOMPK B 1 PTN
 *     C HMKER 1  8   ==> CTY HMKER B 1 L 8
 *     C CTYBK PTN464B==> CTY CTYBK B 464B PTN
 *     C LWLAD P16    ==> CTY LWLAD B 16 L P
 *     C CTYBK    447 ==> CTY CTYBK B 447
 *     C CTYBK 7-8277 ==> CTY CTYBK B 277 L 7-8
 *     C CTYBK 6  277 ==> CTY CTYBK B 277 L 6
 *     C BKFLD 2825-7 ==> CTY BLFLD B 282 L 5-7
 *     C BKFLD 3227 &8==> CTY BLFLD B 322 L 7&8
 *
 * "PM" = Parcel Map Lot
 *
 * PARCEL-MAP          X       6         PM XXXXXX L XXXX
 * PARCEL-LOT          X       4
 * FILLER-PM           X       3
 * ex: PM820   PN B ==> PM 820 L PN B
 *
 * "S" = Section Township Range
 *
 * SECTION             X       3
 * TOWNSHIP            X       3          S XXX T XXX R XXX QT XX
 * RANGE               X       3
 * QUARTER             X       2
 * FILLER-S            X       2
 * ex: S 19 29 28 ==> S 19 T 29 R 28
 *
 * "T" = Tract Map Tract Lot
 *
 * TRACT-MAP           X       6          TR XXXXXX B XXX L XXXX
 * TRACT-BLOCK         X       3
 * TRACT-LOT           X       4
 * ex: T 1472  8  11 ==> TR 1472 B 8 L 11
 *
 * "M" = Meets and Bounds and will be 13 characters
 *
 *****************************************************************************/

int Ker_MergeLegal(char *pOutbuf, char *pRollRec)
{
   static   char acRec[1024], *pRec=NULL;
   char     acTmp[256], acBlk[16], acLot[16], acDesc[LGL_UNFORM_DESC+1], *pTmp;
   unsigned long lTmp;
   double   dTmp;
   int      iTmp, iLoop, iBlk, iLot;
   KER_LEGAL *pLegal;

   // Get first Char rec for first call
   if (!pRec && !lLegalMatch)
   {
      pRec = fgets(acRec, 1024, fdLegal);
   }
   pLegal = (KER_LEGAL *)pRec;

   do
   {
      if (!pRec)
      {
         fclose(fdLegal);
         fdLegal = NULL;
         return 1;      // EOF
      }

      // Compare Apn
      //iLoop = memcmp(pRollRec+ROFF_TE_NO, pLegal->Te_No, LGL_TE_NO);
      iLoop = memcmp(pOutbuf+OFF_TE_NO, pLegal->Te_No, LGL_TE_NO);
      if (iLoop > 0)
      {
         if (bDebug)
            LogMsg0("Skip Legal rec  %.*s", LGL_TE_NO, pLegal->Te_No);
         pRec = fgets(acRec, 1024, fdLegal);
         lLegalSkip++;
      }
   } while (iLoop > 0);

   // If not match, return
   if (iLoop)
      return 1;

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "003120007", 9))
   //   lTmp = 0;
#endif

   // Acres
   dTmp = atofn(pLegal->Acres, LGL_LEGAL_ACRE);
   if (dTmp > 0.0)
   {
      sprintf(acTmp, "%*u", SIZ_LOT_ACRES, (long)(dTmp*1000));
      memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);

      lTmp = (long)(dTmp*SQFT_PER_ACRE);
      sprintf(acTmp, "%*u", SIZ_LOT_SQFT, lTmp);
      memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);
   }

   acBlk[0] = 0;
   acLot[0] = 0;

   // remove bad char
   memcpy(acDesc, pLegal->Desc, LGL_UNFORM_DESC);
   acDesc[LGL_UNFORM_DESC] = 0;
   if (pTmp = strchr(acDesc, 0x9B))
      if (*(pTmp-1) == 'W' && *(pTmp-2) == 'T')
         *pTmp = 'P';
   if (pTmp = strchr(acDesc, 0xA2))
      *pTmp = '&';

   // Tract/Blk/Lot
   if (pLegal->LegalType[0] == 'T')
   {
      memcpy(pOutbuf+OFF_TRACT, pLegal->LegalFormat, 6);

      // Block
      if (pLegal->LegalFormat[6] == '&' && pLegal->LegalFormat[9] == 'P')
      {
         // Portion of two adjacent lots: &23P 22
         iBlk = atoin((char *)&pLegal->LegalFormat[7], 2);
         iLot = atoin((char *)&pLegal->LegalFormat[11], 2);
         iTmp = sprintf(acTmp, "%d", iLot);
         sprintf(acLot, "L P %d&%d", iLot, iBlk);
      } else if (pLegal->LegalFormat[6] == 'P' && pLegal->LegalFormat[12] == '&')
      {
         // Full lot and portion of adjacent one: P1312 &
         iBlk = atoin((char *)&pLegal->LegalFormat[7], 2);
         iLot = atoin((char *)&pLegal->LegalFormat[9], 3);
         iTmp = sprintf(acTmp, "%d", iLot);
         sprintf(acLot, "L %d&P%d", iLot, iBlk);
      } else if (pLegal->LegalFormat[12] == '&')
      {
         // Two lot parcels: 112111&
         iBlk = atoin((char *)&pLegal->LegalFormat[6], 3);
         iLot = atoin((char *)&pLegal->LegalFormat[9], 3);
         iTmp = sprintf(acTmp, "%d", iLot);
         sprintf(acLot, "L %d&%d", iLot, iBlk);
      } else
      {
         if (pLegal->LegalFormat[6] > ' ')
         {
            sprintf(acBlk, "B %.3s ", pLegal->LegalFormat+6);
            memcpy(pOutbuf+OFF_BLOCK, pLegal->LegalFormat+6, 3);
         }
         if (!memcmp((char *)&pLegal->LegalFormat[9], "PTN", 3))
         {
            strcpy(acTmp, "P");
            iTmp = 1;
            strcpy(acLot, "PTN");
         } else if (!memcmp((char *)&pLegal->LegalFormat[9], "PN", 2))
         {
            iTmp = sprintf(acTmp, "%.2s", (char *)&pLegal->LegalFormat[11]);
            sprintf(acLot, "P%.2s", (char *)&pLegal->LegalFormat[11]);
         } else if (pLegal->LegalFormat[9] > ' ')
         {
            iTmp = sprintf(acTmp, "%.4s", (char *)&pLegal->LegalFormat[9]);
            sprintf(acLot, "L %.4s", (char *)&pLegal->LegalFormat[9]);
         } else
            iTmp = 0;
      }

      // Lot
      if (iTmp > 0)
         memcpy(pOutbuf+OFF_LOT, acTmp, iTmp);

      sprintf(acTmp, "TR %.6s %s %s %s", pLegal->LegalFormat, acBlk, acLot, acDesc);
   } else if (pLegal->LegalType[0] == 'S')
   {
      char section[8], township[8], range[8];

      section[0]=township[0]=range[0]=0;
      if (pLegal->LegalFormat[0] > ' ')
         sprintf(section, "S %.3s", pLegal->LegalFormat);
      if (pLegal->LegalFormat[3] > ' ')
         sprintf(township, "T %.3s", pLegal->LegalFormat+3);
      if (pLegal->LegalFormat[6] > ' ')
         sprintf(range, "R %.3s", pLegal->LegalFormat+6);
      
      sprintf(acTmp, "%s %s %s %s", section, township, range, acDesc);
   } else if (pLegal->LegalType[0] == 'C')
   {
      /* ex: C HOMPK 1  PTN ==> CTY HOMPK B 1 PTN
       *     C HMKER 1  8   ==> CTY HMKER B 1 L 8
       *     C CTYBK PTN464B==> CTY CTYBK B 464B PTN
       *     C LWLAD P16    ==> CTY LWLAD B 16 L P
       *     C CTYBK    447 ==> CTY CTYBK B 447
       *     C CTYBK 7-8277 ==> CTY CTYBK B 277 L 7-8
       *     C CTYBK 6  277 ==> CTY CTYBK B 277 L 6
       *     C BKFLD 2825-7 ==> CTY BLFLD B 282 L 5-7
       *     C BKFLD 3227 &8==> CTY BLFLD B 322 L 7&8 */
      if (isdigit(pLegal->LegalFormat[9]) && pLegal->LegalFormat[7] == '-')
      {
         sprintf(acBlk, "B %.4s", (char *)&pLegal->LegalFormat[9]);
         sprintf(acLot, "L %.3s", (char *)&pLegal->LegalFormat[6]);
      } else if (!memcmp((char *)&pLegal->LegalFormat[6], "PTN", 3))
      {
         sprintf(acBlk, "B %.4s PTN", (char *)&pLegal->LegalFormat[9]);
      } else if (!memcmp((char *)&pLegal->LegalFormat[9], "PTN", 3))
      {
         sprintf(acBlk, "B %.3s PTN", (char *)&pLegal->LegalFormat[6]);
      } else if (pLegal->LegalFormat[6] == 'P' && isdigit(pLegal->LegalFormat[7]))
      {
         sprintf(acBlk, "B %.2s L P", (char *)&pLegal->LegalFormat[7]);
      } else
      {
         if (isdigit(pLegal->LegalFormat[6]))
            sprintf(acBlk, "B %.3s", (char *)&pLegal->LegalFormat[6]);
         if (isdigit(pLegal->LegalFormat[9]))
            sprintf(acLot, "L %.4s", (char *)&pLegal->LegalFormat[9]);
      }
      sprintf(acTmp, "CTY %.6s %s %s %s", pLegal->LegalFormat, acBlk, acLot, acDesc);
   } else if (pLegal->LegalType[0] == 'M')
   {
      strcpy(acTmp, acDesc);
   } else if (!memcmp(pLegal->LegalType, "PM", 2))
   {
      /*
       * "PM" = Parcel Map Lot
       *
       * PARCEL-MAP          X       6         PM XXXXXX L XXXX
       * PARCEL-LOT          X       4
       * FILLER-PM           X       3
       * ex: PM820   PN B ==> PM 820 L PN B
       */
      sprintf(acTmp, "PM %.6s L %.4s %s", pLegal->LegalFormat, pLegal->LegalFormat+6, acDesc);
   } else
   {
      acTmp[0] = 0;     // Should not get to this point
      LogMsg("*** Bad legal record.  Please check %.10s", pLegal->Te_No);
   }

   // Legal desc
   if (acTmp[0] > ' ')
   {
      updateLegal(pOutbuf, acTmp);
      lLegalMatch++;
   }

   // Get next Char rec
   pRec = fgets(acRec, 1024, fdLegal);
   return 0;
}

/********************************** Ker_MergeSale *****************************
 *
 * Return value > 0 if sale rec is older than current
 *
 ******************************************************************************/

int Ker_MergeSale(char *pOutbuf, char *pSaleRec, int iCnt)
{
   long     lDttAmt, lRecDate, lTmp;
   int      iTmp;
   char     *pDate, *pAmt, *pDoc, *pSaleDate, acDocNum[32], acAmt[32];
   char     *pNxtDate, *pNxtAmt, *pNxtDoc;
   double   dTax;

   KER_SALE *pRec = (KER_SALE *)pSaleRec;
   pSaleDate = pRec->Rec_Date;

   // Check for date
   lRecDate = atoin(pSaleDate, SSIZ_REC_DATE);
   if (lRecDate < 19000101)
      return 1;

   switch (iCnt)
   {
      case 1:
         pAmt    = pOutbuf+OFF_SALE1_AMT;
         pDate   = pOutbuf+OFF_SALE1_DT;
         pDoc    = pOutbuf+OFF_SALE1_DOC;
         pNxtAmt = pOutbuf+OFF_SALE2_AMT;
         pNxtDate= pOutbuf+OFF_SALE2_DT;
         pNxtDoc = pOutbuf+OFF_SALE2_DOC;
         break;
      case 2:
         pAmt    = pOutbuf+OFF_SALE2_AMT;
         pDate   = pOutbuf+OFF_SALE2_DT;
         pDoc    = pOutbuf+OFF_SALE2_DOC;
         pNxtAmt = pOutbuf+OFF_SALE3_AMT;
         pNxtDate= pOutbuf+OFF_SALE3_DT;
         pNxtDoc = pOutbuf+OFF_SALE3_DOC;
         break;
      case 3:
         pAmt    = pOutbuf+OFF_SALE3_AMT;
         pDate   = pOutbuf+OFF_SALE3_DT;
         pDoc    = pOutbuf+OFF_SALE3_DOC;
         pNxtAmt = NULL;
         pNxtDate= NULL;
         pNxtDoc = NULL;
         break;
      default:
         return 1;
   }

   dTax = atofn(pRec->Doctax, SSIZ_DOCTAX);
   if (dTax > 1.0)
   {
      lDttAmt = (long)(dTax*SALE_FACTOR);
   } else
   {
      lDttAmt = atoin(pRec->Doc_Value, SSIZ_DOC_VALUE);
   }
   if (lDttAmt > 0)
      sprintf(acAmt, "%*u", SIZ_SALE1_AMT, lDttAmt);
   else
      memset(acAmt, ' ', SIZ_SALE1_AMT);
   sprintf(acDocNum, "%.*s          ", SSIZ_REC_DOCNO, pRec->Rec_Docno);

   if ((iTmp = memcmp(pDate, pSaleDate, SIZ_SALE1_DT)) > 0)
   {
      if (iCnt == 2 && lDttAmt > 0)
      {
         // Check next sale record
         pAmt  = pOutbuf+OFF_SALE3_AMT;
         pDate = pOutbuf+OFF_SALE3_DT;
         pDoc  = pOutbuf+OFF_SALE3_DOC;
         iTmp = memcmp(pDate, pSaleDate, SIZ_SALE1_DT);
      }
      if (iTmp)
         return iTmp;   // sale rec is older than current
   }

   // If same date, update sale amt and Doc#
   if (!iTmp)
   {
      lTmp = atoin(pAmt, SIZ_SALE1_AMT);
      if (!lTmp && lDttAmt > 0)
      {
         memcpy(pAmt, acAmt, SIZ_SALE1_AMT);
         memcpy(pDoc, acDocNum, SIZ_SALE1_DOC);
         //*(pAmt+SIZ_SALE1_AMT) = 'F';
      } else if (*pDoc == ' ' && acDocNum[0] > ' ')
         memcpy(pDoc, acDocNum, SIZ_SALE1_DOC);
   } else
   {
      // New sale - move current sale to next
      if (pNxtDate && *pDate > '0')
      {
         memcpy(pNxtAmt, pAmt, SIZ_SALE1_AMT);
         *(pNxtAmt+SIZ_SALE1_AMT) = *(pAmt+SIZ_SALE1_AMT);  // Sale code
         memcpy(pNxtDoc, pDoc, SIZ_SALE1_DOC);
         memcpy(pNxtDate, pDate, SIZ_SALE1_DT);
      }

      memcpy(pDate, pSaleDate, SIZ_SALE1_DT);
      memcpy(pAmt, acAmt, SIZ_SALE1_AMT);

      // Sale code - new format no longer carries sale code
      if (lDttAmt > 0)
         *(pAmt+SIZ_SALE1_AMT) = pRec->Full_Part;

      memcpy(pDoc, acDocNum, SIZ_SALE1_DOC);

      // Update transfer on first sale
      if (1 == iCnt)
      {
         memcpy(pOutbuf+OFF_TRANSFER_DT, pSaleDate, SIZ_TRANSFER_DT);
         if (acDocNum[0] > ' ')
            memcpy(pOutbuf+OFF_TRANSFER_DOC, acDocNum, SIZ_SALE1_DOC);
         else
            memset(pOutbuf+OFF_TRANSFER_DOC, ' ', SIZ_SALE1_DOC);

         // Update seller
         memcpy(pOutbuf+OFF_SELLER, pRec->Transferor, SIZ_SELLER);
      }

   }

   return 0;
}

/******************************** Ker_UpdateSale ******************************
 *
 *
 ******************************************************************************/

int Ker_UpdateSale(char *pOutbuf, char *pRollRec)
{
   static   char  acRec[1024], *pRec=NULL;
   int      iRet, iLoop, iSaleCnt=0;
   KER_SALE *pSale;

   // Get first Char rec for first call
   if (!pRec && !lSaleMatch)
   {
      pRec = fgets(acRec, 1024, fdSale);
   }

   pSale = (KER_SALE *)&acRec[0];
   do
   {
      if (!pRec)
      {
         fclose(fdSale);
         fdSale = NULL;
         return 1;      // EOF
      }

      // Compare Apn
      iLoop = memcmp(pOutbuf+OFF_OTHER_APN, pSale->Te_No, RSIZ_TE_NO);
      if (iLoop > 0)
      {
         if (bDebug)
            LogMsg0("Skip Sale rec  %.*s", RSIZ_TE_NO, pSale->Te_No);
         pRec = fgets(acRec, 1024, fdSale);
         lSaleSkip++;
      }
   } while (iLoop > 0);

   // If not match, return
   if (iLoop)
      return 1;

   do
   {
#ifdef _DEBUG
      //if (!memcmp(pSale->Te_No, "        18", 10))
      //   iRet = 0;
#endif

      // Ignore record number starts with RLT
      if (iSaleCnt < 3 && acRec[SOFF_REC_DOCNO] != 'R')
      {
         iSaleCnt++;
         iRet = Ker_MergeSale(pOutbuf, acRec, iSaleCnt);
      }

      // Get next sale record
      pRec = fgets(acRec, 1024, fdSale);
      if (!pRec)
      {
         fclose(fdSale);
         fdSale = NULL;
         break;
      }

   } while (!memcmp(pOutbuf+OFF_OTHER_APN, acRec, RSIZ_TE_NO));

   lSaleMatch++;

   return 0;
}

/******************************** Ker_UpdateLien *****************************
 *
 *
 *****************************************************************************/

int Ker_UpdateLien(char *pOutbuf)
{
   static   char acRec[512], *pRec=NULL;
   char     acTmp[64];
   long     lTmp;
   int      iLoop;

   LIENEXTR *pLien = (LIENEXTR *)&acRec[0];

   // Get first Char rec for first call
   if (!pRec && !lLienMatch)
      pRec = fgets(acRec, 512, fdLien);

   do
   {
      if (!pRec)
      {
         fclose(fdLien);
         fdLien = NULL;
         return 1;      // EOF
      }

      // Compare Apn
      iLoop = memcmp(pOutbuf+OFF_OTHER_APN, pLien->extra.Ker.Te_No, RSIZ_TE_NO);
      if (iLoop > 0)
      {
         if (bDebug)
            LogMsg0("Skip Lien rec  %.10s", pLien->extra.Ker.Te_No);
         pRec = fgets(acRec, 512, fdLien);
         lLienSkip++;
      }
   } while (iLoop > 0);

   // If not match, return
   if (iLoop)
      return 1;

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "003120007", 9))
   //   lTmp = 0;
#endif

   // Land
   long lLand = atoin(pLien->acLand, SIZ_LIEN_FIXT);
   if (lLand > 0)
   {
      sprintf(acTmp, "%*u", SIZ_LAND, lLand);
      memcpy(pOutbuf+OFF_LAND, acTmp, SIZ_LAND);
   }

   // Improve
   long lImpr = atoin(pLien->acImpr, SIZ_LIEN_FIXT);
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*u", SIZ_IMPR, lImpr);
      memcpy(pOutbuf+OFF_IMPR, acTmp, SIZ_IMPR);
   }

   // Other value
   long lPers = atoin(pLien->acPP_Val, SIZ_LIEN_FIXT);
   long lFixt = atoin(pLien->acME_Val, SIZ_LIEN_FIXT);
   long lMnrl = atoin(pLien->extra.Ker.acMineral, SIZ_LIEN_FIXT);
   long lGross = lPers+lFixt+lMnrl;
   if (lGross > 0)
   {
      sprintf(acTmp, "%*u", SIZ_OTHER, lGross);
      memcpy(pOutbuf+OFF_OTHER, acTmp, SIZ_OTHER);

      if (lMnrl > 0)
      {
         sprintf(acTmp, "%d         ", lMnrl);
         memcpy(pOutbuf+OFF_MINERAL, acTmp, SIZ_MINERAL);
      }
      if (lPers > 0)
      {
         sprintf(acTmp, "%d         ", lPers);
         memcpy(pOutbuf+OFF_PERSPROP, acTmp, SIZ_PERSPROP);
      }
      if (lFixt > 0)
      {
         sprintf(acTmp, "%d         ", lFixt);
         memcpy(pOutbuf+OFF_FIXTR, acTmp, SIZ_FIXTR);
      }
   }

   // Gross total
   lGross += lLand+lImpr;
   if (lGross > 0)
   {
      sprintf(acTmp, "%*u", SIZ_GROSS, lGross);
      memcpy(pOutbuf+OFF_GROSS, acTmp, SIZ_GROSS);

      // Ratio
      if (lImpr > 0)
      {
         sprintf(acTmp, "%*u", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
         memcpy(pOutbuf+OFF_RATIO, acTmp, SIZ_RATIO);
      }
   }

   // HO Exempt
   *(pOutbuf+OFF_HO_FL) = pLien->acHO[0];

   lTmp = atoin(pLien->acExAmt, SIZ_LIEN_EXEAMT);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*u", SIZ_EXE_TOTAL, lTmp);
      memcpy(pOutbuf+OFF_EXE_TOTAL, acTmp, SIZ_EXE_TOTAL);
   }
   lLienMatch++;

   // Get next lien rec
   pRec = fgets(acRec, 512, fdLien);
   return 0;
}

/******************************* Ker_UpdateLien2 *****************************
 *
 * Using lien extract sorted by ATN
 *
 *****************************************************************************/

int Ker_UpdateLien2(char *pOutbuf)
{
   static   char acRec[512], *pRec=NULL;
   char     acTmp[64];
   long     lTmp;
   int      iLoop;

   LIENEXTR *pLien = (LIENEXTR *)&acRec[0];

   // Get first Char rec for first call
   if (!pRec && !lLienMatch)
      pRec = fgets(acRec, 512, fdLien);

   do
   {
      if (!pRec)
      {
         fclose(fdLien);
         fdLien = NULL;
         return 1;      // EOF
      }

      // Compare Apn
      iLoop = memcmp(pOutbuf, pLien->acApn, iApnLen);
      if (iLoop > 0)
      {
         if (bDebug)
            LogMsg0("Skip Lien rec  %.12s", pLien->acApn);
         pRec = fgets(acRec, 512, fdLien);
         lLienSkip++;
      }
   } while (iLoop > 0);

   // If not match, return
   if (iLoop)
      return 1;

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "003120007", 9))
   //   lTmp = 0;
#endif

   // TRA
   memcpy(pOutbuf+OFF_TRA, pLien->acTRA, SIZ_TRA);

   // Land
   long lLand = atoin(pLien->acLand, SIZ_LIEN_FIXT);
   if (lLand > 0)
   {
      sprintf(acTmp, "%*u", SIZ_LAND, lLand);
      memcpy(pOutbuf+OFF_LAND, acTmp, SIZ_LAND);
   }

   // Improve
   long lImpr = atoin(pLien->acImpr, SIZ_LIEN_FIXT);
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*u", SIZ_IMPR, lImpr);
      memcpy(pOutbuf+OFF_IMPR, acTmp, SIZ_IMPR);
   }

   // Other value
   long lPers = atoin(pLien->acPP_Val, SIZ_LIEN_FIXT);
   long lFixt = atoin(pLien->acME_Val, SIZ_LIEN_FIXT);
   long lMnrl = atoin(pLien->extra.Ker.acMineral, SIZ_LIEN_FIXT);
   LONGLONG lGross = lPers+lFixt+lMnrl;
   if (lGross > 0)
   {
      sprintf(acTmp, "%*u", SIZ_OTHER, lGross);
      memcpy(pOutbuf+OFF_OTHER, acTmp, SIZ_OTHER);

      if (lMnrl > 0)
      {
         sprintf(acTmp, "%d         ", lMnrl);
         memcpy(pOutbuf+OFF_MINERAL, acTmp, SIZ_MINERAL);
      }
      if (lPers > 0)
      {
         sprintf(acTmp, "%d         ", lPers);
         memcpy(pOutbuf+OFF_PERSPROP, acTmp, SIZ_PERSPROP);
      }
      if (lFixt > 0)
      {
         sprintf(acTmp, "%d         ", lFixt);
         memcpy(pOutbuf+OFF_FIXTR, acTmp, SIZ_FIXTR);
      }
   }

   // Gross total
   lGross += lLand+lImpr;
   if (lGross > 0)
   {
      sprintf(acTmp, "%*u", SIZ_GROSS, lGross);
      memcpy(pOutbuf+OFF_GROSS, acTmp, SIZ_GROSS);

      // Ratio
      if (lImpr > 0)
      {
         sprintf(acTmp, "%*u", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
         memcpy(pOutbuf+OFF_RATIO, acTmp, SIZ_RATIO);
      }
   }

   // HO Exempt
   *(pOutbuf+OFF_HO_FL) = pLien->acHO[0];
   memcpy(pOutbuf+OFF_EXE_CD1, pLien->acExCode, SIZ_LIEN_EXECODE);

   lTmp = atoin(pLien->acExAmt, SIZ_LIEN_EXEAMT);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*u", SIZ_EXE_TOTAL, lTmp);
      memcpy(pOutbuf+OFF_EXE_TOTAL, acTmp, SIZ_EXE_TOTAL);
      // Set full exemption flag
      if (lTmp >= lGross)
        *(pOutbuf+OFF_FULL_EXEMPT) = 'Y';
   }
   lLienMatch++;

   // Get next lien rec
   pRec = fgets(acRec, 512, fdLien);
   return 0;
}

/******************************************************************************/

void Ker_CopyLien(char *pOutbuf, char *pRollRec)
{
   KER_ROLL *pRec;
   char     acTmp[256];
   LONGLONG lTmp;

   pRec = (KER_ROLL *)pRollRec;

   // Land
   long lLand = atoin(pRec->Land, RSIZ_LAND);
   if (lLand > 0)
   {
      sprintf(acTmp, "%*u", SIZ_LAND, lLand);
      memcpy(pOutbuf+OFF_LAND, acTmp, SIZ_LAND);
   }

   // Improve
   long lImpr = atoin(pRec->Impr, RSIZ_LAND);
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*u", SIZ_IMPR, lImpr);
      memcpy(pOutbuf+OFF_IMPR, acTmp, SIZ_IMPR);
   }

   // Other value
   long lPers = atoin(pRec->PP_Val, RSIZ_LAND);
   long lFixt = atoin(pRec->Fixt, RSIZ_LAND);
   long lMnrl = atoin(pRec->Mineral, RSIZ_LAND);
   LONGLONG lGross = lPers+lFixt+lMnrl;
   if (lGross > 0)
   {
      sprintf(acTmp, "%*u", SIZ_OTHER, lGross);
      memcpy(pOutbuf+OFF_OTHER, acTmp, SIZ_OTHER);

      if (lMnrl > 0)
      {
         sprintf(acTmp, "%d         ", lMnrl);
         memcpy(pOutbuf+OFF_MINERAL, acTmp, SIZ_MINERAL);
      }
      if (lPers > 0)
      {
         sprintf(acTmp, "%d         ", lPers);
         memcpy(pOutbuf+OFF_PERSPROP, acTmp, SIZ_PERSPROP);
      }
      if (lFixt > 0)
      {
         sprintf(acTmp, "%d         ", lFixt);
         memcpy(pOutbuf+OFF_FIXTR, acTmp, SIZ_FIXTR);
      }
   }

   // Gross total
   lGross += lLand+lImpr;
   if (lGross > 0)
   {
      sprintf(acTmp, "%*u", SIZ_GROSS, lGross);
      memcpy(pOutbuf+OFF_GROSS, acTmp, SIZ_GROSS);

      // Ratio
      if (lImpr > 0)
      {
         sprintf(acTmp, "%*u", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
         memcpy(pOutbuf+OFF_RATIO, acTmp, SIZ_RATIO);
      }
   }

   // HO Exempt
   if (pRec->Exe_Code1[0] == 'H' || pRec->Exe_Code2[0] == 'H')
   {
      *(pOutbuf+OFF_HO_FL) = '1';      // 'Y'
   } else
      *(pOutbuf+OFF_HO_FL) = '2';      // 'N'

   if (pRec->Exe_Code1[0] > ' ')
   {
      lTmp = atoin(pRec->Exe_Amt1, RSIZ_EXMPT_VAL);
      lTmp += atoin(pRec->Exe_Amt2, RSIZ_EXMPT_VAL);
      // Exempt cannot be greater than total value
      if (lTmp > lGross)
      {
         lTmp = lGross;
         *(pOutbuf+OFF_FULL_EXEMPT) = 'Y';
      }
      sprintf(acTmp, "%*u", SIZ_EXE_TOTAL, lTmp);
      memcpy(pOutbuf+OFF_EXE_TOTAL, acTmp, SIZ_EXE_TOTAL);
   }
}

/******************************************************************************/

int Ker_CreateLienRec(char *pOutbuf, char *pRollRec)
{
   char     acTmp[256];
   long     lTmp, iTmp;
   KER_LIEN *pRec;
   LIENEXTR *pLienRec;

   pRec     = (KER_LIEN *)pRollRec;
   pLienRec = (LIENEXTR *)pOutbuf;

   // Clear output buffer
   memset(pOutbuf, ' ', sizeof(LIENEXTR));

   // Start copying data
   memcpy(pLienRec->acApn, pRec->Atn, LSIZ_ATN);

   lTmp = atoin(pRec->Te_No, LSIZ_TE_NO);
   sprintf(acTmp, "%*u", LSIZ_TE_NO, lTmp);
   memcpy(pLienRec->extra.Ker.Te_No, acTmp, LSIZ_TE_NO);

   // TRA
   lTmp = atoin(pRec->TRA, LSIZ_TRA_NO);
   if (lTmp > 0)
   {
      iTmp = sprintf(acTmp, "%.6d", lTmp);
      memcpy(pLienRec->acTRA, acTmp, iTmp);
   }

   // Year assessed
   memcpy(pLienRec->acYear, myCounty.acYearAssd, 4);

   // Land
   long lLand = atoin(pRec->Land, LSIZ_LAND_VALUE);
   if (lLand > 0)
   {
      sprintf(acTmp, "%*u", SIZ_LIEN_LAND, lLand);
      memcpy(pLienRec->acLand, acTmp, SIZ_LIEN_LAND);
   }

   // Improve
   long lImpr = atoin(pRec->Impr, LSIZ_LAND_VALUE);
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*u", SIZ_LIEN_IMPR, lImpr);
      memcpy(pLienRec->acImpr, acTmp, SIZ_LIEN_IMPR);
   }

   // Other value
   long lPers = atoin(pRec->PP_Val, LSIZ_LAND_VALUE);
   long lFixt = atoin(pRec->Fixt, LSIZ_LAND_VALUE);
   long lMnrl = atoin(pRec->Mineral, LSIZ_LAND_VALUE);
   lTmp = lPers+lFixt+lMnrl;
   if (lTmp > 0)
   {
      iTmp = sprintf(acTmp, "%*u", sizeof(pLienRec->acOther), lTmp);
      memcpy(pLienRec->acOther, acTmp, iTmp);

      if (lPers > 0)
      {
         iTmp = sprintf(acTmp, "%*u", sizeof(pLienRec->acPP_Val), lPers);
         memcpy(pLienRec->acPP_Val, acTmp, iTmp);
      }
      if (lFixt > 0)
      {
         iTmp = sprintf(acTmp, "%*u", sizeof(pLienRec->acME_Val), lFixt);
         memcpy(pLienRec->acME_Val, acTmp, iTmp);
      }
      if (lMnrl > 0)
      {
         iTmp = sprintf(acTmp, "%*u", sizeof(pLienRec->extra.Ker.acMineral), lMnrl);
         memcpy(pLienRec->extra.Ker.acMineral, acTmp, iTmp);
      }
   }

   // Gross total
   lTmp += lLand+lImpr;
   if (lTmp > 0)
   {
      iTmp = sprintf(acTmp, "%*u", sizeof(pLienRec->acGross), lTmp);
      memcpy(pLienRec->acGross, acTmp, iTmp);

      // Ratio
      if (lImpr > 0)
      {
         iTmp = sprintf(acTmp, "%*u", sizeof(pLienRec->acRatio), (LONGLONG)lImpr*100/(lLand+lImpr));
         memcpy(pLienRec->acRatio, acTmp, iTmp);
      }
   }

   // HO Exempt
   if (pRec->Exmpt_Type_Code[0] == 'H')
      pLienRec->acHO[0] = '1';      // 'Y'
   else
      pLienRec->acHO[0] = '2';      // 'N'

   if (pRec->Exmpt_Type_Code[0] > ' ')
   {
      memcpy(pLienRec->extra.Ker.Exe_Code1, pRec->Exmpt_Type_Code, LSIZ_EXMPT_TYPE_CODE);
      memcpy(pLienRec->acExCode, pRec->Exmpt_Type_Code, LSIZ_EXMPT_TYPE_CODE);
      lTmp = atoin(pRec->Exe_Amt, LSIZ_EXE_VALUE);
      iTmp = sprintf(acTmp, "%*u", sizeof(pLienRec->acExAmt), lTmp);
      memcpy(pLienRec->acExAmt, acTmp, iTmp);
   }

   pLienRec->LF[0] = 10;
   pLienRec->LF[1] = 0;
   return 0;
}

/********************************* Ker_ExtrLien *****************************
 *
 * Extract lien from fix-length record
 *
 ****************************************************************************/

int Ker_ExtrLien(char *pLienFile)
{
   char     *pTmp, acBuf[MAX_RECSIZE], acRollRec[MAX_RECSIZE], acOutFile[_MAX_PATH];
   FILE     *fdLien;
   int      iRet;
   long     lCnt, lSkipRec, lWritten;
   KER_LIEN *pRec;

   // Open roll file
   LogMsg("Open lien file %s", pLienFile);
   fdRoll = fopen(pLienFile, "r");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", pLienFile);
      return -1;
   }

   // Open Output file
   sprintf(acOutFile, acLienTmpl, myCounty.acCntyCode, myCounty.acCntyCode);
   LogMsg("Create lien extract file %s", acOutFile);
   fdLien = fopen(acOutFile, "w");
   if (fdLien == NULL)
   {
      LogMsg("***** Error creating lien extract file: %s\n", acOutFile);
      return -2;
   }

   // Merge loop   
   lCnt=lSkipRec=lWritten = 0;
   pRec = (KER_LIEN *)&acRollRec[0];
   while (!feof(fdRoll))
   {
      // Get roll rec
      pTmp = fgets(acRollRec, MAX_RECSIZE, fdRoll);

      if (!pTmp)
         break;

      // Skip parcel without ATN
      if (pRec->Atn[0] > ' ')
      {
         replNull(acRollRec, ' ', 0);

         // Create new R01 record
         iRet = Ker_CreateLienRec(acBuf, acRollRec);

         // Write to output
         fputs(acBuf, fdLien);
         lWritten++;
      } else
         lSkipRec++;

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdRoll)
      fclose(fdRoll);
   if (fdLien)
      fclose(fdLien);

   LogMsg("Total output records:       %u", lCnt);

   return 0;
}

/**************************** Ker_CreateLienRecCsv **************************
 *
 * Extract lien from csv record - 2020
 *
 ****************************************************************************/

//int Ker_CreateLienRecCsv(char *pOutbuf, char *pRollRec)
//{
//   char     acTmp[256];
//   LONGLONG lTmp;
//   int      iTmp;
//   LIENEXTR *pLienRec;
//
//   pLienRec = (LIENEXTR *)pOutbuf;
//
//   // Parse input rec
//   iTokens = ParseStringIQ(pRollRec, cLdrSep, MAX_FLD_TOKEN, apTokens);
//   if (iTokens < KER_L_PRIOR_LD_VALUE)
//   {
//      LogMsg("***** Error: bad input record for APN=%s (%d)", apTokens[KER_L_ATN], iTokens);
//      return -1;
//   }
//
//   // Clear output buffer
//   memset(pOutbuf, ' ', sizeof(LIENEXTR));
//
//   // Start copying data
//   vmemcpy(pLienRec->acApn, apTokens[KER_L_ATN], LSIZ_ATN);
//
//   lTmp = atol(apTokens[KER_L_TENO]);
//   sprintf(acTmp, "%*u", LSIZ_TE_NO, lTmp);
//   memcpy(pLienRec->extra.Ker.Te_No, acTmp, LSIZ_TE_NO);
//
//   // TRA
//   lTmp = atol(apTokens[KER_L_TRA]);
//   if (lTmp > 0)
//   {
//      iTmp = sprintf(acTmp, "%.6d", lTmp);
//      memcpy(pLienRec->acTRA, acTmp, iTmp);
//   }
//
//   // Year assessed
//   memcpy(pLienRec->acYear, myCounty.acYearAssd, 4);
//
//   // Land
//   long lLand = atol(apTokens[KER_L_LAND]);
//   if (lLand > 0)
//   {
//      sprintf(acTmp, "%*u", SIZ_LIEN_LAND, lLand);
//      memcpy(pLienRec->acLand, acTmp, SIZ_LIEN_LAND);
//   }
//
//   // Improve
//   long lImpr = atol(apTokens[KER_L_IMPR]);
//   if (lImpr > 0)
//   {
//      sprintf(acTmp, "%*u", SIZ_LIEN_IMPR, lImpr);
//      memcpy(pLienRec->acImpr, acTmp, SIZ_LIEN_IMPR);
//   }
//
//   // Other value
//   long lPers = atol(apTokens[KER_L_PERSPROP]);
//   long lFixt = atol(apTokens[KER_L_OTHER_IMP]);
//   long lMnrl = atol(apTokens[KER_L_MINERAL]);
//   lTmp = lPers+lFixt+lMnrl;
//   if (lTmp > 0)
//   {
//      iTmp = sprintf(acTmp, "%*u", sizeof(pLienRec->acOther), lTmp);
//      memcpy(pLienRec->acOther, acTmp, iTmp);
//
//      if (lPers > 0)
//      {
//         iTmp = sprintf(acTmp, "%*u", sizeof(pLienRec->acPP_Val), lPers);
//         memcpy(pLienRec->acPP_Val, acTmp, iTmp);
//      }
//      if (lFixt > 0)
//      {
//         iTmp = sprintf(acTmp, "%*u", sizeof(pLienRec->acME_Val), lFixt);
//         memcpy(pLienRec->acME_Val, acTmp, iTmp);
//      }
//      if (lMnrl > 0)
//      {
//         iTmp = sprintf(acTmp, "%*u", sizeof(pLienRec->extra.Ker.acMineral), lMnrl);
//         memcpy(pLienRec->extra.Ker.acMineral, acTmp, iTmp);
//      }
//   }
//
//   // Gross total
//   lTmp += lLand+lImpr;
//   if (lTmp > 0)
//   {
//      iTmp = sprintf(acTmp, "%*u", sizeof(pLienRec->acGross), lTmp);
//      memcpy(pLienRec->acGross, acTmp, iTmp);
//
//      // Ratio
//      if (lImpr > 0)
//      {
//         iTmp = sprintf(acTmp, "%*u", sizeof(pLienRec->acRatio), (LONGLONG)lImpr*100/(lLand+lImpr));
//         memcpy(pLienRec->acRatio, acTmp, iTmp);
//      }
//   }
//
//   // HO Exempt
//   if (*apTokens[KER_L_EX] == 'H')
//      pLienRec->acHO[0] = '1';      // 'Y'
//   else
//      pLienRec->acHO[0] = '2';      // 'N'
//
//   if (*apTokens[KER_L_EX] > ' ')
//   {
//      vmemcpy(pLienRec->extra.Ker.Exe_Code1, apTokens[KER_L_EX], SIZ_EXE_CD);
//      vmemcpy(pLienRec->acExCode, apTokens[KER_L_EX], SIZ_EXE_CD);
//      lTmp = atol(apTokens[KER_L_EXEMPTIONS]);
//      iTmp = sprintf(acTmp, "%*u", sizeof(pLienRec->acExAmt), lTmp);
//      memcpy(pLienRec->acExAmt, acTmp, iTmp);
//   }
//
//   pLienRec->LF[0] = 10;
//   pLienRec->LF[1] = 0;
//   return 0;
//}

/**************************** Ker_CreateLienRecCsv1 *************************
 *
 * Extract lien from csv record - 2023
 *
 ****************************************************************************/

int Ker_CreateLienRecCsv1(char *pOutbuf, char *pRollRec)
{
   char     acTmp[256];
   LONGLONG lTmp;
   int      iTmp;
   LIENEXTR *pLienRec;

   pLienRec = (LIENEXTR *)pOutbuf;

   // Parse input rec
   iTokens = ParseStringIQ(pRollRec, cLdrSep, MAX_FLD_TOKEN, apTokens);
   if (iTokens < KER_L_FLDS)
   {
      LogMsg("***** Error: bad input record for APN=%s (%d)", apTokens[KER_L_ATN], iTokens);
      return -1;
   }

   // Clear output buffer
   memset(pOutbuf, ' ', sizeof(LIENEXTR));

   // Start copying data
   vmemcpy(pLienRec->acApn, apTokens[KER_L_ATN], LSIZ_ATN);

   lTmp = atol(apTokens[KER_L_TENO]);
   sprintf(acTmp, "%*u", LSIZ_TE_NO, lTmp);
   memcpy(pLienRec->extra.Ker.Te_No, acTmp, LSIZ_TE_NO);

   // TRA
   lTmp = atol(apTokens[KER_L_TRA]);
   if (lTmp > 0)
   {
      iTmp = sprintf(acTmp, "%.6d", lTmp);
      memcpy(pLienRec->acTRA, acTmp, iTmp);
   }

   // Year assessed
   memcpy(pLienRec->acYear, myCounty.acYearAssd, 4);

   // Land
   long lLand = atol(apTokens[KER_L_LAND]);
   if (lLand > 0)
   {
      sprintf(acTmp, "%*u", SIZ_LIEN_LAND, lLand);
      memcpy(pLienRec->acLand, acTmp, SIZ_LIEN_LAND);
   }

   // Improve
   long lImpr = atol(apTokens[KER_L_IMPR]);
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*u", SIZ_LIEN_IMPR, lImpr);
      memcpy(pLienRec->acImpr, acTmp, SIZ_LIEN_IMPR);
   }

   // Other value
   long lPers = atol(apTokens[KER_L_PERSPROP]);
   long lFixt = atol(apTokens[KER_L_OTHER_IMP]);
   long lMnrl = atol(apTokens[KER_L_MINERAL]);
   lTmp = lPers+lFixt+lMnrl;
   if (lTmp > 0)
   {
      iTmp = sprintf(acTmp, "%*u", sizeof(pLienRec->acOther), lTmp);
      memcpy(pLienRec->acOther, acTmp, iTmp);

      if (lPers > 0)
      {
         iTmp = sprintf(acTmp, "%*u", sizeof(pLienRec->acPP_Val), lPers);
         memcpy(pLienRec->acPP_Val, acTmp, iTmp);
      }
      if (lFixt > 0)
      {
         iTmp = sprintf(acTmp, "%*u", sizeof(pLienRec->acME_Val), lFixt);
         memcpy(pLienRec->acME_Val, acTmp, iTmp);
      }
      if (lMnrl > 0)
      {
         iTmp = sprintf(acTmp, "%*u", sizeof(pLienRec->extra.Ker.acMineral), lMnrl);
         memcpy(pLienRec->extra.Ker.acMineral, acTmp, iTmp);
      }
   }

   // Gross total
   lTmp += lLand+lImpr;
   if (lTmp > 0)
   {
      iTmp = sprintf(acTmp, "%*u", sizeof(pLienRec->acGross), lTmp);
      memcpy(pLienRec->acGross, acTmp, iTmp);

      // Ratio
      if (lImpr > 0)
      {
         iTmp = sprintf(acTmp, "%*u", sizeof(pLienRec->acRatio), (LONGLONG)lImpr*100/(lLand+lImpr));
         memcpy(pLienRec->acRatio, acTmp, iTmp);
      }
   }

   // HO Exempt
   if (*apTokens[KER_L_EXECODE] == 'H')
      pLienRec->acHO[0] = '1';      // 'Y'
   else
      pLienRec->acHO[0] = '2';      // 'N'

   if (*apTokens[KER_L_EXECODE] > ' ')
   {
      vmemcpy(pLienRec->extra.Ker.Exe_Code1, apTokens[KER_L_EXECODE], SIZ_EXE_CD);
      vmemcpy(pLienRec->acExCode, apTokens[KER_L_EXECODE], SIZ_EXE_CD);
      lTmp = atol(apTokens[KER_L_EXEAMT]);
      iTmp = sprintf(acTmp, "%*u", sizeof(pLienRec->acExAmt), lTmp);
      memcpy(pLienRec->acExAmt, acTmp, iTmp);
   }

   pLienRec->LF[0] = 10;
   pLienRec->LF[1] = 0;
   return 0;
}

int Ker_ExtrLienCsv(char *pLienFile)
{
   char     *pTmp, acBuf[MAX_RECSIZE], acRollRec[MAX_RECSIZE], acOutFile[_MAX_PATH];
   FILE     *fdLien;
   int      iRet;
   long     lCnt, lSkipRec, lWritten;

   // Open roll file
   LogMsg("Open lien file %s", pLienFile);
   fdRoll = fopen(pLienFile, "r");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", pLienFile);
      return -1;
   }

   // Open Output file
   sprintf(acOutFile, acLienTmpl, myCounty.acCntyCode, myCounty.acCntyCode);
   LogMsg("Create lien extract file %s", acOutFile);
   fdLien = fopen(acOutFile, "w");
   if (fdLien == NULL)
   {
      LogMsg("***** Error creating lien extract file: %s\n", acOutFile);
      return -2;
   }

   // Merge loop   
   lCnt=lSkipRec=lWritten = 0;
   while (!feof(fdRoll))
   {
      // Get roll rec
      pTmp = fgets(acRollRec, MAX_RECSIZE, fdRoll);

      if (!pTmp)
         break;

      replNull(acRollRec, ' ', 0);

      // Create new R01 record
      iRet = Ker_CreateLienRecCsv1(acBuf, acRollRec);
      if (!iRet)
      {
         // Write to output
         fputs(acBuf, fdLien);
         lWritten++;
      } else
         lSkipRec++;

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdRoll)
      fclose(fdRoll);
   if (fdLien)
      fclose(fdLien);

   LogMsg("Total records processed:  %u", lCnt);
   LogMsg("         output records:  %u", lWritten);

   return 0;
}

/********************************* Ker_ExtrRBPR *****************************
 *
 * Extract lien values from RBPR file.
 *
 ****************************************************************************/

int Ker_CreateRBPRRec(char *pOutbuf, char *pRollRec)
{
   char     acTmp[256];
   long     lTmp, iTmp;
   KER_ROLL *pRec;
   LIENEXTR *pLienRec;

   pRec     = (KER_ROLL *)pRollRec;
   pLienRec = (LIENEXTR *)pOutbuf;

   // Clear output buffer
   memset(pOutbuf, ' ', sizeof(LIENEXTR));

   // Start copying data
   memcpy(pLienRec->acApn, pRec->Atn, LSIZ_ATN);

   lTmp = atoin(pRec->Te_No, LSIZ_TE_NO);
   sprintf(acTmp, "%*u", LSIZ_TE_NO, lTmp);
   memcpy(pLienRec->extra.Ker.Te_No, acTmp, LSIZ_TE_NO);

   // TRA
   lTmp = atoin(pRec->TRA, LSIZ_TRA_NO);
   if (lTmp > 0)
   {
      iTmp = sprintf(acTmp, "%.6d", lTmp);
      memcpy(pLienRec->acTRA, acTmp, iTmp);
   }

   // Year assessed
   memcpy(pLienRec->acYear, myCounty.acYearAssd, 4);

   // Land
   long lLand = atoin(pRec->Land, RSIZ_LAND);
   if (lLand > 0)
   {
      sprintf(acTmp, "%*u", SIZ_LIEN_LAND, lLand);
      memcpy(pLienRec->acLand, acTmp, SIZ_LIEN_LAND);
   }

   // Improve
   long lImpr = atoin(pRec->Impr, RSIZ_LAND);
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*u", SIZ_LIEN_IMPR, lImpr);
      memcpy(pLienRec->acImpr, acTmp, SIZ_LIEN_IMPR);
   }

   // Other value
   long lPers = atoin(pRec->PP_Val, RSIZ_LAND);
   long lFixt = atoin(pRec->Fixt, RSIZ_LAND);
   long lMnrl = atoin(pRec->Mineral, RSIZ_LAND);
   lTmp = lPers+lFixt+lMnrl;
   if (lTmp > 0)
   {
      iTmp = sprintf(acTmp, "%*u", sizeof(pLienRec->acOther), lTmp);
      memcpy(pLienRec->acOther, acTmp, iTmp);

      if (lPers > 0)
      {
         iTmp = sprintf(acTmp, "%*u", sizeof(pLienRec->acPP_Val), lPers);
         memcpy(pLienRec->acPP_Val, acTmp, iTmp);
      }
      if (lFixt > 0)
      {
         iTmp = sprintf(acTmp, "%*u", sizeof(pLienRec->acME_Val), lFixt);
         memcpy(pLienRec->acME_Val, acTmp, iTmp);
      }
      if (lMnrl > 0)
      {
         iTmp = sprintf(acTmp, "%*u", sizeof(pLienRec->extra.Ker.acMineral), lMnrl);
         memcpy(pLienRec->extra.Ker.acMineral, acTmp, iTmp);
      }
   }

   // Gross total
   lTmp += lLand+lImpr;
   if (lTmp > 0)
   {
      iTmp = sprintf(acTmp, "%*u", sizeof(pLienRec->acGross), lTmp);
      memcpy(pLienRec->acGross, acTmp, iTmp);

      // Ratio
      if (lImpr > 0)
      {
         iTmp = sprintf(acTmp, "%*u", sizeof(pLienRec->acRatio), (LONGLONG)lImpr*100/(lLand+lImpr));
         memcpy(pLienRec->acRatio, acTmp, iTmp);
      }
   }

   // HO Exempt
   if (pRec->Exe_Code1[0] == 'H')
      pLienRec->acHO[0] = '1';      // 'Y'
   else
      pLienRec->acHO[0] = '2';      // 'N'

   if (pRec->Exe_Code1[0] > ' ')
   {
      memcpy(pLienRec->acExCode, pRec->Exe_Code1, RSIZ_EXMPT_TYPE);
      lTmp = atoin(pRec->Exe_Amt, RSIZ_EXMPT_VAL);
      iTmp = sprintf(acTmp, "%*u", sizeof(pLienRec->acExAmt), lTmp);
      memcpy(pLienRec->acExAmt, acTmp, iTmp);
   }

   pLienRec->LF[0] = 10;
   pLienRec->LF[1] = 0;
   return 0;
}

int Ker_ExtrRBPR(char *pLienFile)
{
   char     *pTmp, acBuf[MAX_RECSIZE], acRollRec[MAX_RECSIZE];
   char     acOutFile[_MAX_PATH], acTenoFile[_MAX_PATH];
   FILE     *fdLien;
   int      iRet;
   long     lCnt, lSkipRec, lWritten;
   KER_ROLL *pRec;

   // Open roll file
   LogMsg("Open lien file %s", pLienFile);
   fdRoll = fopen(pLienFile, "r");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", pLienFile);
      return -1;
   }

   // Open Output file
   GetIniString(sCnty, "LienExtr", "", acTenoFile, _MAX_PATH, acIniFile);
   LogMsg("Open lien extract file %s", acTenoFile);
   fdLien = fopen(acTenoFile, "w");
   if (fdLien == NULL)
   {
      LogMsg("***** Error creating lien extract file: %s\n", acTenoFile);
      return -2;
   }

   // Merge loop   
   lCnt=lSkipRec=lWritten = 0;
   pRec = (KER_ROLL *)&acRollRec[0];
   while (!feof(fdRoll))
   {
      // Get roll rec
      pTmp = fgets(acRollRec, MAX_RECSIZE, fdRoll);

      if (!pTmp)
         break;

      // Skip parcel without ATN
      if (pRec->Atn[0] > ' ')
      {
         replNull(acRollRec, ' ', 0);

         // Create new R01 record
         iRet = Ker_CreateRBPRRec(acBuf, acRollRec);

         // Write to output
         fputs(acBuf, fdLien);
         lWritten++;
      } else
         lSkipRec++;

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdRoll)
      fclose(fdRoll);
   if (fdLien)
      fclose(fdLien);

   // Sort output file on ATN order
   sprintf(acOutFile, acLienTmpl, myCounty.acCntyCode, myCounty.acCntyCode);
   sprintf(acRollRec, "S(1,%d,C,A) F(TXT)", LSIZ_ATN);
   lRecCnt = sortFile(acTenoFile, acOutFile, acRollRec);

   LogMsgD("\nTotal output records:       %u", lCnt);

   return 0;
}

/********************************* Ker_MergeRoll *****************************
 *
 * Return 0 if successful, >0 if error
 *
 *****************************************************************************/

void Ker_MergeRoll(char *pOutbuf, char *pRollRec, int iLen, int iFlag)
{
   KER_ROLL *pRec;
   char     acTmp[256], acTmp1[256];
   long     iRet;

   pRec = (KER_ROLL *)pRollRec;
   // Clear output buffer
   memset(pOutbuf, ' ', iRecLen);

   // Use ATN for APN
   memcpy(pOutbuf, pRec->Atn, RSIZ_ATN);
   memcpy(pOutbuf+OFF_CO_NUM, "15KER", 5);
   *(pOutbuf+OFF_STATUS) = pRec->ParcelStatus[0];

   // Save APN to ALT_APN
   memcpy(pOutbuf+OFF_ALT_APN, pRec->AltApn, RSIZ_APN);

   // Save TE_NO to Other APN
   iRet = atoin(pRec->Te_No, RSIZ_TE_NO);
   sprintf(acTmp, "%*u", RSIZ_TE_NO, iRet);
   memcpy(pOutbuf+OFF_OTHER_APN, acTmp, RSIZ_TE_NO);

   // Format APN
   iRet = formatApn(pRec->Atn, acTmp, &myCounty);
   memcpy(pOutbuf+OFF_APN_D, acTmp, iRet);

   // Create MapLink
   iRet = formatMapLink(pRec->Atn, acTmp, &myCounty);
   memcpy(pOutbuf+OFF_MAPLINK, acTmp, iRet);

   // Create index map link
   if (getIndexPage(acTmp, acTmp1, &myCounty))
      memcpy(pOutbuf+OFF_IMAPLINK, acTmp1, iRet);

   // Year assessed
   memcpy(pOutbuf+OFF_YR_ASSD, myCounty.acYearAssd, 4);

   if (iFlag & CREATE_LIEN)
   {
      Ker_CopyLien(pOutbuf, pRollRec);
   } else
   {
      if (fdLien)
      {
         iRet = Ker_UpdateLien(pOutbuf);
         if (iRet)
         {
            Ker_CopyLien(pOutbuf, pRollRec);
            if (bDebug)
               LogMsg("** New parcel: %.*s", RSIZ_ATN, pRollRec);
         }
      }
   }

   // UseCode
   //memcpy(pOutbuf+OFF_USE_CO, pRec->UseCode, RSIZ_USE_CODE);
   if (pRec->UseCode[0] > ' ')
   {
      memset(pOutbuf+OFF_USE_STD, ' ', SIZ_USE_STD);
      memcpy(pOutbuf+OFF_USE_CO, pRec->UseCode, RSIZ_USE_CODE);
      updateStdUse(pOutbuf+OFF_USE_STD, pRec->UseCode, RSIZ_USE_CODE);
   } else
      memcpy(pOutbuf+OFF_USE_STD, USE_UNASGN, SIZ_USE_STD);

   // TRA
   memcpy(pOutbuf+OFF_TRA, pRec->TRA, RSIZ_TRA);

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "37717027006", 11))
   //   iRet = 1;
#endif

   // Owner
   try {
      Ker_MergeOwner(pOutbuf, pRollRec);
   } catch (...)
   {
      iRet = 1;
   }

   // Mailing
   if (pRec->MAdr_Flg == 'Y')
   {
      // Mailing address is free form 
      //Ker_MergeMailing(pOutbuf, pRollRec);
      if (bDebug)
         LogMsg("*** Skip madr: %.9s -> %.60s", pRec->Atn, pRec->M_StrNum);
   } else
      Ker_MergeMAdr(pOutbuf, pRollRec);

   // AgPreserve
   if (pRec->AgPreserve == 'Y')
      *(pOutbuf+OFF_AG_PRE) = pRec->AgPreserve;
   else
      *(pOutbuf+OFF_AG_PRE) = ' ';

   if (pRec->RollType[0] == '5')
   {
      *(pOutbuf+OFF_FULL_EXEMPT) = 'Y';
      *(pOutbuf+OFF_PUBL_FLG) = 'Y';
   }
}

/********************************** Ker_Load_Roll *****************************
 *
 * All input files are in TE_NO order.  But output KER.R01 need to be sorted in ATN order.
 *
 ******************************************************************************/

int Ker_Load_Roll(int iFirstRec)
{
   char     *pTmp, acBuf[MAX_RECSIZE], acRollRec[MAX_RECSIZE];
   char     acOutFile[_MAX_PATH], acTmpFile[_MAX_PATH], acLienFile[_MAX_PATH];

   HANDLE   fhOut;

   DWORD    nBytesWritten;
   BOOL     bRet;
   long     lRet=0, lCnt=0;
   int      iRet, iRollUpd=0;

   LogMsg("Load %s update roll", myCounty.acCntyCode);

   fdSitus=fdLegal=fdLien= NULL;
   sprintf(acOutFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "R01");
   sprintf(acTmpFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "S01");
   if (_access(acTmpFile, 0))
   {
      if (!_access(acOutFile, 0))
         rename(acOutFile, acTmpFile);
   }

   // Open Roll file
   LogMsg("Open Roll file %s", acRollFile);
   fdRoll = fopen(acRollFile, "r");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", acRollFile);
      return 2;
   }

   // Open Lien file
   GetIniString(sCnty, "LienExtr", "", acLienFile, _MAX_PATH, acIniFile);
   LogMsg("Open Lien file %s", acLienFile);
   fdLien = fopen(acLienFile, "r");
   if (fdLien == NULL)
   {
      LogMsg("***** Error opening lien file: %s\n", acLienFile);
      return 2;
   }

   // Open situs addr file
   if (!_access(acSAdrFile, 0))
   {
      LogMsg("Open Situs file %s", acSAdrFile);
      fdSitus = fopen(acSAdrFile, "r");
      if (fdSitus == NULL)
      {
         LogMsg("***** Error opening Situs file: %s\n", acSAdrFile);
         return 2;
      }
   }

   // Open legal file
   if (!_access(acLegalFile, 0))
   {
      LogMsg("Open Legal file %s", acLegalFile);
      fdLegal = fopen(acLegalFile, "r");
      if (fdLegal == NULL)
      {
         LogMsg("***** Error opening Legal file: %s\n", acLegalFile);
         return 2;
      }
   }

   // Open Output file
   sprintf(acTmpFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "UNS");
   LogMsg("Open output file %s", acTmpFile);
   fhOut = CreateFile(acTmpFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error creating output file %s\n", acTmpFile);
      return 4;
   }

   // Create header record
   if (iFirstRec)
   {
      memset(acBuf, '9', iRecLen);
      // Ignore record start with 99999999
      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
   }

   iNoMatch=iBadCity=iBadSuffix=0;

   // Get first RollRec
   pTmp = fgets((char *)&acRollRec[0], MAX_RECSIZE, fdRoll);
   memset(acBuf, ' ', iRecLen);

   // Merge loop
   while (pTmp)
   {
#ifdef _DEBUG
      //if (!memcmp(&acRollRec[ROFF_TE_NO], "        18", 10))
      //   iRet = 1;
#endif

      // Create new R01 record
      Ker_MergeRoll(acBuf, acRollRec, iRollLen, CREATE_R01);

      if (fdSitus)
         iRet = Ker_MergeSitus(acBuf, acRollRec);

      if (fdLegal)
         iRet = Ker_MergeLegal(acBuf, acRollRec);

      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
      if (!bRet)
      {
         LogMsg("***** Error occurs: %d\n", GetLastError());
         lRet = WRITE_ERR;
         break;
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);

      // Get next roll record
      do {
         // Ignore record without APN
         pTmp = fgets(acRollRec, MAX_RECSIZE, fdRoll);
      } while (pTmp && acRollRec[0] == ' ');
   }

   // Close files
   if (fdRoll)
      fclose(fdRoll);
   if (fdSitus)
      fclose(fdSitus);
   if (fdLegal)
      fclose(fdLegal);
   if (fdLien)
      fclose(fdLien);

   if (fhOut)
      CloseHandle(fhOut);

   // Sort output file on APN
   sprintf(acBuf, "S(1,%d,C,A) F(FIX,%d) B(%d,R)", iApnLen, iRecLen, iRecLen);
   lRet = sortFile(acTmpFile, acOutFile, acBuf);

   LogMsgD("\nTotal output records:       %u\n", lCnt);

   LogMsg("Total Lien matched:         %u", lLienMatch);
   LogMsg("Total Lien skipped:         %u\n", lLienSkip);

   LogMsg("Total Situs matched:        %u", lSitusMatch);
   LogMsg("Total Situs skipped:        %u\n", lSitusSkip);

   LogMsg("Total Legal matched:        %u", lLegalMatch);
   LogMsg("Total Legal skipped:        %u\n", lLegalSkip);

   LogMsg("Total bad-city records:     %u", iBadCity);
   LogMsg("Total bad-suffix records:   %u\n", iBadSuffix);

   lRecCnt = lRet;
   return 0;
}

/******************************* Ker_MergeValue2 *****************************
 *
 * Notes:
 *
 * Return 0 if successful, otherwise error
 *
 *****************************************************************************/

int Ker_MergeValue2(char *pOutbuf)
{
   static   char acRec[1024], *pRec=NULL;
   char     *pTmp, acBuf[256], acTmp[256], *apFlds[VALUE_BILLSUBTYPECODE+2];
   int      iRet, iLoop;

   // Get first rec for first call
   if (!pRec)
   {
      // Drop header
      pRec = fgets(acRec, 256, fdValue);
      pRec = fgets(acRec, 256, fdValue);

      // Get first rec
      pRec = fgets(acRec, 256, fdValue);
   }

   pTmp = pRec;
   do
   {
      if (!pRec)
      {
         fclose(fdValue);
         fdValue = NULL;
         return 1;      // EOF
      }

      // Compare Apn
      iLoop = memcmp(pOutbuf, pRec, iApnLen);
      if (iLoop > 0)
      {
         if (bDebug)
            LogMsg0("Skip value rec  %.*s", iApnLen, pRec);
         pRec = fgets(acRec, 256, fdValue);
         lValueSkip++;
      }
   } while (iLoop > 0);

   // If not match, return
   if (iLoop)
      return 1;

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "00102001015", 11))
   //   lTmp = 0;
#endif
   // Parse input rec
   strcpy(acBuf, acRec);
   iRet = ParseString(acBuf, '|', VALUE_BILLSUBTYPECODE+2, apFlds);
   if (iRet < VALUE_BILLSUBTYPECODE+1)
   {
      LogMsg("***** Bad value record: %s", acRec);
      return -1;
   }

   // TRA
   remChar(apFlds[VALUE_TRAFORMATTED], '-');
   memcpy(pOutbuf+OFF_TRA, apFlds[VALUE_TRAFORMATTED], strlen(apFlds[VALUE_TRAFORMATTED]));


   // Land
   long lLand = atol(apFlds[VALUE_LANDVAL]);
   if (lLand > 0)
   {
      sprintf(acTmp, "%*u", SIZ_LAND, lLand);
      memcpy(pOutbuf+OFF_LAND, acTmp, SIZ_LAND);
   }

   // Improve
   long lImpr = atol(apFlds[VALUE_IMPVAL]);
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*u", SIZ_IMPR, lImpr);
      memcpy(pOutbuf+OFF_IMPR, acTmp, SIZ_IMPR);
   }

   // Other value
   long lPers = atol(apFlds[VALUE_PERSONALPROPVAL]);
   long lFixt = atol(apFlds[VALUE_OTHERIMPFIXTUREVAL]);
   long lMnrl = atol(apFlds[VALUE_MINERALVAL]);
   long lGross = lPers+lFixt+lMnrl;
   if (lGross > 0)
   {
      sprintf(acTmp, "%*u", SIZ_OTHER, lGross);
      memcpy(pOutbuf+OFF_OTHER, acTmp, SIZ_OTHER);

      if (lMnrl > 0)
      {
         sprintf(acTmp, "%d         ", lMnrl);
         memcpy(pOutbuf+OFF_MINERAL, acTmp, SIZ_MINERAL);
      }
      if (lPers > 0)
      {
         sprintf(acTmp, "%d         ", lPers);
         memcpy(pOutbuf+OFF_PERSPROP, acTmp, SIZ_PERSPROP);
      }
      if (lFixt > 0)
      {
         sprintf(acTmp, "%d         ", lFixt);
         memcpy(pOutbuf+OFF_FIXTR, acTmp, SIZ_FIXTR);
      }
   }

   // Gross total
   lGross += lLand+lImpr;
   if (lGross > 0)
   {
      sprintf(acTmp, "%*u", SIZ_GROSS, lGross);
      memcpy(pOutbuf+OFF_GROSS, acTmp, SIZ_GROSS);

      // Ratio
      if (lImpr > 0)
      {
         sprintf(acTmp, "%*u", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
         memcpy(pOutbuf+OFF_RATIO, acTmp, SIZ_RATIO);
      }
   }

   // HO Exempt
   iRet = atol(apFlds[VALUE_EXEMPTVAL]);
   *(pOutbuf+OFF_HO_FL) = '2';
   if (iRet > 0)
   {
      sprintf(acTmp, "%*u", SIZ_EXE_TOTAL, iRet);
      memcpy(pOutbuf+OFF_EXE_TOTAL, acTmp, SIZ_EXE_TOTAL);

      if (iRet == 7000)
         *(pOutbuf+OFF_HO_FL) = '1';

      // Set full exemption flag
      if (iRet >= lGross)
         *(pOutbuf+OFF_FULL_EXEMPT) = 'Y';

   }
   lValueMatch++;

   // Get next situs rec
   pRec = fgets(acRec, 1024, fdValue);
   return 0;
}

/******************************** Ker_MergeRoll2 *****************************
 *
 * Return 0 if successful, otherwise error
 *
 *****************************************************************************/

int Ker_MergeRoll2(char *pOutbuf, char *pRollRec)
{
   char  acTmp[256], acTmp1[256], *apFlds[ROLL_ROLLTYPECODE+2];
   int   iRet, iCnt;

   // Clear output buffer
   memset(pOutbuf, ' ', iRecLen);

#ifdef _DEBUG
   //if (!memcmp(pRollRec, "00940313008", 11))
   //   iRet = 1;
#endif

   // Parse input rec
   iCnt = ParseStringIQ(pRollRec, '|', ROLL_ROLLTYPECODE+2, apFlds);
   if (iCnt < ROLL_ROLLTYPECODE+1)
   {
      LogMsg("***** Bad roll record: %s", pRollRec);
      return -1;
   }

   // Use ATN for APN
   memcpy(pOutbuf, apFlds[ROLL_ATN], strlen(apFlds[ROLL_ATN]));
   memcpy(pOutbuf+OFF_CO_NUM, "15KER", 5);
   
   // Parcel status
   *(pOutbuf+OFF_STATUS) = 'A';

   // Save APN to ALT_APN
   memcpy(pOutbuf+OFF_ALT_APN, apFlds[ROLL_APN], strlen(apFlds[ROLL_APN]));

   // Save TE_NO to Other APN
   iRet = atol(apFlds[ROLL_TENO]);
   sprintf(acTmp, "%*u", RSIZ_TE_NO, iRet);
   memcpy(pOutbuf+OFF_OTHER_APN, acTmp, RSIZ_TE_NO);

   // Format APN
   iRet = formatApn(apFlds[ROLL_ATN], acTmp, &myCounty);
   memcpy(pOutbuf+OFF_APN_D, acTmp, iRet);

   // Create MapLink
   iRet = formatMapLink(apFlds[ROLL_ATN], acTmp, &myCounty);
   memcpy(pOutbuf+OFF_MAPLINK, acTmp, iRet);

   // Create index map link
   if (getIndexPage(acTmp, acTmp1, &myCounty))
      memcpy(pOutbuf+OFF_IMAPLINK, acTmp1, iRet);

   // Year assessed
   memcpy(pOutbuf+OFF_YR_ASSD, myCounty.acYearAssd, 4);

   // UseCode
   if (*apFlds[ROLL_USECODE] > ' ')
   {
      memset(pOutbuf+OFF_USE_STD, ' ', SIZ_USE_STD);
      memcpy(pOutbuf+OFF_USE_CO, apFlds[ROLL_USECODE], strlen(apFlds[ROLL_USECODE]));
      updateStdUse(pOutbuf+OFF_USE_STD, apFlds[ROLL_USECODE], strlen(apFlds[ROLL_USECODE]));
   } else
      memcpy(pOutbuf+OFF_USE_STD, USE_UNASGN, SIZ_USE_STD);

   // Lot acres
   // Acres
   double dTmp = atof(apFlds[ROLL_LEGALACRE]);
   if (dTmp > 0.0)
   {
      sprintf(acTmp, "%*u", SIZ_LOT_ACRES, (long)(dTmp*1000));
      memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);

      iRet = (long)(dTmp*SQFT_PER_ACRE);
      sprintf(acTmp, "%*u", SIZ_LOT_SQFT, iRet);
      memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);
   }

   // Legal
   //sprintf(acTmp, "%s %s %s %.*s", section, township, range, LGL_UNFORM_DESC, pLegal->Desc);

   char sLegal[6000];
   sLegal[0] = 0;
   switch (*apFlds[ROLL_LEGALTYPECODE])
   {
      case 'C':   // CITY-BLOCK-LOT
         sprintf(sLegal, "CITY %s BLOCK %s ", apFlds[ROLL_CITYABBREVIATION], apFlds[ROLL_BLOCK]);
         if (!memcmp(apFlds[ROLL_CITYLOT], "PTN", 3))
            sprintf(acTmp, "%s ", apFlds[ROLL_CITYLOT]);
         else
            sprintf(acTmp, "LOT %s ", apFlds[ROLL_CITYLOT]);
         strcat(sLegal, acTmp);
         break;
      case 'M':   // Meets and Bounds
         // Use unformatted legal
         break;
      case 'P':   // Parcel Map Lot
            sprintf(sLegal, "PM %s LOT %s ", apFlds[ROLL_PARCELMAP], apFlds[ROLL_PARCELLOT]);
         break;
      case 'S':   // Section Township Range Quarter section
         if (*apFlds[ROLL_QUARTER] > ' ')
            sprintf(sLegal, "S %s T %s R %s %s ", apFlds[ROLL_SECTION], apFlds[ROLL_TOWNSHIP], apFlds[ROLL_RANGE], apFlds[ROLL_QUARTER]);
         else
            sprintf(sLegal, "S %s T %s R %s ", apFlds[ROLL_SECTION], apFlds[ROLL_TOWNSHIP], apFlds[ROLL_RANGE]);
         break;
      case 'T':   // Tract Map Tract Lot
         memcpy(pOutbuf+OFF_TRACT, apFlds[ROLL_TRACTMAP], strlen(apFlds[ROLL_TRACTMAP]));
         memcpy(pOutbuf+OFF_BLOCK, apFlds[ROLL_TRACTBLOCK], strlen(apFlds[ROLL_TRACTBLOCK]));
         memcpy(pOutbuf+OFF_LOT, apFlds[ROLL_TRACTLOT], strlen(apFlds[ROLL_TRACTLOT]));

         // Avoid double entry since unformatted legal already includes TR/LOT info
         if (memcmp(apFlds[ROLL_UNFORMAT_LEGAL], "LOT", 3))
         {
            if (*apFlds[ROLL_TRACTBLOCK] > ' ')
               sprintf(sLegal, "MAP %s BLOCK %s ", apFlds[ROLL_TRACTMAP], apFlds[ROLL_TRACTBLOCK]);
            else
               sprintf(sLegal, "MAP %s ", apFlds[ROLL_TRACTMAP]);

            if (!memcmp(apFlds[ROLL_TRACTLOT], "PTN", 3))
               sprintf(acTmp, "%s ", apFlds[ROLL_TRACTLOT]);
            else
               sprintf(acTmp, "LOT %s ", apFlds[ROLL_TRACTLOT]);
            strcat(sLegal, acTmp);
         }
         break;
   }

   if (*apFlds[ROLL_UNFORMAT_LEGAL] > ' ')
   {
      strcat(sLegal, apFlds[ROLL_UNFORMAT_LEGAL]);
      replChar(sLegal, 243, 'N');
      iRet = blankRem(sLegal);
      if (iRet > iMaxLegal)
      {
         iMaxLegal = iRet;
         if (iRet > 256)
            LogMsg("Legal length: %d (ATN=%s)", iRet, apFlds[ROLL_ATN]);
      }
   }

   if (sLegal[0] > ' ')
      updateLegal(pOutbuf, sLegal);

   // Owner
   try 
   {
      Ker_MergeOwner2(pOutbuf, apFlds[ROLL_OWNERNAME]);
      iRet = 0;
   } catch (...)
   {
      iRet = -2;
   }

   // AgPreserve
   if (*apFlds[ROLL_AGPRESERVEDCODE] > ' ')
      *(pOutbuf+OFF_AG_PRE) = 'Y';

   // Government flag
   if (*apFlds[ROLL_ROLLTYPECODE] == '5')
   {
      *(pOutbuf+OFF_PUBL_FLG) = 'Y';
      *(pOutbuf+OFF_FULL_EXEMPT) = 'Y';
   }

   return iRet;
}

/********************************* Ker_Load_Roll2 *****************************
 *
 * Input file in ATN order
 *
 ******************************************************************************/

int Ker_Load_Roll2(int iFirstRec)
{
   char     *pTmp, acBuf[MAX_RECSIZE], acRollRec[6000];
   char     acOutFile[_MAX_PATH], acLienFile[_MAX_PATH];

   HANDLE   fhOut;
   DWORD    nBytesWritten;
   bool     bUseChar=false;
   long     lRet=0, lCnt=0;
   int      bRet, iRet, iRollUpd=0;

   LogMsg0("Load %s update roll", myCounty.acCntyCode);

   fdChar=fdBldg=fdMAdr=fdSitus=fdLegal=fdLien= NULL;

   // Check Lot acres usage
   GetIniString(sCnty, "UseChar", "N", acBuf, _MAX_PATH, acIniFile);
   if (acBuf[0] == 'Y')
      bUseChar = true;

   // Prepare input file names
   GetIniString(sCnty, "LienExtr", "", acLienFile, _MAX_PATH, acIniFile);

   // Open Roll file
   LogMsg("Open Roll file %s", acRollFile);
   fdRoll = fopen(acRollFile, "r");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", acRollFile);
      return -2;
   }

   // Open Char file
   LogMsg("Open Char file %s", acCChrFile);
   fdChar = fopen(acCChrFile, "r");
   if (fdChar == NULL)
   {
      LogMsg("***** Error opening Char file: %s\n", acCChrFile);
      return -2;
   }

   // Open Bldg file
   LogMsg("Open Bldg file %s", acCBldgFile);
   fdBldg = fopen(acCBldgFile, "r");
   if (fdBldg == NULL)
   {
      LogMsg("***** Error opening Bldg file: %s\n", acCBldgFile);
      return -2;
   }

   // Open Value file
   LogMsg("Open Value file %s", acValueFile);
   fdValue = fopen(acValueFile, "r");
   if (fdValue == NULL)
   {
      LogMsg("***** Error opening value file: %s\n", acValueFile);
      return -2;
   }

   // Open Lien file
   LogMsg("Open Lien file %s", acLienFile);
   fdLien = fopen(acLienFile, "r");
   if (fdLien == NULL)
   {
      LogMsg("***** Error opening lien file: %s\n", acLienFile);
      return -2;
   }

   // Open situs addr file
   if (!_access(acSAdrFile, 0))
   {
      LogMsg("Open Situs file %s", acSAdrFile);
      fdSitus = fopen(acSAdrFile, "r");
      if (fdSitus == NULL)
      {
         LogMsg("***** Error opening Situs file: %s\n", acSAdrFile);
         return -2;
      }
   }

   // Open mailing addr file
   if (!_access(acMAdrFile, 0))
   {
      LogMsg("Open Mailing file %s", acMAdrFile);
      fdMAdr = fopen(acMAdrFile, "r");
      if (fdMAdr == NULL)
      {
         LogMsg("***** Error opening Mailing file: %s\n", acMAdrFile);
         return -2;
      }
   }

   // Open Output file
   sprintf(acOutFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "R01");
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error creating output file %s\n", acOutFile);
      return -3;
   }

   // Create header record
   if (iFirstRec)
   {
      memset(acBuf, '9', iRecLen);
      // Ignore record start with 99999999
      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
   }

   iNoMatch=iBadCity=iBadSuffix=0;

   // Skip headers
   pTmp = fgets((char *)&acRollRec[0], MAX_RECSIZE, fdRoll);
   pTmp = fgets((char *)&acRollRec[0], MAX_RECSIZE, fdRoll);

   // Get first RollRec
   pTmp = fgets((char *)&acRollRec[0], MAX_RECSIZE, fdRoll);
   memset(acBuf, ' ', iRecLen);

   // Merge loop
   while (pTmp)
   {
      if (acRollRec[0] == '\n')
         break;

      // Create new R01 record
      iRet = Ker_MergeRoll2(acBuf, acRollRec);
      if (!iRet)
      {
         if (fdLien)
         {
            iRet = Ker_UpdateLien2(acBuf);
            if (iRet && fdValue)
            {
               // New parcel, get current values
               iRet = Ker_MergeValue2(acBuf);
            }
         } else if (fdValue)
            iRet = Ker_MergeValue2(acBuf);

         if (fdSitus)
            iRet = Ker_MergeSitus2(acBuf);

         if (fdMAdr)
            iRet = Ker_MergeMAdr2(acBuf);

         if (fdChar)
            iRet = Ker_MergeCVal(acBuf, bUseChar);

         if (fdBldg)
            iRet = Ker_MergeBldg(acBuf);

         bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
         if (!bRet)
         {
            LogMsg("***** Error occurs: %d\n", GetLastError());
            lRet = WRITE_ERR;
            break;
         }

         if (!(++lCnt % 1000))
            printf("\r%u", lCnt);
      }

      // Get next roll record
      do {
         // Ignore record without APN
         pTmp = fgets(acRollRec, 6000, fdRoll);
      } while (pTmp && *pTmp < '0');
   }

   // Close files
   if (fdRoll)
      fclose(fdRoll);
   if (fdSitus)
      fclose(fdSitus);
   if (fdMAdr)
      fclose(fdMAdr);
   if (fdLien)
      fclose(fdLien);
   if (fdChar)
      fclose(fdChar);
   if (fdBldg)
      fclose(fdBldg);
   if (fdValue)
      fclose(fdValue);

   if (fhOut)
      CloseHandle(fhOut);

   LogMsgD("\nTotal output records:       %u\n", lCnt);

   LogMsg("Total Lien matched:         %u", lLienMatch);
   LogMsg("Total Lien skipped:         %u\n", lLienSkip);

   LogMsg("Total Value matched:        %u", lValueMatch);
   LogMsg("Total Value skipped:        %u\n", lValueSkip);

   LogMsg("Total Mail matched:         %u", lMAdrMatch);
   LogMsg("Total Mail skipped:         %u\n", lMAdrSkip);

   LogMsg("Total Situs matched:        %u", lSitusMatch);
   LogMsg("Total Situs skipped:        %u\n", lSitusSkip);

   LogMsg("Total Char matched:         %u", lCharMatch);
   LogMsg("Total Char skipped:         %u\n", lCharSkip);

   LogMsg("Total Bldg matched:         %u", lBldgMatch);
   LogMsg("Total Bldg skipped:         %u\n", lBldgSkip);

   LogMsg("Total bad-city records:     %u", iBadCity);
   LogMsg("Total bad-suffix records:   %u\n", iBadSuffix);

   lRecCnt = lRet;

   if (!lLienMatch || !lMAdrMatch || !lSitusMatch || !lCharMatch || !lBldgMatch)
      return -1;
   else
      return 0;
}

/********************************* Ker_MergeLien *****************************
 *
 * Note: LDR roll doesn't have APN field, don't apply AltApn.
 *
 * Return 0 if successful, >0 if error
 *
 *****************************************************************************/

void Ker_MergeLien(char *pOutbuf, char *pRollRec, int iLen, int iFlag)
{
   KER_LIEN *pRec;
   char     acTmp[256], acTmp1[256];
   int      iRet, lTmp;

   pRec = (KER_LIEN *)pRollRec;
   // Clear output buffer
   memset(pOutbuf, ' ', iRecLen);

   // Use ATN for APN
   memcpy(pOutbuf, pRec->Atn, LSIZ_ATN);

   // Save TE_NO
   lTmp = atoin(pRec->Te_No, LSIZ_TE_NO);
   sprintf(acTmp, "%*u", LSIZ_TE_NO, lTmp);
   memcpy(pOutbuf+OFF_OTHER_APN, acTmp, LSIZ_TE_NO);

   memcpy(pOutbuf+OFF_CO_NUM, "15KER", 5);
   *(pOutbuf+OFF_STATUS) = 'A';

   // Format APN
   iRet = formatApn(pRec->Atn, acTmp, &myCounty);
   memcpy(pOutbuf+OFF_APN_D, acTmp, iRet);

   // Create MapLink
   iRet = formatMapLink(pRec->Atn, acTmp, &myCounty);
   memcpy(pOutbuf+OFF_MAPLINK, acTmp, iRet);

   // Create index map link
   if (getIndexPage(acTmp, acTmp1, &myCounty))
      memcpy(pOutbuf+OFF_IMAPLINK, acTmp1, iRet);

   // Year assessed
   memcpy(pOutbuf+OFF_YR_ASSD, myCounty.acYearAssd, 4);

   // Land
   long lLand = atoin(pRec->Land, LSIZ_LAND_VALUE);
   if (lLand > 0)
   {
      sprintf(acTmp, "%*u", SIZ_LAND, lLand);
      memcpy(pOutbuf+OFF_LAND, acTmp, SIZ_LAND);
   }

   // Improve
   long lImpr = atoin(pRec->Impr, LSIZ_LAND_VALUE);
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*u", SIZ_IMPR, lImpr);
      memcpy(pOutbuf+OFF_IMPR, acTmp, SIZ_IMPR);
   }

   // Other value
   long lPers = atoin(pRec->PP_Val, LSIZ_LAND_VALUE);
   long lFixt = atoin(pRec->Fixt, LSIZ_LAND_VALUE);
   long lMnrl = atoin(pRec->Mineral, LSIZ_LAND_VALUE);
   lTmp = lPers + lFixt + lMnrl;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*u", SIZ_OTHER, lTmp);
      memcpy(pOutbuf+OFF_OTHER, acTmp, SIZ_OTHER);

      if (lMnrl > 0)
      {
         sprintf(acTmp, "%d         ", lMnrl);
         memcpy(pOutbuf+OFF_MINERAL, acTmp, SIZ_MINERAL);
      }

      if (lPers > 0)
      {
         sprintf(acTmp, "%d         ", lPers);
         memcpy(pOutbuf+OFF_PERSPROP, acTmp, SIZ_PERSPROP);
      }

      if (lFixt > 0)
      {
         sprintf(acTmp, "%d         ", lFixt);
         memcpy(pOutbuf+OFF_FIXTR, acTmp, SIZ_FIXTR);
      }
   }

   // Gross total
   long lGross = lTmp+lLand+lImpr;
   if (lGross > 0)
   {
      sprintf(acTmp, "%*u", SIZ_GROSS, lGross);
      memcpy(pOutbuf+OFF_GROSS, acTmp, SIZ_GROSS);

      // Ratio
      if (lImpr > 0)
      {
         sprintf(acTmp, "%*u", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
         memcpy(pOutbuf+OFF_RATIO, acTmp, SIZ_RATIO);
      }
   }

   // HO Exempt
   if (pRec->Exmpt_Type_Code[0] == 'H')
      *(pOutbuf+OFF_HO_FL) = '1';      // 'Y'
   else
      *(pOutbuf+OFF_HO_FL) = '2';      // 'N'

   if (pRec->Exmpt_Type_Code[0] > ' ')
   {
      lTmp = atoin(pRec->Exe_Amt, LSIZ_EXE_VALUE);
      sprintf(acTmp, "%*u", SIZ_EXE_TOTAL, lTmp);
      memcpy(pOutbuf+OFF_EXE_TOTAL, acTmp, SIZ_EXE_TOTAL);
      memcpy(pOutbuf+OFF_EXE_CD1, pRec->Exmpt_Type_Code, LSIZ_EXMPT_TYPE_CODE);

      if (lTmp > lGross)
         *(pOutbuf+OFF_FULL_EXEMPT) = 'Y';
   }

   // UseCode
   if (pRec->UseCode[0] > ' ')
   {
      memcpy(pOutbuf+OFF_USE_CO, pRec->UseCode, LSIZ_USE_CODE);
      updateStdUse(pOutbuf+OFF_USE_STD, pRec->UseCode, LSIZ_USE_CODE);
   } else
      memcpy(pOutbuf+OFF_USE_STD, USE_UNASGN, SIZ_USE_STD);

   // TRA
   memcpy(pOutbuf+OFF_TRA, pRec->TRA, LSIZ_TRA_NO);

   // Owner
   try {
      Ker_MergeOwner(pOutbuf, pRec->OwnerName, pRec->CareOf);
   } catch (...)
   {
      LogMsg("***** Access violation in Ker_MergeOwner().  Please check %.11s", pOutbuf);
   }

   // Mailing
   try {
      Ker_MergeMAdr(pOutbuf, pRec->M_Addr1, pRec->M_Addr2, pRec->M_Addr3, pRec->M_Zip);
   } catch (...)
   {
      LogMsg("***** Access violation in Ker_MergeMAdr().  Please check %.11s", pOutbuf);
   }

   // AgPreserve
   if (pRec->AgPreserve[0] > ' ')
      *(pOutbuf+OFF_AG_PRE) = 'Y';

   // Government flag
   if (pRec->RollType[0] == '5')
   {
      *(pOutbuf+OFF_PUBL_FLG) = 'Y';
      *(pOutbuf+OFF_FULL_EXEMPT) = 'Y';
   }

   // Legal
   // 07/19/2012 - Use MergeLegal if legal file available
   if (!fdLegal && pRec->Legal[0] > ' ')
   {
      memcpy(acTmp, pRec->Legal, LSIZ_LEGAL);
      acTmp[LSIZ_LEGAL] = 0;
      replStrAll(acTmp, " ,", ",");
      iRet = updateLegal(pOutbuf, acTmp);
      if (iRet > iMaxLegal)
         iMaxLegal = iRet;
   }

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "07701001006", 11))
   //   iRet = 1;
#endif

   // Lot Sqft
   lTmp = atoin(pRec->Land_Acres, LSIZ_LAND_ACRES);
   if (lTmp > 0)
   {
      DWORD dSqft;
      dSqft = ((DWORD)lTmp*SQFT_PER_ACRE)/100;
      if (dSqft < 0)
         dSqft = 0;
      sprintf(acTmp, "%*u", SIZ_LOT_SQFT, ((DWORD)lTmp*SQFT_PER_ACRE)/100);
      memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);

      lTmp *= 10;
      sprintf(acTmp, "%*u", SIZ_LOT_ACRES, lTmp);
      memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);
   }
}

/********************************** Ker_MargeAnn *******************************
 *
 *******************************************************************************/

int Ker_MergeApn(char *pOutbuf)
{
   static   char acRec[6000], *pRec=NULL;
   char     acBuf[6000], *apFlds[32];
   int      iRet, iLoop;

   // Get first situs rec for first call
   if (!pRec)
   {
      // Drop header
      pRec = fgets(acRec, 6000, fdApn);
      pRec = fgets(acRec, 6000, fdApn);

      // Get first rec
      pRec = fgets(acRec, 6000, fdApn);
   }

   do
   {
      if (!pRec)
      {
         fclose(fdApn);
         fdApn = NULL;
         return 1;      // EOF
      }

      // Compare Apn
      iLoop = memcmp(pOutbuf, pRec, iApnLen);
      if (iLoop > 0)
      {
         if (bDebug)
            LogMsg0("Skip APN rec  %.*s", iApnLen, pRec);
         pRec = fgets(acRec, 6000, fdApn);
         lApnSkip++;
      }
   } while (iLoop > 0);

   // If not match, return
   if (iLoop)
      return 1;

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "00102001015", 11))
   //   lTmp = 0;
#endif
   // Parse input rec
   strcpy(acBuf, acRec);
   iRet = ParseString(acBuf, '|', ROLL_ROLLTYPECODE+1, apFlds);
   if (iRet < ROLL_ROLLTYPECODE)
   {
      LogMsg("***** Bad APN record: %s (%d)", acRec, iRet);
      return -1;
   }

   memcpy(pOutbuf+OFF_ALT_APN, apFlds[ROLL_APN], strlen(apFlds[ROLL_APN]));
   lApnMatch++;

   // Get next situs rec
   pRec = fgets(acRec, 6000, fdApn);

   return 0;
}

/********************************* Ker_Load_LDR *****************************
 *
 * LDR roll doesn't contain APN.  We need to merge that into ALT_APN using KER_ROLL.TXT
 *
 ****************************************************************************/

int Ker_Load_LDR(char *pRollFile, int iFirstRec)
{
   char     *pTmp, acBuf[MAX_RECSIZE], acRollRec[MAX_RECSIZE];
   char     acOutFile[_MAX_PATH], acTmpFile[_MAX_PATH];

   HANDLE   fhOut;
   DWORD    nBytesWritten;
   BOOL     bRet;
   long     lRet=0, lCnt=0, iRet;
   KER_LIEN *pRec = (KER_LIEN *)&acRollRec[0];

   LogMsg0("Loading LDR %d", lLienYear);

   fdBldg=fdSitus=fdChar=NULL;

   // Open Roll file
   LogMsg("Open Roll file %s", pRollFile);
   fdRoll = fopen(pRollFile, "r");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", pRollFile);
      return -2;
   }

   // Open Char file
   LogMsg("Open Char file %s", acCChrFile);
   fdChar = fopen(acCChrFile, "r");
   if (fdChar == NULL)
   {
      LogMsg("***** Error opening Char file: %s\n", acCChrFile);
      return -2;
   }

   // Open Bldg file
   LogMsg("Open Bldg file %s", acCBldgFile);
   fdBldg = fopen(acCBldgFile, "r");
   if (fdBldg == NULL)
   {
      LogMsg("***** Error opening Bldg file: %s\n", acCBldgFile);
      return -2;
   }

   // Open situs file
   if (!_access(acSAdrFile, 0))
   {
      LogMsg("Open Situs file %s", acSAdrFile);
      fdSitus = fopen(acSAdrFile, "r");
      if (fdSitus == NULL)
      {
         LogMsg("***** Error opening Situs file: %s\n", acSAdrFile);
         return -2;
      }
   }

   // Open APN file
   LogMsg("Open APN file %s", acRollFile);
   fdApn = fopen(acRollFile, "r");
   if (fdApn == NULL)
   {
      LogMsg("***** Error opening APN file: %s\n", acRollFile);
      return -2;
   }

   // Open Output file
   sprintf(acOutFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "R01");
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error creating output file %s\n", acOutFile);
      return -4;
   }

   // Create header record
   if (iFirstRec)
   {
      memset(acBuf, '9', iRecLen);
      // Ignore record start with 99999999
      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
   }

   iNoMatch=iBadCity=iBadSuffix=0;

   // Get first RollRec
   do {
      pTmp = fgets((char *)&acRollRec[0], MAX_RECSIZE, fdRoll);
   } while (pTmp && !isdigit(acRollRec[LOFF_ATN]));

   memset(acBuf, ' ', iRecLen);

   // Merge loop
   while (pTmp)
   {
#ifdef _DEBUG
      //if (!memcmp(&acRollRec[ROFF_TE_NO], "        18", 10))
      //   iRet = 1;
#endif
      // Create new R01 record
      Ker_MergeLien(acBuf, acRollRec, 0, CREATE_R01|CREATE_LIEN);

      if (fdApn)
         iRet = Ker_MergeApn(acBuf);

      if (fdSitus)
         iRet = Ker_MergeSitus2(acBuf);

      if (fdChar)
         iRet = Ker_MergeCVal(acBuf, false);

      if (fdBldg)
         iRet = Ker_MergeBldg(acBuf);


      // Get last recording date
      iRet = atoin((char *)&acBuf[OFF_TRANSFER_DT], SIZ_TRANSFER_DT);
      if (iRet >= lToday)
         LogMsg0("*** WARNING: Bad last recording date %d at record# %d -->%.10s", iRet, lCnt, acBuf);
      else if (lLastRecDate < iRet)
         lLastRecDate = iRet;

      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
      if (!bRet)
      {
         LogMsg("***** Error occurs: %d\n", GetLastError());
         lRet = WRITE_ERR;
         break;
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);

      // Get next roll record
      do {
         // Ignore record without APN
         pTmp = fgets(acRollRec, MAX_RECSIZE, fdRoll);
      } while (pTmp && pRec->Atn[0] == ' ');
   }

   // Close files
   if (fdRoll)
      fclose(fdRoll);
   if (fdSitus)
      fclose(fdSitus);
   if (fdBldg)
      fclose(fdBldg);
   if (fdChar)
      fclose(fdChar);
   if (fdApn)
      fclose(fdApn);
   if (fhOut)
      CloseHandle(fhOut);

   // Clean up temp file
   if (bClean)
      remove(acTmpFile);

   printf("\n");
   LogMsgD("Total output records:       %u", lCnt);
   LogMsg("Total bad-city records:     %u", iBadCity);
   LogMsg("Total bad-suffix records:   %u\n", iBadSuffix);

   LogMsg("Total Situs matched:        %u", lSitusMatch);
   LogMsg("Total Situs skipped:        %u\n", lSitusSkip);

   LogMsg("Total Char matched:         %u", lCharMatch);
   LogMsg("Total Char skipped:         %u\n", lCharSkip);

   LogMsg("Total Bldg matched:         %u", lBldgMatch);
   LogMsg("Total Bldg skipped:         %u\n", lBldgSkip);

   LogMsg("Total APN matched:          %u", lApnMatch);
   LogMsg("Total APN skipped:          %u\n", lApnSkip);

   lRecCnt = lCnt;
   lLDRRecCount = lCnt;
   return 0;
}

/****************************** Ker_MergeLienCsv *****************************
 *
 * Note: LDR roll doesn't have APN field, don't apply AltApn.
 *
 * Return 0 if successful, >0 if error
 *
 *****************************************************************************/

//int Ker_MergeLienCsv(char *pOutbuf, char *pRollRec, int iFlag)
//{
//   char     acTmp[256], acTmp1[256];
//   int      iRet;
//   unsigned long lTmp;
//
//   // Parse input rec
//   iTokens = ParseStringIQ(pRollRec, cLdrSep, MAX_FLD_TOKEN, apTokens);
//   if (iTokens < KER_L_PRIOR_LD_VALUE)
//   {
//      LogMsg("***** Error: bad input record for APN=%s (%d)", apTokens[KER_L_ATN], iTokens);
//      return -1;
//   }
//
//   // Clear output buffer
//   memset(pOutbuf, ' ', iRecLen);
//
//   // Use ATN for APN
//   vmemcpy(pOutbuf+OFF_APN_S, apTokens[KER_L_ATN], SIZ_APN_S);
//   // Format APN
//   iRet = formatApn(apTokens[KER_L_ATN], acTmp, &myCounty);
//   memcpy(pOutbuf+OFF_APN_D, acTmp, iRet);
//
//   // AltApn
//   vmemcpy(pOutbuf+OFF_ALT_APN, apTokens[KER_L_APN], SIZ_APN_S);
//
//   // Save TE_NO
//   lTmp = atol(apTokens[KER_L_TENO]);
//   sprintf(acTmp, "%*u", LSIZ_TE_NO, lTmp);
//   memcpy(pOutbuf+OFF_OTHER_APN, acTmp, LSIZ_TE_NO);
//
//   memcpy(pOutbuf+OFF_CO_NUM, "15KER", 5);
//   *(pOutbuf+OFF_STATUS) = 'A';
//
//   // Create MapLink
//   iRet = formatMapLink(apTokens[KER_L_ATN], acTmp, &myCounty);
//   memcpy(pOutbuf+OFF_MAPLINK, acTmp, iRet);
//
//   // Create index map link
//   if (getIndexPage(acTmp, acTmp1, &myCounty))
//      memcpy(pOutbuf+OFF_IMAPLINK, acTmp1, iRet);
//
//   // Year assessed
//   memcpy(pOutbuf+OFF_YR_ASSD, myCounty.acYearAssd, 4);
//
//   // Land
//   long lLand = atol(apTokens[KER_L_LAND]);
//   if (lLand > 0)
//   {
//      sprintf(acTmp, "%*u", SIZ_LAND, lLand);
//      memcpy(pOutbuf+OFF_LAND, acTmp, SIZ_LAND);
//   }
//
//   // Improve
//   long lImpr = atol(apTokens[KER_L_IMPR]);
//   if (lImpr > 0)
//   {
//      sprintf(acTmp, "%*u", SIZ_IMPR, lImpr);
//      memcpy(pOutbuf+OFF_IMPR, acTmp, SIZ_IMPR);
//   }
//
//   // Other value
//   long lPers = atol(apTokens[KER_L_PERSPROP]);
//   long lFixt = atol(apTokens[KER_L_OTHER_IMP]);
//   long lMnrl = atol(apTokens[KER_L_MINERAL]);
//   lTmp = lPers + lFixt + lMnrl;
//   if (lTmp > 0)
//   {
//      sprintf(acTmp, "%*u", SIZ_OTHER, lTmp);
//      memcpy(pOutbuf+OFF_OTHER, acTmp, SIZ_OTHER);
//
//      if (lMnrl > 0)
//      {
//         sprintf(acTmp, "%u         ", lMnrl);
//         memcpy(pOutbuf+OFF_MINERAL, acTmp, SIZ_MINERAL);
//      }
//
//      if (lPers > 0)
//      {
//         sprintf(acTmp, "%u         ", lPers);
//         memcpy(pOutbuf+OFF_PERSPROP, acTmp, SIZ_PERSPROP);
//      }
//
//      if (lFixt > 0)
//      {
//         sprintf(acTmp, "%u         ", lFixt);
//         memcpy(pOutbuf+OFF_FIXTR, acTmp, SIZ_FIXTR);
//      }
//   }
//
//   // Gross total
//   LONGLONG lGross = lTmp+lLand+lImpr;
//   if (lGross > 0)
//   {
//      sprintf(acTmp, "%*u", SIZ_GROSS, lGross);
//      memcpy(pOutbuf+OFF_GROSS, acTmp, SIZ_GROSS);
//
//      // Ratio
//      if (lImpr > 0)
//      {
//         sprintf(acTmp, "%*u", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
//         memcpy(pOutbuf+OFF_RATIO, acTmp, SIZ_RATIO);
//      }
//   }
//
//   // HO Exempt
//   if (*apTokens[KER_L_EX] == 'H')
//      *(pOutbuf+OFF_HO_FL) = '1';      // 'Y'
//   else
//      *(pOutbuf+OFF_HO_FL) = '2';      // 'N'
//
//   if (*apTokens[KER_L_EX] > ' ')
//   {
//      lTmp = atol(apTokens[KER_L_EXEMPTIONS]);
//      sprintf(acTmp, "%*u", SIZ_EXE_TOTAL, lTmp);
//      memcpy(pOutbuf+OFF_EXE_TOTAL, acTmp, SIZ_EXE_TOTAL);
//      vmemcpy(pOutbuf+OFF_EXE_CD1, apTokens[KER_L_EX], SIZ_EXE_CD);
//
//      if (lTmp > lGross)
//         *(pOutbuf+OFF_FULL_EXEMPT) = 'Y';
//   }
//
//   // UseCode
//   if (*apTokens[KER_L_USE] > ' ')
//   {
//      vmemcpy(pOutbuf+OFF_USE_CO, apTokens[KER_L_USE], SIZ_USE_CO);
//      updateStdUse(pOutbuf+OFF_USE_STD, apTokens[KER_L_USE]);
//   } else
//      memcpy(pOutbuf+OFF_USE_STD, USE_UNASGN, SIZ_USE_STD);
//
//   // TRA
//   vmemcpy(pOutbuf+OFF_TRA, apTokens[KER_L_TRA], SIZ_TRA);
//
//   // Owner
//   try {
//      Ker_MergeOwner2(pOutbuf, apTokens[KER_L_ASSESSEE]);
//   } catch (...)
//   {
//      LogMsg("***** Access violation in Ker_MergeOwner2().  Please check %.11s", pOutbuf);
//   }
//
//   // Mailing
//   try {
//      Ker_MergeMAdrCsv(pOutbuf, apTokens[KER_L_MADDR1], apTokens[KER_L_MADDR2], apTokens[KER_L_MADDR3], 
//         apTokens[KER_L_ZIPCODE], apTokens[KER_L_CAREOF], apTokens[KER_L_DBA]);
//   } catch (...)
//   {
//      LogMsg("***** Access violation in Ker_MergeMAdrCsv().  Please check %.11s", pOutbuf);
//   }
//
//   // AgPreserve
//   if (*apTokens[KER_L_AGPRESERVE] > ' ')
//      *(pOutbuf+OFF_AG_PRE) = 'Y';
//
//   // Government flag
//   if (*apTokens[KER_L_ROLL] == '5')
//   {
//      *(pOutbuf+OFF_PUBL_FLG) = 'Y';
//      *(pOutbuf+OFF_FULL_EXEMPT) = 'Y';
//   }
//
//   // Legal
//   if (*apTokens[KER_L_LEGAL] > ' ')
//   {
//      strcpy(acTmp, apTokens[KER_L_LEGAL]);
//      replStrAll(acTmp, " ,", ",");
//      iRet = updateLegal(pOutbuf, acTmp);
//      if (iRet > iMaxLegal)
//         iMaxLegal = iRet;
//   }
//
//#ifdef _DEBUG
//   //if (!memcmp(pOutbuf, "07701001006", 11))
//   //   iRet = 1;
//#endif
//
//   // Lot Sqft - 9v99
//   double dTmp = atof(apTokens[KER_L_ACRES]);
//   if (dTmp > 0.0)
//   {
//      lTmp = (unsigned long)(dTmp*SQFT_PER_ACRE);
//      sprintf(acTmp, "%*u", SIZ_LOT_SQFT, lTmp);
//      memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);
//
//      lTmp = (unsigned long)(dTmp*1000.0);
//      sprintf(acTmp, "%*u", SIZ_LOT_ACRES, lTmp);
//      memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);
//   }
//
//   return 0;
//}

int Ker_MergeLienCsv1(char *pOutbuf, char *pRollRec, int iFlag)
{
   char     acTmp[256], acTmp1[256];
   int      iRet;
   unsigned long lTmp;

   // Parse input rec
   iTokens = ParseStringIQ(pRollRec, cLdrSep, MAX_FLD_TOKEN, apTokens);
   if (iTokens < KER_L_FLDS)
   {
      LogMsg("***** Error: bad input record for APN=%s (%d)", apTokens[KER_L_ATN], iTokens);
      return -1;
   }

   // Clear output buffer
   memset(pOutbuf, ' ', iRecLen);

   // Use ATN for APN
   vmemcpy(pOutbuf+OFF_APN_S, apTokens[KER_L_ATN], SIZ_APN_S);
   // Format APN
   iRet = formatApn(apTokens[KER_L_ATN], acTmp, &myCounty);
   memcpy(pOutbuf+OFF_APN_D, acTmp, iRet);

   // AltApn
   //vmemcpy(pOutbuf+OFF_ALT_APN, apTokens[KER_L_APN], SIZ_APN_S);

   // Save TE_NO
   lTmp = atol(apTokens[KER_L_TENO]);
   sprintf(acTmp, "%*u", LSIZ_TE_NO, lTmp);
   memcpy(pOutbuf+OFF_OTHER_APN, acTmp, LSIZ_TE_NO);

   memcpy(pOutbuf+OFF_CO_NUM, "15KER", 5);
   *(pOutbuf+OFF_STATUS) = 'A';

   // Create MapLink
   iRet = formatMapLink(apTokens[KER_L_ATN], acTmp, &myCounty);
   memcpy(pOutbuf+OFF_MAPLINK, acTmp, iRet);

   // Create index map link
   if (getIndexPage(acTmp, acTmp1, &myCounty))
      memcpy(pOutbuf+OFF_IMAPLINK, acTmp1, iRet);

   // Year assessed
   memcpy(pOutbuf+OFF_YR_ASSD, myCounty.acYearAssd, 4);

   // Land
   long lLand = atol(apTokens[KER_L_LAND]);
   if (lLand > 0)
   {
      sprintf(acTmp, "%*u", SIZ_LAND, lLand);
      memcpy(pOutbuf+OFF_LAND, acTmp, SIZ_LAND);
   }

   // Improve
   long lImpr = atol(apTokens[KER_L_IMPR]);
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*u", SIZ_IMPR, lImpr);
      memcpy(pOutbuf+OFF_IMPR, acTmp, SIZ_IMPR);
   }

   // Other value
   long lPers = atol(apTokens[KER_L_PERSPROP]);
   long lFixt = atol(apTokens[KER_L_OTHER_IMP]);
   long lMnrl = atol(apTokens[KER_L_MINERAL]);
   lTmp = lPers + lFixt + lMnrl;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*u", SIZ_OTHER, lTmp);
      memcpy(pOutbuf+OFF_OTHER, acTmp, SIZ_OTHER);

      if (lMnrl > 0)
      {
         sprintf(acTmp, "%u         ", lMnrl);
         memcpy(pOutbuf+OFF_MINERAL, acTmp, SIZ_MINERAL);
      }

      if (lPers > 0)
      {
         sprintf(acTmp, "%u         ", lPers);
         memcpy(pOutbuf+OFF_PERSPROP, acTmp, SIZ_PERSPROP);
      }

      if (lFixt > 0)
      {
         sprintf(acTmp, "%u         ", lFixt);
         memcpy(pOutbuf+OFF_FIXTR, acTmp, SIZ_FIXTR);
      }
   }

   // Gross total
   LONGLONG lGross = lTmp+lLand+lImpr;
   if (lGross > 0)
   {
      sprintf(acTmp, "%*u", SIZ_GROSS, lGross);
      memcpy(pOutbuf+OFF_GROSS, acTmp, SIZ_GROSS);

      // Ratio
      if (lImpr > 0)
      {
         sprintf(acTmp, "%*u", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
         memcpy(pOutbuf+OFF_RATIO, acTmp, SIZ_RATIO);
      }
   }

   // HO Exempt
   if (*apTokens[KER_L_EXECODE] == 'H')
      *(pOutbuf+OFF_HO_FL) = '1';      // 'Y'
   else
      *(pOutbuf+OFF_HO_FL) = '2';      // 'N'

   if (*apTokens[KER_L_EXECODE] > ' ')
   {
      lTmp = atol(apTokens[KER_L_EXEAMT]);
      sprintf(acTmp, "%*u", SIZ_EXE_TOTAL, lTmp);
      memcpy(pOutbuf+OFF_EXE_TOTAL, acTmp, SIZ_EXE_TOTAL);
      vmemcpy(pOutbuf+OFF_EXE_CD1, apTokens[KER_L_EXECODE], SIZ_EXE_CD);

      // Create exemption type
      makeExeType(pOutbuf+OFF_EXE_TYPE, pOutbuf+OFF_EXE_CD1, pOutbuf+OFF_EXE_CD2, pOutbuf+OFF_EXE_CD3, (IDX_TBL4 *)&KER_Exemption);

      if (lTmp > lGross)
         *(pOutbuf+OFF_FULL_EXEMPT) = 'Y';
   }

   // UseCode
   if (*apTokens[KER_L_USE] > ' ')
   {
      vmemcpy(pOutbuf+OFF_USE_CO, apTokens[KER_L_USE], SIZ_USE_CO);
      updateStdUse(pOutbuf+OFF_USE_STD, apTokens[KER_L_USE]);
   } else
      memcpy(pOutbuf+OFF_USE_STD, USE_UNASGN, SIZ_USE_STD);

   // TRA
   vmemcpy(pOutbuf+OFF_TRA, apTokens[KER_L_TRA], SIZ_TRA);

   // Owner
   try {
      Ker_MergeOwner2(pOutbuf, apTokens[KER_L_ASSESSEE]);
   } catch (...)
   {
      LogMsg("***** Access violation in Ker_MergeOwner2().  Please check %.11s", pOutbuf);
   }

   // Mailing
   try {
      Ker_MergeMAdrCsv(pOutbuf, apTokens[KER_L_MADDR1], apTokens[KER_L_MADDR2], apTokens[KER_L_MADDR3], 
         apTokens[KER_L_ZIPCODE], apTokens[KER_L_CAREOF], apTokens[KER_L_DBA]);
   } catch (...)
   {
      LogMsg("***** Access violation in Ker_MergeMAdrCsv().  Please check %.11s", pOutbuf);
   }

   // AgPreserve
   if (*apTokens[KER_L_AGPRESERVE] > ' ')
      *(pOutbuf+OFF_AG_PRE) = 'Y';

   // Government flag
   if (*apTokens[KER_L_ROLL] == '5')
   {
      *(pOutbuf+OFF_PUBL_FLG) = 'Y';
      *(pOutbuf+OFF_FULL_EXEMPT) = 'Y';
   }

   // Legal
   if (*apTokens[KER_L_LEGAL] > ' ')
   {
      strcpy(acTmp, apTokens[KER_L_LEGAL]);
      replStrAll(acTmp, " ,", ",");
      iRet = updateLegal(pOutbuf, acTmp);
      if (iRet > iMaxLegal)
         iMaxLegal = iRet;
   }

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "07701001006", 11))
   //   iRet = 1;
#endif

   // Lot Sqft - 9v99
   double dTmp = atof(apTokens[KER_L_ACRES]);
   if (dTmp > 0.0)
   {
      lTmp = (unsigned long)(dTmp*SQFT_PER_ACRE);
      sprintf(acTmp, "%*u", SIZ_LOT_SQFT, lTmp);
      memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);

      lTmp = (unsigned long)(dTmp*1000.0);
      sprintf(acTmp, "%*u", SIZ_LOT_ACRES, lTmp);
      memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);
   }

   return 0;
}

/******************************* Ker_Load_LDR_Csv ***************************
 *
 * LDR roll is CSV delimited by <TAB>.  This file has both ATN & APN.
 *
 ****************************************************************************/

int Ker_Load_LDR_Csv(char *pRollFile, int iFirstRec)
{
   char     *pTmp, acBuf[MAX_RECSIZE], acRollRec[MAX_RECSIZE];
   char     acOutFile[_MAX_PATH], acTmpFile[_MAX_PATH];

   HANDLE   fhOut;
   DWORD    nBytesWritten;
   BOOL     bRet;
   long     lRet=0, lCnt=0, iRet;
   KER_LIEN *pRec = (KER_LIEN *)&acRollRec[0];

   LogMsg0("Loading LDR %d", lLienYear);
   fdBldg=fdSitus=fdChar=NULL;

   // Open Roll file
   LogMsg("Open Roll file %s", pRollFile);
   fdRoll = fopen(pRollFile, "r");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", pRollFile);
      return -2;
   }

   // Open Char file
   LogMsg("Open Char file %s", acCChrFile);
   fdChar = fopen(acCChrFile, "r");
   if (fdChar == NULL)
   {
      LogMsg("***** Error opening Char file: %s\n", acCChrFile);
      return -2;
   }

   // Open Bldg file
   LogMsg("Open Bldg file %s", acCBldgFile);
   fdBldg = fopen(acCBldgFile, "r");
   if (fdBldg == NULL)
   {
      LogMsg("***** Error opening Bldg file: %s\n", acCBldgFile);
      return -2;
   }

   // Open situs file
   if (!_access(acSAdrFile, 0))
   {
      LogMsg("Open Situs file %s", acSAdrFile);
      fdSitus = fopen(acSAdrFile, "r");
      if (fdSitus == NULL)
      {
         LogMsg("***** Error opening Situs file: %s\n", acSAdrFile);
         return -2;
      }
   }

   // Open Output file
   sprintf(acOutFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "R01");
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error creating output file %s\n", acOutFile);
      return -4;
   }

   // Create header record
   if (iFirstRec)
   {
      memset(acBuf, '9', iRecLen);
      // Ignore record start with 99999999
      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
   }

   iNoMatch=iBadCity=iBadSuffix=0;

   // Get first RollRec
   do {
      pTmp = fgets((char *)&acRollRec[0], MAX_RECSIZE, fdRoll);
   } while (pTmp && !isdigit(acRollRec[0]));

   memset(acBuf, ' ', iRecLen);

   // Merge loop
   while (pTmp)
   {
#ifdef _DEBUG
      //if (!memcmp(&acRollRec[ROFF_TE_NO], "        18", 10))
      //   iRet = 1;
#endif
      // Create new R01 record
      Ker_MergeLienCsv1(acBuf, acRollRec, CREATE_R01);

      if (fdSitus)
         iRet = Ker_MergeSitus2(acBuf);

      if (fdChar)
         iRet = Ker_MergeCVal(acBuf, false);

      if (fdBldg)
         iRet = Ker_MergeBldg(acBuf);


      // Get last recording date
      iRet = atoin((char *)&acBuf[OFF_TRANSFER_DT], SIZ_TRANSFER_DT);
      if (iRet >= lToday)
         LogMsg0("*** WARNING: Bad last recording date %d at record# %d -->%.10s", iRet, lCnt, acBuf);
      else if (lLastRecDate < iRet)
         lLastRecDate = iRet;

      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
      if (!bRet)
      {
         LogMsg("***** Error occurs: %d\n", GetLastError());
         lRet = WRITE_ERR;
         break;
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);

      // Get next roll record
      do {
         // Ignore record without APN
         pTmp = fgets(acRollRec, MAX_RECSIZE, fdRoll);
      } while (pTmp && pRec->Atn[0] == ' ');
   }

   // Close files
   if (fdRoll)
      fclose(fdRoll);
   if (fdSitus)
      fclose(fdSitus);
   if (fdBldg)
      fclose(fdBldg);
   if (fdChar)
      fclose(fdChar);
   if (fhOut)
      CloseHandle(fhOut);

   // Clean up temp file
   if (bClean)
      remove(acTmpFile);

   printf("\n");
   LogMsgD("Total output records:       %u", lCnt);
   LogMsg("Total bad-city records:     %u", iBadCity);
   LogMsg("Total bad-suffix records:   %u\n", iBadSuffix);

   LogMsg("Total Situs matched:        %u", lSitusMatch);
   LogMsg("Total Situs skipped:        %u\n", lSitusSkip);

   LogMsg("Total Char matched:         %u", lCharMatch);
   LogMsg("Total Char skipped:         %u\n", lCharSkip);

   LogMsg("Total Bldg matched:         %u", lBldgMatch);
   LogMsg("Total Bldg skipped:         %u\n", lBldgSkip);

   lRecCnt = lCnt;
   lLDRRecCount = lCnt;
   return 0;
}

/*****************************************************************************
 *
 * This version is to use with RBPR.TXT file (old LDR format).
 * Since 2009 LDR, new file All_Rolls_9GBN2022.TXT and format has been used.
 *
 *****************************************************************************/

int CreateKerRoll(int iFirstRec /* 1=create header rec */)
{
   char     *pTmp, acBuf[MAX_RECSIZE], acRollRec[MAX_RECSIZE];
   char     acOutFile[_MAX_PATH], acTmpFile[_MAX_PATH];

   HANDLE   fhOut;
   DWORD    nBytesWritten;
   BOOL     bRet;
   long     lRet=0, lCnt=0, iRet;

   fdMAdr=fdSitus=fdLegal=fdLien= NULL;
   sprintf(acOutFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "R01");
   sprintf(acTmpFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "UNS");

   // Open Roll file
   LogMsg("Open Roll file %s", acLienFile);
   fdRoll = fopen(acLienFile, "r");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening lien file: %s\n", acLienFile);
      return 2;
   }

   // Open situs file
   if (!_access(acSAdrFile, 0))
   {
      LogMsg("Open Situs file %s", acSAdrFile);
      fdSitus = fopen(acSAdrFile, "r");
      if (fdSitus == NULL)
      {
         LogMsg("***** Error opening Situs file: %s\n", acSAdrFile);
         return 2;
      }
   }

   // Open legal file
   if (!_access(acLegalFile, 0))
   {
      LogMsg("Open legal file %s", acLegalFile);
      fdLegal = fopen(acLegalFile, "r");
      if (fdLegal == NULL)
      {
         LogMsg("***** Error opening Legal file: %s\n", acLegalFile);
         return 2;
      }
   }

   // Open Output file
   LogMsg("Open output file %s", acTmpFile);
   fhOut = CreateFile(acTmpFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error creating output file %s\n", acTmpFile);
      return 4;
   }

   // Create header record
   if (iFirstRec)
   {
      memset(acBuf, '9', iRecLen);
      // Ignore record start with 99999999
      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
   }

   iNoMatch=iBadCity=iBadSuffix=0;

   // Get first RollRec
   pTmp = fgets((char *)&acRollRec[0], MAX_RECSIZE, fdRoll);
   memset(acBuf, ' ', iRecLen);

   // Merge loop
   while (pTmp)
   {
#ifdef _DEBUG
      //if (!memcmp(&acRollRec[ROFF_TE_NO], "        18", 10))
      //   iRet = 1;
#endif

      // Create new R01 record
      Ker_MergeRoll(acBuf, acRollRec, iRollLen, CREATE_R01|CREATE_LIEN);

      if (fdSitus)
         iRet = Ker_MergeSitus(acBuf, acRollRec);

      if (fdLegal)
         iRet = Ker_MergeLegal(acBuf, acRollRec);

      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
      if (!bRet)
      {
         LogMsg("***** Error occurs: %d\n", GetLastError());
         lRet = WRITE_ERR;
         break;
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);

      // Get next roll record
      do {
         // Ignore record without APN
         pTmp = fgets(acRollRec, MAX_RECSIZE, fdRoll);
      } while (pTmp && acRollRec[0] == ' ');
   }

   // Close files
   if (fdRoll)
      fclose(fdRoll);
   if (fdSitus)
      fclose(fdSitus);
   if (fdLegal)
      fclose(fdLegal);

   if (fhOut)
      CloseHandle(fhOut);

   // Sort output file
   sprintf(acBuf, "S(1,11,C,A) F(FIX,%d) B(%d,R)", iRecLen, iRecLen);
   lRet = sortFile(acTmpFile, acOutFile, acBuf);

   // Clean up temp file
   if (bClean)
      remove(acTmpFile);

   LogMsg("Total output records:       %u\n", lCnt);

   //LogMsg("Total Mail matched:         %u", lMAdrMatch);
   //LogMsg("Total Mail skipped:         %u\n", lMAdrSkip);

   LogMsg("Total Situs matched:        %u", lSitusMatch);
   LogMsg("Total Situs skipped:        %u\n", lSitusSkip);

   LogMsg("Total Legal matched:        %u", lLegalMatch);
   LogMsg("Total Legal skipped:        %u\n", lLegalSkip);

   LogMsg("Total bad-city records:     %u", iBadCity);
   LogMsg("Total bad-suffix records:   %u\n", iBadSuffix);

   printf("\nTotal output records: %u\n", lCnt);

   lRecCnt = lCnt;
   return 0;
}

/******************************** Ker_UpdateChar ******************************
 *
 * Ker_UpdateChar() is used for old KER_CHAR format using Ker_Char.dat.
 * Ker_UpdateChar2() is used for new format using Ker_Bldg.dat and Ker_Char.dat
 * Return 0 if successful, < 0 if error
 *
 ******************************************************************************/

int Ker_UpdateChar2(int iSkip)
{
   char     acBuf[MAX_RECSIZE], acRawFile[_MAX_PATH], acOutFile[_MAX_PATH];
   HANDLE   fhIn, fhOut;

   int      iRet;
   DWORD    nBytesRead;
   DWORD    nBytesWritten;
   BOOL     bRet;
   long     lRet=0, lCnt=0;

   LogMsg0("Update CHAR: %s", myCounty.acCntyCode);
   sprintf(acRawFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "R01");
   sprintf(acOutFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "TMP");

   // Open Char file
   LogMsg("Open Char file %s", acCChrFile);
   fdChar = fopen(acCChrFile, "r");
   if (fdChar == NULL)
   {
      LogMsg("***** Error opening Char file: %s\n", acCChrFile);
      return -2;
   }

   // Open Bldg file
   LogMsg("Open Bldg file %s", acCBldgFile);
   fdBldg = fopen(acCBldgFile, "r");
   if (fdBldg == NULL)
   {
      LogMsg("***** Error opening Bldg file: %s\n", acCBldgFile);
      return -2;
   }

   // Open Input file
   LogMsg("Open input file %s", acRawFile);
   fhIn = CreateFile(acRawFile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhIn == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening input file for Char update %s\n", acRawFile);
      return -3;
   }

   // Open Output file
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error creating output file for Char update %s\n", acOutFile);
      return -4;
   }

   // Copy skip record
   memset(acBuf, ' ', iRecLen);
   while (iSkip-- > 0)
   {
      ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);
      // Ignore record start with 99999999
      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
   }

   // Merge loop
   while (bRet)
   {
      bRet = ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);

      // Check for EOF
      if (!bRet)
      {
         LogMsg("***** Error reading input file %s (%f)", acRawFile, GetLastError());
         break;
      }

      // EOF ?
      if (iRecLen != nBytesRead)
         break;

#ifdef _DEBUG
      //if (!memcmp(acBuf, "004020015", 9))
      //   iRet = 1;
#endif

      // Merge Char
      //if (fdChar)
      //   iRet = Ker_MergeChar(acBuf);
      if (fdChar)
         iRet = Ker_MergeCVal(acBuf);

      if (fdBldg)
         iRet = Ker_MergeBldg(acBuf);

      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);

      if (!bRet)
      {
         LogMsg("***** Error occurs: %d\n", GetLastError());
         lRet = WRITE_ERR;
         break;
      }
   }

   // Close files
   if (fdChar)
      fclose(fdChar);
   if (fdBldg)
      fclose(fdBldg);

   if (fhOut)
      CloseHandle(fhOut);
   if (fhIn)
      CloseHandle(fhIn);

   // Rename if success
   if (!lRet)
   {
      LogMsg("Renaming output file");

      // Rename input file to .SAV
      if (bClean)
         remove(acRawFile);
      else
      {
         sprintf(acBuf, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "sav");
         if (!_access(acBuf, 0))
            remove(acBuf);
         rename(acRawFile, acBuf);
      }
      // Rename output file to .R01
      rename(acOutFile, acRawFile);

      LogMsg("Update CHAR complete\n");
   }

   LogMsg("Total output records:       %u", lCnt);
   LogMsg("Total Char matched:         %u", lCharMatch);
   LogMsg("Total Char skipped:         %u\n", lCharSkip);

   return 0;
}

int Ker_UpdateChar(int iSkip)
{
   char     acBuf[MAX_RECSIZE], acRawFile[_MAX_PATH], acOutFile[_MAX_PATH];
   HANDLE   fhIn, fhOut;

   int      iRet;
   DWORD    nBytesRead;
   DWORD    nBytesWritten;
   BOOL     bRet;
   long     lRet=0, lCnt=0;

   LogMsg0("Update CHAR file: %s", acCChrFile);
   sprintf(acRawFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "R01");
   sprintf(acOutFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "TMP");

   // Open Char file
   LogMsg("Open Char file %s", acCChrFile);
   fdChar = fopen(acCChrFile, "r");
   if (fdChar == NULL)
   {
      LogMsg("***** Error opening Char file: %s\n", acCChrFile);
      return -2;
   }

   // Open Input file
   LogMsg("Open input file %s", acRawFile);
   fhIn = CreateFile(acRawFile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhIn == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening input file for Char update %s\n", acRawFile);
      return -3;
   }

   // Open Output file
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error creating output file for Char update %s\n", acOutFile);
      return -4;
   }

   // Copy skip record
   memset(acBuf, ' ', iRecLen);
   while (iSkip-- > 0)
   {
      ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);
      // Ignore record start with 99999999
      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
   }

   // Merge loop
   while (bRet)
   {
      bRet = ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);

      // Check for EOF
      if (!bRet)
      {
         LogMsg("***** Error reading input file %s (%f)", acRawFile, GetLastError());
         break;
      }

      // EOF ?
      if (iRecLen != nBytesRead)
         break;

#ifdef _DEBUG
      //if (!memcmp(acBuf, "004020015", 9))
      //   iRet = 1;
#endif

      // Merge Char
      if (fdChar)
         iRet = Ker_MergeChar(acBuf);

      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);

      if (!bRet)
      {
         LogMsg("***** Error occurs: %d\n", GetLastError());
         lRet = WRITE_ERR;
         break;
      }
   }

   // Close files
   if (fdChar)
      fclose(fdChar);

   if (fhOut)
      CloseHandle(fhOut);
   if (fhIn)
      CloseHandle(fhIn);

   // Rename if success
   if (!lRet)
   {
      LogMsg("Renaming output file");

      // Rename input file to .SAV
      if (bClean)
         remove(acRawFile);
      else
      {
         sprintf(acBuf, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "sav");
         if (!_access(acBuf, 0))
            remove(acBuf);
         rename(acRawFile, acBuf);
      }
      // Rename output file to .R01
      rename(acOutFile, acRawFile);

      LogMsg("Update CHAR complete\n");
   }

   LogMsg("Total output records:       %u", lCnt);
   LogMsg("Total Char matched:         %u", lCharMatch);
   LogMsg("Total Char skipped:         %u\n", lCharSkip);

   return 0;
}

/****************************** Ker_FormatCSale ******************************
 *
 * Return -1 if bad record.  0 if OK.
 *
 *****************************************************************************/

int Ker_FormatCSale(char *pFmtSale, char *pSaleRec)
{
   KER_SALE  *pSale = (KER_SALE *)pSaleRec;
   SCSAL_REC *pHSale= (SCSAL_REC *)pFmtSale;
   char     acTmp[256], *pTmp;
   long     lTmp;

   // Inititalize
   memset(pFmtSale, ' ', sizeof(SCSAL_REC));

   // APN
   if (pSale->Apn[0] < '0' || pSale->Rec_Date[0] < '0' 
      || pSale->Rec_Docno[0] < '0' || pSale->Rec_Docno[0] == 'R')
      return -1;

   // Populate OtherApn with TE_NO for matching with roll
   // this APN will be replaced with ATN after matching with roll file
   lTmp = atoin(pSale->Te_No, RSIZ_TE_NO);
   sprintf(acTmp, "%*u", RSIZ_TE_NO, lTmp);
   memcpy(pHSale->OtherApn, acTmp, RSIZ_TE_NO);

   // Store APN in APN
   memcpy(pHSale->Apn, pSale->Apn, SSIZ_APN);

   // SaleDate
   memcpy(pHSale->DocDate, pSale->Rec_Date, 8);
   lTmp = atoin(pSale->Rec_Date, 8);
   if (lLastRecDate < lTmp)
      lLastRecDate = lTmp;

   // DocNum
   memcpy(pHSale->DocNum, pSale->Rec_Docno, SSIZ_REC_DOCNO);

   // DocType - take critical one only, revisit other later
   lTmp = atoin(pSale->Doc_Type, SSIZ_DOC_TYPE);
   switch (lTmp)
   {
      case 1:     // Grant deed
         pHSale->DocType[0] = '1';
         break;
      case 3:     // Quit claim
         pHSale->DocType[0] = '4';
         pHSale->NoneSale_Flg = 'Y';
         break;
      case 4:     // Tax deed
         memcpy(pHSale->DocType, "67", 2);
         break;
      case 131:     // Trustee's deed
         memcpy(pHSale->DocType, "27", 2);
         break;
   }

   // SalePrice
   double dTax = atofn(pSale->Doctax, SSIZ_DOCTAX);

   // When DocTax = 1.0, PCOR value is an estimate value without sale transaction
   lTmp = 0;
   if (dTax > 1.0)
   {
      lTmp = (long)(dTax*SALE_FACTOR);
      // Sale code - new format no longer carries sale code
      if (pSale->Full_Part == 'F')
         pHSale->SaleCode[0] = pSale->Full_Part;
   } 
   //else if (dTax != 1.0)
   //   lTmp = atoin(pSale->Doc_Value, SSIZ_DOC_VALUE);
   //else
   //   lTmp = atoin(pSale->Pcor_Amt, SSIZ_PCOR_AMT);

   if (lTmp > 0)
   {
      sprintf(acTmp, "%*u", SALE_SIZ_SALEPRICE, lTmp);
      memcpy(pHSale->SalePrice, acTmp, SALE_SIZ_SALEPRICE);
   }

   // Transferor
   memcpy(acTmp, pSale->Transferor, SSIZ_TRANSFEROR);
   acTmp[SSIZ_TRANSFEROR] = 0;
   if (pTmp = strchr(acTmp, 0x9B))
      if (*(pTmp-1) == ' ' && *(pTmp+1) == ' ')
         *pTmp = '&';
   memcpy(pHSale->Seller1, acTmp, SSIZ_TRANSFEROR);

   // Transferee
   memcpy(pHSale->Name1, pSale->Transferee, SSIZ_TRANSFEREE);
   if (pSale->Multi_Transferees == 'Y')
      pHSale->Etal = 'Y';

   // Multi parcels sale
   if (pSale->Multi_Parcel_Sale == 'Y')
      pHSale->MultiSale_Flg = pSale->Multi_Parcel_Sale;

   pHSale->CRLF[0] = '\n';
   pHSale->CRLF[1] = 0;
   return 0;
}

/****************************** Ker_CreateSaleHist ***************************
 *
 * Create history sale.  This function adds new sale record to cumulative file.
 *
 * Return 0 if successful, otherwise error
 *
 *****************************************************************************/

int Ker_CreateSaleHist(void)
{
   char     *pTmp, acCSalRec[1024], acSaleRec[1024], acOutFile[_MAX_PATH];
   int      iRet;
   long     lCnt=0, iUpdateSale=0;

   // Check current sale file
   if (_access(acSaleFile, 0))
   {
      LogMsg("***** Missing current sale file %s", acSaleFile);
      return -1;
   }

   // Open current sale
   LogMsg("Open current sale file %s", acSaleFile);
   fdSale = fopen(acSaleFile, "r");
   if (fdSale == NULL)
   {
      LogMsg("***** Error opening current sale file: %s\n", acSaleFile);
      return -2;
   }

   // Create output file
   sprintf(acOutFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "DAT");
   LogMsg("Create updated sale file %s", acOutFile);
   fdCSale = fopen(acOutFile, "w");
   if (fdCSale == NULL)
   {
      LogMsg("***** Error creating updated sale file: %s\n", acOutFile);
      return -3;
   }

   // Read first history sale
#ifdef _DEBUG
   KER_SALE  *pCurSale = (KER_SALE *)&acSaleRec[0];
   SCSAL_REC *pCumSale = (SCSAL_REC *)&acCSalRec[0];
#endif

   // Merge loop
   while (!feof(fdSale))
   {
      // Get current sale
      pTmp = fgets(acSaleRec, 1024, fdSale);
      if (!pTmp)
         break;

      // Format cumsale record
      iRet = Ker_FormatCSale(acCSalRec, acSaleRec);
      if (!iRet)
      {
         // Output current sale record
         fputs(acCSalRec, fdCSale);
         iUpdateSale++;
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdCSale)
      fclose(fdCSale);
   if (fdSale)
      fclose(fdSale);

   LogMsg("Total sale records processed: %u", lCnt);
   LogMsg("Total sale records extracted: %u\n", iUpdateSale);

   // Update sale history file
   sprintf(acCSalFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "sls");
   sprintf(acOutFile,  acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "dat");

   strcpy(acSaleFile, acOutFile);
   if (!_access(acCSalFile, 0))
   {
      sprintf(acSaleRec, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "srt");
      sprintf(acCSalRec, "%s+%s", acOutFile, acCSalFile);

      // Merge sale files - APN (asc), Saledate (asc), Sale Price (desc)
      iUpdateSale = sortFile(acCSalRec, acSaleRec, "S(1,12,C,A,27,8,C,A,57,10,C,D) DUPO(1,34)");
      if (iUpdateSale > 0)
      {
         // Rename existing SLS file to SAV
         sprintf(acCSalRec, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "sav");
         if (!_access(acCSalRec, 0))
            DeleteFile(acCSalRec);
         iRet = rename(acCSalFile, acCSalRec);
   
         // Rename SRT to SLS file
         rename(acSaleRec, acCSalFile);

         LogMsg("Total sale records output: %u", iUpdateSale);
      }
   } else
   {
      rename(acOutFile, acCSalFile);
   }

   /* 9/12/2013
   // Save old sale file
   sprintf(acCSalFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "SLS");
   if (!_access(acCSalFile, 0))
   {
      sprintf(acSaleRec, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "sav");
      if (!_access(acSaleRec, 0))
         DeleteFile(acSaleRec);
      iRet = rename(acCSalFile, acSaleRec);
   }

   // Don't ned to resort since it's already in TE_NO order
   iRet = rename(acOutFile, acCSalFile);
   if (iRet)
      LogMsg("***** Error renaming %s to %s", acOutFile, acCSalFile);

   // Sort sale file - APN (asc), Saledate (asc), DocNum (asc), SalePrice (desc)
   //sprintf(acSaleRec, "S(1,12,C,A,27,8,C,A,15,12,C,A,57,10,C,D) DUPO(1,34)");
   //iRet = sortFile(acOutFile, acCSalFile, acSaleRec);
   */

   //LogMsg("Total cumulative sale records:    %u", iRet);

   LogMsg("Update Sale History completed.");

   return 0;
}

/* Remove 11/11/2016 ****************************************************************************

int Ker_ExtrRoll(void)
{
   KER_LIEN LienRec;
   char     acMdbFile[_MAX_PATH];
   char     acTmp[256];

   hlAdo    hLien;
   hlAdoRs  rsLien;

   FILE     *fdOut;

   long     lCnt=0;
   CString  strFld;

   LogMsg("Creating Roll export file for %s", myCounty.acCntyCode);

   // Get db provider
   GetIniString(sCnty, "MdbProvider", "", acMdbFile, 256, acIniFile);

   LogMsg("Open roll table %s", acMdbFile);
   sprintf(acTmp, "SELECT * FROM Ker_2010 ORDER BY [TE NO]");
   try
   {
      bool bRet = hLien.Connect(acMdbFile);
      if (bRet)
      {
         LogMsg("%s", acTmp);
         rsLien.Open(hLien, acTmp);
         rsLien.next();             // Set to begining of data set
      }
   } catch(_com_error &e)
   {
      LogMsg("***** Error open %s db: %s", acMdbFile, ComError(e));
      return -2;
   }

   // Open Output file
   LogMsg("Open output file %s", acLienFile);
   fdOut = fopen(acLienFile, "w");
   if (fdOut == NULL)
   {
      printf("Error creating lien output file: %s\n", acLienFile);
      return -2;
   }

   // Loop through record set
   while (rsLien.next())
   {
      memset((void *)&LienRec, ' ', sizeof(KER_LIEN));

#ifdef _DEBUG
      //if (!memcmp(LienRec.Apn, "0346510468", 10))
      //   iTmp = 0;
#endif
      
      strFld = rsLien.GetItem("TE NO");
      memcpy(LienRec.Te_No, strFld, strFld.GetLength());
      strFld = rsLien.GetItem("ASSESSEE NO");
      memcpy(LienRec.Assessee_No, strFld, strFld.GetLength());
      strFld = rsLien.GetItem("TRA NO");
      memcpy(LienRec.TRA, strFld, strFld.GetLength());
      strFld = rsLien.GetItem("ATN");
      memcpy(LienRec.Atn, strFld, strFld.GetLength());


      strFld = rsLien.GetItem("FILE NO");
      memcpy(LienRec.FileNo, strFld, strFld.GetLength());
      strFld = rsLien.GetItem("USE CODE");
      memcpy(LienRec.UseCode, strFld, strFld.GetLength());
      strFld = rsLien.GetItem("EXMPT TYPE CODE");
      memcpy(LienRec.Exmpt_Type_Code, strFld, strFld.GetLength());

      strFld = rsLien.GetItem("ASSESSEE NAME");
      memcpy(LienRec.OwnerName, strFld, strFld.GetLength());
      strFld = rsLien.GetItem("OWNER ADDR L1");
      memcpy(LienRec.M_Addr1, strFld, strFld.GetLength());
      strFld = rsLien.GetItem("OWNER ADDR L2");
      memcpy(LienRec.M_Addr2, strFld, strFld.GetLength());
      strFld = rsLien.GetItem("ZIP CODE");
      memcpy(LienRec.M_Zip, strFld, strFld.GetLength());
      strFld = rsLien.GetItem("LAND ACRES");
      memcpy(LienRec.Land_Acres, strFld, strFld.GetLength());
      strFld = rsLien.GetItem("LAND PEST ACRES");
      memcpy(LienRec.Land_Pet_Acres, strFld, strFld.GetLength());
      strFld = rsLien.GetItem("MINERAL VALUE");
      memcpy(LienRec.Mineral, strFld, strFld.GetLength());
      strFld = rsLien.GetItem("LAND VALUE");
      memcpy(LienRec.Land, strFld, strFld.GetLength());
      strFld = rsLien.GetItem("IMP VALUE");
      memcpy(LienRec.Impr, strFld, strFld.GetLength());
      strFld = rsLien.GetItem("OTHER IMP VALUE");
      memcpy(LienRec.Fixt, strFld, strFld.GetLength());
      strFld = rsLien.GetItem("PER PROP VALUE");
      memcpy(LienRec.PP_Val, strFld, strFld.GetLength());
      strFld = rsLien.GetItem("EXMPT VALUE");
      memcpy(LienRec.Exe_Amt, strFld, strFld.GetLength());
      strFld = rsLien.GetItem("NET VALUE");
      memcpy(LienRec.Net_Val, strFld, strFld.GetLength());
      strFld = rsLien.GetItem("ROLL TYPE CODE");
      memcpy(LienRec.RollType, strFld, strFld.GetLength());
      strFld = rsLien.GetItem("SUPERV DIST NO");
      memcpy(LienRec.Superv_Dist_No, strFld, strFld.GetLength());
      strFld = rsLien.GetItem("SITUS ADDR");
      memcpy(LienRec.Situs, strFld, strFld.GetLength());
      strFld = rsLien.GetItem("LEGAL DESCRIPT");

      int iTmp = blankRem(strFld);
      strFld.Replace(" , ", ", ");

      memcpy(LienRec.Legal, strFld, strFld.GetLength());
      strFld = rsLien.GetItem("IN CARE OF NAME");
      memcpy(LienRec.CareOf, strFld, strFld.GetLength());
      strFld = rsLien.GetItem("DBA NAME");
      memcpy(LienRec.Dba, strFld, strFld.GetLength());
      strFld = rsLien.GetItem("AG PRESERVE");
      memcpy(LienRec.AgPreserve, strFld, strFld.GetLength());
      strFld = rsLien.GetItem("OWNER ADDR L3");
      memcpy(LienRec.M_Addr3, strFld, strFld.GetLength());
      LienRec.CrLf[0] = 10;
      LienRec.CrLf[1] = 0;

      fputs((char *)&LienRec,fdOut);
      

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   rsLien.Close();
   fclose(fdOut);

   // Sort output file
   LogMsg("Total processed records: %u\n", lCnt);
   return 0;
}
*/

/********************************* Ker_GetCVal ******************************
 *
 * Populate CHAR record with values.
 *
 ****************************************************************************

int Ker_GetCVal(char *pOutbuf, char *pCharID, FILE *fd)
{
   static   char acRec[512], *pRec=NULL;
   //char     acTmp[256], *pTmp;
   int      iRet, iLoop;
   KER_CVAL *pChar = (KER_CVAL *)pOutbuf;

   // Get first Char rec for first call
   if (!pRec)
   {
      pRec = fgets(acRec, 512, fd);
   }

   do
   {
      if (!pRec)
         return -1;      // EOF

      // Compare ID
      iLoop = memcmp(pCharID, pRec, strlen(pCharID));
      if (iLoop > 0)
      {
         if (bDebug)
            LogMsg0("Skip Char rec  %s", pRec);
         pRec = fgets(acRec, 512, fd);
      }
   } while (iLoop > 0);

   // If not match, return
   if (iLoop)
      return 1;

   while (!iLoop)
   {
      // Parse record and collect value
      iRet = ParseString(acRec, '|', 4, apTokens);


      // Get new record
      pRec = fgets(acRec, 512, fd);
      iLoop = memcmp(pCharID, pRec, strlen(pCharID));
   }

   return 0;
}

/********************************* Ker_ExtrChar *****************************
 *
 * Sort both Characteristics  and CharacteristicsValue file then merge them.
 * Sort output file on TaxEntityID.
 *
 ****************************************************************************

int Ker_ExtrChar()
{
   char  *pTmp, sCharNdx[_MAX_PATH], sCharVal[_MAX_PATH], sOutFile[_MAX_PATH],
         *pID, sInbuf[256], sOutbuf[2048];
   int   iRet, iCnt=0;
   FILE  *fdCharNdx, *fdCharVal, *fdOut;
   KER_CVAL *pChar = (KER_CVAL *)&sOutbuf[0];

   GetIniString(sCnty, "CharNdx", "", sCharNdx, _MAX_PATH, acIniFile);
   GetIniString(sCnty, "CharVal", "", sCharVal, _MAX_PATH, acIniFile);
   sprintf(sOutFile, "%s\\KER\\Ker_Char.tmp", acTmpPath);

   // Open Char file
   LogMsg("Open Characteristics file %s", sCharNdx);
   if (!(fdCharNdx = fopen(sCharNdx, "r")))
   {
      LogMsg("***** Error opening Characteristics file: %s\n", sCharNdx);
      return -1;
   }

   LogMsg("Open Char Value file %s", sCharVal);
   if (!(fdCharVal = fopen(sCharVal, "r")))
   {
      LogMsg("***** Error opening Char Value file: %s\n", sCharVal);
      return -2;
   }

   // Open output file
   if (!(fdOut = fopen(sOutFile, "w")))
   {
      LogMsg("***** Error creating output file %s", sOutFile);
      return -3;
   }

   // Skip header for both input files
   pTmp = fgets(sInbuf, 256, fdCharNdx);
   pTmp = fgets(sInbuf, 256, fdCharVal);

   while (!feof(fdCharNdx))
   {
      // Get next rec
      if (!(pTmp = fgets(sInbuf, 256, fdCharNdx)))
         break;

      // Collect EntityID, ParcelID, CharacteristicsID
      iRet = ParseString(pTmp, '|', 4, apTokens);
      memcpy(pChar->Entity_No, apTokens[2], strlen(apTokens[2]));
      pID = apTokens[0];
      memcpy(pChar->Atn, pID, strlen(pID));

      // Look for char values
      iRet = Ker_GetCVal(sOutbuf, pID, fdCharVal);
      if (iRet < 0)
         break;

      pChar->CRLF[0] = '\n';
      pChar->CRLF[1] = '\0';
      fputs(sOutbuf, fdOut);

      if (!(++iCnt % 1000))
         printf("\r%u", iCnt);
   }

   fclose(fdOut);
   fclose(fdCharVal);
   fclose(fdCharNdx);

   // Sort output file

   return iRet;
}
*/

/******************************** findIdxValue ******************************
 *
 * Return offset of data field, -1 if not found
 *
 ****************************************************************************/

FLD_IDX *findIdxValue(char *pFldName, FLD_IDX *pFldTbl)
{
   FLD_IDX *pRet=NULL;

   if (pFldName[0] >= '0')
   {
      while (pFldTbl->iNameLen > 0)
      {
         if (!_memicmp(pFldName, pFldTbl->pName, pFldTbl->iNameLen))
         {
            pRet = pFldTbl;
            break;
         }

         pFldTbl++; 
      }
   }

   return pRet;
}

/********************************* Ker_GetCVal ******************************
 *
 * Populate CHAR record with values.
 *
 * Return 1 if value assigned, 0 otherwise
 *
 ****************************************************************************/

int Ker_GetCVal(char *pOutbuf, char *pName, char *pValue, FLD_IDX *pFldTbl)
{
   char     acTmp[256], *pTmp;
   int      iRet=0, iTmp, lTmp;
   double   dTmp;
   KER_CVAL *pChar = (KER_CVAL *)pOutbuf;
   FLD_IDX  *pFldRet;

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "46415011008", 11))
   //   iRet = 0;
#endif
   pFldRet = findIdxValue(pName, pFldTbl);
   if (pFldRet)
   {
      switch (pFldRet->iType)
      {
         case 1:     // LJ
            lTmp = atol(pValue);
            if (lTmp > 0)
            {
               iTmp = sprintf(acTmp, "%d", lTmp);
               memcpy(pOutbuf+pFldRet->iOffset, acTmp, iTmp);
            }
            break;
         case 2:     // RJ
            lTmp = atol(pValue);
            if (lTmp > 0)
            {
               iTmp = sprintf(acTmp, "%*u", pFldRet->iValueLen, lTmp);
               memcpy(pOutbuf+pFldRet->iOffset, acTmp, iTmp);
            }
            break;
         case 3:     // Date - formatted to yyyymmdd
            if (*pValue > ' ')
            {
               pTmp = dateConversion(pValue, acTmp, MMM_DD_YYYY);
               if (pTmp)
                  memcpy(pOutbuf+pFldRet->iOffset, acTmp, strlen(acTmp));
            }
            break;
         case 4:
            dTmp = atof(pValue);
            if (dTmp > 0.0)
            {
               iTmp = sprintf(acTmp, "%d", (long)(1000*dTmp));
               memcpy(pOutbuf+pFldRet->iOffset, acTmp, iTmp);
            }
            break;
         default:    // as is
            iTmp = strlen(pValue);
            if (iTmp > pFldRet->iValueLen)
            {
               LogMsg("*** Field: %s [%s] APN=%.*s", pFldRet->pName, pValue, iApnLen, pOutbuf);
               LogMsg("*** Field value is greater than output buffer (%d > %d).  Please review and make change as needed.", iTmp, pFldRet->iValueLen);
               iTmp = pFldRet->iValueLen;
            }
            memcpy(pOutbuf+pFldRet->iOffset, pValue, iTmp);
            break;
      }
      iRet = 1;
   } 

   return iRet;
}

/********************************* Ker_ExtrCVal *****************************
 *
 * Extract CHAR data from Ker_CharVal.txt
 * Return number of records extracted
 *
 ****************************************************************************/

int Ker_ExtrCVal(char *pInfile, char *pOutfile)
{
   char  *pTmp, sInbuf[4096], sOutbuf[2048], sTmpFile[_MAX_PATH];
   int   iRet, iCnt=0;
   FILE  *fdCharVal, *fdOut;
   KER_CVAL *pChar = (KER_CVAL *)&sOutbuf[0];

   // Open Char file
   LogMsg("Open Char Value file %s", pInfile);
   if (!(fdCharVal = fopen(pInfile, "r")))
   {
      LogMsg("***** Error opening Char Value file: %s\n", pInfile);
      return -2;
   }

   // Open output file
   sprintf(sTmpFile, "%s\\KER\\Ker_Char.tmp", acTmpPath);
   LogMsg("Create Char file %s", sTmpFile);
   if (!(fdOut = fopen(sTmpFile, "w")))
   {
      LogMsg("***** Error creating output file %s", sTmpFile);
      return -3;
   }

   // Skip header
   fgets(sInbuf, 1024, fdCharVal);
   fgets(sInbuf, 1024, fdCharVal);

   pChar->Atn[0] = 0;
   while (!feof(fdCharVal))
   {
      // Get next rec
      if (!(pTmp = fgets(sInbuf, 4096, fdCharVal)))
         break;

      //if (iCnt == 17)
      //   iRet = 0;
      iRet = ParseString(sInbuf, '|', MAX_FLD_TOKEN, apTokens);
      if (iRet < CVAL_FLDVAL)
      {
         LogMsg("Output last record: %.12s", pChar->Atn);
         pChar->CRLF[0] = '\n';
         pChar->CRLF[1] = '\0';
         fputs(sOutbuf, fdOut);
         iCnt++;
         break;
      }

      if (memcmp(pChar->Atn, apTokens[CVAL_ATN], iApnLen) || memcmp(pChar->CharID, apTokens[CVAL_CHARID], strlen(apTokens[CVAL_CHARID])))
      {
         // Save current record
         if (pChar->Atn[0] > ' ')
         {
            pChar->CRLF[0] = '\n';
            pChar->CRLF[1] = '\0';
            fputs(sOutbuf, fdOut);
            iCnt++;
         }

         // Setup new record
         memset(sOutbuf, ' ', sizeof(KER_CVAL));
         memcpy(pChar->Atn, apTokens[CVAL_ATN], strlen(apTokens[CVAL_ATN]));
         memcpy(pChar->Te_No, apTokens[CVAL_TE_NO], strlen(apTokens[CVAL_TE_NO]));
         memcpy(pChar->CharID, apTokens[CVAL_CHARID], strlen(apTokens[CVAL_CHARID]));
      }

#ifdef _DEBUG
      //if (!memcmp(apTokens[0], "49107001004", 11))
      //   iRet = 0;
#endif

      // Look for char values
      iRet = Ker_GetCVal(sOutbuf, apTokens[CVAL_FLDNAME], apTokens[CVAL_FLDVAL], (FLD_IDX *)&tblCharVal[0]);

      if (!(iCnt % 1000))
         printf("\r%u", iCnt);
   }

   fclose(fdOut);
   fclose(fdCharVal);
   LogMsg("Complete %s with %d outrecs", pInfile, iCnt);

   // Sort output file on APN asc, CharID desc
   iCnt = sortFile(sTmpFile, pOutfile, "S(1,12,C,A,23,8,N,D) F(TXT)");

   return iCnt;
}

int Ker_ExtrBldg(char *pInfile, char *pOutfile)
{
   char  *pTmp, sTmpFile[_MAX_PATH], sInbuf[4096], sOutbuf[2048];
   int   iRet, iCnt=0;
   FILE  *fdCharVal, *fdOut;
   KER_CVAL *pChar = (KER_CVAL *)&sOutbuf[0];


   //iRet = sizeof(KER_CVAL);

   // Open Char file
   LogMsg("Open Building Value file %s", pInfile);
   if (!(fdCharVal = fopen(pInfile, "r")))
   {
      LogMsg("***** Error opening Char Value file: %s\n", pInfile);
      return -2;
   }

   // Open output file
   sprintf(sTmpFile, "%s\\KER\\Ker_Bldg.tmp", acTmpPath);
   LogMsg("Create Bldg file %s", sTmpFile);
   if (!(fdOut = fopen(sTmpFile, "w")))
   {
      LogMsg("***** Error creating output file %s", sTmpFile);
      return -3;
   }

   // Skip header
   fgets(sInbuf, 1024, fdCharVal);
   fgets(sInbuf, 1024, fdCharVal);

   pChar->Atn[0] = 0;
   while (!feof(fdCharVal))
   {
      // Get next rec
      if (!(pTmp = fgets(sInbuf, 4096, fdCharVal)))
         break;

      iRet = ParseString(sInbuf, '|', MAX_FLD_TOKEN, apTokens);
      if (iRet < CVAL_FLDVAL)
      {
         LogMsg("Output last record: %.12s", pChar->Atn);
         pChar->CRLF[0] = '\n';
         pChar->CRLF[1] = '\0';
         fputs(sOutbuf, fdOut);
         iCnt++;
         break;
      }

      if (memcmp(pChar->Atn, apTokens[CVAL_ATN], iApnLen) || memcmp(pChar->CharID, apTokens[CVAL_CHARID], strlen(apTokens[CVAL_CHARID])))
      {
         // Save current record
         if (pChar->Atn[0] > ' ')
         {
            pChar->CRLF[0] = '\n';
            pChar->CRLF[1] = '\0';
            fputs(sOutbuf, fdOut);
            iCnt++;
         }

         // Setup new record
         memset(sOutbuf, ' ', sizeof(KER_CVAL));
         memcpy(pChar->Atn, apTokens[CVAL_ATN], strlen(apTokens[CVAL_ATN]));
         memcpy(pChar->Te_No, apTokens[CVAL_TE_NO], strlen(apTokens[CVAL_TE_NO]));
         memcpy(pChar->CharID, apTokens[CVAL_CHARID], strlen(apTokens[CVAL_CHARID]));
      } else if (!memcmp(apTokens[CVAL_FLDNAME], "tblBuilding.BuildingID", 22)
         && pChar->BldgID[0] > ' '
         && memcmp(apTokens[CVAL_FLDVAL], pChar->BldgID, strlen(apTokens[CVAL_FLDVAL])))    // Different building #
      {
         pChar->CRLF[0] = '\n';
         pChar->CRLF[1] = '\0';
         fputs(sOutbuf, fdOut);
         iCnt++;

         memset(&pChar->Condition[0], ' ', sizeof(KER_CVAL)-VOFF_CONDITION);
         memcpy(pChar->BldgID, apTokens[CVAL_FLDVAL], strlen(apTokens[CVAL_FLDVAL]));
      }

#ifdef _DEBUG
      //if (!memcmp(apTokens[0], "00119214008", 11) && !memicmp(apTokens[CVAL_FLDVAL], "Nov", 3))
      //   iRet = 0;
#endif

      // Look for char values
      iRet = Ker_GetCVal(sOutbuf, apTokens[CVAL_FLDNAME], apTokens[CVAL_FLDVAL], (FLD_IDX *)&tblBldgVal[0]);

      if (!(iCnt % 1000))
         printf("\r%u", iCnt);
   }

   fclose(fdOut);
   fclose(fdCharVal);
   LogMsg("Complete %s with %d outrecs", pInfile, iCnt);

   // Sort output file on APN asc, CharID desc
   iCnt = sortFile(sTmpFile, pOutfile, "S(1,12,C,A,23,8,N,D) F(TXT)");

   return iCnt;
}

/***************************** Ker_FormatCSale2 ******************************
 *
 * Create sale record based on new csv format.
 *
 * Return -1 if bad record.  0 if OK.
 *
 *****************************************************************************/

int Ker_FormatCSale2(char *pFmtSale, char *pSaleRec)
{
   SCSAL_REC *pHSale= (SCSAL_REC *)pFmtSale;
   char      *pTmp, acTmp[256];
   int       lTmp, iRet;

   // Inititalize
   memset(pFmtSale, ' ', sizeof(SCSAL_REC));

   // Parse input
   iRet = ParseString(pSaleRec, '|', SALE_MULTISELLERS+1, apTokens);
   if (iRet < SALE_MULTISELLERS)
   {
      LogMsg("***** Bad sale record: %s", pSaleRec);
      return -1;
   }

   // Store ATN in APN
   memcpy(pHSale->Apn, apTokens[SALE_ATN], strlen(apTokens[SALE_ATN]));

   // Populate OtherApn with TE_NO for matching with roll
   lTmp = atol(apTokens[SALE_TENO]);
   sprintf(acTmp, "%*u", RSIZ_TE_NO, lTmp);
   memcpy(pHSale->OtherApn, acTmp, RSIZ_TE_NO);

   // SaleDate
   pTmp = dateConversion(apTokens[SALE_DOCDATE], acTmp, YYYY_MM_DD);
   if (pTmp)
   {
      memcpy(pHSale->DocDate, acTmp, 8);
      lTmp = atoin(acTmp, 8);
      if (lLastRecDate < lTmp)
         lLastRecDate = lTmp;
   }

   // DocNum
   memcpy(pHSale->DocNum, apTokens[SALE_DOCNUM], strlen(apTokens[SALE_DOCNUM]));

   // DocType - take critical one only, revisit other later
   lTmp = atol(apTokens[SALE_DOCTYPE]);
   if (lTmp > 0)
   {
      switch (lTmp)
      {
         case 1:     // Grant deed
            pHSale->DocType[0] = '1';
            break;
         case 3:     // Quit claim
            pHSale->DocType[0] = '4';
            pHSale->NoneSale_Flg = 'Y';
            break;
         case 4:     // Tax deed
            memcpy(pHSale->DocType, "67", 2);
            break;
         case 131:   // Trustee's deed
            memcpy(pHSale->DocType, "27", 2);
            break;
         case 28:    // Affidavit
         case 138:
            pHSale->DocType[0] = '6';
            pHSale->NoneSale_Flg = 'Y';
            break;
         case 35:    // Decree of Distribution
            memcpy(pHSale->DocType, "79", 2);
            pHSale->NoneSale_Flg = 'Y';
            break;
         case 46:    // Order
            memcpy(pHSale->DocType, "80", 2);
            pHSale->NoneSale_Flg = 'Y';
            break;
         case 120:   // Misc
            memcpy(pHSale->DocType, "74", 2);
            pHSale->NoneSale_Flg = 'Y';
            break;
         case 165:   // Corrective Deed
         case 509:
            pHSale->DocType[0] = '9';
            pHSale->NoneSale_Flg = 'Y';
            break;
         default:    // Others
            memcpy(pHSale->DocType, "19", 2);
            pHSale->NoneSale_Flg = 'Y';
            break;
      }

      // DocCode
      memcpy(pHSale->DocCode, apTokens[SALE_DOCTYPE], strlen(apTokens[SALE_DOCTYPE]));
   }

   // When DocTax = 1.0, PCOR value is an estimate value without sale transaction
   double dTax = atof(apTokens[SALE_DOCTAX]);
   if (dTax > 1.0)
   {
      lTmp = (long)(dTax*SALE_FACTOR);

      // SalePrice
      sprintf(acTmp, "%*u", SALE_SIZ_SALEPRICE, lTmp);
      memcpy(pHSale->SalePrice, acTmp, SALE_SIZ_SALEPRICE);
      memcpy(pHSale->StampAmt, apTokens[SALE_DOCTAX], strlen(apTokens[SALE_DOCTAX]));
   } 

   // Consider
   memcpy(pHSale->ConfirmedSalePrice, apTokens[SALE_SALEPRICE], strlen(apTokens[SALE_SALEPRICE]));

   // Transferor
   replChar(apTokens[SALE_SELLER], 0xF3, '&');
   memcpy(pHSale->Seller1, apTokens[SALE_SELLER], strlen(apTokens[SALE_SELLER]));

   // Transferee
   memcpy(pHSale->Name1, apTokens[SALE_BUYER], strlen(apTokens[SALE_BUYER]));

   if (*apTokens[SALE_MULTIPARCELS] == '1')
      pHSale->MultiSale_Flg = 'Y';

   if (*apTokens[SALE_MULTIBUYERS] == '1')
      pHSale->Etal = 'Y';

   // Percent xfer
   lTmp = atol(apTokens[SALE_PCTXFER]);
   if (lTmp > 0)
   {
      iRet = sprintf(acTmp, "%d", lTmp);
      memcpy(pHSale->PctXfer, acTmp, iRet);
      if (lTmp == 100)
         pHSale->SaleCode[0] = 'F';
      else
         pHSale->SaleCode[0] = 'P';
      pHSale->SaleCode[1] = ' ';
   }

   pHSale->CRLF[0] = '\n';
   pHSale->CRLF[1] = 0;
   return 0;
}

/********************************* Ker_ExtrSale *****************************
 *
 *
 ****************************************************************************/

int Ker_ExtrSale(LPSTR pSaleFile)
{
   char     *pTmp, acCSalRec[1024], acSaleRec[1024], acOutFile[_MAX_PATH];
   int      iRet;
   long     lCnt=0, iUpdateSale=0;

   LogMsg0("Extract sale file: %s", pSaleFile);

   // Check current sale file
   if (_access(pSaleFile, 0))
   {
      LogMsg("***** Missing current sale file %s", pSaleFile);
      return -1;
   }

   // Open current sale
   LogMsg("Open current sale file %s", pSaleFile);
   fdSale = fopen(pSaleFile, "r");
   if (fdSale == NULL)
   {
      LogMsg("***** Error opening current sale file: %s\n", pSaleFile);
      return -2;
   }

   // Create output file
   sprintf(acOutFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "DAT");
   LogMsg("Create updated sale file %s", acOutFile);
   fdCSale = fopen(acOutFile, "w");
   if (fdCSale == NULL)
   {
      LogMsg("***** Error creating updated sale file: %s\n", acOutFile);
      return -3;
   }

   // Read first history sale
   SCSAL_REC *pCumSale = (SCSAL_REC *)&acCSalRec[0];

   // Skip header
   fgets(acSaleRec, 1024, fdSale);
   fgets(acSaleRec, 1024, fdSale);

   // Merge loop
   while (!feof(fdSale))
   {
      // Get current sale
      pTmp = fgets(acSaleRec, 1024, fdSale);
      if (!pTmp || *pTmp < '0')
         break;

      // Format cumsale record
      iRet = Ker_FormatCSale2(acCSalRec, acSaleRec);
      if (!iRet)
      {
         // Output current sale record
         fputs(acCSalRec, fdCSale);
         iUpdateSale++;
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdCSale)
      fclose(fdCSale);
   if (fdSale)
      fclose(fdSale);

   // 09/12/2013
   LogMsg("Total sale records processed: %u", lCnt);
   LogMsg("Total sale records extracted: %u\n", iUpdateSale);

   // Update sale history file
   strcpy(acSaleFile, acOutFile);
   if (!_access(acCSalFile, 0))
   {
      sprintf(acSaleRec, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "srt");
      sprintf(acCSalRec, "%s+%s", acOutFile, acCSalFile);

      // Merge sale files - APN (asc), Saledate (asc), DocNum (asc)
      iUpdateSale = sortFile(acCSalRec, acSaleRec, "S(1,12,C,A,27,8,C,A,57,10,C,D,15,12,C,A) DUPO(1,34)");
      if (iUpdateSale > 0)
      {
         // Rename existing SLS file to SAV
         sprintf(acCSalRec, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "sav");
         if (!_access(acCSalRec, 0))
            DeleteFile(acCSalRec);
         iRet = rename(acCSalFile, acCSalRec);
   
         // Rename SRT to SLS file
         rename(acSaleRec, acCSalFile);

         LogMsg("Total sale records output:    %u", iUpdateSale);
      }
   } else
   {
      rename(acOutFile, acCSalFile);
   }

   /* 09/12/2013
   // Sort sale file - APN (asc), Saledate (asc), DocNum (asc), SalePrice (desc)
   sprintf(acSaleRec, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "srt");
   sprintf(acTmp, "S(1,%d,C,A,27,8,C,A,57,10,C,D,15,%d,C,A) DUPO(1,34)", iApnLen, SALE_SIZ_DOCNUM);
   iRet = sortFile(acOutFile, acSaleRec, acTmp);
   if (iRet > 1000)
   {
      if (!_access(acCSalFile, 0))
      {
         sprintf(acCSalRec, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "sav");
         DeleteFile(acCSalRec);
         iRet = rename(acCSalFile, acCSalRec);
      }

      iRet = rename(acSaleRec, acCSalFile);
      if (iRet)
         LogMsg("***** Error renaming %s to %s", acSaleRec, acCSalFile);
   } else
      iRet = -9;
   */

   LogMsg("Total sale records read:      %u", lCnt);
   LogMsg("Sale Extract completed.");

   return iRet;
}

/******************************* Ker_MakeDocLink *******************************
 *
 * Format DocLink
 *
 ******************************************************************************/

void Ker_MakeDocLink(LPSTR pDocLink, LPSTR pDoc, LPSTR pDate)
{
   char  acTmp[256], acDocName[256];

   *pDocLink = 0;
   if (*pDoc > ' ' && *pDate > ' ')
   {
      sprintf(acDocName, "%.4s\\%.3s\\%s", pDate, pDoc+3, pDoc);
      sprintf(acTmp, "%s\\%s.pdf", acDocPath, acDocName);

      if (!_access(acTmp, 0))
         strcpy(pDocLink, acDocName);
   }
}

/****************************************************************************
 *
 ****************************************************************************/

int Ker_ValidateInput(char *pCnty, bool bNewFmt)
{
   __int64  iTmp;
   int      iRet = 0;

   if (bNewFmt)
   {
      // Sale File
      GetIniString(pCnty, "Ker_Sale", "", acSaleFile, _MAX_PATH, acIniFile);
      iTmp = getFileSize(acSaleFile);
      if (iTmp < 100000)
      {
         iTmp = countLine(acSaleFile);
         if (iTmp < 100000)
         {
            LogMsg("***** Error: Bad sale file %s", acSaleFile);
            iRet = 1;
         }
      }

      // Roll file
      GetIniString(pCnty, "Ker_Roll", "", acRollFile, _MAX_PATH, acIniFile);
      iTmp = getFileSize(acRollFile);
      if (iTmp < 100000)
      {
         iTmp = countLine(acRollFile);
         if (iTmp < 100000)
         {
            LogMsg("***** Error: Bad roll file %s", acRollFile);
            iRet = 2;
         }
      }

      // Value File
      GetIniString(pCnty, "Ker_Value", "", acValueFile, _MAX_PATH, acIniFile);
      iTmp = getFileSize(acValueFile);
      if (iTmp < 100000)
      {
         iTmp = countLine(acValueFile);
         if (iTmp < 100000)
         {
            LogMsg("***** Error: Bad value file %s", acValueFile);
            iRet = 16;
         }
      }

      // CHAR file
      GetIniString(pCnty, "Ker_Char", "", acCharFile, _MAX_PATH, acIniFile);
      iTmp = getFileSize(acCharFile);
      if (iTmp < 100000)
      {
         iTmp = countLine(acCharFile);
         if (iTmp < 100000)
         {
            LogMsg("***** Error: Bad CHAR file %s", acCharFile);
            iRet = -1;
         }
      }

      // Building file
      GetIniString(pCnty, "Ker_Bldg", "", acBldgFile, _MAX_PATH, acIniFile);
      iTmp = getFileSize(acBldgFile);
      if (iTmp < 100000)
      {
         iTmp = countLine(acBldgFile);
         if (iTmp < 100000)
         {
            LogMsg("***** Error: Bad Building file %s", acBldgFile);
            iRet = -2;
         }
      }
   } else
   {
      // Sale File
      GetIniString(pCnty, "SaleFile", "", acSaleFile, _MAX_PATH, acIniFile);
      iTmp = getFileSize(acSaleFile);
      if (iTmp < 100000)
      {
         LogMsg("***** Error: Bad sale file %s", acSaleFile);
         iRet = 1;
      }

      // Roll file
      GetIniString(pCnty, "RollFile", "", acRollFile, _MAX_PATH, acIniFile);
      iTmp = getFileSize(acRollFile);
      if (iTmp < 100000)
      {
         LogMsg("***** Error: Bad roll file %s", acRollFile);
         iRet = 2;
      }

      // Legal File
      GetIniString(pCnty, "LeglFile", "", acLegalFile, _MAX_PATH, acIniFile);
      iTmp = getFileSize(acLegalFile);
      if (iTmp < 100000)
      {
         LogMsg("***** Error: Bad legal file %s", acLegalFile);
         iRet = 16;
      }

      // Building file
      GetIniString(pCnty, "CChrBldg", "", acCBldgFile, _MAX_PATH, acIniFile);
      iTmp = getFileSize(acCBldgFile);
      if (iTmp < 100000)
      {
         LogMsg("***** Error: Bad Building file %s", acCBldgFile);
         iRet = -2;
      }
   }

   // Situs addr
   GetIniString(pCnty, "SAdrFile", "", acSAdrFile, _MAX_PATH, acIniFile);
   iTmp = getFileSize(acSAdrFile);
   if (iTmp < 100000)
   {
      LogMsg("***** Error: Bad situs file %s", acSAdrFile);
      iRet = 4;
   }

   // Mailing addr
   GetIniString(pCnty, "MAdrFile", "", acMAdrFile, _MAX_PATH, acIniFile);
   iTmp = getFileSize(acMAdrFile);
   if (iTmp < 100000)
   {
      LogMsg("***** Error: Bad mailing file %s", acMAdrFile);
      iRet = 8;
   }

   return iRet;
}

/***************************** Ker_ParseTaxDetail ****************************
 *
 * Create Detail and Agency records from Tax Service
 * Return number of items output
 *
 *****************************************************************************/

int Ker_ParseTaxDetail(FILE *fdDetail, FILE *fdAgency, FILE *fdLic, char *pInbuf)
{
   static   char acLineItem[512], *pLineItem;

   char     acTmp[2048], acDetail[512], acAgency[512];
   int      iCmp, iTmp, iIdx, iCnt, iRet=0;
   double   dTotalAmt, dRate, dTmp;
   bool     bFound=false;

   KER_TAXSVC  *pInRec  = (KER_TAXSVC *)pInbuf;
   KER_TXLI    *pLi     = (KER_TXLI *)acLineItem;
   TAXDETAIL   *pDetail = (TAXDETAIL *)acDetail;
   TAXAGENCY   *pAgency = (TAXAGENCY *)acAgency, *pResult;

   // Clear output buffer
   memset(acDetail, 0, sizeof(TAXDETAIL));
   memset(acAgency, 0, sizeof(TAXAGENCY));

   // APN
   memcpy(pDetail->Apn, pInRec->Atn , SSVC_ATN);

   // BillNum
   memcpy(pDetail->BillNum, pInRec->Bill_No, SSVC_BILL_NO);

   // Tax Year
   sprintf(pDetail->TaxYear, "20%.2s", pInRec->Tax_Year);

#ifdef _DEBUG
   //if (!memcmp(pDetail->Apn, "00106411005", 9) )
   //   iTmp = 0;
#endif

   // General tax
   dTotalAmt = Zone2Dbl(pInRec->Gen_Amt_Due, SSVC_REAL_AMOUNT);
   if (dTotalAmt > 0.0)
   {
      while (!feof(fdLic))
      {
         if (!pLineItem)
            pLineItem = fgets(acLineItem, 512, fdLic);
         if (pLineItem)
         {
            iTmp = sprintf(acTmp, "20%.2s-%.7s-%.2s-%c", pInbuf, pInbuf+2, pInbuf+9, *(pInbuf+11));
            iCmp = memcmp(acTmp, acLineItem, iTmp);
            if (!iCmp)
            {
               bFound = true;
               replChar(acLineItem, ';', 0);
               dTmp = atof(pLi->TaxAmt);
               sprintf(pDetail->TaxAmt, "%.2f", dTmp);
               dRate = atof(pLi->TaxRate);
               sprintf(pDetail->TaxRate, "%.6f", dRate);
               strcpy(pDetail->TaxCode, pLi->FundNo);
               strcpy(pAgency->Code, pLi->FundNo);

               // Generate csv line and write to file
               Tax_CreateDetailCsv(acTmp, pDetail);
               fputs(acTmp, fdDetail);

               // Tax Agency
               pResult = findTaxAgency(pAgency->Code);
               if (pResult)
               {
                  strcpy(pAgency->Agency, pResult->Agency);
                  strcpy(pAgency->Phone, pResult->Phone);
               } else
               {
                  strcpy(pAgency->Agency, pAgency->Code);
                  LogMsg0("+++ Unknown AV Tax code: %s [%s][%s]", pAgency->Code, pDetail->Apn, pDetail->BillNum);
               }
               Tax_CreateAgencyCsv(acTmp, pAgency);
               fputs(acTmp, fdAgency);
               iRet++;

               // Get next item - set it to NULL so new record will be read next time
               pLineItem = NULL;
            } else if (iCmp > 0)
            {
               pLineItem = NULL;
            } else
               break;
         }
      }

      // Remove tax rate
      pDetail->TaxRate[0] = 0;
   }

   if (!bFound && dTotalAmt > 0.0)
   {
      LogMsg("No Line Items found for bill# %s [APN=%s]", acTmp, pDetail->Apn);
      // Tax amt
      sprintf(pDetail->TaxAmt, "%.2f", dTotalAmt);

      // Tax code
      strcpy(pDetail->TaxCode, "99998");

      // Generate csv line and write to file
      Tax_CreateDetailCsv(acTmp, pDetail);
      fputs(acTmp, fdDetail);

      // Tax Agency
      strcpy(pAgency->Code, "99998");
      strcpy(pAgency->Agency, "GENERAL TAX");
      Tax_CreateAgencyCsv(acTmp, pAgency);
      fputs(acTmp, fdAgency);
      iRet++;
   }

   // Handle county agency
   iCnt = Zone2Int(pInRec->Spec_Asmt_Number, SSVC_SPEC_ASMT_NUMBER);
   for (iIdx = 0; iIdx < iCnt; iIdx++)
   {
      dTotalAmt = Zone2Dbl(pInRec->Agency[iIdx].Amount1, SSVC_REAL_AMOUNT);
      dTotalAmt += Zone2Dbl(pInRec->Agency[iIdx].Amount2, SSVC_REAL_AMOUNT);
      // 3/16/2017 if (dTotalAmt > 0.0)
      {
         // Tax amt
         sprintf(pDetail->TaxAmt, "%.2f", dTotalAmt);

         // Tax code
         memcpy(pAgency->Code, pInRec->Agency[iIdx].Fund_No, SSVC_SA_FUND_NO);
         memcpy(pDetail->TaxCode, pInRec->Agency[iIdx].Fund_No, SSVC_SA_FUND_NO);

         // Generate csv line and write to file
         Tax_CreateDetailCsv(acTmp, pDetail);
         fputs(acTmp, fdDetail);

         // Tax Agency
         pResult = findTaxAgency(pAgency->Code);
         if (pResult)
         {
            strcpy(pAgency->Agency, pResult->Agency);
            strcpy(pAgency->Phone, pResult->Phone);
         } else
         {
            strcpy(pAgency->Agency, pAgency->Code);
            LogMsg0("+++ Unknown Tax code: %s [%.11s]", pAgency->Code, pDetail->Apn);
         }

         Tax_CreateAgencyCsv(acTmp, pAgency);
         fputs(acTmp, fdAgency);
         iRet++;
      }
   }

   return iRet;
}

/**************************** Ker_Load_TaxDetail ******************************
 *
 * Loading TC01CT02.txt to populate Tax_Items & Tax_Agency
 * Merge with TXLIC.TXT for AV line items
 *
 * Return 0 if success.
 *
 *****************************************************************************/

int Ker_Load_TaxDetail(bool bImport)
{
   char     *pTmp, acItemsRec[MAX_RECSIZE], acAgencyRec[512], acRec[512], acLineItem[512];
   char     acItemsFile[_MAX_PATH], acAgencyFile[_MAX_PATH], acTmpFile[_MAX_PATH], acInFile[_MAX_PATH],
            acLicFile[_MAX_PATH], acLipFile[_MAX_PATH], acTmp[256];
   int      iRet;
   long     lItems=0, lCnt=0, lAgency=0;
   FILE     *fdItems, *fdAgency, *fdIn, *fdLic;
   TAXDETAIL  *pTax    = (TAXDETAIL *)&acItemsRec[0];
   TAXAGENCY  *pAgency = (TAXAGENCY *)&acAgencyRec[0];
   KER_TAXSVC *pTaxSvc = (KER_TAXSVC *)&acRec[0];
   KER_TXLI   *pLItem  = (KER_TXLI *)&acLineItem[0];

   LogMsg0("Loading TaxDetail");

   GetIniString(myCounty.acCntyCode, "TaxDetail", "", acInFile, _MAX_PATH, acIniFile);
   GetIniString(myCounty.acCntyCode, "TaxDetailZip", "", acRec, _MAX_PATH, acIniFile);
   GetIniString(myCounty.acCntyCode, "TaxLic", "", acLicFile, _MAX_PATH, acIniFile);
   GetIniString(myCounty.acCntyCode, "TaxLip", "", acLipFile, _MAX_PATH, acIniFile);

   // If zip file is newer, unzip it
   iRet = chkFileDate(acRec, acInFile);
   if (iRet == 1)
   {
      pTmp = strrchr(acInFile, '\\');
      *pTmp = 0;
      // Unzip then rename the zip file
      iRet = unzipOne(acRec, acInFile, acToday, true);
      *pTmp = '\\';
   } 

   // Sort input file on Tax year, BillNum, FundNo
   sprintf(acRec, "%s\\%s\\TaxSrvc.srt", acTmpPath, myCounty.acCntyCode);
   sprintf(acTmp, "S(1,12,C,A,%d,5,C,A) F(TXT)", OSVC_SA_FUND_NO);
   iRet = sortFile(acInFile, acRec, acTmp);
   if (iRet > 0)
      strcpy(acInFile, acRec);
   else
      return -1;

   // Sort input file on Tax year, BillNum, FundNo
   sprintf(acRec, "%s\\%s\\TaxLic.srt", acTmpPath, myCounty.acCntyCode);
   iRet = sortFile(acLicFile, acRec, "S(#1,C,A,#3,C,A) DEL(59) F(TXT)");
   if (iRet > 0)
      strcpy(acLicFile, acRec);
   else
      return -1;

   // Open input file
   LogMsg("Open Detail file %s", acInFile);
   fdIn = fopen(acInFile, "r");
   if (fdIn == NULL)
   {
      LogMsg("***** Error opening Detail file: %s\n", acInFile);
      return -2;
   }  

   LogMsg("Open current line item file %s", acLicFile);
   fdLic = fopen(acLicFile, "r");
   if (fdLic == NULL)
   {
      LogMsg("***** Error opening current line item file: %s\n", acLicFile);
      return -2;
   }  

   //LogMsg("Open previous line item file %s", acLipFile);
   //fdLip = fopen(acLipFile, "r");
   //if (fdLip == NULL)
   //{
   //   LogMsg("***** Error opening previous line item file: %s\n", acLipFile);
   //   return -2;
   //}  

   // Open Items file
   NameTaxCsvFile(acItemsFile, myCounty.acCntyCode, "Items");
   LogMsg("Create Items file %s", acItemsFile);
   fdItems = fopen(acItemsFile, "w");
   if (fdItems == NULL)
   {
      LogMsg("***** Error creating Items file: %s\n", acItemsFile);
      return -4;
   }

   // Open Agency file
   NameTaxCsvFile(acAgencyFile, myCounty.acCntyCode, "Agency");
   sprintf(acTmpFile, "%s\\%s\\%s_Agency.tmp", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   LogMsg("Create Agency file %s", acTmpFile);
   fdAgency = fopen(acTmpFile, "w");
   if (fdAgency == NULL)
   {
      LogMsg("***** Error creating Agency file: %s\n", acTmpFile);
      return -4;
   }

   // Skip old records
   //sprintf(acTaxYear, "%d", lTaxYear);
   //do
   //{
   //   pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdIn);
   //} while (memcmp(acRec, &acTaxYear[2], 2) < 0);


   // Merge loop 
   while (!feof(fdIn))
   {
      pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdIn);
      if (!pTmp) break;

      // Create Items & Agency record
      //if (!memcmp(pTaxSvc->Tax_Year, &acTaxYear[2], 2))
      {
         iRet = Ker_ParseTaxDetail(fdItems, fdAgency, fdLic, acRec);
         if (iRet > 0)
         {
            lItems++;
         //} else
         //{
         //   LogMsg0("---> No Tax value at %d ATN=%.11s", lCnt, pTaxSvc->Atn); 
         }
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdIn)
      fclose(fdIn);
   if (fdLic)
      fclose(fdLic);
   //if (fdLip)
   //   fclose(fdLip);
   if (fdItems)
      fclose(fdItems);
   if (fdAgency)
      fclose(fdAgency);

   LogMsg("Total records processed:    %u", lCnt);
   LogMsg("Total Items records:        %u", lItems);

   // Dedup Agency file before import
   LogMsg("Dedup Agency file %s to %s", acTmpFile, acAgencyFile);
   iRet = sortFile(acTmpFile, acAgencyFile, "S(#1,C,A) DUPOUT F(TXT)");
   if (iRet < 1)
   {
      LogMsg("***** Bad Agency file.  SKip import");
      bImport = false;
   }

   // Import into SQL
   if (bImport)
   {
      iRet = doTaxImport(myCounty.acCntyCode, TAX_DETAIL);
      if (!iRet)
         iRet = doTaxImport(myCounty.acCntyCode, TAX_AGENCY);
   } else
      iRet = 0;

   return iRet;
}

/***************************** Ker_ParseTaxBase ******************************
 *
 *****************************************************************************/

int Ker_ParseTaxBase(char *pBasebuf, char *pInbuf)
{
   TAXBASE  *pBaseRec = (TAXBASE *)pBasebuf;
   KER_TAX  *pInRec   = (KER_TAX *)pInbuf;
   char      acTmp[256];
   int       iTmp, iIdx;

   // Clear output buffer
   memset(pBaseRec, 0, sizeof(TAXBASE));

   // Do not process unsecured, canceled or to be bill parcels
   if (pInRec->Bill_SubType[0] == 'U')
      return -1;
   else if (pInRec->Bill_Status == 'T')
      return -2;

#ifdef _DEBUG
   //if (!memcmp(pInRec->Atn, "50216005001", TSIZ_ATN))
   //   iTmp = 1;
#endif
   // APN
   memcpy(pBaseRec->Apn, pInRec->Atn, TSIZ_ATN);
   memcpy(pBaseRec->OwnerInfo.Apn, pInRec->Atn, TSIZ_ATN);

   // TRA
   memcpy(pBaseRec->TRA, pInRec->TRA, TSIZ_TRA);

   // Tax Year
   memcpy(pBaseRec->TaxYear, pInRec->TaxYear, TSIZ_TAXYEAR);
   memcpy(pBaseRec->OwnerInfo.TaxYear, pInRec->TaxYear, TSIZ_TAXYEAR);

   // Bill number
   memcpy(pBaseRec->BillNum, pInRec->BillNum, TSIZ_BILL_NUMBER);
   memcpy(pBaseRec->OwnerInfo.BillNum, pInRec->BillNum, TSIZ_BILL_NUMBER);

   // BillType
   if (pInRec->Bill_SubType[0] == 'S')
   {
      switch (pInRec->Bill_SubType[1])
      {
         case ' ':      // SECURED
            pBaseRec->isSecd[0] = '1';
            pBaseRec->BillType[0] = BILLTYPE_SECURED;
            break;
         case 'S':      // SECURED SUPPLEMENTAL
            pBaseRec->isSupp[0] = '1';
            pBaseRec->BillType[0] = BILLTYPE_SECURED_SUPPL;
            break;
         case 'E':      // SECURED ESCAPE
            pBaseRec->isSupp[0] = '1';
            pBaseRec->BillType[0] = BILLTYPE_SECURED_ESCAPE;
            break;
         default:
            pBaseRec->isSecd[0] = '1';
            pBaseRec->BillType[0] = BILLTYPE_SECURED;
      }
   } else   // ROLL CORRECTION
      pBaseRec->BillType[0] = BILLTYPE_ROLL_CORRECTION;


   // Check for Tax amount
   double dTotalTax, dTotalDue, dTax1, dTax2, dPen1, dPen2, dPaid1, dPaid2, dRate;
   dTax1 = atofn(pInRec->DueAmt1, TSIZ_TAX_AMT, true);
   dTax2 = atofn(pInRec->DueAmt2, TSIZ_TAX_AMT, true);
   dPen1 = atofn(pInRec->PenAmt1, TSIZ_TAX_AMT);
   dPen2 = atofn(pInRec->PenAmt2, TSIZ_TAX_AMT);
   dPaid1 = atofn(pInRec->PaidAmt1, TSIZ_TAX_AMT);
   dPaid2 = atofn(pInRec->PaidAmt2, TSIZ_TAX_AMT);
   dTotalTax = atofn(pInRec->TotalTax, TSIZ_TAX_AMT, true);
   
   // Ignore credit bill
   if (dTotalTax <= 0)
      return -3;

   memcpy(pBaseRec->DueDate1, pInRec->DueDate1, TSIZ_DATE);
   memcpy(pBaseRec->DueDate2, pInRec->DueDate2, TSIZ_DATE);

   if (dTotalTax > 0.0)
   {
      sprintf(pBaseRec->TotalTaxAmt, "%.2f", dTotalTax);
      if (dTax1 > 0.0)
         sprintf(pBaseRec->TaxAmt1, "%.2f", dTax1);
      if (dTax2 > 0.0)
         sprintf(pBaseRec->TaxAmt2, "%.2f", dTax2);
   }

   // Paid
   if (dPaid1 > 0.0)
   {
      pBaseRec->Inst1Status[0] = TAX_BSTAT_PAID;
      sprintf(pBaseRec->PaidAmt1, "%.2f", dPaid1);
      memcpy(pBaseRec->PaidDate1, pInRec->PaidDate1, TSIZ_DATE);
      dPen1 = 0;
      dTax1 = 0;
   //} else if (dPen1 > 0.0 && ChkDueDate(1, pInRec->DueDate1))
   } else if (ChkDueDate(1, pInRec->DueDate1))
   {
      if (dPen1 > 0.0)
         sprintf(pBaseRec->PenAmt1, "%.2f", dPen1);
      pBaseRec->Inst1Status[0] = TAX_BSTAT_PASTDUE;
   } else
   {
      dPen1 = 0;
      if (dTax1 > 0)
         pBaseRec->Inst1Status[0] = TAX_BSTAT_UNPAID;
   }

   if (dPaid2 > 0.0)
   {
      pBaseRec->Inst2Status[0] = TAX_BSTAT_PAID;
      sprintf(pBaseRec->PaidAmt2, "%.2f", dPaid2);
      memcpy(pBaseRec->PaidDate2, pInRec->PaidDate2, TSIZ_DATE);
      dPen2 = 0;
      dTax2 = 0;
   //} else if (dPen2 > 0.0 && ChkDueDate(2, pInRec->DueDate2))
   } else if (ChkDueDate(2, pInRec->DueDate2))
   {
      if (dPen2 > 0.0)
         sprintf(pBaseRec->PenAmt2, "%.2f", dPen2);
      pBaseRec->Inst2Status[0] = TAX_BSTAT_PASTDUE;
   } else
   {
      dPen2 = 0;
      if (dTax2 > 0)
         pBaseRec->Inst2Status[0] = TAX_BSTAT_UNPAID;
   }
   // Paid in full
   if (pInRec->Bill_Status == 'P')
      dTotalDue = 0;
   else if (pInRec->Bill_Status == 'R')      // Refund
      dTotalDue = dTax1 + dTax2;
   else if (pInRec->Bill_Status == 'C')
   {
      pBaseRec->Inst1Status[0] = TAX_BSTAT_CANCEL;
      pBaseRec->Inst2Status[0] = TAX_BSTAT_CANCEL;
   } else 
   {
      dTotalDue = dTax1 + dTax2 + dPen1 + dPen2;
      sprintf(pBaseRec->TotalDue, "%.2f", dTotalDue);
   }

   // Tax rate
   dRate = atofn(pInRec->Gen_TaxRate, TSIZ_TAX_RATE);
   dRate += atofn(pInRec->Spcl_TaxRate, TSIZ_TAX_RATE);
   if (dRate > 0.0)
      sprintf(pBaseRec->TotalRate, "%.6f", dRate);

   // Owner record
   memcpy(pBaseRec->OwnerInfo.Name1, pInRec->Assessee, TSIZ_NAME);
   myTrim(pBaseRec->OwnerInfo.Name1, TSIZ_NAME);
   iIdx = 0;
   if (pInRec->M_FmtAddr_Cd == 'F')
   {
      if (pInRec->NameBilling[0] > ' ')
         strcpy(pBaseRec->OwnerInfo.MailAdr[iIdx++], myTrim(pInRec->NameBilling, TSIZ_NAME));
      iTmp = sprintf(acTmp, "%.*s%c %.*s %.*s %.*s %.*s", TSIZ_M_STR_NO, pInRec->M_StrNum, pInRec->M_StrFra[0], TSIZ_M_STR_DIR,  pInRec->M_StrDir,
         TSIZ_M_STR_NAME, pInRec->M_StrName, TSIZ_M_STR_TYPE, pInRec->M_StrSfx, TSIZ_M_UNIT_TYPE+TSIZ_M_UNIT, pInRec->M_UnitType);
      iTmp = blankRem(acTmp, iTmp);
      memcpy(pBaseRec->OwnerInfo.MailAdr[iIdx++], acTmp, iTmp);
      if (pInRec->M_Zip4[0] >= '0')
         iTmp = sprintf(acTmp, "%.*s, %.2s %.5s-%.4s", TSIZ_M_CITY, pInRec->M_City, pInRec->M_State, pInRec->M_Zip, pInRec->M_Zip4);
      else
         iTmp = sprintf(acTmp, "%.*s, %.2s %.5s", TSIZ_M_CITY, pInRec->M_City, pInRec->M_State, pInRec->M_Zip);
      iTmp = blankRem(acTmp, iTmp);
      memcpy(pBaseRec->OwnerInfo.MailAdr[iIdx], acTmp, iTmp);
   } else if (pInRec->M_StrName[0] > ' ')
   {
      if (pInRec->NameBilling[0] > ' ')
         strcpy(pBaseRec->OwnerInfo.MailAdr[iIdx++], myTrim(pInRec->NameBilling, TSIZ_NAME));
      iTmp = sprintf(acTmp, "%.*s%c %.*s %.*s %.*s %.*s", TSIZ_M_STR_NO, pInRec->M_StrNum, pInRec->M_StrFra[0], TSIZ_M_STR_DIR,  pInRec->M_StrDir,
         TSIZ_M_STR_NAME, pInRec->M_StrName, TSIZ_M_STR_TYPE, pInRec->M_StrSfx, TSIZ_M_UNIT_TYPE+TSIZ_M_UNIT, pInRec->M_UnitType);
      iTmp = blankRem(acTmp, iTmp);
      memcpy(pBaseRec->OwnerInfo.MailAdr[iIdx++], acTmp, iTmp);
      if (pInRec->M_Zip4[0] >= '0')
         iTmp = sprintf(acTmp, "%.*s, %.2s %.5s-%.4s", TSIZ_M_CITY, pInRec->M_City, pInRec->M_State, pInRec->M_Zip, pInRec->M_Zip4);
      else
         iTmp = sprintf(acTmp, "%.*s, %.2s %.5s", TSIZ_M_CITY, pInRec->M_City, pInRec->M_State, pInRec->M_Zip);
      iTmp = blankRem(acTmp, iTmp);
      memcpy(pBaseRec->OwnerInfo.MailAdr[iIdx], acTmp, iTmp);
   }

   return 0;
}

/********************************* Ker_UpdateDelq *****************************
 *
 ******************************************************************************/

int Ker_UpdateDelq(char *pOutbuf, FILE *fd)
{
   static   char  acRec[MAX_RECSIZE], *pRec=NULL;
   int      iLoop;

   TAXBASE  *pOutRec   = (TAXBASE *)pOutbuf;
   KER_SECR *pRdmptRec = (KER_SECR *)acRec;

   if (!pRec)
   {
      do {
         pRec = fgets(acRec, MAX_RECSIZE, fd);        // Get first rec
      } while (pRdmptRec->Atn_Default[0] < '0');
   }
#ifdef _DEBUG
   //if (!memcmp(acRec, "006 002100400", 13))
   //   iLoop = 0;
#endif
   do
   {
      // Compare Apn
      iLoop = memcmp(pOutRec->Apn, pRdmptRec->Atn_Default, iApnLen);
      if (iLoop > 0)
      {
         pRec = fgets(acRec, MAX_RECSIZE, fd);        // Get next paid delq rec
         if (!pRec)
         {
            fclose(fd);
            fd = NULL;
            return 1;      // EOF
         }
      }
   } while (iLoop > 0);

   if (iLoop)
      return 1;

   lTaxMatch++;

#ifdef _DEBUG
   //if (!memcmp(acRec, "006 002100400", 13))
   //   iRet = 0;
#endif
   if (pRdmptRec->Rdmp_Status[0] == 'A')
   {
      pOutRec->isDelq[0] = '1';
      memcpy(pOutRec->DelqYear, pRdmptRec->Dt_Default, 4);
      memcpy(pOutRec->Def_Date, pRdmptRec->Dt_Default, 8);
   }

   // Get next record
   pRec = fgets(acRec, MAX_RECSIZE, fd);
   if (!pRec)
   {
      fclose(fd);
      fd = NULL;
   }

   return 0;
}

/**************************** Ker_Load_TaxBase *******************************
 *
 * Create base import file and import into SQL if specified
 * Input: G:\CO_DATA\KER\TaxData\CURB.TXT
 *
 * Return 0 if success.
 *
 *****************************************************************************/

int Ker_Load_TaxBase(bool bImport)
{
   char     *pTmp, acBaseRec[MAX_RECSIZE], acRec[MAX_RECSIZE];
   char     acBaseFile[_MAX_PATH], acInFile[_MAX_PATH], acOwnerFile[_MAX_PATH];
   int      iRet;
   long     lBase=0, lCnt=0, lDelq=0;
   FILE     *fdBase, *fdOwner, *fdIn, *fdRdmpt;
   TAXBASE  *pTaxBase = (TAXBASE *)&acBaseRec[0];
   KER_TAX  *pInRec   = (KER_TAX *)&acRec[0];

   LogMsg0("Loading TaxBase");

   GetIniString(myCounty.acCntyCode, "TaxBase", "", acInFile, _MAX_PATH, acIniFile);
   lLastTaxFileDate = getFileDate(acInFile);
   // Only process if new tax file
   iRet = isNewTaxFile(acInFile, myCounty.acCntyCode);
   if (iRet <= 0)
   {
      lLastTaxFileDate = 0;
      return iRet;
   }

   NameTaxCsvFile(acBaseFile, myCounty.acCntyCode, "Base");
   NameTaxCsvFile(acOwnerFile, myCounty.acCntyCode, "Owner");

   pTmp = strrchr(acBaseFile, '\\');
   *pTmp = 0;
   if (_access(acBaseFile, 0))
      _mkdir(acBaseFile);
   *pTmp = '\\';

   // Sort input file on ATN, Tax Year
   sprintf(acBaseRec, "%s\\%s\\%s_curb.srt", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   iRet = sortFile(acInFile, acBaseRec, "S(18,11,C,A,61,4,C,A) F(TXT) ");
   if (iRet <= 0)
      return -1;

   // Open input file
   LogMsg("Open Tax file %s", acBaseRec);
   fdIn = fopen(acBaseRec, "r");
   if (fdIn == NULL)
   {
      LogMsg("***** Error opening Tax file: %s\n", acBaseRec);
      return -2;
   }  

   // Sort Redemption file on ATN, Tax Year
   GetIniString(myCounty.acCntyCode, "TaxRdmpt", "", acInFile, _MAX_PATH, acIniFile);
   sprintf(acBaseRec, "%s\\%s\\%s_Rdmpt.srt", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   iRet = sortFile(acInFile, acBaseRec, "S(42,11,C,A,16,4,C,A) F(TXT) ");
   if (iRet <= 0)
      return -1;

   // Open input file
   LogMsg("Open Redemption file %s", acBaseRec);
   fdRdmpt = fopen(acBaseRec, "r");
   if (fdRdmpt == NULL)
   {
      LogMsg("***** Error opening Redemption file: %s\n", acBaseRec);
      return -2;
   }  

   // Open base file
   LogMsg("Open Base file %s", acBaseFile);
   fdBase = fopen(acBaseFile, "w");
   if (fdBase == NULL)
   {
      LogMsg("***** Error creating base file: %s\n", acBaseFile);
      return -4;
   }

   // Open Owner file
   LogMsg("Open Owner file %s", acOwnerFile);
   fdOwner = fopen(acOwnerFile, "w");
   if (fdOwner == NULL)
   {
      LogMsg("***** Error creating Owner file: %s\n", acOwnerFile);
      return -4;
   }

   // Init variables

   // Merge loop 
   while (!feof(fdIn))
   {
      pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdIn);
      if (!pTmp)
         break;

      // Create new R01 record
      iRet = Ker_ParseTaxBase(acBaseRec, acRec);
      if (!iRet)
      {
         if (fdRdmpt)
            iRet = Ker_UpdateDelq(acBaseRec, fdRdmpt);

         // Create TaxBase record
         Tax_CreateTaxBaseCsv(acRec, (TAXBASE *)&acBaseRec);
         lBase++;
         fputs(acRec, fdBase);

         // Create Owner record
         Tax_CreateTaxOwnerCsv(acRec, &pTaxBase->OwnerInfo);
         fputs(acRec, fdOwner);
      } else if (bDebug)
      {
         if (iRet == -1)
            LogMsg0("Load_TaxBase---> Drop unsecured %d [ATN=%.11s] APN=%.9s", lCnt, pInRec->Atn, pInRec->Apn); 
         else if (iRet == -2)
            LogMsg0("Load_TaxBase---> Drop canceled %d [ATN=%.11s] APN=%.9s", lCnt, pInRec->Atn, pInRec->Apn); 
         else if (iRet == -3)
            LogMsg0("Load_TaxBase---> Drop no tax %d [ATN=%.11s] APN=%.9s", lCnt, pInRec->Atn, pInRec->Apn); 
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdIn)
      fclose(fdIn);
   if (fdRdmpt)
      fclose(fdRdmpt);
   if (fdBase)
      fclose(fdBase);
   if (fdOwner)
      fclose(fdOwner);

   LogMsg("Total records processed:    %u", lCnt);
   LogMsg("Total Base records:         %u", lBase);
   LogMsg("Total Delq records matched: %u", lTaxMatch);

   // Import into SQL
   if (bImport)
   {
      iRet = doTaxImport(myCounty.acCntyCode, TAX_BASE);
      if (!iRet)
         iRet = doTaxImport(myCounty.acCntyCode, TAX_OWNER);
   } else
      iRet = 0;

   return iRet;
}

/****************************** Ker_UpdateDfltAmt ****************************
 *
 * Parse record type 7
 *
 * Return SeqNum
 *
 *****************************************************************************/

int Ker_UpdateDfltAmt(char *pOutbuf, char *pInbuf)
{
   static   double dTaxAmt=0.0;  // reset this value when SeqNum=1
   //int      iSeq, iIdx;
   //TAXDELQ     *pOutRec= (TAXDELQ *)pOutbuf;
   //KER_TAXITEM *pInRec = (KER_TAXITEM *)pInbuf;

   //iSeq = atoin(pInRec->SeqNum, SIZ_TD_SEQNUM);

   //// Tax Year
   //if (iSeq == 1)
   //{
   //   dTaxAmt = 0.0;
   //   memcpy(pOutRec->TaxYear, pInRec->TaxYear, SIZ_TD_YEAR);
   //}

   //iIdx = 0;
   //while (pInRec->asTaxAsmt[iIdx].TaxCode[0] > '0')
   //{
   //   dTaxAmt += Ker_Zone2Dec(pInRec->asTaxAsmt[iIdx].TaxAmt, SIZ_T_AMT);
   //   iIdx++;
   //}

   //sprintf(pOutRec->Def_Amt, "%.2f", dTaxAmt);

   return 0;
}

int Ker_ParseTaxDelq(char *pOutbuf, char *pInbuf)
{
   double   dTmp;
   TAXDELQ  *pOutRec= (TAXDELQ *)pOutbuf;
   KER_SECR *pInRec = (KER_SECR *)pInbuf;

   // Clear output buffer
   memset(pOutbuf, 0, sizeof(TAXDELQ));

   // APN
   memcpy(pOutRec->Apn, pInRec->Atn_Default, TSIZ_ATN);

   // TDN
   memcpy(pOutRec->BillNum, pInRec->Bill_No, SECR_BILL_NO+SECR_BILL_NO_EXT);
   memcpy(pOutRec->Default_No, pInRec->Default_No, SECR_DEFAULT_NO);

   // Tax Status
   pOutRec->isDelq[0] = '0';
         // ' ' = NO DELINQUENCY.
         // 'A' = NORMAL ACTIVE DELINQUENCY.
         // 'R' = REDEEMED.
         // 'S' = SOLD AT TAX AUCTION.
         // 'P' = PARCEL SPLIT.
         // 'D' = REDEMPTION DELETED.
         // 'N' = DUAL ABSTRACT REDEMPTION.

   switch (pInRec->Rdmp_Status[0])
   {
      case 'A':   // ACTIVE DELINQUENCY
         pOutRec->isDelq[0] = '1';
         pOutRec->DelqStatus[0] = TAX_STAT_UNPAID; 
         // 18803417006
         if (pInRec->Install_Status[0] == 'A')
         {
            pOutRec->DelqStatus[0] = TAX_STAT_ONPAYMENT; 
            pOutRec->isDelq[0] = '0';
         } else if (pInRec->Install_Status[0] == 'D')
            pOutRec->DelqStatus[0] = TAX_STAT_DFLTINST; 
         break;
      case 'R':   // REDEEMED
         pOutRec->DelqStatus[0] = TAX_STAT_REDEEMED;
         memcpy(pOutRec->Red_Date, pInRec->Dt_Redeemed, SECR_DT_REDEEMED);
         // Redeem amount
         dTmp = atofn(pInRec->Amount_Coll, SECR_AMOUNT_COLL);
         if (dTmp > 0.0)
            sprintf(pOutRec->Red_Amt, "%.2f", dTmp);
         break;
      case 'S':   // SOLD AT TAX AUCTION
         pOutRec->DelqStatus[0] = TAX_STAT_SOLD2AUCTION;
         pOutRec->isDelq[0] = '1';
         break;
      case 'D':   // REDEMPTION DELETED
         pOutRec->DelqStatus[0] = TAX_STAT_CANCEL;
         break;
      case 'P':   // PARCEL SPLIT
         pOutRec->DelqStatus[0] = pInRec->Rdmp_Status[0];
         break;
      default:
         pOutRec->DelqStatus[0] = pInRec->Rdmp_Status[0];
         break;
   }

   // Updated date
   sprintf(pOutRec->Upd_Date, "%d", lLastTaxFileDate);

   // Tax Year
   memcpy(pOutRec->TaxYear, pInRec->Tax_Year, SECR_TAX_YEAR);

   //// Default amount
   double dTaxAmt = atofn(pInRec->Default_Amt, SECR_DEFAULT_AMT);
   if (dTaxAmt > 0.0)
      sprintf(pOutRec->Def_Amt, "%.2f", dTaxAmt);

   // Default date
   memcpy(pOutRec->Def_Date, pInRec->Dt_Default, SECR_DT_DEFAULT);

   // Pen Amt
   dTmp = atofn(pInRec->Amt_Due_Dlq_Pen, SECR_AMT_DUE_DLQ_PEN);
   if (dTmp > 0)
      sprintf(pOutRec->Pen_Amt, "%.2f", dTmp);

   // Fee Amt
   dTmp = atofn(pInRec->Amt_Due_Dlq_Cst, SECR_AMT_DUE_DLQ_CST);
   if (dTmp > 0)
      sprintf(pOutRec->Fee_Amt, "%.2f", dTmp);

   // Power to sale date
   if (pInRec->Dt_Pts_Notc_Prt[0] >= '0')
      memcpy(pOutRec->Pts_Date, pInRec->Dt_Pts_Notc_Prt, SECR_DT_PTS_NOTC_PRT);

   return 0;
}

/**************************** Ker_Load_TaxDelq *******************************
 *
 * Create Delq import file and import into SQL if specified
 *
 * Return 0 if success.
 *
 *****************************************************************************/

int Ker_Load_TaxDelq(bool bImport)
{
   char     *pTmp, acDelqRec[MAX_RECSIZE], acRec[MAX_RECSIZE];
   char     acDelqFile[_MAX_PATH], acInFile[_MAX_PATH];
   int      iRet;
   long     lBase=0, lCnt=0, lDelq=0;
   FILE     *fdDelq, *fdIn;
   TAXDELQ  *pTaxDelq = (TAXDELQ *)&acDelqRec[0];
   KER_SECR *pInRec   = (KER_SECR *)&acRec[0];

   LogMsg0("Loading TaxDelq");

   GetIniString(myCounty.acCntyCode, "TaxRdmpt", "", acInFile, _MAX_PATH, acIniFile);
   lLastTaxFileDate = getFileDate(acInFile);

   // Open input file
   LogMsg("Open Tax Redemption file %s", acInFile);
   fdIn = fopen(acInFile, "r");
   if (fdIn == NULL)
   {
      LogMsg("***** Error opening Tax Redemption file: %s\n", acInFile);
      return -2;
   }  

   // Open base file
   NameTaxCsvFile(acDelqFile, myCounty.acCntyCode, "Delq");
   LogMsg("Open Delq file %s", acDelqFile);
   fdDelq = fopen(acDelqFile, "w");
   if (fdDelq == NULL)
   {
      LogMsg("***** Error creating Delq file: %s\n", acDelqFile);
      return -4;
   }

   // Init variables

   // Merge loop 
   while (!feof(fdIn))
   {
      pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdIn);
      if (!pTmp)
         break;

      // Process active delinquent only
      //if (pInRec->Rdmp_Status[0] == 'A')
      {
         // Create new R01 record
         iRet = Ker_ParseTaxDelq(acDelqRec, acRec);
         if (!iRet)
         {
            // Create TaxDelq record
            Tax_CreateDelqCsv(acRec, (TAXDELQ *)&acDelqRec);
            lBase++;
            fputs(acRec, fdDelq);

         } else
         {
            if (iRet == -1)
               LogMsg0("Load_TaxBase---> Drop unsecured %d [ATN=%.11s]", lCnt, pInRec->Atn); 
            else if (iRet == -2)
               LogMsg0("Load_TaxBase---> Drop canceled %d [ATN=%.11s]", lCnt, pInRec->Atn); 
            else if (iRet == -3)
               LogMsg0("Load_TaxBase---> Drop no tax %d [ATN=%.11s]", lCnt, pInRec->Atn); 
         }
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdIn)
      fclose(fdIn);
   if (fdDelq)
      fclose(fdDelq);

   LogMsg("Total records processed:    %u", lCnt);
   LogMsg("Total Delq records:         %u", lBase);

   // Import into SQL
   if (bImport)
   {
      iRet = doTaxImport(myCounty.acCntyCode, TAX_DELINQUENT);
   } else
      iRet = 0;

   return iRet;
}

/******************************** Ker_ExtrDist ******************************
 *
 * Extract district data from DIST.TXT and create Ker_TaxDist.txt
 * Return number of records output.
 *
 ****************************************************************************/

int Ker_ExtrDist(char *pInfile, char *pOutfile)
{
   char     *pTmp, acInrec[1024], acOutrec[1024], acTmpfile[_MAX_PATH];
   int      iRet;
   long     lCnt=0;
   FILE     *fdOut, *fdIn;

   LogMsg0("Extract district data from %s", pInfile);

   // Open input file
   LogMsg("Open Tax Dist file %s", pInfile);
   fdIn = fopen(pInfile, "r");
   if (fdIn == NULL)
   {
      LogMsg("***** Error opening Tax district file: %s\n", pInfile);
      return -1;
   }  

   sprintf(acTmpfile, "%s\\KER\\KER_Dist.txt", acTmpPath);
   LogMsg("Create District file %s", acTmpfile);
   fdOut = fopen(acTmpfile, "w");
   if (fdOut == NULL)
   {
      LogMsg("***** Error creating district file: %s\n", acTmpfile);
      return -2;
   }  

   // Output header
   fputs("Code|Agency|Phone|TaxRate|TaxAmt|TCFlg\n", fdOut);

   while (!feof(fdIn))
   {
      pTmp = fgets(acInrec, 1000, fdIn);
      if (!pTmp)
         break;
      if (acInrec[OFF_DIST_DESCRIPT] > ' ')
      {
         if (acInrec[OFF_DIST_PHONE_NO+7] > ' ')
            sprintf(acOutrec, "%.5s|%.30s|(%.3s) %.3s-%.4s|||0\n", &acInrec[OFF_DIST_FUND_NO],&acInrec[OFF_DIST_DESCRIPT],
               &acInrec[OFF_DIST_PHONE_NO],&acInrec[OFF_DIST_PHONE_NO+3],&acInrec[OFF_DIST_PHONE_NO+6]);
         else
            sprintf(acOutrec, "%.5s|%.30s||||0\n", &acInrec[OFF_DIST_FUND_NO],&acInrec[OFF_DIST_DESCRIPT]);

         fputs(acOutrec, fdOut);
      }
      if (!(++lCnt % 100))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdIn)
      fclose(fdIn);
   if (fdOut)
      fclose(fdOut);

   iRet = sortFile(acTmpfile, pOutfile, "S(1,5,C,A) F(TXT) B(38,R) DUPO(1,5)");

   LogMsg("Total records processed:  %u", lCnt);
   LogMsg("                 output:  %u", iRet);

   return iRet;
}

/********************************* Ker_ExtrVal ******************************
 *
 * Extract values from G:\CO_DATA\KER\TaxData\CURB.TXT
 *
 ****************************************************************************/

int Ker_CreateValueRec(char *pOutbuf, char *pInbuf)
{
   char     acTmp[256], acApn[256];
   LONGLONG lTmp;
   int      iTmp;
   LIENEXTR *pLienRec = (LIENEXTR *)pOutbuf;
   KER_TAX  *pInRec   = (KER_TAX *)pInbuf;

   // Clear output buffer
   memset(pOutbuf, ' ', sizeof(LIENEXTR));

   // Format APN
   // Do not process unsecured, canceled or to be bill parcels
   if (pInRec->Bill_SubType[0] == 'U')
      return -1;
   if (!memcmp(pInRec->Bill_SubType, "SS", 2))
      return -1;
   if (pInRec->Bill_Status == 'T' || pInRec->Bill_Status == 'C')
      return -2;

#ifdef _DEBUG
   //if (!memcmp(pInRec->Atn, "382241080", 9))
   //   iTmp = 1;
#endif
   // APN
   memcpy(acApn, pInRec->Atn, TSIZ_ATN);
   iTmp = iTrim(acApn, TSIZ_ATN);
   memcpy(pLienRec->acApn, acApn, iTmp);

   // TRA
   memcpy(pLienRec->acTRA, pInRec->TRA, TSIZ_TRA);

   // Tax Year
   memcpy(pLienRec->acYear, pInRec->TaxYear, TSIZ_TAXYEAR);

   // Land
   ULONG lLand = atoln(pInRec->Land, TSIZ_LAND);
   if (lLand > 0)
   {
      iTmp = sprintf(acTmp, "%*u", sizeof(pLienRec->acLand), lLand);
      memcpy(pLienRec->acLand, acTmp, iTmp);
   }

   // Improve - may have negative value
   long lImpr = atoln(pInRec->Impr, TSIZ_IMPR);
   if (lImpr)
   {
      iTmp = sprintf(acTmp, "%*d", sizeof(pLienRec->acImpr), lImpr);
      memcpy(pLienRec->acImpr, acTmp, iTmp);

      if ((lLand+lImpr) > 0)
      {
         iTmp = sprintf(acTmp, "%*u", sizeof(pLienRec->acRatio), (LONGLONG)lImpr*100/(lLand+lImpr));
         memcpy(pLienRec->acRatio, acTmp, iTmp);
      }
   }

   // PP value
   ULONG lPers = atoln(pInRec->PPVal, TSIZ_PPVAL);

   // Fixture
   ULONG lFixt = atoln(pInRec->Fixtr, TSIZ_FIXTR);

   // Total other
   ULONG lOthers = lPers + lFixt + atoln(pInRec->Mineral, TSIZ_MINERAL);
   if (lOthers > 0)
   {
      iTmp = sprintf(acTmp, "%*u", sizeof(pLienRec->acOther), lOthers);
      memcpy(pLienRec->acOther, acTmp, iTmp);

      if (lPers > 0)
      {
         iTmp = sprintf(acTmp, "%u", lPers);
         memcpy(pLienRec->acPP_Val, acTmp, iTmp);
      }
      if (lFixt > 0)
      {
         iTmp = sprintf(acTmp, "%u", lFixt);
         memcpy(pLienRec->acME_Val, acTmp, iTmp);
      }
   }

   // Gross
   lTmp = lOthers + lLand + lImpr;
   if (lTmp > 0)
   {
      iTmp = sprintf(acTmp, "%*u", sizeof(pLienRec->acGross), lTmp);
      memcpy(pLienRec->acGross, acTmp, iTmp);
   }

   // HO Exempt
   if (pInRec->ExeType[0] == 'H')
      pLienRec->acHO[0] = '1';      // 'Y'
   else
      pLienRec->acHO[0] = '2';      // 'N'

   long lHo_Exe = atoin(pInRec->ExeVal, TSIZ_EXMPT_VAL);
   if (lHo_Exe > 0)
   {
      iTmp = sprintf(acTmp, "%d", lHo_Exe);
      memcpy(pLienRec->acExAmt, acTmp, iTmp);
   }

   // Total Exemption: ExeVal = ExeVal2+ExeVal3
   //long lExe2 = atoin(pInRec->ExeVal2, TSIZ_EXMPT_VAL);
   //long lExe3 = atoin(pInRec->ExeVal3, TSIZ_EXMPT_VAL);
   lTmp = lHo_Exe;
   if (lTmp > 0)
   {
      iTmp = sprintf(acTmp, "%u", lTmp);
      memcpy(pLienRec->acExAmt, acTmp, iTmp);
   } 

   pLienRec->LF[0] = '\n';
   pLienRec->LF[1] = 0;
   return 0;
}

int Ker_ExtrVal()
{
   char      *pTmp, acBuf[1024], acRec[MAX_RECSIZE], acTmpFile[_MAX_PATH];
   int       iRet, iRecSize;
   long      lOut=0, lCnt=0;
   FILE      *fdOut, *fdIn;
   KER_TAX  *pInRec   = (KER_TAX *)acRec;

   LogMsg0("Extracting final values from equalized file.");

   // Open input file
   GetIniString(myCounty.acCntyCode, "EQRollFile", "", acTmpFile, _MAX_PATH, acIniFile);
   LogMsg("Open %s for input", acTmpFile);
   fdIn = fopen(acTmpFile, "r");
   if (fdIn == NULL)
   {
      LogMsg("***** Error opening input file: %s\n", acTmpFile);
      return -2;
   }  

   iRecSize = GetPrivateProfileInt(myCounty.acCntyCode, "TaxRecSize", 3066, acIniFile);

   // Open out file
   sprintf(acTmpFile, acValueTmpl, myCounty.acCntyCode, myCounty.acCntyCode);
   LogMsg("Open file %s for output", acTmpFile);
   fdOut = fopen(acTmpFile, "w");
   if (fdOut == NULL)
   {
      LogMsg("***** Error creating output file: %s\n", acTmpFile);
      return -4;
   }

   // Start loop 
   while (!feof(fdIn))
   {
      pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdIn);
      if (!pTmp)
         break;

      // Create new base record
      iRet = Ker_CreateValueRec(acBuf, acRec);
      if (!iRet)
      {
         fputs(acBuf, fdOut);
         lOut++;
      } else
      {
         LogMsg("---> Drop record %.50s (%d) ", acRec, lCnt); 
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   LogMsg("Total records processed: %u", lCnt);
   LogMsg("Total records output:    %u", lOut);

   // Close files
   if (fdIn)
      fclose(fdIn);
   if (fdOut)
      fclose(fdOut);

   return 0;
}

/*********************************** loadKer ********************************
 *
 * Input files:
 *    - RBPR.DSS         (roll file, 422-byte fixed)
 *    - CHAR.ASC         (char file, comma delimited)
 *    - DOCM.DSS         (sale file, 305-byte )
 *    - LEGL.DSS         (legal file, 152-byte )
 *    - SITS.DSS         (situs file, 82-byte )
 *    - UFAR.DSS         (mailing file, 122-byte )
 *
 * Problems:
 *    1) CHAR.ASC is in APN order, RBPR.DSS, LEGL.DSS, SITS.DSS, and UFAR.DSS
 *       are in TE_NO order.  DOCM.DSS is in DOC_NO order.  We have to sort DOCM
 *       on TE_NO order and process CHAR independently.
 *    2) RBPR.DSS may have record without ATN, we have to remove them.
 *    3) Lien values are increase right after lien date release.  SO Lien extract
 *       is needed on Lien processing for monthly update.
 *
 * Processing:
 *    1) Extract sale for cumulative file (to be done later)
 *    2) Load roll file with situs, legal, mailing and sale.  Sort result
 *       on ATN.
 *    3) On lien date, create lien extract for monthly update
 *    4) Convert CHAR file if needed then update R01 with CHAR data
 *
 * Commands:
 *    -Xc   Extract cum sale from R01 if needed
 *    -U    Monthly update
 *    -L    Lien update
 *    -Lc   Load new CHAR file
 *    -Dr   Do not remove temp file
 *    
 * LDR command: -CKER -L -Xa -Xl -Dr -Ms -Mz -O
 * Monthly cmd: -CKER -U -Xs -Xa [-Mz] [-Xf|-Mf] 
 * Load tax:    -CKER -T
 *
 ****************************************************************************/

int loadKer(int iSkip)
{
   int   iRet=0, iTmp;
   char  acTmp[_MAX_PATH], acTmpFile[_MAX_PATH], acKerDist[_MAX_PATH];

   iApnLen = myCounty.iApnLen;

   // Load tax - 20160316
   if (iLoadTax == TAX_LOADING)              // -T or -Lt
   {
      iRet = GetIniString(myCounty.acCntyCode, "KerDist", "", acKerDist, _MAX_PATH, acIniFile);
      iRet = GetIniString(myCounty.acCntyCode, "TaxDist", "", acTmpFile, _MAX_PATH, acIniFile);
      iRet = chkFileDate(acKerDist, acTmpFile);
      if (iRet == 1)
      {
         iRet = Ker_ExtrDist(acKerDist, acTmpFile);
         if (iRet < 0)
            return iRet;
      }
      iRet = LoadTaxCodeTable(acTmpFile);

      // Load CURB.TXT to populate Tax Base and Tax Owner table
      iRet = Ker_Load_TaxBase(bTaxImport);
      if (!iRet && lLastTaxFileDate > 0)
      {
         iRet = Ker_Load_TaxDetail(bTaxImport);
      }

      if (!iRet)
      {
         iRet = Ker_Load_TaxDelq(bTaxImport);
         if (!iRet)
            iRet = updateDelqFlag(myCounty.acCntyCode);
      }

      //if (!iRet)
      //   iRet = Ker_Load_TaxPaid(bTaxImport);

      // Create empty Tax_Owner table
      //doTaxPrep(myCounty.acCntyCode, TAX_OWNER);
   }

   if (!iLoadFlag && !lOptExtr)
      return iRet;

   // Extract final value
   if (lOptExtr & EXTR_FVAL)                       // -Xf
      iRet = Ker_ExtrVal();

   if (!iLoadFlag)
      return 0;

   // Load tables
   //if (!LoadLUTable((char *)&acLookupTbl[0], "[Quality]", NULL, MAX_ATTR_ENTRIES))
   //{
   //   LogMsg("*** Error Looking for table [Quality] in %s", acLookupTbl);
   //   return 1;
   //}

   // Check for new or old file
   iTmp = GetPrivateProfileString("KER", "Use", "", sCnty, _MAX_PATH, acIniFile);
   if (iTmp <= 0)
   {
      strcpy(sCnty, myCounty.acCntyCode);
      bNewFormat = false;
   } else
   {
      bNewFormat = true;

      GetIniString(sCnty, "CChrTmpl", "", acTmp, _MAX_PATH, acIniFile);
      sprintf(acCBldgFile, acTmp, "Bldg");
      sprintf(acCChrFile,  acTmp, "Char");
   }

   // Validate input file for new format
   if ((iRet = Ker_ValidateInput(sCnty, bNewFormat)) < 0)
      return -1;

   // Fix OtherApn as needed
   //iRet = FixOtherApn(acCSalFile, RSIZ_TE_NO);

   // Sort input file based on ATN
   if (bNewFormat && (iLoadFlag & (EXTR_LIEN|LOAD_LIEN)))
   {
      sprintf(acTmpFile, "%s\\%s\\Ker_Lien.srt", acTmpPath, myCounty.acCntyCode);

      if (lLienYear >= 2023)
         sprintf(acTmp, "S(#5,C,A) OMIT(#5,C,GT,\"9\",OR,#5,C,LT,\"0\") DEL(9) F(TXT)");      
      else if (lLienYear >= 2021)
         sprintf(acTmp, "S(24,10,C,A) F(TXT) OMIT(1,1,C,GT,\"9\")");
      else if (lLienYear >= 2020)
         sprintf(acTmp, "S(#3,C,A) OMIT(#3,C,GT,\"9\",OR,#3,C,LT,\"0\") DEL(9) F(TXT)");

      // Sort on ATN
      iRet = sortFile(acLienFile, acTmpFile, acTmp);
      if (iRet > 0)
         strcpy(acLienFile, acTmpFile);
   }

   if (iLoadFlag & EXTR_LIEN)                      // -Xl
   {
      LogMsg0("Extract lien file");
      if (bNewFormat)
      {
         //iRet = Ker_ExtrLien(acLienFile);          // 2018, 2021, 2022
         iRet = Ker_ExtrLienCsv(acLienFile);       // 2020, 2023
      } else
         iRet = Ker_ExtrRBPR(acLienFile);
   }

   // Extract SALE file
   if (iLoadFlag & (EXTR_SALE|UPDT_SALE))          // -Xs or Us
   {
      if (bNewFormat)
      {
         iRet = Ker_ExtrSale(acSaleFile);
      } else
      {
         // Sort sale file by TE_NO, sale date & doc desc order
         // Do not use APN field in sale file  (not reliable)
         sprintf(acTmp, "%s\\%s", acTmpPath, myCounty.acCntyCode);
         if (_access(acTmp, 0))
            _mkdir(acTmp);
         sprintf(acTmpFile, "%s\\%s_Sale.txt", acTmp, myCounty.acCntyCode);

         // Check first 512 bytes for <CRLF>.  spn 10/12/2006
         if (isTextFile(acSaleFile, 512))
            sprintf(acTmp, "S(1,10,C,A,55,17,C,D) F(TXT) DUPOUT(1,10,55,17)");
         else
            sprintf(acTmp, "S(1,10,C,A,55,17,C,D) F(FIX,%d) OUTREC(1,%d,CRLF)", iSaleLen, iSaleLen);

         // Sort on TE_NO, transfer date
         iRet = sortFile(acSaleFile, acTmpFile, acTmp);
         if (iRet > 0)
         {
            strcpy(acSaleFile, acTmpFile);
            // Update history sale sale
            LogMsg("Update %s sale history file", myCounty.acCntyCode);
            iRet = Ker_CreateSaleHist();
            // Match sale APN to roll APN, replace TE_NO with APN
            // Do this because APN in sale file sometimes incomplete
            iRet = Sale_MatchRoll(acCSalFile, SSIZ_TE_NO, OFF_TE_NO, true);
            if (iRet > 0)
               iRet = 0;
         } else
         {
            LogMsg("***** ERROR sorting sale file %s", acSaleFile);
            iLoadFlag = 0;
            iRet = -1;
         }
      }

      if (!iRet)
         iLoadFlag |= MERG_CSAL;
   }

   // Load or Convert CHAR file
   if (iLoadFlag & EXTR_ATTR)                      // -Xa
   {
      LogMsg0("Loading CHAR file");
      if (bNewFormat)
      {                          
         // CHAR from characteristics
         iRet = Ker_ExtrCVal(acCharFile, acCChrFile);

         // CHAR from Building
         if (iRet > 0)
            iRet = Ker_ExtrBldg(acBldgFile, acCBldgFile);
      } else
      {
         iRet = Ker_ConvertChar(acCharFile, acCChrFile);
      }

      if (iRet <= 0)
         LogMsg("***** Error loading CHAR file.  Use older file if available.");
      else
         LogMsg("Loading CHAR file successful");
   }

   GetIniString(sCnty, "LeglFile", "", acLegalFile, _MAX_PATH, acIniFile);
   if (iLoadFlag & LOAD_LIEN)                      // -L
   {
      if (bNewFormat)
         //iRet = Ker_Load_LDR(acLienFile, iSkip);            // Prior to 2020, 2021
         iRet = Ker_Load_LDR_Csv(acLienFile, iSkip);      // 2020, 2023
      else
      {
         iRet = CreateKerRoll(iSkip);
         if (!iRet)
            iRet = Ker_UpdateChar(iSkip);
      }
   } else if (iLoadFlag & LOAD_UPDT)               // -U
   {
      if (bNewFormat)
         iRet = Ker_Load_Roll2(iSkip);
      else
      {
         iRet = Ker_Load_Roll(iSkip);
         if (!iRet)
            iRet = Ker_UpdateChar2(iSkip);         // Use new CHAR file
            //iRet = Ker_UpdateChar(iSkip);
      }
   }

   // Apply cum sale file to R01
   if (!iRet && (iLoadFlag & MERG_CSAL) )          // -Ms
   {
      // Apply Ker_Sale.sls to R01 file
      iRet = ApplyCumSale(iSkip, acCSalFile, false, SALE_USE_SCUPDXFR);
   }

  // Format DocLinks - Doclinks are concatenate fields separated by comma 
  if (!iRet && (iLoadFlag & (LOAD_UPDT|LOAD_LIEN|MERG_CSAL)) )
      iRet = updateDocLinks(Ker_MakeDocLink, myCounty.acCntyCode, iSkip);

   return iRet;
}
