#ifndef _MERGESMX_H_
#define _MERGESMX_H_

// SecureMaster.csv
#define SMX_R_APN                   0
#define SMX_R_TRA                   1
#define SMX_R_OWNER                 2
#define SMX_R_OWNER2                3  // May contain C/O or ATN:, ATTN:, ATNO, "ATT "
#define SMX_R_CAREOF                4  // May contain part of owner2 or C/O or ATN:, ATTN:, ATNN, "ATT "
#define SMX_R_M_ADDRESS1            5
#define SMX_R_M_ADDRESS2            6  // all blank
#define SMX_R_M_ADDRESS3            7  // all blank
#define SMX_R_M_CITY                8
#define SMX_R_M_STATE               9
#define SMX_R_M_ZIP                 10
#define SMX_R_M_COUNTRY             11 // US, CA or blank
#define SMX_R_S_STRNUM              12
#define SMX_R_S_STRNUM2             13
#define SMX_R_S_STRFRA              14
#define SMX_R_S_STRDIR              15
#define SMX_R_S_STRNAME             16
#define SMX_R_S_STRTYPE             17
#define SMX_R_S_STRUNIT             18
#define SMX_R_S_CITY                19
#define SMX_R_PUCCODE               20 // 2nd & 3rd bytes are old county use code
#define SMX_R_LEGALDESC             21
#define SMX_R_LAND_VAL              22
#define SMX_R_IMPR_VAL              23
#define SMX_R_ROOT_VAL              24
#define SMX_R_MIN_VAL               25
#define SMX_R_PP_VAL                26
#define SMX_R_FIX_VAL               27
#define SMX_R_EXE_CODE              28 // HO?? = Homeowner, HS=School
#define SMX_R_EXE1                  29
#define SMX_R_EXE2                  30
#define SMX_R_NET_VAL               31
// 07-12-2024 #define SMX_R_DOCDATE               32
#define SMX_R_DOCNUM                32
#define SMX_R_SALEDATE              33
#define SMX_R_SALEAMT               34
#define SMX_R_FLDS                  35

// Characteristics.csv
#define SMX_C_APN                   0
#define SMX_C_PROPERTY_TYPE         1
#define SMX_C_CHAR_SCREEN           2
#define SMX_C_LAND_SQFT             3 
#define SMX_C_YEAR_BUILT            4 
#define SMX_C_TOTAL_LIVAREA         5  // SFR & Condo BldgArea
#define SMX_C_TOTAL_ROOMS           6 
#define SMX_C_BEDROOMS              7 
#define SMX_C_FULL_BATHS            8
#define SMX_C_GROSS_BLDGAREA        9  // For business BldgArea
#define SMX_C_UNITS                 10
#define SMX_C_HALF_BATHS            11
#define SMX_C_TOTAL_SQFT            12 // For Mobilehome BldgArea
#define SMX_C_FLDS                  13

// 11/22/2024 - New CHAR file - Characteristic.csv
#define SMX_C2_APN                  0
#define SMX_C2_PROPERTY_USECODE     1
#define SMX_C2_PROPERTY_INFO        2
#define SMX_C2_PROPERTY_TYPE        3
#define SMX_C2_PROPERTY_SUBTYPE     4
#define SMX_C2_YEAR_BUILT           5
#define SMX_C2_LAND_SQFT            6
#define SMX_C2_SQ_FEET_USABLE       7
#define SMX_C2_ACRES                8
#define SMX_C2_BASE_AREA            9
#define SMX_C2_TOTAL_ROOMS          10
#define SMX_C2_BEDROOMS             11
#define SMX_C2_FULL_BATH            12
#define SMX_C2_GROSS_BLDGAREA       13
#define SMX_C2_NO_OF_UNITS          14
#define SMX_C2_HALF_BATH            15
#define SMX_C2_LIVING_ROOM          16
#define SMX_C2_DINING_ROOM          17
#define SMX_C2_KITCHEN              18
#define SMX_C2_FAMILY_ROOM          19
#define SMX_C2_OTHER_ROOMS          20
#define SMX_C2_TOTAL_ROOM           21
#define SMX_C2_BATHROOMS            22
#define SMX_C2_HEATING_TYPE         23
#define SMX_C2_COOLING_TYPE         24
#define SMX_C2_GARAGE_IN_BASEMENT   25    // Yes/No
#define SMX_C2_CARPORT              26
#define SMX_C2_PARKING_SPACES       27
#define SMX_C2_PATTERN              28
#define SMX_C2_FIRST_FLOOR_AREA     29
#define SMX_C2_SECOND_FLOOR_AREA    30
#define SMX_C2_THIRD_FLOOR_AREA     31
#define SMX_C2_ATTIC_AREA           32
#define SMX_C2_FINISHED_BSMT_AREA   33
#define SMX_C2_BASEMENT_AREA        34
#define SMX_C2_STORIES              35
#define SMX_C2_GARAGE_AREA          36
#define SMX_C2_POOL_YEAR            37
#define SMX_C2_WIDTH                38
#define SMX_C2_DEPTH                39
#define SMX_C2_LOCATION             40
#define SMX_C2_AMENITIES            41
#define SMX_C2_WATER_FRONT          42
#define SMX_C2_COMPLEX_NAME         43
#define SMX_C2_APARTMENT_CONV       44
#define SMX_C2_BUILDING_FLOORS      45
#define SMX_C2_EFFECTIVE_YEAR       46
#define SMX_C2_QUALITY_RATING       47
#define SMX_C2_UNIT_LOCATION        48
#define SMX_C2_FIREPLACES           49
#define SMX_C2_CONSTRUCTION_CLASS   50
#define SMX_C2_QUALITY_CLASS        51
#define SMX_C2_NO_4BEDROOMS_35BATHS 52
#define SMX_C2_STORY_HEIGHT         53
#define SMX_C2_NET_RENT_AREA        54
#define SMX_C2_WAREHOUSE_AREA       55
#define SMX_C2_SHOP_AREA            56
#define SMX_C2_STORAGE_AREA         57
#define SMX_C2_REFRIGERATE_AREA     58
#define SMX_C2_OFFICE_AREA          59
#define SMX_C2_MEZZANINE_AREA       60
#define SMX_C2_MISC_AREA            61
#define SMX_C2_WALL_HEIGHT          62
#define SMX_C2_BUILDING_WIDTH       63
#define SMX_C2_BUILDING_LENGTH      64
#define SMX_C2_DOCK_DOOR_COUNT      65
#define SMX_C2_SPACE_NO             66
#define SMX_C2_MANUFACTURER         67
#define SMX_C2_TRADE_NAME           68
#define SMX_C2_LENGTH               69
#define SMX_C2_NO_BLDGS             70
#define SMX_C2_NO_STUDIO            71
#define SMX_C2_NO_1BEDROOM_1BATH    72
#define SMX_C2_NO_1BEDROOM_1BATHA   73
#define SMX_C2_NO_2BEDROOMS_15BATHS 74
#define SMX_C2_NO_1BEDROOM_15BATHA  75
#define SMX_C2_NO_2BEDROOMS_1BATH   76
#define SMX_C2_NO_2BEDROOMS_2BATHS  77
#define SMX_C2_NO_3BEDROOMS_15BATHS 78
#define SMX_C2_NO_3BEDROOMS_1BATH   79
#define SMX_C2_NO_3BEDROOMS_25BATHS 80
#define SMX_C2_NO_3BEDROOMS_2BATHS  81
#define SMX_C2_NO_4BEDROOMS_25BATHS 82
#define SMX_C2_NO_4BEDROOMS_2BATHS  83
#define SMX_C2_NO_4BEDROOMS_3BATHS  84
#define SMX_C2_NO_2BEDROOM_1BATHA   85
#define SMX_C2_NO_2BEDROOM_2BATHA   86
#define SMX_C2_NO_3BEDROOM_15BATHA  87
#define SMX_C2_NO_3BEDROOM_1BATHA   88
#define SMX_C2_NO_3BEDROOM_25BATHA  89
#define SMX_C2_NO_3BEDROOM_2BATHA   90
#define SMX_C2_NO_4BEDROOM_25BATHA  91
#define SMX_C2_NO_4BEDROOM_4BATHA   92
#define SMX_C2_NO_4BEDROOM_35BATHA  93
#define SMX_C2_NO_4BEDROOM_3BATHA   94
#define SMX_C2_PARKING_TYPE         95
#define SMX_C2_ELEVATOR             96
#define SMX_C2_GRADE                97
#define SMX_C2_DOCKING_RIGHTS       98
#define SMX_C2_EASEMENT             99
#define SMX_C2_RENOVATE_YEAR        100
#define SMX_C2_RETAIL_AREA          101
#define SMX_C2_FLDS                 102

// SaleHist.csv
#define SMX_S_APN                   0
#define SMX_S_SALECODE              1
#define SMX_S_SALEDATE              2
#define SMX_S_SALEAMT               3
#define SMX_S_SALEDOC               4
#define SMX_S_FLDS                  5

#define MAX_ROLLREC                 810
#define MAX_SFRCHARREC              330
#define MAX_APTCHARREC              459
#define SMX_SALEREC                 65
#define SMX_CHARREC                 68
#define SMX_VACREC                  60
/*
#define SMX_OFF_APN                 1-1
#define SMX_OFF_FILLER1             10-1
#define SMX_OFF_DOCDATE             21-1
#define SMX_OFF_SALE_SEQ            29-1
#define SMX_OFF_DOCNUM              32-1
#define SMX_OFF_FILLER2             42-1
#define SMX_OFF_SALE_AMT1           55-1

#define SMX_SIZ_APN                 9
#define SMX_SIZ_FILLER1             11
#define SMX_SIZ_DOCDATE             8
#define SMX_SIZ_SALE_SEQ            3
#define SMX_SIZ_DOCNUM              10
#define SMX_SIZ_FILLER2             13
#define SMX_SIZ_SALE_AMT1           9

typedef struct _tSmxSale
{
   char Apn[SMX_SIZ_APN];
   char Filler1[SMX_SIZ_FILLER1];
   char DocDate[SMX_SIZ_DOCDATE];
   char Sale_Seq[SMX_SIZ_SALE_SEQ];
   char DocNum[SMX_SIZ_DOCNUM];
   char Filler2[SMX_SIZ_FILLER2];
   char SalePrice[SMX_SIZ_SALE_AMT1];
} SMX_SALE;

*/

#define SMX_SIZ_APN                 9
#define SMX_SIZ_UNIT                4
#define SMX_SIZ_SEQ                 3
#define SMX_SIZ_CONSTR_YR           4
#define SMX_SIZ_USE_CD1             2
#define SMX_SIZ_LIV_RM              2
#define SMX_SIZ_DIN_RM              2
#define SMX_SIZ_KITCHEN             2
#define SMX_SIZ_FAM_RM              2
#define SMX_SIZ_BEDROOMS            2
#define SMX_SIZ_DEN_LIBRARY         2
#define SMX_SIZ_SUPPL_RM            2
#define SMX_SIZ_ROOMS               3
#define SMX_SIZ_BATHS               3
#define SMX_SIZ_HALFBATHS           3
#define SMX_SIZ_CENT_HT             10
#define SMX_SIZ_CENT_COOL           10
#define SMX_SIZ_GARAGE_BASEMENT     10
#define SMX_SIZ_CARPORT             10
#define SMX_SIZ_STALLS              9
#define SMX_SIZ_PATTERN             10
#define SMX_SIZ_BASEMENT_AREA1      9
#define SMX_SIZ_FLOOR1_AREA         9
#define SMX_SIZ_FLOOR2_AREA         9
#define SMX_SIZ_FLOOR3_AREA         9
#define SMX_SIZ_ATTIC_AREA          9
#define SMX_SIZ_FINISH_BASEMENT     9
#define SMX_SIZ_BASEMENT_AREA2      9
#define SMX_SIZ_ADDL_AREA           9
#define SMX_SIZ_STORIES             12
#define SMX_SIZ_GAR_CLS_ATT         10
#define SMX_SIZ_GAR_CLS_DET         10
#define SMX_SIZ_GARAGE_AREA         9
#define SMX_SIZ_POOL_YEAR           9
#define SMX_SIZ_WIDTH               12
#define SMX_SIZ_DEPTH               12
#define SMX_SIZ_LOT_SQFT_ACTUAL     9
#define SMX_SIZ_LOT_SQFT_USEABLE    9
#define SMX_SIZ_ACRES               9
#define SMX_SIZ_CORNER              10
#define SMX_SIZ_ALLEY               10
#define SMX_SIZ_NON_STREET_FRONT    10
#define SMX_SIZ_COMMON_GREEN        10
#define SMX_SIZ_COMMON_RECREAT      10
#define SMX_SIZ_WATERFRONT          10


// Vacant Land TCB 4/26/07
#define SMX_OFF_VAC_APN                1-1
#define SMX_OFF_VAC_WIDTH              10-1
#define SMX_OFF_VAC_DEPTH              22-1
#define SMX_OFF_VAC_ACT_SQFT           34-1
#define SMX_OFF_VAC_USE_SQFT           43-1
#define SMX_OFF_VAC_ACRES              52-1

#define SMX_SIZ_VAC_APN                9
#define SMX_SIZ_VAC_WIDTH              12
#define SMX_SIZ_VAC_DEPTH              12
#define SMX_SIZ_VAC_ACT_SQFT           9
#define SMX_SIZ_VAC_USE_SQFT           9
#define SMX_SIZ_VAC_ACRES              9

typedef struct _tSmxVac
{
   char Apn[SMX_SIZ_VAC_APN];
   char Width[SMX_SIZ_VAC_WIDTH];            // V999
   char Depth[SMX_SIZ_VAC_DEPTH];            // V999
   char Act_SQFT[SMX_SIZ_VAC_ACT_SQFT];
   char Use_SQFT[SMX_SIZ_VAC_USE_SQFT];
   char Acres[SMX_SIZ_VAC_ACRES];            // V999
} SMX_VAC;


// New change to sale file since Aug 2006
#define SMX_OFF_APN                 1-1
#define SMX_OFF_DOCDATE             10-1
#define SMX_OFF_DOCNUM              18-1
#define SMX_OFF_SALE_AMT1           28-1

#define SMX_SIZ_DOCDATE             8
#define SMX_SIZ_DOCNUM              10
#define SMX_SIZ_SALE_AMT1           12

typedef struct _tSmxSale
{
   char Apn[SMX_SIZ_APN];
   char DocDate[SMX_SIZ_DOCDATE];
   char DocNum[SMX_SIZ_DOCNUM];
   char SalePrice[SMX_SIZ_SALE_AMT1];
} SMX_SALE;

/* 2012 and before
#define RES_OFF_APN                 1-1
#define RES_OFF_UNIT                10-1
#define RES_OFF_SEQ                 14-1
#define RES_OFF_CONSTR_YR           17-1
#define RES_OFF_USE_CD1             21-1
#define RES_OFF_LIV_RM              23-1
#define RES_OFF_DIN_RM              25-1
#define RES_OFF_KITCHEN             27-1
#define RES_OFF_FAM_RM              29-1
#define RES_OFF_BEDROOMS            31-1
#define RES_OFF_DEN_LIBRARY         33-1
#define RES_OFF_SUPPL_RM            35-1
#define RES_OFF_ROOMS               37-1
#define RES_OFF_BATHS               40-1
#define RES_OFF_HALFBATHS           43-1
#define RES_OFF_CENT_HT             46-1
#define RES_OFF_CENT_COOL           56-1
#define RES_OFF_GARAGE_BASEMENT     66-1
#define RES_OFF_CARPORT             76-1
#define RES_OFF_STALLS              86-1
#define RES_OFF_PATTERN             95-1
#define RES_OFF_BASEMENT_AREA1      105-1    // Total Bldg Area
#define RES_OFF_FLOOR1_AREA         114-1
#define RES_OFF_FLOOR2_AREA         123-1
#define RES_OFF_FLOOR3_AREA         132-1
#define RES_OFF_ATTIC_AREA          141-1
#define RES_OFF_FINISH_BASEMENT     150-1
#define RES_OFF_BASEMENT_AREA2      159-1
#define RES_OFF_ADDL_AREA           168-1
#define RES_OFF_STORIES             177-1
#define RES_OFF_GAR_CLS_ATT         189-1
#define RES_OFF_GAR_CLS_DET         199-1
#define RES_OFF_GARAGE_AREA         209-1
#define RES_OFF_POOL_YEAR           218-1
#define RES_OFF_WIDTH               227-1
#define RES_OFF_DEPTH               239-1
#define RES_OFF_LOT_SQFT_ACTUAL     251-1
#define RES_OFF_LOT_SQFT_USEABLE    260-1
#define RES_OFF_ACRES               269-1
#define RES_OFF_CORNER              278-1
#define RES_OFF_ALLEY               288-1
#define RES_OFF_NON_STREET_FRONT    298-1
#define RES_OFF_COMMON_GREEN        308-1
#define RES_OFF_COMMON_RECREAT      318-1
#define RES_OFF_WATERFRONT          328-1
*/
// RESIDENT - 07/2013
#define RES_SIZ_APN                 9
#define RES_SIZ_UNIT                4
#define RES_SIZ_SEQ                 3
#define RES_SIZ_CONSTR_YR           4
#define RES_SIZ_USE_CD1             2
#define RES_SIZ_LIV_RM              2
#define RES_SIZ_DIN_RM              2
#define RES_SIZ_KITCHEN             2
#define RES_SIZ_FAM_RM              2
#define RES_SIZ_BEDROOMS            2
#define RES_SIZ_DEN_LIBRARY         2
#define RES_SIZ_SUPPL_RM            2
#define RES_SIZ_ROOMS               3
#define RES_SIZ_BATHS               3
#define RES_SIZ_HALFBATHS           3
#define RES_SIZ_CENT_HT             10
#define RES_SIZ_CENT_COOL           10
#define RES_SIZ_GARAGE_BASEMENT     10
#define RES_SIZ_CARPORT             10
#define RES_SIZ_STALLS              9
#define RES_SIZ_PATTERN             10
#define RES_SIZ_BLDG_AREA           9
#define RES_SIZ_FLOOR1_AREA         9
#define RES_SIZ_FLOOR2_AREA         9
#define RES_SIZ_FLOOR3_AREA         9
#define RES_SIZ_ATTIC_AREA          9
#define RES_SIZ_FINISH_BASEMENT     9
#define RES_SIZ_BASEMENT_AREA       9
#define RES_SIZ_ADDL_AREA           9
#define RES_SIZ_STORIES             12
#define RES_SIZ_GAR_CLS_ATT         10
#define RES_SIZ_GAR_CLS_DET         10
#define RES_SIZ_GARAGE_AREA         9
#define RES_SIZ_POOL_YEAR           9
#define RES_SIZ_WIDTH               12
#define RES_SIZ_DEPTH               12
#define RES_SIZ_LOT_SQFT_ACTUAL     9
#define RES_SIZ_LOT_SQFT_USEABLE    9
#define RES_SIZ_ACRES               9
#define RES_SIZ_CORNER              10
#define RES_SIZ_ALLEY               10
#define RES_SIZ_NON_STREET_FRONT    10
#define RES_SIZ_COMMON_GREEN        10
#define RES_SIZ_COMMON_RECREAT      10
#define RES_SIZ_WATERFRONT          10

#define RES_OFF_APN                 1-1
#define RES_OFF_CONSTR_YR           10-1
#define RES_OFF_USE_CD1             14-1
#define RES_OFF_LIV_RM              16-1
#define RES_OFF_DIN_RM              18-1
#define RES_OFF_KITCHEN             20-1
#define RES_OFF_FAM_RM              22-1
#define RES_OFF_BEDROOMS            24-1
#define RES_OFF_DEN_LIBRARY         29-1
#define RES_OFF_SUPPL_RM            28-1
#define RES_OFF_ROOMS               30-1
#define RES_OFF_BATHS               33-1
#define RES_OFF_HALFBATHS           36-1
#define RES_OFF_CENT_HT             39-1
#define RES_OFF_CENT_COOL           49-1
#define RES_OFF_GARAGE_BASEMENT     59-1
#define RES_OFF_CARPORT             69-1
#define RES_OFF_STALLS              79-1
#define RES_OFF_PATTERN             88-1
#define RES_OFF_BLDG_AREA           98-1    // Total Living Area not including basement
#define RES_OFF_FLOOR1_AREA         107-1
#define RES_OFF_FLOOR2_AREA         116-1
#define RES_OFF_FLOOR3_AREA         125-1
#define RES_OFF_ATTIC_AREA          134-1
#define RES_OFF_FINISH_BASEMENT     143-1
#define RES_OFF_BASEMENT_AREA       152-1
#define RES_OFF_ADDL_AREA           161-1
#define RES_OFF_STORIES             170-1
#define RES_OFF_GARAGE_AREA         182-1
#define RES_OFF_POOL_YEAR           191-1
#define RES_OFF_WIDTH               200-1
#define RES_OFF_DEPTH               212-1
#define RES_OFF_LOT_SQFT_ACTUAL     224-1
#define RES_OFF_LOT_SQFT_USEABLE    233-1
#define RES_OFF_ACRES               242-1
#define RES_OFF_CORNER              251-1
#define RES_OFF_ALLEY               261-1
#define RES_OFF_NON_STREET_FRONT    271-1
#define RES_OFF_COMMON_GREEN        281-1
#define RES_OFF_COMMON_RECREAT      291-1
#define RES_OFF_WATERFRONT          301-1
//#define RES_OFF_GAR_CLS_ATT         182-1
//#define RES_OFF_GAR_CLS_DET         192-1
//#define RES_OFF_GARAGE_AREA         202-1
//#define RES_OFF_POOL_YEAR           211-1
//#define RES_OFF_WIDTH               220-1
//#define RES_OFF_DEPTH               232-1
//#define RES_OFF_LOT_SQFT_ACTUAL     244-1
//#define RES_OFF_LOT_SQFT_USEABLE    253-1
//#define RES_OFF_ACRES               262-1
//#define RES_OFF_CORNER              271-1
//#define RES_OFF_ALLEY               281-1
//#define RES_OFF_NON_STREET_FRONT    291-1
//#define RES_OFF_COMMON_GREEN        301-1
//#define RES_OFF_COMMON_RECREAT      311-1
//#define RES_OFF_WATERFRONT          321-1

typedef struct _tSmxCharSFR
{
   char Apn[RES_SIZ_APN];                              // 1  
   char Constr_Yr[RES_SIZ_CONSTR_YR];                  // 10 
   char UseCode[RES_SIZ_USE_CD1];                      // 14 
   char Liv_Rm[RES_SIZ_LIV_RM];                        // 16 
   char Din_Rm[RES_SIZ_DIN_RM];                        // 18 
   char Kitchen[RES_SIZ_KITCHEN];                      // 20 
   char Fam_Rm[RES_SIZ_FAM_RM];                        // 22 
   char Bedrooms[RES_SIZ_BEDROOMS];                    // 24 
   char Den_Library[RES_SIZ_DEN_LIBRARY];              // 29 
   char Suppl_Rm[RES_SIZ_SUPPL_RM];                    // 28 
   char Rooms[RES_SIZ_ROOMS];                          // 30 
   char Baths[RES_SIZ_BATHS];                          // 33 
   char HalfBaths[RES_SIZ_HALFBATHS];                  // 36 
   char Cent_Ht[RES_SIZ_CENT_HT];                      // 39 
   char Cent_Cool[RES_SIZ_CENT_COOL];                  // 49 
   char Garage_Basement[RES_SIZ_GARAGE_BASEMENT];      // 59 
   char Carport[RES_SIZ_CARPORT];                      // 69 
   char Stalls[RES_SIZ_STALLS];                        // 79 
   char Pattern[RES_SIZ_PATTERN];                      // 88 
   char BldgArea[RES_SIZ_BLDG_AREA];                   // 98 
   char Floor1_Area[RES_SIZ_FLOOR1_AREA];              // 107
   char Floor2_Area[RES_SIZ_FLOOR2_AREA];              // 116
   char Floor3_Area[RES_SIZ_FLOOR3_AREA];              // 125
   char Attic_Area[RES_SIZ_ATTIC_AREA];                // 134
   char Finish_Basement[RES_SIZ_FINISH_BASEMENT];      // 143
   char Basement_Area[RES_SIZ_BASEMENT_AREA];          // 152
   char Addl_Area[RES_SIZ_ADDL_AREA];                  // 161
   char Stories[RES_SIZ_STORIES];                      // 170
   char Garage_Area[RES_SIZ_GARAGE_AREA];              // 182 - New layout 9/10/2018
   char Pool_Year[RES_SIZ_POOL_YEAR];                  // 191
   char Width[RES_SIZ_WIDTH];                          // 200
   char Depth[RES_SIZ_DEPTH];                          // 212
   char Lot_Sqft_Actual[RES_SIZ_LOT_SQFT_ACTUAL];      // 224
   char Lot_Sqft_Useable[RES_SIZ_LOT_SQFT_USEABLE];    // 233
   char Acres[RES_SIZ_ACRES];                          // 242
   char Corner[RES_SIZ_CORNER];                        // 251
   char Alley[RES_SIZ_ALLEY];                          // 261
   char Non_Street_Front[RES_SIZ_NON_STREET_FRONT];    // 271
   char Common_Green[RES_SIZ_COMMON_GREEN];            // 281
   char Common_Recreat[RES_SIZ_COMMON_RECREAT];        // 291
   char Waterfront[RES_SIZ_WATERFRONT];                // 301
   //char Gar_Cls_Att[RES_SIZ_GAR_CLS_ATT];              // 182
   //char Gar_Cls_Det[RES_SIZ_GAR_CLS_DET];              // 192
   //char Garage_Area[RES_SIZ_GARAGE_AREA];              // 202
   //char Pool_Year[RES_SIZ_POOL_YEAR];                  // 211
   //char Width[RES_SIZ_WIDTH];                          // 220
   //char Depth[RES_SIZ_DEPTH];                          // 232
   //char Lot_Sqft_Actual[RES_SIZ_LOT_SQFT_ACTUAL];      // 244
   //char Lot_Sqft_Useable[RES_SIZ_LOT_SQFT_USEABLE];    // 253
   //char Acres[RES_SIZ_ACRES];                          // 262
   //char Corner[RES_SIZ_CORNER];                        // 271
   //char Alley[RES_SIZ_ALLEY];                          // 281
   //char Non_Street_Front[RES_SIZ_NON_STREET_FRONT];    // 291
   //char Common_Green[RES_SIZ_COMMON_GREEN];            // 301
   //char Common_Recreat[RES_SIZ_COMMON_RECREAT];        // 311
   //char Waterfront[RES_SIZ_WATERFRONT];                // 321
} SMX_CHAR_SFR;

#define APT_OFF_APN                           1-1
#define APT_OFF_YR_BLT                       10-1
#define APT_OFF_USE_CD                       19-1
#define APT_OFF_TYPE1                        21-1
#define APT_OFF_NO_OF_BLDGS                  31-1
#define APT_OFF_NO_OF_STORIES                40-1
#define APT_OFF_GROSS_BLDG_AREA              49-1
#define APT_OFF_NET_RENTABLE_AREA            58-1
#define APT_OFF_STUDIOS                      67-1
#define APT_OFF_1_BED1_BATH                  76-1
#define APT_OFF_1_BED1_BATH_AREA             85-1
#define APT_OFF_2_BED1_BATH                  94-1
#define APT_OFF_2_BED1_BATH_AREA             103-1
#define APT_OFF_2_BED2_BATH                  112-1
#define APT_OFF_2_BED2_BATH_AREA             121-1
#define APT_OFF_BEDRMS                       130-1
#define APT_OFF_DIN_RMS                      139-1
#define APT_OFF_OTHR_RMS                     148-1
#define APT_OFF_TOT_RMS                      157-1
#define APT_OFF_BATHS1                       166-1
#define APT_OFF_HALF_BATHS                   175-1
#define APT_OFF_LAUNDRY_RM                   184-1
#define APT_OFF_STORAGE_RM                   194-1
#define APT_OFF_REC_RM                       204-1
#define APT_OFF_GYM                          214-1
#define APT_OFF_GARGAGE_DETATCHED            224-1
#define APT_OFF_MULTI_STORY                  234-1
#define APT_OFF_FINISHED                     244-1
#define APT_OFF_GARAGE_STALLS_W_DOORS        254-1
#define APT_OFF_GARAGE_STALLS_NO_DOORS       263-1
#define APT_OFF_CARPORT_STALLS               272-1
#define APT_OFF_OPEN_SPC_STALLS              281-1
#define APT_OFF_ELEVATOR                     290-1    // A,E,G,P,Y
#define APT_OFF_FIREPLACES                   300-1
#define APT_OFF_LOT_WIDTH                    309-1
#define APT_OFF_LOT_DEPTH                    321-1
#define APT_OFF_ACRES1                       333-1
#define APT_OFF_LOT_SQFT_ACTUAL1             342-1
#define APT_OFF_LOT_SQFT_USABLE              351-1
#define APT_OFF_GRADE                        360-1
#define APT_OFF_CUL_DE_SAC                   370-1
#define APT_OFF_CORNER1                      380-1
#define APT_OFF_NON_ST_FRONT                 390-1
#define APT_OFF_WATERFRONT1                  400-1
#define APT_OFF_DOCKING_RIGHTS               410-1
#define APT_OFF_COMMON_GREEN1                420-1
#define APT_OFF_COMMON_RECREATION            430-1
#define APT_OFF_EASEMENT                     440-1
#define APT_OFF_UNITS_ALLOWED                450-1


#define APT_SIZ_APN                          9
#define APT_SIZ_YR_BLT                       9
#define APT_SIZ_USE_CD                       2
#define APT_SIZ_TYPE1                        10
#define APT_SIZ_NO_OF_BLDGS                  9
#define APT_SIZ_NO_OF_STORIES                9
#define APT_SIZ_GROSS_BLDG_AREA              9
#define APT_SIZ_NET_RENTABLE_AREA            9
#define APT_SIZ_STUDIOS                      9
#define APT_SIZ_1_BED1_BATH                  9
#define APT_SIZ_1_BED1_BATH_AREA             9
#define APT_SIZ_2_BED1_BATH                  9
#define APT_SIZ_2_BED1_BATH_AREA             9
#define APT_SIZ_2_BED2_BATH                  9
#define APT_SIZ_2_BED2_BATH_AREA             9
#define APT_SIZ_BEDRMS                       9
#define APT_SIZ_DIN_RMS                      9
#define APT_SIZ_OTHR_RMS                     9
#define APT_SIZ_TOT_RMS                      9
#define APT_SIZ_BATHS1                       9
#define APT_SIZ_HALF_BATHS                   9
#define APT_SIZ_LAUNDRY_RM                   10
#define APT_SIZ_STORAGE_RM                   10
#define APT_SIZ_REC_RM                       10
#define APT_SIZ_GYM                          10
#define APT_SIZ_GARGAGE_DETATCHED            10
#define APT_SIZ_MULTI_STORY                  10
#define APT_SIZ_FINISHED                     10
#define APT_SIZ_GARAGE_STALLS_W_DOORS        9
#define APT_SIZ_GARAGE_STALLS_NO_DOORS       9
#define APT_SIZ_CARPORT_STALLS               9
#define APT_SIZ_OPEN_SPC_STALLS              9
#define APT_SIZ_ELEVATOR                     10
#define APT_SIZ_FIREPLACES                   9
#define APT_SIZ_LOT_WIDTH                    12
#define APT_SIZ_LOT_DEPTH                    12
#define APT_SIZ_ACRES1                       9
#define APT_SIZ_LOT_SQFT_ACTUAL1             9
#define APT_SIZ_LOT_SQFT_USABLE              9
#define APT_SIZ_GRADE                        10
#define APT_SIZ_CUL_DE_SAC                   10
#define APT_SIZ_CORNER1                      10
#define APT_SIZ_NON_ST_FRONT                 10
#define APT_SIZ_WATERFRONT1                  10
#define APT_SIZ_DOCKING_RIGHTS               10
#define APT_SIZ_COMMON_GREEN1                10
#define APT_SIZ_COMMON_RECREATION            10
#define APT_SIZ_EASEMENT                     10
#define APT_SIZ_UNITS_ALLOWED                9

typedef struct _tSmxCharAPT
{
   char Apn[APT_SIZ_APN];
   char Yr_Blt[APT_SIZ_YR_BLT];
   char UseCode[APT_SIZ_USE_CD];
   char Type[APT_SIZ_TYPE1];
   char Bldgs[APT_SIZ_NO_OF_BLDGS];
   char No_Of_Stories[APT_SIZ_NO_OF_STORIES];
   char Gross_Bldg_Area[APT_SIZ_GROSS_BLDG_AREA];
   char Net_Rentable_Area[APT_SIZ_NET_RENTABLE_AREA];
   char Studios[APT_SIZ_STUDIOS];
   char Bed11_Bath[APT_SIZ_1_BED1_BATH];
   char Bed11_Bath_Area[APT_SIZ_1_BED1_BATH_AREA];
   char Bed21_Bath[APT_SIZ_2_BED1_BATH];
   char Bed21_Bath_Area[APT_SIZ_2_BED1_BATH_AREA];
   char Bed22_Bath[APT_SIZ_2_BED2_BATH];
   char Bed22_Bath_Area[APT_SIZ_2_BED2_BATH_AREA];
   char Bedrms[APT_SIZ_BEDRMS];
   char Dining_Rms[APT_SIZ_DIN_RMS];
   char Othr_Rms[APT_SIZ_OTHR_RMS];
   char Rooms[APT_SIZ_TOT_RMS];
   char Baths[APT_SIZ_BATHS1];
   char Half_Baths[APT_SIZ_HALF_BATHS];
   char Laundry_Rm[APT_SIZ_LAUNDRY_RM];
   char Storage_Rm[APT_SIZ_STORAGE_RM];
   char Rec_Rm[APT_SIZ_REC_RM];
   char Gym[APT_SIZ_GYM];
   char Gargage_Detatched[APT_SIZ_GARGAGE_DETATCHED];
   char Multi_Story[APT_SIZ_MULTI_STORY];
   char Finished[APT_SIZ_FINISHED];
   char Garage_Stalls_W_Doors[APT_SIZ_GARAGE_STALLS_W_DOORS];
   char Garage_Stalls_No_Doors[APT_SIZ_GARAGE_STALLS_NO_DOORS];
   char Carport_Stalls[APT_SIZ_CARPORT_STALLS];
   char Open_Spc_Stalls[APT_SIZ_OPEN_SPC_STALLS];
   char Elevator[APT_SIZ_ELEVATOR];
   char FirePlaces[APT_SIZ_FIREPLACES];
   char Lot_Width[APT_SIZ_LOT_WIDTH];
   char Lot_Depth[APT_SIZ_LOT_DEPTH];
   char Acres[APT_SIZ_ACRES1];
   char Lot_Sqft_Actual[APT_SIZ_LOT_SQFT_ACTUAL1];
   char Lot_Sqft_Usable[APT_SIZ_LOT_SQFT_USABLE];
   char Grade[APT_SIZ_GRADE];
   char Cul_de_sac[APT_SIZ_CUL_DE_SAC];
   char Corner[APT_SIZ_CORNER1];
   char Non_St_Front[APT_SIZ_NON_ST_FRONT];
   char Waterfront[APT_SIZ_WATERFRONT1];
   char Docking_Rights[APT_SIZ_DOCKING_RIGHTS];
   char Common_Green[APT_SIZ_COMMON_GREEN1];
   char Common_Recreation[APT_SIZ_COMMON_RECREATION];
   char Easement[APT_SIZ_EASEMENT];
   char Units_Allowed[APT_SIZ_UNITS_ALLOWED];
} SMX_CHAR_APT;

#define SMX_SIZ_TRA                 6
#define SMX_SIZ_NAME_1              40
#define SMX_SIZ_NAME_2              40
#define SMX_SIZ_NAME_3              40
#define SMX_SIZ_MADDR1              60
#define SMX_SIZ_MADDR2              60
#define SMX_SIZ_MADDR3              60
#define SMX_SIZ_MCITY               20
#define SMX_SIZ_MSTATE              2
#define SMX_SIZ_MZIP                5
#define SMX_SIZ_MZIP4               4
#define SMX_SIZ_MCOUNTRY            4
#define SMX_SIZ_ST_NUM              8
#define SMX_SIZ_TO_ST_NUM           8
#define SMX_SIZ_FRACTION            3
#define SMX_SIZ_DIRECTION           3
#define SMX_SIZ_NAME                32
#define SMX_SIZ_TYPE                4
#define SMX_SIZ_UNITNO              8
#define SMX_SIZ_CITY                20
#define SMX_SIZ_USE_CODE            2
#define SMX_SIZ_LEGAL_DESC_1        55
#define SMX_SIZ_LEGAL_DESC_2        55
#define SMX_SIZ_LEGAL_DESC_3        55
#define SMX_SIZ_LEGAL_DESC_4        55
#define SMX_SIZ_LAND                12
#define SMX_SIZ_IMPROV              12
#define SMX_SIZ_ROOT                12
#define SMX_SIZ_MINERALRTS          12
#define SMX_SIZ_PERS_PROP           12
#define SMX_SIZ_TRADE_FIXT          12
#define SMX_SIZ_EXEMP_CODE          4
#define SMX_SIZ_EXEMP_1             12
#define SMX_SIZ_EXEMP_2             12
#define SMX_SIZ_NET                 12
#define SMX_SIZ_RECORDING_DATE      8
#define SMX_SIZ_RECORDING_NO        11
#define SMX_SIZ_SALE_DATE           8
#define SMX_SIZ_SALE_AMT            12

#define SMX_OFF_APN                  1-1
#define SMX_OFF_TRA                  10-1
#define SMX_OFF_NAME_1               16-1
#define SMX_OFF_NAME_2               56-1
#define SMX_OFF_NAME_3               96-1
#define SMX_OFF_MADDR1              136-1
#define SMX_OFF_MADDR2              196-1
#define SMX_OFF_MADDR3              256-1
#define SMX_OFF_MCITY               316-1
#define SMX_OFF_MSTATE              336-1
#define SMX_OFF_MZIP                338-1
#define SMX_OFF_MCOUNTRY            347-1
#define SMX_OFF_ST_NUM              351-1
#define SMX_OFF_TO_ST_NUM           359-1
#define SMX_OFF_FRACTION            367-1
#define SMX_OFF_DIRECTION           370-1
#define SMX_OFF_NAME                373-1
#define SMX_OFF_TYPE                405-1
#define SMX_OFF_UNITNO              409-1
#define SMX_OFF_CITY                417-1
#define SMX_OFF_USE_CODE            437-1
#define SMX_OFF_LEGAL_DESC_1        439-1
#define SMX_OFF_LEGAL_DESC_2        494-1
#define SMX_OFF_LEGAL_DESC_3        549-1
#define SMX_OFF_LEGAL_DESC_4        604-1
#define SMX_OFF_LAND                659-1
#define SMX_OFF_IMPROV              671-1
#define SMX_OFF_ROOT                683-1
#define SMX_OFF_MINERALRTS          695-1
#define SMX_OFF_PERS_PROP           707-1
#define SMX_OFF_TRADE_FIXT          719-1
#define SMX_OFF_EXEMP_CODE          731-1
#define SMX_OFF_EXEMP_1             735-1
#define SMX_OFF_EXEMP_2             747-1
#define SMX_OFF_NET                 759-1
#define SMX_OFF_RECORDING_DATE      771-1
#define SMX_OFF_RECORDING_NO        779-1
#define SMX_OFF_SALE_DATE           790-1
#define SMX_OFF_SALE_AMT            798-1

typedef struct _tSmxRoll
{
   char Apn[SMX_SIZ_APN];
   char Tra[SMX_SIZ_TRA];
   char Name_1[SMX_SIZ_NAME_1];
   char Name_2[SMX_SIZ_NAME_2];
   char Name_3[SMX_SIZ_NAME_3];
   char Maddr1[SMX_SIZ_MADDR1];                    // 136
   char Maddr2[SMX_SIZ_MADDR2];
   char Maddr3[SMX_SIZ_MADDR3];
   char Mcity[SMX_SIZ_MCITY];
   char Mstate[SMX_SIZ_MSTATE];
   char Mzip[SMX_SIZ_MZIP];
   char Mzip4[SMX_SIZ_MZIP4];
   char Mcountry[SMX_SIZ_MCOUNTRY];
   char St_Num[SMX_SIZ_ST_NUM];                    // 351
   char To_St_Num[SMX_SIZ_TO_ST_NUM];
   char Fraction[SMX_SIZ_FRACTION];
   char Direction[SMX_SIZ_DIRECTION];
   char Name[SMX_SIZ_NAME];
   char Type[SMX_SIZ_TYPE];
   char UnitNo[SMX_SIZ_UNITNO];                    // 409 - dir may appear here
   char City[SMX_SIZ_CITY];                        // 417
   char Use_Code[SMX_SIZ_USE_CODE];
   char Legal_Desc_1[SMX_SIZ_LEGAL_DESC_1];        // 439
   char Legal_Desc_2[SMX_SIZ_LEGAL_DESC_2];
   char Legal_Desc_3[SMX_SIZ_LEGAL_DESC_3];
   char Legal_Desc_4[SMX_SIZ_LEGAL_DESC_4];
   char Land[SMX_SIZ_LAND];
   char Improv[SMX_SIZ_IMPROV];
   char Root[SMX_SIZ_ROOT];
   char Mineral[SMX_SIZ_MINERALRTS];
   char Pers_Prop[SMX_SIZ_PERS_PROP];
   char Trade_Fixt[SMX_SIZ_TRADE_FIXT];
   char Exemp_Code[SMX_SIZ_EXEMP_CODE];
   char Exemp_1[SMX_SIZ_EXEMP_1];
   char Exemp_2[SMX_SIZ_EXEMP_2];
   char Net[SMX_SIZ_NET];
   char Recording_Date[SMX_SIZ_RECORDING_DATE];
   char Recording_No[SMX_SIZ_RECORDING_NO];
   char Sale_Date[SMX_SIZ_SALE_DATE];
   char Sale_Amt[SMX_SIZ_SALE_AMT];
} SMX_ROLL;

#define  LO_CHAR_ASMT                 1-1
#define  LO_CHAR_USECAT               10-1
#define  LO_CHAR_YRBLT                18-1
#define  LO_CHAR_YREFF                22-1
#define  LO_CHAR_BLDGSQFT             26-1
#define  LO_CHAR_GARSQFT              35-1
#define  LO_CHAR_BEDS                 41-1
#define  LO_CHAR_FBATHS               43-1
#define  LO_CHAR_HBATHS               46-1
#define  LO_CHAR_ROOMS                49-1
#define  LO_CHAR_FP                   52-1
#define  LO_CHAR_STORIES              54-1
#define  LO_CHAR_BLDGS                58-1
#define  LO_CHAR_LOTACRES             60-1
#define  LO_CHAR_LOTSQFTS             69-1
#define  LO_CHAR_SPACE_NO             78-1
#define  LO_CHAR_PARKSPACES           84-1
#define  LO_CHAR_PARKTYPE             90-1
#define  LO_CHAR_HEATING              91-1
#define  LO_CHAR_COOLING              92-1
#define  LO_CHAR_POOL                 93-1
#define  LO_CHAR_VIEW                 94-1
#define  LO_CHAR_QUALITY              95-1
#define  LO_CHAR_CONDITION            96-1

typedef struct _tSmxCharRec
{
   char  Apn[SMX_SIZ_APN];
   char  UseCode[SIZ_USE_CO];
   char  Yr_Blt[SIZ_YR_BLT];
   char  Yr_Eff[SIZ_YR_EFF];
   char  BldgSqft[SIZ_BLDG_SF];
   char  GarSqft[SIZ_GAR_SQFT];
   char  NumBedrooms[SMX_SIZ_BEDROOMS];
   char  NumFullBaths[SMX_SIZ_BATHS];
   char  NumHalfBaths[SMX_SIZ_BATHS];
   char  Rooms[SMX_SIZ_ROOMS];
   char  FirePlaces[SIZ_FIRE_PL];
   char  Stories[SIZ_STORIES];
   char  Bldgs[SIZ_BLDGS];
   char  LotAcre[SIZ_LOT_ACRES];
   char  LotSqft[SIZ_LOT_SQFT];
   char  SpaceNo[SIZ_S_UNITNO];
   char  ParkSpaces[SIZ_PARK_SPACE];
   char  ParkType;
   char  Heating;                // Central heat
   char  Cooling;                // Central air
   char  Pool;
   char  View;
   char  Quality;
   char  Condition;
   char  BldgClass;
   char  BsmtSqft[SIZ_BSMT_SF];
   char  Units[SIZ_UNITS];
   char  Elevator;               // MULTIFAM - A,E,G,P,Y
   char  Elev_Freight;           // OFFICE - Y/N
   char  Elev_Bank;              // OFFICE - Y/N
   char  CRLF[2];
} LO_CHAR;

// Condo file
#define  SMX_COFF_APN              1  -1
#define  SMX_COFF_COMPLEX_NAME     10 -1
#define  SMX_COFF_APARTMENT_CONV   70 -1
#define  SMX_COFF_BUILDING_FLOORS  71 -1
#define  SMX_COFF_NO_OF_UNITS      73 -1
#define  SMX_COFF_SECURITY         77 -1
#define  SMX_COFF_ARCH_APPEAL      78 -1
#define  SMX_COFF_POOL             79 -1
#define  SMX_COFF_TENNIS_COURT     80 -1
#define  SMX_COFF_COMMON_GREEN     81 -1
#define  SMX_COFF_COMMON_REC       82 -1
#define  SMX_COFF_YEAR_BUILT       83 -1
#define  SMX_COFF_EFF_YEAR         87 -1
#define  SMX_COFF_QUALITY          91 -1
#define  SMX_COFF_CONDITION        92 -1
#define  SMX_COFF_LIVING_ROOM      93 -1
#define  SMX_COFF_DINING_ROOM      95 -1
#define  SMX_COFF_KITCHEN          97 -1
#define  SMX_COFF_FAMILY_ROOM      99 -1
#define  SMX_COFF_DEN_LIBRARY      101-1
#define  SMX_COFF_BEDROOMS         103-1
#define  SMX_COFF_SUPP_ROOM        105-1
#define  SMX_COFF_TOTAL_ROOMS      107-1
#define  SMX_COFF_BATHS            110-1
#define  SMX_COFF_BASE_AREA        112-1     // Bldg area
#define  SMX_COFF_GARAGE_AREA      117-1
#define  SMX_COFF_STORIES          121-1
#define  SMX_COFF_ENDUNIT          123-1
#define  SMX_COFF_GROUND_FLOOR     124-1
#define  SMX_COFF_TOP_FLOOR        125-1
#define  SMX_COFF_FIREPLACES       126-1
#define  SMX_COFF_CENTRAL_HEAT     128-1
#define  SMX_COFF_CENTRAL_AIR      129-1
#define  SMX_COFF_CARPORT          130-1
#define  SMX_COFF_STALLS           131-1

#define  SMX_CSIZ_APN              9
#define  SMX_CSIZ_COMPLEX_NAME     60
#define  SMX_CSIZ_APARTMENT_CONV   1
#define  SMX_CSIZ_BUILDING_FLOORS  2
#define  SMX_CSIZ_NO_OF_UNITS      4
#define  SMX_CSIZ_SECURITY         1
#define  SMX_CSIZ_ARCH_APPEAL      1
#define  SMX_CSIZ_POOL             1
#define  SMX_CSIZ_TENNIS_COURT     1
#define  SMX_CSIZ_COMMON_GREEN     1
#define  SMX_CSIZ_COMMON_REC       1
#define  SMX_CSIZ_YEAR_BUILT       4
#define  SMX_CSIZ_EFF_YEAR         4
#define  SMX_CSIZ_QUALITY          1
#define  SMX_CSIZ_CONDITION        1
#define  SMX_CSIZ_LIVING_ROOM      2
#define  SMX_CSIZ_DINING_ROOM      2
#define  SMX_CSIZ_KITCHEN          2
#define  SMX_CSIZ_FAMILY_ROOM      2
#define  SMX_CSIZ_DEN_LIBRARY      2
#define  SMX_CSIZ_BEDROOMS         2
#define  SMX_CSIZ_SUPP_ROOM        2
#define  SMX_CSIZ_TOTAL_ROOMS      3
#define  SMX_CSIZ_BATHS            2
#define  SMX_CSIZ_BASE_AREA        5
#define  SMX_CSIZ_GARAGE_AREA      4
#define  SMX_CSIZ_STORIES          2
#define  SMX_CSIZ_ENDUNIT          1
#define  SMX_CSIZ_GROUND_FLOOR     1
#define  SMX_CSIZ_TOP_FLOOR        1
#define  SMX_CSIZ_FIREPLACES       2
#define  SMX_CSIZ_CENTRAL_HEAT     1
#define  SMX_CSIZ_CENTRAL_AIR      1
#define  SMX_CSIZ_CARPORT          1
#define  SMX_CSIZ_STALLS           2

typedef struct _tSmxCharCondo
{
   char Apn[SMX_CSIZ_APN];
   char ComplexName[SMX_CSIZ_COMPLEX_NAME];
   char AptConversion;
   char Bldg_Floors[SMX_CSIZ_BUILDING_FLOORS];           // Stories in bldg
   char Units[SMX_CSIZ_NO_OF_UNITS];
   char Security;
   char Arch_appeal;
   char Pool;
   char Tennis_Court;
   char Common_Green;
   char Common_Recreation;
   char Yr_Blt[SMX_CSIZ_YEAR_BUILT];
   char Yr_Eff[SMX_CSIZ_EFF_YEAR];
   char Quality;                                         // 91
   char Condition;                                       // 92
   char Living_Rm[SMX_CSIZ_LIVING_ROOM];
   char Dining_Rm[SMX_CSIZ_DINING_ROOM];
   char Kitchen[SMX_CSIZ_KITCHEN];
   char Family_Rm[SMX_CSIZ_FAMILY_ROOM];
   char Den_Lib[SMX_CSIZ_DEN_LIBRARY];
   char Bedrms[SMX_CSIZ_BEDROOMS];
   char Supply_Rm[SMX_CSIZ_SUPP_ROOM];
   char Rooms[SMX_CSIZ_TOTAL_ROOMS];
   char Baths[SMX_CSIZ_BATHS];                           // 110
   char Base_Area[SMX_CSIZ_BASE_AREA];                   // 112
   char Garage_Area[SMX_CSIZ_GARAGE_AREA];               // 117
   char Stories[SMX_CSIZ_STORIES];                       // Stories in unit
   char EndUnit;                                         // Y/N
   char Ground_Floor;
   char Top_Floor;
   char FirePlaces[SMX_CSIZ_FIREPLACES];
   char Central_Heat;
   char Central_Air;
   char CarPort;                                         // Y/N
   char Stalls[SMX_CSIZ_STALLS];
   char CrLf[2];
} SMX_CHAR_CON;

// Hotel file
#define  SMX_HOFF_APN              1 -1
#define  SMX_HOFF_YEAR_BUILT       10-1
#define  SMX_HOFF_ACRES            14-1
#define  SMX_HOFF_GROSS_BLDGAREA   23-1
#define  SMX_HOFF_HOTEL_CLASS      32-1
#define  SMX_HOFF_LAND_AREA        42-1
#define  SMX_HOFF_LOCATION_CODE    51-1
#define  SMX_HOFF_CONST_CLASS      61-1
#define  SMX_HOFF_QUAL_CLASS       71-1
#define  SMX_HOFF_STORIES          81-1
#define  SMX_HOFF_TOTAL_ROOMS      90-1

#define  SMX_HSIZ_APN              9
#define  SMX_HSIZ_YEAR_BUILT       4
#define  SMX_HSIZ_ACRES            9
#define  SMX_HSIZ_GROSS_BLDGAREA   9
#define  SMX_HSIZ_HOTEL_CLASS      10
#define  SMX_HSIZ_LAND_AREA        9
#define  SMX_HSIZ_LOCATION_CODE    10
#define  SMX_HSIZ_CONST_CLASS      10
#define  SMX_HSIZ_QUAL_CLASS       10
#define  SMX_HSIZ_STORIES          9
#define  SMX_HSIZ_TOTAL_ROOMS      9

typedef struct _tSmxCharHotel
{
   char Apn[SMX_HSIZ_APN];
   char Yr_Blt[SMX_HSIZ_YEAR_BUILT];
   char Acres[SMX_HSIZ_ACRES];                  // 14 - V999
   char BldgSqft[SMX_HSIZ_GROSS_BLDGAREA];      // 23
   char Hotel_Class[SMX_HSIZ_HOTEL_CLASS];
   char LotSqft[SMX_HSIZ_LAND_AREA];            // 42
   char Loc_Code[SMX_HSIZ_LOCATION_CODE];
   char Const_Class[SMX_HSIZ_CONST_CLASS];      // 61
   char Quality[SMX_HSIZ_QUAL_CLASS];           // 71
   char Stories[SMX_HSIZ_STORIES];              // 81
   char Rooms[SMX_HSIZ_TOTAL_ROOMS];            // 90
   char CrLf[2];
} SMX_CHAR_HOT;

// Industrial
#define  SMX_IOFF_APN                  1  -1
#define  SMX_IOFF_MARKET_CLASS         10 -1
#define  SMX_IOFF_STORIES              20 -1
#define  SMX_IOFF_STORY_HEIGHT         32 -1
#define  SMX_IOFF_GROSS_BLDGAREA       41 -1
#define  SMX_IOFF_NET_RENT_AREA        50 -1
#define  SMX_IOFF_WAREHOUSE_AREA       59 -1
#define  SMX_IOFF_SHOP_AREA            68 -1
#define  SMX_IOFF_STORAGE_AREA         77 -1
#define  SMX_IOFF_REFRIGERATE_AREA     86 -1
#define  SMX_IOFF_OFFICE_AREA          95 -1
#define  SMX_IOFF_MEZZANINE_AREA       104-1
#define  SMX_IOFF_MISC_AREA            113-1
#define  SMX_IOFF_QUALITY_RANK         122-1
#define  SMX_IOFF_YEAR_BUILT           132-1
#define  SMX_IOFF_WALL_HEIGHT          141-1
#define  SMX_IOFF_BUILDING_WIDTH       150-1
#define  SMX_IOFF_BUILDING_LENGTH      159-1
#define  SMX_IOFF_LAND_SQFT            168-1
#define  SMX_IOFF_ACRES                177-1
#define  SMX_IOFF_PARKING_SPACES       189-1
#define  SMX_IOFF_DOCK_DOOR_COUNT      198-1

#define  SMX_ISIZ_APN                  9
#define  SMX_ISIZ_MARKET_CLASS         10
#define  SMX_ISIZ_STORIES              12
#define  SMX_ISIZ_STORY_HEIGHT         9
#define  SMX_ISIZ_GROSS_BLDGAREA       9
#define  SMX_ISIZ_NET_RENT_AREA        9
#define  SMX_ISIZ_WAREHOUSE_AREA       9
#define  SMX_ISIZ_SHOP_AREA            9
#define  SMX_ISIZ_STORAGE_AREA         9
#define  SMX_ISIZ_REFRIGERATE_AREA     9
#define  SMX_ISIZ_OFFICE_AREA          9
#define  SMX_ISIZ_MEZZANINE_AREA       9
#define  SMX_ISIZ_MISC_AREA            9
#define  SMX_ISIZ_QUALITY_RANK         10
#define  SMX_ISIZ_YEAR_BUILT           9
#define  SMX_ISIZ_WALL_HEIGHT          9
#define  SMX_ISIZ_BUILDING_WIDTH       9
#define  SMX_ISIZ_BUILDING_LENGTH      9
#define  SMX_ISIZ_LAND_SQFT            9
#define  SMX_ISIZ_ACRES                12
#define  SMX_ISIZ_PARKING_SPACES       9
#define  SMX_ISIZ_DOCK_DOOR_COUNT      9

typedef struct _tSmxCharInd
{
   char Apn[SMX_ISIZ_APN];
   char Market_Class[SMX_ISIZ_MARKET_CLASS];
   char Stories[SMX_ISIZ_STORIES];
   char Story_Height[SMX_ISIZ_STORY_HEIGHT];
   char BldgSqft[SMX_ISIZ_GROSS_BLDGAREA];
   char Rent_Area[SMX_ISIZ_MISC_AREA];
   char Warehouse_Area[SMX_ISIZ_MISC_AREA];
   char Shop_Area[SMX_ISIZ_MISC_AREA];
   char Storage_Area[SMX_ISIZ_MISC_AREA];
   char Refrigerate_Area[SMX_ISIZ_MISC_AREA];
   char Office_Area[SMX_ISIZ_MISC_AREA];
   char Mezzanine_Area[SMX_ISIZ_MISC_AREA];
   char Misc_Area[SMX_ISIZ_MISC_AREA];
   char Quality[SMX_ISIZ_QUALITY_RANK];
   char Yr_Blt[SMX_ISIZ_YEAR_BUILT];
   char Wall_Height[SMX_ISIZ_WALL_HEIGHT];
   char Bldg_Width[SMX_ISIZ_BUILDING_WIDTH];
   char Bldg_Lenght[SMX_ISIZ_BUILDING_LENGTH];
   char LotSqft[SMX_ISIZ_LAND_SQFT];
   char Acres[SMX_ISIZ_ACRES];                // V999
   char ParkSpaces[SMX_ISIZ_PARKING_SPACES];  // V999
   char Dock_Door_Count[SMX_ISIZ_DOCK_DOOR_COUNT];
   char CrLf[2];
} SMX_CHAR_IND;

// Mobile home
#define  SMX_MOFF_APN               1
#define  SMX_MOFF_SPACE_NO          10
#define  SMX_MOFF_MANUFACTURER      20
#define  SMX_MOFF_TRADE_NAME        30
#define  SMX_MOFF_YEAR_BUILT        40
#define  SMX_MOFF_FLOOR_PLAN        44
#define  SMX_MOFF_WIDTH             54
#define  SMX_MOFF_LENGTH            59
#define  SMX_MOFF_TOTAL_SQFT        64

#define  SMX_MSIZ_APN               9
#define  SMX_MSIZ_SPACE_NO          10
#define  SMX_MSIZ_MANUFACTURER      10
#define  SMX_MSIZ_TRADE_NAME        10
#define  SMX_MSIZ_YEAR_BUILT        4
#define  SMX_MSIZ_FLOOR_PLAN        10
#define  SMX_MSIZ_WIDTH             5
#define  SMX_MSIZ_LENGTH            5
#define  SMX_MSIZ_TOTAL_SQFT        6

typedef struct _tSmxCharMob
{
   char Apn[SMX_ISIZ_APN];
   char SpaceNo[SMX_MSIZ_SPACE_NO];
   char Manufacture[SMX_MSIZ_MANUFACTURER];
   char Trade_Name[SMX_MSIZ_TRADE_NAME];
   char Yr_Blt[SMX_MSIZ_YEAR_BUILT];
   char Floor_Plan[SMX_MSIZ_FLOOR_PLAN];
   char Width[SMX_MSIZ_WIDTH];
   char Length[SMX_MSIZ_LENGTH];
   char BldgSqft[SMX_MSIZ_TOTAL_SQFT];
   char CrLf[2];
} SMX_CHAR_MOB;

// Office
#define  SMX_OOFF_APN                     1  -1
#define  SMX_OOFF_MARKET_CLASS            10 -1
#define  SMX_OOFF_STORIES                 20 -1
#define  SMX_OOFF_STORY_HEIGHT            29 -1
#define  SMX_OOFF_GROSS_BLDGAREA          38 -1
#define  SMX_OOFF_NET_RENT_AREA           47 -1
#define  SMX_OOFF_STORAGE_AREA            56 -1
#define  SMX_OOFF_OFFICE_AREA             65 -1
#define  SMX_OOFF_MEZZANINE_AREA          74 -1
#define  SMX_OOFF_QUALITY_RANK            83 -1
#define  SMX_OOFF_YEAR_BUILT              93 -1
#define  SMX_OOFF_LOT_SQFT                105-1
#define  SMX_OOFF_LOT_ACRES               117-1
#define  SMX_OOFF_RENOVATE_YEAR           129-1
#define  SMX_OOFF_UNDERGRND_PARK_SPACE    138-1
#define  SMX_OOFF_UNDERGRND_PARK_AREA     147-1
#define  SMX_OOFF_FREIGHT_ELEVATOR        156-1
#define  SMX_OOFF_ELEVATOR_BANK           166-1
#define  SMX_OOFF_BASEMENT_AREA           176-1
#define  SMX_OOFF_SURFACE_PARK_SPACE      185-1
#define  SMX_OOFF_STRUCTURE_AREA          194-1
#define  SMX_OOFF_PARKING_STRUCTURE       203-1
#define  SMX_OOFF_STRUCTURE_PARK_SPACE    213-1
#define  SMX_OOFF_STRUCTURE_STORY         222-1
#define  SMX_OOFF_RETAIL_AREA             231-1

#define  SMX_OSIZ_APN                     9
#define  SMX_OSIZ_MARKET_CLASS            10
#define  SMX_OSIZ_STORIES                 9
#define  SMX_OSIZ_STORY_HEIGHT            9
#define  SMX_OSIZ_GROSS_BLDGAREA          9
#define  SMX_OSIZ_NET_RENT_AREA           9
#define  SMX_OSIZ_STORAGE_AREA            9
#define  SMX_OSIZ_OFFICE_AREA             9
#define  SMX_OSIZ_MEZZANINE_AREA          9
#define  SMX_OSIZ_QUALITY_RANK            10
#define  SMX_OSIZ_YEAR_BUILT              12
#define  SMX_OSIZ_LOT_SQFT                12
#define  SMX_OSIZ_LOT_ACRES               12
#define  SMX_OSIZ_RENOVATE_YEAR           9
#define  SMX_OSIZ_UNDERGRND_PARK_SPACE    9
#define  SMX_OSIZ_UNDERGRND_PARK_AREA     9
#define  SMX_OSIZ_FREIGHT_ELEVATOR        10
#define  SMX_OSIZ_ELEVATOR_BANK           10
#define  SMX_OSIZ_BASEMENT_AREA           9
#define  SMX_OSIZ_SURFACE_PARK_SPACE      9
#define  SMX_OSIZ_STRUCTURE_AREA          9
#define  SMX_OSIZ_PARKING_STRUCTURE       10
#define  SMX_OSIZ_STRUCTURE_PARK_SPACE    9
#define  SMX_OSIZ_STRUCTURE_STORY         9
#define  SMX_OSIZ_RETAIL_AREA             9

typedef struct _tSmxCharOff
{
   char Apn[SMX_OSIZ_APN];
   char Market_Class[SMX_OSIZ_MARKET_CLASS];
   char Stories[SMX_OSIZ_STORIES];
   char Story_Height[SMX_OSIZ_STORY_HEIGHT];
   char BldgSqft[SMX_OSIZ_GROSS_BLDGAREA];
   char Rent_Area[SMX_OSIZ_NET_RENT_AREA];
   char Storage_Area[SMX_OSIZ_STORAGE_AREA];
   char Office_Area[SMX_OSIZ_OFFICE_AREA];
   char Mezzanine_Area[SMX_OSIZ_MEZZANINE_AREA];
   char Quality[SMX_OSIZ_QUALITY_RANK];
   char Yr_Blt[SMX_OSIZ_YEAR_BUILT];
   char LotSqft[SMX_OSIZ_LOT_SQFT];
   char LotAcres[SMX_OSIZ_LOT_ACRES];           // V999
   char Renovate_Year[SMX_OSIZ_RENOVATE_YEAR];
   char Undgr_ParkSpace[SMX_OSIZ_UNDERGRND_PARK_SPACE];
   char Undgr_ParkArea[SMX_OSIZ_UNDERGRND_PARK_AREA];
   char Freight_Elavator[SMX_OSIZ_FREIGHT_ELEVATOR];
   char Elevator_Bank[SMX_OSIZ_ELEVATOR_BANK];
   char Basement_Area[SMX_OSIZ_BASEMENT_AREA];
   char Surf_ParkSpace[SMX_OSIZ_SURFACE_PARK_SPACE];
   char Structure_Area[SMX_OSIZ_STRUCTURE_AREA];
   char Parking_Structure[SMX_OSIZ_PARKING_STRUCTURE];
   char Structure_ParkSpaces[SMX_OSIZ_STRUCTURE_PARK_SPACE];
   char Structure_Stories[SMX_OSIZ_STRUCTURE_STORY];
   char Retail_Area[SMX_OSIZ_RETAIL_AREA];
   char CrLf[2];
} SMX_CHAR_OFF;

// SC - Secure Roll Data Transferable - Installments.csv
// SC - Secure Roll Data #2 - Installments.csv
#define  SRI_APN                 0
#define  SRI_BILL_NO             1
#define  SRI_INST_NO             2
#define  SRI_PAID_DATE           3
#define  SRI_PAID_STATUS         4
#define  SRI_COUNTY_TAX          5
#define  SRI_VOTER_TAX           6
#define  SRI_ASSESSMENTS         7
#define  SRI_TAX_AMT             8
#define  SRI_ACCT_TYPE           9
#define  SRI_FLDS                10

// SC - Secure Roll Data #1 - Accounts.csv
#define  SRA_APN                 0
#define  SRA_BILL_NO             1
#define  SRA_TRA                 2
#define  SRA_ASSESSEE            3
#define  SRA_M_ADDR_1            4
#define  SRA_M_CITY              5
#define  SRA_M_STATE             6
#define  SRA_M_ZIP               7
#define  SRA_LAND                8
#define  SRA_IMPR                9
#define  SRA_FIXT                10
#define  SRA_PP                  11
#define  SRA_USE_CODES           12
#define  SRA_EXE_VALUE           13
#define  SRA_EXE_CODE            14
#define  SRA_TAXABLE_VAL         15
#define  SRA_TOTAL_TAX           16
#define  SRA_LEGAL               17
#define  SRA_S_ADDR_1            18
#define  SRA_S_CITY              19
#define  SRA_TPA_CODE            20
#define  SRA_ESCROW_CO           21
#define  SRA_ASMT_YR             22
#define  SRA_ACCT_TYPE           23
#define  SRA_PAID_STATUS         24
#define  SRA_FLDS                25

// SC - Secure Roll Data #3 - Districts.csv
#define  SRD_APN                 0
#define  SRD_BILL_NO             1
#define  SRD_AMOUNT              2
#define  SRD_DIST_CODE           3
#define  SRD_DIST_NAME           4
#define  SRD_DIST_TYPE           5
#define  SRD_ACCT_TYPE           6
#define  SRD_FLDS                7

// SC - Redempt Roll Data File #1.csv
#define  RDF_APN                 0
#define  RDF_DEFAULT_NO          1
#define  RDF_ROLLYR              2
#define  RDF_TRA                 3
#define  RDF_DEFAULT_DATE        4
#define  RDF_REDEMPTION_DATE     5
#define  RDF_SCHEDULEDSALE_DATE  6
#define  RDF_POS_EXPECTED_DATE   7
#define  RDF_POS_RECORDED_DATE   8
#define  RDF_REDEMPTION_STATUS   9
#define  RDF_PAYMENT_PLAN_STATUS 10
#define  RDF_PAYMENT_PLAN_NO     11
#define  RDF_STANDARD_FLAGS      12
#define  RDF_CURRENT_OWNER       13
#define  RDF_M_ADDR_1            14
#define  RDF_M_ADDR_2            15
#define  RDF_M_ADDR_3            16
#define  RDF_M_ADDR_CITY         17
#define  RDF_M_ADDR_STATE        18
#define  RDF_M_ADDR_ZIP          19
#define  RDF_REDEMPTION_TOTAL    20
#define  RDF_PRIOR_PLAN_PAYMENTS 21
#define  RDF_NET_AMT_TO_REDEEM   22
#define  RDF_POS_REFERENCE_NO    23
#define  RDF_FLDS                24

// SC - Supp Roll Data #1 - Accounts.csv
#define  SUA_APN                 0
#define  SUA_BILL_NO             1
#define  SUA_ACCT_TYPE           2
#define  SUA_ASMT_YR             3
#define  SUA_TRA                 4
#define  SUA_ASSESSEE            5
#define  SUA_M_ADDR1             6
#define  SUA_M__CITY             7
#define  SUA_M_STATE             8
#define  SUA_M_ZIP               9
#define  SUA_S_ADDR1             10
#define  SUA_S_CITY              11
#define  SUA_LEGAL               12
#define  SUA_MAILED_DATE         13
#define  SUA_LAND_NEW            14
#define  SUA_LAND_OLD            15
#define  SUA_LAND_CUR            16
#define  SUA_IMPR_NEW            17
#define  SUA_IMPR_OLD            18
#define  SUA_IMPR_CUR            19
#define  SUA_FIXT_NEW            20
#define  SUA_FIXT_OLD            21
#define  SUA_FIXT_CUR            22
#define  SUA_EXE_NEW             23
#define  SUA_EXE_OLD             24
#define  SUA_EXE_CUR             25
#define  SUA_EXE_CODE            26
#define  SUA_TOTALVALUE_NEW      27
#define  SUA_TOTALVALUE_OLD      28
#define  SUA_TOTALVALUE_CUR      29
#define  SUA_TOTAL_TAX           30
#define  SUA_BALANCE_AMOUNT      31
#define  SUA_BALANCE_STATUS      32
#define  SUA_FLDS                33

// SC - Supp Roll Data #2 - Installments.csv
#define  SUI_APN                 0
#define  SUI_BILL_NO             1
#define  SUI_ACCT_TYPE           2
#define  SUI_INST_NO             3
#define  SUI_DUE_DATE            4
#define  SUI_PAID_DATE           5
#define  SUI_TAX_AMT             6
#define  SUI_BALANCE_AMOUNT      7
#define  SUI_BALANCE_STATUS      8
#define  SUI_FLDS                9

// Standard codes
// H=Homeowners
// D=Disabled Veterans
// V=Veterans
// C=Church
// L=Library
// M=Museums
// R=Religion
// T=Tribal Housing
// W=Welfare
// U=College
// P=Public School
// E=Cemetery
// G=Government
// I=Hospital
// S=School
// X=Misc exempt
// Y=Military

IDX_TBL4 SMX_Exemption[] = 
{
   "HO","H", 2,1,    // Homeowners Exemption
   "CE","E", 2,1,    // Cemetery Exemption
   "CH","C", 2,1,    // Church Exemption
   "CON","G",3,1,    // CONSULATE
   "CO","G", 2,1,    // COUNTY
   "CI","G", 2,1,    // CITY
   "DV","D", 2,1,    // Disabled Veterans Exemption
   "ES","S", 2,1,    // ELEMENTARY SCHOOL
   "FML","M",2,1,    // Free Public Library/Museum Exemption
   "HS","S", 2,1,    // HIGH SCHOOL
   "LSE","X",2,1,    // LEASE
   "PRC","U",2,1,    // PRIVATE COLLEGES
   "PSC","P",2,1,    // PUBLIC SCHOOLS
   "PUC","U",2,1,    // PUBLIC COLLEGES
   "RE","R", 2,1,    // Religious Exemption
   "SP","X", 2,1,    // SPECIAL
   "ST","G", 2,1,    // STATE OF CALIFORNIA
   "USA","G",3,1,    // UNITED STATES OF AMERICA
   "USD","P",3,1,    // UNIFID SCHOOL DIST
   "VE","V", 2,1,    // VETERANS
   "VO","V", 2,1,    // VETERANS ORGANIZATION
   "WE","W", 2,1,    // Welfare Exemption
   "BA","G", 2,1,    // BART
   "","",0,0
};

#endif
