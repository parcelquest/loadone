#ifndef  _MERGE_ALA_H
#define  _MERGE_ALA_H

#define  ROFF_APN             1
#define  ROFF_PRN_APN         15
#define  ROFF_TRA1            31
#define  ROFF_TRA2            34
#define  ROFF_SSTRNUM         38
#define  ROFF_SSTRNAME        49
#define  ROFF_SUNITNO         100
#define  ROFF_SCITY           111
#define  ROFF_SZIP            142
#define  ROFF_SZIP4           148
#define  ROFF_LAND            153
#define  ROFF_IMPR            166
#define  ROFF_CLCL_LAND       179
#define  ROFF_CLCA_IMPR       192
#define  ROFF_FIXTVAL         205
#define  ROFF_PPVAL           218
#define  ROFF_HPP             231
#define  ROFF_HOEXE           244
#define  ROFF_OTEXE           257
#define  ROFF_NET             270
#define  ROFF_DOC_PREF        283
#define  ROFF_DOC_SER         288
#define  ROFF_DATE            295
#define  ROFF_INDATE          304
#define  ROFF_OWNERS          313
#define  ROFF_CAREOF          364
#define  ROFF_ATTN            415
#define  ROFF_MSTRNAME        466
#define  ROFF_MUNITNO         527
#define  ROFF_MCITY           538
#define  ROFF_MZIP            569
#define  ROFF_MZIP4           575
#define  ROFF_M_WSEQ          580
#define  ROFF_M_CKDIGIT       583
#define  ROFF_M_EFF_DATE      585
#define  ROFF_SRC_CD          594
// 20020702
#define  ROFF_USECODE         596
#define  ROFF_SPLIT_PRCL      601   // 'Y' if APN is part of an economic unit
#define  ROFF_DELDATE         603   // Date APN was inactivated, Blank if APN is active

#define  RSIZ_APN             14
#define  RSIZ_PRN_APN         16
#define  RSIZ_TRA1            3
#define  RSIZ_TRA2            4
#define  RSIZ_SSTRNUM         11
#define  RSIZ_SSTRNAME        51
#define  RSIZ_SUNITNO         11
#define  RSIZ_SCITY           31
#define  RSIZ_SZIP            6
#define  RSIZ_SZIP4           5
#define  RSIZ_LAND            13
#define  RSIZ_IMPR            13
#define  RSIZ_CLCL_LAND       13
#define  RSIZ_CLCA_IMPR       13
#define  RSIZ_FIXTVAL         13
#define  RSIZ_PPVAL           13
#define  RSIZ_HPP             13
#define  RSIZ_HOEXE           13
#define  RSIZ_OTEXE           13
#define  RSIZ_NET             13
#define  RSIZ_DOC_PREF        5
#define  RSIZ_DOC_SER         7
#define  RSIZ_DATE            9
#define  RSIZ_OWNERS          51
#define  RSIZ_MSTRNAME        61
#define  RSIZ_MUNITNO         11
#define  RSIZ_MCITY           31
#define  RSIZ_MZIP            6
#define  RSIZ_MZIP4           5
#define  RSIZ_WSEQ            3
#define  RSIZ_CKDIGIT         2
#define  RSIZ_SRC_CD          2
#define  RSIZ_USECODE         5
#define  RSIZ_SPLIT_PRCL      2
#define  ALA_ROLL_FLDS        39

// Every field ended with a TAB
typedef struct _tAlaRoll
{  // 613-bytes roll record
   char  Apn[RSIZ_APN];
   char  Apn_Prn[RSIZ_PRN_APN];
   char  TRA_Prime[RSIZ_TRA1];
   char  TRA_Sec[RSIZ_TRA2];
   char  S_StreetNum[RSIZ_SSTRNUM];
   char  S_StreetName[RSIZ_SSTRNAME];
   char  S_UnitNo[RSIZ_SUNITNO];
   char  S_City[RSIZ_SCITY];
   char  S_Zip[RSIZ_SZIP];
   char  S_Zip4[RSIZ_SZIP4];
   char  Land[RSIZ_LAND];
   char  Impr[RSIZ_IMPR];
   char  CLCA_Land[RSIZ_CLCL_LAND];    // Same as Williamson Act
   char  CLCA_Impr[RSIZ_CLCA_IMPR];
   char  Fixt[RSIZ_FIXTVAL];
   char  Pers[RSIZ_PPVAL];             // Personal Property val
   char  Hpp[RSIZ_HPP];                // House PP val
   char  HOEX[RSIZ_HOEXE];
   char  OTEX[RSIZ_OTEXE];
   char  Net_Total[RSIZ_NET];
   char  Doc_Prefix[RSIZ_DOC_PREF];
   char  Doc_Series[RSIZ_DOC_SER];
   char  Doc_Dt[RSIZ_DATE];
   char  Doc_In_Date[RSIZ_DATE];
   char  Owners[RSIZ_OWNERS];
   char  M_CareOf[RSIZ_OWNERS];
   char  M_AttnName[RSIZ_OWNERS];
   char  M_Street[RSIZ_MSTRNAME];
   char  M_UnitNo[RSIZ_MUNITNO];
   char  M_City[RSIZ_MCITY];
   char  M_Zip[RSIZ_MZIP];
   char  M_Zip4[RSIZ_MZIP4];
   char  M_BC_WSEQ[RSIZ_WSEQ];
   char  M_BC_CKDIGIT[RSIZ_CKDIGIT];
   char  M_Effective_Dt[RSIZ_DATE];
   char  M_Source_Cd[RSIZ_SRC_CD];
   char  UseCode[RSIZ_USECODE];
   char  SplitParcel[RSIZ_SPLIT_PRCL];    // Y/N - Split parcel or parcel sold with another
   char  Del_Dt[RSIZ_DATE];               // yyyymmdd - Date parcel is deleted
   char  crlf[2];
   char  filler[3];                       // Reserve extra space
} ALA_ROLL;

// IE673 - 2020-2021.csv
#define  ALA_CHAR_APN         0
#define  ALA_CHAR_FMT_APN     1
#define  ALA_CHAR_TRA_PRI     2
#define  ALA_CHAR_TRA_SEC     3
#define  ALA_CHAR_S_STRNUM    4
#define  ALA_CHAR_S_STRNAME   5
#define  ALA_CHAR_S_STRUNIT   6
#define  ALA_CHAR_S_CITY      7
#define  ALA_CHAR_S_ZIP       8
#define  ALA_CHAR_S_ZIP4      9
#define  ALA_CHAR_USECODE     10
#define  ALA_CHAR_CHG_DATE    11    // m/d/yyyy
#define  ALA_CHAR_UNITS       12
#define  ALA_CHAR_CLASS       13
#define  ALA_CHAR_EFFYR       14
#define  ALA_CHAR_BLDGAREA    15
#define  ALA_CHAR_LOTSIZE     16
#define  ALA_CHAR_BLDGS       17
#define  ALA_CHAR_STORIES     18
#define  ALA_CHAR_ROOMS       19
#define  ALA_CHAR_BEDS        20
#define  ALA_CHAR_LI_RATIO    21
#define  ALA_CHAR_PCTOFFICE   22
#define  ALA_CHAR_ACRE        23
#define  ALA_CHAR_LANDTYPE    24
#define  ALA_CHAR_CONFORMITY  25
#define  ALA_CHAR_ADDITIONS   26
#define  ALA_CHAR_MISCAREA    27
#define  ALA_CHAR_RENTABLE    28    // Rentable sqft
#define  ALA_CHAR_YRBLT       29
#define  ALA_CHAR_BATHS       30
#define  ALA_CHAR_CONDO       31
#define  ALA_CHAR_ELEVATOR    32
#define  ALA_CHAR_POOL        33
#define  ALA_CHAR_POOLYEAR    34
#define  ALA_CHAR_REMODEL     35
#define  ALA_CHAR_CONDITION   36
#define  ALA_CHAR_PARKING     37
#define  ALA_CHAR_AMENITIES   38
#define  ALA_CHAR_UNITFLOOR   39
#define  ALA_CHAR_WALLHGT     40
#define  ALA_CHAR_VIEW        41
#define  ALA_CHAR_SLOPE       42
#define  ALA_CHAR_TOPO        43
#define  ALA_CHAR_HAZARDS     44
#define  ALA_CHAR_ECONUNIT    45
#define  ALA_CHAR_INA_DATE    46
#define  ALA_CHAR_FLDS        47

#define  COFF_APN             1
#define  COFF_PRN_APN         15
#define  COFF_TRA1            31
#define  COFF_TRA2            34
#define  COFF_SSTRNUM         38
#define  COFF_SSTRNAME        49
#define  COFF_SUNITNO         100
#define  COFF_SCITY           111
#define  COFF_SZIP            142
#define  COFF_SZIP4           148
#define  COFF_USECODE         153
#define  COFF_CHG_DATE        158   // YYYYMMDD
#define  COFF_UNITS           167
#define  COFF_BLDGCLS         173
#define  COFF_YREFF           178
#define  COFF_BLDGAREA        183
#define  COFF_LOTSIZE         191   // 9999999V9
#define  COFF_BLDGS           200
#define  COFF_STORIES         204   // 99V9
#define  COFF_ROOMS           208
#define  COFF_BEDS            212
#define  COFF_RATIO           216   // Land/Impr ratio
#define  COFF_PCT_OFFICE      220
#define  COFF_ACRE_FLG        224   // A if LotSize is in Acres
#define  COFF_LAND_FLG        226   // C=Corner lot, E=Excess land, L=Landlocked or no frontage, U=Unbuildable
                                    // R=Other restricted value (overhead h1gh voltage lines. flood control requirements. etc)
#define  COFF_CONFORM_FLG     228   // O=Over improvement, U=Under improvement, A=Atypical, C=Conform
#define  COFF_ADD_AREA        230   // This value is included in BldgArea
#define  COFF_MISC_AREA       238   // Unhabitable area
#define  COFF_RENTABLE_SPC    246   // Rentable sqft
#define  COFF_YRBLT           254
#define  COFF_BATHS           259   // 9999V9
#define  COFF_CONDO_TYPE      265   // A=Apartment, C=Conversion, T=Townhouse, M=Medical, B=Boat berth
                                    // O=Office, S=Store, W=Warehouse, L=Live/work studio
#define  COFF_ELEVATOR_FLG    267   // Y/N
#define  COFF_POOL_CODE       269   // Y 
#define  COFF_POOL_YRBLT      271   // Year the pool is built
#define  COFF_REMODEL_CODE    276   // A=Major addition, B=Bath, C=Conversion, K=Kitchen, M=Major remodel, R=Remodel T=type unknown
#define  COFF_CONDITION_CODE  280   // A,C,E,F,G,P,T,W (may follow by 2-digit year in which the condition of the property is observed)
                                    // C,T,W are ignored since they are not defined
// New fields added 10/04/2020
#define  COFF_PARKING         284   // C=Carport, G=Garage, X=Unknown
#define  COFF_AMENITIES       289   // (T)ennis court, (H)ot tub, (S)ecurity, (R)ecreation/exercise room
#define  COFF_UNIT_FLR        294   // Floor number in which the unit is located
#define  COFF_WALL_HGT        297   // 99V9 - Max first floor wall height in inches
#define  COFF_VIEW            301   // A,E,F,G,P
/* 
Slope would be viewed from the frontage road standpoint and indicates it would
be harder to build house if up or down.  Would have very minor impact on comp rating.  
Topography is used for terrain over a greater distance.
*/
#define  COFF_SLOPE           303   // (L)evel, (U)pslope, (D)ownslope, (S)ideslope
#define  COFF_TOPO            305   // (L)evel, (M)oderate, (S)teep, (H)illy, (F)lat
#define  COFF_HAZARDS         307   // T=Toxic, S=Slide zone, Q=Seismic, F=Fire storm (may combine multiple codes)
#define  COFF_ECON_UNIT       310   // Y or 1 - if APN is part of an economic unit
#define  COFF_INACTIVE_DATE   312

#define  CSIZ_APN             13
#define  CSIZ_PRN_APN         15
#define  CSIZ_TRA1            2
#define  CSIZ_TRA2            3
#define  CSIZ_SSTRNUM         10
#define  CSIZ_SSTRNAME        50
#define  CSIZ_SUNITNO         10
#define  CSIZ_SCITY           30
#define  CSIZ_SZIP            5
#define  CSIZ_SZIP4           4
#define  CSIZ_USECODE         4
#define  CSIZ_CHAR_CHGDATE    8
#define  CSIZ_UNITS           5
#define  CSIZ_BLDGCLS         4
#define  CSIZ_YREFF           4
#define  CSIZ_BLDGAREA        7
#define  CSIZ_LOTSIZE         8
#define  CSIZ_BLDGS           3
#define  CSIZ_STORIES         3
#define  CSIZ_ROOMS           3
#define  CSIZ_BEDS            3
#define  CSIZ_RATIO           3
#define  CSIZ_PCT_OFFICE      3
#define  CSIZ_ACRE_FLG        1
#define  CSIZ_LAND_FLG        1
#define  CSIZ_CONFORM_FLG     1
#define  CSIZ_ADD_AREA        7
#define  CSIZ_MISC_AREA       7
#define  CSIZ_RENTABLE_SPC    7
#define  CSIZ_YRBLT           4
#define  CSIZ_BATHS           5
#define  CSIZ_CONDO_TYPE      1
#define  CSIZ_ELEVATOR_FLG    1
#define  CSIZ_POOL_CODE       1
#define  CSIZ_POOL_YRBLT      4
#define  CSIZ_REMODEL_CODE    3
#define  CSIZ_CONDITION_CODE  3
#define  CSIZ_PARKING_CODE    4
#define  CSIZ_AMENITIES_CODE  4
#define  CSIZ_UNIT_FLOOR      2
#define  CSIZ_WALL_HEIGHT     3
#define  CSIZ_VIEW_CODE       1
#define  CSIZ_SLOPE_CODE      1
#define  CSIZ_TOPO_CODE       1
#define  CSIZ_HAZARDS_CODE    2
#define  CSIZ_ECON_UNIT_FLG   1
#define  CSIZ_INACTIVE_DATE   8

typedef struct _tAlaVariableUpdate
{  // 322-bytes char record
   char  Apn[CSIZ_APN+1];
   char  Apn_Prn[CSIZ_PRN_APN+1];
   char  TRA_Prime[CSIZ_TRA1+1];
   char  TRA_Sec[CSIZ_TRA2+1];
   char  S_StreetNum[CSIZ_SSTRNUM+1];
   char  S_StreetName[CSIZ_SSTRNAME+1];
   char  S_UnitNo[CSIZ_SUNITNO+1];
   char  S_City[CSIZ_SCITY+1];
   char  S_Zip[CSIZ_SZIP+1];
   char  S_Zip4[CSIZ_SZIP4+1];
   char  UseCode[CSIZ_USECODE+1];
   char  ChangeDate[CSIZ_CHAR_CHGDATE+1];
   char  Units[CSIZ_UNITS+1];
   char  BldgCls[CSIZ_BLDGCLS+1];
   char  YrEff[CSIZ_YREFF+1];
   char  BldgSqft[CSIZ_BLDGAREA+1];
   char  LotSize[CSIZ_LOTSIZE+1];
   char  Bldgs[CSIZ_BLDGS+1];
   char  Stories[CSIZ_STORIES+1];
   char  Rooms[CSIZ_ROOMS+1];
   char  Beds[CSIZ_BEDS+1];
   char  Ratio[CSIZ_RATIO+1];
   char  Pct_Office[CSIZ_PCT_OFFICE+1];
   char  Acre_Flg[CSIZ_ACRE_FLG+1];                  // 'A' if lot size is in acres, blank otherwise
   char  Land_Flg[CSIZ_ACRE_FLG+1];
   char  Conformity_Flg[CSIZ_ACRE_FLG+1];
   char  Add_Area[CSIZ_ADD_AREA+1];
   char  Misc_Area[CSIZ_MISC_AREA+1];
   char  Rentable_Spc[CSIZ_RENTABLE_SPC+1];
   char  YrBlt[CSIZ_YRBLT+1];
   char  Baths[CSIZ_BATHS+1];
   char  CondoType[CSIZ_CONDO_TYPE+1];
   char  Elevator_Flg[CSIZ_ELEVATOR_FLG+1];
   char  Pool_Code[CSIZ_POOL_CODE+1];
   char  Pool_Yrblt[CSIZ_POOL_YRBLT+1];
   char  Remodel_Code[CSIZ_REMODEL_CODE+1];
   char  Condition_Code[CSIZ_CONDITION_CODE+1];
   char  Parking_Code[CSIZ_PARKING_CODE+1];
   char  Amenities_Code[CSIZ_AMENITIES_CODE+1];
   char  Unit_Floor[CSIZ_UNIT_FLOOR+1];
   char  Wall_Height[CSIZ_WALL_HEIGHT+1];
   char  View_Code[CSIZ_VIEW_CODE+1];
   char  Slope_Code[CSIZ_SLOPE_CODE+1];
   char  Topography_Code[CSIZ_TOPO_CODE+1];
   char  Hazard_Code[CSIZ_HAZARDS_CODE+1];
   char  Econ_Unit_Flg[CSIZ_ECON_UNIT_FLG+1];        // 'Y' if APN is part of an economic unit
   char  Inactive_Date[CSIZ_INACTIVE_DATE+1];        // yyyymmdd - Date parcel is inactivated
   char  crlf[2];
} ALA_CHAR;

/***********************************************************************************
RECORED_TYPE            CHARACTER   1     1 - Document Information 2 - Grantor 3 - Grantee 4 - Parcel #
DOCUMENT_NBR            NUMBER      10
MULTI_TITLE_NO          NUMBER      4
RECORDING_DATE          CHARACTER   14    Date and Time of Recording MMDDYYYYHHMISS
DOCUMENT_TYPE           CHAR        12    Document Type. Do we need to translate these
NO_PAGES                NUMBER      4     NoPages ZeroFilled
COUNTY TAX              NUMBER      10    Zero Filled Implied Decimal
CITY TAX1               NUMBER      10    Zero Filled Implied Decimal
CITY CODE1              CHARACTER   3     3 Character City Code
CITY TAX2               NUMBER      10    Zero Filled Implied Decimal
CITY CODE2              CHARACTER   3     3 Character City Code
PCOR_FLAG               CHARACTER   1     N - No PCOR Y - PCOR is filed
NO_PAGES IN PCOR        NUMBER      4     NoPages ZeroFilled
NOTAXONLABEL_FLAG       CHARACTER   1     Y - Label is Printed without Tax N - Default
PCOR_SURCHARGE_FLAG     CHARACTER   1     Y - PCOR Surcharge is collected N - No Surcharge
PCOR_SURCHARGE_AMOUNT   NUMBER      6     Zero Filled Implied Decimal
FILLER                  CHRACTER    6     SPACES
************************************************************************************/
#define  DOCNUM_LEN     10
typedef struct _tAlaDoc
{
   char  RecType;                   // 0
   char  DocNumber[10];             // 1
   char  Multi_Title_No[4];         // 11
   char  DocDate[14];               // 15
   char  DocType[12];               // 29
   char  No_Pages[4];               // 41
   char  CountyTax[10];             // 45
   char  City_Tax1[10];
   char  City_Code1[3];
   char  City_Tax2[10];
   char  City_Code2[3];
   char  PCOR_Flag;
   char  PCOR_Pages[4];
   char  Label_Flag;
   char  PCOR_Surcharge_Flag;
   char  PCOR_Surcharge_Amt[6];
   char  filler[10];             // Add extra filler for CRLF
} ALA_DOC;

/***********************************************************************************
RECORED_TYPE   CHARACTER   1     2-Grantor 3-Grantee
DOCUMENT_NBR   NUMBER      10
MULTI_TITLE_NO NUMBER      4
GRANTOR SEQ    NUMBER      4     Grantor/grantee Sequence. First Grantor will be 0001 and so on
GRANTOR        CHRACTER    81
***********************************************************************************/

typedef struct _tAlaName
{
   char  RecType;
   char  DocNumber[10];
   char  Multi_Title_No[4];
   char  SeqNo[4];
   char  Name[81];
   char  filler[4];
} ALA_NAME;

/***********************************************************************************
RECORED_TYPE      CHARACTER   1     4 - Parcel #
DOCUMENT_NBR      NUMBER      10
MULTI_TITLE_NO    NUMBER      4
LEGAL SEQ         NUMBER      4     Legal Sequence. First Parcel # will be 0001 and so on
PARCEL_NO PART 1  CHRACTER    4     First 4 characters of Parcel # Positions 1 to 4 of Parcel # Left Padded with spaces
PARCEL_NO PART 2  CHRACTER    4     Next 4 characters of Parcel # Positions 5 to 8 of Parcel #  Left Padded with spaces
PARCEL_NO PART 3  CHRACTER    3     Next 3 characters of Parcel # Positions 9 to 11 of Parcel # Left Padded with spaces
PARCEL_NO PART 4  CHRACTER    2     Next 2 characters of Parcel # Positions 12 to 13 of Parcel # Left Padded with spaces
FILLER            CHARACTER   68    Spaces
4200502851400000001 4860027026
***********************************************************************************/

typedef struct _tAlaApn
{
   char  RecType;
   char  DocNumber[10];
   char  Multi_Title_No[4];
   char  SeqNo[4];
   char  Apn1[4];
   char  Apn2[4];
   char  Apn3[3];
   char  Apn4[2];
   char  filler[72];
} ALA_APN;

#define  ALA_SALE_APN_S       0
#define  ALA_SALE_APN_D       1
#define  ALA_SALE_USECD       2
#define  ALA_SALE_USEDESC     3
#define  ALA_SALE_STRNUM      4
#define  ALA_SALE_STRLDIR     5
#define  ALA_SALE_STRNAME     6
#define  ALA_SALE_STRSFX      7
#define  ALA_SALE_STRRDIR     8
#define  ALA_SALE_UNITTYPE    9
#define  ALA_SALE_UNITNUM     10
#define  ALA_SALE_CITY        11
#define  ALA_SALE_STATE       12
#define  ALA_SALE_ZIP         13
#define  ALA_SALE_ZIP4        14
#define  ALA_SALE_DOC_DT      15
#define  ALA_SALE_XFER_DT     16
#define  ALA_SALE_DOC_PRE     17
#define  ALA_SALE_DOC_SER     18
#define  ALA_SALE_SALEAMT     19
#define  ALA_SALE_PARCEL_CNT  20
#define  ALA_SALE_NAME        21
#define  ALA_SALE_NAMETYPE    22

#define  ALA_SALE_NAMETYPECD  9
#define  ALA_SALE_TOKENS      23

// Tax file: ALCO-SECUR.txt
#define  TOFF_TRACER_NUMBER                     1 
#define  TOFF_RECORD_SEQ_KEY                    9 
#define  TOFF_APN                              10 
#define  TOFF_UTILITY_PARCEL                   23 
#define  TOFF_FILLER1                          24 
#define  TOFF_RESOLUTION_NUMBER                38 
#define  TOFF_POST_DATE                        47 
#define  TOFF_CODEAREA                         51 
#define  TOFF_FLOOD_ZONE                       56 
#define  TOFF_SITUS_POST_OFFICE                59 
#define  TOFF_SITUS_HOUSE_NUMBER               61 
#define  TOFF_SITUS_BLANK                      66 
#define  TOFF_SITUS_STREET                     67 
#define  TOFF_VALUE_LAND                       86 
#define  TOFF_VALUE_MACHINE_EQUIP              96 
#define  TOFF_VALUE_IMPROVEMENTS              106 
#define  TOFF_VALUE_BUSINESS_PERSPROP         116 
#define  TOFF_VALUE_HOUSEHLD_PERSPROP         126 
#define  TOFF_VALUE_HOMEOWNER_EXEMPTION       136 
#define  TOFF_VALUE_OTHER_EXEMPTION           146 
#define  TOFF_VALUE_NET_TOTAL                 156 
#define  TOFF_VALUE_PENALTY                   166 
#define  TOFF_PENALTY_PERCENT                 176 
#define  TOFF_HOUSEHLD_PENALTY_PERCENT        180 
#define  TOFF_EXEMPTION_CODE                  184 // H, 0-9
#define  TOFF_EXEMPTION_PERCENT               185 
#define  TOFF_PAYFWD_TRACER_SUFF              189 
#define  TOFF_ORIGINAL_BANK                   191 
#define  TOFF_BANK_CHANGE_DATE                194 
#define  TOFF_CURRENT_BANK_ALPHA              198 
#define  TOFF_DELINQ_SALE_NUMBER              201 
#define  TOFF_SENIOR_CITIZEN_FLAG             210 
#define  TOFF_FICHE_COMMENT_FLAG              211 // 3,4,5,7,8,9,E,BLANK
#define  TOFF_ILL_OR_PI_KEY                   212 
#define  TOFF_CURRENT_OWNERS_NAME             213 
#define  TOFF_LIENDATE_OWNERS_NAME            263 
#define  TOFF_MAIL_CARE_OF_NAME               293 
#define  TOFF_MAIL_STREET_ADDRESS             318 
#define  TOFF_MAIL_CITY_STATE                 343 // Could be foreign country code
#define  TOFF_MAIL_ZIP                        368 // FORGN if foreign addr
#define  TOFF_MAIL_ZIP4                       373 
#define  TOFF_TAXRATE_AV                      377 // +99.9999
#define  TOFF_TAXRATE_FLOOD                   385 
#define  TOFF_CHECKDIG_TAXAMT                 393 
#define  TOFF_CHECKDIG_TRACER_1ST             394 
#define  TOFF_CHECKDIG_TRACER_2ND             395 
#define  TOFF_RUN_NUMBER_LAST_CHANGED         396 
#define  TOFF_LAST_DUPE_REVERSE_SEQ_KEY       401 
#define  TOFF_BANK_COMMENT                    402 
#define  TOFF_1ST_INSTLMT_PMT_DATE            435 
#define  TOFF_1ST_INSTLMT_PMT_BATCH           441 
#define  TOFF_1ST_INSTLMT_PMT_TELLER          444 
#define  TOFF_1ST_INSTLMT_PMT_LOT             446 
#define  TOFF_1ST_INSTLMT_PMT_SEQ             447 
#define  TOFF_1ST_INSTLMT_PMT_CASH_CHK        450 
#define  TOFF_2ND_INSTLMT_PMT_DATE            451 
#define  TOFF_2ND_INSTLMT_PMT_BATCH           457 
#define  TOFF_2ND_INSTLMT_PMT_TELLER          460 
#define  TOFF_2ND_INSTLMT_PMT_LOT             462 
#define  TOFF_2ND_INSTLMT_PMT_SEQ             463 
#define  TOFF_2ND_INSTLMT_PMT_CASH_CHK        466 
#define  TOFF_REMINDER_BILL_FLAG              467 
#define  TOFF_FILLER2                         468 
#define  TOFF_SUPPLEMENTAL_ASSESS_FLAG        469 
#define  TOFF_ADJUSTMENT_NUMBER               470 
#define  TOFF_ADJ_DATE_POSTED                 475 // mmdd
#define  TOFF_ADJ_RESOLUTION_NUMBER           479 
#define  TOFF_ADJ_ACTION_CODE                 486 
#define  TOFF_ADJ_REASON_CODE                 488 
#define  TOFF_ADJ_PENALTY_WAIVER_FLAG         490 
#define  TOFF_ADJ_BILL_PRINT_FLAG             491 // A,R
#define  TOFF_REQ_CURR_PRIOR_FLAG             492 // C,P
#define  TOFF_ADJ_DATA1_YEAR                  493 // yy
#define  TOFF_ADJ_DATA1_CITATION              495 
#define  TOFF_CANCEL_DATE_POSTED              500 
#define  TOFF_CANCEL_RESOLUTION_NUMBER        504 
#define  TOFF_CANCEL_NUMBER                   511 
#define  TOFF_CANCEL_ACTION_CODE              516 
#define  TOFF_CANCEL_REASON_CODE              518 
#define  TOFF_TAX_GRAND_TOTAL_1ST             520 // +9999999.99
#define  TOFF_TAX_GRAND_TOTAL_2ND             531 
#define  TOFF_TAX_INTEREST_1ST                542 
#define  TOFF_TAX_INTEREST_2ND                553 
#define  TOFF_TAX_DELINQ_PENALTY_1ST          564 
#define  TOFF_TAX_DELINQ_PENALTY_2ND          575 
#define  TOFF_TAX_DELINQUENT_COST             586 
#define  TOFF_TAX_TOTAL_1ST                   597 // Total 1st inst
#define  TOFF_TAX_TOTAL_2ND                   608 
#define  TOFF_TAX_AV_1ST                      619 // AV only
#define  TOFF_TAX_AV_2ND                      630 
#define  TOFF_TAX_FLOOD_1ST                   641 
#define  TOFF_TAX_FLOOD_2ND                   652 
#define  TOFF_USE_CODE                        663 
#define  TOFF_MAIL_UNIT_NO                    667 
#define  TOFF_MA_BAR_CODE_WSEQ                671 
#define  TOFF_MA_BAR_CODE_CKDIGIT             673 
#define  TOFF_1ST_PYMT_STATION                674 
#define  TOFF_1ST_PYMT_CONSECUTIVE_NO         675 
#define  TOFF_2ND_PYMT_STATION                680 
#define  TOFF_2ND_PYMT_CONSECUTIVE_NO         681 
#define  TOFF_INSTALL1_DELQ_DUE_DATE          686 
#define  TOFF_INSTALL2_DELQ_DUE_DATE          694 
#define  TOFF_FILLER3                         702 
#define  TOFF_WEB_IVR_FLAG                   1302 
#define  TOFF_WAIVER_PRINT_FLAG              1303 
#define  TOFF_INSTALL1_DUE_DATE              1304 
#define  TOFF_INSTALL2_DUE_DATE              1310 
#define  TOFF_SPECIAL_LEGEND_PRE             1316 // Occurs 50 times
#define  TOFF_SPECIAL_LEGEND                 1318 
#define  TOFF_TAX_SPECIAL_1ST                1321 
#define  TOFF_TAX_SPECIAL_2ND                1336 

#define  TSIZ_TRACER_NUMBER                  8
#define  TSIZ_RECORD_SEQ_KEY                 1
#define  TSIZ_APN                            13
#define  TSIZ_UTILITY_PARCEL                 1
#define  TSIZ_FILLER1                        14
#define  TSIZ_RESOLUTION_NUMBER              9
#define  TSIZ_POST_DATE                      4
#define  TSIZ_CODEAREA                       5
#define  TSIZ_FLOOD_ZONE                     3
#define  TSIZ_SITUS_POST_OFFICE              2
#define  TSIZ_SITUS_HOUSE_NUMBER             5
#define  TSIZ_SITUS_BLANK                    1
#define  TSIZ_SITUS_STREET                   19
#define  TSIZ_VALUE_LAND                     10
#define  TSIZ_VALUE_MACHINE_EQUIP            10
#define  TSIZ_VALUE_IMPROVEMENTS             10
#define  TSIZ_VALUE_BUSINESS_PERSPROP        10
#define  TSIZ_VALUE_HOUSEHLD_PERSPROP        10
#define  TSIZ_VALUE_HOMEOWNER_EXEMPTION      10
#define  TSIZ_VALUE_OTHER_EXEMPTION          10
#define  TSIZ_VALUE_NET_TOTAL                10
#define  TSIZ_VALUE_PENALTY                  10
#define  TSIZ_PENALTY_PERCENT                4
#define  TSIZ_HOUSEHLD_PENALTY_PERCENT       4
#define  TSIZ_EXEMPTION_CODE                 1
#define  TSIZ_EXEMPTION_PERCENT              4
#define  TSIZ_PAYFWD_TRACER_SUFF             2
#define  TSIZ_ORIGINAL_BANK                  3
#define  TSIZ_BANK_CHANGE_DATE               4
#define  TSIZ_CURRENT_BANK_ALPHA             3
#define  TSIZ_DELINQ_SALE_NUMBER             9
#define  TSIZ_SENIOR_CITIZEN_FLAG            1
#define  TSIZ_FICHE_COMMENT_FLAG             1
#define  TSIZ_ILL_OR_PI_KEY                  1
#define  TSIZ_CURRENT_OWNERS_NAME            50
#define  TSIZ_LIENDATE_OWNERS_NAME           30
#define  TSIZ_MAIL_CARE_OF_NAME              25
#define  TSIZ_MAIL_STREET_ADDRESS            25
#define  TSIZ_MAIL_CITY_STATE                25
#define  TSIZ_MAIL_ZIP                       5
#define  TSIZ_MAIL_ZIP4                      4
#define  TSIZ_TAXRATE_AV                     8
#define  TSIZ_TAXRATE_FLOOD                  8
#define  TSIZ_CHECKDIG_TAXAMT                1
#define  TSIZ_CHECKDIG_TRACER_1ST            1
#define  TSIZ_CHECKDIG_TRACER_2ND            1
#define  TSIZ_RUN_NUMBER_LAST_CHANGED        5
#define  TSIZ_LAST_DUPE_REVERSE_SEQ_KEY      1
#define  TSIZ_BANK_COMMENT                   33
#define  TSIZ_1ST_INSTLMT_PMT_DATE           6
#define  TSIZ_1ST_INSTLMT_PMT_BATCH          3
#define  TSIZ_1ST_INSTLMT_PMT_TELLER         2
#define  TSIZ_1ST_INSTLMT_PMT_LOT            1
#define  TSIZ_1ST_INSTLMT_PMT_SEQ            3
#define  TSIZ_1ST_INSTLMT_PMT_CASH_CHK       1
#define  TSIZ_2ND_INSTLMT_PMT_DATE           6
#define  TSIZ_2ND_INSTLMT_PMT_BATCH          3
#define  TSIZ_2ND_INSTLMT_PMT_TELLER         2
#define  TSIZ_2ND_INSTLMT_PMT_LOT            1
#define  TSIZ_2ND_INSTLMT_PMT_SEQ            3
#define  TSIZ_2ND_INSTLMT_PMT_CASH_CHK       1
#define  TSIZ_REMINDER_BILL_FLAG             1
#define  TSIZ_FILLER2                        1
#define  TSIZ_SUPPLEMENTAL_ASSESS_FLAG       1
#define  TSIZ_ADJUSTMENT_NUMBER              5
#define  TSIZ_ADJ_DATE_POSTED                4
#define  TSIZ_ADJ_RESOLUTION_NUMBER          7
#define  TSIZ_ADJ_ACTION_CODE                2
#define  TSIZ_ADJ_REASON_CODE                2
#define  TSIZ_ADJ_PENALTY_WAIVER_FLAG        1
#define  TSIZ_ADJ_BILL_PRINT_FLAG            1
#define  TSIZ_REQ_CURR_PRIOR_FLAG            1
#define  TSIZ_ADJ_DATA1_YEAR                 2
#define  TSIZ_ADJ_DATA1_CITATION             5
#define  TSIZ_CANCEL_DATE_POSTED             4
#define  TSIZ_CANCEL_RESOLUTION_NUMBER       7
#define  TSIZ_CANCEL_NUMBER                  5
#define  TSIZ_CANCEL_ACTION_CODE             2
#define  TSIZ_CANCEL_REASON_CODE             2
#define  TSIZ_TAX_GRAND_TOTAL_1ST            11
#define  TSIZ_TAX_GRAND_TOTAL_2ND            11
#define  TSIZ_TAX_INTEREST_1ST               11
#define  TSIZ_TAX_INTEREST_2ND               11
#define  TSIZ_TAX_DELINQ_PENALTY_1ST         11
#define  TSIZ_TAX_DELINQ_PENALTY_2ND         11
#define  TSIZ_TAX_DELINQUENT_COST            11
#define  TSIZ_TAX_TOTAL_1ST                  11
#define  TSIZ_TAX_TOTAL_2ND                  11
#define  TSIZ_TAX_AV_1ST                     11
#define  TSIZ_TAX_AV_2ND                     11
#define  TSIZ_TAX_FLOOD_1ST                  11
#define  TSIZ_TAX_FLOOD_2ND                  11
#define  TSIZ_USE_CODE                       4
#define  TSIZ_MAIL_UNIT_NO                   4
#define  TSIZ_MA_BAR_CODE_WSEQ               2
#define  TSIZ_MA_BAR_CODE_CKDIGIT            1
#define  TSIZ_1ST_PYMT_STATION               1
#define  TSIZ_1ST_PYMT_CONSECUTIVE_NO        5
#define  TSIZ_2ND_PYMT_STATION               1
#define  TSIZ_2ND_PYMT_CONSECUTIVE_NO        5
#define  TSIZ_INSTALL1_DELQ_DUE_DATE         8
#define  TSIZ_INSTALL2_DELQ_DUE_DATE         8
#define  TSIZ_FILLER3                        600
#define  TSIZ_WEB_IVR_FLAG                   1
#define  TSIZ_WAIVER_PRINT_FLAG              1
#define  TSIZ_INSTALL1_DUE_DATE              6
#define  TSIZ_INSTALL2_DUE_DATE              6
#define  TSIZ_SPECIAL_LEGEND_PRE             2
#define  TSIZ_SPECIAL_LEGEND                 3
#define  TSIZ_TAX_SPECIAL_1ST                15
#define  TSIZ_TAX_SPECIAL_2ND                15
#define  SEC_MAX_SPCTAX                      50

typedef struct _tALA_SPCTAX
{
   char  Special_Legend_Pre         [TSIZ_SPECIAL_LEGEND_PRE];
   char  Special_Legend             [TSIZ_SPECIAL_LEGEND];
   char  Tax_Special_1St            [TSIZ_TAX_SPECIAL_1ST];
   char  Tax_Special_2Nd            [TSIZ_TAX_SPECIAL_2ND];
} ALA_SPCTAX;

typedef struct _tALCO_SEC
{
   char  Tracer_Number              [TSIZ_TRACER_NUMBER];
   char  Record_Seq_Key             [TSIZ_RECORD_SEQ_KEY];
   char  APN                        [TSIZ_APN];
   char  Utility_Parcel             [TSIZ_UTILITY_PARCEL];
   char  Filler1                    [TSIZ_FILLER1];
   char  Resolution_Number          [TSIZ_RESOLUTION_NUMBER];
   char  Post_Date                  [TSIZ_POST_DATE];
   char  TRA                        [TSIZ_CODEAREA];
   char  Flood_Zone                 [TSIZ_FLOOD_ZONE];
   char  Situs_Post_Office          [TSIZ_SITUS_POST_OFFICE];
   char  Situs_House_Number         [TSIZ_SITUS_HOUSE_NUMBER];
   char  Situs_Blank                [TSIZ_SITUS_BLANK];
   char  Situs_Street               [TSIZ_SITUS_STREET];
   char  Value_Land                 [TSIZ_VALUE_LAND];
   char  Value_Machine_Equip        [TSIZ_VALUE_MACHINE_EQUIP];
   char  Value_Improvements         [TSIZ_VALUE_IMPROVEMENTS];
   char  Value_Business_Persprop    [TSIZ_VALUE_BUSINESS_PERSPROP];
   char  Value_Househld_Persprop    [TSIZ_VALUE_HOUSEHLD_PERSPROP];
   char  Value_Homeowner_Exemption  [TSIZ_VALUE_HOMEOWNER_EXEMPTION];
   char  Value_Other_Exemption      [TSIZ_VALUE_OTHER_EXEMPTION];
   char  Value_Net_Total            [TSIZ_VALUE_NET_TOTAL];
   char  Value_Penalty              [TSIZ_VALUE_PENALTY];
   char  Penalty_Percent            [TSIZ_PENALTY_PERCENT];
   char  Househld_Penalty_Percent   [TSIZ_HOUSEHLD_PENALTY_PERCENT];
   char  Exemption_Code             [TSIZ_EXEMPTION_CODE];
   char  Exemption_Percent          [TSIZ_EXEMPTION_PERCENT];
   char  Payfwd_Tracer_Suff         [TSIZ_PAYFWD_TRACER_SUFF];
   char  Original_Bank              [TSIZ_ORIGINAL_BANK];
   char  Bank_Change_Date           [TSIZ_BANK_CHANGE_DATE];
   char  Current_Bank_Alpha         [TSIZ_CURRENT_BANK_ALPHA];
   char  Delinq_Sale_Number         [TSIZ_DELINQ_SALE_NUMBER];
   char  Senior_Citizen_Flag        [TSIZ_SENIOR_CITIZEN_FLAG];
   char  Fiche_Comment_Flag         [TSIZ_FICHE_COMMENT_FLAG];
   char  Ill_Or_Pi_Key              [TSIZ_ILL_OR_PI_KEY];
   char  Current_Owners_Name        [TSIZ_CURRENT_OWNERS_NAME];
   char  Liendate_Owners_Name       [TSIZ_LIENDATE_OWNERS_NAME];
   char  Mail_Care_Of_Name          [TSIZ_MAIL_CARE_OF_NAME];
   char  Mail_Street_Address        [TSIZ_MAIL_STREET_ADDRESS];
   char  Mail_City_State            [TSIZ_MAIL_CITY_STATE];
   char  Mail_Zip                   [TSIZ_MAIL_ZIP];
   char  Mail_Zip4                  [TSIZ_MAIL_ZIP4];
   char  Taxrate_Av                 [TSIZ_TAXRATE_AV];
   char  Taxrate_Flood              [TSIZ_TAXRATE_FLOOD];
   char  Checkdig_Taxamt            [TSIZ_CHECKDIG_TAXAMT];
   char  Checkdig_Tracer_1St        [TSIZ_CHECKDIG_TRACER_1ST];
   char  Checkdig_Tracer_2Nd        [TSIZ_CHECKDIG_TRACER_2ND];
   char  Run_Number_Last_Changed    [TSIZ_RUN_NUMBER_LAST_CHANGED];
   char  Last_Dupe_Reverse_Seq_Key  [TSIZ_LAST_DUPE_REVERSE_SEQ_KEY];
   char  Bank_Comment               [TSIZ_BANK_COMMENT];
   char  Instlmt1_Pmt_Date          [TSIZ_1ST_INSTLMT_PMT_DATE];
   char  Instlmt1_Pmt_Batch         [TSIZ_1ST_INSTLMT_PMT_BATCH];
   char  Instlmt1_Pmt_Teller        [TSIZ_1ST_INSTLMT_PMT_TELLER];
   char  Instlmt1_Pmt_Lot           [TSIZ_1ST_INSTLMT_PMT_LOT];
   char  Instlmt1_Pmt_Seq           [TSIZ_1ST_INSTLMT_PMT_SEQ];
   char  Instlmt1_Pmt_Cash_Chk      [TSIZ_1ST_INSTLMT_PMT_CASH_CHK];
   char  Instlmt2_Pmt_Date          [TSIZ_2ND_INSTLMT_PMT_DATE];
   char  Instlmt2_Pmt_Batch         [TSIZ_2ND_INSTLMT_PMT_BATCH];
   char  Instlmt2_Pmt_Teller        [TSIZ_2ND_INSTLMT_PMT_TELLER];
   char  Instlmt2_Pmt_Lot           [TSIZ_2ND_INSTLMT_PMT_LOT];
   char  Instlmt2_Pmt_Seq           [TSIZ_2ND_INSTLMT_PMT_SEQ];
   char  Instlmt2_Pmt_Cash_Chk      [TSIZ_2ND_INSTLMT_PMT_CASH_CHK];
   char  Reminder_Bill_Flag         [TSIZ_REMINDER_BILL_FLAG];
   char  Filler2                    [TSIZ_FILLER2];
   char  Supplemental_Assess_Flag   [TSIZ_SUPPLEMENTAL_ASSESS_FLAG];
   char  Adjustment_Number          [TSIZ_ADJUSTMENT_NUMBER];
   char  Adj_Date_Posted            [TSIZ_ADJ_DATE_POSTED];
   char  Adj_Resolution_Number      [TSIZ_ADJ_RESOLUTION_NUMBER];
   char  Adj_Action_Code            [TSIZ_ADJ_ACTION_CODE];
   char  Adj_Reason_Code            [TSIZ_ADJ_REASON_CODE];
   char  Adj_Penalty_Waiver_Flag    [TSIZ_ADJ_PENALTY_WAIVER_FLAG];
   char  Adj_Bill_Print_Flag        [TSIZ_ADJ_BILL_PRINT_FLAG];
   char  Req_Curr_Prior_Flag        [TSIZ_REQ_CURR_PRIOR_FLAG];
   char  Adj_Data1_Year             [TSIZ_ADJ_DATA1_YEAR];
   char  Adj_Data1_Citation         [TSIZ_ADJ_DATA1_CITATION];
   char  Cancel_Date_Posted         [TSIZ_CANCEL_DATE_POSTED];
   char  Cancel_Resolution_Number   [TSIZ_CANCEL_RESOLUTION_NUMBER];
   char  Cancel_Number              [TSIZ_CANCEL_NUMBER];
   char  Cancel_Action_Code         [TSIZ_CANCEL_ACTION_CODE];
   char  Cancel_Reason_Code         [TSIZ_CANCEL_REASON_CODE];
   char  Tax_Grand_Total_1St        [TSIZ_TAX_GRAND_TOTAL_1ST];         // Due amt1
   char  Tax_Grand_Total_2Nd        [TSIZ_TAX_GRAND_TOTAL_2ND];         // Due amt2
   char  Tax_Interest_1St           [TSIZ_TAX_INTEREST_1ST];
   char  Tax_Interest_2Nd           [TSIZ_TAX_INTEREST_2ND];
   char  Tax_Delinq_Penalty_1St     [TSIZ_TAX_DELINQ_PENALTY_1ST];
   char  Tax_Delinq_Penalty_2Nd     [TSIZ_TAX_DELINQ_PENALTY_2ND];
   char  Tax_Delinquent_Cost        [TSIZ_TAX_DELINQUENT_COST];
   char  Tax_Total_1St              [TSIZ_TAX_TOTAL_1ST];               // Total tax amt1
   char  Tax_Total_2Nd              [TSIZ_TAX_TOTAL_2ND];               // Total tax amt2
   char  Tax_Av_1St                 [TSIZ_TAX_AV_1ST];                  // Net assessed amt1
   char  Tax_Av_2Nd                 [TSIZ_TAX_AV_2ND];
   char  Tax_Flood_1St              [TSIZ_TAX_FLOOD_1ST];
   char  Tax_Flood_2Nd              [TSIZ_TAX_FLOOD_2ND];
   char  Use_Code                   [TSIZ_USE_CODE];
   char  Mail_Unit_No               [TSIZ_MAIL_UNIT_NO];
   char  Ma_Bar_Code_Wseq           [TSIZ_MA_BAR_CODE_WSEQ];
   char  Ma_Bar_Code_Ckdigit        [TSIZ_MA_BAR_CODE_CKDIGIT];
   char  Pymt1_Station              [TSIZ_1ST_PYMT_STATION];
   char  Pymt1_Consecutive_No       [TSIZ_1ST_PYMT_CONSECUTIVE_NO];
   char  Pymt2_Station              [TSIZ_2ND_PYMT_STATION];
   char  Pymt2_Consecutive_No       [TSIZ_2ND_PYMT_CONSECUTIVE_NO];
   char  Install1_Delq_Due_Date     [TSIZ_INSTALL1_DELQ_DUE_DATE];
   char  Install2_Delq_Due_Date     [TSIZ_INSTALL2_DELQ_DUE_DATE];
   char  Filler3                    [TSIZ_FILLER3];
   char  Web_Ivr_Flag               [TSIZ_WEB_IVR_FLAG];
   char  Waiver_Print_Flag          [TSIZ_WAIVER_PRINT_FLAG];
   char  Install1_Due_Date          [TSIZ_INSTALL1_DUE_DATE];
   char  Install2_Due_Date          [TSIZ_INSTALL2_DUE_DATE];
   ALA_SPCTAX asSpcTax              [SEC_MAX_SPCTAX];
   char  CrLf[2];
} ALCO_SEC;

#define  TSIZ_TCM_ROLL_ID                   1
#define  TSIZ_TCM_ACCT_DFLT_NUM             6
#define  TSIZ_TCM_ACCT_YR                   2
#define  TSIZ_TCM_ACCT_SEP_NUM              2
#define  TSIZ_TCM_ACCT_SEG_NUM              3
#define  TSIZ_TCM_ACCT_SOURCE_NUM           2
#define  TSIZ_TCM_ACCT_TYPE                 2
#define  TSIZ_TCM_ACCT_CORR_NUM             2
#define  TSIZ_TCM_LOGON_ID                  5
#define  TSIZ_TCM_DATE_FULL                 8      // YYYYMMDD
#define  TSIZ_TCM_DATE                      6      // YYMMDD
#define  TSIZ_TCM_UPDATE_TYPE               1
#define  TSIZ_TCM_SITUS_HOUSE               6
#define  TSIZ_TCM_SITUS_STREET              22
#define  TSIZ_TCM_SITUS_CITY                3
#define  TSIZ_TCM_SITUS_ROOM                5
#define  TSIZ_TCM_PARCEL                    13
#define  TSIZ_TCM_TRA                       5
#define  TSIZ_TCM_AV_RATE                   9
#define  TSIZ_TCM_FLOOD_ZONE                3
#define  TSIZ_TCM_FL_RATE                   9
#define  TSIZ_TCM_INST1_PAID                1
#define  TSIZ_TCM_NONCALCULABLE_BILL        1
#define  TSIZ_TCM_AMOUNT                    10
#define  TSIZ_TCM_HOEX                      6
#define  TSIZ_TCM_EXEMPT_CODE               2
#define  TSIZ_TCM_PENALTY_PERCENT           4
#define  TSIZ_TCM_PRIOR_HOEX                6
#define  TSIZ_TCM_AV_TAX                    13
#define  TSIZ_TCM_FL_TAX                    13
#define  TSIZ_TCM_SP_TAX                    13
#define  TSIZ_TCM_SP_LEGEND                 6
#define  TSIZ_TCM_ESC_INT                   13
#define  TSIZ_TCM_DEL_PEN                   13
#define  TSIZ_TCM_DEL_CST                   13
#define  TSIZ_TCM_WAIVER_DEL_PEN            13
#define  TSIZ_TCM_WAIVER_DEL_CST            13
#define  TSIZ_TCM_WAIVER_RDM_PEN            13
#define  TSIZ_TCM_WAIVER_PLN_INT            13
#define  TSIZ_TCM_WAIVER_INPUT_DATE_FULL    8
#define  TSIZ_TCM_WAIVER_INPUT_BY           5
#define  TSIZ_TCM_WAIVER_REASON_CODE        3
#define  TSIZ_TCM_WAIVER_ADJUST_NUM         10
#define  TSIZ_TCM_ASSESSEE_NAME             40
#define  TSIZ_TCM_DBA_NAME                  40
#define  TSIZ_TCM_CARE_OF_NAME              40
#define  TSIZ_TCM_FILLER                    15
#define  TSIZ_TCM_MAIL_STREET               28
#define  TSIZ_TCM_MAIL_ROOM                 10
#define  TSIZ_TCM_MAIL_CITY                 25
#define  TSIZ_TCM_MAIL_ZIP                  9
#define  TSIZ_TCM_PHONE_NUM                 10
#define  TSIZ_TCM_ENROLL_DATE_FULL          8
#define  TSIZ_TCM_TRACER                    8
#define  TSIZ_TCM_BILL_DATE_FULL            6
#define  TSIZ_TCM_ORIG_BILL_DATE_FULL       6
#define  TSIZ_TCM_SUPP_DOCUMENT_YR          2
#define  TSIZ_TCM_SUPP_DEED_PERMIT_NUM      6
#define  TSIZ_TCM_SUPP_ROLL_YR              2
#define  TSIZ_TCM_SUPP_TRANSFER_DATE        6
#define  TSIZ_TCM_SUPP_BEG_ALLOC_DATE       6
#define  TSIZ_TCM_SUPP_END_ALLOC_DATE       6
#define  TSIZ_TCM_SUPP_NOTICE_DATE          6
#define  TSIZ_TCM_SUPP_PRORATE_FACTOR       7
#define  TSIZ_TCM_SUPP_TYPE                 1
#define  TSIZ_TCM_ESC_AUTH_CODE             7
#define  TSIZ_TCM_ESC_DATE_FULL             8
#define  TSIZ_TCM_ESC_RATE_CODE             3
#define  TSIZ_TCM_ESC_YR_FULL               4
#define  TSIZ_TCM_AUDITOR                   3
#define  TSIZ_TCM_INSTALL_ELIGIBLE          1
#define  TSIZ_TCM_CORR_AUTH_CODE            7
#define  TSIZ_TCM_CORR_SURR_DATE_FULL       8
#define  TSIZ_TCM_CORR_TYPE                 1
#define  TSIZ_TCM_CANCEL_DATE_FULL          8
#define  TSIZ_TCM_CANCEL_BY_SEQ_NUM         2
#define  TSIZ_TCM_SOLD_YEAR                 4
#define  TSIZ_TCM_DISCOUNT_DEL_PEN          13
#define  TSIZ_TCM_DISCOUNT_RDM_PEN          13
#define  TSIZ_TCM_AAB_FILE_DATE_FULL        8
#define  TSIZ_TCM_AAB_CASE_NUM              10
#define  TSIZ_TCM_AAB_DECIDE_DATE_FULL      8
#define  TSIZ_TCM_ACTUAL_BILL_DATE_FULL     6
#define  TSIZ_TCM_WAIVER_RESCIND_DATE_FULL  8
#define  TSIZ_TCM_SEP_CANC_CREATE_DATE      8
#define  TSIZ_TCM_SEG_CANC_CREATE_DATE      8
#define  TSIZ_TCM_SEP_FLAG                  1
#define  TSIZ_TCM_SEG_FLAG                  1
#define  TSIZ_TCM_A5_DISCNT_DEL_PEN         13
#define  TSIZ_TCM_A5_DISCNT_RDM_PEN         13
#define  TSIZ_TCM_A5_DISCNT_PLN_INT         13
#define  TCM_MAX_ITEMS                      50

// ALCO-TDTCM.txt
typedef struct _tTDTCM
{
   char  Roll_Id                  [TSIZ_TCM_ROLL_ID                  ];    // 1 - (D)efaulted, (N)on defaulted or Escape
   char  Acct_Dflt_Num            [TSIZ_TCM_ACCT_DFLT_NUM            ];    // 2
   char  Acct_Yr                  [TSIZ_TCM_ACCT_YR                  ];    // 8
   char  Acct_Sep_Num             [TSIZ_TCM_ACCT_SEP_NUM             ];    // 10
   char  Acct_Seg_Num             [TSIZ_TCM_ACCT_SEG_NUM             ];    // 12
   char  Acct_Source_Num          [TSIZ_TCM_ACCT_SOURCE_NUM          ];    // 15 - 01=Secured, 02=Supplemental
   char  Acct_Type                [TSIZ_TCM_ACCT_TYPE                ];    // 17
   char  Acct_Corr_Num            [TSIZ_TCM_ACCT_CORR_NUM            ];    // 19
   char  Logon_Id                 [TSIZ_TCM_LOGON_ID                 ];
   char  Update_Date              [TSIZ_TCM_DATE_FULL                ];    // 26 - YYYYMMDD
   char  Update_Type              [TSIZ_TCM_UPDATE_TYPE              ];    // 34
   char  Situs_House              [TSIZ_TCM_SITUS_HOUSE              ];    // 35
   char  Situs_Street             [TSIZ_TCM_SITUS_STREET             ];    // 41
   char  Situs_City               [TSIZ_TCM_SITUS_CITY               ];    // 63
   char  Situs_Room               [TSIZ_TCM_SITUS_ROOM               ];    // 66
   char  Apn                      [TSIZ_TCM_PARCEL                   ];    // 71
   char  Apn_New                  [TSIZ_TCM_PARCEL                   ];    // 84
   char  TRA                      [TSIZ_TCM_TRA                      ];    // 97
   char  Av_Rate                  [TSIZ_TCM_AV_RATE                  ];    // 102 - +9(3).9(4)
   char  Flood_Zone               [TSIZ_TCM_FLOOD_ZONE               ];    // 111
   char  Fl_Rate                  [TSIZ_TCM_FL_RATE                  ];    // 114
   char  Inst1_Paid               [TSIZ_TCM_INST1_PAID               ];    // 123
   char  Noncalculable_Bill       [TSIZ_TCM_NONCALCULABLE_BILL       ];    // 124 - Y=Noncalculable, N=Calculable
   char  Land                     [TSIZ_TCM_AMOUNT                   ];    // 125 - +9(9)
   char  Struct                   [TSIZ_TCM_AMOUNT                   ];    // 135
   char  Fixt                     [TSIZ_TCM_AMOUNT                   ];    // 145
   char  Bus_Pp                   [TSIZ_TCM_AMOUNT                   ];
   char  House_Pp                 [TSIZ_TCM_AMOUNT                   ];
   char  Hoex                     [TSIZ_TCM_AMOUNT                   ];
   char  Regex                    [TSIZ_TCM_AMOUNT                   ];
   char  Exempt_Code              [TSIZ_TCM_EXEMPT_CODE              ];    // 195
   char  Assessd_Pen              [TSIZ_TCM_AMOUNT                   ];    // 197
   char  Penalty_Percent          [TSIZ_TCM_PENALTY_PERCENT          ];    // 207 - +9(3)
   char  House_Penalty_Percent    [TSIZ_TCM_PENALTY_PERCENT          ];    // 211
   char  Supp_Last_Roll_Land      [TSIZ_TCM_AMOUNT                   ];    // 215
   char  Supp_Last_Roll_Imps      [TSIZ_TCM_AMOUNT                   ];
   char  Supp_Prior_Land          [TSIZ_TCM_AMOUNT                   ];
   char  Supp_Prior_Imps          [TSIZ_TCM_AMOUNT                   ];
   char  Supp_New_Base_Land       [TSIZ_TCM_AMOUNT                   ];
   char  Supp_New_Base_Imps       [TSIZ_TCM_AMOUNT                   ];
   char  Prior_Hoex               [TSIZ_TCM_PRIOR_HOEX               ];    // 275 - +9(5)
   char  Prior_Regex              [TSIZ_TCM_AMOUNT                   ];    // 281
   char  AV_Tax                   [TSIZ_TCM_AV_TAX                   ];    // 291 - +9(09).9(02)
   char  FL_Tax                   [TSIZ_TCM_FL_TAX                   ];    // 304 - +9(09).9(02)
   char  SP_Tax                   [TCM_MAX_ITEMS][TSIZ_TCM_SP_TAX    ];    // 317
   char  SP_Legend                [TCM_MAX_ITEMS][TSIZ_TCM_SP_LEGEND ];    // 967
   char  Esc_Int                  [TSIZ_TCM_ESC_INT                  ];    // 1267 - +9(9).9(2)
   char  Del_Pen                  [TSIZ_TCM_DEL_PEN                  ];    // 1280 
   char  Del_Cst                  [TSIZ_TCM_DEL_CST                  ];    // 1293
   char  Waiver_Del_Pen           [TSIZ_TCM_WAIVER_DEL_PEN           ];    // 1306
   char  Waiver_Del_Cst           [TSIZ_TCM_WAIVER_DEL_CST           ];
   char  Waiver_Rdm_Pen           [TSIZ_TCM_WAIVER_RDM_PEN           ];
   char  Waiver_Pln_Int           [TSIZ_TCM_WAIVER_PLN_INT           ];
   char  Waiver_Input_Date        [TSIZ_TCM_DATE_FULL                ];    // 1358 - YYYYMMDD
   char  Waiver_Input_By          [TSIZ_TCM_WAIVER_INPUT_BY          ];    // 1366
   char  Waiver_Reason_Code       [TSIZ_TCM_WAIVER_REASON_CODE       ];
   char  Waiver_Adjust_Num        [TSIZ_TCM_WAIVER_ADJUST_NUM        ];
   char  Assessee_Name            [TSIZ_TCM_ASSESSEE_NAME            ];
   char  Dba_Name                 [TSIZ_TCM_DBA_NAME                 ];
   char  Care_Of_Name             [TSIZ_TCM_CARE_OF_NAME             ];
   char  Filler                   [TSIZ_TCM_FILLER                   ];
   char  Mail_Street              [TSIZ_TCM_MAIL_STREET              ];
   char  Mail_Room                [TSIZ_TCM_MAIL_ROOM                ];
   char  Mail_City                [TSIZ_TCM_MAIL_CITY                ];
   char  Mail_Zip                 [TSIZ_TCM_MAIL_ZIP                 ];
   char  Phone_Num                [TSIZ_TCM_PHONE_NUM                ];    // 1591
   char  Enroll_Date              [TSIZ_TCM_DATE_FULL                ];    // 1601 - YYYYMMDD
   char  Tracer                   [TSIZ_TCM_TRACER                   ];    // 1609
   char  Bill_Date                [TSIZ_TCM_DATE                     ];    // 1617 - YYYYMM
   char  Orig_Bill_Date           [TSIZ_TCM_DATE                     ];    // 1623 - YYYYMM
   char  Supp_Document_Yr         [TSIZ_TCM_SUPP_DOCUMENT_YR         ];    // 1629 - YY
   char  Supp_Deed_Permit_Num     [TSIZ_TCM_SUPP_DEED_PERMIT_NUM     ];    // 1631
   char  Supp_Roll_Yr             [TSIZ_TCM_SUPP_ROLL_YR             ];    // 1637 - YY
   char  Supp_Transfer_Date       [TSIZ_TCM_DATE                     ];    // 1639 - YYMMDD
   char  Supp_Beg_Alloc_Date      [TSIZ_TCM_DATE                     ];    // 1645 - YYMMDD
   char  Supp_End_Alloc_Date      [TSIZ_TCM_DATE                     ];    // 1651 - YYMMDD
   char  Supp_Notice_Date         [TSIZ_TCM_DATE                     ];    // 1657 - YYMMDD
   char  Supp_Prorate_Factor      [TSIZ_TCM_SUPP_PRORATE_FACTOR      ];    // 1663 - +9(01).9(04)
   char  Supp_Type                [TSIZ_TCM_SUPP_TYPE                ];
   char  Esc_Auth_Code            [TSIZ_TCM_ESC_AUTH_CODE            ];
   char  Esc_Date                 [TSIZ_TCM_DATE_FULL                ];    // 1678 - YYYYMMDD
   char  Esc_Rate_Code            [TSIZ_TCM_ESC_RATE_CODE            ];
   char  Esc_Yr_Full              [TSIZ_TCM_ESC_YR_FULL              ];    // 1689 - YYYY
   char  Auditor                  [TSIZ_TCM_AUDITOR                  ];
   char  Install_Eligible         [TSIZ_TCM_INSTALL_ELIGIBLE         ];
   char  Corr_Auth_Code           [TSIZ_TCM_CORR_AUTH_CODE           ];
   char  Corr_Surr_Date           [TSIZ_TCM_DATE_FULL                ];    // 1704 - YYYYMMDD
   char  Corr_Type                [TSIZ_TCM_CORR_TYPE                ];
   char  Cancel_Date              [TSIZ_TCM_DATE_FULL                ];    // 1713 - YYYYMMDD
   char  Cancel_By_Seq_Num        [TSIZ_TCM_CANCEL_BY_SEQ_NUM        ];
   char  Sold_Year                [TSIZ_TCM_SOLD_YEAR                ];    // 1723 - YYYY
   char  Discount_Del_Pen         [TSIZ_TCM_DISCOUNT_DEL_PEN         ];    // 1727
   char  Discount_Rdm_Pen         [TSIZ_TCM_DISCOUNT_RDM_PEN         ];    // 1740
   char  AAB_File_Date            [TSIZ_TCM_DATE_FULL                ];    // 1753 - YYYYMMDD
   char  AAB_Case_Num             [TSIZ_TCM_AAB_CASE_NUM             ];
   char  AAB_Decide_Date          [TSIZ_TCM_DATE_FULL                ];    // 1771 - YYYYMMDD
   char  Actual_Bill_Date         [TSIZ_TCM_DATE                     ];    // 1779 - YYYYMM
   char  Waiver_Rescind_Date      [TSIZ_TCM_DATE_FULL                ];    // 1785 - YYYYMMDD
   char  Sep_Canc_Create_Date     [TSIZ_TCM_DATE_FULL                ];    // 1793 - YYYYMMDD
   char  Seg_Canc_Create_Date     [TSIZ_TCM_DATE_FULL                ];    // 1801 - YYYYMMDD
   char  Sep_Flag                 [TSIZ_TCM_SEP_FLAG                 ];    // 1809 - (S)eparated special, Separated(R)eminder
   char  Seg_Flag                 [TSIZ_TCM_SEG_FLAG                 ];
   char  A5_Discnt_Del_Pen        [TSIZ_TCM_A5_DISCNT_DEL_PEN        ];    // 1811
   char  A5_Discnt_Rdm_Pen        [TSIZ_TCM_A5_DISCNT_RDM_PEN        ];
   char  A5_Discnt_Pln_Int        [TSIZ_TCM_A5_DISCNT_PLN_INT        ];
} TDTCM;

#define  TSIZ_PAY_PARCEL                     13
#define  TSIZ_PAY_ROLL_ID                    1
#define  TSIZ_PAY_ACCT_ID                    19
#define  TSIZ_PAY_FULL_DATE                  8
#define  TSIZ_PAY_BATCH_NUM                  3
#define  TSIZ_PAY_ITEM_NUM                   3
#define  TSIZ_PAY_ITEM_SUB_NUM               2
#define  TSIZ_PAY_CREDIT_AMT                 13
#define  TSIZ_PAY_PYMT_AMT                   13
#define  TSIZ_PAY_PYMT_TYPE                  2
#define  TSIZ_PAY_PYMT_CODE                  2
#define  TSIZ_PAY_NEW_BATCH_NUM              3
#define  TSIZ_PAY_NEW_ITEM_NUM               3
#define  TSIZ_PAY_OLD_BATCH_NUM              3
#define  TSIZ_PAY_OLD_ITEM_NUM               3
#define  TSIZ_PAY_OLD_PYMT_CODE              2
#define  TSIZ_PAY_RECEIPT_NUM                6
#define  TSIZ_PAY_STUB_CONSEC_NUM            5
#define  TSIZ_PAY_PYMT_CONSEC_NUM            5
#define  TSIZ_PAY_TELLER_NUM                 4
#define  TSIZ_PAY_STATION_NUM                2
#define  TSIZ_PAY_BATCH_TYPE                 2
#define  TSIZ_PAY_PARCEL_PYMT_PLN_INT        13
#define  TSIZ_PAY_PLAN_RESTORE_CODE          2
#define  TSIZ_PAY_PLAN_RESTORE_BATCH_NUM     3
#define  TSIZ_PAY_PLAN_RESTORE_ITEM_NUM      3
#define  TSIZ_PAY_PLAN_RESTORE_ITEM_SUB_NUM  2
#define  TSIZ_FILLER                         47

#define  TSIZ_PAY_AV_TAX                     13
#define  TSIZ_PAY_FL_TAX                     13
#define  TSIZ_PAY_SP_TAX                     13
#define  TSIZ_PAY_ESC_INT                    13
#define  TSIZ_PAY_DEL_PEN                    13
#define  TSIZ_PAY_RDM_PEN                    13
#define  TSIZ_FILLER12                       12
#define  TSIZ_BUCKETS                        4

#define  TSIZ_PAY_DISCNT_DEL_PEN             13
#define  TSIZ_PAY_DISCNT_RDM_PEN             13
#define  TSIZ_PAY_DISCNT_PLN_INT             13
#define  TSIZ_PAY_PLN_FEE                    3
#define  TSIZ_FILLER5                        62
#define  TSIZ_FILLER6                        55

typedef struct _tPayApportionment
{
   char  AV_Tax      [TSIZ_PAY_AV_TAX ];     // 287
   char  FL_Tax      [TSIZ_PAY_FL_TAX ];     // 300
   char  SP_Tax      [TSIZ_PAY_SP_TAX ];     // 313
   char  Esc_Int     [TSIZ_PAY_ESC_INT];     // 326
   char  Del_Pen     [TSIZ_PAY_DEL_PEN];     // 339
   char  Rdm_Pen     [TSIZ_PAY_RDM_PEN];     // 352
   char  Filler      [TSIZ_FILLER12   ];     // 365
} PAY_APTN;

// ALCO-TDPF0.txt
typedef struct _tDefPayment
{
   char  Apn                       [TSIZ_PAY_PARCEL                   ];   // 001
   char  Roll_Id                   [TSIZ_PAY_ROLL_ID                  ];   // 014
   char  Acct_Id                   [TSIZ_PAY_ACCT_ID                  ];   // 015
   char  Post_Date                 [TSIZ_PAY_FULL_DATE                ];   // 034
   char  Batch_Num                 [TSIZ_PAY_BATCH_NUM                ];   // 042
   char  Item_Num                  [TSIZ_PAY_ITEM_NUM                 ];   // 045
   char  Item_Sub_Num              [TSIZ_PAY_ITEM_SUB_NUM             ];   // 048
   char  Credit_Amt                [TSIZ_PAY_CREDIT_AMT               ];   // 050
   char  Pymt_Amt                  [TSIZ_PAY_PYMT_AMT                 ];   // 063
   char  Credit_Date               [TSIZ_PAY_FULL_DATE                ];   // 076
   char  Pymt_Type                 [TSIZ_PAY_PYMT_TYPE                ];   // 084 - (F)ull, (P)artial
   char  Pymt_Code                 [TSIZ_PAY_PYMT_CODE                ];   // 086 - (A)ccept
   char  New_Post_Date             [TSIZ_PAY_FULL_DATE                ];   // 088
   char  New_Batch_Num             [TSIZ_PAY_NEW_BATCH_NUM            ];   // 096
   char  New_Item_Num              [TSIZ_PAY_NEW_ITEM_NUM             ];   // 099
   char  New_Pymt_Code             [TSIZ_PAY_PYMT_CODE                ];   // 102
   char  Old_Post_Date             [TSIZ_PAY_FULL_DATE                ];   // 104
   char  Old_Batch_Num             [TSIZ_PAY_OLD_BATCH_NUM            ];   // 112
   char  Old_Item_Num              [TSIZ_PAY_OLD_ITEM_NUM             ];   // 115
   char  Old_Pymt_Code             [TSIZ_PAY_OLD_PYMT_CODE            ];   // 118
   char  Receipt_Num               [TSIZ_PAY_RECEIPT_NUM              ];   // 120
   char  Stub_Consec_Num           [TSIZ_PAY_STUB_CONSEC_NUM          ];   // 126
   char  Pymt_Consec_Num           [TSIZ_PAY_PYMT_CONSEC_NUM          ];   // 131
   char  Teller_Num                [TSIZ_PAY_TELLER_NUM               ];   // 136
   char  Station_Num               [TSIZ_PAY_STATION_NUM              ];   // 140
   char  Batch_Type                [TSIZ_PAY_BATCH_TYPE               ];   // 142
   char  Parcel_Pymt_Pln_Int       [TSIZ_PAY_PARCEL_PYMT_PLN_INT      ];   // 144
   char  Plan_Restore_Code         [TSIZ_PAY_PLAN_RESTORE_CODE        ];   // 157
   char  Plan_Restore_Date_Full    [TSIZ_PAY_FULL_DATE                ];   // 159
   char  Plan_Restore_Batch_Num    [TSIZ_PAY_PLAN_RESTORE_BATCH_NUM   ];   // 167
   char  Plan_Restore_Item_Num     [TSIZ_PAY_PLAN_RESTORE_ITEM_NUM    ];   // 170
   char  Plan_Restore_Item_Sub_Num [TSIZ_PAY_PLAN_RESTORE_ITEM_SUB_NUM];   // 173
   char  Filler                    [TSIZ_FILLER                       ];   // 175
   char  Del_Cst                   [TSIZ_PAY_PYMT_AMT                 ];   // 222
   char  Rdm_Fee_Pre_84            [TSIZ_PAY_PYMT_AMT                 ];   // 235
   char  Rdm_Fee_All_Oth           [TSIZ_PAY_PYMT_AMT                 ];   // 248
   char  Ntc_Cst                   [TSIZ_PAY_PYMT_AMT                 ];   // 261
   char  Rcd_Fee                   [TSIZ_PAY_PYMT_AMT                 ];   // 274
   PAY_APTN  Aptn_Bucket           [TSIZ_BUCKETS                      ];   // 287
   char  Discnt_Del_Pen            [TSIZ_PAY_DISCNT_DEL_PEN           ];   // 647
   char  Discnt_Rdm_Pen            [TSIZ_PAY_DISCNT_RDM_PEN           ];   // 660
   char  Discnt_Pln_Int            [TSIZ_PAY_DISCNT_PLN_INT           ];   // 673
   char  Pln_Fee                   [TSIZ_PAY_PLN_FEE                  ];   // 686
} TDPF0;

// ALCO-TDPPL.txt
#define  TSIZ_PLN_PARCEL_NO               13
#define  TSIZ_PLN_BILL_TYPE               1
#define  TSIZ_PLN_BILL_NUM                19
#define  TSIZ_PLN_PLAN_TYPE               1
#define  TSIZ_PLN_PLAN_STATUS             1
#define  TSIZ_PLN_PLAN_DATE_FULL          8
#define  TSIZ_PLN_PLAN_SETUP_AMOUNT       12
#define  TSIZ_PLN_WARRANT_NUMBER          06
#define  TSIZ_PLN_LOGON_ID                05
#define  TSIZ_PLN_RATE_BANK_INT_X         10
#define  TSIZ_PLN_RATE_PLN_INT_X          10
#define  TSIZ_PLN_FILLER                  24

typedef struct _tPaymentPlan
{
   char  Pln_Parcel_No          [TSIZ_PLN_PARCEL_NO            ];   // 001
   char  Pln_Bill_Type          [TSIZ_PLN_BILL_TYPE            ];   // 014
   char  Pln_Bill_Num           [TSIZ_PLN_BILL_NUM             ];   // 015
   char  Pln_Plan_Type          [TSIZ_PLN_PLAN_TYPE            ];   // 034 - (E)scape, (R)egular, (P)artial, (B)ankrupt
   char  Pln_Plan_Status        [TSIZ_PLN_PLAN_STATUS          ];   // 035 - (A)ctive, (C)ancel, (D)efaulted, (P)aid
   char  Pln_Plan_Status_Date   [TSIZ_PLN_PLAN_DATE_FULL       ];   // 036
   char  Pln_Plan_Setup_Date    [TSIZ_PLN_PLAN_DATE_FULL       ];   // 044
   char  Pln_Plan_Setup_Amount  [TSIZ_PLN_PLAN_SETUP_AMOUNT    ];   // 052
   char  Pln_Warrant_Date_      [TSIZ_PLN_PLAN_DATE_FULL       ];   // 064
   char  Pln_Warrant_Number     [TSIZ_PLN_WARRANT_NUMBER       ];   // 072
   char  Pln_Logon_Id           [TSIZ_PLN_LOGON_ID             ];   // 078
   char  Pln_Update_Date        [TSIZ_PLN_PLAN_DATE_FULL       ];   // 083
   char  Pln_Notice_Date        [TSIZ_PLN_PLAN_DATE_FULL       ];   // 091
   char  Pln_Rate_Bank_Int_X    [TSIZ_PLN_RATE_BANK_INT_X      ];   // 099
   char  Pln_Rate_Pln_Int_X     [TSIZ_PLN_RATE_PLN_INT_X       ];   // 109
   char  Filler                 [TSIZ_PLN_FILLER               ];   // 119
} TDPPL;

// Standard codes
// H=Homeowners
// D=Disabled Veterans
// V=Veterans
// C=Church
// L=Library
// M=Museums
// R=Religion
// T=Tribal Housing
// W=Welfare
// U=College
// P=Public School
// E=Cemetery
// G=Government
// I=Hospital
// S=School
// X=Misc exempt
// Y=Military

IDX_TBL4 ALA_Exemption[] = 
{
   "HO", "H", 2,1,      // HOME OWNER
   "01", "V", 2,1,      // REGULAR VETERAN
   "02", "I", 2,1,      // HOSPITAL
   "03", "W", 2,1,      // WELFARE
   "04", "E", 2,1,      // CEMETERY
   "05", "C", 2,1,      // CHURCH
   "06", "P", 2,1,      // PUBLIC SCHOOL
   "07", "R", 2,1,      // RELIGIOUS
   "08", "U", 2,1,      // COLLEGE
   "09", "M", 2,1,      // MUSEUM
   "10", "L", 2,1,      // LIBRARY 
   "11", "D", 2,1,      // DISABLED VETERAN
   "19", "X", 2,1,      // HISTORICAL AIRCRAFT
   "20", "X", 2,1,      // EXHIBITION
   "21", "V", 2,1,      // VETERANS ORGANIZATION
   "99", "X", 2,1,      // OTHER
   "","",0,0
};

#endif