#ifndef  _MERGERIV_H_
#define  _MERGERIV_H_

//#define MAX_LIENREC                       160
//#define MAX_ROLLREC                       622
//#define MAX_CHARREC                       150
//#define MAX_SALEREC                       318

// 2024 - AssessmentRollCertified_2024.csv
#define  L_PIN                            0
#define  L_CONVENYANCENUMBER              1     // Store 7-digit DocNum only (1975-0000181-D)
#define  L_CONVEYANCEMONTH                2
#define  L_CONVEYANCEYEAR                 3
#define  L_CLASSDESCRIPTION               4
#define  L_BASEYEAR                       5
#define  L_BUSINESSUSECODEDESCRIPTION     6
#define  L_DOINGBUSINESSAS                7
#define  L_MAILNAME                       8
#define  L_M_ADDR1                        9
#define  L_M_CITY                         10
#define  L_M_STATE                        11
#define  L_M_ZIP                          12
#define  L_S_STRNUM                       13
#define  L_S_STRFRA                       14
#define  L_S_DIR                          15
#define  L_S_STRNAME                      16
#define  L_S_STRSFX                       17
#define  L_S_UNIT                         18
#define  L_S_ZIP                          19
#define  L_S_CITY                         20
#define  L_ASSESSMENTDESCRIPTION          21
#define  L_ISTAXABLE                      22    // Y/N
#define  L_TAXABILITYCODE                 22    // Keep this here for compatibility, no real use
#define  L_TRA                            23
#define  L_GEO                            24
#define  L_RECORDERSTYPE                  25
#define  L_RECORDERSBOOK                  26
#define  L_RECORDERSPAGE                  27
#define  L_LOTTYPE                        28
#define  L_LOT                            29
#define  L_BLOCK                          30
#define  L_PORTIONDIRECTION               31
#define  L_SUBDIVISIONNAMEORTRACT         32
#define  L_ACREAGEAMOUNT                  33
#define  L_FIRSTSECTION                   34
#define  L_FIRSTTOWNSHIP                  35
#define  L_FIRSTRANGE                     36
#define  L_FIRSTRANGEDIRECTION            37
#define  L_LEGALPARTY1                    38
#define  L_LEGALPARTY2                    39
#define  L_LEGALPARTY3                    40
#define  L_LEGALPARTY4                    41
#define  L_LAND                           42
#define  L_IMPROVEMENTS                   43
#define  L_LIVINGIMPROVEMENTS             44
#define  L_PERSONALVALUE                  45
#define  L_TRADEFIXTURESAMOUNT            46
#define  L_PERSONALPROPERTYAPPRAISED      47    // No data
#define  L_TENPERCENTASSESSEDPENALTY      48    // ?
#define  L_HOX                            49
#define  L_VETERANSEXEMPTION              50
#define  L_DISABLEDVETERANSEXEMPTION      51
#define  L_CHURCHEXEMPTION                52
#define  L_RELIGIOUSEXEMPTION             53
#define  L_CEMETERYEXEMPTION              54
#define  L_PUBLICSCHOOLEXEMPTION          55
#define  L_PUBLICLIBRARYEXEMPTION         56
#define  L_PUBLICMUSEUMEXEMPTION          57
#define  L_WELFARECOLLEGEEXEMPTION        58
#define  L_WELFAREHOSPITALEXEMPTION       59
#define  L_WPP_SCHOOLEXEMPTION            60
#define  L_WC_RELIGIOUSEXEMPTION          61
#define  L_OTHEREXEMPTION                 62
#define  L_CAMEFROM                       63
#define  L_CLASSCODE                      64
#define  L_CLASSCODEDESC                  65
#define  L_USECODE                        65
#define  L_COLS                           66

// LDR 2020 - Certified2020AssessmentRoll.csv
// This layout is different from 2019
//#define  L_PIN                            0
//#define  L_CONVENYANCENUMBER              1     // Store 7-digit DocNum only (1975-0000181-D)
//#define  L_CONVEYANCEMONTH                2
//#define  L_CONVEYANCEYEAR                 3
//#define  L_REALPROPERTYUSECODE            4
//#define  L_BASEYEAR                       5
//#define  L_BUSINESSUSECODEDESCRIPTION     6
//#define  L_DOINGBUSINESSAS                7
//#define  L_MAILNAME                       8
//#define  L_M_ADDR1                        9
//#define  L_M_CITY                         10
//#define  L_M_STATE                        11
//#define  L_M_ZIP                          12
//#define  L_S_STRNUM                       13
//#define  L_S_STRFRA                       14
//#define  L_S_DIR                          15
//#define  L_S_STRNAME                      16
//#define  L_S_STRSFX                       17
//#define  L_S_UNIT                         18
//#define  L_S_ZIP                          19
//#define  L_S_CITY                         20
//#define  L_ASSESSMENTDESCRIPTION          21
//#define  L_ISTAXABLE                      22    // Y/N
//#define  L_TAXABILITYCODE                 22
//#define  L_TRA                            23
//#define  L_GEO                            24
//#define  L_RECORDERSTYPE                  25
//#define  L_RECORDERSBOOK                  26
//#define  L_RECORDERSPAGE                  27
//#define  L_LOTTYPE                        28
//#define  L_LOT                            29
//#define  L_BLOCK                          30
//#define  L_PORTIONDIRECTION               31
//#define  L_SUBDIVISIONNAMEORTRACT         32
//#define  L_ACREAGEAMOUNT                  33
//#define  L_FIRSTSECTION                   34
//#define  L_FIRSTTOWNSHIP                  35
//#define  L_FIRSTRANGE                     36
//#define  L_FIRSTRANGEDIRECTION            37
//#define  L_LEGALPARTY1                    38
//#define  L_LEGALPARTY2                    39
//#define  L_LEGALPARTY3                    40
//#define  L_LEGALPARTY4                    41
//#define  L_LAND                           42
//#define  L_IMPROVEMENTS                   43
//#define  L_LIVINGIMPROVEMENTS             44
//#define  L_PERSONALVALUE                  45
//#define  L_TRADEFIXTURESAMOUNT            46
//#define  L_PERSONALPROPERTYAPPRAISED      47    // No data
//#define  L_TENPERCENTASSESSEDPENALTY      48    // ?
//#define  L_HOX                            49
//#define  L_VETERANSEXEMPTION              50
//#define  L_DISABLEDVETERANSEXEMPTION      51
//#define  L_CHURCHEXEMPTION                52
//#define  L_RELIGIOUSEXEMPTION             53
//#define  L_CEMETERYEXEMPTION              54
//#define  L_PUBLICSCHOOLEXEMPTION          55
//#define  L_PUBLICLIBRARYEXEMPTION         56
//#define  L_PUBLICMUSEUMEXEMPTION          57
//#define  L_WELFARECOLLEGEEXEMPTION        58
//#define  L_WELFAREHOSPITALEXEMPTION       59
//#define  L_WPP_SCHOOLEXEMPTION            60
//#define  L_WC_RELIGIOUSEXEMPTION          61
//#define  L_OTHEREXEMPTION                 62
//#define  L_CAMEFROM                       63
//#define  L_CLASSCODE                      64
//#define  L_USECODE                        65
//#define  L_COLS                           66

// LDR 2019 - Certified2019AssessmentRoll.csv
//#define  L_PIN                            0
//#define  L_CONVENYANCENUMBER              1     // Store 7-digit DocNum only (1975-0000181-D)
//#define  L_CONVEYANCEMONTH                2
//#define  L_CONVEYANCEYEAR                 3
//#define  L_REALPROPERTYUSECODE            4
//#define  L_BASEYEAR                       5
//#define  L_BUSINESSUSECODEDESCRIPTION     6
//#define  L_DOINGBUSINESSAS                7
//#define  L_MAILNAME                       8
//#define  L_M_ADDR1                        9
//#define  L_M_CITY                         10
//#define  L_M_STATE                        11
//#define  L_M_ZIP                          12
//#define  L_S_STRNUM                       13
//#define  L_S_STRFRA                       14
//#define  L_S_DIR                          15
//#define  L_S_STRNAME                      16
//#define  L_S_STRSFX                       17
//#define  L_S_UNIT                         18
//#define  L_S_ZIP                          19
//#define  L_S_CITY                         20
//#define  L_ASSESSMENTDESCRIPTION          21
//#define  L_TAXABILITYCODE                 22
//#define  L_TRA                            23
//#define  L_GEO                            24
//#define  L_RECORDERSTYPE                  25
//#define  L_RECORDERSBOOK                  26
//#define  L_RECORDERSPAGE                  27
//#define  L_COUNTYCODE                     28
//#define  L_LOTTYPE                        29
//#define  L_LOT                            30
//#define  L_BLOCK                          31
//#define  L_PORTIONDIRECTION               32
//#define  L_SUBDIVISIONNAMEORTRACT         33
//#define  L_ACREAGEAMOUNT                  34
//#define  L_FIRSTSECTION                   35
//#define  L_FIRSTTOWNSHIP                  36
//#define  L_FIRSTRANGE                     37
//#define  L_FIRSTRANGEDIRECTION            38
//#define  L_LEGALPARTY1                    39
//#define  L_LEGALPARTY2                    40
//#define  L_LEGALPARTY3                    41
//#define  L_LEGALPARTY4                    42
//#define  L_LAND                           43
//#define  L_IMPROVEMENTS                   44
//#define  L_LIVINGIMPROVEMENTS             45
//#define  L_PERSONALVALUE                  46
//#define  L_TRADEFIXTURESAMOUNT            47
//#define  L_PERSONALPROPERTYAPPRAISED      48    // No data
//#define  L_TENPERCENTASSESSEDPENALTY      49    // ?
//#define  L_HOX                            50
//#define  L_VETERANSEXEMPTION              51
//#define  L_DISABLEDVETERANSEXEMPTION      52
//#define  L_CHURCHEXEMPTION                53
//#define  L_RELIGIOUSEXEMPTION             54
//#define  L_CEMETERYEXEMPTION              55
//#define  L_PUBLICSCHOOLEXEMPTION          56
//#define  L_PUBLICLIBRARYEXEMPTION         57
//#define  L_PUBLICMUSEUMEXEMPTION          58
//#define  L_WELFARECOLLEGEEXEMPTION        59
//#define  L_WELFAREHOSPITALEXEMPTION       60
//#define  L_WPP_SCHOOLEXEMPTION            61
//#define  L_WC_RELIGIOUSEXEMPTION          62
//#define  L_OTHEREXEMPTION                 63
//#define  L_CAMEFROM                       64

// SalesDB.csv
#define  S_PIN                            0
#define  S_GEO                            1 
#define  S_CONVEYANCEMONTH                2
#define  S_CONVEYANCEYEAR                 3
#define  S_CONVEYANCENUMBER               4
#define  S_CONVEYANCEDATE                 5
#define  S_FORMSENTDATE                   6
#define  S_SALELETTERDATE                 7
#define  S_TRANSFERTYPECODE               8
#define  S_INDICATEDSALEPRICE             9
#define  S_BASEYEAR                       10
#define  S_BASEYEARFLAG                   11
#define  S_SITUSLINEONE                   12
#define  S_SITUSLINETWO                   13
#define  S_TOTALACRES                     14
#define  S_DESIGNTYPE101                  15
#define  S_CONSTRUCTIONTYPE               16
#define  S_AREA_1_1                       17
#define  S_COSTAREA                       18
#define  S_TOTALAREA                      19
#define  S_EFFECTIVEAGE                   20
#define  S_CARPORT                        21
#define  S_ATTGARAGE                      22
#define  S_DETGARAGE                      23
#define  S_POOLIND                        24
#define  S_BEDROOMS                       25
#define  S_HALFBATHROOM                   26
#define  S_THREEQTRBATHROOM               27
#define  S_FULLBATHROOM                   28
#define  S_TOTALBATHROOMS                 29
#define  S_SPECADJUSTMENT                 30
#define  S_LANDVALUE                      31
#define  S_STRUCTUREVALUE                 32
#define  S_TREEVINEVALUE                  33

// Sales Data 
#define SSIZ_CLUSTER_SORT_KEY  			   2
#define SSIZ_CLUSTER_NO			   		   5
#define SSIZ_ASSESSMENT_NO			   	   9
#define SSIZ_PRIMARY_PARCEL_NO			   9
#define SSIZ_CONVEYANCE_MONTH  		   	2
#define SSIZ_CONVEYANCE_YEAR				   4
#define SSIZ_CONVEYANCE_TYPE_CODE	   	1
#define SSIZ_CONVEYANCE_SEQUENCE_NO		   7
#define SSIZ_CONVEYANCE_DATE				   7
#define SSIZ_DEEDPROC_DATE				      7
#define SSIZ_FORM_SENT_DATE 				   7
#define SSIZ_FORM_RETRN_DATE				   7
#define SSIZ_SALE_LETTR_DATE				   7
#define SSIZ_WARN_LETTR_DATE				   7
#define SSIZ_PNLTY_LTTR_DATE				   7
#define SSIZ_CONVEYANCE_COMPLETE_DATE	   7
#define SSIZ_CONVEYANCE_POST_DATE 		   7
#define SSIZ_TRANSFER_TYPE_CODE			   3
#define SSIZ_MULTIPLE_SALE_FLAG			   1
#define SSIZ_CONVEYANCE_WENTTO_FLAG  	   1
#define SSIZ_CAMEFROM_FLAG				      1
#define SSIZ_TRACK_FURTHER_FLAG			   1
#define SSIZ_LETTER_STATUS_FLAG			   1
#define SSIZ_VALUE_CHANGE_FLAG			   1
#define SSIZ_FULL_VALUE_FLAG				   1
#define SSIZ_CONFIRMED_SALE_PRICE 		   9
#define SSIZ_INDICATED_SALE_PRICE	   	9
#define SSIZ_ADJUSTED_SALE_PRICE			   9
#define SSIZ_USE_CODE    					   2
#define SSIZ_ZONE_CODE	   				   5
#define SSIZ_BASE_YEAR		   			   4
#define SSIZ_BASE_YEAR_FLAG		   		1
#define SSIZ_SITUS_LINE_1			   	   63
#define SSIZ_SITUS_LINE_2				      23
#define SSIZ_TOTAL_ACRES 					   5
#define SSIZ_DESIGN_TYPE	   				2
#define SSIZ_CONSTRUCTION_TYPE 			   1
#define SSIZ_STRUCTURE_QUAL_CODE			   3
#define SSIZ_STRUCTURE_SHAPE_CODE 		   1
#define SSIZ_AREA						         5
#define SSIZ_COST_AREA   					   5
#define SSIZ_TOTAL_AREA	   				   6
#define SSIZ_EFFECTIVE_AGE	   			   3
#define SSIZ_CARPORT_GARAGE_INDICATOR	   1
#define SSIZ_POOL_INDICATOR		   		1
#define SSIZ_BEDROOMS					      2
#define SSIZ_BATHS				      		2
#define SSIZ_SPEC_ADJUSTMENT				   3
#define SSIZ_MISC_STR_RCLD		   		   5
#define SSIZ_TOTAL_RCLD				   	   7
#define SSIZ_MULTI_SHEET_INDICATOR		   1
#define SSIZ_USE_CODE_SORT_KEY			   1
#define SSIZ_LAND_VAL    					   9
#define SSIZ_STRUC_VALUE	   				9
#define SSIZ_TREES_AND_VINES_VALUE		   9

#define SOFF_CLUSTER_SORT_KEY     		  	1-1
#define SOFF_CLUSTER_NO			  	     	   3-1
#define SOFF_ASSESSMENT_NO     		  		8-1
#define SOFF_PRIMARY_PARCEL_NO	       	17-1
#define SOFF_CONVEYANCE_MONTH		 	      26-1
#define SOFF_CONVEYANCE_YEAR   		 		28-1
#define SOFF_CONVEYANCE_TYPE_CODE       	32-1
#define SOFF_CONVEYANCE_SEQUENCE_NO	      33-1
#define SOFF_CONVEYANCE_DATE   		 		40-1
#define SOFF_DEEDPROC_DATE		      	   47-1
#define SOFF_FORM_SENT_DATE		 	      54-1
#define SOFF_FORM_RETRN_DATE   		 		61-1
#define SOFF_SALE_LETTR_DATE	   	 		68-1
#define SOFF_WARN_LETTR_DATE		    		75-1
#define SOFF_PNLTY_LTTR_DATE		 	   	82-1
#define SOFF_CONVEYANCE_COMPLETE_DATE	   89-1
#define SOFF_CONVEYANCE_POST_DATE    	 	96-1
#define SOFF_TRANSFER_TYPE_CODE		   	103-1
#define SOFF_MULTIPLE_SALE_FLAG			   106-1
#define SOFF_CONVEYANCE_WENTTO_FLAG  		107-1
#define SOFF_CAMEFROM_FLAG			         108-1
#define SOFF_TRACK_FURTHER_FLAG   			109-1
#define SOFF_LETTER_STATUS_FLAG	   		110-1
#define SOFF_VALUE_CHANGE_FLAG		   	111-1
#define SOFF_FULL_VALUE_FLAG				   112-1
#define SOFF_CONFIRMED_SALE_PRICE    		113-1
#define SOFF_INDICATED_SALE_PRICE	      122-1
#define SOFF_ADJUSTED_SALE_PRICE     		131-1
#define SOFF_USE_CODE       					140-1
#define SOFF_ZONE_CODE		      			142-1
#define SOFF_BASE_YEAR				      	147-1
#define SOFF_BASE_YEAR_FLAG    				151-1
#define SOFF_SITUS_LINE_1		      		152-1
#define SOFF_SITUS_LINE_2				      215-1
#define SOFF_TOTAL_ACRES    					238-1
#define SOFF_DESIGN_TYPE		      		243-1
#define SOFF_CONSTRUCTION_TYPE		    	245-1
#define SOFF_STRUCTURE_QUAL_CODE		      246-1
#define SOFF_STRUCTURE_SHAPE_CODE    		249-1
#define SOFF_AREA        						250-1
#define SOFF_COST_AREA	      				255-1
#define SOFF_TOTAL_AREA			      	   260-1
#define SOFF_EFFECTIVE_AGE				      266-1
#define SOFF_CARPORT_GARAGE_INDICATOR	   269-1
#define SOFF_POOL_INDICATOR    				270-1
#define SOFF_BEDROOMS		   		     	271-1
#define SOFF_BATHS       						273-1
#define SOFF_SPEC_ADJUSTMENT   				275-1
#define SOFF_MISC_STR_RCLD		     		   278-1
#define SOFF_TOTAL_RCLD				         283-1
#define SOFF_MULTI_SHEET_INDICATOR   		290-1
#define SOFF_USE_CODE_SORT_KEY    			291-1
#define SOFF_LAND_VAL					      292-1
#define SOFF_STRUC_VALUE				     	301-1
#define SOFF_TREES_AND_VINES_VALUE	   	310-1

typedef struct _tRivSale
{
   char Cluster_Sort_Key[SSIZ_CLUSTER_SORT_KEY];
   char Cluster_No[SSIZ_CLUSTER_NO];
   char Assessment_No[SSIZ_ASSESSMENT_NO];
   char Primary_Parcel_No[SSIZ_PRIMARY_PARCEL_NO];
   char Conveyance_Month[SSIZ_CONVEYANCE_MONTH];
   char Conveyance_Year[SSIZ_CONVEYANCE_YEAR];
   char Conveyance_Type_Code[SSIZ_CONVEYANCE_TYPE_CODE];
   char Conveyance_Sequence_NO[SSIZ_CONVEYANCE_SEQUENCE_NO];
   char Conveyance_Date[SSIZ_CONVEYANCE_DATE];
   char Deedproc_Date[SSIZ_DEEDPROC_DATE];
   char Form_Sent_Date[SSIZ_FORM_SENT_DATE];
   char Form_Retrn_Date[SSIZ_FORM_RETRN_DATE];
   char Sale_Lettr_Date[SSIZ_SALE_LETTR_DATE];
   char Warn_Lettr_Date[SSIZ_WARN_LETTR_DATE];
   char Pnlty_Lttr_Date[SSIZ_PNLTY_LTTR_DATE];
   char Conveyance_Complete_Date[SSIZ_CONVEYANCE_COMPLETE_DATE] ;
   char Conveyance_Post_Date[SSIZ_CONVEYANCE_POST_DATE];
   char Transfer_Type_Code[SSIZ_TRANSFER_TYPE_CODE];
   char Multiple_Sale_Flag[SSIZ_MULTIPLE_SALE_FLAG];
   char Conveyance_Wentto_FlaG[SSIZ_CONVEYANCE_WENTTO_FLAG];
   char Camefrom_Flag[SSIZ_CAMEFROM_FLAG];
   char Track_Further_Flag[SSIZ_TRACK_FURTHER_FLAG];
   char Letter_Status_Flag[SSIZ_LETTER_STATUS_FLAG];
   char Value_Change_Flag[SSIZ_VALUE_CHANGE_FLAG];
   char Full_Value_Flag[SSIZ_FULL_VALUE_FLAG];
   char Confirmed_Sale_Price[SSIZ_CONFIRMED_SALE_PRICE];
   char Indicated_Sale_Price[SSIZ_INDICATED_SALE_PRICE];
   char Adjusted_Sale_Price[SSIZ_ADJUSTED_SALE_PRICE];
   char Use_Code[SSIZ_USE_CODE];
   char Zoning[SSIZ_ZONE_CODE];
   char Base_Year[SSIZ_BASE_YEAR];
   char Base_Year_Flag[SSIZ_BASE_YEAR_FLAG];
   char Situs_Line_1[SSIZ_SITUS_LINE_1];
   char Situs_Line_2[SSIZ_SITUS_LINE_2];
   char Total_Acres[SSIZ_TOTAL_ACRES];
   char Design_Type[SSIZ_DESIGN_TYPE];
   char Construction_Type[SSIZ_CONSTRUCTION_TYPE];
   char Structure_Qual_Code[SSIZ_STRUCTURE_QUAL_CODE];
   char Structure_Shape_Code[SSIZ_STRUCTURE_SHAPE_CODE];
   char Area[SSIZ_AREA];
   char Cost_Area[SSIZ_COST_AREA];
   char Total_Area[SSIZ_TOTAL_AREA];
   char Effective_Age[SSIZ_EFFECTIVE_AGE];
   char Carport_Garage_IndicaTor[SSIZ_CARPORT_GARAGE_INDICATOR] ;
   char Pool_Indicator[SSIZ_POOL_INDICATOR];
   char Bedrooms[SSIZ_BEDROOMS];
   char Baths[SSIZ_BATHS];
   char Spec_Adjustment[SSIZ_SPEC_ADJUSTMENT];
   char Misc_Str_Rcld[SSIZ_MISC_STR_RCLD];
   char Total_Rcld[SSIZ_TOTAL_RCLD];
   char Multi_Sheet_Indicator[SSIZ_MULTI_SHEET_INDICATOR];
   char Use_Code_Sort_Key[SSIZ_USE_CODE_SORT_KEY];
   char Land_Val[SSIZ_LAND_VAL];
   char Struc_Value[SSIZ_STRUC_VALUE];
   char Trees_And_Vines_Value[SSIZ_TREES_AND_VINES_VALUE];
} RIV_SALE;


#define RSIZ_ASSESSOR_STUFF	         	5
#define RSIZ_ASSMNT_NO         		   	9
#define RSIZ_PARCEL_NO         		   	9
#define RSIZ_ACRES	            			5
#define RSIZ_RE_USE_CODE       		   	2
#define RSIZ_FILLER1				            49
#define RSIZ_BASE_YEAR         			   4
#define RSIZ_FILLER2				            6
#define RSIZ_BUS_USE_CODE         	   	4
#define RSIZ_FILLER3				            2
#define RSIZ_TAX_CODE			            3
#define RSIZ_TRA		                     6
#define RSIZ_CODE1				            2
#define RSIZ_AMOUNT1				            9
#define RSIZ_CODE2				            2
#define RSIZ_AMOUNT2				            9
#define RSIZ_CODE3				            2
#define RSIZ_AMOUNT3				            9
#define RSIZ_CODE4				            2
#define RSIZ_AMOUNT4	         			   9
#define RSIZ_CODE5				            2
#define RSIZ_AMOUNT5				            9
#define RSIZ_CODE6				            2
#define RSIZ_AMOUNT6				            9
#define RSIZ_CODE7				            2
#define RSIZ_AMOUNT7				            9
#define RSIZ_CODE8				            2
#define RSIZ_AMOUNT8				            9
#define RSIZ_CODE9				            2
#define RSIZ_AMOUNT9				            9
#define RSIZ_CODE10				            2
#define RSIZ_AMOUNT10			            9
#define RSIZ_CODE11				            2
#define RSIZ_AMOUNT11			            9
#define RSIZ_CODE12				            2
#define RSIZ_AMOUNT11			            9
#define RSIZ_CODE13				            2
#define RSIZ_AMOUNT13			            9
#define RSIZ_FILLER4				            21
#define RSIZ_MAIL_TO_NAME		            43
#define RSIZ_MADDRESS			            26
#define RSIZ_MCITY_STATE			         26
#define RSIZ_MZIP4			               4
#define RSIZ_MZIP				               5
#define RSIZ_SSTR_NAME			            18
#define RSIZ_SSTR_SUFFIX	               3
#define RSIZ_SSTR_DIR				         1
#define RSIZ_SSTR_NUMBER			         6
#define RSIZ_SSTR_SUB			            1
#define RSIZ_SSTR_SIDE			            1
#define RSIZ_SUNIT				            4
#define RSIZ_SZIZ4			               4
#define RSIZ_SZIP				               5
#define RSIZ_OWNER			               43
#define RSIZ_OWN1_LAST			            19
#define RSIZ_OWN1_FIRST			            12
#define RSIZ_OWN1_MIDDLE			         12
#define RSIZ_OWN1_FLAG			            1
#define RSIZ_OWN2_LAST			            19
#define RSIZ_OWN2_FIRST			            12
#define RSIZ_OWN2_MIDDLE			         12
#define RSIZ_OWN2_FLAG			            1
#define RSIZ_OWN3_LAST			            19
#define RSIZ_OWN3_FIRST			            12
#define RSIZ_OWN3_MIDDLE			         12
#define RSIZ_OWN3_FLAG			            1
#define RSIZ_OWN4_LAST			            19
#define RSIZ_OWN4_FIRST			            12
#define RSIZ_OWN4_MIDDLE			         12
#define RSIZ_OWN4_FLAG			            1
#define RSIZ_PRI_PROP_USE_CODE	         1
#define RSIZ_SEC_PROP_USE_CODE	         2
#define RSIZ_NO_UNITS			            3
#define RSIZ_MULT_CODE			            1
#define RSIZ_FILLER5				            24

#define ROFF_ASSESSOR_STUFF		         1-1
#define ROFF_ASSMNT_NO			            6-1
#define ROFF_PARCEL_NO         			   15-1
#define ROFF_ACRES				            24-1
#define ROFF_RE_USE_CODE       			   29-1
#define ROFF_FILLER1				            31-1
#define ROFF_BASE_YEAR         			   80-1
#define ROFF_FILLER2				            84-1
#define ROFF_BUS_USE_CODE         		   90-1
#define ROFF_FILLER3				            94-1
#define ROFF_TAX_CODE		            	96-1
#define ROFF_TAX_RATE_AREA		            99-1
#define ROFF_CODE1          			   	105-1
#define ROFF_AMOUNT1			           	   107-1
#define ROFF_CODE2          			   	116-1
#define ROFF_AMOUNT2			         	   118-1
#define ROFF_CODE3          			   	127-1
#define ROFF_AMOUNT3			         	   129-1
#define ROFF_CODE4          			   	138-1
#define ROFF_AMOUNT4			         	   140-1
#define ROFF_CODE5          			   	149-1
#define ROFF_AMOUNT5			         	   151-1
#define ROFF_CODE6          		   		160-1
#define ROFF_AMOUNT6			         	   162-1
#define ROFF_CODE7          		   		171-1
#define ROFF_AMOUNT7			         	   173-1
#define ROFF_CODE8          			   	182-1
#define ROFF_AMOUNT8			         	   184-1
#define ROFF_CODE9          			   	193-1
#define ROFF_AMOUNT9			         	   195-1
#define ROFF_CODE10         			   	204-1
#define ROFF_AMOUNT10			            206-1
#define ROFF_CODE11         			   	215-1
#define ROFF_AMOUNT11			            217-1
#define ROFF_CODE12         			   	226-1
#define ROFF_AMOUNT12			            228-1
#define ROFF_CODE13         				   237-1
#define ROFF_AMOUNT13			            239-1
#define ROFF_FILLER4        			   	248-1
#define ROFF_MAIL_TO_NAME	            	269-1
#define ROFF_MADDRESS          		   	312-1
#define ROFF_MCITY_STATE			         338-1
#define ROFF_MZIP4           			   	364-1
#define ROFF_MZIP           			   	368-1
#define ROFF_SST_NAME			            373-1
#define ROFF_SST_TYPE          		   	391-1
#define ROFF_SST_DIR				            394-1
#define ROFF_SST NUMBER        		   	395-1
#define ROFF_SSTR_SUB		               401-1
#define ROFF_SSTR_SIDE               		402-1
#define ROFF_SUNIT          	   			403-1
#define ROFF_SZIP4		         	      407-1
#define ROFF_SZIP           		   		411-1
#define ROFF_OWN1_LAST		            	416-1
#define ROFF_OWN1_FIRST        		   	435-1
#define ROFF_OWN1_MIDDLE			         447-1
#define ROFF_OWN1_FLAG         			   459-1
#define ROFF_OWN2_LAST		   	         460-1
#define ROFF_OWN2_FIRST           			479-1
#define ROFF_OWN2_MIDDLE			         491-1
#define ROFF_OWN2_FLAG         	   		503-1
#define ROFF_OWN3_LAST			            504-1
#define ROFF_OWN3_FIRST        			   523-1
#define ROFF_OWN3_MIDDLE			         535-1
#define ROFF_OWN3_FLAG          			   547-1
#define ROFF_OWN4_LAST	   		         548-1
#define ROFF_OWN4_FIRST           			567-1
#define ROFF_OWN4_MIDDLE			         579-1
#define ROFF_OWN4_FLAG            			591-1
#define ROFF_PRI_PROP_USE_CODE	         592-1
#define ROFF_SEC_PROP_USE_CODE	         593-1
#define ROFF_NO_UNITS			            595-1
#define ROFF_MULT_CODE			            598-1
#define ROFF_FILLER5				            599-1

typedef struct _tRivRoll
{
   char Assessor_Stuff[RSIZ_ASSESSOR_STUFF];
   char Assmnt_No[RSIZ_ASSMNT_NO];
   char Parcel_No[RSIZ_PARCEL_NO];
   char Acres[RSIZ_ACRES];
   char Re_Use_Code[RSIZ_RE_USE_CODE];
   char Filler1[RSIZ_FILLER1];
   char Base_Year[RSIZ_BASE_YEAR];
   char Filler2[RSIZ_FILLER2];
   char Bus_Use_Code[RSIZ_BUS_USE_CODE];
   char Filler3[RSIZ_FILLER3];
   char Tax_Code[RSIZ_TAX_CODE];
   char Tra[RSIZ_TRA];
   char Code1[RSIZ_CODE1];                            // 105
   char Amount1[RSIZ_AMOUNT1];                        // 107
   char Code2[RSIZ_CODE2];                            // 116
   char Amount2[RSIZ_AMOUNT2];
   char Code3[RSIZ_CODE3];                            // 127
   char Amount3[RSIZ_AMOUNT3];
   char Code4[RSIZ_CODE4];
   char Amount4[RSIZ_AMOUNT4];
   char Code5[RSIZ_CODE5];
   char Amount5[RSIZ_AMOUNT5];
   char Code6[RSIZ_CODE6];
   char Amount6[RSIZ_AMOUNT6];
   char Code7[RSIZ_CODE7];
   char Amount7[RSIZ_AMOUNT7];
   char Code8[RSIZ_CODE8];
   char Amount8[RSIZ_AMOUNT8];
   char Code9[RSIZ_CODE9];
   char Amount9[RSIZ_AMOUNT9];
   char Code10[RSIZ_CODE10];
   char Amount10[RSIZ_AMOUNT10];
   char Code11[RSIZ_CODE11];
   char Amount11[RSIZ_AMOUNT11];
   char Code12[RSIZ_CODE12];
   char Amount12[RSIZ_AMOUNT11];
   char Code13[RSIZ_CODE13];
   char Amount13[RSIZ_AMOUNT13];
   char Filler4[RSIZ_FILLER4];
   char Mail_To_Name[RSIZ_MAIL_TO_NAME];
   char MAddress[RSIZ_MADDRESS];
   char MCity_State[RSIZ_MCITY_STATE];
   char MZip4[RSIZ_MZIP4];
   char MZip[RSIZ_MZIP];
   char SStr_Name[RSIZ_SSTR_NAME];
   char SStr_Suffix[RSIZ_SSTR_SUFFIX];
   char SStr_Dir[RSIZ_SSTR_DIR];
   char SStr_No[RSIZ_SSTR_NUMBER];
   char SStr_Sub[RSIZ_SSTR_SUB];
   char SStr_Side[RSIZ_SSTR_SIDE];
   char SUnit[RSIZ_SUNIT];
   char SZip4[RSIZ_SZIZ4];
   char SZip[RSIZ_SZIP];
   char Own1_Last[RSIZ_OWN1_LAST];                    // 416
   char Own1_First[RSIZ_OWN1_FIRST];
   char Own1_Middle[RSIZ_OWN1_MIDDLE];
   char Own1_Flag;
   char Own2_Last[RSIZ_OWN2_LAST];
   char Own2_First[RSIZ_OWN2_FIRST];
   char Own2_Middle[RSIZ_OWN2_MIDDLE];
   char Own2_Flag;                                    // 503
   char Own3_Last[RSIZ_OWN3_LAST];
   char Own3_First[RSIZ_OWN3_FIRST];
   char Own3_Middle[RSIZ_OWN3_MIDDLE];
   char Own3_Flag;
   char Own4_Last[RSIZ_OWN4_LAST];
   char Own4_First[RSIZ_OWN4_FIRST];
   char Own4_Middle[RSIZ_OWN4_MIDDLE];
   char Own4_Flag;
   char Pri_Prop_Use_Code[RSIZ_PRI_PROP_USE_CODE];
   char Sec_Prop_Use_Code[RSIZ_SEC_PROP_USE_CODE];
   char No_Units[RSIZ_NO_UNITS];
   char Mult_Code[RSIZ_MULT_CODE];                    // 598
   char Filler5[RSIZ_FILLER5];
} RIV_ROLL;

#define SIZ_LIEN_PRIMARY_PARCEL_NO                9
#define SIZ_LIEN_RECORD_TYPE		                 2   
#define SIZ_LIEN_SEQUENCE                         2
#define SIZ_LIEN_SEGMENT_DEPENDANT          	     147
#define OFF_LIEN_PRIMARY_PARCEL_NO			        1-1
#define OFF_LIEN_RECORD_TYPE			   		     10-1
#define OFF_LIEN_SEQUENCE			   		        12-1
#define OFF_LIEN_SEGMENT_DEPENDANT			        14-1


#define SIZ_LIEN_RECORDER_NO		                 9
#define SIZ_LIEN_RECORDING_DATE	                 6
#define SIZ_LIEN_MULT_SALE_FLAG	                 1
#define SIZ_LIEN_TRANSFER_TYPE	                 3
#define SIZ_LIEN_STAMPS			                    8
#define SIZ_LIEN_FULL_VALUE_FLAG	                 1
#define SIZ_LIEN_USE_CODE		                    2
#define SIZ_LIEN_PUI_CODE		                    7
#define SIZ_LIEN_BASE_YEAR		                    4
#define SIZ_LIEN_MULT_BASE_YEAR_FLAG              1
#define SIZ_LIEN_BUS_USE_CODE	                    4
#define SIZ_LIEN_FILLER1			                 102
#define OFF_LIEN_RECORDER_NO			   		     14-1
#define OFF_LIEN_RECORDING_DATE		   		     23-1
#define OFF_LIEN_MULT_SALE_FLAG		   		     29-1
#define OFF_LIEN_TRANSFER_TYPE		   		     30-1
#define OFF_LIEN_STAMPS						           33-1
#define OFF_LIEN_FULL_VALUE_FLAG		   		     41-1
#define OFF_LIEN_USE_CODE			   		        42-1
#define OFF_LIEN_PUI_CODE			                 44-1
#define OFF_LIEN_BASE_YEAR			   	           51-1
#define OFF_LIEN_MULT_BASE_YEAR_FLAG    		     54-1
#define OFF_LIEN_BUS_USE_CODE	   			        56-1
#define OFF_LIEN_FILLER1						        60-1

typedef struct _tRivLien11
{
	char Primary_Parcel_No[SIZ_LIEN_PRIMARY_PARCEL_NO];
	char Record_Type[SIZ_LIEN_RECORD_TYPE];
	char Sequence[SIZ_LIEN_SEQUENCE];
	char Recorder_No[SIZ_LIEN_RECORDER_NO];
	char Recording_Date[SIZ_LIEN_RECORDING_DATE];
	char Mult_Sale_Flag[SIZ_LIEN_MULT_SALE_FLAG];
	char Transfer_Type[SIZ_LIEN_TRANSFER_TYPE];
	char Stamps[SIZ_LIEN_STAMPS];
	char Full_Value_Flag[SIZ_LIEN_FULL_VALUE_FLAG];
	char Use_Code[SIZ_LIEN_USE_CODE];
	char Pui_Code[SIZ_LIEN_PUI_CODE];
	char Base_Year[SIZ_LIEN_BASE_YEAR];
	char Mult_Base_Year_Flag[SIZ_LIEN_MULT_BASE_YEAR_FLAG];
	char Bus_Use_Code[SIZ_LIEN_BUS_USE_CODE];
	char Filler[SIZ_LIEN_FILLER1];
} RIV_LIEN11;

#define SIZ_LIEN_DBA_NAME		                    43
#define SIZ_LIEN_FILLER2			                 104
#define OFF_LIEN_DBA_NAME		   			        14-1
#define OFF_LIEN_FILLER2						        57-1

typedef struct _tRivLien17
{
	char Primary_Parcel_No[SIZ_LIEN_PRIMARY_PARCEL_NO];
	char Record_Type[SIZ_LIEN_RECORD_TYPE];
	char Sequence[SIZ_LIEN_SEQUENCE];
	char Dba_Name[SIZ_LIEN_DBA_NAME];
	char Filler[SIZ_LIEN_FILLER2];
} RIV_LIEN17;

#define SIZ_LIEN_CAREOF			                    43
#define SIZ_LIEN_FILLER3			                 104
#define OFF_LIEN_CAREOF						           14-1
#define OFF_LIEN_FILLER3						        57-1

typedef struct _tRivLien18
{
	char Primary_Parcel_No[SIZ_LIEN_PRIMARY_PARCEL_NO];
	char Record_Type[SIZ_LIEN_RECORD_TYPE];
	char Sequence[SIZ_LIEN_SEQUENCE];
	char Careof[SIZ_LIEN_CAREOF];
	char Filler[SIZ_LIEN_FILLER3];
} RIV_LIEN18;

#define SIZ_LIEN_MSTREET			                 26
#define SIZ_LIEN_MCITY_STATE		                 26
#define SIZ_LIEN_ZIP_PREFIX		                 4
#define SIZ_LIEN_ZIP_CODE		                    5
#define SIZ_LIEN_FILLER4			                 86
#define OFF_LIEN_MSTREET						        14-1
#define OFF_LIEN_MCITY_STATE		   			     40-1
#define OFF_LIEN_ZIP_PREFIX		   			     66-1
#define OFF_LIEN_ZIP_CODE		   			        70-1
#define OFF_LIEN_FILLER4						        75-1

typedef struct _tRivLien19
{
	char Primary_Parcel_No[SIZ_LIEN_PRIMARY_PARCEL_NO];
	char Record_Type[SIZ_LIEN_RECORD_TYPE];
	char Sequence[SIZ_LIEN_SEQUENCE];
	char MStreet[SIZ_LIEN_MSTREET];
	char MCity_State[SIZ_LIEN_MCITY_STATE];
	char Zip_Prefix[SIZ_LIEN_ZIP_PREFIX];
	char Zip_Code[SIZ_LIEN_ZIP_CODE];
	char Filler[SIZ_LIEN_FILLER4];
} RIV_LIEN19;

#define SIZ_LIEN_SSTR_NO			                 6
#define SIZ_LIEN_SSTR_FRACT		                 1
#define SIZ_LIEN_SSTR_DIR		                    1
#define SIZ_LIEN_SSTR_NAME		                    18
#define SIZ_LIEN_SSTR_SUFFIX		                 3
#define SIZ_LIEN_UNIT			                    4
#define SIZ_LIEN_SZIP_PREFIX		                 4
#define SIZ_LIEN_SZIP_CODE		                    5
#define SIZ_LIEN_SCITY			                    13
#define SIZ_LIEN_FILLER5			                 92
#define OFF_LIEN_SSTR_NO						        14-1
#define OFF_LIEN_SSTR_FRACT		   			     20-1
#define OFF_LIEN_SSTR_DIR		   			        21-1
#define OFF_LIEN_SSTR_NAME		   			        22-1
#define OFF_LIEN_SSTR_SUFFIX		   			     40-1
#define OFF_LIEN_UNIT						           43-1
#define OFF_LIEN_SZIP_PREFIX		   			     47-1
#define OFF_LIEN_SZIP_CODE		   			        51-1
#define OFF_LIEN_SCITY						           56-1
#define OFF_LIEN_FILLER5						        69-1

typedef struct _tRivLien20
{
	char Primary_Parcel_No[SIZ_LIEN_PRIMARY_PARCEL_NO];
	char Record_Type[SIZ_LIEN_RECORD_TYPE];
	char Sequence[SIZ_LIEN_SEQUENCE];
	char SStr_No[SIZ_LIEN_SSTR_NO];
	char SStr_Fract[SIZ_LIEN_SSTR_FRACT];
	char SStr_Dir[SIZ_LIEN_SSTR_DIR];
	char SStr_Name[SIZ_LIEN_SSTR_NAME];
	char SStr_Suffix[SIZ_LIEN_SSTR_SUFFIX];
	char Unit[SIZ_LIEN_UNIT];
	char SZip_Prefix[SIZ_LIEN_SZIP_PREFIX];
	char SZip_Code[SIZ_LIEN_SZIP_CODE];
	char SCity[SIZ_LIEN_SCITY];
	char Filler[SIZ_LIEN_FILLER5];
} RIV_LIEN20;


#define SIZ_LIEN_ASSESSMENT		                 50
#define SIZ_LIEN_FILLER6			                 97
#define OFF_LIEN_ASSESSMENT		   			     14-1
#define OFF_LIEN_FILLER6						        64-1

typedef struct _tRivLien21
{
	char Primary_Parcel_No[SIZ_LIEN_PRIMARY_PARCEL_NO];
	char Record_Type[SIZ_LIEN_RECORD_TYPE];
	char Sequence[SIZ_LIEN_SEQUENCE];
	char Assessment[SIZ_LIEN_ASSESSMENT];
	char Filler[SIZ_LIEN_FILLER6];
} RIV_LIEN21;

#define SIZ_LIEN_TAX_YEAR		                    4
#define SIZ_LIEN_TAX_CODE		                    3
#define SIZ_LIEN_RELATION_CODE	                 2
#define SIZ_LIEN_RIVTRA				                 6
#define SIZ_LIEN_ETAL			                    1
#define SIZ_LIEN_REFERENCE_APN	                 9
#define SIZ_LIEN_FILLER7			                 122
#define OFF_LIEN_TAX_YEAR					           14-1
#define OFF_LIEN_TAX_CODE			   		        18-1
#define OFF_LIEN_RELATION_CODE		   		     21-1
#define OFF_LIEN_RIVTRA				                 23-1
#define OFF_LIEN_ETAL						           29-1
#define OFF_LIEN_REFERENCE_APN		   		     30-1
#define OFF_LIEN_FILLER7						        39-1
   
typedef struct _tRivLien23
{
	char Primary_Parcel_No[SIZ_LIEN_PRIMARY_PARCEL_NO];
	char Record_Type[SIZ_LIEN_RECORD_TYPE];
	char Sequence[SIZ_LIEN_SEQUENCE];
	char Tax_Year[SIZ_LIEN_TAX_YEAR];
	char Tax_Code[SIZ_LIEN_TAX_CODE];
	char Relation_Code[SIZ_LIEN_RELATION_CODE];
	char Tra[SIZ_LIEN_RIVTRA];
	char Etal[SIZ_LIEN_ETAL];
	char Reference_Apn[SIZ_LIEN_REFERENCE_APN];
	char Filler[SIZ_LIEN_FILLER7];
} RIV_LIEN23;

#define SIZ_LIEN_RECORDER_MAP_TYPE                2
#define SIZ_LIEN_BOOK			                    3
#define SIZ_LIEN_PAGE			                    3
#define SIZ_LIEN_COUNTY_CODE		                 2
#define SIZ_LIEN_LOT_TYPE		                    1
#define SIZ_LIEN_LOT				                    4
#define SIZ_LIEN_BLOCK			                    3
#define SIZ_LIEN_LOT_INDICATOR	                 1
#define SIZ_LIEN_LOT_PLUS_INDICATOR               1
#define SIZ_LIEN_SUBDIV			                    48
#define SIZ_LIEN_FILLER8			                 79
#define OFF_LIEN_RECORDER_MAP_TYPE      		     14-1
#define OFF_LIEN_BOOK						           16-1
#define OFF_LIEN_PAGE						           19-1
#define OFF_LIEN_COUNTY_CODE			   		     22-1
#define OFF_LIEN_LOT_TYPE			   		        24-1
#define OFF_LIEN_LOT							           25-1
#define OFF_LIEN_BLOCK						           29-1
#define OFF_LIEN_LOT_INDICATOR		   		     32-1
#define OFF_LIEN_LOT_PLUS_INDICATOR     		     33-1
#define OFF_LIEN_SUBDIV						           34-1
#define OFF_LIEN_FILLER8						        82-1

typedef struct _tRivLien26
{
	char Primary_Parcel_No[SIZ_LIEN_PRIMARY_PARCEL_NO];
	char Record_Type[SIZ_LIEN_RECORD_TYPE];
	char Sequence[SIZ_LIEN_SEQUENCE];
	char Recorder_Map_Type__[SIZ_LIEN_RECORDER_MAP_TYPE];
	char Book[SIZ_LIEN_BOOK];
	char Page[SIZ_LIEN_PAGE];
	char County_Code[SIZ_LIEN_COUNTY_CODE];
	char Lot_Type[SIZ_LIEN_LOT_TYPE];
	char Lot[SIZ_LIEN_LOT];
	char Block[SIZ_LIEN_BLOCK];
	char Lot_Indicator[SIZ_LIEN_LOT_INDICATOR];
	char Lot_Plus_Indicator_[SIZ_LIEN_LOT_PLUS_INDICATOR];
	char Subdiv[SIZ_LIEN_SUBDIV];
	char Filler[SIZ_LIEN_FILLER8];
} RIV_LIEN26;

#define SIZ_LIEN_ACRES			                    5
#define SIZ_LIEN_ACRES_QUAL_CODE	                 2
#define SIZ_LIEN_1ST_PORTION		                 2
#define SIZ_LIEN_1ST_SECTION_NO	                 2
#define SIZ_LIEN_1ST_TOWNSHIP	                    1
#define SIZ_LIEN_1ST_RANGE		                    2
#define SIZ_LIEN_1ST_RANGE_DIR	                 1
#define SIZ_LIEN_2ND_PORTION		                 2
#define SIZ_LIEN_2ND_SECTION_NO		              2
#define SIZ_LIEN_2ND_TOWNSHIP	                    1
#define SIZ_LIEN_2ND_RANGE		                    2
#define SIZ_LIEN_2ND_RANGE_DIR	                 1
#define SIZ_LIEN_FILLER9			                 124
#define OFF_LIEN_ACRES						           14-1
#define OFF_LIEN_ACRES_QUAL_CODE		   		     19-1
#define OFF_LIEN_1ST_PORTION			   		     21-1
#define OFF_LIEN_1ST_SECTION_NO		   		     23-1
#define OFF_LIEN_1ST_TOWNSHIP		   		        25-1
#define OFF_LIEN_1ST_RANGE			   		        26-1
#define OFF_LIEN_1ST_RANGE_DIR		   		     28-1
#define OFF_LIEN_2ND_PORTION			   		     29-1
#define OFF_LIEN_2ND_SECTION_NO				        31-1
#define OFF_LIEN_2ND_TOWNSHIP		   		        33-1
#define OFF_LIEN_2ND_RANGE			   		        34-1
#define OFF_LIEN_2ND_RANGE_DIR		   		     36-1
#define OFF_LIEN_FILLER9			   		        37-1

typedef struct _tRivLien28
{
	char Primary_Parcel_No[SIZ_LIEN_PRIMARY_PARCEL_NO];
	char Record_Type[SIZ_LIEN_RECORD_TYPE];
	char Sequence[SIZ_LIEN_SEQUENCE];
	char Acres[SIZ_LIEN_ACRES];
	char Acres_Qual_Code[SIZ_LIEN_ACRES_QUAL_CODE];
	char First_Portion[SIZ_LIEN_1ST_PORTION];
	char First_Section_No[SIZ_LIEN_1ST_SECTION_NO];
	char First_Township[SIZ_LIEN_1ST_TOWNSHIP];
	char First_Range[SIZ_LIEN_1ST_RANGE];
	char First_Range_Dir[SIZ_LIEN_1ST_RANGE_DIR];
	char Second_Portion[SIZ_LIEN_2ND_PORTION];
	char Second_Section_No[SIZ_LIEN_2ND_SECTION_NO];
	char Second_Township[SIZ_LIEN_2ND_TOWNSHIP];
	char Second_Range[SIZ_LIEN_2ND_RANGE];
	char Second_Range_Dir[SIZ_LIEN_2ND_RANGE_DIR];
	char Filler[SIZ_LIEN_FILLER9];
} RIV_LIEN28;

#define SIZ_LIEN_LEGAL			                    43
#define SIZ_LIEN_FILLER10		                    104
#define OFF_LIEN_LEGAL						           14-1
#define OFF_LIEN_FILLER10			   		        57-1

typedef struct _tRivLien30
{
	char Primary_Parcel_No[SIZ_LIEN_PRIMARY_PARCEL_NO];
	char Record_Type[SIZ_LIEN_RECORD_TYPE];
	char Sequence[SIZ_LIEN_SEQUENCE];
	char Legal[SIZ_LIEN_LEGAL];
	char Filler[SIZ_LIEN_FILLER10];
} RIV_LIEN30;

#define SIZ_LIEN_OWNER_NAME		                 43
#define SIZ_LIEN_NAME_SUFFIX		                 3
#define SIZ_LIEN_TITLE_CODE		                 4
#define SIZ_LIEN_VEST_TITLE		                 3
#define SIZ_LIEN_NAME_FORMAT		                 1
#define SIZ_LIEN_FILLER11		                    93
#define OFF_LIEN_OWNER_NAME			   		     14-1
#define OFF_LIEN_NAME_SUFFIX			   		     57-1
#define OFF_LIEN_TITLE_CODE			   		     60-1
#define OFF_LIEN_VEST_TITLE			   		     64-1
#define OFF_LIEN_NAME_FORMAT			   		     67-1
#define OFF_LIEN_FILLER11			   		        68-1

typedef struct _tRivLien52
{
	char Primary_Parcel_No[SIZ_LIEN_PRIMARY_PARCEL_NO];
	char Record_Type[SIZ_LIEN_RECORD_TYPE];
	char Sequence[SIZ_LIEN_SEQUENCE];
	char Owner_Name[SIZ_LIEN_OWNER_NAME];
	char Name_Suffix[SIZ_LIEN_NAME_SUFFIX];
	char Title_Code[SIZ_LIEN_TITLE_CODE];
	char Vest_Title[SIZ_LIEN_VEST_TITLE];
	char Name_Format[SIZ_LIEN_NAME_FORMAT];
	char Filler[SIZ_LIEN_FILLER11];
} RIV_LIEN52;

#define SIZ_LIEN_CODE1			                    2
#define SIZ_LIEN_AMOUNT1			                 9
#define SIZ_LIEN_CODE2			                    2
#define SIZ_LIEN_AMOUNT2			                 9
#define SIZ_LIEN_CODE3			                    2
#define SIZ_LIEN_AMOUNT3			                 9
#define SIZ_LIEN_CODE4			                    2
#define SIZ_LIEN_AMOUNT4			                 9
#define SIZ_LIEN_CODE5			                    2
#define SIZ_LIEN_AMOUNT5			                 9
#define SIZ_LIEN_CODE6			                    2
#define SIZ_LIEN_AMOUNT6			                 9
#define SIZ_LIEN_CODE7			                    2
#define SIZ_LIEN_AMOUNT7			                 9
#define SIZ_LIEN_CODE8			                    2
#define SIZ_LIEN_AMOUNT8			                 9
#define SIZ_LIEN_CODE9			                    2
#define SIZ_LIEN_AMOUNT9			                 9
#define SIZ_LIEN_CODE10			                    2
#define SIZ_LIEN_AMOUNT10		                    9
#define SIZ_LIEN_CODE11			                    2
#define SIZ_LIEN_AMOUNT11		                    9
#define SIZ_LIEN_CODE12			                    2
#define SIZ_LIEN_AMOUNT12		                    9
#define SIZ_LIEN_CODE13			                    2
#define SIZ_LIEN_AMOUNT13		                    9
#define SIZ_LIEN_FILLER12		                    4
#define OFF_LIEN_CODE1						           14-1
#define OFF_LIEN_AMOUNT1						        16-1
#define OFF_LIEN_CODE2						           25-1
#define OFF_LIEN_AMOUNT2						        27-1
#define OFF_LIEN_CODE3						           36-1
#define OFF_LIEN_AMOUNT3						        38-1
#define OFF_LIEN_CODE4						           47-1
#define OFF_LIEN_AMOUNT4						        49-1
#define OFF_LIEN_CODE5						           58-1
#define OFF_LIEN_AMOUNT5						        60-1
#define OFF_LIEN_CODE6						           69-1
#define OFF_LIEN_AMOUNT6						        71-1
#define OFF_LIEN_CODE7						           80-1
#define OFF_LIEN_AMOUNT7						        82-1
#define OFF_LIEN_CODE8						           91-1
#define OFF_LIEN_AMOUNT8						        93-1
#define OFF_LIEN_CODE9						           102-1
#define OFF_LIEN_AMOUNT9						        104-1
#define OFF_LIEN_CODE10						           113-1
#define OFF_LIEN_AMOUNT10			   		        115-1
#define OFF_LIEN_CODE11						           124-1
#define OFF_LIEN_AMOUNT11			   		        126-1
#define OFF_LIEN_CODE12						           135-1
#define OFF_LIEN_AMOUNT12			   		        137-1
#define OFF_LIEN_CODE13						           146-1
#define OFF_LIEN_AMOUNT13			  		           148-1
#define OFF_LIEN_FILLER12			   		        157-1
/*
'01'='LND'   LAND
'02'='STR'   STRUCTURE
'03'='FIX'   TRADE FIXTURES
'04'='BIV'   BUSINESS INVENTORY
'05'='TVV'   TREES/VINES
'06'='CPV'   CITRUS PEST VALUE
'07'='HPP'   HOUSEHOLD PERS. PROP.
'08'='BPP'   BUSINESS PERS. PROP.
'09'='RPP'   RANCH PERS. PROP.
'10'='OPP'   OTHER PERS. PROP.
'13'='BOT'   BOAT VALUE PERS. PROP.
'14'='NCA'   NON-COMMERCIAL AIRCRAFT
'15'='P10'   PENALTY 10%
'16'='P25'   PENALTY 25%
'17'='OAP'   OTHER ASSESSED PENALTY
'21'='HOX'   HOMEOWNERS
'22'='VET'   VETERANS
'23'='DVT'   DISABLED VETERAN
'24'='BVT'   BLIND VETERAN
'25'='CHX'   CHURCH
'26'='REL'   RELIGIOUS
'27'='CEM'   CEMETARY
'28'='TVX'   TREE/VINE
'29'='HFA'   HOME FOR AGED
'30'='WIN'   WINE
'31'='FPS'   FREE PUBLIC SCHOOL
'32'='FPL'   FREE PUBLIC LIBRARY
'33'='FPM'   FREE PUBLIC MUSEUM
'34'='WCX'   WELFARE - COLLEGE
'35'='WHX'   WELFARE - HOSPITAL
'36'='WPS'   WELFARE - PRIVATE/PAROCHIAL SCHOOL
'37'='WCR'   WELFARE - CHARITY/RELIGIOUS
'38'='OXX'   OTHER EXEMPTION
'39'='SSR'   SOLDIER/SAILOR RELIEF ACT
'40'='BIX'   BUSINESS INVENTORY
*/

typedef struct _tRivValExe
{
	char Code[SIZ_LIEN_CODE1];
	char Amount[SIZ_LIEN_AMOUNT1];
} RIV_VALEXE;

typedef struct _tRivLien53
{
	char Primary_Parcel_No[SIZ_LIEN_PRIMARY_PARCEL_NO];
	char Record_Type[SIZ_LIEN_RECORD_TYPE];
	char Sequence[SIZ_LIEN_SEQUENCE];
   RIV_VALEXE ValExe[13];
	char Filler[SIZ_LIEN_FILLER12];
} RIV_LIEN53;

#define SIZ_LIEN_SOLD_YEAR		                    4
#define SIZ_LIEN_SOLD_NO			                 9
#define SIZ_LIEN_SOLD_SUFFIX		                 4
#define SIZ_LIEN_FILLER13		                    130
#define OFF_LIEN_SOLD_YEAR			   		        14-1
#define OFF_LIEN_SOLD_NO						        18-1
#define OFF_LIEN_SOLD_SUFFIX			   		     27-1
#define OFF_LIEN_FILLER13			   		        31-1

typedef struct _tRivLien67
{
	char Primary_Parcel_No[SIZ_LIEN_PRIMARY_PARCEL_NO];
	char Record_Type[SIZ_LIEN_RECORD_TYPE];
	char Sequence[SIZ_LIEN_SEQUENCE];
	char Sold_Year[SIZ_LIEN_SOLD_YEAR];
	char Sold_No[SIZ_LIEN_SOLD_NO];
	char Sold_Suffix[SIZ_LIEN_SOLD_SUFFIX];
	char Filler[SIZ_LIEN_FILLER13];
} RIV_LIEN67;

#define SIZ_LIEN_BOAT_ID			                 8
#define SIZ_LIEN_FILLER14		                    139
#define OFF_LIEN_BOAT_ID						        14-1
#define OFF_LIEN_FILLER14			   		        22-1

typedef struct _tRivLien71
{
	char Primary_Parcel_No[SIZ_LIEN_PRIMARY_PARCEL_NO];
	char Record_Type[SIZ_LIEN_RECORD_TYPE];
	char Sequence[SIZ_LIEN_SEQUENCE];
	char Boat_Id[SIZ_LIEN_BOAT_ID];
	char Filler[SIZ_LIEN_FILLER14];
} RIV_LIEN71;

#define SIZ_LIEN_AIRPLANE_ID		                 7
#define SIZ_LIEN_FILLER15		                    140
#define OFF_LIEN_AIRPLANE_ID			   		     14-1
#define OFF_LIEN_FILLER15			   		        21-1

typedef struct _tRivLien72
{
	char Primary_Parcel_No[SIZ_LIEN_PRIMARY_PARCEL_NO];
	char Record_Type[SIZ_LIEN_RECORD_TYPE];
	char Sequence[SIZ_LIEN_SEQUENCE];
	char Airplane_Id[SIZ_LIEN_AIRPLANE_ID];
	char Filler[SIZ_LIEN_FILLER15];
} RIV_LIEN72;

#define SIZ_LIEN_OWNER_ID		                    43
#define SIZ_LIEN_BAND			                    1
#define SIZ_LIEN_FILLER16		                    103
#define OFF_LIEN_OWNER_ID			   		        14-1
#define OFF_LIEN_BAND						           57-1
#define OFF_LIEN_FILLER16			   		        58-1

typedef struct _tRivLien73
{
	char Primary_Parcel_No[SIZ_LIEN_PRIMARY_PARCEL_NO];
	char Record_Type[SIZ_LIEN_RECORD_TYPE];
	char Sequence[SIZ_LIEN_SEQUENCE];
	char Owner_Id[SIZ_LIEN_OWNER_ID];
	char Band[SIZ_LIEN_BAND];
	char Filler[SIZ_LIEN_FILLER16];
} RIV_LIEN73;

#define SIZ_LIEN_COMMENT			                 35
#define SIZ_LIEN_FILLER17		                    112
#define OFF_LIEN_COMMENT						        14-1
#define OFF_LIEN_FILLER17			   		        49-1

typedef struct _tRivLien74
{
	char Primary_Parcel_No[SIZ_LIEN_PRIMARY_PARCEL_NO];
	char Record_Type[SIZ_LIEN_RECORD_TYPE];
	char Sequence[SIZ_LIEN_SEQUENCE];
	char Comment[SIZ_LIEN_COMMENT];
	char Filler[SIZ_LIEN_FILLER17];
} RIV_LIEN74;

#define SIZ_LIEN_MHOME1_ID		                    7
#define SIZ_LIEN_MHOME2_ID		                    7
#define SIZ_LIEN_FILLER18			                 133
#define OFF_LIEN_MHOME1_ID			   		        14-1
#define OFF_LIEN_MHOME2_ID			   		        21-1
#define OFF_LIEN_FILLER18					           28-1

typedef struct _tRivLien75
{
	char Primary_Parcel_No[SIZ_LIEN_PRIMARY_PARCEL_NO];
	char Record_Type[SIZ_LIEN_RECORD_TYPE];
	char Sequence[SIZ_LIEN_SEQUENCE];
	char Mhome1_Id[SIZ_LIEN_MHOME1_ID];
	char Mhome2_Id[SIZ_LIEN_MHOME2_ID];
	char Filler[SIZ_LIEN_FILLER18];
} RIV_LIEN75;

// Characteristics

#define CSIZ_PRIMARY_PARCEL_NO		      9
#define CSIZ_RECORD_NO		 		         2
#define CSIZ_SSTR_NAME		 		         18
#define CSIZ_SSTR_SUFFIX		 		      3
#define CSIZ_SDIR			 		            1
#define CSIZ_SSTR_NO			 		         6
#define CSIZ_SFRACT_CODE		 		      1
#define CSIZ_SIDE			 		            1
#define CSIZ_UNIT			 		            4
#define CSIZ_ZIP_PREFIX		 		         4
#define CSIZ_ZIP_CODE		 		         5
#define CSIZ_ACRES			 		         5
#define CSIZ_PUI_CODES		 		         7
#define CSIZ_WATER_FRONT		 		      1
#define CSIZ_ELEC			 		            1
#define CSIZ_GAS				 		         1
#define CSIZ_SEWER			 		         1
#define CSIZ_WATER			 		         1
#define CSIZ_STR_SURFACED	 		         1
#define CSIZ_FILLER1			 		         1
#define CSIZ_CONSTR_YEAR		 		      4
#define CSIZ_STORIES			 		         1
#define CSIZ_ROOF_TYPE		 		         1
#define CSIZ_GARAGE1			 		         1
#define CSIZ_GARAGE1_SQFT	 		         4
#define CSIZ_CARPORT1_SQFT	 		         4
#define CSIZ_GARAGE2			 		         1
#define CSIZ_GARAGE2_SQFT	 		         4
#define CSIZ_CARPORT2_SQFT	 		         4
#define CSIZ_MISC			 		            1
#define CSIZ_POOL			 		            1
#define CSIZ_FIREPLACE		 		         1
#define CSIZ_MISC_IMPROV		 		      1
#define CSIZ_LIVING_AREA		 		      5
#define CSIZ_TOT_IMPROV		 		         6
#define CSIZ_ADDT_AREA 		 		         1
#define CSIZ_BEDROOMS		 		         2
#define CSIZ_FULL_BATHS		 		         1
#define CSIZ_BATHS_34		 		         1
#define CSIZ_HALF_BATHS		 		         1
#define CSIZ_CENTRAL_HEAT	 		         1
#define CSIZ_CENTRAL_AIR		 		      1
#define CSIZ_FILLER2			 		         30


#define COFF_PRIMARY_PARCEL_NO   	      1-1
#define COFF_RECORD_NO		    	         10-1
#define COFF_SSTR_NAME		    	         12-1
#define COFF_SSTR_SUFFIX		    	      30-1
#define COFF_SDIR			    	            33-1
#define COFF_SSTR_NO			    	         34-1
#define COFF_SFRACT_CODE		    	      40-1
#define COFF_SIDE			    	            41-1
#define COFF_UNIT			    	            42-1
#define COFF_ZIP_PREFIX		    	         46-1
#define COFF_ZIP_CODE		    	         50-1
#define COFF_ACRES			    	         55-1
#define COFF_PUI_CODES		    	         60-1
#define COFF_WATER_FRONT		    	      67-1
#define COFF_ELEC			    	            68-1
#define COFF_GAS			     		         69-1
#define COFF_SEWER			    	         70-1
#define COFF_WATER			    	         71-1
#define COFF_STR_SURFACED			         72-1
#define COFF_FILLER1			    	         73-1
#define COFF_CONSTR_YEAR		    	      74-1
#define COFF_STORIES			    	         78-1
#define COFF_ROOF_TYPE		    	         79-1
#define COFF_GARAGE1			    	         80-1
#define COFF_GARAGE1_SQFT			         81-1
#define COFF_CARPORT1_SQFT	    	         85-1
#define COFF_GARAGE2			    	         89-1
#define COFF_GARAGE2_SQFT	    	         90-1
#define COFF_CARPORT2_SQFT	    	         94-1
#define COFF_MISC			    	            98-1
#define COFF_POOL			    	            99-1
#define COFF_FIREPLACE		    	         100-1
#define COFF_MISC_IMPROV		    	      101-1
#define COFF_LIVING_AREA		    	      102-1
#define COFF_TOT_IMPROV		    	         107-1
#define COFF_ADDT_AREA 		    	         113-1
#define COFF_BEDROOMS		    	         114-1
#define COFF_FULL_BATHS		    	         116-1
#define COFF_BATHS_34		    	         117-1
#define COFF_HALF_BATHS		    	         118-1
#define COFF_CENTRAL_HEAT	    	         119-1
#define COFF_CENTRAL_AIR		    	      120-1
#define COFF_FILLER2			    	         121-1

typedef struct _tRivChar
{
	char Primary_Parcel_No[CSIZ_PRIMARY_PARCEL_NO];
	char Record_No[CSIZ_RECORD_NO];
	char SStr_Name[CSIZ_SSTR_NAME];
	char SStr_Suffix[CSIZ_SSTR_SUFFIX];
	char Sdir[CSIZ_SDIR];
	char SStr_No[CSIZ_SSTR_NO];
	char Sfract_Code[CSIZ_SFRACT_CODE];
	char Side[CSIZ_SIDE];
	char Unit[CSIZ_UNIT];
	char Zip_Prefix[CSIZ_ZIP_PREFIX];
	char Zip_Code[CSIZ_ZIP_CODE];
	char Acres[CSIZ_ACRES];
	char Pui_Codes[CSIZ_PUI_CODES];
	char Water_Front[CSIZ_WATER_FRONT];
	char Elec[CSIZ_ELEC];
	char Gas [CSIZ_GAS];
	char Sewer[CSIZ_SEWER];
	char Water[CSIZ_WATER];
	char Str_Surfaced[CSIZ_STR_SURFACED];
	char Filler1[CSIZ_FILLER1];
	char Constr_Year[CSIZ_CONSTR_YEAR];
	char Stories[CSIZ_STORIES];
	char Roof_Type[CSIZ_ROOF_TYPE];
	char Garage1[CSIZ_GARAGE1];
	char Garage1_Sqft[CSIZ_GARAGE1_SQFT];
	char Carport1_Sqft [CSIZ_CARPORT1_SQFT];
	char Garage2[CSIZ_GARAGE2];
	char Garage2_Sqft [CSIZ_GARAGE2_SQFT];
	char Carport2_Sqft [CSIZ_CARPORT2_SQFT];
	char Misc[CSIZ_MISC];
	char Pool[CSIZ_POOL];
	char Fireplace[CSIZ_FIREPLACE];        // Y/N
	char Misc_Improv[CSIZ_MISC_IMPROV];    // Y/N
	char Living_Area[CSIZ_LIVING_AREA];
	char Tot_Improv[CSIZ_TOT_IMPROV];
	char Addt_Area [CSIZ_ADDT_AREA];       // Y/N
	char Bedrooms[CSIZ_BEDROOMS];
	char Full_Baths[CSIZ_FULL_BATHS];
	char Baths_34[CSIZ_BATHS_34];
	char Half_Baths[CSIZ_HALF_BATHS];
	char Central_Heat [CSIZ_CENTRAL_HEAT];
	char Central_Air[CSIZ_CENTRAL_AIR];
	char Filler2[CSIZ_FILLER2];
} RIV_CHAR;

#define  HSIZ_ASSMNT_NO          9
#define  HSIZ_RECNUM             7
#define  HSIZ_RECDATE            8  // YYYYMMDD
#define  HSIZ_TRANSFER_TYPE      3
#define  HSIZ_MULTI_FLG          1  // multi sale flag
#define  HSIZ_FULL_VALUE_FLG     1  // less lien
#define  HSIZ_SALE_AMT           10

typedef struct _tRivCumSale
{  // 72-bytes
   char  Apn[HSIZ_ASSMNT_NO];
   char  ParcelNo[HSIZ_ASSMNT_NO];
   char  filler[2];
   char  RecNum[HSIZ_RECNUM];
   char  RecDate[HSIZ_RECDATE];
   char  TransferType[HSIZ_TRANSFER_TYPE];
   char  MultiFlg;                  // Y/N
   char  FullValueFlg;
   char  ConfSaleAmt[HSIZ_SALE_AMT];
   char  RepSaleAmt[HSIZ_SALE_AMT];
   char  AdjSaleAmt[HSIZ_SALE_AMT];
   char  CRLF[2];
} RIV_CSAL;

#define  TSIZ_ASMT_NO      9
#define  TSIZ_DIST_NO      6
#define  TSIZ_TRA          6
#define  TSIZ_TAXAMT       9
#define  TSIZ_PENAMT       7
#define  TSIZ_FEEAMT       5
#define  TSIZ_BILNUM       9

typedef struct t_ASC820CD
{  // 56-byte record
   char  Assmnt_No[TSIZ_ASMT_NO];   // 1  Use this field to connect to tax base
   char  filler1;                   // 10
   char  Dist_No[TSIZ_DIST_NO];     // 11
   char  filler2;                   // 17
   char  TRA[TSIZ_TRA];             // 18
   char  Paid_Ind;                  // 24 P=both paid, B=both unpaid, 1=1st inst paid, 2=2nd unpaid
   char  Tax_Amt[TSIZ_TAXAMT];      // 25 S9(7).V99
   char  Pen_Amt[TSIZ_PENAMT];      // 34 S9(5).V99
   char  Fee_Amt[TSIZ_FEEAMT];      // 41 S99.V99 - $10 autitor fee
   char  BillNum[TSIZ_BILNUM];      // 46
   char  CRLF[2];
} RIV_PAID;

#define  TD_SIZ_ASMT_NO    11
#define  TD_SIZ_PARCEL_NO  11
#define  TD_SIZ_TAXRATE    8
#define  TD_SIZ_TYPE       6
#define  TD_SIZ_TAXAMT     10
#define  TD_SIZ_TOTALAMT   12
#define  TD_SIZ_AGENCY     25
#define  TD_SIZ_SPCTAX     8

typedef struct _tRivAgency
{  // 24 bytes
   char  AgencyId[TD_SIZ_TYPE];        // 31
   char  Amount[TD_SIZ_TAXAMT];        // 37
   char  Rate[TD_SIZ_TAXRATE];         // 47
} RIV_AGENCY;

typedef struct _tRivSpecialTax
{  // 26 bytes
   char  Spec_Type[TD_SIZ_TYPE];
   char  Spec_AmtA[TD_SIZ_TAXAMT];
   char  Spec_AmtB[TD_SIZ_TAXAMT];
} RIV_SPCTAX;

typedef struct _tAG660TA
{  // 864 bytes
   char  Assmnt_No[TD_SIZ_ASMT_NO];    // 1
   char  Parcel_No[TD_SIZ_PARCEL_NO];  // 12
   char  Tax_Rate[TD_SIZ_TAXRATE];     // 23
   RIV_AGENCY  Agency[TD_SIZ_AGENCY];  // 31
   RIV_SPCTAX  SpcTax[TD_SIZ_SPCTAX];  // 631
   char  Asmnt_Flag;                   // 839
   char  TotalAmount[TD_SIZ_TOTALAMT]; // 840
   char  Filler[11];                   // 852
   char  CRLF[2];   
} RIV_DETAIL;

// ASPYMIDY - has 2 different record layouts in the same file
// Layout of RecType 'T'
#define  TRT_OFF_ASMT_NO    	   1
#define  TRT_OFF_PARCEL_NO  	   10
#define  TRT_OFF_FILLER1 		   19
#define  TRT_OFF_TRA				   20
#define  TRT_OFF_REC_TYPE		   26       // N or T
#define  TRT_OFF_FILLER2 		   27
#define  TRT_OFF_TAX_YEAR		   28
#define  TRT_OFF_TAX_CODE		   32
#define  TRT_OFF_YEAR_ASSMNT	   35
#define  TRT_OFF_ORIG_ASSMNT	   44
#define  TRT_OFF_ASSESSEE		   53
#define  TRT_OFF_BILL_DATE		   98
#define  TRT_OFF_STATUS_INST1	   108      // Paid date or UNPAID
#define  TRT_OFF_STATUS_INST2	   118
#define  TRT_OFF_TAX_AMT1		   128
#define  TRT_OFF_PEN_AMT1		   137
#define  TRT_OFF_FEE_AMT1		   144
#define  TRT_OFF_TAX_AMT2		   149
#define  TRT_OFF_PEN_AMT2		   158
#define  TRT_OFF_FEE_AMT2		   165
#define  TRT_OFF_ROLL_CHG		   170

// Roll Change data - Occurs 16 times
#define  TRT_OFF_ROLLCHG_NUM	   170
#define  TRT_OFF_ROLLCHG_DEPT	   181
#define  TRT_OFF_ROLLCHG_ORGDT	184      // Original date
#define  TRT_OFF_ROLLCHG_COMDT	194      // Completion date
#define  TRT_OFF_ROLLCHG_FLAG	   204      // �C� = cancel roll change, blank = active roll change
#define  TRT_OFF_ROLLCHG_REV	   205
// Occurs 4 times
#define  TRT_OFF_ROLLCHG_TYPE	   214
#define  TRT_OFF_ROLLCHG_NOAMT	217

#define  TRT_OFF_COMMENTS		   1642

#define  TRT_SIZ_ASMT_NO    	   9
#define  TRT_SIZ_PARCEL_NO  	   9
#define  TRT_SIZ_TRA				   6
#define  TRT_SIZ_REC_TYPE		   1
#define  TRT_SIZ_TAX_YEAR		   4
#define  TRT_SIZ_TAX_CODE		   3
#define  TRT_SIZ_YEAR_ASSMNT	   9
#define  TRT_SIZ_ORIG_ASSMNT	   9
#define  TRT_SIZ_ASSESSEE		   45
#define  TRT_SIZ_BILL_DATE		   10
#define  TRT_SIZ_STATUS_INST	   10
#define  TRT_SIZ_FILLER1			1
#define  TRT_SIZ_FILLER2			1
#define  TRT_SIZ_TAX_AMT			9
#define  TRT_SIZ_PEN_AMT			7
#define  TRT_SIZ_FEE_AMT			5
#define  TRT_SIZ_ROLL_CHG		   1472
#define  TRT_SIZ_COMMENTS		   651
#define  TRT_SIZ_ROLLCHG_NUM	   11
#define  TRT_SIZ_ROLLCHG_DEPT	   3
#define  TRT_SIZ_ROLLCHG_DATE	   10
#define  TRT_SIZ_ROLLCHG_FLAG	   1
#define  TRT_SIZ_ROLLCHG_REV	   9
#define  TRT_SIZ_ROLLCHG_TYPE	   3
#define  TRT_SIZ_ROLLCHG_NOAMT	9

#define  TRT_MAX_ROLLCHG			16
#define  TRT_MAX_ROLLCHG_SUB	   4

typedef struct _tRollChgSub
{
   char  Rollchg_Type	[TRT_SIZ_ROLLCHG_TYPE];
   char  Rollchg_Noamt	[TRT_SIZ_ROLLCHG_NOAMT];
} RCSUB;

typedef struct _tRollChg
{
   char  Rollchg_Num		[TRT_SIZ_ROLLCHG_NUM	];
   char  Rollchg_Dept	[TRT_SIZ_ROLLCHG_DEPT];
   char  Rollchg_OrgDt	[TRT_SIZ_ROLLCHG_DATE];
   char  Rollchg_ComDt	[TRT_SIZ_ROLLCHG_DATE];
   char  Rollchg_Flag	[TRT_SIZ_ROLLCHG_FLAG];
   char  Rollchg_Rev		[TRT_SIZ_ROLLCHG_REV	];
   RCSUB RCSub				[TRT_MAX_ROLLCHG_SUB	];
} ROLLCHG;

typedef struct _tRedemptionT
{
   char  Assmnt_No  		[TRT_SIZ_ASMT_NO     ];
   char  Parcel_No  		[TRT_SIZ_PARCEL_NO   ];
   char  Filler1 			[TRT_SIZ_FILLER1 	   ];
   char  TRA				[TRT_SIZ_TRA			];
   char  Rec_Type			[TRT_SIZ_REC_TYPE	   ];
   char  Filler2 			[TRT_SIZ_FILLER2 	   ];
   char  Tax_Year			[TRT_SIZ_TAX_YEAR	   ];
   char  Tax_Code			[TRT_SIZ_TAX_CODE	   ];
   char  Year_Assmnt		[TRT_SIZ_YEAR_ASSMNT ];
   char  Orig_Assmnt		[TRT_SIZ_ORIG_ASSMNT ];
   char  Assessee			[TRT_SIZ_ASSESSEE	   ];
   char  Bill_Date		[TRT_SIZ_BILL_DATE   ];
   char  Status_Inst1	[TRT_SIZ_STATUS_INST ];
   char  Status_Inst2	[TRT_SIZ_STATUS_INST ];
   char  Tax_Amt1			[TRT_SIZ_TAX_AMT	   ];
   char  Pen_Amt1			[TRT_SIZ_PEN_AMT	   ];
   char  Fee_Amt1			[TRT_SIZ_FEE_AMT	   ];
   char  Tax_Amt2			[TRT_SIZ_TAX_AMT	   ];
   char  Pen_Amt2			[TRT_SIZ_PEN_AMT	   ];
   char  Fee_Amt2			[TRT_SIZ_FEE_AMT	   ];
   ROLLCHG RChg			[TRT_MAX_ROLLCHG	   ];
   char  Comments			[TRT_SIZ_COMMENTS	   ];
} RIV_RDMT;

// Layout of RecType 'N'
#define  TRN_OFF_ASMT_NO    	1
#define  TRN_OFF_PARCEL_NO  	10
#define  TRN_OFF_FILLER1 		19
#define  TRN_OFF_TRA				20
#define  TRN_OFF_REC_TYPE		26
#define  TRN_OFF_STATUS			27
#define  TRN_OFF_M_NAME			37
#define  TRN_OFF_M_ADDR			82
#define  TRN_OFF_M_CITY			108
#define  TRN_OFF_M_ZIP			134
#define  TRN_OFF_PROP_DESC		139
#define  TRN_OFF_RED_AMT		499      // Redeemed amt only when Status=REDEEMED
#define  TRN_OFF_COLL_DATE		508      // Redemption date
#define  TRN_OFF_COLL_NUM		518
#define  TRN_OFF_POS_DATE		525
#define  TRN_OFF_ASMT_DESC		535
#define  TRN_OFF_FROM_ASMT		585
#define  TRN_OFF_TO_ASMT		637
#define  TRN_OFF_DEFAULT_NO	689
#define  TRN_OFF_IPP_DATE		1830

#define  TRN_SIZ_ASMT_NO    	9
#define  TRN_SIZ_PARCEL_NO  	9
#define  TRN_SIZ_FILLER1 		1
#define  TRN_SIZ_TRA				6
#define  TRN_SIZ_REC_TYPE		1
#define  TRN_SIZ_STATUS			10
#define  TRN_SIZ_M_NAME			45
#define  TRN_SIZ_M_ADDR			26
#define  TRN_SIZ_M_CITY			26
#define  TRN_SIZ_M_ZIP			5
#define  TRN_SIZ_PROP_DESC	   360
#define  TRN_SIZ_RED_AMT		9
#define  TRN_SIZ_COLL_DATE		10
#define  TRN_SIZ_COLL_NUM		7
#define  TRN_SIZ_POS_DATE		10
#define  TRN_SIZ_ASMT_DESC		50
#define  TRN_SIZ_FROM_ASMT		52
#define  TRN_SIZ_TO_ASMT		52
#define  TRN_SIZ_DEFAULT_NO	19
#define  TRN_SIZ_IPP_DATE		10
#define  TRN_SIZ_FILLER2		430
#define  TRN_MAX_DEFAULT_NO	60

typedef struct _tRedemptionN
{
   char  Assmnt_No   [TRN_SIZ_ASMT_NO	  ];
   char  Parcel_No   [TRN_SIZ_PARCEL_NO  ];
   char  Filler1 	  	[TRN_SIZ_FILLER1	  ];
   char  Tra			[TRN_SIZ_TRA		  ];
   char  Rec_Type	   [TRN_SIZ_REC_TYPE	  ];
   char  Status		[TRN_SIZ_STATUS	  ];
   char  M_Name		[TRN_SIZ_M_NAME	  ];
   char  M_Addr		[TRN_SIZ_M_ADDR	  ];
   char  M_City		[TRN_SIZ_M_CITY	  ];
   char  M_Zip		  	[TRN_SIZ_M_ZIP		  ];
   char  Prop_Desc	[TRN_SIZ_PROP_DESC  ];
   char  Red_Amt		[TRN_SIZ_RED_AMT	  ];
   char  Coll_Date	[TRN_SIZ_COLL_DATE  ];
   char  Coll_Num	  	[TRN_SIZ_COLL_NUM	  ];
   char  Pos_Date	  	[TRN_SIZ_POS_DATE	  ];
   char  Asmt_Desc	[TRN_SIZ_ASMT_DESC  ];
   char  From_Asmt	[TRN_SIZ_FROM_ASMT  ];
   char  To_Asmt		[TRN_SIZ_TO_ASMT	  ];
   char  Default_No	[TRN_MAX_DEFAULT_NO][TRN_SIZ_DEFAULT_NO];
   char  Ipp_Date	  	[TRN_SIZ_IPP_DATE	  ];
   char  Filler2		[TRN_SIZ_FILLER2	  ];
} RIV_RDMN;

// Assessor_PropertyCharacteristics_Table.csv
#define  C_OBJECTID              0
#define  C_ASMT                  1
#define  C_SHEET_NBR             2
#define  C_STREET_SURFACED       3
#define  C_YEAR_EST_IND          4
#define  C_YEAR_CONSTRUCTED      5
#define  C_NBR_STORIES           6
#define  C_ROOF_TYPE             7
#define  C_GARAGE1_TYPE          8
#define  C_GARAGE1_AREA          9
#define  C_CARPORT1_AREA         10
#define  C_GARAGE2_TYPE          11
#define  C_GARAGE2_AREA          12
#define  C_CARPORT2_AREA         13
#define  C_FIREPLACE_FLAG        14
#define  C_FIREPLACE_VALUE       15
#define  C_BATHS                 16
#define  C_CENTRAL_HEAT          17
#define  C_CENTRAL_COOL          18
#define  C_DESIGN_TYPE           19
#define  C_CONST_TYPE            20
#define  C_STR_QUAL_CD           21
#define  C_STR_SHAPE_CD          22
#define  C_AREA_1                23
#define  C_TOTAL_AREA            24
#define  C_CARPET_GARAGE         25
#define  C_POOL                  26
#define  C_BEDROOMS              27
#define  C_MISC_STR_RCLD         28
#define  C_TOT_RCLD              29
#define  C_WTR_FAIR              30
#define  C_FLDS                  31

// PropertyCharacteristics.csv
#define  C_APN                                     0
#define  C_CLASSCODEDESCRIPTION                    1
#define  C_SITUSSTREETNUMBER                       2
#define  C_SITUSSTREETNUMBERSUFFIX                 3
#define  C_SITUSSTREETPREDIRECTIONAL               4
#define  C_SITUSSTREETNAME                         5
#define  C_SITUSSTREETTYPE                         6
#define  C_SITUSUNITNUMBER                         7
#define  C_SITUSZIPCODE                            8
#define  C_SITUSCITYNAME                           9
#define  C_STREETSURFACED                          10
#define  C_SEWER                                   11
#define  C_DOMESTICWATER                           12
#define  C_IRRIGATIONWATER                         13
#define  C_WELLWATER                               14
#define  C_UTILITYELECTRIC                         15
#define  C_UTILITYGAS                              16
#define  C_EFFYEARBUILT                            17
#define  C_YEARBUILT                               18
#define  C_NUMBEROFSTORIES                         19
#define  C_ROOFMATERIAL                            20
#define  C_GARAGETYPE                              21
#define  C_GARAGEAREA                              22
#define  C_CARPORTSIZE                             23
#define  C_GARAGE2TYPE                             24
#define  C_GARAGE2AREA                             25
#define  C_CARPORT2SIZE                            26
#define  C_HASFIREPLACE                            27
#define  C_BATHSFULL                               28
#define  C_BATHS3_4                                29
#define  C_BATHSHALF                               30
#define  C_TOTALBATHS                              31
#define  C_HASCENTRALHEATING                       32
#define  C_HASCENTRALCOOLING                       33
#define  C_DESIGNTYPE                              34
#define  C_CONSTRUCTIONTYPE                        35
#define  C_QUALITYCODE                             36
#define  C_SHAPECODE                               37
#define  C_LIVINGAREA                              38
#define  C_ACTUALAREA                              39
#define  C_HASPOOL                                 40
#define  C_BEDROOMCOUNT                            41
#define  C_FAIRWAY                                 42
#define  C_WATERFRONT                              43
#define  C_ACREAGE                                 44    // Added 04/10/2020
#define  C_LANDUNITTYPE                            45
#define  C_SOLAR                                   46
#define  C_COLS                                    47

// AssessmentRoll.csv - Layout changed 11/07/2019
#define  R_APN                                     0
#define  R_CONVENYANCENUMBER                       1
#define  R_CONVEYANCEMONTH                         2
#define  R_CONVEYANCEYEAR                          3
#define  R_CLASSDESCRIPTION                        4
#define  R_BASEYEAR                                5
#define  R_BUSINESSUSECODEDESCRIPTION              6
#define  R_DOINGBUSINESSAS                         7
#define  R_MAILNAME                                8
#define  R_MAILADDRESS                             9
#define  R_MAILCITY                                10
#define  R_MAILSTATE                               11
#define  R_MAILZIPCODE                             12
#define  R_SITUSSTREETNUMBER                       13
#define  R_SITUSSTREETNUMBERSUFFIX                 14
#define  R_SITUSSTREETPREDIRECTIONAL               15
#define  R_SITUSSTREETNAME                         16
#define  R_SITUSSTREETTYPE                         17
#define  R_SITUSUNITNUMBER                         18
#define  R_SITUSZIPCODE                            19
#define  R_SITUSCITYNAME                           20
#define  R_ASSESSMENTDESCRIPTION                   21
#define  R_ISTAXABLE                               22
#define  R_TRA                                     23
#define  R_GEO                                     24
#define  R_RECORDERSTYPE                           25
#define  R_RECORDERSBOOK                           26
#define  R_RECORDERSPAGE                           27
//#define  R_COUNTYCODE                              28
#define  R_LOTTYPE                                 28
#define  R_LOT                                     29
#define  R_BLOCK                                   30
#define  R_PORTIONDIRECTION                        31
#define  R_SUBDIVISIONNAMEORTRACT                  32
#define  R_ACREAGEAMOUNT                           33
#define  R_FIRSTSECTION                            34
#define  R_FIRSTTOWNSHIP                           35
#define  R_FIRSTRANGE                              36
#define  R_FIRSTRANGEDIRECTION                     37
#define  R_LEGALPARTY1                             38
#define  R_LEGALPARTY2                             39
#define  R_LEGALPARTY3                             40
#define  R_LEGALPARTY4                             41
#define  R_LAND                                    42
#define  R_IMPR                                    43
#define  R_LIVINGIMPROVEMENTS                      44
#define  R_PERSONALVALUE                           45
#define  R_TRADEFIXTURESAMOUNT                     46
#define  R_PERSONALPROPERTYAPPRAISED               47
#define  R_TENPERCENTASSESSEDPENALTY               48
#define  R_HOX                                     49
#define  R_VETERANSEXEMPTION                       50
#define  R_DISABLEDVETERANSEXEMPTION               51
#define  R_CHURCHEXEMPTION                         52
#define  R_RELIGIOUSEXEMPTION                      53
#define  R_CEMETERYEXEMPTION                       54
#define  R_PUBLICSCHOOLEXEMPTION                   55
#define  R_PUBLICLIBRARYEXEMPTION                  56
#define  R_PUBLICMUSEUMEXEMPTION                   57
#define  R_WELFARECOLLEGEEXEMPTION                 58
#define  R_WELFAREHOSPITALEXEMPTION                59
#define  R_WELFAREPRIVATEPAROCHIALSCHOOLEXEMPTION  60
#define  R_WELFARECHARITYRELIGIOUSEXEMPTION        61
#define  R_OTHEREXEMPTION                          62
#define  R_CAMEFROM                                63
#define  R_COLS                                    64

// SalesDB.csv
#define  S_APN                                     0
#define  S_GEO                                     1
#define  S_CONVEYANCEMONTH                         2
#define  S_CONVEYANCEYEAR                          3
#define  S_CONVEYANCENUMBER                        4
#define  S_CONVEYANCEDATE                          5
#define  S_FORMSENTDATE                            6
#define  S_SALELETTERDATE                          7
#define  S_TRANSFERTYPECODE                        8
#define  S_INDICATEDSALEPRICE                      9
#define  S_BASEYEAR                                10
#define  S_BASEYEARFLAG                            11
#define  S_SITUSLINEONE                            12
#define  S_SITUSLINETWO                            13
#define  S_TOTALACRES                              14
#define  S_DESIGNTYPE101                           15
#define  S_CONSTRUCTIONTYPE                        16
#define  S_AREA_1_1                                17
#define  S_COSTAREA                                18
#define  S_TOTALAREA                               19
#define  S_EFFECTIVEAGE                            20
#define  S_CARPORT                                 21
#define  S_ATTGARAGE                               22
#define  S_DETGARAGE                               23
#define  S_POOLIND                                 24
#define  S_BEDROOMS                                25
#define  S_HALFBATHROOM                            26
#define  S_THREEQTRBATHROOM                        27
#define  S_FULLBATHROOM                            28
#define  S_TOTALBATHROOMS                          29
#define  S_SPECADJUSTMENT                          30
#define  S_LANDVALUE                               31
#define  S_STRUCTUREVALUE                          32
#define  S_TREEVINEVALUE                           33
#define  S_COLS                                    34

static XLAT_CODE  asRoofMat[] =
{
   // Value, lookup code, value length
   "TILE/SLATE",           "I", 2,      // Tile
   "ROCK/COMPOSITE",       "X", 2,      // Composition
   "SHAKE/WOOD SHINGLES",  "A", 2,      // Wood Shingle
   "OTHER",                "Z", 2,      // Others
   "MEMBRANE/ASPHALT ROLL","K", 2,      // Asphalt
   "CORRUGATED STEEL",     "S", 2,      // Steel
   "",   "",  0
};

// ExtendedRollTaxAmount.CSV
#define  RIV_ET_PIN                    0
#define  RIV_ET_GEO                    1
#define  RIV_ET_TAXAGENCYRATE          2
#define  RIV_ET_AGENCY01               3
#define  RIV_ET_AGENCYAMOUNT01         4
#define  RIV_ET_AGENCYRATE01           5
#define  RIV_ET_AGENCY02               6
#define  RIV_ET_AGENCYAMOUNT02         7
#define  RIV_ET_AGENCYRATE02           8
#define  RIV_ET_AGENCY03               9
#define  RIV_ET_AGENCYAMOUNT03         10
#define  RIV_ET_AGENCYRATE03           11
#define  RIV_ET_AGENCY04               12
#define  RIV_ET_AGENCYAMOUNT04         13
#define  RIV_ET_AGENCYRATE04           14
#define  RIV_ET_AGENCY05               15
#define  RIV_ET_AGENCYAMOUNT05         16
#define  RIV_ET_AGENCYRATE05           17
#define  RIV_ET_AGENCY06               18
#define  RIV_ET_AGENCYAMOUNT06         19
#define  RIV_ET_AGENCYRATE06           20
#define  RIV_ET_AGENCY07               21
#define  RIV_ET_AGENCYAMOUNT07         22
#define  RIV_ET_AGENCYRATE07           23
#define  RIV_ET_AGENCY08               24
#define  RIV_ET_AGENCYAMOUNT08         25
#define  RIV_ET_AGENCYRATE08           26
#define  RIV_ET_AGENCY09               27
#define  RIV_ET_AGENCYAMOUNT09         28
#define  RIV_ET_AGENCYRATE09           29
#define  RIV_ET_AGENCY10               30
#define  RIV_ET_AGENCYAMOUNT10         31
#define  RIV_ET_AGENCYRATE10           32
#define  RIV_ET_SPECIALTAXTYPE01       33
#define  RIV_ET_SPECAMOUNT01A          34
#define  RIV_ET_SPECAMOUNT01B          35
#define  RIV_ET_SPECIALTAXTYPE02       36
#define  RIV_ET_SPECAMOUNT02A          37
#define  RIV_ET_SPECAMOUNT02B          38
#define  RIV_ET_SPECIALTAXTYPE03       39
#define  RIV_ET_SPECAMOUNT03A          40
#define  RIV_ET_SPECAMOUNT03B          41
#define  RIV_ET_SPECIALTAXTYPE04       42
#define  RIV_ET_SPECAMOUNT04A          43
#define  RIV_ET_SPECAMOUNT04B          44
#define  RIV_ET_SPECIALTAXTYPE05       45
#define  RIV_ET_SPECAMOUNT05A          46
#define  RIV_ET_SPECAMOUNT05B          47
#define  RIV_ET_SPECIALTAXTYPE06       48
#define  RIV_ET_SPECAMOUNT06A          49
#define  RIV_ET_SPECAMOUNT06B          50
#define  RIV_ET_SPECIALTAXTYPE07       51
#define  RIV_ET_SPECAMOUNT07A          52
#define  RIV_ET_SPECAMOUNT07B          53
#define  RIV_ET_SPECIALTAXTYPE08       54
#define  RIV_ET_SPECAMOUNT08A          55
#define  RIV_ET_SPECAMOUNT08B          56
#define  RIV_ET_SPECIALTAXTYPE09       57
#define  RIV_ET_SPECAMOUNT09A          58
#define  RIV_ET_SPECAMOUNT09B          59
#define  RIV_ET_SPECIALTAXTYPE10       60
#define  RIV_ET_SPECAMOUNT10A          61
#define  RIV_ET_SPECAMOUNT10B          62
#define  RIV_ET_SPECIALTAXTYPE11       63
#define  RIV_ET_SPECAMOUNT11A          64
#define  RIV_ET_SPECAMOUNT11B          65
#define  RIV_ET_SPECIALTAXTYPE12       66
#define  RIV_ET_SPECAMOUNT12A          67
#define  RIV_ET_SPECAMOUNT12B          68
#define  RIV_ET_SPECIALTAXTYPE13       69
#define  RIV_ET_SPECAMOUNT13A          70
#define  RIV_ET_SPECAMOUNT13B          71
#define  RIV_ET_SPECIALTAXTYPE14       72
#define  RIV_ET_SPECAMOUNT14A          73
#define  RIV_ET_SPECAMOUNT14B          74
#define  RIV_ET_SPECIALTAXTYPE15       75
#define  RIV_ET_SPECAMOUNT15A          76
#define  RIV_ET_SPECAMOUNT15B          77
#define  RIV_ET_ADDITIONALASMNTFLAG    78
#define  RIV_ET_SUMMARYAMOUNT          79
#define  RIV_ET_COLS                   80

#define  RIV_ET_RTAXCOLS               10
#define  RIV_ET_STAXCOLS               15

// Paid/unpaid file SecuredPaidUnpaid.csv
#define  RIV_P_PIN                     0
#define  RIV_P_TAXAUTHORITY            1
#define  RIV_P_TRA                     2
#define  RIV_P_PAIDINDICATOR           3    // P=both paid, B=both unpaid, 1=1st inst paid, 2=2nd unpaid
#define  RIV_P_INSTALLMENT             4
#define  RIV_P_PENALITY                5
#define  RIV_P_COSTAMOUNT              6
#define  RIV_P_BILLNUMBER              7
#define  RIV_P_COLS                    8

// Delinquent file - SecuredPriorYearUnpaid.csv
#define  RIV_D_PIN                     0
#define  RIV_D_GEO                     1
#define  RIV_D_TAXRATEAREA             2
#define  RIV_D_BILLSTATUS              3
#define  RIV_D_MAILNAME                4
#define  RIV_D_MAILADDRESS             5
#define  RIV_D_MAILCITYSTATE           6
#define  RIV_D_MAILZIP                 7
#define  RIV_D_DESCRIPTION             8
#define  RIV_D_REDEMPTIONAMOUNT        9
#define  RIV_D_COLLECTIONDATE          10
#define  RIV_D_COLLECTIONNUMBER        11
#define  RIV_D_POWEROFSALEDATE         12
#define  RIV_D_CAMEFROMPIN1            13
#define  RIV_D_CAMEFROMPIN2            14
#define  RIV_D_CAMEFROMPIN3            15
#define  RIV_D_CAMEFROMPIN4            16
#define  RIV_D_WENTTOPIN1              17
#define  RIV_D_WENTTOPIN2              18
#define  RIV_D_WENTTOPIN3              19
#define  RIV_D_WENTTOPIN4              20
#define  RIV_D_DEFAULTNUMBER1          21
#define  RIV_D_DEFAULTNUMBER2          22
#define  RIV_D_DEFAULTNUMBER3          23
#define  RIV_D_IPPDATE                 24
#define  RIV_D_LAPSEDIPPCREDIT         25
#define  RIV_D_TAXYEAR                 26
#define  RIV_D_TAXABILITYCODE          27
#define  RIV_D_YEARPIN                 28
#define  RIV_D_ORIGINALPIN             29
#define  RIV_D_LEGALPARTY1             30
#define  RIV_D_BILLDATE                31
#define  RIV_D_INST1STATUS             32
#define  RIV_D_INST2STATUS             33
#define  RIV_D_INST1TAXES              34
#define  RIV_D_INST1PENALTY            35
#define  RIV_D_INST1COST               36
#define  RIV_D_INST1INTEREST           37
#define  RIV_D_INST1FEE                38
#define  RIV_D_INST2TAXES              39
#define  RIV_D_INST2PENALTY            40
#define  RIV_D_INST2COST               41
#define  RIV_D_INST2INTEREST           42
#define  RIV_D_INST2FEE                43
#define  RIV_D_RC_NUMBER1              44
#define  RIV_D_RC_COMPLETIONDATE1      45
#define  RIV_D_RC_RTC1                 46
#define  RIV_D_RC_LAND1                47
#define  RIV_D_RC_STRUCTURE1           48
#define  RIV_D_RC_TREEANDVINE1         49
#define  RIV_D_RC_NUMBER2              50
#define  RIV_D_RC_COMPLETIONDATE2      51
#define  RIV_D_RC_RTC2                 52
#define  RIV_D_RC_LAND2                53
#define  RIV_D_RC_STRUCTURE2           54
#define  RIV_D_RC_TREEANDVINE2         55
#define  RIV_D_RC_NUMBER3              56
#define  RIV_D_RC_COMPLETIONDATE3      57
#define  RIV_D_RC_RTC3                 58
#define  RIV_D_RC_LAND3                59
#define  RIV_D_RC_STRUCTURE3           60
#define  RIV_D_RC_TREEANDVINE3         61
#define  RIV_D_RC_NUMBER4              62
#define  RIV_D_RC_COMPLETIONDATE4      63
#define  RIV_D_RC_RTC4                 64
#define  RIV_D_RC_LAND4                65
#define  RIV_D_RC_STRUCTURE4           66
#define  RIV_D_RC_TREEANDVINE4         67
#define  RIV_D_BILLNUMBER              68
#define  RIV_D_COLS                    69

// processed Paid data for merge into Base
#define  RIV_PS_PIN                    0
#define  RIV_PS_TRA                    1
#define  RIV_PS_PAIDINDICATOR          2
#define  RIV_PS_INSTALLMENT            3
#define  RIV_PS_PENALITY               4
#define  RIV_PS_COSTAMOUNT             5
#define  RIV_PS_BILLNUMBER             6
#define  RIV_PS_PAIDDATE               7
#define  RIV_PS_PAIDAMOUNT             8
#define  RIV_PS_COLS                   9

// 03/11/2021
// TTC_CurrentAmounts_test.csv
#define  RIV_CA_PIN                    0
#define  RIV_CA_BILLNUMBER             1
#define  RIV_CA_TAXBILLID              2
#define  RIV_CA_INST1DUEDATE           3
#define  RIV_CA_INST2DUEDATE           4
#define  RIV_CA_INST1TAXCHARGE         5
#define  RIV_CA_INST1PENALTYCHARGE     6
#define  RIV_CA_INST1FEECHARGE         7
#define  RIV_CA_INST1INTERESTCHARGE    8
#define  RIV_CA_INST1TAXPAYMENT        9
#define  RIV_CA_INST1PENALTYPAYMENT    10
#define  RIV_CA_INST1FEEPAYMENT        11
#define  RIV_CA_INST1INTERESTPAYMENT   12
#define  RIV_CA_INST1AMOUNTDUE         13
#define  RIV_CA_INST2TAXCHARGE         14
#define  RIV_CA_INST2PENALTYCHARGE     15
#define  RIV_CA_INST2FEECHARGE         16
#define  RIV_CA_INST2INTERESTCHARGE    17
#define  RIV_CA_INST2TAXPAYMENT        18
#define  RIV_CA_INST2PENALTYPAYMENT    19
#define  RIV_CA_INST2FEEPAYMENT        20
#define  RIV_CA_INST2INTERESTPAYMENT   21
#define  RIV_CA_INST2AMOUNTDUE         22
#define  RIV_CA_FLDS                   23

// TTC_CurrentDistricts_test.csv
#define  RIV_CD_PIN                    0
#define  RIV_CD_TAXRATEAREA            1
#define  RIV_CD_TAXYEAR                2
#define  RIV_CD_BILLNUMBER             3
#define  RIV_CD_BILLTYPE               4
#define  RIV_CD_TAXBILLID              5
#define  RIV_CD_DETAILS                6
#define  RIV_CD_TAXCODE                7
#define  RIV_CD_RATE                   8
#define  RIV_CD_TOTAL                  9
#define  RIV_CD_FLDS                   10

// TTC_PriorYear_test.csv
#define  RIV_PY_PIN                    0
#define  RIV_PY_DEFAULTNUMBER          1
#define  RIV_PY_DEFAULTYEAR            2
#define  RIV_PY_BILLTYPE               3
#define  RIV_PY_AMOUNTDUE              4
#define  RIV_PY_FLDS                   5

// EqualizedRoll.csv
#define  RIV_ER_AGGREGATECALCID                    0
#define  RIV_ER_AGGREGATECALCDESCRIPTION           1              
#define  RIV_ER_TAXYEAR                            2
#define  RIV_ER_PIN                                3
#define  RIV_ER_ROLLTYPESHORTDESCRIPTION           4
#define  RIV_ER_ROLLTYPEDESCRIPTION                5
#define  RIV_ER_ASMTTYPESHORTDESCRIPTION           6
#define  RIV_ER_ASMTTYPEDESCRIPTION                7
#define  RIV_ER_CLASSCATEGORYSHORTDESCRIPTION      8
#define  RIV_ER_CLASSCATEGORYDESCRIPTION           9
#define  RIV_ER_CLASSSUBCATEGORYSHORTDESCRIPTION   10
#define  RIV_ER_CLASSSUBCATEGORYDESCRIPTION        11
#define  RIV_ER_TAGSHORTDESCRIPTION                12
#define  RIV_ER_TAGDESCRIPTION                     13
#define  RIV_ER_PRIMARYTAG                         14
#define  RIV_ER_INCORPORATION                      15
#define  RIV_ER_TAGAREASHORTDESCRIPTION            16
#define  RIV_ER_TAGAREADESCRIPTION                 17
#define  RIV_ER_ASSESSEDLANDAMOUNT                 18
#define  RIV_ER_ASSESSEDIMPROVEMENTAMOUNT          19
#define  RIV_ER_ASSESSEDTRADEFIXTURESAMOUNT        20
#define  RIV_ER_ASSESSEDPERSONALAMOUNT             21
#define  RIV_ER_ASSESSEDLIVINGIMPROVEMENTAMOUNT    22
#define  RIV_ER_ASSESSEDTREESANDVINESAMOUNT        23
#define  RIV_ER_ASSESSEDNCAIRAMOUNT                24
#define  RIV_ER_PENALTYAMOUNT                      25
#define  RIV_ER_PENALTYCODE                        26
#define  RIV_ER_HOEAMOUNT                          27
#define  RIV_ER_LOWVALUEAMOUNT                     28
#define  RIV_ER_OTHEREXEMPTIONAMOUNT               29
#define  RIV_ER_TAXABLELANDAMOUNT                  30
#define  RIV_ER_TAXABLEIMPROVEMENTAMOUNT           31
#define  RIV_ER_TAXABLELIVINGIMPROVEMENTAMOUNT     32
#define  RIV_ER_TAXABLETRADEFIXTURESAMOUNT         33
#define  RIV_ER_TAXABLEPERSONALAMOUNT              34
#define  RIV_ER_NETADJTAXABLEAMOUNT                35
#define  RIV_ER_NETTAXABLEAMOUNT                   36

// GrGr - 02/03/2023
#define RIV_GR_RECDATE        0
#define RIV_GR_GRANTOR        1
#define RIV_GR_GRANTEE        2
#define RIV_GR_DOCTYPE        3
#define RIV_GR_DOCTITLE       4
#define RIV_GR_DOCNUM         5
#define RIV_GR_NUMPAGES       6
#define RIV_GR_APN            7
#define RIV_GR_FLDS           8

// GrGr - 10/19/2022
//#define RIV_GR_RECDATE        0
//#define RIV_GR_GRANTOR        1
//#define RIV_GR_GRANTEE        2
//#define RIV_GR_DOCCODE        3
//#define RIV_GR_CNTYDOCNUM     4
//#define RIV_GR_DOCNUM         5
//#define RIV_GR_NUMPAGES       6
//#define RIV_GR_APN            7
//#define RIV_GR_FLDS           8

// Standard codes
// H=Homeowners
// D=Disabled Veterans
// V=Veterans
// C=Church
// L=Library
// M=Museums
// R=Religion
// T=Tribal Housing
// W=Welfare
// U=College
// P=Public School
// E=Cemetery
// G=Government
// I=Hospital
// S=School
// X=Misc exempt
// Y=Military

IDX_TBL4 RIV_Exemption[] = 
{
   "HO","H", 2,1,    // Homeowners Exemption
   "VE","V", 2,1,    // Veterans
   "DV","D", 2,1,    // Disabled Veterans Exemption
   "CH","C", 2,1,    // Church Exemption
   "RE","R", 2,1,    // Religious Exemption
   "CE","E", 2,1,    // Cemetery Exemption
   "PS","P", 2,1,    // Public School Exemption
   "PL","L", 2,1,    // Public Library Exemption
   "PM","M", 2,1,    // Public Museum Exemption
   "WC","U", 2,1,    // WELFARE COLLEGE
   "WH","I", 2,1,    // WELFARE HOSPITAL
   "WS","S", 2,1,    // WELFARE PRIVATE PAROCHIAL SCHOOL
   "WR","W", 2,1,    // WELFARE CHARITY RELIGIOUS
   "OE","X", 2,1,    // Other
   "","",0,0
};

#endif