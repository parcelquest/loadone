#ifndef  _SENDMAIL_H
#define  _SENDMAIL_H

int   mySendMail(LPCSTR pConfig, LPSTR pSubj, LPSTR pBody, LPSTR pMailTo);
int   initMail(char *pConfig, bool bDebug);
int   sendMail(LPSTR pSubj, LPSTR pBody, LPSTR pMailTo, LPSTR pMailCc=NULL, LPSTR pMailBcc=NULL);
void  closeMail();
int   TestMail(LPSTR pCfgFile);

#endif