#ifndef  _MERGEEDX_H_
#define  _MERGEEDX_H_

#define  ROFF_RECORD_CODE             1
#define  ROFF_APN                     2
#define  ROFF_TRA                    13
#define  ROFF_STATUS                 19
#define  ROFF_AGENCY                 21
#define  ROFF_PRI_CLUSTCD            23
#define  ROFF_SEC_CLUSTCD            25
#define  ROFF_PRI_USECODE            27
#define  ROFF_SEC_USECODE            29
#define  ROFF_DATE_ADDED             31
#define  ROFF_DIRECT_BILL_FLAG1      39
#define  ROFF_ACRES                  40
#define  ROFF_CURR_RECI              48
#define  ROFF_CURR_RECD              55
#define  ROFF_1ST_PRIOR_RECI         63
#define  ROFF_1ST_PRIOR_RECD         70
#define  ROFF_2ND_PRIOR_RECI         78
#define  ROFF_2ND_PRIOR_RECD         85
#define  ROFF_NAME1                  93
#define  ROFF_ETAL                  119
#define  ROFF_NAME2                 123
#define  ROFF_NAME_CD               152
#define  ROFF_MAIL STREET           153
#define  ROFF_ZIP                   183
#define  ROFF_ZIP4                  188
#define  ROFF_ZIP_SUFFIX            192
#define  ROFF_S_STRNUM              193
#define  ROFF_S_STRNAME             199
#define  ROFF_S_UNIT_TYPE           224
#define  ROFF_S_UNIT_NUMBER         229
#define  ROFF_S_DIR                 233   // 232
#define  ROFF_S_STRTYPE             235   // 234
#define  ROFF_S_STRFRA              237   // 237
#define  ROFF_S_AREA                240   // 239
#define  ROFF_LEAD_LINE             242
#define  ROFF_SUBDIV_NUMBER         258
#define  ROFF_ZONING                264
#define  ROFF_ZONE_CHNG_DT          274
#define  ROFF_SEC_ZONE_CD           282
#define  ROFF_1ST_HIST_CENSUS_TR    289
#define  ROFF_1ST_HIST_DISTRICT     292
#define  ROFF_1ST_HIST_PRI_ZONE_CD  295
#define  ROFF_1ST_HIST_SEC_ZONE_CD  299
#define  ROFF_CREATE_DATE           303
#define  ROFF_CREATE_CD             311
#define  ROFF_LAND                  312
#define  ROFF_TIMBER                321
#define  ROFF_GRAZING_MINERAL_RTS   330
#define  ROFF_STRUCTURES            339
#define  ROFF_FIXED_EQUIP           348
#define  ROFF_PLANTS                357
#define  ROFF_OTHER_IMPROV          366
#define  ROFF_BUS_INVEN             375
#define  ROFF_PEN_CD1               384
#define  ROFF_OTH_PERS_PROP         387
#define  ROFF_PEN_CD2               396
#define  ROFF_VALUE_CHNG_DT         399
#define  ROFF_APPRAISAL_DATE        407
#define  ROFF_APPRAISER_INITL       415
#define  ROFF_CAAP_CODE             418
#define  ROFF_FILLER1               420
#define  ROFF_GENL_PLAN_CD          423
#define  ROFF_DTT_AMT               424
#define  ROFF_DTT_CD                433
#define  ROFF_REAPPRAISAL_CD        434
#define  ROFF_1911_BOND             436
#define  ROFF_1915_BOND             437
#define  ROFF_OTHER_BOND            438
#define  ROFF_BLDG_PERMIT_FLAG      439
#define  ROFF_SOLD2ST_FLAG          440
#define  ROFF_MOBILEHOME_FLAG       441
#define  ROFF_ETAL_FLAG             442
#define  ROFF_BUS_FORM_TYPE         443
#define  ROFF_DIRECT_BILL_FLAG2     444
#define  ROFF_FILLER2               445
#define  ROFF_ROLL_YEAR             451
#define  ROFF_EXEMP1_CD             455
#define  ROFF_EXEMP1_AMT            457
#define  ROFF_EXEMP1_YR             466
#define  ROFF_EXEMP1_SSN1           468
#define  ROFF_EXEMP1_SSN2           477
#define  ROFF_EXEMP2_CD             486
#define  ROFF_EXEMP2_AMT            488
#define  ROFF_EXEMP2_YR             497
#define  ROFF_EXEMP2_SSN1           499
#define  ROFF_EXEMP2_SSN2           508
#define  ROFF_EXEMP3_CD             517
#define  ROFF_EXEMP3_AMT            519
#define  ROFF_EXEMP3_YR             528
#define  ROFF_EXEMP3_SSN1           530
#define  ROFF_EXEMP3_SSN2           539

#define  RSIZ_RECORD_CODE           1 
#define  RSIZ_APN                   11
#define  RSIZ_TRA                   6 
#define  RSIZ_STATUS                2 
#define  RSIZ_AGENCY                2 
#define  RSIZ_PRI_CLUSTCD           2 
#define  RSIZ_SEC_CLUSTCD           2 
#define  RSIZ_PRI_USECODE           2 
#define  RSIZ_SEC_USECODE           2 
#define  RSIZ_DATE_ADDED            8 
#define  RSIZ_DIRECT_BILL_FLAG      1 
#define  RSIZ_ACRES                 8 
#define  RSIZ_CURR_RECI             7 
#define  RSIZ_CURR_RECD             8 
#define  RSIZ_1ST_PRIOR_RECI        7 
#define  RSIZ_1ST_PRIOR_RECD        8 
#define  RSIZ_2ND_PRIOR_RECI        7 
#define  RSIZ_2ND_PRIOR_RECD        8 
#define  RSIZ_NAME1                 26
#define  RSIZ_ETAL                  4 
#define  RSIZ_NAME2                 29
#define  RSIZ_NAME_CD               1 
#define  RSIZ_M_STREET              30
#define  RSIZ_M_ZIP                 5 
#define  RSIZ_M_ZIP4                4 
#define  RSIZ_M_ZIP_SFX             1 
#define  RSIZ_S_STRNUM              6 
#define  RSIZ_S_STRNAME             25
#define  RSIZ_S_UNIT_TYPE           5 
#define  RSIZ_S_UNIT_NUMBER         4     // 3
#define  RSIZ_S_DIR                 2 
#define  RSIZ_S_STRTYPE             2 
#define  RSIZ_S_STRFRA              3 
#define  RSIZ_S_AREA                2     // 3 
#define  RSIZ_LEAD_LINE             16
#define  RSIZ_SUBDIV_NUMBER         6 
#define  RSIZ_ZONING                10
#define  RSIZ_ZONE_CHNG_DT          8 
#define  RSIZ_SEC_ZONE_CD           7 
#define  RSIZ_1ST_HIST_CENSUS_TR    3 
#define  RSIZ_1ST_HIST_DISTRICT     3 
#define  RSIZ_1ST_HIST_PRI_ZONE_CD  4 
#define  RSIZ_1ST_HIST_SEC_ZONE_CD  4 
#define  RSIZ_CREATE_DATE           8 
#define  RSIZ_CREATE_CD             1 
#define  RSIZ_LAND                  9 
#define  RSIZ_TIMBER                9 
#define  RSIZ_GRAZING_MINERAL_RTS   9 
#define  RSIZ_STRUCTURES            9 
#define  RSIZ_FIXED_EQUIP           9 
#define  RSIZ_PLANTS                9 
#define  RSIZ_OTHER_IMPROV          9 
#define  RSIZ_BUS_INVEN             9 
#define  RSIZ_PEN_CD1               3 
#define  RSIZ_OTH_PERS_PROP         9 
#define  RSIZ_PEN_CD2               3 
#define  RSIZ_VALUE_CHNG_DT         8 
#define  RSIZ_APPRAISAL_DATE        8 
#define  RSIZ_APPRAISER_INITL       3 
#define  RSIZ_CAAP_CODE             2 
#define  RSIZ_FILLER1               3 
#define  RSIZ_GENL_PLAN_CD          1 
#define  RSIZ_DTT_AMT               9 
#define  RSIZ_DTT_CD                1 
#define  RSIZ_REAPPRAISAL_CD        2 
#define  RSIZ_1911_BOND             1 
#define  RSIZ_1915_BOND             1 
#define  RSIZ_OTHER_BOND            1 
#define  RSIZ_BLDG_PERMIT_FLAG      1 
#define  RSIZ_SOLD2ST_FLAG          1 
#define  RSIZ_MOBILEHOME_FLAG       1 
#define  RSIZ_ETAL_FLAG             1 
#define  RSIZ_BUS_FORM_TYPE         1 
#define  RSIZ_DIRECT_BILL_FLAG      1 
#define  RSIZ_FILLER2               6 
#define  RSIZ_ROLL_YEAR             4 
#define  RSIZ_EXEMP_CD              2 
#define  RSIZ_EXEMP_AMT             9 
#define  RSIZ_EXEMP_YR              2 
#define  RSIZ_EXEMP_SSN             18 

typedef struct _tEdxRollRec
{
   char  RecordCode;
   char  Apn[RSIZ_APN];
   char  TRA[RSIZ_TRA];
   char  Status[RSIZ_STATUS];
   char  Agency[RSIZ_STATUS];
   char  Pri_ClustCode[RSIZ_PRI_CLUSTCD];
   char  Sec_ClustCode[RSIZ_SEC_CLUSTCD];
   char  Pri_UseCode[RSIZ_PRI_USECODE];
   char  Sec_UseCode[RSIZ_SEC_USECODE];
   char  DateAdded[RSIZ_DATE_ADDED];
   char  DirectBillFlg;
   char  Acres[RSIZ_ACRES];
   char  Cur_DocNum[RSIZ_CURR_RECI];         // 48
   char  Cur_DocDate[RSIZ_CURR_RECD];        // 55
   char  P1_DocNum[RSIZ_CURR_RECI];
   char  P1_DocDate[RSIZ_CURR_RECD];
   char  P2_DocNum[RSIZ_CURR_RECI];
   char  P2_DocDate[RSIZ_CURR_RECD];
   char  Name1[RSIZ_NAME1];
   char  Etal[RSIZ_ETAL];
   char  Name2[RSIZ_NAME2];
   char  NameCode;                           // A,C,R,...
   char  M_Addr1[RSIZ_M_STREET];
   char  M_Zip[RSIZ_M_ZIP];
   char  M_Zip4[RSIZ_M_ZIP4];
   char  M_ZipSfx;
   char  S_StrNum[RSIZ_S_STRNUM];
   char  S_StrName[RSIZ_S_STRNAME]; // May contain dir, name, sfx, unit#
   char  S_UnitType[RSIZ_S_UNIT_TYPE];
   char  S_UnitNum[RSIZ_S_UNIT_NUMBER];
   char  S_Dir[RSIZ_S_DIR];
   char  S_StrSfx[RSIZ_S_STRTYPE];
   char  S_StrFra[RSIZ_S_STRFRA];
   char  S_Area[RSIZ_S_AREA];
   char  LeadLine[RSIZ_LEAD_LINE];
   char  SubdivNum[RSIZ_SUBDIV_NUMBER];
   char  Zoning[RSIZ_ZONING];
   char  Zone_ChgDate[RSIZ_ZONE_CHNG_DT];
   char  Sec_ZoneCode[RSIZ_SEC_ZONE_CD];
   char  First_Hist_CencusTr[RSIZ_1ST_HIST_CENSUS_TR];
   char  First_Hist_District[RSIZ_1ST_HIST_DISTRICT];
   char  First_Hist_Pri_ZoneCode[RSIZ_1ST_HIST_PRI_ZONE_CD];     // deed type or vesting
   char  First_Hist_Sec_ZoneCode[RSIZ_1ST_HIST_SEC_ZONE_CD];
   char  CreateDate[RSIZ_CREATE_DATE];
   char  CreateCode;
   char  Land[RSIZ_LAND];
   char  Timber[RSIZ_TIMBER];
   char  Mineral[RSIZ_GRAZING_MINERAL_RTS];
   char  Structure[RSIZ_STRUCTURES];
   char  Fixt_Val[RSIZ_FIXED_EQUIP];          // Fixed machinery & equiptment exempt
   char  Plants[RSIZ_PLANTS];
   char  OtherImpr[RSIZ_OTHER_IMPROV];
   char  Bus_Inv[RSIZ_BUS_INVEN];
   char  Pen_Code1[RSIZ_PEN_CD1];
   char  Other_PP_Val[RSIZ_OTH_PERS_PROP];
   char  Pen_Code2[RSIZ_PEN_CD2];
   char  Value_ChgDate[RSIZ_VALUE_CHNG_DT];
   char  Appr_Date[RSIZ_APPRAISAL_DATE];
   char  filler1[9];
   char  DocTax[RSIZ_DTT_AMT];
   char  DocTax_Code;
   char  Reappr_Code[RSIZ_REAPPRAISAL_CD];
   char  Bond_1911;
   char  Bond_1915;
   char  Bond_Other;
   char  Bldg_Permit_Flag;
   char  Sold2St_Flag;
   char  MH_Flag;
   char  Etal_Flag;
   char  Bus_Form_Type;
   char  filler3[7];
   char  RollYear[RSIZ_ROLL_YEAR];
   char  Exe1_Code[RSIZ_EXEMP_CD];
   char  Exe1_Amt[RSIZ_EXEMP_AMT];
   char  Exe1_Yr[RSIZ_EXEMP_YR];
   char  Exe1_Ssn[RSIZ_EXEMP_SSN];
   char  Exe2_Code[RSIZ_EXEMP_CD];
   char  Exe2_Amt[RSIZ_EXEMP_AMT];
   char  Exe2_Yr[RSIZ_EXEMP_YR];
   char  Exe2_Ssn[RSIZ_EXEMP_SSN];
   char  Exe3_Code[RSIZ_EXEMP_CD];
   char  Exe3_Amt[RSIZ_EXEMP_AMT];
   char  Exe3_Yr[RSIZ_EXEMP_YR];
   char  Exe3_Ssn[RSIZ_EXEMP_SSN];
} EDX_ROLL;

#define  COFF_APN                   1 
#define  COFF_QUAL_CLASS            12
#define  COFF_STORIES               15
#define  COFF_BEDROOMS              17
#define  COFF_BATHS                 21
#define  COFF_ROOMS                 26
#define  COFF_LIVAREA               30
#define  COFF_UNITS                 38
#define  COFF_YRBLT                 42
#define  COFF_EFFYR                 46
#define  COFF_ABNORMAL_DISP         50
#define  COFF_CONDITION             51
#define  COFF_FUNCTIONAL_PLAN       52
#define  COFF_USECODE               53
#define  COFF_LOT_SQFT              54
#define  COFF_WATER_SOURCE          61
#define  COFF_TYPE_ACCESS           62
#define  COFF_TOPOGRAPHY            63
#define  COFF_GROUND_COVER          64
#define  COFF_WATERFRONT            65
#define  COFF_VIEW                  66
#define  COFF_NATURAL_GAS           67
#define  COFF_SEWER                 68
#define  COFF_PARTIAL_VALUE         69

#define  CSIZ_APN                   11
#define  CSIZ_QUAL_CLASS            3 
#define  CSIZ_STORIES               2 
#define  CSIZ_BEDROOMS              4 
#define  CSIZ_BATHS                 5 
#define  CSIZ_ROOMS                 4 
#define  CSIZ_LIVAREA               8 
#define  CSIZ_UNITS                 4 
#define  CSIZ_YRBLT                 4 
#define  CSIZ_EFFYR                 4 
#define  CSIZ_ABNORMAL_DISP         1 
#define  CSIZ_CONDITION             1 
#define  CSIZ_FUNCTIONAL_PLAN       1 
#define  CSIZ_USECODE               1 
#define  CSIZ_LOT_SQFT              7 
#define  CSIZ_WATER_SOURCE          1 
#define  CSIZ_TYPE_ACCESS           1 
#define  CSIZ_TOPOGRAPHY            1 
#define  CSIZ_GROUND_COVER          1 
#define  CSIZ_WATERFRONT            1 
#define  CSIZ_VIEW                  1 
#define  CSIZ_NATURAL_GAS           1 
#define  CSIZ_SEWER                 1 
#define  CSIZ_PARTIAL_VALUE         2 

typedef struct _tEdxCharRec
{
   char  Apn[CSIZ_APN];
   char  QualityClass[CSIZ_QUAL_CLASS];
   char  Num_Stories[CSIZ_STORIES];
   char  NumBedrooms[CSIZ_BEDROOMS];
   char  NumBaths[CSIZ_BATHS];
   char  Total_Rooms[CSIZ_ROOMS];
   char  LivArea[CSIZ_LIVAREA];
   char  Units[CSIZ_UNITS];
   char  YearBuilt[CSIZ_YRBLT];   
   char  EffYear[CSIZ_EFFYR];     
   char  Abnorm_Disp;
   char  Imprv_Condition_Code;
   char  Functional_Plan;
   char  UseCode[CSIZ_USECODE];
   char  Lot_Sqft[CSIZ_LOT_SQFT];
   char  WaterSource;
   char  TypeAccess;
   char  Topography;
   char  GroundCover;
   char  WaterFront;
   char  View;
   char  NaturalGas;
   char  Sewer;
   char  PartialValue[CSIZ_PARTIAL_VALUE];
} EDX_CHAR;


#define SSIZ_APN                 13
#define SSIZ_S_ZIP               5
#define SSIZ_SALE_PRICE          10
#define SSIZ_REC_DATE            6
#define SSIZ_REC_PAGE            4
#define SSIZ_RELIABILITY_CODE    2
#define SSIZ_SALE_TYPE           4
#define SSIZ_NUM_PARCELS         3
#define SSIZ_DEED_DATE           6
#define SSIZ_PARTIAL_INT         3
#define SSIZ_DEED_TYPE           4
#define SSIZ_STAMP_AMOUNT        8
#define SSIZ_STAMP_CODE          1
#define SSIZ_GRANTOR             24
#define SSIZ_GRANTEE             24

#define SSIZ_DATE                10
#define SSIZ_SEQ                 1
#define SSIZ_DOCNUM              7
#define SSIZ_DOCTAX              10
#define SSIZ_PCTXFER             6
#define SSIZ_APPR                3
#define SSIZ_USECODE             2
#define SSIZ_CHARS               88

typedef struct _tSaleLine1
{
   char  filler1;
   char  Apn[SSIZ_APN];          // 2
   char  filler2;
   char  EffDate[SSIZ_DATE];     // 16
   char  filler3;
   char  Seq[SSIZ_SEQ];
   char  filler4;
   char  DocDate[SSIZ_DATE];     // 29
   char  filler5;
   char  Type;
   char  DocNum[SSIZ_DOCNUM];    // 40
   char  filler6[50];   
   char  SalePrice[SSIZ_SALE_PRICE];
   char  AdjSalePrice[SSIZ_SALE_PRICE];
   char  RJ_Code;                // 118 - See table
   char  filler7[2];
   char  Conf_Level;             // 121 - C or blank
   char  filler8;
   char  Appr_Code;              // Y|N|Q
   char  filler9[2];
   char  PctXfer[SSIZ_PCTXFER];  // 126 - 9(3)V99
   char  CrLf[2];
} EDX_SALE1;

typedef struct _tSaleLine2
{
   char  filler1[4];
   char  UseCode[SSIZ_USECODE];
   char  filler2[4];
   char  Acres[9];               // 11
   char  filler3;
   char  Chars[SSIZ_CHARS];      // 21 - V Q CLAS YEAR  BED BATHRM GAR-ST FP GAR-COV SWIM.P-A STORY SQFT-IMP PRICE/SQFT
   char  DocTax[10];             // 109
   char  filler4[15];   
   char  CrLf[2];
} EDX_SALE2;

typedef struct t_EdxCumSale
{
   char  Apn[RSIZ_APN];
   char  ActualSalePrice[SSIZ_SALE_PRICE];
   char  RecDate[8];                             // CCYYMMDD
   char  RecPage[SSIZ_REC_PAGE];
   char  ReliabilityCode[SSIZ_RELIABILITY_CODE];
   char  SaleType[SSIZ_SALE_TYPE];
   char  AdjSalePrice[SSIZ_SALE_PRICE];
   char  NumParcels[SSIZ_NUM_PARCELS];
   char  DeedDate[8];
   char  PctTransfer[SSIZ_PARTIAL_INT];
   char  DeedType[SSIZ_DEED_TYPE];
   char  StampAmt[9];
   char  StampCode;
   char  Grantor[SSIZ_GRANTOR];
   char  Grantee[SSIZ_GRANTOR];
   char  SalePrice[SSIZ_SALE_PRICE];
   char  S_Zip[SSIZ_S_ZIP];
   char  filler;
} EDX_CSAL;

USEXREF  Edx_UseTbl[] =
{
   "00","120",              //VACANT-RESIDENTIAL TO 2.5 AC
   "01","115",              //MOBILEHOME ON UP TO 2.5 ACS.
   "02","100",              //NON-RES IMPRVMNT TO 2.5 ACS.
   "05","120",              //VACANT - MULTI-RESIDENTIAL  
   "11","100",              //RESIDENTIAL IMPRVD TO 2.5 AC
   "12","100",              //MULTI-RESID. 2-3 UNITS      
   "13","100",              //MULTI-RESID. 4 PLUS UNITS   
   "14","100",              //CONDMINIUMS/TOWNHOUSES     '
   "15","813",              //POSS. INT.-FOREST SER. CABIN
   "16","115",              //MOBILEHOMES                 
   "17","791",              //OPEN SPACE CONTRACTS        
   "21","175",              //RURAL VACANT 2.5-20 ACRES   
   "22","175",              //RURAL IMPRVD 2.5-20 ACRES   
   "23","175",              //RURAL SUB-ECONOMIC UNIT     
   "24","175",              //RURAL ECONOMIC UNIT 20+ACRES
   "25","175",              //RURAL RESTRICTED - CLCA     
   "26","175",              //RURAL RESTRICTED - CLCA     
   "28","115",              //MOBILEHOME - 2.5+ ACRES     
   "29","100",              //NON-RES IMPRVMNT -2.5+ ACRES
   "30","835",              //COMMERCIAL VACANT           
   "31","202",              //COMMERCIAL -MINOR IMPRVMNT  
   "32","219",              //COMMERCIAL -MAJOR IMPRVMNT  
   "33","110",              //MOTELS                      
   "34","234",              //SERVICE STATIONS            
   "35","114",              //MOBILEHOME PARKS            
   "40","190",              //INDUSTRIAL VACANT           
   "41","400",              //INDUSTRIAL IMPROVED         
   "50","535",              //TIMBER PRESERVE ZONES       
   "51","536",              //TIMBER RIGHTS               
   "60","600",              //RECREATIONAL PROPERTIES     
   "70","783",              //MINERAL RIGHTS              
   "75","801",              //GRAZING RIGHTS              
   "80","100",              //RESIDENTIAL - TIMESHARE     
   "", ""
};

#define  TSIZ_APN                   11
#define  TSIZ_APN_D                 14
#define  TSIZ_AGENCY                33
#define  TSIZ_TAXCODE               4
#define  TSIZ_EXECODE               26
#define  TSIZ_TAXRATE               10
#define  TSIZ_AMOUNT                9
#define  TSIZ_TAXYEAR               4
#define  TSIZ_DATE                  8
#define  TSIZ_PHONE                 14
#define  TSIZ_BILLNUM               7
#define  TSIZ_DEFAULTNO             7
#define  TSIZ_OWNERNAME             30

// Secured Roll Layout
#define  TOFF_PARCEL_NUMBER               1
#define  TOFF_BILL_NUMBER                15
#define  TOFF_LIENNAME1                  23
#define  TOFF_LIENNAME2                  56
#define  TOFF_CURNAME1                   85
#define  TOFF_CURNAME2                  116
#define  TOFF_STREET                    146
#define  TOFF_CITY                      177
#define  TOFF_STATE                     202
#define  TOFF_COUNTRY                   204
#define  TOFF_ZIP_CODE                  224
#define  TOFF_CORTAC_NUMBER             234
#define  TOFF_TAX_RATE_AREA             246
#define  TOFF_PROPERTY_DESCRIPTION      253
#define  TOFF_SEC_DEF_BILL_NUMBER       283
#define  TOFF_SEC_DEF_TAX_YEAR          290
#define  TOFF_UNSEC_DEF_YEAR            294
#define  TOFF_UNSEC_DEF_BILL_NUMBER     298
#define  TOFF_DELQDATE1                 305
#define  TOFF_INSTAMT1                  315
#define  TOFF_PENAMT1                   324
#define  TOFF_DELQAMT1                  333
#define  TOFF_OCRA_SCAN_LINE_INST1      342
#define  TOFF_DELQDATE2                 400
#define  TOFF_INSTAMT2                  410
#define  TOFF_PENAMT2                   419
#define  TOFF_COST                      428
#define  TOFF_DELQAMT2                  437
#define  TOFF_OCRA_SCAN_LINE_INST2      446
#define  TOFF_VALUEBEFOREEXE            504
#define  TOFF_TAX_RATE                  513
#define  TOFF_GROSS_TAX                 519
#define  TOFF_TOTAL_EXE                 528
#define  TOFF_NET_TAX                   537     // Total tax less direct charge
#define  TOFF_DIRECT_ASSESSMENTS        546
#define  TOFF_TOTAL_TAXDUE              555
#define  TOFF_LAND                      564
#define  TOFF_IMPROVEMENTS              573
#define  TOFF_TOTAL_VALUE               582
#define  TOFF_PERSONAL_PROPERTY         591
#define  TOFF_EXEAMT1                   600
#define  TOFF_EXECODE1                  609
#define  TOFF_EXEAMT2                   611
#define  TOFF_EXECODE2                  620
#define  TOFF_EXEAMT3                   622
#define  TOFF_EXECODE3                  631
#define  TOFF_NET_VALUE                 633
#define  TOFF_AGENCY                    642
#define  TOFF_PHONE                     675
#define  TOFF_AMOUNT                    689
#define  TOFF_DUE_DATE1                2322
#define  TOFF_DUE_DATE2                2338
#define  TOFF_SITUS                    2354
#define  TOFF_SORT_TYPE_KEY            2384
#define  TOFF_SORT_ZIP_KEY             2386

#define  TSIZ_PARCEL_NUMBER              14
#define  TSIZ_BILL_NUMBER                 6
#define  TSIZ_STREET                     31
#define  TSIZ_CITY                       25
#define  TSIZ_STATE                       2
#define  TSIZ_COUNTRY                    20
#define  TSIZ_ZIP_CODE                   10
#define  TSIZ_CORTAC_NUMBER              12
#define  TSIZ_TAX_RATE_AREA               7
#define  TSIZ_PROPERTY_DESCRIPTION       30
#define  TSIZ_SEC_DEF_BILL_NUMBER         7
#define  TSIZ_SEC_DEF_TAX_YEAR            4
#define  TSIZ_UNSEC_DEF_YEAR              4
#define  TSIZ_UNSEC_DEF_BILL_NUMBER       7
#define  TSIZ_DELQDATE1                  10
#define  TSIZ_INSTAMT1                    9
#define  TSIZ_PENAMT1                     9
#define  TSIZ_DELQAMT1                    9
#define  TSIZ_OCRA_SCAN_LINE_INST1       58
#define  TSIZ_COST                        9
#define  TSIZ_VALUEBEFOREEXE              9
#define  TSIZ_TAX_RATE                    6
#define  TSIZ_GROSS_TAX                   9
#define  TSIZ_TOTAL_EXE                   9
#define  TSIZ_NET_TAX                     9
#define  TSIZ_DIRECT_ASSESSMENTS          9
#define  TSIZ_TOTAL_TAXDUE                9
#define  TSIZ_LAND                        9
#define  TSIZ_IMPROVEMENTS                9
#define  TSIZ_TOTAL_VALUE                 9
#define  TSIZ_PERSONAL_PROPERTY           9
#define  TSIZ_EXEAMT1                     9
#define  TSIZ_EXECODE1                    2
#define  TSIZ_EXEAMT2                     9
#define  TSIZ_EXECODE2                    2
#define  TSIZ_EXEAMT3                     9
#define  TSIZ_EXECODE3                    2
#define  TSIZ_NET_VALUE                   9
#define  TSIZ_AGENCY                     33
#define  TSIZ_PHONE                      14
#define  TSIZ_AMOUNT                      9
#define  TSIZ_DUE_DATE1                  16
#define  TSIZ_DUE_DATE2                  16
#define  TSIZ_SITUS                      30
#define  TSIZ_SORT_TYPE_KEY               2
#define  TSIZ_SORT_ZIP_KEY               10
#define  EDX_MAX_AGENCY                  30

// Tax Bill layout
typedef struct _tEdxInst
{
   char  Status;                    // A=active; P=paid; D=deleted
   char  DeleteDate[TSIZ_DATE];
   char  PaidDate[TSIZ_DATE];       // YYYYMMDD
   char  TaxAmt[TSIZ_AMOUNT];       // No Penalty
   char  DelqDate[TSIZ_DATE];
   char  DueAmt[TSIZ_AMOUNT];       // With penalty
} EDX_INST;

typedef struct _tEdxTaxBill
{  // 186-bytes
   char  PropType;                  // 1=Unsecured 2=secured
   char  Apn[TSIZ_APN];
   char  RecType;                   // 2=secured; 3=supplmental; 4=secured abstract (no $ amounts, more info in SA_AR.TXT)
   char  DefaultNo[TSIZ_DEFAULTNO]; // Used for type 4 only, should match with SOLD4TAX & SA_AR
   char  TaxYear[TSIZ_TAXYEAR];
   char  BillNum[TSIZ_BILLNUM];     // Last digit is an alpha. "S" for supplemental bills, blank for all others
   EDX_INST Inst1;
   EDX_INST Inst2;
   char  OrgBillNum[TSIZ_BILLNUM];  // For corrected bills only
   char  Name1[TSIZ_OWNERNAME];
   char  Name2[TSIZ_OWNERNAME];
   char  CRLF[2];
} EDX_TAXBILL;

typedef struct _tEdxAgency
{
   char  Agency[TSIZ_AGENCY];
   char  Phone[TSIZ_PHONE];
   char  Amount[TSIZ_AMOUNT];
} EDX_AGENCY;

// Secured Roll (EDC_SECURED_TAX_BILLS_15-16.txt)
typedef struct _tEdxSecRoll
{
   char  APN_D                [TSIZ_PARCEL_NUMBER        ];
   char  BillNum              [TSIZ_BILL_NUMBER          ];
   char  filler1              [2];
   char  LienName1            [TSIZ_OWNERNAME+1          ];
   char  LienName2            [TSIZ_OWNERNAME+1          ];
   char  CurName1             [TSIZ_OWNERNAME+1          ];
   char  CurName2             [TSIZ_OWNERNAME            ];
   char  Street               [TSIZ_STREET               ];
   char  City                 [TSIZ_CITY                 ];
   char  State                [TSIZ_STATE                ];
   char  Country              [TSIZ_COUNTRY              ];
   char  Zip_Code             [TSIZ_ZIP_CODE             ];
   char  Cortac_Number        [TSIZ_CORTAC_NUMBER        ];
   char  TRA                  [TSIZ_TAX_RATE_AREA        ];
   char  Property_Description [TSIZ_PROPERTY_DESCRIPTION ];
   char  Sec_Def_Bill_Number  [TSIZ_SEC_DEF_BILL_NUMBER  ];
   char  Sec_Def_Tax_Year     [TSIZ_SEC_DEF_TAX_YEAR     ];
   char  Unsec_Def_Year       [TSIZ_UNSEC_DEF_YEAR       ];
   char  Unsec_Def_Bill_Number[TSIZ_UNSEC_DEF_BILL_NUMBER];
   char  DelqDate1            [TSIZ_DELQDATE1            ];
   char  InstAmt1             [TSIZ_INSTAMT1             ];
   char  PenAmt1              [TSIZ_PENAMT1              ];
   char  DelqAmt1             [TSIZ_DELQAMT1             ];
   char  Ocra_Scan_Line_Inst1 [TSIZ_OCRA_SCAN_LINE_INST1 ];
   char  DelqDate2            [TSIZ_DELQDATE1            ];
   char  InstAmt2             [TSIZ_INSTAMT1             ];
   char  PenAmt2              [TSIZ_PENAMT1              ];
   char  Cost                 [TSIZ_COST                 ];
   char  DelqAmt2             [TSIZ_DELQAMT1             ];
   char  Ocra_Scan_Line_Inst2 [TSIZ_OCRA_SCAN_LINE_INST1 ];
   char  ValueBeforeExe       [TSIZ_VALUEBEFOREEXE       ];
   char  Tax_Rate             [TSIZ_TAX_RATE             ];
   char  Gross_Tax            [TSIZ_GROSS_TAX            ];
   char  Total_Exe            [TSIZ_TOTAL_EXE            ];
   char  Net_Tax              [TSIZ_NET_TAX              ];
   char  Direct_Assessments   [TSIZ_DIRECT_ASSESSMENTS   ];
   char  Total_TaxDue         [TSIZ_TOTAL_TAXDUE         ];
   char  Land                 [TSIZ_LAND                 ];
   char  Improvements         [TSIZ_IMPROVEMENTS         ];
   char  Total_Value          [TSIZ_TOTAL_VALUE          ];
   char  Personal_Property    [TSIZ_PERSONAL_PROPERTY    ];
   char  ExeAmt1              [TSIZ_EXEAMT1              ];
   char  ExeCode1             [TSIZ_EXECODE1             ];
   char  ExeAmt2              [TSIZ_EXEAMT2              ];
   char  ExeCode2             [TSIZ_EXECODE2             ];
   char  ExeAmt3              [TSIZ_EXEAMT3              ];
   char  ExeCode3             [TSIZ_EXECODE3             ];
   char  Net_Value            [TSIZ_NET_VALUE            ];
   EDX_AGENCY  Agency         [EDX_MAX_AGENCY            ];
   char  Due_Date1            [TSIZ_DUE_DATE1            ];
   char  Due_Date2            [TSIZ_DUE_DATE2            ];
   char  Situs                [TSIZ_SITUS                ];
   char  Sort_Type_Key        [TSIZ_SORT_TYPE_KEY        ];
   char  Sort_Zip_Key         [TSIZ_SORT_ZIP_KEY         ];
   char  CrLf[2];
} EDX_SECROLL;

#define  TSIZ_TAXCLASS                    5
#define  TSIZ_DFLT_TAXAMT                 8
#define  TSIZ_DFLT_PENAMT                 7
#define  TSIZ_DFLT_COSTAMT                4
#define  TSIZ_PAYMENTNO                   2
#define  TSIZ_RECEIPTNO                   6
#define  TSIZ_INT_RATE                    3
#define  TSIZ_INT_AMT                     7

// Secured Abstract Accounts Receivable (SA_AR.TXT)
typedef struct _tEdxSA_AR
{
   char  DefaultNo[TSIZ_DEFAULTNO];
   char  TaxYear[TSIZ_TAXYEAR];
   char  BillNum[TSIZ_BILLNUM];
   char  Supl_TaxYear[TSIZ_TAXYEAR];
   char  Rec_Code;                           // 23 - Always 2
   char  Apn[TSIZ_APN];                      // 24
   char  TaxClass[TSIZ_TAXCLASS];            // 35 - ?
   char  TaxAmt[TSIZ_DFLT_TAXAMT];           // 40
   char  PenAmt[TSIZ_DFLT_PENAMT];           // 48
   char  CostAmt[TSIZ_DFLT_COSTAMT];         // 55
   char  RedeemFlg;                          // 59 - R, U
} EDX_SAAR;

// Secured Abstract Installment Payment (INSTALMT.TXT)
typedef struct _tEdxPaid
{
   char  DefaultNo[TSIZ_DEFAULTNO];
   char  PaymentNo[TSIZ_PAYMENTNO];
   char  Rec_Code;                           // 10 - Always 2
   char  Apn[TSIZ_APN];                      // 11
   char  PaidDate[TSIZ_DATE];                // 22
   char  RedAmt[TSIZ_DFLT_TAXAMT];           // 30
   char  PaidAmt[TSIZ_DFLT_TAXAMT];          // 38
   char  IntRate[TSIZ_INT_RATE];             // 46
   char  IntPaid[TSIZ_INT_AMT];              // 49
   char  TotalPaid[TSIZ_DFLT_TAXAMT];        // 56
   char  ReceiptNo[TSIZ_RECEIPTNO];          // 64
   char  NormalPayAmt[TSIZ_DFLT_TAXAMT];     // 70
   char  DefaultFlg;                         // 78 - D=default, R=redeemed
   char  RedeemFlg;                          // 79 - Q, T, U
                                             /*
                                                X - Redeemed: Curr month (Changed to R at month end)
                                                R - Redeemed: Prior month (Removed year end )
                                                V - Redeemed: Future month (Changed to X at month end)
                                                Q - Redeemed: Prior month final paymemt (Removed year end )
                                                T - Redeemed: Curr month final payment (Changed to Q at month end)
                                                W - Redeemed: Future month final payment (Changed to T at month end)
                                                A - Active: Abstract added by SAX616 or un-deleted by SAM136 (internal information)
                                                B - Active: Account in bankruptcy
                                                F - Active: Omit from publication
                                                N - Active: New Default created by Separation or Segregation
                                                U - Active: Unredeemed prior month redemption
                                                O - Active: Omit from publication (No longer used)
                                                D - Deleted: Deleted Abstract
                                                C - Control record
                                             */
} EDX_PAID;

// Secured Abstract ORIGINAL DEFAULT AMOUNT (SOLD4TAX.TXT)
typedef struct _tEdxSold4Tax
{
   char  DefaultNo[TSIZ_DEFAULTNO];
   char  Rec_Code;                           // 8 - 1=Unsecured, 2=Secured
   char  Apn[TSIZ_APN];                      // 9
   char  Def_Date[TSIZ_DATE];                // 20
   char  Def_Amt[TSIZ_DFLT_TAXAMT];          // 28
   char  RedeemFlg;                          // 36 - A, R, U, blank
} EDX_SOLD4TAX;

// History sale layout
#define  EDX_HS_APN             0 
#define  EDX_HS_USE_CODE        1
#define  EDX_HS_EFF_DT          2
#define  EDX_HS_EFFDTSEQ        3
#define  EDX_HS_DOCNUM          4
#define  EDX_HS_DOCDATE         5
#define  EDX_HS_DOWNPMNT        6
#define  EDX_HS_TR_DEED         7
#define  EDX_HS_BOND_AMT        8
#define  EDX_HS_ACREAGE         9
#define  EDX_HS_VIEWCODE        10
#define  EDX_HS_CONST_TYPE      11
#define  EDX_HS_QUALCLS         12
#define  EDX_HS_HOUSE_SHAPE     13
#define  EDX_HS_EFFYRBLT        14
#define  EDX_HS_BEDS            15
#define  EDX_HS_BATHS           16
#define  EDX_HS_GAR_STALLS      17
#define  EDX_HS_FIREPL          18
#define  EDX_HS_GAR_CON         19
#define  EDX_HS_SWIM_POOL       20
#define  EDX_HS_STORIES         21
#define  EDX_HS_BLDGSQFT        22
#define  EDX_HS_TOTPSF          23
#define  EDX_HS_DOCTAX          24
#define  EDX_HS_REJ_CODE        25
#define  EDX_HS_CONF_LEVL       26
#define  EDX_HS_APPR_CODE       27
#define  EDX_HS_PCT_REAPPR      28
#define  EDX_HS_APPR_DT         29

#endif