// SFX county definition
#define  ROFF_CTLBYTE              1-1
#define  ROFF_VOLUME               2-1
#define  ROFF_APN                  4-1
#define  ROFF_S_ADDR1              13-1
#define  ROFF_ROLL_YEAR            50-1
#define  ROFF_ROLL_STATUS          52-1
#define  ROFF_LAND                 53-1
#define  ROFF_STRUCT               64-1
#define  ROFF_TRADE_FIXT           75-1
#define  ROFF_PERSPROP             86-1
#define  ROFF_HO_EXE               97-1
#define  ROFF_OTHER_EXEMP          108-1
#define  ROFF_OTHER_EXEMP_CODE     119-1
#define  ROFF_WORKYEAR             121-1
#define  ROFF_WY_STATUS            123-1
#define  ROFF_WY_LAND              124-1
#define  ROFF_WY_STRUCT            135-1
#define  ROFF_WY_TRADE_FIXT        146-1
#define  ROFF_WY_PERS_PROP         157-1
#define  ROFF_WY_HOMEOWNER_EXEMP   168-1
#define  ROFF_WY_OTHER_EXEMP       179-1
#define  ROFF_WY_EXEMPTION_TYPE    190-1
#define  ROFF_VALUE_DATE           192-1
#define  ROFF_BASE_YEAR_CODE       198-1
#define  ROFF_SALES_BASE_YR        199-1
#define  ROFF_PRIOR_SALE_CODE      203-1
#define  ROFF_PRIOR_SALE_AMOUNT    204-1
#define  ROFF_PRIOR_SALE_DATE      215-1     // YYMMDD
#define  ROFF_CURRENT_SALE_CODE    221-1
#define  ROFF_CURRENT_SALE_AMOUNT  222-1
#define  ROFF_CURRENT_SALE_DATE    233-1
#define  ROFF_SALES_RATIO          239-1
#define  ROFF_REENTRY              243-1
#define  ROFF_OTHER_TRAN_CODE      247-1
#define  ROFF_OTHER_TRAN_DATE      249-1
#define  ROFF_SOLD2ST_CODE         255-1
#define  ROFF_SOLD2ST_DATE         256-1
#define  ROFF_SOLD2ST_NUMBER       262-1
#define  ROFF_LEASEHOLD            267-1
#define  ROFF_LAND_USE             268-1
#define  ROFF_NEIGHBORHOOD         271-1
#define  ROFF_QUAL_CLASS_CODE      274-1
#define  ROFF_QUAL_FINISH_BASEMNT  277-1
#define  ROFF_KITCHEN              278-1
#define  ROFF_BUILTIN              281-1
#define  ROFF_STORY_CODE           285-1
#define  ROFF_SUB_LEVEL            286-1
#define  ROFF_ROOM_CODE            287-1
#define  ROFF_CONST_TYPE           288-1
#define  ROFF_BASE_LOT             291-1
#define  ROFF_ZONING               295-1
#define  ROFF_APPRAISER            301-1
#define  ROFF_CHAR_CHNG_DATE       304-1
#define  ROFF_LOT_CODE             308-1
#define  ROFF_WIDTH                309-1
#define  ROFF_DEPTH                313-1
#define  ROFF_LOT_SQFT             317-1
#define  ROFF_BASEMENT_AREA        327-1
#define  ROFF_STORIES              332-1
#define  ROFF_UNITS                335-1
#define  ROFF_TOTAL_ROOMS          340-1
#define  ROFF_BEDS                 345-1
#define  ROFF_BATHS                350-1
#define  ROFF_BLDG_SQFT            355-1
#define  ROFF_CONST_YEAR           362-1
#define  ROFF_AGE                  366-1
#define  ROFF_ALT_NC               371-1
#define  ROFF_TRA                  375-1
#define  ROFF_LAST_CHG_DATE        379-1
#define  ROFF_UNUSED4              387-1

#define  RSIZ_CTLBYTE              1
#define  RSIZ_VOLUME               2
#define  RSIZ_APN                  9
#define  RSIZ_S_ADDR1              37
#define  RSIZ_ROLL_YEAR            2
#define  RSIZ_ROLL_STATUS          1
#define  RSIZ_LAND                 11
#define  RSIZ_STRUCT               11
#define  RSIZ_TRADE_FIXT           11
#define  RSIZ_PERSPROP             11
#define  RSIZ_HO_EXE               11
#define  RSIZ_OTHER_EXEMP          11
#define  RSIZ_OTHER_EXEMP_CODE     2
#define  RSIZ_WORKYEAR             2
#define  RSIZ_WY_STATUS            1
#define  RSIZ_WY_LAND              11
#define  RSIZ_WY_STRUCT            11
#define  RSIZ_WY_TRADE_FIXT        11
#define  RSIZ_WY_PERS_PROP         11
#define  RSIZ_WY_HOMEOWNER_EXEMP   11
#define  RSIZ_WY_OTHER_EXEMP       11
#define  RSIZ_WY_EXEMPTION_TYPE    2
#define  RSIZ_VALUE_DATE           6
#define  RSIZ_BASE_YEAR_CODE       1
#define  RSIZ_SALES_BASE_YR        4
#define  RSIZ_PRIOR_SALE_CODE      1
#define  RSIZ_PRIOR_SALE_AMOUNT    11
#define  RSIZ_PRIOR_SALE_DATE      6
#define  RSIZ_CURRENT_SALE_CODE    1
#define  RSIZ_CURRENT_SALE_AMOUNT  11
#define  RSIZ_CURRENT_SALE_DATE    6
#define  RSIZ_SALES_RATIO          4
#define  RSIZ_UNUSED3              4
#define  RSIZ_OTHER_TRAN_CODE      2
#define  RSIZ_OTHER_TRAN_DATE      6
#define  RSIZ_SOLD2ST_CODE         1
#define  RSIZ_SOLD2ST_DATE         6
#define  RSIZ_SOLD2ST_NUMBER       5
#define  RSIZ_LEASEHOLD            1
#define  RSIZ_LAND_USE             3
#define  RSIZ_NEIGHBORHOOD         3
#define  RSIZ_QUAL_CLASS_CODE      3
#define  RSIZ_QUAL_FINISH_BASEMNT  1
#define  RSIZ_KITCHEN              3
#define  RSIZ_BUILTIN              4
#define  RSIZ_STORY_CODE           1
#define  RSIZ_SUB_LEVEL            1
#define  RSIZ_ROOM_CODE            1
#define  RSIZ_CONST_TYPE           3
#define  RSIZ_BASE_LOT             4
#define  RSIZ_ZONING               6
#define  RSIZ_APPRAISER            3
#define  RSIZ_CHAR_CHNG_DATE       4
#define  RSIZ_LOT_CODE             1
#define  RSIZ_WIDTH                4
#define  RSIZ_DEPTH                4
#define  RSIZ_LOT_SQFT             10
#define  RSIZ_BASEMENT_AREA        5
#define  RSIZ_STORIES              3
#define  RSIZ_UNITS                5
#define  RSIZ_TOTAL_ROOMS          5
#define  RSIZ_BEDS                 5
#define  RSIZ_BATHS                5
#define  RSIZ_BLDG_SQFT            7
#define  RSIZ_CONST_YEAR           4
#define  RSIZ_AGE                  5
#define  RSIZ_ALT_NC               4
#define  RSIZ_TRA                  4
#define  RSIZ_LAST_CHG_DATE        8
#define  RSIZ_UNUSED4              33
#define  RSIZ_SFX                  419

typedef struct _tSfxRoll
{
   char  CntlByte;                     
   char  Volume[RSIZ_VOLUME];                    
   char  Apn[RSIZ_APN];                         // Block-Lot 9999A999A
   char  S_Addr1[RSIZ_S_ADDR1];
   char  RollYr[RSIZ_ROLL_YEAR];                // YY
   char  Status;
   char  Land[RSIZ_LAND];
   char  Impr[RSIZ_LAND];
   char  Fixt_Val[RSIZ_LAND];                   // Fixed machinery & equiptment exempt
   char  PP_Val[RSIZ_LAND];
   char  HO_Exe[RSIZ_LAND];
   char  Other_Exe[RSIZ_LAND];                  // 108
   char  Exe_Code[RSIZ_OTHER_EXEMP_CODE];       // 119
   char  wy_RollYr[RSIZ_ROLL_YEAR];             // 121 - YY
   char  wy_Status;                             // 123
   char  wy_Land[RSIZ_LAND];                    // 124
   char  wy_Impr[RSIZ_LAND];                    // 135
   char  wy_Fixt_Val[RSIZ_LAND];                // 146
   char  wy_PP_Val[RSIZ_LAND];                  // 157
   char  wy_HO_Exe[RSIZ_LAND];                  // 168
   char  wy_Other_Exe[RSIZ_LAND];               // 179
   char  wy_Exe_Code[RSIZ_OTHER_EXEMP_CODE];    // 190
   char  ValueDate[RSIZ_VALUE_DATE];            // 192 - YYMMDD
   char  filler2;
   char  Sale_BaseYr[RSIZ_SALES_BASE_YR];       // 199 - YYYY
   char  Prior_SaleCode;                        // 203
   char  Prior_SaleAmt[RSIZ_PRIOR_SALE_AMOUNT]; // 204
   char  Prior_SaleDt[RSIZ_PRIOR_SALE_DATE];    // 215 - YYMMDD
   char  SaleCode;                              // 221
   char  SaleAmt[RSIZ_PRIOR_SALE_AMOUNT];       // 222
   char  SaleDt[RSIZ_PRIOR_SALE_DATE];          // YYMMDD
   char  SaleRatio[RSIZ_SALES_RATIO];           // 9V999
   char  filler3[4];
   char  Trans_Code[RSIZ_OTHER_TRAN_CODE];
   char  Trans_Date[RSIZ_OTHER_TRAN_DATE];      // YYMMDD
   char  filler4[13];
   char  UseCode[RSIZ_LAND_USE];
   char  Neighborhood[RSIZ_NEIGHBORHOOD];
   char  QualCode[RSIZ_QUAL_CLASS_CODE];        // Don't use
   char  QualFinBsmt;
   char  Kitchen[RSIZ_KITCHEN];
   char  BuiltIn[RSIZ_BUILTIN];
   char  StoryCode;                             // Don't use
   char  SubLevel;                              // Don't use
   char  RoomCode;                              // Don't use
   char  ConstType[RSIZ_CONST_TYPE];
   char  BaseLot[RSIZ_BASE_LOT];
   char  Zoning[RSIZ_ZONING];
   char  filler5[7];
   char  LotCode;
   char  Width[RSIZ_WIDTH];
   char  Depth[RSIZ_DEPTH];
   char  LotSqft[RSIZ_LOT_SQFT];
   char  BasemntArea[RSIZ_BASEMENT_AREA];
   char  Stories[RSIZ_STORIES];
   char  Units[RSIZ_UNITS];
   char  Rooms[RSIZ_TOTAL_ROOMS];
   char  Beds[RSIZ_BEDS];
   char  Baths[RSIZ_BATHS];
   char  BldgSqft[RSIZ_BLDG_SQFT];
   char  ConstYr[RSIZ_CONST_YEAR];              // YYYY
   char  Age[RSIZ_AGE];
   char  Alt_NC[RSIZ_ALT_NC];
   char  TRA[RSIZ_TRA];
   char  LastChgDate[RSIZ_LAST_CHG_DATE];       // YYYYMMDD
   char  filler6[36];
} SFX_ROLL;

typedef struct _tSfxSitus
{
   char  HseNum2[4];
   char  HseSub2[1];
   char  HseNum1[4];
   char  HseSub1[1];
   char  StrName[20];
   char  StrSfx[2];
   char  UnitNo[5];
   char  filler[27];
} SFX_SITUS;

#define OSIZ_APN                 9
#define OSIZ_NAME                30
#define OSIZ_DATE                6
#define OSIZ_RECNUM              8
#define OSIZ_PCT_OWN             10
#define OSIZ_LOTCODE             3

#define OOFF_APN                 4
#define OOFF_SEQNO               13
#define OOFF_NAME                15
#define OOFF_RECDATE             45
#define OOFF_STATUS              51
#define OOFF_NAMEFLG             52
#define OOFF_RECNUM              56
#define OOFF_PCTOWN              64
#define OOFF_SALE_DT             74
#define OOFF_UPDT_DT             80

typedef struct _tSfxOwnerRec
{
   char  CntlByte;                     // 1
   char  Volume[2];                    // 2
   char  Apn[RSIZ_APN];                // 4
   char  SeqNo[2];                     // 13
   char  Name[OSIZ_NAME];              // 15
   char  RecDate[OSIZ_DATE];           // 45 - YYMMDD
   char  Status;                       // 51 - N = Gov property
   char  NameFlg;                      // 52 
   char  NewLotCode[OSIZ_LOTCODE];     // 53
   char  RecNum[OSIZ_RECNUM];          // 56
   char  PctOwn[OSIZ_PCT_OWN];         // 64
   char  SaleDt[OSIZ_DATE];            // 74 - YYMMDD
   char  UpdtDt[OSIZ_DATE];            // 80 - MMDDYY
   char  CRLF[2];                      // 86
} SFX_OWNER;

#define ASIZ_APN                 9
#define ASIZ_BLOCK               5
#define ASIZ_LOT                 4
#define ASIZ_NAME                27
#define ASIZ_DATE                6
#define ASIZ_M_ADDR              27

typedef struct _tSfxMAdrRec
{
   char  CntlByte;               // 1
   char  Volume[2];              // 2
   char  Apn[ASIZ_APN];          // 4
   char  filler1[23];            // 13
   char  Source;                 // 36
   char  Name[ASIZ_NAME];        // 37
   char  M_Addr1[ASIZ_M_ADDR];   // 64
   char  M_Addr2[ASIZ_M_ADDR];   // 91
   char  M_Addr3[ASIZ_M_ADDR];   // 118
   char  M_Zip[5];               // 145
   char  M_Zip4[4];              // 150
   char  UpdtDt[ASIZ_DATE];      // 154 - MMDDYY
   char  CRLF[2];                // 160
} SFX_MADR;

#define SSIZ_APN                 9
#define SSIZ_REC_PAGE            8
#define SSIZ_REC_DATE            8
#define SSIZ_SALE_PRICE          9

typedef struct t_SfxCumSale
{
   char  Apn[SSIZ_APN];
   char  RecNum[SSIZ_REC_PAGE];
   char  RecDate[SSIZ_REC_DATE];          // CCYYMMDD
   char  SaleCode;
   char  filler[2];
   char  SalePrice[SSIZ_SALE_PRICE];
   char  SaleDate[SSIZ_REC_DATE];
} SFX_CSAL;


// Tax data 03/18/2021
// PT___Tax_Data_Secured_New-03_14_2021_03_00AM.csv
#define  TDS_APN                 0
#define  TDS_BOOK                1
#define  TDS_BLOCK               2
#define  TDS_LOT                 3
#define  TDS_BILL_NO             4
#define  TDS_INST_NO             5
#define  TDS_ROLL_YR             6
#define  TDS_ASMT_YR             7
#define  TDS_ACCOUNT_TYPE        8
#define  TDS_SITUS_ADDRESS       9
#define  TDS_ASSD_VAL            10
#define  TDS_ASSD_LAND           11
#define  TDS_ASSD_IMPR           12
#define  TDS_ASSD_FIXT           13
#define  TDS_ASSD_PERS           14
#define  TDS_ASSD_OTHER          15
#define  TDS_TOTAL_EXE_VAL       16
#define  TDS_DUE_DATE            17
#define  TDS_EXE_TYPE            18
#define  TDS_LAST_CORR_DATE      19
#define  TDS_ESCROW_COMPANY      20
#define  TDS_CUSTOM_FLAGS        21
#define  TDS_TRA_NAME            22
#define  TDS_PEN_AMT             23
#define  TDS_EXE_AMT             24
#define  TDS_ASSESSEE_NAME       25
#define  TDS_DBA_NAME            26
#define  TDS_MAIL_NAME           27
#define  TDS_MAIL_ADDR_LINE_1    28
#define  TDS_MAIL_ADDR_LINE_2    29
#define  TDS_MAIL_ADDR_LINE_3    30
#define  TDS_MAIL_ADDR_CITY      31
#define  TDS_MAIL_ADDR_STATE     32
#define  TDS_MAIL_ADDR_ZIP       33
#define  TDS_TAX_AMT             34
#define  TDS_ACCOUNT_STATUS      35
#define  TDS_PAID_STATUS         36
#define  TDS_DUE_AMT             37
#define  TDS_IS_DEFAULTED        38
#define  TDS_DEFAULT_DATE        39
#define  TDS_DEFAULT_NUMBER      40
#define  TDS_PAID_DATE           41
#define  TDS_FLDS                42

// PT___Tax_Data_Redemption_New-03_14_2021_03_00AM.csv
#define  TDR_APN                 0
#define  TDR_BOOK                1
#define  TDR_BLOCK               2
#define  TDR_LOT                 3
#define  TDR_ROLL                4
#define  TDR_ROLL_YR             5
#define  TDR_ASMT_YR             6
#define  TDR_ACCOUNT_TYPE        7
#define  TDR_BILL_NO             8
#define  TDR_INST_NO             9
#define  TDR_IS_PAYMENT_PLAN     10
#define  TDR_CANCELED            11
#define  TDR_STANDARD_FLAGS      12
#define  TDR_ASSESSEE_NAME       13
#define  TDR_TOTAL_TAX           14
#define  TDR_ACCOUNT_STATUS      15
#define  TDR_PAID_STATUS         16
#define  TDR_IS_DEFAULTED        17
#define  TDR_DEFAULT_NO          18
#define  TDR_DEFAULT_DATE        19
#define  TDR_IN_ACTIVE_PLAN      20
#define  TDR_FLDS                21

// PT___Tax_Data_Special_Assessments_New-03_14_2021_03_00AM
#define  TSA_APN                 0
#define  TSA_BILL_NO             1
#define  TSA_ROLL_YR             2
#define  TSA_ACCOUNT_TYPE        3
#define  TSA_DIST_CODE           4
#define  TSA_DISTRIBUTION_CLASS  5
#define  TSA_BALANCE_STATUS      6
#define  TSA_DIST_NAME           7
#define  TSA_DIST_TAX_RATE       8
#define  TSA_DIST_TYPE           9
#define  TSA_TRA_CODE            10
#define  TSA_TRA_NAME            11
#define  TSA_AMOUNT              12
#define  TSA_FLDS                13

// PT___Tax_Data_Sec_Supp_New-03_14_2021_03_00AM.csv
#define  TSS_APN                 0
#define  TSS_BOOK                1
#define  TSS_BLOCK               2
#define  TSS_LOT                 3
#define  TSS_BILL_NO             4
#define  TSS_INST_NO             5
#define  TSS_ROLL_YR             6
#define  TSS_ASMT_YR             7
#define  TSS_ACCOUNT_TYPE        8
#define  TSS_SITUS_ADDRESS       9
#define  TSS_ASSD_LAND_VAL       10
#define  TSS_ASSD_IMPR_VAL       11
#define  TSS_TOTAL_EXE_VAL       12
#define  TSS_EXE_TYPE            13
#define  TSS_TOTAL_ASSD_VAL      14
#define  TSS_ASSD_LAND_VAL_NEW   15
#define  TSS_ASSD_IMPR_VAL_NEW   16
#define  TSS_TOTAL_EXE_VAL_NEW   17
#define  TSS_TOTAL_ASSD_VAL_NEW  18
#define  TSS_I_MATH_CONDITION    19
#define  TSS_J_MATH_CONDITION    20
#define  TSS_K_MATH_CONDITION    21
#define  TSS_L_MATH_CONDITION    22
#define  TSS_EVENT_DATE          23
#define  TSS_MAILED_DATE         24
#define  TSS_LAST_CORR_DATE      25
#define  TSS_ASMT_NUMBER         26
#define  TSS_TAX_RATE_YR         27
#define  TSS_PEN_AMT             28
#define  TSS_DUE_DATE            29
#define  TSS_PAID_DATE           30
#define  TSS_ASSESSEE_NAME       31
#define  TSS_MAIL_NAME           32
#define  TSS_MAIL_ADDR_LINE_1    33
#define  TSS_MAIL_ADDR_LINE_2    34
#define  TSS_MAIL_ADDR_LINE_3    35
#define  TSS_MAIL_ADDR_CITY      36
#define  TSS_MAIL_ADDR_STATE     37
#define  TSS_MAIL_ADDR_ZIP       38
#define  TSS_TAX_AMT             39
#define  TSS_ACCOUNT_STATUS      40
#define  TSS_PAID_STATUS         41
#define  TSS_DUE_AMT             42
#define  TSS_IS_DEFAULTED        43
#define  TSS_DEFAULT_DATE        44
#define  TSS_DEFAULT_NUMBER      45
#define  TSS_FLDS                46

// Standard codes
// H=Homeowners
// D=Disabled Veterans
// V=Veterans
// C=Church
// L=Library
// M=Museums
// R=Religion
// T=Tribal Housing
// W=Welfare
// U=College
// P=Public School
// E=Cemetery
// G=Government
// I=Hospital
// S=School
// X=Misc exempt
// Y=Military

IDX_TBL4 SFX_Exemption[] = 
{
   "01","S", 2,1,    // 
   "02","I", 2,1,    // 
   "03","W", 2,1,    // 
   "04","D", 2,1,    // 
   "06","R", 2,1,    // 
   "07","C", 2,1,    // 
   "08","U", 2,1,    // 
   "11","H", 2,1,    // 
   "13","X", 2,1,    // LOW-INCOME HOUSING
   "14","M", 2,1,    // MUSEUM/PUBLIC LIBRARY
   "18","X", 2,1,    // LESSOR
   "19","P", 2,1,    // Public School Lessee
   "25","E", 2,1,    // Cemetery
   "HO","H", 2,1,    // 
   "","",0,0
};


