/********************************************************************
 *
 * 09/17/2012 Fix bug in splitOwner()
 * 02/14/2016 Modify splitOwner() to keep swap name the same as Name1 if first name
 *            includes a number or first char is not alpha.
 * 02/16/2016 Modify splitOwner() to keep swap name the same as Name1 if last name is single letter.
 * 06/30/2020 Modify splitOwner() to remove extra space in swap name
 *
 ********************************************************************/

#include "stdafx.h"
#include "Prodlib.h"
#include "Utils.h"
#include "R01.h"
#include "doOwner.h"

/********************************************************************
 *
 * Assuming that the longest last name may have up to 3 words.  We 
 * marks all space within last name so we can parse it easier.
 *
 * Return TRUE if there is comma in name
 *
 ********************************************************************/

bool markLastName(char *pName)
{
   char *pTmp, *pTmp1;
   bool  bRet=false;

   if (pTmp = strchr(pName, ','))
   {
      bRet = true;
      *pTmp = 0;
      pTmp1 = pName;
      while (*++pTmp1)
      {
         if (*pTmp1 == ' ')
            *pTmp1 = '+';
      }
      *pTmp = ' '; // Replace null with space
   }

   return bRet;
}

void clearMarker(char *pName)
{
   char *pTmp;

   pTmp = pName;
   while (*++pTmp)
   {
      if (*pTmp == '+')
         *pTmp = ' ';
   }
}

/************************************* MergeName **********************************
 *
 * Merge names that has the same last name
 *
 **********************************************************************************/

void MergeName(char *pName1, char *pName2, char *pNames)
{
   char *pTmp, acTmp[128];
   int iTmp;

   strcpy(acTmp, pName1);
   if (acTmp[strlen(pName1)-1] != '&')
      strcat(acTmp, " &");

   pTmp = strchr(pName2, ',');
   if (pTmp)
      strcat(acTmp, ++pTmp);
   else
   {
      // In case there is no comma after last name
      iTmp = 0;
      while (*(pName1+iTmp) == *(pName2+iTmp))
         iTmp++;

      // If last name is matching up OK, we can merge them
      if (iTmp > 2 && *(pName1+iTmp) == ',' && *(pName2+iTmp) == ' ')
         strcat(acTmp, pName2+iTmp);

   }
   strcpy(pNames, acTmp);
}

/************************************* MergeName1 *********************************
 *
 * Merge names that has the same last name.  If mergable, it returns 1, else 0.
 *
 **********************************************************************************/

int MergeName1(char *pOwner1, char *pOwner2, char *pNames)
{
   char  acOwner1[128], acOwner2[128], acTmp[128], acSave[16], *pTmp, *pName1, *pName2;

   *pNames = 0;
   strcpy(acOwner1, pOwner1);
   strcpy(acOwner2, pOwner2);

   pName1 = strchr(acOwner1, ' ');
   pName2 = strchr(acOwner2, ' ');
   // If either name is single word, do nothing
   if (!pName1 || !pName2)
      return 0;

   *pName1 = 0;
   *pName2 = 0;
   // If last name are not the same, do nothing
   if (strcmp(acOwner1, acOwner2))
      return 0;

   *pName1 = ' ';       // Keep Name1 as is
   pName1 = acOwner1;
   pName2++;            // Skip last name of owner2

   // Check for "TR" in name 1
   pTmp = strrchr(pName1, ' ');
   if (pTmp && !strcmp(pTmp, " TR"))
   {
      strcpy(acSave, pTmp);
      *pTmp = 0;
   } else
      acSave[0] = 0;

   pTmp = strrchr(pName2, ' ');
   if (pTmp && !strcmp(pTmp, " TR") )
      *pTmp = 0;

   // Combine owners
   sprintf(acTmp, "%s & %s%s", pName1, pName2, acSave);
   strcpy(pNames, acTmp);

   return 1;
}

/************************************* MergeName1 *********************************
 *
 * Merge names that has the same last name.  If mergable, return 1.
 * If same last and first name, return 2.  Return 0 if not mergable.
 *
 **********************************************************************************/

int MergeName2(char *pOwner1, char *pOwner2, char *pNames, char bChk)
{
   char  acOwner1[128], acOwner2[128], acTmp[128];
   char  *pName1, *pName2, *pTmp1, *pTmp2;

   *pNames = 0;

   // If the same name, return
   if (!strcmp(pOwner1, pOwner2))
      return 2;

   strcpy(acOwner1, pOwner1);
   strcpy(acOwner2, pOwner2);

   pName1 = strchr(acOwner1, bChk);
   pName2 = strchr(acOwner2, bChk);
   // If either name is single word, do nothing
   if (!pName1 || !pName2)
      return 0;

   *pName1 = 0;
   *pName2 = 0;

   // If last name are not the same, do nothing
   if (strcmp(acOwner1, acOwner2))
      return 0;

   // If they have the same first name, don't combine and return 2
   pTmp1 = strchr(pName1+2, ' ');
   pTmp2 = strchr(pName2+2, ' ');
   if (pTmp1 && pTmp2)
   {
      *pTmp1 = 0;
      *pTmp2 = 0;
      if (!strcmp(pName1+1, pName2+1))
         return 2;
      
      *pTmp1 = ' ';
      *pTmp2 = ' ';
   } else if (!pTmp1 && !pTmp2)
   {
      if (!strcmp(pName1+1, pName2+1))
         return 2;
   }

   // Combine owners
   *pName1 = ' ';       // Keep Name1 as is
   pName2++;            // Skip last name of owner2

   sprintf(acTmp, "%s & %s", acOwner1, pName2);
   blankRem(acTmp);
   strcpy(pNames, acTmp);

   return 1;
}

/************************************* splitOwner *********************************
 *
 * Split owner names into individual components then produce name fields for PQ4
 * and CD-Assr products.
 * Return:
 *   -1 = Same as input name
 *    0 = compound name
 *    1 = single name
 *    2 = 2 separate names
 *
 **********************************************************************************/

int splitOwner(char *pNames, OWNER *pOwners, int iParseType)
{
   char  strSwapName[128], strOwners[128], strOwner2[128];
   char  acOF[64], acOL[64], acOM[64], acSF[64], acSM[64], acSL[64];
   char  acSpouse[128], acTitle[64];
   int   iFlg=0;

   memset((void *)pOwners, 0, sizeof(OWNER));
   strOwner2[0] = 0;
   acTitle[0] = 0;

   switch (iParseType)
   {
      case 1:
         iFlg = ParseOwnerName1(pNames, acOF, acOM, acOL, acSF, acSM, acSL);
         break;
      case 2:
         iFlg = ParseOwnerName2(pNames, acOF, acOM, acOL, acSF, acSM, acSL);
         break;
      case 3:
         iFlg = ParseOwnerName3(pNames, acOF, acOM, acOL, acSF, acSM, acSL, acTitle);
         break;
      case 4:
         iFlg = ParseOwnerName4(pNames, acOF, acOM, acOL, acSF, acSM, acSL, acTitle);
         break;
      case 5:
         iFlg = ParseOwnerName5(pNames, acOF, acOM, acOL, acSF, acSM, acSL, acTitle);
         break;
      case 6:
         iFlg = ParseOwnerName6(pNames, acOF, acOM, acOL, acSF, acSM, acSL, acTitle);
         break;
      default:
         ParseOwnerName(pNames, acOF, acOM, acOL, acSF, acSM, acSL, acTitle);
   }

   if (iFlg > 0)
   {
      // Strip off extra spaces
      blankRem(pNames);
      strncpy(pOwners->acName1, pNames, 63);
      strncpy(pOwners->acSwapName, pNames, 63);
      return -1;
   }

   strcpy(strSwapName, acOF);
   if (acOM[0])
   {
      strcat(strSwapName, " ");
      strcat(strSwapName, acOM);
   }

   // If spouse last name avail,, concat name1 and name2
   if (acSL[0])
   {
      strcat(strSwapName, " ");
      strcat(strSwapName, acOL);

      // Make owner2
      if (acSM[0])
         sprintf(strOwner2, "%s %s %s", acSL, acSF, acSM);
      else
         sprintf(strOwner2, "%s %s", acSL, acSF);

      // Make Owner1
      if (acOM[0])
         sprintf(strOwners, "%s %s %s", acOL, acOF, acOM);
      else
         sprintf(strOwners, "%s %s", acOL, acOF);

      iFlg = 2;        // 2 separate names
   } else
   {
      iFlg = 0;
      if (acSF[0])
      {
         sprintf(acSpouse, " & %s ", acSF);
         if (acSM[0])
         {
            if (strcmp(acSM, acOL))
            {
               char *pTmp;

               pTmp = strstr(acSM, acOL);
               if (pTmp && pTmp != &acSM[0])
               {
                  *(--pTmp) = 0;
                  iFlg = 1;            // ignore spouse last name
               }
               strcat(acSpouse, acSM);
               strcat(acSpouse, " ");
            } else
            {
               iFlg = 1;               // ignore spouse last name
               acSM[0] = 0;
            }
         }
      } else
         strcpy(acSpouse, " ");

      if (acOL[0] > ' ')
      {
         // Make Owner1
         if (acOM[0])
            sprintf(strOwners, "%s %s %s%s", acOL, acOF, acOM, acSpouse);
         else
            sprintf(strOwners, "%s %s%s", acOL, acOF, acSpouse);

         // Add spouse to swap name - 02/14/2016
         if (!strSwapName[0] || isNumIncluded(strSwapName) || !isalpha(strSwapName[0]) || strlen(acOL) == 1)
         {
            strcpy(strSwapName, pNames);
            iFlg = -1;
         } else
         {
            strcat(acSpouse, acOL);
            strcat(strSwapName, acSpouse);
            blankRem(strSwapName);
         }
      } else
      {
         strcpy(strSwapName, pNames);
         strcpy(strOwners, pNames);
         iFlg = -1;
      }
   }

   strncpy(pOwners->acName1, strOwners, 63);
   strncpy(pOwners->acName2, strOwner2, 63);
   strncpy(pOwners->acSwapName, strSwapName, 63);

   strcpy(pOwners->acOF, acOF);
   acOM[sizeof(pOwners->acOM)-1] = 0;
   strcpy(pOwners->acOM, acOM);
   strcpy(pOwners->acOL, acOL);
   strcpy(pOwners->acSF, acSF);
   acSM[sizeof(pOwners->acOM)-1] = 0;
   strcpy(pOwners->acSM, acSM);
   strcpy(pOwners->acSL, acSL);
   strcpy(pOwners->acTitle, acTitle);

   if (acTitle[0])
   {
      char *pTmp;
      pTmp = strchr(pOwners->acName1, '&');
      if (pTmp)
      {
         *--pTmp = 0;
         if (acTitle[0] == '1')
            sprintf(strOwners, "%s %s %s", pOwners->acName1, &acTitle[1], ++pTmp);
         else if (acTitle[0] == '2')
            sprintf(strOwners, "%s %s %s", pOwners->acName1, ++pTmp, &acTitle[1]);
         else
            sprintf(strOwners, "%s %s %s", pOwners->acName1, acTitle, ++pTmp);
         strncpy(pOwners->acName1, strOwners, 63);
      } else
      {
         if (pOwners->acName1[strlen(pOwners->acName1)-1] != ' ')
            strcat(pOwners->acName1, " ");

         if (acTitle[0] <= '2')
            strcat(pOwners->acName1, &acTitle[1]);
         else
            strcat(pOwners->acName1, acTitle);
      }
   }
   return iFlg;
}

/******************************************************************************
 *
 * This function swap first name first to last name first or last name 
 * first to first name first base on flag.  This function does not process
 * vesting, title, trust.  It is purely swap name.
 *
 * iFlag = 0   ; input name is last name first (default)
 *       = 1   ; input name is first name first (to be implemented)
 *
 * Return pointer to swapped name if successful, otherwise return NULL.
 *
 ******************************************************************************/

char *swapName(char *pInName, char *pOutName, int iFlag)
{
   char  acName[128], *pTmp, *pRet = NULL;

   *pOutName = 0;
   strcpy(acName, pInName);
   myTrim(acName);
   if (!iFlag)
   {
      pTmp = strchr(acName, ' ');
      if (pTmp)
      {
         *pTmp++ = 0;
         strcpy(pOutName, pTmp);
         strcat(pOutName, " ");
         strcat(pOutName, acName);
      } else
         strcpy(pOutName, acName);
      pRet = pOutName;
   }

   return pRet;
}