#ifndef _MERGESCR_H_
#define _MERGESCR_H_ 1

// SCR county definition
#define   SIZ_SCR_APN         11
#define   SIZ_SCR_TRA         5
#define   SIZ_SCR_NAME1       48
#define   SIZ_SCR_NAME2       48
#define   SIZ_SCR_CAREOF      48
#define   SIZ_SCR_MAILADDR    28
#define   SIZ_SCR_MAILCITY    25
#define   SIZ_SCR_MAILSTATE   2
#define   SIZ_SCR_MAILZIP     5
#define   SIZ_SCR_USECODE     3
#define   SIZ_SCR_LAND_CODE   2
#define   SIZ_SCR_LAND        11
#define   SIZ_SCR_IMPR        11
#define   SIZ_SCR_PP_VAL      11
#define   SIZ_SCR_ME_VAL      11
#define   SIZ_SCR_EXEMP_AMT   11
#define   SIZ_SCR_EXEMP_CODE  1
#define   SIZ_SCR_SUPP_FLAG   1
#define   SIZ_SCR_S_STRNAME   20
#define   SIZ_SCR_S_STRNUM    7
#define   SIZ_SCR_S_FRACT     3
#define   SIZ_SCR_S_UNIT      7
#define   SIZ_SCR_S_CITY      13
#define   SIZ_SCR_S_FLAG      1
#define   SIZ_SCR_S_ZIP       5
#define   SIZ_SCR_S_ZIP4      4
#define   SIZ_SCR_FILLER      21

#define   OFF_SCR_APN         1
#define   OFF_SCR_TRA         12
#define   OFF_SCR_NAME1       17
#define   OFF_SCR_NAME2       65
#define   OFF_SCR_CAREOF      113
#define   OFF_SCR_MAILADDR    161
#define   OFF_SCR_MAILCITY    189
#define   OFF_SCR_MAILSTATE   214
#define   OFF_SCR_MAILZIP     216
#define   OFF_SCR_USECODE     221
#define   OFF_SCR_LAND_CODE   224
#define   OFF_SCR_LAND        226
#define   OFF_SCR_IMPR        237
#define   OFF_SCR_PP_VAL      248
#define   OFF_SCR_ME_VAL      259
#define   OFF_SCR_EXEMP_AMT   270
// H=Home owner, R=Religious, W=Welfare/School, F=Museum
// X=??, C=Church
#define   OFF_SCR_EXEMP_CODE  281
#define   OFF_SCR_SUPP_FLAG   282
#define   OFF_SCR_S_STRNAME   283
#define   OFF_SCR_S_STRNUM    303
#define   OFF_SCR_S_FRACT     310
#define   OFF_SCR_S_UNIT      313
#define   OFF_SCR_S_CITY      320
#define   OFF_SCR_S_FLAG      333
#define   OFF_SCR_S_ZIP       334
#define   OFF_SCR_S_ZIP4      339
#define   OFF_SCR_FILLER      343

typedef struct _tScrRoll
{
   char  Apn[SIZ_SCR_APN];
   char  TRA[SIZ_SCR_TRA];
   char  Name1[SIZ_SCR_NAME1];
   char  Name2[SIZ_SCR_NAME2];
   char  CareOf[SIZ_SCR_CAREOF];
   char  M_Addr1[SIZ_SCR_MAILADDR];
   char  M_City[SIZ_SCR_MAILCITY];
   char  M_St[SIZ_SCR_MAILSTATE];
   char  M_Zip[SIZ_SCR_MAILZIP];
   char  UseCode[SIZ_SCR_USECODE];
   char  LandCode[SIZ_SCR_LAND_CODE];
   char  Land[SIZ_SCR_LAND];
   char  Impr[SIZ_SCR_IMPR];
   char  PP_Val[SIZ_SCR_PP_VAL];             // Personal property exempt
   char  ME_Val[SIZ_SCR_ME_VAL];             // Fixed machinery & equiptment exempt
   char  HOEX[SIZ_SCR_EXEMP_AMT];
   char  ExeCode;
   char  Supp_Flag;
   char  S_StreetName[SIZ_SCR_S_STRNAME];    // May contain dir, name, sfx, unit#
   char  S_StreetNum[SIZ_SCR_S_STRNUM];
   char  S_StreetFract[SIZ_SCR_S_FRACT];
   char  S_Unit[SIZ_SCR_S_UNIT];
   char  S_City[SIZ_SCR_S_CITY];
   char  S_Flag;
   char  S_Zip[SIZ_SCR_S_ZIP];
   char  S_Zip4[SIZ_SCR_S_ZIP4];
   char  filler[SIZ_SCR_FILLER];
   char  crlf[2];
} SCR_ROLL;

#define  SOFF_SCR_APN            0
//#define  SOFF_SCR_UNUSED         11
#define  SOFF_SCR_PRICE          21
#define  SOFF_SCR_CONF_PRICE     32
#define  SOFF_SCR_FULL_LEIN_FLAG 43
#define  SOFF_SCR_DEED_TYPE      44
#define  SOFF_SCR_DOCDATE        46
#define  SOFF_SCR_REAPP_CODE     54
#define  SOFF_SCR_PREV_OWNER     56
#define  SOFF_SCR_PREV_OWNPCT    104
#define  SOFF_SCR_PREV_VEST      108
#define  SOFF_SCR_COO_FLAG       110
#define  SOFF_SCR_OWNPCT         111
#define  SOFF_SCR_TRANS_DATE     116
//#define  SOFF_SCR_UNUSED         124
#define  SOFF_SCR_ENR_FLAG       125
#define  SOFF_SCR_NOPRCL_XFER    126
#define  SOFF_SCR_DOCNUM         129
//#define  SOFF_SCR_UNUSED         141

#define  SSIZ_SCR_APN            11
//#define  SSIZ_SCR_UNUSED         10
#define  SSIZ_SCR_PRICE          11
#define  SSIZ_SCR_CONF_PRICE     11
#define  SSIZ_SCR_FULL_LEIN_FLAG 1
#define  SSIZ_SCR_DEED_TYPE      2
#define  SSIZ_SCR_DOCDATE        8
#define  SSIZ_SCR_REAPP_CODE     2
#define  SSIZ_SCR_PREV_OWNER     48
#define  SSIZ_SCR_PREV_OWNPCT    4
#define  SSIZ_SCR_PREV_VEST      2
#define  SSIZ_SCR_COO_FLAG       1
#define  SSIZ_SCR_OWNPCT         5
#define  SSIZ_SCR_TRANS_DATE     8
//#define  SSIZ_SCR_UNUSED         1
#define  SSIZ_SCR_ENR_FLAG       1
#define  SSIZ_SCR_NOPRCL_XFER    3
#define  SSIZ_SCR_DOCNUM         12
//#define  SSIZ_SCR_UNUSED         19

// New layout 
// SCC_ParcelQuest_Get_AssessmentValues_Exemptions.txt
// Pin|TaxYear|Land|Improvements|Adjusted Net Taxable Value|Net Taxable Value|ExemptionName|ExemptionAmount
/*
#define  SCR_V_PIN            0
#define  SCR_V_TAXYEAR        1
#define  SCR_V_LAND           2
#define  SCR_V_IMPR           3
#define  SCR_V_ADJ_VALUE      4
#define  SCR_V_NET_VALUE      5
#define  SCR_V_EXE_NAME       6
#define  SCR_V_EXE_AMT        7
*/
// 2013 LDR
// PIN|AIN|TaxYear|RollCaste|RevenueObjectType|RevenueObjectDescription|Land|Improvements|Personal|
// LivingImprovements|Adjusted Net Taxable Value|Net Taxable Value|ExemptionName|ExemptionAmount
//#define  SCR_V_PIN            0
//#define  SCR_V_AIN            1
//#define  SCR_V_TAXYEAR        2
//#define  SCR_V_ROLLTYPE       3
//#define  SCR_V_OBJTYPE        4
//#define  SCR_V_OBJDESC        5
//#define  SCR_V_LAND           6
//#define  SCR_V_IMPR           7
//#define  SCR_V_PERSONAL       8
//#define  SCR_V_LIVINGIMPR     9
//#define  SCR_V_ADJ_VALUE      10
//#define  SCR_V_NET_VALUE      11
//#define  SCR_V_EXE_NAME       12
//#define  SCR_V_EXE_AMT        13

// 2024 LDR - SCC_ParcelQuest_Get_AssessmentValues_Exemptions.txt
#define  SCR_L_PIN            0
#define  SCR_L_AIN            1
#define  SCR_L_TAXYEAR        2
#define  SCR_L_ROLLCASTE      3
#define  SCR_L_REVOBJTYPE     4
#define  SCR_L_REVOBJDESC     5
#define  SCR_L_ASMTTYPE       6
#define  SCR_L_LAND           7
#define  SCR_L_IMPR           8
#define  SCR_L_PERSPROP       9
#define  SCR_L_LIVIMPR        10
#define  SCR_L_ADJNET         11
#define  SCR_L_NET            12
#define  SCR_L_EXENAME        13
#define  SCR_L_EXEAMT         14
#define  SCR_L_FLDS           15

// 2023 LDR - SCC_ParcelQuest_Get_AssessmentValues_Exemptions.txt
//#define  SCR_L_PIN            0
//#define  SCR_L_AIN            1
//#define  SCR_L_REVOBJID       2
//#define  SCR_L_REVOBJTYPE     3
//#define  SCR_L_REVOBJDESC     4
//#define  SCR_L_TAXYEAR        5
//#define  SCR_L_ROLLCASTE      6
//#define  SCR_L_ASMTID         7
//#define  SCR_L_ASMTEVENTID    8
//#define  SCR_L_ASMTTRANID     9
//#define  SCR_L_ASMTTYPE       10
//#define  SCR_L_ASMTTRANTYPE   11
//#define  SCR_L_ASMTTRANSTATUS 12
//#define  SCR_L_CLASSCATSDESC  13
//#define  SCR_L_TAXTYPESDESC   14
//#define  SCR_L_VTID           15
//#define  SCR_L_VTSHORTDESC    16
//#define  SCR_L_VTDESC         17
//#define  SCR_L_VTVALUETYPE    18
//#define  SCR_L_VTATTRTYPE1    19
//#define  SCR_L_AVATTRIBUTE1   20
//#define  SCR_L_DESC           21
//#define  SCR_L_AMOUNT         22
//#define  SCR_L_FLDS           23

// Mapping fields for RevenueObjectType='annual' and RollCasteSDescr = 'Secured'
#define  SCR_X_PIN            0
#define  SCR_X_AIN            1
#define  SCR_X_TAXYEAR        2
#define  SCR_X_REVOBJTYPE     3
#define  SCR_X_REVOBJDESC     4
#define  SCR_X_TAXYEARX       5
#define  SCR_X_ROLLCASTE      6
#define  SCR_X_LAND           7
#define  SCR_X_IMPR           8
#define  SCR_X_OTHIMPR        9
#define  SCR_X_LIVIMPR        10
#define  SCR_X_GROSS          11    // Do not use this field
#define  SCR_X_NET            12
#define  SCR_X_CLASSCATSDESC  13
#define  SCR_X_EXEAMT         14
#define  SCR_X_FLDS           15

// 2017 LDR
// PIN|AIN|TaxYear|RollCaste|RevenueObjectType|RevenueObjectDescription|AssessmentType|Land|Improvements|Personal|
// LivingImprovements|Adjusted Net Taxable Value|Net Taxable Value|ExemptionName|ExemptionAmount
#define  SCR_V_PIN            0
#define  SCR_V_AIN            1
#define  SCR_V_TAXYEAR        2
#define  SCR_V_ROLLTYPE       3
#define  SCR_V_OBJTYPE        4
#define  SCR_V_OBJDESC        5
#define  SCR_V_TYPE           6
#define  SCR_V_LAND           7
#define  SCR_V_IMPR           8
#define  SCR_V_PERSONAL       9
#define  SCR_V_LIVINGIMPR     10
#define  SCR_V_ADJ_VALUE      11
#define  SCR_V_NET_VALUE      12
#define  SCR_V_EXE_NAME       13
#define  SCR_V_EXE_AMT        14

// SCC_ParcelQuest_Get_Characteristics.txt
// Strap|BldgNumber|BldgYearBuilt|BldgEffectiveYear|BldgQuality|ParcelYearCreated|BldgMainArea|BldgClass|ParcelSizeAcreage|
// ParcelSizeSqFt|SpecialTaxTreatment|NumberOfUnits|Concrete|RoomCount|Bedrooms|FullBath|HalfBath|FirePlaces|Pool|Spa|Stories|
// M&S_Rank|Lavatories|M&S_Shape|Carport|Deck|Condition|Roof|Heat|HVAC|ResidentialShape|HasOutBuildings|GPLandUse|Zoning|View|
// Slope|Constraints|Water|Sanitation|Garage
#define  SCR_C_APN            0
#define  SCR_C_BLDGNUM        1
#define  SCR_C_YRBLT          2
#define  SCR_C_EFFYR          3
#define  SCR_C_BLDGQUAL       4
#define  SCR_C_YEARCREATED    5
#define  SCR_C_BLDGSQFT       6
#define  SCR_C_BLDGCLS        7
#define  SCR_C_LOTACRE        8
#define  SCR_C_LOTSQFT        9
#define  SCR_C_SPCTAXTM       10
#define  SCR_C_UNITS          11
#define  SCR_C_CONCRETE       12
#define  SCR_C_ROOMS          13
#define  SCR_C_BEDROOMS       14
#define  SCR_C_FULLBATH       15
#define  SCR_C_HALFBATH       16
#define  SCR_C_FIREPLACES     17
#define  SCR_C_POOL           18
#define  SCR_C_SPA            19
#define  SCR_C_STORIES        20
#define  SCR_C_MS_RANK        21
#define  SCR_C_LAVASTORIES    22
#define  SCR_C_MS_SHAPE       23
#define  SCR_C_CARSQFT        24
#define  SCR_C_DECK           25
#define  SCR_C_CONDITION      26
#define  SCR_C_ROOF           27
#define  SCR_C_HEAT           28
#define  SCR_C_HVAC           29
#define  SCR_C_RES_SHAPE      30
#define  SCR_C_OTHERBLDGS     31
#define  SCR_C_GP_LANDUSE     32
#define  SCR_C_ZONING         33
#define  SCR_C_VIEW           34
#define  SCR_C_TOPOGRAPHY     35
#define  SCR_C_CONSTRAINTS    36
#define  SCR_C_WATER          37
#define  SCR_C_SANITATION     38
#define  SCR_C_GARSQFT        39
#define  SCR_C_SUBAREASIZE    40    // 07/31/2023
#define  SCR_C_FLDS           40

static XLAT_CODE  asHeating[] =
{
   // Value, lookup code, value length
   "B",  "8", 1,              // Baseboard
   "C",  "Z", 1,              // Central
   "FL", "C", 2,              // Floor
   "FO", "B", 2,              // Forced air
   "HE", "G", 2,              // Heat Pump
   "O",  "X", 1,              // Other
   "R",  "I", 1,              // Radiant
   "W",  "D", 1,              // Wall
   "Z",  "T", 1,              // Zone
   "",   "",  0
};

static XLAT_CODE  asCooling[] =
{
   // Value, lookup code, value length
   "C",  "3", 1,              // Complete Hvac
   "E",  "Q", 1,              // Electric 
   "FL", "M", 2,              // Floor
   "FO", "4", 2,              // Forced air
   "HE", "H", 2,              // Heat Pump
   "N",  "N", 1,              // None
   "P",  "D", 1,              // Package Unit
   "S",  "X", 1,              // Space Heaters
   "V",  "X", 1,              // Ventilation
   "WAL","L", 3,              // Wall
   "WAR","X", 3,              // Warmed And Cooled Air
   "",   "",  0
};

// SCC_GetDefaultedRedeemedInfoForTaxServices.txt
// PIN|TaxYear|RollType|AssessmentType|BillType|FirstInstallmentAmount|FirstInstallmentDueDate|FirstInstallmentPaymentDate|FirstInstallmentDefaultDate|FirstInstallmentDefaulted|FirstInstallmentRedeemed|FirstInstallmentPenalty|SecondInstallmentAmount|SecondInstallmentDueDate|SecondInstallmentPaymentDate|SecondInstallmentDefaultDate|SecondInstallmentDefaulted|SecondInstallmentRedeemed|SecondInstallmentPenalty|RedemptionPaymentPlanFlag|EscapePaymentPlanFlag|PowerToSellFlag|TotalAmountRequiredToRedeemPIN|PmtPlanPIN|PmtPlanLegalParty|PmtPlanNumber|PmtPlanStatus|PmtPlanStartDate|MostRecentPmtPlanPaymentDate|MostRecentPmtPlanPaymentAmount|PrimaryOwnerRecipient|PrimaryOwnerDeliveryAddress|PrimaryOwnerLastLine|PrimaryOwnerCountry|PrimaryOwnerCity|PrimaryOwnerState|PrimaryOwnerPostalCd|PrimaryOwnerStreetNumber|PrimaryOwnerPredirectional|PrimaryOwnerStreetName|PrimaryOwnerStreetType|PrimaryOwnerPostdirectional|PrimaryOwnerSecondAddrUnit|PrimaryOwnerSecondAddrRange|SitusFullAddress
#define  SCR_D_PIN                        0
#define  SCR_D_TAXYEAR                    1
#define  SCR_D_ROLLTYPE                   2
#define  SCR_D_ASSESSMENTTYPE             3
#define  SCR_D_BILLTYPE                   4
#define  SCR_D_FIRSTINSTAMOUNT            5
#define  SCR_D_FIRSTINSTDUEDATE           6
#define  SCR_D_FIRSTINSTPAYMENTDATE       7
#define  SCR_D_FIRSTINSTDEFAULTDATE       8
#define  SCR_D_FIRSTINSTDEFAULTED         9
#define  SCR_D_FIRSTINSTREDEEMED          10
#define  SCR_D_FIRSTINSTPENALTY           11
#define  SCR_D_SECONDINSTAMOUNT           12
#define  SCR_D_SECONDINSTDUEDATE          13
#define  SCR_D_SECONDINSTPAYMENTDATE      14
#define  SCR_D_SECONDINSTDEFAULTDATE      15
#define  SCR_D_SECONDINSTDEFAULTED        16
#define  SCR_D_SECONDINSTREDEEMED         17
#define  SCR_D_SECONDINSTPENALTY          18
#define  SCR_D_REDEMPTIONPMTPLANFLAG      19
#define  SCR_D_ESCAPEPMTPLANFLAG          20
#define  SCR_D_POWERTOSELLFLAG            21
#define  SCR_D_TOTALAMTREQTOREDEEMPIN     22
#define  SCR_D_PMTPLANPIN                 23
#define  SCR_D_PMTPLANLEGALPARTY          24
#define  SCR_D_PMTPLANNUMBER              25
#define  SCR_D_PMTPLANSTATUS              26
#define  SCR_D_PMTPLANSTARTDATE           27
#define  SCR_D_MOSTRECENTPAYMENTDATE      28
#define  SCR_D_MOSTRECENTPAYMENTAMOUNT    29
#define  SCR_D_PRIMARYOWNERRECIPIENT      30
#define  SCR_D_PRIMARYOWNERMAILADDR       31
#define  SCR_D_PRIMARYOWNERLASTLINE       32
#define  SCR_D_PRIMARYOWNERCOUNTRY        33
#define  SCR_D_PRIMARYOWNERCITY           34
#define  SCR_D_PRIMARYOWNERSTATE          35
#define  SCR_D_PRIMARYOWNERZIPCODE        36
#define  SCR_D_PRIMARYOWNERSTRNUM         37
#define  SCR_D_PRIMARYOWNERPREDIR         38
#define  SCR_D_PRIMARYOWNERSTRNAME        39
#define  SCR_D_PRIMARYOWNERSTRTYPE        40
#define  SCR_D_PRIMARYOWNERPOSTDIR        41
#define  SCR_D_PRIMARYOWNER2NDADDRUNIT    42
#define  SCR_D_PRIMARYOWNER2NDADDRRANGE   43
#define  SCR_D_SITUSFULLADDR              44

// SCC_ParcelQuest_Get_Etals.txt
#define  SCR_N_PIN            0
#define  SCR_N_NAMES          1

// SCC_ParcelQuest_Get_ParcelStatus.txt
#define  SCR_P_PIN            0
#define  SCR_P_STATUS         1  // A & I
#define  SCR_P_PUBLIC         2  // 1 or 0


// SCC_ParcelQuest_Get_PrimaryOwner_TRA_UseCode_LegalDescr.txt
// Original:  Pin|TaxRateArea|UseCode|LegalDescription|PrimaryOwner
// 8/22/2013: Pin|RevenueObjectType|TaxRateArea|UseCode|LegalDescription|PrimaryOwner

#define  SCR_LE_PIN           0
#define  SCR_LE_TYPE          1
#define  SCR_LE_TRA           2
#define  SCR_LE_USECODE       3
#define  SCR_LE_LEGAL         4
#define  SCR_LE_PRIOWNER      5

// SCC_ParcelQuest_Get_PrimaryOwnerMailingAddress.txt
// PIN|Recipient|DeliveryAddress|LastLine|Country|City|State|PostalCd|StreetNumber|Predirectional|StreetName|StreetType|Postdirectional|SecondAddrUnit|SecondAddrRange
#define  SCR_M_PIN            0
#define  SCR_M_RECIPIENT      1
#define  SCR_M_DELIVERYADDR   2
#define  SCR_M_ATTN           3     // Added on 03/05/2013
#define  SCR_M_LASTLINE       4
#define  SCR_M_COUNTRY        5
#define  SCR_M_CITY           6
#define  SCR_M_STATE          7
#define  SCR_M_POSTALCD       8
#define  SCR_M_STRNUM         9
#define  SCR_M_PRE_DIR        10
#define  SCR_M_STRNAME        11
#define  SCR_M_STRTYPE        12
#define  SCR_M_POSTDIR        13
#define  SCR_M_2ND_ADDRUNIT   14
#define  SCR_M_2ND_ADDRRANGE  15

// SCC_ParcelQuest_Get_SalesTransfers.txt
// DocNumber|PIN|BegEffDate|DocDate|DeedType|IsLegalPartyGrantor|LegalPartyName|Consideration|Reappraisability
#define  SCR_S_DOCNUMBER      0
#define  SCR_S_PIN            1
#define  SCR_S_BEGEFFDATE     2
#define  SCR_S_DOCDATE        3
#define  SCR_S_DOCTYPE        4
#define  SCR_S_ISGRANTOR      5
#define  SCR_S_NAME           6
#define  SCR_S_CONSIDERATION  7
#define  SCR_S_REAPPRAISE     8

// SCC_ParcelQuest_Get_SitusAddresses.txt
// PIN|StreetNumber|StreetNumberSfx|Predirectional|StreetName|StreetType|Postdirectional|UnitNumber|City|State|PostalCd|UnitType|ClassCdLDescr|Roll
#define  SCR_A_PIN            0
#define  SCR_A_STRNUM         1
#define  SCR_A_STRSUB         2
#define  SCR_A_PREDIR         3
#define  SCR_A_STRNAME        4
#define  SCR_A_STRTYPE        5
#define  SCR_A_POSTDIR        6
#define  SCR_A_UNITNUM        7
#define  SCR_A_CITY           8
#define  SCR_A_STATE          9
#define  SCR_A_POSTALCD       10
#define  SCR_A_UNITTYPE       11
#define  SCR_A_CDLDESCR       12
#define  SCR_A_ROLLTYPE       13

// SCC_ParcelQuest_Get_TaxYearTaxesDue_Paid.txt  
/*
#define  SCR_T_APN                     0
#define  SCR_T_TAXYEAR                 1
#define  SCR_T_FIRSTINSTAMOUNT         2
#define  SCR_T_FIRSTINSTPENALTY        3
#define  SCR_T_FIRSTINSTDEFAULTED      4
#define  SCR_T_FIRSTINSTDUEDATE        5
#define  SCR_T_FIRSTINSTPAYMENTDATE    6
#define  SCR_T_SECONDINSTAMOUNT        7
#define  SCR_T_SECONDINSTPENALTY       8
#define  SCR_T_SECONDINSTDEFAULTED     9
#define  SCR_T_SECONDINSTDUEDATE       10
#define  SCR_T_SECONDINSTPAYMENTDATE   11
#define  SCR_T_ROLLTYPELDESCR          12
#define  SCR_T_ASMTTYPELDESCR          13
#define  SCR_T_FLAG                    14
#define  SCR_T_BILLTYPE                15
*/
// SCC_GetAnnualRollForTaxServices.txt
#define  SCR_T_APN                              0
#define  SCR_T_TAXYEAR                          1
#define  SCR_T_ROLLTYPE                         2
#define  SCR_T_ASSESSMENTTYPE                   3
#define  SCR_T_BILLTYPE                         4
#define  SCR_T_LANDVALUE                        5
#define  SCR_T_IMPROVEMENTSVALUE                6
#define  SCR_T_FIRSTINSTALLMENTAMOUNT           7
#define  SCR_T_FIRSTINSTALLMENTDUEDATE          8
#define  SCR_T_FIRSTINSTALLMENTPAYMENTDATE      9
#define  SCR_T_FIRSTINSTALLMENTDEFAULTED        10
#define  SCR_T_FIRSTINSTALLMENTPENALTY          11
#define  SCR_T_SECONDINSTALLMENTAMOUNT          12
#define  SCR_T_SECONDINSTALLMENTDUEDATE         13
#define  SCR_T_SECONDINSTALLMENTPAYMENTDATE     14
#define  SCR_T_SECONDINSTALLMENTDEFAULTED       15
#define  SCR_T_SECONDINSTALLMENTPENALTY         16
#define  SCR_T_PRIMARYOWNERRECIPIENT            17
#define  SCR_T_PRIMARYOWNERDELIVERYADDRESS      18
#define  SCR_T_PRIMARYOWNERLASTLINE             19
#define  SCR_T_PRIMARYOWNERCOUNTRY              20
#define  SCR_T_PRIMARYOWNERCITY                 21
#define  SCR_T_PRIMARYOWNERSTATE                22
#define  SCR_T_PRIMARYOWNERPOSTALCD             23
#define  SCR_T_PRIMARYOWNERSTREETNUMBER         24
#define  SCR_T_PRIMARYOWNERPREDIRECTIONAL       25
#define  SCR_T_PRIMARYOWNERSTREETNAME           26
#define  SCR_T_PRIMARYOWNERSTREETTYPE           27
#define  SCR_T_PRIMARYOWNERPOSTDIRECTIONAL      28
#define  SCR_T_PRIMARYOWNERSECONDADDRUNIT       29
#define  SCR_T_PRIMARYOWNERSECONDADDRRANGE      30
#define  SCR_T_SITUSFULLADDRESS                 31

// SCC_GetDefaultedRedeemedInfoForTaxServices.txt
#define  SCR_DR_APN                             0
#define  SCR_DR_TAXYEAR                         1
#define  SCR_DR_ROLLTYPE                        2     // Real/State Assessed Real/Manufactured Home
#define  SCR_DR_ASSESSMENTTYPE                  3
#define  SCR_DR_BILLTYPE                        4     // Annual/Supplemental/Transfer/Escaped Assessment
#define  SCR_DR_FIRSTINSTALLMENTAMOUNT          5
#define  SCR_DR_FIRSTINSTALLMENTDUEDATE         6
#define  SCR_DR_FIRSTINSTALLMENTPAYMENTDATE     7
#define  SCR_DR_FIRSTINSTALLMENTDEFAULTDATE     8 
#define  SCR_DR_FIRSTINSTALLMENTDEFAULTED       9 
#define  SCR_DR_FIRSTINSTALLMENTREDEEMED        10
#define  SCR_DR_FIRSTINSTALLMENTPENALTY         11
#define  SCR_DR_SECONDINSTALLMENTAMOUNT         12
#define  SCR_DR_SECONDINSTALLMENTDUEDATE        13
#define  SCR_DR_SECONDINSTALLMENTPAYMENTDATE    14
#define  SCR_DR_SECONDINSTALLMENTDEFAULTDATE    15
#define  SCR_DR_SECONDINSTALLMENTDEFAULTED      16
#define  SCR_DR_SECONDINSTALLMENTREDEEMED       17
#define  SCR_DR_SECONDINSTALLMENTPENALTY        18
#define  SCR_DR_REDEMPTIONPAYMENTPLANFLAG       19
#define  SCR_DR_ESCAPEPAYMENTPLANFLAG           20
#define  SCR_DR_POWERTOSELLFLAG                 21
#define  SCR_DR_TOTALAMOUNTREQUIREDTOREDEEM     22
#define  SCR_DR_PMTPLANPIN                      23
#define  SCR_DR_PMTPLANLEGALPARTY               24
#define  SCR_DR_PMTPLANNUMBER                   25
#define  SCR_DR_PMTPLANSTATUS                   26    // Active/Defaulted/Fulfilled
#define  SCR_DR_PMTPLANSTARTDATE                27
#define  SCR_DR_MOSTRECENTPMTPLANPAYMENTDATE    28
#define  SCR_DR_MOSTRECENTPMTPLANPAYMENTAMOUNT  29
#define  SCR_DR_PRIMARYOWNERRECIPIENT           30
#define  SCR_DR_PRIMARYOWNERDELIVERYADDRESS     31
#define  SCR_DR_PRIMARYOWNERLASTLINE            32
#define  SCR_DR_PRIMARYOWNERCOUNTRY             33
#define  SCR_DR_PRIMARYOWNERCITY                34
#define  SCR_DR_PRIMARYOWNERSTATE               35
#define  SCR_DR_PRIMARYOWNERPOSTALCD            36
#define  SCR_DR_PRIMARYOWNERSTREETNUMBER        37
#define  SCR_DR_PRIMARYOWNERPREDIRECTIONAL      38
#define  SCR_DR_PRIMARYOWNERSTREETNAME          39
#define  SCR_DR_PRIMARYOWNERSTREETTYPE          40
#define  SCR_DR_PRIMARYOWNERPOSTDIRECTIONAL     41
#define  SCR_DR_PRIMARYOWNERSECONDADDRUNIT      42
#define  SCR_DR_PRIMARYOWNERSECONDADDRRANGE     43
#define  SCR_DR_SITUSFULLADDRESS                44

#define  SCR_T_TAG_SHORT                        1
#define  SCR_T_TAG_LONG                         2
#define  SCR_T_INSTALLMENT                      3
#define  SCR_T_TAG_DESC                         4
#define  SCR_T_FUND_CODE                        5
#define  SCR_T_FUND_DESC                        6
#define  SCR_T_TAXRATE                          7
#define  SCR_T_TAXAMT                           8

// Standard codes
// H=Homeowners
// D=Disabled Veterans
// V=Veterans
// C=Church
// L=Library
// M=Museums
// R=Religion
// T=Tribal Housing
// W=Welfare
// U=College
// P=Public School
// E=Cemetery
// G=Government
// I=Hospital
// S=School
// X=Misc exempt
// Y=Military

IDX_TBL4 SCR_Exemption[] = 
{
   "HO","H", 2,1,    // Homeowners Exemption
   "CE","E", 2,1,    // Cemetery Exemption
   "CH","C", 2,1,    // Church Exemption
   "CO","U", 2,1,    // College Exemption
   "DV","D", 2,1,    // Disabled Veterans Exemption
   "MU","M", 2,1,    // Free Public Library/Museum Exemption
   "LV","X", 2,1,    // Low Value Parcel
   "SC","P", 2,1,    // Public School Exemption
   "RE","R", 2,1,    // Religious Exemption
   "WE","W", 2,1,    // Welfare Exemption
   "","",0,0
};

#endif