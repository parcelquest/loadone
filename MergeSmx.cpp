/******************************************************************************
 *
 * Input files:
 *    - SECMSTR.TXT              (roll file, 810 bytes ASCII)
 *    - RESIDENT.TXT             (char for SFR 330-byte ASCII)
 *    - APARTMENT.TXT            (char for APT 360-byte ASCII)
 *    - SALEHIST.TXT             (sale file, 65-byte ASCII)
 *    - VACANT.TXT               (Vacant Land 60-bytes ASCII)
 *
 * Tax files as of 04/01/2021
 *    - SC - Secure Roll Data #1 - Accounts.csv
 *    - SC - Secure Roll Data #2 - Installments.csv
 *    - SC - Secure Roll Data #3 - Districts.csv
 *    - SC - Secure Roll Data Transferable - Installments.csv (currently not used)
 *    - SC - Supp Roll Data #1 - Accounts.csv
 *    - SC - Supp Roll Data #2 - Installments.csv
 *    - SC - Supp Roll Data #3 - Supp Info.csv (currently not used)
 *    - SC - Redempt Roll Data File #1.csv
 *    - SC - Redemptions Roll Data #2B.csv
 *    - SC - Redempt Roll Data #3 - Pmnt Plans.csv
 *
 * Oprating tasks:
 *    - Normal update: LoadOne -U -O -CSMX -Usi [-Lc]
 *    - Load Lien: LoadOne -L -O -CSMX [-Ms|-Usi] [-Lc] 
 *
 * Notes:
 *    - Data not used: BASEMENT AREA, LOT DIMENSION, OTHER ROOMS
 *
 * Revision:
 * 06/20/2006 1.0       TCB
 * 07/17/2006 1.2.27.1  SPN Ready for production
 * 11/03/2006 1.3.4     SPN Fix situs street name bug
 * 11/26/2006 1.3.5     SPN Adding new characteristic data from various files.
 * 04/30/2007 1.4.12    TCB Add Vacant Land.
 * 05/01/2007 1.4.12.1  SPN Fix Smx_MergeVac() and sort vacant.txt before processing
 * 02/14/2008 1.5.5     Adding support for Standard Usecode
 * 03/24/2008 1.5.7.2   Format IMAPLINK.
 * 11/19/2008 8.4.5     Fix Smx_ProcessIndustrial() to correct number of parking spaces.
 * 12/11/2008 8.5.1     Make StrNum left justified
 * 12/22/2008 8.5.3     Fix situs StrNum problem.
 * 03/16/2009 8.7.0     Format STORIES as 99.0.  Divide stories by 10.    
 *                      Populate owner name as is.  Remove ET AL and merge same last name only.
 * 05/11/2009 8.8       Set full exemption flag
 * 07/13/2009 9.1.2     Add option to extract lien and populate other values to R01 rec.
 * 02/09/2010 9.4.1     Fix Parking Type in Smx_ProcessSfr()
 * 02/18/2010 9.4.2     Merge long LEGAL into LEGAL1 & LEGAL2 (max 216).
 * 07/15/2010 10.1.3    Rename CreateSmxRoll() & MergeSmxRoll() to Smx_Load_LDR() & Smx_Load_Roll().
 * 08/10/2010 10.1.10   Modify Smx_MergeRoll() to add EXE_CD to R01 record.
 * 09/15/2010 10.1.12   Modify Smx_MergeSAdr() to add Unit# to situs.
 * 10/14/2010 10.1.14   Fix Smx_ProcessApt() and Smx_ProcessSfr() to make sure that no 
 *                      building in SMX taller than 99 stories.  Force EXE_TOTAL to be <= GROSS.
 * 05/16/2011 10.5.10   Fix county UseCode format error Smx_MergeChar() & Smx_MergeRoll().
 * 07/06/2011 11.0.2    Add S_HSENO.  No range StrNum found.
 * 11/04/2011 11.4.15   Add Smx_ExtrSale() & Smx_FormatSale() to build history file with -Xs option.
 * 01/29/2012 11.6.4    Fix CARE_OF in Smx_MergeOwner().
 * 07/11/2013 13.0.3    Remove bad chars in Legal and verify sale record length.
 * 08/06/2013 13.3.10   Modify Smx_ProcessSfr(), remove NAME_TYPE, and use updateCareOf() and logUseCode()
 * 09/11/1013 13.7.14   Add Units, Elevator, BsmtSqft, and BldgClass
 * 10/10/2013 13.10.4.3 Use updateVesting() to update Vesting and Etal flag. Remove CONS_TYPE.
 * 10/29/2014 14.8.0    Fix memory violation in Smx_MergeOwner() & Smx_MergeSAdr().
 * 06/17/2015 14.15.4   Modify Smx_Load_Roll() to skip duplicate APN.  
 *                      Also modify Smx_MergeMAdr() to ignore mail addr with "Situs not found"
 *                      Append Name2 to Name1 if Name2 starts with "OF ".
 * 07/13/2015 15.0.4    Modify Smx_MergeOwner() to remove known bad char in Name2.
 * 01/05/2016 15.4.0    Add load tax option.
 * 02/17/2016 15.6.0    Remove duplicate owner name and fix both situs and mailing CTY_ST_D
 *                      in Smx_MergeSAdr() and Smx_MergeMAdr().
 * 07/13/2016 16.0.5    Modify Smx_Load_Roll() to ignore new records without Owner.  This can fix
 *                      the duplicate entry problem (See apn 021371010).
 * 07/06/2018 18.1.0    Modify SMX_MergeChar() and related functions to use STDCHAR instead of LO_CHAR layout.  
 *                      Rename all old functions that uses LO_CHAR to *x().
 * 07/30/2018 18.2.4    Modify Smx_MergeSAdr() to fix situs UNIT# problem.  Also look into legal for UNIT#
 *                      if legal starts with word "UNIT".
 * 08/15/2018 18.3.1    Fix UNIT# in Smx_MergeSAdr() and Smx_MergeMAdr().  Drop # if needed to retain max digit.
 * 09/21/2018 18.5.0    Modify Smx_ProcessSfr() due to record layout change.
 * 02/18/2019 18.9.5    Modify Smx_Load_Roll() to continue processing if vacant file is not available.
 * 07/13/2020 20.1.5    Remove sale update from Smx_Load_LDR() and Smx_Load_Roll().  Add -Ms option.
 * 04/02/2021 20.7.14   Add Smx_Load_TaxBase(), Smx_Load_TaxItems(), Smx_Load_TaxDelq() to support new tax files.
 * 04/03/2021 20.7.14.2 Add Smx_Load_TaxSupp() to load supplemental tax files.
 * 05/07/2021 20.7.18   Set FirePlace='M' when greater than 9 in Smx_ProcessApt() & Smx_ProcessCondo().
 * 05/26/2021 20.8.1.1  Modify Smx_ParseTaxDetail() to fix tax code issue.
 * 08/19/2021 21.1.5    Fix TRA problem in Smx_UpdateSecAcct().
 * 11/02/2021 21.2.11   Hardcode acreage for APN 080030020 in Smx_ProcessSfr()
 * 01/14/2022 21.4.8    Modify Smx_Load_TaxBase() & Smx_Load_TaxSupp() to move sort cmd to INI file.
 * 03/17/2022 21.8.2    Check for bad char in Smx_MergeOwner().
 * 07/12/2022 22.0.1    Fix known bad char in Smx_MergeOwner().
 * 10/11/2022 22.2.7    If supplemental data is not avail, import base data.
 * 07/17/2023 23.0.5    Modify Smx_ParseTaxSupp() to skip "Preliminary Roll" record.
 * 01/30/2024 23.5.7    Add Smx_ConvStdChar(), Smx_MergeStdChar(), Smx_MergeOwnerCsv(), Smx_MergeSitus(),
 *                      Smx_MergeMailing(), Smx_MergeRollCsv(), Smx_Load_RollCsv(), Smx_ExtrSaleCsv()
 *                      to support new data layout.
 * 02/02/2024 23.6.0    Modify Smx_parseMAdr1(), Smx_MergeMailing() & Smx_MergeSitus() to populate UnitNox.
 * 03/13/2024 23.7.1    Modify Smx_ExtrSaleCsv() to remove '-' from DocNum.  Temporary remove TRANSFER
 *                      if SALE1 present or incomplete TRANSFER_DOC from Smx_MergeRollCsv().
 * 06/16/2024 23.9.3    Force return error when load sale failed for any reason.
 * 06/17/2024 23.9.4    Modify Smx_Load_RollCsv() to use external sort command in INI file.
 * 07/12/2024 24.0.2    Modify Smx_MergeRollCsv() to add Mineral & ExeType.  Add Smx_Load_LDRCsv(),
 *                      Smx_CreateLienRecCsv(), and Smx_ExtrLienCsv() to support new LDR format.
 * 08/22/2024 24.1.2    Modify Smx_MergeMailing() to extend M_CITY to 20 bytes.
 * 08/27/2024 24.1.3    Fix EXE_CODE in Smx_MergeRollCsv(), fix M_ZIP in Smx_MergeMailing().
 * 10/02/2024 24.1.6    Modify Smx_MergeOwnerCsv() to filter out known bad char in owner name.
 * 10/21/2024 24.1.9    Fix bug in Smx_UpdateSecAcct() that may assign wrong tax year to current year 
 *                      record when more than one escape bills applied to the same APN.
 *                      Modify Smx_ParseTaxDetail() to ignore prior year detail bill.
 * 11/07/2024 24.3.2    Uncomment old code to merge all data fields.
 * 12/02/2024 24.4.0    Add Smx_ConvStdChar2() to support new CHAR layout for "Characteristic.csv".
 * 01/23/2025 24.4.7    Modify Smx_MergeRoll(), Smx_MergeRollCsv(), and Smx_ExtrLienCsv() to add FullExeFlg.
 *
 ******************************************************************************/

#include "stdafx.h"
#include "CountyInfo.h"
#include "Prodlib.h"
#include "R01.h"
#include "RecDef.h"
#include "Tables.h"
#include "Logs.h"
#include "LoadDef.h"
#include "Utils.h"
#include "XlatTbls.h"
#include "doSort.h"
#include "doOwner.h"
#include "formatApn.h"
#include "SaleRec.h"
#include "LoadOne.h"
#include "MergeSmx.h"
#include "UseCode.h"
#include "Update.h"
#include "LOExtrn.h"
#include "Tax.h"
#include "CharRec.h"

static   FILE     *fdChar, *fdVac;
static   long     lCharSkip, lSaleSkip, lVacSkip;
static   long     lCharMatch, lSaleMatch, lVacMatch, lLglUnit;

static   int      iUseMailCity;

/********************************* Smx_MergeVac *****************************
 *
 * Use actual sqft.  If not avail, check for usable sqft.
 *
 *****************************************************************************/

int Smx_MergeVac(char *pOutbuf)
{
   static char    acRec[1024], *pRec=NULL;
   char           acTmp[256];
   long           lTmp, lSqft;
   int            iLoop;
   SMX_VAC        *pVac;

   // Get first Vac rec for first call
   if (!pRec)
      pRec = fgets(acRec,  1024, fdVac);

   do
   {
      iLoop = memcmp(pOutbuf, acRec, myCounty.iApnLen);
      if (iLoop > 0)
      {
         if (bDebug)
            LogMsg0("*** Skip Vac: %.9s", acRec);

         lVacSkip++;
         pRec = fgets(acRec,  1024, fdVac);
         if (!pRec)
         {
            fclose(fdVac);
            fdVac = NULL;
            return 1;      // EOF
         }
      }
   } while (iLoop > 0);

   // If not match, return
   if (iLoop)
      return 1;

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "004191410", 9))
   //   iLoop = 0;
#endif

   pVac = (SMX_VAC *)acRec;

   // LotAcre
   lTmp = atoin(pVac->Acres, SMX_SIZ_VAC_ACRES);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%d          ", lTmp);
      memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);
   }

   // If actual sqft is not avail, use usable sqft
   lSqft = atoin(pVac->Act_SQFT, SMX_SIZ_VAC_ACT_SQFT);
   if (!lSqft)
      lSqft = atoin(pVac->Use_SQFT, SMX_SIZ_VAC_USE_SQFT);
   if (lSqft > 0)
   {
      sprintf(acTmp, "%d          ", lSqft);
      memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);
   }

   lVacMatch++;

   // Get next Char rec
   pRec = fgets(acRec,  1024, fdVac);

   return 0;
}

/****************************** Smx_ProcessSfr() ****************************
 *
 * Merge SFR into CHAR file
 * 09/11/2013 - Add Basement (No BldgCls
 *
 ****************************************************************************/

int Smx_ProcessSfr(FILE *fdOut)
{
   FILE     *fdIn;
   char     acChar[1024], acTmp[256], acOutbuf[2048], *pTmp;
   int      iTmp, iCnt=0;
   long     lTmp;
   STDCHAR        *pStdChar = (STDCHAR *)acOutbuf;
   SMX_CHAR_SFR   *pSfrChar = (SMX_CHAR_SFR *)acChar;

   GetIniString(myCounty.acCntyCode, "RESIDENT", "", acTmp, _MAX_PATH, acIniFile);
   LogMsg("Loading %s", acTmp);
   if (!(fdIn = fopen(acTmp, "r")))
   {
      LogMsg("***** Error opening RESIDENT file: %s", acTmp);
      return -1;
   }

   while ((pTmp = fgets(acChar,  1024, fdIn)))
   {
      memset(acOutbuf, ' ', sizeof(STDCHAR));
      memcpy(pStdChar->Apn, pSfrChar->Apn , RES_SIZ_APN);

      // YrBlt
      memcpy(pStdChar->YrBlt, pSfrChar->Constr_Yr, RES_SIZ_CONSTR_YR);

      // Usecode
      if (pSfrChar->UseCode[0] > ' ')
         memcpy(pStdChar->LandUse, pSfrChar->UseCode, RES_SIZ_USE_CD1);

      // Beds
      iTmp = atoin(pSfrChar->Bedrooms, RES_SIZ_BEDROOMS);
      if (iTmp > 0 && iTmp < 100)
      {
         sprintf(acTmp, "%d   ", iTmp);
         memcpy(pStdChar->Beds, acTmp, RES_SIZ_BEDROOMS);
      }

      // Baths
      iTmp = atoin(pSfrChar->Baths, RES_SIZ_BATHS);
      if (iTmp > 0 && iTmp < 100)
      {
         sprintf(acTmp, "%d   ", iTmp);
         memcpy(pStdChar->FBaths, acTmp, RES_SIZ_BATHS);
         memcpy(pStdChar->Bath_4Q, acTmp, RES_SIZ_BATHS);
      }

      // Half bath 500 means 1
      iTmp = atoin(pSfrChar->HalfBaths, RES_SIZ_BATHS);
      if (iTmp > 0)
      {
         // There are 150, 250, 300, 350, 030, 600, 990
         if (iTmp <= 300)
            pStdChar->Bath_1Q[0] = '1';
         else if (iTmp <= 600)
            pStdChar->Bath_2Q[0] = '1';
         else if (iTmp <= 990)
            pStdChar->Bath_3Q[0] = '1';      // APN=077060190
         else 
            pStdChar->Bath_4Q[0] = '1';
         
         pStdChar->HBaths[0] = '1';
      }

      // Rooms
      iTmp = atoin(pSfrChar->Rooms, RES_SIZ_ROOMS);
      if (iTmp > 0)
      {
         sprintf(acTmp, "%d    ", iTmp);
         memcpy(pStdChar->Rooms, acTmp, RES_SIZ_ROOMS);
      }

      // Stories: 1000 = 1 story
      iTmp = atoin(pSfrChar->Stories, RES_SIZ_STORIES);
      if (iTmp >= 1000)
      {
#ifdef _DEBUG
         //if (!memcmp(pSfrChar->Apn, "005290120", 9)) 
         //   lTmp = 1000;
#endif
         // Check for possible error
         if (iTmp > 99000)
            memcpy(pStdChar->Stories, "1.0", 3);
         else
         {
            sprintf(acTmp, "%.1f  ", (float)(iTmp/1000.0));
            memcpy(pStdChar->Stories, acTmp, SIZ_STORIES);
         }
      }

      // Heat-Cool
      if (pSfrChar->Cent_Ht[0] == 'Y')
         pStdChar->Heating[0] = 'Z';
      if (pSfrChar->Cent_Cool[0] == 'Y')
         pStdChar->Cooling[0] = 'C';

      // Parking-Garage
      lTmp = atoin(pSfrChar->Stalls, RES_SIZ_STALLS);
      if (lTmp > 0)
      {
         iTmp = sprintf(acTmp, "%d", lTmp);
         memcpy(pStdChar->ParkSpace, acTmp, iTmp);
         pStdChar->ParkType[0] = 'Z';
      }
#ifdef _DEBUG
      //if (!memcmp(pSfrChar, "004440090", 9)) 
      //if (lTmp > 100)
      //   iTmp = 1;
#endif

      // Park type
      //if (pSfrChar->Gar_Cls_Att[0] > ' ')
      //   pStdChar->ParkType[0] = 'I';
      //else if (pSfrChar->Gar_Cls_Det[0] > ' ')
      //   pStdChar->ParkType[0] = 'L';
      //else 
      if (pSfrChar->Garage_Basement[0] == 'Y')
         pStdChar->ParkType[0] = 'E';
      else if (pSfrChar->Carport[0] == 'Y')
         pStdChar->ParkType[0] = 'C';

      // GarSqft
      lTmp = atoin(pSfrChar->Garage_Area, RES_SIZ_GARAGE_AREA);
      if (lTmp > 0)
      {
         iTmp = sprintf(acTmp, "%d", lTmp);
         memcpy(pStdChar->GarSqft, acTmp, iTmp);
      }

      // BldgSqft - All floors + addl area
      lTmp = atoin(pSfrChar->BldgArea, RES_SIZ_BLDG_AREA);
      if (lTmp > 0)
      {
         iTmp = sprintf(acTmp, "%d", lTmp);
         memcpy(pStdChar->BldgSqft, acTmp, iTmp);
      }
      lTmp = atoin(pSfrChar->Floor1_Area, RES_SIZ_FLOOR1_AREA);
      if (lTmp > 0)
      {
         iTmp = sprintf(acTmp, "%d", lTmp);
         memcpy(pStdChar->Sqft_1stFl, acTmp, iTmp);
      }
      lTmp = atoin(pSfrChar->Floor2_Area, RES_SIZ_FLOOR2_AREA);
      if (lTmp > 0)
      {
         iTmp = sprintf(acTmp, "%d", lTmp);
         memcpy(pStdChar->Sqft_2ndFl, acTmp, iTmp);
      }
      lTmp = atoin(pSfrChar->Floor3_Area, RES_SIZ_FLOOR3_AREA);
      if (lTmp > 0)
      {
         iTmp = sprintf(acTmp, "%d", lTmp);
         memcpy(pStdChar->Sqft_Above2nd, acTmp, iTmp);
      }
      lTmp = atoin(pSfrChar->Addl_Area, RES_SIZ_ADDL_AREA);
      if (lTmp > 0)
      {
         iTmp = sprintf(acTmp, "%d", lTmp);
         memcpy(pStdChar->MiscSqft, acTmp, iTmp);
      }

      // Basement area
      lTmp = atoin(pSfrChar->Basement_Area, RES_SIZ_BASEMENT_AREA);
      if (lTmp > 0)
      {
         iTmp = sprintf(acTmp, "%d", lTmp);
         memcpy(pStdChar->BsmtSqft, acTmp, iTmp);
      }

      // LotSize
      if (!memcmp(pSfrChar->Apn, "080030020", 9))
      {
         // 11/02/2021
         memcpy(pStdChar->LotAcre, "27400", 5);
         memcpy(pStdChar->LotSqft, "1193544", 7);
      } else
      {
         lTmp = atoin(pSfrChar->Acres, RES_SIZ_ACRES);
         if (lTmp > 0)
         {
            iTmp = sprintf(acTmp, "%d", lTmp);
            memcpy(pStdChar->LotAcre, acTmp, iTmp);
         }
         lTmp = atoin(pSfrChar->Lot_Sqft_Actual, RES_SIZ_ACRES);
         if (lTmp > 0)
         {
            iTmp = sprintf(acTmp, "%d", lTmp);
            memcpy(pStdChar->LotSqft, acTmp, iTmp);
         }
      }

      // Pool
      lTmp = atoin(pSfrChar->Pool_Year, RES_SIZ_POOL_YEAR);
      if (lTmp > 0)
         pStdChar->Pool[0] = 'P';

      // View
      if (pSfrChar->Corner[0] == 'Y')
         pStdChar->View[0] = 'J';
      else if (pSfrChar->Waterfront[0] == 'Y')
         pStdChar->View[0] = 'W';

      pStdChar->CRLF[0] = '\n';
      pStdChar->CRLF[1] = '\0';
      fputs((char *)&pStdChar->Apn[0], fdOut);

      if (!(++iCnt % 1000))
         printf("\r%u", iCnt);
   }

   fclose(fdIn);
   printf("\n");
   LogMsg("Total records output: %d", iCnt);
   return 0;
}

/****************************** Smx_ProcessApt() ****************************
 *
 * Merge APT into CHAR file
 *
 * 09/11/2013 - Add Elevator & Units (no Basement or BldgCls)
 *
 ****************************************************************************/

int Smx_ProcessApt(FILE *fdOut)
{
   FILE     *fdIn;
   char     acChar[1024], acTmp[256], acOutbuf[2048], *pTmp;
   int      iTmp, iCnt=0;
   long     lTmp;
   STDCHAR        *pStdChar = (STDCHAR *)acOutbuf;
   SMX_CHAR_APT   *pAptChar = (SMX_CHAR_APT *)acChar;

   GetIniString(myCounty.acCntyCode, "MULTIFAM", "", acTmp, _MAX_PATH, acIniFile);
   LogMsg("Loading %s", acTmp);
   if (!(fdIn = fopen(acTmp, "r")))
   {
      LogMsg("***** Error opening MULTIFAM file: %s", acTmp);
      return -1;
   }

   while ((pTmp = fgets(acChar,  1024, fdIn)))
   {
      memset(acOutbuf, ' ', sizeof(STDCHAR));
      memcpy(pStdChar->Apn, pAptChar->Apn , APT_SIZ_APN);
#ifdef _DEBUG
      //if (!memcmp(pAptChar, "002072140", 9))
      //   iTmp = 1;
#endif

      // YrBlt
      lTmp = atoin(pAptChar->Yr_Blt, APT_SIZ_YR_BLT);
      if (lTmp > 1700 && lTmp < 2100)
      {
         iTmp = sprintf(acTmp, "%d", lTmp);
         memcpy(pStdChar->YrBlt, acTmp, iTmp);
      }

      // Usecode
      if (pAptChar->UseCode[0] > ' ')
         memcpy(pStdChar->LandUse, pAptChar->UseCode, APT_SIZ_USE_CD);

      // Bldgs
      lTmp = atoin(pAptChar->Bldgs, APT_SIZ_NO_OF_BLDGS);
      if (lTmp > 0 && lTmp < 100)
      {
         iTmp = sprintf(acTmp, "%d", lTmp);
         memcpy(pStdChar->Bldgs, acTmp, iTmp);
      }

      // Stories
      lTmp = atoin(pAptChar->No_Of_Stories, APT_SIZ_NO_OF_STORIES);
      if (lTmp >= 1000)
      {
         // Check for possible error
         if (lTmp > 99000)
            memcpy(pStdChar->Stories, "1.0", 3);
         else
         {
            sprintf(acTmp, "%.1f  ", (float)(lTmp/1000.0));
            memcpy(pStdChar->Stories, acTmp, SIZ_STORIES);
         }
      }

      // BldgSqft
      lTmp = atoin(pAptChar->Gross_Bldg_Area, APT_SIZ_GROSS_BLDG_AREA);
      if (lTmp > 0)
      {
         iTmp = sprintf(acTmp, "%d", lTmp);
         memcpy(pStdChar->BldgSqft, acTmp, iTmp);
      }

      // Beds
      lTmp = atoin(pAptChar->Bedrms, APT_SIZ_BEDRMS);
      if (lTmp > 0 && lTmp < 100)
      {
         sprintf(acTmp, "%d   ", lTmp);
         memcpy(pStdChar->Beds, acTmp, SIZ_CHAR_BEDS);
      }

      // Baths
      lTmp = atoin(pAptChar->Baths, APT_SIZ_BATHS1);
      if (lTmp > 0 && lTmp < 100)
      {
         sprintf(acTmp, "%d   ", lTmp);
         memcpy(pStdChar->FBaths, acTmp, SIZ_CHAR_BATHS);
         memcpy(pStdChar->Bath_4Q, acTmp, SIZ_CHAR_SIZE2);
      }

      // Half bath: contains actual number of half baths
      lTmp = atoin(pAptChar->Half_Baths, APT_SIZ_HALF_BATHS);
      if (lTmp > 0 && lTmp < 100)
      {
         sprintf(acTmp, "%d   ", lTmp);
         memcpy(pStdChar->HBaths, acTmp, SIZ_CHAR_BATHS);
         memcpy(pStdChar->Bath_2Q, acTmp, SIZ_CHAR_SIZE2);
      }

      // Rooms
      iTmp = atoin(pAptChar->Rooms, APT_SIZ_TOT_RMS);
      if (iTmp > 0)
      {
         sprintf(acTmp, "%d    ", iTmp);
         memcpy(pStdChar->Rooms, acTmp, SMX_SIZ_ROOMS);
      }

      // Garage
      int iGarage = atoin(pAptChar->Garage_Stalls_W_Doors, APT_SIZ_GARAGE_STALLS_W_DOORS);
      iGarage += atoin(pAptChar->Garage_Stalls_No_Doors, APT_SIZ_GARAGE_STALLS_NO_DOORS);
      int iOpenSpc = atoin(pAptChar->Open_Spc_Stalls, APT_SIZ_OPEN_SPC_STALLS);
      int iCarPort = atoin(pAptChar->Carport_Stalls, APT_SIZ_CARPORT_STALLS);
      lTmp = iGarage + iOpenSpc + iCarPort;
      if (lTmp > 0)
      {
         iTmp = sprintf(acTmp, "%d", lTmp);
         memcpy(pStdChar->ParkSpace, acTmp, iTmp);
         if (iGarage > 0 && iCarPort > 0)
            pStdChar->ParkType[0] = '2';
         else if (iCarPort > 0)
            pStdChar->ParkType[0] = 'C';
         else if (iGarage > 0)
            pStdChar->ParkType[0] = 'Z';
         else if (iOpenSpc > 0)
            pStdChar->ParkType[0] = 'G';
      }

#ifdef _DEBUG
      //if (!memcmp(pAptChar, "002194050", 9))
      //   iTmp = 1;
#endif

      // Fire place
      lTmp = atoin(pAptChar->FirePlaces, APT_SIZ_FIREPLACES);
      if (lTmp > 9)
         pStdChar->Fireplace[0] = 'M';
      else if (lTmp > 0)
         pStdChar->Fireplace[0] = '0' | lTmp;
      else if (pAptChar->FirePlaces[0] > '0')
         LogMsg("*** Bad FirePlace %.9s [%.*s]", pAptChar->FirePlaces, iApnLen, pAptChar->Apn);

      // LotAcre
      lTmp = atoin(pAptChar->Acres, APT_SIZ_ACRES1);
      if (lTmp > 0)
      {
         iTmp = sprintf(acTmp, "%d", lTmp);
         memcpy(pStdChar->LotAcre, acTmp, iTmp);
      }

      // Lot Sqft
      lTmp = atoin(pAptChar->Lot_Sqft_Actual, APT_SIZ_LOT_SQFT_ACTUAL1);
      if (lTmp > 0)
      {
         iTmp = sprintf(acTmp, "%d", lTmp);
         memcpy(pStdChar->LotSqft, acTmp, iTmp);
      }

      // View
      if (pAptChar->Corner[0] == 'Y')
         pStdChar->View[0] = 'J';
      else if (pAptChar->Waterfront[0] == 'Y')
         pStdChar->View[0] = 'W';
      else if (pAptChar->Cul_de_sac[0] == 'Y')
         pStdChar->View[0] = 'K';

      // Elevator - A, E, F, G, P
      if (pAptChar->Elevator[0] >= 'A')
         pStdChar->HasElevator = pAptChar->Elevator[0];

      // Units
      lTmp = atoin(pAptChar->Units_Allowed, APT_SIZ_UNITS_ALLOWED);
      if (lTmp > 0 && lTmp < 1000)
      {
         iTmp = sprintf(acTmp, "%d", lTmp);
         memcpy(pStdChar->Units, acTmp, iTmp);
      }

      pStdChar->CRLF[0] = '\n';
      pStdChar->CRLF[1] = '\0';
      fputs((char *)&pStdChar->Apn[0], fdOut);

      iCnt++;
      if (!(++iCnt % 1000))
         printf("\r%u", iCnt);
   }

   printf("\n");
   fclose(fdIn);
   LogMsg("Total records output: %d", iCnt);

   return 0;
}

/****************************** Smx_ProcessCondo() ****************************
 *
 * Merge CONDO into CHAR file
 *
 * 09/11/2013 - Add Units (no BldgCls, Elevator, Basement)
 *
 ****************************************************************************/

int Smx_ProcessCondo(FILE *fdOut)
{
   FILE     *fdIn;
   char     acChar[1024], acTmp[256], acOutbuf[2048], *pTmp;
   int      iTmp, iCnt=0;
   long     lTmp;
   STDCHAR        *pStdChar   = (STDCHAR *)acOutbuf;
   SMX_CHAR_CON   *pCondoChar = (SMX_CHAR_CON *)acChar;

   GetIniString(myCounty.acCntyCode, "CONDO", "", acTmp, _MAX_PATH, acIniFile);
   LogMsg("Loading %s", acTmp);
   if (!(fdIn = fopen(acTmp, "r")))
   {
      LogMsg("***** Error opening CONDO file: %s", acTmp);
      return -1;
   }

   while ((pTmp = fgets(acChar,  1024, fdIn)))
   {
      memset(acOutbuf, ' ', sizeof(STDCHAR));
      memcpy(pStdChar->Apn, pCondoChar->Apn , SMX_SIZ_APN);
#ifdef _DEBUG
      //if (!memcmp(pCondoChar, "002072140", 9))
      //   iTmp = 1;
#endif

      // Pool
      if (pCondoChar->Pool == 'Y')
         pStdChar->Pool[0] = 'P';

      // YrBlt
      lTmp = atoin(pCondoChar->Yr_Blt, SMX_CSIZ_YEAR_BUILT);
      if (lTmp > 1700 && lTmp < 2100)
      {
         iTmp = sprintf(acTmp, "%d", lTmp);
         memcpy(pStdChar->YrBlt, acTmp, iTmp);
      }

      // Yr_Eff
      lTmp = atoin(pCondoChar->Yr_Eff, SMX_CSIZ_YEAR_BUILT);
      if (lTmp > 1700 && lTmp < 2100)
      {
         iTmp = sprintf(acTmp, "%d", lTmp);
         memcpy(pStdChar->YrEff, acTmp, iTmp);
      }

      // Quality
      if (pCondoChar->Quality > ' ')
         pStdChar->BldgQual = pCondoChar->Quality;

      // Condition
      if (pCondoChar->Condition > ' ')
         pStdChar->ImprCond[0] = pCondoChar->Condition;

      // Beds
      lTmp = atoin(pCondoChar->Bedrms, SMX_CSIZ_BEDROOMS);
      if (lTmp > 0 && lTmp < 100)
      {
         sprintf(acTmp, "%d   ", lTmp);
         memcpy(pStdChar->Beds, acTmp, SMX_SIZ_BEDROOMS);
      }

      // Baths
      lTmp = atoin(pCondoChar->Baths, SMX_CSIZ_BATHS);
      if (pCondoChar->Baths[0] > '0')
      {
         pStdChar->FBaths[0] = pCondoChar->Baths[0];
         pStdChar->Bath_4Q[0] = pCondoChar->Baths[0];
         if (pCondoChar->Baths[1] > '0')
         {
            pStdChar->HBaths[0] = '1';
            pStdChar->Bath_2Q[0] = '1';
         }
      }

      // Rooms
      iTmp = atoin(pCondoChar->Rooms, SMX_CSIZ_TOTAL_ROOMS);
      if (iTmp > 0)
      {
         sprintf(acTmp, "%d    ", iTmp);
         memcpy(pStdChar->Rooms, acTmp, SMX_SIZ_ROOMS);
      }

      // BldgSqft
      lTmp = atoin(pCondoChar->Base_Area, SMX_CSIZ_BASE_AREA);
      if (lTmp > 0)
      {
         iTmp = sprintf(acTmp, "%d", lTmp);
         memcpy(pStdChar->BldgSqft, acTmp, iTmp);
      }

      // Garage Sqft
      lTmp = atoin(pCondoChar->Garage_Area, SMX_CSIZ_GARAGE_AREA);
      if (lTmp > 0)
      {
         iTmp = sprintf(acTmp, "%d", lTmp);
         memcpy(pStdChar->GarSqft, acTmp, iTmp);
         pStdChar->ParkType[0] = 'Z';
      }

      // Stories
      lTmp = atoin(pCondoChar->Stories, SMX_CSIZ_STORIES);
      if (lTmp > 0)
      {
         sprintf(acTmp, "%d.0  ", lTmp);
         memcpy(pStdChar->Stories, acTmp, SIZ_STORIES);
      }

      // Fire place
      lTmp = atoin(pCondoChar->FirePlaces, SMX_CSIZ_FIREPLACES);
      if (lTmp > 9)
         pStdChar->Fireplace[0] = 'M';
      else if (lTmp > 0)
         pStdChar->Fireplace[0] = '0' | lTmp;
      else if (pCondoChar->FirePlaces[0] > '0')
         LogMsg("*** Bad FirePlace %.2s [%.*s]", pCondoChar->FirePlaces, iApnLen, pCondoChar->Apn);

      // Central Heat Y/N
      if (pCondoChar->Central_Heat == 'Y')
         pStdChar->Heating[0] = 'Z';

      // Central Air Y/N
      if (pCondoChar->Central_Air == 'Y')
         pStdChar->Cooling[0] = 'Z';

      // Parking spaces
      lTmp = atoin(pCondoChar->Stalls, SMX_CSIZ_STALLS);
      if (lTmp > 0)
      {
#ifdef _DEBUG
         //if (lTmp > 100)
         //   iTmp = 1;
#endif

         iTmp = sprintf(acTmp, "%d", lTmp);
         memcpy(pStdChar->ParkSpace, acTmp, iTmp);
         if (pCondoChar->CarPort == 'Y')
            pStdChar->ParkType[0] = 'C';
         else
            pStdChar->ParkType[0] = 'Z';
      }

      // Units
      lTmp = atoin(pCondoChar->Units, SMX_CSIZ_NO_OF_UNITS);
      if (lTmp > 0)
      {
         iTmp = sprintf(acTmp, "%d", lTmp);
         memcpy(pStdChar->Units, acTmp, iTmp);
      }

      pStdChar->CRLF[0] = '\n';
      pStdChar->CRLF[1] = '\0';
      fputs((char *)&pStdChar->Apn[0], fdOut);

      iCnt++;
      if (!(++iCnt % 1000))
         printf("\r%u", iCnt);
   }

   printf("\n");
   fclose(fdIn);

   LogMsg("Total records output: %d", iCnt);
   return 0;
}

/****************************** Smx_ProcessHotel() ****************************
 *
 * Merge HOTEL into CHAR file
 *
 * 09/11/2013 - Add BldgClass
 *
 ****************************************************************************/

int Smx_ProcessHotel(FILE *fdOut)
{
   FILE     *fdIn;
   char     acChar[1024], acTmp[256], acOutbuf[2048], *pTmp;
   int      iTmp, iCnt=0;
   long     lTmp;
   STDCHAR        *pStdChar   = (STDCHAR *)acOutbuf;
   SMX_CHAR_HOT   *pHotelChar  = (SMX_CHAR_HOT *)acChar;

   GetIniString(myCounty.acCntyCode, "HOTEL", "", acTmp, _MAX_PATH, acIniFile);
   LogMsg("Loading %s", acTmp);
   if (!(fdIn = fopen(acTmp, "r")))
   {
      LogMsg("***** Error opening HOTEL file: %s", acTmp);
      return -1;
   }

   while ((pTmp = fgets(acChar,  1024, fdIn)))
   {
      memset(acOutbuf, ' ', sizeof(STDCHAR));
      memcpy(pStdChar->Apn, pHotelChar->Apn , SMX_SIZ_APN);
#ifdef _DEBUG
      //if (!memcmp(pHotelChar, "002072140", 9))
      //   iTmp = 1;
#endif

      // YrBlt
      lTmp = atoin(pHotelChar->Yr_Blt, SMX_HSIZ_YEAR_BUILT);
      if (lTmp > 1700 && lTmp < 2100)
      {
         iTmp = sprintf(acTmp, "%d", lTmp);
         memcpy(pStdChar->YrBlt, acTmp, iTmp);
      }

      // LotSize
      lTmp = atoin(pHotelChar->Acres, SMX_HSIZ_ACRES);
      if (lTmp > 0)
      {
         iTmp = sprintf(acTmp, "%d", lTmp);
         memcpy(pStdChar->LotAcre, acTmp, iTmp);
      }
      lTmp = atoin(pHotelChar->LotSqft, SMX_HSIZ_LAND_AREA);
      if (lTmp > 0)
      {
         iTmp = sprintf(acTmp, "%d", lTmp);
         memcpy(pStdChar->LotSqft, acTmp, iTmp);
      }

      // BldgSqft
      lTmp = atoin(pHotelChar->BldgSqft, SMX_HSIZ_GROSS_BLDGAREA);
      if (lTmp > 0)
      {
         iTmp = sprintf(acTmp, "%d", lTmp);
         memcpy(pStdChar->BldgSqft, acTmp, iTmp);
      }

      // Bldg class
      if (pHotelChar->Const_Class[0] > ' ')
         pStdChar->BldgClass = pHotelChar->Const_Class[0];

      // Quality
      if (pHotelChar->Quality[0] > ' ')
         pStdChar->BldgQual = pHotelChar->Quality[0];

      // Stories
      lTmp = atoin(pHotelChar->Stories, SMX_HSIZ_STORIES);
      if (lTmp > 0)
      {
         sprintf(acTmp, "%d.0  ", lTmp);
         memcpy(pStdChar->Stories, acTmp, SIZ_STORIES);
      }

      // Rooms
      iTmp = atoin(pHotelChar->Rooms, SMX_HSIZ_TOTAL_ROOMS);
      if (iTmp > 0)
      {
         sprintf(acTmp, "%d    ", iTmp);
         memcpy(pStdChar->Rooms, acTmp, SMX_SIZ_ROOMS);
      }

      pStdChar->CRLF[0] = '\n';
      pStdChar->CRLF[1] = '\0';
      fputs((char *)&pStdChar->Apn[0], fdOut);

      iCnt++;
      if (!(++iCnt % 1000))
         printf("\r%u", iCnt);
   }

   printf("\n");
   fclose(fdIn);

   LogMsg("Total records output: %d", iCnt);
   return 0;
}

/************************** Smx_ProcessIndustrial() *************************
 *
 * Merge INDUSTRIAL into CHAR file
 *
 ****************************************************************************/

int Smx_ProcessIndustrial(FILE *fdOut)
{
   FILE     *fdIn;
   char     acChar[1024], acTmp[256], acOutbuf[2048], *pTmp;
   int      iTmp, iCnt=0;
   long     lTmp;
   STDCHAR        *pStdChar = (STDCHAR *)acOutbuf;
   SMX_CHAR_IND   *pIndChar = (SMX_CHAR_IND *)acChar;

   GetIniString(myCounty.acCntyCode, "INDUSTR", "", acTmp, _MAX_PATH, acIniFile);
   LogMsg("Loading %s", acTmp);
   if (!(fdIn = fopen(acTmp, "r")))
   {
      LogMsg("***** Error opening INDUSTR file: %s", acTmp);
      return -1;
   }

   while ((pTmp = fgets(acChar,  1024, fdIn)))
   {
      memset(acOutbuf, ' ', sizeof(STDCHAR));
      memcpy(pStdChar->Apn, pIndChar->Apn , SMX_SIZ_APN);

      // Stories
      lTmp = atoin(pIndChar->Stories, SMX_HSIZ_STORIES);
      if (lTmp > 0)
      {
         sprintf(acTmp, "%d.0  ", lTmp);
         memcpy(pStdChar->Stories, acTmp, SIZ_STORIES);
      }

      // BldgSqft
      lTmp = atoin(pIndChar->BldgSqft, SMX_HSIZ_GROSS_BLDGAREA);
      if (lTmp > 0)
      {
         iTmp = sprintf(acTmp, "%d", lTmp);
         memcpy(pStdChar->BldgSqft, acTmp, iTmp);
      }

      // Quality
      if (pIndChar->Quality[0] > ' ')
         pStdChar->BldgQual = pIndChar->Quality[0];

      // YrBlt
      lTmp = atoin(pIndChar->Yr_Blt, SMX_ISIZ_YEAR_BUILT);
      if (lTmp > 1700 && lTmp < 2100)
      {
         iTmp = sprintf(acTmp, "%d", lTmp);
         memcpy(pStdChar->YrBlt, acTmp, iTmp);
      }

      // LotSize
      lTmp = atoin(pIndChar->LotSqft, SMX_ISIZ_LAND_SQFT);
      if (lTmp > 0)
      {
         iTmp = sprintf(acTmp, "%d", lTmp);
         memcpy(pStdChar->LotSqft, acTmp, iTmp);
      }
      lTmp = atoin(pIndChar->Acres, SMX_ISIZ_ACRES);
      if (lTmp > 0)
      {
         iTmp = sprintf(acTmp, "%d", lTmp);
         memcpy(pStdChar->LotAcre, acTmp, iTmp);
      }

      // Parking spaces
      lTmp = atoin(pIndChar->ParkSpaces, SMX_ISIZ_PARKING_SPACES);
      if (lTmp > 0)
      {
         if (lTmp > 999)
            lTmp /= 1000;

         iTmp = sprintf(acTmp, "%d", lTmp);
         memcpy(pStdChar->ParkSpace, acTmp, iTmp);
         pStdChar->ParkType[0] = 'Z';
      }

      pStdChar->CRLF[0] = '\n';
      pStdChar->CRLF[1] = '\0';
      fputs((char *)&pStdChar->Apn[0], fdOut);

      iCnt++;
      if (!(++iCnt % 1000))
         printf("\r%u", iCnt);
   }

   printf("\n");
   fclose(fdIn);

   LogMsg("Total records output: %d", iCnt);
   return 0;
}

/**************************** Smx_ProcessMobile() ***************************
 *
 * Merge MOBILE into CHAR file
 *
 ****************************************************************************/

int Smx_ProcessMobile(FILE *fdOut)
{
   FILE     *fdIn;
   char     acChar[1024], acTmp[256], acOutbuf[2048], *pTmp;
   int      iTmp, iCnt=0;
   long     lTmp;
   STDCHAR        *pStdChar = (STDCHAR *)acOutbuf;
   SMX_CHAR_MOB   *pMobChar = (SMX_CHAR_MOB *)acChar;

   GetIniString(myCounty.acCntyCode, "MOBILE", "", acTmp, _MAX_PATH, acIniFile);
   LogMsg("Loading %s", acTmp);
   if (!(fdIn = fopen(acTmp, "r")))
   {
      LogMsg("***** Error opening INDUSTR file: %s", acTmp);
      return -1;
   }

   while ((pTmp = fgets(acChar,  1024, fdIn)))
   {
      memset(acOutbuf, ' ', sizeof(STDCHAR));
      memcpy(pStdChar->Apn, pMobChar->Apn , SMX_SIZ_APN);

      // Space number
      //if (pMobChar->SpaceNo[0] > '0')
      //   memcpy(pStdChar->SpaceNo, pMobChar->SpaceNo, SIZ_S_UNITNO);

      // YrBlt
      lTmp = atoin(pMobChar->Yr_Blt, SMX_MSIZ_YEAR_BUILT);
      if (lTmp > 1700 && lTmp < 2100)
      {
         iTmp = sprintf(acTmp, "%d", lTmp);
         memcpy(pStdChar->YrBlt, acTmp, iTmp);
      }

      // BldgSqft
      lTmp = atoin(pMobChar->BldgSqft, SMX_HSIZ_GROSS_BLDGAREA);
      if (lTmp > 0)
      {
         iTmp = sprintf(acTmp, "%d", lTmp);
         memcpy(pStdChar->BldgSqft, acTmp, iTmp);
      }

      pStdChar->CRLF[0] = '\n';
      pStdChar->CRLF[1] = '\0';
      fputs((char *)&pStdChar->Apn[0], fdOut);

      iCnt++;
      if (!(++iCnt % 1000))
         printf("\r%u", iCnt);
   }

   printf("\n");
   fclose(fdIn);

   LogMsg("Total records output: %d", iCnt);
   return 0;
}

/****************************** Smx_ProcessOffice() *************************
 *
 * 09/11/2013 - Add BsmtSqft & Elevator (No Units or BldgCls)
 *
 ****************************************************************************/

int Smx_ProcessOffice(FILE *fdOut)
{
   FILE     *fdIn;
   char     acChar[1024], acTmp[256], acOutbuf[2048], *pTmp;
   int      iTmp, iCnt=0;
   long     lTmp;
   STDCHAR        *pStdChar = (STDCHAR *)acOutbuf;
   SMX_CHAR_OFF   *pOffChar = (SMX_CHAR_OFF *)acChar;

   GetIniString(myCounty.acCntyCode, "OFFICE", "", acTmp, _MAX_PATH, acIniFile);
   LogMsg("Loading %s", acTmp);
   if (!(fdIn = fopen(acTmp, "r")))
   {
      LogMsg("***** Error opening OFFICE file: %s", acTmp);
      return -1;
   }

   while ((pTmp = fgets(acChar,  1024, fdIn)))
   {
      memset(acOutbuf, ' ', sizeof(STDCHAR));
      memcpy(pStdChar->Apn, pOffChar->Apn , SMX_SIZ_APN);

      // Stories
      lTmp = atoin(pOffChar->Stories, SMX_OSIZ_STORIES);
      if (lTmp > 0)
      {
         sprintf(acTmp, "%d.0  ", lTmp);
         memcpy(pStdChar->Stories, acTmp, SIZ_STORIES);
      }

      // BldgSqft
      lTmp = atoin(pOffChar->BldgSqft, SMX_OSIZ_GROSS_BLDGAREA);
      if (lTmp > 0)
      {
         iTmp = sprintf(acTmp, "%d", lTmp);
         memcpy(pStdChar->BldgSqft, acTmp, iTmp);
      }

      // Basement area
      lTmp = atoin(pOffChar->Basement_Area, SMX_OSIZ_BASEMENT_AREA);
      if (lTmp > 0)
      {
         iTmp = sprintf(acTmp, "%d", lTmp);
         memcpy(pStdChar->BsmtSqft, acTmp, iTmp);
      }

      // Quality
      if (pOffChar->Quality[0] > ' ')
         pStdChar->BldgQual = pOffChar->Quality[0];

      // YrBlt
      lTmp = atoin(pOffChar->Yr_Blt, SMX_OSIZ_YEAR_BUILT);
      if (lTmp > 1700 && lTmp < 2100)
      {
         iTmp = sprintf(acTmp, "%d", lTmp);
         memcpy(pStdChar->YrBlt, acTmp, iTmp);
      }

      // Yr_Eff
      lTmp = atoin(pOffChar->Renovate_Year, SMX_OSIZ_RENOVATE_YEAR);
      if (lTmp > 1700 && lTmp < 2099)
      {
         iTmp = sprintf(acTmp, "%d", lTmp);
         memcpy(pStdChar->YrEff, acTmp, iTmp);
      }

      // LotSize
      lTmp = atoin(pOffChar->LotSqft, SMX_OSIZ_LOT_SQFT);
      if (lTmp > 0)
      {
         iTmp = sprintf(acTmp, "%d", lTmp);
         memcpy(pStdChar->LotSqft, acTmp, iTmp);
      }
      lTmp = atoin(pOffChar->LotAcres, SMX_OSIZ_LOT_ACRES);
      if (lTmp > 0)
      {
         iTmp = sprintf(acTmp, "%d", lTmp);
         memcpy(pStdChar->LotAcre, acTmp, iTmp);
      }

      // Parking spaces
      lTmp = atoin(pOffChar->Surf_ParkSpace, SMX_OSIZ_SURFACE_PARK_SPACE);
      if (lTmp > 0)
      {
         iTmp = sprintf(acTmp, "%d", lTmp);
         memcpy(pStdChar->ParkSpace, acTmp, iTmp);
         pStdChar->ParkType[0] = 'G';
      }

      // Elevator - there are freight elev and elev bank
      if (pOffChar->Elevator_Bank[0] == 'Y')
         pStdChar->HasElevator = 'Y';
      else if (pOffChar->Freight_Elavator[0] == 'Y')
         pStdChar->HasElevator = 'Y';

      pStdChar->CRLF[0] = '\n';
      pStdChar->CRLF[1] = '\0';
      fputs((char *)&pStdChar->Apn[0], fdOut);

      iCnt++;
      if (!(++iCnt % 1000))
         printf("\r%u", iCnt);
   }

   printf("\n");
   fclose(fdIn);

   LogMsg("Total records output: %d", iCnt);
   return 0;
}

/****************************** Smx_ConvertChar() ***************************
 *
 * Merge SFR and APT file into CHAR file
 * Return > 0 if successful.  < 0 is file open error, 0 is sort error.
 *
 ****************************************************************************/

int Smx_ConvertChar(char *pOutfile)
{
   FILE     *fdOut;
   char     acTmpFile[256], acTmp[256];
   int      iRet;

   sprintf(acTmpFile, "%s\\Smx\\Smx_Char.Tmp", acTmpPath);
   if (!(fdOut = fopen(acTmpFile, "w")))
   {
      LogMsg("***** Error creating output file %s", acTmpFile);
      return -2;
   }

   // Process SFR - Resident
   iRet = Smx_ProcessSfr(fdOut);

   // Process APT - Multifamily
   if (!iRet)
      iRet = Smx_ProcessApt(fdOut);

   // Process CONDO
   if (!iRet)
      iRet = Smx_ProcessCondo(fdOut);

   // Process HOT
   if (!iRet)
      iRet = Smx_ProcessHotel(fdOut);

   // Process IND
   if (!iRet)
      iRet = Smx_ProcessIndustrial(fdOut);

   // Process MOB
   if (!iRet)
      iRet = Smx_ProcessMobile(fdOut);

   // Process OFF
   if (!iRet)
      iRet = Smx_ProcessOffice(fdOut);

   fclose(fdOut);

   // Sort output on APN
   if (!iRet)
   {
      sprintf(acTmp, "S(1,%d,C,A)", iApnLen);
      iRet = sortFile(acTmpFile, pOutfile, acTmp);
   }

   printf("\n");
   return iRet;
}

/****************************** Smx_ConvStdChar ******************************
 *
 * Convert char file to STDCHAR format.
 *
 *****************************************************************************/

int Smx_ConvStdChar(char *pInfile)
{
   FILE     *fdIn, *fdOut;
   char     acApn[32], acInbuf[1024], acOutbuf[2048], acTmpFile[256], acTmp[256], *pRec;
   int      iRet, iTmp, iCnt=0, iMultiApn=0;
   STDCHAR  *pCharRec=(STDCHAR *)&acOutbuf[0];

   LogMsg0("Converting char file %s", pInfile);
   if (!(fdIn = fopen(pInfile, "r")))
      return -1;

   sprintf(acTmpFile, "%s\\%s\\%s_char.tmp", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   if (!(fdOut = fopen(acTmpFile, "w")))
   {
      fclose(fdIn);
      LogMsg("***** Error creating output file %s", acTmpFile);
      return -2;
   }

   // Skip first record - header
   iTmp = 0;
   for (iTmp = 0; iTmp < iHdrRows; iTmp++)
      pRec = fgets(acInbuf, 1024, fdIn);

   acApn[0] = 0;
   while (!feof(fdIn))
   {
      pRec = fgets(acInbuf, 1024, fdIn);

      if (!pRec)
         break;
#ifdef _DEBUG
      //if (!memcmp(acBuf, "930218200012009759", 9))
      //   iRet = 0;
#endif

      iRet = ParseStringIQ(pRec, 124, SMX_C_FLDS+1, apTokens);
      if (iRet < SMX_C_FLDS)
      {
         LogMsg("*** Bad CHAR record (%d): %s", iCnt, pRec);
         continue;
      }

      if (!memcmp(acApn, apTokens[SMX_C_APN], iApnLen))
      {
         if (!_memicmp(apTokens[SMX_C_CHAR_SCREEN], "Land", 3) || !_memicmp(apTokens[SMX_C_CHAR_SCREEN], "Accessory", 3))
            continue;
         iMultiApn++;
      }

      memset(acOutbuf, ' ', sizeof(STDCHAR));

      // Apn
      memcpy(pCharRec->Apn, apTokens[SMX_C_APN], strlen(apTokens[SMX_C_APN]));
      strcpy(acApn, apTokens[SMX_C_APN]);

      // YrBlt
      iTmp = atoi(apTokens[SMX_C_YEAR_BUILT]);
      if (iTmp > 1600 && iTmp != 1900)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(pCharRec->YrBlt, acTmp, iRet);
      }

      // BldgSqft
      if (!_memicmp(apTokens[SMX_C_CHAR_SCREEN], "SFR", 3) || !_memicmp(apTokens[SMX_C_CHAR_SCREEN], "Condos", 4) )
         iTmp = SMX_C_TOTAL_LIVAREA;
      else if (!_memicmp(apTokens[SMX_C_CHAR_SCREEN], "Mobil", 3) )
         iTmp = SMX_C_TOTAL_SQFT;
      else
         iTmp = SMX_C_GROSS_BLDGAREA;

      long lSqft = atol(apTokens[iTmp]);
      if (lSqft > 100)
      {
         iRet = sprintf(acTmp, "%u", lSqft);
         memcpy(pCharRec->BldgSqft, acTmp, iRet);
      }

      // Lot Sqft
      lSqft = atol(apTokens[SMX_C_LAND_SQFT]);
      if (lSqft > 100)
      {
         iRet = sprintf(acTmp, "%u", lSqft);
         memcpy(pCharRec->LotSqft, acTmp, iRet);
         long lLotAcres = (long)((double)lSqft*SQFT_MF_1000);
         iRet = sprintf(acTmp, "%u", lLotAcres);
         memcpy(pCharRec->LotAcre, acTmp, iRet);
      }

      // Beds
      iTmp = atoi(apTokens[SMX_C_BEDROOMS]);
      if (iTmp > 0)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(pCharRec->Beds, acTmp, iRet);
      }

      // Baths
      iTmp = atoi(apTokens[SMX_C_FULL_BATHS]);
      if (iTmp > 0)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(pCharRec->FBaths, acTmp, iRet);
      }

      // Half baths
      iTmp = atoi(apTokens[SMX_C_HALF_BATHS]);
      if (iTmp > 0)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(pCharRec->HBaths, acTmp, iRet);
      }

      // Total rooms
      iTmp = atoi(apTokens[SMX_C_TOTAL_ROOMS]);
      if (iTmp > 0)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(pCharRec->Rooms, acTmp, iRet);
      }

      // # of units - this value is questionable, county is correcting on the fly
      iTmp = atoi(apTokens[SMX_C_UNITS]);
      if (iTmp > 0 && iTmp < 200)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(pCharRec->Units, acTmp, iRet);
      }

      pCharRec->CRLF[0] = '\n';
      pCharRec->CRLF[1] = '\0';
      try {
         fputs(acOutbuf, fdOut);
      } catch (...)
      {
         LogMsg("??? Exception occurs in Smx_ConvStdChar(),  APN=%s", apTokens[0]);
      }

      if (!(++iCnt % 1000))
         printf("\r%u", iCnt);
   }

   if (fdIn) fclose(fdIn);
   if (fdOut) fclose(fdOut);

   LogMsg("Number of records processed: %d", iCnt);
   LogMsg("          multiApn records:  %d", iMultiApn);

   // Sort output on ASMT
   if (iCnt > 100)
   {
      LogMsgD("\nSorting char file %s --> %s", acTmpFile, acCChrFile);
      iRet = sortFile(acTmpFile, acCChrFile, "S(1,12,C,A,53,4,C,D) F(TXT)");
   } else
   {
      printf("\n");
      iRet = 0;
   }
   return iRet;
}

/****************************** Smx_ConvStdChar2 *****************************
 *
 * Convert char file to STDCHAR format.
 * Characteristic.csv 11/22/2024
 *
 *****************************************************************************/

int Smx_ConvStdChar2(char *pInfile)
{
   FILE     *fdIn, *fdOut;
   char     acApn[32], acInbuf[2048], acOutbuf[2048], acTmpFile[256], acTmp[256], *pRec;
   int      iRet, iTmp, iCnt=0, iMultiApn=0;
   double   dAcres, dTmp;
   ULONG    lSqft;
   STDCHAR  *pCharRec=(STDCHAR *)&acOutbuf[0];

   LogMsg0("Converting char file %s", pInfile);
   if (!(fdIn = fopen(pInfile, "r")))
      return -1;

   sprintf(acTmpFile, "%s\\%s\\%s_char.tmp", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   if (!(fdOut = fopen(acTmpFile, "w")))
   {
      fclose(fdIn);
      LogMsg("***** Error creating output file %s", acTmpFile);
      return -2;
   }

   // Skip first record - header
   iTmp = 0;
   for (iTmp = 0; iTmp < iHdrRows; iTmp++)
      pRec = fgets(acInbuf, 2048, fdIn);

   acApn[0] = 0;
   while (!feof(fdIn))
   {
      pRec = fgets(acInbuf, 2048, fdIn);

      if (!pRec)
         break;
#ifdef _DEBUG
      //if (!memcmp(acBuf, "930218200012009759", 9))
      //   iRet = 0;
#endif

      iRet = ParseStringIQ(pRec, 124, SMX_C2_FLDS+1, apTokens);
      if (iRet < SMX_C2_FLDS)
      {
         LogMsg("*** Bad CHAR record (%d): %s", iCnt, pRec);
         continue;
      }

      //if (!memcmp(acApn, apTokens[SMX_C2_APN], iApnLen))
      //{
      //   if (!_memicmp(apTokens[SMX_C2_CHAR_SCREEN], "Land", 3) || !_memicmp(apTokens[SMX_C2_CHAR_SCREEN], "Accessory", 3))
      //      continue;
      //   iMultiApn++;
      //}

      memset(acOutbuf, ' ', sizeof(STDCHAR));

      // Apn
      iTmp = strlen(apTokens[SMX_C2_APN]);
      if (!memcmp(acApn, apTokens[SMX_C2_APN], iTmp))
         iMultiApn++;
      memcpy(pCharRec->Apn, apTokens[SMX_C2_APN], iTmp);
      strcpy(acApn, apTokens[SMX_C2_APN]);

      // YrBlt
      iTmp = atoi(apTokens[SMX_C2_YEAR_BUILT]);
      if (iTmp > 1600 && iTmp != 1900)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(pCharRec->YrBlt, acTmp, iRet);
      }

      // YrEff
      iTmp = atoi(apTokens[SMX_C2_EFFECTIVE_YEAR]);
      if (iTmp > 1600 && iTmp != 1900)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(pCharRec->YrEff, acTmp, iRet);
      }

      // BldgSqft
      lSqft = atol(apTokens[SMX_C2_BASE_AREA]);
      if (lSqft > 0)
      {
         iRet = sprintf(acTmp, "%u", lSqft);
         memcpy(pCharRec->BldgSqft, acTmp, iRet);
      } else
      {
         lSqft = atol(apTokens[SMX_C2_GROSS_BLDGAREA]);
         if (lSqft > 0)
         {
            iRet = sprintf(acTmp, "%u", lSqft);
            memcpy(pCharRec->BldgSqft, acTmp, iRet);
         }
      }

      // First floor area
      lSqft = atol(apTokens[SMX_C2_FIRST_FLOOR_AREA]);
      if (lSqft > 0)
      {
         iRet = sprintf(acTmp, "%u", lSqft);
         memcpy(pCharRec->Sqft_1stFl, acTmp, iRet);

         lSqft = atol(apTokens[SMX_C2_SECOND_FLOOR_AREA]);
         if (lSqft > 0)
         {
            iRet = sprintf(acTmp, "%u", lSqft);
            memcpy(pCharRec->Sqft_2ndFl, acTmp, iRet);

            lSqft = atol(apTokens[SMX_C2_THIRD_FLOOR_AREA]);
            if (lSqft > 0)
            {
               iRet = sprintf(acTmp, "%u", lSqft);
               memcpy(pCharRec->Sqft_Above2nd, acTmp, iRet);
            }
         }
      } 

      // Basement area
      lSqft = atol(apTokens[SMX_C2_FINISHED_BSMT_AREA]);
      if (lSqft > 0)
      {
         iRet = sprintf(acTmp, "%u", lSqft);
         memcpy(pCharRec->BsmtSqft, acTmp, iRet);
      } else
      {
         lSqft = atol(apTokens[SMX_C2_BASEMENT_AREA]);
         if (lSqft > 0)
         {
            iRet = sprintf(acTmp, "%u", lSqft);
            memcpy(pCharRec->BsmtSqft, acTmp, iRet);
         }
      }

      // Misc Sqft
      lSqft = atol(apTokens[SMX_C2_ATTIC_AREA]);
      if (lSqft > 0)
      {
         iRet = sprintf(acTmp, "%u", lSqft);
         memcpy(pCharRec->MiscSqft, acTmp, iRet);
      }

      // Garage area
      lSqft = atol(apTokens[SMX_C2_GARAGE_AREA]);
      if (lSqft > 0)
      {
         iRet = sprintf(acTmp, "%u", lSqft);
         memcpy(pCharRec->GarSqft, acTmp, iRet);
      }

      // Stories
      dTmp = atof(apTokens[SMX_C2_STORIES]);
      if (dTmp > 0.0)
      {
         iRet = sprintf(acTmp, "%.1f", dTmp);
         memcpy(pCharRec->Stories, acTmp, iRet);
      }

      // Lot Sqft
      dAcres = atof(apTokens[SMX_C2_ACRES]);
      lSqft = atol(apTokens[SMX_C2_LAND_SQFT]);
      if (lSqft > 10)
      {
         iRet = sprintf(acTmp, "%u", lSqft);
         memcpy(pCharRec->LotSqft, acTmp, iRet);
         long lLotAcres = (long)((double)lSqft*SQFT_MF_1000);
         iRet = sprintf(acTmp, "%u", lLotAcres);
         memcpy(pCharRec->LotAcre, acTmp, iRet);
      } else if (dAcres > 0.0)
      {
         iRet = sprintf(acTmp, "%u", (ULONG)(dAcres*1000.0));
         memcpy(pCharRec->LotAcre, acTmp, iRet);
         lSqft = (ULONG)(dAcres*SQFT_PER_ACRE);
         iRet = sprintf(acTmp, "%u", lSqft);
         memcpy(pCharRec->LotSqft, acTmp, iRet);
     }

      // Beds
      iTmp = atoi(apTokens[SMX_C2_BEDROOMS]);
      if (iTmp > 0)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(pCharRec->Beds, acTmp, iRet);
      }

      // Studio
      iTmp = atoi(apTokens[SMX_C2_NO_STUDIO]);
      if (iTmp > 0)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(pCharRec->NumStudio, acTmp, iRet);
      }
      // One bed
      iTmp = atoi(apTokens[SMX_C2_NO_1BEDROOM_1BATH]);
      if (iTmp > 0)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(pCharRec->OneBed, acTmp, iRet);
      }
      // Two beds
      iTmp = atoi(apTokens[SMX_C2_NO_2BEDROOMS_1BATH]) + atoi(apTokens[SMX_C2_NO_2BEDROOMS_2BATHS]) + atoi(apTokens[SMX_C2_NO_2BEDROOMS_15BATHS]);
      if (iTmp > 0)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(pCharRec->TwoBeds, acTmp, iRet);
      }
      // Three beds
      iTmp = atoi(apTokens[SMX_C2_NO_3BEDROOMS_1BATH]) + atoi(apTokens[SMX_C2_NO_3BEDROOMS_15BATHS]) + atoi(apTokens[SMX_C2_NO_3BEDROOMS_2BATHS]) + atoi(apTokens[SMX_C2_NO_3BEDROOMS_25BATHS]);
      if (iTmp > 0)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(pCharRec->ThreeBeds, acTmp, iRet);
      }
      // Four beds
      iTmp = atoi(apTokens[SMX_C2_NO_4BEDROOMS_3BATHS]) + atoi(apTokens[SMX_C2_NO_4BEDROOMS_2BATHS]) + atoi(apTokens[SMX_C2_NO_4BEDROOMS_25BATHS]) + atoi(apTokens[SMX_C2_NO_4BEDROOMS_35BATHS]);
      if (iTmp > 0)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(pCharRec->FourBeds, acTmp, iRet);
      }

      // Baths
      iTmp = atoi(apTokens[SMX_C2_FULL_BATH]);
      if (iTmp > 0)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(pCharRec->FBaths, acTmp, iRet);
      }

      // Half baths
      iTmp = atoi(apTokens[SMX_C2_HALF_BATH]);
      if (iTmp > 0)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(pCharRec->HBaths, acTmp, iRet);
      }

      // Total rooms
      iTmp = atoi(apTokens[SMX_C2_TOTAL_ROOMS]);
      if (iTmp > 0)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(pCharRec->Rooms, acTmp, iRet);
      }

      // # of units - this value is questionable, county is correcting on the fly
      iTmp = atoi(apTokens[SMX_C2_NO_OF_UNITS]);
      if (iTmp > 0 && iTmp < 1000)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(pCharRec->Units, acTmp, iRet);
      }

      // # of building
      iTmp = atoi(apTokens[SMX_C2_NO_BLDGS]);
      if (iTmp > 0)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(pCharRec->Bldgs, acTmp, iRet);
      }

      // Heating
      if (!_memicmp(apTokens[SMX_C2_HEATING_TYPE], "Central", 3))
         pCharRec->Heating[0] = 'Z';
      else if (!_memicmp(apTokens[SMX_C2_HEATING_TYPE], "Electric", 3))
         pCharRec->Heating[0] = '7';
      else if (!_memicmp(apTokens[SMX_C2_HEATING_TYPE], "Forced Air", 3))
         pCharRec->Heating[0] = 'B';
      else if (!_memicmp(apTokens[SMX_C2_HEATING_TYPE], "Heat Pump", 3))
         pCharRec->Heating[0] = 'G';
      else if (!_memicmp(apTokens[SMX_C2_HEATING_TYPE], "Radiant", 3))
         pCharRec->Heating[0] = 'I';
      else if (!_memicmp(apTokens[SMX_C2_HEATING_TYPE], "Wall", 3))
         pCharRec->Heating[0] = 'M';
      else if (!_memicmp(apTokens[SMX_C2_HEATING_TYPE], "Other", 3))
         pCharRec->Heating[0] = 'X';
      else if (!_memicmp(apTokens[SMX_C2_HEATING_TYPE], "Unknown", 3))
         pCharRec->Heating[0] = '9';
      else if (!_memicmp(apTokens[SMX_C2_HEATING_TYPE], "None", 3))
         pCharRec->Heating[0] = 'L';
      else if (*apTokens[SMX_C2_HEATING_TYPE] > ' ')
         LogMsg("??? Unknown Heating type: %s", apTokens[SMX_C2_HEATING_TYPE]);

      // Cooling
      if (!_memicmp(apTokens[SMX_C2_COOLING_TYPE], "Central", 3))
         pCharRec->Cooling[0] = 'C';
      else if (!_memicmp(apTokens[SMX_C2_COOLING_TYPE], "Refrigerated", 3))
         pCharRec->Cooling[0] = 'A';
      else if (!_memicmp(apTokens[SMX_C2_COOLING_TYPE], "Warmed", 3))
         pCharRec->Cooling[0] = 'X';
      else if (!_memicmp(apTokens[SMX_C2_COOLING_TYPE], "Other", 3))
         pCharRec->Cooling[0] = 'X';
      else if (!_memicmp(apTokens[SMX_C2_COOLING_TYPE], "Unknown", 3))
         pCharRec->Cooling[0] = '9';
      else if (!_memicmp(apTokens[SMX_C2_COOLING_TYPE], "None", 3))
         pCharRec->Cooling[0] = 'N';
      else if (*apTokens[SMX_C2_COOLING_TYPE] > ' ')
         LogMsg("??? Unknown Cooling type: %s", apTokens[SMX_C2_COOLING_TYPE]);

      // Parking space
      int iCarport = atoi(apTokens[SMX_C2_CARPORT]);
      iTmp = atoi(apTokens[SMX_C2_PARKING_SPACES]);
      if (iTmp > 0)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(pCharRec->ParkSpace, acTmp, iRet);
      } else if (iCarport > 0)
      {
         iRet = sprintf(acTmp, "%d", iCarport);
         memcpy(pCharRec->ParkSpace, acTmp, iRet);
      }

      // Parking type
      if (!_memicmp(apTokens[SMX_C2_PARKING_TYPE], "Attached", 3))
         pCharRec->ParkType[0] = 'I';
      else if (!_memicmp(apTokens[SMX_C2_PARKING_TYPE], "Detached", 3))
         pCharRec->ParkType[0] = 'L';
      else if (!_memicmp(apTokens[SMX_C2_PARKING_TYPE], "Carport", 3))
         pCharRec->ParkType[0] = 'C';
      else if (!_memicmp(apTokens[SMX_C2_PARKING_TYPE], "Built-In", 3))
         pCharRec->ParkType[0] = 'B';
      else if (!_memicmp(apTokens[SMX_C2_PARKING_TYPE], "Surface", 3))
         pCharRec->ParkType[0] = 'G';
      else if (!_memicmp(apTokens[SMX_C2_PARKING_TYPE], "Underground", 3))
         pCharRec->ParkType[0] = 'S';
      else if (!_memicmp(apTokens[SMX_C2_PARKING_TYPE], "Other", 3))
         pCharRec->ParkType[0] = '4';
      else if (!_memicmp(apTokens[SMX_C2_PARKING_TYPE], "Unknown", 3))
         pCharRec->ParkType[0] = '9';
      else if (*apTokens[SMX_C2_GARAGE_IN_BASEMENT] == 'Y')
         pCharRec->ParkType[0] = 'S';
      else if (iCarport > 0)
         pCharRec->ParkType[0] = 'C';
      else if (*apTokens[SMX_C2_PARKING_TYPE] > ' ')
         LogMsg("??? Unknown Parking type: %s", apTokens[SMX_C2_PARKING_TYPE]);

      // Pool - data not reliable

      // View
      if (*apTokens[SMX_C2_WATER_FRONT] == 'Y')
         pCharRec->View[0] = 'W';
      else if (*apTokens[SMX_C2_LOCATION] == 'A')
         pCharRec->View[0] = 'H';      // Alley/Fairway
      else if (!_memicmp(apTokens[SMX_C2_LOCATION], "Corner", 2))
         pCharRec->View[0] = 'J';
      else if (!_memicmp(apTokens[SMX_C2_LOCATION], "Cul", 2))
         pCharRec->View[0] = 'K';
      else if (*apTokens[SMX_C2_LOCATION] > ' ')
         LogMsg("??? Unknown site influent: %s", apTokens[SMX_C2_LOCATION]);

      // Fire place
      iTmp = atoi(apTokens[SMX_C2_FIREPLACES]);
      if (iTmp > 0 && iTmp < 10)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(pCharRec->Fireplace, acTmp, iRet);
      }

      // Quality class
      if (*apTokens[SMX_C2_QUALITY_CLASS] == 'A')
         pCharRec->BldgQual = 'A';
      else if (*apTokens[SMX_C2_QUALITY_CLASS] == 'G')
         pCharRec->BldgQual = 'G';      
      else if (*apTokens[SMX_C2_QUALITY_CLASS] == 'E')
         pCharRec->BldgQual = 'E';      
      else if (*apTokens[SMX_C2_QUALITY_CLASS] == 'F')
         pCharRec->BldgQual = 'F';      
      else if (*apTokens[SMX_C2_QUALITY_CLASS] == 'P')
         pCharRec->BldgQual = 'P';      

      if (*apTokens[SMX_C2_CONSTRUCTION_CLASS] > ' ' && *apTokens[SMX_C2_CONSTRUCTION_CLASS] != 'O')
         pCharRec->BldgClass = *apTokens[SMX_C2_CONSTRUCTION_CLASS];

      // Elevator
      if (*apTokens[SMX_C2_ELEVATOR] > '0')
         pCharRec->Elevator[0] = *apTokens[SMX_C2_ELEVATOR];

      // Amenities
      if (*apTokens[SMX_C2_AMENITIES] > '0')
         vmemcpy(pCharRec->Amenities, apTokens[SMX_C2_AMENITIES], SIZ_CHAR_AMENITIES);

      pCharRec->CRLF[0] = '\n';
      pCharRec->CRLF[1] = '\0';
      try {
         fputs(acOutbuf, fdOut);
      } catch (...)
      {
         LogMsg("??? Exception occurs in Smx_ConvStdChar(),  APN=%s", apTokens[0]);
      }

      if (!(++iCnt % 1000))
         printf("\r%u", iCnt);
   }

   if (fdIn) fclose(fdIn);
   if (fdOut) fclose(fdOut);

   LogMsg("Number of records processed: %d", iCnt);
   LogMsg("          multiApn records:  %d", iMultiApn);

   // Sort output on ASMT
   if (iCnt > 100)
   {
      LogMsgD("\nSorting char file %s --> %s", acTmpFile, acCChrFile);
      iRet = sortFile(acTmpFile, acCChrFile, "S(1,12,C,A,185,10,I,D,53,4,C,D) F(TXT)");
   } else
   {
      printf("\n");
      iRet = 0;
   }
   return iRet;
}

/******************************** Smx_MergeOwner *****************************
 *
 *
 *****************************************************************************/

void Smx_MergeOwner(char *pOutbuf, char *pRollRec)
{
   int   iTmp;
   char  acOwner1[128], acOwner2[128], acLName[128], *pTmp;

   OWNER    myOwner1;
   SMX_ROLL *pRec;

   pRec = (SMX_ROLL *)pRollRec;

   // Clear output buffer if needed
   removeNames(pOutbuf, true, true);

   if (pRec->Name_3[0] > ' ' && !memcmp(pRec->Name_3, "C/O", 3))
   {
      if (pRec->Name_3[5] == 162 && pRec->Name_3[6] == 'E')
         pRec->Name_3[5] = 'P';
      updateCareOf(pOutbuf, pRec->Name_3, SMX_SIZ_NAME_3);
   } else if (pRec->Name_2[0] > ' ' && !memcmp(pRec->Name_2, "C/O", 3))
   {
      if (pRec->Name_2[5] == -94 && pRec->Name_2[6] == 'E')
         pRec->Name_2[5] = 'P';
      updateCareOf(pOutbuf, pRec->Name_2, SMX_SIZ_NAME_2);
      pRec->Name_2[0] = 0;
   } else if (pRec->Name_3[0] > ' ' && !memcmp(pRec->Name_3, "ATTN", 4))
   {
      updateCareOf(pOutbuf, pRec->Name_3, SMX_SIZ_NAME_3);
      pRec->Name_3[0] = 0;
   } else if (pRec->Name_2[0] > ' ' && !memcmp(pRec->Name_2, "ATTN", 4))
   {
      updateCareOf(pOutbuf, pRec->Name_2, SMX_SIZ_NAME_2);
      pRec->Name_2[0] = 0;
   }

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "016211030", 9) )
   //   iTmp = 0;
#endif

   // Copy Name1
   memcpy(acOwner1, pRec->Name_1, SMX_SIZ_NAME_1);
   blankRem(acOwner1, SMX_SIZ_NAME_1);
   // Fix known bad char
   if ((unsigned char)acOwner1[2] == 0xD1)
      acOwner1[2] = 'N';

   // Save last name
   strcpy(acLName, acOwner1);
   pTmp = strchr(acLName, ' ');
   if (pTmp) *(++pTmp) = 0;

   // If name2 is the same as name1, ignore it
   if (pRec->Name_2[0] > ' ' && memcmp(pRec->Name_1, pRec->Name_2, SMX_SIZ_NAME_1))
   {
      memcpy(acOwner2, pRec->Name_2, SMX_SIZ_NAME_1);
      blankRem(acOwner2, SMX_SIZ_NAME_1);
      // Check for known bad char
      if (pTmp = strchr(acOwner2, 0xD1))
         *pTmp = 'N';
      if (!memcmp(acOwner2, "OF ", 3))
      {
         strcat(acOwner1, " ");
         strcat(acOwner1, acOwner2);
         acOwner2[0] = 0;
      }
   } else
   {
      acOwner2[0] = 0;
   }

   // Update vesting
   iTmp = updateVesting(myCounty.acCntyCode, acOwner1, pOutbuf+OFF_VEST, pOutbuf+OFF_ETAL_FLG);

   if (pTmp = strchr(acOwner1, '*') )
      *pTmp = 0;

   if (acOwner2[0])
   {
      // If Owner1 and Owner2 have the same last name, combine them if individual names
      if (!memcmp(acLName, acOwner2, strlen(acLName)) )
      {
         if (!strstr(acOwner1, " ET AL")  &&
             !strstr(acOwner1, " TRUST")  &&
             !strstr(acOwner1, " L/E")    &&
             !strstr(acOwner1, " EST OF") &&
             !strstr(acOwner1, " LESSEE") &&
             memcmp(&acOwner1[strlen(acOwner1)-3], " TR", 3) &&
             memcmp(&acOwner2[strlen(acOwner2)-3], " TR", 3) &&
             !strstr(acOwner2, " TRUST"))
         {
            iTmp = MergeName1(acOwner1, acOwner2, acLName);
            if (iTmp)
            {
               acOwner2[0] = 0;
               strcpy(acOwner1, acLName);
            }
         }
      }
   }

   // Save owners
   vmemcpy(pOutbuf+OFF_NAME1, acOwner1, SIZ_NAME1);
   if (acOwner2[0])
   {
      if (strcmp(acOwner1, acOwner2))
         vmemcpy(pOutbuf+OFF_NAME2, acOwner2, SIZ_NAME2);
      else
         lDupOwners++;
   }

   // Remove ET AL
   if (pTmp = strstr(acOwner1, " ET AL") )
      *pTmp = 0;

   // Remove bad char
   iTmp = replChar(acOwner1, 0xA5, 'N');
   //if (iTmp)
   //   iTmp = 0;

   // Now parse owners
   try {
      iTmp = splitOwner(acOwner1, &myOwner1, 3);
   } catch (...)
   {
      iTmp = -1;
      LogMsg("*** Bad char in owner name: %s APN=%.12s", acOwner1, pOutbuf);
   }

   if (iTmp == -1 || strstr(acOwner1, "CLUB"))
      vmemcpy(pOutbuf+OFF_NAME_SWAP, acOwner1, SIZ_NAME_SWAP);
   else
      vmemcpy(pOutbuf+OFF_NAME_SWAP, myOwner1.acSwapName, SIZ_NAME_SWAP);
}

/***************************** Smx_MergeOwnerCsv *****************************
 *
 * - pCareOf may contain part of owner2 or C/O or ATN:, ATTN:, ATNN, "ATT "
 *
 *****************************************************************************/

void Smx_MergeOwnerCsv(char *pOutbuf, char *pOwner1, char *pOwner2, char *pCareOf)
{
   int   iTmp;
   char  acCareOf[128], acOwner1[128], acOwner2[128], acLName[128], *pTmp;

   OWNER    myOwner1;

   // Clear output buffer if needed
   removeNames(pOutbuf, true, true);

   strcpy(acOwner2, pOwner2);
   strcpy(acCareOf, pCareOf);
   if (!memcmp(pCareOf, "C/O", 3)  || !memcmp(pCareOf, "ATTN", 4) || !memcmp(pCareOf, "ATN:", 4) ||
       !memcmp(pCareOf, "ATNN", 4) || !memcmp(pCareOf, "ATT ", 4))
   {
      updateCareOf(pOutbuf, acCareOf);
      acCareOf[0] = 0;
   } else 
   if (!memcmp(pOwner2, "C/O", 3)  || !memcmp(pOwner2, "ATTN", 4) || !memcmp(pOwner2, "ATN:", 4) ||
       !memcmp(pOwner2, "ATNN", 4) || !memcmp(pOwner2, "ATT ", 4))
   {
      updateCareOf(pOutbuf, acOwner2, SMX_SIZ_NAME_2);
      acOwner2[0] = 0;
   }

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "016211030", 9) )
   //   iTmp = 0;
#endif

   // Copy Name1
   strcpy(acOwner1, pOwner1);
   // Fix known bad char
   if ((unsigned char)acOwner1[2] == 0xD1)
      acOwner1[2] = 'N';

   iTmp = replChar(acOwner1, 0xA5, 'N');
   iTmp = replChar(acOwner1, 0xEF, 'N');
   remChar(acOwner1, 0xBF);
   remChar(acOwner1, 0xBD);


   // Save last name
   strcpy(acLName, acOwner1);
   pTmp = strchr(acLName, ' ');
   if (pTmp) *(++pTmp) = 0;

   // If name2 is the same as name1, ignore it
   if (acOwner2[0] > ' ' && strcmp(acOwner1, acOwner2))
   {
      // Check for known bad char
      if (pTmp = strchr(acOwner2, 0xD1))
         *pTmp = 'N';
      if (!memcmp(acOwner2, "OF ", 3))
      {
         strcat(acOwner1, " ");
         strcat(acOwner1, acOwner2);
         acOwner2[0] = 0;
      }
   } else
   {
      acOwner2[0] = 0;
   }

   // Update vesting
   iTmp = updateVesting(myCounty.acCntyCode, acOwner1, pOutbuf+OFF_VEST, pOutbuf+OFF_ETAL_FLG);

   if (pTmp = strchr(acOwner1, '*') )
      *pTmp = 0;

   if (acOwner2[0])
   {
      // If Owner1 and Owner2 have the same last name, combine them if individual names
      if (!memcmp(acLName, acOwner2, strlen(acLName)) )
      {
         if (!strstr(acOwner1, " ET AL")  &&
             !strstr(acOwner1, " TRUST")  &&
             !strstr(acOwner1, " L/E")    &&
             !strstr(acOwner1, " EST OF") &&
             !strstr(acOwner1, " LESSEE") &&
             memcmp(&acOwner1[strlen(acOwner1)-3], " TR", 3) &&
             memcmp(&acOwner2[strlen(acOwner2)-3], " TR", 3) &&
             !strstr(acOwner2, " TRUST"))
         {
            iTmp = MergeName1(acOwner1, acOwner2, acLName);
            if (iTmp)
            {
               acOwner2[0] = 0;
               strcpy(acOwner1, acLName);
            }
         }
      }
   }

   // Save owners
   vmemcpy(pOutbuf+OFF_NAME1, acOwner1, SIZ_NAME1);
   if (acOwner2[0])
   {
      if (strcmp(acOwner1, acOwner2))
         vmemcpy(pOutbuf+OFF_NAME2, acOwner2, SIZ_NAME2);
      else
         lDupOwners++;
   }

   // Remove ET AL
   if (pTmp = strstr(acOwner1, " ET AL") )
      *pTmp = 0;

   // Now parse owners
   try {
      iTmp = splitOwner(acOwner1, &myOwner1, 3);
   } catch (...)
   {
      iTmp = -1;
      LogMsg("*** Bad char in owner name: %s APN=%.12s", acOwner1, pOutbuf);
   }

   if (iTmp == -1 || strstr(acOwner1, "CLUB"))
      vmemcpy(pOutbuf+OFF_NAME_SWAP, acOwner1, SIZ_NAME_SWAP);
   else
      vmemcpy(pOutbuf+OFF_NAME_SWAP, myOwner1.acSwapName, SIZ_NAME_SWAP);
}

/********************************* Smx_MergeSAdr *****************************
 *
 * Return 0 if successful, >0 if error
 *
 *****************************************************************************/

void Smx_MergeSAdr(char *pOutbuf, char *pRollRec)
{
   char     acTmp[256], acAddr1[128], acAddr2[128], acCode[64], *pTmp;
   int      iTmp;
   ADR_REC  sSitusAdr;
   SMX_ROLL *pRec;

   pRec = (SMX_ROLL *)pRollRec;

   // Situs
   acAddr1[0] = 0;
   acAddr2[0] = 0;
   memset((void *)&sSitusAdr, 0, sizeof(ADR_REC));
   pRec->Apn[iApnLen] = 0;

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "100540060", 9) )
   //   iTmp = 0;
#endif

   if (memcmp(pRec->St_Num, "        ", 8) && memcmp(pRec->St_Num, "00000000", 8) && memcmp(pRec->Name, "        ", 8))
   {
      // Remove old adr
      removeSitus(pOutbuf);

      // Formatting new address
      memcpy(acTmp, pRec->St_Num, SMX_SIZ_ST_NUM);
      blankRem(acTmp, SMX_SIZ_ST_NUM);

      long lTmp = atoin(acTmp, SMX_SIZ_ST_NUM);
      if (lTmp > 0)
      {
         // StrNum
         iTmp = sprintf(sSitusAdr.strNum, "%d", lTmp);
         memcpy(pOutbuf+OFF_S_STRNUM, sSitusAdr.strNum, iTmp);
         memcpy(pOutbuf+OFF_S_HSENO, sSitusAdr.strNum, iTmp);

         // StrFra
         if (pRec->Fraction[0] > ' ')
         {
            memcpy(sSitusAdr.strSub, pRec->Fraction, SMX_SIZ_FRACTION);
            sSitusAdr.strSub[SMX_SIZ_FRACTION] = 0;
            memcpy(pOutbuf+OFF_S_STR_SUB, pRec->Fraction, SMX_SIZ_FRACTION);
         }

         // Direction
         if (pRec->Direction[0] > ' ')
         {
            memcpy(pOutbuf+OFF_S_DIR, pRec->Direction, SIZ_S_DIR);
            memcpy(sSitusAdr.strDir, pRec->Direction, SIZ_S_DIR);
            sSitusAdr.strDir[SIZ_S_DIR] = 0;
         } else if (pRec->UnitNo[0] >= 'E')
         {
            memcpy(acTmp, pRec->UnitNo, SMX_SIZ_UNITNO);
            iTmp = blankRem(acTmp, SMX_SIZ_UNITNO);
            if (isDir(acTmp))
            {
               memcpy(pOutbuf+OFF_S_DIR, acTmp, iTmp);
               strcpy(sSitusAdr.strDir, acTmp);
               pRec->UnitNo[0] = ' ';
            }
         }

         // Check UnitNo in Legal
         if (pRec->UnitNo[0] == ' ' && !memcmp(pRec->Legal_Desc_1, "UNIT ", 5))
         {
            memcpy(acTmp, &pRec->Legal_Desc_1[5], 10);
            acTmp[10] = 0;
            if (pTmp = strchr(acTmp, ' '))
            {
               *pTmp = 0;
               // Take single unit only
               if (!(pTmp = strchr(acTmp, '-')))
               {
                  strcpy(pRec->UnitNo, acTmp);
                  lLglUnit++;
               }
            }
         }
         
         // StrName
         memcpy(sSitusAdr.strName, pRec->Name, SIZ_S_STREET);
         iTmp = blankRem(sSitusAdr.strName, SIZ_S_STREET);
         if (iTmp > 0)
            memcpy(pOutbuf+OFF_S_STREET, sSitusAdr.strName, iTmp);

         // StrSfx
         if (pRec->Type[0] > ' ')
         {
            memcpy(sSitusAdr.SfxName, pRec->Type, SMX_SIZ_TYPE);
            blankRem(sSitusAdr.SfxName, SMX_SIZ_TYPE);
            int iSfxCode = GetSfxCode(sSitusAdr.SfxName);
            if (iSfxCode > 0)
            {
               iTmp = sprintf(sSitusAdr.strSfx, "%d", iSfxCode);
               memcpy(pOutbuf+OFF_S_SUFF, sSitusAdr.strSfx, iTmp);
            }
         }

         // UnitNo - do not add '#' since Unit# is too long
         if (pRec->UnitNo[0] > ' ')
         {
            if (!memcmp(pRec->UnitNo, "APT", 3) || !memcmp(pRec->UnitNo, "STE", 3))
            {
               memcpy(&sSitusAdr.Unit[0], &pRec->UnitNo[4], SMX_SIZ_UNITNO-4);
               //sSitusAdr.Unit[0] = '#';
               sSitusAdr.Unit[SMX_SIZ_UNITNO-3] = 0;
            } else if (!memcmp(pRec->UnitNo, "UNIT", 4) )
            {
               // 018074050
               memcpy(&sSitusAdr.Unit[0], &pRec->UnitNo[5], SMX_SIZ_UNITNO-5);
               //sSitusAdr.Unit[0] = '#';
               sSitusAdr.Unit[SMX_SIZ_UNITNO-4] = 0;
            } else if (!memcmp(pRec->UnitNo, "SUIT ", 5) )
            {
               // 046090290
               memcpy(&sSitusAdr.Unit[0], &pRec->UnitNo[5], SMX_SIZ_UNITNO-5);
               //sSitusAdr.Unit[0] = '#';
               sSitusAdr.Unit[SMX_SIZ_UNITNO-4] = 0;
            } else if (!memcmp(pRec->UnitNo, "BLDG", 4) || !memcmp(pRec->UnitNo, "UPPR", 4) || !memcmp(pRec->UnitNo, "LOWR", 4))
            {
               // 035011140 (BLDG A)
               // 050272010 (UPPR)
               // 050273420 (POWR)
               memcpy(sSitusAdr.Unit, pRec->UnitNo, SMX_SIZ_UNITNO);
               sSitusAdr.Unit[SIZ_S_UNITNO] = 0;
            } else if (pRec->UnitNo[0] == '#')
            {
               // 056176120 (#4,#5,#6)
               if (pRec->UnitNo[1] == ' ')
               {
                  // 046100350 (# 100-22)
                  memcpy(&sSitusAdr.Unit[0], &pRec->UnitNo[2], SMX_SIZ_UNITNO-2);
                  //sSitusAdr.Unit[0] = '#';
               } else
               {
                  memcpy(acTmp, &pRec->UnitNo[1], SMX_SIZ_UNITNO-1);
                  iTmp = iTrim(acTmp, SMX_SIZ_UNITNO-1);
                  if (iTmp == 5 && isNumber(acTmp, 5) && acTmp[0] == '7')
                  {
                     sprintf(sSitusAdr.Unit, "%.3s-%.2s", acTmp, &acTmp[3]);
                     LogMsg("*** Convert UnitNo: %s to %s [%.10s]", acTmp, sSitusAdr.Unit, pOutbuf);
                  } else
                     memcpy(sSitusAdr.Unit, &pRec->UnitNo[1], SMX_SIZ_UNITNO-1);
               }
               sSitusAdr.Unit[SIZ_S_UNITNO] = 0;
            } else
            {
               // 014081220, 015010220 (400-500)
               // 015010610 (200& 210)
               // 015142010 (S100-180)
               // 015164080 (A,B,C,&D)
               // 034177030 (A B &C)
               memcpy(&sSitusAdr.Unit[0], pRec->UnitNo, SIZ_S_UNITNO);
               //sSitusAdr.Unit[0] = '#';
               //if (strstr(sSitusAdr.Unit, "UNI") || !memcmp(pRec->UnitNo, "CUT", 3))
               //   memcpy(sSitusAdr.Unit, pRec->UnitNo, SIZ_S_UNITNO);
               sSitusAdr.Unit[SIZ_S_UNITNO] = 0;
            }
            iTmp = blankRem(sSitusAdr.Unit);
            memcpy(pOutbuf+OFF_S_UNITNO, sSitusAdr.Unit, iTmp);
         }

         sprintf(acTmp, "%s %s %s %s %s %s", sSitusAdr.strNum, sSitusAdr.strSub, sSitusAdr.strDir,
            sSitusAdr.strName, sSitusAdr.SfxName, sSitusAdr.Unit);

         iTmp = blankRem(acTmp);
         vmemcpy(pOutbuf+OFF_S_ADDR_D, acTmp, SIZ_S_ADDR_D, iTmp);
      }

      // Parsing city name
      memcpy(acTmp, pRec->City, SMX_SIZ_CITY);
      acTmp[SMX_SIZ_CITY] = 0;

      // Encode city
      if (acTmp[0] >= 'A')
      {
         City2Code(acTmp, acCode, pRec->Apn);
         if (acCode[0] > ' ')
         {
            memcpy(pOutbuf+OFF_S_CITY, acCode, strlen(acCode));
            memcpy(pOutbuf+OFF_S_ST, "CA",2);
            strcat(acTmp, " CA");
            iTmp = blankRem(acTmp);
            vmemcpy(pOutbuf+OFF_S_CTY_ST_D, acTmp, SIZ_S_CTY_ST_D, iTmp);
         }
      }
   }

}

void Smx_MergeSitus(char *pOutbuf)
{
   char     acTmp[256], acAddr1[128], acAddr2[128], acCode[64], *pTmp;
   int      iTmp;
   ADR_REC  sSitusAdr;

   // Situs
   acAddr1[0] = 0;
   acAddr2[0] = 0;
   memset((void *)&sSitusAdr, 0, sizeof(ADR_REC));

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "100540060", 9) )
   //   iTmp = 0;
#endif

   if (*apTokens[SMX_R_S_STRNUM] > '0' && *apTokens[SMX_R_S_STRNAME] > '0')
   {
      // Remove old adr
      removeSitus(pOutbuf);

      // Formatting new address
      long lTmp = atol(apTokens[SMX_R_S_STRNUM]);
      if (lTmp > 0)
      {
         // StrNum
         iTmp = sprintf(sSitusAdr.strNum, "%d", lTmp);
         memcpy(pOutbuf+OFF_S_STRNUM, sSitusAdr.strNum, iTmp);
         if (*apTokens[SMX_R_S_STRNUM2] > '0')
         {
            iTmp = sprintf(sSitusAdr.HseNo, "%s-%s", apTokens[SMX_R_S_STRNUM], apTokens[SMX_R_S_STRNUM2]);
            vmemcpy(pOutbuf+OFF_S_HSENO, sSitusAdr.HseNo, SIZ_S_HSENO, iTmp);
         } else
         {
            strcpy(sSitusAdr.HseNo, apTokens[SMX_R_S_STRNUM]);
            memcpy(pOutbuf+OFF_S_HSENO, sSitusAdr.strNum, iTmp);
         }

         // StrFra
         if (*apTokens[SMX_R_S_STRFRA] > '0')
         {
            vmemcpy(sSitusAdr.strSub, apTokens[SMX_R_S_STRFRA], SIZ_M_STR_SUB);
            memcpy(pOutbuf+OFF_S_STR_SUB, apTokens[SMX_R_S_STRFRA], SIZ_M_STR_SUB);
         }

         // Direction
         if (*apTokens[SMX_R_S_STRDIR] > '0')
         {
            vmemcpy(pOutbuf+OFF_S_DIR, apTokens[SMX_R_S_STRDIR], SIZ_S_DIR);
            vmemcpy(sSitusAdr.strDir, apTokens[SMX_R_S_STRDIR], SIZ_S_DIR);
         } else if (*apTokens[SMX_R_S_STRUNIT] >= 'E')
         {
            if (isDir(apTokens[SMX_R_S_STRUNIT]))
            {
               vmemcpy(pOutbuf+OFF_S_DIR, apTokens[SMX_R_S_STRUNIT], SIZ_S_DIR);
               strcpy(sSitusAdr.strDir, apTokens[SMX_R_S_STRUNIT]);
               *apTokens[SMX_R_S_STRUNIT] = ' ';
            }
         }

#ifdef _DEBUG
         //if (!memcmp(pOutbuf, "123570150", 9) )
         //   iTmp = 0;
#endif
         // Check UnitNo in Legal
         if (*apTokens[SMX_R_S_STRUNIT] == ' ' && !memcmp(apTokens[SMX_R_LEGALDESC], "UNIT ", 5))
         {
            memcpy(acTmp, apTokens[SMX_R_LEGALDESC]+5, 10);
            acTmp[10] = 0;
            if (pTmp = strchr(acTmp, ' '))
            {
               *pTmp = 0;
               strcpy(sSitusAdr.UnitNox, acTmp);
               // Take single unit only
               if ((pTmp = strchr(acTmp, '-')))
               {
                  *pTmp = 0;
                  strcpy(sSitusAdr.Unit, acTmp);
                  lLglUnit++;
               } else
                  strcpy(sSitusAdr.Unit, acTmp);
            }
         }
         
         // StrName
         strcpy(sSitusAdr.strName, apTokens[SMX_R_S_STRNAME]);
         iTmp = blankRem(sSitusAdr.strName, SIZ_S_STREET);
         if (iTmp > 0)
            memcpy(pOutbuf+OFF_S_STREET, sSitusAdr.strName, iTmp);

         // StrSfx
         if (*apTokens[SMX_R_S_STRTYPE] > ' ')
         {
            strcpy(sSitusAdr.SfxName, apTokens[SMX_R_S_STRTYPE]);
            int iSfxCode = GetSfxCode(sSitusAdr.SfxName);
            if (iSfxCode > 0)
            {
               iTmp = sprintf(sSitusAdr.strSfx, "%d", iSfxCode);
               memcpy(pOutbuf+OFF_S_SUFF, sSitusAdr.strSfx, iTmp);
            }
         }

         // UnitNo - do not add '#' since Unit# is too long
         if (*apTokens[SMX_R_S_STRUNIT] > ' ')
         {
            if (strstr(apTokens[SMX_R_S_STRUNIT], "# "))
            {
               sprintf(acTmp, "#%s", apTokens[SMX_R_S_STRUNIT]+2);
               strcpy(apTokens[SMX_R_S_STRUNIT], acTmp);
            }
            strncpy(sSitusAdr.UnitNox, apTokens[SMX_R_S_STRUNIT], SIZ_M_UNITNOX);
            if (!memcmp(apTokens[SMX_R_S_STRUNIT], "APT", 3) || !memcmp(apTokens[SMX_R_S_STRUNIT], "STE", 3))
            {
               strncpy(sSitusAdr.Unit, apTokens[SMX_R_S_STRUNIT]+4, SIZ_M_UNITNO);
            } else if (!memcmp(apTokens[SMX_R_S_STRUNIT], "UNIT", 4) || !memcmp(apTokens[SMX_R_S_STRUNIT], "SUIT ", 5) )
            {
               // 018074050
               strcpy(&sSitusAdr.Unit[0], apTokens[SMX_R_S_STRUNIT]+5);
            } else if (!memcmp(apTokens[SMX_R_S_STRUNIT], "BLDG", 4) || 
                       !memcmp(apTokens[SMX_R_S_STRUNIT], "UPPR", 4) || 
                       !memcmp(apTokens[SMX_R_S_STRUNIT], "LOWR", 4))
            {
               // 035011140 (BLDG A)
               // 050272010 (UPPR)
               // 050273420 (POWR)
               strncpy(sSitusAdr.Unit, apTokens[SMX_R_S_STRUNIT], SIZ_M_UNITNO);
            } else if (*apTokens[SMX_R_S_STRUNIT] == '#')
            {
               // 056176120 (#4,#5,#6)
               if (*(apTokens[SMX_R_S_STRUNIT]+1) == ' ')
               {
                  // 046100350 (# 100-22)
                  strcpy(&sSitusAdr.Unit[0], apTokens[SMX_R_S_STRUNIT]+2);
               } else
               {
                  strcpy(acTmp, apTokens[SMX_R_S_STRUNIT]+1);
                  iTmp = iTrim(acTmp);
                  if (iTmp == 5 && isNumber(acTmp, 5) && acTmp[0] == '7')
                  {
                     sprintf(sSitusAdr.Unit, "%.3s-%.2s", acTmp, &acTmp[3]);
                     LogMsg("*** Convert UnitNo: %s to %s [%.10s]", acTmp, sSitusAdr.Unit, pOutbuf);
                  } else
                     strcpy(sSitusAdr.Unit, acTmp);
               }
               sSitusAdr.Unit[SIZ_S_UNITNO] = 0;
            } else
            {
               // 014081220, 015010220 (400-500)
               // 015010610 (200& 210)
               // 015142010 (S100-180)
               // 015164080 (A,B,C,&D)
               // 034177030 (A B &C)
               strcpy(&sSitusAdr.Unit[0], apTokens[SMX_R_S_STRUNIT]);
            }
            vmemcpy(pOutbuf+OFF_S_UNITNO, sSitusAdr.Unit, SIZ_S_UNITNO);
            vmemcpy(pOutbuf+OFF_S_UNITNOX, sSitusAdr.UnitNox, SIZ_S_UNITNOX);
         }

         if (sSitusAdr.strSub[0] > ' ')
            sprintf(acTmp, "%s %s %s %s %s %s", sSitusAdr.strNum, sSitusAdr.strSub, sSitusAdr.strDir,
               sSitusAdr.strName, sSitusAdr.SfxName, apTokens[SMX_R_S_STRUNIT]);
         else
            sprintf(acTmp, "%s %s %s %s %s", sSitusAdr.HseNo, sSitusAdr.strDir,
               sSitusAdr.strName, sSitusAdr.SfxName, apTokens[SMX_R_S_STRUNIT]);

         iTmp = blankRem(acTmp);
         vmemcpy(pOutbuf+OFF_S_ADDR_D, acTmp, SIZ_S_ADDR_D, iTmp);
      }

      // Encode city
      if (*apTokens[SMX_R_S_CITY] >= 'A')
      {
         strcpy(acTmp, apTokens[SMX_R_S_CITY]);
         City2Code(acTmp, acCode, SMX_R_APN);
         if (acCode[0] > ' ')
         {
            memcpy(pOutbuf+OFF_S_CITY, acCode, strlen(acCode));
            memcpy(pOutbuf+OFF_S_ST, "CA",2);
            strcat(acTmp, " CA");
            iTmp = blankRem(acTmp);
            vmemcpy(pOutbuf+OFF_S_CTY_ST_D, acTmp, SIZ_S_CTY_ST_D, iTmp);
         }
      }
   }
}

/********************************* parseMAdr1() *****************************
 *
 * Copy from R01.cpp and customize for SMX
 *
 ****************************************************************************/

void Smx_parseMAdr1(ADR_REC *pAdrRec, char *pAddr)
{
   char  acAdr[256], acStrName[256], acSfx[64], acTmp[256], *apItems[16], *pTmp;
   char *pAdr;
   int   iCnt, iTmp, iIdx, iDir;
   bool  bRet, bDir;

   // Check for PO Box
   if (!memcmp(pAddr, "PO BOX", 6)   ||
       !memcmp(pAddr, "P.O. BOX", 8) ||
       !memcmp(pAddr, "P O ", 4)     ||
       !memcmp(pAddr, "PO BX", 5)    ||
       !memcmp(pAddr, "BOX ", 4)     ||
       !memcmp(pAddr, "POB ", 4) )
   {
      strcpy(pAdrRec->strName, pAddr);
      return;
   }

   acStrName[0] = 0;
   pAdr = pAddr;
   if (*pAdr == '%')
   {
      pTmp = strchr(pAdr+3, ' ');
      if (pTmp && isdigit(*++pTmp))
         pAdr = pTmp;
      else
         return;        // bad address
   }

   // Remove leading space - SFX
   while (*pAdr && *pAdr == ' ') pAdr++;

   // Remove '.' and extra space
   iTmp = 0;
   while (*pAdr)
   {
      // Remove dot, comma, and single quote
      if (*pAdr != '.' && *pAdr != ',' && *pAdr != 39)
         acAdr[iTmp++] = *pAdr;

      // Remove multi-space
      if (*pAdr == ' ')
         while (*pAdr == ' ') pAdr++;
      else
         pAdr++;
   }
   acAdr[iTmp] = 0;

   // Tokenize address
   iCnt = ParseString(acAdr, 32, 16, apItems);

   // If strNum is 0, do not parse address
   iTmp = atoi(apItems[0]);
   if (iTmp == 0 || iCnt == 1)
   {
      strcpy(acStrName, pAddr);
      pAdrRec->lStrNum = 0;
   } else
   {
      if (apItems[0][strlen(apItems[0])-1] > '9')
      {
         // StrSub ?
         pTmp = apItems[0];
         while (*pTmp && isdigit(*pTmp) )
            pTmp++;
         if (*pTmp && (*pTmp == '-' || *pTmp == '/'))
            pTmp++;
         if (*pTmp && strlen(pTmp) <= SIZ_M_STR_SUB)
            strcpy(pAdrRec->strSub, pTmp);
      }

      strcpy(pAdrRec->HseNo, apItems[0]);
      pAdrRec->lStrNum = iTmp;
      sprintf(acTmp, "%d", iTmp);
      strcpy(pAdrRec->strNum, acTmp);
      iIdx = 1;

      // Fraction - Make sure this is not the last token
      if (strchr(apItems[iIdx], '/') && (iCnt > (iIdx+1)) && (strlen(apItems[iIdx]) <= SIZ_S_STR_SUB))
      {
         strcpy(pAdrRec->strSub, apItems[iIdx++]);
         if (!strcmp(apItems[iIdx], "RD") || !strcmp(apItems[iIdx], "ROAD") || !strcmp(apItems[iIdx], "AVE"))
         {
            if (iCnt > iIdx+1)
            {
               sprintf(acStrName, "%s %s", apItems[iIdx], apItems[iIdx+1]);
               iIdx += 2;
               if (iIdx >= iCnt)
               {
                  strcpy(pAdrRec->strName, acStrName);
                  return;
               }
            }
         }
      }

      // Unit is sometimes placed after strnum as in: 9416 #A CARLTON OAKS
      if (apItems[iIdx][0] == '#')
      {
         strncpy(pAdrRec->UnitNox, apItems[iIdx], SIZ_M_UNITNOX);
         if (strlen(apItems[iIdx]) > SIZ_M_UNITNO)
            strncpy(pAdrRec->Unit, (char *)&apItems[iIdx][1], SIZ_M_UNITNO);
         else
            strncpy(pAdrRec->Unit, (char *)&apItems[iIdx][0], SIZ_M_UNITNO);
         iIdx++;
      }

      if (iIdx >= iCnt)
      {
         // Bad address
         return;
      }

      // Dir
      iDir = 0;
      bDir = false;
      strcpy(acSfx, apItems[iIdx]);
      if (strlen(apItems[iIdx]) < 3)
      {
         if (acSfx[1] == '.')
            acSfx[1] = 0;

         while (iDir <= 7)
         {
            if (!strcmp(acSfx, asDir[iDir]))
            {
               bDir = true;
               break;
            }
            iDir++;
         }
         if (!bDir)
         {
            iDir = 0;
            while (iDir <= 1)
            {
               if (!strcmp(acSfx, asDir2[iDir]))
               {
                  bDir = true;
                  break;
               }
               iDir++;
            }
         }
      } else
      {
         // Check for long direction
         while (iDir < 4)
         {
            if (!strcmp(apItems[iIdx], asDir1[iDir]))
            {
               bDir = true;
               break;
            }
            iDir++;
         }
      }

      // Possible cases E ST or W RD
      // Check for suffix
      bRet = false;
      //if (iTmp <= 7 && iCnt > 2)
      if (bDir && iCnt > 2)
      {
         // If next item is suffix, then this one is not direction
         // if (iTmp=GetSfxCode(apItems[iIdx+1]) && iCnt==iIdx+2)
         // The above statement won't work if Unit# is included.
         //if ((iTmp=GetSfxDev(apItems[iIdx+1])) &&
         if ((iTmp=GetSfxCodeX(apItems[iIdx+1], acTmp)) &&
             memcmp(apItems[iIdx+1], "HWY", 3) && 
             memcmp(apItems[iIdx+1], "HIGHWAY", 7)) 
         {
            strncpy(acStrName, apItems[iIdx], SIZ_M_STREET);
            //strcpy(pAdrRec->strSfx, GetSfxStr(iTmp));
            strcpy(pAdrRec->strSfx, acTmp);
            sprintf(acTmp, "%d", iTmp);
            strcpy(pAdrRec->SfxCode, acTmp);
            bRet = true;         // Done
         } else
         {
            // This is direction
            strcpy(pAdrRec->strDir, asDir[iDir]);
            iIdx++;
         }
      }

      // Check for unit #
      if (apItems[iCnt-1][0] == '#')
      {
         strncpy(pAdrRec->UnitNox, apItems[iCnt-1], SIZ_M_UNITNOX);
         if (strlen(apItems[iCnt-1]) > SIZ_M_UNITNO)
            strncpy(pAdrRec->Unit, (char *)&apItems[iCnt-1][1], SIZ_M_UNITNO);
         else
            strncpy(pAdrRec->Unit, (char *)&apItems[iCnt-1][0], SIZ_M_UNITNO);
         iCnt--;

         // If last token is STE or SUITE, remove it.
         if (!strcmp(apItems[iCnt-1], "STE") || !strcmp(apItems[iCnt-1], "SUITE")
            || !strcmp(apItems[iCnt-1], "UNIT") || !strcmp(apItems[iCnt-1], "APT")
            || !strcmp(apItems[iCnt-1], "SP") || !strcmp(apItems[iCnt-1], "RM") )
            iCnt--;
      } else if (!strcmp(apItems[iCnt-2], "STE") || !strcmp(apItems[iCnt-2], "SUITE")
         || !strcmp(apItems[iCnt-2], "UNIT") || !strcmp(apItems[iCnt-2], "UNIT#")
         || !strcmp(apItems[iCnt-2], "APT")  || !strcmp(apItems[iCnt-2], "APTS") || !strcmp(apItems[iCnt-2], "APT#")
         || !strcmp(apItems[iCnt-2], "RM")   || !strcmp(apItems[iCnt-2], "SP")   || !strcmp(apItems[iCnt-2], "LOT") )
      {
         sprintf(acTmp, "#%s", apItems[iCnt-1]);
         strncpy(pAdrRec->Unit, acTmp, SIZ_M_UNITNO);
         strncpy(pAdrRec->UnitNox, acTmp, SIZ_M_UNITNOX);
         iCnt -= 2;
      } else if ((pTmp=strchr(apItems[iCnt-1], '#')))
      {  // 15025 NE SACRAMENTO ST#54, 838 S GRAND AVE APT#611
         if (!memcmp(apItems[iCnt-1], "SP", 2))
         {
            *pTmp = ' ';
         } else
         {
            strncpy(pAdrRec->UnitNox, pTmp, SIZ_M_UNITNOX);
            strncpy(pAdrRec->Unit, pTmp, SIZ_M_UNITNO);
            *pTmp = 0;
            if (!strcmp(apItems[iCnt-1], "APT"))
               iCnt--;
         }
      } else if (!memcmp(apItems[iCnt-1], "FL-", 3) || !memcmp(apItems[iCnt-1], "FLR-", 4))
      {
         strncpy(pAdrRec->UnitNox, apItems[iCnt-1], SIZ_M_UNITNOX);
         strncpy(pAdrRec->Unit, apItems[iCnt-1], SIZ_M_UNITNO);
         iCnt--;
      } else if (!strcmp(apItems[iCnt-2], "LOT") && atoi(apItems[iCnt-1]) > 0)
      {  // 245 WILDWOOD DR LOT 183, 45055 E FLORIDA AVE SP 36
         sprintf(acTmp, "#%s", apItems[iCnt-1]);
         strncpy(pAdrRec->UnitNox, acTmp, SIZ_M_UNITNOX);
         strncpy(pAdrRec->Unit, acTmp, SIZ_M_UNITNO);
         iCnt -= 2;
      } else if (iCnt > 3 && atoi(apItems[iCnt-1]) > 0 && acStrName[0] <= ' ')
      {  // 555 N DANEBO 33
         // Not 21051 STATE ROUTE 772, 5323 HERMITAGE AVE# 1, 3100-R WEST HWY 86
         if (strcmp(apItems[iCnt-2], "ROUTE") && strcmp(apItems[iCnt-2], "HWY")
            && strcmp(apItems[iCnt-2], "BOX") && strcmp(apItems[iCnt-2], "ROAD")
            && strcmp(apItems[iCnt-2], "SP")  && strcmp(apItems[iCnt-2], "HIGHWAY"))   // MER
         {
            // Check for 123 COUNTY RD 105 on SIS->001110050000
            if (!strstr(pAddr, "COUNTY RD"))
            {
               sprintf(acTmp, "#%s ", apItems[iCnt-1]);
               strncpy(pAdrRec->Unit, acTmp, SIZ_M_UNITNO);
               strncpy(pAdrRec->UnitNox, acTmp, SIZ_M_UNITNOX);
               iCnt--;
            } else
            {
               sprintf(acStrName, "COUNTY ROAD %s", apItems[iCnt-1]);
               bRet = true;
            }

         }

         if (pTmp=strchr(apItems[iCnt-1], '#'))
            *pTmp = 0;
      }

      // Not direction, token is str name
      if (!bRet && iIdx < iCnt)
      {
         // Check suffix
         //if (iTmp=GetSfxDev(apItems[iCnt-1]))
         if (iTmp=GetSfxCodeX(apItems[iCnt-1], acTmp))
         {
            strcpy(pAdrRec->strSfx, acTmp);
            //strcpy(pAdrRec->strSfx, GetSfxStr(iTmp));
            sprintf(acTmp, "%d", iTmp);
            strcpy(pAdrRec->SfxCode, acTmp);

            while (iIdx < iCnt-1)
            {
               // If multiple address, take the first one.
               // This loop break after first suffix found, ignore the rest.
               // We also must avoid strname like 123 ST PAUL BLVD.  StrSfx must
               // be indexed > 1
               if ((iIdx > 1) &&
                   (iIdx < iCnt-2) &&
                   (iTmp=GetSfxCodeX(apItems[iIdx], acTmp)) )
                   //(iTmp=GetSfxDev(apItems[iIdx])) )
               {
                  //strcpy(pAdrRec->strSfx, GetSfxStr(iTmp));
                  strcpy(pAdrRec->strSfx, acTmp);
                  sprintf(acTmp, "%d", iTmp);
                  strcpy(pAdrRec->SfxCode, acTmp);
                  break;
               }
               strcat(acStrName, apItems[iIdx++]);
               if (iIdx < iCnt)
                  strcat(acStrName, " ");
            }
         } else if (!strcmp(apItems[iCnt-2], "HWY"))
         {  // 123 HWY 70
            //strcpy(pAdrRec->strName, apItems[iCnt-2]);
            //strcat(pAdrRec->strName, " ");
            //strcat(pAdrRec->strName, apItems[iCnt-1]);
            sprintf(acStrName, "%s %s", apItems[iCnt-2], apItems[iCnt-1]);
         } else
         {
            // Scan for suffix in multi address cases.  Keep the first one
            // 123 MAIN ST 540 N SANTA ANA BLVD
            while (iIdx < iCnt)
            {
               if (iIdx > 1)
               {
                  //if (iTmp = GetSfxDev(apItems[iIdx]))
                  if (iTmp = GetSfxCodeX(apItems[iIdx], acTmp))
                  {
                     //strcpy(pAdrRec->strSfx, pTmp);
                     strcpy(pAdrRec->strSfx, acTmp);
                     sprintf(acTmp, "%d", iTmp);
                     strcpy(pAdrRec->SfxCode, acTmp);
                     break;
                  }
               }

               //if (strlen(apItems[iIdx]) > SIZ_M_STREET)
               //   memcpy(acStrName, apItems[iIdx], SIZ_M_STREET);
               //else
               strcat(acStrName, apItems[iIdx]);
               iIdx++;
               if (iIdx < iCnt)
                  strcat(acStrName, " ");
            }
         }
      }
   }
   strncpy(pAdrRec->strName, acStrName, SIZ_M_STREET);
}

/********************************** Smx_MergeMAdr ****************************
 *
 * C/O is often on Name3
 * No DBA found anywhere
 *
 * Return 0 if successful, >0 if error
 *
 *****************************************************************************/

void Smx_MergeMAdr(char *pOutbuf, char *pRollRec)
{
   char     acTmp[256], acAddr1[64];
   int      iTmp;

   SMX_ROLL *pRec;
   ADR_REC  sMailAdr;

   pRec = (SMX_ROLL *)pRollRec;

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "100540030", 9) )
   //   iTmp = 0;
#endif

   // Ignore bad mail addr.
   if (!_memicmp(pRec->Maddr1, "Situs not", 9))
      return;

   // Clear old Mailing
   removeMailing(pOutbuf, false);

   // Check for blank address
   if (memcmp(pRec->Maddr1, "     ", 5))
   {
      memcpy(acAddr1, pRec->Maddr1, SMX_SIZ_MADDR1);
      blankRem(acAddr1, SMX_SIZ_MADDR1);

      // Parsing mail address
      memset((void *)&sMailAdr, 0, sizeof(ADR_REC));
      Smx_parseMAdr1(&sMailAdr, acAddr1);

      if (sMailAdr.lStrNum > 0)
      {
         iTmp = sprintf(acTmp, "%d", sMailAdr.lStrNum);
         memcpy(pOutbuf+OFF_M_STRNUM, acTmp, iTmp);
         if (sMailAdr.strSub[0] > '0')
         {
            sprintf(acTmp, "%s  ", sMailAdr.strSub);
            memcpy(pOutbuf+OFF_M_STR_SUB, acTmp, SIZ_M_STR_SUB);
         }
      }
      memcpy(pOutbuf+OFF_M_DIR, sMailAdr.strDir, strlen(sMailAdr.strDir));
      memcpy(pOutbuf+OFF_M_STREET, sMailAdr.strName, strlen(sMailAdr.strName));
      memcpy(pOutbuf+OFF_M_SUFF, sMailAdr.strSfx, strlen(sMailAdr.strSfx));
      if (sMailAdr.Unit[0] > ' ')
      {
         iTmp = strlen(sMailAdr.Unit);
         if (iTmp > SIZ_M_UNITNO && sMailAdr.Unit[0] == '#')
            memcpy(pOutbuf+OFF_M_UNITNO, &sMailAdr.Unit[1], iTmp-1);
         else
            memcpy(pOutbuf+OFF_M_UNITNO, sMailAdr.Unit, iTmp);
      }

      vmemcpy(pOutbuf+OFF_M_ADDR_D, acAddr1, SIZ_M_ADDR_D);

      // City-state
      memcpy(acTmp, pRec->Mcity, SMX_SIZ_MCITY);
      iTmp = blankRem(acTmp, SIZ_M_CITY);
      vmemcpy(pOutbuf+OFF_M_CITY, acTmp, SIZ_M_CITY, iTmp);
      vmemcpy(pOutbuf+OFF_M_ST, pRec->Mstate, SIZ_M_ST);

      iTmp = sprintf(acAddr1, "%s %.2s", acTmp, pRec->Mstate);
      vmemcpy(pOutbuf+OFF_M_CTY_ST_D, acAddr1, SIZ_M_CTY_ST_D, iTmp);

      // Zipcode
      iTmp = atoin(pRec->Mzip, SMX_SIZ_MZIP);
      if (iTmp > 500)
      {
         vmemcpy(pOutbuf+OFF_M_ZIP, pRec->Mzip, SIZ_M_ZIP);
         iTmp = atoin(pRec->Mzip4, SIZ_M_ZIP4);
         if (iTmp > 0)
            vmemcpy(pOutbuf+OFF_M_ZIP4, pRec->Mzip4, SIZ_M_ZIP4);
      }
   }
}

void Smx_MergeMailing(char *pOutbuf)
{
   char     acTmp[256], acAddr1[64], *pTmp;
   int      iTmp;

   ADR_REC  sMailAdr;

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "123580010", 9) )
   //   iTmp = 0;
#endif

   // Clear old Mailing
   removeMailing(pOutbuf, false);

   // Check for blank address
   if (*apTokens[SMX_R_M_ADDRESS1] > ' ')
   {
      strcpy(acAddr1, apTokens[SMX_R_M_ADDRESS1]);

      // Parsing mail address
      memset((void *)&sMailAdr, 0, sizeof(ADR_REC));
      Smx_parseMAdr1(&sMailAdr, acAddr1);

      if (sMailAdr.lStrNum > 0)
      {
         iTmp = sprintf(acTmp, "%d", sMailAdr.lStrNum);
         memcpy(pOutbuf+OFF_M_STRNUM, acTmp, iTmp);
         if (sMailAdr.strSub[0] > '0')
         {
            sprintf(acTmp, "%s  ", sMailAdr.strSub);
            memcpy(pOutbuf+OFF_M_STR_SUB, acTmp, SIZ_M_STR_SUB);
         }
      }
      memcpy(pOutbuf+OFF_M_DIR, sMailAdr.strDir, strlen(sMailAdr.strDir));
      memcpy(pOutbuf+OFF_M_STREET, sMailAdr.strName, strlen(sMailAdr.strName));
      memcpy(pOutbuf+OFF_M_SUFF, sMailAdr.strSfx, strlen(sMailAdr.strSfx));
      if (sMailAdr.Unit[0] > ' ')
      {
         vmemcpy(pOutbuf+OFF_M_UNITNOX, sMailAdr.UnitNox, SIZ_M_UNITNOX);
         vmemcpy(pOutbuf+OFF_M_UNITNO, sMailAdr.Unit, SIZ_M_UNITNO);
      }

      vmemcpy(pOutbuf+OFF_M_ADDR_D, acAddr1, SIZ_M_ADDR_D);

      // City-state
      iTmp = strlen(apTokens[SMX_R_M_CITY]);
      if (iTmp > SIZ_M_CITY)
      {
         memcpy(pOutbuf+OFF_M_CITY, apTokens[SMX_R_M_CITY], SIZ_M_CITY);
         vmemcpy(pOutbuf+OFF_M_CITYX, &apTokens[SMX_R_M_CITY][SIZ_M_CITY], SIZ_M_CITYX);
      } else
         vmemcpy(pOutbuf+OFF_M_CITY, apTokens[SMX_R_M_CITY], SIZ_M_CITY);
      vmemcpy(pOutbuf+OFF_M_ST, apTokens[SMX_R_M_STATE], SIZ_M_ST);

      iTmp = sprintf(acAddr1, "%s %s", apTokens[SMX_R_M_CITY], apTokens[SMX_R_M_STATE]);
      vmemcpy(pOutbuf+OFF_M_CTY_ST_D, acAddr1, SIZ_M_CTY_ST_D, iTmp);

      // Zipcode
      if (*apTokens[SMX_R_M_ZIP] > ' ')
      {
         if (pTmp = strchr(apTokens[SMX_R_M_ZIP], '-'))
            *pTmp = 0;
         vmemcpy(pOutbuf+OFF_M_ZIP, apTokens[SMX_R_M_ZIP], SIZ_M_ZIP+SIZ_M_ZIP4);
      }
   }
}

/********************************* Smx_MergeChar *****************************
 *
 * Modify 7/6/2018 to use STDCHAR instead of LO_CHAR layout
 *
 *****************************************************************************/

int Smx_MergeChar(char *pOutbuf)
{
   static char    acRec[2048], *pRec=NULL;
   char           acTmp[256];
   long           lTmp;
   int            iRet, iLoop, iBeds, iFBath, iHBath, iFp;
   STDCHAR        *pChar;

   iRet=iBeds=iFBath=iHBath=iFp=0;

   // Get first Char rec for first call
   if (!pRec)
      pRec = fgets(acRec,  2048, fdChar);

   do
   {
      iLoop = memcmp(pOutbuf, acRec, myCounty.iApnLen);
      if (iLoop > 0)
      {
         pRec = fgets(acRec,  2048, fdChar);
         if (!pRec)
         {
            fclose(fdChar);
            fdChar = NULL;
            return 1;      // EOF
         }
      }
   } while (iLoop > 0);

   // If not match, return
   if (iLoop)
      return 1;

   pChar = (STDCHAR *)acRec;

   // UseCode
   if (*(pOutbuf+OFF_USE_CO) == ' ')
   {
      lTmp = atoin(pChar->LandUse, SMX_SIZ_USE_CD1);
      if (lTmp > 0)
      {
         sprintf(acTmp, "%.2d", lTmp);
         memcpy(pOutbuf+OFF_USE_CO, acTmp, 2);
         updateStdUse(pOutbuf+OFF_USE_STD, acTmp, 2, pOutbuf);
      } else
         memcpy(pOutbuf+OFF_USE_STD, USE_UNASGN, SIZ_USE_STD);
   } 

   // Yrblt
   memcpy(pOutbuf+OFF_YR_BLT, pChar->YrBlt, SIZ_YR_BLT);

   // YrEff
   memcpy(pOutbuf+OFF_YR_EFF, pChar->YrEff, SIZ_YR_BLT);

   // BldgSqft
   memcpy(pOutbuf+OFF_BLDG_SF, pChar->BldgSqft, SIZ_BLDG_SF);
   memcpy(pOutbuf+OFF_FLOOR1_SF, pChar->Sqft_1stFl, SIZ_FLOOR1_SF);
   memcpy(pOutbuf+OFF_FLOOR2_SF, pChar->Sqft_2ndFl, SIZ_FLOOR1_SF);
   memcpy(pOutbuf+OFF_FLOOR3_SF, pChar->Sqft_Above2nd, SIZ_FLOOR1_SF);

   // AddlSqft
   memcpy(pOutbuf+OFF_MISCIMPR_SF, pChar->MiscSqft, SIZ_MISCIMPR_SF);

   // BsmtSqft
   memcpy(pOutbuf+OFF_BSMT_SF, pChar->BsmtSqft, SIZ_BSMT_SF);

   // Garage
   if (pChar->ParkType[0] > ' ')
   {
      *(pOutbuf+OFF_PARK_TYPE) = pChar->ParkType[0];
      memcpy(pOutbuf+OFF_PARK_SPACE, pChar->ParkSpace, SIZ_CHAR_SIZE4);
      memcpy(pOutbuf+OFF_GAR_SQFT, pChar->GarSqft, SIZ_GAR_SQFT);
   } else if (pChar->GarSqft[0] > '0')
   {
      memcpy(pOutbuf+OFF_GAR_SQFT, pChar->GarSqft, SIZ_GAR_SQFT);
      *(pOutbuf+OFF_PARK_TYPE) = 'Z';
   } else if (pChar->ParkSpace[0] > '0')
   {
      memcpy(pOutbuf+OFF_PARK_SPACE, pChar->ParkSpace, SIZ_CHAR_SIZE4);
   } else
      *(pOutbuf+OFF_PARK_TYPE) = pChar->ParkType[0];

   // Beds
   iBeds = atoin(pChar->Beds, SIZ_CHAR_BEDS);
   if (iBeds > 0)
   {
      sprintf(acTmp, "%*u", SIZ_BEDS, iBeds);
      memcpy(pOutbuf+OFF_BEDS, acTmp, SIZ_BEDS);
   } else
      memset(pOutbuf+OFF_BEDS, ' ', SIZ_BEDS);

   // Bath
   iFBath = atoin(pChar->FBaths, SIZ_CHAR_BATHS);
   if (iFBath > 0)
   {
      sprintf(acTmp, "%*u", SIZ_BATH_F, iFBath);
      memcpy(pOutbuf+OFF_BATH_F, acTmp, SIZ_BATH_F);
   } else
      memset(pOutbuf+OFF_BATH_F, ' ', SIZ_BATH_F);

   // Half bath
   iHBath = atoin(pChar->HBaths, SIZ_CHAR_BATHS);
   if (iHBath > 0)
   {
      sprintf(acTmp, "%*u", SIZ_BATH_H, iHBath);
      memcpy(pOutbuf+OFF_BATH_H, acTmp, SIZ_BATH_H);
   } else
      memset(pOutbuf+OFF_BATH_H, ' ', SIZ_BATH_H);

   memcpy(pOutbuf+OFF_BATH_1Q, pChar->Bath_1Q, SIZ_BATH_H);
   memcpy(pOutbuf+OFF_BATH_2Q, pChar->Bath_2Q, SIZ_BATH_H);
   memcpy(pOutbuf+OFF_BATH_3Q, pChar->Bath_3Q, SIZ_BATH_H);
   memcpy(pOutbuf+OFF_BATH_4Q, pChar->Bath_4Q, SIZ_BATH_H);

   // Rooms
   iRet = atoin(pChar->Rooms, SIZ_CHAR_ROOMS);
   if (iRet > 0)
   {
      sprintf(acTmp, "%*u", SIZ_ROOMS, iRet);
      memcpy(pOutbuf+OFF_ROOMS, acTmp, SIZ_ROOMS);
   } else
      memset(pOutbuf+OFF_ROOMS, ' ', SIZ_ROOMS);

   // Fireplace
   memcpy(pOutbuf+OFF_FIRE_PL, pChar->Fireplace, SIZ_FIRE_PL);

   // Stories
   memcpy(pOutbuf+OFF_STORIES, pChar->Stories, SIZ_STORIES);

   // Bldgs
   memcpy(pOutbuf+OFF_BLDGS, pChar->Bldgs, SIZ_BLDGS);

   // Lot Sqft
   memcpy(pOutbuf+OFF_LOT_ACRES, pChar->LotAcre, SIZ_LOT_ACRES);
   memcpy(pOutbuf+OFF_LOT_SQFT, pChar->LotSqft, SIZ_LOT_SQFT);

   // Central Heating-Cooling
   *(pOutbuf+OFF_HEAT) = pChar->Heating[0]; 
   *(pOutbuf+OFF_AIR_COND) = pChar->Cooling[0];

   // Pool
   *(pOutbuf+OFF_POOL) = pChar->Pool[0];

   // View
   *(pOutbuf+OFF_VIEW) = pChar->View[0];

   // BldgClass
   *(pOutbuf+OFF_BLDG_CLASS) = pChar->BldgClass;

   // Quality
   *(pOutbuf+OFF_BLDG_QUAL) = pChar->BldgQual;

   // Condition
   *(pOutbuf+OFF_IMPR_COND) = pChar->ImprCond[0];

   // Units
   memcpy(pOutbuf+OFF_UNITS, pChar->Units, SIZ_UNITS);

   // Elevator
   *(pOutbuf+OFF_ELEVATOR) = pChar->HasElevator;

   lCharMatch++;

   // Get next Char rec
   pRec = fgets(acRec,  2048, fdChar);

   return 0;
}

int Smx_MergeStdChar(char *pOutbuf)
{
   static char    acRec[2048], *pRec=NULL;
   char           acTmp[256];
   long           lTmp;
   int            iRet, iLoop, iBeds, iFBath, iHBath, iFp;
   STDCHAR        *pChar;

   iRet=iBeds=iFBath=iHBath=iFp=0;

   // Get first Char rec for first call
   if (!pRec)
      pRec = fgets(acRec,  2048, fdChar);

   do
   {
      iLoop = memcmp(pOutbuf, acRec, myCounty.iApnLen);
      if (iLoop > 0)
      {
         pRec = fgets(acRec,  2048, fdChar);
         if (!pRec)
         {
            fclose(fdChar);
            fdChar = NULL;
            return 1;      // EOF
         }
      }
   } while (iLoop > 0);

   // If not match, return
   if (iLoop)
      return 1;

   pChar = (STDCHAR *)acRec;

   // UseCode
   if (*(pOutbuf+OFF_USE_CO) == ' ')
   {
      lTmp = atoin(pChar->LandUse, SMX_SIZ_USE_CD1);
      if (lTmp > 0)
      {
         sprintf(acTmp, "%.2d", lTmp);
         memcpy(pOutbuf+OFF_USE_CO, acTmp, 2);
         updateStdUse(pOutbuf+OFF_USE_STD, acTmp, 2, pOutbuf);
      } else
         memcpy(pOutbuf+OFF_USE_STD, USE_UNASGN, SIZ_USE_STD);
   } 

   // Yrblt
   if (pChar->YrBlt[0] > '0')
      memcpy(pOutbuf+OFF_YR_BLT, pChar->YrBlt, SIZ_YR_BLT);

   // YrEff
   if (pChar->YrEff[0] > '0')
      memcpy(pOutbuf+OFF_YR_EFF, pChar->YrEff, SIZ_YR_BLT);

   // BldgSqft
   if (pChar->BldgSqft[0] > '0')
      memcpy(pOutbuf+OFF_BLDG_SF, pChar->BldgSqft, SIZ_BLDG_SF);

   // Floor Sqft
   memcpy(pOutbuf+OFF_FLOOR1_SF, pChar->Sqft_1stFl, SIZ_FLOOR1_SF);
   memcpy(pOutbuf+OFF_FLOOR2_SF, pChar->Sqft_2ndFl, SIZ_FLOOR1_SF);
   memcpy(pOutbuf+OFF_FLOOR3_SF, pChar->Sqft_Above2nd, SIZ_FLOOR1_SF);

   // AddlSqft
   memcpy(pOutbuf+OFF_MISCIMPR_SF, pChar->MiscSqft, SIZ_MISCIMPR_SF);

   // BsmtSqft
   memcpy(pOutbuf+OFF_BSMT_SF, pChar->BsmtSqft, SIZ_BSMT_SF);

   // Garage
   if (pChar->ParkType[0] > ' ')
   {
      *(pOutbuf+OFF_PARK_TYPE) = pChar->ParkType[0];
      memcpy(pOutbuf+OFF_PARK_SPACE, pChar->ParkSpace, SIZ_CHAR_SIZE4);
      memcpy(pOutbuf+OFF_GAR_SQFT, pChar->GarSqft, SIZ_GAR_SQFT);
   } else if (pChar->GarSqft[0] > '0')
   {
      memcpy(pOutbuf+OFF_GAR_SQFT, pChar->GarSqft, SIZ_GAR_SQFT);
      *(pOutbuf+OFF_PARK_TYPE) = 'Z';
   } else if (pChar->ParkSpace[0] > '0')
   {
      memcpy(pOutbuf+OFF_PARK_SPACE, pChar->ParkSpace, SIZ_CHAR_SIZE4);
   } 

   // Beds
   iBeds = atoin(pChar->Beds, SIZ_CHAR_BEDS);
   if (iBeds > 0)
   {
      sprintf(acTmp, "%*u", SIZ_BEDS, iBeds);
      memcpy(pOutbuf+OFF_BEDS, acTmp, SIZ_BEDS);
   } else
      memset(pOutbuf+OFF_BEDS, ' ', SIZ_BEDS);

   // Bath
   iFBath = atoin(pChar->FBaths, SIZ_CHAR_BATHS);
   if (iFBath > 0)
   {
      sprintf(acTmp, "%*u", SIZ_BATH_F, iFBath);
      memcpy(pOutbuf+OFF_BATH_F, acTmp, SIZ_BATH_F);
   } else
      memset(pOutbuf+OFF_BATH_F, ' ', SIZ_BATH_F);

   // Half bath
   iHBath = atoin(pChar->HBaths, SIZ_CHAR_BATHS);
   if (iHBath > 0)
   {
      sprintf(acTmp, "%*u", SIZ_BATH_H, iHBath);
      memcpy(pOutbuf+OFF_BATH_H, acTmp, SIZ_BATH_H);
   } else
      memset(pOutbuf+OFF_BATH_H, ' ', SIZ_BATH_H);

   memcpy(pOutbuf+OFF_BATH_1Q, pChar->Bath_1Q, SIZ_BATH_H);
   memcpy(pOutbuf+OFF_BATH_2Q, pChar->Bath_2Q, SIZ_BATH_H);
   memcpy(pOutbuf+OFF_BATH_3Q, pChar->Bath_3Q, SIZ_BATH_H);
   memcpy(pOutbuf+OFF_BATH_4Q, pChar->Bath_4Q, SIZ_BATH_H);

   // Rooms
   iRet = atoin(pChar->Rooms, SIZ_CHAR_ROOMS);
   if (iRet > 0)
   {
      sprintf(acTmp, "%*u", SIZ_ROOMS, iRet);
      memcpy(pOutbuf+OFF_ROOMS, acTmp, SIZ_ROOMS);
   } else
      memset(pOutbuf+OFF_ROOMS, ' ', SIZ_ROOMS);

   // Fireplace
   memcpy(pOutbuf+OFF_FIRE_PL, pChar->Fireplace, SIZ_FIRE_PL);

   // Stories
   memcpy(pOutbuf+OFF_STORIES, pChar->Stories, SIZ_STORIES);

   // Bldgs
   memcpy(pOutbuf+OFF_BLDGS, pChar->Bldgs, SIZ_BLDGS);

   // Lot Sqft
   memcpy(pOutbuf+OFF_LOT_ACRES, pChar->LotAcre, SIZ_LOT_ACRES);
   memcpy(pOutbuf+OFF_LOT_SQFT, pChar->LotSqft, SIZ_LOT_SQFT);

   // Central Heating-Cooling
   *(pOutbuf+OFF_HEAT) = pChar->Heating[0]; 
   *(pOutbuf+OFF_AIR_COND) = pChar->Cooling[0];

   // Pool
   *(pOutbuf+OFF_POOL) = pChar->Pool[0];

   // View
   *(pOutbuf+OFF_VIEW) = pChar->View[0];

   // BldgClass
   *(pOutbuf+OFF_BLDG_CLASS) = pChar->BldgClass;

   // Quality
   *(pOutbuf+OFF_BLDG_QUAL) = pChar->BldgQual;

   // Condition
   *(pOutbuf+OFF_IMPR_COND) = pChar->ImprCond[0];

   // Units
   memcpy(pOutbuf+OFF_UNITS, pChar->Units, SIZ_UNITS);

   // Elevator
   *(pOutbuf+OFF_ELEVATOR) = pChar->HasElevator;

   lCharMatch++;

   // Get next Char rec
   pRec = fgets(acRec,  2048, fdChar);

   return 0;
}

/***************************** Smx_MergeSaleRec ******************************
 *
 * Merge new sale data into current sale.  Move other sales accordingly.
 * Only update seller if that is the sale from assessor.
 * This is the modified version of MergeSale() which will not update sale
 * without SaleAmt.  Only transfer is updated.
 *
 *****************************************************************************/

int Smx_MergeSaleRec(char *pOutbuf, char *pRec)
{
   long  lCurSaleDt, lLstSaleDt, lPrice, lLastAmt;
   char  acTmp[32];

   SMX_SALE *pSaleRec = (SMX_SALE *)pRec;

   lCurSaleDt = atoin(pSaleRec->DocDate, SIZ_SALE1_DT);
   lLstSaleDt = atoin(pOutbuf+OFF_SALE1_DT, SIZ_SALE1_DT);

   if (lCurSaleDt < lLstSaleDt || lCurSaleDt > lToday)
      return -1;

   lPrice = atoin(pSaleRec->SalePrice, SMX_SIZ_SALE_AMT1);
   if (lCurSaleDt == lLstSaleDt)
   {
      lLastAmt = atoin(pOutbuf+OFF_SALE1_AMT, SIZ_SALE1_AMT);
      if (!lPrice && lLastAmt > 0)
         return 0;
   } else if (lPrice > 0)
   {
      // Move sale2 to sale3
      memcpy(pOutbuf+OFF_SALE3_DOC, pOutbuf+OFF_SALE2_DOC, SIZ_SALE3_DOC);
      memcpy(pOutbuf+OFF_SALE3_DT, pOutbuf+OFF_SALE2_DT, SIZ_SALE3_DT);
      memcpy(pOutbuf+OFF_SALE3_AMT, pOutbuf+OFF_SALE2_AMT, SIZ_SALE3_AMT);
      *(pOutbuf+OFF_AR_CODE3) = *(pOutbuf+OFF_AR_CODE2);

      // Move sale1 to sale2
      memcpy(pOutbuf+OFF_SALE2_DOC, pOutbuf+OFF_SALE1_DOC, SIZ_SALE3_DOC);
      memcpy(pOutbuf+OFF_SALE2_DT, pOutbuf+OFF_SALE1_DT, SIZ_SALE3_DT);
      memcpy(pOutbuf+OFF_SALE2_AMT, pOutbuf+OFF_SALE1_AMT, SIZ_SALE3_AMT);
      *(pOutbuf+OFF_AR_CODE2) = *(pOutbuf+OFF_AR_CODE1);
   }

   // Update current sale
   /*
   char acDocNum[32];
   if (pSaleRec->DocNum[0] > ' ')
   {
      if (pSaleRec->DocNum[9] == ' ')
      {
         memcpy(acDocNum, "19", 2);
         memcpy(&acDocNum[2], pSaleRec->DocNum, SMX_SIZ_DOCNUM-2);
      } else
         memcpy(acDocNum, pSaleRec->DocNum, SMX_SIZ_DOCNUM);
      acDocNum[SMX_SIZ_DOCNUM] = 0;
   } else
      acDocNum[0] = 0;
   */
   if (lPrice > 0)
   {
      memcpy(pOutbuf+OFF_SALE1_DOC, pSaleRec->DocNum, SMX_SIZ_DOCNUM);
      memcpy(pOutbuf+OFF_SALE1_DT, pSaleRec->DocDate, SIZ_SALE3_DT);

      // Check for questionable sale amt
      if (lPrice > 1000)
      {
         sprintf(acTmp, "%*u", SIZ_SALE1_AMT, lPrice);
         memcpy(pOutbuf+OFF_SALE1_AMT, acTmp, SIZ_SALE1_AMT);
      } else
         memset(pOutbuf+OFF_SALE1_AMT, ' ', SIZ_SALE1_AMT);
   }

   // Update transfers
   lLstSaleDt = atoin(pOutbuf+OFF_TRANSFER_DT, SIZ_TRANSFER_DT);
   if (lCurSaleDt >= lLstSaleDt)
   {
      memcpy(pOutbuf+OFF_TRANSFER_DOC, pSaleRec->DocNum, SMX_SIZ_DOCNUM);
      memcpy(pOutbuf+OFF_TRANSFER_DT, pSaleRec->DocDate, SIZ_TRANSFER_DT);
   }

   if (lLastRecDate < lCurSaleDt)
      lLastRecDate = lCurSaleDt;

   *(pOutbuf+OFF_AR_CODE1) = 'A';

   return 1;
}

/********************************* Smx_MergeSale *****************************
 *
 * Sale file are in sorted order APN, Saledate, Doc#
 *
 *****************************************************************************/

int Smx_MergeSale(char *pOutbuf, bool bNewRec=false)
{
   static   char  acRec[1024], *pRec=NULL;
   char     *pTmp;
   int      iRet, iLoop, iSaleCnt;

   // Get first Char rec for first call
   if (!pRec && lSaleMatch == 0)
      pTmp = fgets(acRec,  1024, fdSale);

   do
   {
      iLoop = memcmp(pOutbuf, &acRec[SMX_OFF_APN], SMX_SIZ_APN);
      if (iLoop > 0)
      {
         pTmp = fgets(acRec,  1024, fdSale);
         if (!pTmp)
         {
            fclose(fdSale);
            fdSale = NULL;
            return 1;      // EOF
         }
      }
   } while (iLoop > 0);

   // If not match, return
   if (iLoop)
      return 0;

   iSaleCnt = 0;
   do
   {
      iRet = Smx_MergeSaleRec(pOutbuf, acRec);
      // Get next sale record
      pTmp = fgets(acRec,  1024, fdSale);
      if (!pTmp)
      {
         fclose(fdSale);
         fdSale = NULL;
         break;      // EOF
      }

      // Update flag
      *(pOutbuf+OFF_AR_CODE1) = 'A';
   } while (!memcmp(pOutbuf, &acRec[SMX_OFF_APN], SMX_SIZ_APN) );

   lSaleMatch++;
   return 0;
}
/********************************* Smx_MergeRoll *****************************
 *
 * Return 0 if successful, >0 if error
 *
 *****************************************************************************/

void Smx_MergeRoll(char *pOutbuf, char *pRollRec, int iLen, int iFlag)
{
   char     acTmp[256], acTmp1[256];
   LONGLONG lTmp;
   int      iRet;

   SMX_ROLL *pRec = (SMX_ROLL *)pRollRec;

   if (iFlag & CREATE_R01)
   {
      memset(pOutbuf, ' ', iRecLen);

      memcpy(pOutbuf, pRec->Apn, SMX_SIZ_APN);
      memcpy(pOutbuf+OFF_CO_NUM, "41SMX", 5);

      // Format APN
      iRet = formatApn(pRec->Apn, acTmp, &myCounty);
      memcpy(pOutbuf+OFF_APN_D, acTmp, iRet);

      // Create MapLink
      iRet = formatMapLink(pRec->Apn, acTmp, &myCounty);
      memcpy(pOutbuf+OFF_MAPLINK, acTmp, iRet);

      // Create index map link
      if (getIndexPage(acTmp, acTmp1, &myCounty))
         memcpy(pOutbuf+OFF_IMAPLINK, acTmp1, iRet);

      // Year assessed
      memcpy(pOutbuf+OFF_YR_ASSD, myCounty.acYearAssd, 4);

      // Land
      long lLand = atoin(pRec->Land, SMX_SIZ_LAND);
      if (lLand > 0)
      {
         sprintf(acTmp, "%*u", SIZ_LAND, lLand);
         memcpy(pOutbuf+OFF_LAND, acTmp, SIZ_LAND);
      }

      // Improve
      long lImpr = atoin(pRec->Improv, SMX_SIZ_IMPROV);
      if (lImpr > 0)
      {
         sprintf(acTmp, "%*u", SIZ_IMPR, lImpr);
         memcpy(pOutbuf+OFF_IMPR, acTmp, SIZ_IMPR);
      }

      // Other value
      long lPers    = atoin(pRec->Pers_Prop, SMX_SIZ_PERS_PROP);
      long lFixture = atoin(pRec->Trade_Fixt, SMX_SIZ_TRADE_FIXT);
      long lMineral = atoin(pRec->Mineral, SMX_SIZ_MINERALRTS);
      lTmp = lMineral+lFixture+lPers;
      if (lTmp > 0)
      {
         sprintf(acTmp, "%*u", SIZ_OTHER, lTmp);
         memcpy(pOutbuf+OFF_OTHER, acTmp, SIZ_OTHER);

         if (lPers > 0)
         {
            sprintf(acTmp, "%u         ", lPers);
            memcpy(pOutbuf+OFF_PERSPROP, acTmp, SIZ_PERSPROP);
         }

         if (lFixture > 0)
         {
            sprintf(acTmp, "%u         ", lFixture);
            memcpy(pOutbuf+OFF_FIXTR, acTmp, SIZ_FIXTR);
         }
         if (lMineral > 0)
         {
            sprintf(acTmp, "%u         ", lMineral);
            memcpy(pOutbuf+OFF_MINERAL, acTmp, SIZ_MINERAL);
         }
      }

      // Add others to land & impr 02/13/2008
      LONGLONG lGross = lTmp+lLand+lImpr;
      if (lGross > 0)
      {
         sprintf(acTmp, "%*u", SIZ_GROSS, lGross);
         memcpy(pOutbuf+OFF_GROSS, acTmp, SIZ_GROSS);
      }

      // Ratio
      if (lImpr > 0)
      {
         sprintf(acTmp, "%*u", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
         memcpy(pOutbuf+OFF_RATIO, acTmp, SIZ_RATIO);
      }

      // HO Exempt
      if (pRec->Exemp_Code[0] == 'H')
         *(pOutbuf+OFF_HO_FL) = '1';      // 'Y'
      else
         *(pOutbuf+OFF_HO_FL) = '2';      // 'N'

      // Exemp total
      LONGLONG lExe = atoin(pRec->Exemp_1, SMX_SIZ_EXEMP_1);
      lExe += atoin(pRec->Exemp_2, SMX_SIZ_EXEMP_1);
      if (lExe > 0)
      {
         if (lExe > lGross)
         {
            LogMsg("* Overwrite EXE_TOTAL of %d with %d", lTmp, lGross);
            lExe = lGross;
         }
         sprintf(acTmp, "%*u", SIZ_EXE_TOTAL, lTmp);
         memcpy(pOutbuf+OFF_EXE_TOTAL, acTmp, SIZ_EXE_TOTAL);
      }

      // Save exemption code
      if (lGross == lExe)
         *(pOutbuf+OFF_FULL_EXEMPT) = 'Y';      
      if (pRec->Exemp_Code[0] > ' ')
      {
         // Set full exemption flag
         lTmp = atoin(pRec->Net, SMX_SIZ_NET);
         if (lTmp == 0)
            *(pOutbuf+OFF_FULL_EXEMPT) = 'Y';

         memcpy(pOutbuf+OFF_EXE_CD1, pRec->Exemp_Code, SIZ_EXE_CD);
      } 

   }

   // TRA
   lTmp = atoin(pRec->Tra, SMX_SIZ_TRA);
   if (lTmp > 0)
      memcpy(pOutbuf+OFF_TRA, pRec->Tra, SMX_SIZ_TRA);

   // Parcel status
   *(pOutbuf+OFF_STATUS) = 'A';

   // Legal
   if (memcmp(pRec->Legal_Desc_1, "   ", 3) )
   {
      memcpy(acTmp, pRec->Legal_Desc_1, SMX_SIZ_LEGAL_DESC_1*4);
      acTmp[SMX_SIZ_LEGAL_DESC_1*4] = 0;
      remUnPrtChar(acTmp);
      iRet = updateLegal(pOutbuf, acTmp);
      if (iRet > iMaxLegal)
         iMaxLegal = iRet;
   }

   // UseCode
   lTmp = atoin(pRec->Use_Code, SMX_SIZ_USE_CODE);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%.2d", lTmp);
      memcpy(pOutbuf+OFF_USE_CO, acTmp, 2);
      updateStdUse(pOutbuf+OFF_USE_STD, acTmp, 2, pOutbuf);
   } else
      memcpy(pOutbuf+OFF_USE_STD, USE_UNASGN, SIZ_USE_STD);

#ifdef _DEBUG
   //if (!memcmp(pRec->Apn, "070202010", 9))
   //   iRet = 1;
#endif

   // Doc date
   lTmp = atoin(pRec->Recording_Date, SMX_SIZ_RECORDING_DATE);
   if (lTmp > 0)
   {
      memcpy(pOutbuf+OFF_TRANSFER_DT, pRec->Recording_Date, SIZ_TRANSFER_DT);
      memcpy(acTmp, pRec->Recording_No, SMX_SIZ_RECORDING_NO);
      blankRem(acTmp, SMX_SIZ_RECORDING_NO);
      memcpy(pOutbuf+OFF_TRANSFER_DOC, acTmp, strlen(acTmp));
   }

   Smx_MergeOwner(pOutbuf, pRollRec);
   Smx_MergeMAdr(pOutbuf, pRollRec);
   Smx_MergeSAdr(pOutbuf, pRollRec);
}

/********************************* Smx_Load_Roll ******************************
 *
 *
 ******************************************************************************/

int Smx_Load_Roll(int iSkip)
{
   char     acRec[MAX_RECSIZE], acBuf[MAX_RECSIZE], acRollRec[MAX_RECSIZE];
   char     acRawFile[_MAX_PATH], acOutFile[_MAX_PATH], acLastApn[32];

   HANDLE   fhIn, fhOut;

   int      iRet, Result, iRollUpd=0, iNewRec=0, iRetiredRec=0;
   DWORD    nBytesRead;
   DWORD    nBytesWritten;
   BOOL     bRet, bEof;
   long     lRet=0, lCnt=0;

   LogMsg0("Loading Rol Update file: %s", acRollFile);

   sprintf(acRawFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "S01");
   sprintf(acOutFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "R01");

   // Rename files for processing
   if (_access(acRawFile, 0))
   {
      if (!_access(acOutFile, 0))
         rename(acOutFile, acRawFile);
      else
      {
         LogMsg("***** Missing input file %s.  Please recheck!", acRawFile);
         return 1;
      }
   }

   // Open Roll file
   LogMsg("Open Roll file %s", acRollFile);
   fdRoll = fopen(acRollFile, "r");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", acRollFile);
      return 2;
   }

   // Open Char file
   LogMsg("Open Char file %s", acCChrFile);
   fdChar = fopen(acCChrFile, "r");
   if (fdChar == NULL)
   {
      LogMsg("***** Error opening Char file: %s\n", acCChrFile);
      return 2;
   }

   // Open Vacant Land file
   if (!_access(acVacFile, 0))
   {
      LogMsg("Open Vacant Land file %s", acVacFile);
      fdVac = fopen(acVacFile, "r");
      if (fdVac == NULL)
      {
         LogMsg("***** Error opening Vacant Land file: %s\n", acVacFile);
         return 2;
      }
   } else
   {
      LogMsg("*** Missing Vacant Land file: %s\n", acVacFile);
      fdVac = NULL;
   }

   // Open Input file
   LogMsg("Open input file %s", acRawFile);
   fhIn = CreateFile(acRawFile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhIn == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening input file: %s\n", acRawFile);
      return 2;
   }

   // Open Output file
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening output file: %s\n", acOutFile);
      return 2;
   }

   iRet = fread(acRollRec, 1, MAX_ROLLREC, fdRoll);

   bEof = false;

   // Copy skip record
   memset(acBuf, ' ', iRecLen);
   while (iSkip-- > 0)
   {
      ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);
      // Ignore record start with 99999999
      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
   }

   iNoMatch=iBadCity=iBadSuffix=0;
   SMX_ROLL *pRec = (SMX_ROLL *)&acRollRec[0];
   memset(acLastApn, 0, 32);

   // Merge loop
   while (!bEof)
   {
      bRet = ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);

      // Check for EOF
      if (!bRet)
      {
         LogMsg("***** Error reading input file %s (%f)", acRawFile, GetLastError());
         break;
      }

      // EOF ?
      if (iRecLen != nBytesRead)
         break;

      NextRollRec:

      // Ignore this record
      if (!memcmp(acRollRec, "105990010", 9) && !memcmp(pRec->Name_1, "COMCAST", 7))
         iRet = fread(acRollRec, 1, MAX_ROLLREC, fdRoll);

      // If new APN is smaller than previous one, drop it and investigate
      if (memcmp(acRollRec, acLastApn, iApnLen) <= 0)
      {
         LogMsg("***** Duplicate record found in roll APN=%s", acLastApn);
         iRet = fread(acRollRec, 1, MAX_ROLLREC, fdRoll);
      }
      Result = memcmp(acBuf, &acRollRec[SMX_OFF_APN], iApnLen);
      if (!Result)
      {
         // Merge roll data
         Smx_MergeRoll(acBuf, acRollRec, iRollLen, UPDATE_R01);
         iRollUpd++;

         // Merge Vacant Land
         if (fdVac)
            iRet = Smx_MergeVac(acBuf);

         // Merge Char
         if (fdChar)
            iRet = Smx_MergeChar(acBuf);

         bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
         if (!(++lCnt % 1000))
            printf("\r%u", lCnt);

         // Save last written parcel
         memcpy(acLastApn, acBuf, iApnLen);

         // Read new record
         iRet = fread(acRollRec, 1, MAX_ROLLREC, fdRoll);
         if (iRet != MAX_ROLLREC)
            bEof = true;
      } else if (Result > 0)
      {
         if (bDebug)
            LogMsg0("* New roll record : %.*s (%d)", iApnLen, &acRollRec[SMX_OFF_APN], lCnt);

         // Create new R01 record
         memset(acRec, ' ', iRecLen);
         Smx_MergeRoll(acRec, acRollRec, iRollLen, CREATE_R01);

         // Only take new records with Owner name
         if (acRec[OFF_NAME1] > ' ')
         {
            iNewRec++;

            // Merge Vacant Land
            if (fdVac)
               iRet = Smx_MergeVac(acRec);

            // Merge Char
            if (fdChar)
               iRet = Smx_MergeChar(acRec);

            bRet = WriteFile(fhOut, acRec, iRecLen, &nBytesWritten, NULL);
            if (!(++lCnt % 1000))
               printf("\r%u", lCnt);

            // Save last written parcel
            memcpy(acLastApn, acRec, iApnLen);
         } else
            LogMsg("*** Drop parcel (no owner name) APN=%.*s", iApnLen, acRec);

         // Read new record
         iRet = fread(acRollRec, 1, MAX_ROLLREC, fdRoll);
         if (iRet != MAX_ROLLREC)
            bEof = true;    // Signal to stop
         else
            goto NextRollRec;
      } else
      {
         // Record may be retired
         if (bDebug)
            LogMsg0("*** Roll not match (retired record?) : R01->%.*s < Roll->%.*s (%d) ***", iApnLen, acBuf, iApnLen, &acRollRec[SMX_OFF_APN], lCnt);
         iRetiredRec++;
         continue;
      }
   }

   // Do the rest of the file
   while (!bEof)
   {
      if (bDebug)
         LogMsg0("*** New roll record : %.*s (%d) ***", iApnLen, &acRollRec[SMX_OFF_APN], lCnt);

      // Create new R01 record
      memset(acRec, ' ', iRecLen);
      Smx_MergeRoll(acRec, acRollRec, iRollLen, CREATE_R01);
      iNewRec++;

      // Merge Vacant Land
      if (fdVac)
         iRet = Smx_MergeVac(acRec);

      // Merge Char
      if (fdChar)
         iRet = Smx_MergeChar(acRec);

      bRet = WriteFile(fhOut, acRec, iRecLen, &nBytesWritten, NULL);
      lCnt++;

      iRet = fread(acRollRec, 1, MAX_ROLLREC, fdRoll);
      if (iRet != MAX_ROLLREC)
         break;

         bEof = true;    // Signal to stop
   }

   // Close files
   if (fdRoll)
      fclose(fdRoll);
   if (fdChar)
      fclose(fdChar);
   if (fdVac)
      fclose(fdVac);

   if (fhOut)
      CloseHandle(fhOut);
   if (fhIn)
      CloseHandle(fhIn);

   LogMsg("Total output records:       %u", lCnt);
   LogMsg("      new records:          %u", iNewRec);
   LogMsg("      retired records:      %u", iRetiredRec);
   LogMsg("      Unit from Legal:      %u\n", lLglUnit);
   LogMsg("      bad-city records:     %u", iBadCity);
   LogMsg("      bad-suffix records:   %u\n", iBadSuffix);
   LogMsg("      Char matched:         %u", lCharMatch);
   LogMsg("      Char skiped:          %u\n", lCharSkip);
   if (fdVac)
   {
      LogMsg("      Vac. matched:         %u", lVacMatch);
      LogMsg("      Vac. skiped:          %u\n", lVacSkip);
   }

   if (lDupOwners > 0)
      LogMsg("Number of duplicate names:  %u", lDupOwners);

   printf("\nTotal output records: %u\n", lCnt);

   lRecCnt = lCnt;
   return 0;
}

/****************************** Smx_MergeRollCsv *****************************
 *
 * Return 0 if successful, < 0 if error
 *        1 retired record, not use
 *
 *****************************************************************************/

int Smx_MergeRollCsv(char *pOutbuf, char *pRollRec, int iLen, int iFlag)
{
   char     acTmp[256], acTmp1[256];
   LONGLONG lTmp;
   int      iRet=0, iTmp;

   // Parse roll record
   iRet = ParseStringIQ(pRollRec, '|', MAX_FLD_TOKEN, apTokens);
   if (iRet < SMX_R_FLDS)
   {
      LogMsg("***** Error: bad input record for APN=%s", apTokens[SMX_R_APN]);
      return -1;
   }
#ifdef _DEBUG
   //if (!memcmp(apTokens[R_APN], "692460080", 9) )
   //   iTmp = 0;
#endif

   if (iFlag & CREATE_R01)
   {
      // Clear output buffer
      memset(pOutbuf, ' ', iRecLen);

      // Start copying data
      memcpy(pOutbuf, apTokens[SMX_R_APN], strlen(apTokens[SMX_R_APN]));
      *(pOutbuf+OFF_STATUS) = 'A';

      // Format APN
      iRet = formatApn(apTokens[SMX_R_APN], acTmp, &myCounty);
      memcpy(pOutbuf+OFF_APN_D, acTmp, iRet);

      // Create MapLink and output new record
      iRet = formatMapLink(apTokens[SMX_R_APN], acTmp, &myCounty);
      memcpy(pOutbuf+OFF_MAPLINK, acTmp, iRet);

      // Create index map link
      if (getIndexPage(acTmp, acTmp1, &myCounty))
         memcpy(pOutbuf+OFF_IMAPLINK, acTmp1, iRet);

      // County code
      memcpy(pOutbuf+OFF_CO_NUM, "41SMX", 5);

      // Year assessed
      memcpy(pOutbuf+OFF_YR_ASSD, myCounty.acYearAssd, 4);

      // Land
      LONGLONG lLand = atoi(apTokens[SMX_R_LAND_VAL]);
      if (lLand > 0)
      {
         sprintf(acTmp, "%*u", SIZ_LAND, lLand);
         memcpy(pOutbuf+OFF_LAND, acTmp, SIZ_LAND);
      }

      // Improve
      LONGLONG lImpr = atoi(apTokens[SMX_R_IMPR_VAL]);
      if (lImpr > 0)
      {
         sprintf(acTmp, "%*u", SIZ_IMPR, lImpr);
         memcpy(pOutbuf+OFF_IMPR, acTmp, SIZ_IMPR);
      }

      // Other values
      long lFixt = atoi(apTokens[SMX_R_FIX_VAL]);
      long lPP   = atoi(apTokens[SMX_R_PP_VAL]);
      long lMin  = atol(apTokens[SMX_R_MIN_VAL]);
      lTmp = lFixt + lPP + lMin;
      if (lTmp > 0)
      {
         sprintf(acTmp, "%*u", SIZ_OTHER, lTmp);
         memcpy(pOutbuf+OFF_OTHER, acTmp, SIZ_OTHER);

         if (lFixt > 0)
         {
            sprintf(acTmp, "%d         ", lFixt);
            memcpy(pOutbuf+OFF_FIXTR, acTmp, SIZ_FIXTR);
         }
         if (lPP > 0)
         {
            sprintf(acTmp, "%d         ", lPP);
            memcpy(pOutbuf+OFF_PERSPROP, acTmp, SIZ_PERSPROP);
         }
         if (lMin > 0)
         {
            sprintf(acTmp, "%d         ", lMin);
            memcpy(pOutbuf+OFF_MINERAL, acTmp, SIZ_MINERAL);
         }
      }

      // Gross total
      LONGLONG lGross = lTmp + lLand + lImpr;
      if (lGross > 0)
      {
         sprintf(acTmp, "%*u", SIZ_GROSS, lGross);
         memcpy(pOutbuf+OFF_GROSS, acTmp, SIZ_GROSS);
      }

      // Ratio
      if (lImpr > 0)
      {
         sprintf(acTmp, "%*u", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
         memcpy(pOutbuf+OFF_RATIO, acTmp, SIZ_RATIO);
      }

      // TRA
      iTmp = atol(apTokens[SMX_R_TRA]);
      sprintf(acTmp, "%.6u", iTmp);
      memcpy(pOutbuf+OFF_TRA, acTmp, 6);

      // UseCode
      if (*apTokens[SMX_R_PUCCODE] > ' ')
      {
         memcpy(acTmp1, apTokens[SMX_R_PUCCODE]+1, 2);
         acTmp1[2] = 0;
         memcpy(pOutbuf+OFF_USE_CO, acTmp1, 2);
         updateStdUse(pOutbuf+OFF_USE_STD, acTmp1, 2, pOutbuf);
      } else
         memcpy(pOutbuf+OFF_USE_STD, USE_UNASGN, SIZ_USE_STD);

      // Legal 
      if (*apTokens[SMX_R_LEGALDESC] > ' ')
      {
         strcpy(acTmp, apTokens[SMX_R_LEGALDESC]);
         iRet = updateLegal(pOutbuf, acTmp);
         if (iRet > iMaxLegal)
            iMaxLegal = iRet;
      }

      // HO_Exe
      if (!memcmp(apTokens[SMX_R_EXE_CODE], "HO", 2))
         *(pOutbuf+OFF_HO_FL) = '1';
      else
         *(pOutbuf+OFF_HO_FL) = '2';

      LONGLONG lExe = atol(apTokens[SMX_R_EXE1]) + atol(apTokens[SMX_R_EXE2]);
      if (lExe > 0)
      {
         iTmp = sprintf(acTmp1, "%u", lExe);
         memcpy(pOutbuf+OFF_EXE_TOTAL, acTmp1, iTmp);
         if (lExe >= lGross)
         {
            lExe = lGross;
            *(pOutbuf+OFF_FULL_EXEMPT) = 'Y';   
         }
      } 
         
      // Set full exemption flag
      lTmp = atol(apTokens[SMX_R_NET_VAL]);
      if (lTmp == 0)
         *(pOutbuf+OFF_FULL_EXEMPT) = 'Y';

      // SMX has EXEVAL1 & EXEVAL2, but only one appears at a time.  EXEVAL1 is for HO, and EXEVAL2 is for others
      if (*apTokens[SMX_R_EXE_CODE] > ' ')
      {
         vmemcpy(pOutbuf+OFF_EXE_CD1, apTokens[SMX_R_EXE_CODE], SIZ_EXE_CD1);

         // Create exemption type
         makeExeType(pOutbuf+OFF_EXE_TYPE, pOutbuf+OFF_EXE_CD1, pOutbuf+OFF_EXE_CD2, pOutbuf+OFF_EXE_CD3, (IDX_TBL4 *)&SMX_Exemption);
      }
   }

   // Owner
   Smx_MergeOwnerCsv(pOutbuf, apTokens[SMX_R_OWNER], apTokens[SMX_R_OWNER2], apTokens[SMX_R_CAREOF]);

   // Situs
   Smx_MergeSitus(pOutbuf);

   // Mailing
   Smx_MergeMailing(pOutbuf);

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "002023020", 9) )
   //   iTmp = 0;
#endif

   // Last DocNum/DocDate - blank out if different year
   if (memcmp(pOutbuf+OFF_TRANSFER_DT, pOutbuf+OFF_TRANSFER_DOC, 4))
   {
      if (memcmp(pOutbuf+OFF_TRANSFER_DT+2, pOutbuf+OFF_TRANSFER_DOC, 2))
      {
         memset(pOutbuf+OFF_TRANSFER_DT, ' ', SIZ_TRANSFER_DT);
         memset(pOutbuf+OFF_TRANSFER_DOC, ' ', SIZ_TRANSFER_DOC);
      }
   }
   return 0;
}

/****************************** Smx_Load_RollCsv ******************************
 *
 * Load updated roll file SecureMaster.csv (01/24/2024)
 *
 ******************************************************************************/

int Smx_Load_RollCsv(int iSkip)
{
   char     acRec[MAX_RECSIZE], acBuf[MAX_RECSIZE], acRollRec[MAX_RECSIZE], acLastApn[32];
   char     acRawFile[_MAX_PATH], acOutFile[_MAX_PATH], acSrtFile[_MAX_PATH], *pTmp;
   int      iRet, iTmp, iRollUpd=0, iNewRec=0, iRetiredRec=0, lCnt=0, iDupApn=0;
   DWORD    nBytesRead, nBytesWritten;
   BOOL     bRet, bEof;
   HANDLE   fhIn, fhOut;

   LogMsg0("Loading roll update");

   sprintf(acRawFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "S01");
   sprintf(acOutFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "R01");

   // Rename files for processing
   if (_access(acRawFile, 0))
   {
      if (!_access(acOutFile, 0))
         rename(acOutFile, acRawFile);
      else
      {
         LogMsg("***** Missing input file %s.  Please recheck!", acRawFile);
         return -1;
      }
   }

   // Sort input
   sprintf(acSrtFile, "%s\\Smx\\Smx_Roll.srt", acTmpPath);
   // OpTech Sort doesn't accept F(TXT) this file type (LF without CR)
   // We need to use general purpose file type for it
   //sprintf(acRec, "S(1,9,C,A) F(GP,(10),()) OMIT(1,1,C,GT,\"9\")");
   GetPrivateProfileString("SMX", "RollSrtCmd", "", acRec, 256, acIniFile);
   iTmp = sortFile(acRollFile, acSrtFile, acRec, &iRet);
   if (!iTmp)
      return iRet;

   // Open roll file
   LogMsg("Open Roll file %s", acSrtFile);
   fdRoll = fopen(acSrtFile, "r");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", acSrtFile);
      return 2;
   }

   LogMsg("Open Char file %s", acCChrFile);
   fdChar = fopen(acCChrFile, "r");
   if (fdChar == NULL)
   {
      LogMsg("***** Error opening Char file: %s\n", acCChrFile);
      return -2;
   }

   // Open Input file
   LogMsg("Open input file %s", acRawFile);
   fhIn = CreateFile(acRawFile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhIn == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening input file: %s\n", acRawFile);
      return -3;
   }

   // Open Output file
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening output file: %s\n", acOutFile);
      return 4;
   }

   // Get 1st rec
   pTmp = fgets((char *)&acRollRec[0], MAX_RECSIZE, fdRoll);
   bEof = (pTmp ? false:true);

   // Init variables
   iNoMatch=iBadCity=iBadSuffix=0;
   strcpy(acLastApn, "000000000");

   // Copy skip record
   memset(acBuf, ' ', iRecLen);
   while (iSkip-- > 0)
   {
      ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);
      // Ignore record start with 99999999
      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
   }

   // Merge loop
   while (!bEof)
   {
      bRet = ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);

      // Check for EOF
      if (!bRet)
      {
         LogMsg("***** Error reading input file %s (%f)", acRawFile, GetLastError());
         break;
      }

      // EOF ?
      if (!nBytesRead)
         break;

NextRollRec:
#ifdef _DEBUG
      //if (!memcmp(acRollRec, "689040017X260", 13) || !memcmp(acBuf, "689040017260", 12))
      //   iTmp = 0;
#endif

      iTmp = memcmp(acBuf, acRollRec, iApnLen);
      if (!iTmp)
      {
         // Create R01 record from roll data
         iRet = Smx_MergeRollCsv(acBuf, acRollRec, MAX_RECSIZE, UPDATE_R01);
         if (!iRet)
         {
            // Merge Char
            if (fdChar)
               iTmp = Smx_MergeStdChar(acBuf);

            iRollUpd++;
         }

         // Read next roll record
         pTmp = fgets(acRollRec, MAX_RECSIZE, fdRoll);

         if (iRet > 0)
            goto NextRollRec;
         if (!pTmp)
            bEof = true;         // Signal to stop
      } else if (iTmp > 0)       // Roll not match, new roll record?
      {
         if (bDebug)
            LogMsg0("New roll record : %.*s (%d)", iApnLen, acRollRec, lCnt);

         // Create new R01 record
         iRet = Smx_MergeRollCsv(acRec, acRollRec, MAX_RECSIZE, CREATE_R01);
         if (!iRet)
         {
            // Merge Char
            if (fdChar)
               iTmp = Smx_MergeStdChar(acRec);

            if (memcmp(acLastApn, acRec, 14))
            {
               memcpy(acLastApn, acRec, 14);
               bRet = WriteFile(fhOut, acRec, iRecLen, &nBytesWritten, NULL);
               iNewRec++;
            } else
            {
               LogMsg("---> Drop duplicate APN: %.14s", acLastApn);
               bDupApn = true;
               iDupApn++;
            }
         }
         lCnt++;

         // Get next roll record
         pTmp = fgets(acRollRec, MAX_RECSIZE, fdRoll);
         if (!pTmp)
            bEof = true;    // Signal to stop
         else
            goto NextRollRec;
      } else
      {
         // Record may be retired
         if (bDebug)
            LogMsg0("Retired record? : R01->%.*s < Roll->%.*s (%d)", iApnLen, acBuf, iApnLen, (char *)&acRollRec[1], lCnt);
         iRetiredRec++;

         continue;
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
      if (!iRet)
      {
         if (memcmp(acLastApn, acBuf, 14))
         {
            memcpy(acLastApn, acBuf, 14);
            bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
            if (!bRet)
            {
               LogMsg("Error occurs: %d\n", GetLastError());
               break;
            }
         }
      }
   }

   // Do the rest of the file
   while (!bEof && isdigit(acRollRec[0]))
   {
      // Create new R01 record
      iRet = Smx_MergeRollCsv(acRec, acRollRec, MAX_RECSIZE, CREATE_R01);

      if (!iRet)
      {
         if (bDebug)
            LogMsg0("New roll record : %.*s (%d) ", iApnLen, acRollRec, lCnt);

         // Merge Char
         if (fdChar)
            iRet = Smx_MergeStdChar(acRec);

         if (memcmp(acLastApn, acRec, 14))
         {
            memcpy(acLastApn, acRec, 14);
            bRet = WriteFile(fhOut, acRec, iRecLen, &nBytesWritten, NULL);
            iNewRec++;
         } else
         {
            LogMsg("---> Duplicate APN: %.14s (END)", acLastApn);
            bDupApn = true;
            iDupApn++;
         }
      }
      lCnt++;

      // Get next roll record
      pTmp = fgets(acRollRec, MAX_RECSIZE, fdRoll);

      if (!pTmp)
         bEof = true;    // Signal to stop
   }

   // Close files
   if (fdRoll)
      fclose(fdRoll);
   if (fdChar)
      fclose(fdChar);
   if (fhOut)
      CloseHandle(fhOut);

   LogMsg("Total output records:       %u", lCnt);
   LogMsg("Total retired records:      %u", iRetiredRec);
   LogMsg("Total bad-city records:     %u", iBadCity);
   LogMsg("Total bad-suffix records:   %u\n", iBadSuffix);

   LogMsg("Number of Char matched:     %u", lCharMatch);
   LogMsg("          Char skiped:      %u\n", lCharSkip);
   LogMsg("          duplicate APN:    %u\n", iDupApn);

   printf("\nTotal output records: %u", lCnt);

   lRecCnt = lCnt;
   return 0;
}


/********************************* Smx_Load_LDR *****************************
 *
 * Remove sale update and use -Ms instead.
 *
 ****************************************************************************/

int Smx_Load_LDR(int iFirstRec)
{
   char     acBuf[MAX_RECSIZE], acRollRec[MAX_RECSIZE];
   static   char acRec[MAX_RECSIZE];
   char     acOutFile[_MAX_PATH];

   HANDLE   fhOut;

   int      iRet;
   DWORD    nBytesWritten;
   BOOL     bRet, bEof;
   long     lRet=0;

   LogMsg0("Loading LDR %d", lLienYear);

   // Open roll file
   LogMsg("Open Roll file %s", acRollFile);
   fdRoll = fopen(acRollFile, "r");
   if (fdRoll == NULL)
   {
      printf("Error opening roll file: %s\n", acRollFile);
      return -2;
   }

   // Open Char file
   LogMsg("Open Char file %s", acCChrFile);
   fdChar = fopen(acCChrFile, "r");
   if (fdChar == NULL)
   {
      LogMsg("***** Error opening Char file: %s\n", acCChrFile);
      return -2;
   }

   // Open Vacant Land file
   LogMsg("Open Vacant Land file %s", acVacFile);
   fdVac = fopen(acVacFile, "r");
   if (fdVac == NULL)
   {
      LogMsg("***** Error opening Vacant Land file: %s\n", acVacFile);
      return -2;
   }

   // Open Output file
   sprintf(acOutFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "R01");
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
             FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      printf("Error opening output file: %s\n", acOutFile);
      return -2;
   }

   // Output first header record
   if (iFirstRec > 0)
   {
      memset(acBuf, '9', iRecLen);
      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
   }


   // Get first RollRec
   iRet = fread(acRollRec,  1, iRollLen, fdRoll);
   bEof = false;

   // Init variables
   iNoMatch=iBadCity=iBadSuffix=0;

   // Merge loop
   while (!bEof)
   {
      Smx_MergeRoll(acBuf, acRollRec, iRollLen, CREATE_R01);

      // Merge Vacant Land
      if (fdVac)
         iRet = Smx_MergeVac(acBuf);

      // Merge Char
      if (fdChar)
         iRet = Smx_MergeChar(acBuf);

      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
      if (!(++lLDRRecCount % 1000))
         printf("\r%u", lLDRRecCount);

      if (!bRet)
      {
         LogMsg("***** Error writing to output file at record %d\n", lLDRRecCount);
         lRet = WRITE_ERR;
         break;
      }

      iRet = fread(acRollRec,  1, iRollLen,fdRoll);

      if (!iRet)
         bEof = true;
   }

   // Close files
   if (fdRoll)
      fclose(fdRoll);
   if (fdChar)
      fclose(fdChar);
   if (fdVac)
      fclose(fdVac);
   if (fhOut)
      CloseHandle(fhOut);

   LogMsg("Total input records:        %u", lLDRRecCount);
   LogMsg("      output records:       %u", lLDRRecCount);
   LogMsg("      Unit from Legal:      %u", lLglUnit);
   LogMsg("      bad-city records:     %u", iBadCity);
   LogMsg("      bad-suffix records:   %u\n", iBadSuffix);
   LogMsg("      Char matched:         %u", lCharMatch);
   LogMsg("      Char skiped:          %u\n", lCharSkip);
   LogMsg("      Vac. matched:         %u", lVacMatch);
   LogMsg("      Vac. skiped:          %u\n", lVacSkip);

   printf("\nTotal output records: %u\n", lLDRRecCount);

   lRecCnt = lLDRRecCount;
   return lRet;
}

/****************************** Smx_Load_LDRCsv ******************************
 *
 * Load updated roll file Secured Roll 2024.csv (07/12/2024)
 *
 ******************************************************************************/

int Smx_Load_LDRCsv(int iSkip)
{
   char     acRec[MAX_RECSIZE], acBuf[MAX_RECSIZE], acRollRec[MAX_RECSIZE], acLastApn[32],
            acOutFile[_MAX_PATH], acSrtFile[_MAX_PATH], *pTmp;
   int      iRet, iTmp, iRetiredRec=0, iDupApn=0;
   DWORD    nBytesWritten;
   BOOL     bRet;
   HANDLE   fhOut;

   LogMsg0("Loading LDR %d", lLienYear);


   // Sort input
   sprintf(acSrtFile, "%s\\Smx\\Smx_Lien.srt", acTmpPath);
   GetPrivateProfileString("SMX", "RollSrtCmd", "", acRec, 256, acIniFile);
   iTmp = sortFile(acRollFile, acSrtFile, acRec, &iRet);
   if (!iTmp)
      return iRet;

   // Open roll file
   LogMsg("Open Roll file %s", acSrtFile);
   fdRoll = fopen(acSrtFile, "r");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening LDR file: %s\n", acSrtFile);
      return 2;
   }

   LogMsg("Open Char file %s", acCChrFile);
   fdChar = fopen(acCChrFile, "r");
   if (fdChar == NULL)
   {
      LogMsg("***** Error opening Char file: %s\n", acCChrFile);
      return -2;
   }

   // Open Output file
   sprintf(acOutFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "R01");
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening output file: %s\n", acOutFile);
      return 4;
   }

   // Init variables
   iNoMatch=iBadCity=iBadSuffix=0;
   strcpy(acLastApn, "000000000");

   // Output first header record
   if (iSkip > 0)
   {
      memset(acBuf, '9', iRecLen);
      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
   }

   // Get 1st rec
   pTmp = fgets((char *)&acRollRec[0], MAX_RECSIZE, fdRoll);

   // Merge loop
   lLDRRecCount = 0;
   while (!feof(fdRoll))
   {
#ifdef _DEBUG
      //if (!memcmp(acRollRec, "689040017X260", 13) || !memcmp(acBuf, "689040017260", 12))
      //   iTmp = 0;
#endif

      // Create R01 record from roll data
      iRet = Smx_MergeRollCsv(acBuf, acRollRec, MAX_RECSIZE, CREATE_R01);
      if (!iRet)
      {
         // Merge Char
         if (fdChar)
            iTmp = Smx_MergeStdChar(acBuf);

         if (memcmp(acLastApn, acBuf, SIZ_APN_S))
         {
            memcpy(acLastApn, acBuf, SIZ_APN_S);
            bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
            if (!bRet)
            {
               LogMsg("***** Error occurs: %d\n", GetLastError());
               break;
            }
            lLDRRecCount++;
         } else
         {
               LogMsg("---> Drop duplicate APN: %.14s", acLastApn);
               iDupApn++;
         }
      }

      if (!(lLDRRecCount % 1000))
         printf("\r%u", lLDRRecCount);

      // Read next roll record
      pTmp = fgets(acRollRec, MAX_RECSIZE, fdRoll);
   }

   // Close files
   if (fdRoll)
      fclose(fdRoll);
   if (fdChar)
      fclose(fdChar);
   if (fhOut)
      CloseHandle(fhOut);

   LogMsg("Total output records:       %u", lLDRRecCount);
   LogMsg("Total bad-city records:     %u", iBadCity);
   LogMsg("Total bad-suffix records:   %u\n", iBadSuffix);

   LogMsg("Number of Char matched:     %u", lCharMatch);
   LogMsg("          Char skiped:      %u\n", lCharSkip);
   LogMsg("          duplicate APN:    %u\n", iDupApn);

   printf("\nTotal output records: %u", lLDRRecCount);

   lRecCnt = lLDRRecCount;
   return 0;
}

/******************************* Lax_CreateLienRec ***************************
 *
 * Return 0 if successful, >0 if error
 *
 *****************************************************************************/

int Smx_CreateLienRec(char *pOutbuf, char *pRollRec)
{
   char     acTmp[256];
   long     iTmp;
   LONGLONG lTmp;
   LIENEXTR *pLienRec = (LIENEXTR *)pOutbuf;
   SMX_ROLL *pRec     = (SMX_ROLL *)pRollRec;

   // Clear output buffer
   memset(pOutbuf, ' ', sizeof(LIENEXTR));

   // Start copying data
   memcpy(pLienRec->acApn, pRec->Apn, SMX_SIZ_APN);

   // TRA
   lTmp = atoin(pRec->Tra, SMX_SIZ_TRA);
   if (lTmp > 0)
   {
      iTmp = sprintf(acTmp, "%.6d", lTmp);
      memcpy(pLienRec->acTRA, acTmp, iTmp);
   }

   // Year assessed
   memcpy(pLienRec->acYear, myCounty.acYearAssd, 4);

   // Land
   long lLand = atoin(pRec->Land, SMX_SIZ_LAND);
   if (lLand > 0)
   {
      iTmp = sprintf(acTmp, "%*u", sizeof(pLienRec->acLand), lLand);
      memcpy(pLienRec->acLand, acTmp, iTmp);
   }

   // Improve
   long lImpr = atoin(pRec->Improv, SMX_SIZ_IMPROV);
   if (lImpr > 0)
   {
      iTmp = sprintf(acTmp, "%*u", sizeof(pLienRec->acImpr), lImpr);
      memcpy(pLienRec->acImpr, acTmp, iTmp);
   }

   // Other values
   long lPers    = atoin(pRec->Pers_Prop,  SMX_SIZ_PERS_PROP);
   long lFixture = atoin(pRec->Trade_Fixt, SMX_SIZ_TRADE_FIXT);
   long lMineral = atoin(pRec->Mineral,    SMX_SIZ_MINERALRTS);
#ifdef _DEBUG
   //long lRoot    = atoin(pRec->Root,       SMX_SIZ_ROOT);
   //if (lRoot > 0)
   //   lTmp = 0;
#endif
   lTmp = lPers + lFixture + lMineral;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*u", SIZ_LIEN_FIXT, lTmp);
      memcpy(pLienRec->acOther, acTmp, SIZ_OTHER);

      if (lPers > 0)
      {
         iTmp = sprintf(acTmp, "%*u", sizeof(pLienRec->acPP_Val), lPers);
         memcpy(pLienRec->acPP_Val, acTmp, iTmp);
      }

      if (lFixture > 0)
      {
         iTmp = sprintf(acTmp, "%*u", sizeof(pLienRec->acME_Val), lFixture);
         memcpy(pLienRec->acME_Val, acTmp, iTmp);
      }
      if (lMineral > 0)
      {
         iTmp = sprintf(acTmp, "%*u", sizeof(pLienRec->extra.Smx.Mineral), lMineral);
         memcpy(pLienRec->extra.Smx.Mineral, acTmp, iTmp);
      }
   }

   // Gross total
   lTmp += (lLand+lImpr);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*u", SIZ_LIEN_FIXT, lTmp);
      memcpy(pLienRec->acGross, acTmp, SIZ_LIEN_FIXT);
   }

   // Ratio
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*u", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
      memcpy(pLienRec->acRatio, acTmp, SIZ_RATIO);
   }

   // Exempt
   long lExe1 = atoin(pRec->Exemp_1, SMX_SIZ_EXEMP_1);
   long lExe2 = atoin(pRec->Exemp_2, SMX_SIZ_EXEMP_1);
   lTmp = lExe1 + lExe2;
   {
      iTmp = sprintf(acTmp, "%*u", sizeof(pLienRec->acExAmt), lTmp);
      memcpy(pLienRec->acExAmt, acTmp, iTmp);
   }

   if (pRec->Exemp_Code[0] == 'H')
      pLienRec->acHO[0] = '1';      // 'Y'
   else
      pLienRec->acHO[0] = '2';      // 'N'

   // Set full exemption flag
   lTmp = atoin(pRec->Net, SMX_SIZ_NET);
   if (pRec->Exemp_Code[0] > ' ' && lTmp == 0)
      pLienRec->SpclFlag = LX_FULLEXE_FLG;

   // Prop8
   //if (pRec->LandKey == '8')
   //   pLienRec->SpclFlag |= LX_PROP8_FLG;

   pLienRec->LF[0] = '\n';
   pLienRec->LF[1] = 0;
   return 0;
}

int Smx_CreateLienRecCsv(char *pOutbuf, char *pRollRec)
{
   char     acTmp[256];
   long     iTmp, iRet;
   LONGLONG lTmp;
   LIENEXTR *pLienRec = (LIENEXTR *)pOutbuf;

   // Parse roll record
   iRet = ParseStringIQ(pRollRec, '|', MAX_FLD_TOKEN, apTokens);
   if (iRet < SMX_R_FLDS)
   {
      LogMsg("***** Error: bad input record for APN=%s", apTokens[SMX_R_APN]);
      return -1;
   }

   // Clear buffer
   memset(pOutbuf, ' ', sizeof(LIENEXTR));

   // Start copying data
   memcpy(pOutbuf, apTokens[SMX_R_APN], strlen(apTokens[SMX_R_APN]));
   *(pOutbuf+OFF_STATUS) = 'A';

   // TRA
   iTmp = atol(apTokens[SMX_R_TRA]);
   sprintf(acTmp, "%.6u", iTmp);
   memcpy(pLienRec->acTRA, acTmp, 6);

   // Year assessed
   memcpy(pLienRec->acYear, myCounty.acYearAssd, 4);

   // Land
   LONGLONG lLand = atol(apTokens[SMX_R_LAND_VAL]);
   if (lLand > 0)
   {
      iTmp = sprintf(acTmp, "%*u", sizeof(pLienRec->acLand), lLand);
      memcpy(pLienRec->acLand, acTmp, iTmp);
   }

   // Improve
   LONGLONG lImpr = atol(apTokens[SMX_R_IMPR_VAL]);
   if (lImpr > 0)
   {
      iTmp = sprintf(acTmp, "%*u", sizeof(pLienRec->acImpr), lImpr);
      memcpy(pLienRec->acImpr, acTmp, iTmp);
   }

   // Other values
   long lPers    = atol(apTokens[SMX_R_PP_VAL]);
   long lFixture = atol(apTokens[SMX_R_FIX_VAL]);
   long lMineral = atol(apTokens[SMX_R_MIN_VAL]);
   lTmp = lPers + lFixture + lMineral;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*u", SIZ_LIEN_FIXT, lTmp);
      memcpy(pLienRec->acOther, acTmp, SIZ_OTHER);

      if (lPers > 0)
      {
         iTmp = sprintf(acTmp, "%*u", sizeof(pLienRec->acPP_Val), lPers);
         memcpy(pLienRec->acPP_Val, acTmp, iTmp);
      }
      if (lFixture > 0)
      {
         iTmp = sprintf(acTmp, "%*u", sizeof(pLienRec->acME_Val), lFixture);
         memcpy(pLienRec->acME_Val, acTmp, iTmp);
      }
      if (lMineral > 0)
      {
         iTmp = sprintf(acTmp, "%*u", sizeof(pLienRec->extra.Smx.Mineral), lMineral);
         memcpy(pLienRec->extra.Smx.Mineral, acTmp, iTmp);
      }
   }

   // Gross total
   LONGLONG lGross = lTmp + lLand + lImpr;
   if (lGross > 0)
   {
      sprintf(acTmp, "%*u", SIZ_LIEN_FIXT, lGross);
      memcpy(pLienRec->acGross, acTmp, SIZ_LIEN_FIXT);
   }

   // Ratio
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*u", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
      memcpy(pLienRec->acRatio, acTmp, SIZ_RATIO);
   }

   // Exempt
   LONGLONG lExe = atol(apTokens[SMX_R_EXE1]) + atol(apTokens[SMX_R_EXE2]);
   if (lExe > 0)
   {
      iTmp = sprintf(acTmp, "%*u", sizeof(pLienRec->acExAmt), lExe);
      memcpy(pLienRec->acExAmt, acTmp, iTmp);
      vmemcpy(pLienRec->extra.Smx.Exe_Code, apTokens[SMX_R_EXE_CODE], SIZ_LIEN_EXECODEM);
      vmemcpy(pLienRec->acExCode, apTokens[SMX_R_EXE_CODE], SIZ_LIEN_EXECODE);
   }

   if (!memcmp(apTokens[SMX_R_EXE_CODE], "HO", 2))
      pLienRec->acHO[0] = '1';      // 'Y'
   else
      pLienRec->acHO[0] = '2';      // 'N'

   // Set full exemption flag
   lTmp = atol(apTokens[SMX_R_NET_VAL]);
   if (*apTokens[SMX_R_EXE_CODE] > ' ' && lTmp == 0)
      pLienRec->SpclFlag = LX_FULLEXE_FLG;
   else if (lGross <= lExe)
      pLienRec->SpclFlag = LX_FULLEXE_FLG;

   pLienRec->LF[0] = '\n';
   pLienRec->LF[1] = 0;
   return 0;
}

/********************************* Smx_ExtrLien *****************************
 *
 *
 ****************************************************************************/

int Smx_ExtrLien()
{
   char  *pTmp, acBuf[MAX_RECSIZE], acRollRec[MAX_RECSIZE], acOutFile[_MAX_PATH];
   long  lCnt=0;
   FILE  *fdLien;

   // Open roll file
   LogMsg("Open Roll file %s", acRollFile);
   fdRoll = fopen(acRollFile, "r");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", acRollFile);
      return -2;
   }

   // Open Output file
   sprintf(acOutFile, acLienTmpl, myCounty.acCntyCode, myCounty.acCntyCode);
   LogMsg("Open output file %s", acOutFile);
   fdLien = fopen(acOutFile, "w");
   if (fdLien == NULL)
   {
      LogMsg("***** Error opening Lien extract file: %s\n", acOutFile);
      return -4;
   }

   // Merge loop
   while (!feof(fdRoll))
   {
      // Get roll rec
      pTmp = fgets(acRollRec, MAX_RECSIZE, fdRoll);
      if (!pTmp)
         break;

      // Create new lien record
      Smx_CreateLienRec(acBuf, acRollRec);

      fputs(acBuf, fdLien);
      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdRoll)
      fclose(fdRoll);
   if (fdLien)
      fclose(fdLien);

   LogMsgD("Total records output:     %d\n", lCnt);

   lRecCnt = lCnt;
   return 0;
}

int Smx_ExtrLienCsv()
{
   char  *pTmp, acBuf[MAX_RECSIZE], acRollRec[MAX_RECSIZE], acOutFile[_MAX_PATH];
   long  lCnt=0;
   FILE  *fdLien;

   // Open roll file
   LogMsg("Open LDR file %s", acRollFile);
   fdRoll = fopen(acRollFile, "r");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", acRollFile);
      return -2;
   }

   // Open Output file
   sprintf(acOutFile, acLienTmpl, myCounty.acCntyCode, myCounty.acCntyCode);
   LogMsg("Open output file %s", acOutFile);
   fdLien = fopen(acOutFile, "w");
   if (fdLien == NULL)
   {
      LogMsg("***** Error opening Lien extract file: %s\n", acOutFile);
      return -4;
   }

   // Drop header
   pTmp = fgets(acRollRec, MAX_RECSIZE, fdRoll);

   // Merge loop
   while (!feof(fdRoll))
   {
      // Get roll rec
      pTmp = fgets(acRollRec, MAX_RECSIZE, fdRoll);
      if (!pTmp)
         break;

      // Create new lien record
      Smx_CreateLienRecCsv(acBuf, acRollRec);

      fputs(acBuf, fdLien);
      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdRoll)
      fclose(fdRoll);
   if (fdLien)
      fclose(fdLien);

   LogMsgD("Total records output:     %d\n", lCnt);

   lRecCnt = lCnt;
   return 0;
}

/********************************* Smx_FormatSale ****************************
 *
 * Return -1 if bad record.  0 if OK.
 *
 *****************************************************************************/

int Smx_FormatSale(char *pFmtSale, char *pSale)
{
   SCSAL_REC *pCSale = (SCSAL_REC *)pFmtSale;
   SMX_SALE  *pSaleRec=(SMX_SALE *) pSale;
   
   char     acTmp[32];
   long     lPrice;

   // Drop records without DocDate & DocNum
   if (pSaleRec->Apn[0] == ' ' || pSaleRec->DocDate[0] == ' ')
      return -1;

   memset(pFmtSale, ' ', sizeof(SCSAL_REC));

   // APN
   memcpy(pCSale->Apn, pSaleRec->Apn, iApnLen);

   // DocNum
   memcpy(pCSale->DocNum, pSaleRec->DocNum, SMX_SIZ_DOCNUM);

   // DocDate
   if (isValidYMD(pSaleRec->DocDate))
      memcpy(pCSale->DocDate, pSaleRec->DocDate, SMX_SIZ_DOCDATE);
   else
      return -1;

   // Sale Price
   lPrice = atoin(pSaleRec->SalePrice, SMX_SIZ_SALE_AMT1);
   if (lPrice > 1000)
   {
      sprintf(acTmp, "%*u", SIZ_SALE1_AMT, lPrice);
      memcpy(pCSale->SalePrice, acTmp, SIZ_SALE1_AMT);
   }

   pCSale->ARCode = 'A';
   pCSale->CRLF[0] = '\n';
   pCSale->CRLF[1] = 0;

   return 0;
}

/****************************** Smx_ExtrSale *********************************
 *
 * Extract history sale.  
 *
 * Return 0 if successful, otherwise error
 *
 *****************************************************************************/

int Smx_ExtrSale(void)
{
   char     *pTmp, acCSalRec[1024], acSaleRec[1024], acTmp[256];
   char     acTmpSale[_MAX_PATH];

   long     lCnt=0;
   int		iRet, iTmp, iUpdateSale=0;

   // Check current sale file
   if (_access(acSaleFile, 0))
   {
      LogMsg("***** Missing current sale file %s", acSaleFile);
      return -1;
   }

   // Open current sale
   LogMsg("Open current sale file %s", acSaleFile);
   fdSale = fopen(acSaleFile, "r");
   if (fdSale == NULL)
   {
      LogMsg("***** Error opening current sale file: %s\n", acSaleFile);
      return -2;
   }

   // Create output file
   sprintf(acTmpSale, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "tmp");
   LogMsg("Create updated sale file %s", acTmpSale);
   fdCSale = fopen(acTmpSale, "w");
   if (fdCSale == NULL)
   {
      LogMsg("***** Error creating output sale file: %s\n", acTmpSale);
      return -3;
   }

   // Merge loop
   while (!feof(fdSale))
   {
      // Get current sale
      pTmp = fgets(acSaleRec, 1024, fdSale);
      if (!pTmp)
         break;

      iRet = strlen(acSaleRec);
      if (iRet > iSaleLen)
         LogMsg("*** Incompatible sale record size %d > %d, please verify and update INI file.", iRet, iSaleLen);

      iRet = Smx_FormatSale(acCSalRec, acSaleRec);
      if (!iRet)
      {
         fputs(acCSalRec, fdCSale);
         iUpdateSale++;
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdCSale)
      fclose(fdCSale);
   if (fdSale)
      fclose(fdSale);

   // Update sale history file
   sprintf(acCSalFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "sls");

   // Sort/merge sale file - APN (asc), Saledate (asc), DocNum (asc)
   sprintf(acTmp, "S(1,%d,C,A,27,8,C,A,15,%d,C,A,57,10,C,D) DUPO(1,34)", iApnLen, SALE_SIZ_DOCNUM);
   iTmp = sortFile(acTmpSale, acCSalFile, acTmp);

   LogMsg("Total sale records processed:  %u", lCnt);
   LogMsg("Total sale records updated:    %u", iUpdateSale);
   LogMsg("Total extracted sale records:  %u", iTmp);

   LogMsg("Update Sale History completed.");
   printf("\nTotal extracted sale records:  %u\n", iTmp);

   return 0;
}

/***************************** Smx_ExtrSaleCsv ******************************
 *
 * Create standard sale file from SaleHist.csv
 *
 ****************************************************************************/

int Smx_ExtrSaleCsv()
{
   char     acTmpFile[_MAX_PATH];
   char     acTmp[256], acRec[1024], acSaleRec[1024], *pTmp;

   FILE      *fdOut;
   SCSAL_REC *pSale = (SCSAL_REC *)&acSaleRec[0];

   int      iTmp, iDrop=0;
   long     lCnt=0;
   unsigned long lPrice, lTmp;

   LogMsg0("Creating standard sale file for %s", myCounty.acCntyCode);

   // Open Sales file
   LogMsg("Open Sales file %s", acSaleFile);
   fdSale = fopen(acSaleFile, "r");
   if (fdSale == NULL)
   {
      LogMsg("***** Error opening Sales file: %s\n", acSaleFile);
      return -1;
   }

   // Open Output file
   sprintf(acTmpFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "TMP");

   LogMsg("Open output file %s", acTmpFile);
   fdOut = fopen(acTmpFile, "w");
   if (fdOut == NULL)
   {
      LogMsg("***** Error creating sale output file: %s\n", acTmpFile);
      return -2;
   }

   // Loop through record set
   while (!feof(fdSale))
   {  
      if (!(pTmp = fgets(acRec, 1024, fdSale)))
         break;
      lCnt++;

      if (*pTmp > '9')
      {
         iDrop++;
         continue;
      }

      if (*pTmp < '0')
      {
         LogMsg("*** Warning: No APN at %d.", lCnt);
         iDrop++;
         continue;
      }

      // Parse input rec
      iTokens = ParseStringIQ(acRec, '|', SMX_S_FLDS+1, apTokens);
      if (iTokens < SMX_S_FLDS)
      {
         LogMsg0("*** Error: bad sale record %.10s (#tokens=%d)", acRec, iTokens);
         iDrop++;
         continue;
      }

      if (*apTokens[SMX_S_SALEDATE] < '1' || *apTokens[SMX_S_SALEDOC] < '1' || *apTokens[SMX_S_SALEDOC] > '9')
      {
         if (bDebug)
            LogMsg("*** Warning: bad sale record %.10s (either no saledate or saledoc", acRec);
         iDrop++;
         continue;
      }

      // Reset output record
      memset(acSaleRec, ' ', sizeof(SCSAL_REC));

      // APN
      memcpy(pSale->Apn, apTokens[SMX_S_APN], strlen(apTokens[SMX_S_APN]));

      // Doc date
      vmemcpy(pSale->DocDate, apTokens[SMX_S_SALEDATE], 10);

      // Docnum
      iTmp = iTrim(apTokens[SMX_S_SALEDOC]);
      if (iTmp < 10)
      {
         sprintf(acTmp, "19%s", apTokens[SMX_S_SALEDOC]);
         vmemcpy(pSale->DocNum, acTmp, 10);
      } else
      {
         iTmp = remChar(apTokens[SMX_S_SALEDOC], '-');
         memcpy(pSale->DocNum, apTokens[SMX_S_SALEDOC], iTmp);
         if (iTmp < 10)
            LogMsg("*** Bad DocNum %s [%s]", apTokens[SMX_S_SALEDOC], apTokens[SMX_S_APN]);
      }

#ifdef _DEBUG
      //if (!memcmp(pSale->Apn, "027071031000", 9))
      //   lPrice = 0;
#endif

      // Sale price
      lPrice = (unsigned long)atol(apTokens[SMX_S_SALEAMT]);

      // Doc code 
      if (lPrice > 10000)
      {
         sprintf(acTmp, "%*u", SALE_SIZ_SALEPRICE, lPrice);
         memcpy(pSale->SalePrice, acTmp, SALE_SIZ_SALEPRICE);

         if (*(apTokens[SMX_S_SALECODE]+strlen(apTokens[SMX_S_SALECODE])-1) == 'M')
            pSale->MultiSale_Flg = 'Y';
      }

      if (!memcmp(apTokens[SMX_S_SALECODE], "SALE", 4) ||
          !memcmp(apTokens[SMX_S_SALECODE], "CIO-SALE", 5) ||
          !memcmp(apTokens[SMX_S_SALECODE], "CIO-BSALE", 6) )
         pSale->DocType[0] = '1';
      else if (!memcmp(apTokens[SMX_S_SALECODE], "CIO-REASS", 7))
         memcpy(pSale->DocType, "13", 2);
      else if (!memcmp(apTokens[SMX_S_SALECODE], "CIO-RETRL", 7))
         pSale->DocType[0] = '4';

      //vmemcpy(pSale->DocCode, apTokens[SMX_S_SALECODE], SALE_SIZ_DOCCODE);
      //iTmp = findDocType(apTokens[SMX_S_SALECODE], (IDX_TBL5 *)&SMX_DocCode[0]);
      //if (iTmp >= 0)
      //{
      //   memcpy(pSale->DocType, SMX_DocCode[iTmp].pCode, SMX_DocCode[iTmp].iCodeLen);
      //   pSale->NoneSale_Flg = SMX_DocCode[iTmp].flag;
      //} else if (lPrice > 100)
      //   pSale->DocType[0] = '1';
     
      pSale->CRLF[0] = 10;
      pSale->CRLF[1] = 0;
      fputs(acSaleRec, fdOut);

      if (!(lCnt % 1000))
         printf("\r%u", lCnt);
   }

   fclose(fdSale);
   fclose(fdOut);
   
   // If cum sale file exist, combine them
   if (!_access(acCSalFile, 0))
   {
      strcpy(acRec, acTmpFile);
      sprintf(acSaleRec, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "srt");
      sprintf(acTmpFile, "%s+%s", acRec, acCSalFile);
   } 

   // Sort output file and dedup on APN asc, DocDate asc, DocNum asc
   sprintf(acTmp, "S(1,12,C,A,27,8,C,A,15,12,C,A) F(TXT) DUPO(1,34) ");
   lTmp = sortFile(acTmpFile, acSaleRec, acTmp);
   if (lTmp > 0)
   {
      sprintf(acRec, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "sav");
      if (!_access(acRec, 0))
         DeleteFile(acRec);
      rename(acCSalFile, acRec);
      
      // Rename srt to SLS file
      rename(acSaleRec, acCSalFile);
      iTmp = 0;
   } else
      iTmp = -1;

   LogMsg("Total processed records: %u", lCnt);
   LogMsg("        records dropped: %u", iDrop);
   LogMsg("                 output: %u\n", lTmp);
   return iTmp;
}

/****************************** Smx_ParseTaxBase ******************************
 *
 * Create TAXBASE record
 * The 2 input records are installment 1 & 2.  We have to combine them into one base record.
 *
 ******************************************************************************/

int Smx_ParseTaxBase(char *pOutbuf, char *pRec1, char *pRec2)
{
   char     *apItem1[50], *apItem2[50], *pTmp;
   int      iTem1, iTem2;
   double   dTaxAmt1, dTaxAmt2, dPaidAmt1, dPaidAmt2, dTotalDue, dTotalTax;

   TAXBASE  *pTaxBase = (TAXBASE *)pOutbuf;

   iTem1 = ParseStringNQ(pRec1, ',', 50, apItem1);
   if (iTem1 < SRI_FLDS)
   {
      LogMsg("***** Parse TDS -->Bad tax record 1 (%d)", iTem1);
      return -1;
   }
   iTem2 = ParseStringNQ(pRec2, ',', 50, apItem2);
   if (iTem2 < SRI_FLDS)
   {
      LogMsg("***** Parse TDS -->Bad tax record 2 (%d)", iTem2);
      return -1;
   }

   if  (strcmp(apItem1[SRI_APN], apItem2[SRI_APN]))
   {
      LogMsg("***** Mismatch APN: %s <> %s", apItem1[SRI_APN], apItem2[SRI_APN]);
      return 1;
   }

   // Init buffer
   memset(pOutbuf, 0, sizeof(TAXBASE));
   strcpy(pTaxBase->Assmnt_No, apItem1[SRI_APN]);

   // Remove hyphen from APN
   remChar(apItem1[SRI_APN], '-');
   strcpy(pTaxBase->Apn, apItem1[SRI_APN]);

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "032121230", 9))
   //   iTmp = 0;
#endif

   dTotalDue=dPaidAmt1=dPaidAmt2 = 0.0;

   // Tax Year
   iTaxYear = atol(apItem1[SRI_BILL_NO]);
   if (iTaxYear != lTaxYear)
      return iTaxYear;
   memcpy(pTaxBase->TaxYear, apItem1[SRI_BILL_NO], 4);

   // BillNum
   strcpy(pTaxBase->BillNum, apItem1[SRI_BILL_NO]);
   dTaxAmt1 = atof(apItem1[SRI_TAX_AMT]);
   dTaxAmt2 = atof(apItem2[SRI_TAX_AMT]);
   dTotalTax = dTaxAmt1+dTaxAmt2;

   // Installment 1
   if (*apItem1[SRI_INST_NO] == '1')
   {
      // Tax Amt
      if (dTaxAmt1 > 0.0)
      {
         strcpy(pTaxBase->TaxAmt1, apItem1[SRI_TAX_AMT]);

         // Due date
         InstDueDate(pTaxBase->DueDate1, 1, iTaxYear);

         if (*apItem1[SRI_PAID_STATUS] == 'P')
         {
            pTaxBase->Inst1Status[0] = TAX_BSTAT_PAID;
            dPaidAmt1 = dTaxAmt1;

            pTmp = dateConversion(apItem1[SRI_PAID_DATE], pTaxBase->PaidDate1, MM_DD_YYYY_1);
            if (!pTmp)
               LogMsg("*** Invalid paid date1: APN=%s, PaidDate1=%s", apItem1[SRI_APN], apItem1[SRI_PAID_DATE]);
         } else if (*apItem1[SRI_PAID_STATUS] == 'U')
            pTaxBase->Inst1Status[0] = TAX_BSTAT_UNPAID;
         else
            LogMsg("*** Unknown paid status1: %s [%s]", apItem1[SRI_PAID_STATUS], apItem1[SRI_APN]);
      } else
            pTaxBase->Inst1Status[0] = TAX_BSTAT_NOTAX;
   } else
      LogMsg("*** Installment #1 out of sync APN=%s", apItem1[SRI_APN]);

   // Installment 2
   if (*apItem2[SRI_INST_NO] == '2')
   {
      if (dTaxAmt2 > 0.0)
      {
         strcpy(pTaxBase->TaxAmt2, apItem2[SRI_TAX_AMT]);
         InstDueDate(pTaxBase->DueDate2, 2, iTaxYear);

         if (*apItem2[SRI_PAID_STATUS] == 'P')
         {
            pTaxBase->Inst2Status[0] = TAX_BSTAT_PAID;
            dPaidAmt2 = dTaxAmt2;

            pTmp = dateConversion(apItem2[SRI_PAID_DATE], pTaxBase->PaidDate2, MM_DD_YYYY_1);
            if (!pTmp)
               LogMsg("*** Invalid paid date2: APN=%s PaidDate2=%s", apItem2[SRI_APN], apItem2[SRI_PAID_DATE]);
         } else if (*apItem2[SRI_PAID_STATUS] == 'U')
            pTaxBase->Inst2Status[0] = TAX_BSTAT_UNPAID;
         else
            LogMsg("*** Unknown paid status2: %s [%s]", apItem2[SRI_PAID_STATUS], apItem2[SRI_APN]);
      } else
         pTaxBase->Inst2Status[0] = TAX_BSTAT_NOTAX;
   } else
      LogMsg("*** Installment #2 out of sync APN=%s", apItem2[SRI_APN]);

   // Total tax amount
   sprintf(pTaxBase->TotalTaxAmt, "%.2f", dTotalTax);

   // Total due amount
   dTotalDue = dTotalTax - (dPaidAmt1+dPaidAmt2);
   sprintf(pTaxBase->TotalDue, "%.2f", dTotalDue);

   if (dPaidAmt1 > 0.0)
      sprintf(pTaxBase->PaidAmt1, "%.2f", dPaidAmt1);
   if (dPaidAmt2 > 0.0)
      sprintf(pTaxBase->PaidAmt2, "%.2f", dPaidAmt2);

   // Penalty amount

   // Bill type
   if (*(apItem2[SRI_ACCT_TYPE]+8) == 'E')
      pTaxBase->BillType[0] = BILLTYPE_SECURED_ESCAPE;
   else
      pTaxBase->BillType[0] = BILLTYPE_SECURED;

   pTaxBase->isSecd[0] = '1';
   pTaxBase->isSupp[0] = '0';

   return 0;
}

/********************************* UpdateTRA **********************************
 *
 * Input: "SC - Secure Roll Data #1 - Accounts.csv"
 *
 ******************************************************************************/

int Smx_UpdateSecAcct(char *pOutbuf)
{
   static   char  acRec[2048], *apItems[30], *pRec=NULL;
   static   int   iItems=0;
   int      iLoop, iTmp;
   char     *pTmp;
   TAXBASE  *pTaxBase = (TAXBASE *)pOutbuf;

   // Get first Char rec for first call
   if (!pRec)
      pRec = fgets(acRec, 2048, fdRoll);

   ReCheck:
   do
   {
      // Compare Apn
      iLoop = memcmp(pTaxBase->Assmnt_No, pRec, strlen(pTaxBase->Assmnt_No));
      if (iLoop > 0)
      {
         pRec = fgets(acRec, 2048, fdRoll);
         if (!pRec)
         {
            fclose(fdRoll);
            fdRoll = NULL;
            break;
         }
      }
   } while (iLoop > 0);

   // If not match, return
   if (iLoop)
      return 1;

#ifdef _DEBUG
//      if (!memcmp(pOutbuf, "10145008", 8))
//         iRet = 0;
#endif
   // Parse input tax data
   iItems = ParseStringNQ(acRec, ',', 30, apItems);
   if (iItems < SRA_FLDS)
   {
      LogMsg("***** Error: bad Tax account record for APN=%s (#tokens=%d).  Should be %d", acRec, iItems, SRA_FLDS);
      return -1;
   } else if (iItems > SRA_FLDS)
      LogMsg("*** Warning: Number of tokens in Tax account record for APN=%s (#tokens=%d).  Should be %d", acRec, iItems, SRA_FLDS);

   iTmp = atoln(apItems[SRA_BILL_NO], 4);
   if (iTmp < lTaxYear)
   {
      pRec = fgets(acRec, 2048, fdRoll);
      goto ReCheck;
   }

   if (strcmp(pTaxBase->BillNum, apItems[SRA_BILL_NO]))
      LogMsg("***** Error: Bill Number are not the same: %s <> %s [%s]", pTaxBase->BillNum, apItems[SRA_BILL_NO], apItems[0]);

   // Update TRA
   if (pTmp = strchr(apItems[SRA_TRA], '-'))
      sprintf(pTaxBase->TRA, "%.3d%s", atol(apItems[SRA_TRA]), pTmp+1);
   else 
      strcpy(pTaxBase->TRA, apItems[SRA_TRA]);

   // Assessment year
   strcpy(pTaxBase->TaxYear, apItems[SRA_ASMT_YR]);
   iApnMatch++;

   // Get next roll record
   pRec = fgets(acRec, 2048, fdRoll);

   return 0;
}

/****************************** Smx_Load_TaxBase *****************************
 *
 * Load "SC - Secure Roll Data #1 - Accounts.csv"
 *    - This file may have multiple tax bill (annual & escape)
 *
 * Load installment file 
 *    - "SC - Secure Roll Data #2 - Installments.csv"
 *    - "SC - Secure Roll Data Transferable - Installments.csv"
 *
 * Return 0 if successful, otherwise error
 *
 *****************************************************************************/

int Smx_Load_TaxBase(char *pSecAcct, char *pSecInst, bool bImport)
{
   char     *pTmp, acBuf[MAX_RECSIZE], acRec1[MAX_RECSIZE], acRec2[MAX_RECSIZE], acBaseFile[_MAX_PATH],
            sSrtCmd1[256], sSrtCmd2[256];
   long     lOut=0, lCnt=0, iRet;
   FILE     *fdInst, *fdBase, *fdR01;
   TAXBASE  *pTaxBase = (TAXBASE *)&acBuf[0];

   LogMsg0("Loading tax base");

   // Sort input file
   if (pSecAcct)
   {
      iRet = GetIniString(myCounty.acCntyCode, "SecSrtCmd1",  "", sSrtCmd1, _MAX_PATH, acIniFile);
      if (iRet > 20)
         GetIniString(myCounty.acCntyCode, "SecSrtCmd2",  "", sSrtCmd2, _MAX_PATH, acIniFile);
      else
      {
         strcpy(sSrtCmd1, "S(#1,C,A,#2,C,A) F(TXT) DEL(44) OMIT(#1,C,GT,\"A\")");
         strcpy(sSrtCmd2, "S(#1,C,A,#2,C,A,#3,C,A) F(TXT) DEL(44) OMIT(#1,C,GT,\"A\")");
      }

      sprintf(acBuf, "%s\\%s\\SR_Acct.srt", acTmpPath, myCounty.acCntyCode);
      iRet = sortFile(pSecAcct, acBuf, sSrtCmd1);
      if (iRet < 100)
         return -1;

      // Open main tax roll
      LogMsg("Open Tax account file %s", acBuf);
      fdRoll = fopen(acBuf, "r");
      if (fdRoll == NULL)
      {
         LogMsg("***** Error opening Tax accountfile: %s\n", acBuf);
         return -2;
      }  
   } else
      fdRoll = NULL;

   // Open installment file
   sprintf(acBuf, "%s\\%s\\SR_Inst.srt", acTmpPath, myCounty.acCntyCode);
   iRet = sortFile(pSecInst, acBuf, sSrtCmd2);
   if (iRet < 100)
      return -1;

   LogMsg("Open Tax installment file %s", acBuf);
   fdInst = fopen(acBuf, "r");
   if (fdInst == NULL)
   {
      LogMsg("***** Error opening Tax installment file: %s\n", acBuf);
      return -2;
   }  

   // Open Output file
   NameTaxCsvFile(acBaseFile, myCounty.acCntyCode, "Base");
   LogMsg("Open output file %s", acBaseFile);
   if (fdRoll)
      fdBase = fopen(acBaseFile, "w");
   else
      fdBase = fopen(acBaseFile, "a+");
   if (fdBase == NULL)
   {
      LogMsg("***** Error creating output file: %s\n", acBaseFile);
      return -4;
   }

   // Open raw file to update TRA
   fdR01 = OpenR01(myCounty.acCntyCode);
   iApnMatch = 0;

   // Start loop 
   while (!feof(fdInst))
   {
      if (!(pTmp = fgets((char *)&acRec1[0], MAX_RECSIZE, fdInst)))
         break;
      if (!(pTmp = fgets((char *)&acRec2[0], MAX_RECSIZE, fdInst)))
         break;

      // Create new TAXBASE record
      iRet = Smx_ParseTaxBase(acBuf, acRec1, acRec2);
      if (!iRet)
      {
         if (fdRoll)
            Smx_UpdateSecAcct(acBuf);
         else if (fdR01)
            UpdateTRA(acBuf, fdR01);

         // Create delimited record
         Tax_CreateTaxBaseCsv(acRec1, (TAXBASE *)&acBuf);
         fputs(acRec1, fdBase);
         lOut++;
      } else 
         LogMsg("---> Load_TaxBase: drop record %d [%.40s]", lCnt, acRec1); 

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdRoll)
      fclose(fdRoll);
   if (fdInst)
      fclose(fdInst);
   if (fdR01)
      fclose(fdR01);
   if (fdBase)
      fclose(fdBase);

   LogMsg("Total records processed:       %u", lCnt);
   LogMsg("Total output records:          %u", lOut);
   LogMsg("Total APN matched records:     %u", iApnMatch);
   printf("\nTotal output records: %u", lCnt);

   // Import into SQL
   if (bImport)
      iRet = doTaxImport(myCounty.acCntyCode, TAX_BASE);
   else
      iRet = 0;

   return iRet;
}

/****************************** Smx_ParseTaxSupp ******************************
 *
 * Create TAXBASE record
 * The 2 input records are installment 1 & 2.  We have to combine them into one base record.
 *
 ******************************************************************************/

int Smx_ParseTaxSupp(char *pOutbuf, char *pRec1, char *pRec2)
{
   char     *apItem1[50], *apItem2[50], *pTmp;
   int      iTem1, iTem2;
   double   dTaxAmt1, dTaxAmt2, dPaidAmt1, dPaidAmt2, dTotalDue, dTotalTax;

   TAXBASE  *pTaxBase = (TAXBASE *)pOutbuf;

   iTem1 = ParseStringNQ(pRec1, ',', 50, apItem1);
   if (iTem1 < SUI_FLDS)
   {
      LogMsg("***** Parse SUI -->Bad tax record 1 (%d)", iTem1);
      return -1;
   }
   iTem2 = ParseStringNQ(pRec2, ',', 50, apItem2);
   if (iTem2 < SUI_FLDS)
   {
      LogMsg("***** Parse SUI -->Bad tax record 2 (%d)", iTem2);
      return -1;
   }

   if  (strcmp(apItem1[SUI_APN], apItem2[SUI_APN]))
   {
      LogMsg("***** Mismatch APN: %s <> %s", apItem1[SUI_APN], apItem2[SUI_APN]);
      return 1;
   }

   // Process secured supplemental only, ignore transferable supplemental
   if (*(apItem1[SUI_ACCT_TYPE]) != 'S')
      return 1;

   // Ignore preliminary roll
   if (!_memicmp(apItem1[SUI_BALANCE_STATUS], "PRE", 3))
      return 1;

   // Init buffer
   memset(pOutbuf, 0, sizeof(TAXBASE));
   strcpy(pTaxBase->Assmnt_No, apItem1[SUI_APN]);

   // Remove hyphen from APN
   remChar(apItem1[SUI_APN], '-');
   strcpy(pTaxBase->Apn, apItem1[SUI_APN]);

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "0884510314", 10))
   //   iRet = 0;
#endif

   dTotalDue=dPaidAmt1=dPaidAmt2 = 0.0;

   // Tax Year
   iTaxYear = atol(apItem1[SUI_BILL_NO]);
   if (iTaxYear != lTaxYear)
   {
      if (bDebug)
         LogMsg("*** Drop rec due to bad tax year: APN=%s, TaxYear=%.4s", pTaxBase->Apn, apItem1[SUI_BILL_NO]);
      return 1;
   }
   memcpy(pTaxBase->TaxYear, apItem1[SUI_BILL_NO], 4);

   // BillNum
   strcpy(pTaxBase->BillNum, apItem1[SUI_BILL_NO]);
   dTaxAmt1 = atof(apItem1[SUI_TAX_AMT]);
   dTaxAmt2 = atof(apItem2[SUI_TAX_AMT]);
   dTotalTax = dTaxAmt1+dTaxAmt2;

   // Installment 1
   if (*apItem1[SUI_INST_NO] == '1')
   {
      // Tax Amt
      if (dTaxAmt1 > 0.0)
      {
         strcpy(pTaxBase->TaxAmt1, apItem1[SUI_TAX_AMT]);

         // Due date
         pTmp = dateConversion(apItem1[SUI_DUE_DATE], pTaxBase->DueDate1, MM_DD_YYYY_1);

         if (!_memicmp(apItem1[SUI_BALANCE_STATUS], "PA", 2))
         {
            pTaxBase->Inst1Status[0] = TAX_BSTAT_PAID;
            dPaidAmt1 = dTaxAmt1;

            pTmp = dateConversion(apItem1[SUI_PAID_DATE], pTaxBase->PaidDate1, MM_DD_YYYY_1);
            if (!pTmp)
               LogMsg("*** Invalid paid date1: APN=%s, PaidDate1=%s", apItem1[SUI_APN], apItem1[SUI_PAID_DATE]);
         } else if (*apItem1[SUI_BALANCE_STATUS] == 'U')
            pTaxBase->Inst1Status[0] = TAX_BSTAT_UNPAID;
         else
            LogMsg("*** Unknown paid status1: %s [%s]", apItem1[SUI_BALANCE_STATUS], apItem1[SUI_APN]);
      } else
            pTaxBase->Inst1Status[0] = TAX_BSTAT_NOTAX;
   } else
      LogMsg("*** Installment #1 out of sync APN=%s", apItem1[SUI_APN]);

   // Installment 2
   if (*apItem2[SUI_INST_NO] == '2')
   {
      if (dTaxAmt2 > 0.0)
      {
         strcpy(pTaxBase->TaxAmt2, apItem2[SUI_TAX_AMT]);
         pTmp = dateConversion(apItem2[SUI_DUE_DATE], pTaxBase->DueDate2, MM_DD_YYYY_1);

         if (!_memicmp(apItem2[SUI_BALANCE_STATUS], "PA", 2))
         {
            pTaxBase->Inst2Status[0] = TAX_BSTAT_PAID;
            dPaidAmt2 = dTaxAmt2;

            pTmp = dateConversion(apItem2[SUI_PAID_DATE], pTaxBase->PaidDate2, MM_DD_YYYY_1);
            if (!pTmp)
               LogMsg("*** Invalid paid date2: APN=%s PaidDate2=%s", apItem2[SUI_APN], apItem2[SUI_PAID_DATE]);
         } else if (*apItem2[SUI_BALANCE_STATUS] == 'U')
            pTaxBase->Inst2Status[0] = TAX_BSTAT_UNPAID;
         else
            LogMsg("*** Unknown paid status2: %s [%s]", apItem2[SUI_BALANCE_STATUS], apItem2[SUI_APN]);
      } else
         pTaxBase->Inst2Status[0] = TAX_BSTAT_NOTAX;
   } else
      LogMsg("*** Installment #2 out of sync APN=%s", apItem2[SUI_APN]);

   // Total tax amount
   sprintf(pTaxBase->TotalTaxAmt, "%.2f", dTotalTax);

   // Total due amount
   dTotalDue = atof(apItem1[SUI_BALANCE_AMOUNT])+atof(apItem2[SUI_BALANCE_AMOUNT]);
   sprintf(pTaxBase->TotalDue, "%.2f", dTotalDue);

   if (dPaidAmt1 > 0.0)
      sprintf(pTaxBase->PaidAmt1, "%.2f", dPaidAmt1);
   if (dPaidAmt2 > 0.0)
      sprintf(pTaxBase->PaidAmt2, "%.2f", dPaidAmt2);

   // Penalty amount

   // Bill type
   pTaxBase->BillType[0] = BILLTYPE_SECURED_SUPPL;

   pTaxBase->isSecd[0] = '0';
   pTaxBase->isSupp[0] = '1';

   return 0;
}

/********************************* UpdateTRA **********************************
 *
 * Input: "SC - Supp Roll Data #1 - Accounts.csv"
 *
 ******************************************************************************/

int Smx_UpdateSuppAcct(char *pOutbuf)
{
   static   char  acRec[2048], *apItems[50], *pRec=NULL;
   static   int   iItems=0;
   int      iLoop;
   char     *pTmp;
   TAXBASE  *pTaxBase = (TAXBASE *)pOutbuf;

   // Get first Char rec for first call
   if (!pRec)
      pRec = fgets(acRec, 2048, fdRoll);

   do
   {
      // Compare Apn
      iLoop = memcmp(pTaxBase->Assmnt_No, pRec, strlen(pTaxBase->Assmnt_No));
      if (iLoop > 0)
      {
         pRec = fgets(acRec, 2048, fdRoll);
         if (!pRec)
         {
            fclose(fdRoll);
            fdRoll = NULL;
            break;
         }
      }
   } while (iLoop > 0);

   // If not match, return
   if (iLoop)
      return 1;

#ifdef _DEBUG
//      if (!memcmp(pOutbuf, "10145008", 8))
//         iRet = 0;
#endif
   // Parse input tax data
   iItems = ParseStringNQ(acRec, ',', 50, apItems);
   if (iItems < SUA_FLDS)
   {
      LogMsg("***** Error: bad Tax supp account record for APN=%s (#tokens=%d).  Should be %d", acRec, iItems, SUA_FLDS);
      return -1;
   } else if (iItems > SUA_FLDS)
      LogMsg("*** Warning: Number of tokens in Tax supp account record for APN=%s (#tokens=%d).  Should be %d", acRec, iItems, SUA_FLDS);

   if (strcmp(pTaxBase->BillNum, apItems[SUA_BILL_NO]))
      LogMsg("***** Error: Bill Number are not the same: %s <> %s [%s]", pTaxBase->BillNum, apItems[SUA_BILL_NO], apItems[0]);

   // Update TRA
   if (pTmp = strchr(apItems[SUA_TRA], '-'))
      sprintf(pTaxBase->TRA, "%.3d%s", atol(apItems[SUA_TRA]), pTmp+1);

   // Assessment year
   strcpy(pTaxBase->TaxYear, apItems[SUA_ASMT_YR]);
   iApnMatch++;

   // Get next roll record
   pRec = fgets(acRec, 2048, fdRoll);

   return 0;
}

/******************************* Smx_Load_TaxSupp ****************************
 *
 * Load "SC - Supp Roll Data #2 - Installments.csv" & "SC - Supp Roll Data #1 - Accounts.csv"
 *
 * Return 0 if success.
 *
 *****************************************************************************/

int Smx_Load_TaxSupp(char *pSecAcct, char *pSecInst, bool bImport)
{
   char     *pTmp, acBuf[MAX_RECSIZE], acRec1[MAX_RECSIZE], acRec2[MAX_RECSIZE], acBaseFile[_MAX_PATH],
            sSrtCmd1[256], sSrtCmd2[256];
   long     lOut=0, lCnt=0, iRet;
   FILE     *fdInst, *fdBase;
   TAXBASE  *pTaxBase = (TAXBASE *)&acBuf[0];

   LogMsg0("Loading tax supplemental");

   // Sort input file
   if (pSecAcct)
   {
      iRet = GetIniString(myCounty.acCntyCode, "SupSrtCmd1",  "", acBuf, _MAX_PATH, acIniFile);
      if (iRet > 20)
      {
         sprintf(sSrtCmd1, acBuf, lLienYear);
         GetIniString(myCounty.acCntyCode, "SupSrtCmd2",  "", sSrtCmd2, _MAX_PATH, acIniFile);
      } else
      {
         sprintf(sSrtCmd1, "S(#1,C,A,#4,C,A) F(TXT) DEL(44) OMIT(#4,C,NE,\"%d\",OR,#3,C,GT,\"T\")", lLienYear);
         strcpy(sSrtCmd2, "S(#1,C,A,#2,C,A,#4,C,A) F(TXT) DEL(44)");
      }

      // Sort on APN & Asmt year
      sprintf(acBuf, "%s\\%s\\SU_Acct.srt", acTmpPath, myCounty.acCntyCode);
      iRet = sortFile(pSecAcct, acBuf, sSrtCmd1);
      if (iRet < 100)
         return -1;

      // Open main tax roll
      LogMsg("Open Tax account file %s", acBuf);
      fdRoll = fopen(acBuf, "r");
      if (fdRoll == NULL)
      {
         LogMsg("***** Error opening Tax accountfile: %s\n", acBuf);
         return -2;
      }  
   } else
      fdRoll = NULL;

   // Open installment file
   sprintf(acBuf, "%s\\%s\\SU_Inst.srt", acTmpPath, myCounty.acCntyCode);
   iRet = sortFile(pSecInst, acBuf, sSrtCmd2);
   if (iRet < 100)
      return -1;

   LogMsg("Open Tax installment file %s", acBuf);
   fdInst = fopen(acBuf, "r");
   if (fdInst == NULL)
   {
      LogMsg("***** Error opening Tax installment file: %s\n", acBuf);
      return -2;
   }  

   // Open Output file
   NameTaxCsvFile(acBaseFile, myCounty.acCntyCode, "Base");
   LogMsg("Open output file %s", acBaseFile);
   fdBase = fopen(acBaseFile, "a+");
   if (fdBase == NULL)
   {
      LogMsg("***** Error creating output file: %s\n", acBaseFile);
      return -4;
   }

   iApnMatch = 0;

   // Start loop 
   while (!feof(fdInst))
   {
      if (!(pTmp = fgets((char *)&acRec1[0], MAX_RECSIZE, fdInst)))
         break;
      if (!(pTmp = fgets((char *)&acRec2[0], MAX_RECSIZE, fdInst)))
         break;

      // Create new TAXBASE record
      iRet = Smx_ParseTaxSupp(acBuf, acRec1, acRec2);
      if (!iRet)
      {
         if (fdRoll)
            Smx_UpdateSuppAcct(acBuf);

         // Create delimited record
         Tax_CreateTaxBaseCsv(acRec1, (TAXBASE *)&acBuf);
         fputs(acRec1, fdBase);
         lOut++;
      } else if (iRet != 1)
         LogMsg("---> Load_TaxSupp: drop record %d [%.40s]", lCnt, acRec1); 

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdRoll)
      fclose(fdRoll);
   if (fdInst)
      fclose(fdInst);
   if (fdBase)
      fclose(fdBase);

   LogMsg("Total records processed:       %u", lCnt);
   LogMsg("Total output records:          %u", lOut);
   LogMsg("Total APN matched records:     %u", iApnMatch);
   printf("\nTotal output records: %u", lCnt);

   // Import into SQL
   if (bImport)
      iRet = doTaxImport(myCounty.acCntyCode, TAX_BASE);
   else
      iRet = 0;

   return iRet;
}

/**************************** Smx_Load_TaxDetail ******************************
 *
 * Input: SC - Secure Roll Data #3 - Districts.csv
 * Return 0 if success.
 *
 *****************************************************************************/

int Smx_ParseTaxDetail(char *pItems, char *pAgency, char *pInbuf)
{
   char     *apItems[32], acCode[32], *pTmp;
   int      iTems, iTmp;
   double   dTmp;

   TAXDETAIL  *pItemsRec  = (TAXDETAIL *)pItems;
   TAXAGENCY  *pAgencyRec = (TAXAGENCY *)pAgency;
   TAXAGENCY  *pResult;

   // Clear output buffer
   memset(pItems, 0, sizeof(TAXDETAIL));
   memset(pAgency, 0, sizeof(TAXAGENCY));

   iTems = ParseStringNQ(pInbuf, ',', 32, apItems);
   if (iTems < SRD_FLDS)
   {
      LogMsg("***** ParsePTS -->Bad tax record 1 (%d)", iTems);
      return -1;
   }

   // APN
   remChar(apItems[SRD_APN], '-');
   strcpy(pItemsRec->Apn, apItems[SRD_APN]);

   // Tax Year
   memcpy(pItemsRec->TaxYear, apItems[SRD_BILL_NO], 4);
   iTmp = atol(pItemsRec->TaxYear);
   if (iTmp != lTaxYear)
      return iTmp;

   // BillNum
   strcpy(pItemsRec->BillNum, apItems[SRD_BILL_NO]);

#ifdef _DEBUG
   //if (!memcmp(pItemsRec->Apn, "002011020", 9) )
   //   iTmp = 0;
#endif

   // Tax Desc
   if (*apItems[SRD_DIST_CODE] > ' ')
   {
      strcpy(pAgencyRec->Agency, apItems[SRD_DIST_NAME]);
      strcpy(pItemsRec->TaxDesc, apItems[SRD_DIST_NAME]);

      // Check for voter approved tax code
      if (*apItems[SRD_DIST_TYPE] == 'V')
      {
         strcpy(acCode, apItems[SRD_DIST_CODE]);
         iTmp = atol(acCode);
         pTmp = strchr(apItems[SRD_DIST_CODE], '_');
         sprintf(acCode, "%d_%s", iTmp, pTmp+4);
         pResult = findTaxAgency(acCode);
         if (!pResult)
         {
            strcpy(pItemsRec->TaxCode, acCode);
            strcpy(pAgencyRec->Code, acCode);
         }
      } else
      {
         pResult = findTaxAgency(apItems[SRD_DIST_CODE]);
         if (!pResult)
         {
            strcpy(pItemsRec->TaxCode, apItems[SRD_DIST_CODE]);
            strcpy(pAgencyRec->Code, apItems[SRD_DIST_CODE]);
         }
      }

      // Tax Desc
      if (pResult)
      {
         strcpy(pAgencyRec->Code, pResult->Code);
         strcpy(pAgencyRec->Agency, pResult->Agency);
         strcpy(pAgencyRec->Phone, pResult->Phone);
         strcpy(pAgencyRec->TaxRate, pResult->TaxRate);
         strcpy(pItemsRec->TaxDesc, pResult->Agency);
         strcpy(pItemsRec->TaxCode, pResult->Code);
         strcpy(pItemsRec->TaxRate, pResult->TaxRate);
         pAgencyRec->TC_Flag[0] = pResult->TC_Flag[0];
         pItemsRec->TC_Flag[0] = pResult->TC_Flag[0];
      } else
      {
         LogMsg("+++ Unknown Agency: %s - %s [%s]", apItems[SRD_DIST_CODE], apItems[SRD_DIST_NAME], apItems[SRD_APN]);
      }
   } 

   // Tax Rate


   // Tax Amount
   dTmp = atof(apItems[SRD_AMOUNT]);
   if (dTmp > 0.0)
   {
      strcpy(pItemsRec->TaxAmt, apItems[SRD_AMOUNT]);
      return 0;
   } else
      return 3;
}

int Smx_Load_TaxItems(bool bImport, int iSkipHdr)
{
   char     *pTmp, acItemsRec[MAX_RECSIZE], acAgencyRec[512], acRec[512];
   char     acItemsFile[_MAX_PATH], acAgencyFile[_MAX_PATH], acTmpFile[_MAX_PATH], acInFile[_MAX_PATH];

   int      iRet;
   long     lItems=0, lCnt=0, lAgency=0;
   FILE     *fdItems, *fdAgency, *fdIn;
   TAXDETAIL *pTax   = (TAXDETAIL *)&acItemsRec[0];

   LogMsg0("Loading Detail file");

   GetIniString(myCounty.acCntyCode, "SecRoll3", "", acInFile, _MAX_PATH, acIniFile);
   NameTaxCsvFile(acItemsFile, myCounty.acCntyCode, "Items");
   NameTaxCsvFile(acAgencyFile, myCounty.acCntyCode, "Agency");
   sprintf(acTmpFile, "%s\\%s\\%s_Agency.tmp", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);

   // Open input file
   LogMsg("Open Detail file %s", acInFile);
   fdIn = fopen(acInFile, "r");
   if (fdIn == NULL)
   {
      LogMsg("***** Error opening Detail file: %s\n", acInFile);
      return -2;
   }  

   // Open Items file
   LogMsg("Create Items file %s", acItemsFile);
   fdItems = fopen(acItemsFile, "w");
   if (fdItems == NULL)
   {
      LogMsg("***** Error creating Items file: %s\n", acItemsFile);
      return -4;
   }

   // Open Agency file
   LogMsg("Create Agency file %s", acTmpFile);
   fdAgency = fopen(acTmpFile, "w");
   if (fdAgency == NULL)
   {
      LogMsg("***** Error creating Agency file: %s\n", acTmpFile);
      return -4;
   }

   // Init variables

   // Skip header record
   for (iRet = 0; iRet < iSkipHdr; iRet++)
      pTmp = fgets((char *)&acRec[0], 512, fdIn);

   // Merge loop 
   while (!feof(fdIn))
   {
      pTmp = fgets((char *)&acRec[0], 512, fdIn);
      if (!pTmp)
         break;

      // Create Items & Agency record
      iRet = Smx_ParseTaxDetail(acItemsRec, acAgencyRec, acRec);
      if (!iRet)
      {
         // Create TaxBase record
         Tax_CreateDetailCsv(acRec, (TAXDETAIL *)&acItemsRec);
         lItems++;
         fputs(acRec, fdItems);

         // Create Agency record
         Tax_CreateAgencyCsv(acRec, (TAXAGENCY *)&acAgencyRec);
#ifdef _DEBUG
         //if (acRec[0] < '0')
         //   iRet = 0;
#endif
         fputs(acRec, fdAgency);
      } else if (iRet == 1)
      {
         LogMsg0("---> Drop detail record %d [%.80s]", lCnt, acRec); 
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdIn)
      fclose(fdIn);
   if (fdItems)
      fclose(fdItems);
   if (fdAgency)
      fclose(fdAgency);

   LogMsg("Total records processed:    %u", lCnt);
   LogMsg("Total Items records:        %u", lItems);

   // Import into SQL
   if (bImport)
   {
      iRet = doTaxImport(myCounty.acCntyCode, TAX_DETAIL);
      if (!iRet)
      {
         // Populate TotalRate - EXEC spUpdateTotalRate 'SMX'
         iRet = doUpdateTotalRate(myCounty.acCntyCode, "B");

         // Dedup Agency file before import
         LogMsg("Dedup Agency file %s to %s", acTmpFile, acAgencyFile);
         iRet = sortFile(acTmpFile, acAgencyFile, "S(#1,C,A,#2,C,A) DEL(124) DUPOUT F(TXT) OMIT(1,1,C,EQ,\"|\")");
         if (iRet > 0)
            iRet = doTaxImport(myCounty.acCntyCode, TAX_AGENCY);
      }
   } else
      iRet = 0;

   return iRet;
}

/**************************** Smx_Load_TaxDelq *******************************
 *
 * Load "SC - Redempt Roll Data File #1.csv"
 *
 * Return 0 if success.
 *
 *****************************************************************************/

int Smx_ParseTaxDelq(char *pOutbuf, char *pInbuf)
{
   char     *apItems[50], *pTmp;
   int      iTems;
   double   dDelqAmt;

   TAXDELQ  *pOutRec = (TAXDELQ *)pOutbuf;

#ifdef _DEBUG
   //if (!memcmp(pInRec->Apn, "0312200108", 10))
   //   iRet = 0;
#endif

   iTems = ParseStringNQ(pInbuf, cDelim, 50, apItems);
   if (iTems < RDF_FLDS)
   {
      LogMsg("***** Parse RDF -->Bad tax record (%d)", iTems);
      return -1;
   }

   // Reset buffer
   memset(pOutbuf, 0, sizeof(TAXDELQ));

   // APN
   remChar(apItems[RDF_APN], '-');
   strcpy(pOutRec->Apn, apItems[RDF_APN]);

   // Default Number
   strcpy(pOutRec->Default_No, apItems[RDF_DEFAULT_NO]);

   // Default date
   pTmp = dateConversion(apItems[RDF_DEFAULT_DATE], pOutRec->Def_Date, MM_DD_YYYY_1);
   if (!pTmp)
      LogMsg("*** Invalid default date: APN=%s, Date=%s", pOutRec->Apn, apItems[RDF_DEFAULT_DATE]);

   // Tax year
   strcpy(pOutRec->TaxYear, apItems[RDF_ROLLYR]);

   // Account type
   pOutRec->isSecd[0] = '1';
   pOutRec->isSupp[0] = '0';

   // Delq Amt
   dDelqAmt = atof(apItems[RDF_REDEMPTION_TOTAL]);

   // Paid Status
   if (*apItems[RDF_PAYMENT_PLAN_STATUS] == 'A')
      pOutRec->DelqStatus[0] = TAX_STAT_ONPAYMENT;
   else if (*apItems[RDF_REDEMPTION_STATUS] == 'A')
      pOutRec->DelqStatus[0] = TAX_STAT_UNPAID;
   else if (*apItems[RDF_REDEMPTION_STATUS] == 'R')
      pOutRec->DelqStatus[0] = TAX_STAT_REDEEMED;
   else if (*apItems[RDF_REDEMPTION_STATUS] == 'C')
      pOutRec->DelqStatus[0] = TAX_STAT_CANCEL;
   else if (*apItems[RDF_REDEMPTION_STATUS] == 'S')
      pOutRec->DelqStatus[0] = TAX_STAT_SOLD2AUCTION;
   else
      LogMsg("Invalid paid status %s [%s]", apItems[RDF_REDEMPTION_STATUS], apItems[RDF_APN]);

   // Is delinq?
   if (dDelqAmt > 0.0)
   {
      sprintf(pOutRec->Def_Amt, "%.2f", dDelqAmt);
      if (*apItems[RDF_REDEMPTION_STATUS] == 'A')
         pOutRec->isDelq[0] = '1';
      else
         pOutRec->isDelq[0] = '1';

      return 0;
   } else
      return 1;
}

int Smx_Load_TaxDelq(bool bImport, int iSkipHdr)
{
   char     *pTmp, acBuf[MAX_RECSIZE], acRec[MAX_RECSIZE], acTmp[MAX_RECSIZE];
   char     acTmpFile[_MAX_PATH], acOutFile[_MAX_PATH], acInFile[_MAX_PATH];

   int      iRet;
   long     lOut=0, lCnt=0;
   FILE     *fdOut, *fdIn;
   TAXDELQ  *pOutRec = (TAXDELQ *)acBuf;

   LogMsg0("Loading Redemption file");

   sprintf(acTmpFile, "%s\\%s\\%s_Delq.Tmp", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   NameTaxCsvFile(acOutFile, myCounty.acCntyCode, "Delq");

   GetIniString(myCounty.acCntyCode, "RdmRoll1", "", acInFile, _MAX_PATH, acIniFile);
   lLastFileDate = getFileDate(acInFile);

   // Open input file
   LogMsg("Open delinquent tax file %s", acInFile);
   fdIn = fopen(acInFile, "r");
   if (fdIn == NULL)
   {
      LogMsg("***** Error opening delinquent tax file: %s\n", acInFile);
      return -2;
   }  

   // Open Output file
   LogMsg("Open output file %s", acTmpFile);
   fdOut = fopen(acTmpFile, "w");
   if (fdOut == NULL)
   {
      LogMsg("***** Error creating output file: %s\n", acTmpFile);
      return -4;
   }

   // Skip header record
   for (iRet = 0; iRet < iSkipHdr; iRet++)
      pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdIn);

   // Merge loop    
   iRet = 0;
   acBuf[0] = 0;
   while (!feof(fdIn))
   {
      if (!(pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdIn)))
         break;

#ifdef _DEBUG      
      //if (!memcmp(acRec, "00", 2) && !memcmp(&acRec[16], "0151000528", 10))
      //{
      //   iRecType = 0;
      //}
#endif

      iRet = Smx_ParseTaxDelq(acBuf, acRec);
      if (!iRet)
      {
         //if (pOutRec->DelqStatus[0] != TAX_STAT_REDEEMED && pOutRec->DelqStatus[0] != TAX_STAT_CANCEL)
         //   pOutRec->isDelq[0] = '1';

         sprintf(pOutRec->Upd_Date, "%d", lLastFileDate);

         // Output last record
         Tax_CreateDelqCsv(acTmp, (TAXDELQ *)pOutRec);

         // Output record			
         fputs(acTmp, fdOut);
         lOut++;
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdIn)
      fclose(fdIn);
   if (fdOut)
      fclose(fdOut);

   // Sort output to dedup entry
   iRet = sortFile(acTmpFile, acOutFile, "S(#1,C,A,#3,C,A,#5,C,A) DUPOUT DEL(124)");

   LogMsg("Total records processed:          %u", lCnt);
   LogMsg("Total delinquent records output:  %u", lOut);

   printf("\nTotal output records: %u", lCnt);

   // Import into SQL
   if (bImport && lOut > 0)
   {
      iRet = doTaxImport(myCounty.acCntyCode, TAX_DELINQUENT);
   } else
      iRet = 0;

   return iRet;
}

/*********************************** loadSmx ********************************
 *
 * Input files:
 *    - SECMSTR.TXT              (roll file, 810 bytes ASCII)
 *    - RESIDENT.TXT             (char for SFR 330-byte ASCII)
 *    - APARTMENT.TXT            (char for APT 360-byte ASCII)
 *    - SALEHIST.TXT             (sale file, 65-byte ASCII)
 *    - VACANT.TXT               (Vacant Land 60-bytes ASCII)
 *
 * Following are tasks to be done here:
 *    - Normal update: LoadOne -U [-Lc] -Xsi [-T]
 *    - Load Lien: LoadOne -L [-Lc] -Xsi
 *    - Use -Lc when there is new char files
 *
 ****************************************************************************/

int loadSmx(int iSkip)
{
   char  acTmpFile[_MAX_PATH];
   int   iRet=0;

   iApnLen = SMX_SIZ_APN;

   // Load tax - input file need sort dedup
   //if (iLoadTax == TAX_LOADING)                    // -T or -Lt
   //{
   //   TC_SetDateFmt(MM_DD_YYYY_1, true);
   //   iRet = TC_LoadTax(myCounty.acCntyCode, bTaxImport);
   //}

   if (iLoadTax == TAX_LOADING)                    // -T or -Lt
   {
      char  acSecRoll[_MAX_PATH], acSecRollA[_MAX_PATH], acSecRollB[_MAX_PATH];

      iRet = GetIniString(myCounty.acCntyCode, "TaxDist", "", acTmpFile, _MAX_PATH, acIniFile);
      if (iRet > 0)
         iRet = LoadTaxCodeTable(acTmpFile);

      GetIniString(myCounty.acCntyCode, "SecRoll1",  "", acSecRoll, _MAX_PATH, acIniFile);
      GetIniString(myCounty.acCntyCode, "SecRoll2A", "", acSecRollA, _MAX_PATH, acIniFile);
      lLastTaxFileDate = getFileDate(acSecRollA);

      // Only process if new tax file
      iRet = isNewTaxFile(acSecRollA, myCounty.acCntyCode);
      if (iRet <= 0)
      {
         lLastTaxFileDate = 0;
         return iRet;
      }

      // Load tax base
      iRet = Smx_Load_TaxBase(acSecRoll, acSecRollA, false);
      if (!iRet)
      {
         // Load Secured Transfer - tax portion to be transfered to new owner.
         //iRet = Smx_Load_TaxBase(NULL, acSecRollB, bTaxImport);

         // Load tax supplement file
         GetIniString(myCounty.acCntyCode, "SupRoll1", "", acSecRollA, _MAX_PATH, acIniFile);
         GetIniString(myCounty.acCntyCode, "SupRoll2", "", acSecRollB, _MAX_PATH, acIniFile);
         iRet = Smx_Load_TaxSupp(acSecRollA, acSecRollB, bTaxImport);
         if (iRet < 0 && bTaxImport)
            iRet = doTaxImport(myCounty.acCntyCode, TAX_BASE);

         // Load tax detail
         iRet = Smx_Load_TaxItems(bTaxImport, 1);

         // Load redemption file
         iRet = Smx_Load_TaxDelq(bTaxImport, 1);

         // Update Delq flag in Tax_Base
         if (!iRet)
            iRet = updateDelqFlag(myCounty.acCntyCode);
      }
   }

   // Exit if load/update tax only
   if (!iLoadFlag)
      return iRet;

   // Sale extract
   if (iLoadFlag & EXTR_SALE)                      // -Xs
   {
      LogMsg0("Extract %s sale history file", myCounty.acCntyCode);
      if (strstr(acSaleFile, "csv"))
         iRet = Smx_ExtrSaleCsv();
      else
         iRet = Smx_ExtrSale();
      if (iRet)         
         return iRet;

      iLoadFlag |= MERG_CSAL;                   // Signal merge sale to R01
   }

   // Sort vacant lot file
   //if (!_access(acVacFile, 0))
   //{
   //   sprintf(acTmpFile, "%s\\%s", acTmpPath, myCounty.acCntyCode);
   //   if (_access(acTmpFile, 0))
   //      _mkdir(acTmpFile);
   //   sprintf(acTmpFile, "%s\\%s\\Vacant.dat", acTmpPath, myCounty.acCntyCode);
   //   iRet = sortFile(acVacFile, acTmpFile, "S(1,9,C,A)");
   //   if (iRet > 0)
   //   {
   //      strcpy(acVacFile, acTmpFile);
   //      iRet = 0;
   //   } else
   //      LogMsg("***** Error sorting vacant file %s", acVacFile);
   //}

   // Create CHAR file
   if (iLoadFlag & EXTR_ATTR)                      // -Xa
   {
      LogMsg0("Extract %s Char file", myCounty.acCntyCode);
      if (strstr(acCharFile, "csv"))
         iRet = Smx_ConvStdChar2(acCharFile);
      else
         iRet = Smx_ConvertChar(acCChrFile);
      if (iRet <= 0)
      {
         LogMsg("***** Error converting Char file to %s", acCChrFile);
         return -1;
      }
   }

   if (iLoadFlag & EXTR_LIEN)                      // -Xl
   {
      LogMsg0("Extract %s Lien file", myCounty.acCntyCode);
      if (strstr(acRollFile, "csv"))
         iRet = Smx_ExtrLienCsv();
      else
         iRet = Smx_ExtrLien();
   }

   if (iLoadFlag & LOAD_LIEN)                      // -L
   {
      if (strstr(acRollFile, "csv"))
         iRet = Smx_Load_LDRCsv(iSkip);
      else
         iRet = Smx_Load_LDR(iSkip);
   } else if (iLoadFlag & LOAD_UPDT)               // -U
   {
      if (strstr(acRollFile, "csv"))
         iRet = Smx_Load_RollCsv(iSkip);
      else
         iRet = Smx_Load_Roll(iSkip);
   }

   if (!iRet && (iLoadFlag & MERG_CSAL) )          // -Ms
   {
      // Apply Scr_Sale.sls to R01 file
      iRet = ApplyCumSale(iSkip, acCSalFile, false, SALE_USE_SCUPDXFR);
   }

   return iRet;
}