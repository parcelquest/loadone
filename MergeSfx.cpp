/******************************************************************************
 *
 * Input files:
 *    - AIASECD               (roll file, 391-byte ebcdic)
 *    - AIAOWNR               (owner file, 85-byte ebcdic)
 *    - AIAMADR               (mailaddr file, 172-byte ebcdic)
 *
 * Following are tasks to be done here:
 *    - Convert ebcdic to ascii
 *    - Normal update: LoadOne -U -CSFX
 *    - Load Lien: LoadOne -L -CSFX -Xl
 *
 * Programming Notes:
 *    - This county needs to be run in 2 passes.  1st pass is to load roll file
 *      and update roll with owner, update sale_cum with docnum from owner and
 *      sale price/date from roll.  Sort sale_com (APN,A,SALEDATE,D) and dedup.
 *      2nd pass is to update roll with sale_cum data.
 *    - It may be better updating roll and sale at the same time.  Review!!!
 *
 * Revision:
 * 20051212             Copy from MergeSac.cpp
 * 20061130 1.3.7       Fix "Missing addr line2" in Sfx_MergeMAdr() and "Situs street
 *                      Name" bug in Sfx_MergeSAdr().
 * 03/20/2008 1.5.7     Use standard function to update usecode
 * 03/24/2008 1.5.7.2   Format IMAPLINK.
 * 03/28/2008 1.5.8     Fix bug that copies mail city with embedded null in Sfx_MergeMAdr().
 * 08/08/2008 8.1.1     Rename CreateSfxRoll() to Sfx_Load_LDR().  Add Sfx_ExtrLien() and lien extract option
 * 01/17/2009 8.5.6     Fix TRA
 * 03/23/2009 8.7.1.1   Format STORIES as 99.9.  We no longer multiply by 10.
 * 08/05/2009 9.1.5     Populating other values to R01 record.
 * 09/01/2009 9.2.0     Remove output of new/old APN files.  We never use this data anyway.
 * 02/23/2010 9.4.3     Fix CARE_OF issue.
 * 05/05/2010 9.5.7     Modify Sfx_MatchOwner() to fix bad owner record.  Write Sfx_ConvertSale()
 *                      to convert old cum sale to new format.  Remove Sfx_MergeSale().
 *                      Rename MergeSfxRoll() to Sfx_Load_Roll().
 * 08/06/2010 10.1.9    Modify Sfx_Load_LDR() to change cum sale sort command.  Also change
 *                      option in ApplyCumSale to use SALE_USE_SCUPDXFR.
 * 08/12/2011 11.1.8    Add option to clean up history sales.  Add S_HSENO.
 * 03/09/2012 11.6.9    Only translate EBCDIC to ASCII if input roll file is EBCDIC.
 * 05/09/2012 11.9.4    Modify Sfx_MergeSAdr() to support new roll layout in AIASECD.
 *                      Rename old function to Sfx_MergeSAdr_Pre201205(). Use version 11.9.2 to process old file.
 * 05/09/2012 11.9.4.1  Revert back to old format since county has revert back.
 * 05/18/2012 11.9.7    Modify Sfx_MergeMAdr() to force StrSfx "LA" to "LN" avoiding change to 
 *                      Suffix_xlat.txt which causes problem in other places.
 * 07/19/2012 12.1.2    Modify Sfx_MergeRoll() to ignore record with known bad APN.
 * 08/15/2012 12.2.5    Call ApplyCumSale() with option to ignore DOCNUM if missing.
 *                      Change sort order of cum sale file to use last DOCNUM with same DOCDATE.
 * 03/05/2013 12.4.7    Fix bug in Sfx_MatchOwner() when record is broken into multiple lines.
 *                      Also ignore DOCNUM if RECDATE in owner file is not the same as 
 *                      SALEDATE in roll file.
 * 05/25/2013 12.6.4    Reformat DocNum to match with county Recorder and make it linkable to DataTree.
 * 08/01/2013 13.3.10   Change has been made to parse Situs. County staff has nothing to do
 *                      but keep changing data format.
 * 09/11/2013 13.7.14   Add Basement and BldgClass.
 * 09/24/2013 13.8.3    Fix bug in Sfx_ExtrLien() & Sfx_CreateLienRec().
 * 10/10/2013 13.10.4.3 Use updateVesting() to update Vesting and Etal flag. Remove CONS_TYPE.
 * 11/28/2013 13.11.10  Populate S_HSENO for house number without hyphen in Sfx_MergeSAdr()
 * 01/15/2014 13.11.13  Add Sfx_MergeDocExt() to populate GRGR from the web.
 * 01/16/2014 13.11.14  Modify Sfx_MergeDocRec() to remove trailing DocNum extension.
 * 05/08/2014 13.14.4   Rename Sfx_MergeOwner() to Sfx_MergeOwnerX() then rewrite Sfx_MergeOwner()
 *                      to populate owner same as county provided /w some clean up.
 * 08/13/2014 14.3.1    Fix bad char in Sfx_MergeMAdr().
 * 08/19/2015 15.1.1    Modify Sfx_MergeDocRec() to update TRANSFER regardless of DocType.
 * 08/28/2015 15.1.3    Add -Mr option to add lot sqft & lot area
 * 09/13/2015 15.2.2    Standardize MergeArea.
 * 11/12/2015 15.3.3    Modify Sfx_MergeSAdr() to fix some bad address.
 * 11/13/2015 15.3.3.1  Fix bug in Sfx_MergeSAdr()
 * 11/19/2015 15.3.5    Remove all situs with street name "V".  Also remove all StrSub='V'
 *                      This is indication of vacant parcel.  Also, clean up some special cases for SFX
 * 01/04/2016 15.4.0    Add option to load TC file by calling Load_TC()
 * 02/17/2016 15.6.0    Fix swap name in Sfx_MergeOwner(), don't swap if name includes digit
*                       or has Vesting.  Remove comma in street name in Sfx_MergeSAdr().
 * 03/26/2016 15.8.1    Move dispError() to Logs.cpp
 * 10/18/2017 17.3.3    Fix Sfx_MergeOwner() to avoid exception error when processing name (0024 024).
 * 05/22/2018 17.11.0   Add -Xs option to allow extract Attom sale file.  Now allow application
 *                      to process both sale from roll file (use -U only) and Attom file separately (use -Xs).
 * 08/09/2018 18.3.0    Add -Mn to merge sale from NDC.
 * 08/14/2018 18.4.0    Replace R01.MergeArea() with PQ_MergeLotArea() using new SFX_Basemap.txt
 * 09/29/2018 18.5.1    Move -Mr option to LoadOne.cpp
 * 03/06/2019 18.9.9    Use SCSAL_EXT for sale extract from roll update file.  Increase
 *                      dup buffer to 2048 to avoid sort error.
 * 03/07/2019 18.9.10   Modify Sfx_Load_Roll() to omit sale record without DocNum.
 * 08/01/2019 19.0.6    Increase sort buffer in Sfx_Load_LDR()
 * 03/02/2020 19.7.2    Fix bug in Sfx_Load_Roll()
 * 04/02/2020 19.8.7    Remove -Xs and replace it with -Xn option to format and merge NDC recorder data.
 * 04/27/2020 19.9.0    Remove -Ut and use -T to load both full & partial tax file with TC_LoadTax().
 * 08/03/2020 20.2.6    Modify Sfx_CreateLienRec() & Sfx_MergeRoll() to add EXE_CODE
 * 10/20/2020 20.3.4    Modify Sfx_CreateLienRec() & Sfx_MergeRoll() to fix 32-bit issue of TotalValue & TotalExe.
 * 10/30/2020 20.4.1    Modify Sfx_MergeRoll() to populate PQZoning.
 * 03/24/2021 20.7.11   Add functions to process new county tax files.  Detail file currently only include
 *                      special assessments, not based county tax & tax rate.
 * 05/05/2021 20.7.17   Modify Sfx_MergeRoll() to clean up BldgClass.
 * 05/26/2021 20.8.1.1  Modify Sfx_Load_TaxItems() to skip loading detail if file not exist.
 * 07/21/2021 21.1.0    Fix problem of value > 32-bit int in Sfx_MergeRoll().
 * 08/19/2021 21.1.5    Modify Sfx_ParseTaxSupp() to load tax data after LDR.
 * 10/18/2021 21.2.8    Fix DueDate2 on Sfx_ParseTaxBase() when validate of future date (next year).
 * 11/04/2021 21.2.12   Modify Sfx_Load_TaxSupp() to ignore file date check.
 * 03/16/2022 21.8.1.1  Temporary fix UNITS % ROOMS in Sfx_MergeRoll() due to bad data.
 * 05/31/2022 21.9.3    Modify sale update to allow combination of both county sales (new update) and 
 *                      ATTOM sales (old sales).  Run with both -Mn & -Ms until we have a source that has
 *                      a complete set of sales.  Currently ATTOM is 6 mos behind and county has only last sale/transfer.
 * 07/20/2022 22.0.2    Change '\\' to '/' in Sfx_MergeOwner().
 * 10/19/2022 22.2.8    Modify Sfx_ParseTaxSupp() to ignore a bill if it is from prior year and it has been paid.
 * 09/08/2023 23.1.6    Remove -Mg to avoid confusion since we no longer have GrGr data.
 * 09/22/2023 23.2.1    Modify Sfx_MatchOwner() to take DocNum > ' '.  
 * 01/11/2024 23.5.5    Fix M_UNITNOX in Sfx_MergeMAdr(), Sfx_MergeSAdr(), Sfx_MergeSAdr2().
 * 02/01/2024 23.6.0    Use ApplyCumSaleNR() to update NDC sale instead of ApplyCumSale().
 * 05/22/2024 23.8.4    Modify loadSfx() to reverse the sale update process to update county (SFX)
 *                      sale first then NDC sale. This allows latest sale to show up in BULK extract.
 *            23.8.4.1  Reverse sale update back to previous order since problem is now fixed in ApplySCSalRec_ChkXfer().
 * 05/28/2024 23.8.7    Fix StrNum of multi unit in Sfx_MergeSAdr().
 * 07/12/2024 24.0.2    Modify Sfx_MergeRoll() to add ExeType.
 * 08/24/2024 24.1.3    Modify Sfx_MergeMAdr() to extend M_CITY.
 * 11/16/2024 24.3.4    Modify Sfx_MergeSAdr() to keep post dir in street name and to fix situs UNITNO.
 *
 ******************************************************************************/

#include "stdafx.h"
#include "CountyInfo.h"
#include "hlAdo.h"
#include "Prodlib.h"
#include "R01.h"
#include "RecDef.h"
#include "Tables.h"
#include "Logs.h"
#include "LoadDef.h"
#include "Utils.h"
#include "XlatTbls.h"
#include "doSort.h"
#include "doOwner.h"
#include "formatApn.h"
#include "SaleRec.h"
#include "LoadOne.h"
#include "UseCode.h"
#include "LOExtrn.h"
#include "PQ.h"
#include "MergeSfx.h"
#include "Tax.h"
#include "NdcExtr.h"

static XLAT_CODE  asConstType[] =
{
   // Value, lookup code, value length
   "A",  "S", 1,               // Structural steel, fireproof
   "B",  "B", 1,               // Concrete
   "C",  "C", 1,               // Masonry or concrete
   "D",  "Z", 1,               // Wood frame
   "F",  " ", 1,               // Unknown
   "STE","S", 3,               // Structural steel, fireproof
   "S",  "M", 1,               // Special
   "WOO","Z", 3,               // Wood frame
   "",   "",  0
};

static LU_ENTRY tblAttr[MAX_ATTR_ENTRIES];
static FILE     *fdOwner, *fdMAdr, *fdNewSale;
static long     lOwnerSkip, lMAdrSkip, lSaleSkip;
static long     lOwnerMatch, lMAdrMatch, lSaleMatch;
static int      iOwnerLen, iBadSitus;
static char     acOwnerFile[_MAX_PATH], acAdrFile[_MAX_PATH];

static SCSAL_EXT sCurSale;

IDX_TBL5 SFX_DocType[] =
{
   "DEED  ",                     "1 ", 'N', 6, 2,
   "TAX DEED",                   "67", 'N', 8,  2,
   //"REQUEST NOTICE DEFAULT",     "52", 'Y', 18, 2,
   //"NOTICE OF DEFAULT",          "52", 'Y', 14, 2,
   //"MECHANICS LIEN",             "28", 'Y', 12, 2,
   //"TRUSTEE DEED",               "27", 'Y', 12, 2,
   //"TAX LIEN - NOTICE OF",       "28", 'Y', 8,  2,
   //"FEDERAL TAX LIEN-NOTICE OF", "28", 'Y', 16, 2,
   "","",0,0,0
};

/********************************* MergeDocRec *******************************
 *
 * 
 *****************************************************************************/

int Sfx_MergeDocRec(char *pGrGrRec, char *pOutbuf)
{
   long  lCurSaleDt, lLstSaleDt;
   int   iTmp, iDocIdx;
   char  *pTmp;

   GRGR_DOC *pSaleRec = (GRGR_DOC *)pGrGrRec;

   lCurSaleDt = atoin(pSaleRec->DocDate, SIZ_SALE1_DT);
   lLstSaleDt = atoin(pOutbuf+OFF_TRANSFER_DT, SIZ_TRANSFER_DT);

   // Update transfers
   if (lCurSaleDt > lLstSaleDt)
   {
      memcpy(pOutbuf+OFF_TRANSFER_DOC, pSaleRec->DocNum, 7);
      memcpy(pOutbuf+OFF_TRANSFER_DT, pSaleRec->DocDate, SIZ_TRANSFER_DT);
   }

   // Use new record only
   lLstSaleDt = atoin(pOutbuf+OFF_SALE1_DT, SIZ_SALE1_DT);
   if (lCurSaleDt < lLienDate)
      return -1;

#ifdef _DEBUG
   if (!memcmp(pOutbuf, "5687 019", 8))
      iTmp = 0;
#endif

   // Only take documents with predefined DocType
   pSaleRec->DocTitle[SIZ_GD_TITLE-1] = 0;
   iDocIdx = findDocType(pSaleRec->DocTitle, (IDX_TBL5 *)&SFX_DocType[0]);
   if (iDocIdx < 0)
      return 0;

   if (lCurSaleDt == lLstSaleDt)
   {
      if (*(pOutbuf+OFF_SALE1_DOC) == ' ' && pSaleRec->DocNum[0] > ' ')
         memcpy(pOutbuf+OFF_SALE1_DOC, pSaleRec->DocNum, 7);
      if (*(pOutbuf+OFF_SALE1_DOCTYPE) == ' ' && iDocIdx < 3)
         memcpy(pOutbuf+OFF_SALE1_DOCTYPE, SFX_DocType[iDocIdx].pCode, SFX_DocType[iDocIdx].iCodeLen);

      if (*pSaleRec->Grantor[0] >= 'A' && memcmp(pOutbuf+OFF_SELLER, pSaleRec->Grantor[0], 10))
      {
         *pSaleRec->Grantor[1] = 0;
         if (pTmp = strchr(pSaleRec->Grantor[0], '('))
            *pTmp = '\0';
         iTmp = strlen(pSaleRec->Grantor[0]);
         if (iTmp > SIZ_SELLER)
            iTmp = SIZ_SELLER;
         memset(pOutbuf+OFF_SELLER, ' ', SIZ_SELLER);
         memcpy(pOutbuf+OFF_SELLER, pSaleRec->Grantor[0], iTmp);

         *(pOutbuf+OFF_AR_CODE1) = 'W';
      }
      return 1;
   } else if (*(pOutbuf+OFF_SALE1_DOC) > ' ')
   {
      // Move sale2 to sale3
      memcpy(pOutbuf+OFF_SALE3_DOC, pOutbuf+OFF_SALE2_DOC, SIZ_SALE3_DOC);
      memcpy(pOutbuf+OFF_SALE3_DT, pOutbuf+OFF_SALE2_DT, SIZ_SALE3_DT);
      memcpy(pOutbuf+OFF_SALE3_DOCTYPE, pOutbuf+OFF_SALE2_DOCTYPE, SIZ_SALE3_DOCTYPE);
      memcpy(pOutbuf+OFF_SALE3_AMT, pOutbuf+OFF_SALE2_AMT, SALE_SIZ_SALEPRICE);
      memcpy(pOutbuf+OFF_SALE3_CODE, pOutbuf+OFF_SALE2_CODE, SALE_SIZ_SALECODE);
      *(pOutbuf+OFF_AR_CODE3) = *(pOutbuf+OFF_AR_CODE2);

      // Move sale1 to sale2
      memcpy(pOutbuf+OFF_SALE2_DOC, pOutbuf+OFF_SALE1_DOC, SIZ_SALE3_DOC);
      memcpy(pOutbuf+OFF_SALE2_DT, pOutbuf+OFF_SALE1_DT, SIZ_SALE3_DT);
      memcpy(pOutbuf+OFF_SALE2_DOCTYPE, pOutbuf+OFF_SALE1_DOCTYPE, SIZ_SALE3_DOCTYPE);
      memcpy(pOutbuf+OFF_SALE2_AMT, pOutbuf+OFF_SALE1_AMT, SALE_SIZ_SALEPRICE);
      memcpy(pOutbuf+OFF_SALE2_CODE, pOutbuf+OFF_SALE1_CODE, SALE_SIZ_SALECODE);
      *(pOutbuf+OFF_AR_CODE2) = *(pOutbuf+OFF_AR_CODE1);
   }

   // Update current sale
   memcpy(pOutbuf+OFF_SALE1_DOC, pSaleRec->DocNum, 7);
   memcpy(pOutbuf+OFF_SALE1_DT, pSaleRec->DocDate, SIZ_SALE3_DT);
   memcpy(pOutbuf+OFF_SALE1_DOCTYPE, SFX_DocType[iDocIdx].pCode, SFX_DocType[iDocIdx].iCodeLen);
   memset(pOutbuf+OFF_SALE1_AMT, ' ', SALE_SIZ_SALEPRICE);
   memset(pOutbuf+OFF_SALE1_CODE, ' ', SALE_SIZ_SALECODE);

   *pSaleRec->Grantor[1] = 0;
   if (pTmp = strchr(pSaleRec->Grantor[0], '('))
      *pTmp = '\0';
   iTmp = strlen(pSaleRec->Grantor[0]);
   if (iTmp > SIZ_SELLER)
      iTmp = SIZ_SELLER;
   memset(pOutbuf+OFF_SELLER, ' ', SIZ_SELLER);
   memcpy(pOutbuf+OFF_SELLER, pSaleRec->Grantor[0], iTmp);

   *(pOutbuf+OFF_AR_CODE1) = 'W';

   return 1;
}

/*****************************************************************************
 *
 * Format searchable APN.  Assuming APN is always trailed by space.
 *
 *****************************************************************************/

void Sfx_FmtApn(char *pApn)
{
   if (*(pApn+4) == '-')
      *(pApn+4) = ' ';
   else
   {
      memcpy(pApn+5, pApn+6, 4);
      *(pApn+9) = ' ';
   }
}

/******************************** MergeDocExt ********************************
 *
 * Merge data grab from website to PQ4 product w/o sale price.
 *
 *****************************************************************************/

int Sfx_MergeDocExt(char *pCnty, char *pDocFile)
{
   char     *pTmp, acBuf[MAX_RECSIZE], acTmp[64];
   char     cFileCnt=1;
   char     acRawFile[_MAX_PATH], acOutFile[_MAX_PATH];

   HANDLE   fhIn, fhOut;
   GRGR_DOC SaleRec;
   FILE     *fdDoc;

   int      iTmp, iSaleUpd=0;
   DWORD    nBytesRead;
   DWORD    nBytesWritten;
   BOOL     bRet, bEof, bRename=true;
   long     lRet=0, lCnt=0;

   memset(&SaleRec, ' ', sizeof(GRGR_DOC));

   sprintf(acRawFile, acRawTmpl, pCnty, pCnty, "R01");
   if (_access(acRawFile, 0))
   {
      sprintf(acRawFile, acRawTmpl, pCnty, pCnty, "S01");
      bRename = false;
   }

   sprintf(acOutFile, acRawTmpl, pCnty, pCnty, "N01");
   if (!iGrGrApnLen)
      iGrGrApnLen = myCounty.iApnLen;

   // Open GrGr file
   LogMsg("Merge Recorder data %s to R01 file", pDocFile);
   fdDoc = fopen(pDocFile, "r");
   if (fdDoc == NULL)
   {
      LogMsg("***** Error opening recorder extract file: %s\n", pDocFile);
      return -2;
   }

   // Get first record
   pTmp = fgets((char *)&SaleRec, sizeof(GRGR_DOC), fdDoc);
   if (!pTmp || feof(fdDoc))
   {
      LogMsg("*** No recorder data available");
      return 0;
   }
   Sfx_FmtApn(SaleRec.APN);

   // Open Input file
   LogMsg("Open input file %s", acRawFile);
   fhIn = CreateFile(acRawFile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhIn == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening input file to merge GrGr: %s\n", acRawFile);
      return -3;
   }

   // Open Output file
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening output file to merge GrGr: %s\n", acOutFile);
      return -4;
   }

   // Copy skip record
   ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);
   bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);

   iNoMatch=0;
   bEof = false;

   // Merge loop
   while (!bEof)
   {
      bRet = ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);

      // Check for EOF
      if (!bRet)
      {
         LogMsg("***** Error reading input file %s (%f)", acRawFile, GetLastError());
         break;
      }

      if (!nBytesRead)
         break;

      // Update sale
GrGr_ReLoad:
      iTmp = memcmp(acBuf, SaleRec.APN, iGrGrApnLen);
      if (!iTmp)
      {
#ifdef _DEBUG
         //if (!memcmp(acBuf, "014118611", 9))
         //   iTmp = 0;
#endif

         // Merge sale data
         iTmp = Sfx_MergeDocRec((char *)&SaleRec, acBuf);
         if (iTmp > 0)
         {
            iSaleUpd++;

            // Get last recording date
            iTmp = atoin((char *)&acBuf[OFF_TRANSFER_DT], SIZ_TRANSFER_DT);
            if (iTmp >= lToday)
               LogMsg0("*** WARNING: Bad last recording date %d at record# %d -->%.10s", iTmp, lCnt, acBuf);
            else if (lLastRecDate < iTmp)
               lLastRecDate = iTmp;
         }
         // Read next sale record
         pTmp = fgets((char *)&SaleRec, sizeof(GRGR_DOC), fdDoc);
         if (!pTmp)
            bEof = false;    // Signal to stop sale update
         else
         {
            Sfx_FmtApn(SaleRec.APN);
            goto GrGr_ReLoad;
         }
      } else
      {
         if (iTmp > 0)        // Sale not match, advance to next sale record
         {
            if (!memcmp(acBuf, "99999999", 8))
            {
               nBytesRead = 0;
               continue;
            } else
            {
               iNoMatch++;
               if (bDebug)
                  LogMsg0("*** Sale not match : %.12s > %.12s (%d) ", acBuf, SaleRec.APN, lCnt);
               pTmp = fgets((char *)&SaleRec, sizeof(GRGR_DOC), fdDoc);
               if (!pTmp)
                  bEof = true;    // Signal to stop sale update
               else
               {
                  Sfx_FmtApn(SaleRec.APN);
                  goto GrGr_ReLoad;
               }
            }
         }
      }

      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);

      if (!bRet)
      {
         LogMsg("***** Error occurs: %d\n", GetLastError());
         lRet = -99;
         break;
      }
   }

   // Do the rest of the file
   while (nBytesRead>0 && ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL))
   {
      if (!nBytesRead)
         break;

      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
      lCnt++;
   }


   // Close files
   if (fdDoc)
      fclose(fdDoc);
   if (fhOut)
      CloseHandle(fhOut);
   if (fhIn)
   {
      CloseHandle(fhIn);

      // Rename outfile
      if (bRename)
      {
         sprintf(acTmp, "M0%c", cFileCnt|0x30);
         sprintf(acBuf, acRawTmpl, pCnty, pCnty, acTmp);
         if (!_access(acBuf, 0))
            remove(acBuf);
         iTmp = rename(acRawFile, acBuf);
      } else
      {
         sprintf(acTmp, "R0%c", cFileCnt|0x30);
         sprintf(acRawFile, acRawTmpl, pCnty, pCnty, acTmp);
      }
      LogMsg("Rename output file %s to %s", acOutFile, acRawFile);
      iTmp = rename(acOutFile, acRawFile);
   }

   // Update output record count
   if (lCnt > 1000)
      lRecCnt = lCnt;

   LogMsg("Total output records:       %u", lCnt);
   LogMsg("Total GrGr records updated: %u", iSaleUpd);
   LogMsg("Total GrGr records skipped: %u", iNoMatch);
   return 0;
}

/********************************** formDate *********************************
 *
 * Reformat sale/transfer date from YYMMDD to YYYYMMDD.  This function does
 * no verification.  Caller has to verify data before calling.
 *
 *****************************************************************************/

static char *formDate(char *pInDate, char *pOutDate)
{
   if (memcmp(pInDate, (char *)&acToday[2], 2) > 0)
      sprintf(pOutDate, "19%.6s", pInDate);
   else
      sprintf(pOutDate, "20%.6s", pInDate);

   return pOutDate;
}

static long lformDate(char *pInDate, char *pOutDate)
{
   long lRet;

   lRet = atoin(pInDate, 6);
   if (lRet > 0)
   {
      if (memcmp(pInDate, (char *)&acToday[2], 2) > 0)
         sprintf(pOutDate, "19%.6s", pInDate);
      else
         sprintf(pOutDate, "20%.6s", pInDate);
   } else
      strcpy(pOutDate, "        ");
   return lRet;
}

/******************************** Sfx_MergeOwner *****************************
 *
 * Due to free form name in SFX, we keep Name1 as is and try to parse swapname
 * as best as we can.
 * Return 0 if successful, >0 if error
 *
 *****************************************************************************/

void Sfx_MergeOwner(char *pOutbuf, char *pNames)
{
   int   iTmp, iRet;
   char  acTmp1[128], acTmp[128], acSave1[64], *pTmp, *pTmp1;
   char  acName1[64], acOwner[64];
   OWNER myOwner;

   // Clear output buffer if needed
   removeNames(pOutbuf, false);
   memset(acTmp, 0, 128);

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "0078 072", 8))
   //   iTmp = 0;
#endif

   // Remove multiple spaces
   pTmp = strcpy(acTmp1, pNames);
   iTmp = 0;
   while (*pTmp)
   {
      // skip unwanted chars at beginning of name
      if (*pTmp < '1' && iTmp == 0)
      {
         pTmp++;
         continue;
      }

      // Fix problem as RAMON&PURISIMA
      if (*pTmp == '&' && acTmp[iTmp-1] != ' ')
      {
         acTmp[iTmp++] = ' ';
         acTmp[iTmp++] = *pTmp;
         if (*(pTmp+1) != ' ')
            acTmp[iTmp++] = ' ';
      } else  if (*pTmp == '\\')
      {
         acTmp[iTmp++] = '/';
      } else
         acTmp[iTmp++] = toupper(*pTmp);

      // Remove multi-space
      if (*pTmp == ' ')
         while (*pTmp == ' ') pTmp++;
      else
         pTmp++;

      // Remove unwanted characters
      if (*pTmp == '`' || *pTmp == ':' || *pTmp == 39)
         pTmp++;

      // Replace '.' with space
      if (*pTmp == '.')
         *pTmp = ' ';
   }
   if (acTmp[iTmp-1] == ' ' || acTmp[iTmp-1] == '-' || acTmp[iTmp-1] == '&')
      iTmp -= 1;
   acTmp[iTmp] = 0;
   acSave1[0] = 0;

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "1405 057", 8))
   //   iTmp = 0;
#endif

   // Replace double && with &
   replStr(acTmp, "& &", "&");

   // Remove special vesting
   if ((pTmp=strstr(acTmp, ",MR")) || (pTmp=strstr(acTmp, ", MR")) )
      *pTmp = 0;

   // Update vesting
   iTmp = updateVesting(myCounty.acCntyCode, acTmp, pOutbuf+OFF_VEST, pOutbuf+OFF_ETAL_FLG);
   if (iTmp == 2)
   {
      vmemcpy(pOutbuf+OFF_NAME1, acTmp, SIZ_NAME1);
      vmemcpy(pOutbuf+OFF_NAME_SWAP, acTmp, SIZ_NAME_SWAP);
      return;
   }

   if ((pTmp=strstr(acTmp, " 1/2")) || (pTmp=strstr(acTmp, " 1/4")) ||
      (pTmp=strstr(acTmp, " 1/6"))  || (pTmp=strstr(acTmp, " 1/3")) ||
      (pTmp=strstr(acTmp, " 2/3"))  || (pTmp=strstr(acTmp, " 28/270")) ||
      (pTmp=strstr(acTmp, " W/H"))  || (pTmp=strstr(acTmp, " H/W")) )
      *pTmp = 0;

   // If number appears in name, do not parse
   if (isNumIncluded(acTmp))
   {
      vmemcpy(pOutbuf+OFF_NAME1, acTmp, SIZ_NAME1);
      vmemcpy(pOutbuf+OFF_NAME_SWAP, acTmp, SIZ_NAME_SWAP);
      return;
   }

   // Save it 
   strcpy(acOwner, acTmp);

   // SADOWY 1999 TRUST
   if (iTmp > 2 && acTmp[iTmp]  && !strchr((char *)&acTmp[iTmp], '&'))
   {
     if ( (pTmp=strstr(acTmp, " ETAL")) || (pTmp=strstr(acTmp, " ET AL")))
        *pTmp = 0;

      // If TRUST appears after number, save from number forward
      if ((pTmp=strstr((char *)&acTmp[iTmp], " LIVING")) || (pTmp=strstr((char *)&acTmp[iTmp], " REVOC") ) ||
          (pTmp=strstr((char *)&acTmp[iTmp], " TRUST")) )
      {
         if (&acTmp[iTmp] < pTmp)
         {
            iTmp--;
            strcpy(acSave1, (char *)&acTmp[iTmp]);
            acTmp[iTmp] = 0;
         }
      }
   }

   // Drop what in parenthesis
   if (pTmp = strchr(acTmp, '('))
      *pTmp = 0;        

   // Filter out words, things in parenthesis
   // MONDANI NELLIE M ESTATE OF

   if ((pTmp = strchr(acTmp, '('))      || (pTmp=strstr(acTmp, " CO TR")) ||
      (pTmp=strstr(acTmp, " CO-TR"))    || (pTmp=strstr(acTmp, " COTR")) ||
      (pTmp=strstr(acTmp, " LIFE EST")) || (pTmp=strstr(acTmp, " ET AL TRUSTEE")) ||
      (pTmp=strstr(acTmp, ",TRSTE"))    || (pTmp=strstr(acTmp, " TSTE")) ||
      (pTmp=strstr(acTmp, ",TRUSTEE"))  || (pTmp=strstr(acTmp, " TRUSTEE")) ||
      (pTmp=strstr(acTmp, " TRST"))     || (pTmp=strstr(acTmp, " TR ")) ||
      (pTmp=strstr(acTmp, " TRES"))     || (pTmp=strstr(acTmp, " TTEE")) ||
      (pTmp=strstr(acTmp, " ESTATE OF"))|| (pTmp=strstr(acTmp, " ESTS OF")) ||
      (pTmp=strstr(acTmp, " EST OF"))   || (pTmp=strstr(acTmp, ",ET AL")) ||
      (pTmp=strstr(acTmp, " ETAL"))     || (pTmp=strstr(acTmp, " ET AL")) )
      *pTmp = 0;

   if (!memcmp((char *)&acTmp[strlen(acTmp)-3], " RE", 3))
      acTmp[strlen(acTmp)-3] = 0;
   else if (!memcmp((char *)&acTmp[strlen(acTmp)-4], " REV", 4))
      acTmp[strlen(acTmp)-4] = 0;

   // Ignore MD
   if (pTmp=strstr(acTmp, " MD &"))
   {  // BRYCE I. MORRICE, MD & ELLEN S
      *pTmp = 0;
      pTmp += 3;
      strcat(acTmp, pTmp);
   } else if (!memcmp(acTmp, "DR ", 3))
   {  // Ignore DR in "DR ANVAR M VELJI & PARI VELJI"
      pTmp = (char *)&acTmp[3];
      strcpy(acTmp, pTmp);
   }

   iTmp = strlen(acTmp);
   if ((pTmp=strstr(acTmp, " REVOC"))  || (pTmp=strstr(acTmp, " REV TR"))  ||
       (pTmp=strstr(acTmp, " REV LIV"))|| (pTmp=strstr(acTmp, " REV LVNG")) ||
       (pTmp=strstr(acTmp, " FAMILY "))|| (pTmp=strstr(acTmp, " FMLY ")) ||
       (pTmp=strstr(acTmp, " FAM TR")) || (pTmp=strstr(acTmp, " LVG TR")) ||
       (pTmp=strstr(acTmp, " LVNG TR"))|| (pTmp=strstr(acTmp, " EXEMPT TR")) ||
       (pTmp=strstr(acTmp, " LIV TR")) || (pTmp=strstr(acTmp, " LIVING TR")) ||
       (pTmp=strstr(acTmp, " TRUST")) )
   {  // KLOTZ GENE L & JANE G FAMILY TRUST TESTAMENTARY
      if (isdigit(*(pTmp-1)) )
         while (isdigit(*(--pTmp)));

      strcpy(acSave1, pTmp);
      *pTmp = 0;
      strcpy(acName1, acTmp);
   } else if (!memcmp((char *)&acTmp[iTmp-6], " FAM T", 6))
   {
      acTmp[iTmp-6] = 0;
      strcpy(acSave1, " FAM TR");
      strcpy(acName1, acTmp);
   } else if (!memcmp((char *)&acTmp[iTmp-6], " REV T", 6))
   {
      acTmp[iTmp-6] = 0;
      strcpy(acSave1, " REV TR");
      strcpy(acName1, acTmp);
   } else if (!memcmp((char *)&acTmp[iTmp-6], " REV I", 6))
   {
      acTmp[iTmp-6] = 0;
      strcpy(acSave1, " REV I");
      strcpy(acName1, acTmp);
   } else if (!memcmp((char *)&acTmp[iTmp-7], " LIVING", 7))
   {
      acTmp[iTmp-7] = 0;
      strcpy(acSave1, " LIVING");
      strcpy(acName1, acTmp);
   } else if (!memcmp((char *)&acTmp[iTmp-5], " TRUS", 5))
   {
      acTmp[iTmp-5] = 0;
      strcpy(acSave1, " TRUST");
      strcpy(acName1, acTmp);
   } else if (!memcmp((char *)&acTmp[iTmp-4], " TRU", 4))
   {
      acTmp[iTmp-4] = 0;
      strcpy(acSave1, " TRUST");
      strcpy(acName1, acTmp);
   } else if (!memcmp((char *)&acTmp[iTmp-3], " MD", 3))
   {
      acTmp[iTmp-3] = 0;
      strcpy(acName1, acTmp);
   } else
      strcpy(acName1, acTmp);

   if (pTmp = strstr(acSave1, " THE"))
      *pTmp = 0;

   // Remove alias
   if ((pTmp=strstr(acName1, " AS ")) || (pTmp=strstr(acName1, " AKA ")) )
      *pTmp = 0;

   // We keep first two name only, drop the rest
   // TAYLOR JAMES H & MERRIEL & LEFEVRE PATRICIA M & RE
   if (pTmp = strchr(acName1, '&'))
   {
      if (pTmp1 = strchr(pTmp+1, '&'))
         *pTmp1++ = 0;
   }

   // Update owner
   vmemcpy(pOutbuf+OFF_NAME1, acOwner, SIZ_NAME1);

   // Now parse owners
   memset((void *)&myOwner, 0, sizeof(OWNER));
   if (strchr(acName1, ' '))
   {
      iRet = splitOwner(acName1, &myOwner, 6);

      // If name is not swapable, use Name1 for swap
      if (iRet == -1)
         vmemcpy(pOutbuf+OFF_NAME_SWAP, acOwner, SIZ_NAME_SWAP);
      else
         vmemcpy(pOutbuf+OFF_NAME_SWAP, myOwner.acSwapName, SIZ_NAME_SWAP);
   } else
      vmemcpy(pOutbuf+OFF_NAME_SWAP, acOwner, SIZ_NAME_SWAP);
}

void Sfx_MergeOwnerX(char *pOutbuf, char *pNames)
{
   int   iTmp, iTmp1, iRet;
   char  acTmp1[128], acTmp[128], acSave1[64], *pTmp, *pTmp1;
   char  acName1[64], acName2[64];
   OWNER myOwner;

   // Clear output buffer if needed
   removeNames(pOutbuf, false);
   memset(acTmp, 0, 128);

   // Remove multiple spaces
   pTmp = strcpy(acTmp1, pNames);
   iTmp = 0;
   while (*pTmp)
   {
      // skip unwanted chars at beginning of name
      if (*pTmp < '1' && iTmp == 0)
      {
         pTmp++;
         continue;
      }

      // Fix problem as RAMON&PURISIMA
      if (*pTmp == '&' && acTmp[iTmp-1] != ' ')
      {
         acTmp[iTmp++] = ' ';
         acTmp[iTmp++] = *pTmp;
         if (*(pTmp+1) != ' ')
            acTmp[iTmp++] = ' ';
      } else
         acTmp[iTmp++] = toupper(*pTmp);

      // Remove multi-space
      if (*pTmp == ' ')
         while (*pTmp == ' ') pTmp++;
      else
         pTmp++;

      // Remove unwanted characters
      if (*pTmp == '`' || *pTmp == ':' || *pTmp == 39)
         pTmp++;

      // Replace '.' with space
      if (*pTmp == '.')
         *pTmp = ' ';
   }
   if (acTmp[iTmp-1] == ' ' || acTmp[iTmp-1] == '-')
      iTmp -= 1;
   acTmp[iTmp] = 0;
   acSave1[0] = 0;

   // Update vesting
   iTmp = updateVesting(myCounty.acCntyCode, acTmp, pOutbuf+OFF_VEST, pOutbuf+OFF_ETAL_FLG);

   if ((pTmp=strstr(acTmp, " 1/2")) || (pTmp=strstr(acTmp, " 1/4")) ||
      (pTmp=strstr(acTmp, " 1/6")) || (pTmp=strstr(acTmp, " 1/3")) ||
      (pTmp=strstr(acTmp, " 2/3")) || (pTmp=strstr(acTmp, " 28/270")) ||
      (pTmp=strstr(acTmp, " W/H")) || (pTmp=strstr(acTmp, " H/W")) ||
      (pTmp=strstr(acTmp, " & &"))  )
      *pTmp = 0;

   // Check for year that goes before TRUST
   iTmp =0;
   while (acTmp[iTmp])
   {
      if (isdigit(acTmp[iTmp]))
         break;
      iTmp++;
   }

   // If number appears at the beginning of name, do not parse
   if (!iTmp)
   {
      iTmp1 = strlen(acTmp);
      if (iTmp1 > SIZ_NAME1) iTmp1 = SIZ_NAME1;
      memcpy(pOutbuf+OFF_NAME1, acTmp, iTmp1);
      memcpy(pOutbuf+OFF_NAME_SWAP, acTmp, iTmp1);
      return;
   }

   // Save it - Only do it for individual trust
   // SADOWY 1999 TRUST
   if (acTmp[iTmp]  && !strchr((char *)&acTmp[iTmp], '&'))
   {
     if ( (pTmp=strstr(acTmp, " ETAL")) || (pTmp=strstr(acTmp, " ET AL")))
        *pTmp = 0;

      // If TRUST appears after number, save from number forward
      if ((pTmp=strstr((char *)&acTmp[iTmp], " LIVING")) || (pTmp=strstr((char *)&acTmp[iTmp], " REVOC") ) ||
          (pTmp=strstr((char *)&acTmp[iTmp], " TRUST")) )
      {
         if (&acTmp[iTmp] < pTmp)
         {
            iTmp--;
            strcpy(acSave1, (char *)&acTmp[iTmp]);
            acTmp[iTmp] = 0;
         }
      }
   }

   // Split name into two if ';' presents
   if (pTmp = strchr(acTmp, ';'))
   {  // ALLEN GLADYS; DEVOILE MARTHA LORENE
      *pTmp++ = 0;
      if (pTmp1 = strchr(pTmp, ';'))
         *pTmp1 = 0;
      strcpy(acName2, pTmp);
      if ((pTmp=strstr(acName2, " ETAL")) || (pTmp=strstr(acName2, " ET AL")) )
         *pTmp = 0;
   } else if (pTmp = strchr(acTmp, '('))
      *pTmp = 0;        // Drop what in parenthesis

   // Filter out words, things in parenthesis
   // MONDANI NELLIE M ESTATE OF

   if ((pTmp = strchr(acTmp, '('))      || (pTmp=strstr(acTmp, " CO TR")) ||
      (pTmp=strstr(acTmp, " CO-TR"))    || (pTmp=strstr(acTmp, " COTR")) ||
      (pTmp=strstr(acTmp, " LIFE EST")) || (pTmp=strstr(acTmp, " ET AL TRUSTEE")) ||
      (pTmp=strstr(acTmp, ",TRSTE"))    || (pTmp=strstr(acTmp, " TSTE")) ||
      (pTmp=strstr(acTmp, ",TRUSTEE"))  || (pTmp=strstr(acTmp, " TRUSTEE")) ||
      (pTmp=strstr(acTmp, " TRST"))     || (pTmp=strstr(acTmp, " TR ")) ||
      (pTmp=strstr(acTmp, " TRES"))     || (pTmp=strstr(acTmp, " TTEE")) ||
      (pTmp=strstr(acTmp, " ESTATE OF"))|| (pTmp=strstr(acTmp, " ESTS OF")) ||
      (pTmp=strstr(acTmp, " EST OF"))   || (pTmp=strstr(acTmp, ",ET AL")) ||
      (pTmp=strstr(acTmp, " ETAL"))     || (pTmp=strstr(acTmp, " ET AL")) )
      *pTmp = 0;

   if (!memcmp((char *)&acTmp[strlen(acTmp)-3], " RE", 3))
      acTmp[strlen(acTmp)-3] = 0;
   else if (!memcmp((char *)&acTmp[strlen(acTmp)-4], " REV", 4))
      acTmp[strlen(acTmp)-4] = 0;

   // Ignore MD
   if (pTmp=strstr(acTmp, " MD &"))
   {  // BRYCE I. MORRICE, MD & ELLEN S
      *pTmp = 0;
      pTmp += 3;
      strcat(acTmp, pTmp);
   } else if (!memcmp(acTmp, "DR ", 3))
   {  // Ignore DR in "DR ANVAR M VELJI & PARI VELJI"
      pTmp = (char *)&acTmp[3];
      strcpy(acTmp, pTmp);
   }

   iTmp = strlen(acTmp);
   if ((pTmp=strstr(acTmp, " REVOC"))  || (pTmp=strstr(acTmp, " REV TR"))  ||
       (pTmp=strstr(acTmp, " REV LIV"))|| (pTmp=strstr(acTmp, " REV LVNG")) ||
       (pTmp=strstr(acTmp, " FAMILY "))|| (pTmp=strstr(acTmp, " FMLY ")) ||
       (pTmp=strstr(acTmp, " FAM TR")) || (pTmp=strstr(acTmp, " LVG TR")) ||
       (pTmp=strstr(acTmp, " LVNG TR"))|| (pTmp=strstr(acTmp, " EXEMPT TR")) ||
       (pTmp=strstr(acTmp, " LIV TR")) || (pTmp=strstr(acTmp, " LIVING TR")) ||
       (pTmp=strstr(acTmp, " TRUST")) )
   {  // KLOTZ GENE L & JANE G FAMILY TRUST TESTAMENTARY
      if (isdigit(*(pTmp-1)) )
         while (isdigit(*(--pTmp)));

      strcpy(acSave1, pTmp);
      *pTmp = 0;
      strcpy(acName1, acTmp);
   } else if (!memcmp((char *)&acTmp[iTmp-6], " FAM T", 6))
   {
      acTmp[iTmp-6] = 0;
      strcpy(acSave1, " FAM TR");
      strcpy(acName1, acTmp);
   } else if (!memcmp((char *)&acTmp[iTmp-6], " REV T", 6))
   {
      acTmp[iTmp-6] = 0;
      strcpy(acSave1, " REV TR");
      strcpy(acName1, acTmp);
   } else if (!memcmp((char *)&acTmp[iTmp-6], " REV I", 6))
   {
      acTmp[iTmp-6] = 0;
      strcpy(acSave1, " REV I");
      strcpy(acName1, acTmp);
   } else if (!memcmp((char *)&acTmp[iTmp-7], " LIVING", 7))
   {
      acTmp[iTmp-7] = 0;
      strcpy(acSave1, " LIVING");
      strcpy(acName1, acTmp);
   } else if (!memcmp((char *)&acTmp[iTmp-5], " TRUS", 5))
   {
      acTmp[iTmp-5] = 0;
      strcpy(acSave1, " TRUST");
      strcpy(acName1, acTmp);
   } else if (!memcmp((char *)&acTmp[iTmp-4], " TRU", 4))
   {
      acTmp[iTmp-4] = 0;
      strcpy(acSave1, " TRUST");
      strcpy(acName1, acTmp);
   } else if (!memcmp((char *)&acTmp[iTmp-3], " MD", 3))
   {
      acTmp[iTmp-3] = 0;
      strcpy(acName1, acTmp);
   } else
      strcpy(acName1, acTmp);

   if (pTmp = strstr(acSave1, " THE"))
      *pTmp = 0;

   // Remove alias
   if ((pTmp=strstr(acName1, " AS ")) || (pTmp=strstr(acName1, " AKA ")) )
      *pTmp = 0;

   // We keep first two name only, drop the rest
   // TAYLOR JAMES H & MERRIEL & LEFEVRE PATRICIA M & RE
   if (pTmp = strchr(acName1, '&'))
   {
      if (pTmp1 = strchr(pTmp+1, '&'))
      {
         *pTmp1++ = 0;
         if (*pTmp1 == ' ') pTmp1++;
         strcpy(acName2, pTmp1);
      }
   }

   // Now parse owners
   memset((void *)&myOwner, 0, sizeof(OWNER));
   iRet = 0;
   if (strchr(acName1, ' '))
   {
      iRet = splitOwner(acName1, &myOwner, 6);

      // If parsable, use it.  Otherwise, keep the original
      if (iRet >= 0)
         strcpy(acTmp1, myOwner.acName1);
      else
         strcpy(acTmp1, acName1);

      // Check for saving?
      if (acSave1[0])
      {
         myTrim(acTmp1);
         strcat(acTmp1, acSave1);
      }

      iTmp = strlen(acTmp1);
      if (iTmp > SIZ_NAME1)
         iTmp = SIZ_NAME1;
      memcpy(pOutbuf+OFF_NAME1, acTmp1, iTmp);

      // If name is not swapable, use Name1 for swap
      if (iRet == -1)
      {
         iTmp = strlen(acName1);
         if (iTmp > SIZ_NAME_SWAP)
            iTmp = SIZ_NAME_SWAP;
         memcpy(pOutbuf+OFF_NAME_SWAP, acName1, iTmp);
      } else
      {
         iTmp = strlen(myOwner.acSwapName);
         if (iTmp > SIZ_NAME_SWAP)
            iTmp = SIZ_NAME_SWAP;
         memcpy(pOutbuf+OFF_NAME_SWAP, myOwner.acSwapName, iTmp);
      }

   } else
   {
      if (acSave1[1] > ' ')
         strcat(acName1, acSave1);
      else
         strcpy(acName1, pNames);

      iTmp = strlen(acName1);
      if (iTmp > SIZ_NAME1)
         iTmp = SIZ_NAME1;
      memcpy(pOutbuf+OFF_NAME1, acName1, iTmp);
      memcpy(pOutbuf+OFF_NAME_SWAP, acName1, iTmp);
   }
}

/******************************** Sfx_MatchOwner *****************************
 *
 * Update Owner name, RecDate, RecNum (format as A999-9999).
 * Return 0 if match, -1 is EOF, 1 is not matched
 *
 *****************************************************************************/

int Sfx_MatchOwner(char *pOutbuf)
{
   static    char acRec[512], *pRec=NULL;
   char      acTmp[256], acDocNum[32], *pTmp;
   int       iRet=0, iTmp, iParseFlg=0;
   long      lSaleDate, lRecDate;
   SFX_OWNER *pORec;

   // Get rec
   if (!pRec)
   {
      // Get first rec
      if (!iOwnerLen)
         pRec = fgets(acRec, 512, fdOwner);
      else
      {
         iTmp = fread(acRec, 1,iOwnerLen, fdOwner);
         pRec = &acRec[0];
      }
   }

   pORec = (SFX_OWNER *)pRec;

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "1829 024", 8))
   //   iTmp = 0;
#endif

   do
   {
      if (!pRec)
      {
         fclose(fdOwner);
         fdOwner = NULL;
         return 1;      // EOF
      }

#ifdef _DEBUG
      //if (!isdigit(pORec->Apn[0]) || !memcmp(pORec->Apn, "4072 001B", 9))
      //   iTmp = 0;
#endif
      iTmp = memcmp(pOutbuf, pORec->Apn, myCounty.iApnLen);
      if (iTmp > 0)
      {
         if (bDebug)
            LogMsg0("Skip Owner rec %.*s", myCounty.iApnLen, pORec->Apn);
         if (!iOwnerLen)
         {
            pRec = fgets(acRec, 512, fdOwner);

            // Check for corrupted input line
            if (pRec)
            {
               iRet = strlen(pRec);
               if (iRet < 85)
               {
                  pTmp = fgets(acTmp, 256, fdOwner);
                  pTmp = strchr(acRec, '\n');
                  *pTmp = ' ';
                  strcat(acRec, acTmp);
               }
            }
         } else
         {
            iTmp = fread(acRec, 1,iOwnerLen, fdOwner);
            if (iTmp != iOwnerLen)
               pRec = NULL;
         }
         lOwnerSkip++;
      }
   } while (iTmp > 0);

   // If not match, return
   if (iTmp)
      return 1;

   // Cleanup read data
   iTmp = replCharEx(acRec, 31, 32, iOwnerLen-2);
   if (iTmp > 0)
      LogMsg("*** Bad Owner record at pos %d [%.8s]", iTmp, pORec->Apn);

   // Merge Owner
   memcpy(acTmp, pORec->Name, OSIZ_NAME);
   myTrim(acTmp, OSIZ_NAME);

   // Check for public office
   if (pORec->Status == 'N')
   {
      iTmp = strlen(acTmp);
      memcpy(pOutbuf+OFF_NAME1, acTmp, iTmp);
      memcpy(pOutbuf+OFF_NAME_SWAP, acTmp, iTmp);
      *(pOutbuf+OFF_PUBL_FLG) = 'Y';
      memcpy(pOutbuf+OFF_VEST, "GV", 2);
   } else
      Sfx_MergeOwner(pOutbuf, acTmp);

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "3082 018", 8))
   //   iTmp = 0;
#endif
   // Recording info
   if (pORec->RecNum[0] > ' ')
   {
      //sprintf(acDocNum, "%.4s-%.4s           ", pORec->RecNum, (char *)&pORec->RecNum[4]);
      sprintf(acDocNum, "%.4s%.3s           ", pORec->RecNum, (char *)&pORec->RecNum[5]);
      formDate(pORec->RecDate, acTmp);
      lRecDate  = atoin(acTmp, SIZ_TRANSFER_DT);
      if (lRecDate > 0)
      {
         lSaleDate = atoin(sCurSale.DocDate, SIZ_TRANSFER_DT);
         if (lRecDate == lSaleDate)
         {  // Same date - This is a sale, copy DocNum
            if (sCurSale.DocNum[0] == ' ')
            {
               memcpy(sCurSale.DocNum, acDocNum, SIZ_SALE1_DOC);
               memcpy(sCurSale.Name1, pORec->Name, OSIZ_NAME);
            } else
               LogMsg("??? Questionable transaction: %.10s", pORec->Apn);
         } 
      }
   }

   lOwnerMatch++;

   // Get next record
   if (!iOwnerLen)
   {
      pRec = fgets(acRec, 512, fdOwner);
      // Check for corrupted input line
      if (pRec)
      {
         iRet = strlen(pRec);
         if (iRet < 85)
         {
            pTmp = fgets(acTmp, 256, fdOwner);
            pTmp = strchr(acRec, '\n');
            *pTmp = ' ';
            strcat(acRec, acTmp);
         }
      }
   } else
   {
      iTmp = fread(acRec, 1,iOwnerLen, fdOwner);
      if (iTmp != iOwnerLen)
         pRec = NULL;
   }
   pTmp = pRec;

   return 0;
}

/******************************** Sfx_MergeMAdr ******************************
 *
 * Merge Mailing address
 * Remove apos char from street name such as O'FARRELL to OFARRELL
 * Return 0 if match, -1 is EOF, 1 is not matched
 *
 *****************************************************************************/

#define  DONT_PARSE_LINE1  1
#define  DONT_PARSE_LINE2  2

int Sfx_MergeMAdr(char *pOutbuf)
{
   static   char acRec[512], *pRec=NULL;
   char     acTmp[256], acAddr1[256], acAddr2[256], *pTmp;
   int      iRet=0, iTmp, iStrNo, iParseFlg=0;
   SFX_MADR *pMRec;
   ADR_REC  sMailAdr;

   // Get rec
   if (!pRec)
   {
      // Skip header record
      pRec = fgets(acRec, 512, fdMAdr);
      // Get first rec
      pRec = fgets(acRec, 512, fdMAdr);
   }

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "3985 047", 8))
   //   iTmp = 0;
#endif
   pMRec = (SFX_MADR *)pRec;
   do
   {
      if (!pRec)
      {
         fclose(fdMAdr);
         fdMAdr = NULL;
         return -1;      // EOF
      }

      // Add 1 to Situs rec to skip double quote
      iTmp = memcmp(pOutbuf, pMRec->Apn, ASIZ_APN);
      if (iTmp > 0)
      {
         if (bDebug)
            LogMsg0("Skip Mailing rec %.*s", ASIZ_APN, pMRec->Apn);
         pRec = fgets(acRec, 512, fdMAdr);
         lMAdrSkip++;
      }
   } while (iTmp > 0);

   // If not match, return
   if (iTmp)
      return 1;

   // Clear old Mailing
   removeMailing(pOutbuf, true);

   // Convert to upper case
   _strupr(acRec);

   // Check for known bad char
   if (pTmp = strchr(acRec, 0xFC))
      *pTmp = 'U';

   // Check for blank
   if (!memcmp(pMRec->M_Addr2, "     ", 5))
   {
      // Populate mailing address with situs.
      memcpy(pOutbuf+OFF_M_STRNUM, pOutbuf+OFF_S_STRNUM, SIZ_S_STRNUM);
      memcpy(pOutbuf+OFF_M_STR_SUB, pOutbuf+OFF_S_STR_SUB, SIZ_M_STR_SUB);
      memcpy(pOutbuf+OFF_M_DIR, pOutbuf+OFF_S_DIR, SIZ_M_DIR);
      memcpy(pOutbuf+OFF_M_STREET, pOutbuf+OFF_S_STREET, SIZ_M_STREET);
      memcpy(pOutbuf+OFF_M_UNITNO, pOutbuf+OFF_S_UNITNO, SIZ_S_UNITNO);
      memcpy(pOutbuf+OFF_M_UNITNOX, pOutbuf+OFF_S_UNITNOX, SIZ_M_UNITNOX);

      // Translate suffix
      iTmp = atoin(pOutbuf+OFF_S_SUFF, 3);
      if (iTmp > 0)
      {
         iTmp = sprintf(acTmp, "%s", GetSfxStr(iTmp));
         memcpy(pOutbuf+OFF_M_SUFF, acTmp, iTmp);
      }

      // Translate city
      memcpy(pOutbuf+OFF_M_CITY, "SAN FRANCISCO", 13);
      memcpy(pOutbuf+OFF_M_ST, pOutbuf+OFF_S_ST, SIZ_M_ST);
      memcpy(pOutbuf+OFF_M_ZIP, pOutbuf+OFF_S_ZIP, SIZ_M_ZIP);
      memcpy(pOutbuf+OFF_M_ADDR_D, pOutbuf+OFF_S_ADDR_D, SIZ_M_ADDR_D);
      memcpy(pOutbuf+OFF_M_CTY_ST_D, pOutbuf+OFF_S_CTY_ST_D, SIZ_M_CTY_ST_D);

      return 0;
   }

   iStrNo = atol(pMRec->M_Addr1);

   if (!memcmp(pMRec->M_Addr1, "PO BOX", 6) || !memcmp(pMRec->M_Addr1, "P.O.", 4) ||
       !memcmp(pMRec->M_Addr1, "P O BOX", 7) )
   {
      memcpy(acAddr1, pMRec->M_Addr1, ASIZ_M_ADDR);
      memcpy(acAddr2, pMRec->M_Addr2, ASIZ_M_ADDR);
      iParseFlg = DONT_PARSE_LINE1;
   } else if (!memcmp(pMRec->M_Addr1, "C/O", 3) )
   {
      /*
      memcpy(acTmp, (char *)&pMRec->M_Addr1[4], ASIZ_M_ADDR-4);
      acTmp[ASIZ_M_ADDR-4] = 0;
      iTmp = strlen(acTmp);
      if (iTmp > SIZ_CARE_OF) iTmp = SIZ_CARE_OF;
      memcpy(pOutbuf+OFF_CARE_OF, (char *)&pMRec->M_Addr1[4], iTmp);
      */
      updateCareOf(pOutbuf, pMRec->M_Addr1, ASIZ_M_ADDR);
      memcpy(acAddr1, pMRec->M_Addr2, ASIZ_M_ADDR);
      memcpy(acAddr2, pMRec->M_Addr3, ASIZ_M_ADDR);
   } else if (pMRec->M_Addr1[0] == '%')
   {
      /*
      if (pMRec->M_Addr1[1] == ' ')
      {
         memcpy(acTmp, (char *)&pMRec->M_Addr1[2], ASIZ_M_ADDR-2);
         acTmp[ASIZ_M_ADDR-2] = 0;
      } else
      {
         memcpy(acTmp, (char *)&pMRec->M_Addr1[1], ASIZ_M_ADDR-1);
         acTmp[ASIZ_M_ADDR-1] = 0;
      }

      iTmp = strlen(acTmp);
      if (iTmp > SIZ_CARE_OF) iTmp = SIZ_CARE_OF;
      memcpy(pOutbuf+OFF_CARE_OF, acTmp, iTmp);
      */
      updateCareOf(pOutbuf, pMRec->M_Addr1, ASIZ_M_ADDR);
      memcpy(acAddr1, pMRec->M_Addr2, ASIZ_M_ADDR);
      memcpy(acAddr2, pMRec->M_Addr3, ASIZ_M_ADDR);
   } else if (pMRec->M_Addr3[0] > ' ')
   {
      memcpy(acAddr1, pMRec->M_Addr2, ASIZ_M_ADDR);
      memcpy(acAddr2, pMRec->M_Addr3, ASIZ_M_ADDR);
   } else
   {
      memcpy(acAddr1, pMRec->M_Addr1, ASIZ_M_ADDR);
      memcpy(acAddr2, pMRec->M_Addr2, ASIZ_M_ADDR);
   }
   myTrim(acAddr1, ASIZ_M_ADDR);
   myTrim(acAddr2, ASIZ_M_ADDR);

   // Check for foreign address
   if (pTmp = strrchr(acAddr2, ','))
   {
      *pTmp++ = ' ';
      while (*pTmp == ' ') pTmp++;
      if (!*pTmp) pTmp = NULL;
   } else
      pTmp = strrchr(acAddr2, ' ');

   if (pTmp && strlen(pTmp) != 3)
   {
      // We cannot parse foreign addresses, so keep it as is
      iParseFlg = -1;
   } else if (!pTmp || !*pTmp)
      iParseFlg = DONT_PARSE_LINE2;

   // Parse line 1
   if (iParseFlg & DONT_PARSE_LINE1)
   {
      memcpy(pOutbuf+OFF_M_STREET, acAddr1, strlen(acAddr1));
      memcpy(pOutbuf+OFF_M_ADDR_D, acAddr1, strlen(acAddr1));
   } else
   {
      // Parsing mail address line 1
      memset((void *)&sMailAdr, 0, sizeof(ADR_REC));

#ifdef _DEBUG
      // Check for " 14914 226 AV N E"
      //if (!memcmp(pOutbuf, "0187 028", 8))
      //   iTmp = 0;
#endif

      // Translate "N E" into "NE"
      strcpy(acTmp, acAddr1);
      if (!memcmp(_strrev(acTmp), "E N ", 4) )
      {
         sMailAdr.lStrNum = atol(acAddr1);
         pTmp = acAddr1;
         while (*pTmp == ' ') pTmp++;
         while (*pTmp != ' ') pTmp++;
         strcpy(sMailAdr.strName, ++pTmp);
         pTmp = strrchr(sMailAdr.strName, ' ');
         *pTmp++ = 'E';
         *pTmp = 0;
      } else
      {
         // Force StrSfx "LA" to "LN"
         pTmp = strrchr(acAddr1, ' ');
         if (pTmp && !strcmp(pTmp, " LA"))
            *(pTmp+2) = 'N';
         parseMAdr1(&sMailAdr, acAddr1);
      }

      if (sMailAdr.lStrNum > 0)
      {
         sprintf(acTmp, "%d       ", sMailAdr.lStrNum);
         memcpy(pOutbuf+OFF_M_STRNUM, acTmp, SIZ_M_STRNUM);
         if (sMailAdr.strSub[0] > '0')
         {
            sprintf(acTmp, "%s  ", sMailAdr.strSub);
            memcpy(pOutbuf+OFF_M_STR_SUB, acTmp, SIZ_M_STR_SUB);
         }
      }
      memcpy(pOutbuf+OFF_M_DIR, sMailAdr.strDir, strlen(sMailAdr.strDir));
      memcpy(pOutbuf+OFF_M_STREET, sMailAdr.strName, strlen(sMailAdr.strName));
      memcpy(pOutbuf+OFF_M_SUFF, sMailAdr.strSfx, strlen(sMailAdr.strSfx));
      if (sMailAdr.Unit[0] > ' ')
      {
         if (sMailAdr.Unit[0] != '#')
         {
            *(pOutbuf+OFF_M_UNITNO) = '#';
            vmemcpy(pOutbuf+OFF_M_UNITNO+1, sMailAdr.Unit, SIZ_M_UNITNO-1);
            *(pOutbuf+OFF_M_UNITNOX) = '#';
            vmemcpy(pOutbuf+OFF_M_UNITNOX+1, sMailAdr.UnitNox, SIZ_M_UNITNOX-1);
         } else
         {
            vmemcpy(pOutbuf+OFF_M_UNITNO, sMailAdr.Unit, SIZ_M_UNITNOX);
            vmemcpy(pOutbuf+OFF_M_UNITNOX, sMailAdr.UnitNox, SIZ_M_UNITNOX);
         }
      }

      vmemcpy(pOutbuf+OFF_M_ADDR_D, acAddr1, SIZ_M_ADDR_D);
   }

   // Check for known bad char in addr2
   if (pTmp = strchr(acAddr2, 0xDC))
      *pTmp = 'U';

   // Update addr line 2
   vmemcpy(pOutbuf+OFF_M_CTY_ST_D, acAddr2, SIZ_M_CTY_ST_D);

   if (iParseFlg & DONT_PARSE_LINE2)
   {
      vmemcpy(pOutbuf+OFF_M_CITY, acAddr2, SIZ_M_CITY);
   } else
   {
      pTmp = strrchr(acAddr2, ' ');
      if (pTmp)
         *pTmp++ = 0;
      else if (pTmp = strrchr(acAddr2, ','))
         *pTmp++ = 0;

      iTmp = strlen(acAddr2);
      if (iTmp > SIZ_M_CITY)
      {
         memcpy(pOutbuf+OFF_M_CITY, acAddr2, SIZ_M_CITY);
         vmemcpy(pOutbuf+OFF_M_CITYX, &acAddr2[SIZ_M_CITY], SIZ_M_CITYX);
         if (bDebug)
            LogMsg("*** Long mail city: %s [%.*s]", acAddr2, iApnLen, pOutbuf);
      } else
         vmemcpy(pOutbuf+OFF_M_CITY, acAddr2, SIZ_M_CITY);

      if (pTmp && strlen(pTmp) == SIZ_M_ST)
         memcpy(pOutbuf+OFF_M_ST, pTmp, SIZ_M_ST);
   }

   iTmp = atoin(pMRec->M_Zip, 5);
   if (iTmp > 500)
   {
      iTmp = sprintf(acAddr2, "%.5d", iTmp);
      memcpy(pOutbuf+OFF_M_ZIP, acAddr2, iTmp);
   } else
      memcpy(pOutbuf+OFF_M_ZIP, BLANK32, SIZ_M_ZIP);


   lMAdrMatch++;

   // Get next record
   pRec = fgets(acRec, 512, fdMAdr);

   return 0;
}

/********************************* Sfx_MergeSAdr *****************************
 *
 * 'V' after StrNum indicates vacant parcel.  However, this field is not properly maintain.
 *
 * Return 0 if successful, >0 if error
 *
 *****************************************************************************/

int Sfx_MergeSAdr(char *pOutbuf, char *pRollRec)
{
   char     acTmp[256], acAddr1[128], *pTmp, *pStrName;
   int      iTmp;
   ADR_REC  sSitusAdr;
   SFX_ROLL *pRec;

   pRec = (SFX_ROLL *)pRollRec;

   // 2013 LDR has different situs format
   memcpy(acTmp, pRec->S_Addr1, RSIZ_S_ADDR1);
   iTmp = blankRem(acTmp, RSIZ_S_ADDR1);

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "2607 105", 8) ) 
   //   pTmp = pRollRec;
#endif
   if (acTmp[0] > ' ')
   {
      // Clear old situs
      removeSitus(pOutbuf);

      // Ignore address with only one token
      if (!(pStrName = strchr(acTmp, ' ')) || strstr(acTmp, "V V "))
      {
         LogMsg0("--> Drop situs: %.10s %.*s", pOutbuf, RSIZ_S_ADDR1, pRec->S_Addr1);
         return -1;
      }

      // Remove 'V' after StrNum
      if (pTmp = strchr(acTmp, ' '))
      {
         if (*(pTmp-1) == 'V')
            *(pTmp-1) = ' ';
      }

      // Fix input
      *pStrName = 0;
      if (isdigit(acTmp[0]) && (pTmp = strchr(acTmp, '-')))
      {
         // 0052 078 445-FRANCISCO  UNIT F101
         if (*(pTmp+1) >= 'A' && *(pTmp+2) >= 'A' && isdigit(*(pTmp-1)) )
         {
            *pTmp = ' ';
            pStrName = pTmp;
         } else
            *pStrName = ' ';
      } else 
         *pStrName = ' ';

      iTmp = remChar(acTmp, ',');
      vmemcpy(pOutbuf+OFF_S_ADDR_D, acTmp, SIZ_S_ADDR_D, iTmp);

      // Multi situs, parse first one
      if (pTmp = strchr(acTmp, '/'))
         if (isalpha(*(pTmp-2)))
            *pTmp = 0;

      // Find multi StrNum
      long lNum1, lNum2;
      lNum1 = atol(acTmp);
      if (lNum1 > 0 && (pTmp = strstr(acTmp, " - ")))
      {
         lNum2 = atol(pTmp+2);
         if (lNum2 > lNum1)
         {
            iTmp = sprintf(acAddr1, "%d-%d", lNum1, lNum2);
            replStr(acTmp, " - ", "-");
         } else if (lNum2 < lNum1)
         {
            if (!lNum2)
            {
               iTmp = sprintf(acAddr1, "%d", lNum1);
            } else
            {
               LogMsg("??? StrNum %d-%d APN=%.10s", lNum1, lNum2, pOutbuf); 
               iTmp = sprintf(acAddr1, "%d-%d", lNum1, lNum2);
               replStr(acTmp, " - ", "-");
            }
         } else
            iTmp = sprintf(acAddr1, "%d", lNum1);
         vmemcpy(pOutbuf+OFF_S_HSENO,  acAddr1, SIZ_S_HSENO, iTmp);
      } else if (lNum1 > 0 && *(pStrName+1) == '&')
      {
         // Take first two numbers
         if (pTmp = strchr(pStrName+3, ' '))
         {
            *pTmp = 0;
            strcpy(acAddr1, acTmp);
            *pTmp = ' ';
            iTmp = remChar(acAddr1, ' ');
            if (iTmp > SIZ_S_HSENO)
            {
               *pStrName = 0;
               strcpy(acAddr1, acTmp);
               *pStrName = ' ';
            }
         }
         vmemcpy(pOutbuf+OFF_S_HSENO,  acAddr1, SIZ_S_HSENO);
      } else if (lNum1 > 0)
      {
         *pStrName = 0;
         strcpy(acAddr1, acTmp);
         *pStrName = ' ';
         vmemcpy(pOutbuf+OFF_S_HSENO,  acAddr1, SIZ_S_HSENO);
      }

      // 355 BUENA VISTA AVE EAST 101W
      // 186 MUSEUM WY / 180 STATES RD
      parseAdr1C_2(&sSitusAdr, acTmp);

      if (sSitusAdr.strNum[0] > '0')
         vmemcpy(pOutbuf+OFF_S_STRNUM, sSitusAdr.strNum, SIZ_S_STRNUM);
      if (sSitusAdr.strSub[0] > '0')
         vmemcpy(pOutbuf+OFF_S_STR_SUB, sSitusAdr.strSub, SIZ_S_STR_SUB);
      if (sSitusAdr.strDir[0] > ' ')
         vmemcpy(pOutbuf+OFF_S_DIR, sSitusAdr.strDir, SIZ_S_DIR);
      if (sSitusAdr.strName[0] > ' ')
      {
         if (pTmp = strstr(sSitusAdr.strName, " #"))
         {
            *(pTmp+1) = ' ';
            blankRem(sSitusAdr.strName);
         } else if (pTmp = strchr(sSitusAdr.strName, '#'))
         {
            if (sSitusAdr.Unit[0] <= ' ')
            {
               remChar(pTmp, ' ');
               strcpy(sSitusAdr.Unit, pTmp);
               strcpy(sSitusAdr.UnitNox, pTmp);
            }
            *pTmp = 0;

            // Check for StrSfx - last chance
            if (pTmp = strrchr(sSitusAdr.strName, ' '))
            {
               iTmp = GetSfxDev(pTmp+1);
               if (iTmp > 0)
               {
                  *pTmp = 0;
                  sprintf(sSitusAdr.strSfx, "%d", iTmp);
               }
            }
         }

         if (sSitusAdr.strName[0] == '0')
            vmemcpy(pOutbuf+OFF_S_STREET, &sSitusAdr.strName[1], SIZ_S_STREET);
         else
            vmemcpy(pOutbuf+OFF_S_STREET, sSitusAdr.strName, SIZ_S_STREET);
      }

      if (sSitusAdr.strSfx[0] > ' ')
         vmemcpy(pOutbuf+OFF_S_SUFF, sSitusAdr.strSfx, SIZ_S_SUFF);

      // Copy unit#
      if (sSitusAdr.Unit[0] > ' ')
      {
         vmemcpy(pOutbuf+OFF_S_UNITNO, sSitusAdr.Unit, SIZ_S_UNITNO);
         vmemcpy(pOutbuf+OFF_S_UNITNOX, sSitusAdr.UnitNox, SIZ_S_UNITNOX);
      }
   }

   // state
   memcpy(pOutbuf+OFF_S_ST, "CA", 2);
   *(pOutbuf+OFF_S_CITY) = '1';
   memcpy(pOutbuf+OFF_S_CTY_ST_D, "SAN FRANCISCO CA", 16);

   return 0;
}

void Sfx_MergeSAdr2(char *pOutbuf, char *pRollRec)
{
   char      acTmp[256], acAddr1[256], *pTmp;

   SFX_ROLL  *pRec;
   SFX_SITUS *pAdr;

   pRec = (SFX_ROLL *)pRollRec;
   pAdr = (SFX_SITUS *)pRec->S_Addr1;

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "0253T026H", 8))
   //   lTmp = 0;
#endif
   if (pAdr->StrName[0] > '0')
   {
      // Clear old situs
      removeSitus(pOutbuf);
      acAddr1[0] = 0;

      // Find multi StrNum
      long lNum1, lNum2, iTmp;
      lNum1 = atoin(pAdr->HseNum1, 4);
      lNum2 = atoin(pAdr->HseNum2, 4);
      if (lNum2 > 0)
      {
         if (lNum2 > lNum1)
         {
            if (pAdr->HseSub1[0] > '0')
               iTmp = sprintf(acAddr1, "%d%c-%d%c ", lNum1, pAdr->HseSub1[0], lNum2, pAdr->HseSub2[0]);
            else
               iTmp = sprintf(acAddr1, "%d-%d%c ", lNum1, lNum2, pAdr->HseSub2[0]);
         } else
         {
            if (pAdr->HseSub2[0] > '0')
               iTmp = sprintf(acAddr1, "%d%c-%d%c ", lNum2, pAdr->HseSub2[0], lNum1, pAdr->HseSub1[0]);
            else
               iTmp = sprintf(acAddr1, "%d-%d%c ", lNum2, lNum1, pAdr->HseSub1[0]);
         }

         vmemcpy(pOutbuf+OFF_S_HSENO, acAddr1, SIZ_S_HSENO);
      } else if (lNum1 > 0)
      {
         iTmp = sprintf(acAddr1, "%d%c ", lNum1, pAdr->HseSub1[0]);
         memcpy(pOutbuf+OFF_S_HSENO,  acAddr1, iTmp);
      }

      // 355 BUENA VISTA AVE EAST 101W
      // 186 MUSEUM WY / 180 STATES RD
      //parseAdr1C(&sSitusAdr, &acTmp[10]);

      if (lNum1 > 0)
      {
         iTmp = sprintf(acTmp, "%d", lNum1);
         memcpy(pOutbuf+OFF_S_STRNUM, acTmp, strlen(acTmp));
      }
      if (pAdr->HseSub1[0] > '0')
         *(pOutbuf+OFF_S_STR_SUB) = pAdr->HseSub1[0];

      if (pAdr->StrName[0] > '0')
      {
         memcpy(acTmp, pAdr->StrName, 20);
         iTmp = blankRem(acTmp, 20);
         if (acTmp[iTmp-1] == '#')
            acTmp[iTmp-1] = ' ';      

         if (!memcmp(pAdr->StrSfx, "SW", 2))
         {
            strcat(acTmp, " STAIRWAY");
            iTmp = blankRem(acTmp);
            pAdr->StrSfx[0] = ' ';
         }
         memcpy(pOutbuf+OFF_S_STREET, acTmp, iTmp);
         strcat(acAddr1, acTmp);
      }
      if (pAdr->StrSfx[0] > '0')
      {
         memcpy(acTmp, pAdr->StrSfx, 2);
         acTmp[2] = 0;

         strcat(acAddr1, " ");
         strcat(acAddr1, acTmp);

         if (iTmp = GetSfxDev(acTmp))
         {
            iTmp = sprintf(acTmp, "%d", iTmp);
            memcpy(pOutbuf+OFF_S_SUFF, acTmp, iTmp);
         } else if (!memcmp(pAdr->StrSfx, "SY", 2))
         {  // Misspell of ST
            *(pOutbuf+OFF_S_SUFF) = '1';
         } else  if (isdigit(pAdr->StrSfx[0]) && isdigit(pAdr->StrSfx[1]))
         {  // Portion of UNIT#
            //memcpy(pAdr->UnitNo, pAdr->StrSfx, 4);
            iTmp = 0;
         } else
            LogMsg("*** Unknown StrSfx: %s APN=%.10s", acTmp, pOutbuf);
      }

      // Copy unit#
      if (memcmp(pAdr->UnitNo, "0000", 4))
      {
         strcat(acAddr1, " ");
         lNum1 = atoin(pAdr->UnitNo, 4);
         if (lNum1 > 0)
         {
            iTmp = sprintf(acTmp, "#%d%c", lNum1, pAdr->UnitNo[4]);
            memcpy(pOutbuf+OFF_S_UNITNO, acTmp, iTmp);
            memcpy(pOutbuf+OFF_S_UNITNOX, acTmp, iTmp);
            strcat(acAddr1, acTmp);
         } else
         {
            // Remove left over from "UNIT"
            pAdr->UnitNo[4] = 0;
            if (pAdr->UnitNo[0] == 'T')
               pTmp = &pAdr->UnitNo[2];
            if (pAdr->UnitNo[4] > '0' && !memcmp(pAdr->UnitNo, "0000", 4))
            {
               iTmp = sprintf(acTmp, "#%c", pAdr->UnitNo[4]);
               memcpy(pOutbuf+OFF_S_UNITNO, acTmp, iTmp);
               memcpy(pOutbuf+OFF_S_UNITNOX, acTmp, iTmp);
               strcat(acAddr1, acTmp);
            } else
            {
               pTmp = pAdr->UnitNo;
               for (iTmp = 4; iTmp > 0; iTmp--)
               {
                  if (*pTmp > '0')
                     break;
                  pTmp++;
               }

               if (iTmp > 0)
               {
                  // Check for StrSfx
                  if (!memcmp(pTmp, "AVE", 3))
                  {
                     memcpy(pOutbuf+OFF_S_SUFF, "3  ", 3);
                  } else
                  {
                     sprintf(acTmp, "#%.*s", iTmp+1, pTmp);
                     iTmp = remChar(acTmp, ' ');
                     memcpy(pOutbuf+OFF_S_UNITNO, acTmp, iTmp);
                     memcpy(pOutbuf+OFF_S_UNITNOX, acTmp, iTmp);
                     strcat(acAddr1, acTmp);
                  }
               }
            }
         }
      }

      iTmp = blankRem(acAddr1);
      memcpy(pOutbuf+OFF_S_ADDR_D, acAddr1, iTmp);
   }

   // state
   memcpy(pOutbuf+OFF_S_ST, "CA", 2);
   *(pOutbuf+OFF_S_CITY) = '1';
   memcpy(pOutbuf+OFF_S_CTY_ST_D, "SAN FRANCISCO CA", 16);
}

/********************************* Sfx_MergeRoll *****************************
 *
 * Create sale record with DocDate & SalePrice.  DocNum & Buyer will be populated
 * in MergeOwner().
 *
 * Return 0 if successful, >0 if error
 *
 *****************************************************************************/

int Sfx_MergeRoll(char *pOutbuf, char *pRollRec, int iLen, int iFlag)
{
   SFX_ROLL *pRec;
   char     acTmp[256], acTmp1[256], *pTmp;
   LONGLONG lTmp;
   int      iRet, iTmp;
   double   dTmp;

   pRec = (SFX_ROLL *)pRollRec;

   if (pRec->Apn[0] < '0')
      return 1;

#ifdef _DEBUG
   //if (!memcmp(pRec->Apn, "0695 007", 8))
   //   iRet = 0;
#endif

   if (iFlag & CREATE_R01)
   {
      // Clear output buffer
      memset(pOutbuf, ' ', iRecLen);

      // Start copying data
      memcpy(pOutbuf, pRec->Apn, RSIZ_APN);
      memcpy(pOutbuf+OFF_CO_NUM, "38SFX", 5);

      //*(pOutbuf+OFF_STATUS) = pRec->Status;

      // Format APN
      iRet = formatApn(pRec->Apn, acTmp, &myCounty);
      memcpy(pOutbuf+OFF_APN_D, acTmp, iRet);

      // Create MapLink
      iRet = formatMapLink(pRec->Apn, acTmp, &myCounty);
      memcpy(pOutbuf+OFF_MAPLINK, acTmp, iRet);

      // Create index map link
      if (getIndexPage(acTmp, acTmp1, &myCounty))
         memcpy(pOutbuf+OFF_IMAPLINK, acTmp1, iRet);

      // Year assessed
      memcpy(pOutbuf+OFF_YR_ASSD, myCounty.acYearAssd, 4);

      // HO Exempt
      long lHoe = atoin(pRec->HO_Exe, RSIZ_HO_EXE);
      if (lHoe > 0)
         *(pOutbuf+OFF_HO_FL) = '1';      // 'Y'
      else
         *(pOutbuf+OFF_HO_FL) = '2';      // 'N'

      // Other exempt
      memcpy(acTmp, pRec->Other_Exe, RSIZ_HO_EXE);
      acTmp[RSIZ_HO_EXE] = 0;
      LONGLONG lOthExe = strtoul(acTmp, &pTmp, 10);

      // Exemp total
      lTmp = lHoe+lOthExe;
      if (lTmp > 0)
      {
         sprintf(acTmp, "%*u", SIZ_EXE_TOTAL, lTmp);
         memcpy(pOutbuf+OFF_EXE_TOTAL, acTmp, SIZ_EXE_TOTAL);
         memcpy(pOutbuf+OFF_EXE_CD1, pRec->Exe_Code, RSIZ_OTHER_EXEMP_CODE);

         // Create exemption type
         pTmp = makeExeType(pOutbuf+OFF_EXE_TYPE, pOutbuf+OFF_EXE_CD1, pOutbuf+OFF_EXE_CD2, pOutbuf+OFF_EXE_CD3, (IDX_TBL4 *)&SFX_Exemption);
         if (!pTmp)
            LogMsg("*** Unknown ExeCode: %.2s, APN=%.10s", pRec->Exe_Code, pOutbuf);
      //} else
      //{
      //   lHoe = atoin(pRec->wy_HO_Exe, RSIZ_HO_EXE);
      //   lOthExe = atoln(pRec->wy_HO_Exe, RSIZ_HO_EXE);
      //   lTmp = lHoe+lOthExe;
      //   if (lTmp > 0)
      //   {
      //      sprintf(acTmp, "%*u", SIZ_EXE_TOTAL, lTmp);
      //      memcpy(pOutbuf+OFF_EXE_TOTAL, acTmp, SIZ_EXE_TOTAL);
      //      memcpy(pOutbuf+OFF_EXE_CD1, pRec->wy_Exe_Code, RSIZ_OTHER_EXEMP_CODE);
      //   }
      }

      // Land
      long lLand = atoin(pRec->Land, RSIZ_LAND);
      if (lLand > 0)
      {
         sprintf(acTmp, "%*u", SIZ_LAND, lLand);
         memcpy(pOutbuf+OFF_LAND, acTmp, SIZ_LAND);
      }

      // Improve
      long lImpr = atoin(pRec->Impr, RSIZ_LAND);
      if (lImpr > 0)
      {
         sprintf(acTmp, "%*u", SIZ_IMPR, lImpr);
         memcpy(pOutbuf+OFF_IMPR, acTmp, SIZ_IMPR);
      }

      // Other value
      long lFixture = atoin(pRec->Fixt_Val, RSIZ_LAND);
      long lPers = atoin(pRec->PP_Val, RSIZ_LAND);
      lTmp = lPers + lFixture;
      if (lTmp > 0)
      {
         sprintf(acTmp, "%*u", SIZ_OTHER, lTmp);
         memcpy(pOutbuf+OFF_OTHER, acTmp, SIZ_OTHER);

         if (lPers > 0)
         {
            sprintf(acTmp, "%u         ", lPers);
            memcpy(pOutbuf+OFF_PERSPROP, acTmp, SIZ_PERSPROP);
         }
         if (lFixture > 0)
         {
            sprintf(acTmp, "%u         ", lFixture);
            memcpy(pOutbuf+OFF_FIXTR, acTmp, SIZ_FIXTR);
         }
      }

      // Gross total
      lTmp += (LONGLONG)(lLand+lImpr);
      if (lTmp > 0)
      {
         sprintf(acTmp, "%*u", SIZ_GROSS, lTmp);
         memcpy(pOutbuf+OFF_GROSS, acTmp, SIZ_GROSS);

         // Ratio
         if (lImpr > 0)
         {
            //dTmp = (lImpr*100+50)/(lLand+lImpr);
            sprintf(acTmp, "%*d", SIZ_RATIO, ((LONGLONG)lImpr*100+50)/(lLand+lImpr));
            memcpy(pOutbuf+OFF_RATIO, acTmp, SIZ_RATIO);
         }

         if (lTmp > 2147483647)
            LogMsg("*** Big Total Value in [%.9s]: %u", pRec->Apn, lTmp);
      }
   }

   // TRA
   lTmp = atoin(pRec->TRA, RSIZ_TRA);
   if (lTmp > 0)
   {
      iRet = sprintf(acTmp, "%.6d", lTmp);
      memcpy(pOutbuf+OFF_TRA, acTmp, iRet);
   }

   // UseCode
   if (pRec->UseCode[0] > ' ')
   {
      memcpy(pOutbuf+OFF_USE_CO, pRec->UseCode, RSIZ_LAND_USE);
      updateStdUse(pOutbuf+OFF_USE_STD, pRec->UseCode, RSIZ_LAND_USE, pOutbuf);
   } else
      memcpy(pOutbuf+OFF_USE_STD, USE_UNASGN, SIZ_USE_STD);

   // Situs 
   iTmp = Sfx_MergeSAdr(pOutbuf, pRollRec);
   if (iTmp < 0)
      iBadSitus++;

   // Owner - Process in other place

   // Legal - not avail.

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "3082 018", 8))
   //   iTmp = 0;
#endif

      // Sales/Transfer - YYMMDD
   memset((void *)&sCurSale, 32, sizeof(SCSAL_EXT));
   memcpy(sCurSale.Apn, pRec->Apn, RSIZ_APN);
   strcpy(sCurSale.CRLF, "\n");
   lTmp = atoin(pRec->SaleDt, sizeof(pRec->SaleDt));
   if (lTmp > 0)
   {
      formDate(pRec->SaleDt, acTmp);
      memcpy(sCurSale.DocDate, acTmp, SIZ_TRANSFER_DT);

      lTmp = atoin(pRec->SaleAmt, RSIZ_CURRENT_SALE_AMOUNT);
      if (lTmp > 0)
      {
         sprintf(acTmp, "%*u", SALE_SIZ_SALEPRICE, lTmp);
         memcpy(sCurSale.SalePrice, acTmp, SALE_SIZ_SALEPRICE);
         sCurSale.DocType[0] = '1';
         sCurSale.DocCode[0] = pRec->SaleCode;
         sCurSale.ARCode = 'A';
#ifdef _DEBUG
         //if (pRec->SaleCode != 'S' && pRec->SaleCode != ' ')
         //   iTmp = 0;
#endif
      }
   }

   // ConstType
   /* Not use - see BldgClass 10/10/2013
   if (pRec->ConstType[0] > ' ')
   {
      iTmp = 0;
      while (asConstType[iTmp].iLen > 0)
      {
         if (!memcmp(pRec->ConstType, asConstType[iTmp].acSrc, asConstType[iTmp].iLen))
         {
            *(pOutbuf+OFF_CONS_TYPE) = asConstType[iTmp].acCode[0];
            break;
         }
         iTmp++;
      }
   }
   */

   // Zoning
   if (pRec->Zoning[0] > ' ')
   {
      memcpy(pOutbuf+OFF_ZONE, pRec->Zoning, RSIZ_ZONING);
      if (*(pOutbuf+OFF_ZONE_X1) == ' ')
         vmemcpy(pOutbuf+OFF_ZONE_X1, pRec->Zoning, SIZ_ZONE_X1, RSIZ_ZONING);
   }

   // Lot width x depth - not use

   // Lot Sqft - ignore the last 2 digits
   lTmp = atoin(pRec->LotSqft, RSIZ_LOT_SQFT-2);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*u", SIZ_LOT_SQFT, lTmp);
      memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);
      dTmp = (double)lTmp*ACRES_FACTOR/SQFT_PER_ACRE;
      if (dTmp > 0)
      {
         sprintf(acTmp, "%*u", SIZ_LOT_ACRES, (long)dTmp);
         memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);
      }
   }

   // Basement area 
   lTmp = atoin(pRec->BasemntArea, RSIZ_BASEMENT_AREA);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_BSMT_SF, lTmp);
      memcpy(pOutbuf+OFF_BSMT_SF, acTmp, SIZ_BSMT_SF);
   }

   // Bldgclass
   if (pRec->ConstType[0] >= 'A' || !memcmp(pRec->ConstType, "STE", 3) || !memcmp(pRec->ConstType, "WOO", 3))
      *(pOutbuf+OFF_BLDG_CLASS) = pRec->ConstType[0];
   else if (pRec->ConstType[0] > ' ')
   {
      LogMsg("*** Bad BldgClass: %.3s [%.9s]", pRec->ConstType, pRec->Apn);
      *(pOutbuf+OFF_BLDG_CLASS) = ' ';
   }

   // Stories
   iTmp = atoin(pRec->Stories, RSIZ_STORIES);
   if (iTmp > 0 && iTmp < 60)
   {
      if (bDebug && iTmp > 9)
         LogMsg("*** Please verify number of stories for %.*s (%d)", iApnLen, pOutbuf, iTmp);

      sprintf(acTmp, "%d.0  ", iTmp);
      memcpy(pOutbuf+OFF_STORIES, acTmp, SIZ_STORIES);
   }

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "0827 025", 8))
   //   iTmp = 0;
#endif

   // Units
   iTmp = atoin(pRec->Units, RSIZ_UNITS);
   if (iTmp > 0 && iTmp < 5000)
   {
      sprintf(acTmp, "%*d", SIZ_UNITS, iTmp);
      memcpy(pOutbuf+OFF_UNITS, acTmp, SIZ_UNITS);
   } else if (iTmp > 0)
      LogMsg("*** Bad number of Units: %u APN=%.9s", iTmp, pOutbuf);

   // Rooms
   iTmp = atoin(pRec->Rooms, RSIZ_TOTAL_ROOMS);
   if (iTmp > 0 && iTmp < 999)
   {
      sprintf(acTmp, "%*d", SIZ_ROOMS, iTmp);
      memcpy(pOutbuf+OFF_ROOMS, acTmp, SIZ_ROOMS);
   } else if (iTmp > 0)
      LogMsg("*** Bad number of Rooms: %u APN=%.9s", iTmp, pOutbuf);

   // Beds
   iTmp = atoin(pRec->Beds, RSIZ_BEDS);
   if (iTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_BEDS, iTmp);
      memcpy(pOutbuf+OFF_BEDS, acTmp, SIZ_BEDS);
   }

   // Baths
   iTmp = atoin(pRec->Baths, RSIZ_BATHS);
   if (iTmp > 0 && iTmp < 700)
   {
      sprintf(acTmp, "%*d", SIZ_BATH_F, iTmp);
      memcpy(pOutbuf+OFF_BATH_F, acTmp, SIZ_BATH_F);
   }

   // BldgSqft
   lTmp = atoin(pRec->BldgSqft, RSIZ_BLDG_SQFT);
   if (lTmp > 10)
   {
      sprintf(acTmp, "%*u", SIZ_BLDG_SF, lTmp);
      memcpy(pOutbuf+OFF_BLDG_SF, acTmp, SIZ_BLDG_SF);
   }

   // YrBlt
   iTmp = atoin(pRec->ConstYr, RSIZ_CONST_YEAR);
   if (iTmp > 1800 && iTmp != 1900)
   {
      sprintf(acTmp, "%d", iTmp);
      memcpy(pOutbuf+OFF_YR_BLT, acTmp, SIZ_YR_BLT);
   }

   return 0;
}

/********************************* Sfx_Load_Roll ******************************
 *
 *
 ******************************************************************************/

int Sfx_Load_Roll(int iSkip)
{
   char     *pTmp, acRec[MAX_RECSIZE], acBuf[MAX_RECSIZE], acRollRec[MAX_RECSIZE];
   char     acRawFile[_MAX_PATH], acOutFile[_MAX_PATH], acNewSaleFile[_MAX_PATH];

   HANDLE   fhIn, fhOut;

   int      iRet, iTmp, iRollUpd=0, iNewRec=0, iRetiredRec=0;
   DWORD    nBytesRead;
   DWORD    nBytesWritten;
   BOOL     bRet, bEof;
   long     lRet=0, lCnt=0;

   LogMsg0("Loading Roll update");

   sprintf(acRawFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "S01");
   sprintf(acOutFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "R01");

   // Rename files for processing
   iRet = 0;
   if (_access(acRawFile, 0))
   {
      if (!_access(acOutFile, 0))
         iRet = rename(acOutFile, acRawFile);
      else
      {
         LogMsg("***** Missing input file %s.  Please recheck!", acRawFile);
         iRet = -2;
      }
   }
   if (iRet < 0)
   {
      if (iRet == -1)
         LogMsg("***** Error renaming file %s (%d).  Please recheck!", acOutFile, _errno);
      return iRet;
   }

   // Open Roll file
   LogMsg("Open Roll file %s", acRollFile);
   fdRoll = fopen(acRollFile, "r");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", acRollFile);
      return -2;
   }

   // Open Owner file
   LogMsg("Open Owner file %s", acOwnerFile);
   if (!iOwnerLen)
      fdOwner = fopen(acOwnerFile, "r");
   else
      fdOwner = fopen(acOwnerFile, "rb");

   if (fdOwner == NULL)
   {
      LogMsg("***** Error opening Owner file: %s\n", acOwnerFile);
      return -2;
   }

   // Open Mail address file
   LogMsg("Open Address file %s", acAdrFile);
   fdMAdr = fopen(acAdrFile, "r");
   if (fdMAdr == NULL)
   {
      LogMsg("***** Error opening address file: %s\n", acAdrFile);
      return -2;
   }

   // New Cumulative Sale file
   GetIniString("Data", "SaleExp", "", acESalTmpl, _MAX_PATH, acIniFile);
   sprintf(acNewSaleFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "Dat");
   LogMsg("Create new Cumulative Sales file %s", acNewSaleFile);
   if (!(fdNewSale = fopen(acNewSaleFile, "w")))
   {
      LogMsg("***** Error creating new Cumulative Sales file: %s\n", acNewSaleFile);
      return -2;
   }

   // Open Input file
   LogMsg("Open input file %s", acRawFile);
   fhIn = CreateFile(acRawFile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhIn == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error Opening input file %s", acRawFile);
      return -3;
   }

   // Open Output file
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
      return -4;

   // Get first RollRec
   pTmp = fgets((char *)&acRollRec[0], MAX_RECSIZE, fdRoll);
   bEof = (pTmp ? false:true);

   // Copy skip record
   memset(acBuf, ' ', iRecLen);
   while (iSkip-- > 0)
   {
      ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);
      // Ignore record start with 99999999
      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
   }

   iNoMatch=iBadCity=iBadSuffix=0;

   // Merge loop
   while (!bEof)
   {
      bRet = ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);

      // Check for EOF
      if (!bRet)
      {
         LogMsg("***** Error reading input file %s (%f)", acRawFile, GetLastError());
         break;
      }

      // EOF ?
      if (iRecLen != nBytesRead)
         break;

      NextRollRec:
#ifdef _DEBUG
      //if (!memcmp(acBuf, "3082 018", 8))
      //   iTmp = 0;
#endif
      iTmp = memcmp(acBuf, (char *)&acRollRec[ROFF_APN], iApnLen);
      if (!iTmp)
      {
         // Merge roll data
         iRet = Sfx_MergeRoll(acBuf, acRollRec, iRollLen, UPDATE_R01);
         if (!iRet)
         {
            iRollUpd++;

            // Merge mailing addr
            if (fdMAdr)
               iRet = Sfx_MergeMAdr(acBuf);

            // Merge Owner
            if (fdOwner)
               iRet = Sfx_MatchOwner(acBuf);

            // Output sale record
            if (sCurSale.DocDate[0] > ' ')
               fputs(&sCurSale.Apn[0], fdNewSale);
         } else
            LogMsg("*** Drop rec: %.40s", acRollRec);
            
         // Read next roll record
         pTmp = fgets(acRollRec, MAX_RECSIZE, fdRoll);

         if (!pTmp)
            bEof = true;    // Signal to stop
      } else if (iTmp > 0)  // Roll not match, new roll record?
      {
         if (bDebug)
            LogMsg0("*** New roll record : %.*s (%d) ***", iApnLen, (char *)&acRollRec[ROFF_APN], lCnt);

         // Create new R01 record
         iRet = Sfx_MergeRoll(acRec, acRollRec, iRollLen, CREATE_R01);
         if (!iRet)
         {
            iNewRec++;

            // Merge mailing addr
            if (fdMAdr)
               iRet = Sfx_MergeMAdr(acRec);

            // Merge Owner
            if (fdOwner)
               iRet = Sfx_MatchOwner(acRec);

            // Update Sales
            if (sCurSale.DocDate[0] > ' ')
               fputs(&sCurSale.Apn[0], fdNewSale);

            bRet = WriteFile(fhOut, acRec, iRecLen, &nBytesWritten, NULL);
            lCnt++;
         } else
            LogMsg("*** Drop rec: %.40s", acRollRec);

         // Get next roll record
         pTmp = fgets(acRollRec, MAX_RECSIZE, fdRoll);

         if (!pTmp)
            bEof = true;    // Signal to stop
         else
            goto NextRollRec;
      } else
      {
         // Record may be retired
         if (bDebug)
            LogMsg0("*** Roll not match (retired record?) : R01->%.*s < Roll->%.*s (%d) ***",
            iApnLen, acBuf, iApnLen, (char *)&acRollRec[ROFF_APN], lCnt);
         iRetiredRec++;

         continue;
      }

      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);

      if (!bRet)
      {
         LogMsg("Error occurs: %d\n", GetLastError());
         lRet = WRITE_ERR;
         break;
      }
   }

   // Do the rest of the file
   while (!bEof)
   {
      if (bDebug)
         LogMsg0("*** New roll record : %.*s (%d) ***", iApnLen, (char *)&acRollRec[ROFF_APN], lCnt);

      // Create new R01 record
      iRet = Sfx_MergeRoll(acRec, acRollRec, iRollLen, CREATE_R01);
      if (!iRet)
      {
         iNewRec++;

         // Merge mailing addr
         if (fdMAdr)
            iRet = Sfx_MergeMAdr(acRec);

         // Merge Owner
         if (fdOwner)
            iRet = Sfx_MatchOwner(acRec);

         // Update Sales
         if (sCurSale.DocDate[0] > ' ')
            fputs(&sCurSale.Apn[0], fdNewSale);

         bRet = WriteFile(fhOut, acRec, iRecLen, &nBytesWritten, NULL);
         lCnt++;
      } else
         LogMsg("*** Drop rec: %.40s", acRollRec);

      // Get next roll record
      pTmp = fgets(acRollRec, MAX_RECSIZE, fdRoll);

      if (!pTmp)
         break;
   }

   // Close files
   if (fdRoll)
      fclose(fdRoll);
   if (fdOwner)
      fclose(fdOwner);
   if (fdMAdr)
      fclose(fdMAdr);
   if (fdNewSale)
      fclose(fdNewSale);

   if (fhOut)
      CloseHandle(fhOut);
   if (fhIn)
      CloseHandle(fhIn);

   // Append new sales to cum sale file
   sprintf(acOutFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "srt");
   sprintf(acCSalFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "sls");
   sprintf(acBuf, "%s+%s", acNewSaleFile, acCSalFile);
   // Sort/merge sale file - APN (asc), Saledate (asc), DocNum (asc)
   sprintf(acRec, "S(1,%d,C,A,27,8,C,A,15,12,C,D,57,10,C,D,507,1,C,D) OMIT(15,1,C,EQ,\" \") DUPO(B2048,1,%d,27,8,15,12)", iApnLen, iApnLen);
   lRet = sortFile(acBuf, acOutFile, acRec);
   if (lRet > 0)
   {
      // Rename old cum sale file
      sprintf(acBuf, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "sav");
      if (!_access(acBuf, 0))
         DeleteFile(acBuf);
      iRet = rename(acCSalFile, acBuf);
      if (iRet)
      {
         LogMsg("***** Error renaming %s to %s: %d", acCSalFile, acBuf, errno);
      } else
      {
         LogMsg("Rename %s to %s", acOutFile, acCSalFile);
         iRet = rename(acOutFile, acCSalFile);
      }
   } else
      iRet = -1;

   LogMsg("Total output records:       %u", lCnt);
   LogMsg("Total new records:          %u", iNewRec);
   LogMsg("Total retired records:      %u", iRetiredRec);
   LogMsg("Total bad-situs records:    %u", iBadSitus);
   LogMsg("Total bad-city records:     %u", iBadCity);
   LogMsg("Total bad-suffix records:   %u\n", iBadSuffix);

   printf("\nTotal output records: %u\n", lCnt);

   lRecCnt = lCnt;
   return iRet;
}

/********************************* Sfx_Load_LDR *****************************
 *
 *
 ****************************************************************************/

int Sfx_Load_LDR(int iFirstRec /* 1=create header rec */)
{
   char     *pTmp, acBuf[MAX_RECSIZE], acRollRec[MAX_RECSIZE];
   char     acOutFile[_MAX_PATH], acNewSaleFile[_MAX_PATH];

   HANDLE   fhOut;
   DWORD    nBytesWritten;
   BOOL     bRet;
   long     lRet=0, lCnt=0, iRet;

   LogMsg0("Loading LDR data");

   // Open roll file
   LogMsg("Open lien file %s", acRollFile);
   fdRoll = fopen(acRollFile, "r");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", acRollFile);
      return 2;
   }

   // Open Owner file
   LogMsg("Open Owner file %s", acOwnerFile);
   fdOwner = fopen(acOwnerFile, "r");
   if (fdOwner == NULL)
   {
      LogMsg("***** Error opening Owner file: %s\n", acOwnerFile);
      return 2;
   }

   // Open Mail address file
   LogMsg("Open Mail Address file %s", acAdrFile);
   fdMAdr = fopen(acAdrFile, "r");
   if (fdMAdr == NULL)
   {
      LogMsg("***** Error opening mail address file: %s\n", acAdrFile);
      return 2;
   }

   // New Cumulative Sale file
   sprintf(acNewSaleFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "Dat");
   LogMsg("Create new Cumulative Sales file %s", acNewSaleFile);
   if (!(fdNewSale = fopen(acNewSaleFile, "w")))
   {
      LogMsg("***** Error creating new Cumulative Sales file: %s\n", acNewSaleFile);
      return -2;
   }

   // Open Output file
   sprintf(acOutFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "R01");
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error creating output file: %s\n", acOutFile);
      return -4;
   }

   // Output first header record
   if (iFirstRec > 0)
   {
      memset(acBuf, '9', iRecLen);
      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
   }

   // Init variables
   iNoMatch=iBadCity=iBadSuffix=0;

   // Merge loop
   while (!feof(fdRoll))
   {
      pTmp = fgets(acRollRec, MAX_RECSIZE, fdRoll);
      if (!pTmp)
         break;

      // Create new R01 record
      iRet = Sfx_MergeRoll(acBuf, acRollRec, iRollLen, CREATE_R01|CREATE_LIEN);
      if (!iRet)
      {
         // Merge mailing addr
         if (fdMAdr)
            iRet = Sfx_MergeMAdr(acBuf);

#ifdef _DEBUG
      //if (!memcmp(acBuf, "0012 007", 8))
      //   iRet = 0;
#endif

         // Merge Owner
         if (fdOwner)
            iRet = Sfx_MatchOwner(acBuf);

         // Output sale record
         if (sCurSale.DocDate[0] > ' ')
            fputs(&sCurSale.Apn[0], fdNewSale);

         lLDRRecCount++;
         bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
      } else
         LogMsg("*** Drop rec: %.40s", acRollRec);

      if (!bRet)
      {
         LogMsg("Error writing to output file at record %d\n", lCnt);
         lRet = WRITE_ERR;
         break;
      }
      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdRoll)
      fclose(fdRoll);
   if (fdOwner)
      fclose(fdOwner);
   if (fdMAdr)
      fclose(fdMAdr);
   if (fdNewSale)
      fclose(fdNewSale);

   if (fhOut)
      CloseHandle(fhOut);

   // Append new sales to cum sale file
   //sprintf(acCSalFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "sls");
   sprintf(acOutFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "srt");
   sprintf(acBuf, "%s+%s", acNewSaleFile, acCSalFile);
   // Sort/merge sale file - APN (asc), Saledate (asc), DocNum (asc)
   sprintf(acRollRec, "S(1,%d,C,A,27,8,C,A,15,%d,C,A,57,10,C,D) DUPO(B2048,1,34)", iApnLen, SALE_SIZ_DOCNUM);
   lRet = sortFile(acBuf, acOutFile, acRollRec);
   if (lRet > 0)
   {
      // Rename old cum sale file
      sprintf(acBuf, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "sav");
      if (!_access(acBuf, 0))
         DeleteFile(acBuf);
      iRet = rename(acCSalFile, acBuf);
      if (iRet)
      {
         LogMsg("***** Error renaming %s to %s: %d", acCSalFile, acBuf, errno);
      } else
      {
         iRet = rename(acOutFile, acCSalFile);
      }
   } else
      iRet = -1;

   LogMsg("Total input records:        %u", lCnt);
   LogMsg("Total output records:       %u", lLDRRecCount);
   LogMsg("Total bad-city records:     %u", iBadCity);
   LogMsg("Total bad-suffix records:   %u\n", iBadSuffix);
   printf("\nTotal output records: %u\n", lLDRRecCount);

   lRecCnt = lLDRRecCount;
   return 0;
}

/******************************* Sfx_CreateLienRec ***************************
 *
 * Return 0 if successful, >0 if error
 *
 *****************************************************************************/

int Sfx_CreateLienRec(char *pOutbuf, char *pRollRec)
{
   char     acTmp[256], *pTmp;
   int      iTmp;
   SFX_ROLL *pRec = (SFX_ROLL *)pRollRec;
   LIENEXTR *pLienRec = (LIENEXTR *)pOutbuf;
   LONGLONG lTmp;

   if (pRec->Apn[0] < '0')
      return 1;

   // Clear output buffer
   memset(pOutbuf, ' ', sizeof(LIENEXTR));

   // Start copying data
   memcpy(pLienRec->acApn, pRec->Apn, RSIZ_APN);

   // TRA
   lTmp = atoin(pRec->TRA, RSIZ_TRA);
   if (lTmp > 0)
   {
      iTmp = sprintf(acTmp, "%.6d", lTmp);
      memcpy(pLienRec->acTRA, acTmp, iTmp);
   }

   // Year assessed
   memcpy(pLienRec->acYear, myCounty.acYearAssd, 4);

   // Land
   long lLand = atoin(pRec->Land, RSIZ_LAND);
   if (lLand > 0)
   {
      iTmp = sprintf(acTmp, "%*u", sizeof(pLienRec->acLand), lLand);
      memcpy(pLienRec->acLand, acTmp, iTmp);
   }

   // Improve - Ratio
   long lImpr = atoin(pRec->Impr, RSIZ_LAND);
   if (lImpr > 0)
   {
      iTmp = sprintf(acTmp, "%*u", sizeof(pLienRec->acImpr), lImpr);
      memcpy(pLienRec->acImpr, acTmp, iTmp);

      iTmp = sprintf(acTmp, "%*u", sizeof(pLienRec->acRatio), (LONGLONG)lImpr*100/(lLand+lImpr));
      memcpy(pLienRec->acRatio, acTmp, iTmp);
   }

   // Pers Prop
   long lPers = atoin(pRec->PP_Val, RSIZ_LAND);

   // Fixed Equitment
   long lFixt = atoin(pRec->Fixt_Val, RSIZ_LAND);

   // Total other
   lTmp = lPers + lFixt;
   if (lTmp > 0)
   {
      iTmp = sprintf(acTmp, "%*u", sizeof(pLienRec->acOther), lTmp);
      memcpy(pLienRec->acOther, acTmp, iTmp);

      if (lFixt > 0)
      {
         iTmp = sprintf(acTmp, "%*u", sizeof(pLienRec->acME_Val), lFixt);
         memcpy(pLienRec->acME_Val, acTmp, iTmp);
      }
      if (lPers > 0)
      {
         iTmp = sprintf(acTmp, "%*u", sizeof(pLienRec->acPP_Val), lPers);
         memcpy(pLienRec->acPP_Val, acTmp, iTmp);
      }
   }

   // Gross
   lTmp += (lLand + lImpr);
   if (lTmp > 0)
   {
      iTmp = sprintf(acTmp, "%*u", sizeof(pLienRec->acGross), lTmp);
      memcpy(pLienRec->acGross, acTmp, iTmp);
   }

   // HO Exempt
   long lExe = atoin(pRec->HO_Exe, RSIZ_HO_EXE);
   if (lExe > 0)
   {
      pLienRec->acHO[0] = '1';      // 'Y'
      iTmp = sprintf(acTmp, "%*u", sizeof(pLienRec->extra.Sfx.HO_Exe), lExe);
      memcpy(pLienRec->extra.Sfx.HO_Exe, acTmp, iTmp);
   } else
      pLienRec->acHO[0] = '2';      // 'N'

#ifdef _DEBUG
   //if (!memcmp(pRec->Apn, "0695 007", 8))
   //   iTmp = 0;
#endif

   // Other exempt
   //long lOthExe = atoin(pRec->Other_Exe, RSIZ_HO_EXE);
   memcpy(acTmp, pRec->Other_Exe, RSIZ_HO_EXE);
   acTmp[RSIZ_HO_EXE] = 0;
   LONGLONG lOthExe = strtoul(acTmp, &pTmp, 10);
   if (lOthExe > 0)
   {
      iTmp = sprintf(acTmp, "%*u", sizeof(pLienRec->extra.Sfx.Misc_Exe), lOthExe);
      memcpy(pLienRec->extra.Sfx.Misc_Exe, acTmp, iTmp);
   }

   // Exemp total
   lTmp = lExe+lOthExe;
   if (lTmp > 0)
   {
      iTmp = sprintf(acTmp, "%*u", sizeof(pLienRec->acExAmt), lTmp);
      memcpy(pLienRec->acExAmt, acTmp, iTmp);

      // Exemption code
      memcpy(pLienRec->acExCode, pRec->Exe_Code, SIZ_LIEN_EXECODE);
   }

   pLienRec->extra.Sfx.Roll_Status = pRec->Status;

   pLienRec->LF[0] = '\n';
   pLienRec->LF[1] = 0;
   return 0;
}

/********************************* Sfx_ExtrLien *****************************
 *
 *
 ****************************************************************************/

int Sfx_ExtrLien()
{
   char  *pTmp, acBuf[MAX_RECSIZE], acRollRec[MAX_RECSIZE], acOutFile[_MAX_PATH];
   FILE  *fdLien;
   long  lCnt=0, iRet;

   // Open roll file
   LogMsg("Open Roll file %s", acRollFile);
   fdRoll = fopen(acRollFile, "r");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", acRollFile);
      return 2;
   }

   // Open Output file
   sprintf(acOutFile, acLienTmpl, myCounty.acCntyCode, myCounty.acCntyCode);
   LogMsg("Open output file %s", acOutFile);
   fdLien = fopen(acOutFile, "w");
   if (fdLien == NULL)
   {
      LogMsg("***** Error opening Lien extract file: %s\n", acOutFile);
      return 4;
   }

   // Merge loop
   while (!feof(fdRoll))
   {
      // Get roll rec
      pTmp = fgets(acRollRec, MAX_RECSIZE, fdRoll);
      if (!pTmp)
         break;

      // Create new lien record
      iRet = Sfx_CreateLienRec(acBuf, acRollRec);
      if (!iRet)
      {
         fputs(acBuf, fdLien);
         lRecCnt++;
      }
      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdRoll)
      fclose(fdRoll);
   if (fdLien)
      fclose(fdLien);

   LogMsgD("Total records output:     %d\n", lRecCnt);

   return 0;
}

/****************************** Sfx_ConvertSale *****************************
 *
 *
 ****************************************************************************/

int Sfx_ConvertSale(char *pCnty, char *pInfile)
{
   char     acInbuf[1024], acOutbuf[1024];
   char     acOutFile[_MAX_PATH], acSrtFile[_MAX_PATH], acTmp[256];
   long     lCnt=0, lOut=0, iRet, lTmp;
   FILE     *fdOut;

   SFX_CSAL  *pSale  = (SFX_CSAL *)&acInbuf[0];
   SCSAL_REC *pCSale = (SCSAL_REC *)&acOutbuf[0];
   SFX_CSAL  sLstSale;

   if (_access(pInfile, 0))
   {
      LogMsg("***** Sfx_ConvertSale(): Missing input file: %s", pInfile);
      return -1;
   }

   sprintf(acOutFile, acESalTmpl, pCnty, pCnty, "Tmp");
   LogMsg("Convert %s to %s.", pInfile, acOutFile);

   // Open input file
   LogMsg("Open input sale file %s", pInfile);
   fdSale = fopen(pInfile, "rb");
   if (fdSale == NULL)
   {
      LogMsg("***** Error opening sale file: %s\n", pInfile);
      return -2;
   }

   // Open output file
   LogMsg("Create output sale file %s", acOutFile);
   fdOut = fopen(acOutFile, "w");
   if (fdOut == NULL)
   {
      LogMsg("***** Error creating output sale file: %s\n", acOutFile);
      return -3;
   }

   // Convert loop
   while (!feof(fdSale))
   {
      if (45 != fread(acInbuf, 1, 45, fdSale))
         break;


      if (memcmp(sLstSale.Apn, pSale->Apn, SSIZ_APN) ||
         (!memcmp(sLstSale.Apn, pSale->Apn, SSIZ_APN) && memcmp(sLstSale.RecDate, pSale->RecDate, 8)) )
      {
         // Save it
         memcpy(&sLstSale.Apn[0], acInbuf, 45);

         // Reformat sale
         memset(acOutbuf, ' ', sizeof(SCSAL_REC));

         memcpy(pCSale->Apn, pSale->Apn, SSIZ_APN);
         memcpy(pCSale->DocDate, pSale->RecDate, SSIZ_REC_DATE);

         if (pSale->RecNum[0] > ' ')
         {        
            memcpy(pCSale->DocNum, pSale->RecNum, 4);
            //pCSale->DocNum[4] = '-';
            //memcpy(&pCSale->DocNum[5], &pSale->RecNum[4], 4);
            memcpy(&pCSale->DocNum[5], &pSale->RecNum[5], 3);
         }

         lTmp = atoin(pSale->SalePrice, SSIZ_SALE_PRICE);
         if (lTmp > 0)
         {
            sprintf(acTmp, "%*u", SALE_SIZ_SALEPRICE, lTmp);
            memcpy(pCSale->SalePrice, acTmp, SALE_SIZ_SALEPRICE);
            pCSale->DocType[0] = '1';
         }

         // Write to output file
         pCSale->CRLF[0] = '\n';
         pCSale->CRLF[1] = 0;

         fputs(acOutbuf, fdOut);
         lOut++;
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdSale)
      fclose(fdSale);
   if (fdOut)
      fclose(fdOut);

   // Sort sale file - APN (asc), Saledate (asc), DocNum (asc), Sale price (desc)
   sprintf(acInbuf, "S(1,%d,C,A,27,8,C,A,15,12,C,A,57,10,C,D) DUPO(1,34)", SSIZ_APN);
   sprintf(acSrtFile, acESalTmpl, pCnty, pCnty, "sls");
   iRet = sortFile(acOutFile, acSrtFile, acInbuf);
   if (!iRet)
      iRet = 1;
   else
      iRet = 0;

   LogMsg("Total input records:        %u", lCnt);
   LogMsg("Total output records:       %u", lOut);
   LogMsg("Last recording date:        %u", lLastRecDate);

   printf("\nTotal output records: %u\n", lCnt);
   return iRet;
}

/****************************** Sfx_ParseTaxBase ******************************
 *
 * Create TAXBASE record
 * The 2 input records are installment 1 & 2.  We have to combine them into one base record.
 *
 ******************************************************************************/

int Sfx_ParseTaxBase(char *pOutbuf, char *pRec1, char *pRec2)
{
   char     *apItem1[50], *apItem2[50], *pTmp;
   int      iTem1, iTem2;
   double   dTaxAmt1, dTaxAmt2, dPaidAmt1, dPaidAmt2, dTotalDue, dTotalTax;

   TAXBASE  *pTaxBase = (TAXBASE *)pOutbuf;

   iTem1 = ParseStringNQ(pRec1, ',', 50, apItem1);
   if (iTem1 < TDS_FLDS)
   {
      LogMsg("***** Parse TDS -->Bad tax record 1 (%d)", iTem1);
      return -1;
   }
   iTem2 = ParseStringNQ(pRec2, ',', 50, apItem2);
   if (iTem2 < TDS_FLDS)
   {
      LogMsg("***** Parse TDS -->Bad tax record 2 (%d)", iTem2);
      return -1;
   }

   if  (strcmp(apItem1[TDS_APN], apItem2[TDS_APN]))
   {
      LogMsg("***** Mismatch APN: %s <> %s", apItem1[TDS_APN], apItem2[TDS_APN]);
      return 1;
   }

   // Init buffer
   memset(pOutbuf, 0, sizeof(TAXBASE));

   // Remove hyphen from APN
   if (*(apItem1[TDS_APN]+4) == '-')
      replChar(apItem1[TDS_APN], '-', ' ');
   else
      remChar(apItem1[TDS_APN], '-');
   strcpy(pTaxBase->Apn, apItem1[TDS_APN]);

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "0884510314", 10))
   //   iRet = 0;
#endif

   dTotalDue=dPaidAmt1=dPaidAmt2 = 0.0;

   // Tax Year
   iTaxYear = atol(apItem1[TDS_ASMT_YR]);
   if (iTaxYear != lTaxYear)
   {
      LogMsg("*** Drop rec due to bad tax year: APN=%s, TaxYear=%s", pTaxBase->Apn, apItem1[TDS_ASMT_YR]);
      return iTaxYear;
   }
   strcpy(pTaxBase->TaxYear, apItem1[TDS_ASMT_YR]);

   // BillNum
   strcpy(pTaxBase->BillNum, apItem1[TDS_BILL_NO]);
   dTaxAmt1 = atof(apItem1[TDS_TAX_AMT]);
   dTaxAmt2 = atof(apItem2[TDS_TAX_AMT]);
   dTotalTax = dTaxAmt1+dTaxAmt2;

   // Installment 1
   if (*apItem1[TDS_INST_NO] == '1')
   {
      // Tax Amt
      if (dTaxAmt1 > 0.0)
      {
         strcpy(pTaxBase->TaxAmt1, apItem1[TDS_TAX_AMT]);

         // Due date
         pTmp = dateConversion(apItem1[TDS_DUE_DATE], pTaxBase->DueDate1, MM_DD_YYYY_1);
         if (!pTmp)
            LogMsg("*** Invalid due date1: APN=%s, DueDate1=%s", pTaxBase->Apn, pTaxBase->DueDate1);

         if (*apItem1[TDS_PAID_STATUS] == 'P')
         {
            pTaxBase->Inst1Status[0] = TAX_BSTAT_PAID;
            dPaidAmt1 = dTaxAmt1;

            pTmp = dateConversion(apItem1[TDS_PAID_DATE], pTaxBase->PaidDate1, MM_DD_YYYY_1);
            if (!pTmp)
               LogMsg("*** Invalid paid date1: APN=%s, PaidDate1=%s", apItem1[TDS_APN], apItem1[TDS_PAID_DATE]);
         } else if (*apItem1[TDS_PAID_STATUS] == 'U')
            pTaxBase->Inst1Status[0] = TAX_BSTAT_UNPAID;
         else
            LogMsg("*** Unknown paid status1: %s [%s]", apItem1[TDS_PAID_STATUS], apItem1[TDS_APN]);
      } else
            pTaxBase->Inst1Status[0] = TAX_BSTAT_NOTAX;
   } else
      LogMsg("*** Installment #1 out of sync APN=%s", apItem1[TDS_APN]);

   // Installment 2
   if (*apItem2[TDS_INST_NO] == '2')
   {
      if (dTaxAmt2 > 0.0)
      {
         strcpy(pTaxBase->TaxAmt2, apItem2[TDS_TAX_AMT]);
         pTmp = dateConversion(apItem2[TDS_DUE_DATE], pTaxBase->DueDate2, MM_DD_YYYY_1, lLienYear+1);
         if (!pTmp)
            LogMsg("*** Invalid due date2: APN=%s, DueDate2=%s", pTaxBase->Apn, apItem2[TDS_DUE_DATE]);

         if (*apItem2[TDS_PAID_STATUS] == 'P')
         {
            pTaxBase->Inst2Status[0] = TAX_BSTAT_PAID;
            dPaidAmt2 = dTaxAmt2;

            pTmp = dateConversion(apItem2[TDS_PAID_DATE], pTaxBase->PaidDate2, MM_DD_YYYY_1);
            if (!pTmp)
               LogMsg("*** Invalid paid date2: APN=%s PaidDate2=%s", apItem2[TDS_APN], apItem2[TDS_PAID_DATE]);
         } else if (*apItem2[TDS_PAID_STATUS] == 'U')
            pTaxBase->Inst2Status[0] = TAX_BSTAT_UNPAID;
         else
            LogMsg("*** Unknown paid status2: %s [%s]", apItem2[TDS_PAID_STATUS], apItem2[TDS_APN]);
      } else
         pTaxBase->Inst2Status[0] = TAX_BSTAT_NOTAX;
   } else
      LogMsg("*** Installment #2 out of sync APN=%s", apItem2[TDS_APN]);

   // Total tax amount
   sprintf(pTaxBase->TotalTaxAmt, "%.2f", dTotalTax);

   // Total due amount
   double dDueAmt2, dDueAmt1;
   dDueAmt1 = atof(apItem1[TDS_DUE_AMT]);
   dDueAmt2 = atof(apItem2[TDS_DUE_AMT]);
   dTotalDue = dDueAmt1 + dDueAmt2;
   sprintf(pTaxBase->TotalDue, "%.2f", dTotalDue);

   if (dPaidAmt1 > 0.0)
      sprintf(pTaxBase->PaidAmt1, "%.2f", dPaidAmt1);
   if (dPaidAmt2 > 0.0)
      sprintf(pTaxBase->PaidAmt2, "%.2f", dPaidAmt2);

   // Penalty amount
   double dPenAmt2, dPenAmt1;
   dPenAmt1 = atof(apItem1[TDS_PEN_AMT]);
   if (dPenAmt1 > 0.0)
      sprintf(pTaxBase->PenAmt1, "%.2f", dPenAmt1);
   dPenAmt2 = atof(apItem2[TDS_PEN_AMT]);
   if (dPenAmt2 > 0.0)
      sprintf(pTaxBase->PenAmt2, "%.2f", dPenAmt2);

   pTaxBase->BillType[0] = BILLTYPE_SECURED;
   pTaxBase->isSecd[0] = '1';
   pTaxBase->isSupp[0] = '0';

   return 0;
}

/******************************* Sfx_Load_TaxBase ****************************
 *
 * Create import file and import into Sfx_Tax_Base table
 *
 * Return 0 if success.
 *
 *****************************************************************************/

int Sfx_Load_TaxBase(bool bImport, int iSkipHdr)
{
   char     *pTmp, acBuf[MAX_RECSIZE], acRec1[MAX_RECSIZE], acRec2[MAX_RECSIZE];
   char     acBaseFile[_MAX_PATH], acInFile[_MAX_PATH];

   int      iRet;
   long     lOut=0, lCnt=0;
   FILE     *fdIn, *fdBase, *fdR01;
   TAXBASE  *pTaxBase = (TAXBASE *)&acBuf[0];

   LogMsg0("Loading tax file");

   GetIniString(myCounty.acCntyCode, "TaxRollInfo", "", acInFile, _MAX_PATH, acIniFile);
   NameTaxCsvFile(acBaseFile, myCounty.acCntyCode, "Base");
   lLastTaxFileDate = getFileDate(acInFile);

   // Only process if new tax file
   iRet = isNewTaxFile(acInFile, myCounty.acCntyCode);
   if (iRet <= 0)
   {
      lLastTaxFileDate = 0;
      return iRet;
   }

   // Open input file
   LogMsg("Open Tax file %s", acInFile);
   fdIn = fopen(acInFile, "r");
   if (fdIn == NULL)
   {
      LogMsg("***** Error opening Tax file: %s\n", acInFile);
      return -2;
   }  

   // Open Output file
   LogMsg("Open output file %s", acBaseFile);
   fdBase = fopen(acBaseFile, "w");
   if (fdBase == NULL)
   {
      LogMsg("***** Error creating output file: %s\n", acBaseFile);
      return -4;
   }

   // Open raw file to update TRA
   fdR01 = OpenR01(myCounty.acCntyCode);
   iApnMatch = 0;

   // Skip header record
   for (iRet = 0; iRet < iSkipHdr; iRet++)
      pTmp = fgets((char *)&acRec1[0], MAX_RECSIZE, fdIn);

   // Start loop 
   while (!feof(fdIn))
   {
      if (!(pTmp = fgets((char *)&acRec1[0], MAX_RECSIZE, fdIn)))
         break;
      if (!(pTmp = fgets((char *)&acRec2[0], MAX_RECSIZE, fdIn)))
         break;

      // Create new TAXBASE record
      iRet = Sfx_ParseTaxBase(acBuf, acRec1, acRec2);
      if (!iRet)
      {
         // Update TRA
         if (fdR01)
            UpdateTRA(acBuf, fdR01);

         // Create delimited record
         Tax_CreateTaxBaseCsv(acRec1, (TAXBASE *)&acBuf);
         fputs(acRec1, fdBase);
         lOut++;
      } else 
         LogMsg("---> Load_TaxBase: drop record %d [%.40s]", lCnt, acRec1); 

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdIn)
      fclose(fdIn);
   if (fdR01)
      fclose(fdR01);
   if (fdBase)
      fclose(fdBase);

   LogMsg("Total records processed:       %u", lCnt);
   LogMsg("Total output records:          %u", lOut);
   LogMsg("Total APN matched records:     %u", iApnMatch);
   printf("\nTotal output records: %u", lCnt);

   // Import into SQL
   if (bImport)
      iRet = doTaxImport(myCounty.acCntyCode, TAX_BASE);
   else
      iRet = 0;

   return iRet;
}

/**************************** Sfx_Load_TaxDetail ******************************
 *
 * Return 0 if success.
 *
 *****************************************************************************/

int Sfx_ParseTaxDetail(char *pItems, char *pAgency, char *pInbuf)
{
   char     *apItems[32];
   int      iTems;

   TAXDETAIL  *pItemsRec  = (TAXDETAIL *)pItems;
   TAXAGENCY  *pAgencyRec = (TAXAGENCY *)pAgency;
   TAXAGENCY  *pResult;
   double      dTmp;

   // Clear output buffer
   memset(pItems, 0, sizeof(TAXDETAIL));
   memset(pAgency, 0, sizeof(TAXAGENCY));

   iTems = ParseStringNQ(pInbuf, ',', 32, apItems);
   if (iTems < TSA_FLDS)
   {
      LogMsg("***** ParsePTS -->Bad tax record 1 (%d)", iTems);
      return -1;
   }

   // APN
   if (*(apItems[TSA_APN]+4) == '-')
      replChar(apItems[TSA_APN], '-', ' ');
   else
      remChar(apItems[TSA_APN], '-');
   strcpy(pItemsRec->Apn, apItems[TSA_APN]);

   // Tax Year
   strcpy(pItemsRec->TaxYear, apItems[TSA_ROLL_YR]);

   // BillNum
   strcpy(pItemsRec->BillNum, apItems[TSA_BILL_NO]);

   // Tax Desc
   if (*apItems[TSA_DIST_CODE] > ' ')
   {
      strcpy(pItemsRec->TaxDesc, apItems[TSA_DIST_NAME]);
      strcpy(pAgencyRec->Agency, pItemsRec->TaxDesc);

      // Tax Desc
      pResult = findTaxAgency(apItems[TSA_DIST_CODE]);
      if (pResult)
      {
         strcpy(pAgencyRec->Code, pResult->Code);
         strcpy(pAgencyRec->Agency,pResult->Agency);
         strcpy(pAgencyRec->Phone, pResult->Phone);
         strcpy(pItemsRec->TaxCode, pResult->Code);
         pAgencyRec->TC_Flag[0] = pResult->TC_Flag[0];
         pItemsRec->TC_Flag[0] = pResult->TC_Flag[0];
      } else
      {
         LogMsg("+++ Unknown Agency: %s [%s]", pItemsRec->TaxDesc, apItems[TSA_APN]);
         strcpy(pItemsRec->TaxCode, apItems[TSA_DIST_CODE]);
         strcpy(pAgencyRec->Code, apItems[TSA_DIST_CODE]);
      }
   } 

   // Tax Rate

   // TRA
   //strcpy(pItemsRec->Assmnt_No, apItems[TSA_TRA_CODE]);
   //remChar(pItemsRec->Assmnt_No, '-');

   // Tax Amount
   dTmp = atof(apItems[TSA_AMOUNT]);
   if (dTmp > 0.0)
   {
      strcpy(pItemsRec->TaxAmt, apItems[TSA_AMOUNT]);
      return 0;
   } else
      return 3;
}

int Sfx_Load_TaxItems(bool bImport, int iSkipHdr)
{
   char     *pTmp, acItemsRec[MAX_RECSIZE], acAgencyRec[512], acRec[512];
   char     acItemsFile[_MAX_PATH], acAgencyFile[_MAX_PATH], acTmpFile[_MAX_PATH], acInFile[_MAX_PATH];

   int      iRet;
   long     lItems=0, lCnt=0, lAgency=0;
   FILE     *fdItems, *fdAgency, *fdIn;
   TAXDETAIL *pTax   = (TAXDETAIL *)&acItemsRec[0];

   LogMsg0("Loading Detail file");

   GetIniString(myCounty.acCntyCode, "TaxDetail", "", acInFile, _MAX_PATH, acIniFile);
   NameTaxCsvFile(acItemsFile, myCounty.acCntyCode, "Items");
   NameTaxCsvFile(acAgencyFile, myCounty.acCntyCode, "Agency");
   sprintf(acTmpFile, "%s\\%s\\%s_Agency.tmp", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);

   if (_access(acInFile, 0))
   {
      LogMsg("--- Missing detail file %s.  Skip loading", acInFile);
      return 1;
   }

   // Open input file
   LogMsg("Open Detail file %s", acInFile);
   fdIn = fopen(acInFile, "r");
   if (fdIn == NULL)
   {
      LogMsg("***** Error opening Detail file: %s\n", acInFile);
      return -2;
   }  

   // Open Items file
   LogMsg("Create Items file %s", acItemsFile);
   fdItems = fopen(acItemsFile, "w");
   if (fdItems == NULL)
   {
      LogMsg("***** Error creating Items file: %s\n", acItemsFile);
      return -4;
   }

   // Open Agency file
   LogMsg("Create Agency file %s", acTmpFile);
   fdAgency = fopen(acTmpFile, "w");
   if (fdAgency == NULL)
   {
      LogMsg("***** Error creating Agency file: %s\n", acTmpFile);
      return -4;
   }

   // Init variables

   // Skip header record
   for (iRet = 0; iRet < iSkipHdr; iRet++)
      pTmp = fgets((char *)&acRec[0], 512, fdIn);

   // Merge loop 
   while (!feof(fdIn))
   {
      pTmp = fgets((char *)&acRec[0], 512, fdIn);
      if (!pTmp)
         break;

      // Create Items & Agency record
      iRet = Sfx_ParseTaxDetail(acItemsRec, acAgencyRec, acRec);
      if (!iRet)
      {
         // Create TaxBase record
         Tax_CreateDetailCsv(acRec, (TAXDETAIL *)&acItemsRec);
         lItems++;
         fputs(acRec, fdItems);

         // Create Agency record
         Tax_CreateAgencyCsv(acRec, (TAXAGENCY *)&acAgencyRec);
#ifdef _DEBUG
         //if (acRec[0] < '0')
         //   iRet = 0;
#endif
         fputs(acRec, fdAgency);
      } else if (iRet == 1)
      {
         LogMsg0("---> Drop detail record %d [%.80s]", lCnt, acRec); 
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdIn)
      fclose(fdIn);
   if (fdItems)
      fclose(fdItems);
   if (fdAgency)
      fclose(fdAgency);

   LogMsg("Total records processed:    %u", lCnt);
   LogMsg("Total Items records:        %u", lItems);

   // Import into SQL
   if (bImport)
   {
      iRet = doTaxImport(myCounty.acCntyCode, TAX_DETAIL);
      if (!iRet)
      {
         // Populate TotalRate - EXEC spUpdateTotalRate 'EDX'
         //iRet = doUpdateTotalRate(myCounty.acCntyCode);

         // Dedup Agency file before import
         LogMsg("Dedup Agency file %s to %s", acTmpFile, acAgencyFile);
         iRet = sortFile(acTmpFile, acAgencyFile, "S(#1,C,A,#2,C,A) DEL(124) DUPOUT F(TXT) OMIT(1,1,C,EQ,\"|\")");
         if (iRet > 0)
            iRet = doTaxImport(myCounty.acCntyCode, TAX_AGENCY);
      }
   } else
      iRet = 0;

   return iRet;
}

/**************************** Sfx_Load_TaxDelq *******************************
 *
 *
 * Return 0 if success.
 *
 *****************************************************************************/

int Sfx_ParseTaxDelq(char *pOutbuf, char *pRec1, char *pRec2)
{
   char     *apItem1[50], *apItem2[50], *pTmp;
   int      iTem1, iTem2;
   double   dDelqAmt, dAmt1, dAmt2;

   TAXDELQ  *pOutRec = (TAXDELQ *)pOutbuf;

#ifdef _DEBUG
   //if (!memcmp(pInRec->Apn, "0312200108", 10))
   //   iRet = 0;
#endif

   iTem1 = ParseStringNQ(pRec1, cDelim, 50, apItem1);
   if (iTem1 < TDR_FLDS)
   {
      LogMsg("***** Parse TDR -->Bad tax record 1 (%d)", iTem1);
      return -1;
   }
   iTem2 = ParseStringNQ(pRec2, cDelim, 50, apItem2);
   if (iTem2 < TDR_FLDS)
   {
      LogMsg("***** Parse TDR -->Bad tax record 2 (%d)", iTem2);
      return -1;
   }

   if  (strcmp(apItem1[TDR_APN], apItem2[TDR_APN]))
   {
      LogMsg("***** Mismatch Delq APN: %s <> %s", apItem1[TDR_APN], apItem2[TDR_APN]);
      return 1;
   }

   // Reset buffer
   memset(pOutbuf, 0, sizeof(TAXDELQ));

   // APN
   if (*(apItem1[TDR_APN]+4) == '-')
      replChar(apItem1[TDR_APN], '-', ' ');
   else
      remChar(apItem1[TDR_APN], '-');
   strcpy(pOutRec->Apn, apItem1[TDR_APN]);

   // Default Number
   strcpy(pOutRec->Default_No, apItem1[TDR_DEFAULT_NO]);

   // Default date
   pTmp = dateConversion(apItem1[TDR_DEFAULT_DATE], pOutRec->Def_Date, MM_DD_YYYY_1);
   if (!pTmp)
      LogMsg("*** Invalid default date: APN=%s, Date=%s", pOutRec->Apn, apItem1[TDS_DUE_DATE]);

   // Tax year
   strcpy(pOutRec->TaxYear, apItem1[TDR_ASMT_YR]);

   // Account type
   if (!_memicmp(apItem1[TDR_ACCOUNT_TYPE], "Secured Supplemental", 10))
   {
      pOutRec->isSupp[0] = '1';
      pOutRec->isSecd[0] = '0';
   } else 
   {
      pOutRec->isSecd[0] = '1';
      pOutRec->isSupp[0] = '0';
   }

   // Delq Amt
   dAmt1 = dAmt2 = 0;
   if (*apItem1[TDR_PAID_STATUS] == 'U')
      dAmt1 = atof(apItem1[TDR_TOTAL_TAX]);
   if (*apItem2[TDR_PAID_STATUS] == 'U')
      dAmt2 = atof(apItem2[TDR_TOTAL_TAX]);
   dDelqAmt = dAmt1 + dAmt2;

   // Paid Status
   if (*apItem1[TDR_IN_ACTIVE_PLAN] == 'Y')
      pOutRec->DelqStatus[0] = TAX_STAT_ONPAYMENT;
   else if (*apItem1[TDR_CANCELED] == 'Y')
      pOutRec->DelqStatus[0] = TAX_STAT_CANCEL;
   else if (*apItem1[TDR_ACCOUNT_STATUS] == 'P')
      pOutRec->DelqStatus[0] = TAX_STAT_REDEEMED;
   else if (*apItem1[TDR_ACCOUNT_STATUS] == 'U')
      pOutRec->DelqStatus[0] = TAX_STAT_UNPAID;
   else
      LogMsg("Invalid paid status %s [%s]", apItem1[TDR_ACCOUNT_STATUS], apItem1[TDR_APN]);

   // Is delinq?
   if (dDelqAmt > 0.0)
   {
      sprintf(pOutRec->Def_Amt, "%.2f", dDelqAmt);
      if (*apItem2[TDR_PAID_STATUS] == 'U')
      {
         pOutRec->isDelq[0] = '1';
         if (*apItem1[TDR_PAID_STATUS] == 'U')
            pOutRec->InstDel[0] = '3';
         else
            pOutRec->InstDel[0] = '2';
      }
      return 0;
   } else
      return 1;
}

int Sfx_Load_TaxDelq(bool bImport, int iSkipHdr)
{
   char     *pTmp, acBuf[MAX_RECSIZE], acRec1[MAX_RECSIZE], acRec2[MAX_RECSIZE], acTmp[MAX_RECSIZE];
   char     acTmpFile[_MAX_PATH], acOutFile[_MAX_PATH], acInFile[_MAX_PATH];

   int      iRet;
   long     lOut=0, lCnt=0;
   FILE     *fdOut, *fdIn;
   TAXDELQ  *pOutRec = (TAXDELQ *)acBuf;

   LogMsg0("Loading Redemption file");

   sprintf(acTmpFile, "%s\\%s\\%s_Delq.Tmp", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   NameTaxCsvFile(acOutFile, myCounty.acCntyCode, "Delq");

   GetIniString(myCounty.acCntyCode, "SecDefault", "", acInFile, _MAX_PATH, acIniFile);
   lLastFileDate = getFileDate(acInFile);

   // Open input file
   LogMsg("Open delinquent tax file %s", acInFile);
   fdIn = fopen(acInFile, "r");
   if (fdIn == NULL)
   {
      LogMsg("***** Error opening delinquent tax file: %s\n", acInFile);
      return -2;
   }  

   // Open Output file
   LogMsg("Open output file %s", acTmpFile);
   fdOut = fopen(acTmpFile, "w");
   if (fdOut == NULL)
   {
      LogMsg("***** Error creating output file: %s\n", acTmpFile);
      return -4;
   }

   // Skip header record
   for (iRet = 0; iRet < iSkipHdr; iRet++)
      pTmp = fgets((char *)&acRec1[0], 512, fdIn);

   // Merge loop    
   iRet = 0;
   acBuf[0] = 0;
   while (!feof(fdIn))
   {
      if (!(pTmp = fgets((char *)&acRec1[0], MAX_RECSIZE, fdIn)))
         break;
      if (!(pTmp = fgets((char *)&acRec2[0], MAX_RECSIZE, fdIn)))
         break;

#ifdef _DEBUG      
      //if (!memcmp(acRec, "00", 2) && !memcmp(&acRec[16], "0151000528", 10))
      //{
      //   iRecType = 0;
      //}
#endif

      iRet = Sfx_ParseTaxDelq(acBuf, acRec1, acRec2);
      if (!iRet)
      {
         //if (pOutRec->DelqStatus[0] != TAX_STAT_REDEEMED && pOutRec->DelqStatus[0] != TAX_STAT_CANCEL)
         //   pOutRec->isDelq[0] = '1';

         sprintf(pOutRec->Upd_Date, "%d", lLastFileDate);

         // Output last record
         Tax_CreateDelqCsv(acTmp, (TAXDELQ *)pOutRec);

         // Output record			
         fputs(acTmp, fdOut);
         lOut++;
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdIn)
      fclose(fdIn);
   if (fdOut)
      fclose(fdOut);

   // Sort output to dedup entry
   iRet = sortFile(acTmpFile, acOutFile, "S(#1,C,A,#3,C,A,#5,C,A) DUPOUT DEL(124)");

   LogMsg("Total records processed:          %u", lCnt);
   LogMsg("Total delinquent records output:  %u", lOut);

   printf("\nTotal output records: %u", lCnt);

   // Import into SQL
   if (bImport && lOut > 0)
   {
      iRet = doTaxImport(myCounty.acCntyCode, TAX_DELINQUENT);
   } else
      iRet = 0;

   return iRet;
}

/****************************** Sfx_ParseTaxSupp ******************************
 *
 * Create TAXBASE record
 * The 2 input records are installment 1 & 2.  We have to combine them into one base record.
 * If a tax bill from prior year and it has been paid, ignore it.
 *
 ******************************************************************************/

int Sfx_ParseTaxSupp(char *pOutbuf, char *pRec1, char *pRec2)
{
   char     *apItem1[50], *apItem2[50], *pTmp;
   int      iTem1, iTem2, iRollYear;
   double   dDueAmt2, dDueAmt1, dTaxAmt1, dTaxAmt2, dPaidAmt1, dPaidAmt2, dTotalDue, dTotalTax;

   TAXBASE  *pTaxBase = (TAXBASE *)pOutbuf;

   iTem1 = ParseStringNQ(pRec1, ',', 50, apItem1);
   if (iTem1 < TSS_FLDS)
   {
      LogMsg("***** Parse TDS -->Bad tax record 1 (%d)", iTem1);
      return -1;
   }
   iTem2 = ParseStringNQ(pRec2, ',', 50, apItem2);
   if (iTem2 < TSS_FLDS)
   {
      LogMsg("***** Parse TDS -->Bad tax record 2 (%d)", iTem2);
      return -1;
   }

   if  (strcmp(apItem1[TSS_APN], apItem2[TSS_APN]))
   {
      LogMsg("***** Mismatch APN: %s <> %s", apItem1[TSS_APN], apItem2[TSS_APN]);
      return -2;
   }

   // Installment balance
   dDueAmt1 = atof(apItem1[TSS_DUE_AMT]);
   dDueAmt2 = atof(apItem2[TSS_DUE_AMT]);

   // Roll year - Ignore supl from prior year that has been paid
   iRollYear = atol(apItem1[TSS_ROLL_YR]);
   if (iRollYear != lTaxYear && dDueAmt2 == 0.0)
      return 1;

   // Init buffer
   memset(pOutbuf, 0, sizeof(TAXBASE));

   // Remove hyphen from APN
   if (*(apItem1[TSS_APN]+4) == '-')
      replChar(apItem1[TSS_APN], '-', ' ');
   else
      remChar(apItem1[TSS_APN], '-');
   strcpy(pTaxBase->Apn, apItem1[TSS_APN]);

#ifdef _DEBUG
   //if (!memcmp(apItem1[TSS_APN], "0025 043", 8))
   //   iTaxYear = 0;
#endif

   // Tax Year
   iTaxYear = atol(apItem1[TSS_ASMT_YR]);
   strcpy(pTaxBase->TaxYear, apItem1[TSS_ASMT_YR]);

   // BillNum
   strcpy(pTaxBase->BillNum, apItem1[TSS_BILL_NO]);
   dTaxAmt1 = atof(apItem1[TSS_TAX_AMT]);
   dTaxAmt2 = atof(apItem2[TSS_TAX_AMT]);
   dTotalTax = dTaxAmt1+dTaxAmt2;
   dPaidAmt1 = dPaidAmt2 = 0;

   // Installment 1
   if (*apItem1[TSS_INST_NO] == '1')
   {
      // Tax Amt
      if (dTaxAmt1 > 0.0)
      {
         strcpy(pTaxBase->TaxAmt1, apItem1[TSS_TAX_AMT]);

         // Due date
         if (*apItem1[TSS_DUE_DATE] >= '0')
         {
            pTmp = dateConversion(apItem1[TSS_DUE_DATE], pTaxBase->DueDate1, MM_DD_YYYY_1, lLienYear);
            if (!pTmp)
               LogMsg("*** Invalid due date1: APN=%s, DueDate1=%s", pTaxBase->Apn, pTaxBase->DueDate1);
         }
         if (!_memicmp(apItem1[TSS_PAID_STATUS], "PA", 2))
         {
            pTaxBase->Inst1Status[0] = TAX_BSTAT_PAID;
            dPaidAmt1 = dTaxAmt1;

            pTmp = dateConversion(apItem1[TSS_PAID_DATE], pTaxBase->PaidDate1, MM_DD_YYYY_1, lLienYear);
            if (!pTmp)
               LogMsg("*** Invalid paid date1: APN=%s, PaidDate1=%s", apItem1[TSS_APN], apItem1[TSS_PAID_DATE]);
         } else if (*apItem1[TSS_PAID_STATUS] == 'U')
            pTaxBase->Inst1Status[0] = TAX_BSTAT_UNPAID;
         else if (!_memicmp(apItem1[TSS_PAID_STATUS], "PRE", 3))
            pTaxBase->Inst1Status[0] = TAX_BSTAT_UNPAID;
         else
            LogMsg("*** Unknown paid status1: %s [%s]", apItem1[TSS_PAID_STATUS], apItem1[TSS_APN]);
      } else
            pTaxBase->Inst1Status[0] = TAX_BSTAT_NOTAX;
   } else
      LogMsg("*** Installment #1 out of sync APN=%s", apItem1[TSS_APN]);

   // Installment 2
   if (*apItem2[TSS_INST_NO] == '2')
   {
      if (dTaxAmt2 > 0.0)
      {
         strcpy(pTaxBase->TaxAmt2, apItem2[TSS_TAX_AMT]);
         if (*apItem2[TSS_DUE_DATE] >= '0')
         {
            pTmp = dateConversion(apItem2[TSS_DUE_DATE], pTaxBase->DueDate2, MM_DD_YYYY_1, lLienYear);
            if (!pTmp)
               LogMsg("*** Invalid due date2: APN=%s, DueDate2=%s", pTaxBase->Apn, apItem2[TSS_DUE_DATE]);
         }

         if (!_memicmp(apItem2[TSS_PAID_STATUS], "PA", 2))
         {
            pTaxBase->Inst2Status[0] = TAX_BSTAT_PAID;
            dPaidAmt2 = dTaxAmt2;

            pTmp = dateConversion(apItem2[TSS_PAID_DATE], pTaxBase->PaidDate2, MM_DD_YYYY_1, lLienYear);
            if (!pTmp)
               LogMsg("*** Invalid paid date2: APN=%s PaidDate2=%s", apItem2[TSS_APN], apItem2[TSS_PAID_DATE]);
         } else if (*apItem2[TSS_PAID_STATUS] == 'U')
            pTaxBase->Inst2Status[0] = TAX_BSTAT_UNPAID;
         else if (!_memicmp(apItem2[TSS_PAID_STATUS], "PRE", 3))
            pTaxBase->Inst2Status[0] = TAX_BSTAT_UNPAID;
         else 
            LogMsg("*** Unknown paid status2: %s [%s]", apItem2[TSS_PAID_STATUS], apItem2[TSS_APN]);
      } else
         pTaxBase->Inst2Status[0] = TAX_BSTAT_NOTAX;
   } else
      LogMsg("*** Installment #2 out of sync APN=%s", apItem2[TSS_APN]);

   // Total tax amount
   sprintf(pTaxBase->TotalTaxAmt, "%.2f", dTotalTax);

   // Total due amount
   dTotalDue = dDueAmt1 + dDueAmt2;
   sprintf(pTaxBase->TotalDue, "%.2f", dTotalDue);

   if (dPaidAmt1 > 0.0)
      sprintf(pTaxBase->PaidAmt1, "%.2f", dPaidAmt1);
   if (dPaidAmt2 > 0.0)
      sprintf(pTaxBase->PaidAmt2, "%.2f", dPaidAmt2);

   // Penalty amount
   double dPenAmt2, dPenAmt1;
   dPenAmt1 = atof(apItem1[TSS_PEN_AMT]);
   if (dPenAmt1 > 0.0)
      sprintf(pTaxBase->PenAmt1, "%.2f", dPenAmt1);
   dPenAmt2 = atof(apItem2[TSS_PEN_AMT]);
   if (dPenAmt2 > 0.0)
      sprintf(pTaxBase->PenAmt2, "%.2f", dPenAmt2);

   pTaxBase->BillType[0] = BILLTYPE_SECURED_SUPPL;
   pTaxBase->isSecd[0] = '0';
   pTaxBase->isSupp[0] = '1';

   return 0;
}

/******************************* Sfx_Load_TaxSupp ****************************
 *
 * Create import file and import into Sfx_Tax_Base table
 *
 * Return 0 if success.
 *
 *****************************************************************************/

int Sfx_Load_TaxSupp(bool bImport, int iSkipHdr)
{
   char     *pTmp, acBuf[MAX_RECSIZE], acRec1[MAX_RECSIZE], acRec2[MAX_RECSIZE];
   char     acBaseFile[_MAX_PATH], acInFile[_MAX_PATH];

   int      iRet;
   long     lOut=0, lCnt=0;
   FILE     *fdIn, *fdBase, *fdR01;
   TAXBASE  *pTaxBase = (TAXBASE *)&acBuf[0];

   LogMsg0("Loading tax supplemental file");

   GetIniString(myCounty.acCntyCode, "SuppTax", "", acInFile, _MAX_PATH, acIniFile);
   NameTaxCsvFile(acBaseFile, myCounty.acCntyCode, "Base");
   lLastTaxFileDate = getFileDate(acInFile);

   // Open input file
   LogMsg("Open Tax file %s", acInFile);
   fdIn = fopen(acInFile, "r");
   if (fdIn == NULL)
   {
      LogMsg("***** Error opening Tax file: %s\n", acInFile);
      return -2;
   }  

   // Open Output file
   LogMsg("Open output file %s", acBaseFile);
   fdBase = fopen(acBaseFile, "a+");
   if (fdBase == NULL)
   {
      LogMsg("***** Error opening output file: %s\n", acBaseFile);
      return -4;
   }

   // Open raw file to update TRA
   fdR01 = OpenR01(myCounty.acCntyCode);
   UpdateTRA(acBuf, fdR01, true);
   iApnMatch = 0;

   // Skip header record
   for (iRet = 0; iRet < iSkipHdr; iRet++)
      pTmp = fgets((char *)&acRec1[0], MAX_RECSIZE, fdIn);

   // Start loop 
   while (!feof(fdIn))
   {
      if (!(pTmp = fgets((char *)&acRec1[0], MAX_RECSIZE, fdIn)))
         break;
      if (!(pTmp = fgets((char *)&acRec2[0], MAX_RECSIZE, fdIn)))
         break;

      // Create new TAXBASE record
      iRet = Sfx_ParseTaxSupp(acBuf, acRec1, acRec2);
      if (!iRet)
      {
         // Update TRA
         if (fdR01)
            UpdateTRA(acBuf, fdR01);

         // Create delimited record
         Tax_CreateTaxBaseCsv(acRec1, (TAXBASE *)&acBuf);
         fputs(acRec1, fdBase);
         lOut++;
      } else if (bDebug && iRet > 0)
         LogMsg("---> Sfx_Load_TaxSupp: drop record %d [%.40s]", lCnt, acRec1); 

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdIn)
      fclose(fdIn);
   if (fdR01)
      fclose(fdR01);
   if (fdBase)
      fclose(fdBase);

   LogMsg("Total records processed:       %u", lCnt);
   LogMsg("Total output records:          %u", lOut);
   LogMsg("Total APN matched records:     %u", iApnMatch);
   printf("\nTotal output records: %u", lCnt);

   // Import into SQL
   if (bImport)
      iRet = doTaxImport(myCounty.acCntyCode, TAX_BASE);
   else
      iRet = 0;

   return iRet;
}

/*********************************** loadSfx ********************************
 *
 * Input files:
 *    - AIASECD               (roll file, 391-byte ebcdic)
 *    - AIAOWNR               (owner file, 85-byte ebcdic)
 *    - AIAMADR               (mailaddr file, 172-byte ebcdic)
 *    - sfx_sale_newcum.raw   (cumulative sale file, 45-byte ascii)
 *
 * Following are tasks to be done here:
 *    - Convert ebcdic to ascii
 *    - Normal update: LoadOne -U -Xs -Mg
 *    - Load Lien: LoadOne -L -Xl -Ms -Mg
 * Notes:
 *    - If -Xs & -Ms are used, sale file SFX_ASH.SLS will be merged into R01.
 *    - If -Ms is used alone, sale file SFX_SALE.SLS will be used.
 *
 ****************************************************************************/

int loadSfx(int iSkip)
{
   int   iRet=0;
   char  acDefFile[_MAX_PATH], acTmpFile[_MAX_PATH], *pTmp;
   bool  bAscii;

   iApnLen = myCounty.iApnLen;
   iOwnerLen = GetPrivateProfileInt(myCounty.acCntyCode, "OwnerRecSize", 0, acIniFile);

   //iRet = SC2SCExt(acCSalFile);

   // Convert old cum sale file to new format
   //GetIniString(myCounty.acCntyCode, "OldCumSaleFile", "", acTmpFile, _MAX_PATH, acIniFile);
   //iRet = Sfx_ConvertSale(myCounty.acCntyCode, acTmpFile);

   // Clean up sale file - use as needed
   //iRet = CleanupSales(acCSalFile);

   // Remove NonSaleFlg
   //iRet = FixCumSale(acCSalFile, SALE_FLD_NONSALE, true);
   
   // Fix DocNum
   //iRet = FixCumSale(acCSalFile, SALE_FLD_DOCNUM+CNTY_SFX, true);

   // Fix APN
   //GetIniString(myCounty.acCntyCode, "XSalFile", "", acTmpFile, _MAX_PATH, acIniFile);    
   //iRet = Sfx_FixWebExtr(acTmpFile);

   // Load tax
   //if (iLoadTax == TAX_LOADING)                    // -T or -Lt
   //{
   //   TC_SetDateFmt(MM_DD_YYYY_1, true);
   //   iRet = TC_LoadTax(myCounty.acCntyCode, bTaxImport);
   //}

   if (iLoadTax == TAX_LOADING)                    // -T or -Lt
   {
      iRet = GetIniString(myCounty.acCntyCode, "TaxDist", "", acTmpFile, _MAX_PATH, acIniFile);
      if (iRet > 0)
         iRet = LoadTaxCodeTable(acTmpFile);

      // Load PT___Tax_Data_Secured
      iRet = Sfx_Load_TaxBase(false, iHdrRows);

      if (!iRet && lLastTaxFileDate > 0)
      {
         // Load PT___Tax_Data_Sec_Supp
         iRet = Sfx_Load_TaxSupp(bTaxImport, iHdrRows);

         // Load PT___Tax_Data_Special_Assessments
         // Ignore return code from this function since we only load it once a year
         Sfx_Load_TaxItems(bTaxImport, iHdrRows);

         // Load PT___Tax_Data_Redemption
         if (!iRet)
            iRet |= Sfx_Load_TaxDelq(bTaxImport, iHdrRows);

         // Update Delq flag in Tax_Base
         if (!iRet)
            iRet = updateDelqFlag(myCounty.acCntyCode);
      }
      if (!iLoadFlag)
         return iRet;
   }

   // Exit if load/update tax only
   if (!iLoadFlag)
      return iRet;

   // Extract Attom sales 
   if (iLoadFlag & EXTR_NRSAL)                      // -Xn
   {
      iRet = GetIniString(myCounty.acCntyCode, "NdcSale", "", acSaleFile, _MAX_PATH, acIniFile);
      if (!_access(acSaleFile, 0))
      {
         iRet = NR_CreateSCSale(myCounty.acCntyCode, acSaleFile, iApnLen);
         if (iRet)
            return iRet;
      } else
      {
         LogMsg("*** Sale file not available.  Please verify: %s", acSaleFile);
         return -1;
      }
   }

   // Scan for EBCDIC file
   bAscii = isTextFile(acRollFile, iRollLen);

   if (!bAscii && (iLoadFlag & (LOAD_LIEN|LOAD_UPDT|EXTR_LIEN)))
   {
      // Translate Roll file ebcdic to ascii
      GetIniString(myCounty.acCntyCode, "RollDef", "", acDefFile, _MAX_PATH, acIniFile);
      if (_access(acDefFile, 0))
      {
         LogMsg("***** Error: missing roll definition file %s", acDefFile);
         return 1;
      }

      strcpy(acTmpFile, acRollFile);
      pTmp = strrchr(acRollFile, '.');
      if (pTmp)
         strcpy(++pTmp, "ASC");
      else
         strcat(acRollFile, ".ASC");
      if (bEbc2Asc || _access(acRollFile, 0))
      {
         LogMsg("Translate %s to Ascii %s", acTmpFile, acRollFile);
         iRet = F_Ebc2Asc(acTmpFile, acRollFile, acDefFile, 0);
         if (iRet < 0)
         {
            dispError(iRet, acTmpFile, acRollFile, acDefFile);
            return 1;
         }
      }
   }
   
   // Translate to ascii
   GetIniString(myCounty.acCntyCode, "OwnerFile", "", acOwnerFile, _MAX_PATH, acIniFile);
   GetIniString(myCounty.acCntyCode, "AdrFile", "", acAdrFile, _MAX_PATH, acIniFile);
   if (!bAscii && (iLoadFlag & (LOAD_LIEN|LOAD_UPDT)))
   {
      // Translate owner file ebcdic to ascii
      GetIniString(myCounty.acCntyCode, "OwnerDef", "", acDefFile, _MAX_PATH, acIniFile);
      if (_access(acDefFile, 0))
      {
         LogMsg("***** Error: missing owner definition file %s", acDefFile);
         return 1;
      }
      if (_access(acOwnerFile, 0))
      {
         LogMsg("***** Error: missing owner file %s", acOwnerFile);
         return 1;
      }

      strcpy(acTmpFile, acOwnerFile);
      pTmp = strrchr(acOwnerFile, '.');
      if (pTmp)
         strcpy(++pTmp, "ASC");
      else
         strcat(acOwnerFile, ".ASC");
      if (bEbc2Asc)
      {
         LogMsg("Translate %s to Ascii %s", acTmpFile, acOwnerFile);
         iRet = F_Ebc2Asc(acTmpFile, acOwnerFile, acDefFile, 0);
         if (iRet < 0)
         {
            dispError(iRet, acTmpFile, acOwnerFile, acDefFile);
            return 1;
         }
      }

      // Translate mail address file ebcdic to ascii
      GetIniString(myCounty.acCntyCode, "AdrDef", "", acDefFile, _MAX_PATH, acIniFile);
      if (_access(acDefFile, 0))
      {
         LogMsg("***** Error: missing address definition file %s", acDefFile);
         return 1;
      }
      if (_access(acAdrFile, 0))
      {
         LogMsg("***** Error: missing address file %s", acAdrFile);
         return 1;
      }

      strcpy(acTmpFile, acAdrFile);
      pTmp = strrchr(acAdrFile, '.');
      if (pTmp)
         strcpy(++pTmp, "ASC");
      else
         strcat(acAdrFile, ".ASC");
      if (bEbc2Asc)
      {
         LogMsg("Translate %s to Ascii %s", acTmpFile, acAdrFile);
         iRet = F_Ebc2Asc(acTmpFile, acAdrFile, acDefFile, 0);
         if (iRet < 0)
         {
            dispError(iRet, acTmpFile, acAdrFile, acDefFile);
            return 1;
         }
      }
   }

   // Extract lien
   if (iLoadFlag & EXTR_LIEN)                      // -Xl
      Sfx_ExtrLien();

   // Merge roll file
   if (iLoadFlag & LOAD_LIEN)                      // -L
      iRet = Sfx_Load_LDR(iSkip);
   if (iLoadFlag & LOAD_UPDT)                      // -U
      iRet = Sfx_Load_Roll(iSkip);

   // Apply NDC sales
   if (!iRet && iLoadFlag & UPDT_XSAL)             // -Mn
   {
      GetIniString("Data", "ASH_File", acESalTmpl, acESalTmpl, _MAX_PATH, acIniFile);
      sprintf(acCSalFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "Sls");

      // Apply Sfx_ash.sls to R01 file
      iRet = ApplyCumSaleNR(iSkip, acTmpFile, SALE_USE_SCUPDXFR, CLEAR_OLD_SALE);
      if (!iRet)
         iLoadFlag |= LOAD_UPDT;
   }
   
   // Apply cum sale file to R01 - DocDate in county file may not match with DocNum
   if (!iRet && iLoadFlag & MERG_CSAL)             // -Ms
   {
      GetIniString("Data", "SaleExp", "", acESalTmpl, _MAX_PATH, acIniFile);
      sprintf(acCSalFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "Sls");

      // Apply Sfx_sale.sls to R01 file
      iRet = ApplyCumSale(iSkip, acCSalFile, false, SALE_USE_SCUPDXFR, VERIFY_DATE);
      if (!iRet)
         iLoadFlag |= LOAD_UPDT;
   } 


   //if (!iRet && (iLoadFlag & LOAD_UPDT))
   //{
   //   // Apply Sfx_Sale.sls to R01 file
   //   iRet = ApplyCumSale(iSkip, acCSalFile, false, SALE_USE_SCUPDXFR, DONT_CHK_DOCNUM|CLEAR_OLD_SALE);
   //}

   // Merge GrGr
   //if (iLoadFlag & MERG_GRGR)                      // -Mg
   //{
   //   GetIniString(myCounty.acCntyCode, "XSalFile", "", acTmpFile, _MAX_PATH, acIniFile);    
   //   iRet = Sfx_MergeDocExt(myCounty.acCntyCode, acTmpFile);
   //   if (iRet)
   //      iLoadFlag = 0;
   //   else
   //   {
   //      if (!(iLoadFlag & LOAD_LIEN))
   //         iLoadFlag |= LOAD_UPDT;
   //   }
   //}
 
   // Fix TRA
   if (!iRet && (lOptMisc & M_OPT_FIXTRA))
      iRet = PQ_FixR01(myCounty.acCntyCode, PQ_FIX_TRA);

   return iRet;
}