#if !defined(AFX_STDAFX_H__LOADONE_EFA4_45F7_9334_A6196A45181B__INCLUDED_)
#define AFX_STDAFX_H__LOADONE_EFA4_45F7_9334_A6196A45181B__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#define VC_EXTRALEAN		// Exclude rarely-used stuff from Windows headers

#include <afx.h>
#include <afxwin.h>         // MFC core and standard components
#include <afxext.h>         // MFC extensions
#include <iostream>

#include <sys/types.h>
#include <sys/stat.h>
#include <time.h>
#include <stdio.h>
#include <io.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <errno.h>
#include <direct.h>

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_STDAFX_H__LOADONE_EFA4_45F7_9334_A6196A45181B__INCLUDED_)
