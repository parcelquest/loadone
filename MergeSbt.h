#ifndef _MERGE_SBT
#define _MERGE_SBT

// SBT county definition
#define  ROFF_APN                  1
#define  ROFF_LIEN_OWNER           11-1
#define  ROFF_CURR_OWNER           61
#define  ROFF_MAIL_ADDR            111
#define  ROFF_MAIL_CITY            136
#define  ROFF_UNUSED               158
#define  ROFF_MAIL_STATE           159
#define  ROFF_MAIL_ZIP             161
#define  ROFF_PROPDESC             170-1
#define  ROFF_APPR_YEAR            215
#define  ROFF_TRA                  217
#define  ROFF_NEW_OWNER_CODE       222
#define  ROFF_ETAL_FLAG            223
#define  ROFF_REC_NUM              224
#define  ROFF_USECODE              233
#define  ROFF_UNITS                238
#define  ROFF_ZONE                 243
#define  ROFF_HOME_OWNER_FLAG      253
#define  ROFF_PERS_PROP_FLAG       254
#define  ROFF_AG_TIMB_FLAG         255
#define  ROFF_PEN_ASMT_FLAG        256
#define  ROFF_REVALUE_PROP_CODE    257
#define  ROFF_EXEMP_CODE           258
#define  ROFF_DELQ_YR              260
#define  ROFF_REVALUE_DATE         262
#define  ROFF_REVALUE_PCT          268
#define  ROFF_ACRES                274
#define  ROFF_LOT_DIMENSION        280
#define  ROFF_RECORDED_MAP         286
#define  ROFF_STATUS_CODE          294
#define  ROFF_LAND                 295
#define  ROFF_IMPR                 305
#define  ROFF_TREES_VINES          315
#define  ROFF_PERS_PROP            325
#define  ROFF_EXEMP                335

#define  RSIZ_APN                  10
#define  RSIZ_LIEN_OWNER           50
#define  RSIZ_CURR_OWNER           50
#define  RSIZ_MAIL_ADDR1           25
#define  RSIZ_MAIL_CITY            22
#define  RSIZ_UNUSED               1
#define  RSIZ_MAIL_STATE           2
#define  RSIZ_MAIL_ZIP             5
#define  RSIZ_MAIL_ZIP4            4
#define  RSIZ_PROPDESC             45
#define  RSIZ_APPR_YEAR            2
#define  RSIZ_TRA                  5
#define  RSIZ_NEW_OWNER_CODE       1
#define  RSIZ_ETAL_FLAG            1
#define  RSIZ_REC_NUM              9
#define  RSIZ_USECODE              5
#define  RSIZ_UNITS                5
#define  RSIZ_ZONE                 10
#define  RSIZ_HOME_OWNER_FLAG      1
#define  RSIZ_PERS_PROP_FLAG       1
#define  RSIZ_AG_TIMB_FLAG         1
#define  RSIZ_PEN_ASMT_FLAG        1
#define  RSIZ_REVALUE_PROP_CODE    1
#define  RSIZ_EXEMP_CODE           2
#define  RSIZ_DELQ_YR              2
#define  RSIZ_REVALUE_DATE         6
#define  RSIZ_REVALUE_PCT          6
#define  RSIZ_ACRES                6
#define  RSIZ_LOT_DIMENSION        6
#define  RSIZ_RECORDED_MAP         8
#define  RSIZ_STATUS_CODE          1
#define  RSIZ_LAND                 10
#define  RSIZ_IMPR                 10
#define  RSIZ_TREES_VINES          10
#define  RSIZ_PERS_PROP            10
#define  RSIZ_EXEMP                10

typedef struct _tSbtRoll
{  // 346 bytes
   char Apn              [RSIZ_APN              ];
   char Lien_Owner       [RSIZ_LIEN_OWNER       ];
   char Curr_Owner       [RSIZ_CURR_OWNER       ];
   char M_Addr1          [RSIZ_MAIL_ADDR1       ];
   char M_City           [RSIZ_MAIL_CITY        ];
   char Unused           [RSIZ_UNUSED           ];
   char M_State          [RSIZ_MAIL_STATE       ];
   char M_Zip            [RSIZ_MAIL_ZIP         ];
   char M_Zip4           [RSIZ_MAIL_ZIP4        ];
   char PropDesc         [RSIZ_PROPDESC         ]; // Situs or legal
   char Appr_Year        [RSIZ_APPR_YEAR        ];
   char Tra              [RSIZ_TRA              ];
   char New_Owner_Code   [RSIZ_NEW_OWNER_CODE   ];
   char Etal_Flag        [RSIZ_ETAL_FLAG        ]; // Y/N - not reliable
   char DocNum           [RSIZ_REC_NUM          ];
   char UseCode          [RSIZ_USECODE          ];
   char Units            [RSIZ_UNITS            ]; // None
   char Zone             [RSIZ_ZONE             ]; // None
   char Home_Owner_Flag  [RSIZ_HOME_OWNER_FLAG  ];
   char Pers_Prop_Flag   [RSIZ_PERS_PROP_FLAG   ];
   char Ag_Timb_Flag     [RSIZ_AG_TIMB_FLAG     ];
   char Pen_Asmt_Flag    [RSIZ_PEN_ASMT_FLAG    ];
   char Revalue_Prop_Code[RSIZ_REVALUE_PROP_CODE];
   char Exemp_Code       [RSIZ_EXEMP_CODE       ];
   char Delq_Yr          [RSIZ_DELQ_YR          ];
   char Revalue_Date     [RSIZ_REVALUE_DATE     ];
   char Revalue_Pct      [RSIZ_REVALUE_PCT      ];
   char Acres            [RSIZ_ACRES            ]; // V99
   char Lot_Dimension    [RSIZ_LOT_DIMENSION    ]; // 999x999
   char Recorded_Map     [RSIZ_RECORDED_MAP     ];
   char Status_Code      [RSIZ_STATUS_CODE      ];
   char Land             [RSIZ_LAND             ];
   char Impr             [RSIZ_IMPR             ];
   char Trees_Vines      [RSIZ_TREES_VINES      ];
   char Pers_Prop        [RSIZ_PERS_PROP        ];
   char Exemp            [RSIZ_EXEMP            ];
   char  CrLf[2];
} SBT_ROLL;

#endif