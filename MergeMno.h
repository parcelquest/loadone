#ifndef  _MERGE_MNO_H
#define  _MERGE_MNO_H


#define MNO_GR_PRSERV							0
#define MNO_GR_SOURCETABLE						1		
#define MNO_GR_APN_NUMBER						2
#define MNO_GR_GRANTOR							3
#define MNO_GR_GRANTEE							4
#define MNO_GR_INSTRUMENT_TYPE				5				
#define MNO_GR_OR_PRFOLDER						6
#define MNO_GR_OR_INSTRUMENT_NUMBER			7			
#define MNO_GR_OR_INSTRUMENT_DATE			8
#define MNO_GR_OR_FILE_DATE					9
#define MNO_GR_OR_VOLUME						10
#define MNO_GR_OR_PAGE							11
#define MNO_GR_OR_COMMENTS						12
#define MNO_GR_OR_SUB_BLOCK_LOT				13
#define MNO_GR_OR_TOWN_RANGE_SEC				14
#define MNO_GR_OR_MAP_TYPE						15
#define MNO_GR_OR_MAP_NUMBER					16
#define MNO_GR_OR_RECORDED_MAP_TYPE			17
#define MNO_GR_OR_MAP_BOOK						18
#define MNO_GR_OR_MAP_PAGE						19
#define MNO_GR_OR_PROJECT_NAME				20
#define MNO_GR_OR_STREET						21
#define MNO_GR_AW_PRFOLDER						22
#define MNO_GR_AW_STATUS						23
#define MNO_GR_AW_INSTRUMENT_NUMBER			24
#define MNO_GR_AW_FILE_DATE					25
#define MNO_GR_AW_SIGN_DATE					26
#define MNO_GR_AW_FORWARD						27
#define MNO_GR_AW_PARCEL_COUNT				28
#define MNO_GR_AW_PERCENT_OWNER				29
#define MNO_GR_AW_COMMENTS						30		
#define MNO_GR_AW_INDICATED_PRICE			31
#define MNO_GR_AW_BASE_YEAR					32
#define MNO_GR_AW_DTT							33
#define MNO_GR_AW_MAMMOTH						34
#define MNO_GR_AW_HOX							35
#define MNO_GR_AW_PROFILE						36
#define MNO_GR_AW_BOOK							37
#define MNO_GR_AW_SUBDIVISION					38
#define MNO_GR_AW_CHG_DATE						39
#define MNO_GR_AW_FORM_TYPE					40

#define MNOSIZ_GR_PRSERV						8
#define MNOSIZ_GR_SOURCETABLE					2
#define MNOSIZ_GR_APN_NUMBER					17
#define MNOSIZ_GR_GRANTOR						50
#define MNOSIZ_GR_GRANTEE						50
#define MNOSIZ_GR_INSTRUMENT_TYPE			30
#define MNOSIZ_GR_OR_PRFOLDER					30			
#define MNOSIZ_GR_OR_INSTRUMENT_NUMBER		10
#define MNOSIZ_GR_OR_INSTRUMENT_DATE		15
#define MNOSIZ_GR_OR_FILE_DATE				15
#define MNOSIZ_GR_OR_VOLUME					5		
#define MNOSIZ_GR_OR_PAGE						5
#define MNOSIZ_GR_OR_COMMENTS					200
#define MNOSIZ_GR_OR_SUB_BLOCK_LOT			5
#define MNOSIZ_GR_OR_TOWN_RANGE_SEC			5
#define MNOSIZ_GR_OR_MAP_TYPE					5
#define MNOSIZ_GR_OR_MAP_NUMBER				5
#define MNOSIZ_GR_OR_RECORDED_MAP_TYPE		5		
#define MNOSIZ_GR_OR_MAP_BOOK					5
#define MNOSIZ_GR_OR_MAP_PAGE					5
#define MNOSIZ_GR_OR_PROJECT_NAME			30
#define MNOSIZ_GR_OR_STREET					24
#define MNOSIZ_GR_AW_PRFOLDER					30
#define MNOSIZ_GR_AW_STATUS					5
#define MNOSIZ_GR_AW_INSTRUMENT_NUMBER		10
#define MNOSIZ_GR_AW_FILE_DATE				15
#define MNOSIZ_GR_AW_SIGN_DATE				15
#define MNOSIZ_GR_AW_FORWARD					5
#define MNOSIZ_GR_AW_PARCEL_COUNT			5			
#define MNOSIZ_GR_AW_PERCENT_OWNER			5
#define MNOSIZ_GR_AW_COMMENTS					200
#define MNOSIZ_GR_AW_INDICATED_PRICE		10
#define MNOSIZ_GR_AW_BASE_YEAR				4
#define MNOSIZ_GR_AW_DTT						10
#define MNOSIZ_GR_AW_MAMMOTH					10
#define MNOSIZ_GR_AW_HOX						10
#define MNOSIZ_GR_AW_PROFILE					10
#define MNOSIZ_GR_AW_BOOK						10
#define MNOSIZ_GR_AW_SUBDIVISION				200
#define MNOSIZ_GR_AW_CHG_DATE					15
#define MNOSIZ_GR_AW_FORM_TYPE				5


// BParcels
#define  MNO_BP_APN  	   		   	   0
#define  MNO_BP_ZONING				   	   1
#define  MNO_BP_NUM     		   	      2
#define  MNO_BP_PREFIX				   	   3
#define  MNO_BP_STREET			   	      4
#define  MNO_BP_CITY                      5
#define  MNO_BP_TRA                       6
#define  MNO_BP_USECODE                   7


#define  MNOSIZ_BP_APN				   	   13
#define  MNOSIZ_BP_ZONING				   	10
#define  MNOSIZ_BP_NUM     		   	   7
#define  MNOSIZ_BP_PREFIX				   	5
#define  MNOSIZ_BP_STREET			   	   24
#define  MNOSIZ_BP_CITY                   24
#define  MNOSIZ_BP_TRA                    4
#define  MNOSIZ_BP_USECODE				      8

typedef struct _tBP
{
   char  APN[MNOSIZ_BP_APN];
   char  Zoning[MNOSIZ_BP_ZONING];
   char  St_Num[MNOSIZ_BP_NUM];
   char  St_Prefix[MNOSIZ_BP_PREFIX];
   char  Street[MNOSIZ_BP_STREET];
   char  City[MNOSIZ_BP_CITY];
   char  Tra[MNOSIZ_BP_TRA];
   char  UseCode[MNOSIZ_BP_USECODE];
   char  CRLF[2];
} MNO_BP;

// _CHAR
#define  MNO_CHAR_APN				   	   0
#define  MNO_CHAR_IMAGE	      				1
#define  MNO_CHAR_YRBLT		      			2
#define  MNO_CHAR_EFFYR			      		3
#define  MNO_CHAR_BEDS				      	4
#define  MNO_CHAR_BATH					      5
#define  MNO_CHAR_HBATH 				   	6
#define  MNO_CHAR_ROOMS	      				7
#define  MNO_CHAR_FIREPL	      			8
#define  MNO_CHAR_WS				         	9
#define  MNO_CHAR_POOL				      	10
#define  MNO_CHAR_SQFT					      11
#define  MNO_CHAR_STORIES  				   12
#define  MNO_CHAR_GSIZE 		   			13
#define  MNO_CHAR_CLASS	   		   		14
#define  MNO_CHAR_QUAL		   		   	15
#define  MNO_CHAR_SEPTIC		   		   16
#define  MNO_CHAR_WELL				   	   17

#define  MNOSIZ_CHAR_APN				   	   13
#define  MNOSIZ_CHAR_IMAGE	      				1
#define  MNOSIZ_CHAR_YRBLT		      			4
#define  MNOSIZ_CHAR_EFFYR			      		4
#define  MNOSIZ_CHAR_BEDS				      	1
#define  MNOSIZ_CHAR_BATH					      1
#define  MNOSIZ_CHAR_HBATH 				   	1
#define  MNOSIZ_CHAR_ROOMS	      				1
#define  MNOSIZ_CHAR_FIREPL	      			1
#define  MNOSIZ_CHAR_WS				         	1
#define  MNOSIZ_CHAR_POOL				      	1
#define  MNOSIZ_CHAR_SQFT  				      5
#define  MNOSIZ_CHAR_STORIES  				   1
#define  MNOSIZ_CHAR_GSIZE 		   			5
#define  MNOSIZ_CHAR_CLASS	   		   		1
#define  MNOSIZ_CHAR_QUAL		   		   	5
#define  MNOSIZ_CHAR_SEPTIC		   		   1
#define  MNOSIZ_CHAR_WELL				   	   1

typedef struct _tChar
{
   char  APN[MNOSIZ_CHAR_APN];
   char  Image[MNOSIZ_CHAR_IMAGE];
   char  YrBlt[MNOSIZ_CHAR_YRBLT];
   char  EffYr[MNOSIZ_CHAR_EFFYR];
   char  Beds[MNOSIZ_CHAR_BEDS];
   char  Bath[MNOSIZ_CHAR_BATH];
   char  HBath[MNOSIZ_CHAR_HBATH];
   char  Rooms[MNOSIZ_CHAR_ROOMS];
   char  FirePl[MNOSIZ_CHAR_FIREPL];
   char  Ws[MNOSIZ_CHAR_WS];
   char  Pool[MNOSIZ_CHAR_POOL];
   char  Sqft[MNOSIZ_CHAR_SQFT];
   char  Stories[MNOSIZ_CHAR_STORIES];
   char  GSize[MNOSIZ_CHAR_GSIZE];
   char  Class[MNOSIZ_CHAR_CLASS];
   char  Qual[MNOSIZ_CHAR_QUAL];
   char  Septic[MNOSIZ_CHAR_SEPTIC];
   char  Well[MNOSIZ_CHAR_WELL];
   char  CRLF[2];
} MNO_CHAR;

#define  MNOOFF_LIEN_PROPERTY_TYPE		   1-1
#define  MNOOFF_LIEN_APN			         3-1
#define  MNOOFF_LIEN_OWNER_NAME		      16-1
#define  MNOOFF_LIEN_CAREOF			      47-1
#define  MNOOFF_LIEN_DBA			         82-1
#define  MNOOFF_LIEN_LIEN_DATE_OWNER		117-1
#define  MNOOFF_LIEN_MAIL_ADDR_1		      152-1
#define  MNOOFF_LIEN_MAIL_ADDR_2		      183-1
#define  MNOOFF_LIEN_MAIL_CITY_STATE		214-1
#define  MNOOFF_LIEN_ZIP_ZIP_4		      240-1
#define  MNOOFF_LIEN_LAND			         250-1
#define  MNOOFF_LIEN_IMPROV			      259-1
#define  MNOOFF_LIEN_PERS_PROP		      268-1
#define  MNOOFF_LIEN_OTHER_VALUE		      277-1
#define  MNOOFF_LIEN_TRADE_FIXT		      286-1
#define  MNOOFF_LIEN_MOB_HM			      295-1
#define  MNOOFF_LIEN_HOMEOWNER_EXEMP		304-1
#define  MNOOFF_LIEN_VETERAN_EXEMP		   313-1
#define  MNOOFF_LIEN_WELFARE_EXEMP		   322-1
#define  MNOOFF_LIEN_CHURCH_EXEMP		   331-1
#define  MNOOFF_LIEN_OTHER_EXEMP		      340-1
#define  MNOOFF_LIEN_NET_VALUE		      349-1
#define  MNOOFF_LIEN_PENALTY_MESSAGE		358-1
#define  MNOOFF_LIEN_COMMENT			      388-1
#define  MNOOFF_LIEN_TRA			         428-1
#define  MNOOFF_LIEN_TAX_EXEMPT		      432-1

#define  MNOSIZ_LIEN_PROPERTY_TYPE		   2 
#define  MNOSIZ_LIEN_APN			         13
#define  MNOSIZ_LIEN_OWNER_NAME			   31
#define  MNOSIZ_LIEN_CAREOF			      35
#define  MNOSIZ_LIEN_DBA			         35
#define  MNOSIZ_LIEN_LIEN_DATE_OWNER		35
#define  MNOSIZ_LIEN_MAIL_ADDR_1		      31
#define  MNOSIZ_LIEN_MAIL_ADDR_2		      31
#define  MNOSIZ_LIEN_MAIL_CITY_STATE		26
#define  MNOSIZ_LIEN_ZIP_ZIP_4			   10
#define  MNOSIZ_LIEN_LAND			         9 
#define  MNOSIZ_LIEN_IMPROV			      9 
#define  MNOSIZ_LIEN_PERS_PROP			   9 
#define  MNOSIZ_LIEN_OTHER_VALUE		      9 
#define  MNOSIZ_LIEN_TRADE_FIXT			   9 
#define  MNOSIZ_LIEN_MOB_HM			      9 
#define  MNOSIZ_LIEN_HOMEOWNER_EXEMP		9 
#define  MNOSIZ_LIEN_VETERAN_EXEMP		   9 
#define  MNOSIZ_LIEN_WELFARE_EXEMP		   9 
#define  MNOSIZ_LIEN_CHURCH_EXEMP		   9 
#define  MNOSIZ_LIEN_OTHER_EXEMP		      9 
#define  MNOSIZ_LIEN_NET_VALUE			   9 
#define  MNOSIZ_LIEN_PENALTY_MESSAGE		30
#define  MNOSIZ_LIEN_COMMENT			      40
#define  MNOSIZ_LIEN_TRA			         4 
#define  MNOSIZ_LIEN_TAX_EXEMPT			   1 


typedef struct _tMnoLien
{
   char Property_Type[MNOSIZ_LIEN_PROPERTY_TYPE];
   char Apn[MNOSIZ_LIEN_APN];
   char Owner_Name[MNOSIZ_LIEN_OWNER_NAME];
   char Careof[MNOSIZ_LIEN_CAREOF];
   char Dba[MNOSIZ_LIEN_DBA];
   char Lien_Date_Owner[MNOSIZ_LIEN_LIEN_DATE_OWNER];
   char Mail_Addr_1[MNOSIZ_LIEN_MAIL_ADDR_1];
   char Mail_Addr_2[MNOSIZ_LIEN_MAIL_ADDR_2];
   char Mail_City_State[MNOSIZ_LIEN_MAIL_CITY_STATE];
   char Zip_Zip_4[MNOSIZ_LIEN_ZIP_ZIP_4];    
   char Land[MNOSIZ_LIEN_LAND];
   char Improv[MNOSIZ_LIEN_IMPROV];
   char Pers_Prop[MNOSIZ_LIEN_PERS_PROP];
   char Other_Value[MNOSIZ_LIEN_OTHER_VALUE];
   char Trade_Fixt[MNOSIZ_LIEN_TRADE_FIXT];
   char Mob_Hm[MNOSIZ_LIEN_MOB_HM];
   char Homeowner_Exemp[MNOSIZ_LIEN_HOMEOWNER_EXEMP];
   char Veteran_Exemp[MNOSIZ_LIEN_VETERAN_EXEMP];
   char Welfare_Exemp[MNOSIZ_LIEN_WELFARE_EXEMP];
   char Church_Exemp[MNOSIZ_LIEN_CHURCH_EXEMP];
   char Other_Exemp[MNOSIZ_LIEN_OTHER_EXEMP];
   char Net_Value[MNOSIZ_LIEN_NET_VALUE];
   char Penalty_Message[MNOSIZ_LIEN_PENALTY_MESSAGE];
   char Comment[MNOSIZ_LIEN_COMMENT];
   char Tra[MNOSIZ_LIEN_TRA];
   char Tax_Exempt[MNOSIZ_LIEN_TAX_EXEMPT];
} MNO_LIEN;


#define  MNOOFF_ROLL_PROPERTY_TYPE  			1-1
#define  MNOOFF_ROLL_APN				         3-1
#define  MNOOFF_ROLL_OWNER_NAME			   	16-1
#define  MNOOFF_ROLL_CAREOF						47-1
#define  MNOOFF_ROLL_DBA							82-1
#define  MNOOFF_ROLL_LIEN_DATE_OWNER   		117-1
#define  MNOOFF_ROLL_MAIL_ADDR_1		      	152-1
#define  MNOOFF_ROLL_MAIL_ADDR_2					183-1
#define  MNOOFF_ROLL_MAIL_CITY_STATE   		214-1
#define  MNOOFF_ROLL_MAIL_ZIP 			   	240-1
#define  MNOOFF_ROLL_LAND							250-1
#define  MNOOFF_ROLL_IMPROV   					259-1
#define  MNOOFF_ROLL_PERS_PROP   				268-1
#define  MNOOFF_ROLL_OTHER_VALUE 				277-1
#define  MNOOFF_ROLL_TRADE_FIXT	   			286-1
#define  MNOOFF_ROLL_MOB_HM			   		295-1
#define  MNOOFF_ROLL_HOMEOWNER_EXEMP	   	304-1
#define  MNOOFF_ROLL_VETERAN_EXEMP				313-1
#define  MNOOFF_ROLL_WELFARE_EXEMP  			322-1
#define  MNOOFF_ROLL_CHURCH_EXEMP	   		331-1
#define  MNOOFF_ROLL_OTHER_EXEMP 		   	340-1
#define  MNOOFF_ROLL_NET_VALUE					349-1
#define  MNOOFF_ROLL_PENALTY_MESSAGE   		358-1
#define  MNOOFF_ROLL_COMMENT			       	388-1
#define  MNOOFF_ROLL_TRA      					428-1
#define  MNOOFF_ROLL_TAX_EXEMPT  				432-1
#define  MNOOFF_ROLL_MAIL_ADDR_BROKEN  		433-1 
#define  MNOOFF_ROLL_MAIL_ADDR_CA         	434-1 
#define  MNOOFF_ROLL_MAIL_ADDR_USA				435-1 
#define  MNOOFF_ROLL_GROSS_VALUE    			436-1
#define  MNOOFF_ROLL_TOTAL_EXE_AMOUNT  		445-1
#define  MNOOFF_ROLL_LAND_IMP 			   	454-1
#define  MNOOFF_ROLL_PERCENT_LAND_IMP			463-1
#define  MNOOFF_ROLL_ST_NO     					466-1
#define	MNOOFF_ROLL_ST_NO_FRACT    			473-1
#define	MNOOFF_ROLL_ST_DIR   		   		476-1
#define	MNOOFF_ROLL_ST_NAME				   	478-1
#define	MNOOFF_ROLL_ST_TYPE						508-1
#define	MNOOFF_ROLL_CONDO_UNIT_APT 			513-1
#define	MNOOFF_ROLL_TO_ST_NO 		   		521-1
#define	MNOOFF_ROLL_FILLER1				   	527-1
#define	MNOOFF_ROLL_CITY						   529-1
#define	MNOOFF_ROLL_STATE	   					546-1
#define	MNOOFF_ROLL_ZIP         				548-1
#define	MNOOFF_ROLL_NAME_1		   			557-1
#define	MNOOFF_ROLL_NORM_NAME_1		   		607-1
#define	MNOOFF_ROLL_NORM_CLEANED_NAME_1  	632-1
#define	MNOOFF_ROLL_SWAPPED_NAME_1 			682-1
#define	MNOOFF_ROLL_SALUTATION_CODE   		732-1
#define	MNOOFF_ROLL_SALUTATION			   	734-1
#define	MNOOFF_ROLL_NAME_SUFFIX_CODE			738-1
#define	MNOOFF_ROLL_NAME_SUFFIX	   	   	740-1
#define	MNOOFF_ROLL_NAME_FLAG		   		743-1 
#define	MNOOFF_ROLL_IMAGE					   	744-1
#define	MNOOFF_ROLL_YRBLT						   746-1
#define	MNOOFF_ROLL_EFFYR 						750-1
#define	MNOOFF_ROLL_BEDROOMS 					754-1
#define	MNOOFF_ROLL_BATHS		   				756-1
#define	MNOOFF_ROLL_HBATHS		   			758-1
#define	MNOOFF_ROLL_ROOMS				   		760-1
#define	MNOOFF_ROLL_FIREPLACE			   	762-1 
#define	MNOOFF_ROLL_WOOD_STOVE					763-1 
#define	MNOOFF_ROLL_POOL		   				764-1 
#define	MNOOFF_ROLL_BLDG_AREA	   			765-1
#define	MNOOFF_ROLL_STORIES			   		772-1
#define	MNOOFF_ROLL_GARAGE_SQ_FT		   	774-1
#define	MNOOFF_ROLL_CLASS						   779-1 
#define	MNOOFF_ROLL_QUALITY  					780-1
#define	MNOOFF_ROLL_SEPTIC	   				784-1 
#define	MNOOFF_ROLL_WELL			   			785-1 
#define	MNOOFF_ROLL_FILLER2			   		786-1
#define	MNOOFF_ROLL_MATCHING_LIEN		   	959-1 
#define	MNOOFF_ROLL_LIEN_DATE					960-1 

#define  MNOSIZ_ROLL_PROPERTY_TYPE				2 
#define  MNOSIZ_ROLL_APN         				13
#define  MNOSIZ_ROLL_OWNER_NAME	   			31
#define  MNOSIZ_ROLL_CAREOF						35
#define  MNOSIZ_ROLL_DBA							35
#define  MNOSIZ_ROLL_LIEN_DATE_OWNER			35
#define  MNOSIZ_ROLL_MAIL_ADDR_1    			31
#define  MNOSIZ_ROLL_MAIL_ADDR_2		         31
#define  MNOSIZ_ROLL_MAIL_CITY_STATE			26
#define  MNOSIZ_ROLL_MAIL_ZIP 					10
#define  MNOSIZ_ROLL_LAND        				9 
#define  MNOSIZ_ROLL_IMPROV		      		9 
#define  MNOSIZ_ROLL_PERS_PROP					9 
#define  MNOSIZ_ROLL_OTHER_VALUE		         9 
#define  MNOSIZ_ROLL_TRADE_FIXT  				9 
#define  MNOSIZ_ROLL_MOB_HM		      		9 
#define  MNOSIZ_ROLL_HOMEOWNER_EXEMP			9 
#define  MNOSIZ_ROLL_VETERAN_EXEMP				9 
#define  MNOSIZ_ROLL_WELFARE_EXEMP				9 
#define  MNOSIZ_ROLL_CHURCH_EXEMP				9 
#define  MNOSIZ_ROLL_OTHER_EXEMP     			9 
#define  MNOSIZ_ROLL_NET_VALUE		   		9 
#define  MNOSIZ_ROLL_PENALTY_MESSAGE			30
#define  MNOSIZ_ROLL_COMMENT			         40
#define  MNOSIZ_ROLL_TRA          				4 
#define  MNOSIZ_ROLL_TAX_EXEMPT	   			1 
#define  MNOSIZ_ROLL_MAIL_ADDR_BROKEN			1 
#define  MNOSIZ_ROLL_MAIL_ADDR_CA      		1 
#define  MNOSIZ_ROLL_MAIL_ADDR_USA	   		1 
#define  MNOSIZ_ROLL_GROSS_VALUE 				9 
#define  MNOSIZ_ROLL_TOTAL_EXE_AMOUNT			9 
#define  MNOSIZ_ROLL_LAND_IMP 					9 
#define  MNOSIZ_ROLL_PERCENT_LAND_IMP   		3 
#define  MNOSIZ_ROLL_ST_NO       				7 
#define	MNOSIZ_ROLL_ST_NO_FRACT    			3 
#define	MNOSIZ_ROLL_ST_DIR   		   		2 
#define	MNOSIZ_ROLL_ST_NAME						30
#define	MNOSIZ_ROLL_ST_TYPE     				5 
#define	MNOSIZ_ROLL_CONDO_UNIT_APT	   		8 
#define	MNOSIZ_ROLL_TO_ST_NO 					6 
#define	MNOSIZ_ROLL_FILLER1						2 
#define	MNOSIZ_ROLL_CITY        				17
#define	MNOSIZ_ROLL_STATE							2 
#define	MNOSIZ_ROLL_ZIP      					9 
#define	MNOSIZ_ROLL_NAME_1      				50
#define	MNOSIZ_ROLL_NORM_NAME_1	      		25
#define	MNOSIZ_ROLL_NORM_CLEANED_NAME_1		50
#define	MNOSIZ_ROLL_SWAPPED_NAME_1 			50
#define	MNOSIZ_ROLL_SALUTATION_CODE			2 
#define	MNOSIZ_ROLL_SALUTATION					4 
#define	MNOSIZ_ROLL_NAME_SUFFIX_CODE			2 
#define	MNOSIZ_ROLL_NAME_SUFFIX					3 
#define	MNOSIZ_ROLL_NAME_FLAG					1 
#define	MNOSIZ_ROLL_IMAGE							2 
#define	MNOSIZ_ROLL_YRBLT       				4 
#define	MNOSIZ_ROLL_EFFYR							4 
#define	MNOSIZ_ROLL_BEDROOMS			         2 
#define	MNOSIZ_ROLL_BATHS       				2 
#define	MNOSIZ_ROLL_HBATHS		      		2 
#define	MNOSIZ_ROLL_ROOMS							2 
#define	MNOSIZ_ROLL_FIREPLACE   				1 
#define	MNOSIZ_ROLL_WOOD_STOVE	   			1 
#define	MNOSIZ_ROLL_POOL							1 
#define	MNOSIZ_ROLL_BLDG_AREA					7 
#define	MNOSIZ_ROLL_STORIES		      		2 
#define	MNOSIZ_ROLL_GARAGE_SQ_FT				5 
#define	MNOSIZ_ROLL_CLASS							1 
#define	MNOSIZ_ROLL_QUALITY     				4 
#define	MNOSIZ_ROLL_SEPTIC		      		1 
#define	MNOSIZ_ROLL_WELL							1 
#define	MNOSIZ_ROLL_FILLER2     				173 
#define	MNOSIZ_ROLL_MATCHING_LIEN  			1 
#define	MNOSIZ_ROLL_LIEN_DATE		   		1 

typedef struct _tMnoRoll
{
   char Property_Type[MNOSIZ_ROLL_PROPERTY_TYPE];
   char Apn[MNOSIZ_ROLL_APN];
   char Owner_Name[MNOSIZ_ROLL_OWNER_NAME];
   char Careof[MNOSIZ_ROLL_CAREOF];
   char Dba[MNOSIZ_ROLL_DBA];
   char Lien_Date_Owner[MNOSIZ_ROLL_LIEN_DATE_OWNER];
   char Mail_Addr_1[MNOSIZ_ROLL_MAIL_ADDR_1];
   char Mail_Addr_2[MNOSIZ_ROLL_MAIL_ADDR_2];
   char Mail_City_State[MNOSIZ_ROLL_MAIL_CITY_STATE];
   char Mail_Zip[MNOSIZ_ROLL_MAIL_ZIP];
   char Land[MNOSIZ_ROLL_LAND];
   char Improv[MNOSIZ_ROLL_IMPROV];
   char Pers_Prop[MNOSIZ_ROLL_PERS_PROP];
   char Other_Value[MNOSIZ_ROLL_OTHER_VALUE];
   char Trade_Fixt[MNOSIZ_ROLL_TRADE_FIXT];
   char Mob_Hm[MNOSIZ_ROLL_MOB_HM];
   char Homeowner_Exemp[MNOSIZ_ROLL_HOMEOWNER_EXEMP];
   char Veteran_Exemp[MNOSIZ_ROLL_VETERAN_EXEMP];
   char Welfare_Exemp[MNOSIZ_ROLL_WELFARE_EXEMP];
   char Church_Exemp[MNOSIZ_ROLL_CHURCH_EXEMP];
   char Other_Exemp [MNOSIZ_ROLL_OTHER_EXEMP];
   char Net_Value[MNOSIZ_ROLL_NET_VALUE];
   char Penalty_Message[MNOSIZ_ROLL_PENALTY_MESSAGE];
   char Comment[MNOSIZ_ROLL_COMMENT];
   char Tra[MNOSIZ_ROLL_TRA];
   char Tax_Exempt[MNOSIZ_ROLL_TAX_EXEMPT];
/*
   char Mail_Addr_Broken[MNOSIZ_ROLL_MAIL_ADDR_BROKEN];
   char Mail_Addr_Ca[MNOSIZ_ROLL_MAIL_ADDR_CA];
   char Mail_Addr_Usa[MNOSIZ_ROLL_MAIL_ADDR_USA];
   char Gross_Value [MNOSIZ_ROLL_GROSS_VALUE];
   char Total_Exe_Amount[MNOSIZ_ROLL_TOTAL_EXE_AMOUNT];
   char Land_Imp [MNOSIZ_ROLL_LAND_IMP];
   char Percent_Land_Imp[MNOSIZ_ROLL_PERCENT_LAND_IMP];
   char St_No [MNOSIZ_ROLL_ST_NO];
   char St_No_Fract [MNOSIZ_ROLL_ST_NO_FRACT];
   char St_Dir[MNOSIZ_ROLL_ST_DIR];
   char St_Name[MNOSIZ_ROLL_ST_NAME];
   char St_Type[MNOSIZ_ROLL_ST_TYPE];
   char Condo_Unit_Apt[MNOSIZ_ROLL_CONDO_UNIT_APT];
   char To_St_No [MNOSIZ_ROLL_TO_ST_NO];
   char Filler1[MNOSIZ_ROLL_FILLER1];
   char City[MNOSIZ_ROLL_CITY];
   char State[MNOSIZ_ROLL_STATE];
   char Zip[MNOSIZ_ROLL_ZIP];
   char Name_1[MNOSIZ_ROLL_NAME_1];
   char Norm_Name_1[MNOSIZ_ROLL_NORM_NAME_1];
   char Norm_Cleaned_Name_1[MNOSIZ_ROLL_NORM_CLEANED_NAME_1];
   char Swapped_Name_1 [MNOSIZ_ROLL_SWAPPED_NAME_1];
   char Salutation_Code[MNOSIZ_ROLL_SALUTATION_CODE];
   char Salutation[MNOSIZ_ROLL_SALUTATION];
   char Name_Suffix_Code[MNOSIZ_ROLL_NAME_SUFFIX_CODE];
   char Name_Suffix[MNOSIZ_ROLL_NAME_SUFFIX];
   char Name_Flag[MNOSIZ_ROLL_NAME_FLAG];
   char Image[MNOSIZ_ROLL_IMAGE];
   char Yrblt[MNOSIZ_ROLL_YRBLT];
   char Effyr[MNOSIZ_ROLL_EFFYR];
   char Bedrooms[MNOSIZ_ROLL_BEDROOMS];
   char Baths[MNOSIZ_ROLL_BATHS];
   char Hbaths[MNOSIZ_ROLL_HBATHS];
   char Rooms[MNOSIZ_ROLL_ROOMS];
   char Fireplace[MNOSIZ_ROLL_FIREPLACE];
   char Wood_Stove[MNOSIZ_ROLL_WOOD_STOVE];
   char Pool[MNOSIZ_ROLL_POOL];
   char Bldg_Area[MNOSIZ_ROLL_BLDG_AREA];
   char Stories[MNOSIZ_ROLL_STORIES];
   char Garage_Sq_Ft[MNOSIZ_ROLL_GARAGE_SQ_FT];
   char Class[MNOSIZ_ROLL_CLASS];
   char Quality[MNOSIZ_ROLL_QUALITY];
   char Septic[MNOSIZ_ROLL_SEPTIC];
   char Well[MNOSIZ_ROLL_WELL];
   char Filler2[MNOSIZ_ROLL_FILLER2];
   char Matching_Lien[MNOSIZ_ROLL_MATCHING_LIEN];
   char Lien_Date[MNOSIZ_ROLL_LIEN_DATE];
*/
} MNO_ROLL;

#endif