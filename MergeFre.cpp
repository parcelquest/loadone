/******************************************************************************
 *
 * To build FRE PQ4 file, we have to merge following files:
 * 1) LDR Roll file (ldnav_cd.txt)
 * 2) Situs file (situs_cd.txt)
 * 3) Roll update file (mcnav_cd.txt)
 * 4) Characteristic file (char_cd.txt)
 * 5) Sale file (sales_cd.txt)
 * 6) Addl_cd.txt contains additional owners (currently not used 9/6/2013)
 *
 *    - These files are in ASCII but may include null character.  Open them 
 *      in binary to be safe.
 *
 * Notes:
 *    - Basement area is available but not use
 *    - Currently update file is only get update when run with -U option, not -Lg
 *
 *
 * Run LoadOne with following options:
 *    -L -Mg -Xl: load lien
 *    -U -Lg    : monthly update
 *    -Lg       : process GrGr file
 *    -Mg       : merge GrGr data to R01 file (using output from -Lg)
 *
 * 07/25/2006           Fix bug that leaves null character in record.  Reformat DocNum
 *                      for GrGr data.
 * 12/09/2006 1.3.7     Process PCTRA.TXT file to merge public parcels into roll file.
 * 01/19/2007 1.4.1     Change formula to calculate OtherVal=PersProp since Fixture is
 *                      already included in Impr value.
 * 07/13/2007 1.4.18    Modify Fre_MergePublicParcels() to skip bad records that causes
 *                      corruption in R01 file.
 * 12/17/2007 1.4.33.2  Allow Merge GrGr into R01 without -U option if GrGr loading successful.
 *                      This will trigger BUILDCDA to start indexing on daily GrGr run.
 * 02/15/2008 1.5.5     Adding support for Stanrd Usecode
 * 03/24/2008 1.5.7.2   Format IMAPLINK.
 * 03/25/2008 1.5.8     Remove retired parcels
 * 06/04/2008 1.6.2     Rename Fre_CreateGrGrFile() to Fre_LoadGrGr()
 * 07/10/2008 8.0.3     Change LDR file from EBCDIC to ASCII.  Replace FRE_ROLL with FRE_LIEN
 *                      and modify Fre_MergeLienRoll() to work with new layout.
 * 10/07/2008 8.3.3     All files are now processed in ASCII.  Change logic for merging
 *                      public parcels.  Only add parcels with owner name, ignore the rest.
 * 03/16/2009 8.7.0     Format STORIES as 99.0.  We no longer multiply by 10.
 * 04/02/2009 8.7.2     Add Fre_ExtrLien() and  modify Fre_MergeLienRoll() to add PP_Val
 *                      and MH_Val to R01 record.
 * 05/10/2009 8.8       Set full exemption flag
 * 07/10/2009 9.1.2     Sales_cd.txt contains null, 0x1C & 0x1A in data. Modify
 *                      Fre_MergeSale(), Fre_CreatePQ4(), and Fre_MergePQ4() to read
 *                      sale data as binary.
 * 07/27/2009 9.1.4     Fix Fre_CreateLienRec() and Fre_MergeLien() to correct lien values.
 * 02/06/2010 9.4.0     Extending LEGAL to LEGAL1
 * 04/12/2010 9.5.3     Rename Fre_CreatePQ4() to Fre_Load_LDR() and Fre_MergePQ4() to Fre_Load_Roll().
 *                      Fix bug in Fre_Load_Roll() by open roll file in binary mode to 
 *                      avoid eof character in the middle of the file.
 * 07/13/2010 10.1.1    Open CHARS file in binary mode to fix problem that record might
 *                      have embeded null chars.  Modify Fre_MergeChar() and related functions
 *                      to update full length legal from CHARS file.  This legal will overwrite
 *                      input from roll/ldr since Legal in LDR are mostly situs. Mocify Fre_MergeLien()
 *                      to add CLSEXE_CODE (other exemption) to EXE_CD1.
 * 05/17/2011 10.5.11   Fix LotAcres issue in Fre_MergePublicParcels().
 * 07/07/2011 11.0.2    Add S_HSENO. Add -Xs option and Fre_MergeCumSale() to create sale history.
 * 04/30/2012 11.9.2.2  Fix bug in Fre_Load_Roll() that calling replUnPrtChar() corrupts memory.
 * 09/19/2012 12.2.8    Fix Fre_MergeOwner() to keep Owner1 the same as county provided.
 * 04/28/2013 12.6.1    Use ApplyCumSale() to merge sale.  Change DOCNUM format to 9999999.
 *                      Call updateDocLinks() to update DOCLINKS for use in web import.
 *                      Add Fre_MakeDocLink() to create DocLink. Modify Load_GrGr() to output
 *                      GRGR_DEF record instead of FRE_GREC. 
 * 07/23/2013 13.1.5    Modify Fre_CreateLienRec() to reset Fixt to 0 when it is larger than Impr.
 * 07/25/2013 13.2.7    Modify Fre_ExtrSale() to scan for bad chars in sale file before processing.
 * 09/10/2013 13.7.14   Modify Fre_MergeChar() to add basement sqft and yrblt.
 * 10/02/2013 13.10.0   Use updateVesting() to update Vesting and Etal flag.
 * 10/11/2013 13.11.0   Add 1-4 QBaths to R01
 * 11/01/2013 13.11.8   Fix HBath in Fre_MergeChar() to restore to previous logic.
 * 02/11/2014 13.11.17  Add FRE_DocCode[] and modify Fre_FormatSale() to update DocType, SaleCode, 
 *                      and MultiSale_Flg.
 * 05/21/2014 13.14.5   Fix bug that move owner to seller in Fre_FormatSale().
 * 12/02/2014 14.10.0   Modify Fre_MakeDocLink() to fix document path.
 * 03/13/2015 14.13.0   Update params in updateDocLinks()
 * 05/02/2015 14.14.2   Add Fre_LoadGrGrXls() to support loading GRGR in XLSX format.
 * 02/11/2016 15.5.2    Modify FRE_GRGRXls2Txt() to accept sheet name via param passing and
 *                      modify Fre_LoadGrGrXls() to get sheet name from INI file.  Default to "rof02404"
 * 02/16/2016 15.6.0    Remove duplicate owner name, Format DocNum to 7 digits.
 * 03/12/2016 15.8.1    Load tax data: Base, Items, Delq, Agency.  Move DocCode[] to MergeFre.h
 * 07/25/2016 16.1.2    Fix IMAPLINK in Fre_MergePublicParcels().
 * 08/21/2016 16.2.1    Modify Fre_ParseTaxDetail() to fix TaxCode 0001 by adding a char to avoid duplicate.
 * 08/22/2016 16.2.2    Adding DelqYear to TaxBase.
 * 09/07/2016 16.2.5    Call doTaxPrep() to create empty Tax_Owner table.
 * 09/29/2016 16.2.9    Fix bad char in LEGAL in Fre_MergeNavRoll(), Fre_MergeChar(), and Fre_MergeLien()
 * 10/07/2016 16.3.2    Modify Fre_ParseTaxDetail() to use lTaxYear instead of lLienYear for default tax year
 * 11/20/2016 16.7.1    Modify Fre_FormatSale() to add PctXfer
 * 02/16/2017 16.10.6   Fix refund value in Fre_ParseTaxDetail().
 * 04/02/2017 16.13.5   Modify Fre_Load_TaxBase() to populate TRA in TaxBase record.
 * 07/14/2017 17.1.2    Modify Fre_LoadGrGrXls() to change sort command to dupout the first 44 bytes only, not 144.
 * 09/14/2017 17.2.4    Set isDelq flag and TotalDue in Fre_ParseTaxBase().
 * 09/20/2017 17.2.4.6  Fix TotalDue and only set Delq flag when both DelqDate and DelqAmt are present.
 * 11/21/2017 17.5.0    Call updateDelqFlg() to populate delqYear & defaultAmt in Tax_Base table.
 * 03/31/2018 17.8.0    Fix DocNum bug in FRE_GRGRXls2Txt()
 * 05/02/2018 17.10.7   Modify Fre_ParseTaxDetail() & Fre_ParseTaxBase() to set BillNum=Apn+"-01".
 * 06/18/2018 17.12.3   Add special case for 07505019S in Fre_MergeChar()
 * 10/19/2018 18.5.6    Add -Xa and Fre_ConvStdChar() to create standard char file.
 * 11/03/2018 18.5.12   Modify FRE_GRGRXls2Txt to ignore "NOTICE OF ASSESSMENT" document
 * 11/04/2018 18.5.13   Modify FRE_GRGRXls2Txt() to support only sheet name "rof02024"
 * 11/24/2018 18.6.4    Add Fre_LoadGrGrCsv() to support new GrGr CSV format.
 * 12/12/2018 18.7.1    Fix broken records in Fre_LoadGrGrCsv() and add DocType if available.
 * 04/12/2019 18.11.3   Increase acApn[] array to fix multi APN problem.
 * 06/29/2019 19.0.1    Increase sort buffer in GrGr_SortMerge().  Increase size of 
 *                      acApn & apItems[] in Fre_LoadGrGrCsv() to avoid memory overrun.
 * 07/08/2019 19.0.2    Add -X8 option and Fre_ExtrProp8() & Fre_MergeProp8().  
 *                      Modify Fre_Load_LDR() to populate Prop8 flag.
 * 08/21/2019 19.1.0    Modify Fre_LoadGrGrCsv() to increase input buffer and APN list.  
 *                      GRGR APN can be as long as 38000 bytes.
 * 11/05/2019 19.4.2    Modify Fre_ParseTaxDetail() & Fre_Load_TaxDetail() to update tb_TotalRate.
 * 11/12/2019 19.4.5    Add Fre_Load_TaxBase_Supp() and Fre_Load_TaxDetail_Supp() to load supplemental tax.
 * 11/13/2019 19.5.1    Revise Fre_ParseTaxDetailSupp() to assign better guess of tax code.
 * 05/05/2020 19.9.5    Modify Fre_FmtChar() & Fre_MergeChar() to pull LotAcre from legal when LotArea=1.
 * 06/18/2020 20.0.0    Add Exemption codes to R01 file
 * 07/11/2020 20.1.4    Modify Fre_Load_LDR() to ignore updating Pro8 if file not available.
 * 10/28/2020 20.3.6    Modify Fre_MergeChar() to populate PQZoning.
 * 11/25/2020 20.5.3    Modify Fre_Load_TaxBase_Supp() to import Delq table.
 * 12/02/2020 20.5.4    Modify Fre_ExtrSale() to sort output file on TransDate instead of RecDate.
 *                      Fix TransDate in Fre_FormatSale().
 * 07/02/2021 21.0.1    Modify Fre_MergeChar() to populate QualityClass and to fix problem when 
 *                      there is more than 9 baths. Modify Fre_MergeMAdr() to correct misspelled zipcode.
 * 08/11/2021 21.1.2    Modify Fre_MergeChar() to set LotSqft=0 when greater than 999,999,999
 * 09/30/2021 21.2.6    Modify Fre_FmtChar() to use lot acres from legal since LotAcres field has too many errors.
 * 10/01/2021 21.2.6.3  Modify Fre_MergeChar() to use lot acres from legal.
 * 10/21/2021 21.2.10   Modify Fre_Load_TaxBase_Supp() to set LastTaxFileDate based on supplemental file.
 * 10/14/2021 21.3.2    Rename GrGr_SortMerge() to Fre_GrGr_SortMerge().
 * 04/26/2022 21.9.0    Use acGrBkTmpl as backup folder for GrGr files. 
 * 07/29/2022 22.1.2    Modify Fre_FmtChar() & Fre_MergeChar() to fix acreage extracted from legal.
 * 11/17/2022 22.3.5    Modify Fre_ParseTaxDetailSupp() to add tax rate to supplemental detail records.
 * 03/17/2023 22.5.1    Set YR_ASSD for public parcels in Fre_MergePublicParcels() using current roll year.
 * 07/02/2023 23.0.0    Handle special cases in Fre_MergeMAdr().
 * 07/12/2023 23.0.2    Fix M_STRNAME issue in Fre_MergeMAdr().
 * 07/16/2023 23.0.4    Increase apItems[] from 512 to 2048 in Fre_LoadGrGrCsv().
 * 08/29/2023 23.1.4    Fix supp tax update in Fre_Load_TaxDetail_Supp().
 * 02/01/2024 23.6.0    Modify Fre_MergeMAdr() to fix mail unit issue.
 * 03/06/2024 23.6.3    Modify Fre_MergeMAdr() to fix bad zipcodes due to typo.
 * 04/03/2024 23.8.1    Add code to support -Xn & -Mn options (using NDC sale data) 
 * 06/13/2024 23.9.2    Modify Fre_LoadGrGrCsv() to fix date format of new GRGR input file.
 * 07/16/2024 24.0.4    Modify Fre_MergeLien() to add ExeType.
 * 07/19/2024 24.0.7    Modify Fre_MergePublicParcels() & Fre_Load_Roll() to fix duplicate APN issue.
 * 01/17/2025 24.4.4    Modify Fre_ParseTaxDetail() to update TaxRate on the fly for supllemental.
 *
 ******************************************************************************/

#include "stdafx.h"
#include "Prodlib.h"
#include "CountyInfo.h"
#include "R01.h"
#include "RecDef.h"
#include "Tables.h"
#include "Logs.h"
#include "LoadDef.h"
#include "Utils.h"
#include "XlatTbls.h"
#include "doSort.h"
#include "doOwner.h"
#include "doGrgr.h"
#include "formatApn.h"
#include "SaleRec.h"
#include "LoadOne.h"
#include "MergeFre.h"
#include "UseCode.h"
#include "LOExtrn.h"
#include "PQ.h"
#include "CSpreadSheet.h"
#include "Tax.h"
#include "CharRec.h"
#include "NdcExtr.h"

static   char  acSitusFile[_MAX_PATH], acNavFile[_MAX_PATH], acPersFile[_MAX_PATH], acLastApn[32];
static   char  acSluFile[_MAX_PATH], acUnsFile[_MAX_PATH], acEtalFile[_MAX_PATH], acProp8File[_MAX_PATH];
static   FILE  *fdChar, *fdSitus, *fdNav, *fdPubParcel, *fdProp8;
static   long  lCharSkip, lSaleSkip, lCChrSkip, lGrGrSkip, lSitusSkip, lPubParcelAdd, lProp8Skip, lProp8Match;
static   long  lCharMatch, lSaleMatch, lCChrMatch, lSeqNum, lGrGrMatch, lSitusMatch, lPubParcelMatch;
static   HANDLE   fhOut;

/******************************* Fre_ExtrProp8 ******************************
 *
 * Extract prop 8
 *
 ****************************************************************************/

int Fre_ExtrProp8(char *pProp8File)
{
   char  *pTmp, acBuf[MAX_RECSIZE], acRollRec[MAX_RECSIZE];
   long  lCnt=0, iProp8Cnt=0;
   FILE  *fdOut;

   LogMsg0("Extract Prop8 flag");

   // Open roll file
   fdProp8 = fopen(pProp8File, "r");
   if (fdProp8 == NULL)
   {
      LogMsg("***** Error opening Prop8 file: %s\n", pProp8File);
      return 2;
   }

   // Open Output file
   sprintf(acProp8File, acProp8Tmpl, myCounty.acCntyCode, myCounty.acCntyCode, myCounty.acYearAssd);
   LogMsg("Create prop8 output file %s", acProp8File);
   fdOut = fopen(acProp8File, "w");
   if (fdOut == NULL)
   {
      LogMsg("***** Error opening Prop8 extract file: %s\n", acProp8File);
      return 4;
   }

   // print header
   fputs("APN,Prop8_Flg\n", fdOut);

   // Merge loop
   while (!feof(fdProp8))
   {
      // Get roll rec
      pTmp = fgets(acRollRec, MAX_RECSIZE, fdProp8);
      if (!pTmp)
         break;

      iTokens = ParseStringNQ1(acRollRec, ',', 45, apTokens);
      if (iTokens > 35 && *apTokens[1] == 'Y')
      {
         sprintf(acBuf, "%s,Y\n", apTokens[0]);

         fputs(acBuf, fdOut);
         iProp8Cnt++;
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdProp8)
      fclose(fdProp8);
   if (fdOut)
      fclose(fdOut);

   LogMsgD("\nTotal prop8 records output: %u", iProp8Cnt);
   LogMsgD("Total records processed:    %u\n", lCnt);

   return 0;
}

/**********************************************************************
 * Sort and merge GRGR_DEF record
 * Each record may contain either Grantor or Grantee
 **********************************************************************/

int Fre_GrGr_SortMerge(char *pInFile, char *pSortFile)
{
   FILE  *fdIn, *fdOut;
   long  lRet, lCnt;
   int   iGrantors, iGrantees, iTmp;
   char  acTmpFile[_MAX_PATH], acRec[2048], acTmp[_MAX_PATH], acLGrantee[SIZ_GR_NAME], acLGrantor[SIZ_GR_NAME];
   char  *pTmp;

   GRGR_DEF curGrGrRec, *pRec = (GRGR_DEF *)&acRec[0];

   LogMsg("Sort & Merge GrGr - preprocessing");

   // Temp file name
   sprintf(acTmpFile, "%s\\%s\\%s_Grgr.in", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);

   // Sort by APN, Date, Doc#, remove record has same APN, Doc#, DocDate, Owners
   strcpy(acTmp,"S(17,14,C,A,37,8,C,A,1,12,C,A,45,20,C,A,125,10,C,D) OMIT(17,1,C,EQ,\" \") DUPO(B8192,1,150)");
   // Sort Fre_GrGr.tmp to Fre_GrGr.in
   lRet = sortFile(pInFile, acTmpFile, acTmp);
   if (lRet < 2)
      return lRet;

   LogMsg("Open input file %s", acTmpFile);
   if (!(fdIn = fopen(acTmpFile, "r")))
   {
      LogMsg("***** Error opening file input file %s", acTmpFile);
      return -1;
   }

   // Create output file
   fdOut = fopen(pSortFile, "w");
   if (fdOut == NULL)
   {
      printf("Error creating GrGr output file: %s\n", pSortFile);
      return -1;
   }

   // Initialize new one
   memset((void *)&curGrGrRec, ' ', sizeof(GRGR_DEF));
   lCnt = 0;
   while (!feof(fdIn))
   {
      pTmp = fgets(acRec, 2048, fdIn);
      if (!pTmp) break;

      if (!isdigit(*pTmp) || pRec->APN[0] == ' ')
         continue;

      // Is new doc?
      if (memcmp(pRec->DocNum, curGrGrRec.DocNum, GSIZ_DOCNUM) || memcmp(pRec->APN, curGrGrRec.APN, iApnLen))
      {
         // Save last document
         if (curGrGrRec.DocNum[0] > ' ')
         {
            if (iGrantees > 0)
            {
               iTmp = sprintf(acTmp, "%d", iGrantees);
               memcpy(curGrGrRec.NameCnt, acTmp, iTmp);
            }
            curGrGrRec.CRLF[0] = 10;
            curGrGrRec.CRLF[1] = 0;

            fputs((char *)&curGrGrRec, fdOut);
            lCnt++;
         }

         // Copy new one
         memcpy(&curGrGrRec, acRec, sizeof(GRGR_DEF));
         if (curGrGrRec.Grantees[0].NameType[0] == 'E')
         {
            iGrantees = 1;
            iGrantors = 0;
            memcpy(acLGrantee, pRec->Grantees[0].Name, SIZ_GR_NAME);
         } else if (curGrGrRec.Grantors[0].NameType[0] == 'O')
         {
            iGrantees = 0;
            iGrantors = 1;
            memcpy(acLGrantor, pRec->Grantors[0].Name, SIZ_GR_NAME);
         } else
            iGrantors = iGrantees = 0;
      } else
      {
         // Name
         if (pRec->Grantees[0].NameType[0] == 'E')
         {
            // Grantee
            if (iGrantees < MAX_NAMES)
            {
               // Check to avoid duplicate name
               if (memcmp(acLGrantee, pRec->Grantees[0].Name, SIZ_GR_NAME))
               {
                  memcpy(curGrGrRec.Grantees[iGrantees].Name, pRec->Grantees[0].Name, SIZ_GR_NAME);
                  curGrGrRec.Grantees[iGrantees].NameType[0] = 'E';
                  memcpy(acLGrantee, pRec->Grantees[0].Name, SIZ_GR_NAME);
                  iGrantees++;
               }
            } else
            {
               curGrGrRec.MoreName = 'Y';
               iGrantees++;
            }
         } else  if (pRec->Grantors[0].NameType[0] == 'O')
         {
            // Grantor
            if (iGrantors < MAX_NAMES)
            {
               // Check to avoid duplicate name
               if (memcmp(acLGrantor, pRec->Grantors[0].Name, SIZ_GR_NAME))
               {
                  memcpy(curGrGrRec.Grantors[iGrantors].Name, pRec->Grantors[0].Name, SIZ_GR_NAME);
                  curGrGrRec.Grantors[iGrantors].NameType[0] = 'O';
                  memcpy(acLGrantor, pRec->Grantors[iGrantors].Name, SIZ_GR_NAME);
                  iGrantors++;
               }
            } else
               iGrantors++;
         }
      }
      if (!(lCnt % 1000))
         printf("\r%d", lCnt);
   }

   // Write out last record
   if (curGrGrRec.DocNum[0] > ' ')
   {
      iTmp = sprintf(acTmp, "%d", iGrantees);
      memcpy(curGrGrRec.NameCnt, acTmp, iTmp);
      curGrGrRec.CRLF[0] = 10;
      curGrGrRec.CRLF[1] = 0;
      fputs((char *)&curGrGrRec, fdOut);
      lCnt++;
   }
   fclose(fdIn);
   fclose(fdOut);

   LogMsg("GrGr_SortMerge completed with %d records output", lCnt);
   if (bClean)
      _unlink(acTmpFile);

   return lCnt;
}

/**********************************************************************
 *
 **********************************************************************/

int Fre_SortMerge_GrGr(char *pFileName, char *pSortFile)
{
   FILE  *fdIn, *fdOut;
   long  lRet;
   char  acTmpFile[_MAX_PATH], acRec[512];
   char  *pTmp;
   FRE_GRGR *pRec;

   // Temp file name
   strcpy(acTmpFile, pSortFile);
   pTmp = strrchr(acTmpFile, '.');
   strcpy(pTmp, ".tmp");

   // Create temp file
   fdOut = fopen(acTmpFile, "w");
   if (fdOut == NULL)
   {
      printf("Error creating temporary GrGr output file: %s\n", acTmpFile);
      return -1;
   }

   LogMsg("Open input file %s", pFileName);
   if (!(fdIn = fopen(pFileName, "r")))
   {
      fclose(fdOut);
      LogMsg("***** Error opening file input file %s", pFileName);
      return -1;
   }

   pRec = (FRE_GRGR *)&acRec[0];
   while (!feof(fdIn))
   {
      pTmp = fgets(acRec, 512, fdIn);
      if (!pTmp) break;

      if (!isdigit(*pTmp) || pRec->acFmtApn[0] == ' ')
         continue;

      fputs(acRec, fdOut);
      //lRet++;
   }

   fclose(fdIn);
   fclose(fdOut);

   LogMsg("Sort input file %s to %s", acTmpFile, pSortFile);

   // Sort up to name and ignore xref name
   sprintf(acRec, "S(1,150,C,A) DUPO(1,150)");
   lRet = sortFile(acTmpFile, pSortFile, acRec);

   if (lRet <= 1)
   {
      *pSortFile = 0;
      LogMsg("***** Error sorting %s.  Please contact Sony", acTmpFile);
      lRet = -2;
   } else
   {
      if (bClean)
         _unlink(acTmpFile);
      lRet = 0;
   }

   return lRet;
}

/********************************** MergeGrGr *******************************
 *
 * Merge new sale data into current sale.  Move other sales accordingly.
 * This version merges GRGR_DEF into R01 for DEED only with or without SaleAmt.
 * 
 * Not complete ... 2bd
 *
 *****************************************************************************

int Fre_MergeGrGr(GRGR_DEF *pSaleRec, char *pOutbuf, int iUpdtFlg)
{
   long  iTmp, lCurSaleDt, lLstSaleDt, lPrice, lLastAmt;
   char  *pTmp, acDocType[32];

   // Check DocType
   iTmp = findDocType(pSaleRec->DocTitle, (IDX_TBL5 *)&FRE_DocTbl[0]);
   if (iTmp != 1 && iTmp != 13)
      return 0;
//      memcpy(pCSale->DocType, FRE_DocTbl[iTmp].pCode, FRE_DocTbl[iTmp].iCodeLen);
//      pCSale->NoneSale_Flg = FRE_DocTbl[iTmp].flag;

   // Drop if no sale price 05/13/2010 sn
   lPrice = atoin(pSaleRec->SalePrice, SIZ_SALE1_AMT); 
   if (!lPrice && acDocType[0] != '1')
      return 0;

   if (lPrice > 1000 && acDocType[0] < '1')
      strcpy(acDocType, "1     ");

   lCurSaleDt = atoin(pSaleRec->DocDate, SIZ_SALE1_DT);
   lLstSaleDt = atoin(pOutbuf+OFF_SALE1_DT, SIZ_SALE1_DT);
   if (lCurSaleDt < lLstSaleDt)
      return -1;

   if (lCurSaleDt == lLstSaleDt)
   {
      if (*(pOutbuf+OFF_SALE1_DOC) == ' ' && pSaleRec->DocNum[0] > ' ')
         memcpy(pOutbuf+OFF_SALE1_DOC, pSaleRec->DocNum, SIZ_SALE1_DOC);

      lLastAmt = atoin(pOutbuf+OFF_SALE1_AMT, SIZ_SALE1_AMT);
      if (lLastAmt > 0)
         return 0;
   } else
   {
      // Move sale2 to sale3
      memcpy(pOutbuf+OFF_SALE3_DOC, pOutbuf+OFF_SALE2_DOC, SIZ_SALE3_DOC);
      memcpy(pOutbuf+OFF_SALE3_DT, pOutbuf+OFF_SALE2_DT, SIZ_SALE3_DT);
      memcpy(pOutbuf+OFF_SALE3_DOCTYPE, pOutbuf+OFF_SALE2_DOCTYPE, SIZ_SALE3_DOCTYPE);
      memcpy(pOutbuf+OFF_SALE3_AMT, pOutbuf+OFF_SALE2_AMT, SIZ_SALE3_AMT);
      memcpy(pOutbuf+OFF_SALE3_CODE, pOutbuf+OFF_SALE2_CODE, SIZ_SALE3_CODE);
      *(pOutbuf+OFF_AR_CODE3) = *(pOutbuf+OFF_AR_CODE2);

      // Move sale1 to sale2
      memcpy(pOutbuf+OFF_SALE2_DOC, pOutbuf+OFF_SALE1_DOC, SIZ_SALE3_DOC);
      memcpy(pOutbuf+OFF_SALE2_DT, pOutbuf+OFF_SALE1_DT, SIZ_SALE3_DT);
      memcpy(pOutbuf+OFF_SALE2_DOCTYPE, pOutbuf+OFF_SALE1_DOCTYPE, SIZ_SALE3_DOCTYPE);
      memcpy(pOutbuf+OFF_SALE2_AMT, pOutbuf+OFF_SALE1_AMT, SIZ_SALE3_AMT);
      memcpy(pOutbuf+OFF_SALE2_CODE, pOutbuf+OFF_SALE1_CODE, SIZ_SALE3_CODE);
      *(pOutbuf+OFF_AR_CODE2) = *(pOutbuf+OFF_AR_CODE1);
   }

   // Update current sale
   memcpy(pOutbuf+OFF_SALE1_DOC, pSaleRec->DocNum, SIZ_SALE3_DOC);
   memcpy(pOutbuf+OFF_SALE1_DT, pSaleRec->DocDate, SIZ_SALE3_DT);
   memcpy(pOutbuf+OFF_SALE1_DOCTYPE, acDocType, 3);

   // Remove sale code - 
   memset(pOutbuf+OFF_SALE1_CODE, ' ', SALE_SIZ_SALECODE);

   if (pSaleRec->Grantors[0].Name[0] > ' ')
      memcpy(pOutbuf+OFF_SELLER, pSaleRec->Grantors[0].Name, SIZ_SELLER);

   memcpy(pOutbuf+OFF_SALE1_AMT, pSaleRec->SalePrice, SIZ_SALE1_AMT);
   *(pOutbuf+OFF_AR_CODE1) = 'R';

   // Update transfers
   lLstSaleDt = atoin(pOutbuf+OFF_TRANSFER_DT, SIZ_SALE1_DT);
   if (lCurSaleDt > lLstSaleDt)
   {
      if (iUpdtFlg & GRGR_UPD_XFR)
      {
         memcpy(pOutbuf+OFF_TRANSFER_DOC, pSaleRec->DocNum, SIZ_TRANSFER_DOC);
         memcpy(pOutbuf+OFF_TRANSFER_DT, pSaleRec->DocDate, SIZ_TRANSFER_DT);
      }

      // Update owner
      if (iUpdtFlg & GRGR_UPD_OWNER)
      {
      }
   }

   return 1;
}

/***************************** Fre_CreateGrGrFile *****************************
 *
 * Logic:
 *    - Load all docs
 *    - If there is tax amount, calculate sale price
 *    - Merge all docs with same doc#.
 * Return 0 if successful, otherwise error
 *
 *****************************************************************************/

int Fre_LoadGrGr(char *pCnty)
{
   char     *pTmp, acRec[MAX_RECSIZE];
   char     acTmp[256], cFileCnt=1;
   char     acGrGrIn[_MAX_PATH], acGrGrOut[_MAX_PATH], acGrGrBak[_MAX_PATH];
   char     acInFile[_MAX_PATH], acSrtFile[_MAX_PATH];
   char     acLGrantor[GSIZ_NAME+1], acLGrantee[GSIZ_NAME+1];
   struct   _finddata_t  sFileInfo;

   FILE     *fdIn, *fdOut;
   FRE_GRGR *pRec;
   GRGR_DEF curGrGrRec;

   int      iRet, iTmp, iMatched=0, iNotMatched=0;
   int      iGrantors, iGrantees, iApn1, iApn2;
   BOOL     bEof=false;
   long     lCnt, lHandle, lSalePrice;
   double   dTax;

   // Get raw file name
   GetIniString(pCnty, "GrGrIn", "", acGrGrIn, _MAX_PATH, acIniFile);
   LogMsg("Load %s GrGr file: %s", pCnty, acGrGrIn);

   // Prepare backup folder
   sprintf(acGrGrBak, acGrBkTmpl, pCnty, acToday);
   if (_access(acGrGrBak, 0))
      _mkdir(acGrGrBak);

   // Create temp output file
   sprintf(acGrGrOut, acGrGrTmpl, pCnty, pCnty, "TMP");

   LogMsg("Open output file %s", acGrGrOut);
   fdOut = fopen(acGrGrOut, "w");
   if (fdOut == NULL)
   {
      printf("Error creating GrGr output file: %s\n", acGrGrOut);
      return -1;
   }

   // Open Input file
   lHandle = _findfirst(acGrGrIn, &sFileInfo);
   if (lHandle > 0)
   {
      pTmp = strrchr(acGrGrIn, '\\');
      if (pTmp)
         *pTmp = 0;
      iRet = 0;
   } else
   {
      bGrGrAvail = false;
      LogMsg("*** No new GrGr file to process");
      fclose(fdOut);
      return 1;
   }

   // Create backup folder
   if (_access(acGrGrBak, 0))
      _mkdir(acGrGrBak);

   pRec = (FRE_GRGR *)&acRec[0];
   lCnt = 0;
   while (!iRet)
   {
      // Create input name
      sprintf(acInFile, "%s\\%s", acGrGrIn, sFileInfo.name);
      sprintf(acSrtFile, "%s\\%s\\%s", acTmpPath, myCounty.acCntyCode, sFileInfo.name);

      // Update GrGr file date
      iRet = getFileDate(acInFile);
      if (iRet > lLastGrGrDate)
         lLastGrGrDate = iRet;

      // Sort input file
      iRet = Fre_SortMerge_GrGr(acInFile, acSrtFile);
      if (iRet)
         continue;

      LogMsg("Open input file %s", acSrtFile);
      if (!(fdIn = fopen(acSrtFile, "r")))
      {
         LogMsg("***** Error opening file %s", acSrtFile);
         break;
      }

      // Initialize new one
      memset((void *)&curGrGrRec, ' ', sizeof(GRGR_DEF));
      lSalePrice = 0;
      iGrantors  = iGrantees = 0;

      while (!feof(fdIn))
      {
         pTmp = fgets(acRec, 1024, fdIn);
         if (!pTmp)
            break;

         pRec->filler1[0] = 0;
         pRec->filler2[0] = 0;
         pRec->filler3[0] = 0;
         pRec->filler4[0] = 0;
         pRec->filler5[0] = 0;
         pRec->filler6[0] = 0;
         pRec->filler7[0] = 0;

         // Is new doc?
         if (memcmp(pRec->acDocNum, curGrGrRec.DocNum, GSIZ_DOCNUM))
         {
            // Save last document
            if (curGrGrRec.DocNum[0] > ' ')
            {
               curGrGrRec.CRLF[0] = 10;
               curGrGrRec.CRLF[1] = 0;

               fputs((char *)&curGrGrRec, fdOut);
            }

            // Initialize new one
            memset((void *)&curGrGrRec, ' ', sizeof(FRE_GREC));
            iGrantors = iGrantees = 0;
            lSalePrice = 0;

            // Convert sale date from DD-MMM-YYYY to YYYYMMDD
            pTmp = dateConversion(pRec->acDocDate, acTmp, DD_MMM_YYYY);
            memcpy(curGrGrRec.DocDate, acTmp, SALE_SIZ_DOCDATE);
            memcpy(curGrGrRec.DocNum, pRec->acDocNum, GSIZ_DOCNUM);

            // APN
            iApn1 = iApn2 = 0;
            while (pRec->acFmtApn[iApn1])
            {
               // Ignore '-' and single quote \'
               if (pRec->acFmtApn[iApn1] != 45 && pRec->acFmtApn[iApn1] != 39)
                  curGrGrRec.APN[iApn2++] = pRec->acFmtApn[iApn1];
               iApn1++;
            }

            // Remove '-' and '(' in DocType
            strcpy(acTmp, pRec->acDocType);
            if ((pTmp=strchr(acTmp, '-')) || (pTmp=strchr(acTmp, '(')) )
            {
               *pTmp = 0;
               memcpy(curGrGrRec.DocTitle, pTmp, strlen(pTmp));
            } else
               memcpy(curGrGrRec.DocTitle, pRec->acDocType, GSIZ_DOCTYPE);
         }

         // Tax Amt
         dTax = atofn(pRec->acTaxAmt, GSIZ_DOCTAX);
         lSalePrice = (long)(dTax*SALE_FACTOR);
         if (lSalePrice > 0)
         {
            sprintf(acTmp, "%*u", GSIZ_DOCTAX, (long)(dTax*100.0));
            memcpy(curGrGrRec.Tax, acTmp, GSIZ_DOCTAX);
            sprintf(acTmp, "%*u", GSIZ_DOCTAX, lSalePrice);
            memcpy(curGrGrRec.SalePrice, acTmp, GSIZ_DOCTAX);
         }

         // Name
         myTrim(pRec->acName);
         if (pRec->acNameType[0] == 'E')
         {
            // Grantee
            if (iGrantees < MAX_NAMES)
            {
               // Check to avoid duplicate name
               if (strcmp(acLGrantee, pRec->acName))
               {
                  memcpy(curGrGrRec.Grantees[iGrantees].Name, pRec->acName, strlen(pRec->acName));
                  curGrGrRec.Grantees[iGrantees].NameType[0] = 'E';
                  strcpy(acLGrantee, pRec->acName);
                  iGrantees++;
               }
            } else
            {
               curGrGrRec.MoreName = 'Y';
               iGrantees++;
            }
         } else  if (pRec->acNameType[0] == 'R')
         {
            // Grantor
            if (iGrantors < MAX_NAMES)
            {
               // Check to avoid duplicate name
               if (strcmp(acLGrantor, pRec->acName))
               {
                  memcpy(curGrGrRec.Grantors[iGrantors].Name, pRec->acName, strlen(pRec->acName));
                  curGrGrRec.Grantors[iGrantors].NameType[0] = 'O';
                  strcpy(acLGrantor, pRec->acName);
                  iGrantors++;
               }
            } else
               iGrantors++;
         }

         iMatched++;

         lCnt++;
         if (!(lCnt % 1000))
            printf("\r%d", lCnt);
      }

      // Write out last record
      if (curGrGrRec.DocNum[0] > ' ')
      {
         curGrGrRec.CRLF[0] = 10;
         curGrGrRec.CRLF[1] = 0;
         fputs((char *)&curGrGrRec, fdOut);
      }

      LogMsg("Number of matched records: %d", iMatched);
      LogMsg("Total records so far     : %d", lCnt);

      fclose(fdIn);

      // Move file
      sprintf(acTmp, "%s\\%s", acGrGrBak, sFileInfo.name);
      sprintf(acRec, "%s\\%s", acGrGrIn, sFileInfo.name);
      rename(acRec, acTmp);

      // Find next file
      iRet = _findnext(lHandle, &sFileInfo);
   }

   // Close handle
   _findclose(lHandle);

   // Close files
   if (fdOut)
      fclose(fdOut);

   LogMsg("Total matched records    : %d", iMatched);
   LogMsg("Total processed records  : %u", lCnt);

   // Update cummulated grgr file
   if (iRet > 0)
   {
      // Sort output file and dedup
      sprintf(acGrGrIn, acGrGrTmpl, pCnty, pCnty, "sls"); // Fre_GrGr.sls
      if (!_access(acGrGrIn, 0))
         sprintf(acInFile, "%s+%s", acGrGrOut, acGrGrIn);
      else
         strcpy(acInFile, acGrGrOut);

      // Sort by APN, RecDate, DocNum, Sale price
      sprintf(acTmp, "S(%d,12,C,A,%d,8,C,A,%d,12,C,A,125,1,C,D) F(TXT) OMIT(%d,1,C,EQ,\" \") DUPO(B2048,1,130) ", 
         OFF_GD_APN, OFF_GD_DOCDATE, OFF_GD_DOCNUM, OFF_GD_APN);

      // Sort on APN asc, DocDate asc, DocNum asc
      sprintf(acGrGrOut, acGrGrTmpl, pCnty, pCnty, "dat"); // Fre_GrGr.dat
      lCnt = sortFile(acInFile, acGrGrOut, acTmp);
      if (lCnt > 0)
      {
         if (!_access(acGrGrIn, 0))
            DeleteFile(acGrGrIn);

         // Rename Dat to SLS file
         iTmp = rename(acGrGrOut, acGrGrIn);
         LogMsg("GRGR process completed: %d", lCnt);
         iRet = 0;
      } else
      {
         iRet = -1;
         LogMsg("***** Error sorting output file in Fre_LoadGrGrXsl()");
      }
   }

   printf("\n");
   LogMsgD("Total GrGr records output: %u", lCnt);

   if (lCnt > 0)
      return 0;
   else
      return -5;
}

/********************************** GRGRXls2Txt *******************************
 *
 * Convert XLSX to TXT format.  Skip 2 header rows.
 * Do not process sheet name "rof02401".
 *
 ******************************************************************************/

#define FRE_GRGR_COLS      7
#define FRE_GRGR_DOCNUM    0
#define FRE_GRGR_DOCDATE   1
#define FRE_GRGR_DOCTYPE   2
#define FRE_GRGR_DOCTAX    3
#define FRE_GRGR_APN			4
#define FRE_GRGR_RECTYPE	5
#define FRE_GRGR_NAME		6
#define FRE_GRGR_XNAME     7

int FRE_GRGRXls2Txt(char *pInfile, FILE *fdOut, char *pShtName, int iStartRow=3)
{
   char     *pTmp, acTmp[256], acDocNum[32];
   int      iRet, iRowCnt, lCnt, iRows, iCols, iSkipRows, iRecCnt, iLen;
	double	dTax;
	long		lPrice, lDocNum;

   GRGR_DEF curGrGrRec;

   CString  strTmp, strApn;
   CStringArray asRows;

   printf("\nProcess GrGr file %s\n", pInfile);
   LogMsg("Process GrGr file %s", pInfile);

   // Open input file - no backup
   CSpreadSheet sprSht(pInfile, pShtName, false, "xlsx");

   iRows = sprSht.GetTotalRows();
   iCols = sprSht.GetTotalColumns();
   if (iCols < FRE_GRGR_COLS)
   {
      LogMsg("Skip: %s.", pInfile);
      return 0;
   }

   // Initialize pointers
   lCnt = 0;
   iSkipRows = iRecCnt = 0;
   sprSht.GetFieldNames(asRows);

   // Skip firsst header row
   for (iRowCnt = iStartRow; iRowCnt <= iRows; iRowCnt++)
   {
      memset(&curGrGrRec, ' ', sizeof(GRGR_DEF));
      sprSht.ReadRow(asRows, iRowCnt);
      lCnt++;
   
      if (asRows.GetSize() < FRE_GRGR_COLS)
      {
         iSkipRows++;
         LogMsg("Skip row# %d", iRowCnt);
      } else
      {
         strApn = asRows.GetAt(FRE_GRGR_APN);  
         if (!strApn.IsEmpty())
         {
            // DocType
            strcpy(acTmp, asRows.GetAt(FRE_GRGR_DOCTYPE));
            if (!memcmp(acTmp, "NOTICE OF ASSESSMENT", 12))
               continue;
            memcpy(curGrGrRec.DocTitle, acTmp, strlen(acTmp));

            // APN - remove hyphen
            strcpy(acTmp, strApn);
				iRet = remChar(acTmp, '-');
            memcpy(curGrGrRec.APN, acTmp, iRet);

            // DocDate
            char acDate[32];
            strcpy(acTmp, asRows.GetAt(FRE_GRGR_DOCDATE));
            pTmp = dateConversion(acTmp, acDate, YYYY_MM_DD);
            if (pTmp)
               memcpy(curGrGrRec.DocDate, acDate, 8);
            else
               strTmp = asRows.GetAt(1);  

            // DocNum 
            strcpy(acDocNum, asRows.GetAt(FRE_GRGR_DOCNUM));
            iLen = strlen(acDocNum);
            if (iLen < 10 && isNumber(acDocNum))
            {
               lDocNum = atol(acDocNum);
               iLen = sprintf(acDocNum, "%.7d", lDocNum);
               memcpy(curGrGrRec.DocNum, acDocNum, iLen);
            } else if (iLen >= 12)
            {
               lDocNum = atol(&acDocNum[5]);
               iLen = sprintf(acDocNum, "%.7d", lDocNum);
               memcpy(curGrGrRec.DocNum, acDocNum, iLen);
            } else
            {
               vmemcpy(curGrGrRec.DocNum, acDocNum, iLen);
               LogMsg("??? Questionable DocNum: %s [%s]", acDocNum, strApn);
            }

				// DocTax
            strcpy(acTmp, asRows.GetAt(FRE_GRGR_DOCTAX));
				if (acTmp[0] > '0')
				{
					dTax = atof(acTmp);
					iRet = sprintf(acTmp, "%10d", (long)(100*dTax));
					memcpy(curGrGrRec.Tax, acTmp, iRet);
					lPrice = (long)(dTax*SALE_FACTOR);
					iRet = sprintf(acTmp, "%10d", lPrice);
					memcpy(curGrGrRec.SalePrice, acTmp, iRet);
				}

				// Name type
				if (asRows.GetAt(FRE_GRGR_RECTYPE).Left(1) == "R")
				{
					// Seller
					strcpy(acTmp, asRows.GetAt(FRE_GRGR_NAME));
					memcpy(curGrGrRec.Grantors[0].Name, acTmp, strlen(acTmp));
					curGrGrRec.Grantors[0].NameType[0] = 'O';
				} else if (asRows.GetAt(FRE_GRGR_RECTYPE).Left(1) == "E")
				{
					// Buyer
					strcpy(acTmp, asRows.GetAt(FRE_GRGR_NAME));
					memcpy(curGrGrRec.Grantees[0].Name, acTmp, strlen(acTmp));
					curGrGrRec.Grantees[0].NameType[0] = 'E';
				}

            // Output GrGr record
            curGrGrRec.CRLF[0] = '\n';
            curGrGrRec.CRLF[1] = 0;
            iRet = fputs((char *)&curGrGrRec, fdOut);
            iRecCnt++;
         } 
      }

      if (!(lCnt % 1000))
         printf("\r%d", lCnt);
   }

   LogMsgD("\nTotal processed records  : %u", lCnt);
   LogMsgD("        skipped records  : %u", iSkipRows);
   LogMsgD("         output records  : %u\n", iRecCnt);

   return lCnt;
}

/***************************** Fre_LoadGrGrCsv *******************************
 *
 * Process GrGr in CSV format
 * Logic:
 *    - Load all docs
 *    - If there is tax amount, calculate sale price
 *    - Merge all docs with same doc# & APN.
 * Notes:
 *    - Same Doc# with multiple APN, create as many records as APNs.  If two records
 *      has same Doc# & APN, keep one that has Transfer Tax
 *    - 
 *
 * Return 0 if successful, otherwise error
 *
 *****************************************************************************/

int Fre_LoadGrGrCsv(char *pCnty)
{
   char     *pTmp, acRec[65536], acOutRec[MAX_RECSIZE], acTmp[512], acLastDoc[256], cGrGrDelim,
            acInFile[512], acGrGrIn[_MAX_PATH], acGrGrOut[_MAX_PATH], acGrGrBak[_MAX_PATH],
            acDate[32], acApn[50000], *apItems[2048];
   int      iGrantors, iGrantees, iRet, iTmp, lDocNum, iApnCnt;
   long     lCnt, lHandle, lSalePrice, lRecOut;
   double   dTax;

   FILE     *fdIn, *fdOut;
   GRGR_DEF *pGrGrRec = (GRGR_DEF *)&acOutRec[0] ;
   struct   _finddata_t  sFileInfo;
   
   // Get raw file name
   GetIniString(pCnty, "GrGrIn", "", acGrGrIn, _MAX_PATH, acIniFile);
   LogMsg("Load %s GrGr file: %s", pCnty, acGrGrIn);
   GetIniString(pCnty, "GrGrDelim", ",", acTmp, _MAX_PATH, acIniFile);
   cGrGrDelim = acTmp[0];

   strcpy(acGrGrBak, acGrGrIn);
   pTmp = strrchr(acGrGrBak, '\\');
   if (pTmp)
      *++pTmp = 0;
   dateString(acRec, 0);
   sprintf(acTmp, "GrGr_%s", acRec);
   strcat(acGrGrBak, acTmp);

   // Create temp output file
   sprintf(acGrGrOut, acGrGrTmpl, pCnty, pCnty, "TMP");

   LogMsg("Open output file %s", acGrGrOut);
   fdOut = fopen(acGrGrOut, "w");
   if (fdOut == NULL)
   {
      printf("Error creating GrGr output file: %s\n", acGrGrOut);
      return -1;
   }

   // Open Input file
   lHandle = _findfirst(acGrGrIn, &sFileInfo);
   if (lHandle > 0)
   {
      pTmp = strrchr(acGrGrIn, '\\');
      if (pTmp)
         *pTmp = 0;
      iRet = 0;
   } else
   {
      bGrGrAvail = false;
      LogMsg("*** No new GrGr file to process");
      fclose(fdOut);
      return 1;
   }

   // Create backup folder
   if (_access(acGrGrBak, 0))
      _mkdir(acGrGrBak);

   lCnt=lRecOut=0;
   while (!iRet)
   {
      // Create input name
      sprintf(acInFile, "%s\\%s", acGrGrIn, sFileInfo.name);

      // Update GrGr file date
      iRet = getFileDate(acInFile);
      if (iRet > lLastGrGrDate)
         lLastGrGrDate = iRet;

      // Sort input S(#1,C,A)  DUPOUT BYPASS(141,R) OMIT(#3,C,LT,"0")

      sprintf(acTmp, "%s\\%s\\%s", acTmpPath, pCnty, sFileInfo.name);
      iRet = RebuildCsv(acInFile, acTmp, ',', FRE_GR_COLS);
      if (iRet > 0)
         strcpy(acInFile, acTmp);

      LogMsg("Open input file %s", acInFile);
      if (!(fdIn = fopen(acInFile, "r")))
      {
         LogMsg("***** Error opening file %s", acInFile);
         break;
      }

      // Skip header
      pTmp = fgets(acRec, 1024, fdIn);

      // Initialize new one
      memset(acOutRec, ' ', sizeof(GRGR_DEF));
      lSalePrice = 0;
      iGrantors = iGrantees = 0;
      acLastDoc[0] = 0;
      acApn[0] = 0;

      while (!feof(fdIn))
      {
         pTmp = fgets(acRec, 65536, fdIn);
         if (!pTmp)
            break;

#ifdef _DEBUG
         //if (!memcmp(&acRec[1], "2019-0079932", 12) || !memcmp(&acRec[1], "2019-0082484", 12))
         //   iRet = strlen(acRec);
#endif
         // Parse input string
         iTokens = ParseStringNQ(acRec, cGrGrDelim, FRE_GR_COLS+1, apTokens);
         if (iTokens < FRE_GR_COLS)
         {
            LogMsg("*** Bad input record: %.80s (Tokens=%d)", acRec, iTokens);
            continue;
         }

         // Is new doc?
         if (memcmp(apTokens[FRE_GR_DOCNUM], acLastDoc, strlen(apTokens[FRE_GR_DOCNUM])))
         {
            // Save last document
            if (acApn[0] >= '0' && pGrGrRec->DocNum[0] >= '0')
            {
               iRet = sprintf(acTmp, "%d", iGrantees);
               memcpy(pGrGrRec->NameCnt, acTmp, iRet);
               pGrGrRec->CRLF[0] = 10;
               pGrGrRec->CRLF[1] = 0;
               iApnCnt = ParseString(acApn, ',', 2048, apItems);
               if (iApnCnt > 1)
               {
                  pGrGrRec->MultiApn = 'Y';
                  if (iApnCnt > 200)
                     LogMsg("*** Watch out for large record.  Possible memory overrun at DocNum=%s (%d)", apTokens[FRE_GR_DOCNUM], iApnCnt);
               }
               for (iTmp = 0; iTmp < iApnCnt; iTmp++)
               {
                  if (isdigit(*apItems[iTmp]))
                  {
                     memcpy(pGrGrRec->APN, apItems[iTmp], strlen(apItems[iTmp]));
                     fputs(acOutRec, fdOut);
                     lRecOut++;
                  }
               }
               acLastDoc[0] = 0;
               acApn[0] = 0;
            }

            // Ignore these document
            if (!memcmp(apTokens[FRE_GR_DOCTITLE], "NOTICE OF ASSESSMENT", 12) || 
                !memcmp(apTokens[FRE_GR_DOCTITLE], "NOTICE OF COMPLETION", 12) ||
                !memcmp(apTokens[FRE_GR_DOCTITLE], "MISCELLANEOUS", 12) ||
                !memcmp(apTokens[FRE_GR_DOCTITLE], "FINANCING STATEMENT", 12) ||
                !memcmp(apTokens[FRE_GR_DOCTITLE], "REQUEST FOR COPY", 14)                 
                )
            {
               continue;
            }

            if (!memcmp(apTokens[FRE_GR_DOCTITLE], "-EASEMENT", 9) )
               strcpy(apTokens[FRE_GR_DOCTITLE], "EASEMENT");

            // Initialize new one
            memset(acOutRec, ' ', sizeof(GRGR_DEF));            
            iGrantors = iGrantees = 0;
            lSalePrice = 0;


#ifdef _DEBUG
            iRet = strlen(apTokens[FRE_GR_APN]);
            if (iRet > 50000)
            {
               LogMsg("***** APN is too long for DocNum: %s (%d)", apTokens[FRE_GR_DOCNUM], iRet);
               *(apTokens[FRE_GR_APN]+49998) = 0;
            }
#endif
            strcpy(acApn, apTokens[FRE_GR_APN]);
            iRet = remChar(acApn, '-');

            // Old DocDate prior to 07/2023: MM_DD_YYYY_2 (MM/DD/YYYY)
            // New DocDate 06/12/2024: WD_MMM_DD_YYYY (Wed May 15 15:25:36 PDT 2024)
            strcpy(acTmp, apTokens[FRE_GR_DOCDATE]);
            if (strchr(acTmp, '/'))
               pTmp = dateConversion(acTmp, acDate, MM_DD_YYYY_2);
            else
               pTmp = dateConversion(acTmp, acDate, WD_MMM_DD_YYYY);
            if (pTmp)
               memcpy(pGrGrRec->DocDate, acDate, 8);
            else
            {
               LogMsg("*** Invalid DocDate: %s [DocNum=%s] (ignore)", apTokens[FRE_GR_DOCDATE], apTokens[FRE_GR_DOCNUM]);  
               continue;
            }

            // DocNum
            strcpy(acLastDoc, apTokens[FRE_GR_DOCNUM]);
            iTmp = strlen(apTokens[FRE_GR_DOCNUM]);
            if (iTmp < 10 && isNumber(acLastDoc))
            {
               lDocNum = atol(acLastDoc);
               iTmp = sprintf(acTmp, "%.7d", lDocNum);
               memcpy(pGrGrRec->DocNum, acTmp, iTmp);
            } else if (iTmp == 12)
            {
               lDocNum = atol(&acLastDoc[5]);
               iTmp = sprintf(acTmp, "%.7d", lDocNum);
               memcpy(pGrGrRec->DocNum, acTmp, iTmp);
            } else
            {
               LogMsg("??? Questionable DocNum: %s (ignore)", acLastDoc);
               continue;
            }

            // DocType - This field is currently blank
            if (iTokens > FRE_GR_DOCTYPE)
               memcpy(pGrGrRec->DocType, apTokens[FRE_GR_DOCTYPE], strlen(apTokens[FRE_GR_DOCTYPE]));

            // DocTitle
            memcpy(pGrGrRec->DocTitle, apTokens[FRE_GR_DOCTITLE], strlen(apTokens[FRE_GR_DOCTITLE]));
         }

         // Tax Amt
         dTax = atof(apTokens[FRE_GR_DOCTAX]);
         lSalePrice = (long)(dTax*SALE_FACTOR);
         if (lSalePrice > 0)
         {
            iRet = sprintf(acTmp, "%10d", (long)(100*dTax));
            memcpy(pGrGrRec->Tax, acTmp, iRet);
            iRet = sprintf(acTmp, "%10d", lSalePrice);
            memcpy(pGrGrRec->SalePrice, acTmp, iRet);
         }

			// Name type
         if (*apTokens[FRE_GR_GRANTOR] > ' ' && iGrantors < MAX_NAMES)
         {
			   memcpy(pGrGrRec->Grantors[iGrantors].Name, apTokens[FRE_GR_GRANTOR], strlen(apTokens[FRE_GR_GRANTOR]));
			   pGrGrRec->Grantors[iGrantors++].NameType[0] = 'O';
         }

         if (*apTokens[FRE_GR_GRANTEE] > ' ')
         {
            if (iGrantees < MAX_NAMES)
            {
               if (!iGrantees || !memcpy(pGrGrRec->Grantees[iGrantees-1].Name, apTokens[FRE_GR_GRANTEE], strlen(apTokens[FRE_GR_GRANTEE])))
               {
			         memcpy(pGrGrRec->Grantees[iGrantees].Name, apTokens[FRE_GR_GRANTEE], strlen(apTokens[FRE_GR_GRANTEE]));
			         pGrGrRec->Grantees[iGrantees++].NameType[0] = 'E';
               }
            } else
            {
               pGrGrRec->MoreName = 'Y';
               iGrantees++;
            }
         }

         // Number of pages
         iRet = atol(apTokens[FRE_GR_NUMPAGES]);
         if (iRet > 0)
            memcpy(pGrGrRec->NumPages, apTokens[FRE_GR_NUMPAGES], strlen(apTokens[FRE_GR_NUMPAGES]));

         lCnt++;
         if (!(lCnt % 1000))
            printf("\r%d", lCnt);
      }

      // Write out last record
      if (acApn[0] >= '0')
      {
         iRet = sprintf(acTmp, "%d", iGrantees);
         memcpy(pGrGrRec->NameCnt, acTmp, iRet);
         pGrGrRec->CRLF[0] = 10;
         pGrGrRec->CRLF[1] = 0;
         iApnCnt = ParseString(acApn, ',', 2048, apItems);
         if (iApnCnt > 1)
            pGrGrRec->MultiApn = 'Y';
         for (iTmp = 0; iTmp < iApnCnt; iTmp++)
         {
            if (isdigit(*apItems[iTmp]))
            {
               memcpy(pGrGrRec->APN, apItems[iTmp], strlen(apItems[iTmp]));
               fputs(acOutRec, fdOut);
               lRecOut++;
            }
         }
      }

      LogMsg("Total records so far: %d", lCnt);
      fclose(fdIn);

      // Move file
      sprintf(acTmp, "%s\\%s", acGrGrBak, sFileInfo.name);
      sprintf(acRec, "%s\\%s", acGrGrIn, sFileInfo.name);
      rename(acRec, acTmp);

      // Find next file
      iRet = _findnext(lHandle, &sFileInfo);
   }

   // Close handle
   _findclose(lHandle);

   // Close output file
   if (fdOut)
      fclose(fdOut);

   // Sort output file
   LogMsg("Total processed records: %u", lCnt);
   LogMsg("         output records: %u\n", lRecOut);

   // Merge all record with same Doc# and APN
   char acSrtFile[_MAX_PATH];
   sprintf(acSrtFile, acGrGrTmpl, pCnty, pCnty, "srt"); // Fre_GrGr.srt
   iRet = Fre_GrGr_SortMerge(acGrGrOut, acSrtFile);

   if (iRet > 0)
   {
      // Sort output file and dedup
      sprintf(acGrGrIn, acGrGrTmpl, pCnty, pCnty, "sls"); // Fre_GrGr.sls
      if (!_access(acGrGrIn, 0))
         sprintf(acInFile, "%s+%s", acSrtFile, acGrGrIn);
      else
         strcpy(acInFile, acSrtFile);

      // Sort by APN, RecDate, DocNum, Sale price
      // Dupout the first 44 bytes (changed on 7/14/2017)
      // S(17,12,C,A,37,8,C,A,1,12,C,A,125,1,C,D) F(TXT) OMIT(17,1,C,LT,"0",OR,17,1,C,GT,"9") DUPO(B2048,1,44)
      sprintf(acTmp, "S(%d,12,C,A,%d,8,C,A,%d,12,C,A,125,10,C,D) F(TXT) OMIT(17,1,C,LT,\"0\",OR,17,1,C,GT,\"9\") DUPO(B8192,1,44) ", 
         OFF_GD_APN, OFF_GD_DOCDATE, OFF_GD_DOCNUM);

      // Sort on APN asc, DocDate asc, DocNum asc
      sprintf(acGrGrOut, acGrGrTmpl, pCnty, pCnty, "dat"); // Fre_GrGr.dat
      lCnt = sortFile(acInFile, acGrGrOut, acTmp);
      if (lCnt > 0)
      {
         if (!_access(acGrGrIn, 0))
            DeleteFile(acGrGrIn);

         // Rename Dat to SLS file
         iRet = rename(acGrGrOut, acGrGrIn);
         LogMsg("GRGR process completed: %d", lCnt);
      } else
      {
         iRet = -1;
         LogMsg("***** Error sorting output file in Fre_LoadGrGrCsv()");
      }
   }

   return iRet;
}

/***************************** Fre_LoadGrGrXls *******************************
 *
 * Process GrGr in XLSX format
 * Logic:
 *    - Load all docs
 *    - If there is tax amount, calculate sale price
 *    - Merge all docs with same doc#.
 * Notes:
 *    - From beginning, the sheet name in xls is "rof02404"
 *    - Since 1/1/2016, the sheet name in xlsx file has been changed to "rof02401"
 *
 * Return 0 if successful, otherwise error
 *
 *****************************************************************************/

int Fre_LoadGrGrXls(char *pCnty)
{
   char     *pTmp, acRec[MAX_RECSIZE];
   char     acTmp[256], cFileCnt=1;
   char     acGrGrIn[_MAX_PATH], acGrGrOut[_MAX_PATH], acGrGrBak[_MAX_PATH];
   char     acInFile[_MAX_PATH], acSrtFile[_MAX_PATH], acShtName[64];
   struct   _finddata_t  sFileInfo;
   FILE     *fdOut;

   int      iRet, iTmp;
   BOOL     bEof=false;
   long     lCnt, lHandle;

   // Get raw file name
   GetIniString(pCnty, "GrGrIn", "", acGrGrIn, _MAX_PATH, acIniFile);
   LogMsg("Load %s GrGr file: %s", pCnty, acGrGrIn);
   GetIniString(pCnty, "XlsSheet", "rof02404", acShtName, _MAX_PATH, acIniFile);

   strcpy(acGrGrBak, acGrGrIn);
   pTmp = strrchr(acGrGrBak, '\\');
   if (pTmp)
      *++pTmp = 0;
   dateString(acRec, 0);
   sprintf(acTmp, "GrGr_%s", acRec);
   strcat(acGrGrBak, acTmp);

   // Create temp output file
   sprintf(acGrGrOut, "%s\\%s\\%s_GrGr.tmp", acTmpPath, pCnty, pCnty);

   LogMsg("Open output file %s", acGrGrOut);
   fdOut = fopen(acGrGrOut, "w");
   if (fdOut == NULL)
   {
      printf("Error creating GrGr output file: %s\n", acGrGrOut);
      return -1;
   }

   // Open Input file
   lHandle = _findfirst(acGrGrIn, &sFileInfo);
   if (lHandle > 0)
   {
      pTmp = strrchr(acGrGrIn, '\\');
      if (pTmp)
         *pTmp = 0;
      iRet = 0;
   } else
   {
      bGrGrAvail = false;
      LogMsg("*** No new GrGr file to process");
      fclose(fdOut);
      return 1;
   }

   // Create backup folder
   if (_access(acGrGrBak, 0))
      _mkdir(acGrGrBak);

   lCnt = 0;
   while (!iRet)
   {
      // Create input name
      sprintf(acInFile, "%s\\%s", acGrGrIn, sFileInfo.name);

      // Update GrGr file date
      iRet = getFileDate(acInFile);
      if (iRet > lLastGrGrDate)
         lLastGrGrDate = iRet;

      // Convert xlsx to csv
		iRet = FRE_GRGRXls2Txt(acInFile, fdOut, acShtName);
      if (!iRet)
      {
         LogMsg("***** Bad input file. Please review.");
      } else
         lCnt += iRet;

      sprintf(acTmp, "%s\\%s", acGrGrBak, sFileInfo.name);
      sprintf(acRec, "%s\\%s", acGrGrIn, sFileInfo.name);
      rename(acRec, acTmp);

      // Find next file
      iRet = _findnext(lHandle, &sFileInfo);
   }

   // Close handle
   _findclose(lHandle);

   // Close files
   if (fdOut)
      fclose(fdOut);

   //LogMsg("Total matched records    : %d", iMatched);
   LogMsg("Total processed records  : %u", lCnt);

   // Merge all record with same Doc# and APN
   sprintf(acSrtFile, acGrGrTmpl, pCnty, pCnty, "srt"); // Fre_GrGr.srt
   iRet = Fre_GrGr_SortMerge(acGrGrOut, acSrtFile);

   // Update cummulated grgr file
   if (iRet > 0)
   {
      // Sort output file and dedup
      sprintf(acGrGrIn, acGrGrTmpl, pCnty, pCnty, "sls"); // Fre_GrGr.sls
      if (!_access(acGrGrIn, 0))
         sprintf(acInFile, "%s+%s", acSrtFile, acGrGrIn);
      else
         strcpy(acInFile, acSrtFile);

      // Sort by APN, RecDate, DocNum, Sale price
      // Dupout the first 44 bytes (changed on 7/14/2017)
      // S(17,12,C,A,37,8,C,A,1,12,C,A,125,1,C,D) F(TXT) OMIT(17,1,C,LT,"0",OR,17,1,C,GT,"9") DUPO(B2048,1,44)
      sprintf(acTmp, "S(%d,12,C,A,%d,8,C,A,%d,12,C,A,125,10,C,D) F(TXT) OMIT(17,1,C,LT,\"0\",OR,17,1,C,GT,\"9\") DUPO(B2048,1,44) ", 
         OFF_GD_APN, OFF_GD_DOCDATE, OFF_GD_DOCNUM);

      // Sort on APN asc, DocDate asc, DocNum asc
      sprintf(acGrGrOut, acGrGrTmpl, pCnty, pCnty, "dat"); // Fre_GrGr.dat
      lCnt = sortFile(acInFile, acGrGrOut, acTmp);
      if (lCnt > 0)
      {
         if (!_access(acGrGrIn, 0))
            DeleteFile(acGrGrIn);

         // Rename Dat to SLS file
         iTmp = rename(acGrGrOut, acGrGrIn);
         LogMsg("GRGR process completed: %d", lCnt);
         iRet = 0;
      } else
      {
         iRet = -1;
         LogMsg("***** Error sorting output file in Fre_LoadGrGrXsl()");
      }
   }

   LogMsg("Total GrGr records output: %u", lCnt);

   if (lCnt > 0)
      return 0;
   else
      return -5;
}

/******************************** Fre_MergeOwner *****************************

   VANCE ROBERT H & CAROL ANN TRUSTEES    -
   BRITTON JOHN B & MARLENE D TRS         -
   HUARACHA RUBEN                         -
   PERKINS TERRY & NANCY
   TISCARENO JAVIER M/ADRIANA DE JIMENEZ M          MARQUEZ JUAN MIGUEL
   MADRIGAL ELVIRA                                  MOLINA CHRISTINA
   SANTIBANEZ DAVID                                 SANTIBANEZ JOHNNY ET AL
   HUSSEIN SAIF                                     HUSSEIN AHMED NAJI
   BELTRAN ESPERANZA                                BELTRAN ALBERT
   RODRIGUEZ ROBERTO A & THERESA AGUILAR            PINEDA RICHARDO & MARLENE R
   LORENZETTI FREDERICK M                           LORENZETTI FLORA TRUSTEE ETAL
   DE WITT YVONNE TRUSTEE                           MILLER ROGER A TRUSTEE ETAL

 *****************************************************************************/

void Fre_MergeOwner(char *pOutbuf, char *pName1, char *pName2)
{
   int   iTmp;
   char  acOwner1[128], acOwner2[128], acOwners[128], acTmp[128], acTmp1[64], acTmp2[64];
   char  *pTmp;
   bool  bXlat;

   OWNER    myOwner;

   // Clear output buffer if needed
   removeNames(pOutbuf, false);

   // Initialize
   bXlat = false;
   memcpy(acTmp1, pName1, RSIZ_NAME1);
   blankRem(acTmp1, RSIZ_NAME2);
   _strupr(acTmp1);

   if (*pName2 > ' ')
   {
      memcpy(acTmp2, pName2, RSIZ_NAME2);
      blankRem(acTmp2, RSIZ_NAME2);
   } else
      acTmp2[0] = 0;

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "01312301S", 9))
   //   iTmp = 0;
#endif

   iTmp = strlen(acTmp1)-1;
   if (acTmp1[iTmp] == '-')
   {
      acTmp1[iTmp] = ' ';
      if (*pName2 > ' ' && !strchr(acTmp2, ' '))
      {
         strcat(acTmp1, acTmp2);
         acTmp2[0] = 0;
      }
      blankRem(acTmp1);
   }

   strcpy(acOwner1, acTmp1);
   // Check for special char
   if (pTmp = strchr(acOwner1, '/'))
   {
      if (!memcmp(pTmp-1, "C/F", 3) || !memcmp(pTmp-1, "R/L", 3))
         *(pTmp-2) = 0;
      else
      {
         *pTmp = 0;
         sprintf(acTmp, "%s & %s", acOwner1, ++pTmp);
         strcpy(acOwner1, acTmp);
      }
   }
   if (pTmp = strchr(acOwner1, '*'))
      *pTmp = 0;

   // Update vesting
   iTmp = updateVesting(myCounty.acCntyCode, acOwner1, pOutbuf+OFF_VEST, pOutbuf+OFF_ETAL_FLG);

   // Save Name1
   strcpy(acOwners, acOwner1);

   // Following words are filtered from Name1
   if ((pTmp=strstr(acOwner1, " ET AL")) || (pTmp=strstr(acOwner1, " ETAL")) )
      *pTmp = 0;

   // Following words are filtered for swapname but keep in Name1
   if ((pTmp=strstr(acOwner1, " LIFE EST"))  || (pTmp=strstr(acOwner1, " BYPASS TR")) ||
       (pTmp=strstr(acOwner1, " BY DEMAND")) || (pTmp=strstr(acOwner1, " TSTE")) ||
       (pTmp=strstr(acOwner1, " TTEE"))      || (pTmp=strstr(acOwner1, " TRUST")) )
      *pTmp = 0;

   acOwner2[0] = 0;
   // If Name1 and Name2 are the same, drop Name2
   if (acTmp2[0] && strcmp(acTmp1, acTmp2))
      strcpy(acOwner2, acTmp2);

   // Translate OR to & before parsing
   if (pTmp = strstr(acOwner1, " OR "))
   {
      bXlat = true;
      *pTmp = 0;
      sprintf(acTmp, "%s & %s", acOwner1, pTmp+4);
      strcpy(acOwner1, acTmp);
   }

   // Now parse owners
   iTmp = splitOwner(acOwner1, &myOwner, 3);
   if (iTmp < 0)
      strcpy(myOwner.acSwapName, acOwners);
   if (bXlat)
      replStr(myOwner.acSwapName, "&", "OR");

   // Name1
   memcpy(pOutbuf+OFF_NAME1, acOwners, strlen(acOwners));
   // Swapname
   memcpy(pOutbuf+OFF_NAME_SWAP, myOwner.acSwapName, strlen(myOwner.acSwapName));

   // Name2
   if (acOwner2[0])
   {
      if ((pTmp=strstr(acOwner2, " ET AL")) || (pTmp=strstr(acOwner2, " ETAL"))    ||
          (pTmp=strstr(acOwner2, " TSTE"))  || (pTmp=strstr(acOwner2, " TRUSTEE")) ||
          (pTmp=strstr(acOwner2, " TTEE"))  )
         *pTmp = 0;

      if (pTmp = strchr(acOwner2, '*'))
         *pTmp = 0;

      // Ignore Name2 if it's the same as Name1
      if (!strcmp(acOwner1, acOwner2))
         lDupOwners++;
      else
         vmemcpy(pOutbuf+OFF_NAME2, acOwner2, SIZ_NAME2);
   }
}

/********************************* Fre_MergeChar *****************************
 *
 * Will revisit to clean up beds and baths for multi units
 *
 *****************************************************************************/

int Fre_MergeChar(char *pOutbuf)
{
   static   char acRec[1024], *pRec=NULL;
   static   int  iCharRead=0;

   char     acTmp[256], acCode[32], acAcres[32], *pTmp, *pAcres;
   ULONG    lBldgSqft, lAcres, lLotAcres, lLotSqft, lGarSqft, lFlr1Sqft, lFlr2Sqft;

   int      iRet, iTmp, iTmp1, iLoop, iBeds, iFBath, iHBath;
   FRE_CHAR *pChar;

   iRet=iBeds=iFBath=iHBath=0;
   lLotSqft=lBldgSqft=lLotAcres=0;
   acAcres[0] = 0;

   // Get first Char rec for first call
   if (!iCharRead)
   {
      //pRec = fgets(acRec, 1024, fdChar);
      iCharRead = fread(acRec, 1, iCharLen, fdChar);
   }
   //pChar = (FRE_CHAR *)pRec;
   pChar = (FRE_CHAR *)&acRec[0];

   do
   {
      if (iCharRead < iCharLen)
      {
         fclose(fdChar);
         fdChar = NULL;
         return 1;      // EOF
      }

#ifdef _DEBUG
      //if (!memcmp(pChar->Apn, "36302012", 6))
      //   iRet = 1;
#endif
      // Compare Apn
      iLoop = memcmp(pOutbuf, pChar->Apn, RSIZ_APN);
      if (iLoop > 0)
      {
         if (bDebug)
            LogMsg0("Skip Char rec  %.*s", RSIZ_APN, pChar->Apn);
         //pRec = fgets(acRec, 1024, fdChar);
         iCharRead = fread(acRec, 1, iCharLen, fdChar);
         lCharSkip++;
      }
   } while (iLoop > 0);

   // If not match, return
   if (iLoop)
      return 1;

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "003120007", 9))
   //   lTmp = 0;
#endif

   // Legal
   lAcres = 0;
   if (pChar->Legal[0] > ' ')
   {
      memcpy(acTmp, pChar->Legal, CSIZ_NARRATIVE);
      acTmp[CSIZ_NARRATIVE] = 0;
      replCharEx(acTmp, 31, 32, CSIZ_NARRATIVE);
      if (acTmp[1] == '|')
         acTmp[1] = '1';
      blankRem(acTmp);
      iTmp = updateLegal(pOutbuf, acTmp);

      // Scan for AC
      if ((pTmp = strstr(acTmp, " AC ")) || (pTmp = strstr(acTmp, " ACS ")))
      {
         *pTmp = 0;
         pAcres = strrchr(acTmp, ' ');
         if (pAcres)
            pTmp = pAcres+1;
         else
            pTmp = acTmp;
         pAcres = strrchr(pTmp, '*');
         if (pAcres)
            strcpy(acAcres, pAcres + 1);
         else
            strcpy(acAcres, pTmp);

         double dTmp = atof(acAcres)+0.001;
         lAcres = (ULONG)(dTmp * 100.0);

#ifdef _DEBUG
         //LogMsg("Acres = %s [%.9s]", acAcres, pRawRec->Apn);
#endif
      } 
   }

   // Lot area - V99
   lLotAcres = atoin(pChar->LotArea, CSIZ_LOT_AREA);

   // Check legal acres vs LotAcres
   if (lAcres > 1 && lAcres != lLotAcres)
   {
      iTmp = sprintf(acTmp, "%u", lLotAcres);
      iTmp1 = sprintf(acAcres, "%u", lAcres);
      if (bDebug && lLotAcres > 0 && (iTmp != iTmp1 || acTmp[0] != acAcres[0]))
         LogMsg0("%.9s Legal acres != Lot acres: %u <> %u.  Use acres from legal.", pOutbuf, lAcres, lLotAcres);
      lLotSqft = (ULONG)((double)lAcres*SQFT_FACTOR_100);
      lLotAcres = lAcres*10;
   } else if (lLotAcres > 1)
   {
      // If the depth is 1, the value here is acre, otherwise sqft
      iTmp = atoin(pChar->LotD, CSIZ_LOT_DEPTH);
      if (iTmp == 1)
      {
         lLotSqft = (ULONG)((double)lLotAcres*SQFT_FACTOR_100);
         lLotAcres *= 10;
      } else
      {
         lLotSqft = lLotAcres;
         lLotAcres = (ULONG)((double)lLotSqft*SQFT_MF_1000);
      }
   }

   if (lLotAcres > 1)
   {
      if (lLotSqft > 999999999)
         lLotSqft = 0;

      sprintf(acTmp, "%*u", SIZ_LOT_SQFT, lLotSqft);
      memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);

      sprintf(acTmp, "%*u", SIZ_LOT_ACRES, lLotAcres);
      memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);
   }

   // LandUse
   if (pChar->UseCode[0] > '0')
   {
      memcpy(pOutbuf+OFF_USE_CO, pChar->UseCode, CSIZ_USECODE);
      updateStdUse(pOutbuf+OFF_USE_STD, pChar->UseCode, CSIZ_USECODE, pOutbuf);
   } else
      memcpy(pOutbuf+OFF_USE_STD, USE_UNASGN, SIZ_USE_STD);

   // Zoning
   if (pChar->Zoning[0] > '0')
   {
      memcpy(pOutbuf+OFF_ZONE, pChar->Zoning, SIZ_ZONE);
      if (*(pOutbuf+OFF_ZONE_X1) == ' ')
         vmemcpy(pOutbuf+OFF_ZONE_X1, pChar->Zoning, SIZ_ZONE_X1, CSIZ_ZONING);
   }

   // Cooling
   if (pChar->Cooling == '1')
      *(pOutbuf+OFF_AIR_COND) = 'A';    // REFRIGERATION
   else if (pChar->Cooling == '2')
      *(pOutbuf+OFF_AIR_COND) = 'E';    // EVAPORATIVE COOLER

   // Fireplace
   if (pChar->FirePlace > '0')
      *(pOutbuf+OFF_FIRE_PL) = pChar->FirePlace;

   // Pool/Spa
   iTmp = atoin(pChar->PoolW, CSIZ_POOL_WIDTH);
   if (iTmp > 0)
      *(pOutbuf+OFF_POOL) = 'P';    // Pool

   // Garage area
   lGarSqft = atoin(pChar->Garage_Area, CSIZ_GARAGE_AREA);
   if (lGarSqft > 10)
   {
      sprintf(acTmp, "%*u", SIZ_GAR_SQFT, lGarSqft);
      memcpy(pOutbuf+OFF_GAR_SQFT, acTmp, SIZ_GAR_SQFT);
      *(pOutbuf+OFF_PARK_TYPE) = 'Z';
   }

   // Beds - Baths 99V99
   iBeds = atoin(pChar->Beds, CSIZ_BEDS-2);
   if (iBeds > 0)
   {
      sprintf(acTmp, "%*u", SIZ_BEDS, iBeds);
      memcpy(pOutbuf+OFF_BEDS, acTmp, SIZ_BEDS);
   }
   iBeds = atoin(pChar->Beds, CSIZ_BEDS);

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "44933519", 8))
   //   iTmp = 0;
#endif

   // Baths
   iFBath = atoin(pChar->Baths, CSIZ_BATHS-2);
   if (iFBath > 0)
   {
      // Clear old data
      *(pOutbuf+OFF_BATH_1Q) = ' ';
      *(pOutbuf+OFF_BATH_2Q) = ' ';
      *(pOutbuf+OFF_BATH_3Q) = ' ';
      *(pOutbuf+OFF_BATH_4Q) = ' ';

      iTmp = atoin(&pChar->Baths[2], CSIZ_BATHS-2);
      if (iTmp > 0)     // half bath are 25, 50 or 75%
      {
         // Half bath
         iHBath = 1;
         sprintf(acTmp, "%*u", SIZ_BATH_H, iHBath);
         memcpy(pOutbuf+OFF_BATH_H, acTmp, SIZ_BATH_H);
         
         if (iTmp >= 60)
            *(pOutbuf+OFF_BATH_3Q) = '1';
         else if (iTmp >= 40)
            *(pOutbuf+OFF_BATH_2Q) = '1';
         else 
            *(pOutbuf+OFF_BATH_1Q) = '1';
      }

      // Full bath
      sprintf(acTmp, "%*u", SIZ_BATH_F, iFBath);
      memcpy(pOutbuf+OFF_BATH_F, acTmp, SIZ_BATH_F);
      memcpy(pOutbuf+OFF_BATH_4Q, acTmp, SIZ_BATH_F);
   }

   // YrEff
   if (pChar->EffYear[0] > '0')
   {
      iTmp = atoin(pChar->EffYear, SIZ_YR_EFF);
      if (iTmp > 1900)
      {
         // For EDX, EffYr and YrBlt are the same
         memcpy(pOutbuf+OFF_YR_EFF, pChar->EffYear, SIZ_YR_EFF);
         memcpy(pOutbuf+OFF_YR_BLT, pChar->EffYear, SIZ_YR_EFF);
      }
   }

   // BldgSqft - stories
   iTmp = atoin(pChar->Floors, CSIZ_FLOORS);
   lFlr1Sqft = atoin(pChar->First_Flr_Area, CSIZ_1ST_FLOOR_AREA);
   lFlr2Sqft = atoin(pChar->Second_Flr_Area, CSIZ_2ND_FLOOR_AREA);
   if (iTmp > 0 && iTmp < 20)
   {
      iRet = sprintf(acTmp, "%d.0", iTmp);
      memcpy(pOutbuf+OFF_STORIES, acTmp, iRet);
   } else if (lFlr2Sqft > 0)
      memcpy(pOutbuf+OFF_STORIES, "2.0", 3);
   else if (lFlr1Sqft > 0)
      memcpy(pOutbuf+OFF_STORIES, "1.0", 3);

   lBldgSqft = lFlr1Sqft+lFlr2Sqft+atoin(pChar->Addl_Impr_Area, CSIZ_ADDL_IMPR_AREA);
   if (lBldgSqft > 10)
   {
      sprintf(acTmp, "%*u", SIZ_BLDG_SF, lBldgSqft);
      memcpy(pOutbuf+OFF_BLDG_SF, acTmp, SIZ_BLDG_SF);
   }

   // Units
   iTmp = atoin(pChar->Units, CSIZ_UNITS);
   if (iTmp > 0 && iTmp <= iBeds)         // Number of units must be <= number of befrooms
   {
      sprintf(acTmp, "%*u", SIZ_UNITS, iTmp);
      memcpy(pOutbuf+OFF_UNITS, acTmp, SIZ_UNITS);
   }

   // Quality/class
   if (pChar->BldgClass[0] > ' ')
      memcpy(pOutbuf+OFF_QUALITYCLASS, pChar->BldgClass, CSIZ_BLDGCLASS);

   if (isalpha(pChar->BldgClass[0]))
   {  // D045A
      *(pOutbuf+OFF_BLDG_CLASS) = pChar->BldgClass[0];
      iTmp = atoin((char *)&pChar->BldgClass[1], 2);
      if (iTmp > 0)
      {
         sprintf(acTmp, "%d.%c", iTmp, pChar->BldgClass[3]);
         iRet = Value2Code(acTmp, acCode, NULL);
         blankPad(acCode, SIZ_BLDG_QUAL);
         memcpy(pOutbuf+OFF_BLDG_QUAL, acCode, SIZ_BLDG_QUAL);
      }
   }

   // Basement Sqft 
   long lBsmtSqft = atoin(pChar->Basement_Area, CSIZ_BASEMENT_AREA);
   if (lBsmtSqft > 10)
   {
      sprintf(acTmp, "%*u", SIZ_BSMT_SF, lBsmtSqft);
      memcpy(pOutbuf+OFF_BSMT_SF, acTmp, SIZ_BSMT_SF);
   }

   // Others
   lCharMatch++;

   // Get next Char rec
   //pRec = fgets(acRec, 1024, fdChar);
   iCharRead = fread(acRec, 1, iCharLen, fdChar);

   return 0;
}

/****************************** Fre_FmtChar **********************************
 *
 * Convert FRE_CHAR record to STDCHAR format
 *
 *****************************************************************************/

int Fre_FmtChar(char *pOutbuf, char *pInbuf)
{
   char     acTmp[_MAX_PATH], acAcres[32], *pTmp, *pAcres;
   int      iTmp, iTmp1;
   ULONG    lTmp, lAcres, lLotAcres, lLotSqft;
   double   dTmp;

   STDCHAR  *pCharRec=(STDCHAR *)pOutbuf;
   FRE_CHAR *pRawRec =(FRE_CHAR *)pInbuf;
   
   // Init output buffer
   memset(pOutbuf, ' ', sizeof(STDCHAR));
   acAcres[0] = 0;

   // APN
   memcpy(pCharRec->Apn, pRawRec->Apn, RSIZ_APN);

   // Format APN
   iTmp = formatApn(pRawRec->Apn, acTmp, &myCounty);
   memcpy(pCharRec->Apn_D, acTmp, iTmp);

   // Unit Seq
   memcpy(pCharRec->UnitSeqNo, pRawRec->SheetNum, CSIZ_SHEET_NUM);

#ifdef _DEBUG
   //if (!memcmp(pCharRec->Apn, "00113028", 8) )
   //   iTmp = 0;
#endif

   // Legal
   lAcres = 0;
   if (pRawRec->Legal[0] > ' ')
   {
      memcpy(acTmp, pRawRec->Legal, CSIZ_NARRATIVE);
      acTmp[CSIZ_NARRATIVE] = 0;
      replCharEx(acTmp, 31, 32, CSIZ_NARRATIVE);
      if (acTmp[1] == '|')
         acTmp[1] = '1';
      blankRem(acTmp);
      vmemcpy(pCharRec->Misc.Legal, acTmp, CSIZ_NARRATIVE);

      // Scan for AC
      if ((pTmp = strstr(acTmp, " AC ")) || (pTmp = strstr(acTmp, " ACS ")))
      {
         *pTmp = 0;
         pAcres = strrchr(acTmp, ' ');
         if (pAcres)
            pTmp = pAcres+1;
         else
            pTmp = acTmp;
         pAcres = strrchr(pTmp, '*');
         if (pAcres)
            strcpy(acAcres, pAcres + 1);
         else
            strcpy(acAcres, pTmp);

         dTmp = atof(acAcres)+0.001;
         lAcres = (ULONG)(dTmp * 100.0);

#ifdef _DEBUG
         //LogMsg("Acres = %s [%.9s]", acAcres, pRawRec->Apn);
#endif
      } 
   }

   // Lot area - V99
   lLotAcres = atoin(pRawRec->LotArea, CSIZ_LOT_AREA);

   // Check legal acres vs LotAcres
   if (lAcres > 1 && lAcres != lLotAcres)
   {
      iTmp = sprintf(acTmp, "%u", lLotAcres);
      iTmp1 = sprintf(acAcres, "%u", lAcres);
      if (bDebug && lLotAcres > 0 && (iTmp != iTmp1 || acTmp[0] != acAcres[0]))
         LogMsg0("%.9s Legal acres != Lot acres: %u <> %u.  Use acres from legal.", pOutbuf, lAcres, lLotAcres);
      lLotSqft = (ULONG)((double)lAcres*SQFT_FACTOR_100);
      lLotAcres = lAcres*10;
   } else if (lLotAcres > 1)
   {
      // If the depth is 1, the value here is acre, otherwise sqft
      iTmp = atoin(pRawRec->LotD, CSIZ_LOT_DEPTH);
      if (iTmp == 1)
      {
         lLotSqft = (ULONG)((double)lLotAcres*SQFT_FACTOR_100);
         lLotAcres *= 10;
      } else
      {
         lLotSqft = lLotAcres;
         lLotAcres = (ULONG)((double)lLotSqft*SQFT_MF_1000);
      }
   }

   if (lLotAcres > 1)
   {
      iTmp = sprintf(acTmp, "%u", lLotSqft);
      memcpy(pCharRec->LotSqft, acTmp, iTmp);

      iTmp = sprintf(acTmp, "%u", lLotAcres);
      memcpy(pCharRec->LotAcre, acTmp, iTmp);
   }

      // Zoning
   if (pRawRec->Zoning[0] > ' ')
      memcpy(pCharRec->Zoning, pRawRec->Zoning, SIZ_ZONE);

   // UseCode
   if (pRawRec->UseCode[0] > ' ')
      memcpy(pCharRec->LandUse, pRawRec->UseCode, CSIZ_USECODE);

   // Cooling
   if (pRawRec->Cooling == '1')
      pCharRec->Cooling[0] = 'A';    // REFRIGERATION
   else if (pRawRec->Cooling == '2')
      pCharRec->Cooling[0] = 'E';    // EVAPORATIVE COOLER

   // Fireplace
   if (pRawRec->FirePlace > '0')
      pCharRec->Fireplace[0] = pRawRec->FirePlace;

   // Pool/Spa
   iTmp = atoin(pRawRec->PoolW, CSIZ_POOL_WIDTH);
   if (iTmp > 0)
      pCharRec->Pool[0] = 'P';    // Pool

   // Garage area
   lTmp = atoin(pRawRec->Garage_Area, CSIZ_GARAGE_AREA);
   if (lTmp > 10)
   {
      iTmp = sprintf(acTmp, "%d", lTmp);
      memcpy(pCharRec->GarSqft, acTmp, iTmp);
      pCharRec->ParkType[0] = 'Z';
   }

   // Beds - Baths 99V99
   int iBeds = atoin(pRawRec->Beds, CSIZ_BEDS-2);
   if (!iBeds)
      iBeds = atoin(pRawRec->Beds, CSIZ_BEDS);
   if (iBeds > 0)
   {
      iTmp = sprintf(acTmp, "%d", iBeds);
      memcpy(pCharRec->Beds, acTmp, iTmp);
   }

   // Baths
   int iFBath = atoin(pRawRec->Baths, CSIZ_BATHS-2);
   if (iFBath > 0)
   {
      iTmp = atoin(&pRawRec->Baths[2], CSIZ_BATHS-2);
      if (iTmp > 0)     // half bath are 25, 50 or 75%
      {
         // Half bath
         pCharRec->HBaths[0] = '1';
         
         if (iTmp >= 60)
            pCharRec->Bath_3Q[0] = '1';
         else if (iTmp >= 40)
            pCharRec->Bath_2Q[0] = '1';
         else 
            pCharRec->Bath_1Q[0] = '1';
      }

      // Full bath
      iTmp = sprintf(acTmp, "%d", iFBath);
      memcpy(pCharRec->FBaths, acTmp, iTmp);
      memcpy(pCharRec->Bath_4Q, acTmp, iTmp);
   }

   // YrEff
   if (pRawRec->EffYear[0] > '0')
   {
      iTmp = atoin(pRawRec->EffYear, SIZ_YR_EFF);
      if (iTmp > 1900)
      {
         // For FRE, EffYr and YrBlt are the same
         memcpy(pCharRec->YrEff, pRawRec->EffYear, SIZ_YR_EFF);
         memcpy(pCharRec->YrBlt, pRawRec->EffYear, SIZ_YR_EFF);
      }
   }

   // BldgSqft - stories
   int iFloors = atoin(pRawRec->Floors, CSIZ_FLOORS);
   int lFlr1Sqft = atoin(pRawRec->First_Flr_Area, CSIZ_1ST_FLOOR_AREA);
   int lFlr2Sqft = atoin(pRawRec->Second_Flr_Area, CSIZ_2ND_FLOOR_AREA);
   int lAddSqft  = atoin(pRawRec->Addl_Impr_Area, CSIZ_ADDL_IMPR_AREA);
   if (iFloors > 0 && iFloors < 20)
   {
      iTmp = sprintf(acTmp, "%d.0", iFloors);
      memcpy(pCharRec->Stories, acTmp, iTmp);
   } else if (lFlr2Sqft > 0)
      memcpy(pCharRec->Stories, "2.0", 3);
   else if (lFlr1Sqft > 0)
      memcpy(pCharRec->Stories, "1.0", 3);

   int lBldgSqft = lFlr1Sqft+lFlr2Sqft+lAddSqft;
   if (lBldgSqft > 10)
   {
      iTmp = sprintf(acTmp, "%d", lBldgSqft);
      memcpy(pCharRec->BldgSqft, acTmp, iTmp);

      iTmp = sprintf(acTmp, "%d", lFlr1Sqft);
      memcpy(pCharRec->Sqft_1stFl, acTmp, iTmp);

      if (lFlr2Sqft > 0)
      {
         iTmp = sprintf(acTmp, "%d", lFlr2Sqft);
         memcpy(pCharRec->Sqft_2ndFl, acTmp, iTmp);
      }
      if (lAddSqft > 0)
      {
         iTmp = sprintf(acTmp, "%d", lAddSqft);
         memcpy(pCharRec->MiscSqft, acTmp, iTmp);
      }
   }

   // Basement Sqft 
   long lBsmtSqft = atoin(pRawRec->Basement_Area, CSIZ_BASEMENT_AREA);
   if (lBsmtSqft > 10)
   {
      iTmp = sprintf(acTmp, "%d", lBsmtSqft);
      memcpy(pCharRec->BsmtSqft, acTmp, iTmp);
   }

   // GarSqft
   lTmp = atoin(pRawRec->Garage_Area, CSIZ_GARAGE_AREA);
   if (lTmp > 0)
   {
      iTmp = sprintf(acTmp, "%d", lTmp);
      memcpy(pCharRec->GarSqft, acTmp, iTmp);
   }

   // Units
   int iUnits = atoin(pRawRec->Units, CSIZ_UNITS);
   if (iUnits > 0 && iUnits <= iBeds)         // Number of units must be <= number of befrooms
   {
      iTmp = sprintf(acTmp, "%d", iUnits);
      memcpy(pCharRec->Units, acTmp, iTmp);
   }

   // Quality/class
   if (isalpha(pRawRec->BldgClass[0]))
   {  // D045A
      memcpy(pCharRec->QualityClass, pRawRec->BldgClass, CSIZ_BLDGCLASS);
      pCharRec->BldgClass = pRawRec->BldgClass[0];
      iTmp = atoin((char *)&pRawRec->BldgClass[1], 2);
      if (iTmp > 0)
      {
         char acCode[32];

         sprintf(acTmp, "%d.%c", iTmp, pRawRec->BldgClass[3]);
         iTmp = Value2Code(acTmp, acCode, NULL);
         if (iTmp >= 0)
            pCharRec->BldgQual = acCode[0];
      }
   }

   // Neighborhood
   memcpy(pCharRec->Nbh_Code, pRawRec->NeighborhoodCode, SIZ_CHAR_NBH_CODE);

   // Pool
   iTmp = atoin(pRawRec->PoolW, CSIZ_POOL_WIDTH);
   if (iTmp > 0)
      pCharRec->Pool[0] = 'P';

   pCharRec->CRLF[0] = '\n';
   pCharRec->CRLF[1] = '\0';

   if (strlen(pOutbuf) > 1024)
      iTmp = 0;

   return 0;
}

/***************************** Fre_ConvStdChar *******************************
 *
 * Return 0 if successful, otherwise error
 *
 *****************************************************************************/

int Fre_ConvStdChar(char *pInfile)
{
   char  acCharRec[2048], acInRec[MAX_RECSIZE], acTmpFile[_MAX_PATH];
   long  lCnt=0, lChars=0, iRet;
   FILE  *fdIn;

   LogMsg0("Extracting Chars from %s", pInfile);

   // Check current sale file
   if (_access(pInfile, 0))
   {
      LogMsg("***** Missing input file %s", pInfile);
      return -1;
   }

   // Open char file
   LogMsg("Open char file to extract CHARS %s", pInfile);
   fdIn = fopen(pInfile, "r");
   if (fdIn == NULL)
   {
      LogMsg("***** Error opening char file: %s\n", pInfile);
      return -2;
   }

   // Create output file
   sprintf(acTmpFile, "%s\\%s\\%s_Char.tmp", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode); 
   LogMsg("Create output chars file %s", acTmpFile);
   fdChar = fopen(acTmpFile, "w");
   if (fdChar == NULL)
   {
      LogMsg("***** Error creating char file: %s\n", acTmpFile);
      return -3;
   }

   // Merge loop
   while (!feof(fdIn))
   {
      // Get next record
      iRet = fread(acInRec, 1, iCharLen, fdIn);
#ifdef _DEBUG
   //if (!memcmp(acInRec, "00001018", 8) )
   //   iRet = 0;
#endif

      iRet = replUnPrtChar(acInRec, ' ', iCharLen-1);
      //if (iRet > 0)
      //   iRet = 0;
      iRet = Fre_FmtChar(acCharRec, acInRec);

      if (!iRet)
      {
         fputs(acCharRec, fdChar);
         lChars++;
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);

   }

   // Close files
   if (fdChar)
      fclose(fdChar);
   if (fdIn)
      fclose(fdIn);

   LogMsg("Total input records processed: %u", lCnt);
   LogMsg("Total char records output:     %u", lChars);

   // Sort output - APN, UnitSeq
   if (lChars > 100)
   {
      LogMsgD("\nSorting char file %s --> %s", acTmpFile, acCChrFile);
      sprintf(acInRec, "S(1,%d,C,A,83,2,C,A) ", iApnLen);
      iRet = sortFile(acTmpFile, acCChrFile, acInRec);
   } else
   {
      printf("\n");
      iRet = 0;
   }

   return iRet;
}

/***************************** Fre_MergeSaleRec ******************************
 *
 * Merge new sale data into current sale.  Move other sales accordingly.
 *
 *****************************************************************************/

int Fre_MergeSaleRec(char *pOutbuf, char *pSale)
{
   long     lCurSaleDt, lLstSaleDt, lPrice, lLastAmt, lDocNum;
   char     acTmp[32], acDocNum[32], acDocDate[32], *pTmp;

   FRE_SALE *pSaleRec = (FRE_SALE *)pSale;

   pTmp = dateConversion(pSaleRec->RecDate, acDocDate, MMDDYY2);
   if (pTmp)
      lCurSaleDt = atoin(pTmp, SIZ_SALE1_DT);
   else
      return -1;

   lLstSaleDt = atoin(pOutbuf+OFF_SALE1_DT, SIZ_SALE1_DT);

   if (lCurSaleDt < lLstSaleDt)
      return -1;

   lPrice = atoin(pSaleRec->Dtt_Amt, SSIZ_DTT_AMT);
   if (lPrice > 0)
      lPrice = (long)((double)lPrice*SALE_FACTOR_100);
   else
      lPrice = atoin(pSaleRec->SalePrice, SSIZ_SALE_AMT);

   if (lCurSaleDt == lLstSaleDt)
   {
      lLastAmt = atoin(pOutbuf+OFF_SALE1_AMT, SIZ_SALE1_AMT);
      if (!lPrice && lLastAmt > 0)
         return 0;
   } else
   {
      // Move sale2 to sale3
      memcpy(pOutbuf+OFF_SALE3_DOC, pOutbuf+OFF_SALE2_DOC, SIZ_SALE3_DOC);
      memcpy(pOutbuf+OFF_SALE3_DT, pOutbuf+OFF_SALE2_DT, SIZ_SALE3_DT);
      memcpy(pOutbuf+OFF_SALE3_DOCTYPE, pOutbuf+OFF_SALE2_DOCTYPE, SIZ_SALE3_DOCTYPE);
      memcpy(pOutbuf+OFF_SALE3_AMT, pOutbuf+OFF_SALE2_AMT, SIZ_SALE3_AMT);
      //memcpy(pOutbuf+OFF_SALE3_CODE, pOutbuf+OFF_SALE2_CODE, SIZ_SALE3_CODE);
      *(pOutbuf+OFF_AR_CODE3) = *(pOutbuf+OFF_AR_CODE2);

      // Move sale1 to sale2
      memcpy(pOutbuf+OFF_SALE2_DOC, pOutbuf+OFF_SALE1_DOC, SIZ_SALE3_DOC);
      memcpy(pOutbuf+OFF_SALE2_DT, pOutbuf+OFF_SALE1_DT, SIZ_SALE3_DT);
      memcpy(pOutbuf+OFF_SALE2_DOCTYPE, pOutbuf+OFF_SALE1_DOCTYPE, SIZ_SALE3_DOCTYPE);
      memcpy(pOutbuf+OFF_SALE2_AMT, pOutbuf+OFF_SALE1_AMT, SIZ_SALE3_AMT);
      //memcpy(pOutbuf+OFF_SALE2_CODE, pOutbuf+OFF_SALE1_CODE, SIZ_SALE3_CODE);
      *(pOutbuf+OFF_AR_CODE2) = *(pOutbuf+OFF_AR_CODE1);
   }

   // Update current sale
   lDocNum = atoin(pSaleRec->DocNum, SSIZ_INST_NUMBER);
   sprintf(acDocNum, "%d          ", lDocNum);

   memcpy(pOutbuf+OFF_SALE1_DOC, acDocNum, SSIZ_INST_NUMBER);
   memcpy(pOutbuf+OFF_SALE1_DT, acDocDate, SIZ_SALE1_DT);
   //if (isdigit(pSaleRec->DocType[0]))
   //   memcpy(pOutbuf+OFF_SALE1_DOCTYPE, pSaleRec->DocType, 3);
   //else if (pTmp=findDocType(pSaleRec->DocType, acTmp))
   //   memcpy(pOutbuf+OFF_SALE1_DOCTYPE, pTmp, strlen(pTmp));

   // Remove sale code -
   // Notes: SALE_SIZ_SALECODE is smaller than  SIZ_SALE1_CODE.  Don't swap them.
   //if (pSaleRec->SaleCode[0] >= '0')
   //   memcpy(pOutbuf+OFF_SALE1_CODE, pSaleRec->SaleCode, SALE_SIZ_SALECODE);
   //else
   //   memset(pOutbuf+OFF_SALE1_CODE, ' ', SALE_SIZ_SALECODE);

   // Check for questionable sale amt
   if (lPrice > 1000)
   {
      sprintf(acTmp, "%*u", SIZ_SALE1_AMT, lPrice);
      memcpy(pOutbuf+OFF_SALE1_AMT, acTmp, SIZ_SALE1_AMT);

      // Seller
      //if (pSaleRec->Seller[0] > ' ')
      //   memcpy(pOutbuf+OFF_SELLER, pSaleRec->Seller, SIZ_SELLER);
      //else
      //   memset(pOutbuf+OFF_SELLER, ' ', SIZ_SELLER);

      // Sale code

   } else
   {
      memset(pOutbuf+OFF_SALE1_AMT, ' ', SIZ_SALE1_AMT);
      //memset(pOutbuf+OFF_SELLER, ' ', SIZ_SELLER);
   }

   // Update transfers
   lLstSaleDt = atoin(pOutbuf+OFF_TRANSFER_DT, SIZ_TRANSFER_DT);
   if (lCurSaleDt >= lLstSaleDt)
   {
      memcpy(pOutbuf+OFF_TRANSFER_DOC, acDocNum, SSIZ_INST_NUMBER);
      memcpy(pOutbuf+OFF_TRANSFER_DT, acDocDate, SIZ_TRANSFER_DT);
   }

   *(pOutbuf+OFF_AR_CODE1) = 'A';
   return 1;
}

/********************************* Fre_MergeSale ******************************
 *
 * Input: Sale file is output from Fre_UpdateSaleHist().
 * Notes: sales_cd.txt may contain non-printable characters.  We have to read it 
 *        in binary mode to avoid problem.
 *
 ******************************************************************************/

int Fre_MergeSale(char *pOutbuf)
{
   static   char  acRec[1024], *pRec=NULL;
   int      iRet, iLoop;
   FRE_SALE *pSale;

   // Get first Char rec for first call
   if (!pRec)
   {
      iRet = fread(acRec, 1, iSaleLen, fdSale);
      pRec = acRec;
   }


   pSale = (FRE_SALE *)&acRec[0];
#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "31311112", 8))
   //   iRet = 0;
#endif

   do
   {
      if (!pRec)
      {
         fclose(fdSale);
         fdSale = NULL;
         return -1;      // EOF
      }

      // Compare Apn
      iLoop = memcmp(pOutbuf, pSale->Apn, SSIZ_APN);
      if (iLoop > 0)
      {
         if (bDebug)
            LogMsg0("Skip Sale rec  %.*s", SSIZ_APN, pSale->Apn);

         iRet = fread(acRec, 1, iSaleLen, fdSale);
         if (iRet != iSaleLen)
            pRec = NULL;
         lSaleSkip++;
      }
   } while (iLoop > 0);

   // If not match, return
   if (iLoop)
      return 1;

   do
   {
      iRet = Fre_MergeSaleRec(pOutbuf, acRec);

      // Get next sale record
      iRet = fread(acRec, 1, iSaleLen, fdSale);
      if (iRet != iSaleLen)
      {
         fclose(fdSale);
         fdSale = NULL;
         break;
      }

   } while (!memcmp(pOutbuf, pSale->Apn, SSIZ_APN));

   lSaleMatch++;

   return 0;
}

/******************************** Fre_MergeSitus *****************************
 *
 * Notes:
 *    - Drop street name '0' or 'O' if strNum is 0.
 *    - SANDIA CREEK TER WEST
 *    - Since there is no situs city, copy it from mailing if same strname
 *      and strnum.  Otherwise, match TRA.
 *
 * Return 0 if successful, >0 if error
 *
 *****************************************************************************/

int Fre_MergeSitus(char *pOutbuf)
{
   static   char acRec[1024], *pRec=NULL;
   char     acTmp[256], acAddr1[64], acCity[32], acCode[32];
   int      lTmp, iLoop;

   FRE_SITUS   *pSitus = (FRE_SITUS *)&acRec[0];
   ADR_REC     sAdrRec;

   // Get first rec for first call
   if (!pRec)
      pRec = fgets(acRec, 1024, fdSitus);

   do
   {
      if (!pRec)
      {
         fclose(fdSitus);
         fdSitus = NULL;
         return 1;      // EOF
      }

      // Compare Apn
      iLoop = memcmp(pOutbuf, pSitus->Apn, RSIZ_APN);
      if (iLoop > 0)
      {
         if (bDebug)
            LogMsg0("Skip situs Addr rec  %.*s", RSIZ_APN, pSitus->Apn);
         pRec = fgets(acRec, 1024, fdSitus);
         lSitusSkip++;
      }
   } while (iLoop > 0);

   // If not match, return
   if (iLoop)
      return 1;

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "02314101", 9))
   //   lTmp = 0;
#endif

   // Initialize
   acAddr1[0] = 0;
   lTmp = atoin(pSitus->StrNum, CSIZ_STRNUM);
   if (lTmp > 0)
   {
      sprintf(acAddr1, "%d       ", lTmp);
      memcpy(pOutbuf+OFF_S_STRNUM, acAddr1, SIZ_S_STRNUM);
      memcpy(pOutbuf+OFF_S_HSENO, acAddr1, SIZ_S_STRNUM);
      if (pSitus->StrFra[0] > ' ')
         memcpy(pOutbuf+OFF_S_STR_SUB, pSitus->StrFra, CSIZ_STRFRA);

      sprintf(acAddr1, "%d %.3s %.2s %.*s", lTmp, pSitus->StrFra, pSitus->StrDir, CSIZ_STRNAME, pSitus->StrName);
      blankRem(acAddr1);
      memcpy(pOutbuf+OFF_S_ADDR_D, acAddr1, strlen(acAddr1));
   }

   // Parse street name
   memcpy(acTmp, pSitus->StrName, CSIZ_STRNAME);
   blankRem(acTmp, CSIZ_STRNAME);
   parseAdrDNS(&sAdrRec, acTmp);

   if (pSitus->StrDir[0] > ' ')
      memcpy(pOutbuf+OFF_S_DIR, pSitus->StrDir, SIZ_S_DIR);
   else if (sAdrRec.strDir[0] > ' ')
      memcpy(pOutbuf+OFF_S_DIR, sAdrRec.strDir, strlen(sAdrRec.strDir));

   if (sAdrRec.strSfx[0] > ' ')
      memcpy(pOutbuf+OFF_S_SUFF, sAdrRec.strSfx, strlen(sAdrRec.strSfx));

   if (sAdrRec.strName[0] > ' ')
      memcpy(pOutbuf+OFF_S_STREET, sAdrRec.strName, strlen(sAdrRec.strName));

   if (sAdrRec.Unit[0] > ' ')
      memcpy(pOutbuf+OFF_S_UNITNO, sAdrRec.Unit, strlen(sAdrRec.Unit));

   // City
   if (pSitus->City[0] > ' ')
   {
      memcpy(acTmp, pSitus->City, CSIZ_CITYCODE);
      acTmp[CSIZ_CITYCODE] = 0;
      Abbr2Code(acTmp, acCode, acCity);
      if (acCode[0] > ' ')
      {
         memcpy(pOutbuf+OFF_S_CITY, acCode, strlen(acCode));
         strcat(acCity, " CA");
         memcpy(pOutbuf+OFF_S_CTY_ST_D, acCity, strlen(acCity));
      }
   }

   // State
   memcpy(pOutbuf+OFF_S_ST, "CA", 2);

   lSitusMatch++;

   // Get next situs rec
   pRec = fgets(acRec, 1024, fdSitus);
   return 0;
}

/********************************* Fre_MergeMAdr *****************************
 *
 * Mail address has similar problem as situs, but worse due to inconsistency.
 * Now we have to check for direction both pre and post strname.
 *
 * C/O ERIC ROMERO*2944 29TH ST*SAN DIEGO CA\
 *
 *****************************************************************************/

void Fre_MergeMAdr(char *pOutbuf, char *pAddr1, char *pAddr2, char *pAddr3, char *pAddr4)
{
   char     acTmp[256];
   char     acAddr[4][64], acAddr1[64], acAddr2[64];
   int      iTmp, iIdx, iCnt;

   ADR_REC  sMailAdr;

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "02016036S", 9))
   //   iTmp = 0;
#endif

   // Initialize
   *acAddr[0]=*acAddr[1]=*acAddr[2]=*acAddr[3]=0;
   removeMailing(pOutbuf, true);

   // Parse addr lines
   iIdx=iCnt=0;
   if (pAddr1[0] > ' ')
   {
      memcpy(acAddr[iCnt], pAddr1, RSIZ_ADDR);
      acAddr[iCnt][RSIZ_ADDR] = 0;
      iCnt++;
      if (pAddr2[0] > ' ')
      {
         memcpy(acAddr[iCnt], pAddr2, RSIZ_ADDR);
         acAddr[iCnt][RSIZ_ADDR] = 0;
         iCnt++;
         if (pAddr3[0] > ' ')
         {
            memcpy(acAddr[iCnt], pAddr3, RSIZ_ADDR);
            acAddr[iCnt][RSIZ_ADDR] = 0;
            iCnt++;
            if (pAddr4[0] > ' ')
            {
               memcpy(acAddr[iCnt], pAddr4, RSIZ_ADDR);
               acAddr[iCnt][RSIZ_ADDR] = 0;
               iCnt++;
            }
         }
      }
   }

   // Check for CareOf
   if (*acAddr[0] == '%' || !memcmp(acAddr[0], "C/O", 3) || !memcmp(acAddr[0], "ATTN", 4))
   {
      updateCareOf(pOutbuf, acAddr[0], strlen(acAddr[0]));
      iIdx++;
   }

   if (iCnt > 1)
   {
      strcpy(acAddr2, acAddr[--iCnt]);
      vmemcpy(pOutbuf+OFF_M_CTY_ST_D, acAddr2, SIZ_M_CTY_ST_D);
   } else
      return;     // bad address

   strcpy(acAddr1, acAddr[iIdx]);
   if (*acAddr[iIdx+1] == '#')
   {
      iIdx++;
      strcpy(acAddr1, &acAddr[iIdx][1]);
   } 
   vmemcpy(pOutbuf+OFF_M_ADDR_D, acAddr1, SIZ_M_ADDR_D);

   // Parse addr1
   memset((void *)&sMailAdr, 0, sizeof(ADR_REC));
   parseMAdr1_2(&sMailAdr, acAddr1);
   if (sMailAdr.lStrNum > 0)
   {
      sprintf(acTmp, "%d       ", sMailAdr.lStrNum);
      memcpy(pOutbuf+OFF_M_STRNUM, acTmp, SIZ_M_STRNUM);
      if (sMailAdr.strSub[0] > '0')
      {
         sprintf(acTmp, "%s  ", sMailAdr.strSub);
         memcpy(pOutbuf+OFF_M_STR_SUB, acTmp, SIZ_M_STR_SUB);
      }
      memcpy(pOutbuf+OFF_M_DIR, sMailAdr.strDir, strlen(sMailAdr.strDir));
      memcpy(pOutbuf+OFF_M_STREET, sMailAdr.strName, strlen(sMailAdr.strName));
      memcpy(pOutbuf+OFF_M_SUFF, sMailAdr.strSfx, strlen(sMailAdr.strSfx));
      if (sMailAdr.Unit[0] > ' ')
      {
         vmemcpy(pOutbuf+OFF_M_UNITNO, sMailAdr.Unit, SIZ_M_UNITNO);
         vmemcpy(pOutbuf+OFF_M_UNITNOX, sMailAdr.UnitNox, SIZ_M_UNITNOX);
      }
   } else
      memcpy(pOutbuf+OFF_M_STREET, acAddr1, strlen(acAddr1));

   // Parse addr2
   parseMAdr2(&sMailAdr, acAddr2);
   if (sMailAdr.State[0] > ' ')
      memcpy(pOutbuf+OFF_M_ST, sMailAdr.State, 2);

   // Save city name
   if (sMailAdr.City[0] > ' ')
   {
      // Ignore city name starts with "DO NOT"
      if (memcmp(sMailAdr.City, "DO NOT", 6))
         vmemcpy(pOutbuf+OFF_M_CITY, sMailAdr.City, SIZ_M_CITY);
   }

   // Zipcode
   if (sMailAdr.Zip[0] > ' ')
   {
      if (sMailAdr.Zip[4] == 'O')
         sMailAdr.Zip[4] = '0';
      iTmp = strlen(sMailAdr.Zip);
      if (iTmp == SIZ_M_ZIP)
         memcpy(pOutbuf+OFF_M_ZIP, sMailAdr.Zip, SIZ_M_ZIP);
      else if (!memcmp(sMailAdr.Zip, "933619", 6))
         memcpy(pOutbuf+OFF_M_ZIP, "93619", 5);
      else if (!memcmp(sMailAdr.Zip, "939711", 6))
         memcpy(pOutbuf+OFF_M_ZIP, "93711", 5);
      else if (!memcmp(sMailAdr.City, "CLOVIS", 6))
         memcpy(pOutbuf+OFF_M_ZIP, "93611", 5);
      else if (iTmp >= SIZ_M_ZIP)
         memcpy(pOutbuf+OFF_M_ZIP, sMailAdr.Zip, SIZ_M_ZIP);
      else if (iTmp == 4)
      {
         if (!memcmp(sMailAdr.City, "IVINS", 5) && !memcmp(sMailAdr.Zip, "4738", 4))
            memcpy(pOutbuf+OFF_M_ZIP, "84738", 5);
         else if (!memcmp(sMailAdr.City, "FRESNO", 6) )
         {
            if (!memcmp(sMailAdr.Zip, "9727", 4))
               memcpy(pOutbuf+OFF_M_ZIP, "93727", 5);
            else if (!memcmp(sMailAdr.Zip, "9370", 4))
               memcpy(pOutbuf+OFF_M_ZIP, "93704", 5);
            else if (!memcmp(sMailAdr.Zip, "9737", 4))
               memcpy(pOutbuf+OFF_M_ZIP, "93737", 5);
            else
               LogMsg("*** Invalid mailing zipcode: %.9s [%.*s]", sMailAdr.Zip, iApnLen, pOutbuf);
         } 
         else if (!memcmp(sMailAdr.City, "RCHO ", 5) && !memcmp(sMailAdr.Zip, "9173", 4))
            memcpy(pOutbuf+OFF_M_ZIP, "917392315", 9);
         else if (!memcmp(sMailAdr.City, "PEORIA", 5) && !memcmp(sMailAdr.Zip, "8383", 4))
            memcpy(pOutbuf+OFF_M_ZIP, "853835633", 9);
         else if (!memcmp(sMailAdr.City, "SAN JUAN", 5) && !memcmp(sMailAdr.Zip, "9504", 4))
            memcpy(pOutbuf+OFF_M_ZIP, "95045", 5);
         else if (!memcmp(sMailAdr.City, "SANGER", 5) && !memcmp(sMailAdr.Zip, "9657", 4))
            memcpy(pOutbuf+OFF_M_ZIP, "93657", 5);
      } 
      else if (iTmp == 3)
         memcpy(pOutbuf+OFF_M_ZIP, sMailAdr.Zip, 3);
      else
      {
         LogMsg("*** Invalid mailing zipcode: %.9s [%.*s]", sMailAdr.Zip, iApnLen, pOutbuf);
      }
   }
}

/********************************* Fre_MergeRoll *****************************
 *
 * Return 0 if successful, >0 if error
 *
 *****************************************************************************/

int Fre_MergeLien(char *pOutbuf, char *pRollRec, int iLen, int iFlag)
{
   FRE_LIEN *pRec;
   char     acTmp[256], acTmp1[256], *pTmp;
   long     lTmp;
   double   dTmp;
   int      iRet;

   pRec = (FRE_LIEN *)pRollRec;

   // Clear output buffer
   memset(pOutbuf, ' ', iRecLen);

   // Start copying data
   memcpy(pOutbuf, pRec->Apn, RSIZ_APN);
   memcpy(pOutbuf+OFF_CO_NUM, "10FRE", 5);

   // Format APN
   iRet = formatApn(pOutbuf, acTmp, &myCounty);
   memcpy(pOutbuf+OFF_APN_D, acTmp, iRet);

   // Create MapLink
   iRet = formatMapLink(pOutbuf, acTmp, &myCounty);
   memcpy(pOutbuf+OFF_MAPLINK, acTmp, iRet);

   // Create index map link
   if (getIndexPage(acTmp, acTmp1, &myCounty))
      memcpy(pOutbuf+OFF_IMAPLINK, acTmp1, iRet);

   // Year assessed
   memcpy(pOutbuf+OFF_YR_ASSD, myCounty.acYearAssd, 4);

   // Lien Values
   long lLand = atoin(pRec->Land, LSIZ_LAND);
   long lFixt  = atoin(pRec->Fixt, LSIZ_FIXT);
   long lPP_MH = atoin(pRec->PP_Val, LSIZ_PERSPROP_MB);
   long lMH    = atoin(pRec->MH_Value, LSIZ_MB_HOME_VALUE);
   long lImpr = atoin(pRec->Impr, LSIZ_IMPR_FIXT);

   // Fixture is included in Impr, we have to subtract it
   if (lFixt > 0)
   {
      if (lImpr >= lFixt)
      {
         lImpr -= lFixt;      
         sprintf(acTmp, "%d         ", lFixt);
         memcpy(pOutbuf+OFF_FIXTR, acTmp, SIZ_FIXTR);

      } else
      {
         LogMsg("??? MergeLien(): bad data on %.9s: Impr=%d, Fixt=%d, PP=%d.  Reset Fixt.", pRec->Apn, lImpr, lFixt, lPP_MH);
         lFixt = 0;
      }
   }

   if (lLand > 0)
   {
      sprintf(acTmp, "%*u", SIZ_LAND, lLand);
      memcpy(pOutbuf+OFF_LAND, acTmp, SIZ_LAND);
   }
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*u", SIZ_IMPR, lImpr);
      memcpy(pOutbuf+OFF_IMPR, acTmp, SIZ_IMPR);
   }

   long lOthers = lFixt+lPP_MH;
   if (lOthers > 0)
   {
      sprintf(acTmp, "%*u", SIZ_OTHER, lOthers);
      memcpy(pOutbuf+OFF_OTHER, acTmp, SIZ_OTHER);

      // PP_MH contains both PP & MH
      if (lPP_MH > lMH)
      {
         sprintf(acTmp, "%d         ", lPP_MH-lMH);
         memcpy(pOutbuf+OFF_PERSPROP, acTmp, SIZ_PERSPROP);
      }
      if (lMH > 0)
      {
         sprintf(acTmp, "%d         ", lMH);
         memcpy(pOutbuf+OFF_PP_MH, acTmp, SIZ_PP_MH);
      }
   }

   // Gross
   LONGLONG lGross = lOthers + lLand + lImpr;
   if (lGross > 0)
   {
      sprintf(acTmp, "%*u", SIZ_GROSS, lGross);
      memcpy(pOutbuf+OFF_GROSS, acTmp, SIZ_GROSS);

      // Ratio
      if (lImpr > 0)
      {
         dTmp = (LONGLONG)lImpr*100/(lLand+lImpr)+0.5;
         sprintf(acTmp, "%*u", SIZ_RATIO, (long)dTmp);
         memcpy(pOutbuf+OFF_RATIO, acTmp, SIZ_RATIO);
      }
   }

   // Parcel status - assume county send us only active parcels
   *(pOutbuf+OFF_STATUS) = 'A';

   // HO Exempt
   if (pRec->HOExe_Code == 'A')
   {
      *(pOutbuf+OFF_HO_FL) = '1';      // 'Y'
      *(pOutbuf+OFF_EXE_CD1) = pRec->HOExe_Code;
   } else
      *(pOutbuf+OFF_HO_FL) = '2';      // 'N'

   lTmp = atoin(pRec->HOExe, LSIZ_HOEXE);
   lTmp += atoin(pRec->ClsExe, LSIZ_CLS_EXE_AMT);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*u", SIZ_EXE_TOTAL, lTmp);
      memcpy(pOutbuf+OFF_EXE_TOTAL, acTmp, SIZ_EXE_TOTAL);
   }

   // Other exempt code
   if (pRec->ClsExe_Code > ' ')
   {
      if (*(pOutbuf+OFF_EXE_CD1) == ' ')
         *(pOutbuf+OFF_EXE_CD1) = pRec->ClsExe_Code;
      else
         *(pOutbuf+OFF_EXE_CD2) = pRec->ClsExe_Code;
   }

   // Create exemption type
   makeExeType(pOutbuf+OFF_EXE_TYPE, pOutbuf+OFF_EXE_CD1, pOutbuf+OFF_EXE_CD2, pOutbuf+OFF_EXE_CD3, (IDX_TBL4 *)&FRE_Exemption);

   // Set full exemption flag
   if ((lGross - lTmp) <= 0) 
      *(pOutbuf+OFF_FULL_EXEMPT) = 'Y';

   // TRA
   memcpy(pOutbuf+OFF_TRA, pRec->TRA, LSIZ_TRA);

   // Legal
   memcpy(acTmp, pRec->Narative, LSIZ_NARRATIVE);
   blankRem(acTmp, LSIZ_NARRATIVE);
   if (acTmp[1] == '|')
      acTmp[1] = '1';
   updateLegal(pOutbuf, acTmp);

   // Sold to state
   lTmp = atoin(pRec->Sold2St_Date, LSIZ_SOLD2STATE_DATE);
   if (lTmp > 0)
   {
      char  acYear[4];
      memcpy(acYear, pRec->Sold2St_Date, 2);
      acYear[2] = 0;
      pTmp = dateConversion(acYear, acTmp, YY2YYYY);
      if (pTmp)
         memcpy(pOutbuf+OFF_DEL_YR, acTmp, 4);
   }

   // Owner
   Fre_MergeOwner(pOutbuf, pRec->Name1, pRec->Name2);

   // Mailing
   Fre_MergeMAdr(pOutbuf, pRec->M_Addr1, pRec->M_Addr2, pRec->M_Addr3, pRec->M_Addr4);

   return 0;
}

/********************************* Fre_MergeNavRoll **************************
 *
 * Return 0 if successful, >0 if error
 *
 *****************************************************************************/

int Fre_MergeNavRoll(char *pOutbuf, char *pRollRec, int iLen, int iFlag)
{
   FRE_NAV  *pRec;
   char     acTmp[256], acTmp1[256], *pTmp;
   long     lTmp;
   int      iRet;

   pRec = (FRE_NAV *)pRollRec;
   if (iFlag & CREATE_R01)
   {
      // Clear output buffer
      memset(pOutbuf, ' ', iRecLen);

      // Start copying data
      memcpy(pOutbuf, pRec->Apn, RSIZ_APN);
      memcpy(pOutbuf+OFF_CO_NUM, "10FRE", 5);

      // Format APN
      iRet = formatApn(pRec->Apn, acTmp, &myCounty);
      memcpy(pOutbuf+OFF_APN_D, acTmp, iRet);

      // Create MapLink
      iRet = formatMapLink(pRec->Apn, acTmp, &myCounty);
      memcpy(pOutbuf+OFF_MAPLINK, acTmp, iRet);

      // Create index map link
      if (getIndexPage(acTmp, acTmp1, &myCounty))
         memcpy(pOutbuf+OFF_IMAPLINK, acTmp1, iRet);

      // Year assessed
      memcpy(pOutbuf+OFF_YR_ASSD, myCounty.acYearAssd, 4);

   }

#ifdef _DEBUG
//   if (!memcmp(pRec->Apn, "10145008", 8))
//      iRet = 1;
#endif

   // Legal
   memcpy(acTmp, pRec->Narative, RSIZ_NARRATIVE);
   acTmp[RSIZ_NARRATIVE] = 0;
   if (acTmp[1] == '|')
      acTmp[1] = '1';
   iRet = updateLegal(pOutbuf, acTmp);

   // Transfer - mmddyy
   char  acDate[16];
   memcpy(acTmp, pRec->RecDate1, NSIZ_RECDATE);
   pTmp = dateConversion(acTmp, acDate, MMDDYY2);
   if (pTmp)
   {
      lTmp = atoin(pRec->RecNum1, NSIZ_RECNUM);
      if (lTmp)
      {
         memcpy(pOutbuf+OFF_TRANSFER_DT, acDate, SIZ_TRANSFER_DT);
         sprintf(acTmp, "%.7d                 ", lTmp);
         memcpy(pOutbuf+OFF_TRANSFER_DOC, acTmp, SIZ_TRANSFER_DOC);
      }
   }

   // Owner name
   Fre_MergeOwner(pOutbuf, pRec->Name1, pRec->Name2);

   // Mailing
   Fre_MergeMAdr(pOutbuf, pRec->M_Addr1, pRec->M_Addr2, pRec->M_Addr3, pRec->M_Addr4);

   return 0;
}


/*************************** Fre_MergePublicParcels *************************
 *
 * This function merge public parcels from PCTRA.TXT file to roll file.
 * Input: FRE_TRA
 *
 ****************************************************************************/

int Fre_MergePublicParcels(char *pOutbuf)
{
   static   char acRec[1024], *pRec=NULL;
   char     acTmp[256], acTmp1[256];
   int      iLoop, iRet, iTmp;
   char     acBuf[MAX_RECSIZE];
   DWORD    nBytesWritten;
   ULONG     lLotAcres, lLotSqft;

   FRE_TRA   *pPubParcel = (FRE_TRA *)&acRec[0];

   // Get first rec for first call
   if (!pRec)
      pRec = fgets(acRec, 1024, fdPubParcel);

   iRet = 1;
   do
   {
      if (!pRec)
      {
         fclose(fdPubParcel);
         fdPubParcel = NULL;
         break;      // EOF
      }
#ifdef _DEBUG
      //if (!memcmp(pPubParcel->Apn, "34328060", 8))
      //   iRet = 1;
#endif

      // Compare pPubParcel
      iLoop = memcmp(pOutbuf, pPubParcel->Apn, TSIZ_APN);
      if (iLoop > 0)
      {
         // Skip bad records
         if (strlen(acRec) > TOFF_ACREAGE && pPubParcel->Owner[0] > ' ')
         {
            if (bDebug)
               LogMsg0("Add new public parcel rec  %.*s", TSIZ_APN, pPubParcel->Apn);

            memset(acBuf, ' ', iRecLen);

            // Format APN
            iTmp = formatApn(pPubParcel->Apn, acTmp, &myCounty);
            memcpy(&acBuf[OFF_APN_D], acTmp, iTmp);

            memcpy(acBuf, pPubParcel->Apn, TSIZ_APN);
            memcpy(&acBuf[OFF_CO_NUM], "10FREA", 6);

            // Create MapLink
            iTmp = formatMapLink(pPubParcel->Apn, acTmp, &myCounty);
            memcpy(&acBuf[OFF_MAPLINK], acTmp, iTmp);

            // Create index map link
            if (getIndexPage(acTmp, acTmp1, &myCounty))
               memcpy(&acBuf[OFF_IMAPLINK], acTmp1, iTmp);

            // TRA
            memcpy(&acBuf[OFF_TRA], pPubParcel->TRA, TSIZ_TRA);

            // Year assessed
            memcpy(&acBuf[OFF_YR_ASSD], myCounty.acYearAssd, 4);

            // Set publuc parcel flag
            acBuf[OFF_PUBL_FLG] = 'Y';

            // Owner
            Fre_MergeOwner(acBuf, pPubParcel->Owner, "");

            // Lot Acreage - V99
            lLotAcres = atoin(pPubParcel->Acreage, TSIZ_ACREAGE);
            if (lLotAcres > 0)
            {
               lLotSqft = (ULONG)((double)lLotAcres*SQFT_FACTOR_100);
               lLotAcres *= 10;

               if (lLotSqft > 999999999)
                  lLotSqft = 0;
               sprintf(acTmp, "%*u", SIZ_LOT_SQFT, lLotSqft);
               memcpy(&acBuf[OFF_LOT_SQFT], acTmp, SIZ_LOT_SQFT);

               sprintf(acTmp, "%*u", SIZ_LOT_ACRES, lLotAcres);
               memcpy(&acBuf[OFF_LOT_ACRES], acTmp, SIZ_LOT_ACRES);
            }

            if (memcmp(acLastApn, acBuf, TSIZ_APN))
            {
               memcpy(acLastApn, acBuf, TSIZ_APN);

               WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
               lPubParcelAdd++;
            }
         }

         pRec = fgets(acRec, 1024, fdPubParcel);
         iRet = 0;
      } else if (!iLoop)
      {
         // Update known public parcels
         if (strlen(acRec) > TOFF_ACREAGE && pPubParcel->Owner[0] > ' ')
         {
            // Lot Acreage - V99
            lLotAcres = atoin(pPubParcel->Acreage, TSIZ_ACREAGE);
            if (lLotAcres > 0)
            {
               lLotSqft = (ULONG)((double)lLotAcres*SQFT_FACTOR_100);
               lLotAcres *= 10;

               if (lLotSqft > 999999999)
                  lLotSqft = 0;
               sprintf(acTmp, "%*u", SIZ_LOT_SQFT, lLotSqft);
               memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);

               sprintf(acTmp, "%*u", SIZ_LOT_ACRES, lLotAcres);
               memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);
            }

            *(pOutbuf+OFF_PUBL_FLG) = 'Y';
         }

         pRec = fgets(acRec, 1024, fdPubParcel);
         lPubParcelMatch++;
         iRet = 0;
      }
   } while (iLoop > 0);

   return iRet;
}

/********************************* Fre_Load_Roll ****************************
 *
 *
 ****************************************************************************/

int Fre_Load_Roll(int iSkip)
{
   char     acRec[MAX_RECSIZE], acBuf[MAX_RECSIZE], acRollRec[1024];
   char     acRawFile[_MAX_PATH], acOutFile[_MAX_PATH], acLastApn[32];

   HANDLE   fhIn;

   int      iRet, iTmp, iRollUpd=0, iNewRec=0, iRetiredRec=0;
   DWORD    nBytesRead;
   DWORD    nBytesWritten;
   BOOL     bRet, bEof=false;
   long     lRet=0, lCnt=0;

   sprintf(acRawFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "S01");
   sprintf(acOutFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "R01");

   // Rename files for processing
   if (_access(acRawFile, 0))
   {
      if (!_access(acOutFile, 0))
         rename(acOutFile, acRawFile);
      else
      {
         LogMsg("***** Missing input file %s.  Please recheck!", acRawFile);
         return -1;
      }
   }

   // Open Roll file
   LogMsg("Open Roll file %s", acRollFile);
   fdRoll = fopen(acRollFile, "rb");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", acRollFile);
      return -2;
   }

   // Open Public Parcel file
   LogMsg("Open Public Parcel file %s", acPubParcelFile);
   fdPubParcel = fopen(acPubParcelFile, "r");
   if (fdPubParcel == NULL)
   {
      LogMsg("***** Error opening Public Parcel file: %s\n", acPubParcelFile);
      return -2;
   }

   // Open Situs file
   LogMsg("Open Situs file %s", acSitusFile);
   fdSitus = fopen(acSitusFile, "r");
   if (fdSitus == NULL)
   {
      LogMsg("***** Error opening Situs file: %s\n", acSitusFile);
      return -2;
   }

   // Open Char file
   LogMsg("Open Char file %s", acCharFile);
   fdChar = fopen(acCharFile, "r");
   if (fdChar == NULL)
   {
      LogMsg("***** Error opening Char file: %s\n", acCharFile);
      return -2;
   }

   // Open Input file
   LogMsg("Open input file %s", acRawFile);
   fhIn = CreateFile(acRawFile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhIn == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening input file: %s\n", acRawFile);
      return -2;
   }

   // Open Output file
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error creating output file: %s\n", acOutFile);
      return -2;
   }

   // Get first RollRec - Don't use fgets to prevent reading char(0x1A) in legal
   iRet = fread(acRollRec, 1, iRollLen, fdRoll);

   // Copy skip record
   memset(acBuf, ' ', iRecLen);
   while (iSkip-- > 0)
   {
      ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);
      // Ignore record start with 99999999
      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
   }

   iNoMatch=iBadCity=iBadSuffix=0;
   acLastApn[0] = 0;

   // Merge loop
   while (!bEof)
   {
      bRet = ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);

      // Check for EOF
      if (!bRet)
      {
         LogMsg("***** Error reading input file %s (%f)", acRawFile, GetLastError());
         break;
      }

      // EOF ?
      if (iRecLen != nBytesRead)
         break;

      NextRollRec:
      // Replace unprintable chars with space
      replUnPrtChar(acRollRec, ' ', iRollLen);

#ifdef _DEBUG
      //if (!memcmp(acBuf, "34328060", 8))
      //   iRet = 1;
#endif
      iTmp = memcmp(acBuf, acRollRec, iApnLen);
      if (!iTmp)
      {
         // Merge roll data
         iRet = Fre_MergeNavRoll(acBuf, acRollRec, iRollLen, UPDATE_R01);
         iRollUpd++;

         // Merge Situs
         if (fdSitus)
            iRet = Fre_MergeSitus(acBuf);

         // Merge Char
         if (fdChar)
            iRet = Fre_MergeChar(acBuf);

         // Merge public parcels
         if (fdPubParcel)
            iRet = Fre_MergePublicParcels(acBuf);

         // Read next roll record
         iRet = fread(acRollRec, 1, iRollLen, fdRoll);
         if (iRet != iRollLen)
            bEof = true;    // Signal to stop
      } else if (iTmp > 0)       // Roll not match, new roll record?
      {
         if (bDebug)
            LogMsg0("*** New roll record : %.*s (%d) ***", iApnLen, acRollRec, lCnt);

         // Create new R01 record
         iRet = Fre_MergeNavRoll(acRec, acRollRec, iRollLen, CREATE_R01);
         if (!iRet)
         {
            iNewRec++;

            // Merge Situs
            if (fdSitus)
               iRet = Fre_MergeSitus(acRec);

            // Merge Char
            if (fdChar)
               iRet = Fre_MergeChar(acRec);

            // Merge public parcels
            if (fdPubParcel)
               iRet = Fre_MergePublicParcels(acRec);

            // Save last recording date
            lRet = atoin((char *)&acRec[OFF_TRANSFER_DT], 8);
            if (lRet > lLastRecDate && lRet < lToday)
               lLastRecDate = lRet;

            if (memcmp(acLastApn, acRec, SIZ_APN_S))
            {
               memcpy(acLastApn, acRec, SIZ_APN_S);
               bRet = WriteFile(fhOut, acRec, iRecLen, &nBytesWritten, NULL);
               lCnt++;
            } else
               LogMsg("*** 1) Duplicate APN: %.14s", acLastApn);
         } else
            LogMsg("*** 2) Duplicate APN: %.14s", acLastApn);

         // Get next roll record
         iRet = fread(acRollRec, 1, iRollLen, fdRoll);
         if (iRet != iRollLen)
            bEof = true;    // Signal to stop
         else
            goto NextRollRec;
      } else 
      {
         // Do not count retired public parcels         
         if (acBuf[OFF_PUBL_FLG] != 'Y')
            iRetiredRec++;

         // Record may be retired
         if (bDebug)
            LogMsg0("Roll not match (retired record?) : R01->%.*s < Roll->%.*s (%d)", iApnLen, acBuf, iApnLen, acRollRec, lCnt);
         continue;
      } 

      // Save last recording date
      lRet = atoin((char *)&acBuf[OFF_TRANSFER_DT], 8);
      if (lRet > lLastRecDate && lRet < lToday)
         lLastRecDate = lRet;

      if (memcmp(acLastApn, acBuf, SIZ_APN_S))
      {
         memcpy(acLastApn, acBuf, SIZ_APN_S);
         bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
         lCnt++;
      } else
         LogMsg("*** 3) Duplicate APN: %.14s", acLastApn);

      if (!(lCnt % 1000))
         printf("\r%u", lCnt);

      if (!bRet)
      {
         LogMsg("***** Error occurs: %d\n", GetLastError());
         lRet = WRITE_ERR;
         break;
      }
   }

   // Do the rest of the file
   while (!bEof)
   {
      if (bDebug)
         LogMsg0("*** New roll record : %.*s (%d) ***", iApnLen, acRollRec, lCnt);

      replUnPrtChar(acRollRec, ' ', iRollLen);

      // Create new R01 record
      iRet = Fre_MergeNavRoll(acRec, acRollRec, iRollLen, CREATE_R01);
      if (!iRet)
      {
         iNewRec++;

         // Merge Situs
         if (fdSitus)
            iRet = Fre_MergeSitus(acRec);

         // Merge Char
         if (fdChar)
            iRet = Fre_MergeChar(acRec);

         // Merge public parcels
         if (fdPubParcel)
            iRet = Fre_MergePublicParcels(acRec);

         // Save last recording date
         lRet = atoin((char *)&acRec[OFF_TRANSFER_DT], 8);
         if (lRet > lLastRecDate && lRet < lToday)
            lLastRecDate = lRet;

         if (memcmp(acLastApn, acRec, SIZ_APN_S))
         {
            memcpy(acLastApn, acRec, SIZ_APN_S);
            bRet = WriteFile(fhOut, acRec, iRecLen, &nBytesWritten, NULL);
            lCnt++;
         } else
            LogMsg("*** 4) Duplicate APN: %.14s", acLastApn);
      }

      // Get next roll record
      iRet = fread(acRollRec, 1, iRollLen, fdRoll);
      if (iRet != iRollLen)
         bEof = true;    // Signal to stop
   }

   // Close files
   if (fdRoll)
      fclose(fdRoll);
   if (fdPubParcel)
      fclose(fdPubParcel);
   if (fdSitus)
      fclose(fdSitus);
   if (fdChar)
      fclose(fdChar);

   if (fhOut)
      CloseHandle(fhOut);
   if (fhIn)
      CloseHandle(fhIn);

   LogMsg("Total output records:       %u", lCnt);
   LogMsg("Total new records:          %u", iNewRec);
   LogMsg("Total retired records:      %u", iRetiredRec);
   LogMsg("Total Char matched:         %u", lCharMatch);
   LogMsg("Total Situs matched:        %u", lSitusMatch);
   LogMsg("Total Public Parcel matched:%u", lPubParcelMatch);
   LogMsg("Total Public Parcel added:  %u\n", lPubParcelAdd);

   LogMsg("Total Char skipped:         %u", lCharSkip);
   LogMsg("Total Situs skipped:        %u\n", lSitusSkip);

   if (lDupOwners > 0)
      LogMsg("Number of duplicate owners: %u", lDupOwners);

   LogMsg("Total bad-city records:     %u", iBadCity);
   LogMsg("Total bad-suffix records:   %u\n", iBadSuffix);

   printf("\nTotal output records from Fre_Load_Roll(): %u\n", lCnt);

   lRecCnt = lCnt;
   return 0;
}

/********************************* Fre_MergeProp8 ****************************
 *
 * Set Prop8 flag using extracted prop8 file 
 *
 *****************************************************************************/

int Fre_MergeProp8(char *pOutbuf)
{
   static   char acRec[256], *pRec=NULL;
   int      iLoop;

   // Get first Char rec for first call
   if (!pRec)
   {
      // Skip header
      pRec = fgets(acRec, 256, fdProp8);
      if (acRec[0] > '9')
         pRec = fgets(acRec, 256, fdProp8);
   }

   do
   {
      if (!pRec)
      {
         fclose(fdProp8);
         fdProp8 = NULL;
         return 1;      // EOF
      }

      // Compare Apn
      iLoop = memcmp(pOutbuf, acRec, iApnLen);
      if (iLoop > 0)
      {
         if (bDebug)
            LogMsg0("Skip Prop8 rec  %s", acRec);
         pRec = fgets(acRec, 256, fdProp8);
         lProp8Skip++;
      }
   } while (iLoop > 0);

   // If not match, return
   if (iLoop)
      return 1;

   // Prop 8
   *(pOutbuf+OFF_PROP8_FLG) = 'Y';

   lProp8Match++;

   // Get next Char rec
   pRec = fgets(acRec, 256, fdProp8);

   return 0;
}

/********************************* Fre_Load_LDR *****************************
 *
 *
 ****************************************************************************/

int Fre_Load_LDR(int iFirstRec /* 1=create header rec */)
{
   char     *pTmp, acBuf[MAX_RECSIZE], acRollRec[MAX_RECSIZE];
   char     acOutFile[_MAX_PATH];
   DWORD    nBytesWritten;
   BOOL     bRet;
   long     lRet=0, lCnt=0;
   int      iRet, iRollUpd=0;

   sprintf(acOutFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "R01");

   // Open Roll file
   LogMsg("Open Roll file %s", acRollFile);
   fdRoll = fopen(acRollFile, "r");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", acRollFile);
      return -2;
   }

   // Open Public Parcel file
   LogMsg("Open Public Parcel file %s", acPubParcelFile);
   fdPubParcel = fopen(acPubParcelFile, "r");
   if (fdPubParcel == NULL)
   {
      LogMsg("***** Error opening Public Parcel file: %s\n", acPubParcelFile);
      return -2;
   }

   // Open Char file
   LogMsg("Open Char file %s", acCharFile);
   fdChar = fopen(acCharFile, "r");
   if (fdChar == NULL)
   {
      LogMsg("***** Error opening Char file: %s\n", acCharFile);
      return -2;
   }

   // Open Prop8 file
   sprintf(acProp8File, acProp8Tmpl, myCounty.acCntyCode, myCounty.acCntyCode, myCounty.acYearAssd);
   if (!_access(acProp8File, 0))
   {
      LogMsg("Open Prop8 file %s", acProp8File);
      fdProp8 = fopen(acProp8File, "r");
      if (fdProp8 == NULL)
      {
         LogMsg("***** Error opening Prop8 file: %s\n", acProp8File);
         return -2;
      }
   } else
      fdProp8 = NULL;

   // Open Situs file
   LogMsg("Open Situs file %s", acSitusFile);
   fdSitus = fopen(acSitusFile, "r");
   if (fdSitus == NULL)
   {
      LogMsg("***** Error opening Situs file: %s\n", acSitusFile);
      return -2;
   }

   // Open Output file
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error creating output file: %s\n", acOutFile);
      return -2;
   }

   // Create header record
   if (iFirstRec)
   {
      memset(acBuf, '9', iRecLen);
      // Ignore record start with 99999999
      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
   }

   iNoMatch=iBadCity=iBadSuffix=0;

   // Get first RollRec
   pTmp = fgets((char *)&acRollRec[0], MAX_RECSIZE, fdRoll);
   memset(acBuf, ' ', iRecLen);

   // Merge loop
   while (pTmp)
   {
#ifdef _DEBUG
      //if (!memcmp(acBuf, "40145303", 8))
      //   iRet = 1;
#endif

      // Create new R01 record
      iRet = Fre_MergeLien(acBuf, acRollRec, iRollLen, CREATE_R01);

      // Merge Char
      if (fdChar)
         iRet = Fre_MergeChar(acBuf);

      // Merge Situs
      if (fdSitus)
         iRet = Fre_MergeSitus(acBuf);

      // Set Prop8 flag
      if (fdProp8)
         iRet = Fre_MergeProp8(acBuf);

#ifdef _DEBUG
      iRet = replCharEx(acBuf, 31, ' ', iRecLen);
      if (iRet)
         LogMsg("Error: null is found at %d on %.*s", iRet, iApnLen, acBuf);
#endif
      lLDRRecCount++;
      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
      if (!bRet)
      {
         LogMsg("***** Error occurs: %d\n", GetLastError());
         lRet = WRITE_ERR;
         break;
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);

      // Get next roll record
      pTmp = fgets(acRollRec, MAX_RECSIZE, fdRoll);
   }

   // Close files
   if (fdProp8)
      fclose(fdProp8);
   if (fdRoll)
      fclose(fdRoll);
   if (fdPubParcel)
      fclose(fdPubParcel);
   if (fdChar)
      fclose(fdChar);
   if (fdSitus)
      fclose(fdSitus);
   if (fdSale)
      fclose(fdSale);
   if (fhOut)
      CloseHandle(fhOut);

   LogMsg("Total input records:        %u", lCnt);
   LogMsg("     output records:        %u", lLDRRecCount);
   LogMsg("       Char matched:        %u", lCharMatch);
   LogMsg("       Char skipped:        %u", lCharSkip);
   LogMsg("      Situs matched:        %u", lSitusMatch);
   LogMsg("      Situs skipped:        %u", lSitusSkip);
   LogMsg("      Prop8 matched:        %u", lProp8Match);
   LogMsg("      Prop8 skipped:        %u\n", lProp8Skip);

   LogMsg("Total bad-city records:     %u", iBadCity);
   LogMsg("Total bad-suffix records:   %u\n", iBadSuffix);

   printf("\nTotal output records: %u\n", lCnt);

   lRecCnt = lLDRRecCount;
   return 0;
}

/***************************** Fre_CreateLienRec  ****************************
 *
 * Return 0 if successful, >0 if error
 *
 *****************************************************************************/

int Fre_CreateLienRec(char *pOutbuf, char *pRollRec)
{
   char     acTmp[256];
   long     lTmp, iTmp;
   LIENEXTR *pLienRec = (LIENEXTR *)pOutbuf;
   FRE_LIEN *pRec     = (FRE_LIEN *)pRollRec;

   // Clear output buffer
   memset(pOutbuf, ' ', sizeof(LIENEXTR));

   // Start copying data
   memcpy(pLienRec->acApn, pRec->Apn, RSIZ_APN);

   // TRA
   lTmp = atoin(pRec->TRA, RSIZ_TRA);
   if (lTmp > 0)
   {
      iTmp = sprintf(acTmp, "%.6d", lTmp);
      memcpy(pLienRec->acTRA, acTmp, iTmp);
   }

   // Year assessed
   memcpy(pLienRec->acYear, myCounty.acYearAssd, 4);

   // HO Exempt
   long lHoExe = atoin(pRec->HOExe, LSIZ_HOEXE);
   long lOthExe= atoin(pRec->ClsExe, LSIZ_CLS_EXE_AMT);
   long lTotalExe = lHoExe+lOthExe;
   if (lTotalExe > 0)
   {
      iTmp = sprintf(acTmp, "%*u", sizeof(pLienRec->acExAmt), lTotalExe);
      memcpy(pLienRec->acExAmt, acTmp, iTmp);

      if (lHoExe > 0)
      {
         pLienRec->acHO[0] = '1';      // 'Y'
         iTmp = sprintf(acTmp, "%*u", sizeof(pLienRec->extra.Fre.HOExe), lHoExe);
         memcpy(pLienRec->extra.Fre.HOExe, acTmp, iTmp);
      } else
         pLienRec->acHO[0] = '2';      // 'N'

      if (lOthExe > 0)
      {
         pLienRec->acExCode[0] = pRec->ClsExe_Code;
         pLienRec->extra.Fre.ClsExe_Cnt = pRec->ClsExe_Cnt;
         iTmp = sprintf(acTmp, "%*u", sizeof(pLienRec->extra.Fre.ClsExe), lOthExe);
         memcpy(pLienRec->extra.Fre.ClsExe, acTmp, iTmp);
      }
   }

   // Lien Values
   long lLand = atoin(pRec->Land, LSIZ_LAND);
   long lFixt  = atoin(pRec->Fixt, LSIZ_FIXT);
   long lPP_MH = atoin(pRec->PP_Val, LSIZ_PERSPROP_MB);
   long lMH    = atoin(pRec->MH_Value, LSIZ_MB_HOME_VALUE);
   long lImpr = atoin(pRec->Impr, LSIZ_IMPR_FIXT);

   // Fixture is included in Impr, we have to subtract it
   if (lFixt > 0)
   {
      if (lImpr >= lFixt)
      {
         lImpr -= lFixt;      
         sprintf(acTmp, "%*u", SIZ_LIEN_PERS, lFixt);
         memcpy(pLienRec->acME_Val, acTmp, SIZ_LIEN_PERS);

      } else
      {
         LogMsg("??? CreateLienRec(): bad data on %.9s: Impr=%d, Fixt=%d, PP=%d. Reset Fixt.", pRec->Apn, lImpr, lFixt, lPP_MH);
         lFixt = 0;
      }
   }

   if (lLand > 0)
   {
      sprintf(acTmp, "%*u", SIZ_LIEN_LAND, lLand);
      memcpy(pLienRec->acLand, acTmp, SIZ_LIEN_LAND);
   }
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*u", SIZ_LIEN_IMPR, lImpr);
      memcpy(pLienRec->acImpr, acTmp, SIZ_LIEN_IMPR);
   }

   LONGLONG lOthers = lFixt+lPP_MH;
   if (lOthers > 0)
   {
      sprintf(acTmp, "%*u", SIZ_LIEN_PERS, lOthers);
      memcpy(pLienRec->acOther, acTmp, SIZ_LIEN_PERS);

      // PP_MH contains both PP & MH
      if (lPP_MH > lMH)
      {
         sprintf(acTmp, "%*u", SIZ_LIEN_PERS, lPP_MH-lMH);
         memcpy(pLienRec->acPP_Val, acTmp, SIZ_LIEN_PERS);
      }
      if (lMH > 0)
      {
         iTmp = sprintf(acTmp, "%*u", sizeof(pLienRec->extra.Fre.MH_Val), lMH);
         memcpy(pLienRec->extra.Fre.MH_Val, acTmp, iTmp);
      }
   }

   // Gross
   LONGLONG lGross = lOthers + lLand + lImpr;
   if (lGross > 0)
   {
      sprintf(acTmp, "%*u", SIZ_LIEN_GROSS, lGross);
      memcpy(pLienRec->acGross, acTmp, SIZ_LIEN_GROSS);
   }

   // Impr - Ratio
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*u", SIZ_LIEN_RATIO, (long)((LONGLONG)(lImpr*100)/(lLand+lImpr)+0.5));
      memcpy(pLienRec->acRatio, acTmp, SIZ_LIEN_RATIO);
   }

   pLienRec->LF[0] = '\n';
   pLienRec->LF[1] = 0;

   return 0;
}

/********************************* Fre_ExtrLien *****************************
 *
 *
 ****************************************************************************/

int Fre_ExtrLien()
{
   char     *pTmp, acBuf[MAX_RECSIZE], acRollRec[MAX_RECSIZE];
   char     acOutFile[_MAX_PATH];
   FILE     *fdLien;
   int      iRet;
   long     lCnt=0;

   LogMsg0("Extract Lien file");

   // Open roll file
   LogMsg("Open Roll file %s", acRollFile);
   fdRoll = fopen(acRollFile, "r");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", acRollFile);
      return -1;
   }

   // Open Output file
   sprintf(acOutFile, acLienTmpl, myCounty.acCntyCode, myCounty.acCntyCode);
   LogMsg("Open lien extract file %s", acOutFile);
   fdLien = fopen(acOutFile, "w");
   if (fdLien == NULL)
   {
      LogMsg("***** Error creating lien extract file: %s\n", acOutFile);
      return -2;
   }

   // Merge loop
   while (!feof(fdRoll))
   {
      // Get roll rec
      pTmp = fgets(acRollRec, iRollLen, fdRoll);

      if (!pTmp)
         break;

      // Create new value record
      iRet = Fre_CreateLienRec(acBuf, acRollRec);

      // Write to output
      fputs(acBuf, fdLien);
      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdRoll)
      fclose(fdRoll);
   if (fdLien)
      fclose(fdLien);

   LogMsg("Total output records:       %u", lCnt);
   printf("\nTotal output records: %u", lCnt);

   lRecCnt = lCnt;
   return 0;
}

/********************************* Fre_FormatSale ****************************
 *
 * Return -1 if bad record.  0 if OK.
 *
 *****************************************************************************/

int Fre_FormatSale(char *pFmtSale, char *pSale)
{
   SCSAL_REC *pCSale = (SCSAL_REC *)pFmtSale;
   FRE_SALE *pSaleRec = (FRE_SALE *)pSale;
   int		iTmp;
   char     acTmp[32], *pTmp;
   char     acDocNum[32], acDocDate[32];
   long     lCurSaleDt, lLstSaleDt, lPrice, lDocNum;

   memset(pFmtSale, ' ', sizeof(SCSAL_REC));
   memcpy(pCSale->Apn, pSaleRec->Apn, SSIZ_APN);
   pTmp = dateConversion(pSaleRec->RecDate, acDocDate, MMDDYY2);
   if (pTmp)
      lCurSaleDt = atoin(pTmp, SSIZ_RECDATE);
   else
      return -1;

   lLstSaleDt = 0;
   if (lCurSaleDt < lLstSaleDt)
      return -1;

   lPrice = atoin(pSaleRec->Dtt_Amt, SSIZ_DTT_AMT);
   if (lPrice > 0)
      lPrice = (long)((double)lPrice*SALE_FACTOR_100);
   else
      lPrice = atoin(pSaleRec->SalePrice, SSIZ_SALE_AMT);

   // Check DocNum
   lDocNum = atoin(pSaleRec->DocNum, SSIZ_INST_NUMBER);
   //if (lDocNum > 400000 && !lPrice)
   //{
   //   LogMsg("*** Ignore record: DocNum=%.6s, DocDate=%.8s, DocType=%.2s, SalePrice=%u, APN=%.10s", 
   //      pSaleRec->DocNum, pSaleRec->TransferDate, pSaleRec->DocCode, lPrice, pSaleRec->Apn);
   //   return -2;
   //}
   iTmp = sprintf(acDocNum, "%.7d", lDocNum);
   memcpy(pCSale->DocNum, acDocNum, iTmp);
   memcpy(pCSale->DocDate, acDocDate, SALE_SIZ_DOCDATE);

   // Check for questionable sale amt
   if (lPrice > 1000)
   {
      iTmp = sprintf(acTmp, "%*u", SALE_SIZ_SALEPRICE, lPrice);
      memcpy(pCSale->SalePrice, acTmp, iTmp);
   }

   // 20201201
   //pTmp = dateConversion(pSaleRec->TransferDate, acTmp, MMDDYY2);
   //if (pTmp)
      memcpy(pCSale->TransferDate, pSaleRec->TransferDate, SSIZ_TRANS_DATE);

   // Translate DocCode to standard DocType
   if (pSaleRec->DocCode[0] > ' ')
   {
      memcpy(pCSale->DocCode, pSaleRec->DocCode, SSIZ_DOC_CODE);
      iTmp = findDocType(pSaleRec->DocCode, (IDX_TBL5 *)&FRE_DocCode[0]);
      if (iTmp >= 0)
      {
         memcpy(pCSale->DocType, FRE_DocCode[iTmp].pCode, FRE_DocCode[iTmp].iCodeLen);
         pCSale->NoneSale_Flg = FRE_DocCode[iTmp].flag;
         iTmp = atoin(pSaleRec->DocCode, SSIZ_DOC_CODE);
         if (iTmp > 20)
            pCSale->MultiSale_Flg = 'Y';
         if (iTmp == 3 || iTmp == 23)
            pCSale->SaleCode[0] = 'P';
         else if (lPrice > 1000 || iTmp == 1)
            pCSale->SaleCode[0] = 'F';
      } 
   }

   // Percent transfer
   if (pSaleRec->Pct_Flag[0] == 'P')
   {
      iTmp = atol(pSaleRec->Pct_Xfer);
      if (iTmp > 0)
      {
         if (iTmp == 1000)
            pCSale->SaleCode[0] = 'F';
         else
            pCSale->SaleCode[0] = 'P';
         iTmp = sprintf(acTmp, "%d", (int)(iTmp/10.0));
         memcpy(pCSale->PctXfer, acTmp, iTmp);
      }
   }
   memcpy(pCSale->Name1, pSaleRec->Owner, SSIZ_OWNER);
   memcpy(pCSale->MailAdr1, pSaleRec->Mail_Addr1, SSIZ_MAIL_ADDR1);
   memcpy(pCSale->MailAdr2, pSaleRec->Mail_Addr2, SSIZ_MAIL_ADDR2);

   pCSale->CRLF[0] = '\n';
   pCSale->CRLF[1] = 0;

   return 0;
}

/****************************** Fre_ExtrSale *********************************
 *
 * Extract history sale.  Sales_cd.txt contains full history of sales.  There is 
 * no need to concate with old file.
 * Sort output on Transfer date instead of recording date.
 *
 * Return 0 if successful, otherwise error
 *
 *****************************************************************************/

int Fre_ExtrSale(void)
{
   char     *pTmp, acCSalRec[1024], acSaleRec[1024], acTmp[256];
   char     acTmpSale[_MAX_PATH];

   long     lCnt=0;
   int		iRet, iTmp, iSaleCnt=0;

   LogMsg0("Extract sale file %s", acSaleFile);

   // Check current sale file
   if (_access(acSaleFile, 0))
   {
      LogMsg("***** Missing current sale file %s", acSaleFile);
      return -1;
   }

   // Remove bad char in sale file
   sprintf(acTmp, "%s\\%s\\Sale_file.fix", acTmpPath, myCounty.acCntyCode);
   iRet = replBadChars(acSaleFile, acTmp, iSaleLen);
   if (iRet > 0)
      strcpy(acSaleFile, acTmp);

   // Open current sale
   LogMsg("Open current sale file %s", acSaleFile);
   fdSale = fopen(acSaleFile, "r");
   if (fdSale == NULL)
   {
      LogMsg("***** Error opening current sale file: %s\n", acSaleFile);
      return -2;
   }

   // Create output file
   sprintf(acTmpSale, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "tmp");
   LogMsg("Create updated sale file %s", acTmpSale);
   fdCSale = fopen(acTmpSale, "w");
   if (fdCSale == NULL)
   {
      LogMsg("***** Error creating updated sale file: %s\n", acTmpSale);
      return -3;
   }

   // Merge loop
   while (!feof(fdSale))
   {
#ifdef _DEBUG
      //if (!memcmp(acSaleRec, "15837109", 8))
      //   iRet = 1;
#endif

      // Get current sale
      pTmp = fgets(acSaleRec, 1024, fdSale);
      if (!pTmp)
         break;
      
      iRet = Fre_FormatSale(acCSalRec, acSaleRec);
      if (!iRet)
      {
         fputs(acCSalRec, fdCSale);
         iSaleCnt++;
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);

   }

   // Close files
   if (fdCSale)
      fclose(fdCSale);
   if (fdSale)
      fclose(fdSale);

   // Sort/merge sale file - APN (asc), Saledate (asc), DocNum (asc), SalePrice (desc)
   //sprintf(acTmp, "S(1,%d,C,A,27,8,C,A,15,%d,C,A,57,10,C,D) DUPO(1,34)", iApnLen, SALE_SIZ_DOCNUM);
   sprintf(acTmp, "S(1,%d,C,A,432,8,C,A,57,10,C,D,15,12,C,A) DUPO(1,34)", iApnLen);
   iTmp = sortFile(acTmpSale, acCSalFile, acTmp);

   LogMsg("Total sale records processed:   %u", lCnt);
   LogMsg("Total sale records extracted:   %u", iSaleCnt);
   LogMsg("Total sale records after dedup: %u", iTmp);

   LogMsg("Update Sale History completed.");
   printf("\nTotal extracted sale records:  %u\n", iTmp);

   return 0;
}

/******************************* Fre_MakeDocLink *******************************
 *
 * Format DocLink
 * DocNum:  0042379
 * DocDate: 19990322
 *
 ******************************************************************************/

void Fre_MakeDocLink(LPSTR pDocLink, LPSTR pDoc, LPSTR pDate)
{
   char  acTmp[256], acDocName[256];
   long  lTmp;

   *pDocLink = 0;
   if (*pDoc > ' ' && *pDate > ' ')
   {
      lTmp = atol(pDoc);
      sprintf(pDoc, "%.7d", lTmp);

      sprintf(acDocName, "%.4s\\%.4s\\%.4s%s", pDate, pDoc, pDate, pDoc);
      sprintf(acTmp, "%s\\%s.pdf", acDocPath, acDocName);

      if (!_access(acTmp, 0))
         strcpy(pDocLink, acDocName);
   }
}

/***************************** Fre_ParseTaxBase ******************************
 *
 *
 *****************************************************************************/

int Fre_ParseTaxBase(char *pBase, char *pDelq, char *pInbuf)
{
   TAXBASE  *pBaseRec = (TAXBASE *)pBase;
   TAXDELQ  *pDelqRec = (TAXDELQ *)pDelq;
   FRE_TAX  *pInRec   = (FRE_TAX *)pInbuf;
   double   dTotalTax, dTotalDue, dTotalFee, dTotalPaid, dTax1, dTax2, dPen1, dPen2, 
            dDue1, dDue2, dPaid1, dPaid2, dFee1, dFee2;
   int      iTmp;

   // Clear output buffer
   memset(pBase, 0, sizeof(TAXBASE));
   memset(pDelqRec, 0, sizeof(TAXDELQ));

   // APN
   strcpy(pBaseRec->Apn, myTrim(pInRec->Apn, TSIZ_APN));

#ifdef _DEBUG
   //if (!memcmp(pBaseRec->Apn, "00102009", 8))
   //   iTmp = atoin(pInRec->TaxYear, 4);
#endif

   // BillNum
   sprintf(pBaseRec->BillNum, "%s-01", pBaseRec->Apn);

   // TRA

   // Tax Year
   iTmp = atoin(pInRec->TaxYear, 4);
   if (iTmp != lTaxYear)
      return 1;
   memcpy(pBaseRec->TaxYear, pInRec->TaxYear, 4);

   // Check for Tax amount
   dTax1 = atof(pInRec->sInst1.TaxAmt);
   dTax2 = atof(pInRec->sInst2.TaxAmt);
   dPen1 = atof(pInRec->sInst1.PenAmt);
   dPen2 = atof(pInRec->sInst2.PenAmt);
   dDue1 = atof(pInRec->sInst1.TotalDue);    // Total of tax + pen
   dDue2 = atof(pInRec->sInst2.TotalDue);
   dFee1 = atof(pInRec->sInst1.FeeAmt);
   dFee2 = atof(pInRec->sInst2.FeeAmt);
   dPaid1 = atof(pInRec->sInst1.PaidAmt);
   dPaid2 = atof(pInRec->sInst2.PaidAmt);

   dTotalTax = dTax1+dTax2;
   dTotalFee = dFee1+dFee2;
   dTotalPaid= dPaid1+dPaid2;
   dTotalDue = (dDue1+dDue2) - dTotalPaid;

   memcpy(pBaseRec->DueDate1, pInRec->sInst1.DueDate, TSIZ_DATE);
   memcpy(pBaseRec->DueDate2, pInRec->sInst2.DueDate, TSIZ_DATE);

   pBaseRec->Inst1Status[0] = TAX_BSTAT_UNPAID;
   pBaseRec->Inst2Status[0] = TAX_BSTAT_UNPAID;

   // Fee
   if (dTotalFee > 0.0)
   {
      sprintf(pBaseRec->TotalFees, "%.2f", dTotalFee);
   }

   // Penalty
   if (dPen1 > 0.0)
      sprintf(pBaseRec->PenAmt1, "%.2f", dPen1);
   if (dPen2 > 0.0)
      sprintf(pBaseRec->PenAmt2, "%.2f", dPen2);

   if (dTotalTax > 0.0)
   {
      sprintf(pBaseRec->TotalTaxAmt, "%.2f", dTotalTax);
      if (dTax1 > 0.0)
         sprintf(pBaseRec->TaxAmt1, "%.2f", dTax1);
      if (dTax2 > 0.0)
         sprintf(pBaseRec->TaxAmt2, "%.2f", dTax2);
   }

   // Paid
   if (dPaid1 > 0.0)
   {
      sprintf(pBaseRec->PaidAmt1, "%.2f", dPaid1);
      memcpy(pBaseRec->PaidDate1, pInRec->sInst1.PaidDate, TSIZ_DATE);
      pBaseRec->Inst1Status[0] = TAX_BSTAT_PAID;
   }
   if (dPaid2 > 0.0)
   {
      sprintf(pBaseRec->PaidAmt2, "%.2f", dPaid2);
      memcpy(pBaseRec->PaidDate2, pInRec->sInst2.PaidDate, TSIZ_DATE);
      pBaseRec->Inst2Status[0] = TAX_BSTAT_PAID;
   }

   // Total due
   if (dTotalDue > 0)
   {
      sprintf(pBaseRec->TotalDue, "%.2f", dTotalDue);
   }

   // Tax rate

   // Default
   dTotalTax = atof(pInRec->DelqAmt);
   if (pInRec->DefaultDate[0] > '0' && dTotalTax > 0.0)
   {
      memcpy(pBaseRec->DelqYear, pInRec->DefaultDate, 4);
      strcpy(pDelqRec->Apn, pBaseRec->Apn);

      int iDelqYr = atoin(pInRec->DefaultDate, 4);
      sprintf(pDelqRec->TaxYear, "%d", iDelqYr-1);

      sprintf(pDelqRec->Def_Amt, "%.2f", dTotalTax);
      memcpy(pDelqRec->Def_Date, pInRec->DefaultDate, TSIZ_DATE);
      sprintf(pBaseRec->Upd_Date, "%d", lLastTaxFileDate);

      pDelqRec->DelqStatus[0] = TAX_STAT_UNPAID;   // County only provide Default Date & Amt, no indication of on payment plan
      pDelqRec->isDelq[0] = '1';
      pBaseRec->isDelq[0] = '1';
   } else
      pBaseRec->isDelq[0] = '0';

   pBaseRec->BillType[0] = BILLTYPE_SECURED;
   pBaseRec->isSecd[0] = '1';
   pBaseRec->isSupp[0] = '0';

   return 0;
}

/**************************** Fre_Load_TaxBase *******************************
 *
 * Create base import file and import into SQL if specified
 *
 * Return 0 if success.
 *
 *****************************************************************************/

int Fre_Load_TaxBase(bool bImport)
{
   char     *pTmp, acBaseRec[MAX_RECSIZE], acDelqRec[512], acRec[512];
   char     acBaseFile[_MAX_PATH], acDelqFile[_MAX_PATH], acInFile[_MAX_PATH];

   int      iRet;
   long     lBase=0, lCnt=0, lDelq=0;
   FILE     *fdBase, *fdDelq, *fdIn, *fdR01;
   TAXBASE  *pTaxBase = (TAXBASE *)&acBaseRec[0];

   LogMsg0("Loading Tax file");

   GetIniString(myCounty.acCntyCode, "TaxBase", "", acInFile, _MAX_PATH, acIniFile);
   lLastTaxFileDate = getFileDate(acInFile);
   // Only process if new tax file
   iRet = isNewTaxFile(NULL, myCounty.acCntyCode);
   if (iRet <= 0)
   {
      lLastTaxFileDate = 0;
      return iRet;
   }

   NameTaxCsvFile(acDelqFile, myCounty.acCntyCode, "Delq");
   NameTaxCsvFile(acBaseFile, myCounty.acCntyCode, "Base");

   // Open input file
   LogMsg("Open Tax file %s", acInFile);
   fdIn = fopen(acInFile, "r");
   if (fdIn == NULL)
   {
      LogMsg("***** Error opening Tax file: %s\n", acInFile);
      return -2;
   }  

   // Open Delq file
   LogMsg("Open Delq file %s", acDelqFile);
   fdDelq = fopen(acDelqFile, "w");
   if (fdDelq == NULL)
   {
      LogMsg("***** Error creating Delq file: %s\n", acDelqFile);
      return -4;
   }

   // Open base file
   LogMsg("Open Base file %s", acBaseFile);
   fdBase = fopen(acBaseFile, "w");
   if (fdBase == NULL)
   {
      LogMsg("***** Error creating base file: %s\n", acBaseFile);
      return -4;
   }

   // Open raw file
   fdR01 = OpenR01(myCounty.acCntyCode);

   // Merge loop 
   while (!feof(fdIn))
   {
      pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdIn);
      if (!pTmp)
         break;

      // Create new tax base record
      iRet = Fre_ParseTaxBase(acBaseRec, acDelqRec, acRec);
      if (!iRet)
      {
#ifdef _DEBUG
         //if (!memcmp(pTaxBase, "08302056S", 9))
         //   iRet = 0;
#endif

         // Update TRA
         if (fdR01)
            UpdateTRA(acBaseRec, fdR01);

         // Create TaxBase record
         Tax_CreateTaxBaseCsv(acRec, (TAXBASE *)&acBaseRec);
         lBase++;
         fputs(acRec, fdBase);

         // Create Delq record
         if (acDelqRec[0] > ' ')
         {
            Tax_CreateDelqCsv(acRec, (TAXDELQ *)&acDelqRec);
            fputs(acRec, fdDelq);
            lDelq++;
         }
      } else
      {
         LogMsg0("---> Drop prior year Base record %d [%.80s]", lCnt, acRec); 
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdIn)
      fclose(fdIn);
   if (fdBase)
      fclose(fdBase);
   if (fdDelq)
      fclose(fdDelq);
   if (fdR01)
      fclose(fdR01);

   LogMsg("Total records processed:    %u", lCnt);
   LogMsg("Total Base records:         %u", lBase);
   LogMsg("Total Delq records:         %u", lDelq);
   LogMsg("Total TRA updated:          %u", iApnMatch);

   // Import into SQL
   if (bImport)
   {
      iRet = doTaxImport(myCounty.acCntyCode, TAX_BASE);
      if (!iRet)
         iRet = doTaxImport(myCounty.acCntyCode, TAX_DELINQUENT);
   } else
      iRet = 0;

   return iRet;
}

/***************************** Fre_ParseTaxDetail ****************************
 *
 *
 *****************************************************************************/

int Fre_ParseTaxDetail(char *pItems, char *pAgency, char *pInbuf)
{
   TAXDETAIL  *pItemsRec  = (TAXDETAIL *)pItems;
   TAXAGENCY  *pAgencyRec = (TAXAGENCY *)pAgency;
   TAXAGENCY  *pResult;

   FRE_DETAIL *pInRec   = (FRE_DETAIL *)pInbuf;
   double      dTmp;

   // Clear output buffer
   memset(pItems, 0, sizeof(TAXDETAIL));
   memset(pAgency, 0, sizeof(TAXAGENCY));

   // APN
   strcpy(pItemsRec->Apn, myTrim(pInRec->Apn, TSIZ_APN));

   // BillNum
   sprintf(pItemsRec->BillNum, "%s-01", pItemsRec->Apn);

   // Tax Year
   sprintf(pItemsRec->TaxYear, "%d", lTaxYear);

   // Tax Desc
   strcpy(pItemsRec->TaxDesc, myTrim(pInRec->Agency, TSIZ_AGENCY));
   strcpy(pAgencyRec->Agency, pItemsRec->TaxDesc);

   // Tax Code
   memcpy(pItemsRec->TaxCode, pInRec->TaxCode, TSIZ_TAXCODE);
   memcpy(pAgencyRec->Code, pInRec->TaxCode, TSIZ_TAXCODE);
   if (!memcmp(pInRec->TaxCode, "0001", 4))
   {
      pItemsRec->TaxCode[4] = pInRec->Agency[0];
      pAgencyRec->Code[4]   = pInRec->Agency[0];
      pAgencyRec->TC_Flag[0] = '1';
      pItemsRec->TC_Flag[0] = '1';
   }

   // Tax Rate
   if (pInRec->TaxRate[1] > ' ')
   {
      dTmp = atof(pInRec->TaxRate);
      sprintf(pItemsRec->TaxRate, "%.6f", dTmp);
      strcpy(pAgencyRec->TaxRate, pItemsRec->TaxRate);

      pResult = findTaxAgency(pItemsRec->TaxCode);
      if (pResult)
      {
         double dTmp1 = atof(pResult->TaxRate);
         if (dTmp1 != dTmp)
            strcpy(pResult->TaxRate, pAgencyRec->TaxRate);
      }
   } else
   {
      strcpy(pItemsRec->TaxRate, "0.0");
      strcpy(pAgencyRec->TaxRate, "0.0");
   }

   // Tax Amount
   dTmp = atof(pInRec->TaxAmt);
   sprintf(pItemsRec->TaxAmt, "%.2f", dTmp);

   return 0;
}

/**************************** Fre_Load_TaxDetail ******************************
 *
 * Create base import file and import into SQL if specified
 *
 * Return 0 if success.
 *
 *****************************************************************************/

int Fre_Load_TaxDetail(bool bImport)
{
   char     *pTmp, acItemsRec[MAX_RECSIZE], acAgencyRec[512], acRec[512];
   char     acItemsFile[_MAX_PATH], acAgencyFile[_MAX_PATH], acTmpFile[_MAX_PATH], acInFile[_MAX_PATH];

   int      iRet;
   long     lItems=0, lCnt=0, lAgency=0;
   FILE     *fdItems, *fdAgency, *fdIn;
   TAXDETAIL *pTax = (TAXDETAIL *)&acItemsRec[0];

   LogMsg0("Loading Detail file");

   GetIniString(myCounty.acCntyCode, "TaxDetail", "", acInFile, _MAX_PATH, acIniFile);
   NameTaxCsvFile(acItemsFile, myCounty.acCntyCode, "Items");
   NameTaxCsvFile(acAgencyFile, myCounty.acCntyCode, "Agency");
   sprintf(acTmpFile, "%s\\%s\\%s_Agency.tmp", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);

   // Open input file
   LogMsg("Open Detail file %s", acInFile);
   fdIn = fopen(acInFile, "r");
   if (fdIn == NULL)
   {
      LogMsg("***** Error opening Detail file: %s\n", acInFile);
      return -2;
   }  

   // Open Items file
   LogMsg("Create Items file %s", acItemsFile);
   fdItems = fopen(acItemsFile, "w");
   if (fdItems == NULL)
   {
      LogMsg("***** Error creating Items file: %s\n", acItemsFile);
      return -4;
   }

   // Open Agency file
   LogMsg("Create Agency file %s", acTmpFile);
   fdAgency = fopen(acTmpFile, "w");
   if (fdAgency == NULL)
   {
      LogMsg("***** Error creating Agency file: %s\n", acTmpFile);
      return -4;
   }

   // Init variables

   // Merge loop 
   while (!feof(fdIn))
   {
      pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdIn);
      if (!pTmp)
         break;

      // Create Items & Agency record
      iRet = Fre_ParseTaxDetail(acItemsRec, acAgencyRec, acRec);
      if (!iRet)
      {
         // Create TaxBase record
         Tax_CreateDetailCsv(acRec, (TAXDETAIL *)&acItemsRec);
         lItems++;
         fputs(acRec, fdItems);

         // Create Agency record
         Tax_CreateAgencyCsv(acRec, (TAXAGENCY *)&acAgencyRec);
         fputs(acRec, fdAgency);
      } else
      {
         LogMsg0("---> Drop Detail record %d [%.80s]", lCnt, acRec); 
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdIn)
      fclose(fdIn);
   if (fdItems)
      fclose(fdItems);
   if (fdAgency)
      fclose(fdAgency);

   LogMsg("Total records processed:    %u", lCnt);
   LogMsg("Total Items records:        %u", lItems);

   // Dedup Agency file before import
   LogMsg("Dedup Agency file %s to %s", acTmpFile, acAgencyFile);
   iRet = sortFile(acTmpFile, acAgencyFile, "S(#1,C,A) DUPOUT F(TXT)");
   if (iRet > 0)
      iRet = doTaxImport(myCounty.acCntyCode, TAX_AGENCY);

   // Import into SQL
   if (bImport)
   {
      iRet = doTaxImport(myCounty.acCntyCode, TAX_DETAIL);
      if (!iRet)
      {
         // Populate TotalRate - EXEC spUpdateTotalRate 'FRE'
         iRet = doUpdateTotalRate(myCounty.acCntyCode);
      }
   } else
      iRet = 0;

   return iRet;
}

/************************** Fre_ParseTaxDetailSupp ***************************
 *
 * Supplemental detail record doesn't contain Agency name.
 *
 *****************************************************************************/

int Fre_ParseTaxDetailSupp(char *pItems, char *pInbuf)
{
   TAXDETAIL  *pItemsRec  = (TAXDETAIL *)pItems;
   TAXAGENCY  *pResult;
   FRE_SUPDET *pInRec   = (FRE_SUPDET *)pInbuf;
   double      dTmp;
   int         iTmp;

   // Clear output buffer
   memset(pItems, 0, sizeof(TAXDETAIL));

   // Tax Year
   sprintf(pItemsRec->TaxYear, "20%.2s", pInRec->Rate_Yr);
   iTmp = atol(pItemsRec->TaxYear);
   if (iTmp < (lLienYear-1))
      return -1;

   // APN
   memcpy(pItemsRec->Apn, pInRec->Apn, FRE_SUP_APN);
   myTrim(pItemsRec->Apn);

   // BillNum
   sprintf(pItemsRec->BillNum, "%s-%.2s", pItemsRec->Apn, pInRec->ApnSfx);

   // Tax Amount
   dTmp = atof(pInRec->TaxAmt);
   sprintf(pItemsRec->TaxAmt, "%.2f", dTmp);

   // Tax Code
   memcpy(pItemsRec->TaxCode, pInRec->TaxCode, TSIZ_TAXCODE);
   if (!memcmp(pInRec->TaxCode, "0001", 4))
   {
      if (dTmp < 0)
         pItemsRec->TaxCode[4] = 'L';
      else
         pItemsRec->TaxCode[4] = 'F';

      pItemsRec->TaxRate[0] = '1';
      pItemsRec->TC_Flag[0] = '1';
   } else
   {
      // Tax Desc
      pResult = findTaxAgency(pItemsRec->TaxCode);
      if (pResult)
      {
         pItemsRec->TC_Flag[0] = pResult->TC_Flag[0];
         if (pItemsRec->TaxRate[1] < '0')
            strcpy(pItemsRec->TaxRate, pResult->TaxRate);
      } else
      {
         LogMsg("+++ Unknown supp tax code: %s [%s]", pItemsRec->TaxCode, pItemsRec->BillNum);
      }
   }

   return 0;
}

/************************** Fre_Load_TaxDetail_Supp **************************
 *
 * Load SupplementalRevDistrict.txt into FRE_Tax_Items table
 *
 * Return 0 if success.
 *
 *****************************************************************************/

int Fre_Load_TaxDetail_Supp(bool bImport)
{
   char     *pTmp, acItemsRec[MAX_RECSIZE], acRec[512];
   char     acItemsFile[_MAX_PATH], acInFile[_MAX_PATH];

   int      iRet;
   long     lItems=0, lCnt=0, lAgency=0;
   FILE     *fdItems, *fdIn;
   FRE_SUPDET *pTax = (FRE_SUPDET *)&acItemsRec[0];

   LogMsg0("Loading Detail Supplemental file");

   GetIniString(myCounty.acCntyCode, "SupDetail", "", acInFile, _MAX_PATH, acIniFile);
   sprintf(acItemsFile, sTaxOutTmpl, myCounty.acCntyCode, "Items");

   // Open input file
   LogMsg("Open Detail file %s", acInFile);
   fdIn = fopen(acInFile, "r");
   if (fdIn == NULL)
   {
      LogMsg("***** Error opening Detail file: %s\n", acInFile);
      return -2;
   }  

   // Open Items file
   LogMsg("Create Items file %s", acItemsFile);
   fdItems = fopen(acItemsFile, "a");
   if (fdItems == NULL)
   {
      LogMsg("***** Error creating Items file: %s\n", acItemsFile);
      return -4;
   }

   // Merge loop 
   while (!feof(fdIn))
   {
      pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdIn);
      if (!pTmp)
         break;

      // Create Items record
      iRet = Fre_ParseTaxDetailSupp(acItemsRec, acRec);
      if (!iRet)
      {
         // Create TaxBase record
         Tax_CreateDetailCsv(acRec, (TAXDETAIL *)&acItemsRec);
         lItems++;
         fputs(acRec, fdItems);
      } else if (bDebug)
      {
         LogMsg0("---> Drop supp detail record %d: %.35s", lCnt, acRec); 
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdIn)
      fclose(fdIn);
   if (fdItems)
      fclose(fdItems);

   LogMsg("Total records processed:    %u", lCnt);
   LogMsg("Total Items records:        %u", lItems);

   // Import into SQL
   if (bImport)
   {
      iRet = doTaxImport(myCounty.acCntyCode, TAX_DETAIL);
      if (!iRet)
      {
         // Populate TotalRate - EXEC spUpdateTotalRate 'FRE'
         iRet = doUpdateTotalRate(myCounty.acCntyCode, "B");
      }
   } else
      iRet = 0;

   return iRet;
}

/***************************** Fre_ParseTaxSupBase ***************************
 *
 *
 *****************************************************************************/

int Fre_ParseTaxSupBase(char *pBase, char *pDelq, char *pInbuf)
{
   TAXBASE     *pBaseRec = (TAXBASE *)pBase;
   TAXDELQ     *pDelqRec = (TAXDELQ *)pDelq;
   FRE_SUPBASE *pInRec   = (FRE_SUPBASE *)pInbuf;
   double      dTmp;

   // Clear output buffer
   memset(pBase, 0, sizeof(TAXBASE));
   memset(pDelqRec, 0, sizeof(TAXDELQ));

   // APN
   memcpy(pBaseRec->Apn, pInRec->Apn, FRE_BASE_APN);
   if (pBaseRec->Apn[8] == ' ')
      pBaseRec->Apn[8] = 0;

#ifdef _DEBUG
   //int      iRet;
   //if (!memcmp(pBaseRec->Apn, "00405003", 8))
   //   iRet = 0;
#endif

   // BillNum
   sprintf(pBaseRec->BillNum, "%s-%.2s", pBaseRec->Apn, pInRec->ApnSfx);
   
   // TRA
   memcpy(pBaseRec->TRA, pInRec->Tra, FRE_BASE_TRA);

   // Tax Year
   sprintf(pBaseRec->TaxYear, "20%.2s", pInRec->Roll_Yr);

   // Tax rate
   dTmp = atof(pInRec->TaxRate);
   sprintf(pBaseRec->TotalRate, "%.6f", dTmp);

   pBaseRec->BillType[0] = BILLTYPE_SECURED_SUPPL;
   pBaseRec->isSecd[0] = '0';
   pBaseRec->isSupp[0] = '1';
   pBaseRec->isDelq[0] = '0';

   return 0;
}

/***************************** Fre_ParseTaxSupInst ***************************
 *
 *
 *****************************************************************************/

int Fre_ParseTaxSupInst(char *pBase, char *pInbuf)
{
   TAXBASE     *pBaseRec = (TAXBASE *)pBase;
   FRE_SUPINST *pInRec   = (FRE_SUPINST *)pInbuf;
   double      dTmp, dTaxAmt1, dTaxAmt2, dPenAmt1, dPenAmt2, dPaidAmt1, dPaidAmt2,
               dTotalTax, dTotalDue;

   // APN
   if (memcmp(pBaseRec->Apn, pInRec->Apn, strlen(pBaseRec->Apn)))
      return -1;

#ifdef _DEBUG
   //int      iRet;
   //if (!memcmp(pBaseRec->Apn, "00405003", 8))
   //   iRet = 0;
#endif

   // Tax Amt
   dTaxAmt1  = atof(pInRec->Inst1.TaxAmt);
   dTaxAmt2  = atof(pInRec->Inst2.TaxAmt);
   dPaidAmt1 = atof(pInRec->Inst1.PaidAmt);
   dPaidAmt2 = atof(pInRec->Inst2.PaidAmt);
   dPenAmt1  = atof(pInRec->Inst1.PenAmt);
   dPenAmt2  = atof(pInRec->Inst2.PenAmt);

   sprintf(pBaseRec->TaxAmt1, "%.2f", dTaxAmt1+dPenAmt1);
   sprintf(pBaseRec->TaxAmt2, "%.2f", dTaxAmt2+dPenAmt2);

   // Total tax amt
   dTotalTax = dTaxAmt1 + dTaxAmt2;
   sprintf(pBaseRec->TotalTaxAmt, "%.2f", dTotalTax);

   // Total due
   dTmp = dTotalTax+dPenAmt1+dPenAmt2;
   dTotalDue = dTmp - (dPaidAmt1+dPaidAmt2);
   sprintf(pBaseRec->TotalDue, "%.2f", dTotalDue);

   sprintf(pBaseRec->PenAmt1, "%.2f", dPenAmt1);
   sprintf(pBaseRec->PenAmt2, "%.2f", dPenAmt2);

   // Pen amt
   if (dPenAmt1 > 0)
      sprintf(pBaseRec->PenAmt1, "%.2f", dPenAmt1);
   if (dPenAmt2 > 0)
      sprintf(pBaseRec->PenAmt2, "%.2f", dPenAmt2);

   // Due Date
   memcpy(pBaseRec->DueDate1, pInRec->Inst1.DueDate, TSIZ_DATE);
   memcpy(pBaseRec->DueDate2, pInRec->Inst2.DueDate, TSIZ_DATE);
   pBaseRec->Inst1Status[0] = TAX_BSTAT_UNPAID;
   pBaseRec->Inst2Status[0] = TAX_BSTAT_UNPAID;

   // Paid date
   if (dPaidAmt1 > 0)
   {
      memcpy(pBaseRec->PaidDate1, pInRec->Inst1.PaidDate, TSIZ_DATE);
      sprintf(pBaseRec->PaidAmt1, "%.2f", dPaidAmt1);
      pBaseRec->Inst1Status[0] = TAX_BSTAT_PAID;
   }
   if (dPaidAmt2 > 0)
   {
      memcpy(pBaseRec->PaidDate2, pInRec->Inst2.PaidDate, TSIZ_DATE);
      sprintf(pBaseRec->PaidAmt2, "%.2f", dPaidAmt2);
      pBaseRec->Inst2Status[0] = TAX_BSTAT_PAID;
   }

   return 0;
}

/************************** Fre_Load_TaxBase_Supp ****************************
 *
 *
 *****************************************************************************/

int Fre_Load_TaxBase_Supp(bool bImport)
{
   char     *pTmp, acBaseRec[MAX_RECSIZE], acDelqRec[512], acRec[512];
   char     acBaseFile[_MAX_PATH], acSupInstFile[_MAX_PATH], acInFile[_MAX_PATH];

   int      iRet;
   long     lBase=0, lCnt=0;
   FILE     *fdBase, *fdIn, *fdInst;
   TAXBASE  *pTaxBase = (TAXBASE *)&acBaseRec[0];

   LogMsg0("Loading Tax Supplemental file");

   GetIniString(myCounty.acCntyCode, "SupBase", "", acInFile, _MAX_PATH, acIniFile);
   GetIniString(myCounty.acCntyCode, "SupInst", "", acSupInstFile, _MAX_PATH, acIniFile);

   NameTaxCsvFile(acBaseFile, myCounty.acCntyCode, "Base");

   // Open input file
   LogMsg("Open Tax file %s", acInFile);
   fdIn = fopen(acInFile, "r");
   if (fdIn == NULL)
   {
      LogMsg("***** Error opening Tax file: %s\n", acInFile);
      return -2;
   }  

   // Open supplemental installation file
   LogMsg("Open Supplemental Installation file %s", acSupInstFile);
   fdInst = fopen(acSupInstFile, "r");
   if (fdInst == NULL)
   {
      LogMsg("***** Error opening Supplemental Installation file: %s\n", acSupInstFile);
      return -2;
   }  

   // Open base file
   LogMsg("Open Base file %s", acBaseFile);
   fdBase = fopen(acBaseFile, "a");
   if (fdBase == NULL)
   {
      LogMsg("***** Error creating base file: %s\n", acBaseFile);
      return -4;
   }
   lLastTaxFileDate = getFileDate(acSupInstFile);

   // Merge loop 
   while (!feof(fdIn))
   {
      pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdIn);
      if (!pTmp)
         break;

      // Create new R01 record
      iRet = Fre_ParseTaxSupBase(acBaseRec, acDelqRec, acRec);
      if (!iRet)
      {
         // Update tax amount
         if (pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdInst))
            iRet = Fre_ParseTaxSupInst(acBaseRec, acRec);

#ifdef _DEBUG
         //if (!memcmp(pTaxBase, "00104006", 8))
         //   iRet = 0;
#endif
         iRet = atol(pTaxBase->TaxYear);
         if (iRet >= (lLienYear-1) || pTaxBase->TotalDue[0] > '0')
         {
            // Create TaxBase record
            Tax_CreateTaxBaseCsv(acRec, (TAXBASE *)&acBaseRec);
            lBase++;
            fputs(acRec, fdBase);
         }
      } else if (bDebug)
      {
         LogMsg0("---> Drop Supp Base record %d [%.35s]", lCnt, acRec); 
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdIn)
      fclose(fdIn);
   if (fdBase)
      fclose(fdBase);
   if (fdInst)
      fclose(fdInst);

   LogMsg("Total records processed:    %u", lCnt);
   LogMsg("Total Base records:         %u", lBase);

   // Import into SQL
   if (bImport)
   {
      iRet = doTaxImport(myCounty.acCntyCode, TAX_BASE);
      if (!iRet)
         iRet = doTaxImport(myCounty.acCntyCode, TAX_DELINQUENT);
   } else
      iRet = 0;

   return iRet;
}

/*********************************** loadFre ********************************
 *
 * Run LoadOne with following options:
 *    -L  : to load lien
 *    -U  : to load update roll
 *    -Lg : to process GrGr and merge to R01 file
 *    -Mg : to merge GrGr to R01 file
 *    -Xsi: to extract sale file and create sql import
 *
 * Normal Update: -CFRE -U -E -Lg -Xs[i] -O
 * LDR process:   -CFRE -L -Xl -Mg -O -Ms
 *
 ****************************************************************************/

int loadFre(int iSkip)
{
   int   iRet=0;
   char  acTmp[_MAX_PATH];

   // Preset APN length
   iApnLen = myCounty.iApnLen;

   // Reformat DocNum in cum sale file
   //iRet = FixCumSale(acCSalFile, SALE_FLD_DOCNUM+8);

   //sprintf(acTmp, acGrGrTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "Sls");
   //iRet = FixCumGrGr(acTmp, SALE_FLD_DOCNUM+CNTY_FRE);

   // Fix GrGr file - one time
   //sprintf(acTmp, acGrGrTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "Sls");
   //iRet = FixGrGrDef(acTmp, 0, false);

   // Load tax
   if (iLoadTax == TAX_LOADING)                    // -T or -Lt
   {
      // Load Tax Agency table
      iRet = GetIniString(myCounty.acCntyCode, "TaxDist", "", acTmp, _MAX_PATH, acIniFile);
      if (iRet > 0)
         iRet = LoadTaxCodeTable(acTmp);

      // Load Tax Base and Delq
      iRet = Fre_Load_TaxBase(false);
      if (!iRet && lLastTaxFileDate > 0)
      {
         // Load Items and Agency
         iRet = Fre_Load_TaxDetail(false);

         // Load tax supplement
         iRet = Fre_Load_TaxBase_Supp(bTaxImport);

         // Load detail supplement
         iRet = Fre_Load_TaxDetail_Supp(bTaxImport);

         iRet = updateDelqFlag(myCounty.acCntyCode);
      }

      // Create empty Tax_Owner table
      //doTaxPrep(myCounty.acCntyCode, TAX_OWNER);
   }

   if (!iLoadFlag)
      return iRet;

   // Extract prop8 flag to text file
   if (lOptProp8 & MYOPT_EXT)                      // -X8 
   {
      GetIniString(myCounty.acCntyCode, "Prop8File", "", acTmp, _MAX_PATH, acIniFile);
      iRet = Fre_ExtrProp8(acTmp);
   }

   // Extract CHAR file
   if (iLoadFlag & EXTR_ATTR)                      // -Xa
      iRet = Fre_ConvStdChar(acCharFile);

   // Extract Lien
   if (iLoadFlag & EXTR_LIEN)                      // -Xl
      iRet = Fre_ExtrLien();

   // Load GrGr
   if (iLoadFlag & LOAD_GRGR)                      // -Lg
   {
      GetIniString(myCounty.acCntyCode, "GrGrIn", "", acTmp, _MAX_PATH, acIniFile);
      if (strstr(acTmp, ".xlsx"))
         iRet = Fre_LoadGrGrXls(myCounty.acCntyCode);
      else if (strstr(acTmp, ".csv"))
         iRet = Fre_LoadGrGrCsv(myCounty.acCntyCode);
      else if (strstr(acTmp, ".flt"))
		   iRet = Fre_LoadGrGr(myCounty.acCntyCode);
      else
      {
         LogMsg("*** Invalid setting GrGrIn for %.3s.  Please verify INI file.", myCounty.acCntyCode);
         iRet = 1;
      }
      if (!iRet)
         iLoadFlag |= MERG_GRGR;
   }

   // Extract sales 
   if (iLoadFlag & EXTR_SALE)                      // -Xs
   {
      iRet = Fre_ExtrSale();
      if (!iRet)
         iLoadFlag |= MERG_CSAL;
   }

   // Extract Attom sales 
   if (iLoadFlag & EXTR_NRSAL)                     // -Xn
   {
      iRet = GetIniString(myCounty.acCntyCode, "NdcSale", "", acSaleFile, _MAX_PATH, acIniFile);
      if (!_access(acSaleFile, 0))
      {
         iRet = NR_CreateSCSale(myCounty.acCntyCode, acSaleFile, iApnLen);
         if (iRet)
            return iRet;
      } else
      {
         LogMsg("*** Sale file not available.  Please verify: %s", acSaleFile);
         return -1;
      }
   }

   // Load roll
   if (iLoadFlag & (LOAD_LIEN|LOAD_UPDT))          // -L or -U
   {
      // Convert situs file to ascii - Sort on APN, Prime flag
      GetIniString(myCounty.acCntyCode, "SitusFile", "", acSitusFile, _MAX_PATH, acIniFile);

      if (iLoadFlag & LOAD_LIEN)                   // -L
      {
         // Create Lien file
         LogMsg0("Load %s Lien file", myCounty.acCntyCode);
         iRet = Fre_Load_LDR(iSkip);
      } else if (iLoadFlag & LOAD_UPDT)            // -U
      {
         LogMsg0("Load %s roll update file", myCounty.acCntyCode);
         iRet = Fre_Load_Roll(iSkip);
      }
   }

   // Apply cum sale file to R01
   if (!iRet && (iLoadFlag & MERG_CSAL) )          // -Ms
   {
      // Apply Fre_Sale.sls to R01 file
      sprintf(acCSalFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "sls");
      iRet = ApplyCumSale(iSkip, acCSalFile, false, SALE_USE_SCUPDXFR, CLEAR_OLD_SALE);
   }

   if (!iRet && (iLoadFlag & UPDT_XSAL))           // -Mn
   {
      // Apply Lax_Ash.sls to R01 file
      GetIniString("Data", "ASH_File", acESalTmpl, acESalTmpl, _MAX_PATH, acIniFile);
      sprintf(acCSalFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "sls");
      iRet = ApplyCumSaleNR(iSkip, acCSalFile, SALE_USE_SCUPDXFR);
      if (!iRet)
         iLoadFlag |= LOAD_UPDT;
   }

   if (!iRet && (iLoadFlag & MERG_GRGR))           // -Mg
   {
      // Load special table for GrGr
      GetIniString(myCounty.acCntyCode, "GrGr_Xref", "", acTmp, _MAX_PATH, acIniFile);
      iNumDeeds = LoadXrefTable(acTmp, (XREFTBL *)&asDeed[0], MAX_DEED_ENTRIES);

      iRet = MergeGrGrDef(myCounty.acCntyCode, false, 0);

      if (!iRet && !(iLoadFlag & LOAD_LIEN))
         iLoadFlag |= LOAD_UPDT;                   // Trigger build index
   }

   // Format DocLinks - Doclinks are concatenate fields separated by comma 
   if (!iRet && (iLoadFlag & (LOAD_UPDT|LOAD_LIEN|MERG_CSAL)) )
      iRet = updateDocLinks(Fre_MakeDocLink, myCounty.acCntyCode, iSkip, 1);

   if (!iRet && bMergeOthers)
   {
      // Merge other values
      sprintf(acTmp, acLienTmpl, myCounty.acCntyCode, myCounty.acCntyCode);
      if (!_access(acTmp, 0))
         iRet = PQ_MergeOthers(myCounty.acCntyCode, acTmp, 0, iSkip);
      else
         LogMsg("***** Lien extract is missing: %s.  Please locate the file and retry", acTmp);
   }

   return iRet;
}