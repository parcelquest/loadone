/******************************************************************************
 *
 * Input files:
 *     ASTB271      :    HIST(ETAL)  90   EBC
 *     ASUB100      :    UN-SEC      440  EBC
 *     ASTB100      :    SEC ROLL    705  EBC
 *
 * Operation commands:
 *    - Normal update:  LoadOne -CMEN -U -Xsi -T
 *    - Load Lien:      LoadOne -CMEN -L
 *
 * Revision:
 * 08/29/2006 TCB
 * 09/29/2006 SPN       Bug fixes and adding option to process county data for assessor.
 * 10/08/2006 1.2.39    First production test.
 * 10/17/2006 1.3.3     Modify Men_MergeSalerec(), Men_Create_AHist(), and Men_Create_ASec()
 *                      to format DeedNo with leading 0. This requires for DocImage link.
 * 12/12/2006 1.3.8     Modify input layout for roll file (ASTB100) to add situs dir,
 *                      strsub, and Unit#.  Drop legal block & lot fields
 * 01/15/2008 1.5.3     Modify Men_CreatePQ4Rec to support standard Usecode.
 * 02/15/2008 1.5.5     Keep original Usecode as is.  Use last two digits of pri-use
 *                      for translation to std code.  This county won't create update
 *                      file.  So we need to import the whole file into SQL server.
 * 02/18/2008 1.5.5.1   Fix bug that format DocNum with null character in output file
 *                      when DocNum is single digit.
 * 03/03/2008 1.5.5.2   Modify Men_ExtrLien() to fix alignment bug.  Now we temporary
 *                      copy ExeCode to exCode until the structure is redesigned.
 * 03/11/2008 1.5.6     Set flag bCreateUpd=true to trigger update file creation
 * 03/24/2008 1.5.7.2   Format IMAPLINK.
 * 04/08/2008 1.5.10    Reset SaleAmt in Men_MergeSaleRec() if sale price is 0.
 * 06/04/2008 1.6.2     Fix OtherValue in Men_CreateLienRec(). The bug is that ME_Val is a 
 *                      duplicate of PP_Val.
 * 07/18/2008 8.0.3     Modify to support 2008 LDR.  Rename Men_CreateRoll() to Men_Load_LDR().
 *                      Add sale data when load LDR.  Loading active parcels only.
 * 11/07/2008 8.4.2     Modify layout to add Zoning code.  DBA field has been overlayed
 *                      with UNSEC_PP and ZONING.
 * 12/11/2008 8.5.1     Make StrNum left justified
 * 05/10/2009 8.8       Set full exemption flag
 * 06/09/2009 8.8.3     Cleanup garbage in Usecode.  Take Use_Code_1 only.
 * 07/01/2009 9.1.0     Modify Men_CreatePQ4Rec() and Men_CreateLienRec() to populate other values.
 * 01/22/2009 9.3.8.3   Add function Men_ExtrProp8() to extract Prop8.
 * 02/23/2010 9.4.3     Fix CARE_OF issue.
 * 07/12/2011 11.0.2    Rename Men_MergePQ4() to Men_Load_Roll(). Add S_HSENO.
 * 11/04/2011 11.4.15   Add Men_ExtrSale() & Men_FormatSale() to build history file.
 * 11/08/2011 11.4.15.1 Fix bug in loadMen().
 * 12/29/2011 11.6.1    Adding Men_Load_Tax*() to load tax files intp SQL.
 * 01/02/2012 11.6.1.1  Fix bug in Men_MergeSale() to ignore sales that have future date.
 * 02/15/2011 11.6.6    Modify Men_Load_TaxBase() and Men_Load_TaxDelq() to support EBCDIC
 *                      tax files tctbflat, tcabflat.
 * 03/12/2012 11.6.10   Add Men_Load_TaxSupp() to support supplemental file supp.txt
 * 03/16/2012 11.8.10.1 Modify Men_CreatePQ4Rec() to handle special case for USECODE.
 * 09/25/2013 13.8.4    Fix CareOf in Men_MergeOwner().
 * 10/08/2013 13.10.4   Use updateVesting() to update Vesting and Etal flag.
 * 10/16/2015 15.3.1    Modify Men_Load_TaxBase() & Men_ParseTaxBase() to create Owner info
 *                      file for import into Tax_Owner table.
 * 03/07/2016 15.7.0    Rename output file "Detail.csv" to "Items.csv" in Load_TaxBase().
 *                      Check for suplemental file before processing.
 * 03/26/2016 15.8.1    Move dispError() to Logs.cpp
 * 08/24/2016 16.2.2    Modify Men_ParseTaxDetail() to output agency record. Modify Men_ParseTaxBase() 
 *                      to add DelqYear to base record. Modify Men_Load_TaxBase() to import TaxAgency.
 *                      Modify Men_ParseTaxDelq() to add DefaultNum and use last file date for Upd_Date.
 * 09/15/2016 16.2.5    Rewrite Men_ParseTaxBase & Men_ParseTaxDelq.  Add Men_MergeTaxDelq to update DelqYear
 *                      in Tax_Base.  No longer use STS_DATE in SecRoll for DelqYear.
 * 11/18/2016 16.7.0    Modify Men_FormatSale() to update Full/Partial sale on Sale_Code
 * 12/07/2016 16.7.1.1  Add comment to Men_ParseTaxDetail()
 * 12/31/2016 16.8.4    Remove code that stores VESTING in SCSale.filler
 * 03/09/2017 16.12.0   Populate inst1Status, inst1Status, and billType in TaxBase.
 *                      Handle bill correction and cancel in ParseTaxBase().
 * 07/23/2017 17.1.3    Modify Men_MergeSAdr() to add special known cases of bad city code
 * 11/19/2017 17.5.0    Use updateDelqFlag() to update TaxBase's delqFlg & delqYear.
 *                      Modify Men_ParseTaxDelq() to set TaxYear based on default date (one year prior).
 * 01/19/2018 17.6.2    Rename Men_CreatePQ4Rec() to Men_MergeRoll() and update Multi_APN flag
 * 06/05/2019 18.12.8   Remove chkNegValue() and use chkSignValue() instead.
 * 06/17/2020 19.10.2   Add -Xn & -Mn option.
 * 06/22/2020 20.0.0    Update EXE_CD1 regardless of exemt value.
 * 10/28/2020 20.3.6    Modify Men_MergeRoll() to populate PQZoning.
 * 08/15/2021 21.1.3    Add Men_Load_LDR_Csv(), Men_MergeLienCsv(), Men_MergeRollChar()
 *                      to support new LDR file.
 * 08/17/2021 21.1.4    Modify Men_MergeMAdr() to remove unknown City,  Add Men_ExtrLienCsv() to extract value
 *                      from new LDR file "2021 ANNUAL ROLLS - SECURED.txt".
 * 12/15/2021 21.4.6    Add Men_Load_RollCorrection() & Men_ExtrRollCorrection() to process roll correction file.
 * 01/14/2022 21.4.8    Modify Men_FormatSale() to set DocType='1' for all purchases (salerec->type='P')
 *
 ******************************************************************************/

#include "stdafx.h"
#include "CountyInfo.h"
#include "hlAdo.h"
#include "Prodlib.h"
#include "R01.h"
#include "RecDef.h"
#include "Tables.h"
#include "Logs.h"
#include "LoadDef.h"
#include "Utils.h"
#include "XlatTbls.h"
#include "doSort.h"
#include "doOwner.h"
#include "formatApn.h"
#include "SaleRec.h"
#include "LoadOne.h"
#include "MergeMen.h"
#include "UseCode.h"
#include "LOExtrn.h"
#include "Tax.h"
#include "NdcExtr.h"
#include "CharRec.h"

static   FILE  *fdLien, *fdDelq, *fdChar;
static   long  lCharSkip, lCharMatch, lSaleSkip, lSaleMatch, lLienSkip, lLienMatch, lUnkMailAdr;
static   char  acUnsRoll[_MAX_PATH];

//static long     lCharSkip, lSaleSkip, lRollSkip;
//static long     lCharMatch, lSaleMatch, lMatchCChr, lRollMatch;

extern   REC_CNT     asRecCnt[];

/******************************* Men_ExtrSaleCsv *****************************
 *
 * Extract sale file CA-Mendo-Sales.csv 05/27/2022
 *
 *****************************************************************************/

int Men_ExtrSaleCsv()
{
   char      acOutFile[_MAX_PATH], acTmpFile[_MAX_PATH], acApn[32], acTmp[256], acRec[2048], *pRec;
   long      lCnt=0, lPrice, iTmp;
   FILE      *fdOut;
   SCSAL_REC SaleRec;

   LogMsg0("Loading Sale file");

   // Check for Unicode
   sprintf(acTmpFile, "%s\\MEN\\Sales.txt", acTmpPath);
   if (!UnicodeToAnsi(acSaleFile, acTmpFile))
      strcpy(acSaleFile, acTmpFile);

   // Open Sales file
   LogMsg("Open Sales file %s", acSaleFile);
   fdSale = fopen(acSaleFile, "r");
   if (fdSale == NULL)
   {
      LogMsg("***** Error opening Sales file: %s\n", acSaleFile);
      return 2;
   }

   // Open Output file
   sprintf(acTmpFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "TMP");
   LogMsg("Open output file %s", acTmpFile);
   fdOut = fopen(acTmpFile, "w");
   if (fdOut == NULL)
   {
      LogMsg("***** Error creating sale output file: %s\n", acTmpFile);
      return -2;
   }

   // Loop through record set
   while (!feof(fdSale))
   {  
      pRec = fgets(acRec, 2048, fdSale);
      if (!pRec)
         break;

      // Parse input rec
      iTokens = ParseStringNQ(pRec, cDelim, MAX_FLD_TOKEN, apTokens);

      if (iTokens < S_COLS)
      {
         LogMsg0("*** Error: bad sale record %.16s (#tokens=%d)", acRec, iTokens);
         continue;
      }

      // Collect data - Only take sale rec that has both docnum and docdate
      if (*apTokens[S_CONVEYANCENUMBER] == ' ' || *apTokens[S_CONVEYANCEDATE] == ' ')
         continue;
      if (*apTokens[S_CONVEYANCENUMBER] > '9' || *apTokens[S_CONVEYANCENUMBER] < '2')
         continue;

      // Reset output record
      memset((void *)&SaleRec, ' ', sizeof(SCSAL_REC));

      // APN
      iTmp = iTrim(apTokens[S_PIN]);
      sprintf(acApn, "%.*s%s", iApnLen-iTmp, "000000", apTokens[S_PIN]);
      if (iTmp < iApnLen)
      {
         LogMsg("*** Verify APN: %s", apTokens[S_PIN]);
         memcpy(SaleRec.Apn, acApn, iApnLen);
      } else
         memcpy(SaleRec.Apn, apTokens[S_PIN], iApnLen);

      // Doc date
      pRec = dateConversion(apTokens[S_CONVEYANCEDATE], acTmp, YYYY_MM_DD);
      if (pRec)
      {
         memcpy(SaleRec.DocDate, pRec, 8);
         iTmp = atol(acTmp);
         if (iTmp > lLastRecDate)
            lLastRecDate = iTmp;
      } else
      {
         LogMsg("*** Bad sale date: %s [%s]", apTokens[S_CONVEYANCEDATE], apTokens[S_PIN]);
         continue;
      }

      // Docnum
      if (*(apTokens[S_CONVEYANCENUMBER]+4) == '-')
         vmemcpy(SaleRec.DocNum, apTokens[S_CONVEYANCENUMBER]+5, 5);
      else
      {
         LogMsg("*** Bad docnum: %s [%s]", apTokens[S_CONVEYANCENUMBER], apTokens[S_PIN]);
         continue;
      }

      // Sale price
      lPrice = atol(apTokens[S_INDICATEDSALEPRICE]);
      if (!lPrice)
         lPrice = atol(apTokens[S_ADJUSTEDSALEPRICE]);

      if (lPrice > 0)
      {
         sprintf(acTmp, "%*u", SIZ_SALE1_AMT, lPrice);
         memcpy(SaleRec.SalePrice, acTmp, SIZ_SALE1_AMT);
      }

      // Transfer Type
      int iIdx = XrefCodeIndex((XREFTBL *)&asDeed[0], apTokens[S_TRANSFERTYPECODE], iNumDeeds);
      if (iIdx >= 0)
      {
         if (lPrice > 1000)
         {
            // Set fractional interest
            if (iIdx == 57)
               SaleRec.SaleCode[0] = 'P';
         } else if (asDeed[iIdx].acFlags[0] == 'Y')
            SaleRec.NoneSale_Flg = 'Y';
         iTmp = sprintf(acTmp, "%d", asDeed[iIdx].iIdxNum);
         memcpy(SaleRec.DocType, acTmp, iTmp);
         memcpy(SaleRec.DocCode, apTokens[S_TRANSFERTYPECODE], 3);
      } else
         LogMsg("*** Unknown TransferTypeCode: '%s' APN=%s", apTokens[S_TRANSFERTYPECODE], apTokens[S_PIN]);

      // Group sale?

      // Seller

      // Buyer

      SaleRec.CRLF[0] = 10;
      SaleRec.CRLF[1] = 0;
      fputs((char *)&SaleRec,fdOut);

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   fclose(fdSale);
   fclose(fdOut);
   
   LogMsg("Total processed records: %u", lCnt);
   LogMsg("    Last recording date: %u\n", lLastRecDate);

   // Update cumulative sale file
   if (lCnt > 0)
   {
      // Append and resort SLS file
      // Sort on APN asc, DocDate asc, DocNum asc
      sprintf(acTmp, "S(1,14,C,A,27,8,C,A,15,12,C,A) F(TXT) DUPO(1,34) ");
      sprintf(acOutFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "srt");
      sprintf(acCSalFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "Sls");     
      if (!_access(acCSalFile, 0))
         sprintf(acRec, "%s+%s", acTmpFile, acCSalFile);
      else
         strcpy(acRec, acTmpFile);
      lCnt = sortFile(acRec, acOutFile, acTmp);
      if (lCnt > 0)
      {
         // Rename old cum sale
         sprintf(acTmpFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "sav");
         iTmp = 0;
         if (!_access(acTmpFile, 0))
            iTmp = remove(acTmpFile);
         if (!_access(acCSalFile, 0))
            iTmp = rename(acCSalFile, acTmpFile);
         if (!iTmp)
            iTmp = rename(acOutFile, acCSalFile);
         else
            LogMsg("***** Error removing %s.  Please rerun with -Xs option", acCSalFile);
      }
   } else
      iTmp = -1;

   LogMsg("Total Sale records output: %d.\n", lCnt);
   return iTmp;
}

/******************************* Men_AddUnits ********************************
 *
 * Use Men_Units.txt to populate number of units.  This file is generated by 
 * importing the PropertyCharacteristics.csv into SQL then select those APN
 * with multiple count.
 * select pin, count(*) as c from men_propchar group by pin
 * having count(*) > 1 order by pin
 *
 *****************************************************************************/

int Men_AddUnits(char *pOutbuf, FILE **fdUnit)
{
   static   char acRec[512], *apItems[8], *pRec=NULL;
   static   int iItems=0;

   int      iLoop;
   STDCHAR  *pStdChar = (STDCHAR *)pOutbuf;

   // Get first Char rec for first call
   if (!pRec)
   {
      pRec = fgets(acRec, 512, *fdUnit);
      iItems = ParseString(acRec, '|', 3, apItems);
   }

   do
   {
      // Compare Apn
      iLoop = memcmp(pOutbuf, acRec, iApnLen);
      if (iLoop > 0)
      {
         if (bDebug)
            LogMsg0("Skip rec  %s", pRec);
         pRec = fgets(acRec, 512, *fdUnit);
         if (!pRec)
         {
            fclose(*fdUnit);
            *fdUnit = NULL;
            return -1;      // EOF
         } else
            iItems = ParseString(acRec, '|', 3, apItems);
      }
   } while (iLoop > 0);

   // If not match, return
   if (iLoop)
      return 1;

#ifdef _DEBUG
   //if (!memcmp(pOutBase->Assmnt_No, "44257021", 8))
   //   iTmp = 0;
#endif
   if (iItems > 1)
      memcpy(pStdChar->Units, apItems[1], strlen(apItems[1]));
   
   return 0;
}

/***************************** Men_ConvStdCharCsv ****************************
 *
 * Convert CA-Mendo-PropChar.csv to STD_CHAR format
 *
 *****************************************************************************/

int Men_ConvStdCharCsv(char *pCharfile)
{
   char     acInbuf[1024], acOutbuf[2048], *pRec;
   char     acTmpFile[256], acTmp[256], cTmp1;
   double   dTmp;
   int      iRet, iTmp, iBeds, iFBath, iHBath, iBath3Q, iBath4Q, iCnt, lTmp, lSqft;

   STDCHAR  *pStdChar = (STDCHAR *)acOutbuf;
   FILE     *fdChar, *fdOut, *fdUnit;

   LogMsg0("Converting standard char file.");

   GetIniString(myCounty.acCntyCode, "UnitFile", "", acTmpFile, _MAX_PATH, acIniFile);
   fdUnit = (FILE *)NULL;
   if (!_access(acTmpFile, 0))
   {
      LogMsg("Open Unit file %s", acTmpFile);
      if (!(fdUnit = fopen(acTmpFile, "r")))
         LogMsg("*** Error opening Unit file %s.  Ignore updating number of Units", acTmpFile);
   }

   // Check for Unicode
   sprintf(acTmpFile, "%s\\MEN\\PropChar.txt", acTmpPath);
   if (!UnicodeToAnsi(pCharfile, acTmpFile))
      strcpy(pCharfile, acTmpFile);
   LogMsg("Open char file %s", pCharfile);
   if (!(fdChar = fopen(pCharfile, "r")))
   {
      LogMsg("***** Error opening input file %s", pCharfile);
      return -1;
   }

   sprintf(acTmpFile, "%s\\%s\\%s_char.tmp", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   if (!(fdOut = fopen(acTmpFile, "w")))
   {
      fclose(fdChar);
      LogMsg("***** Error creating output file %s", acTmpFile);
      return -2;
   }

   iCnt = 0;
   while (!feof(fdChar))
   {
      pRec = fgets(acInbuf, 1024, fdChar);
      if (!pRec) break;

      if (*pRec > '9')
         continue;

#ifdef _DEBUG
      //if (!memcmp(acInbuf, "009619939", 9))
      //   iTokens = 0;
#endif

      // Remove all "N/A"
      replStrAll(acInbuf, "N/A", "");

      iTokens = ParseStringNQ(pRec, cDelim, MAX_FLD_TOKEN, apTokens);
      if (iTokens < C_COLS)
      {
         if (iTokens > 1)
            LogMsg("*** Bad CHAR record (%d): %s [%d]", iCnt, pRec, iTokens);
         continue;
      } else if (iTokens > C_COLS)
         LogMsg("*** Please check for possible corruption record: %s", pRec);

      memset(acOutbuf, ' ', sizeof(STDCHAR));

      // Format APN
      vmemcpy(pStdChar->Apn, apTokens[C_APN], SIZ_CHAR_APN);
      iRet = formatApn(apTokens[C_APN], acTmp, &myCounty);
      memcpy(pStdChar->Apn_D, acTmp, iRet);

      // Yrblt
      lTmp = atol(apTokens[C_YEARBUILT]);
      if (lTmp > 1700)
         memcpy(pStdChar->YrBlt, apTokens[C_YEARBUILT], SIZ_YR_BLT);
      lTmp = atol(apTokens[C_EFFYEARBUILT]);
      if (lTmp > 1900)
         memcpy(pStdChar->YrEff, apTokens[C_EFFYEARBUILT], SIZ_YR_BLT);

      // Stories
      iTmp = atol(apTokens[C_NUMBEROFSTORIES]);
      if (iTmp > 0 && iTmp < 100)
      {
         if (bDebug && iTmp > 9)
            LogMsg("*** Please verify number of stories for %s (%d)", apTokens[C_APN], iTmp);

         iRet = sprintf(acTmp, "%d.0", iTmp);
         memcpy(pStdChar->Stories, acTmp, iRet);
      }

      // Garage Sqft
      // Property may have more than one garage, but we only have space for one
      long lGarSqft = atol(apTokens[C_GARAGEAREA]) + atol(apTokens[C_GARAGE2AREA]);
      long lCarpSqft= atol(apTokens[C_CARPORTSIZE]) + atol(apTokens[C_CARPORT2SIZE]);

      // Garage type
      if (*apTokens[C_GARAGETYPE] == 'A' || *apTokens[C_GARAGE2TYPE] == 'A')
         pStdChar->ParkType[0] = 'I';              // Attached garage
      else if (*apTokens[C_GARAGETYPE] == 'D')
         pStdChar->ParkType[0] = 'L';              // Detached garage
      else if (lCarpSqft > 100)
         pStdChar->ParkType[0] = 'C';              // Carport

      if (lGarSqft > 100 || lCarpSqft > 100)
      {
         if (lGarSqft >= 100 && lCarpSqft > 100)
         {
            lGarSqft += lCarpSqft;
            pStdChar->ParkType[0] = '2';           // Garage/Carport
         }
         iTmp = sprintf(acTmp, "%d", lGarSqft);
         memcpy(pStdChar->GarSqft, acTmp, iTmp);
      }

      // Central Heating-Cooling
      if (*apTokens[C_HASCENTRALHEATING] == 'Y')
         pStdChar->Heating[0] = 'Z';
      if (*apTokens[C_HASCENTRALCOOLING] == 'Y')
         pStdChar->Cooling[0] = 'C';

      // Beds
      iBeds = atol(apTokens[C_BEDROOMCOUNT]);
      if (iBeds > 0)
      {
         iTmp = sprintf(acTmp, "%d", iBeds);
         memcpy(pStdChar->Beds, acTmp, iTmp);
      }

#ifdef _DEBUG
      //if (!memcmp(pStdChar->Apn, "009600002", 9) )
      //   lTmp = 0;
#endif

      // Bath
      iBath4Q = atol(apTokens[C_BATHSFULL]);
      iHBath  = atol(apTokens[C_BATHSHALF]);
      iBath3Q = atol(apTokens[C_BATHS3_4]);
      iFBath = iBath3Q+iBath4Q;
      if (iBath3Q > 0)
      {
         iTmp = sprintf(acTmp, "%d", iBath3Q);
         memcpy(pStdChar->Bath_3QX, acTmp, iTmp);
         if (iTmp < 3)
            memcpy(pStdChar->Bath_3Q, acTmp, iTmp);
      }

      if (iBath4Q > 0)
      {
         iTmp = sprintf(acTmp, "%d", iBath4Q);
         memcpy(pStdChar->Bath_4QX, acTmp, iTmp);
         if (iTmp < 3)
            memcpy(pStdChar->Bath_4Q, acTmp, iTmp);
      }

      if (iFBath > 0 && iFBath < 100)
      {
         iTmp = sprintf(acTmp, "%d", iFBath);
         memcpy(pStdChar->FBaths, acTmp, iTmp);
      }

      if (iHBath > 0)
      {
         iTmp = sprintf(acTmp, "%d", iHBath);
         memcpy(pStdChar->Bath_2QX, acTmp, iTmp);
         if (iTmp < 3)
         {
            memcpy(pStdChar->HBaths, acTmp, iTmp);
            memcpy(pStdChar->Bath_2Q, acTmp, iTmp);
         }
      }

      // Fireplace Y/N
      if (*apTokens[C_HASFIREPLACE] > ' ')
         pStdChar->Fireplace[0] = *apTokens[C_HASFIREPLACE];

      // Roof
      if (*apTokens[C_ROOFTYPE] >= 'A')
      {
         _strupr(apTokens[C_ROOFTYPE]);
         pRec = findXlatCodeA(apTokens[C_ROOFTYPE], &asRoofType[0]);
         if (pRec)
            pStdChar->RoofType[0] = *pRec;
      }

      // Pool/Spa Y/N
      if (*apTokens[C_HASPOOL] == 'Y')
         pStdChar->Pool[0] = 'P';       // Pool

      // Electric
      if (*apTokens[C_UTILITYELECTRIC] >= 'A')
      {
         switch (*apTokens[C_UTILITYELECTRIC])
         {
            case 'A':
            case 'D':
               cTmp1 = *apTokens[C_UTILITYELECTRIC];  // AVAIL
               break;
            case 'N':
               cTmp1 = 'N';                           // NON AVAIL
               break;
            default:
               if (!_memicmp(apTokens[C_UTILITYELECTRIC], "Under", 5))
                  cTmp1 = 'D';
               else
                  cTmp1 = ' ';
         }
         pStdChar->HasElectric = cTmp1;
      }

      // Gas
      if (*apTokens[C_UTILITYGAS] >= 'A')
      {
         switch (*apTokens[C_UTILITYGAS])
         {
            case 'A':
            case 'D':
               cTmp1 = *apTokens[C_UTILITYGAS];    // AVAIL
               break;
            case 'N':
               cTmp1 = 'N';                        // NON AVAIL
               break;
            default:
               cTmp1 = ' ';
         }
         pStdChar->HasGas = cTmp1;
      }

      // Water - currently not avail. 5/27/2022
      if (*apTokens[C_DOMESTICWATER] == 'A' || *apTokens[C_DOMESTICWATER] == 'D')
      {
         pStdChar->Water = *apTokens[C_DOMESTICWATER];
         pStdChar->HasWater = 'Y';
      } else if (*apTokens[C_WELLWATER] == 'A' || *apTokens[C_WELLWATER] == 'D')
      {
         pStdChar->Water = *apTokens[C_WELLWATER];
         pStdChar->HasWater = 'W';
         pStdChar->HasWell = 'Y';
      } else if (*apTokens[C_IRRIGATIONWATER] == 'A' || *apTokens[C_IRRIGATIONWATER] == 'D')
      {
         pStdChar->HasWater = 'L';
         pStdChar->Water = *apTokens[C_IRRIGATIONWATER];
      } else if (*apTokens[C_DOMESTICWATER] == 'N' || *apTokens[C_WELLWATER] == 'N' || *apTokens[C_IRRIGATIONWATER] == 'N')
      {
         pStdChar->Water = 'N';
         pStdChar->HasWater = 'N';
      }

      // Sewer
      if (*apTokens[C_SEWER] > ' ')
      {
         pStdChar->Sewer = *apTokens[C_SEWER];
         if (*apTokens[C_SEWER] == 'A' || *apTokens[C_SEWER] == 'D')
            pStdChar->HasSewer = 'Y';
      }

      // View
      if (*apTokens[C_FAIRWAY] == 'Y')
         pStdChar->View[0] = 'H';   
      else if (*apTokens[C_WATERFRONT] == 'Y')
         pStdChar->View[0] = 'W';   

      // BldgSqft
      lSqft = atol(apTokens[C_LIVINGAREA]);
      if (!lSqft)
         lSqft = atol(apTokens[C_ACTUALAREA]);
      if (lSqft > 0)
      {
         iTmp = sprintf(acTmp, "%d", lSqft);
         memcpy(pStdChar->BldgSqft, acTmp, iTmp);
      }

      // Construction type
      if (*apTokens[C_CONSTRUCTIONTYPE] >= 'A')
      {
         _strupr(apTokens[C_CONSTRUCTIONTYPE]);
         pRec = findXlatCodeA(apTokens[C_CONSTRUCTIONTYPE], &asConstType[0]);
         if (pRec)
            pStdChar->ConstType[0] = *pRec;
      }

      // BldgClass 
      if (*apTokens[C_CONSTRUCTIONTYPE] == 'W')
         pStdChar->BldgClass = 'D';
      else if (*apTokens[C_CONSTRUCTIONTYPE] == 'M')
         pStdChar->BldgClass = 'C';
      else if (!memcmp(apTokens[C_CONSTRUCTIONTYPE], "PO", 2))
         pStdChar->BldgClass = 'P';
      else if (!memcmp(apTokens[C_CONSTRUCTIONTYPE], "PR", 2))
         pStdChar->BldgClass = 'S';

      // Quality
      if (!_memicmp(apTokens[C_QUALITYCODE], "Rank", 4))
         dTmp = atof(apTokens[C_QUALITYCODE]+5);
      else
         dTmp = atof(apTokens[C_QUALITYCODE]);
      if (dTmp > 0.1)
      {
         char acCode[16];

         sprintf(acTmp, "%.1f", dTmp);
         iTmp = Value2Code(acTmp, acCode, NULL);
         if (acCode[0] > ' ')
            pStdChar->BldgQual = acCode[0];
      }

      // QualityClass
      if (pStdChar->BldgClass > ' ' && dTmp > 0.1)
      {
         sprintf(acTmp, "%c%.1f%c", pStdChar->BldgClass, dTmp, *apTokens[C_SHAPECODE]);
         vmemcpy(pStdChar->QualityClass, acTmp, SIZ_CHAR_QCLS, strlen(acTmp));
      }

      // Lot sqft - Lot Acres
      dTmp = atof(apTokens[C_ACREAGE]);
      if (dTmp > 0.001)
      {
         iTmp = sprintf(acTmp, "%d", (long)(dTmp*1000));
         memcpy(pStdChar->LotAcre, acTmp, iTmp);
         lSqft = long(dTmp*SQFT_PER_ACRE);

         iTmp = sprintf(acTmp, "%d", lSqft);
         memcpy(pStdChar->LotSqft, acTmp, iTmp);
      }

      // Add #Units
      if (fdUnit)
      {
         Men_AddUnits(acOutbuf, &fdUnit);
      }

      pStdChar->CRLF[0] = '\n';
      pStdChar->CRLF[1] = '\0';
      fputs(acOutbuf, fdOut);

      if (!(++iCnt % 1000))
         printf("\r%u", iCnt);
   }

   if (fdChar) fclose(fdChar);
   if (fdOut)  fclose(fdOut);
   if (fdUnit) fclose(fdUnit);

   LogMsg("Number of records processed: %d", iCnt);

   // Sort output on ASMT
   if (iCnt > 100)
   {
      // Rename old CHAR file if needed
      if (!_access(acCChrFile, 0))
      {
         strcpy(acInbuf, acCChrFile);
         replStr(acInbuf, ".dat", ".sav");
         if (!_access(acInbuf, 0))
         {
            LogMsg("Delete old %s", acInbuf);
            DeleteFile(acInbuf);
         }
         LogMsg("Rename %s to %s", acCChrFile, acInbuf);
         RenameToExt(acCChrFile, "sav");
      }

      LogMsgD("\nSorting char file %s --> %s", acTmpFile, acCChrFile);
      // Sort on APN, YrBlt to keep the latest record on top
      iRet = sortFile(acTmpFile, acCChrFile, "S(1,14,C,A,53,4,C,D) F(TXT) ");
   } else
   {
      printf("\n");
      iRet = 0;
   }

   return iRet;
}

/******************************** Men_MergeOwner *****************************
 *
 * Return 0 if successful, >0 if error
 *
 *****************************************************************************/

void Men_MergeOwner(char *pOutbuf, char *pRollRec)
{
   int   iTmp;
   char  acTmp[256], acName1[256], acName2[256], *pTmp;
   bool  bAppend=false;

   OWNER    myOwner;
   MEN_ROLL *pRec;

   pRec = (MEN_ROLL *)pRollRec;

   // Clear output buffer if needed
   removeNames(pOutbuf, false);

   // Check CareOf
   if (pRec->Careof[0] > ' ')
      updateCareOf(pOutbuf, pRec->Careof, RSIZ_CAREOF);

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "1670301800", 10))
   //   iTmp = 0;
#endif

   // Owner name
   memcpy(acName1, pRec->Name1, RSIZ_NAME1);
   acName1[RSIZ_NAME1] = 0;
   memcpy(acName2, pRec->Name2, RSIZ_NAME2);
   acName2[RSIZ_NAME2] = 0;

   // Determine whether to append name1 and name2
   if ((pTmp = strstr(acName1, " / ")) || !memcmp(&acName1[RSIZ_NAME1-2], " /", 2))
   {
      if (acName1[RSIZ_NAME1-1] == '/')
         acName1[RSIZ_NAME1-2] = 0;
      else
         *pTmp = 0;
   } else if (memcmp(acName2, "  ", 2) )
      bAppend = true;

   // Remove number in name1
   // FREY LUKE 14.29%              1072100800
   pTmp = acName1;
   while (*pTmp)
   {
      // Break where name has numeric value
      if (isdigit(*pTmp))
         break;

      if (*pTmp == '.' || *pTmp == '(' || *pTmp == ')')
         *pTmp = ' ';
      pTmp++;
   }
   *pTmp = 0;

   // Remove number in name2
   pTmp = acName2;
   while (*pTmp)
   {
      // Break where name has numeric value
      if (isdigit(*pTmp))
         break;

      if (*pTmp == '.' || *pTmp == '(' || *pTmp == ')')
         *pTmp = ' ';
      pTmp++;
   }
   *pTmp = 0;

   // Append name1 and name2
   if (bAppend)
   {
      if (!memcmp(acName2, "TTEES", 5) && acName1[strlen(acName1)-1] != ' ')
         strcat(acName1, " ");
      strcat(acName1, acName2);
      acName2[0] = 0;
   } else
      blankRem(acName2);

   // Update vesting
   iTmp = updateVesting(myCounty.acCntyCode, acName1, pOutbuf+OFF_VEST, pOutbuf+OFF_ETAL_FLG);
   if (pRec->Etal_Flag[0] == 'Y')
      *(pOutbuf+OFF_ETAL_FLG) = 'Y';

   // Check for MD, DDS, and DVM
   if (pTmp=strstr(acName1, " MD "))
      memset(pTmp, ' ', 3);
   else if ((pTmp=strstr(acName1, " DDS ")) ||
            (pTmp=strstr(acName1, " DVA ")) ||
            (pTmp=strstr(acName1, " DVM ")) )
      memset(pTmp, ' ', 4);
   blankRem(acName1);

   // Save Name1
   strcpy(acTmp, acName1);

   // Terminate name
   iTmp = strlen(acTmp);
   if (acTmp[iTmp-1] == '-')
      acTmp[iTmp-1] = 0;

   if ((pTmp=strstr(acTmp, " ETAL")) || (pTmp=strstr(acTmp, " ET AL")) )
      *pTmp = 0;

   if ((pTmp=strstr(acTmp, "CO-TTEE")) || (pTmp=strstr(acTmp, " TTEE"))     ||
       (pTmp=strstr(acTmp, "TTEES"))   || (pTmp=strstr(acTmp, " TRUSTEE")) )
      *pTmp = 0;

   // Check for Name2
   /*
   if (pTmp = strchr(acTmp, '/'))
   {
      *pTmp++ = 0;

      // Only do this if they have been appended
      if (bAppend)
      {
         if (*pTmp == ' ') pTmp++;
         strcpy(acName2, pTmp);
      }
   }
   */

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "00108234", 8) )
   //   iTmp = 0;
#endif

   if ((pTmp=strstr(acTmp, " EST OF")) || (pTmp=strstr(acTmp, " LIFE EST")) ||
       (pTmp=strstr(acTmp, " ESTATE OF")) )
      *pTmp = 0;

   // Now parse owners
   iTmp = splitOwner(acTmp, &myOwner, 3);

   iTmp = strlen(acName1);
   if (iTmp > SIZ_NAME1)
      iTmp = SIZ_NAME1;
   memcpy(pOutbuf+OFF_NAME1, acName1, iTmp);

   if (acName2[0] && strchr(acName2, ' '))
   {  // Only process name2 if it has more than one token
      iTmp = strlen(acName2);
      if (iTmp > SIZ_NAME2)
         iTmp = SIZ_NAME2;
      memcpy(pOutbuf+OFF_NAME2, acName2, iTmp);
   }

   iTmp = strlen(myOwner.acSwapName);
   if (iTmp > SIZ_NAME_SWAP)
      iTmp = SIZ_NAME_SWAP;
   memcpy(pOutbuf+OFF_NAME_SWAP, myOwner.acSwapName, iTmp);
}

void Men_MergeOwner(char *pOutbuf, char *pOwner, char *pOwnerX)
{
   int   iTmp;
   char  acTmp[256], acName1[256], *pTmp;
   bool  bAppend=false;

   OWNER    myOwner;

   // Clear output buffer if needed
   removeNames(pOutbuf, false);

   // Check CareOf

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "1670301800", 10))
   //   iTmp = 0;
#endif

   // Save name1
   _strupr(pOwner);
   _strupr(pOwnerX);
   if (*pOwnerX > ' ')
   {
      if (strstr(pOwnerX, " ETUX ") || strstr(pOwnerX, " JR ") || 
         strstr(pOwnerX, " III ") || strchr(pOwnerX, '/') )
      {
         vmemcpy(pOutbuf+OFF_NAME_SWAP, pOwner, SIZ_NAME_SWAP);
         vmemcpy(pOutbuf+OFF_NAME1, pOwner, SIZ_NAME1);
         return;
      } else
         strcpy(acName1, pOwnerX);
   } else
      strcpy(acName1, pOwner);
   
   iTmp = iTrim(acName1);
   if (acName1[iTmp-1] == '/')
   {
      if (acName1[iTmp-2] < 'A')
         acName1[iTmp-2] = 0;
      else
         acName1[iTmp-1] = 0;
   }

   // Update Owner1
   vmemcpy(pOutbuf+OFF_NAME1, acName1, SIZ_NAME1);

   // Remove number in name1
   // FREY LUKE 14.29% 
   pTmp = acName1;
   while (*pTmp)
   {
      // Break where name has numeric value
      if (isdigit(*pTmp))
         break;

      if (*pTmp == '.' || *pTmp == '(' || *pTmp == ')')
         *pTmp = ' ';
      pTmp++;
   }
   *pTmp = 0;

   // Update vesting
   iTmp = updateVesting(myCounty.acCntyCode, acName1, pOutbuf+OFF_VEST, pOutbuf+OFF_ETAL_FLG);

   // Check for MD, DDS, and DVM
   if (pTmp=strstr(acName1, " MD "))
      *pTmp = 0;
   else if ((pTmp=strstr(acName1, " DDS ")) || (pTmp=strstr(acName1, " DVA ")) || (pTmp=strstr(acName1, " DVM ")) )
      *pTmp = 0;

   // Save Name1
   strcpy(acTmp, acName1);

   // Terminate name
   iTmp = strlen(acTmp);
   if (acTmp[iTmp-1] == '-')
      acTmp[iTmp-1] = 0;

   if ((pTmp=strstr(acTmp, " ETAL")) || (pTmp=strstr(acTmp, " ET AL")) )
      *pTmp = 0;

   if ((pTmp=strstr(acTmp, "CO-TTEE")) || (pTmp=strstr(acTmp, " TTEE"))     ||
       (pTmp=strstr(acTmp, "TTEES"))   || (pTmp=strstr(acTmp, " TRUSTEE")) )
      *pTmp = 0;

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "00108234", 8) )
   //   iTmp = 0;
#endif

   if ((pTmp=strstr(acTmp, " EST OF")) || (pTmp=strstr(acTmp, " LIFE EST")) ||
       (pTmp=strstr(acTmp, " ESTATE OF")) )
      *pTmp = 0;

   // Now parse owners
   iTmp = splitOwner(acTmp, &myOwner, 3);

   if (iTmp >= 0)
      vmemcpy(pOutbuf+OFF_NAME_SWAP, myOwner.acSwapName, SIZ_NAME_SWAP);
   else
      vmemcpy(pOutbuf+OFF_NAME_SWAP, acTmp, SIZ_NAME_SWAP);
}

/********************************* Men_MergeSAdr *****************************
 *
 * 7/24/2017 Adding special cases for city
 *
 *****************************************************************************/

void Men_MergeSAdr(char *pOutbuf, char *pRollRec)
{
   MEN_ROLL *pRec;
   char     acTmp[256], acTmp2[256], acAddr1[64], acAddr2[64], acCity[64], acCode[64];
   int      iTmp, iStrNo;
   ADR_REC  sSitusAdr;

   pRec = (MEN_ROLL *)pRollRec;

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "0010501600", 10) )
   //   iTmp = 0;
#endif
   // Situs
   acAddr1[0] = 0;
   acAddr2[0] = 0;
   memset((void *)&sSitusAdr, 0, sizeof(ADR_REC));
   pRec->Apn[iApnLen] = 0;
   if (pRec->Situs_Street_Number[0] > ' ')
   {
      iStrNo = atoin(pRec->Situs_Street_Number, RSIZ_SITUS_STREET_NUMBER);
      if (iStrNo > 0)
      {
         sprintf(acAddr1, "%d       ", iStrNo);
         memcpy(pOutbuf+OFF_S_STRNUM, acAddr1, SIZ_S_STRNUM);
         memcpy(pOutbuf+OFF_S_HSENO, acAddr1, SIZ_S_STRNUM);
      }

      memcpy(acTmp2, pRec->Situs_Street_Name, RSIZ_SITUS_STREET_NAME);
      blankRem(acTmp2, RSIZ_SITUS_STREET_NAME);

      // Parse StrName into StrName, Dir, Suffix, and Unit#
      parseAdr1_4(&sSitusAdr, acTmp2, false);

      if (pRec->Situs_Dir[0] > 'A')
      {
         if (!memcmp(pRec->Situs_Dir, "NO", 2))
            strcpy(sSitusAdr.strDir, "N");
         else if (!memcmp(pRec->Situs_Dir, "SO", 2))
            strcpy(sSitusAdr.strDir, "S");
         else if (!memcmp(pRec->Situs_Dir, "EA", 2))
            strcpy(sSitusAdr.strDir, "E");
         else if (!memcmp(pRec->Situs_Dir, "WE", 2))
            strcpy(sSitusAdr.strDir, "W");
      }

      if (pRec->Situs_Unit[0] > '0')
      {
         if (pRec->Situs_Unit[0] == '#')
            memcpy(sSitusAdr.Unit, pRec->Situs_Unit, RSIZ_SITUS_UNIT);
         else
         {
            sSitusAdr.Unit[0] = '#';
            memcpy(&sSitusAdr.Unit[1], pRec->Situs_Unit, RSIZ_SITUS_UNIT-1);
         }
      }

      if (pRec->Situs_StrSub[0] > '0')
      {
         memcpy(sSitusAdr.strSub, pRec->Situs_StrSub, RSIZ_SITUS_STR_SUB);
         memcpy(pOutbuf+OFF_S_STR_SUB, pRec->Situs_StrSub, RSIZ_SITUS_STR_SUB);
      }

      if (sSitusAdr.strName[0] > ' ')
         memcpy(pOutbuf+OFF_S_STREET, sSitusAdr.strName, strlen(sSitusAdr.strName));
      if (sSitusAdr.strDir[0] > ' ')
         memcpy(pOutbuf+OFF_S_DIR, sSitusAdr.strDir, strlen(sSitusAdr.strDir));
      if (sSitusAdr.SfxCode[0] > ' ')
         memcpy(pOutbuf+OFF_S_SUFF, sSitusAdr.SfxCode, strlen(sSitusAdr.SfxCode));
      if (sSitusAdr.Unit[0] > ' ')
         memcpy(pOutbuf+OFF_S_UNITNO, sSitusAdr.Unit, strlen(sSitusAdr.Unit));

      if (iStrNo > 0)
      {
         iTmp = atoi(sSitusAdr.SfxCode);
         if (iTmp > 0)
            sprintf(acTmp, "%d %s %s %s %s %s", iStrNo, sSitusAdr.strSub, sSitusAdr.strDir,
               sSitusAdr.strName, sSitusAdr.strSfx, sSitusAdr.Unit);
         else
            sprintf(acTmp, "%d %s %s %s %s", iStrNo, sSitusAdr.strSub, sSitusAdr.strDir,
               sSitusAdr.strName, sSitusAdr.Unit);

         blankRem(acTmp);
         iTmp = strlen(acTmp);
         if (iTmp > SIZ_S_ADDR_D) iTmp = SIZ_S_ADDR_D;
         memcpy(pOutbuf+OFF_S_ADDR_D, acTmp, iTmp);
      }
   }

   // Parsing city name
   memcpy(acTmp, pRec->City_Code, RSIZ_CITY_CODE);
   acTmp[RSIZ_CITY_CODE] = 0;

   // City
   if (pRec->City_Code[0] > ' ')
   {
      memcpy(acTmp, pRec->City_Code, RSIZ_CITY_CODE);
      acTmp[RSIZ_CITY_CODE] = 0;
      Abbr2Code(acTmp, acCode, acCity, pOutbuf);
      if (acCode[0] > ' ')
      {
         memcpy(pOutbuf+OFF_S_CITY, acCode, strlen(acCode));
         strcat(acCity, " CA");
         memcpy(pOutbuf+OFF_S_CTY_ST_D, acCity, strlen(acCity));
      } else if (!memcmp(pOutbuf, "0022312300", iApnLen) || !memcmp(pOutbuf, "7001407600", iApnLen))
      {
         memcpy(pOutbuf+OFF_S_CITY, "28", 2);      // UKIAH
         strcpy(acCity, "UKIAH CA");
         memcpy(pOutbuf+OFF_S_CTY_ST_D, acCity, strlen(acCity));
      } else if (!memcmp(pOutbuf, "0125002000", iApnLen))
      {
         memcpy(pOutbuf+OFF_S_CITY, "16", 2);      // LEGGETT
         strcpy(acCity, "LEGGETT CA");
         memcpy(pOutbuf+OFF_S_CTY_ST_D, acCity, strlen(acCity));
      } else if (!memcmp(pOutbuf, "1323100800", iApnLen))
      {
         memcpy(pOutbuf+OFF_S_CITY, "18", 2);      // MANCHESTER
         strcpy(acCity, "MANCHESTER CA");
         memcpy(pOutbuf+OFF_S_CTY_ST_D, acCity, strlen(acCity));
      }
   }
   memcpy(pOutbuf+OFF_S_ST, "CA", 2);
}

/********************************* Men_MergeSAdr *****************************
 *
 * 08/13/2021 New layout
 *
 *****************************************************************************/

void Men_MergeSAdr(char *pOutbuf, char *pSAddr1, char *pCity, char *pZip)
{
   char     *pTmp, *pAddr1, acTmp[256], acUnit[8], 
            acCode[16], acAddr1[64], acAddr2[64];
   int      iTmp;
   ADR_REC  sSitusAdr;

   // Clear old Mailing
   removeSitus(pOutbuf);

   // Check for blank address
   if (!memcmp(pSAddr1, "     ", 5))
      return;

   strcpy(acAddr1, pSAddr1);
   blankRem(acAddr1);
   pAddr1 = acAddr1;

   vmemcpy(pOutbuf+OFF_S_ADDR_D, acAddr1, SIZ_S_ADDR_D);
   memset((void *)&sSitusAdr, 0, sizeof(ADR_REC));
   parseMAdr1(&sSitusAdr, acAddr1);

   // Situs
   if (sSitusAdr.lStrNum > 0 && sSitusAdr.strName[0] >= ' ')
   {
      int   iIdx=0;

      removeSitus(pOutbuf);

      vmemcpy(pOutbuf+OFF_S_STRNUM, sSitusAdr.strNum, SIZ_S_STRNUM);
      vmemcpy(pOutbuf+OFF_S_HSENO, sSitusAdr.HseNo, SIZ_S_HSENO);
      vmemcpy(pOutbuf+OFF_S_STR_SUB, sSitusAdr.strSub, SIZ_S_STR_SUB);
      strcpy(acTmp, sSitusAdr.strName);
      if (pTmp = strchr(acTmp, '*'))
         *pTmp = ' ';
      if (pTmp = strchr(acTmp, '('))
      {
         *pTmp++ = 0;
         if (!memcmp(pTmp, "REAR", 4))
            strcpy(acUnit, "REAR");
      }

      // Prepare display field
      vmemcpy(pOutbuf+OFF_S_ADDR_D, acAddr1, SIZ_S_ADDR_D);

      if (sSitusAdr.strName[0] > ' ')
         vmemcpy(pOutbuf+OFF_S_STREET, sSitusAdr.strName, SIZ_S_STREET);
      if (sSitusAdr.strDir[0] > ' ' && isDir(sSitusAdr.strDir))
         vmemcpy(pOutbuf+OFF_S_DIR, sSitusAdr.strDir, SIZ_S_DIR);
      if (sSitusAdr.SfxCode[0] > ' ')
         vmemcpy(pOutbuf+OFF_S_SUFF, sSitusAdr.SfxCode, SIZ_S_SUFF);
      if (sSitusAdr.Unit[0] > ' ')
         vmemcpy(pOutbuf+OFF_S_UNITNO, sSitusAdr.Unit, SIZ_S_UNITNO);
      
#ifdef _DEBUG
      //if (!memcmp(pOutbuf, "048523120", 9))
      //   iTmp = 0;
#endif

      // Situs city
      acCode[0] = acAddr2[0] = 0;
      if (*pCity >= 'A')
      {
         strcpy(acAddr2, pCity);
         City2CodeEx(acAddr2, acCode, sSitusAdr.City, pOutbuf);
         if (acCode[0] > ' ')
         {
            strcpy(acAddr2, sSitusAdr.City);
            memcpy(pOutbuf+OFF_S_CITY, acCode, 3);
            // Turn ON this 2 lines when zipcode is in Iny_City.N2CX
            //if (City2Zip(acAddr2, acTmp))
            //   memcpy(pOutbuf+OFF_S_ZIP, acTmp, 5);
         }
      }

      if (*pZip > ' ')
         vmemcpy(pOutbuf+OFF_S_ZIP, pZip, SIZ_S_ZIP);

      memcpy(pOutbuf+OFF_S_ST, "CA", 2);     
      if (sSitusAdr.City[0] > ' ')
      {
         sprintf(acAddr2, "%s CA %s", sSitusAdr.City, pZip);
         iTmp = blankRem(acAddr2);
         memcpy(pOutbuf+OFF_S_CTY_ST_D, acAddr2, iTmp);
      }
   }
}

/********************************** Men_MergeMAdr ****************************
 *
 * Return 0 if successful, >0 if error
 *
 *****************************************************************************/

void Men_MergeMAdr(char *pOutbuf, char *pRollRec)
{
   char      *pAddr1, acTmp[256], acAddr1[64];
   int       iTmp;
   bool      bNoMail = false;

   ADR_REC   sMailAdr;
   MEN_ROLL  *pRec = (MEN_ROLL *)pRollRec;

   // Clear old Mailing
   removeMailing(pOutbuf, false);

   // Check for blank address
   if (memcmp(pRec->Mail_Addr, "     ", 5))
   {
      if (!memcmp(pRec->Mail_Addr, "C/O", 3) )
      {
         iTmp = RSIZ_MAIL_ADDR-4;
         memcpy(acAddr1, &pRec->Mail_Addr[4], iTmp);
      } else
      {
         iTmp = RSIZ_MAIL_ADDR;
         memcpy(acAddr1, pRec->Mail_Addr, RSIZ_MAIL_ADDR);
      }
      pAddr1 = myTrim(acAddr1, iTmp);

      // Parsing mail address
      memset((void *)&sMailAdr, 0, sizeof(ADR_REC));
      parseMAdr1(&sMailAdr, acAddr1);

      if (sMailAdr.lStrNum > 0)
      {
         iTmp = sprintf(acTmp, "%d", sMailAdr.lStrNum);
         memcpy(pOutbuf+OFF_M_STRNUM, acTmp, iTmp);
         if (sMailAdr.strSub[0] > '0')
         {
            sprintf(acTmp, "%s  ", sMailAdr.strSub);
            memcpy(pOutbuf+OFF_M_STR_SUB, acTmp, SIZ_M_STR_SUB);
         }
      }
      blankPad(sMailAdr.strDir, SIZ_M_DIR);
      memcpy(pOutbuf+OFF_M_DIR, sMailAdr.strDir, SIZ_M_DIR);
      blankPad(sMailAdr.strName, SIZ_M_STREET);
      memcpy(pOutbuf+OFF_M_STREET, sMailAdr.strName, SIZ_M_STREET);
      blankPad(sMailAdr.strSfx, SIZ_M_SUFF);
      memcpy(pOutbuf+OFF_M_SUFF, sMailAdr.strSfx, SIZ_M_SUFF);
      if (sMailAdr.Unit[0] > ' ')
      {
         if (sMailAdr.Unit[0] == '#')
            memcpy(pOutbuf+OFF_M_UNITNO, sMailAdr.Unit, strlen(sMailAdr.Unit));
         else
         {
            *(pOutbuf+OFF_M_UNITNO) = '#';
            memcpy(pOutbuf+OFF_M_UNITNO+1, sMailAdr.Unit, strlen(sMailAdr.Unit));
         }
      }

      memcpy(pOutbuf+OFF_M_ADDR_D, acAddr1, strlen(acAddr1));

      ADR_REC sAdrRec;
      memset((char *)&sAdrRec, 0, sizeof(ADR_REC));

#ifdef _DEBUG
      //if (!memcmp(pOutbuf, "0033100800", 10) )
      //   iTmp = 0;
#endif
      if (memcmp(pRec->Mail_City_State, "UNK", 3) && memcmp(pRec->Mail_City_State, "TEST", 4))
      {
         memcpy(acTmp, pRec->Mail_City_State, RSIZ_MAIL_CITY_STATE);
         blankRem(acTmp, RSIZ_MAIL_CITY_STATE);
         parseMAdr2(&sAdrRec, acTmp);

         iTmp = atoin(pRec->Mail_Zip, RSIZ_MAIL_ZIP);
         if (iTmp > 500)
            memcpy(pOutbuf+OFF_M_ZIP, pRec->Mail_Zip, SIZ_M_ZIP);

         if (sAdrRec.City[0] > ' ')
         {
            vmemcpy(pOutbuf+OFF_M_CITY, sAdrRec.City, SIZ_M_CITY);
            vmemcpy(pOutbuf+OFF_M_ST, sAdrRec.State, SIZ_M_ST);

            sprintf(acTmp, "%s %s %.5s", sAdrRec.City, sAdrRec.State, pRec->Mail_Zip);
            iTmp = blankRem(acTmp);
            memcpy(pOutbuf+OFF_M_CTY_ST_D, acTmp, iTmp);
         }
      } else
      {
         LogMsg("*** Ignore unknown mailadr: %.25s [%.10s]", pRec->Mail_City_State, pRec->Apn);
         lUnkMailAdr++;
      }
   }
}

/********************************* Men_MergeMAdr *****************************
 *
 * 08/13/2021 New layout
 *
 *****************************************************************************/

void Men_MergeMAdr(char *pOutbuf, char *pMAddr1, char *pMAddr2)
{
   char     *pTmp, *pAddr1, acTmp[256], acAddr1[256], acAddr2[256];
   int      iTmp;

   // Clear old Mailing
   removeMailing(pOutbuf);

   // Check for blank address
   if (!memcmp(pMAddr1, "     ", 5))
      return;

   strcpy(acAddr1, pMAddr1);
   strcpy(acAddr2, pMAddr2);
   blankRem(acAddr1);
   blankRem(acAddr2);
   pAddr1 = acAddr1;

   iTmp = 0;
   if (*pAddr1 == '%' || !_memicmp(pAddr1, "C/O", 3))
   {
      if (*pAddr1 == '%')
         pAddr1 += 2;
      else
         pAddr1 += 4;

      // Check for C/O name
      pTmp = strchr(pAddr1, ',');
      if (pTmp && !isdigit(*(pTmp-1)) && *(pOutbuf+OFF_CARE_OF) == ' ')
      {
         *pTmp = 0;
         pAddr1 = pTmp + 1;
         if (*pAddr1 == ' ')
            pAddr1++;
         
         updateCareOf(pOutbuf, pAddr1, strlen(pAddr1));
      }
   }

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "0263400200", 10))
   //   iTmp = 0;
#endif
   // Start processing
   vmemcpy(pOutbuf+OFF_M_ADDR_D, pAddr1, SIZ_M_ADDR_D);

   // Parsing mail address
   ADR_REC sMailAdr;
   memset((void *)&sMailAdr, 0, sizeof(ADR_REC));
   parseMAdr1(&sMailAdr, pAddr1);

   if (sMailAdr.lStrNum > 0)
   {
      iTmp = sprintf(acTmp, "%d", sMailAdr.lStrNum);
      memcpy(pOutbuf+OFF_M_STRNUM, acTmp, iTmp);

      if (sMailAdr.strSub[0] > '0')
      {
         sprintf(acTmp, "%s  ", sMailAdr.strSub);
         memcpy(pOutbuf+OFF_M_STR_SUB, acTmp, SIZ_M_STR_SUB);
      }

      if (sMailAdr.Unit[0] > ' ')
         memcpy(pOutbuf+OFF_M_UNITNO, sMailAdr.Unit, strlen(sMailAdr.Unit));
   }
   memcpy(pOutbuf+OFF_M_DIR, sMailAdr.strDir, strlen(sMailAdr.strDir));
   memcpy(pOutbuf+OFF_M_STREET, sMailAdr.strName, strlen(sMailAdr.strName));
   memcpy(pOutbuf+OFF_M_SUFF, sMailAdr.strSfx, strlen(sMailAdr.strSfx));

   vmemcpy(pOutbuf+OFF_M_CTY_ST_D, acAddr2, SIZ_M_CTY_ST_D);
   parseAdr2(&sMailAdr, acAddr2);
   if (sMailAdr.City[0] >= 'A')
      vmemcpy(pOutbuf+OFF_M_CITY, sMailAdr.City, SIZ_M_CITY);
   if (sMailAdr.State[0] >= 'A' && sMailAdr.State[1] >= 'A')
      memcpy(pOutbuf+OFF_M_ST, sMailAdr.State, SIZ_M_ST);
   if (sMailAdr.Zip[0] >= '0')
      vmemcpy(pOutbuf+OFF_M_ZIP, sMailAdr.Zip, SIZ_M_ZIP);
}

/***************************** Men_MergeSaleRec ******************************
 *
 *
 *****************************************************************************/

int Men_MergeSaleRec(char *pOutbuf, char *pSale)
{
   long     lCurSaleDt, lLstSaleDt, lPrice, lLastAmt, lDocNum, lBook;
   char     acTmp[32], acDocNum[64];

   MEN_HIST *pSaleRec = (MEN_HIST *)pSale;

#ifdef _DEBUG
   //if (!memcmp(pSaleRec->Apn, "00809504", 8))
   //   lPrice = 0;
#endif

   lPrice=0;
   lCurSaleDt = atoin(pSaleRec->Rec_Date, HSIZ_REC_DATE);
   lLstSaleDt = atoin(pOutbuf+OFF_SALE1_DT, SIZ_SALE1_DT);

   if (lCurSaleDt < lLstSaleDt || lCurSaleDt > lToday)
      return -1;

   lPrice = atoin(pSaleRec->Stamp_Amt, HSIZ_STAMP_AMT);
   if (lPrice > 0)
      lPrice = (long)((double)lPrice * SALE_FACTOR_100);
   if (lCurSaleDt == lLstSaleDt)
   {
      lLastAmt = atoin(pOutbuf+OFF_SALE1_AMT, SIZ_SALE1_AMT);
      if (!lPrice && lLastAmt > 0)
         return 0;
   } else
   {
      // Move sale2 to sale3
      memcpy(pOutbuf+OFF_SALE3_DOC, pOutbuf+OFF_SALE2_DOC, SIZ_SALE3_DOC);
      memcpy(pOutbuf+OFF_SALE3_DT, pOutbuf+OFF_SALE2_DT, SIZ_SALE3_DT);
      memcpy(pOutbuf+OFF_SALE3_AMT, pOutbuf+OFF_SALE2_AMT, SIZ_SALE3_AMT);
      *(pOutbuf+OFF_AR_CODE3) = *(pOutbuf+OFF_AR_CODE2);

      // Move sale1 to sale2
      memcpy(pOutbuf+OFF_SALE2_DOC, pOutbuf+OFF_SALE1_DOC, SIZ_SALE3_DOC);
      memcpy(pOutbuf+OFF_SALE2_DT, pOutbuf+OFF_SALE1_DT, SIZ_SALE3_DT);
      memcpy(pOutbuf+OFF_SALE2_AMT, pOutbuf+OFF_SALE1_AMT, SIZ_SALE3_AMT);
      *(pOutbuf+OFF_AR_CODE2) = *(pOutbuf+OFF_AR_CODE1);
   }

   // Update current sale
   //lDocNum = atoin(pSaleRec->Deed_No, HSIZ_DEED_NO);
   //sprintf(acDocNum, "%*d", HSIZ_DEED_NO, lDocNum);

   lDocNum = atoin(pSaleRec->Deed_No, HSIZ_DEED_NO);
   lBook   = atoin(pSaleRec->Rec_Book, HSIZ_REC_BOOK);
   if (lDocNum > 0)
   {
      //sprintf(acDocNum, "%d          ", lDocNum);
      //memcpy(pOutbuf+OFF_SALE1_DOC, acDocNum, SIZ_SALE1_DOC);
      sprintf(acDocNum, "%.*s            ", HSIZ_DEED_NO, pSaleRec->Deed_No);
      memcpy(pOutbuf+OFF_SALE1_DOC, acDocNum, SIZ_SALE1_DOC);
   } else if (lBook > 0)
   {
      lDocNum = lBook;
      sprintf(acDocNum, "%d %.*s            ", lBook, HSIZ_REC_PAGE, pSaleRec->Rec_Page);
      memcpy(pOutbuf+OFF_SALE1_DOC, acDocNum, SIZ_SALE1_DOC);
   } else
   {
      acDocNum[0] = 0;
      memset(pOutbuf+OFF_SALE1_DOC, ' ', SIZ_SALE1_DOC);
   }

   if (isValidYMD(pSaleRec->Rec_Date))
      memcpy(pOutbuf+OFF_SALE1_DT, pSaleRec->Rec_Date, SIZ_SALE1_DT);
   else
      memset(pOutbuf+OFF_SALE1_DT, ' ', SIZ_SALE1_DT);

   if (lPrice > 5000)
   {
      sprintf(acTmp, "%*d", SIZ_SALE1_AMT, lPrice);
      memcpy(pOutbuf+OFF_SALE1_AMT, acTmp, SIZ_SALE1_AMT);
   } else
      memset(pOutbuf+OFF_SALE1_AMT, ' ', SIZ_SALE1_AMT);

   // Update transfers
   lLstSaleDt = atoin(pOutbuf+OFF_TRANSFER_DT, SIZ_TRANSFER_DT);
   if (lCurSaleDt >= lLstSaleDt)
   {
      if (lDocNum > 0)
         memcpy(pOutbuf+OFF_TRANSFER_DOC, acDocNum, SIZ_TRANSFER_DOC);
      else
         memset(pOutbuf+OFF_TRANSFER_DOC, ' ', SIZ_TRANSFER_DOC);

      if (isValidYMD(pSaleRec->Rec_Date))
         memcpy(pOutbuf+OFF_TRANSFER_DT, pSaleRec->Rec_Date, SIZ_TRANSFER_DT);
      else
         memset(pOutbuf+OFF_TRANSFER_DT, ' ', SIZ_TRANSFER_DT);
   }

   *(pOutbuf+OFF_AR_CODE1) = 'A';
   return 1;
}

/********************************* Men_MergeSale ******************************
 *
 *
 ******************************************************************************/

int Men_MergeSale(char *pOutbuf)
{
   static   char  acRec[1024], *pRec=NULL;
   int      iRet, iLoop;
   MEN_HIST *pSale;

   // Get first Sale rec for first call
   if (!pRec && !lSaleMatch)
      pRec = fgets(acRec, 1024, fdSale);
   pSale = (MEN_HIST *)&acRec[0];

   do
   {
      // Compare Apn
      iLoop = memcmp(pOutbuf, pSale->Apn, HSIZ_APN);
      if (iLoop > 0)
      {
         if (bDebug)
            LogMsg0("Skip Sale rec  %.*s", HSIZ_APN, pSale->Apn);
         pRec = fgets(acRec, 1024, fdSale);
         if (!pRec)
         {
            fclose(fdSale);
            fdSale = NULL;
            return 1;      // EOF
         }
         lSaleSkip++;
      }
   } while (iLoop > 0);

   if (iLoop)
      return 1;

   do
   {
      // Only take record type 'A' or 'P'
      if (pSale->Type[0] == 'A' || pSale->Type[0] == 'P')
         iRet = Men_MergeSaleRec(pOutbuf, acRec);

      pRec = fgets(acRec, 1024, fdSale);
      if (!pRec)
      {
         fclose(fdSale);
         fdSale = NULL;
         break;
      }
   } while (!memcmp(pOutbuf, pSale->Apn, HSIZ_APN));

   lSaleMatch++;

   return 0;
}

/********************************* Men_AddSales *******************************
 *
 * Add sale record to Secured roll file
 *
 ******************************************************************************/

int Men_AddSales(char *pOutbuf)
{
   static   char  acRec[1024], *pRec=NULL;
   int      iLoop, iCnt, iTmp;
   MEN_AHIST *pSale = (MEN_AHIST *)&acRec[0];
   MEN_ASEC  *pASec = (MEN_ASEC *)pOutbuf;

   // Get first Sale rec for first call
   if (!pRec)
   {
      // Skip first rec
      iTmp = fread(acRec, 1, iCSalLen, fdSale);
      iTmp = fread(acRec, 1, iCSalLen, fdSale);
      if (iTmp != iCSalLen)
      {
         fclose(fdSale);
         fdSale = NULL;
         return 1;      // EOF
      }
      pRec = acRec;
   }

   do
   {
      // Compare Apn
      iLoop = memcmp(pASec->Apn, pSale->Apn, HSIZ_APN);
      if (iLoop > 0)
      {
         iTmp = fread(acRec, 1, iCSalLen, fdSale);
         if (iTmp != iCSalLen)
         {
            fclose(fdSale);
            fdSale = NULL;
            break;      // EOF
         }
         lSaleSkip++;
      }
   } while (iLoop > 0);

   if (iLoop)
      return 1;

   iCnt = 0;
   do
   {
      // Only take record type 'A' or 'P'
      if (pSale->Type[0] == 'A' || pSale->Type[0] == 'P')
         memcpy((void *)&pASec->Sales[iCnt++], pSale->Rec_Book, sizeof(MEN_SALE));

      iTmp = fread(acRec, 1, iCSalLen, fdSale);
      if (iTmp != iCSalLen)
      {
         fclose(fdSale);
         fdSale = NULL;
         break;      // EOF
      }
   } while (!memcmp(pASec->Apn, pSale->Apn, HSIZ_APN) && iCnt < 3);

   lSaleMatch++;

   return 0;
}

/******************************** Men_XlatUseCode ****************************
 *
 * Return pointer to the correct use code.
 *
 *****************************************************************************

char *Men_XlatUseCode(char *pCntyUse)
{
   int   iTmp;

   iTmp = 0;
   while (Men_UseTbl[iTmp].acCntyUse[0])
      if (!memcmp(Men_UseTbl[iTmp].acCntyUse, pCntyUse, 2))
         return (char *)&Men_UseTbl[iTmp].acStdUse[0];
      else
         iTmp++;

   return NULL;
}

/******************************** Men_MergeRoll ******************************
 *
 * Return 0 if successful, >0 if error
 *
 *****************************************************************************/

int Men_MergeRoll(char *pOutbuf, char *pRollRec)
{
   MEN_ROLL *pRec;
   char     acTmp[256], acTmp1[256];
   int      iRet, iTmp;
   long     lTmp, lNewTrans;

   pRec = (MEN_ROLL *)pRollRec;

   // Clear output buffer
   memset(pOutbuf, ' ', iRecLen);

   // Start copying data
   memcpy(pOutbuf, pRec->Apn, RSIZ_APN);

   // Format APN
   iRet = formatApn(pRec->Apn, acTmp, &myCounty);
   memcpy(pOutbuf+OFF_APN_D, acTmp, iRet);

   // Create MapLink and output new record
   iRet = formatMapLink(pRec->Apn, acTmp, &myCounty);
   memcpy(pOutbuf+OFF_MAPLINK, acTmp, iRet);

   // Create index map link
   if (getIndexPage(acTmp, acTmp1, &myCounty))
      memcpy(pOutbuf+OFF_IMAPLINK, acTmp1, iRet);

   memcpy(pOutbuf+OFF_CO_NUM, "23MEN", 6);

   if (pRec->Status == 'U')
   {
      *(pOutbuf+OFF_STATUS) = 'O';
      *(pOutbuf+OFF_FULL_EXEMPT) = 'Y';
   } else if (pRec->Status == 'P')
      *(pOutbuf+OFF_STATUS) = 'P';
   else
      *(pOutbuf+OFF_STATUS) = 'A';

   // Year assessed
   memcpy(pOutbuf+OFF_YR_ASSD, myCounty.acYearAssd, 4);

   // Land
   long lLand = atoin(pRec->Land, RSIZ_LAND);
   if (lLand > 0)
   {
      sprintf(acTmp, "%*d", SIZ_LAND, lLand);
      memcpy(pOutbuf+OFF_LAND, acTmp, SIZ_LAND);
   }

   // Improve
   long lImpr = atoin(pRec->Structure, RSIZ_STRUCTURE);
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*d", SIZ_IMPR, lImpr);
      memcpy(pOutbuf+OFF_IMPR, acTmp, SIZ_IMPR);
   }

   long lPers = atoin(pRec->Pers_Prop, RSIZ_PERS_PROP);
   long lTree = atoin(pRec->Trees, RSIZ_TREES);
   long lFixt = atoin(pRec->Trade_Fixt, RSIZ_TRADE_FIXT);
   lTmp = lPers + lFixt + lTree;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_OTHER, lTmp);
      memcpy(pOutbuf+OFF_OTHER, acTmp, SIZ_OTHER);

      if (lFixt > 0)
      {
         sprintf(acTmp, "%d         ", lFixt);
         memcpy(pOutbuf+OFF_FIXTR, acTmp, SIZ_FIXTR);
      }
      if (lTree > 0)
      {
         sprintf(acTmp, "%d         ", lTree);
         memcpy(pOutbuf+OFF_TREEVINES, acTmp, SIZ_TREEVINES);
      }
      if (lPers > 0)
      {
         sprintf(acTmp, "%d         ", lPers);
         memcpy(pOutbuf+OFF_PERSPROP, acTmp, SIZ_PERSPROP);
      }
   }

   // Gross total
   lTmp += (lLand+lImpr);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_GROSS, lTmp);
      memcpy(pOutbuf+OFF_GROSS, acTmp, SIZ_GROSS);
   }

   // Ratio
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*d", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
      memcpy(pOutbuf+OFF_RATIO, acTmp, SIZ_RATIO);
   }

   lLand = atoin(pRec->Williamson_Land, RSIZ_LAND);
   if (lLand > 0)
   {
      sprintf(acTmp, "%d         ", lLand);
      memcpy(pOutbuf+OFF_CLCA_LAND, acTmp, SIZ_CLCA_LAND);
   }
   lTree = atoin(pRec->Williamson_Trees, RSIZ_TREES);
   if (lTree > 0)
   {
      sprintf(acTmp, "%d         ", lTree);
      memcpy(pOutbuf+OFF_CLCA_IMPR, acTmp, SIZ_CLCA_IMPR);
   }

   // HO Exempt
   long lExe = atoin(pRec->Homeowner_Exemp, RSIZ_HOMEOWNER_EXEMP);
   if (lExe > 0)
      *(pOutbuf+OFF_HO_FL) = '1';      // 'Y'
   else
      *(pOutbuf+OFF_HO_FL) = '2';      // 'N'

   lExe += atoin(pRec->Other_Exemp, RSIZ_OTHER_EXEMP);
   if (lExe > 0)
   {
      sprintf(acTmp, "%*d", SIZ_EXE_TOTAL, lExe);
      memcpy(pOutbuf+OFF_EXE_TOTAL, acTmp, SIZ_EXE_TOTAL);
   }

   // Exempt code
   *(pOutbuf+OFF_EXE_CD1) = pRec->Exemp_Code[0];

   // Full exemption
   if (pRec->Status == 'U')
      *(pOutbuf+OFF_FULL_EXEMPT) = 'Y';

   // Prop 8
   if (pRec->Prop8_Flag == 'Y')
      *(pOutbuf+OFF_PROP8_FLG) = 'Y';

   // TRA
   lTmp = atoin(pRec->Tra, RSIZ_TRA);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%.6d", lTmp);
      memcpy(pOutbuf+OFF_TRA, acTmp, 6);
   }

   // Zoning - remove leading '0', '-', and blank
   // Ignore overwritten data from previous field
   if (memcmp(pRec->Zoning, "EMENT", 5))
   {
      memcpy(acTmp, pRec->Zoning, RSIZ_ZONING);
      iRet = blankRem(acTmp, RSIZ_ZONING);
      if (iRet > 0)
      {
         memcpy(pOutbuf+OFF_ZONE, acTmp, iRet);
         if (*(pOutbuf+OFF_ZONE_X1) == ' ')
            memcpy(pOutbuf+OFF_ZONE_X1, acTmp, iRet);
      }
   }

   // Usecode
   memcpy(pOutbuf+OFF_USE_CO, pRec->Use_Code_1, RSIZ_USE_CODE_1);

   // Standard usecode - Ignore first 2 digits
   memcpy(acTmp, &pRec->Use_Code_1[2], 2);
   acTmp[2] = 0;
   // Check for special cases
   if (pRec->Use_Code_1[0] == 'A' || (pRec->Use_Code_1[2] == 'A' && pRec->Use_Code_1[3] == 'G'))
   {
      // Ag Preserved
      if (pRec->Use_Code_1[1] == 'P')
         memcpy(pOutbuf+OFF_USE_STD, USE_AGP, 3);
      else
      {
         // AN = Ag Nonrenewal, AG = Tree & Vine market
         //memcpy(pOutbuf+OFF_USE_STD, USE_MISC, 3);
         memcpy(pOutbuf+OFF_USE_STD, USE_AG, 3);
      }
   } else if (acTmp[0] >= '0')
   {
      iRet = updateStdUse(pOutbuf+OFF_USE_STD, acTmp, 2, pOutbuf);
      if (!iRet)
      {
         // If failed, use the first 2 digits - Paul's email 3/16/2012
         memcpy(acTmp, &pRec->Use_Code_1[0], 2);
         acTmp[2] = 0;
         iTmp = atol(acTmp);
         if (!memcmp(acTmp, "06", 2) || iTmp == 67)
            memcpy(pOutbuf+OFF_USE_STD, USE_REC, 3);
         else if (iTmp == 13 || iTmp == 15 || acTmp[0] == 'C')
            memcpy(pOutbuf+OFF_USE_STD, USE_COMM, 3);
         else if (acTmp[0] == 'R')
            memcpy(pOutbuf+OFF_USE_STD, USE_RES, 3);
         else
            iRet = updateStdUse(pOutbuf+OFF_USE_STD, acTmp, 2, pOutbuf);
      }
   } else
      memcpy(pOutbuf+OFF_USE_STD, USE_UNASGN, SIZ_USE_STD);

#ifdef _DEBUG
   //if (!memcmp(pRec->Apn, "0195000570", 10))
   //   lTmp = 0;
#endif

   // Transfer
   memcpy(acTmp, pRec->Rec_Date, RSIZ_REC_DATE);
   acTmp[RSIZ_REC_DATE] = 0;
   lNewTrans = atoi(acTmp);
   lTmp = atoin(pOutbuf+OFF_TRANSFER_DT, 8);
   if (lNewTrans > lTmp && isValidYMD(acTmp))
   {
      memcpy(pOutbuf+OFF_TRANSFER_DT, acTmp, SIZ_TRANSFER_DT);

      long lDocNum = atoin(pRec->Deed_No, RSIZ_DEED_NO);
      long lBook   = atoin(pRec->Rec_Book, RSIZ_REC_BOOK);
      char acDocNum[32];
      if (lDocNum > 0)
      {
         sprintf(acDocNum, "%d            ", lDocNum);
         memcpy(pOutbuf+OFF_TRANSFER_DOC, acDocNum, SIZ_TRANSFER_DOC);
      } else if (lBook > 0)
      {
         sprintf(acDocNum, "%d %.*s          ", lBook, RSIZ_REC_PAGE, pRec->Rec_Page);
         memcpy(pOutbuf+OFF_TRANSFER_DOC, acDocNum, SIZ_TRANSFER_DOC);
      } else
         memset(pOutbuf+OFF_TRANSFER_DOC, ' ', SIZ_TRANSFER_DOC);

   }

   double dAcres = atofn(pRec->Acres, RSIZ_ACRES);
   if (dAcres > 0.0)
   {
      sprintf(acTmp, "%*d", SIZ_LOT_ACRES, (long)(dAcres*10));
      memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);
      lTmp = (long)(dAcres * SQFT_FACTOR_100);
      sprintf(acTmp, "%*d", SIZ_LOT_SQFT, lTmp);
      memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);
   }

   Men_MergeOwner(pOutbuf, pRollRec);

   Men_MergeMAdr(pOutbuf, pRollRec);

   Men_MergeSAdr(pOutbuf, pRollRec);

   // 01/19/2018
   if (pRec->MultiApn_Flag[0] == 'A')
      *(pOutbuf+OFF_MULTI_APN) = 'Y';
   else if (*(pOutbuf+OFF_MULTI_APN) == 'Y')
      *(pOutbuf+OFF_MULTI_APN) = 'N';

   return 0;

}

/******************************** Men_Load_LDR ******************************
 *
 * Create roll file then sort by APN.
 *
 ****************************************************************************/

int Men_Load_LDR(int iFirstRec)
{
   char     *pTmp, acBuf[MAX_RECSIZE], acRollRec[MAX_RECSIZE];
   char     acOutFile[_MAX_PATH];

   HANDLE   fhOut;

   int      iRet, iRollSkip;
   DWORD    nBytesWritten;
   BOOL     bRet, bEof;
   long     lRet=0, lCnt=0;

   // Open roll file
   LogMsg("Open roll file %s", acRollFile);
   fdRoll = fopen(acRollFile, "r");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", acRollFile);
      return 2;
   }

   // Open Sale file
   //LogMsg("Open Sale file %s", acSaleFile);
   //fdSale = fopen(acSaleFile, "r");
   //if (fdSale == NULL)
   //{
   //   LogMsg("***** Error opening sale file: %s\n", acSaleFile);
   //   return 2;
   //}

   // Open Output file
   sprintf(acOutFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "R01");
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);
   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening output file: %s\n", acOutFile);
      return 4;
   }

   // Output first header record
   if (iFirstRec > 0)
   {
      memset(acBuf, '9', iRecLen);
      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
   }

   pTmp = fgets((char *)&acRollRec[0], MAX_RECSIZE, fdRoll);
   bEof = (pTmp ? false:true);

   // Init variables
   iRollSkip=iNoMatch=iBadCity=iBadSuffix=0;

   // Merge loop
   while (pTmp)
   {
      lCnt++;
      if (acRollRec[0] != 'R')
      {
         iRet = Men_MergeRoll(acBuf, acRollRec);

         // Merge Sale
         //if (fdSale)
         //   iRet = Men_MergeSale(acBuf);

         // Save last recording date
         lRet = atoin((char *)&acBuf[OFF_TRANSFER_DT], 8);
         if (lRet > lLastRecDate && lRet < lToday)
            lLastRecDate = lRet;

         bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
         if (!(++lLDRRecCount % 1000))
            printf("\r%u", lLDRRecCount);

         if (!bRet)
         {
            LogMsg("***** Error writing to output file at record %d\n", lCnt);
            lRet = WRITE_ERR;
            break;
         }
      } else
         iRollSkip++;

      pTmp = fgets(acRollRec, MAX_RECSIZE, fdRoll);
   }

   // Close files
   if (fdRoll)
      fclose(fdRoll);
   //if (fdSale)
   //   fclose(fdSale);
   if (fhOut)
      CloseHandle(fhOut);

   // Sort output file
   /*
   sprintf(acOutFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "R01");
   LogMsg("Sort output file %s to %s", acTmpFile, acOutFile);
   sprintf(acBuf, "S(1,%d,C,A) F(FIX,%d) B(%d,R)", RSIZ_APN, iRecLen, iRecLen);
   lRet = sortFile(acTmpFile, acOutFile, acBuf);
   if (lRet <= 0)
   {
      LogMsg("***** ERROR sorting roll output file %s to %s [%s]", acTmpFile, acOutFile, acBuf);
      iRet = -1;
   } else
      iRet = remove(acTmpFile);
   */

   LogMsg("Total input records:        %u", lCnt);
   LogMsg("Total output records:       %u", lLDRRecCount);
   LogMsg("Total skipped records:      %u", iRollSkip);
   LogMsg("Total bad-city records:     %u", iBadCity);
   LogMsg("Total bad-suffix records:   %u", iBadSuffix);
   LogMsg("Last recording date:        %u", lLastRecDate);
   printf("\nTotal output records: %u\n", lCnt);

   lRecCnt = lCnt;
   return 0;
}

/********************************* Men_MergeLien *****************************
 *
 * Return 0 if successful, >0 if error
 *
 *****************************************************************************/

void Men_MergeLienRec(char *pOutbuf, char *pLienRec)
{
   LIENEXTR *pLien = (LIENEXTR *)pLienRec;

   // H/O exempt
   *(pOutbuf+OFF_HO_FL) = pLien->acHO[0];
   memcpy(pOutbuf+OFF_EXE_TOTAL, pLien->acExAmt, SIZ_EXE_TOTAL);

   // Land
   memcpy(pOutbuf+OFF_LAND, pLien->acLand, SIZ_LAND);

   // Improve
   memcpy(pOutbuf+OFF_IMPR, pLien->acImpr, SIZ_IMPR);

   // Others
   memcpy(pOutbuf+OFF_OTHER, pLien->acOther, SIZ_OTHER);

   // Gross
   memcpy(pOutbuf+OFF_GROSS, pLien->acGross, SIZ_GROSS);

   // Ratio
   memcpy(pOutbuf+OFF_RATIO, pLien->acRatio, SIZ_RATIO);

   // Other value
   long lTmp = atoin(pLien->acME_Val, sizeof(pLien->acME_Val));
   char acTmp[32];
   if (lTmp > 0)
   {
      sprintf(acTmp, "%d         ", lTmp);
      memcpy(pOutbuf+OFF_FIXTR, acTmp, SIZ_FIXTR);
   }
   lTmp = atoin(pLien->acPP_Val, sizeof(pLien->acPP_Val));
   if (lTmp > 0)
   {
      sprintf(acTmp, "%d         ", lTmp);
      memcpy(pOutbuf+OFF_PERSPROP, acTmp, SIZ_PERSPROP);
   }
   lTmp = atoin(pLien->extra.Men.acTV_Val, sizeof(pLien->extra.Men.acTV_Val));
   if (lTmp > 0)
   {
      sprintf(acTmp, "%d         ", lTmp);
      memcpy(pOutbuf+OFF_TREEVINES, acTmp, SIZ_TREEVINES);
   }
   lTmp = atoin(pLien->extra.Men.acWil_Land, sizeof(pLien->extra.Men.acWil_Land));
   if (lTmp > 0)
   {
      sprintf(acTmp, "%d         ", lTmp);
      memcpy(pOutbuf+OFF_CLCA_LAND, acTmp, SIZ_CLCA_LAND);
   }
   lTmp = atoin(pLien->extra.Men.acWil_TV, sizeof(pLien->extra.Men.acWil_TV));
   if (lTmp > 0)
   {
      sprintf(acTmp, "%d         ", lTmp);
      memcpy(pOutbuf+OFF_CLCA_IMPR, acTmp, SIZ_CLCA_IMPR);
   }
}

int Men_MergeLien(char *pOutbuf)
{
   static   char  acRec[1024], *pRec=NULL;
   int      iLoop;
   LIENEXTR *pLienRec;

   // Get first Sale rec for first call
   if (!pRec)
      pRec = fgets(acRec, 1024, fdLien);
   pLienRec = (LIENEXTR *)&acRec[0];

   do
   {
      // Compare Apn
      iLoop = memcmp(pOutbuf, pLienRec->acApn, RSIZ_APN);
      if (iLoop > 0)
      {
         if (bDebug)
            LogMsg0("Skip Lien rec  %.*s", RSIZ_APN, pLienRec->acApn);
         pRec = fgets(acRec, 1024, fdLien);
         if (!pRec)
         {
            fclose(fdLien);
            fdLien = NULL;
            return 1;      // EOF
         }
         lLienSkip++;
      }
   } while (iLoop > 0);

   if (iLoop)
      return 1;

   Men_MergeLienRec(pOutbuf, acRec);

   pRec = fgets(acRec, 1024, fdLien);
   if (!pRec)
   {
      fclose(fdLien);
      fdLien = NULL;
   }

   lLienMatch++;

   return 0;
}

/********************************* Men_Load_Roll ****************************
 *
 * This county reload the whole file every time.  So don't create update file.
 *
 ****************************************************************************/

int Men_Load_Roll(int iSkip)
{
   char     *pTmp, acBuf[MAX_RECSIZE], acRollRec[MAX_RECSIZE];
   char     acOutFile[_MAX_PATH], acLienExtr[_MAX_PATH];

   HANDLE   fhOut;

   int      iRet;
   DWORD    nBytesWritten;
   BOOL     bRet, bEof;
   long     lRet=0, lCnt=0, lTmp;

   // Open roll file
   LogMsg("Open roll file %s", acRollFile);
   fdRoll = fopen(acRollFile, "r");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", acRollFile);
      return -2;
   }

   // Open Lien extract file
   sprintf(acLienExtr, acLienTmpl, myCounty.acCntyCode, myCounty.acCntyCode);
   LogMsg("Open Lien extract file %s", acLienExtr);
   fdLien = fopen(acLienExtr, "r");
   if (fdLien == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", acLienExtr);
      return -2;
   }

   // Open Output file
   sprintf(acOutFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "R01");
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);
   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening output file: %s\n", acOutFile);
      return -4;
   }

   // Output first header record
   if (iSkip > 0)
   {
      memset(acBuf, '9', iRecLen);
      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
   }

   pTmp = fgets((char *)&acRollRec[0], MAX_RECSIZE, fdRoll);
   bEof = (pTmp ? false:true);

   // Init variables
   iNoMatch=iBadCity=iBadSuffix=0;

   // Merge loop
   while (pTmp)
   {
      if (acRollRec[0] != 'R')
      {
         iRet = Men_MergeRoll(acBuf, acRollRec);

         // Merge Lien data
         if (fdLien)
            iRet = Men_MergeLien(acBuf);

         // Save last recording date
         lTmp = atoin((char *)&acBuf[OFF_TRANSFER_DT], 8);
         if (lTmp > lLastRecDate && lTmp < lToday)
            lLastRecDate = lTmp;

         bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
         if (!(++lCnt % 1000))
            printf("\r%u", lCnt);

         if (!bRet)
         {
            LogMsg("***** Error writing to output file at record %d\n", lCnt);
            lRet = WRITE_ERR;
            break;
         }
      }
      pTmp = fgets(acRollRec, MAX_RECSIZE, fdRoll);
   }
   // Close files
   if (fdRoll)
      fclose(fdRoll);
   if (fdLien)
      fclose(fdLien);
   if (fhOut)
      CloseHandle(fhOut);

   LogMsg("Total output records:       %u", lCnt);
   LogMsg("      Lien recs matched:    %u", lLienMatch);
   LogMsg("      Lien recs skipped:    %u", lLienSkip);
   LogMsg("      Bad mail addresses:   %u", lUnkMailAdr);
   LogMsg("      bad-city records:     %u", iBadCity);
   LogMsg("      bad-suffix records:   %u", iBadSuffix);
   LogMsg("Last recording date:        %u", lLastRecDate);
   printf("\nTotal output records: %u\n", lCnt);

   lRecCnt = lCnt;

   return lRet;
}

/******************************* Men_CreateLienRec ***************************
 *
 * Return 0 if successful, >0 if error
 *
 *****************************************************************************/

int Men_CreateLienRec(char *pOutbuf, char *pRollRec)
{
   char     acTmp[256];
   long     lTmp;
   int      iTmp;
   MEN_ROLL *pRec = (MEN_ROLL *)pRollRec;
   LIENEXTR *pLienRec = (LIENEXTR *)pOutbuf;

   // Clear output buffer
   memset(pOutbuf, ' ', sizeof(LIENEXTR));

   // Start copying data
   memcpy(pLienRec->acApn, pRec->Apn, RSIZ_APN);
   memcpy(pLienRec->acTRA, pRec->Tra, RSIZ_TRA);

   // Year assessed
   memcpy(pLienRec->acYear, myCounty.acYearAssd, 4);

   // Land
   long lLand = atoin(pRec->Land, RSIZ_LAND);
   if (lLand > 0)
   {
      iTmp = sprintf(acTmp, "%*d", sizeof(pLienRec->acLand), lLand);
      memcpy(pLienRec->acLand, acTmp, iTmp);
   }

   // Improve - Ratio
   long lImpr = atoin(pRec->Structure, RSIZ_STRUCTURE);
   if (lImpr > 0)
   {
      iTmp = sprintf(acTmp, "%*d", sizeof(pLienRec->acImpr), lImpr);
      memcpy(pLienRec->acImpr, acTmp, iTmp);

      iTmp = sprintf(acTmp, "%*d", sizeof(pLienRec->acRatio), (LONGLONG)lImpr*100/(lLand+lImpr));
      memcpy(pLienRec->acRatio, acTmp, iTmp);
   }

   // Other values
   long lPers = atoin(pRec->Pers_Prop, RSIZ_PERS_PROP);
   long lFixt = atoin(pRec->Trade_Fixt, RSIZ_PERS_PROP);
   long lTree = atoin(pRec->Trees, RSIZ_TREES);

   // Total other
   long lOther = lPers + lTree + lFixt;
   if (lOther > 0)
   {
      iTmp = sprintf(acTmp, "%*d", sizeof(pLienRec->acGross), lOther);
      memcpy(pLienRec->acOther, acTmp, iTmp);

      if (lFixt > 0)
      {
         iTmp = sprintf(acTmp, "%*d", sizeof(pLienRec->acME_Val), lFixt);
         memcpy(pLienRec->acME_Val, acTmp, iTmp);
      }

      if (lPers > 0)
      {
         iTmp = sprintf(acTmp, "%*d", sizeof(pLienRec->acPP_Val), lPers);
         memcpy(pLienRec->acPP_Val, acTmp, iTmp);
      }

      if (lTree > 0)
      {
         iTmp = sprintf(acTmp, "%*d", sizeof(pLienRec->extra.Men.acTV_Val), lTree);
         memcpy(pLienRec->extra.Men.acTV_Val, acTmp, iTmp);
      }
   }

   // Gross
   lTmp = lOther + lLand + lImpr;
   if (lTmp > 0)
   {
      iTmp = sprintf(acTmp, "%*d", sizeof(pLienRec->acGross), lTmp);
      memcpy(pLienRec->acGross, acTmp, iTmp);
   }

   // Williamson values
   lLand = atoin(pRec->Williamson_Land, RSIZ_WILLIAMSON_LAND);
   if (lLand > 0)
   {
      iTmp = sprintf(acTmp, "%*d", sizeof(pLienRec->extra.Men.acWil_Land), lLand);
      memcpy(pLienRec->extra.Men.acWil_Land, acTmp, iTmp);
   }

   lTree = atoin(pRec->Williamson_Trees, RSIZ_WILLIAMSON_TREES);
   if (lTree > 0)
   {
      iTmp = sprintf(acTmp, "%*d", sizeof(pLienRec->extra.Men.acWil_TV), lTree);
      memcpy(pLienRec->extra.Men.acWil_TV, acTmp, iTmp);
   }

   // HO Exempt
   long lExe = atoin(pRec->Homeowner_Exemp, RSIZ_HOMEOWNER_EXEMP);
   if (lExe > 0)
      pLienRec->acHO[0] = '1';         // Y
   else
      pLienRec->acHO[0] = '2';         // N

   lTmp = atoin(pRec->Other_Exemp, RSIZ_OTHER_EXEMP);
   if (lTmp > 0)
   {
      iTmp = sprintf(acTmp, "%*d", sizeof(pLienRec->extra.Men.acOtherExe), lTmp);
      memcpy(pLienRec->extra.Men.acOtherExe, acTmp, iTmp);
   }

   lExe += lTmp;
   if (lExe > 0)
   {
      iTmp = sprintf(acTmp, "%*d", sizeof(pLienRec->acExAmt), lExe);
      memcpy(pLienRec->acExAmt, acTmp, iTmp);
   }

   // Exe values: 4=HO, 6
   //if (pRec->Exemp_Code[0] == 'H')
   //   pLienRec->acExCode[0] = '4';
   //else
      pLienRec->acExCode[0] = pRec->Exemp_Code[0];

   // Full exemption
   if (pRec->Status == 'U')
      pLienRec->SpclFlag = LX_FULLEXE_FLG;

   // Prop 8
   if (pRec->Prop8_Flag == 'Y')
      pLienRec->SpclFlag |= LX_PROP8_FLG;

   pLienRec->LF[0] = '\n';
   pLienRec->LF[1] = 0;
   return 0;
}

/******************************** Men_ExtrProp8 *****************************
 *
 *
 ****************************************************************************/

int Men_ExtrProp8()
{
   char  *pTmp, acBuf[256], acRollRec[MAX_RECSIZE], acOutFile[_MAX_PATH];
   long  lCnt=0, iProp8Cnt=0;
   FILE  *fdOut;

   LogMsg("\nExtract Prop8 flag from lien roll %s", acRollFile);

   // Open roll file
   fdRoll = fopen(acRollFile, "r");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", acRollFile);
      return 2;
   }

   // Open Output file
   sprintf(acOutFile, acProp8Tmpl, myCounty.acCntyCode, myCounty.acCntyCode, myCounty.acYearAssd);
   LogMsg("Create prop8 output file %s", acOutFile);
   fdOut = fopen(acOutFile, "w");
   if (fdOut == NULL)
   {
      LogMsg("***** Error opening Prop8 extract file: %s\n", acOutFile);
      return 4;
   }

   // print header
   fputs("APN,Prop8_Flg\n", fdOut);

   // Merge loop
   while (!feof(fdRoll))
   {
      // Get roll rec
      pTmp = fgets(acRollRec, MAX_RECSIZE, fdRoll);
      if (!pTmp)
         break;

      if (acRollRec[0] != 'R')
      {
         // Prop8
         if (acRollRec[ROFF_PROP8_FLAG] == 'Y')
         {
            acRollRec[ROFF_APN+RSIZ_APN] = 0;
            sprintf(acBuf, "%s,Y\n", &acRollRec[ROFF_APN]);

            fputs(acBuf, fdOut);
            iProp8Cnt++;
         }
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdRoll)
      fclose(fdRoll);
   if (fdOut)
      fclose(fdOut);

   LogMsgD("\nTotal prop8 records output: %u", iProp8Cnt);
   LogMsgD("Total records processed:    %u\n", lCnt);

   return 0;
}

/********************************* Men_ExtrLien *****************************
 *
 *
 ****************************************************************************/

int Men_ExtrLien()
{
   char     *pTmp, acBuf[MAX_RECSIZE], acRollRec[MAX_RECSIZE];
   char     acOutFile[_MAX_PATH];
   long     lCnt, lRead;

   // Open roll file
   LogMsg("Open Roll file %s", acRollFile);
   fdRoll = fopen(acRollFile, "r");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", acRollFile);
      return -1;
   }

   // Open Output file
   sprintf(acOutFile, acLienTmpl, myCounty.acCntyCode, myCounty.acCntyCode);
   LogMsg("Open output file %s", acOutFile);
   fdLien = fopen(acOutFile, "w");
   if (fdLien == NULL)
   {
      LogMsg("***** Error opening Lien extract file: %s\n", acOutFile);
      return -2;
   }

   // Merge loop
   lCnt=lRead=0;
   while (!feof(fdRoll))
   {
      // Get roll rec
      pTmp = fgets(acRollRec, MAX_RECSIZE, fdRoll);
      if (!pTmp)
         break;

      // Create new lien record
      if (acRollRec[0] != 'R')
      {      
         Men_CreateLienRec(acBuf, acRollRec);
         fputs(acBuf, fdLien);
         lCnt++;
      }
      if (!(++lRead % 1000))
         printf("\r%u", lRead);
   }

   // Close files
   if (fdRoll)
      fclose(fdRoll);
   if (fdLien)
      fclose(fdLien);

   LogMsg("Total records processed:    %d", lRead);
   LogMsg("         records output:    %d\n", lCnt);

   lRecCnt = lCnt;
   return 0;
}

/*************************** Men_ExtrRollCorrection *************************
 *
 * Extract values from roll correction file and replace the Lien_Exp.MEN
 *
 ****************************************************************************/

int Men_CreateRollCorrectionRec(char *pOutbuf, char *pRollRec)
{
   char     acTmp[256];
   long     lTmp;
   int      iRet, iTmp;
   LIENEXTR *pLienRec = (LIENEXTR *)pOutbuf;

   // Clear output buffer
   memset(pOutbuf, ' ', sizeof(LIENEXTR));

   // Parse input string
   iRet = ParseStringNQ(pRollRec, ',', MAX_FLD_TOKEN, apTokens);
   if (iRet < MEN_COR_FLDS)
   {
      LogMsg("***** Men_CreateRollCorrectionRec: bad input record for APN=%s", apTokens[0]);
      return -1;
   }

   // Start copying data
   vmemcpy(pLienRec->acApn, apTokens[MEN_COR_AIN], iApnLen);

   // TRA
   lTmp = atol(apTokens[MEN_COR_TAG]);
   if (lTmp > 0)
   {
      iTmp = sprintf(acTmp, "%.6d", lTmp);
      memcpy(pLienRec->acTRA, acTmp, iTmp);
   }

   // Year assessed
   vmemcpy(pLienRec->acYear, apTokens[MEN_COR_TAXYEAR], 4);

   // Land
   long lLand = dollar2Num(apTokens[MEN_COR_ASSDLAND]);
   if (lLand > 0)
   {
      iTmp = sprintf(acTmp, "%*u", sizeof(pLienRec->acLand), lLand);
      memcpy(pLienRec->acLand, acTmp, iTmp);
   }

   // Improve
   long lImpr = dollar2Num(apTokens[MEN_COR_ASSDIMP]);
   if (lImpr > 0)
   {
      iTmp = sprintf(acTmp, "%*u", sizeof(pLienRec->acImpr), lImpr);
      memcpy(pLienRec->acImpr, acTmp, iTmp);

      iTmp = sprintf(acTmp, "%*d", sizeof(pLienRec->acRatio), (LONGLONG)lImpr*100/(lLand+lImpr));
      memcpy(pLienRec->acRatio, acTmp, iTmp);
   }

   long lPers = dollar2Num(apTokens[MEN_COR_ASSDPERSONAL]);
   long lTree = dollar2Num(apTokens[MEN_COR_ASSDLIVIMP]);
   long lFixt = dollar2Num(apTokens[MEN_COR_ASSDFIXTURES]);
   long lOther= lPers + lFixt + lTree;
   if (lOther > 0)
   {
      iTmp = sprintf(acTmp, "%*u", sizeof(pLienRec->acOther), lOther);
      memcpy(pLienRec->acOther, acTmp, iTmp);

      if (lFixt > 0)
      {
         iTmp = sprintf(acTmp, "%*u", sizeof(pLienRec->acME_Val), lFixt);
         memcpy(pLienRec->acME_Val, acTmp, iTmp);
      }

      if (lPers > 0)
      {
         iTmp = sprintf(acTmp, "%*u", sizeof(pLienRec->acPP_Val), lPers);
         memcpy(pLienRec->acPP_Val, acTmp, iTmp);
      }

      if (lTree > 0)
      {
         iTmp = sprintf(acTmp, "%*u", sizeof(pLienRec->extra.Men.acTV_Val), lTree);
         memcpy(pLienRec->extra.Men.acTV_Val, acTmp, iTmp);
      }
   }

   // Gross total
   long lGross = dollar2Num(apTokens[MEN_COR_ASSESSEDFULL]);
   if (lGross > 0)
   {
      iTmp = sprintf(acTmp, "%*u", sizeof(pLienRec->acGross), lGross);
      memcpy(pLienRec->acGross, acTmp, iTmp);
   }

   // HO Exempt
   long lExe = dollar2Num(apTokens[MEN_COR_HOX]);
   if (lExe > 0)
      pLienRec->acHO[0] = '1';         // Y
   else
      pLienRec->acHO[0] = '2';         // N

   lTmp = dollar2Num(apTokens[MEN_COR_OTHEREXMPT]);
   if (lTmp > 0)
   {
      iTmp = sprintf(acTmp, "%*u", sizeof(pLienRec->extra.Men.acOtherExe), lTmp);
      memcpy(pLienRec->extra.Men.acOtherExe, acTmp, iTmp);
   }

   lExe = dollar2Num(apTokens[MEN_COR_TOTALEXMPT]);
   if (lExe > 0)
   {
      iTmp = sprintf(acTmp, "%*u", sizeof(pLienRec->acExAmt), lExe);
      memcpy(pLienRec->acExAmt, acTmp, iTmp);
   }

   // Full exemption
   if (lExe > 0 && lExe >= lGross)
      pLienRec->SpclFlag = LX_FULLEXE_FLG;

   pLienRec->LF[0] = '\n';
   pLienRec->LF[1] = 0;
   return 0;
}

int Men_ExtrRollCorrection()
{
   char     *pTmp, acBuf[MAX_RECSIZE], acRollRec[MAX_RECSIZE];
   char     acOutFile[_MAX_PATH];
   long     lCnt, lRead;

   // Open roll file
   LogMsg("Open Roll file %s", acRollFile);
   fdRoll = fopen(acRollFile, "r");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", acRollFile);
      return -1;
   }

   // Open Output file
   sprintf(acOutFile, acLienTmpl, myCounty.acCntyCode, myCounty.acCntyCode);
   LogMsg("Open output file %s", acOutFile);
   fdLien = fopen(acOutFile, "w");
   if (fdLien == NULL)
   {
      LogMsg("***** Error opening Lien extract file: %s\n", acOutFile);
      return -2;
   }

   // Drop header
   pTmp = fgets(acRollRec, MAX_RECSIZE, fdRoll);

   // Merge loop
   lCnt=lRead=0;
   while (!feof(fdRoll))
   {
      // Get roll rec
      pTmp = fgets(acRollRec, MAX_RECSIZE, fdRoll);
      if (!pTmp)
         break;

      // Create new lien record
      if (memcmp(acRollRec, "0000", 4) > 0)
      {
         Men_CreateRollCorrectionRec(acBuf, acRollRec);
         fputs(acBuf, fdLien);
         lCnt++;
      }
      if (!(lCnt % 1000))
         printf("\r%u", lRead);
   }

   // Close files
   if (fdRoll)
      fclose(fdRoll);
   if (fdLien)
      fclose(fdLien);

   LogMsg("Total records output:    %d\n", lCnt);

   return 0;
}

/****************************** Men_Create_AHist ****************************
 *
 *
 ****************************************************************************/

int Men_Create_AHist()
{
   char     *pTmp, acBuf[128], acInrec[128];
   char     acOutFile[_MAX_PATH];
   int      iRet;
   double   dTmp;
   long     lTmp, lCnt=0;

   FILE        *fdOut;
   MEN_HIST    *pHist  = (MEN_HIST *)acInrec;
   MEN_AHIST   *pAHist = (MEN_AHIST *)acBuf;

   // Open History file
   LogMsg("Open History file %s", acSaleFile);
   fdSale = fopen(acSaleFile, "r");
   if (fdSale == NULL)
   {
      LogMsg("***** Error History file: %s\n", acSaleFile);
      return -2;
   }

   // Open Output file
   GetIniString(myCounty.acCntyCode, "AsrHFile", "", acOutFile, _MAX_PATH, acIniFile);
   iCSalLen = sizeof(MEN_AHIST);
   LogMsg("Creating Assr History file %s", acOutFile);
   fdOut = fopen(acOutFile, "wb");
   if (fdOut == NULL)
   {
      LogMsg("***** Error creating Assr History file: %s\n", acOutFile);
      return -4;
   }

   memset(acBuf, '9', iCSalLen);
   iRet = fwrite(acBuf, 1, iCSalLen, fdOut);

   // Merge loop
   while (!feof(fdSale))
   {
      // Get roll rec
      pTmp = fgets(acInrec, MAX_RECSIZE, fdSale);
      if (!pTmp)
         break;

      // Create new record
      memcpy(acBuf, acInrec, AHOFF_PER_OWNERSHIP);

      // Reformat Rec Bk/Pg
      //lTmp = atoin(pHist->Rec_Book, HSIZ_REC_BOOK+HSIZ_REC_PAGE);
      //if (!lTmp)
      //   memset(pAHist->Rec_Book, '0', HSIZ_REC_BOOK+HSIZ_REC_PAGE);

      // Reformat deed_no
      lTmp = atoin(pHist->Deed_No, HSIZ_DEED_NO);
      if (lTmp > 0)
      {
         //sprintf(acTmp, "%*d", AHSIZ_DEED_NO, lTmp);
         //memcpy(pAHist->Deed_No, acTmp, AHSIZ_DEED_NO);
         memcpy(pAHist->Deed_No, pHist->Deed_No, HSIZ_DEED_NO);
      } else
         memset(pAHist->Deed_No, ' ', AHSIZ_DEED_NO);

      // Reformat Pct_Own
      lTmp = atoin(pHist->Pct_Own, HSIZ_PER_OWNERSHIP);
      if (lTmp > 0)
      {
         dTmp = (double)lTmp/100.0;
         sprintf(pAHist->Pct_Own, "%*.2f", AHSIZ_PER_OWNERSHIP, dTmp);
      } else
         memset(pAHist->Pct_Own, ' ', AHSIZ_PER_OWNERSHIP);

      // Reformat Stamp_Amt
      lTmp = atoin(pHist->Stamp_Amt, HSIZ_STAMP_AMT);
      if (lTmp > 0)
      {
         dTmp = (double)lTmp/100.0;
         sprintf(pAHist->Stamp_Amt, "%*.2f", AHSIZ_STAMP_AMT, dTmp);

         // Sale Amt
         lTmp = (long)(lTmp * SALE_FACTOR_100);
         sprintf(pAHist->SaleAmt, "%*d", AHSIZ_SALE_AMT, lTmp);
      } else
      {
         memset(pAHist->Stamp_Amt, ' ', AHSIZ_STAMP_AMT);
         memset(pAHist->SaleAmt, ' ', AHSIZ_SALE_AMT);
      }

      pAHist->Dtt_Code[0] = pHist->Dtt_Code[0];

      memset(pAHist->Filler, ' ', AHSIZ_FILLER);

      iRet = fwrite(acBuf, 1, iCSalLen, fdOut);
      if (iRet != iCSalLen)
      {
         LogMsg("***** Error writing output to history file (%d)", errno);
         break;
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdSale)
      fclose(fdSale);
   if (fdOut)
      fclose(fdOut);

   LogMsg("Total History records output:     %d\n", lCnt);
   printf("\nTotal History records output: %u\n", lCnt);

   return lCnt;
}

/****************************** Men_Create_AUns *****************************
 *
 *
 ****************************************************************************/

int Men_Create_AUns()
{
   char     *pTmp, acBuf[1024], acInrec[1024];
   char     acOutFile[_MAX_PATH];
   int      iLen, iRet;
   double   dTmp;
   long     lTmp, lCnt=0;

   FILE       *fdOut, *fdUns;
   MEN_UNS    *pUns  = (MEN_UNS *)acInrec;
   MEN_AUNS   *pAUns = (MEN_AUNS *)acBuf;

   // Open Unsecured file
   LogMsg("Open Unsecured file %s", acUnsRoll);
   fdUns = fopen(acUnsRoll, "r");
   if (fdUns == NULL)
   {
      LogMsg("***** Error opening Unsecured file: %s\n", acUnsRoll);
      return -2;
   }

   // Open Output file
   GetIniString(myCounty.acCntyCode, "AsrUFile", "", acOutFile, _MAX_PATH, acIniFile);
   iLen = sizeof(MEN_AUNS);
   LogMsg("Creating Assr Unsecured file %s", acOutFile);
   fdOut = fopen(acOutFile, "wb");
   if (fdOut == NULL)
   {
      LogMsg("***** Error creating Assr Unsecured file: %s\n", acOutFile);
      return -4;
   }

   memset(acBuf, '9', iLen);
   iRet = fwrite(acBuf, 1, iLen, fdOut);

   // Merge loop
   while (!feof(fdUns))
   {
      // Get roll rec
      pTmp = fgets(acInrec, MAX_RECSIZE, fdUns);
      if (!pTmp)
         break;

      // Create new record
      memcpy(acBuf, acInrec, AUOFF_TAX_RATE);

      // Reformat Land
      long lLand = atoin(pUns->Land, USIZ_LAND);
      if (lLand > 0)
         sprintf(pAUns->Land, "%*d", AUSIZ_LAND, lLand);
      else
         memset(pAUns->Land, ' ', AUSIZ_LAND);

      // Reformat Impr (Structure)
      long lImpr = atoin(pUns->Impr, USIZ_IMPR);
      if (lImpr > 0)
         sprintf(pAUns->Impr, "%*d", AUSIZ_IMPR, lImpr);
      else
         memset(pAUns->Impr, ' ', AUSIZ_IMPR);

      // Reformat Pers_Prop
      long lPers = atoin(pUns->Pers_Prop, USIZ_PERS_PROP);
      if (lPers > 0)
         sprintf(pAUns->Pers_Prop, "%*d", AUSIZ_PERS_PROP, lPers);
      else
         memset(pAUns->Pers_Prop, ' ', AUSIZ_PERS_PROP);

      // Reformat Fixture/Equiptment
      long lFixt = atoin(pUns->Trade_Fixt, USIZ_TRADE_FIXT);
      if (lFixt > 0)
         sprintf(pAUns->Trade_Fixt, "%*d", AUSIZ_TRADE_FIXT, lFixt);
      else
         memset(pAUns->Trade_Fixt, ' ', AUSIZ_TRADE_FIXT);

      // Reformat Exemption
      long lExe = atoin(pUns->Exemp, USIZ_EXEMP);
      if (lExe > 0)
         sprintf(pAUns->Exemp, "%*d", AUSIZ_EXEMP, lExe);
      else
         memset(pAUns->Exemp, ' ', AUSIZ_EXEMP);

      // Reformat Cost
      long lCost = atoin(pUns->Cost, USIZ_COST);
      if (lCost > 0)
         sprintf(pAUns->Cost, "%*d", AUSIZ_COST, lCost);
      else
         memset(pAUns->Cost, ' ', AUSIZ_COST);

      // Reformat Boat value
      long lBoat = atoin(pUns->Boat_Value, USIZ_BOAT_VALUE);
      if (lBoat > 0)
         sprintf(pAUns->Boat_Value, "%*d", AUSIZ_BOAT_VALUE, lBoat);
      else
         memset(pAUns->Boat_Value, ' ', AUSIZ_BOAT_VALUE);

      // Exemp code
      memcpy(pAUns->Exemp_Code, pUns->Exemp_Code, USIZ_EXEMP_CODE);

      // Reformat Tax rate
      lTmp = atoin(pUns->Tax_Rate, USIZ_TAX_RATE);
      if (lTmp > 0)
      {
         dTmp = (double)lTmp/100000.0;
         sprintf(pAUns->Tax_Rate, "%*.5f", AUSIZ_TAX_RATE, dTmp);
      } else
         memset(pAUns->Tax_Rate, ' ', AUSIZ_TAX_RATE);

      // Reformat Tax_Amt
      lTmp = atoin(pUns->Tax_Amt, USIZ_TAX_AMT);
      if (lTmp > 0)
      {
         dTmp = (double)lTmp/100.0;
         sprintf(pAUns->Tax_Amt, "%*.2f", AUSIZ_TAX_AMT, dTmp);
      } else
         memset(pAUns->Tax_Amt, ' ', AUSIZ_TAX_AMT);

      // Reformat Int rate
      lTmp = atoin(pUns->Int_Rate, USIZ_INT_RATE);
      if (lTmp > 0)
      {
         dTmp = (double)lTmp/100000.0;
         sprintf(pAUns->Int_Rate, "%*.5f", AUSIZ_INT_RATE, dTmp);
      } else
         memset(pAUns->Int_Rate, ' ', AUSIZ_INT_RATE);

      // Reformat Int_Amt
      lTmp = atoin(pUns->Int_Amt, USIZ_INT_AMT);
      if (lTmp > 0)
      {
         dTmp = (double)lTmp/100.0;
         sprintf(pAUns->Int_Amt, "%*.2f", AUSIZ_INT_AMT, dTmp);
      } else
         memset(pAUns->Int_Amt, ' ', AUSIZ_INT_AMT);

      // Reformat Bill number
      lTmp = atoin(pUns->Bill_Nu, AUSIZ_BILL_NU);
      if (lTmp > 0)
         sprintf(pAUns->Bill_Nu, "%*d", AUSIZ_BILL_NU, lTmp);
      else
         memset(pAUns->Bill_Nu, ' ', AUSIZ_BILL_NU);

      // Reformat Bill date - Cnvert MMDDYYYY to YYYYMMDD
      lTmp = atoin(pUns->Bill_Date, AUSIZ_BILL_DATE);
      if (lTmp > 0)
         sprintf(pAUns->Bill_Date, "%4s%.4s", &pUns->Bill_Date[4], pUns->Bill_Date);
      else
         memset(pAUns->Bill_Date, ' ', AUSIZ_BILL_DATE);

      // Reformat Maint date
      lTmp = atoin(pUns->Maint_Date, AUSIZ_BILL_DATE);
      if (lTmp > 0)
         sprintf(pAUns->Maint_Date, "%4s%.4s", &pUns->Maint_Date[4], pUns->Maint_Date);
      else
         memset(pAUns->Maint_Date, ' ', AUSIZ_BILL_DATE);

      // Reformat Statement date
      lTmp = atoin(pUns->Statement_Date, AUSIZ_BILL_DATE);
      if (lTmp > 0)
         sprintf(pAUns->Statement_Date, "%.4s%.4s", &pUns->Statement_Date[4], pUns->Statement_Date);
      else
         memset(pAUns->Statement_Date, ' ', AUSIZ_BILL_DATE);

      // Copy Exemp & StrName
      memcpy(pAUns->Exemp_Percent, pUns->Exemp_Percent, AUOFF_STREET_NUMBER-AUOFF_EXEMP_PERCENT);

      // Reformat StrNum
      lTmp = atoin(pUns->Street_Number, USIZ_STREET_NUMBER);
      if (lTmp > 0)
         sprintf(pAUns->Street_Number, "%*d", AUSIZ_STREET_NUMBER, lTmp);
      else
         memset(pAUns->Street_Number, ' ', AUSIZ_STREET_NUMBER);

      // Copy more input data as is
      memcpy(pAUns->Unit, pUns->Unit, AUOFF_BPS_DATE-AUOFF_UNIT);

      // Reformat BPS date
      lTmp = atoin(pUns->Bps_Date, AUSIZ_BILL_DATE);
      if (lTmp > 0)
         sprintf(pAUns->Bps_Date, "%4s%.4s", &pUns->Bps_Date[4], pUns->Bps_Date);
      else
         memset(pAUns->Bps_Date, ' ', AUSIZ_BILL_DATE);

      // Copy Phone
      memcpy(pAUns->Phone, pUns->Phone, AUSIZ_PHONE);
      memset(pAUns->Filler1, ' ', AUSIZ_FILLER1);

      // Calculation
      // Gross
      lTmp = lLand+lImpr+lPers+lFixt;
      if (lTmp > 0)
         sprintf(pAUns->Gross, "%*d", AUSIZ_GROSS, lTmp);
      else
         memset(pAUns->Gross, ' ', AUSIZ_GROSS);

      // Net
      lTmp -= lExe;
      if (lTmp > 0)
         sprintf(pAUns->Net, "%*d", AUSIZ_NET, lTmp);
      else
         memset(pAUns->Net, ' ', AUSIZ_NET);

      // Land+Impr
      lTmp = lLand+lImpr;
      if (lTmp > 0)
         sprintf(pAUns->LandImpr, "%*d", AUSIZ_LAND_IMPR, lTmp);
      else
         memset(pAUns->LandImpr, ' ', AUSIZ_LAND_IMPR);

      // LI Ratio
      if (lImpr > 0)
         sprintf(pAUns->Ratio, "%*d", AUSIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
      else
         memset(pAUns->Ratio, ' ', AUSIZ_RATIO);

      // Reset filler
      memset(pAUns->Filler2, ' ', AUSIZ_FILLER2);

      iRet = fwrite(acBuf, 1, iLen, fdOut);
      if (iRet != iLen)
      {
         LogMsg("***** Error writing output to Assr Unsecured file (%d)", errno);
         break;
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdUns)
      fclose(fdUns);
   if (fdOut)
      fclose(fdOut);

   LogMsg("Total Unsecured records output:     %d\n", lCnt);
   printf("\nTotal Unsecured records output: %u\n", lCnt);

   return lCnt;
}

/****************************** Men_Create_ASec *****************************
 *
 *
 ****************************************************************************/

int Men_Create_ASec()
{
   char     *pTmp, acBuf[MAX_RECSIZE], acInrec[MAX_RECSIZE];
   char     acOutFile[_MAX_PATH], acHistFile[_MAX_PATH], acTmp[128];
   int      iLen, iRet;
   double   dTmp;
   long     lTmp, lCnt=0;

   FILE       *fdOut;
   MEN_ROLL   *pSec  = (MEN_ROLL *)acInrec;
   MEN_ASEC   *pASec = (MEN_ASEC *)acBuf;

   // Open History file
   GetIniString(myCounty.acCntyCode, "AsrHFile", "", acHistFile, _MAX_PATH, acIniFile);
   iCSalLen = sizeof(MEN_AHIST);
   LogMsg("Open History file %s", acHistFile);
   fdSale = fopen(acHistFile, "rb");
   if (fdSale == NULL)
   {
      LogMsg("***** Error History file: %s\n", acHistFile);
      return -2;
   }
   lSaleMatch = lSaleSkip = 0;

   // Open Secured Roll file
   LogMsg("Open Secured Roll file %s", acRollFile);
   fdRoll = fopen(acRollFile, "r");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening Secured Roll file: %s\n", acRollFile);
      return -2;
   }

   // Open Output file
   GetIniString(myCounty.acCntyCode, "AsrSFile", "", acOutFile, _MAX_PATH, acIniFile);
   iLen = sizeof(MEN_ASEC);
   LogMsg("Creating Assr Secured file %s", acOutFile);
   fdOut = fopen(acOutFile, "wb");
   if (fdOut == NULL)
   {
      LogMsg("***** Error creating Assr Unsecured file: %s\n", acOutFile);
      return -4;
   }

   memset(acBuf, '9', iLen);
   iRet = fwrite(acBuf, 1, iLen, fdOut);

   // Merge loop
   while (!feof(fdRoll))
   {
      // Get roll rec
      pTmp = fgets(acInrec, MAX_RECSIZE, fdRoll);
      if (!pTmp)
         break;

      // Create new record
      memcpy(acBuf, acInrec, AROFF_FLAGS);

      // StrNum
      lTmp = atoin(pSec->Situs_Street_Number, ARSIZ_SITUS_STREET_NUMBER);
      if (lTmp > 0)
      {
         sprintf(acTmp, "%*d", ARSIZ_SITUS_STREET_NUMBER, lTmp);
         memcpy(pASec->Situs_Street_Number, acTmp, ARSIZ_SITUS_STREET_NUMBER);
      } else
         memset(pASec->Situs_Street_Number, ' ', ARSIZ_SITUS_STREET_NUMBER);

      // Reformat Land
      long lLand = atoin(pSec->Land, RSIZ_LAND);
      if (lLand > 0)
         sprintf(pASec->Land, "%*d", ARSIZ_LAND, lLand);
      else
         memset(pASec->Land, ' ', ARSIZ_LAND);

      // Reformat Impr (Structure)
      long lImpr = atoin(pSec->Structure, RSIZ_STRUCTURE);
      if (lImpr > 0)
         sprintf(pASec->Structure, "%*d", ARSIZ_STRUCTURE, lImpr);
      else
         memset(pASec->Structure, ' ', ARSIZ_STRUCTURE);

      // Reformat Tree value
      long lTrees = atoin(pSec->Trees, RSIZ_TREES);
      if (lTrees > 0)
         sprintf(pASec->Trees, "%*d", ARSIZ_TREES, lTrees);
      else
         memset(pASec->Trees, ' ', ARSIZ_TREES);

      // Reformat Fixture/Equiptment
      long lFixt = atoin(pSec->Trade_Fixt, RSIZ_TRADE_FIXT);
      if (lFixt > 0)
         sprintf(pASec->Trade_Fixt, "%*d", ARSIZ_TRADE_FIXT, lFixt);
      else
         memset(pASec->Trade_Fixt, ' ', ARSIZ_TRADE_FIXT);

      // Reformat Pers_Prop
      long lPers = atoin(pSec->Pers_Prop, RSIZ_PERS_PROP);
      if (lPers > 0)
         sprintf(pASec->Pers_Prop, "%*d", ARSIZ_PERS_PROP, lPers);
      else
         memset(pASec->Pers_Prop, ' ', ARSIZ_PERS_PROP);

      // Reformat Exemption
      long lExe = atoin(pSec->Homeowner_Exemp, RSIZ_HOMEOWNER_EXEMP);
      if (lExe > 0)
         sprintf(pASec->Homeowner_Exemp, "%*d", ARSIZ_HOMEOWNER_EXEMP, lExe);
      else
         memset(pASec->Homeowner_Exemp, ' ', ARSIZ_HOMEOWNER_EXEMP);

      lTmp = atoin(pSec->Other_Exemp, RSIZ_OTHER_EXEMP);
      if (lTmp > 0)
      {
         sprintf(pASec->Other_Exemp, "%*d", ARSIZ_OTHER_EXEMP, lTmp);
         lExe += lTmp;
      } else
         memset(pASec->Other_Exemp, ' ', ARSIZ_OTHER_EXEMP);

      // Williamson Land
      long lWilLand = atoin(pSec->Williamson_Land, RSIZ_WILLIAMSON_LAND);
      if (lWilLand > 0)
         sprintf(pASec->Williamson_Land, "%*d", ARSIZ_WILLIAMSON_LAND, lWilLand);
      else
         memset(pASec->Williamson_Land, ' ', ARSIZ_WILLIAMSON_LAND);

      // Williamson Tree
      long lWilTrees = atoin(pSec->Williamson_Trees, RSIZ_WILLIAMSON_TREES);
      if (lWilTrees > 0)
      {
         sprintf(acTmp, "%*d", ARSIZ_WILLIAMSON_TREES, lWilTrees);
         memcpy(pASec->Williamson_Trees, acTmp, ARSIZ_WILLIAMSON_TREES);
      } else
         memset(pASec->Williamson_Trees, ' ', ARSIZ_WILLIAMSON_TREES);

      if (pASec->Msg_Code_1[0] == '0') pASec->Msg_Code_1[0] = ' ';
      if (pASec->Msg_Code_2[0] == '0') pASec->Msg_Code_2[0] = ' ';

      // Rec Bk/Pg
      lTmp = atoin(pSec->Rec_Book, RSIZ_REC_BOOK);
      if (!lTmp)
         memset(pASec->Rec_Book, ' ', ARSIZ_REC_BOOK);

      // Reformat deed_no
      lTmp = atoin(pSec->Deed_No, RSIZ_DEED_NO);
      if (lTmp > 0)
      {
         //sprintf(acTmp, "%*d", ARSIZ_DEED_NO, lTmp);
         //memcpy(pASec->Deed_No, acTmp, ARSIZ_DEED_NO);
         memcpy(pASec->Deed_No, pSec->Deed_No, RSIZ_DEED_NO);
      } else
         memset(pASec->Deed_No, ' ', ARSIZ_DEED_NO);

      // Reformat Maint date
      lTmp = atoin(pSec->Maint_Date, ARSIZ_MAINT_DATE);
      if (lTmp > 0)
      {
         sprintf(acTmp, "%.4s%.4s", &pSec->Maint_Date[4], pSec->Maint_Date);
         memcpy(pASec->Maint_Date, acTmp, ARSIZ_MAINT_DATE);
      } else
         memset(pASec->Maint_Date, ' ', ARSIZ_MAINT_DATE);

      // Sqft 1, 2, 3
      lTmp = atoin(pSec->Sqft_1, ARSIZ_SQFT_1);
      if (lTmp > 0)
         sprintf(pASec->Sqft_1, "%*d", ARSIZ_SQFT_1, lTmp);
      else
         memset(pASec->Sqft_1, ' ', ARSIZ_SQFT_1);
      lTmp = atoin(pSec->Sqft_2, ARSIZ_SQFT_1);
      if (lTmp > 0)
         sprintf(pASec->Sqft_2, "%*d", ARSIZ_SQFT_1, lTmp);
      else
         memset(pASec->Sqft_2, ' ', ARSIZ_SQFT_1);
      lTmp = atoin(pSec->Sqft_3, ARSIZ_SQFT_1);
      if (lTmp > 0)
         sprintf(pASec->Sqft_3, "%*d", ARSIZ_SQFT_1, lTmp);
      else
         memset(pASec->Sqft_3, ' ', ARSIZ_SQFT_1);

      // Etal flag
      pASec->Etal_Flag[0] = pSec->Etal_Flag[0];

      // Reformat prior Land
      lTmp = atoin(pSec->Prior_Land, RSIZ_LAND);
      if (lTmp > 0)
         sprintf(pASec->Prior_Land, "%*d", ARSIZ_LAND, lTmp);
      else
         memset(pASec->Prior_Land, ' ', ARSIZ_LAND);

      // Reformat prior Impr (Structure)
      lTmp = atoin(pSec->Prior_Struc, RSIZ_STRUCTURE);
      if (lTmp > 0)
         sprintf(pASec->Prior_Struc, "%*d", ARSIZ_STRUCTURE, lTmp);
      else
         memset(pASec->Prior_Struc, ' ', ARSIZ_STRUCTURE);

      // Reformat prior Tree value
      lTmp = atoin(pSec->Prior_Trees, RSIZ_TREES);
      if (lTmp > 0)
         sprintf(pASec->Prior_Trees, "%*d", ARSIZ_TREES, lTmp);
      else
         memset(pASec->Prior_Trees, ' ', ARSIZ_TREES);

      // Reformat prior Fixture/Equiptment
      lTmp = atoin(pSec->Prior_Trade_Fixt, RSIZ_TRADE_FIXT);
      if (lTmp > 0)
         sprintf(pASec->Prior_Trade_Fixt, "%*d", ARSIZ_TRADE_FIXT, lTmp);
      else
         memset(pASec->Prior_Trade_Fixt, ' ', ARSIZ_TRADE_FIXT);

      // Reformat prior Pers_Prop
      lTmp = atoin(pSec->Prior_Pers_Prop, RSIZ_PERS_PROP);
      if (lTmp > 0)
         sprintf(pASec->Prior_Pers_Prop, "%*d", ARSIZ_PERS_PROP, lTmp);
      else
         memset(pASec->Prior_Pers_Prop, ' ', ARSIZ_PERS_PROP);

      // Reformat prior Exemption
      lTmp = atoin(pSec->Prior_Homeowner_Exemp, RSIZ_HOMEOWNER_EXEMP);
      if (lTmp > 0)
         sprintf(pASec->Prior_Homeowner_Exemp, "%*d", ARSIZ_HOMEOWNER_EXEMP, lTmp);
      else
         memset(pASec->Prior_Homeowner_Exemp, ' ', ARSIZ_HOMEOWNER_EXEMP);

      lTmp = atoin(pSec->Prior_Other_Exemp, RSIZ_OTHER_EXEMP);
      if (lTmp > 0)
      {
         sprintf(acTmp, "%*d", ARSIZ_OTHER_EXEMP, lTmp);
         memcpy(pASec->Prior_Other_Exemp, acTmp, ARSIZ_OTHER_EXEMP);
      } else
         memset(pASec->Prior_Other_Exemp, ' ', ARSIZ_OTHER_EXEMP);

      if (pASec->Prior_Msg_Code_1[0] == '0') pASec->Prior_Msg_Code_1[0] = ' ';
      if (pASec->Prior_Msg_Code_2[0] == '0') pASec->Prior_Msg_Code_2[0] = ' ';

      // Reformat appraisal dates
      if (pSec->Appr_Open_Dt_1[0] > ' ')
      {
         memcpy(pASec->Appr_Open_Dt_1, &pSec->Appr_Open_Dt_1[4], 4);
         memcpy(&pASec->Appr_Open_Dt_1[4], pSec->Appr_Open_Dt_1, 4);
      }
      if (pSec->Appr_Close_Dt_1[0] > ' ')
      {
         memcpy(pASec->Appr_Close_Dt_1, &pSec->Appr_Close_Dt_1[4], 4);
         memcpy(&pASec->Appr_Close_Dt_1[4], pSec->Appr_Close_Dt_1, 4);
      }
      if (pSec->Appr_Open_Dt_2[0] > ' ')
      {
         memcpy(pASec->Appr_Open_Dt_2, &pSec->Appr_Open_Dt_2[4], 4);
         memcpy(&pASec->Appr_Open_Dt_2[4], pSec->Appr_Open_Dt_2, 4);
      }
      if (pSec->Appr_Close_Dt_2[0] > ' ')
      {
         memcpy(pASec->Appr_Close_Dt_2, &pSec->Appr_Close_Dt_2[4], 4);
         memcpy(&pASec->Appr_Close_Dt_2[4], pSec->Appr_Close_Dt_2, 4);
      }
      if (pSec->Appr_Open_Dt_3[0] > ' ')
      {
         memcpy(pASec->Appr_Open_Dt_3, &pSec->Appr_Open_Dt_3[4], 4);
         memcpy(&pASec->Appr_Open_Dt_3[4], pSec->Appr_Open_Dt_3, 4);
      }
      if (pSec->Appr_Close_Dt_3[0] > ' ')
      {
         memcpy(pASec->Appr_Close_Dt_3, &pSec->Appr_Close_Dt_3[4], 4);
         memcpy(&pASec->Appr_Close_Dt_3[4], pSec->Appr_Close_Dt_3, 4);
      }
      if (pSec->Appr_Open_Dt_4[0] > ' ')
      {
         memcpy(pASec->Appr_Open_Dt_4, &pSec->Appr_Open_Dt_4[4], 4);
         memcpy(&pASec->Appr_Open_Dt_4[4], pSec->Appr_Open_Dt_4, 4);
      }
      if (pSec->Appr_Close_Dt_4[0] > ' ')
      {
         memcpy(pASec->Appr_Close_Dt_4, &pSec->Appr_Close_Dt_4[4], 4);
         memcpy(&pASec->Appr_Close_Dt_4[4], pSec->Appr_Close_Dt_4, 4);
      }

      // Sqft 4
      lTmp = atoin(pSec->Sqft_4, ARSIZ_SQFT_1);
      if (lTmp > 0)
      {
         sprintf(acTmp, "%*d", ARSIZ_SQFT_1, lTmp);
         memcpy(pASec->Sqft_4, acTmp, ARSIZ_SQFT_1);
      } else
         memset(pASec->Sqft_4, ' ', ARSIZ_SQFT_1);

      if (pASec->Bd_Ex_Cd_1[0] == '0') pASec->Bd_Ex_Cd_1[0] = ' ';
      if (pASec->Bd_Ex_Cd_2[0] == '0') pASec->Bd_Ex_Cd_2[0] = ' ';

      // Base year value seq
      lTmp = atoin(pSec->Base_Year_Value_Sequence, RSIZ_BASE_YEAR_VALUE_SEQUENCE);
      if (lTmp > 0)
      {
         sprintf(acTmp, "%*d", ARSIZ_BASE_YEAR_VALUE_SEQUENCE, lTmp);
         memcpy(pASec->Base_Year_Value_Sequence, acTmp, ARSIZ_BASE_YEAR_VALUE_SEQUENCE);
      } else
         memset(pASec->Base_Year_Value_Sequence, ' ', ARSIZ_BASE_YEAR_VALUE_SEQUENCE);

      // Base tree/vine seq
      lTmp = atoin(pSec->Tv_Seq, RSIZ_TV_SEQ);
      if (lTmp > 0)
      {
         sprintf(acTmp, "%*d", ARSIZ_TV_SEQ, lTmp);
         memcpy(pASec->Tv_Seq, acTmp, ARSIZ_TV_SEQ);
      } else
         memset(pASec->Tv_Seq, ' ', ARSIZ_TV_SEQ);

      // CSHTST
      lTmp = atoin(pSec->Cshtst, RSIZ_CSHTST);
      if (lTmp > 0)
      {
         sprintf(acTmp, "%*d", ARSIZ_CSHTST, lTmp);
         memcpy(pASec->Cshtst, acTmp, ARSIZ_CSHTST);
      } else
         memset(pASec->Cshtst, ' ', ARSIZ_CSHTST);

      // Ag_Preserve_No
      lTmp = atoin(pSec->Ag_Preserve_No, RSIZ_AG_PRESERVE_NO);
      if (lTmp > 0)
      {
         sprintf(acTmp, "%*d", ARSIZ_AG_PRESERVE_NO, lTmp);
         memcpy(pASec->Ag_Preserve_No, acTmp, ARSIZ_AG_PRESERVE_NO);
      } else
         memset(pASec->Ag_Preserve_No, ' ', ARSIZ_AG_PRESERVE_NO);

      // ---------------- End of county supply data -----------------------

      memset(pASec->Flags, ' ', sizeof(MEN_ASEC)-AROFF_FLAGS+1);

      // Gross
      long lGross = lLand+lImpr+lPers+lFixt+lTrees;
      if (lGross > 0)
         sprintf(pASec->Gross, "%*d", ARSIZ_GROSS, lGross);

      // Total Exemp
      if (lExe > 0)
         sprintf(pASec->TotalExe, "%*d", ARSIZ_TOTAL_EXE, lExe);

      // Net
      lTmp = lGross - lExe;
      if (lTmp > 0)
         sprintf(pASec->Net, "%*d", ARSIZ_NET, lTmp);

      // Land+Impr
      lTmp = lLand+lImpr;
      if (lTmp > 0)
         sprintf(pASec->LandImpr, "%*d", ARSIZ_LAND_IMPR, lTmp);

      // LI Ratio
      if (lImpr > 0)
         sprintf(pASec->LI_Ratio, "%*d", ARSIZ_LI_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));

      // Land+Impr+Trees
      lTmp += lTrees;
      if (lTmp > 0)
         sprintf(pASec->LandImprTree, "%*d", ARSIZ_LAND_IMPR_TREE, lTmp);

      // LIT Ratio
      if (lTmp > 0)
         sprintf(pASec->LIT_Ratio, "%*d", ARSIZ_LIT_RATIO, (LONGLONG)lTmp*100/(lLand+lTmp));

      // Lot Acres V99
      lTmp = atoin(pSec->Acres, RSIZ_ACRES);
      if (lTmp > 0)
      {
         dTmp = (double)lTmp/100.0;
         sprintf(pASec->LotAcres, "%*.2f", ARSIZ_LOTACRES, dTmp);

         long lLotSqft = (long)((double)lTmp * SQFT_FACTOR_100);
         sprintf(pASec->LotSqft, "%*d", ARSIZ_LOTSQFT, lLotSqft);

         // Land value per Acres

         // Land value per Sqft
      }

      // Add Sale recs
      if (fdSale)
         iRet = Men_AddSales(acBuf);

      // Reset filler
      //memset(pASec->Filler, ' ', ARSIZ_FILLER);

      iRet = fwrite(acBuf, 1, iLen, fdOut);
      if (iRet != iLen)
      {
         LogMsg("***** Error writing output to Assr Unsecured file (%d)", errno);
         break;
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdRoll)
      fclose(fdRoll);
   if (fdSale)
      fclose(fdSale);
   if (fdOut)
      fclose(fdOut);

   LogMsg("Total Secured records output:     %d\n", lCnt);
   printf("\nTotal Secured records output: %u\n", lCnt);

   return lCnt;
}

/********************************* Men_FormatSale ****************************
 *
 * Keep sale price if > 1.00
 *
 * Return -1 if bad record.  0 if OK.
 *
 *****************************************************************************/

int Men_FormatSale(char *pFmtSale, char *pSale)
{
   SCSAL_REC *pCSale = (SCSAL_REC *)pFmtSale;
   MEN_HIST  *pSaleRec=(MEN_HIST *) pSale;
   
   int		iTmp;
   char     acDocNum[32], acTmp[32];
   long     lPrice, lDocNum, lBook;

   if (*pSale < '0')
      return -1;

   // Only take record type 'A' or 'P'
   if (pSaleRec->Type[0] != 'A' && pSaleRec->Type[0] != 'P')
      return 1;

   memset(pFmtSale, ' ', sizeof(SCSAL_REC));

   // APN
   memcpy(pCSale->Apn, pSaleRec->Apn, iApnLen);

   // DocNum
   lDocNum = atoin(pSaleRec->Deed_No, HSIZ_DEED_NO);
   lBook   = atoin(pSaleRec->Rec_Book, HSIZ_REC_BOOK);
   if (lDocNum > 0)
   {
      memcpy(pCSale->DocNum, pSaleRec->Deed_No, HSIZ_DEED_NO);
   } else if (lBook > 0)
   {
      iTmp = sprintf(acDocNum, "%d %.*s", lBook, HSIZ_REC_PAGE, pSaleRec->Rec_Page);
      memcpy(pCSale->DocNum, acDocNum, iTmp);
   }

#ifdef _DEBUG
   //if (!memcmp("0504401300", pSaleRec->Apn, iApnLen))
   //   iTmp = 0;
#endif

   // DocDate
   if (isValidYMD(pSaleRec->Rec_Date, true, true, true))
   {
      memcpy(pCSale->DocDate, pSaleRec->Rec_Date, SIZ_SALE1_DT);
      iTmp = atoin(pSaleRec->Rec_Date, SIZ_SALE1_DT);
      if (iTmp > lLastRecDate)
         lLastRecDate = iTmp;
   }

   // Drop records without DocDate & DocNum
   if (pCSale->DocNum[0] == ' ' || pCSale->DocDate[0] == ' ')
      return -1;

   // DocType - 12/29/2021: P=Purchase
   if (pSaleRec->Type[0] == 'P')
      pCSale->DocType[0] = '1';

   // Sale Price
   lPrice = atoin(pSaleRec->Stamp_Amt, HSIZ_STAMP_AMT);
   if (lPrice >= 55)
   {
      iTmp = sprintf(acTmp, "%.2f", (double)lPrice/100.0);
      memcpy(pCSale->StampAmt, acTmp, iTmp);

      lPrice = (long)((double)lPrice * SALE_FACTOR_100);
      sprintf(acTmp, "%*d", SIZ_SALE1_AMT, lPrice);
      memcpy(pCSale->SalePrice, acTmp, SIZ_SALE1_AMT);
   } else if (lPrice > 0)
   {
      LogMsg("??? Questionable StampAmt APN= %.*s, StampAmt=%d", iApnLen, pSale, lPrice);
   }

   // Transferee
   memcpy(pCSale->Name1, pSaleRec->Name, HSIZ_NAME);

   // Percent own
   iTmp = atoin(pSaleRec->Pct_Own, 3);
   if (iTmp > 0 && lPrice > 0)
   {
      memcpy(pCSale->PctXfer, pSaleRec->Pct_Own, 3);
      if (iTmp == 100)
         pCSale->SaleCode[0] = 'F';
      else
         pCSale->SaleCode[0] = 'P';
      pCSale->SaleCode[1] = ' ';
   }

   // DocType

   pCSale->ARCode = 'A';
   pCSale->CRLF[0] = '\n';
   pCSale->CRLF[1] = 0;

   return 0;
}

/****************************** Men_ExtrSale *********************************
 *
 * Extract history sale.  
 *
 * Return 0 if successful, otherwise error
 *
 *****************************************************************************/

int Men_ExtrSale(void)
{
   char     *pTmp, acCSalRec[1024], acSaleRec[1024], acTmp[256];
   char     acTmpSale[_MAX_PATH];

   long     lCnt=0;
   int		iRet, iTmp, iUpdateSale=0;

   // Check current sale file
   if (_access(acSaleFile, 0))
   {
      LogMsg("***** Missing current sale file %s", acSaleFile);
      return -1;
   }

   // Open current sale
   LogMsg("Open current sale file %s", acSaleFile);
   fdSale = fopen(acSaleFile, "r");
   if (fdSale == NULL)
   {
      LogMsg("***** Error opening current sale file: %s\n", acSaleFile);
      return -2;
   }

   // Create output file
   sprintf(acTmpSale, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "tmp");
   LogMsg("Create updated sale file %s", acTmpSale);
   fdCSale = fopen(acTmpSale, "w");
   if (fdCSale == NULL)
   {
      LogMsg("***** Error creating updated sale file: %s\n", acTmpSale);
      return -3;
   }
   lLastRecDate = 0;
   setToday(0);

   // Merge loop
   while (!feof(fdSale))
   {

      // Get current sale
      pTmp = fgets(acSaleRec, 1024, fdSale);
      if (!pTmp)
         break;

      iRet = Men_FormatSale(acCSalRec, acSaleRec);
      if (!iRet)
      {
         fputs(acCSalRec, fdCSale);
         iUpdateSale++;
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdCSale)
      fclose(fdCSale);
   if (fdSale)
      fclose(fdSale);

   // Update sale history file
   sprintf(acCSalFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "sls");

   // Sort/merge sale file - APN (asc), Saledate (asc), DocNum (asc)
   sprintf(acTmp, "S(1,%d,C,A,27,8,C,A,15,%d,C,A,57,10,C,D) DUPO(1,34)", iApnLen, SALE_SIZ_DOCNUM);
   iTmp = sortFile(acTmpSale, acCSalFile, acTmp);

   LogMsg("Total sale records processed:  %u", lCnt);
   LogMsg("Total sale records updated:    %u", iUpdateSale);
   LogMsg("Total extracted sale records:  %u", iTmp);
   LogMsg("         Last recording date:  %u", lLastRecDate);

   LogMsg("Update Sale History completed.");
   printf("\nTotal extracted sale records:  %u\n", iTmp);

   return 0;
}

/***************************** Men_ParseTaxDetail ****************************
 *
 * Return 0 if success
 *
 *****************************************************************************/

int Men_ParseTaxDetail(FILE *fdDetail, FILE *fdAgency)
{
   char     acTmp[512], acOutbuf[512];
   int      iIdx;
   long     lTmp;
   TAXDETAIL *pDetail = (TAXDETAIL *)acOutbuf;
   TAXAGENCY *pResult, sAgency, *pAgency;

   // Clear output buffer
   memset(acOutbuf, 0, sizeof(TAXDETAIL));
   memset(&sAgency, 0, sizeof(TAXAGENCY));
   pAgency = &sAgency;

   // APN
   strcpy(pDetail->Apn, apTokens[MEN_SEC_APN]);

   // BillNumber
   strcpy(pDetail->BillNum, apTokens[MEN_SEC_BILL_NUM]);

   // Tax Year
   sprintf(pDetail->TaxYear, "%d", lTaxYear);

   iIdx = MEN_SEC_AS_CODE_1;
   while (*apTokens[iIdx] > ' ')
   {
      memset(&sAgency, 0, sizeof(TAXAGENCY));

      // Tax code
      strcpy(pDetail->TaxCode, apTokens[iIdx]);
      strcpy(pAgency->Code, apTokens[iIdx]);

      memset(pDetail->TaxRate, 0, sizeof(pDetail->TaxRate));
      pResult = findTaxAgency(pAgency->Code, 0);
      if (pResult)
      {
         strcpy(pAgency->Agency, pResult->Agency);
         strcpy(pAgency->Phone, pResult->Phone);
         pAgency->TC_Flag[0] = pResult->TC_Flag[0];
         pDetail->TC_Flag[0] = pResult->TC_Flag[0];

         // Tax Rate - Make sure we have latest tax rate table for the tax year
         if (pResult->TaxRate[0] > ' ')
         {
            strcpy(pAgency->TaxRate, pResult->TaxRate);
            strcpy(pDetail->TaxRate, pResult->TaxRate);
         }
      } else
      {
         pAgency->Agency[0] = 0;
         LogMsg("+++ Unknown TaxCode: %s APN=%s", pDetail->TaxCode, pDetail->Apn);
      }

      // Tax amt
      lTmp = atol(apTokens[iIdx+1]);
      lTmp += atol(apTokens[iIdx+2]);

      if (lTmp > 0)
         sprintf(pDetail->TaxAmt, "%.2f", (double)lTmp/100.0);
      else
         strcpy(pDetail->TaxAmt, "0");

      // Generate csv line and write to file
      Tax_CreateDetailCsv(acTmp, pDetail);
      fputs(acTmp, fdDetail);

      // Forming Agency record
      Tax_CreateAgencyCsv(acTmp, pAgency);
      fputs(acTmp, fdAgency);

      iIdx += 3;
   }

   return 0;
}

/***************************** Men_ParseTaxBase ******************************
 *
 *
 *****************************************************************************/

int Men_ParseTaxBase(char *pOutbuf, char *pInbuf)
{
   char     acTmp[256], *pTmp;
   int      iTmp;
   TAXBASE  *pOutRec = (TAXBASE *)pOutbuf;

   // Parse input tax data
   iTmp = ParseStringIQ(pInbuf, '|', MAX_FLD_TOKEN, apTokens);
   if (iTmp < MEN_SEC_BRUP)
   {
      LogMsg("***** Error: bad TaxBase record for APN=%.*s (#tokens=%d)", iApnLen, pInbuf, iTmp);
      return -1;
   }

   // Clear output buffer
   memset(pOutbuf, 0, sizeof(TAXBASE));

   // If canceled, ignore it
   if (*apTokens[MEN_SEC_I1_STAT] == 'C' || *apTokens[MEN_SEC_I2_STAT] == 'C')
   {
      if (!memcmp(apTokens[MEN_SEC_PAYOR], "CANCELLED", 6))
         LogMsg("+++ Keep %s reason %s", apTokens[MEN_SEC_APN], apTokens[MEN_SEC_PAYOR]);
      else if (*(apTokens[MEN_SEC_APN]+9) != 'N')
         return 1;
   }

   if (*(apTokens[MEN_SEC_APN]+9) == 'N')
   {
      LogMsg("<--- Rename record %s = %.9s0", apTokens[MEN_SEC_APN], apTokens[MEN_SEC_APN]); 
      *(apTokens[MEN_SEC_APN]+9) = '0';
   }

   // APN
   strcpy(pOutRec->Apn, apTokens[MEN_SEC_APN]);
   strcpy(pOutRec->BillNum, myBTrim(apTokens[MEN_SEC_BILL_NUM]));
   strcpy(pOutRec->OwnerInfo.Apn, apTokens[MEN_SEC_APN]);
   strcpy(pOutRec->OwnerInfo.BillNum, apTokens[MEN_SEC_BILL_NUM]);

   // Bill Type
   pOutRec->BillType[0] = BILLTYPE_SECURED;

#ifdef _DEBUG
   //if (!memcmp(pOutRec->Apn, "0010408900", 10))
   //   iTmp = 0;
#endif

   // Installment Status
   if (*apTokens[MEN_SEC_I1_STAT] == 'P')
      pOutRec->Inst1Status[0] = TAX_BSTAT_PAID;
   else if (*apTokens[MEN_SEC_I1_STAT] == 'C')
      pOutRec->Inst1Status[0] = TAX_BSTAT_CANCEL;
   else if (*apTokens[MEN_SEC_I1_STAT] == 'N')
      pOutRec->Inst1Status[0] = TAX_BSTAT_NOTAX;
   else
      pOutRec->Inst1Status[0] = TAX_BSTAT_UNPAID;

   if (*apTokens[MEN_SEC_I2_STAT] == 'P')
      pOutRec->Inst2Status[0] = TAX_BSTAT_PAID;
   else if (*apTokens[MEN_SEC_I1_STAT] == 'C')
      pOutRec->Inst2Status[0] = TAX_BSTAT_CANCEL;
   else if (*apTokens[MEN_SEC_I1_STAT] == 'N')
      pOutRec->Inst2Status[0] = TAX_BSTAT_NOTAX;
   else
      pOutRec->Inst2Status[0] = TAX_BSTAT_UNPAID;

   // TRA
   strcpy(pOutRec->TRA, apTokens[MEN_SEC_TRA]);

   // Tax Year
   iTmp = sprintf(pOutRec->TaxYear, "%d", lTaxYear);
   strcpy(pOutRec->OwnerInfo.TaxYear, pOutRec->TaxYear);

   // Default date - Use data from Delq file instead
   //pOutRec->isDelq[0] = '0';
   //if (*apTokens[MEN_SEC_STS_DATE] > ' ' )
   //{
   //   apTokens[MEN_SEC_STS_DATE][8] = 0;
   //   pTmp = dateConversion(apTokens[MEN_SEC_STS_DATE], pOutRec->Def_Date, MMDDYYYY);
   //   if (pTmp)
   //   {
   //      memcpy(pOutRec->DelqYear, pOutRec->Def_Date, 4);
   //      if (!memcmp(pOutRec->DelqYear, pOutRec->TaxYear, 4))
   //         pOutRec->isDelq[0] = '1';
   //   }
   //}

   // Penalty
   double dPen1 = atof(apTokens[MEN_SEC_I1_PEN]);
   double dPen2 = atof(apTokens[MEN_SEC_I2_PEN]);
   double dTotalDue = 0;

   // Check for Tax amount
   double dTax1 = atof(apTokens[MEN_SEC_I1_TAX]);
   if (dTax1 > 0.0)
      sprintf(pOutRec->TaxAmt1, "%.2f", dTax1/100.0);

   double dTax2 = atof(apTokens[MEN_SEC_I2_TAX]);
   if (dTax2 > 0.0)
      sprintf(pOutRec->TaxAmt2, "%.2f", dTax2/100.0);

   double dTaxTotal = atof(apTokens[MEN_SEC_NET_TAX]);
   if (dTaxTotal > 0.0)
   {
      sprintf(pOutRec->TotalTaxAmt, "%.2f", dTaxTotal);

      // Paid Date
      if (*apTokens[MEN_SEC_I1_PAIDDATE] >= '0' && dateConversion(apTokens[MEN_SEC_I1_PAIDDATE], acTmp, MMDDYYYY))
      {
         strcpy(pOutRec->PaidDate1, acTmp);
         dPen1 = 0;
      }
      if (*apTokens[MEN_SEC_I2_PAIDDATE] >= '0' && dateConversion(apTokens[MEN_SEC_I2_PAIDDATE], acTmp, MMDDYYYY))
      {
         strcpy(pOutRec->PaidDate2, acTmp);
         dPen2 = 0;
      }

      // Due date
      if (*apTokens[MEN_SEC_I1_DUEDATE] >= '0' && dateConversion(apTokens[MEN_SEC_I1_DUEDATE], acTmp, MMDDYYYY))
         strcpy(pOutRec->DueDate1, acTmp);
      if (*apTokens[MEN_SEC_I2_DUEDATE] >= '0' && dateConversion(apTokens[MEN_SEC_I2_DUEDATE], acTmp, MMDDYYYY, lToyear+1))
         strcpy(pOutRec->DueDate2, acTmp);

      // If past due date, add penalty to due amt
      if (dPen1 > 0.0)
      {
         if (ChkDueDate(1, pOutRec->DueDate1))
         {
            dTotalDue = dTax1+dPen1+dTax2;
            sprintf(pOutRec->PenAmt1, "%.2f", dPen1/100.0);
         } else
            dTotalDue = dTax1;
      }
      if (dPen2 > 0.0  && ChkDueDate(2, pOutRec->DueDate2))
      {
         dTotalDue += dPen2;
         sprintf(pOutRec->PenAmt2, "%.2f", dPen2/100.0);
      }
   }

   if (dTotalDue > 0.0)
      sprintf(pOutRec->TotalDue, "%.2f", dTotalDue/100.0);

   // Tax rate
   strcpy(pOutRec->TotalRate, apTokens[MEN_SEC_TOT_RATE]);

   // Owner Info
   myTrim(apTokens[MEN_SEC_NAME2]);
   if ((pTmp = strstr(apTokens[MEN_SEC_NAME1], " /")) || (pTmp = strstr(apTokens[MEN_SEC_NAME1], "/ ")))
   {
      *pTmp = 0;
      strcpy(pOutRec->OwnerInfo.Name1, apTokens[MEN_SEC_NAME1]);
      strcpy(pOutRec->OwnerInfo.Name2, apTokens[MEN_SEC_NAME2]);
   } else if (pTmp = strstr(apTokens[MEN_SEC_NAME1], "1/2/"))
   {
      *(pTmp+3) = 0;
      strcpy(pOutRec->OwnerInfo.Name1, apTokens[MEN_SEC_NAME1]);
      strcpy(pOutRec->OwnerInfo.Name2, apTokens[MEN_SEC_NAME2]);
   } else if (*apTokens[MEN_SEC_NAME2] > ' ' && (pTmp = strstr(apTokens[MEN_SEC_NAME1], " TTEE")))
   {
      iTmp = strlen(apTokens[MEN_SEC_NAME2]);
      if (iTmp > 15)
      {
         strcpy(pOutRec->OwnerInfo.Name1, myTrim(apTokens[MEN_SEC_NAME1]));
         strcpy(pOutRec->OwnerInfo.Name2, apTokens[MEN_SEC_NAME2]);
      } else
      {
         strcpy(pOutRec->OwnerInfo.Name1, apTokens[MEN_SEC_NAME1]);
         strcat(pOutRec->OwnerInfo.Name1, apTokens[MEN_SEC_NAME2]);
      }
   } else if (*apTokens[MEN_SEC_NAME2] > ' ')
   {
      sprintf(acTmp, "%s%s", apTokens[MEN_SEC_NAME1], apTokens[MEN_SEC_NAME2]);
      iTmp = blankRem(acTmp);
      if (iTmp >= TAX_NAME)
      {
         strcpy(pOutRec->OwnerInfo.Name1, apTokens[MEN_SEC_NAME1]);
         strcpy(pOutRec->OwnerInfo.Name2, apTokens[MEN_SEC_NAME2]);
      } else
         strcpy(pOutRec->OwnerInfo.Name1, acTmp);
   } else
      strcpy(pOutRec->OwnerInfo.Name1, myTrim(apTokens[MEN_SEC_NAME1]));

   // CareOf
   if (*apTokens[MEN_SEC_CAREOF] > ' ')
      strcpy(pOutRec->OwnerInfo.CareOf, apTokens[MEN_SEC_CAREOF]);

   // DBA
   if (*apTokens[MEN_SEC_DBA] > ' ')
      strcpy(pOutRec->OwnerInfo.Dba, apTokens[MEN_SEC_DBA]);

   // Mailing address
   if (*apTokens[MEN_SEC_M_ADDR] > ' ' && memcmp(apTokens[MEN_SEC_M_ADDR], "UNK", 3))
   {
      strcpy(pOutRec->OwnerInfo.MailAdr[0], myTrim(apTokens[MEN_SEC_M_ADDR]));
      sprintf(acTmp, "%s %s", apTokens[MEN_SEC_M_CITYST], apTokens[MEN_SEC_M_ZIP]);
      iTmp = blankRem(acTmp);
      strcpy(pOutRec->OwnerInfo.MailAdr[1], acTmp);
   }

   pOutRec->isSecd[0] = '1';
   pOutRec->isSupp[0] = '0';

   return 0;
}

/****************************** Men_MergeTaxDelq *****************************
 *
 * Copy DelqYear from delq record to populate Tax_Base
 *
 *****************************************************************************/

int Men_MergeTaxDelq(char *pOutbuf)
{
   static   char  acRec[2048], *pRec=NULL;
   char     *apItems[MAX_FLD_TOKEN], acTmp[32];
   TAXBASE  *pTaxBase = (TAXBASE *)pOutbuf;

   int      iLoop, iTmp;

   // Get first Sale rec for first call
   if (!pRec)
   {
      pRec = fgets(acRec, 2048, fdDelq);
   }

   do
   {
      // Compare Apn
      iLoop = memcmp(pTaxBase->Apn, (pRec+OFF_DELQ_PRCL), iApnLen);
      if (iLoop > 0)
      {
         if (bDebug)
            LogMsg0("Skip delq rec  %.*s", iApnLen, pRec+OFF_DELQ_PRCL);
         pRec = fgets(acRec, 2048, fdDelq);
         if (!pRec)
         {
            fclose(fdDelq);
            fdDelq = NULL;
            return -1;      // EOF
         }

         lSaleSkip++;
      }
   } while (iLoop > 0);

   // If not match, return
   if (iLoop)
      return 1;

   // Parse input tax data
   iTmp = ParseStringIQ(acRec, '|', MAX_FLD_TOKEN, apItems);
   if (iTmp < MEN_DELQ_REM)
   {
      LogMsg("***** Error: bad TaxDelq record for: %.50s (#tokens=%d)", acRec, iTmp);
      return -1;
   }

   // Default date
   if (dateConversion(apItems[MEN_DELQ_TAX_DEF_DATE], acTmp, MMDDYYYY))
   {
      strcpy(pTaxBase->Def_Date, acTmp);
      memcpy(pTaxBase->DelqYear, acTmp, 4);

      if (*apItems[MEN_DELQ_RDM_STAT] != 'P')
         pTaxBase->isDelq[0] = '1';
   }

   lSaleMatch++;

   return 0;
}

/**************************** Men_Load_TaxBase *******************************
 *
 * Create import file for Tax_Base, Tax_Items, Tax_Agency & Tax_Owner tables
 *
 * Return 0 if success.
 *
 *****************************************************************************/

int Men_Load_TaxBase(bool bImport)
{
   char     *pTmp, acBuf[MAX_RECSIZE], acRec[MAX_RECSIZE], acTmpFile[_MAX_PATH];
   char     acAgencyFile[_MAX_PATH], acOwnerFile[_MAX_PATH], acBaseFile[_MAX_PATH], 
            acDetailFile[_MAX_PATH], acDelqFile[_MAX_PATH], acSecRollFile[_MAX_PATH];

   int      iRet, iLen;
   long     lOut=0, lCnt=0, lTmp;
   FILE     *fdAgency, *fdOwner, *fdBase, *fdDetail, *fdSecRoll;
   TAXBASE  *pTaxBase = (TAXBASE *)&acBuf[0];

   LogMsg("Loading Current tax file");

   GetIniString(myCounty.acCntyCode, "CurrTax", "", acSecRollFile, _MAX_PATH, acIniFile);
   lLastTaxFileDate = getFileDate(acSecRollFile);
   // Only process if new tax file
   iRet = isNewTaxFile(NULL, myCounty.acCntyCode);
   if (iRet <= 0)
   {
      lLastTaxFileDate = 0;
      return iRet;
   }

   iLen = GetPrivateProfileInt(myCounty.acCntyCode, "CurrTaxLen", 0, acIniFile);
   sprintf(acDetailFile, sTaxOutTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "Items");
   sprintf(acOwnerFile, sTaxOutTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "Owner");
   sprintf(acBaseFile, sTaxOutTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "Base");
   sprintf(acAgencyFile, sTaxOutTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "Agency");

   pTmp = strrchr(acBaseFile, '\\');
   *pTmp = 0;
   if (_access(acBaseFile, 0))
      _mkdir(acBaseFile);
   *pTmp = '\\';

   // Convert EBCDIC to ASCII before use
   if (iLen > 0)
   {                    
      strcpy(acBuf, acSecRollFile);
      strcat(acSecRollFile, ".ASC");
      iRet = getFileDate(acSecRollFile);
      if (iRet <= lLastTaxFileDate)
      {
         LogMsg("Convert EBC2ASC: %s --> %s", acBuf, acSecRollFile);
         iRet = doEBC2ASCAddCR(acBuf, acSecRollFile, iLen);
         if (iRet)
         {
            LogMsg("***** Error converting EBC2ASC from %s to %s", acBuf, acSecRollFile);
            return -1;
         }
      }
   }

   GetIniString(myCounty.acCntyCode, "DelqTax", "", acDelqFile, _MAX_PATH, acIniFile);
   iLen = GetPrivateProfileInt(myCounty.acCntyCode, "DelqTaxLen", 0, acIniFile);
   lTmp = getFileDate(acDelqFile);

   // Convert EBCDIC to ASCII before use
   if (iLen > 0)
   {                    
      strcpy(acBuf, acDelqFile);
      strcat(acDelqFile, ".ASC");
      iRet = getFileDate(acDelqFile);
      if (iRet <= lTmp)
      {
         LogMsg("Convert EBC2ASC: %s --> %s", acBuf, acDelqFile);
         iRet = doEBC2ASCAddCR(acBuf, acDelqFile, iLen);
         if (iRet)
         {
            LogMsg("***** Error converting EBC2ASC from %s to %s", acBuf, acDelqFile);
            return -1;
         }
      }
   }

   // Open input file
   LogMsg("Open delinquent tax file %s", acDelqFile);
   fdDelq = fopen(acDelqFile, "r");
   if (fdDelq == NULL)
   {
      LogMsg("***** Error opening delinquent tax file: %s\n", acDelqFile);
      return -2;
   }  

   // Open input file
   LogMsg("Open Current tax file %s", acSecRollFile);
   fdSecRoll = fopen(acSecRollFile, "r");
   if (fdSecRoll == NULL)
   {
      LogMsg("***** Error opening Current tax file: %s\n", acSecRollFile);
      return -2;
   }  

   // Open Detail file
   LogMsg("Open Detail file %s", acDetailFile);
   fdDetail = fopen(acDetailFile, "w");
   if (fdDetail == NULL)
   {
      LogMsg("***** Error creating Detail file: %s\n", acDetailFile);
      return -4;
   }

   // Open base file
   LogMsg("Open Base file %s", acBaseFile);
   fdBase = fopen(acBaseFile, "w");
   if (fdBase == NULL)
   {
      LogMsg("***** Error creating base file: %s\n", acBaseFile);
      return -4;
   }

   // Open owner file
   LogMsg("Open Owner file %s", acOwnerFile);
   fdOwner = fopen(acOwnerFile, "w");
   if (fdOwner == NULL)
   {
      LogMsg("***** Error creating Owner file: %s\n", acOwnerFile);
      return -4;
   }

   // Open agency file
   sprintf(acTmpFile, "%s\\%s\\%s_Agency.lst", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   LogMsg("Open Agency file %s", acTmpFile);
   fdAgency = fopen(acTmpFile, "w");
   if (fdAgency == NULL)
   {
      LogMsg("***** Error creating Owner file: %s\n", acTmpFile);
      return -4;
   }

   // Init variables

   // Merge loop 
   while (!feof(fdSecRoll))
   {
      pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdSecRoll);
      if (!pTmp)
         break;

      // Create new R01 record
      iRet = Men_ParseTaxBase(acBuf, acRec);
      if (!iRet)
      {
         // Create Detail & Agency records
         Men_ParseTaxDetail(fdDetail, fdAgency);

         // Update delq data
         iRet = Men_MergeTaxDelq(acBuf);

         // Create TaxBase record
         Tax_CreateTaxBaseCsv(acRec, (TAXBASE *)&acBuf);
         lOut++;
         fputs(acRec, fdBase);

         // Create Owner record
         Tax_CreateTaxOwnerCsv(acRec, &pTaxBase->OwnerInfo);
         fputs(acRec, fdOwner);
      } else
      {
         if (iRet == 1)
            LogMsg("---> Drop cancel record %d [%.40s]", lCnt, acRec); 
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdSecRoll)
      fclose(fdSecRoll);
   if (fdDelq)
      fclose(fdDelq);
   if (fdBase)
      fclose(fdBase);
   if (fdDetail)
      fclose(fdDetail);
   if (fdOwner)
      fclose(fdOwner);
   if (fdAgency)
      fclose(fdAgency);

   LogMsg("Total records processed:    %u", lCnt);
   LogMsg("Total output records:       %u", lOut);

   // Import into SQL
   if (bImport)
   {
      iRet = doTaxImport(myCounty.acCntyCode, TAX_BASE);
      if (!iRet)
      {
         iRet = doTaxImport(myCounty.acCntyCode, TAX_OWNER);
         iRet = doTaxImport(myCounty.acCntyCode, TAX_DETAIL);
      }

      if (!iRet)
      {
         // Dedup Agency file before import
         LogMsg("Dedup Agency file %s to %s", acTmpFile, acAgencyFile);
         iRet = sortFile(acTmpFile, acAgencyFile, "S(#1,C,A) DUPOUT F(TXT)");
         if (iRet > 0)
            iRet = doTaxImport(myCounty.acCntyCode, TAX_AGENCY);
      }

   } else
      iRet = 0;

   return iRet;
}

/****************************** Men_ParseTaxDelq *****************************
 *
 * Return number of records output
 *
 *****************************************************************************/

int Men_ParseTaxDelq(char *pOutbuf, char *pInbuf, FILE *fdOut)
{
   char     acTmp[256];
   int      iTmp, iCnt;
   long     lTmp;
   TAXDELQ  *pOutRec = (TAXDELQ *)pOutbuf;

   // Parse input tax data
   iTmp = ParseStringIQ(pInbuf, '|', MAX_FLD_TOKEN, apTokens);
   if (iTmp < MEN_DELQ_REM)
   {
      LogMsg("***** Error: bad TaxDelq record for: %.50s (#tokens=%d)", pInbuf, iTmp);
      return -1;
   }

   // Clear output buffer
   memset(pOutbuf, 0, sizeof(TAXDELQ));

   // APN
   strcpy(pOutRec->Apn, myTrim(apTokens[MEN_DELQ_PRCL]));

   // Updated date
   if (*apTokens[MEN_DELQ_ACT_DATE] > ' ' && dateConversion(apTokens[MEN_DELQ_ACT_DATE], acTmp, MMDDYYYY))
      strcpy(pOutRec->Upd_Date, acTmp);
   else
      sprintf(pOutRec->Upd_Date, "%d", lLastTaxFileDate);
/*
#define  MEN_DELQ_A0_ASSMT_NBR      9
#define  MEN_DELQ_A0_YR             10
#define  MEN_DELQ_A0_PRCL           11    // Orig. parcel
#define  MEN_DELQ_A0_TYPE           12    // Secu(R)ed, (S)uplemental
#define  MEN_DELQ_A0_PAY_FLAG       13    // N ???
#define  MEN_DELQ_A0_AREA           14    // TRA
#define  MEN_DELQ_A0_TAX            15
#define  MEN_DELQ_A0_PEN            16
#define  MEN_DELQ_A0_COST           17
#define  MEN_DELQ_A0_AUDIT_AMT      18
*/
   iCnt = 0;

/* Need more investigation
   iTmp = MEN_DELQ_A0_ASSMT_NBR;
   while (iTmp < MEN_DELQ_A9_AUDIT_AMT && *apTokens[iTmp+1] > ' ')
   {
      // For now, just process secured record
      if (*apTokens[iTmp+3] == 'R')
      {
         strcpy(pOutRec->Assmnt_No, apTokens[iTmp]);

         // Tax Year
         strcpy(pOutRec->TaxYear, apTokens[iTmp+1]);

         // Default amt
         lTmp = atol(apTokens[iTmp+6]);
         sprintf(pOutRec->Def_Amt, "%.2f", (double)lTmp/100.0);

         // Pen
         lTmp = atol(apTokens[iTmp+7]);
         sprintf(pOutRec->Pen_Amt, "%.2f", (double)lTmp/100.0);

         // Fee
         lTmp = atol(apTokens[iTmp+8]);
         sprintf(pOutRec->Fee_Amt, "%.2f", (double)lTmp/100.0);

         // Create delimited record
         Tax_CreateDelqCsv(acTmp, (TAXDELQ *)pOutRec);

         // Output record			
         fputs(acTmp, fdOut);
         iCnt++;
      }

      iTmp += 10;
   }
*/

   // Default number
   strcpy(pOutRec->Default_No, myTrim(apTokens[MEN_DELQ_TAX_DEF_NR]));

   // Default date
   if (dateConversion(apTokens[MEN_DELQ_TAX_DEF_DATE], acTmp, MMDDYYYY))
   {
      strcpy(pOutRec->Def_Date, acTmp);
      // Tax Year
      iTmp = atoin(acTmp, 4);
      sprintf(pOutRec->TaxYear, "%d", iTmp-1);
   }

   // Default amt
   lTmp = atol(apTokens[MEN_DELQ_TAX_DEF_AMT]);
   sprintf(pOutRec->Def_Amt, "%.2f", (double)lTmp/100.0);

   // Redemption date
   if (dateConversion(apTokens[MEN_DELQ_RDM_DATE], acTmp, MMDDYYYY))
      strcpy(pOutRec->Red_Date, acTmp);

   // Redemption amt
   lTmp = atol(apTokens[MEN_DELQ_RDM_AMT]);
   sprintf(pOutRec->Red_Amt, "%.2f", (double)lTmp/100.0);

   // Tax amt
   lTmp = atol(apTokens[MEN_DELQ_TOT_TAX]);
   sprintf(pOutRec->Tax_Amt, "%.2f", (double)lTmp/100.0);

   // Pen
   lTmp = atol(apTokens[MEN_DELQ_TOT_PEN]);
   sprintf(pOutRec->Pen_Amt, "%.2f", (double)lTmp/100.0);

   // Fee
   long lCost = atol(apTokens[MEN_DELQ_TOT_COST]);
   long lStateFee = atol(apTokens[MEN_DELQ_STATE_FEE]);
   lTmp = lStateFee+lCost;
   sprintf(pOutRec->Fee_Amt, "%.2f", (double)lTmp/100.0);

   if (*apTokens[MEN_DELQ_RDM_STAT] == 'P')
      pOutRec->isDelq[0] = '0';
   else
      pOutRec->isDelq[0] = '1';

   // Create delimited record
   Tax_CreateDelqCsv(acTmp, (TAXDELQ *)pOutRec);

   // Output record			
   fputs(acTmp, fdOut);
   iCnt++;

   return iCnt;
}

/**************************** Men_Load_Delq **********************************
 *
 * Create import file for sabsdata.www and import into SQL if specified
 *
 * Return 0 if success.
 *
 *****************************************************************************/

int Men_Load_TaxDelq(bool bImport)
{
   char     *pTmp, acBuf[MAX_RECSIZE], acRec[MAX_RECSIZE];
   char     acOutFile[_MAX_PATH], acInFile[_MAX_PATH];

   int      iRet, iLen;
   long     lOut=0, lCnt=0;
   FILE     *fdOut, *fdIn;

   sprintf(acOutFile, sTaxOutTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "Delq");
   pTmp = strrchr(acOutFile, '\\');
   *pTmp = 0;
   if (_access(acOutFile, 0))
      _mkdir(acOutFile);
   *pTmp = '\\';

   GetIniString(myCounty.acCntyCode, "DelqTax", "", acInFile, _MAX_PATH, acIniFile);
   iLen = GetPrivateProfileInt(myCounty.acCntyCode, "DelqTaxLen", 0, acIniFile);
   lLastFileDate = getFileDate(acInFile);

   // Convert EBCDIC to ASCII before use
   if (iLen > 0)
   {                    
      strcpy(acBuf, acInFile);
      strcat(acInFile, ".ASC");
      iRet = getFileDate(acInFile);
      if (iRet <= lLastFileDate)
      {
         LogMsg("Convert EBC2ASC: %s --> %s", acBuf, acInFile);
         iRet = doEBC2ASCAddCR(acBuf, acInFile, iLen);
         if (iRet)
         {
            LogMsg("***** Error converting EBC2ASC from %s to %s", acBuf, acInFile);
            return -1;
         }
      }
   }

   // Open input file
   LogMsg("Open delinquent tax file %s", acInFile);
   fdIn = fopen(acInFile, "r");
   if (fdIn == NULL)
   {
      LogMsg("***** Error opening delinquent tax file: %s\n", acInFile);
      return -2;
   }  

   // Open Output file
   LogMsg("Open output file %s", acOutFile);
   fdOut = fopen(acOutFile, "w");
   if (fdOut == NULL)
   {
      LogMsg("***** Error creating output file: %s\n", acOutFile);
      return -4;
   }

   // Merge loop 
   while (!feof(fdIn))
   {
      pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdIn);
      if (!pTmp || *pTmp < ' ')
         break;

      // Create new TaxDelq record
      iRet = Men_ParseTaxDelq(acBuf, acRec, fdOut);
      if (iRet > 0)
      {
         lOut += iRet;
      } else
      {
         LogMsg("---> Drop record %d [%.40s]", lCnt, acRec); 
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdIn)
      fclose(fdIn);
   if (fdOut)
      fclose(fdOut);

   LogMsg("Total records processed:          %u", lCnt);
   LogMsg("Total delinquent records output:  %u", lOut);

   printf("\nTotal output records: %u", lCnt);

   // Import into SQL
   if (bImport && lOut > 0)
   {
      iRet = doTaxImport(myCounty.acCntyCode, TAX_DELINQUENT);
   } else
      iRet = 0;

   return iRet;
}

/***************************** Men_ParseTaxSupp ******************************
 *
 *
 *****************************************************************************/

int Men_ParseTaxSupp(char *pOutbuf, char *pInbuf)
{
   char     acTmp[256];
   int      iTmp, lTmp;

   TAXSUPP  *pOutRec = (TAXSUPP *) pOutbuf;
   MEN_SUPP *pInRec  = (MEN_SUPP *)pInbuf;

   // Clear output buffer
   memset(pOutbuf, 0, sizeof(TAXSUPP));

   // APN
   memcpy(pOutRec->Apn, pInRec->Apn, MEN_SSIZ_APN);
   memcpy(pOutRec->ApnSeq, pInRec->ApnSeq, MEN_SSIZ_APNSEQ);
   pOutRec->CurPri[0] = pInRec->CP_Indicator[0];
   pOutRec->Prorate[0] = pInRec->CP_Indicator[1];

   // BillNum
   memcpy(pOutRec->BillNum, pInRec->BillNum, MEN_SSIZ_BILLNUM);

   // Roll Year 0910 = 2009-2010
   iTmp = atoin(pInRec->RollYear, 2);
   if (iTmp >= 90)
      sprintf(pOutRec->RollYear, "19%d", iTmp);
   else
      sprintf(pOutRec->RollYear, "20%d", iTmp);

   // Roll year factor - % of the year the billing is for
   iTmp = atoin(pInRec->RollFctr, MEN_SSIZ_ROLLFCTR);
   if (iTmp > 0)
      sprintf(pOutRec->RollFctr, "%d", iTmp);

   // Event Date
   if (pInRec->EventDate[0] > ' ' && dateConversion(pInRec->EventDate, acTmp, MMDDYYYY))
      strcpy(pOutRec->EventDate, acTmp);

   // Notice Date
   if (pInRec->NoteDate[0] > ' ' && dateConversion(pInRec->NoteDate, acTmp, MMDDYYYY))
      strcpy(pOutRec->NoteDate, acTmp);

   // Check for Net amount - for dollar amount, last byte may need translation
   // }=0, J=1, K=2, L=3, M=4, ...
   bool bRet = chkSignValue(pInRec->NetTax, MEN_SSIZ_NETTAX);
   lTmp = atoin(pInRec->NetTax, MEN_SSIZ_NETTAX);
   if (lTmp > 0)
   {
      if (bRet)
         sprintf(pOutRec->NetAmt, "-%.2f", (double)lTmp/100);
      else
         sprintf(pOutRec->NetAmt, "%.2f", (double)lTmp/100);
   }

   // Penalty amount - if not a refund, there will be penalty value
   if (pInRec->I1_Status[0] != 'R')
   {
      lTmp = atoin(pInRec->TotalPen, MEN_SSIZ_TOLALPEN);
      if (lTmp > 0)
         sprintf(pOutRec->PenAmt, "%.2f", (double)lTmp/100);

      lTmp = atoin(pInRec->I1_PenAmt, MEN_SSIZ_I1_PENAMT);
      if (lTmp > 0)
         sprintf(pOutRec->PenAmt1, "%.2f", (double)lTmp/100);

      lTmp = atoin(pInRec->I2_PenAmt, MEN_SSIZ_I1_PENAMT);
      if (lTmp > 0)
         sprintf(pOutRec->PenAmt2, "%.2f", (double)lTmp/100);
   }

   // Status: P= paid, N=not billed, R=Refund, " " =Unpaid, C=Canceled
   pOutRec->Status1[0] = pInRec->I1_Status[0];
   pOutRec->Status2[0] = pInRec->I2_Status[0];

   // Installed Tax Amount
   bRet = chkSignValue(pInRec->I1_TaxAmt, MEN_SSIZ_I1_TAXAMT);
   lTmp = atoin(pInRec->I1_TaxAmt, MEN_SSIZ_I1_TAXAMT);
   if (lTmp > 0)
   {
      if (bRet)
         sprintf(pOutRec->TaxAmt1, "-%.2f", (double)lTmp/100);
      else
         sprintf(pOutRec->TaxAmt1, "%.2f", (double)lTmp/100);
   }
   bRet = chkSignValue(pInRec->I2_TaxAmt, MEN_SSIZ_I1_TAXAMT);
   lTmp = atoin(pInRec->I2_TaxAmt, MEN_SSIZ_I1_TAXAMT);
   if (lTmp > 0)
   {
      if (bRet)
         sprintf(pOutRec->TaxAmt2, "-%.2f", (double)lTmp/100);
      else
         sprintf(pOutRec->TaxAmt2, "%.2f", (double)lTmp/100);
   }

   // Delinquent date
   if (pInRec->I1_DelqDate[0] > ' ' && dateConversion(pInRec->I1_DelqDate, acTmp, MMDDYYYY))
      strcpy(pOutRec->DelqDate1, acTmp);
   if (pInRec->I2_DelqDate[0] > ' ' && dateConversion(pInRec->I2_DelqDate, acTmp, MMDDYYYY))
      strcpy(pOutRec->DelqDate2, acTmp);

   // Paid date
   if (pInRec->I1_PaidDate[0] > ' ' && dateConversion(pInRec->I1_PaidDate, acTmp, MMDDYYYY))
      strcpy(pOutRec->PaidDate1, acTmp);
   if (pInRec->I2_PaidDate[0] > ' ' && dateConversion(pInRec->I2_PaidDate, acTmp, MMDDYYYY))
      strcpy(pOutRec->PaidDate2, acTmp);

   // 'C'urrent, 'P'rior, or 'N'ext roll
   pOutRec->RollSupp[0] = pInRec->RollSupp[0];
   // 'S'ecured or 'U'nsecured
   pOutRec->Type[0] = pInRec->Type[0];

   pOutRec->CRLF[0] = 10;
   pOutRec->CRLF[1] = 0;

   return 0;
}

/**************************** Men_Load_TaxSupp *******************************
 *
 * Create import file for sabsdata.www and import into SQL if specified
 *
 * Return 0 if success.
 *
 *****************************************************************************/

int Men_Load_TaxSupp(bool bImport)
{
   char     *pTmp, acBuf[MAX_RECSIZE], acRec[MAX_RECSIZE];
   char     acOutFile[_MAX_PATH], acInFile[_MAX_PATH];

   int      iRet, iLen;
   long     lOut=0, lCnt=0;
   FILE     *fdOut, *fdIn;

   sprintf(acOutFile, sTaxOutTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "Supp");
   pTmp = strrchr(acOutFile, '\\');
   *pTmp = 0;
   if (_access(acOutFile, 0))
      _mkdir(acOutFile);
   *pTmp = '\\';

   GetIniString(myCounty.acCntyCode, "SuppTax", "", acInFile, _MAX_PATH, acIniFile);
   iLen = GetPrivateProfileInt(myCounty.acCntyCode, "SuppTaxLen", 0, acIniFile);

   // Convert EBCDIC to ASCII before use
   if (iLen > 0)
   {                    
      strcpy(acBuf, acInFile);
      strcat(acBuf, ".ASC");
      iRet = doEBC2ASCAddCR(acInFile, acBuf, iLen);
      if (!iRet)
         strcpy(acInFile, acBuf);
      else
      {
         LogMsg("***** Error converting EBC2ASC from %s to %s", acInFile, acBuf);
         return -1;
      }
   }

   // Open input file
   LogMsg("Open supplemental tax file %s", acInFile);
   fdIn = fopen(acInFile, "r");
   if (fdIn == NULL)
   {
      LogMsg("***** Error opening supplemental tax file: %s\n", acInFile);
      return -2;
   }  

   lLastFileDate = getFileDate(acInFile);

   // Open Output file
   LogMsg("Open output file %s", acOutFile);
   fdOut = fopen(acOutFile, "w");
   if (fdOut == NULL)
   {
      LogMsg("***** Error creating output file: %s\n", acOutFile);
      return -4;
   }

   // Merge loop 
   while (!feof(fdIn))
   {
      pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdIn);
      if (!pTmp || *pTmp < ' ')
         break;

      // Create new R01 record
      iRet = Men_ParseTaxSupp(acBuf, acRec);
      if (!iRet)
      {
         // Create delimited record
         Tax_CreateSuppCsv(acRec, (TAXSUPP *)&acBuf);

         // Output record			
         lOut++;
         fputs(acRec, fdOut);
      } else
      {
         LogMsg("---> Drop record %d [%.40s]", lCnt, acRec); 
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdIn)
      fclose(fdIn);
   if (fdOut)
      fclose(fdOut);

   LogMsg("Total records processed:          %u", lCnt);
   LogMsg("Total delinquent records output:  %u", lOut);

   printf("\nTotal output records: %u", lCnt);

   // Import into SQL
   if (bImport)
   {
      iRet = doTaxImport(myCounty.acCntyCode, TAX_SUPPLEMENT);
   } else
      iRet = 0;

   return iRet;
}

/***************************** Men_MergeLienCsv ******************************
 *
 * For LDR "2021 ANNUAL ROLLS - SEPARATED.txt".  
 * Return 0 if successful, < 0 if error
 *        1 retired record, not use
 *
 *****************************************************************************/

int Men_MergeLienCsv(char *pOutbuf, char *pRollRec)
{
   static   char acApn[32];
   char     acTmp[256], acTmp1[64];
   long     lTmp;
   int      iRet;

   // Replace null char with space
   iRet = replNull(pRollRec, ' ', 0);

   // Parse input rec
   iRet = ParseStringIQ(pRollRec, 9, MAX_FLD_TOKEN, apTokens);
   if (iRet < L_FLDS)
   {
      LogMsg("***** Error: bad input record for APN=%s (%d)", apTokens[L_APN], iRet);
      return -1;
   }

   // Skip Personal Property
   if (*apTokens[L_ROLLTYPE] == 'P')
   {
      LogMsg("*** Ignore Personal Property APN: %s", apTokens[L_APN]);
      return 1;
   }

   if (!strcmp(apTokens[L_APN], acApn))
   {
      LogMsg("*** Ignore duplicate APN: %s", apTokens[L_APN]);
      return 1;
   }

   // Copy APN
   strcpy(acApn, apTokens[L_APN]);
   iRet = iTrim(acApn);
   if (iRet != iApnLen)
   {
      LogMsg("*** Ignore bad APN: %s", acApn);
      return 1;
   }

   // Clear output buffer
   memset(pOutbuf, ' ', iRecLen);
   memcpy(pOutbuf, acApn, iApnLen);

   // Format APN
   iRet = formatApn(acApn, acTmp, &myCounty);
   memcpy(pOutbuf+OFF_APN_D, acTmp, iRet);

   // Create MapLink and output new record
   iRet = formatMapLink(acApn, acTmp, &myCounty);
   memcpy(pOutbuf+OFF_MAPLINK, acTmp, iRet);

   // Create index map link
   if (getIndexPage(acTmp, acTmp1, &myCounty))
      memcpy(pOutbuf+OFF_IMAPLINK, acTmp1, iRet);

   // County code
   memcpy(pOutbuf+OFF_CO_NUM, "23MEN", 5);

   // status
   *(pOutbuf+OFF_STATUS) = 'A';

   // TRA
   sprintf(acTmp, "%.6d", atol(apTokens[L_TRA]));
   vmemcpy(pOutbuf+OFF_TRA, acTmp, 6);

   // Year assessed
   memcpy(pOutbuf+OFF_YR_ASSD, myCounty.acYearAssd, 4);

   // Land
   long lLand = dollar2Num(apTokens[L_LAND]);
   if (lLand > 0)
   {
      sprintf(acTmp, "%*d", SIZ_LAND, lLand);
      memcpy(pOutbuf+OFF_LAND, acTmp, SIZ_LAND);
   }

   // Improve
   long lImpr = dollar2Num(apTokens[L_IMPR]);
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*d", SIZ_IMPR, lImpr);
      memcpy(pOutbuf+OFF_IMPR, acTmp, SIZ_IMPR);
   }

   // Other value: Fixture, PersProp, PPMH
   long lFixtr = dollar2Num(apTokens[L_FIXTURES]);
   long lPers  = dollar2Num(apTokens[L_PPVAL]);
   long lGrowVal = dollar2Num(apTokens[L_LIVINGIMPR]);
   lTmp = lFixtr+lPers+lGrowVal;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_OTHER, lTmp);
      memcpy(pOutbuf+OFF_OTHER, acTmp, SIZ_OTHER);

      if (lFixtr > 0)
      {
         sprintf(acTmp, "%d         ", lFixtr);
         memcpy(pOutbuf+OFF_FIXTR, acTmp, SIZ_FIXTR);
      }
      if (lPers > 0)
      {
         sprintf(acTmp, "%d         ", lPers);
         memcpy(pOutbuf+OFF_PERSPROP, acTmp, SIZ_PERSPROP);
      }
   }

   // Gross total
   lTmp += (lLand+lImpr);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_GROSS, lTmp);
      memcpy(pOutbuf+OFF_GROSS, acTmp, SIZ_GROSS);
   }

   // Ratio
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*d", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
      memcpy(pOutbuf+OFF_RATIO, acTmp, SIZ_RATIO);
   }

   // Exemption
   long lExe1 = dollar2Num(apTokens[L_HOEXE]);
   long lTotalExe = dollar2Num(apTokens[L_TOTALEXE]);
   if (lTotalExe > 0)
   {
      sprintf(acTmp, "%*d", SIZ_EXE_TOTAL, lTotalExe);
      memcpy(pOutbuf+OFF_EXE_TOTAL, acTmp, SIZ_EXE_TOTAL);
   }  

   if (lExe1 > 0)
      *(pOutbuf+OFF_HO_FL) = '1';      // 'Y'
   else
      *(pOutbuf+OFF_HO_FL) = '2';      // 'N'

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "001100061000", 9) )
   //   iTmp = 0;
#endif

   // Legal - obtain legal from roll update
   //if (*apTokens[L_PROPDESC] > ' ')
   //   updateLegal(pOutbuf, apTokens[L_PROPDESC]);

   // UseCode - pull from old roll file REDIFILE

   // Owner
   Men_MergeOwner(pOutbuf, apTokens[L_OWNER], apTokens[L_OWNERX]);

   // Situs
   Men_MergeSAdr(pOutbuf, apTokens[L_SADDR1], apTokens[L_SCITY], apTokens[L_SZIP]);

   // Mailing
   Men_MergeMAdr(pOutbuf, apTokens[L_MADDR1], apTokens[L_MADDR2]);

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "006380014000", 9))
   //   iTmp = 0;
#endif

   return 0;
}

/**************************** Men_MergeRollChar ******************************
 *
 * Merge LotSize, BldgCls, UseCode
 *
 * Return 0 if successful, otherwise error
 *
 *****************************************************************************/

int Men_MergeRollChar(char *pOutbuf)
{
   static   char  acRec[2048], *pTmp;
   int      iLoop, iRet;
   char     acTmp[1024];

   MEN_ROLL  *pRec;

   // Get first Char rec for first call
   if (!pTmp)
   {
      pTmp = fgets(acRec, 2048, fdRoll);
   }

   pRec = (MEN_ROLL *)&acRec[0];

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "0121901100", 8))
   //   iLoop = 0;
#endif

   do
   {
      if (!pTmp)
      {
         fclose(fdRoll);
         fdRoll = NULL;
         return 1;      // EOF
      }

      // Compare Apn
      iLoop = memcmp(pOutbuf, pRec->Apn, iApnLen);
      if (iLoop > 0)
      {
         if (bDebug)
            LogMsg0("Skip Roll rec  %.*s", iApnLen, pRec->Apn);
         pTmp = fgets(acRec, 2048, fdRoll);
         lLienSkip++;
      }
   } while (iLoop > 0);

   // If not match, return
   if (iLoop)
      return 1;

   // Ignore overwritten data from previous field
   if (memcmp(pRec->Zoning, "EMENT", 5))
   {
      memcpy(acTmp, pRec->Zoning, RSIZ_ZONING);
      iRet = blankRem(acTmp, RSIZ_ZONING);
      if (iRet > 0)
      {
         memcpy(pOutbuf+OFF_ZONE, acTmp, iRet);
         if (*(pOutbuf+OFF_ZONE_X1) == ' ')
            memcpy(pOutbuf+OFF_ZONE_X1, acTmp, iRet);
      }
   }

   // Usecode
   memcpy(pOutbuf+OFF_USE_CO, pRec->Use_Code_1, RSIZ_USE_CODE_1);

   // Standard usecode - Ignore first 2 digits
   memcpy(acTmp, &pRec->Use_Code_1[2], 2);
   acTmp[2] = 0;
   // Check for special cases
   if (pRec->Use_Code_1[0] == 'A' || (pRec->Use_Code_1[2] == 'A' && pRec->Use_Code_1[3] == 'G'))
   {
      // Ag Preserved
      if (pRec->Use_Code_1[1] == 'P')
         memcpy(pOutbuf+OFF_USE_STD, USE_AGP, 3);
      else
      {
         // AN = Ag Nonrenewal, AG = Tree & Vine market
         //memcpy(pOutbuf+OFF_USE_STD, USE_MISC, 3);
         memcpy(pOutbuf+OFF_USE_STD, USE_AG, 3);
      }
   } else if (acTmp[0] >= '0')
   {
      iRet = updateStdUse(pOutbuf+OFF_USE_STD, acTmp, 2, pOutbuf);
      if (!iRet)
      {
         // If failed, use the first 2 digits - Paul's email 3/16/2012
         memcpy(acTmp, &pRec->Use_Code_1[0], 2);
         acTmp[2] = 0;
         iRet = atol(acTmp);
         if (!memcmp(acTmp, "06", 2) || iRet == 67)
            memcpy(pOutbuf+OFF_USE_STD, USE_REC, 3);
         else if (iRet == 13 || iRet == 15 || acTmp[0] == 'C')
            memcpy(pOutbuf+OFF_USE_STD, USE_COMM, 3);
         else if (acTmp[0] == 'R')
            memcpy(pOutbuf+OFF_USE_STD, USE_RES, 3);
         else
            iRet = updateStdUse(pOutbuf+OFF_USE_STD, acTmp, 2, pOutbuf);
      }
   } else
      memcpy(pOutbuf+OFF_USE_STD, USE_UNASGN, SIZ_USE_STD);

#ifdef _DEBUG
   //if (!memcmp(pRec->Apn, "0195000570", 10))
   //   lTmp = 0;
#endif

   // Transfer
   memcpy(acTmp, pRec->Rec_Date, RSIZ_REC_DATE);
   acTmp[RSIZ_REC_DATE] = 0;
   ULONG lNewTrans = atoi(acTmp);
   ULONG lTmp = atoin(pOutbuf+OFF_TRANSFER_DT, 8);
   if (lNewTrans > lTmp && isValidYMD(acTmp))
   {
      memcpy(pOutbuf+OFF_TRANSFER_DT, acTmp, SIZ_TRANSFER_DT);

      long lDocNum = atoin(pRec->Deed_No, RSIZ_DEED_NO);
      long lBook   = atoin(pRec->Rec_Book, RSIZ_REC_BOOK);
      char acDocNum[32];
      if (lDocNum > 0)
      {
         sprintf(acDocNum, "%d            ", lDocNum);
         memcpy(pOutbuf+OFF_TRANSFER_DOC, acDocNum, SIZ_TRANSFER_DOC);
      } else if (lBook > 0)
      {
         sprintf(acDocNum, "%d %.*s          ", lBook, RSIZ_REC_PAGE, pRec->Rec_Page);
         memcpy(pOutbuf+OFF_TRANSFER_DOC, acDocNum, SIZ_TRANSFER_DOC);
      } else
         memset(pOutbuf+OFF_TRANSFER_DOC, ' ', SIZ_TRANSFER_DOC);

   }

   // Merge LotSize
   double dAcres = atofn(pRec->Acres, RSIZ_ACRES);
   if (dAcres > 0.0)
   {
      sprintf(acTmp, "%*d", SIZ_LOT_ACRES, (long)(dAcres*10));
      memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);
      lTmp = (ULONG)(dAcres * SQFT_FACTOR_100);
      sprintf(acTmp, "%*d", SIZ_LOT_SQFT, lTmp);
      memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);
   }

   if (pRec->MultiApn_Flag[0] == 'A')
      *(pOutbuf+OFF_MULTI_APN) = 'Y';
   else if (*(pOutbuf+OFF_MULTI_APN) == 'Y')
      *(pOutbuf+OFF_MULTI_APN) = 'N';

   // Legal

   // Get next record
   pTmp = fgets(acRec, 2048, fdRoll);
   lLienMatch++;

   return 0;
}

/****************************** Men_Load_LDR_Csv ****************************
 *
 * Load LDR 2021
 *
 ****************************************************************************/

int Men_Load_LDR_Csv(int iFirstRec /* 1=create header rec */)
{
   char     *pTmp, acBuf[MAX_RECSIZE], acRec[2048];
   char     acOutFile[_MAX_PATH], acTmpFile[_MAX_PATH];

   HANDLE   fhOut;
   FILE     *fdLien;

   int      iRet;
   DWORD    nBytesWritten;
   BOOL     bRet;
   long     lRet=0, lCnt;

   // If LDR file has been sorted by -Xl, no need to resort
   if (!strstr(acLienFile, "lien.srt"))
   {
      GetIniString(myCounty.acCntyCode, "LienFile", "", acTmpFile, _MAX_PATH, acIniFile);

      // Sort roll file on ASMT
      sprintf(acLienFile, "%s\\%s\\%s_lien.srt", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
      strcpy(acRec, "S(#1,N,A) F(TXT) DEL(9) ");
      iRet = sortFile(acTmpFile, acLienFile, acRec);  
      if (!iRet)
         return -1;
   } 

   // Open Lien file
   LogMsg("Open lien file %s", acLienFile);
   fdLien = fopen(acLienFile, "r");
   if (fdLien == NULL)
   {
      LogMsg("***** Error opening lien file: %s\n", acLienFile);
      return -1;
   }  

   // Open Char file
   //LogMsg("Open Char file %s", acCChrFile);
   //fdChar = fopen(acCChrFile, "r");
   //if (fdChar == NULL)
   //{
   //   LogMsg("***** Error opening char file: %s\n", acCharFile);
   //   return -2;
   //}

   LogMsg("Open roll file %s", acRollFile);
   fdRoll = fopen(acRollFile, "r");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", acRollFile);
      return -2;
   }

   // Open Output file
   sprintf(acOutFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "R01");
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening output file: %s\n", acOutFile);
      return -4;
   }

   // Output first header record
   if (iFirstRec > 0)
   {
      memset(acBuf, '9', iRecLen);
      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
   }

   // Get 1st rec
   do {
      pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdLien);
   } while (pTmp && !isdigit(*pTmp));

   lCnt = 1;

   // Init variables
   iNoMatch=iBadCity=iBadSuffix=0;

   // Merge loop 
   while (!feof(fdLien))
   {
      // Create new R01 record
      iRet = Men_MergeLienCsv(acBuf, acRec);     // 2020
      if (!iRet)
      {
         // Merge LotSize, BldgCls, UseCode from REDIFILE
         if (fdRoll)
            iRet = Men_MergeRollChar(acBuf);

         //if (fdChar)
         //   iRet = Men_MergeStdChar(acBuf);

         lLDRRecCount++;
         bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
         if (!bRet)
         {
            LogMsg("***** Error writing to output file at record %d\n", lCnt);
            lRet = WRITE_ERR;
            break;
         }
      } 

      // Get next roll record
      do {
         pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdLien);
      } while (pTmp && !isdigit(*pTmp));

      if (!pTmp)
         break;
      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdLien)
      fclose(fdLien);
   if (fdRoll)
      fclose(fdRoll);
   //if (fdChar)
   //   fclose(fdChar);
   if (fhOut)
      CloseHandle(fhOut); 

   LogMsg(" Total input records:  %u", lCnt);
   LogMsg("      output records:  %u", lLDRRecCount);
   //LogMsg("        Char matched:  %u", lCharMatch);
   //LogMsg("         Char skiped:  %u", lCharSkip);
   LogMsg("        Roll matched:  %u", lLienMatch);
   LogMsg("         Roll skiped:  %u", lLienSkip);
   LogMsg("    bad-city records:  %u", iBadCity);
   LogMsg("  bad-suffix records:  %u\n", iBadSuffix);
   printf("\nTotal output records: %u\n", lLDRRecCount);

   lRecCnt = lLDRRecCount;
   return lRet;
}

/******************************* Men_ExtrLienCsv *****************************
 *
 *****************************************************************************/

int Men_ExtrLienRec(char *pOutbuf, char *pRollRec)
{
   int      iRet, lTmp, iTmp;
   char     acTmp[64], acApn[32];
   LIENEXTR *pLienExtr = (LIENEXTR *)pOutbuf;

   // Replace null char with space
   iRet = replNull(pRollRec, ' ', 0);

   // Parse input rec
   iRet = ParseStringIQ(pRollRec, 9, MAX_FLD_TOKEN, apTokens);
   if (iRet < L_FLDS)
   {
      LogMsg("***** Error: bad input record for APN=%s (%d)", apTokens[L_APN], iRet);
      return -1;
   }

   // Skip Personal Property
   if (*apTokens[L_ROLLTYPE] == 'P')
   {
      LogMsg("*** Ignore Personal Property APN: %s", apTokens[L_APN]);
      return 1;
   }

   // Copy APN
   strcpy(acApn, apTokens[L_APN]);
   iRet = iTrim(acApn);
   if (iRet != iApnLen)
   {
      LogMsg("*** Ignore bad APN: %s", acApn);
      return 1;
   }

   // Clear output buffer
   memset(pOutbuf, ' ', sizeof(LIENEXTR));

   // Copy APN
   memcpy(pLienExtr->acApn, acApn, iApnLen);

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "001100061000", 9) )
   //   iTmp = 0;
#endif

   // Year assessed
   memcpy(pLienExtr->acYear, myCounty.acYearAssd, 4);

   // Land
   long lLand = dollar2Num(apTokens[L_LAND]);
   if (lLand > 0)
   {
      sprintf(acTmp, "%*u", SIZ_LAND, lLand);
      memcpy(pLienExtr->acLand, acTmp, SIZ_LAND);
   }

   // Improve
   long lImpr = dollar2Num(apTokens[L_IMPR]);
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*u", SIZ_IMPR, lImpr);
      memcpy(pLienExtr->acImpr, acTmp, SIZ_IMPR);
   }

   // Other value: Fixture, PersProp, PPMH
   long lFixtr = dollar2Num(apTokens[L_FIXTURES]);
   long lPers  = dollar2Num(apTokens[L_PPVAL]);
   long lGrowVal = dollar2Num(apTokens[L_LIVINGIMPR]);
   lTmp = lFixtr+lPers+lGrowVal;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*u", SIZ_OTHER, lTmp);
      memcpy(pLienExtr->acOther, acTmp, SIZ_OTHER);

      if (lFixtr > 0)
      {
         sprintf(acTmp, "%u         ", lFixtr);
         memcpy(pLienExtr->acME_Val, acTmp, SIZ_FIXTR);
      }
      if (lPers > 0)
      {
         sprintf(acTmp, "%u         ", lPers);
         memcpy(pLienExtr->acPP_Val, acTmp, SIZ_PERSPROP);
      }
      if (lGrowVal > 0)
      {
         iTmp = sprintf(acTmp, "%d", lGrowVal);
         memcpy(pLienExtr->extra.MB.GrowImpr, acTmp, iTmp);
      }
   }

   // Gross total
   lTmp += (lLand+lImpr);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*u", SIZ_GROSS, lTmp);
      memcpy(pLienExtr->acGross, acTmp, SIZ_GROSS);
   }

   // Ratio
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*u", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
      memcpy(pLienExtr->acRatio, acTmp, SIZ_RATIO);
   }

   // Exemption
   long lExe1 = dollar2Num(apTokens[L_HOEXE]);
   long lTotalExe = dollar2Num(apTokens[L_TOTALEXE]);
   if (lTotalExe > 0)
   {
      sprintf(acTmp, "%*u", SIZ_EXE_TOTAL, lTotalExe);
      memcpy(pLienExtr->acExAmt, acTmp, SIZ_EXE_TOTAL);
   }  

   if (lExe1 > 0)
      pLienExtr->acHO[0] = '1';      // 'Y'
   else
      pLienExtr->acHO[0] = '2';      // 'N'

   pLienExtr->LF[0] = 10;
   pLienExtr->LF[1] = 0;

   return 0;
}

/******************************* Men_ExtrLienCsv *****************************
 *
 * Extract values from "2021 ANNUAL ROLLS - SECURED.txt"
 *
 *****************************************************************************/

int Men_ExtrLienCsv(LPCSTR pCnty)
{
   char     *pTmp, acBuf[1024], acRec[MAX_RECSIZE], acOutFile[_MAX_PATH];
   int      iRet, iNewRec=0, lCnt=0;
   FILE     *fdLienExt;

   LogMsg0("Extract LDR values for %s", pCnty);

   GetIniString(myCounty.acCntyCode, "LienFile", "", acRec, _MAX_PATH, acIniFile);

   // Sort roll file on ASMT
   sprintf(acLienFile, "%s\\%s\\%s_lien.srt", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   iRet = sortFile(acRec, acLienFile, "S(#1,N,A) F(TXT) DEL(9) ");  
   if (!iRet)
      return -1;

   LogMsg("Open sorted LDR file %s", acLienFile);
   fdLien = fopen(acLienFile, "r");
   if (fdLien == NULL)
   {
      LogMsg("***** Error opening LDR file: %s\n", acLienFile);
      return -2;
   }

   // Create lien extract
   sprintf(acOutFile, acLienTmpl, pCnty, pCnty);
   LogMsg("Create lien extract file %s", acOutFile);
   fdLienExt = fopen(acOutFile, "w");
   if (fdLienExt == NULL)
   {
      LogMsg("***** Error creating lien extract file: %s\n", acOutFile);
      return -3;
   }

   // Get 1st rec
   do {
      pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdLien);
   } while (pTmp && !isdigit(*pTmp));

   // Merge loop
   while (pTmp && !feof(fdLien))
   {
      // Create new record
      iRet = Men_ExtrLienRec(acBuf, acRec);
      if (!iRet)
      {
         // Write to output
         fputs(acBuf, fdLienExt);
         iNewRec++;
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);

      // Get next roll record
      pTmp = fgets(acRec, MAX_RECSIZE, fdLien);
      if (!isdigit(acRec[1]))
         break;      // EOF
   }

   // Close files
   if (fdLien)
      fclose(fdLien);
   if (fdLienExt)
      fclose(fdLienExt);

   LogMsg("Total output records:       %u", iNewRec);
   LogMsg("Total records processed:    %u", lCnt);
   printf("\nTotal output records: %u", lCnt);

   lRecCnt = lCnt;
   return 0;
}

/*************************** Men_Load_RollCorrection *************************
 *
 * Extract values from "2021 ANNUAL ROLLS COMBINED FOR PARCEL QUEST 12-9-2021.csv"
 * and replace LDR data.
 *
 * Run FixCsv and resort it by AIN before processing
 *
 *****************************************************************************/

int Men_MergeRollCorrection(char *pOutbuf, char *pRollRec)
{
   char     acTmp[256];
   long     lTmp, iRet;

#ifdef _DEBUG
   //if (!memcmp(pRollRec, "0010824200", 10))
   //   lTmp = 0;
#endif

   // Parse input string
   iRet = ParseStringNQ(pRollRec, ',', MAX_FLD_TOKEN, apTokens);
   if (iRet < MEN_COR_FLDS)
   {
      LogMsg("***** Men_MergeRollCorrection: bad input record for APN=%s", apTokens[0]);
      return -1;
   }

   // Land
   long lLand = dollar2Num(apTokens[MEN_COR_ASSDLAND]);
   if (lLand > 0)
   {
      sprintf(acTmp, "%*u", SIZ_LAND, lLand);
      memcpy(pOutbuf+OFF_LAND, acTmp, SIZ_LAND);
   }

   // Improve
   long lImpr = dollar2Num(apTokens[MEN_COR_ASSDIMP]);
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*u", SIZ_IMPR, lImpr);
      memcpy(pOutbuf+OFF_IMPR, acTmp, SIZ_IMPR);
   }

   long lPers = dollar2Num(apTokens[MEN_COR_ASSDPERSONAL]);
   long lLiv = dollar2Num(apTokens[MEN_COR_ASSDLIVIMP]);
   long lFixt = dollar2Num(apTokens[MEN_COR_ASSDFIXTURES]);
   lTmp = lPers + lFixt + lLiv;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*u", SIZ_OTHER, lTmp);
      memcpy(pOutbuf+OFF_OTHER, acTmp, SIZ_OTHER);

      if (lFixt > 0)
      {
         sprintf(acTmp, "%u         ", lFixt);
         memcpy(pOutbuf+OFF_FIXTR, acTmp, SIZ_FIXTR);
      }
      if (lLiv > 0)
      {
         sprintf(acTmp, "%u         ", lLiv);
         memcpy(pOutbuf+OFF_TREEVINES, acTmp, SIZ_TREEVINES);
      }
      if (lPers > 0)
      {
         sprintf(acTmp, "%u         ", lPers);
         memcpy(pOutbuf+OFF_PERSPROP, acTmp, SIZ_PERSPROP);
      }
   }

   // Gross total
   lTmp += (lLand+lImpr);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*u", SIZ_GROSS, lTmp);
      memcpy(pOutbuf+OFF_GROSS, acTmp, SIZ_GROSS);
   }

   // Ratio
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*u", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
      memcpy(pOutbuf+OFF_RATIO, acTmp, SIZ_RATIO);
   }

   // HO Exempt
   long lExe = dollar2Num(apTokens[MEN_COR_HOX]);
   if (lExe > 0)
      *(pOutbuf+OFF_HO_FL) = '1';      // 'Y'
   else
      *(pOutbuf+OFF_HO_FL) = '2';      // 'N'

   lExe = dollar2Num(apTokens[MEN_COR_TOTALEXMPT]);
   if (lExe > 0)
   {
      sprintf(acTmp, "%*u", SIZ_EXE_TOTAL, lExe);
      memcpy(pOutbuf+OFF_EXE_TOTAL, acTmp, SIZ_EXE_TOTAL);
   }

   // Full exemption
   if (lExe > 0 && lExe >= lTmp)
      *(pOutbuf+OFF_FULL_EXEMPT) = 'Y';

   // TRA
   lTmp = atol(apTokens[MEN_COR_TAG]);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%.6d", lTmp);
      memcpy(pOutbuf+OFF_TRA, acTmp, 6);
   }

   return 0;
}

int Men_Load_RollCorrection(int iSkip)
{
   char     *pTmp, acBuf[MAX_RECSIZE], acRollRec[MAX_RECSIZE];
   char     acOutFile[_MAX_PATH], acRawFile[_MAX_PATH];

   HANDLE   fhIn, fhOut;

   int      iRet, iTmp, iRollUpd, iRollDrop;
   DWORD    nBytesRead, nBytesWritten;
   BOOL     bRet, bEof;
   long     lRet=0, lCnt=0;

   sprintf(acRawFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "S01");
   sprintf(acOutFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "R01");

   // Rename files for processing
   if (_access(acRawFile, 0))
   {
      if (!_access(acOutFile, 0))
         rename(acOutFile, acRawFile);
      else
      {
         LogMsg("***** Missing input file %s.  Please recheck!", acRawFile);
         return 1;
      }
   }

   // Open roll file
   LogMsg("Open roll correction file %s", acRollFile);
   fdRoll = fopen(acRollFile, "r");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening roll correction file: %s\n", acRollFile);
      return -2;
   }

   // Open Input file
   LogMsg("Open input file %s", acRawFile);
   fhIn = CreateFile(acRawFile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);
   if (fhIn == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening input file: %s\n", acRawFile);
      return 2;
   }

   // Open Output file
   sprintf(acOutFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "R01");
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);
   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening output file: %s\n", acOutFile);
      return -4;
   }

   // Output first header record
   if (iSkip > 0)
   {
      bRet = ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);
      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
   }

   pTmp = fgets((char *)&acRollRec[0], MAX_RECSIZE, fdRoll);
   if (*pTmp > '9')
      pTmp = fgets((char *)&acRollRec[0], MAX_RECSIZE, fdRoll);
   bEof = (pTmp ? false:true);

   // Init variables
   iRollUpd=iRollDrop=0;

   // Merge loop
   while (!bEof)
   {
      bRet = ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);

      // Check for EOF
      if (!bRet)
      {
         LogMsg("***** Error reading input file %s (%f)", acRawFile, GetLastError());
         break;
      }

      // EOF ?
      if (iRecLen != nBytesRead)
         break;

      NextRollRec:
#ifdef _DEBUG
      //if (!memcmp(acBuf, "004020015", 9))
      //   iRet = 1;
#endif
      iTmp = memcmp(acBuf, acRollRec, iApnLen);
      if (!iTmp)
      {
         // Merge roll data
         iRet = Men_MergeRollCorrection(acBuf, acRollRec);
         if (!iRet)
            iRollUpd++;

         bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);

         // Read next roll record
         pTmp = fgets(acRollRec, MAX_RECSIZE, fdRoll);
         lCnt++;

         if (!pTmp)
            bEof = true;    // Signal to stop
      } else if (iTmp > 0)
      {
         iRollDrop++;         // Ignore roll record that doesn't match

         // Get next record
         pTmp = fgets(acRollRec, MAX_RECSIZE, fdRoll);
         lCnt++;

         if (!pTmp)
            bEof = true;    // Signal to stop
         else
            goto NextRollRec;
      } else
      {
         bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
      }

      if (!(lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Write out any record left from S01 
   while (bRet)
   {
      bRet = ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);
      if (nBytesRead == iRecLen)
      {
         WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
         lCnt++;
      } else
         break;
   }

   // Close files
   if (fdRoll)
      fclose(fdRoll);
   if (fhIn)
      CloseHandle(fhIn);
   if (fhOut)
      CloseHandle(fhOut);

   LogMsg("Total output records:       %u", lCnt);
   LogMsg("      updated records:      %u", iRollUpd);
   LogMsg("      dropped records:      %u\n", iRollDrop);
   printf("\nTotal output records: %u\n", lCnt);

   lRecCnt = lCnt;

   return lRet;
}

/********************************* Men_MergeSAdr *****************************
 *
 * This version parse mail address from "CA-Mendo-AsmtRoll.csv"
 * Return 0 if successful, >0 if error
 *
 *****************************************************************************/

int Men_MergeSAdr(char *pOutbuf)
{
   static   char acRec[512], *pRec=NULL;
   char     acTmp[256], acCode[32], acAddr1[256];
   long     lTmp;
   int      iTmp;

   // Remove old situs
   removeSitus(pOutbuf);

   // Merge data
   acAddr1[0] = 0;
   lTmp = atol(apTokens[R_SITUSSTREETNUMBER]);
   if (lTmp > 0)
   {
      memcpy(pOutbuf+OFF_S_HSENO, apTokens[R_SITUSSTREETNUMBER], strlen(apTokens[R_SITUSSTREETNUMBER]));
      iTmp = sprintf(acTmp, "%d ", lTmp);
      memcpy(pOutbuf+OFF_S_STRNUM, acTmp, iTmp);
      strcpy(acAddr1, acTmp);

      if (*apTokens[R_SITUSSTREETNUMBERSUFFIX] > ' ')
         vmemcpy(pOutbuf+OFF_S_STR_SUB, apTokens[R_SITUSSTREETNUMBERSUFFIX], SIZ_S_STR_SUB);

      if (*apTokens[R_SITUSSTREETPREDIRECTIONAL] > ' ')
      {
         strcat(acAddr1, apTokens[R_SITUSSTREETPREDIRECTIONAL]);
         strcat(acAddr1, " ");
         memcpy(pOutbuf+OFF_S_DIR, apTokens[R_SITUSSTREETPREDIRECTIONAL], strlen(apTokens[R_SITUSSTREETPREDIRECTIONAL]));
      }
   }

   strcpy(acTmp, _strupr(apTokens[R_SITUSSTREETNAME]));
   strcat(acAddr1, acTmp);
   vmemcpy(pOutbuf+OFF_S_STREET, acTmp, SIZ_S_STREET);

   if (*apTokens[R_SITUSSTREETTYPE] > ' ')
   {
      strcat(acAddr1, " ");
      strcat(acAddr1, apTokens[R_SITUSSTREETTYPE]);

      iTmp = GetSfxCodeX(apTokens[R_SITUSSTREETTYPE], acTmp);
      if (iTmp > 0)
         Sfx2Code(acTmp, acCode);
      else
      {
         LogMsg0("*** Invalid suffix: %s", apTokens[R_SITUSSTREETTYPE]);
         iBadSuffix++;
         memset(acCode, ' ', SIZ_S_SUFF);
      }
      memcpy(pOutbuf+OFF_S_SUFF, acCode, SIZ_S_SUFF);
   } 

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "616141018", 9))
   //   acTmp[0] = 0;
#endif

   if (isalnum(*apTokens[R_SITUSUNITNUMBER]) || *apTokens[R_SITUSUNITNUMBER] == '#')
   {
      strcat(acAddr1, " ");
      if (isdigit(*apTokens[R_SITUSUNITNUMBER]))
         strcat(acAddr1, "#");
      else if ((*apTokens[R_SITUSUNITNUMBER]) != '#')
         strcat(acAddr1, "STE ");
      strcat(acAddr1, apTokens[R_SITUSUNITNUMBER]);
      memcpy(pOutbuf+OFF_S_UNITNO, apTokens[R_SITUSUNITNUMBER], strlen(apTokens[R_SITUSUNITNUMBER]));
   } else if (*(pOutbuf+OFF_M_UNITNO) > ' ' &&
      !memcmp(pOutbuf+OFF_S_STRNUM, pOutbuf+OFF_M_STRNUM, SIZ_S_STRNUM) &&
      !memcmp(pOutbuf+OFF_S_STREET, pOutbuf+OFF_M_STREET, SIZ_S_STREET) )
   {
      memcpy(pOutbuf+OFF_S_UNITNO, pOutbuf+OFF_M_UNITNO, SIZ_M_UNITNO);
      sprintf(acTmp, " %.*s", SIZ_M_UNITNO, pOutbuf+OFF_M_UNITNO);
      strcat(acAddr1, acTmp);
   } else if (*apTokens[R_SITUSUNITNUMBER] > ' ')
   {
      if (isalnum(*(1+apTokens[R_SITUSUNITNUMBER])))
      {
         strcat(acAddr1, " #");
         strcat(acAddr1, apTokens[R_SITUSUNITNUMBER]+1);
         memcpy(pOutbuf+OFF_S_UNITNO, apTokens[R_SITUSUNITNUMBER]+1, strlen(apTokens[R_SITUSUNITNUMBER])-1);
      } else
         LogMsg("*** Invalid situs Unit#: %s [%s]", apTokens[R_SITUSUNITNUMBER], apTokens[R_APN]);
   }

   iTmp = blankRem(acAddr1);
   memcpy(pOutbuf+OFF_S_ADDR_D, acAddr1, iTmp);
   memcpy(pOutbuf+OFF_S_ST, "CA", 2);

   // Situs city
   if (*apTokens[R_SITUSCITYNAME] > ' ')
   {
      char sCity[32];

      iTmp = City2CodeEx(_strupr(apTokens[R_SITUSCITYNAME]), acCode, sCity, apTokens[R_APN]);
      if (iTmp > 0)
      {
         vmemcpy(pOutbuf+OFF_S_CITY, acCode, SIZ_S_CITY);
      } else
         strcpy(sCity, apTokens[R_SITUSCITYNAME]);

      if (*apTokens[R_SITUSZIPCODE] == '9')
         vmemcpy(pOutbuf+OFF_S_ZIP, apTokens[R_SITUSZIPCODE], SIZ_S_ZIP);

      iTmp = sprintf(acTmp, "%s, CA ", sCity, apTokens[R_SITUSZIPCODE]);
      memcpy(pOutbuf+OFF_S_CTY_ST_D, acTmp, iTmp);
   }

   return 0;
}

/******************************** Men_MergeMAdr ******************************
 *
 * This version parse mail address from "CA-Mendo-AsmtRoll.csv"
 * Return 0 if successful, >0 if error
 *
 *****************************************************************************/

void Men_MergeMAdr(char *pOutbuf)
{
   char        acTmp[256], acAddr1[256], *pAddr1;
   int         iTmp;
   bool        bUnitAvail = false;

   ADR_REC     sMailAdr;

   // Clear old Mailing
   removeMailing(pOutbuf);

   if (*apTokens[R_MAILNAME] > ' ')
   {
      iTmp = replUnPrtChar(apTokens[R_MAILNAME]);
      updateCareOf(pOutbuf, apTokens[R_MAILNAME], 0);
   }

   // Check for blank address
   replUnPrtChar(apTokens[R_MAILADDRESS]);
   pAddr1 = apTokens[R_MAILADDRESS];
   if (memcmp(pAddr1, "     ", 5) && memcmp(pAddr1, "UNKNOWN", 7))
   {
      strcpy(acAddr1, pAddr1);
      memset((void *)&sMailAdr, 0, sizeof(ADR_REC));

      // Parsing mail address
      parseAdr1(&sMailAdr, acAddr1);

      if (sMailAdr.lStrNum > 0)
      {
         iTmp = sprintf(acTmp, "%d ", sMailAdr.lStrNum);
         memcpy(pOutbuf+OFF_M_STRNUM, acTmp, iTmp);
         if (sMailAdr.strSub[0] > '0')
         {
            sprintf(acTmp, "%s  ", sMailAdr.strSub);
            memcpy(pOutbuf+OFF_M_STR_SUB, acTmp, SIZ_M_STR_SUB);
         }
      }

      if (sMailAdr.strDir[0] > ' ')
         memcpy(pOutbuf+OFF_M_DIR, sMailAdr.strDir, strlen(sMailAdr.strDir));

      memcpy(pOutbuf+OFF_M_STREET, sMailAdr.strName, strlen(sMailAdr.strName));

      if (sMailAdr.strSfx[0] > ' ')
         memcpy(pOutbuf+OFF_M_SUFF, sMailAdr.strSfx, strlen(sMailAdr.strSfx));

      if (sMailAdr.Unit[0] > ' ')
      {
         if (sMailAdr.Unit[0] == '#')
            memcpy(pOutbuf+OFF_M_UNITNO, sMailAdr.Unit, strlen(sMailAdr.Unit));
         else
         {
            *(pOutbuf+OFF_M_UNITNO) = '#';
            memcpy(pOutbuf+OFF_M_UNITNO+1, sMailAdr.Unit, strlen(sMailAdr.Unit));
         }
      }

      memcpy(pOutbuf+OFF_M_ADDR_D, acAddr1, strlen(acAddr1));

#ifdef _DEBUG
      //if (!memcmp(pOutbuf, "607224002", 9) )
      //   iTmp = 0;
#endif

      // City/State
      if (*apTokens[R_MAILCITY] > ' ')
      {
         // Remove quote & comma at the end of city name
         strcpy(acAddr1, apTokens[R_MAILCITY]);
         iTmp = iTrim(acAddr1);
         if (acAddr1[iTmp-1] == ',')
            acAddr1[iTmp-1] = 0;

         if (*apTokens[R_MAILSTATE] > ' ')
         {
            vmemcpy(pOutbuf+OFF_M_CITY, acAddr1, SIZ_M_CITY);
            vmemcpy(pOutbuf+OFF_M_ST, apTokens[R_MAILSTATE], 2);

            iTmp = sprintf(acTmp, "%s, %s %s", acAddr1, apTokens[R_MAILSTATE], apTokens[R_MAILZIPCODE]);
            vmemcpy(pOutbuf+OFF_M_CTY_ST_D, acTmp, SIZ_M_CTY_ST_D, iTmp);
         } else
         {
            vmemcpy(pOutbuf+OFF_M_CITY, acAddr1, SIZ_M_CITY+SIZ_M_ST+SIZ_M_ZIP);
            vmemcpy(pOutbuf+OFF_M_CTY_ST_D, acAddr1, SIZ_M_CTY_ST_D);
         }
      }

      iTmp = atol(apTokens[R_MAILZIPCODE]);
      if (iTmp > 500)
         vmemcpy(pOutbuf+OFF_M_ZIP, apTokens[R_MAILZIPCODE], SIZ_M_ZIP);
   }
}

/******************************** Men_MergeOwner *****************************
 *
 * This version parses owners from CA-Mendo-AsmtRoll.csv
 *
 * Return 0 if successful, >0 if error
 *
 *****************************************************************************/

void Men_MergeOwner(char *pOutbuf)
{
   int   iTmp;
   char  *pName1, *pName2;
   OWNER myOwner;

   // Clear old names, vesting, and DBA
   removeNames(pOutbuf, false, true);

   if (*apTokens[R_DOINGBUSINESSAS] > ' ')
   {
      iTmp = cleanOwner(apTokens[R_DOINGBUSINESSAS]);
      if (iTmp > 0)
         blankRem(apTokens[R_DOINGBUSINESSAS]);
      vmemcpy(pOutbuf+OFF_DBA, apTokens[R_DOINGBUSINESSAS], SIZ_DBA);
   }

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "009619813", 9) )
   //   iTmp = 0;
#endif
   cleanOwner(apTokens[R_LEGALPARTY1]);
   cleanOwner(apTokens[R_LEGALPARTY2]);

   if (*apTokens[R_LEGALPARTY1] > ' ')
   {
      pName1 = apTokens[R_LEGALPARTY1];
      pName2 = apTokens[R_LEGALPARTY2];
   } else
   {
      pName1 = apTokens[R_LEGALPARTY2];
      pName2 = apTokens[R_LEGALPARTY1];
   }

   if (*pName1 > ' ')
   {
      vmemcpy(pOutbuf+OFF_NAME1, pName1, SIZ_NAME1);

      // Update vesting
      iTmp = updateVesting(myCounty.acCntyCode, pName1, pOutbuf+OFF_VEST, pOutbuf+OFF_ETAL_FLG);
      if (!iTmp)
      {
         iTmp = splitOwner(pName1, &myOwner, 3);
         vmemcpy(pOutbuf+OFF_NAME_SWAP, myOwner.acSwapName, SIZ_NAME_SWAP);
      } else
         vmemcpy(pOutbuf+OFF_NAME_SWAP, pName1, SIZ_NAME_SWAP);

   }

   if (*pName2 > ' ')
   {
      vmemcpy(pOutbuf+OFF_NAME2, pName2, SIZ_NAME2);

      // Update vesting
      if (*(pOutbuf+OFF_VEST) == ' ')
         updateVesting(myCounty.acCntyCode, pName2, pOutbuf+OFF_VEST, pOutbuf+OFF_ETAL_FLG);
   }

   // Check to see if we can combine names
   //pTmp = strchr(pName1, ' ');
   //if (!memcmp(pName1, pName2, pTmp-pName1))
   //{
   //   iTmp = sprintf(acTmp, "%.*s & %.*s %.*s",
   //      RSIZ_OWN1_FIRST+RSIZ_OWN1_MIDDLE, pRec->Own1_First,
   //      RSIZ_OWN2_FIRST+RSIZ_OWN2_MIDDLE, pRec->Own2_First,
   //      RSIZ_OWN1_LAST, pRec->Own1_Last);
   //   iTmp = blankRem(acTmp, iTmp);
   //   vmemcpy(pOutbuf+OFF_NAME_SWAP, acTmp, SIZ_NAME_SWAP, iTmp);
   //} else
   //{
   //   // Swapped name
   //   if (pRec->Own1_Flag == 'F')
   //      sprintf(acTmp, "%.24s %.19s", pRec->Own1_First, pRec->Own1_Last);
   //   else
   //      memcpy(acTmp, pRec->Own1_Last, RSIZ_OWNER);
   //   iTmp = blankRem(acTmp, RSIZ_OWNER);
   //   memcpy(pOutbuf+OFF_NAME_SWAP, acTmp, iTmp);
   //}

   if (*apTokens[R_LEGALPARTY3] > ' ')
      *(pOutbuf+OFF_ETAL_FLG) = 'Y';
}

/******************************* Men_MergeChar *******************************
 *
 * Merge Men_Char.dat in STDCHAR format
 *
 *****************************************************************************/

int Men_MergeStdChar(char *pOutbuf)
{
   static   char acRec[2048], *pRec=NULL;
   char     acTmp[256];
   long     lBldgSqft, lGarSqft, lTmp;
   int      iLoop, iBeds, iFBath, iHBath;
   STDCHAR *pChar;

   // Get first Char rec for first call
   if (!pRec)
      pRec = fgets(acRec, 2048, fdChar);

   pChar = (STDCHAR *)pRec;

   do
   {
      if (!pRec)
      {
         fclose(fdChar);
         fdChar = NULL;
         return 1;      // EOF
      }

      // Compare APN
      iLoop = memcmp(pOutbuf, pRec, iApnLen);
      if (iLoop > 0)
      {
         if (bDebug)
            LogMsg0("Skip Char rec  %.*s", iApnLen, pRec);
         pRec = fgets(acRec, 2048, fdChar);
         lCharSkip++;
      }
   } while (iLoop > 0);

   // If not match, return
   if (iLoop)
      return 1;

   // Yrblt
   lTmp = atoin(pChar->YrBlt, SIZ_CHAR_YRBLT);
   if (lTmp > 1600 && lTmp <= lToyear)
      memcpy(pOutbuf+OFF_YR_BLT, pChar->YrBlt, SIZ_YR_BLT);

   // Eff Yrblt
   lTmp = atoin(pChar->YrEff, SIZ_CHAR_YRBLT);
   if (lTmp > 1600 && lTmp <= lToyear)
      memcpy(pOutbuf+OFF_YR_EFF, pChar->YrEff, SIZ_YR_BLT);

   // BldgSqft
   lBldgSqft = atoin(pChar->BldgSqft, SIZ_CHAR_SQFT);
   if (lBldgSqft > 10)
   {
      sprintf(acTmp, "%*u", SIZ_BLDG_SF, lBldgSqft);
      memcpy(pOutbuf+OFF_BLDG_SF, acTmp, SIZ_BLDG_SF);
   }

   // Garage Sqft (may be carport or detached garage)
   lGarSqft = atoin(pChar->GarSqft, SIZ_CHAR_SQFT);
   if (lGarSqft > 10)
   {
      sprintf(acTmp, "%*u", SIZ_GAR_SQFT, lGarSqft);
      memcpy(pOutbuf+OFF_GAR_SQFT, acTmp, SIZ_GAR_SQFT);
      *(pOutbuf+OFF_PARK_TYPE) = pChar->ParkType[0];  
   }

   // Pools
   *(pOutbuf+OFF_POOL) = pChar->Pool[0];

   // Beds
   iBeds = atoin(pChar->Beds, SIZ_CHAR_BEDS);
   if (iBeds > 0)
   {
      sprintf(acTmp, "%*u", SIZ_BEDS, iBeds);
      memcpy(pOutbuf+OFF_BEDS, acTmp, SIZ_BEDS);
   }

#ifdef _DEBUG
   //if (!memcmp(pChar->Apn, "009600002", 9) )
   //   lTmp = 0;
#endif

   // Bath
   iFBath = atoin(pChar->FBaths, SIZ_CHAR_BATHS);
   if (iFBath > 0)
   {
      sprintf(acTmp, "%*u", SIZ_BATH_F, iFBath);
      memcpy(pOutbuf+OFF_BATH_F, acTmp, SIZ_BATH_F);
      
      *(pOutbuf+OFF_BATH_4Q) = pChar->Bath_4Q[0];
   }

   // Half bath
   iHBath = atoin(pChar->HBaths, SIZ_CHAR_BATHS);
   if (iHBath > 0)
   {
      sprintf(acTmp, "%*u", SIZ_BATH_H, iHBath);
      memcpy(pOutbuf+OFF_BATH_H, acTmp, SIZ_BATH_H);

      *(pOutbuf+OFF_BATH_2Q) = pChar->Bath_2Q[0];
   }

   // 3/4 bath
   *(pOutbuf+OFF_BATH_3Q) = pChar->Bath_3Q[0];

   // Stories
   memcpy(pOutbuf+OFF_STORIES, pChar->Stories, SIZ_STORIES);

   // Central Heating-Cooling
   *(pOutbuf+OFF_HEAT) = pChar->Heating[0];
   *(pOutbuf+OFF_AIR_COND) = pChar->Cooling[0];

   // Fireplace
   if (pChar->Fireplace[0] == 'Y')
      memcpy(pOutbuf+OFF_FIRE_PL, " 1", 2);

   // Roof
   *(pOutbuf+OFF_ROOF_MAT) = pChar->RoofMat[0];

   // Water
   *(pOutbuf+OFF_WATER) = pChar->HasWater;

   // Sewer
   *(pOutbuf+OFF_SEWER) = pChar->HasSewer;

   // View
   *(pOutbuf+OFF_VIEW) = pChar->View[0];

   // Zoning
   //if (pChar->Zoning[0] > ' ')
   //   memcpy(pOutbuf+OFF_ZONE, pChar->Zoning, SIZ_ZONE);

   // Quality Class
   *(pOutbuf+OFF_BLDG_CLASS) = pChar->BldgClass;
   *(pOutbuf+OFF_BLDG_QUAL) = pChar->BldgQual;
   memcpy(pOutbuf+OFF_QUALITYCLASS, pChar->QualityClass, SIZ_CHAR_QCLS);

   // Acres
   lTmp = atoin(pChar->LotSqft, SIZ_CHAR_SQFT);
   if (lTmp > 0)
   {
      // Lot Sqft
      sprintf(acTmp, "%*u", SIZ_LOT_SQFT, lTmp);
      memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);

      // Format Acres
      lTmp = atoin(pChar->LotAcre, SIZ_CHAR_SQFT);
      sprintf(acTmp, "%*u", SIZ_LOT_ACRES, lTmp);
      memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);
   }

   // Number of units
   lTmp = atoin(pChar->Units, SIZ_CHAR_UNITS);
   if (lTmp > 1)
   {
      sprintf(acTmp, "%*u", SIZ_UNITS, lTmp);
      memcpy(pOutbuf+OFF_UNITS, acTmp, SIZ_UNITS);
   }

   lCharMatch++;

   // Get next Char rec
   pRec = fgets(acRec, 2048, fdChar);

   return 0;
}

/****************************** Men_MergeRollCsv *****************************
 *
 * Return 0 if successful, < 0 if error
 *        1 retired record, not use
 *
 *****************************************************************************/

int Men_MergeRollCsv(char *pOutbuf, char *pRollRec, int iLen, int iFlag)
{
   char     acTmp[256], acTmp1[256];
   LONGLONG lTmp;
   double   dTmp;
   int      iRet=0, iTmp;

   // Remove extra spaces
   blankRem(pRollRec);

   // Parse roll record
   iRet = ParseStringNQ(pRollRec, cDelim, MAX_FLD_TOKEN, apTokens);
   if (iRet < R_COLS)
   {
      LogMsg("***** Error: bad input record for APN=%s", apTokens[R_APN]);
      return -1;
   }
#ifdef _DEBUG
   //if (!memcmp(apTokens[R_APN], "164760072", 9) )
   //   iTmp = 0;
#endif

   // Ignore non-taxable parcels - Keep non-taxable parcel during update
   //if (*apTokens[R_ISTAXABLE] == 'N')
   //   return 1;

   // Ignore unsecured parcel 
   if (_memicmp(apTokens[R_ROLLCAT], "Sec", 3))
      return 1;
   // Ignore BPP parcels
   if (strlen(apTokens[R_APN]) < iApnLen)
   {
      LogMsg("*** Skip: %s", apTokens[R_APN]);
      return 1;
   }

   if (iFlag & CREATE_R01)
   {
      // Clear output buffer
      memset(pOutbuf, ' ', iRecLen);

      // Start copying data
      memcpy(pOutbuf, apTokens[R_APN], strlen(apTokens[R_APN]));
      *(pOutbuf+OFF_STATUS) = 'A';

      // Format APN
      iRet = formatApn(apTokens[R_APN], acTmp, &myCounty);
      memcpy(pOutbuf+OFF_APN_D, acTmp, iRet);

      // Create MapLink and output new record
      iRet = formatMapLink(apTokens[R_APN], acTmp, &myCounty);
      memcpy(pOutbuf+OFF_MAPLINK, acTmp, iRet);

      // Create index map link
      if (getIndexPage(acTmp, acTmp1, &myCounty))
         memcpy(pOutbuf+OFF_IMAPLINK, acTmp1, iRet);

      // County code
      memcpy(pOutbuf+OFF_CO_NUM, "23MEN", 5);

      // Year assessed
      memcpy(pOutbuf+OFF_YR_ASSD, myCounty.acYearAssd, 4);

      // Land
      long lLand = atoi(apTokens[R_LAND]);
      if (lLand > 0)
      {
         sprintf(acTmp, "%*u", SIZ_LAND, lLand);
         memcpy(pOutbuf+OFF_LAND, acTmp, SIZ_LAND);
      }

      // Improve
      long lImpr = atoi(apTokens[R_IMPR]);
      if (lImpr > 0)
      {
         sprintf(acTmp, "%*u", SIZ_IMPR, lImpr);
         memcpy(pOutbuf+OFF_IMPR, acTmp, SIZ_IMPR);
      }

      // Other values
      long lFixt  = atoi(apTokens[R_TRADEFIXTURESAMOUNT]);
      long lPP    = atoi(apTokens[R_PERSONALVALUE]);
      long lGrow  = atoi(apTokens[R_LIVINGIMPROVEMENTS]);
      lTmp = lFixt+lPP+lGrow;
      if (lTmp > 0)
      {
         sprintf(acTmp, "%*u", SIZ_OTHER, lTmp);
         memcpy(pOutbuf+OFF_OTHER, acTmp, SIZ_OTHER);

         if (lFixt > 0)
         {
            sprintf(acTmp, "%d         ", lFixt);
            memcpy(pOutbuf+OFF_FIXTR, acTmp, SIZ_FIXTR);
         }
         if (lPP > 0)
         {
            sprintf(acTmp, "%d         ", lPP);
            memcpy(pOutbuf+OFF_PERSPROP, acTmp, SIZ_PERSPROP);
         }
         if (lGrow > 0)
         {
            sprintf(acTmp, "%d         ", lGrow);
            memcpy(pOutbuf+OFF_GR_IMPR, acTmp, SIZ_GR_IMPR);
         }
      }

      // Gross total
      lTmp += (lLand+lImpr);
      if (lTmp > 0)
      {
         sprintf(acTmp, "%*u", SIZ_GROSS, lTmp);
         memcpy(pOutbuf+OFF_GROSS, acTmp, SIZ_GROSS);
      }

      // Ratio
      if (lImpr > 0)
      {
         sprintf(acTmp, "%*u", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
         memcpy(pOutbuf+OFF_RATIO, acTmp, SIZ_RATIO);
      }
   }

   // TRA
   iTmp = atol(apTokens[R_TRA]);
   sprintf(acTmp, "%.6u", iTmp);
   memcpy(pOutbuf+OFF_TRA, acTmp, 6);

   // Is Taxable
   if (*apTokens[R_ISTAXABLE] == 'N')
      *(pOutbuf+OFF_FULL_EXEMPT) = 'Y';

   // Legal
   iTmp = replUnPrtChar(apTokens[R_ASSESSMENTDESCRIPTION], ' ');
   iTmp = updateLegal(pOutbuf, _strupr(apTokens[R_ASSESSMENTDESCRIPTION]));
   if (iTmp > iMaxLegal)
      iMaxLegal = iTmp;

   // Zoning - special case
   if (*(pOutbuf+OFF_ZONE+1) == '|')
      *(pOutbuf+OFF_ZONE+1) = '1';

   // UseCode - being updated at LDR

   // Acres
   dTmp = atof(apTokens[R_ACREAGEAMOUNT]);
   if (dTmp > 0.0)
   {
      // Lot Sqft
      lTmp = (long)(dTmp * SQFT_PER_ACRE);
      sprintf(acTmp, "%*u", SIZ_LOT_SQFT, lTmp);
      memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);

      // Format Acres
      lTmp = (long)(dTmp * ACRES_FACTOR);
      sprintf(acTmp, "%*u", SIZ_LOT_ACRES, lTmp);
      memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);
   }

#ifdef _DEBUG
   //if (!memcmp(apTokens[R_APN], "009000009", 9) )
   //   iTmp = 0;
#endif

   // Owner
   try {
      Men_MergeOwner(pOutbuf);
   } catch(...) {
      LogMsg("***** Exeception occured in Men_MergeOwner()");
   }

   // Situs
   Men_MergeSAdr(pOutbuf);

   // Mailing
   Men_MergeMAdr(pOutbuf);

   return 0;
}

/****************************** Men_Load_RollCsv ******************************
 *
 * Load updated roll file AssessmentRoll.csv (10/06/2019)
 *
 ******************************************************************************/

int Men_Load_RollCsv(int iSkip)
{
   char     acRec[MAX_RECSIZE], acBuf[MAX_RECSIZE], acRollRec[MAX_RECSIZE], acLastApn[32];
   char     acRawFile[_MAX_PATH], acOutFile[_MAX_PATH], acSrtFile[_MAX_PATH], *pTmp;
   int      iRet, iTmp, iRollUpd=0, iNewRec=0, iRetiredRec=0, lCnt=0;
   DWORD    nBytesRead, nBytesWritten;
   BOOL     bRet, bEof;
   HANDLE   fhIn, fhOut;

   LogMsg0("Loading roll update");

   sprintf(acRawFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "S01");
   sprintf(acOutFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "R01");

   // Rename files for processing
   if (_access(acRawFile, 0))
   {
      if (!_access(acOutFile, 0))
         rename(acOutFile, acRawFile);
      else
      {
         LogMsg("***** Missing input file %s.  Please recheck!", acRawFile);
         return -1;
      }
   }

   // Sort input
   sprintf(acSrtFile, "%s\\RIV\\Men_Roll.srt", acTmpPath);
   sprintf(acRec, "S(#1,C,A) DEL(%d) F(TXT)", cDelim);
   iTmp = sortFile(acRollFile, acSrtFile, acRec);

   // Open roll file
   LogMsg("Open Roll file %s", acSrtFile);
   fdRoll = fopen(acSrtFile, "r");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", acSrtFile);
      return 2;
   }

   LogMsg("Open Char file %s", acCChrFile);
   fdChar = fopen(acCChrFile, "r");
   if (fdChar == NULL)
   {
      LogMsg("***** Error opening Char file: %s\n", acCChrFile);
      return -2;
   }

   // Open Input file
   LogMsg("Open input file %s", acRawFile);
   fhIn = CreateFile(acRawFile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhIn == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening input file: %s\n", acRawFile);
      return -3;
   }

   // Open Output file
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening output file: %s\n", acOutFile);
      return 4;
   }

   // Get 1st rec
   pTmp = fgets((char *)&acRollRec[0], MAX_RECSIZE, fdRoll);    
   bEof = (pTmp ? false:true);

   // Init variables
   iNoMatch=iBadCity=iBadSuffix=0;
   strcpy(acLastApn, "000000000");

   // Copy skip record
   memset(acBuf, ' ', iRecLen);
   while (iSkip-- > 0)
   {
      ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);
      // Ignore record start with 99999999
      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
   }

   // Merge loop
   while (!bEof)
   {
      bRet = ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);

      // Check for EOF
      if (!bRet)
      {
         LogMsg("***** Error reading input file %s (%f)", acRawFile, GetLastError());
         break;
      }

      // EOF ?
      if (!nBytesRead)
         break;

NextRollRec:
#ifdef _DEBUG
      //if (!memcmp(acRollRec, "164760072", 9) )
      //   iTmp = 0;
#endif

      iTmp = memcmp(acBuf, &acRollRec[iSkipQuote], iApnLen);
      if (!iTmp)
      {
         // Create R01 record from roll data
         iRet = Men_MergeRollCsv(acBuf, acRollRec, MAX_RECSIZE, UPDATE_R01);
         if (!iRet)
         {
            // Merge Char
            if (fdChar)
               iTmp = Men_MergeStdChar(acBuf);

            iRollUpd++;
         }

         // Read next roll record
         pTmp = fgets(acRollRec, MAX_RECSIZE, fdRoll);

         if (!pTmp)
            bEof = true;         // Signal to stop
      } else if (iTmp > 0)       // Roll not match, new roll record?
      {
         if (bDebug)
            LogMsg0("New roll record : %.*s (%d)", iApnLen, acRollRec, lCnt);

         // Create new R01 record
         iRet = Men_MergeRollCsv(acRec, acRollRec, MAX_RECSIZE, CREATE_R01);
         if (!iRet)
         {
            // Merge Char
            if (fdChar)
               iTmp = Men_MergeStdChar(acRec);

            if (memcmp(acLastApn, acRec, 14))
            {
               memcpy(acLastApn, acRec, 14);
               bRet = WriteFile(fhOut, acRec, iRecLen, &nBytesWritten, NULL);
               iNewRec++;
            } else
            {
               LogMsg("---> Duplicate APN: %.14s", acLastApn);
               bDupApn = true;
            }
         }
         lCnt++;

         // Get next roll record
         pTmp = fgets(acRollRec, MAX_RECSIZE, fdRoll);
         if (!pTmp)
            bEof = true;    // Signal to stop
         else
            goto NextRollRec;
      } else
      {
         // Record may be retired
         if (bDebug)
            LogMsg0("Retired record? : R01->%.*s < Roll->%.*s (%d)", iApnLen, acBuf, iApnLen, (char *)&acRollRec[1], lCnt);
         iRetiredRec++;

         continue;
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
      if (!iRet)
      {
         if (memcmp(acLastApn, acBuf, 14))
         {
            memcpy(acLastApn, acBuf, 14);
            bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
            if (!bRet)
            {
               LogMsg("Error occurs: %d\n", GetLastError());
               break;
            }
         }
      }
   }

   // Do the rest of the file
   while (!bEof && isdigit(acRollRec[0]))
   {
      // Create new R01 record
      iRet = Men_MergeRollCsv(acRec, acRollRec, MAX_RECSIZE, CREATE_R01);

      if (!iRet)
      {
         if (bDebug)
            LogMsg0("New roll record : %.*s (%d) ", iApnLen, acRollRec, lCnt);

         // Merge Char
         if (fdChar)
            iRet = Men_MergeStdChar(acRec);

         if (memcmp(acLastApn, acRec, 14))
         {
            memcpy(acLastApn, acRec, 14);
            bRet = WriteFile(fhOut, acRec, iRecLen, &nBytesWritten, NULL);
            iNewRec++;
         } else
         {
            LogMsg("---> Duplicate APN: %.14s", acLastApn);
            bDupApn = true;
         }
      }
      lCnt++;

      // Get next roll record
      pTmp = fgets(acRollRec, MAX_RECSIZE, fdRoll);

      if (!pTmp)
         bEof = true;    // Signal to stop
   }

   // Close files
   if (fdRoll)
      fclose(fdRoll);
   if (fdChar)
      fclose(fdChar);
   if (fhOut)
      CloseHandle(fhOut);

   LogMsg("Total output records:       %u", lCnt);
   LogMsg("Total retired records:      %u", iRetiredRec);
   LogMsg("Total bad-city records:     %u", iBadCity);
   LogMsg("Total bad-suffix records:   %u\n", iBadSuffix);

   LogMsg("Number of Char matched:     %u", lCharMatch);
   LogMsg("Number of Char skiped:      %u\n", lCharSkip);

   printf("\nTotal output records: %u", lCnt);

   lRecCnt = lCnt;
   return 0;
}

/******************************************************************************
 *
 * -L = load lien
 * -U = load update
 * -Xl= extract lien
 * -La= load data for assessor
 *
 * Regular update: -CMEN -U -Xsi -T
 *
 ******************************************************************************/

//#define _SKIP 1
int loadMen(int iSkip)
{
   int   iRet, iTmp;
   char  acDefFile[_MAX_PATH], acTmpFile[_MAX_PATH], acTmp[256];

   iApnLen = myCounty.iApnLen;

   // Load tax
   if (iLoadTax == TAX_LOADING)              // -T or -Lt
   {
      // Load Tax Agency table
      iRet = GetIniString(myCounty.acCntyCode, "TaxDist", "", acTmpFile, _MAX_PATH, acIniFile);
      if (iRet > 0)
         iRet = LoadTaxCodeTable(acTmpFile);

      iRet = Men_Load_TaxBase(bTaxImport);
      if (!iRet && lLastTaxFileDate > 0)
      {
         iRet = Men_Load_TaxDelq(bTaxImport);
         if (!iRet)
            iRet = updateDelqFlag(myCounty.acCntyCode);

         // To be tested
         GetIniString(myCounty.acCntyCode, "SuppTax", "", acTmp, _MAX_PATH, acIniFile);
         if (acTmp[0] > '0' && !_access(acTmp, 0))
            iRet = Men_Load_TaxSupp(bTaxImport);
      } 
   }

   if (!iLoadFlag && !lOptMisc)
      return 0;

   // Load char file
   if (iLoadFlag & EXTR_ATTR)                      // -Xa
   {
      // 04/26/2022
      iRet = Men_ConvStdCharCsv(acCharFile);
      if (iRet > 0)
         iRet = 0;
   }

   // Load new sale file LDR 2022
   if (iLoadFlag & EXTR_SALE)                      // -Xs
   {
      // Load special table for Sale
      GetIniString(myCounty.acCntyCode, "Sale_Xref", "", acTmpFile, _MAX_PATH, acIniFile);
      iNumDeeds = LoadXrefTable(acTmpFile, (XREFTBL *)&asDeed[0], MAX_DEED_ENTRIES);

      iRet = Men_ExtrSaleCsv();
      if (!iRet)
         iLoadFlag |= MERG_CSAL;
   }

   // Sale extract
//   if (iLoadFlag & EXTR_SALE)                      // -Xs
//   {
//#ifndef _SKIP
//      // Convert sale file ebcdic to ascii
//      sprintf(acTmpFile, "%s\\%s\\%s_Sale.tmp", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
//      LogMsg0("Translate %s to Ascii %s", acSaleFile, acTmpFile);
//      iRet = F_Ebc2Asc(acSaleFile, acTmpFile, iSaleLen);
//      if (iRet < 0)
//      {
//         LogMsg("***** Error converting sale file to ASCII %s to %s", acSaleFile, acTmpFile);
//         return 1;
//      }
//
//      // Sort sale file
//      sprintf(acSaleFile, "%s\\%s\\%s_Sale.asc", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
//      LogMsg("Sort sale file %s to %s", acTmpFile, acSaleFile);
//      sprintf(acTmp, "S(1,%d,C,A,%d,8,C,A,%d,%d,C,A) F(TXT)", HSIZ_APN, HOFF_REC_DATE, HOFF_DEED_NO, HSIZ_DEED_NO);
//      iRet = sortFile(acTmpFile, acSaleFile, acTmp);
//      if (iRet <= 0)
//      {
//         LogMsg("***** Error sorting sale file %s to %s", acTmpFile, acSaleFile);
//         return 1;
//      }
//#else
//      sprintf(acSaleFile, "%s\\%s\\%s_Sale.asc", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
//#endif
//
//      LogMsg0("Extract %s sale history file", myCounty.acCntyCode);
//      iRet = Men_ExtrSale();
//      if (!iRet)         
//         iLoadFlag |= MERG_CSAL;                   // Signal merge sale to R01
//   }

   // Extract NDC recorder sale 
   if (iLoadFlag & EXTR_NRSAL)                     // -Xn
   {
      iRet = GetIniString(myCounty.acCntyCode, "NdcSale", "", acSaleFile, _MAX_PATH, acIniFile);
      if (!_access(acSaleFile, 0))
      {
         iRet = NR_CreateSCSale(myCounty.acCntyCode, acSaleFile, iApnLen);
         if (iRet)
            return iRet;
      } else
      {
         LogMsg("*** Sale file not available.  Please verify: %s", acSaleFile);
         return -1;
      }
   }

#ifndef _SKIP
   if (iLoadFlag & (LOAD_LIEN|LOAD_UPDT))
   {
      // Translate Secured Roll file ebcdic to ascii
      sprintf(acTmpFile, "%s\\%s\\%s_Roll.tmp", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
      GetIniString(myCounty.acCntyCode, "RollDef", "", acDefFile, _MAX_PATH, acIniFile);
      if (_access(acDefFile, 0))
      {
         LogMsg("***** Error: missing definition file %s", acDefFile);
         return 1;
      }

      GetIniString(myCounty.acCntyCode, "RollFile", "", acRollFile, _MAX_PATH, acIniFile);

      LogMsg0("Translate %s to Ascii %s", acRollFile, acTmpFile);
      iRet = F_Ebc2Asc(acRollFile, acTmpFile, acDefFile, 0);
      if (iRet < 0)
      {
         dispError(iRet, acRollFile, acTmpFile, acDefFile);
         return 1;
      }

      // Sort roll file
      sprintf(acRollFile, "%s\\%s\\%s_Roll.asc", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);

      sprintf(acTmp, "S(%d,%d,C,A) F(TXT)", ROFF_APN+1, RSIZ_APN);
      LogMsg("Sort roll file %s to %s [%s]", acTmpFile, acRollFile, acTmp);
      iRet = sortFile(acTmpFile, acRollFile, acTmp);
      if (iRet <= 0)
      {
         LogMsg("***** Error sorting sale file %s to %s", acSaleFile, acTmpFile);
         return 1;
      }
   }
#else
   //if (iLoadFlag & (LOAD_LIEN|EXTR_LIEN))
   //   sprintf(acRollFile, "%s\\%s\\%s_Lien.asc", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   //else
      sprintf(acRollFile, "%s\\%s\\%s_Roll.asc", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
#endif

   iRet = 0;
   if (lOptProp8 == MYOPT_EXT)                        // -X8
      iRet = Men_ExtrProp8();

   if (iLoadFlag & EXTR_LIEN)                         // -Xl
   {
      //iRet = Men_ExtrLien();
      // 2021
      iRet = Men_ExtrLienCsv(myCounty.acCntyCode);
   }

   if (iLoadFlag & LOAD_LIEN)                         // -L
   {
      // Create PQ4 file
      LogMsg0("Load %s LDR roll", myCounty.acCntyCode);
      //iRet = Men_Load_LDR(iSkip);
      // 2021
      iRet = Men_Load_LDR_Csv(iSkip);
   } else if (iLoadFlag & LOAD_UPDT)                  // -U
   {
      // Create PQ4 file
      LogMsg0("Load %s roll update file", myCounty.acCntyCode);
      //iRet = Men_Load_Roll(iSkip);
      iRet = Men_Load_RollCsv(iSkip);
   } else if (lOptMisc & M_OPT_ROLLCOR)               // -Uc
   {
      GetIniString(myCounty.acCntyCode, "RollCorr", "", acRollFile, _MAX_PATH, acIniFile);
      if (!_access(acRollFile, 0))
      {
         LogMsg0("Extract roll correction values");
         iRet = Men_ExtrRollCorrection();

         if (!iRet)
         {
            LogMsg0("Load %s roll correction file", myCounty.acCntyCode);
            iRet = Men_Load_RollCorrection(iSkip);
         }
      }
   }

   // Apply cum sale file to R01
   if (!iRet && (iLoadFlag & MERG_CSAL) )             // -Ms
   {
      // Apply Men_Sale.sls to R01 file
      iRet = ApplyCumSale(iSkip, acCSalFile, false, SALE_USE_SCUPDXFR, CLEAR_UPD_SALE);
   }

   // Apply NDC sale file to R01
   if (!iRet && (iLoadFlag & UPDT_XSAL))              // -Mn
   {
      // Apply Men_Ash.sls to R01 file
      GetIniString("Data", "ASH_File", acESalTmpl, acESalTmpl, _MAX_PATH, acIniFile);
      sprintf(acTmpFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "sls");
      iRet = ApplyCumSaleNR(iSkip, acTmpFile, SALE_USE_SCUPDXFR);
      if (!iRet)
         iLoadFlag |= LOAD_UPDT;
   }

   if (!iRet && (iLoadFlag & (LOAD_ASSR|UPDT_ASSR)))  // -La, -Ua
   {
      strcpy(acTmpFile, acSaleFile);

      // Resort sale file in descending order
      sprintf(acSaleFile, "%s\\%s\\%s_Sale.dsc", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
#ifndef _SKIP
      LogMsg("Sort sale file %s to %s", acTmpFile, acSaleFile);
      sprintf(acTmp, "S(1,%d,C,A,%d,8,C,D,%d,%d,C,D) F(TXT)", HSIZ_APN, HOFF_REC_DATE, HOFF_DEED_NO, HSIZ_DEED_NO);
      iTmp = sortFile(acTmpFile, acSaleFile, acTmp);
      if (iTmp <= 0)
      {
         LogMsg("***** Error sorting sale file %s to %s", acTmpFile, acSaleFile);
         return 1;
      }
#endif
      // Create history file
      iTmp = Men_Create_AHist();
      asRecCnt[0].RecCnt = iTmp;
      strcpy(asRecCnt[0].Prefix, "MENH");

#ifndef _SKIP
      // Convert unsecured file ebcdic to ascii
      GetIniString(myCounty.acCntyCode, "UnsDef", "", acDefFile, _MAX_PATH, acIniFile);
      if (_access(acDefFile, 0))
      {
         LogMsg("***** Error: missing definition file %s", acDefFile);
         return 1;
      }

      if (iLoadFlag & (LOAD_LIEN|EXTR_LIEN))
         GetIniString(myCounty.acCntyCode, "UnsLienFile", "", acTmpFile, _MAX_PATH, acIniFile);
      else
         GetIniString(myCounty.acCntyCode, "UnsFile", "", acTmpFile, _MAX_PATH, acIniFile);
      sprintf(acUnsRoll, "%s\\%s\\%s_Uns.asc", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
      LogMsg("Translate %s to Ascii %s", acTmpFile, acUnsRoll);
      iRet = F_Ebc2Asc(acTmpFile, acUnsRoll, acDefFile, 0);
      if (iRet < 0)
      {
         dispError(iRet, acTmpFile, acUnsRoll, acDefFile);
         return 1;
      }
#else
      sprintf(acUnsRoll, "%s\\%s\\%s_Uns.asc", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
#endif

      // Create assr unsecured roll
      iTmp = Men_Create_AUns();
      asRecCnt[1].RecCnt = iTmp;
      strcpy(asRecCnt[1].Prefix, "MENU");

      // Create assr roll
      iTmp = Men_Create_ASec();
      asRecCnt[2].RecCnt = iTmp;
      strcpy(asRecCnt[2].Prefix, "MENS");

      if (iTmp <= 0)
         iRet = -1;
   }

   return iRet;
}