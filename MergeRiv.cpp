/******************************************************************************
 *
 * Input files:
 *    - ASX100C                  (lien file, 162-byte ASCII, multi record)  2012-
 *    - RIVEXTCT                 (lien file, 160-byte ebcdic, multi record) 1999-2011
 *    - RIVKEYIN                 (roll update file, 622-byte ebcdic)
 *    - RIVPRCH                  (char file, 150-byte ebcdic)
 *    - RIVSALES                 (sale file, 318-byte ebcdic)
 *    - RIV_SALE_NEWCUM.SRT      (cumulative sale up to 20021231)
 *
 * Roll update 2021
 *    - AssessmentRoll.csv          (roll update)
 *    - PropertyCharacteristics.csv (char file)
 *    - SalesDB.csv                 (sale file)
 *
 * LDR 2019:
 *    - Certified2019AssessmentRoll.csv 
 *      This file have broken record and need rebuild before processing.
 *    - SalesDB.csv
 *
 * Following are tasks to be done here:
 *    - Convert ebcdic to ascii
 *    - Normal update: LoadOne -U
 *    - Load Lien: LoadOne -L (make sure rename SRIV.S01 to SRIV.O01 before running)
 *    - Then run : LoadOne -U
 *
 * Notes:
 *    - When new LDR arrives, update cum sale using prior month sale file (function
 *      to be implemented) because new sale file will only contain last 2 years.
 *
 *    - To load LDR, always run in 2 passes, why?.  First run -L then run -U
 *    - Sale date only has yyyymm, append "00" to make it yyyymm00.
 *
 *      06/16/2022: 
 *      So as soon as you are able, we need to grab all values for the PINs ending with �BPP� from last year�s 
 *      closed roll and combine them into OTHERVALUE for the main secured parcel. We also want to show the 
 *      breakout in the V file, which is usually personal property and fixtures. Then we�ll be ready to do 
 *      this going forward for this year. You�ll also see a similar layout with the equalized roll when we 
 *      get to that point later in the year.
 *
 * Revision:
 * 07/07/2006 1.2.25    Modify for 2006 LDR
 * 07/13/2007 1.4.17.2  Modify for 2007 LDR
 * 03/18/2008 1.5.7.1   Use standard function to update usecode
 * 03/24/2008 1.5.7.2   Format IMAPLINK.
 * 07/18/2008 8.0.3     Modify for 2008 LDR.  Adding chars to LDR as well.
 * 01/08/2009 8.5.5.1   Add acLatestSale to track latest sale date in Sale update history.
 * 01/08/2009 8.5.5.2   Fix merge sale bug.
 * 03/23/2009 8.7.1.1   Format STORIES as 99.9.  We no longer multiply by 10.
 * 05/11/2009 8.8       Set full exemption flag
 * 06/29/2009 9.1.0     Modify Riv_MergeLien() to add other values.
 * 07/10/2009 9.1.2     Remove MAX_LIENREC and use LienRecSize from INI file. Add
 *                      Riv_ExtrLien() and Riv_CreateLienRec().
 * 12/09/2009 9.3.5     Set Prop8 flag using Prop8 Assessment file.
 *                      Also add Riv_MergeProp8() to merge prop8 data into roll file
 * 12/23/2009 9.3.6     Use lOptProp8 instead of bSetProp8 flag.
 * 02/12/2010 9.4.2     Add Riv_ExtractProp8()
 * 02/24/2010 9.5.0     Fix CARE_OF issue. Add Riv_MergeSaleChar() to populate chars
 *                      from sale file.  If exist, use this to overide data from CHAR file.
 *                      Use SCSAL_REC format for cum sale.
 * 04/23/2010 9.5.5     Remove Riv_FormatCSale() and use NumOfPrclXfer instad of MultiSale_Flg for group sale.
 * 07/15/2010 10.1.3    Add new Riv_MergeSaleChar() to merge CHARS from extracted file.
 *                      Modify Riv_MergeLien() to capture full legal. Modify Riv_Load_LDR() to
 *                      use sale chars to update Zoning & BldgCls.  Add Riv_ExtrCharSale()
 *                      to extract CHARS from sale file.  Add -Xa option to extract chars from sale file.
 * 04/08/2011 10.5.3    Fix MAPLINK issue by using Reference APN in Rec23 of LDR file
 *                      or ParcelNo in update roll file.
 * 05/24/2011 10.5.13   Fix bad character in Riv_Load_LDR() and Riv_MergeRoll().
 * 07/07/2011 11.0.2    Add S_HSENO. No range StrNum found. Update TRANSFER using sale data.
 * 07/15/2012 12.1.2    Modify loadRiv(), Riv_ExtrLien() and Riv_Load_LDR() to handle new LDR file ASX100C.
 *                      Add TaxCode, Block & Lot to R01 output.
 * 08/01/2012 12.2.4    Fix bug that reset iRollLen to 160.  When loading update roll, do not
 *                      set state="CA" unless city or zipcode is present.
 * 10/25/2012 12.3.2    Skip unsecured parcels book 000-001.
 * 11/08/2012 12.3.3    Modify Riv_FormatSaleRec() to standardize DOCNUM format.
 * 04/03/2013 12.5.2    Adding UNITS to R01 in Riv_MergeRoll()
 * 09/10/2013 13.7.14   Adding roof material
 * 10/04/2013 13.10.3   Adding 3/4 bath and original Fbath to R01 at position 565
 * 10/07/2013 13.10.4   Drop original FBath. Modify Riv_MergeCOwner() and set ETAL_FLG.
 *                      Modify Riv_MergeRoll() to update CareOf.
 * 10/11/2013 13.11.0   Adding 1-4 Qbaths to R01.
 * 10/15/2013 13.11.2   Use updateVesting() to update Vesting and Etal flag.
 * 09/26/2014 14.6.0    Add -G option to process GrGr and Riv_ExtractGrGr().
 * 10/09/2014 14.7.0    Modify Riv_MergeSaleChar() and only merge data not avail in CHAR file.
 *                      Modify Riv_MergeSaleRec() to standardize DocNum to 7 bytes numeric.
 *                      Modify Riv_FormatSaleRec() to update DocType and set MultiSale_Flag correctly.
 *                      Modify Riv_LoadGrGr() to move GRGR file to backup folder after successfully 
 *                      complete and fix DocType translation by using XrefCodeIndexEx() instead of XrefCodeIndex().
 *                      Add -Fy option to fix DocNum (one time run only). Can be used to reload old file.
 *                      Add feature to skip ASCII conversion if ASC file is newer then raw file.
 * 01/14/2015 14.10.4   Modify SORT command in Riv_UpdateSaleHist() to remove duplicate.
 *                      Add Riv_LoadGrGrMdb() to process single MDB file and modify load GrGr process.
 * 02/16/2016 15.6.0    Fix swap name in Riv_MergeCOwner() and clean up Riv_MergeMAdr().
 * 03/26/2016 15.8.1    Add option to load tax data.
 * 04/20/2016 15.9.1    Use updateTaxAgency() to merge agency list to Tax_Agency table.
 * 08/31/2016 16.2.3    Add Riv_MergeTaxPaid() to merge paid amounts to Tax_Base
 * 09/01/2016 16.2.4    Create empty Owner & Delq tables.
 * 10/22/2016 16.5.1    Modify Riv_Load_TaxPaid() due to change in TAXPAID definition
 * 12/04/2016 16.7.6    Modify Riv_FormatSaleRec() to set Full/Partial sale code and MultiSale_Flg
 * 01/07/2017 16.8.7    Add BillNum and DueDate to TaxBase record.
 * 02/01/2017 16.10.2   Use defined constant to set PaidStatus.
 * 02/06/2017 16.10.4   Modify Riv_UpdateSAdr() to locate city based on zipcode.  
 *                      Add Riv_MergeOwner() to format Owner1 & 2 seperately (per customer request).
 * 03/12/2017 16.12.0   Modify Riv_MergeTaxPaid() to add instStatus, updte TotalDue & Penalty.
 * 06/05/2017 16.14.16  Fix Situs problem in Riv_MergeRoll().  Add -Lz to load zipcode file from EDX.
 *                      Add Riv_Load_TaxDelq() to load redemption file ASPYMIDY.
 * 06/08/2017 16.14.17  Finetune Riv_Load_TaxDelq() and related fundtions to load Delq.
 * 07/03/2017 17.1.1    LDR file is now in ASCII.  Modify Riv_MergeLOwner() to update vesting.
 *                      Remove bad and known bad char in Riv_MergeSAdr() & Riv_MergeLien().
 *                      Modify loadRiv() to bypass EBCDIC translation for LDR processing.
 * 08/30/2017 17.2.2    Set TaxYear base on value from INI file.  Modify Riv_ParseTaxDetail() to set TaxYear.
 * 08/31/2017 17.2.2.2  Fix Riv_MergeTaxPaid() to correct TotalDue calculation.
 * 11/01/2017 17.4.4    Fix bug in Riv_MergeOwner() caused by blankRem().
 * 03/07/2018 17.6.8    Check for missing APN field in Riv_LoadGrGrMdb() to avoid crash.
 * 07/03/2018 18.0.1    Fix sort error in Riv_ExtrCharSale() by increase DUPOUT buffer.
 * 08/18/2018 18.3.2    Modify Riv_LoadGrGr() to exit and report error immediately if missing field.
 * 12/03/2018 18.6.6    Modify Riv_ParseTaxBase(() to save TotalDue.  Modify Riv_MergeTaxPaid() to update
 *                      TaxBase only when TotalTax in TaxPaid is the same as TaxBase.  As of today, the tax paid
 *                      file has not been updated with current tax roll.  It still contains 2017 data.
 *            18.6.6.1  Modify Riv_Load_Roll() to remove sale update.
 *            18.6.6.3  Modify Riv_UpdateSAdr() toremove situs before updating. Fix bug in Riv_MergeRoll().
 * 12/15/2018 18.7.2    Fix bug in Riv_UpdateSAdr()
 * 05/15/2019 18.12.2   Add -Xa and Riv_ConvStdChar().  Disable Riv_ExtrCharSale() in load_Riv().
 * 06/16/2019 18.12.10  Fix Riv_UpdateSAdr() to assign city name based on zipcode if available.
 *                      Modify Riv_MergeMAdr() to fix known split of UNIT# between Addr1 & Addr2.
 *                      Remove -Lz option and use RIV_Zipcode table from Production.
 * 06/20/2019 18.13.0   Fix bug in Riv_UpdateSAdr().  Fix Zoning '|' to '1' in Riv_GetSaleChar().
 *                      Add Riv_MergeStdChar().  Modify Riv_Load_Roll() & Riv_Load_LDR() to use  
 *                      Riv_MergeStdChar() instead of Riv_MergeChar().
 * 08/10/2019 19.0.6    Add Riv_MergeLOwner1(), Riv_MergeMailing(), Riv_MergeSitus(), Riv_MergeLien1(),
 *                      Riv_MergeOldInfo(), Riv_Load_LDR1(), Riv_ExtrSale() to support new LDR and sale files.
 *                      Modify Riv_LoadGrGrMdb() to fix bad char in Grantor/Grantee name.
 * 08/21/2019 19.0.8    Bug fix Riv_MergeMailing() what messed up CareOf.
 * 08/24/2019 19.1.1    Add Riv_ConvStdCharCsv() to process new CHAR file Assessor_PropertyCharacteristics_Table.csv
 *                      Modify Riv_MergeOldInfo() to merge more CHAR fields from old file which are not included in the new one.
 * 09/24/2019 19.2.2    Add Riv_MergeAttr() to merge attribute only when -Ma is used.
 * 09/25/2019 19.2.2.1  Add View & Zoning check to Riv_ConvStdCharCsv().
 * 10/07/2019 19.2.3    Add Riv_MergeOwner(pOutbuf), Riv_MergeMAdr(pOutbuf), , Riv_MergeSAdr(pOutbuf), Riv_ConvStdCharCsv(),
 *                      Riv_MergeRollCsv(), and Riv_Load_RollCsv() to process new files from RIV.  Modify Riv_ExtrSale()
 *                      to filter out records with invalid date.
 * 10/08/2019 19.2.3.1  Fix Riv_MergeStdChar() to use HasWater & HasSewer instead of Water & Sewer.
 *                      Fix bug in loadRiv() to use rebuilt roll file instead of county roll file.
 * 10/19/2019 19.2.4    Add Riv_ParseMAdr1() to replace R01.arseMAdr1().  Modify Riv_MergeMailing() & Riv_MergeSAdr()
 *                      to fix UNITNO problem.
 * 11/07/2019 19.4.3    Modify Riv_Load_RollCsv() to sort roll file before processing.
 *                      Change parameter on RebuildCsv() due to roll layout change.
 * 11/18/2019 19.5.3    Modify Riv_MergeOwner() & Riv_MergeLOwner1() to remove '|' from owner name.
 *                      Modify Riv_MergeLien1() to check for TotalExe not greater than total assessed amount
 *                      and set FullExe to 'Y' if they are equal. 
 *                      Modify Riv_MergeOldInfo() to change Zoning which includes '|' to '1'.
 * 12/05/2019 19.5.9    Add Riv_Load_TaxRoll() and related fulctions to process Extended Roll tax file.
 * 01/18/2020 19.6.0    Modify Riv_Load_RollCsv() to skip duplicate APN.  Fix UNIT# in Riv_MergeSAdr().
 *            19.6.0.1  Bug fix.
 * 01/25/2020 19.6.2    Add Riv_Load_ExtTaxRoll(), Riv_Load_TaxPaidCsv() & Riv_Load_TaxDelqCsv()
 *                      Modify Riv_ParseTaxRoll() and Riv_MergeTaxPaid() to update paid/unpaid data.
 * 01/27/2020 19.6.3    Rebuild delq file before processing.
 * 03/04/2020 19.8.1    Modify Riv_MergeStdChar() & Riv_ConvStdCharCsv() to add YrBlt, leave EffYr alone (new layout).
 * 04/10/2020 19.8.10   Modify Riv_MergeStdChar() & Riv_ConvStdCharCsv() to add LotSqft & LotAcre (new fields) 
 * 04/21/2020 19.8.12   Fix bug in Riv_MergeTaxPaid() that creates bad TotalDue.
 * 07/11/2020 20.1.4    Correct exemption code and ignore transfer data in Riv_MergeLien1().
 *                      Remove duplicate APN in Riv_Load_LDR1()
 * 07/17/2020 20.1.5    Modify Riv_Load_LDR1() to fix bug that first record is missing.
 * 07/28/2020 20.2.3    Modify Riv_MergeLien1() & Riv_MergeRollCsv() to ignore non-taxable parcels.
 * 08/10/2020 20.2.8    Modify Riv_MergeMailing() to clean up mail addr.  Modify Riv_MergeSitus()
 *                      to add known city name based on zipcode.
 * 08/14/2020 20.0.10   Add Riv_MergeLien2() to get new county USECODE.  Store long UseCode in USE_COX
 *                      and brief version at USE_CO.
 * 08/17/2020 20.2.11   Modify Riv_MergeRollCsv() to keep non-taxable parcel in monthly update.
 *                      Allow foreign mail city to overflow into state & zip.
 * 08/21/2020 20.2.12   Change RoofType to RoofMat.
 * 10/28/2020 20.3.6    Modify Riv_MergeOldInfo() to populate PQZoning.
 * 11/06/2020 20.4.2    Fix Other Exempt bug in Riv_MergeLien2() by adding all of them to TOTAL_EXE.
 * 12/10/2020 20.5.5    APN in sale & roll files are now without zero prefix.  We have to modify Riv_ExtrSale()
 *                      to format APN properly.  Add Riv_RebuildCsv() to customize for RIV only.
 *                      Modify Riv_MergeRollCsv() to reformat TRA.
 * 01/28/2021 20.7.2    Modify Riv_ParseTaxRoll() to set default BillType=BILLTYPE_SECURED
 * 02/20/2021 20.7.7    Modify Riv_MergeLien2() to include non-taxable parcels.
 * 03/10/2021 20.7.9    Add Riv_Load_TaxCurrent() & Riv_CreateTaxItems() to load new county tax files.
 *                      Fix Riv_MergeOwner() to populate Owner field with owner2 if owner1 is blank.
 * 03/29/2021 20.7.12   Modify Riv_CreateTaxItems() to adjust misalignment.
 *                      Add Riv_AddUnits() to merge #Units to STDCHAR file.
 * 03/30/2021 20.7.12.1 Remove unused code.
 *            20.7.12.2 Modify Riv_MergeStdChar() to merge #Units to R01.
 * 04/01/2021 20.7.13   Modify Riv_CreateTaxItems() & Riv_Load_TaxCurrent() to ignore double quote to support
 *                      new tax files as of 04/01/2021.
 * 04/07/2021 20.7.15   Modify Riv_CreateTaxItems() to remove hyphen from ta_code.  This will fix 
 *                      Agency search in PQWeb.
 * 04/20/2021 20.7.16   Modify Riv_MergeRollCsv() & Riv_MergeLien2() to ignore unsecured parcels (book 000-008)
 * 05/05/2021 20.7.17   Modify Riv_MergeRollCsv() to ignore BPP parcels.
 * 07/02/2021 21.0.1    Modify Riv_MergeMailing() to remove unprintable char in mail city.
 *                      Modify Riv_MergeStdChar() to populate QualityClass.
 *                      Modify Riv_MergeSitus() to fix problem where zipcode is in the city name field.
 * 07/06/2021 21.0.2    Add NonTax=Y|N to [RIV] section. Modify Riv_Load_LDR1() & Riv_MergeLien2() to allow 
 *                      option to include/exclude non-taxable parcels.
 * 08/11/2021 21.1.2    Fix bug in Riv_MergeOldInfo() that overwrite Zoning.
 * 10/05/2021 21.2.7    Add special case in Extr_Sale() for 564062028
 * 02/18/2022 21.4.13   Add Riv_ExtrLegal() to extract legal description for a specific list of APN.
 * 03/03/2022 21.6.0    Add -Xf & -Mf options.  Add Riv_ExtrVal() to extract values from equalized file.
 * 04/11/2022 21.8.8    Add -Xl option and Riv_ExtrLienCsv()
 * 05/03/2022 21.9.1    Modify Riv_MergeSAdr() to add situs zip to S_CTY_ST_D
 * 06/17/2022 21.9.4    Add Riv_Load_LDR2() to process LDR file.  This version add values from BPP record
 *                      to the main parcel.
 * 06/28/2022 21.9.6    Modify Riv_ConvStdCharCsv() to assign ActualArea to Sqft_Above2nd (for Black Knight).
 * 06/30/2022 22.0.0    Add Riv_ExtrChar() to create XC module.
 * 07/14/2022 22.0.1    Modify loadRiv() to filter out extended APN with '-' since it may cause duplicate.
 * 07/21/2022 22.0.2    Modify Riv_MergeLien2() & Riv_ExtractOtherValues() to add LATEPEN to R01 record.
 * 07/28/2022 22.1.2    Modify Riv_MergeSitus() & Riv_MergeSAdr() to copy UnitNo from mail to situs only when HO_FL=1.
 * 09/22/2022 22.2.4    Fix bug in Riv_MergeMAdr() that cut off Unit#.
 * 10/20/2022 22.2.9    Add Riv_LoadGrGrCsv() and modify Riv_LoadGrGr() to support new CSV format.
 * 10/31/2022 22.2.11   Remove option to merge final value (it's moved to LoadOne.cpp).
 * 12/05/2022 22.4.0    Modify Riv_RebuildCsv() to replace '-' with 'X' in APN so we can sort properly.
 *                      Modify Riv_ConvStdCharCsv() to fix APN extension. Modify Riv_MergeLien2() to put LIVINGIMPR
 *                      on OTH_IMPR instead of FIXTR_RP. Modify Riv_ExtractOtherValues() to add exemption.
 *                      Modify Riv_Load_LDR2() to handle BPP and APN extension.  Modify Riv_ExtrLienCsv() and add Riv_UpdateLienRecCsv() 
 *                      to add BPP info to LIENEXTR record.  See INI file for special handling of value file EqualizedRoll.txt
 * 12/08/2022 22.4.1    Add -Mf option to merge final value (RIV specific)
 * 12/14/2022 22.4.3    Modify Riv_Load_RollCsv() to bypass bad records.
 * 02/08/2023 22.4.6    Modify Riv_LoadGrGrCsv() due to change in GRGR file layout.  The county has remove field CNTYDOCNUM
 *                      and add field DOCDESC.
 * 04/14/2023 22.6.2    Modify Riv_ParseMAdr1() to handle special case of "IMPERIAL HWY"
 * 06/23/2023 22.8.6    Modify Riv_MergeRollCsv() to remove single quote from OFF_ZONE and OFF_ZONE_X1
 * 09/14/2023 23.2.0    Modify Riv_MergeSAdr() to add unknown StrSfx to StrName.  For invalid city name, 
 *                      use known city based on zip code or mail city if same StrNum & StrName.
 * 09/26/2023 23.2.2    Fix DueDate2 in Riv_Load_TaxCurrent()
 * 10/10/2023 23.3.1    Update Riv_ConvStdCharCsv() since county added SOLAR flag.
 * 11/03/2023 23.4.1    Modify Riv_ExtrVal() to use cDelim instead of hardcode '|'
 * 01/03/2024 23.5.1    Modify Riv_CreateValueRec() to set ExCode="HO" if [RIV_ER_HOEAMOUNT] > 0.
 * 02/02/2024 23.6.0    Modify Riv_MergeMAdr() & Riv_ParseMAdr1() to fix mail unit issue.
 * 03/06/2024 23.6.3    Modify Riv_ExtrSale() to use cDelim instead of hardcode ',' as delimiter.
 * 03/15/2024 23.7.2    Fix bug in Riv_MergeSAdr() that put StrSfx in front of StrName in S_ADDR_D
 * 04/23/2024 23.8.1    Change sort order of output file from DESC to ASC on YrBlt in Riv_ConvStdCharCsv().
 * 05/01/2024 23.8.2    Count BPP records dropped.
 * 05/22/2024 23.8.5    Add Riv_CreateBppRec().  Modify Riv_Load_RollCsv() to output PP records.
 *                      Modify loadRiv() to call PQ_UpdateBpp() if there is new BPP records found in roll update.
 * 05/23/2024 23.8.6    Modify Riv_MergeSAdr() to fix known suffix error.
 *                      Modify Riv_Load_RollCsv() to handle special case on condo unit where APN may include '-'.
 * 07/12/2024 24.0.2    Modify Riv_MergeSitus() to handle misspelling of suffix.  Modify Riv_MergeLien2() to add ExeType.
 * 07/15/2024 24.0.4    Modify Riv_Load_LDR2() & Riv_ExtrLienCsv() to fix problem that skips BPP records.
 *
 ******************************************************************************/

#include "stdafx.h"
#include "CountyInfo.h"
#include "Prodlib.h"
#include "R01.h"
#include "RecDef.h"
#include "Tables.h"
#include "CharRec.h"
#include "Logs.h"
#include "LoadDef.h"
#include "Utils.h"
#include "XlatTbls.h"
#include "doGrgr.h"
#include "doSort.h"
#include "doOwner.h"
#include "formatApn.h"
#include "SaleRec.h"
#include "LoadOne.h"
#include "UseCode.h"
#include "LOExtrn.h"
#include "hlado.h"
#include "Tax.h"
#include "SqlExt.h"
#include "PQ.h"
#include "MergeRiv.h"

static FILE    *fdPaid, *fdChar, *fdCChr;
static long    lCharSkip, lSaleSkip, lRollSkip;
static long    lCharMatch, lSaleMatch, lMatchCChr, lRollMatch;
static int     iBpp;

/******************************** Riv_MergeOwner *****************************
 *
 * This version parses owner from RIVKEYIN
 * Keep owners seperate even if they have the same last name
 *
 * Return 0 if successful, >0 if error
 *
 *****************************************************************************/

void Riv_MergeOwner(char *pOutbuf, char *pRollRec)
{
   int   iTmp;
   char  acTmp[128];

   RIV_ROLL *pRec = (RIV_ROLL *)pRollRec;

   // Clear old names, vesting, and careof
   removeNames(pOutbuf, true);

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "009706487", 9) || !memcmp(pOutbuf, "009717140", 9) )
   //   iTmp = 0;
#endif

   if (pRec->Own1_Last[0] > ' ')
   {
      memcpy(acTmp, pRec->Own1_Last, RSIZ_OWNER);
      iTmp = blankRem(acTmp, RSIZ_OWNER);
      memcpy(pOutbuf+OFF_NAME1, acTmp, iTmp);

      // Update vesting
      if (pRec->Own1_Flag == 'U')
         updateVesting(myCounty.acCntyCode, acTmp, pOutbuf+OFF_VEST, pOutbuf+OFF_ETAL_FLG);
   }

   if (pRec->Own2_Last[0] > ' ')
   {
      memcpy(acTmp, pRec->Own2_Last, RSIZ_OWNER);
      iTmp = blankRem(acTmp, RSIZ_OWNER);
      memcpy(pOutbuf+OFF_NAME2, acTmp, iTmp);

      // Update vesting
      if (pRec->Own2_Flag == 'U' && *(pOutbuf+OFF_VEST) == ' ')
         updateVesting(myCounty.acCntyCode, acTmp, pOutbuf+OFF_VEST, pOutbuf+OFF_ETAL_FLG);
   }

   // Check to see if we can combine names
   if (pRec->Own1_Flag == 'F' && pRec->Own2_Flag == 'F' &&
       !memcmp(pRec->Own1_Last, pRec->Own2_Last, RSIZ_OWN1_LAST))
   {
      iTmp = sprintf(acTmp, "%.*s & %.*s %.*s",
         RSIZ_OWN1_FIRST+RSIZ_OWN1_MIDDLE, pRec->Own1_First,
         RSIZ_OWN2_FIRST+RSIZ_OWN2_MIDDLE, pRec->Own2_First,
         RSIZ_OWN1_LAST, pRec->Own1_Last);
      iTmp = blankRem(acTmp, iTmp);
      vmemcpy(pOutbuf+OFF_NAME_SWAP, acTmp, SIZ_NAME_SWAP, iTmp);
   } else
   {
      // Swapped name
      if (pRec->Own1_Flag == 'F')
         sprintf(acTmp, "%.24s %.19s", pRec->Own1_First, pRec->Own1_Last);
      else
         memcpy(acTmp, pRec->Own1_Last, RSIZ_OWNER);
      iTmp = blankRem(acTmp, RSIZ_OWNER);
      memcpy(pOutbuf+OFF_NAME_SWAP, acTmp, iTmp);
   }

   if (pRec->Own3_Last[0] > ' ')
      *(pOutbuf+OFF_ETAL_FLG) = 'Y';
}

/******************************** Riv_MergeOwner *****************************
 *
 * This version parses owners from AssessmentRoll.csv
 *
 * Return 0 if successful, >0 if error
 *
 *****************************************************************************/

void Riv_MergeOwner(char *pOutbuf)
{
   int   iTmp;
   char  *pName1, *pName2;
   OWNER myOwner;

   // Clear old names, vesting, and DBA
   removeNames(pOutbuf, false, true);

   if (*apTokens[R_DOINGBUSINESSAS] > ' ')
   {
      iTmp = cleanOwner(apTokens[R_DOINGBUSINESSAS]);
      if (iTmp > 0)
         blankRem(apTokens[R_DOINGBUSINESSAS]);
      vmemcpy(pOutbuf+OFF_DBA, apTokens[R_DOINGBUSINESSAS], SIZ_DBA);
   }


#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "009619813", 9) )
   //   iTmp = 0;
#endif
   cleanOwner(apTokens[R_LEGALPARTY1]);
   cleanOwner(apTokens[R_LEGALPARTY2]);

   if (*apTokens[R_LEGALPARTY1] > ' ')
   {
      pName1 = apTokens[R_LEGALPARTY1];
      pName2 = apTokens[R_LEGALPARTY2];
   } else
   {
      pName1 = apTokens[R_LEGALPARTY2];
      pName2 = apTokens[R_LEGALPARTY1];
   }

   if (*pName1 > ' ')
   {
      vmemcpy(pOutbuf+OFF_NAME1, pName1, SIZ_NAME1);

      // Update vesting
      iTmp = updateVesting(myCounty.acCntyCode, pName1, pOutbuf+OFF_VEST, pOutbuf+OFF_ETAL_FLG);
      if (!iTmp)
      {
         iTmp = splitOwner(pName1, &myOwner, 3);
         vmemcpy(pOutbuf+OFF_NAME_SWAP, myOwner.acSwapName, SIZ_NAME_SWAP);
      } else
         vmemcpy(pOutbuf+OFF_NAME_SWAP, pName1, SIZ_NAME_SWAP);

   }

   if (*pName2 > ' ')
   {
      vmemcpy(pOutbuf+OFF_NAME2, pName2, SIZ_NAME2);

      // Update vesting
      if (*(pOutbuf+OFF_VEST) == ' ')
         updateVesting(myCounty.acCntyCode, pName2, pOutbuf+OFF_VEST, pOutbuf+OFF_ETAL_FLG);
   }

   // Check to see if we can combine names
   //pTmp = strchr(pName1, ' ');
   //if (!memcmp(pName1, pName2, pTmp-pName1))
   //{
   //   iTmp = sprintf(acTmp, "%.*s & %.*s %.*s",
   //      RSIZ_OWN1_FIRST+RSIZ_OWN1_MIDDLE, pRec->Own1_First,
   //      RSIZ_OWN2_FIRST+RSIZ_OWN2_MIDDLE, pRec->Own2_First,
   //      RSIZ_OWN1_LAST, pRec->Own1_Last);
   //   iTmp = blankRem(acTmp, iTmp);
   //   vmemcpy(pOutbuf+OFF_NAME_SWAP, acTmp, SIZ_NAME_SWAP, iTmp);
   //} else
   //{
   //   // Swapped name
   //   if (pRec->Own1_Flag == 'F')
   //      sprintf(acTmp, "%.24s %.19s", pRec->Own1_First, pRec->Own1_Last);
   //   else
   //      memcpy(acTmp, pRec->Own1_Last, RSIZ_OWNER);
   //   iTmp = blankRem(acTmp, RSIZ_OWNER);
   //   memcpy(pOutbuf+OFF_NAME_SWAP, acTmp, iTmp);
   //}

   if (*apTokens[R_LEGALPARTY3] > ' ')
      *(pOutbuf+OFF_ETAL_FLG) = 'Y';
}

void Riv_MergeLOwner(char *pOutbuf, RIV_LIEN52 *pNames)
{
   int   iNameSeq, iTmp;
   char  acTmp[128];

   iNameSeq = atoin(pNames->Sequence, SIZ_LIEN_SEQUENCE);
   if (iNameSeq == 1)
   {
      // 19 LAST, 12 FIRST, 12 MIDL
      // Name1
      memcpy(acTmp, pNames->Owner_Name, SIZ_LIEN_OWNER_NAME);
      iTmp = blankRem(acTmp, SIZ_LIEN_OWNER_NAME);
      memcpy(pOutbuf+OFF_NAME1, acTmp, iTmp);

      // Update vesting
      iTmp = updateVesting(myCounty.acCntyCode, acTmp, pOutbuf+OFF_VEST, pOutbuf+OFF_ETAL_FLG);

      // Swapped name
      if (!iTmp)
      {
         sprintf(acTmp, "%.24s %.19s", &pNames->Owner_Name[19], pNames->Owner_Name);
         iTmp = blankRem(acTmp, SIZ_LIEN_OWNER_NAME);
      }
      vmemcpy(pOutbuf+OFF_NAME_SWAP, acTmp, SIZ_NAME_SWAP);
   } else if (iNameSeq == 2)
   {
      // Name2
      memcpy(acTmp, pNames->Owner_Name, SIZ_LIEN_OWNER_NAME);
      iTmp = blankRem(acTmp, SIZ_LIEN_OWNER_NAME);
      memcpy(pOutbuf+OFF_NAME2, acTmp, iTmp);
#ifdef _DEBUG
      // Test only
      //if (*(pOutbuf+OFF_NAME1) == ' ')
      //   LogMsg0("Name2 is populated but Name1 is still blank *****");
#endif
   }
}

void Riv_MergeLOwner1(char *pOutbuf, char *pNames, int iNameSeq)
{
   int   iTmp;
   OWNER myOwner;

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "009000009", 9) )
   //   iTmp = 0;
#endif

   iTmp = cleanOwner(pNames);
   if (iNameSeq == 1)
   {
      // 19 LAST, 12 FIRST, 12 MIDL
      // Name1
      vmemcpy(pOutbuf+OFF_NAME1, pNames, SIZ_NAME1);

      // Update vesting
      iTmp = updateVesting(myCounty.acCntyCode, pNames, pOutbuf+OFF_VEST, pOutbuf+OFF_ETAL_FLG);

      // Swapped name - if no vesting found
      if (!iTmp)
      {
         iTmp = splitOwner(pNames, &myOwner, 3);
         vmemcpy(pOutbuf+OFF_NAME_SWAP, myOwner.acSwapName, SIZ_NAME_SWAP);
      } else
         vmemcpy(pOutbuf+OFF_NAME_SWAP, pNames, SIZ_NAME_SWAP);
   } else if (iNameSeq == 2)
   {
      // Name2
      iTmp = blankRem(pNames);
      vmemcpy(pOutbuf+OFF_NAME2, pNames, SIZ_NAME2, iTmp);
   }
}

/********************************* Riv_MergeSAdr *****************************
 *
 * Return 0 if successful, >0 if error
 *
 *****************************************************************************/

void Riv_MergeSAdr(char *pOutbuf, char *pRollRec)
{
   RIV_LIEN20 *pRec;
   char     acTmp[256], acAddr1[64], acAddr2[64], acCode[64];
   int      iTmp, iStrNo, iSfxCode;

   ADR_REC  sSitusAdr;

   pRec = (RIV_LIEN20 *)pRollRec;

   // Situs
   acAddr1[0] = 0;
   acAddr2[0] = 0;
   memset((void *)&sSitusAdr, 0, sizeof(ADR_REC));

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "008524989", 9) )
   //   lTmp = 0;
#endif

   // If no street name, don't bother check anything else
   if (pRec->SStr_Name[0] > ' ')
   {
      iStrNo = atoin(pRec->SStr_No, SIZ_LIEN_SSTR_NO);
      if (iStrNo > 0)
      {
         sprintf(acAddr1, "%d       ", iStrNo);
         memcpy(pOutbuf+OFF_S_STRNUM, acAddr1, SIZ_S_STRNUM);
         memcpy(pOutbuf+OFF_S_HSENO, acAddr1, SIZ_S_STRNUM);
      }

      if (pRec->SStr_Dir[0] > ' ')
         *(pOutbuf+OFF_S_DIR) = pRec->SStr_Dir[0];

      // Copy street name
      memcpy(pOutbuf+OFF_S_STREET, pRec->SStr_Name, SIZ_LIEN_SSTR_NAME);

      if (pRec->SStr_Suffix[0] > ' ')
      {
         memcpy(acTmp, pRec->SStr_Suffix, SIZ_LIEN_SSTR_SUFFIX);
         acTmp[SIZ_LIEN_SSTR_SUFFIX] = 0;
         myTrim(acTmp);
         iSfxCode = GetSfxCode(acTmp);
         if (iSfxCode)
         {
            iTmp = sprintf(acCode, "%d", iSfxCode);
            memcpy(pOutbuf+OFF_S_SUFF, acCode, iTmp);
         }
      }

      if (pRec->Unit[0] > ' ')
      {
         *(pOutbuf+OFF_S_UNITNO) = '#';
         memcpy(pOutbuf+OFF_S_UNITNO+1, pRec->Unit, SIZ_LIEN_UNIT);
         iTmp = sprintf(acAddr1, "%d %c %.18s %.3s #%.4s", iStrNo, pRec->SStr_Dir[0], pRec->SStr_Name, pRec->SStr_Suffix, pRec->Unit);
      } else
         iTmp = sprintf(acAddr1, "%d %c %.18s %.3s", iStrNo, pRec->SStr_Dir[0], pRec->SStr_Name, pRec->SStr_Suffix);

      replUChar((unsigned char *)acAddr1, 0x1C, ' ', iTmp);
      blankRem(acAddr1);
      memcpy(pOutbuf+OFF_S_ADDR_D, acAddr1, strlen(acAddr1));
   }

   // City-state
   if (pRec->SCity[0] > ' ')
   {
      memcpy(acTmp, pRec->SCity, SIZ_LIEN_SCITY);
      myTrim(acTmp, SIZ_LIEN_SCITY);
      City2Code(acTmp, acCode, pOutbuf);
      memcpy(pOutbuf+OFF_S_CITY, acCode, SIZ_S_CITY);
      memcpy(pOutbuf+OFF_S_ZIP, pRec->SZip_Code, SIZ_S_ZIP);

      memcpy(pOutbuf+OFF_S_ST, "CA", 2);
      strcat(acTmp, " CA");
      memcpy(pOutbuf+OFF_S_CTY_ST_D, acTmp, strlen(acTmp));
   }

   // If no mail addr, copy situs over
   if (*(pOutbuf+OFF_M_STREET) == ' ')
   {
      memcpy(pOutbuf+OFF_M_STRNUM, pOutbuf+OFF_S_STRNUM, SIZ_M_STRNUM);
      *(pOutbuf+OFF_M_DIR) = *(pOutbuf+OFF_S_DIR);
      memcpy(pOutbuf+OFF_M_STREET, pOutbuf+OFF_S_STREET, SIZ_M_STREET);
      if (pRec->SStr_Suffix[0] > ' ')
         memcpy(pOutbuf+OFF_M_SUFF, pRec->SStr_Suffix, SIZ_LIEN_SSTR_SUFFIX);

      if (pRec->Unit[0] > ' ')
         memcpy(pOutbuf+OFF_M_UNITNO, pRec->Unit, SIZ_LIEN_UNIT);

      memcpy(pOutbuf+OFF_M_ADDR_D, pOutbuf+OFF_S_ADDR_D, SIZ_M_ADDR_D);
      memcpy(pOutbuf+OFF_M_ST, "CA", 2);

      if (pRec->SCity[0] > ' ')
      {
         memcpy(pOutbuf+OFF_M_CITY, pRec->SCity, SIZ_LIEN_SCITY);
         memcpy(pOutbuf+OFF_M_ZIP, pRec->SZip_Code, SIZ_M_ZIP);
         memcpy(pOutbuf+OFF_M_CTY_ST_D, pOutbuf+OFF_S_CTY_ST_D, SIZ_M_CTY_ST_D);
      }
   }
}

/******************************** Riv_UpdateSAdr *****************************
 *
 * Return 0 if successful, >0 if error
 *
 *****************************************************************************/

//void Riv_UpdateSAdr(char *pOutbuf, char *pRollRec)
//{
//   RIV_ROLL *pRec;
//   char     acTmp[256], acAddr1[64], acCode[64], sCity[64], *pTmp;
//   int      iTmp, iStrNo, iSfxCode, iZip;
//
//   pRec = (RIV_ROLL *)pRollRec;
//
//   removeSitus(pOutbuf);
//
//#ifdef _DEBUG
//   //if (!memcmp(pOutbuf, "129020040", 9))
//   //   iTmp = 0;
//#endif
//
//   // If no street name, don't bother check anything else
//   if (pRec->SStr_Name[0] > ' ')
//   {
//      iStrNo = atoin(pRec->SStr_No, RSIZ_SSTR_NUMBER);
//      if (iStrNo > 0)
//      {
//         iTmp = sprintf(acAddr1, "%d ", iStrNo);
//         memcpy(pOutbuf+OFF_S_STRNUM, acAddr1, iTmp);
//         memcpy(pOutbuf+OFF_S_HSENO, acAddr1, iTmp);
//      }
//
//      if (pRec->SStr_Dir[0] > ' ')
//         *(pOutbuf+OFF_S_DIR) = pRec->SStr_Dir[0];
//
//      // Copy street name
//      memcpy(acTmp, pRec->SStr_Name, RSIZ_SSTR_NAME);
//      myTrim(acTmp, RSIZ_SSTR_NAME);
//      if ((pTmp = strrchr(acTmp, ' ')) && !strcmp(pTmp, " ST"))
//      {
//         *pTmp = 0;
//         memcpy(pRec->SStr_Suffix, "ST", 2);
//         vmemcpy(pOutbuf+OFF_S_STREET, acTmp, RSIZ_SSTR_NAME);
//      } else
//         memcpy(pOutbuf+OFF_S_STREET, pRec->SStr_Name, RSIZ_SSTR_NAME);
//
//      if (pRec->SStr_Suffix[0] > ' ')
//      {
//         memcpy(acTmp, pRec->SStr_Suffix, RSIZ_SSTR_SUFFIX);
//         acTmp[RSIZ_SSTR_SUFFIX] = 0;
//         myTrim(acTmp);
//         iSfxCode = GetSfxCode(acTmp);
//         if (iSfxCode)
//         {
//            iTmp = sprintf(acCode, "%d", iSfxCode);
//            memcpy(pOutbuf+OFF_S_SUFF, acCode, iTmp);
//         }
//      }
//
//      if (pRec->SUnit[0] > ' ')
//      {
//         *(pOutbuf+OFF_S_UNITNO) = '#';
//         memcpy(pOutbuf+OFF_S_UNITNO+1, pRec->SUnit, RSIZ_SUNIT);
//         sprintf(acAddr1, "%d %c %.18s %.3s #%.4s", iStrNo, pRec->SStr_Dir[0],
//            pRec->SStr_Name, pRec->SStr_Suffix, pRec->SUnit);
//      } else
//         sprintf(acAddr1, "%d %c %.18s %.3s", iStrNo, pRec->SStr_Dir[0],
//            pRec->SStr_Name, pRec->SStr_Suffix);
//
//      blankRem(acAddr1);
//      memcpy(pOutbuf+OFF_S_ADDR_D, acAddr1, strlen(acAddr1));
//   }
//
//   if (pRec->SZip[0] > '0')
//   {
//      memcpy(pOutbuf+OFF_S_ZIP, pRec->SZip, SIZ_S_ZIP);
//      iZip = atoin(pRec->SZip, SIZ_S_ZIP);
//   } else if (!memcmp(pOutbuf, "12902", 5))
//   {
//      memcpy(pOutbuf+OFF_S_ZIP, "92860", SIZ_S_ZIP);
//      iZip = 92860;
//   } else
//      iZip = 0;
//
//   if (iZip > 90000)
//   {
//      sprintf(acCode, "%d", iZip);
//      iTmp = locateCity(acCode, sCity, "RIV");
//      if (iTmp == 1)
//      {
//         if (!memcmp(sCity, "MARCH AIR RESERVE BASE", 10))
//            strcpy(sCity, "RIVERSIDE");
//         iTmp = City2Code(sCity, acCode, pOutbuf);
//         if (iTmp > 0)
//         {
//            memcpy(pOutbuf+OFF_S_CITY, acCode, 3);
//            iTmp = sprintf(acTmp, "%s CA", sCity);
//            memcpy(pOutbuf+OFF_S_CTY_ST_D, acTmp, iTmp);
//         }
//      }
//   }
//
//   // state
//   if (*(pOutbuf+OFF_S_ZIP) == '9')
//      memcpy(pOutbuf+OFF_S_ST, "CA", 2);
//
//   // Test mail addr
//   if (*(pOutbuf+OFF_M_CITY) > ' ' && (pRec->SZip[0]=='9' && !memcmp(pRec->MZip, pRec->SZip, SIZ_M_ZIP) ))
//   {
//      memcpy(acTmp, pOutbuf+OFF_M_CITY, SIZ_M_CITY);
//      myTrim(acTmp, SIZ_M_CITY);
//      if (iZip > 90000 && strcmp(sCity, acTmp))
//      {
//         if (memcmp(sCity, "CATHEDRAL C", 11))
//            LogMsg0("*** City name mismatched m=%s to s=%s Zipcode=%.5s for %.9s (may be wrong mail zip)", acTmp, sCity, pRec->MZip, pOutbuf);
//         iBadCity++;
//      } 
//   } 
//}

/********************************* parseMAdr1() *****************************
 *
 * Copy from R01.CPP to customize for RIV
 *
 ****************************************************************************/

void Riv_ParseMAdr1(ADR_REC *pAdrRec, char *pAddr)
{
   char  acAdr[256], acStrName[256], acSfx[64], acTmp[256], *apItems[16];
   char  *pAdr, *pTmp;
   int   iCnt, iTmp, iIdx, iDir;
   bool  bRet, bDir;

   // Check for PO Box
   if (!memcmp(pAddr, "PO BOX", 6)   ||
       !memcmp(pAddr, "P.O. BOX", 8) ||
       !memcmp(pAddr, "P O ", 4)     ||
       !memcmp(pAddr, "PO BX", 5)    ||
       !memcmp(pAddr, "BOX ", 4)     ||
       !memcmp(pAddr, "POB ", 4) )
   {
      strcpy(pAdrRec->strName, pAddr);
      return;
   }

   acStrName[0] = 0;
   pAdr = pAddr;
   if (*pAdr == '%')
   {
      pTmp = strchr(pAdr+3, ' ');
      if (pTmp && isdigit(*++pTmp))
         pAdr = pTmp;
      else
         return;        // bad address
   }

   // Remove leading space
   while (*pAdr && *pAdr == ' ') pAdr++;

   // Remove '.' and extra space
   iTmp = 0;
   while (*pAdr)
   {
      // Remove dot, comma, and single quote
      if (*pAdr != '.' && *pAdr != ',' && *pAdr != 39)
         acAdr[iTmp++] = *pAdr;

      // Remove multi-space
      if (*pAdr == ' ')
         while (*pAdr == ' ') pAdr++;
      else
         pAdr++;
   }
   acAdr[iTmp] = 0;

   //replStr(acAdr, " CO RD", " COUNTY ROAD");

   // Tokenize address
   iCnt = ParseString(acAdr, 32, 16, apItems);

   // If strNum is 0, do not parse address
   iTmp = atoi(apItems[0]);
   if (iTmp == 0 || iCnt == 1)
   {
      strcpy(acStrName, pAddr);
      pAdrRec->lStrNum = 0;
   } else
   {
      if (apItems[0][strlen(apItems[0])-1] > '9')
      {
         // StrSub ?
         pTmp = apItems[0];
         while (*pTmp && isdigit(*pTmp) )
            pTmp++;
         if (*pTmp && (*pTmp == '-' || *pTmp == '/'))
            pTmp++;
         if (*pTmp && strlen(pTmp) <= SIZ_M_STR_SUB)
            strcpy(pAdrRec->strSub, pTmp);
      }

      strcpy(pAdrRec->HseNo, apItems[0]);
      pAdrRec->lStrNum = iTmp;
      sprintf(acTmp, "%d", iTmp);
      strcpy(pAdrRec->strNum, acTmp);
      iIdx = 1;

      // Fraction - Make sure this is not the last token
      if (strchr(apItems[iIdx], '/') && (iCnt > (iIdx+1)) && (strlen(apItems[iIdx]) <= SIZ_S_STR_SUB))
      {
         strcpy(pAdrRec->strSub, apItems[iIdx++]);
         if (!strcmp(apItems[iIdx], "RD") || !strcmp(apItems[iIdx], "ROAD") || !strcmp(apItems[iIdx], "AVE"))
         {
            if (iCnt > iIdx+1)
            {
               sprintf(acStrName, "%s %s", apItems[iIdx], apItems[iIdx+1]);
               iIdx += 2;
               if (iIdx >= iCnt)
               {
                  strcpy(pAdrRec->strName, acStrName);
                  return;
               }
            }
         }
      }

      // Unit is sometimes placed after strnum as in: 9416 #A CARLTON OAKS
      if (apItems[iIdx][0] == '#')
      {
         strncpy(pAdrRec->Unit, (char *)&apItems[iIdx][0], SIZ_M_UNITNO);
         iIdx++;
      }

      if (iIdx >= iCnt)
      {
         // Bad address
         return;
      }

      // Dir
      iDir = 0;
      bDir = false;
      strcpy(acSfx, apItems[iIdx]);
      if (strlen(apItems[iIdx]) < 3)
      {
         if (acSfx[1] == '.')
            acSfx[1] = 0;

         while (iDir <= 7)
         {
            if (!strcmp(acSfx, asDir[iDir]))
            {
               bDir = true;
               break;
            }
            iDir++;
         }
         if (!bDir)
         {
            iDir = 0;
            while (iDir <= 1)
            {
               if (!strcmp(acSfx, asDir2[iDir]))
               {
                  bDir = true;
                  break;
               }
               iDir++;
            }
         }
      } else
      {
         // Check for long direction
         while (iDir < 4)
         {
            if (!strcmp(apItems[iIdx], asDir1[iDir]))
            {
               bDir = true;
               break;
            }
            iDir++;
         }
      }

      // Possible cases E ST or W RD
      // Check for suffix
      bRet = false;
      //if (iTmp <= 7 && iCnt > 2)
      if (bDir && iCnt > 2)
      {
         // If next item is suffix, then this one is not direction
         // if (iTmp=GetSfxCode(apItems[iIdx+1]) && iCnt==iIdx+2)
         // The above statement won't work if Unit# is included.
         if ((iTmp=GetSfxCodeX(apItems[iIdx+1], acTmp)) &&
             memcmp(apItems[iIdx+1], "HWY", 3) && 
             memcmp(apItems[iIdx+1], "HIGHWAY", 7)) 
         {
            strncpy(acStrName, apItems[iIdx], SIZ_M_STREET);
            strcpy(pAdrRec->strSfx, acTmp);
            sprintf(acTmp, "%d", iTmp);
            strcpy(pAdrRec->SfxCode, acTmp);
            bRet = true;         // Done
         } else
         {
            // This is direction
            strcpy(pAdrRec->strDir, asDir[iDir]);
            iIdx++;
         }
      }

      // Check for unit #
      if (apItems[iCnt-1][0] == '#')
      {
         strncpy(pAdrRec->Unit, (char *)&apItems[iCnt-1][0], SIZ_M_UNITNO);
         iCnt--;

         // If last token is STE or SUITE, remove it.
         if (!strcmp(apItems[iCnt-1], "STE") || !strcmp(apItems[iCnt-1], "SUITE")
            || !strcmp(apItems[iCnt-1], "UNIT") || !strcmp(apItems[iCnt-1], "APT")
            || !strcmp(apItems[iCnt-1], "SP") || !strcmp(apItems[iCnt-1], "RM") )
            iCnt--;
      } else if (!strcmp(apItems[iCnt-2], "STE") || !strcmp(apItems[iCnt-2], "SUITE")
         || !strcmp(apItems[iCnt-2], "UNIT") || !strcmp(apItems[iCnt-2], "UNIT#") || !strcmp(apItems[iCnt-2], "NO")
         || !strcmp(apItems[iCnt-2], "APT")  || !strcmp(apItems[iCnt-2], "APTS") || !strcmp(apItems[iCnt-2], "APT#")
         || !strcmp(apItems[iCnt-2], "RM")   || !strcmp(apItems[iCnt-2], "SP")   || !strcmp(apItems[iCnt-2], "LOT") )
      {
         sprintf(acTmp, "#%s", apItems[iCnt-1]);
         strncpy(pAdrRec->Unit, acTmp, SIZ_M_UNITNO);
         strncpy(pAdrRec->UnitNox, acTmp, SIZ_M_UNITNOX);
         iCnt -= 2;
      } else if ((pTmp=strchr(apItems[iCnt-1], '#')))
      {  // 15025 NE SACRAMENTO ST#54, 838 S GRAND AVE APT#611
         if (!memcmp(apItems[iCnt-1], "SP", 2))
         {
            *pTmp = ' ';
         } else
         {
            strncpy(pAdrRec->UnitNox, pTmp, SIZ_M_UNITNOX);
            strncpy(pAdrRec->Unit, pTmp, SIZ_M_UNITNO);
            *pTmp = 0;
            if (!strcmp(apItems[iCnt-1], "APT"))
               iCnt--;
         }
      } else if (!memcmp(apItems[iCnt-1], "FL-", 3) || !memcmp(apItems[iCnt-1], "FLR-", 4))
      {
         strncpy(pAdrRec->Unit, apItems[iCnt-1], SIZ_M_UNITNO);
         strncpy(pAdrRec->UnitNox, apItems[iCnt-1], SIZ_M_UNITNOX);
         iCnt--;
      } else if (!strcmp(apItems[iCnt-2], "LOT") && atoi(apItems[iCnt-1]) > 0)
      {  // 245 WILDWOOD DR LOT 183, 45055 E FLORIDA AVE SP 36
         sprintf(acTmp, "#%s     ", apItems[iCnt-1]);
         strncpy(pAdrRec->Unit, acTmp, SIZ_M_UNITNO);
         strncpy(pAdrRec->UnitNox, acTmp, SIZ_M_UNITNOX);
         iCnt -= 2;
      } else if (pTmp = strstr(pAddr, " CO RD"))
      {
         sprintf(acStrName, "COUNTY ROAD%s", pTmp+6);
         bRet = true;
      } else if (iCnt > 3 && atoi(apItems[iCnt-1]) > 0 && acStrName[0] <= ' ')
      {  // 555 N DANEBO 33
         // Not 21051 STATE ROUTE 772, 5323 HERMITAGE AVE# 1, 3100-R WEST HWY 86
         if (strcmp(apItems[iCnt-2], "ROUTE") && strcmp(apItems[iCnt-2], "HWY")
            && strcmp(apItems[iCnt-2], "BOX") && strcmp(apItems[iCnt-2], "ROAD")
            && strcmp(apItems[iCnt-2], "SP")  && strcmp(apItems[iCnt-2], "HIGHWAY"))   // MER
         {
            // Check for 123 COUNTY RD 105 on SIS->001110050000
            if (!strstr(pAddr, "COUNTY RD"))
            {
               sprintf(acTmp, "#%s     ", apItems[iCnt-1]);
               strncpy(pAdrRec->Unit, acTmp, SIZ_M_UNITNO);
               strncpy(pAdrRec->UnitNox, acTmp, SIZ_M_UNITNOX);
               iCnt--;
            } else
            {
               sprintf(acStrName, "COUNTY ROAD %s", apItems[iCnt-1]);
               bRet = true;
            }
         } else if (!strcmp(apItems[iCnt-2], "HWY"))
         {
            sprintf(acTmp, "#%s", apItems[iCnt-1]);
            strncpy(pAdrRec->Unit, acTmp, SIZ_M_UNITNO);
            strncpy(pAdrRec->UnitNox, acTmp, SIZ_M_UNITNOX);
            iCnt--;
         }

         if (pTmp=strchr(apItems[iCnt-1], '#'))
            *pTmp = 0;
      }

      // Not direction, token is str name
      if (!bRet && iIdx < iCnt)
      {
         // Check suffix
         if (iTmp=GetSfxCodeX(apItems[iCnt-1], acTmp))
         {
            strcpy(pAdrRec->strSfx, acTmp);
            sprintf(acTmp, "%d", iTmp);
            strcpy(pAdrRec->SfxCode, acTmp);

            while (iIdx < iCnt-1)
            {
               // If multiple address, take the first one.
               // This loop break after first suffix found, ignore the rest.
               // We also must avoid strname like 123 ST PAUL BLVD.  StrSfx must
               // be indexed > 1
               if ((iIdx > 1) &&
                   (iIdx < iCnt-2) &&
                   (iTmp=GetSfxCodeX(apItems[iIdx], acTmp)) )
               {
                  strcpy(pAdrRec->strSfx, acTmp);
                  sprintf(acTmp, "%d", iTmp);
                  strcpy(pAdrRec->SfxCode, acTmp);
                  break;
               }
               strcat(acStrName, apItems[iIdx++]);
               if (iIdx < iCnt)
                  strcat(acStrName, " ");
            }
         } else if (!strcmp(apItems[iCnt-2], "HWY"))
         {  // 123 HWY 70
            sprintf(acStrName, "%s %s", apItems[iCnt-2], apItems[iCnt-1]);
         } else
         {
            // Scan for suffix in multi address cases.  Keep the first one
            // 123 MAIN ST 540 N SANTA ANA BLVD
            while (iIdx < iCnt)
            {
               if (iIdx > 1)
               {
                  if (iTmp = GetSfxCodeX(apItems[iIdx], acTmp))
                  {
                     strcpy(pAdrRec->strSfx, acTmp);
                     sprintf(acTmp, "%d", iTmp);
                     strcpy(pAdrRec->SfxCode, acTmp);
                     break;
                  }
               }

               strcat(acStrName, apItems[iIdx]);
               iIdx++;
               if (iIdx < iCnt)
                  strcat(acStrName, " ");
            }
         }
      }
   }
   strncpy(pAdrRec->strName, acStrName, SIZ_M_STREET);
}

/******************************** Riv_MergeMAdr ******************************
 *
 * This version parse mail address from "AssessmentRoll.csv"
 * Return 0 if successful, >0 if error
 *
 *****************************************************************************/

void Riv_MergeMAdr(char *pOutbuf)
{
   char        acTmp[256], acAddr1[64], *pAddr1;
   int         iTmp;
   bool        bUnitAvail = false;

   ADR_REC     sMailAdr;

   // Clear old Mailing
   removeMailing(pOutbuf);

   if (*apTokens[R_MAILNAME] > ' ')
   {
      iTmp = replUnPrtChar(apTokens[R_MAILNAME]);
      updateCareOf(pOutbuf, apTokens[R_MAILNAME], 0);
   }

   // Check for blank address
   replUnPrtChar(apTokens[R_MAILADDRESS]);
   pAddr1 = apTokens[R_MAILADDRESS];
   if (memcmp(pAddr1, "     ", 5) && memcmp(pAddr1, "UNKNOWN", 7))
   {
      strcpy(acAddr1, pAddr1);
      memset((void *)&sMailAdr, 0, sizeof(ADR_REC));

#ifdef _DEBUG
      //if (!memcmp(pOutbuf, "692460076", 9) )
      //   iTmp = 0;
#endif

      // Parsing mail address
      Riv_ParseMAdr1(&sMailAdr, acAddr1);

      if (sMailAdr.lStrNum > 0)
      {
         iTmp = sprintf(acTmp, "%d ", sMailAdr.lStrNum);
         memcpy(pOutbuf+OFF_M_STRNUM, acTmp, iTmp);
         if (sMailAdr.strSub[0] > '0')
         {
            sprintf(acTmp, "%s  ", sMailAdr.strSub);
            memcpy(pOutbuf+OFF_M_STR_SUB, acTmp, SIZ_M_STR_SUB);
         }
      }

      if (sMailAdr.strDir[0] > ' ')
         memcpy(pOutbuf+OFF_M_DIR, sMailAdr.strDir, strlen(sMailAdr.strDir));

      memcpy(pOutbuf+OFF_M_STREET, sMailAdr.strName, strlen(sMailAdr.strName));

      if (sMailAdr.strSfx[0] > ' ')
         memcpy(pOutbuf+OFF_M_SUFF, sMailAdr.strSfx, strlen(sMailAdr.strSfx));

      if (sMailAdr.Unit[0] > ' ')
      {
         if (sMailAdr.Unit[0] == '#')
         {
            vmemcpy(pOutbuf+OFF_M_UNITNO, sMailAdr.Unit, SIZ_M_UNITNO);
            vmemcpy(pOutbuf+OFF_M_UNITNOX, sMailAdr.UnitNox, SIZ_M_UNITNOX);
         } else
         {
            *(pOutbuf+OFF_M_UNITNO) = '#';
            *(pOutbuf+OFF_M_UNITNOX) = '#';
            vmemcpy(pOutbuf+OFF_M_UNITNO+1, sMailAdr.Unit, SIZ_M_UNITNO);
            vmemcpy(pOutbuf+OFF_M_UNITNOX+1, sMailAdr.UnitNox, SIZ_M_UNITNOX);
         }
      }

      memcpy(pOutbuf+OFF_M_ADDR_D, acAddr1, strlen(acAddr1));

      // City/State
      if (*apTokens[R_MAILCITY] > ' ')
      {
         // Remove quote & comma at the end of city name
         strcpy(acAddr1, apTokens[R_MAILCITY]);
         if ((unsigned char)acAddr1[1] == 0xC2 && (unsigned char)acAddr1[2] == 0x8D && acAddr1[0] == 'P')
            strcpy(acAddr1, "PALM DESERT");

         iTmp = iTrim(acAddr1);
         if (acAddr1[iTmp-1] == ',')
            acAddr1[iTmp-1] = 0;

         if (*apTokens[R_MAILSTATE] > ' ')
         {
            vmemcpy(pOutbuf+OFF_M_CITY, acAddr1, SIZ_M_CITY);
            vmemcpy(pOutbuf+OFF_M_ST, apTokens[R_MAILSTATE], 2);

            iTmp = sprintf(acTmp, "%s, %s %s", acAddr1, apTokens[R_MAILSTATE], apTokens[R_MAILZIPCODE]);
            vmemcpy(pOutbuf+OFF_M_CTY_ST_D, acTmp, SIZ_M_CTY_ST_D, iTmp);
         } else
         {
            vmemcpy(pOutbuf+OFF_M_CITY, acAddr1, SIZ_M_CITY+SIZ_M_ST+SIZ_M_ZIP);
            vmemcpy(pOutbuf+OFF_M_CTY_ST_D, acAddr1, SIZ_M_CTY_ST_D);
         }
      }

      iTmp = atol(apTokens[R_MAILZIPCODE]);
      if (iTmp > 500)
         vmemcpy(pOutbuf+OFF_M_ZIP, apTokens[R_MAILZIPCODE], SIZ_M_ZIP);
   }
}

/******************************** Riv_MergeMAdr ******************************
 *
 * Return 0 if successful, >0 if error
 *
 *****************************************************************************/

void Riv_MergeMAdr(char *pOutbuf, char *pAddr1, char *pAddr2, char *pZip)
{
   char        acTmp[256], acAddr1[64], acAddr2[64], *pTmp;
   int         iTmp;
   bool        bUnitAvail = false;

   ADR_REC     sMailAdr;

   // Clear old Mailing
   removeMailing(pOutbuf, false);

   // Check for blank address
   if (*pAddr1 != '/' &&
       memcmp(pAddr1, "     ", 5) &&
       memcmp(pAddr1, "UNKNOWN", 7))
   {
      memcpy(acAddr1, pAddr1, SIZ_LIEN_MSTREET);
      pAddr1 = myTrim(acAddr1, SIZ_LIEN_MSTREET);
      memset((void *)&sMailAdr, 0, sizeof(ADR_REC));

#ifdef _DEBUG
      //if (!memcmp(pOutbuf, "009601255", 9) )
      //   iTmp = 0;
#endif

      // Fix known problem - 009601255
      if ((pTmp = strstr(acAddr1, " DR NO")) && acAddr1[strlen(acAddr1)-1] == 'O') 
      {
         *(pTmp+3) = 0;
         if (*pAddr2 == ' ')
            pAddr2++;
         if ((pTmp = strchr(pAddr2, ' ')) && isdigit(*(pAddr2+1)) )
         {
            *pTmp = 0;
            strcpy(sMailAdr.Unit, pAddr2);
            bUnitAvail = true;
            *(pAddr2+SIZ_LIEN_MCITY_STATE) = 0;
            pTmp++;
            if (*pTmp == ' ')
               pTmp++;
            strcpy(acAddr2, pTmp);
         } else
            memcpy(acAddr2, pAddr2, SIZ_LIEN_MCITY_STATE);
      } else
         memcpy(acAddr2, pAddr2, SIZ_LIEN_MCITY_STATE);
      blankRem(acAddr2, SIZ_LIEN_MCITY_STATE);

      // Parsing mail address
      Riv_ParseMAdr1(&sMailAdr, acAddr1);

      if (sMailAdr.lStrNum > 0)
      {
         iTmp = sprintf(acTmp, "%d ", sMailAdr.lStrNum);
         memcpy(pOutbuf+OFF_M_STRNUM, acTmp, iTmp);
         if (sMailAdr.strSub[0] > '0')
         {
            sprintf(acTmp, "%s  ", sMailAdr.strSub);
            memcpy(pOutbuf+OFF_M_STR_SUB, acTmp, SIZ_M_STR_SUB);
         }
      }

      if (sMailAdr.strDir[0] > ' ')
         memcpy(pOutbuf+OFF_M_DIR, sMailAdr.strDir, strlen(sMailAdr.strDir));

      memcpy(pOutbuf+OFF_M_STREET, sMailAdr.strName, strlen(sMailAdr.strName));

      if (sMailAdr.strSfx[0] > ' ')
         memcpy(pOutbuf+OFF_M_SUFF, sMailAdr.strSfx, strlen(sMailAdr.strSfx));

      if (sMailAdr.Unit[0] > ' ')
      {
         if (sMailAdr.Unit[0] == '#')
         {
            vmemcpy(pOutbuf+OFF_M_UNITNO, sMailAdr.Unit, SIZ_M_UNITNO);
            vmemcpy(pOutbuf+OFF_M_UNITNOX, sMailAdr.UnitNox, SIZ_M_UNITNOX);
         } else
         {
            *(pOutbuf+OFF_M_UNITNO) = '#';
            *(pOutbuf+OFF_M_UNITNOX) = '#';
            vmemcpy(pOutbuf+OFF_M_UNITNO+1, sMailAdr.Unit, SIZ_M_UNITNO);
            vmemcpy(pOutbuf+OFF_M_UNITNOX+1, sMailAdr.UnitNox, SIZ_M_UNITNOX);
         }
      }

      if (bUnitAvail)
      {
         iTmp = sprintf(acTmp, "%s #%s", acAddr1, sMailAdr.Unit);
         memcpy(pOutbuf+OFF_M_ADDR_D, acTmp, iTmp);
      } else
         memcpy(pOutbuf+OFF_M_ADDR_D, acAddr1, strlen(acAddr1));

      // City/State
      parseMAdr2(&sMailAdr, acAddr2);
      if (sMailAdr.City[0] > ' ')
      {
         vmemcpy(pOutbuf+OFF_M_CITY, sMailAdr.City, SIZ_M_CITY);
         memcpy(pOutbuf+OFF_M_ST, sMailAdr.State, 2);

         iTmp = sprintf(acTmp, "%s %s", sMailAdr.City, sMailAdr.State);
         vmemcpy(pOutbuf+OFF_M_CTY_ST_D, acTmp, SIZ_M_CTY_ST_D, iTmp);
      }

      iTmp = atoin(pZip, SIZ_LIEN_ZIP_CODE);
      if (iTmp > 500)
         memcpy(pOutbuf+OFF_M_ZIP, pZip, SIZ_M_ZIP);
   }
}

/****************************** Riv_MergeMailing *****************************
 *
 * Merge mail address for LDR 2019
 *
 * Return 0 if successful, >0 if error
 *
 *****************************************************************************/

void Riv_MergeMailing(char *pOutbuf)
{
   char        acTmp[256], acAddr1[64], *pAddr1;
   int         iTmp;
   bool        bUnitAvail = false;

   ADR_REC     sMailAdr;

   if (*apTokens[L_MAILNAME] > ' ')
   {
      iTmp = replUnPrtChar(apTokens[L_MAILNAME]);
      if (iTmp > 0)
         blankRem(apTokens[L_MAILNAME]);
      updateCareOf(pOutbuf, apTokens[L_MAILNAME], 0);
   }

   // Check for blank address
   replUnPrtChar(apTokens[L_M_ADDR1]);
   pAddr1 = apTokens[L_M_ADDR1];
   if (memcmp(pAddr1, "     ", 5) && memcmp(pAddr1, "UNKNOWN", 7))
   {
      strcpy(acAddr1, pAddr1);
      pAddr1 = myTrim(acAddr1);
      memset((void *)&sMailAdr, 0, sizeof(ADR_REC));

      // Parsing mail address
      Riv_ParseMAdr1(&sMailAdr, _strupr(acAddr1));

      if (sMailAdr.lStrNum > 0)
      {
         iTmp = sprintf(acTmp, "%d ", sMailAdr.lStrNum);
         memcpy(pOutbuf+OFF_M_STRNUM, acTmp, iTmp);
         if (sMailAdr.strSub[0] > '0')
         {
            sprintf(acTmp, "%s  ", sMailAdr.strSub);
            memcpy(pOutbuf+OFF_M_STR_SUB, acTmp, SIZ_M_STR_SUB);
         }
      }

      if (sMailAdr.strDir[0] > ' ')
         vmemcpy(pOutbuf+OFF_M_DIR, sMailAdr.strDir, SIZ_M_DIR);

      vmemcpy(pOutbuf+OFF_M_STREET, sMailAdr.strName, SIZ_M_STREET);

      if (sMailAdr.strSfx[0] > ' ')
         vmemcpy(pOutbuf+OFF_M_SUFF, sMailAdr.strSfx, SIZ_M_SUFF);

      if (sMailAdr.Unit[0] > ' ')
      {
         if (sMailAdr.Unit[0] == '#')
            vmemcpy(pOutbuf+OFF_M_UNITNO, sMailAdr.Unit, SIZ_M_UNITNO);
         else
         {
            *(pOutbuf+OFF_M_UNITNO) = '#';
            vmemcpy(pOutbuf+OFF_M_UNITNO+1, sMailAdr.Unit, SIZ_M_UNITNO);
         }
      }

      if (bUnitAvail)
      {
         iTmp = sprintf(acTmp, "%s #%s", acAddr1, sMailAdr.Unit);
         vmemcpy(pOutbuf+OFF_M_ADDR_D, acTmp, SIZ_M_ADDR_D, iTmp);
      } else
         vmemcpy(pOutbuf+OFF_M_ADDR_D, acAddr1, SIZ_M_ADDR_D);

#ifdef _DEBUG
      //if (!memcmp(pOutbuf, "607224002", 9) )
      //   iTmp = 0;
#endif

      // City/State
      if (*apTokens[L_M_CITY] > ' ')
      {
         // Remove quote & comma at the end of city name
         strcpy(acAddr1, apTokens[L_M_CITY]);
         iTmp = iTrim(acAddr1);
         if (acAddr1[iTmp-1] == ',')
            acAddr1[iTmp-1] = 0;
         remUnPrtChar(acAddr1);

         if (*apTokens[L_M_STATE] > ' ')
         {
            vmemcpy(pOutbuf+OFF_M_CITY, acAddr1, SIZ_M_CITY);
            vmemcpy(pOutbuf+OFF_M_ST, apTokens[L_M_STATE], 2);

            iTmp = sprintf(acTmp, "%s, %s %s", acAddr1, apTokens[L_M_STATE], apTokens[L_M_ZIP]);
            vmemcpy(pOutbuf+OFF_M_CTY_ST_D, acTmp, SIZ_M_CTY_ST_D, iTmp);
         } else
         {
            vmemcpy(pOutbuf+OFF_M_CITY, acAddr1, SIZ_M_CITY+SIZ_M_ST+SIZ_M_ZIP);
            vmemcpy(pOutbuf+OFF_M_CTY_ST_D, acAddr1, SIZ_M_CTY_ST_D);
         }
      }

      iTmp = atol(apTokens[L_M_ZIP]);
      if (iTmp > 500)
         vmemcpy(pOutbuf+OFF_M_ZIP, apTokens[L_M_ZIP], SIZ_M_ZIP);
   }
}

/******************************* Riv_GetSaleChar *****************************
 *
 * Get QualityClass & Zoning from sale file and update CHAR rec
 *
 *****************************************************************************/

//void Riv_GetSaleChar(char *pOutbuf, FILE *fd)
//{
//   static   char acSaleRec[1024], *pRec;
//   char     acTmp[256], acCode[32], *pTmp;
//   int      iLoop, iTmp;
//
//   STDCHAR  *pStdChar = (STDCHAR *)pOutbuf;
//   RIV_SALE *pSale    = (RIV_SALE *)acSaleRec;
//
//   if (!pRec)
//   {
//      pRec = fgets(acSaleRec, 1024, fd);
//   }
//
//   do
//   {
//      // Compare Apn
//      iLoop = memcmp(pOutbuf, pSale->Assessment_No, iApnLen);
//      if (iLoop > 0)
//      {
//         pRec = fgets(acSaleRec, 1024, fd);
//         if (!pRec)
//         {
//            fclose(fd);
//            fd = NULL;
//            return;           // EOF
//         }
//      }
//   } while (iLoop > 0);
//
//   // If not match, return
//   if (iLoop)
//      return;
//
//   // Zoning
//   if (pSale->Zoning[0] > ' ')
//   {
//      pSale->Zoning[SSIZ_ZONE_CODE] = 0;
//      if (pTmp = strchr(pSale->Zoning, '|'))
//         *pTmp = '1';
//      memcpy(pStdChar->Zoning, pSale->Zoning, SSIZ_ZONE_CODE);
//   }
//
//   // Look for QualityClass
//   if (pSale->Construction_Type[0] > ' ')
//   {
//      pStdChar->BldgClass = pSale->Construction_Type[0];
//
//      // Quality
//      iTmp = atoin(pSale->Structure_Qual_Code, SSIZ_STRUCTURE_QUAL_CODE);
//      if (iTmp > 0)
//      {
//         sprintf(acTmp, "%.1f", (double)(iTmp/10.0));
//         iTmp = Value2Code(acTmp, acCode, NULL);
//         if (acCode[0] > ' ')
//            pStdChar->BldgQual = acCode[0];
//      }
//   }
//}

/******************************* Riv_AddUnits ********************************
 *
 * Use Riv_Units.txt to populate number of units.  This file is generated by 
 * importing the PropertyCharacteristics.csv into SQL then select those APN
 * with multiple count.
 *
 *****************************************************************************/

int Riv_AddUnits(char *pOutbuf, FILE **fdUnit)
{
   static   char acRec[512], *apItems[8], *pRec=NULL;
   static   int iItems=0;

   int      iLoop;
   STDCHAR  *pStdChar = (STDCHAR *)pOutbuf;

   // Get first Char rec for first call
   if (!pRec)
   {
      pRec = fgets(acRec, 512, *fdUnit);
      iItems = ParseString(acRec, '|', 3, apItems);
   }

   do
   {
      // Compare Apn
      iLoop = memcmp(pOutbuf, acRec, iApnLen);
      if (iLoop > 0)
      {
         if (bDebug)
            LogMsg0("Skip Unit rec  %s", pRec);
         pRec = fgets(acRec, 512, *fdUnit);
         if (!pRec)
         {
            fclose(*fdUnit);
            *fdUnit = NULL;
            return -1;      // EOF
         } else
            iItems = ParseString(acRec, '|', 3, apItems);
      }
   } while (iLoop > 0);

   // If not match, return
   if (iLoop)
      return 1;

#ifdef _DEBUG
   //if (!memcmp(pOutBase->Assmnt_No, "44257021", 8))
   //   iTmp = 0;
#endif
   if (iItems > 1)
      memcpy(pStdChar->Units, apItems[1], strlen(apItems[1]));
   
   return 0;
}

/****************************** Riv_ExtrChar ******************************
 *
 * Extract the whole char file to CSV format.  Cleaning up data where possible
 * This function is used to create "Riverside, CA XC.txt" for Black Knight.
 *
 * Return number of records output.
 *
 *****************************************************************************/

int Riv_ExtrChar(char *pCharfile)
{
   FILE     *fdIn, *fdOut;
   char     acInbuf[MAX_RECSIZE], acOutbuf[MAX_RECSIZE], acTmpFile[_MAX_PATH], acTmp[256],
				*pRec, *pDelim="|";
   int      iRet, iTmp, iCnt=0;

   LogMsg0("Extracting char file for bulk client");

   LogMsg("Open char file %s", pCharfile);
   if (!(fdIn = fopen(pCharfile, "r")))
   {
		LogMsg("***** Char file not available: %s.", pCharfile);
      return -1;
   }

	iRet = GetIniString(myCounty.acCntyCode, "CharExtr", "", acTmpFile, _MAX_PATH, acIniFile);
	if (!iRet)
	{
		LogMsg("*** Output file not defined.  Ignore extracting job.");
		return 0;
	}

   LogMsg("Open output file %s", acTmpFile);
   if (!(fdOut = fopen(acTmpFile, "w")))
   {
      fclose(fdIn);
      LogMsg("***** Error creating output file %s", acTmpFile);
      return -2;
   }

   // Skip first record - header
   pRec = fgets(acInbuf, MAX_RECSIZE, fdIn);

   // Print header
   pRec = "FIPS|APN_D|ROOF_TYPE|ROOF_MAT|PATIO_SF|DECK_SF|ELEVATOR|TOPO|FLATWORK_SF|GUESTHSE_SF|MISCIMPR_SF|PORCH_SF|FLOOR_1|FLOOR_2|FLOOR_3|BSMT_SF|MISC_IMPR|ETAL_FLG|BATH_1Q|BATH_2Q|BATH_3Q|BATH_4Q|IMPR_COND|NBH_CODE|TAX_STAT\n";
   fputs(pRec, fdOut);

   while (!feof(fdIn))
   {
      pRec = fgets(acInbuf, MAX_RECSIZE, fdIn);

      if (!pRec)
         break;

      iTokens = ParseString(pRec, cDelim, MAX_FLD_TOKEN, apTokens);
      if (iTokens < C_SOLAR)
      {
         if (iTokens > 1)
            LogMsg("*** Bad CHAR record (%d): %s [%d]", iCnt, pRec, iTokens);
         continue;
      } else if (iTokens > C_COLS)
         LogMsg("*** Please check for possible corruption record: %s", pRec);

		// Fips
		strcpy(acOutbuf, "06065");

      // Apn
      iTmp = formatApn(apTokens[C_APN], acTmp, &myCounty);
      AppendString(acOutbuf, acTmp, pDelim, false);


      // Roof type
      strcat(acOutbuf, pDelim);
      // Roof Material
      AppendString(acOutbuf, apTokens[C_ROOFMATERIAL], pDelim, false);
      // Patio Sqft
      strcat(acOutbuf, pDelim);
      // Deck Sqft
      strcat(acOutbuf, pDelim);
      // Elevator
      strcat(acOutbuf, pDelim);
      // Topo
      strcat(acOutbuf, pDelim);
      // FLATWORK_SF
      strcat(acOutbuf, pDelim);
      // GUESTHSE_SF
      strcat(acOutbuf, pDelim);
      // MISCIMPR_SF
      strcat(acOutbuf, pDelim);
      // PORCH_SF
      strcat(acOutbuf, pDelim);
      // FLOOR_1
      strcat(acOutbuf, pDelim);
      // FLOOR_2
      strcat(acOutbuf, pDelim);
      // FLOOR_3
      if (pRec = strchr(apTokens[C_ACTUALAREA], '.')) *pRec = 0;
      AppendString(acOutbuf, apTokens[C_ACTUALAREA], pDelim, false);
      // BSMT_SF
      strcat(acOutbuf, pDelim);
      // MISC_IMPR
      strcat(acOutbuf, pDelim);
      // ETAL_FLG
      strcat(acOutbuf, pDelim);
      // BATH_1Q
      strcat(acOutbuf, pDelim);
      // BATH_2Q
      if (*apTokens[C_BATHSHALF] > '0')
         AppendString(acOutbuf, apTokens[C_BATHSHALF], pDelim, false);
      else
         strcat(acOutbuf, pDelim);
      // BATH_3Q
      if (*apTokens[C_BATHS3_4] > '0')
         AppendString(acOutbuf, apTokens[C_BATHS3_4], pDelim, false);
      else
         strcat(acOutbuf, pDelim);
      // BATH_4Q
      if (*apTokens[C_BATHSFULL] > '0')
         AppendString(acOutbuf, apTokens[C_BATHSFULL], pDelim, false);
      else
         strcat(acOutbuf, pDelim);
      // IMPR_COND
      strcat(acOutbuf, pDelim);
      // NBH_CODE
      strcat(acOutbuf, pDelim);
      // TAX_STAT
      strcat(acOutbuf, pDelim);

      strcat(acOutbuf, "\n");

      fputs(acOutbuf, fdOut);

      if (!(++iCnt % 1000))
         printf("\r%u", iCnt);
   }

   fclose(fdIn);
   fclose(fdOut);

	LogMsg("Total CHAR records output: %d\n", iCnt);

   return iCnt;
}

/***************************** Riv_ConvStdCharCsv ****************************
 *
 * Convert PropertyCharacteristics.csv to STD_CHAR format
 * 03/04/2020 Add YrBlt
 * 04/09/2020 Add Acreage & LandUnitType
 * 03/29/2021 Add #Units from Riv_Units.txt
 * 12/05/2022 Fix APN extension by removing '-' and reformat APN_D properly
 * 10/10/2023 Update Riv_ConvStdCharCsv() since county added SOLAR flag
 * 04/23/2024 Change sort order of output file from DESC to ASC on YearBlt
 *
 *****************************************************************************/

int Riv_ConvStdCharCsv(char *pCharfile)
{
   char     acInbuf[1024], acOutbuf[2048], *pRec;
   char     acTmpFile[256], acTmp[256], acApn[64], cTmp1;
   double   dTmp;
   int      iRet, iTmp, iBeds, iFBath, iHBath, iBath3Q, iBath4Q, iCnt, lTmp, lSqft;

   STDCHAR  *pStdChar = (STDCHAR *)acOutbuf;
   FILE     *fdChar, *fdOut, *fdUnit;

   LogMsg0("Converting standard char file.");

   GetIniString(myCounty.acCntyCode, "UnitFile", "", acTmpFile, _MAX_PATH, acIniFile);
   fdUnit = (FILE *)NULL;
   if (!_access(acTmpFile, 0))
   {
      LogMsg("Open Unit file %s", acTmpFile);
      if (!(fdUnit = fopen(acTmpFile, "r")))
         LogMsg("*** Error opening Unit file %s.  Ignore updating number of Units", acTmpFile);
   }

   LogMsg("Open char file %s", pCharfile);
   if (!(fdChar = fopen(pCharfile, "r")))
      return -1;

   sprintf(acTmpFile, "%s\\%s\\%s_char.tmp", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   if (!(fdOut = fopen(acTmpFile, "w")))
   {
      fclose(fdChar);
      LogMsg("***** Error creating output file %s", acTmpFile);
      return -2;
   }

   // Skip header
   pRec = fgets(acInbuf, 1024, fdChar);

   iCnt = 0;
   while (!feof(fdChar))
   {
      pRec = fgets(acInbuf, 1024, fdChar);
      if (!pRec) break;

#ifdef _DEBUG
      //if (!memcmp(acInbuf, "009619939", 9))
      //   iTokens = 0;
#endif

      iTokens = ParseString(pRec, cDelim, MAX_FLD_TOKEN, apTokens);
      if (iTokens < C_SOLAR)
      {
         if (iTokens > 1)
            LogMsg("*** Bad CHAR record (%d): %s [%d]", iCnt, pRec, iTokens);
         continue;
      } else if (iTokens > C_COLS)
         LogMsg("*** Please check for possible corruption record: %s", pRec);

      memset(acOutbuf, ' ', sizeof(STDCHAR));

      // Format APN
      if (*(apTokens[C_APN]+9) == '-')
      {
         memcpy(acApn, apTokens[C_APN], 9);
         strcpy(&acApn[9], apTokens[C_APN]+10);
         iTmp = sprintf(acTmp, "%.3s-%.3s-%.3s-%s", acApn, &acApn[3], &acApn[6], &acApn[9]); 
         iApnLen = 12;
      } else
      {
         strcpy(acApn, apTokens[C_APN]);
         iTmp = sprintf(acTmp, "%.3s-%.3s-%s", acApn, &acApn[3], &acApn[6]); 
         iApnLen = 9;
      }

      memcpy(pStdChar->Apn, acApn, iApnLen);
      //iRet = formatApn(apTokens[C_APN], acTmp, &myCounty);
      memcpy(pStdChar->Apn_D, acTmp, iTmp);

      // Yrblt
      lTmp = atol(apTokens[C_YEARBUILT]);
      if (lTmp > 1700)
         memcpy(pStdChar->YrBlt, apTokens[C_YEARBUILT], SIZ_YR_BLT);
      lTmp = atol(apTokens[C_EFFYEARBUILT]);
      if (lTmp > 1900)
         memcpy(pStdChar->YrEff, apTokens[C_EFFYEARBUILT], SIZ_YR_BLT);

      // Stories
      iTmp = atol(apTokens[C_NUMBEROFSTORIES]);
      if (iTmp > 0 && iTmp < 100)
      {
         if (bDebug && iTmp > 9)
            LogMsg("*** Please verify number of stories for %s (%d)", apTokens[C_APN], iTmp);

         iRet = sprintf(acTmp, "%d.0", iTmp);
         memcpy(pStdChar->Stories, acTmp, iRet);
      }

      // Garage Sqft
      // Property may have more than one garage, but we only have space for one
      long lGarSqft = atol(apTokens[C_GARAGEAREA]) + atol(apTokens[C_GARAGE2AREA]);
      long lCarpSqft= atol(apTokens[C_CARPORTSIZE]) + atol(apTokens[C_CARPORT2SIZE]);

      // Garage type
      if (*apTokens[C_GARAGETYPE] == 'A' || *apTokens[C_GARAGE2TYPE] == 'A')
         pStdChar->ParkType[0] = 'I';              // Attached garage
      else if (*apTokens[C_GARAGETYPE] == 'D')
         pStdChar->ParkType[0] = 'L';              // Detached garage
      else if (lCarpSqft > 100)
         pStdChar->ParkType[0] = 'C';              // Carport

      if (lGarSqft > 100 || lCarpSqft > 100)
      {
         if (lGarSqft >= 100 && lCarpSqft > 100)
         {
            lGarSqft += lCarpSqft;
            pStdChar->ParkType[0] = '2';           // Garage/Carport
         }
         iTmp = sprintf(acTmp, "%d", lGarSqft);
         memcpy(pStdChar->GarSqft, acTmp, iTmp);
      }

      // Central Heating-Cooling
      if (*apTokens[C_HASCENTRALHEATING] == 'Y')
         pStdChar->Heating[0] = 'Z';
      if (*apTokens[C_HASCENTRALCOOLING] == 'Y')
         pStdChar->Cooling[0] = 'C';

      // Beds
      iBeds = atol(apTokens[C_BEDROOMCOUNT]);
      if (iBeds > 0)
      {
         iTmp = sprintf(acTmp, "%d", iBeds);
         memcpy(pStdChar->Beds, acTmp, iTmp);
      }

#ifdef _DEBUG
      //if (!memcmp(pStdChar->Apn, "009600002", 9) )
      //   lTmp = 0;
#endif

      // Bath
      iBath4Q = atol(apTokens[C_BATHSFULL]);
      iHBath  = atol(apTokens[C_BATHSHALF]);
      iBath3Q = atol(apTokens[C_BATHS3_4]);
      iFBath = iBath3Q+iBath4Q;
      if (iBath3Q > 0)
      {
         iTmp = sprintf(acTmp, "%d", iBath3Q);
         memcpy(pStdChar->Bath_3QX, acTmp, iTmp);
         if (iTmp < 3)
            memcpy(pStdChar->Bath_3Q, acTmp, iTmp);
      }

      if (iBath4Q > 0)
      {
         iTmp = sprintf(acTmp, "%d", iBath4Q);
         memcpy(pStdChar->Bath_4QX, acTmp, iTmp);
         if (iTmp < 3)
            memcpy(pStdChar->Bath_4Q, acTmp, iTmp);
      }

      if (iFBath > 0 && iFBath < 100)
      {
         iTmp = sprintf(acTmp, "%d", iFBath);
         memcpy(pStdChar->FBaths, acTmp, iTmp);
      }

      if (iHBath > 0)
      {
         iTmp = sprintf(acTmp, "%d", iHBath);
         memcpy(pStdChar->Bath_2QX, acTmp, iTmp);
         if (iTmp < 3)
         {
            memcpy(pStdChar->HBaths, acTmp, iTmp);
            memcpy(pStdChar->Bath_2Q, acTmp, iTmp);
         }
      }

      // Fireplace
      if (*apTokens[C_HASFIREPLACE] == 'T')
         pStdChar->Fireplace[0] = 'Y';
      if (*apTokens[C_HASFIREPLACE] == 'F')
         pStdChar->Fireplace[0] = 'N';

      // Roof
      if (*apTokens[C_ROOFMATERIAL] >= 'A')
      {
         _strupr(apTokens[C_ROOFMATERIAL]);
         pRec = findXlatCodeA(apTokens[C_ROOFMATERIAL], &asRoofMat[0]);
         if (pRec)
            pStdChar->RoofMat[0] = *pRec;
      }

      // Pool/Spa
      if (*apTokens[C_HASPOOL] == '1')
         pStdChar->Pool[0] = 'P';       // Pool

      // Electric
      if (*apTokens[C_UTILITYELECTRIC] >= 'A')
      {
         switch (*apTokens[C_UTILITYELECTRIC])
         {
            case 'A':
            case 'D':
               cTmp1 = *apTokens[C_UTILITYELECTRIC];  // AVAIL
               break;
            case 'N':
               cTmp1 = 'N';                           // NON AVAIL
               break;
            default:
               if (!_memicmp(apTokens[C_UTILITYELECTRIC], "Under", 5))
                  cTmp1 = 'D';
               else
                  cTmp1 = ' ';
         }
         pStdChar->HasElectric = cTmp1;
      }

      // Gas
      if (*apTokens[C_UTILITYGAS] >= 'A')
      {
         switch (*apTokens[C_UTILITYGAS])
         {
            case 'A':
            case 'D':
               cTmp1 = *apTokens[C_UTILITYGAS];    // AVAIL
               break;
            case 'N':
               cTmp1 = 'N';                        // NON AVAIL
               break;
            default:
               cTmp1 = ' ';
         }
         pStdChar->HasGas = cTmp1;
      }

      // Water
      if (*apTokens[C_DOMESTICWATER] == 'A' || *apTokens[C_DOMESTICWATER] == 'D')
      {
         pStdChar->Water = *apTokens[C_DOMESTICWATER];
         pStdChar->HasWater = 'Y';
      } else if (*apTokens[C_WELLWATER] == 'A' || *apTokens[C_WELLWATER] == 'D')
      {
         pStdChar->Water = *apTokens[C_WELLWATER];
         pStdChar->HasWater = 'W';
         pStdChar->HasWell = 'Y';
      } else if (*apTokens[C_IRRIGATIONWATER] == 'A' || *apTokens[C_IRRIGATIONWATER] == 'D')
      {
         pStdChar->HasWater = 'L';
         pStdChar->Water = *apTokens[C_IRRIGATIONWATER];
      } else if (*apTokens[C_DOMESTICWATER] == 'N' || *apTokens[C_WELLWATER] == 'N' || *apTokens[C_IRRIGATIONWATER] == 'N')
      {
         pStdChar->Water = 'N';
         pStdChar->HasWater = 'N';
      }

      // Sewer
      if (*apTokens[C_SEWER] > ' ')
      {
         pStdChar->Sewer = *apTokens[C_SEWER];
         if (*apTokens[C_SEWER] == 'A' || *apTokens[C_SEWER] == 'D')
            pStdChar->HasSewer = 'Y';
      }

      // View
      if (*apTokens[C_FAIRWAY] == 'Y')
         pStdChar->View[0] = 'H';   
      else if (*apTokens[C_WATERFRONT] == 'Y')
         pStdChar->View[0] = 'W';   

      // BldgSqft
      lSqft = atol(apTokens[C_LIVINGAREA]);
      if (lSqft > 0)
      {
         iTmp = sprintf(acTmp, "%d", lSqft);
         memcpy(pStdChar->BldgSqft, acTmp, iTmp);
      }

      // ActualSqft
      lSqft = atol(apTokens[C_ACTUALAREA]);
      if (lSqft > 0)
      {
         iTmp = sprintf(acTmp, "%d", lSqft);
         memcpy(pStdChar->Sqft_Above2nd, acTmp, iTmp);
      }

      // BldgClass 
      if (pRec = strchr(apTokens[C_CONSTRUCTIONTYPE], '('))
         pStdChar->BldgClass = *(pRec+1);

      // Quality
      dTmp = atof(apTokens[C_QUALITYCODE]);
      if (dTmp > 0.1)
      {
         char acCode[16];

         sprintf(acTmp, "%.1f", dTmp);
         iTmp = Value2Code(acTmp, acCode, NULL);
         if (acCode[0] > ' ')
            pStdChar->BldgQual = acCode[0];
      }

      // QualityClass
      if (pStdChar->BldgClass > ' ' && dTmp > 0.1)
      {
         sprintf(acTmp, "%c%.1f%c", pStdChar->BldgClass, dTmp, *apTokens[C_SHAPECODE]);
         vmemcpy(pStdChar->QualityClass, acTmp, SIZ_CHAR_QCLS, strlen(acTmp));
      }

      // Lot sqft - Lot Acres
      dTmp = atof(apTokens[C_ACREAGE]);
      if (dTmp > 0.001)
      {
         if (*apTokens[C_LANDUNITTYPE] == 'A' || *apTokens[C_LANDUNITTYPE] == 'U')
         {
            iTmp = sprintf(acTmp, "%d", (long)(dTmp*1000));
            memcpy(pStdChar->LotAcre, acTmp, iTmp);
            lSqft = long(dTmp*SQFT_PER_ACRE);
         } else
         {
            iTmp = sprintf(acTmp, "%d", (long)(dTmp/SQFT_FACTOR_1000));
            memcpy(pStdChar->LotAcre, acTmp, iTmp);
            lSqft = (long)dTmp;
         }

         iTmp = sprintf(acTmp, "%d", lSqft);
         memcpy(pStdChar->LotSqft, acTmp, iTmp);
      }

      // Add #Units
      if (fdUnit)
      {
         Riv_AddUnits(acOutbuf, &fdUnit);
      }

      if (iTokens > C_SOLAR)
         pStdChar->HasSolar = *apTokens[C_SOLAR];

      pStdChar->CRLF[0] = '\n';
      pStdChar->CRLF[1] = '\0';
      fputs(acOutbuf, fdOut);

      if (!(++iCnt % 1000))
         printf("\r%u", iCnt);
   }

   if (fdChar) fclose(fdChar);
   if (fdOut)  fclose(fdOut);
   if (fdUnit) fclose(fdUnit);
   LogMsg("Number of records processed: %d", iCnt);

   // Sort output on ASMT
   if (iCnt > 100)
   {
      // Rename old CHAR file if needed
      if (!_access(acCChrFile, 0))
      {
         strcpy(acInbuf, acCChrFile);
         replStr(acInbuf, ".dat", ".sav");
         if (!_access(acInbuf, 0))
         {
            LogMsg("Delete old %s", acInbuf);
            DeleteFile(acInbuf);
         }
         LogMsg("Rename %s to %s", acCChrFile, acInbuf);
         RenameToExt(acCChrFile, "sav");
      }

      LogMsgD("\nSorting char file %s --> %s", acTmpFile, acCChrFile);
      // Sort on APN, YrBlt to keep the latest record on top
      iRet = sortFile(acTmpFile, acCChrFile, "S(1,14,C,A,53,4,C,A) F(TXT) ");
   } else
   {
      printf("\n");
      iRet = 0;
   }

   return iRet;
}

/***************************** Riv_ConvStdCharCsv ****************************
 *
 * Convert "Assessor_PropertyCharacteristics_Table.csv" to STD_CHAR format
 *
 * No View, Water, Sewer, Electric, Gas
 *
 *****************************************************************************/

//int Riv_ConvStdCharCsv_Asr(char *pCharfile)
//{
//   char     acInbuf[1024], acOutbuf[2048], *pRec;
//   char     acTmpFile[256], acTmp[256], cTmp1;
//   double   dTmp;
//   int      iRet, iTmp, iBeds, iFBath, iHBath, iCnt, lTmp, lSqft;
//
//   STDCHAR  *pStdChar = (STDCHAR *)acOutbuf;
//   FILE     *fdChar, *fdOut;
//
//   LogMsg0("Converting standard char file %s\n", pCharfile);
//   if (!(fdChar = fopen(pCharfile, "r")))
//      return -1;
//
//   sprintf(acTmpFile, "%s\\%s\\%s_char.tmp", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
//   if (!(fdOut = fopen(acTmpFile, "w")))
//   {
//      fclose(fdChar);
//      LogMsg("***** Error creating output file %s", acTmpFile);
//      return -2;
//   }
//
//   // Skip header
//   pRec = fgets(acInbuf, 1024, fdChar);
//
//   iCnt = 0;
//   while (!feof(fdChar))
//   {
//      pRec = fgets(acInbuf, 1024, fdChar);
//      if (!pRec) break;
//
//      iTokens = ParseStringNQ(pRec, cDelim, MAX_FLD_TOKEN, apTokens);
//      if (iTokens < C_FLDS)
//      {
//         if (iTokens > 1)
//            LogMsg("*** Bad CHAR record (%d): %s [%d]", iCnt, pRec, iTokens);
//         continue;
//      }
//
//      memset(acOutbuf, ' ', sizeof(STDCHAR));
//
//      // Format APN
//      memcpy(pStdChar->Apn, apTokens[C_ASMT], strlen(apTokens[C_ASMT]));
//      iRet = formatApn(apTokens[C_ASMT], acTmp, &myCounty);
//      memcpy(pStdChar->Apn_D, acTmp, iRet);
//
//      // Yrblt
//      lTmp = atol(apTokens[C_YEAR_CONSTRUCTED]);
//      if (lTmp > 1700)
//         memcpy(pStdChar->YrBlt, apTokens[C_YEAR_CONSTRUCTED], SIZ_YR_BLT);
//
//      // Stories
//      iTmp = atol(apTokens[C_NBR_STORIES]);
//      if (iTmp > 0)
//      {
//         if (bDebug && iTmp > 9)
//            LogMsg("*** Please verify number of stories for %s (%d)", apTokens[C_ASMT], iTmp);
//
//         iRet = sprintf(acTmp, "%d.0", iTmp);
//         memcpy(pStdChar->Stories, acTmp, iRet);
//      }
//
//      // Garage Sqft
//      // Property may have more than one garage, but we only have space for one
//      long lGarSqft = atol(apTokens[C_GARAGE1_AREA]);
//      long lCarpSqft= atol(apTokens[C_CARPORT1_AREA]);
//      if (lGarSqft > 10 || lCarpSqft > 10)
//      {
//         if (lGarSqft >= 200 && lCarpSqft > 10)
//            lGarSqft += lCarpSqft;
//
//         iTmp = sprintf(acTmp, "%d", lGarSqft);
//         memcpy(pStdChar->GarSqft, acTmp, iTmp);
//      }
//
//      // Garage type
//      if (*apTokens[C_GARAGE1_TYPE] == 'A')
//         pStdChar->ParkType[0] = 'I';              // Attached garage
//      else if (*apTokens[C_GARAGE1_TYPE] == 'D')
//         pStdChar->ParkType[0] = 'L';              // Detached garage
//      else if (lCarpSqft > 10)
//         pStdChar->ParkType[0] = 'C';           // Carport
//
//      // Central Heating-Cooling
//      if (*apTokens[C_CENTRAL_HEAT] == 'Y')
//         pStdChar->Heating[0] = 'Z';
//      if (*apTokens[C_CENTRAL_COOL] == 'Y')
//         pStdChar->Cooling[0] = 'C';
//
//      // Beds
//      iBeds = atol(apTokens[C_BEDROOMS]);
//      if (iBeds > 0)
//      {
//         iTmp = sprintf(acTmp, "%d", iBeds);
//         memcpy(pStdChar->Beds, acTmp, iTmp);
//      }
//
//#ifdef _DEBUG
//      //if (!memcmp(pStdChar->Apn, "009600002", 9) )
//      //   lTmp = 0;
//#endif
//
//      // Bath
//      iFBath = atoi(apTokens[C_BATHS]);
//      iHBath = 0;
//      if (pRec = strchr(apTokens[C_BATHS], '.'))
//      {
//         iTmp = atol(pRec+1);
//         if (iTmp == 5 || iTmp == 50)
//         {
//            pStdChar->Bath_2Q[0] = '1';
//            iHBath = 1;
//         }
//         else if (iTmp == 75)
//         {
//            iRet = sprintf(acTmp, "%d", iFBath);
//            memcpy(pStdChar->Bath_4Q, acTmp, iRet);
//            pStdChar->Bath_3Q[0] = '1';
//            iFBath += 1;
//         }
//         else if (iTmp == 25)
//         {
//            pStdChar->Bath_1Q[0] = '1';
//            iHBath = 1;
//         }
//      } else if (iFBath > 0)
//      {
//         iRet = sprintf(acTmp, "%d", iFBath);
//         memcpy(pStdChar->Bath_4Q, acTmp, iRet);
//      }
//
//      if (iFBath > 0)
//      {
//         iTmp = sprintf(acTmp, "%d", iFBath);
//         memcpy(pStdChar->FBaths, acTmp, iTmp);
//
//         if (pStdChar->Bath_4Q[0] < '1')
//            memcpy(pStdChar->Bath_4Q, acTmp, iTmp);
//      }
//      if (iHBath > 0)
//      {
//         iTmp = sprintf(acTmp, "%d", iHBath);
//         memcpy(pStdChar->HBaths, acTmp, iTmp);
//      }
//
//      // Fireplace
//      if (*apTokens[C_FIREPLACE_FLAG] >= 'N')
//         pStdChar->Fireplace[0] = *apTokens[C_FIREPLACE_FLAG];
//
//      // Roof
//      if (*apTokens[C_ROOF_TYPE] > '0')
//      {
//         switch (*apTokens[C_ROOF_TYPE])
//         {
//            case '1':
//               cTmp1 = 'I';   // 1=TILE
//               break;
//            case '2':
//               cTmp1 = 'B';   // 2=SHAKE
//               break;
//            case '3':
//               cTmp1 = 'H';   // 3=ROCK & GRAVEL/COMPOSITION
//               break;
//            default:
//               cTmp1 = ' ';
//         }
//         pStdChar->RoofMat[0] = cTmp1;
//      }
//
//      // Pool/Spa
//      if (*apTokens[C_POOL] == 'Y')
//         pStdChar->Pool[0] = 'P';       // Pool
//
//      // Electric
//      //pStdChar->HasElectric = pCharRec->Elec[0];
//
//      // Gas
//      //pStdChar->HasGas = pCharRec->Gas[0];
//
//      // Water - N=NONE,Y=YES,D=DEVLOPD,U=UNDRGRD,F=
//
//      //if (pCharRec->Water[0] > '0')
//      //{
//      //   switch (pCharRec->Water[0])
//      //   {
//      //      case 'A':
//      //      case 'D':
//      //      case 'Y':
//      //         cTmp1 = 'A';   // AVAIL
//      //         break;
//      //      case 'N':
//      //         cTmp1 = 'N';   // NON AVAIL
//      //         break;
//      //      default:
//      //         cTmp1 = ' ';
//      //   }
//      //   pStdChar->Water = cTmp1;
//      //}
//
//      // Sewer
//      //if (pCharRec->Sewer[0] > '0')
//      //{
//      //   switch (pCharRec->Sewer[0])
//      //   {
//      //      case 'A':
//      //      case 'D':
//      //         cTmp1 = 'A';   // AVAIL
//      //         break;
//      //      case 'N':
//      //         cTmp1 = 'X';   // NON AVAIL
//      //         break;
//      //      case 'Y':
//      //         cTmp1 = 'Y';   // YES
//      //         break;
//      //      default:
//      //         cTmp1 = ' ';
//      //   }
//      //   pStdChar->Sewer = cTmp1;
//      //}
//
//      // View
//      if (*apTokens[C_WTR_FAIR] > '0')
//      {
//         switch (*apTokens[C_WTR_FAIR])
//         {
//            case 'W':
//               pStdChar->View[0] = 'W';   // Water front
//               break;
//            case 'F':
//               pStdChar->View[0] = 'H';   // Fairway
//               break;
//            default:
//               break;
//         }
//      }
//
//      // BldgSqft
//      lSqft = atol(apTokens[C_AREA_1]);
//      if (lSqft > 0)
//      {
//         iTmp = sprintf(acTmp, "%d", lSqft);
//         memcpy(pStdChar->BldgSqft, acTmp, iTmp);
//      }
//
//      // Lot sqft - Lot Acres
//      //lSqft = atol(pCharRec->Acres, CSIZ_ACRES);
//      //if (lSqft > 0)
//      //{
//      //   iTmp = sprintf(acTmp, "%d", lSqft*10);
//      //   memcpy(pStdChar->LotAcre, acTmp, iTmp);
//      //   lTmp = long((double)lSqft*SQFT_FACTOR_100);
//      //   iTmp = sprintf(acTmp, "%d", lTmp);
//      //   memcpy(pStdChar->LotSqft, acTmp, iTmp);
//      //}
//
//      // BldgClass 
//      if (*apTokens[C_CONST_TYPE] > '0')
//         pStdChar->BldgClass = *apTokens[C_CONST_TYPE];
//
//      // Quality
//      dTmp = atof(apTokens[C_STR_QUAL_CD]);
//      if (dTmp > 0.1)
//      {
//         char acCode[16];
//
//         sprintf(acTmp, "%.1f", dTmp);
//         iTmp = Value2Code(acTmp, acCode, NULL);
//         if (acCode[0] > ' ')
//            pStdChar->BldgQual = acCode[0];
//      }
//
//      pStdChar->CRLF[0] = '\n';
//      pStdChar->CRLF[1] = '\0';
//      fputs(acOutbuf, fdOut);
//
//      if (!(++iCnt % 1000))
//         printf("\r%u", iCnt);
//   }
//
//   if (fdChar) fclose(fdChar);
//   if (fdOut)  fclose(fdOut);
//
//   // Sort output on ASMT
//   if (iCnt > 100)
//   {
//      // Rename old CHAR file if needed
//      if (!_access(acCChrFile, 0))
//      {
//         strcpy(acInbuf, acCChrFile);
//         replStr(acInbuf, ".dat", ".sav");
//         if (!_access(acInbuf, 0))
//         {
//            LogMsg("Delete old %s", acInbuf);
//            DeleteFile(acInbuf);
//         }
//         LogMsg("Rename %s to %s", acCChrFile, acInbuf);
//         RenameToExt(acCChrFile, "sav");
//      }
//
//      LogMsgD("\nSorting char file %s --> %s", acTmpFile, acCChrFile);
//      // Sort on APN, YrBlt to keep the latest record on top
//      iRet = sortFile(acTmpFile, acCChrFile, "S(1,14,C,A,53,4,C,D) F(TXT) ");
//   } else
//   {
//      printf("\n");
//      iRet = 0;
//   }
//
//   LogMsg("Number of records output: %d", iCnt);
//   return iRet;
//}

/******************************* Riv_ConvStdChar *****************************
 *
 * Convert char file to STD_CHAR format using data from both rivprch & rivsales.
 *
 * Get QualityClass & Zoning from sale file
 *
 *****************************************************************************/

//int Riv_ConvStdChar(char *pCharfile, char *pSaleFile)
//{
//   char     acInbuf[1024], acOutbuf[2048];
//   char     acTmpFile[256], acTmp[256], cTmp1;
//   long     lTmp, lSqft;
//   int      iRet, iTmp, iBeds, iFBath, iHBath, iCnt;
//
//   RIV_CHAR *pCharRec = (RIV_CHAR *)acInbuf;
//   STDCHAR  *pStdChar = (STDCHAR *)acOutbuf;
//   FILE     *fdChar, *fdOut;
//
//   LogMsg("Converting standard char file %s\n", pCharfile);
//   if (!(fdChar = fopen(pCharfile, "r")))
//      return -1;
//
//   LogMsg("Open sale file %s\n", pSaleFile);
//   fdSale = fopen(pSaleFile, "r");
//
//   sprintf(acTmpFile, "%s\\%s\\%s_char.tmp", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
//   if (!(fdOut = fopen(acTmpFile, "w")))
//   {
//      fclose(fdChar);
//      LogMsg("***** Error creating output file %s", acTmpFile);
//      return -2;
//   }
//
//   iCnt = 0;
//   while (!feof(fdChar))
//   {
//      //pRec = fgets(acInbuf, 1024, fdIn);
//      //if (!pRec) break;
//      iRet = fread(acInbuf, 1, iCharLen, fdChar);
//      if (iRet != iCharLen) break;
//      if (acInbuf[0] <= ' ')
//         continue;
//
//      memset(acOutbuf, ' ', sizeof(STDCHAR));
//
//      // Format APN
//      memcpy(pStdChar->Apn, pCharRec->Primary_Parcel_No, CSIZ_PRIMARY_PARCEL_NO);
//      iRet = formatApn(pCharRec->Primary_Parcel_No, acTmp, &myCounty);
//      memcpy(pStdChar->Apn_D, acTmp, iRet);
//
//      // Yrblt
//      lTmp = atoin(pCharRec->Constr_Year, CSIZ_CONSTR_YEAR);
//      if (lTmp > 1700)
//         memcpy(pStdChar->YrBlt, pCharRec->Constr_Year, SIZ_YR_BLT);
//
//      // Stories
//      iTmp = atoin(pCharRec->Stories, CSIZ_STORIES);
//      if (iTmp > 0)
//      {
//         if (bDebug && iTmp > 9)
//            LogMsg("*** Please verify number of stories for %.*s (%d)", iApnLen, pCharRec->Primary_Parcel_No, iTmp);
//
//         sprintf(acTmp, "%d.0  ", iTmp);
//         memcpy(pStdChar->Stories, acTmp, SIZ_STORIES);
//      }
//
//      // Garage Sqft
//      // Property may have more than one garage, but we only have space for one
//      long lGarSqft = atoin(pCharRec->Garage1_Sqft, CSIZ_GARAGE1_SQFT);
//      long lCarpSqft= atoin(pCharRec->Carport1_Sqft, CSIZ_CARPORT1_SQFT);
//      if (lGarSqft > 10 || lCarpSqft > 10)
//      {
//         if (pCharRec->Garage1 == pCharRec->Garage2)
//            lGarSqft += atoin(pCharRec->Garage2_Sqft, CSIZ_GARAGE1_SQFT);
//
//         if (pCharRec->Garage1[0] == '2' && lCarpSqft > 0)
//            lGarSqft += lCarpSqft;
//
//         iTmp = sprintf(acTmp, "%d", lGarSqft);
//         memcpy(pStdChar->GarSqft, acTmp, iTmp);
//      }
//
//      // Garage type
//      if (pCharRec->Garage1[0] == '1')
//         pStdChar->ParkType[0] = 'I';              // Attached garage
//      else if (pCharRec->Garage1[0] == '2')
//      {
//         if (lCarpSqft > 0)
//            pStdChar->ParkType[0] = 'C';           // Carport
//         else
//            pStdChar->ParkType[0] = 'L';           // Detached garage
//      }
//
//      // Central Heating-Cooling
//      if (pCharRec->Central_Heat[0] == 'Y')
//         pStdChar->Heating[0] = 'Z';
//      if (pCharRec->Central_Air[0] == 'Y')
//         pStdChar->Cooling[0] = 'C';
//
//      // Beds
//      iBeds = atoin(pCharRec->Bedrooms, CSIZ_BEDROOMS);
//      if (iBeds > 0)
//      {
//         iTmp = sprintf(acTmp, "%d", iBeds);
//         memcpy(pStdChar->Beds, acTmp, iTmp);
//      }
//
//      // Bath
//      iFBath = 0;
//      if (pCharRec->Full_Baths[0] > '0')
//      {
//         iFBath = pCharRec->Full_Baths[0] & 0x0F;
//         pStdChar->Bath_4Q[0] = pCharRec->Full_Baths[0];
//      }
//
//      if (pCharRec->Baths_34[0] > '0')
//      {
//         // Save new full baths where 3/4 bath is added
//         iFBath += pCharRec->Baths_34[0] & 0x0F;
//         iTmp = sprintf(acTmp, "%d", iFBath);
//         vmemcpy(pStdChar->FBaths, acTmp, iTmp);
//
//         // Save 3/4 baths for Lexis
//         pStdChar->Bath_3Q[0] = pCharRec->Baths_34[0];
//      } else if (iFBath > 0)
//      {
//         iTmp = sprintf(acTmp, "%d", iFBath);
//         memcpy(pStdChar->FBaths, acTmp, iTmp);
//      }
//
//      // Half bath
//      if (pCharRec->Half_Baths[0] > '0')
//      {
//         pStdChar->Bath_2Q[0] = pCharRec->Half_Baths[0];
//
//         iHBath = pCharRec->Half_Baths[0] & 0x0F;
//         iTmp = sprintf(acTmp, "%d", iHBath);
//         memcpy(pStdChar->HBaths, acTmp, iTmp);
//      }
//
//      // Fireplace
//      pStdChar->Fireplace[0] = pCharRec->Fireplace[0];
//
//      // Roof
//      if (pCharRec->Roof_Type[0] > '0')
//      {
//         switch (pCharRec->Roof_Type[0])
//         {
//            case '1':
//               cTmp1 = 'I';   // 1=TILE
//               break;
//            case '2':
//               cTmp1 = 'B';   // 2=SHAKE
//               break;
//            case '3':
//               cTmp1 = 'H';   // 3=ROCK & GRAVEL/COMPOSITION
//               break;
//            default:
//               cTmp1 = ' ';
//         }
//         pStdChar->RoofMat[0] = cTmp1;
//      }
//
//#ifdef _DEBUG
//      //if (!memcmp(pStdChar->Apn, "009600001", 9) )
//      //   lTmp = 0;
//#endif
//      // Pool/Spa
//      if (pCharRec->Pool[0] == 'Y')
//         pStdChar->Pool[0] = 'P';       // Pool
//
//      // Electric
//      pStdChar->HasElectric = pCharRec->Elec[0];
//
//      // Gas
//      pStdChar->HasGas = pCharRec->Gas[0];
//
//      // Water - N=NONE,A=AVAIL,D=DEVLOPD,U=UNDRGRD
//      if (pCharRec->Water[0] > '0')
//      {
//         switch (pCharRec->Water[0])
//         {
//            case 'A':
//            case 'D':
//            case 'Y':
//               cTmp1 = 'A';   // AVAIL
//               break;
//            case 'N':
//               cTmp1 = 'N';   // NON AVAIL
//               break;
//            default:
//               cTmp1 = ' ';
//         }
//         pStdChar->Water = cTmp1;
//      }
//
//      // Sewer
//      if (pCharRec->Sewer[0] > '0')
//      {
//         switch (pCharRec->Sewer[0])
//         {
//            case 'A':
//            case 'D':
//               cTmp1 = 'A';   // AVAIL
//               break;
//            case 'N':
//               cTmp1 = 'X';   // NON AVAIL
//               break;
//            case 'Y':
//               cTmp1 = 'Y';   // YES
//               break;
//            default:
//               cTmp1 = ' ';
//         }
//         pStdChar->Sewer = cTmp1;
//      }
//
//      // View
//      if (pCharRec->Water_Front[0] > '0')
//      {
//         switch (pCharRec->Water_Front[0])
//         {
//            case '1':
//               cTmp1 = 'W';   // Water front
//               break;
//            case '2':
//               cTmp1 = 'H';   // Fairway
//               break;
//            default:
//               cTmp1 = ' ';
//         }
//         pStdChar->View[0] = cTmp1;
//      }
//
//      // BldgSqft
//      lSqft = atoin(pCharRec->Living_Area, CSIZ_LIVING_AREA);
//      if (lSqft > 0)
//      {
//         iTmp = sprintf(acTmp, "%d", lSqft);
//         memcpy(pStdChar->BldgSqft, acTmp, iTmp);
//      }
//
//      // Lot sqft - Lot Acres
//      lSqft = atoin(pCharRec->Acres, CSIZ_ACRES);
//      if (lSqft > 0)
//      {
//         iTmp = sprintf(acTmp, "%d", lSqft*10);
//         memcpy(pStdChar->LotAcre, acTmp, iTmp);
//         lTmp = long((double)lSqft*SQFT_FACTOR_100);
//         iTmp = sprintf(acTmp, "%d", lTmp);
//         memcpy(pStdChar->LotSqft, acTmp, iTmp);
//      }
//
//      // QualityClass - from sale file
//      if (fdSale)
//         Riv_GetSaleChar(acOutbuf, fdSale);
//
//      pStdChar->CRLF[0] = '\n';
//      pStdChar->CRLF[1] = '\0';
//      fputs(acOutbuf, fdOut);
//
//      if (!(++iCnt % 1000))
//         printf("\r%u", iCnt);
//   }
//
//   if (fdChar) fclose(fdChar);
//   if (fdSale) fclose(fdSale);
//   if (fdOut)  fclose(fdOut);
//
//   // Sort output on ASMT
//   if (iCnt > 100)
//   {
//      // Rename old CHAR file if needed
//      if (!_access(acCChrFile, 0))
//      {
//         strcpy(acInbuf, acCChrFile);
//         replStr(acInbuf, ".dat", ".sav");
//         if (!_access(acInbuf, 0))
//         {
//            LogMsg("Delete old %s", acInbuf);
//            DeleteFile(acInbuf);
//         }
//         LogMsg("Rename %s to %s", acCChrFile, acInbuf);
//         RenameToExt(acCChrFile, "sav");
//      }
//
//      LogMsgD("\nSorting char file %s --> %s", acTmpFile, acCChrFile);
//      // Sort on APN, YrBlt to keep the latest record on top
//      iRet = sortFile(acTmpFile, acCChrFile, "S(1,14,C,A,53,4,C,D) F(TXT) ");
//   } else
//   {
//      printf("\n");
//      iRet = 0;
//   }
//
//   LogMsg("Number of records output: %d", iCnt);
//   return iRet;
//}

/********************************* Riv_MergeChar *****************************
 *
 *
 *
 *****************************************************************************/

//int Riv_MergeChar(char *pOutbuf)
//{
//   static char    acRec[1024], *pRec=NULL;
//   char           acTmp[256], cTmp1;
//   long           lTmp, lBldgSqft, lGarSqft, lCarpSqft;
//   int            iRet, iTmp, iLoop, iBeds, iFBath, iHBath, iFp;
//   RIV_CHAR *pChar;
//
//   iRet=iBeds=iFBath=iHBath=iFp=0;
//   lBldgSqft=lGarSqft=0;
//
//   // Get first Char rec for first call
//   if (!pRec)
//   {
//      //iRet = fread(acRec, 1, MAX_CHARREC, fdChar);
//      iRet = fread(acRec, 1, iCharLen, fdChar);
//      pRec = acRec;
//   }
//
//   do
//   {
//      // Compare Apn
//      iLoop = memcmp(pOutbuf, acRec, myCounty.iApnLen);
//      if (iLoop > 0)
//      {
//         if (bDebug)
//            LogMsg0("Skip Char rec  %.*s", myCounty.iApnLen, pRec);
//
//         //iRet = fread(acRec, 1, MAX_CHARREC, fdChar);
//         iRet = fread(acRec, 1, iCharLen, fdChar);
//         if (!iRet)
//         {
//            fclose(fdChar);
//            fdChar = NULL;
//            return -1;      // EOF
//         }
//         lCharSkip++;
//      }
//   } while (iLoop > 0);
//
//   // If not match, return
//   if (iLoop)
//      return 1;
//
//   pChar = (RIV_CHAR *)acRec;
//
//
//   lBldgSqft = atoin(pChar->Living_Area, CSIZ_LIVING_AREA);
//   // This is only a flag, not a value
//   // lBldgSqft += atoin(pChar->Addt_Area, CSIZ_ADDT_AREA);
//   if (lBldgSqft > 0)
//   {
//      sprintf(acTmp, "%*u", SIZ_BLDG_SF, lBldgSqft);
//      memcpy(pOutbuf+OFF_BLDG_SF, acTmp, SIZ_BLDG_SF);
//   }
//
//   // Yrblt
//   lTmp = atoin(pChar->Constr_Year, CSIZ_CONSTR_YEAR);
//   if (lTmp > 1700)
//      memcpy(pOutbuf+OFF_YR_BLT, pChar->Constr_Year, SIZ_YR_BLT);
//
//   // Stories
//   iTmp = atoin(pChar->Stories, CSIZ_STORIES);
//   if (iTmp > 0)
//   {
//      if (bDebug && iTmp > 9)
//         LogMsg("*** Please verify number of stories for %.*s (%d)", iApnLen, pOutbuf, iTmp);
//
//      sprintf(acTmp, "%d.0  ", iTmp);
//      memcpy(pOutbuf+OFF_STORIES, acTmp, SIZ_STORIES);
//   }
//
//   // Garage Sqft
//   // Property may have more than one garage, but we only have space for one
//   lGarSqft = atoin(pChar->Garage1_Sqft, CSIZ_GARAGE1_SQFT);
//   lCarpSqft= atoin(pChar->Carport1_Sqft, CSIZ_CARPORT1_SQFT);
//   if (lGarSqft > 10 || lCarpSqft > 10)
//   {
//      if (pChar->Garage1 == pChar->Garage2)
//         lGarSqft += atoin(pChar->Garage2_Sqft, CSIZ_GARAGE1_SQFT);
//
//      if (pChar->Garage1[0] == '2' && lCarpSqft > 0)
//         lGarSqft += lCarpSqft;
//
//      sprintf(acTmp, "%*u", SIZ_GAR_SQFT, lGarSqft);
//      memcpy(pOutbuf+OFF_GAR_SQFT, acTmp, SIZ_GAR_SQFT);
//   }
//
//   // Garage type
//   if (pChar->Garage1[0] == '1')
//      *(pOutbuf+OFF_PARK_TYPE) = 'I';        // Attached garage
//   else if (pChar->Garage1[0] == '2')
//   {
//      if (lCarpSqft > 0)
//         *(pOutbuf+OFF_PARK_TYPE) = 'C';     // Carport
//      else
//         *(pOutbuf+OFF_PARK_TYPE) = 'L';     // Detached garage
//   }
//
//   // Central Heating-Cooling
//   if (pChar->Central_Heat[0] == 'Y')
//      *(pOutbuf+OFF_HEAT) = 'Z';
//   if (pChar->Central_Air[0] == 'Y')
//      *(pOutbuf+OFF_AIR_COND) = 'C';
//
//   // Beds
//   iBeds = atoin(pChar->Bedrooms, CSIZ_BEDROOMS);
//   if (iBeds > 0)
//   {
//      sprintf(acTmp, "%*u", SIZ_BEDS, iBeds);
//      memcpy(pOutbuf+OFF_BEDS, acTmp, SIZ_BEDS);
//   }
//
//   // Bath
//   if (pChar->Full_Baths[0] > '0')
//   {
//      iFBath = pChar->Full_Baths[0] & 0x0F;
//      *(pOutbuf+OFF_BATH_4Q) = pChar->Full_Baths[0];
//   }
//
//   if (pChar->Baths_34[0] > '0')
//   {
//      // Save new full baths where 3/4 bath is added
//      iFBath += pChar->Baths_34[0] & 0x0F;
//      sprintf(acTmp, "%*u", SIZ_BATH_F, iFBath);
//      memcpy(pOutbuf+OFF_BATH_F, acTmp, SIZ_BATH_F);
//
//      // Save 3/4 baths for Lexis
//      *(pOutbuf+OFF_BATH_3Q) = pChar->Baths_34[0];
//   } else if (iFBath > 0)
//   {
//      sprintf(acTmp, "%*u", SIZ_BATH_F, iFBath);
//      memcpy(pOutbuf+OFF_BATH_F, acTmp, SIZ_BATH_F);
//   }
//
//   // Half bath
//   if (pChar->Half_Baths[0] > '0')
//   {
//      *(pOutbuf+OFF_BATH_2Q) = pChar->Half_Baths[0];
//
//      iFBath = pChar->Half_Baths[0] & 0x0F;
//      sprintf(acTmp, "%*u", SIZ_BATH_H, iFBath);
//      memcpy(pOutbuf+OFF_BATH_H, acTmp, SIZ_BATH_H);
//   }
//
//   // Fireplace
//   if (pChar->Fireplace[0] == 'Y')
//      memcpy(pOutbuf+OFF_FIRE_PL, " 1", 2);
//
//   // Roof
//   if (pChar->Roof_Type[0] > '0')
//   {
//      switch (pChar->Roof_Type[0])
//      {
//         case '1':
//            cTmp1 = 'I';   // 1=TILE
//            break;
//         case '2':
//            cTmp1 = 'B';   // 2=SHAKE
//            break;
//         case '3':
//            cTmp1 = 'H';   // 3=ROCK & GRAVEL/COMPOSITION
//            break;
//         default:
//            cTmp1 = ' ';
//      }
//      *(pOutbuf+OFF_ROOF_MAT) = cTmp1;
//   }
//
//   // Floor - Other values are not known 0 and 1
////   if (pChar->Floor_Type == '2')
////      *(pOutbuf+OFF_FNDN_TYPE) = 'S';
//
//#ifdef _DEBUG
//   //if (!memcmp(pOutbuf, "101274042", 9) )
//   //   lTmp = 0;
//#endif
//   // Pool/Spa
//   if (pChar->Pool[0] == 'Y')
//      *(pOutbuf+OFF_POOL) = 'P';       // Pool
//
//   // Water - N=NONE,A=AVAIL,D=DEVLOPD,U=UNDRGRD
//   if (pChar->Water[0] > '0')
//   {
//      switch (pChar->Water[0])
//      {
//         case 'A':
//         case 'D':
//         case 'Y':
//            cTmp1 = 'A';   // AVAIL
//            break;
//         case 'N':
//            cTmp1 = 'N';   // NON AVAIL
//            break;
//         default:
//            cTmp1 = ' ';
//      }
//      *(pOutbuf+OFF_WATER) = cTmp1;
//   }
//
//   // Sewer
//   if (pChar->Sewer[0] > '0')
//   {
//      switch (pChar->Sewer[0])
//      {
//         case 'A':
//         case 'D':
//            cTmp1 = 'A';   // AVAIL
//            break;
//         case 'N':
//            cTmp1 = 'X';   // NON AVAIL
//            break;
//         case 'Y':
//            cTmp1 = 'Y';   // YES
//            break;
//         default:
//            cTmp1 = ' ';
//      }
//      *(pOutbuf+OFF_SEWER) = cTmp1;
//   }
//
//   // View
//   if (pChar->Water_Front[0] > '0')
//   {
//      switch (pChar->Water_Front[0])
//      {
//         case '1':
//            cTmp1 = 'W';   // Water front
//            break;
//         case '2':
//            cTmp1 = 'H';   // Fairway
//            break;
//         default:
//            cTmp1 = ' ';
//      }
//      *(pOutbuf+OFF_VIEW) = cTmp1;
//   }
//
//   // Zoning
//   // Usecode
//   // Basement Sqft - not use
//   // Units - avail in roll file
//   // Buildings - not avail
//
//   // Others
//
//   lCharMatch++;
//
//   // Get next Char rec
//   iRet = fread(acRec, 1, iCharLen, fdChar);
//
//   return 0;
//}

/******************************* Riv_MergeChar *******************************
 *
 * Merge Riv_Char.dat in STDCHAR format
 *
 *****************************************************************************/

int Riv_MergeStdChar(char *pOutbuf)
{
   static   char acRec[2048], *pRec=NULL;
   char     acTmp[256];
   long     lBldgSqft, lGarSqft, lTmp;
   int      iLoop, iBeds, iFBath, iHBath;
   STDCHAR *pChar;

   // Get first Char rec for first call
   if (!pRec)
      pRec = fgets(acRec, 2048, fdChar);

   pChar = (STDCHAR *)pRec;

   do
   {
      if (!pRec)
      {
         fclose(fdChar);
         fdChar = NULL;
         return 1;      // EOF
      }

      // Compare APN
      iLoop = memcmp(pOutbuf, pRec, iApnLen);
      if (iLoop > 0)
      {
         if (bDebug)
            LogMsg0("Skip Char rec  %.*s", iApnLen, pRec);
         pRec = fgets(acRec, 2048, fdChar);
         lCharSkip++;
      }
   } while (iLoop > 0);

   // If not match, return
   if (iLoop)
      return 1;

   // Yrblt
   lTmp = atoin(pChar->YrBlt, SIZ_CHAR_YRBLT);
   if (lTmp > 1600 && lTmp <= lToyear)
      memcpy(pOutbuf+OFF_YR_BLT, pChar->YrBlt, SIZ_YR_BLT);

   // Eff Yrblt
   lTmp = atoin(pChar->YrEff, SIZ_CHAR_YRBLT);
   if (lTmp > 1600 && lTmp <= lToyear)
      memcpy(pOutbuf+OFF_YR_EFF, pChar->YrEff, SIZ_YR_BLT);

   // BldgSqft
   lBldgSqft = atoin(pChar->BldgSqft, SIZ_CHAR_SQFT);
   if (lBldgSqft > 10)
   {
      sprintf(acTmp, "%*u", SIZ_BLDG_SF, lBldgSqft);
      memcpy(pOutbuf+OFF_BLDG_SF, acTmp, SIZ_BLDG_SF);
   }

   // Do not merge 6/28/2022 - spn
   //// First floor sqft
   //lTmp = atoin(pChar->Sqft_1stFl, SIZ_CHAR_SQFT);
   //if (lTmp > 10)
   //{
   //   sprintf(acTmp, "%*u", SIZ_FLOOR1_SF, lTmp);
   //   memcpy(pOutbuf+OFF_FLOOR1_SF, acTmp, SIZ_FLOOR1_SF);
   //}
   //// Second floor sqft
   //lTmp = atoin(pChar->Sqft_2ndFl, SIZ_CHAR_SQFT);
   //if (lTmp > 10)
   //{
   //   sprintf(acTmp, "%*u", SIZ_FLOOR2_SF, lTmp);
   //   memcpy(pOutbuf+OFF_FLOOR2_SF, acTmp, SIZ_FLOOR2_SF);
   //}
   //// Actual area sqft
   //lTmp = atoin(pChar->Sqft_Above2nd, SIZ_CHAR_SQFT);
   //if (lTmp > 10)
   //{
   //   sprintf(acTmp, "%*u", SIZ_FLOOR3_SF, lTmp);
   //   memcpy(pOutbuf+OFF_FLOOR3_SF, acTmp, SIZ_FLOOR3_SF);
   //}

   // Garage Sqft (may be carport or detached garage)
   lGarSqft = atoin(pChar->GarSqft, SIZ_CHAR_SQFT);
   if (lGarSqft > 10)
   {
      sprintf(acTmp, "%*u", SIZ_GAR_SQFT, lGarSqft);
      memcpy(pOutbuf+OFF_GAR_SQFT, acTmp, SIZ_GAR_SQFT);
      *(pOutbuf+OFF_PARK_TYPE) = pChar->ParkType[0];  
   }

   // Pools
   *(pOutbuf+OFF_POOL) = pChar->Pool[0];

   // Beds
   iBeds = atoin(pChar->Beds, SIZ_CHAR_BEDS);
   if (iBeds > 0)
   {
      sprintf(acTmp, "%*u", SIZ_BEDS, iBeds);
      memcpy(pOutbuf+OFF_BEDS, acTmp, SIZ_BEDS);
   }

#ifdef _DEBUG
   //if (!memcmp(pChar->Apn, "009600002", 9) )
   //   lTmp = 0;
#endif

   // Bath
   iFBath = atoin(pChar->FBaths, SIZ_CHAR_BATHS);
   if (iFBath > 0)
   {
      sprintf(acTmp, "%*u", SIZ_BATH_F, iFBath);
      memcpy(pOutbuf+OFF_BATH_F, acTmp, SIZ_BATH_F);
      
      *(pOutbuf+OFF_BATH_4Q) = pChar->Bath_4Q[0];
   }

   // Half bath
   iHBath = atoin(pChar->HBaths, SIZ_CHAR_BATHS);
   if (iHBath > 0)
   {
      sprintf(acTmp, "%*u", SIZ_BATH_H, iHBath);
      memcpy(pOutbuf+OFF_BATH_H, acTmp, SIZ_BATH_H);

      *(pOutbuf+OFF_BATH_2Q) = pChar->Bath_2Q[0];
   }

   // 3/4 bath
   *(pOutbuf+OFF_BATH_3Q) = pChar->Bath_3Q[0];

   // Stories
   memcpy(pOutbuf+OFF_STORIES, pChar->Stories, SIZ_STORIES);

   // Central Heating-Cooling
   *(pOutbuf+OFF_HEAT) = pChar->Heating[0];
   *(pOutbuf+OFF_AIR_COND) = pChar->Cooling[0];

   // Fireplace
   if (pChar->Fireplace[0] == 'Y')
      memcpy(pOutbuf+OFF_FIRE_PL, " 1", 2);

   // Roof
   *(pOutbuf+OFF_ROOF_MAT) = pChar->RoofMat[0];

   // Water
   *(pOutbuf+OFF_WATER) = pChar->HasWater;

   // Sewer
   *(pOutbuf+OFF_SEWER) = pChar->HasSewer;

   // View
   *(pOutbuf+OFF_VIEW) = pChar->View[0];

   // Zoning
   //if (pChar->Zoning[0] > ' ')
   //   memcpy(pOutbuf+OFF_ZONE, pChar->Zoning, SIZ_ZONE);

   // Quality Class
   *(pOutbuf+OFF_BLDG_CLASS) = pChar->BldgClass;
   *(pOutbuf+OFF_BLDG_QUAL) = pChar->BldgQual;
   memcpy(pOutbuf+OFF_QUALITYCLASS, pChar->QualityClass, SIZ_CHAR_QCLS);

   // Acres
   lTmp = atoin(pChar->LotSqft, SIZ_CHAR_SQFT);
   if (lTmp > 0)
   {
      // Lot Sqft
      sprintf(acTmp, "%*u", SIZ_LOT_SQFT, lTmp);
      memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);

      // Format Acres
      lTmp = atoin(pChar->LotAcre, SIZ_CHAR_SQFT);
      sprintf(acTmp, "%*u", SIZ_LOT_ACRES, lTmp);
      memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);
   }

   // Number of units
   lTmp = atoin(pChar->Units, RSIZ_NO_UNITS);
   if (lTmp > 1)
   {
      sprintf(acTmp, "%*u", SIZ_UNITS, lTmp);
      memcpy(pOutbuf+OFF_UNITS, acTmp, SIZ_UNITS);
   }

   lCharMatch++;

   // Get next Char rec
   pRec = fgets(acRec, 2048, fdChar);

   return 0;
}

/********************************* Riv_MergeSaleChar **************************
 *
 * Merge extract chars from sale file to R01 record. This is to supplement MergeChar()
 *
 ******************************************************************************/

//int Riv_MergeSaleChar(char *pOutbuf)
//{
//   static char    acRec[2048], *pRec=NULL;
//   int            lTmp, iLoop;
//   STDCHAR        *pChar;
//
//   // Get first Char rec for first call
//   if (!pRec)
//   {
//      pRec = fgets(acRec, 1024, fdCChr);
//   }
//
//   pChar = (STDCHAR *)acRec;
//   do
//   {
//      // Compare Apn
//      iLoop = memcmp(pOutbuf, pChar->Apn, iApnLen);
//      if (iLoop > 0)
//      {
//         if (bDebug)
//            LogMsg0("Skip Char rec  %.*s", iApnLen, pRec);
//
//         pRec = fgets(acRec, 1024, fdCChr);
//         if (!pRec)
//         {
//            fclose(fdCChr);
//            fdCChr = NULL;
//            return -1;      // EOF
//         }
//      }
//   } while (iLoop > 0);
//
//   // If not match, return
//   if (iLoop)
//      return 1;
//
//   // Zoning
//   if (pChar->Zoning[0] > ' ' && *(pOutbuf+OFF_ZONE) == ' ')
//      memcpy(pOutbuf+OFF_ZONE, pChar->Zoning, SSIZ_ZONE_CODE);
//
//   // BldgClass
//   if (pChar->BldgClass > ' ' && *(pOutbuf+OFF_BLDG_CLASS) == ' ')
//   {
//      *(pOutbuf+OFF_BLDG_CLASS) = pChar->BldgClass;
//      *(pOutbuf+OFF_BLDG_QUAL)  = pChar->BldgQual;
//   }
//
//   // BldgSqft
//   lTmp = atoin(pChar->BldgSqft, sizeof(pChar->BldgSqft));
//   if (lTmp > 0 && isBlank(pOutbuf+OFF_BLDG_SF, SIZ_BLDG_SF))
//      memcpy(pOutbuf+OFF_BLDG_SF, pChar->BldgSqft, SIZ_BLDG_SF);
//
//   // Carport/Garage (G or C)
//   if (*(pOutbuf+OFF_PARK_TYPE) == ' ' && pChar->ParkType[0] > ' ')
//      *(pOutbuf+OFF_PARK_TYPE) = pChar->ParkType[0];
//
//   // Pool 
//   if (*(pOutbuf+OFF_POOL) == ' ' && pChar->Pool[0] > ' ')
//      *(pOutbuf+OFF_POOL) = pChar->Pool[0];
//
//   // Bed
//   lTmp = atoin(pChar->Beds, sizeof(pChar->Beds));
//   if (lTmp > 0  && isBlank(pOutbuf+OFF_BEDS, SIZ_BEDS))
//      memcpy(pOutbuf+OFF_BEDS, pChar->Beds, SIZ_BEDS);
//
//   // Bath
//   if (*(pOutbuf+OFF_BATH_F) == ' ' && *(pOutbuf+OFF_BATH_F+1) == ' ')
//      memcpy(pOutbuf+OFF_BATH_F, pChar->FBaths, SIZ_BATH_F);
//
//   // Get next Char rec
//   pRec = fgets(acRec, 1024, fdCChr);
//   lMatchCChr++;
//
//   return 0;
//}

/***************************** Riv_MergeSaleRec ******************************
 *
 * Merge new sale data into current sale.  Move other sales accordingly.
 * Only update seller if that is the sale from assessor.
 * This is the modified version of MergeSale() which will not update sale
 * without SaleAmt.  Only transfer is updated.
 *
 *****************************************************************************/

//int Riv_MergeSaleRec(char *pOutbuf, char *pSaleRec, bool bNewRec)
//{
//   long  lCurSaleDt, lLstSaleDt, lPrice, lLastAmt, lDocNum, lTmp;
//   char  acDocNum[32], acDate[12], acTmp[128];
//   RIV_SALE *pRec = (RIV_SALE *)pSaleRec;
//
//   lCurSaleDt = JulianToGregorian(pRec->Conveyance_Date, acDate);
//   lLstSaleDt = atoin(pOutbuf+OFF_SALE1_DT, SIZ_SALE1_DT);
//
//   if (lCurSaleDt < lLstSaleDt)
//      return -1;
//
//   lPrice = atoin(pRec->Indicated_Sale_Price, SSIZ_INDICATED_SALE_PRICE);
//   if (lCurSaleDt == lLstSaleDt)
//   {
//      lLastAmt = atoin(pOutbuf+OFF_SALE1_AMT, SIZ_SALE1_AMT);
//      if (!lPrice && lLastAmt > 0)
//         return 0;
//   } else if (lPrice > 0 && lLstSaleDt > 0)
//   {
//      // Move sale2 to sale3
//      memcpy(pOutbuf+OFF_SALE3_DOC, pOutbuf+OFF_SALE2_DOC, SIZ_SALE3_DOC);
//      memcpy(pOutbuf+OFF_SALE3_DT, pOutbuf+OFF_SALE2_DT, SIZ_SALE3_DT);
//      memcpy(pOutbuf+OFF_SALE3_DOCTYPE, pOutbuf+OFF_SALE2_DOCTYPE, SIZ_SALE3_DOCTYPE);
//      memcpy(pOutbuf+OFF_SALE3_AMT, pOutbuf+OFF_SALE2_AMT, SIZ_SALE3_AMT);
//      memcpy(pOutbuf+OFF_SALE3_CODE, pOutbuf+OFF_SALE2_CODE, SIZ_SALE3_CODE);
//      *(pOutbuf+OFF_AR_CODE3) = *(pOutbuf+OFF_AR_CODE2);
//
//      // Move sale1 to sale2
//      memcpy(pOutbuf+OFF_SALE2_DOC, pOutbuf+OFF_SALE1_DOC, SIZ_SALE3_DOC);
//      memcpy(pOutbuf+OFF_SALE2_DT, pOutbuf+OFF_SALE1_DT, SIZ_SALE3_DT);
//      memcpy(pOutbuf+OFF_SALE2_DOCTYPE, pOutbuf+OFF_SALE1_DOCTYPE, SIZ_SALE3_DOCTYPE);
//      memcpy(pOutbuf+OFF_SALE2_AMT, pOutbuf+OFF_SALE1_AMT, SIZ_SALE3_AMT);
//      memcpy(pOutbuf+OFF_SALE2_CODE, pOutbuf+OFF_SALE1_CODE, SIZ_SALE3_CODE);
//      *(pOutbuf+OFF_AR_CODE2) = *(pOutbuf+OFF_AR_CODE1);
//
//      memset(pOutbuf+OFF_SALE1_AMT, ' ', SIZ_SALE3_AMT);
//      memset(pOutbuf+OFF_SALE1_DT, ' ', SIZ_SALE1_DT+SIZ_SALE1_DOC);
//   }
//
//   // Update current sale
//   lDocNum = atoin(pRec->Conveyance_Sequence_NO, SSIZ_CONVEYANCE_SEQUENCE_NO);
//   lTmp = sprintf(acDocNum, "%.7d     ", lDocNum);
//   if (lPrice > 0)
//   {
//      memcpy(pOutbuf+OFF_SALE1_DOC, acDocNum, lTmp);
//      memcpy(pOutbuf+OFF_SALE1_DT, acDate, SIZ_SALE3_DT);
//
//      // Round off to 100
//      sprintf(acTmp, "%*u00", SIZ_SALE1_AMT-2, lPrice/100);
//      memcpy(pOutbuf+OFF_SALE1_AMT, acTmp, SIZ_SALE1_AMT);
//
//      // Seller
//      //if (bSaleFlag && pSaleRec->Seller[0] > ' ')
//      //   memcpy(pOutbuf+OFF_SELLER, pSaleRec->Seller, SIZ_SELLER);
//   }
//
//   // Update transfers
//   lLstSaleDt = atoin(pOutbuf+OFF_TRANSFER_DT, SIZ_TRANSFER_DT);
//   if (lCurSaleDt >= lLstSaleDt)
//   {
//      memcpy(pOutbuf+OFF_TRANSFER_DOC, acDocNum, SIZ_TRANSFER_DOC);
//      memcpy(pOutbuf+OFF_TRANSFER_DT, acDate, SIZ_TRANSFER_DT);
//   }
//
//   *(pOutbuf+OFF_AR_CODE1) = 'A';
//
//   // Update lien data if new record
//   if (bNewRec && (*(pOutbuf+OFF_GROSS+7) == ' '))
//   {
//      long lLand, lImpr, lOtherImpr;
//
//      lLand = atoin(pRec->Land_Val, SSIZ_LAND_VAL);
//      if (lLand > 0)
//      {
//         sprintf(acTmp, "%*u", SIZ_LAND, lLand);
//         memcpy(pOutbuf+OFF_LAND, acTmp, SIZ_LAND);
//      }
//      lImpr = atoin(pRec->Struc_Value, SSIZ_LAND_VAL);
//      if (lImpr > 0)
//      {
//         sprintf(acTmp, "%*u", SIZ_LAND, lImpr);
//         memcpy(pOutbuf+OFF_IMPR, acTmp, SIZ_LAND);
//
//         sprintf(acTmp, "%*u", SIZ_RATIO, lImpr*100/(lLand+lImpr));
//         memcpy(pOutbuf+OFF_RATIO, acTmp, SIZ_RATIO);
//      }
//      lOtherImpr = atoin(pRec->Trees_And_Vines_Value, SSIZ_LAND_VAL);
//      if (lOtherImpr > 0)
//      {
//         sprintf(acTmp, "%*u", SIZ_LAND, lOtherImpr);
//         memcpy(pOutbuf+OFF_OTHER, acTmp, SIZ_LAND);
//      }
//
//      lTmp = lLand+lImpr+lOtherImpr;
//      if (lTmp > 0)
//      {
//         sprintf(acTmp, "%*u", SIZ_GROSS, lTmp);
//         memcpy(pOutbuf+OFF_GROSS, acTmp, SIZ_GROSS);
//      }
//   }
//
//   // Zoning
//   if (pRec->Zoning[0] > ' ' && *(pOutbuf+OFF_ZONE) == ' ')
//      memcpy(pOutbuf+OFF_ZONE, pRec->Zoning, SSIZ_ZONE_CODE);
//
//   return 1;
//}

/***************************** Riv_MergeSaleChar *****************************
 *
 * Merge chars from sale rec where data available.
 *
 *****************************************************************************/

//void Riv_MergeSaleChar(char *pOutbuf, char *pSaleRec, bool bNewRec)
//{
//   long  lTmp, iTmp;
//   char  acTmp[128], acCode[128];
//   RIV_SALE *pRec = (RIV_SALE *)pSaleRec;
//
//   // Update lien data if new record
//   if (bNewRec && (*(pOutbuf+OFF_GROSS+7) == ' '))
//   {
//      long lLand, lImpr, lOtherImpr;
//
//      lLand = atoin(pRec->Land_Val, SSIZ_LAND_VAL);
//      if (lLand > 0)
//      {
//         sprintf(acTmp, "%*u", SIZ_LAND, lLand);
//         memcpy(pOutbuf+OFF_LAND, acTmp, SIZ_LAND);
//      }
//      lImpr = atoin(pRec->Struc_Value, SSIZ_LAND_VAL);
//      if (lImpr > 0)
//      {
//         sprintf(acTmp, "%*u", SIZ_LAND, lImpr);
//         memcpy(pOutbuf+OFF_IMPR, acTmp, SIZ_LAND);
//
//         sprintf(acTmp, "%*u", SIZ_RATIO, lImpr*100/(lLand+lImpr));
//         memcpy(pOutbuf+OFF_RATIO, acTmp, SIZ_RATIO);
//      }
//      lOtherImpr = atoin(pRec->Trees_And_Vines_Value, SSIZ_LAND_VAL);
//      if (lOtherImpr > 0)
//      {
//         sprintf(acTmp, "%*u", SIZ_LAND, lOtherImpr);
//         memcpy(pOutbuf+OFF_OTHER, acTmp, SIZ_LAND);
//      }
//
//      lTmp = lLand+lImpr+lOtherImpr;
//      if (lTmp > 0)
//      {
//         sprintf(acTmp, "%*u", SIZ_GROSS, lTmp);
//         memcpy(pOutbuf+OFF_GROSS, acTmp, SIZ_GROSS);
//      }
//   }
//
//   // Use code
//
//   // Zoning
//   if (pRec->Zoning[0] > ' ' && *(pOutbuf+OFF_ZONE) == ' ')
//      memcpy(pOutbuf+OFF_ZONE, pRec->Zoning, SSIZ_ZONE_CODE);
//
//   // BldgClass
//   if (pRec->Construction_Type[0] > ' ')
//   {
//      *(pOutbuf+OFF_BLDG_CLASS) = pRec->Construction_Type[0];
//
//      // Quality
//      lTmp = atoin(pRec->Structure_Qual_Code, SSIZ_STRUCTURE_QUAL_CODE);
//      if (lTmp > 0)
//      {
//         sprintf(acTmp, "%.1f", (double)(lTmp/10.0));
//         iTmp = Value2Code(acTmp, acCode, NULL);
//         if (acCode[0] > ' ')
//            *(pOutbuf+OFF_BLDG_QUAL) = acCode[0];
//      }
//   }
//
//   // BldgSqft
//   lTmp = atoin(pRec->Area, SSIZ_AREA);
//   if (lTmp > 0)
//   {
//      sprintf(acTmp, "%*u", SIZ_BLDG_SF, lTmp);
//      memcpy(pOutbuf+OFF_BLDG_SF, acTmp, SIZ_BLDG_SF);
//   }
//
//   // Eff. year = lAssdYear - EffAge
//   /*
//   lTmp = atoin(pRec->Effective_Age, SSIZ_EFFECTIVE_AGE);
//   if (lTmp > 0)
//   {
//      lTmp = myCounty.iYearAssd - lTmp;
//      iTmp = sprintf(acTmp, "%d", lTmp);
//      memcpy(pOutbuf+OFF_YR_EFF, acTmp, iTmp);
//   }
//   */
//
//   // Carport/Garage (G or C)
//   if (*(pOutbuf+OFF_PARK_TYPE) == ' ')
//   {
//      if (pRec->Carport_Garage_IndicaTor[0] == 'G')
//         *(pOutbuf+OFF_PARK_TYPE) = 'Z';
//      else if (pRec->Carport_Garage_IndicaTor[0] == 'C')
//         *(pOutbuf+OFF_PARK_TYPE) = 'C';
//   }
//
//   // Pool (Y/N)
//   if (pRec->Pool_Indicator[0] == 'Y')
//      *(pOutbuf+OFF_POOL) = 'P';
//
//   // Bed
//   lTmp = atoin(pRec->Bedrooms, SSIZ_BEDROOMS);
//   if (lTmp > 0)
//   {
//      sprintf(acTmp, "%*u", SIZ_BEDS, lTmp);
//      memcpy(pOutbuf+OFF_BEDS, acTmp, SIZ_BEDS);
//   }
//
//   // Bath
//   if (*(pOutbuf+OFF_BATH_F) == ' ' && *(pOutbuf+OFF_BATH_F+1) == ' ')
//   {
//      lTmp = atoin(pRec->Baths, SSIZ_BATHS);
//      if (lTmp > 0)
//      {
//         sprintf(acTmp, "%*u", SIZ_BATH_F, lTmp);
//         memcpy(pOutbuf+OFF_BATH_F, acTmp, SIZ_BATH_F);
//      }
//      memset(pOutbuf+OFF_BATH_H, ' ', SIZ_BATH_H);
//   }
//}

/********************************* Riv_MergeSale *****************************
 *
 *
 *
 *****************************************************************************/

//int Riv_MergeSale(char *pOutbuf, bool bNewRec=false)
//{
//   static   char  acRec[1024], *pRec=NULL;
//   RIV_SALE *pSaleRec = (RIV_SALE *)&acRec[0];
//   int      iRet, iLoop;
//   //int      iReadLen;
//
//#ifdef _DEBUG
//   //if (!memcmp(pOutbuf, "009000130", 9) )
//   //   iRet = 0;
//#endif
//
//   // Get first Sale rec for first call
//   if (!pRec)
//   {
//      //iReadLen = fread(acRec, 1, iSaleLen, fdSale);
//      //pRec = acRec;
//      pRec = fgets(acRec, 1024, fdSale);
//   }
//
//   do
//   {
//      // Compare Apn
//      iLoop = memcmp(pOutbuf, &acRec[SOFF_ASSESSMENT_NO], SSIZ_ASSESSMENT_NO);
//      if (iLoop > 0)
//      {
//         if (bDebug)
//            LogMsg0("Skip Sale rec  %.*s", myCounty.iApnLen, &acRec[SOFF_ASSESSMENT_NO]);
//         //iReadLen = fread(acRec, 1, iSaleLen, fdSale);
//         //if (iReadLen != iSaleLen)
//         pRec = fgets(acRec, 1024, fdSale);
//         if (!pRec)
//         {
//            fclose(fdSale);
//            fdSale = NULL;
//            return -1;      // EOF
//         }
//
//         lSaleSkip++;
//      }
//   } while (iLoop > 0);
//
//   // If not match, return
//   if (iLoop)
//      return 1;
//
//   do
//   {
//#ifdef _DEBUG
//      //if (!memcmp(pOutbuf, "009000144", 9))
//      //   iRet = 0;
//#endif
//      // Merge Char
//      Riv_MergeSaleChar(pOutbuf, acRec, bNewRec);
//
//      // Return 1 if new sale is insert
//      iRet = Riv_MergeSaleRec(pOutbuf, acRec, bNewRec);
//
//      // Get next sale record
//      //iReadLen = fread(acRec, 1, iSaleLen, fdSale);
//      //if (iReadLen != iSaleLen)
//      pRec = fgets(acRec, 1024, fdSale);
//      if (!pRec)
//      {
//         fclose(fdSale);
//         fdSale = NULL;
//         break;
//      }
//
//      // Update flag
//      *(pOutbuf+OFF_AR_CODE1) = 'A';           // Assessor-Recorder code
//   } while (!memcmp(pOutbuf, &acRec[SOFF_ASSESSMENT_NO], myCounty.iApnLen));
//
//   iRet = atoin(pOutbuf+OFF_TRANSFER_DT, 8);
//   if (iRet > lLastRecDate && iRet < lToday)
//      lLastRecDate = iRet;
//
//   lSaleMatch++;
//
//   return 0;
//}

/********************************* Riv_MergeRoll *****************************
 *
 * Unit# may fails over into MCity_State.
 *
 * Return 0 if successful, >0 if error
 *
 *****************************************************************************/

//void Riv_MergeRoll(char *pOutbuf, char *pRollRec, int iLen, int iFlag)
//{
//   char     acTmp[256], acTmp1[256], *pTmp;
//   long     lTmp, lSqFtLand;
//   int      iRet, iTmp;
//   double   dAcreAge;
//
//   RIV_ROLL *pRec = (RIV_ROLL *)pRollRec;
//
//#ifdef _DEBUG
//   //if (!memcmp(pOutbuf, "129020040", 9) )
//   //   lTmp = 0;
//#endif
//
//   // Check known bad chars
//   if (pRec->MCity_State[0] < '0' || pRec->MCity_State[0] > 'Z')
//      pRec->MCity_State[0] = ' ';
//   if (pRec->MAddress[0] < '0' || pRec->MAddress[0] > 'Z')
//      pRec->MAddress[0] = ' ';
//   pRec->Filler1[0] = ' ';
//
//   // Check for bad char
//   if (iTmp = replUnPrtChar(pRollRec, ' ', iLen-1))
//      LogMsg0("*** Bad character at %d in RIV_ROLL where APN=%.*s", iTmp+1, RSIZ_ASSMNT_NO, pRec->Assmnt_No);
//
//   if (iFlag & CREATE_R01)
//   {
//      memcpy(pOutbuf, pRec->Assmnt_No, RSIZ_ASSMNT_NO);
//      memcpy(pOutbuf+OFF_CO_NUM, "33RIV", 5);
//
//      // Format APN
//      iRet = formatApn(pRec->Assmnt_No, acTmp, &myCounty);
//      memcpy(pOutbuf+OFF_APN_D, acTmp, iRet);
//
//      // Alternate APN
//      memcpy(pOutbuf+OFF_ALT_APN, pRec->Parcel_No, RSIZ_PARCEL_NO);
//
//      // Create MapLink
//      iRet = formatMapLink(pRec->Parcel_No, acTmp, &myCounty);
//      memcpy(pOutbuf+OFF_MAPLINK, acTmp, iRet);
//
//      // Create index map link
//      if (getIndexPage(acTmp, acTmp1, &myCounty))
//         memcpy(pOutbuf+OFF_IMAPLINK, acTmp1, iRet);
//
//      // Parcel status
//      *(pOutbuf+OFF_STATUS) = 'A';
//      *(pOutbuf+OFF_FULL_EXEMPT) = ' ';
//
//      // Year assessed
//      memcpy(pOutbuf+OFF_YR_ASSD, myCounty.acYearAssd, 4);
//   }
//
//#ifdef _DEBUG
//   //if (!memcmp(pOutbuf, "010638448", 9) )
//   //   lTmp = 0;
//#endif
//
//   // TRA
//   lTmp = atoin(pRec->Tra, RSIZ_TRA);
//   if (lTmp > 0)
//      memcpy(pOutbuf+OFF_TRA, pRec->Tra, RSIZ_TRA);
//
//   // Tax code
//   if (pRec->Tax_Code[0] > '0')
//   {
//      memcpy(pOutbuf+OFF_TAX_CODE, pRec->Tax_Code, SIZ_TAX_CODE);
//      // Set full exemption flag
//      if (pRec->Tax_Code[0] < '4')
//         *(pOutbuf+OFF_FULL_EXEMPT) = 'Y';
//   }
//
//   // UseCode
//   iTmp = 0;
//   if (pRec->Re_Use_Code[0] > ' ')
//   {
//      memcpy(acTmp, pRec->Re_Use_Code, RSIZ_RE_USE_CODE);
//      iTmp = RSIZ_RE_USE_CODE;
//   } else if (pRec->Bus_Use_Code[0] > ' ')
//   {
//      memcpy(acTmp, pRec->Bus_Use_Code, RSIZ_BUS_USE_CODE);
//      iTmp = RSIZ_BUS_USE_CODE;
//   }
//
//   if (iTmp > 0)
//   {
//      acTmp[iTmp] = 0;
//      memcpy(pOutbuf+OFF_USE_CO, acTmp, iTmp);
//      updateStdUse(pOutbuf+OFF_USE_STD, acTmp, iTmp, pOutbuf);
//   } else
//      memcpy(pOutbuf+OFF_USE_STD, USE_UNASGN, SIZ_USE_STD);
//
//   // Lot acreage
//   dAcreAge = atoin(pRec->Acres, RSIZ_ACRES);
//   if (dAcreAge > 0.0)
//   {
//      sprintf(acTmp, "%*u", SIZ_LOT_ACRES, (long)(dAcreAge*10));
//      memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);
//      lSqFtLand = (long)(dAcreAge * SQFT_FACTOR_100);
//      sprintf(acTmp, "%*u", SIZ_LOT_SQFT, lSqFtLand);
//      memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);
//   }
//
//   // Owner
//   //Riv_MergeCOwner(pOutbuf, pRollRec);
//   Riv_MergeOwner(pOutbuf, pRollRec);     // 20170206 Format owner name seperately to make it similar to LDR format
//   
//   // CareOf
//   if (pRec->Mail_To_Name[0] >= 'A')
//   {
//      memcpy(acTmp, pRec->Mail_To_Name, RSIZ_MAIL_TO_NAME);
//      acTmp[RSIZ_MAIL_TO_NAME] = 0;
//
//      if ((pTmp = strstr(acTmp, "C/O")) || (pTmp = strstr(acTmp, "ATTN")) )
//      {
//         updateCareOf(pOutbuf, pTmp, RSIZ_MAIL_TO_NAME - (pTmp-&acTmp[0]));
//      }
//   }
//
//   //Riv_UpdateMAdr(pOutbuf, pRollRec);
//   Riv_MergeMAdr(pOutbuf, pRec->MAddress, pRec->MCity_State, pRec->MZip);
//
//   // Update situs
//   Riv_UpdateSAdr(pOutbuf, pRollRec);
//
//   // If no mail addr, copy situs over
//   if (!memcmp(pOutbuf+OFF_M_STREET, "     ", 5) )
//   {
//      memcpy(pOutbuf+OFF_M_STRNUM, pOutbuf+OFF_S_STRNUM, SIZ_M_STRNUM);
//      *(pOutbuf+OFF_M_DIR) = *(pOutbuf+OFF_S_DIR);
//      memcpy(pOutbuf+OFF_M_STREET, pOutbuf+OFF_S_STREET, SIZ_M_STREET);
//      if (pRec->SStr_Suffix[0] > ' ')
//         memcpy(pOutbuf+OFF_M_SUFF, pRec->SStr_Suffix, RSIZ_SSTR_SUFFIX);
//
//      if (pRec->SUnit[0] > ' ')
//         memcpy(pOutbuf+OFF_M_UNITNO, pRec->SUnit, RSIZ_SUNIT);
//
//      iTmp = atoin(pOutbuf+OFF_S_CITY, 3);
//      if (iTmp > 0)
//      {
//         pTmp = GetCityName(iTmp);
//         if (pTmp)
//         {
//            iTmp = strlen(pTmp);
//            if (iTmp > SIZ_M_CITY) iTmp = SIZ_M_CITY;
//            memcpy(pOutbuf+OFF_M_CITY, pTmp, iTmp);
//         }
//      }
//      memcpy(pOutbuf+OFF_M_ADDR_D, pOutbuf+OFF_S_ADDR_D, SIZ_M_ADDR_D);
//      memcpy(pOutbuf+OFF_M_ST, "CA", 2);
//
//      memcpy(pOutbuf+OFF_M_ZIP, pRec->SZip, SIZ_M_ZIP);
//      memcpy(pOutbuf+OFF_M_CTY_ST_D, pOutbuf+OFF_S_CTY_ST_D, SIZ_M_CTY_ST_D);
//   }
//
//   // Number of units
//   iTmp = atoin(pRec->No_Units, RSIZ_NO_UNITS);
//   if (iTmp > 1)
//   {
//      sprintf(acTmp, "%*u", SIZ_UNITS, iTmp);
//      memcpy(pOutbuf+OFF_UNITS, acTmp, SIZ_UNITS);
//   }
//}

int Riv_MergeLien(char *pOutbuf, char *pRollRec, int iLen, char *pLegal)
{
   char        acTmp[256], acTmp1[256];
   long        lSqFtLand;
   int         iRet, iTmp, iRecNo, iRecType;
   double      dAcreAge;

   RIV_LIEN11 *pRec11 = (RIV_LIEN11 *)pRollRec;
   RIV_LIEN17 *pRec17 = (RIV_LIEN17 *)pRollRec;
   RIV_LIEN18 *pRec18 = (RIV_LIEN18 *)pRollRec;
   RIV_LIEN19 *pRec19 = (RIV_LIEN19 *)pRollRec;
   RIV_LIEN20 *pRec20 = (RIV_LIEN20 *)pRollRec;
   RIV_LIEN21 *pRec21 = (RIV_LIEN21 *)pRollRec;
   RIV_LIEN23 *pRec23 = (RIV_LIEN23 *)pRollRec;
   RIV_LIEN26 *pRec26 = (RIV_LIEN26 *)pRollRec;
   RIV_LIEN28 *pRec28 = (RIV_LIEN28 *)pRollRec;
   RIV_LIEN30 *pRec30 = (RIV_LIEN30 *)pRollRec;
   RIV_LIEN52 *pRec52 = (RIV_LIEN52 *)pRollRec;
   RIV_LIEN53 *pRec53 = (RIV_LIEN53 *)pRollRec;
   RIV_LIEN67 *pRec67 = (RIV_LIEN67 *)pRollRec;
   RIV_LIEN71 *pRec71 = (RIV_LIEN71 *)pRollRec;
   RIV_LIEN72 *pRec72 = (RIV_LIEN72 *)pRollRec;
   RIV_LIEN73 *pRec73 = (RIV_LIEN73 *)pRollRec;
   RIV_LIEN74 *pRec74 = (RIV_LIEN74 *)pRollRec;
   RIV_LIEN75 *pRec75 = (RIV_LIEN75 *)pRollRec;

   iRecType = (atoin(pRollRec+OFF_LIEN_RECORD_TYPE, SIZ_LIEN_RECORD_TYPE));
   switch(iRecType)
   {
      case 11:
         // Start copying data
         memcpy(pOutbuf, pRec11->Primary_Parcel_No, SIZ_LIEN_PRIMARY_PARCEL_NO);
         memcpy(pOutbuf+OFF_CO_NUM, "33RIV", 5);

         *(pOutbuf+OFF_STATUS) = 'A';

         // Format APN
         iRet = formatApn(pRec11->Primary_Parcel_No, acTmp, &myCounty);
         memcpy(pOutbuf+OFF_APN_D, acTmp, iRet);

         // Create MapLink
         /* Use Reference APN to create map link
         iRet = formatMapLink(pRec11->Primary_Parcel_No, acTmp, &myCounty);
         memcpy(pOutbuf+OFF_MAPLINK, acTmp, iRet);

         // Create index map link
         if (getIndexPage(acTmp, acTmp1, &myCounty))
            memcpy(pOutbuf+OFF_IMAPLINK, acTmp1, iRet);
         */
         // Year assessed
         memcpy(pOutbuf+OFF_YR_ASSD, myCounty.acYearAssd, 4);

         // UseCode
         //if (pRec11->Bus_Use_Code[0] > ' ')
         //   memcpy(pOutbuf+OFF_USE_CO, pRec11->Bus_Use_Code, SIZ_LIEN_BUS_USE_CODE);
         //if (pRec11->Use_Code[0] > ' ')
         //   memcpy(pOutbuf+OFF_USE_CO, pRec11->Use_Code, SIZ_LIEN_USE_CODE);
         iTmp = 0;
         if (pRec11->Use_Code[0] > ' ')
         {
            memcpy(acTmp, pRec11->Use_Code, SIZ_LIEN_USE_CODE);
            iTmp = SIZ_LIEN_USE_CODE;
         } else if (pRec11->Bus_Use_Code[0] > ' ')
         {
            memcpy(acTmp, pRec11->Bus_Use_Code, SIZ_LIEN_BUS_USE_CODE);
            iTmp = SIZ_LIEN_BUS_USE_CODE;
         }

         if (iTmp > 0)
         {
            acTmp[iTmp] = 0;
            memcpy(pOutbuf+OFF_USE_CO, acTmp, iTmp);
            updateStdUse(pOutbuf+OFF_USE_STD, acTmp, iTmp, pOutbuf);
         } else
            memcpy(pOutbuf+OFF_USE_STD, USE_UNASGN, SIZ_USE_STD);

         if (pRec11->Recording_Date[2] > '0')
         {
            memcpy(pOutbuf+OFF_TRANSFER_DT, &pRec11->Recording_Date[2], 4);
            memcpy(pOutbuf+OFF_TRANSFER_DT+4, pRec11->Recording_Date, 2);
            memcpy(pOutbuf+OFF_TRANSFER_DT+6, "00", 2);
         }

         iRecNo = atoin(pRec11->Recorder_No, SIZ_LIEN_RECORDER_NO);
         if (iRecNo > 0)
         {
            iTmp = sprintf(acTmp, "%.7d", iRecNo);
            memcpy(pOutbuf+OFF_TRANSFER_DOC, acTmp, iTmp);
         }
         break;

      case 18:
         if (pRec18->Careof[0] > ' ')
         {
            iTmp = replUChar((unsigned char *)pRec18->Careof, 0xA9, 'E', SIZ_LIEN_CAREOF);
#ifdef _DEBUG
            //if (iTmp > 0)
            //   iTmp = 0;
#endif
            updateCareOf(pOutbuf, pRec18->Careof, SIZ_LIEN_CAREOF);
         }
         break;

      case 19:
      {
         iTmp = replUChar((unsigned char *)pRec19->MStreet, 0x1C, ' ', SIZ_LIEN_MSTREET);
         iTmp = remUChar((unsigned char *)pRec19->MCity_State, 0x8D, SIZ_LIEN_MCITY_STATE);
         Riv_MergeMAdr(pOutbuf, pRec19->MStreet, pRec19->MCity_State, pRec19->Zip_Code);
         break;
      }

      case 20:
         iTmp = replUChar((unsigned char *)pRollRec, 0x1C, ' ', iRollLen);
         Riv_MergeSAdr(pOutbuf, pRollRec);
         break;

      case 23: // Tax code/TRA/
         if ((pRec23->Tra[0] > ' ') && (atoin(pRec23->Tra, SIZ_LIEN_RIVTRA)))
            memcpy(pOutbuf+OFF_TRA, pRec23->Tra, SIZ_LIEN_RIVTRA);
         memcpy(pOutbuf+OFF_ALT_APN, pRec23->Reference_Apn, SIZ_LIEN_REFERENCE_APN);

         // MapLink
         iRet = formatMapLink(pRec23->Reference_Apn, acTmp, &myCounty);
         memcpy(pOutbuf+OFF_MAPLINK, acTmp, iRet);

         // Create index map link
         if (getIndexPage(acTmp, acTmp1, &myCounty))
            memcpy(pOutbuf+OFF_IMAPLINK, acTmp1, iRet);

         // Set full exemption flag
         if (pRec23->Tax_Code[0] > '0' && pRec23->Tax_Code[0] < '4')
            *(pOutbuf+OFF_FULL_EXEMPT) = 'Y';
         memcpy(pOutbuf+OFF_TAX_CODE, pRec23->Tax_Code, SIZ_LIEN_TAX_CODE);
         break;

      case 26: // Tract/Block/Lot & Subdiv
         memcpy(pOutbuf+OFF_BLOCK, pRec26->Block, SIZ_LIEN_BLOCK);
         memcpy(pOutbuf+OFF_LOT, pRec26->Lot, SIZ_LIEN_LOT);
         //memcpy(pOutbuf+OFF_SUBDIV, pRec26->Subdiv, SIZ_SUBDIV);
         break;

      case 28:
         dAcreAge = atoin(pRec28->Acres, SIZ_LIEN_ACRES);
         if (dAcreAge > 0.0)
         {
            sprintf(acTmp, "%*u", SIZ_LOT_ACRES, (long)(dAcreAge*10));
            memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);
            lSqFtLand = (long)(dAcreAge * SQFT_FACTOR_100);
            sprintf(acTmp, "%*u", SIZ_LOT_SQFT, lSqFtLand);
            memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);
         }
         break;

      case 30:
         //if (pRec30->Legal[0] > ' ' && *(pOutbuf+OFF_LEGAL) == ' ')
         if (pRec30->Legal[0] > ' ')
         {
            memcpy(acTmp, pRec30->Legal, SIZ_LIEN_LEGAL);
            acTmp[SIZ_LIEN_LEGAL] = 0;
            if (*pLegal > ' ')
               strcat(pLegal, " ");
            strcat(pLegal, acTmp);
            //memcpy(pOutbuf+OFF_LEGAL, pRec30->Legal, SIZ_LEGAL);
         }
         break;

      case 52:
         iTmp = replUChar((unsigned char *)pRec52->Owner_Name, 0x9B, 0xA2, SIZ_LIEN_OWNER_NAME);
#ifdef _DEBUG
         //if (iTmp > 0)
         //   iTmp = 0;
#endif
         Riv_MergeLOwner(pOutbuf, pRec52);
         break;

      case 53:
         LONGLONG lTotalExe, lOtherImpr, lGross;
         long lBusInv, lBus_PP, lHouse_PP, lRanch_PP, lOther_PP, lFixtr, lTree, lLand, lImpr, lAircraft_PP, lBoat_PP, lTmp;
         iTmp = 0;
         lTotalExe=lOtherImpr=lLand=lImpr = 0;
         lBusInv=lBus_PP=lHouse_PP=lRanch_PP=lOther_PP=lFixtr=lTree=lAircraft_PP=lBoat_PP = 0;

         while (iTmp < 13)
         {
            iRet = atoin(pRec53->ValExe[iTmp].Code, SIZ_LIEN_CODE1);
            if (!iRet)
               break;

            /*
            '01'='LND'   LAND
            '02'='STR'   STRUCTURE
            '03'='FIX'   TRADE FIXTURES
            '04'='BIV'   BUSINESS INVENTORY
            '05'='TVV'   TREES/VINES
            '06'='CPV'   CITRUS PEST VALUE
            '07'='HPP'   HOUSEHOLD PERS. PROP.
            '08'='BPP'   BUSINESS PERS. PROP.
            '09'='RPP'   RANCH PERS. PROP.
            '10'='OPP'   OTHER PERS. PROP.
            '13'='BOT'   BOAT VALUE PERS. PROP.
            '14'='NCA'   NON-COMMERCIAL AIRCRAFT
            '15'='P10'   PENALTY 10%
            '16'='P25'   PENALTY 25%
            '17'='OAP'   OTHER ASSESSED PENALTY
            '21'='HOX'   HOMEOWNERS
            '22'='VET'   VETERANS
            '23'='DVT'   DISABLED VETERAN
            '24'='BVT'   BLIND VETERAN
            '25'='CHX'   CHURCH
            '26'='REL'   RELIGIOUS
            '27'='CEM'   CEMETARY
            '28'='TVX'   TREE/VINE
            '29'='HFA'   HOME FOR AGED
            '30'='WIN'   WINE
            '31'='FPS'   FREE PUBLIC SCHOOL
            '32'='FPL'   FREE PUBLIC LIBRARY
            '33'='FPM'   FREE PUBLIC MUSEUM
            '34'='WCX'   WELFARE - COLLEGE
            '35'='WHX'   WELFARE - HOSPITAL
            '36'='WPS'   WELFARE - PRIVATE/PAROCHIAL SCHOOL
            '37'='WCR'   WELFARE - CHARITY/RELIGIOUS
            '38'='OXX'   OTHER EXEMPTION
            '39'='SSR'   SOLDIER/SAILOR RELIEF ACT
            '40'='BIX'   BUSINESS INVENTORY
            */
#ifdef _DEBUG
            //if (!memcmp(pOutbuf, "000155022", 9) )
            //   lTmp = 0;
#endif
            lTmp = atoin(pRec53->ValExe[iTmp].Amount, SIZ_LIEN_AMOUNT1);
            switch (iRet)
            {
               case 1:
                  lLand = lTmp;
                  break;
               case 2:
                  lImpr = lTmp;
                  break;
               case 21: // HO occupied
                  lTotalExe += lTmp;
                  *(pOutbuf+OFF_HO_FL) = '1';
                  break;
               case 3:
                  lFixtr += lTmp;
                  break;
               case 4:
               case 40:
                  lBusInv += lTmp;
                  break;
               case 5:
               case 6:
                  lTree += lTmp;
                  break;
               case 7:
                  lHouse_PP += lTmp;
                  break;
               case 8:
                  lBus_PP += lTmp;
                  break;
               case 9:
                  lRanch_PP += lTmp;
                  break;
               case 10:
                  lOther_PP += lTmp;
                  break;
               case 13:
                  lBoat_PP += lTmp;
                  break;
               case 14:
                  lAircraft_PP += lTmp;
                  break;
               default:
                  if (iRet > 22 && iRet < 40)
                  {
                     lTotalExe += lTmp;
                  } else if (iRet > 2 && iRet < 15)
                     lOtherImpr += lTmp;
                  else if (iRet < 15 || iRet > 16)   // Penalty
                     LogMsg0("*** Unknown code: %d with value %d", iRet, lTmp);
                  break;
            }

            iTmp++;
         }

         // Personal properties
         lTmp = lBus_PP+lHouse_PP+lRanch_PP+lOther_PP+lAircraft_PP+lBoat_PP;
         if (lTmp > 0)
         {
            sprintf(acTmp, "%u         ", lTmp);
            memcpy(pOutbuf+OFF_PERSPROP, acTmp, SIZ_PERSPROP);
         }

         lOtherImpr += (lTmp+lBusInv+lFixtr+lTree);
         if (lOtherImpr > 0)
         {
            sprintf(acTmp, "%*u", SIZ_LAND, lOtherImpr);
            memcpy(pOutbuf+OFF_OTHER, acTmp, SIZ_LAND);

            if (lFixtr > 0)
            {
               sprintf(acTmp, "%u         ", lFixtr);
               memcpy(pOutbuf+OFF_FIXTR, acTmp, SIZ_FIXTR);
            }

            if (lBusInv > 0)
            {
               sprintf(acTmp, "%u         ", lBusInv);
               memcpy(pOutbuf+OFF_BUSINV, acTmp, SIZ_BUSINV);
            }

            if (lTree > 0)
            {
               sprintf(acTmp, "%u         ", lTree);
               memcpy(pOutbuf+OFF_TREEVINES, acTmp, SIZ_TREEVINES);
            }
         }

         if (lLand > 0)
         {
            sprintf(acTmp, "%*u", SIZ_LAND, lLand);
            memcpy(pOutbuf+OFF_LAND, acTmp, SIZ_LAND);
         }
         if (lImpr > 0)
         {
            sprintf(acTmp, "%*u", SIZ_LAND, lImpr);
            memcpy(pOutbuf+OFF_IMPR, acTmp, SIZ_LAND);

            sprintf(acTmp, "%*u", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
            memcpy(pOutbuf+OFF_RATIO, acTmp, SIZ_RATIO);
         }
         if (lTotalExe > 0)
         {
            sprintf(acTmp, "%*u", SIZ_EXE_TOTAL, lTotalExe);
            memcpy(pOutbuf+OFF_EXE_TOTAL, acTmp, SIZ_LAND);
         }

         lGross = lLand+lImpr+lOtherImpr;
         if (lGross > 0)
         {
            sprintf(acTmp, "%*u", SIZ_GROSS, lGross);
            memcpy(pOutbuf+OFF_GROSS, acTmp, SIZ_GROSS);
         }
         break;

      case 67:
         if (pRec67->Sold_Year[0] > ' ')
            memcpy(pOutbuf+OFF_DEL_YR, pRec67->Sold_Year, SIZ_DEL_YR);
         break;

      case 17:
      case 21:
      case 71:
      case 72:
      case 73:
      case 74:
      case 75:
      case 80:
         break;

      default:
         if (iRecType)
            LogMsg("*** Unknown type: %.80s", pRollRec);
         break;
   }

   return iRecType;
}

/********************************* Riv_Load_Roll ******************************
 *
 * Load updated role file RIVKEYIN
 *
 ******************************************************************************/

//int Riv_Load_Roll(int iSkip)
//{
//   char     acRec[MAX_RECSIZE], acBuf[MAX_RECSIZE], acRollRec[MAX_RECSIZE];
//   char     acRawFile[_MAX_PATH], acOutFile[_MAX_PATH];
//
//   HANDLE   fhIn, fhOut;
//   RIV_ROLL *pRec = (RIV_ROLL *)&acRollRec[0];
//
//   int      iRet, Result, iRollUpd=0, iNewRec=0, iRetiredRec=0, lCnt=0;
//   DWORD    nBytesRead;
//   DWORD    nBytesWritten;
//   BOOL     bRet, bEof;
//
//   sprintf(acRawFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "S01");
//   sprintf(acOutFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "R01");
//
//   // Rename files for processing
//   if (_access(acRawFile, 0))
//   {
//      if (!_access(acOutFile, 0))
//         rename(acOutFile, acRawFile);
//      else
//      {
//         LogMsg("***** Missing input file %s.  Please recheck!", acRawFile);
//         return -1;
//      }
//   }
//
//   // Open Roll file
//   LogMsg("Open Roll file %s", acRollFile);
//   fdRoll = fopen(acRollFile, "r");
//   if (fdRoll == NULL)
//   {
//      LogMsg("***** Error opening roll file: %s\n", acRollFile);
//      return -2;
//   }
//
//   // Open Char file
//   LogMsg("Open Char file %s", acCChrFile);
//   fdChar = fopen(acCChrFile, "r");
//   if (fdChar == NULL)
//   {
//      LogMsg("***** Error opening Char file: %s\n", acCChrFile);
//      return -2;
//   }
//
//   // Open Input file
//   LogMsg("Open input file %s", acRawFile);
//   fhIn = CreateFile(acRawFile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
//          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);
//
//   if (fhIn == INVALID_HANDLE_VALUE)
//   {
//      LogMsg("***** Error opening input file: %s\n", acRawFile);
//      return -3;
//   }
//
//   // Open Output file
//   LogMsg("Open output file %s", acOutFile);
//   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
//          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);
//
//   if (fhOut == INVALID_HANDLE_VALUE)
//   {
//      LogMsg("***** Error opening output file: %s\n", acOutFile);
//      return -4;
//   }
//
//   // Copy skip record
//   memset(acBuf, ' ', iRecLen);
//   while (iSkip-- > 0)
//   {
//      ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);
//      // Ignore record start with 99999999
//      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
//   }
//
//   // Skip unsecured parcels
//   do
//   {
//      iRet = fread(acRollRec, 1, iRollLen, fdRoll);
//      Result = atoin(pRec->Assmnt_No, 3);
//   } while (iRet == iRollLen && Result < 2);
//
//   iNoMatch=iBadCity=iBadSuffix=0;
//
//   // Merge loop
//   bEof = false;
//   while (!bEof)
//   {
//      bRet = ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);
//#ifdef _DEBUG
//      // Test problem that not all sale records go in
//      //if (!memcmp(acBuf, "009600377", 9) )
//      //   iRet = 0;
//#endif
//
//      // Check for EOF
//      if (!bRet)
//      {
//         LogMsg("***** Error reading input file %s (%f)", acRawFile, GetLastError());
//         break;
//      }
//
//      // EOF ?
//      if (iRecLen != nBytesRead)
//         break;
//
//      NextRollRec:
//
//      Result = memcmp(acBuf, &acRollRec[ROFF_ASSMNT_NO], iApnLen);
//      if (!Result)
//      {
//         // Merge roll data
//         Riv_MergeRoll(acBuf, acRollRec, iRollLen, UPDATE_R01);
//         iRollUpd++;
//
//         // Merge Char
//         if (fdChar)
//            iRet = Riv_MergeStdChar(acBuf);
//
//         if (acBuf[OFF_TRANSFER_DOC] > '0' && (lOptMisc & M_OPT_FIXDOCN))
//         {
//            iRet = atoin(&acBuf[OFF_TRANSFER_DOC], 7);
//            sprintf(acRec, "%.7d", iRet);
//            memcpy(&acBuf[OFF_TRANSFER_DOC], acRec, 7);
//         }
//
//         bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
//         if (!(++lCnt % 1000))
//            printf("\r%u", lCnt);
//
//         //iRet = fread(acRollRec, 1, MAX_ROLLREC, fdRoll);
//         iRet = fread(acRollRec, 1, iRollLen, fdRoll);
//         if (iRet != iRollLen)
//            bEof = true;
//      } else if (Result > 0)
//      {
//         if (bDebug)
//            LogMsg0("*** New roll record : %.*s (%d) ***", iApnLen, &acRollRec[ROFF_ASSMNT_NO], lCnt);
//
//         // Create new R01 record
//         memset(acRec, ' ', iRecLen);
//         Riv_MergeRoll(acRec, acRollRec, iRollLen, CREATE_R01);
//         iNewRec++;
//
//         // Merge Char
//         if (fdChar)
//            iRet = Riv_MergeStdChar(acRec);
//
//         bRet = WriteFile(fhOut, acRec, iRecLen, &nBytesWritten, NULL);
//         if (!(++lCnt % 1000))
//            printf("\r%u", lCnt);
//
//         //iRet = fread(acRollRec, 1, MAX_ROLLREC, fdRoll);
//         iRet = fread(acRollRec, 1, iRollLen, fdRoll);
//         if (iRet != iRollLen)
//            bEof = true;    // Signal to stop
//         else
//            goto NextRollRec;
//      } else
//      {
//         // Record may be retired
//         if (bDebug)
//            LogMsg0("*** Roll not match (retired record?) : R01->%.*s < Roll->%.*s (%d) ***", iApnLen, acBuf, iApnLen, &acRollRec[ROFF_ASSMNT_NO], lCnt);
//         iRetiredRec++;
//         continue;
//      }
//   }
//
//   // Do the rest of the file
//   while (!bEof)
//   {
//      if (bDebug)
//         LogMsg0("*** New roll record : %.*s (%d) ***", iApnLen, &acRollRec[ROFF_ASSMNT_NO], lCnt);
//
//      // Create new R01 record
//      memset(acRec, ' ', iRecLen);
//      Riv_MergeRoll(acRec, acRollRec, iRollLen, CREATE_R01);
//      iNewRec++;
//
//      // Merge Char
//      if (fdChar)
//         iRet = Riv_MergeStdChar(acRec);
//
//      bRet = WriteFile(fhOut, acRec, iRecLen, &nBytesWritten, NULL);
//      lCnt++;
//
//      //iRet = fread(acRollRec, 1, MAX_ROLLREC, fdRoll);
//      iRet = fread(acRollRec, 1, iRollLen, fdRoll);
//      if (iRet != iRollLen)
//         break;
//
//         bEof = true;    // Signal to stop
//   }
//
//   // Close files
//   if (fdRoll)
//      fclose(fdRoll);
//   if (fdChar)
//      fclose(fdChar);
//   if (fhOut)
//      CloseHandle(fhOut);
//   if (fhIn)
//      CloseHandle(fhIn);
//
//   LogMsg("Total output records:       %u", lCnt);
//   LogMsg("Total new records:          %u", iNewRec);
//   LogMsg("Total retired records:      %u", iRetiredRec);
//   LogMsg("Total char rec matched:     %u", lCharMatch);
//   LogMsg("Total char rec skipped:     %u", lCharSkip);
//   LogMsg("Total bad-city records:     %u", iBadCity);
//   LogMsg("Total bad-suffix records:   %u\n", iBadSuffix);
//
//   printf("\nTotal output records: %u\n", lCnt);
//
//   lRecCnt = lCnt;
//   return 0;
//}

/******************************** Riv_MergeSAdr ******************************
 *
 * Merge Situs address from AssessmentRoll.csv
 * Return 0 if successful, otherwise error.
 *
 *****************************************************************************/

int Riv_MergeSAdr(char *pOutbuf)
{
   static   char acRec[512], *pRec=NULL;
   char     acTmp[256], acCode[32], acStrName[32], acStrType[32], acAddr1[256];
   long     lTmp;
   int      iTmp;

   // Remove old situs
   removeSitus(pOutbuf);

   // Merge data
   acAddr1[0] = 0;
   lTmp = atol(apTokens[R_SITUSSTREETNUMBER]);
   if (lTmp > 0)
   {
      memcpy(pOutbuf+OFF_S_HSENO, apTokens[R_SITUSSTREETNUMBER], strlen(apTokens[R_SITUSSTREETNUMBER]));
      iTmp = sprintf(acTmp, "%d ", lTmp);
      memcpy(pOutbuf+OFF_S_STRNUM, acTmp, iTmp);
      strcpy(acAddr1, acTmp);

      if (*apTokens[R_SITUSSTREETNUMBERSUFFIX] > ' ')
         vmemcpy(pOutbuf+OFF_S_STR_SUB, apTokens[R_SITUSSTREETNUMBERSUFFIX], SIZ_S_STR_SUB);

      if (*apTokens[R_SITUSSTREETPREDIRECTIONAL] > ' ')
      {
         strcat(acAddr1, apTokens[R_SITUSSTREETPREDIRECTIONAL]);
         strcat(acAddr1, " ");
         memcpy(pOutbuf+OFF_S_DIR, apTokens[R_SITUSSTREETPREDIRECTIONAL], strlen(apTokens[R_SITUSSTREETPREDIRECTIONAL]));
      }
   }

   strcpy(acStrName, _strupr(apTokens[R_SITUSSTREETNAME]));
   if (!strcmp(acStrName, "RIV"))
      strcpy(acStrName, "RIVERSIDE");

   strcat(acAddr1, acStrName);
   if (*apTokens[R_SITUSSTREETTYPE] > ' ')
   {
      if (!memcmp(apTokens[R_SITUSSTREETTYPE], "BOULE", 5))
         strcpy(acStrType, "BLVD");
      else
         strcpy(acStrType, apTokens[R_SITUSSTREETTYPE]);

      strcat(acAddr1, " ");
      strcat(acAddr1, acStrType);

      iTmp = GetSfxCodeX(acStrType, acTmp);
      if (iTmp > 0)
      {
         Sfx2Code(acTmp, acCode);
      } else
      {
         LogMsg("*** Invalid suffix: %s [%s].  Add it to street name.", acStrType, apTokens[R_APN]);
         iBadSuffix++;
         memset(acCode, ' ', SIZ_S_SUFF);

         // Add to street name
         strcat(acStrName, " ");
         strcat(acStrName, apTokens[R_SITUSSTREETTYPE]);
      }
      memcpy(pOutbuf+OFF_S_SUFF, acCode, SIZ_S_SUFF);
   } 

   // Street name
   vmemcpy(pOutbuf+OFF_S_STREET, acStrName, SIZ_S_STREET);

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "616141018", 9))
   //   acTmp[0] = 0;
#endif
   _strupr(apTokens[R_SITUSUNITNUMBER]);
   if (isalnum(*apTokens[R_SITUSUNITNUMBER]) || *apTokens[R_SITUSUNITNUMBER] == '#')
   {
      strcat(acAddr1, " ");
      if (isdigit(*apTokens[R_SITUSUNITNUMBER]))
         strcat(acAddr1, "#");
      else if ((*apTokens[R_SITUSUNITNUMBER]) != '#')
         strcat(acAddr1, "STE ");
      strcat(acAddr1, apTokens[R_SITUSUNITNUMBER]);
      memcpy(pOutbuf+OFF_S_UNITNO, apTokens[R_SITUSUNITNUMBER], strlen(apTokens[R_SITUSUNITNUMBER]));
   } else if (*(pOutbuf+OFF_M_UNITNO) > ' ' && *(pOutbuf+OFF_HO_FL) == '1' &&
      !memcmp(pOutbuf+OFF_S_STRNUM, pOutbuf+OFF_M_STRNUM, SIZ_S_STRNUM) &&
      !memcmp(pOutbuf+OFF_S_STREET, pOutbuf+OFF_M_STREET, SIZ_S_STREET) )
   {
      memcpy(pOutbuf+OFF_S_UNITNO, pOutbuf+OFF_M_UNITNO, SIZ_M_UNITNO);
      sprintf(acTmp, " %.*s", SIZ_M_UNITNO, pOutbuf+OFF_M_UNITNO);
      strcat(acAddr1, acTmp);
   } else if (*apTokens[R_SITUSUNITNUMBER] > ' ')
   {
      if (isalnum(*(1+apTokens[R_SITUSUNITNUMBER])))
      {
         strcat(acAddr1, " #");
         strcat(acAddr1, apTokens[R_SITUSUNITNUMBER]+1);
         memcpy(pOutbuf+OFF_S_UNITNO, apTokens[R_SITUSUNITNUMBER]+1, strlen(apTokens[R_SITUSUNITNUMBER])-1);
      } else
         LogMsg("*** Invalid situs Unit#: %s [%s]", apTokens[R_SITUSUNITNUMBER], apTokens[R_APN]);
   }

   iTmp = blankRem(acAddr1);
   memcpy(pOutbuf+OFF_S_ADDR_D, acAddr1, iTmp);
   memcpy(pOutbuf+OFF_S_ST, "CA", 2);

   // Situs city
   if (*apTokens[R_SITUSCITYNAME] > ' ' && _memicmp(apTokens[R_SITUSCITYNAME], "UNKNOWN", 7))
   {
      char sCity[32];
      int  iMatch = 0;

      strcpy(acTmp, _strupr(apTokens[R_SITUSCITYNAME]));

ChkSitusCity:
      iTmp = City2CodeEx(acTmp, acCode, sCity, apTokens[R_APN]);
      if (iTmp > 0)
      {
         vmemcpy(pOutbuf+OFF_S_CITY, acCode, SIZ_S_CITY);
      } else if (!iMatch
         && !memcmp(pOutbuf+OFF_S_STRNUM, pOutbuf+OFF_M_STRNUM, SIZ_S_STRNUM) 
         && !memcmp(pOutbuf+OFF_S_STREET, pOutbuf+OFF_M_STREET, SIZ_S_STREET) )
      {
         memcpy(acTmp, pOutbuf+OFF_M_CITY, SIZ_M_CITY);
         myTrim(acTmp, SIZ_M_CITY);
         iMatch = 1;
         goto ChkSitusCity;
      } else if (!iMatch)
      {
         iMatch = 2;
         acTmp[0] = 0;
         if (!memcmp(apTokens[R_SITUSZIPCODE], "92211", 5))
            strcpy(acTmp, "PALM DESERT");
         else if (!memcmp(apTokens[R_SITUSZIPCODE], "92223", 5))
            strcpy(acTmp, "BEAUMONT");
         else if (!memcmp(apTokens[R_SITUSZIPCODE], "92530", 5))
            strcpy(acTmp, "LAKE ELSINORE");
         else if (!memcmp(apTokens[R_SITUSZIPCODE], "92508", 5) || !memcmp(apTokens[R_SITUSZIPCODE], "92518", 5))
            strcpy(acTmp, "RIVERSIDE");
         else if (!memcmp(apTokens[R_SITUSZIPCODE], "92584", 5) || !memcmp(apTokens[R_SITUSZIPCODE], "92587", 5))
            strcpy(acTmp, "MENIFEE");

         if (acTmp[0] >= 'A')
            goto ChkSitusCity;
      } 

      if (iMatch == 1)
         memcpy(pOutbuf+OFF_S_ZIP, pOutbuf+OFF_M_ZIP, SIZ_S_ZIP);
      else if (*apTokens[R_SITUSZIPCODE] == '9')
         vmemcpy(pOutbuf+OFF_S_ZIP, apTokens[R_SITUSZIPCODE], SIZ_S_ZIP);

      if (sCity[0] >= 'A')
      {
         iTmp = sprintf(acTmp, "%s, CA %s", sCity, apTokens[R_SITUSZIPCODE]);
         memcpy(pOutbuf+OFF_S_CTY_ST_D, acTmp, iTmp);
      } else if (*(pOutbuf+OFF_S_ZIP) == '9')
      {
         iTmp = sprintf(acTmp, "CA %s", apTokens[R_SITUSZIPCODE]);
         memcpy(pOutbuf+OFF_S_CTY_ST_D, acTmp, iTmp);
      }
   }

   return 0;
}

/****************************** Riv_MergeRollCsv *****************************
 *
 * Return 0 if successful, < 0 if error
 *        1 retired record, not use
 *
 *****************************************************************************/

int Riv_MergeRollCsv(char *pOutbuf, char *pRollRec, int iLen, int iFlag)
{
   char     acTmp[256], acTmp1[256];
   LONGLONG lTmp;
   long     lFixt, lPP, lGrow, lLand, lImpr;
   double   dTmp;
   int      iRet=0, iTmp;

   // Parse roll record
   iRet = ParseStringNQ(pRollRec, cDelim, MAX_FLD_TOKEN, apTokens);
   if (iRet < R_COLS)
   {
      LogMsg("***** Error: bad input record for APN=%s", apTokens[R_APN]);
      return -1;
   }
#ifdef _DEBUG
   //if (!_memicmp(pRollRec, "689040017263", 12) )
   //   iTmp = 0;
#endif

   // Ignore non-taxable parcels - Keep non-taxable parcel during update
   //if (*apTokens[R_ISTAXABLE] == 'N')
   //   return 1;

   // Ignore unsecured parcel 000-008
   if (memcmp(apTokens[R_APN], "008", 3) <= 0)
      return 1;

   // BPP parcels
   if (!_memicmp(apTokens[R_APN]+9, "BPP", 3))
   {
      // We may use this later when there is request
      //LogMsg("*** Ignore BPP record %s", apTokens[R_APN]);
      return 2;
   }

   if (iFlag & CREATE_R01)
   {
      // Clear output buffer
      memset(pOutbuf, ' ', iRecLen);

      // Start copying data
      memcpy(pOutbuf, apTokens[R_APN], strlen(apTokens[R_APN]));
      *(pOutbuf+OFF_STATUS) = 'A';

      // Format APN
      iRet = formatApn(apTokens[R_APN], acTmp, &myCounty);
      memcpy(pOutbuf+OFF_APN_D, acTmp, iRet);

      // Create MapLink and output new record
      iRet = formatMapLink(apTokens[R_APN], acTmp, &myCounty);
      memcpy(pOutbuf+OFF_MAPLINK, acTmp, iRet);

      // Create index map link
      if (getIndexPage(acTmp, acTmp1, &myCounty))
         memcpy(pOutbuf+OFF_IMAPLINK, acTmp1, iRet);

      // County code
      memcpy(pOutbuf+OFF_CO_NUM, "33RIV", 5);

      // Year assessed
      memcpy(pOutbuf+OFF_YR_ASSD, myCounty.acYearAssd, 4);

      // Land
      lLand = atoi(apTokens[R_LAND]);
      if (lLand > 0)
      {
         sprintf(acTmp, "%*u", SIZ_LAND, lLand);
         memcpy(pOutbuf+OFF_LAND, acTmp, SIZ_LAND);
      }

      // Improve
      lImpr = atoi(apTokens[R_IMPR]);
      if (lImpr > 0)
      {
         sprintf(acTmp, "%*u", SIZ_IMPR, lImpr);
         memcpy(pOutbuf+OFF_IMPR, acTmp, SIZ_IMPR);
      }

      // Other values
      lFixt  = atoi(apTokens[R_TRADEFIXTURESAMOUNT]);
      lPP    = atoi(apTokens[R_PERSONALVALUE]);
      lGrow  = atoi(apTokens[R_LIVINGIMPROVEMENTS]);
      lTmp = lFixt+lPP+lGrow;
      if (lTmp > 0)
      {
         sprintf(acTmp, "%*u", SIZ_OTHER, lTmp);
         memcpy(pOutbuf+OFF_OTHER, acTmp, SIZ_OTHER);

         if (lFixt > 0)
         {
            sprintf(acTmp, "%d         ", lFixt);
            memcpy(pOutbuf+OFF_FIXTR, acTmp, SIZ_FIXTR);
         }
         if (lPP > 0)
         {
            sprintf(acTmp, "%d         ", lPP);
            memcpy(pOutbuf+OFF_PERSPROP, acTmp, SIZ_PERSPROP);
         }
         if (lGrow > 0)
         {
            sprintf(acTmp, "%d         ", lGrow);
            memcpy(pOutbuf+OFF_GR_IMPR, acTmp, SIZ_GR_IMPR);
         }
      }

      // Gross total
      lTmp += (lLand+lImpr);
      if (lTmp > 0)
      {
         sprintf(acTmp, "%*u", SIZ_GROSS, lTmp);
         memcpy(pOutbuf+OFF_GROSS, acTmp, SIZ_GROSS);
      }

      // Ratio
      if (lImpr > 0)
      {
         sprintf(acTmp, "%*u", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
         memcpy(pOutbuf+OFF_RATIO, acTmp, SIZ_RATIO);
      }
   }

   // TRA
   iTmp = atol(apTokens[R_TRA]);
   sprintf(acTmp, "%.6u", iTmp);
   memcpy(pOutbuf+OFF_TRA, acTmp, 6);

   // Is Taxable
   if (*apTokens[R_ISTAXABLE] == 'N')
      *(pOutbuf+OFF_FULL_EXEMPT) = 'Y';

   // Legal
   iTmp = replUnPrtChar(apTokens[R_ASSESSMENTDESCRIPTION], ' ');
   iTmp = updateLegal(pOutbuf, _strupr(apTokens[R_ASSESSMENTDESCRIPTION]));
   if (iTmp > iMaxLegal)
      iMaxLegal = iTmp;

#ifdef _DEBUG
   //if (!memcmp(apTokens[R_APN], "486482012", 9) )
   //   iTmp = 0;
#endif


   // Zoning - special case
   if (*(pOutbuf+OFF_ZONE+1) == '|')
      *(pOutbuf+OFF_ZONE+1) = '1';
   else if (*(pOutbuf+OFF_ZONE) == 39)
      *(pOutbuf+OFF_ZONE) = ' ';
   if (*(pOutbuf+OFF_ZONE_X1) == 39)
      *(pOutbuf+OFF_ZONE_X1) = ' ';

   // UseCode - being updated at LDR

   // Acres
   dTmp = atof(apTokens[R_ACREAGEAMOUNT]);
   if (dTmp > 0.0)
   {
      // Lot Sqft
      lTmp = (long)(dTmp * SQFT_PER_ACRE);
      sprintf(acTmp, "%*u", SIZ_LOT_SQFT, lTmp);
      memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);

      // Format Acres
      lTmp = (long)(dTmp * ACRES_FACTOR);
      sprintf(acTmp, "%*u", SIZ_LOT_ACRES, lTmp);
      memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);
   }

   // Owner
   try {
      Riv_MergeOwner(pOutbuf);
   } catch(...) {
      LogMsg("***** Exeception occured in Riv_MergeOwner()");
   }

   // Mailing
   Riv_MergeMAdr(pOutbuf);

   // Situs
   Riv_MergeSAdr(pOutbuf);

   return 0;
}

/****************************** Riv_CreateBppRec ******************************
 *
 * Create a BPP record
 *
 ******************************************************************************/

int Riv_CreateBppRec(char *pOutbuf)
{
   BPP_REC *pOutrec = (BPP_REC *)pOutbuf;
   long     lFixtr, lPP, iTmp;
   char     sTmp[32];

   memset(pOutbuf, ' ', sizeof(BPP_REC));
   memcpy(pOutrec->Apn, apTokens[R_APN], strlen(apTokens[R_APN]));
   lFixtr = atol(apTokens[R_TRADEFIXTURESAMOUNT]);
   iTmp = sprintf(sTmp, "%ld", lFixtr);
   memcpy(pOutrec->Fixtr, sTmp, iTmp);
   lPP = atol(apTokens[R_PERSONALVALUE]);
   iTmp = sprintf(sTmp, "%ld", lPP);
   memcpy(pOutrec->PersProp, sTmp, iTmp);
   pOutrec->CRLF[0] = '\n';
   pOutrec->CRLF[1] = 0;

   if (lFixtr > 0 || lPP > 0)
      return 1;
   else
      return 0;
}

/****************************** Riv_Load_RollCsv ******************************
 *
 * Load updated roll file AssessmentRoll.csv (10/06/2019)
 *
 ******************************************************************************/

int Riv_Load_RollCsv(int iSkip)
{
   char     acRec[MAX_RECSIZE], acBuf[MAX_RECSIZE], acRollRec[MAX_RECSIZE], acLastApn[32], acApn[32];
   char     acRawFile[_MAX_PATH], acOutFile[_MAX_PATH], acSrtFile[_MAX_PATH], acBppFile[_MAX_PATH], *pTmp;
   int      iRet, iTmp, iRollUpd=0, iNewRec=0, iRetiredRec=0, lCnt=0, iDrop=0;
   DWORD    nBytesRead, nBytesWritten;
   BOOL     bRet, bEof;
   HANDLE   fhIn, fhOut;
   FILE     *fdBpp;

   LogMsg0("Loading roll update");

   sprintf(acRawFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "S01");
   sprintf(acOutFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "R01");

   // Rename files for processing
   if (_access(acRawFile, 0))
   {
      if (!_access(acOutFile, 0))
         rename(acOutFile, acRawFile);
      else
      {
         LogMsg("***** Missing input file %s.  Please recheck!", acRawFile);
         return -1;
      }
   }

   // Sort input
   sprintf(acSrtFile, "%s\\RIV\\Riv_Roll.srt", acTmpPath);
   sprintf(acRec, "S(#1,C,A) DEL(%d) F(TXT)", cDelim);
   iTmp = sortFile(acRollFile, acSrtFile, acRec);

   // Open roll file
   LogMsg("Open Roll file %s", acSrtFile);
   fdRoll = fopen(acSrtFile, "r");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", acSrtFile);
      return 2;
   }

   LogMsg("Open Char file %s", acCChrFile);
   fdChar = fopen(acCChrFile, "r");
   if (fdChar == NULL)
   {
      LogMsg("***** Error opening Char file: %s\n", acCChrFile);
      return -2;
   }

   // Open Input file
   LogMsg("Open input file %s", acRawFile);
   fhIn = CreateFile(acRawFile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhIn == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening input file: %s\n", acRawFile);
      return -3;
   }

   // Open Output file
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening output file: %s\n", acOutFile);
      return 4;
   }

   // Open Bpp file
   GetIniString("Data", "BppFile", ".\\", acRec, _MAX_PATH, acIniFile);
   sprintf(acBppFile, acRec, myCounty.acCntyCode, myCounty.acCntyCode);
   if (!(fdBpp = fopen(acBppFile, "w")))
   {
      fclose(fdBpp);
      LogMsg("***** Error creating output file %s", acBppFile);
      return -2;
   }

   // Get 1st rec
   pTmp = fgets((char *)&acRollRec[0], MAX_RECSIZE, fdRoll);    
   bEof = (pTmp ? false:true);

   // Init variables
   iNoMatch=iBadCity=iBadSuffix=0;
   strcpy(acLastApn, "000000000");

   // Copy skip record
   memset(acBuf, ' ', iRecLen);
   while (iSkip-- > 0)
   {
      ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);
      // Ignore record start with 99999999
      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
   }

   // Merge loop
   iBpp = 0;
   while (!bEof)
   {
      bRet = ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);

      // Check for EOF
      if (!bRet)
      {
         LogMsg("***** Error reading input file %s (%f)", acRawFile, GetLastError());
         break;
      }

      // EOF ?
      if (!nBytesRead)
         break;

NextRollRec:
#ifdef _DEBUG
      //if (!memcmp(acRollRec, "689040017X263", 13) )
      //   iTmp = 0;
      //if (!memcmp(acBuf, "689040017-263", 9) )
      //   iTmp = 0;
#endif

      if (acRollRec[9] == 'X')
      {
         sprintf(acApn, "%.9s%.3s", acRollRec, &acRollRec[10]);
         iTmp = memcmp(acBuf, acApn, 12);
      } else
         iTmp = memcmp(acBuf, acRollRec, iApnLen);
      if (!iTmp)
      {
         // Create R01 record from roll data
         iRet = Riv_MergeRollCsv(acBuf, acRollRec, MAX_RECSIZE, UPDATE_R01);
         if (!iRet)
         {
            // Merge Char
            if (fdChar)
               iTmp = Riv_MergeStdChar(acBuf);

            iRollUpd++;
         } else if (iRet == 2)
         {
            // Create BPP record
            iRet = Riv_CreateBppRec(acRec);
            if (iRet == 1)
            {
               fputs(acRec, fdBpp);
               iBpp++;
            }
         } else
            iDrop++;

         // Read next roll record
         pTmp = fgets(acRollRec, MAX_RECSIZE, fdRoll);

         if (iRet > 0)
            goto NextRollRec;
         if (!pTmp)
            bEof = true;         // Signal to stop
      } else if (iTmp > 0)       // Roll not match, new roll record?
      {
         if (bDebug)
            LogMsg0("New roll record : %.*s (%d)", iApnLen, acRollRec, lCnt);

         // Create new R01 record
         iRet = Riv_MergeRollCsv(acRec, acRollRec, MAX_RECSIZE, CREATE_R01);
         if (!iRet)
         {
            // Merge Char
            if (fdChar)
               iTmp = Riv_MergeStdChar(acRec);

            if (memcmp(acLastApn, acRec, 14))
            {
               memcpy(acLastApn, acRec, 14);
               bRet = WriteFile(fhOut, acRec, iRecLen, &nBytesWritten, NULL);
               iNewRec++;
            } else
            {
               LogMsg("---> Duplicate APN: %.14s (NEW)", acLastApn);
               bDupApn = true;
            }
         } else if (iRet == 2)
         {
            // Create BPP record
            iRet = Riv_CreateBppRec(acRec);
            if (iRet == 1)
            {
               fputs(acRec, fdBpp);
               iBpp++;
            }
         } else
            iDrop++;

         lCnt++;

         // Get next roll record
         pTmp = fgets(acRollRec, MAX_RECSIZE, fdRoll);
         if (!pTmp)
            bEof = true;    // Signal to stop
         else
            goto NextRollRec;
      } else
      {
         // Record may be retired
         if (bDebug)
            LogMsg0("Retired record? : R01->%.*s < Roll->%.*s (%d)", iApnLen, acBuf, iApnLen, (char *)&acRollRec[1], lCnt);
         iRetiredRec++;

         continue;
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
      if (!iRet)
      {
         if (memcmp(acLastApn, acBuf, 14))
         {
            memcpy(acLastApn, acBuf, 14);
            bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
            if (!bRet)
            {
               LogMsg("Error occurs: %d\n", GetLastError());
               break;
            }
         }
      }
   }

   // Do the rest of the file
   while (!bEof && isdigit(acRollRec[0]))
   {
      // Create new R01 record
      iRet = Riv_MergeRollCsv(acRec, acRollRec, MAX_RECSIZE, CREATE_R01);

      if (!iRet)
      {
         if (bDebug)
            LogMsg0("New roll record : %.*s (%d) ", iApnLen, acRollRec, lCnt);

         // Merge Char
         if (fdChar)
            iRet = Riv_MergeStdChar(acRec);

         if (memcmp(acLastApn, acRec, 14))
         {
            memcpy(acLastApn, acRec, 14);
            bRet = WriteFile(fhOut, acRec, iRecLen, &nBytesWritten, NULL);
            iNewRec++;
         } else
         {
            LogMsg("---> Duplicate APN: %.14s (END)", acLastApn);
            bDupApn = true;
         }
      } else if (iRet == 2)
      {
         // Create BPP record
         iRet = Riv_CreateBppRec(acRec);
         if (iRet == 1)
         {
            fputs(acRec, fdBpp);
            iBpp++;
         }
      } else
         iDrop++;

      lCnt++;

      // Get next roll record
      pTmp = fgets(acRollRec, MAX_RECSIZE, fdRoll);

      if (!pTmp)
         bEof = true;    // Signal to stop
   }

   // Close files
   if (fdRoll)
      fclose(fdRoll);
   if (fdChar)
      fclose(fdChar);
   if (fdBpp)
      fclose(fdBpp);
   if (fhOut)
      CloseHandle(fhOut);

   LogMsg("Total output records:       %u", lCnt);
   LogMsg("      BPP records:          %u", iBpp);
   LogMsg("      records  dropped:     %u", iDrop);
   LogMsg("      retired records:      %u", iRetiredRec);
   LogMsg("      bad-city records:     %u", iBadCity);
   LogMsg("      bad-suffix records:   %u\n", iBadSuffix);

   LogMsg("Number of Char matched:     %u", lCharMatch);
   LogMsg("Number of Char skiped:      %u\n", lCharSkip);

   printf("\nTotal output records: %u", lCnt);

   lRecCnt = lCnt;
   return 0;
}

/******************************** Riv_MergeSitus ****************************
 *
 *
 ****************************************************************************/

int Riv_MergeSitus(char *pOutbuf)
{
   static   char acRec[256], *pRec=NULL;
   char     *pTmp, acTmp[256], acTmp1[64], acAddr1[64], acSfx[8];
   int      lTmp, iRet, iTmp;

   // Initialize
   acAddr1[0] = 0;
   lTmp = atol(apTokens[L_S_STRNUM]);
   if (lTmp > 0)
   {
      iTmp = sprintf(acAddr1, "%d ", lTmp);
      memcpy(pOutbuf+OFF_S_STRNUM, acAddr1, iTmp);
      memcpy(pOutbuf+OFF_S_HSENO, apTokens[L_S_STRNUM], strlen(apTokens[L_S_STRNUM]));
      if (*apTokens[L_S_STRFRA] > ' ')
      {
         vmemcpy(pOutbuf+OFF_S_STR_SUB, apTokens[L_S_STRFRA], SIZ_S_STR_SUB);
         strcat(acAddr1, apTokens[L_S_STRFRA]);
         strcat(acAddr1, " ");
      }
   }

   // DIR - N, S, E, W, NO, SO
   if (*apTokens[L_S_DIR] > ' ')
   {
      *(pOutbuf+OFF_S_DIR) = *apTokens[L_S_DIR];
      acTmp[0] = *apTokens[L_S_DIR];
      acTmp[1] = ' ';
      acTmp[2] = 0;
      strcat(acAddr1, acTmp);
   }
   vmemcpy(pOutbuf+OFF_S_STREET, _strupr(apTokens[L_S_STRNAME]), SIZ_S_STREET);
   acSfx[0] = 0;
   if (*apTokens[L_S_STRSFX] > ' ')
   {
      // Translate to code
      if (!memcmp(apTokens[L_S_STRSFX], "BOULEVAR", 8))
         strcpy(acTmp, "BLVD");
      else
         strcpy(acTmp, apTokens[L_S_STRSFX]);
      iTmp = GetSfxCodeX(acTmp, acSfx);
      if (iTmp)
      {
         iRet = sprintf(acTmp1, "%d", iTmp);
         memcpy(pOutbuf+OFF_S_SUFF, acTmp1, iRet);
      } else
      {
         LogMsg0("*** Unknown suffix %s,  Add to StrName [%s]", acTmp, apTokens[L_PIN]);
         iTmp = sprintf(acTmp1, "%s %s", apTokens[L_S_STRNAME], apTokens[L_S_STRSFX]);
         vmemcpy(pOutbuf+OFF_S_STREET, acTmp1, SIZ_S_STREET, iTmp);
         strcpy(acSfx, apTokens[L_S_STRSFX]);
      }
   }

   sprintf(acTmp, "%s %s ", apTokens[L_S_STRNAME], acSfx);
   strcat(acAddr1, acTmp);

   if (*apTokens[L_S_UNIT] > '0')
   {
      // Remove garbage
      if (pTmp = strchr(apTokens[L_S_UNIT], ')'))
         *pTmp = 0;

      if (*apTokens[L_S_UNIT] == '#')
         iTmp = 0;
      else 
      {
         iTmp = 1;
         *(pOutbuf+OFF_S_UNITNO) = '#';
         strcat(acAddr1, "#");
      }
      memcpy(pOutbuf+OFF_S_UNITNO+iTmp, apTokens[L_S_UNIT], strlen(apTokens[L_S_UNIT]));
      strcat(acAddr1, apTokens[L_S_UNIT]);
   } else if (*(pOutbuf+OFF_M_UNITNO) > ' ' && *(pOutbuf+OFF_HO_FL) == '1' && 
      !memcmp(pOutbuf+OFF_S_STRNUM, pOutbuf+OFF_M_STRNUM, SIZ_S_STRNUM) &&
      !memcmp(pOutbuf+OFF_S_STREET, pOutbuf+OFF_M_STREET, SIZ_S_STREET) )
   {
      memcpy(pOutbuf+OFF_S_UNITNO, pOutbuf+OFF_M_UNITNO, SIZ_M_UNITNO);
      sprintf(acTmp, " %.*s", SIZ_M_UNITNO, pOutbuf+OFF_M_UNITNO);
      strcat(acAddr1, acTmp);
   }

   iTmp = blankRem(acAddr1);
   memcpy(pOutbuf+OFF_S_ADDR_D, acAddr1, iTmp);

   // Zipcode
   int lZip = atoin(apTokens[L_S_ZIP], 5);
   if (lZip > 90000)
      memcpy(pOutbuf+OFF_S_ZIP, apTokens[L_S_ZIP], SIZ_S_ZIP);
   else if (*apTokens[L_S_CITY] >= 'A' && !memcmp(apTokens[L_S_CITY], pOutbuf+OFF_M_CITY, strlen(apTokens[L_S_CITY])))
   {
      lZip = atoin(pOutbuf+OFF_M_ZIP, SIZ_M_ZIP);
      if (lZip > 90000)
         memcpy(pOutbuf+OFF_S_ZIP, pOutbuf+OFF_M_ZIP, SIZ_M_ZIP);
   }

   // City
   if (*apTokens[L_S_CITY] > ' ')
   {
      strcpy(acTmp, apTokens[L_S_CITY]);
      iTmp = City2Code(_strupr(acTmp), acTmp1, pOutbuf);
      if (!iTmp)
      {
         if (!memcmp(apTokens[L_S_ZIP], "92880", 5) || !memcmp(apTokens[L_S_ZIP], "92881", 5))
            strcpy(acTmp, "CORONA");
         else if (!memcmp(apTokens[L_S_ZIP], "91752", 5))
            strcpy(acTmp, "MIRA LOMA");
         else if (!memcmp(apTokens[L_S_ZIP], "92211", 5))
            strcpy(acTmp, "PALM DESERT");
         else if (!memcmp(apTokens[L_S_ZIP], "92223", 5))
            strcpy(acTmp, "BEAUMONT");
         else if (!memcmp(apTokens[L_S_ZIP], "92236", 5))
            strcpy(acTmp, "INDIO");
         else if (!memcmp(apTokens[L_S_ZIP], "92274", 5))
            strcpy(acTmp, "THERMAL");
         else if (!memcmp(apTokens[L_S_ZIP], "92549", 5))
            strcpy(acTmp, "IDYLLWILD");
         else if (!memcmp(apTokens[L_S_ZIP], "92555", 5))
            strcpy(acTmp, "MORENO VALLEY");
         else if (!memcmp(apTokens[L_S_ZIP], "92562", 5))
            strcpy(acTmp, "MURRIETA");
         else if (!memcmp(apTokens[L_S_ZIP], "92567", 5))
            strcpy(acTmp, "NUEVO");
         else if (!memcmp(apTokens[L_S_ZIP], "92584", 5))
            strcpy(acTmp, "MENIFEE");
         else if (!memcmp(apTokens[L_S_ZIP], "92530", 5))
            strcpy(acTmp, "LAKE ELSINORE");
         else if (!memcmp(apTokens[L_S_ZIP], "92583", 5))
            strcpy(acTmp, "SAN JACINTO");
         else if (!memcmp(apTokens[L_S_ZIP], "92276", 5))
            strcpy(acTmp, "THOUSAND PALMS");
         else if (!memcmp(acTmp, "92563", 5))
         {
            strcpy(acTmp, "MURRIETA");
            memcpy(pOutbuf+OFF_S_ZIP, "92563", 5);
            lZip = 92563;
         }
         else if (!memcmp(acTmp, "92582", 5))
         {
            strcpy(acTmp, "SAN JACINTO");
            memcpy(pOutbuf+OFF_S_ZIP, "92582", 5);
            lZip = 92582;
         }
         else if (!memcmp(acTmp, "92584", 5))
         {
            strcpy(acTmp, "MENIFEE");
            memcpy(pOutbuf+OFF_S_ZIP, "92584", 5);
            lZip = 92584;
         }
         iTmp = City2Code(acTmp, acTmp1, pOutbuf);
      } 

      memcpy(pOutbuf+OFF_S_CITY, acTmp1, SIZ_M_CITY);
      iTmp = sprintf(acTmp1, "%s, CA %d", acTmp, lZip);
      memcpy(pOutbuf+OFF_S_CTY_ST_D, acTmp1, iTmp);
   }

   // State
   memcpy(pOutbuf+OFF_S_ST, "CA", 2);

   return 0;
}

/********************************* Riv_Load_LDR *****************************
 *
 *
 ****************************************************************************/

//int Riv_Load_LDR(int iFirstRec)
//{
//   char     acBuf[MAX_RECSIZE], acRollRec[256], acLegal[MAX_RECSIZE];
//   char     acOutFile[_MAX_PATH], *pTmp;
//
//   HANDLE   fhOut;
//   
//   int      iRet, iTmp;
//   DWORD    nBytesWritten;
//   BOOL     bRet;
//   long     lRet=0, lCnt=0;
//
//   // Open roll file
//   LogMsg("Open Roll file %s", acRollFile);
//   fdRoll = fopen(acRollFile, "rb");
//   if (fdRoll == NULL)
//   {
//      printf("Error opening roll file: %s\n", acRollFile);
//      return -1;
//   }
//
//   // Open Char file
//   LogMsg("Open Char file %s", acCChrFile);
//   fdChar = fopen(acCChrFile, "r");
//   if (fdChar == NULL)
//   {
//      LogMsg("***** Error opening Char file: %s\n", acCChrFile);
//      return -2;
//   }
//
//   // Skip unsecured parcels
//   do
//   {
//      iRet = fread(acRollRec, 1, iRollLen, fdRoll);
//      iTmp = atoin(&acRollRec[OFF_LIEN_PRIMARY_PARCEL_NO], 3);
//   } while (iRet == iRollLen && iTmp < 2);
//
//   // Open Output file
//   sprintf(acOutFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "R01");
//   LogMsg("Open output file %s", acOutFile);
//   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
//             FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);
//
//   if (fhOut == INVALID_HANDLE_VALUE)
//   {
//      LogMsg("***** Error opening output file: %s\n", acOutFile);
//      return -4;
//   }
//
//   // Output first header record
//   if (iFirstRec > 0)
//      memset(acBuf, '9', iRecLen);
//
//   // Init variables
//   lRollSkip=iNoMatch=iBadCity=iBadSuffix=0;
//   acLegal[0] = 0;
//   iMaxLegal = 0;
//   lMatchCChr = 0;
//
//   // Merge loop
//   pTmp = acRollRec;
//   while (!feof(fdRoll))
//   {
//      iRet = fread(acRollRec, 1, iRollLen, fdRoll);
//      if (iRet != iRollLen)
//         break;
//
//      if (acRollRec[OFF_LIEN_PRIMARY_PARCEL_NO] == ' ')
//      {
//         lRollSkip++;
//         continue;
//      }
//
//      if (memcmp(&acRollRec[OFF_LIEN_PRIMARY_PARCEL_NO], acBuf, SIZ_LIEN_PRIMARY_PARCEL_NO))
//      {
//         // Ignore record without APN
//         if (acBuf[0] > ' ')
//         {
//            // Update legal
//            if (acLegal[0] > ' ')
//            {
//               iRet = updateLegal(acBuf, acLegal);
//               if (iRet > iMaxLegal)
//                  iMaxLegal = iRet;
//               acLegal[0] = 0;
//            }
//
//            iRet = atoin((char *)&acBuf[OFF_TRANSFER_DT], 8);
//            if (iRet > lLastRecDate && iRet < lToday)
//               lLastRecDate = iRet;
//
//            // Check for bad chars
//            //if (iRet = replUnPrtChar(acBuf, ' ', iRecLen))
//            //   LogMsg0("*** Bad character in at %d APN=%.12s", iRet, acBuf);
//
//            bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
//            if (!bRet)
//            {
//               LogMsg("Error writing to output file at record %d\n", lCnt);
//               lRet = WRITE_ERR;
//               break;
//            }
//            lLDRRecCount++;
//         } else
//            lRollSkip++;
//
//         memset(acBuf, ' ', iRecLen);
//         acBuf[OFF_HO_FL] = '2';   // N
//      }
//
//      if (!(++lCnt % 1000))
//         printf("\r%u", lCnt);
//
//      // Create new R01 record
//      iRet = Riv_MergeLien(acBuf, acRollRec, iRollLen, acLegal);
//      if (iRet == 11)
//      {
//         // Merge Char
//         if (fdChar)
//            iRet = Riv_MergeStdChar(acBuf);
//      }
//   }
//
//   // Close files
//   if (fdChar)
//      fclose(fdChar);
//   if (fdRoll)
//      fclose(fdRoll);
//   if (fhOut)
//      CloseHandle(fhOut);
//
//   LogMsg("Total input records:        %u", lCnt);
//   LogMsg("Total output records:       %u", lLDRRecCount);
//   LogMsg("Total records skipped:      %u", lRollSkip);
//   LogMsg("Total char rec matched:     %u", lCharMatch);
//   LogMsg("Total char rec skipped:     %u", lCharSkip);
//   LogMsg("Total bad-city records:     %u", iBadCity);
//   LogMsg("Total bad-suffix records:   %u\n", iBadSuffix);
//
//   printf("\nTotal output records: %u\n", lCnt);
//
//   lRecCnt = lLDRRecCount;
//   return 0;
//}

/******************************************************************************
 *
 * LDR 2019
 *
 ******************************************************************************/

//int Riv_MergeLien1(char *pOutbuf, char *pRollRec)
//{
//   char     acTmp[256], acTmp1[64], acApn[16];
//   long     lTmp;
//   int      iRet, iTmp;
//
//   // Parse input rec
//   iTokens = ParseStringNQ(pRollRec, ',', MAX_FLD_TOKEN, apTokens);
//   if (iTokens < L_CAMEFROM)
//   {
//      LogMsg("***** Error: bad input record for APN=%s (%d)", apTokens[L_PIN], iTokens);
//      return -1;
//   }
//
//   // Ignore non-taxable parcels
//   if (*apTokens[L_ISTAXABLE] == 'N')
//      return 1;
//
//   iRet = strlen(apTokens[L_PIN]);
//   if (iRet != iApnLen)
//   {
//      if (bDebug)
//         LogMsg("*** Drop bad parcel: %s", apTokens[L_PIN]);
//      return -1;
//   } else
//      strcpy(acApn, apTokens[L_PIN]);
//
//   // Clear output buffer
//   memset(pOutbuf, ' ', iRecLen);
//
//   // Start copying data
//   memcpy(pOutbuf, acApn, iApnLen);
//
//   // Format APN
//   iRet = formatApn(acApn, acTmp, &myCounty);
//   memcpy(pOutbuf+OFF_APN_D, acTmp, iRet);
//
//   // Create MapLink and output new record
//   iRet = formatMapLink(acApn, acTmp, &myCounty);
//   memcpy(pOutbuf+OFF_MAPLINK, acTmp, iRet);
//
//   // Create index map link
//   if (getIndexPage(acTmp, acTmp1, &myCounty))
//      memcpy(pOutbuf+OFF_IMAPLINK, acTmp1, iRet);
//
//   // Copy ALT_APN
//   memcpy(pOutbuf+OFF_ALT_APN, apTokens[L_GEO], strlen(apTokens[L_GEO]));
//
//   // County code
//   memcpy(pOutbuf+OFF_CO_NUM, "33RIV", 5);
//
//   // status
//   *(pOutbuf+OFF_STATUS) = 'A';
//
//   // TRA
//   memcpy(pOutbuf+OFF_TRA, apTokens[L_TRA], strlen(apTokens[L_TRA]));
//
//   // Year assessed
//   memcpy(pOutbuf+OFF_YR_ASSD, myCounty.acYearAssd, 4);
//
//   // Land
//   long lLand = atoi(apTokens[L_LAND]);
//   if (lLand > 0)
//   {
//      sprintf(acTmp, "%*u", SIZ_LAND, lLand);
//      memcpy(pOutbuf+OFF_LAND, acTmp, SIZ_LAND);
//   }
//
//   // Improve
//   long lImpr = atoi(apTokens[L_IMPROVEMENTS]);
//   if (lImpr > 0)
//   {
//      sprintf(acTmp, "%*u", SIZ_IMPR, lImpr);
//      memcpy(pOutbuf+OFF_IMPR, acTmp, SIZ_IMPR);
//   }
//
//   // Other value: 
//   long lFixtr = atoi(apTokens[L_TRADEFIXTURESAMOUNT]);
//   long lFixtRP= atoi(apTokens[L_LIVINGIMPROVEMENTS]);
//   long lPers  = atoi(apTokens[L_PERSONALVALUE]);
//   lTmp = lFixtr+lPers+lFixtRP;
//   if (lTmp > 0)
//   {
//      sprintf(acTmp, "%*u", SIZ_OTHER, lTmp);
//      memcpy(pOutbuf+OFF_OTHER, acTmp, SIZ_OTHER);
//
//      if (lFixtr > 0)
//      {
//         sprintf(acTmp, "%d         ", lFixtr);
//         memcpy(pOutbuf+OFF_FIXTR, acTmp, SIZ_FIXTR);
//      }
//      if (lFixtRP > 0)
//      {
//         sprintf(acTmp, "%d         ", lFixtRP);
//         memcpy(pOutbuf+OFF_FIXTR_RP, acTmp, SIZ_FIXTR_RP);
//      }
//      if (lPers > 0)
//      {
//         sprintf(acTmp, "%d         ", lPers);
//         memcpy(pOutbuf+OFF_PERSPROP, acTmp, SIZ_PERSPROP);
//      }
//   }
//
//   // Gross total
//   long lGross = lTmp+lLand+lImpr;
//   if (lGross > 0)
//   {
//      sprintf(acTmp, "%*u", SIZ_GROSS, lGross);
//      memcpy(pOutbuf+OFF_GROSS, acTmp, SIZ_GROSS);
//   }
//
//   // Ratio
//   if (lImpr > 0)
//   {
//      sprintf(acTmp, "%*u", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
//      memcpy(pOutbuf+OFF_RATIO, acTmp, SIZ_RATIO);
//   }
//
//#ifdef _DEBUG
//   //if (!memcmp(pOutbuf, "103051035", 9))
//   //   iTmp = 0;
//#endif
//
//   // Exemption
//   long lExe1 = atol(apTokens[L_HOX]);
//   long lExe2 = atol(apTokens[L_OTHEREXEMPTION]);
//   long lTotalExe = lExe1+lExe2;
//   if (lTotalExe > 0)
//   {
//      if (lTotalExe > lGross)
//         lTotalExe = lGross;
//      sprintf(acTmp, "%*u", SIZ_EXE_TOTAL, lTotalExe);
//      memcpy(pOutbuf+OFF_EXE_TOTAL, acTmp, SIZ_EXE_TOTAL);
//   }  
//
//   if (lExe1 > 0)
//   {
//      *(pOutbuf+OFF_HO_FL) = '1';      // 'Y'
//      memcpy(pOutbuf+OFF_EXE_CD1, "HO", 2);
//   } else
//   {
//      *(pOutbuf+OFF_HO_FL) = '2';      // 'N'
//      iTmp = OFF_EXE_CD1;
//
//      lTmp = atol(apTokens[L_VETERANSEXEMPTION]);
//      if (lTmp > 1)
//      {
//         memcpy(pOutbuf+iTmp, "VE", 2);
//         iTmp++;
//      }
//      lTmp = atol(apTokens[L_DISABLEDVETERANSEXEMPTION]);
//      if (lTmp > 1)
//      {
//         memcpy(pOutbuf+iTmp, "DV", 2);
//         iTmp++;
//      }
//      lTmp = atol(apTokens[L_CHURCHEXEMPTION]);
//      if (lTmp > 1)
//      {
//         memcpy(pOutbuf+iTmp, "CH", 2);
//         iTmp++;
//      }
//      lTmp = atol(apTokens[L_RELIGIOUSEXEMPTION]);
//      if (lTmp > 1 && iTmp < 3)
//      {
//         memcpy(pOutbuf+iTmp, "RE", 2);
//         iTmp++;
//      }
//      lTmp = atol(apTokens[L_CEMETERYEXEMPTION]);
//      if (lTmp > 1 && iTmp < 3)
//      {
//         memcpy(pOutbuf+iTmp, "CE", 2);
//         iTmp++;
//      }
//      lTmp = atol(apTokens[L_PUBLICSCHOOLEXEMPTION]);
//      if (lTmp > 1 && iTmp < 3)
//      {
//         memcpy(pOutbuf+iTmp, "PS", 2);
//         iTmp++;
//      }
//      lTmp = atol(apTokens[L_PUBLICLIBRARYEXEMPTION]);
//      if (lTmp > 1 && iTmp < 3)
//      {
//         memcpy(pOutbuf+iTmp, "PL", 2);
//         iTmp++;
//      }
//      lTmp = atol(apTokens[L_PUBLICMUSEUMEXEMPTION]);
//      if (lTmp > 1 && iTmp < 3)
//      {
//         memcpy(pOutbuf+iTmp, "PM", 2);
//         iTmp++;
//      }
//      lTmp = atol(apTokens[L_WELFARECOLLEGEEXEMPTION]);
//      if (lTmp > 1 && iTmp < 3)
//      {
//         memcpy(pOutbuf+iTmp, "WC", 2);
//         iTmp++;
//      }
//      lTmp = atol(apTokens[L_WELFAREHOSPITALEXEMPTION]);
//      if (lTmp > 1 && iTmp < 3)
//      {
//         memcpy(pOutbuf+iTmp, "WH", 2);
//         iTmp++;
//      }
//      lTmp = atol(apTokens[L_WPP_SCHOOLEXEMPTION]);
//      if (lTmp > 1 && iTmp < 3)
//      {
//         memcpy(pOutbuf+iTmp, "WS", 2);
//         iTmp++;
//      }
//      lTmp = atol(apTokens[L_WC_RELIGIOUSEXEMPTION]);
//      if (lTmp > 1 && iTmp < 3)
//      {
//         memcpy(pOutbuf+iTmp, "WR", 2);
//         iTmp++;
//      }
//   }
//
//   // Set full exemption flag 
//   if (lGross == lTotalExe)
//   {
//      *(pOutbuf+OFF_FULL_EXEMPT) = 'Y';
//   } else if (isdigit(*apTokens[L_TAXABILITYCODE]))
//   {
//      // For 2020, there is no tax code.  The new field ISTAXABLE contains Y/N
//      vmemcpy(pOutbuf+OFF_TAX_CODE, apTokens[L_TAXABILITYCODE], SIZ_TAX_CODE);
//      if (*apTokens[L_TAXABILITYCODE] < '4')
//         *(pOutbuf+OFF_FULL_EXEMPT) = 'Y';
//   }
//
//   // Lot acreage
//   double dAcreage = atof(apTokens[L_ACREAGEAMOUNT]);
//   if (dAcreage > 0.0)
//   {
//      sprintf(acTmp, "%*u", SIZ_LOT_ACRES, (long)(dAcreage*1000));
//      memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);
//      lTmp = (long)(dAcreage * SQFT_PER_ACRE);
//      sprintf(acTmp, "%*u", SIZ_LOT_SQFT, lTmp);
//      memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);
//   }
//
//   // Block/Lot/Track
//   if (!_memicmp(apTokens[L_LOTTYPE], "LOT", 3))
//      vmemcpy(pOutbuf+OFF_LOT, apTokens[L_LOT], SIZ_LOT);
//
//   // Legal
//   _strupr(apTokens[L_ASSESSMENTDESCRIPTION]);
//   remJSChar(apTokens[L_ASSESSMENTDESCRIPTION]);
//   iRet = updateLegal(pOutbuf, apTokens[L_ASSESSMENTDESCRIPTION]);
//   if (iRet > iMaxLegal)
//      iMaxLegal = iRet;
//
//   // Owner
//   iRet = replUnPrtChar(apTokens[L_LEGALPARTY1]);
//   if (iRet > 0)
//      blankRem(apTokens[L_LEGALPARTY1]);
//   Riv_MergeLOwner1(pOutbuf, apTokens[L_LEGALPARTY1], 1);
//   if (*apTokens[L_LEGALPARTY2] > ' ')
//      Riv_MergeLOwner1(pOutbuf, apTokens[L_LEGALPARTY2], 2);
//
//   // Mailing
//   Riv_MergeMailing(pOutbuf);
//
//   // Situs - always populate situs after mailing
//   Riv_MergeSitus(pOutbuf);
//
//   // DocNum/DocDate
//   //lTmp = atol(apTokens[L_CONVEYANCEYEAR]);
//   //if (lTmp > 1900 && *apTokens[L_CONVENYANCENUMBER] > '0')
//   //{
//   //   vmemcpy(pOutbuf+OFF_TRANSFER_DOC, apTokens[L_CONVENYANCENUMBER]+5, 7);
//   //   iTmp = sprintf(acTmp, "%s%s00", apTokens[L_CONVEYANCEYEAR], apTokens[L_CONVEYANCEMONTH]);
//   //   memcpy(pOutbuf+OFF_TRANSFER_DT, acTmp, iTmp);
//   //}
//
//   // SetTaxcode, Prop8 flag, FullExe flag
//   //iTmp = updateTaxCode(pOutbuf, apTokens[L_TAXABILITY], true, true);
//
//   return 0;
//}

/******************************************************************************
 *
 * LDR 2020
 *
 ******************************************************************************/

int Riv_MergeLien2(char *pOutbuf, char *pRollRec, char cNonTax)
{
   char     acTmp[256], acTmp1[64], acApn[16];
   LONGLONG lTmp;
   int      iRet, iTmp, iIdx=0;

   // Parse input rec
   iTokens = ParseStringNQ(pRollRec, cLdrSep, MAX_FLD_TOKEN, apTokens);
   if (iTokens < L_COLS)
   {
      LogMsg("***** Error: bad input record for APN=%s (%d)", apTokens[L_PIN], iTokens);
      return -1;
   }

   // Ignore non-taxable parcels
   if (cNonTax == 'N' && *apTokens[L_ISTAXABLE] == 'N')
      return -1;

   //iRet = strlen(apTokens[L_PIN]);
   //if (iRet != iApnLen)
   //{
   //   if (bDebug)
   //      LogMsg("*** Drop bad parcel: %s", apTokens[L_PIN]);
   //   return -1;
   //}

   // Ignore unsecured parcel 000-008
   if (memcmp(apTokens[L_PIN], "008", 3) <= 0)
      return 1;

   // Clear output buffer
   memset(pOutbuf, ' ', iRecLen);

   if (*(apTokens[L_PIN]+9) == 'X')
   {
      memcpy(acApn, apTokens[L_PIN], 9);
      strcpy(&acApn[9], apTokens[L_PIN]+10);
      iTmp = sprintf(acTmp, "%.3s-%.3s-%.3s-%s", acApn, &acApn[3], &acApn[6], &acApn[9]); 
      iApnLen = 12;
   } else
   {
      strcpy(acApn, apTokens[L_PIN]);
      iTmp = sprintf(acTmp, "%.3s-%.3s-%s", acApn, &acApn[3], &acApn[6]); 
      iApnLen = 9;
   }

   // Start copying data
   memcpy(pOutbuf, acApn, strlen(acApn));

   // Format APN
   //iRet = formatApn(acApn, acTmp, &myCounty);
   memcpy(pOutbuf+OFF_APN_D, acTmp, iTmp);

   // Create MapLink and output new record
   iTmp = formatMapLink(acApn, acTmp, &myCounty);
   memcpy(pOutbuf+OFF_MAPLINK, acTmp, iTmp);

   // Create index map link
   if (getIndexPage(acTmp, acTmp1, &myCounty))
      memcpy(pOutbuf+OFF_IMAPLINK, acTmp1, strlen(acTmp1));

   // Copy ALT_APN
   memcpy(pOutbuf+OFF_ALT_APN, apTokens[L_GEO], strlen(apTokens[L_GEO]));

   // County code
   memcpy(pOutbuf+OFF_CO_NUM, "33RIVA", 6);

   // status

   // TRA
   memcpy(pOutbuf+OFF_TRA, apTokens[L_TRA], strlen(apTokens[L_TRA]));

   // Year assessed
   memcpy(pOutbuf+OFF_YR_ASSD, myCounty.acYearAssd, 4);

   // Land
   long lLand = atoi(apTokens[L_LAND]);
   if (lLand > 0)
   {
      sprintf(acTmp, "%*u", SIZ_LAND, lLand);
      memcpy(pOutbuf+OFF_LAND, acTmp, SIZ_LAND);
   }

   // Improve
   long lImpr = atoi(apTokens[L_IMPROVEMENTS]);
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*u", SIZ_IMPR, lImpr);
      memcpy(pOutbuf+OFF_IMPR, acTmp, SIZ_IMPR);
   }

   // Other value: 
   long lFixtr = atoi(apTokens[L_TRADEFIXTURESAMOUNT]);
   long lPers  = atoi(apTokens[L_PERSONALVALUE]);
   long lLivImpr= atoi(apTokens[L_LIVINGIMPROVEMENTS]);
   lTmp = lFixtr+lPers+lLivImpr;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*u", SIZ_OTHER, lTmp);
      memcpy(pOutbuf+OFF_OTHER, acTmp, SIZ_OTHER);

      if (lFixtr > 0)
      {
         sprintf(acTmp, "%d         ", lFixtr);
         memcpy(pOutbuf+OFF_FIXTR, acTmp, SIZ_FIXTR);
      }
      if (lLivImpr > 0)
      {
         sprintf(acTmp, "%d         ", lLivImpr);
         memcpy(pOutbuf+OFF_OTH_IMPR, acTmp, SIZ_OTH_IMPR);
      }
      if (lPers > 0)
      {
         sprintf(acTmp, "%d         ", lPers);
         memcpy(pOutbuf+OFF_PERSPROP, acTmp, SIZ_PERSPROP);
      }
   }

   // Gross total
   LONGLONG lGross = lTmp+lLand+lImpr;
   if (lGross > 0)
   {
      sprintf(acTmp, "%*u", SIZ_GROSS, lGross);
      memcpy(pOutbuf+OFF_GROSS, acTmp, SIZ_GROSS);
   }

   // Ratio
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*u", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
      memcpy(pOutbuf+OFF_RATIO, acTmp, SIZ_RATIO);
   }

   // Exemption
   long lHO_Exe = atol(apTokens[L_HOX]);
   ULONG lOE_Exe = atol(apTokens[L_OTHEREXEMPTION]);
   ULONG lVE_Exe = atol(apTokens[L_VETERANSEXEMPTION]);
   ULONG lDV_Exe = atol(apTokens[L_DISABLEDVETERANSEXEMPTION]);
   ULONG lCH_Exe = atol(apTokens[L_CHURCHEXEMPTION]);
   ULONG lRE_Exe = atol(apTokens[L_RELIGIOUSEXEMPTION]);
   ULONG lCE_Exe = atol(apTokens[L_CEMETERYEXEMPTION]);
   ULONG lPS_Exe = atol(apTokens[L_PUBLICSCHOOLEXEMPTION]);
   ULONG lPL_Exe = atol(apTokens[L_PUBLICLIBRARYEXEMPTION]);
   ULONG lPM_Exe = atol(apTokens[L_PUBLICMUSEUMEXEMPTION]);
   ULONG lWC_Exe = atol(apTokens[L_WELFARECOLLEGEEXEMPTION]);
   ULONG lWH_Exe = atol(apTokens[L_WELFAREHOSPITALEXEMPTION]);
   ULONG lWS_Exe = atol(apTokens[L_WPP_SCHOOLEXEMPTION]);
   ULONG lWR_Exe = atol(apTokens[L_WC_RELIGIOUSEXEMPTION]);
   LONGLONG lTotalExe = lHO_Exe+lOE_Exe+lVE_Exe+lDV_Exe+lCH_Exe+lRE_Exe+lCE_Exe+lPS_Exe+lPL_Exe+lPM_Exe+
      lWC_Exe+lWH_Exe+lWS_Exe+lWR_Exe;
   if (lTotalExe > 0)
   {
      if (lTotalExe > lGross)
         lTotalExe = lGross;
      sprintf(acTmp, "%*u", SIZ_EXE_TOTAL, lTotalExe);
      memcpy(pOutbuf+OFF_EXE_TOTAL, acTmp, SIZ_EXE_TOTAL);
   }  

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "138470010", 9))
   //   iTmp = 0;
   //if (lOE_Exe > 0 && lOE_Exe != lTotalExe)
   //   iTmp = 0;
#endif

   iIdx = OFF_EXE_CD1;
   if (lHO_Exe > 0)
   {
      *(pOutbuf+OFF_HO_FL) = '1';      // 'Y'
      memcpy(pOutbuf+OFF_EXE_CD1, "HO", 2);
      iIdx = OFF_EXE_CD2;
      iTmp = 1;
   } else
   {
      *(pOutbuf+OFF_HO_FL) = '2';      // 'N'
      iTmp = 0;
   }
   if (lTotalExe > lHO_Exe)
   {
      if (lVE_Exe > 1)
      {
         memcpy(pOutbuf+iIdx, "VE", 2);
         iTmp++;
         iIdx += SIZ_EXE_CD1;
      }
      if (lDV_Exe > 1)
      {
         memcpy(pOutbuf+iIdx, "DV", 2);
         iTmp++;
         iIdx += SIZ_EXE_CD1;
      }
      if (lCE_Exe > 1)
      {
         memcpy(pOutbuf+iIdx, "CH", 2);
         iTmp++;
         iIdx += SIZ_EXE_CD1;
      }
      if (lRE_Exe > 1 && iTmp < 3)
      {
         memcpy(pOutbuf+iIdx, "RE", 2);
         iTmp++;
         iIdx += SIZ_EXE_CD1;
      }
      if (lCE_Exe > 1 && iTmp < 3)
      {
         memcpy(pOutbuf+iIdx, "CE", 2);
         iTmp++;
         iIdx += SIZ_EXE_CD1;
      }
      if (lPS_Exe > 1 && iTmp < 3)
      {
         memcpy(pOutbuf+iIdx, "PS", 2);
         iTmp++;
         iIdx += SIZ_EXE_CD1;
      }
      if (lPL_Exe > 1 && iTmp < 3)
      {
         memcpy(pOutbuf+iIdx, "PL", 2);
         iTmp++;
         iIdx += SIZ_EXE_CD1;
      }
      if (lPM_Exe > 1 && iTmp < 3)
      {
         memcpy(pOutbuf+iIdx, "PM", 2);
         iTmp++;
         iIdx += SIZ_EXE_CD1;
      }
      if (lWC_Exe > 1 && iTmp < 3)
      {
         memcpy(pOutbuf+iIdx, "WC", 2);
         iTmp++;
         iIdx += SIZ_EXE_CD1;
      }
      if (lWH_Exe > 1 && iTmp < 3)
      {
         memcpy(pOutbuf+iIdx, "WH", 2);
         iTmp++;
         iIdx += SIZ_EXE_CD1;
      }
      if (lWS_Exe > 1 && iTmp < 3)
      {
         memcpy(pOutbuf+iIdx, "WS", 2);
         iTmp++;
         iIdx += SIZ_EXE_CD1;
      }
      if (lWR_Exe > 1 && iTmp < 3)
      {
         memcpy(pOutbuf+iIdx, "WR", 2);
         iTmp++;
         iIdx += SIZ_EXE_CD1;
      }
      if (lOE_Exe > 1 && iTmp < 3)
      {
         memcpy(pOutbuf+iIdx, "OE", 2);
         iTmp++;
      }

      if (iTmp > 3)
         LogMsg("***** Too many ExeCode (%d). This may overwrite other field. Please contact programmer", iTmp);
   }

#ifdef _DEBUG
   //if (iTmp > 3)
   //   iTmp = 0;
#endif

   // Create exemption type
   makeExeType(pOutbuf+OFF_EXE_TYPE, pOutbuf+OFF_EXE_CD1, pOutbuf+OFF_EXE_CD2, pOutbuf+OFF_EXE_CD3, (IDX_TBL4 *)&RIV_Exemption);

   // Populate per bulk customer request 07/20/2022
   lTmp = atol(apTokens[L_TENPERCENTASSESSEDPENALTY]);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*u", SIZ_LATE_PENALTY, lTmp);
      memcpy(pOutbuf+OFF_LATE_PENALTY, acTmp, SIZ_LATE_PENALTY);
   }

   // Set full exemption flag 
   if (lGross == lTotalExe)
   {
      *(pOutbuf+OFF_FULL_EXEMPT) = 'Y';
   } else if (isdigit(*apTokens[L_TAXABILITYCODE]))
   {
      // For 2020, there is no tax code.  The new field ISTAXABLE contains Y/N
      vmemcpy(pOutbuf+OFF_TAX_CODE, apTokens[L_TAXABILITYCODE], SIZ_TAX_CODE);
      if (*apTokens[L_TAXABILITYCODE] < '4')
         *(pOutbuf+OFF_FULL_EXEMPT) = 'Y';
   }

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "009000009", 9) )
   //   iTmp = 0;
#endif

   // UseCode - LDR 2020
   if (*apTokens[L_USECODE] > ' ')
   {
      strcpy(acTmp, apTokens[L_USECODE]);
      iTmp = iTrim(acTmp);
      iTmp = remChar(acTmp, '-', iTmp);
      vmemcpy(pOutbuf+OFF_USE_CO, acTmp, SIZ_USE_CO, iTmp);
      vmemcpy(pOutbuf+OFF_USE_COX, apTokens[L_USECODE], SIZ_USE_COX); // Temporary keep it here
      updateStdUse(pOutbuf+OFF_USE_STD, pOutbuf+OFF_USE_CO, SIZ_USE_CO, pOutbuf);
   } else
      memcpy(pOutbuf+OFF_USE_STD, USE_UNASGN, SIZ_USE_STD);

   // Lot acreage
   double dAcreage = atof(apTokens[L_ACREAGEAMOUNT]);
   if (dAcreage > 0.0)
   {
      sprintf(acTmp, "%*u", SIZ_LOT_ACRES, (long)(dAcreage*1000));
      memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);
      lTmp = (ULONG)(dAcreage * SQFT_PER_ACRE);
      sprintf(acTmp, "%*u", SIZ_LOT_SQFT, lTmp);
      memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);
   }

   // Block/Lot/Track
   if (!_memicmp(apTokens[L_LOTTYPE], "LOT", 3))
      vmemcpy(pOutbuf+OFF_LOT, apTokens[L_LOT], SIZ_LOT);

   // Legal
   _strupr(apTokens[L_ASSESSMENTDESCRIPTION]);
   remJSChar(apTokens[L_ASSESSMENTDESCRIPTION]);
   iRet = updateLegal(pOutbuf, apTokens[L_ASSESSMENTDESCRIPTION]);
   if (iRet > iMaxLegal)
      iMaxLegal = iRet;

   // Owner
   iRet = replUnPrtChar(apTokens[L_LEGALPARTY1]);
   if (iRet > 0)
      blankRem(apTokens[L_LEGALPARTY1]);
   Riv_MergeLOwner1(pOutbuf, apTokens[L_LEGALPARTY1], 1);
   if (*apTokens[L_LEGALPARTY2] > ' ')
      Riv_MergeLOwner1(pOutbuf, apTokens[L_LEGALPARTY2], 2);

   // Mailing
   Riv_MergeMailing(pOutbuf);

   // Situs - always populate situs after mailing
   Riv_MergeSitus(pOutbuf);

   // DocNum/DocDate
   //lTmp = atol(apTokens[L_CONVEYANCEYEAR]);
   //if (lTmp > 1900 && *apTokens[L_CONVENYANCENUMBER] > '0')
   //{
   //   vmemcpy(pOutbuf+OFF_TRANSFER_DOC, apTokens[L_CONVENYANCENUMBER]+5, 7);
   //   iTmp = sprintf(acTmp, "%s%s00", apTokens[L_CONVEYANCEYEAR], apTokens[L_CONVEYANCEMONTH]);
   //   memcpy(pOutbuf+OFF_TRANSFER_DT, acTmp, iTmp);
   //}

   // SetTaxcode, Prop8 flag, FullExe flag
   //iTmp = updateTaxCode(pOutbuf, apTokens[L_TAXABILITY], true, true);

   // 02/20/2021
   if (*apTokens[R_ISTAXABLE] == 'N')
      *(pOutbuf+OFF_FULL_EXEMPT) = 'Y';

   return 0;
}

/******************************************************************************
 *
 * LDR 2021-
 * Extract other values and apply to main record
 *
 ******************************************************************************/

int Riv_ExtractOtherValues(char *pOutbuf, char *pRollRec)
{
   char     acTmp[256];
   LONGLONG lTmp;

   // Parse input rec
   iTokens = ParseStringNQ(pRollRec, ',', MAX_FLD_TOKEN, apTokens);
   if (iTokens < L_COLS)
   {
      LogMsg("***** Error: bad input record for APN=%s (%d)", apTokens[L_PIN], iTokens);
      return -1;
   }

   ULONG lLand  = atoln(pOutbuf+OFF_LAND, SIZ_LAND);
   ULONG lImpr  = atoln(pOutbuf+OFF_IMPR, SIZ_IMPR);
   ULONG lLivImpr= atoln(pOutbuf+OFF_OTH_IMPR, SIZ_OTH_IMPR);
#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "959080032", 9))
   //   lTmp = 0;
#endif

   // Other value: 
   ULONG lFixtr = atoi(apTokens[L_TRADEFIXTURESAMOUNT]);
   ULONG lPers  = atoi(apTokens[L_PERSONALVALUE]);
   lTmp = lFixtr+lPers+lLivImpr;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*u", SIZ_OTHER, lTmp);
      memcpy(pOutbuf+OFF_OTHER, acTmp, SIZ_OTHER);

      if (lFixtr > 0)
      {
         sprintf(acTmp, "%d         ", lFixtr);
         memcpy(pOutbuf+OFF_FIXTR, acTmp, SIZ_FIXTR);
      }
      if (lPers > 0)
      {
         sprintf(acTmp, "%d         ", lPers);
         memcpy(pOutbuf+OFF_PERSPROP, acTmp, SIZ_PERSPROP);
      }
   }

   // Gross total
   LONGLONG lGross = lTmp+lLand+lImpr;
   if (lGross > 0)
   {
      sprintf(acTmp, "%*u", SIZ_GROSS, lGross);
      memcpy(pOutbuf+OFF_GROSS, acTmp, SIZ_GROSS);
   }

   // Ratio
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*u", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
      memcpy(pOutbuf+OFF_RATIO, acTmp, SIZ_RATIO);
   }

   ULONG lOE_Exe = atol(apTokens[L_OTHEREXEMPTION]);
   ULONG lVE_Exe = atol(apTokens[L_VETERANSEXEMPTION]);
   ULONG lDV_Exe = atol(apTokens[L_DISABLEDVETERANSEXEMPTION]);
   ULONG lCH_Exe = atol(apTokens[L_CHURCHEXEMPTION]);
   ULONG lRE_Exe = atol(apTokens[L_RELIGIOUSEXEMPTION]);
   ULONG lCE_Exe = atol(apTokens[L_CEMETERYEXEMPTION]);
   ULONG lPS_Exe = atol(apTokens[L_PUBLICSCHOOLEXEMPTION]);
   ULONG lPL_Exe = atol(apTokens[L_PUBLICLIBRARYEXEMPTION]);
   ULONG lPM_Exe = atol(apTokens[L_PUBLICMUSEUMEXEMPTION]);
   ULONG lWC_Exe = atol(apTokens[L_WELFARECOLLEGEEXEMPTION]);
   ULONG lWH_Exe = atol(apTokens[L_WELFAREHOSPITALEXEMPTION]);
   ULONG lWS_Exe = atol(apTokens[L_WPP_SCHOOLEXEMPTION]);
   ULONG lWR_Exe = atol(apTokens[L_WC_RELIGIOUSEXEMPTION]);
   LONGLONG lTotalExe = lOE_Exe+lVE_Exe+lDV_Exe+lCH_Exe+lRE_Exe+lCE_Exe+lPS_Exe+lPL_Exe+lPM_Exe+
      lWC_Exe+lWH_Exe+lWS_Exe+lWR_Exe+atoln(pOutbuf+OFF_EXE_TOTAL, SIZ_EXE_TOTAL);
   if (lTotalExe > 0)
   {
      if (lTotalExe > lGross)
         lTotalExe = lGross;
      sprintf(acTmp, "%*u", SIZ_EXE_TOTAL, lTotalExe);
      memcpy(pOutbuf+OFF_EXE_TOTAL, acTmp, SIZ_EXE_TOTAL);
   }  

   // Populate per bulk customer request 07/20/2022
   lTmp = atol(apTokens[L_TENPERCENTASSESSEDPENALTY]);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*u", SIZ_LATE_PENALTY, lTmp);
      memcpy(pOutbuf+OFF_LATE_PENALTY, acTmp, SIZ_LATE_PENALTY);
   }

   return 0;
}

/***************************** Riv_MergeOldInfo *****************************
 *
 * Copy data from old R01 file to new one since new roll doesn't include (2019)
 * or data is bad.*
 *
 ****************************************************************************/

int Riv_MergeOldInfo(char *pOutbuf, FILE *fd)
{
   static   char acRec[MAX_RECSIZE];
   static   int iRead=0;
   int      iRet=0, iTmp;

   if (!iRead)
   {
      // Skip header
      iRead = fread(acRec, 1, iRecLen, fd);
      // Get first record
      iRead = fread(acRec, 1, iRecLen, fd);
   }

   do 
   {
      if (iRead < iRecLen)
      {
         fclose(fd);
         fd = NULL;
         return 1;      // EOF 
      }

      iTmp = memcmp(pOutbuf, acRec, iApnLen);
      if (iTmp > 0)
      {
         if (bDebug)
            LogMsg0("Skip Roll rec: %.10s", acRec);
         iRead = fread(acRec, 1, iRecLen, fd);
         lRollSkip++;
      }
   } while (iTmp > 0);

   // If not match, return
   if (iTmp)
      return 1;

   // Update Usecode
   //if (*(pOutbuf+OFF_USE_CO) == ' ')
   //{
   //   memcpy(pOutbuf+OFF_USE_CO, &acRec[OFF_USE_CO], SIZ_USE_CO);
   //   memcpy(pOutbuf+OFF_USE_STD, &acRec[OFF_USE_STD], SIZ_USE_STD);
   //}

   // Lot acreage
   iTmp = atoin(&acRec[OFF_LOT_SQFT], SIZ_LOT_SQFT);
   if (*(pOutbuf+OFF_LOT_SQFT+8) == ' ' && iTmp > 100)
   {
      memcpy(pOutbuf+OFF_LOT_ACRES, &acRec[OFF_LOT_ACRES], SIZ_LOT_ACRES);
      memcpy(pOutbuf+OFF_LOT_SQFT, &acRec[OFF_LOT_SQFT], SIZ_LOT_SQFT);
   }

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "486482012", 9))
   //   iTmp = 0;
#endif

   // Zoning
   if (*(pOutbuf+OFF_ZONE) == ' ')
   {
      if (acRec[OFF_ZONE+1] == '|')
         acRec[OFF_ZONE+1] = '1';
      memcpy(pOutbuf+OFF_ZONE, &acRec[OFF_ZONE], SIZ_ZONE);
      if (*(pOutbuf+OFF_ZONE_X1) == ' ')
         vmemcpy(pOutbuf+OFF_ZONE_X1, &acRec[OFF_ZONE], SIZ_ZONE);
   }

   // Water
   if (*(pOutbuf+OFF_WATER) == ' ')
      *(pOutbuf+OFF_WATER) = acRec[OFF_WATER];

   // Sewer
   if (*(pOutbuf+OFF_SEWER) == ' ')
      *(pOutbuf+OFF_SEWER) = acRec[OFF_SEWER];

   // Gas

   // Electric

   // View
   if (*(pOutbuf+OFF_VIEW) == ' ')
      *(pOutbuf+OFF_VIEW) = acRec[OFF_VIEW];

   // Get next record
   iRead = fread(acRec, 1, iRecLen, fd);
   lRollMatch++;

   return 0;
}

/********************************* Riv_Load_LDR *****************************
 *
 * Load LDR using Certified2019AssessmentRoll.txt (2019)
 *
 ****************************************************************************/

int Riv_Load_LDR1(int iFirstRec, char cNonTax)
{
   char     acBuf[MAX_RECSIZE], acRollRec[MAX_RECSIZE], acLegal[1024], acLastApn[32];
   char     acOutFile[_MAX_PATH], *pTmp;

   HANDLE   fhOut;
   FILE     *fdLDR;
   
   int      iRet, iTmp, iDupApn=0;
   long     lRet=0, lCnt=0;
   DWORD    nBytesWritten;
   BOOL     bRet;

   LogMsg0("Loading LDR %d", lLienYear);

   // Open roll file
   LogMsg("Open Roll file %s", acRollFile);
   fdLDR = fopen(acRollFile, "r");
   if (fdLDR == NULL)
   {
      LogMsg("Error opening roll file: %s\n", acRollFile);
      return -1;
   }

   // Open Char file
   LogMsg("Open Char file %s", acCChrFile);
   fdChar = fopen(acCChrFile, "r");
   if (fdChar == NULL)
   {
      LogMsg("***** Error opening Char file: %s\n", acCChrFile);
      return -2;
   }

   // Open old raw file
   sprintf(acBuf, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "O01");
   LogMsg("Open input file %s", acBuf);
   fdRoll = fopen(acBuf, "rb");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening old roll file: %s\n", acBuf);
      return -3;
   }

   // Skip unsecured parcels
   //do
   //{
   //   pTmp = fgets(acRollRec, MAX_RECSIZE, fdLDR);
   //   iTmp = atoin(acRollRec, 3);
   //} while (pTmp && iTmp < 2);

   // Open Output file
   sprintf(acOutFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "R01");
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
             FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);
   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening output file: %s\n", acOutFile);
      return -4;
   }

   // Output first header record
   if (iFirstRec > 0)
   {
      memset(acBuf, '9', iRecLen);
      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
   }

   // Init variables
   lRollSkip=iNoMatch=iBadCity=iBadSuffix=0;
   acLegal[0] = 0;
   iMaxLegal = 0;
   lMatchCChr = 0;
   acLastApn[0] = 0;

   // Merge loop
   pTmp = acRollRec;
   while (!feof(fdLDR))
   {
      pTmp = fgets(acRollRec, MAX_RECSIZE, fdLDR);
      if (!pTmp)
         break;

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);

      iTmp = atoin(acRollRec, 3);
      if (acRollRec[0] > '9' || iTmp < 2)
         continue;

      if (!memcmp(acLastApn, acRollRec, iApnLen))
      {
         if (bDebug)
            LogMsg("*** Duplicate APN: %.*s", iApnLen, acRollRec);
         iDupApn++;
         continue;
      } else
         memcpy(acLastApn, acRollRec, iApnLen);

      // Create new R01 record
      iRet = Riv_MergeLien2(acBuf, acRollRec, cNonTax);
      if (!iRet)
      {
         // Merge UseCode and other fields from O01
         if (fdRoll)
            iRet = Riv_MergeOldInfo(acBuf, fdRoll);

         // Merge Char
         if (fdChar)
            iRet = Riv_MergeStdChar(acBuf);

         lLDRRecCount++;
         bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
         if (!bRet)
         {
            LogMsg("***** Error writing to output file at record %d\n", lCnt);
            lRet = WRITE_ERR;
            break;
         }
      }
   }

   // Close files
   if (fdRoll)
      fclose(fdRoll);
   if (fdChar)
      fclose(fdChar);
   if (fdLDR)
      fclose(fdLDR);
   if (fhOut)
      CloseHandle(fhOut);

   LogMsg("Total input records:        %u", lCnt);
   LogMsg("Total output records:       %u", lLDRRecCount);
   LogMsg("Total records skipped:      %u", lRollSkip);
   LogMsg("Total char matched:         %u", lCharMatch);
   LogMsg("Total char skipped:         %u", lCharSkip);
   LogMsg("Total roll matched:         %u", lCharMatch);
   LogMsg("Total roll skipped:         %u", lCharSkip);
   LogMsg("Total bad-city records:     %u", iBadCity);
   LogMsg("Total bad-suffix records:   %u\n", iBadSuffix);
   LogMsg("          Max Legal length: %u", iMaxLegal);
   LogMsg("       Duplicate APN count: %u\n", iDupApn);

   lRecCnt = lLDRRecCount;
   return 0;
}

/******************************** Riv_Load_LDR2 *****************************
 *
 * This version combines the BPP parcel to the main record and put the values in other values
 *
 ****************************************************************************/

int Riv_Load_LDR2(int iFirstRec, char cNonTax)
{
   char     acBuf[3072], acRollRec[MAX_RECSIZE], acBPPRec[MAX_RECSIZE], acLegal[1024], acLastApn[32];
   char     acOutFile[_MAX_PATH], acTmpFile[_MAX_PATH], *pTmp;

   HANDLE   fhOut;
   FILE     *fdLDR;
   
   int      iRet, iTmp, iDupApn=0;
   long     lRet=0, lCnt=0;
   DWORD    nBytesWritten;
   BOOL     bRet;

   LogMsg0("Loading LDR %d", lLienYear);

   // Open roll file
   LogMsg("Open Roll file %s", acRollFile);
   fdLDR = fopen(acRollFile, "r");
   if (fdLDR == NULL)
   {
      LogMsg("Error opening roll file: %s\n", acRollFile);
      return -1;
   }

   // Open Char file
   LogMsg("Open Char file %s", acCChrFile);
   fdChar = fopen(acCChrFile, "r");
   if (fdChar == NULL)
   {
      LogMsg("***** Error opening Char file: %s\n", acCChrFile);
      return -2;
   }

   // Open old raw file
   sprintf(acBuf, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "O01");
   LogMsg("Open input file %s", acBuf);
   fdRoll = fopen(acBuf, "rb");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening old roll file: %s\n", acBuf);
      return -3;
   }

   // Open Output file
   sprintf(acTmpFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "T01");
   sprintf(acOutFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "R01");
   LogMsg("Open output file %s", acTmpFile);
   fhOut = CreateFile(acTmpFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
             FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);
   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening output file: %s\n", acTmpFile);
      return -4;
   }

   // Output first header record
   if (iFirstRec > 0)
   {
      memset(acBuf, '9', iRecLen);
      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
   }

   // Init variables
   lRollSkip=iNoMatch=iBadCity=iBadSuffix=0;
   acLegal[0] = 0;
   iMaxLegal = 0;
   lMatchCChr = 0;
   acLastApn[0] = 0;

   // Merge loop
   pTmp = acRollRec;
   while (!feof(fdLDR))
   {
      pTmp = fgets(acRollRec, MAX_RECSIZE, fdLDR);
      if (!pTmp)
         break;

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);

      iTmp = atoin(acRollRec, 3);
      if (acRollRec[0] > '9' || iTmp < 2)
         continue;

#ifdef _DEBUG
      // 689040017
      //if (!memcmp(acRollRec, "689040017", 9))
      //   iTmp = 0;
#endif
      if (acRollRec[9] == 'B' || acRollRec[9] == 'b')
      {
         // Save BPP rec
         strcpy(acBPPRec, acRollRec);
      } else
      {
         // Create new R01 record
         iRet = Riv_MergeLien2(acBuf, acRollRec, cNonTax);
         if (!iRet)
         {
            // Merge UseCode and other fields from O01
            if (fdRoll)
               iRet = Riv_MergeOldInfo(acBuf, fdRoll);

            // Merge Char
            if (fdChar)
               iRet = Riv_MergeStdChar(acBuf);
         } else
            acBPPRec[0] = 0;                          // Clear BPP record since master record is not ised

         if (!memcmp(acBPPRec, acRollRec, iApnLen))
         {
            iRet = Riv_ExtractOtherValues(acBuf, acBPPRec);
            if (iRet < 0)
            {
               LogMsg("Bad record: %s", acBPPRec);
               iRet = 0;
            }
         }
         acBPPRec[0] = 0;

         if (iRet >= 0)
         {
            lLDRRecCount++;
            bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
            if (!bRet)
            {
               LogMsg("***** Error writing to output file at record %d\n", lCnt);
               lRet = WRITE_ERR;
               break;
            }
         }
      }
   }

   // Close files
   if (fdRoll)
      fclose(fdRoll);
   if (fdChar)
      fclose(fdChar);
   if (fdLDR)
      fclose(fdLDR);
   if (fhOut)
      CloseHandle(fhOut);

   // Resort output
   lLDRRecCount = sortFile(acTmpFile, acOutFile, "S(1,14,C,A) F(FIX,1900) B(1900,R)");

   LogMsg("Total input records:        %u", lCnt);
   LogMsg("Total output records:       %u", lLDRRecCount);
   LogMsg("Total records skipped:      %u", lRollSkip);
   LogMsg("Total char matched:         %u", lCharMatch);
   LogMsg("Total char skipped:         %u", lCharSkip);
   LogMsg("Total roll matched:         %u", lCharMatch);
   LogMsg("Total roll skipped:         %u", lCharSkip);
   LogMsg("Total bad-city records:     %u", iBadCity);
   LogMsg("Total bad-suffix records:   %u\n", iBadSuffix);
   LogMsg("          Max Legal length: %u", iMaxLegal);
   LogMsg("       Duplicate APN count: %u\n", iDupApn);

   lRecCnt = lLDRRecCount;
   return 0;
}

/**************************** Riv_FormatSaleRec ******************************
 *
 * Update history sale.  This function adds new sale record to cumulative file.
 *
 * Return 0 if successful, otherwise error
 *
 *****************************************************************************/

//int Riv_FormatSaleRec(char *pFmtSale, char *pSaleRec)
//{
//   RIV_SALE  *pSale = (RIV_SALE *)pSaleRec;
//   SCSAL_REC *pCSal = (SCSAL_REC *)pFmtSale;
//   long      lCurSaleDt, lTmp;
//   int       iRet;
//   char      acTmp[32];
//
//   // Inititalize
//   memset(pFmtSale, ' ', sizeof(SCSAL_REC));
//   iRet = -1;
//
//#ifdef _DEBUG
//   //if (!memcmp(pSale->Assessment_No, "009000273", iApnLen))
//   //   acTmp[0] = 0;
//#endif
//
//   // APN
//   lTmp = atoin(pSale->Assessment_No, iApnLen);
//   if (!lTmp)
//      return iRet;
//
//   lCurSaleDt = JulianToGregorian(pSale->Conveyance_Date, acTmp);
//   if (lCurSaleDt < 19000101 || lCurSaleDt > lToday)
//      return iRet;
//
//   lTmp = atoin(pSale->Conveyance_Sequence_NO, SSIZ_CONVEYANCE_SEQUENCE_NO);
//   if (!lTmp)
//      return iRet;
//
//   // Assessment
//   memcpy(pCSal->Apn, pSale->Assessment_No, iApnLen);
//
//   // Recording info
//   lTmp = atoin(pSale->Conveyance_Sequence_NO, SSIZ_CONVEYANCE_SEQUENCE_NO);
//   sprintf(acTmp, "%.*d", SSIZ_CONVEYANCE_SEQUENCE_NO, lTmp);
//   memcpy(pCSal->DocNum, acTmp, SSIZ_CONVEYANCE_SEQUENCE_NO);
//   lTmp = sprintf(acTmp, "%d", lCurSaleDt);
//   memcpy(pCSal->DocDate, acTmp, lTmp);
//
//   // Sale price
//   long lPrice = atoin(pSale->Confirmed_Sale_Price, SSIZ_INDICATED_SALE_PRICE);
//   if (lPrice > 0)
//   {
//      sprintf(acTmp, "%*u", SALE_SIZ_SALEPRICE, lPrice);
//      memcpy(pCSal->SalePrice, acTmp, SALE_SIZ_SALEPRICE);
//      pCSal->DocType[0] = '1';
//   } else
//   {
//      lPrice = atoin(pSale->Indicated_Sale_Price, SSIZ_INDICATED_SALE_PRICE);
//      if (lPrice > 0)
//      {
//         sprintf(acTmp, "%*u", SALE_SIZ_SALEPRICE, lPrice);
//         memcpy(pCSal->SalePrice, acTmp, SALE_SIZ_SALEPRICE);
//         pCSal->DocType[0] = '1';
//      } else
//      {
//         lPrice = atoin(pSale->Adjusted_Sale_Price, SSIZ_ADJUSTED_SALE_PRICE);
//         if (lPrice > 0)
//         {
//            sprintf(acTmp, "%*u", SALE_SIZ_SALEPRICE, lPrice);
//            memcpy(pCSal->SalePrice, acTmp, SALE_SIZ_SALEPRICE);
//            pCSal->DocType[0] = '1';
//         }
//      }
//   }
//
//   // Doc code
//   if (pSale->Transfer_Type_Code[0] > ' ')
//   {
//      memcpy(pCSal->DocCode, pSale->Transfer_Type_Code, SALE_SIZ_DOCCODE);
//      if (!memcmp(pCSal->DocCode, "STT", 3) || !memcmp(pCSal->DocCode, "STN", 3))
//         pCSal->DocType[0] = '1';
//      else if (!memcmp(pCSal->DocCode, "FCT", 3))
//         memcpy(pCSal->DocType, "27", 2);
//      else 
//      {
//         int iIdx = XrefCodeIndex((XREFTBL *)&asDeed[0], pSale->Transfer_Type_Code, iNumDeeds);
//         if (iIdx >= 0)
//         {
//            if (asDeed[iIdx].acFlags[0] == 'Y')
//               pCSal->NoneSale_Flg = 'Y';
//            lTmp = sprintf(acTmp, "%d", asDeed[iIdx].iIdxNum);
//            memcpy(pCSal->DocType, acTmp, lTmp);
//
//            // Set fractional interest
//            if (lPrice > 0 && lTmp == 57)
//               pCSal->SaleCode[0] = 'P';
//         }
//      }
//   }
//
//   // Multiple sale flag Y/N
//   if (pSale->Multiple_Sale_Flag[0] == 'Y')
//   {
//      pCSal->MultiSale_Flg = 'Y';
//      memcpy(pCSal->PrimaryApn, pSale->Primary_Parcel_No, iApnLen);
//   }
//
//   // Sale code
//   if (lPrice > 0 && pCSal->SaleCode[0] < 'A' && pCSal->DocType[0] == '1')
//      pCSal->SaleCode[0] = 'F';
//
//   // Percent xfer
//   if (lPrice > 0 && isdigit(pSale->Transfer_Type_Code[1]))
//   {
//      pCSal->SaleCode[0] = 'P';
//      memcpy(pCSal->PctXfer, &pSale->Transfer_Type_Code[1], 2);
//   }
//
//   pCSal->CRLF[0] = '\n';
//   pCSal->CRLF[1] = 0;
//   return 0;
//}

//int Riv_UpdateSaleHist(void)
//{
//   SCSAL_REC *pCSale;
//
//   char     *pTmp, acCSalRec[1024], acSaleRec[1024], acTmp[256];
//   char     acOutFile[_MAX_PATH], acLatestSale[16];
//   int      iRet, iTmp, iUpdateSale=0, iNewSale=0;
//   BOOL     bRet;
//   long     lCnt=0;
//
//   // Check current sale file
//   if (_access(acSaleFile, 0))
//   {
//      LogMsg("***** Missing current sale file %s", acSaleFile);
//      return -1;
//   }
//
//   // Open current sale
//   LogMsg("Open current sale file %s", acSaleFile);
//   fdSale = fopen(acSaleFile, "r");
//   if (fdSale == NULL)
//   {
//      LogMsg("***** Error opening current sale file: %s\n", acSaleFile);
//      return -2;
//   }
//
//   // Create output file
//   sprintf(acOutFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "TMP");
//   LogMsg("Create updated sale file %s", acOutFile);
//   fdCSale = fopen(acOutFile, "w");
//   if (fdCSale == NULL)
//   {
//      LogMsg("***** Error creating updated sale file: %s\n", acOutFile);
//      return -2;
//   }
//
//   /* Create test file
//   FILE *fdTmp;
//   sprintf(acTmp, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "Xfr");
//   LogMsg("Create transfer type file %s", acTmp);
//   fdTmp = fopen(acTmp, "w");
//   if (fdTmp == NULL)
//   {
//      LogMsg("***** Error creating transfer type file: %s\n", acTmp);
//      return -2;
//   } 
//   */
//
//   acLatestSale[0] =0;
//   pCSale = (SCSAL_REC *)&acCSalRec[0];
//
//   // Merge loop
//   while (!feof(fdSale))
//   {
//      // Get current sale
//      pTmp = fgets(acSaleRec, 1024, fdSale);
//      if (!pTmp)
//         break;
//
//      // Format cumsale record
//      iRet = Riv_FormatSaleRec(acCSalRec, acSaleRec);
//      if (!iRet)
//      {
//         // Output current sale record
//         fputs(acCSalRec, fdCSale);
//         iUpdateSale++;
//         if (memcmp(acLatestSale, pCSale->DocDate, 8) < 0)
//            memcpy(acLatestSale, pCSale->DocDate, 8);
//
//         //if (pCSale->DocCode[0] > ' ')
//         //{
//         //   sprintf(acTmp, "%.3s\n", pCSale->DocCode);
//         //   fputs(acTmp, fdTmp);
//         //}
//      } else
//         LogMsg("Drop sale %.16s", acSaleRec);
//
//      if (!(++lCnt % 1000))
//         printf("\r%u", lCnt);
//   }
//
//   // Close files
//   if (fdCSale)
//      fclose(fdCSale);
//   if (fdSale)
//      fclose(fdSale);
//   //if (fdTmp)
//   //   fclose(fdTmp);
//
//   char  acHistFile[256], acTmpFile[256];
//
//   sprintf(acHistFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "SLS");
//   sprintf(acTmpFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "DAT");
//   strcat(acOutFile, "+");
//   strcat(acOutFile, acHistFile);
//
//   // Sort sale file - APN (asc), Saledate (asc), DocNum (asc)
//   sprintf(acTmp, "S(1,10,C,A,27,8,C,A,15,12,C,A,57,10,C,D) F(TXT) DUPO(1,10,27,8,16,6) ");
//   iTmp = sortFile(acOutFile, acTmpFile, acTmp);
//
//   // Rename old history file to SL1
//   sprintf(acOutFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "SL1");
//   if (!_access(acOutFile, 0))
//   {
//      LogMsg("Removing %s", acOutFile);
//      bRet = DeleteFile(acOutFile);
//      if (!bRet)
//         LogMsg("***** Error deleting %s", acOutFile);
//   }
//
//   LogMsg("Renaming %s to %s", acHistFile, acOutFile);
//   bRet = MoveFile(acHistFile, acOutFile);
//   if (!bRet)
//      LogMsg("***** Error renaming %s to %s", acHistFile, acOutFile);
//
//   // Rename history file
//   LogMsg("Renaming %s to %s", acTmpFile, acHistFile);
//   bRet = MoveFile(acTmpFile, acHistFile);
//   if (!bRet)
//      LogMsg("***** Error renaming %s to %s", acTmpFile, acHistFile);
//
//   LogMsg("Total sale records processed:   %u", lCnt);
//   LogMsg("Total sale records updated:     %u", iUpdateSale);
//   LogMsg("Total cumulative sale records:  %u", iTmp);
//
//   acLatestSale[8] = 0;
//   LogMsgD("Update Sale History completed.  Latest sale is %s", acLatestSale);
//
//   return 0;
//}

/******************************** Riv_ExtrSale ******************************
 *
 * Extract sale file SalesDB.csv 10/04/2019
 * Add special case for 564062028
 *
 *****************************************************************************/

int Riv_ExtrSale()
{
   char      acOutFile[_MAX_PATH], acTmpFile[_MAX_PATH];
   char      acApn[32], acTmp[256], acRec[2048], *pRec;
   long      lCnt=0, lPrice, iTmp;
   FILE      *fdOut;
   SCSAL_REC SaleRec;

   LogMsg0("Loading Sale file");

   // Open Sales file
   LogMsg("Open Sales file %s", acSaleFile);
   fdSale = fopen(acSaleFile, "r");
   if (fdSale == NULL)
   {
      LogMsg("***** Error opening Sales file: %s\n", acSaleFile);
      return 2;
   }

   // Skip header
   pRec = fgets(acRec, 2048, fdSale);

   // Open Output file
   sprintf(acTmpFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "TMP");

   LogMsg("Open output file %s", acTmpFile);
   fdOut = fopen(acTmpFile, "w");
   if (fdOut == NULL)
   {
      LogMsg("***** Error creating sale output file: %s\n", acTmpFile);
      return -2;
   }

   // Loop through record set
   while (!feof(fdSale))
   {  
      pRec = fgets(acRec, 2048, fdSale);
      if (!pRec)
         break;

      // Parse input rec
      iTokens = ParseStringNQ(pRec, cDelim, MAX_FLD_TOKEN, apTokens);

      if (iTokens < S_TREEVINEVALUE)
      {
         LogMsg0("*** Error: bad sale record %.16s (#tokens=%d)", acRec, iTokens);
         continue;
      }

      // Collect data - Only take sale rec that has both docnum and docdate
      if (*apTokens[S_CONVEYANCENUMBER] == ' ' || *apTokens[S_CONVEYANCEDATE] == ' ')
         continue;

      // Reset output record
      memset((void *)&SaleRec, ' ', sizeof(SCSAL_REC));

      // APN
      iTmp = strlen(apTokens[S_PIN]);
      sprintf(acApn, "%.*s%s", iApnLen-iTmp, "000000", apTokens[S_PIN]);
      if (iTmp < iApnLen)
         memcpy(SaleRec.Apn, acApn, iApnLen);
      else
         memcpy(SaleRec.Apn, apTokens[S_PIN], iApnLen);

      // Doc date
      pRec = dateConversion(apTokens[S_CONVEYANCEDATE], acTmp, MM_DD_YYYY_1);
      if (pRec)
      {
         memcpy(SaleRec.DocDate, pRec, 8);
         iTmp = atol(acTmp);
         if (iTmp > lLastRecDate)
            lLastRecDate = iTmp;
      } else
      {
         LogMsg("*** Bad sale date: %s [%s]", apTokens[S_CONVEYANCEDATE], apTokens[S_APN]);
         continue;
      }

      // Docnum
      if (*(apTokens[S_CONVEYANCENUMBER]+4) == '-')
         vmemcpy(SaleRec.DocNum, apTokens[S_CONVEYANCENUMBER]+5, 7);
      else
      {
         LogMsg("*** Bad docnum: %s [%s]", apTokens[S_CONVEYANCENUMBER], apTokens[S_APN]);
         continue;
      }

      // Special case 10/05/2021
      if (!memcmp(SaleRec.Apn, "564062028", 9))
      {
         if (!memcmp(SaleRec.DocDate, "20210409", 8) && !memcmp(SaleRec.DocNum, "022369", 5))
         {
            LogMsg("*** Invalid docnum: %s %.8s [%s]", apTokens[S_CONVEYANCENUMBER], SaleRec.DocDate, apTokens[S_APN]);
            continue;
         }
      }

      // Sale price
      lPrice = atol(apTokens[S_INDICATEDSALEPRICE]);
      if (lPrice > 0)
      {
         sprintf(acTmp, "%*u", SIZ_SALE1_AMT, lPrice);
         memcpy(SaleRec.SalePrice, acTmp, SIZ_SALE1_AMT);
      }

      // Transfer Type
      int iIdx = XrefCodeIndex((XREFTBL *)&asDeed[0], apTokens[S_TRANSFERTYPECODE], iNumDeeds);
      if (iIdx >= 0)
      {
         if (lPrice > 1000)
         {
            // Set fractional interest
            if (iIdx == 57)
               SaleRec.SaleCode[0] = 'P';
         } else if (asDeed[iIdx].acFlags[0] == 'Y')
            SaleRec.NoneSale_Flg = 'Y';
         iTmp = sprintf(acTmp, "%d", asDeed[iIdx].iIdxNum);
         memcpy(SaleRec.DocType, acTmp, iTmp);
         memcpy(SaleRec.DocCode, apTokens[S_TRANSFERTYPECODE], 3);
      } else
         LogMsg("*** Unknown TransferTypeCode: '%s' APN=%s", apTokens[S_TRANSFERTYPECODE], apTokens[S_APN]);

      // Group sale?

      // Seller

      // Buyer

      SaleRec.CRLF[0] = 10;
      SaleRec.CRLF[1] = 0;
      fputs((char *)&SaleRec,fdOut);

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   fclose(fdSale);
   fclose(fdOut);
   
   LogMsg("Total processed records: %u", lCnt);
   LogMsg("    Last recording date: %u\n", lLastRecDate);

   // Update cumulative sale file
   if (lCnt > 0)
   {
      // Append and resort SLS file
      // Sort on APN asc, DocDate asc, DocNum asc
      sprintf(acTmp, "S(1,14,C,A,27,8,C,A,15,12,C,A) F(TXT) DUPO(1,34) ");
      sprintf(acOutFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "srt");
      sprintf(acCSalFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "Sls");     
      if (!_access(acCSalFile, 0))
         sprintf(acRec, "%s+%s", acTmpFile, acCSalFile);
      else
         strcpy(acRec, acTmpFile);
      lCnt = sortFile(acRec, acOutFile, acTmp);
      if (lCnt > 0)
      {
         // Rename old cum sale
         sprintf(acTmpFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "sav");
         if (!_access(acTmpFile, 0))
            iTmp = remove(acTmpFile);
         if (!_access(acCSalFile, 0))
            iTmp = rename(acCSalFile, acTmpFile);
         if (!iTmp)
            iTmp = rename(acOutFile, acCSalFile);
         else
            LogMsg("***** Error removing %s.  Please rerun with -Xs option", acCSalFile);
      }
   } else
      iTmp = -1;

   LogMsg("Total Sale records output: %d.\n", lCnt);
   return iTmp;
}

int Riv_UpdateLienRecExe(char *pOutbuf, char *pExeCode, char *pExeAmt, int iIdx)
{
   int iRet = 0;
   LIENEXTR *pLienRec = (LIENEXTR *)pOutbuf;

   switch (iIdx)
   {
      case 1:
         memcpy(pLienRec->extra.Riv.Exe_Code1, pExeCode, 2);
         if (*pExeAmt > '0')
            memcpy(pLienRec->extra.Riv.Exe_Amt1, pExeAmt, 2);
         break;
      case 2:
         memcpy(pLienRec->extra.Riv.Exe_Code2, pExeCode, 2);
         if (*pExeAmt > '0')
            memcpy(pLienRec->extra.Riv.Exe_Amt2, pExeAmt, 2);
         break;
      case 3:
         memcpy(pLienRec->extra.Riv.Exe_Code3, pExeCode, 2);
         if (*pExeAmt > '0')
            memcpy(pLienRec->extra.Riv.Exe_Amt3, pExeAmt, 2);
         break;
      default:
         iRet = 10;
   }

   return iRet;
}

/******************************* Riv_CreateLienRec ***************************
 *
 * Return 0 if successful, >0 if error
 *
 *****************************************************************************/

int Riv_CreateLienRec(char *pOutbuf, char *pRollRec)
{
   LIENEXTR *pLienRec = (LIENEXTR *)pOutbuf;
   char      acTmp[256];
   int       iRet, iTmp, iRecType, lTmp;

   RIV_LIEN11 *pRec11 = (RIV_LIEN11 *)pRollRec;
   RIV_LIEN17 *pRec17 = (RIV_LIEN17 *)pRollRec;
   RIV_LIEN18 *pRec18 = (RIV_LIEN18 *)pRollRec;
   RIV_LIEN19 *pRec19 = (RIV_LIEN19 *)pRollRec;
   RIV_LIEN20 *pRec20 = (RIV_LIEN20 *)pRollRec;
   RIV_LIEN21 *pRec21 = (RIV_LIEN21 *)pRollRec;
   RIV_LIEN23 *pRec23 = (RIV_LIEN23 *)pRollRec;
   RIV_LIEN26 *pRec26 = (RIV_LIEN26 *)pRollRec;
   RIV_LIEN28 *pRec28 = (RIV_LIEN28 *)pRollRec;
   RIV_LIEN30 *pRec30 = (RIV_LIEN30 *)pRollRec;
   RIV_LIEN52 *pRec52 = (RIV_LIEN52 *)pRollRec;
   RIV_LIEN53 *pRec53 = (RIV_LIEN53 *)pRollRec;
   RIV_LIEN67 *pRec67 = (RIV_LIEN67 *)pRollRec;
   RIV_LIEN71 *pRec71 = (RIV_LIEN71 *)pRollRec;
   RIV_LIEN72 *pRec72 = (RIV_LIEN72 *)pRollRec;
   RIV_LIEN73 *pRec73 = (RIV_LIEN73 *)pRollRec;
   RIV_LIEN74 *pRec74 = (RIV_LIEN74 *)pRollRec;
   RIV_LIEN75 *pRec75 = (RIV_LIEN75 *)pRollRec;

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "000113968", 9) )
   //   lTmp = 0;
#endif

   iRecType = (atoin(pRollRec+OFF_LIEN_RECORD_TYPE, SIZ_LIEN_RECORD_TYPE));
   switch(iRecType)
   {
      case 11:
         // Start copying data
         memcpy(pLienRec->acApn, pRec11->Primary_Parcel_No, SIZ_LIEN_PRIMARY_PARCEL_NO);

         // Year assessed
         memcpy(pLienRec->acYear, myCounty.acYearAssd, 4);
         break;

      case 23:
         iTmp = atoin(pRec23->Tra, SIZ_LIEN_RIVTRA);
         if (iTmp > 0)
            memcpy(pLienRec->acTRA, pRec23->Tra, SIZ_LIEN_RIVTRA);
         memcpy(pLienRec->extra.Riv.XrefApn, pRec23->Reference_Apn, SIZ_LIEN_REFERENCE_APN);

         // Set full exemption flag
         if (pRec23->Tax_Code[0] > '0' && pRec23->Tax_Code[0] < '4')
            pLienRec->SpclFlag |= LX_FULLEXE_FLG;
         memcpy(pLienRec->extra.Riv.TaxCode, pRec23->Tax_Code, SIZ_LIEN_TAX_CODE);
         break;

      case 53:
         long lTotalExe, lOtherImpr, lLand, lImpr, lAircraft_PP, lBoat_PP, lPers;
         long lBusInv, lBus_PP, lHouse_PP, lRanch_PP, lOther_PP, lFixtr, lTree;
         lTotalExe=lOtherImpr=lLand=lImpr=lPers= 0;
         lBusInv=lBus_PP=lHouse_PP=lRanch_PP=lOther_PP=lFixtr=lTree=lAircraft_PP=lBoat_PP = 0;

         iTmp = 0;
         while (iTmp < 13)
         {
            iRet = atoin(pRec53->ValExe[iTmp].Code, SIZ_LIEN_CODE1);
            if (!iRet)
               break;
            lTmp = atoin(pRec53->ValExe[iTmp].Amount, SIZ_LIEN_AMOUNT1);
            switch (iRet)
            {
               case 1:
                  lLand = lTmp;
                  break;
               case 2:
                  lImpr = lTmp;
                  break;
               case 21: // HO occupied
                  lTotalExe += lTmp;
                  pLienRec->acHO[0] = '1';
                  sprintf(acTmp, "%d", lTmp);
                  iRet = Riv_UpdateLienRecExe(pOutbuf, "HO", acTmp, iTmp);
                  if (!iRet) iTmp++;
                  break;
               case 3:
                  lFixtr += lTmp;
                  break;
               case 4:
               case 40:
                  lBusInv += lTmp;
                  break;
               case 5:
               case 6:
                  lTree += lTmp;
                  break;
               case 7:
                  lHouse_PP += lTmp;
                  break;
               case 8:
                  lBus_PP += lTmp;
                  break;
               case 9:
                  lRanch_PP += lTmp;
                  break;
               case 10:
                  lOther_PP += lTmp;
                  break;
               case 13:
                  lBoat_PP += lTmp;
                  break;
               case 14:
                  lAircraft_PP += lTmp;
                  break;
               default:
                  if (iRet > 22 && iRet < 40)
                  {
                     lTotalExe += lTmp;              // Other exempt
                     sprintf(acTmp, "%d", lTmp);
                     iRet = Riv_UpdateLienRecExe(pOutbuf, "HO", acTmp, iTmp);
                     if (!iRet) iTmp++;
                  } else if (iRet > 2 && iRet < 15)
                     lOtherImpr += lTmp;
                  else if (iRet < 15 || iRet > 16)   // Penalty
                     LogMsg0("*** Unknown code: %d with value %d", iRet, lTmp);
                  break;
            }

            iTmp++;
         }

         // Personal properties
         lPers = lBus_PP+lHouse_PP+lRanch_PP+lOther_PP+lAircraft_PP+lBoat_PP;

         lOtherImpr += (lPers+lBusInv+lFixtr+lTree);
         if (lOtherImpr > 0)
         {
            sprintf(acTmp, "%*u", SIZ_LIEN_FIXT, lOtherImpr);
            memcpy(pLienRec->acOther, acTmp, SIZ_LIEN_FIXT);

            if (lPers > 0)
            {
               sprintf(acTmp, "%*u", SIZ_LIEN_FIXT, lPers);
               memcpy(pLienRec->acPP_Val, acTmp, SIZ_LIEN_FIXT);
            }
            if (lFixtr > 0)
            {
               sprintf(acTmp, "%*u", SIZ_LIEN_FIXT, lFixtr);
               memcpy(pLienRec->acME_Val, acTmp, SIZ_LIEN_FIXT);
            }

            if (lBusInv > 0)
            {
               sprintf(acTmp, "%*u", SIZ_LIEN_FIXT, lBusInv);
               memcpy(pLienRec->extra.Riv.Bus_Inv, acTmp, SIZ_LIEN_FIXT);
            }

            if (lTree > 0)
            {
               iTmp = sprintf(acTmp, "%u", lTree);
               memcpy(pLienRec->extra.Riv.LivingImpr, acTmp, iTmp);
            }
         }

         if (lLand > 0)
         {
            sprintf(acTmp, "%*u", SIZ_LIEN_LAND, lLand);
            memcpy(pLienRec->acLand, acTmp, SIZ_LIEN_LAND);
         }
         if (lImpr > 0)
         {
            sprintf(acTmp, "%*u", SIZ_LIEN_LAND, lImpr);
            memcpy(pLienRec->acImpr, acTmp, SIZ_LIEN_LAND);

            sprintf(acTmp, "%*u", SIZ_LIEN_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
            memcpy(pLienRec->acRatio, acTmp, SIZ_LIEN_RATIO);
         }

         lTmp = lLand+lImpr+lOtherImpr;
         if (lTmp > 0)
         {
            sprintf(acTmp, "%*u", SIZ_LIEN_FIXT, lTmp);
            memcpy(pLienRec->acGross, acTmp, SIZ_LIEN_FIXT);
         }
         if (lTotalExe > 0)
         {
            sprintf(acTmp, "%*u", SIZ_LIEN_EXEAMT, lTotalExe);
            memcpy(pLienRec->acExAmt, acTmp, SIZ_LIEN_EXEAMT);
         }
         break;

      case 67:
         if (pRec67->Sold_Year[0] > ' ')
            memcpy(pLienRec->extra.Riv.DelqYr, pRec67->Sold_Year, SIZ_DEL_YR);
         break;

      default:
         break;
   }

   return 0;
}

/********************************* Riv_ExtrLien *****************************
 *
 * 2017 - Modify to read ASCII with CRLF
 *
 ****************************************************************************/

int Riv_ExtrLien()
{
   char  acBuf[512], acRollRec[MAX_RECSIZE], acOutFile[_MAX_PATH], *pTmp;
   long  lCnt=0, iRet;
   FILE  *fdLien;
   LIENEXTR *pLienRec = (LIENEXTR *)acBuf;

   LogMsg0("Extract lien for %s", myCounty.acCntyCode);

   // Open roll file
   LogMsg("Open Roll file %s", acRollFile);
   fdRoll = fopen(acRollFile, "rb");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", acRollFile);
      return 2;
   }

   // Open Output file
   sprintf(acOutFile, acLienTmpl, myCounty.acCntyCode, myCounty.acCntyCode);
   LogMsg("Open output file %s", acOutFile);
   fdLien = fopen(acOutFile, "w");
   if (fdLien == NULL)
   {
      LogMsg("***** Error opening Lien extract file: %s\n", acOutFile);
      return 4;
   }

   // Merge loop
   pTmp = acRollRec;
   while (!feof(fdRoll))
   {
      iRet = fread(acRollRec, 1, iRollLen, fdRoll);
      if (iRet != iRollLen)
         break;

      if (acRollRec[OFF_LIEN_PRIMARY_PARCEL_NO] == ' ')
         continue;

      if (memcmp(&acRollRec[OFF_LIEN_PRIMARY_PARCEL_NO], acBuf, SIZ_LIEN_PRIMARY_PARCEL_NO))
      {
         // Ignore record without APN
         if (acBuf[0] > ' ')
         {
            iRet = atoin((char *)&acBuf[OFF_TRANSFER_DT], 8);
            if (iRet > lLastRecDate && iRet < lToday)
               lLastRecDate = iRet;

            // Output rec
            pLienRec->LF[0] = '\n';
            pLienRec->LF[1] = 0;
            fputs(acBuf, fdLien);

            if (!(++lCnt % 1000))
               printf("\r%u", lCnt);
         }

         memset(acBuf, ' ', sizeof(LIENEXTR));
         pLienRec->acHO[0] = '2';
      }

      // Create new R01 record
      iRet = Riv_CreateLienRec(acBuf, acRollRec);
   }

   // Close files
   if (fdRoll)
      fclose(fdRoll);
   if (fdLien)
      fclose(fdLien);

   LogMsgD("Total records output:     %d\n", lCnt);
   lRecCnt = lCnt;
   return 0;
}

/****************************** Riv_ExtrLienCsv *****************************
 *
 * 2019 - Input file is comma delimited
 *
 ****************************************************************************/

int Riv_CreateLienRecCsv(char *pOutbuf)
{
   char     acTmp[256];
   long     iTmp;
   LIENEXTR *pLienRec = (LIENEXTR *)pOutbuf;
   ULONG    lTmp;

   // Clear output buffer
   memset(pOutbuf, ' ', sizeof(LIENEXTR));

   // Start copying data
   iTmp = remChar(apTokens[L_PIN], 'X');
   vmemcpy(pLienRec->acApn, apTokens[L_PIN], iTmp);

   // Year assessed
   memcpy(pLienRec->acYear, myCounty.acYearAssd, 4);

   // TRA
   memcpy(pLienRec->acTRA, apTokens[L_TRA], strlen(apTokens[L_TRA]));

   // Land
   long lLand = atol(apTokens[L_LAND]);
   if (lLand > 0)
   {
      iTmp = sprintf(acTmp, "%*u", SIZ_LIEN_LAND, lLand);
      memcpy(pLienRec->acLand, acTmp, iTmp);
   }

   // Improve
   long lImpr = atol(apTokens[L_IMPROVEMENTS]);
   if (lImpr > 0)
   {
      iTmp = sprintf(acTmp, "%*u", SIZ_LIEN_IMPR, lImpr);
      memcpy(pLienRec->acImpr, acTmp, iTmp);
   }

   // Other values
   long lFixtr  = atoi(apTokens[L_TRADEFIXTURESAMOUNT]);
   long lLivImpr= atoi(apTokens[L_LIVINGIMPROVEMENTS]);
   long lPers   = atoi(apTokens[L_PERSONALVALUE]);
   lTmp = lFixtr+lPers+lLivImpr;
   if (lTmp > 0)
   {
      iTmp = sprintf(acTmp, "%*u", SIZ_LIEN_OTHERS, lTmp);
      memcpy(pLienRec->acOther, acTmp, iTmp);

      if (lPers > 0)
      {
         iTmp = sprintf(acTmp, "%*u", SIZ_LIEN_PERS, lPers);
         memcpy(pLienRec->acPP_Val, acTmp, iTmp);
      }

      if (lFixtr > 0)
      {
         iTmp = sprintf(acTmp, "%*u", SIZ_LIEN_FIXT, lFixtr);
         memcpy(pLienRec->acME_Val, acTmp, iTmp);
      }

      if (lLivImpr > 0)
      {
         iTmp = sprintf(acTmp, "%u", lLivImpr);
         memcpy(pLienRec->extra.Riv.LivingImpr, acTmp, iTmp);
      }
   }

   // Gross total
   ULONG lGross = lTmp + lLand + lImpr;
   if (lGross > 0)
   {
      sprintf(acTmp, "%*u", SIZ_LIEN_FIXT, lGross);
      memcpy(pLienRec->acGross, acTmp, SIZ_LIEN_FIXT);
   }

   // Ratio
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*u", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
      memcpy(pLienRec->acRatio, acTmp, SIZ_RATIO);
   }

   // Exemption
   long lHO_Exe = atol(apTokens[L_HOX]);
   ULONG lOE_Exe = atol(apTokens[L_OTHEREXEMPTION]);
   ULONG lVE_Exe = atol(apTokens[L_VETERANSEXEMPTION]);
   ULONG lDV_Exe = atol(apTokens[L_DISABLEDVETERANSEXEMPTION]);
   ULONG lCH_Exe = atol(apTokens[L_CHURCHEXEMPTION]);
   ULONG lRE_Exe = atol(apTokens[L_RELIGIOUSEXEMPTION]);
   ULONG lCE_Exe = atol(apTokens[L_CEMETERYEXEMPTION]);
   ULONG lPS_Exe = atol(apTokens[L_PUBLICSCHOOLEXEMPTION]);
   ULONG lPL_Exe = atol(apTokens[L_PUBLICLIBRARYEXEMPTION]);
   ULONG lPM_Exe = atol(apTokens[L_PUBLICMUSEUMEXEMPTION]);
   ULONG lWC_Exe = atol(apTokens[L_WELFARECOLLEGEEXEMPTION]);
   ULONG lWH_Exe = atol(apTokens[L_WELFAREHOSPITALEXEMPTION]);
   ULONG lWS_Exe = atol(apTokens[L_WPP_SCHOOLEXEMPTION]);
   ULONG lWR_Exe = atol(apTokens[L_WC_RELIGIOUSEXEMPTION]);
   ULONG lTotalExe = lHO_Exe+lOE_Exe+lVE_Exe+lDV_Exe+lCH_Exe+lRE_Exe+lCE_Exe+lPS_Exe+lPL_Exe+lPM_Exe+
      lWC_Exe+lWH_Exe+lWS_Exe+lWR_Exe;

   pLienRec->acHO[0] = '2';            // 'N'
   if (lTotalExe > 0)
   {
      if (lTotalExe > lGross)
         lTotalExe = lGross;
      iTmp = sprintf(acTmp, "%*u", SIZ_LIEN_EXEAMT, lTotalExe);
      memcpy(pLienRec->acExAmt, acTmp, iTmp);

      if (lHO_Exe > 0)
         pLienRec->acHO[0] = '1';      // 'Y'

      // Set full exemption flag 
      if (lGross == lTotalExe || *apTokens[L_ISTAXABLE] == 'N')
      {
         pLienRec->SpclFlag = LX_FULLEXE_FLG;
      } else if (isdigit(*apTokens[L_TAXABILITYCODE]))
      {
         // For 2020, there is no tax code.  The new field ISTAXABLE contains Y/N
         vmemcpy(pLienRec->acTaxCode, apTokens[L_TAXABILITYCODE], SIZ_TAX_CODE);
      }
   }  

   // Populate per bulk customer request 07/20/2022
   lTmp = atol(apTokens[L_TENPERCENTASSESSEDPENALTY]);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*u", SIZ_LIEN_LATEPEN, lTmp);
      memcpy(pLienRec->acLatePen, acTmp, SIZ_LIEN_LATEPEN);
   }

   pLienRec->LF[0] = '\n';
   pLienRec->LF[1] = 0;
   return 0;
}

int Riv_UpdateLienRecCsv(char *pOutbuf, char *pRec)
{
   char     acTmp[256];
   long     iTmp;
   LIENEXTR *pLienRec = (LIENEXTR *)pOutbuf;
   ULONG    lTmp, lLand, lImpr, lLivImpr;

   iTokens = ParseStringNQ(pRec, ',', MAX_FLD_TOKEN, apTokens);
   if (iTokens < L_COLS)
   {
      LogMsg("***** Error: bad input record for APN=%s (%d)", apTokens[L_PIN], iTokens);
      return -1;
   }

   // Other values
   long lFixtr  = atoi(apTokens[L_TRADEFIXTURESAMOUNT]);
   long lPers   = atoi(apTokens[L_PERSONALVALUE]);
   lLivImpr = atoln(pLienRec->extra.Riv.LivingImpr, SIZ_LIEN_AMT);
   lTmp = lFixtr+lPers+lLivImpr;
   if (lTmp > 0)
   {
      iTmp = sprintf(acTmp, "%*u", SIZ_LIEN_OTHERS, lTmp);
      memcpy(pLienRec->acOther, acTmp, iTmp);

      if (lPers > 0)
      {
         iTmp = sprintf(acTmp, "%*u", SIZ_LIEN_PERS, lPers);
         memcpy(pLienRec->acPP_Val, acTmp, iTmp);
      }

      if (lFixtr > 0)
      {
         iTmp = sprintf(acTmp, "%*u", SIZ_LIEN_FIXT, lFixtr);
         memcpy(pLienRec->acME_Val, acTmp, iTmp);
      }
   }

   lLand = atoln(pLienRec->acLand, SIZ_LIEN_AMT);
   lImpr = atoln(pLienRec->acImpr, SIZ_LIEN_AMT);

   // Gross total
   ULONG lGross = lTmp + lLand + lImpr;
   if (lGross > 0)
   {
      sprintf(acTmp, "%*u", SIZ_LIEN_FIXT, lGross);
      memcpy(pLienRec->acGross, acTmp, SIZ_LIEN_FIXT);
   }

   // Exemption
   ULONG lOE_Exe = atol(apTokens[L_OTHEREXEMPTION]);
   ULONG lVE_Exe = atol(apTokens[L_VETERANSEXEMPTION]);
   ULONG lDV_Exe = atol(apTokens[L_DISABLEDVETERANSEXEMPTION]);
   ULONG lCH_Exe = atol(apTokens[L_CHURCHEXEMPTION]);
   ULONG lRE_Exe = atol(apTokens[L_RELIGIOUSEXEMPTION]);
   ULONG lCE_Exe = atol(apTokens[L_CEMETERYEXEMPTION]);
   ULONG lPS_Exe = atol(apTokens[L_PUBLICSCHOOLEXEMPTION]);
   ULONG lPL_Exe = atol(apTokens[L_PUBLICLIBRARYEXEMPTION]);
   ULONG lPM_Exe = atol(apTokens[L_PUBLICMUSEUMEXEMPTION]);
   ULONG lWC_Exe = atol(apTokens[L_WELFARECOLLEGEEXEMPTION]);
   ULONG lWH_Exe = atol(apTokens[L_WELFAREHOSPITALEXEMPTION]);
   ULONG lWS_Exe = atol(apTokens[L_WPP_SCHOOLEXEMPTION]);
   ULONG lWR_Exe = atol(apTokens[L_WC_RELIGIOUSEXEMPTION]);
   ULONG lTotalExe = lOE_Exe+lVE_Exe+lDV_Exe+lCH_Exe+lRE_Exe+lCE_Exe+lPS_Exe+lPL_Exe+lPM_Exe+
      lWC_Exe+lWH_Exe+lWS_Exe+lWR_Exe;

   if (lTotalExe > 0)
   {
      pLienRec->acHO[0] = '2';            // 'N'
      if (lTotalExe > lGross)
         lTotalExe = lGross;
      iTmp = sprintf(acTmp, "%*u", SIZ_LIEN_EXEAMT, lTotalExe);
      memcpy(pLienRec->acExAmt, acTmp, iTmp);

      // Set full exemption flag 
      if (lGross == lTotalExe || *apTokens[L_ISTAXABLE] == 'N')
      {
         pLienRec->SpclFlag = LX_FULLEXE_FLG;
      } else if (isdigit(*apTokens[L_TAXABILITYCODE]))
      {
         // For 2020, there is no tax code.  The new field ISTAXABLE contains Y/N
         vmemcpy(pLienRec->acTaxCode, apTokens[L_TAXABILITYCODE], SIZ_TAX_CODE);
      }
   }  

   // Populate per bulk customer request 07/20/2022
   lTmp = atol(apTokens[L_TENPERCENTASSESSEDPENALTY]);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*u", SIZ_LIEN_LATEPEN, lTmp);
      memcpy(pLienRec->acLatePen, acTmp, SIZ_LIEN_LATEPEN);
   }

   pLienRec->LF[0] = '\n';
   pLienRec->LF[1] = 0;
   return 0;
}

int Riv_ExtrLienCsv()
{
   char      *pTmp, acBuf[1024], acRec[MAX_RECSIZE], acBPPRec[MAX_RECSIZE], acTmpFile[_MAX_PATH], acOutFile[_MAX_PATH], cNonTax;
   int       iRet;
   long      lOut=0, lCnt=0;
   FILE      *fdOut, *fdIn;
   LIENEXTR *pRec = (LIENEXTR *)acBuf;

   LogMsg0("Extracting LDR value.");

   // Open input file
   LogMsg("Open %s for input", acRollFile);
   fdIn = fopen(acRollFile, "r");
   if (fdIn == NULL)
   {
      LogMsg("***** Error opening input file: %s\n", acRollFile);
      return -2;
   }  

   // Open out file
   sprintf(acTmpFile, acLienTmpl, myCounty.acCntyCode, "Tmp");
   sprintf(acOutFile, acLienTmpl, myCounty.acCntyCode, myCounty.acCntyCode);
   LogMsg("Open file %s for output", acTmpFile);
   fdOut = fopen(acTmpFile, "w");
   if (fdOut == NULL)
   {
      LogMsg("***** Error creating output file: %s\n", acTmpFile);
      return -4;
   }

   iRet = GetPrivateProfileString(myCounty.acCntyCode, "NonTax", "Y", acRec, 4, acIniFile);
   cNonTax = acRec[0];

   // Start loop 
   while (!feof(fdIn))
   {
      pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdIn);
      if (!pTmp)
         break;

      // Ignore unsecured parcel 000-008
      if (acRec[0] > '9' || memcmp(acRec, "008", 3) <= 0)
         continue;

      if (acRec[9] == 'B' || acRec[9] == 'b')
      {
         // Save BPP rec
         strcpy(acBPPRec, acRec);
         continue;
      }

      // Parse input rec
      iTokens = ParseStringNQ(acRec, cLdrSep, MAX_FLD_TOKEN, apTokens);
      if (iTokens < L_COLS)
      {
         LogMsg("***** Error: bad input record for APN=%s (%d)", apTokens[L_PIN], iTokens);
         continue;
      }

      // Ignore non-taxable parcels
      //if (cNonTax == 'N' && *apTokens[L_ISTAXABLE] == 'N')
      //   continue;

      //iRet = strlen(apTokens[L_PIN]);
      //if (iRet != iApnLen)
      //{
      //   if (bDebug)
      //      LogMsg("*** Drop bad APN: %s", apTokens[L_PIN]);
      //   continue;
      //}

      iRet = Riv_CreateLienRecCsv(acBuf);
      if (iRet)
         acBPPRec[0] = 0;

      if (!memcmp(acBPPRec, acRec, iApnLen))
      {
         iRet = Riv_UpdateLienRecCsv(acBuf, acBPPRec);
         if (iRet < 0)
         {
            LogMsg("Bad record: %s", acBPPRec);
            iRet = 0;
         }
      }
      acBPPRec[0] = 0;
      if (!iRet)
      {
         fputs(acBuf, fdOut);
         lOut++;
      }


      // Create new base record
      //if (memcmp(apTokens[L_PIN], pRec->acApn, iApnLen) || *(apTokens[L_PIN]+9) == '-')
      //{
      //   if (!iRet)
      //   {
      //      fputs(acBuf, fdOut);
      //      lOut++;
      //   }
      //   iRet = Riv_CreateLienRecCsv(acBuf);
      //} else
      //   iRet = Riv_UpdateLienRecCsv(acBuf);

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   lOut = sortFile(acTmpFile, acOutFile, "S(1,14,C,A) F(TXT)");

   LogMsg("Total records processed: %u", lCnt);
   LogMsg("Total records output:    %u", lOut);

   // Close files
   if (fdIn)
      fclose(fdIn);
   if (fdOut)
      fclose(fdOut);

   return iRet;
}

/******************************* Riv_MergeProp8 *******************************
 *
 *
 ******************************************************************************/

int Riv_MergeProp8(char *pProp8File, int iSkip)
{
   char     *pTmp, acBuf[MAX_RECSIZE], acProp8Rec[MAX_RECSIZE];
   char     acRawFile[_MAX_PATH], acOutFile[_MAX_PATH];

   HANDLE   fhIn, fhOut;
   FILE     *fdProp8;

   int      iTmp, iProp8Match=0, iProp8Drop=0;
   DWORD    nBytesRead, nBytesWritten;
   BOOL     bRet, bEof;
   long     lRet=0, lCnt=0;

   LogMsg0("Merge Prop8 data to R01");

   sprintf(acRawFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "R01");
   sprintf(acOutFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "M01");

   // Rename files for processing
   if (_access(acRawFile, 0))
   {
      LogMsg("Missing input file %s.  Please recheck!", acRawFile);
      return -1;
   }

   // Open Prop8 file
   LogMsg("Open Prop8 file %s", pProp8File);
   fdProp8 = fopen(pProp8File, "r");
   if (fdProp8 == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", pProp8File);
      return -2;
   }

   // Open Input file
   LogMsg("Open input file %s", acRawFile);
   fhIn = CreateFile(acRawFile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhIn == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening raw file: %s\n", acRawFile);
      return -3;
   }

   // Open Output file
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error creating output file: %s\n", acOutFile);
      return -4;
   }

   // Get first lien rec
   pTmp = fgets((char *)&acProp8Rec[0], 1024, fdProp8);
   if (acProp8Rec[1] > '9')
      pTmp = fgets((char *)&acProp8Rec[0], 1024, fdProp8);

   bEof = (pTmp ? false:true);

   // Copy skip record
   memset(acBuf, ' ', iRecLen);
   while (iSkip-- > 0)
   {
      ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);
      WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
   }

   // Merge loop
   while (!bEof)
   {
      bRet = ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);

      // Check for EOF
      if (!bRet)
      {
         LogMsg("Error reading input file %s (%f)", acRawFile, GetLastError());
         break;
      }

      // EOF ?
      if (!nBytesRead)
         break;

NextProp8Rec:
      iTmp = memcmp(acBuf, &acProp8Rec[1], iApnLen);
      if (!iTmp)
      {
         iProp8Match++;
         acBuf[OFF_PROP8_FLG] = 'Y';

         // Read next roll record
         pTmp = fgets(acProp8Rec, 1024, fdProp8);

         if (!pTmp)
            bEof = true;    // Signal to stop
      } else if (iTmp > 0)       
      {  // Roll not match, drop lien record?
         iProp8Drop++;
         // Get next lien record
         pTmp = fgets(acProp8Rec, 1024, fdProp8);

         if (!pTmp)
            bEof = true;    // Signal to stop
         else
            goto NextProp8Rec;
      }

      WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);

      if (!bRet)
      {
         LogMsg("***** Error occurs: %d\n", GetLastError());
         lRet = -1;
         break;
      }
   }

   // Close files
   if (fdProp8)
      fclose(fdProp8);
   if (fhOut)
      CloseHandle(fhOut);
   if (fhIn)
      CloseHandle(fhIn);

   // Rename output file
   iTmp = remove(acRawFile);
   if (!iTmp)
   {
      iTmp = rename(acOutFile, acRawFile);
      if (iTmp < 0)
      {
         LogMsgD("***** Error renaming file %s to %s", acOutFile, acRawFile);
         iTmp = -1;
      }
   } else
   {
      LogMsgD("***** Error removing file %s", acRawFile);
      iTmp = -1;
   }

   LogMsg("Total output records:       %u", lCnt);
   LogMsg("Total records matched:      %u", iProp8Match);
   LogMsg("Total records dropped:      %u", iProp8Drop);

   printf("\nTotal output records: %u", lCnt);

   lRecCnt = lCnt;
   return iTmp;
}

/******************************** Riv_ExtrProp8 *****************************
 *
 *
 ****************************************************************************/

int Riv_ExtractProp8(char *pProp8File)
{
   char     *pTmp, acTmp[256], acProp8Rec[256];
   char     acOutFile[_MAX_PATH];
   long     lCnt=0;
   FILE     *fdProp8, *fdExt;

   if (lLienYear > 1900)
      sprintf(acTmp, "%d", lLienYear);
   else
      strcpy(acTmp, "yyyy");
   sprintf(acOutFile, acProp8Tmpl, myCounty.acCntyCode, myCounty.acCntyCode, acTmp, "P8");

   // Open Prop8 file
   LogMsg("Open Prop8 file %s", pProp8File);
   fdProp8 = fopen(pProp8File, "r");
   if (fdProp8 == NULL)
   {
      LogMsg("***** Error opening prop8 file: %s\n", pProp8File);
      return -2;
   }

   // Open Output file
   LogMsg("Open output file %s", acOutFile);
   fdExt = fopen(acOutFile, "w");
   if (fdExt == NULL)
   {
      LogMsg("***** Error creating output file: %s\n", acOutFile);
      return -4;
   }

   // Output header
   fputs("APN\n", fdExt);

   // Get first rec - skip hdr if needed
   pTmp = fgets((char *)&acProp8Rec[0], 256, fdProp8);
   if (acProp8Rec[1] > '9')
      pTmp = fgets((char *)&acProp8Rec[0], 256, fdProp8);

   // Merge loop
   while (pTmp)
   {
      // Update flag
      sprintf(acTmp, "%.*s\n", iApnLen, &acProp8Rec[1]);
      fputs(acTmp, fdExt);

      // Read next prop8 record
      pTmp = fgets(acProp8Rec, 256, fdProp8);

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdProp8)
      fclose(fdProp8);
   if (fdExt)
      fclose(fdExt);

   LogMsgD("Total records extracted:      %u", lCnt);
   return 0;
}

/***************************** Riv_MergeSaleChar *****************************
 *
 * Merge chars from sale rec where data available.
 *
 *****************************************************************************/

//void Riv_FormatCharRec(char *pCharRec, char *pSaleRec)
//{
//   long  lTmp, iTmp;
//   char  acTmp[128], acCode[128];
//   RIV_SALE *pRec  = (RIV_SALE *)pSaleRec;
//   STDCHAR  *pChar = (STDCHAR *)pCharRec;
//
//   // Reset output rec
//   memset(pCharRec, ' ', sizeof(STDCHAR));
//
//   // Copy APN
//   memcpy(pChar->Apn, pRec->Assessment_No, SSIZ_ASSESSMENT_NO);
//
//   // Zoning
//   if (pRec->Zoning[0] > ' ')
//      memcpy(pChar->Zoning, pRec->Zoning, SSIZ_ZONE_CODE);
//
//   // BldgClass
//   if (pRec->Construction_Type[0] > ' ')
//   {
//      pChar->BldgClass = pRec->Construction_Type[0];
//
//      // Quality
//      lTmp = atoin(pRec->Structure_Qual_Code, SSIZ_STRUCTURE_QUAL_CODE);
//      if (lTmp > 0)
//      {
//         sprintf(acTmp, "%.1f", (double)(lTmp/10.0));
//         iTmp = Value2Code(acTmp, acCode, NULL);
//         if (acCode[0] > ' ')
//            pChar->BldgQual = acCode[0];
//      }
//   }
//
//   // BldgSqft
//   lTmp = atoin(pRec->Area, SSIZ_AREA);
//   if (lTmp > 0)
//   {
//      sprintf(acTmp, "%*u", SIZ_BLDG_SF, lTmp);
//      memcpy(pChar->BldgSqft, acTmp, SIZ_BLDG_SF);
//   }
//
//   // Eff. year = lAssdYear - EffAge
//   /*
//   lTmp = atoin(pRec->Effective_Age, SSIZ_EFFECTIVE_AGE);
//   if (lTmp > 0)
//   {
//      lTmp = myCounty.iYearAssd - lTmp;
//      iTmp = sprintf(acTmp, "%d", lTmp);
//      memcpy(pOutbuf+OFF_YR_EFF, acTmp, iTmp);
//   }
//   */
//
//   // Carport/Garage (G or C)
//   if (pRec->Carport_Garage_IndicaTor[0] == 'G')
//      pChar->ParkType[0] = 'Z';
//   else if (pRec->Carport_Garage_IndicaTor[0] == 'C')
//      pChar->ParkType[0] = 'C';
//
//   // Pool (Y/N)
//   if (pRec->Pool_Indicator[0] == 'Y')
//      pChar->Pool[0] = 'P';
//
//   // Bed
//   lTmp = atoin(pRec->Bedrooms, SSIZ_BEDROOMS);
//   if (lTmp > 0)
//   {
//      sprintf(acTmp, "%*u", SIZ_BEDS, lTmp);
//      memcpy(pChar->Beds, acTmp, SIZ_BEDS);
//   }
//
//   // Bath
//   lTmp = atoin(pRec->Baths, SSIZ_BATHS);
//   if (lTmp > 0)
//   {
//      sprintf(acTmp, "%*u", SIZ_BATH_F, lTmp);
//      memcpy(pChar->FBaths, acTmp, SIZ_BATH_F);
//   }
//
//   pChar->CRLF[0] = '\n';
//   pChar->CRLF[1] = '\0';
//}

/****************************** Riv_ExtrCharSale ****************************
 *
 *
 ****************************************************************************/

//int Riv_ExtrCharSale(char *pSaleFile, char *pCharFile)
//{
//   STDCHAR  *pChar;
//   char     *pTmp, acCharRec[2048], acSaleRec[1024], acTmpFile[_MAX_PATH];
//   long     lCnt=0;
//
//   // Check current sale file
//   if (_access(pSaleFile, 0))
//   {
//      LogMsg("***** Missing current sale file %s", pSaleFile);
//      return -1;
//   }
//
//   // Open current sale
//   LogMsg("Open current sale file %s", pSaleFile);
//   fdSale = fopen(pSaleFile, "r");
//   if (fdSale == NULL)
//   {
//      LogMsg("***** Error opening current sale file: %s\n", pSaleFile);
//      return -2;
//   }
//
//   // Create output file
//   sprintf(acTmpFile, "%s\\%s\\%s_Char.tmp", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
//   LogMsg("Create temporary char file %s", acTmpFile);
//   fdCChr = fopen(acTmpFile, "w");
//   if (fdCChr == NULL)
//   {
//      LogMsg("***** Error creating temporary char from current sale file: %s\n", acTmpFile);
//      return -3;
//   }
//
//   pChar = (STDCHAR *)&acCharRec[0];
//
//   // Merge loop
//   while (!feof(fdSale))
//   {
//      // Get current sale
//      pTmp = fgets(acSaleRec, 1024, fdSale);
//      if (!pTmp)
//         break;
//
//      // Format cumsale record
//      Riv_FormatCharRec(acCharRec, acSaleRec);
//      fputs(acCharRec, fdCChr);
//
//      if (!(++lCnt % 1000))
//         printf("\r%u", lCnt);
//   }
//
//   // Close files
//   if (fdCChr)
//      fclose(fdCChr);
//   if (fdSale)
//      fclose(fdSale);
//
//   LogMsg("Number of CHARS extracted: %d", lCnt);
//
//   // Sort output
//   sprintf(acCharRec, "S(1,%d,C,A) DUPOUT(B4096,1,%d)", iApnLen, sizeof(STDCHAR));
//   lCnt = sortFile(acTmpFile, pCharFile, acCharRec);
//   
//   return 0;
//}

/***************************** Riv_LoadGrGrMdb *******************************
 *
 * Extract Grgr data from Riverside_*.mdb and output to RIV_GRGR.DAT
 * then appended to RIV_GRGR.SLS
 * Input:  Riverside_mmddyyyy_mmddyyyy.mdb
 * Output: Riv_GrGr.tmp (GRGR_DOC format).  
 *
 * Return 0 if successful, Otherwise error
 *
 *****************************************************************************/

int Riv_LoadGrGrMdb(FILE *fdOut, char *pInfile)
{
   GRGR_DOC GrgrRec;
   char     acTmp[256], acTmp1[32], acTmpFile[_MAX_PATH], *pTmp;
   char     acMdbProvider[_MAX_PATH], acSaleTbl[32];

   //FILE     *fdOut;
   int      iTmp, iGrantee, iGrantor;
   long     lCnt=0, lRecOut=0, lTmp;
   CString  strFld, strDocNum;
   hlAdoRs  rsGrGr;
   hlAdo    hSaleDb;

   LogMsg0("Loading %s", pInfile);

   // Open database
   try 
   {
      GetIniString("Database", "MdbProvider", "", acMdbProvider, _MAX_PATH, acIniFile);
      GetIniString(myCounty.acCntyCode, "GrGrTbl", "", acSaleTbl, _MAX_PATH, acIniFile);

      strcpy(acTmpFile, acMdbProvider);
      strcat(acTmpFile, pInfile);
      if (!hSaleDb.Connect(acTmpFile))
      {
         LogMsg("***** ERROR accessing %s", pInfile);
         return -1;
      }
   } catch(_com_error &e)
   {
      LogMsg("***** Error open MDB %s: %s", pInfile, ComError(e));
      return -1;
   }

   // Open GrGr file
   LogMsg("Open GrGr table %s", acSaleTbl);
   try
   {
      sprintf(acTmp, "SELECT * FROM %s ORDER BY UserDocNumber", acSaleTbl);
      LogMsg("%s", acTmp);
      rsGrGr.Open(hSaleDb, acTmp);
   }
   AdoCatch(e)
   {
      LogMsg("***** Error open table %s: %s", acSaleTbl, ComError(e));
      return -2;
   }

   // Loop through record set
   strDocNum = "";
   memset((void *)&GrgrRec, ' ', sizeof(GRGR_DOC));
   while (rsGrGr.next())
   {  
      // Doc# 
      strFld = rsGrGr.GetItem("UserDocNumber");
      if (strFld != strDocNum)
      {
         // Save prev one
         if (strDocNum > " ")
         {
            if (iGrantee > 0)
            {
               iTmp = sprintf(acTmp, "%d", iGrantee);
               memcpy(GrgrRec.NameCnt, acTmp, iTmp);
            }
            GrgrRec.CRLF[0] = 10;
            GrgrRec.CRLF[1] = 0;
            fputs((char *)&GrgrRec,fdOut);
            lRecOut++;
         }

         strDocNum = strFld;
         memset((void *)&GrgrRec, ' ', sizeof(GRGR_DOC));
         memcpy(GrgrRec.DocNum, strFld, strFld.GetLength());
         iGrantee = 0;
         iGrantor = 0;
         GrgrRec.NoneSale = 'Y';
         GrgrRec.NoneXfer = 'Y';
      }

      // APN
      try
      {
         strFld = rsGrGr.GetItem("Parcel_No");
      } catch (...)
      {
         LogMsg("***** Invalid input file.  Missing field Parcel_No");
         break;
      }
      if (GrgrRec.APN[0] == ' ' && strFld > "0")
         memcpy(GrgrRec.APN, strFld, strFld.GetLength());
      else if (strFld > " " && memcmp(GrgrRec.APN, strFld, strFld.GetLength()))
         LogMsg("*** Multi APN? DocNum=%s", strDocNum);

      // Recording date
      if (GrgrRec.DocDate[0] == ' ')
      {
         lTmp = 0;
         strcpy(acTmp, rsGrGr.GetItem("RecordingDate"));
         pTmp = dateConversion(acTmp, acTmp1, MM_DD_YYYY_1);
         if (pTmp)
         {
            memcpy(GrgrRec.DocDate, acTmp1, 8);
            lTmp = atol(acTmp1);
         }

         if (lTmp > lLastRecDate && lTmp < lToday)
            lLastRecDate = lTmp;
      }

      // Grantee
      strFld = rsGrGr.GetItem("GRANTEE");
      if (strFld.GetLength() > 2)
      {
         if (iGrantee < 2)
         {
            strcpy(acTmp, strFld);
            remUnPrtChar(acTmp);
            memcpy(GrgrRec.Grantee[iGrantee], acTmp, strlen(acTmp));
         }
         iGrantee++;
      }

      // Grantor
      strFld = rsGrGr.GetItem("GRANTOR");
      if (strFld.GetLength() > 2 && iGrantor < 2)
      {
         strcpy(acTmp, strFld);
         remUnPrtChar(acTmp);
         memcpy(GrgrRec.Grantor[iGrantor++], acTmp, strlen(acTmp));
      }

      // Doc Code
      strFld = rsGrGr.GetItem("DOCTYPE");
      if (GrgrRec.DocTitle[0] = ' ')
      {
         memcpy(GrgrRec.DocTitle, strFld, strFld.GetLength());
         strFld.TrimRight();
         // Translate DocType
         // 149=DEED,38=EASEMENT DEED,39=PARTIAL EASEMENT DEED,481=TRUSTEES DEED, ... Riv_Grgr_DeedRef.csv
         iTmp = XrefCodeIndexEx((XREFTBL *)&asDeed[0], strFld.GetBuffer(0), iNumDeeds);
         if (iTmp >= 0)
         {
            GrgrRec.NoneSale = asDeed[iTmp].acFlags[0];
            GrgrRec.NoneXfer = asDeed[iTmp].acOther[0];
            iTmp = sprintf(acTmp, "%d", asDeed[iTmp].iIdxNum);
            memcpy(GrgrRec.DocType, acTmp, iTmp);
         } else if (acTmp[0] > ' ')
         {
            LogMsg("*** RIV GRGR - Unknown Doc code: [%s] APN=%.12s", strFld, GrgrRec.APN);
         }
      }

      // Num pages
      strFld = rsGrGr.GetItem("NumOfPages");
      if (GrgrRec.NumPages[0] == ' ')
         memcpy(GrgrRec.NumPages, strFld, strFld.GetLength());

      // DocNumber
      strFld = rsGrGr.GetItem("DocNumber");
      if (GrgrRec.ReferenceData[0] == ' ')
         memcpy(GrgrRec.ReferenceData, strFld, strFld.GetLength());

      /*
      sTmp = rsGrGr.GetItem("DocTax");
      lTmp = atol(sTmp);
      if (lTmp > 1000)
      {
         sprintf(acTmp, "%*ld", SIZ_GD_TAX, lTmp);
         memcpy(GrGrRec.DocTax, acTmp, SIZ_GD_TAX);
      }

      sTmp = rsGrGr.GetItem("SalePrice");
      lTmp = atol(sTmp);
      if (lTmp > 1000)
      {
         sprintf(acTmp, "%*ld", SIZ_GD_SALE, (lTmp/100)*100);
         memcpy(GrGrRec.SalePrice, acTmp, SIZ_GD_SALE);
      }
      */

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   rsGrGr.Close();
   hSaleDb.~hlAdo();
   
   // Sort output file
   LogMsg("Total processed records: %u", lCnt);
   LogMsg("         output records: %u", lRecOut);
   LogMsg("    Last recording date: %u\n", lLastRecDate);

   return 0;
}

/***************************** Riv_LoadGrGrCsv *******************************
 *
 * Extract Grgr data from Riverside_*.csv and output to RIV_GRGR.DAT
 * then appended to RIV_GRGR.SLS
 * Input:  Riverside_mmddyyyy_mmddyyyy.csv
 * Output: Riv_GrGr.tmp (GRGR_DOC format).  
 *
 * Return 0 if successful, Otherwise error
 *
 *****************************************************************************/

int Riv_LoadGrGrCsv(FILE *fdOut, char *pInfile)
{
   GRGR_DOC GrgrRec;
   char     acRec[256], acTmp[256], acTmp1[32], *pTmp, acDocNum[32];

   int      iTmp, iGrantee, iGrantor;
   long     lCnt=0, lRecOut=0, lTmp;
   FILE     *fdIn;

   LogMsg0("Loading %s", pInfile);

   if (!(fdIn = fopen(pInfile, "r")))
   {
      LogMsg("***** Error opening %s", pInfile);
      return -1;
   }

   // Loop through record set
   acDocNum[0] = 0;
   memset((void *)&GrgrRec, ' ', sizeof(GRGR_DOC));
   while (!feof(fdIn))
   {  
      pTmp = fgets(acRec, MAX_RECSIZE, fdIn);
      if (!pTmp)
         break;

      if (acRec[0] > '9')
         break;

      iTokens = ParseString(acRec, ',', 16, apTokens);
      if (iTokens < RIV_GR_FLDS)
      {
         LogMsg("*** Skip invalid rec, number of token=%d (line %d)", iTokens, lCnt);
         continue;
      }

      // Doc# 
      if (strcmp(acDocNum, apTokens[RIV_GR_DOCNUM]))
      {
         // Save prev one
         if (acDocNum[0] > ' ')
         {
            if (iGrantee > 0)
            {
               iTmp = sprintf(acTmp, "%d", iGrantee);
               memcpy(GrgrRec.NameCnt, acTmp, iTmp);
            }
            GrgrRec.CRLF[0] = 10;
            GrgrRec.CRLF[1] = 0;
            fputs((char *)&GrgrRec,fdOut);
            lRecOut++;
         }

         strcpy(acDocNum, apTokens[RIV_GR_DOCNUM]);
         memset((void *)&GrgrRec, ' ', sizeof(GRGR_DOC));
         memcpy(GrgrRec.DocNum, acDocNum, strlen(acDocNum));
         iGrantee = 0;
         iGrantor = 0;
         GrgrRec.NoneSale = 'Y';
         GrgrRec.NoneXfer = 'Y';
      }

      // APN
      if (GrgrRec.APN[0] == ' ' && acDocNum[0] > ' ')
         memcpy(GrgrRec.APN, apTokens[RIV_GR_APN], strlen(apTokens[RIV_GR_APN]));
      else if (*apTokens[RIV_GR_APN] > ' ' && memcmp(GrgrRec.APN, apTokens[RIV_GR_APN], strlen(apTokens[RIV_GR_APN])))
         LogMsg("*** Multi APN? DocNum=%s (line %d)", acDocNum, lCnt);

      // Recording date
      if (GrgrRec.DocDate[0] == ' ')
      {
         lTmp = 0;
         strcpy(acTmp, apTokens[RIV_GR_RECDATE]);
         pTmp = dateConversion(acTmp, acTmp1, MM_DD_YYYY_1);
         if (pTmp)
         {
            memcpy(GrgrRec.DocDate, acTmp1, 8);
            lTmp = atol(acTmp1);
         }

         if (lTmp > lLastRecDate && lTmp < lToday)
            lLastRecDate = lTmp;
      }

      // Grantee
      if (strlen(apTokens[RIV_GR_GRANTEE]) > 2)
      {
         if (iGrantee < 2)
         {
            iTmp = remUnPrtChar(apTokens[RIV_GR_GRANTEE]);
            memcpy(GrgrRec.Grantee[iGrantee], apTokens[RIV_GR_GRANTEE], iTmp);
         }
         iGrantee++;
      }

      // Grantor
      if (strlen(apTokens[RIV_GR_GRANTOR]) > 2 && iGrantor < 2)
      {
         iTmp = remUnPrtChar(apTokens[RIV_GR_GRANTOR]);
         memcpy(GrgrRec.Grantor[iGrantor++], apTokens[RIV_GR_GRANTOR], iTmp);
      }

      // Doc Code
      if (GrgrRec.DocTitle[0] = ' ')
      {
         memcpy(GrgrRec.DocTitle, apTokens[RIV_GR_DOCTYPE], strlen(apTokens[RIV_GR_DOCTYPE]));
         // Translate DocType
         // 149=DEED,38=EASEMENT DEED,39=PARTIAL EASEMENT DEED,481=TRUSTEES DEED, ... Riv_Grgr_DeedRef.csv
         iTmp = XrefCodeIndexEx((XREFTBL *)&asDeed[0], apTokens[RIV_GR_DOCTYPE], iNumDeeds);
         if (iTmp >= 0)
         {
            GrgrRec.NoneSale = asDeed[iTmp].acFlags[0];
            GrgrRec.NoneXfer = asDeed[iTmp].acOther[0];
            iTmp = sprintf(acTmp, "%d", asDeed[iTmp].iIdxNum);
            memcpy(GrgrRec.DocType, acTmp, iTmp);
         } else if (acTmp[0] > ' ')
         {
            LogMsg("*** RIV GRGR - Unknown Doc type: [%s] DocNum=%.12s", apTokens[RIV_GR_DOCTYPE], apTokens[RIV_GR_DOCNUM]);
         }
      }

      // Num pages
      if (GrgrRec.NumPages[0] == ' ')
         vmemcpy(GrgrRec.NumPages, apTokens[RIV_GR_NUMPAGES], SIZ_GD_NUMPAGES);

      // Ref DocNumber
      if (GrgrRec.ReferenceData[0] == ' ')
         vmemcpy(GrgrRec.ReferenceData, apTokens[RIV_GR_DOCTITLE], SIZ_GD_REF);

      /*
      sTmp = rsGrGr.GetItem("DocTax");
      lTmp = atol(sTmp);
      if (lTmp > 1000)
      {
         sprintf(acTmp, "%*ld", SIZ_GD_TAX, lTmp);
         memcpy(GrGrRec.DocTax, acTmp, SIZ_GD_TAX);
      }

      sTmp = rsGrGr.GetItem("SalePrice");
      lTmp = atol(sTmp);
      if (lTmp > 1000)
      {
         sprintf(acTmp, "%*ld", SIZ_GD_SALE, (lTmp/100)*100);
         memcpy(GrGrRec.SalePrice, acTmp, SIZ_GD_SALE);
      }
      */

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   fclose(fdIn);

   LogMsg("Total processed records: %u", lCnt);
   LogMsg("         output records: %u", lRecOut);
   LogMsg("    Last recording date: %u\n", lLastRecDate);

   return 0;
}

int Riv_LoadGrGr(char *pCnty)
{
   char     *pTmp, acRec[MAX_RECSIZE], acTmp[256];
   char     acInFile[_MAX_PATH], acGrGrIn[_MAX_PATH], acGrGrOut[_MAX_PATH], acGrGrBak[_MAX_PATH];
   int      iRet, iTmp;
   long     lCnt, lHandle;
   bool     bSuccess = true;

   FILE     *fdOut;
   struct   _finddata_t  sFileInfo;

   LogMsg0("Load %s GrGr", pCnty);

   // Get raw file name
   GetIniString(pCnty, "GrGrIn", "", acGrGrIn, _MAX_PATH, acIniFile);

   strcpy(acGrGrBak, acGrGrIn);
   pTmp = strrchr(acGrGrBak, '\\');
   if (pTmp)
      *++pTmp = 0;
   dateString(acRec, 0);
   sprintf(acTmp, "GrGr_%s", acRec);
   strcat(acGrGrBak, acTmp);

   // Create temp output file
   sprintf(acGrGrOut, acGrGrTmpl, pCnty, pCnty, "TMP");

   LogMsg("Open output file %s", acGrGrOut);
   fdOut = fopen(acGrGrOut, "w");
   if (fdOut == NULL)
   {
      printf("Error creating GrGr output file: %s\n", acGrGrOut);
      return -1;
   }

   // Open Input file
   lHandle = _findfirst(acGrGrIn, &sFileInfo);
   if (lHandle > 0)
   {
      pTmp = strrchr(acGrGrIn, '\\');
      if (pTmp)
         *pTmp = 0;
      iRet = 0;
   } else
   {
      bGrGrAvail = false;
      LogMsg("*** No new GrGr file to process");
      fclose(fdOut);
      return 1;
   }

   // Create backup folder
   if (_access(acGrGrBak, 0))
      _mkdir(acGrGrBak);

   while (!iRet)
   {
      // Create input name
      sprintf(acInFile, "%s\\%s", acGrGrIn, sFileInfo.name);
 
      // Update GrGr file date
      iRet = getFileDate(acInFile);
      if (iRet > lLastGrGrDate)
         lLastGrGrDate = iRet;

      if (strstr(acInFile, ".csv"))
      {
         // Rebuild CSV file to repair broken records
         sprintf(acRec, "%s\\RIV\\%s", acTmpPath, sFileInfo.name);
         iRet = RebuildCsv(acInFile, acRec, ',', RIV_GR_FLDS);
         if (iRet < 0)
         {
            LogMsg("***** Bad input file: %s", acInFile);
            goto Next_GrFile;
         }

         // Sort input file
         sprintf(acInFile, "%s\\RIV\\%s.srt", acTmpPath, sFileInfo.name);
         iRet = sortFile(acRec, acInFile, "S(#6,C,A) F(TXT) DEL(44)");
         if (iRet < 10)
            return -2;

         iRet = Riv_LoadGrGrCsv(fdOut, acInFile);
      } else
         iRet = Riv_LoadGrGrMdb(fdOut, acInFile);
      if (iRet < 0)
      {
         bSuccess = false;
         break;
      }

      // Move file
      sprintf(acTmp, "%s\\%s", acGrGrBak, sFileInfo.name);
      sprintf(acRec, "%s\\%s", acGrGrIn, sFileInfo.name);
      rename(acRec, acTmp);

      // Find next file
Next_GrFile:
      iRet = _findnext(lHandle, &sFileInfo);
   }

   // Close handle
   _findclose(lHandle);

   // Close files
   if (fdOut)
      fclose(fdOut);

   if (!bSuccess)
      return -1;

    // Sort output file and dedup
   char acSrtFile[_MAX_PATH];
   sprintf(acSrtFile, acGrGrTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "Srt");
   sprintf(acRec, acGrGrTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "Sls");      // accummulated sale file
   if (!_access(acRec, 0))
      sprintf(acInFile, "%s+%s", acGrGrOut, acRec);
   else
      strcpy(acInFile, acGrGrOut);

   // Sort by APN, RecDate, DocNum
   LogMsg("Sorting %s to %s.", acInFile, acSrtFile);
   sprintf(acTmp, "S(%d,12,C,A,%d,8,C,A,%d,12,C,A) F(TXT) OMIT(%d,1,C,EQ,\" \") DUPO(1,47) ", 
      OFF_GD_APN, OFF_GD_DOCDATE, OFF_GD_DOCNUM, OFF_GD_APN);

   // Sort on APN asc, DocDate asc, DocNum asc
   lCnt = sortFile(acInFile, acSrtFile, acTmp);
   if (lCnt > 0)
   {
      if (!_access(acRec, 0))
         DeleteFile(acRec);

      // Rename Dat to SLS file
      iTmp = rename(acSrtFile, acRec);
      LogMsg("GRGR process completed: %d", lCnt);
      iRet = 0;
   } else
   {
      iRet = -1;
      LogMsg("***** Error sorting output file in Riv_LoadGrGr()");
   }

   return iRet;
}

/***************************** Riv_ParseTaxDetail ****************************
 * 
 * In RIV, we use Assessment Number for APN
 *
 * Return 0 if success
 *
 *****************************************************************************/

int Riv_ParseTaxDetail(FILE *fdDetail, FILE *fdAgency, char *pInbuf, char *pBillNum)
{
   char     acTmp[512], acDetail[512], acAgency[512];
   int      iIdx, iTmp;
   double   dTmp, dTotalTax;

   RIV_DETAIL *pInRec  = (RIV_DETAIL *)pInbuf;
   TAXDETAIL  *pDetail = (TAXDETAIL *)acDetail;
   TAXAGENCY  *pAgency = (TAXAGENCY *)acAgency, *pResult;

   // Clear output buffer
   memset(acDetail, 0, sizeof(TAXDETAIL));
   memset(acAgency, 0, sizeof(TAXAGENCY));

   // APN
   memcpy(pDetail->Apn, pInRec->Assmnt_No, iApnLen);

   // Tax Year
   iTmp = sprintf(pDetail->TaxYear, "%d", lTaxYear);

   // Bill Number
   memcpy(pDetail->BillNum, pBillNum, TSIZ_BILNUM);

#ifdef _DEBUG
   //if (!memcmp(pDetail->Apn, "009000001", 9) )
   //   iTmp = 0;
#endif

   // Handle regular county agency
   dTotalTax = 0;
   for (iIdx = 0; iIdx < TD_SIZ_AGENCY; iIdx++)
   {
      dTmp = atofn(pInRec->Agency[iIdx].Amount, TD_SIZ_TAXAMT);
      if (dTmp > 0.0)
      {
         dTotalTax += dTmp;

         // Tax amt
         sprintf(pDetail->TaxAmt, "%.2f", dTmp);

         // Tax Rate
         dTmp = atofn(pInRec->Agency[iIdx].Rate, TD_SIZ_TAXRATE);
         sprintf(pDetail->TaxRate, "%.5f", dTmp);

         // Tax code
         memcpy(pDetail->TaxCode, pInRec->Agency[iIdx].AgencyId, TD_SIZ_TYPE);

         // Create Agency record
         strcpy(pAgency->Code, pDetail->TaxCode);
         pAgency->TC_Flag[0] = pDetail->TC_Flag[0];

         //strcpy(pAgency->TaxRate, pDetail->TaxRate);
         pResult = findTaxAgency(pAgency->Code);
         if (pResult)
         {
            strcpy(pAgency->Agency, pResult->Agency);
            pAgency->TC_Flag[0] = pResult->TC_Flag[0];
            pDetail->TC_Flag[0] = pResult->TC_Flag[0];
         } else
         {
            sprintf(pAgency->Agency, "%s - SPECIAL ASSESSMENT", pAgency->Code);
            LogMsg("+++ Unknown TaxCode: %s", pDetail->TaxCode);
            pDetail->TC_Flag[0] = '0';
            pAgency->TC_Flag[0] = '0';
         }

         Tax_CreateAgencyCsv(acTmp, pAgency);
         fputs(acTmp, fdAgency);

         // Generate csv line and write to file
         Tax_CreateDetailCsv(acTmp, pDetail);
         fputs(acTmp, fdDetail);
      } else
         break;
   }

   // Preset tax rate
   strcpy(pDetail->TaxRate, "0.00000");

   // Handle special tax
   for (iIdx = 0; iIdx < TD_SIZ_SPCTAX; iIdx++)
   {
      double   dAmtA, dAmtB;

      dAmtA = atofn(pInRec->SpcTax[iIdx].Spec_AmtA, TD_SIZ_TAXAMT);
      dAmtB = atofn(pInRec->SpcTax[iIdx].Spec_AmtB, TD_SIZ_TAXAMT);
      dTmp = dAmtA + dAmtB;
      if (dTmp == 0.0)
         break;

      // Tax amt
      sprintf(pDetail->TaxAmt, "%.2f", dTmp);

      // Tax code
      memcpy(pDetail->TaxCode, pInRec->SpcTax[iIdx].Spec_Type, TD_SIZ_TYPE);

      // Create Agency record
      strcpy(pAgency->Code, pDetail->TaxCode);
      pResult = findTaxAgency(pAgency->Code);
      if (pResult)
      {
         strcpy(pAgency->Agency, pResult->Agency);
         pAgency->TC_Flag[0] = pResult->TC_Flag[0];
         pDetail->TC_Flag[0] = pResult->TC_Flag[0];
      } else
      {
         sprintf(pAgency->Agency, "%s - SPECIAL ASSESSMENT", pAgency->Code);
         LogMsg("+++ Unknown TaxCode: %s", pDetail->TaxCode);
         pAgency->TC_Flag[0] = '0';
         pDetail->TC_Flag[0] = '0';
      }

      // Generate csv line and write to file
      Tax_CreateDetailCsv(acTmp, pDetail);
      fputs(acTmp, fdDetail);

      Tax_CreateAgencyCsv(acTmp, pAgency);
      fputs(acTmp, fdAgency);
   }

   return 0;
}

/***************************** Riv_ParseTaxBase ******************************
 *
 *
 *****************************************************************************/

int Riv_ParseTaxBase(char *pOutbuf, char *pInbuf)
{
   double   dTmp, dTotalTax;

   TAXBASE    *pOutRec = (TAXBASE *)pOutbuf;
   RIV_DETAIL *pInRec  = (RIV_DETAIL *)pInbuf;

   // Clear output buffer
   memset(pOutbuf, 0, sizeof(TAXBASE));

   // APN
   memcpy(pOutRec->Apn, pInRec->Assmnt_No, iApnLen);

   // TRA
   // BillNum

   // Tax Year
   sprintf(pOutRec->TaxYear, "%d", lTaxYear);
   strcpy(pOutRec->OwnerInfo.TaxYear, pOutRec->TaxYear);

   // Check for Tax amount
   dTotalTax = atofn(pInRec->TotalAmount, TD_SIZ_TOTALAMT);
   if (dTotalTax > 0.0)
   {
      pOutRec->dTotalTax = dTotalTax;
      sprintf(pOutRec->TotalTaxAmt, "%.2f", dTotalTax);
      sprintf(pOutRec->TotalDue, "%.2f", dTotalTax);
      dTmp = dTotalTax / 2.0;
      sprintf(pOutRec->TaxAmt1, "%.2f", dTmp);
      strcpy(pOutRec->TaxAmt2, pOutRec->TaxAmt1);

      // Due date
      InstDueDate(pOutRec->DueDate1, 1, lTaxYear);
      InstDueDate(pOutRec->DueDate2, 2, lTaxYear);
   }

   // Tax rate
   dTmp = atofn(pInRec->Tax_Rate, TD_SIZ_TAXRATE);
   if (dTmp > 0.0)
      sprintf(pOutRec->TotalRate, "%.5f", dTmp);

   pOutRec->isDelq[0] = '0';
   pOutRec->isSecd[0] = '1';
   pOutRec->isSupp[0] = '0';
   pOutRec->BillType[0] = BILLTYPE_SECURED;

   return 0;
}

/****************************** Riv_MergeTaxPaid *****************************
 *
 * Update Base record using TRA, BillNum, PaidAmt, Penalty, Cost, and paid status from paid file
 *
 *****************************************************************************/

int Riv_MergeTaxPaid(char *pOutbuf)
{
   static   char  acRec[1024], *pRec=NULL, *apItems[16], acApn[32];
   TAXBASE  *pTaxBase = (TAXBASE *)pOutbuf;

   int      iLoop, iTmp;
   double   dTax1, dTax2, dPaid1, dPaid2, dPen1, dPen2, dFee1, dFee2, dTotalFee, dTotalPen, dTotalDue, dTotalPaid, dTotalTax;
   char     StatInst1, StatInst2, sBillNum[32];

   // Get first Sale rec for first call
   if (!pRec)
   {
      do {
         pRec = fgets(acRec, 1024, fdPaid);
         iTmp = strchr(acRec, '|') -  &acRec[0];
      } while (iTmp != iApnLen);
   }

   do
   {
      // Compare Apn
      iLoop = memcmp(pTaxBase->Apn, acRec, iApnLen);
      if (iLoop > 0)
      {
         if (bDebug)
            LogMsg0("Skip Paid rec  %.*s", iApnLen, acRec);
         pRec = fgets(acRec, 1024, fdPaid);
         if (!pRec)
         {
            fclose(fdPaid);
            fdPaid = NULL;
            return -1;      // EOF
         }
         iTmp = strchr(acRec, '|') -  &acRec[0];
      } else
         iTmp = iApnLen;
   } while (iLoop > 0 || iTmp != iApnLen);

   // If not match, return
   if (iLoop)
      return 1;

   dTax1=dTax2=dTotalTax=dPaid1=dPaid2=dPen1=dPen2=dFee1=dFee2=dTotalFee=dTotalPen=dTotalPaid=dTotalDue = 0.0;
   StatInst1=StatInst2 = TAX_BSTAT_UNPAID;

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "008102853", 9))
   //   iLoop = 0;
#endif
   sBillNum[0] = 0;
   do
   {
      iTmp = ParseString(acRec, '|', MAX_FLD_TOKEN, apItems);
      if (iTmp < RIV_PS_COLS)
      {
         LogMsg("***** Error: bad Tax paid record for APN=%s (#tokens=%d)", acRec, iTmp);
         return -1;
      }
      if (!sBillNum[0])
         strcpy(sBillNum, apItems[RIV_PS_BILLNUMBER]);

      // Add up tax paid amt and penaty
      switch (*apItems[RIV_PS_PAIDINDICATOR])
      {
         case '1':      // Inst1 paid
            //dTax1  = atof(apItems[RIV_PS_INSTALLMENT]);  // The installment is not reliable
            dTax1  = atof(pTaxBase->TaxAmt1);
            dPaid1 = dTax1;
            dPen1  = atof(apItems[RIV_PS_PENALITY]);
            dFee1  = atof(apItems[RIV_PS_COSTAMOUNT]);
            StatInst1 = TAX_BSTAT_PAID;      // Inst1 paid
            break;
         case '2':      // Inst2 unpaid
            //dTax2  = atof(apItems[RIV_PS_INSTALLMENT]);
            dTax2  = atof(pTaxBase->TaxAmt2);
            dPaid2 = 0;
            dPen2  = atof(apItems[RIV_PS_PENALITY]);
            dFee2  = atof(apItems[RIV_PS_COSTAMOUNT]);
            StatInst2 = TAX_BSTAT_UNPAID;       // Inst2 unpaid
            dTotalDue = dTax2+dPen2+dFee2;
            break;
         case 'P':      // Both paid
            dTotalTax  = pTaxBase->dTotalTax;
            dTotalPaid = dTotalTax;    // 639172029 01/15/2020
            dTotalPen = atof(apItems[RIV_PS_PENALITY]);
            dTotalFee = atof(apItems[RIV_PS_COSTAMOUNT]);
            StatInst1 = TAX_BSTAT_PAID;    
            StatInst2 = TAX_BSTAT_PAID;    
            dTotalDue = 0;
            break;
         case 'B':      // Both unpaid
            dTotalTax  = pTaxBase->dTotalTax;
            dTotalPaid = 0;
            dTotalPen  = atof(apItems[RIV_PS_PENALITY]);
            dTotalFee  = atof(apItems[RIV_PS_COSTAMOUNT]);
            StatInst1 = TAX_BSTAT_UNPAID;    
            StatInst2 = TAX_BSTAT_UNPAID;    
            dTotalDue = dTotalTax+dTotalPen+dTotalFee;
            break;
         default:
            LogMsg("*** Unknown paid indicator: %s", apItems[RIV_PS_PAIDINDICATOR]);
            break;
      }

      // TRA
      memcpy(pTaxBase->TRA, apItems[RIV_PS_TRA], TSIZ_TRA);

      do {
         // Get next paid record
         pRec = fgets(acRec, 1024, fdPaid);
         if (!pRec)
         {
            iTmp = iApnLen;
            fclose(fdPaid);
            fdPaid = NULL;
         } else
            iTmp = strchr(acRec, '|') -  &acRec[0];
      } while (iTmp != iApnLen);
   } while (pRec && !memcmp(pTaxBase->Apn, acRec, iApnLen));

   if (!dTotalTax)
   {
      dTotalTax = dTax1 + dTax2;
   }

   // This code work temporary before 4/10/2020 only
   if (dTotalPen > 0.0)
   {
      dPen1 = dTotalPen;
      sprintf(pTaxBase->PenAmt1, "%.2f", dPen1);
   }

   if (dTotalFee > 0.0)
   {
      dFee1 = dTotalFee;
      sprintf(pTaxBase->TotalFees, "%.2f", dTotalFee);
   }

   // If both paid
   if (dTotalPaid > 0.0)
   {
      dPaid1 = atof(pTaxBase->TaxAmt1) + dPen1 + dFee1;
      dPaid2 = dTotalPaid - dPaid1;
   } else
      dTotalPaid = dPaid1 + dPaid2;

#if _DEBUG
   //if (dTotalDue > dTotalTax && dPen1 == 0)
   //   pRec = NULL;
#endif

   pTaxBase->Inst1Status[0] = StatInst1;
   pTaxBase->Inst2Status[0] = StatInst2;
   memcpy(pTaxBase->BillNum, sBillNum, TSIZ_BILNUM);

   // Update TaxBase record
   if (dPaid1 > 0.0)
      sprintf(pTaxBase->PaidAmt1, "%.2f", dPaid1);
   if (dPaid2 > 0.0)
      sprintf(pTaxBase->PaidAmt2, "%.2f", dPaid2);
   if (dTotalDue > 0.0)
      sprintf(pTaxBase->TotalDue, "%.2f", dTotalDue);

   return 0;
}

/**************************** Riv_Load_TaxBase *******************************
 *
 * Create base import file and import into SQL if specified
 * Input: AG660TA (detail ASCII file) contains current tax roll
 *        ASC820CD (paid EBCDIC file) contains tax paid data.
 *
 * Return 0 if success.
 *
 *****************************************************************************/

int Riv_Load_TaxBase(char *pPaidFile, bool bImport)
{
   char     *pTmp, acBuf[1024], acRec[MAX_RECSIZE];
   char     acBaseFile[_MAX_PATH], acDetailFile[_MAX_PATH], acInFile[_MAX_PATH];
   char     acTmpFile[_MAX_PATH], acAgencyFile[_MAX_PATH];

   int      iRet;
   long     lOut=0, lCnt=0;
   FILE     *fdBase, *fdDetail, *fdAgency, *fdIn;
   TAXBASE  *pTaxBase = (TAXBASE *)&acBuf[0];

   LogMsg("Loading tax file");

   GetIniString(myCounty.acCntyCode, "TaxDetail", "", acInFile, _MAX_PATH, acIniFile);
   NameTaxCsvFile(acBaseFile, myCounty.acCntyCode, "Base");
   NameTaxCsvFile(acDetailFile, myCounty.acCntyCode, "Items");
   NameTaxCsvFile(acAgencyFile, myCounty.acCntyCode, "Agency");

   // Open input file
   LogMsg("Open current tax file %s", acInFile);
   fdIn = fopen(acInFile, "r");
   if (fdIn == NULL)
   {
      LogMsg("***** Error opening Current tax file: %s\n", acInFile);
      return -2;
   }  

   // Open input paid file
   LogMsg("Open tax file %s", pPaidFile);
   fdPaid = fopen(pPaidFile, "r");
   if (fdPaid == NULL)
   {
      LogMsg("***** Error opening Current tax paid file: %s\n", pPaidFile);
      return -2;
   }  

   // Open Detail file
   LogMsg("Open Detail output file %s", acDetailFile);
   fdDetail = fopen(acDetailFile, "w");
   if (fdDetail == NULL)
   {
      LogMsg("***** Error creating Detail output file: %s\n", acDetailFile);
      return -4;
   }

   // Open base file
   LogMsg("Open Base file %s", acBaseFile);
   fdBase = fopen(acBaseFile, "w");
   if (fdBase == NULL)
   {
      LogMsg("***** Error creating base file: %s\n", acBaseFile);
      return -4;
   }

   // Open Agency file
   sprintf(acTmpFile, "%s\\%s\\%s_Agency.tmp", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   LogMsg("Open Agency file %s", acTmpFile);
   fdAgency = fopen(acTmpFile, "w");
   if (fdAgency == NULL)
   {
      LogMsg("***** Error creating Agency file: %s\n", acTmpFile);
      return -4;
   }

   // Merge loop 
   while (!feof(fdIn))
   {
      pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdIn);
      if (!pTmp)
         break;

      // Create new R01 record
      iRet = Riv_ParseTaxBase(acBuf, acRec);
      if (!iRet)
      {
         // Pull TRA, BillNum, PaidStatus from Paid file
         // to populate Base & Detail records
         iRet = Riv_MergeTaxPaid(acBuf);

         // Create detail & agency records
         iRet = Riv_ParseTaxDetail(fdDetail, fdAgency, acRec, pTaxBase->BillNum);

         // Create TaxBase record
         Tax_CreateTaxBaseCsv(acRec, (TAXBASE *)&acBuf);
         lOut++;
         fputs(acRec, fdBase);
      } else
      {
         LogMsg("---> Drop record %d [%.40s]", lCnt, acRec); 
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdIn)
      fclose(fdIn);
   if (fdPaid)
      fclose(fdPaid);
   if (fdBase)
      fclose(fdBase);
   if (fdDetail)
      fclose(fdDetail);
   if (fdAgency)
      fclose(fdAgency);

   LogMsg("Total records processed:    %u", lCnt);
   LogMsg("Total output records:       %u", lOut);
   LogMsg("Total paid records update:  %u", lSaleMatch);

   // Dedup Agency file
   iRet = sortFile(acTmpFile, acAgencyFile, "S(#1,C,A) DUPO F(TXT)");

   // Import into SQL
   if (bImport)
   {
      iRet = doTaxImport(myCounty.acCntyCode, TAX_BASE);
      if (!iRet)
         iRet = doTaxImport(myCounty.acCntyCode, TAX_DETAIL);
      if (!iRet)
      {
         iRet = doTaxImport(myCounty.acCntyCode, TAX_AGENCY);

         // Merge tax agencies
         //iRet = updateTaxAgency(myCounty.acCntyCode);
      }
   } else
      iRet = 0;

   return iRet;
}

/******************************* Riv_Load_TaxPaid *****************************
 *
 * Load Tax paid/Unpaid file
 *
 ******************************************************************************/

int Riv_Load_TaxPaid(char *pPaidFile, bool bImport)
{
   char     *pTmp, acBuf[1024], acRec[MAX_RECSIZE];
   char     acTmpFile[_MAX_PATH];

   int      iRet, lCnt=0;
   FILE     *fdIn, *fdOut;
   TAXPAID  *pTaxPaid = (TAXPAID *)&acBuf[0];
   RIV_PAID *pRivPaid = (RIV_PAID *)&acRec[0];
   double   dTmp, dTaxAmt;


   LogMsg("Loading tax paid file");

   // Open input paid file
   LogMsg("Open tax file %s", pPaidFile);
   fdIn = fopen(pPaidFile, "r");
   if (fdIn == NULL)
   {
      LogMsg("***** Error opening Current tax paid file: %s\n", pPaidFile);
      return -2;
   }  

   // Create paid output file
   NameTaxCsvFile(acTmpFile, myCounty.acCntyCode, "Paid");
   LogMsg("Open Paid file %s", acTmpFile);
   fdOut = fopen(acTmpFile, "w");
   if (fdOut == NULL)
   {
      LogMsg("***** Error creating Paid file: %s\n", acTmpFile);
      return -4;
   }

   while (!feof(fdIn))
   {
      pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdIn);
      if (!pTmp)
         break;

      // Clear output buffer
      memset(acBuf, 0, sizeof(TAXPAID));

#ifdef _DEBUG
      //if (!memcmp(pRivPaid->Assmnt_No, "008524989", 9) )
      //   iRet = 0;
#endif

      // APN
      memcpy(pTaxPaid->Apn, pRivPaid->Assmnt_No, iApnLen);

      // Bill No
      memcpy(pTaxPaid->BillNum, pRivPaid->BillNum, TSIZ_BILNUM);

      // TRA
      //memcpy(pTaxPaid->TRA, pRivPaid->TRA, TSIZ_TRA);

      // District
      //memcpy(pTaxPaid->Dist_No, pRivPaid->Dist_No, TSIZ_DIST_NO);

      // Paid indicator
      pTaxPaid->PaidFlg[0] = pRivPaid->Paid_Ind;

      // Tax amt
      dTaxAmt = atofn(pRivPaid->Tax_Amt, TSIZ_TAXAMT);
      if (dTaxAmt > 0.0)
         sprintf(pTaxPaid->TaxAmt, "%.2f", dTaxAmt/100.0);

      // Pen amt
      dTmp = atofn(pRivPaid->Pen_Amt, TSIZ_PENAMT);
      if (dTmp > 0.0)
      {
         sprintf(pTaxPaid->PenAmt, "%.2f", dTmp/100.0);
         dTaxAmt += dTmp;
      }

      // Fee amt
      dTmp = atofn(pRivPaid->Fee_Amt, TSIZ_FEEAMT);
      if (dTmp > 0.0)
      {
         sprintf(pTaxPaid->FeeAmt, "%.2f", dTmp/100.0);
         dTaxAmt += dTmp;
      }

      if (dTaxAmt > 0.0)
         sprintf(pTaxPaid->PaidAmt, "%.2f", dTmp/100.0);

      Tax_CreatePaidCsv(acRec, (TAXPAID *)&acBuf);
      fputs(acRec, fdOut);

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   if (fdOut)
      fclose(fdOut);
   if (fdIn)
      fclose(fdIn);

   LogMsg("Total paid records processed:    %u", lCnt);

   // Import into SQL
   if (bImport)
      iRet = doTaxImport(myCounty.acCntyCode, TAX_PAID);
   else
      iRet = 0;

   return iRet;
}

/********************************* Load_TaxDelq *****************************
 *
 * Loading ASPYMIDY
 *
 ****************************************************************************/

int Riv_ParseRdmN(char *pOutbuf, char *pInbuf)
{
   double   dTaxAmt;
   char     acTmp[256];

   TAXDELQ  *pOutRec= (TAXDELQ *)pOutbuf;
   RIV_RDMN *pInRec = (RIV_RDMN *)pInbuf;

   // Clear output buffer
   memset(pOutbuf, 0, sizeof(TAXDELQ));

   // APN
   memcpy(pOutRec->Apn, pInRec->Assmnt_No, TRT_SIZ_ASMT_NO);

   // Default Number
   memcpy(pOutRec->Default_No, pInRec->Default_No[0], TRN_SIZ_DEFAULT_NO);

   // Updated date
   sprintf(pOutRec->Upd_Date, "%d", lLastTaxFileDate);

   // Tax Year - get this from 'T' record

   // Default date - first 4-digit of Default_No is delq year
   //memcpy(pOutRec->Def_Date, pInRec->Default_No[0], 4);

   // Check status
   pOutRec->isDelq[0] = '1';
   if (!memcmp(pInRec->Status, "REDEEM", 3))             // Delinquency has been fully-paid
   {
      // Red amount
      dTaxAmt  = atofn(pInRec->Red_Amt, TRT_SIZ_TAX_AMT);
      if (dTaxAmt > 0.0)
         sprintf(pOutRec->Red_Amt, "%.2f", dTaxAmt/100.0);
      pOutRec->DelqStatus[0] = TAX_STAT_REDEEMED;
      pOutRec->isDelq[0] = 0;

      // Red date
      if (isdigit(pInRec->Coll_Date[0]))
      {
         sprintf(acTmp, "%.4s%.2s%.2s", &pInRec->Coll_Date[6], &pInRec->Coll_Date[0], &pInRec->Coll_Date[3]);
         memcpy(pOutRec->Red_Date, acTmp, 8);
      } else
         LogMsg("*** Invalid redemption date: %.10s", pInRec->Coll_Date);
   } else if (!memcmp(pInRec->Status, "COMPLETE", 3))    // Delq
   {
      pOutRec->DelqStatus[0] = TAX_STAT_UNPAID;
   } else if (!memcmp(pInRec->Status, "IPP LAPSED", 5))  // IPP plan has been lapsed (due to failure to make required payments)
   {
      pOutRec->DelqStatus[0] = TAX_STAT_DFLTINST;
   } else if (!memcmp(pInRec->Status, "IPP", 3))         // Installment plan in good standine
   {
      pOutRec->DelqStatus[0] = TAX_STAT_ONPAYMENT;
      pOutRec->isDelq[0] = 0;
   } else if (!memcmp(pInRec->Status, "LITIGATION", 3))  // Bankruptcy has been filed; partial payments have been made but they are not sufficient to redeem delinquency
   {
      pOutRec->DelqStatus[0] = TAX_STAT_BKRPCY;
   } else if (!memcmp(pInRec->Status, "TXSAL RED", 3))   // Property has been sold at tax-sale; proceeds from the sale have paid the delinquency
   {
      pOutRec->DelqStatus[0] = TAX_STAT_REDEEMED;
      pOutRec->isDelq[0] = 0;
   } else if (!memcmp(pInRec->Status, "CLX/SEPVAL", 3))  // Delinquency on original parcel-number has been cancelled; delinquency has been split among new parcel-numbers
   {
      pOutRec->DelqStatus[0] = TAX_STAT_CANCEL;
      pOutRec->isDelq[0] = 0;
   } else if (!memcmp(pInRec->Status, "UNDIVINT", 3))    // Delinquency on the original parcel-number has been cancelled; delinquency has been moved to Undivided Interest assessment-numbers (assessment-numbers beginning with "0081")
   {
      pOutRec->DelqStatus[0] = TAX_STAT_CANCEL;
      pOutRec->isDelq[0] = 0;
   } else if (!memcmp(pInRec->Status, "BY-VALUE", 4))    // Conditions exist that cause the delinquent-amount to be incorrect or unbillable or uncollectible
   {
      pOutRec->DelqStatus[0] = TAX_STAT_BYVALUE;
   } else if (!memcmp(pInRec->Status, "SLD TXSALE", 5))
   {
      pOutRec->DelqStatus[0] = TAX_STAT_SOLD2AUCTION;
   } else
   {  // *NONE*
      pOutRec->DelqStatus[0] = TAX_STAT_UNPAID;
      pOutRec->isDelq[0] = '1';
      if (memcmp(pInRec->Status, "*NONE*", 5))
         LogMsg("*** Unknown status: %.10s", pInRec->Status);
   }

   return 0;
}

int Riv_Load_TaxDelq(char *pDelqFile, bool bImport)
{
   char     *pTmp, acBuf[1024], acRec[MAX_RECSIZE], acOutBuf[2048];
   char     acTmpFile[_MAX_PATH];

   int      iRet, lCnt=0;
   FILE     *fdIn, *fdOut;
   TAXDELQ  *pTaxDelq = (TAXDELQ *)&acBuf[0];
   RIV_RDMT *pRiv_RdmT = (RIV_RDMT *)&acRec[0];
   RIV_RDMN *pRiv_RdmN = (RIV_RDMN *)&acRec[0];
   double   dTotalTax, dTaxs, dPens, dFees;


   LogMsg("Loading tax redemption file");

   // Open input file
   LogMsg("Open tax redemption file %s", pDelqFile);
   fdIn = fopen(pDelqFile, "rb");
   if (fdIn == NULL)
   {
      LogMsg("***** Error opening tax redemption file: %s\n", pDelqFile);
      return -2;
   }  

   // Create output file
   NameTaxCsvFile(acTmpFile, myCounty.acCntyCode, "Delq");
   LogMsg("Open Delq file %s", acTmpFile);
   fdOut = fopen(acTmpFile, "w");
   if (fdOut == NULL)
   {
      LogMsg("***** Error creating Delq file: %s\n", acTmpFile);
      return -4;
   }

   pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdIn);
   while (!feof(fdIn))
   {
      // Clear output buffer
      memset(acBuf, 0, sizeof(TAXDELQ));

#ifdef _DEBUG
      //if (!memcmp(pRiv_Rdm->Assmnt_No, "008524989", 9) )
      //   iRet = 0;
#endif

      if (pRiv_RdmT->Rec_Type[0] == 'N')
         iRet = Riv_ParseRdmN(acBuf, acRec);
      else
      {
         LogMsg("***** Input file must be corrupted: %.80s", acRec);
         fclose(fdOut);
         fclose(fdIn);
         return -1;
      }

      dTotalTax = 0;
      pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdIn);
      while (pTmp && pRiv_RdmT->Rec_Type[0] == 'T' && !memcmp(acBuf, pRiv_RdmT->Assmnt_No, TRT_SIZ_ASMT_NO))
      {
         if (pRiv_RdmT->Status_Inst1[0] == 'U')
         {
            dTaxs = atofn(pRiv_RdmT->Tax_Amt1, TRT_SIZ_TAX_AMT);
            dTaxs +=atofn(pRiv_RdmT->Tax_Amt2, TRT_SIZ_TAX_AMT);
            dPens = atofn(pRiv_RdmT->Pen_Amt1, TRT_SIZ_PEN_AMT);
            dPens +=atofn(pRiv_RdmT->Pen_Amt2, TRT_SIZ_PEN_AMT);
            dFees = atofn(pRiv_RdmT->Fee_Amt1, TRT_SIZ_FEE_AMT);
            dFees +=atofn(pRiv_RdmT->Fee_Amt2, TRT_SIZ_FEE_AMT);

         } else if (pRiv_RdmT->Status_Inst2[0] == 'U')
         {
            dTaxs = atofn(pRiv_RdmT->Tax_Amt2, TRT_SIZ_TAX_AMT);
            dPens = atofn(pRiv_RdmT->Pen_Amt2, TRT_SIZ_PEN_AMT);
            dFees = atofn(pRiv_RdmT->Fee_Amt2, TRT_SIZ_FEE_AMT);
         } else
            dTaxs=dPens=dFees=0.0;

         // Only output record that has not been paid
         dTotalTax = dTaxs+dPens+dFees;
         if (dTotalTax > 0.0)
         {
            memcpy(pTaxDelq->TaxYear, pRiv_RdmT->Tax_Year, 4);

            // Default Amt - When Fee & Pen are added to Delq table, we can break DefAmt to separate fields
            sprintf(pTaxDelq->Def_Amt, "%.2f", dTotalTax/100.0);

            // Pen & Fee - For future use
            if (dPens > 0.0)
               sprintf(pTaxDelq->Pen_Amt, "%.2f", dPens/100.0);
            if (dFees > 0.0)
               sprintf(pTaxDelq->Fee_Amt, "%.2f", dFees/100.0);

            // Output delq record for every year
            Tax_CreateDelqCsv(acOutBuf, (TAXDELQ *)acBuf);
            fputs(acOutBuf, fdOut);
         }

         pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdIn);
         lCnt++;
      } 

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   if (fdOut)
      fclose(fdOut);
   if (fdIn)
      fclose(fdIn);

   LogMsg("Total Delq records processed:    %u", lCnt);

   // Import into SQL
   if (bImport)
      iRet = doTaxImport(myCounty.acCntyCode, TAX_DELINQUENT);
   else
      iRet = 0;

   return iRet;
}

/********************************* Riv_MergeAttr ******************************
 *
 * Merge CHAR data
 *
 ******************************************************************************/

int Riv_MergeAttr(int iSkip)
{
   char     acBuf[MAX_RECSIZE], acRawFile[_MAX_PATH], acOutFile[_MAX_PATH];

   HANDLE   fhIn, fhOut;
   int      iRet, lCnt=0;
   DWORD    nBytesRead;
   DWORD    nBytesWritten;
   BOOL     bRet, bEof;

   LogMsg0("Merge attribute chars.");

   sprintf(acRawFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "S01");
   sprintf(acOutFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "R01");

   // Rename files for processing
   if (_access(acRawFile, 0))
   {
      if (!_access(acOutFile, 0))
         rename(acOutFile, acRawFile);
      else
      {
         LogMsg("***** Missing input file %s.  Please recheck!", acRawFile);
         return -1;
      }
   }

   // Open Char file
   LogMsg("Open Char file %s", acCChrFile);
   fdChar = fopen(acCChrFile, "r");
   if (fdChar == NULL)
   {
      LogMsg("***** Error opening Char file: %s\n", acCChrFile);
      return -2;
   }

   // Open Input file
   LogMsg("Open input file %s", acRawFile);
   fhIn = CreateFile(acRawFile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhIn == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening input file: %s\n", acRawFile);
      return -3;
   }

   // Open Output file
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening output file: %s\n", acOutFile);
      return -4;
   }

   // Copy skip record
   memset(acBuf, ' ', iRecLen);
   while (iSkip-- > 0)
   {
      ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);
      // Ignore record start with 99999999
      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
   }

   // Merge loop
   bEof = false;
   while (!bEof)
   {
      bRet = ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);

      // Check for EOF
      if (!bRet)
         break;

      // EOF ?
      if (iRecLen != nBytesRead)
         break;

      // Merge Char
      if (fdChar)
         iRet = Riv_MergeStdChar(acBuf);

      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdChar)
      fclose(fdChar);
   if (fhOut)
      CloseHandle(fhOut);
   if (fhIn)
      CloseHandle(fhIn);

   LogMsg("Total output records:       %u", lCnt);
   LogMsg("Total char rec matched:     %u", lCharMatch);
   LogMsg("Total char rec skipped:     %u\n", lCharSkip);

   // Need this to start check S01/R01
   lRecCnt = lCnt;
   return 0;
}

/*************************** Riv_ParseTaxRoll() *******************************
 *
 * Parse extended roll tax file to TaxBase, TaxItems, ans TaxAgency
 *
 ******************************************************************************/

int Riv_ParseTaxRoll(char *pInbuf, char *pTaxBase, FILE *fdDetail, FILE *fdAgency)
{
   int      iTmp, iIdx;
   double	dTax, dTax1, dTax2, dRate, dTotalTax;
   char     acTmp[1024], acDetail[1024], acAgency[1024], *pTmp;

   TAXBASE   *pOutBase   = (TAXBASE *)pTaxBase;
   TAXDETAIL *pOutDetail = (TAXDETAIL *)acDetail;
   TAXAGENCY *pOutAgency = (TAXAGENCY *)acAgency, *pResult;

   // Parse input tax data
   iTokens = ParseString(pInbuf, cDelim, MAX_FLD_TOKEN, apTokens);
   if (iTokens < RIV_ET_COLS)
   {
      LogMsg("***** Error: bad Tax record for APN=%s (#tokens=%d)", pInbuf, iTokens);
      return -1;
   }

   // Clear output buffer
   memset(pTaxBase, 0, sizeof(TAXBASE));
   memset(acDetail, 0, sizeof(TAXDETAIL));

#ifdef _DEBUG
   //if (!strcmp(apTokens[TC_APN_S], "008102853"))
   //   iTmp = 0;
#endif
   
   // APN
   strcpy(pOutBase->Apn, apTokens[RIV_ET_PIN]);
   strcpy(pOutDetail->Apn, apTokens[RIV_ET_PIN]);
   pOutBase->isSecd[0] = '1';
   pOutBase->isSupp[0] = '0';
   pOutBase->BillType[0] = BILLTYPE_SECURED;

   // BillNum
   //strcpy(pOutBase->BillNum, apTokens[RIV_ET_GEO]);
   //strcpy(pOutDetail->BillNum, apTokens[RIV_ET_GEO]);

   // Tax Year
   sprintf(pOutBase->TaxYear, "%d", lTaxYear);
   sprintf(pOutDetail->TaxYear, "%d", lTaxYear);

   // TaxAmt
   dTotalTax = atof(apTokens[RIV_ET_SUMMARYAMOUNT]);
   if (dTotalTax > 0)
   {
      pOutBase->dTotalTax = dTotalTax;
      strcpy(pOutBase->TotalTaxAmt, apTokens[RIV_ET_SUMMARYAMOUNT]);
      sprintf(pOutBase->TaxAmt1, "%.2f", dTotalTax/2.0);
      strcpy(pOutBase->TaxAmt2, pOutBase->TaxAmt1);
      sprintf(pOutBase->DueDate1, "%d1210", lTaxYear);
      sprintf(pOutBase->DueDate2, "%d0410", lTaxYear+1);
   } else
   {
      LogMsg("*** No tax amount %s", pOutBase->Apn);
      return 1;
   }

   // Merge Paid info
   iTmp = Riv_MergeTaxPaid(pTaxBase);
   if (!iTmp)
      strcpy(pOutDetail->BillNum, pOutBase->BillNum);

   // Total tax rate
   if (*apTokens[RIV_ET_TAXAGENCYRATE] > ' ')
   {
      dRate = atof(apTokens[RIV_ET_TAXAGENCYRATE]);
      if (dRate)
         sprintf(pOutBase->TotalRate, "%.6f", dRate);
   } else
      dRate = 0.0;

   // Get regular Tax amount
   iIdx = RIV_ET_AGENCY01;
   dTax = 0;
   while (iIdx <= RIV_ET_AGENCY10)
   {
      memset(acAgency, 0, sizeof(TAXAGENCY));

      // If no more entry, get out
      if (*apTokens[iIdx] < '0')
         break;

      dTax1 = atof(apTokens[iIdx+1]);
      sprintf(pOutDetail->TaxAmt, "%.2f", dTax1);
      dRate = atof(apTokens[iIdx+2]);
      sprintf(pOutDetail->TaxRate, "%.6f", dRate);

      // Tax Agency 
      if (pTmp = strchr(apTokens[iIdx], ' '))
         *pTmp = 0;
      strcpy(pOutDetail->TaxCode, apTokens[iIdx]);
      strcpy(pOutAgency->Code, apTokens[iIdx]);

      // Tax code
      if (iNumTaxDist > 1)
      {
         pResult = findTaxAgency(apTokens[iIdx], 0);
         if (pResult)
         {
            strcpy(pOutAgency->Agency, pResult->Agency);
            strcpy(pOutAgency->Phone, pResult->Phone);
            pOutAgency->TC_Flag[0] = pResult->TC_Flag[0];
            pOutDetail->TC_Flag[0] = pResult->TC_Flag[0];
         } else
         {
            if (*apTokens[iIdx] > ' ')
            {
               LogMsg("+++ New Agency: %s", apTokens[iIdx]);
               sprintf(pOutAgency->Agency, "Tax Agency - %s", pOutAgency->Code);
            } 

            pOutAgency->Phone[0] = 0;
            pOutAgency->TC_Flag[0] = '0';
            pOutDetail->TC_Flag[0] = '0';
         }
      }

      dTax += dTax1;
      Tax_CreateDetailCsv(acTmp, (TAXDETAIL *)&acDetail);
      fputs(acTmp, fdDetail);

      Tax_CreateAgencyCsv(acTmp, (TAXAGENCY *)&acAgency);
      fputs(acTmp, fdAgency);

      iIdx += 3;
   }

   // Get special Tax amount
   iIdx = RIV_ET_SPECIALTAXTYPE01;
   memset(pOutDetail->TaxRate, 0, 10);
   while (iIdx <= RIV_ET_SPECIALTAXTYPE15)
   {
      memset(acAgency, 0, sizeof(TAXAGENCY));

      // If no more entry, get out
      if (*apTokens[iIdx] < '0')
         break;

      dTax1 = atof(apTokens[iIdx+1]);
      dTax2 = atof(apTokens[iIdx+2]);
      sprintf(pOutDetail->TaxAmt, "%.2f", dTax1+dTax2);

      // Tax Agency 
      if (pTmp = strchr(apTokens[iIdx], ' '))
         *pTmp = 0;

      strcpy(pOutDetail->TaxCode, apTokens[iIdx]);
      strcpy(pOutAgency->Code, apTokens[iIdx]);

      // Tax code
      if (iNumTaxDist > 1)
      {
         pResult = findTaxAgency(apTokens[iIdx], 0);
         if (pResult)
         {
            strcpy(pOutAgency->Agency, pResult->Agency);
            strcpy(pOutAgency->Phone, pResult->Phone);
            pOutAgency->TC_Flag[0] = pResult->TC_Flag[0];
            pOutDetail->TC_Flag[0] = pResult->TC_Flag[0];
         } else
         {
            LogMsg("+++ New Agency: %s", apTokens[iIdx]);
            sprintf(pOutAgency->Agency, "Tax Agency - %s", pOutAgency->Code);
            pOutAgency->Phone[0] = 0;
            pOutAgency->TC_Flag[0] = '0';
            pOutDetail->TC_Flag[0] = '0';
         }
      }

      dTax += (dTax1+dTax2);
      Tax_CreateDetailCsv(acTmp, (TAXDETAIL *)&acDetail);
      fputs(acTmp, fdDetail);

      Tax_CreateAgencyCsv(acTmp, (TAXAGENCY *)&acAgency);
      fputs(acTmp, fdAgency);

      iIdx += 3;
      if (dTax != dTotalTax)
         iTmp = 0;
   }

   return 0;
}

/***************************** Riv_Load_TaxRoll() ******************************
 *
 * Load ExtendedRollTaxAmount.csv
 * Create TaxBase, TaxItems, and TaxAgency
 *
 ******************************************************************************/

int Riv_Load_TaxRoll(char *pInfile, bool bImport)
{
   char     sDetailFile[_MAX_PATH], sTmp[_MAX_PATH];
   char     sAgencyFile[_MAX_PATH], sBaseFile[_MAX_PATH], sTmpFile[_MAX_PATH];
   char     *pTmp, acBase[1024], acDetail[1024], acRec[MAX_RECSIZE];
   int      iRet, iParseType=0;
   long     lOut=0, lCnt=0, lDrop=0, lBad=0, lNoTax=0;
   FILE     *fdIn, *fdBase, *fdDetail, *fdAgency;
   TAXDETAIL *pDetail = (TAXDETAIL *)acDetail;
   TAXBASE   *pBase = (TAXBASE *)acBase;

   LogMsg0("Loading extended tax roll file: %s", pInfile);

   // Verify temp folder
   sprintf(sTmp, "%s\\%s", acTmpPath, myCounty.acCntyCode);
   if (_access(sTmp, 0))
      _mkdir(sTmp);

   // Verify import folder
   NameTaxCsvFile(sTmp, myCounty.acCntyCode, "Paid");

   // Open input file
   LogMsg("Open Tax file %s", pInfile);
   fdIn = fopen(pInfile, "r");
   if (fdIn == NULL)
   {
      LogMsg("***** Error opening Tax file: %s\n", pInfile);
      return -2;
   }  

   // Create output file names
   NameTaxCsvFile(sBaseFile, myCounty.acCntyCode, "Base");
   NameTaxCsvFile(sDetailFile, myCounty.acCntyCode, "Items");
   NameTaxCsvFile(sAgencyFile, myCounty.acCntyCode, "Agency");
   sprintf(sTmpFile, "%s\\%s\\%s_Agency.tmp", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);

   LogMsg("Create TaxBase file %s", sBaseFile);
   fdBase = fopen(sBaseFile, "w");
   if (fdBase == NULL)
   {
      LogMsg("***** Error creating TaxBase file: %s\n", sBaseFile);
      return -4;
   }

   LogMsg("Create TaxItems file %s", sDetailFile);
   fdDetail = fopen(sDetailFile, "w");
   if (fdDetail == NULL)
   {
      LogMsg("***** Error creating TaxItems file: %s\n", sDetailFile);
      return -4;
   }

   LogMsg("Create TaxAgency file %s", sTmpFile);
   fdAgency = fopen(sTmpFile, "w");
   if (fdAgency == NULL)
   {
      LogMsg("***** Error creating TaxAgency file: %s\n", sTmpFile);
      return -4;
   }

   // Skip header record
   pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdIn);
   iNewAgency = 0;
   cDelim = ',';

   // Merge loop 
   while (!feof(fdIn))
   {
      pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdIn);
      if (!pTmp || *pTmp > '9')
         break;

      if (*pTmp < '0')
         continue;

      iRet = Riv_ParseTaxRoll(acRec, acBase, fdDetail, fdAgency);
      if (!iRet)
      {
         Tax_CreateTaxBaseCsv(acRec, (TAXBASE *)&acBase);
         fputs(acRec, fdBase);

         lOut++;
      } else if (iRet == 1)
         lNoTax++;
      else
         lBad++;
         //LogMsg("---> Drop detail record %d [%s] (%d)", lCnt, acRec, iRet); 

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   fclose(fdBase);
   fclose(fdDetail);
   fclose(fdAgency);

   // Import into SQL
   if (bImport)
   {
      iRet = doTaxImport(myCounty.acCntyCode, TAX_BASE);
      if (!iRet)
      {
         iRet = doTaxImport(myCounty.acCntyCode, TAX_DETAIL);
         if (!iRet)
         {
            // Populate TotalRate 
            //iRet = doUpdateTotalRate(myCounty.acCntyCode);

            // Dedup Agency file before import
            LogMsg("Dedup Agency file %s", sTmpFile);
            iRet = sortFile(sTmpFile, sAgencyFile, "S(#1,C,A) DUPOUT F(TXT)");
            if (iRet > 0)
            {
               iRet = doTaxImport(myCounty.acCntyCode, TAX_AGENCY);
            }
         }
      }
   } else
      iRet = 0;

   if (iNewAgency > 0)
      iRet = 99;        // signal send mail
   return iRet;
}

/*************************** Riv_Load_ExtTaxRoll() ****************************
 *
 * Load ExtendedRollTax.csv
 * Create TaxBase, TaxItems, and TaxAgency
 *
 ******************************************************************************/

int Riv_Load_ExtTaxRoll(char *pInfile, bool bImport)
{
   char     sDetailFile[_MAX_PATH], sTmp[_MAX_PATH];
   char     sAgencyFile[_MAX_PATH], sBaseFile[_MAX_PATH], sTmpFile[_MAX_PATH];
   char     *pTmp, acBase[1024], acDetail[1024], acRec[MAX_RECSIZE];
   int      iRet, iParseType=0;
   long     lOut=0, lCnt=0, lDrop=0, lBad=0, lNoTax=0;
   FILE     *fdIn, *fdBase, *fdDetail, *fdAgency;
   TAXDETAIL *pDetail = (TAXDETAIL *)acDetail;
   TAXBASE   *pBase = (TAXBASE *)acBase;

   LogMsg0("Loading extended tax roll file: %s", pInfile);

   // Verify temp folder
   sprintf(sTmp, "%s\\%s", acTmpPath, myCounty.acCntyCode);
   if (_access(sTmp, 0))
      _mkdir(sTmp);

   // Sort input file
   sprintf(sTmpFile, "%s\\Riv\\Riv_TaxRoll.txt", acTmpPath);
   iRet = sortFile(pInfile, sTmpFile, "S(#1,C,A) DEL(44) ");
   if (!iRet)
      return -1;

   // Open input file
   LogMsg("Open Tax file %s", sTmpFile);
   fdIn = fopen(sTmpFile, "r");
   if (fdIn == NULL)
   {
      LogMsg("***** Error opening Tax file: %s\n", sTmpFile);
      return -2;
   }  

   // Open paid file
   NameTaxCsvFile(sTmpFile, myCounty.acCntyCode, "Paid");
   LogMsg("Open Paid file %s", sTmpFile);
   fdPaid = fopen(sTmpFile, "r");
   if (fdPaid == NULL)
   {
      LogMsg("***** Error opening Tax paid file: %s\n", sTmpFile);
      return -2;
   }  

   // Create output file names
   NameTaxCsvFile(sBaseFile, myCounty.acCntyCode, "Base");
   NameTaxCsvFile(sDetailFile, myCounty.acCntyCode, "Items");
   NameTaxCsvFile(sAgencyFile, myCounty.acCntyCode, "Agency");
   sprintf(sTmpFile, "%s\\%s\\%s_Agency.tmp", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);

   LogMsg("Create TaxBase file %s", sBaseFile);
   fdBase = fopen(sBaseFile, "w");
   if (fdBase == NULL)
   {
      LogMsg("***** Error creating TaxBase file: %s\n", sBaseFile);
      return -4;
   }

   LogMsg("Create TaxItems file %s", sDetailFile);
   fdDetail = fopen(sDetailFile, "w");
   if (fdDetail == NULL)
   {
      LogMsg("***** Error creating TaxItems file: %s\n", sDetailFile);
      return -4;
   }

   LogMsg("Create TaxAgency file %s", sTmpFile);
   fdAgency = fopen(sTmpFile, "w");
   if (fdAgency == NULL)
   {
      LogMsg("***** Error creating TaxAgency file: %s\n", sTmpFile);
      return -4;
   }

   // Init variables
   iNewAgency = 0;
   cDelim = ',';

   // Merge loop 
   while (!feof(fdIn))
   {
      pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdIn);
      if (!pTmp || *pTmp > '9')
         break;

      if (*pTmp < '0')
         continue;

      iRet = Riv_ParseTaxRoll(acRec, acBase, fdDetail, fdAgency);
      if (!iRet)
      {
         Tax_CreateTaxBaseCsv(acRec, (TAXBASE *)&acBase);
         fputs(acRec, fdBase);

         lOut++;
      } else if (iRet == 1)
         lNoTax++;
      else
         lBad++;
         //LogMsg("---> Drop detail record %d [%s] (%d)", lCnt, acRec, iRet); 

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   fclose(fdBase);
   fclose(fdDetail);
   fclose(fdAgency);
   fclose(fdIn);
   if (fdPaid)
      fclose(fdPaid);

   // Import into SQL
   if (bImport)
   {
      iRet = doTaxImport(myCounty.acCntyCode, TAX_BASE);
      if (!iRet)
      {
         iRet = doTaxImport(myCounty.acCntyCode, TAX_DETAIL);
         if (!iRet)
         {
            // Populate TotalRate 
            //iRet = doUpdateTotalRate(myCounty.acCntyCode);

            // Dedup Agency file before import
            LogMsg("Dedup Agency file %s", sTmpFile);
            iRet = sortFile(sTmpFile, sAgencyFile, "S(#1,C,A) DUPOUT F(TXT)");
            if (iRet > 0)
            {
               iRet = doTaxImport(myCounty.acCntyCode, TAX_AGENCY);
            }
         }
      }
   } else
      iRet = 0;

   if (iNewAgency > 0)
      iRet = 99;        // signal send mail
   return iRet;
}

/*************************** Riv_Load_TaxPaidCsv ******************************
 *
 * Load SecuredPaidUnpaid.csv
 *
 ******************************************************************************/

int Riv_Load_TaxPaidCsv(char *pPaidFile, bool bImport)
{
   char     *pTmp, acBuf[1024], acRec[MAX_RECSIZE];
   char     acTmp[1024];

   int      iRet, lCnt=0, lOut=0;
   FILE     *fdIn, *fdOut;
   TAXPAID  *pTaxPaid = (TAXPAID *)&acBuf[0];
   double   dTaxAmt, dPen, dFee, dTmp;


   LogMsg0("Loading tax paid file");

   // Sort paid file on PIN & PAIDINDICATOR
   sprintf(acTmp, "%s\\Riv\\Riv_Paid.txt", acTmpPath);
   iRet = sortFile(pPaidFile, acTmp, "S(#1,C,A,#4,C,A) DEL(44) DUPOUT");
   if (!iRet)
      return -1;

   // Open input paid file
   LogMsg("Open tax file %s", acTmp);
   fdIn = fopen(acTmp, "r");
   if (fdIn == NULL)
   {
      LogMsg("***** Error opening tax paid file: %s\n", acTmp);
      return -2;
   }  

   // Create paid output file
   NameTaxCsvFile(acTmp, myCounty.acCntyCode, "Paid");
   LogMsg("Open Paid file %s", acTmp);
   fdOut = fopen(acTmp, "w");
   if (fdOut == NULL)
   {
      LogMsg("***** Error creating output Paid file: %s\n", acTmp);
      return -4;
   }

   iRet = getFileDate(pPaidFile);
   if (iRet > lLastTaxFileDate)
      lLastTaxFileDate = iRet;

   // Clear output buffer
   memset(acBuf, 0, sizeof(TAXPAID));
   dTaxAmt = dPen = dFee = 0;
   while (!feof(fdIn))
   {
      pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdIn);
      if (!pTmp || *pTmp > '9')
         break;

      // Parse input tax data
      iTokens = ParseString(acRec, cDelim, MAX_FLD_TOKEN, apTokens);
      if (iTokens < RIV_P_COLS)
      {
         LogMsg("***** Error: bad Tax paid record for APN=%s (#tokens=%d)", acRec, iTokens);
         return -1;
      }

#ifdef _DEBUG
      //if (!memcmp(acRec, "009000198", 10) )
      //   iRet = 0;
#endif
      if (acBuf[0] >= '0') 
      {
         if (strcmp(pTaxPaid->Apn, acRec) || pTaxPaid->PaidFlg[0] != *apTokens[RIV_P_PAIDINDICATOR])
         {
            //dTaxAmt /= 2.0;
            if (dTaxAmt > 0.0)
            {
               sprintf(pTaxPaid->TaxAmt, "%.2f", dTaxAmt);
            }

            //if (pTaxPaid->PaidFlg[0] != 'B' && pTaxPaid->PaidFlg[0] != 'P')
            //   dPen /= 2.0;
            if (dPen > 0.0)
            {
               sprintf(pTaxPaid->PenAmt, "%.2f", dPen);
               dTaxAmt += dPen;
               dPen = 0;
            }

            if (dFee > 0.0)
            {
               sprintf(pTaxPaid->FeeAmt, "%.2f", dFee);
               dTaxAmt += dFee;
               dFee = 0;
            }

            if (dTaxAmt > 0.0 && (pTaxPaid->PaidFlg[0] == 'P' || pTaxPaid->PaidFlg[0] == '1'))
            {
               sprintf(pTaxPaid->PaidAmt, "%.2f", dTaxAmt);
            }

            sprintf(acTmp, "%s|%s|%s|%s|%s|%s|%s|%s|%s\n",
               pTaxPaid->Apn,pTaxPaid->TRA,pTaxPaid->PaidFlg,pTaxPaid->TaxAmt,pTaxPaid->PenAmt,pTaxPaid->FeeAmt,
               pTaxPaid->BillNum,pTaxPaid->PaidDate,pTaxPaid->PaidAmt);
            fputs(acTmp, fdOut);
            lOut++;
            memset(acBuf, 0, sizeof(TAXPAID));
            dTaxAmt = 0;
         }
      } 

      if (acBuf[0] == 0)
      {
         // APN
         strcpy(pTaxPaid->Apn, apTokens[RIV_P_PIN]);

         // Bill No
         strcpy(pTaxPaid->BillNum, apTokens[RIV_P_BILLNUMBER]);

         // TRA
         strcpy(pTaxPaid->TRA, apTokens[RIV_P_TRA]);

         // Paid indicator
         pTaxPaid->PaidFlg[0] = *apTokens[RIV_P_PAIDINDICATOR];
      }

      // Tax amt
      dTmp = atof(apTokens[RIV_P_INSTALLMENT]);
      dTaxAmt += dTmp;

      // Pen amt
      dTmp = atof(apTokens[RIV_P_PENALITY]);
      dPen += dTmp;

      // Fee amt
      dTmp = atof(apTokens[RIV_P_COSTAMOUNT]);
      dFee += dTmp;

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Last record
   if (acBuf[0] >= '0') 
   {
      if (dTaxAmt > 0.0)
         sprintf(pTaxPaid->TaxAmt, "%.2f", dTaxAmt);

      if (dPen > 0.0)
      {
         sprintf(pTaxPaid->PenAmt, "%.2f", dPen);
         dPen = 0;
      }

      if (dFee > 0.0)
      {
         sprintf(pTaxPaid->FeeAmt, "%.2f", dFee);
         dFee = 0;
      }

      if (dTaxAmt > 0.0)
      {
         //dTaxAmt /= 2;
         //dPen /= 2;
         switch (pTaxPaid->PaidFlg[0])
         {
            case '1':      // Inst1 paid
               sprintf(pTaxPaid->PaidAmt, "%.2f", dTaxAmt/2 + dPen + dFee);
               break;
            case '2':      // Inst2 unpaid
               strcpy(pTaxPaid->PaidAmt, "0");
               break;
            case 'P':      // Both paid
               sprintf(pTaxPaid->PaidAmt, "%.2f", dTaxAmt + dPen + dFee);
               break;
            case 'B':      // Both unpaid
               strcpy(pTaxPaid->PaidAmt, "0");
               break;
         }
         dTaxAmt = 0;
      }

      sprintf(acTmp, "%s|%s|%s|%s|%s|%s|%s|%s|%s\n",
         pTaxPaid->Apn,pTaxPaid->TRA,pTaxPaid->PaidFlg,pTaxPaid->TaxAmt,pTaxPaid->PenAmt,pTaxPaid->FeeAmt,
         pTaxPaid->BillNum,pTaxPaid->PaidDate,pTaxPaid->PaidAmt);
      fputs(acTmp, fdOut);
      lOut++;
   }

   if (fdOut)
      fclose(fdOut);
   if (fdIn)
      fclose(fdIn);

   LogMsg("Total paid records processed:    %u", lCnt);

   // Import into SQL
   if (bImport)
      iRet = doTaxImport(myCounty.acCntyCode, TAX_PAID);
   else
      iRet = 0;

   return iRet;
}

/****************************** Riv_Load_TaxDelqCsv *************************
 *
 * Load SecuredPriorYearUnpaid.csv
 *
 ****************************************************************************/

int Riv_Load_TaxDelqCsv(char *pDelqFile, bool bImport)
{
   char     *pTmp, acBuf[1024], acRec[MAX_RECSIZE], acOutBuf[2048];
   char     acTmpFile[_MAX_PATH];

   int      iRet, lCnt=0;
   FILE     *fdIn, *fdOut;
   TAXDELQ  *pTaxDelq = (TAXDELQ *)&acBuf[0];
   double   dTotalTax, dTaxs, dPens, dFees, dInts, dCreds;

   LogMsg0("Loading tax redemption file");

   sprintf(acTmpFile, "%s\\Riv\\Riv_Delq.txt", acTmpPath);
   iRet = sortFile(pDelqFile, acTmpFile, "S(#1,C,A,#27,C,A) DEL(44)");
   if (!iRet)
      return -1;

   // Open input file
   LogMsg("Open tax redemption file %s", acTmpFile);
   fdIn = fopen(acTmpFile, "r");
   if (fdIn == NULL)
   {
      LogMsg("***** Error opening tax redemption file: %s\n", acTmpFile);
      return -2;
   }  

   // Create output file
   NameTaxCsvFile(acTmpFile, myCounty.acCntyCode, "Delq");
   LogMsg("Open Delq file %s", acTmpFile);
   fdOut = fopen(acTmpFile, "w");
   if (fdOut == NULL)
   {
      LogMsg("***** Error creating Delq file: %s\n", acTmpFile);
      return -4;
   }

   // Clear output buffer
   memset(acBuf, 0, sizeof(TAXDELQ));
   while (!feof(fdIn))
   {
      pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdIn);
      if (!pTmp || *pTmp > '9')
         break;

      // Bypass BPP records
      if (acRec[9] == 'B' || acRec[0] < '0')
         continue;

      // Parse input record
      iTokens = ParseString(acRec, cDelim, MAX_FLD_TOKEN, apTokens);
      if (iTokens < RIV_D_COLS)
      {
         LogMsg("***** Error: bad delq record for APN=%s (#tokens=%d)", acRec, iTokens);
         break;
      }

#ifdef _DEBUG
      //if (!memcmp(apTokens[RIV_D_PIN], "010008497", 9) )
      //   iRet = 0;
#endif

      // Skip redeemed & By-value records
      if (*apTokens[RIV_D_BILLSTATUS] == 'R' || *apTokens[RIV_D_BILLSTATUS] == 'B')
         continue;

      // APN
      strcpy(pTaxDelq->Apn, apTokens[RIV_D_PIN]);

      // Default Number
      strncpy(pTaxDelq->Default_No, apTokens[RIV_D_BILLNUMBER], 13);

      // Updated date
      sprintf(pTaxDelq->Upd_Date, "%d", lLastTaxFileDate);

      if (*apTokens[RIV_D_INST1STATUS] == 'U')
      {
         dTaxs = atof(apTokens[RIV_D_INST1TAXES]);
         dTaxs +=atof(apTokens[RIV_D_INST2TAXES]);
         dPens = atof(apTokens[RIV_D_INST1PENALTY]);
         dPens +=atof(apTokens[RIV_D_INST2PENALTY]);
         dFees = atof(apTokens[RIV_D_INST1COST])+atof(apTokens[RIV_D_INST1FEE]);
         dFees +=atof(apTokens[RIV_D_INST2COST])+atof(apTokens[RIV_D_INST2FEE]);
         dInts = atof(apTokens[RIV_D_INST1INTEREST])+atof(apTokens[RIV_D_INST2INTEREST]);
      } else if (*apTokens[RIV_D_INST2STATUS] == 'U')
      {
         dTaxs = atof(apTokens[RIV_D_INST2TAXES]);
         dPens = atof(apTokens[RIV_D_INST2PENALTY]);
         dFees = atof(apTokens[RIV_D_INST2COST])+atof(apTokens[RIV_D_INST2FEE]);
         dInts = atof(apTokens[RIV_D_INST1INTEREST])+atof(apTokens[RIV_D_INST2INTEREST]);
      } else
         dTaxs=dPens=dFees=dInts=0.0;

      // IPPLapsed credit
      dCreds = 0; //atof(apTokens[RIV_D_LAPSEDIPPCREDIT]);

      // Only output record that has not been paid
      dTotalTax = dTaxs+dPens+dFees+dInts+dCreds;
      if (dTotalTax > 0.0)
      {
         strcpy(pTaxDelq->TaxYear, apTokens[RIV_D_TAXYEAR]);

         // Default Amt - When Fee & Pen are added to Delq table, we can break DefAmt to separate fields
         sprintf(pTaxDelq->Def_Amt, "%.2f", dTotalTax);

         // Pen & Fee
         if (dPens > 0.0)
         {
            sprintf(pTaxDelq->Pen_Amt, "%.2f", dPens);
            sprintf(pTaxDelq->Fee_Amt, "%.2f", dFees+dInts);
         }

         pTaxDelq->isDelq[0] = '1';
         pTaxDelq->isSecd[0] = '1';
         //pTaxDelq->DelqStatus = '';

         // Output delq record for every year
         Tax_CreateDelqCsv(acOutBuf, (TAXDELQ *)acBuf);
         fputs(acOutBuf, fdOut);
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   if (fdOut)
      fclose(fdOut);
   if (fdIn)
      fclose(fdIn);

   LogMsg("Total Delq records processed:    %u", lCnt);

   // Import into SQL
   if (bImport)
      iRet = doTaxImport(myCounty.acCntyCode, TAX_DELINQUENT);
   else
      iRet = 0;

   return iRet;
}

/*************************** Riv_CreateTaxItems() *****************************
 *
 * Parse current detail tax bill to update TaxBase and create TaxItems and TaxAgency
 *
 ******************************************************************************/

int Riv_CreateTaxItems(char *pTaxBase, FILE *fdDetail, FILE *fdItems, FILE *fdAgency)
{
   static   char acRec[1024], *apItems[16];
   static   long lPos=0;

   int      iRet, iTmp, iItems, iAdjIdx;
   double   dTaxRate;
   char     acTmp[1024], acDetail[1024], acAgency[1024], *pTmp;

   TAXBASE   *pOutBase   = (TAXBASE *)pTaxBase;
   TAXDETAIL *pOutDetail = (TAXDETAIL *)acDetail;
   TAXAGENCY *pOutAgency = (TAXAGENCY *)acAgency, *pResult;

#ifdef _DEBUG
   //if (!memcmp(pOutBase->Assmnt_No, "44257021", 8))
   //   iTmp = 0;
#endif
   
   dTaxRate = 0;
   iRet = 1;
   while (!feof(fdDetail))
   {
      // Save current position
      lPos = ftell(fdDetail);
      pTmp = fgets(acRec, 1024, fdDetail);
      if (!pTmp || *pTmp > '9')
         break;
     
#ifdef _DEBUG
      //if (!memcmp(acRec, "355370065", 9) )
      //   iRet = 0;
#endif
      // Parse detail rec
      iAdjIdx = 0;
      iItems = ParseStringNQ(acRec, cDelim, MAX_FLD_TOKEN, apItems);
      if (iItems < RIV_CD_FLDS)
      {
         LogMsg("***** Error: bad Tax record for APN=%s (#tokens=%d)", acRec, iItems);
         return -1;
      } else if (iItems > RIV_CD_FLDS)
      {
         iAdjIdx++;
         LogMsg("*** WARNING: Too many tokens APN=%s (#tokens=%d).", acRec, iItems);
      }

      iTmp = strcmp(pOutBase->Assmnt_No, apItems[RIV_CD_TAXBILLID]);
      if (!iTmp)
      {
         iRet = 0;
         memset(acAgency, 0, sizeof(TAXAGENCY));
         memset(acDetail, 0, sizeof(TAXDETAIL));
         strcpy(pOutDetail->Apn, apItems[RIV_CD_PIN]);
         strcpy(pOutDetail->Assmnt_No, apItems[RIV_CD_TAXBILLID]);
         strcpy(pOutDetail->TaxYear, apItems[RIV_CD_TAXYEAR]);
         strcpy(pOutDetail->BillNum, apItems[RIV_CD_BILLNUMBER]);
         strcpy(pOutDetail->TaxRate, apItems[RIV_CD_RATE+iAdjIdx]);
         strcpy(pOutDetail->TaxAmt, apItems[RIV_CD_TOTAL+iAdjIdx]);
         strcpy(pOutDetail->TaxDesc, apItems[RIV_CD_DETAILS]);
         strcpy(pOutDetail->TaxCode, apItems[RIV_CD_TAXCODE+iAdjIdx]);
         remChar(pOutDetail->TaxCode, '-');
         dTaxRate += atof(pOutDetail->TaxRate);

         // Update TaxBase
         //if (!pOutBase->Apn[0])
         if (!pOutBase->TaxYear[0])
         {
            // pOutBase contains new APN.  We need old APN for current tax roll.
            if (strcmp(pOutBase->Apn, pOutDetail->Apn))
            {
               LogMsg("*** APN mismatched BillID=%s Base->PIN=%s ... Detail->PIN=%s", pOutDetail->BillNum, pOutBase->Apn, pOutDetail->Apn);
               strcpy(pOutBase->Apn, pOutDetail->Apn);
               strcpy(pOutBase->BillNum, pOutDetail->BillNum);
            }
            strcpy(pOutBase->TaxYear, pOutDetail->TaxYear);

            remChar(apItems[RIV_CD_TAXRATEAREA], '-');
            iTmp = atol(apItems[RIV_CD_TAXRATEAREA]);
            sprintf(pOutBase->TRA, "%.6d", iTmp);

            // BillType (Secured Annual|Secured Escape|Secured Penalty|Secured Supplemental)
            //  (Unsecured Annual|Unsecured Escape|Unsecured Penalty|Unsecured Supplemental) 
            if (*apItems[RIV_CD_BILLTYPE] == 'S')
            {
               switch (*(apItems[RIV_CD_BILLTYPE]+8))
               {
                  case 'A':
                     pOutBase->isSecd[0] = '1';
                     pOutBase->isSupp[0] = '0';
                     pOutBase->BillType[0] = BILLTYPE_SECURED;
                     break;
                  case 'E':
                     pOutBase->isSecd[0] = '1';
                     pOutBase->isSupp[0] = '0';
                     pOutBase->BillType[0] = BILLTYPE_SECURED_ESCAPE;
                     break;
                  case 'P':
                     pOutBase->isSecd[0] = '1';
                     pOutBase->isSupp[0] = '0';
                     pOutBase->BillType[0] = BILLTYPE_SECURED;
                     break;
                  case 'S':
                     pOutBase->isSecd[0] = '0';
                     pOutBase->isSupp[0] = '1';
                     pOutBase->BillType[0] = BILLTYPE_SECURED_SUPPL;
                     break;
               } 
            } else if (*apItems[RIV_CD_BILLTYPE] == 'U')
            {
               switch (*(apItems[RIV_CD_BILLTYPE]+10))
               {
                  case 'A':
                     pOutBase->isSecd[0] = '0';
                     pOutBase->isSupp[0] = '0';
                     pOutBase->BillType[0] = BILLTYPE_UNSECURED;
                     break;
                  case 'E':
                     pOutBase->isSecd[0] = '0';
                     pOutBase->isSupp[0] = '0';
                     pOutBase->BillType[0] = BILLTYPE_UNSECURED_ESCAPE;
                     break;
                  case 'P':
                     pOutBase->isSecd[0] = '0';
                     pOutBase->isSupp[0] = '0';
                     pOutBase->BillType[0] = BILLTYPE_UNSECURED;
                     break;
                  case 'S':
                     pOutBase->isSecd[0] = '0';
                     pOutBase->isSupp[0] = '1';
                     pOutBase->BillType[0] = BILLTYPE_UNSECURED_SUPPL;
                     break;
               } 
            }            
         }

         // Tax code
         if (iNumTaxDist > 1)
         {
            pResult = findTaxAgency(pOutDetail->TaxCode, 0);
            if (pResult)
            {
               strcpy(pOutAgency->Agency, pResult->Agency);
               strcpy(pOutAgency->Code, pResult->Code);
               strcpy(pOutAgency->Phone, pResult->Phone);
               pOutAgency->TC_Flag[0] = pResult->TC_Flag[0];
               pOutDetail->TC_Flag[0] = pResult->TC_Flag[0];
            } else
            {
               strcpy(pOutAgency->Agency, pOutDetail->TaxDesc);
               strcpy(pOutAgency->Code, pOutDetail->TaxCode);
               pOutAgency->Phone[0] = 0;
               pOutAgency->TC_Flag[0] = '0';
               pOutDetail->TC_Flag[0] = '0';
               LogMsg("+++ New Agency: %s:%s", pOutDetail->TaxCode, pOutDetail->TaxDesc);
            }
         }

         // Output detail record
         Tax_CreateDetailCsv(acTmp, (TAXDETAIL *)&acDetail);
         fputs(acTmp, fdItems);

         Tax_CreateAgencyCsv(acTmp, (TAXAGENCY *)&acAgency);
         fputs(acTmp, fdAgency);
      } else if (iTmp > 0)
      {
         continue;
      } else
      {
         fseek(fdDetail, lPos, SEEK_SET);
         break;
      }
   }

   // Update TaxBase
   sprintf(pOutBase->TotalRate, "%.2f", dTaxRate);

   return iRet;
}

/*************************** Riv_Load_TaxCurrent ******************************
 *
 * Load TTC_CurrentAmounts.csv & TTC_CurrentDistricts.csv
 * Load current tax files and create Base, Items, & Agency import.
 *
 ******************************************************************************/

int Riv_Load_TaxCurrent(char *pCurrentAmountFile, char *pDetailFile, bool bImport)
{
   char     *pTmp, acOutbuf[1024], acRec[1024], acCurrent[_MAX_PATH], acDetail[_MAX_PATH], acTmp[256];
   char     sBase[1024], sAgencyFile[_MAX_PATH], sBaseFile[_MAX_PATH], sItemsFile[_MAX_PATH], sTmpFile[_MAX_PATH];
   long     iRet, lOut=0, lCnt=0, lDrop=0, lBad=0, lNoTax=0;
   double	dTax1, dTax2, dPen1, dPen2, dFee1, dFee2, dInt1, dInt2, dTaxDue1, dTaxDue2,
   	      dPaid1, dPaid2, dPaidPen1, dPaidPen2, dPaidFee1, dPaidFee2, dPaidInt1, dPaidInt2, 
            dTotalTax, dTotalDue, dTotalFees;

   TAXBASE  *pOutBase = (TAXBASE *)sBase;
   FILE     *fdCurrent, *fdDetail, *fdBase, *fdItems, *fdAgency;

   LogMsg0("Loading current tax file");

   // Sort current tax file on TaxBillId
   sprintf(acCurrent, "%s\\Riv\\Riv_Current.txt", acTmpPath);
   iRet = sortFile(pCurrentAmountFile, acCurrent, "S(#3,C,A) DEL(44) OMIT(#4,C,GT,\"9999\")");
   if (!iRet)
      return -1;

   // Sort detail file on TaxBillId & PIN
   sprintf(acDetail, "%s\\Riv\\Riv_Detail.txt", acTmpPath);
   iRet = sortFile(pDetailFile, acDetail, "S(#6,C,A,#1,C,A) DEL(44) ");
   if (!iRet)
      return -1;

   // Open current tax file
   LogMsg("Open current tax file %s", acCurrent);
   fdCurrent = fopen(acCurrent, "r");
   if (fdCurrent == NULL)
   {
      LogMsg("***** Error opening current tax file: %s\n", acCurrent);
      return -2;
   }  

   // Open detail file
   LogMsg("Open tax detail file %s", acDetail);
   fdDetail = fopen(acDetail, "r");
   if (fdDetail == NULL)
   {
      LogMsg("***** Error opening tax Detail file: %s\n", acDetail);
      return -2;
   }  

   // Create output file names
   NameTaxCsvFile(sBaseFile, myCounty.acCntyCode, "Base");
   NameTaxCsvFile(sItemsFile, myCounty.acCntyCode, "Items");
   NameTaxCsvFile(sAgencyFile, myCounty.acCntyCode, "Agency");
   sprintf(sTmpFile, "%s\\%s\\%s_Agency.tmp", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);

   LogMsg("Create TaxBase file %s", sBaseFile);
   fdBase = fopen(sBaseFile, "w");
   if (fdBase == NULL)
   {
      LogMsg("***** Error creating TaxBase file: %s\n", sBaseFile);
      return -4;
   }

   LogMsg("Create TaxItems file %s", sItemsFile);
   fdItems = fopen(sItemsFile, "w");
   if (fdItems == NULL)
   {
      LogMsg("***** Error creating TaxItems file: %s\n", sItemsFile);
      return -4;
   }

   LogMsg("Create TaxAgency file %s", sTmpFile);
   fdAgency = fopen(sTmpFile, "w");
   if (fdAgency == NULL)
   {
      LogMsg("***** Error creating TaxAgency file: %s\n", sTmpFile);
      return -4;
   }

   iRet = getFileDate(pCurrentAmountFile);
   if (iRet > lLastTaxFileDate)
      lLastTaxFileDate = iRet;

   // Init variables
   iNewAgency = 0;
   while (!feof(fdCurrent))
   {
      pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdCurrent);
      if (!pTmp || *pTmp > '9')
         break;

#ifdef _DEBUG
      //if (!memcmp(acRec, "44207859", 8) )
      //   iRet = 0;
      //if (lOut == 138169)
      //   iRet = 0;
#endif

      // Parse input tax data
      iTokens = ParseStringNQ(acRec, cDelim, MAX_FLD_TOKEN, apTokens);
      if (iTokens < RIV_CA_TAXBILLID)
      {
         LogMsg("***** Error: bad Tax bill record for taxbillid=%s (#tokens=%d)", acRec, iTokens);
         return -1;
      }

      // Reset buffer
      memset(sBase, 0, sizeof(TAXBASE));

      // Populate TaxBase
      strcpy(pOutBase->Assmnt_No, apTokens[RIV_CA_TAXBILLID]);
      strcpy(pOutBase->Apn, apTokens[RIV_CA_PIN]);
      strcpy(pOutBase->BillNum, apTokens[RIV_CA_BILLNUMBER]);

      // Due Date - there is no paid date
      pTmp = dateConversion(apTokens[RIV_CA_INST1DUEDATE], acTmp, YYYY_MM_DD);
      if (pTmp)
         strcpy(pOutBase->DueDate1, acTmp);
      pTmp = dateConversion(apTokens[RIV_CA_INST2DUEDATE], acTmp, YYYY_MM_DD, lTaxYear+1);
      if (pTmp)
         strcpy(pOutBase->DueDate2, acTmp);

      // 1st inst charge/paid/due
      strcpy(pOutBase->TaxAmt1, apTokens[RIV_CA_INST1TAXCHARGE]);
      if (*apTokens[RIV_CA_INST1TAXPAYMENT] == '-')
         strcpy(pOutBase->PaidAmt1, apTokens[RIV_CA_INST1TAXPAYMENT]+1);
      else
         strcpy(pOutBase->PaidAmt1, apTokens[RIV_CA_INST1TAXPAYMENT]);
      dTax1 = atof(apTokens[RIV_CA_INST1TAXCHARGE]);
      dPaid1 = atof(pOutBase->PaidAmt1);
      dTaxDue1 = atof(apTokens[RIV_CA_INST1AMOUNTDUE]);

      // 1st penalty/fee/int
      dPen1 = atof(apTokens[RIV_CA_INST1PENALTYCHARGE]);
      strcpy(pOutBase->PenAmt1, apTokens[RIV_CA_INST1PENALTYCHARGE]);
      dFee1 = atof(apTokens[RIV_CA_INST1FEECHARGE]);
      dInt1 = atof(apTokens[RIV_CA_INST1INTERESTCHARGE]);

      dPaidPen1=dPaidFee1=dPaidInt1 = 0;
      if (*apTokens[RIV_CA_INST1PENALTYPAYMENT] == '-')
         dPaidPen1 = atof(apTokens[RIV_CA_INST1PENALTYPAYMENT]+1);
      if (*apTokens[RIV_CA_INST1FEEPAYMENT] == '-')
         dPaidFee1 = atof(apTokens[RIV_CA_INST1FEEPAYMENT]+1);
      if (*apTokens[RIV_CA_INST1INTERESTPAYMENT] == '-')
         dPaidInt1 = atof(apTokens[RIV_CA_INST1INTERESTPAYMENT]+1);

      // 2nd inst charge/paid/due
      strcpy(pOutBase->TaxAmt2, apTokens[RIV_CA_INST2TAXCHARGE]);
      if (*apTokens[RIV_CA_INST2TAXPAYMENT] == '-')
         strcpy(pOutBase->PaidAmt2, apTokens[RIV_CA_INST2TAXPAYMENT]+1);
      else
         strcpy(pOutBase->PaidAmt2, apTokens[RIV_CA_INST2TAXPAYMENT]);
      dTax2 = atof(apTokens[RIV_CA_INST2TAXCHARGE]);
      dPaid2 = atof(pOutBase->PaidAmt2);
      dTaxDue2 = atof(apTokens[RIV_CA_INST2AMOUNTDUE]);

      // 2nd penalty/fee/int
      dPen2 = atof(apTokens[RIV_CA_INST2PENALTYCHARGE]);
      strcpy(pOutBase->PenAmt2, apTokens[RIV_CA_INST2PENALTYCHARGE]);
      dFee2 = atof(apTokens[RIV_CA_INST2FEECHARGE]);
      dInt2 = atof(apTokens[RIV_CA_INST2INTERESTCHARGE]);
      dPaidPen2=dPaidFee2=dPaidInt2 = 0;
      if (*apTokens[RIV_CA_INST2PENALTYPAYMENT] == '-')
         dPaidPen2 = atof(apTokens[RIV_CA_INST2PENALTYPAYMENT]+1);
      if (*apTokens[RIV_CA_INST2FEEPAYMENT] == '-')
         dPaidFee2 = atof(apTokens[RIV_CA_INST2FEEPAYMENT]+1);
      if (*apTokens[RIV_CA_INST2INTERESTPAYMENT] == '-')
         dPaidInt2 = atof(apTokens[RIV_CA_INST2INTERESTPAYMENT]+1);

      // TaxAmt
      dTotalTax = dTax1+dTax2;
      pOutBase->dTotalTax = dTotalTax;
      sprintf(pOutBase->TotalTaxAmt, "%.2f", dTotalTax);

      dTotalDue = dTaxDue1+dTaxDue2;
      sprintf(pOutBase->TotalDue, "%.2f", dTotalDue);

      dTotalFees = dFee1+dFee2;
      sprintf(pOutBase->TotalFees, "%.2f", dTotalFees);

      // Paid status
      if (dPaid1 >= dTax1)
         pOutBase->Inst1Status[0] = TAX_BSTAT_PAID;
      else if (!dTax1)
         pOutBase->Inst1Status[0] = TAX_BSTAT_NOTAX;
      else if (dPaid1 < dTax1)
      {
         if (dPen1 > 0)
            pOutBase->Inst1Status[0] = TAX_BSTAT_PASTDUE;
         else
            pOutBase->Inst1Status[0] = TAX_BSTAT_UNPAID;
      } 

      if (dPaid2 >= dTax2)
         pOutBase->Inst2Status[0] = TAX_BSTAT_PAID;
      else if (!dTax2)
         pOutBase->Inst2Status[0] = TAX_BSTAT_NOTAX;
      else if (dPaid2 < dTax2)
      {
         if (dPen2 > 0)
            pOutBase->Inst2Status[0] = TAX_BSTAT_PASTDUE;
         else
            pOutBase->Inst2Status[0] = TAX_BSTAT_UNPAID;
      } 

      // Create detail
      iRet = Riv_CreateTaxItems(sBase, fdDetail, fdItems, fdAgency);
      if (iRet == 1)
      {
         lNoTax++;
         lBad++;
         LogMsg("--- No Detail, taxbillid=%s, PIN=%s", pOutBase->Assmnt_No, pOutBase->Apn);
      }

      if (iRet >= 0)
      {
         Tax_CreateTaxBaseCsv(acOutbuf, (TAXBASE *)&sBase);
         fputs(acOutbuf, fdBase);

         lOut++;
      } else
         lBad++;

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   fclose(fdBase);
   fclose(fdItems);
   fclose(fdAgency);
   fclose(fdCurrent);
   fclose(fdDetail);

   LogMsg("Total records processed:    %u", lCnt);
   LogMsg("         output records:    %u", lOut);
   LogMsg("           skip records:    %u", lBad);
   LogMsg("      no detail records:    %u", lNoTax);

   // Import into SQL
   if (bImport)
   {
      iRet = doTaxImport(myCounty.acCntyCode, TAX_BASE);
      if (!iRet)
         iRet = doTaxImport(myCounty.acCntyCode, TAX_DETAIL);
      if (!iRet)
      {
         // Dedup Agency file before import
         LogMsg("Dedup Agency file %s to %s", sTmpFile, sAgencyFile);
         iRet = sortFile(sTmpFile, sAgencyFile, "S(#1,C,A) DUPOUT F(TXT)");
         if (iRet > 0)
            iRet = doTaxImport(myCounty.acCntyCode, TAX_AGENCY);
      }
   } else
      iRet = 0;

   return iRet;
}

/**************************** Riv_Load_TaxPriorYear *************************
 *
 * Load TTC_PriorYear_test.csv
 *
 ****************************************************************************/

int Riv_Load_TaxPriorYear(char *pDelqFile, bool bImport)
{
   char     *pTmp, acBuf[1024], acRec[MAX_RECSIZE], acOutBuf[2048];
   char     acTmpFile[_MAX_PATH];

   int      iRet, lCnt=0;
   FILE     *fdIn, *fdOut;
   TAXDELQ  *pTaxDelq = (TAXDELQ *)&acBuf[0];
   double   dTotalTax;

   LogMsg0("Loading tax prior year file");

   sprintf(acTmpFile, "%s\\Riv\\Riv_Delq.txt", acTmpPath);
   iRet = sortFile(pDelqFile, acTmpFile, "S(#1,C,A,#3,C,A) DEL(44) OMIT(#4,C,GT,\"S\",OR,#2,C,GT,\"9999\") ");
   if (!iRet)
      return -1;

   // Open input file
   LogMsg("Open tax redemption file %s", acTmpFile);
   fdIn = fopen(acTmpFile, "r");
   if (fdIn == NULL)
   {
      LogMsg("***** Error opening tax redemption file: %s\n", acTmpFile);
      return -2;
   }  

   // Create output file
   NameTaxCsvFile(acTmpFile, myCounty.acCntyCode, "Delq");
   LogMsg("Open Delq file %s", acTmpFile);
   fdOut = fopen(acTmpFile, "w");
   if (fdOut == NULL)
   {
      LogMsg("***** Error creating Delq file: %s\n", acTmpFile);
      return -4;
   }

   // Clear output buffer
   memset(acBuf, 0, sizeof(TAXDELQ));
   // Updated date
   sprintf(pTaxDelq->Upd_Date, "%d", lLastTaxFileDate);

   while (!feof(fdIn))
   {
      pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdIn);
      if (!pTmp || *pTmp > '9')
         break;

      // Bypass BPP records
      if (acRec[10] == 'B' || acRec[1] < '0')
         continue;

      // Parse input record
      iTokens = ParseStringNQ(acRec, cDelim, MAX_FLD_TOKEN, apTokens);
      if (iTokens < RIV_PY_FLDS)
      {
         LogMsg("***** Error: bad delq record for APN=%s (#tokens=%d)", acRec, iTokens);
         break;
      }

#ifdef _DEBUG
      //if (!memcmp(apTokens[RIV_D_PIN], "010008497", 9) )
      //   iRet = 0;
#endif

      dTotalTax = atof(apTokens[RIV_PY_AMOUNTDUE]);
      if (dTotalTax > 0.0)
      {
         // APN
         strcpy(pTaxDelq->Apn, apTokens[RIV_PY_PIN]);

         // Default Number
         strcpy(pTaxDelq->Default_No, apTokens[RIV_PY_DEFAULTNUMBER]);
         strcpy(pTaxDelq->TaxYear, apTokens[RIV_PY_DEFAULTYEAR]);

         // Default Amt - When Fee & Pen are added to Delq table, we can break DefAmt to separate fields
         sprintf(pTaxDelq->Def_Amt, "%.2f", dTotalTax);
         pTaxDelq->isDelq[0] = '1';

         if (!_memicmp(apTokens[RIV_PY_BILLTYPE], "Defaulted Secured", 13))
            pTaxDelq->isSecd[0] = '1';
         else
            pTaxDelq->isSecd[0] = '0';

         // Output delq record for every year
         Tax_CreateDelqCsv(acOutBuf, (TAXDELQ *)acBuf);
         fputs(acOutBuf, fdOut);
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   if (fdOut)
      fclose(fdOut);
   if (fdIn)
      fclose(fdIn);

   LogMsg("Total Delq records processed:    %u", lCnt);

   // Import into SQL
   if (bImport)
      iRet = doTaxImport(myCounty.acCntyCode, TAX_DELINQUENT);
   else
      iRet = 0;

   return iRet;
}

/******************************** Riv_ExtrLegal *****************************
 *
 * Extract legal for specific list of APN from Assessment Roll file
 * Input:  parcel file
 * Output: legal file
 * Return number of records extracted. <0 if error.
 *
 ****************************************************************************/

int Riv_ExtrLegal(char *pParcel, char *pOutfile)
{
   char     *pTmp, acBuf[6000], acTmpFile[_MAX_PATH], acOutFile[_MAX_PATH], 
            acApn[32], acTmp[256], acInRec[6000];
   long     iRet, lCnt=0, lOutRecs=0, lNotFound=0;
   FILE     *fdParcel, *fdOut;
   BOOL     bExtrAll;

   LogMsg0("Extract Legal");

   if (pParcel)
   {
      LogMsg("Extract Legal for %s", pParcel);
      bExtrAll = false;

      // Sort input file
      sprintf(acTmpFile, "%s\\%s\\Parcels.srt", acTmpPath, myCounty.acCntyCode); 
      sprintf(acTmp, "S(1,%d,C,A) OMIT(1,1,C,GT,\"9\") DUPO", iApnLen);
      iRet = sortFile(pParcel, acTmpFile, acTmp);
      if (iRet <= 0)
         return 0;

      // Open parcels file
      LogMsg("Open parcel file %s", acTmpFile);
      fdParcel = fopen(acTmpFile, "r");
      if (fdParcel == NULL)
      {
         LogMsg("***** Error opening parcel file: %s\n", acTmpFile);
         return -1;
      }

      // Get first parcel rec
      pTmp = fgets((char *)&acBuf[0], 512, fdParcel);
      strcpy(acOutFile, pOutfile);
   } else
   {
      LogMsg0("Extract Legal for %s", myCounty.acCntyCode);
      bExtrAll = true;
      fdParcel = NULL;

      GetIniString("Data", "LegalTmpl", "", acTmp, _MAX_PATH, acIniFile);
      sprintf(acOutFile, acTmp, myCounty.acCntyName);
   }

   // Open roll file
   LogMsg("Open Roll file %s", acRollFile);
   fdRoll = fopen(acRollFile, "rb");
   if (fdRoll == NULL)
   {
      printf("***** Error opening roll file: %s\n", acRollFile);
      return -2;
   }

   // Skip header
   pTmp = fgets(acInRec, 6000, fdRoll);

   // Create Output file
   LogMsg("Open output file %s", acOutFile);
   fdOut = fopen(acOutFile, "w");
   if (fdOut == NULL)
   {
      LogMsg("***** Error creating legal file: %s\n", acOutFile);
      return -3;
   }

   // Output header - FIPS|APN_D|LEGAL
   fputs("FIPS|APN_D|LEGAL\n", fdOut);

   // Merge loop
   while (!feof(fdRoll))
   {
      pTmp = fgets(acInRec, 6000, fdRoll);
      if (!pTmp)
         break;

      // Parse input record
      iRet = ParseStringNQ(acInRec, cDelim, MAX_FLD_TOKEN, apTokens);
      if (iRet < R_COLS)
      {
         LogMsg("***** Error: bad input record for APN=%s", apTokens[R_APN]);
         return -1;
      }

      // Ignore unsecured parcel 000-008
      if (memcmp(apTokens[R_APN], "008", 3) <= 0)
         continue;
      // Ignore BPP parcels
      if (!memcmp(apTokens[R_APN]+9, "BPP", 3))
         continue;

#ifdef _DEBUG      
      //if (!memcmp(acInRec, "2079011016", 10))
      //   iRet = 0;
#endif

      if (bExtrAll)
      {
         // Create output record
         replUnPrtChar(apTokens[R_ASSESSMENTDESCRIPTION], ' ');
         blankRem(apTokens[R_ASSESSMENTDESCRIPTION]);
         iRet = formatApn(apTokens[R_APN], acApn, &myCounty);
         sprintf(acBuf, "06065|%s|%s\n", acApn, apTokens[R_ASSESSMENTDESCRIPTION]);   
         fputs(acBuf, fdOut);  
         lOutRecs++;
      } else
      {
ReCheck:
         iRet = memcmp(acBuf, apTokens[R_APN], iApnLen);
         // If matched, get legal
         if (!iRet)
         {
            replUnPrtChar(apTokens[R_ASSESSMENTDESCRIPTION], ' ');
            blankRem(apTokens[R_ASSESSMENTDESCRIPTION]);
            iRet = formatApn(apTokens[R_APN], acApn, &myCounty);
            sprintf(acBuf, "06065|%s|%s\n", acApn, apTokens[R_ASSESSMENTDESCRIPTION]);   
            fputs(acBuf, fdOut);  
            lOutRecs++;

            // Get new parcel
            pTmp = fgets((char *)&acBuf[0], 512, fdParcel);
            if (!pTmp)
               break;
         } else if (iRet < 0)
         {
            // No legal for this record
            acBuf[iApnLen] = 0;
            if (bDebug)
               LogMsg("No legal for: %s", acBuf);
            lNotFound++;

            // Get new parcel
            pTmp = fgets((char *)&acBuf[0], 512, fdParcel);
            if (!pTmp)
               break;

            lCnt++;
            goto ReCheck;
         }
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdRoll)
      fclose(fdRoll);
   if (fdParcel)
      fclose(fdParcel);
   if (fdOut)
      fclose(fdOut);

   LogMsg("Total input records:  %u", lCnt);
   LogMsg("Total output records: %u", lOutRecs);
   LogMsg("Total no legal:       %u", lNotFound);
   LogMsg("Max legal length:     %u", iMaxLegal);

   return lOutRecs;
}

/********************************* Riv_ExtrVal ******************************
 *
 * Extract values from EqualizedRoll.csv
 *
 ****************************************************************************/

int Riv_CreateValueRec(char *pOutbuf)
{
   char     acTmp[256];
   long     iTmp;
   ULONG    lLand, lImpr;

   LIENEXTR *pLienRec = (LIENEXTR *)pOutbuf;
   LONGLONG lTmp;

   // Clear output buffer
   memset(pOutbuf, ' ', sizeof(LIENEXTR));

   // Start copying data
   remChar(apTokens[RIV_ER_PIN], 'X');
   vmemcpy(pLienRec->acApn, apTokens[RIV_ER_PIN], SIZ_LIEN_APN);

   // Year assessed
   memcpy(pLienRec->acYear, myCounty.acYearAssd, 4);

#ifdef _DEBUG      
   //if (!memcmp(apTokens[RIV_ER_PIN], "009613082", 9))
   //   iTmp = 0;
#endif

   if (*(apTokens[RIV_ER_PIN]+9) != 'B')
   {
      // Land
      lLand = atol(apTokens[RIV_ER_TAXABLELANDAMOUNT]);
      if (lLand > 0)
      {
         iTmp = sprintf(acTmp, "%*u", SIZ_LIEN_LAND, lLand);
         memcpy(pLienRec->acLand, acTmp, iTmp);
      }
      // Improve
      lImpr = atol(apTokens[RIV_ER_TAXABLEIMPROVEMENTAMOUNT]);
      if (lImpr > 0)
      {
         iTmp = sprintf(acTmp, "%*u", SIZ_LIEN_IMPR, lImpr);
         memcpy(pLienRec->acImpr, acTmp, iTmp);
      }
   } else
   {
      lLand = 0;
      lImpr = 0;
   }

   // Other values.  Notes: Fixtr & PP are only available in BPP records
   long lLivImpr = atol(apTokens[RIV_ER_TAXABLELIVINGIMPROVEMENTAMOUNT]) ;
   long lPers = atol(apTokens[RIV_ER_TAXABLEPERSONALAMOUNT]);
   long lFixture = atol(apTokens[RIV_ER_TAXABLETRADEFIXTURESAMOUNT]);
   lTmp = lPers + lFixture + lLivImpr;
   if (lTmp > 0)
   {
      iTmp = sprintf(acTmp, "%*u", SIZ_LIEN_OTHERS, lTmp);
      memcpy(pLienRec->acOther, acTmp, iTmp);

      if (lPers > 0)
      {
         iTmp = sprintf(acTmp, "%*u", SIZ_LIEN_PERS, lPers);
         memcpy(pLienRec->acPP_Val, acTmp, iTmp);
      }

      if (lFixture > 0)
      {
         iTmp = sprintf(acTmp, "%*u", SIZ_LIEN_FIXT, lFixture);
         memcpy(pLienRec->acME_Val, acTmp, iTmp);
      }

      if (lLivImpr > 0)
      {
         iTmp = sprintf(acTmp, "%u", lLivImpr);
         memcpy(pLienRec->extra.Riv.LivingImpr, acTmp, iTmp);
      }
   }

   // Gross total
   lTmp += (lLand+lImpr);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*u", SIZ_LIEN_FIXT, lTmp);
      memcpy(pLienRec->acGross, acTmp, SIZ_LIEN_FIXT);
   }

   // Ratio
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*u", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
      memcpy(pLienRec->acRatio, acTmp, SIZ_RATIO);
   }

   // HO Exempt
   long lExe = -atol(apTokens[RIV_ER_HOEAMOUNT]);
   if (lExe > 0)
   {
      pLienRec->acHO[0] = '1';      // 'Y'
      iTmp = sprintf(acTmp, "%*u", SIZ_LIEN_HO_EXE, lExe);
      memcpy(pLienRec->extra.Riv.Exe_Amt1, acTmp, iTmp);
      memcpy(pLienRec->acExCode, "HO", 2);
   } else
      pLienRec->acHO[0] = '2';      // 'N'

   long lOthExe = -atol(apTokens[RIV_ER_OTHEREXEMPTIONAMOUNT]);
   if (lOthExe > 0)
   {
      iTmp = sprintf(acTmp, "%*u", sizeof(pLienRec->extra.Riv.Exe_Amt1), lOthExe);
      memcpy(pLienRec->extra.Riv.Exe_Amt2, acTmp, iTmp);
   }

   // Total Exe
   lTmp = lExe+lOthExe;
   if (lTmp > 0)
   {
      iTmp = sprintf(acTmp, "%*u", sizeof(pLienRec->acExAmt), lTmp);
      memcpy(pLienRec->acExAmt, acTmp, iTmp);
   }

   pLienRec->LF[0] = '\n';
   pLienRec->LF[1] = 0;
   return 0;
}

int Riv_UpdateValueRec(char *pOutbuf)
{
   char     acTmp[256];
   long     iTmp;
   ULONG    lLand, lImpr, lLivImpr;

   LIENEXTR *pLienRec = (LIENEXTR *)pOutbuf;
   LONGLONG lTmp;

#ifdef _DEBUG      
   //if (!memcmp(apTokens[RIV_ER_PIN], "009613082", 9))
   //   iTmp = 0;
#endif

   // Other values
   long lPers = atol(apTokens[RIV_ER_TAXABLEPERSONALAMOUNT]);
   long lFixture = atol(apTokens[RIV_ER_TAXABLETRADEFIXTURESAMOUNT]);
   lLivImpr = atoln(pLienRec->extra.Riv.LivingImpr, SIZ_LIEN_AMT);

   lTmp = lPers + lFixture + lLivImpr;
   if (lTmp > 0)
   {
      iTmp = sprintf(acTmp, "%*u", SIZ_LIEN_OTHERS, lTmp);
      memcpy(pLienRec->acOther, acTmp, iTmp);

      if (lPers > 0)
      {
         iTmp = sprintf(acTmp, "%*u", SIZ_LIEN_PERS, lPers);
         memcpy(pLienRec->acPP_Val, acTmp, iTmp);
      }

      if (lFixture > 0)
      {
         iTmp = sprintf(acTmp, "%*u", SIZ_LIEN_FIXT, lFixture);
         memcpy(pLienRec->acME_Val, acTmp, iTmp);
      }
   }

   // Gross total
   lLand = atoln(pLienRec->acLand, SIZ_LIEN_AMT);
   lImpr = atoln(pLienRec->acImpr, SIZ_LIEN_AMT);
   lTmp += (lLand+lImpr);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*u", SIZ_LIEN_FIXT, lTmp);
      memcpy(pLienRec->acGross, acTmp, SIZ_LIEN_FIXT);
   }

   long lOthExe = -atol(apTokens[RIV_ER_OTHEREXEMPTIONAMOUNT]);
   if (lOthExe)
   {
      iTmp = sprintf(acTmp, "%*u", sizeof(pLienRec->extra.Riv.Exe_Amt1), lOthExe);
      memcpy(pLienRec->extra.Riv.Exe_Amt2, acTmp, iTmp);
      pLienRec->acHO[0] = '2';      // 'N'
   }

   // Total Exe
   long lExe = atoln(pLienRec->acExAmt, 10);
   lTmp = lExe+lOthExe;
   if (lTmp > 0)
   {
      iTmp = sprintf(acTmp, "%*u", sizeof(pLienRec->acExAmt), lTmp);
      memcpy(pLienRec->acExAmt, acTmp, iTmp);
   }

   pLienRec->LF[0] = '\n';
   pLienRec->LF[1] = 0;
   return 0;
}

int Riv_ExtrVal()
{
   char      *pTmp, acBuf[1024], acRec[MAX_RECSIZE], acTmpFile[_MAX_PATH];
   int       iRet;
   long      lOut=0, lCnt=0;
   FILE      *fdOut, *fdIn;
   LIENEXTR  *pRec = (LIENEXTR *)acBuf;

   LogMsg0("Extracting final values from equalized file.");

   // Open input file
   GetIniString(myCounty.acCntyCode, "EQRollFile", "", acTmpFile, _MAX_PATH, acIniFile);
   LogMsg("Open %s for input", acTmpFile);
   fdIn = fopen(acTmpFile, "r");
   if (fdIn == NULL)
   {
      LogMsg("***** Error opening input file: %s\n", acTmpFile);
      return -2;
   }  

   // Open out file
   sprintf(acTmpFile, acValueTmpl, myCounty.acCntyCode, myCounty.acCntyCode);
   LogMsg("Open file %s for output", acTmpFile);
   fdOut = fopen(acTmpFile, "w");
   if (fdOut == NULL)
   {
      LogMsg("***** Error creating output file: %s\n", acTmpFile);
      return -4;
   }

   // Drop header
   pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdIn);

   // Start loop 
   iRet = -1;
   while (!feof(fdIn))
   {
      pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdIn);
      if (!pTmp)
      {
         // Output last record
         if (!iRet)
         {
            fputs(acBuf, fdOut);
            lOut++;
         }

         break;
      }

      // Parse input record
      iTokens = ParseStringIQ(acRec, cDelim, 256, apTokens);
      if (iTokens < RIV_ER_NETTAXABLEAMOUNT)
      {
         if (iTokens > RIV_ER_PIN)
            LogMsg("*** Bad record: %s", apTokens[RIV_ER_PIN]);
         continue;
      }

      if (!_memicmp(apTokens[RIV_ER_ASMTTYPEDESCRIPTION], "Unsecured", 3))
         continue;
      if (!_memicmp(apTokens[RIV_ER_ROLLTYPESHORTDESCRIPTION], "SBE", 3))
         continue;

      // Create new base record
      if (memcmp(apTokens[RIV_ER_PIN], pRec->acApn, iApnLen) || *(apTokens[RIV_ER_PIN]+9) == 'X')
      {
         if (!iRet)
         {
            fputs(acBuf, fdOut);
            lOut++;
         }
         iRet = Riv_CreateValueRec(acBuf);
      } else
         iRet = Riv_UpdateValueRec(acBuf);
      if (iRet)
      {
         LogMsg("---> Drop record %d [APN=%s, TaxYear=%s, Type=%s]", lCnt, apTokens[RIV_ER_PIN], apTokens[RIV_ER_TAXYEAR], apTokens[RIV_ER_ROLLTYPEDESCRIPTION]); 
         iRet = 0;    
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   LogMsg("Total records processed: %u", lCnt);
   LogMsg("Total records output:    %u", lOut);

   // Close files
   if (fdIn)
      fclose(fdIn);
   if (fdOut)
      fclose(fdOut);

   return iRet;
}

/******************************* Riv_RebuildCsv ******************************
 *
 * Rebuild CSV file and validate that each row has a certain number of tokens.
 * Special version for RIV
 *
 * Input file must be a CSV file.
 *
 * Return number of output records if success, <0 if error.
 *
 ****************************************************************************/

int Riv_RebuildCsv(LPSTR pInfile, LPSTR pOutfile, unsigned char cDelim, int iTokens)
{
   int   iCnt=0, iRet;
   char  sBuf[65536], sRec[65536], *pTmp;
   FILE  *fdInput, *fdOutput;

   LogMsg0("Rebuild CSV %s ==> %s", pInfile, pOutfile);

   // Verify file open OK
   fdInput = fopen(pInfile, "r");
   if (!fdInput)
   {
      LogMsg("***** Cannot open input file %s\n", pInfile);
      return -1;
   }

   fdOutput = fopen(pOutfile, "w");
   if (!fdOutput)
   {
      fclose(fdInput);
      LogMsg("***** Cannot open output file %s\n", pOutfile);
      return -2;
   }
            
   while (!feof(fdInput))
   {
#ifdef _DEBUG
      //if (!memcmp(sBuf, "92082|06300400090000", 9))    // 1403606..1406923
      //   iRet = 0;
#endif
      iRet = myGetStrTC(sBuf, cDelim, iTokens, 65536, fdInput);
      if (iRet > 0)
      {
         iRet = replChar(sBuf, 13, 32);
         strcat(sBuf, "\n");     // Put CRLF back on
         iRet = strlen(sBuf);
         if (iRet >= 65535)
            LogMsg("*** WARNING: RebuildCsv() has large string of %d characters.  Please check it out", iRet);
         if (sBuf[9] == '-')
            sBuf[9] = 'X';
         else if (sBuf[0] <= '9')
         {
            pTmp = strchr(sBuf, cDelim);
            *pTmp = 0;
            iRet = strlen(sBuf);
            if (iRet < iApnLen)
            {
               iRet = sprintf(sRec, "%.*s%s%c%s", iApnLen-iRet, "000000", sBuf, cDelim, pTmp+1);
            } else
            {
               *pTmp = cDelim;
            }
         }

         fputs(sBuf, fdOutput);
      } else
      {
         if (iRet == -2)
            LogMsg("***** Record corruption: %.50s", sBuf);
         else if (iRet == -3)
            LogMsg("*** Too many tokens in row: %d", iCnt);
         break;
      }

      if (!(++iCnt % 1000))
         printf("\r%u", iCnt);
   }

   fclose(fdInput);
   fclose(fdOutput);

   LogMsg("RebuildCsv completed with %d records output.", iCnt);

   return iCnt;
}

/*********************************** loadRiv ********************************
 *
 * Input files:
 *    - RIVEXTCT                 (lien file, 160-byte ebcdic, multi record)
 *                               2017 - file in ASCII
 *    - RIVKEYIN                 (roll update file, 622-byte ebcdic)
 *    - RIVPRCH                  (char file, 150-byte ebcdic)
 *    - RIVSALES                 (sale file, 318-byte ebcdic)
 *    - RIV_SALE_NEWCUM.SRT      (cumulative sale up to 20021231)
 *
 * Following are tasks to be done here:
 *    - Convert ebcdic to ascii
 *    - Normal update: LoadOne -U -Us
 *    - Load Lien: LoadOne -L -Us -Xa -X8 [-Xc] 
 *    - Merge cum sale -Ms
 *
 * New input files:
 *    - AssessmentRollCertified.csv (lien file)
 *    - EqualizedRoll_yyyy.csv      (EQ roll file or final roll file) *** used in Riv_ExtrVal()
 *    - AssessmentRoll.csv          (roll update)
 *    - SalesDB.csv                 (sale file)
 *    - PropertyCharacteristics.csv (char file)
 *
 *
 ****************************************************************************/

int loadRiv(int iSkip)
{
   int   iRet=0;
   long  lXlatDate=0;
   char  acRawFile[_MAX_PATH], acTmpFile[_MAX_PATH], acDetailFile[_MAX_PATH],
         acPaidFile[_MAX_PATH], acDelqFile[_MAX_PATH], acTmp[_MAX_PATH];

   iApnLen = myCounty.iApnLen;

   // Reformat DocNum to 7 digits
   //iRet = FixCumSale(acCSalFile, SALE_FLD_DOCNUM+7, false);

   // Fix GRGR_DOC
   //sprintf(acTmpFile, acGrGrTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "Sls");
   //iRet = FixGrGrDoc(acTmpFile, CNTY_RIV, false);

   // Loading Tax
   if (iLoadTax == TAX_LOADING)                    // -T or -Lt
   {
      iRet = GetIniString(myCounty.acCntyCode, "TaxDist", "", acTmpFile, _MAX_PATH, acIniFile);
      if (iRet > 0)
         iRet = LoadTaxCodeTable(acTmpFile);

      GetIniString(myCounty.acCntyCode, "TaxDetail", "", acDetailFile, _MAX_PATH, acIniFile);
      lLastTaxFileDate = getFileDate(acDetailFile);
      // Only process if new tax file
      iRet = isNewTaxFile(acDetailFile, myCounty.acCntyCode);
      if (iRet <= 0)
      {
         if (!iRet)
         {
            LogMsg("*** No new tax file ***");
            iRet = 1;
         }
         return iRet;
      }

      // 03/09/2021 - Load Current Amount & District data
      GetIniString(myCounty.acCntyCode, "TaxPaid", "", acPaidFile, _MAX_PATH, acIniFile);
      iRet = Riv_Load_TaxCurrent(acPaidFile, acDetailFile, bTaxImport);
      if (!iRet)
      {
         GetIniString(myCounty.acCntyCode, "Redemption", "", acDelqFile, _MAX_PATH, acIniFile);
         iRet = Riv_Load_TaxPriorYear(acDelqFile, bTaxImport);
         if (!iRet)
            iRet = updateDelqFlag(myCounty.acCntyCode);
      }

      // Load Tax paid - no import - output will feed into Riv_Load_ExtTaxRoll()
      // GetIniString(myCounty.acCntyCode, "TaxPaid", "", acPaidFile, _MAX_PATH, acIniFile);
      //iRet = Riv_Load_TaxPaidCsv(acPaidFile, false);

      // 01/25/2020
      //iRet = Riv_Load_ExtTaxRoll(acDetailFile, bTaxImport);
      //if (!iRet)
      //{
      //   GetIniString(myCounty.acCntyCode, "Redemption", "", acDelqFile, _MAX_PATH, acIniFile);
      //   sprintf(acTmpFile, "%s\\Riv\\Riv_Redemption.csv", acTmpPath);

      //   // Fix CSV before processing
      //   iRet = RebuildCsv(acDelqFile, acTmpFile, cDelim, RIV_D_COLS);
      //   if (iRet > 1000)
      //      iRet = Riv_Load_TaxDelqCsv(acTmpFile, bTaxImport);
      //   if (!iRet)
      //      iRet = updateDelqFlag(myCounty.acCntyCode);
      //}

      /* Old tax file format
      GetIniString(myCounty.acCntyCode, "TaxPaid", "", acPaidFile, _MAX_PATH, acIniFile);
      lLastTaxFileDate = getFileDate(acPaidFile);
      // Only process if new tax file
      iRet = isNewTaxFile(NULL, myCounty.acCntyCode);
      if (iRet > 0)
      {
         // Convert payment file EBCDIC to ASCII 
         sprintf(acTmpFile, "%s\\Riv\\Riv_Paid.asc", acTmpPath);
         if (bEbc2Asc && (_access(acTmpFile, 0) || (1 != chkFileDate(acTmpFile, acPaidFile))))
         {
            GetIniString(myCounty.acCntyCode, "PaidDef", "", acDefFile, _MAX_PATH, acIniFile);
            if (_access(acDefFile, 0))
            {
               LogMsg("***** Error: missing tax definition file %s", acDefFile);
               return 1;
            }

            LogMsg("Translate %s to Ascii %s", acPaidFile, acTmpFile);
            iRet = F_Ebc2Asc(acPaidFile, acTmpFile, acDefFile, 31);
            if (iRet < 0)
            {
               dispError(iRet, acPaidFile, acTmpFile, acDefFile);
               return 1;
            }
         }
         strcpy(acPaidFile, acTmpFile);

         // Convert redemption file EBCDIC to ASCII 
         GetIniString(myCounty.acCntyCode, "Redemption", "", acRdmFile, _MAX_PATH, acIniFile);
         sprintf(acTmpFile, "%s\\Riv\\Riv_Rdm.asc", acTmpPath);
         if (bEbc2Asc && (_access(acTmpFile, 0) || (1 != chkFileDate(acTmpFile, acRdmFile))))
         {
            // Load layout #1 - Tax year depend
            GetIniString(myCounty.acCntyCode, "RdmDef1", "", acDefFile, _MAX_PATH, acIniFile);
            if (_access(acDefFile, 0))
            {
               LogMsg("***** Error: missing tax definition file %s", acDefFile);
               return 1;
            }

            sprintf(acRdm1File, "%s\\Riv\\Riv_Rdm1.asc", acTmpPath);
            LogMsg("Translate %s to Ascii %s", acRdmFile, acRdm1File);
            iRet = F_Ebc2Asc(acRdmFile, acRdm1File, acDefFile, 14, 0xE3);      // 0xE3='T'
            if (iRet < 0)
            {
               dispError(iRet, acRdmFile, acRdm1File, acDefFile);
               return 1;
            }

            // Load layout #2 - Non tax year depend
            sprintf(acRdm2File, "%s\\Riv\\Riv_Rdm2.asc", acTmpPath);
            GetIniString(myCounty.acCntyCode, "RdmDef2", "", acDefFile, _MAX_PATH, acIniFile);
            if (_access(acDefFile, 0))
            {
               LogMsg("***** Error: missing tax definition file %s", acDefFile);
               return 1;
            }

            LogMsg("Translate %s to Ascii %s", acRdmFile, acRdm2File);
            iRet = F_Ebc2Asc(acRdmFile, acRdm2File, acDefFile, 14, 0xD5); // 0xD5='N'
            if (iRet < 0)
            {
               dispError(iRet, acRdmFile, acRdm2File, acDefFile);
               return 1;
            }

            // Append Redemption files
            sprintf(acTmp, "%s+%s", acRdm1File, acRdm2File);
            iTmp = sortFile(acTmp, acTmpFile, "S(1,9,C,A,26,1,C,A,27,5,C,A) F(TXT) ");
            if (iTmp <= 0)
            {
               iLoadFlag = 0;
               LogMsg("***** ERROR sorting Redemption file %s to %s", acTmp, acTmpFile);
            }
         }
         strcpy(acRdmFile, acTmpFile);

         // Load Tax Paid table
         //iRet = Riv_Load_TaxPaid(acPaidFile, bTaxImport);

         // Set extended due date
         TC_SetDateFmt(0, true);

         // Load Tax Base, Detail & Agency tables
         iRet = Riv_Load_TaxBase(acPaidFile, bTaxImport);

         if (!iRet)
         {
            iRet = Riv_Load_TaxDelq(acRdmFile, bTaxImport);
            if (!iRet)
               iRet = updateDelqFlag(myCounty.acCntyCode);
         }
      } else
         lLastTaxFileDate = 0;
         */
   }

   if (!iLoadFlag && !lOptExtr && !lOptMisc)
      return iRet;

   // Extract final value
   if (lOptExtr & EXTR_FVAL)                       // -Xf
      iRet = Riv_ExtrVal();

   // Merge final value
   if (lOptMisc & M_OPT_MERGVAL)                   // -Mf
   {
      iRet = PQ_MergeValueExt(myCounty.acCntyCode, GRP_RIV, iSkip);
      lOptMisc = 0;
   }

   // Extract legal.  Use as needed
   if (iLoadFlag & EXTR_DESC)                      // -Xd
   {
      // Get list of APN to extract legal
      iRet = GetIniString(myCounty.acCntyCode, "ParcelFile", "", acRawFile, _MAX_PATH, acIniFile);
      if (iRet > 1)
      {
         // Prepare output file
         GetIniString("Data", "LegalTmpl", "", acTmp, _MAX_PATH, acIniFile);
         sprintf(acTmpFile, acTmp, myCounty.acCntyName);
         iRet = Riv_ExtrLegal(acRawFile, acTmpFile);
      } else
         iRet = Riv_ExtrLegal(NULL, NULL);
   }

   // Load GRGR
   if (iLoadFlag & LOAD_GRGR)                      // -G
   {
      // Load special table for GrGr
      GetIniString(myCounty.acCntyCode, "GrGr_Xref", "", acRawFile, _MAX_PATH, acIniFile);
      iNumDeeds = LoadXrefTable(acRawFile, (XREFTBL *)&asDeed[0], MAX_DEED_ENTRIES);

      iRet = Riv_LoadGrGr(myCounty.acCntyCode);
      // Flag that GrGr is loaded
      if (!iRet)
         iLoadFlag |= MERG_GRGR; 
      else if (iRet < 0)
         bGrGrAvail = false;
   } 

   // Load new sale file LDR 2019
   if (iLoadFlag & EXTR_SALE)                      // -Xs
   {
      // Load special table for Sale
      GetIniString(myCounty.acCntyCode, "Sale_Xref", "", acRawFile, _MAX_PATH, acIniFile);
      iNumDeeds = LoadXrefTable(acRawFile, (XREFTBL *)&asDeed[0], MAX_DEED_ENTRIES);

      iRet = Riv_ExtrSale();
      if (!iRet)
         iLoadFlag |= MERG_CSAL;
   }
   //if (iLoadFlag & (UPDT_SALE|EXTR_SALE|EXTR_ATTR))    // -Us, -Xs, -Xa
   //{
   //   if (!strstr(acSaleFile, ".TXT"))
   //   {
   //      lSrcDate = getFileDate(acSaleFile);
   //      lXlatDate = 0;
   //      sprintf(acTmpFile, "%s\\Riv\\Riv_Sale.asc", acTmpPath);
   //      if (!_access(acTmpFile, 0))
   //         lXlatDate = getFileDate(acTmpFile);

   //      if (lSrcDate > lXlatDate)
   //      {
   //         LogMsgD("Translate %s to Ascii %s", acSaleFile, acTmpFile);
   //         // Sort on AsmntNo, Conveyance date
   //         sprintf(acTmp, "S(8,9,C,A,40,7,C,A) F(FIX,%d) OUTREC(%%ETOA,1,%d,CRLF)  ALT(C:\\TOOLS\\EBCDIC.ALT)", iSaleLen, iSaleLen);
   //         iTmp = sortFile(acSaleFile, acTmpFile, acTmp);
   //         if (iTmp <= 0)
   //         {
   //            iLoadFlag = 0;
   //            LogMsg("***** ERROR sorting sale file %s to %s", acSaleFile, acTmpFile);
   //         }
   //      }
   //      strcpy(acSaleFile, acTmpFile);
   //   }

   //   // Update history sale sale
   //   if (iLoadFlag & (UPDT_SALE|EXTR_SALE)) // -Us|-Xs
   //   {
   //      // Load special table for Sale
   //      GetIniString(myCounty.acCntyCode, "Riv_Sale_Xref", "", acRawFile, _MAX_PATH, acIniFile);
   //      iNumDeeds = LoadXrefTable(acRawFile, (XREFTBL *)&asDeed[0], MAX_DEED_ENTRIES);

   //      LogMsgD("Update %s sale history file", myCounty.acCntyCode);
   //      iRet = Riv_UpdateSaleHist();
   //      if (!iRet)
   //         iLoadFlag |= MERG_CSAL;
   //   }

   //   // Extract Chars from sale file
   //   // Remove 5/15/2019 - spn
   //   //if (iLoadFlag & EXTR_ATTR)                   // -Xa
   //   //   iRet = Riv_ExtrCharSale(acSaleFile, acCChrFile);
   //}

	// Extract XC Char file - Riverside, CA XC.txt
   if (lOptExtr & EXTR_XCHAR)                      // -XC
      iRet = Riv_ExtrChar(acCharFile);

   // Load char file
   if (iLoadFlag & EXTR_ATTR)                      // -Xa
   {
      //sprintf(acTmpFile, "%s\\Riv\\Riv_Char.asc", acTmpPath);
      //LogMsgD("Translate %s to Ascii %s", acCharFile, acTmpFile);
      //doEBC2ASC(acCharFile, acTmpFile);

      //// Extract characteristics from both CHAR and SALE files
      //iRet = Riv_ConvStdChar(acTmpFile, acSaleFile);

      // 10/05/2019 - Extract CHAR from PropertyCharacteristics.csv
      iRet = Riv_ConvStdCharCsv(acCharFile);
      if (iRet > 0)
         iRet = 0;
   }

   if (iLoadFlag & MERG_ATTR)                      // -Ma
   {
      iRet = Riv_MergeAttr(iSkip);
      if (!iRet)
         iLoadFlag |= LOAD_UPDT;
      return iRet;
   }

   // Translate ebcdic to ascii
   //if (iLoadFlag & (LOAD_LIEN|LOAD_UPDT))
   //{
   //   // Translate char file
   //   lSrcDate = getFileDate(acCharFile);
   //   lXlatDate = 0;
   //   sprintf(acTmpFile, "%s\\Riv\\Riv_Char.asc", acTmpPath);
   //   if (!_access(acTmpFile, 0))
   //      lXlatDate = getFileDate(acTmpFile);

   //   if (lSrcDate > lXlatDate)
   //   {
   //      LogMsgD("Translate %s to Ascii %s", acCharFile, acTmpFile);
   //      doEBC2ASC(acCharFile, acTmpFile);
   //   }
   //   strcpy(acCharFile, acTmpFile);
   //}

   // Translate Roll file 
   //if (iLoadFlag & LOAD_UPDT)                      // Translate EBCDIC to ASCII if needed
   //{
   //   if (!strstr(acRollFile, ".TXT"))
   //   {
   //      lSrcDate = getFileDate(acRollFile);
   //      lXlatDate = 0;
   //      sprintf(acTmpFile, "%s\\Riv\\Riv_Roll.asc", acTmpPath);
   //      if (!_access(acTmpFile, 0))
   //         lXlatDate = getFileDate(acTmpFile);

   //      if (lSrcDate > lXlatDate)
   //      {
   //         LogMsgD("Translate %s to Ascii %s", acRollFile, acTmpFile);
   //         doEBC2ASC(acRollFile, acTmpFile);
   //      }
   //      strcpy(acRollFile, acTmpFile);
   //   }
   //}

   if (lLienYear > 2018 && (iLoadFlag & (EXTR_LIEN|LOAD_LIEN)))  // Rebuild CSV
   {
      sprintf(acTmpFile, "%s\\Riv\\Riv_Lien.srt", acTmpPath);
      LogMsg("Check lien file for rebuild: %s", acTmpFile);
      if (_access(acTmpFile, 0) || chkFileDate(acRollFile, acTmpFile) == 1)
      {
         sprintf(acTmp, "%s\\Riv\\Riv_Lien.txt", acTmpPath);
         iRet = Riv_RebuildCsv(acRollFile, acTmp, cLdrSep, L_COLS);
         if (iRet < 800000) 
            return -1;

         // Resort LDR file
         iRet = sortFile(acTmp, acTmpFile, "S(1,9,C,A,10,4,C,D) F(TXT)");
         if (iRet < 800000) 
            return -2;
      } else
      {
         LogMsg("Use existing lien file: %s", acTmpFile);
         iRet = 0;
      }
      strcpy(acRollFile, acTmpFile);
   }

   // Extract lien
   if (iLoadFlag & EXTR_LIEN)                      // -Xl
   {
      if (lLienYear <= 2018)
         iRet = Riv_ExtrLien();
      else
         iRet = Riv_ExtrLienCsv();
   }

   // Merge roll file
   if (iLoadFlag & LOAD_LIEN)                      // -L
   {
      //iRet = Riv_Load_LDR(iSkip);
      iRet = GetPrivateProfileString(myCounty.acCntyCode, "NonTax", "Y", acTmpFile, 4, acIniFile);
      //iRet = Riv_Load_LDR1(iSkip, acTmpFile[0]);// 2019-2020
      iRet = Riv_Load_LDR2(iSkip, acTmpFile[0]);// 2021-

      if (!iRet)
         iLoadFlag |= MERG_CSAL;
   } else if (iLoadFlag & LOAD_UPDT)               // -U
   {
      sprintf(acTmpFile, "%s\\Riv\\Riv_Roll.txt", acTmpPath);
      if (_access(acTmpFile, 0) || chkFileDate(acRollFile, acTmpFile) == 1)
      {
         iRet = Riv_RebuildCsv(acRollFile, acTmpFile, ',', R_COLS);
         if (iRet > 800000) iRet = 0;
      } else
         iRet = 0;
      if (!iRet)
      {
         strcpy(acRollFile, acTmpFile);
         iRet = Riv_Load_RollCsv(iSkip);
         if (iBpp > 0)
            iRet = PQ_UpdateBpp(myCounty.acCntyCode, CNTY_RIV);
      }
   } else 
      iRet = 0;

   // Apply cum sale file to R01 - 02/24/2010 spn
   if (!iRet && (iLoadFlag & MERG_CSAL) )          // -Ms
   {
      // Apply Riv_Sale.sls to R01 file
      sprintf(acCSalFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "sls");
      iRet = ApplyCumSale(iSkip, acCSalFile, false, SALE_USE_SCUPDXFR);
   }

   // Update Usecode
   if (iLoadFlag & UPDT_SUSE)
      iRet = updateR01StdUse(myCounty.acCntyCode, iSkip);

   // Extract prop8 flag to text file
   if (lOptProp8 & MYOPT_EXT)                      // -X8 
   {
      GetIniString(myCounty.acCntyCode, "Prop8File", "", acTmpFile, _MAX_PATH, acIniFile);
      if (acTmpFile[0] > ' ' && !_access(acTmpFile, 0))
         iRet = Riv_ExtractProp8(acTmpFile);
      else
         LogMsg("*** Prop8File not defined or available.  Please check INI file");
   }

   // Populate prop8 flag on R01 
   if (!iRet && (lOptProp8 & MYOPT_SET))           // -B8
   {
      GetIniString(myCounty.acCntyCode, "Prop8File", "", acTmpFile, _MAX_PATH, acIniFile);
      iRet = Riv_MergeProp8(acTmpFile, iSkip);
   }

   // Merge GrGr
   if (!iRet && (iLoadFlag & MERG_GRGR))           // -Mg
   {
      iRet = MergeGrGrDoc(myCounty.acCntyCode, 1);
      if (iRet)
         iLoadFlag = 0;
      else
      {
         if (!(iLoadFlag & LOAD_LIEN))
            iLoadFlag |= LOAD_UPDT;
      }
   }

   return iRet;
}