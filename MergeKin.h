#ifndef  _MERGEKIN_H_
#define  _MERGEKIN_H_

// KIN county definition

#define ROFF_APN                         1-1
#define ROFF_TAX_YEAR                    13-1   // Tax year correction
#define ROFF_BOC                         17-1   // Board Order Correction number (blank for base APN)
#define ROFF_ROLL_TYPE                   22-1   // A  = Assessment roll
                                                // C  = SB813 Secured city
                                                // D  = SB813 Secured school
                                                // Q  = SB813 Unsecured city
                                                // R  = SB813 Unsecured school
                                                // S  = Secured tax roll
                                                // T  = Unsecured airplanes
                                                // U  = Unsecured tax roll
                                                // V  = Unsecured supplemental county
                                                // W  = Unsecured supplemental school
                                                // X  = Unsecured prior supplemental
                                                // Y  = Unsecured prior roll
                                                // Z  = Unsecured prior airplane

#define ROFF_TAX_TYPE                    23-1   // BR = Board order refund 
                                                // CB = Corrected bill
                                                // CR = Collection refund 
                                                // CS = Corrected supplemental
                                                // LV = Low value tax
                                                // NB = New bill
                                                // NO = New owner
                                                // PR = Prior refund 
                                                // SB = Supllemental bill
                                                // SP = New bill
                                                // SR = SB813 refund
                                                // TR = Tax collection refund

#define ROFF_RECORD_STATUS               25-1   // A  = Action
                                                // C  = Completed
                                                // D  = Duplicate bill
                                                // I  = Inactive
                                                // L  = ?
                                                // N  = Not processed
                                                // P  = Pending

#define ROFF_FILLER1                     26-1
#define ROFF_TRA                         27-1
#define ROFF_CORTAC_NUMBER               33-1
#define ROFF_CORTAC_CUST_NO              37-1
#define ROFF_CORTAC_LOAN_ID              42-1
#define ROFF_NAME                        67-1
#define ROFF_ALT_APN                     108-1
#define ROFF_FILLER2                     129-1
#define ROFF_LIEN_NAME                   140-1
#define ROFF_M_ADDR_1                    181-1
#define ROFF_M_ADDR_2                    222-1
#define ROFF_M_ADDR_3                    252-1
#define ROFF_M_ADDR_4                    282-1
#define ROFF_MAIL_ADDR_FORMATTED         312-1
#define ROFF_DESC_LINE_1                 313-1
#define ROFF_DESC_LINE_2                 345-1
#define ROFF_DEFAULT_DATE                377-1
#define ROFF_DEFAULT_NUMBER              385-1
#define ROFF_IRS_AMOUNT                  391-1  // IRS non deductible amount
#define ROFF_REDEEMED_DATE               402-1
#define ROFF_ACRES                       410-1
#define ROFF_LAND                        417-1
#define ROFF_FIXED_IMPROV                426-1
#define ROFF_GROWING_IMP                 435-1
#define ROFF_STRUCTURE_IMP               444-1
#define ROFF_PERS_PROP                   453-1
#define ROFF_FILLER3                     462-1
#define ROFF_NET_VALUE                   471-1
#define ROFF_PERS_PROP_PEN_FLAG          480-1
#define ROFF_MOBILE_HOME_FLAG            481-1
#define ROFF_TAXABILITY                  482-1
#define ROFF_FILLER4                     485-1
#define ROFF_INTEREST_DT                 486-1
#define ROFF_AG_PRESERVE_FLAG            494-1
#define ROFF_USE                         495-1
#define ROFF_BUSINESS_CODE               499-1
#define ROFF_FLAG_FOR_REDEMPTION         501-1
#define ROFF_PRIOR_UNSEC_DISCHARGE_FLAG  502-1
#define ROFF_NAME_CHANGE_FLAG            503-1

#define ROFF_NUMBER_OF_EXEMPS            504-1
#define ROFF_EXEMP_1                     509-1
#define ROFF_EXEMP_2                     520-1
#define ROFF_EXEMP_3                     531-1

#define ROFF_TAX_ENTRIES                 542-1
#define ROFF_TAX_1                       547-1
#define ROFF_TAX_2                       600-1
#define ROFF_TAX_3                       653-1
#define ROFF_TAX_4                       706-1
#define ROFF_TAX_5                       759-1
#define ROFF_TAX_6                       812-1
#define ROFF_TAX_7                       865-1
#define ROFF_TAX_8                       918-1
#define ROFF_TAX_9                       971-1
#define ROFF_TAX_10                      1024-1
#define ROFF_TAX_11                      1077-1
#define ROFF_TAX_12                      1130-1
#define ROFF_TAX_13                      1183-1
#define ROFF_TAX_14                      1236-1
#define ROFF_TAX_15                      1289-1
#define ROFF_TAX_16                      1342-1

#define ROFF_1ST_INSTALL_DUE_DATE        1395-1
#define ROFF_1ST_INSTALL_TOT_AMT         1403-1
#define ROFF_1ST_INSTALL_PEN_AMT         1414-1
#define ROFF_1ST_INSTALL_PEN_CODE        1423-1
#define ROFF_COLL_REG_NO1                1424-1    // 1st install collection number, CAN= payment canceled
#define ROFF_COLL_NO1                    1427-1
#define ROFF_SEQ__NO1                    1430-1
#define ROFF_1ST_INSTALL_COLL_DATE       1434-1
#define ROFF_CARRY_OVER_1ST_PAID_FLAG    1442-1    // Y=1st paid, don't carry over
                                                   // N=Carry over to next roll
#define ROFF_CARRY_OVER_RECORD_FLAG      1443-1    // Y=Carry over to next roll
#define ROFF_BOC_NOT_ALLOWED_FLAG        1444-1    // N=No BOC allowed
#define ROFF_FILLER8                     1445-1
#define ROFF_2ND_INSTALL_DUE_DATE        1446-1
#define ROFF_2ND_INSTALL_TOT_AMT         1454-1
#define ROFF_2ND_INSTALL_PEN_AMT         1465-1
#define ROFF_DELINQUENT_COST             1474-1
#define ROFF_2ND_INSTALL_PEN_CODE        1481-1
#define ROFF_COLL_REG_NO2                1482-1
#define ROFF_COLL_NO2                    1485-1
#define ROFF_SEQ__NO2                    1488-1
#define ROFF_2ND_INSTALL_COLL_DATE       1492-1
#define ROFF_REFUND_AMT                  1500-1
#define ROFF_REFUND_XREF                 1511-1    // BOC or Collection number)
#define ROFF_MISC_COST                   1516-1

#define ROFF_ROLL_CHANGE_ENTRIES         1525-1
#define ROFF_ROLL_CHG_NUMBER1            1530-1
#define ROFF_ROLL_CHG_NUMBER2            1584-1
#define ROFF_ROLL_CHG_NUMBER3            1638-1
#define ROFF_ROLL_CHG_NUMBER4            1692-1
#define ROFF_ROLL_CHG_NUMBER5            1746-1
#define ROFF_ROLL_CHG_NUMBER6            1800-1
#define ROFF_ROLL_CHG_NUMBER7            1854-1
#define ROFF_ROLL_CHG_NUMBER8            1908-1

#define ROFF_TRAN_DATE_1                 1962-1
#define ROFF_TRAN_DATE_2                 1970-1
#define ROFF_DELINQ_NOTICE_BYPASS_FLAG   1978-1
#define ROFF_TEETER_APORT_FLAG           1979-1    // Y=Teeter
#define ROFF_FILLER17                    1980-1
#define ROFF_1ST_INSTALL_AMT_PD          1981-1
#define ROFF_2ND_INSTALL_AMT_PD          1992-1
#define ROFF_1ST_INSTALL_CANCEL_DT       2003-1
#define ROFF_2ND_INSTALL_CANCEL_DT       2011-1
#define ROFF_APPORTIONMENT_YEAR          2019-1
#define ROFF_FILLER18                    2023-1
#define ROFF_VENDOR_NUMBER               2029-1    // Refund vendor information
#define ROFF_VENDOR_SUFFIX               2034-1
#define ROFF_VENDOR_NAME                 2035-1
#define ROFF_VENDOR_ADDR                 2076-1
#define ROFF_VENDOR_CITY_STATE           2111-1
#define ROFF_VENDOR_EXTRA_ADDR           2146-1
#define ROFF_WARRANT_NO                  2181-1
#define ROFF_REFUND_DATE                 2188-1
#define ROFF_SB813_SUPPL_BILL_CNT        2196-1
#define ROFF_SB813_TRANSFER_DATE         2199-1
#define ROFF_SB813_PRORATION_FACTOR      2207-1
#define ROFF_SB813_PRORATION_PERCENT     2213-1
#define ROFF_DAYS_OWNED                  2219-1
#define ROFF_OWNERSHIP_PERIOD            2222-1
#define ROFF_FILLER19                    2225-1
// Unsecured delinquent items
#define ROFF_LIEN_INSTRUMENT_NUMBER      2228-1
#define ROFF_LIEN_DATE                   2238-1
#define ROFF_LIEN_RECORD_NUMBER          2246-1
#define ROFF_RELEASE_RECORD_NUMBER       2255-1
#define ROFF_RELEASE_DATE                2264-1
#define ROFF_INTENT_DATE                 2272-1
#define ROFF_FILLER20                    2280-1
#define ROFF_JUDGEMENT_DATE              2286-1
#define ROFF_JUDGEMENT_RECORD_NUMBER     2294-1
#define ROFF_SATISFACTION_RECORD_NUMBER  2303-1
#define ROFF_SATISFACTION_DATE           2312-1
#define ROFF_FEE_ASSMT                   2320-1
#define ROFF_ORIGINATING_ASSMT           2332-1
#define ROFF_JUDGEMENT_NUMBER            2344-1
#define ROFF_FILLER21                    2355-1
#define ROFF_ALTERNATE_SORT              2362-1
#define ROFF_FILLER22                    2380-1
#define ROFF_DATE_REFUND_WAS_AUTHOR      2388-1
#define ROFF_ORIG_2ND_IN_DUE_DATE        2396-1    // Used by redempt: If BOC occurs, then orig due date is saved
                                                   // Sec delinq data Power sell/Recis
#define ROFF_COURT                       2404-1
#define ROFF_BANKRUPTCY_NUMBER           2408-1
#define ROFF_BANKRUPTCY_DATE             2420-1
#define ROFF_BANKRUPTCY_TYPE             2428-1
#define ROFF_SELL_NOTICE_DATE            2430-1
#define ROFF_SELL_DOCUMENT_NUMBER        2438-1
#define ROFF_SELL_NOTICE_RECORD_NUMBER   2447-1
#define ROFF_RECISION_DATE               2456-1
#define ROFF_RECISION_DOCUMENT_NUMBER    2464-1
#define ROFF_RECISION_RECORD_NUMBER      2473-1
#define ROFF_XREF_APN                    2482-1
#define ROFF_DEED_DATE                   2494-1
#define ROFF_DEED_DOC_NUMBER             2502-1
#define ROFF_BANKRUPTCY_REMOVED_DATE     2516-1
#define ROFF_ZIP_CODE_SORT               2524-1
#define ROFF_TAX_BILL_DATE               2534-1
#define ROFF_APPORT_INS1_DATE            2542-1
#define ROFF_APPORT_INS2_DATE            2550-1
#define ROFF_FILLER23                    2558-1
#define ROFF_S_STRNUM                    2571-1
#define ROFF_S_STRDIR                    2576-1
#define ROFF_S_STRNAME                   2578-1
#define ROFF_S_STRSFX                    2602-1
#define ROFF_S_UNIT                      2606-1
#define ROFF_S_CITY                      2610-1
#define ROFF_S_MULT_DESCR                2614-1
#define ROFF_FILLER24                    2615-1

#define RSIZ_APN                         12
#define RSIZ_TAX_YEAR                    4
#define RSIZ_BOC                         5
#define RSIZ_ROLL_TYPE                   1
#define RSIZ_TAX_TYPE                    2
#define RSIZ_RECORD_STATUS               1
#define RSIZ_FILLER1                     1
#define RSIZ_TRA                         6
#define RSIZ_CORTAC_NUMBER               4
#define RSIZ_CORTAC_CUST_NO              5
#define RSIZ_CORTAC_LOAN_ID              25
#define RSIZ_NAME                        41
#define RSIZ_ALT_APN                     21
#define RSIZ_FILLER2                     11
#define RSIZ_M_ADDR_1                    41
#define RSIZ_M_ADDR_2                    30
#define RSIZ_M_ADDR_3                    30
#define RSIZ_M_ADDR_4                    30
#define RSIZ_MAIL_ADDR_FORMATTED         1
#define RSIZ_DESC_LINE_1                 32
#define RSIZ_DESC_LINE_2                 32
#define RSIZ_DEFAULT_NO                  6
#define RSIZ_IRS_AMOUNT                  11
#define RSIZ_ACRES                       7
#define RSIZ_LAND                        9
#define RSIZ_FIXED_IMPROV                9
#define RSIZ_GROWING_IMP                 9
#define RSIZ_STRUCTURE_IMP               9
#define RSIZ_PERS_PROP                   9
#define RSIZ_FILLER3                     9
#define RSIZ_NET_VALUE                   9
#define RSIZ_PERS_PROP_PEN_FLAG          1
#define RSIZ_MOBILE_HOME_FLAG            1
#define RSIZ_TAXABILITY                  3
#define RSIZ_FILLER4                     1
#define RSIZ_AG_PRESERVE_FLAG            1
#define RSIZ_USE_CODE                    4
#define RSIZ_BUSINESS_CODE               2
#define RSIZ_FLAG_FOR_REDEMPTION         1
#define RSIZ_PRIOR_UNSEC_DISCHARGE_FLAG  1
#define RSIZ_NAME_CHANGE_FLAG            1
#define RSIZ_NUMBER_OF_EXEMPS            5
#define RSIZ_EXE_AMT                     9

#define RSIZ_TAX_ENTRIES                 5
#define RSIZ_TAX_CODE                    3
#define RSIZ_TAX_RATE                    9
#define RSIZ_INST_AMT                    11
#define RSIZ_PEN_AMT                     9

#define RSIZ_1ST_INSTALL_PEN_CODE        1
#define RSIZ_COLL_REG_NO1                3
#define RSIZ_COLL_NO1                    3
#define RSIZ_SEQ_NO1                     4
#define RSIZ_CARRY_OVER_1ST_PAID_FLAG    1
#define RSIZ_CARRY_OVER_RECORD_FLAG      1
#define RSIZ_BOC_NOT_ALLOWED_FLAG        1
#define RSIZ_FILLER8                     1
#define RSIZ_DELINQUENT_COST             7
#define RSIZ_2ND_INSTALL_PEN_CODE        1
#define RSIZ_COLL_REG_NO2                3
#define RSIZ_COLL_NO2                    3
#define RSIZ_SEQ_NO2                     4
#define RSIZ_REFUND_AMT                  11
#define RSIZ_REFUND_XREF                 5
#define RSIZ_MISC_COST                   9

#define RSIZ_ROLL_CHANGE_ENTRIES         5
#define RSIZ_ROLL_CHG_NUMBER             5
#define RSIZ_ROLL_CHG_PERSON             4
#define RSIZ_R_T_CODE                    7
#define RSIZ_R_T_DESCRIPTION             17
#define RSIZ_ROLL_CHG_TYPE_OF_ACTION     2
#define RSIZ_ROLL_CHG_FILLER             2
#define RSIZ_ROLL_CHG_TYPE               2
#define RSIZ_ROLL_CHG_XREF               7

#define RSIZ_DELINQ_NOTICE_BYPASS_FLAG   1
#define RSIZ_TEETER_APORT_FLAG           1
#define RSIZ_FILLER17                    1
#define RSIZ_FILLER18                    6
#define RSIZ_VENDOR_NUMBER               5
#define RSIZ_VENDOR_SUFFIX               1
#define RSIZ_VENDOR_NAME                 41
#define RSIZ_VENDOR_ADDR                 35
#define RSIZ_VENDOR_CITY_STATE           35
#define RSIZ_VENDOR_EXTRA_ADDR           35
#define RSIZ_WARRANT                     7
#define RSIZ_SB813_SUPPL_BILL_CNT        3
#define RSIZ_SB813_PRORATION_FACTOR      6
#define RSIZ_SB813_PRORATION_PERCENT     6
#define RSIZ_DAYS_OWNED                  3
#define RSIZ_OWNERSHIP_PERIOD            3
#define RSIZ_FILLER19                    3
#define RSIZ_LIEN_INSTRUMENT_NUMBER      10
#define RSIZ_LIEN_RECORD_NUMBER          9
#define RSIZ_RELEASE_RECORD_NUMBER       9
#define RSIZ_FILLER20                    6
#define RSIZ_JUDGEMENT_RECORD_NUMBER     9
#define RSIZ_SATISFACTION_RECORD_NUMBER  9
#define RSIZ_FEE_ASSMT                   12
#define RSIZ_ORIGINATING_ASSMT           12
#define RSIZ_JUDGEMENT_NUMBER            11
#define RSIZ_FILLER21                    7
#define RSIZ_ALTERNATE_SORT              18
#define RSIZ_FILLER22                    8
#define RSIZ_COURT                       4
#define RSIZ_BANKRUPTCY_NUMBER           12
#define RSIZ_BANKRUPTCY_TYPE             2
#define RSIZ_SELL_DOCUMENT_NUMBER        9
#define RSIZ_SELL_NOTICE_RECORD_NUMBER   9
#define RSIZ_RECISION_DOCUMENT_NUMBER    9
#define RSIZ_RECISION_RECORD_NUMBER      9
#define RSIZ_XREF_APN                    12
#define RSIZ_DEED_DATE                   8
#define RSIZ_DEED_DOC_NUMBER             14
#define RSIZ_ZIP_CODE_SORT               10
#define RSIZ_FILLER23                    13
#define RSIZ_S_STRNUM                    5
#define RSIZ_S_STRDIR                    2
#define RSIZ_S_STRNAME                   24
#define RSIZ_S_STRSFX                    4
#define RSIZ_S_UNIT                      4
#define RSIZ_S_CITY                      4
#define RSIZ_S_MULT_DESCR                1
#define RSIZ_FILLER24                    385
#define RSIZ_DATE8                       8

#define MAX_TAX_ITEMS                    16
#define MAX_EXE_ITEMS                    3
#define MAX_RCHG_ITEMS                   8

typedef struct _tSitusRec
{
   char S_StrNum[RSIZ_S_STRNUM];
   char S_StrDir[RSIZ_S_STRDIR];
   char S_StrName[RSIZ_S_STRNAME];
   char S_StrSfx[RSIZ_S_STRSFX];
   char S_Unit[RSIZ_S_UNIT];
   char S_CityAbbr[RSIZ_S_CITY];
} KINSITUS;

typedef struct _tMailRec
{
   char M_Addr1[RSIZ_M_ADDR_1];
   char M_Addr2[RSIZ_M_ADDR_2];
   char M_Addr3[RSIZ_M_ADDR_2];
   char M_Addr4[RSIZ_M_ADDR_2];
} KINMAIL;

typedef struct _tExeRec
{
   char  Code;
   char  filler;
   char  Amt[RSIZ_EXE_AMT];
} KIN_EXE;

typedef struct _tTaxInfo
{  // First rec starts at 547
   char  Code[RSIZ_TAX_CODE];          // 547
   char  Rate[RSIZ_TAX_RATE];          // 999V999999
   char  Type;                         // 1 = DIRECT ASSESS         2 = LAND ONLY (NO MINERAL)
                                       // 3 = LAND+IMP (NO MINERAL) 4 = LND+IMP+PPV+BIV
                                       // 5 = LAND                  6 = LAND + IMP
                                       // 7 = NET (ASSESS - EXEMP)  9 = DIRECT ASSESS

   char  Inst_Amt1[RSIZ_INST_AMT];     // 560
   char  Pen_Amt1[RSIZ_PEN_AMT];       // 571
   char  Inst_Amt2[RSIZ_INST_AMT];     // 580
   char  Pen_Amt2[RSIZ_PEN_AMT];       // 591
} KIN_TAX;
typedef struct _tRollChg
{  // 1530
   char  Number[RSIZ_ROLL_CHG_NUMBER];
   char  Date[RSIZ_DATE8];
   char  Person[RSIZ_ROLL_CHG_PERSON];
   char  RTCode[RSIZ_R_T_CODE];
   char  RTDesc[RSIZ_R_T_DESCRIPTION];
   char  TypeOfAction[RSIZ_ROLL_CHG_TYPE_OF_ACTION];
   char  filler[RSIZ_ROLL_CHG_FILLER];
   char  Type[RSIZ_ROLL_CHG_TYPE];
   char  Xref[RSIZ_ROLL_CHG_XREF];
} KIN_RCHG;

typedef struct _tKinRoll
{  // 3001 bytes ascii
   char  Apn[RSIZ_APN];
   char  TaxYear[RSIZ_TAX_YEAR];
   char  BOC[RSIZ_BOC];
   char  RollType;
   char  TaxType[RSIZ_TAX_TYPE];
   char  Status;
   char  filler1;
   char  TRA[RSIZ_TRA];
   char  Cortac_Num[RSIZ_CORTAC_NUMBER];
   char  Cortac_CustNo[RSIZ_CORTAC_CUST_NO];
   char  Cortac_LoanID[RSIZ_CORTAC_LOAN_ID];
   char  OwnerName[RSIZ_NAME];
   char  Alt_Apn[RSIZ_ALT_APN];
   char  filler2[RSIZ_FILLER2];
   char  Assessee[RSIZ_NAME];

   KINMAIL Mail;
   //char  M_Addr1[RSIZ_M_ADDR_1];       // 181
   //char  M_Addr2[RSIZ_M_ADDR_2];       // 222
   //char  M_Addr3[RSIZ_M_ADDR_3];       // 252
   //char  M_Addr4[RSIZ_M_ADDR_4];       // 282
   char  M_Addr_Fmt;
   char  Desc_Line1[RSIZ_DESC_LINE_1];             // 313 - Legal or situs
   char  Desc_Line2[RSIZ_DESC_LINE_1];
   char  Default_Date[RSIZ_DATE8];                 // 377 - yyyymmdd
   char  Default_No[RSIZ_DEFAULT_NO];
   char  IRS_Amt[RSIZ_IRS_AMOUNT];
   char  Redeemed_Date[RSIZ_DATE8];                // 402
   char  Acreage[RSIZ_ACRES];                      // V99
   char  Land[RSIZ_LAND];                          // 417
   char  Fixed_Impr[RSIZ_FIXED_IMPROV];
   char  Growing_Impr[RSIZ_GROWING_IMP];
   char  Struct_Impr[RSIZ_STRUCTURE_IMP];
   char  PP_Val[RSIZ_PERS_PROP];
   char  filler3[RSIZ_FILLER3];
   char  NetVal[RSIZ_NET_VALUE];
   char  PP_Pen_Flg;
   char  MH_Flg;
   char  Taxability[RSIZ_TAXABILITY];
   char  filler4[RSIZ_FILLER4];
   char  Int_Date[RSIZ_DATE8];                     // Interest date
   char  AgPreserve;
   char  UseCode[RSIZ_USE_CODE];
   char  Bus_Code[RSIZ_BUSINESS_CODE];
   char  Redemption_Flg;
   char  Discharged_Flg;
   char  NameChg_Flg;

   char     NumExemps[RSIZ_NUMBER_OF_EXEMPS];
   KIN_EXE  asExeItems[MAX_EXE_ITEMS];             // 509

   char     NumTaxes[RSIZ_TAX_ENTRIES];
   KIN_TAX  asTaxItems[MAX_TAX_ITEMS];             // 547

   char  Inst_Due_Date1[RSIZ_DATE8];
   char  Inst_Tot_Amt1[RSIZ_INST_AMT];
   char  Inst_Pen_Amt1[RSIZ_PEN_AMT];
   char  Inst_Pen_Code1;                           // 1423 - P
   char  Coll_Reg1[RSIZ_COLL_REG_NO1];
   char  Coll_No1[RSIZ_COLL_NO1];
   char  Seq_No1[RSIZ_SEQ_NO1];
   char  Inst_Paid_Date1[RSIZ_DATE8];
   char  Carry_Over_1st_Paid_Flg;
   char  Carry_Over_Record_Flg;
   char  BOC_Not_Allowed_Flg;
   char  filler8;
   char  Inst_Due_Date2[RSIZ_DATE8];
   char  Inst_Tot_Amt2[RSIZ_INST_AMT];
   char  Inst_Pen_Amt2[RSIZ_PEN_AMT];
   char  Delq_Cost[RSIZ_DELINQUENT_COST];          // 1474 - V99
   char  Inst_Pen_Code2;
   char  Coll_Reg2[RSIZ_COLL_REG_NO1];
   char  Coll_No2[RSIZ_COLL_NO1];
   char  Seq_No2[RSIZ_SEQ_NO1];
   char  Inst_Paid_Date2[RSIZ_DATE8];
   char  Refund_Amt[RSIZ_REFUND_AMT];
   char  Refund_Zref[RSIZ_REFUND_XREF];
   char  Misc_Cost[RSIZ_MISC_COST];

   char  NumChgs[RSIZ_ROLL_CHANGE_ENTRIES];
   KIN_RCHG asRollChg[MAX_RCHG_ITEMS];

   char  TranDate1[RSIZ_DATE8];
   char  TranDate2[RSIZ_DATE8];
   char  Delq_Notice_Bypass_Flg;
   char  Teeter_Aport_Flg;
   char  filler17;
   char  Inst_Paid_Amt1[RSIZ_INST_AMT];                  // 1981 - V99
   char  Inst_Paid_Amt2[RSIZ_INST_AMT];                  // 1992
   char  Inst_Cancel_Dt1[RSIZ_DATE8];
   char  Inst_Cancel_Dt2[RSIZ_DATE8];
   char  Apportionment_Year[RSIZ_TAX_YEAR];
   char  filler18[RSIZ_FILLER18];
   char  Vendor_Number[RSIZ_VENDOR_NUMBER];              // 2035 - 
   char  Vendor_Suffix;
   char  Vendor_Name[RSIZ_VENDOR_NAME];                  // 2041
   char  Vendor_Addr[RSIZ_VENDOR_ADDR];
   char  Vendor_CitySt[RSIZ_VENDOR_CITY_STATE];
   char  Vendor_Extra_Addr[RSIZ_VENDOR_EXTRA_ADDR];
   char  Warrant[RSIZ_WARRANT];
   char  Refund_Date[RSIZ_DATE8];
   char  SB813_Suppl_Bill_Cnt[RSIZ_SB813_SUPPL_BILL_CNT];// 2196
   char  SB813_Transfer_Date[RSIZ_DATE8];
   char  SB813_Proration_Factor[RSIZ_SB813_PRORATION_FACTOR];
   char  SB813_Proration_Percent[RSIZ_SB813_PRORATION_PERCENT];
   char  Days_Owned[RSIZ_DAYS_OWNED];
   char  Own_Period[RSIZ_OWNERSHIP_PERIOD];
   char  filler19[RSIZ_FILLER19];
   char  Lien_Instrument_Num[RSIZ_LIEN_INSTRUMENT_NUMBER];
   char  Lien_Date[RSIZ_DATE8];
   char  Lien_RecNum[RSIZ_LIEN_RECORD_NUMBER];
   char  Release_RecNum[RSIZ_RELEASE_RECORD_NUMBER];
   char  Release_Date[RSIZ_DATE8];
   char  Intent_Date[RSIZ_DATE8];
   char  filler20[RSIZ_FILLER20];
   char  Judgement_Date[RSIZ_DATE8];
   char  Judgement_RecNum[RSIZ_JUDGEMENT_RECORD_NUMBER];
   char  Satisfaction_RecNum[RSIZ_SATISFACTION_RECORD_NUMBER];
   char  Satisfaction_Date[RSIZ_DATE8];
   char  Fee_Asmt[RSIZ_FEE_ASSMT];
   char  Org_Asmt[RSIZ_FEE_ASSMT];
   char  Judgement_Number[RSIZ_JUDGEMENT_NUMBER];
   char  filler21[RSIZ_FILLER21];
   char  Alt_Sort[RSIZ_ALTERNATE_SORT];
   char  filler22[RSIZ_FILLER22];
   char  Refund_Author_Date[RSIZ_DATE8];
   char  Orig_2nd_In_Due_Date[RSIZ_DATE8];
   char  Court[RSIZ_COURT];
   char  Bankruptcy_Number[RSIZ_BANKRUPTCY_NUMBER];
   char  Bankruptcy_Date[RSIZ_DATE8];
   char  Bankruptcy_Type[RSIZ_BANKRUPTCY_TYPE];
   char  Sell_Notice_Date[RSIZ_DATE8];
   char  Sell_DocNum[RSIZ_SELL_DOCUMENT_NUMBER];
   char  Sell_Notice_RecNum[RSIZ_SELL_NOTICE_RECORD_NUMBER];
   char  Recision_Date[RSIZ_DATE8];
   char  Recision_DocNum[RSIZ_SELL_DOCUMENT_NUMBER];
   char  Recision_RecNum[RSIZ_SELL_NOTICE_RECORD_NUMBER];
   char  Xref_Apn[RSIZ_XREF_APN];
   char  DeedDate[RSIZ_DATE8];                              // 2494
   char  DeedNum[RSIZ_DEED_DOC_NUMBER];
   char  Bankruptcy_Removed_Date[RSIZ_DATE8];
   char  ZipCode[RSIZ_ZIP_CODE_SORT];
   char  TaxBill_Date[RSIZ_DATE8];
   char  Apport_Ins1_Date[RSIZ_DATE8];
   char  Apport_Ins2_Date[RSIZ_DATE8];
   char  filler23[RSIZ_FILLER23];
   KINSITUS Situs;
   //char  S_StrNum[RSIZ_S_STRNUM];
   //char  S_StrDir[RSIZ_S_STRDIR];
   //char  S_StrName[RSIZ_S_STRNAME];
   //char  S_StrSfx[RSIZ_S_STRSFX];
   //char  S_Unit[RSIZ_S_UNIT];
   //char  S_CityAbbr[RSIZ_S_CITY];
   char  S_Mult_Desc[RSIZ_S_MULT_DESCR];
   char  filler24[RSIZ_FILLER24];
} KIN_ROLL;

#define SOFF_APN         1
#define SOFF_DOC_YEAR    13
#define SOFF_DOC_TYPE    17
#define SOFF_DOC_NUM     18
#define SOFF_DOC_DATE    27
#define SOFF_TRANSFEROR  35
#define SOFF_TRANSFEREE  76
#define SOFF_ACRES       117
#define SOFF_SALE_PRICE  124
#define SOFF_DOC_TAX     133
#define SOFF_GRP_APN     144
#define SOFF_S_STRNAME   156
#define SOFF_S_STRNUM    180
#define SOFF_S_STRSFX    185
#define SOFF_S_STRDIR    189
#define SOFF_S_UNIT      191
#define SOFF_S_CITY      195

#define SSIZ_APN         12
#define SSIZ_DOC_YEAR    4
#define SSIZ_DOC_TYPE    1
#define SSIZ_DOC_NUM     9
#define SSIZ_DOC_DATE    8
#define SSIZ_TRANSFEROR  41
#define SSIZ_TRANSFEREE  41
#define SSIZ_ACRES       7
#define SSIZ_SALE_PRICE  9
#define SSIZ_DOC_TAX     11
#define SSIZ_GRP_APN     12
#define SSIZ_S_STRNAME   24
#define SSIZ_S_STRNUM    5
#define SSIZ_S_STRSFX    4
#define SSIZ_S_STRDIR    2
#define SSIZ_S_UNIT      4
#define SSIZ_S_CITY      4

typedef struct _tKinSale
{  // 200 bytes ascii (198-bytes ebcdic)
   char  Apn[SSIZ_APN];
   char  DocYear[SSIZ_DOC_YEAR];
   char  DocType;
   char  DocNum[SSIZ_DOC_NUM];
   char  DocDate[SSIZ_DOC_DATE];
   char  Transferor[SSIZ_TRANSFEROR];
   char  Transferee[SSIZ_TRANSFEREE];
   char  Acres[SSIZ_ACRES];            // 9(5)V99
   char  SalePrice[SSIZ_SALE_PRICE];
   char  Doc_Tax[SSIZ_DOC_TAX];        // 9(9)V99
   char  GroupApn[SSIZ_GRP_APN];
   char  S_StrName[SSIZ_S_STRNAME];
   char  S_StrNum[SSIZ_S_STRNUM];
   char  S_StrSfx[SSIZ_S_STRSFX];
   char  S_StrDir[SSIZ_S_STRDIR];
   char  S_Unit[SSIZ_S_UNIT];
   char  S_City[SSIZ_S_CITY];
   char  CrLf[2];
} KIN_SALE;

#define  OFF_CSAL_APN         1
#define  OFF_CSAL_DOCTYPE     13
#define  OFF_CSAL_DOCNUM      14
#define  OFF_CSAL_RECDATE     23

typedef struct t_KinCumSale
{  // 180 bytes ascii
   char  Apn[SSIZ_APN];
   char  DocType;
   char  DocNum[SSIZ_DOC_NUM];
   char  DocDate[SSIZ_DOC_DATE];       // YYYYMMDD
   char  Buyer[SSIZ_TRANSFEROR];
   char  Seller[SSIZ_TRANSFEROR];      // Seller or primary apn of multi-parc sale
   char  SalePrice[SSIZ_SALE_PRICE];
   char  S_StrNum[6];
   char  S_StrDir[2];
   char  S_StrName[24];
   char  S_StrSfx[9];
   char  S_City[4];
   char  filler[14];
} KIN_CSAL;


#define COFF_APN             1
#define COFF_CHAR_TYPE       13
#define COFF_BLDG_CLASS      14
#define COFF_EFF_YEAR        19
#define COFF_CONSTR_YEAR     23
#define COFF_LIVING_AREA     27
#define COFF_BATHS           33
#define COFF_BEDS            36
#define COFF_ROOMS           38
#define COFF_GARAGE_AREA     40
#define COFF_CARPORT         46
#define COFF_HEATING         47
#define COFF_COOLING         49
#define COFF_MISC_IMPROV     51
#define COFF_NEIGH_CODE      52
#define COFF_POOL_SPA        56
#define COFF_FIREPLACE       57
#define COFF_RES_STRUCT2     58
#define COFF_LOT_AREA        59
#define COFF_BASEMENT_SQFT   68
#define COFF_FLR2_SQFT       74
#define COFF_STORIES         80
#define COFF_SKIRTING        83
#define COFF_PORCH_SQFT      86
#define COFF_STORAGE_SQFT    92
#define COFF_FENCE           98
#define COFF_AWNING_SQFT     103
#define COFF_MODEL           109
#define COFF_MANUFACTURER    119
#define COFF_UNUSED          129

#define CSIZ_APN             12
#define CSIZ_CHAR_TYPE       1
#define CSIZ_BLDG_CLASS      5
#define CSIZ_EFF_YR          4
#define CSIZ_CONSTR_YEAR     4
#define CSIZ_LIVING_AREA     6
#define CSIZ_BATHS           3
#define CSIZ_BEDS            2
#define CSIZ_ROOMS           2
#define CSIZ_GARAGE_AREA     6
#define CSIZ_CARPORT         1
#define CSIZ_HEATING         2
#define CSIZ_COOLING         2
#define CSIZ_MISC_IMPROV     1
#define CSIZ_NEIGH_CODE      4
#define CSIZ_POOL_SPA        1
#define CSIZ_FIREPLACE       1
#define CSIZ_RES_STRUCT2     1
#define CSIZ_LOT_AREA        9
#define CSIZ_BASEMENT_SQFT   6
#define CSIZ_FLR2_SQFT       6
#define CSIZ_STORIES         3
#define CSIZ_SKIRTING        3
#define CSIZ_PORCH_SQFT      6
#define CSIZ_STORAGE_SQFT    6
#define CSIZ_FENCE           5
#define CSIZ_AWNING_SQFT     6
#define CSIZ_MODEL           10
#define CSIZ_MANUFACTURER    10
#define CSIZ_UNUSED          122

typedef struct _tKinChar
{
   char  Apn[CSIZ_APN];
   char  Char_Type[CSIZ_CHAR_TYPE];    // 'R' = RESIDENTIAL
                                       // '1' = M/H SINGLE WIDE
                                       // '2' = M/H DOUBLE WIDE
                                       // '3' = M/H TRIPLE WIDE
                                       // 'S' = M/H SPECIAL
                                       // 'C' = COMMERCIAL
                                       // 'I' = INDUSTRIAL
   char  BldgClass[CSIZ_BLDG_CLASS];
   char  EffYear[CSIZ_EFF_YR];
   char  YrBlt[CSIZ_EFF_YR];
   char  BldgSqft[CSIZ_LIVING_AREA];
   char  Baths[CSIZ_BATHS];            // 99V9
   char  Beds[CSIZ_BEDS];
   char  Rooms[CSIZ_ROOMS];
   char  GarSqft[CSIZ_GARAGE_AREA];
   char  Carport;                      // Y/N
   char  Heating[CSIZ_HEATING];        // M=MODERN C=CONVENTIONAL
   char  Cooling[CSIZ_COOLING];        // M=MODERN C=CONVENTIONAL
   char  MiscImpr;                     // Y/N
   // SFR char.
   char  Neigh_Code[CSIZ_NEIGH_CODE];
   char  Pool_Spa;                     // P, S, or B=both
   char  Fireplace;                    // Number of fireplaces
   char  Res_Struct2;                  // Y/N
   char  LotSqft[CSIZ_LOT_AREA];
   char  BsmtSqft[CSIZ_BASEMENT_SQFT];
   char  Flr2_Sqft[CSIZ_FLR2_SQFT];
   char  Stories[CSIZ_STORIES];        // 99V9
   // Mobile home char
   char  Skirting[CSIZ_SKIRTING];      // Linear feet
   char  PorchSqft[CSIZ_PORCH_SQFT];
   char  StorageSqft[CSIZ_STORAGE_SQFT];
   char  Fence[CSIZ_FENCE];            // Linear feet
   char  AwningSqft[CSIZ_AWNING_SQFT];
   char  Model[CSIZ_MODEL];
   char  Manufacturer[CSIZ_MANUFACTURER];
   char  filler[CSIZ_UNUSED];
} KIN_CHAR;

// Public parcels file
#define POFF_APN                         1
#define POFF_OWNER                       13
#define POFF_ADDR1                       54
#define POFF_ADDR2                       95
#define POFF_ADDR3                       125
#define POFF_ADDR4                       155
#define POFF_SECTION                     185
#define POFF_TOWNSHIP                    187
#define POFF_RANGE                       189
#define POFF_TRA                         191
#define POFF_TAXABILITY                  197 // Newly added 4/5/2018
#define POFF_LANDUSE                     200
#define POFF_TAXYEAR                     204
#define POFF_ACRES                       208
#define POFF_AG_IND                      215
#define POFF_BEDS                        216
#define POFF_BATHS                       218
#define POFF_ROOMS                       221
#define POFF_LOTSIZE                     223
#define POFF_BLDGSQFT                    232
#define POFF_POOL                        238
#define POFF_YRBLT                       239
#define POFF_UNITS                       243
#define POFF_STORIES                     247
#define POFF_2ND_RES_IND                 250
#define POFF_AG_NONRENEWAL_FLG           251
#define POFF_AG_PRSV_STARTDATE           252
#define POFF_NONRENEWAL_DATE             260
#define POFF_FSZ_EFFDATE                 268
#define POFF_WATER_DIST                  276
#define POFF_ZONING                      278
#define POFF_S_STRNUM                    284
#define POFF_S_STRDIR                    289
#define POFF_S_STRNAME                   291
#define POFF_S_STRTYPE                   315
#define POFF_S_UNITNUM                   319
#define POFF_S_COMMUNITY                 323

#define PSIZ_APN                         12
#define PSIZ_OWNER                       41
#define PSIZ_ADDR1                       41
#define PSIZ_ADDR2                       30
#define PSIZ_ADDR3                       30
#define PSIZ_ADDR4                       30
#define PSIZ_SECTION                     2
#define PSIZ_TOWNSHIP                    2
#define PSIZ_RANGE                       2
#define PSIZ_TRA                         6
#define PSIZ_TAXABILITY                  3
#define PSIZ_LANDUSE                     4
#define PSIZ_TAXYEAR                     4
#define PSIZ_ACRES                       7
#define PSIZ_AG_IND                      1
#define PSIZ_BEDS                        2
#define PSIZ_BATHS                       3
#define PSIZ_ROOMS                       2
#define PSIZ_LOTSIZE                     9
#define PSIZ_BLDGSQFT                    6
#define PSIZ_POOL                        1
#define PSIZ_YRBLT                       4
#define PSIZ_UNITS                       4
#define PSIZ_STORIES                     3
#define PSIZ_2ND_RES_IND                 1
#define PSIZ_AG_NONRENEWAL_FLG           1
#define PSIZ_AG_PRSV_STARTDATE           8
#define PSIZ_NONRENEWAL_DATE             8
#define PSIZ_FSZ_EFFDATE                 8
#define PSIZ_WATER_DIST                  2
#define PSIZ_ZONING                      6
#define PSIZ_S_STRNUM                    5
#define PSIZ_S_STRDIR                    2
#define PSIZ_S_STRNAME                   24
#define PSIZ_S_STRTYPE                   4
#define PSIZ_S_UNITNUM                   4
#define PSIZ_S_COMMUNITY                 4

typedef struct _Kin_Public
{
   char Apn[PSIZ_APN];
   char Owner[PSIZ_OWNER];
   KINMAIL Mail;
   char Section[PSIZ_SECTION];
   char Township[PSIZ_TOWNSHIP];
   char Range[PSIZ_RANGE];
   char Tra[PSIZ_TRA];
   char Taxability[PSIZ_TAXABILITY];
   char UseCode[PSIZ_LANDUSE];
   char TaxYear[PSIZ_TAXYEAR];
   char Acres[PSIZ_ACRES];
   char Ag_Ind[PSIZ_AG_IND];
   char Beds[PSIZ_BEDS];
   char Baths[PSIZ_BATHS];
   char Rooms[PSIZ_ROOMS];
   char LotSize[PSIZ_LOTSIZE];
   char BldgSqft[PSIZ_BLDGSQFT];
   char Pool[PSIZ_POOL];
   char YrBlt[PSIZ_YRBLT];
   char Units[PSIZ_UNITS];
   char Stories[PSIZ_STORIES];
   char Sec_Res[PSIZ_2ND_RES_IND];
   char Ag_Nonrenewal[PSIZ_AG_NONRENEWAL_FLG];
   char Ag_StartDate[PSIZ_AG_PRSV_STARTDATE];
   char Ag_Nonrenewal_Date[PSIZ_NONRENEWAL_DATE];
   char EffDate[PSIZ_FSZ_EFFDATE];
   char WaterDist[PSIZ_WATER_DIST];
   char Zoning[PSIZ_ZONING];
   KINSITUS Situs;
   char CrLf[2];
} KIN_PUB;

// New layout
//#define POFF_APN                         1
//#define POFF_OWNER                       13
//#define POFF_ADDR1                       54
//#define POFF_ADDR2                       95
//#define POFF_ADDR3                       125
//#define POFF_ADDR4                       155
//#define POFF_SECTION                     185
//#define POFF_TOWNSHIP                    187
//#define POFF_RANGE                       189
//#define POFF_TRA                         191
//#define POFF_TAXABILITY                  197
//#define POFF_LANDUSE                     200
//#define POFF_TAXYEAR                     204
//#define POFF_ACRES                       204 
//#define POFF_AG_IND                      211 
//#define POFF_BEDS                        216
//#define POFF_BATHS                       218
//#define POFF_ROOMS                       221
//#define POFF_LOTSIZE                     223
//#define POFF_BLDGSQFT                    232
//#define POFF_POOL                        238
//#define POFF_YRBLT                       239
//#define POFF_UNITS                       243
//#define POFF_STORIES                     247
//#define POFF_2ND_RES_IND                 250
//#define POFF_AG_NONRENEWAL_FLG           251
//#define POFF_AG_PRSV_STARTDATE           252
//#define POFF_NONRENEWAL_DATE             260
//#define POFF_FSZ_EFFDATE                 268
//#define POFF_WATER_DIST                  276
//#define POFF_ZONING                      278
//#define POFF_S_STRNUM                    284
//#define POFF_S_STRDIR                    289
//#define POFF_S_STRNAME                   291
//#define POFF_S_STRTYPE                   315
//#define POFF_S_UNITNUM                   319
//#define POFF_S_COMMUNITY                 323

//typedef struct _Kin_Public
//{
//   char Apn[PSIZ_APN];
//   char Owner[PSIZ_OWNER];
//   KINMAIL Mail;
//   char Section[PSIZ_SECTION];
//   char Township[PSIZ_TOWNSHIP];
//   char Range[PSIZ_RANGE];
//   char Tra[PSIZ_TRA];
//   char Taxability[PSIZ_TAXABILITY];
//   char UseCode[PSIZ_LANDUSE];
//   char TaxYear[PSIZ_TAXYEAR];
//   char Acres[PSIZ_ACRES];
//   char Ag_Ind[PSIZ_AG_IND];
//   char Beds[PSIZ_BEDS];
//   char Baths[PSIZ_BATHS];
//   char Rooms[PSIZ_ROOMS];
//   char LotSize[PSIZ_LOTSIZE];
//   char BldgSqft[PSIZ_BLDGSQFT];
//   char Pool[PSIZ_POOL];
//   char YrBlt[PSIZ_YRBLT];
//   char Units[PSIZ_UNITS];
//   char Stories[PSIZ_STORIES];
//   char Sec_Res[PSIZ_2ND_RES_IND];
//   char Ag_Nonrenewal[PSIZ_AG_NONRENEWAL_FLG];
//   char Ag_StartDate[PSIZ_AG_PRSV_STARTDATE];
//   char Ag_Nonrenewal_Date[PSIZ_NONRENEWAL_DATE];
//   char EffDate[PSIZ_FSZ_EFFDATE];
//   char WaterDist[PSIZ_WATER_DIST];
//   char Zoning[PSIZ_ZONING];
//   KINSITUS Situs;
//   char CrLf[2];
//} KIN_PUB;

#define  TSIZ_FEE_PARCEL           12
#define  TSIZ_APN                  12
#define  TSIZ_TAXYEAR              4
#define  TSIZ_BOC_NO               5
#define  TSIZ_ROLL_TYPE            1
#define  TSIZ_BILL_TYPE            2
#define  TSIZ_STATUS               1
#define  TSIZ_TRA                  6
#define  TSIZ_CORTAC_INFO          34
#define  TSIZ_OWNER_INFO           214
#define  TSIZ_SITUS                64
#define  TSIZ_DELQ_DATE            8
#define  TSIZ_DELQ_NO              6
#define  TSIZ_NON_DECT_AMT         13
#define  TSIZ_ACRES                9
#define  TSIZ_LAND                 10
#define  TSIZ_FIXTR                10
#define  TSIZ_GROWIMPR             10
#define  TSIZ_IMPR                 10
#define  TSIZ_PPVAL                10
#define  TSIZ_BUSINV               10
#define  TSIZ_NET_AMT              10
#define  TSIZ_PP_PEN_FLG           1
#define  TSIZ_MH_FLG               1
#define  TSIZ_TAXABILITY           3
#define  TSIZ_INT_DUEDATE          8
#define  TSIZ_AG_PRE               1
#define  TSIZ_USECODE              4
#define  TSIZ_BUSCODE              2
#define  TSIZ_BOC_FLG              1
#define  TSIZ_EXE_INDEX            7
#define  TSIZ_EXE_CNT              3
#define  TSIZ_EXEAMT               10
#define  TSIZ_TAX_INDEX            7
#define  TSIZ_TAX_CNT              16
#define  TSIZ_TAXCODE              3
#define  TSIZ_TAXRATE              10
#define  TSIZ_TAXTYPE              1
#define  TSIZ_DUEDATE1             8
#define  TSIZ_TAXAMT1              13
#define  TSIZ_PENAMT1              11
#define  TSIZ_PENCODE1             1
#define  TSIZ_INST1_COLL_NO        10
#define  TSIZ_INST1_COLL_DATE      8
#define  TSIZ_CO_1ST_PAID_FLG      1
#define  TSIZ_CO_REC_FLG           1
#define  TSIZ_DUEDATE2             8
#define  TSIZ_TAXAMT2              13
#define  TSIZ_PENAMT2              11
#define  TSIZ_DELQ_COST            9
#define  TSIZ_PENCODE2             1
#define  TSIZ_INST2_COLL_NO        10
#define  TSIZ_INST2_COLL_DATE      8
#define  TSIZ_REFUND_AMT           13
#define  TSIZ_REFUND_XREF          5
#define  TSIZ_MISC_COST            11
#define  TSIZ_ROLLCHG_INDEX        7
#define  TSIZ_ROLLCHG_CNT          8
#define  TSIZ_ROLLCHG_NO           5
#define  TSIZ_ROLLCHG_DATE         8
#define  TSIZ_ROLLCHG_PERSON       4
#define  TSIZ_RT_CODE_1            7
#define  TSIZ_RT_DESC              17
#define  TSIZ_ACTION_TYPE          2
#define  TSIZ_ROLLCHG_TYPE         2
#define  TSIZ_ROLLCHG_XREF         7
#define  TSIZ_TRANS_DATE1          8
#define  TSIZ_TRANS_DATE2          8
#define  TSIZ_DELQ_NOTICE_BP_FLG   1
#define  TSIZ_TEETER_FLG           1
#define  TSIZ_PAIDAMT1             13
#define  TSIZ_PAIDAMT2             13
#define  TSIZ_CANCEL_DATE1         8
#define  TSIZ_CANCEL_DATE2         8
#define  TSIZ_APPORT_YEAR          4
#define  TSIZ_VENDOR_NO            5
#define  TSIZ_VENDOR_SUFF          1
#define  TSIZ_VENDOR_NAME          41
#define  TSIZ_VENDOR_ADR1          35
#define  TSIZ_VENDOR_ADR2          35
#define  TSIZ_VENDOR_ADR3          35
#define  TSIZ_WARRANT_NO           7
#define  TSIZ_REFUND_DATE          8
#define  TSIZ_SB813_SUPL_BILLS     3
#define  TSIZ_SB813_EVENT_DATE     8
#define  TSIZ_SB813_PRO_FACTOR     8
#define  TSIZ_PRORATION_PCT        8
#define  TSIZ_DAYS_OWN             3
#define  TSIZ_OWN_PERIOD           3
#define  TSIZ_FILLER1              3
#define  TSIZ_INSTRUMENT_NO        10
#define  TSIZ_LIEN_DATE            8
#define  TSIZ_LIEN_REC_NO          9
#define  TSIZ_RELEASE_REC_NO       9
#define  TSIZ_LIEN_RELEASE_DATE    8
#define  TSIZ_INTENT_DATE          8
#define  TSIZ_JUDGEMENT_DATE       8
#define  TSIZ_JUDGEMENT_REC_NO     9
#define  TSIZ_SATISFACTION_REC_NO  9
#define  TSIZ_SATISFY_DATE         8
#define  TSIZ_FEE_ASMT             12
#define  TSIZ_ORIG_ASMT            12
#define  TSIZ_JUDGEMENT_NO         11
#define  TSIZ_FILLER2              7
#define  TSIZ_ALT_DEPT_TYPE        1
#define  TSIZ_ALT_STATUS           1
#define  TSIZ_ALT_KEY              16
#define  TSIZ_UFO_USE              8
#define  TSIZ_REFUND_AUTH_DATE     8
#define  TSIZ_ORIGINAL_DUE_DATE2   8
#define  TSIZ_COURT                4
#define  TSIZ_BANKRUPTCY_NO        12
#define  TSIZ_BANKRUPTCY_DATE      8
#define  TSIZ_BANKRUPTCY_TYPE      2
#define  TSIZ_SELL_NOTICE_DATE     8
#define  TSIZ_SELL_DOC_NO          9
#define  TSIZ_SELL_NOTICE_REC_NO   9
#define  TSIZ_RECISION_DATE        8
#define  TSIZ_RECISION_DOC_NO      9
#define  TSIZ_RECISION_REC_NO      9
#define  TSIZ_XREF_APN_NO          12
#define  TSIZ_CURRENT_DEED_DATE    8
#define  TSIZ_CURRENT_DEED_DOC_NO  14
#define  TSIZ_BKRTCY_REMOVED_DATE  8
#define  TSIZ_ZIP_CODE_SORT        10
#define  TSIZ_TAX_BILL_DATE        8
#define  TSIZ_APPORT_INSTAL1_DATE  8
#define  TSIZ_APPORT_INSTAL2_DATE  8
#define  TSIZ_STRUCT_SITUS_LINE    44
#define  TSIZ_UFO_DUMMY            1
#define  TSIZ_FISCAL_YEAR          4
#define  TSIZ_REDEEMED_FLG         1
#define  TSIZ_RDMPT_PEN_AMT        10
#define  TSIZ_MONTHLY_PEN_RATE     6
#define  TSIZ_APPORT_FLG           1
#define  TSIZ_SALE_AMT             10
#define  TSIZ_RDMPT_FEE            6
#define  TSIZ_RECORDING_FEES       6
#define  TSIZ_ADDITIONAL_FEES      6
#define  TSIZ_PAYEE_NAME           41
#define  TSIZ_PAYEE_STREET         35
#define  TSIZ_PAYEE_CITY           20
#define  TSIZ_PAYEE_STATE          2
#define  TSIZ_PAYEE_ZIP            5
#define  TSIZ_PAYEE_ZIP4           4
#define  TSIZ_LAST_DELQ_ORIG_NO    12
#define  TSIZ_PAY_PLAN_ACCOUNT_NO  7
#define  TSIZ_DFLT_DATE            8
#define  TSIZ_DFLT_CREDIT_AMT      10
#define  TSIZ_YEAR_5TH_DFLT_FLG    1
#define  TSIZ_COLL_OCCR_FLG        1
#define  TSIZ_RDMPT_CERT_FLG       1
#define  TSIZ_FULL_RDMPT_AMOUNT    10
#define  TSIZ_INT_PAID             10
#define  TSIZ_DFLT_INT             10
#define  TSIZ_PAYMNT_TRANS_DATE    8
#define  TSIZ_PAY_PLAN_STUP_FEES   11
#define  TSIZ_UFO_DELETE_FLG       1

#define  TOFF_FEE_PARCEL           1
#define  TOFF_APN                  13
#define  TOFF_TAXYEAR              25
#define  TOFF_BOC_NO               29
#define  TOFF_ROLL_TYPE            34
#define  TOFF_BILL_TYPE            35
#define  TOFF_STATUS               37
#define  TOFF_TRA                  38
#define  TOFF_CORTAC_INFO          44
#define  TOFF_OWNER_INFO           78
#define  TOFF_SITUS                292
#define  TOFF_DELQ_DATE            356
#define  TOFF_DELQ_NO              364
#define  TOFF_NON_DECT_AMT         370
#define  TOFF_DATE_RED_FROM_ST     383
#define  TOFF_ACRES                391
#define  TOFF_LAND                 400
#define  TOFF_FIXTR                410
#define  TOFF_GROWIMPR             420
#define  TOFF_IMPR                 430
#define  TOFF_PPVAL                440
#define  TOFF_BUSINV               450
#define  TOFF_NET_AMT              460
#define  TOFF_PP_PEN_FLG           470
#define  TOFF_MH_FLG               471      // Y=MH PRESENT
#define  TOFF_TAXABILITY           472
#define  TOFF_INT_DUEDATE          475
#define  TOFF_AG_PRE               483
#define  TOFF_USECODE              484
#define  TOFF_BUSCODE              488
#define  TOFF_BOC_FLG              490      // REDEMPTION
#define  TOFF_EXE_INDEX            491
#define  TOFF_EXE_ARRAY            498      // MAX 3
#define  TOFF_TAX_INDEX            531
#define  TOFF_TAX_ARRAY            538      // MAX 16
#define  TOFF_DUEDATE1             1530
#define  TOFF_TAXAMT1              1538
#define  TOFF_PENAMT1              1551
#define  TOFF_PENCODE1             1562
#define  TOFF_INST1_COLL_NO        1563     // CAN=PAYMENT CANCELED
#define  TOFF_INST1_COLL_DATE      1573
#define  TOFF_CO_1ST_PAID_FLG      1581     // Y=1ST PAID, DON'T CARRY OVER. N=CARRY OVER TO NEXT ROLL
#define  TOFF_CO_REC_FLG           1582
#define  TOFF_DUEDATE2             1583
#define  TOFF_TAXAMT2              1591
#define  TOFF_PENAMT2              1604
#define  TOFF_DELQ_COST            1615
#define  TOFF_PENCODE2             1624
#define  TOFF_INST2_COLL_NO        1625
#define  TOFF_INST2_COLL_DATE      1635
#define  TOFF_REFUND_AMT           1643
#define  TOFF_REFUND_XREF          1656     // BOC OR COLL
#define  TOFF_MISC_COST            1661
#define  TOFF_ROLLCHG_INDEX        1672
#define  TOFF_ROLLCHG_ARRAY        1679     // MAX 8
#define  TOFF_TRANS_DATE1          2095
#define  TOFF_TRANS_DATE2          2103
#define  TOFF_DELQ_NOTICE_BP_FLG   2111     // B
#define  TOFF_TEETER_FLG           2112     // Y=TEETER
#define  TOFF_PAIDAMT1             2113
#define  TOFF_PAIDAMT2             2126
#define  TOFF_CANCEL_DATE1         2139
#define  TOFF_CANCEL_DATE2         2147
#define  TOFF_APPORT_YEAR          2155
#define  TOFF_VENDOR_NO            2159
#define  TOFF_VENDOR_SUFF          2164
#define  TOFF_VENDOR_NAME          2165
#define  TOFF_VENDOR_ADR1          2206
#define  TOFF_VENDOR_ADR2          2241
#define  TOFF_VENDOR_ADR3          2276
#define  TOFF_WARRANT_NO           2311     // FOR REFUND
#define  TOFF_REFUND_DATE          2318
#define  TOFF_SB813_SUPL_BILLS     2326     // BILL COUNT
#define  TOFF_SB813_EVENT_DATE     2329
#define  TOFF_SB813_PRO_FACTOR     2337
#define  TOFF_PRORATION_PCT        2345
#define  TOFF_DAYS_OWN             2353
#define  TOFF_OWN_PERIOD           2356
#define  TOFF_FILLER1              2359
#define  TOFF_INSTRUMENT_NO        2362
#define  TOFF_LIEN_DATE            2372
#define  TOFF_LIEN_REC_NO          2380
#define  TOFF_RELEASE_REC_NO       2389
#define  TOFF_LIEN_RELEASE_DATE    2398
#define  TOFF_INTENT_DATE          2406
#define  TOFF_JUDGEMENT_DATE       2414
#define  TOFF_JUDGEMENT_REC_NO     2422
#define  TOFF_SATISFACTION_REC_NO  2431
#define  TOFF_SATISFY_DATE         2440
#define  TOFF_FEE_ASMT             2448
#define  TOFF_ORIG_ASMT            2460
#define  TOFF_JUDGEMENT_NO         2472
#define  TOFF_FILLER2              2483
#define  TOFF_ALT_DEPT_TYPE        2490
#define  TOFF_ALT_STATUS           2491
#define  TOFF_ALT_KEY              2492
#define  TOFF_UFO_USE              2508
#define  TOFF_REFUND_AUTH_DATE     2516
#define  TOFF_ORIGINAL_DUE_DATE2   2524
#define  TOFF_COURT                2532
#define  TOFF_BANKRUPTCY_NO        2536
#define  TOFF_BANKRUPTCY_DATE      2548
#define  TOFF_BANKRUPTCY_TYPE      2556
#define  TOFF_SELL_NOTICE_DATE     2558
#define  TOFF_SELL_DOC_NO          2566
#define  TOFF_SELL_NOTICE_REC_NO   2575
#define  TOFF_RECISION_DATE        2584
#define  TOFF_RECISION_DOC_NO      2592
#define  TOFF_RECISION_REC_NO      2601
#define  TOFF_XREF_APN_NO          2610
#define  TOFF_CURRENT_DEED_DATE    2622
#define  TOFF_CURRENT_DEED_DOC_NO  2630
#define  TOFF_BKRTCY_REMOVED_DATE  2644
#define  TOFF_ZIP_CODE_SORT        2652
#define  TOFF_TAX_BILL_DATE        2662
#define  TOFF_APPORT_INSTAL1_DATE  2670
#define  TOFF_APPORT_INSTAL2_DATE  2678
#define  TOFF_STRUCT_SITUS_LINE    2686
#define  TOFF_UFO_DUMMY            2730
#define  TOFF_FISCAL_YEAR          2731
#define  TOFF_REDEEMED_FLG         2735
#define  TOFF_RDMPT_PEN_AMT        2736
#define  TOFF_MONTHLY_PEN_RATE     2746
#define  TOFF_APPORT_FLG           2752
#define  TOFF_SALE_AMT             2753
#define  TOFF_RDMPT_FEE            2763
#define  TOFF_RECORDING_FEES       2769
#define  TOFF_ADDITIONAL_FEES      2775
#define  TOFF_PAYEE_NAME           2781
#define  TOFF_PAYEE_STREET         2822
#define  TOFF_PAYEE_CITY           2857
#define  TOFF_PAYEE_STATE          2877
#define  TOFF_PAYEE_ZIP            2879
#define  TOFF_PAYEE_ZIP4           2884
#define  TOFF_LAST_DELQ_ORIG_NO    2888
#define  TOFF_PAY_PLAN_ACCOUNT_NO  2900
#define  TOFF_DFLT_DATE            2907
#define  TOFF_DFLT_CREDIT_AMT      2915
#define  TOFF_YEAR_5TH_DFLT_FLG    2925
#define  TOFF_COLL_OCCR_FLG        2926
#define  TOFF_RDMPT_CERT_FLG       2927
#define  TOFF_FULL_RDMPT_AMOUNT    2928
#define  TOFF_INT_PAID             2938
#define  TOFF_DFLT_INT             2948
#define  TOFF_PAYMNT_TRANS_DATE    2958
#define  TOFF_PAY_PLAN_STUP_FEES   2966
#define  TOFF_UFO_DELETE_FLG       2977

typedef struct t_ExeRdmptInfo
{
   char  Code;
   char  Amt[TSIZ_EXEAMT];
} KIN_REXE;
typedef struct t_TaxRdmptInfo
{
   char  TaxCode[TSIZ_TAXCODE];
   char  TaxRate[TSIZ_TAXRATE];
   char  TaxType[TSIZ_TAXTYPE];
   char  InstAmt1[TSIZ_TAXAMT1];
   char  PenAmt1[TSIZ_PENAMT1];
   char  InstAmt2[TSIZ_TAXAMT2];
   char  PenAmt2[TSIZ_PENAMT2];
} KIN_RTAX;
typedef struct t_RollChgInfo
{
   char  Rollchg_No     [TSIZ_ROLLCHG_NO    ];
   char  Rollchg_Date   [TSIZ_ROLLCHG_DATE  ];
   char  Rollchg_Person [TSIZ_ROLLCHG_PERSON];
   char  Rt_Code_1      [TSIZ_RT_CODE_1     ];
   char  Rt_Desc        [TSIZ_RT_DESC       ];
   char  Action_Type    [TSIZ_ACTION_TYPE   ];
   char  Rollchg_Type   [TSIZ_ROLLCHG_TYPE  ];
   char  Rollchg_Xref   [TSIZ_ROLLCHG_XREF  ];
} KIN_RC;

typedef struct _tF02052T
{
   char  Fee_Parcel          [TSIZ_FEE_PARCEL         ];
   char  Apn                 [TSIZ_APN                ];
   char  Taxyear             [TSIZ_TAXYEAR            ];
   char  Boc_No              [TSIZ_BOC_NO             ];
   char  Roll_Type           [TSIZ_ROLL_TYPE          ];
   // *  ASSESSMENT ROLL         'A'
   // *  CURR SUPL SECURED       'C','D'
   // *  CURR SUPL UNSECURED     'Q','R','V','W'
   // *  CURR UNSECURED          'U'
   // *  CURR UNSECURED PLANE    'T'
   // *  CURR SECURED            'S'
   // *  PRIOR SUPL SECURED      'E','F'
   // *  PRIOR SUPL UNSECURED    'G','H','X','J'
   // *  PRIOR UNSECURED         'Y'
   // *  PRIOR UNSECURED PLANE   'Z'
   // *  PRIOR SECURED           'I'
   // *  PRIOR PAY PLAN CODE     '8'
   // *  NON VALID COLLECTION    '9'
   char  Bill_Type           [TSIZ_BILL_TYPE          ];
   // *  BR  =  BOARD ORDER REFUND BILL
   // *  CB  =  CORRECTED BILL
   // *  CR  =  COLLECTED REFUND BILL
   // *  NB  &  SP = NEW BILL
   // *  PR =  PRIOR REFUND BILL
   // *  SB =  SUPPLEMENTAL BILL
   char  Status              [TSIZ_STATUS             ];
   // *  I  =  INACTIVE
   // *  A  =  ACTIVE
   // *  P  =  PENDING
   // *  N  =  NOT PROCESS
   // *  C  =  COMPLETED
   // *  D  =  DUPLICATE BILL
   char  TRA                 [TSIZ_TRA                ];
   char  Cortac_Info         [TSIZ_CORTAC_INFO        ];
   char  Owner_Info          [TSIZ_OWNER_INFO         ];
   char  Situs               [TSIZ_SITUS              ];
   char  Delq_Date           [TSIZ_DELQ_DATE          ];
   char  Delq_No             [TSIZ_DELQ_NO            ];
   char  Non_Dect_Amt        [TSIZ_NON_DECT_AMT       ];
   char  Redeemed_Date       [TSIZ_DELQ_DATE          ];
   char  Acres               [TSIZ_ACRES              ];
   char  Land                [TSIZ_LAND               ];
   char  Fixtr               [TSIZ_FIXTR              ];
   char  GrowImpr            [TSIZ_GROWIMPR           ];
   char  Impr                [TSIZ_IMPR               ];
   char  PPVal               [TSIZ_PPVAL              ];
   char  BusInv              [TSIZ_BUSINV             ];
   char  Net_Amt             [TSIZ_NET_AMT            ];
   char  PP_Pen_Flg          [TSIZ_PP_PEN_FLG         ];
   char  MH_Flg              [TSIZ_MH_FLG             ];   // Y=MH Present
   char  Taxability          [TSIZ_TAXABILITY         ];
   char  Int_DueDate         [TSIZ_INT_DUEDATE        ];
   char  Ag_Pre              [TSIZ_AG_PRE             ];
   char  UseCode             [TSIZ_USECODE            ];
   char  BusCode             [TSIZ_BUSCODE            ];
   char  Boc_Flg             [TSIZ_BOC_FLG            ];   // Redemption
   char  Exe_Index           [TSIZ_EXE_INDEX          ];
   KIN_REXE Exe_Info         [TSIZ_EXE_CNT            ];   // Max 3
   char  Tax_Index           [TSIZ_TAX_INDEX          ];
   KIN_RTAX Tax_Info         [TSIZ_TAX_CNT            ];   // Max 16
   char  DueDate1            [TSIZ_DUEDATE1           ];
   char  TaxAmt1             [TSIZ_TAXAMT1            ];
   char  PenAmt1             [TSIZ_PENAMT1            ];
   char  PenCode1            [TSIZ_PENCODE1           ];
   char  Inst1_Coll_No       [TSIZ_INST1_COLL_NO      ];   // Can=Payment Canceled
   char  Inst1_Coll_Date     [TSIZ_INST1_COLL_DATE    ];
   char  Co_1St_Paid_Flg     [TSIZ_CO_1ST_PAID_FLG    ];   // Y=1St Paid, Don't Carry Over. N=Carry Over To Next Roll
   char  Co_Rec_Flg          [TSIZ_CO_REC_FLG         ];
   char  DueDate2            [TSIZ_DUEDATE2           ];
   char  TaxAmt2             [TSIZ_TAXAMT2            ];
   char  PenAmt2             [TSIZ_PENAMT2            ];
   char  Delq_Cost           [TSIZ_DELQ_COST          ];
   char  PenCode2            [TSIZ_PENCODE2           ];
   char  Inst2_Coll_No       [TSIZ_INST2_COLL_NO      ];
   char  Inst2_Coll_Date     [TSIZ_INST2_COLL_DATE    ];
   char  Refund_Amt          [TSIZ_REFUND_AMT         ];
   char  Refund_Xref         [TSIZ_REFUND_XREF        ];   // Boc Or Coll
   char  Misc_Cost           [TSIZ_MISC_COST          ];
   char  Rollchg_Index       [TSIZ_ROLLCHG_INDEX      ];
   KIN_RC Rollchg_Info       [TSIZ_ROLLCHG_CNT        ];   // Max 8
   char  Trans_Date1         [TSIZ_TRANS_DATE1        ];
   char  Trans_Date2         [TSIZ_TRANS_DATE2        ];
   char  Delq_Notice_Bp_Flg  [TSIZ_DELQ_NOTICE_BP_FLG ];   // B
   char  Teeter_Flg          [TSIZ_TEETER_FLG         ];   // Y=Teeter
   char  PaidAmt1            [TSIZ_PAIDAMT1           ];
   char  PaidAmt2            [TSIZ_PAIDAMT2           ];
   char  Cancel_Date1        [TSIZ_CANCEL_DATE1       ];
   char  Cancel_Date2        [TSIZ_CANCEL_DATE2       ];
   char  Apport_Year         [TSIZ_APPORT_YEAR        ];
   char  Vendor_No           [TSIZ_VENDOR_NO          ];
   char  Vendor_Suff         [TSIZ_VENDOR_SUFF        ];
   char  Vendor_Name         [TSIZ_VENDOR_NAME        ];
   char  Vendor_Adr1         [TSIZ_VENDOR_ADR1        ];
   char  Vendor_Adr2         [TSIZ_VENDOR_ADR2        ];
   char  Vendor_Adr3         [TSIZ_VENDOR_ADR3        ];
   char  Warrant_No          [TSIZ_WARRANT_NO         ];   // For Refund
   char  Refund_Date         [TSIZ_REFUND_DATE        ];
   char  Sb813_Supl_Bills    [TSIZ_SB813_SUPL_BILLS   ];   // Bill Count
   char  Sb813_Event_Date    [TSIZ_SB813_EVENT_DATE   ];
   char  Sb813_Pro_Factor    [TSIZ_SB813_PRO_FACTOR   ];
   char  Proration_Pct       [TSIZ_PRORATION_PCT      ];
   char  Days_Own            [TSIZ_DAYS_OWN           ];
   char  Own_Period          [TSIZ_OWN_PERIOD         ];
   char  Filler1             [TSIZ_FILLER1            ];
   char  Instrument_No       [TSIZ_INSTRUMENT_NO      ];
   char  Lien_Date           [TSIZ_LIEN_DATE          ];
   char  Lien_Rec_No         [TSIZ_LIEN_REC_NO        ];
   char  Release_Rec_No      [TSIZ_RELEASE_REC_NO     ];
   char  Lien_Release_Date   [TSIZ_LIEN_RELEASE_DATE  ];
   char  Intent_Date         [TSIZ_INTENT_DATE        ];
   char  Judgement_Date      [TSIZ_JUDGEMENT_DATE     ];
   char  Judgement_Rec_No    [TSIZ_JUDGEMENT_REC_NO   ];
   char  Satisfaction_Rec_No [TSIZ_SATISFACTION_REC_NO];
   char  Satisfy_Date        [TSIZ_SATISFY_DATE       ];
   char  Fee_Asmt            [TSIZ_FEE_ASMT           ];
   char  Orig_Asmt           [TSIZ_ORIG_ASMT          ];
   char  Judgement_No        [TSIZ_JUDGEMENT_NO       ];
   char  Filler2             [TSIZ_FILLER2            ];
   char  Alt_Dept_Type       [TSIZ_ALT_DEPT_TYPE      ];
   char  Alt_Status          [TSIZ_ALT_STATUS         ];
   char  Alt_Key             [TSIZ_ALT_KEY            ];
   char  Ufo_Use             [TSIZ_UFO_USE            ];
   char  Refund_Auth_Date    [TSIZ_REFUND_AUTH_DATE   ];
   char  Original_DueDate2   [TSIZ_ORIGINAL_DUE_DATE2 ];
   char  Court               [TSIZ_COURT              ];
   char  Bankruptcy_No       [TSIZ_BANKRUPTCY_NO      ];
   char  Bankruptcy_Date     [TSIZ_BANKRUPTCY_DATE    ];
   char  Bankruptcy_Type     [TSIZ_BANKRUPTCY_TYPE    ];
   char  Sell_Notice_Date    [TSIZ_SELL_NOTICE_DATE   ];
   char  Sell_Doc_No         [TSIZ_SELL_DOC_NO        ];
   char  Sell_Notice_Rec_No  [TSIZ_SELL_NOTICE_REC_NO ];
   char  Recision_Date       [TSIZ_RECISION_DATE      ];
   char  Recision_Doc_No     [TSIZ_RECISION_DOC_NO    ];
   char  Recision_Rec_No     [TSIZ_RECISION_REC_NO    ];
   char  Xref_Apn_No         [TSIZ_XREF_APN_NO        ];
   char  Current_Deed_Date   [TSIZ_CURRENT_DEED_DATE  ];
   char  Current_Deed_Doc_No [TSIZ_CURRENT_DEED_DOC_NO];
   char  Bkrtcy_Removed_Date [TSIZ_BKRTCY_REMOVED_DATE];
   char  Zip_Code_Sort       [TSIZ_ZIP_CODE_SORT      ];
   char  Tax_Bill_Date       [TSIZ_TAX_BILL_DATE      ];
   char  Apport_Inst1_Date   [TSIZ_APPORT_INSTAL1_DATE];
   char  Apport_Inst2_Date   [TSIZ_APPORT_INSTAL2_DATE];
   char  Struct_Situs_Line   [TSIZ_STRUCT_SITUS_LINE  ];
   char  Ufo_Dummy           [TSIZ_UFO_DUMMY          ];
   char  Fiscal_Year         [TSIZ_FISCAL_YEAR        ];
   char  Redeemed_Flg        [TSIZ_REDEEMED_FLG       ];
   char  Rdmpt_Pen_Amt       [TSIZ_RDMPT_PEN_AMT      ];
   char  Monthly_Pen_Rate    [TSIZ_MONTHLY_PEN_RATE   ];
   char  Apport_Flg          [TSIZ_APPORT_FLG         ];
   char  Sale_Amt            [TSIZ_SALE_AMT           ];
   char  Rdmpt_Fee           [TSIZ_RDMPT_FEE          ];
   char  Recording_Fees      [TSIZ_RECORDING_FEES     ];
   char  Additional_Fees     [TSIZ_ADDITIONAL_FEES    ];
   char  Payee_Name          [TSIZ_PAYEE_NAME         ];
   char  Payee_Street        [TSIZ_PAYEE_STREET       ];
   char  Payee_City          [TSIZ_PAYEE_CITY         ];
   char  Payee_State         [TSIZ_PAYEE_STATE        ];
   char  Payee_Zip           [TSIZ_PAYEE_ZIP          ];
   char  Payee_Zip4          [TSIZ_PAYEE_ZIP4         ];
   char  Last_Delq_Orig_No   [TSIZ_LAST_DELQ_ORIG_NO  ];
   char  Pay_Plan_Account_No [TSIZ_PAY_PLAN_ACCOUNT_NO];
   char  Dflt_Date           [TSIZ_DFLT_DATE          ];
   char  Dflt_Credit_Amt     [TSIZ_DFLT_CREDIT_AMT    ];
   char  Year_5Th_Dflt_Flg   [TSIZ_YEAR_5TH_DFLT_FLG  ];
   char  Coll_Occr_Flg       [TSIZ_COLL_OCCR_FLG      ];
   char  Rdmpt_Cert_Flg      [TSIZ_RDMPT_CERT_FLG     ];
   char  Full_Rdmpt_Amount   [TSIZ_FULL_RDMPT_AMOUNT  ];
   char  Int_Paid            [TSIZ_INT_PAID           ];
   char  Dflt_Int            [TSIZ_DFLT_INT           ];
   char  Paymnt_Trans_Date   [TSIZ_PAYMNT_TRANS_DATE  ];
   char  Pay_Plan_Stup_Fees  [TSIZ_PAY_PLAN_STUP_FEES ];
   char  Ufo_Delete_Flg      [TSIZ_UFO_DELETE_FLG     ];
} KIN_RDMPT;

#endif

