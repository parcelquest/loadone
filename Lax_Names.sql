if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Lax_Names]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[Lax_Names]
GO

CREATE TABLE [dbo].[Lax_Names] (
	[DocNum] [varchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[SeqNum] [int] NULL ,
	[NameType] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[Name] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL 
) ON [PRIMARY]
GO

 CREATE  INDEX [IX_Lax_Names] ON [dbo].[Lax_Names]([DocNum]) ON [PRIMARY]
GO

