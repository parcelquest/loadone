/*******************************************************************************
 *
 * Data avail in sec roll file:
 *    - TRACT, BLOCK, LOT, CONDO BLDG, CONDO UNIT, PREV APN & VOID YEAR, 
 *      NON-TAX CODE, MH INFO, BASEMENT AREA, ROOM INFO, 
 *    - There is no DocTax or SalePrice in update file
 *    - Update files are not cumulative.  So we have to keep all of them.
 *
 * Input files: Notes that LDR file name might be changed every year.  Watch out!!!
 *    - 2022 Secured Parcel Data.csv (Lien file, it may change every year)
 *    - ASR_SEC_PUBINFO              (Update file, 302-byte ASCII with CRLF)
 *      We receive this monthly and it contains only new/updated record.s.
 *
 * Operation commands:
 *    -L : to load lien
 *    -U : to load update roll
 *    -G : to process GrGr and create sale file for MergeAdr
 *    -Xc: extract sale from R01 file
 *    -Xl: extract lien values
 *    -Uu: special load to update standard usecode on R01
 *    -Us: update sale file using roll file
 *
 * Load lien : -O -CVEN -L -Xl -Mg -Ms
 * Reg update: -O -CVEN -U -G -Xs
 *
 * 08/16/2007 1.4.27    Adding CSpreadSheet.h to MergeVen.cpp for GrGr processing.
 * 08/19/2007 1.4.27.1  Add Ven_ExtrLien() and reorganize the sale update process.
 *                      Main source of sale update now is GrGr.
 * 12/20/2007 1.5.1     Rework on GRGR load/update.
 * 03/21/2008 1.5.7.1   Use standard function to update usecode.
 * 03/25/2008 1.5.8     Adding option -Uu to update standard usecode
 * 03/31/2008 1.5.9     Fix Ven_LoadGrGrXls() because file layout has been changed.
 * 05/09/2008 1.5.10.1  Creating Ven_LoadGrGrTxt() and modifying Ven_LoadGrGr() to
 *                      support new format again.  We are now received comma delimited
 *                      file.  DocNum is only 8 bytes prefix with DocDate.  Ignore 
 *                      last '0'.  It is now more data but also more confusion.
 * 06/20/2008 1.6.6     Keep original APN in ALT_APN field.
 * 11/19/2008 8.4.5     Fix Ven_LoadGrGrTxt()
 * 12/11/2008 8.5.1     Make StrNum left justified
 * 01/17/2009 8.5.6     Fix TRA
 * 01/20/2009 8.5.6.1   Check for wide-char data in GRGR file.
 * 03/17/2009 8.7.1     Format STORIES as 99.9.  We no longer multiply by 10.
 * 05/11/2009 8.8       Set full exemption flag
 * 07/02/2009 9.1.1     Modify Ven_MergeRoll() & Ven_ExtrLien() to correctly capture
 *                      other values.  Add option to merge GrGr.
 * 07/02/2010 10.0.1    Rename Ven_LoadLien() to Ven_Load_LDR().  Adding Exemption code
 *                      to R01 file and log number of LDR records and dropped count.
 * 07/14/2010 10.1.3    Fix LEGAL DESC in Ven_MergeRoll().
 * 01/18/2011 10.4.1.1  Fix Ven_CombineGrGr() when number of Grantor/Grantee greater MAX_NAMES.
 *                      This causes system crashed in release mode.
 * 05/18/2011 10.5.12   Fix Ven_LoadGrGr() to return 0 if success.
 * 07/05/2011 11.0.1    Add S_HSENO.  Found 200 HSENO with '-'.
 * 11/03/2011 11.4.14   Modify Ven_LoadGrGr() to check more than one location for input file.
 * 01/12/2012 11.6.3    Modify Ven_LoadGrGrTxt() to add log msg.  Add option to convert GRGR to SCSAL_REC
 *                      Modify GrGr output to use SCSAL_REC format instead of GRGR_DEF
 *                      Convert old VEN_Grgr.sls to SCSAL_REC format.
 *                      Auto import Grgr data into SQL
 * 01/26/2012 11.6.4    Add more error check loading GrGr. Standardise DocNum to 7 digits with 2 digits year.
 *                      VEN_GRGR.SLS is now in SCSAL_REC, no more GRGR_DEF.
 *                      Fix bug in Ven_MergeUpdt() and Ven_MergeRoll() by increasing size for acCode[].
 *                      Use ApplyCumSale to merge GRGR data in R01 file.
 * 02/09/2012 11.6.5    Update SalePrice using new DocTax field from VENTURA_PUBINFO.TXT.
 *                      Export sale info to VEN_Sale.sls.  Using two different DeefXref files
 *                      for Sale & GrGr.  Convert cum sale to SCSAL_REC format.
 * 04/04/2012 11.9.0    Remove Ven_MergeRollSale() from Ven_MergeUpdt().  Remove sale update 
 *                      from Ven_Load_LDR().  Add Ven_FixCumSale() to fix up sale file.
 *                      Turn ON sort option when calling ApplyCumSale to update GRGR.
 * 05/02/2012 11.9.3    Fix mail zip bug in Ven_MergeUpdt().
 * 06/04/2012 11.9.9    Call createSaleImport() with type 2 for GrGr import.
 * 01/02/2013 12.4.0.1  Check error on GrGr import to SQL.
 * 09/11/2013 13.7.14   Add Bsmt, Floors, Porch, Deck, and Patio sqft
 * 10/01/2013 13.9.0    Rewrite Ven_MergeOwner() to populate Vesting.
 * 10/07/2013 13.10.3   Add 1/4 & 3/4 baths
 * 10/11/2013 13.11.0   Adding 1-4 Qbaths to R01
 * 02/14/2014 13.12.0   Add VEN_DocCode[].  Add -Xs option and Ven_ExtrSale() to replace Ven_UpdateCumSale().
 *                      Fix APN in cum sale to match with R01's APN.
 * 03/13/2014 13.12.3   Rename update file if load successful.
 * 05/07/2014 13.14.2   Modify Ven_CleanName() to fix some owner name that was chopped off.
 * 05/23/2014 13.14.5   New update file now adding last DocTax and SaleDate. If SaleDate is the same as 
 *                      current RecDate, create a sale record with everything. If not, create new sale
 *                      record with SaleDate and DocTax then later match with GRGR data to get DocNum.
 * 06/23/2014 13.14.8   Add NoneSale_Flg to DeedRef table so only actual sales are added to PQ products.
 * 07/10/2015 15.0.2    Modify Ven_UpdateDocNum() to set Spc_Flg=3 after update DocNum.
 *                      Change sale update logic by using GRGR_UPD_GD option to update sale using GD from GrGr.
 * 01/24/2017 16.9.8    Add function to load tax files.
 * 02/01/2017 16.10.2   Add MergeTaxPaid() to update tax base table.
 * 02/02/2017 16.10.2.1 Fine tune Ven_Load_TaxBase() and MergeTaxPaid() 
 * 02/04/2017 16.10.3   Add Ven_MergeTaxRdmpt() to update delq/redeem.
 * 02/07/2017 16.10.5   Add Ven_Load_TaxSup(), need more testing
 * 03/04/2017 16.11.0   Modify Ven_ParseTaxDetail() to add TC_Flag
 * 03/13/2017 16.12.0   Modify Ven_MergeTaxPaid() to update instStatus.
 * 03/20/2017 16.12.5.1 Modify Ven_ParseTaxBase() to populate PenAmt only if past due date.
 *                      Modify Ven_MergeTaxPaid() to clear PenAmt when paid.
 *                      Add Ven_MergeTaxDue() to update corrected bill.
 * 01/14/2018 17.6.1    Finetuning MergeTaxDue() & MergeTaxPaid().
 * 02/05/2018 17.6.5    Modify Ven_MergeRoll() to update MultiApn flag when loading LDR.
 * 04/30/2020 19.9.3    When GrGr data not available, return immediately and email is -E specified.
 * 07/18/2020 20.1.6    Modify Ven_MergeRoll() to verify direction before assign to S_DIR.
 * 10/09/2020 20.2.14   Modify Ven_ParseTaxBase() to support new record layout of P8102-01.
 * 04/19/2021 20.7.15   Format tax code as 9999 instead of 99-99 in order to search in PQW.
 * 05/07/2021 20.7.18   Set FirePlace='M' when greater than 9 in Ven_MergeRoll().
 * 02/15/2022 21.4.12   Add Ven_LoadGrGrDoc() to handle new GRGR format to replace Ven_LoadGrGrTxt().
 * 07/13/2022 22.0.1    Add Ven_LoadLien(), Ven_MergeLien(), Ven_MergeChar(), Ven_MergeMAdr(), Ven_MergeSAdr(), 
 *                      Ven_ExtrLienCsv() for new LDR layout.  Fix duplicate in Ven_MergeOwner().
 * 07/15/2022           Fix M_ZIP in Ven_MergeMAdr() & S_HSENO in Ven_MergeSAdr().
 * 10/31/2022 22.2.11   Fix SwapName bug in Ven_MergeOwner().  Add Ven_MergeUpdt2() and modify Ven_LoadUpdt()
 *                      to support new roll file format from the county.  Modify sort command on roll file.
 * 11/07/2022 22.3.2    Remove Ven_MergeUpdt2() since county has corrected the update roll layout.
 *            22.3.2.1  Add zipcode to M_CTY_ST_D in Ven_MergeUpdt().
 * 11/14/2022 22.3.3    Add Ven_MergeUpdt2() & Ven_FormatUpdtSale2() back to support new record layout with long DocNum.
 * 11/16/2022 22.3.4    Fix -Ld in loadVen() where roll file name was not formatted correctly.
 * 12/09/2022 22.4.2    Fix mail address in Ven_MergeMAdr()
 * 12/11/2022 22.4.2.1  Modify Ven_MergeMAdr() to add mail UNITNO.
 * 07/10/2023 23.0.1    Add Ven_MergeMAdr1(), Ven_MergeSAdr1(), Ven_MergeLien1(), and modify Ven_Load_Lien()
 *                      to support new file layout.
 * 11/20/2023 23.4.2    Adding Ven_Load_TaxBaseCsv(), Ven_Load_TaxSuppCsv() & Ven_Load_TaxDelqCsv() to support new tax files.
 * 12/12/2023 23.4.4    Add Ven_Load_Lien2(), Ven_MergeSAdr2(), Ven_MergeLien2() to rerun new 2023 file.
 * 01/11/2024 23.5.4    Modify Ven_MergeMAdr1() to put "PO BOX" on M_STREET.  Modify Ven_MergeLien2()
 *                      to remove default HO_FLG=2 when there is no exemption.  Change sort command in loadVen()
 *                      when loading daily file (this is a fixed length record).
 * 02/03/2024 23.6.0    Modify Ven_MergeUpdt(), Ven_MergeUpdt2(), Ven_MergeMAdr(), Ven_MergeMAdr1(), Ven_MergeSAdr2() to populate UnitNox.
 * 05/20/2024 23.8.3    Modify Ven_ParseTaxBaseCsv() to add new /correction tax bill if it has new BillNum.  In 
 *                      the case of same BillNum, we will deal with it later.
 * 07/05/2024 24.0.0    Ignore Name2 if it's the same as Name1 in Ven_MergeOwner().  Add Ven_MergeMAdr3(),
 *                      Ven_MergeSAdr3(), Ven_MergeLien3(), Ven_CreateLienRec3() to support new LDR file format.
 * 07/07/2024           Modify Ven_ParseTaxSuppCsv() to support new layout of "VenRpt-50 Secured Supplemental Billing File_20240610103811.txt"
 * 10/09/2024           Add -Lu option and Ven_UpdateAll() & Ven_LoadUpdt1() to load all update files. 
 *                      Modify Ven_MergeOwner() to rename "State Of California" and ignore Owner2.
 * 11/08/2024 24.3.2.1  Modify Ven_ParseTaxDetailCsv() & Ven_ParseTaxBaseCsv() to match new county tax file.
 * 01/20/2025 24.4.5    Modify Ven_Load_TaxDelqCsv() and related functions due to format changed.
 * 01/21/2025 24.4.5.1  Fix Ven_Load_TaxDelqCsv() and related functions to add Delq year and redeemed amount.
 *
 *******************************************************************************/

#include "stdafx.h"
#include "Prodlib.h"
#include "Utils.h"
#include "CountyInfo.h"
#include "R01.h"
#include "RecDef.h"
#include "Tables.h"
#include "XlatTbls.h"
#include "SaleRec.h"
#include "Logs.h"
#include "LoadOne.h"
#include "Utils.h"
#include "doSort.h"
#include "doOwner.h"
#include "doGrGr.h"
#include "FormatApn.h"
#include "Usecode.h"
#include "LOExtrn.h"
#include "PQ.h"
#include "MergeVen.h"
#include "Tax.h"

IDX_TBL5 VEN_DocCode[] =
{// DOCCODE, DOCTYPE, NONSALE, CODELEN, TYPELEN
   "AC","7 ",'N',2,2,  // ASSIGNMENT DEED
   "AD","5 ",'N',2,2,  // ADMINISTRATOR'S DEED
   "AJ","6 ",'Y',2,2,  // AFFIDAVIT
   "AL","6 ",'Y',2,2,  // AFFIDAVIT
   "AS","8 ",'N',2,2,  // AGREEMENT OF SALE
   "AT","6 ",'Y',2,2,  // AFFIDAVIT
   "AV","7 ",'N',2,2,  // ASSIGNMENT DEED
   "CD","9 ",'N',2,2,  // CORRECTION DEED
   "CP","35",'Y',2,2,  // COMMUNITY PROPERTY
   "DG","16",'N',2,2,  // GIFT DEED
   "DT","12",'Y',2,2,  // DECLARATION
   "D ","1 ",'N',2,2,  // Deed
   "ED","40",'Y',2,2,  // EASEMENT DEED
   "GD","1 ",'N',2,2,  // GRANT DEED
   "LA","44",'N',2,2,  // LEASE ASSIGNMENT
   "QD","4 ",'Y',2,2,  // QUITCLAIM DEED
   "TS","78",'N',2,2,  // TRUSTEE'S DEED UPON SALE
   "TX","67",'N',2,2,  // TAX DEED
   "UD","76",'Y',2,2,  // UNRECORDED DOCUMENT
   "","",0,0,0
};

static   long lSaleMatch, lSaleSkip, lLastDueUpdtDate, lBillCorrection, lCharMatch;
static	FILE *fdBase, *fdPaid, *fdDetail, *fdAgency, *fdRdmpt, *fdDelq, *fdDue, *fdChar;

/******************************** Ven_FormatApn *****************************
 *
 * Return 0 if successful, >0 if error
 *
 *****************************************************************************/

int Ven_FormatApn(char *pApn, char *pFmtApn)
{
   int   iTmp;

   iTmp = sprintf(pFmtApn, "%.3s%.6s%c", pApn, pApn+4, *(pApn+3));
   
   return iTmp;
}

/******************************** Ven_CleanName ******************************
 *
 * Clean up owner name and look for vesting
 *
 * Return 99 if found a vesting
 *
 *****************************************************************************/

int Ven_CleanName(char *pSrcName, char *pOwner, char *pDstName, char *pVesting, int iLen=0)
{
   char  acTmp[128], acSave[64], *pTmp;

   if (iLen > 0)
   {
      memcpy(acTmp, pSrcName, iLen);
      acTmp[iLen] = 0;
   } else
      strcpy(acTmp, pSrcName);

   // Replace '-' with '&'
   if (pTmp=strrchr(acTmp, '-'))
   {
      if (*(pTmp+2) > ' ' && pTmp > strchr(acTmp, ' '))
      {
         *(pTmp) = 0;
         strcpy(acSave, acTmp);
         strcat(acSave, " & ");
         pTmp++;
         strcat(acSave, pTmp);
         strcpy(acTmp,acSave);
      } else if (*(pTmp+1) < ' ')
         *pTmp = 0;
   } 

   remCharEx(acTmp, ".,'");
   replChar(acTmp, '*', ' ');
   blankRem(acTmp);
   strcpy(pOwner, acTmp);

   pTmp = findVesting(acTmp, pVesting);
   if (pTmp)
      *pTmp = 0;
   strcpy(pDstName, acTmp);

   if (pTmp)
      return 99;
   else
      return 0;
}

int Ven_CleanNameX(char *pSrcName, char *pOwner, char *pDstName, char *pVesting, int iLen=0)
{
   char  acTmp[128], acSave[64], *pTmp;

   if (iLen > 0)
   {
      memcpy(acTmp, pSrcName, iLen);
      acTmp[iLen] = 0;
   } else
      strcpy(acTmp, pSrcName);

   // Replace '-' with '&'
   if (pTmp=strrchr(acTmp, '-'))
   {
      if (*(pTmp+2) > ' ')
      {
         *(pTmp) = 0;
         strcpy(acSave, acTmp);
         strcat(acSave, " & ");
         pTmp++;
         strcat(acSave, pTmp);
         strcpy(acTmp,acSave);
      } else
         *pTmp = 0;
   } 

   remCharEx(acTmp, ".,'*");

   blankRem(acTmp);
   strcpy(pOwner, acTmp);

   pTmp = findVesting(acTmp, pVesting);
   if (pTmp)
      *pTmp = 0;
   strcpy(pDstName, acTmp);

   if (pTmp)
      return 99;
   else
      return 0;
}

/******************************** Ven_MergeOwner *****************************
 *
 * 0030103300 60001PAULSON SIGNE L*ET AL
 *
 * Return 0 if successful, >0 if error
 *
 *****************************************************************************/

void Ven_MergeOwner(char *pOutbuf, char *pName1, char *pName2)
{
   int   iTmp;
   char  acOwner1[64], acOwner2[64], acTmp[64], acName1[64], acName2[64], 
         acVesting1[8], acVesting2[8], *pTmp, *pTmp1;

   OWNER myOwner;

   // Clear output buffer if needed
   removeNames(pOutbuf);

   if (!memcmp(pName1, pName2, RSIZ_NAME1))
      *pName2 = 0;

   // Initialize
   memcpy(acName1, pName1, RSIZ_NAME1);
   if (!memcmp(pName2, "ET AL", 5))
      acName2[0] = 0;
   else
      memcpy(acName2, pName2, RSIZ_NAME2);
   blankRem(acName1, RSIZ_NAME1);
   acVesting1[0]=acVesting2[0] = 0;

   if (!memcmp(acName1, "CALIFORNIA STATE OF", 18))
   {
      memcpy(pOutbuf+OFF_NAME1, "STATE OF CALIFORNIA ", 20);
      memcpy(pOutbuf+OFF_NAME_SWAP, "STATE OF CALIFORNIA ", 20);
   } else
   {
      // Check for CareOf
      iTmp = blankRem(acName2, RSIZ_NAME2);
      if (!memcmp(acName2, "ATTN", 4) || !memcmp(acName2, "C/O", 3) || !memcmp(acName2, "ATTM", 4))
      {
         updateCareOf(pOutbuf, acName2, iTmp);
         acName2[0] = 0;
      }

      // Cleanup Name1 
      iTmp = Ven_CleanName(acName1, acOwner1, acTmp, acVesting1);
      if (iTmp == 99)
      {
         if (strchr(acTmp, ' '))
            strcpy(acName1, acTmp);
         memcpy(pOutbuf+OFF_VEST, acVesting1, strlen(acVesting1));

         // Check ETAL
         if (!memcmp(acVesting1, "EA", 2))
            *(pOutbuf+OFF_ETAL_FLG) = 'Y';
      } else
         strcpy(acName1, acOwner1);


      // Populate Name1
      vmemcpy(pOutbuf+OFF_NAME1, acOwner1, SIZ_NAME1);

      // Populate Name2
      if (acName2[0] > ' ')
      {
         iTmp = Ven_CleanName(acName2, acOwner2, acTmp, acVesting2);
         if (iTmp == 99)
         {
            if (strchr(acTmp, ' '))
               strcpy(acName2, acTmp);

            if (acVesting1[0] < 'A')
            {
               memcpy(pOutbuf+OFF_VEST, acVesting2, strlen(acVesting2));

               // Check EtAl
               if (!memcmp(acVesting2, "EA", 2))
                  *(pOutbuf+OFF_ETAL_FLG) = 'Y';
            } 
         } else
            strcpy(acName2, acOwner2);

         vmemcpy(pOutbuf+OFF_NAME2, acOwner2, SIZ_NAME2);
      }

      // Try merge two names
      if (acVesting1[0] < 'A' && acVesting2[0] < 'A' && acName2[0] > ' '    &&
          !strchr(acOwner1, '&') && !strchr(acOwner2, '&') )
      {
         pTmp  = strchr(acName1, ' ');
         pTmp1 = strchr(acName2, ' ');
         if (pTmp && pTmp1)
         {
            *pTmp = 0;
            *pTmp1 = 0;
            if (!strcmp(acName1, acName2))
            {
               // Last name matched
               *pTmp = ' ';
               strcat(acName1, " & ");
               strcat(acName1, pTmp1+1);

               if (strlen(acName1) > SIZ_NAME_SWAP)
                  strcpy(acName1, acOwner1);
            } else
               *pTmp = ' ';
         }
      }

      // Now parse owners
      splitOwner(acName1, &myOwner, 5);
      if (*(pOutbuf+OFF_VEST) > ' ' && isVestChk(pOutbuf+OFF_VEST))
      {
         memcpy(myOwner.acSwapName, pOutbuf+OFF_NAME1, SIZ_NAME1);
         myOwner.acSwapName[SIZ_NAME1] = 0;
      }
      iTmp = strlen(myOwner.acSwapName);
      memcpy(pOutbuf+OFF_NAME_SWAP, myOwner.acSwapName, iTmp);
   }
}

void Ven_MergeOwnerX(char *pOutbuf, char *pName1, char *pName2)
{
   int   iTmp;
   char  acOwner1[64], acOwner2[64], acTmp[64], acName1[64], acName2[64], 
         acVesting1[8], acVesting2[8], *pTmp, *pTmp1;

   OWNER myOwner;

   // Clear output buffer if needed
   removeNames(pOutbuf);

   // Initialize
   memcpy(acName1, pName1, RSIZ_NAME1);
   memcpy(acName2, pName2, RSIZ_NAME2);
   blankRem(acName1, RSIZ_NAME1);
   acVesting1[0]=acVesting2[0] = 0;

#ifdef _DEBUG
   // 0011900850
   //if (!memcmp(pOutbuf, "0011900850", 10))
   //   iTmp = 0;
#endif

   // Check for CareOf
   iTmp = blankRem(acName2, RSIZ_NAME2);
   if (!memcmp(acName2, "ATTN", 4) || !memcmp(acName2, "C/O", 3))
   {
      updateCareOf(pOutbuf, acName2, iTmp);
      acName2[0] = 0;
   }

   // Cleanup Name1 
   iTmp = Ven_CleanNameX(acName1, acOwner1, acTmp, acVesting1);
   if (iTmp == 99)
   {
      if (strchr(acTmp, ' '))
         strcpy(acName1, acTmp);
      memcpy(pOutbuf+OFF_VEST, acVesting1, strlen(acVesting1));

      // Check EtAl
      if (!memcmp(acVesting1, "EA", 2))
         *(pOutbuf+OFF_ETAL_FLG) = 'Y';
   } else
      strcpy(acName1, acOwner1);

   // Populate Name1
   iTmp = strlen(acOwner1);
   memcpy(pOutbuf+OFF_NAME1, acOwner1, iTmp);

   // Populate Name2
   if (acName2[0] > ' ')
   {
      iTmp = Ven_CleanNameX(acName2, acOwner2, acTmp, acVesting2);
      if (iTmp == 99)
      {
         if (strchr(acTmp, ' '))
            strcpy(acName2, acTmp);

         if (acVesting1[0] < 'A')
         {
            memcpy(pOutbuf+OFF_VEST, acVesting2, strlen(acVesting2));

            // Check EtAl
            if (!memcmp(acVesting2, "EA", 2))
               *(pOutbuf+OFF_ETAL_FLG) = 'Y';
         } 
      } else
         strcpy(acName2, acOwner2);

      iTmp = strlen(acOwner2);
      if (iTmp > SIZ_NAME2) iTmp = SIZ_NAME2;
      memcpy(pOutbuf+OFF_NAME2, acOwner2, iTmp);
   }

   // Try merge two names
   if (acVesting1[0] < 'A' && acVesting2[0] < 'A' && acName2[0] > ' '    &&
       !strchr(acOwner1, '&') && !strchr(acOwner2, '&') )
   {
      pTmp  = strchr(acName1, ' ');
      pTmp1 = strchr(acName2, ' ');
      if (pTmp && pTmp1)
      {
         *pTmp = 0;
         *pTmp1 = 0;
         if (!strcmp(acName1, acName2))
         {
            // Last name matched
            *pTmp = ' ';
            strcat(acName1, " & ");
            strcat(acName1, pTmp1+1);
         } else
            *pTmp = ' ';
      }
   }

   // Now parse owners
   splitOwner(acName1, &myOwner, 5);
   if (*(pOutbuf+OFF_VEST) > ' ' && isVestChk(pOutbuf+OFF_VEST))
   {
      memcpy(myOwner.acSwapName, pOutbuf+OFF_NAME1, SIZ_NAME1);
      myOwner.acSwapName[SIZ_NAME1] = 0;
   }
   iTmp = strlen(myOwner.acSwapName);
   memcpy(pOutbuf+OFF_NAME_SWAP, myOwner.acSwapName, iTmp);
}

/* 10/01/2013 - replaced by above function
void Ven_MergeOwner1(char *pOutbuf, char *pName1, char *pName2)
{
   int   iTmp, iTmp1, iMerge=0;
   char  acOwners[128], acTmp[128], acSave[64], acSave1[64];
   char  *pTmp, *pTmp1, acName2[64], acVest[4];

   OWNER myOwner;

   // Clear output buffer if needed
   removeNames(pOutbuf);

   // Remove multiple spaces
   memcpy(acOwners, pName1, RSIZ_NAME1);
   memcpy(acName2, pName2, RSIZ_NAME2);
   blankRem(acOwners, RSIZ_NAME1);

   acVest[0] = 0;
   pTmp = acOwners;
   iTmp = iTmp1 = 0;
   while (*pTmp)
   {
      // Mark name with numeric value
      if (isdigit(*pTmp))
         iTmp1++;

      // skip unwanted chars at beginning of name
      if (*pTmp < '1' && iTmp == 0)
      {
         pTmp++;
         continue;
      }

      acTmp[iTmp++] = *pTmp;
      // Remove multi-space
      if (*pTmp == ' ')
         while (*pTmp == ' ') pTmp++;
      else
         pTmp++;

      // Remove '.' too
      if (*pTmp == '.' || *pTmp == '{' || *pTmp == '}' || *pTmp == '*')
         pTmp++;
   }
   acTmp[iTmp] = 0;

   // Leave name as is if number present
   if (isdigit(acTmp[0]))
   {
      if (iTmp > SIZ_NAME1) iTmp = SIZ_NAME1;
      memcpy(pOutbuf+OFF_NAME1, acTmp, iTmp);
      memcpy(pOutbuf+OFF_NAME_SWAP, acTmp, iTmp);
      return;
   }

   // Check for CareOf
   iTmp = blankRem(acName2, RSIZ_NAME2);
   if (!memcmp(acName2, "ATTN", 4) || !memcmp(acName2, "C/O", 3))
   {
      updateCareOf(pOutbuf, acName2, iTmp);
      acName2[0] = 0;
   }

   // Replace '-' with '&'
   if (pTmp=strrchr(acTmp, '-'))
   {
      if (*(pTmp+2) > ' ')
      {
         *(pTmp) = 0;
         strcpy(acSave1, acTmp);
         strcat(acSave1, " & ");
         pTmp++;
         strcat(acSave1, pTmp);
         strcpy(acTmp,acSave1);
         iMerge = 1;
      } else
         *pTmp = 0;
   } 

   // Drop everything from these words
   if ((pTmp=strstr(acTmp, " ETAL")) || (pTmp=strstr(acTmp, " ET AL")) )
   {
      *pTmp = 0;
      strcpy(acVest, "EA");
   }

   // Save owner for Name1
   strcpy(acOwners, acTmp);

   // Save data to append later
   if ((pTmp1=strstr(acTmp, " TRUST")) || (pTmp1=strstr(acTmp, " TR ")) )
   {
      if ((pTmp=strstr(acTmp, " FAM"))   || (pTmp=strstr(acTmp, " LIV"))   || 
          (pTmp=strstr(acTmp, " REVOC")) || (pTmp=strstr(acTmp, " REV ")) ||
          (pTmp=strstr(acTmp, " MARITAL EXEMP "))
         )
      {
         // Check for year that goes before these words
         while (isdigit(*(pTmp-1)))
            pTmp--;
         *pTmp = 0;
      } else
         *pTmp1 = 0;

      iMerge |= 2;
   }

   if ((pTmp=strstr(acTmp, " LIFE EST"))  || (pTmp=strstr(acTmp, " CO-TR")) ||
       (pTmp=strstr(acTmp, " FAM TR"))    || (pTmp=strstr(acTmp, " IRREV TR")) ||
       (pTmp=strstr(acTmp, " GEN TR"))    || (pTmp=strstr(acTmp, " SURV TR"))  ||
       (pTmp=strstr(acTmp, " LIV TR"))    || (pTmp=strstr(acTmp, " REV TR")) ||
       (pTmp=strstr(acTmp, " SUCC "))     || (pTmp=strstr(acTmp, " SUC TTEE")) 
      )
   {
      // Check for year that goes before these words
      while (isdigit(*(pTmp-1)))
         pTmp--;
      *pTmp = 0;
      iMerge |= 2;
   }

   // Check last word for TR or TTEE or EST
   if (pTmp = strrchr(acTmp, ' '))
   {
      if (!strcmp(pTmp, " TR") || !strcmp(pTmp, " EST") || 
          !strcmp(pTmp, " TTEE") || !strcmp(pTmp, " ESTATE"))
      {
         *pTmp = 0;
         iMerge |= 2;
      }
   }

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "00311618", 8))
   //   iTmp = 0;
#endif

   // Now parse owners if there is more than one word
   if (strchr(acTmp, ' '))
      iTmp = splitOwner(acTmp, &myOwner, 5);
   else
   {
      // Name is not parsable
      iTmp = -1;
   }

   switch (iTmp)
   {
      case -1:
         iMerge |= 16;
         break;
      case 2:
         iMerge |= 1;
         break;
   }

   // If name is not parsable, use input
   if (iMerge & 16)
      strcpy(myOwner.acSwapName, acOwners);

   iTmp = strlen(myOwner.acSwapName);
   if (iTmp > SIZ_NAME_SWAP)
      iTmp = SIZ_NAME_SWAP;
   memcpy(pOutbuf+OFF_NAME_SWAP, myOwner.acSwapName, iTmp);

   // Process Name2
   if (acName2[0] > ' ')
   {
      if (pTmp=strrchr(acName2, '-'))
      {
         // If name2 is a combined name already, do not merge with name1
         if (*(pTmp+2) > ' ')
         {
            *(pTmp) = 0;
            strcpy(acTmp, acName2);
            strcat(acTmp, " & ");
            pTmp++;
            strcat(acTmp, pTmp);
            strcpy(acName2, acTmp);
            iMerge |= 4;           
         } else
            *pTmp = 0;
      } 

      // Check for removable words
      if ((pTmp1=strstr(acName2, "TRUST")) || (pTmp1=strstr(acName2, " TR ")) )
         iMerge |= 8;

      if ((pTmp=strstr(acName2, " LIFE EST")))
         iMerge |= 8;

      // Check last word for TR or TTEE or EST
      if (pTmp = strrchr(acName2, ' '))
      {
         if (!strcmp(pTmp, " TR") || !strcmp(pTmp, " EST") || !strcmp(pTmp, " TTEE"))
            iMerge |= 8;
      }
   }

   // Merge name 1 and name2 if no flag set and name2 is avail.
   if (!iMerge && acName2[0] > '0')
   {
      // If they have the same last name, merge them
      strcpy(acSave, myOwner.acName1);
      strcpy(acSave1, acName2);
      pTmp  = strchr(acSave, ' ');
      pTmp1 = strchr(acSave1, ' ');
      if (pTmp && pTmp1)
      {
         *pTmp = 0;
         *pTmp1 = 0;
         if (!strcmp(acSave, acSave1))
         {
            // Last name matched
            strcat(myOwner.acName1, " & ");
            strcat(myOwner.acName1, pTmp1+1);
            acName2[0] = 0;
         }
      }
      if (acName2[0] > '0')
      {
         iTmp = strlen(acName2);
         if (iTmp > SIZ_NAME2)
            iTmp = SIZ_NAME2;
         memcpy(pOutbuf+OFF_NAME2, acName2, iTmp);
      }

      iTmp = strlen(myOwner.acName1);
      if (iTmp > SIZ_NAME1)
         iTmp = SIZ_NAME1;
      memcpy(pOutbuf+OFF_NAME1, myOwner.acName1, iTmp);
   } else
   {
      // Name2
      if (acName2[0] > '0')
      {
         iTmp = strlen(acName2);
         if (iTmp > SIZ_NAME2)
            iTmp = SIZ_NAME2;
         memcpy(pOutbuf+OFF_NAME2, acName2, iTmp);
      }

      // Name1
      if (iMerge)
      {
         iTmp = strlen(acOwners);
         if (iTmp > SIZ_NAME1)
            iTmp = SIZ_NAME1;
         memcpy(pOutbuf+OFF_NAME1, acOwners, iTmp);
      } else
      {  // Single name
         iTmp = strlen(myOwner.acName1);
         if (iTmp > SIZ_NAME1)
            iTmp = SIZ_NAME1;
         memcpy(pOutbuf+OFF_NAME1, myOwner.acName1, iTmp);
      }
   }
}

/***************************** Ven_MergeSaleRec ***************************
 *
 **************************************************************************/

int Ven_MergeSaleRec(char *pOutbuf, char *pGrGrRec)
{
   long  lCurSaleDt, lLstSaleDt, lDocNum;
   int   iTmp;
   char  acDocNum[32];

   SALE_REC1 *pSaleRec = (SALE_REC1 *)pGrGrRec;

   lCurSaleDt = atoin(pSaleRec->acDocDate, SIZ_SALE1_DT);
   lLstSaleDt = atoin(pOutbuf+OFF_SALE1_DT, SIZ_SALE1_DT);

   if (lCurSaleDt <= lLstSaleDt)
      return 0;

   // Move sale2 to sale3
   memcpy(pOutbuf+OFF_SALE3_DT, pOutbuf+OFF_SALE2_DT, SIZ_SALE3_DT);
   memcpy(pOutbuf+OFF_SALE3_DOC, pOutbuf+OFF_SALE2_DOC, SIZ_SALE3_DOC);
   memcpy(pOutbuf+OFF_SALE3_DOCTYPE, pOutbuf+OFF_SALE2_DOCTYPE, SIZ_SALE3_DOCTYPE);
   memcpy(pOutbuf+OFF_SALE3_AMT, pOutbuf+OFF_SALE2_AMT, SIZ_SALE3_AMT);
   memcpy(pOutbuf+OFF_SALE3_CODE, pOutbuf+OFF_SALE2_CODE, SIZ_SALE3_CODE);
   *(pOutbuf+OFF_AR_CODE1+2) = *(pOutbuf+OFF_AR_CODE1+1);

   // Move sale1 to sale2
   memcpy(pOutbuf+OFF_SALE2_DT, pOutbuf+OFF_SALE1_DT, SIZ_SALE2_DT);
   memcpy(pOutbuf+OFF_SALE2_DOC, pOutbuf+OFF_SALE1_DOC, SIZ_SALE2_DOC);
   memcpy(pOutbuf+OFF_SALE2_DOCTYPE, pOutbuf+OFF_SALE1_DOCTYPE, SIZ_SALE3_DOCTYPE);
   memcpy(pOutbuf+OFF_SALE2_AMT, pOutbuf+OFF_SALE1_AMT, SIZ_SALE3_AMT);
   memcpy(pOutbuf+OFF_SALE2_CODE, pOutbuf+OFF_SALE1_CODE, SIZ_SALE3_CODE);
   *(pOutbuf+OFF_AR_CODE1+1) = *(pOutbuf+OFF_AR_CODE1);

   // Update current sale
   lDocNum = atoin(pSaleRec->acDocNum, 9);
   sprintf(acDocNum, "%.7d     ", lDocNum);
   memcpy(pOutbuf+OFF_SALE1_DT, pSaleRec->acDocDate, SIZ_SALE1_DT);
   memcpy(pOutbuf+OFF_SALE1_DOC, acDocNum, SIZ_SALE1_DOC);

   iTmp = atoi(pSaleRec->acDocType);
   if (iTmp > 0)
      memcpy(pOutbuf+OFF_SALE1_DOCTYPE, pSaleRec->acDocType, 4);
   else
      memset(pOutbuf+OFF_SALE1_DOCTYPE, ' ', SIZ_SALE1_DOCTYPE);


   // Move current owner (name1) to seller
   memcpy(pOutbuf+OFF_SELLER, pSaleRec->acSeller, SIZ_SELLER);

   // Update transfers
   lLstSaleDt = atoin(pOutbuf+OFF_TRANSFER_DT, SIZ_SALE1_DT);
   if (lCurSaleDt > lLstSaleDt)
   {
      memcpy(pOutbuf+OFF_TRANSFER_DT, pSaleRec->acDocDate, SIZ_TRANSFER_DT);
      memcpy(pOutbuf+OFF_TRANSFER_DOC, acDocNum, SIZ_SALE1_DOC);
   }

   // There is no sale price in update file, we have to clear current sale price
   memset(pOutbuf+OFF_SALE1_AMT, ' ', SIZ_SALE1_AMT);
   memset(pOutbuf+OFF_SALE1_CODE, ' ', SIZ_SALE1_CODE);
   *(pOutbuf+OFF_AR_CODE1) = 'R';

   return 1;
}

/****************************** Ven_MergeGrGrSale *****************************
 *
 * Input: Sale file is output from GrGr extract.
 *
 ******************************************************************************/

int Ven_MergeGrGrSale(char *pOutbuf)
{
   static   char  acRec[1024], *pRec=NULL;
   int      iRet, iLoop;
   SALE_REC1 *pSale;

   // Get first Char rec for first call
   if (!pRec)
      pRec = fgets(acRec, 1024, fdSale);

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "001060075", 9))
   //   iRet = 0;
#endif
   pSale = (SALE_REC1 *)&acRec[0];
   do
   {
      if (!pRec)
      {
         fclose(fdSale);
         fdSale = NULL;
         return 1;      // EOF
      }

      // Compare Apn
      iLoop = memcmp(pOutbuf, pSale->acApn, iApnLen);
      if (iLoop > 0)
      {
         if (bDebug)
            LogMsg0("Skip Sale rec  %.*s", iApnLen, pSale->acApn);
         pRec = fgets(acRec, 1024, fdSale);
         lSaleSkip++;
      }
   } while (iLoop > 0);

   // If not match, return
   if (iLoop)
      return 1;

   do
   {
      iRet = Ven_MergeSaleRec(pOutbuf, acRec);

      // Get next sale record
      pRec = fgets(acRec, 1024, fdSale);
      if (!pRec)
      {
         fclose(fdSale);
         fdSale = NULL;
         break;
      }

   } while (!memcmp(pOutbuf, pSale->acApn, iApnLen));

   lSaleMatch++;
  
   return 0;
}

/**************************** Ven_MergeRollSale ***************************
 *
 **************************************************************************/

int Ven_MergeRollSale(char *pOutbuf, char *pRoll)
{
   long   lCurSaleDt, lLstSaleDt, lPrice;
   char   acTmp[32];
   int    iTmp;
   double dTax; 

   VEN_UPDT *pSaleRec = (VEN_UPDT *)pRoll;

   lCurSaleDt = atoin(pSaleRec->Doc_Dt, SIZ_SALE1_DT);
   lLstSaleDt = atoin(pOutbuf+OFF_SALE1_DT, SIZ_SALE1_DT);
   dTax = atofn(pSaleRec->Doc_Tax, SSIZ_DOC_TAX);

   if (lCurSaleDt < lLstSaleDt)
      return 0;

   if (lCurSaleDt == lLstSaleDt)
   {
      // Update current sale with new information
      memcpy(pOutbuf+OFF_SALE1_DOC, pSaleRec->DocNum, SSIZ_DOC_NR);
      memcpy(pOutbuf+OFF_TRANSFER_DOC, pSaleRec->DocNum, SSIZ_DOC_NR);

      iTmp = findDocType(pSaleRec->DocType, (IDX_TBL5 *)&VEN_DocCode[0]);
      if (iTmp >= 0)
         memcpy(pOutbuf+OFF_SALE1_DOCTYPE, VEN_DocCode[iTmp].pCode, VEN_DocCode[iTmp].iCodeLen);

      if (dTax > 0.0)
      {
         lPrice = (long)((dTax * SALE_FACTOR)/100.0) * 100;
         sprintf(acTmp, "%*u", SIZ_SALE1_AMT, lPrice);
         memcpy(pOutbuf+OFF_SALE1_AMT, acTmp, SIZ_SALE1_AMT);
      }

      *(pOutbuf+OFF_AR_CODE1) = 'A';
      return 0;
   } else
   {
      // Move sale2 to sale3
      memcpy(pOutbuf+OFF_SALE3_DT, pOutbuf+OFF_SALE2_DT, SIZ_SALE3_DT);
      memcpy(pOutbuf+OFF_SALE3_DOC, pOutbuf+OFF_SALE2_DOC, SIZ_SALE3_DOC);
      memcpy(pOutbuf+OFF_SALE3_DOCTYPE, pOutbuf+OFF_SALE2_DOCTYPE, SIZ_SALE3_DOCTYPE);
      memcpy(pOutbuf+OFF_SALE3_AMT, pOutbuf+OFF_SALE2_AMT, SIZ_SALE3_AMT);
      memcpy(pOutbuf+OFF_SALE3_CODE, pOutbuf+OFF_SALE2_CODE, SIZ_SALE3_CODE);
      *(pOutbuf+OFF_AR_CODE1+2) = *(pOutbuf+OFF_AR_CODE1+1);

      // Move sale1 to sale2
      memcpy(pOutbuf+OFF_SALE2_DT, pOutbuf+OFF_SALE1_DT, SIZ_SALE2_DT);
      memcpy(pOutbuf+OFF_SALE2_DOC, pOutbuf+OFF_SALE1_DOC, SIZ_SALE2_DOC);
      memcpy(pOutbuf+OFF_SALE2_DOCTYPE, pOutbuf+OFF_SALE1_DOCTYPE, SIZ_SALE3_DOCTYPE);
      memcpy(pOutbuf+OFF_SALE2_AMT, pOutbuf+OFF_SALE1_AMT, SIZ_SALE3_AMT);
      memcpy(pOutbuf+OFF_SALE2_CODE, pOutbuf+OFF_SALE1_CODE, SIZ_SALE3_CODE);
      *(pOutbuf+OFF_AR_CODE1+1) = *(pOutbuf+OFF_AR_CODE1);
   }

   // Update current sale
   memcpy(pOutbuf+OFF_SALE1_DT, pSaleRec->Doc_Dt, SIZ_SALE1_DT);
   memcpy(pOutbuf+OFF_SALE1_DOC, pSaleRec->DocNum, SSIZ_DOC_NR);

   iTmp = findDocType(pSaleRec->DocType, (IDX_TBL5 *)&VEN_DocCode[0]);
   if (iTmp >= 0)
      memcpy(pOutbuf+OFF_SALE1_DOCTYPE, VEN_DocCode[iTmp].pCode, VEN_DocCode[iTmp].iCodeLen);

   // Move current owner (name1) to seller
   memcpy(pOutbuf+OFF_SELLER, pOutbuf+OFF_NAME1, SIZ_SELLER);

   // Update transfers
   lLstSaleDt = atoin(pOutbuf+OFF_TRANSFER_DT, SIZ_SALE1_DT);
   if (lCurSaleDt > lLstSaleDt)
   {
      memcpy(pOutbuf+OFF_TRANSFER_DT, pSaleRec->Doc_Dt, SIZ_TRANSFER_DT);
      memcpy(pOutbuf+OFF_TRANSFER_DOC, pSaleRec->DocNum, SSIZ_DOC_NR);
   }

   // If there is no sale price in update file, we have to clear current sale price
   if (dTax > 0.0)
   {
      lPrice = (long)((dTax * SALE_FACTOR)/100.0) * 100;
      sprintf(acTmp, "%*u", SIZ_SALE1_AMT, lPrice);
      memcpy(pOutbuf+OFF_SALE1_AMT, acTmp, SIZ_SALE1_AMT);
   } else
      memset(pOutbuf+OFF_SALE1_AMT, ' ', SIZ_SALE1_AMT);
   memset(pOutbuf+OFF_SALE1_CODE, ' ', SIZ_SALE1_CODE);
   *(pOutbuf+OFF_AR_CODE1) = 'A';

   return 1;
}

/********************************* Ven_MergeUpdt *****************************
 *
 * This version is used with daily file "VENTURA_PUBINFO-yyymmdd"
 *
 * Return 0 if successful, >0 if error
 *
 *****************************************************************************/

int Ven_MergeUpdt(char *pOutbuf, char *pRollRec, int iFlag)
{
   VEN_UPDT *pRec;
   char      *pTmp, acTmp[256], acTmp1[256], acCity[64], acCode[32], acSfx[16];
   int       iRet;
   long      lTmp, lBldgSqft, lSqFtLand;
   int       iTmp, iCode;

   pRec = (VEN_UPDT *)pRollRec;
   iRet = 0;

#ifdef _DEBUG
   //if (!memcmp(pRec->Apn, "5260144365", 9))
   //   iTmp = 0;
#endif
   if (iFlag & CREATE_R01)
   {
      // Clear output buffer
      memset(pOutbuf, ' ', iRecLen);

      // Start copying data
      memcpy(acTmp1, pRec->Apn, SSIZ_APN);

      // Save original APN as Alt APN
      memcpy(pOutbuf+OFF_ALT_APN, acTmp1, SSIZ_APN);

      // Reformat APN to CDDATA's format
      iTmp = Ven_FormatApn(acTmp1, acTmp);
      memcpy(pOutbuf, acTmp, iTmp);

      memcpy(pOutbuf+OFF_CO_NUM, "56VEN", 5);
      
      // Format APN for display
      iTmp = formatApn(acTmp, acTmp1, &myCounty);
      memcpy(pOutbuf+OFF_APN_D, acTmp1, iTmp);

      // Create MapLink and output new record
      iTmp = formatMapLink(acTmp, acTmp1, &myCounty);
      memcpy(pOutbuf+OFF_MAPLINK, acTmp1, iTmp);

      // Create index map link
      if (getIndexPage(acTmp1, acTmp, &myCounty))
         memcpy(pOutbuf+OFF_IMAPLINK, acTmp, iRet);
   }

   // TRA
   lTmp = atoin(pRec->TRA, RSIZ_TRA);
   if (lTmp > 0)
   {
      iTmp = sprintf(acTmp, "%.6d", lTmp);
      memcpy(pOutbuf+OFF_TRA, acTmp, iTmp);
   }

   // APN Status 
   // - T is active parcel being transfer
   // - V is voided/deleted parcel
   if (pRec->Apn_Status[0] == 'V')
      *(pOutbuf+OFF_STATUS) = pRec->Apn_Status[0];
   else
      *(pOutbuf+OFF_STATUS) = 'A';

   // Bldg Sqft
   lBldgSqft = atoin(pRec->BldgSqft, SSIZ_SQ_FT_I);
   if (lBldgSqft > 0)
   {
      sprintf(acTmp, "%*u", SIZ_BLDG_SF, lBldgSqft);
      memcpy(pOutbuf+OFF_BLDG_SF, acTmp, SIZ_BLDG_SF);
   }

   // Number of units
   memcpy(pOutbuf+OFF_UNITS, pRec->Tot_Units,SSIZ_TOT_UNITS);

   // LotSqft
   lSqFtLand = atoin(pRec->LotSqft, SSIZ_SQ_FT_L);
   if (lSqFtLand > 0)
   {
      sprintf(acTmp, "%*u", SIZ_LOT_SQFT, lSqFtLand);
      memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);
   }

   double dAcreAge = atofn(pRec->Acres, SSIZ_ACREAGE);
   if (dAcreAge > 0.0)
   {
      sprintf(acTmp, "%*u", SIZ_LOT_ACRES, (long)(dAcreAge*1000));
      memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);
      if (lSqFtLand == 0)
      {
         lSqFtLand = (long)(dAcreAge * SQFT_PER_ACRE);
         sprintf(acTmp, "%*u", SIZ_LOT_SQFT, lSqFtLand);
         memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);
      }
   } else if (lSqFtLand > 0)
   {
      dAcreAge = lSqFtLand/SQFT_PER_ACRE; 
      sprintf(acTmp, "%*u", SIZ_LOT_ACRES, (long)(dAcreAge*1000));
      memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);
   }

   // Mailing
   removeMailing(pOutbuf, false);

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "0753200100", 9))
   //   iTmp = 0;
#endif
   ADR_REC sMailAdr;
   memset(&sMailAdr, 0, sizeof(ADR_REC));
   if (memcmp(pRec->M_Addr1, "     ", 5) && memcmp(pRec->M_Addr1, "BLANK", 5))
   {
      memcpy(acTmp, pRec->M_Addr1, SSIZ_MAIL_ADDR);
      blankRem(acTmp, RSIZ_M_STREET);
      vmemcpy(pOutbuf+OFF_M_ADDR_D, acTmp, SIZ_M_ADDR_D);

      // Parse mailing
      parseMAdr1(&sMailAdr, acTmp);

      if (sMailAdr.lStrNum > 0)
      {
         iTmp = sprintf(acTmp, "%d", sMailAdr.lStrNum);
         memcpy(pOutbuf+OFF_M_STRNUM, acTmp, iTmp);
      }
      if (sMailAdr.strSub[0] > ' ')
         vmemcpy(pOutbuf+OFF_M_STR_SUB, sMailAdr.strSub, SIZ_M_STR_SUB);
      if (sMailAdr.strDir[0] > ' ')
         vmemcpy(pOutbuf+OFF_M_DIR, sMailAdr.strDir, SIZ_M_DIR);
      vmemcpy(pOutbuf+OFF_M_STREET, sMailAdr.strName, SIZ_M_STREET);
      if (sMailAdr.strSfx[0] > ' ')
         vmemcpy(pOutbuf+OFF_M_SUFF, sMailAdr.strSfx, SIZ_M_SUFF);
      if (sMailAdr.Unit[0] > ' ')
      {
         vmemcpy(pOutbuf+OFF_M_UNITNO, sMailAdr.Unit, SIZ_M_UNITNO);
         vmemcpy(pOutbuf+OFF_M_UNITNOX, sMailAdr.UnitNox, SIZ_M_UNITNOX);
      }
   }

   memcpy(acTmp, pRec->M_CtySt, SSIZ_CTY_STA);
   myTrim(acTmp, SSIZ_CTY_STA);
   if (acTmp[0] > '0' && memcmp(acTmp, "     ", 5) && memcmp(acTmp, "BLANK", 5))
   {
      parseMAdr2(&sMailAdr, acTmp);
      iTmp = strlen(sMailAdr.City);
      if (iTmp > SIZ_M_CITY) iTmp = SIZ_M_CITY;
      memcpy(pOutbuf+OFF_M_CITY, sMailAdr.City, iTmp);
      memcpy(pOutbuf+OFF_M_ST, sMailAdr.State, SIZ_M_ST);
      // Ignore zip starts with XXX
      if (!memcmp(pRec->M_Zip, "XXX", 3) || !memcmp(pRec->M_Zip, "UNK", 3))
         memset(pOutbuf+OFF_M_ZIP, ' ', SIZ_M_ZIP+SIZ_M_ZIP4);
      else
      {
         if (sMailAdr.Zip[0] > ' ')
         {
            memcpy(pOutbuf+OFF_M_ZIP, sMailAdr.Zip, strlen(sMailAdr.Zip));
         } else if (pRec->M_Zip[0] > ' ')
         {
            memcpy(pOutbuf+OFF_M_ZIP, pRec->M_Zip, SIZ_M_ZIP);
            if (pRec->M_Zip[SIZ_M_ZIP] == '-')
               memcpy(pOutbuf+OFF_M_ZIP4, &pRec->M_Zip[SIZ_M_ZIP+1], SIZ_M_ZIP4);
            sprintf(acTmp, "%.*s %.*s", SSIZ_CTY_STA, pRec->M_CtySt, SSIZ_ZIP, pRec->M_Zip);
         }
      }
      iTmp = blankRem(acTmp);
      memcpy(pOutbuf+OFF_M_CTY_ST_D, acTmp, iTmp);
   }

   // Situs - StrNum field can be number street name
   removeSitus(pOutbuf);

   lTmp = atoin(pRec->Situs_Nr, SSIZ_SITUS_NR);
   if (lTmp > 0)
   {
      // Save original HSENO
      memcpy(pOutbuf+OFF_S_HSENO, pRec->Situs_Nr, SSIZ_SITUS_NR);

      iTmp = sprintf(acTmp, "%d", lTmp);
      memcpy(pOutbuf+OFF_S_STRNUM, acTmp, iTmp);
      memcpy(pOutbuf+OFF_S_DIR, pRec->Situs_Dir, SIZ_S_DIR);
      memcpy(pOutbuf+OFF_S_STREET, pRec->Situs_Street, SIZ_S_STREET);
      memcpy(acTmp, pRec->Situs_Type, SSIZ_SITUS_TYPE);
      acTmp[SSIZ_SITUS_TYPE] = 0;
      iTmp = GetSfxCodeX(acTmp, acSfx);
      if (iTmp > 0)
      {
         sprintf(acCode, "%d     ", iTmp); 
         memcpy(pOutbuf+OFF_S_SUFF, acCode, SIZ_S_SUFF);
      }
      memcpy(pOutbuf+OFF_S_UNITNO, pRec->Situs_Unit, SIZ_S_UNITNO);

      if (pRec->Situs_Unit[0] > ' ')
         sprintf(acTmp, "%.36s %s #%.*s", pRec->Situs_Nr, acSfx, SIZ_S_UNITNO, pRec->Situs_Unit);
      else
         sprintf(acTmp, "%.36s %s", pRec->Situs_Nr, acSfx);

      blankRem(acTmp);
      iTmp = strlen(acTmp);
      if (iTmp > SIZ_S_ADDR_D)
         iTmp = SIZ_S_ADDR_D;
      memcpy(pOutbuf+OFF_S_ADDR_D, acTmp, iTmp);
   }
   // state
   memcpy(pOutbuf+OFF_S_ST, "CA", 2);

   // Find city
   memcpy(acTmp, pRec->Site_Area, SSIZ_SA);
   acTmp[SSIZ_SA] = 0;
   Abbr2Code(acTmp, acCode, acCity, pOutbuf);   
   if (acCode[0] > ' ')
   {
      memcpy(pOutbuf+OFF_S_CITY, acCode, strlen(acCode));
      iTmp = sprintf(acTmp, "%s CA", acCity);
      memcpy(pOutbuf+OFF_S_CTY_ST_D, acTmp, iTmp);
   } else  // Matching up with mailing addr to get city name
   if (!memcmp(pOutbuf+OFF_S_STRNUM, pOutbuf+OFF_M_STRNUM, SIZ_S_STRNUM) &&
       !memcmp(pOutbuf+OFF_S_STREET, pOutbuf+OFF_M_STREET, SIZ_S_STREET) &&
       *(pOutbuf+OFF_M_CITY) > ' ')
   {
      memcpy(acCity, pOutbuf+OFF_M_CITY, SIZ_M_CITY);
      myTrim(acCity, SIZ_M_CITY);
      City2Code(acCity, acCode);
      memcpy(pOutbuf+OFF_S_CITY, acCode, SIZ_M_CITY);
      memcpy(pOutbuf+OFF_S_ZIP, pOutbuf+OFF_M_ZIP, SIZ_S_ZIP);
   } else
   {
      memcpy(acTmp, pRec->TRA, 2);
      acTmp[2] = 0;
      iCode = XrefCode2Idx(&asTRA[0], acTmp, iNumTRA);
      if (iCode > 0)
      {
         iTmp = sprintf(acCode, "%d", iCode);
         memcpy(pOutbuf+OFF_S_CITY, acCode, iTmp);
         pTmp = XrefIdx2Name(&asTRA[0], iCode, iNumTRA);
         iTmp = sprintf(acTmp, "%s CA", pTmp);
         if (memcmp(acTmp, "     ", 5) && memcmp(acTmp, "BLANK", 5))
            memcpy(pOutbuf+OFF_S_CTY_ST_D, acTmp, iTmp);
      }
   }

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "5922500250", 10))
   //   iTmp = 0;
#endif

   // Merge sale
   //Ven_MergeRollSale(pOutbuf, pRollRec);

   // Owner
   if (memcmp(pRec->Name1, "     ", 5))
   {
      Ven_MergeOwner(pOutbuf, (char *)pRec->Name1, (char *)pRec->Name2);
   } else
      iRet |= 1;

   return iRet;
}

/******************************** Ven_MergeUpd2t *****************************
 *
 * "ParcelQuestDataExtract.txt" (11/10/2022)
 *
 * Return 0 if successful, >0 if error
 *
 *****************************************************************************/

int Ven_MergeUpdt2(char *pOutbuf, char *pRollRec, int iFlag)
{
   VEN_UPDT2 *pRec;
   char      *pTmp, acTmp[256], acTmp1[256], acCity[64], acCode[32], acSfx[16];
   int       iRet;
   long      lTmp, lBldgSqft, lSqFtLand;
   int       iTmp, iCode;

   pRec = (VEN_UPDT2 *)pRollRec;
   iRet = 0;

#ifdef _DEBUG
   //if (!memcmp(pRec->Apn, "5260144365", 9))
   //   iTmp = 0;
#endif
   if (iFlag & CREATE_R01)
   {
      // Clear output buffer
      memset(pOutbuf, ' ', iRecLen);

      // Start copying data
      memcpy(acTmp1, pRec->Apn, SSIZ_APN);

      // Save original APN as Alt APN
      memcpy(pOutbuf+OFF_ALT_APN, acTmp1, SSIZ_APN);

      // Reformat APN to CDDATA's format
      iTmp = Ven_FormatApn(acTmp1, acTmp);
      memcpy(pOutbuf, acTmp, iTmp);

      memcpy(pOutbuf+OFF_CO_NUM, "56VEN", 5);
      
      // Format APN for display
      iTmp = formatApn(acTmp, acTmp1, &myCounty);
      memcpy(pOutbuf+OFF_APN_D, acTmp1, iTmp);

      // Create MapLink and output new record
      iTmp = formatMapLink(acTmp, acTmp1, &myCounty);
      memcpy(pOutbuf+OFF_MAPLINK, acTmp1, iTmp);

      // Create index map link
      if (getIndexPage(acTmp1, acTmp, &myCounty))
         memcpy(pOutbuf+OFF_IMAPLINK, acTmp, iRet);
   }

   // TRA
   lTmp = atoin(pRec->TRA, RSIZ_TRA);
   if (lTmp > 0)
   {
      iTmp = sprintf(acTmp, "%.6d", lTmp);
      memcpy(pOutbuf+OFF_TRA, acTmp, iTmp);
   }

   // APN Status 
   // - T is active parcel being transfer
   // - V is voided/deleted parcel
   if (pRec->Apn_Status[0] == 'V')
      *(pOutbuf+OFF_STATUS) = pRec->Apn_Status[0];
   else
      *(pOutbuf+OFF_STATUS) = 'A';

   // Bldg Sqft
   lBldgSqft = atoin(pRec->BldgSqft, SSIZ_SQ_FT_I);
   if (lBldgSqft > 0)
   {
      sprintf(acTmp, "%*u", SIZ_BLDG_SF, lBldgSqft);
      memcpy(pOutbuf+OFF_BLDG_SF, acTmp, SIZ_BLDG_SF);
   }

   // Number of units
   memcpy(pOutbuf+OFF_UNITS, pRec->Tot_Units,SSIZ_TOT_UNITS);

   // LotSqft
   lSqFtLand = atoin(pRec->LotSqft, SSIZ_SQ_FT_L);
   if (lSqFtLand > 0)
   {
      sprintf(acTmp, "%*u", SIZ_LOT_SQFT, lSqFtLand);
      memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);
   }

   double dAcreAge = atofn(pRec->Acres, SSIZ_ACREAGE);
   if (dAcreAge > 0.0)
   {
      sprintf(acTmp, "%*u", SIZ_LOT_ACRES, (long)(dAcreAge*1000));
      memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);
      if (lSqFtLand == 0)
      {
         lSqFtLand = (long)(dAcreAge * SQFT_PER_ACRE);
         sprintf(acTmp, "%*u", SIZ_LOT_SQFT, lSqFtLand);
         memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);
      }
   } else if (lSqFtLand > 0)
   {
      dAcreAge = lSqFtLand/SQFT_PER_ACRE; 
      sprintf(acTmp, "%*u", SIZ_LOT_ACRES, (long)(dAcreAge*1000));
      memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);
   }

   // Mailing
   removeMailing(pOutbuf, false);

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "075320010", 9))
   //   iTmp = 0;
#endif
   ADR_REC sMailAdr;
   memset(&sMailAdr, 0, sizeof(ADR_REC));
   if (memcmp(pRec->M_Addr1, "     ", 5) && memcmp(pRec->M_Addr1, "BLANK", 5))
   {
      memcpy(acTmp, pRec->M_Addr1, SSIZ_MAIL_ADDR);
      blankRem(acTmp, RSIZ_M_STREET);
      vmemcpy(pOutbuf+OFF_M_ADDR_D, acTmp, SIZ_M_ADDR_D);

      // Parse mailing
      parseMAdr1(&sMailAdr, acTmp);

      if (sMailAdr.lStrNum > 0)
      {
         iTmp = sprintf(acTmp, "%d", sMailAdr.lStrNum);
         memcpy(pOutbuf+OFF_M_STRNUM, acTmp, iTmp);
      }
      if (sMailAdr.strSub[0] > ' ')
         vmemcpy(pOutbuf+OFF_M_STR_SUB, sMailAdr.strSub, SIZ_M_STR_SUB);
      if (sMailAdr.strDir[0] > ' ')
         vmemcpy(pOutbuf+OFF_M_DIR, sMailAdr.strDir, SIZ_M_DIR);
      vmemcpy(pOutbuf+OFF_M_STREET, sMailAdr.strName, SIZ_M_STREET);
      if (sMailAdr.strSfx[0] > ' ')
         vmemcpy(pOutbuf+OFF_M_SUFF, sMailAdr.strSfx, SIZ_M_SUFF);
      if (sMailAdr.Unit[0] > ' ')
      {
         vmemcpy(pOutbuf+OFF_M_UNITNO, sMailAdr.Unit, SIZ_M_UNITNO);
         vmemcpy(pOutbuf+OFF_M_UNITNOX, sMailAdr.UnitNox, SIZ_M_UNITNOX);
      }
   }

   memcpy(acTmp, pRec->M_CtySt, SSIZ_CTY_STA);
   myTrim(acTmp, SSIZ_CTY_STA);
   if (acTmp[0] > '0' && memcmp(acTmp, "     ", 5) && memcmp(acTmp, "BLANK", 5))
   {
      parseMAdr2(&sMailAdr, acTmp);
      vmemcpy(pOutbuf+OFF_M_CITY, sMailAdr.City, SIZ_M_CITY);
      vmemcpy(pOutbuf+OFF_M_ST, sMailAdr.State, SIZ_M_ST);
      // Ignore zip starts with XXX
      if (!memcmp(pRec->M_Zip, "XXX", 3) || !memcmp(pRec->M_Zip, "UNK", 3))
         memset(pOutbuf+OFF_M_ZIP, ' ', SIZ_M_ZIP+SIZ_M_ZIP4);
      else
      {
         if (sMailAdr.Zip[0] > ' ')
         {
            memcpy(pOutbuf+OFF_M_ZIP, sMailAdr.Zip, strlen(sMailAdr.Zip));
         } else if (pRec->M_Zip[0] > ' ')
         {
            memcpy(pOutbuf+OFF_M_ZIP, pRec->M_Zip, SIZ_M_ZIP);
            if (pRec->M_Zip[SIZ_M_ZIP] == '-')
               memcpy(pOutbuf+OFF_M_ZIP4, &pRec->M_Zip[SIZ_M_ZIP+1], SIZ_M_ZIP4);
            sprintf(acTmp, "%.*s %.*s", SSIZ_CTY_STA, pRec->M_CtySt, SSIZ_ZIP, pRec->M_Zip);
         }
      }
      iTmp = blankRem(acTmp);
      memcpy(pOutbuf+OFF_M_CTY_ST_D, acTmp, iTmp);
   }

   // Situs - StrNum field can be number street name
   removeSitus(pOutbuf);

   lTmp = atoin(pRec->Situs_Nr, SSIZ_SITUS_NR);
   if (lTmp > 0)
   {
      // Save original HSENO
      memcpy(pOutbuf+OFF_S_HSENO, pRec->Situs_Nr, SSIZ_SITUS_NR);

      iTmp = sprintf(acTmp, "%d", lTmp);
      memcpy(pOutbuf+OFF_S_STRNUM, acTmp, iTmp);
      memcpy(pOutbuf+OFF_S_DIR, pRec->Situs_Dir, SIZ_S_DIR);
      memcpy(pOutbuf+OFF_S_STREET, pRec->Situs_Street, SIZ_S_STREET);
      memcpy(acTmp, pRec->Situs_Type, SSIZ_SITUS_TYPE);
      acTmp[SSIZ_SITUS_TYPE] = 0;
      iTmp = GetSfxCodeX(acTmp, acSfx);
      if (iTmp > 0)
      {
         sprintf(acCode, "%d     ", iTmp); 
         memcpy(pOutbuf+OFF_S_SUFF, acCode, SIZ_S_SUFF);
      }
      memcpy(pOutbuf+OFF_S_UNITNO, pRec->Situs_Unit, SIZ_S_UNITNO);
      memcpy(pOutbuf+OFF_S_UNITNOX, pRec->Situs_Unit, SIZ_S_UNITNOX);

      if (pRec->Situs_Unit[0] > ' ')
         sprintf(acTmp, "%.36s %s #%.*s", pRec->Situs_Nr, acSfx, SIZ_S_UNITNO, pRec->Situs_Unit);
      else
         sprintf(acTmp, "%.36s %s", pRec->Situs_Nr, acSfx);

      blankRem(acTmp);
      vmemcpy(pOutbuf+OFF_S_ADDR_D, acTmp, SIZ_S_ADDR_D);
   }
   // state
   memcpy(pOutbuf+OFF_S_ST, "CA", 2);

   // Find city
   memcpy(acTmp, pRec->Site_Area, SSIZ_SA);
   acTmp[SSIZ_SA] = 0;
   Abbr2Code(acTmp, acCode, acCity, pOutbuf);   
   if (acCode[0] > ' ')
   {
      memcpy(pOutbuf+OFF_S_CITY, acCode, strlen(acCode));
      iTmp = sprintf(acTmp, "%s CA", acCity);
      memcpy(pOutbuf+OFF_S_CTY_ST_D, acTmp, iTmp);
   } else  // Matching up with mailing addr to get city name
   if (!memcmp(pOutbuf+OFF_S_STRNUM, pOutbuf+OFF_M_STRNUM, SIZ_S_STRNUM) &&
       !memcmp(pOutbuf+OFF_S_STREET, pOutbuf+OFF_M_STREET, SIZ_S_STREET) &&
       *(pOutbuf+OFF_M_CITY) > ' ')
   {
      memcpy(acCity, pOutbuf+OFF_M_CITY, SIZ_M_CITY);
      myTrim(acCity, SIZ_M_CITY);
      City2Code(acCity, acCode);
      memcpy(pOutbuf+OFF_S_CITY, acCode, SIZ_M_CITY);
      memcpy(pOutbuf+OFF_S_ZIP, pOutbuf+OFF_M_ZIP, SIZ_S_ZIP);
   } else
   {
      memcpy(acTmp, pRec->TRA, 2);
      acTmp[2] = 0;
      iCode = XrefCode2Idx(&asTRA[0], acTmp, iNumTRA);
      if (iCode > 0)
      {
         iTmp = sprintf(acCode, "%d", iCode);
         memcpy(pOutbuf+OFF_S_CITY, acCode, iTmp);
         pTmp = XrefIdx2Name(&asTRA[0], iCode, iNumTRA);
         iTmp = sprintf(acTmp, "%s CA", pTmp);
         if (memcmp(acTmp, "     ", 5) && memcmp(acTmp, "BLANK", 5))
            memcpy(pOutbuf+OFF_S_CTY_ST_D, acTmp, iTmp);
      }
   }

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "5922500250", 10))
   //   iTmp = 0;
#endif

   // Merge sale
   //Ven_MergeRollSale(pOutbuf, pRollRec);

   // Owner
   if (memcmp(pRec->Name1, "     ", 5))
   {
      Ven_MergeOwner(pOutbuf, (char *)pRec->Name1, (char *)pRec->Name2);
   } else
      iRet |= 1;

   return iRet;
}

/********************************** Ven_MergeRoll *****************************
 *
 * Return 0 if successful, >0 if error
 *
 *****************************************************************************/

//int Ven_MergeRoll(char *pOutbuf, char *pRollRec, int iLen, int iFlag)
//{
//   char     *pTmp, acTmp[256], acTmp1[256], acCity[64], acCode[32];
//   long     lBldgSqft, lSqFtLand, lGarSqft, lLand, lImpr;
//   LONGLONG lTmp;
//   int      iRet=0;
//   int      iBeds, iBath, iTmp, iCode;
//   char     acDocType[32];
//
//   VEN_ROLL *pRec = (VEN_ROLL *)pRollRec;
//
//#ifdef _DEBUG
//   //if (!memcmp(pRec, "6340220450", 10))
//   //   iTmp = 0;
//#endif
//
//   if (iFlag & CREATE_R01)
//   {
//      // Clear output buffer
//      memset(pOutbuf, ' ', iRecLen);
//
//      // Start copying data
//      memcpy(acTmp1, pRec->Apn, SSIZ_APN);
//
//      // Save original APN as Alt APN
//      memcpy(pOutbuf+OFF_ALT_APN, acTmp1, SSIZ_APN);
//
//      // Reformat APN to CDDATA's format
//      iTmp = Ven_FormatApn(acTmp1, acTmp);
//      memcpy(pOutbuf, acTmp, iTmp);
//
//      memcpy(pOutbuf+OFF_CO_NUM, "56VEN", 5);
//      
//      // Format APN for display
//      iRet = formatApn(acTmp, acTmp1, &myCounty);
//      memcpy(pOutbuf+OFF_APN_D, acTmp1, iRet);
//
//      // Create MapLink and output new record
//      iRet = formatMapLink(acTmp, acTmp1, &myCounty);
//      memcpy(pOutbuf+OFF_MAPLINK, acTmp1, iRet);
//   
//      // Create index map link
//      if (getIndexPage(acTmp1, acTmp, &myCounty))
//         memcpy(pOutbuf+OFF_IMAPLINK, acTmp, iRet);
//
//      // Year assessed
//      memcpy(pOutbuf+OFF_YR_ASSD, myCounty.acYearAssd, 4);
//
//      // HO Exempt
//      if (!memcmp(pRec->ExeCode1, "HO ", 3) || !memcmp(pRec->ExeCode2, "HO ", 3))
//         *(pOutbuf+OFF_HO_FL) = '1';      // Y
//      else
//         *(pOutbuf+OFF_HO_FL) = '2';      // N
//  
//      // Exe Amt
//      lTmp  = atoin(pRec->ExeValue1, RSIZ_EXEVALUE1); 
//      lTmp += atoin(pRec->ExeValue2, RSIZ_EXEVALUE1); 
//      lTmp += atoin(pRec->ExeValue3, RSIZ_EXEVALUE1); 
//      if (lTmp > 0)
//      {
//         sprintf(acTmp, "%*u", SIZ_EXE_TOTAL, lTmp);
//         memcpy(pOutbuf+OFF_EXE_TOTAL, acTmp, SIZ_EXE_TOTAL);
//      }
//
//      // Exe code
//      memcpy(pOutbuf+OFF_EXE_CD1, pRec->ExeCode1, SIZ_EXE_CD);
//      memcpy(pOutbuf+OFF_EXE_CD2, pRec->ExeCode2, SIZ_EXE_CD);
//      memcpy(pOutbuf+OFF_EXE_CD3, pRec->ExeCode3, SIZ_EXE_CD);
//
//      // Land
//      lLand = atoin(pRec->Land, RSIZ_LANDVALUE);
//      if (lLand > 0)
//      {
//         sprintf(acTmp, "%*u", SIZ_LAND, lLand);
//         memcpy(pOutbuf+OFF_LAND, acTmp, SIZ_LAND);
//      }
//
//      // Improve
//      lImpr = atoin(pRec->Impr, RSIZ_IMPROVEVALUE);
//      if (lImpr > 0)
//      {
//         sprintf(acTmp, "%*u", SIZ_IMPR, lImpr);
//         memcpy(pOutbuf+OFF_IMPR, acTmp, SIZ_IMPR);
//      }
//
//       // Other value
//      long lMineral = atoin(pRec->MineralValue, RSIZ_MINERALVALUE);
//      long lTree    = atoin(pRec->TreesValue, RSIZ_TREESVALUE);
//      long lFixt    = atoin(pRec->FixtureValue, RSIZ_TRADEVALUE);
//      long lPers    = atoin(pRec->PersonalValue, RSIZ_PERSONALVALUE);
//      lPers += atoin(pRec->UnitPPValue, RSIZ_PERSONALVALUE);
//      lFixt += atoin(pRec->UnitTFValue, RSIZ_TRADEVALUE);
//
//      lTmp = lMineral + lTree + lFixt + lPers;
//      if (lTmp > 0)
//      {
//         sprintf(acTmp, "%*u", SIZ_OTHER, lTmp);
//         memcpy(pOutbuf+OFF_OTHER, acTmp, SIZ_OTHER);
//
//         if (lMineral > 0)
//         {
//            sprintf(acTmp, "%u         ", lMineral);
//            memcpy(pOutbuf+OFF_MINERAL, acTmp, SIZ_MINERAL);
//         }
//         if (lTree > 0)
//         {
//            sprintf(acTmp, "%u         ", lTree);
//            memcpy(pOutbuf+OFF_TREEVINES, acTmp, SIZ_TREEVINES);
//         }
//         if (lFixt > 0)
//         {
//            sprintf(acTmp, "%u         ", lFixt);
//            memcpy(pOutbuf+OFF_FIXTR, acTmp, SIZ_FIXTR);
//         }
//         if (lPers > 0)
//         {
//            sprintf(acTmp, "%u         ", lPers);
//            memcpy(pOutbuf+OFF_PERSPROP, acTmp, SIZ_PERSPROP);
//         }
//      }
//
//      // Gross total
//      lTmp += lLand+lImpr;
//      if (lTmp > 0)
//      {
//         sprintf(acTmp, "%*u", SIZ_GROSS, lTmp);
//         memcpy(pOutbuf+OFF_GROSS, acTmp, SIZ_GROSS);
//      }
//
//      // Ratio
//      if (lImpr > 0)
//      {
//         sprintf(acTmp, "%*u", SIZ_RATIO, (LONGLONG)(lImpr*100/(lLand+lImpr) + 0.5) );
//         memcpy(pOutbuf+OFF_RATIO, acTmp, SIZ_RATIO);
//      }
//   } else
//      removeMailing(pOutbuf, false);
//
//   // TRA
//   lTmp = atoin(pRec->TRA, RSIZ_TRA);
//   if (lTmp > 0)
//   {
//      iRet = sprintf(acTmp, "%.6d", lTmp);
//      memcpy(pOutbuf+OFF_TRA, acTmp, iRet);
//   }
//
//   // Status
//   if (pRec->Apn_Status[0] == 'V')
//      *(pOutbuf+OFF_STATUS) = 'V';
//   else
//      *(pOutbuf+OFF_STATUS) = 'A';
//
//   // Set full exemption flag
//   if (pRec->NonTaxCode > '0')
//      *(pOutbuf+OFF_FULL_EXEMPT) = 'Y';
//   else
//      *(pOutbuf+OFF_FULL_EXEMPT) = ' ';
//
//   // Set MultiAPn flag
//   iTmp = atoin(pRec->NumParcels, RSIZ_NUM_PARCELS);
//   if (iTmp > 0)
//      *(pOutbuf+OFF_MULTI_APN) = 'Y';
//
//   // Yrblt
//   lTmp = atoin(pRec->YearBuilt, RSIZ_YEARBUILT);
//   if (lTmp > 1700)
//      memcpy(pOutbuf+OFF_YR_BLT, pRec->YearBuilt, SIZ_YR_BLT);
//
//   // Garage Sqft
//   lGarSqft = atoin(pRec->Gargage, RSIZ_GARGAGE);
//   if (lGarSqft > 10)
//   {
//      sprintf(acTmp, "%*u", SIZ_GAR_SQFT, lGarSqft);
//      memcpy(pOutbuf+OFF_GAR_SQFT, acTmp, SIZ_GAR_SQFT);
//      *(pOutbuf+OFF_PARK_TYPE) = 'Z';
//   } else
//   {
//      lGarSqft = atoin(pRec->Carport, RSIZ_GARGAGE);
//      if (lGarSqft > 10)
//      {
//         sprintf(acTmp, "%*u", SIZ_GAR_SQFT, lGarSqft);
//         memcpy(pOutbuf+OFF_GAR_SQFT, acTmp, SIZ_GAR_SQFT);
//         *(pOutbuf+OFF_PARK_TYPE) = 'C';
//      }
//   }
//
//   // Beds
//   iBeds = atoin(pRec->Beds, RSIZ_BEDS);
//   if (iBeds > 0)
//   {
//      sprintf(acTmp, "%*u", SIZ_BEDS, iBeds);
//      memcpy(pOutbuf+OFF_BEDS, acTmp, SIZ_BEDS);
//   }
//
//   // Bath
//   iBath = atoin(pRec->Baths, RSIZ_BATHS);
//   if (iBath > 0)
//   {
//      *(pOutbuf+OFF_BATH_F) = pRec->Baths[0];
//      *(pOutbuf+OFF_BATH_4Q) = pRec->Baths[0];
//
//      iTmp = atoin(&pRec->Baths[1], 2);
//      if (iTmp > 0)
//      {
//         *(pOutbuf+OFF_BATH_H) = '1';
//         if (iTmp == 50)
//            *(pOutbuf+OFF_BATH_2Q) = '1';
//         else if (iTmp > 50)
//            *(pOutbuf+OFF_BATH_3Q) = '1';
//         else if (iTmp < 50)
//            *(pOutbuf+OFF_BATH_1Q) = '1';
//      }
//   }
//   
//   // Total rooms
//   iTmp = atoin(pRec->TotalRooms, RSIZ_TOTALROOMS);
//   if (iTmp > 0)
//   {
//      sprintf(acTmp, "%*u", SIZ_ROOMS, iTmp);
//      memcpy(pOutbuf+OFF_ROOMS, acTmp, SIZ_ROOMS);
//   }
//
//   // Fireplace
//   memset(pOutbuf+OFF_FIRE_PL, ' ', SIZ_FIRE_PL);
//   iTmp = atoin(pRec->FP, RSIZ_FP);
//   if (iTmp > 9)
//      *(pOutbuf+OFF_FIRE_PL) = 'M';
//   else if (iTmp > 0)
//      *(pOutbuf+OFF_FIRE_PL) = '0' | iTmp;
//   else if (pRec->FP[0] > '0')
//      LogMsg("*** Bad FirePlace %.*s [%.*s]", RSIZ_FP, pRec->FP, iApnLen, pRec->Apn);
//
//   // Pool
//   if (pRec->Pool[0] > '0')
//      *(pOutbuf+OFF_POOL) = 'P';
//
//   // Stories
//   int iStories=0;
//   int iFlr3 = atoin(pRec->Floor3, RSIZ_FLOOR1);
//   int iFlr2 = atoin(pRec->Floor2, RSIZ_FLOOR1);
//   int iFlr1 = atoin(pRec->Floor1, RSIZ_FLOOR1);
//   if (iFlr1 > 0)
//   {
//      iStories = 1;
//      if (iFlr2 > 0)
//      {
//         iStories = 2;
//         if (iFlr3 > 0)
//            iStories = 3;
//      }
//      sprintf(acTmp, "%d.0  ", iStories);
//      memcpy(pOutbuf+OFF_STORIES, acTmp, SIZ_STORIES);
//   }
//
//   // Bldg Sqft = BldgSize+Addition1+Addition2 (new)
//   // Bldg Sqft = BldgSize+Basement (old)
//   lBldgSqft = iFlr1+iFlr2+iFlr3;
//   lBldgSqft += atoin(pRec->Addition1, RSIZ_ADDITION1);
//   lBldgSqft += atoin(pRec->Addition2, RSIZ_ADDITION1);
//   if (lBldgSqft > 0)
//   {
//      sprintf(acTmp, "%*u", SIZ_BLDG_SF, lBldgSqft);
//      memcpy(pOutbuf+OFF_BLDG_SF, acTmp, SIZ_BLDG_SF);
//   }
//
//   // Basement area
//   lTmp = atoin(pRec->Basement, RSIZ_BASEMENT);
//   if (lTmp > 0)
//   {
//      sprintf(acTmp, "%*u", SIZ_BSMT_SF, lTmp);
//      memcpy(pOutbuf+OFF_BSMT_SF, acTmp, SIZ_BSMT_SF);
//   }
//
//   // First floor
//   lTmp = atoin(pRec->Floor1, RSIZ_FLOOR1);
//   if (lTmp > 0)
//   {
//      sprintf(acTmp, "%*u", SIZ_FLOOR_SF, lTmp);
//      memcpy(pOutbuf+OFF_FLOOR1_SF, acTmp, SIZ_FLOOR_SF);
//   }
//
//   // Second floor
//   lTmp = atoin(pRec->Floor2, RSIZ_FLOOR1);
//   if (lTmp > 0)
//   {
//      sprintf(acTmp, "%*u", SIZ_FLOOR_SF, lTmp);
//      memcpy(pOutbuf+OFF_FLOOR2_SF, acTmp, SIZ_FLOOR_SF);
//   }
//
//   // Third floor
//   lTmp = atoin(pRec->Floor3, RSIZ_FLOOR1);
//   if (lTmp > 0)
//   {
//      sprintf(acTmp, "%*u", SIZ_FLOOR_SF, lTmp);
//      memcpy(pOutbuf+OFF_FLOOR3_SF, acTmp, SIZ_FLOOR_SF);
//   }
//
//   // Patio
//   lTmp = atoin(pRec->Patio, RSIZ_PATIO);
//   if (lTmp > 0)
//   {
//      sprintf(acTmp, "%*u", SIZ_PATIO_SF, lTmp);
//      memcpy(pOutbuf+OFF_PATIO_SF, acTmp, SIZ_PATIO_SF);
//   }
//
//   // Deck
//   lTmp = atoin(pRec->Deck, RSIZ_DECK);
//   if (lTmp > 0)
//   {
//      sprintf(acTmp, "%*u", SIZ_DECK_SF, lTmp);
//      memcpy(pOutbuf+OFF_DECK_SF, acTmp, SIZ_DECK_SF);
//   }
//
//   // Porch
//   lTmp = atoin(pRec->Porch, RSIZ_PORCH);
//   if (lTmp > 0)
//   {
//      sprintf(acTmp, "%*u", SIZ_PORCH_SF, lTmp);
//      memcpy(pOutbuf+OFF_PORCH_SF, acTmp, SIZ_PORCH_SF);
//   }
//
//   // LotSqft
//   lSqFtLand = atoin(pRec->LotSqft, RSIZ_LOT_SQFT);
//   if (lSqFtLand > 0)
//   {
//      sprintf(acTmp, "%*u", SIZ_LOT_SQFT, lSqFtLand);
//      memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);
//   } 
//
//   double dAcreAge = atofn(pRec->Acres, RSIZ_ACREAGE);
//   if (dAcreAge > 0.0)
//   {
//      sprintf(acTmp, "%*u", SIZ_LOT_ACRES, (long)(dAcreAge*1000));
//      memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);
//      if (lSqFtLand == 0)
//      {
//         lSqFtLand = (long)(dAcreAge * SQFT_PER_ACRE);
//         sprintf(acTmp, "%*u", SIZ_LOT_SQFT, lSqFtLand);
//         memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);
//      }
//   } else if (lSqFtLand > 0)
//   {
//      dAcreAge = lSqFtLand/SQFT_PER_ACRE; 
//      sprintf(acTmp, "%*u", SIZ_LOT_ACRES, (long)(dAcreAge*1000));
//      memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);
//   }
//
//   // Use
//   if (pRec->UseCode[0] > ' ')
//   {
//      memcpy(pOutbuf+OFF_USE_CO, pRec->UseCode, RSIZ_USECODE);
//      updateStdUse(pOutbuf+OFF_USE_STD, pRec->UseCode, RSIZ_USECODE, pOutbuf);
//   } else
//      memcpy(pOutbuf+OFF_USE_STD, USE_UNASGN, SIZ_USE_STD);
//
//   iTmp = XrefCode2Idx((XREFTBL *)&asDeed[0], pRec->DocType, iNumDeeds);
//   if (iTmp > 0)
//   {
//      iTmp = sprintf(acDocType, "%d     ", iTmp);
//      memcpy(pOutbuf+OFF_SALE1_DOCTYPE, acDocType, iTmp);
//   }
//
//   // Legal Desc
//   memcpy(pOutbuf+OFF_TRACT, pRec->TractNo, RSIZ_TRACTNO);
//   memcpy(pOutbuf+OFF_BLOCK, pRec->BlockNo, RSIZ_BLOCKNO);
//   if (pRec->LotNo[0] > '0' && pRec->LotNo[0] <= 'Z')
//      memcpy(pOutbuf+OFF_LOT, pRec->LotNo, RSIZ_LOTNO);
//
//   //memset(pOutbuf+OFF_LEGAL, ' ', SIZ_LEGAL);
//   acTmp[0] = 0;
//   if (memcmp(pRec->TractNo, "      ", RSIZ_TRACTNO))
//   {
//      memcpy(acTmp1, pRec->TractNo, RSIZ_TRACTNO);
//      myTrim(acTmp1, RSIZ_TRACTNO);
//      strcat(acTmp, " TR ");
//      strcat(acTmp, acTmp1);
//   } 
//
//   if (memcmp(pRec->BlockNo, "  ", RSIZ_BLOCKNO))
//   {
//      memcpy(acTmp1, pRec->BlockNo, RSIZ_BLOCKNO);
//      myTrim(acTmp1, RSIZ_BLOCKNO);
//      strcat(acTmp, " BLK ");
//      strcat(acTmp, acTmp1);
//   } 
//
//   if (pRec->LotNo[0] > '0' && pRec->LotNo[0] <= 'Z')
//   {
//      memcpy(acTmp1, pRec->LotNo, RSIZ_LOTNO);
//      myTrim(acTmp1, RSIZ_LOTNO);
//      strcat(acTmp, " LT ");
//      strcat(acTmp, acTmp1);
//   } 
//
//   if (memcmp(pRec->CondoUnit, "     ", RSIZ_CONDOUNIT))
//   {
//      memcpy(acTmp1, pRec->CondoUnit, RSIZ_CONDOUNIT);
//      myTrim(acTmp1, RSIZ_CONDOUNIT);
//      strcat(acTmp, " UNT ");
//      strcat(acTmp, acTmp1);
//   } 
//
//   if (memcmp(pRec->MapNo, "          ", RSIZ_MAPNO))
//   {
//      memcpy(acTmp1, pRec->MapNo, RSIZ_MAPNO);
//      myTrim(acTmp1, RSIZ_MAPNO);
//      strcat(acTmp, " MP REF ");
//      strcat(acTmp, acTmp1);
//   } 
//
//   if (memcmp(pRec->CondoRef, "         ", RSIZ_CONDOREF))
//   {
//      memcpy(acTmp1, pRec->CondoRef, RSIZ_CONDOREF);
//      myTrim(acTmp1, RSIZ_CONDOREF);
//      strcat(acTmp, " CNDO REF ");
//      strcat(acTmp, acTmp1);
//   } 
//
//   if (memcmp(pRec->CondoBldg, "    ", RSIZ_CONDOBLDG))
//   {
//      memcpy(acTmp1, pRec->CondoBldg, RSIZ_CONDOBLDG);
//      myTrim(acTmp1, RSIZ_CONDOBLDG);
//      strcat(acTmp, " BLDG ");
//      strcat(acTmp, acTmp1);
//   } 
//
//   if (memcmp(pRec->PrevAPN, "          ", RSIZ_PREVAPN))
//   {
//      memcpy(acTmp1, pRec->PrevAPN, RSIZ_PREVAPN);
//      myTrim(acTmp1, RSIZ_PREVAPN);
//      strcat(acTmp, " PR APN ");
//      strcat(acTmp, acTmp1);
//   } 
//
//   // Fixup Legal
//   if (acTmp[0] > 0)
//      updateLegal(pOutbuf, acTmp);
//
//#ifdef _DEBUG
//   //if (!memcmp(pOutbuf, "801161015", 9))
//   //   iTmp = 0;
//#endif
//   ADR_REC sMailAdr;
//   memset(&sMailAdr, 0, sizeof(ADR_REC));
//   if (memcmp(pRec->M_Street, "     ", 5) && memcmp(pRec->M_Street, "BLANK", 5))
//   {
//      memcpy(acTmp, pRec->M_Street, RSIZ_M_STREET);
//      iTmp = blankRem(acTmp, RSIZ_M_STREET);
//      vmemcpy(pOutbuf+OFF_M_ADDR_D, acTmp, SIZ_M_ADDR_D);
//
//      // Parse mailing
//      parseMAdr1(&sMailAdr, acTmp);
//
//      if (sMailAdr.lStrNum > 0)
//      {
//         iTmp = sprintf(acTmp, "%d", sMailAdr.lStrNum);
//         memcpy(pOutbuf+OFF_M_STRNUM, acTmp, iTmp);
//      }
//      if (sMailAdr.strSub[0] > ' ')
//         memcpy(pOutbuf+OFF_M_STR_SUB, sMailAdr.strSub, strlen(sMailAdr.strSub));
//      if (sMailAdr.strDir[0] > ' ')
//         memcpy(pOutbuf+OFF_M_DIR, sMailAdr.strDir, strlen(sMailAdr.strDir));
//      memcpy(pOutbuf+OFF_M_STREET, sMailAdr.strName, strlen(sMailAdr.strName));
//      if (sMailAdr.strSfx[0] > ' ')
//         memcpy(pOutbuf+OFF_M_SUFF, sMailAdr.strSfx, strlen(sMailAdr.strSfx));
//      if (sMailAdr.Unit[0] > ' ')
//         memcpy(pOutbuf+OFF_M_UNITNO, sMailAdr.Unit, strlen(sMailAdr.Unit));
//   }
//
//   memcpy(acTmp, pRec->M_City, RSIZ_M_CITY);
//   myTrim(acTmp, RSIZ_M_CITY);
//   if (memcmp(acTmp, "BLANK", 5))
//   {
//      parseMAdr2(&sMailAdr, acTmp);
//      iTmp = strlen(sMailAdr.City);
//      if (iTmp > SIZ_M_CITY) iTmp = SIZ_M_CITY;
//      memcpy(pOutbuf+OFF_M_CITY, sMailAdr.City, iTmp);
//      memcpy(pOutbuf+OFF_M_ST, sMailAdr.State, SIZ_M_ST);
//   }
//
//   if (memcmp(pRec->M_Zip, "  ", 2) && 
//       memcmp(pRec->M_Zip, "BLANK", 5) && 
//       memcmp(pRec->M_Zip, "XX", 2))
//   {
//      memcpy(pOutbuf+OFF_M_ZIP, pRec->M_Zip, SIZ_M_ZIP);
//      if (pRec->M_Zip[5] == '-')
//         memcpy(pOutbuf+OFF_M_ZIP4, pRec->M_Zip+6, SIZ_M_ZIP4);
//      else
//         memcpy(pOutbuf+OFF_M_ZIP4, BLANK32, SIZ_M_ZIP4);
//
//      if (memcmp(pRec->M_City, "     ", 5) && memcmp(pRec->M_City, "BLANK", 5))
//         memcpy(pOutbuf+OFF_M_CTY_ST_D, pRec->M_City, RSIZ_M_CITY);
//   }   
//
//   // Situs - StrNum field can be number street name
//   removeSitus(pOutbuf);
//
//   lTmp = atoin(pRec->S_StreetNum, RSIZ_S_STREETNUM); 
//   if (lTmp > 0)
//   {
//      char acSfx[8];
//
//      // Save original HSENO
//      memcpy(pOutbuf+OFF_S_HSENO, pRec->S_StreetNum, RSIZ_S_STREETNUM);
//      // Found 200 situs with '-'
//      //if (isCharIncluded(pRec->S_StreetNum, '-', RSIZ_S_STREETNUM))
//      //   lRecCnt++;
//
//      iTmp = sprintf(acTmp, "%d", lTmp);
//      memcpy(pOutbuf+OFF_S_STRNUM, acTmp, iTmp);
//      if (isalpha(pRec->S_Dir[0]))
//      {
//         if (isDirx(pRec->S_Dir))
//            memcpy(pOutbuf+OFF_S_DIR, pRec->S_Dir, SIZ_S_DIR);
//         else if (!memcmp(pRec->S_Dir, "SO", 2))
//            *(pOutbuf+OFF_S_DIR) = 'S';
//         else if (pRec->S_Dir[1] == ' ' && pRec->S_StreetName[0] == ' ')
//            pRec->S_StreetName[0] = pRec->S_Dir[0];
//         else
//            LogMsg("*** Invalid direction: %.2s [%.10s]", pRec->S_Dir, pOutbuf);
//      }
//
//      memcpy(pOutbuf+OFF_S_STREET, pRec->S_StreetName, SIZ_S_STREET);
//      memcpy(acTmp, pRec->S_Type, RSIZ_S_TYPE);
//      acTmp[RSIZ_S_TYPE] = 0;
//      iTmp = GetSfxCodeX(acTmp, acSfx);
//      if (iTmp > 0)
//      {
//         sprintf(acCode, "%d     ", iTmp); 
//         memcpy(pOutbuf+OFF_S_SUFF, acCode, SIZ_S_SUFF);
//      }
//
//      if (pRec->S_UnitNo[0] > ' ')
//      {
//         sprintf(acTmp, "%.36s %s #%.*s", pRec->S_StreetNum, acSfx, SIZ_S_UNITNO, pRec->S_UnitNo);
//         memcpy(pOutbuf+OFF_S_UNITNO, pRec->S_UnitNo, SIZ_S_UNITNO);
//      } else
//         sprintf(acTmp, "%.36s %s", pRec->S_StreetNum, acSfx);
//
//      blankRem(acTmp);
//      vmemcpy(pOutbuf+OFF_S_ADDR_D, acTmp, SIZ_S_ADDR_D);
//   }
//   // state
//   memcpy(pOutbuf+OFF_S_ST, "CA", 2);
//
//   // Find city
//   memcpy(acTmp, pRec->SiteArea, RSIZ_SITEAREA);
//   acTmp[SSIZ_SA] = 0;
//   Abbr2Code(acTmp, acCode, acCity, pOutbuf);   
//   if (acCode[0] > ' ')
//   {
//      memcpy(pOutbuf+OFF_S_CITY, acCode, strlen(acCode));
//      strcat(acCity, " CA");
//      iTmp = blankRem(acCity);
//      vmemcpy(pOutbuf+OFF_S_CTY_ST_D, acCity, SIZ_S_CTY_ST_D, iTmp);
//   } else  // Matching up with mailing addr to get city name
//   if (!memcmp(pOutbuf+OFF_S_STRNUM, pOutbuf+OFF_M_STRNUM, SIZ_S_STRNUM) &&
//       !memcmp(pOutbuf+OFF_S_STREET, pOutbuf+OFF_M_STREET, SIZ_S_STREET) &&
//       *(pOutbuf+OFF_M_CITY) > ' ')
//   {
//      memcpy(acCity, pOutbuf+OFF_M_CITY, SIZ_M_CITY);
//      myTrim(acCity, SIZ_M_CITY);
//      City2Code(acCity, acCode, pOutbuf);
//      memcpy(pOutbuf+OFF_S_CITY, acCode, SIZ_S_CITY);
//      memcpy(pOutbuf+OFF_S_ZIP, pOutbuf+OFF_M_ZIP, SIZ_S_ZIP);
//   } else
//   {
//      memcpy(acTmp, pRec->TRA, 2);
//      acTmp[2] = 0;
//      iCode = XrefCode2Idx(&asTRA[0], acTmp, iNumTRA);
//      if (iCode > 0)
//      {
//         iTmp = sprintf(acCode, "%d", iCode);
//         memcpy(pOutbuf+OFF_S_CITY, acCode, iTmp);
//         pTmp = XrefIdx2Name(&asTRA[0], iCode, iNumTRA);
//         iTmp = sprintf(acTmp, "%s CA", pTmp);
//         if (memcmp(acTmp, "     ", 5) && memcmp(acTmp, "BLANK", 5))
//            memcpy(pOutbuf+OFF_S_CTY_ST_D, acTmp, iTmp);
//      }
//   }
//
//#ifdef _DEBUG
//   //if (!memcmp(pRec, "0520270025", 10))
//   //   iTmp = 0;
//#endif
//
//   // Owner
//   if (memcmp(pRec->Name1, "     ", 5))
//   {
//      Ven_MergeOwner(pOutbuf, (char *)pRec->Name1, (char *)pRec->Name2);
//   } else
//      iRet |= 1;
//
//   /*
//   // Sales data
//   double dSaleAmt;
//
//   if (pRec->DttAmt[0] > ' ')
//   {
//      dSaleAmt = (atofn(pRec->DttAmt, RSIZ_DTT_AMT) / 1.1) * 1000 + 0.5;
//      if (dSaleAmt > 0.0)
//      {
//         sprintf(acTmp, "%*u", SIZ_SALE1_AMT, (long)(dSaleAmt));
//         memcpy(pOutbuf+OFF_SALE1_AMT, acTmp, SIZ_SALE1_AMT);
//      }
//   } else
//      memset(pOutbuf+OFF_SALE1_AMT, ' ', SIZ_SALE1_AMT);
//
//   // Doc Date
//   memcpy(pOutbuf+OFF_TRANSFER_DT, pRec->Doc_Dt, RSIZ_DOC_DT);
//   memcpy(pOutbuf+OFF_SALE1_DT, pRec->Doc_Dt, RSIZ_DOC_DT);
//
//   // Doc No
//   if (!memcmp(pRec->DocNum, "000000000", 9))
//   {
//      memcpy(pOutbuf+OFF_TRANSFER_DOC, BLANK32, RSIZ_DOCNUM);
//      memcpy(pOutbuf+OFF_SALE1_DOC, BLANK32, RSIZ_DOCNUM);
//   } else
//   {
//      memcpy(pOutbuf+OFF_TRANSFER_DOC, pRec->DocNum, RSIZ_DOCNUM);
//      memcpy(pOutbuf+OFF_SALE1_DOC, pRec->DocNum, RSIZ_DOCNUM);
//      *(pOutbuf+OFF_AR_CODE1) = 'A';    // Assessor   
//   }
//   */
//   return iRet;
//}

/********************************* Ven_MergeMAdr ****************************
 *
 * ATTN THE NORTHERN TRUST CO TTEE, 201 S LAKE AV, STE 600, PASADENA, CA 91101 
 * 1690 TANAGER ST, APT 301, VENTURA, CA 93003
 * 10850 CHURCH ST APT P201, RANCHO CUCAMONGA CA 91730-8044
 * ATTN L W PENNINGTON TTEE, 148 ELENA ST APT J, SANTA FE NM 87501
 * ATTN ALEX P FREIBERG TTEE, 6947 KINGLET ST, VENTURA CA 93003-7139
 * ATTN TAX COLLECTOR'S-UNKNOWN ADDRE, 800 S VICTORIA, VENTURA CA 93009
 * C/O TOM AND TERRI ECKERT, 11475 N CORRIENTE CR, PRESCOTT, AZ 86305
 * 25152 SPRINGFIELD CT STE 180 (0753200100)
 *
 ****************************************************************************/

int Ven_MergeMAdr(char *pOutbuf)
{
   char  acTmp[256], sFullAddr[256], sAddr1[128], sAddr2[128], *apItems[8], *pTmp;
   int   iTmp, iCnt;

   ADR_REC sMailAdr;
   memset(&sMailAdr, 0, sizeof(ADR_REC));

   strcpy(sFullAddr, apTokens[VEN_L_FULLMAIL]);
   iCnt = ParseStringIQ(sFullAddr, ',', 8, apItems);
   for (iTmp = 0; iTmp < iCnt; iTmp++)
      lTrim(apItems[iTmp]);

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "0753200100", 10))
   //   iTmp = 0;
#endif

   if (!memcmp(apItems[0], "C/O", 3) || !memcmp(apItems[0], "ATT", 3))
   {
      if (iCnt == 2)
      {
         // ATTN: CNN ABUDHABIPO BOX 77753, ABU DHABI UAE
         // ATTN: CATHERINE KING TTEE PO BOX 14, FILLMORE CA 93016
         // 0690020315: C/O ALTUS GROUP PO BOX 71970, PHOENIX AZ 85050
         if (pTmp = strstr(apItems[0], "PO BOX"))
         {
            strcpy(sAddr1, pTmp);
            strcpy(sAddr2, apItems[1]);            
            *pTmp = 0;
         } else
         {
            // ATTN: KD STRVZESKI 270 QUAIL ST STE B, SANTA PAULA CA 93060
            pTmp = apItems[0];
            while (*pTmp && !isdigit(*pTmp)) 
               pTmp++;
            if (*pTmp > '0' && *pTmp <= '9')
            {
               strcpy(sAddr1, pTmp);
               *(pTmp-1) = 0;
               strcpy(sAddr2, apItems[1]);
            } else
            {
               strcpy(sAddr1, apItems[1]);
               sAddr2[0] = 0;
            }
         }
         updateCareOf(pOutbuf, apItems[0]);
      } else if (iCnt == 1)
      {
         if (pTmp = strstr(apItems[0], "PO BOX"))
         {
            strcpy(sAddr1, pTmp);
            //strcpy(sAddr2, apItems[1]);            
            sAddr2[0] = 0;
            *pTmp = 0;
         } else
         {
            // ATTN  TAX DEPARTMENT 27200 TOURNEY RD STE 315 SANTA CLARITA CA 91355
            pTmp = apItems[0];
            while (*pTmp && !isdigit(*pTmp)) 
               pTmp++;
            if (*pTmp > '0' && *pTmp <= '9')
            {
               strcpy(acTmp, pTmp);
               *(pTmp-1) = 0;
               updateCareOf(pOutbuf, apItems[0]);
               if (pTmp = strstr(acTmp, "SANTA"))
               {
                  *(pTmp-1) = 0;
                  strcpy(sAddr1, acTmp);
                  strcpy(sAddr2, pTmp);
               } else
               {
                  strcpy(sAddr1, acTmp);
                  sAddr2[0] = 0;
               }
            } else
            {
               strcpy(sAddr1, apItems[1]);
               sAddr2[0] = 0;
            }
         }
      } else
      {
         updateCareOf(pOutbuf, apItems[0]);
         strcpy(sAddr1, apItems[1]);
      }
      if (iCnt > 3)
      {
         if (!memcmp(apItems[1], "STE", 3) || !memcmp(apItems[1], "APT", 3))
         {
            sprintf(sAddr1, "%s %s", apItems[0], apItems[1]);
            if (iCnt > 4)
               sprintf(sAddr2, "%s %s", apItems[3], apItems[4]);
            else
               sprintf(sAddr2, "%s %s", apItems[2], apItems[3]);
         } else if (!memcmp(apItems[2], "STE", 3) || !memcmp(apItems[2], "APT", 3))
         {
            sprintf(sAddr1, "%s %s", apItems[1], apItems[2]);
            if (iCnt > 4)
               sprintf(sAddr2, "%s %s", apItems[3], apItems[4]);
            else
               strcpy(sAddr2, apItems[3]);
         } else
         {
            sprintf(sAddr2, "%s %s", apItems[2], apItems[3]);
         }
      } else if (iCnt == 3)
         strcpy(sAddr2, apItems[2]);
   } else
   {
      strcpy(sAddr1, apItems[0]);
      if (iCnt >= 3)
      {
         if (!memcmp(apItems[1], "STE", 3) || !memcmp(apItems[1], "APT", 3))
         {
            sprintf(sAddr1, "%s %s", apItems[0], apItems[1]);
            // 530 E LOS ANGELES AV, STE 115, PO BOX 163 (6510211235)
            if (iCnt > 4)
               sprintf(sAddr2, "%s %s", apItems[3], apItems[4]);
            else if (iCnt > 3)
               sprintf(sAddr2, "%s %s", apItems[2], apItems[3]);
            else
               strcpy(sAddr2, apItems[2]);
         } else
         {
            sprintf(sAddr2, "%s %s", apItems[1], apItems[2]);
         }
      } else
      {
         strcpy(sAddr2, apItems[1]);
      }
   }

   blankRem(sAddr1);
   blankRem(sAddr2);

   // Save original Addr
   vmemcpy(pOutbuf+OFF_M_ADDR_D, sAddr1, SIZ_M_ADDR_D);
   vmemcpy(pOutbuf+OFF_M_CTY_ST_D, sAddr2, SIZ_M_CTY_ST_D);

   // Parse mailing
   parseMAdr1(&sMailAdr, sAddr1);

   if (sMailAdr.lStrNum > 0)
   {
      iTmp = sprintf(acTmp, "%d", sMailAdr.lStrNum);
      memcpy(pOutbuf+OFF_M_STRNUM, acTmp, iTmp);
   }
   if (sMailAdr.strSub[0] > ' ')
      memcpy(pOutbuf+OFF_M_STR_SUB, sMailAdr.strSub, strlen(sMailAdr.strSub));
   if (sMailAdr.strDir[0] > ' ')
      memcpy(pOutbuf+OFF_M_DIR, sMailAdr.strDir, strlen(sMailAdr.strDir));
   vmemcpy(pOutbuf+OFF_M_STREET, sMailAdr.strName, SIZ_M_STREET);
   if (sMailAdr.strSfx[0] > ' ')
      memcpy(pOutbuf+OFF_M_SUFF, sMailAdr.strSfx, strlen(sMailAdr.strSfx));
   if (sMailAdr.Unit[0] > ' ')
   {
      vmemcpy(pOutbuf+OFF_M_UNITNO, sMailAdr.Unit, SIZ_M_UNITNO);
      vmemcpy(pOutbuf+OFF_M_UNITNOX, sMailAdr.UnitNox, SIZ_M_UNITNOX);
   }

   if (sAddr2[0] > ' ')
   {
      parseMAdr2(&sMailAdr, sAddr2);
      vmemcpy(pOutbuf+OFF_M_CITY, sMailAdr.City, SIZ_M_CITY);
      vmemcpy(pOutbuf+OFF_M_ST, sMailAdr.State, SIZ_M_ST);
      if (sMailAdr.Zip[4] != '-')
         vmemcpy(pOutbuf+OFF_M_ZIP, sMailAdr.Zip, SIZ_M_ZIP);
      else
      {
         if (!memcmp(sMailAdr.State, "CA", 2) && sMailAdr.Zip[0] != '9')
         {
            memcpy(pOutbuf+OFF_M_ZIP+1, sMailAdr.Zip, 4);
            *(pOutbuf+OFF_M_ZIP) = '9';
         } else
            LogMsg("*** Bad mailzip: %s APN=%.12s", sMailAdr.Zip, pOutbuf);
      }
   }
   return 0;
}

/******************************** Ven_MergeMAdr1 ****************************
 *
 * This function to process situs from  "2023 Secured Roll.csv"
 *
 ****************************************************************************/

int Ven_MergeMAdr1(char *pOutbuf)
{
   char  acTmp[256], sFullAddr[256], sAddr1[128], sAddr2[128], *apItems[8];
   int   iTmp, iCnt;

   ADR_REC sMailAdr;
   memset(&sMailAdr, 0, sizeof(ADR_REC));
   strcpy(sAddr1, apTokens[VEN_L1_MAIL_ADDR]);
   strcpy(sAddr2, apTokens[VEN_L1_CTY_STA]);
   blankRem(sAddr1);

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "0753200100", 10))
   //   iTmp = 0;
#endif

   if (!memcmp(apTokens[VEN_L1_MAIL_ADDR], "C/O ", 4) || !memcmp(apTokens[VEN_L1_MAIL_ADDR], "ATTN:", 5))
   {
      updateCareOf(pOutbuf, sAddr1);
      sAddr1[0] = 0;
   } else if (memcmp(sAddr1, "PO BOX", 4))
   {
      strcpy(sFullAddr, sAddr1);
      
      iCnt = ParseStringIQ(sFullAddr, ' ', 8, apItems);
      for (iTmp = 0; iTmp < iCnt; iTmp++)
         lTrim(apItems[iTmp]);

      // Parse mailing
      parseMAdr1(&sMailAdr, sAddr1);

      if (sMailAdr.lStrNum > 0)
      {
         iTmp = sprintf(acTmp, "%d", sMailAdr.lStrNum);
         memcpy(pOutbuf+OFF_M_STRNUM, acTmp, iTmp);
      }
      if (sMailAdr.strSub[0] > ' ')
         memcpy(pOutbuf+OFF_M_STR_SUB, sMailAdr.strSub, strlen(sMailAdr.strSub));
      if (sMailAdr.strDir[0] > ' ')
         memcpy(pOutbuf+OFF_M_DIR, sMailAdr.strDir, strlen(sMailAdr.strDir));
      memcpy(pOutbuf+OFF_M_STREET, sMailAdr.strName, strlen(sMailAdr.strName));
      if (sMailAdr.strSfx[0] > ' ')
         memcpy(pOutbuf+OFF_M_SUFF, sMailAdr.strSfx, strlen(sMailAdr.strSfx));
      if (sMailAdr.Unit[0] > ' ')
      {
         vmemcpy(pOutbuf+OFF_M_UNITNO, sMailAdr.Unit, SIZ_M_UNITNO);
         vmemcpy(pOutbuf+OFF_M_UNITNOX, sMailAdr.UnitNox, SIZ_M_UNITNOX);
      }
   } else
      memcpy(pOutbuf+OFF_M_STREET, sAddr1, strlen(sAddr1));


   if (*apTokens[VEN_L1_ZIP] > ' ')
      sprintf(sAddr2, "%s %s", apTokens[VEN_L1_CTY_STA], apTokens[VEN_L1_ZIP]);

   // Save original Addr
   if (sAddr1[0] > ' ')
      vmemcpy(pOutbuf+OFF_M_ADDR_D, sAddr1, SIZ_M_ADDR_D);

   if (sAddr2[0] > ' ')
   {
      vmemcpy(pOutbuf+OFF_M_CTY_ST_D, sAddr2, SIZ_M_CTY_ST_D);
      parseMAdr2(&sMailAdr, sAddr2);
      vmemcpy(pOutbuf+OFF_M_CITY, sMailAdr.City, SIZ_M_CITY);
      vmemcpy(pOutbuf+OFF_M_ST, sMailAdr.State, SIZ_M_ST);
      if (sMailAdr.Zip[4] != '-')
         vmemcpy(pOutbuf+OFF_M_ZIP, sMailAdr.Zip, SIZ_M_ZIP);
      else
      {
         if (!memcmp(sMailAdr.State, "CA", 2) && sMailAdr.Zip[0] != '9')
         {
            memcpy(pOutbuf+OFF_M_ZIP+1, sMailAdr.Zip, 4);
            *(pOutbuf+OFF_M_ZIP) = '9';
         } else
            LogMsg("*** Bad mailzip: %s APN=%.12s", sMailAdr.Zip, pOutbuf);
      }
   }
   return 0;
}

int Ven_MergeMAdr3(char *pOutbuf)
{
   char  acTmp[256], sFullAddr[256], sAddr1[128], sAddr2[128], *apItems[8];
   int   iTmp, iCnt;

   ADR_REC sMailAdr;
   memset(&sMailAdr, 0, sizeof(ADR_REC));
   strcpy(sAddr1, apTokens[VEN_L3_M_ADDR]);
   strcpy(sAddr2, apTokens[VEN_L3_M_CTYST]);
   blankRem(sAddr1);

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "0753200100", 10))
   //   iTmp = 0;
#endif

   if (!memcmp(apTokens[VEN_L3_M_ADDR], "C/O ", 4) || !memcmp(apTokens[VEN_L3_M_ADDR], "ATTN:", 5))
   {
      updateCareOf(pOutbuf, sAddr1);
      sAddr1[0] = 0;
   } else if (memcmp(sAddr1, "PO BOX", 4))
   {
      strcpy(sFullAddr, sAddr1);
      
      if (1)

      iCnt = ParseStringIQ(sFullAddr, ' ', 8, apItems);
      for (iTmp = 0; iTmp < iCnt; iTmp++)
         lTrim(apItems[iTmp]);

      // Parse mailing
      parseMAdr1(&sMailAdr, sAddr1);

      if (sMailAdr.lStrNum > 0)
      {
         iTmp = sprintf(acTmp, "%d", sMailAdr.lStrNum);
         memcpy(pOutbuf+OFF_M_STRNUM, acTmp, iTmp);
      }
      if (sMailAdr.strSub[0] > ' ')
         memcpy(pOutbuf+OFF_M_STR_SUB, sMailAdr.strSub, strlen(sMailAdr.strSub));
      if (sMailAdr.strDir[0] > ' ')
         memcpy(pOutbuf+OFF_M_DIR, sMailAdr.strDir, strlen(sMailAdr.strDir));
      memcpy(pOutbuf+OFF_M_STREET, sMailAdr.strName, strlen(sMailAdr.strName));
      if (sMailAdr.strSfx[0] > ' ')
         memcpy(pOutbuf+OFF_M_SUFF, sMailAdr.strSfx, strlen(sMailAdr.strSfx));
      if (sMailAdr.Unit[0] > ' ')
      {
         vmemcpy(pOutbuf+OFF_M_UNITNO, sMailAdr.Unit, SIZ_M_UNITNO);
         vmemcpy(pOutbuf+OFF_M_UNITNOX, sMailAdr.UnitNox, SIZ_M_UNITNOX);
      }
   } else
      memcpy(pOutbuf+OFF_M_STREET, sAddr1, strlen(sAddr1));


   if (*apTokens[VEN_L3_ZIP] > ' ')
      sprintf(sAddr2, "%s %s", apTokens[VEN_L3_M_CTYST], apTokens[VEN_L3_ZIP]);

   // Save original Addr
   if (sAddr1[0] > ' ')
      vmemcpy(pOutbuf+OFF_M_ADDR_D, sAddr1, SIZ_M_ADDR_D);

   if (sAddr2[0] > ' ')
   {
      vmemcpy(pOutbuf+OFF_M_CTY_ST_D, sAddr2, SIZ_M_CTY_ST_D);
      parseMAdr2(&sMailAdr, sAddr2);
      vmemcpy(pOutbuf+OFF_M_CITY, sMailAdr.City, SIZ_M_CITY);
      vmemcpy(pOutbuf+OFF_M_ST, sMailAdr.State, SIZ_M_ST);
      if (sMailAdr.Zip[4] != '-')
         vmemcpy(pOutbuf+OFF_M_ZIP, sMailAdr.Zip, SIZ_M_ZIP);
      else
      {
         if (!memcmp(sMailAdr.State, "CA", 2) && sMailAdr.Zip[0] != '9')
         {
            memcpy(pOutbuf+OFF_M_ZIP+1, sMailAdr.Zip, 4);
            *(pOutbuf+OFF_M_ZIP) = '9';
         } else
            LogMsg("*** Bad mailzip: %s APN=%.12s", sMailAdr.Zip, pOutbuf);
      }
   }
   return 0;
}

/********************************* Ven_MergeSAdr ****************************
 *
 * 4383 OJAI-SANTA PAULA RD, SANTA PAULA 
 * SANTA PAULA
 * FOOTHILL RD, SANTA PAULA
 * 16604 LOCKWOOD VALLEY RD
 *
 ****************************************************************************/

int Ven_MergeSAdr(char *pOutbuf, char *pInbuf)
{
   char  acTmp[256], sFullAddr[256], sAddr1[128], sAddr2[128], *apItems[8];
   int   iTmp, iCnt;

   strcpy(sFullAddr, pInbuf);
   iCnt = ParseStringIQ(sFullAddr, ',', 8, apItems);

   if (iCnt == 2)
   {
      lTrim(apItems[1]);
      if (!strcmp(apItems[1], "DR"))
      {
         sprintf(sAddr1, "%s %s", apItems[0], apItems[1]);
         sAddr2[0] = 0;
      } else if (!memcmp(apItems[1], "SLIP ", 5))
      {
         sprintf(sAddr1, "%s #%s", apItems[0], apItems[1]+5);
         sAddr2[0] = 0;
      } else if (strlen(apItems[1]) == 1)
      {
         sprintf(sAddr1, "%s #%s", apItems[0], apItems[1]);
         if (!memcmp(apItems[0], "487", 3) || !memcmp(apItems[0], "1529", 4))
            strcpy(sAddr2, "SIMI VALLEY");
         else
            sAddr2[0] = 0;
      } else
      {
         strcpy(sAddr1, apItems[0]);
         strcpy(sAddr2, apItems[1]);
      }
   } else if (iCnt == 1)
   {
      if (isdigit(*apItems[0]) || strstr(apItems[0], " AV") || strstr(apItems[0], " DR") || strstr(apItems[0], " BL"))
      {
         sAddr2[0] = 0;
         strcpy(sAddr1, apItems[0]);
      } else if (!memcmp(apItems[0], "ADAMS CANYON", 6))
      {
         strcpy(sAddr1, apItems[0]);
         strcpy(sAddr2, "SANTA PAULA");
      } else 
      {
         sAddr1[0] = 0;
         strcpy(sAddr2, apItems[0]);
      }
   } else if (iCnt == 3)
   {
      // 0180021405: 613 S LA LUNA AV, A, OJAI
      sprintf(sAddr1, "%s #%s", apItems[0], apItems[1]);
      strcpy(sAddr2, apItems[2]);
   } else
   {
      strcpy(sAddr1, apItems[0]);
      strcpy(sAddr2, apItems[1]);
   }

   blankRem(sAddr1);
   blankRem(sAddr2);

   // Save original Addr
   vmemcpy(pOutbuf+OFF_S_ADDR_D, sAddr1, SIZ_S_ADDR_D);
   vmemcpy(pOutbuf+OFF_S_CTY_ST_D, sAddr2, SIZ_S_CTY_ST_D);

   // Parse situs
   ADR_REC sSitus;
   memset(&sSitus, 0, sizeof(ADR_REC));

   parseMAdr1(&sSitus, sAddr1);
   if (sSitus.lStrNum > 0)
   {
      iTmp = sprintf(acTmp, "%d", sSitus.lStrNum);
      memcpy(pOutbuf+OFF_S_STRNUM, acTmp, iTmp);
      vmemcpy(pOutbuf+OFF_S_HSENO, sSitus.HseNo, SIZ_S_HSENO);
   }
   if (sSitus.strSub[0] > ' ')
      memcpy(pOutbuf+OFF_S_STR_SUB, sSitus.strSub, strlen(sSitus.strSub));
   if (sSitus.strDir[0] > ' ')
      memcpy(pOutbuf+OFF_S_DIR, sSitus.strDir, strlen(sSitus.strDir));
   memcpy(pOutbuf+OFF_S_STREET, sSitus.strName, strlen(sSitus.strName));
   if (sSitus.strSfx[0] > ' ')
      memcpy(pOutbuf+OFF_S_SUFF, sSitus.SfxCode, strlen(sSitus.SfxCode));
   if (sSitus.Unit[0] > ' ')
      memcpy(pOutbuf+OFF_S_UNITNO, sSitus.Unit, strlen(sSitus.Unit));

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "2054112250", 10))
   //   iTmp = 0;
#endif

   if (sAddr2[0] > ' ')
   {
      parseMAdr2(&sSitus, sAddr2);
      if (sSitus.City[0] > ' ')
      {
         City2Code(sSitus.City, acTmp, pOutbuf);
         memcpy(pOutbuf+OFF_S_CITY, acTmp, SIZ_S_CITY);
         memcpy(pOutbuf+OFF_S_ST, "CA", 2);
      }
   }
   return 0;
}

/******************************** Ven_MergeSAdr2 ****************************
 *
 * This function to process full situs from  "2023 Secured Complete Summary.csv"
 *
 ****************************************************************************/

int Ven_MergeSAdr2(char *pOutbuf, char *pInbuf, char *pSiteArea)
{
   char  acTmp[256], sFullAddr[256], sAddr1[128], sAddr2[128], *apItems[8];
   int   iTmp, iCnt;

   strcpy(sFullAddr, pInbuf);
   iCnt = ParseStringIQ(sFullAddr, ',', 8, apItems);

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "6600050270", 10) || !memcmp(pOutbuf, "9041000100", 10))
   //   iTmp = 0;
#endif

   if (iCnt == 2)
   {
      lTrim(apItems[1]);
      if (!strcmp(apItems[1], "DR"))
      {
         sprintf(sAddr1, "%s %s", apItems[0], apItems[1]);
         sAddr2[0] = 0;
      } else if (!memcmp(apItems[1], "SLIP ", 5) || !memcmp(apItems[1], "SPC ", 4))
      {
         sprintf(sAddr1, "%s %s", apItems[0], apItems[1]);
         sAddr2[0] = 0;
      } else if (strlen(apItems[1]) == 1)
      {
         sprintf(sAddr1, "%s #%s", apItems[0], apItems[1]);
         if (!memcmp(apItems[0], "487", 3) || !memcmp(apItems[0], "1529", 4))
            strcpy(sAddr2, "SIMI VALLEY");
         else
            sAddr2[0] = 0;
      } else if (!memcmp(apItems[1], "THOUSAND", 8))
      {
         strcpy(sAddr1, apItems[0]);
         strcpy(sAddr2, "THOUSAND OAKS");
      } else
      {
         strcpy(sAddr1, apItems[0]);
         strcpy(sAddr2, apItems[1]);
      }
   } else if (iCnt == 1)
   {
      if (isdigit(*apItems[0]) || strstr(apItems[0], " AV") || 
         strstr(apItems[0], " DR") || strstr(apItems[0], " BL") || strstr(apItems[0], " RD"))
      {
         sAddr2[0] = 0;
         strcpy(sAddr1, apItems[0]);
      } else if (!memcmp(apItems[0], "ADAMS CANYON", 6))
      {
         strcpy(sAddr1, apItems[0]);
         strcpy(sAddr2, "SANTA PAULA");
      } else if (!memcmp(apItems[0], "CAWELTI ", 6))
      {
         strcpy(sAddr1, apItems[0]);
         strcpy(sAddr2, "CAMARILLO");
      } else if (!memcmp(apItems[0], "LILAC ", 6))
      {
         strcpy(sAddr1, apItems[0]);
         strcpy(sAddr2, "SIMI VALLEY");
      } else if (!memcmp(apItems[0], "VIA EL CERRO", 8))
      {
         strcpy(sAddr1, apItems[0]);
         strcpy(sAddr2, "THOUSAND OAKS");
      } else 
      {
         sAddr1[0] = 0;
         strcpy(sAddr2, apItems[0]);
      }
   } else if (iCnt == 3)
   {
      // 0180021405: 613 S LA LUNA AV, A, OJAI
      sprintf(sAddr1, "%s %s", apItems[0], apItems[1]);
      if (!memcmp(apItems[2], "THOUSAND", 8) || !memcmp(apItems[2], " THOUSAND", 8))
         strcpy(sAddr2, "THOUSAND OAKS");
      else
         strcpy(sAddr2, apItems[2]);
   } else
   {
      strcpy(sAddr1, apItems[0]);
      strcpy(sAddr2, apItems[1]);
   }

   blankRem(sAddr1);
   blankRem(sAddr2);

   // Save original Addr
   vmemcpy(pOutbuf+OFF_S_ADDR_D, sAddr1, SIZ_S_ADDR_D);

   // Parse situs
   ADR_REC sSitus;
   memset(&sSitus, 0, sizeof(ADR_REC));

   parseMAdr1(&sSitus, sAddr1);
   if (sSitus.lStrNum > 0)
   {
      iTmp = sprintf(acTmp, "%d", sSitus.lStrNum);
      memcpy(pOutbuf+OFF_S_STRNUM, acTmp, iTmp);
      vmemcpy(pOutbuf+OFF_S_HSENO, sSitus.HseNo, SIZ_S_HSENO);
   }
   if (sSitus.strSub[0] > ' ')
      memcpy(pOutbuf+OFF_S_STR_SUB, sSitus.strSub, strlen(sSitus.strSub));
   if (sSitus.strDir[0] > ' ')
      memcpy(pOutbuf+OFF_S_DIR, sSitus.strDir, strlen(sSitus.strDir));
   memcpy(pOutbuf+OFF_S_STREET, sSitus.strName, strlen(sSitus.strName));
   if (sSitus.strSfx[0] > ' ')
      memcpy(pOutbuf+OFF_S_SUFF, sSitus.SfxCode, strlen(sSitus.SfxCode));
   if (sSitus.Unit[0] > ' ')
   {
      vmemcpy(pOutbuf+OFF_S_UNITNO, sSitus.Unit, SIZ_S_UNITNO);
      vmemcpy(pOutbuf+OFF_S_UNITNOX, sSitus.UnitNox, SIZ_S_UNITNOX);
   }

   memcpy(pOutbuf+OFF_S_ST, "CA", 2);

   // Find city
   char acCode[8], acCity[32];
   strcpy(acTmp, pSiteArea);
   Abbr2Code(acTmp, acCode, acCity, pOutbuf);   
   if (acCode[0] > ' ')
   {
      memcpy(pOutbuf+OFF_S_CITY, acCode, strlen(acCode));
      strcat(acCity, " CA");
      iTmp = blankRem(acCity);
      vmemcpy(pOutbuf+OFF_S_CTY_ST_D, acCity, SIZ_S_CTY_ST_D, iTmp);
   } else if (sAddr2[0] > ' ')
   {
      parseMAdr2(&sSitus, sAddr2);
      if (sSitus.City[0] > ' ')
      {
         City2Code(sSitus.City, acTmp, pOutbuf);
         sprintf(acCity, "%s CA", sSitus.City);
         memcpy(pOutbuf+OFF_S_CITY, acTmp, SIZ_S_CITY);
         vmemcpy(pOutbuf+OFF_S_CTY_ST_D, acCity, SIZ_S_CTY_ST_D, iTmp);
      }
   }
   return 0;
}

/******************************** Ven_MergeSAdr1 ****************************
 *
 * This function to process situs from  "2023 Secured Roll.csv"
 *
 ****************************************************************************/

int Ven_MergeSAdr1(char *pOutbuf)
{
   static   char acRec[512], *pRec=NULL;
   char     acTmp[256], acCode[32], acAddr1[256], *pTmp;
   long     lTmp;
   int      iTmp;

   // Remove old situs
   removeSitus(pOutbuf);

   // Merge data
   acAddr1[0] = 0;
   lTmp = atol(apTokens[VEN_L1_S_NR]);
   if (lTmp > 0)
   {
      memcpy(pOutbuf+OFF_S_HSENO, apTokens[VEN_L1_S_NR], strlen(apTokens[VEN_L1_S_NR]));
      iTmp = sprintf(acTmp, "%d ", lTmp);
      memcpy(pOutbuf+OFF_S_STRNUM, acTmp, iTmp);
      sprintf(acAddr1, "%s ", apTokens[VEN_L1_S_NR]);
      if (pTmp = strstr(apTokens[VEN_L1_S_NR], " 1/2"))
         vmemcpy(pOutbuf+OFF_S_STR_SUB, pTmp+1, SIZ_S_STR_SUB);

      if (*apTokens[VEN_L1_S_DIR] > ' ')
      {
         strcat(acAddr1, apTokens[VEN_L1_S_DIR]);
         strcat(acAddr1, " ");
         vmemcpy(pOutbuf+OFF_S_DIR, apTokens[VEN_L1_S_DIR], SIZ_S_DIR);
         strcat(acAddr1, " ");
      }
   }

   strcpy(acTmp, _strupr(apTokens[VEN_L1_S_STREET]));
   strcat(acAddr1, acTmp);
   vmemcpy(pOutbuf+OFF_S_STREET, acTmp, SIZ_S_STREET);

   if (*apTokens[VEN_L1_S_TYP] > ' ')
   {
      strcat(acAddr1, " ");
      strcat(acAddr1, apTokens[VEN_L1_S_TYP]);

      iTmp = GetSfxCodeX(apTokens[VEN_L1_S_TYP], acTmp);
      if (iTmp > 0)
         Sfx2Code(acTmp, acCode);
      else
      {
         LogMsg0("*** Invalid suffix: %s", apTokens[VEN_L1_S_TYP]);
         iBadSuffix++;
         memset(acCode, ' ', SIZ_S_SUFF);
      }
      memcpy(pOutbuf+OFF_S_SUFF, acCode, SIZ_S_SUFF);
   } 

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "616141018", 9))
   //   acTmp[0] = 0;
#endif

   if (isalnum(*apTokens[VEN_L1_S_UNIT]) || *apTokens[VEN_L1_S_UNIT] == '#')
   {
      strcat(acAddr1, " ");
      if (isdigit(*apTokens[VEN_L1_S_UNIT]))
         strcat(acAddr1, "#");
      else if ((*apTokens[VEN_L1_S_UNIT]) != '#')
         strcat(acAddr1, "STE ");
      strcat(acAddr1, apTokens[VEN_L1_S_UNIT]);
      vmemcpy(pOutbuf+OFF_S_UNITNO, apTokens[VEN_L1_S_UNIT], SIZ_S_UNITNO);
      vmemcpy(pOutbuf+OFF_S_UNITNOX, apTokens[VEN_L1_S_UNIT], SIZ_S_UNITNOX);
   //} else if (*(pOutbuf+OFF_M_UNITNO) > ' ' && *(pOutbuf+OFF_HO_FL) == '1' &&
   //   !memcmp(pOutbuf+OFF_S_STRNUM, pOutbuf+OFF_M_STRNUM, SIZ_S_STRNUM) &&
   //   !memcmp(pOutbuf+OFF_S_STREET, pOutbuf+OFF_M_STREET, SIZ_S_STREET) )
   //{
   //   memcpy(pOutbuf+OFF_S_UNITNO, pOutbuf+OFF_M_UNITNO, SIZ_M_UNITNO);
   //   sprintf(acTmp, " %.*s", SIZ_M_UNITNO, pOutbuf+OFF_M_UNITNO);
   //   strcat(acAddr1, acTmp);
   } else if (*apTokens[VEN_L1_S_UNIT] > ' ')
   {
      if (isalnum(*(1+apTokens[VEN_L1_S_UNIT])))
      {
         strcat(acAddr1, " #");
         strcat(acAddr1, apTokens[VEN_L1_S_UNIT]+1);
         vmemcpy(pOutbuf+OFF_S_UNITNO, apTokens[VEN_L1_S_UNIT]+1, SIZ_S_UNITNO);
      } else
         LogMsg("*** Invalid situs Unit#: %s [%s]", apTokens[VEN_L1_S_UNIT], apTokens[VEN_L1_APN]);
   }

   iTmp = blankRem(acAddr1);
   memcpy(pOutbuf+OFF_S_ADDR_D, acAddr1, iTmp);
   memcpy(pOutbuf+OFF_S_ST, "CA", 2);

   // Find city
   char sCity[64];
   iTmp = atoi(apTokens[VEN_L1_SA]);
   if (iTmp > 0)
   {
      sprintf(acTmp, "%.2d", iTmp);
      Abbr2Code(acTmp, acCode, sCity, pOutbuf);   
   } else
      acCode[0] = 0;
   if (acCode[0] > ' ')
   {
      memcpy(pOutbuf+OFF_S_CITY, acCode, strlen(acCode));
      strcat(sCity, " CA");
      iTmp = blankRem(sCity);
      vmemcpy(pOutbuf+OFF_S_CTY_ST_D, sCity, SIZ_S_CTY_ST_D, iTmp);
   } else  // Matching up with mailing addr to get city name
   if (!memcmp(pOutbuf+OFF_S_STRNUM, pOutbuf+OFF_M_STRNUM, SIZ_S_STRNUM) &&
       !memcmp(pOutbuf+OFF_S_STREET, pOutbuf+OFF_M_STREET, SIZ_S_STREET) &&
       *(pOutbuf+OFF_M_CITY) > ' ')
   {
      memcpy(sCity, pOutbuf+OFF_M_CITY, SIZ_M_CITY);
      myTrim(sCity, SIZ_M_CITY);
      City2Code(sCity, acCode, pOutbuf);
      memcpy(pOutbuf+OFF_S_CITY, acCode, SIZ_S_CITY);
      memcpy(pOutbuf+OFF_S_ZIP, pOutbuf+OFF_M_ZIP, SIZ_S_ZIP);
   } else if (iNumTRA > 0)
   {
      memcpy(acTmp, apTokens[VEN_L1_TRA], 2);
      acTmp[2] = 0;
      int iCode = XrefCode2Idx(&asTRA[0], acTmp, iNumTRA);
      if (iCode > 0)
      {
         iTmp = sprintf(acCode, "%d", iCode);
         memcpy(pOutbuf+OFF_S_CITY, acCode, iTmp);
         pTmp = XrefIdx2Name(&asTRA[0], iCode, iNumTRA);
         iTmp = sprintf(acTmp, "%s CA", pTmp);
         if (memcmp(acTmp, "     ", 5) && memcmp(acTmp, "BLANK", 5))
            memcpy(pOutbuf+OFF_S_CTY_ST_D, acTmp, iTmp);
      }
   }

   return 0;
}

/******************************** Ven_MergeSAdr3 ****************************
 *
 * This function to process situs from  "2024 Secured Roll.csv"
 *
 ****************************************************************************/

int Ven_MergeSAdr3(char *pOutbuf)
{
   static   char acRec[512], *pRec=NULL;
   char     acTmp[256], acCode[32], acAddr1[256], *pTmp;
   long     lTmp;
   int      iTmp;

   // Remove old situs
   removeSitus(pOutbuf);

   // Merge data
   acAddr1[0] = 0;
   lTmp = atol(apTokens[VEN_L3_S_NR]);
   if (lTmp > 0)
   {
      memcpy(pOutbuf+OFF_S_HSENO, apTokens[VEN_L3_S_NR], strlen(apTokens[VEN_L3_S_NR]));
      iTmp = sprintf(acTmp, "%d ", lTmp);
      memcpy(pOutbuf+OFF_S_STRNUM, acTmp, iTmp);
      sprintf(acAddr1, "%s ", apTokens[VEN_L3_S_NR]);
      if (pTmp = strstr(apTokens[VEN_L3_S_NR], " 1/2"))
         vmemcpy(pOutbuf+OFF_S_STR_SUB, pTmp+1, SIZ_S_STR_SUB);

      //if (*apTokens[VEN_L3_S_DIR] > ' ')
      //{
      //   strcat(acAddr1, apTokens[VEN_L3_S_DIR]);
      //   strcat(acAddr1, " ");
      //   vmemcpy(pOutbuf+OFF_S_DIR, apTokens[VEN_L3_S_DIR], SIZ_S_DIR);
      //   strcat(acAddr1, " ");
      //}
   }

   strcpy(acAddr1, _strupr(apTokens[VEN_L3_S_ADDR]));

   // Parse situs
   ADR_REC sSitus;
   memset(&sSitus, 0, sizeof(ADR_REC));

   parseMAdr1(&sSitus, acAddr1);
   if (sSitus.lStrNum > 0)
   {
      iTmp = sprintf(acTmp, "%d", sSitus.lStrNum);
      memcpy(pOutbuf+OFF_S_STRNUM, acTmp, iTmp);
      vmemcpy(pOutbuf+OFF_S_HSENO, sSitus.HseNo, SIZ_S_HSENO);
   }
   if (sSitus.strSub[0] > ' ')
      memcpy(pOutbuf+OFF_S_STR_SUB, sSitus.strSub, strlen(sSitus.strSub));
   if (sSitus.strDir[0] > ' ')
      memcpy(pOutbuf+OFF_S_DIR, sSitus.strDir, strlen(sSitus.strDir));
   memcpy(pOutbuf+OFF_S_STREET, sSitus.strName, strlen(sSitus.strName));
   if (sSitus.strSfx[0] > ' ')
      memcpy(pOutbuf+OFF_S_SUFF, sSitus.SfxCode, strlen(sSitus.SfxCode));
   if (sSitus.Unit[0] > ' ')
   {
      vmemcpy(pOutbuf+OFF_S_UNITNO, sSitus.Unit, SIZ_S_UNITNO);
      vmemcpy(pOutbuf+OFF_S_UNITNOX, sSitus.UnitNox, SIZ_S_UNITNOX);
   }

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "616141018", 9))
   //   acTmp[0] = 0;
#endif

   iTmp = blankRem(acAddr1);
   memcpy(pOutbuf+OFF_S_ADDR_D, acAddr1, iTmp);
   memcpy(pOutbuf+OFF_S_ST, "CA", 2);

   // Find city
   char sCity[64];
   iTmp = atoi(apTokens[VEN_L3_SA]);
   if (iTmp > 0)
   {
      sprintf(acTmp, "%.2d", iTmp);
      Abbr2Code(acTmp, acCode, sCity, pOutbuf);   
   } else
      acCode[0] = 0;
   if (acCode[0] > ' ')
   {
      memcpy(pOutbuf+OFF_S_CITY, acCode, strlen(acCode));
      strcat(sCity, " CA");
      iTmp = blankRem(sCity);
      vmemcpy(pOutbuf+OFF_S_CTY_ST_D, sCity, SIZ_S_CTY_ST_D, iTmp);
   } else if (*apTokens[VEN_L3_S_CTYST] >= 'A')
   {
      vmemcpy(pOutbuf+OFF_S_CTY_ST_D, apTokens[VEN_L3_S_CTYST], SIZ_S_CTY_ST_D);
      if (pTmp = strrchr(apTokens[VEN_L3_S_CTYST], ' '))
      {
         *pTmp = 0;
         City2Code(apTokens[VEN_L3_S_CTYST], acCode, pOutbuf);
         memcpy(pOutbuf+OFF_S_CITY, acCode, SIZ_S_CITY);
      }
   } else  // Matching up with mailing addr to get city name
   if (!memcmp(pOutbuf+OFF_S_STRNUM, pOutbuf+OFF_M_STRNUM, SIZ_S_STRNUM) &&
       !memcmp(pOutbuf+OFF_S_STREET, pOutbuf+OFF_M_STREET, SIZ_S_STREET) &&
       *(pOutbuf+OFF_M_CITY) > ' ')
   {
      memcpy(sCity, pOutbuf+OFF_M_CITY, SIZ_M_CITY);
      myTrim(sCity, SIZ_M_CITY);
      City2Code(sCity, acCode, pOutbuf);
      memcpy(pOutbuf+OFF_S_CITY, acCode, SIZ_S_CITY);
      memcpy(pOutbuf+OFF_S_ZIP, pOutbuf+OFF_M_ZIP, SIZ_S_ZIP);
   } else if (iNumTRA > 0)
   {
      memcpy(acTmp, apTokens[VEN_L3_TRA], 2);
      acTmp[2] = 0;
      int iCode = XrefCode2Idx(&asTRA[0], acTmp, iNumTRA);
      if (iCode > 0)
      {
         iTmp = sprintf(acCode, "%d", iCode);
         memcpy(pOutbuf+OFF_S_CITY, acCode, iTmp);
         pTmp = XrefIdx2Name(&asTRA[0], iCode, iNumTRA);
         iTmp = sprintf(acTmp, "%s CA", pTmp);
         if (memcmp(acTmp, "     ", 5) && memcmp(acTmp, "BLANK", 5))
            memcpy(pOutbuf+OFF_S_CTY_ST_D, acTmp, iTmp);
      }
   }

   return 0;
}

/********************************** Ven_LoadUpdt ****************************
 *
 * Load roll update file
 *
 ****************************************************************************/

int Ven_LoadUpdt(int iLoadFlag, int iSkip)
{
   char     *pTmp, acRec[MAX_RECSIZE], acBuf[MAX_RECSIZE], acRollRec[1024];
   char     sApn[32], acRawFile[_MAX_PATH], acOutFile[_MAX_PATH];

   HANDLE   fhIn, fhOut;

   int      iRet, iTmp, iRollUpd=0, iNewRec=0, iRetiredRec=0;
   DWORD    nBytesRead;
   DWORD    nBytesWritten;
   BOOL     bRet, bEof;
   long     lTmp, lRet=0, lCnt=0;
   VEN_ROLL *pRec = (VEN_ROLL *)acRollRec;

   sprintf(acRawFile, acRawTmpl, "VEN", "VEN", "S01");
   sprintf(acOutFile, acRawTmpl, "VEN", "VEN", "R01");

   // Rename files for processing
   if (_access(acRawFile, 0))
   {
      if (!_access(acOutFile, 0))
         rename(acOutFile, acRawFile);
      else
      {
         LogMsg("***** Missing input file %s.  Please recheck!");
         return 1;
      }
   }

   // Open file
   LogMsg("Open Roll file %s", acRollFile);
   fdRoll = fopen(acRollFile, "r");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", acRollFile);
      return 3;
   }

   // Open Input file
   LogMsg("Open input file %s", acRawFile);
   fhIn = CreateFile(acRawFile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhIn == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening input file: %s\n", acRawFile);
      return 3;
   }

   // Open Output file
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening output file: %s\n", acOutFile);
      return 4;
   }

   // Get first RollRec
   pTmp = fgets((char *)&acRollRec[0], 1024, fdRoll);
   bEof = (pTmp ? false:true);

   // Copy skip record
   memset(acBuf, ' ', iRecLen);
   while (iSkip-- > 0)
   {
      ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);
      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
   }

   iNoMatch=iBadCity=iBadSuffix=0;
   // Merge loop
   while (!bEof)
   {
      bRet = ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);
      if (!bRet)
      {
         LogMsg("***** Error reading input file %s (%f)", acRawFile, GetLastError());
         break;
      }

      if (!nBytesRead)
         break;

      NextRollRec:
#ifdef _DEBUG
      //if (!memcmp(acRollRec, "9070210025", iApnLen))
      //   iTmp = 0;
#endif
      iTmp = Ven_FormatApn(acRollRec, sApn);

      iTmp = memcmp(acBuf, sApn, iApnLen);
      if (!iTmp)
      {
         // For VENTURA_PUBINFO-yyyymmdd.txt or ParcelQuestDataExtract.txt
         //iRet = Ven_MergeUpdt(acBuf, acRollRec, UPDATE_R01);
         iRet = Ven_MergeUpdt2(acBuf, acRollRec, UPDATE_R01);
         iRollUpd++;

         // Read next roll record
         pTmp = fgets(acRollRec, 1024, fdRoll);

         if (!pTmp)
            bEof = true;    // Signal to stop
      } else if (iTmp > 0)       // Roll not match, new roll record
      {
         if (bDebug)
            LogMsg0("New roll record : %.*s (%d)", iApnLen, acRollRec, lCnt);

         // Create new R01 record
         //iRet = Ven_MergeUpdt(acRec, acRollRec, CREATE_R01);
         iRet = Ven_MergeUpdt2(acRec, acRollRec, CREATE_R01);
         iNewRec++;

         // Save last recording date
         lTmp = atoin((char *)&acRec[OFF_TRANSFER_DT], 8);
         if (lTmp > lLastRecDate && lTmp < lToday)
            lLastRecDate = lTmp;

         bRet = WriteFile(fhOut, acRec, iRecLen, &nBytesWritten, NULL);
         if (!(++lCnt % 1000))
            printf("\r%u", lCnt);

         // Get next roll record
         pTmp = fgets(acRollRec, 1024, fdRoll);

         if (!pTmp)
            bEof = true;    // Signal to stop
         else
            goto NextRollRec;
      } else
      {
         // Temporary fix
         if (!memcmp(&acBuf[OFF_M_ZIP], "XXX", 3) || !memcmp(&acBuf[OFF_M_ZIP], "UNK", 3))
            memset(&acBuf[OFF_M_ZIP], ' ', 9);

      }

      // Save last recording date
      lTmp = atoin((char *)&acBuf[OFF_TRANSFER_DT], 8);
      if (lTmp > lLastRecDate && lTmp < lToday)
         lLastRecDate = lTmp;

      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);

      if (!bRet)
      {
         LogMsg("***** Error occurs: %d\n", GetLastError());
         lRet = WRITE_ERR;
         break;
      }
   }

   // Do the rest of the file
   while (nBytesRead>0 && ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL))
   {
      if (!nBytesRead)
         break;

      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);

      iNoMatch++;
   }

   // If there are more new records, add them
   while (!bEof)
   {
      if (bDebug)
         LogMsg0("*** New roll record : %.*s (%d) ***", iApnLen, acRollRec, lCnt);

      // Create new R01 record
      iRet = Ven_MergeUpdt2(acRec, acRollRec, CREATE_R01);
      iNewRec++;

      // Save last recording date
      lTmp = atoin((char *)&acBuf[OFF_TRANSFER_DT], 8);
      if (lTmp > lLastRecDate && lTmp < lToday)
         lLastRecDate = lTmp;

      bRet = WriteFile(fhOut, acRec, iRecLen, &nBytesWritten, NULL);
      lCnt++;

      // Get next roll record
      pTmp = fgets(acRollRec, 1024, fdRoll);

      if (!pTmp || !memcmp(acRollRec, "9999999", 7))
         bEof = true;    // Signal to stop
   }

   // Close files
   if (fdRoll)
      fclose(fdRoll);
   if (fhOut)
      CloseHandle(fhOut);
   if (fhIn)
      CloseHandle(fhIn);

   LogMsg("Total output records:       %u", lCnt);
   LogMsg("      new records:          %u", iNewRec);
   LogMsg("      retired records:      %u", iRetiredRec);
   LogMsg("      bad-city records:     %u", iBadCity);
   LogMsg("      bad-suffix records:   %u", iBadSuffix);

   printf("\nTotal output records: %u", lCnt);

   lRecCnt = lCnt;
   return lRet;
}

int Ven_LoadUpdt1(int iLoadFlag)
{
   char     *pTmp, acRec[MAX_RECSIZE], acBuf[MAX_RECSIZE], acRollRec[1024];
   char     sApn[32], acRawFile[_MAX_PATH], acOutFile[_MAX_PATH];

   HANDLE   fhIn, fhOut;

   int      iRet, iTmp, iRollUpd=0, iNewRec=0, iRetiredRec=0;
   DWORD    nBytesRead;
   DWORD    nBytesWritten;
   BOOL     bRet, bEof;
   long     lTmp, lRet=0, lCnt=0;
   VEN_ROLL *pRec = (VEN_ROLL *)acRollRec;

   sprintf(acRawFile, acRawTmpl, "VEN", "VEN", "S01");
   sprintf(acOutFile, acRawTmpl, "VEN", "VEN", "R01");

   // Rename files for processing
   if (_access(acRawFile, 0))
   {
      if (!_access(acOutFile, 0))
         rename(acOutFile, acRawFile);
      else
      {
         LogMsg("***** Missing input file %s.  Please recheck!");
         return 1;
      }
   }

   // Open file
   LogMsg("Open Roll file %s", acRollFile);
   fdRoll = fopen(acRollFile, "r");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", acRollFile);
      return 3;
   }

   // Open Input file
   LogMsg("Open input file %s", acRawFile);
   fhIn = CreateFile(acRawFile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhIn == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening input file: %s\n", acRawFile);
      return 3;
   }

   // Open Output file
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening output file: %s\n", acOutFile);
      return 4;
   }

   // Get first RollRec
   pTmp = fgets((char *)&acRollRec[0], 1024, fdRoll);
   bEof = (pTmp ? false:true);

   // Copy skip record
   memset(acBuf, ' ', iRecLen);
   ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);
   bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);

   iNoMatch=iBadCity=iBadSuffix=0;
   // Merge loop
   while (!bEof)
   {
      bRet = ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);
      if (!bRet)
      {
         LogMsg("***** Error reading input file %s (%f)", acRawFile, GetLastError());
         break;
      }

      if (!nBytesRead)
         break;

      NextRollRec:
      iTmp = Ven_FormatApn(acRollRec, sApn);

      iTmp = memcmp(acBuf, sApn, iApnLen);
      if (!iTmp)
      {
         // For VENTURA_PUBINFO-yyyymmdd.txt or ParcelQuestDataExtract.txt
         iRet = Ven_MergeUpdt2(acBuf, acRollRec, UPDATE_R01);
         iRollUpd++;

         // Read next roll record
         pTmp = fgets(acRollRec, 1024, fdRoll);

         if (!pTmp)
            bEof = true;    // Signal to stop
      } else if (iTmp > 0)       // Roll not match, new roll record
      {
         if (bDebug)
            LogMsg0("New roll record : %.*s (%d)", iApnLen, acRollRec, lCnt);

         // Create new R01 record
         iRet = Ven_MergeUpdt2(acRec, acRollRec, CREATE_R01);
         iNewRec++;

         // Save last recording date
         lTmp = atoin((char *)&acRec[OFF_TRANSFER_DT], 8);
         if (lTmp > lLastRecDate && lTmp < lToday)
            lLastRecDate = lTmp;

         bRet = WriteFile(fhOut, acRec, iRecLen, &nBytesWritten, NULL);
         if (!(++lCnt % 1000))
            printf("\r%u", lCnt);

         // Get next roll record
         pTmp = fgets(acRollRec, 1024, fdRoll);

         if (!pTmp)
            bEof = true;    // Signal to stop
         else
            goto NextRollRec;
      } else
      {
         // Temporary fix
         if (!memcmp(&acBuf[OFF_M_ZIP], "XXX", 3) || !memcmp(&acBuf[OFF_M_ZIP], "UNK", 3))
            memset(&acBuf[OFF_M_ZIP], ' ', 9);
      }

      // Save last recording date
      lTmp = atoin((char *)&acBuf[OFF_TRANSFER_DT], 8);
      if (lTmp > lLastRecDate && lTmp < lToday)
         lLastRecDate = lTmp;

      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);

      if (!bRet)
      {
         LogMsg("***** Error occurs: %d\n", GetLastError());
         lRet = WRITE_ERR;
         break;
      }
   }

   // Do the rest of the file
   while (nBytesRead>0 && ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL))
   {
      if (!nBytesRead)
         break;

      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);

      iNoMatch++;
   }

   // If there are more new records, add them
   while (!bEof)
   {
      if (bDebug)
         LogMsg0("*** New roll record : %.*s (%d) ***", iApnLen, acRollRec, lCnt);

      // Create new R01 record
      iRet = Ven_MergeUpdt2(acRec, acRollRec, CREATE_R01);
      iNewRec++;

      // Save last recording date
      lTmp = atoin((char *)&acBuf[OFF_TRANSFER_DT], 8);
      if (lTmp > lLastRecDate && lTmp < lToday)
         lLastRecDate = lTmp;

      bRet = WriteFile(fhOut, acRec, iRecLen, &nBytesWritten, NULL);
      lCnt++;

      // Get next roll record
      pTmp = fgets(acRollRec, 1024, fdRoll);

      if (!pTmp || !memcmp(acRollRec, "9999999", 7))
         bEof = true;    // Signal to stop
   }

   // Close files
   if (fdRoll)
      fclose(fdRoll);
   if (fhOut)
      CloseHandle(fhOut);
   if (fhIn)
      CloseHandle(fhIn);

   LogMsg("Total output records:       %u", lCnt);
   LogMsg("      new records:          %u", iNewRec);
   LogMsg("      retired records:      %u", iRetiredRec);
   LogMsg("      bad-city records:     %u", iBadCity);
   LogMsg("      bad-suffix records:   %u", iBadSuffix);

   printf("\nTotal output records: %u", lCnt);

   lRecCnt = lCnt;
   return lRet;
}

/********************************* Ven_Load_LDR *****************************
 *
 * 
 ****************************************************************************/

//int Ven_Load_LDR(int iLoadFlag, int iFirstRec /* 1=create header rec */)
//{
//   char     *pTmp, acBuf[MAX_RECSIZE], acRollRec[MAX_RECSIZE];
//   char     acOutFile[_MAX_PATH];
//
//   HANDLE   fhOut;
//
//   int      iRet;
//   DWORD    nBytesWritten;
//   BOOL     bRet, bEof;
//   long     lRet=0, lCnt=0, lSaleCnt=0, lDropCount;
//   VEN_ROLL *pRec = (VEN_ROLL *)&acRollRec;
//
//   // Open roll file
//   LogMsg("Open Roll file %s", acRollFile);
//   fdRoll = fopen(acRollFile, "r");
//   if (fdRoll == NULL)
//   {
//      LogMsg("***** Error opening roll file: %s\n", acRollFile);
//      return 2;
//   }
//
//   // Open Output file
//   sprintf(acOutFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "R01");
//   LogMsg("Open output file %s", acOutFile);
//   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
//          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);
//   if (fhOut == INVALID_HANDLE_VALUE)
//   {
//      LogMsg("***** Error opening output file: %s\n", acOutFile);
//      return 4;
//   }
//
//   // Output first header record
//   if (iFirstRec > 0)
//   {
//      memset(acBuf, '9', iRecLen);
//      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
//   }
//
//   // Get first RollRec
//   pTmp = fgets((char *)&acRollRec[0], iRollLen, fdRoll);
//   if (!memcmp(acRollRec, "0000000000", iApnLen))
//      pTmp = fgets((char *)&acRollRec[0], iRollLen, fdRoll);
//   bEof = (pTmp ? false:true);
//
//   // Init variables
//   iNoMatch=iBadCity=iBadSuffix=0;
//   lDropCount = 0;
//
//   // Merge loop
//   while (pTmp)
//   {
//      // Create new R01 record
//      if (memcmp(acRollRec+ROFF_APN-1, "0000000000", RSIZ_APN))
//      {
//         if (acRollRec[ROFF_APN_STATUS-1] != 'V')
//         {
//            iRet = Ven_MergeRoll(acBuf, acRollRec, iRollLen, CREATE_R01|CREATE_LIEN);
//            bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
//            if (!bRet)
//            {
//               LogMsg("***** Error writing to output file at record %d\n", lCnt);
//               lRet = WRITE_ERR;
//               break;
//            }
//            lLDRRecCount++;
//         } else
//            lDropCount++;
//      }
//      // Get next roll record
//      pTmp = fgets(acRollRec, iRollLen, fdRoll);
//      if (!(++lCnt % 1000))
//         printf("\r%u", lCnt);
//   }
//
//   // Close files
//   if (fdRoll)
//      fclose(fdRoll);
//   if (fhOut)
//      CloseHandle(fhOut);
//
//   LogMsg("Total output records:       %u", lCnt);
//   LogMsg("Number of dropped records:  %u", lDropCount);
//   LogMsg("      bad-city records:     %u", iBadCity);
//   LogMsg("      bad-suffix records:   %u", iBadSuffix);
//   LogMsg("      sales extracted:      %u", lSaleCnt);
//   LogMsg("Last recording date:        %u", lLastRecDate);
//   printf("\nTotal output records: %u\n", lLDRRecCount);
//
//   lRecCnt = lLDRRecCount;
//   return lRet;
//}

/******************************** Ven_MergeChar ******************************
 *
 * Using CHAR from "2022_07_05_Public_Data_Final.csv" to populate roll file
 *
 *****************************************************************************/

int Ven_MergeChar(char *pOutbuf)
{
   static   char acRec[1024], *pRec=NULL;
   char     acBuf[1024], acTmp[256], acCode[32], *pTmp, *apItems[100];
   long     lTmp, lBldgSqft, lGarSqft;
   int      iRet, iLoop, iBeds, iBath, iHBath, iTmp;

   // Get first Char rec for first call
   if (!pRec)
   {
      pRec = fgets(acRec, 1024, fdChar);        // Remove header
      pRec = fgets(acRec, 1024, fdChar);
   }

   do
   {
      if (!pRec)
      {
         fclose(fdChar);
         fdChar = NULL;
         return 1;      // EOF
      }
      strcpy(acBuf, acRec);
      iRet = ParseStringIQ(acBuf, '|', 100, apItems);
      if (iRet < VEN_P_FLDS)
      {
         LogMsg("*** Bad CHAR record: %s (%d)", pRec, iRet);
         return 1;
      }

      // Compare Asmt
      iLoop = memcmp(pOutbuf+OFF_ALT_APN, apItems[VEN_P_APN], iApnLen);
      if (iLoop > 0)
      {
         //if (bDebug)
            LogMsg0("Skip Char rec  %.*s", iApnLen, apItems[VEN_P_APN]);
         pRec = fgets(acRec, 1024, fdChar);
      }
   } while (iLoop > 0);

   // If not match, return
   if (iLoop)
      return 1;

   // Quality Class
   if (*apItems[VEN_P_QC] > ' ')
   {
      pTmp = apItems[VEN_P_QC];
      vmemcpy(pOutbuf+OFF_QUALITYCLASS, pTmp, SIZ_QUALITYCLASS);
      *(pOutbuf+OFF_BLDG_CLASS) = *pTmp;
      if (isdigit(*(pTmp+1)))
      {
         iRet = Quality2Code(pTmp+1, acCode, NULL);
         vmemcpy(pOutbuf+OFF_BLDG_QUAL, acCode, SIZ_BLDG_QUAL);
      }
   } 

   // Yrblt
   if (*apItems[VEN_P_YRBLT] > '0')
      vmemcpy(pOutbuf+OFF_YR_BLT, apItems[VEN_P_YRBLT], SIZ_YR_BLT);

   // BldgSqft
   lBldgSqft = atol(apItems[VEN_P_SQFTI]);
   if (lBldgSqft > 10)
   {
      sprintf(acTmp, "%*d", SIZ_BLDG_SF, lBldgSqft);
      memcpy(pOutbuf+OFF_BLDG_SF, acTmp, SIZ_BLDG_SF);
   } 

   // Garage Sqft
   lGarSqft = atol(apItems[VEN_P_GARA]);
   if (lGarSqft > 10)
   {
      sprintf(acTmp, "%*d", SIZ_GAR_SQFT, lGarSqft);
      memcpy(pOutbuf+OFF_GAR_SQFT, acTmp, SIZ_GAR_SQFT);
      *(pOutbuf+OFF_PARK_TYPE) = 'Z';
   } else
   {
      lGarSqft = atol(apItems[VEN_P_CARPTA]);
      if (lGarSqft > 10)
      {
         sprintf(acTmp, "%*u", SIZ_GAR_SQFT, lGarSqft);
         memcpy(pOutbuf+OFF_GAR_SQFT, acTmp, SIZ_GAR_SQFT);
         *(pOutbuf+OFF_PARK_TYPE) = 'C';
      }
   }

   // Beds
   iBeds = atol(apItems[VEN_P_BEDS]);
   if (iBeds > 0)
   {
      sprintf(acTmp, "%*u", SIZ_BEDS, iBeds);
      memcpy(pOutbuf+OFF_BEDS, acTmp, SIZ_BEDS);
   }

   // Bath
   iBath = atol(apItems[VEN_P_BATHS]);
   if (iBath > 0)
   {
      if (iBath > 999)
      {
         memcpy(pOutbuf+OFF_BATH_F, apItems[VEN_P_BATHS], 2);
         memcpy(pOutbuf+OFF_BATH_4Q, apItems[VEN_P_BATHS], 2);
         iHBath = 2;
      } else
      {
         *(pOutbuf+OFF_BATH_F) = *apItems[VEN_P_BATHS];
         *(pOutbuf+OFF_BATH_4Q) = *apItems[VEN_P_BATHS];
         iHBath = 1;
      }
      iTmp = atol(apItems[VEN_P_BATHS]+iHBath);
      if (iTmp > 0)
      {
         *(pOutbuf+OFF_BATH_H) = '1';
         if (iTmp == 50)
            *(pOutbuf+OFF_BATH_2Q) = '1';
         else if (iTmp > 50)
            *(pOutbuf+OFF_BATH_3Q) = '1';
         else if (iTmp < 50)
            *(pOutbuf+OFF_BATH_1Q) = '1';
      }
   }
   
   // Total rooms
   iTmp = atol(apItems[VEN_P_TOTRMS]);
   if (iTmp > 0)
   {
      sprintf(acTmp, "%*u", SIZ_ROOMS, iTmp);
      memcpy(pOutbuf+OFF_ROOMS, acTmp, SIZ_ROOMS);
   }

   // Fireplace
   iTmp = atol(apItems[VEN_P_FP]);
   if (iTmp > 9)
      *(pOutbuf+OFF_FIRE_PL) = 'M';
   else if (iTmp > 0)
      *(pOutbuf+OFF_FIRE_PL) = '0' | iTmp;

   // Pool
   if (*apItems[VEN_P_POOLA] > '0')
      *(pOutbuf+OFF_POOL) = 'P';

   // Stories
   int iStories=0;
   int iFlr3 = atol(apItems[VEN_P_FLR3A]);
   int iFlr2 = atol(apItems[VEN_P_FLR2A]);
   int iFlr1 = atol(apItems[VEN_P_FLR1A]);
   if (iFlr1 > 0)
   {
      sprintf(acTmp, "%*u", SIZ_FLOOR_SF, iFlr1);
      memcpy(pOutbuf+OFF_FLOOR1_SF, acTmp, SIZ_FLOOR_SF);
      iStories = 1;
      if (iFlr2 > 0)
      {
         sprintf(acTmp, "%*u", SIZ_FLOOR_SF, iFlr2);
         memcpy(pOutbuf+OFF_FLOOR2_SF, acTmp, SIZ_FLOOR_SF);
         iStories = 2;
         if (iFlr3 > 0)
         {
            sprintf(acTmp, "%*u", SIZ_FLOOR_SF, iFlr3);
            memcpy(pOutbuf+OFF_FLOOR3_SF, acTmp, SIZ_FLOOR_SF);
            iStories = 3;
         }
      }
      sprintf(acTmp, "%d.0  ", iStories);
      memcpy(pOutbuf+OFF_STORIES, acTmp, SIZ_STORIES);
   }

   // Legal Desc
   if (*apItems[VEN_P_TRACT] > ' ')
      vmemcpy(pOutbuf+OFF_TRACT, apItems[VEN_P_TRACT], SIZ_TRACT);
   if (*apItems[VEN_P_LOT] > ' ')
      vmemcpy(pOutbuf+OFF_LOT, apItems[VEN_P_LOT], SIZ_LOT);

   // Patio
   lTmp = atol(apItems[VEN_P_PATA]);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*u", SIZ_PATIO_SF, lTmp);
      memcpy(pOutbuf+OFF_PATIO_SF, acTmp, SIZ_PATIO_SF);
   }

   // Deck
   lTmp = atol(apItems[VEN_P_DECKA]);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*u", SIZ_DECK_SF, lTmp);
      memcpy(pOutbuf+OFF_DECK_SF, acTmp, SIZ_DECK_SF);
   }

   // Porch
   lTmp = atol(apItems[VEN_P_PORA]);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*u", SIZ_PORCH_SF, lTmp);
      memcpy(pOutbuf+OFF_PORCH_SF, acTmp, SIZ_PORCH_SF);
   }

   // LotSqft
   lTmp = atol(apItems[VEN_P_SQFTL]);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*u", SIZ_LOT_SQFT, lTmp);
      memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);
   } 

   double dAcreAge = atof(apItems[VEN_P_ACREAGE]);
   if (dAcreAge > 0.0)
   {
      sprintf(acTmp, "%*u", SIZ_LOT_ACRES, (long)(dAcreAge*1000));
      memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);
   }

   // Exemption code
   if (*apItems[VEN_P_EXMPCD1] > ' ')
      vmemcpy(pOutbuf+OFF_EXE_CD1, apItems[VEN_P_EXMPCD1], SIZ_EXE_CD1);
   if (*apItems[VEN_P_EXMPCD2] > ' ')
      vmemcpy(pOutbuf+OFF_EXE_CD2, apItems[VEN_P_EXMPCD2], SIZ_EXE_CD1);
   if (*apItems[VEN_P_EXMPCD3] > ' ')
      vmemcpy(pOutbuf+OFF_EXE_CD3, apItems[VEN_P_EXMPCD3], SIZ_EXE_CD1);

   lCharMatch++;

   // Get next record
   pRec = fgets(acRec, 1024, fdChar);

   return 0;
}

/******************************* Ven_Load_Lien ******************************
 *
 * Loading LDR file "2022 Secured Parcel Data.csv"
 *
 ****************************************************************************/

int Ven_MergeLien(char *pOutbuf, char *pRollRec)
{
   char     acTmp[256], acTmp1[256];
   double   dTmp;
   int      iRet=0, iTmp;
   unsigned long lTmp;

   // Parse input
   iTokens = ParseStringIQ(pRollRec, '|', MAX_FLD_TOKEN, apTokens);
   if (iTokens < VEN_L_FLDS)
   {
      LogMsg("***** Error: bad input record for %s (%d)", iTokens);
      return -1;
   }

   // Clear output buffer
   memset(pOutbuf, ' ', iRecLen);

   // unformatted APN
   vmemcpy(pOutbuf+OFF_ALT_APN, apTokens[VEN_L_APN], iApnLen);
   // formatted APN
   iRet = Ven_FormatApn(apTokens[VEN_L_APN], acTmp);
   memcpy(pOutbuf+OFF_APN_S, acTmp, iRet);
   // Display APN
   iRet = formatApn(acTmp, acTmp1, &myCounty);
   memcpy(pOutbuf+OFF_APN_D, acTmp1, iRet);

   // Create MapLink and output new record
   iRet = formatMapLink(acTmp, acTmp1, &myCounty);
   memcpy(pOutbuf+OFF_MAPLINK, acTmp1, iRet);

   // Create index map link
   if (getIndexPage(acTmp1, acTmp, &myCounty))
      memcpy(pOutbuf+OFF_IMAPLINK, acTmp, iRet);

   // County code
   memcpy(pOutbuf+OFF_CO_NUM, "56VENA", 6);

   // Year assessed
   memcpy(pOutbuf+OFF_YR_ASSD, myCounty.acYearAssd, 4);

   // TRA
   lTmp = atol(apTokens[VEN_L_TRA]);
   if (lTmp > 0)
   {
      iRet = sprintf(acTmp, "%.6d", lTmp);
      vmemcpy(pOutbuf+OFF_TRA, acTmp, iRet);
   }

   // Land
   long lLand = atoi(apTokens[VEN_L_LAND]);
   if (lLand > 0)
   {
      sprintf(acTmp, "%*u", SIZ_LAND, lLand);
      memcpy(pOutbuf+OFF_LAND, acTmp, SIZ_LAND);
   }

   // Improve
   long lImpr = atoi(apTokens[VEN_L_IMPR]);
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*u", SIZ_IMPR, lImpr);
      memcpy(pOutbuf+OFF_IMPR, acTmp, SIZ_IMPR);
   }

   long lFixt = atoi(apTokens[VEN_L_FIXTURE]);
   long lPP   = atoi(apTokens[VEN_L_PERSPROP]);
   long lTree = atoi(apTokens[VEN_L_TREESVINES]);
   long lMineral = atoi(apTokens[VEN_L_MINERALRIGHTS]);
   lTmp = lPP+lFixt+lTree+lMineral;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*u", SIZ_OTHER, lTmp);
      memcpy(pOutbuf+OFF_OTHER, acTmp, SIZ_OTHER);
      if (lPP > 0)
      {
         sprintf(acTmp, "%*u", SIZ_PERSPROP, lPP);
         memcpy(pOutbuf+OFF_PERSPROP, acTmp, SIZ_PERSPROP);
      }
      if (lFixt > 0)
      {
         sprintf(acTmp, "%*u", SIZ_FIXTR, lFixt);
         memcpy(pOutbuf+OFF_FIXTR, acTmp, SIZ_FIXTR);
      }
      if (lTree > 0)
      {
         sprintf(acTmp, "%*u", SIZ_TREEVINES, lTree);
         memcpy(pOutbuf+OFF_TREEVINES, acTmp, SIZ_TREEVINES);
      }
      if (lMineral > 0)
      {
         sprintf(acTmp, "%*u", SIZ_MINERAL, lMineral);
         memcpy(pOutbuf+OFF_MINERAL, acTmp, SIZ_MINERAL);
      }
   }

   // Gross total
   lTmp += lLand+lImpr;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*u", SIZ_GROSS, lTmp);
      memcpy(pOutbuf+OFF_GROSS, acTmp, SIZ_GROSS);
   }

   // Ratio
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*d", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
      memcpy(pOutbuf+OFF_RATIO, acTmp, SIZ_RATIO);
   }

   // Exemption
   *(pOutbuf+OFF_HO_FL) = '2';            // N
   lTmp = atol(apTokens[VEN_L_EXEAMOUNT]); 
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*u", SIZ_EXE_TOTAL, lTmp);
      memcpy(pOutbuf+OFF_EXE_TOTAL, acTmp, SIZ_EXE_TOTAL);
      if (*apTokens[VEN_L_EXETYPENAME] == 'H')
         *(pOutbuf+OFF_HO_FL) = '1';      // Y
   }

   // Full exemption flag
   if (strstr(apTokens[VEN_L_LANDUMTN], "WHOLLY EXEMPT"))
      *(pOutbuf+OFF_FULL_EXEMPT) = 'Y';

   // Legal
   if (*apTokens[VEN_L_LEGALDESC] > ' ')
   {
      iTmp = updateLegal(pOutbuf, apTokens[VEN_L_LEGALDESC]);
   }

   // UseCode
   if (*apTokens[VEN_L_USECODE] > ' ')
   {
      vmemcpy(pOutbuf+OFF_USE_CO, apTokens[VEN_L_USECODE], SIZ_USE_CO);
      updateStdUse(pOutbuf+OFF_USE_STD, apTokens[VEN_L_USECODE], 0, pOutbuf);
   } else
      memcpy(pOutbuf+OFF_USE_STD, USE_UNASGN, SIZ_USE_STD);

   // Owner
   try
   {
      Ven_MergeOwner(pOutbuf, apTokens[VEN_L_OWNER1], apTokens[VEN_L_OWNER2]);
   } catch(...)
   {
      LogMsg("***** Execption occured in Imp_MergeOwner()");
   }

#ifdef _DEBUG
   //if (!memcmp(apTokens[0], "0160130090", 10))
   //   iTmp = 0;
#endif

   // Situs
   if (*apTokens[VEN_L_FULLSITUS] > ' ' && memcmp(apTokens[VEN_L_FULLSITUS], "NO SITUS", 7) ) 
      Ven_MergeSAdr(pOutbuf, apTokens[VEN_L_FULLSITUS]);

   // Mailing
   if (*apTokens[VEN_L_FULLMAIL] > ' ' && memcmp(apTokens[VEN_L_FULLMAIL], "ADDRESS", 7) 
      && !strstr(apTokens[VEN_L_FULLMAIL], "BLANK"))
      Ven_MergeMAdr(pOutbuf);

   // Recorded Doc
   if (*apTokens[VEN_L_DOCNUM] == 'R' && *apTokens[VEN_L_DOCDATE] > ' ')
   {
      vmemcpy(pOutbuf+OFF_TRANSFER_DOC, apTokens[VEN_L_DOCNUM]+2, 9);
      if (apTokens[VEN_L_DOCDATE][4] == '-')
         dateConversion(apTokens[VEN_L_DOCDATE], acTmp, YYYY_MM_DD);
      else
         dateConversion(apTokens[VEN_L_DOCDATE], acTmp, MM_DD_YYYY_1);

      if (acTmp[0] > ' ')
      {
         memcpy(pOutbuf+OFF_TRANSFER_DT, acTmp, SIZ_TRANSFER_DT);
         lTmp = atol(acTmp);
         if (lTmp > lLastRecDate && lTmp < lToday)
            lLastRecDate = lTmp;
      }
   }

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "0020801550", 9))
   //   iTmp = 0;
#endif
   // LotSize
   dTmp = atof(apTokens[VEN_L_IMPRAREA1]);
   if (dTmp > 0)
   {
      // Lot Sqft
      if (dTmp < 999999999)
      {
         sprintf(acTmp, "%*u", SIZ_LOT_SQFT, (long)dTmp);
         memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);
      } else
         LogMsg("*** MergeLien->Ignore LotSqft too big %u [%.12s]", lTmp, pOutbuf);

      // Format Acres
      lTmp = (unsigned long)(dTmp / SQFT_FACTOR_1000);
      sprintf(acTmp, "%*u", SIZ_LOT_ACRES, lTmp);
      memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);
   }

   // BldgSqft
   lTmp = atol(apTokens[VEN_L_IMPRAREA2]);
   if (lTmp > 0)
   {
      if (!memcmp(apTokens[VEN_L_FORMTYPE2], "SINGLE", 6) 
         || !memcmp(apTokens[VEN_L_FORMTYPE2], "MANUF", 5)
         || !memcmp(apTokens[VEN_L_FORMTYPE2], "INDUST", 5))
      {
         sprintf(acTmp, "%*u", SIZ_BLDG_SF, lTmp);
         memcpy(pOutbuf+OFF_BLDG_SF, acTmp, SIZ_BLDG_SF);
         vmemcpy(pOutbuf+OFF_YR_BLT, apTokens[VEN_L_CONSTYEAR2], SIZ_YR_BLT);
      }
   }

   return 0;
}

/****************************** Ven_MergeLien1 ******************************
 *
 * Loading LDR file "2023 Secured Roll.csv"
 *
 ****************************************************************************/

int Ven_MergeLien1(char *pOutbuf, char *pRollRec)
{
   char     acTmp[256], acTmp1[256], acCode[256], *pTmp;
   double   dTmp;
   int      iRet=0, iTmp;
   ULONG    lTmp;

   // Parse input
   iTokens = ParseStringIQ(pRollRec, '|', MAX_FLD_TOKEN, apTokens);
   if (iTokens < VEN_L1_FLDS)
   {
      LogMsg("***** Error: bad input record for %.50s (%d)", pRollRec, iTokens);
      return -1;
   }

   // Clear output buffer
   memset(pOutbuf, ' ', iRecLen);

   // unformatted APN
   vmemcpy(pOutbuf+OFF_ALT_APN, apTokens[VEN_L1_APN], iApnLen);
   // formatted APN
   iRet = Ven_FormatApn(apTokens[VEN_L1_APN], acTmp);
   memcpy(pOutbuf+OFF_APN_S, acTmp, iRet);
   // Display APN
   iRet = formatApn(acTmp, acTmp1, &myCounty);
   memcpy(pOutbuf+OFF_APN_D, acTmp1, iRet);

   // Create MapLink and output new record
   iRet = formatMapLink(acTmp, acTmp1, &myCounty);
   memcpy(pOutbuf+OFF_MAPLINK, acTmp1, iRet);

   // Create index map link
   if (getIndexPage(acTmp1, acTmp, &myCounty))
      memcpy(pOutbuf+OFF_IMAPLINK, acTmp, iRet);

   // County code
   memcpy(pOutbuf+OFF_CO_NUM, "56VENA", 6);

   // Year assessed
   memcpy(pOutbuf+OFF_YR_ASSD, myCounty.acYearAssd, 4);

   // TRA
   lTmp = atol(apTokens[VEN_L1_TRA]);
   if (lTmp > 0)
   {
      iRet = sprintf(acTmp, "%.6d", lTmp);
      vmemcpy(pOutbuf+OFF_TRA, acTmp, iRet);
   }

   // Land
   ULONG lLand = atoi(apTokens[VEN_L1_LAND]);
   if (lLand > 0)
   {
      sprintf(acTmp, "%*u", SIZ_LAND, lLand);
      memcpy(pOutbuf+OFF_LAND, acTmp, SIZ_LAND);
   }

   // Improve
   ULONG lImpr = atoi(apTokens[VEN_L1_IMPR]);
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*u", SIZ_IMPR, lImpr);
      memcpy(pOutbuf+OFF_IMPR, acTmp, SIZ_IMPR);
   }

   ULONG lFixt = atoi(apTokens[VEN_L1_FIXTURE]);
   ULONG lPP   = atoi(apTokens[VEN_L1_PERSPROP]);
   ULONG lTree = atoi(apTokens[VEN_L1_TREEVINES]);
   ULONG lMineral = atoi(apTokens[VEN_L1_MINERALRIGHTS]);
   lTmp = lPP+lFixt+lTree+lMineral;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*u", SIZ_OTHER, lTmp);
      memcpy(pOutbuf+OFF_OTHER, acTmp, SIZ_OTHER);
      if (lPP > 0)
      {
         sprintf(acTmp, "%*u", SIZ_PERSPROP, lPP);
         memcpy(pOutbuf+OFF_PERSPROP, acTmp, SIZ_PERSPROP);
      }
      if (lFixt > 0)
      {
         sprintf(acTmp, "%*u", SIZ_FIXTR, lFixt);
         memcpy(pOutbuf+OFF_FIXTR, acTmp, SIZ_FIXTR);
      }
      if (lTree > 0)
      {
         sprintf(acTmp, "%*u", SIZ_TREEVINES, lTree);
         memcpy(pOutbuf+OFF_TREEVINES, acTmp, SIZ_TREEVINES);
      }
      if (lMineral > 0)
      {
         sprintf(acTmp, "%*u", SIZ_MINERAL, lMineral);
         memcpy(pOutbuf+OFF_MINERAL, acTmp, SIZ_MINERAL);
      }
   }

   // Gross total
   lTmp += lLand+lImpr;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*u", SIZ_GROSS, lTmp);
      memcpy(pOutbuf+OFF_GROSS, acTmp, SIZ_GROSS);
   }

   // Ratio
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*d", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
      memcpy(pOutbuf+OFF_RATIO, acTmp, SIZ_RATIO);
   }

   // Exemption - HO, HOSP CEM, CH, COL, D.VET, MUS, O.CON, P.SCH, REL, SCH, WEL
   *(pOutbuf+OFF_HO_FL) = '2';            // N
   long lExe1 = atol(apTokens[VEN_L1_EXEAMT1]); 
   long lExe2 = atol(apTokens[VEN_L1_EXEAMT2]); 
   long lExe3 = atol(apTokens[VEN_L1_EXEAMT3]); 
   lTmp = lExe1 + lExe2 + lExe3;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*u", SIZ_EXE_TOTAL, lTmp);
      memcpy(pOutbuf+OFF_EXE_TOTAL, acTmp, SIZ_EXE_TOTAL);
      if (!strcmp(apTokens[VEN_L1_EXE_CD1], "HO"))
         *(pOutbuf+OFF_HO_FL) = '1';      // Y
   }

   // Full exemption flag
   if (*apTokens[VEN_L1_NON_TAX_CD] > ' ')
      *(pOutbuf+OFF_FULL_EXEMPT) = 'Y';

   // Legal - combine of various fields
   // 6113200450= Tract: 287403 CondoRef: 83-63233 Lot: 102 MapNr: 90 MR 63 Unit: -409 
   //                    Tract  Condo_Bldg-Condo_Unit   Lot        Map_Nr         Condo_Ref
   // 6113310350= Tract: 390607 :  Lot: 113 MapNr: 100MR 88
   // 0530733000= Block: 3 Lot: 4 MapNr: 3MR10
   // 2070380065= Tract: 318101 MapNr: 083MR 051 Unit: -609
   char sLegal[256], sUnit[32];
   sLegal[0] = 0;
   sUnit[0] = 0;
   if (*apTokens[VEN_L1_TRACT] > ' ')
   {
      vmemcpy(pOutbuf+OFF_TRACT, apTokens[VEN_L1_TRACT], SIZ_TRACT);
      sprintf(sLegal, "Tract: %s ", apTokens[VEN_L1_TRACT]);
   }
   if (*apTokens[VEN_L1_BLK] > ' ')
   {
      vmemcpy(pOutbuf+OFF_BLOCK, apTokens[VEN_L1_BLK], SIZ_BLOCK);
      sprintf(acTmp, "%sBlk: %s ", sLegal, apTokens[VEN_L1_BLK]);
      strcpy(sLegal, acTmp);
   }
   if (*apTokens[VEN_L1_LOT] > ' ')
   {
      vmemcpy(pOutbuf+OFF_LOT, apTokens[VEN_L1_LOT], SIZ_LOT);
      sprintf(acTmp, "%sLot: %s ", sLegal, apTokens[VEN_L1_LOT]);
      strcpy(sLegal, acTmp);
   }
   if (*apTokens[VEN_L1_CONDO_BLDG] > ' ')
   {
      sprintf(acTmp, "%sCondoRef: %s-%s ", sLegal, apTokens[VEN_L1_CONDO_BLDG], apTokens[VEN_L1_CONDO_UNIT]);
      strcpy(sLegal, acTmp);
   }
   if (*apTokens[VEN_L1_MAP_NR] > ' ')
   {
      sprintf(acTmp, "%sMapNr: %s ", sLegal, apTokens[VEN_L1_MAP_NR]);
      strcpy(sLegal, acTmp);
   }
   if (*apTokens[VEN_L1_CONDO_REF] > ' ' && strlen(apTokens[VEN_L1_CONDO_REF]) > 2 && *apTokens[VEN_L1_CONDO_BLDG] < ' ')
   {
      sprintf(acTmp, "%sUnit: %s", sLegal, apTokens[VEN_L1_CONDO_REF]);
      strcpy(sLegal, acTmp);
   }
   if (sLegal[0] > ' ')
      iTmp = updateLegal(pOutbuf, sLegal);

   // UseCode
   if (*apTokens[VEN_L1_USECODE] > ' ')
   {
      vmemcpy(pOutbuf+OFF_USE_CO, apTokens[VEN_L1_USECODE], SIZ_USE_CO);
      updateStdUse(pOutbuf+OFF_USE_STD, apTokens[VEN_L1_USECODE], 0, pOutbuf);
   } else
      memcpy(pOutbuf+OFF_USE_STD, USE_UNASGN, SIZ_USE_STD);

   // Owner
   try
   {
      Ven_MergeOwner(pOutbuf, apTokens[VEN_L1_NAME1], apTokens[VEN_L1_NAME2]);
   } catch(...)
   {
      LogMsg("***** Execption occured in Imp_MergeOwner()");
   }

#ifdef _DEBUG
   //if (!memcmp(apTokens[0], "0020080155", 9))
   //   iTmp = 0;
#endif

   // Situs
   if (*apTokens[VEN_L1_S_STREET] > ' ') 
      Ven_MergeSAdr1(pOutbuf);

   // Mailing - ignore "BLANK PURSUANT - CA GC6254.21"
   if (*apTokens[VEN_L1_MAIL_ADDR] > ' ' && memcmp(apTokens[VEN_L1_MAIL_ADDR], "BLANK", 5))
      Ven_MergeMAdr1(pOutbuf);

   // Recorded Doc - DocNum is in various forms
   // 1910390955: 20210902001637980 20210902
   // 6500032305: 20210930001798240 20210930
   // 1830342185: 2021091400170008  20210914
   // 6500032445: 2022000018025     20220210
   // 1390401115: 2023000015863     20230228
   // 6500032165: 010128872         20010706
   // 2160232085: C-03696695        20030508
   // 2180020545: C-03700003        19760412
   if (*apTokens[VEN_L1_DOC_NR] > ' ' && *apTokens[VEN_L1_DOCDATE] > ' ')
   {
      iTmp = strlen(apTokens[VEN_L1_DOC_NR]);
      if (*apTokens[VEN_L1_DOC_NR] > '9')
      {
         if (iTmp == 9)
            memcpy(pOutbuf+OFF_TRANSFER_DOC, apTokens[VEN_L1_DOC_NR], 9);
         else if (iTmp > 12)
         {
            memcpy(pOutbuf+OFF_TRANSFER_DOC, apTokens[VEN_L1_DOC_NR]+2, 2);
            memcpy(pOutbuf+OFF_TRANSFER_DOC, apTokens[VEN_L1_DOC_NR]+(iTmp-7), 7);
         }
      } else
      {
         memcpy(pOutbuf+OFF_TRANSFER_DOC, apTokens[VEN_L1_DOC_NR]+(iTmp-7), 7);
      }
      vmemcpy(pOutbuf+OFF_TRANSFER_DT, apTokens[VEN_L1_DOCDATE], SIZ_TRANSFER_DT);
      lTmp = atol(apTokens[VEN_L1_DOCDATE]);
      if (lTmp > lLastRecDate && lTmp < lToday)
         lLastRecDate = lTmp;
   }

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "0020801550", 9))
   //   iTmp = 0;
#endif
   // LotSize
   lTmp = atol(apTokens[VEN_L1_LOTSQFT]);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*u", SIZ_LOT_SQFT, lTmp);
      memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);
   }

   // LotAcres
   dTmp = atof(apTokens[VEN_L1_ACREAGE]);
   if (dTmp > 0.0)
   {
      // Format Acres
      lTmp = (unsigned long)(dTmp*1000.0);
      sprintf(acTmp, "%*u", SIZ_LOT_ACRES, lTmp);
      memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);
   }

   // BldgSqft
   lTmp = atol(apTokens[VEN_L1_SQ_FT_I]);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*u", SIZ_BLDG_SF, lTmp);
      memcpy(pOutbuf+OFF_BLDG_SF, acTmp, SIZ_BLDG_SF);
   }

   // YrBlt
   if (*apTokens[VEN_L1_YR_BLT] >= '1')
      vmemcpy(pOutbuf+OFF_YR_BLT, apTokens[VEN_L1_YR_BLT], SIZ_YR_BLT);

   // Quality Class
   if (*apTokens[VEN_L1_QC] > ' ')
   {
      pTmp = apTokens[VEN_L1_QC];
      vmemcpy(pOutbuf+OFF_QUALITYCLASS, pTmp, SIZ_QUALITYCLASS);
      *(pOutbuf+OFF_BLDG_CLASS) = *pTmp;
      if (isdigit(*(pTmp+1)))
      {
         iRet = Quality2Code(pTmp+1, acCode, NULL);
         vmemcpy(pOutbuf+OFF_BLDG_QUAL, acCode, SIZ_BLDG_QUAL);
      }
   } 

   // Yrblt
   if (*apTokens[VEN_L1_YR_BLT] > '0')
      vmemcpy(pOutbuf+OFF_YR_BLT, apTokens[VEN_L1_YR_BLT], SIZ_YR_BLT);

   // BldgSqft
   long lBldgSqft = atol(apTokens[VEN_L1_SQ_FT_I]);
   if (lBldgSqft > 10)
   {
      sprintf(acTmp, "%*d", SIZ_BLDG_SF, lBldgSqft);
      memcpy(pOutbuf+OFF_BLDG_SF, acTmp, SIZ_BLDG_SF);
   } 

   // Garage Sqft
   long lGarSqft = atol(apTokens[VEN_L1_GAR_A]);
   if (lGarSqft > 10)
   {
      sprintf(acTmp, "%*d", SIZ_GAR_SQFT, lGarSqft);
      memcpy(pOutbuf+OFF_GAR_SQFT, acTmp, SIZ_GAR_SQFT);
      *(pOutbuf+OFF_PARK_TYPE) = 'Z';
   } else
   {
      lGarSqft = atol(apTokens[VEN_L1_CARPT_A]);
      if (lGarSqft > 10)
      {
         sprintf(acTmp, "%*u", SIZ_GAR_SQFT, lGarSqft);
         memcpy(pOutbuf+OFF_GAR_SQFT, acTmp, SIZ_GAR_SQFT);
         *(pOutbuf+OFF_PARK_TYPE) = 'C';
      }
   }

   // Beds
   int iBeds = atol(apTokens[VEN_L1_BEDS]);
   if (iBeds > 0)
   {
      sprintf(acTmp, "%*u", SIZ_BEDS, iBeds);
      memcpy(pOutbuf+OFF_BEDS, acTmp, SIZ_BEDS);
   }

   // Bath
   int iBath = atol(apTokens[VEN_L1_BATHS]);
   int iHBath;
   if (iBath > 0)
   {
      if (iBath > 999)
      {
         memcpy(pOutbuf+OFF_BATH_F, apTokens[VEN_L1_BATHS], 2);
         memcpy(pOutbuf+OFF_BATH_4Q, apTokens[VEN_L1_BATHS], 2);
         iHBath = 2;
      } else
      {
         *(pOutbuf+OFF_BATH_F) = *apTokens[VEN_L1_BATHS];
         *(pOutbuf+OFF_BATH_4Q) = *apTokens[VEN_L1_BATHS];
         iHBath = 1;
      }
      iTmp = atol(apTokens[VEN_L1_BATHS]+iHBath);
      if (iTmp > 0)
      {
         *(pOutbuf+OFF_BATH_H) = '1';
         if (iTmp == 50)
            *(pOutbuf+OFF_BATH_2Q) = '1';
         else if (iTmp > 50)
            *(pOutbuf+OFF_BATH_3Q) = '1';
         else if (iTmp < 50)
            *(pOutbuf+OFF_BATH_1Q) = '1';
      }
   }
   
   // Total rooms
   iTmp = atol(apTokens[VEN_L1_TOT_RMS]);
   if (iTmp > 0)
   {
      sprintf(acTmp, "%*u", SIZ_ROOMS, iTmp);
      memcpy(pOutbuf+OFF_ROOMS, acTmp, SIZ_ROOMS);
   }

   // Fireplace
   iTmp = atol(apTokens[VEN_L1_FP]);
   if (iTmp > 9)
      *(pOutbuf+OFF_FIRE_PL) = 'M';
   else if (iTmp > 0)
      *(pOutbuf+OFF_FIRE_PL) = '0' | iTmp;

   // Pool
   if (*apTokens[VEN_L1_POOL_A] > '0')
      *(pOutbuf+OFF_POOL) = 'P';

   // Stories
   int iStories=0;
   int iFlr3 = atol(apTokens[VEN_L1_FLR_3_A]);
   int iFlr2 = atol(apTokens[VEN_L1_FLR_2_A]);
   int iFlr1 = atol(apTokens[VEN_L1_FLR_1_A]);
   if (iFlr1 > 0)
   {
      sprintf(acTmp, "%*u", SIZ_FLOOR_SF, iFlr1);
      memcpy(pOutbuf+OFF_FLOOR1_SF, acTmp, SIZ_FLOOR_SF);
      iStories = 1;
      if (iFlr2 > 0)
      {
         sprintf(acTmp, "%*u", SIZ_FLOOR_SF, iFlr2);
         memcpy(pOutbuf+OFF_FLOOR2_SF, acTmp, SIZ_FLOOR_SF);
         iStories = 2;
         if (iFlr3 > 0)
         {
            sprintf(acTmp, "%*u", SIZ_FLOOR_SF, iFlr3);
            memcpy(pOutbuf+OFF_FLOOR3_SF, acTmp, SIZ_FLOOR_SF);
            iStories = 3;
         }
      }
      sprintf(acTmp, "%d.0  ", iStories);
      memcpy(pOutbuf+OFF_STORIES, acTmp, SIZ_STORIES);
   }

   // Patio
   lTmp = atol(apTokens[VEN_L1_PAT_A]);
   lTmp += atol(apTokens[VEN_L1_EN_PAT_A]);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*u", SIZ_PATIO_SF, lTmp);
      memcpy(pOutbuf+OFF_PATIO_SF, acTmp, SIZ_PATIO_SF);
   }

   // Deck
   lTmp = atol(apTokens[VEN_L1_DECK_A]);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*u", SIZ_DECK_SF, lTmp);
      memcpy(pOutbuf+OFF_DECK_SF, acTmp, SIZ_DECK_SF);
   }

   // Porch
   lTmp = atol(apTokens[VEN_L1_POR_A]);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*u", SIZ_PORCH_SF, lTmp);
      memcpy(pOutbuf+OFF_PORCH_SF, acTmp, SIZ_PORCH_SF);
   }

   return 0;
}

/****************************** Ven_MergeLien2 ******************************
 *
 * Loading LDR file "2023 Secured Complete Summary.csv"
 *
 ****************************************************************************/

int Ven_MergeLien2(char *pOutbuf, char *pRollRec)
{
   char     acTmp[256], acTmp1[256], acCode[256], *pTmp;
   double   dTmp;
   int      iRet=0, iTmp;
   ULONG    lTmp;

   // Parse input
   iTokens = ParseStringIQ(pRollRec, '|', MAX_FLD_TOKEN, apTokens);
   if (iTokens < VEN_L2_FLDS)
   {
      LogMsg("***** Error: bad input record for %s (%d)", iTokens);
      return -1;
   }

   // Clear output buffer
   memset(pOutbuf, ' ', iRecLen);

   // unformatted APN
   vmemcpy(pOutbuf+OFF_ALT_APN, apTokens[VEN_L2_APN], iApnLen);
   // formatted APN
   iRet = Ven_FormatApn(apTokens[VEN_L2_APN], acTmp);
   memcpy(pOutbuf+OFF_APN_S, acTmp, iRet);
   // Display APN
   iRet = formatApn(acTmp, acTmp1, &myCounty);
   memcpy(pOutbuf+OFF_APN_D, acTmp1, iRet);

   // Create MapLink and output new record
   iRet = formatMapLink(acTmp, acTmp1, &myCounty);
   memcpy(pOutbuf+OFF_MAPLINK, acTmp1, iRet);

   // Create index map link
   if (getIndexPage(acTmp1, acTmp, &myCounty))
      memcpy(pOutbuf+OFF_IMAPLINK, acTmp, iRet);

   // County code
   memcpy(pOutbuf+OFF_CO_NUM, "56VENA", 6);

   // Year assessed
   memcpy(pOutbuf+OFF_YR_ASSD, myCounty.acYearAssd, 4);

   // TRA
   lTmp = atol(apTokens[VEN_L2_TRA]);
   if (lTmp > 0)
   {
      iRet = sprintf(acTmp, "%.6d", lTmp);
      vmemcpy(pOutbuf+OFF_TRA, acTmp, iRet);
   }

   // Land
   ULONG lLand = atoi(apTokens[VEN_L2_LAND]);
   if (lLand > 0)
   {
      sprintf(acTmp, "%*u", SIZ_LAND, lLand);
      memcpy(pOutbuf+OFF_LAND, acTmp, SIZ_LAND);
   }

   // Improve
   ULONG lImpr = atoi(apTokens[VEN_L2_IMPR]);
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*u", SIZ_IMPR, lImpr);
      memcpy(pOutbuf+OFF_IMPR, acTmp, SIZ_IMPR);
   }

   ULONG lFixt = atoi(apTokens[VEN_L2_FIXTURE]);
   ULONG lPP   = atoi(apTokens[VEN_L2_PERSPROP]);
   ULONG lTree = atoi(apTokens[VEN_L2_TREESVINES]);
   ULONG lMineral = atoi(apTokens[VEN_L2_MINERALRIGHTS]);
   lTmp = lPP+lFixt+lTree+lMineral;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*u", SIZ_OTHER, lTmp);
      memcpy(pOutbuf+OFF_OTHER, acTmp, SIZ_OTHER);
      if (lPP > 0)
      {
         sprintf(acTmp, "%*u", SIZ_PERSPROP, lPP);
         memcpy(pOutbuf+OFF_PERSPROP, acTmp, SIZ_PERSPROP);
      }
      if (lFixt > 0)
      {
         sprintf(acTmp, "%*u", SIZ_FIXTR, lFixt);
         memcpy(pOutbuf+OFF_FIXTR, acTmp, SIZ_FIXTR);
      }
      if (lTree > 0)
      {
         sprintf(acTmp, "%*u", SIZ_TREEVINES, lTree);
         memcpy(pOutbuf+OFF_TREEVINES, acTmp, SIZ_TREEVINES);
      }
      if (lMineral > 0)
      {
         sprintf(acTmp, "%*u", SIZ_MINERAL, lMineral);
         memcpy(pOutbuf+OFF_MINERAL, acTmp, SIZ_MINERAL);
      }
   }

   // Gross total
   lTmp += lLand+lImpr;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*u", SIZ_GROSS, lTmp);
      memcpy(pOutbuf+OFF_GROSS, acTmp, SIZ_GROSS);
   }

   // Ratio
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*d", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
      memcpy(pOutbuf+OFF_RATIO, acTmp, SIZ_RATIO);
   }

#ifdef _DEBUG
   //if (!memcmp(apTokens[0], "0010110215", 9))
   //   iTmp = 0;
#endif

   // Exemption - HO, HOSP CEM, CH, COL, D.VET, MUS, O.CON, P.SCH, REL, SCH, WEL
   long lExe1 = atol(apTokens[VEN_L2_EXEAMT1]); 
   long lExe2 = atol(apTokens[VEN_L2_EXEAMT2]); 
   long lExe3 = atol(apTokens[VEN_L2_EXEAMT3]); 
   lTmp = lExe1 + lExe2 + lExe3;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*u", SIZ_EXE_TOTAL, lTmp);
      memcpy(pOutbuf+OFF_EXE_TOTAL, acTmp, SIZ_EXE_TOTAL);
      if (!memcmp(apTokens[VEN_L2_EXE_CD1], "HO", 2))
         *(pOutbuf+OFF_HO_FL) = '1';            // Y
      else
         *(pOutbuf+OFF_HO_FL) = '2';            // N
   }

   // Full exemption flag
   if (*apTokens[VEN_L2_NON_TAX_CD] > ' ')
      *(pOutbuf+OFF_FULL_EXEMPT) = 'Y';

   // Legal - combine of various fields
   // 6113200450= Tract: 287403 CondoRef: 83-63233 Lot: 102 MapNr: 90 MR 63 Unit: -409 
   //                    Tract  Condo_Bldg-Condo_Unit   Lot        Map_Nr         Condo_Ref
   // 6113310350= Tract: 390607 :  Lot: 113 MapNr: 100MR 88
   // 0530733000= Block: 3 Lot: 4 MapNr: 3MR10
   // 2070380065= Tract: 318101 MapNr: 083MR 051 Unit: -609
   char sLegal[256], sUnit[32];
   sLegal[0] = 0;
   sUnit[0] = 0;
   if (*apTokens[VEN_L2_TRACT] > ' ')
   {
      vmemcpy(pOutbuf+OFF_TRACT, apTokens[VEN_L2_TRACT], SIZ_TRACT);
      sprintf(sLegal, "Tract: %s ", apTokens[VEN_L2_TRACT]);
   }
   if (*apTokens[VEN_L2_BLK] > ' ')
   {
      vmemcpy(pOutbuf+OFF_BLOCK, apTokens[VEN_L2_BLK], SIZ_BLOCK);
      sprintf(acTmp, "%sBlk: %s ", sLegal, apTokens[VEN_L2_BLK]);
      strcpy(sLegal, acTmp);
   }
   if (*apTokens[VEN_L2_LOT] > ' ')
   {
      vmemcpy(pOutbuf+OFF_LOT, apTokens[VEN_L2_LOT], SIZ_LOT);
      sprintf(acTmp, "%sLot: %s ", sLegal, apTokens[VEN_L2_LOT]);
      strcpy(sLegal, acTmp);
   }
   if (*apTokens[VEN_L2_CONDO_BLDG] > ' ')
   {
      sprintf(acTmp, "%sCondoRef: %s-%s ", sLegal, apTokens[VEN_L2_CONDO_BLDG], apTokens[VEN_L2_CONDO_UNIT]);
      strcpy(sLegal, acTmp);
   }
   if (*apTokens[VEN_L2_MAP_NR] > ' ')
   {
      sprintf(acTmp, "%sMapNr: %s ", sLegal, apTokens[VEN_L2_MAP_NR]);
      strcpy(sLegal, acTmp);
   }
   if (*apTokens[VEN_L2_CONDO_REF] > ' ' && strlen(apTokens[VEN_L2_CONDO_REF]) > 2 && *apTokens[VEN_L2_CONDO_BLDG] < ' ')
   {
      sprintf(acTmp, "%sUnit: %s", sLegal, apTokens[VEN_L2_CONDO_REF]);
      strcpy(sLegal, acTmp);
   }
   if (sLegal[0] > ' ')
      iTmp = updateLegal(pOutbuf, sLegal);

   // UseCode
   if (*apTokens[VEN_L2_SITE_USE] > ' ')
   {
      vmemcpy(pOutbuf+OFF_USE_CO, apTokens[VEN_L2_SITE_USE], SIZ_USE_CO);
      updateStdUse(pOutbuf+OFF_USE_STD, apTokens[VEN_L2_SITE_USE], 0, pOutbuf);
   } else
      memcpy(pOutbuf+OFF_USE_STD, USE_UNASGN, SIZ_USE_STD);

   // Owner
   try
   {
      Ven_MergeOwner(pOutbuf, apTokens[VEN_L2_NAME1], apTokens[VEN_L2_NAME2]);
   } catch(...)
   {
      LogMsg("***** Execption occured in Imp_MergeOwner()");
   }

   // Situs
   if (*apTokens[VEN_L2_FULLSITUS] > ' ') 
      Ven_MergeSAdr2(pOutbuf, apTokens[VEN_L2_FULLSITUS], apTokens[VEN_L2_SA]);

   // Mailing - ignore "BLANK PURSUANT - CA GC6254.21"
   if (*apTokens[VEN_L2_MAIL_ADDR] > ' ' && memcmp(apTokens[VEN_L2_MAIL_ADDR], "BLANK", 5))
      Ven_MergeMAdr1(pOutbuf);

   // Recorded Doc - DocNum is in various forms
   // 1910390955: 20210902001637980 20210902
   // 6500032305: 20210930001798240 20210930
   // 1830342185: 2021091400170008  20210914
   // 6500032445: 2022000018025     20220210
   // 1390401115: 2023000015863     20230228
   // 6500032165: 010128872         20010706
   // 2160232085: C-03696695        20030508
   // 2180020545: C-03700003        19760412
   if (*apTokens[VEN_L2_DOC_NR] > ' ' && *apTokens[VEN_L2_DOCDATE] > ' ')
   {
      iTmp = strlen(apTokens[VEN_L2_DOC_NR]);
      if (*apTokens[VEN_L2_DOC_NR] > '9')
      {
         if (iTmp == 9)
            memcpy(pOutbuf+OFF_TRANSFER_DOC, apTokens[VEN_L2_DOC_NR], 9);
         else if (iTmp > 12)
         {
            memcpy(pOutbuf+OFF_TRANSFER_DOC, apTokens[VEN_L2_DOC_NR]+2, 2);
            memcpy(pOutbuf+OFF_TRANSFER_DOC, apTokens[VEN_L2_DOC_NR]+(iTmp-7), 7);
         }
      } else
      {
         memcpy(pOutbuf+OFF_TRANSFER_DOC, apTokens[VEN_L2_DOC_NR]+(iTmp-7), 7);
      }
      vmemcpy(pOutbuf+OFF_TRANSFER_DT, apTokens[VEN_L2_DOCDATE], SIZ_TRANSFER_DT);
      lTmp = atol(apTokens[VEN_L2_DOCDATE]);
      if (lTmp > lLastRecDate && lTmp < lToday)
         lLastRecDate = lTmp;
   }

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "0020801550", 9))
   //   iTmp = 0;
#endif
   // LotSize
   lTmp = atol(apTokens[VEN_L2_LOTSQFT]);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*u", SIZ_LOT_SQFT, lTmp);
      memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);
   }

   // LotAcres
   dTmp = atof(apTokens[VEN_L2_ACREAGE]);
   if (dTmp > 0.0)
   {
      // Format Acres
      lTmp = (unsigned long)(dTmp*1000.0);
      sprintf(acTmp, "%*u", SIZ_LOT_ACRES, lTmp);
      memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);
   }

   // BldgSqft
   lTmp = atol(apTokens[VEN_L2_SQ_FT_I]);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*u", SIZ_BLDG_SF, lTmp);
      memcpy(pOutbuf+OFF_BLDG_SF, acTmp, SIZ_BLDG_SF);
   }

   // YrBlt
   if (*apTokens[VEN_L2_YR_BLT] >= '1')
      vmemcpy(pOutbuf+OFF_YR_BLT, apTokens[VEN_L2_YR_BLT], SIZ_YR_BLT);

   // Quality Class
   if (*apTokens[VEN_L2_QC] > ' ')
   {
      pTmp = apTokens[VEN_L2_QC];
      vmemcpy(pOutbuf+OFF_QUALITYCLASS, pTmp, SIZ_QUALITYCLASS);
      *(pOutbuf+OFF_BLDG_CLASS) = *pTmp;
      if (isdigit(*(pTmp+1)))
      {
         iRet = Quality2Code(pTmp+1, acCode, NULL);
         vmemcpy(pOutbuf+OFF_BLDG_QUAL, acCode, SIZ_BLDG_QUAL);
      }
   } 

   // Yrblt
   if (*apTokens[VEN_L2_YR_BLT] > '0')
      vmemcpy(pOutbuf+OFF_YR_BLT, apTokens[VEN_L2_YR_BLT], SIZ_YR_BLT);

   // BldgSqft
   long lBldgSqft = atol(apTokens[VEN_L2_SQ_FT_I]);
   if (lBldgSqft > 10)
   {
      sprintf(acTmp, "%*d", SIZ_BLDG_SF, lBldgSqft);
      memcpy(pOutbuf+OFF_BLDG_SF, acTmp, SIZ_BLDG_SF);
   } 

   // Garage Sqft
   long lGarSqft = atol(apTokens[VEN_L2_GAR_A]);
   if (lGarSqft > 10)
   {
      sprintf(acTmp, "%*d", SIZ_GAR_SQFT, lGarSqft);
      memcpy(pOutbuf+OFF_GAR_SQFT, acTmp, SIZ_GAR_SQFT);
      *(pOutbuf+OFF_PARK_TYPE) = 'Z';
   } else
   {
      lGarSqft = atol(apTokens[VEN_L2_CARPT_A]);
      if (lGarSqft > 10)
      {
         sprintf(acTmp, "%*u", SIZ_GAR_SQFT, lGarSqft);
         memcpy(pOutbuf+OFF_GAR_SQFT, acTmp, SIZ_GAR_SQFT);
         *(pOutbuf+OFF_PARK_TYPE) = 'C';
      }
   }

   // Beds
   int iBeds = atol(apTokens[VEN_L2_BEDS]);
   if (iBeds > 0)
   {
      sprintf(acTmp, "%*u", SIZ_BEDS, iBeds);
      memcpy(pOutbuf+OFF_BEDS, acTmp, SIZ_BEDS);
   }

   // Bath
   int iBath = atol(apTokens[VEN_L2_BATHS]);
   int iHBath;
   if (iBath > 0)
   {
      if (iBath > 999)
      {
         memcpy(pOutbuf+OFF_BATH_F, apTokens[VEN_L2_BATHS], 2);
         memcpy(pOutbuf+OFF_BATH_4Q, apTokens[VEN_L2_BATHS], 2);
         iHBath = 2;
      } else
      {
         *(pOutbuf+OFF_BATH_F) = *apTokens[VEN_L2_BATHS];
         *(pOutbuf+OFF_BATH_4Q) = *apTokens[VEN_L2_BATHS];
         iHBath = 1;
      }
      iTmp = atol(apTokens[VEN_L2_BATHS]+iHBath);
      if (iTmp > 0)
      {
         *(pOutbuf+OFF_BATH_H) = '1';
         if (iTmp == 50)
            *(pOutbuf+OFF_BATH_2Q) = '1';
         else if (iTmp > 50)
            *(pOutbuf+OFF_BATH_3Q) = '1';
         else if (iTmp < 50)
            *(pOutbuf+OFF_BATH_1Q) = '1';
      }
   }
   
   // Total rooms
   iTmp = atol(apTokens[VEN_L2_TOT_RMS]);
   if (iTmp > 0)
   {
      sprintf(acTmp, "%*u", SIZ_ROOMS, iTmp);
      memcpy(pOutbuf+OFF_ROOMS, acTmp, SIZ_ROOMS);
   }

   // Fireplace
   iTmp = atol(apTokens[VEN_L2_FP]);
   if (iTmp > 9)
      *(pOutbuf+OFF_FIRE_PL) = 'M';
   else if (iTmp > 0)
      *(pOutbuf+OFF_FIRE_PL) = '0' | iTmp;

   // Pool
   if (*apTokens[VEN_L2_POOL_A] > '0')
      *(pOutbuf+OFF_POOL) = 'P';

   // Stories
   int iStories=0;
   int iFlr3 = atol(apTokens[VEN_L2_FLR_3_A]);
   int iFlr2 = atol(apTokens[VEN_L2_FLR_2_A]);
   int iFlr1 = atol(apTokens[VEN_L2_FLR_1_A]);
   if (iFlr1 > 0)
   {
      sprintf(acTmp, "%*u", SIZ_FLOOR_SF, iFlr1);
      memcpy(pOutbuf+OFF_FLOOR1_SF, acTmp, SIZ_FLOOR_SF);
      iStories = 1;
      if (iFlr2 > 0)
      {
         sprintf(acTmp, "%*u", SIZ_FLOOR_SF, iFlr2);
         memcpy(pOutbuf+OFF_FLOOR2_SF, acTmp, SIZ_FLOOR_SF);
         iStories = 2;
         if (iFlr3 > 0)
         {
            sprintf(acTmp, "%*u", SIZ_FLOOR_SF, iFlr3);
            memcpy(pOutbuf+OFF_FLOOR3_SF, acTmp, SIZ_FLOOR_SF);
            iStories = 3;
         }
      }
      sprintf(acTmp, "%d.0  ", iStories);
      memcpy(pOutbuf+OFF_STORIES, acTmp, SIZ_STORIES);
   }

   // Patio
   lTmp = atol(apTokens[VEN_L2_PAT_A]);
   lTmp += atol(apTokens[VEN_L2_EN_PAT_A]);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*u", SIZ_PATIO_SF, lTmp);
      memcpy(pOutbuf+OFF_PATIO_SF, acTmp, SIZ_PATIO_SF);
   }

   // Deck
   lTmp = atol(apTokens[VEN_L2_DECK_A]);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*u", SIZ_DECK_SF, lTmp);
      memcpy(pOutbuf+OFF_DECK_SF, acTmp, SIZ_DECK_SF);
   }

   // Porch
   lTmp = atol(apTokens[VEN_L2_POR_A]);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*u", SIZ_PORCH_SF, lTmp);
      memcpy(pOutbuf+OFF_PORCH_SF, acTmp, SIZ_PORCH_SF);
   }

   return 0;
}

/****************************** Ven_MergeLien3 ******************************
 *
 * Loading LDR file "2024-25 Secured Roll.csv"
 *
 ****************************************************************************/

int Ven_MergeLien3(char *pOutbuf, char *pRollRec)
{
   char     acTmp[256], acTmp1[256];
   double   dTmp;
   int      iRet=0, iTmp;
   ULONG    lTmp;

   // Remove NULL
   replStrAll(pRollRec, "NULL", "");
   
   // Parse input
   iTokens = ParseStringIQ(pRollRec, '|', MAX_FLD_TOKEN, apTokens);
   if (iTokens < VEN_L3_FLDS)
   {
      LogMsg("***** Error: bad input record for %s (%d)", iTokens);
      return -1;
   }

   // Clear output buffer
   memset(pOutbuf, ' ', iRecLen);

   // unformatted APN
   vmemcpy(pOutbuf+OFF_ALT_APN, apTokens[VEN_L3_APN], iApnLen);
   // formatted APN
   iRet = Ven_FormatApn(apTokens[VEN_L3_APN], acTmp);
   memcpy(pOutbuf+OFF_APN_S, acTmp, iRet);
   // Display APN
   iRet = formatApn(acTmp, acTmp1, &myCounty);
   memcpy(pOutbuf+OFF_APN_D, acTmp1, iRet);

   // Create MapLink and output new record
   iRet = formatMapLink(acTmp, acTmp1, &myCounty);
   memcpy(pOutbuf+OFF_MAPLINK, acTmp1, iRet);

   // Create index map link
   if (getIndexPage(acTmp1, acTmp, &myCounty))
      memcpy(pOutbuf+OFF_IMAPLINK, acTmp, iRet);

   // County code
   memcpy(pOutbuf+OFF_CO_NUM, "56VENA", 6);

   // Year assessed
   memcpy(pOutbuf+OFF_YR_ASSD, myCounty.acYearAssd, 4);

   // TRA
   lTmp = atol(apTokens[VEN_L3_TRA]);
   if (lTmp > 0)
   {
      iRet = sprintf(acTmp, "%.6d", lTmp);
      vmemcpy(pOutbuf+OFF_TRA, acTmp, iRet);
   }

   // Land
   ULONG lLand = atoi(apTokens[VEN_L3_LAND]);
   if (lLand > 0)
   {
      sprintf(acTmp, "%*u", SIZ_LAND, lLand);
      memcpy(pOutbuf+OFF_LAND, acTmp, SIZ_LAND);
   }

   // Improve
   ULONG lImpr = atoi(apTokens[VEN_L3_IMPR]);
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*u", SIZ_IMPR, lImpr);
      memcpy(pOutbuf+OFF_IMPR, acTmp, SIZ_IMPR);
   }

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "0181212900", 10))
   //   iTmp = 0;
#endif

   ULONG lFixt = atoi(apTokens[VEN_L3_FIXTURE]);
   ULONG lPP   = atoi(apTokens[VEN_L3_PERSPROP]);
   ULONG lTree = atoi(apTokens[VEN_L3_TREESVINES]);
   ULONG lMineral = atoi(apTokens[VEN_L3_MINERALRIGHTS]);
   lTmp = lPP+lFixt+lTree+lMineral;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*u", SIZ_OTHER, lTmp);
      memcpy(pOutbuf+OFF_OTHER, acTmp, SIZ_OTHER);
      if (lPP > 0)
      {
         sprintf(acTmp, "%*u", SIZ_PERSPROP, lPP);
         memcpy(pOutbuf+OFF_PERSPROP, acTmp, SIZ_PERSPROP);
      }
      if (lFixt > 0)
      {
         sprintf(acTmp, "%*u", SIZ_FIXTR, lFixt);
         memcpy(pOutbuf+OFF_FIXTR, acTmp, SIZ_FIXTR);
      }
      if (lTree > 0)
      {
         sprintf(acTmp, "%*u", SIZ_TREEVINES, lTree);
         memcpy(pOutbuf+OFF_TREEVINES, acTmp, SIZ_TREEVINES);
      }
      if (lMineral > 0)
      {
         sprintf(acTmp, "%*u", SIZ_MINERAL, lMineral);
         memcpy(pOutbuf+OFF_MINERAL, acTmp, SIZ_MINERAL);
      }
   }

   // Gross total
   lTmp += lLand+lImpr;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*u", SIZ_GROSS, lTmp);
      memcpy(pOutbuf+OFF_GROSS, acTmp, SIZ_GROSS);
   }

   // Ratio
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*d", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
      memcpy(pOutbuf+OFF_RATIO, acTmp, SIZ_RATIO);
   }

   // Exemption - HO, HOSP CEM, CH, COL, D.VET, MUS, O.CON, P.SCH, REL, SCH, WEL
   long lExe1 = atol(apTokens[VEN_L3_EXEAMT1]); 
   long lExe2 = atol(apTokens[VEN_L3_EXEAMT2]); 
   long lExe3 = atol(apTokens[VEN_L3_EXEAMT3]); 
   lTmp = lExe1 + lExe2 + lExe3;
   *(pOutbuf+OFF_HO_FL) = '2';            // N
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*u", SIZ_EXE_TOTAL, lTmp);
      memcpy(pOutbuf+OFF_EXE_TOTAL, acTmp, SIZ_EXE_TOTAL);
      if (!memcmp(apTokens[VEN_L3_EXE_CD1], "HOM", 3) || !memcmp(apTokens[VEN_L3_EXE_CD2], "HOM", 3))
         *(pOutbuf+OFF_HO_FL) = '1';            // Y

      memcpy(pOutbuf+OFF_EXE_CD1, apTokens[VEN_L3_EXE_CD1], 3);
      if (lExe2 > 0)
         memcpy(pOutbuf+OFF_EXE_CD2, apTokens[VEN_L3_EXE_CD2], 3);
      if (lExe3 > 0)
         memcpy(pOutbuf+OFF_EXE_CD3, apTokens[VEN_L3_EXE_CD3], 3);

      // Create exemption type
      makeExeType(pOutbuf+OFF_EXE_TYPE, pOutbuf+OFF_EXE_CD1, pOutbuf+OFF_EXE_CD2, pOutbuf+OFF_EXE_CD3, (IDX_TBL4 *)&VEN_Exemption);
   }

   // Full exemption flag
   //if (*apTokens[VEN_L3_NON_TAX_CD] > ' ')
   //   *(pOutbuf+OFF_FULL_EXEMPT) = 'Y';

   // Legal - combine of various fields
   // 6113200450= Tract: 287403 CondoRef: 83-63233 Lot: 102 MapNr: 90 MR 63 Unit: -409 
   //                    Tract  Condo_Bldg-Condo_Unit   Lot        Map_Nr         Condo_Ref
   // 6113310350= Tract: 390607 :  Lot: 113 MapNr: 100MR 88
   // 0530733000= Block: 3 Lot: 4 MapNr: 3MR10
   // 2070380065= Tract: 318101 MapNr: 083MR 051 Unit: -609
   char sLegal[256], sUnit[32];
   sLegal[0] = 0;
   sUnit[0] = 0;
   if (*apTokens[VEN_L3_TRACT] > ' ')
   {
      vmemcpy(pOutbuf+OFF_TRACT, apTokens[VEN_L3_TRACT], SIZ_TRACT);
      sprintf(sLegal, "Tract: %s ", apTokens[VEN_L3_TRACT]);
   }
   //if (*apTokens[VEN_L3_BLK] > ' ')
   //{
   //   vmemcpy(pOutbuf+OFF_BLOCK, apTokens[VEN_L3_BLK], SIZ_BLOCK);
   //   sprintf(acTmp, "%sBlk: %s ", sLegal, apTokens[VEN_L3_BLK]);
   //   strcpy(sLegal, acTmp);
   //}
   if (*apTokens[VEN_L3_LOT] > ' ')
   {
      vmemcpy(pOutbuf+OFF_LOT, apTokens[VEN_L3_LOT], SIZ_LOT);
      sprintf(acTmp, "%sLot: %s ", sLegal, apTokens[VEN_L3_LOT]);
      strcpy(sLegal, acTmp);
   }
   //if (*apTokens[VEN_L3_CONDO_BLDG] > ' ')
   //{
   //   sprintf(acTmp, "%sCondoRef: %s-%s ", sLegal, apTokens[VEN_L3_CONDO_BLDG], apTokens[VEN_L3_CONDO_UNIT]);
   //   strcpy(sLegal, acTmp);
   //}
   if (*apTokens[VEN_L3_MAP_NR] > ' ')
   {
      sprintf(acTmp, "%sMapNr: %s ", sLegal, apTokens[VEN_L3_MAP_NR]);
      strcpy(sLegal, acTmp);
   }
   if (*apTokens[VEN_L3_CONDO_REF] > ' ' && strlen(apTokens[VEN_L3_CONDO_REF]) > 2)
   {
      sprintf(acTmp, "%sUnit: %s", sLegal, apTokens[VEN_L3_CONDO_REF]);
      strcpy(sLegal, acTmp);
   }
   if (sLegal[0] > ' ')
      iTmp = updateLegal(pOutbuf, sLegal);

   // UseCode
   if (*apTokens[VEN_L3_SITE_USE] > ' ')
   {
      vmemcpy(pOutbuf+OFF_USE_CO, apTokens[VEN_L3_SITE_USE], SIZ_USE_CO);
      updateStdUse(pOutbuf+OFF_USE_STD, apTokens[VEN_L3_SITE_USE], 0, pOutbuf);
   } else
      memcpy(pOutbuf+OFF_USE_STD, USE_UNASGN, SIZ_USE_STD);

   // Owner
   try
   {
      Ven_MergeOwner(pOutbuf, apTokens[VEN_L3_NAME1], apTokens[VEN_L3_NAME2]);
   } catch(...)
   {
      LogMsg("***** Execption occured in Imp_MergeOwner()");
   }

   // Situs
   if (*apTokens[VEN_L3_S_ADDR] > ' ') 
      Ven_MergeSAdr3(pOutbuf);

   // Mailing - ignore "BLANK PURSUANT - CA GC6254.21"
   if (*apTokens[VEN_L3_M_ADDR] > ' ' && memcmp(apTokens[VEN_L3_M_ADDR], "BLANK", 5))
      Ven_MergeMAdr3(pOutbuf);

   // Recorded Doc - DocNum is in various forms
   // 1910390955: 20210902001637980 20210902
   // 6500032305: 20210930001798240 20210930
   // 1830342185: 2021091400170008  20210914
   // 6500032445: 2022000018025     20220210
   // 1390401115: 2023000015863     20230228
   // 6500032165: 010128872         20010706
   // 2160232085: C-03696695        20030508
   // 2180020545: C-03700003        19760412

   // Skip bad doc
   if (*apTokens[VEN_L3_DOC_NR] > '9' ||
      strstr(apTokens[VEN_L3_DOC_NR], "ERROR") || 
      strstr(apTokens[VEN_L3_DOC_NR], "-MX"))
      *apTokens[VEN_L3_DOC_NR] = ' ';

   if (*apTokens[VEN_L3_DOC_NR] > ' ' && *apTokens[VEN_L3_DOCDATE] > ' ')
   {
      iTmp = strlen(apTokens[VEN_L3_DOC_NR]);
      if (*apTokens[VEN_L3_DOC_NR] > '9')
      {
         if (iTmp == 9)
            memcpy(pOutbuf+OFF_TRANSFER_DOC, apTokens[VEN_L3_DOC_NR], 9);
         else if (iTmp > 12)
         {
            memcpy(pOutbuf+OFF_TRANSFER_DOC, apTokens[VEN_L3_DOC_NR]+2, 2);
            memcpy(pOutbuf+OFF_TRANSFER_DOC, apTokens[VEN_L3_DOC_NR]+(iTmp-7), 7);
         }
      } else
      {
         memcpy(pOutbuf+OFF_TRANSFER_DOC, apTokens[VEN_L3_DOC_NR], iTmp);
      }
      vmemcpy(pOutbuf+OFF_TRANSFER_DT, apTokens[VEN_L3_DOCDATE], SIZ_TRANSFER_DT);
      lTmp = atol(apTokens[VEN_L3_DOCDATE]);
      if (lTmp > lLastRecDate && lTmp < lToday)
         lLastRecDate = lTmp;
   }

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "0020801550", 9))
   //   iTmp = 0;
#endif
   // LotSize
   lTmp = atol(apTokens[VEN_L3_LOTSQFT]);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*u", SIZ_LOT_SQFT, lTmp);
      memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);
   }

   // LotAcres
   dTmp = atof(apTokens[VEN_L3_ACREAGE]);
   if (dTmp > 0.0)
   {
      // Format Acres
      lTmp = (unsigned long)(dTmp*1000.0);
      sprintf(acTmp, "%*u", SIZ_LOT_ACRES, lTmp);
      memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);
   }

   // BldgSqft
   lTmp = atol(apTokens[VEN_L3_BLDGSQFT]);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*u", SIZ_BLDG_SF, lTmp);
      memcpy(pOutbuf+OFF_BLDG_SF, acTmp, SIZ_BLDG_SF);
   }

   // Yrblt
   if (*apTokens[VEN_L3_YR_BLT] > '0')
      vmemcpy(pOutbuf+OFF_YR_BLT, apTokens[VEN_L3_YR_BLT], SIZ_YR_BLT);

   // Garage Sqft
   long lGarSqft = atol(apTokens[VEN_L3_GAR_A]);
   if (lGarSqft > 10)
   {
      sprintf(acTmp, "%*d", SIZ_GAR_SQFT, lGarSqft);
      memcpy(pOutbuf+OFF_GAR_SQFT, acTmp, SIZ_GAR_SQFT);
      *(pOutbuf+OFF_PARK_TYPE) = 'Z';
   } else
   {
      lGarSqft = atol(apTokens[VEN_L3_CARPT_A]);
      if (lGarSqft > 10)
      {
         sprintf(acTmp, "%*u", SIZ_GAR_SQFT, lGarSqft);
         memcpy(pOutbuf+OFF_GAR_SQFT, acTmp, SIZ_GAR_SQFT);
         *(pOutbuf+OFF_PARK_TYPE) = 'C';
      }
   }

   // Beds
   int iBeds = atol(apTokens[VEN_L3_BEDS]);
   if (iBeds > 0)
   {
      sprintf(acTmp, "%*u", SIZ_BEDS, iBeds);
      memcpy(pOutbuf+OFF_BEDS, acTmp, SIZ_BEDS);
   }

   // Bath
   int iBath = atol(apTokens[VEN_L3_BATHS]);
   int iHBath;
   if (iBath > 0)
   {
      if (iBath > 999)
      {
         memcpy(pOutbuf+OFF_BATH_F, apTokens[VEN_L3_BATHS], 2);
         memcpy(pOutbuf+OFF_BATH_4Q, apTokens[VEN_L3_BATHS], 2);
         iHBath = 2;
      } else
      {
         *(pOutbuf+OFF_BATH_F) = *apTokens[VEN_L3_BATHS];
         *(pOutbuf+OFF_BATH_4Q) = *apTokens[VEN_L3_BATHS];
         iHBath = 1;
      }
      iTmp = atol(apTokens[VEN_L3_BATHS]+iHBath);
      if (iTmp > 0)
      {
         *(pOutbuf+OFF_BATH_H) = '1';
         if (iTmp == 50)
            *(pOutbuf+OFF_BATH_2Q) = '1';
         else if (iTmp > 50)
            *(pOutbuf+OFF_BATH_3Q) = '1';
         else if (iTmp < 50)
            *(pOutbuf+OFF_BATH_1Q) = '1';
      }
   }
   
   // Total rooms
   //iTmp = atol(apTokens[VEN_L3_TOT_RMS]);
   //if (iTmp > 0)
   //{
   //   sprintf(acTmp, "%*u", SIZ_ROOMS, iTmp);
   //   memcpy(pOutbuf+OFF_ROOMS, acTmp, SIZ_ROOMS);
   //}

   // QualityClass

   // Fireplace
   iTmp = atol(apTokens[VEN_L3_FP]);
   if (iTmp > 9)
      *(pOutbuf+OFF_FIRE_PL) = 'M';
   else if (iTmp > 0)
      *(pOutbuf+OFF_FIRE_PL) = '0' | iTmp;

   // Pool
   if (*apTokens[VEN_L3_POOL_A] > '0')
      *(pOutbuf+OFF_POOL) = 'P';

   // Stories
   int iStories=0;
   int iFlr3 = atol(apTokens[VEN_L3_FLR_3_A]);
   int iFlr2 = atol(apTokens[VEN_L3_FLR_2_A]);
   int iFlr1 = atol(apTokens[VEN_L3_FLR_1_A]);
   if (iFlr1 > 0)
   {
      sprintf(acTmp, "%*u", SIZ_FLOOR_SF, iFlr1);
      memcpy(pOutbuf+OFF_FLOOR1_SF, acTmp, SIZ_FLOOR_SF);
      iStories = 1;
      if (iFlr2 > 0)
      {
         sprintf(acTmp, "%*u", SIZ_FLOOR_SF, iFlr2);
         memcpy(pOutbuf+OFF_FLOOR2_SF, acTmp, SIZ_FLOOR_SF);
         iStories = 2;
         if (iFlr3 > 0)
         {
            sprintf(acTmp, "%*u", SIZ_FLOOR_SF, iFlr3);
            memcpy(pOutbuf+OFF_FLOOR3_SF, acTmp, SIZ_FLOOR_SF);
            iStories = 3;
         }
      }
      sprintf(acTmp, "%d.0  ", iStories);
      memcpy(pOutbuf+OFF_STORIES, acTmp, SIZ_STORIES);
   }

   // Patio
   lTmp = atol(apTokens[VEN_L3_PAT_A]);
   lTmp += atol(apTokens[VEN_L3_EN_PAT_A]);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*u", SIZ_PATIO_SF, lTmp);
      memcpy(pOutbuf+OFF_PATIO_SF, acTmp, SIZ_PATIO_SF);
   }

   // Deck
   lTmp = atol(apTokens[VEN_L3_DECK_A]);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*u", SIZ_DECK_SF, lTmp);
      memcpy(pOutbuf+OFF_DECK_SF, acTmp, SIZ_DECK_SF);
   }

   // Porch
   lTmp = atol(apTokens[VEN_L3_POR_A]);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*u", SIZ_PORCH_SF, lTmp);
      memcpy(pOutbuf+OFF_PORCH_SF, acTmp, SIZ_PORCH_SF);
   }

   return 0;
}

/******************************* Ven_Load_Lien ******************************
 *
 * Loading LDR file
 *
 ****************************************************************************/

int Ven_Load_Lien(int iFirstRec /* 1=create header rec */)
{
   char     *pTmp, acBuf[MAX_RECSIZE], acRec[MAX_RECSIZE], acLastBuf[2000];
   char     acOutFile[_MAX_PATH];

   HANDLE   fhOut;
   DWORD    nBytesWritten;
   BOOL     bRet;
   long     iRet, lRet, lCnt=0, lRollDrop;
   FILE     *fdLien;

   LogMsg0("Loading LDR roll", acLienFile);
   sprintf(acBuf, "%s\\VEN\\LDR_Roll.txt", acTmpPath);
   //iRet = sortFile(acLienFile, acBuf, "S(#1,C,A) DEL(124)");
   iRet = sortFile(acLienFile, acBuf, "S(1,3,C,A,5,6,C,A,4,1,C,A) DEL(124) OMIT(1,3,C,EQ,\"000\") DUPOUT F(TXT)");
   if (iRet < 200000)
      return -1;

   // Open Lien file
   LogMsg("Open lien file: %s", acBuf);
   fdLien = fopen(acBuf, "r");
   if (fdLien == NULL)
   {
      LogMsg("***** Error opening LDR file: %s\n", acBuf);
      return -1;
   }

   // Open Output file
   sprintf(acOutFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "R01");
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error creating output file: %s\n", acOutFile);
      return -2;
   }

   // Output first header record
   if (iFirstRec > 0)
   {
      memset(acBuf, '9', iRecLen);
      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
   }

   // Get first rec
   pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdLien);

   // Init variables
   lLDRRecCount=lRollDrop=iNoMatch=iBadCity=iBadSuffix=0;

   // Merge loop
   acLastBuf[0] = 0;
   while (!feof(fdLien))
   {
#ifdef _DEBUG
      //if (!memcmp(acRec, "0020801550", 9))
      //   iRet = 0;
#endif
      if (lLienYear == 2022)
         iRet = Ven_MergeLien(acBuf, acRec);
      else 
         iRet = Ven_MergeLien1(acBuf, acRec);
      if (!iRet)
      {
         if (memcmp(acBuf, acLastBuf, iApnLen))
         {
            // Save last recording date
            lRet = atoin((char *)&acBuf[OFF_TRANSFER_DT], 8);
            if (lRet > lLastRecDate && lRet < lToday)
               lLastRecDate = lRet;

            lLDRRecCount++;
            bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
            memcpy(acLastBuf, acBuf, R01_LEN);
         } else
         {
            // Duplicate APN, merge data
            //Ven_UpdateLien(acBuf, acLastBuf)
            lRollDrop++;
         }
      } else
         lRollDrop++;

      pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdLien);
      if (!pTmp || *pTmp > '9')
         break;

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdLien)
      fclose(fdLien);
   if (fhOut)
      CloseHandle(fhOut);

   LogMsg("Total input records:        %u", lCnt);
   LogMsg("      output records:       %u", lLDRRecCount);
   LogMsg("      roll record dropped:  %u", lRollDrop);
   LogMsg("      bad-city records:     %u", iBadCity);
   LogMsg("      bad-suffix records:   %u\n", iBadSuffix);

   LogMsg("Last recording date:        %u", lLastRecDate);

   lRecCnt = lLDRRecCount;
   return 0;
}

// 12/12/2023 - Reload 2023 data
int Ven_Load_Lien2(int iFirstRec /* 1=create header rec */)
{
   char     *pTmp, acBuf[MAX_RECSIZE], acRec[MAX_RECSIZE], acLastBuf[2000];
   char     acOutFile[_MAX_PATH];

   HANDLE   fhOut;
   DWORD    nBytesWritten;
   BOOL     bRet;
   long     iRet, lRet, lCnt=0, lRollDrop;
   FILE     *fdLien;

   LogMsg0("Loading LDR roll", acLienFile);
   sprintf(acBuf, "%s\\VEN\\LDR_Roll.txt", acTmpPath);
   iRet = sortFile(acLienFile, acBuf, "S(1,3,C,A,5,6,C,A,4,1,C,A) DEL(124) OMIT(1,3,C,EQ,\"000\") DUPOUT F(TXT)");
   if (iRet < 200000)
      return -1;

   // Open Lien file
   LogMsg("Open lien file: %s", acBuf);
   fdLien = fopen(acBuf, "r");
   if (fdLien == NULL)
   {
      LogMsg("***** Error opening LDR file: %s\n", acBuf);
      return -1;
   }

   // Open Output file
   sprintf(acOutFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "R01");
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error creating output file: %s\n", acOutFile);
      return -2;
   }

   // Output first header record
   if (iFirstRec > 0)
   {
      memset(acBuf, '9', iRecLen);
      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
   }

   // Get first rec
   pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdLien);

   // Init variables
   lLDRRecCount=lRollDrop=iNoMatch=iBadCity=iBadSuffix=0;

   // Merge loop
   acLastBuf[0] = 0;
   while (!feof(fdLien))
   {
#ifdef _DEBUG
      //if (!memcmp(acRec, "0020801550", 9))
      //   iRet = 0;
#endif
      iRet = Ven_MergeLien2(acBuf, acRec);
      if (!iRet)
      {
         if (memcmp(acBuf, acLastBuf, iApnLen))
         {
            // Save last recording date
            lRet = atoin((char *)&acBuf[OFF_TRANSFER_DT], 8);
            if (lRet > lLastRecDate && lRet < lToday)
               lLastRecDate = lRet;

            lLDRRecCount++;
            bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
            memcpy(acLastBuf, acBuf, R01_LEN);
         } else
         {
            lRollDrop++;
         }
      } else
         lRollDrop++;

      pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdLien);
      if (!pTmp || *pTmp > '9')
         break;

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdLien)
      fclose(fdLien);
   if (fhOut)
      CloseHandle(fhOut);

   LogMsg("Total input records:        %u", lCnt);
   LogMsg("      output records:       %u", lLDRRecCount);
   LogMsg("      roll record dropped:  %u", lRollDrop);
   LogMsg("      bad-city records:     %u", iBadCity);
   LogMsg("      bad-suffix records:   %u\n", iBadSuffix);

   LogMsg("Last recording date:        %u", lLastRecDate);

   lRecCnt = lLDRRecCount;
   return 0;
}

int Ven_Load_Lien3(int iFirstRec /* 1=create header rec */)
{
   char     *pTmp, acBuf[MAX_RECSIZE], acRec[MAX_RECSIZE], acLastBuf[2000];
   char     acOutFile[_MAX_PATH];

   HANDLE   fhOut;
   DWORD    nBytesWritten;
   BOOL     bRet;
   long     iRet, lRet, lCnt=0, lRollDrop;
   FILE     *fdLien;

   LogMsg0("Loading LDR roll", acLienFile);
   sprintf(acBuf, "%s\\VEN\\LDR_Roll.txt", acTmpPath);
   iRet = sortFile(acLienFile, acBuf, "S(1,3,C,A,5,6,C,A,4,1,C,A) DEL(124) OMIT(1,3,C,EQ,\"000\") DUPOUT F(TXT)");
   if (iRet < 200000)
      return -1;

   // Open Lien file
   LogMsg("Open lien file: %s", acBuf);
   fdLien = fopen(acBuf, "r");
   if (fdLien == NULL)
   {
      LogMsg("***** Error opening LDR file: %s\n", acBuf);
      return -1;
   }

   // Open Output file
   sprintf(acOutFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "R01");
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error creating output file: %s\n", acOutFile);
      return -2;
   }

   // Output first header record
   if (iFirstRec > 0)
   {
      memset(acBuf, '9', iRecLen);
      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
   }

   // Get first rec
   pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdLien);

   // Init variables
   lLDRRecCount=lRollDrop=iNoMatch=iBadCity=iBadSuffix=0;

   // Merge loop
   acLastBuf[0] = 0;
   while (!feof(fdLien))
   {
#ifdef _DEBUG
      //if (!memcmp(acRec, "0020801550", 9))
      //   iRet = 0;
#endif
      iRet = Ven_MergeLien3(acBuf, acRec);
      if (!iRet)
      {
         if (memcmp(acBuf, acLastBuf, iApnLen))
         {
            // Save last recording date
            lRet = atoin((char *)&acBuf[OFF_TRANSFER_DT], 8);
            if (lRet > lLastRecDate && lRet < lToday)
               lLastRecDate = lRet;

            lLDRRecCount++;
            bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
            memcpy(acLastBuf, acBuf, R01_LEN);
         } else
         {
            lRollDrop++;
         }
      } else
         lRollDrop++;

      pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdLien);
      if (!pTmp || *pTmp > '9')
         break;

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdLien)
      fclose(fdLien);
   if (fhOut)
      CloseHandle(fhOut);

   LogMsg("Total input records:        %u", lCnt);
   LogMsg("      output records:       %u", lLDRRecCount);
   LogMsg("      roll record dropped:  %u", lRollDrop);
   LogMsg("      bad-city records:     %u", iBadCity);
   LogMsg("      bad-suffix records:   %u\n", iBadSuffix);

   LogMsg("Last recording date:        %u", lLastRecDate);

   lRecCnt = lLDRRecCount;
   return 0;
}

/*
#define VEN_GRGR_COLS      5
#define VEN_GRGR_DOCNUM    0
#define VEN_GRGR_SELLER    1
#define VEN_GRGR_BUYER     2
#define VEN_GRGR_DOCTYPE   3
#define VEN_GRGR_REMARK    4

int Ven_LoadGrGrXls(char *pInfile, FILE *fdOut)
{
   char     *pTmp, acTmp[256], acDocNum[32], myApn[32];
   int      iRet, iRowCnt, lCnt, iRows, iCols, iSkipRows, iRecCnt;

   GRGR_DEF curGrGrRec;

   CString  strTmp, strRemark;
   CStringArray asRows;

   printf("\nProcess GrGr file %s\n", pInfile);
   LogMsg("Process GrGr file %s", pInfile);

   // Open input file - no backup
   CSpreadSheet sprSht(pInfile, "Sheet1", false);

   iRows = sprSht.GetTotalRows();
   iCols = sprSht.GetTotalColumns();

   if (iCols < VEN_GRGR_COLS)
   {
      LogMsg("Skip: %s.  There is no remark column in this file", pInfile);
      printf("Skip: %s.  There is no remark column in this file\n", pInfile);
      return 0;
   }

   // Initialize pointers
   lCnt = 0;
   iSkipRows = iRecCnt = 0;
   sprSht.GetFieldNames(asRows);

   // Skip firsst header row
   for (iRowCnt = 2; iRowCnt <= iRows; iRowCnt++)
   {
      memset(&curGrGrRec, ' ', sizeof(GRGR_DEF));
      sprSht.ReadRow(asRows, iRowCnt);
   
      if (asRows.GetSize() < VEN_GRGR_COLS)
      {
         iSkipRows++;
         LogMsg("Skip row# %d", iRowCnt);
      } else
      {
         strRemark = asRows.GetAt(VEN_GRGR_REMARK);  
         if (!strRemark.IsEmpty())
         {
            // Parse APN -  SIMI VALLEY APN#:6400132555...
            iRet = strRemark.Find("APN#");

            // No APN - skip
            if (iRet > 0)
            {
               strcpy(acTmp, strRemark.Mid(iRet+5));
               if (pTmp = strchr(acTmp, '.'))
                  *pTmp = 0;
               else
                  myTrim(acTmp);
               iRet = strlen(acTmp);
               if (iRet == iApnLen)
               {
                  // Move fourth digit to the end
                  iRet = sprintf(myApn, "%.3s%s%c", acTmp, &acTmp[4], acTmp[3]);
                  memcpy(curGrGrRec.APN, myApn, iRet);
               } else 
               {
                  if (iRet == (iApnLen-1))
                     strcat(acTmp, "0");
                  memcpy(curGrGrRec.APN, acTmp, strlen(acTmp));
               }

               // DocDate
               //char acDate[32];
               //strcpy(acTmp, asRows.GetAt(0));
               //pTmp = dateConversion(acTmp, acDate, YYYY_MM_DD);
               //if (pTmp)
               //   memcpy(curGrGrRec.DocDate, acDate, 8);
               //else
               //   strTmp = asRows.GetAt(1);  
               strcpy(curGrGrRec.DocDate, asRows.GetAt(VEN_GRGR_DOCNUM).Left(8));

               // DocNum - Drop date, first and last zeros
               strcpy(acDocNum, asRows.GetAt(VEN_GRGR_DOCNUM).Mid(9,7));
               memcpy(curGrGrRec.DocNum, acDocNum, strlen(acDocNum));

               // Seller
               strcpy(acTmp, asRows.GetAt(VEN_GRGR_SELLER));
               memcpy(curGrGrRec.Grantors[0].Name, acTmp, strlen(acTmp));
               curGrGrRec.Grantors[0].NameType[0] = 'O';

               // Buyer
               strcpy(acTmp, asRows.GetAt(VEN_GRGR_BUYER));
               memcpy(curGrGrRec.Grantees[0].Name, acTmp, strlen(acTmp));
               curGrGrRec.Grantees[0].NameType[0] = 'E';

               // DocType
               strcpy(acTmp, asRows.GetAt(VEN_GRGR_DOCTYPE));
               iRet = XrefCode2Idx((XREFTBL *)&asDeed[0], acTmp, iNumDeeds);
               if (iRet > 0)
               {
                  iRet = sprintf(acTmp, "%d", iRet);
                  memcpy(curGrGrRec.DocTitle, acTmp, iRet);
               } else
                  memcpy(curGrGrRec.DocTitle, acTmp, strlen(acTmp));

               // Output GrGr record
               curGrGrRec.CRLF[0] = '\n';
               curGrGrRec.CRLF[1] = 0;
               iRet = fputs((char *)&curGrGrRec, fdOut);
               iRecCnt++;
            }
         } 
      }

      lCnt++;
      if (!(lCnt % 1000))
         printf("\r%d", lCnt);
   }

   LogMsgD("\nTotal processed records  : %u", lCnt);
   LogMsgD("        skipped records  : %u", iSkipRows);
   LogMsgD("         output records  : %u\n", iRecCnt);

   return lCnt;
}

/****************************** Ven_LoadGrGrTxt ******************************
 *
 * Process file "Index Export for 03-2019.txt" and to SCSAL_REC record format
 * Return number of process records
 *
 *****************************************************************************/

//#define VEN_GRGR_COLS      10
//#define VEN_GRGR_LNAME     0
//#define VEN_GRGR_FNAME     1
//#define VEN_GRGR_ROLE      2
//#define VEN_GRGR_DOCTYPE   3
//#define VEN_GRGR_OTHPARTY  4
//#define VEN_GRGR_DOCDATE   5
//#define VEN_GRGR_DOCNUM    6
//#define VEN_GRGR_AMOUNT    7
//#define VEN_GRGR_SLEGAL    8
//#define VEN_GRGR_MARGINAL  9

//int Ven_LoadGrGrTxt(char *pInfile, FILE *fdOut)
//{
//   FILE     *fdIn; 
//   char     acBuf[2048], acBuf1[2048], acTmp[256], acFmtApn[32], *pRec;
//   int      iRet, iCnt=0;
//   VEN_GRGR curGrGrRec;
//   long     lSalePrice;
//   char     *pTmp;
//
//   LogMsg("Loading %s ...", pInfile);
//   if (!(fdIn = fopen(pInfile, "r")))
//   {
//      LogMsg("***** Error opening GrGr file %s", pInfile);
//      return -1;
//   }
//
//   memset(acBuf1, 0, 2048);
//
//   // Drop header
//   pRec = fgets(acBuf, 2048, fdIn);
//   if (memcmp(acBuf, "NAME", 4))
//      fseek(fdIn, 0, SEEK_SET);        // Reset to beginning of the file if not header
//
//   // Loop through EOF
//   while (!feof(fdIn))
//   {
//      pRec = fgets(acBuf, 2048, fdIn);
//      if (!pRec)
//         break;
//#ifdef _DEBUG
//      //if (iCnt >= 160)
//      //   iRet = 0;
//#endif
//      //replChar(acBuf, '\n', ' ');
//
//      iRet = ParseString(acBuf, 9, VEN_GRGR_COLS, apTokens);
//
//      // Check for multiple APN
//      if (!apTokens[VEN_GRGR_MARGINAL] || *apTokens[VEN_GRGR_MARGINAL] < '0')
//         continue;
//
//      memset((void *)&curGrGrRec, ' ', sizeof(VEN_GRGR));
//      strcpy(acTmp, apTokens[VEN_GRGR_MARGINAL]);
//      // Add mineral byte to the end if APN length is shorter than defined
//      iRet = strlen(acTmp);
//      if (iRet < iApnLen)
//         continue;            // Bad APN
//      else
//         acTmp[iApnLen] = 0;  // Just in case
//
//      // Reformat APN to match with R01
//      iRet = Ven_FormatApn(acTmp, acFmtApn);
//      memcpy(curGrGrRec.Apn, acFmtApn, iRet);
//
//      // Check whether we should combine first & last name
//      if (apTokens[VEN_GRGR_FNAME] && *apTokens[VEN_GRGR_FNAME] > ' ')
//      {
//         sprintf(acTmp, "%s, %s", apTokens[VEN_GRGR_LNAME], apTokens[VEN_GRGR_FNAME]);
//      } else
//      {
//         strcpy(acTmp, apTokens[VEN_GRGR_LNAME]);
//         curGrGrRec.Company = 'Y';
//      }
//      acTmp[SIZ_GR_NAME] = 0;
//      memcpy(curGrGrRec.Name1, acTmp, strlen(acTmp));
//      iRet = strlen(apTokens[VEN_GRGR_OTHPARTY]);
//      if (iRet > SIZ_GR_NAME)
//         iRet = SIZ_GR_NAME;
//      memcpy(curGrGrRec.Name2, apTokens[VEN_GRGR_OTHPARTY], iRet);
//      if (!_memicmp(apTokens[VEN_GRGR_ROLE], "Grantor", 7))
//         curGrGrRec.RecType = 'O';
//      else if (!_memicmp(apTokens[VEN_GRGR_ROLE], "Grantee", 7))
//         curGrGrRec.RecType = 'E';
//      else
//         curGrGrRec.RecType = '?';
//
//      // Amount
//      strcpy(acTmp, apTokens[VEN_GRGR_AMOUNT]);
//      lSalePrice = atol(acTmp);
//      if (lSalePrice > 0)
//      {
//         sprintf(acTmp, "%*u", SIZ_GR_SALE, lSalePrice);
//         memcpy(curGrGrRec.Amount, acTmp, SIZ_GR_SALE);
//      }
//
//      // DocType
//      strcpy(acTmp, apTokens[VEN_GRGR_DOCTYPE]);
//      //iRet = XrefCode2Idx((XREFTBL *)&asDeed[0], acTmp, iNumDeeds);
//      //if (iRet > 0)
//      iRet = XrefCodeIndex((XREFTBL *)&asDeed[0], acTmp, iNumDeeds);
//      if (iRet >= 0)
//      {
//         //iRet = sprintf(acTmp, "%d", iRet);
//         curGrGrRec.NoneSale_Flg = asDeed[iRet].acFlags[0];
//         iRet = sprintf(acTmp, "%d", asDeed[iRet].iIdxNum);
//         memcpy(curGrGrRec.DocType, acTmp, iRet);
//      } else if (acTmp[0] > ' ')
//      {
//         LogMsg("*** VEN GRGR - Unknown DocTitle: %s [%s]", acTmp, acFmtApn);
//         memcpy(curGrGrRec.DocType, acTmp, strlen(acTmp));
//         curGrGrRec.NoneSale_Flg = 'Y';
//      }
//
//      // DocDate, DocNum
//      if (*apTokens[VEN_GRGR_DOCNUM] > ' ')
//      {
//         pTmp = apTokens[VEN_GRGR_DOCNUM]+8;
//         memcpy(curGrGrRec.DocDate, apTokens[VEN_GRGR_DOCNUM], 8);
//         
//         int iDocNum = atoin(pTmp, 8);              // Ignore trailing zero
//         iRet = sprintf(acTmp, "%.2s%.7d", apTokens[VEN_GRGR_DOCNUM]+2, iDocNum);
//         memcpy(curGrGrRec.DocNum, acTmp, iRet);    
//      }
//
//      curGrGrRec.CRLF[0] = '\n';
//      curGrGrRec.CRLF[1] = '\0';
//      iRet = fputs((char *)&curGrGrRec, fdOut);
//      memset(acBuf,0,2048);
//
//      if (!(++iCnt % 1000))
//         printf("\r%u", iCnt);
//   }
//
//
//   if (fdIn) fclose(fdIn);
//
//   printf("\n");
//   LogMsgD("Number of GrGr record output:  %d\n", iCnt);
//
//   return iCnt;
//}

/****************************** Ven_LoadGrGrDoc ******************************
 *
 * Process file "ParcelQuest - Index Extract_EXTRACT51S18.txt" and output
 * to SCSAL_EXT record format.
 *
 * Return number of process records
 *
 *****************************************************************************/

#define VEN_GRGR_COLS      9
#define VEN_GRGR_DOCNUM    0
#define VEN_GRGR_DOCDATE   1
#define VEN_GRGR_DOCTYPE   2
#define VEN_GRGR_PAGES     3
#define VEN_GRGR_RELDOC    4
#define VEN_GRGR_GRANTOR   5
#define VEN_GRGR_GRANTEE   6
#define VEN_GRGR_LEGAL     7
#define VEN_GRGR_IMAGEPATH 8

// Load Grgr file to SCSAL_REC format
int Ven_LoadGrGrDoc(char *pInfile, FILE *fdOut)
{
   FILE     *fdIn; 
   char     acBuf[2048], acApn[700], acTmp[700], acTmp1[256], acFmtApn[32], *pRec;
   char     *pTmp, *apApns[64], *apItems[64];
   int      iItems, iApns, iRet, iCnt=0;

   SCSAL_REC curGrGrRec;

   LogMsg("Loading %s ...", pInfile);
   if (!(fdIn = fopen(pInfile, "r")))
   {
      LogMsg("***** Error opening GrGr file %s", pInfile);
      return -1;
   }

   // Drop header
   pRec = fgets(acBuf, 2048, fdIn);
   if (_memicmp(acBuf, "DOCU", 4))
      fseek(fdIn, 0, SEEK_SET);        // Reset to beginning of the file if not header

   // Loop through EOF
   while (!feof(fdIn))
   {
      pRec = fgets(acBuf, 2048, fdIn);
      if (!pRec)
         break;
#ifdef _DEBUG
      //if (iCnt >= 160)
      //   iRet = 0;
#endif
      //replChar(acBuf, '\n', ' ');

      iRet = ParseStringNQ(acBuf, 9, VEN_GRGR_COLS, apTokens);
      if (iRet < VEN_GRGR_IMAGEPATH)
         continue;

      // Check for APN
      if (memcmp(apTokens[VEN_GRGR_LEGAL], "Parcel:", 7))
         continue;

      memset((void *)&curGrGrRec, ' ', sizeof(SCSAL_REC));
      strcpy(acApn, apTokens[VEN_GRGR_LEGAL]);

      // Ignore APN shorter than defined
      iRet = strlen(acApn);
      if (iRet < iApnLen+8)
      {
         LogMsg("Skip APN: %s", acApn);
         continue;            // Bad APN
      }

      // Check multi APN
      iApns = ParseString(acApn, ',', 63, apApns);
      if (iApns > 1)
         curGrGrRec.MultiSale_Flg = 'Y';

      // Reformat APN to match with R01
      iRet = Ven_FormatApn(apApns[0]+8, acFmtApn);
      memcpy(curGrGrRec.Apn, acFmtApn, iRet);

      if (*apTokens[VEN_GRGR_GRANTOR] > ' ')
      {
         iItems = ParseString(apTokens[VEN_GRGR_GRANTOR], ',', 10, apItems);
         vmemcpy(curGrGrRec.Seller1, apItems[0], SIZ_GR_NAME);
         if (iItems > 1)
            vmemcpy(curGrGrRec.Seller2, apItems[1], SIZ_GR_NAME);
      }

      if (*apTokens[VEN_GRGR_GRANTEE] > ' ')
      {
         iItems = ParseString(apTokens[VEN_GRGR_GRANTEE], ',', 10, apItems);
         iRet = sprintf(acTmp1, "%d", iItems);
         memcpy(curGrGrRec.NameCnt, acTmp1, iRet);

         vmemcpy(curGrGrRec.Name1, apItems[0], SIZ_GR_NAME);
         if (iItems > 1)
            vmemcpy(curGrGrRec.Name2, apItems[1], SIZ_GR_NAME);

         if (iItems > 2)
            curGrGrRec.Etal = 'Y';
      }

      // DocType
      strcpy(acTmp, apTokens[VEN_GRGR_DOCTYPE]);
      // Use this when using with SCSAL_EXT
      //vmemcpy(curGrGrRec.DocTitle, acTmp, SALE_SIZ_DOCTITLE);
      iRet = XrefCodeIndex((XREFTBL *)&asDeed[0], acTmp, iNumDeeds);
      if (iRet >= 0)
      {
         //iRet = sprintf(acTmp, "%d", iRet);
         curGrGrRec.NoneSale_Flg = asDeed[iRet].acFlags[0];
         iRet = sprintf(acTmp, "%d", asDeed[iRet].iIdxNum);
         memcpy(curGrGrRec.DocType, acTmp, iRet);
      } else if (acTmp[0] > ' ')
      {
         LogMsg("*** VEN GRGR - Unknown DocTitle: %s [%s]", acTmp, acFmtApn);
         curGrGrRec.NoneSale_Flg = 'Y';
      }

      // DocDate, DocNum
      if (*apTokens[VEN_GRGR_DOCNUM] > ' ')
      {
         pTmp = dateConversion(apTokens[VEN_GRGR_DOCDATE], acTmp1, MM_DD_YYYY_2);
         if (pTmp)
         {
            memcpy(curGrGrRec.DocDate, pTmp, 8);         
            memcpy(curGrGrRec.DocNum, apTokens[VEN_GRGR_DOCNUM]+2, 2);    
            vmemcpy(curGrGrRec.DocNum+2, apTokens[VEN_GRGR_DOCNUM]+6, RSIZ_DOCNUM-2);    
         }
      }

      curGrGrRec.ARCode  = 'R';
      curGrGrRec.CRLF[0] = '\n';
      curGrGrRec.CRLF[1] = 0;
      iRet = fputs((char *)&curGrGrRec, fdOut);
      iCnt++;
      iItems = 1;
      while (iItems < iApns)
      {
         iRet = Ven_FormatApn(apApns[iItems]+8, acFmtApn);
         memcpy(curGrGrRec.Apn, acFmtApn, iRet);
         iRet = fputs((char *)&curGrGrRec, fdOut);
         iItems++;
         iCnt++;
      }

      memset(acBuf,0,2048);
      if (!(iCnt % 1000))
         printf("\r%u", iCnt);
   }

   if (fdIn) fclose(fdIn);

   printf("\n");
   LogMsgD("Number of GrGr record output:  %d", iCnt);

   return iCnt;
}

// Load Grgr file to GRGR_DEF format
int Ven_LoadGrGrDoc1(char *pInfile, FILE *fdOut)
{
   FILE     *fdIn; 
   char     acBuf[2048], acBuf1[2048], acTmp[256], acTmp1[256], acFmtApn[32], *pRec;
   char     *pTmp, *apItems[256];
   int      iIdx, iItems, iRet, iCnt=0;

   GRGR_DEF curGrGrRec;

   LogMsg("Loading %s ...", pInfile);
   if (!(fdIn = fopen(pInfile, "r")))
   {
      LogMsg("***** Error opening GrGr file %s", pInfile);
      return -1;
   }

   memset(acBuf1, 0, 2048);

   // Drop header
   pRec = fgets(acBuf, 2048, fdIn);
   if (_memicmp(acBuf, "DOCU", 4))
      fseek(fdIn, 0, SEEK_SET);        // Reset to beginning of the file if not header

   // Loop through EOF
   while (!feof(fdIn))
   {
      pRec = fgets(acBuf, 2048, fdIn);
      if (!pRec)
         break;
#ifdef _DEBUG
      //if (iCnt >= 160)
      //   iRet = 0;
#endif
      //replChar(acBuf, '\n', ' ');

      iRet = ParseStringNQ(acBuf, 9, VEN_GRGR_COLS, apTokens);
      if (iRet < VEN_GRGR_IMAGEPATH)
         continue;

      // Check for APN
      if (memcmp(apTokens[VEN_GRGR_LEGAL], "Parcel:", 7))
         continue;

      memset((void *)&curGrGrRec, ' ', sizeof(GRGR_DEF));
      strcpy(acTmp, apTokens[VEN_GRGR_LEGAL]);

      // Ignore APN shorter than defined
      iRet = strlen(acTmp);
      if (iRet < iApnLen+8)
      {
         LogMsg("Skip APN: %s", acTmp);
         continue;            // Bad APN
      }

      // Check multi APN
      iItems = ParseString(acTmp, ',', 256, apItems);
      if (iItems > 1)
      {
         acTmp1[0] = 0;
         iIdx = 0;
         do {
            strcat(acTmp1, apItems[++iIdx]+8);
            if (iIdx < iItems-1)
               strcat(acTmp1, ",");
         } while (iIdx < iItems-1);

         vmemcpy(curGrGrRec.ReferenceData, acTmp1, SIZ_GR_REF);
         curGrGrRec.MultiApn = 'Y';
      }

      // Reformat APN to match with R01
      iRet = Ven_FormatApn(apItems[0]+8, acFmtApn);
      memcpy(curGrGrRec.APN, acFmtApn, iRet);

      if (*apTokens[VEN_GRGR_GRANTOR] > ' ')
      {
         iItems = ParseString(apTokens[VEN_GRGR_GRANTOR], ',', 256, apItems);
         iIdx = 0;
         do {
            vmemcpy(curGrGrRec.Grantors[iIdx].Name, apItems[iIdx++], SIZ_GR_NAME);
         } while (iIdx < iItems && iIdx < MAX_NAMES);
      }

      if (*apTokens[VEN_GRGR_GRANTEE] > ' ')
      {
         iItems = ParseString(apTokens[VEN_GRGR_GRANTEE], ',', 256, apItems);
         iIdx = 0;
         do {
            vmemcpy(curGrGrRec.Grantees[iIdx].Name, apItems[iIdx++], SIZ_GR_NAME);
         } while (iIdx < iItems && iIdx < MAX_NAMES);

         if (iIdx < iItems)
         {
            curGrGrRec.MoreName = 'Y';
            iRet = sprintf(acTmp1, "%d", iItems);
            memcpy(curGrGrRec.NameCnt, acTmp1, iRet);
         }
      }

      // DocType
      strcpy(acTmp, apTokens[VEN_GRGR_DOCTYPE]);
      vmemcpy(curGrGrRec.DocTitle, acTmp, SIZ_GR_TITLE);
      iRet = XrefCodeIndex((XREFTBL *)&asDeed[0], acTmp, iNumDeeds);
      if (iRet >= 0)
      {
         //iRet = sprintf(acTmp, "%d", iRet);
         curGrGrRec.NoneSale = asDeed[iRet].acFlags[0];
         iRet = sprintf(acTmp, "%d", asDeed[iRet].iIdxNum);
         memcpy(curGrGrRec.DocType, acTmp, iRet);
      } else if (acTmp[0] > ' ')
      {
         LogMsg("*** VEN GRGR - Unknown DocTitle: %s [%s]", acTmp, acFmtApn);
         curGrGrRec.NoneSale = 'Y';
      }

      // DocDate, DocNum
      if (*apTokens[VEN_GRGR_DOCNUM] > ' ')
      {
         pTmp = dateConversion(apTokens[VEN_GRGR_DOCDATE], acTmp1, MM_DD_YYYY_2);
         if (pTmp)
            memcpy(curGrGrRec.DocDate, pTmp, 8);
         
         vmemcpy(curGrGrRec.DocNum, apTokens[VEN_GRGR_DOCNUM]+4, RSIZ_DOCNUM);    
      }

      curGrGrRec.CRLF[0] = '\n';
      curGrGrRec.CRLF[1] = '\0';
      iRet = fputs((char *)&curGrGrRec, fdOut);
      memset(acBuf,0,2048);

      if (!(++iCnt % 1000))
         printf("\r%u", iCnt);
   }


   if (fdIn) fclose(fdIn);

   printf("\n");
   LogMsgD("Number of GrGr record output:  %d\n", iCnt);

   return iCnt;
}

/****************************** Ven_MakeGrGrDef *****************************
 *
 * This function is obsolete 5/20/2014 and was replaced by Ven_MakeCSaleRec()
 *
 ****************************************************************************

void Ven_MakeGrGrDef(GRGR_DEF *pStdGrRec, VEN_GRGR *pVenGrRec, int iIdx)
{
   // Form GRGR_DEF record
   if (pVenGrRec->RecType == 'E')
   {
      memset(pStdGrRec, ' ', sizeof(GRGR_DEF)-2);
      memcpy(pStdGrRec->APN, pVenGrRec->Apn, iApnLen);
      memcpy(pStdGrRec->DocNum, pVenGrRec->DocNum, sizeof(pVenGrRec->DocNum));
      memcpy(pStdGrRec->DocDate, pVenGrRec->DocDate, sizeof(pVenGrRec->DocDate));
      memcpy(pStdGrRec->Grantees[iIdx].Name, pVenGrRec->Name1, sizeof(pVenGrRec->Name1));
      pStdGrRec->Grantees[iIdx].NameType[0] = pVenGrRec->Company;
      memcpy(pStdGrRec->DocTitle, pVenGrRec->DocType, sizeof(pVenGrRec->DocType));
      memcpy(pStdGrRec->SalePrice, pVenGrRec->Amount, sizeof(pVenGrRec->Amount));
   } else
   {
      memset(pStdGrRec, ' ', sizeof(GRGR_DEF)-2);
      memcpy(pStdGrRec->APN, pVenGrRec->Apn, iApnLen);
      memcpy(pStdGrRec->DocNum, pVenGrRec->DocNum, sizeof(pVenGrRec->DocNum));
      memcpy(pStdGrRec->DocDate, pVenGrRec->DocDate, sizeof(pVenGrRec->DocDate));
      memcpy(pStdGrRec->Grantors[iIdx].Name, pVenGrRec->Name1, sizeof(pVenGrRec->Name1));
      pStdGrRec->Grantors[iIdx].NameType[0] = pVenGrRec->Company;
      memcpy(pStdGrRec->DocTitle, pVenGrRec->DocType, sizeof(pVenGrRec->DocType));
      memcpy(pStdGrRec->SalePrice, pVenGrRec->Amount, sizeof(pVenGrRec->Amount));
   }
}

/****************************** Ven_MakeCSaleRec ****************************
 *
 * 
 ****************************************************************************/

void Ven_MakeCSaleRec(SCSAL_REC *pStdSaleRec, VEN_GRGR *pVenGrRec, int iIdx)
{
   // Form SCSAL_REC record
   if (pVenGrRec->RecType == 'E')
   {
      memset(pStdSaleRec, ' ', sizeof(SCSAL_REC)-2);
      memcpy(pStdSaleRec->Apn, pVenGrRec->Apn, iApnLen);
      memcpy(pStdSaleRec->DocNum, pVenGrRec->DocNum, sizeof(pVenGrRec->DocNum));
      memcpy(pStdSaleRec->DocDate, pVenGrRec->DocDate, sizeof(pVenGrRec->DocDate));
      if (iIdx == 0)
         memcpy(pStdSaleRec->Name1, pVenGrRec->Name1, sizeof(pVenGrRec->Name1));
      if (iIdx == 1)
         memcpy(pStdSaleRec->Name2, pVenGrRec->Name1, sizeof(pVenGrRec->Name1));

      memcpy(pStdSaleRec->DocType, pVenGrRec->DocType, sizeof(pVenGrRec->DocType));
      memcpy(pStdSaleRec->SalePrice, pVenGrRec->Amount, sizeof(pVenGrRec->Amount));
   } else
   {
      memset(pStdSaleRec, ' ', sizeof(SCSAL_REC)-2);
      memcpy(pStdSaleRec->Apn, pVenGrRec->Apn, iApnLen);
      memcpy(pStdSaleRec->DocNum, pVenGrRec->DocNum, sizeof(pVenGrRec->DocNum));
      memcpy(pStdSaleRec->DocDate, pVenGrRec->DocDate, sizeof(pVenGrRec->DocDate));

      if (iIdx == 0)
         memcpy(pStdSaleRec->Seller1, pVenGrRec->Name1, SALE_SIZ_SELLER);
      if (iIdx == 1)
         memcpy(pStdSaleRec->Seller2, pVenGrRec->Name1, SALE_SIZ_SELLER);
      memcpy(pStdSaleRec->DocType, pVenGrRec->DocType, sizeof(pVenGrRec->DocType));
      memcpy(pStdSaleRec->SalePrice, pVenGrRec->Amount, sizeof(pVenGrRec->Amount));
   }

   if (pVenGrRec->NoneSale_Flg > ' ')
      pStdSaleRec->NoneSale_Flg = pVenGrRec->NoneSale_Flg;
}     

/******************************** Ven_CombineGrGr ***************************
 *
 * Convert VEN_GRGR to GRGR_DEF and combine them into one record per document.
 * Use E record for both grantor and grantee.  If more than one E rec exist, 
 * choose Company=Y.
 * 1) Affidavit does not contain Grantee name.
 *
 ****************************************************************************

int Ven_CombineGrGr(char *pInfile, char *pOutfile)
{
   FILE     *fdIn, *fdOut; 
   char     acTmp[256], *pRec;
   int      iRet, iCnt, iGtorCnt, iGteeCnt;
   VEN_GRGR curGrGrRec, prvGrGrRec;
   GRGR_DEF stdGrGrRec;

   if (!(fdIn = fopen(pInfile, "r")))
   {
      LogMsgD("***** Error opening GrGr file %s", pInfile);
      return -1;
   }

   if (!(fdOut = fopen(pOutfile, "w")))
   {
      LogMsgD("***** Error creating output file %s", pOutfile);
      return -1;
   }

   // Init output record
   stdGrGrRec.CRLF[0] = '\n';
   stdGrGrRec.CRLF[1] = 0;
   iCnt=iGtorCnt=iGteeCnt = 0;

   // Read first record
   pRec = fgets((char *)&prvGrGrRec, sizeof(VEN_GRGR), fdIn);
   Ven_MakeGrGrDef(&stdGrGrRec, &prvGrGrRec, 0);

   while (!feof(fdIn))
   {
      pRec = fgets((char *)&curGrGrRec, sizeof(VEN_GRGR), fdIn);
      if (!pRec)
      {
         // Output last record
         if (iGteeCnt > 0)
         {
            iRet = sprintf(acTmp, "%d", iGteeCnt);
            memcpy(stdGrGrRec.NameCnt, acTmp, iRet);
         }
         iRet = fputs((char *)&stdGrGrRec, fdOut);
         break;
      }

      // Check for new APN - If so, output previous one - compare APN+DocNum
      if (memcmp(prvGrGrRec.Apn, curGrGrRec.Apn, RSIZ_APN+RSIZ_DOCNUM))
      {
         // Output current rec
         if (iGteeCnt > 0)
         {
            iRet = sprintf(acTmp, "%d", iGteeCnt);
            memcpy(stdGrGrRec.NameCnt, acTmp, iRet);
         }
         iRet = fputs((char *)&stdGrGrRec, fdOut);

         // Prepare new rec
         iGtorCnt=iGteeCnt = 0;
         if (curGrGrRec.RecType == 'E')
         {
            Ven_MakeGrGrDef(&stdGrGrRec, &curGrGrRec, iGteeCnt++);
         } else
         {
            Ven_MakeGrGrDef(&stdGrGrRec, &curGrGrRec, iGtorCnt++);  
         }

         // Save current
         strcpy((char *)&prvGrGrRec, (char *)&curGrGrRec);
      } else if (curGrGrRec.RecType == 'E')
      {
         if (iGteeCnt < MAX_NAMES)
         {
            memcpy(stdGrGrRec.Grantees[iGteeCnt].Name, curGrGrRec.Name1, sizeof(curGrGrRec.Name1));      
            stdGrGrRec.Grantees[iGteeCnt].NameType[0] = curGrGrRec.Company;
         } else
            stdGrGrRec.MoreName = 'Y';
         iGteeCnt++;
      } else if (curGrGrRec.RecType == 'O')
      {
         if (iGtorCnt < MAX_NAMES)
         {
            memcpy(stdGrGrRec.Grantors[iGtorCnt].Name, curGrGrRec.Name1, sizeof(curGrGrRec.Name1));      
            stdGrGrRec.Grantors[iGtorCnt].NameType[0] = curGrGrRec.Company;
         } else
            iRet = 0;
         iGtorCnt++;
      } 

      if (!(++iCnt % 1000))
         printf("\r%u", iCnt);
   }

   printf("\n");
   fclose(fdIn);
   fclose(fdOut);

   return 0;
}

/******************************** Ven_CombineGrGr ***************************
 *
 * Convert VEN_GRGR to SCSAL_REC and combine them into one record per document.
 * Use E record for both grantor and grantee.  If more than two E rec exist, 
 * set Etal=Y.
 * 1) Affidavit does not contain Grantee name.
 *
 ****************************************************************************/

int Ven_CombineGrGr(char *pInfile, char *pOutfile)
{
   FILE      *fdIn, *fdOut; 
   char      acTmp[256], *pRec;
   int       iRet, iCnt, iGtorCnt, iGteeCnt;
   VEN_GRGR  curGrGrRec, prvGrGrRec;
   SCSAL_REC stdGrGrRec;

   if (!(fdIn = fopen(pInfile, "r")))
   {
      LogMsgD("***** Error opening GrGr file %s", pInfile);
      return -1;
   }

   if (!(fdOut = fopen(pOutfile, "w")))
   {
      LogMsgD("***** Error creating output file %s", pOutfile);
      return -1;
   }

   // Init output record
   stdGrGrRec.CRLF[0] = '\n';
   stdGrGrRec.CRLF[1] = 0;
   iCnt=iGtorCnt=iGteeCnt = 0;

   // Read first record
   pRec = fgets((char *)&prvGrGrRec, sizeof(VEN_GRGR), fdIn);
   //Ven_MakeGrGrDef(&stdGrGrRec, &prvGrGrRec, 0);
   Ven_MakeCSaleRec(&stdGrGrRec, &prvGrGrRec, 0);

   while (!feof(fdIn))
   {
      pRec = fgets((char *)&curGrGrRec, sizeof(VEN_GRGR), fdIn);
      if (!pRec)
      {
         // Output last record
         if (iGteeCnt > 0)
         {
            iRet = sprintf(acTmp, "%d", iGteeCnt);
            memcpy(stdGrGrRec.NameCnt, acTmp, iRet);
            if (iGteeCnt > 2)
               stdGrGrRec.Etal = 'Y';
         }
         iRet = fputs((char *)&stdGrGrRec, fdOut);
         break;
      }

      // Check for new APN - If so, output previous one - compare APN+DocNum
      if (memcmp(prvGrGrRec.Apn, curGrGrRec.Apn, RSIZ_APN+RSIZ_DOCNUM))
      {
         // Output current rec
         if (iGteeCnt > 0)
         {
            iRet = sprintf(acTmp, "%d", iGteeCnt);
            memcpy(stdGrGrRec.NameCnt, acTmp, iRet);
            if (iGteeCnt > 2)
               stdGrGrRec.Etal = 'Y';
         }
         iRet = fputs((char *)&stdGrGrRec, fdOut);

         // Prepare new rec
         iGtorCnt=iGteeCnt = 0;
         if (curGrGrRec.RecType == 'E')
         {
            //Ven_MakeGrGrDef(&stdGrGrRec, &curGrGrRec, iGteeCnt++);
            Ven_MakeCSaleRec(&stdGrGrRec, &curGrGrRec, iGteeCnt++);
         } else
         {
            //Ven_MakeGrGrDef(&stdGrGrRec, &curGrGrRec, iGtorCnt++);  
            Ven_MakeCSaleRec(&stdGrGrRec, &curGrGrRec, iGtorCnt++);  
         }

         // Save current
         strcpy((char *)&prvGrGrRec, (char *)&curGrGrRec);
      } else if (curGrGrRec.RecType == 'E')
      {
         if (iGteeCnt == 0)
            memcpy(stdGrGrRec.Name1, curGrGrRec.Name1, sizeof(curGrGrRec.Name1));      
         else if (iGteeCnt == 1)
            memcpy(stdGrGrRec.Name2, curGrGrRec.Name1, sizeof(curGrGrRec.Name1));      
         else
            stdGrGrRec.Etal = 'Y';
         iGteeCnt++;
      } else if (curGrGrRec.RecType == 'O')
      {
         if (iGtorCnt == 0)
            memcpy(stdGrGrRec.Seller1, curGrGrRec.Name1, sizeof(stdGrGrRec.Seller1));      
         else if (iGtorCnt == 1)
            memcpy(stdGrGrRec.Seller2, curGrGrRec.Name1, sizeof(stdGrGrRec.Seller1));      
         else
            iRet = 0;
         iGtorCnt++;
      } 

      if (!(++iCnt % 1000))
         printf("\r%u", iCnt);
   }

   printf("\n");
   fclose(fdIn);
   fclose(fdOut);

   return 0;
}

/******************************** Ven_LoadGrGr ******************************
 *
 * Return 0 if success.
 *
 ****************************************************************************/

int Ven_LoadGrGr()
{
   char     *pTmp;
   char     acTmp[256], cFileCnt=1;
   char     acGrGrIn[_MAX_PATH], acGrGrOut[_MAX_PATH], acGrGrBak[_MAX_PATH];
   char     acInFile[_MAX_PATH];
   struct   _finddata_t  sFileInfo;

   FILE     *fdOut;
   int      iCnt, iRet;
   long     lCnt, lHandle;

   LogMsg0("VEN GrGr processing ...");

   // Get raw file name
   GetIniString(myCounty.acCntyCode, "GrGrIn", "", acGrGrIn, _MAX_PATH, acIniFile);
   dateString(acTmp, 0);

   iCnt = 2;
   while (iCnt > 0)
   {
      // Open Input file
      lHandle = _findfirst(acGrGrIn, &sFileInfo);
      if (lHandle > 0)
      {
         sprintf(acGrGrBak, acGrBkTmpl, myCounty.acCntyCode, acTmp);
         if (_access(acGrGrBak, 0))
            _mkdir(acGrGrBak);

         pTmp = strrchr(acGrGrIn, '\\');
         if (pTmp)
            *pTmp = 0;
         iRet = 0;
         break;
      } else
      {
         GetIniString(myCounty.acCntyCode, "GrGrIn1", "", acGrGrIn, _MAX_PATH, acIniFile);
         iCnt--;
      }
   }

   if (!iCnt)
   {
      bGrGrAvail = false;
      LogMsg("*** No new GrGr available for processing: %s", acGrGrIn);
      return 1;
   }

   // Create Output file - Ven_GrGr.dat
   sprintf(acGrGrOut, acGrGrTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "Dat");
   LogMsg("Open output file %s", acGrGrOut);
   fdOut = fopen(acGrGrOut, "w");
   if (fdOut == NULL)
   {
      LogMsg("***** Error creating GrGr output file: %s\n", acGrGrOut);
      _findclose(lHandle);
      return -2;
   }

   iCnt = lCnt = 0;
   while (!iRet)
   {
      sprintf(acInFile, "%s\\%s", acGrGrIn, sFileInfo.name);
      LogMsg("Loading %s", acInFile);

      // Update GrGr file date
      iRet = getFileDate(acInFile);
      if (iRet > lLastGrGrDate)
         lLastGrGrDate = iRet;

      // Check for Unicode file
      sprintf(acTmp, "%s\\VEN\\LastGrGr.txt", acTmpPath);
      if (!UnicodeToAnsi(acInFile, acTmp))
         strcpy(acInFile, acTmp);

      // Parse input file
      //iRet = Ven_LoadGrGrTxt(acInFile, fdOut);
      iRet = Ven_LoadGrGrDoc(acInFile, fdOut);
      if (iRet <= 0)
         LogMsg("*** Skip %s", acInFile);
      else
      {
         lCnt += iRet;

         // Move input file to backup folder
         sprintf(acTmp, "%s\\%s", acGrGrBak, sFileInfo.name);
         sprintf(acInFile, "%s\\%s", acGrGrIn, sFileInfo.name);
         LogMsg("Move %s to %s", acInFile, acTmp);
         if (rename(acInFile, acTmp))
            LogMsg("*** Unable to move %s to %s", acInFile, acTmp);
      }

      iCnt++;
      // Find next file
      iRet = _findnext(lHandle, &sFileInfo);
   }

   // Close handle
   _findclose(lHandle);
   iRet = 0;

   // Close files
   if (fdOut)
      fclose(fdOut);

   LogMsg("Number of GRGR files have been processed: %d", iCnt);
   LogMsg("Total processed records  : %u", lCnt);
   printf("\nTotal processed records  : %u\n", lCnt);

   // Sort output
   if (lCnt > 0)
   {
      // Append Ven_GrGr.dat to Ven_GrGr.sls for backup
      sprintf(acGrGrBak, acGrGrTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "Sls");
      LogMsgD("Append %s --> %s", acGrGrOut, acGrGrBak);

      // Sort - APN (asc), Saledate (asc), DocNum (asc)
      sprintf(acTmp, "S(1,%d,C,A,27,8,C,A,15,%d,C,A) DUPO(B2048,1,34) OMIT(1,1,C,GT,\"9\") ", iApnLen, SALE_SIZ_DOCNUM);
      sprintf(acInFile, "%s+%s", acGrGrOut, acGrGrBak);
      sprintf(acGrGrOut, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "srt");
      lCnt = sortFile(acInFile, acGrGrOut, acTmp);
      if (lCnt > 0)
      {
         sprintf(acTmp, acGrGrTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "sav");
         if (!_access(acTmp, 0))
            DeleteFile(acTmp);

         // Rename sls to sav
         LogMsg("Rename %s --> %s", acGrGrBak, acTmp);
         rename(acGrGrBak, acTmp);
   
         // Rename srt to SLS file
         LogMsg("Rename %s --> %s", acGrGrOut, acGrGrBak);
         rename(acGrGrOut, acGrGrBak);
      }
   } else
      iRet = 1;

   LogMsg("Total output records after dedup: %u", lCnt);
   printf("\nTotal output records after dedup: %u\n", lCnt);

   return iRet;
}

/******************************** Ven_ExtrLien ******************************
 *
 * Extract values from "ASR_SEC_PUBINFO"
 * 
 ****************************************************************************/

int Ven_ExtrLien()
{
   char     *pTmp, acBuf[512], acRollRec[1024], acTmp[256], acTmp1[32];
   char     acOutFile[_MAX_PATH];

   int      iTmp, lCnt=0;
   LONGLONG lTmp;
   FILE     *fdLienExt;
   VEN_ROLL *pRec = (VEN_ROLL *)&acRollRec;
   LIENEXTR *pLienExtr = (LIENEXTR *)&acBuf;

   // Open roll file
   LogMsg("Open Roll file %s", acRollFile);
   fdRoll = fopen(acRollFile, "r");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", acRollFile);
      return 2;
   }

   // Open Output file
   sprintf(acOutFile, acLienTmpl, myCounty.acCntyCode, myCounty.acCntyCode);
   LogMsg("Open output file %s", acOutFile);
   if (!(fdLienExt = fopen(acOutFile, "w")))
   {
      LogMsg("***** Error creating lien extract file: %s\n", acOutFile);
      return 2;
   }

   // Get first RollRec
   pTmp = fgets((char *)&acRollRec[0], 1024, fdRoll);
   if (!memcmp(acRollRec, "0000000000", iApnLen))
      pTmp = fgets((char *)&acRollRec[0], iRollLen, fdRoll);

   // Init variables
   iNoMatch=iBadCity=iBadSuffix=0;

   // Merge loop
   while (!feof(fdRoll))
   {
      // Create new R01 record
      if (memcmp(acRollRec+ROFF_APN-1, "0000000000", iApnLen))
      {
         if (acRollRec[ROFF_APN_STATUS-1] != 'V')
         {
            // Clear output buffer
            memset(acBuf, ' ', sizeof(LIENEXTR));

            // Start copying data
            memcpy(acTmp1, pRec->Apn, SSIZ_APN);
            iTmp = Ven_FormatApn(acTmp1, acTmp);
            memcpy(pLienExtr->acApn, acTmp, iTmp);
            // Prev APN - unformatted
            memcpy(pLienExtr->extra.Ven.PrevApn, pRec->PrevAPN, RSIZ_PREVAPN);

            // Year assessed
            memcpy(pLienExtr->acYear, myCounty.acYearAssd, 4);

            // HO Exempt
            if (!memcmp(pRec->ExeCode1, "HO ", 3) || !memcmp(pRec->ExeCode2, "HO ", 3))
               pLienExtr->acHO[0] = '1';      // Y
            else
               pLienExtr->acHO[0] = '2';      // N
  
            // Exe Amt
            long lExe1 = atoin(pRec->ExeValue1, RSIZ_EXEVALUE1); 
            long lExe2 = atoin(pRec->ExeValue2, RSIZ_EXEVALUE1); 
            long lExe3 = atoin(pRec->ExeValue3, RSIZ_EXEVALUE1);
            lTmp = lExe1+lExe2+lExe3;
            if (lTmp > 0)
            {
               sprintf(acTmp, "%*u", SIZ_EXE_TOTAL, lTmp);
               memcpy(pLienExtr->acExAmt, acTmp, SIZ_EXE_TOTAL);

               if (lExe1 > 0)
               {
                  iTmp = sprintf(acTmp, "%*u", sizeof(pLienExtr->extra.Ven.Exe_Amt1), lExe1);
                  memcpy(pLienExtr->extra.Ven.Exe_Amt1, acTmp, iTmp);
                  memcpy(pLienExtr->extra.Ven.Exe_Code1, pRec->ExeCode1, RSIZ_EXECODE1);
               }
               if (lExe2 > 0)
               {
                  iTmp = sprintf(acTmp, "%*u", sizeof(pLienExtr->extra.Ven.Exe_Amt2), lExe2);
                  memcpy(pLienExtr->extra.Ven.Exe_Amt2, acTmp, iTmp);
                  memcpy(pLienExtr->extra.Ven.Exe_Code2, pRec->ExeCode2, RSIZ_EXECODE1);
               }
               if (lExe3 > 0)
               {
                  iTmp = sprintf(acTmp, "%*u", sizeof(pLienExtr->extra.Ven.Exe_Amt3), lExe3);
                  memcpy(pLienExtr->extra.Ven.Exe_Amt3, acTmp, iTmp);
                  memcpy(pLienExtr->extra.Ven.Exe_Code3, pRec->ExeCode3, RSIZ_EXECODE1);
               }
            }

            // Land
            long lLand = atoin(pRec->Land, RSIZ_LANDVALUE);
            if (lLand > 0)
            {
               sprintf(acTmp, "%*u", SIZ_LAND, lLand);
               memcpy(pLienExtr->acLand, acTmp, SIZ_LAND);
            }

            // Improve
            long lImpr = atoin(pRec->Impr, RSIZ_IMPROVEVALUE);
            if (lImpr > 0)
            {
               sprintf(acTmp, "%*u", SIZ_IMPR, lImpr);
               memcpy(pLienExtr->acImpr, acTmp, SIZ_IMPR);
            }

            // Other values
            long lPers = atoin(pRec->PersonalValue, RSIZ_PERSONALVALUE);
            long lFixt = atoin(pRec->FixtureValue, RSIZ_TRADEVALUE);
            long lMineral = atoin(pRec->MineralValue, RSIZ_MINERALVALUE);
            long lTrees = atoin(pRec->TreesValue, RSIZ_TREESVALUE);
            lPers += atoin(pRec->UnitPPValue, RSIZ_PERSONALVALUE);
            lFixt += atoin(pRec->UnitTFValue, RSIZ_TRADEVALUE);

            // Other value
            lTmp = lMineral + lTrees + lFixt + lPers;
            if (lTmp > 0)
            {
               sprintf(acTmp, "%*u", SIZ_OTHER, lTmp);
               memcpy(pLienExtr->acOther, acTmp, SIZ_OTHER);

               if (lPers > 0)
               {
                  iTmp = sprintf(acTmp, "%*u", sizeof(pLienExtr->acPP_Val), lPers);
                  memcpy(pLienExtr->acPP_Val, acTmp, iTmp);
               }
               if (lFixt > 0)
               {
                  iTmp = sprintf(acTmp, "%*u", sizeof(pLienExtr->acME_Val), lFixt);
                  memcpy(pLienExtr->acME_Val, acTmp, iTmp);
               }
               if (lMineral > 0)
               {
                  iTmp = sprintf(acTmp, "%*u", sizeof(pLienExtr->extra.Ven.Mineral), lMineral);
                  memcpy(pLienExtr->extra.Ven.Mineral, acTmp, iTmp);
               }
               if (lTrees > 0)
               {
                  iTmp = sprintf(acTmp, "%*u", sizeof(pLienExtr->extra.Ven.Tree), lTrees);
                  memcpy(pLienExtr->extra.Ven.Tree, acTmp, iTmp);
               }
            }

            // Gross total
            lTmp += lLand+lImpr;
            if (lTmp > 0)
            {
               sprintf(acTmp, "%*u", SIZ_GROSS, lTmp);
               memcpy(pLienExtr->acGross, acTmp, SIZ_GROSS);
            }

            // Ratio
            if (lImpr > 0)
            {
               sprintf(acTmp, "%*u", SIZ_RATIO, (LONGLONG)(lImpr*100/(lLand+lImpr) + 0.5) );
               memcpy(pLienExtr->acRatio, acTmp, SIZ_RATIO);
            }

            // TRA
            lTmp = atoin(pRec->TRA, RSIZ_TRA);
            if (lTmp > 0)
            {
               iTmp = sprintf(acTmp, "%.6d", lTmp);
               memcpy(pLienExtr->acTRA, acTmp, iTmp);
            }

            // Full exemption flag
            if (pRec->NonTaxCode > '0')
               pLienExtr->SpclFlag = LX_FULLEXE_FLG;

            // Prop 8


            pLienExtr->LF[0] = 10;
            pLienExtr->LF[1] = 0;

            fputs(acBuf, fdLienExt);

            lCnt++;
            if (!(lCnt % 1000))
               printf("\r%d", lCnt);
         }
      }
      
      // Get next roll record
      pTmp = fgets(acRollRec, 1024, fdRoll);
   }

   // Close files
   if (fdRoll)
      fclose(fdRoll);
   if (fdLienExt)
      fclose(fdLienExt);

   LogMsg("Total output records:       %u", lCnt);
   printf("\nTotal output records: %u\n", lCnt);

   return 0;
}

// Extract values from "2022 Secured Parcel Data.csv"
int Ven_CreateLienRec(char *pOutbuf, char *pRollRec)
{
   int      iRet, lTmp;
   char     acTmp[64], *apTokens[32];
   LIENEXTR *pLienExtr = (LIENEXTR *)pOutbuf;

   iTokens = ParseStringNQ(pRollRec, '|', VEN_L_FLDS+1, apTokens);
   if (iTokens < VEN_L_FLDS)
   {
      LogMsg("***** Error: bad input record for APN=%s", apTokens[VEN_L_APN]);
      return -1;
   }

   // Clear output buffer
   memset(pOutbuf, ' ', sizeof(LIENEXTR));

   // unformatted APN
   vmemcpy(pLienExtr->extra.Ven.PrevApn, apTokens[VEN_L_APN], iApnLen);
   // formatted APN
   iRet = Ven_FormatApn(apTokens[VEN_L_APN], acTmp);
   memcpy(pLienExtr->acApn, acTmp, iRet);

   // TRA
   lTmp = atol(apTokens[VEN_L_TRA]);
   if (lTmp > 0)
   {
      iRet = sprintf(acTmp, "%.6d", lTmp);
      memcpy(pLienExtr->acTRA, acTmp, 6);
   }

   // Full exemption flag
   if (strstr(apTokens[VEN_L_LANDUMTN], "WHOLLY EXEMPT"))
      pLienExtr->SpclFlag = LX_FULLEXE_FLG;

   // Land
   long lLand = atol(apTokens[VEN_L_LAND]);
   if (lLand > 0)
   {
      sprintf(acTmp, "%*u", SIZ_LIEN_LAND, lLand); 
      memcpy(pLienExtr->acLand, acTmp, SIZ_LIEN_LAND);
   }

   // Impr
   long lImpr = atol(apTokens[VEN_L_IMPR]);
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*u", SIZ_LIEN_LAND, lImpr); 
      memcpy(pLienExtr->acImpr, acTmp, SIZ_LIEN_LAND);
   }

   // Other values
   long lFixt = atoi(apTokens[VEN_L_FIXTURE]);
   long lPP   = atoi(apTokens[VEN_L_PERSPROP]);
   long lTree = atoi(apTokens[VEN_L_TREESVINES]);
   long lMineral = atoi(apTokens[VEN_L_MINERALRIGHTS]);

   lTmp = lMineral+lTree+lFixt+lPP;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_LIEN_OTHERS, lTmp);
      memcpy(pLienExtr->acOther, acTmp, SIZ_LIEN_OTHERS);
      
      if (lMineral > 0)
      {
         sprintf(acTmp, "%*d", SIZ_LIEN_FIXT, lMineral);
         memcpy(pLienExtr->extra.Ven.Mineral, acTmp, SIZ_LIEN_FIXT);
      }
      if (lTree > 0)
      {
         sprintf(acTmp, "%*d", SIZ_LIEN_FIXT, lTree);
         memcpy(pLienExtr->extra.Ven.Tree, acTmp, SIZ_LIEN_FIXT);
      }
      if (lFixt > 0)
      {
         sprintf(acTmp, "%*d", SIZ_LIEN_FIXT, lFixt);
         memcpy(pLienExtr->acME_Val, acTmp, SIZ_LIEN_FIXT);
      }
      if (lPP > 0)
      {
         sprintf(acTmp, "%*d", SIZ_LIEN_FIXT, lPP);
         memcpy(pLienExtr->acPP_Val, acTmp, SIZ_LIEN_FIXT);
      }
   }

   // Gross
   lTmp += lLand+lImpr;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_GROSS, lTmp);
      memcpy(pLienExtr->acGross, acTmp, SIZ_GROSS);

      // Ratio
      if (lImpr > 0)
      {
         sprintf(acTmp, "%*d", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
         memcpy(pLienExtr->acRatio, acTmp, SIZ_RATIO);
      }
   }

   long lExe = atol(apTokens[VEN_L_EXEAMOUNT]);
   if (*apTokens[VEN_L_EXETYPENAME] == 'H')
      pLienExtr->acHO[0] = '1';      // 'Y'
   else if (lExe > 0)
      pLienExtr->acHO[0] = '2';      // 'N'

   if (lExe > 0)
   {
      sprintf(acTmp, "%*d", SIZ_LIEN_EXEAMT, lExe);
      memcpy(pLienExtr->acExAmt, acTmp, SIZ_LIEN_EXEAMT);
      pLienExtr->acExCode[0] = *apTokens[VEN_L_EXETYPENAME];
   }

   // Year assessed
   memcpy(pLienExtr->acYear, myCounty.acYearAssd, 4);

   pLienExtr->LF[0] = 10;
   pLienExtr->LF[1] = 0;

   return 0;
}

// Extract values from "2024-25 Secured Roll.csv"
int Ven_CreateLienRec3(char *pOutbuf, char *pRollRec)
{
   int      iRet, lTmp;
   //char     acTmp[64], *apTokens[32];
   char     acTmp[64];
   LIENEXTR *pLienExtr = (LIENEXTR *)pOutbuf;

   iTokens = ParseStringNQ(pRollRec, '|', VEN_L3_FLDS+1, apTokens);
   if (iTokens < VEN_L3_FLDS)
   {
      LogMsg("***** Error: bad input record for APN=%s", apTokens[VEN_L3_APN]);
      return -1;
   }

   // Clear output buffer
   memset(pOutbuf, ' ', sizeof(LIENEXTR));

   // unformatted APN
   vmemcpy(pLienExtr->extra.Ven.PrevApn, apTokens[VEN_L3_APN], iApnLen);
   // formatted APN
   iRet = Ven_FormatApn(apTokens[VEN_L3_APN], acTmp);
   memcpy(pLienExtr->acApn, acTmp, iRet);

   // TRA
   lTmp = atol(apTokens[VEN_L3_TRA]);
   if (lTmp > 0)
   {
      iRet = sprintf(acTmp, "%.6d", lTmp);
      memcpy(pLienExtr->acTRA, acTmp, 6);
   }

   // Full exemption flag
   //if (strstr(apTokens[VEN_L3_LANDUMTN], "WHOLLY EXEMPT"))
   //   pLienExtr->SpclFlag = LX_FULLEXE_FLG;

   // Land
   long lLand = atol(apTokens[VEN_L3_LAND]);
   if (lLand > 0)
   {
      sprintf(acTmp, "%*u", SIZ_LIEN_LAND, lLand); 
      memcpy(pLienExtr->acLand, acTmp, SIZ_LIEN_LAND);
   }

   // Impr
   long lImpr = atol(apTokens[VEN_L3_IMPR]);
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*u", SIZ_LIEN_LAND, lImpr); 
      memcpy(pLienExtr->acImpr, acTmp, SIZ_LIEN_LAND);
   }

   // Other values
   long lFixt = atoi(apTokens[VEN_L3_FIXTURE]);
   long lPP   = atoi(apTokens[VEN_L3_PERSPROP]);
   long lTree = atoi(apTokens[VEN_L3_TREESVINES]);
   long lMineral = atoi(apTokens[VEN_L3_MINERALRIGHTS]);

   lTmp = lMineral+lTree+lFixt+lPP;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_LIEN_OTHERS, lTmp);
      memcpy(pLienExtr->acOther, acTmp, SIZ_LIEN_OTHERS);
      
      if (lMineral > 0)
      {
         sprintf(acTmp, "%*d", SIZ_LIEN_FIXT, lMineral);
         memcpy(pLienExtr->extra.Ven.Mineral, acTmp, SIZ_LIEN_FIXT);
      }
      if (lTree > 0)
      {
         sprintf(acTmp, "%*d", SIZ_LIEN_FIXT, lTree);
         memcpy(pLienExtr->extra.Ven.Tree, acTmp, SIZ_LIEN_FIXT);
      }
      if (lFixt > 0)
      {
         sprintf(acTmp, "%*d", SIZ_LIEN_FIXT, lFixt);
         memcpy(pLienExtr->acME_Val, acTmp, SIZ_LIEN_FIXT);
      }
      if (lPP > 0)
      {
         sprintf(acTmp, "%*d", SIZ_LIEN_FIXT, lPP);
         memcpy(pLienExtr->acPP_Val, acTmp, SIZ_LIEN_FIXT);
      }
   }

   // Gross
   lTmp += lLand+lImpr;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_GROSS, lTmp);
      memcpy(pLienExtr->acGross, acTmp, SIZ_GROSS);

      // Ratio
      if (lImpr > 0)
      {
         sprintf(acTmp, "%*d", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
         memcpy(pLienExtr->acRatio, acTmp, SIZ_RATIO);
      }
   }

   long lExe1 = atol(apTokens[VEN_L3_EXEAMT1]);
   long lExe2 = atol(apTokens[VEN_L3_EXEAMT2]);
   lTmp = lExe1+lExe2;

   if (!memcmp(apTokens[VEN_L3_EXE_CD1], "HOM", 3) || !memcmp(apTokens[VEN_L3_EXE_CD2], "HOM", 3))
      pLienExtr->acHO[0] = '1';      // 'Y'
   else 
      pLienExtr->acHO[0] = '2';      // 'N'

   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_LIEN_EXEAMT, lTmp);
      memcpy(pLienExtr->acExAmt, acTmp, SIZ_LIEN_EXEAMT);
      vmemcpy(pLienExtr->extra.Ven.Exe_Code1, apTokens[VEN_L3_EXE_CD1], VENSIZ_EXECODE);
      vmemcpy(pLienExtr->extra.Ven.Exe_Code2, apTokens[VEN_L3_EXE_CD2], VENSIZ_EXECODE);
   }

   // Year assessed
   memcpy(pLienExtr->acYear, myCounty.acYearAssd, 4);

   pLienExtr->LF[0] = 10;
   pLienExtr->LF[1] = 0;

   return 0;
}

/***************************** Ven_ExtrLienCsv ******************************
 *
 *
 ****************************************************************************/

int Ven_ExtrLienCsv(LPCSTR pCnty, LPCSTR pLDRFile=NULL)
{
   char     *pTmp, acBuf[1024], acRec[MAX_RECSIZE], acOutFile[_MAX_PATH];
   int      iRet, iNewRec=0, lCnt=0;
   FILE     *fdLienExt;

   LogMsg("\nExtract lien roll for %s", pCnty);

   // Open lien file
   if (pLDRFile)
      strcpy(acBuf, pLDRFile);
   else
      strcpy(acBuf, acLienFile);

   LogMsg("Open LDR Value file %s", acBuf);

   // Sort on ASMT - file is already sorted
   //sprintf(acTmpFile, "%s\\%s\\%s_LienVal.srt", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   //iRet = sortFile(acBuf, acTmpFile, "S(#1,C,A) DEL(124) ");
   fdRoll = fopen(acBuf, "r");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening sorted value file: %s\n", acBuf);
      return 2;
   }

   // Create lien extract
   sprintf(acOutFile, acLienTmpl, pCnty, pCnty);
   LogMsg("Open lien extract file %s", acOutFile);
   fdLienExt = fopen(acOutFile, "w");
   if (fdLienExt == NULL)
   {
      LogMsg("***** Error creating lien extract file: %s\n", acOutFile);
      return -2;
   }

   // Get 1st rec
   pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdRoll);
   pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdRoll);

   // Merge loop
   while (pTmp && !feof(fdRoll))
   {
      // Create new record
      if (lLienYear <= 2023)
         iRet = Ven_CreateLienRec(acBuf, acRec);    
      else
         iRet = Ven_CreateLienRec3(acBuf, acRec);    
      if (!iRet)
      {
         // Write to output
         fputs(acBuf, fdLienExt);

         iNewRec++;
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);

      // Get next roll record
      pTmp = fgets(acRec, MAX_RECSIZE, fdRoll);
      if (!isdigit(acRec[1]))
         break;      // EOF
   }

   // Close files
   if (fdRoll)
      fclose(fdRoll);
   if (fdLienExt)
      fclose(fdLienExt);

   LogMsg("Total output records:       %u", iNewRec);
   LogMsg("Total records processed:    %u", lCnt);
   printf("\nTotal output records: %u", lCnt);

   lRecCnt = lCnt;
   return 0;
}

// Convert GRGR_DEF to SCSAL_REC
static int Grgr2Sale(char *pInbuf, char *pOutbuf)
{
   GRGR_DEF  *pSale  = (GRGR_DEF *) pInbuf;
   SCSAL_REC *pCSale = (SCSAL_REC *)pOutbuf;
   int       iTmp, iDocNum;
   char      acTmp[1024];

   if (!isdigit(pSale->APN[0]))
      return -1;

   iDocNum = atoin(pSale->DocNum, SALE_SIZ_DOCNUM);
   if (!iDocNum)
      return -1;

   memcpy(pCSale->Apn, pSale->APN, SALE_SIZ_APN);
   memcpy(pCSale->DocDate, pSale->DocDate, SALE_SIZ_DOCDATE);
   iTmp = sprintf(acTmp, "%.2s%.7d", &pSale->DocDate[2], iDocNum);
   memcpy(pCSale->DocNum, acTmp, iTmp);

   if (isdigit(pSale->DocTitle[0]))
      memcpy(pCSale->DocType, pSale->DocTitle, 3);
   else
   {
      if (!memcmp(pSale->DocTitle, "ADEA", 4))
      {
         pCSale->DocType[0] = '6';
         pCSale->NoneSale_Flg = 'Y';
      } else if (!memcmp(pSale->DocTitle, "NOTI", 4))
      {  // Don't know what kind of notice
         //memcpy(pCSale->DocType, "   ", 3);
         pCSale->NoneSale_Flg = 'Y';
      } else
         LogMsg("Unknown DocTitle: %.8s [APN=%.*s] [DOCNUM=%.12s]", pSale->DocTitle, iApnLen, pSale->APN, pSale->DocNum);
   }
   memcpy(pCSale->SalePrice, pSale->SalePrice, SALE_SIZ_SALEPRICE);
   memcpy(pCSale->StampAmt, pSale->Tax, SALE_SIZ_STAMPAMT);

   memcpy(pCSale->Seller1, pSale->Grantors[0].Name, SALE_SIZ_SELLER);
   memcpy(pCSale->Seller2, pSale->Grantors[1].Name, SALE_SIZ_SELLER);
   memcpy(pCSale->Name1, pSale->Grantees[0].Name, SIZ_GR_NAME);
   memcpy(pCSale->Name2, pSale->Grantees[1].Name, SIZ_GR_NAME);
   memcpy(pCSale->CareOf, pSale->CurCareOf, SIZ_GR_NAME);

   if (pSale->CurMailing.strName[0] > ' ')
   {
      sprintf(acTmp, "%s %s %s %s %s %s", pSale->CurMailing.strNum,
         pSale->CurMailing.strSub, pSale->CurMailing.strDir,
         pSale->CurMailing.strName,pSale->CurMailing.strSfx,pSale->CurMailing.Unit);

      iTmp = blankRem(acTmp);
      if (iTmp > SALE_SIZ_M_ADR1)
         iTmp = SALE_SIZ_M_ADR1;
      memcpy(pCSale->MailAdr1, acTmp, iTmp);

      sprintf(acTmp, "%s, %s", pSale->CurMailing.City, pSale->CurMailing.State);
      iTmp = blankRem(acTmp);
      if (iTmp > SALE_SIZ_M_ADR2)
         iTmp = SALE_SIZ_M_ADR2;
      memcpy(pCSale->MailAdr2, acTmp, iTmp);

      memcpy(pCSale->MailZip, pSale->CurMailing.Zip, SIZ_M_ZIP+SIZ_M_ZIP4);
   }

   // Multi parcel
   iTmp = atoin(pSale->ParcelCount, SIZ_GR_PRCLCNT);
   if (iTmp > 1)
      pCSale->MultiSale_Flg = 'Y';

   pCSale->Etal = pSale->MoreName;
   pCSale->OwnerMatched = pSale->Owner_Matched;
   pCSale->ApnMatched = pSale->APN_Matched;
   pCSale->ARCode = 'R';

   return 0;
}

/******************************** Ven_GrGr2Sale *******************************
 *
 ******************************************************************************/

int Ven_GrGr2Sale(char *pCnty, char *pInfile, char *pOutfile)
{
   char     acInbuf[2048], acOutbuf[2048], *pRec;
   char     acOutFile[_MAX_PATH], acSrtFile[_MAX_PATH];
   long     lCnt=0, lOut=0, iRet;
   FILE     *fdOut;

   SCSAL_REC *pOutRec = (SCSAL_REC *)&acOutbuf[0];

   if (_access(pInfile, 0))
   {
      LogMsg("***** ConvertSaleData(): Missing input file: %s", pInfile);
      return -1;
   }

   sprintf(acOutFile, "%s\\%s\\%s_Sale.tmp", acTmpPath, pCnty, pCnty);
   LogMsg("Convert %s to %s.", pInfile, acOutFile);

   // Open input file
   LogMsg("Open input sale file %s", pInfile);
   fdSale = fopen(pInfile, "r");
   if (fdSale == NULL)
   {
      LogMsg("***** Error opening sale file: %s\n", pInfile);
      return -2;
   }

   // Open output file
   LogMsg("Create output sale file %s", acOutFile);
   fdOut = fopen(acOutFile, "w");
   if (fdOut == NULL)
   {
      LogMsg("***** Error creating output sale file: %s\n", acOutFile);
      return -3;
   }

   // Convert loop
   while (!feof(fdSale))
   {
      if (!(pRec = fgets(acInbuf, 2048, fdSale)))
         break;

#ifdef _DEBUG
      //if (!memcmp(acInbuf, "1064202300", 10))
      //   iRet = 0;
#endif
      // Drop bad record
      //iRet = atoin(acInbuf, 6);
      //if (acInbuf[0] == ' ' || iRet < 1000 || iRet > 999998)
      //{
      //   lCnt++;
      //   continue;
      //}

      // Reformat sale
      memset(acOutbuf, ' ', sizeof(SCSAL_REC));
      iRet = Grgr2Sale(acInbuf, acOutbuf);

      // Write to output file
      if (!iRet && pOutRec->DocDate[0] >= '0')
      {
         pOutRec->CRLF[0] = '\n';
         pOutRec->CRLF[1] = 0;

         fputs(acOutbuf, fdOut);
         lOut++;
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdSale)
      fclose(fdSale);
   if (fdOut)
      fclose(fdOut);

   // Sort sale file - APN (asc), Saledate (asc), DocNum (asc), Sale price (desc)
   sprintf(acInbuf, "S(1,14,C,A,27,8,C,A,15,12,C,A,57,10,C,D) DUPO(1,34)");
   if (pOutfile)
      strcpy(acSrtFile, pOutfile);
   else
      sprintf(acSrtFile, acESalTmpl, pCnty, pCnty, "sls");
   iRet = sortFile(acOutFile, acSrtFile, acInbuf);
   if (!iRet)
      iRet = 1;
   else
      iRet = 0;

   LogMsg("Total input records:        %u", lCnt);
   LogMsg("Total output records:       %u", lOut);

   printf("\nTotal output records: %u\n", lCnt);
   return iRet;
}

/**************************** Ven_FormatSale ******************************
 *
 **************************************************************************/

int Ven_FormatRollSale(char *pOutbuf, char *pRoll, char *pSaleEx)
{
   char   acTmp[32];
   int    iTmp, lPrice;
   double dTax; 

   VEN_ROLL  *pRollRec= (VEN_ROLL  *)pRoll;
   SCSAL_REC *pOutRec = (SCSAL_REC *)pOutbuf;

   // Ignore sale that has no sale date
   iTmp = atoin(pRollRec->Doc_Dt, RSIZ_DOC_DT);
   if (iTmp < 1900)
      return 1;

   // APN
   iTmp = Ven_FormatApn(pRollRec->Apn, acTmp);
   memcpy(pOutRec->Apn, acTmp, iTmp);
   memcpy(pOutRec->OtherApn, pRollRec->Apn, iTmp);

   // Update current sale
   memcpy(pOutRec->DocDate, pRollRec->Doc_Dt,  RSIZ_DOC_DT);
   memcpy(pOutRec->DocNum,  pRollRec->DocNum,  RSIZ_DOCNUM);

   if (pRollRec->DocType[0] > ' ')
   {
      memcpy(pOutRec->DocCode, pRollRec->DocType, RSIZ_DOCTYPE);
      iTmp = findDocType(pRollRec->DocType, (IDX_TBL5 *)&VEN_DocCode[0]);
      if (iTmp >= 0)
      {
         memcpy(pOutRec->DocType, VEN_DocCode[iTmp].pCode, VEN_DocCode[iTmp].iCodeLen);
         pOutRec->NoneSale_Flg = VEN_DocCode[iTmp].flag;
      } 
   }

   // Current owner 
   memcpy(pOutRec->Name1, pRollRec->Name1, RSIZ_NAME1);
   if (!memcmp(pRollRec->Name2, "ATTN", 4))
      memcpy(pOutRec->CareOf, pRollRec->Name2, RSIZ_NAME2);
   else
      memcpy(pOutRec->Name2, pRollRec->Name2, RSIZ_NAME2);

   // Mail addr
   memcpy(pOutRec->MailAdr1, pRollRec->M_Street, RSIZ_M_STREET);
   memcpy(pOutRec->MailAdr2, pRollRec->M_City,   RSIZ_M_CITY);
   memcpy(pOutRec->MailZip,  pRollRec->M_Zip,    RSIZ_M_ZIP);

   pOutRec->ARCode = 'A';

   // Sale price
   dTax = atofn(pRollRec->Doc_Tax, SSIZ_DOC_TAX);
   if (dTax > 0.0)
   {
      lPrice = (long)((dTax * SALE_FACTOR)/100.0) * 100;

      // Check sale date - if it's the same as RecDate, use DocTax
      if (!memcmp(pRollRec->Doc_Dt, pRollRec->SaleDate, 8))
      {
         sprintf(acTmp, "%*u", SIZ_SALE1_AMT, lPrice);
         memcpy(pOutRec->SalePrice, acTmp, SIZ_SALE1_AMT);
         iTmp = sprintf(acTmp, "%.2f", dTax);
         memcpy(pOutRec->StampAmt, acTmp, iTmp);
      } else
      {  // Create extra sale record
         SCSAL_REC *pExRec = (SCSAL_REC *)pSaleEx;

         memcpy(pExRec->Apn, pOutRec->Apn, SALE_SIZ_APN);
         memcpy(pExRec->OtherApn, pOutRec->OtherApn, SALE_SIZ_APN);
         memcpy(pExRec->DocDate, pRollRec->SaleDate, SSIZ_DATE);
         pExRec->DocType[0] = '1';
         pExRec->Spc_Flg = '2';     // No DocNum

         sprintf(acTmp, "%*u", SIZ_SALE1_AMT, lPrice);
         memcpy(pExRec->SalePrice, acTmp, SIZ_SALE1_AMT);
         iTmp = sprintf(acTmp, "%.2f", dTax);
         memcpy(pExRec->StampAmt, acTmp, iTmp);
      }
   }

   return 0;
}

/**************************** Ven_FormatUpdtSale **************************
 *
 * If SaleDate is the same with RecDate, use DocTax for that transaction.
 * If not, create new record with APN, SaleDate, and DocTax.  This record
 * will be match with GRGR data to create new sale record.
 *
 **************************************************************************/

int Ven_FormatUpdtSale(char *pOutbuf, char *pRoll, char  *pSaleEx)
{
   char   acTmp[32];
   int    iTmp, lPrice;
   double dTax; 

   VEN_UPDT  *pSaleRec = (VEN_UPDT  *)pRoll;
   SCSAL_REC *pOutRec  = (SCSAL_REC *)pOutbuf;

   // Ignore sale that has no sale date
   iTmp = atoin(pSaleRec->Doc_Dt, SSIZ_DATE);
   if (iTmp < 1900)
      return 1;

   // APN
   iTmp = Ven_FormatApn(pSaleRec->Apn, acTmp);
   memcpy(pOutRec->Apn, acTmp, iTmp);
   memcpy(pOutRec->OtherApn, pSaleRec->Apn, iTmp);

   // Update current sale
   memcpy(pOutRec->DocDate, pSaleRec->Doc_Dt, SSIZ_DATE);
   memcpy(pOutRec->DocNum,  pSaleRec->DocNum, SSIZ_DOC_NR);

   if (pSaleRec->DocType[0] > ' ')
   {
      memcpy(pOutRec->DocCode, pSaleRec->DocType, SALE_SIZ_DOCCODE);
      iTmp = findDocType(pSaleRec->DocType, (IDX_TBL5 *)&VEN_DocCode[0]);
      if (iTmp >= 0)
      {
         memcpy(pOutRec->DocType, VEN_DocCode[iTmp].pCode, VEN_DocCode[iTmp].iCodeLen);
         pOutRec->NoneSale_Flg = VEN_DocCode[iTmp].flag;
      } 
   }

   // Current owner 
   memcpy(pOutRec->Name1, pSaleRec->Name1, SSIZ_NAME1);
   if (!memcmp(pSaleRec->Name2, "ATTN", 4))
      memcpy(pOutRec->CareOf, pSaleRec->Name2, SSIZ_NAME2);
   else
      memcpy(pOutRec->Name2, pSaleRec->Name2, SSIZ_NAME2);

   // Mail addr
   memcpy(pOutRec->MailAdr1, pSaleRec->M_Addr1, SSIZ_MAIL_ADDR);
   memcpy(pOutRec->MailAdr2, pSaleRec->M_CtySt, SSIZ_CTY_STA);
   memcpy(pOutRec->MailZip,  pSaleRec->M_Zip,   SALE_SIZ_M_ZIP);

   pOutRec->ARCode = 'A';

   // Sale price
   dTax = atofn(pSaleRec->Doc_Tax, SSIZ_DOC_TAX);
   if (dTax > 0.0)
   {
      lPrice = (long)((dTax * SALE_FACTOR)/100.0) * 100;

      // Check sale date - if it's the same as RecDate, use DocTax
      if (!memcmp(pSaleRec->Doc_Dt, pSaleRec->SaleDate, 8))
      {
         sprintf(acTmp, "%*u", SIZ_SALE1_AMT, lPrice);
         memcpy(pOutRec->SalePrice, acTmp, SIZ_SALE1_AMT);
         iTmp = sprintf(acTmp, "%.2f", dTax);
         memcpy(pOutRec->StampAmt, acTmp, iTmp);
         pOutRec->Spc_Flg = '1';                            // Confirm sale
      } else
      {  // Create extra sale record
         SCSAL_REC *pExRec = (SCSAL_REC *)pSaleEx;

         memcpy(pExRec->Apn, pOutRec->Apn, SALE_SIZ_APN);
         memcpy(pExRec->OtherApn, pOutRec->OtherApn, SALE_SIZ_APN);
         memcpy(pExRec->DocDate, pSaleRec->SaleDate, SSIZ_DATE);
         pExRec->DocType[0] = '1';
         pExRec->Spc_Flg = '2';                             // No DocNum

         sprintf(acTmp, "%*u", SIZ_SALE1_AMT, lPrice);
         memcpy(pExRec->SalePrice, acTmp, SIZ_SALE1_AMT);
         iTmp = sprintf(acTmp, "%.2f", dTax);
         memcpy(pExRec->StampAmt, acTmp, iTmp);
      }
   }

   return 0;
}

int Ven_FormatUpdtSale2(char *pOutbuf, char *pRoll, char  *pSaleEx)
{
   char   acTmp[32];
   int    iTmp, lPrice;
   double dTax; 

   VEN_UPDT2 *pSaleRec = (VEN_UPDT2 *)pRoll;
   SCSAL_REC *pOutRec  = (SCSAL_REC *)pOutbuf;

   // Ignore sale that has no sale date
   iTmp = atoin(pSaleRec->Doc_Dt, SSIZ_DATE);
   if (iTmp < 1900)
      return 1;

   // APN
   iTmp = Ven_FormatApn(pSaleRec->Apn, acTmp);
   memcpy(pOutRec->Apn, acTmp, iTmp);
   memcpy(pOutRec->OtherApn, pSaleRec->Apn, iTmp);

   // Update current sale
   memcpy(pOutRec->DocDate, pSaleRec->Doc_Dt, SSIZ_DATE);
   iTmp = blankRem(pSaleRec->DocNum, SSIZ_DOC_NR2);
   if (iTmp == SSIZ_DOC_NR)
      memcpy(pOutRec->DocNum,  pSaleRec->DocNum, SSIZ_DOC_NR);
   else if (iTmp == SSIZ_DOC_NR2)
   {
      // New DocNum format
      memcpy(pOutRec->DocNum,  &pSaleRec->DocNum[2], 2);
      memcpy(&pOutRec->DocNum[2],  &pSaleRec->DocNum[6], 7);      
   } else
   {
      LogMsg("*** Questionable DocNum: %s [%s]", pSaleRec->DocNum, acTmp);
      return 1;
   }

   if (pSaleRec->DocType[0] > ' ')
   {
      memcpy(pOutRec->DocCode, pSaleRec->DocType, SALE_SIZ_DOCCODE);
      iTmp = findDocType(pSaleRec->DocType, (IDX_TBL5 *)&VEN_DocCode[0]);
      if (iTmp >= 0)
      {
         memcpy(pOutRec->DocType, VEN_DocCode[iTmp].pCode, VEN_DocCode[iTmp].iCodeLen);
         pOutRec->NoneSale_Flg = VEN_DocCode[iTmp].flag;
      } 
   }

   // Current owner 
   memcpy(pOutRec->Name1, pSaleRec->Name1, SSIZ_NAME1);
   if (!memcmp(pSaleRec->Name2, "ATTN", 4))
      memcpy(pOutRec->CareOf, pSaleRec->Name2, SSIZ_NAME2);
   else
      memcpy(pOutRec->Name2, pSaleRec->Name2, SSIZ_NAME2);

   // Mail addr
   memcpy(pOutRec->MailAdr1, pSaleRec->M_Addr1, SSIZ_MAIL_ADDR);
   memcpy(pOutRec->MailAdr2, pSaleRec->M_CtySt, SSIZ_CTY_STA);
   memcpy(pOutRec->MailZip,  pSaleRec->M_Zip,   SALE_SIZ_M_ZIP);

   pOutRec->ARCode = 'A';

   // Sale price
   dTax = atofn(pSaleRec->Doc_Tax, SSIZ_DOC_TAX);
   if (dTax > 0.0)
   {
      lPrice = (long)((dTax * SALE_FACTOR)/100.0) * 100;

      // Check sale date - if it's the same as RecDate, use DocTax
      if (!memcmp(pSaleRec->Doc_Dt, pSaleRec->SaleDate, 8))
      {
         sprintf(acTmp, "%*u", SIZ_SALE1_AMT, lPrice);
         memcpy(pOutRec->SalePrice, acTmp, SIZ_SALE1_AMT);
         iTmp = sprintf(acTmp, "%.2f", dTax);
         memcpy(pOutRec->StampAmt, acTmp, iTmp);
         pOutRec->Spc_Flg = '1';                            // Confirm sale
      } else
      {  // Create extra sale record
         SCSAL_REC *pExRec = (SCSAL_REC *)pSaleEx;

         memcpy(pExRec->Apn, pOutRec->Apn, SALE_SIZ_APN);
         memcpy(pExRec->OtherApn, pOutRec->OtherApn, SALE_SIZ_APN);
         memcpy(pExRec->DocDate, pSaleRec->SaleDate, SSIZ_DATE);
         pExRec->DocType[0] = '1';
         pExRec->Spc_Flg = '2';                             // No DocNum

         sprintf(acTmp, "%*u", SIZ_SALE1_AMT, lPrice);
         memcpy(pExRec->SalePrice, acTmp, SIZ_SALE1_AMT);
         iTmp = sprintf(acTmp, "%.2f", dTax);
         memcpy(pExRec->StampAmt, acTmp, iTmp);
      }
   }

   return 0;
}

int Ven_FormatUpdtSaleX(char *pOutbuf, char *pRoll)
{
   char   acTmp[32];
   int    iTmp, lPrice;
   double dTax; 

   VEN_UPDT  *pSaleRec = (VEN_UPDT  *)pRoll;
   SCSAL_REC *pOutRec  = (SCSAL_REC *)pOutbuf;

   // Ignore sale that has no sale date
   iTmp = atoin(pSaleRec->Doc_Dt, SSIZ_DATE);
   if (iTmp < 1900)
      return 1;

   // APN
   iTmp = Ven_FormatApn(pSaleRec->Apn, acTmp);
   memcpy(pOutRec->Apn, acTmp, iTmp);
   memcpy(pOutRec->OtherApn, pSaleRec->Apn, iTmp);

   // Update current sale
   memcpy(pOutRec->DocDate, pSaleRec->Doc_Dt, SSIZ_DATE);
   memcpy(pOutRec->DocNum,  pSaleRec->DocNum, SSIZ_DOC_NR);

   // Sale price
   dTax = atofn(pSaleRec->Doc_Tax, SSIZ_DOC_TAX);
   if (dTax > 0.0)
   {
      lPrice = (long)((dTax * SALE_FACTOR)/100.0) * 100;
      sprintf(acTmp, "%*u", SIZ_SALE1_AMT, lPrice);
      memcpy(pOutRec->SalePrice, acTmp, SIZ_SALE1_AMT);
      iTmp = sprintf(acTmp, "%.2f", dTax);
      memcpy(pOutRec->StampAmt, acTmp, iTmp);
   }
   
   if (pSaleRec->DocType[0] > ' ')
   {
      memcpy(pOutRec->DocCode, pSaleRec->DocType, SALE_SIZ_DOCCODE);
      iTmp = findDocType(pSaleRec->DocType, (IDX_TBL5 *)&VEN_DocCode[0]);
      if (iTmp >= 0)
      {
         memcpy(pOutRec->DocType, VEN_DocCode[iTmp].pCode, VEN_DocCode[iTmp].iCodeLen);
         pOutRec->NoneSale_Flg = VEN_DocCode[iTmp].flag;
      } 
   }

   // Current owner 
   memcpy(pOutRec->Name1, pSaleRec->Name1, SSIZ_NAME1);
   if (!memcmp(pSaleRec->Name2, "ATTN", 4))
      memcpy(pOutRec->CareOf, pSaleRec->Name2, SSIZ_NAME2);
   else
      memcpy(pOutRec->Name2, pSaleRec->Name2, SSIZ_NAME2);

   // Mail addr
   memcpy(pOutRec->MailAdr1, pSaleRec->M_Addr1, SSIZ_MAIL_ADDR);
   memcpy(pOutRec->MailAdr2, pSaleRec->M_CtySt, SSIZ_CTY_STA);
   memcpy(pOutRec->MailZip,  pSaleRec->M_Zip,   SSIZ_ZIP);

   pOutRec->ARCode = 'A';

   return 0;
}

/****************************** Ven_UpdateCumSale ****************************
 *
 * Update history sale.  This function adds new sale record to cumulative file.
 *
 * Return 0 if successful, otherwise error
 *
 *****************************************************************************

int Ven_UpdateCumSale(char *pSalefile)
{
   char     *pTmp, acCSalRec[1024], acSaleRec[1024], acTmp[256];
   char     acOutFile[_MAX_PATH], acHistSale[_MAX_PATH], acTmpSale[_MAX_PATH];

   FILE     *fdCSalee, *fdSale;
   int      iRet, iTmp, iUpdateSale=0;
   long     lLstSaleDt=0, lCnt=0;
   SCSAL_REC *pSaleRec =(SCSAL_REC *)&acCSalRec[0];
   VEN_UPDT  *pRollUpdt=(VEN_UPDT  *)&acSaleRec[0];

   LogMsg("Extract sales from current roll file %s", pSalefile);

   // Check current sale file
   if (_access(pSalefile, 0))
   {
      LogMsg("***** Input file missing %s", pSalefile);
      return -1;
   }

   // Open current sale
   LogMsg("Open roll update file %s", pSalefile);
   fdSale = fopen(pSalefile, "r");
   if (fdSale == NULL)
   {
      LogMsg("***** Error opening current roll update file: %s\n", pSalefile);
      return -2;
   }

   // Create output file
   sprintf(acTmpSale, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "tmp");
   LogMsg("Create updated sale file %s", acTmpSale);
   fdCSalee = fopen(acTmpSale, "w");
   if (fdCSalee == NULL)
   {
      LogMsg("***** Error creating updated sale file: %s\n", acTmpSale);
      return -3;
   }

   // Merge loop
   while (!feof(fdSale))
   {
      // Get current sale
      pTmp = fgets(acSaleRec, 1024, fdSale);
      if (!pTmp)
         break;

      // Clear buffer
      memset(acCSalRec, ' ', sizeof(SCSAL_REC));

      // Create new record
      iRet = Ven_FormatUpdtSale(acCSalRec, acSaleRec);
      if (!iRet)
      {
         strcpy(pSaleRec->CRLF, "\n");
         fputs(acCSalRec, fdCSalee);
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdCSalee)
      fclose(fdCSalee);
   if (fdSale)
      fclose(fdSale);

   // Update sale history file
   sprintf(acHistSale, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "sls");
   sprintf(acOutFile,  acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "dat");

   // Sort/merge sale file - APN (asc), Saledate (asc), DocNum (asc)
   sprintf(acTmp, "S(1,%d,C,A,27,8,C,A,15,%d,C,A,57,10,C,D) DUPO(1,34)", iApnLen, SALE_SIZ_DOCNUM);
   iTmp = sortFile(acTmpSale, acOutFile, acTmp);
   if (iTmp > 0)
   {
      strcpy(acSaleFile, acOutFile);
      if (!_access(acHistSale, 0))
      {
         sprintf(acSaleRec, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "srt");
         sprintf(acTmpSale, "%s+%s", acOutFile, acHistSale);
         iTmp = sortFile(acTmpSale, acSaleRec, acTmp);
         if (iTmp > 0)
         {
            DeleteFile(acHistSale);
      
            // Rename srt to SLS file
            rename(acSaleRec, acHistSale);
         }
      } else
      {
         rename(acOutFile, acHistSale);
      }
   }

   LogMsg("Total sale records processed:   %u", lCnt);
   LogMsg("Total cumulative sale records:  %u", iTmp);
   LogMsg("Total sale records updated:     %u", iUpdateSale);
   LogMsg("Latest recording date:          %u", lLstSaleDt);

   LogMsg("Update Sale History completed.");

   return 0;
}
*/

/******************************** Ven_ExtrSale *******************************
 *
 * Update history sale.  This function adds new sale record to cumulative file.
 *
 * Type: 1 = annual roll file
 *       2 = monthly update file
 *
 * Return 0 if successful, otherwise error
 *
 *****************************************************************************/

int Ven_ExtrSale(char *pInfile, int iType)
{
   char     *pTmp, acCSalRec[1024], acSaleRec[1024], acSaleEx[1024], acTmp[256];
   char     acOutFile[_MAX_PATH], acTmpSale[_MAX_PATH];

   FILE     *fdCSalee, *fdSale;
   int      iRet, iTmp, iUpdateSale=0;
   long     lLstSaleDt=0, lCnt=0;
   SCSAL_REC *pSaleRec =(SCSAL_REC *)&acCSalRec[0];
   SCSAL_REC *pSaleEx  =(SCSAL_REC *)&acSaleEx[0];

   LogMsg0("Extract sales from current roll file %s", pInfile);

   // Open current sale
   LogMsg("Open roll update file %s", pInfile);
   fdSale = fopen(pInfile, "r");
   if (fdSale == NULL)
   {
      LogMsg("***** Error opening current roll update file: %s\n", pInfile);
      return -2;
   }

   // Create output file
   if (iType == 1)
      sprintf(acTmpSale, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "rol");
   else
      sprintf(acTmpSale, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "upd");

   LogMsg("Create updated sale file %s", acTmpSale);
   fdCSalee = fopen(acTmpSale, "w");
   if (fdCSalee == NULL)
   {
      LogMsg("***** Error creating updated sale file: %s\n", acTmpSale);
      return -3;
   }

   // Merge loop
   while (!feof(fdSale))
   {
      // Get current sale
      pTmp = fgets(acSaleRec, 1024, fdSale);
      if (!pTmp)
         break;

      // Clear buffer
      memset(acCSalRec, ' ', sizeof(SCSAL_REC));
      memset(acSaleEx,  ' ', sizeof(SCSAL_REC));

      // Create new record
      if (iType == 1)
         iRet = Ven_FormatRollSale(acCSalRec, acSaleRec, acSaleEx);
      else if (iType == 2)
         iRet = Ven_FormatUpdtSale2(acCSalRec, acSaleRec, acSaleEx);
      else
         LogMsg("***** Invalid record type %d", iType);

#ifdef _DEBUG
      //if (!memcmp(acSaleRec, "0020100085", 9))
      //   iTmp = 0;
#endif

      if (!iRet)
      {
         // Validate DocNum
         if (!memcmp(&pSaleRec->DocDate[2], pSaleRec->DocNum, 2))
         {
            iTmp = atoin(pSaleRec->DocDate, 8);
            if (iTmp > lLstSaleDt && iTmp < lToday)
               lLstSaleDt = iTmp;

            strcpy(pSaleRec->CRLF, "\n");
            fputs(acCSalRec, fdCSalee);
         } else
            LogMsg("--> Drop sale: %.80s", acSaleRec);

         if (acSaleEx[0] > ' ')
         {
            strcpy(pSaleEx->CRLF, "\n");
            fputs(acSaleEx, fdCSalee);
         }
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdCSalee)
      fclose(fdCSalee);
   if (fdSale)
      fclose(fdSale);

   // Sort/merge sale file - APN (asc), Saledate (asc), DocNum (asc), SalePrice (desc), SpecialFlg (desc)
   sprintf(acTmp, "S(1,%d,C,A,27,8,C,A,15,%d,C,A,57,10,C,D,130,1,C,D) DUPO(1,34)", iApnLen, SALE_SIZ_DOCNUM);
   if (!_access(acCSalFile, 0))
   {
      sprintf(acSaleRec, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "srt");
      sprintf(acOutFile, "%s+%s", acTmpSale, acCSalFile);
      iTmp = sortFile(acOutFile, acSaleRec, acTmp);
      if (iTmp > 0)
      {
         sprintf(acOutFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "sav");
         if (!_access(acOutFile, 0))
            DeleteFile(acOutFile);

         // Rename sls to sav
         rename(acCSalFile, acOutFile);
   
         // Rename srt to SLS file
         rename(acSaleRec, acCSalFile);
      }
   } else
   {
      iTmp = sortFile(acTmpSale, acCSalFile, acTmp);
   }

   LogMsg("Total sale records processed:   %u", lCnt);
   LogMsg("Total cumulative sale records:  %u", iTmp);
   LogMsg("Latest recording date:          %u", lLstSaleDt);

   LogMsg("Update Sale History completed.");

   return 0;
}

/******************************** Ven_ExtrSale *******************************
 *
 * Update history sale.  This function adds new sale record to cumulative file.
 *
 * Return number of records, otherwise error
 *
 *****************************************************************************


int Ven_ExtrSale(char *pInfile, FILE *fdCSalee)
{
   char     *pTmp, acCSalRec[1024], acSaleRec[1024], acSaleEx[1024], acTmp[256];
   char     acOutFile[_MAX_PATH], acTmpSale[_MAX_PATH];

   FILE     *fdSale;
   int      iRet, iTmp, iUpdateSale=0;
   long     lLstSaleDt=0, lCnt=0;
   SCSAL_REC *pSaleRec =(SCSAL_REC *)&acCSalRec[0];
   SCSAL_REC *pSaleEx  =(SCSAL_REC *)&acSaleEx[0];

   LogMsg("Extract sales from current roll file %s", pInfile);

   // Open current sale
   LogMsg("Open roll update file %s", pInfile);
   fdSale = fopen(pInfile, "r");
   if (fdSale == NULL)
   {
      LogMsg("***** Error opening current roll update file: %s\n", pInfile);
      return -2;
   }

   // Merge loop
   while (!feof(fdSale))
   {
      // Get current sale
      pTmp = fgets(acSaleRec, 1024, fdSale);
      if (!pTmp)
         break;

      // Clear buffer
      memset(acCSalRec, ' ', sizeof(SCSAL_REC));
      memset(acSaleEx,  ' ', sizeof(SCSAL_REC));

      // Create new record
      iRet = Ven_FormatUpdtSale(acCSalRec, acSaleRec, acSaleEx);

#ifdef _DEBUG
      //if (!memcmp(acSaleRec, "0020100085", 9))
      //   iTmp = 0;
#endif

      if (!iRet)
      {
         // Validate DocNum
         if (!memcmp(&pSaleRec->DocDate[2], pSaleRec->DocNum, 2))
         {
            iTmp = atoin(pSaleRec->DocDate, 8);
            if (iTmp > lLstSaleDt && iTmp < lToday)
               lLstSaleDt = iTmp;

            strcpy(pSaleRec->CRLF, "\n");
            fputs(acCSalRec, fdCSalee);
         } else
            LogMsg("--> Drop sale: %.80s", acSaleRec);

         if (acSaleEx[0] > ' ')
         {
            strcpy(pSaleEx->CRLF, "\n");
            fputs(acSaleEx, fdCSalee);
         }
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);

   }

   // Close files
   if (fdSale)
      fclose(fdSale);

   LogMsg("Total sale records processed:   %u", lCnt);
   LogMsg("Latest recording date:          %u", lLstSaleDt);

   return 0;
}
*/
/*******************************************************************************
 *
 * Extract sale from update files
 *
 *******************************************************************************

extern char m_sDataSrc[];
int Ven_ExtrSales()
{
   char     *pTmp;
   char     acTmp[256], cFileCnt=1;
   char     acTmpFile[_MAX_PATH], acSaleOut[_MAX_PATH], acBakFolder[_MAX_PATH];
   char     acInFile[_MAX_PATH];
   struct   _finddata_t  sFileInfo;

   FILE     *fdOut;
   int      iCnt, iRet;
   long     lCnt, lHandle;

   LogMsg("VEN sale extract ...");

   // Get raw file name
   GetIniString(myCounty.acCntyCode, "UpdFile", "", acInFile, _MAX_PATH, acIniFile);

   // Move to backup folder after process
   sprintf(acBakFolder, "%s\\%s\\Bak", m_sDataSrc, myCounty.acCntyCode);
   if (_access(acBakFolder, 0))
      _mkdir(acBakFolder);

   // Find first Input file
   lHandle = _findfirst(acInFile, &sFileInfo);
   if (lHandle > 0)
   {

      pTmp = strrchr(acInFile, '\\');
      if (pTmp)
         *pTmp = 0;
      iRet = 0;
   } else
   {
      LogMsg("*** No new update available for processing: %s", acInFile);
      return 0;
   }

    // Create Output file - Ven_Sale.dat
   sprintf(acSaleOut, acSaleTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "Dat");
   LogMsg("Open output file %s", acSaleOut);
   fdOut = fopen(acSaleOut, "w");
   if (fdOut == NULL)
   {
      printf("Error creating GrGr output file: %s\n", acSaleOut);
      _findclose(lHandle);
      return -2;
   }

   iCnt = lCnt = 0;
   while (!iRet)
   {
      sprintf(acTmpFile, "%s\\%s", acInFile, sFileInfo.name);
      LogMsg("Loading %s", acTmpFile);

      // Parse input file
      iRet = Ven_ExtrSale(acTmpFile, fdOut);
      if (iRet <= 0)
         LogMsg("*** Skip %s", acTmpFile);
      else
      {
         lCnt += iRet;

         // Move input file to backup folder
         sprintf(acTmp, "%s\\%s", acBakFolder, sFileInfo.name);
         LogMsg("Move %s to %s", acTmpFile, acTmp);
         if (rename(acTmpFile, acTmp))
            LogMsg("*** Unable to move %s to %s", acTmpFile, acTmp);
      }

      iCnt++;
      // Find next file
      iRet = _findnext(lHandle, &sFileInfo);
   }

   // Close handle
   _findclose(lHandle);
   iRet = 0;

   // Close files
   if (fdOut)
      fclose(fdOut);

   LogMsg("Number of GRGR files have been processed: %d", iCnt);
   LogMsg("Total processed records  : %u", lCnt);
   printf("\nTotal processed records  : %u\n", lCnt);

   // Sort output
   if (lCnt > 0)
   {
          
   } else
      iRet = 0;

   LogMsg("Total output records after dedup: %u", lCnt);
   printf("\nTotal output records after dedup: %u\n", lCnt);

   return iRet;
}

/******************************* FixCumSale **********************************
 *
 * Fix specific case in sale file.
 *
 * Return 0 if successful, otherwise error.
 *
 *****************************************************************************

int Ven_FixCumSale(char *pInfile)
{
   char     acInbuf[1024], acOutFile[_MAX_PATH], acTmp[64], *pRec;
   char     acPrevSale[32], acPrevApn[32];
   long     lCnt=0, lDocNum=0, lPrice=0, iTmp;
   FILE     *fdOut;

   SCSAL_REC *pInRec  = (SCSAL_REC *)&acInbuf[0];

   if (_access(pInfile, 0))
   {
      LogMsg("***** FixCumSale(): Missing input file: %s", pInfile);
      return -1;
   }

   LogMsg("Fix Sale Price for %s", pInfile);

   // Open input file
   LogMsg("Open input sale file %s", pInfile);
   fdSale = fopen(pInfile, "r");
   if (fdSale == NULL)
   {
      LogMsg("***** Error opening sale file: %s\n", pInfile);
      return -2;
   }

   // Open output file
   strcpy(acOutFile, pInfile);
   pRec = strrchr(acOutFile, '.');
   strcpy(pRec, ".out");
   LogMsg("Create output sale file %s", acOutFile);
   fdOut = fopen(acOutFile, "w");
   if (fdOut == NULL)
   {
      LogMsg("***** Error creating output sale file: %s\n", acOutFile);
      return -3;
   }

   // Convert loop
   acPrevApn[0] = 0;
   acPrevApn[iApnLen] = 0;
   acPrevSale[0] = 0;
   acPrevSale[SALE_SIZ_SALEPRICE] = 0;
   while (!feof(fdSale))
   {
      if (!(pRec = fgets(acInbuf, 1024, fdSale)))
         break;

#ifdef _DEBUG
      //if (!memcmp(acInbuf, "0282630300", 9))
      //   iTmp = 0;
#endif
      if (pInRec->DocNum[8] == ' ')
      {
         sprintf(acTmp, "%.2s%.7s", &pInRec->DocDate[2], &pInRec->DocNum[0]);
         memcpy(pInRec->DocNum, acTmp, 9);
         lDocNum++;
      }

      if (!memcmp(acPrevApn, pInRec->Apn, iApnLen) && !memcmp(acPrevSale, pInRec->SalePrice, SALE_SIZ_SALEPRICE))
      {
         memset(pInRec->SalePrice, ' ', SALE_SIZ_SALEPRICE);
         lPrice++;
      } else if (memcmp(acPrevApn, pInRec->Apn, iApnLen))
      {
         memcpy(acPrevApn, pInRec->Apn, iApnLen);
         memcpy(acPrevSale, pInRec->SalePrice, SALE_SIZ_SALEPRICE);
      } else if (memcmp(acPrevSale, pInRec->SalePrice, SALE_SIZ_SALEPRICE) && pInRec->SalePrice[8] > ' ')
      {
         memcpy(acPrevSale, pInRec->SalePrice, SALE_SIZ_SALEPRICE);
      }

      fputs(acInbuf, fdOut);

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdSale)
      fclose(fdSale);
   if (fdOut)
      fclose(fdOut);

   if ((lDocNum+lPrice) > 0)
   {
      // Save input file
      strcpy(acInbuf, pInfile);
      pRec = strrchr(acInbuf, '.');
      strcpy(pRec, ".org");
      if (!_access(acInbuf, 0))
         DeleteFile(acInbuf);

      // Save input file
      iTmp = rename(pInfile, acInbuf);

      // Rename output file
      iTmp = rename(acOutFile, pInfile);
   } else
      LogMsg("Good sale file, no record drop!");

   LogMsgD("\nTotal input records:     %u\n", lCnt);
   LogMsg("    bad sale price:     %u", lDocNum);
   LogMsg("    bad doc num   :     %u", lPrice);

   return iTmp;
}


int Ven_FixSaleApn(LPCSTR pSaleFile, LPCSTR pBadList)
{
   char     acBuf[1024], acBadRec[512], acTmpFile[_MAX_PATH];
   char     *pTmp, *pKey;
   FILE     *fdIn, *fdOut, *fdBad;
   int      iRet, iNoApn, iApnMatch, iApnUnmatch, lCnt;
   bool     bEof;

   SCSAL_REC *pSale = (SCSAL_REC *)&acBuf[0];

   LogMsg("Fix sale APN in %s", pSaleFile);

   // Open input file
   if (!(fdIn = fopen(pSaleFile, "r")))
   {
      LogMsg("***** Error opening %s", pSaleFile);
      return -1;
   }

   // Open bad APN list
   if (!(fdBad = fopen(pBadList, "r")))
   {
      LogMsg("***** Error opening %s", pBadList);
      return -1;
   }

   // Create output temp file
   strcpy(acTmpFile, pSaleFile);
   pTmp = strrchr(acTmpFile, '.');
   strcpy(pTmp, ".out");
   if (!(fdOut = fopen(acTmpFile, "w")))
   {
      LogMsg("***** Error creating %s", acTmpFile);
      fclose(fdIn);
      return -2;
   }

   // Skip first record as needed
   pTmp = fgets(acBadRec, 512, fdBad);
   pTmp = fgets(acBadRec, 512, fdBad);

   // Initialize counters
   lCnt=iNoApn=iApnMatch=iApnUnmatch=0;
   bEof = false;
   pKey = pSale->Apn;

   // Loop through input file
   while (!feof(fdIn))
   {
      if (!(lCnt++ % 1000))
         printf("\r%u", lCnt);

      // Read input record
      if (!(pTmp = fgets(acBuf, 1024, fdIn)))
         break;

      // If no APN, skip
      if (pKey[iApnLen-1] == ' ')
      {
         iNoApn++;
         continue;
      }
#ifdef _DEBUG
      //if (!memcmp(pSale->APN, "0104015000000", 13))
      //   iRet = 0;
#endif
      // Match APN
      do
      {
         // Compare
         iRet = memcmp(pKey, &acBadRec[0], iApnLen);
         if (!iRet)
         {
            memcpy(pSale->OtherApn, pSale->Apn, iApnLen);
            memcpy(pSale->Apn, &acBadRec[iApnLen+1], iApnLen);
            iApnMatch++;
            break;
         } else
         { 
            // Get next roll record
            if (iRet == 1)
            {
               // Get another APN
               pTmp = fgets(acBadRec, 512, fdBad);
               // EOF ?
               if (!pTmp)
               {
                  bEof = true;
                  break;
               }
            } else
               iApnUnmatch++;
         } 
      } while (iRet > 0);
      
      // Output record
      fputs(acBuf, fdOut);
   }

   // Check for leftover
   while (!feof(fdIn))
   {
      // Read input record
      if (!(pTmp = fgets(acBuf, 1024, fdIn)))
         break;

      // If no APN, skip
      if (pSale->Apn[0] == ' ')
         iNoApn++;

      // Output record
      fputs(acBuf, fdOut);

      if (!(lCnt++ % 1000))
         printf("\r%u", lCnt);
   }

   if (fdIn)
      fclose(fdIn);
   if (fdOut)
      fclose(fdOut);
   if (fdBad)
      fclose(fdBad);

   // Sort output and dedup
   strcpy(acBuf, pSaleFile);
   pTmp = strrchr(acBuf, '.');
   strcpy(pTmp, ".fix");
   iRet = sortFile(acTmpFile, acBuf, "S(1,14,C,A,27,8,C,A,15,12,C,A,57,10,C,D) DUPO(1,34)");

   LogMsg("Number of records read: %d", lCnt);
   LogMsg("                output: %d", iRet);
   LogMsg("         APN unmatched: %d", iApnUnmatch);
   LogMsg("           APN matched: %d", iApnMatch);
   LogMsg("                No APN: %d\n", iNoApn);
   printf("\n");

   return iApnMatch;
}
*/

/**************************** Ven_UpdateDocNum ******************************
 *
 * Copy DocNum from GrGr to Sale. Do not copy DocType and NoneSale_Flg.
 *
 ****************************************************************************/

int Ven_UpdateDocNum(char *pSaleRec)
{
   static   char  acRec[1024], *pRec=NULL;
   int      iRet=1, iLoop;

   SCSAL_REC *pSale = (SCSAL_REC *)pSaleRec;
   SCSAL_REC *pGrgr = (SCSAL_REC *)&acRec[0];

   // Get first Char rec for first call
   if (!pRec)
      pRec = fgets(acRec, 1024, fdGrGr);

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "001060075", 9))
   //   iRet = 0;
#endif

   // First match APN
   do
   {
      if (!pRec)
      {
         fclose(fdGrGr);
         fdGrGr = NULL;
         return iRet;      // EOF
      }

      // Compare Apn
      iLoop = memcmp(pSale->Apn, pGrgr->Apn, iApnLen);
      if (iLoop > 0)
      {
         pRec = fgets(acRec, 1024, fdGrGr);
      }
   } while (iLoop > 0);

   // If not match, return
   if (iLoop)
      return iRet;

   // Second match DocDate
   do 
   {
      iLoop = memcmp(pSale->DocDate, pGrgr->DocDate, 8);
      if (iLoop > 0)
         pRec = fgets(acRec, 1024, fdGrGr);
   } while (iLoop > 0 && !memcmp(pSale->Apn, pGrgr->Apn, iApnLen));

   if (!iLoop && !memcmp(pSale->Apn, pGrgr->Apn, iApnLen) && isdigit(pGrgr->DocNum[0]))
   {
      if (pSale->DocNum[0] < '0')
      {
         memcpy(pSale->DocNum,  pGrgr->DocNum,  SALE_SIZ_DOCNUM);
         pSale->Spc_Flg = '3';      // Flag to specify DocNum is from GRGR
         iRet = 0;
      }
   }

   return iRet;
}

/*************************** Ven_UpdateSaleUsingGrGr ************************
 *
 * If sale has no DocNum and DocDate is the same as GrGr DocDate, 
 * copy GrGr DocNum to sale record for that parcel.
 *
 ****************************************************************************/

int Ven_UpdateSaleUsingGrGr()
{
   char     acSaleBuf[1024], acTmp[512], acTmpFile[_MAX_PATH];
   char     *pTmp;
   FILE     *fdOut;
   int      iRet, iUpdate=0, lCnt=0;

   SCSAL_REC *pSale = (SCSAL_REC *)&acSaleBuf[0];

   LogMsg0("Update sale Doc# using GrGr");

   // Open input file
   LogMsg("Open cum sale file: %s", acCSalFile);
   if (!(fdCSale = fopen(acCSalFile, "r")))
   {
      LogMsg("***** Error opening %s", acCSalFile);
      return -1;
   }

   sprintf(acTmp, acGrGrTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "Sls");
   LogMsg("Open GrGr file: %s", acTmp);
   if (!(fdGrGr = fopen(acTmp, "r")))
   {
      LogMsg("***** Error opening %s", acTmp);
      return -2;
   }

   // Open output file
   sprintf(acTmpFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "tmp");
   LogMsg("Open temp output file: %s", acTmpFile);
   if (!(fdOut = fopen(acTmpFile, "w")))
   {
      LogMsg("***** Error opening %s", acTmpFile);
      return -3;
   }

   while (!feof(fdCSale))
   {
      if (!(pTmp = fgets(acSaleBuf, 1024, fdCSale)))
         break;

      iRet = Ven_UpdateDocNum(acSaleBuf);
      if (!iRet)
         iUpdate++;
      fputs(acSaleBuf, fdOut);

      if (!(++lCnt % 1000))
         printf("\r%u - %d", lCnt, iUpdate);      
   }

   // Close files
   if (fdCSale)
      fclose(fdCSale);
   if (fdGrGr)
      fclose(fdGrGr);
   if (fdOut)
      fclose(fdOut);

   // Rename output if updating occurs
   if (iUpdate > 0)
   {
      sprintf(acTmp, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "Ugr");
      if (!_access(acTmp, 0))
         DeleteFile(acTmp);
      iRet = rename(acCSalFile, acTmp);
      iRet = rename(acTmpFile, acCSalFile);
   }

   LogMsg("Total output records:    %u", lCnt);
   LogMsg("      updated records:   %u", iUpdate);

   return 0;
}

/***************************** Ven_ParseTaxBase ******************************
 *
 * Input: secured_8102-01\P8102-01
 *
 *****************************************************************************/

//void Ven_ParseTaxBase(char *pOutbuf, char *pInbuf)
//{
//   char     acTmp[512], acApn[32], *pTmp;
//   int      iTmp;
//   int      iTaxRate, iPenalty, iTotalDue;
//   double   dTotalTax;
//   TAXBASE  *pOutRec = (TAXBASE *)pOutbuf;
//	VEN_SECTAX *pInRec= (VEN_SECTAX *)pInbuf;
//
//   // Clear output buffer
//   memset(pOutbuf, 0, sizeof(TAXBASE));
//
//   // APN - May need to convert to our format
//   pTmp = pInRec->APN;
//	memcpy(pOutRec->Assmnt_No, pTmp, TSIZ_APN);
//   iTmp = sprintf(acApn, "%.3s%.6s%.1s", pTmp, pTmp+4, pTmp+3);
//   memcpy(pOutRec->Apn, acApn, iTmp);
//   memcpy(pOutRec->BillNum, pInRec->BillNumber, TBS_BILL_NUMBER);
//   memcpy(pOutRec->OwnerInfo.Apn, acApn, iTmp);
//   memcpy(pOutRec->OwnerInfo.BillNum, pInRec->BillNumber, TBS_BILL_NUMBER);
//   sprintf(pOutRec->Upd_Date, "%d", lLastTaxFileDate);
//
//   // TRA
//   sprintf(pOutRec->TRA, "0%.5s", pInRec->TRA);
//
//   // Tax Year
//   iTmp = sprintf(pOutRec->TaxYear, "%d", lTaxYear);
//   strcpy(pOutRec->OwnerInfo.TaxYear, pOutRec->TaxYear);
//
//#ifdef _DEBUG
//   //if (!memcmp(pOutRec->Apn, "0020600400", 10))
//   //   iTmp = 0;
//#endif
//
//   // Check for Tax amount
//   iTotalDue = 0;
//   dTotalTax = (double)(atoin(pInRec->FirstInstallment, TBS_1ST_INST)/100.0); 
//   if (dTotalTax > 0.0)
//   {
//      iTmp = sprintf(pOutRec->TaxAmt1, "%.2f", dTotalTax);
//      memcpy(pOutRec->TaxAmt2, pOutRec->TaxAmt1, iTmp);
//      dTotalTax *= 2.0;
//      sprintf(pOutRec->TotalTaxAmt, "%.2f", dTotalTax);
//
//      // Due Date
//      InstDueDate(pOutRec->DueDate1, 1, lTaxYear);
//      InstDueDate(pOutRec->DueDate2, 2, lTaxYear);
//   }
//
//   // Penalty
//   iPenalty = atoin(pInRec->Penalty, TBS_PENALTY); 
//   if (iPenalty > 0)
//   {
//      if (ChkDueDate(1, pOutRec->DueDate1))
//         sprintf(pOutRec->PenAmt1, "%.2f", iPenalty/100.0);
//      if (ChkDueDate(2, pOutRec->DueDate2))
//         sprintf(pOutRec->PenAmt2, "%.2f", iPenalty/100.0);
//   }
//
//   // Tax rate
//   iTaxRate = atoin(pInRec->Type90TaxRate, TBS_TAX_RATE);
//   iTaxRate += atoin(pInRec->Type92TaxRate, TBS_TAX_RATE);
//   iTaxRate += atoin(pInRec->Type98TaxRate, TBS_TAX_RATE);
//   sprintf(pOutRec->TotalRate, "%.6f", (double)iTaxRate/1000000.0);
//
//   pOutRec->isDelq[0] = '0';
//	if (pInRec->RollType == '1')
//   {
//		pOutRec->isSecd[0] = '1';
//      pOutRec->BillType[0] = BILLTYPE_SECURED;
//   } else if (pInRec->RollType == '2')
//   {
//		pOutRec->isSecd[0] = '1';
//      pOutRec->BillType[0] = BILLTYPE_UTILITY;
//   } else
//   {
//		pOutRec->isSupp[0] = '0';
//      pOutRec->BillType[0] = BILLTYPE_SECURED_SUPPL;
//   }
//
//	// Owner Info
//   //memcpy(pOutRec->OwnerInfo.Name1, pInbuf+TB_O_NAME_1, TBS_NAME);
//   //memcpy(acTmp, pInbuf+TB_O_NAME_2, TBS_NAME);
//   //myTrim(acTmp, TBS_NAME);
//   //iTmp = strlen(acTmp);
//
//   //if (!memcmp(pInbuf+TB_O_NAME_2, "C/O", 3) || !memcmp(pInbuf+TB_O_NAME_2, "ATTN", 4))
//   //   memcpy(pOutRec->OwnerInfo.CareOf, acTmp, iTmp);
//   //else if (!memcmp(pInbuf+TB_O_NAME_2, "DBA", 3) )
//   //   memcpy(pOutRec->OwnerInfo.Dba, acTmp, iTmp);
//   //else if (iTmp > 2 && iTmp < 8)
//   //   strcat(pOutRec->OwnerInfo.Name1, acTmp);
//   //else if (iTmp > 1)
//   //   memcpy(pOutRec->OwnerInfo.Name2, acTmp, iTmp);
//
//   //// Mailing address
//   //memcpy(pOutRec->OwnerInfo.MailAdr[0], pInbuf+TB_M_ADDRESS, TBS_M_ADDRESS);
//   //myTrim(pOutRec->OwnerInfo.MailAdr[0], TBS_M_ADDRESS);
//   //memcpy(acTmp, pInbuf+TB_M_CITYST, TB_M_CITYST+TB_M_ZIP);
//   //iTmp = blankRem(acTmp);
//   //memcpy(pOutRec->OwnerInfo.MailAdr[1], acTmp, iTmp);
//
//	// Create detail record for general tax
//   TAXDETAIL sDetail;
//   memset(&sDetail, 0, sizeof(TAXDETAIL));
//	strcpy(sDetail.Assmnt_No, pOutRec->Assmnt_No);
//   strcpy(sDetail.Apn, pOutRec->Apn);
//   strcpy(sDetail.BillNum, pOutRec->BillNum);
//   strcpy(sDetail.TaxYear, pOutRec->TaxYear);
//	dTotalTax  = (double)atoin(pInRec->Type90Tax, TBS_TYPE90TAX);
//	dTotalTax += (double)atoin(pInRec->Type92Tax, TBS_TYPE92TAX);
//	dTotalTax += (double)atoin(pInRec->Type98Tax, TBS_TYPE98TAX);
//	sprintf(sDetail.TaxAmt, "%.2f", (dTotalTax*2)/100.0);
//	//iTotalTax  = atoin(pInRec->GeneralCounty, TBS_VALUE);
//	//iTotalTax += atoin(pInRec->Fire, TBS_VALUE);
//	//iTotalTax += atoin(pInRec->Flood, TBS_VALUE);
//	//iTotalTax += atoin(pInRec->Library, TBS_VALUE);
//	//iTotalTax += atoin(pInRec->City, TBS_VALUE);
//	//iTotalTax += atoin(pInRec->Schools, TBS_VALUE);
//	//sprintf(sDetail.TaxAmt, "%.2f", (double)iTotalTax/100.0);
//
//   strcpy(sDetail.TaxCode, "0001");
//   sDetail.TC_Flag[0] = '1';
//   Tax_CreateDetailCsv(acTmp, &sDetail);
//   fputs(acTmp, fdDetail);
//}

/***************************** Ven_ParseTaxDetail ****************************
 *
 * Return 0 if success
 *
 *****************************************************************************/

//void Ven_ParseTaxDetail(char *pInbuf)
//{
//   char     *pTmp, acTmp[512], acOutbuf[512];
//   int      iIdx, iTmp;
//   long     lTmp;
//   TAXDETAIL *pDetail = (TAXDETAIL *)acOutbuf;
//   TAXAGENCY *pResult, sAgency, *pAgency;
//	VEN_ASMNTS *pInRec= (VEN_ASMNTS *)pInbuf;
//
//   // Clear output buffer
//   memset(acOutbuf, 0, sizeof(TAXDETAIL));
//   pAgency = &sAgency;
//
//   // APN
//   pTmp = pInRec->APN;
//	memcpy(pDetail->Assmnt_No, pTmp, TSIZ_APN);
//   iTmp = sprintf(acTmp, "%.3s%.6s%.1s", pTmp, pTmp+4, pTmp+3);
//   memcpy(pDetail->Apn, acTmp, iTmp);
//
//	// BillNumber
//   memcpy(pDetail->BillNum, pInRec->BillNum, TBS_BILL_NUMBER);
//
//#ifdef _DEBUG
//   //if (!memcmp(pDetail->Apn, "0020600400", 10))
//   //   iTmp = 0;
//#endif
//
//   // Tax Year
//   sprintf(pDetail->TaxYear, "%d", lTaxYear);
//
//   for (iIdx=0; iIdx < TBS_MAX_ASMNT; iIdx++)
//   {
//		lTmp = atoin(pInRec->asAsmnts[iIdx].Amt, TBS_SPECIAL_ASMT);
//		if (!lTmp)
//			break;
//
//      memset(&sAgency, 0, sizeof(TAXAGENCY));
//
//      // Tax code
//      sprintf(pDetail->TaxCode, "%.2s%.2s", pInRec->asAsmnts[iIdx].Dist, pInRec->asAsmnts[iIdx].Zone);
//      strcpy(pAgency->Code, pDetail->TaxCode);
//      pResult = findTaxAgency(pAgency->Code, 0);
//      if (pResult)
//      {
//         strcpy(pAgency->Agency, pResult->Agency);
//         strcpy(pAgency->Phone, pResult->Phone);
//         pAgency->TC_Flag[0] = pResult->TC_Flag[0];
//         pDetail->TC_Flag[0] = pResult->TC_Flag[0];
//
//         // Tax Rate
//         if (pResult->TaxRate[0] > ' ')
//         {
//            strcpy(pAgency->TaxRate, pResult->TaxRate);
//            strcpy(pDetail->TaxRate, pResult->TaxRate);
//         }
//      } else
//      {
//         pAgency->Agency[0] = 0;
//         LogMsg("+++ Unknown TaxCode: %s Assmnt_No=%s", pDetail->TaxCode, pDetail->Assmnt_No);
//      }
//
//      // Tax amt
//      sprintf(pDetail->TaxAmt, "%.2f", (double)(lTmp*2)/100.0);
//
//      // Generate csv line and write to file
//      Tax_CreateDetailCsv(acTmp, pDetail);
//      fputs(acTmp, fdDetail);
//
//      // Forming Agency record
//      Tax_CreateAgencyCsv(acTmp, pAgency);
//      fputs(acTmp, fdAgency);
//   }
//}

/****************************** Ven_MergeTaxDue ******************************
 *
 * Update current parcels with tax due.  Tax correction may appear in this file.
 * Input: payment-status_8111-74\P8111-74
 *
 *****************************************************************************/

//int Ven_MergeTaxDue(char *pOutbuf)
//{
//   static   char  acRec[1024], *pRec=NULL;
//   VEN_DUE  *pVenDue = (VEN_DUE *)&acRec[0];
//   TAXBASE  *pTaxBase = (TAXBASE *)pOutbuf;
//
//   int      iLoop, iDueDate1, iDueDate2;
//   double   dDue1, dDue2, dTax1, dTax2, dTotalDue;
//
//   // Get first Sale rec for first call
//   if (!pRec)
//      pRec = fgets(acRec, 1024, fdDue);
//
//   do
//   {
//      // Compare Apn
//      iLoop = memcmp(pTaxBase->Assmnt_No, pVenDue->APN, iApnLen);
//      if (iLoop > 0)
//      {
//         if (bDebug)
//            LogMsg0("Skip Sec Tax Due rec  %.*s - %s", iApnLen, pVenDue->APN, pTaxBase->Assmnt_No);
//         pRec = fgets(acRec, 1024, fdDue);
//         if (!pRec)
//         {
//            fclose(fdDue);
//            fdDue = NULL;
//            return -1;      // EOF
//         }
//
//         lSaleSkip++;
//      }
//   } while (iLoop > 0);
//
//   // If not match, return
//   if (iLoop)
//      return 1;
//
//#ifdef _DEBUG
//   //if (!memcmp(pTaxBase->Apn, "0008079520", 9))
//   //   iLoop = 0;
//#endif
//
//   // Update tax amt & Due date
//   dDue1 = atofn(pVenDue->InstAmt1, TDS_INSTAMT);
//   dDue2 = atofn(pVenDue->InstAmt2, TDS_INSTAMT);
//   iDueDate1 = atoin(pVenDue->DueDate1, TDS_DUE_DATE);
//   iDueDate2 = atoin(pVenDue->DueDate2, TDS_DUE_DATE);
//
//#if _DEBUG
//   //if (dTotalFee > 0 || dTotalPen > 0)
//   //   pRec = NULL;
//#endif
//
//   // Check for correction
//   dTax1 = atofn(pTaxBase->TaxAmt1, TAX_AMT);
//   dTax2 = atofn(pTaxBase->TaxAmt2, TAX_AMT);
//
//   if (memcmp(pTaxBase->BillNum, pVenDue->BillNum, TAS_BILL_NUMBER))
//   {
//      LogMsg0("---> Bill correction for APN=%s old=%s, new=%.6s0", pTaxBase->Apn, pTaxBase->BillNum, pVenDue->BillNum);
//      lBillCorrection++;
//
//      // Update bill number
//      sprintf(pTaxBase->BillNum, "%.6s0", pVenDue->BillNum);
//      pTaxBase->BillType[0] = BILLTYPE_ROLL_CORRECTION;
//      sprintf(pTaxBase->TotalTaxAmt, "%.2f", dDue1+dDue2);
//
//      if (!dDue1 && !dDue2)
//      {
//         pTaxBase->PenAmt1[0] = 0;
//         pTaxBase->PenAmt2[0] = 0;
//         pTaxBase->TotalDue[0] = 0;
//      } else if (iDueDate1 > lLastDueUpdtDate)
//      {
//         pTaxBase->PenAmt1[0] = 0;
//         pTaxBase->PenAmt2[0] = 0;
//      } else if (iDueDate2 > lLastDueUpdtDate)
//         pTaxBase->PenAmt2[0] = 0;
//
//      sprintf(pTaxBase->TaxAmt1, "%.2f", dDue1);
//      if (iDueDate1 > 20000000)
//         memcpy(pTaxBase->DueDate1, pVenDue->DueDate1, TDS_DUE_DATE);
//
//      sprintf(pTaxBase->TaxAmt2, "%.2f", dDue2);
//      if (iDueDate2 > 20000000)
//         memcpy(pTaxBase->DueDate2, pVenDue->DueDate2, TDS_DUE_DATE);
//   }
//
//   if (pVenDue->InstCode[0] == '1')
//      dTotalDue = dDue2;
//   else if (pVenDue->InstCode[0] == '0')
//      dTotalDue = dDue2+dDue1;
//   else
//      dTotalDue = 0;
//
//   if (dTotalDue > 0.0)
//   {
//      if (ChkDueDate(1, pTaxBase->DueDate1))
//         dTotalDue += atof(pTaxBase->PenAmt1);
//      if (ChkDueDate(2, pTaxBase->DueDate2))
//         dTotalDue += atof(pTaxBase->PenAmt2);
//
//      sprintf(pTaxBase->TotalDue, "%.2f", dTotalDue);
//   }
//
//   // Get next sale record
//   pRec = fgets(acRec, 1024, fdDue);
//   if (!pRec)
//   {
//      fclose(fdDue);
//      fdDue = NULL;
//   }
//
//   return 0;
//}

/****************************** Ven_MergeTaxPaid *****************************
 *
 * Input: payment-status_8111-71\P8111-71
 *
 *****************************************************************************/

//int Ven_MergeTaxPaid(char *pOutbuf)
//{
//   static   char  acRec[256], *pRec=NULL;
//   VEN_PAID *pVenPaid = (VEN_PAID *)&acRec[0];
//   TAXBASE  *pTaxBase = (TAXBASE *)pOutbuf;
//
//   int      iLoop, iDueDate1, iDueDate2;
//   double   dPaid1, dPaid2, dPen1, dPen2, dTotalFee, dTotalDue;
//
//   // Get first Sale rec for first call
//   if (!pRec)
//   {
//      pRec = fgets(acRec, 256, fdPaid);
//   }
//
//   do
//   {
//      // Compare Apn
//      iLoop = memcmp(pTaxBase->Assmnt_No, pVenPaid->APN, iApnLen);
//      if (iLoop > 0)
//      {
//         if (bDebug)
//            LogMsg("*** Skip Sec Tax Paid rec  %.*s - %s", iApnLen, pVenPaid->APN, pTaxBase->Assmnt_No);
//         pRec = fgets(acRec, 256, fdPaid);
//         if (!pRec)
//         {
//            fclose(fdPaid);
//            fdPaid = NULL;
//            return -1;      // EOF
//         }
//
//         lSaleSkip++;
//      }
//   } while (iLoop > 0);
//
//   // If not match, return
//   if (iLoop)
//      return 1;
//
//   if (memcmp(pTaxBase->BillNum, pVenPaid->BillNum, TAS_BILL_NUMBER))
//      LogMsg0("*** Paid BillNum mismatched.  Base=%.6s, Paid=%.6s, Apn=%.10s", pTaxBase->BillNum, pVenPaid->BillNum, pVenPaid->APN);
//
//   //bool bRetry = false;
//   //PaidBillComp:
//   //if (memcmp(pTaxBase->BillNum, pVenPaid->BillNum, TAS_BILL_NUMBER))
//   //{
//   //   if (!bRetry)
//   //      LogMsg0("*** Invalid BillNum.  Base=%.6s, Paid=%.6s, Apn=%.10s", pTaxBase->BillNum, pVenPaid->BillNum, pVenPaid->APN);
//   //   else
//   //      return 1;
//
//   //   pRec = fgets(acRec, 256, fdPaid);
//   //   if (!pRec || bRetry)
//   //      return 1;
//   //   bRetry = true;
//   //   goto PaidBillComp;
//   //}
//
//   dPaid1=dPaid2=dPen1=dPen2=dTotalFee=dTotalDue = 0.0;
//#ifdef _DEBUG
//   //if (!memcmp(pTaxBase->Apn, "0463001950", 10))
//   //   iLoop = 0;
//#endif
//
//   iDueDate1 = atol(pTaxBase->DueDate1);
//   iDueDate2 = atol(pTaxBase->DueDate2);
//
//   // Calculate tax paid amt and penaty
//   pTaxBase->Inst1Status[0] = TAX_BSTAT_UNPAID;       // Both unpaid
//   pTaxBase->Inst2Status[0] = TAX_BSTAT_UNPAID;
//   if (pVenPaid->PaidCode1 > '0')
//   {
//      dPaid1 = (double)atoin(pVenPaid->InstAmt1, TAS_INSTAMT)/100.0;
//      if (pVenPaid->PaidCode1 == '2')
//      {
//         dPen1 = (double)atoin(pVenPaid->PenAmt1, TAS_PENALTY)/100.0;
//         sprintf(pTaxBase->PenAmt1, "%.2f", dPen1);
//      } 
//      pTaxBase->Inst1Status[0] = TAX_BSTAT_PAID;      // Inst1 paid
//   } 
//   if (pVenPaid->PaidCode2 > '0')
//   {
//      dPaid2 = (double)atoin(pVenPaid->InstAmt2, TAS_INSTAMT)/100.0;
//      if (pVenPaid->PaidCode2 == '2')
//      {
//         dPen2 = (double)atoin(pVenPaid->PenAmt2, TAS_PENALTY)/100.0;
//         sprintf(pTaxBase->PenAmt2, "%.2f", dPen2);
//      } 
//      pTaxBase->Inst2Status[0] = TAX_BSTAT_PAID;      // Both paid
//   } 
//
//   dTotalFee = (double)atoin(pVenPaid->Cost, TAS_COST)/100.0;
//
//#if _DEBUG
//   //if (dTotalFee > 0 || dTotalPen > 0)
//   //   pRec = NULL;
//#endif
//
//   // Update TaxBase record
//   if (dPaid1 > 0.0)
//   {
//      memcpy(pTaxBase->PaidDate1, pVenPaid->PaidDate1, 8);
//      sprintf(pTaxBase->PaidAmt1, "%.2f", dPaid1+dPen1);
//      if (!dPen1)
//         pTaxBase->PenAmt1[0] = 0;
//   } else
//   {
//      dTotalDue = atof(pTaxBase->TaxAmt1);
//      if (lLastTaxFileDate > iDueDate1)
//         dTotalDue += atof(pTaxBase->PenAmt1);
//      else
//         pTaxBase->PenAmt1[0] = 0;
//   }
//
//   if (dPaid2 > 0.0)
//   {
//      memcpy(pTaxBase->PaidDate2, pVenPaid->PaidDate2, 8);
//      sprintf(pTaxBase->PaidAmt2, "%.2f", dPaid2);
//      if (!dPen2)
//         pTaxBase->PenAmt2[0] = 0;
//   } else 
//   {
//      if (lLastTaxFileDate > iDueDate1)
//         dTotalDue += atof(pTaxBase->TaxAmt2);
//      if (lLastTaxFileDate > iDueDate2)
//         dTotalDue += atof(pTaxBase->PenAmt2);
//      else
//         pTaxBase->PenAmt2[0] = 0;
//   }
//
//   if (dTotalDue > 0.0)
//      sprintf(pTaxBase->TotalDue, "%.2f", dTotalDue);
//
//   if (dTotalFee > 0.0)
//      sprintf(pTaxBase->TotalFees, "%.2f", dTotalFee);
//
//   // Get next sale record
//   pRec = fgets(acRec, 256, fdPaid);
//   if (!pRec)
//   {
//      fclose(fdPaid);
//      fdPaid = NULL;
//   }
//
//   lSaleMatch++;
//   return 0;
//}

/***************************** Ven_MergeTaxRdmpt ****************************
 *
 * Input: redemption_8111-72\P8111-72
 *
 ****************************************************************************/

//int Ven_MergeTaxRdmpt(char *pOutbuf)
//{
//   static    char  acRec[1024], *pRec=NULL;
//
//   int       iLoop, iTmp;
//   double    dPaid, dDelqAmt, dTmp;
//   char      acDelqBuf[512];
//   VEN_RDMPT *pVenRdmpt = (VEN_RDMPT *)&acRec[0];
//   VEN_DFLT  *pVenDflt  = (VEN_DFLT *)&acRec[0];
//   TAXBASE   *pTaxBase  = (TAXBASE *)pOutbuf;
//   TAXDELQ   *pTaxDelq  = (TAXDELQ *)&acDelqBuf[0];
//
//   // Get first Sale rec for first call
//   if (!pRec)
//      pRec = fgets(acRec, 1024, fdRdmpt);
//
//   do
//   {
//      // Compare Apn
//      iLoop = memcmp(pTaxBase->Assmnt_No, pVenRdmpt->APN, iApnLen);
//      if (iLoop > 0)
//      {
//         if (bDebug)
//            LogMsg0("Skip Sec Tax Rdmpt Paid rec  %.*s - %s", iApnLen, pVenRdmpt->APN, pTaxBase->Assmnt_No);
//         pRec = fgets(acRec, 1024, fdRdmpt);
//         if (!pRec)
//         {
//            fclose(fdRdmpt);
//            fdRdmpt = NULL;
//            return -1;      // EOF
//         }
//      }
//   } while (iLoop > 0);
//
//   // If not match, return
//   if (iLoop)
//      return 1;
//
//#ifdef _DEBUG
//   //if (!memcmp(pVenRdmpt->APN, "0880253045", iApnLen))
//   //   iTmp = 0;
//#endif
//
//   memset(acDelqBuf, 0, sizeof(TAXDELQ));
//   strcpy(pTaxDelq->Apn, pTaxBase->Apn);
//   pTaxBase->isDelq[0] = '1';
//   pTaxDelq->isDelq[0] = '1';
//   if (strstr(pVenRdmpt->Remarks, "5 Y"))
//      pTaxDelq->DelqStatus[0] = TAX_STAT_ONPAYMENT;
//   else
//      pTaxDelq->DelqStatus[0] = TAX_STAT_UNPAID;
//   sprintf(pTaxDelq->Upd_Date, "%d", iDelqUpdtDate);
//
//   while (!memcmp(pTaxBase->Assmnt_No, pVenRdmpt->APN, iApnLen))
//   {
//      if (acRec[1] == '1')
//      {
//         if (pVenRdmpt->Date_Paid[0] > '0')
//         {
//            memcpy(pTaxDelq->Red_Date, pVenRdmpt->Date_Paid, 8);
//            dPaid = (double)atoin(pVenRdmpt->Amount_Paid, TRS_AMOUNT)/100.0;
//            sprintf(pTaxDelq->Red_Amt, "%.2f", dPaid);
//            pTaxBase->isDelq[0] = '0';
//            pTaxDelq->isDelq[0] = '0';
//            pTaxDelq->DelqStatus[0] = TAX_STAT_REDEEMED;
//         }
//      } else
//      {
//         if (pTaxBase->DelqYear[0] < '1')
//            memcpy(pTaxBase->DelqYear, pVenDflt->TaxYear, 4);
//         memcpy(pTaxDelq->TaxYear, pVenDflt->TaxYear, 4);
//         pTaxDelq->InstDel[0] = pVenDflt->Inst_Code;
//         dDelqAmt = (double)atoin(pVenDflt->Dflt_90, TRS_DFLT90)/100.0;
//         dDelqAmt += (double)atoin(pVenDflt->Dflt_92, TRS_DFLT92)/100.0;
//         dDelqAmt += (double)atoin(pVenDflt->asDflt93[0], TRS_DFLT93)/100.0;
//         for (iTmp = 1; iTmp < TRS_MAX_ENTRY; iTmp++)
//         {
//            dTmp = (double)atoin(pVenDflt->asDflt93[iTmp], TRS_DFLT93)/100.0;
//            if (dTmp == 0.0)
//               break;
//            dDelqAmt += dTmp;
//         }
//
//         if (dDelqAmt > 0.0)
//            sprintf(pTaxDelq->Def_Amt, "%.2f", dDelqAmt);
//
//         // Create delq record and output
//         Tax_CreateDelqCsv(acRec, (TAXDELQ *)&acDelqBuf);
//         fputs(acRec, fdDelq);
//      }
//
//      // Get next sale record
//      pRec = fgets(acRec, 1024, fdRdmpt);
//      if (!pRec)
//      {
//         fclose(fdRdmpt);
//         fdRdmpt = NULL;
//         break;
//      }
//   }
//
//   return 0;
//}

/****************************** Ven_Load_TaxBase ****************************
 *
 ****************************************************************************/

//int Ven_Load_TaxBase(bool bImport)
//{
//   char     *pTmp, acBuf[MAX_RECSIZE], acRec[MAX_RECSIZE], acPaidFile[_MAX_PATH], acTmpFile[_MAX_PATH];
//   char     acAgencyFile[_MAX_PATH], acBaseFile[_MAX_PATH], acItemsFile[_MAX_PATH], acInFile[_MAX_PATH];
//   char     acDelqFile[_MAX_PATH], acDueFile[_MAX_PATH], acRdmptFile[_MAX_PATH];
//   int      iRet;
//   long     lOut=0, lCnt=0;
//   FILE     *fdBase, *fdIn;
//   TAXBASE  *pTaxBase = (TAXBASE *)&acBuf[0];
//   TAXAGENCY sAgency;
//	VEN_SECTAX *pInRec= (VEN_SECTAX *)&acRec[0];
//
//   LogMsg0("Loading Current tax file");
//
//   GetIniString(myCounty.acCntyCode, "TaxFile", "", acInFile, _MAX_PATH, acIniFile);
//   GetIniString(myCounty.acCntyCode, "Redemption", "", acRdmptFile, _MAX_PATH, acIniFile);
//   GetIniString(myCounty.acCntyCode, "TaxPaid", "", acPaidFile, _MAX_PATH, acIniFile);
//   lLastTaxFileDate = getFileDate(acPaidFile);
//   GetIniString(myCounty.acCntyCode, "TaxDue", "", acDueFile, _MAX_PATH, acIniFile);
//   lLastDueUpdtDate = getFileDate(acDueFile);
//   if (lLastDueUpdtDate > lLastTaxFileDate)
//      lLastTaxFileDate = lLastDueUpdtDate;
//
//   // Only process if new tax file
//   iRet = isNewTaxFile(acDueFile, myCounty.acCntyCode);
//   if (iRet <= 0)
//   {
//      lLastTaxFileDate = 0;
//      return iRet;
//   }
//
//   NameTaxCsvFile(acBaseFile, myCounty.acCntyCode, "Base");
//   NameTaxCsvFile(acItemsFile, myCounty.acCntyCode, "Items");
//   NameTaxCsvFile(acAgencyFile, myCounty.acCntyCode, "Agency");
//   NameTaxCsvFile(acDelqFile, myCounty.acCntyCode, "Delq");
//   if (pTmp = strrchr(acBaseFile, '\\'))
//   {
//      *pTmp = 0;
//      if (_access(acBaseFile, 0))
//         _mkdir(acBaseFile);
//      *pTmp = '\\';
//   }
//
//   // Sort tax file
//   sprintf(acBuf, "S(6,10,C,A,550,1,C,A) F(TXT)");
//   sprintf(acTmpFile, "%s\\%s\\Tax_Secd.txt", acTmpPath, myCounty.acCntyCode);
//   iRet = sortFile(acInFile, acTmpFile, acBuf);
//
//   // Open input file
//   LogMsg("Open Current tax file %s", acTmpFile);
//   fdIn = fopen(acTmpFile, "r");
//   if (fdIn == NULL)
//   {
//      LogMsg("***** Error opening Current tax file: %s\n", acTmpFile);
//      return -2;
//   }  
//
//   // Sort paid file
//   sprintf(acBuf, "S(6,10,C,A) F(TXT)");
//   sprintf(acTmpFile, "%s\\%s\\Tax_Paid.txt", acTmpPath, myCounty.acCntyCode);
//   iRet = sortFile(acPaidFile, acTmpFile, acBuf);
//
//   // Open input paid file
//   LogMsg("Open tax paid file %s", acTmpFile);
//   fdPaid = fopen(acTmpFile, "r");
//   if (fdPaid == NULL)
//   {
//      LogMsg("***** Error opening Current tax paid file: %s\n", acTmpFile);
//      return -2;
//   }  
//
//   // Sort Redemption file
//   sprintf(acBuf, "S(3,10,C,A,1,2,C,A) F(TXT)");
//   sprintf(acTmpFile, "%s\\%s\\Tax_Rdmpt.txt", acTmpPath, myCounty.acCntyCode);
//   iRet = sortFile(acRdmptFile, acTmpFile, acBuf);
//
//   // Open input redemption file
//   LogMsg("Open tax redemption file %s", acTmpFile);
//   fdRdmpt = fopen(acTmpFile, "r");
//   if (fdRdmpt == NULL)
//   {
//      LogMsg("***** Error opening redemption file: %s\n", acTmpFile);
//      return -2;
//   }  
//   iDelqUpdtDate = getFileDate(acRdmptFile);
//
//   // Sort Tax Due file
//   sprintf(acBuf, "S(17,10,C,A) F(TXT)");
//   sprintf(acTmpFile, "%s\\%s\\Tax_Due.txt", acTmpPath, myCounty.acCntyCode);
//   iRet = sortFile(acDueFile, acTmpFile, acBuf);
//
//   // Open input due file
//   LogMsg("Open tax due file %s", acTmpFile);
//   fdDue = fopen(acTmpFile, "r");
//   if (fdDue == NULL)
//   {
//      LogMsg("***** Error opening tax due file: %s\n", acTmpFile);
//      return -2;
//   }  
//
//   // Open Detail file
//   LogMsg("Open Detail file %s", acItemsFile);
//   fdDetail = fopen(acItemsFile, "w");
//   if (fdDetail == NULL)
//   {
//      LogMsg("***** Error creating Detail file: %s\n", acItemsFile);
//      return -4;
//   }
//
//   // Open base file
//   LogMsg("Open Base file %s", acBaseFile);
//   fdBase = fopen(acBaseFile, "w");
//   if (fdBase == NULL)
//   {
//      LogMsg("***** Error creating base file: %s\n", acBaseFile);
//      return -4;
//   }
//
//   // Open Agency file
//   LogMsg("Open Agency file %s", acAgencyFile);
//   fdAgency = fopen(acAgencyFile, "w");
//   if (fdAgency == NULL)
//   {
//      LogMsg("***** Error creating Agency file: %s\n", acAgencyFile);
//      return -4;
//   }
//
//   // Open Delq file
//   LogMsg("Open Delq file %s", acDelqFile);
//   fdDelq = fopen(acDelqFile, "w");
//   if (fdDelq == NULL)
//   {
//      LogMsg("***** Error creating Delq file: %s\n", acDelqFile);
//      return -4;
//   }
//
//	// Create an agency record for general tax
//   memset(&sAgency, 0, sizeof(TAXAGENCY));
//   strcpy(sAgency.Code, "0001");
//	strcpy(sAgency.Agency, "GENERAL TAX TOTAL");
//   Tax_CreateAgencyCsv(acBuf, &sAgency);
//   fputs(acBuf, fdAgency);
//
//   // Merge loop 
//   while (!feof(fdIn))
//   {
//      pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdIn);
//      if (!pTmp)
//         break;
//
//		if (pInRec->RecType == '1')
//		{
//			// Create new tax base record
//			Ven_ParseTaxBase(acBuf, acRec);
//
//         // Update Tax bill
//         if (fdDue)
//            iRet = Ven_MergeTaxDue(acBuf);
//
//         // Update payment
//         if (fdPaid)
//            iRet = Ven_MergeTaxPaid(acBuf);
//
//         // Update Delq/Redemption
//         if (fdRdmpt)
//            iRet = Ven_MergeTaxRdmpt(acBuf);
//
//         // Create TaxBase record
//         Tax_CreateTaxBaseCsv(acRec, (TAXBASE *)&acBuf);
//         lOut++;
//         fputs(acRec, fdBase);
//
//         // Create Owner record
//         //Tax_CreateTaxOwnerCsv(acRec, &pTaxBase->OwnerInfo);
//         //fputs(acRec, fdOwner);
//      } else if (pInRec->RecType == '2')
//      {
//         Ven_ParseTaxDetail(acRec);
//      } else
//         LogMsg("---> Unknown record type %c [%.40s]", pInRec->RecType, acRec); 
//
//      if (!(++lCnt % 1000))
//         printf("\r%u", lCnt);
//   }
//
//   // Close files
//   if (fdIn)
//      fclose(fdIn);
//   if (fdPaid)
//      fclose(fdPaid);
//   if (fdBase)
//      fclose(fdBase);
//   if (fdDetail)
//      fclose(fdDetail);
//   if (fdAgency)
//      fclose(fdAgency);
//   if (fdRdmpt)
//      fclose(fdRdmpt);
//   if (fdDelq)
//      fclose(fdDelq);
//   if (fdDue)
//      fclose(fdDue);
//
//   LogMsg("Total records processed: %u", lCnt);
//   LogMsg("   Total output records: %u", lOut);
//   LogMsg("   Paid records matched: %u", lSaleMatch);
//   LogMsg("   Paid records skipped: %u", lSaleSkip);
//   LogMsg("   # of bill correction: %u\n", lBillCorrection);
//
//   // Import into SQL
//   if (bImport)
//   {
//      iRet = doTaxImport(myCounty.acCntyCode, TAX_BASE);
//      if (!iRet)
//      {
//         iRet = doTaxImport(myCounty.acCntyCode, TAX_DETAIL);
//         iRet = doTaxImport(myCounty.acCntyCode, TAX_DELINQUENT);
//
//         // Dedup Agency file before import
//         sprintf(acInFile, "%s\\%s\\%s_Agency.tmp", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
//
//         LogMsg("Dedup Agency file %s", acAgencyFile);
//         iRet = sortFile(acAgencyFile, acInFile, "S(#1,C,A) DUPOUT F(TXT)");
//         if (iRet > 0)
//         {
//            CopyFile(acInFile, acAgencyFile, false);
//            iRet = doTaxImport(myCounty.acCntyCode, TAX_AGENCY);
//         }
//      }
//   } else
//      iRet = 0;
//	return iRet;
//}

/************************** Ven_Load_TaxBaseCsv *****************************
 *
 * Load VenRpt-40 Secured Extended Role.txt & VenRpt-30 Secured Billing File.txt
 *
 ****************************************************************************/

void Ven_ParseTaxBaseCsv(char *pOutbuf, char *pInbuf)
{
   char     acApn[32], *pTmp;
   int      iTmp;
   double   dTotalTax, dTaxRate, dPenalty, dTotalDue;
   TAXBASE  *pOutRec = (TAXBASE *)pOutbuf;

   // Clear output buffer
   memset(pOutbuf, 0, sizeof(TAXBASE));
   iTokens = ParseStringIQ(pInbuf, cDelim, ER1_COLS, apTokens);

   // APN - May need to convert to our format
	pTmp = strcpy(pOutRec->Assmnt_No, apTokens[ER1_APN]);
   iTmp = sprintf(acApn, "%.3s%.6s%.1s", pTmp, pTmp+4, pTmp+3);
   memcpy(pOutRec->Apn, acApn, iTmp);
   strcpy(pOutRec->BillNum, apTokens[ER1_BILLNUM]);
   sprintf(pOutRec->Upd_Date, "%d", lLastTaxFileDate);

   // TRA
   sprintf(pOutRec->TRA, "0%s", apTokens[ER1_TRA]);

   // Tax Year
   iTmp = sprintf(pOutRec->TaxYear, "%d", lTaxYear);

#ifdef _DEBUG
   //if (!memcmp(pOutRec->Apn, "0020600400", 10))
   //   iTmp = 0;
#endif

   // Check for Tax amount
   dTotalDue = 0;
   dTotalTax = atof(apTokens[ER1_1ST_INST_AMT]);

#ifdef _DEBUG
   //dTotalDue = atof(apTokens[ER1_TYPE_90_TAX]);
   //dTotalDue += atof(apTokens[ER1_TYPE_92_TAX]);
   //dTotalDue += atof(apTokens[ER1_TYPE_98_TAX]);
   //if ((long)dTotalTax != (long)dTotalDue)
   //{
   //   iTmp = 0;
   //}
#endif

   if (dTotalTax > 0.0)
   {
      iTmp = sprintf(pOutRec->TaxAmt1, "%.2f", dTotalTax);
      memcpy(pOutRec->TaxAmt2, pOutRec->TaxAmt1, iTmp);
      dTotalTax *= 2.0;
      sprintf(pOutRec->TotalTaxAmt, "%.2f", dTotalTax);
      pOutRec->dTotalTax = dTotalTax;

      // Due Date
      InstDueDate(pOutRec->DueDate1, 1, lTaxYear);
      InstDueDate(pOutRec->DueDate2, 2, lTaxYear);
   }

   // Penalty
   dPenalty = atof(apTokens[ER1_PENALTY]); 
   if (dPenalty > 0.0)
   {
      if (ChkDueDate(1, pOutRec->DueDate1))
         sprintf(pOutRec->PenAmt1, "%.2f", dPenalty);
      if (ChkDueDate(2, pOutRec->DueDate2))
         sprintf(pOutRec->PenAmt2, "%.2f", dPenalty);
   }

   // Tax rate
   dTaxRate = atof(apTokens[ER1_TYPE_90_TAXRATE]);
   dTaxRate += atof(apTokens[ER1_TYPE_92_TAXRATE]);
   dTaxRate += atof(apTokens[ER1_TYPE_98_TAXRATE]);
   sprintf(pOutRec->TotalRate, "%.6f", dTaxRate);

   pOutRec->isDelq[0] = '0';
	pOutRec->isSecd[0] = '1';
	pOutRec->isSupp[0] = '0';
	if (*apTokens[ER1_ROLL_TYPE] == '1')
   {
      pOutRec->BillType[0] = BILLTYPE_SECURED;
   } else if (*apTokens[ER1_ROLL_TYPE] == '3')
   {
      pOutRec->BillType[0] = BILLTYPE_UTILITY;
   }

	//Remove 11/08/2024 - Create detail record for general tax
   //TAXDETAIL sDetail;
   //memset(&sDetail, 0, sizeof(TAXDETAIL));
   //strcpy(sDetail.Assmnt_No, pOutRec->Assmnt_No);
   //strcpy(sDetail.Apn, pOutRec->Apn);
   //strcpy(sDetail.BillNum, pOutRec->BillNum);
   //strcpy(sDetail.TaxYear, pOutRec->TaxYear);

   //dBaseAmt = atof(apTokens[ER1_TYPE_90_TAX]);
   //dBaseAmt += atof(apTokens[ER1_TYPE_92_TAX]);
   //sprintf(sDetail.TaxAmt, "%.2f", dBaseAmt*2);

   //strcpy(sDetail.TaxCode, "00000001");
   //sDetail.TC_Flag[0] = '1';
   //Tax_CreateDetailCsv(acTmp, &sDetail);
   //fputs(acTmp, fdDetail);
}

int Ven_MergeTaxPaidCsv(char *pOutbuf)
{
   static   char  acRec[1024], *pRec=NULL, *apItems[64];
   static   int   iItems;

   TAXBASE  *pTaxBase = (TAXBASE *)pOutbuf;

   int      iLoop;
   double   dPaid1, dPaid2, dPen1, dPen2, dTotalFee, dTotalDue;

   // Get first Sale rec for first call
   if (!pRec)
   {
      pRec = fgets(acRec, 1024, fdPaid);
      iItems = ParseStringIQ(acRec, cDelim, SB_COLS+1, apItems);
   }

   ReCheck:
   do
   {
      // Compare Apn
      iLoop = memcmp(pTaxBase->Assmnt_No, apItems[SB_APN], iApnLen);
      if (iLoop > 0)
      {
         if (bDebug)
            LogMsg("*** Skip Sec Tax Paid rec  %s - %s", apItems[SB_APN], pTaxBase->Assmnt_No);
         pRec = fgets(acRec, 1024, fdPaid);
         if (!pRec)
         {
            fclose(fdPaid);
            fdPaid = NULL;
            return -1;      // EOF
         }
         iItems = ParseStringIQ(acRec, cDelim, SB_COLS+1, apItems);

         lSaleSkip++;
      }
   } while (iLoop > 0);

   // If not match, return
   if (iLoop)
      return 1;

   if (strcmp(pTaxBase->BillNum, apItems[SB_BILLNUM]))
   {
      LogMsg0("---> Bill correction for APN=%s old=%s, new=%s", pTaxBase->Apn, pTaxBase->BillNum, apItems[SB_BILLNUM]);
      lBillCorrection++;

      // Check for correction
      double dTax1 = atof(apItems[SB_1ST_INST_AMT]);
      double dTax2 = atof(apItems[SB_2ND_INST_AMT]);

      pTaxBase->BillType[0] = BILLTYPE_ROLL_CORRECTION;
      sprintf(pTaxBase->TotalTaxAmt, "%.2f", dTax1+dTax2);
      strcpy(pTaxBase->BillNum, apItems[SB_BILLNUM]);

      sprintf(pTaxBase->TaxAmt1, "%.2f", dTax1);
      sprintf(pTaxBase->TaxAmt2, "%.2f", dTax2);
   }

   dPaid1=dPaid2=dPen1=dPen2=dTotalFee=dTotalDue = 0.0;
#ifdef _DEBUG
   //if (!memcmp(pTaxBase->Apn, "0463001950", 10))
   //   iLoop = 0;
#endif

   // Calculate tax paid amt and penaty
   pTaxBase->Inst1Status[0] = TAX_BSTAT_UNPAID;       // Both unpaid
   pTaxBase->Inst2Status[0] = TAX_BSTAT_UNPAID;
   if (*apItems[SB_1ST_INST_PAYCODE] > '0')
   {
      dPaid1 = atof(apItems[SB_1ST_INST_PAIDAMT]);
      if (*apItems[SB_1ST_INST_PAYCODE] == '2')
      {
         dPen1 = atof(apItems[SB_1ST_INST_PENAMT]);
         sprintf(pTaxBase->PenAmt1, "%.2f", dPen1);
      } else if (*apItems[SB_1ST_INST_PAYCODE] == '4')
      {
         // Debug
         dPen1 = atof(apItems[SB_1ST_INST_PENAMT]);
         sprintf(pTaxBase->PenAmt1, "%.2f", dPen1);
      }
      pTaxBase->Inst1Status[0] = TAX_BSTAT_PAID;      // Inst1 paid
   } 
   if (*apItems[SB_2ND_INST_PAYCODE] > '0')
   {
      dPaid2 = atof(apItems[SB_2ND_INST_PAIDAMT]);
      if (*apItems[SB_2ND_INST_PAYCODE] == '2')
      {
         dPen2 =  atof(apItems[SB_2ND_INST_PENAMT]);
         sprintf(pTaxBase->PenAmt2, "%.2f", dPen2);
      } else if (*apItems[SB_2ND_INST_PAYCODE] == '4')
      {
         // Debug
         dPen2 =  atof(apItems[SB_2ND_INST_PENAMT]);
         sprintf(pTaxBase->PenAmt2, "%.2f", dPen2);
      }
      pTaxBase->Inst2Status[0] = TAX_BSTAT_PAID;      // Both paid
   } 

#if _DEBUG
   //if (dTotalFee > 0 || dTotalPen > 0)
   //   pRec = NULL;
#endif

   // Update TaxBase record
   if (dPaid1 > 0.0)
   {
      strcpy(pTaxBase->PaidDate1, apItems[SB_1ST_INST_PAIDDATE]);
      sprintf(pTaxBase->PaidAmt1, "%.2f", dPaid1+dPen1);
   }
   if (dPaid2 > 0.0)
   {
      strcpy(pTaxBase->PaidDate2, apItems[SB_2ND_INST_PAIDDATE]);
      sprintf(pTaxBase->PaidAmt2, "%.2f", dPaid2);
   }

   dTotalDue = atof(apItems[SB_1ST_INST_BALAMT]) + atof(apItems[SB_2ND_INST_BALAMT]);
   sprintf(pTaxBase->TotalDue, "%.2f", dTotalDue);

   dTotalFee = atof(apItems[SB_COST_AMT]);
   if (dTotalFee > 0.0)
      sprintf(pTaxBase->TotalFees, "%.2f", dTotalFee);

   // Get next sale record
   pRec = fgets(acRec, 1024, fdPaid);
   if (!pRec)
   {
      fclose(fdPaid);
      fdPaid = NULL;
   } else
   {
      iItems = ParseStringIQ(acRec, cDelim, SB_COLS+1, apItems);
      if (!memcmp(pTaxBase->Assmnt_No, apItems[SB_APN], iApnLen) && strcmp(pTaxBase->BillNum, apItems[SB_BILLNUM]))
      {
         char sTmp[MAX_RECSIZE];
         Tax_CreateTaxBaseCsv(sTmp, (TAXBASE *)pOutbuf);
         fputs(sTmp, fdBase);
         goto ReCheck;
      }
   }

   lSaleMatch++;
   return 0;
}

// Parsing record type 2
void Ven_ParseTaxDetailCsv(char *pInbuf, char *pBaseRec)
{
   char     *pTmp, acTmp[1024], acOutbuf[512], *apItems[64];
   int      iIdx, iTmp, iItems;
   double   dTmp;
   TAXDETAIL *pDetail = (TAXDETAIL *)acOutbuf;
   TAXAGENCY *pResult, sAgency, *pAgency;
   TAXBASE   *pTaxBase = (TAXBASE *)pBaseRec;

   // Clear output buffer
   memset(acOutbuf, 0, sizeof(TAXDETAIL));
   pAgency = &sAgency;

   // Parse input string
   iItems = ParseStringIQ(pInbuf, cDelim, ER2_COLS, apItems);

   // APN
	pTmp = strcpy(pDetail->Assmnt_No, apItems[ER2_APN]);
   iTmp = sprintf(acTmp, "%.3s%.6s%.1s", pTmp, pTmp+4, pTmp+3);
   memcpy(pDetail->Apn, acTmp, iTmp);

	// BillNumber
   strcpy(pDetail->BillNum, pTaxBase->BillNum);

#ifdef _DEBUG
   //if (!memcmp(pDetail->Apn, "6320501750", 10))
   //   iTmp = 0;
#endif

   // Tax Year
   sprintf(pDetail->TaxYear, "%d", lTaxYear);

   for (iIdx=0; iIdx < ER2_COUNT; iIdx++)
   {
		dTmp = atof(apItems[ER2_ASST_AMT+iIdx*2]);
		if (!dTmp)
			break;

      memset(&sAgency, 0, sizeof(TAXAGENCY));

      // Tax code
      strcpy(pDetail->TaxCode, apItems[ER2_FUNDNUM+iIdx*2]);
      strcpy(pAgency->Code, pDetail->TaxCode);
      pResult = findTaxAgency(pAgency->Code, 0);
      if (pResult)
      {
         strcpy(pAgency->Agency, pResult->Agency);
         strcpy(pAgency->Phone, pResult->Phone);
         pAgency->TC_Flag[0] = pResult->TC_Flag[0];
         pDetail->TC_Flag[0] = pResult->TC_Flag[0];

         // Tax Rate
         if (pResult->TaxRate[0] > ' ')
         {
            strcpy(pAgency->TaxRate, pResult->TaxRate);
            strcpy(pDetail->TaxRate, pResult->TaxRate);
         } else
         {
            pAgency->TaxRate[0] = 0;
            pDetail->TaxRate[0] = 0;
         }
      } else
      {
         pAgency->Agency[0] = 0;
         LogMsg("+++ Unknown TaxCode: %s Assmnt_No=%s", pDetail->TaxCode, pDetail->Assmnt_No);
      }

      // Tax amt
      sprintf(pDetail->TaxAmt, "%.2f", dTmp);

      // Generate csv line and write to file
      Tax_CreateDetailCsv(acTmp, pDetail);
      fputs(acTmp, fdDetail);

      // Forming Agency record
      Tax_CreateAgencyCsv(acTmp, pAgency);
      fputs(acTmp, fdAgency);
   }
}

int Ven_Load_TaxBaseCsv(bool bImport)
{
   char     *pTmp, acBuf[MAX_RECSIZE], acRec[MAX_RECSIZE], acPaidFile[_MAX_PATH], acTmpFile[_MAX_PATH];
   char     acAgencyFile[_MAX_PATH], acBaseFile[_MAX_PATH], acItemsFile[_MAX_PATH], acInFile[_MAX_PATH];
   int      iRet;
   long     lOut=0, lCnt=0;
   FILE     *fdIn;
   TAXBASE  *pTaxBase = (TAXBASE *)&acBuf[0];
   TAXAGENCY sAgency;

   LogMsg0("Loading Current tax file");

   GetIniString(myCounty.acCntyCode, "TaxFile", "", acInFile, _MAX_PATH, acIniFile);
   GetIniString(myCounty.acCntyCode, "TaxPaid", "", acPaidFile, _MAX_PATH, acIniFile);
   lLastTaxFileDate = getFileDate(acPaidFile);

   // Only process if new tax file
   iRet = isNewTaxFile(acPaidFile, myCounty.acCntyCode);
   if (iRet <= 0)
   {
      lLastTaxFileDate = 0;
      return iRet;
   }

   NameTaxCsvFile(acBaseFile, myCounty.acCntyCode, "Base");
   NameTaxCsvFile(acItemsFile, myCounty.acCntyCode, "Items");
   NameTaxCsvFile(acAgencyFile, myCounty.acCntyCode, "Agency");
   if (pTmp = strrchr(acBaseFile, '\\'))
   {
      *pTmp = 0;
      if (_access(acBaseFile, 0))
         _mkdir(acBaseFile);
      *pTmp = '\\';
   }

   // Sort tax file - APN, RecType
   sprintf(acBuf, "S(#3,C,A,#1,C,A) F(TXT) DEL(9)");
   sprintf(acTmpFile, "%s\\%s\\Tax_Secd.txt", acTmpPath, myCounty.acCntyCode);
   iRet = sortFile(acInFile, acTmpFile, acBuf);
   if (iRet <= 0)
      return -1;

   // Open input file
   LogMsg("Open base tax file %s", acTmpFile);
   fdIn = fopen(acTmpFile, "r");
   if (fdIn == NULL)
   {
      LogMsg("***** Error opening Current tax file: %s\n", acTmpFile);
      return -2;
   }  

   // Sort paid file
   sprintf(acBuf, "S(#2,C,A) F(TXT) DEL(9)");
   sprintf(acTmpFile, "%s\\%s\\Tax_Paid.txt", acTmpPath, myCounty.acCntyCode);
   iRet = sortFile(acPaidFile, acTmpFile, acBuf);

   // Open input paid file
   LogMsg("Open tax paid file %s", acTmpFile);
   fdPaid = fopen(acTmpFile, "r");
   if (fdPaid == NULL)
   {
      LogMsg("***** Error opening Current tax paid file: %s\n", acTmpFile);
      return -2;
   }  

   // Open Detail file
   LogMsg("Open Detail file %s", acItemsFile);
   fdDetail = fopen(acItemsFile, "w");
   if (fdDetail == NULL)
   {
      LogMsg("***** Error creating Detail file: %s\n", acItemsFile);
      return -4;
   }

   // Open base file
   LogMsg("Open Base file %s", acBaseFile);
   fdBase = fopen(acBaseFile, "w");
   if (fdBase == NULL)
   {
      LogMsg("***** Error creating base file: %s\n", acBaseFile);
      return -4;
   }

   // Open Agency file
   LogMsg("Open Agency file %s", acAgencyFile);
   fdAgency = fopen(acAgencyFile, "w");
   if (fdAgency == NULL)
   {
      LogMsg("***** Error creating Agency file: %s\n", acAgencyFile);
      return -4;
   }

	// Create an agency record for general tax
   memset(&sAgency, 0, sizeof(TAXAGENCY));
 //  strcpy(sAgency.Code, "00000001");
	//strcpy(sAgency.Agency, "GENERAL TAX TOTAL");
 //  sAgency.TC_Flag[0] = '1';
 //  Tax_CreateAgencyCsv(acBuf, &sAgency);
 //  fputs(acBuf, fdAgency);

   // Merge loop 
   while (!feof(fdIn))
   {
      pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdIn);
      if (!pTmp)
         break;

		if (acRec[0] == '1')
		{
			// Create new tax base record
			Ven_ParseTaxBaseCsv(acBuf, acRec);

         // Update payment
         if (fdPaid)
            iRet = Ven_MergeTaxPaidCsv(acBuf);

         // Create TaxBase record
         Tax_CreateTaxBaseCsv(acRec, (TAXBASE *)&acBuf);
         lOut++;
         fputs(acRec, fdBase);
      } else if (acRec[0] == '2')
      {
         Ven_ParseTaxDetailCsv(acRec, acBuf);
      } else
         LogMsg("---> Unknown record type [%.40s]", acRec); 

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdIn)
      fclose(fdIn);
   if (fdPaid)
      fclose(fdPaid);
   if (fdBase)
      fclose(fdBase);
   if (fdDetail)
      fclose(fdDetail);
   if (fdAgency)
      fclose(fdAgency);

   LogMsg("Total records processed: %u", lCnt);
   LogMsg("         output records: %u", lOut);
   LogMsg("        Bill correction: %u", lBillCorrection);
   LogMsg("   Paid records matched: %u", lSaleMatch);
   LogMsg("   Paid records skipped: %u\n", lSaleSkip);
   
   // Import into SQL
   if (bImport)
   {
      iRet = doTaxImport(myCounty.acCntyCode, TAX_BASE);
      if (!iRet)
      {
         iRet = doTaxImport(myCounty.acCntyCode, TAX_DETAIL);

         // Dedup Agency file before import
         sprintf(acInFile, "%s\\%s\\%s_Agency.tmp", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);

         LogMsg("Dedup Agency file %s", acAgencyFile);
         iRet = sortFile(acAgencyFile, acInFile, "S(#1,C,A) DUPOUT F(TXT)");
         if (iRet > 0)
         {
            CopyFile(acInFile, acAgencyFile, false);
            iRet = doTaxImport(myCounty.acCntyCode, TAX_AGENCY);
         }
      }
   } else
      iRet = 0;
	return iRet;
}

/***************************** Ven_ParseTaxSupp ******************************
 *
 *****************************************************************************/

void Ven_ParseTaxSupp(char *pOutbuf, char *pInbuf)
{
   char     acTmp[512], acApn[32], *pTmp;
   int      iTmp;
   int      iTotalTax, iTaxRate, iPenalty, iTax1, iTax2;
   TAXBASE  *pOutRec = (TAXBASE *)pOutbuf;
	VEN_SUPP *pInRec= (VEN_SUPP *)pInbuf;

   // Ignore prior year record
   iTmp = atoin(pInRec->Tax_Year, 4);
   if (iTmp < lTaxYear)
      return;

   // Clear output buffer
   memset(pOutbuf, 0, sizeof(TAXBASE));

   // APN - May need to convert to our format
   pTmp = pInRec->APN;
	memcpy(pOutRec->Assmnt_No, pTmp, TSIZ_APN);
   iTmp = sprintf(acApn, "%.3s%.6s%.1s", pTmp, pTmp+4, pTmp+3);
   memcpy(pOutRec->Apn, acApn, iTmp);
   memcpy(pOutRec->BillNum, pInRec->BillNumber, TSS_STMT_NO_R);
   memcpy(pOutRec->OwnerInfo.Apn, acApn, iTmp);
   memcpy(pOutRec->OwnerInfo.BillNum, pInRec->BillNumber, TSS_STMT_NO_R);
   sprintf(pOutRec->Upd_Date, "%d", lLastTaxFileDate);

   // TRA
   sprintf(pOutRec->TRA, "0%.5s", pInRec->TRA);

   // Tax Year
   strcpy(pOutRec->TaxYear, pInRec->Tax_Year);
   strcpy(pOutRec->OwnerInfo.TaxYear, pOutRec->TaxYear);

   // Check for Tax amount
   iTax1 = atoin(pInRec->InstAmt1, TSS_INST_1); 
   if (iTax1 > 0)
   {
      iTmp = sprintf(pOutRec->TaxAmt1, "%.2f", iTax1/100.0);

      // Paid date
      iTmp = atoin(pInRec->PaidDate1, TSS_DT_PD_INST1);
      if (iTmp > 20000101)
         memcpy(pOutRec->PaidDate1, pInRec->PaidDate1, TSS_DT_PD_INST1);
      else
      {
         // Due Date
         iTmp = atoin(pInRec->DueDate1, TSS_DEL_DT_INST1);
         if (iTmp > 20000101)
            memcpy(pOutRec->DueDate1, pInRec->DueDate1, TSS_DEL_DT_INST1);
         else
            InstDueDate(pOutRec->DueDate1, 1, lTaxYear+1);
      }
   }

   iTax2 = atoin(pInRec->InstAmt2, TSS_INST_2); 
   if (iTax2 > 0)
   {
      iTmp = sprintf(pOutRec->TaxAmt2, "%.2f", iTax2/100.0);

      // Paid date
      iTmp = atoin(pInRec->PaidDate2, TSS_DT_PD_INST2);
      if (iTmp > 20000101)
         memcpy(pOutRec->PaidDate2, pInRec->PaidDate2, TSS_DT_PD_INST2);
      else
      {
         // Due Date
         iTmp = atoin(pInRec->DueDate2, TSS_DEL_DT_INST2);
         if (iTmp > 20000101)
            memcpy(pOutRec->DueDate2, pInRec->DueDate2, TSS_DEL_DT_INST2);
         else
            InstDueDate(pOutRec->DueDate2, 2, lTaxYear+1);
      }
   }

   // Penalty
   iPenalty =  atoin(pInRec->Pen90_Amt1, TSS_PEN90_1NST1); 
   iPenalty += atoin(pInRec->Pen92_Amt1, TSS_PEN90_1NST1); 
   if (iPenalty > 0)
      sprintf(pOutRec->PenAmt1, "%.2f", iPenalty/100.0);

   iPenalty =  atoin(pInRec->Pen90_Amt2, TSS_PEN90_1NST1); 
   iPenalty += atoin(pInRec->Pen92_Amt2, TSS_PEN90_1NST1); 
   if (iPenalty > 0)
      sprintf(pOutRec->PenAmt2, "%.2f", iPenalty/100.0);

   // Tax rate
   iTaxRate = atoin(pInRec->Type90TaxRate, TSS_RATE_90_R);
   iTaxRate += atoin(pInRec->Type92TaxRate, TSS_RATE_90_R);
   sprintf(pOutRec->TotalRate, "%.6f", iTaxRate/1000000.0);

   pOutRec->isDelq[0] = '0';
   pOutRec->isSupp[0] = '1';

	// Create detail record for general tax
   TAXDETAIL sDetail;
   memset(&sDetail, 0, sizeof(TAXDETAIL));
	strcpy(sDetail.Assmnt_No, pOutRec->Assmnt_No);
   strcpy(sDetail.Apn, pOutRec->Apn);
   strcpy(sDetail.BillNum, pOutRec->BillNum);
   strcpy(sDetail.TaxYear, pOutRec->TaxYear);
	iTotalTax  = atoin(pInRec->Type90Tax, TBS_VALUE);
	iTotalTax += atoin(pInRec->Type92Tax, TBS_VALUE);
	sprintf(sDetail.TaxAmt, "%.2f", (double)(iTotalTax*2)/100.0);
	strcpy(sDetail.TaxCode, "00-00");
   Tax_CreateDetailCsv(acTmp, &sDetail);
   fputs(acTmp, fdDetail);
}

/****************************** Ven_MergeSuppPaid *****************************
 *
 *****************************************************************************/

int Ven_MergeSuppPaid(char *pOutbuf)
{
   static   char  acRec[1024], *pRec=NULL;
   VEN_PSUP *pVenPaid = (VEN_PSUP *)&acRec[0];
   TAXBASE  *pTaxBase = (TAXBASE *)pOutbuf;

   int      iLoop, iDueDate1, iDueDate2, iPaidDate1, iPaidDate2;
   double   dPaid1, dPaid2, dPen1, dPen2, dTotalFee, dTotalDue;

   // Get first Sale rec for first call
   if (!pRec)
   {
      pRec = fgets(acRec, 1024, fdPaid);
   }

   do
   {
      // Compare Apn
      iLoop = memcmp(pTaxBase->Assmnt_No, pVenPaid->APN, iApnLen);
      if (iLoop > 0)
      {
         if (bDebug)
            LogMsg0("Skip Supp Paid rec  %.*s - %s", iApnLen, pVenPaid->APN, pTaxBase->Assmnt_No);
         pRec = fgets(acRec, 1024, fdPaid);
         if (!pRec)
         {
            fclose(fdPaid);
            fdPaid = NULL;
            return -1;      // EOF
         }

         lSaleSkip++;
      }
   } while (iLoop > 0);

   // If not match, return
   if (iLoop)
      return 1;

   if (memcmp(pTaxBase->BillNum, pVenPaid->BillNum, TSPS_BILLNUM))
      LogMsg("*** Invalid BillNum.  Base=%.6s, Paid=%.6s, Apn=%.10s", pTaxBase->BillNum, pVenPaid->BillNum, pVenPaid->APN);

   dPaid1=dPaid2=dPen1=dPen2=dTotalFee=dTotalDue = 0.0;
#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "009000144", 9))
   //   iRet = 0;
#endif

   iPaidDate1 = atol(pVenPaid->PaidDate1);
   iPaidDate2 = atol(pVenPaid->PaidDate1);
   iDueDate1 = atol(pTaxBase->DueDate1);
   iDueDate2 = atol(pTaxBase->DueDate2);

   // Calculate tax paid amt and penaty
   pTaxBase->Inst1Status[0] = TAX_BSTAT_UNPAID;       // Both unpaid
   pTaxBase->Inst2Status[0] = TAX_BSTAT_UNPAID;
   if (iPaidDate1 > 20000101)
   {
      dPaid1= (double)atoin(pVenPaid->InstAmt1, TSPS_INST_AMT1)/100.0;
      dPen1 = (double)atoin(pVenPaid->PenAmt1, TSPS_INST_PEN1)/100.0;
      pTaxBase->Inst1Status[0] = TAX_BSTAT_PAID;      // Inst1 paid
   }

   if (iPaidDate2 > 20000101)
   {
      dPaid2= (double)atoin(pVenPaid->InstAmt2, TSPS_INST_AMT1)/100.0;
      dPen2 = (double)atoin(pVenPaid->PenAmt2, TSPS_INST_PEN1)/100.0;
      pTaxBase->Inst2Status[0] = TAX_BSTAT_PAID;      // Both paid
   }

   dTotalFee = (double)atoin(pVenPaid->Cost, TAS_COST)/100.0;

#if _DEBUG
   //if (dTotalFee > 0 || dTotalPen > 0)
   //   pRec = NULL;
#endif

   // Update TaxBase record
   if (dPaid1 > 0.0)
   {
      memcpy(pTaxBase->PaidDate1, pVenPaid->PaidDate1, 8);
      sprintf(pTaxBase->PaidAmt1, "%.2f", dPaid1+dPen1);
      if (dPen1 > 0.0)
         sprintf(pTaxBase->PenAmt1, "%.2f", dPen1);
   } else
   {
      dTotalDue = atof(pTaxBase->TaxAmt1);
      if (lToday > iDueDate1)
         dTotalDue += atof(pTaxBase->PenAmt1);
   }

   if (dPaid2 > 0.0)
   {
      memcpy(pTaxBase->PaidDate2, pVenPaid->PaidDate2, 8);
      sprintf(pTaxBase->PaidAmt2, "%.2f", dPaid2+dPen2+dTotalFee);
      if (dPen2 > 0.0)
         sprintf(pTaxBase->PenAmt2, "%.2f", dPen2);
   } else 
   {
      if (lToday > iDueDate1)
         dTotalDue += atof(pTaxBase->TaxAmt2);
      if (lToday > iDueDate2)
         dTotalDue += atof(pTaxBase->PenAmt2);
   }

   if (dTotalDue > 0.0)
      sprintf(pTaxBase->TotalDue, "%.2f", dTotalDue);

   if (dTotalFee > 0.0)
      sprintf(pTaxBase->TotalFees, "%.2f", dTotalFee);

   // Get next sale record
   pRec = fgets(acRec, 1024, fdPaid);
   if (!pRec)
   {
      fclose(fdPaid);
      fdPaid = NULL;
   }

   lSaleMatch++;
   return 0;
}


/***************************** Ven_MergeSuppRdmpt ***************************
 *
 ****************************************************************************/

int Ven_MergeSuppRdmpt(char *pOutbuf)
{
   static    char  acRec[1024], *pRec=NULL;

   int       iLoop;
   char      acDelqBuf[512];
   VEN_RSUP  *pVenRdmpt = (VEN_RSUP *)&acRec[0];
   VEN_DFLT  *pVenDflt  = (VEN_DFLT *)&acRec[0];
   TAXBASE   *pTaxBase  = (TAXBASE *)pOutbuf;
   TAXDELQ   *pTaxDelq  = (TAXDELQ *)&acDelqBuf[0];

   // Get first Sale rec for first call
   if (!pRec)
      pRec = fgets(acRec, 1024, fdRdmpt);

   do
   {
      // Compare Apn
      iLoop = memcmp(pTaxBase->Assmnt_No, pVenRdmpt->APN, iApnLen);
      if (iLoop > 0)
      {
         if (bDebug)
            LogMsg0("Skip Supp Rdmpt rec  %.*s - %s", iApnLen, pVenRdmpt->APN, pTaxBase->Assmnt_No);
         pRec = fgets(acRec, 1024, fdRdmpt);
         if (!pRec)
         {
            fclose(fdRdmpt);
            fdRdmpt = NULL;
            return -1;      // EOF
         }
      }
   } while (iLoop > 0);

   // If not match, return
   if (iLoop)
      return 1;

#ifdef _DEBUG
   //if (!memcmp(pVenRdmpt->APN, "0419060084", iApnLen))
   //   iTmp = 0;
#endif

   memset(acDelqBuf, 0, sizeof(TAXDELQ));
   strcpy(pTaxDelq->Apn, pTaxBase->Apn);
   pTaxBase->isDelq[0] = '1';
   pTaxDelq->isDelq[0] = '1';
   pTaxDelq->DelqStatus[0] = TAX_STAT_UNPAID;
   sprintf(pTaxDelq->Upd_Date, "%d", iDelqUpdtDate);

   while (!memcmp(pTaxBase->Assmnt_No, pVenRdmpt->APN, iApnLen))
   {
      //if (acRec[1] == '1')
      //{
      //   if (pVenRdmpt->Date_Paid[0] > '0')
      //   {
      //      memcpy(pTaxDelq->Red_Date, pVenRdmpt->Date_Paid, 8);
      //      dPaid = (double)atoin(pVenRdmpt->Amount_Paid, TRS_AMOUNT)/100.0;
      //      sprintf(pTaxDelq->Red_Amt, "%.2f", dPaid);
      //      pTaxBase->isDelq[0] = '0';
      //      pTaxDelq->isDelq[0] = '0';
      //      pTaxDelq->DelqStatus[0] = TAX_STAT_REDEEMED;
      //   }
      //} else
      //{
      //   if (pTaxBase->DelqYear[0] < '1')
      //      memcpy(pTaxBase->DelqYear, pVenDflt->TaxYear, 4);
      //   memcpy(pTaxDelq->TaxYear, pVenDflt->TaxYear, 4);
      //   pTaxDelq->InstDel[0] = pVenDflt->Inst_Code;
      //   dDelqAmt = (double)atoin(pVenDflt->Dflt_90, TRS_DFLT90)/100.0;
      //   dDelqAmt += (double)atoin(pVenDflt->Dflt_92, TRS_DFLT92)/100.0;
      //   dDelqAmt += (double)atoin(pVenDflt->asDflt93[0], TRS_DFLT93)/100.0;
      //   for (iTmp = 1; iTmp < TRS_MAX_ENTRY; iTmp++)
      //   {
      //      dTmp = (double)atoin(pVenDflt->asDflt93[iTmp], TRS_DFLT93)/100.0;
      //      if (dTmp == 0.0)
      //         break;
      //      dDelqAmt += dTmp;
      //   }

      //   if (dDelqAmt > 0.0)
      //      sprintf(pTaxDelq->Def_Amt, "%.2f", dDelqAmt);

      //   // Create delq record and output
      //   Tax_CreateDelqCsv(acRec, (TAXDELQ *)&acDelqBuf);
      //   fputs(acRec, fdDelq);
      //}

      // Get next sale record
      pRec = fgets(acRec, 1024, fdRdmpt);
      if (!pRec)
      {
         fclose(fdRdmpt);
         fdRdmpt = NULL;
         break;
      }
   }

   return 0;
}

/****************************** Ven_Load_TaxSupp ****************************
 *
 * Loading supplement file and append output to TaxBase & TaxDelq
 *
 ****************************************************************************/

int Ven_Load_TaxSupp(bool bImport)
{
   char     *pTmp, acBuf[MAX_RECSIZE], acRec[MAX_RECSIZE], acPaidFile[_MAX_PATH], acTmpFile[_MAX_PATH];
   char     acAgencyFile[_MAX_PATH], acBaseFile[_MAX_PATH], acItemsFile[_MAX_PATH], acInFile[_MAX_PATH];
   char     acDelqFile[_MAX_PATH], acRdmptFile[_MAX_PATH];
   int      iRet;
   long     lOut=0, lCnt=0;
   FILE     *fdBase, *fdIn;
   TAXBASE  *pTaxBase = (TAXBASE *)&acBuf[0];
   TAXAGENCY sAgency;
	VEN_SUPP *pInRec= (VEN_SUPP *)&acRec[0];

   LogMsg("Loading Current tax supplement file");

   GetIniString(myCounty.acCntyCode, "SuppTax", "", acInFile, _MAX_PATH, acIniFile);
   NameTaxCsvFile(acBaseFile, myCounty.acCntyCode, "Base");
   NameTaxCsvFile(acItemsFile, myCounty.acCntyCode, "Items");
   NameTaxCsvFile(acAgencyFile, myCounty.acCntyCode, "Agency");
   NameTaxCsvFile(acDelqFile, myCounty.acCntyCode, "Delq");

   if (pTmp = strrchr(acBaseFile, '\\'))
   {
      *pTmp = 0;
      if (_access(acBaseFile, 0))
         _mkdir(acBaseFile);
      *pTmp = '\\';
   }

   // Sort tax file
   sprintf(acBuf, "S(%d,10,C,A,%d,4,C,A) F(TXT)", TS_APN, TS_TAX_YEAR);
   sprintf(acTmpFile, "%s\\%s\\Tax_Supp.txt", acTmpPath, myCounty.acCntyCode);
   iRet = sortFile(acInFile, acTmpFile, acBuf);

   // Open input file
   LogMsg("Open Current tax file %s", acTmpFile);
   fdIn = fopen(acTmpFile, "r");
   if (fdIn == NULL)
   {
      LogMsg("***** Error opening Current tax file: %s\n", acTmpFile);
      return -2;
   }  

   // Sort paid file
   sprintf(acBuf, "S(%d,10,C,A) F(TXT)", TSP_APN);
   sprintf(acTmpFile, "%s\\%s\\Tax_SuppPaid.txt", acTmpPath, myCounty.acCntyCode);
   iRet = sortFile(acPaidFile, acTmpFile, acBuf);

   // Open input paid file
   LogMsg("Open tax paid file %s", acTmpFile);
   fdPaid = fopen(acTmpFile, "r");
   if (fdPaid == NULL)
   {
      LogMsg("***** Error opening Current tax paid file: %s\n", acTmpFile);
      return -2;
   }  
   lLastTaxFileDate = getFileDate(acPaidFile);

   // Sort Redemption file
   sprintf(acBuf, "S(%d,10,C,A,%d,%d,C,A) F(TXT)", TSR_APN, TSR_BILLNUM, TSRS_BILLNUM);
   sprintf(acTmpFile, "%s\\%s\\Tax_SuppRed.txt", acTmpPath, myCounty.acCntyCode);
   iRet = sortFile(acRdmptFile, acTmpFile, acBuf);

   // Open input redemption file
   LogMsg("Open tax redemption file %s", acTmpFile);
   fdRdmpt = fopen(acTmpFile, "r");
   if (fdRdmpt == NULL)
   {
      LogMsg("***** Error opening redemption file: %s\n", acTmpFile);
      return -2;
   }  
   iDelqUpdtDate = getFileDate(acRdmptFile);

   // Open Detail file
   LogMsg("Open Detail file %s", acItemsFile);
   fdDetail = fopen(acItemsFile, "w");
   if (fdDetail == NULL)
   {
      LogMsg("***** Error creating Detail file: %s\n", acItemsFile);
      return -4;
   }

   // Open base file
   LogMsg("Open Base file %s", acBaseFile);
   fdBase = fopen(acBaseFile, "w");
   if (fdBase == NULL)
   {
      LogMsg("***** Error creating base file: %s\n", acBaseFile);
      return -4;
   }

   // Open Agency file
   LogMsg("Open Agency file %s", acAgencyFile);
   fdAgency = fopen(acAgencyFile, "w");
   if (fdAgency == NULL)
   {
      LogMsg("***** Error creating Agency file: %s\n", acAgencyFile);
      return -4;
   }

   // Open Delq file
   LogMsg("Open Delq file %s", acDelqFile);
   fdDelq = fopen(acDelqFile, "w");
   if (fdDelq == NULL)
   {
      LogMsg("***** Error creating Delq file: %s\n", acDelqFile);
      return -4;
   }

	// Create an agency record for general tax
   memset(&sAgency, 0, sizeof(TAXAGENCY));
   strcpy(sAgency.Code, "0000");
	strcpy(sAgency.Agency, "GENERAL TAX TOTAL");
   Tax_CreateAgencyCsv(acBuf, &sAgency);
   fputs(acBuf, fdAgency);

   // Merge loop 
   while (!feof(fdIn))
   {
      pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdIn);
      if (!pTmp)
         break;

		if (pInRec->Sec_Unsec[0] == 'S')
		{
			// Create new tax base record
			Ven_ParseTaxSupp(acBuf, acRec);

         // Update payment
         if (fdPaid)
            iRet = Ven_MergeSuppPaid(acBuf);

         // Update Delq/Redemption
         if (fdRdmpt)
            iRet = Ven_MergeSuppRdmpt(acBuf);

         // Create TaxBase record
         Tax_CreateTaxBaseCsv(acRec, (TAXBASE *)&acBuf);
         lOut++;
         fputs(acRec, fdBase);
      } else
         LogMsg("---> Ignore unsecured record type %c [%.40s]", pInRec->Sec_Unsec, acRec); 

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdIn)
      fclose(fdIn);
   if (fdPaid)
      fclose(fdPaid);
   if (fdBase)
      fclose(fdBase);
   if (fdDetail)
      fclose(fdDetail);
   if (fdAgency)
      fclose(fdAgency);
   if (fdRdmpt)
      fclose(fdRdmpt);
   if (fdDelq)
      fclose(fdDelq);

   LogMsg("Total records processed: %u", lCnt);
   LogMsg("   Total output records: %u", lOut);
   LogMsg("   Paid records matched: %u", lSaleMatch);
   LogMsg("   Paid records skipped: %u", lSaleSkip);

   // Import into SQL
   //if (bImport)
   //{
   //   iRet = doTaxImport(myCounty.acCntyCode, TAX_BASE);
   //   if (!iRet)
   //   {
   //      iRet = doTaxImport(myCounty.acCntyCode, TAX_DETAIL);
   //      iRet = doTaxImport(myCounty.acCntyCode, TAX_DELINQUENT);

   //      // Dedup Agency file before import
   //      sprintf(acInFile, "%s\\%s\\%s_Agency.tmp", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);

   //      LogMsg("Dedup Agency file %s", acAgencyFile);
   //      iRet = sortFile(acAgencyFile, acInFile, "S(#1,C,A) DUPOUT F(TXT)");
   //      if (iRet > 0)
   //      {
   //         CopyFile(acInFile, acAgencyFile, false);
   //         iRet = doTaxImport(myCounty.acCntyCode, TAX_AGENCY);
   //      }
   //   }
   //} else
   //   iRet = 0;
	return iRet;
}

/*************************** Ven_Load_TaxSuppCsv ****************************
 *
 * Loading supplement file and append output to TaxBase
 * Input= "VenRpt-50 Secured Supplemental Billing File.txt"
 *
 ****************************************************************************/

int Ven_ParseTaxSuppCsv(char *pOutbuf, char *pInbuf)
{
   char     acApn[32], *pTmp;
   int      iTmp;
   double   dTotalTax, dTotalDue, dTax1, dTax2;
   TAXBASE  *pOutRec = (TAXBASE *)pOutbuf;

   // Clear output buffer
   memset(pOutbuf, 0, sizeof(TAXBASE));
   iTokens = ParseStringIQ(pInbuf, cDelim, SSB_COLS+1, apTokens);
   if (iTokens < SSB_COLS)
   {
      LogMsg("*** Bad supp record (%d) %.40s", pInbuf);
      return -1;
   }

   // APN - May need to convert to our format
	pTmp = strcpy(pOutRec->Assmnt_No, apTokens[SSB_APN]);
   iTmp = sprintf(acApn, "%.3s%.6s%.1s", pTmp, pTmp+4, pTmp+3);
   memcpy(pOutRec->Apn, acApn, iTmp);
   strcpy(pOutRec->BillNum, apTokens[SSB_BILLNUM]);
   sprintf(pOutRec->Upd_Date, "%d", lLastTaxFileDate);

   // TRA
   sprintf(pOutRec->TRA, "0%s", apTokens[SSB_TRA]);

   // Tax Year
   //iTmp = sprintf(pOutRec->TaxYear, "%d", lTaxYear);
   strcpy(pOutRec->TaxYear, apTokens[SSB_TAXYEAR]);
	pOutRec->isSupp[0] = '1';
   pOutRec->isSecd[0] = '0';
   pOutRec->isDelq[0] = '0';
   pOutRec->BillType[0] = BILLTYPE_SECURED_SUPPL;

#ifdef _DEBUG
   //if (!memcmp(pOutRec->Apn, "0020600400", 10))
   //   iTmp = 0;
#endif

   // Check for Tax amount
   dTotalDue = atof(apTokens[SSB_1ST_INST_BALAMT]) + atof(apTokens[SSB_2ND_INST_BALAMT]);
   iTmp = sprintf(pOutRec->TotalDue, "%.2f", dTotalDue);

   dTax1 = atof(apTokens[SSB_1ST_INST_AMT]);
   dTax2 = atof(apTokens[SSB_2ND_INST_AMT]);
   dTotalTax = dTax1+dTax2;
   if (dTax1 > 0.0)
   {
      iTmp = sprintf(pOutRec->TaxAmt1, "%.2f", dTax1);

      // Due Date
      //InstDueDate(pOutRec->DueDate1, 1, lTaxYear);
      strcpy(pOutRec->DueDate1, apTokens[SSB_1ST_INST_DUEDATE]);
      pOutRec->Inst1Status[0] = TAX_BSTAT_UNPAID;      // Set default
   }
   if (dTax2 > 0.0)
   {
      iTmp = sprintf(pOutRec->TaxAmt2, "%.2f", dTax2);

      // Due Date
      //InstDueDate(pOutRec->DueDate2, 2, lTaxYear);
      strcpy(pOutRec->DueDate2, apTokens[SSB_2ND_INST_DUEDATE]);
      pOutRec->Inst2Status[0] = TAX_BSTAT_UNPAID;      // Set default
   }
   sprintf(pOutRec->TotalTaxAmt, "%.2f", dTotalTax);

   // Penalty
   dTax1 = atof(apTokens[SSB_1ST_INST_PENAMT]); 
   if (dTax1 > 0.0)
      sprintf(pOutRec->PenAmt1, "%.2f", dTax1);

   dTax2 = atof(apTokens[SSB_2ND_INST_PENAMT]); 
   if (dTax2 > 0.0)
      sprintf(pOutRec->PenAmt2, "%.2f", dTax2);

   // Paid
   dTax1 = atof(apTokens[SSB_1ST_INST_PAIDAMT]); 
   if (dTax1 > 0.0)
   {
      sprintf(pOutRec->PaidAmt1, "%.2f", dTax1);
      strcpy(pOutRec->PaidDate1, apTokens[SSB_1ST_INST_PAIDDATE]);
      pOutRec->Inst1Status[0] = TAX_BSTAT_PAID;      // Inst1 paid
   }
   dTax2 = atof(apTokens[SSB_2ND_INST_PAIDAMT]); 
   if (dTax2 > 0.0)
   {
      sprintf(pOutRec->PaidAmt2, "%.2f", dTax2);
      strcpy(pOutRec->PaidDate2, myTrim(apTokens[SSB_2ND_INST_PAIDDATE]));
      pOutRec->Inst2Status[0] = TAX_BSTAT_PAID;      // Inst1 paid
   }

   return 0;
}

int Ven_Load_TaxSuppCsv(bool bImport)
{
   char     *pTmp, acBuf[MAX_RECSIZE], acRec[MAX_RECSIZE], acBaseFile[_MAX_PATH], acInFile[_MAX_PATH];
   int      iRet;
   long     lOut=0, lCnt=0;
   FILE     *fdBase, *fdIn;
   TAXBASE  *pTaxBase = (TAXBASE *)&acBuf[0];

   LogMsg("Loading tax supplement file");

   GetIniString(myCounty.acCntyCode, "SuppTax", "", acInFile, _MAX_PATH, acIniFile);
   NameTaxCsvFile(acBaseFile, myCounty.acCntyCode, "Base");

   // Open input file
   LogMsg("Open supplemental tax file %s", acInFile);
   fdIn = fopen(acInFile, "r");
   if (fdIn == NULL)
   {
      LogMsg("***** Error opening supplemental tax file: %s\n", acInFile);
      return -2;
   }  

   // Open base file
   LogMsg("Open Base file %s", acBaseFile);
   fdBase = fopen(acBaseFile, "a+");
   if (fdBase == NULL)
   {
      LogMsg("***** Error creating base file: %s\n", acBaseFile);
      return -4;
   }

   // Merge loop 
   while (!feof(fdIn))
   {
      pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdIn);
      if (!pTmp)
         break;

		// Create new tax base record
		iRet = Ven_ParseTaxSuppCsv(acBuf, acRec);
      if (!iRet)
      {
         // Create TaxBase record
         Tax_CreateTaxBaseCsv(acRec, (TAXBASE *)&acBuf);
         lOut++;
         fputs(acRec, fdBase);
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdIn)
      fclose(fdIn);
   if (fdBase)
      fclose(fdBase);

   LogMsg("Total records processed: %u", lCnt);
   LogMsg("   Total output records: %u", lOut);
   LogMsg("   Paid records matched: %u", lSaleMatch);
   LogMsg("   Paid records skipped: %u\n", lSaleSkip);

   // Import into SQL
   if (bImport)
   {
      iRet = doTaxImport(myCounty.acCntyCode, TAX_BASE);
      if (!iRet)
      {
         iRet = doTaxImport(myCounty.acCntyCode, TAX_DETAIL);

         // Dedup Agency file before import
         sprintf(acInFile, "%s\\%s\\%s_Agency.tmp", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);

         char acAgencyFile[_MAX_PATH];
         NameTaxCsvFile(acAgencyFile, myCounty.acCntyCode, "Agency");
         LogMsg("Dedup Agency file %s", acAgencyFile);
         iRet = sortFile(acAgencyFile, acInFile, "S(#1,C,A) DUPOUT F(TXT)");
         if (iRet > 0)
         {
            CopyFile(acInFile, acAgencyFile, false);
            iRet = doTaxImport(myCounty.acCntyCode, TAX_AGENCY);
         }
      }
   } else
      iRet = 0;

	return iRet;
}

/****************************** Ven_Load_TaxDelqCsv *************************
 *
 * Load "VenRpt-10 Redemption-Supplemental Redemption Billing File.txt"
 *
 ****************************************************************************/

int Ven_ParseTaxDelq(char *pOutbuf, char *pInbuf)
{
   int      iTmp;
   double   dDueAmt, dPaidAmt, dTaxAmt;
   char     *pTmp;
   TAXDELQ  *pOutRec = (TAXDELQ *)pOutbuf;

   // Parse input tax data
   iTokens = ParseStringIQ(pInbuf, cDelim, MAX_FLD_TOKEN, apTokens);
   if (iTokens < RB1_COLS)
   {
      LogMsg("***** Error: bad Delq record for: %.50s (#tokens=%d)", pInbuf, iTokens);
      return -1;
   }

   // APN
	pTmp = strcpy(pOutRec->Assmnt_No, apTokens[RB1_APN]);
   iTmp = sprintf(pOutRec->Apn, "%.3s%.6s%.1s", pTmp, pTmp+4, pTmp+3);
   //strcpy(pOutRec->BillNum, apTokens[SSB_BILLNUM]);

#ifdef _DEBUG
   //if (!memcmp(pOutRec->Apn, "2105001054", 10))
   //   iTmp = 0;
#endif

   // Updated date
   sprintf(pOutRec->Upd_Date, "%d", lLastTaxFileDate);

   // Default amount
   dDueAmt = atof(apTokens[RB1_TAX_DUE]);
   if (dDueAmt > 0.0)
   {
      sprintf(pOutRec->Def_Amt, "%.2f", dDueAmt);
      pOutRec->isDelq[0] = '1';
   }
   //} else
   //   return 1;                                    // Paid

   dPaidAmt= atof(apTokens[RB1_TAX_PAID]);
   dTaxAmt = dDueAmt + dPaidAmt;

   // Redemption date
   if (dPaidAmt > 0.0)
   {
      strcpy(pOutRec->Red_Amt, apTokens[RB1_TAX_PAID]);      
      pOutRec->DelqStatus[0] = TAX_STAT_ONPAYMENT;

      pOutRec->isDelq[0] = '0';
      if (*apTokens[RB1_PAID_DATE] > '0')
      {
         dateConversion(apTokens[RB1_PAID_DATE], pOutRec->Red_Date,  MM_DD_YYYY_1);
      }
   } else if (!dDueAmt)
   {
      pOutRec->DelqStatus[0] = TAX_STAT_REDEEMED;
   } else
   {
      pOutRec->DelqStatus[0] = TAX_STAT_UNPAID;
   }

   return 0;
}

int Ven_ParseTaxDelqYear(char *pOutbuf, char *pInbuf)
{
   TAXDELQ  *pOutRec = (TAXDELQ *)pOutbuf;
   int      iDefYear;

   // Parse input tax data
   iTokens = ParseStringIQ(pInbuf, cDelim, MAX_FLD_TOKEN, apTokens);
   if (iTokens < RB2_COLS)
   {
      LogMsg("***** Error: bad Delq record for: %.50s (#tokens=%d)", pInbuf, iTokens);
      return -1;
   }

   // Bill number for tax year
   strcpy(pOutRec->BillNum, apTokens[RB2_BILLNUM]);

   // Default date
   iDefYear = atol(apTokens[RB2_YEAR]);
   sprintf(pOutRec->Def_Date, "%d", iDefYear+1);

   // Tax Year
   strcpy(pOutRec->TaxYear, apTokens[RB2_YEAR]);

   return 0;
}

int Ven_Load_TaxDelqCsv(char *pDelqFile, bool bAppend, bool bImport)
{
   char     *pTmp, acBuf[1024], acRec[MAX_RECSIZE], acOutBuf[2048];
   char     acTmpFile[_MAX_PATH];

   int      iRet, iDefYr, lCnt=0;
   FILE     *fdIn, *fdOut;
   TAXDELQ  *pTaxDelq = (TAXDELQ *)&acBuf[0];

   LogMsg0("Loading tax redemption file");

   // Open input file
   LogMsg("Open tax redemption file %s", pDelqFile);
   fdIn = fopen(pDelqFile, "r");
   if (fdIn == NULL)
   {
      LogMsg("***** Error opening tax redemption file: %s\n", pDelqFile);
      return -2;
   }  

   // Create output file
   NameTaxCsvFile(acTmpFile, myCounty.acCntyCode, "Delq");
   LogMsg("Open Delq file %s", acTmpFile);
   if (bAppend)
      fdOut = fopen(acTmpFile, "a+");
   else
      fdOut = fopen(acTmpFile, "w");
   if (fdOut == NULL)
   {
      LogMsg("***** Error creating Delq file: %s\n", acTmpFile);
      return -4;
   }

   // Clear output buffer
   memset(acBuf, 0, sizeof(TAXDELQ));
   iDefYr = 0;

   while (!feof(fdIn))
   {
      pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdIn);
      if (!pTmp || *pTmp > '9')
      {
         // Output last delq record
         if (acBuf[0] > ' ')
         {
            Tax_CreateDelqCsv(acOutBuf, (TAXDELQ *)acBuf);
            fputs(acOutBuf, fdOut);
            lRecCnt++;
         }
         break;
      }

      if (acRec[0] == '1')
      {
#ifdef _DEBUG
         //int iTmp;
         //if (!memcmp(pTaxDelq->Apn, "649120230", 9))
         //   iTmp = 0;
#endif
         // Output delq record for every year
         if (iDefYr > 1900)
         {
            Tax_CreateDelqCsv(acOutBuf, (TAXDELQ *)acBuf);
            fputs(acOutBuf, fdOut);
            lRecCnt++;
         } else
            iRet = 0;

         memset(acBuf, 0, sizeof(TAXDELQ));
         iDefYr = 0;

         iRet = Ven_ParseTaxDelq(acBuf, acRec);
      } else if (acRec[0] == '2')
      {
         if (!iDefYr && !iRet)
         {
            iRet = Ven_ParseTaxDelqYear(acBuf, acRec);
            iDefYr = atol(pTaxDelq->Def_Date);
         } 
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   if (fdOut)
      fclose(fdOut);
   if (fdIn)
      fclose(fdIn);

   LogMsg("Total Delq records processed: %u", lCnt);
   LogMsg("                   output:    %u", lRecCnt);

   // Import into SQL
   if (bImport)
      iRet = doTaxImport(myCounty.acCntyCode, TAX_DELINQUENT);
   else
      iRet = 0;

   return iRet;
}

/******************************* Ven_UpdateAll ******************************
 *
 * Load all update files
 *
 ****************************************************************************/

int Ven_UpdateAll()
{
   int   iCnt, iRet;
   long  lHandle;
   char  acTmp[_MAX_PATH], acTmpFile[_MAX_PATH], acRawFile[_MAX_PATH], *pTmp;
   struct _finddata_t  sFileInfo;

   GetIniString(myCounty.acCntyCode, "DailyFiles", "", acTmpFile, _MAX_PATH, acIniFile);
   iCnt = 0;
   iRet = 0;
   lHandle = _findfirst(acTmpFile, &sFileInfo);
   while (!iRet && lHandle > 0)
   {
      pTmp = strrchr(acTmpFile, '\\');
      strcpy(pTmp+1, sFileInfo.name);
      strcpy(acRollFile, acTmpFile);

      strcpy(acTmpFile, acRollFile);
      if (pTmp=strrchr(acTmpFile, '.'))
         strcpy(pTmp, ".SRT");
      else
         strcat(acTmpFile, ".SRT");
      // Sort on APN, ParcelQuestDataExtract.txt is fixed length
      iRet = sortFile(acRollFile, acTmpFile, "S(1,3,C,A,5,6,C,A,4,1,C,A) OMIT(1,3,C,EQ,\"000\") DUPOUT F(TXT)");
      if (iRet > 1)
      {
         iCnt++;
         strcpy(acRollFile, acTmpFile);
         iRet = Ven_LoadUpdt1(iLoadFlag);

         // Rename input file
         if (!iRet)
         {
            sprintf(acTmpFile, "%s.%s", acRollFile, acToday);
            try
            {
               rename(acRollFile, acTmpFile);
               sprintf(acRawFile, acRawTmpl, "VEN", "VEN", "S01");
               sprintf(acTmp, acRawTmpl, "VEN", "VEN", "X01");
               if (!_access(acTmp, 0))
                  remove(acTmp);
               rename(acRawFile, acTmp);
            } catch (...)
            {
               sprintf(acTmp, "*** Error renaming roll update %s to %s", acRollFile, acTmpFile);
               LogMsg(acTmp);
            }
         }
      }

      iRet = _findnext(lHandle, &sFileInfo);
   } 

   if (lHandle > 0)
      _findclose(lHandle);

   LogMsg("Number of files loaded: %d", iCnt);

   return 0;
}

/*********************************** loadVen ********************************
 *
 * Run LoadOne with following options:
 *    -L : to load lien
 *    -U : to load update roll
 *    -G : to process GrGr and create sale file for MergeAdr
 *    -Xc: extract sale from R01 file
 *    -Uu: special load to update standard usecode on R01
 *    -L[yyyymmdd]: Load specific daily file
 *    -Ld         : Load today file
 *    -Xsi
 *
 * Load lien : -O -CVEN -L -Xl -Mg
 * Reg update: -O -CVEN -U -G -Usi
 *
 * Notes:
 *    - Sale price is derived from DTTAMT in Lien file.  If the property is
 *      sold multiple time during the year, we don't have all of them.  Data
 *      from update and GrGr files don't have sale price either.  So we should 
 *      update cumulative sale Ven_cum_sale.dat before load lien.
 *    - Each time LDR is processed, cum sale file is updated.
 *    - Modify the logic of updating sales by 
 *
 ****************************************************************************/

int loadVen(int iSkip)
{
   int   iRet=0;
   char  acTmp[1024], acTmpFile[_MAX_PATH], acRawFile[_MAX_PATH], *pTmp;

   // Preset APN length for the whole process
   iApnLen = myCounty.iApnLen;

   // Load tax
   if (iLoadTax == TAX_LOADING)                    // -T or -Lt
   {
      // Load Tax Agency table
      iRet = GetIniString(myCounty.acCntyCode, "TaxDist", "", acTmpFile, _MAX_PATH, acIniFile);
      if (iRet > 0)
         iRet = LoadTaxCodeTable(acTmpFile);

      // Set extended due date
      TC_SetDateFmt(0, true);

      // Load Tax base
      iRet = Ven_Load_TaxBaseCsv(false);
      if (!iRet)
      {
         // Load supplemental file
         GetIniString(myCounty.acCntyCode, "SuppTax", "", acTmp, _MAX_PATH, acIniFile);
         if (acTmp[0] > '0' && !_access(acTmp, 0))
            iRet = Ven_Load_TaxSuppCsv(bTaxImport);

         // Load redemption file
         GetIniString(myCounty.acCntyCode, "Redemption", "", acTmp, _MAX_PATH, acIniFile);
         if (acTmp[0] > '0' && !_access(acTmp, 0))
         {
            lRecCnt = 0;
            iRet = Ven_Load_TaxDelqCsv(acTmp, false, false);

            if (!iRet)
            {
               GetIniString(myCounty.acCntyCode, "SuppRedemption", "", acTmp, _MAX_PATH, acIniFile);
               if (acTmp[0] > '0' && !_access(acTmp, 0))
                  iRet = Ven_Load_TaxDelqCsv(acTmp, true, bTaxImport);
            }

            if (!iRet)
               iRet = updateDelqFlag(myCounty.acCntyCode);
         }
      } else
         return -1;
   }

   if (!iLoadFlag)
      return 0;

   // Convert old DocNum YY9999999 in GrGr to new one 999999999 - one time 02/12/2022
   //sprintf(acTmpFile, acGrGrTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "sls");
   //iRet = FixSCSalEx(acTmpFile, SALE_FIX_DOCNUM, CNTY_VEN);

   // Fix FirePlace
   //iRet = PQ_FixR01(myCounty.acCntyCode, PQ_FIX_FP);

   // Fix Spc_Flg in Ven_Sale.sls by replacing binary with ascii value - one time
   //iRet = FixSCSal(acCSalFile, SALE_FIX_SPCFLG);

   // Extract Hist sales
   /* 04/04/2012  one time use only
   if (iLoadFlag & EXTR_CSAL)             // -Xc
   {
      sprintf(acRawFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "O01");
      if (_access(acRawFile, 0))
      {
         sprintf(acRawFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "R01");
         if (_access(acRawFile, 0))
            sprintf(acRawFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "S01");
      }
      LogMsg("Extract history sale to %s", acCSalFile);
      iRet = ExtrHSale1(acRawFile, acCSalFile, iRecLen);
   } 

   // Convert old sale file to new format - one time task
   if (bConvSale)
   {
      // Convert old cum sale format to standard cumsale format
      iRet = convertSaleData(myCounty.acCntyCode, acCSalFile, 2);
   }

   // Convert GrGr to sales - one time task
   if (bConvGrGr)
   {
      // Convert old VEN_GRGR.SLS to VEN_GRGR.EXP
      // Rename VEN_GRGR.SLS to VEN_GRGR.RAW and VEN_GRGR.EXP to VEN_GRGR.SLS
      sprintf(acTmpFile, acGrGrTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "EXP");
      sprintf(acTmp, acGrGrTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "SLS");
      iRet = Ven_GrGr2Sale(myCounty.acCntyCode, acTmp, acTmpFile);
      
      if (!iRet)
      {
         sprintf(acRawFile, acGrGrTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "RAW");
         rename(acTmp, acRawFile);
         rename(acTmpFile, acTmp);
      }
   } 

   sprintf(acCSalFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "sls");
   iRet = Ven_FixCumSale(acCSalFile);

   // Fix APN - one time use only
   strcpy(acTmpFile, "H:\\CO_PROD\\SVEN_CD\\raw\\Ven_BadApn.txt");
   iRet = Ven_FixSaleApn(acCSalFile, acTmpFile);
   */

   // Loading GrGr
   if (iLoadFlag & LOAD_GRGR)                      // -G
   {
      // Load special table for GrGr
      GetIniString(myCounty.acCntyCode, "GrGr_Xref", "", acRawFile, _MAX_PATH, acIniFile);
      iNumDeeds = LoadXrefTable(acRawFile, (XREFTBL *)&asDeed[0], MAX_DEED_ENTRIES);
      
      LogMsg0("Load %s GrGr file", myCounty.acCntyCode);
      iRet = Ven_LoadGrGr();     // Output Ven_Grgr.sls unsorted
      if (!iRet)
         iLoadFlag |= MERG_GRGR;
      else if (iRet == 1)
         return iRet;
   }

   // Reload standard table - no longer use 2/13/2014
   //GetIniString(myCounty.acCntyCode, "Sale_Xref", "", acRawFile, _MAX_PATH, acIniFile);
   //iNumDeeds = LoadXrefTable(acRawFile, (XREFTBL *)&asDeed[0], MAX_DEED_ENTRIES);

   // Update sales from VENTURA_PUBINFO.TXT
   /*
   if (iLoadFlag & UPDT_SALE)             // -Us
   {
      iRet = Ven_UpdateCumSale(acRollFile);
      if (!iRet)
         iLoadFlag |= MERG_CSAL;
      else
         return iRet;
   }
   */

   if (iLoadFlag & EXTR_LIEN)                      // -Xl
   {
      LogMsg0("Extract %s Lien file", myCounty.acCntyCode);
      iRet = Ven_ExtrLienCsv(myCounty.acCntyCode);
   }

   // Setup roll file for daily load
   if (iLoadFlag & LOAD_DAILY)                     // -Ld
   {
      GetIniString(myCounty.acCntyCode, "DailyFile", "", acTmp, _MAX_PATH, acIniFile);
      if (lProcDate > 20000101)
      {
         char  sProcDate[32], sNewDate[32];

         sprintf(sProcDate, "%d", lProcDate);
         // ParcelQuestDataExtract_110822.txt
         sprintf(sNewDate, "%.2s%.2s%.2s", &sProcDate[4], &sProcDate[6], &sProcDate[2]);
         lProcDate = atol(sNewDate);
      } 
      sprintf(acTmpFile, acTmp, lProcDate);

      struct   _finddata_t  sFileInfo;
      long lHandle = _findfirst(acTmpFile, &sFileInfo);
      if (lHandle > 0)
      {
         pTmp = strrchr(acTmpFile, '\\');
         strcpy(pTmp+1, sFileInfo.name);
         strcpy(acRollFile, acTmpFile);
         _findclose(lHandle);
      } else
      {
         LogMsg("*** Daily file not available, process stop! ***");
         bGrGrAvail = false;
         return -1;
      }
   }

   // Sort roll file to remove bad records
   if (iLoadFlag & (LOAD_UPDT|LOAD_DAILY))
   {
      LogMsg("Sort Roll file %s", acRollFile);
      lLastFileDate = getFileDate(acRollFile);

      strcpy(acTmpFile, acRollFile);
      if (pTmp=strrchr(acTmpFile, '.'))
         strcpy(pTmp, ".SRT");
      else
         strcat(acTmpFile, ".SRT");
      //iRet = sortFile(acRollFile, acTmpFile, "S(1,3,C,A,5,6,C,A,4,1,C,A) DEL(124) OMIT(1,3,C,EQ,\"000\") DUPOUT F(TXT)");
      // Sort on APN, ParcelQuestDataExtract.txt is fixed length
      iRet = sortFile(acRollFile, acTmpFile, "S(1,3,C,A,5,6,C,A,4,1,C,A) OMIT(1,3,C,EQ,\"000\") DUPOUT F(TXT)");
      if (iRet > 10)
         strcpy(acRollFile, acTmpFile);
   }

   // Extract sales from roll or update file and merge to cum sale file
   if (iLoadFlag & EXTR_SALE)                      // -Xs
   {
      if (iLoadFlag & LOAD_LIEN)
         iRet = Ven_ExtrSale(acRollFile, 1);
      else
         iRet = Ven_ExtrSale(acRollFile, 2);

      if (!iRet)
         iLoadFlag |= MERG_CSAL;
      else
         return iRet;
   }

   // Merge sale with Grgr - If sale has no DocNum and DocDate is the same as GrGr DocDate, 
   // copy GrGr DocNum to sale record for that parcel.
   if (iLoadFlag & (EXTR_SALE|LOAD_GRGR)) 
   {
      iRet = Ven_UpdateSaleUsingGrGr();

      // Merge duplicate transactions
      iRet = SaleDedup(acCSalFile);
      // Remove DocTax & SalePrice if it is carried over from prior sale.
      iRet = FixDocTax(acCSalFile);
   }

   // Process lien roll
   if (iLoadFlag & (LOAD_LIEN|EXTR_LIEN))          // -L
   {
      // 2022
      //iRet = Ven_Load_Lien(iSkip);
      // Rerun 2023 - Using "2023 Secured Complete Summary.csv"
      //iRet = Ven_Load_Lien2(iSkip);

      // 2024
      iRet = Ven_Load_Lien3(iSkip);

      //// Sort lien roll
      //sprintf(acTmpFile, "%s\\%s\\Ven_Lien.srt", acTmpPath, myCounty.acCntyCode);
      //LogMsg("Sort lien roll: %s --> %s", acRollFile, acTmpFile);
      //// Sort Book, Page&Parcel, Mineral
      //iRet = sortFile(acRollFile, acTmpFile, "S(1,3,C,A,5,6,C,A,4,1,C,A) OMIT(1,4,C,EQ,\"0000\") F(TXT)");
      //if (iRet <= 0)
      //   return -1;

      //strcpy(acRollFile, acTmpFile);
      //if (iLoadFlag & EXTR_LIEN)                   // -Xl
      //{
      //   LogMsg0("Extract %s Lien file", myCounty.acCntyCode);
      //   //iRet = Ven_ExtrLien();
      //}

      //if (iLoadFlag & LOAD_LIEN)                   // -L
      //{
      //   // Create Lien file
      //   LogMsg0("Load %s Lien file", myCounty.acCntyCode);
      //   //iRet = Ven_Load_LDR(iLoadFlag, iSkip);
      //}
   } else if (iLoadFlag & (LOAD_UPDT|LOAD_DAILY))  // -Ld|-U
   {  // Process update roll
      LogMsg0("Load %s roll update file", myCounty.acCntyCode);
      iRet = Ven_LoadUpdt(iLoadFlag, iSkip);

      // Rename input file
      GetPrivateProfileString(myCounty.acCntyCode, "RenameInput", "", acTmp, _MAX_PATH, acIniFile);
      if (!iRet && acTmp[0] == 'Y')
      {
         sprintf(acTmpFile, "%s.%s", acRollFile, acToday);
         try
         {
            rename(acRollFile, acTmpFile);
         } catch (...)
         {
            sprintf(acTmp, "*** Error renaming roll update %s to %s", acRollFile, acTmpFile);
            LogMsg(acTmp);
         }
      }
   } else if (iLoadFlag & LOAD_ALL) 
   {
      iRet = Ven_UpdateAll();
      if (!iRet)
         iLoadFlag |= LOAD_UPDT;
   }

   // Apply cum sale
   if (!iRet && (iLoadFlag & MERG_CSAL) )          // -Ms
   {
      SetKeepMAFlg(true);     // Do not reset the MA flag which was set by Ven_MergeRoll()
      iRet = ApplyCumSaleWP(iSkip, acCSalFile, false, SALE_USE_SCUPDXFR, CLEAR_OLD_SALE);
   }

   // Apply cum sale file to R01
   if (!iRet && (iLoadFlag & MERG_GRGR) )          // -Mg
   {
      // Apply Ven_Grgr.sls to R01 file
      sprintf(acTmpFile, acGrGrTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "sls");
      // 20150702 - update GD & XFER
      iRet = ApplyCumSale(iSkip, acTmpFile, false, GRGR_UPD_GD, 0);
      //iRet = ApplyCumSale(iSkip, acTmpFile, false, GRGR_UPD_XFR, 0);
      iLoadFlag |= LOAD_UPDT; // Force compare
   }

   // Update usecode - manually run only
   if (iLoadFlag & UPDT_SUSE)
      iRet = updateR01StdUse(myCounty.acCntyCode, iSkip);

   // Fix TRA
   if (!iRet && (lOptMisc & M_OPT_FIXTRA))
      iRet = PQ_FixR01(myCounty.acCntyCode, PQ_FIX_TRA, 0, true);

   return iRet;
}

