// SCL county definition
#ifndef  _MERGESCL_H
#define  _MERGESCL_H

#define SIZ_SCL_GR_TYPECODE      1
#define SIZ_SCL_GR_FILLER1       1     
#define SIZ_SCL_GR_GRGR				74
#define SIZ_SCL_GR_FILLER2			1
#define SIZ_SCL_GR_PARTY			74
#define SIZ_SCL_GR_FILLER3			1           
#define SIZ_SCL_GR_TITLE			30
#define SIZ_SCL_GR_FILLER4       1        
#define SIZ_SCL_GR_DATE				10
#define SIZ_SCL_GR_FILLER5       1
#define SIZ_SCL_GR_DOCNO			10
#define SIZ_SCL_GR_FILLER6			1
#define SIZ_SCL_GR_APN				8
#define SIZ_SCL_GR_FILLER7       1
#define SIZ_SCL_GR_TRANSFER_TAX  12

#define OFF_SCL_GR_TYPECODE      1
#define OFF_SCL_GR_FILLER1       2     
#define OFF_SCL_GR_GRGR				3
#define OFF_SCL_GR_FILLER2			77
#define OFF_SCL_GR_PARTY			78
#define OFF_SCL_GR_FILLER3			152           
#define OFF_SCL_GR_TITLE			153
#define OFF_SCL_GR_FILLER4       183        
#define OFF_SCL_GR_DATE				184
#define OFF_SCL_GR_FILLER5       194
#define OFF_SCL_GR_DOCNO			195
#define OFF_SCL_GR_FILLER6			205
#define OFF_SCL_GR_APN				206
#define OFF_SCL_GR_FILLER7       214
#define OFF_SCL_GR_TRANSFER_TAX  215

typedef struct _tSclGrGr
{
	char Typecode;
	char Filler1[SIZ_SCL_GR_FILLER1];
	char Grgr[SIZ_SCL_GR_GRGR];
	char Filler2[SIZ_SCL_GR_FILLER2];
	char Party[SIZ_SCL_GR_PARTY];
	char Filler3[SIZ_SCL_GR_FILLER3];
	char Title[SIZ_SCL_GR_TITLE];
	char Filler4[SIZ_SCL_GR_FILLER4];
	char Date[SIZ_SCL_GR_DATE];
	char Filler5[SIZ_SCL_GR_FILLER5];
	char Docno[SIZ_SCL_GR_DOCNO];
	char Filler6[SIZ_SCL_GR_FILLER6];
	char Apn[SIZ_SCL_GR_APN];
	char Filler7[SIZ_SCL_GR_FILLER7];
	char Transfer_Tax[SIZ_SCL_GR_TRANSFER_TAX];
} SCL_GRGR;

// Use these DocCode for GrGr
IDX_SALE SCL_DocCode[] =
{// DOCTITLE, DOCTYPE, ISSALE, ISTRANSFER, CODELEN, TYPELEN
   "DEED   ",                       "13",'Y','Y',7 ,2,
   "QCDD",                          "4 ",'N','Y',4 ,2,
   "DEED/QUITCLAIM",                "4 ",'N','Y',10,2,
   "TRUSTEES DEED",                 "27",'N','Y',10,2,   
   "NOTICE TRUSTEE SALE",           "27",'N','Y',17,2,   
   "NOTICE OF DEF",                 "52",'N','Y',12,2,
   "CERTIFICATE SALE",              "67",'N','Y',16,2,      // Tax Deed
   "SHERIFFS DEED EXECUTION",       "25",'N','Y',23,2,  
   "ASSIGNMENT  ",                  "7 ",'N','N',12,2,
   "CORRECTION DEED",               "9 ",'N','N',12,2,
   "DEED OF TRUST",                 "65",'N','N',10,2,
   "EASEMENT",                      "40",'N','N',8 ,2,
   "","",0,0,0
};

#define   RSIZ_SCL_APN              8
#define   RSIZ_SCL_TRA              5
#define   RSIZ_SCL_NAME1            43
#define   RSIZ_SCL_DOCNUM1          7
#define   RSIZ_SCL_NAME2            43
#define   RSIZ_SCL_CAREOF           43
#define   RSIZ_SCL_M_ADDR1          43
#define   RSIZ_SCL_M_ADDR2          28    // 20240823
#define   RSIZ_SCL_FILLER1          2
#define   RSIZ_SCL_M_ZIP            10
#define   RSIZ_SCL_FILLER2          5
#define   RSIZ_SCL_LAND             9
#define   RSIZ_SCL_IMPR             9
#define   RSIZ_SCL_PP_VAL           9
#define   RSIZ_SCL_EXEAMT           9
#define   RSIZ_SCL_USECODE          2
#define   RSIZ_SCL_ZONING           5
#define   RSIZ_SCL_EXETYPE          1
#define   RSIZ_SCL_WELL             1
#define   RSIZ_SCL_SOLD2ST          2
#define   RSIZ_SCL_DAYSDFLT         2
#define   RSIZ_SCL_FILLER3          6
#define   RSIZ_SCL_DOCDATE          6
#define   RSIZ_SCL_PRIOR_LAND       9
#define   RSIZ_SCL_PRIOR_IMPR       9
#define   RSIZ_SCL_LAT              8
#define   RSIZ_SCL_LONG             7
#define   RSIZ_SCL_SEC_IMPR         9
#define   RSIZ_SCL_FILLER4          1
#define   RSIZ_SCL_FIXT             9
#define   RSIZ_SCL_FIXT_AUDIT       2
#define   RSIZ_SCL_PPVAL1           9
#define   RSIZ_SCL_PP_AUDIT         2
#define   RSIZ_SCL_UNSEC_FLAG       1
#define   RSIZ_SCL_HO_EXE           8
#define   RSIZ_SCL_BUS_INV          8
#define   RSIZ_SCL_OTHEXE           8
#define   RSIZ_SCL_PARCEL_FLAG1     1
#define   RSIZ_SCL_DOCNUM           10
#define   RSIZ_SCL_PARCEL_FLAG2     1
#define   RSIZ_SCL_FILLER5          2

#define   ROFF_SCL_APN              1
#define   ROFF_SCL_TRA              9
#define   ROFF_SCL_NAME1            14
#define   ROFF_SCL_DOCNUM1          57
#define   ROFF_SCL_NAME2            64
#define   ROFF_SCL_CAREOF           107
#define   ROFF_SCL_M_ADDR1          150
#define   ROFF_SCL_M_ADDR2          193
//#define   ROFF_SCL_FILLER1          219
#define   ROFF_SCL_M_ZIP            221
#define   ROFF_SCL_DASH             226
#define   ROFF_SCL_M_ZIP4           227
#define   ROFF_SCL_FILLER2          231
#define   ROFF_SCL_LAND             236
#define   ROFF_SCL_IMPR             245
#define   ROFF_SCL_PP_VAL           254
#define   ROFF_SCL_EXEAMT           263
#define   ROFF_SCL_USECODE          272
#define   ROFF_SCL_ZONING           274
#define   ROFF_SCL_EXETYPE          279
#define   ROFF_SCL_WELL             280
#define   ROFF_SCL_SOLD2ST          281
#define   ROFF_SCL_DAYSDFLT         283
#define   ROFF_SCL_FILLER3          285
#define   ROFF_SCL_DOCDATE          291
#define   ROFF_SCL_PRIOR_LAND       297
#define   ROFF_SCL_PRIOR_IMPR       306
#define   ROFF_SCL_LAT              315
#define   ROFF_SCL_LONG             323
#define   ROFF_SCL_SEC_IMPR         330
#define   ROFF_SCL_FILLER4          339
#define   ROFF_SCL_FIXT             340
#define   ROFF_SCL_FIXT_AUDIT       349
#define   ROFF_SCL_PPVAL1           351
#define   ROFF_SCL_PP_AUDIT         360
#define   ROFF_SCL_UNSEC_FLAG       362
#define   ROFF_SCL_HO_EXE           363
#define   ROFF_SCL_BUS_INV          371
#define   ROFF_SCL_OTHEXE           379
#define   ROFF_SCL_PARCEL_FLAG1     387
#define   ROFF_SCL_DOCNUM           388
#define   ROFF_SCL_PARCEL_FLAG2     398
#define   ROFF_SCL_FILLER5          399

typedef struct _tSclRoll
{  // 402 bytes
   char  Apn[RSIZ_SCL_APN];
   char  TRA[RSIZ_SCL_TRA];
   char  Name1[RSIZ_SCL_NAME1];
   char  DocNum1[RSIZ_SCL_DOCNUM1];          // Last 7-digits
   char  Name2[RSIZ_SCL_NAME2];
   char  CareOf[RSIZ_SCL_CAREOF];
   char  M_Addr1[RSIZ_SCL_M_ADDR1];
   char  M_CitySt[RSIZ_SCL_M_ADDR2];
   //char  filler1[RSIZ_SCL_FILLER1];
   char  M_Zip[RSIZ_SCL_M_ZIP];
   char  filler2[RSIZ_SCL_FILLER2];
   char  Land[RSIZ_SCL_LAND];
   char  Impr[RSIZ_SCL_IMPR];                // Includes FE & ST
   char  PP_Val[RSIZ_SCL_PP_VAL];            // Personal property value
   char  TotalExe[RSIZ_SCL_EXEAMT];          // HO + Constitutional
   char  UseCode[RSIZ_SCL_USECODE];
   char  Zoning[RSIZ_SCL_ZONING];
   char  ExeType;                            // Exemption type 
   char  WellFlag;
   char  Sold2St[RSIZ_SCL_SOLD2ST];          // First year of tax default
   char  DaysDefault[RSIZ_SCL_DAYSDFLT];     // Day of tax default (Month is always June)
   char  filler3[RSIZ_SCL_FILLER3];
   char  DocDate[RSIZ_SCL_DOCDATE];          // Date transferred - (MMDDYY)
   char  Prior_Land[RSIZ_SCL_PRIOR_LAND];
   char  Prior_Impr[RSIZ_SCL_PRIOR_IMPR];
   char  Latitude[RSIZ_SCL_LAT];             // X-Coordinate - obsolete
   char  Longitude[RSIZ_SCL_LONG];           // Y-Coordinate - obsolete
   char  Structure[RSIZ_SCL_SEC_IMPR];       // Structure value
   char  filler4;
   char  Fixt[RSIZ_SCL_FIXT];                // Fixed equipment
   char  Fixt_AuditYr[RSIZ_SCL_FIXT_AUDIT];  // Audit year (Fixed equip & Structure) obsolete
   char  PP_Val1[RSIZ_SCL_PP_VAL];           // Personal property value (redundant)
   char  PP_AuditYr[RSIZ_SCL_PP_AUDIT];      // Audit year - (Personal Property) obsolete
   char  Unsecured_Flag;                     // obsolete
   char  HOExe[RSIZ_SCL_HO_EXE];             // Homeowners exemption value
   char  BusInv[RSIZ_SCL_BUS_INV];           // Business inventory value - obsolete
   char  OtherExe[RSIZ_SCL_OTHEXE];          // Other exemption value (Constitutional)
   char  Parcel_Flag1;                       // Parcel flag (0=assessed, 1=non-assessed)
   char  DocNum[RSIZ_SCL_DOCNUM];            // Document number (All 8-digits + 2-spaces)
   char  Parcel_Flag2;                       // Parcel flag2 (P=public, S=SBE, N=assessed) (All 'N')
   char  filler5[RSIZ_SCL_FILLER5];
   char  CrLf[2];
} SCL_ROLL;

#define  AOFF_SCL_APN            1
#define  AOFF_SCL_S_STRNAME      9
#define  AOFF_SCL_S_STRSFX       31
#define  AOFF_SCL_S_STRDIR       33
#define  AOFF_SCL_S_STRNUM       35
#define  AOFF_SCL_S_FRACT        41
#define  AOFF_SCL_S_UNIT         42
#define  AOFF_SCL_FILLER1        46
#define  AOFF_SCL_S_CITY         48
#define  AOFF_SCL_SCHOOLCODE     50
#define  AOFF_SCL_TRACTNO        52
#define  AOFF_SCL_TRA            58
#define  AOFF_SCL_BLOCKNO        63
#define  AOFF_SCL_TRAFICZONE     66
#define  AOFF_SCL_FILLER2        69
#define  AOFF_SCL_S_ZIP          84
#define  AOFF_SCL_S_ZIP_SEQ      89
#define  AOFF_SCL_S_ZIP4         92
#define  AOFF_SCL_PUBCODE        96
#define  AOFF_SCL_USECODE        97
#define  AOFF_SCL_FILLER3        99

#define  ASIZ_SCL_S_STRNAME      22
#define  ASIZ_SCL_S_STRSFX       2
#define  ASIZ_SCL_S_STRDIR       2
#define  ASIZ_SCL_S_STRNUM       6
#define  ASIZ_SCL_S_FRACT        1
#define  ASIZ_SCL_S_UNIT         4
#define  ASIZ_SCL_FILLER1        2
#define  ASIZ_SCL_S_CITY         2
#define  ASIZ_SCL_SCHOOLCODE     2
#define  ASIZ_SCL_TRACTNO        6
#define  ASIZ_SCL_TRA            5
#define  ASIZ_SCL_BLOCKNO        3
#define  ASIZ_SCL_TRAFICZONE     3
#define  ASIZ_SCL_FILLER2        15
#define  ASIZ_SCL_S_ZIP          5
#define  ASIZ_SCL_S_ZIP_SEQ      3
#define  ASIZ_SCL_S_ZIP4         4
#define  ASIZ_SCL_PUBCODE        1
#define  ASIZ_SCL_USECODE        2
#define  ASIZ_SCL_FILLER3        6

typedef struct _tSclSitus
{  // 106 bytes
   char  Apn[RSIZ_SCL_APN];
   char  strName[ASIZ_SCL_S_STRNAME];
   char  strSfx[ASIZ_SCL_S_STRSFX];
   char  strDir[ASIZ_SCL_S_STRDIR];
   char  strNum[ASIZ_SCL_S_STRNUM];
   char  strFra[ASIZ_SCL_S_FRACT];
   char  Unit[ASIZ_SCL_S_UNIT];
   char  filler1[ASIZ_SCL_FILLER1];
   char  City[ASIZ_SCL_S_CITY];
   char  SchoolCode[ASIZ_SCL_SCHOOLCODE];
   char  Tract[ASIZ_SCL_TRACTNO];
   char  TRA[ASIZ_SCL_TRA];
   char  Block[ASIZ_SCL_TRACTNO];
   char  TraficZone[ASIZ_SCL_TRACTNO];
   char  filler2[ASIZ_SCL_FILLER2];
   char  Zip[ASIZ_SCL_S_ZIP];
   char  ZipSeq[ASIZ_SCL_S_ZIP_SEQ];
   char  Zip4[ASIZ_SCL_S_ZIP4];
   char  PubCode[ASIZ_SCL_PUBCODE];
   char  UseCode[ASIZ_SCL_USECODE];
   char  filler3[ASIZ_SCL_FILLER3];
   char  CrLf[2];
} SCL_SITUS;
#define  SCL_SITUS_SIZ           106

// SF902CSV-2023-07-05.txt
#define  SCL_SIT_APN             0
#define  SCL_SIT_STRNAME         1
#define  SCL_SIT_STRTYPE         2
#define  SCL_SIT_STRDIR          3
#define  SCL_SIT_STRNUM          4
#define  SCL_SIT_STRSUB          5
#define  SCL_SIT_UNITNO          6
#define  SCL_SIT_CITY_CD         7
#define  SCL_SIT_SCHOOL_CD       8
#define  SCL_SIT_TRACT           9
#define  SCL_SIT_TRA             10
#define  SCL_SIT_BLOCK           11
#define  SCL_SIT_TRAF_ZONE       12
#define  SCL_SIT_ZIP             13
#define  SCL_SIT_SEQ_NO          14
#define  SCL_SIT_ZIP4            15
#define  SCL_SIT_PUB_CD          16
#define  SCL_SIT_USE_CD          17
#define  SCL_SIT_FLDS            18

// PC851SFC_CSV-2023-07-17.CSV
#define  SF_APN                  0
#define  SF_BUILDING_NUM         1
#define  SF_PROPERTY_TYPE        2
#define  SF_USE_CODE             3
#define  SF_ZONING_CODE          4
#define  SF_SPECIAL_PROP_FLG     5
#define  SF_DATE_UPDATED         6
#define  SF_LAND_ACRES           7
#define  SF_USABLE_SQ_FEET       8
#define  SF_YEAR_BUILT           9
#define  SF_EFFECTIVE_YEAR       10
#define  SF_TOTAL_ROOMS          11
#define  SF_BATH_ROOMS           12
#define  SF_DINING_ROOM          13
#define  SF_FAMILY_ROOM          14
#define  SF_BEDROOM              15
#define  SF_UTILITY_ROOM         16
#define  SF_QUALITY_CLASS        17
#define  SF_MODEL_NUM            18
#define  SF_HEAT_AIR_COND        19
#define  SF_GARAGE_PORT          20       // G=garage, P=carport, B=Garage/carport, N=none
#define  SF_PORCH_FLAG           21
#define  SF_PATIO_CODE           22       // B=both, U=uncovered, C=cover, N=none
#define  SF_MISC_COSTS           23
#define  SF_POOL_SAP_CODE        24       // P=pool, S=spa, B=pool/spa, N=none
#define  SF_HILLSIDE_FLAG        25
#define  SF_TENNIS_FLAG          26
#define  SF_GARAGE_CONV_FLAG     27
#define  SF_EXTRA_KITCHEN        28
#define  SF_EXTRA_PLUMB          29
#define  SF_CONDITION_CODE       30
#define  SF_SCREEN_ROOM_FLAG     31
#define  SF_TOTAL_AREA           32
#define  SF_FIRST_FLOOR_AREA     33
#define  SF_SECOND_FLR_AREA      34
#define  SF_THIRD_FLOOR_AREA     35
#define  SF_BASEMENT_AREA        36
#define  SF_BASEMENT_FACTOR      37
#define  SF_GARAGE_AREA          38
#define  SF_GARAGE_FACTOR        39
#define  SF_ADDITION_AREA        40
#define  SF_ADDITION_FACTOR      41
#define  SF_REMARKS              42

// PC851MFC_CSV-2023-07-17.CSV
#define  MF_APN                  0
#define  MF_BUILDING_NUM         1
#define  MF_PROPERTY_TYPE        2
#define  MF_USE_CODE             3
#define  MF_ZONING_CODE          4
#define  MF_SPECIAL_PROP_FLG     5
#define  MF_DATE_UPDATED         6
#define  MF_LAND_ACRES           7
#define  MF_USABLE_SQ_FEET       8
#define  MF_YEAR_BUILT           9
#define  MF_EFFECTIVE_YEAR       10
#define  MF_QUALITY_CLASS        11
#define  MF_TOTAL_AREA           12
#define  MF_NUMBER_UNITS         13
#define  MF_LEASE_AREA           14
#define  MF_GAS_FLAG             15
#define  MF_ELECTRIC_FLAG        16
#define  MF_WATER_FLAG           17
#define  MF_GARBASE_FLAG         18
#define  MF_CABLE_TV_FLAG        19
#define  MF_PATIO_BAL_FLAG       20
#define  MF_LAUNDRY_FLAG         21
#define  MF_DISHWASHER_FLAG      22
#define  MF_CENT_HEAT_FLAG       23
#define  MF_AIR_COND_FLAG        24
#define  MF_SAUNA_FLAG           25
#define  MF_TENNIS_FLAG          26
#define  MF_REC_ROOM_FLAG        27
#define  MF_FIREPLACE_FLAG       28
#define  MF_ELEVATOR_FLAG        29
#define  MF_LAKE_STREAM_FLAG     30
#define  MF_POOL_FLAG            31
#define  MF_CONDITION_CODE       32
#define  MF_NUMBER_FLOORS        33
#define  MF_COV_PARKING          34
#define  MF_OPEN_PARKING         35
#define  MF_REMARKS              36

// PC851CIC_CSV-2023-07-17.CSV
#define  CI_APN                  0
#define  CI_BUILDING_NUM         1
#define  CI_PROPERTY_TYPE        2
#define  CI_USE_CODE             3
#define  CI_ZONING_CODE          4
#define  CI_SPECIAL_PROP_FLG     5
#define  CI_DATE_UPDATED         6
#define  CI_LAND_ACRES           7
#define  CI_USABLE_SQ_FEET       8
#define  CI_YEAR_BUILT           9
#define  CI_EFFECTIVE_YEAR       10
#define  CI_NUMBER_UNITS         11
#define  CI_LEASE_AREA           12
#define  CI_RENTABLE_AREA        13
#define  CI_OFFICE_PERCENT       14
#define  CI_WAREHOUSE_PCNT       15
#define  CI_PARKING_RATIO        16
#define  CI_QUALITY_CLASS        17
#define  CI_AIR_COND_FLAG        18
#define  CI_ELEVATOR_FLAG        19
#define  CI_SPRINKLERS_FLAG      20
#define  CI_CONST_CLASS          21
#define  CI_MS_CLASS             22
#define  CI_CONDITION_CODE       23
#define  CI_TOTAL_AREA           24
#define  CI_NUMBER_FLOORS        25
#define  CI_WALL_HEIGHT          26
#define  CI_NUMBER_TENANTS       27
#define  CI_GROSS_INC            28
#define  CI_VACANCY_PERCENT      29
#define  CI_EXPENSES_PERCENT     30
#define  CI_REMARKS              31

// PC851AMC_CSV-2023-07-17.CSV
#define  AG_APN                  0
#define  AG_BUILDING_NUM         1
#define  AG_PROPERTY_TYPE        2
#define  AG_USE_CODE             3
#define  AG_ZONING_CODE          4
#define  AG_SPECIAL_PROP_FLG     5
#define  AG_DATE_UPDATED         6
#define  AG_LAND_ACRES           7
#define  AG_USABLE_SQ_FEET       8

#define  SCL_CHAR_SF             1
#define  SCL_CHAR_MF             2
#define  SCL_CHAR_CI             3
#define  SCL_CHAR_AG             4

// SCL legal
#define  LSIZ_SCL_APN            10
#define  LSIZ_SCL_TRACT          6
#define  LSIZ_SCL_NUMBER         4

typedef struct _tSclLegal
{  // 38 bytes
   char  Apn[LSIZ_SCL_APN];
   char  Tract[LSIZ_SCL_TRACT];
   char  Unit[LSIZ_SCL_NUMBER];
   char  Lot[LSIZ_SCL_NUMBER];
   char  Block[LSIZ_SCL_NUMBER];
   char  Book[LSIZ_SCL_NUMBER];
   char  Page[LSIZ_SCL_NUMBER];
   char  CrLf[2];
} SCL_LEGAL;

// LE852CSV-2022-07-05.TXT
#define  SCL_LGL_APN          0
#define  SCL_LGL_TRACT        1
#define  SCL_LGL_UNIT         2
#define  SCL_LGL_LOT          3
#define  SCL_LGL_BLOCK        4
#define  SCL_LGL_BOOK         5
#define  SCL_LGL_PAGE         6
#define  SCL_LGL_FLDS         7

#define  TSIZ_APN             15
#define  TSIZ_TRA             10
#define  TSIZ_AMT             15
#define  TSIZ_DATE            8
#define  TSIZ_CODE            2
#define  TSIZ_STATUS          20

typedef  struct _tSecTaxRoll
{
   char  TaxYear[4];
   char  APN[TSIZ_APN];
   char  APN_Suffix[TSIZ_APN];
   char  TRA[TSIZ_TRA];
   char  Inst1_40[TSIZ_AMT];
   char  Inst2_40[TSIZ_AMT];
   char  Inst1_60[TSIZ_AMT];
   char  Inst2_60[TSIZ_AMT];
   char  Inst1_70[TSIZ_AMT];
   char  Inst2_70[TSIZ_AMT];
   char  Inst1_SAT[TSIZ_AMT];
   char  Inst2_SAT[TSIZ_AMT];
   char  Int1[TSIZ_AMT];
   char  Int2[TSIZ_AMT];
   char  BadChk1[TSIZ_AMT];
   char  BadChk2[TSIZ_AMT];
   char  Pen1[TSIZ_AMT];
   char  Pen2[TSIZ_AMT];
   char  Cost1[TSIZ_AMT];
   char  Cost2[TSIZ_AMT];
   char  Other1[TSIZ_AMT];
   char  Other2[TSIZ_AMT];
   char  PaidDate1[TSIZ_DATE];
   char  PaidDate2[TSIZ_DATE];
   char  CollCode1[TSIZ_CODE];
   char  CollCode2[TSIZ_CODE];
   char  CrLf[2];
} SEC_TAX;

#define  TSIZ_FILLER1         625
typedef  struct _tSecTaxExt
{
   char  TaxYear[4];
   char  APN[TSIZ_APN];
   char  APN_Suffix[TSIZ_APN];
   char  TRA[TSIZ_TRA];
   char  filler1[TSIZ_FILLER1];
   char  Land[TSIZ_AMT];               // 671
   char  Impr[TSIZ_AMT];
   char  PP_Val[TSIZ_AMT];
   char  HO_Exe[TSIZ_AMT];
   char  Oth_Exe[TSIZ_AMT];
   char  Inst1_40[TSIZ_AMT];           // 746
   char  Inst2_40[TSIZ_AMT];           // 761
   char  Inst1_60[TSIZ_AMT];
   char  Inst2_60[TSIZ_AMT];
   char  Inst1_70[TSIZ_AMT];           // 806
   char  Inst2_70[TSIZ_AMT];           // 821
   char  Inst1_SAT[TSIZ_AMT];          // 836
   char  Inst2_SAT[TSIZ_AMT];          // 851
   char  Int1[TSIZ_AMT];               // 866
   char  Int2[TSIZ_AMT];               // 881
   char  BadChk1[TSIZ_AMT];            // 896
   char  BadChk2[TSIZ_AMT];            // 911
   char  Pen1[TSIZ_AMT];               // 926
   char  Pen2[TSIZ_AMT];               // 941
   char  Cost1[TSIZ_AMT];              // 956
   char  Cost2[TSIZ_AMT];              // 971
   char  Other1[TSIZ_AMT];             // 986
   char  Other2[TSIZ_AMT];             // 1001
   char  S_HseNum[6];                  // 1015
   char  S_HseSfx[3];
   char  S_UnitNo[4];
   char  S_StrDir[2];
   char  S_StrName[22];
   char  S_StrSfx[2];
   char  S_City[15];
   char  S_Zip[5];
   char  DueDate1[TSIZ_DATE];
   char  DueDate2[TSIZ_DATE];
   char  CrLf[2];
} SEC_TAXEXT;

// MF_Redemption_Abstract_Layout.txt
typedef  struct _tRedemptionAbs
{
   char  DefaultedYear[4];
   char  Apn[TSIZ_APN];
   char  Apn_Suffix[TSIZ_APN];
   char  TRA[TSIZ_TRA];
   char  Inst_Plan[1];
   char  S_HseNum[6];
   char  S_HseSfx[3];
   char  S_UnitNo[4];
   char  S_StrName[22];
   char  S_StrDir[2];
   char  S_StrSfx[2];
   char  S_City[15];
   char  S_Zip[5];
   char  Assessee[100];
   char  CareOf[120];
   char  M_Addr1[120];
   char  M_Addr2[120];
   char  M_PostalCode[20];
   char  PayoffAmt[TSIZ_AMT];             // TazAmt=PayoffAmt+SpclAsmnt
   char  SpclAsmnt[TSIZ_AMT];
   char  RetChkChrg[TSIZ_AMT];
   char  Cost[TSIZ_AMT];
   char  Redemption_Penalty[TSIZ_AMT];
   char  Redemption_Fee[TSIZ_AMT];
   char  Penalty[TSIZ_AMT];               // 10% penalty
   char  Other_Charge[TSIZ_AMT];
   char  Paid_Amt[TSIZ_AMT];
   char  Paid_Status[TSIZ_STATUS];
   char  Redeemed_Date[TSIZ_DATE];
   char  Abs_ID[15];
   char  CrLf[2];
} SCL_TAXDELQ;

#define  TSIZ_ASMT_CODE       10
#define  TSIZ_ASMT_NAME       60
typedef  struct _tSpecialAssessment
{
   char  APN[TSIZ_APN];
   char  RollType[TSIZ_APN];
   char  TaxCode[TSIZ_ASMT_CODE];
   char  Name[TSIZ_ASMT_NAME];
   char  AsmtType[TSIZ_ASMT_CODE];
   char  AsmtAmt[TSIZ_AMT];
   char  CrLf[2];
} SPC_ASMT;

// Standard codes
// H=Homeowners
// D=Disabled Veterans
// V=Veterans
// C=Church
// L=Library
// M=Museums
// R=Religion
// T=Tribal Housing
// W=Welfare
// U=College
// P=Public School
// E=Cemetery
// I=Hospital
// S=School
// X=Misc exempt
// Y=Military

IDX_TBL4 SCL_Exemption[] = 
{
   "1", "W", 1,1,
   "2", "C", 1,1,     // CHURCH
   "3", "U", 1,1,     // COLLEGE
   "4", "X", 1,1,     // MISC. - PUBLIC BENEFIT
   "5", "D", 1,1,     // DISABLED VETERAN
   "6", "S", 1,1,     // SCHOOLS BELOW COLLEGE GRADE
   "7", "H", 1,1,     // HOMEOWNER
   "8", "I", 1,1,     // HOSPITAL
   "9", "R", 1,1,     // RELIGIOUS
   "","",0,0
};

#endif