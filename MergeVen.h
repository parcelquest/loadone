#ifndef  _MERGE_VEN_H
#define  _MERGE_VEN_H

// 2024/07/04 - 2024-25 Secured Roll.csv
#define  VEN_L3_APN              0
#define  VEN_L3_APN_STATUS       1
#define  VEN_L3_TRA              2
#define  VEN_L3_NAME1            3
#define  VEN_L3_NAME2            4
#define  VEN_L3_M_ADDR           5
#define  VEN_L3_M_CTYST          6
#define  VEN_L3_ZIP              7
#define  VEN_L3_S_NR             8
#define  VEN_L3_S_ADDR           9
#define  VEN_L3_S_CTYST          10
#define  VEN_L3_DOC_NR           11
#define  VEN_L3_DOCDATE          12
#define  VEN_L3_TRACT            13
#define  VEN_L3_LOT              14
#define  VEN_L3_MAP_NR           15
#define  VEN_L3_CONDO_REF        16
#define  VEN_L3_SITE_USE         17
#define  VEN_L3_SA               18
#define  VEN_L3_ZONE             19
#define  VEN_L3_LOTSQFT          20
#define  VEN_L3_BLDGSQFT         21
#define  VEN_L3_ACREAGE          22
#define  VEN_L3_YR_BLT           23
#define  VEN_L3_FLR_1_A          24
#define  VEN_L3_FLR_2_A          25
#define  VEN_L3_FLR_3_A          26
#define  VEN_L3_BAL_A            27
#define  VEN_L3_POR_A            28
#define  VEN_L3_OTHER_A          29
#define  VEN_L3_GAR_A            30
#define  VEN_L3_CARPT_A          31
#define  VEN_L3_PAT_A            32
#define  VEN_L3_EN_PAT_A         33
#define  VEN_L3_POOL_A           34
#define  VEN_L3_DECK_A           35
#define  VEN_L3_FP               36
#define  VEN_L3_BEDS             37
#define  VEN_L3_FAM_DEN          38
#define  VEN_L3_DINING           39
#define  VEN_L3_UTIL_RM          40
#define  VEN_L3_OTHER_RMS        41
#define  VEN_L3_BATHS            42
#define  VEN_L3_LOT_WIDTH        43
#define  VEN_L3_LOT_DEPTH        44
#define  VEN_L3_CL               45
#define  VEN_L3_DIST             46
#define  VEN_L3_NGH              47
#define  VEN_L3_SOLAR_E_FROM     48
#define  VEN_L3_SOLAR_CHAR       49
#define  VEN_L3_LAND             50
#define  VEN_L3_IMPR             51
#define  VEN_L3_PERSPROP         52
#define  VEN_L3_FIXTURE          53
#define  VEN_L3_MINERALRIGHTS    54
#define  VEN_L3_TREESVINES       55
#define  VEN_L3_EXE_CD1          56
#define  VEN_L3_EXEAMT1          57
#define  VEN_L3_EXE_CD2          58
#define  VEN_L3_EXEAMT2          59
#define  VEN_L3_EXE_CD3          60
#define  VEN_L3_EXEAMT3          61
#define  VEN_L3_EXE_CD4          62
#define  VEN_L3_EXEAMT4          63
#define  VEN_L3_ASSDVAL          64
#define  VEN_L3_FLDS             65

// 12/12/2023 - 2023 Secured Complete Summary.csv
#define  VEN_L2_APN              0
#define  VEN_L2_NAME1            1
#define  VEN_L2_NAME2            2
#define  VEN_L2_MAIL_ATTN        3
#define  VEN_L2_MAIL_ADDR        4
#define  VEN_L2_CTY_STA          5
#define  VEN_L2_ZIP              6
#define  VEN_L2_FULLSITUS        7
#define  VEN_L2_ISPUBRES         8     // IsPublicAccessRestricted
#define  VEN_L2_LAND             9  
#define  VEN_L2_IMPR             10
#define  VEN_L2_PERSPROP         11
#define  VEN_L2_FIXTURE          12
#define  VEN_L2_MINERALRIGHTS    13
#define  VEN_L2_TREESVINES       14
#define  VEN_L2_ASSDVAL          15
#define  VEN_L2_FULLCASHVAL      16
#define  VEN_L2_EXE_CD1          17
#define  VEN_L2_EXEAMT1          18
#define  VEN_L2_EXE_CD2          19
#define  VEN_L2_EXEAMT2          20
#define  VEN_L2_EXE_CD3          21
#define  VEN_L2_EXEAMT3          22
#define  VEN_L2_EXE_CD4          23 
#define  VEN_L2_EXEAMT4          24
#define  VEN_L2_NETVALUE         25
#define  VEN_L2_PENAMT           26
#define  VEN_L2_TOTALVALUE       27
#define  VEN_L2_INTAMT           28
#define  VEN_L2_APN2             29
#define  VEN_L2_APN_STATUS       30
#define  VEN_L2_TRA              31
#define  VEN_L2_DOC_NR           32
#define  VEN_L2_DOCDATE          33 
#define  VEN_L2_DOC_TYPE         34
#define  VEN_L2_ARC              35
#define  VEN_L2_TRACT            36
#define  VEN_L2_BLK              37
#define  VEN_L2_LOT              38
#define  VEN_L2_LOT_SUB          39
#define  VEN_L2_MAP_NR           40
#define  VEN_L2_CONDO_REF        41
#define  VEN_L2_CONDO_BLDG       42
#define  VEN_L2_CONDO_UNIT       43
#define  VEN_L2_PREV_APN         44
#define  VEN_L2_VOID_YR_CC       45
#define  VEN_L2_BSE_YR_APN       46
#define  VEN_L2_BSE_YR           47
#define  VEN_L2_SITE_USE         48
#define  VEN_L2_SA               49
#define  VEN_L2_ZONE             50
#define  VEN_L2_LOTSQFT          51
#define  VEN_L2_ACREAGE          52
#define  VEN_L2_DT_SALE_Y2K      53
#define  VEN_L2_DTS              54
#define  VEN_L2_EFF_DOC_DT_Y2K   55
#define  VEN_L2_MH_V             56
#define  VEN_L2_MH_MOD_YR        57
#define  VEN_L2_MH_W_X_L         58
#define  VEN_L2_MH_SPACE         59
#define  VEN_L2_MH_PARK          60
#define  VEN_L2_YR_BLT           61
#define  VEN_L2_FLR_1_A          62
#define  VEN_L2_FLR_2_A          63
#define  VEN_L2_FLR_3_A          64
#define  VEN_L2_BAL_A            65
#define  VEN_L2_POR_A            66
#define  VEN_L2_BSMT_A           67
#define  VEN_L2_ADDN_1_A         68
#define  VEN_L2_ADDN_2_A         69
#define  VEN_L2_OTHER_A          70
#define  VEN_L2_GAR_A            71
#define  VEN_L2_CARPT_A          72
#define  VEN_L2_PAT_A            73 
#define  VEN_L2_EN_PAT_A         74 
#define  VEN_L2_POOL_A           75
#define  VEN_L2_DECK_A           76
#define  VEN_L2_FP               77
#define  VEN_L2_BEDS             78
#define  VEN_L2_FAM_DEN          79
#define  VEN_L2_DINING           80
#define  VEN_L2_UTIL_RM          81
#define  VEN_L2_OTHER_RMS        82
#define  VEN_L2_TOT_RMS          83
#define  VEN_L2_BATHS            84
#define  VEN_L2_NPISP            85
#define  VEN_L2_TRA_SPLIT        86
#define  VEN_L2_DBA              87
#define  VEN_L2_FRONTAGE         88
#define  VEN_L2_LOT_WIDTH        89
#define  VEN_L2_LOT_DEPTH        90
#define  VEN_L2_DOC_FAC          91
#define  VEN_L2_RR_SPUR          92
#define  VEN_L2_OFFICE_A         93
#define  VEN_L2_MEZZ_A           94
#define  VEN_L2_RETAIL_A         95
#define  VEN_L2_SHOP_A           96
#define  VEN_L2_STORAGE_A        97
#define  VEN_L2_WHSE_A           98
#define  VEN_L2_BLNK_5           99
#define  VEN_L2_APL_CD           100
#define  VEN_L2_CL               101
#define  VEN_L2_DIST             102
#define  VEN_L2_DT_V_TRF_Y2K     103
#define  VEN_L2_NGH              104
#define  VEN_L2_NUIS             105
#define  VEN_L2_QC               106
#define  VEN_L2_SQ_FT_I          107
#define  VEN_L2_STRUC_FAIL       108
#define  VEN_L2_TRAFFIC          109
#define  VEN_L2_VIEW             110
#define  VEN_L2_SP               111
#define  VEN_L2_NON_TAX_CD       112
#define  VEN_L2_FLDS             113

// 07/07/2023 2023 Secured Roll.csv
#define  VEN_L1_APN              0
#define  VEN_L1_TRA              1
#define  VEN_L1_NAME1            2
#define  VEN_L1_NAME2            3
#define  VEN_L1_MAIL_ADDR        4
#define  VEN_L1_CTY_STA          5
#define  VEN_L1_ZIP              6
#define  VEN_L1_DOC_NR           7
#define  VEN_L1_DOC_DT           8     // blank
#define  VEN_L1_DOC_TYPE         9
#define  VEN_L1_ARC              10
#define  VEN_L1_S_NR             11
#define  VEN_L1_S_DIR            12
#define  VEN_L1_S_STREET         13
#define  VEN_L1_S_TYP            14
#define  VEN_L1_S_UNIT           15
#define  VEN_L1_SA               16
#define  VEN_L1_TRACT            17
#define  VEN_L1_MAP_NR           18
#define  VEN_L1_PREV_APN         19
#define  VEN_L1_BLK              20
#define  VEN_L1_LOT              21
#define  VEN_L1_LOT_SUB          22    // blank
#define  VEN_L1_CONDO_REF        23
#define  VEN_L1_CONDO_BLDG       24
#define  VEN_L1_CONDO_UNIT       25
#define  VEN_L1_VOID_YR          26
#define  VEN_L1_BSE_YR_APN       27
#define  VEN_L1_BSE_YR           28
#define  VEN_L1_NPISP            29
#define  VEN_L1_USECODE          30
#define  VEN_L1_DTS              31
#define  VEN_L1_EFF_DOC_DT       32    // blank
#define  VEN_L1_LAND             33
#define  VEN_L1_IMPR             34
#define  VEN_L1_TREEVINES        35
#define  VEN_L1_MINERALRIGHTS    36
#define  VEN_L1_FIXTURE          37
#define  VEN_L1_UNIT_TF_V        38
#define  VEN_L1_PERSPROP         39
#define  VEN_L1_EXE_CD1          40
#define  VEN_L1_ACREAGE          41
#define  VEN_L1_LOTSQFT          42
#define  VEN_L1_ZONE             43
#define  VEN_L1_YR_BLT           44
#define  VEN_L1_FLR_1_A          45
#define  VEN_L1_FLR_2_A          46
#define  VEN_L1_BSMT_A           47
#define  VEN_L1_ADDN_1_A         48
#define  VEN_L1_ADDN_2_A         49
#define  VEN_L1_OTHER_A          50
#define  VEN_L1_GAR_A            51
#define  VEN_L1_PAT_A            52
#define  VEN_L1_EN_PAT_A         53
#define  VEN_L1_POOL_A           54
#define  VEN_L1_DECK_A           55
#define  VEN_L1_FP               56
#define  VEN_L1_BATHS            57
#define  VEN_L1_BEDS             58
#define  VEN_L1_FAM_DEN          59
#define  VEN_L1_DINING           60
#define  VEN_L1_UTIL_RM          61
#define  VEN_L1_OTHER_RMS        62
#define  VEN_L1_TOT_RMS          63
#define  VEN_L1_MH_PARK          64
#define  VEN_L1_MH_SPACE         65
#define  VEN_L1_MH_W_X_L         66
#define  VEN_L1_MH_MOD_YR        67
#define  VEN_L1_MH_V             68
#define  VEN_L1_CL               69
#define  VEN_L1_AG_PRES_NR       70
#define  VEN_L1_EXEAMT1          71
#define  VEN_L1_S_CITY           72    // BLANK
#define  VEN_L1_S_ZIP            73    // BLANK
#define  VEN_L1_FILLER           74
#define  VEN_L1_OWN_CD1          75
#define  VEN_L1_OWN_CD2          76
#define  VEN_L1_DOCDATE          77
#define  VEN_L1_VOID_YR_CC       78
#define  VEN_L1_SALEDATE         79
#define  VEN_L1_EFF_DOCDATE      80
#define  VEN_L1_FLR_3_A          81
#define  VEN_L1_BAL_A            82
#define  VEN_L1_POR_A            83
#define  VEN_L1_CARPT_A          84
#define  VEN_L1_TRA_SPLIT        85
#define  VEN_L1_DBA              86
#define  VEN_L1_FRONTAGE         87
#define  VEN_L1_LOT_WIDTH        88
#define  VEN_L1_LOT_DEPTH        89
#define  VEN_L1_DOC_FAC          90
#define  VEN_L1_RR_SPUR          91
#define  VEN_L1_OFFICE_A         92
#define  VEN_L1_MEZZ_A           93
#define  VEN_L1_RETAIL_A         94
#define  VEN_L1_SHOP_A           95
#define  VEN_L1_STORAGE_A        96
#define  VEN_L1_WHSE_A           97
#define  VEN_L1_BLNK_5           98
#define  VEN_L1_UNIT_PP_V        99
#define  VEN_L1_EXE_CD2          100
#define  VEN_L1_EXEAMT2          101
#define  VEN_L1_EXE_CD3          102
#define  VEN_L1_EXEAMT3          103
#define  VEN_L1_APL_CD           104
#define  VEN_L1_DIST             105
#define  VEN_L1_DT_V_TRF_Y2K     106
#define  VEN_L1_NGH              107
#define  VEN_L1_NUIS             108
#define  VEN_L1_QC               109
#define  VEN_L1_SQ_FT_I          110
#define  VEN_L1_STRUC_FAIL       111
#define  VEN_L1_TRAFFIC          112
#define  VEN_L1_VIEW             113
#define  VEN_L1_SP               114
#define  VEN_L1_NON_TAX_CD       115
#define  VEN_L1_UPDATE_DT        116
#define  VEN_L1_FLDS             117

// 2022/07/08 - 2022 Secured Parcel Data.csv
#define  VEN_L_APN               0
#define  VEN_L_ASMTNUMBER        1
#define  VEN_L_PARCELTYPE        2
#define  VEN_L_USECODE           3
#define  VEN_L_LANDUMTN          4
#define  VEN_L_LANDUPTN          5
#define  VEN_L_DOCNUM            6
#define  VEN_L_DOCDATE           7
#define  VEN_L_TRA               8
#define  VEN_L_OWNERCODE1        9
#define  VEN_L_OWNER1            10
#define  VEN_L_OWNERCODE2        11
#define  VEN_L_OWNER2            12
#define  VEN_L_FULLMAIL          13
#define  VEN_L_FULLSITUS         14
#define  VEN_L_LAND              15
#define  VEN_L_IMPR              16
#define  VEN_L_FIXTURE           17
#define  VEN_L_PERSPROP          18
#define  VEN_L_MINERALRIGHTS     19
#define  VEN_L_TREESVINES        20
#define  VEN_L_EXETYPENAME       21
#define  VEN_L_EXEAMOUNT         22
#define  VEN_L_LEGALDESC         23
#define  VEN_L_LEGALTYPENAME     24
#define  VEN_L_TRACK             25    // Field1
#define  VEN_L_PREVAPN           26
#define  VEN_L_LOT               27
#define  VEN_L_MAPREF            28
#define  VEN_L_UNITNUM           29
#define  VEN_L_FORMTYPE1         30
#define  VEN_L_QUALCLASS1        31
#define  VEN_L_IMPRAREA1         32    // LotSqft
#define  VEN_L_CONSTYEAR1        33
#define  VEN_L_FORMTYPE2         34
#define  VEN_L_QUALCLASS2        35
#define  VEN_L_IMPRAREA2         36    // BldgSqft if FORMTYPE2=SFR
#define  VEN_L_CONSTYEAR2        37    // YrBlt
#define  VEN_L_FORMTYPE3         38
#define  VEN_L_QUALCLASS3        39
#define  VEN_L_IMPRAREA3         40
#define  VEN_L_CONSTYEAR3        41
#define  VEN_L_FORMTYPE4         42
#define  VEN_L_QUALCLASS4        43
#define  VEN_L_IMPRAREA4         44
#define  VEN_L_CONSTYEAR4        45
#define  VEN_L_FLDS              46

// 2022_07_05_Public_Data_Final
#define  VEN_P_ACREAGE           0
#define  VEN_P_ADDN1A            1
#define  VEN_P_ADDN2A            2
#define  VEN_P_ADDN3A            3
#define  VEN_P_APLCD             4
#define  VEN_P_APN               5
#define  VEN_P_APNSTATUS         6
#define  VEN_P_ARC               7
#define  VEN_P_BALA              8     // Balcony area
#define  VEN_P_BATHS             9
#define  VEN_P_BEDS              10
#define  VEN_P_BLK               11
#define  VEN_P_BSEYR             12
#define  VEN_P_BSEYRAPN          13
#define  VEN_P_CARPTA            14    // Carport area
#define  VEN_P_CL                15
#define  VEN_P_CONDOBLDG         16
#define  VEN_P_CONDOREF          17
#define  VEN_P_CONDOUNIT         18
#define  VEN_P_CTYSTA            19
#define  VEN_P_DECKA             20
#define  VEN_P_DIST              21
#define  VEN_P_DOCDTY2K          22
#define  VEN_P_DOCNR             23
#define  VEN_P_DOCTYPE           24
#define  VEN_P_DTS               25
#define  VEN_P_DTSALEY2K         26
#define  VEN_P_DTVTRFY2K         27
#define  VEN_P_EFFDOCDTY2K       28
#define  VEN_P_ENPATA            29
#define  VEN_P_EXMPCD1           30
#define  VEN_P_EXMPCD2           31
#define  VEN_P_EXMPCD3           32
#define  VEN_P_EXMPV1            33
#define  VEN_P_EXMPV2            34
#define  VEN_P_EXMPV3            35
#define  VEN_P_FAMDEN            36
#define  VEN_P_FLR1A             37
#define  VEN_P_FLR2A             38
#define  VEN_P_FLR3A             39
#define  VEN_P_FP                40
#define  VEN_P_FRONTAGE          41
#define  VEN_P_GARA              42    // Garage area
#define  VEN_P_IV                43    // Impr value
#define  VEN_P_LOT               44
#define  VEN_P_LOTDEPTH          45
#define  VEN_P_LOTSUB            46
#define  VEN_P_LOTWIDTH          47
#define  VEN_P_LV                48    // Land value
#define  VEN_P_MAILADDR          49
#define  VEN_P_MAPNR             50
#define  VEN_P_MHMODYR           51
#define  VEN_P_MHPARK            52
#define  VEN_P_MHSPACE           53
#define  VEN_P_MHV               54
#define  VEN_P_MHWXL             55
#define  VEN_P_MRV               56
#define  VEN_P_NAME1             57
#define  VEN_P_NAME2             58
#define  VEN_P_NGH               59
#define  VEN_P_NONTAXCD          60
#define  VEN_P_NPISP             61
#define  VEN_P_NUIS              62
#define  VEN_P_OTHERA            63
#define  VEN_P_OTHERRMS          64
#define  VEN_P_PATA              65    // Patio area
#define  VEN_P_POOLA             66    // Pool area
#define  VEN_P_PORA              67    // Porch area
#define  VEN_P_PPV               68    // Personal property value
#define  VEN_P_PREVAPN           69
#define  VEN_P_QC                70    // Quality class
#define  VEN_P_SA                71
#define  VEN_P_SITEUSE           72
#define  VEN_P_SITUSDIR          73
#define  VEN_P_SITUSNR           74
#define  VEN_P_SITUSSTREET       75
#define  VEN_P_SITUSTYP          76
#define  VEN_P_SITUSUNIT         77
#define  VEN_P_SP                78
#define  VEN_P_SQFTI             79    // BldgSqft
#define  VEN_P_SQFTL             80    // LotSqft
#define  VEN_P_STRUCFAIL         81
#define  VEN_P_TFV               82
#define  VEN_P_TOTRMS            83
#define  VEN_P_TRA               84
#define  VEN_P_TRACT             85
#define  VEN_P_TRAFFIC           86
#define  VEN_P_TRASPLIT          87
#define  VEN_P_TVV               88
#define  VEN_P_VIEW              89
#define  VEN_P_VOIDYRCC          90
#define  VEN_P_YRBLT             91
#define  VEN_P_ZIP               92
#define  VEN_P_ZONE              93
#define  VEN_P_FLDS              94

// ASR_SEC_PUBINFO
#define  ROFF_APN              1
#define  ROFF_APN_STATUS       11
#define  ROFF_TRA              12
#define  ROFF_NAME1            17
#define  ROFF_NAME2            53
#define  ROFF_OWNER_CD1        89
#define  ROFF_OWNER_CD2        90
#define  ROFF_M_STREET         91
#define  ROFF_M_CITY           136
#define  ROFF_M_ZIP            160
#define  ROFF_DOCNUM           170     // Xfer DOCNUM
#define  ROFF_DOC_DT           179     // Xfer DOCDATE.  This may not be the same as sale date at offset 367
#define  ROFF_DOCTYPE          187
#define  ROFF_ROLCHANGENO      191
#define  ROFF_S_STREETNUM      200
#define  ROFF_S_DIR            209
#define  ROFF_S_STREETNAME     211
#define  ROFF_S_TYPE           236
#define  ROFF_S_UNITNO         238
#define  ROFF_TRACTNO          248
#define  ROFF_BLOCKNO          254
#define  ROFF_LOTNO            256
#define  ROFF_LOTSUBNO         262
#define  ROFF_MAPNO            263
#define  ROFF_CONDOREF         273
#define  ROFF_CONDOBLDG        282
#define  ROFF_CONDOUNIT        286
#define  ROFF_PREVAPN          291
#define  ROFF_VOID_CCYR        301
#define  ROFF_BASEYEARAPN      305
#define  ROFF_BASEYEAR         315
#define  ROFF_USECODE          320
#define  ROFF_SITEAREA         324
#define  ROFF_ZONE             326
#define  ROFF_NONTAXCODE       335
#define  ROFF_BLDG_SQFT        336
#define  ROFF_LOT_SQFT         344
#define  ROFF_ACREAGE          355
#define  ROFF_UNITS            363
#define  ROFF_SALEDATE         367
#define  ROFF_DTT_AMT          375
#define  ROFF_EFF_DOC_DT       382
#define  ROFF_MHVALUE_FLG      390
#define  ROFF_MHYEAR           391
#define  ROFF_MHWIDTH          395
#define  ROFF_MHSPACE          401
#define  ROFF_MHPARKNAME       406
#define  ROFF_YEARBUILT        446
#define  ROFF_FLOOR1           450
#define  ROFF_FLOOR2           455
#define  ROFF_FLOOR3           460
#define  ROFF_BALCONY          465
#define  ROFF_PORCH            470
#define  ROFF_BASEMENT         475
#define  ROFF_ADDITION1        480
#define  ROFF_ADDITION2        485
#define  ROFF_OTHERAREA        490
#define  ROFF_GARGAGE          495
#define  ROFF_CARPORT          500
#define  ROFF_PATIO            505
#define  ROFF_ENCLOSEDPATIO    510
#define  ROFF_POOL             515
#define  ROFF_DECK             520
#define  ROFF_FP               525
#define  ROFF_BEDS             527
#define  ROFF_FAMILY           529
#define  ROFF_DINING           530
#define  ROFF_UTILITY          531
#define  ROFF_OTHERROOMS       532
#define  ROFF_TOTALROOMS       533
#define  ROFF_BATHS            535
#define  ROFF_NUM_PARCELS      538
#define  ROFF_FILLER1          542
#define  ROFF_DBA              547
#define  ROFF_FRONTAGE         563
#define  ROFF_LOTWIDTH         568
#define  ROFF_LOTDEPTH         573
#define  ROFF_DOCKFAC          578
#define  ROFF_RR_SPUR          579
#define  ROFF_OFFICE_AREA      580
#define  ROFF_MEZZANINE_AREA   585
#define  ROFF_RETAIL_AREA      590
#define  ROFF_SHOP_AREA        595
#define  ROFF_STORAGE_AREA     600
#define  ROFF_WAREHOUSE_AREA   605
#define  ROFF_FILLER2          610
#define  ROFF_LANDVALUE        615
#define  ROFF_IMPROVEVALUE     624
#define  ROFF_MINERALVALUE     633
#define  ROFF_TREESVALUE       642
#define  ROFF_TRADEVALUE       651
#define  ROFF_PERSONALVALUE    660
#define  ROFF_UNITTFVALUE      669
#define  ROFF_UNITPPVALUE      678
#define  ROFF_EXECODE1         687
#define  ROFF_EXEVALUE1        693
#define  ROFF_EXECODE2         702
#define  ROFF_EXEVALUE2        708
#define  ROFF_EXECODE3         717
#define  ROFF_EXEVALUE3        723

#define  RSIZ_APN              10
#define  RSIZ_APN_STATUS       1
#define  RSIZ_TRA              5
#define  RSIZ_NAME1            36
#define  RSIZ_NAME2            36
#define  RSIZ_OWNER_CD1        1
#define  RSIZ_OWNER_CD2        1
#define  RSIZ_M_STREET         45
#define  RSIZ_M_CITY           24
#define  RSIZ_M_ZIP            10
#define  RSIZ_DOCNUM           9
#define  RSIZ_DOC_DT           8
#define  RSIZ_DOCTYPE          4
#define  RSIZ_ROLCHANGENO      9
#define  RSIZ_S_STREETNUM      9
#define  RSIZ_S_DIR            2
#define  RSIZ_S_STREETNAME     25
#define  RSIZ_S_TYPE           2
#define  RSIZ_S_UNITNO         10
#define  RSIZ_TRACTNO          6
#define  RSIZ_BLOCKNO          2
#define  RSIZ_LOTNO            6
#define  RSIZ_LOTSUBNO         1
#define  RSIZ_MAPNO            10
#define  RSIZ_CONDOREF         9
#define  RSIZ_CONDOBLDG        4
#define  RSIZ_CONDOUNIT        5
#define  RSIZ_PREVAPN          10
#define  RSIZ_VOID_CCYR        4
#define  RSIZ_BASEYEARAPN      10
#define  RSIZ_BASEYEAR         5
#define  RSIZ_USECODE          4
#define  RSIZ_SITEAREA         2
#define  RSIZ_ZONE             9
#define  RSIZ_NONTAXCODE       1
#define  RSIZ_BLDG_SQFT        8
#define  RSIZ_LOT_SQFT         11
#define  RSIZ_ACREAGE          8
#define  RSIZ_UNITS            4
#define  RSIZ_SALEDATE         8
#define  RSIZ_DTT_AMT          7
#define  RSIZ_EFF_DOC_DT       8
#define  RSIZ_MHVALUE_FLG      1
#define  RSIZ_MHYEAR           4
#define  RSIZ_MHWIDTH          6
#define  RSIZ_MHSPACE          5
#define  RSIZ_MHPARKNAME       40
#define  RSIZ_YEARBUILT        4
#define  RSIZ_FLOOR1           5
#define  RSIZ_FLOOR2           5
#define  RSIZ_FLOOR3           5
#define  RSIZ_BALCONY          5
#define  RSIZ_PORCH            5
#define  RSIZ_BASEMENT         5
#define  RSIZ_ADDITION1        5
#define  RSIZ_ADDITION2        5
#define  RSIZ_OTHERAREA        5
#define  RSIZ_GARGAGE          5
#define  RSIZ_CARPORT          5
#define  RSIZ_PATIO            5
#define  RSIZ_ENCLOSEDPATIO    5
#define  RSIZ_POOL             5
#define  RSIZ_DECK             5
#define  RSIZ_FP               2
#define  RSIZ_BEDS             2
#define  RSIZ_FAMILY           1
#define  RSIZ_DINING           1
#define  RSIZ_UTILITY          1
#define  RSIZ_OTHERROOMS       1
#define  RSIZ_TOTALROOMS       2
#define  RSIZ_BATHS            3
#define  RSIZ_NUM_PARCELS      4
#define  RSIZ_FILLER1          5
#define  RSIZ_DBA              16
#define  RSIZ_FRONTAGE         5
#define  RSIZ_LOTWIDTH         5
#define  RSIZ_LOTDEPTH         5
#define  RSIZ_DOCKFAC          1
#define  RSIZ_RR_SPUR          1
#define  RSIZ_OFFICE_AREA      5
#define  RSIZ_MEZZANINE_AREA   5
#define  RSIZ_RETAIL_AREA      5
#define  RSIZ_SHOP_AREA        5
#define  RSIZ_STORAGE_AREA     5
#define  RSIZ_WAREHOUSE_AREA   5
#define  RSIZ_FILLER2          5
#define  RSIZ_LANDVALUE        9
#define  RSIZ_IMPROVEVALUE     9
#define  RSIZ_MINERALVALUE     9
#define  RSIZ_TREESVALUE       9
#define  RSIZ_TRADEVALUE       9
#define  RSIZ_PERSONALVALUE    9
#define  RSIZ_UNITTFVALUE      9
#define  RSIZ_UNITPPVALUE      9
#define  RSIZ_EXECODE1         6
#define  RSIZ_EXEVALUE1        9
#define  RSIZ_EXECODE2         6
#define  RSIZ_EXEVALUE2        9
#define  RSIZ_EXECODE3         6
#define  RSIZ_EXEVALUE3        9

typedef struct _tVenRoll
{
   char  Apn[RSIZ_APN];
   char  Apn_Status[RSIZ_APN_STATUS];
   char  TRA[RSIZ_TRA];
   char  Name1[RSIZ_NAME1];
   char  Name2[RSIZ_NAME2];
   char  Owner_CD1[RSIZ_OWNER_CD1];
   char  Owner_CD2[RSIZ_OWNER_CD1];
   char  M_Street[RSIZ_M_STREET];               // 91
   char  M_City[RSIZ_M_CITY];
   char  M_Zip[RSIZ_M_ZIP];
   char  DocNum[RSIZ_DOCNUM];
   char  Doc_Dt[RSIZ_DOC_DT];
   char  DocType[RSIZ_DOCTYPE];
   char  RolChangeNo[RSIZ_ROLCHANGENO];
   char  S_StreetNum[RSIZ_S_STREETNUM];         // 200
   char  S_Dir[RSIZ_S_DIR];
   char  S_StreetName[RSIZ_S_STREETNAME];
   char  S_Type[RSIZ_S_TYPE];
   char  S_UnitNo[RSIZ_S_UNITNO];
   char  TractNo[RSIZ_TRACTNO];
   char  BlockNo[RSIZ_BLOCKNO];
   char  LotNo[RSIZ_LOTNO];
   char  LotSubNo[RSIZ_LOTSUBNO];
   char  MapNo[RSIZ_MAPNO];
   char  CondoRef[RSIZ_CONDOREF];
   char  CondoBldg[RSIZ_CONDOBLDG];
   char  CondoUnit[RSIZ_CONDOUNIT];
   char  PrevAPN[RSIZ_PREVAPN];
   char  VoidYear[RSIZ_VOID_CCYR];
   char  BaseYear_APN[RSIZ_BASEYEARAPN];
   char  BaseYear[RSIZ_BASEYEAR];
   char  UseCode[RSIZ_USECODE];
   char  SiteArea[RSIZ_SITEAREA];      // City code
   char  Zone[RSIZ_ZONE];
   // 1=County, 2=City, 3=State, 4=Federal,
   // 5=SBE, 6=Special dist, 7=Housing Dev, 8=School
   char  NonTaxCode;
   char  BldgSqft[RSIZ_BLDG_SQFT];
   char  LotSqft[RSIZ_LOT_SQFT];
   char  Acres[RSIZ_ACREAGE];
   char  Units[RSIZ_UNITS];
   char  SaleDate[RSIZ_SALEDATE];
   char  Doc_Tax[RSIZ_DTT_AMT];
   char  Eff_Doc_Dt[RSIZ_EFF_DOC_DT];
   char  MHValue[RSIZ_MHVALUE_FLG];
   char  MHYear[RSIZ_MHYEAR];
   char  MHWidth[RSIZ_MHWIDTH];
   char  MHSpace[RSIZ_MHSPACE];
   char  MHParkName[RSIZ_MHPARKNAME];
   char  YearBuilt[RSIZ_YEARBUILT];
   char  Floor1[RSIZ_FLOOR1];
   char  Floor2[RSIZ_FLOOR1];
   char  Floor3[RSIZ_FLOOR1];
   char  Balcony[RSIZ_BALCONY];
   char  Porch[RSIZ_PORCH];
   char  Basement[RSIZ_BASEMENT];
   char  Addition1[RSIZ_BALCONY];
   char  Addition2[RSIZ_BALCONY];
   char  OtherArea[RSIZ_BALCONY];
   char  Gargage[RSIZ_BALCONY];
   char  Carport[RSIZ_BALCONY];
   char  Patio[RSIZ_BALCONY];
   char  EnclosedPatio[RSIZ_BALCONY];
   char  Pool[RSIZ_BALCONY];
   char  Deck[RSIZ_DECK];
   char  FP[RSIZ_FP];
   char  Beds[RSIZ_BEDS];
   char  FamilyRoom;
   char  DiningRoom;
   char  UtilityRoom;
   char  OtherRooms;
   char  TotalRooms[RSIZ_TOTALROOMS];
   char  Baths[RSIZ_BATHS];
   char  NumParcels[RSIZ_NUM_PARCELS];    // 538
   char  Filler1[RSIZ_FILLER1];
   char  DBA[RSIZ_DBA];
   char  Frontage[RSIZ_FRONTAGE];
   char  LotWidth[RSIZ_LOTWIDTH];
   char  LotDepth[RSIZ_LOTDEPTH];
   char  DockFac[RSIZ_DOCKFAC];
   char  RRSpur[RSIZ_RR_SPUR];
   char  Office[RSIZ_OFFICE_AREA];
   char  Mezzanine[RSIZ_MEZZANINE_AREA];
   char  Retail[RSIZ_RETAIL_AREA];
   char  Shop[RSIZ_SHOP_AREA];
   char  Storage[RSIZ_STORAGE_AREA];
   char  Warehouse[RSIZ_STORAGE_AREA];
   char  Filler2[RSIZ_FILLER2];
   char  Land[RSIZ_LANDVALUE];
   char  Impr[RSIZ_LANDVALUE];
   char  MineralValue[RSIZ_LANDVALUE];
   char  TreesValue[RSIZ_LANDVALUE];
   char  FixtureValue[RSIZ_LANDVALUE];
   char  PersonalValue[RSIZ_LANDVALUE];
   char  UnitTFValue[RSIZ_LANDVALUE];
   char  UnitPPValue[RSIZ_LANDVALUE];
   char  ExeCode1[RSIZ_EXECODE1];
   char  ExeValue1[RSIZ_EXEVALUE1];
   char  ExeCode2[RSIZ_EXECODE1];
   char  ExeValue2[RSIZ_EXEVALUE1];
   char  ExeCode3[RSIZ_EXECODE1];
   char  ExeValue3[RSIZ_EXEVALUE1];
   char  Filler3[19];
} VEN_ROLL;

typedef struct _tSiteCity
{
   char  SiteArea[4];
   char  City[16];
} SITE_CITY;

#define  SOFF_APN            1
#define  SOFF_APN_STATUS     11
#define  SOFF_TRA            12
#define  SOFF_NAME1          17
#define  SOFF_NAME2          53
#define  SOFF_OWN_CD1        89
#define  SOFF_OWN_CD2        90
#define  SOFF_MAIL_ADDR      91
#define  SOFF_CTY_STA        136
#define  SOFF_ZIP            160
#define  SOFF_DOC_NR         170
#define  SOFF_DOC_DT_Y2K     179
#define  SOFF_DOC_TYPE       187
#define  SOFF_ARC            191
#define  SOFF_SITUS_NR       200
#define  SOFF_SITUS_DIR      209
#define  SOFF_SITUS_STREET   211
#define  SOFF_SITUS_TYPE     236
#define  SOFF_SITUS_UNIT     238
#define  SOFF_SITE_USE       248
#define  SOFF_SA             252
#define  SOFF_NON_TAX_CD     254
#define  SOFF_SQ_FT_I        255
#define  SOFF_SQ_FT_L        263
#define  SOFF_LOTSQFT        255
#define  SOFF_BLDGSQFT       263
#define  SOFF_ACREAGE        274
#define  SOFF_TOT_UNITS      282
#define  SOFF_DOC_TAX        286
#define  SOFF_EFS_DT         293
#define  SOFF_SALE_DT        301

#define  SSIZ_APN            10
#define  SSIZ_APN_STATUS     1
#define  SSIZ_TRA            5
#define  SSIZ_NAME1          36
#define  SSIZ_NAME2          36
#define  SSIZ_OWN_CD1        1
#define  SSIZ_OWN_CD2        1
#define  SSIZ_MAIL_ADDR      45
#define  SSIZ_CTY_STA        24
#define  SSIZ_ZIP            10
#define  SSIZ_DOC_NR         9
#define  SSIZ_DOC_NR2        13
#define  SSIZ_DOC_TYPE       4
#define  SSIZ_ARC            9
#define  SSIZ_SITUS_NR       9
#define  SSIZ_SITUS_DIR      2
#define  SSIZ_SITUS_STREET   25
#define  SSIZ_SITUS_TYPE     2
#define  SSIZ_SITUS_UNIT     10
#define  SSIZ_SITE_USE       4
#define  SSIZ_SA             2
#define  SSIZ_NON_TAX_CD     1
#define  SSIZ_SQ_FT_I        8
#define  SSIZ_SQ_FT_L        11
#define  SSIZ_ACREAGE        8
#define  SSIZ_TOT_UNITS      4
#define  SSIZ_DOC_TAX        7
#define  SSIZ_DATE           8

// "VENTURA_PUBINFO-yyymmdd
typedef struct _tVenUpdt
{  // 310-bytes /w crlf
   char  Apn         [SSIZ_APN];
   char  Apn_Status  [SSIZ_APN_STATUS];
   char  TRA         [SSIZ_TRA];
   char  Name1       [SSIZ_NAME1];
   char  Name2       [SSIZ_NAME2];
   char  Own_Cd1     [SSIZ_OWN_CD1];
   char  Own_Cd2     [SSIZ_OWN_CD2];
   char  M_Addr1     [SSIZ_MAIL_ADDR];
   char  M_CtySt     [SSIZ_CTY_STA];
   char  M_Zip       [SSIZ_ZIP];
   char  DocNum      [SSIZ_DOC_NR];
   char  Doc_Dt      [SSIZ_DATE];
   char  DocType     [SSIZ_DOC_TYPE];
   char  RollChg_Nr  [SSIZ_ARC];
   char  Situs_Nr    [SSIZ_SITUS_NR];
   char  Situs_Dir   [SSIZ_SITUS_DIR];
   char  Situs_Street[SSIZ_SITUS_STREET];
   char  Situs_Type  [SSIZ_SITUS_TYPE];
   char  Situs_Unit  [SSIZ_SITUS_UNIT];
   char  Site_Use    [SSIZ_SITE_USE];
   char  Site_Area   [SSIZ_SA];
   char  NonTaxCode;
   char  BldgSqft    [SSIZ_SQ_FT_I];
   char  LotSqft     [SSIZ_SQ_FT_L];
   char  Acres       [SSIZ_ACREAGE];      // 9999.999
   char  Tot_Units   [SSIZ_TOT_UNITS];
   char  Doc_Tax     [SSIZ_DOC_TAX];
   char  Efs_Dt      [SSIZ_DATE];         // Trans start date
   char  SaleDate    [SSIZ_DATE];         // Last sale date
   char  CRLF        [2];
} VEN_UPDT;

// ParcelQuestDataExtract.txt
typedef struct _tVenUpdt2
{  // 350-bytes /w crlf
   char  Apn         [SSIZ_APN];          // 1
   char  Apn_Status  [SSIZ_APN_STATUS];   // 11
   char  TRA         [SSIZ_TRA];          // 12
   char  Name1       [SSIZ_NAME1];        // 17
   char  Name2       [SSIZ_NAME2];        // 53
   char  Own_Cd1     [SSIZ_OWN_CD1];      // 89
   char  Own_Cd2     [SSIZ_OWN_CD2];      // 90
   char  M_Addr1     [SSIZ_MAIL_ADDR];    // 91
   char  M_CtySt     [SSIZ_CTY_STA];      // 136
   char  M_Zip       [SSIZ_ZIP];          // 160
   char  DocNum      [SSIZ_DOC_NR2];      // 170
   char  filler      [37];
   char  Doc_Dt      [SSIZ_DATE];         // 220
   char  DocType     [SSIZ_DOC_TYPE];     // 228
   char  RollChg_Nr  [SSIZ_ARC];          // 232
   char  Situs_Nr    [SSIZ_SITUS_NR];     // 241
   char  Situs_Dir   [SSIZ_SITUS_DIR];    // 250
   char  Situs_Street[SSIZ_SITUS_STREET]; // 252
   char  Situs_Type  [SSIZ_SITUS_TYPE];   // 277
   char  Situs_Unit  [SSIZ_SITUS_UNIT];   // 279
   char  Site_Use    [SSIZ_SITE_USE];     // 289
   char  Site_Area   [SSIZ_SA];           // 293
   char  NonTaxCode;                      // 295
   char  BldgSqft    [SSIZ_SQ_FT_I];      // 296
   char  LotSqft     [SSIZ_SQ_FT_L];      // 304
   char  Acres       [SSIZ_ACREAGE];      // 315        9999.999
   char  Tot_Units   [SSIZ_TOT_UNITS];    // 323
   char  Doc_Tax     [SSIZ_DOC_TAX];      // 327
   char  Efs_Dt      [SSIZ_DATE];         // 330        Trans start date
   char  SaleDate    [SSIZ_DATE];         // 342        Last sale date
   char  CRLF        [2];
} VEN_UPDT2;

#define  GSIZ_DOCTYPE          10
typedef struct _tVenGrGr
{  // 160-bytes
   char  Apn         [RSIZ_APN];      // 1
   char  DocNum      [RSIZ_DOCNUM];   // 11
   char  DocDate     [RSIZ_DOC_DT];   // 20
   char  Name1       [SIZ_GR_NAME];   // 28
   char  Name2       [SIZ_GR_NAME];   // 78
   char  DocType     [GSIZ_DOCTYPE];  // 128
   char  Amount      [SIZ_GR_SALE];   // 138
   char  RecType;                     // 148 - O/E/B
   char  Company;                     // 149
   char  NoneSale_Flg;                // 150
   char  MultiApn;                    // 151 - Y/N
   char  filler      [6];             // 152 
   char  CRLF        [2];             // 158
} VEN_GRGR;

// Tax Secured tax file - P8102-01
#define  TSIZ_TRA              5
#define  TSIZ_APN              10
#define  TSIZ_NAME             29
#define  TSIZ_M_ADDRESS        28
#define  TSIZ_CITYSTATE        23
#define  TSIZ_ZIPCODE          5
#define  TSIZ_IMPR_VALUE       10
#define  TSIZ_VALUE            9
#define  TSIZ_DB06             9
#define  TSIZ_EXE_TYPE         1
#define  TSIZ_EXE              9
#define  TSIZ_PENASMNT         2
#define  TSIZ_MILLDROPTYPE90   7
#define  TSIZ_MILLDROPTYPE92   7
#define  TSIZ_MILLDROPTYPE98   7
#define  TSIZ_MAILINGCD        4
#define  TSIZ_LOANINFONO       25
#define  TSIZ_DEFAULTCODE      2
#define  TSIZ_TYPE90TAXRATE    7
#define  TSIZ_TYPE92TAXRATE    7
#define  TSIZ_TYPE98TAXRATE    7
#define  TSIZ_BILLNUMBER       7
#define  TSIZ_NETVALUE         9
#define  TSIZ_PENALTY          9
#define  TSIZ_MAILCODE         4
#define  TSIZ_FILLER           53

// Record type 1 - Basic change record
#define TB_TAX_RATE_AREA            1
#define TB_PARCEL_NUMBER            6
#define TB_M_NAME_1                 16     // Mail name
#define TB_M_NAME_2                 45
#define TB_M_ADDRESS                74
#define TB_M_CITYST                 102
#define TB_M_ZIP                    125
#define TB_LAND_VALUE               130
#define TB_MINERAL_VALUE            139
#define TB_IMPROVEMENT_VALUE        148
#define TB_TREES_VINES_VALUE        158
#define TB_PERSONAL_PROPERTY_VALUE  167
#define TB_DB06                     176
#define TB_EXEMPTION_1_TYPE         185
#define TB_EXEMPTION_1              186
#define TB_EXEMPTION_2_TYPE         195
#define TB_EXEMPTION_2              196
#define TB_EXEMPTION_3_TYPE         205
#define TB_EXEMPTION_3              206
#define TB_EXEMPTION_4_TYPE         215
#define TB_EXEMPTION_4              216
#define TB_TYPE_90_TAX              225    // Tax amount (1 inst)
#define TB_TYPE_92_TAX              236    // Land+Impr tax (1 inst)
#define TB_TYPE_98_TAX              247    // Special assessment amt - United Water Conservation
#define TB_PENALTY_ASSESSMENT       256    // Penalty amt
#define TB_MILL_DROP_TYPE_90        258    // Mill drop for Tax Amount
#define TB_MILL_DROP_TYPE_92        265    // Mill drop for Land & Improvement tax
#define TB_MILL_DROP_TYPE_98        272    // Mill drop for Special assessment amt
#define TB_DELINQUENT_CODE          279
#define TB_SR_CITIZENS_CODE         280
#define TB_CORTAC_MAILING_CD        281
#define TB_LOAN_INFO_NO             285
#define TB_DEFAULT_CODE             310
#define TB_FIRST_INSTALLMENT        312
#define TB_TYPE_90_TAX_RATE         325    // Tax Rate for Tax Amount
#define TB_TYPE_92_TAX_RATE         332
#define TB_TYPE_98_TAX_RATE         339
#define TB_GENERAL_COUNTY           346    // Total value
#define TB_FIRE                     355
#define TB_LIBRARY                  364
#define TB_FLOOD                    373
#define TB_SCHOOLS                  382
#define TB_CITY                     391
#define TB_SPECIAL_ASSESSMENT       400
#define TB_SPECIAL_DISTRICT         409
#define TB_STATEMENT_NUMBER_1       418
#define TB_ROLL_TYPE                425    // 2=Util, 1=Reg Sec
#define TB_O_NAME_1                 426    // Owner on record as of Jan 1st
#define TB_O_NAME_2                 455
#define TB_NET_VALUE                484    // Property net value
#define TB_PENALTY                  493
#define TB_MAIL_CODE                502
#define TB_FILLER                   506
#define TB_REC_TYPE                 550    // 1=Basic change record, 2=Special Assessment Record

// Record type 2 - Special Assessment Record
#define TB_DISTRICT_1               16
#define TB_ZONE_1                   18
#define TB_SPECIAL_ASMT_1           20
#define TB_DISTRICT_2               29
#define TB_ZONE_2                   31
#define TB_SPECIAL_ASMT_2           33
#define TB_DISTRICT_3               42
#define TB_ZONE_3                   44
#define TB_SPECIAL_ASMT_3           46
#define TB_DISTRICT_4               55
#define TB_ZONE_4                   57
#define TB_SPECIAL_ASMT_4           59
#define TB_DISTRICT_5               68
#define TB_ZONE_5                   70
#define TB_SPECIAL_ASMT_5           72
#define TB_DISTRICT_6               81
#define TB_ZONE_6                   83
#define TB_SPECIAL_ASMT_6           85
#define TB_DISTRICT_7               94
#define TB_ZONE_7                   96
#define TB_SPECIAL_ASMT_7           98
#define TB_DISTRICT_8               107
#define TB_ZONE_8                   109
#define TB_SPECIAL_ASMT_8           111
#define TB_DISTRICT_9               120
#define TB_ZONE_9                   122
#define TB_SPECIAL_ASMT_9           124
#define TB_DISTRICT_10              133
#define TB_ZONE_10                  135
#define TB_SPECIAL_ASMT_10          137
#define TB_DISTRICT_11              146
#define TB_ZONE_11                  148
#define TB_SPECIAL_ASMT_11          150
#define TB_DISTRICT_12              159
#define TB_ZONE_12                  161
#define TB_SPECIAL_ASMT_12          163
#define TB_DISTRICT_13              172
#define TB_ZONE_13                  174
#define TB_SPECIAL_ASMT_13          176
#define TB_DISTRICT_14              185
#define TB_ZONE_14                  187
#define TB_SPECIAL_ASMT_14          189
#define TB_DISTRICT_15              198
#define TB_ZONE_15                  200
#define TB_SPECIAL_ASMT_15          202
#define TB_DISTRICT_16              211
#define TB_ZONE_16                  213
#define TB_SPECIAL_ASMT_16          215
#define TB_DISTRICT_17              224
#define TB_ZONE_17                  226
#define TB_SPECIAL_ASMT_17          228
#define TB_DISTRICT_18              237
#define TB_ZONE_18                  239
#define TB_SPECIAL_ASMT_18          241
#define TB_DISTRICT_19              250
#define TB_ZONE_19                  252
#define TB_SPECIAL_ASMT_19          254
#define TB_DISTRICT_20              263
#define TB_ZONE_20                  265
#define TB_SPECIAL_ASMT_20          267
#define TB_DISTRICT_21              276
#define TB_ZONE_21                  278
#define TB_SPECIAL_ASMT_21          280
#define TB_DISTRICT_22              289
#define TB_ZONE_22                  291
#define TB_SPECIAL_ASMT_22          293
#define TB_DISTRICT_23              302
#define TB_ZONE_23                  304
#define TB_SPECIAL_ASMT_23          306
#define TB_DISTRICT_24              315
#define TB_ZONE_24                  317
#define TB_SPECIAL_ASMT_24          319
#define TB_DISTRICT_25              328
#define TB_ZONE_25                  330
#define TB_SPECIAL_ASMT_25          332
#define TB_DISTRICT_26              341
#define TB_ZONE_26                  343
#define TB_SPECIAL_ASMT_26          345
#define TB_DISTRICT_27              354
#define TB_ZONE_27                  356
#define TB_SPECIAL_ASMT_27          358
#define TB_DISTRICT_28              367
#define TB_ZONE_28                  369
#define TB_SPECIAL_ASMT_28          371
#define TB_DISTRICT_29              380
#define TB_ZONE_29                  382
#define TB_SPECIAL_ASMT_29          384
#define TB_DISTRICT_30              393
#define TB_ZONE_30                  395
#define TB_SPECIAL_ASMT_30          397
#define TB_DISTRICT_31              406
#define TB_ZONE_31                  408
#define TB_SPECIAL_ASMT_31          410
#define TB_DISTRICT_32              419
#define TB_ZONE_32                  421
#define TB_SPECIAL_ASMT_32          423
#define TB_DISTRICT_33              432
#define TB_ZONE_33                  434
#define TB_SPECIAL_ASMT_33          436
#define TB_DISTRICT_34              445
#define TB_ZONE_34                  447
#define TB_SPECIAL_ASMT_34          449
#define TB_DISTRICT_35              458
#define TB_ZONE_35                  460
#define TB_SPECIAL_ASMT_35          462
#define TB_DISTRICT_36              471
#define TB_ZONE_36                  473
#define TB_SPECIAL_ASMT_36          475
#define TB_DISTRICT_37              484
#define TB_ZONE_37                  486
#define TB_SPECIAL_ASMT_37          488
#define TB_STATEMENT_NUMBER_2       497
#define TB_SITUS_ADDRESS            504
#define TB_CORTAC_MAIL_CODE         536

#define TBS_TRA                     5
#define TBS_APN                     10
#define TBS_NAME                    29
#define TBS_M_ADDRESS               28
#define TBS_M_CITYST                23
#define TBS_M_ZIP                   5
#define TBS_VALUE                   9
#define TBS_DB06                    9
#define TBS_EXE_TYPE                1
#define TBS_EXE                     9
#define TBS_TYPE90TAX               11
#define TBS_TYPE92TAX               11
#define TBS_TYPE98TAX               9
#define TBS_PEN_ASMNT               2
#define TBS_MILL_DROP               7
#define TBS_DELINQUENT_CODE         1
#define TBS_SR_CITIZENS_CODE        1
#define TBS_CORTAC_MAILING_CD       4
#define TBS_LOAN_INFO_NO            25
#define TBS_DEFAULT_CODE            2
#define TBS_1ST_INST                13
#define TBS_TAX_RATE                7
#define TBS_GENERAL_COUNTY          9
#define TBS_FIRE                    9
#define TBS_LIBRARY                 9
#define TBS_FLOOD                   9
#define TBS_SCHOOLS                 9
#define TBS_CITY                    9
#define TBS_SPECIAL_ASSESSMENT      9
#define TBS_SPECIAL_DISTRICT        9
#define TBS_BILL_NUMBER             7
#define TBS_ROLL_TYPE               1
#define TBS_NET_VALUE               9
#define TBS_PENALTY                 9
#define TBS_MAILCODE                4
#define TBS_FILLER                  44
#define TBS_REC_TYPE                1

#define TBS_DISTRICT                2
#define TBS_ZONE                    2
#define TBS_SPECIAL_ASMT            9
#define TBS_SITUS_ADDRESS           32
#define TBS_MAX_ASMNT               37

typedef struct _t8102_01
{
   char  TRA                   [TBS_TRA             ];  // 1
   char  APN                   [TBS_APN             ];  // 6
   char  Name1                 [TBS_NAME            ];  // 16
   char  Name2                 [TBS_NAME            ];  // 45
   char  M_Address             [TBS_M_ADDRESS       ];  // 74
   char  CityState             [TBS_M_CITYST        ];  // 102
   char  ZipCode               [TBS_M_ZIP           ];  // 125
   char  LandValue             [TBS_VALUE           ];  // 130
   char  MineralValue          [TBS_VALUE           ];  // 139
   char  ImprValue             [TSIZ_IMPR_VALUE     ];  // 148
   char  TVValue               [TBS_VALUE           ];  // 158
   char  PPValue               [TBS_VALUE           ];  // 167
   char  DB06                  [TBS_DB06            ];  // 176
   char  Exemp1Type            [TBS_EXE_TYPE        ];  // 185
   char  Exemp1                [TBS_EXE             ];  // 186
   char  Exemp2Type            [TBS_EXE_TYPE        ];  // 195
   char  Exemp2                [TBS_EXE             ];  // 196
   char  Exemp3Type            [TBS_EXE_TYPE        ];  // 205
   char  Exemp3                [TBS_EXE             ];  // 206
   char  Exemp4Type            [TBS_EXE_TYPE        ];  // 215
   char  Exemp4                [TBS_EXE             ];  // 216
   char  Type90Tax             [TBS_TYPE90TAX       ];  // 225 - Tax amount (1 inst)
   char  Type92Tax             [TBS_TYPE92TAX       ];  // 236 - Land+Impr tax (1 inst)
   char  Type98Tax             [TBS_TYPE98TAX       ];  // 247 - Special assessment amt - United Water Conservation
   char  PenAsmnt              [TBS_PEN_ASMNT       ];  // 256 - Penalty amt
   char  MillDropType90        [TBS_MILL_DROP       ];  // 258 - Mill drop for Tax Amount
   char  MillDropType92        [TBS_MILL_DROP       ];  // 265 - Mill drop for Land & Improvement tax
   char  MillDropType98        [TBS_MILL_DROP       ];  // 272 - Mill drop for Special assessment amt
   char  DelqCode;                                      // 279
   char  SrCitizensCode;                                // 280
   char  CortacMailCd          [TBS_MAILCODE        ];  // 281
   char  LoanInfoNo            [TBS_LOAN_INFO_NO    ];  // 285
   char  DefaultCode           [TBS_DEFAULT_CODE    ];  // 310
   char  FirstInstallment      [TBS_1ST_INST        ];  // 312
   char  Type90TaxRate         [TBS_TAX_RATE        ];  // 325
   char  Type92TaxRate         [TBS_TAX_RATE        ];  // 332
   char  Type98TaxRate         [TBS_TAX_RATE        ];  // 339
   char  GeneralCounty         [TBS_VALUE           ];  // 346
   char  Fire                  [TBS_VALUE           ];  // 355
   char  Library               [TBS_VALUE           ];  // 364
   char  Flood                 [TBS_VALUE           ];  // 373
   char  Schools               [TBS_VALUE           ];  // 382
   char  City                  [TBS_VALUE           ];  // 391
   char  SpecialAsmnt          [TBS_VALUE           ];  // 400
   char  SpecialDistrict       [TBS_VALUE           ];  // 409
   char  BillNumber            [TBS_BILL_NUMBER     ];  // 418
   char  RollType;                                      // 425 - 2=Util, 1=Reg Sec
   char  LienOwner1            [TBS_NAME            ];  // 426
   char  LienOwner2            [TBS_NAME            ];  // 455
   char  NetValue              [TBS_VALUE           ];  // 484
   char  Penalty               [TBS_PENALTY         ];  // 493
   char  MailCode              [TBS_MAILCODE        ];  // 502
   char  Filler                [TBS_FILLER          ];  // 506
   char  RecType;                                       // 550 - 1=Basic change record, 2=Special Assessment Record
} VEN_SECTAX;

typedef struct _tSpecialAsmnt
{
   char  Dist[TBS_DISTRICT];
   char  Zone[TBS_ZONE];
   char  Amt[TBS_SPECIAL_ASMT];
} VEN_ASMNT;
typedef struct _t8102_02
{
   char  TRA                   [TBS_TRA];               // 1
   char  APN                   [TBS_APN];               // 6
   VEN_ASMNT asAsmnts          [TBS_MAX_ASMNT];         // 16 - 13*37=481
   char  BillNum               [TBS_BILL_NUMBER];       // 497
   char  Situs                 [TBS_SITUS_ADDRESS];     // 504
   char  MailCode              [TBS_MAILCODE];          // 536
   char  Filler                [10];                    // 540
   char  RecType;                                       // 550 - 2=Special Assessment Record
} VEN_ASMNTS;

#define  TR_RECTYPE           1
#define  TR_APN               3
#define  TR_NAME1             13
#define  TR_NAME2             49
#define  TR_S_STRNUM          85
#define  TR_S_STRALPHA        91
#define  TR_S_STRDIR          94
#define  TR_S_STRNAME         96
#define  TR_S_STRSFX          121
#define  TR_S_UNIT            123
#define  TR_S_CATEGORY        128
#define  TR_M_ADDRESS         133
#define  TR_M_CITYST          178
#define  TR_M_ZIP             202
#define  TR_DFLT_ASSRNR       212
#define  TR_DATE_PAID         222
#define  TR_AMOUNT_PAID       230
#define  TR_DELQ_AMOUNT       241
#define  TR_DELQ_DATE_BAD     252
#define  TR_DELQ_DATE_PAID    260
#define  TR_REMARKS           268

#define  TRS_RECTYPE          2
#define  TRS_APN              10
#define  TRS_NAME             36
#define  TRS_S_STRNUM         6
#define  TRS_S_STRALPHA       3
#define  TRS_S_STRDIR         2
#define  TRS_S_STRNAME        25
#define  TRS_S_STRSFX         2
#define  TRS_S_UNIT           5
#define  TRS_S_CATEGORY       5
#define  TRS_M_ADDRESS        45
#define  TRS_M_CITYST         24
#define  TRS_M_ZIP            10
#define  TRS_DFLT_ASSRNR      10
#define  TRS_DATE             8
#define  TRS_AMOUNT           11
#define  TRS_REMARKS          60
#define  TRS_YEAR             4
#define  TRS_DFLT90           11
#define  TRS_DFLT92           8
#define  TRS_DFLT93           9
#define  TRS_MAX_ENTRY        8

typedef struct _t8111_72_01
{
   char  RecType       [TRS_RECTYPE     ];
   char  APN           [TRS_APN         ];
   char  Name1         [TRS_NAME        ];
   char  Name2         [TRS_NAME        ];
   char  S_StrNum      [TRS_S_STRNUM    ];
   char  S_StrAlpha    [TRS_S_STRALPHA  ];
   char  S_StrDir      [TRS_S_STRDIR    ];
   char  S_StrName     [TRS_S_STRNAME   ];
   char  S_StrSfx      [TRS_S_STRSFX    ];
   char  S_Unit        [TRS_S_UNIT      ];
   char  S_Category    [TRS_S_CATEGORY  ];
   char  M_Address     [TRS_M_ADDRESS   ];
   char  M_CitySt      [TRS_M_CITYST    ];
   char  M_Zip         [TRS_M_ZIP       ];
   char  Dflt_AssrNr   [TRS_DFLT_ASSRNR ];  // 212
   char  Date_Paid     [TRS_DATE        ];  // 222
   char  Amount_Paid   [TRS_AMOUNT      ];  // 230
   char  Delq_Amount   [TRS_AMOUNT      ];  // 241
   char  Delq_Date_Bad [TRS_DATE        ];  // 252
   char  Delq_Date_Paid[TRS_DATE        ];  // 260
   char  Remarks       [TRS_REMARKS     ];
} VEN_RDMPT;

typedef struct _t8111_72_02
{
   char  RecType       [TRS_RECTYPE     ];  // 1
   char  APN           [TRS_APN         ];  // 3
   char  TaxYear       [TRS_YEAR        ];  // 13
   char  Dflt_90       [TRS_DFLT90      ];  // 17
   char  Dflt_92       [TRS_DFLT92      ];  // 28
   char  asDflt93      [TRS_MAX_ENTRY][TRS_DFLT93];
   char  Inst_Code;
   char  Filler        [220];
} VEN_DFLT;

// All parcels with at least 1 installment paid - P8111-71
#define TA_TAX_RATE_AREA            1 -1
#define TA_PARCEL_NUMBER            6 -1
#define TA_STATEMENT_NUMBER         16-1
#define TA_FIRST_INSTALLMENT        22-1  // 1st Installment of Taxes Due
#define TA_FIRST_PEN_AMT            33-1  // 1st Installment of Penalty Amount
#define TA_SECOND_INSTALLMENT       42-1  // 2nd Installment of Taxes Due
#define TA_SECOND_PEN_AMT           53-1  // 2nd Installment of Penalty Amount
#define TA_COST                     62-1  // Cost due w/2nd Penalty Amount
#define TA_FIRST_PAID_DATE          66-1  // Date 1st installment Paid
#define TA_FIRST_PAID_CODE          74-1  // 1=Paid on Time, 2=Paid w/Penalty
#define TA_SECOND_PAID_DATE         75-1  // Date 2nd installment Paid
#define TA_SECOND_PAID_CODE         83-1  // 1=Paid on Time, 2=Paid w/Penalty + Cost

#define TAS_BILL_NUMBER             6
#define TAS_INSTAMT                 11
#define TAS_PENALTY                 9
#define TAS_SECOND_INSTALLMENT      11
#define TAS_SECOND_PEN_AMT          9
#define TAS_COST                    4
#define TAS_PAID_DATE               8
#define TAS_FILLER                  6
typedef struct _t8111_71
{
   char  TRA                   [TBS_TRA];
   char  APN                   [TBS_APN];
   char  BillNum               [TAS_BILL_NUMBER];  // trailing zero is being dropped
   char  InstAmt1              [TAS_INSTAMT];
   char  PenAmt1               [TAS_PENALTY];
   char  InstAmt2              [TAS_INSTAMT];
   char  PenAmt2               [TAS_PENALTY];
   char  Cost                  [TAS_COST];         // Cost due w/2nd Penalty Amount
   char  PaidDate1             [TAS_PAID_DATE];
   char  PaidCode1;                                // 1=Paid on Time, 2=Paid with Penalty
   char  PaidDate2             [TAS_PAID_DATE];
   char  PaidCode2;                                // 1=Paid on Time, 2=Paid w/Penalty + Cost
   char  Filler                [TAS_FILLER];
   char  RecType;
} VEN_PAID;

// Active parcels with tax amount paid - P8111-73
#define TP_PARCEL_NUMBER            1-1
#define TP_INSTALLMENT_CODE         12-1  // 0=Unpaid,1=1st Paid, 2=2nd Paid
#define TP_1ST_INST_AMT             14-1
#define TP_2ND_INST_AMT             26-1
#define TP_STATEMENT_NUMBER         38-1
#define TP_CODE                     45-1  // 04056

#define TPS_PARCEL_NUMBER           10
#define TPS_INSTALLMENT_CODE        1
#define TPS_INST_AMT                11
#define TPS_STATEMENT_NUMBER        6
#define TPS_CODE                    5

// Active parcels with the tax amount due - P8111-74
#define TD_CODE                     4-1   // 04056
#define TD_PARCEL_NUMBER            17-1
#define TD_INSTALLMENT_CODE         31-1  // 0=Unpaid,1=1st Paid, 2=2nd Paid
#define TD_1ST_INST_AMT             32-1
#define TD_2ND_INST_AMT             43-1
#define TD_1ST_DELQ_DATE            54-1
#define TD_2ND_DELQ_DATE            62-1
#define TD_STATEMENT_NUMBER         74-1

#define  TDS_FILLER1                3
#define  TDS_CODE                   5
#define  TDS_FILLER2                8
#define  TDS_APN                    10
#define  TDS_FILLER3                4
#define  TDS_INSTCODE               1
#define  TDS_INSTAMT                11
#define  TDS_DUE_DATE               8
#define  TDS_FILLER4                4
#define  TDS_STMNT_NO               6
#define  TDS_FILLER5                1

typedef struct _t8111_74
{
   char  Filler1     [TDS_FILLER1];
   char  Code        [TDS_CODE];
   char  Filler2     [TDS_FILLER2];
   char  APN         [TDS_APN];
   char  Filler3     [TDS_FILLER3];
   char  InstCode    [TDS_INSTCODE];  // 0=Unpaid,1=1st Paid, 2=2nd Paid
   char  InstAmt1    [TDS_INSTAMT];
   char  InstAmt2    [TDS_INSTAMT];
   char  DueDate1    [TDS_DUE_DATE];  // 1st ARC delinquent date
   char  DueDate2    [TDS_DUE_DATE];  // 2nd ARC delinquent date
   char  Filler4     [TDS_FILLER4];
   char  BillNum     [TDS_STMNT_NO];
   char  Filler5     [TDS_FILLER5];
} VEN_DUE;

//P8105-09: Supplemental Secured Billing FTP File
//   - APN, Value Info, Owner Info, Supp Tax Info.
#define  TS_APN                1
#define  TS_ACCT               11
#define  TS_LINE_NO            16
#define  TS_SEC_UNSEC          25
#define  TS_CANCEL             26
#define  TS_COR_PROC           27
#define  TS_TRA                28
#define  TS_OWNER_1            33
#define  TS_OWNER_2            69
#define  TS_MAIL_ADDRESS       105
#define  TS_CTY_STATE          150
#define  TS_ZIP_OWNER          174
#define  TS_DOC_NBR            179
#define  TS_DOC_DATE           188
#define  TS_EFF_DOC_DATE       196
#define  TS_PERMIT_DATE        204
#define  TS_DT_POST            212
#define  TS_HSE_NBR            220
#define  TS_SUFF               226
#define  TS_DIR                229
#define  TS_ST_NAME            231
#define  TS_SUPPL_TYPE         256
#define  TS_CATEG              258
#define  TS_SUPPL_UNIT         263
#define  TS_SUPPLE_AREA        268
#define  TS_TAX_YEAR           270
#define  TS_DT_VAL_NOTCE       276
#define  TS_CHG_OWN_DT         284
#define  TS_APL_DT             292
#define  TS_APL_DT_MNT         300
#define  TS_DT_VAL_MNT         308
#define  TS_DT_VAL_TRF         316
#define  TS_RPT_DT             324
#define  TS_DT_SUP_ROLL        332
#define  TS_DT_SUP_BILL        340
#define  TS_SUPP_STMT_NR       348
#define  TS_PRORATION_FAC      354
#define  TS_CRE_CD_1           359
#define  TS_CRE_VAL_1          361
#define  TS_CRE_CD_2           370
#define  TS_CRE_VAL_2          372
#define  TS_CRE_CD_3           381
#define  TS_CRE_VAL_3          383
#define  TS_CRV_LAND_VAL       392
#define  TS_CRV_IMP_VAL        401
#define  TS_CRV_MIN_RTS_VAL    410
#define  TS_CRV_TREE_VAL       419
#define  TS_CRV_TRADE_VAL      428
#define  TS_CRV_UNIT_TF_VAL    437
#define  TS_PROCESS_DATE       446
#define  TS_NO_DAYS_TAXED      454
#define  TS_NO_DAYS_OWNED      457
#define  TS_M_CODE             460
#define  TS_ADJ_TAX_AMT        461
#define  TS_TAX_RATE_YEAR      466
#define  TS_TRAN_CD            472
#define  TS_HOF                473
#define  TS_NONTAX             475
#define  TS_PC                 476
#define  TS_B_Y_CD_1           478
#define  TS_B_Y_VAL_1          480
#define  TS_B_Y_CD_2           489
#define  TS_B_Y_VAL_2          491
#define  TS_B_Y_CD_3           500
#define  TS_B_Y_VAL_3          502
#define  TS_B_Y_LAND_VAL       511
#define  TS_B_Y_IMP_VAL        520
#define  TS_B_Y_MR_VAL         529
#define  TS_B_Y_TV_VAL         538
#define  TS_B_Y_TF_VAL         547
#define  TS_B_Y_UNIT_TF_VAL    556
#define  TS_CUM_CD_1           565
#define  TS_CUM_VAL_1          567
#define  TS_CUM_CD_2           576
#define  TS_CUM_VAL_2          578
#define  TS_CUM_CD_3           587
#define  TS_CUM_VAL_3          589
#define  TS_CUM_LAND_VAL       598
#define  TS_CUM_IMP_VAL        607
#define  TS_CUM_MR_VAL         616
#define  TS_CUM_TV_VAL         625
#define  TS_CUM_TF_VAL         634
#define  TS_CUM_UNIT_TF_VAL    643
#define  TS_SUPP_CD_1          652
#define  TS_SUPP_VAL_1         654
#define  TS_SUPP_CD_2          663
#define  TS_SUPP_VAL_2         665
#define  TS_SUPP_CD_3          674
#define  TS_SUPP_VAL_3         676
#define  TS_SUPP_LAND_VAL      685
#define  TS_SUPP_IMP_VAL       694
#define  TS_SUPP_MR_VAL        703
#define  TS_SUPP_TV_VAL        712
#define  TS_SUPP_TF_VAL        721
#define  TS_SUPP_UNIT_TF_VAL   730
#define  TS_TOTAL_VALUE        739
#define  TS_TYPE_90            748
#define  TS_TYPE_92            759
#define  TS_INST_1             770
#define  TS_INST_2             781
#define  TS_RATE_90_R          792
#define  TS_RATE_92_R          799
#define  TS_DEL_DT_INST1       806
#define  TS_DEL_DT_INST2       814
#define  TS_DT_PD_INST1        822
#define  TS_DT_PD_INST2        830
#define  TS_TOT_TAX_EXEM       838
#define  TS_COST               847
#define  TS_BILL_DATE          851
#define  TS_DUE_DATE_PAY1      859
#define  TS_DUE_DATE_PAY2      867
#define  TS_PEN90_1NST1        875
#define  TS_PEN90_2ND          884
#define  TS_PEN92_1NST1        893
#define  TS_PEN92_2ND          902
#define  TS_STMT_NO_R          911
#define  TS_PROCESS_CODE       917
#define  TS_NEG_FLAG           918
#define  TS_PRO_TAX            919
#define  TS_DUE_DATE_PAY_U     928
#define  TS_PAY_CD_INST1       936
#define  TS_NO_DAYS_OWNED_X    937
#define  TS_LAND_NEG_FLAG      940
#define  TS_SUPP_IMP_NEG_FLAG  941
#define  TS_SUPP_MR_NEG_FLAG   942
#define  TS_SUPP_TV_NEG_FLAG   943
#define  TS_SUPP_TF_NEG_FLAG   944
#define  TS_Filler             945

#define  TSS_APN               10
#define  TSS_ACCT              5
#define  TSS_LINE_NO           9
#define  TSS_SEC_UNSEC         1
#define  TSS_CANCEL            1
#define  TSS_COR_PROC          1
#define  TSS_TRA               5
#define  TSS_OWNER_1           36
#define  TSS_OWNER_2           36
#define  TSS_MAIL_ADDRESS      45
#define  TSS_CTY_STATE         24
#define  TSS_ZIP_OWNER         5
#define  TSS_DOC_NBR           9
#define  TSS_DOC_DATE          8
#define  TSS_EFF_DOC_DATE      8
#define  TSS_PERMIT_DATE       8
#define  TSS_DT_POST           8
#define  TSS_HSE_NBR           6
#define  TSS_SUFF              3
#define  TSS_DIR               2
#define  TSS_ST_NAME           25
#define  TSS_SUPPL_TYPE        2
#define  TSS_CATEG             5
#define  TSS_SUPPL_UNIT        5
#define  TSS_SUPPLE_AREA       2
#define  TSS_TAX_YEAR          6
#define  TSS_DT_VAL_NOTCE      8
#define  TSS_CHG_OWN_DT        8
#define  TSS_APL_DT            8
#define  TSS_APL_DT_MNT        8
#define  TSS_DT_VAL_MNT        8
#define  TSS_DT_VAL_TRF        8
#define  TSS_RPT_DT            8
#define  TSS_DT_SUP_ROLL       8
#define  TSS_DT_SUP_BILL       8
#define  TSS_SUPP_STMT_NR      6
#define  TSS_PRORATION_FAC     5
#define  TSS_CRE_CD_1          2
#define  TSS_CRE_VAL_1         9
#define  TSS_CRE_CD_2          2
#define  TSS_CRE_VAL_2         9
#define  TSS_CRE_CD_3          2
#define  TSS_CRE_VAL_3         9
#define  TSS_CRV_LAND_VAL      9
#define  TSS_CRV_IMP_VAL       9
#define  TSS_CRV_MIN_RTS_VAL   9
#define  TSS_CRV_TREE_VAL      9
#define  TSS_CRV_TRADE_VAL     9
#define  TSS_CRV_UNIT_TF_VAL   9
#define  TSS_PROCESS_DATE      8
#define  TSS_NO_DAYS_TAXED     3
#define  TSS_NO_DAYS_OWNED     3
#define  TSS_M_CODE            1
#define  TSS_ADJ_TAX_AMT       5
#define  TSS_TAX_RATE_YEAR     6
#define  TSS_TRAN_CD           1
#define  TSS_HOF               2
#define  TSS_NONTAX            1
#define  TSS_PC                2
#define  TSS_B_Y_CD_1          2
#define  TSS_B_Y_VAL_1         9
#define  TSS_B_Y_CD_2          2
#define  TSS_B_Y_VAL_2         9
#define  TSS_B_Y_CD_3          2
#define  TSS_B_Y_VAL_3         9
#define  TSS_B_Y_LAND_VAL      9
#define  TSS_B_Y_IMP_VAL       9
#define  TSS_B_Y_MR_VAL        9
#define  TSS_B_Y_TV_VAL        9
#define  TSS_B_Y_TF_VAL        9
#define  TSS_B_Y_UNIT_TF_VAL   9
#define  TSS_CUM_CD_1          2
#define  TSS_CUM_VAL_1         9
#define  TSS_CUM_CD_2          2
#define  TSS_CUM_VAL_2         9
#define  TSS_CUM_CD_3          2
#define  TSS_CUM_VAL_3         9
#define  TSS_CUM_LAND_VAL      9
#define  TSS_CUM_IMP_VAL       9
#define  TSS_CUM_MR_VAL        9
#define  TSS_CUM_TV_VAL        9
#define  TSS_CUM_TF_VAL        9
#define  TSS_CUM_UNIT_TF_VAL   9
#define  TSS_SUPP_CD_1         2
#define  TSS_SUPP_VAL_1        9
#define  TSS_SUPP_CD_2         2
#define  TSS_SUPP_VAL_2        9
#define  TSS_SUPP_CD_3         2
#define  TSS_SUPP_VAL_3        9
#define  TSS_SUPP_LAND_VAL     9
#define  TSS_SUPP_IMP_VAL      9
#define  TSS_SUPP_MR_VAL       9
#define  TSS_SUPP_TV_VAL       9
#define  TSS_SUPP_TF_VAL       9
#define  TSS_SUPP_UNIT_TF_VAL  9
#define  TSS_TOTAL_VALUE       9
#define  TSS_TYPE_90           11
#define  TSS_TYPE_92           11
#define  TSS_INST_1            11
#define  TSS_INST_2            11
#define  TSS_RATE_90_R         7
#define  TSS_RATE_92_R         7
#define  TSS_DEL_DT_INST1      8
#define  TSS_DEL_DT_INST2      8
#define  TSS_DT_PD_INST1       8
#define  TSS_DT_PD_INST2       8
#define  TSS_TOT_TAX_EXEM      9
#define  TSS_COST              4
#define  TSS_BILL_DATE         8
#define  TSS_DUE_DATE_PAY1     8
#define  TSS_DUE_DATE_PAY2     8
#define  TSS_PEN90_1NST1       9
#define  TSS_PEN90_2ND         9
#define  TSS_PEN92_1NST1       9
#define  TSS_PEN92_2ND         9
#define  TSS_STMT_NO_R         6
#define  TSS_PROCESS_CODE      1
#define  TSS_NEG_FLAG          1
#define  TSS_PRO_TAX           9
#define  TSS_DUE_DATE_PAY_U    8
#define  TSS_PAY_CD_INST1      1
#define  TSS_NO_DAYS_OWNED_X   3
#define  TSS_LAND_NEG_FLAG     1
#define  TSS_SUPP_IMP_NEG_FLAG 1
#define  TSS_SUPP_MR_NEG_FLAG  1
#define  TSS_SUPP_TV_NEG_FLAG  1
#define  TSS_SUPP_TF_NEG_FLAG  1
#define  TSS_FILLER            4

typedef struct _t8105_09
{
   char  APN              [TSS_APN              ];  // Parcel number
   char  Acct             [TSS_ACCT             ];  // Account number
   char  Line_No          [TSS_LINE_NO          ];  // Line number
   char  Sec_Unsec        [TSS_SEC_UNSEC        ];  // Secured/Unsecured Flag
   char  Cancel           [TSS_CANCEL           ];  // Cancel flag
   char  Cor_Proc         [TSS_COR_PROC         ];  // Corrected Process Flag
   char  TRA              [TSS_TRA              ];  // Tax Rate Area
   char  Owner_1          [TSS_OWNER_1          ];  // Owner Name 1
   char  Owner_2          [TSS_OWNER_2          ];  // Owner Name 2
   char  Mail_Address     [TSS_MAIL_ADDRESS     ];  // Mailing Address
   char  Cty_State        [TSS_CTY_STATE        ];  // City, State
   char  Zip_Owner        [TSS_ZIP_OWNER        ];  // Zip Code
   char  Doc_Nbr          [TSS_DOC_NBR          ];  // Document number
   char  Doc_Date         [TSS_DOC_DATE         ];  // Document date
   char  Eff_Doc_Date     [TSS_EFF_DOC_DATE     ];  // Effective Document Date
   char  Permit_Date      [TSS_PERMIT_DATE      ];  // Permit Date
   char  Dt_Post          [TSS_DT_POST          ];  // Date Posted
   char  Hse_Nbr          [TSS_HSE_NBR          ];  // House number
   char  Suff             [TSS_SUFF             ];  // Suffix
   char  Dir              [TSS_DIR              ];  // Direction
   char  St_Name          [TSS_ST_NAME          ];  // Street name
   char  Suppl_Type       [TSS_SUPPL_TYPE       ];  // Street type, Ave, blvd
   char  Categ            [TSS_CATEG            ];  // Category
   char  Suppl_Unit       [TSS_SUPPL_UNIT       ];  // Supplemental Unit
   char  Supple_Area      [TSS_SUPPLE_AREA      ];  // Supplemental Area
   char  Tax_Year         [TSS_TAX_YEAR         ];  // Tax year
   char  Dt_Val_Notce     [TSS_DT_VAL_NOTCE     ];  // Date Valuation Notice
   char  Chg_Own_Dt       [TSS_CHG_OWN_DT       ];  // Date Owner Changed
   char  Apl_Dt           [TSS_APL_DT           ];  // Application Date
   char  Apl_Dt_Mnt       [TSS_APL_DT_MNT       ];  // Application Date
   char  Dt_Val_Mnt       [TSS_DT_VAL_MNT       ];  // Date Value Maintenance
   char  Dt_Val_Trf       [TSS_DT_VAL_TRF       ];  // Date Value Transferred
   char  Rpt_Dt           [TSS_RPT_DT           ];  // Report Date
   char  Dt_Sup_Roll      [TSS_DT_SUP_ROLL      ];  // Date Supplemental Roll
   char  Dt_Sup_Bill      [TSS_DT_SUP_BILL      ];  // Date Supplemental Bill
   char  Supp_Stmt_Nr     [TSS_SUPP_STMT_NR     ];  // Supplemental Statement Number
   char  Proration_Fac    [TSS_PRORATION_FAC    ];  // Proration Factor
   char  Cre_Cd_1         [TSS_CRE_CD_1         ];  // Code 1
   char  Cre_Val_1        [TSS_CRE_VAL_1        ];  // Value 1
   char  Cre_Cd_2         [TSS_CRE_CD_2         ];  // Code 2
   char  Cre_Val_2        [TSS_CRE_VAL_2        ];  // Value 2
   char  Cre_Cd_3         [TSS_CRE_CD_3         ];  // Code 3
   char  Cre_Val_3        [TSS_CRE_VAL_3        ];  // Value 3
   char  Crv_Land_Val     [TSS_CRV_LAND_VAL     ];  // Land Value
   char  Crv_Imp_Val      [TSS_CRV_IMP_VAL      ];  // Improvements Value
   char  Crv_Min_Rts_Val  [TSS_CRV_MIN_RTS_VAL  ];  // Mineral Rights Value
   char  Crv_Tree_Val     [TSS_CRV_TREE_VAL     ];  // Trees Value
   char  Crv_Trade_Val    [TSS_CRV_TRADE_VAL    ];  // Trade Fixtures Value
   char  Crv_Unit_Tf_Val  [TSS_CRV_UNIT_TF_VAL  ];  // Unit Trade Fix. Value
   char  Process_Date     [TSS_PROCESS_DATE     ];  // Process Date
   char  No_Days_Taxed    [TSS_NO_DAYS_TAXED    ];  // Number Days Taxed
   char  No_Days_Owned    [TSS_NO_DAYS_OWNED    ];  // Number Days Owned
   char  M_Code           [TSS_M_CODE           ];  // M Code
   char  Adj_Tax_Amt      [TSS_ADJ_TAX_AMT      ];  // Adjusted Tax Amount
   char  Tax_Rate_Year    [TSS_TAX_RATE_YEAR    ];  // Tax Rate Year
   char  Tran_Cd          [TSS_TRAN_CD          ];  // Transaction Code
   char  Hof              [TSS_HOF              ];  // Home Owners Flag
   char  Nontax           [TSS_NONTAX           ];  // Non Tax Flag
   char  Pc               [TSS_PC               ];  // PC code
   char  B_Y_Cd_1         [TSS_B_Y_CD_1         ];  // Base year Code 1
   char  B_Y_Val_1        [TSS_B_Y_VAL_1        ];  // Base Year Value 1
   char  B_Y_Cd_2         [TSS_B_Y_CD_2         ];  // Base year Code 2
   char  B_Y_Val_2        [TSS_B_Y_VAL_2        ];  // Base Year Value 2
   char  B_Y_Cd_3         [TSS_B_Y_CD_3         ];  // Base year Code 3
   char  B_Y_Val_3        [TSS_B_Y_VAL_3        ];  // Base Year Value 3
   char  B_Y_Land_Val     [TSS_B_Y_LAND_VAL     ];  // Base Year Land Value
   char  B_Y_Imp_Val      [TSS_B_Y_IMP_VAL      ];  // Base Year Improvements Value
   char  B_Y_Mr_Val       [TSS_B_Y_MR_VAL       ];  // Base Year Min Rights Value
   char  B_Y_Tv_Val       [TSS_B_Y_TV_VAL       ];  // Base Year Trees  Value
   char  B_Y_Tf_Val       [TSS_B_Y_TF_VAL       ];  // Base Year Trade Fix Value
   char  B_Y_Unit_Tf_Val  [TSS_B_Y_UNIT_TF_VAL  ];  // Base Year Unit Trade Fix Value
   char  Cum_Cd_1         [TSS_CUM_CD_1         ];  // Cumulative
   char  Cum_Val_1        [TSS_CUM_VAL_1        ];  // Cumulative
   char  Cum_Cd_2         [TSS_CUM_CD_2         ];  // Cumulative
   char  Cum_Val_2        [TSS_CUM_VAL_2        ];  // Cumulative
   char  Cum_Cd_3         [TSS_CUM_CD_3         ];  // Cumulative
   char  Cum_Val_3        [TSS_CUM_VAL_3        ];  // Cumulative
   char  Cum_Land_Val     [TSS_CUM_LAND_VAL     ];  // Cumulative
   char  Cum_Imp_Val      [TSS_CUM_IMP_VAL      ];  // Cumulative
   char  Cum_Mr_Val       [TSS_CUM_MR_VAL       ];  // Cumulative
   char  Cum_Tv_Val       [TSS_CUM_TV_VAL       ];  // Cumulative
   char  Cum_Tf_Val       [TSS_CUM_TF_VAL       ];  // Cumulative
   char  Cum_Unit_Tf_Val  [TSS_CUM_UNIT_TF_VAL  ];  // Cumulative
   char  Supp_Cd_1        [TSS_SUPP_CD_1        ];  // Supplemental
   char  Supp_Val_1       [TSS_SUPP_VAL_1       ];  // Supplemental
   char  Supp_Cd_2        [TSS_SUPP_CD_2        ];  // Supplemental
   char  Supp_Val_2       [TSS_SUPP_VAL_2       ];  // Supplemental
   char  Supp_Cd_3        [TSS_SUPP_CD_3        ];  // Supplemental
   char  Supp_Val_3       [TSS_SUPP_VAL_3       ];  // Supplemental
   char  Supp_Land_Val    [TSS_SUPP_LAND_VAL    ];  // Supplemental
   char  Supp_Imp_Val     [TSS_SUPP_IMP_VAL     ];  // Supplemental
   char  Supp_Mr_Val      [TSS_SUPP_MR_VAL      ];  // Supplemental
   char  Supp_Tv_Val      [TSS_SUPP_TV_VAL      ];  // Supplemental
   char  Supp_Tf_Val      [TSS_SUPP_TF_VAL      ];  // Supplemental
   char  Supp_Unit_Tf_Val [TSS_SUPP_UNIT_TF_VAL ];  // Supplemental
   char  Total_Value      [TSS_TOTAL_VALUE      ];  // Total Value
   char  Type90Tax        [TSS_TYPE_90          ];  // Type 90 Tax
   char  Type92Tax        [TSS_TYPE_92          ];  // Type 92 Tax
   char  InstAmt1         [TSS_INST_1           ];  // Tax Installment 1
   char  InstAmt2         [TSS_INST_2           ];  // Tax Installment 2
   char  Type90TaxRate    [TSS_RATE_90_R        ];  // Type 90 Rate
   char  Type92TaxRate    [TSS_RATE_92_R        ];  // Type 92 Rate
   char  DelqDate1        [TSS_DEL_DT_INST1     ];  // Delinquent Date Installment 1
   char  DelqDate2        [TSS_DEL_DT_INST2     ];  // Delinquent Date Installment 2
   char  PaidDate1        [TSS_DT_PD_INST1      ];  // Date Paid Installment 1
   char  PaidDate2        [TSS_DT_PD_INST2      ];  // Date Paid Installment 2
   char  Tot_Tax_Exem     [TSS_TOT_TAX_EXEM     ];  // Total Tax Exemption
   char  Cost             [TSS_COST             ];  // Cost
   char  Bill_Date        [TSS_BILL_DATE        ];  // Billing Date
   char  DueDate1         [TSS_DUE_DATE_PAY1    ];  // Due Date Payment 1
   char  DueDate2         [TSS_DUE_DATE_PAY2    ];  // Due Date Payment 2
   char  Pen90_Amt1       [TSS_PEN90_1NST1      ];  // Penalty Type 90, Instal. 1
   char  Pen90_Amt2       [TSS_PEN90_2ND        ];  // Penalty Type 90, Instal. 2
   char  Pen92_Amt1       [TSS_PEN92_1NST1      ];  // Penalty Type 92, Instal. 1
   char  Pen92_Amt2       [TSS_PEN92_2ND        ];  // Penalty Type 92, Instal. 2
   char  BillNumber       [TSS_STMT_NO_R        ];  // Statement Number
   char  Process_Code     [TSS_PROCESS_CODE     ];  // Process Code
   char  Neg_Flag         [TSS_NEG_FLAG         ];  // Negative Flag
   char  Pro_Tax          [TSS_PRO_TAX          ];  // Prorated Tax
   char  Due_Date_Pay_U   [TSS_DUE_DATE_PAY_U   ];  // Due Date Payment Unsecured
   char  Pay_Cd_Inst1     [TSS_PAY_CD_INST1     ];  // Pay Code Instal. 1
   char  No_Days_Owned_X  [TSS_NO_DAYS_OWNED_X  ];  // Number Days Owned
   char  Land_Neg_Flag    [TSS_LAND_NEG_FLAG    ];  // Negative Land Flag
   char  Supp_Imp_Neg_Flag[TSS_SUPP_IMP_NEG_FLAG];  // Negative Improvement Flag
   char  Supp_Mr_Neg_Flag [TSS_SUPP_MR_NEG_FLAG ];  // Negative Mineral Rights Flag
   char  Supp_Tv_Neg_Flag [TSS_SUPP_TV_NEG_FLAG ];  // Negative Trees-Vines Flag
   char  Supp_Tf_Neg_Flag [TSS_SUPP_TF_NEG_FLAG ];  // Negative Trade Fixture Flag
   char  Filler           [TSS_FILLER           ];
} VEN_SUPP;

// P8105-05: Supplemental Secured Tax payment status
#define  TSP_TRA               1
#define  TSP_APN               6
#define  TSP_BILLNUM           16
#define  TSP_SEQ_NO_1ST        22
#define  TSP_INST_AMT1         29
#define  TSP_INST_PEN1         40
#define  TSP_SEQ_NO_2ND        49
#define  TSP_INST_AMT2         56
#define  TSP_INST_PEN2         67
#define  TSP_COST              76
#define  TSP_DATE_PAID1        80
#define  TSP_DATE_PAID2        88
#define  TSP_FILLER            96

#define  TSPS_TRA              5
#define  TSPS_APN              10
#define  TSPS_BILLNUM          6
#define  TSPS_SEQ_NO_1ST       7
#define  TSPS_INST_AMT1        11
#define  TSPS_INST_PEN1        9
#define  TSPS_SEQ_NO_2ND       7
#define  TSPS_INST_AMT2        11
#define  TSPS_INST_PEN2        9
#define  TSPS_COST             4
#define  TSPS_DATE_PAID1       8
#define  TSPS_DATE_PAID2       8
#define  TSPS_FILLER           5

typedef struct _t8105_05
{
   char  TRA          [TSPS_TRA       ];  // 1  Tax Rate Area
   char  APN          [TSPS_APN       ];  // 6  Parcel, APN
   char  BillNum      [TSPS_BILLNUM   ];  // 16 Number Assigned to the Bill
   char  Seq_No_1St   [TSPS_SEQ_NO_1ST];  // 22 Internal Tax Office Payment ID 1st Install
   char  InstAmt1     [TSPS_INST_AMT1 ];  // 29 1st Installment of Taxes Due
   char  PenAmt1      [TSPS_INST_PEN1 ];  // 40 1st Installment of Penalty Amount
   char  Seq_No_2Nd   [TSPS_SEQ_NO_2ND];  // 49 Internal Tax Office Payment ID 2nd Install
   char  InstAmt2     [TSPS_INST_AMT2 ];  // 56 2nd Installment of Taxes Due
   char  PenAmt2      [TSPS_INST_PEN2 ];  // 67 2nd Installment of Penalty Amount
   char  Cost         [TSPS_COST      ];  // 76 Cost due w/2nd Penalty Amount
   char  PaidDate1    [TSPS_DATE_PAID1];  // 80 Date 1st installment Paid
   char  PaidDate2    [TSPS_DATE_PAID2];  // 88 Date 2nd installment Paid
   char  Filler       [TSPS_FILLER    ];  // 96
} VEN_PSUP;

// Z8103-31: Supplemental Redemption
#define  TSR_BILLNUM            1
#define  TSR_TRA                8
#define  TSR_APN                13
#define  TSR_ACCOUNT            24
#define  TSR_LINE_NUMBER        29
#define  TSR_TYPE_90_INST1      40
#define  TSR_TYPE_92_INST1      49
#define  TSR_TYPE_90_INST2      58
#define  TSR_TYPE_92_INST2      67
#define  TSR_TYPE_90_PEN1       76
#define  TSR_TYPE_90_PEN2       85
#define  TSR_TYPE_92_PEN1       94
#define  TSR_TYPE_92_PEN2       103
#define  TSR_COST               112
#define  TSR_DATE_PAID1         117
#define  TSR_DATE_PAID2         125
#define  TSR_DATE_BILLED        133
#define  TSR_EFF_DATE           141
#define  TSR_DATE_DELQ_INST1    149
#define  TSR_DATE_DELQ_INST2    157
#define  TSR_PROCESS_DATE       165
#define  TSR_SEQ_NUMBER_1       173
#define  TSR_SEQ_NUMBER_2       180
#define  TSR_OWNER_NAME_1       187
#define  TSR_APPT_FLAG_1        223
#define  TSR_APPT_FLAG_2        224
#define  TSR_INST_CODE_1        225
#define  TSR_INST_CODE_2        226
#define  TSR_REFUND_CORRECTION  227
#define  TSR_SEC_UNSEC_CODE     228
#define  TSR_CORRECTION_RECORD  229
#define  TSR_DELETE_CODE        230
#define  TSR_UNSEC_PENALTY      231
#define  TSR_DAYS_OWNED         240
#define  TSR_RED_FEE            243
#define  TSR_ROLL_YEAR          252
#define  TSR_DFLT_YEAR          258
#define  TSR_CALAMITY           264
#define  TSR_DFLT_DATE          265
#define  TSR_PREV_APN           273
#define  TSR_PREV_TRA           284
#define  TSR_DFLT_APN           289
#define  TSR_SEC_RED_FLAG       300
#define  TSR_RBP_FLAG           301
#define  TSR_RED__CORR_FLAG     302
#define  TSR_INTEREST           303
#define  TSR_DFLT_AMT           312
#define  TSR_PREV_BILLNUM       321
#define  TSR_DEPOSIT_PERMIT     328
#define  TSR_NSF                334
#define  TSR_MONTHLY_INT        337
#define  TSR_PREV_RED_BILLNUM   346

#define  TSRS_BILLNUM           7
#define  TSRS_TRA               5
#define  TSRS_APN               11
#define  TSRS_ACCOUNT           5
#define  TSRS_LINE_NUMBER       11
#define  TSRS_TYPE_90_INST1     9
#define  TSRS_TYPE_92_INST1     9
#define  TSRS_TYPE_90_INST2     9
#define  TSRS_TYPE_92_INST2     9
#define  TSRS_TYPE_90_PEN1      9
#define  TSRS_TYPE_90_PEN2      9
#define  TSRS_TYPE_92_PEN1      9
#define  TSRS_TYPE_92_PEN2      9
#define  TSRS_COST              5
#define  TSRS_DATE_PAID1        8
#define  TSRS_DATE_PAID2        8
#define  TSRS_DATE_BILLED       8
#define  TSRS_EFF_DATE          8
#define  TSRS_DATE_DELQ_INST1   8
#define  TSRS_DATE_DELQ_INST2   8
#define  TSRS_PROCESS_DATE      8
#define  TSRS_SEQ_NUMBER_1      7
#define  TSRS_SEQ_NUMBER_2      7
#define  TSRS_OWNER_NAME_1      36
#define  TSRS_APPT_FLAG_1       1
#define  TSRS_APPT_FLAG_2       1
#define  TSRS_INST_CODE_1       1
#define  TSRS_INST_CODE_2       1
#define  TSRS_REFUND_CORRECTION 1
#define  TSRS_SEC_UNSEC_CODE    1
#define  TSRS_CORRECTION_RECORD 1
#define  TSRS_DELETE_CODE       1
#define  TSRS_UNSEC_PENALTY     9
#define  TSRS_DAYS_OWNED        3
#define  TSRS_RED_FEE           9
#define  TSRS_ROLL_YEAR         6
#define  TSRS_DFLT_YEAR         6
#define  TSRS_CALAMITY          1
#define  TSRS_DFLT_DATE         8
#define  TSRS_PREV_APN          11
#define  TSRS_PREV_TRA          5
#define  TSRS_DFLT_APN          11
#define  TSRS_SEC_RED_FLAG      1
#define  TSRS_RBP_FLAG          1
#define  TSRS_RED_CORR_FLAG     1
#define  TSRS_INTEREST          9
#define  TSRS_DFLT_AMT          9
#define  TSRS_PREV_BILLNUM      7
#define  TSRS_DEPOSIT_PERMIT    6
#define  TSRS_NSF               3
#define  TSRS_MONTHLY_INT       9
#define  TSRS_PREV_RED_BILLNUM  7

typedef struct _t8103_31
{
   char  BillNum            [TSRS_BILLNUM          ];
   char  TRA                [TSRS_TRA              ];
   char  APN                [TSRS_APN              ];
   char  Account            [TSRS_ACCOUNT          ];
   char  Line_Number        [TSRS_LINE_NUMBER      ];
   char  Type_90_Inst1      [TSRS_TYPE_90_INST1    ];
   char  Type_92_Inst1      [TSRS_TYPE_92_INST1    ];
   char  Type_90_Inst2      [TSRS_TYPE_90_INST2    ];
   char  Type_92_Inst2      [TSRS_TYPE_92_INST2    ];
   char  Type_90_Pen1       [TSRS_TYPE_90_PEN1     ];
   char  Type_90_Pen2       [TSRS_TYPE_90_PEN2     ];
   char  Type_92_Pen1       [TSRS_TYPE_92_PEN1     ];
   char  Type_92_Pen2       [TSRS_TYPE_92_PEN2     ];
   char  Cost               [TSRS_COST             ];
   char  Date_Paid1         [TSRS_DATE_PAID1       ];
   char  Date_Paid2         [TSRS_DATE_PAID2       ];
   char  Date_Billed        [TSRS_DATE_BILLED      ];
   char  Eff_Date           [TSRS_EFF_DATE         ];
   char  Date_delq_Inst1    [TSRS_DATE_DELQ_INST1  ];
   char  Date_delq_Inst2    [TSRS_DATE_DELQ_INST2  ];
   char  Process_Date       [TSRS_PROCESS_DATE     ];
   char  Seq_Number_1       [TSRS_SEQ_NUMBER_1     ];
   char  Seq_Number_2       [TSRS_SEQ_NUMBER_2     ];
   char  Owner_Name_1       [TSRS_OWNER_NAME_1     ];
   char  Appt_Flag_1        [TSRS_APPT_FLAG_1      ];
   char  Appt_Flag_2        [TSRS_APPT_FLAG_2      ];
   char  Inst_Code_1        [TSRS_INST_CODE_1      ];
   char  Inst_Code_2        [TSRS_INST_CODE_2      ];
   char  Refund_Correction  [TSRS_REFUND_CORRECTION];
   char  Sec_Unsec_Code     [TSRS_SEC_UNSEC_CODE   ];
   char  Correction_Record  [TSRS_CORRECTION_RECORD];
   char  Delete_Code        [TSRS_DELETE_CODE      ];
   char  Unsec_Penalty      [TSRS_UNSEC_PENALTY    ];
   char  Days_Owned         [TSRS_DAYS_OWNED       ];
   char  Red_Fee            [TSRS_RED_FEE          ];
   char  Roll_Year          [TSRS_ROLL_YEAR        ];  // Roll Year (CCYYYY  ie.199899)
   char  Dflt_Year          [TSRS_DFLT_YEAR        ];  // Default Year (CCYYYY  ie.199899)
   char  Calamity           [TSRS_CALAMITY         ];
   char  Dflt_Date          [TSRS_DFLT_DATE        ];
   char  Prev_APN           [TSRS_PREV_APN         ];
   char  Prev_TRA           [TSRS_PREV_TRA         ];
   char  Dflt_APN           [TSRS_DFLT_APN         ];
   char  Sec_Red_Flag       [TSRS_SEC_RED_FLAG     ];
   char  RBP_Flag           [TSRS_RBP_FLAG         ];  // Roll  Being Prepared Flag
   char  Red_Corr_Flag      [TSRS_RED_CORR_FLAG   ];
   char  Interest           [TSRS_INTEREST         ];  // Interest Amount (9999999.99)
   char  Dflt_Amt           [TSRS_DFLT_AMT         ];  // Default Amount (9999999.99)
   char  Prev_BillNum       [TSRS_PREV_BILLNUM     ];  // Previous Statement number
   char  Deposit_Permit     [TSRS_DEPOSIT_PERMIT   ];
   char  NSF                [TSRS_NSF              ];
   char  Monthly_Int        [TSRS_MONTHLY_INT      ];  // Monthly Interest Amount (9999999.99)
   char  Prev_Red_BillNum   [TSRS_PREV_RED_BILLNUM ];
} VEN_RSUP;

// 11/16/2023 New tax file layouts
// VenRpt-30 Secured Billing File
#define SB_TRA                      0
#define SB_APN                      1
#define SB_BILLNUM                  2
#define SB_BILLREV                  3     // Bill Revision
#define SB_1ST_INST_AMT             4
#define SB_1ST_INST_PENAMT          5     // Penalty amount for late payment of 1st installment
#define SB_1ST_INST_PAIDAMT         6
#define SB_1ST_INST_BALAMT          7     // Balance of tax due on 1st installment
#define SB_1ST_INST_PAIDDATE        8     // Date of most recently payment - CCYYMMDD
#define SB_1ST_INST_PAYCODE         9     // 0=Unpaid,1=Partially Paid,3=Fully paid on time, 4=Fully paid including penalty
#define SB_2ND_INST_AMT             10
#define SB_2ND_INST_PENAMT          11
#define SB_2ND_INST_PAIDAMT         12
#define SB_COST_AMT                 13    // Cost due with 2nd penalty amount
#define SB_2ND_INST_BALAMT          14
#define SB_2ND_INST_PAIDDATE        15
#define SB_2ND_INST_PAYCODE         16    // 0=Unpaid,1=Partially Paid,3=Fully paid on time, 4=Fully paid including penalty
#define SB_COLS                     17

// New tax layout for "VenRpt-40 Secured Extended Roll" - 11/16/2023
// Type=1
#define ER1_RECTYPE                 0
#define ER1_TRA                     1
#define ER1_APN                     2
#define ER1_S_ADDR                  3
#define ER1_M_NAME1                 4     // Mailing name1
#define ER1_M_NAME2                 5
#define ER1_M_ADDR1                 6
#define ER1_M_CITY                  7
#define ER1_M_STATE                 8
#define ER1_M_ZIP                   9
#define ER1_LAND                    10    // Value of land (dollars)
#define ER1_MINERAL                 11    // Mineral Value (dollars)
#define ER1_IMPR                    12    // Value of Improvement (dollars)
#define ER1_TREEVINES               13    // Value of Trees and Vines (dollars)
#define ER1_PERSPROP                14    // Value of Personal Property (dollars)
#define ER1_EXE1_TYPE               15    // Exemption 1 flag
                                          // 1 - CEMETERIES,
                                          // 4 - CHURCH,
                                          // 5 - RELIGIOUS,
                                          // 6 - COLLEGE,
                                          // 7 - WELFARE,
                                          // 8 - OTHER, CONSOLATE,
                                          // 9 - HOMEOWNER,
                                          // 10 - DISABLED VETERAN
                                          // 12 - HISTORICAL AIRCRAFT,
                                          // 18 - HOSPITAL,
                                          // 19 - MUSEUM,
                                          // 20 - PRIVATE SCHOOL,
                                          // 22 - PUBLIC SCHOOL,
                                          // 24 - MULTI)
#define ER1_EXE1                    16    // Exemption 1 Value (dollars,decimal,cents)
#define ER1_EXE2_TYPE               17
#define ER1_EXE2                    18
#define ER1_EXE3_TYPE               19
#define ER1_EXE3                    20
#define ER1_EXE4_TYPE               21
#define ER1_EXE4                    22
#define ER1_TYPE_90_TAX             23    // Tax Amount (dollars,decimal,cents)
#define ER1_TYPE_92_TAX             24    // Land & Improvement tax (dollars,decimal,cents)
#define ER1_TYPE_98_TAX             25    // Special Assessment amount (dollars,decimal,cents)
#define ER1_PEN_AMT                 26
#define ER1_SR_CODE                 27    // Sr. Citizens Tax Deferred (not provided)
#define ER1_MAIL_CODE               28
#define ER1_LOAN_INFO               29
#define ER1_1ST_INST_AMT            30
#define ER1_TYPE_90_TAXRATE         31    // Tax Rate for Tax Amount (X.XXXXXX)
#define ER1_TYPE_92_TAXRATE         32    // Tax Rate for Land & Improvement tax (X.XXXXXX)
#define ER1_TYPE_98_TAXRATE         33    // Tax Rate for Special Assessment amount (X.XXXXXX)
#define ER1_BILLNUM                 34
#define ER1_BILLREV                 35
#define ER1_ROLL_TYPE               36    // 3=Util  1=Reg Sec
#define ER1_NAME1                   37    // Owner of record on Jan 1st
#define ER1_NAME2                   38
#define ER1_NET_VALUE               39    // Property Net Value
#define ER1_PENALTY                 40
#define ER1_COLS                    41

// Type=2
#define ER2_RECTYPE                 0
#define ER2_TRA                     1
#define ER2_APN                     2
#define ER2_FUNDNUM                 3     // Direct Assessment Subfund number
#define ER2_ASST_AMT                4     // Direct Assessment Value (dollars, decimal,cents)
#define ER2_COUNT                   16    // Number of funds
#define ER2_COLS                    35

// New tax layout for "VenRpt-10 Redemption Redemption Billing File" - 01/10/2025
// New tax layout for "VenRpt-15 Redemption-Supplemental Redemption Billing File" - 01/10/2025
// Type=1
#define RB1_RECTYPE                 0     // "1"
#define RB1_TRA                     1
#define RB1_APN                     2
#define RB1_NAME1                   3
#define RB1_NAME2                   4
#define RB1_M_ADDR1                 5
#define RB1_M_CITY                  6
#define RB1_M_STATE                 7
#define RB1_M_ZIP                   8
#define RB1_S_STRNUM                9
#define RB1_S_STREET                10
#define RB1_S_CITY                  11
#define RB1_TAX_DUE                 12    // Amount of taxes due (dollars,decimal,cents) (zero if paid) 
                                          // (if unpaid, should total the sum of Defaulted taxes of all 
                                          // record types 2 plus redemption fee)
#define RB1_TAX_PAID                13    // Amount of taxes paid (dollars,decimal,cents)
#define RB1_PAID_DATE               14
#define RB1_REMARKS                 15    // Remarks, such as "Payment Plan", "Defaulted Payment Plan", "Bankruptcy", "Power to Sell"
#define RB1_COLS                    16

//#define RB1_RECTYPE                 0     // "1"
//#define RB1_TRA                     1
//#define RB1_APN                     2
//#define RB1_S_ADDR                  3
//#define RB1_NAME1                   4
//#define RB1_NAME2                   5
//#define RB1_M_ADDR1                 6
//#define RB1_M_CITY                  7
//#define RB1_M_STATE                 8
//#define RB1_M_ZIP                   9
//#define RB1_S_STRNUM                10
//#define RB1_S_STREET                11
//#define RB1_S_CITY                  12
//#define RB1_TAX_DUE                 13    // Amount of taxes due (dollars,decimal,cents) (zero if paid) 
//                                          // (if unpaid, should total the sum of Defaulted taxes of all 
//                                          // record types 2 plus redemption fee)
//#define RB1_TAX_PAID                14    // Amount of taxes paid (dollars,decimal,cents)
//#define RB1_PAID_DATE               15
//#define RB1_REMARKS                 16    // Remarks, such as "Payment Plan", "Defaulted Payment Plan", "Bankruptcy", "Power to Sell"
//#define RB1_COLS                    17

// Type=2
#define RB2_RECTYPE                 0     // "2"
#define RB2_APN                     1
#define RB2_YEAR                    2     // CCYY - year of original tax bill
#define RB2_BILLNUM                 3     // Oustanding bill number for the given year
#define RB2_BILLREV                 4
#define RB2_TYPE_90_DEFAMT          5     // Amount of Type 90 taxes that are unpaid (dollars,decimal,cents)
#define RB2_TYPE_92_DEFAMT          6
#define RB2_TYPE_98_DEFAMT          7
#define RB2_COLS                    8

// New tax layout for "VenRpt-50 Secured Supplemental Billing File" - 11/16/2023
//#define SSB_TRA                     0
//#define SSB_APN                     1
//#define SSB_BILLNUM                 2
//#define SSB_BILLREV                 3     // Bill Revision
//#define SSB_1ST_INST_AMT            4
//#define SSB_1ST_INST_PENAMT         5     // Penalty amount for late payment of 1st installment
//#define SSB_COST                    6     // Cost amount rolled over from secured roll (dollars,decimal,cents)
//#define SSB_1ST_INST_PAIDAMT        7
//#define SSB_1ST_INST_BALAMT         8     // Balance of tax due on 1st installment
//#define SSB_1ST_INST_PAIDDATE       9     // Date of last payment - CCYYMMDD
//#define SSB_2ND_INST_AMT            10
//#define SSB_2ND_INST_PENAMT         11    // Penalty amount for late payment of 2nd installment
//#define SSB_2ND_INST_PAIDAMT        12
//#define SSB_2ND_INST_BALAMT         13    // Balance remaining on 2nd installment (dollars,decimal,cents)
//#define SSB_2ND_INST_PAIDDATE       14    // Date of last payment - CCYYMMDD
//#define SSB_COLS                    15

// VenRpt-50-Secured Supplemental Billing File v2 - 04/10/2024
//#define SSB_TRA                     0
//#define SSB_APN                     1
//#define SSB_BILLNUM                 2
//#define SSB_BILLREV                 3     // Bill Revision
//#define SSB_1ST_INST_AMT            4
//#define SSB_1ST_INST_PENAMT         5     // Penalty amount for late payment of 1st installment
//#define SSB_COST                    6     // Cost amount rolled over from secured roll (dollars,decimal,cents)
//#define SSB_1ST_INST_DUEDATE        7     // 1st installment due date CCYYMMDD
//#define SSB_1ST_INST_PAIDAMT        8     // 1st installment due date
//#define SSB_1ST_INST_BALAMT         9     // Balance of tax due on 1st installment
//#define SSB_1ST_INST_PAIDDATE       10    // Date of last payment - CCYYMMDD
//#define SSB_2ND_INST_AMT            11
//#define SSB_2ND_INST_PENAMT         12    // Penalty amount for late payment of 2nd installment
//#define SSB_2ND_INST_DUEDATE        13    // 2nd installment due date - CCYYMMDD
//#define SSB_2ND_INST_PAIDAMT        14
//#define SSB_2ND_INST_BALAMT         15    // Balance remaining on 2nd installment (dollars,decimal,cents)
//#define SSB_2ND_INST_PAIDDATE       16    // Date of last payment - CCYYMMDD
//#define SSB_COLS                    17

// VenRpt-50-Secured Supplemental Billing File v3 - 06/10/2024
#define SSB_TRA                     0
#define SSB_APN                     1
#define SSB_BILLNUM                 2
#define SSB_BILLREV                 3     // Bill Revision
#define SSB_TAXYEAR                 4
#define SSB_EFFTAXYEAR              5
#define SSB_1ST_INST_AMT            6
#define SSB_1ST_INST_PENAMT         7     // Penalty amount for late payment of 1st installment
#define SSB_COST                    8     // Cost amount rolled over from secured roll (dollars,decimal,cents)
#define SSB_1ST_INST_DUEDATE        9     // 1st installment due date CCYYMMDD
#define SSB_1ST_INST_PAIDAMT        10    // 1st installment due date
#define SSB_1ST_INST_BALAMT         11    // Balance of tax due on 1st installment
#define SSB_1ST_INST_PAIDDATE       12    // Date of last payment - CCYYMMDD
#define SSB_2ND_INST_AMT            13
#define SSB_2ND_INST_PENAMT         14    // Penalty amount for late payment of 2nd installment
#define SSB_2ND_INST_DUEDATE        15    // 2nd installment due date - CCYYMMDD
#define SSB_2ND_INST_PAIDAMT        16
#define SSB_2ND_INST_BALAMT         17    // Balance remaining on 2nd installment (dollars,decimal,cents)
#define SSB_2ND_INST_PAIDDATE       18    // Date of last payment - CCYYMMDD
#define SSB_COLS                    19

// Standard codes
// H=Homeowners
// D=Disabled Veterans
// V=Veterans
// C=Church
// L=Library
// M=Museums
// R=Religion
// T=Tribal Housing
// W=Welfare
// U=College
// P=Public School
// E=Cemetery
// I=Hospital
// S=School
// X=Misc exempt
// Y=Military

IDX_TBL4 VEN_Exemption[] = 
{
   "HOM", "H", 3,1,
   "DIS", "D", 3,1,
   "VET", "V", 3,1,
   "CHU", "C", 3,1,
   "COL", "U", 3,1,
   "HOS", "I", 3,1,
   "PRI", "S", 3,1,
   "PUB", "P", 3,1,
   "REL", "R", 3,1,
   "MUS", "M", 3,1,
   "CEM", "E", 3,1,
   "WEL", "W", 3,1,
   "OTH", "X", 3,1,
   "","",0,0
};

#endif