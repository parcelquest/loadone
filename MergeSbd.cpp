/*****************************************************************************
 *
 * Notes:
 *    - May need to extract CHARS and sales from old file to new file
 *    - SBD is no longer sending us legal info.  We have to pull it from 2008.
 *
 * Options:
 *    -CSBD -L -Ma -Mg -Xl -Xd -Mr (load lien)
 *    -CSBD -U -Mg[i] [-Ig] -Mr -[X|M]a -Xo -[X|M]s (load update)
 *
 * Revision
 * 07/18/2006 1.2.27.3  Test version
 * 03/21/2008 1.5.7.1   Use standard function to update usecode.  
 * 07/16/2008 8.0.3     Modify Sbd_CleanStr(), Sbd_MergeMAdr(), rename CreateSbdRoll()
 *                      to Sbd_Load_LDR() and fix for 2008 LDR.  Roll file rec now
 *                      may spread over multiple lines.
 * 01/17/2009 8.5.6     Fix TRA
 * 07/13/2009 9.1.2     Add Sbd_MergeLegal() & Sbd_LegalExtr() to pull legal from 2008 LDR
 *                      and merge into 2009.  Add other value to R01 rec.
 * 02/18/2010 9.4.2     Merge long LEGAL into LEGAL1 & LEGAL2 (max legal is 2805 chars).
 * 07/12/2010 10.1.1    Modify Sbd_MergeRoll() to fix wrong CountyID in R01 file.  
 *                      Modify Sbd_MergeLegal() to use updateLegal() to capture as much
 *                      as posible. Clean up Sbd_LegalExtr().  Add -Xd option to extract legal file.
 * 07/21/2010 10.1.5    Set FULL_EXEMPT flag.  If SBD_ROLL_TAX_STAT <> 1, set FULL_EXEMPT='Y'.
 * 08/04/2010 10.1.9    Remove bad characters from LEGAL and change output format for Sbd_Legal.txt
 * 12/23/2010 10.4.0    Add Sbd_Load_Roll(), Sbd_MergeUpdRoll(), Sbd_MergeSAdr1(), Sbd_MergeMAdr1(), 
 *                      Sbd_MergeOwner1() to support updated roll.
 * 12/28/2010           Update roll has only bill owner.  So update owner name only if 
 *                      owner name has been changed or new property.  Adding Sbd_ExtractOwners()
 *                      to extract owner name and Sbd_MergeOwnerX() to verify owner name before update.
 * 07/13/2011 11.0.3    Add S_HSENO.  Activate Merge Legal in Sbd_Load_LDR().
 * 07/09/2012 12.1.1    Add APN to updateStdUse().
 * 10/30/2012 12.3.2    Add -Mg option using Sbd_MergeDocExt() to merge data from county website to R01.
 * 11/08/2012 12.3.3    Update SBD_DocType[]
 * 07/10/2013 13.0.2    Fix bad chars in Legal, CareOf, Owners, and Situs Street name.
 *                      Require resort R01 to make sure output file is aorted properly.
 * 10/04/2013 13.10.2   Fix double title in owner name and add vesting to R01.
 * 10/13/2013 13.11.1   Modify Sbd_MergeOwner() to fix owner issue. Modify Sbd_MergeMAdr() to 
 *                      accept mailing as parameter.  Modify Sbd_MergeRoll() to fix misalignment
 *                      problem. Add Sbd_MergeCharRec() to replace MergeCharRec() in CharRec.cpp
 *                      since this one is SBD specific.
 * 02/19/2014 13.12.1   Add -Ig option to import GRGR data into SQL.
 * 08/28/2015 15.1.3    Add -Mr option to add lot sqft & lot area
 * 09/13/2015 15.2.2    Standardize MergeArea.
 * 09/23/2015 15.2.4    Modify to support new update with sale and characteristics data (quaterly)
 * 10/09/2015 15.3.0    Add Sbd_ConvStdChar(), Sbd_MergeChar(), Sbd_Load_Roll(), Sbd_MergeUpdRoll()
 *                      to support new data from SBD.
 * 02/03/2016 15.5.1    Modify SBD_DocCode[] to use in Sbd_MergeDocRec() instead of SBD_DocType[].
 *            15.5.1.2  Fix MapLink bug in Sbd_MergeUpdRoll() and a bug in Sbd_MergeDocRec()
 * 02/16/2016 15.6.0    Modify Sbd_MergeOwner1() to remove old names before applying new one,
 *                      Keep swap name the same as Owner1 if there is vesting.
 * 03/08/2016 15.8.0    Clean up unused variables.
 * 04/05/2016 15.5.2    Modify all sale formatting functions to add DocNum and Seller names.
 * 06/25/2016 16.0.0    Add option to load TC data.
 * 07/07/2016 16.0.4    Modify Sbd_MergeUpdRoll() to update lot sqft & acres only when data avail.
 *                      not set value of 0.
 * 08/19/2016 16.2.1    Modify Sbd_ExtractOwners() to extract owner name from OWNER.TRANSFER.TXT
 *                      Add -Xo option to extract oener name.  Load TaxAgency list to add TaxCode.
 * 09/29/2016 16.2.9    Fix bad char in LEGAL in Sbd_MergeRoll() & Sbd_MergeUpdRoll()
 * 12/07/2016 16.7.7    Modify Sbd_Fmt%Sale() to populate Full/Partial sale
 * 12/10/2016 16.8.0    Automatically load CHAR and SALE.
 * 01/16/2017 16.9.4    Add Sbd_Load_TaxRoll() & Sbd_Load_TaxDetail() to process new tax file.
 * 02/09/2017 16.10.5   Fix AIR_COND translation bug in Sbd_ConvStdChar()
 * 03/28/2017 16.13.1   Remove BillNum from Tax_Base in Sbd_ParseTaxBase() since we don't have
 *                      matching one in Tax_Items.  Automatic check for new TC file to run.
 *                      Use TC_LoadTaxAgencyAndDetail() to replace Sbd_Load_TaxDetail()
 *                      Remove duplicate entries in DIST.TC before processing.
 * 04/03/2017 16.13.6   Use Load_TC() instead of Sbd_Load_TaxRoll() since SBD.TC is more up to date.
 * 05/02/2017 16.14.7   Add Sbd_MergeOwnerXfer() to merge xfer info into R01. Modify Sbd_Load_Roll()
 *                      to update xfer info. Fix bug in Sbd_ExtractOwners() & Sbd_MergeDocExt()
 *                      If there is new owner file, re-extract it automatically.
 * 05/10/2017 16.14.10  Modify Sbd_ExtractOwners() to ignore bad DocNum
 * 05/12/2017 16.14.11  Add Sbd_ExtrChar() to extract XC file for bulk clients
 * 05/24/2017 16.14.13  Fix bug in LoadSbd() that always tries to extract CHAR then exit.
 * 02/01/2018 17.6.4    Modify Sbd_ExtrSale() to extract all sale file except MP.  
 *                      Create Sbd_ExtrMPSale() to handle MP sales since only first record contains sale info.
 * 02/05/2018 17.6.5    When new sale file is loaded, automatically import sale.
 * 04/05/2018 17.8.1    Modify Sbd_Fmt???Sale() change sale code from 'I' to 'W', 'U' to 'P'
 * 04/28/2018 17.10.4   Modify Sbd_FmtSfrSale() to set NoneSale_Flg from SBD_DocType[]
 * 05/23/2018 17.11.1   Turn off updating Items table
 * 06/13/2018 17.12.2   Fix Sbd_MergeDocRec() to update all sale info.
 * 08/14/2018 18.4.0    Replace R01.MergeArea() with PQ_MergeLotArea() using new SBD_Basemap.txt
 * 09/23/2018 18.5.0    Modify Sbd_MergeSAdr1() to cleanup S_UNITNO and populate S_UNITNOX.
 * 09/29/2018 18.5.1    Move -Mr option to LoadOne.cpp
 * 02/12/2019 18.9.3    Modify Sbd_MergeRoll() to include inactive parcels.
 * 04/11/2019 18.11.2   Adding SBD_ExtrTRI() to extract tax data from county file TRP.BDROM.TRB02.BIN.
 *                      Modify Load_TaxBase() & ParseTaxBase() to use new tax extract to create Tax_Base.
 * 04/20/2019 18.11.5   Add Sbd_Load_TaxDelq() and modify Sbd_Load_TaxDetail() to sort agency list before import.
 * 04/29/2019 18.11.8   Modify Sbd_MergeDocRec() to use IDX_SALE DocType
 * 05/20/2019 18.12.5   Modify Sbd_Load_TaxDelq() to load new DELQ file TR345PC.TXT
 * 05/21/2019 18.12.6   Fix tax amt in Sbd_ParseTaxDelq().  Remove BillNum in Sbd_ParseTaxBase() since
 *                      there is no BillNum in detail record.
 * 07/06/2019 19.0.2    Modify Sbd_MergeMAdr() to update DBA.  Modify Sbd_MergeRoll() to pass DBA to Sbd_MergeMAdr().
 *                      Add Sbd_ExtrProp8() for -X8 option. Add Sbd_MergeProp8() to update Prop8 flag.
 *                      Modify Sbd_Load_LDR() to use new Prop8 file to update R01 record.  Sort LDR file
 *                      before calling Sbd_Load_LDR() to avoid record out of sync.
 * 11/24/2019 19.5.7    Revise tax processing to include supplemental and properly set BillNum for current tax bill.
 * 11/28/2019 19.5.8    Revise Sbd_ParseTaxDelq() to ignore records older than 2000.
 * 02/23/2020 19.7.0    Add support for loading NDC sale data.
 * 02/25/2020 19.7.0.1  Modify Sbd_Load_Roll() to ignore update transfers using Owner file.
 *                      Add -Xn & -Mn to extract and merge NDC sale data via ApplyCumSaleNR().
 * 03/09/2020 19.8.3    Add Sbd_ExtractOwnerCsv() to extract owner in delimited format for MO module.
 *                      To use -Xo in CSV format, set OwnerExtr in [SBD] section of INI file.
 *                      Modify Sbd_Load_Roll() to add EXE_CD1
 * 04/02/2020 19.8.7    Add error checking for -Xn option.
 * 04/09/2020 19.8.9    Modify Sbd_Load_Roll() to merge char and transfer.
 * 07/14/2020 20.1.5    Fine tune Sbd_MergeSAdr1(), skip remove header in Sbd_Load_LDR()
 * 09/30/2020 20.2.13   Modify Sbd_Load_Roll() to check for -Msx option to remove transfer.
 * 10/22/2020 20.3.5    Modify Sbd_MergeOwner() & Sbd_MergeOwner1() to fix OWNER1 & OWNER_SWAP
 *                      overwrite the next field.
 * 05/06/2021 20.7.18   Fix situs Unit# in Sbd_MergeSAdr1().  Set FirePlace='M' when more than 9 in Sbd_ConvStdChar().
 *                      Ignore SaleCode when there is no translation in Sbd_Fmt???Sale().
 * 05/26/2021 20.8.1    Add global sPrevCity & sPrevZip.  Modify Sbd_MergeSAdr1(), Sbd_MergeRoll() & 
 *                      Sbd_MergeUpdRoll() to populate missing zip code.
 * 06/17/2021 20.9.1    Bug fix in Sbd_MergeMAdr1() to remove leading space before process mail addr.
 *                      Do not turn ON -Ma when loading roll update.
 * 07/14/2021 20.0.4    Modify Sbd_MergeStdChar() to populate QualityClass.  Modify Sbd_MergeSAdr1()
 *                      to fix city name for known county error.  Modify Sbd_Load_LDR() to merge char.
 * 09/03/2021 21.2.1    Modify Sbd_MergeSAdr1() to add special cases for some parcels.
 * 05/25/2022 21.9.1    Add -Xl & -Xf.  Add Sbd_ExtrLien() & Sbd_ExtrVal().  Modify Sbd_ExtrTRI() to extract ROLVALU record
 * 08/11/2022 22.1.3    Finish up the -Xf option.
 * 08/17/2022 22.1.4    Add -Mf option and fix value check in Sbd_ExtrVal().
 * 10/31/2022 22.2.11   Remove option to merge final value (it's moved to LoadOne.cpp).
 * 05/23/2023 22.7.2    Modify Sbd_MergeOwnerXfer() to fix transfer issue.  We now update Transfer
 *                      based on info from NPP.OWNER.TRANSFER.TXT.  Modify Sbd_Load_Roll() to bypass
 *                      header record and update transfer regardless record has data present.
 * 10/19/2023 23.3.5    Add Sbd_ExtrTaxItems() to extract tax info from PI347FTP.DAT and create Item_Extr.txt
 *                      Add Sbd_Load_TaxDetailEx() & Sbd_ParseTaxDetailEx() for loading new detail file Item_Extr.txt
 * 11/24/2023 23.4.3    Change tax loading procedure to skip loading tax delq if there is no new file.
 * 02/02/2024 23.6.0    Modify Sbd_MergeMAdr1() & Sbd_MergeSAdr() to populate UnitNox.
 * 03/20/2024 23.7.3    Modify Sbd_MergeSAdr1() to remove post dir from unit#.
 * 04/23/2024 23.8.1    Remove unused functions Sbd_MergeCharRec() & Sbd_MergeCharXRec().
 * 07/09/2024 24.0.2    Modify Sbd_MergeRoll() to add ExeCode & ExeType.
 * 08/20/2024 24.1.1    Add Sbd_ConvStdChar1() to load new CHAR file.
 * 10/17/2024 24.1.7    Modify CreateValueRec() to set default HO_FLG='2'
 * 01/30/2025 24.4.8    Modify Sbd_MergeOwnerXfer() to update TRANSFER if XFERDOC & XFERDATE has the same year.
 * 01/31/2025 24.4.9    Modify Sbd_ParseTaxDetailEx() to set TCFlg.
 *
 ******************************************************************************/

#include "stdafx.h"
#include "Prodlib.h"
#include "CountyInfo.h"
#include "R01.h"
#include "RecDef.h"
#include "Tables.h"
#include "SaleRec.h"
#include "Logs.h"
#include "LoadDef.h"
#include "Utils.h"
#include "XlatTbls.h"
#include "doOwner.h"
#include "doSort.h"
#include "formatApn.h"
#include "LoadOne.h"
#include "CharRec.h"
#include "MergeSbd.h"
#include "UseCode.h"
#include "LOExtrn.h"
#include "PQ.h"
#include "Tax.h"
#include "Ebc2Asc.h"
#include "NdcExtr.h"

static FILE *fdLegal, *fdOwner, *fdChar, *fdProp8, *fdTaxStat;
static long lLegalMatch, lFullExe, lCharSkip, lCharMatch, lTaxSkip, lTaxMatch, lProp8Skip, lProp8Match, lUpdateXfer;
static char acProp8File[_MAX_PATH], sPrevCity[16], sPrevZip[16];

/******************************** Sbd_MergeOwner *****************************
 *
 * Return 0 if successful, >0 if error
 *
 *****************************************************************************/

void Sbd_MergeOwner(char *pOutbuf, char *pName1, char *pName2, char *pVest1, char *pVest2)
{
   int   iRet;
   char  *pTmp, acOwner1[128], acOwner2[128], acTmp[128], acLName[64];
   OWNER myOwner1, myOwner2;

   // Clear output buffer if needed
   removeNames(pOutbuf, false);

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "0108303200000", 9))
   //   iTmp = 0;
#endif

   // Save last name
   strcpy(acLName, pName1);
   pTmp = strchr(acLName, ',');
   if (pTmp) *pTmp = 0;

   // Copy Name1
   strcpy(acOwner1, pName1);
   pTmp = &acOwner1[strlen(acOwner1)-1];
   if (*pTmp == '/' || *pTmp == '-')
      *pTmp = 0;
   blankRem(acOwner1);

   // If name2 is the same as name1, ignore it
   if (*pName2 > ' ' && strcmp(pName1, pName2))
   {
      strcpy(acOwner2, pName2);
      blankRem(acOwner2);
   } else
   {
      acOwner2[0] = 0;
   }

   // Update vesting
   pTmp = NULL;
   if (*pVest1 > ' ')
      memcpy(pOutbuf+OFF_VEST, pVest1, strlen(pVest1));
   else if (*pVest2 > ' ')
      memcpy(pOutbuf+OFF_VEST, pVest2, strlen(pVest2));

   if (acOwner2[0])
   {
      // If Owner1 and Owner2 are "HW" and have the same last name, combine them
      if (!memcmp(pVest1, pVest2, 2) && 
          !memcmp(acLName, acOwner2, strlen(acLName)) 
         )
      {
         MergeName(acOwner1, acOwner2, acOwner1);
         acOwner2[0] = 0;
      } else
         remChar(acOwner2, ',');
   }

   remChar(acOwner1, ',');
   vmemcpy(pOutbuf+OFF_NAME1, acOwner1, SIZ_NAME1);
   if (acOwner2[0])
      vmemcpy(pOutbuf+OFF_NAME2, acOwner2, SIZ_NAME2);

   // Now parse owners
   iRet = splitOwner(acOwner1, &myOwner1, 3);
   if (acOwner2[0])
   {
      splitOwner(acOwner2, &myOwner2, 3);
      if (!strcmp(myOwner1.acOL, myOwner2.acOL) && !memcmp(pVest1, pVest2, 2))
      {
         sprintf(acTmp, "%s %s & %s", myOwner1.acOF, myOwner1.acOM, myOwner2.acSwapName);
         iRet = blankRem(acTmp);
         vmemcpy(pOutbuf+OFF_NAME_SWAP, acTmp, SIZ_NAME_SWAP, iRet);
      } else
         vmemcpy(pOutbuf+OFF_NAME_SWAP, myOwner1.acSwapName, SIZ_NAME_SWAP);
   } else
      vmemcpy(pOutbuf+OFF_NAME_SWAP, myOwner1.acSwapName, SIZ_NAME_SWAP);
}

void Sbd_MergeOwner1(char *pOutbuf, char *pName1, char *pDba=NULL)
{
   int   iRet;
   char  acOwner1[128], acTmp1[128], acVesting[8], *pTmp;
   OWNER myOwner1;

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "0108302070000", 9))
   //   iRet = 0;
#endif

   // Copy Name1
   strcpy(acOwner1, pName1);

   if (pTmp = strchr(acOwner1, '\\'))
      *pTmp = '/';

   pTmp = &acOwner1[strlen(acOwner1)-1];
   if (*pTmp == '/' || *pTmp == '-')
      *pTmp = 0;

   // Cleanup known bad char
   iRet = remUnPrtChar(acOwner1);
   iRet = remChar(acOwner1, '"');
   replChar(acOwner1, ',', ' ');
   iRet = blankRem(acOwner1);

   // If same name, do not update
   //if (!memcmp(acOwner1, pOutbuf+OFF_NAME1, iRet))
  //    return;

   // Clear name and DBA in output buffer 
   removeNames(pOutbuf, false, true);

   // Update DBA
   if (pDba && *pDba)
      vmemcpy(pOutbuf+OFF_DBA, pDba, SIZ_DBA);

   // Update vesting
   acVesting[0] = 0;
   strcpy(acTmp1, acOwner1);
   pTmp = findVesting(acTmp1, acVesting);
   if (pTmp)
   {
      if (!memcmp(acVesting, "EA", 2))
         *(pOutbuf+OFF_ETAL_FLG) = 'Y';
      memcpy(pOutbuf+OFF_VEST, acVesting, strlen(acVesting));

      // Remove vesting from owner name for parsing
      *pTmp = 0;
   }

   // Check for trust
   //if (strstr(acOwner1, " TRS") || 
   //    strstr(acOwner1, " TR ") || 
   //    strstr(acOwner1, " TRUST ") || 
   //    strstr(acOwner1, " COMPANY ") || 
   //    strstr(acOwner1, " LLC ") || 
   //    strstr(acOwner1, "EST OF") )
   if (acVesting[0] > ' ')
   {                        
      vmemcpy(pOutbuf+OFF_NAME1, acOwner1, SIZ_NAME1);
      vmemcpy(pOutbuf+OFF_NAME_SWAP, acOwner1, SIZ_NAME_SWAP);
      return;
   }

   // Now parse owners
   iRet = splitOwner(acTmp1, &myOwner1, 3);
   if (myOwner1.acTitle[0] > ' ')
   {
      strcat(myOwner1.acSwapName, " ");
      strcat(myOwner1.acSwapName, myOwner1.acTitle);
   }
   vmemcpy(pOutbuf+OFF_NAME1, acOwner1, SIZ_NAME1);
   vmemcpy(pOutbuf+OFF_NAME_SWAP, myOwner1.acSwapName, SIZ_NAME_SWAP);

   if (myOwner1.acVest[0] > ' ')
      memcpy(pOutbuf+OFF_VEST, myOwner1.acVest, strlen(myOwner1.acVest));
}

/********************************* Sbd_CleanStr ******************************
 *
 * Merge Mail address
 *
 *****************************************************************************/

void Sbd_CleanStr(char *pBuf, int iLen=0)
{
   char *pTmp, acTmp[512];
   int   iTmp=0;
   bool  bSpace = false;

   if (iLen > 0)
      *(pBuf+iLen) = 0;

   // Remove leading space
   pTmp = pBuf;
   while (*pTmp == ' ')
      pTmp++;

   // Remove extra space in the middle
   while (*pTmp)
   {
      if (*pTmp == ' ')
      {
         if (!bSpace)
         {
            bSpace = true;
            acTmp[iTmp++] = ' ';
         }
      } else if ((unsigned)(*pTmp) > 122)
      {
         // Special case
         if (acTmp[iTmp-1] == 'A')
         {
            acTmp[iTmp++] = 'P';
            bSpace = false;
         }
      } else if (*pTmp > ' ')
      {
         if (*pTmp > 'Z')
         {
            if (*pTmp >= 'a' && (unsigned)(*pTmp) <= 122)
               acTmp[iTmp++] = *pTmp & 0x5F;       // Convert to upper case
         } else 
            acTmp[iTmp++] = *pTmp;
         bSpace = false;
      }

      pTmp++;
   }

   if (iTmp > 0 && acTmp[iTmp-1] <= ' ')
      acTmp[iTmp-1] = 0;
   else
      acTmp[iTmp] = 0;
   strcpy(pBuf, acTmp);
}

/******************************** Sbd_MergeMAdr ******************************
 *
 * Merge Mail address
 *
 *****************************************************************************/

void Sbd_MergeMAdr(char *pOutbuf, char *pDba, char *pCareOf, char *pMail1, char *pMail2, char *pZip, char *pZip4)
{
   char     acTmp[256], acAddr1[64], acAddr2[64];
   int      iTmp;
   char     *pTmp;
   CString  strIn;
   ADR_REC  sMailAdr;

   // Clear old Mailing
   removeMailing(pOutbuf, true, true);
   memset((void *)&sMailAdr, 0, sizeof(ADR_REC));

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "0417351020000", 10))
   //   iTmp = 0;
#endif

   // CareOf
   if (*pCareOf > ' ')
   {
      iTmp = remUnPrtChar(pCareOf);
      updateCareOf(pOutbuf, pCareOf, iTmp);
   }

   // DBA
   if (*pDba > ' ')
   {
      iTmp = remUnPrtChar(pDba);
      vmemcpy(pOutbuf+OFF_DBA, pDba, SIZ_DBA, iTmp);
   }

   // Mail address
   strcpy(acAddr1, pMail1);
   // Ignore unknown address
   if (!memcmp(acAddr1, "ADDRESS", 7))
      return;

   // Remove extra space and convert some chars
   Sbd_CleanStr(acAddr1);

   // Remove C/O if embeded in addr1
   if (acAddr1[0] > ' ')
   {
      pTmp = strstr(acAddr1, " C/O");
      if (pTmp)
      {
         *pTmp++ = 0;
         if (*(pOutbuf+OFF_CARE_OF) == ' ')
            updateCareOf(pOutbuf, pTmp, strlen(pTmp));
      }
      memcpy(pOutbuf+OFF_M_ADDR_D, acAddr1, strlen(acAddr1));

      // Parse mail address
      // 3333 NEW HYDE PARK RD STE 100 PO BOX 5020 (1008132050000)
      parseMAdr1_2(&sMailAdr, acAddr1);
      // 20080715 parseMAdr1_1(&sMailAdr, acAddr1);
      if (sMailAdr.lStrNum > 0)
      {
         sprintf(acTmp, "%d       ", sMailAdr.lStrNum);
         memcpy(pOutbuf+OFF_M_STRNUM, acTmp, SIZ_M_STRNUM);
         memcpy(pOutbuf+OFF_M_STR_SUB, sMailAdr.strSub, strlen(sMailAdr.strSub));
      }
      memcpy(pOutbuf+OFF_M_DIR, sMailAdr.strDir, strlen(sMailAdr.strDir));
      iTmp = strlen(sMailAdr.strName);
      if (iTmp > SIZ_M_STREET && !memcmp(sMailAdr.strName, "P O BOX",7))
      {
         // Skip the first 9 bytes and search for next space after box #
         pTmp = strchr((char *)&sMailAdr.strName[9], ' ');
         if (pTmp)
            *pTmp = 0;

        memcpy(pOutbuf+OFF_M_STREET, sMailAdr.strName, strlen(sMailAdr.strName));
        sMailAdr.strSfx[SIZ_M_SUFF] = 0;
      } else
      {
         sMailAdr.strName[SIZ_M_STREET] = 0;
         memcpy(pOutbuf+OFF_M_STREET, sMailAdr.strName, strlen(sMailAdr.strName));
         memcpy(pOutbuf+OFF_M_SUFF, sMailAdr.strSfx, strlen(sMailAdr.strSfx));
      }

      // Unit #
      if (sMailAdr.Unit[0] > ' ')
      {
         vmemcpy(pOutbuf+OFF_M_UNITNO, sMailAdr.Unit, SIZ_M_UNITNO);
         vmemcpy(pOutbuf+OFF_M_UNITNOX, sMailAdr.Unit, SIZ_M_UNITNOX);
      }

      // City/St - Zip
      strcpy(acAddr2, pMail2);
      Sbd_CleanStr(acAddr2);
      if (acAddr2[0] > ' ')
      {
         vmemcpy(pOutbuf+OFF_M_CTY_ST_D, acAddr2, SIZ_M_CTY_ST_D);
         parseAdr2(&sMailAdr, acAddr2);
         vmemcpy(pOutbuf+OFF_M_CITY, sMailAdr.City, SIZ_M_CITY);
         vmemcpy(pOutbuf+OFF_M_ST, sMailAdr.State, SIZ_M_ST);

         if (sMailAdr.Zip[0] > ' ')
            vmemcpy(pOutbuf+OFF_M_ZIP, sMailAdr.Zip, SIZ_M_ZIP);
         else if (isdigit(*pZip))
            vmemcpy(pOutbuf+OFF_M_ZIP, pZip, 9);
      }
   } else
   {
      // If situs available, use it
      if (*(pOutbuf+OFF_S_STRNUM) > ' ')
      {
         memcpy(pOutbuf+OFF_M_STRNUM, pOutbuf+OFF_S_STRNUM, SIZ_M_STRNUM);
         memcpy(pOutbuf+OFF_M_DIR, pOutbuf+OFF_S_DIR, SIZ_M_DIR);
         memcpy(pOutbuf+OFF_M_STREET, pOutbuf+OFF_S_STREET, SIZ_M_STREET);
         if (*apTokens[SBD_ROLL_SITUS_SUFF] > ' ')
            vmemcpy(pOutbuf+OFF_M_SUFF, apTokens[SBD_ROLL_SITUS_SUFF], SIZ_M_SUFF);
         if (*apTokens[SBD_ROLL_SITUS_CITY] > ' ')
            vmemcpy(pOutbuf+OFF_M_CITY, apTokens[SBD_ROLL_SITUS_CITY], SIZ_M_CITY);
         memcpy(pOutbuf+OFF_M_ST, "CA", 2);
         memcpy(pOutbuf+OFF_M_ZIP, pOutbuf+OFF_S_ZIP, SIZ_M_ZIP);
         memcpy(pOutbuf+OFF_M_ADDR_D, pOutbuf+OFF_S_ADDR_D, SIZ_M_ADDR_D);
         memcpy(pOutbuf+OFF_M_CTY_ST_D, pOutbuf+OFF_S_CTY_ST_D, SIZ_M_CTY_ST_D);
      }
   }
}

/********************************* Sbd_MergeMAdr1 *****************************
 *
 * Temporary ignore Zip4.
 *
 ******************************************************************************/

void Sbd_MergeMAdr1(char *pOutbuf, LPSTR pAdr1, LPSTR pAdr2, LPSTR pZip, LPSTR pSitusSfx)
{
   char     acTmp[256], acAddr1[64], acAddr2[64];
   int      iTmp;
   char     *pTmp, *pAddr1;
   ADR_REC  sMailAdr;

   // Clear old Mailing
   removeMailing(pOutbuf, false);
   memset((void *)&sMailAdr, 0, sizeof(ADR_REC));

   // Mail address
   strcpy(acAddr1, pAdr1);

   if (pZip && *pZip > ' ')
   {
      if (9 == strlen(pZip))
         //sprintf(acAddr2, "%s %.5s-%s", pAdr2, pZip, pZip+5);
         sprintf(acAddr2, "%s %.5s", pAdr2, pZip);
      else
         sprintf(acAddr2, "%s %s", pAdr2, pZip);
   } else
      strcpy(acAddr2, pAdr2);

   // Ignore unknown address
   if (!memcmp(acAddr1, "ADDRESS", 7))
      acAddr1[0] = 0;

   // Remove extra space and convert some chars
   Sbd_CleanStr(acAddr1);

   // Remove C/O if embeded in addr1
   if (acAddr1[0] > ' ')
   {
      remChar(acAddr2, ',');
      Sbd_CleanStr(acAddr2);

#ifdef _DEBUG
      //if (!memcmp(pOutbuf, "0108301030000", 10))
      //   iTmp = 0;
#endif
      // Store mail 1 & 2 as is
      vmemcpy(pOutbuf+OFF_M_ADDR_D, acAddr1, SIZ_M_ADDR_D);
      vmemcpy(pOutbuf+OFF_M_CTY_ST_D, acAddr2, SIZ_M_CTY_ST_D);

      pAddr1 = &acAddr1[0];
      if (!memcmp(acAddr1, "C/O", 3))
      {
         if (acAddr1[4] <= '9')
            pAddr1 = &acAddr1[4];
         else if (pTmp = strstr(acAddr1, " PO BOX"))
         {
            *pTmp++ = 0;
            pAddr1 = pTmp;
            updateCareOf(pOutbuf, acAddr1, strlen(acAddr1));
         } else if (pTmp = strstr(acAddr1, " BOX "))
         {
            *pTmp++ = 0;
            pAddr1 = pTmp;
            updateCareOf(pOutbuf, acAddr1, strlen(acAddr1));
         }
      } else
      {
         pTmp = strstr(acAddr1, " C/O");
         if (pTmp)
         {
            *pTmp++ = 0;
            updateCareOf(pOutbuf, pTmp, strlen(pTmp));
         }
      }

      // Skip # at StrNum
      if (*pAddr1 == '#')
         pAddr1++;

      // Parse mail address
      // 3333 NEW HYDE PARK RD STE 100 PO BOX 5020 (1008132050000)
      parseMAdr1_2(&sMailAdr, pAddr1);
      // 20080715 parseMAdr1_1(&sMailAdr, acAddr1);
      if (sMailAdr.lStrNum > 0)
      {
         sprintf(acTmp, "%d       ", sMailAdr.lStrNum);
         memcpy(pOutbuf+OFF_M_STRNUM, acTmp, SIZ_M_STRNUM);
         memcpy(pOutbuf+OFF_M_STR_SUB, sMailAdr.strSub, strlen(sMailAdr.strSub));
      }
      memcpy(pOutbuf+OFF_M_DIR, sMailAdr.strDir, strlen(sMailAdr.strDir));
      iTmp = strlen(sMailAdr.strName);
      if (iTmp > SIZ_M_STREET && !memcmp(sMailAdr.strName, "P O BOX",7))
      {
         // Skip the first 9 bytes and search for next space after box #
         pTmp = strchr((char *)&sMailAdr.strName[9], ' ');
         if (pTmp)
            *pTmp = 0;

        memcpy(pOutbuf+OFF_M_STREET, sMailAdr.strName, strlen(sMailAdr.strName));
        sMailAdr.strSfx[SIZ_M_SUFF] = 0;
      } else
      {
         sMailAdr.strName[SIZ_M_STREET] = 0;
         memcpy(pOutbuf+OFF_M_STREET, sMailAdr.strName, strlen(sMailAdr.strName));
         memcpy(pOutbuf+OFF_M_SUFF, sMailAdr.strSfx, strlen(sMailAdr.strSfx));
      }

      // Unit #
      if (sMailAdr.Unit[0] > ' ')
      {
         vmemcpy(pOutbuf+OFF_M_UNITNO, sMailAdr.Unit, SIZ_M_UNITNO);
         vmemcpy(pOutbuf+OFF_M_UNITNOX, sMailAdr.UnitNox, SIZ_M_UNITNOX);
      }

      // City/St - Zip
      if (acAddr2[0] > ' ')
      {
         parseAdr2(&sMailAdr, acAddr2);

         iTmp = strlen(sMailAdr.City);
         if (iTmp > SIZ_M_CITY) iTmp = SIZ_M_CITY;
         vmemcpy(pOutbuf+OFF_M_CITY, sMailAdr.City, SIZ_M_CITY);
         vmemcpy(pOutbuf+OFF_M_ST, sMailAdr.State, strlen(sMailAdr.State));

         if (sMailAdr.Zip[0] > ' ')
         {
            vmemcpy(pOutbuf+OFF_M_ZIP, sMailAdr.Zip, SIZ_M_ZIP);
            if (sMailAdr.Zip4[0] > ' ')
               vmemcpy(pOutbuf+OFF_M_ZIP4, sMailAdr.Zip4, SIZ_M_ZIP4);
         }
      }
   } else
   {
      // If situs available, use it
      if (*(pOutbuf+OFF_S_STRNUM) > ' ')
      {
         memcpy(pOutbuf+OFF_M_STRNUM, pOutbuf+OFF_S_STRNUM, SIZ_M_STRNUM);
         memcpy(pOutbuf+OFF_M_DIR, pOutbuf+OFF_S_DIR, SIZ_M_DIR);
         memcpy(pOutbuf+OFF_M_STREET, pOutbuf+OFF_S_STREET, SIZ_M_STREET);

         if (*pSitusSfx > ' ')
            memcpy(pOutbuf+OFF_M_SUFF, pSitusSfx, strlen(pSitusSfx));

         memcpy(pOutbuf+OFF_M_UNITNO, pOutbuf+OFF_S_UNITNO, SIZ_M_UNITNO);
         memcpy(pOutbuf+OFF_M_UNITNOX, pOutbuf+OFF_S_UNITNOX, SIZ_M_UNITNOX);

         iTmp = atoin(pOutbuf+OFF_S_CITY, 3);
         if (iTmp > 0)
         {
            pTmp = GetCityName(iTmp);
            memcpy(pOutbuf+OFF_M_CITY, pTmp, strlen(pTmp));
         }

         memcpy(pOutbuf+OFF_M_ST, "CA", 2);
         memcpy(pOutbuf+OFF_M_ZIP, pOutbuf+OFF_S_ZIP, SIZ_M_ZIP);
         memcpy(pOutbuf+OFF_M_ADDR_D, pOutbuf+OFF_S_ADDR_D, SIZ_M_ADDR_D);
         memcpy(pOutbuf+OFF_M_CTY_ST_D, pOutbuf+OFF_S_CTY_ST_D, SIZ_M_CTY_ST_D);
      }
   }
}

/******************************** Sbd_MergeSAdr ******************************
 *
 * Merge Situs address
 *
 *****************************************************************************/

void Sbd_MergeSAdr(char *pOutbuf)
{
   char     acTmp[256], acAddr1[128], acCity[32], acStr[32], acSfx[16], acUnit[16];
   long     iTmp, lTmp, lStrNo;
   char     *pTmp;
   ADR_REC  sAddr;

   // Clear old Mailing
   removeSitus(pOutbuf);
   memset((void *)&sAddr, 0, sizeof(ADR_REC));

   acUnit[0]=acAddr1[0] = 0;
   lStrNo = atol(apTokens[SBD_ROLL_SITUS_NBR]);
   if (lStrNo > 0)
   {
      // Check for '-'
      //if (pTmp = isCharIncluded(apTokens[SBD_ROLL_SITUS_NBR], '-', strlen(apTokens[SBD_ROLL_SITUS_NBR])))
      //   pTmp++;
      memcpy(pOutbuf+OFF_S_HSENO, apTokens[SBD_ROLL_SITUS_NBR], strlen(apTokens[SBD_ROLL_SITUS_NBR]));

      sprintf(acTmp, "%d       ", lStrNo);
      memcpy(pOutbuf+OFF_S_STRNUM, acTmp, SIZ_S_STRNUM);
      pTmp = strchr(acTmp, ' ');
      *(pTmp+1) = 0;
      strcpy(acAddr1, acTmp);

      if (pTmp = strchr(apTokens[SBD_ROLL_SITUS_NBR], ' '))
         memcpy(pOutbuf+OFF_S_STR_SUB, pTmp+1, strlen(pTmp+1));
   }

   if (*apTokens[SBD_ROLL_SITUS_DIR] > ' ')
   {
      strcat(acAddr1, apTokens[SBD_ROLL_SITUS_DIR]);
      strcat(acAddr1, " ");
      memcpy(pOutbuf+OFF_S_DIR, apTokens[SBD_ROLL_SITUS_DIR], strlen(apTokens[SBD_ROLL_SITUS_DIR]));
   }

   strcpy(acStr, apTokens[SBD_ROLL_SITUS_NAME]);
   blankRem(acStr);
   strcpy(acSfx, apTokens[SBD_ROLL_SITUS_SUFF]);
   blankRem(acSfx);
   if (acStr[0] > ' ')
   {
      if (pTmp = strchr(acStr, '`'))
         *pTmp = ' ';

      // If strName also has sfx, remove it
      pTmp = strrchr(acStr, ' ');
      if (pTmp && !strcmp(pTmp+1, acSfx))
         *pTmp = 0;
      else if ((pTmp = strstr(acStr, " - UNIT ")))
      {
         *pTmp = 0;
         pTmp += 8;
         strcpy(acUnit, pTmp);
      } else if ((pTmp = strstr(acStr, " UNIT ")) ||
                 (pTmp = strstr(acStr, " UNIT-")) || 
                 (pTmp = strstr(acStr, " - NO ")))
      {
         *pTmp = 0;
         pTmp += 6;
         strcpy(acUnit, pTmp);
      } else if ((pTmp = strstr(acStr, " NO ")) || (pTmp = strstr(acStr, " NO.")))
      {
         *pTmp = 0;
         pTmp += 4;
         strcpy(acUnit, pTmp);
      } else if ((pTmp = strstr(acStr, " APT ")) || (pTmp = strstr(acStr, " APT.")))
      {
         *pTmp = 0;
         pTmp += 5;
         strcpy(acUnit, pTmp);
      } else if ((pTmp = strstr(acStr, " #")))
      {
         *pTmp = 0;
         strcpy(acUnit, ++pTmp);
      } else if ((pTmp = strstr(acStr, " SP ")) || (pTmp = strstr(acStr, " SP.")))
      {
         *pTmp = 0;
         pTmp += 4;
         strcpy(acUnit, pTmp);
      } else if ((pTmp = strstr(acStr, " SP")))
      {
         if (isdigit(*(pTmp+3)))
         {
            *pTmp = 0;
            pTmp += 3;
            strcpy(acUnit, pTmp);
         }
      } else if ((pTmp = strstr(acStr, " -")))
      {
         // FLORA -B
         if (strlen(pTmp) < 6)
         {
            *pTmp = 0;
            pTmp += 2;
            strcpy(acUnit, pTmp);
         }
      }

      memcpy(pOutbuf+OFF_S_STREET, acStr, strlen(acStr));
      strcat(acAddr1, acStr);

      if (acSfx[0] > ' ')
      {
         strcat(acAddr1, " ");
         strcat(acAddr1, acSfx);


         STRSFX *pStrSfx;
         if (pStrSfx = GetSfxCode(acSfx, &Sbd_Sfx[0]))
            memcpy(pOutbuf+OFF_S_SUFF, pStrSfx->pSfxCode, pStrSfx->iLen);
         else
         {
            LogMsg0("*** Invalid suffix: %s, APN=%.13s", acSfx, pOutbuf);
            iBadSuffix++;
         }
      }

      // Process unit if it has not been identified
      if (!acUnit[0])
         strcpy(acUnit, apTokens[SBD_ROLL_SITUS_QUAL]);

      blankRem(acUnit);
      if (acUnit[0] > ' ' && lStrNo > 0)
      {
         strcat(acAddr1, " ");
         strcat(acAddr1, acUnit);
         iTmp = strlen(acUnit);
         // If it is longer than predefined size, it might be garbage.
         if (iTmp <= SIZ_S_UNITNO)
         {
            vmemcpy(pOutbuf+OFF_S_UNITNO, acUnit, SIZ_S_UNITNO);
            vmemcpy(pOutbuf+OFF_S_UNITNOX, acUnit, SIZ_S_UNITNOX);
         }
      }     
   }
   vmemcpy(pOutbuf+OFF_S_ADDR_D, acAddr1, SIZ_S_ADDR_D);

   // Zipcode
   lTmp = atol(apTokens[SBD_ROLL_SITUS_ZIP1]);
   if (lTmp >= 90000)
      memcpy(pOutbuf+OFF_S_ZIP, apTokens[SBD_ROLL_SITUS_ZIP1], SIZ_S_ZIP);

   // Situs city
   strcpy(acCity, apTokens[SBD_ROLL_SITUS_CITY]);
   blankRem(acCity);
   if (*acCity > ' ')
   {
      City2Code(acCity, acTmp);
      memcpy(pOutbuf+OFF_S_CITY, acTmp, SIZ_S_CITY);
      memcpy(pOutbuf+OFF_S_ST, "CA", 2);

      iTmp = 0;
      if (acCity[0] > ' ')
         iTmp = sprintf(acTmp, "%s, CA %.5s", acCity, pOutbuf+OFF_S_ZIP);
      vmemcpy(pOutbuf+OFF_S_CTY_ST_D, acTmp, SIZ_S_CTY_ST_D);
   }
}

void Sbd_MergeSAdr1(char *pOutbuf, LPSTR pStrNum, LPSTR pStrDir, LPSTR pStrName, 
                   LPSTR pStrSfx, LPSTR pStrUnit, LPSTR pStrComm, LPSTR pStrZip)
{
   char     acTmp[256], acAddr1[128], acCity[32], acStr[32], acSfx[16], acUnit[16];
   long     iTmp, lTmp, lStrNo;
   char     *pTmp;
   ADR_REC  sAddr;

   // Clear old addr
   removeSitus(pOutbuf);
   memset((void *)&sAddr, 0, sizeof(ADR_REC));

   acUnit[0]=acAddr1[0] = 0;
   lStrNo = atol(pStrNum);
   if (lStrNo > 0)
   {
      // Check for '-'
      //if (pTmp = isCharIncluded(pStrNum, '-', strlen(pStrNum)))
      //   pTmp++;
      memcpy(pOutbuf+OFF_S_HSENO, pStrNum, strlen(pStrNum));

      sprintf(acTmp, "%d       ", lStrNo);
      memcpy(pOutbuf+OFF_S_STRNUM, acTmp, SIZ_S_STRNUM);
      pTmp = strchr(acTmp, ' ');
      *(pTmp+1) = 0;
      strcpy(acAddr1, acTmp);

      if (pTmp = strchr(pStrNum, ' '))
         memcpy(pOutbuf+OFF_S_STR_SUB, pTmp+1, strlen(pTmp+1));
   }

   if (*pStrDir > ' ')
   {
      strcat(acAddr1, pStrDir);
      strcat(acAddr1, " ");
      memcpy(pOutbuf+OFF_S_DIR, pStrDir, strlen(pStrDir));
   }

   strcpy(acStr, pStrName);
   blankRem(acStr);
   strcpy(acSfx, pStrSfx);
   blankRem(acSfx);
   if (acStr[0] > ' ')
   {
      if (pTmp = strchr(acStr, '`'))
         *pTmp = ' ';

      // If strName also has sfx, remove it
      pTmp = strrchr(acStr, ' ');
      if (pTmp && !strcmp(pTmp+1, acSfx))
         *pTmp = 0;
      else if ((pTmp = strstr(acStr, " - UNIT ")))
      {
         *pTmp = 0;
         pTmp += 8;
         strcpy(acUnit, pTmp);
      } else if ((pTmp = strstr(acStr, " UNIT ")) ||
                 (pTmp = strstr(acStr, " UNIT-")) || 
                 (pTmp = strstr(acStr, " - NO ")))
      {
         *pTmp = 0;
         pTmp += 6;
         strcpy(acUnit, pTmp);
      } else if ((pTmp = strstr(acStr, " NO ")) || (pTmp = strstr(acStr, " NO.")))
      {
         *pTmp = 0;
         pTmp += 4;
         strcpy(acUnit, pTmp);
      } else if ((pTmp = strstr(acStr, " APT ")) || (pTmp = strstr(acStr, " APT.")))
      {
         *pTmp = 0;
         pTmp += 5;
         strcpy(acUnit, pTmp);
      } else if ((pTmp = strstr(acStr, " #")))
      {
         *pTmp++ = 0;
         strcpy(acUnit, pTmp);
      } else if ((pTmp = strstr(acStr, " SP ")) || (pTmp = strstr(acStr, " SP.")))
      {
         *pTmp++ = 0;
         strcpy(acUnit, pTmp);
      } else if ((pTmp = strstr(acStr, " SP")))
      {
         if (isdigit(*(pTmp+3)))
         {
            *pTmp = 0;
            pTmp += 3;
            sprintf(acUnit, "SP %s", pTmp);
         }
      } else if ((pTmp = strstr(acStr, " -")))
      {
         // FLORA -B
         if (strlen(pTmp) < 6)
         {
            *pTmp = 0;
            pTmp += 2;
            strcpy(acUnit, pTmp);
         }
      }

      // Special case
      if (!strcmp(acSfx, "BAY"))
      {
         strcat(acStr, " ");
         strcat(acStr, acSfx);
         acSfx[0] = 0;
      }

      // Remove known bad chars
      iTmp = strlen(acStr);
      remUChar((unsigned char *)&acStr[0], 0x8D, iTmp);
      memcpy(pOutbuf+OFF_S_STREET, acStr, iTmp);
      strcat(acAddr1, acStr);

      if (acSfx[0] > ' ')
      {
         strcat(acAddr1, " ");
         strcat(acAddr1, acSfx);

         STRSFX *pStrSfx;
         if (pStrSfx = GetSfxCode(acSfx, &Sbd_Sfx[0]))
            memcpy(pOutbuf+OFF_S_SUFF, pStrSfx->pSfxCode, pStrSfx->iLen);
         else
         {
            LogMsg0("*** Invalid suffix: %s, APN=%.13s", acSfx, pOutbuf);
            iBadSuffix++;
         }
      }

      // Process unit if it has not been identified
      strcpy(acTmp, pStrUnit);
      myBTrim(acTmp);
      if (!memcmp(acTmp, "E SP ", 5))
      {
         *(pOutbuf+OFF_S_POSTDIR) = 'E';
         strcpy(acUnit, &acTmp[2]);
         strcat(acAddr1, " E");
      } else
      if (acTmp[0] > ' ' && memcmp(pStrUnit, "AND ", 4))
      {
         iTmp = blankRem(acTmp, ' ');
         // If UnitNo is too long, take first 6 bytes (i.e. #101-102)
         if (acTmp[0] == '#')
         {
            if (pTmp = strchr(acTmp, ' '))
               *pTmp = 0;
            iTmp = strlen(acTmp);
            acTmp[SIZ_S_UNITNO+1] = 0;
            if (iTmp <= SIZ_S_UNITNO)
               strcpy(acUnit, acTmp);
            else 
            {
               memcpy(acUnit, &acTmp[1], SIZ_S_UNITNO);
               acUnit[SIZ_S_UNITNO] = 0;
            }
         } else if (!memcmp(acTmp, "UNIT ", 5) || !memcmp(acTmp, "UNITS", 5) || !memcmp(acTmp, "UINT ", 5))
            strcpy(acUnit, &acTmp[5]);
         else if (!memcmp(acTmp, "UNIT", 4) || !memcmp(acTmp, "UNT ", 4))
            strcpy(acUnit, &acTmp[4]);
         else if (!memcmp(acTmp, "UN IT ", 6) || !memcmp(acTmp, "SUITE ", 6))
            strcpy(acUnit, &acTmp[6]);
         else if (!memcmp(acTmp, "U# ", 3))
            strcpy(acUnit, &acTmp[3]);
         else if (!memcmp(acTmp, "U ", 2))
            strcpy(acUnit, &acTmp[2]);
         else if (!memcmp(acTmp, "NO. ", 4))
            strcpy(acUnit, &acTmp[4]);
         else if (!memcmp(acTmp, "SPACE ", 6))
            strcpy(acUnit, &acTmp[6]);
         else if (!memcmp(acTmp, "SP ", 3))
            strcpy(acUnit, &acTmp[3]);
         else if (!memcmp(acTmp, "SP# ", 4))
            strcpy(acUnit, &acTmp[4]);
         else if (!memcmp(acTmp, "STE. ", 5) || !memcmp(acTmp, "STES ", 5))
            strcpy(acUnit, &acTmp[5]);
         else if (!memcmp(acTmp, "STE#", 4) || !memcmp(acTmp, "STE ", 4))
            strcpy(acUnit, &acTmp[4]);
         else if (!memcmp(acTmp, "APT ", 4))
            strcpy(acUnit, &acTmp[4]);
         else if (!memcmp(acTmp, "BL1", 3))
            sprintf(acUnit, "B1-", &acTmp[6]);
         else if (!memcmp(acTmp, "B1 ", 3))
            sprintf(acUnit, "B1-", &acTmp[5]);
         else if (!memcmp(acTmp, "B2 ", 3))
            sprintf(acUnit, "B2-", &acTmp[5]);
         else if (pTmp = strchr(&acTmp[2], '#'))
            strcpy(acUnit, pTmp+1);
         else if (!memcmp(acTmp, "A THRU C", 8) 
            || !memcmp(acTmp, "A, B, C", 7) 
            || !memcmp(acTmp, "A, B & C", 8)
            || !memcmp(acTmp, "A B & C", 7)
            || !memcmp(acTmp, "A,B & C", 7)
            || !memcmp(acTmp, "A,B, & C", 8))
            strcpy(acUnit, "A-C");
         else if (!memcmp(acTmp, "A,B,C,D", 7) 
            || !memcmp(acTmp, "A/B/C/D", 7)
            || !memcmp(acTmp, "A B C D", 7))
            strcpy(acUnit, "A-D");
         else if (!memcmp(acTmp, "A AND B", 7) || !memcmp(acTmp, "A & UNIT B", 7))
            strcpy(acUnit, "A & B");
         else if (!memcmp(acTmp, "A THRU K", 8) )
            strcpy(acUnit, "A-K");
         else if (!memcmp(acTmp, "G,H,I,K", 7) )
            strcpy(acUnit, "G-K");
         else
            strcpy(acUnit, acTmp);
      } else if (!memcmp(acUnit, "UNIT ", 5))
         strcpy(acUnit, &acUnit[5]);

      if (!memcmp(acUnit, "A AND B", 7))
         strcpy(acUnit, "A & B");
      else if (pTmp = strstr(acUnit, " BLVD"))
         *pTmp = 0;

      iTmp = blankRem(acUnit);

      // Remove extra blank after #
      if (acUnit[0] == '#' && acUnit[1] == ' ')
         strcpy(&acUnit[1], &acUnit[2]);

      // Replace FT
      if (pTmp = strchr(acUnit, '\''))
         strcpy(pTmp, "FT");

      if (acUnit[0] > ' ' && lStrNo > 0)
      {
         // Extended UnitNo
         vmemcpy(pOutbuf+OFF_S_UNITNOX, acUnit, SIZ_S_UNITNOX);

         if (acUnit[0] != '#' && strlen(acUnit) < 4)
            strcat(acAddr1, " #");
         else
            strcat(acAddr1, " ");
         strcat(acAddr1, acUnit);
         iTmp = strlen(acUnit);
         // If it is longer than predefined size, it might be garbage.
         if (iTmp <= SIZ_S_UNITNO)
            memcpy(pOutbuf+OFF_S_UNITNO, acUnit, iTmp);
         else if (acUnit[0] != '(')
         {
            LogMsg("??? UnitNo: %s [%.13s]", acUnit, pOutbuf);if (pTmp = strchr(acUnit, ' '))
            {
               *pTmp = 0;
               if (*(pTmp-1) == ',')
                  *(pTmp-1) = 0;
            }
            if (strcmp(acUnit, "COMON") && strcmp(acUnit, "CITY") && strcmp(acUnit, "UN")
               && memcmp(acUnit, "BL", 2) && memcmp(acUnit, "PAR", 3) && memcmp(acUnit, "LOT", 3))
               vmemcpy(pOutbuf+OFF_S_UNITNO, acUnit, SIZ_S_UNITNO);
         }
      }     
   }
   vmemcpy(pOutbuf+OFF_S_ADDR_D, acAddr1, SIZ_S_ADDR_D);

   lTmp = atol(pStrZip);
   if (lTmp > 90000)
      memcpy(pOutbuf+OFF_S_ZIP, pStrZip, SIZ_S_ZIP);

#ifdef _DEBUG
      //if (!memcmp(pOutbuf, "0313141030000", 13))
      //   iTmp = 0;
#endif

   // Situs city 
   if (!memcmp(pOutbuf, "060627113", 9) || !memcmp(pOutbuf, "060628107", 9))
   {
         memcpy(pOutbuf+OFF_S_CITY, "71", 2);
         memcpy(pOutbuf+OFF_S_ST, "CA", 2);
         memcpy(pOutbuf+OFF_S_ZIP, "92252", 5);
         memcpy(pOutbuf+OFF_S_CTY_ST_D, "JOSHUA TREE, CA 92252", 21);
   } else if (!memcmp(pOutbuf, "028129159", 9) || !memcmp(pOutbuf, "028129169", 9))
   {
         memcpy(pOutbuf+OFF_S_CITY, "82", 2);
         memcpy(pOutbuf+OFF_S_ST, "CA", 2);
         memcpy(pOutbuf+OFF_S_ZIP, "92354", 5);
         memcpy(pOutbuf+OFF_S_CTY_ST_D, "LOMA LINDA, CA 92354", 20);
   } else if (!memcmp(pStrZip, "92354", 5))
   {
         memcpy(pOutbuf+OFF_S_CITY, "82", 2);
         memcpy(pOutbuf+OFF_S_ST, "CA", 2);
         memcpy(pOutbuf+OFF_S_ZIP, "92354", 5);
         memcpy(pOutbuf+OFF_S_CTY_ST_D, "LOMA LINDA, CA 92354", 20);
   } else
   if (*pStrComm > ' ')
   {
      char acZip[32];
      iTmp = Abbr2CZ(pStrComm, acTmp, acCity, acZip, pOutbuf);
      if (iTmp >= 0)
      {
         memcpy(pOutbuf+OFF_S_CITY, acTmp, strlen(acTmp));
         memcpy(pOutbuf+OFF_S_ST, "CA", 2);
         if (*(pOutbuf+OFF_S_ZIP) == ' ' && acZip[0] == '9')
            vmemcpy(pOutbuf+OFF_S_ZIP, acZip, SIZ_S_ZIP);

         if (acCity[0] > ' ')
         {
            myTrim(acCity);
            iTmp = sprintf(acTmp, "%s, CA %.5s", acCity, pOutbuf+OFF_S_ZIP);
            vmemcpy(pOutbuf+OFF_S_CTY_ST_D, acTmp, SIZ_S_CTY_ST_D, iTmp);
         }
      }
   }
}

/********************************* Sbd_MergeRoll *****************************
 *
 * Return 0 if successful, < 0 if error
 *        1 retired record, not use
 *
 *****************************************************************************/

int Sbd_MergeRoll(char *pOutbuf, char *pRollRec, int iLen, int iFlag)
{
   char     acTmp[256], acTmp1[256], acApn[32], *pTmp;
   LONGLONG lTmp;
   int      iRet=0, iTmp, iAlign;

   // Parse input string
   iTokens = ParseStringIQ(pRollRec, cDelim, MAX_FLD_TOKEN, apTokens);
   if (iTokens < SBD_ROLL_FLDS)
   {
      LogMsg("***** Error: bad input record for APN=%s-%s-%s-%s%s", 
         apTokens[SBD_ROLL_PAR_BOOK], apTokens[SBD_ROLL_PAR_PAGE], 
         apTokens[SBD_ROLL_PAR_LINE], apTokens[SBD_ROLL_PAR_TYPE], apTokens[SBD_ROLL_PAR_SEQ]);
      return -1;
   }

   // This is a temporary fix for 2013 LDR. Need review for next year LDR.
   iAlign = 0;
   if (iTokens > 94)
      iAlign = iTokens-94;

   // Get all records but set appropriate status
   //if ((*apTokens[SBD_ROLL_PAR_STAT] != 'A'))
   //   return 1;

   if (iFlag & CREATE_R01)
   {
      // Clear output buffer
      memset(pOutbuf, ' ', iRecLen);

      // Start copying data
      iTmp = sprintf(acApn, "%s%s%s%s%s", apTokens[SBD_ROLL_PAR_BOOK], apTokens[SBD_ROLL_PAR_PAGE], 
         apTokens[SBD_ROLL_PAR_LINE], apTokens[SBD_ROLL_PAR_TYPE], apTokens[SBD_ROLL_PAR_SEQ]);
      memcpy(pOutbuf, acApn, iTmp);

      //if ((*apTokens[SBD_ROLL_PAR_STAT] != 'A'))
      //   LogMsg0("+++ New Status: %s", apTokens[SBD_ROLL_PAR_STAT]);
      *(pOutbuf+OFF_STATUS) = *apTokens[SBD_ROLL_PAR_STAT];

      // Tax status
      *(pOutbuf+OFF_TAX_STAT) = *apTokens[SBD_ROLL_TAX_STAT];

      // Format APN
      iTmp = sprintf(acTmp, "%s-%s-%s-%s%s", apTokens[SBD_ROLL_PAR_BOOK], apTokens[SBD_ROLL_PAR_PAGE], 
         apTokens[SBD_ROLL_PAR_LINE], apTokens[SBD_ROLL_PAR_TYPE], apTokens[SBD_ROLL_PAR_SEQ]);
      memcpy(pOutbuf+OFF_APN_D, acTmp, iTmp);

      // Create MapLink and output new record
      iTmp = formatMapLink(acApn, acTmp, &myCounty);
      memcpy(pOutbuf+OFF_MAPLINK, acTmp, iTmp);

      // Create index map link
      if (getIndexPage(acTmp, acTmp1, &myCounty))
         memcpy(pOutbuf+OFF_IMAPLINK, acTmp1, strlen(acTmp1));

      // County code
      memcpy(pOutbuf+OFF_CO_NUM, "36SBD", 5);

      // Year assessed
      memcpy(pOutbuf+OFF_YR_ASSD, myCounty.acYearAssd, 4);

      // Land
      long lLand = atol(apTokens[SBD_ROLL_LAND_VALUE]);
      if (lLand > 0)
      {
         sprintf(acTmp, "%*u", SIZ_LAND, lLand);
         memcpy(pOutbuf+OFF_LAND, acTmp, SIZ_LAND);
      }

      // Improve
      long lImpr = atol(apTokens[SBD_ROLL_IMPR_VALUE]);
      if (lImpr > 0)
      {
         sprintf(acTmp, "%*u", SIZ_IMPR, lImpr);
         memcpy(pOutbuf+OFF_IMPR, acTmp, SIZ_IMPR);
      }

      long lOtherImpr = atol(apTokens[SBD_ROLL_PER_PRO_VAL]);
      if (lOtherImpr > 0)
      {
         sprintf(acTmp, "%*u", SIZ_OTHER, lOtherImpr);
         memcpy(pOutbuf+OFF_OTHER, acTmp, SIZ_OTHER);

         sprintf(acTmp, "%u         ", lOtherImpr);
         memcpy(pOutbuf+OFF_PERSPROP, acTmp, SIZ_PERSPROP);
      }

      // Gross total
      lTmp = lLand+lImpr+lOtherImpr;
      if (lTmp > 0)
      {
         sprintf(acTmp, "%*u", SIZ_GROSS, lTmp);
         memcpy(pOutbuf+OFF_GROSS, acTmp, SIZ_GROSS);
      }

      // Ratio
      if (lImpr > 0)
      {
         sprintf(acTmp, "%*u", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
         memcpy(pOutbuf+OFF_RATIO, acTmp, SIZ_RATIO);
      }

      // HO Exempt
      lTmp = atol(apTokens[SBD_ROLL_HOX_AMT]);
      iTmp = OFF_EXE_CD1;
      if (lTmp > 0)
      {
         *(pOutbuf+OFF_HO_FL) = '1';      // 'Y'
         memcpy(pOutbuf+OFF_EXE_CD1, "00", 2);
         iTmp = OFF_EXE_CD2;
      } else
         *(pOutbuf+OFF_HO_FL) = '2';      // 'N'

      // Exemp total
      lTmp += atol(apTokens[SBD_ROLL_EX1_AMT]);
      lTmp += atol(apTokens[SBD_ROLL_EX2_AMT]);
      if (lTmp > 0)
      {
         sprintf(acTmp, "%*u", SIZ_EXE_TOTAL, lTmp);
         memcpy(pOutbuf+OFF_EXE_TOTAL, acTmp, SIZ_EXE_TOTAL);

         if (*apTokens[SBD_ROLL_EX1_TYPE] > ' ')
         {
            vmemcpy(pOutbuf+iTmp, apTokens[SBD_ROLL_EX1_TYPE], SIZ_EXE_CD1);
            iTmp += SIZ_EXE_CD1;
         }
         if (*apTokens[SBD_ROLL_EX2_TYPE] > ' ')
            vmemcpy(pOutbuf+iTmp, apTokens[SBD_ROLL_EX2_TYPE], SIZ_EXE_CD1);

         // Create exemption type
         makeExeType(pOutbuf+OFF_EXE_TYPE, pOutbuf+OFF_EXE_CD1, pOutbuf+OFF_EXE_CD2, pOutbuf+OFF_EXE_CD3, (IDX_TBL4 *)&SBD_Exemption);
      }
   }

   // TRA
   lTmp = atol(apTokens[SBD_ROLL_TRA]);
   if (lTmp > 0)
   {
      // Ignore leading zero
      iTmp = sprintf(acTmp, "%.6d", lTmp);
      memcpy(pOutbuf+OFF_TRA, acTmp, iTmp);
   } else
   {
      LogMsg("*** Questionable TRA: %s", apTokens[SBD_ROLL_TRA]);
      //memcpy(pOutbuf+OFF_TRA, apTokens[SBD_ROLL_TRA], strlen(apTokens[SBD_ROLL_TRA]));
   }

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "019342302", 9))
   //   iTmp = 0;
#endif

   // Tax status
   // 1 = Assessed by county
   // 2 = Exempt
   // 3 = Assessed by SBE
   // 4 = Not assessed, common area
   // 5 = Not assessed, insufficient value
   iTmp = atol(apTokens[SBD_ROLL_TAX_STAT]);
   if (iTmp > 1)
   {
      *(pOutbuf+OFF_FULL_EXEMPT) = 'Y';
      lFullExe++;
   }

   // Legal
   char acTract[32], acLot[32], acBlk[32], acLegal[MAX_RECSIZE];

   acTract[0]=acLot[0]=acBlk[0] = 0;
   if (*apTokens[SBD_ROLL_LGL_TRACT+iAlign] > ' ')
      sprintf(acTract, "TRACT-%s", apTokens[SBD_ROLL_LGL_TRACT+iAlign]);
   if (*apTokens[SBD_ROLL_LGL_LOT+iAlign] > ' ')
      sprintf(acLot, "LOT-%s", apTokens[SBD_ROLL_LGL_LOT+iAlign]);
   if (*apTokens[SBD_ROLL_LGL_BLK+iAlign] > ' ')
      sprintf(acBlk, "BLOCK-%s", apTokens[SBD_ROLL_LGL_BLK+iAlign]);

   if (iTokens < SBD_ROLL_LGL_DESC12)
   {  // 2009
      sprintf(acTmp, "%s %s %s", acTract, acLot, acBlk);

      iTmp = blankRem(acTmp);
      memcpy(pOutbuf+OFF_LEGAL, acTmp, iTmp);
   } else if (iTokens > SBD_ROLL_LGL_DESC1)
   {  // 2010
      if (pTmp = strchr(apTokens[SBD_ROLL_LGL_DESC12], 10))
         *pTmp = 0;

      acLegal[0] = 0;
      iTmp = SBD_ROLL_LGL_DESC1+iAlign;
      while (iTmp < iTokens)
      {
         strcat(acLegal, apTokens[iTmp]);
         strcat(acLegal, " ");
         if (strlen(apTokens[iTmp]) < 3)
            break;
         iTmp++;
      }

      iTmp = replUnPrtChar(acLegal, 32, strlen(acLegal));
      if (pTmp = strchr(acLegal, '|'))
      {
         if (*(pTmp+1) == 'E')
            *pTmp = 'G';
         else 
            *pTmp = '1';
      }
      iTmp = updateLegal(pOutbuf, acLegal);
   }

   // Zoning

   // UseCode
   iTmp = strlen(apTokens[SBD_ROLL_USECODE]);
   if (iTmp > 0 && *apTokens[SBD_ROLL_USECODE] != '?')
   {
      vmemcpy(pOutbuf+OFF_USE_CO, apTokens[SBD_ROLL_USECODE], SIZ_USE_CO, iTmp);
      updateStdUse(pOutbuf+OFF_USE_STD, pOutbuf+OFF_USE_CO, iTmp, pOutbuf);
   } else
      memcpy(pOutbuf+OFF_USE_STD, USE_UNASGN, SIZ_USE_STD);

   // Acres

   // Recorded Doc

   // Owner  
   if (*apTokens[SBD_ROLL_OWNER] == '"')
      quoteRem(apTokens[SBD_ROLL_OWNER]);
   iTmp = remUnPrtChar(apTokens[SBD_ROLL_OWNER+iAlign]);
   iTmp = remUnPrtChar(apTokens[SBD_ROLL_CO_OWNER+iAlign]);

   Sbd_MergeOwner(pOutbuf, apTokens[SBD_ROLL_OWNER+iAlign], apTokens[SBD_ROLL_CO_OWNER+iAlign], 
      apTokens[SBD_ROLL_VESTING+iAlign], apTokens[SBD_ROLL_CO_VESTING+iAlign]);
   if (*apTokens[SBD_ROLL_ETAL_FLAG+iAlign] == 'Y')
      *(pOutbuf+OFF_ETAL_FLG) = 'Y';

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "0483202060000", 12))
   //   iTmp = 0;
#endif

   // Merge Situs
   iTmp = remUnPrtChar(apTokens[SBD_ROLL_SITUS_NAME]);
   Sbd_MergeSAdr1(pOutbuf, apTokens[SBD_ROLL_SITUS_NBR], apTokens[SBD_ROLL_SITUS_DIR], 
      apTokens[SBD_ROLL_SITUS_NAME], apTokens[SBD_ROLL_SITUS_SUFF], apTokens[SBD_ROLL_SITUS_QUAL], 
      apTokens[SBD_ROLL_SITUS_COMM+iAlign], apTokens[SBD_ROLL_SITUS_ZIP1+iAlign]);

   // Fill in Situs zip
   if (*(pOutbuf+OFF_S_ZIP) == ' ' && sPrevZip[0] == '9' &&
      !memcmp(apTokens[SBD_ROLL_SITUS_COMM+iAlign], sPrevCity, 3) )
   {
      vmemcpy(pOutbuf+OFF_S_ZIP, sPrevZip, 5);
   } else
   {
      strcpy(sPrevCity, apTokens[SBD_ROLL_SITUS_COMM+iAlign]);
      strcpy(sPrevZip, apTokens[SBD_ROLL_SITUS_ZIP1+iAlign]);
   }

   // Mailing
   Sbd_MergeMAdr(pOutbuf, apTokens[SBD_ROLL_DBA_OWNER], apTokens[SBD_ROLL_CAREOF], apTokens[SBD_ROLL_MAIL_LINE1], 
      apTokens[SBD_ROLL_MAIL_LINE2], apTokens[SBD_ROLL_MAIL_ZIP1], apTokens[SBD_ROLL_MAIL_ZIP2]);

   return 0;
}

/******************************** Sbd_MergeOwnerX ****************************
 *
 * Verify extracted LDR owner with updated version.  If they are differed, update them.
 *
 *****************************************************************************/

int Sbd_MergeOwnerX(char *pOutbuf, char *pOwner, char *pDba)
{
   static char acRec[256], *pRec=NULL;
   int         iLoop;
   OWNEREX     *OwnerEx = (OWNEREX *)&acRec[0];

   if (!fdOwner)
      return 1;

   // Get first Vac rec for first call
   if (!pRec)
      pRec = fgets(acRec,  256, fdOwner);

   do
   {
      iLoop = memcmp(pOutbuf, acRec, iApnLen);
      if (iLoop > 0)
      {
         if (bDebug)
            LogMsg0("*** Skip owner: %.14s", acRec);

         pRec = fgets(acRec,  256, fdOwner);
         if (!pRec)
         {
            fclose(fdOwner);
            fdOwner = NULL;
            return 1;      // EOF
         }
      }
   } while (iLoop > 0);

   // If not match, return
   if (iLoop)
      return 1;

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "004191410", 9))
   //   iLoop = 0;
#endif
   //char *pTmp;
   //if (pTmp = strchr(pOwner, ','))
   //   *pTmp = ' ';
   replChar(pOwner, ',', ' ');
   blankRem(pOwner);

   // Replace owner if they are differred
   if (memcmp(OwnerEx->Owners[0], pOwner, strlen(pOwner)))
      Sbd_MergeOwner1(pOutbuf, pOwner, pDba);

   // Get next Char rec
   pRec = fgets(acRec,  256, fdOwner);

   return 0;
}

/***************************** Sbd_MergeUpdRoll ******************************
 *
 * Return 0 if successful, < 0 if error
 *        1 retired record, not use
 *
 *****************************************************************************/

int Sbd_MergeUpdRoll(char *pOutbuf, char *pRollRec, int iFlag)
{
   char     acTmp[256], acTmp1[256];
   LONGLONG lTmp;
   int      iRet=0, iTmp;

   // Parse input line
   iTokens = ParseStringIQ(pRollRec, cDelim, MAX_FLD_TOKEN, apTokens);
   if (iTokens < SBD_RU_BASE_YEAR)
   {
      LogMsg("***** Error: bad input record for APN=%s", apTokens[SBD_RU_PARCEL]);
      return -1;
   }

   if (iFlag & CREATE_R01)
   {
      // Clear output buffer
      memset(pOutbuf, ' ', iRecLen);

      // Start copying data
      memcpy(pOutbuf, apTokens[SBD_RU_PARCEL], iApnLen);
      *(pOutbuf+OFF_STATUS) = 'A';

      // Format APN
      iTmp = formatApn(apTokens[SBD_RU_PARCEL], acTmp, &myCounty);
      memcpy(pOutbuf+OFF_APN_D, acTmp, iTmp);

      // Create MapLink and output new record
      iTmp = formatMapLink(apTokens[SBD_RU_PARCEL], acTmp, &myCounty);
      memcpy(pOutbuf+OFF_MAPLINK, acTmp, iTmp);

      // Create index map link
      if (getIndexPage(acTmp, acTmp1, &myCounty))
         memcpy(pOutbuf+OFF_IMAPLINK, acTmp1, strlen(acTmp1));

      // County code
      memcpy(pOutbuf+OFF_CO_NUM, "36SBD", 5);

      // Year assessed
      memcpy(pOutbuf+OFF_YR_ASSD, myCounty.acYearAssd, 4);

      // Land
      long lLand = atol(apTokens[SBD_RU_LAND_VALUE]);
      if (lLand > 0)
      {
         sprintf(acTmp, "%*u", SIZ_LAND, lLand);
         memcpy(pOutbuf+OFF_LAND, acTmp, SIZ_LAND);
      }

      // Improve
      long lImpr = atol(apTokens[SBD_RU_IMPR_VALUE]);
      if (lImpr > 0)
      {
         sprintf(acTmp, "%*u", SIZ_IMPR, lImpr);
         memcpy(pOutbuf+OFF_IMPR, acTmp, SIZ_IMPR);
      }

      long lOtherImpr = atol(apTokens[SBD_RU_PERSPROP_VALUE]);
      if (lOtherImpr > 0)
      {
         sprintf(acTmp, "%*u", SIZ_OTHER, lOtherImpr);
         memcpy(pOutbuf+OFF_OTHER, acTmp, SIZ_OTHER);

         sprintf(acTmp, "%d         ", lOtherImpr);
         memcpy(pOutbuf+OFF_PERSPROP, acTmp, SIZ_PERSPROP);
      }

      // Gross total
      LONGLONG lGross = lLand+lImpr+lOtherImpr;
      if (lGross > 0)
      {
         sprintf(acTmp, "%*u", SIZ_GROSS, lGross);
         memcpy(pOutbuf+OFF_GROSS, acTmp, SIZ_GROSS);
      }

      // Ratio
      if (lImpr > 0)
      {
         sprintf(acTmp, "%*u", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
         memcpy(pOutbuf+OFF_RATIO, acTmp, SIZ_RATIO);
      }

      // HO Exempt
      if (*apTokens[SBD_RU_HOX_INDICATOR] == '1')
         *(pOutbuf+OFF_HO_FL) = '1';      // 'Y'
      else
         *(pOutbuf+OFF_HO_FL) = '2';      // 'N'

      // Exemp total
      lTmp = atol(apTokens[SBD_RU_EXEM_VALUE]);
      if (lTmp > 0)
      {
         sprintf(acTmp, "%*u", SIZ_EXE_TOTAL, lTmp);
         memcpy(pOutbuf+OFF_EXE_TOTAL, acTmp, SIZ_EXE_TOTAL);
      }

      if (lTmp > lGross)
         LogMsg("??? Exe > Gross apn=%.13s", pOutbuf);

      // TRA
      lTmp = atol(apTokens[SBD_RU_TRA]);
      if (lTmp > 0)
      {
         // Ignore leading zero
         iTmp = sprintf(acTmp, "%.6d", lTmp);
         memcpy(pOutbuf+OFF_TRA, acTmp, iTmp);
      } else if (*apTokens[SBD_RU_TRA] > '0')
      {
         LogMsg("*** Questionable TRA: %s", apTokens[SBD_RU_TRA]);
      }

      // Tax status
      iTmp = atol(apTokens[SBD_RU_TAX_STATUS]);
      if (iTmp > 1)
      {
         *(pOutbuf+OFF_FULL_EXEMPT) = 'Y';
         lFullExe++;
      }

      // Legal
      char acLegal[512], *pTmp;
      if (*(pOutbuf+OFF_LEGAL) == ' ')
      {
         acLegal[0] = 0;
         strcpy(acLegal, apTokens[SBD_RU_LEG1]);
         strcat(acLegal, apTokens[SBD_RU_LEG2]);
         strcat(acLegal, apTokens[SBD_RU_LEG3]);
         strcat(acLegal, apTokens[SBD_RU_LEG4]);

         iTmp = replUnPrtChar(acLegal, 32, 0);
         if (pTmp = strchr(acLegal, '|'))
         {
            if (*(pTmp+1) == 'E')
               *pTmp = 'G';
            else 
               *pTmp = '1';
         }
         iTmp = updateLegal(pOutbuf, acLegal);
      }
   } 
   
   // UseCode
   iTmp = strlen(apTokens[SBD_RU_USECODE]);
   if (iTmp > 0 && *apTokens[SBD_RU_USECODE] != '?')
   {
      vmemcpy(pOutbuf+OFF_USE_CO, apTokens[SBD_RU_USECODE], SIZ_USE_CO, iTmp);
      updateStdUse(pOutbuf+OFF_USE_STD, apTokens[SBD_RU_USECODE], iTmp, pOutbuf);
   } else
      memcpy(pOutbuf+OFF_USE_STD, USE_UNASGN, SIZ_USE_STD);

   // Acres
   if (*apTokens[SBD_RU_AREA_ACRE])
   {
      double dAcres;
      long   lLotAcres, lLotSqft;

      dAcres = atof(apTokens[SBD_RU_AREA_ACRE]);
      lLotAcres = (long)(dAcres*1000.0);
      lLotSqft  = (long)(SQFT_FACTOR_1000*lLotAcres);

      if (lLotSqft > 0)
      {
         sprintf(acTmp, "%*u", SIZ_LOT_SQFT, lLotSqft);
         memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);
         sprintf(acTmp, "%*u", SIZ_LOT_ACRES, lLotAcres);
         memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);
      }
   }

   // Recorded Doc - Update in Load_Roll by Sbd_MergeOwnerXfer()

   // Owner
   Sbd_MergeOwner1(pOutbuf, apTokens[SBD_RU_NAME], apTokens[SBD_RU_DBA_NAME]);

   // Merge Situs
   Sbd_MergeSAdr1(pOutbuf, apTokens[SBD_RU_SITUS_NBR], apTokens[SBD_RU_SITUS_DIR], 
      apTokens[SBD_RU_SITUS_NAME], apTokens[SBD_RU_SITUS_SUFF], apTokens[SBD_RU_SITUS_QUAL], 
      apTokens[SBD_RU_SITUS_COMM], apTokens[SBD_RU_SITUS_ZIP]);

   // Fill in Situs zip
   if (*(pOutbuf+OFF_S_ZIP) == ' ' && sPrevZip[0] == '9' &&
      !memcmp(apTokens[SBD_RU_SITUS_COMM], sPrevCity, 3) )
   {
      vmemcpy(pOutbuf+OFF_S_ZIP, sPrevZip, 5);
   } else if (*apTokens[SBD_RU_SITUS_COMM] > ' ' && *apTokens[SBD_RU_SITUS_ZIP] == '9')
   {
      strcpy(sPrevCity, apTokens[SBD_RU_SITUS_COMM]);
      strcpy(sPrevZip, apTokens[SBD_RU_SITUS_ZIP]);
   }

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "0281321180000", 12))
   //   iTmp = 0;
#endif

   // Mailing
   Sbd_MergeMAdr1(pOutbuf, apTokens[SBD_RU_MAIL_LINE1], apTokens[SBD_RU_MAIL_LINE2], 
      myTrim(apTokens[SBD_RU_MAIL_ZIP]), apTokens[SBD_RU_SITUS_SUFF]);

   return 0;
}

/********************************* Sbd_MergeLegal ****************************
 *
 * Use actual sqft.  If not avail, check for usable sqft.
 *
 *****************************************************************************/

int Sbd_MergeLegal(char *pOutbuf)
{
   static char acRec[4096], *pRec=NULL;
   int         iLoop;

   // Get first Vac rec for first call
   if (!pRec)
      pRec = fgets(acRec,  4096, fdLegal);

   do
   {
      iLoop = memcmp(pOutbuf, acRec, iApnLen);
      if (iLoop > 0)
      {
         if (bDebug)
            LogMsg0("*** Skip legal: %.13s", acRec);

         pRec = fgets(acRec,  4096, fdLegal);
         if (!pRec)
         {
            fclose(fdLegal);
            fdLegal = NULL;
            return 1;      // EOF
         }
      }
   } while (iLoop > 0);

   // If not match, return
   if (iLoop)
      return 1;

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "004191410", 9))
   //   iLoop = 0;
#endif

   if (acRec[myCounty.iApnLen+1] > ' ')
   {
      updateLegal(pOutbuf, &acRec[myCounty.iApnLen+1]);
      lLegalMatch++;
   }

#ifdef _DEBUG
   /*
   char *pTmp = &acRec[myCounty.iApnLen+1];
   int   iTmp;

   if ((iTmp = strlen(pTmp)) > 240)
      LogMsg("%.*s: %d", myCounty.iApnLen, acRec, iTmp);
   */
#endif

   // Get next Char rec
   pRec = fgets(acRec,  4096, fdLegal);

   return 0;
}

/***************************** Sbd_MergeOwnerXfer ****************************
 *
 * Update transfer Date & DocNum using Owner Transfer file.
 * Only populate if current record is blank.
 *
 *****************************************************************************/

int Sbd_MergeOwnerXfer(char *pOutbuf)
{
   static char acRec[1024], *pRec=NULL;
   int         iLoop, iTmp;

   OWNEREX     *pOwner = (OWNEREX *)&acRec;

   // Get first Vac rec for first call
   if (!pRec)
      pRec = fgets(acRec,  1024, fdOwner);

   do
   {
      iLoop = memcmp(pOutbuf, acRec, iApnLen);
      if (iLoop > 0)
      {
         if (bDebug)
            LogMsg0("*** Skip owner rec: %.13s", acRec);

         pRec = fgets(acRec,  1024, fdOwner);
         if (!pRec)
         {
            fclose(fdOwner);
            fdOwner = NULL;
            return 1;      // EOF
         }
      }
   } while (iLoop > 0);

   // If not match, return
   if (iLoop)
      return 1;

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "0108301010000", 9))
   //   iTmp = 1;
#endif

   if (pOwner->XferDate[0] > ' ' && !memcmp(pOwner->XferDate, pOwner->XferDoc, 4))
   {
      memcpy(pOutbuf+OFF_TRANSFER_DT, pOwner->XferDate, SIZ_TRANSFER_DT);
      memcpy(pOutbuf+OFF_TRANSFER_DOC, pOwner->XferDoc, SIZ_TRANSFER_DOC);
      lUpdateXfer++;
   }

   // Get next Char rec
   pRec = fgets(acRec,  1024, fdOwner);

   return 0;
}

/********************************* Sdx_MergeProp8 ****************************
 *
 * Set Prop8 flag using prop8 file from the county
 *
 *****************************************************************************/

int Sbd_MergeProp8(char *pOutbuf)
{
   static   char acRec[1024], *pRec=NULL;
   int      iLoop;

   // Get first Char rec for first call
   if (!pRec)
   {
      // Skip header
      pRec = fgets(acRec, 1024, fdProp8);
      if (acRec[0] > '9')
         pRec = fgets(acRec, 1024, fdProp8);
   }

   do
   {
      if (!pRec)
      {
         fclose(fdProp8);
         fdProp8 = NULL;
         return 1;      // EOF
      }

      // Compare Apn
      iLoop = memcmp(pOutbuf, acRec, iApnLen);
      if (iLoop > 0)
      {
         if (bDebug)
            LogMsg0("Skip Prop8 rec  %.*s", iApnLen, acRec);
         pRec = fgets(acRec, 1024, fdProp8);
         lProp8Skip++;
      }
   } while (iLoop > 0);

   // If not match, return
   if (iLoop)
      return 1;

   // Prop 8
   *(pOutbuf+OFF_PROP8_FLG) = 'Y';

   lProp8Match++;

   // Get next Char rec
   pRec = fgets(acRec, 1024, fdProp8);

   return 0;
}

/****************************** Sbd_MergeStdChar ****************************
 *
 *
 ****************************************************************************/

int Sbd_MergeStdChar(char *pOutbuf)
{
   static   char acRec[2048], *pRec=NULL;
   char     acTmp[256];
   long     lBldgSqft, lGarSqft, lCarSqft, lTmp;
   int      iLoop, iBeds, iFBath, iHBath, iTmp;
   STDCHAR *pChar;

   // Get first Char rec for first call
   if (!pRec)
      pRec = fgets(acRec, 1024, fdChar);

   pChar = (STDCHAR *)pRec;

   do
   {
      if (!pRec)
      {
         fclose(fdChar);
         fdChar = NULL;
         return 1;      // EOF
      }

      // Compare APN
      iLoop = memcmp(pOutbuf, pRec, iApnLen);
      if (iLoop > 0)
      {
         if (bDebug)
            LogMsg0("Skip Char rec  %.*s", iApnLen, pRec);
         pRec = fgets(acRec, 1024, fdChar);
         lCharSkip++;
      }
   } while (iLoop > 0);

   // If not match, return
   if (iLoop)
      return 1;

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "010832425000", 9))
   //   iLoop = 0;
#endif

   // YrBlt
   lTmp = atoin(pChar->YrBlt, SIZ_CHAR_YRBLT);
   if (lTmp > 1600 && lTmp <= lToyear)
      memcpy(pOutbuf+OFF_YR_BLT, pChar->YrBlt, SIZ_YR_BLT);

   // YrEff
   lTmp = atoin(pChar->YrEff, SIZ_CHAR_YRBLT);
   if (lTmp > 1600 && lTmp <= lToyear)
      memcpy(pOutbuf+OFF_YR_EFF, pChar->YrEff, SIZ_YR_BLT);

   // Lot Sqft
   lTmp = atoin(pChar->LotSqft, SIZ_CHAR_SQFT);
   if (lTmp > 10)
   {
      sprintf(acTmp, "%*u", SIZ_LOT_SQFT, lTmp);
      memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);
   }

   // Lot Acres
   lTmp = atoin(pChar->LotAcre, SIZ_CHAR_SQFT);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*u", SIZ_LOT_ACRES, lTmp);
      memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);
   }

   // BldgSqft
   lBldgSqft = atoin(pChar->BldgSqft, SIZ_CHAR_SQFT);
   if (lBldgSqft > 10)
   {
      sprintf(acTmp, "%*u", SIZ_BLDG_SF, lBldgSqft);
      memcpy(pOutbuf+OFF_BLDG_SF, acTmp, SIZ_BLDG_SF);
   }

   // Deck Sqft
   vmemcpy(pOutbuf+OFF_DECK_SF, pChar->DeckSqft, SIZ_DECK_SF, SIZ_CHAR_SQFT);

   // Porch Sqft
   vmemcpy(pOutbuf+OFF_PORCH_SF, pChar->PorchSqft, SIZ_PORCH_SF, SIZ_CHAR_SQFT);

   // Garage Sqft
   lGarSqft = atoin(pChar->GarSqft, SIZ_CHAR_SQFT);
   lCarSqft = atoin(pChar->CarpSqft, SIZ_CHAR_SQFT);
   if (lGarSqft > 10)
   {
      sprintf(acTmp, "%*u", SIZ_GAR_SQFT, lGarSqft);
      memcpy(pOutbuf+OFF_GAR_SQFT, acTmp, SIZ_GAR_SQFT);
   } else if (lCarSqft > 10)
   {
      sprintf(acTmp, "%*u", SIZ_GAR_SQFT, lCarSqft);
      memcpy(pOutbuf+OFF_GAR_SQFT, acTmp, SIZ_GAR_SQFT);
   }
   *(pOutbuf+OFF_PARK_TYPE) = pChar->ParkType[0];  

   // Parking spaces
   iTmp = atoin(pChar->ParkSpace, SIZ_CHAR_SIZE4);
   if (iTmp > 0)
   {
      sprintf(acTmp, "%d      ", iTmp);
      memcpy(pOutbuf+OFF_PARK_SPACE, acTmp, SIZ_PARK_SPACE);
   }

   // Beds
   iBeds = atoin(pChar->Beds, SIZ_CHAR_BEDS);
   if (iBeds > 0)
   {
      sprintf(acTmp, "%*u", SIZ_BEDS, iBeds);
      memcpy(pOutbuf+OFF_BEDS, acTmp, SIZ_BEDS);
   }

   // Bath
   iFBath = atoin(pChar->FBaths, SIZ_CHAR_BATHS);
   if (iFBath > 0)
   {
      sprintf(acTmp, "%*u", SIZ_BATH_F, iFBath);
      memcpy(pOutbuf+OFF_BATH_F, acTmp, SIZ_BATH_F);
      memcpy(pOutbuf+OFF_BATH_4Q, acTmp, SIZ_BATH_F);
   }

   // Half bath
   iHBath = atoin(pChar->HBaths, SIZ_CHAR_BATHS);
   if (iHBath > 0)
   {
      sprintf(acTmp, "%*u", SIZ_BATH_H, iHBath);
      memcpy(pOutbuf+OFF_BATH_H, acTmp, SIZ_BATH_H);
      if (pChar->Bath_1Q[0] > ' ')
         *(pOutbuf+OFF_BATH_1Q) = pChar->Bath_1Q[0];
      else if (pChar->Bath_2Q[0] > ' ')
         *(pOutbuf+OFF_BATH_2Q) = pChar->Bath_2Q[0];
      else if (pChar->Bath_3Q[0] > ' ')
         *(pOutbuf+OFF_BATH_3Q) = pChar->Bath_3Q[0];
   }

   // Total rooms
   iTmp = atoin(pChar->Rooms, SIZ_CHAR_ROOMS);
   if (iTmp > 0)
   {
      sprintf(acTmp, "%*u", SIZ_ROOMS, iTmp);
      memcpy(pOutbuf+OFF_ROOMS, acTmp, SIZ_ROOMS);
   }

   // Units
   iTmp = atoin(pChar->Units, SIZ_CHAR_UNITS);
   if (iTmp > 0)
   {
      sprintf(acTmp, "%*u", SIZ_UNITS, iTmp);
      memcpy(pOutbuf+OFF_UNITS, acTmp, SIZ_UNITS);
   }

   // Stories
   vmemcpy(pOutbuf+OFF_STORIES, pChar->Stories, SIZ_STORIES, SIZ_CHAR_SIZE4);

   // Fire place
   vmemcpy(pOutbuf+OFF_FIRE_PL, pChar->Fireplace, SIZ_FIRE_PL, SIZ_CHAR_SIZE2);

   // Quality Class
   *(pOutbuf+OFF_BLDG_CLASS) = pChar->BldgClass;
   *(pOutbuf+OFF_BLDG_QUAL) = pChar->BldgQual;
   memcpy(pOutbuf+OFF_QUALITYCLASS, pChar->QualityClass, SIZ_CHAR_QCLS);

   // Pools
   *(pOutbuf+OFF_POOL) = pChar->Pool[0];

   // View
   *(pOutbuf+OFF_VIEW) = pChar->View[0];

   // Water
   *(pOutbuf+OFF_WATER) = pChar->Water;

   // Sewer
   *(pOutbuf+OFF_SEWER) = pChar->Sewer;

   // Elevator
   *(pOutbuf+OFF_ELEVATOR) = pChar->HasElevator;

   // Heat
   *(pOutbuf+OFF_HEAT) = pChar->Heating[0];

   // Cool
   *(pOutbuf+OFF_AIR_COND) = pChar->Cooling[0];

   lCharMatch++;

   return 0;
}

/******************************** Sbd_ExtrLien ******************************
 *
 * Extract value from npp.publicuc.secwleg.txt
 *
 ****************************************************************************/

int Sbd_CreateLienRec(char *pOutbuf, char *pRollRec)
{
   char     acTmp[256], acApn[32];
   int      iTmp;
   LONGLONG lTmp;
   LIENEXTR *pLienRec = (LIENEXTR *)pOutbuf;

   // Parse input string
   iTokens = ParseStringIQ(pRollRec, cDelim, MAX_FLD_TOKEN, apTokens);
   if (iTokens < SBD_ROLL_FLDS)
   {
      LogMsg("***** Error: bad input record for APN=%s-%s-%s-%s%s", 
         apTokens[SBD_ROLL_PAR_BOOK], apTokens[SBD_ROLL_PAR_PAGE], 
         apTokens[SBD_ROLL_PAR_LINE], apTokens[SBD_ROLL_PAR_TYPE], apTokens[SBD_ROLL_PAR_SEQ]);
      return -1;
   }

   // Clear output buffer
   memset(pOutbuf, ' ', sizeof(LIENEXTR));

   // Start copying data
   iTmp = sprintf(acApn, "%s%s%s%s%s", apTokens[SBD_ROLL_PAR_BOOK], apTokens[SBD_ROLL_PAR_PAGE], 
      apTokens[SBD_ROLL_PAR_LINE], apTokens[SBD_ROLL_PAR_TYPE], apTokens[SBD_ROLL_PAR_SEQ]);
   memcpy(pLienRec->acApn, acApn, iTmp);

   // TRA
   lTmp = atol(apTokens[SBD_ROLL_TRA]);
   if (lTmp > 0)
   {
      // Ignore leading zero
      iTmp = sprintf(acTmp, "%.6d", lTmp);
      memcpy(pLienRec->acTRA, acTmp, iTmp);
   }

   // Year assessed
   memcpy(pLienRec->acYear, myCounty.acYearAssd, 4);

   // Land
   long lLand = atol(apTokens[SBD_ROLL_LAND_VALUE]);
   if (lLand > 0)
   {
      sprintf(acTmp, "%*u", SIZ_LAND, lLand);
      memcpy(pLienRec->acLand, acTmp, SIZ_LAND);
   }

   // Improve
   long lImpr = atol(apTokens[SBD_ROLL_IMPR_VALUE]);
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*u", SIZ_IMPR, lImpr);
      memcpy(pLienRec->acImpr, acTmp, SIZ_LIEN_IMPR);
   }

   // Other value
   long lPers = atol(apTokens[SBD_ROLL_PER_PRO_VAL]);
   if (lPers > 0)
   {
      sprintf(acTmp, "%*u", SIZ_OTHER, lPers);
      memcpy(pLienRec->acOther, acTmp, SIZ_LIEN_PERS);
      sprintf(acTmp, "%u         ", lPers);
      memcpy(pLienRec->acPP_Val, acTmp, SIZ_LIEN_PERS);
   }

   // Gross
   LONGLONG lGross = lPers + lLand + lImpr;
   if (lGross > 0)
   {
      sprintf(acTmp, "%*u", SIZ_LIEN_GROSS, lGross);
      memcpy(pLienRec->acGross, acTmp, SIZ_LIEN_GROSS);
   }

   // Impr - Ratio
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*u", SIZ_LIEN_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
      memcpy(pLienRec->acRatio, acTmp, SIZ_LIEN_RATIO);
   }

   // HO Exempt
   long lHO_Exe = atol(apTokens[SBD_ROLL_HOX_AMT]);
   if (lHO_Exe > 0)
      pLienRec->acHO[0] = '1';         // 'Y'

   // Exemp total
   lTmp = atol(apTokens[SBD_ROLL_EX1_AMT]);
   lTmp += atol(apTokens[SBD_ROLL_EX2_AMT]);
   if (lTmp > 0 && !lHO_Exe)
      pLienRec->acHO[0] = '2';
   else
      lTmp += lHO_Exe;
   if (lTmp > 0)
   {
      iTmp = sprintf(acTmp, "%*u", sizeof(pLienRec->acExAmt), lTmp);
      memcpy(pLienRec->acExAmt, acTmp, iTmp);
   }

   pLienRec->LF[0] = '\n';
   pLienRec->LF[1] = 0;

   return 0;
}

int Sbd_ExtrLien()
{
   char     *pTmp, acBuf[MAX_RECSIZE], acRollRec[MAX_RECSIZE];
   char     acOutFile[_MAX_PATH];
   FILE     *fdLien;
   int      iRet;
   long     lCnt=0;

   LogMsg0("Extract LDR value");

   // Open roll file
   LogMsg("Open Roll file %s", acRollFile);
   fdRoll = fopen(acRollFile, "r");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", acRollFile);
      return -1;
   }

   // Open Output file
   sprintf(acOutFile, acLienTmpl, myCounty.acCntyCode, myCounty.acCntyCode);
   LogMsg("Open lien extract file %s", acOutFile);
   fdLien = fopen(acOutFile, "w");
   if (fdLien == NULL)
   {
      LogMsg("***** Error creating lien extract file: %s\n", acOutFile);
      return -2;
   }

   // Merge loop
   while (!feof(fdRoll))
   {
      // Get roll rec
      pTmp = fgets(acRollRec, iRollLen, fdRoll);

      if (!pTmp)
         break;

      // Create new R01 record
      iRet = Sbd_CreateLienRec(acBuf, acRollRec);

      // Write to output
      fputs(acBuf, fdLien);
      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdRoll)
      fclose(fdRoll);
   if (fdLien)
      fclose(fdLien);

   LogMsg("Total output records:       %u", lCnt);
   printf("\nTotal output records: %u", lCnt);

   lRecCnt = lCnt;
   return 0;

}
/******************************** Sbd_Load_LDR ******************************
 *
 * 
 *
 ****************************************************************************/

int Sbd_Load_LDR(int iFirstRec /* 1=create header rec */)
{
   char     *pTmp, acBuf[MAX_RECSIZE], acRec[MAX_RECSIZE];
   char     acOutFile[_MAX_PATH], acLegalFile[_MAX_PATH];

   HANDLE   fhOut;

   int      iRet, iRollSkip;
   DWORD    nBytesWritten;
   BOOL     bRet, bEof;
   long     lRet, lCnt=0;

   // Open roll file
   LogMsg("Open Roll file %s", acRollFile);
   fdRoll = fopen(acRollFile, "r");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", acRollFile);
      return -1;
   }

   // Open Prop8 file
   sprintf(acProp8File, acProp8Tmpl, myCounty.acCntyCode, myCounty.acCntyCode, myCounty.acYearAssd);
   if (acProp8File[0] > ' ' && !_access(acProp8File, 0))
   {
      LogMsg("Open Prop8 file %s", acProp8File);
      fdProp8 = fopen(acProp8File, "r");
      if (fdProp8 == NULL)
      {
         LogMsg("***** Error opening Prop8 file: %s\n", acProp8File);
         return -2;
      }
      lProp8Skip=lProp8Match=0;
   } else
   {
      fdProp8 = NULL;
      LogMsg("*** Prop8 file not available (%s).  Skip updating Prop8 flag", acProp8File);
   }

   // Check legal file
   iRet = GetIniString(myCounty.acCntyCode, "LegalExtr", "", acLegalFile, _MAX_PATH, acIniFile);
   if (iRet > 10 && !_access(acLegalFile, 0))
   {
      fdLegal = fopen(acLegalFile, "r");
      if (fdLegal == NULL)
      {
         LogMsg("***** Error opening legal file: %s\n", acLegalFile);
         return -2;
      }
   } else
      fdLegal = NULL;

   // Open Char file
   LogMsg("Open Char file %s", acCChrFile);
   fdChar = fopen(acCChrFile, "r");
   if (fdChar == NULL)
   {
      LogMsg("***** Error opening acreage file: %s\n", acCChrFile);
      return -2;
   }

   // Open Output file
   sprintf(acOutFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "R01");
   LogMsg("Create output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error creating output file: %s\n", acOutFile);
      return -4;
   }

   // Output first header record
   if (iFirstRec > 0)
   {
      memset(acBuf, '9', iRecLen);
      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
   }

   // Drop header record - file already sorted and header dropped
   //pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdRoll);
   // Get 1st rec
   pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdRoll);
   bEof = (pTmp ? false:true);

   // Init variables
   lFullExe=iRollSkip=iNoMatch=iBadCity=iBadSuffix=0;

   // Merge loop
   while (pTmp && !feof(fdRoll))
   {
      // Create new R01 record
      iRet = Sbd_MergeRoll(acBuf, acRec, MAX_RECSIZE, CREATE_R01);
      if (!iRet)
      {
#ifdef _DEBUG
         //iRet = replCharEx(acBuf, 31, ' ', iRecLen);
         //if (iRet)
         //   iRet = 0;
#endif
         // Merge Prop8
         if (fdProp8)
            iRet = Sbd_MergeProp8(acBuf);

         // Merge legal
         if (fdLegal)
            Sbd_MergeLegal(acBuf);

         // Merge Char
         if (fdChar)
            Sbd_MergeStdChar(acBuf);

         // Save last recording date
         //lRet = atoin((char *)&acBuf[OFF_TRANSFER_DT], 8);
         //if (lRet > lLastRecDate && lRet < lToday)
         //   lLastRecDate = lRet;

         lLDRRecCount++;
         bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);

         if (!bRet)
         {
            LogMsg("Error writing to output file at record %d\n", lCnt);
            lRet = WRITE_ERR;
            break;
         }
      } else if (iRet == -1)
         LogMsg("*** Invalid record %.26s", acRec);
      else if (iRet == 1)
         iRollSkip++;


      // Get next roll record
      pTmp = fgets(acRec, MAX_RECSIZE, fdRoll);
      if (pTmp)
      {
         iRet = strlen(pTmp);
         // Test for possible line break
         if (acRec[iRet-2] != '~')
         {
            // Read more
            lRet = ftell(fdRoll);
            pTmp = fgets(acBuf, MAX_RECSIZE, fdRoll);
            if (pTmp && *(pTmp+4) != '~')
            {
               strncpy(&acRec[iRet-1], acBuf, MAX_RECSIZE-iRet);
               pTmp = acRec;
            } else
            {
               iRet = fseek(fdRoll, lRet, SEEK_SET);            
            }
         }
      }
      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdRoll)
      fclose(fdRoll);
   if (fdProp8)
      fclose(fdProp8);
   if (fdLegal)
      fclose(fdLegal);
   if (fdChar)
      fclose(fdChar);
   if (fhOut)
      CloseHandle(fhOut);

   LogMsg("Total input records:        %u", lCnt);
   LogMsg("     output records:        %u", lLDRRecCount);
   LogMsg("    records skipped:        %u", iRollSkip);
   LogMsg("       Char matched:        %u", lCharMatch);
   LogMsg("       Char skipped:        %u", lCharSkip);
   LogMsg("      Prop8 matched:        %u", lProp8Match);
   LogMsg("      Prop8 skipped:        %u", lProp8Skip);

   if (fdLegal)
      LogMsg("Total legal matched:        %u", lLegalMatch);
   LogMsg("Total Full exempt records:  %u", lFullExe);
   LogMsg("Total bad-city records:     %u", iBadCity);
   LogMsg("Total bad-suffix records:   %u\n", iBadSuffix);
   LogMsg("Last recording date:        %u", lLastRecDate);

   lRecCnt = lLDRRecCount;
   return 0;
}

/******************************** Sbd_Load_Roll *****************************
 *
 * Load update roll file.  This file is not in the same format as LDR file
 * and number of fields is also different.
 * Use Owner file to update transfer info.
 * 02/24/2020 Drop use of owner file to update transfer since it's not always transfer.
 *            It contains last document file on the parcel.  It may be deed of trust.
 * 04/06/2020 Paul asks to put this back in.
 *
 ****************************************************************************/

int Sbd_Load_Roll(int iSkip /* 1=create header rec */)
{
   char     *pTmp, acRec[MAX_RECSIZE], acBuf[MAX_RECSIZE];
   char     acRawFile[_MAX_PATH], acOutFile[_MAX_PATH], acRollRec[MAX_RECSIZE];

   HANDLE   fhIn, fhOut;

   int      iApnLen, iTmp, iRollUpd=0, iNewRec=0, iRetiredRec=0;
   DWORD    nBytesRead;
   DWORD    nBytesWritten;
   BOOL     bRet, bEof;
   long     lRet=0, lCnt=0;

   lUpdateXfer = 0;
   iApnLen = myCounty.iApnLen;
   sprintf(acRawFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "S01");
   sprintf(acOutFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "R01");

   // Rename files for processing
   if (_access(acRawFile, 0))
   {
      if (!_access(acOutFile, 0))
         rename(acOutFile, acRawFile);
      else
      {
         LogMsg("***** Missing input file %s.  Please recheck!", acRawFile);
         return -1;
      }
   }

   // Open Char file
   LogMsg("Open Char file %s", acCChrFile);
   fdChar = fopen(acCChrFile, "r");
   if (fdChar == NULL)
   {
      LogMsg("***** Error opening acreage file: %s\n", acCChrFile);
      return -2;
   }

   // Open owner file
   sprintf(acRec, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "Owner");
   LogMsg("Open Owner file %s", acRec);
   fdOwner = fopen(acRec, "r");
   if (fdOwner == NULL)
   {
      LogMsg("***** Error opening Owner file: %s.  Ignore updating transfer from owner file.\n", acRec);
   }

   // Open Roll file
   LogMsg("Open Roll file %s", acRollFile);
   fdRoll = fopen(acRollFile, "r");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", acRollFile);
      return -3;
   }

   // Open Input file
   LogMsg("Open input file %s", acRawFile);
   fhIn = CreateFile(acRawFile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);
   if (fhIn == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening input file: %s\n", acRawFile);
      return -4;
   }

   // Open Output file
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening output file: %s\n", acOutFile);
      return -5;
   }

   //Skip header
   //pTmp = fgets((char *)&acRollRec[0], MAX_RECSIZE, fdRoll);

   // Get first RollRec
   pTmp = fgets((char *)&acRollRec[0], MAX_RECSIZE, fdRoll);
   bEof = (pTmp ? false:true);

   // Copy skip record
   memset(acBuf, ' ', iRecLen);
   while (iSkip-- > 0)
   {
      ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);
      // Ignore record start with 99999999
      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
   }

   iNoMatch=iBadCity=iBadSuffix=0;

   // Merge loop
   while (!bEof)
   {
      bRet = ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);

      // Check for EOF
      if (!bRet)
      {
         LogMsg("***** Error reading input file %s (%f)", acRawFile, GetLastError());
         break;
      }

      // EOF ?
      if (iRecLen != nBytesRead)
         break;

      NextRollRec:
#ifdef _DEBUG
      //if (!memcmp(acBuf, "0108301010000", 9))
      //   iTmp = 1;
#endif

      // Test bad input line
      if (acRollRec[10] != '~')
      {
         LogMsg("***** Invalid input line: %s", acRollRec);

         // Read next roll record
         pTmp = fgets(acRollRec, MAX_RECSIZE, fdRoll);
         if (!pTmp)
            break;
      }

      iTmp = memcmp(acBuf, &acRollRec[ROFF_PAR_BOOK], iApnLen);
      if (!iTmp)
      {
         // Merge roll data
         Sbd_MergeUpdRoll(acBuf, acRollRec, UPDATE_R01);
         iRollUpd++;

         if (lOptMisc & M_OPT_REMXFER)             // -Msx
         {
            memset(&acBuf[OFF_TRANSFER_DT], ' ', SIZ_SALE1_DT);
            memset(&acBuf[OFF_TRANSFER_DOC], ' ', SIZ_SALE1_DOC);
         }

         // Merge Transfer
         //if (fdOwner && acBuf[OFF_TRANSFER_DT] == ' ')
         if (fdOwner)
            iTmp = Sbd_MergeOwnerXfer(acBuf);

         // Merge CHAR
         if (fdChar)
            iTmp = Sbd_MergeStdChar(acBuf);

         // Write output
         bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);

         // Read next roll record
         pTmp = fgets(acRollRec, MAX_RECSIZE, fdRoll);

         if (!pTmp)
            bEof = true;         // Signal to stop
      } else if (iTmp > 0)       // Roll not match, new roll record?
      {
         if (bDebug)
            LogMsg0("*** New roll record : %.24s (%d) ***", acRollRec, lCnt+iRetiredRec);

         // Create new R01 record
         Sbd_MergeUpdRoll(acRec, acRollRec, CREATE_R01);
         iNewRec++;

         // Merge Transfer
         if (fdOwner && acRec[OFF_TRANSFER_DT] == ' ')
            iTmp = Sbd_MergeOwnerXfer(acRec);

         if (fdChar)
            iTmp = Sbd_MergeStdChar(acRec);

         bRet = WriteFile(fhOut, acRec, iRecLen, &nBytesWritten, NULL);
         lCnt++;

         // Get next roll record
         pTmp = fgets(acRollRec, MAX_RECSIZE, fdRoll);

         if (!pTmp)
            bEof = true;    // Signal to stop
         else
            goto NextRollRec;
      } else
      {
         // Record may be retired
         if (bDebug)
            LogMsg0("*** Roll not match (retired record?) : R01->%.*s < Roll->%.*s (%d) ***", iApnLen, acBuf, iApnLen, acRollRec, lCnt);
         iRetiredRec++;
         continue;
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);

      if (!bRet)
      {
         LogMsg("***** Error in Sbd_Load_Roll(): %d\n", GetLastError());
         lRet = WRITE_ERR;
         break;
      }
   }

   // Do the rest of the file
   while (!bEof)
   {
      if (bDebug)
         LogMsg0("*** New roll record : %.*s (%d) ***", iApnLen, acRollRec, lCnt);

      // Create new R01 record
      Sbd_MergeUpdRoll(acRec, acRollRec, CREATE_R01);
      iNewRec++;

      // Merge Transfer
      if (fdOwner && acRec[OFF_TRANSFER_DT] == ' ')
         iTmp = Sbd_MergeOwnerXfer(acRec);

      if (fdChar)
         iTmp = Sbd_MergeStdChar(acRec);

      bRet = WriteFile(fhOut, acRec, iRecLen, &nBytesWritten, NULL);
      lCnt++;

      // Get next roll record
      pTmp = fgets(acRollRec, MAX_RECSIZE, fdRoll);

      if (!pTmp)
         bEof = true;    // Signal to stop
   }

   // Close files
   if (fdRoll)
      fclose(fdRoll);
   if (fdOwner)
      fclose(fdOwner);
   if (fdChar)
      fclose(fdChar);

   if (fhOut)
      CloseHandle(fhOut);
   if (fhIn)
      CloseHandle(fhIn);

   LogMsg("Total output records:       %u", lCnt);
   LogMsg("Total new records:          %u", iNewRec);
   LogMsg("Total retired records:      %u", iRetiredRec);
   LogMsg("Total Full exempt records:  %u", lFullExe);
   LogMsg("Total chars update:         %u", lCharMatch);
   LogMsg("Total owner xfer update:    %u\n", lUpdateXfer);
   
   LogMsg("Total bad-city records:     %u", iBadCity);
   LogMsg("Total bad-suffix records:   %u\n", iBadSuffix);

   lRecCnt = lCnt;
   return 0;
}

/******************************* Sbd_LegalExtr ******************************
 *
 * Extract legal from roll file to save for later use
 *
 ****************************************************************************/

int Sbd_LegalExtr(void)
{
   char     *pTmp, acBuf[MAX_RECSIZE], acRec[MAX_RECSIZE];
   char     acApn[_MAX_PATH], acLegal[MAX_RECSIZE];
   long     iRet, lRet, lCnt=0, lLegalLen=0;

   FILE     *fdLegal;

   LogMsg0("Extract legal file");

   // Open roll file
   LogMsg("Open Roll file %s", acRollFile);
   fdRoll = fopen(acRollFile, "r");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", acRollFile);
      return -1;
   }

   // Open Output file
   iRet = GetIniString("SBD", "LegalExtr", "", acLegal, _MAX_PATH, acIniFile);
   if (iRet < 10)
      return -99;

   LogMsg("Open output file %s", acLegal);
   fdLegal = fopen(acLegal, "w");
   if (fdLegal == NULL)
   {
      LogMsg("***** Error creating legal file: %s\n", acLegal);
      return -2;
   }

   // Drop header record
   pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdRoll);
   // Get 1st rec
   pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdRoll);

   // Merge loop
   while (pTmp && !feof(fdRoll))
   {
      iTokens = ParseStringIQ(acRec, '~', MAX_FLD_TOKEN, apTokens);
      if (iTokens < SBD_ROLL_LGL_BLK)
      {
         LogMsg("***** Error: bad input record for APN=%s-%s-%s-%s%s.  Process stops", 
            apTokens[SBD_ROLL_PAR_BOOK], apTokens[SBD_ROLL_PAR_PAGE], 
            apTokens[SBD_ROLL_PAR_LINE], apTokens[SBD_ROLL_PAR_TYPE], apTokens[SBD_ROLL_PAR_SEQ]);
         break;
      }

      // Format APN
      sprintf(acApn, "%s-%s-%s-%s%s", apTokens[SBD_ROLL_PAR_BOOK], apTokens[SBD_ROLL_PAR_PAGE], 
         apTokens[SBD_ROLL_PAR_LINE], apTokens[SBD_ROLL_PAR_TYPE], apTokens[SBD_ROLL_PAR_SEQ]);
      iRet = SBD_ROLL_LGL_DESC1;
      acLegal[0] = 0;
      while (iRet < iTokens)
      {
         strcat(acLegal, apTokens[iRet]);
         strcat(acLegal, " ");
         iRet++;
      }

#ifdef _DEBUG
      //if (!memcmp(acApn, "0127-361-73", 11))
      //   iRet = 0;
#endif

      iRet = replUnPrtChar(acLegal, 32, strlen(acLegal));
      iRet = blankRem(acLegal);
      if (iRet > 2)
         sprintf(acBuf, "CA071|%s|%s\n", acApn, acLegal);
      else
         sprintf(acBuf, "CA071|%s|\n", acApn);

      // Output to file
      fputs(acBuf, fdLegal);
      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
      if (iRet > lLegalLen)
         lLegalLen = iRet;

      // Get next roll record
      pTmp = fgets(acRec, MAX_RECSIZE, fdRoll);
      if (pTmp)
      {
         iRet = strlen(pTmp);
         // Test for possible line break
         if (acRec[iRet-2] != '~')
         {
            // Read more
            lRet = ftell(fdRoll);
            pTmp = fgets(acBuf, MAX_RECSIZE, fdRoll);
            if (*(pTmp+4) != '~')
            {
               strncpy(&acRec[iRet-1], acBuf, MAX_RECSIZE-iRet);
               pTmp = acRec;
            } else
            {
               iRet = fseek(fdRoll, lRet, SEEK_SET);            
            }
         }
      }
   }

   // Close files
   if (fdRoll)
      fclose(fdRoll);
   if (fdLegal)
      fclose(fdLegal);

   LogMsg("Total output records:       %u", lCnt);
   LogMsg("Max legal length:           %d\n", lLegalLen);

   return 0;
}

/******************************** Sbd_ExtractOwners ***************************
 *
 * Extract current Owners and transfer info from OWNER.TRANSFER file and merge 
 * into one record per APN. If there are more than 2 names, set Etal flag to TRUE.
 * If input file is older than extracted file, do not extract.
 *
 * Return 0 if successful, 1 if no extract, <0 if error.
 *
 ******************************************************************************/

int Sbd_ExtractOwners(char *pOwnerFile, char *pOwnerEx)
{
   int   iIdx, iRet, lCnt=0, lOut=0;
   char  acInbuf[512], acOutbuf[512], acTmp[256], acDoc[16], *pTmp;
   FILE  *fdIn, *fdOut;
   SBD_OWNER   *pInrec = (SBD_OWNER *)acInbuf;
   OWNEREX     *pOutrec= (OWNEREX *)acOutbuf;

   LogMsg0("Extract Owners from %s to %s", pOwnerFile, pOwnerEx);

   // Open input file
   LogMsg("Open Current Owner file %s", pOwnerFile);
   fdIn = fopen(pOwnerFile, "r");
   if (fdIn == NULL)
   {
      LogMsg("***** Error opening Current Owner file: %s\n", pOwnerFile);
      return -2;
   }

   // Open output file
   LogMsg("Open output file %s", pOwnerEx);
   fdOut = fopen(pOwnerEx, "w");
   if (fdOut == NULL)
   {
      LogMsg("***** Error creating Owner file: %s\n", pOwnerEx);
      return -2;
   }

   // Get 1st rec
   pTmp = fgets((char *)&acInbuf[0], 512, fdIn);
   iIdx = 0;

   // Merge loop
   while (pTmp && !feof(fdIn))
   {
      if (memcmp(pInrec->Apn, pOutrec->Apn, SIZ_OWNER_PARCEL_NUMBER))
      {
         // Output current record
         if (iIdx > 0)
         {
            iRet = sprintf(acTmp, "%d", iIdx);
            memcpy(pOutrec->NameCnt, acTmp, iRet);
            if (iIdx > 2)
               pOutrec->Etal = 'Y';

            pOutrec->CrLf[0] = '\n';
            pOutrec->CrLf[1] = 0;
            iRet = fputs(acOutbuf, fdOut);
            lOut++;
         }

         // Prepare new record
         memset(acOutbuf, ' ', sizeof(OWNEREX));

         // Copy Name
         memcpy(pOutrec->Apn, pInrec->Apn, SIZ_OWNER_PARCEL_NUMBER);
         iRet = blankRem(pInrec->Owner);
         memcpy(pOutrec->Owners[0], pInrec->Owner, iRet);

#ifdef _DEBUG
         // 1988019/300 = 8819/300
         // 1973037/442 = 7337/442
         // 1988069/39  = 8869/39
         // 1948005/81  = 4805/81
         // 193000      ? 300
         // 195404      ? 544
         // 1954#166    ? 1954#166
         // 19#2006     ? #206
         // 19#3079     ? #379
         // 19#10043    ? #1043
         // 19(707)9226 ? (77)9226/1465
         // 19BK02225 P ? BK2225 PG37
         // 19960102..L ? 1996102..LC
         // 194/019/61  ? 4/19/61 233
         // 1981-008575 = 1981-008575
         // 1990083955  = 19900083955
         //if (!memcmp(pOutrec->Apn, "04972951700", 9))
         //   iRet = 0;
#endif
         // Copy Transfer
         if (pInrec->XferDate[0] >= '0' && pInrec->XferDate[0] <= '1')
         {
            if (pTmp = dateConversion(pInrec->XferDate, acTmp, MM_DD_YYYY_2))
            {
               memcpy(pOutrec->XferDate, acTmp, 8);
               if (isNumber(pInrec->XferDoc, SIZ_OWNER_DOC_NUMBER))
               {
                  memcpy(pOutrec->XferDoc, pInrec->XferDoc, SIZ_OWNER_DOC_NUMBER);
               } else if (isNumber(pInrec->XferDoc, SIZ_OWNER_DOC_NUMBER-1))
               {
                  iRet = sprintf(acTmp, "%.4s0%.6s", pInrec->XferDoc, &pInrec->XferDoc[4]);
                  memcpy(pOutrec->XferDoc, acTmp, iRet);
               } else if (pInrec->XferDoc[0] > ' ')
               {
                  memcpy(acDoc, pInrec->XferDoc, SIZ_OWNER_DOC_NUMBER);
                  iRet = iTrim(acDoc, SIZ_OWNER_DOC_NUMBER);
                  if (acDoc[4] == '-')
                     memcpy(pOutrec->XferDoc, acDoc, iRet);
                  else if (acDoc[7] == '/' && acDoc[3] != '/')
                  {
                     iRet = sprintf(acTmp, "%.2s%s", &acDoc[2], &acDoc[5]);
                     memcpy(pOutrec->XferDoc, acTmp, iRet);
                  } else
                     LogMsg("*** Bad DocNum: %.11s DocDate= %s APN=%.13s", pInrec->XferDoc, acTmp, pInrec->Apn);
               }
            } else
               LogMsg("*** Bad date: %.10s APN=%.13s", pInrec->XferDate, pInrec->Apn);
         } else if (pInrec->XferDate[0] != 'N')
            LogMsg("*** Invalid date: %.10s [%.13s]", pInrec->XferDate, pInrec->Apn);

         iIdx = 1;
      } else if (iIdx < 5)
      {
         int   iTmp;
         // Copy Name
         iRet = blankRem(pInrec->Owner);
         for (iTmp = 0; iTmp < iIdx; iTmp++)
         {
            if (!memcmp(pInrec->Owner, pOutrec->Owners[iTmp], iRet))
               break;
         }
         if (iTmp == iIdx)
            memcpy(pOutrec->Owners[iIdx++], pInrec->Owner, iRet);
         // Only add name if it is on the same xfer date
         //if (pTmp = dateConversion(pInrec->XferDate, acTmp, MM_DD_YYYY_2))
         //{
         //   if (!memcmp(acTmp, pOutrec->XferDate, 8))
         //   {
         //      // Copy Name
         //      iRet = blankRem(pInrec->Owner);
         //      memcpy(pOutrec->Owners[iIdx++], pInrec->Owner, iRet);
         //   } else
         //      LogMsg("--- Drop record not on the same transfer: %s [%.13s]", acTmp, pInrec->Apn);
         //}
      } else
         iIdx++;

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);

      // Get next roll record
      pTmp = fgets(acInbuf, 512, fdIn);
   }

   // Close files
   if (fdIn)
      fclose(fdIn);
   if (fdOut)
      fclose(fdOut);

   LogMsg("Total input records:       %u", lCnt);
   LogMsg("Total output records:      %u\n", lOut);

   return 0;
}

/********************************* Sbd_GetTaxStat ****************************
 *
 * Set TaxStatus using TaxStat file SSBD.TaxStat extracted from the county annual 
 * roll file NPP.PUBLICUC.SECWLEG.TXT
 *
 *****************************************************************************/

int Sbd_GetTaxStat(char *pOutbuf, char *pTaxStat)
{
   static   char acRec[64], *pRec=NULL;
   int      iLoop;
   SBD_OWNER *pOwner = (SBD_OWNER *)pOutbuf;

   // Get first Char rec for first call
   if (!pRec)
   {
      pRec = fgets(acRec, 64, fdTaxStat);
   }

   if (pTaxStat)
      *pTaxStat = ' ';

   do
   {
      if (!pRec)
      {
         fclose(fdTaxStat);
         fdTaxStat = NULL;
         return 1;      // EOF
      }

      // Compare Apn
      iLoop = memcmp(pOutbuf, acRec, iApnLen);
      if (iLoop > 0)
         pRec = fgets(acRec, 64, fdTaxStat);
   } while (iLoop > 0);

   // If not match, return
   if (iLoop)
      return 1;

   // Set Tax Status
   if (pTaxStat)
      *pTaxStat = acRec[14];
   else
      pOwner->TaxStat = acRec[14];

   lProp8Match++;

   return 0;
}

/****************************** Sbd_ExtractOwnerCsv ***************************
 *
 * Convert Owners file to CSV format 
 *
 * Return 0 if successful, 1 if no extract, <0 if error.
 *
 ******************************************************************************/

int Sbd_ExtractOwnerCsv(char *pOwnerFile, char *pOwnerEx)
{
   int   iRet, lCnt=0;
   char  acInbuf[512], acOutbuf[512], acTmp[_MAX_PATH], *pTmp;
   char  acApn[32], acDocDate[32], acDocNum[32];

   FILE      *fdOut;
   SBD_OWNER *pInrec = (SBD_OWNER *)acInbuf;

   LogMsg0("Extract Owners from %s to %s", pOwnerFile, pOwnerEx);

   // Open input file
   LogMsg("Open Current Owner file %s", pOwnerFile);
   fdOwner = fopen(pOwnerFile, "r");
   if (fdOwner == NULL)
   {
      LogMsg("***** Error opening Current Owner file: %s\n", pOwnerFile);
      return -2;
   }

   // Open output file
   LogMsg("Open output file %s", pOwnerEx);
   fdOut = fopen(pOwnerEx, "w");
   if (fdOut == NULL)
   {
      LogMsg("***** Error creating Owner file: %s\n", pOwnerEx);
      return -2;
   }
   fputs("FIPS|APN_D|TRANSDATE|TRANSDOC|PCT_INT|OWNER\n", fdOut);

   // Loop thru all records
   lProp8Match = 0;
   while (!feof(fdOwner))
   {
      pTmp = fgets((char *)&acInbuf[0], 512, fdOwner);
      if (!pTmp)
         break;

      acDocDate[0] = 0;
      if (pInrec->XferDate[0] >= '0' && pInrec->XferDate[0] <= '1')
         dateConversion(pInrec->XferDate, acDocDate, MM_DD_YYYY_2);

      acDocNum[0] = 0;
      if (pInrec->XferDoc[0] > '0')
      {
         if (isNumber(pInrec->XferDoc, SIZ_OWNER_DOC_NUMBER))
         {
            memcpy(acDocNum, pInrec->XferDoc, SIZ_OWNER_DOC_NUMBER);
            acDocNum[SIZ_OWNER_DOC_NUMBER] = 0;
         } else if (isNumber(pInrec->XferDoc, SIZ_OWNER_DOC_NUMBER-1))
         {
            sprintf(acDocNum, "%.4s0%.6s", pInrec->XferDoc, &pInrec->XferDoc[4]);
         } else if (pInrec->XferDoc[0] > ' ')
         {
            memcpy(acTmp, pInrec->XferDoc, SIZ_OWNER_DOC_NUMBER);
            iRet = iTrim(acTmp, SIZ_OWNER_DOC_NUMBER);
            if (acTmp[4] == '-')
               strcpy(acDocNum, acTmp);
            else if (acTmp[7] == '/' && acTmp[3] != '/')
               sprintf(acDocNum, "%.2s%s", &acTmp[2], &acTmp[5]);
            else
               LogMsg("*** Bad DocNum: %.11s DocDate= %.10s APN=%.13s", pInrec->XferDoc, pInrec->XferDate, pInrec->Apn);
         }
      } 

      if (acDocDate[0] > ' ')
      {
         iRet = formatApn(pInrec->Apn, acApn, &myCounty);
         sprintf(acOutbuf, "06071|%s|%s|%s|%.2f|%s\n", acApn, acDocDate, acDocNum, atoin(pInrec->XferPct, 5)/100.0, myTrim(pInrec->Owner));
         iRet = fputs(acOutbuf, fdOut);
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdOwner)
      fclose(fdOwner);
   if (fdOut)
      fclose(fdOut);

   LogMsg("Total output records:  %u\n", lCnt);

   return 0;
}

/********************************* MergeDocRec *******************************
 *
 * Return 0 if nothing get updated
 *        1 if only transfer updated
 *        2 if sale get updated
 *
 *****************************************************************************/

int Sbd_MergeDocRec(char *pGrGrRec, char *pOutbuf)
{
   long  lCurSaleDt, lLstSaleDt, lLstXferDt;
   int   iDocIdx, iRet = 0;
   char  *pTmp;

   GRGR_DOC *pSaleRec = (GRGR_DOC *)pGrGrRec;

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "030335116", 9))
   //   iRet = 0;
#endif

   lCurSaleDt = atoin(pSaleRec->DocDate, SIZ_SALE1_DT);

   // Only take documents with predefined DocType
   pSaleRec->DocTitle[SIZ_GD_TITLE-1] = 0;
   iDocIdx = findDocType(pSaleRec->DocTitle, (IDX_TBL5 *)&SBD_DocCode1[0]);
   if (iDocIdx < 0)
      return iRet;
 
   if (SBD_DocCode1[iDocIdx].isTransfer == 'N')
      return iRet;

   // Update transfers
   lLstXferDt = atoin(pOutbuf+OFF_TRANSFER_DT, SIZ_TRANSFER_DT);
   if (lCurSaleDt > lLstXferDt)
   {
      memcpy(pOutbuf+OFF_TRANSFER_DOC, pSaleRec->DocNum, SIZ_TRANSFER_DOC);
      memcpy(pOutbuf+OFF_TRANSFER_DT, pSaleRec->DocDate, SIZ_TRANSFER_DT);
      iRet = 1;
   }

   // If none sale, ignore it
   if (SBD_DocCode1[iDocIdx].isSale == 'N')
      return iRet;

   // Skip old data
   lLstSaleDt = atoin(pOutbuf+OFF_SALE1_DT, SIZ_SALE1_DT);
   if (lCurSaleDt < lLstSaleDt)
      return iRet;

   if (lCurSaleDt == lLstSaleDt)
   {
      if (*(pOutbuf+OFF_SALE1_DOC) == ' ' && pSaleRec->DocNum[0] > ' ')
         memcpy(pOutbuf+OFF_SALE1_DOC, pSaleRec->DocNum, SIZ_SALE1_DOC);
      return 1;
   } else if (*(pOutbuf+OFF_SALE1_DOC) > ' ')
   {
      // Move sale2 to sale3
      memcpy(pOutbuf+OFF_SALE3_DOC, pOutbuf+OFF_SALE2_DOC, SIZ_SALE3_DOC);
      memcpy(pOutbuf+OFF_SALE3_DT, pOutbuf+OFF_SALE2_DT, SIZ_SALE2_DT);
      memcpy(pOutbuf+OFF_SALE3_DOCTYPE, pOutbuf+OFF_SALE2_DOCTYPE, SIZ_SALE2_DOCTYPE);
      memcpy(pOutbuf+OFF_SALE3_AMT, pOutbuf+OFF_SALE2_AMT, SIZ_SALE2_AMT);
      memcpy(pOutbuf+OFF_SALE3_CODE, pOutbuf+OFF_SALE2_CODE, SIZ_SALE2_CODE);

      // Move sale1 to sale2
      memcpy(pOutbuf+OFF_SALE2_DOC, pOutbuf+OFF_SALE1_DOC, SIZ_SALE1_DOC);
      memcpy(pOutbuf+OFF_SALE2_DT, pOutbuf+OFF_SALE1_DT, SIZ_SALE1_DT);
      memcpy(pOutbuf+OFF_SALE2_DOCTYPE, pOutbuf+OFF_SALE1_DOCTYPE, SIZ_SALE1_DOCTYPE);
      memcpy(pOutbuf+OFF_SALE2_AMT, pOutbuf+OFF_SALE1_AMT, SIZ_SALE1_AMT);
      memcpy(pOutbuf+OFF_SALE2_CODE, pOutbuf+OFF_SALE1_CODE, SIZ_SALE1_CODE);
   }

   // Update current sale
   memcpy(pOutbuf+OFF_SALE1_DOC, pSaleRec->DocNum, SIZ_SALE3_DOC);
   memcpy(pOutbuf+OFF_SALE1_DT, pSaleRec->DocDate, SIZ_SALE3_DT);
   memcpy(pOutbuf+OFF_SALE1_DOCTYPE, SBD_DocCode1[iDocIdx].DocType, SBD_DocCode1[iDocIdx].DocTypeLen);
   // Reset SalePrice
   memset(pOutbuf+OFF_SALE1_AMT, ' ', SIZ_SALE1_AMT);
   memset(pOutbuf+OFF_SALE1_CODE, ' ', SIZ_SALE1_CODE);

   *pSaleRec->Grantor[1] = 0;
   if (pTmp = strchr(pSaleRec->Grantor[0], '('))
      *pTmp = '\0';
   memset(pOutbuf+OFF_SELLER, ' ', SIZ_SELLER);
   vmemcpy(pOutbuf+OFF_SELLER, pSaleRec->Grantor[0], SIZ_SELLER);

   *(pOutbuf+OFF_AR_CODE1) = 'W';

   return 2;
}

/******************************** MergeDocExt ********************************
 *
 * Merge data grab from website to PQ4 product w/o sale price.
 *
 *****************************************************************************/

int Sbd_MergeDocExt(char *pCnty, char *pDocFile)
{
   char     *pTmp, acBuf[MAX_RECSIZE], acSaleRec[1024], acTmp[64];
   char     cFileCnt=1;
   char     acRawFile[_MAX_PATH], acOutFile[_MAX_PATH];

   HANDLE   fhIn, fhOut;
   GRGR_DOC *pSaleRec = (GRGR_DOC *)&acSaleRec;
   FILE     *fdDoc;

   int      iTmp, iSaleUpd=0;
   DWORD    nBytesRead;
   DWORD    nBytesWritten;
   BOOL     bRet, bEof, bRename=true;
   long     lRet=0, lCnt=0;

   memset(acSaleRec, ' ', sizeof(GRGR_DOC));

   sprintf(acRawFile, acRawTmpl, pCnty, pCnty, "R01");
   if (_access(acRawFile, 0))
   {
      sprintf(acRawFile, acRawTmpl, pCnty, pCnty, "S01");
      bRename = false;
   }

   sprintf(acOutFile, acRawTmpl, pCnty, pCnty, "N01");
   if (!iGrGrApnLen)
      iGrGrApnLen = myCounty.iApnLen;

   // Open GrGr file
   LogMsg0("Merge Recorder data %s to R01 file", pDocFile);
   fdDoc = fopen(pDocFile, "r");
   if (fdDoc == NULL)
   {
      LogMsg("***** Error opening recorder extract file: %s\n", pDocFile);
      return -2;
   }

   // Get first record
   pTmp = fgets(acSaleRec, 1024, fdDoc);
   if (!pTmp || feof(fdDoc))
   {
      LogMsg("*** No recorder data available");
      return 0;
   }

   // Open Input file
   LogMsg("Open input file %s", acRawFile);
   fhIn = CreateFile(acRawFile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhIn == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening input file to merge GrGr: %s\n", acRawFile);
      return -3;
   }

   // Open Output file
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening output file to merge GrGr: %s\n", acOutFile);
      return -4;
   }

   // Copy skip record
   ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);
   bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);

   iNoMatch=0;
   bEof = false;

   // Merge loop
   while (!bEof)
   {
      bRet = ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);
#ifdef _DEBUG
      //iTmp = replUnPrtChar(acBuf, ' ', 1900);
      //if (iTmp > 0)
      //   LogMsg("1. Bad char found at parcel %.13s (pos %d)", acBuf, iTmp);
#endif

      // Check for EOF
      if (!bRet)
      {
         LogMsg("***** Error reading input file %s (%f)", acRawFile, GetLastError());
         break;
      }

      if (!nBytesRead)
      {
         // EOF
         CloseHandle(fhIn);
         CloseHandle(fhOut);
         fhIn = 0;
         fhOut = 0;

         LogMsg("Rename output file %s to %s", acOutFile, acRawFile);
         if (bRename)
         {
            sprintf(acTmp, "M0%c", cFileCnt|0x30);
            sprintf(acBuf, acRawTmpl, pCnty, pCnty, acTmp);
            if (!_access(acBuf, 0))
               remove(acBuf);
            iTmp = rename(acRawFile, acBuf);
            iTmp = rename(acOutFile, acRawFile);
         } else
         {
            sprintf(acTmp, "R0%c", cFileCnt|0x30);
            sprintf(acBuf, acRawTmpl, pCnty, pCnty, acTmp);
            iTmp = rename(acOutFile, acBuf);
         }


         // Check for next input
         cFileCnt++;
         acRawFile[strlen(acRawFile)-1] = cFileCnt | 0x30;
         if (!_access(acRawFile, 0))
         {
            // Open next Input file
            LogMsg("Open input file %s", acRawFile);
            fhIn = CreateFile(acRawFile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
                  FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

            if (fhIn == INVALID_HANDLE_VALUE)
            {
               LogMsg("***** Error opening input file to merge GrGr: %s\n", acRawFile);
               break;
            }
            bRet = ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);

            // Open Output file
            acOutFile[strlen(acRawFile)-1] = cFileCnt | 0x30;
            LogMsg("Open output file %s", acOutFile);
            fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
                  FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);
            if (fhOut == INVALID_HANDLE_VALUE)
            {
               LogMsg("***** Error opening output file to merge GrGr: %s\n", acOutFile);
               break;
            }
         } else
            break;
      }

#ifdef _DEBUG
      //if (!memcmp(acBuf, "030335116", 9))
      //   iTmp = 0;
#endif
      // Update sale
GrGr_ReLoad:
      iTmp = memcmp(acBuf, pSaleRec->APN, iGrGrApnLen);
      if (!iTmp)
      {
#ifdef _DEBUG
         //if (!memcmp(acBuf, "0335221220000", 9))
         //   iTmp = 0;
         if (strlen(acSaleRec) != 511)
         {
            iTmp = strlen(acSaleRec);
            LogMsg0("***** Bad GrGr record length: %d, APN=%.13s", iTmp, pSaleRec->APN);
         }
#endif

         // Merge sale data
         iTmp = Sbd_MergeDocRec(acSaleRec, acBuf);
         if (iTmp > 0)
         {
            iSaleUpd++;
          
            // Get last recording date
            iTmp = atoin((char *)&acBuf[OFF_TRANSFER_DT], SIZ_TRANSFER_DT);
            if (iTmp >= lToday)
               LogMsg0("*** WARNING: Bad last recording date %d at record# %d -->%.10s", iTmp, lCnt, acBuf);
            else if (lLastRecDate < iTmp)
               lLastRecDate = iTmp;
         }
#ifdef _DEBUG
         //iTmp = replUnPrtChar(acBuf, ' ', 1900);
         //if (iTmp > 0)
         //   LogMsg("2. Bad char found at parcel %.13s (pos %d)", acBuf, iTmp);
#endif
         // Read next sale record
         pTmp = fgets(acSaleRec, 1024, fdDoc);
         if (!pTmp)
            bEof = false;    // Signal to stop sale update
         else
            goto GrGr_ReLoad;
      } else
      {
         if (iTmp > 0)        // Sale not match, advance to next sale record
         {
            if (!memcmp(acBuf, "99999999", 8))
            {
               nBytesRead = 0;
               continue;
            } else
            {
               iNoMatch++;
               if (bDebug)
                  LogMsg0("*** Sale not match : %.12s > %.12s (%d) ", acBuf, pSaleRec->APN, lCnt);
               pTmp = fgets(acSaleRec, 1024, fdDoc);
               if (!pTmp)
                  bEof = true;    // Signal to stop sale update
               else
                  goto GrGr_ReLoad;
            }
         }
      }

      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);

      if (!bRet)
      {
         LogMsg("***** Error occurs: %d\n", GetLastError());
         lRet = -99;
         break;
      }
   }

   // Do the rest of the file
   while (nBytesRead>0 && ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL))
   {
      if (!nBytesRead)
         break;

      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
      lCnt++;
   }


   // Close files
   if (fdDoc)
      fclose(fdDoc);
   if (fhOut)
      CloseHandle(fhOut);
   if (fhIn)
   {
      CloseHandle(fhIn);

      // Rename outfile
      if (bRename)
      {
         sprintf(acTmp, "M0%c", cFileCnt|0x30);
         sprintf(acBuf, acRawTmpl, pCnty, pCnty, acTmp);
         if (!_access(acBuf, 0))
            remove(acBuf);
         iTmp = rename(acRawFile, acBuf);
      } else
      {
         sprintf(acTmp, "R0%c", cFileCnt|0x30);
         sprintf(acRawFile, acRawTmpl, pCnty, pCnty, acTmp);
      }
      LogMsg("Rename output file %s to %s", acOutFile, acRawFile);
      iTmp = rename(acOutFile, acRawFile);
   }

   // Update output record count
   if (lCnt > 1000)
      lRecCnt = lCnt;

   LogMsg("Total output records:       %u", lCnt);
   LogMsg("Total GrGr records updated: %u", iSaleUpd);
   LogMsg("Total GrGr records skipped: %u", iNoMatch);
   return 0;
}

/****************************** Sbd_ExtrChar ******************************
 *
 * Extract the whole char file to CSV format.  Cleaing up data where possible
 * This function is used to create "San Bernadino, CA XC.txt" for customer.
 *
 * Return number of records output.
 *
 *****************************************************************************/

int Sbd_ExtrChar(char *pInfile)
{
   FILE     *fdIn, *fdOut;
   char     acInbuf[MAX_RECSIZE], acOutbuf[MAX_RECSIZE], acTmpFile[_MAX_PATH], acTmp[256],
				*pRec, *pDelim="|";
   int      iRet, iTmp, iCnt=0;

   LogMsg0("Extracting char file for bulk client");

   LogMsg("Open input file %s", pInfile);
   if (!(fdIn = fopen(pInfile, "r")))
      return -1;

	iRet = GetIniString(myCounty.acCntyCode, "CharExtr", "", acTmpFile, _MAX_PATH, acIniFile);
	if (!iRet)
	{
		LogMsg("*** Output file not defined.  Ignore extracting job.");
		return 0;
	}

   LogMsg("Open output file %s", acTmpFile);
   if (!(fdOut = fopen(acTmpFile, "w")))
   {
      fclose(fdIn);
      LogMsg("***** Error creating output file %s", acTmpFile);
      return -2;
   }

   // Skip first record - header
   for (iTmp = 0; iTmp < iHdrRows; iTmp++)
      pRec = fgets(acInbuf, MAX_RECSIZE, fdIn);

   // Print header
   pRec = "FIPS|APN_D|FILE_CREATE_DATE|DISTRICT|RESP_GRP|RESP_UNIT|LAND_TYPE|SIZE_RANGE|USE_CODE|TRA|PROL_IMPR_VAL|PROL_LAND_VAL|EXEM1_TYPE|EXEM2_TYPE|EXEMHOX_TOT_AMT|SITUS_STREET_NBR|SITUS_STREET_FRA|SITUS_STREET_DIR|SITUS_STREET_NAME|SITUS_STREET_SUFF|SITUS_STREET_QUAL|SITUS_COMM|SITUS_CITY|SITUS_ZIP1|SITUS_ZIP2|OWNR_RELATION|OWNR_DOC_DATE|OWNR_DOC_NBR|OWNR_ACQ_DATE|OWNR_VAL_YR|OWNR_CHG_YR|MAIL_1STOWNR|MAIL_2NDOWNR|MAIL_ADDR_LN1|MAIL_ADDR_LN2|LAND_LOT_WIDTH|LAND_LOT_DEPTH|LAND_LOT_FOOTAGE|GROSS_ACRE|LAND_NET_ACRE|LAND_ACCESS|LAND_SLOPE_DIR|LAND_SLOPE_DEGRE|LAND_VIEW_QUALTY|LAND_VIEW_TYPE|LAND_SEWER|LAND_WATER|LAND_ELECTRICITY|LAND_GAS|LAND_ENCROACHMNT|LAND_NUISANCE1|LAND_NUISANCE2|LAND_INFLUENCE1|LAND_INFLUENCE2|SFRS_SEQ|SFRS_CONST_TYPE|SFRS_QUALITY|SFRS_SHAPE|SFRS_COST_TABLE|SFRS_MH_MAKE|SFRS_SIZE|SFRS_CONST_YEAR|SFRS_EFF_YEAR|SFRS_NBR_BATH|SFRS_NBR_BDRM|SFRS_FAMILY_DEN|SFRS_TOTAL_ROOM|SFRS_ROOF_TYPE|SFRS_LANDSC_QUAL|SFRS_FUNC_OBS|SFRS_TMS_SEASON|SFRS_COST_MOD|SFRS_FUN_OBS_PCT|SFRS_ECO_OBS_PCT|SFRS_DEFR_MAINT|SFRS_FLR1_SIZE|SFRS_FLR2_SIZE|SFRS_FLR2_FACTOR|SFRS_FLR3_SIZE|SFRS_FLR3_FACTOR|SFRS_BAS1_SIZE|SFRS_BAS1_FACTOR|SFRS_BAS2_SIZE|SFRS_BAS2_FACTOR|SFRS_ADD1_SIZE|SFRS_ADD1_FACTOR|SFRS_ADD2_SIZE|SFRS_ADD2_FACTOR|SFRS_ADD3_SIZE|SFRS_ADD3_FACTOR|SFRS_PCH1_SIZE|SFRS_PCH1_FACTOR|SFRS_PCH2_SIZE|SFRS_PCH2_FACTOR|SFRS_PCH3_SIZE|SFRS_PCH3_FACTOR|SFRS_DECK_SQF|SFRS_DECK_FACTOR|SFRS_DECK_STALL|SFRS_PORT_SQF|SFRS_PORT_FACTOR|SFRS_PORT_STALL|SFRS_GARG_SQF|SFRS_GARG_CLASS|SFRS_GARG_STALL|SFRS_AC_RCN|SFRS_HEAT|SFRS_COOL|SFRS_FIRPLC_RCN|SFRS_NBR_FIRPLC|SFRS_YRD_IMP_RCN|SFRS_MISC_RCN|SFRS_POOL_RCN|SFRS_POOL_TYPE|SFRS_SPC_IMP_RCN|SFRS_SPC_IMP1|SFRS_SPC_IMP2|SFRS_SPC_IMP3|SFRS_MH_SIDING|SFRS_MH_FND_LF|SFRS_MH_FND_TYPE|SFRS_MH_SKT_LF|SFRS_MH_SKT_TYPE|SFRS_STREET_NBR|SFRS_STREET_FRA|SFRS_STREET_DIR|SFRS_STREET_NAME|SFRS_STREET_SUFF|SFRS_STREET_QUAL|SFRS_COMM|SFRS_CITY|SFRS_ZIP1|SFRS_ZIP2|OTHR_SEQ|OTHR_USE|OTHR_STRUC_QUAL|OTHR_STRUC_TYPE|OTHR_GRO_LEASE|OTHR_NET_LEASE|OTHR_CONST_YEAR|OTHR_EFF_YEAR|OTHR_NBR_UNITS|OTHR_AVG_SF_UNITS|OTHR_ADJ_AVG_SQF|OTHR_NBR_STORIES|OTHR_STORY_HIGHT|OTHR_ELEVATOR|OTHR_LANDSC_QUAL|OTHR_FUNC_OBSOL|OTHR_LB_RATIO|OTHR_ADJ_LB_RATIO|OTHR_APT1_UNITS|OTHR_APT1_BDRM|OTHR_APT1_BATH|OTHR_APT2_UNITS|OTHR_APT2_BDRM|OTHR_APT2_BATH|OTHR_APT3_UNITS|OTHR_APT3_BDRM|OTHR_APT3_BATH|OTHR_APT4_UNITS|OTHR_APT4_BDRM|OTHR_APT4_BATH|OTHR_APT5_UNITS|OTHR_APT5_BDRM|OTHR_APT5_BATH|OTHR_GARAGE|OTHR_CARPORT|OTHR_OPEN_SPACES|OTHR_RV_SPACES|OTHR_LAUNDRY|OTHR_REC_BLDG|OTHR_APT_POOL|OTHR_APT_SPA|OTHR_TENNIS|OTHR_SEC_GATE|OTHR_OFFICE_PCNT|OTHR_MEZZ_SQF|OTHR_RESTAURANT|OTHR_HOTEL_POOL|OTHR_HOTEL_SPA|OTHR_CONF_ROOM|OTHR_STREET_NBR|OTHR_STREET_FRA|OTHR_STREET_DIR|OTHR_STREET_NAME|OTHR_STREET_SUFF|OTHR_STREET_QUAL|OTHR_COMM|OTHR_CITY|OTHR_ZIP1|OTHR_ZIP2|NEIGHBORHOOD\n";
   fputs(pRec, fdOut);

   while (!feof(fdIn))
   {
      pRec = fgets(acInbuf, MAX_RECSIZE, fdIn);

      if (!pRec)
         break;

      if (cDelim == ',')
         iRet = ParseStringNQ(pRec, cDelim, SBD_CHR_NEIGHBORHOOD+1, apTokens);
      else
         iRet = ParseStringIQ(pRec, cDelim, SBD_CHR_NEIGHBORHOOD+1, apTokens);
      if (iRet < SBD_CHR_NEIGHBORHOOD)
      {
         LogMsg("*** Bad CHAR record (%d): %.80s", iRet, acInbuf);
         continue;
      }

		// Fips
		strcpy(acOutbuf, "06071");

      // Apn
      iTmp = formatApn(apTokens[SBD_CHR_PARCEL_NBR], acTmp, &myCounty);
      AppendString(acOutbuf, acTmp, pDelim, false);

		// File creation date
      AppendString(acOutbuf, apTokens[SBD_CHR_CREATE_DATE], pDelim, false);

		// Fix TRA
		iTmp = atol(apTokens[SBD_CHR_TRA]);
		sprintf(acTmp, "%.6d", iTmp);
		apTokens[SBD_CHR_TRA] = acTmp;

		for (iTmp = SBD_CHR_DISTRICT; iTmp < SBD_CHR_NEIGHBORHOOD; iTmp++)
		{
			strcat(acOutbuf, pDelim);
			strcat(acOutbuf, myTrim(apTokens[iTmp]));
		}
		AppendString(acOutbuf, apTokens[iTmp], pDelim, false);

      fputs(acOutbuf, fdOut);

      if (!(++iCnt % 1000))
         printf("\r%u", iCnt);
   }

   fclose(fdIn);
   fclose(fdOut);

	LogMsg("Total CHAR records output: %d\n", iCnt);

   return iCnt;
}

/****************************** Sbd_ConvStdChar ******************************
 *
 * Convert NPP.PROPERTYCHARACTERISTICS.TXT to STDCHAR format.
 * Return number of records output.
 *
 *****************************************************************************/

int Sbd_ConvStdChar(char *pInfile)
{
   FILE     *fdIn, *fdOut;
   char     acInbuf[MAX_RECSIZE], acOutbuf[2048], acTmpFile[256], acTmp[256], *pTmp, *pRec;
   int      iRet, iTmp, iCnt=0;
   double   dAcres;

   STDCHAR  *pCharRec=(STDCHAR *)&acOutbuf[0];

   LogMsg0("Convert char file to standard format");

   LogMsg("Open input file %s", pInfile);
   if (!(fdIn = fopen(pInfile, "r")))
      return -1;

   sprintf(acTmpFile, "%s\\%s\\%s_char.tmp", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   LogMsg("Open output file %s", acTmpFile);
   if (!(fdOut = fopen(acTmpFile, "w")))
   {
      fclose(fdIn);
      LogMsg("***** Error creating output file %s", acTmpFile);
      return -2;
   }

   // Skip first record - header
   iTmp = 0;
   for (iTmp = 0; iTmp < iHdrRows; iTmp++)
      pRec = fgets(acInbuf, MAX_RECSIZE, fdIn);

   while (!feof(fdIn))
   {
      pRec = fgets(acInbuf, MAX_RECSIZE, fdIn);

      if (!pRec)
         break;

      if (cDelim == ',')
         iRet = ParseStringNQ(pRec, cDelim, SBD_CHR_NEIGHBORHOOD+1, apTokens);
      else
         iRet = ParseStringIQ(pRec, cDelim, SBD_CHR_NEIGHBORHOOD+1, apTokens);
      if (iRet < SBD_CHR_NEIGHBORHOOD)
      {
         LogMsg("*** Bad CHAR record (%d): %.80s", iRet, acInbuf);
         continue;
      }

      memset(acOutbuf, ' ', sizeof(STDCHAR));

      // Apn
      vmemcpy(pCharRec->Apn, apTokens[SBD_CHR_PARCEL_NBR], SIZ_CHAR_APN);

      // Seq
      iTmp = atoi(apTokens[SBD_CHR_SFRS_SEQ]);
      if (iTmp > 100)
      {
         sprintf(acTmp, "%d", iTmp);
         vmemcpy(pCharRec->UnitSeqNo, acTmp, SIZ_CHAR_SIZE2);
      }

      // Usecode
      iTmp = atol(apTokens[SBD_CHR_USE_CODE]);
      if (iTmp > 0)
         vmemcpy(pCharRec->LandUse, apTokens[SBD_CHR_USE_CODE], SIZ_CHAR_SIZE8);
      else if (*apTokens[SBD_CHR_OTHR_USE] > ' ')
         vmemcpy(pCharRec->LandUse, apTokens[SBD_CHR_OTHR_USE], SIZ_CHAR_SIZE8);

      // Lot acre
      dAcres = atof(apTokens[SBD_CHR_GROSS_ACRE]);
      if (dAcres > 0.001)
      {
         dAcres *= 1000.0;
         sprintf(acTmp, "%d", (long)dAcres);
         vmemcpy(pCharRec->LotAcre, acTmp, SIZ_CHAR_SQFT);
      }

      // Lot Sqft
      long lLotSqft = atoi(apTokens[SBD_CHR_LAND_LOT_FOOTAGE]);
      if (lLotSqft > 100)
      {
         sprintf(acTmp, "%d", lLotSqft);
         vmemcpy(pCharRec->LotSqft, acTmp, SIZ_CHAR_SQFT);

         if (dAcres == 0.0)
         {
            dAcres = (double)lLotSqft / SQFT_FACTOR_1000;
            sprintf(acTmp, "%d", (long)dAcres);
            vmemcpy(pCharRec->LotAcre, acTmp, SIZ_CHAR_SQFT);
         }
      }

      // View
      if (*apTokens[SBD_CHR_LAND_VIEW_TYPE] > ' ')
      {
         if (pTmp = findXlatCode(*apTokens[SBD_CHR_LAND_VIEW_TYPE], &asView[0]))
            pCharRec->View[0] = *pTmp;
         else
            LogMsg("*** Unknown View Type: %s", apTokens[SBD_CHR_LAND_VIEW_TYPE]);
      }

      // Water
      if (*apTokens[SBD_CHR_LAND_WATER] > ' ')
      {
         if (pTmp = findXlatCode(*apTokens[SBD_CHR_LAND_WATER], &asWater[0]))
            pCharRec->Water = *pTmp;
         else
            LogMsg("*** Unknown Water code: %s", apTokens[SBD_CHR_LAND_WATER]);
      }

      // Sewer
      if (*apTokens[SBD_CHR_LAND_SEWER] > ' ')
      {
         if (pTmp = findXlatCode(*apTokens[SBD_CHR_LAND_SEWER], &asSewer[0]))
            pCharRec->Sewer = *pTmp;
         else
            LogMsg("*** Unknown Sewer code: %s", apTokens[SBD_CHR_LAND_SEWER]);
      }

      // BldgSqft
      iTmp = atoi(apTokens[SBD_CHR_SFRS_SIZE]);
      if (iTmp > 100)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(pCharRec->BldgSqft, acTmp, iRet);
      }

      // YrBlt
      iTmp = atoi(apTokens[SBD_CHR_SFRS_CONST_YEAR]);
      if (iTmp > 1600 && iTmp != 1900)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(pCharRec->YrBlt, acTmp, iRet);
      } else if (iTmp = atoi(apTokens[SBD_CHR_OTHR_CONST_YEAR]))
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(pCharRec->YrBlt, acTmp, iRet);
      }

      // YrEff
      iTmp = atoi(apTokens[SBD_CHR_SFRS_EFF_YEAR]);
      if (iTmp > 1600 && iTmp != 1900)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(pCharRec->YrEff, acTmp, iRet);
      } else if (iTmp = atoi(apTokens[SBD_CHR_OTHR_EFF_YEAR]))
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(pCharRec->YrEff, acTmp, iRet);
      }

      // Beds
      iTmp = atoi(apTokens[SBD_CHR_SFRS_NBR_BDRM]);
      if (iTmp > 0)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(pCharRec->Beds, acTmp, iRet);
      }

      // Baths
      iTmp = atoi(apTokens[SBD_CHR_SFRS_NBR_BATH]);
      if (iTmp > 0)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(pCharRec->FBaths, acTmp, iRet);
         memcpy(pCharRec->Bath_4Q, acTmp, iRet);
         if (pTmp = strchr(apTokens[SBD_CHR_SFRS_NBR_BATH], '.'))
         {
            // Half baths
            iTmp = atoi(pTmp+1);
            if (iTmp > 0)
            {
               pCharRec->HBaths[0] = '1';
               if (iTmp <= 3)
                  pCharRec->Bath_1Q[0] = '1';
               else if (iTmp <= 6)
                  pCharRec->Bath_2Q[0] = '1';
               else 
                  pCharRec->Bath_3Q[0] = '1';
            }
         }
      }

      // Total rooms
      iTmp = atoi(apTokens[SBD_CHR_SFRS_TOTAL_ROOM]);
      if (iTmp > 0)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(pCharRec->Rooms, acTmp, iRet);
      }

      // Floors
      int iStories = 0;
      iTmp = atoi(apTokens[SBD_CHR_SFRS_FLR3_SIZE]);
      if (iTmp > 0)
      {
         iStories = 3;
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(pCharRec->Sqft_Above2nd, acTmp, iRet);
      }
      iTmp = atoi(apTokens[SBD_CHR_SFRS_FLR2_SIZE]);
      if (iTmp > 0)
      {
         if (!iStories)
            iStories = 2;
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(pCharRec->Sqft_2ndFl, acTmp, iRet);
      }
      iTmp = atoi(apTokens[SBD_CHR_SFRS_FLR1_SIZE]);
      if (iTmp > 0)
      {
         if (!iStories)
            iStories = 1;
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(pCharRec->Sqft_1stFl, acTmp, iRet);
      }
        
      if (*apTokens[SBD_CHR_OTHR_NBR_STORIES] > '0' && iStories == 0)
         iStories = atoi(apTokens[SBD_CHR_OTHR_NBR_STORIES]);
      if (iStories > 0)
      {
         iRet = sprintf(acTmp, "%d.0", iStories);
         memcpy(pCharRec->Stories, acTmp, iRet);
      }

      // Elevator
      if (*apTokens[SBD_CHR_OTHR_ELEVATOR] > '0')
         pCharRec->HasElevator = 'Y';

      // Basement
      iTmp = atoi(apTokens[SBD_CHR_SFRS_BAS1_SIZE]);
      iTmp += atoi(apTokens[SBD_CHR_SFRS_BAS2_SIZE]);
      if (iTmp > 0)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(pCharRec->BsmtSqft, acTmp, iRet);
      }

      // Addition
      iTmp = atoi(apTokens[SBD_CHR_SFRS_ADD1_SIZE]);
      iTmp += atoi(apTokens[SBD_CHR_SFRS_ADD2_SIZE]);
      iTmp += atoi(apTokens[SBD_CHR_SFRS_ADD3_SIZE]);
      if (iTmp > 0)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(pCharRec->MiscSqft, acTmp, iRet);
      }

      // Porch
      iTmp = atoi(apTokens[SBD_CHR_SFRS_PCH1_SIZE]);
      iTmp += atoi(apTokens[SBD_CHR_SFRS_PCH2_SIZE]);
      iTmp += atoi(apTokens[SBD_CHR_SFRS_PCH3_SIZE]);
      if (iTmp > 0)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(pCharRec->PorchSqft, acTmp, iRet);
      }

      // Decks
      iTmp = atoi(apTokens[SBD_CHR_SFRS_DECK_SQF]);
      if (iTmp > 0)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(pCharRec->DeckSqft, acTmp, iRet);
      }

      // Garage Sqft
      iTmp = atoi(apTokens[SBD_CHR_SFRS_GARG_SQF]);
      if (iTmp > 100)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(pCharRec->GarSqft, acTmp, iRet);
      }

      // Number of parking spaces
      iTmp = atoi(apTokens[SBD_CHR_SFRS_GARG_STALL]);
      if (iTmp > 0)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(pCharRec->ParkSpace, acTmp, iRet);

         if (*apTokens[SBD_CHR_SFRS_GARG_CLASS] == 'A' || *apTokens[SBD_CHR_SFRS_GARG_CLASS] == 'D')
            pCharRec->ParkType[0] = *apTokens[SBD_CHR_SFRS_GARG_CLASS];
      } else if (*apTokens[SBD_CHR_OTHR_GARAGE] > '0')
      {
         iTmp = atoi(apTokens[SBD_CHR_OTHR_GARAGE]);
         if (iTmp > 0 && iTmp < 999 && iTmp != 99)
         {
            iRet = sprintf(acTmp, "%d", iTmp);
            memcpy(pCharRec->ParkSpace, acTmp, iRet);
         }
      }

      // Car port Sqft
      iTmp = atoi(apTokens[SBD_CHR_SFRS_PORT_SQF]);
      if (iTmp > 100)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(pCharRec->CarpSqft, acTmp, iRet);
      }

      // Number of car port spaces
      iTmp = atoi(apTokens[SBD_CHR_SFRS_PORT_STALL]);
      if (iTmp > 0 && iTmp < 999 && iTmp != 99)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(pCharRec->Misc.sExtra.CarPort, acTmp, iRet);
         if (pCharRec->ParkSpace[0] == ' ')
         {
            memcpy(pCharRec->ParkSpace, acTmp, iRet);
            pCharRec->ParkType[0] = 'C';
         }
      } else if (*apTokens[SBD_CHR_OTHR_CARPORT] > '0') 
      {
         iTmp = atoi(apTokens[SBD_CHR_OTHR_CARPORT]);
         if (iTmp > 0 && iTmp < 999 && iTmp != 99)
         {
            iRet = sprintf(acTmp, "%d", iTmp);
            memcpy(pCharRec->Misc.sExtra.CarPort, acTmp, iRet);
            if (pCharRec->ParkSpace[0] == ' ')
            {
               memcpy(pCharRec->ParkSpace, acTmp, iRet);
               pCharRec->ParkType[0] = 'C';
            }
         }
      }

      // Open spaces
      iTmp = atoi(apTokens[SBD_CHR_OTHR_OPEN_SPACES]);
      if (iTmp > 0 && iTmp < 999 && iTmp != 99)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(pCharRec->Misc.sExtra.OpenSpace, acTmp, iRet);
         if (pCharRec->ParkSpace[0] == ' ')
         {
            memcpy(pCharRec->ParkSpace, acTmp, iRet);
            pCharRec->ParkType[0] = 'W';
         }
      }

      // RV spaces
      iTmp = atoi(apTokens[SBD_CHR_OTHR_RV_SPACES]);
      if (iTmp > 0 && iTmp < 99)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(pCharRec->Misc.sExtra.RVSpace, acTmp, iRet);
      }

      // Heat
      if (*apTokens[SBD_CHR_SFRS_HEAT] > ' ')
      {
         if (pTmp = findXlatCode(*apTokens[SBD_CHR_SFRS_HEAT], &asHeat[0]))
            pCharRec->Heating[0] = *pTmp;
         else
            LogMsg("*** Unknown Heat type: %s", apTokens[SBD_CHR_SFRS_HEAT]);
      }

#ifdef _DEBUG
      //if (!memcmp(apTokens[SBD_CHR_PARCEL_NBR], "0108301010000", 9))
      //   iRet = 0;
#endif
      // Cool
      if (*apTokens[SBD_CHR_SFRS_COOL] > ' ')
      {
         if (pTmp = findXlatCode(*apTokens[SBD_CHR_SFRS_COOL], &asCool[0]))
            pCharRec->Cooling[0] = *pTmp;
         else
            LogMsg("*** Unknown Cool type: %s", apTokens[SBD_CHR_SFRS_COOL]);
      }

      // Fireplace
      iTmp = atoi(apTokens[SBD_CHR_SFRS_NBR_FIRPLC]);
      if (iTmp > 0)
      {
         if (iTmp > 9)
            pCharRec->Fireplace[0] = 'M';
         else
            pCharRec->Fireplace[0] = *apTokens[SBD_CHR_SFRS_NBR_FIRPLC];
      }

      // Pool/Spa
      if (*apTokens[SBD_CHR_SFRS_POOL_TYPE] > ' ')
      {
         if (pTmp = findXlatCode(*apTokens[SBD_CHR_SFRS_POOL_TYPE], &asPool[0]))
            pCharRec->Pool[0] = *pTmp;
         else
            LogMsg("*** Unknown Pool type: %s", apTokens[SBD_CHR_SFRS_POOL_TYPE]);
      }

      // Number of units
      iTmp = atoi(apTokens[SBD_CHR_OTHR_NBR_UNITS]);
      if (iTmp > 1 && iTmp < 9999)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(pCharRec->Units, acTmp, iRet);
      }

      // Quality Class
      if (*apTokens[SBD_CHR_SFRS_QUALITY] > '0')
      {
         if (*apTokens[SBD_CHR_SFRS_CONST_TYPE] > '0')
         {
            pCharRec->BldgClass = *apTokens[SBD_CHR_SFRS_CONST_TYPE];
            iRet = sprintf(acTmp, "%c%s%c", pCharRec->BldgClass, apTokens[SBD_CHR_SFRS_QUALITY], *apTokens[SBD_CHR_SFRS_SHAPE]);
            vmemcpy(pCharRec->QualityClass, acTmp, SIZ_CHAR_QCLS);
         }

         iRet = Quality2Code(apTokens[SBD_CHR_SFRS_QUALITY], acTmp, NULL);
         if (iRet > 0)
            pCharRec->BldgQual = acTmp[0];
      } else if (*apTokens[SBD_CHR_OTHR_STRUC_TYPE] > '0')
      {
         pCharRec->BldgClass = *apTokens[SBD_CHR_OTHR_STRUC_TYPE];
         sprintf(acTmp, "%c%s", pCharRec->BldgClass, apTokens[SBD_CHR_OTHR_STRUC_QUAL]);
         vmemcpy(pCharRec->QualityClass, acTmp, SIZ_CHAR_QCLS);
         switch (*apTokens[SBD_CHR_OTHR_STRUC_QUAL])
         {
            case 'A':
            case 'G':
            case 'E':
            case 'V':
               pCharRec->BldgQual = *apTokens[SBD_CHR_OTHR_STRUC_QUAL];
            case 'L':   // Low
               pCharRec->BldgQual = 'F';
            case 'C':   // Cheap
               pCharRec->BldgQual = 'P';
         }
      }

      // Neighborhood
      if (*apTokens[SBD_CHR_NEIGHBORHOOD] > ' ')
      {
         blankRem(apTokens[SBD_CHR_NEIGHBORHOOD]);
         vmemcpy(pCharRec->Nbh_Code, apTokens[SBD_CHR_NEIGHBORHOOD], SIZ_CHAR_SIZE4);
      }

      // Gas

      // Electricity

      pCharRec->CRLF[0] = '\n';
      pCharRec->CRLF[1] = '\0';
      try {
         iRet = fputs(acOutbuf, fdOut);
      } catch (...)
      {
         LogMsg("??? Exception occurs in Sbd_ConvStdChar(),  APN=%s", apTokens[0]);
         iRet = -1;
         break;
      }

      if (!(++iCnt % 1000))
         printf("\r%u", iCnt);
   }

   if (fdIn) fclose(fdIn);
   if (fdOut) fclose(fdOut);

   // Sort output on ASMT
   if (iRet >= 0 && iCnt > 10000)
   {
      iRet = sortFile(acTmpFile, acCChrFile, "S(1,12,C,A) F(TXT)");
      LogMsg("Number of records output: %d", iRet);
   } else
   {
      LogMsg("*** Bad input file.  Please check.\n");
      iRet = -1;
   }

   return iRet;
}

/****************************** Sbd_ConvStdChar1 *****************************
 *
 * Convert PIMSPropertyCharacteristics.TXT to STDCHAR format.
 * Return number of records output.
 *
 *****************************************************************************/

int Sbd_ConvStdChar1(char *pInfile)
{
   FILE     *fdIn, *fdOut;
   char     acInbuf[MAX_RECSIZE], acOutbuf[2048], acTmpFile[256], acTmp[256], *pTmp, *pRec;
   int      iRet, iTmp, iCnt=0;
   double   dAcres;

   STDCHAR  *pCharRec=(STDCHAR *)&acOutbuf[0];

   LogMsg0("Convert char file to standard format");

   LogMsg("Open input file %s", pInfile);
   if (!(fdIn = fopen(pInfile, "r")))
      return -1;

   sprintf(acTmpFile, "%s\\%s\\%s_char.tmp", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   LogMsg("Open output file %s", acTmpFile);
   if (!(fdOut = fopen(acTmpFile, "w")))
   {
      fclose(fdIn);
      LogMsg("***** Error creating output file %s", acTmpFile);
      return -2;
   }

   // Skip first record - header
   iTmp = 0;
   for (iTmp = 0; iTmp < iHdrRows; iTmp++)
      pRec = fgets(acInbuf, MAX_RECSIZE, fdIn);

   while (!feof(fdIn))
   {
      pRec = fgets(acInbuf, MAX_RECSIZE, fdIn);

      if (!pRec)
         break;

      if (cDelim == ',')
         iRet = ParseStringNQ(pRec, cDelim, SBD_CHR_NEIGHBORHOOD+1, apTokens);
      else
         iRet = ParseStringIQ(pRec, cDelim, SBD_CHR_NEIGHBORHOOD+1, apTokens);
      if (iRet < SBD_CHR_NEIGHBORHOOD)
      {
         LogMsg("*** Bad CHAR record (%d): %.80s", iRet, acInbuf);
         continue;
      }

      memset(acOutbuf, ' ', sizeof(STDCHAR));

      // Apn
      vmemcpy(pCharRec->Apn, apTokens[SBD_CHR_PARCEL_NBR], SIZ_CHAR_APN);

      // Seq
      iTmp = atoi(apTokens[SBD_CHR_SFRS_SEQ]);
      if (iTmp > 0)
      {
         if (iTmp > 99)
         {
            sprintf(acTmp, "%4d", iTmp);
            vmemcpy(pCharRec->BldgSeqNo, acTmp, SIZ_CHAR_SIZE4);
         } else
         {
            sprintf(acTmp, "%2d", iTmp);
            vmemcpy(pCharRec->UnitSeqNo, acTmp, SIZ_CHAR_SIZE2);
         }
      }

      // Usecode
      iTmp = atol(apTokens[SBD_CHR_USE_CODE]);
      if (iTmp > 0)
         vmemcpy(pCharRec->LandUse, apTokens[SBD_CHR_USE_CODE], SIZ_CHAR_SIZE8);
      else if (*apTokens[SBD_CHR_OTHR_USE] > ' ')
         vmemcpy(pCharRec->LandUse, apTokens[SBD_CHR_OTHR_USE], SIZ_CHAR_SIZE8);

      // Lot acre
      dAcres = atof(apTokens[SBD_CHR_GROSS_ACRE]);
      if (dAcres > 0.001)
      {
         //dAcres *= 1000.0;
         sprintf(acTmp, "%d", (long)dAcres);
         vmemcpy(pCharRec->LotAcre, acTmp, SIZ_CHAR_SQFT);
      }

      // Lot Sqft
      long lLotSqft = atoi(apTokens[SBD_CHR_LAND_LOT_FOOTAGE]);
      if (lLotSqft > 100)
      {
         sprintf(acTmp, "%d", lLotSqft);
         vmemcpy(pCharRec->LotSqft, acTmp, SIZ_CHAR_SQFT);

         if (dAcres == 0.0)
         {
            dAcres = (double)lLotSqft / SQFT_FACTOR_1000;
            sprintf(acTmp, "%d", (long)dAcres);
            vmemcpy(pCharRec->LotAcre, acTmp, SIZ_CHAR_SQFT);
         }
      }

      // View
      if (*apTokens[SBD_CHR_LAND_VIEW_TYPE] > ' ')
      {
         if (pTmp = findXlatCode(*apTokens[SBD_CHR_LAND_VIEW_TYPE], &asView[0]))
            pCharRec->View[0] = *pTmp;
         else
            LogMsg("*** Unknown View Type: %s", apTokens[SBD_CHR_LAND_VIEW_TYPE]);
      }

      // Water
      if (*apTokens[SBD_CHR_LAND_WATER] > ' ')
      {
         if (pTmp = findXlatCode(*apTokens[SBD_CHR_LAND_WATER], &asWater[0]))
            pCharRec->Water = *pTmp;
         else
            LogMsg("*** Unknown Water code: %s", apTokens[SBD_CHR_LAND_WATER]);
      }

      // Sewer
      if (*apTokens[SBD_CHR_LAND_SEWER] > ' ')
      {
         if (pTmp = findXlatCode(*apTokens[SBD_CHR_LAND_SEWER], &asSewer[0]))
            pCharRec->Sewer = *pTmp;
         else
            LogMsg("*** Unknown Sewer code: %s", apTokens[SBD_CHR_LAND_SEWER]);
      }

      // BldgSqft
      iTmp = atoi(apTokens[SBD_CHR_SFRS_SIZE]);
      if (iTmp > 100)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(pCharRec->BldgSqft, acTmp, iRet);
      }

      // YrBlt
      iTmp = atoi(apTokens[SBD_CHR_SFRS_CONST_YEAR]);
      if (iTmp > 1600 && iTmp != 1900)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(pCharRec->YrBlt, acTmp, iRet);
      } else if (iTmp = atoi(apTokens[SBD_CHR_OTHR_CONST_YEAR]))
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(pCharRec->YrBlt, acTmp, iRet);
      }

      // YrEff
      iTmp = atoi(apTokens[SBD_CHR_SFRS_EFF_YEAR]);
      if (iTmp > 1600 && iTmp != 1900)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(pCharRec->YrEff, acTmp, iRet);
      } else if (iTmp = atoi(apTokens[SBD_CHR_OTHR_EFF_YEAR]))
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(pCharRec->YrEff, acTmp, iRet);
      }

      // Beds
      iTmp = atoi(apTokens[SBD_CHR_SFRS_NBR_BDRM]);
      if (iTmp > 0)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(pCharRec->Beds, acTmp, iRet);
      }

      // Baths
      iTmp = atoi(apTokens[SBD_CHR_SFRS_NBR_BATH]);
      if (iTmp > 0)
      {
         iRet = sprintf(acTmp, "%d", iTmp/100);
         memcpy(pCharRec->FBaths, acTmp, iRet);
         memcpy(pCharRec->Bath_4Q, acTmp, iRet);
         iRet = iTmp - (iTmp/100)*100;
         if (iRet > 0)
         {
            // Half baths
            pCharRec->HBaths[0] = '1';
            if (iRet <= 30)
               pCharRec->Bath_1Q[0] = '1';
            else if (iRet <= 60)
               pCharRec->Bath_2Q[0] = '1';
            else 
               pCharRec->Bath_3Q[0] = '1';
         }
      }

      // Total rooms
      iTmp = atoi(apTokens[SBD_CHR_SFRS_TOTAL_ROOM]);
      if (iTmp > 0)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(pCharRec->Rooms, acTmp, iRet);
      }

      // Floors
      int iStories = 0;
      iTmp = atoi(apTokens[SBD_CHR_SFRS_FLR3_SIZE]);
      if (iTmp > 0)
      {
         iStories = 3;
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(pCharRec->Sqft_Above2nd, acTmp, iRet);
      }
      iTmp = atoi(apTokens[SBD_CHR_SFRS_FLR2_SIZE]);
      if (iTmp > 0)
      {
         if (!iStories)
            iStories = 2;
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(pCharRec->Sqft_2ndFl, acTmp, iRet);
      }
      iTmp = atoi(apTokens[SBD_CHR_SFRS_FLR1_SIZE]);
      if (iTmp > 0)
      {
         if (!iStories)
            iStories = 1;
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(pCharRec->Sqft_1stFl, acTmp, iRet);
      }
        
      if (*apTokens[SBD_CHR_OTHR_NBR_STORIES] > '0' && iStories == 0)
         iStories = atoi(apTokens[SBD_CHR_OTHR_NBR_STORIES]);
      if (iStories > 0)
      {
         iRet = sprintf(acTmp, "%d.0", iStories);
         memcpy(pCharRec->Stories, acTmp, iRet);
      }

      // Elevator
      if (*apTokens[SBD_CHR_OTHR_ELEVATOR] > '0')
         pCharRec->HasElevator = 'Y';

      // Basement
      iTmp = atoi(apTokens[SBD_CHR_SFRS_BAS1_SIZE]);
      iTmp += atoi(apTokens[SBD_CHR_SFRS_BAS2_SIZE]);
      if (iTmp > 0)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(pCharRec->BsmtSqft, acTmp, iRet);
      }

      // Addition
      iTmp = atoi(apTokens[SBD_CHR_SFRS_ADD1_SIZE]);
      iTmp += atoi(apTokens[SBD_CHR_SFRS_ADD2_SIZE]);
      iTmp += atoi(apTokens[SBD_CHR_SFRS_ADD3_SIZE]);
      if (iTmp > 0)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(pCharRec->MiscSqft, acTmp, iRet);
      }

      // Porch
      iTmp = atoi(apTokens[SBD_CHR_SFRS_PCH1_SIZE]);
      iTmp += atoi(apTokens[SBD_CHR_SFRS_PCH2_SIZE]);
      iTmp += atoi(apTokens[SBD_CHR_SFRS_PCH3_SIZE]);
      if (iTmp > 0)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(pCharRec->PorchSqft, acTmp, iRet);
      }

      // Decks
      iTmp = atoi(apTokens[SBD_CHR_SFRS_DECK_SQF]);
      if (iTmp > 0)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(pCharRec->DeckSqft, acTmp, iRet);
      }

      // Garage Sqft
      iTmp = atoi(apTokens[SBD_CHR_SFRS_GARG_SQF]);
      if (iTmp > 100)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(pCharRec->GarSqft, acTmp, iRet);
      }

      // Number of parking spaces
      iTmp = atoi(apTokens[SBD_CHR_SFRS_GARG_STALL]);
      if (iTmp > 0)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(pCharRec->ParkSpace, acTmp, iRet);

         if (*apTokens[SBD_CHR_SFRS_GARG_CLASS] == 'A' || *apTokens[SBD_CHR_SFRS_GARG_CLASS] == 'D')
            pCharRec->ParkType[0] = *apTokens[SBD_CHR_SFRS_GARG_CLASS];
      } else if (*apTokens[SBD_CHR_OTHR_GARAGE] > '0')
      {
         iTmp = atoi(apTokens[SBD_CHR_OTHR_GARAGE]);
         if (iTmp > 0 && iTmp < 999 && iTmp != 99)
         {
            iRet = sprintf(acTmp, "%d", iTmp);
            memcpy(pCharRec->ParkSpace, acTmp, iRet);
         }
      }

      // Car port Sqft
      iTmp = atoi(apTokens[SBD_CHR_SFRS_PORT_SQF]);
      if (iTmp > 100)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(pCharRec->CarpSqft, acTmp, iRet);
      }

      // Number of car port spaces
      iTmp = atoi(apTokens[SBD_CHR_SFRS_PORT_STALL]);
      if (iTmp > 0 && iTmp < 999 && iTmp != 99)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(pCharRec->Misc.sExtra.CarPort, acTmp, iRet);
         if (pCharRec->ParkSpace[0] == ' ')
         {
            memcpy(pCharRec->ParkSpace, acTmp, iRet);
            pCharRec->ParkType[0] = 'C';
         }
      } else if (*apTokens[SBD_CHR_OTHR_CARPORT] > '0') 
      {
         iTmp = atoi(apTokens[SBD_CHR_OTHR_CARPORT]);
         if (iTmp > 0 && iTmp < 999 && iTmp != 99)
         {
            iRet = sprintf(acTmp, "%d", iTmp);
            memcpy(pCharRec->Misc.sExtra.CarPort, acTmp, iRet);
            if (pCharRec->ParkSpace[0] == ' ')
            {
               memcpy(pCharRec->ParkSpace, acTmp, iRet);
               pCharRec->ParkType[0] = 'C';
            }
         }
      }

      // Open spaces
      iTmp = atoi(apTokens[SBD_CHR_OTHR_OPEN_SPACES]);
      if (iTmp > 0 && iTmp < 999 && iTmp != 99)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(pCharRec->Misc.sExtra.OpenSpace, acTmp, iRet);
         if (pCharRec->ParkSpace[0] == ' ')
         {
            memcpy(pCharRec->ParkSpace, acTmp, iRet);
            pCharRec->ParkType[0] = 'W';
         }
      }

      // RV spaces
      iTmp = atoi(apTokens[SBD_CHR_OTHR_RV_SPACES]);
      if (iTmp > 0 && iTmp < 99)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(pCharRec->Misc.sExtra.RVSpace, acTmp, iRet);
      }

      // Heat
      if (*apTokens[SBD_CHR_SFRS_HEAT] > ' ')
      {
         if (pTmp = findXlatCode(*apTokens[SBD_CHR_SFRS_HEAT], &asHeat[0]))
            pCharRec->Heating[0] = *pTmp;
         else
            LogMsg("*** Unknown Heat type: %s", apTokens[SBD_CHR_SFRS_HEAT]);
      }

#ifdef _DEBUG
      //if (!memcmp(apTokens[SBD_CHR_PARCEL_NBR], "0108301030000", 9))
      //   iRet = 0;
#endif
      // Cool
      if (*apTokens[SBD_CHR_SFRS_COOL] > ' ')
      {
         if (pTmp = findXlatCode(*apTokens[SBD_CHR_SFRS_COOL], &asCool[0]))
            pCharRec->Cooling[0] = *pTmp;
         else
            LogMsg("*** Unknown Cool type: %s", apTokens[SBD_CHR_SFRS_COOL]);
      }

      // Fireplace
      iTmp = atoi(apTokens[SBD_CHR_SFRS_NBR_FIRPLC]);
      if (iTmp > 0)
      {
         if (iTmp > 9)
            pCharRec->Fireplace[0] = 'M';
         else
         {
            iRet = sprintf(acTmp, "%d", iTmp);
            memcpy(pCharRec->Fireplace, acTmp, iRet);
         }
      }

      // Pool/Spa
      if (*apTokens[SBD_CHR_SFRS_POOL_TYPE] > ' ')
      {
         if (pTmp = findXlatCode(*apTokens[SBD_CHR_SFRS_POOL_TYPE], &asPool[0]))
            pCharRec->Pool[0] = *pTmp;
         else
            LogMsg("*** Unknown Pool type: %s", apTokens[SBD_CHR_SFRS_POOL_TYPE]);
      }

      // Number of units
      iTmp = atoi(apTokens[SBD_CHR_OTHR_NBR_UNITS]);
      if (iTmp > 1 && iTmp < 9999)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(pCharRec->Units, acTmp, iRet);
      }

      // Quality Class
      if (*apTokens[SBD_CHR_SFRS_CONST_TYPE] > '0')
      {
         pCharRec->BldgClass = *apTokens[SBD_CHR_SFRS_CONST_TYPE];
         if (*apTokens[SBD_CHR_SFRS_QUALITY] > ' ')
         {
            iRet = sprintf(acTmp, "%c%s%c", pCharRec->BldgClass, apTokens[SBD_CHR_SFRS_QUALITY], *apTokens[SBD_CHR_SFRS_SHAPE]);
            vmemcpy(pCharRec->QualityClass, acTmp, SIZ_CHAR_QCLS);
            iRet = Quality2Code(apTokens[SBD_CHR_SFRS_QUALITY], acTmp, NULL);
            if (iRet > 0)
               pCharRec->BldgQual = acTmp[0];
         }
      } else if (*apTokens[SBD_CHR_OTHR_STRUC_TYPE] > '0')
      {
         pCharRec->BldgClass = *apTokens[SBD_CHR_OTHR_STRUC_TYPE];
         sprintf(acTmp, "%c%s", pCharRec->BldgClass, apTokens[SBD_CHR_OTHR_STRUC_QUAL]);
         vmemcpy(pCharRec->QualityClass, acTmp, SIZ_CHAR_QCLS);
         switch (*apTokens[SBD_CHR_OTHR_STRUC_QUAL])
         {
            case 'A':
            case 'G':
            case 'E':
            case 'V':
               pCharRec->BldgQual = *apTokens[SBD_CHR_OTHR_STRUC_QUAL];
            case 'L':   // Low
               pCharRec->BldgQual = 'F';
            case 'C':   // Cheap
               pCharRec->BldgQual = 'P';
         }
      }

      // Neighborhood
      if (*apTokens[SBD_CHR_NEIGHBORHOOD] > ' ')
      {
         blankRem(apTokens[SBD_CHR_NEIGHBORHOOD]);
         vmemcpy(pCharRec->Nbh_Code, apTokens[SBD_CHR_NEIGHBORHOOD], SIZ_CHAR_SIZE4);
      }

      // Gas

      // Electricity

      pCharRec->CRLF[0] = '\n';
      pCharRec->CRLF[1] = '\0';
      try {
         iRet = fputs(acOutbuf, fdOut);
      } catch (...)
      {
         LogMsg("??? Exception occurs in Sbd_ConvStdChar1(),  APN=%s", apTokens[0]);
         iRet = -1;
         break;
      }

      if (!(++iCnt % 1000))
         printf("\r%u", iCnt);
   }

   if (fdIn) fclose(fdIn);
   if (fdOut) fclose(fdOut);

   // Sort output on ASMT
   if (iRet >= 0 && iCnt > 10000)
   {
      iRet = sortFile(acTmpFile, acCChrFile, "S(1,12,C,A) F(TXT)");
      LogMsg("Number of records output: %d", iRet);
   } else
   {
      LogMsg("*** Bad input file.  Please check.\n");
      iRet = -1;
   }

   return iRet;
}

/******************************* Sbd_MergeChar *******************************
 *
 * Merge Sbd_Char.dat in STDCHAR format
 *
 *****************************************************************************/

int Sbd_MergeChar(int iSkip)
{
   char     acBuf[MAX_RECSIZE], acRawFile[_MAX_PATH], acOutFile[_MAX_PATH], acTmp[256];
   int      iRet, lCnt=0;
   DWORD    nBytesRead, nBytesWritten;
   BOOL     bRet, bUseR01=true;
   HANDLE   fhIn, fhOut;

   sprintf(acRawFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "R01");
   sprintf(acOutFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "C01");

   // Rename files for processing
   if (_access(acRawFile, 0))
   {
      sprintf(acRawFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "S01");
      if (!_access(acRawFile, 0))
      {
         LogMsg("Use %s as input for MergeChar", acRawFile);
         bUseR01 = false;
      } else
      {
         LogMsg("***** Error in MergeChar(): Missing input file %s.  Please recheck!", acRawFile);
         return -1;
      }
   }

   // Open Char file
   LogMsg("Open Char file %s", acCChrFile);
   fdChar = fopen(acCChrFile, "r");
   if (fdChar == NULL)
   {
      LogMsg("***** Error opening acreage file: %s\n", acCChrFile);
      return -2;
   }

   // Open Input file
   LogMsg("Open input file %s", acRawFile);
   fhIn = CreateFile(acRawFile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhIn == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening input file: %s\n", acRawFile);
      return -3;
   }

   // Open Output file
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening output file: %s\n", acOutFile);
      return -4;
   }

   // Copy skip record
   memset(acBuf, ' ', iRecLen);
   while (iSkip-- > 0)
   {
      ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);
      // Ignore record start with 99999999
      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
   }

   // Merge loop
   do
   {
      bRet = ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);

      // Check for EOF
      if (!bRet)
      {
         LogMsg("***** Error reading input file %s (%f)", acRawFile, GetLastError());
         break;
      }

      // EOF ?
      if (!nBytesRead)
         break;

      // Update acreage info
      if (fdChar)
         iRet = Sbd_MergeStdChar(acBuf);

      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);

      if (!bRet)
      {
         LogMsg("***** Error writing to out file %s: %d\n", acOutFile, GetLastError());
         break;
      }
   } while (bRet);

   // Close files
   if (fdChar)
      fclose(fdChar);
   if (fhOut)
      CloseHandle(fhOut);
   if (fhIn)
      CloseHandle(fhIn);

   // Rename R01 to SAV if R01 is used as input
   if (bUseR01)
   {
      sprintf(acTmp, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "SAV");
      if (!_access(acTmp, 0))
      {
         LogMsg("Remove saved file %s", acTmp);
         if (iRet=remove(acTmp))
            LogMsg("***** Error removing %s to %s (%d)", acTmp, errno);
      }

      LogMsg("Rename %s to %s", acRawFile, acTmp);
      if (iRet=rename(acRawFile, acTmp))
         LogMsg("***** Error renaming %s to %s (%d)", acRawFile, acTmp, errno);
   } else
      sprintf(acRawFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "R01");

   LogMsg("Rename %s to %s", acOutFile, acRawFile);
   if (iRet=rename(acOutFile, acRawFile))
      LogMsg("***** Error renaming %s to %s (%d)", acOutFile, acRawFile, errno);

   LogMsg("Total output records:       %u", lCnt);
   LogMsg("Total records updated:      %u", lCharMatch);

   lRecCnt = lCnt;
   return iRet;
}

/********************************* Sbd_FormatSale *****************************
 
  Sale Type code translation (A, P, T, U, ...)

  Blank = Sales Price is the Confirmed Amount from the PCOR.
      A = Sales Price is an Adjusted Amount entered by an appraiser which can take into account
          things such as personal property included in the gross sales price. (W)
            - 04/03/2018 change from 'I' to 'W'
      T = Sales Price is the Document Amount computed by dividing the Document Transfer Tax
          by .0011, and the Sales Price is considered to be the total sales price for the
          parcel (land and improvements) (F)
      P = S Sales Price is the Document Amount computed by dividing the Document Transfer Tax
          by .0011, and the Sales Price is considered to be the total sales price for the
          parcel (land and improvements) less liens and encumbrances. (P)
            - 12/07/2016 change from 'N' to 'P'
      U = Sales Price is the Document Amount computed by dividing the Document Transfer Tax
          by .0011, and it is unknown whether the Sales Price is considered to be the total
          or partial sales price for the parcel (land and improvements). (U)

  Return -1 if bad record.  0 if OK.
 
 ******************************************************************************/

int Sbd_FmtSfrSale(FILE *fdOut, char *pFmtSale, char *pSale)
{
   SCSAL_REC *pStdSale = (SCSAL_REC *)pFmtSale;
   int		iTmp, lCurSaleDt, lPrice, iLen;
   char     acDocDate[32], acTmp[32], *pTmp;

   memset(pFmtSale, ' ', sizeof(SCSAL_REC));

   memcpy(pStdSale->Apn, pSale+OFF_SFR_SALE_PARCEL_NUMBER, SIZ_SFR_SALE_PARCEL_NUMBER);
   pTmp = dateConversion(pSale+OFF_SFR_SALE_SALE_DATE, acDocDate, MM_DD_YYYY_1);
   if (pTmp)
      lCurSaleDt = atoin(pTmp, SIZ_SALE1_DT);
   else
      return -1;

   iLen = strlen(pSale);

   // Sale date
   memcpy(pStdSale->DocDate, acDocDate, SALE_SIZ_DOCDATE);
   iTmp = atol(acDocDate);
   if (iTmp > lLastRecDate && iTmp < lToday)
      lLastRecDate = iTmp;

   // Sale Price
   lPrice = atoin(pSale+OFF_SFR_SALE_SALE_PRICE, SIZ_SFR_SALE_SALE_PRICE);
   if (lPrice > 100)
   {
      iTmp = sprintf(acTmp, "%*u", SALE_SIZ_SALEPRICE, lPrice);
      memcpy(pStdSale->SalePrice, acTmp, iTmp);

      // DocType
      pStdSale->DocType[0] = '1';         // GD

      // Sale code
      char cSaleType = *(pSale+OFF_SFR_SALE_SALE_TYPE);
      if (cSaleType > ' ')
      {
         switch (cSaleType)
         {
            case 'A':
               pStdSale->SaleCode[0] = 'W';
               break;
            case 'T':
               pStdSale->SaleCode[0] = 'F';
               break;
            case 'P':
               pStdSale->SaleCode[0] = 'P';
               break;
            case 'U':
               pStdSale->SaleCode[0] = ' ';     // Unknown whether F or P
               break;
            default:
               LogMsg("*** Unknown Sfr Sale code: %c - %.13s", cSaleType, pSale);
         }
      }
   }

#ifdef _DEBUG
   //if (!memcmp(pSale, "0108491180000", 12))
   //   iTmp = 1;
#endif

   // Update current sale
   if (iLen > OFF_SFR_SALE_DOCTYPE)
   {
      memcpy(pStdSale->DocNum, pSale+OFF_SFR_SALE_DOCNUM, 12);

      // DocType
      iTmp = findSortedDocType(pSale+OFF_SFR_SALE_DOCTYPE, (IDX_TBL5 *)&SBD_DocType[0]);
      if (iTmp >= 0)
      {
         memcpy(pStdSale->DocType, SBD_DocType[iTmp].pCode, SBD_DocType[iTmp].iCodeLen);
         pStdSale->NoneSale_Flg = SBD_DocType[iTmp].flag;
      } else
         LogMsg("*** Unknown DocType: %.4s - %.13s", pSale+OFF_SFR_SALE_DOCTYPE, pSale);

      memcpy(pStdSale->DocCode, pSale+OFF_SFR_SALE_DOCTYPE+1, 3);

      // Seller
      *(pSale+iLen-1) = 0;
      *(pSale+OFF_SFR_SALE_SELLER+SALE_SIZ_SELLER) = 0;
      vmemcpy(pStdSale->Seller1, pSale+OFF_SFR_SALE_SELLER, SALE_SIZ_SELLER);
   }

   pStdSale->Spc_Flg = SALE_TYPE_SFR;
   pStdSale->CRLF[0] = '\n';
   pStdSale->CRLF[1] = 0;
   iTmp = fputs(pFmtSale, fdOut);

   // Extract prev transaction if DocNum avail
   if (iLen > OFF_SFR_SALE_PRIOR_DOCTYPE)
   {
      pTmp = dateConversion(pSale+OFF_SFR_SALE_PRIOR_SALE_DATE, acDocDate, MM_DD_YYYY_1);
      if (pTmp && !memcmp(acDocDate, pSale+OFF_SFR_SALE_PRIOR_DOCNUM, 4))
      {
         memcpy(pStdSale->DocDate, acDocDate, SALE_SIZ_DOCDATE);
         memcpy(pStdSale->DocNum, pSale+OFF_SFR_SALE_PRIOR_DOCNUM, 12);

         // Sale Price
         lPrice = atoin(pSale+OFF_SFR_SALE_PRIOR_SALE_PRICE, SIZ_SFR_SALE_SALE_PRICE);
         if (lPrice > 100)
         {
            iTmp = sprintf(acTmp, "%*u", SALE_SIZ_SALEPRICE, lPrice);
            memcpy(pStdSale->SalePrice, acTmp, iTmp);

            // DocType
            pStdSale->DocType[0] = '1';         // GD
         } else
         {
            memset(pStdSale->SalePrice, ' ', SALE_SIZ_SALEPRICE);
            memset(pStdSale->DocType, ' ', 3);
         }
         pStdSale->SaleCode[0] = ' ';

         // DocType
         iTmp = findDocType(pSale+OFF_SFR_SALE_PRIOR_DOCTYPE, (IDX_TBL5 *)&SBD_DocType[0]);
         if (iTmp >= 0)
            memcpy(pStdSale->DocType, SBD_DocType[iTmp].pCode, SBD_DocType[iTmp].iCodeLen);
         else
         {
            LogMsg("*** Unknown DocType: %.4s - %.13s", pSale+OFF_SFR_SALE_PRIOR_DOCTYPE, pSale);
         }
         memcpy(pStdSale->DocCode, pSale+OFF_SFR_SALE_PRIOR_DOCTYPE+1, 3);

         // Seller
         *(pSale+OFF_SFR_SALE_PRIOR_SELLER+SALE_SIZ_SELLER) = 0;
         memset(pStdSale->Seller1, ' ', SALE_SIZ_SELLER);
         vmemcpy(pStdSale->Seller1, pSale+OFF_SFR_SALE_PRIOR_SELLER, SALE_SIZ_SELLER);
         iTmp = fputs(pFmtSale, fdOut);
      } else
         LogMsg("*** Questionable transaction: %.12s %s %.12s - ignore", pSale, acDocDate, pSale+OFF_SFR_SALE_PRIOR_DOCNUM);
   }

   return 0;
}

int Sbd_FmtMhSale(FILE *fdOut, char *pFmtSale, char *pSale)
{
   SCSAL_REC *pStdSale = (SCSAL_REC *)pFmtSale;
   int		iTmp, lCurSaleDt, lPrice, iLen;
   char     acDocDate[32], acTmp[32], *pTmp;

   memset(pFmtSale, ' ', sizeof(SCSAL_REC));

   memcpy(pStdSale->Apn, pSale+OFF_MH_SALE_PARCEL_NUMBER, SIZ_MH_SALE_PARCEL_NUMBER);
   pTmp = dateConversion(pSale+OFF_MH_SALE_SALE_DATE, acDocDate, MM_DD_YYYY_1);
   if (pTmp)
      lCurSaleDt = atoin(pTmp, SIZ_SALE1_DT);
   else
      return -1;

   iLen = strlen(pSale);

   // Sale date
   memcpy(pStdSale->DocDate, acDocDate, SALE_SIZ_DOCDATE);
   iTmp = atol(acDocDate);
   if (iTmp > lLastRecDate && iTmp < lToday)
      lLastRecDate = iTmp;

   // Sale Price
   lPrice = atoin(pSale+OFF_MH_SALE_SALE_PRICE, SIZ_MH_SALE_SALE_PRICE);
   if (lPrice > 100)
   {
      iTmp = sprintf(acTmp, "%*u", SALE_SIZ_SALEPRICE, lPrice);
      memcpy(pStdSale->SalePrice, acTmp, iTmp);

      // DocType
      pStdSale->DocType[0] = '1';         // GD

      // Sale code
      char cSaleType = *(pSale+OFF_MH_SALE_SALE_TYPE);
      if (cSaleType > ' ')
      {
         switch (cSaleType)
         {
            case 'A':
               pStdSale->SaleCode[0] = 'W';
               break;
            case 'T':
               pStdSale->SaleCode[0] = 'F';
               break;
            case 'P':
            case 'U':
               pStdSale->SaleCode[0] = 'P';
               break;
            default:
               LogMsg("*** Unknown MH Sale code: %c", cSaleType);
         }
      }
   }

   // 0108303020000
#ifdef _DEBUG
   //if (!memcmp(pSale, "0108303020000", 12))
   //   iTmp = 1;
#endif

   // Update current sale
   if (iLen > OFF_MH_SALE_DOCTYPE)
   {
      memcpy(pStdSale->DocNum, pSale+OFF_MH_SALE_DOCNUM, 12);

      // DocType
      iTmp = findSortedDocType(pSale+OFF_MH_SALE_DOCTYPE, (IDX_TBL5 *)&SBD_DocType[0]);
      if (iTmp >= 0)
         memcpy(pStdSale->DocType, SBD_DocType[iTmp].pCode, SBD_DocType[iTmp].iCodeLen);
      else
         LogMsg("*** Unknown DocType: %.4s - %.13s", pSale+OFF_MH_SALE_DOCTYPE, pSale);

      memcpy(pStdSale->DocCode, pSale+OFF_MH_SALE_DOCTYPE+1, 3);

      // Seller
      *(pSale+iLen-1) = 0;
      *(pSale+OFF_MH_SALE_SELLER+SALE_SIZ_SELLER) = 0;
      vmemcpy(pStdSale->Seller1, pSale+OFF_MH_SALE_SELLER, SALE_SIZ_SELLER);
   }

   pStdSale->Spc_Flg = SALE_TYPE_MH;
   pStdSale->CRLF[0] = '\n';
   pStdSale->CRLF[1] = 0;
   iTmp = fputs(pFmtSale, fdOut);

   // Extract prev transaction if DocNum avail
   if (iLen > OFF_MH_SALE_PRIOR_DOCTYPE)
   {
      pTmp = dateConversion(pSale+OFF_MH_SALE_PRIOR_SALE_DATE, acDocDate, MM_DD_YYYY_1);
      if (pTmp && !memcmp(acDocDate, pSale+OFF_MH_SALE_PRIOR_DOCNUM, 4))
      {
         memcpy(pStdSale->DocDate, acDocDate, SALE_SIZ_DOCDATE);
         memcpy(pStdSale->DocNum, pSale+OFF_MH_SALE_PRIOR_DOCNUM, 12);

         // Sale Price
         lPrice = atoin(pSale+OFF_MH_SALE_PRIOR_SALE_PRICE, SIZ_MH_SALE_SALE_PRICE);
         if (lPrice > 100)
         {
            iTmp = sprintf(acTmp, "%*u", SALE_SIZ_SALEPRICE, lPrice);
            memcpy(pStdSale->SalePrice, acTmp, iTmp);

            // DocType
            pStdSale->DocType[0] = '1';         // GD
         } else
         {
            memset(pStdSale->SalePrice, ' ', SALE_SIZ_SALEPRICE);
            memset(pStdSale->DocType, ' ', 3);
         }
         pStdSale->SaleCode[0] = ' ';

         // DocType
         iTmp = findDocType(pSale+OFF_MH_SALE_PRIOR_DOCTYPE, (IDX_TBL5 *)&SBD_DocType[0]);
         if (iTmp >= 0)
            memcpy(pStdSale->DocType, SBD_DocType[iTmp].pCode, SBD_DocType[iTmp].iCodeLen);
         else
         {
            LogMsg("*** Unknown DocType: %.4s - %.13s", pSale+OFF_MH_SALE_PRIOR_DOCTYPE, pSale);
         }
         memcpy(pStdSale->DocCode, pSale+OFF_MH_SALE_PRIOR_DOCTYPE+1, 3);

         // Seller
         *(pSale+OFF_MH_SALE_PRIOR_SELLER+SALE_SIZ_SELLER) = 0;
         memset(pStdSale->Seller1, ' ', SALE_SIZ_SELLER);
         vmemcpy(pStdSale->Seller1, pSale+OFF_MH_SALE_PRIOR_SELLER, SALE_SIZ_SELLER);
         iTmp = fputs(pFmtSale, fdOut);
      } else
         LogMsg("*** Questionable transaction: %.12s %s %.12s - ignore", pSale, acDocDate, pSale+OFF_MH_SALE_PRIOR_DOCNUM);
   }

   return 0;
}

int Sbd_FmtLandSale(FILE *fdOut, char *pFmtSale, char *pSale)
{
   SCSAL_REC *pStdSale = (SCSAL_REC *)pFmtSale;
   int		iTmp, lCurSaleDt, lPrice, iLen;
   char     acDocDate[32], acTmp[32], *pTmp;

   memset(pFmtSale, ' ', sizeof(SCSAL_REC));

   memcpy(pStdSale->Apn, pSale+OFF_LAND_SALE_PARCEL_NUMBER, SIZ_LAND_SALE_PARCEL_NUMBER);
   pTmp = dateConversion(pSale+OFF_LAND_SALE_SALE_DATE, acDocDate, MM_DD_YYYY_1);
   if (pTmp)
      lCurSaleDt = atoin(pTmp, SIZ_SALE1_DT);
   else
      return -1;

   iLen = strlen(pSale);

   // Sale date
   memcpy(pStdSale->DocDate, acDocDate, SALE_SIZ_DOCDATE);
   iTmp = atol(acDocDate);
   if (iTmp > lLastRecDate && iTmp < lToday)
      lLastRecDate = iTmp;

   // Sale Price
   lPrice = atoin(pSale+OFF_LAND_SALE_SALE_PRICE, SIZ_LAND_SALE_SALE_PRICE);
   if (lPrice > 100)
   {
      iTmp = sprintf(acTmp, "%*u", SALE_SIZ_SALEPRICE, lPrice);
      memcpy(pStdSale->SalePrice, acTmp, iTmp);

      // DocType
      pStdSale->DocType[0] = '1';         // GD

      // Sale code
      char cSaleType = *(pSale+OFF_LAND_SALE_SALE_TYPE);
      if (cSaleType > ' ')
      {
         switch (cSaleType)
         {
            case 'A':
               pStdSale->SaleCode[0] = 'W';
               break;
            case 'T':
               pStdSale->SaleCode[0] = 'F';
               break;
            case 'P':
            case 'U':
               pStdSale->SaleCode[0] = 'P';
               break;
            default:
               LogMsg("*** Unknown Land Sale code: %c", cSaleType);
         }
      }
   }

   // 0108303020000
#ifdef _DEBUG
   //if (!memcmp(pSale, "0108303020000", 12))
   //   iTmp = 1;
#endif

   // Update current sale
   if (iLen > OFF_LAND_SALE_DOCTYPE)
   {
      memcpy(pStdSale->DocNum, pSale+OFF_LAND_SALE_DOCNUM, 12);

      // DocType
      iTmp = findSortedDocType(pSale+OFF_LAND_SALE_DOCTYPE, (IDX_TBL5 *)&SBD_DocType[0]);
      if (iTmp >= 0)
         memcpy(pStdSale->DocType, SBD_DocType[iTmp].pCode, SBD_DocType[iTmp].iCodeLen);
      else
         LogMsg("*** Unknown DocType: %.4s - %.13s", pSale+OFF_LAND_SALE_DOCTYPE, pSale);

      memcpy(pStdSale->DocCode, pSale+OFF_LAND_SALE_DOCTYPE+1, 3);

      // Seller
      *(pSale+iLen-1) = 0;
      *(pSale+OFF_LAND_SALE_SELLER+SALE_SIZ_SELLER) = 0;
      vmemcpy(pStdSale->Seller1, pSale+OFF_LAND_SALE_SELLER, SALE_SIZ_SELLER);
   }

   pStdSale->Spc_Flg = SALE_TYPE_LAND;
   pStdSale->CRLF[0] = '\n';
   pStdSale->CRLF[1] = 0;
   iTmp = fputs(pFmtSale, fdOut);

   // Extract prev transaction if DocNum avail
   if (iLen > OFF_LAND_SALE_PRIOR_DOCTYPE)
   {
      pTmp = dateConversion(pSale+OFF_LAND_SALE_PRIOR_SALE_DATE, acDocDate, MM_DD_YYYY_1);
      if (pTmp && !memcmp(acDocDate, pSale+OFF_LAND_SALE_PRIOR_DOCNUM, 4))
      {
         memcpy(pStdSale->DocDate, acDocDate, SALE_SIZ_DOCDATE);
         memcpy(pStdSale->DocNum, pSale+OFF_LAND_SALE_PRIOR_DOCNUM, 12);

         // Sale Price
         lPrice = atoin(pSale+OFF_LAND_SALE_PRIOR_SALE_PRICE, SIZ_LAND_SALE_SALE_PRICE);
         if (lPrice > 100)
         {
            iTmp = sprintf(acTmp, "%*u", SALE_SIZ_SALEPRICE, lPrice);
            memcpy(pStdSale->SalePrice, acTmp, iTmp);

            // DocType
            pStdSale->DocType[0] = '1';         // GD
         } else
         {
            memset(pStdSale->SalePrice, ' ', SALE_SIZ_SALEPRICE);
            memset(pStdSale->DocType, ' ', 3);
         }
         pStdSale->SaleCode[0] = ' ';

         // DocType
         iTmp = findDocType(pSale+OFF_LAND_SALE_PRIOR_DOCTYPE, (IDX_TBL5 *)&SBD_DocType[0]);
         if (iTmp >= 0)
            memcpy(pStdSale->DocType, SBD_DocType[iTmp].pCode, SBD_DocType[iTmp].iCodeLen);
         else
         {
            LogMsg("*** Unknown DocType: %.4s - %.13s", pSale+OFF_LAND_SALE_PRIOR_DOCTYPE, pSale);
         }
         memcpy(pStdSale->DocCode, pSale+OFF_LAND_SALE_PRIOR_DOCTYPE+1, 3);

         // Seller
         *(pSale+OFF_LAND_SALE_PRIOR_SELLER+SALE_SIZ_SELLER) = 0;
         memset(pStdSale->Seller1, ' ', SALE_SIZ_SELLER);
         vmemcpy(pStdSale->Seller1, pSale+OFF_LAND_SALE_PRIOR_SELLER, SALE_SIZ_SELLER);
         iTmp = fputs(pFmtSale, fdOut);
      } else
         LogMsg("*** Questionable transaction: %.12s %s %.12s - ignore", pSale, acDocDate, pSale+OFF_LAND_SALE_PRIOR_DOCNUM);
   }

   return 0;
}

int Sbd_FmtDockSale(FILE *fdOut, char *pFmtSale, char *pSale)
{
   SCSAL_REC *pStdSale = (SCSAL_REC *)pFmtSale;
   int		iTmp, lCurSaleDt, lPrice, iLen;
   char     acDocDate[32], acTmp[32], *pTmp;

   memset(pFmtSale, ' ', sizeof(SCSAL_REC));

   memcpy(pStdSale->Apn, pSale+OFF_DOCK_SALE_PARCEL_NUMBER, SIZ_DOCK_SALE_PARCEL_NUMBER);
   pTmp = dateConversion(pSale+OFF_DOCK_SALE_SALE_DATE, acDocDate, MM_DD_YYYY_1);
   if (pTmp)
      lCurSaleDt = atoin(pTmp, SIZ_SALE1_DT);
   else
      return -1;

   iLen = strlen(pSale);

   // Primary APN
   if (*(pSale+OFF_DOCK_SALE_PARCEL_NUMBER_FROM) >= '0')
      memcpy(pStdSale->PrimaryApn, pSale+OFF_DOCK_SALE_PARCEL_NUMBER_FROM, SIZ_MP_SALE_PARCEL_NUMBER);

   // Sale date
   memcpy(pStdSale->DocDate, acDocDate, SALE_SIZ_DOCDATE);
   iTmp = atol(acDocDate);
   if (iTmp > lLastRecDate && iTmp < lToday)
      lLastRecDate = iTmp;

   // Sale Price
   lPrice = atoin(pSale+OFF_DOCK_SALE_SALE_PRICE, SIZ_DOCK_SALE_SALE_PRICE);
   if (lPrice > 100)
   {
      iTmp = sprintf(acTmp, "%*u", SALE_SIZ_SALEPRICE, lPrice);
      memcpy(pStdSale->SalePrice, acTmp, iTmp);

      // DocType
      pStdSale->DocType[0] = '1';         // GD

      // Sale code
      char cSaleType = *(pSale+OFF_DOCK_SALE_SALE_TYPE);
      if (cSaleType > ' ')
      {
         switch (cSaleType)
         {
            case 'A':
               pStdSale->SaleCode[0] = 'W';
               break;
            case 'T':
               pStdSale->SaleCode[0] = 'F';
               break;
            case 'P':
            case 'U':
               pStdSale->SaleCode[0] = 'P';
               break;
            default:
               LogMsg("*** Unknown Dock Sale code: %c", cSaleType);
         }
      }
   }

   // 0108303020000
#ifdef _DEBUG
   //if (!memcmp(pSale, "0108303020000", 12))
   //   iTmp = 1;
#endif

   // Update current sale
   if (iLen > OFF_DOCK_SALE_DOCTYPE)
   {
      memcpy(pStdSale->DocNum, pSale+OFF_DOCK_SALE_DOCNUM, 12);

      // DocType
      iTmp = findSortedDocType(pSale+OFF_DOCK_SALE_DOCTYPE, (IDX_TBL5 *)&SBD_DocType[0]);
      if (iTmp >= 0)
         memcpy(pStdSale->DocType, SBD_DocType[iTmp].pCode, SBD_DocType[iTmp].iCodeLen);
      else
         LogMsg("*** Unknown DocType: %.4s", pSale+OFF_DOCK_SALE_DOCTYPE);

      memcpy(pStdSale->DocCode, pSale+OFF_DOCK_SALE_DOCTYPE+1, 3);

      // Seller
      *(pSale+iLen-1) = 0;
      *(pSale+OFF_DOCK_SALE_SELLER+SALE_SIZ_SELLER) = 0;
      vmemcpy(pStdSale->Seller1, pSale+OFF_DOCK_SALE_SELLER, SALE_SIZ_SELLER);
   }

   pStdSale->Spc_Flg = SALE_TYPE_DOCK;
   pStdSale->CRLF[0] = '\n';
   pStdSale->CRLF[1] = 0;
   iTmp = fputs(pFmtSale, fdOut);

   return 0;
}

int Sbd_FmtOthSale(FILE *fdOut, char *pFmtSale, char *pSale)
{
   SCSAL_REC *pStdSale = (SCSAL_REC *)pFmtSale;
   int		iTmp, lCurSaleDt, lPrice, iLen;
   char     acDocDate[32], acTmp[32], *pTmp;

   memset(pFmtSale, ' ', sizeof(SCSAL_REC));

   memcpy(pStdSale->Apn, pSale+OFF_OTH_SALE_PARCEL_NUMBER, SIZ_OTH_SALE_PARCEL_NUMBER);
   pTmp = dateConversion(pSale+OFF_OTH_SALE_SALE_DATE, acDocDate, MM_DD_YYYY_1);
   if (pTmp)
      lCurSaleDt = atoin(pTmp, SIZ_SALE1_DT);
   else
      return -1;

   iLen = strlen(pSale);

   // Sale date
   memcpy(pStdSale->DocDate, acDocDate, SALE_SIZ_DOCDATE);
   iTmp = atol(acDocDate);
   if (iTmp > lLastRecDate && iTmp < lToday)
      lLastRecDate = iTmp;

   // Sale Price
   lPrice = atoin(pSale+OFF_OTH_SALE_SALE_PRICE, SIZ_OTH_SALE_SALE_PRICE);
   if (lPrice > 100)
   {
      iTmp = sprintf(acTmp, "%*u", SALE_SIZ_SALEPRICE, lPrice);
      memcpy(pStdSale->SalePrice, acTmp, iTmp);

      // DocType
      pStdSale->DocType[0] = '1';         // GD

      // Sale code
      char cSaleType = *(pSale+OFF_OTH_SALE_SALE_TYPE);
      if (cSaleType > ' ')
      {
         switch (cSaleType)
         {
            case 'A':
               pStdSale->SaleCode[0] = 'W';
               break;
            case 'T':
               pStdSale->SaleCode[0] = 'F';
               break;
            case 'P':
            case 'U':
               pStdSale->SaleCode[0] = 'P';
               break;
            default:
               LogMsg("*** Unknown Oth Sale code: %c", cSaleType);
         }
      }
   }

   // 0108303020000
#ifdef _DEBUG
   //if (!memcmp(pSale, "0108303020000", 12))
   //   iTmp = 1;
#endif

   // Update current sale
   if (iLen > OFF_OTH_SALE_DOCTYPE)
   {
      memcpy(pStdSale->DocNum, pSale+OFF_OTH_SALE_DOCNUM, 12);

      // DocType
      iTmp = findSortedDocType(pSale+OFF_OTH_SALE_DOCTYPE, (IDX_TBL5 *)&SBD_DocType[0]);
      if (iTmp >= 0)
         memcpy(pStdSale->DocType, SBD_DocType[iTmp].pCode, SBD_DocType[iTmp].iCodeLen);
      else
         LogMsg("*** Unknown DocType: %.4s - %.13s", pSale+OFF_OTH_SALE_DOCTYPE, pSale);

      memcpy(pStdSale->DocCode, pSale+OFF_OTH_SALE_DOCTYPE+1, 3);

      // Seller
      *(pSale+iLen-1) = 0;
      *(pSale+OFF_OTH_SALE_SELLER+SALE_SIZ_SELLER) = 0;
      vmemcpy(pStdSale->Seller1, pSale+OFF_OTH_SALE_SELLER, SALE_SIZ_SELLER);
   }

   pStdSale->Spc_Flg = SALE_TYPE_OTH;
   pStdSale->CRLF[0] = '\n';
   pStdSale->CRLF[1] = 0;
   iTmp = fputs(pFmtSale, fdOut);

   // Extract prev transaction if DocNum avail
   if (iLen > OFF_OTH_SALE_PRIOR_DOCTYPE)
   {
      pTmp = dateConversion(pSale+OFF_OTH_SALE_PRIOR_SALE_DATE, acDocDate, MM_DD_YYYY_1);
      if (pTmp && !memcmp(acDocDate, pSale+OFF_OTH_SALE_PRIOR_DOCNUM, 4))
      {
         memcpy(pStdSale->DocDate, acDocDate, SALE_SIZ_DOCDATE);
         memcpy(pStdSale->DocNum, pSale+OFF_OTH_SALE_PRIOR_DOCNUM, 12);

         // Sale Price
         lPrice = atoin(pSale+OFF_OTH_SALE_PRIOR_SALE_PRICE, SIZ_OTH_SALE_SALE_PRICE);
         if (lPrice > 100)
         {
            iTmp = sprintf(acTmp, "%*u", SALE_SIZ_SALEPRICE, lPrice);
            memcpy(pStdSale->SalePrice, acTmp, iTmp);

            // DocType
            pStdSale->DocType[0] = '1';         // GD
         } else
         {
            memset(pStdSale->SalePrice, ' ', SALE_SIZ_SALEPRICE);
            memset(pStdSale->DocType, ' ', 3);
         }
         pStdSale->SaleCode[0] = ' ';

         // DocType
         iTmp = findDocType(pSale+OFF_OTH_SALE_PRIOR_DOCTYPE, (IDX_TBL5 *)&SBD_DocType[0]);
         if (iTmp >= 0)
            memcpy(pStdSale->DocType, SBD_DocType[iTmp].pCode, SBD_DocType[iTmp].iCodeLen);
         else
         {
            LogMsg("*** Unknown DocType: %.4s - %.13s", pSale+OFF_OTH_SALE_PRIOR_DOCTYPE, pSale);
         }
         memcpy(pStdSale->DocCode, pSale+OFF_OTH_SALE_PRIOR_DOCTYPE+1, 3);

         // Seller
         *(pSale+OFF_OTH_SALE_PRIOR_SELLER+SALE_SIZ_SELLER) = 0;
         memset(pStdSale->Seller1, ' ', SALE_SIZ_SELLER);
         vmemcpy(pStdSale->Seller1, pSale+OFF_OTH_SALE_PRIOR_SELLER, SALE_SIZ_SELLER);
         iTmp = fputs(pFmtSale, fdOut);
      } else
         LogMsg("*** Questionable transaction: %.12s %s %.12s - ignore", pSale, acDocDate, pSale+OFF_OTH_SALE_PRIOR_DOCNUM);
   }

   return 0;
}

/****************************** Sbd_ExtrSale *********************************
 *
 * Extract history sales. 
 *
 * Return 0 if successful, otherwise error
 *
 *****************************************************************************/

int Sbd_ExtrSale(int iType, char *pOutfile=NULL)
{
   char     *pTmp, acStdSaleRec[1024], acSaleRec[1024], acTmpSale[_MAX_PATH], acSaleType[16];
   int		iRet, iUpdateSale=0, lCnt=0;
   FILE     *fdOut;
   
   acSaleType[0] = 0;
   switch (iType)
   {
      case SALE_TYPE_SFR:
         strcpy(acSaleType, "SfrSale");
         break;
      case SALE_TYPE_MP:
         strcpy(acSaleType, "MpSale");
         break;
      case SALE_TYPE_MH:
         strcpy(acSaleType, "MhSale");
         break;
      case SALE_TYPE_LAND:
         strcpy(acSaleType, "LandSale");
         break;
      case SALE_TYPE_DOCK:
         strcpy(acSaleType, "DockSale");
         break;
      case SALE_TYPE_OTH:
         strcpy(acSaleType, "OthSale");
         break;
   }

   GetIniString(myCounty.acCntyCode, acSaleType, "", acSaleFile, _MAX_PATH, acIniFile);    
   LogMsg("Extract %s file", acSaleType);

   // Check current sale file
   if (_access(acSaleFile, 0))
   {
      LogMsg("***** Missing current sale file %s", acSaleFile);
      return -1;
   }

   // Open input sale file
   LogMsg("Open %s file %s", acSaleType, acSaleFile);
   fdSale = fopen(acSaleFile, "r");
   if (fdSale == NULL)
   {
      LogMsg("***** Error opening %s sale file: %s\n", acSaleType, acSaleFile);
      return -2;
   }
   
   if (pOutfile)
      strcpy(acTmpSale, pOutfile);
   else
      sprintf(acTmpSale, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, acSaleType);

   // Create output file
   LogMsg("Create output sale file %s", acTmpSale);
   fdOut = fopen(acTmpSale, "a+");
   if (fdOut == NULL)
   {
      LogMsg("***** Error opening sale file: %s\n", acTmpSale);
      return -3;
   }

   // Init global
   lLastRecDate = 0;

   // Merge loop
   while (!feof(fdSale))
   {
#ifdef _DEBUG
      //if (!memcmp(acSaleRec, "0417223160000", 12))
      //   iRet = 1;
#endif

      // Get current sale
      pTmp = fgets(acSaleRec, 1024, fdSale);
      if (!pTmp)
         break;
      
      switch (iType)
      {
         case SALE_TYPE_SFR:
            iRet = Sbd_FmtSfrSale(fdOut, acStdSaleRec, acSaleRec);
            break;
         //case SALE_TYPE_MP:
         //   iRet = Sbd_FmtMpSale(fdOut, acStdSaleRec, acSaleRec);
         //   break;
         case SALE_TYPE_MH:
            iRet = Sbd_FmtMhSale(fdOut, acStdSaleRec, acSaleRec);
            break;
         case SALE_TYPE_LAND:
            iRet = Sbd_FmtLandSale(fdOut, acStdSaleRec, acSaleRec);
            break;
         case SALE_TYPE_DOCK:
            iRet = Sbd_FmtDockSale(fdOut, acStdSaleRec, acSaleRec);
            break;
         case SALE_TYPE_OTH:
            iRet = Sbd_FmtOthSale(fdOut, acStdSaleRec, acSaleRec);
            break;
      }

      if (!iRet)
         iUpdateSale++;

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);

   }

   // Close files
   if (fdOut)
      fclose(fdOut);
   if (fdSale)
      fclose(fdSale);

   LogMsg("Total sale records from %s: %u", acSaleType, lCnt);
   LogMsg("Lastest recording date: %d\n", lLastRecDate);

   return 0;
}

/****************************** Sbd_ExtrMPSale *******************************
 *
 * Extract history MP sales. In MPSale, only first record has sale info. 
 * For any subsequent record with the sale lead APN and sale date, we copy prev rec info.
 *
 * Return 0 if successful, otherwise error
 *
 *****************************************************************************/

int Sbd_FmtMpSale(FILE *fdOut, char *pFmtSale, char *pSale)
{
   SCSAL_REC *pStdSale = (SCSAL_REC *)pFmtSale;
   int		iTmp, lCurSaleDt, lPrice, iLen;
   char     acDocDate[32], acTmp[32], *pTmp;

   pTmp = dateConversion(pSale+OFF_MP_SALE_SALE_DATE, acDocDate, MM_DD_YYYY_1);
   if (pTmp)
      lCurSaleDt = atoin(pTmp, SIZ_SALE1_DT);
   else
      lCurSaleDt = 0;

   iLen = strlen(pSale);

   // Reset for new transaction
   if (memcmp(pStdSale->PrimaryApn, pSale, SIZ_MP_SALE_PARCEL_NUMBER) || (lCurSaleDt > 0 && memcmp(pStdSale->DocDate, acDocDate, SIZ_SALE1_DT)))
   {
      memset(pFmtSale, ' ', sizeof(SCSAL_REC));
      // Primary APN
      memcpy(pStdSale->PrimaryApn, pSale, SIZ_MP_SALE_PARCEL_NUMBER);

      // Multi sale
      pStdSale->MultiSale_Flg = 'Y';

      // Sale date
      memcpy(pStdSale->DocDate, acDocDate, SALE_SIZ_DOCDATE);
      iTmp = atol(acDocDate);
      if (iTmp > lLastRecDate && iTmp < lToday)
         lLastRecDate = iTmp;

      // Sale Price
      lPrice = atoin(pSale+OFF_MP_SALE_SALE_PRICE, SIZ_MP_SALE_SALE_PRICE);
      if (lPrice > 100)
      {
         iTmp = sprintf(acTmp, "%*u", SALE_SIZ_SALEPRICE, lPrice);
         memcpy(pStdSale->SalePrice, acTmp, iTmp);

         // DocType
         pStdSale->DocType[0] = '1';         // GD

         // Sale code
         pStdSale->SaleCode[0] = 'P';
         //char cSaleType = *(pSale+OFF_MP_SALE_SALE_TYPE);
         //if (cSaleType > ' ')
         //{
         //   switch (cSaleType)
         //   {
         //      case 'A':
         //         pStdSale->SaleCode[0] = 'I';
         //         break;
         //      case 'T':
         //         pStdSale->SaleCode[0] = 'F';
         //         break;
         //      case 'P':
         //         pStdSale->SaleCode[0] = 'P';
         //         break;
         //      default:
         //         pStdSale->SaleCode[0] = cSaleType;
         //   }
         //}
      }

#ifdef _DEBUG
      //if (!memcmp(pSale, "0108303020000", 12))
      //   iTmp = 1;
#endif

      // Update current sale
      if (iLen > OFF_MP_SALE_DOCTYPE)
      {
         memcpy(pStdSale->DocNum, pSale+OFF_MP_SALE_DOCNUM, 12);

         // DocType
         iTmp = findSortedDocType(pSale+OFF_MP_SALE_DOCTYPE, (IDX_TBL5 *)&SBD_DocType[0]);
         if (iTmp >= 0)
            memcpy(pStdSale->DocType, SBD_DocType[iTmp].pCode, SBD_DocType[iTmp].iCodeLen);
         else
            LogMsg("*** Unknown DocType: %.4s - %.13s", pSale+OFF_MP_SALE_DOCTYPE, pSale);

         memcpy(pStdSale->DocCode, pSale+OFF_MP_SALE_DOCTYPE+1, 3);

         // Seller
         if (iLen > OFF_MP_SALE_SELLER)
         {
            *(pSale+iLen-1) = 0;
            vmemcpy(pStdSale->Seller1, pSale+OFF_MP_SALE_SELLER, SALE_SIZ_SELLER);
         }
      }

      pStdSale->Spc_Flg = SALE_TYPE_MP;
      pStdSale->CRLF[0] = '\n';
      pStdSale->CRLF[1] = 0;
   }

   // APN
   memcpy(pStdSale->Apn, pSale+OFF_MP_SALE_PARCEL_NUMBER, SIZ_MP_SALE_PARCEL_NUMBER);
   if (pStdSale->DocDate[0] > ' ')
      iTmp = fputs(pFmtSale, fdOut);

   // Extract prev transaction if DocNum avail
   //if (iLen > OFF_MP_SALE_PRIOR_DOCTYPE)
   //{
   //   pTmp = dateConversion(pSale+OFF_MP_SALE_PRIOR_SALE_DATE, acDocDate, MM_DD_YYYY_1);
   //   if (pTmp && !memcmp(acDocDate, pSale+OFF_MP_SALE_PRIOR_DOCNUM, 4))
   //   {
   //      memcpy(pStdSale->DocDate, acDocDate, SALE_SIZ_DOCDATE);
   //      memcpy(pStdSale->DocNum, pSale+OFF_MP_SALE_PRIOR_DOCNUM, 12);

   //      // Sale Price
   //      lPrice = atoin(pSale+OFF_MP_SALE_PRIOR_SALE_PRICE, SIZ_MP_SALE_SALE_PRICE);
   //      if (lPrice > 100)
   //      {
   //         iTmp = sprintf(acTmp, "%*u", SALE_SIZ_SALEPRICE, lPrice);
   //         memcpy(pStdSale->SalePrice, acTmp, iTmp);

   //         // DocType
   //         pStdSale->DocType[0] = '1';         // GD
   //      } else
   //      {
   //         memset(pStdSale->SalePrice, ' ', SALE_SIZ_SALEPRICE);
   //         memset(pStdSale->DocType, ' ', 3);
   //      }
   //      pStdSale->SaleCode[0] = ' ';

   //      // DocType
   //      iTmp = findDocType(pSale+OFF_MP_SALE_PRIOR_DOCTYPE, (IDX_TBL5 *)&SBD_DocType[0]);
   //      if (iTmp >= 0)
   //         memcpy(pStdSale->DocType, SBD_DocType[iTmp].pCode, SBD_DocType[iTmp].iCodeLen);
   //      else
   //      {
   //         LogMsg("*** Unknown DocType: %.4s", pSale+OFF_MP_SALE_PRIOR_DOCTYPE);
   //      }
   //      memcpy(pStdSale->DocCode, pSale+OFF_MP_SALE_PRIOR_DOCTYPE+1, 3);

   //      // Seller
   //      *(pSale+OFF_MP_SALE_PRIOR_SELLER+SALE_SIZ_SELLER) = 0;
   //      memset(pStdSale->Seller1, ' ', SALE_SIZ_SELLER);
   //      vmemcpy(pStdSale->Seller1, pSale+OFF_MP_SALE_PRIOR_SELLER, SALE_SIZ_SELLER);
   //      iTmp = fputs(pFmtSale, fdOut);
   //   } else
   //      LogMsg("*** Questionable transaction: %.12s %s %.12s - ignore", pSale, acDocDate, pSale+OFF_MP_SALE_PRIOR_DOCNUM);
   //}

   return 0;
}

int Sbd_ExtrMPSale(char *pOutfile=NULL)
{
   char     *pTmp, acStdSaleRec[1024], acSaleRec[1024], acTmpSale[_MAX_PATH], acSaleType[16];
   int		iRet, iUpdateSale=0, lCnt=0;
   FILE     *fdOut;
   
   LogMsg("Extract MPSale file %s", acSaleFile);
   strcpy(acSaleType, "MpSale");
   GetIniString(myCounty.acCntyCode, acSaleType, "", acSaleFile, _MAX_PATH, acIniFile);    

   // Check current sale file
   if (_access(acSaleFile, 0))
   {
      LogMsg("***** Missing %s sale file %s", acSaleType, acSaleFile);
      return -1;
   }

   // Open input sale file
   LogMsg("Open %s file %s", acSaleType, acSaleFile);
   fdSale = fopen(acSaleFile, "r");
   if (fdSale == NULL)
   {
      LogMsg("***** Error opening %s sale file: %s\n", acSaleType, acSaleFile);
      return -2;
   }
   
   if (pOutfile)
      strcpy(acTmpSale, pOutfile);
   else
      sprintf(acTmpSale, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, acSaleType);

   // Create output file
   LogMsg("Create output sale file %s", acTmpSale);
   fdOut = fopen(acTmpSale, "a+");
   if (fdOut == NULL)
   {
      LogMsg("***** Error opening sale file: %s\n", acTmpSale);
      return -3;
   }

   // Init global
   lLastRecDate = 0;

   // Merge loop
   while (!feof(fdSale))
   {
#ifdef _DEBUG
      //if (!memcmp(acSaleRec, "15837109", 8))
      //   iRet = 1;
#endif

      // Get current sale
      pTmp = fgets(acSaleRec, 1024, fdSale);
      if (!pTmp)
         break;
      
      iRet = Sbd_FmtMpSale(fdOut, acStdSaleRec, acSaleRec);
      if (!iRet)
         iUpdateSale++;

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);

   }

   // Close files
   if (fdOut)
      fclose(fdOut);
   if (fdSale)
      fclose(fdSale);

   LogMsg("Total sale records from %s: %u", acSaleType, lCnt);
   LogMsg("Lastest recording date: %d\n", lLastRecDate);

   return 0;
}

/******************************* Sbd_MergeXferOwner ***************************
 *
 * Merge Owners from OWNER.TRANSFER file.  If current owner2 is empty and update it if posible.
 * Update Owner2, Etal flag, Transfer Date & Doc if needed
 *
 * Notes: 8/18/2016
 *        This function is written but probably never used.  The OWNER.TRANSFER file is annual
 *        while the secured roll is daily.  We have to rely on the daily file for up to date info.
 *
 ******************************************************************************/

int Sbd_MergeOwnerRec(char *pOutbuf)
{
   static   char acRec[512], *pRec=NULL;
   int      iLoop, iTmp, iCnt;
   OWNEREX  *pOwner = (OWNEREX *)acRec;

   // Get first Char rec for first call
   if (!pRec)
      pRec = fgets(acRec, 512, fdOwner);

   do
   {
      if (!pRec)
      {
         fclose(fdOwner);
         fdOwner = NULL;
         return 1;      // EOF
      }

      // Compare APN
      iLoop = memcmp(pOutbuf, pRec, iApnLen);
      if (iLoop > 0)
      {
         if (bDebug)
            LogMsg0("Skip Owner rec  %.*s", iApnLen, pOwner->Apn);
         pRec = fgets(acRec, 512, fdOwner);
         lCharSkip++;
      }
   } while (iLoop > 0);

   // If not match, return
   if (iLoop)
      return 1;

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "010832425000", 9))
   //   iLoop = 0;
#endif

   // Update Name2
   if (*(pOutbuf+OFF_NAME2) == ' ')
   {
      iCnt = atoi(pOwner->NameCnt);
      if (iCnt > 4) iCnt = 4; 

      for (iTmp = 0; iTmp < iCnt; iTmp++)
         if (memcmp(pOutbuf+OFF_NAME1, pOwner->Owners[iTmp], SIZ_NAME2))
            break;
      if (iTmp < iCnt && *pOwner->Owners[iTmp] != ' ')
         memcpy(pOutbuf+OFF_NAME2, pOwner->Owners[iTmp], SIZ_NAME2);
   }

   // Update Etal flag
   *(pOutbuf+OFF_ETAL_FLG) = pOwner->Etal;

   // Update Transfer
   if (memcmp(pOutbuf+OFF_TRANSFER_DT, pOwner->XferDate, SIZ_TRANSFER_DT) < 0)
   {
      memcpy(pOutbuf+OFF_TRANSFER_DT, pOwner->XferDate, SIZ_TRANSFER_DT);
      memcpy(pOutbuf+OFF_TRANSFER_DOC, pOwner->XferDoc, SIZ_TRANSFER_DOC);
   }

   lCharMatch++;

   // Get next Char rec
   pRec = fgets(acRec, 512, fdOwner);

   return 0;
}

int Sbd_MergeXferOwner(int iSkip)
{
   char     acBuf[MAX_RECSIZE], acRawFile[_MAX_PATH], acOutFile[_MAX_PATH], acTmp[256];
   int      iRet, iRollUpd=0, lCnt=0;
   DWORD    nBytesRead, nBytesWritten;
   BOOL     bRet, bUseR01=true;
   HANDLE   fhIn, fhOut;

   sprintf(acRawFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "R01");
   sprintf(acOutFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "X01");

   // Rename files for processing
   if (_access(acRawFile, 0))
   {
      sprintf(acRawFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "S01");
      if (!_access(acRawFile, 0))
      {
         LogMsg("Use %s as input for MergeChar", acRawFile);
         bUseR01 = false;
      } else
      {
         LogMsg("***** Error in MergeChar(): Missing input file %s.  Please recheck!", acRawFile);
         return -1;
      }
   }

   // Open owner file
   sprintf(acTmp, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "Owner");
   LogMsg("Open Owner file %s", acTmp);
   fdOwner = fopen(acTmp, "r");
   if (fdOwner == NULL)
   {
      LogMsg("***** Error opening acreage file: %s\n", acTmp);
      return -2;
   }

   // Open Input file
   LogMsg("Open input file %s", acRawFile);
   fhIn = CreateFile(acRawFile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhIn == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening input file: %s\n", acRawFile);
      return -3;
   }

   // Open Output file
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening output file: %s\n", acOutFile);
      return -4;
   }

   // Copy skip record
   memset(acBuf, ' ', iRecLen);
   while (iSkip-- > 0)
   {
      ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);
      // Ignore record start with 99999999
      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
   }

   // Merge loop
   do
   {
      bRet = ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);

      // Check for EOF
      if (!bRet)
      {
         LogMsg("***** Error reading input file %s (%f)", acRawFile, GetLastError());
         break;
      }

      // EOF ?
      if (!nBytesRead)
         break;

      // Update acreage info
      if (fdOwner)
      {
         iRet = Sbd_MergeOwnerRec(acBuf);
         if (!iRet)
            iRollUpd++;
      }

      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);

      if (!bRet)
      {
         LogMsg("***** Error writing to out file %s: %d\n", acOutFile, GetLastError());
         break;
      }
   } while (bRet);

   // Close files
   if (fdOwner)
      fclose(fdOwner);
   if (fhOut)
      CloseHandle(fhOut);
   if (fhIn)
      CloseHandle(fhIn);

   // Rename R01 to SAV if R01 is used as input
   if (bUseR01)
   {
      sprintf(acTmp, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "SAV");
      if (!_access(acTmp, 0))
      {
         LogMsg("Remove saved file %s", acTmp);
         if (iRet=remove(acTmp))
            LogMsg("***** Error removing %s to %s (%d)", acTmp, errno);
      }

      LogMsg("Rename %s to %s", acRawFile, acTmp);
      if (iRet=rename(acRawFile, acTmp))
         LogMsg("***** Error renaming %s to %s (%d)", acRawFile, acTmp, errno);
   } else
      sprintf(acRawFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "R01");

   LogMsg("Rename %s to %s", acOutFile, acRawFile);
   if (iRet=rename(acOutFile, acRawFile))
      LogMsg("***** Error renaming %s to %s (%d)", acOutFile, acRawFile, errno);

   LogMsg("Total output records:       %u", lCnt);
   LogMsg("Total records updated:      %u", iRollUpd);

   lRecCnt = lCnt;
   return iRet;
}

/***************************** Sbd_ParseTR340 *******************************
 *
 * Parse TR340 record to create TaxBase output
 *
 * Return 0 if success
 *
 ****************************************************************************/

int Sbd_ParseTR340(char *pOutbuf, char *pInbuf)
{
   int      iTmp;
   long     taxYear;
   double	dTax1, dTax2, dTaxTotal, dDue1, dDue2;

   TAXBASE     *pOutRec = (TAXBASE *)pOutbuf;
   SBD_TR340   *pInRec  = (SBD_TR340 *)pInbuf;

   // Clear output buffer
   memset(pOutbuf, 0, sizeof(TAXBASE));

   // APN
   memcpy(pOutRec->Apn, pInRec->Apn, TSIZ_APN);

   // Tax Year
   sprintf(pOutRec->TaxYear, "20%.2s", pInRec->RollYear);
   taxYear = atol(pOutRec->TaxYear);

   // Defaulted year

   // Bill Number - Since we have bill number in detail record, it's better not stored it.
   //memcpy(pOutRec->BillNum, pInRec->BillNum, TSIZ_BILLNUM);

   // TRA - ignore first char in TRA
   memcpy(pOutRec->TRA, &pInRec->TRA[1], 6);

#ifdef _DEBUG
   //if (!memcmp(pOutRec->Apn, "060097011", 9))
   //   iTmp = 0;
#endif

   // Check for Tax amount
   dTax1 = atofn(pInRec->TaxAmt1, TSIZ_INSTAMT);
   dTax2 = atofn(pInRec->TaxAmt2, TSIZ_INSTAMT);
   dDue1=dDue2=0.0;
   iTmp = atoin(pInRec->PaidStatus, TSIZ_PAIDSTATUS);
   if (!iTmp)
   {  // Nothing paid
      if (ChkDueDate(1, taxYear))
      {
         dDue1 = dTax1;
         dDue2 = dTax2;
         pOutRec->Inst1Status[0] = TAX_BSTAT_PASTDUE;
      } else
         pOutRec->Inst1Status[0] = TAX_BSTAT_UNPAID;
   } else if (iTmp == 1)
   {  // Inst1 paid
      pOutRec->Inst1Status[0] = TAX_BSTAT_PAID;
      sprintf(pOutRec->PaidAmt1, "%.2f", dTax1);
      if (ChkDueDate(2, taxYear))
      {
         dDue2 = dTax2;
         pOutRec->Inst2Status[0] = TAX_BSTAT_PASTDUE;
      } else
         pOutRec->Inst2Status[0] = TAX_BSTAT_UNPAID;
   } else if (iTmp == 2)
   {  // Both paid
      pOutRec->Inst1Status[0] = TAX_BSTAT_PAID;
      pOutRec->Inst2Status[0] = TAX_BSTAT_PAID;
      sprintf(pOutRec->PaidAmt1, "%.2f", dTax1);
      sprintf(pOutRec->PaidAmt2, "%.2f", dTax2);
   } else
   {
      LogMsg("??? Questionable paid status on APN= %s:%c", pOutRec->Apn, pInRec->PaidStatus);
   }

   // Total tax
   dTaxTotal = dTax1 + dTax2;

#ifdef _DEBUG
   //if ((int)dTaxTotal != (int)(dTax1+dTax2))
   //   iRet = 0;
#endif
   if (dTaxTotal > 0.0)
   {
      sprintf(pOutRec->TaxAmt1, "%.2f", dTax1);
      sprintf(pOutRec->TaxAmt2, "%.2f", dTax2);
      sprintf(pOutRec->TotalTaxAmt, "%.2f", dTaxTotal);
   }

   // Total due
   dTaxTotal = dDue1+dDue2;
   if (dTaxTotal > 0.0)
      sprintf(pOutRec->TotalDue, "%.2f", dTaxTotal);

   pOutRec->BillType[0] = BILLTYPE_SECURED;
   pOutRec->isSecd[0] = '1';
   pOutRec->isSupp[0] = '0';

   return 0;
}

/***************************** Sbd_Load_TR340 ********************************
 *
 * Create import file from tax roll tr340pc.txt and import into SQL 
 * Do not output cancelled bill.
 *
 * Return 0 if success.
 *
 *****************************************************************************/

int Sbd_Load_TR340(bool bImport)
{
   char     *pTmp, acBase[1024], acRec[512],
            acBaseFile[_MAX_PATH], acInFile[_MAX_PATH], acOutbuf[MAX_RECSIZE];
            
   int      iRet, iDrop=0;
   long     lBase=0, lCnt=0;
   FILE     *fdBase, *fdIn;
   TAXBASE  *pBase = (TAXBASE *)acBase;

   LogMsg0("Loading Tax for SBD");
   NameTaxCsvFile(acBaseFile, myCounty.acCntyCode, "Base");

   // Check file date - do not need to check DIST.TC file since it contains no payment info
   GetIniString(myCounty.acCntyCode, "TaxRollInfo", "", acInFile, _MAX_PATH, acIniFile);
   lLastTaxFileDate = getFileDate(acInFile);
   // Only process if new tax file
   iRet = isNewTaxFile(NULL, myCounty.acCntyCode);
   if (iRet <= 0)
   {
      lLastTaxFileDate = 0;
      return iRet;
   }

   // Open input file
   LogMsg("Open Secured tax file %s", acInFile);
   fdIn = fopen(acInFile, "r");
   if (fdIn == NULL)
   {
      LogMsg("***** Error opening Secured tax file: %s\n", acInFile);
      return -2;
   }  

   // Open Output file
   LogMsg("Create Base file %s", acBaseFile);
   fdBase = fopen(acBaseFile, "w");
   if (fdBase == NULL)
   {
      LogMsg("***** Error creating output file: %s\n", acBaseFile);
      return -4;
   }

   // Merge loop 
   while (!feof(fdIn))
   {
      pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdIn);
      if (!pTmp)
         break;

      // Create new R01 record
      iRet = Sbd_ParseTR340(acBase, acRec);
      if (!iRet)
      {
         // Create TaxBase record
         Tax_CreateTaxBaseCsv(acOutbuf, (TAXBASE *)&acBase);

         // Output record			
         lBase++;
         fputs(acOutbuf, fdBase);
      } else
      {
         LogMsg("---> Drop record %d [%.40s]", lCnt, acRec); 
         iDrop++;
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdIn)
      fclose(fdIn);
   if (fdBase)
      fclose(fdBase);

   printf("\nTotal records processed:     %u\n", lCnt);
   LogMsg("Total records processed:   %u", lCnt);
   LogMsg("    base records output:   %u", lBase);

   if (iDrop > 5)
   {
      LogMsg("Total records dropped: %u", iDrop);
      return -5;
   }

   // Import into SQL
   if (bImport)
   {
      iRet = doTaxImport(myCounty.acCntyCode, TAX_BASE);
      if (!iRet)
         iRet = doTaxPrep(myCounty.acCntyCode, TAX_DELINQUENT);
   } else
      iRet = 0;

   return iRet;
}

/***************************** Sbd_ParseTaxDetail ****************************
 *
 * Create detail and angency records
 *
 * Return 0 if there is tax amt, otherwise return 1
 *
 *****************************************************************************/

//int Sbd_ParseTaxDetail(char *pDetailBuf, char *pAgencyBuf, char *pInbuf)
//{
//   SBD_PI344  *pInRec   = (SBD_PI344 *)pInbuf;
//   TAXDETAIL  *pDetail  = (TAXDETAIL *)pDetailBuf;
//   TAXAGENCY  *pResult, *pAgency = (TAXAGENCY *)pAgencyBuf;
//   double      dTax;
//   int         iRet;
//
//   // Clear output buffer
//   memset(pDetailBuf,  0, sizeof(TAXDETAIL));
//   memset(pAgencyBuf, 0, sizeof(TAXAGENCY));
//
//   // APN
//   memcpy(pDetail->Apn, pInRec->Apn, TSIZ_APN);
//
//   // Tax Year
//   memcpy(pDetail->TaxYear, pInRec->RollYear, 4);
//
//   // Tax Code
//   memcpy(pDetail->TaxCode, pInRec->TaxCode, TSIZ_TAXCODE);
//   memcpy(pAgency->Code,    pInRec->TaxCode, TSIZ_TAXCODE);
//
//   // Tax Desc
//   pResult = findTaxAgency(pDetail->TaxCode);
//   if (pResult)
//   {
//      strcpy(pAgency->Agency,pResult->Agency);
//      strcpy(pAgency->Phone,pResult->Phone);
//      pAgency->TC_Flag[0] = pResult->TC_Flag[0];
//   } else
//   {
//      LogMsg0("*** Unknown Agency: %s [%s]", pDetail->TaxCode, pDetail->Apn);
//      sprintf(pAgency->Agency, "%s - SPECIAL ASSESSMENT", pDetail->TaxCode);
//   }
//
//   // Tax Amt
//   dTax  = atofn(pInRec->AsmntAmt, TSIZ_ASMNT_AMT)/100.0;
//   dTax += atofn(pInRec->ProcessCost, TSIZ_COST)/100.0;
//   if (dTax > 0.0)
//   {
//      sprintf(pDetail->TaxAmt, "%.2f", dTax);
//      iRet = 0;
//   } else
//      iRet = 1;
//
//   return iRet;
//}

/***************************** Sbd_ParseTaxDetail ****************************
 *
 * Create detail and angency records using input from AsmtExtr file
 *
 * Return 0 if there is tax amt, otherwise return 1
 *
 *****************************************************************************/

int Sbd_ParseTaxDetailEx(char *pDetailBuf, char *pAgencyBuf, char *pInbuf)
{
   TAXDETAIL  *pDetail  = (TAXDETAIL *)pDetailBuf;
   TAXAGENCY  *pResult, *pAgency = (TAXAGENCY *)pAgencyBuf;
   double      dTax;
   int         iRet;

   // Clear output buffer
   memset(pDetailBuf, 0, sizeof(TAXDETAIL));
   memset(pAgencyBuf, 0, sizeof(TAXAGENCY));

   iTokens = ParseStringIQ(pInbuf, '|', TI_COLS, apTokens);
   if (iTokens < TI_COLS)
   {
      LogMsg("***** Bad entry: %s", pInbuf);
      return -1;
   }

   // APN
   strcpy(pDetail->Apn, apTokens[TI_APN]);

   // Tax Year
   sprintf(pDetail->TaxYear, "%d", lTaxYear);

   // Tax Code
   strcpy(pDetail->TaxCode, apTokens[TI_TAXCODE]);
   strcpy(pAgency->Code,    apTokens[TI_TAXCODE]);

   // Tax Desc
   pResult = findTaxAgency(pDetail->TaxCode);
   if (pResult)
   {
      strcpy(pAgency->Agency,pResult->Agency);
      strcpy(pAgency->Phone,pResult->Phone);
      pAgency->TC_Flag[0] = pResult->TC_Flag[0];
      pDetail->TC_Flag[0] = pResult->TC_Flag[0];
   } else
   {
      LogMsg0("*** Unknown Agency: %s [%s]", pDetail->TaxCode, pDetail->Apn);
      sprintf(pAgency->Agency, "%s - SPECIAL ASSESSMENT", pDetail->TaxCode);
   }

   // Tax Amt
   dTax  = atof(apTokens[TI_TAXAMT]);
   if (dTax > 0.0)
   {
      sprintf(pDetail->TaxAmt, "%.2f", dTax);
      iRet = 0;
   } else
      iRet = 1;

   return iRet;
}

/**************************** Sbd_Load_TaxDetail ******************************
 *
 * Create detail import file and import into SQL if specified
 *
 * Return 0 if success.
 *
 *****************************************************************************/

//int Sbd_Load_TaxDetail(bool bImport)
//{
//   char     *pTmp, acItemsRec[MAX_RECSIZE], acAgencyRec[512], acRec[512];
//   char     acItemsFile[_MAX_PATH], acAgencyFile[_MAX_PATH], acTmpFile[_MAX_PATH], acInFile[_MAX_PATH];
//
//   int      iRet;
//   long     lItems=0, lCnt=0, lAgency=0;
//   FILE     *fdItems, *fdAgency, *fdIn;
//   TAXDETAIL *pTax = (TAXDETAIL *)&acItemsRec[0];
//   TAXAGENCY *pAgency = (TAXAGENCY *)&acAgencyRec[0];
//
//   LogMsg0("Loading Tax Detail file");
//
//   // Open input file
//   GetIniString(myCounty.acCntyCode, "TaxDetail", "", acInFile, _MAX_PATH, acIniFile);
//   LogMsg("Open Detail file %s", acInFile);
//   fdIn = fopen(acInFile, "r");
//   if (fdIn == NULL)
//   {
//      LogMsg("***** Error opening Detail file: %s\n", acInFile);
//      return -2;
//   }  
//
//   // Open Items file
//   NameTaxCsvFile(acItemsFile, myCounty.acCntyCode, "Items");
//   LogMsg("Open Items file %s for appending.", acItemsFile);
//   fdItems = fopen(acItemsFile, "a");
//   if (fdItems == NULL)
//   {
//      LogMsg("***** Error creating Items file: %s\n", acItemsFile);
//      return -4;
//   }
//
//   // Open Agency file
//   NameTaxCsvFile(acAgencyFile, myCounty.acCntyCode, "Agency");
//   sprintf(acTmpFile, "%s\\%s\\%s_Agency.tmp", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
//   LogMsg("Create Agency file %s", acTmpFile);
//   fdAgency = fopen(acTmpFile, "w");
//   if (fdAgency == NULL)
//   {
//      LogMsg("***** Error creating Agency file: %s\n", acTmpFile);
//      return -4;
//   }
//
//   // Init Agency table
//   memset(acAgencyRec, 0, sizeof(TAXAGENCY));
//   strcpy(pAgency->Code, "00000001");
//   strcpy(pAgency->Agency, "GENERAL TAX");
//   pAgency->TC_Flag[0] = '1';
//   Tax_CreateAgencyCsv(acRec, (TAXAGENCY *)&acAgencyRec);
//   fputs(acRec, fdAgency);
//   strcpy(pAgency->Code, "00000002");
//   strcpy(pAgency->Agency, "GENERAL SUPPLEMENTAL TAX");
//   Tax_CreateAgencyCsv(acRec, (TAXAGENCY *)&acAgencyRec);
//   fputs(acRec, fdAgency);
//   
//   // Merge loop 
//   while (!feof(fdIn))
//   {
//      pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdIn);
//      if (!pTmp)
//         break;
//
//      // Create Items & Agency record
//      iRet = Sbd_ParseTaxDetail(acItemsRec, acAgencyRec, acRec);
//      if (!iRet)
//      {
//         // Create TaxBase record
//         Tax_CreateDetailCsv(acRec, (TAXDETAIL *)&acItemsRec);
//         lItems++;
//         fputs(acRec, fdItems);
//
//         // Create Agency record
//         Tax_CreateAgencyCsv(acRec, (TAXAGENCY *)&acAgencyRec);
//         fputs(acRec, fdAgency);
//      } else
//      {
//         LogMsg0("---> Drop record %d [%.80s]", lCnt, acRec); 
//      }
//
//      if (!(++lCnt % 1000))
//         printf("\r%u", lCnt);
//   }
//
//   // Close files
//   if (fdIn)
//      fclose(fdIn);
//   if (fdItems)
//      fclose(fdItems);
//   if (fdAgency)
//      fclose(fdAgency);
//
//   LogMsg("Total records processed:    %u", lCnt);
//   LogMsg("Total Items records:        %u", lItems);
//
//   LogMsg("Dedup Agency file %s to %s", acTmpFile, acAgencyFile);
//   iRet = sortFile(acTmpFile, acAgencyFile, "S(#1,C,A) DUPOUT F(TXT)");
//
//   // Import into SQL
//   if (bImport)
//   {
//      iRet = doTaxImport(myCounty.acCntyCode, TAX_DETAIL);
//      if (!iRet)
//         iRet = doTaxImport(myCounty.acCntyCode, TAX_AGENCY);
//
//      // Update BillNum on Tax_Items using data from Tax_Base
//      if (!iRet)
//      {
//         iRet = update_TI_BillNum(myCounty.acCntyCode, "Ex");
//      }
//   } else
//      iRet = 0;
//
//   return iRet;
//}

int Sbd_Load_TaxDetailEx(char *pInFile, bool bImport)
{
   char     *pTmp, acItemsRec[MAX_RECSIZE], acAgencyRec[512], acRec[512];
   char     acItemsFile[_MAX_PATH], acAgencyFile[_MAX_PATH], acTmpFile[_MAX_PATH];

   int      iRet;
   long     lItems=0, lCnt=0, lAgency=0;
   FILE     *fdItems, *fdAgency, *fdIn;
   TAXDETAIL *pTax = (TAXDETAIL *)&acItemsRec[0];
   TAXAGENCY *pAgency = (TAXAGENCY *)&acAgencyRec[0];

   LogMsg0("Loading Tax Detail file Ex");

   // Open input file
   LogMsg("Open Detail file %s", pInFile);
   fdIn = fopen(pInFile, "r");
   if (fdIn == NULL)
   {
      LogMsg("***** Error opening Detail file: %s\n", pInFile);
      return -2;
   }  

   // Open Items file
   NameTaxCsvFile(acItemsFile, myCounty.acCntyCode, "Items");
   LogMsg("Open Items file %s for appending.", acItemsFile);
   fdItems = fopen(acItemsFile, "a");
   if (fdItems == NULL)
   {
      LogMsg("***** Error creating Items file: %s\n", acItemsFile);
      return -4;
   }

   // Open Agency file
   NameTaxCsvFile(acAgencyFile, myCounty.acCntyCode, "Agency");
   sprintf(acTmpFile, "%s\\%s\\%s_Agency.tmp", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   LogMsg("Create Agency file %s", acTmpFile);
   fdAgency = fopen(acTmpFile, "w");
   if (fdAgency == NULL)
   {
      LogMsg("***** Error creating Agency file: %s\n", acTmpFile);
      return -4;
   }

   // Init Agency table
   memset(acAgencyRec, 0, sizeof(TAXAGENCY));
   strcpy(pAgency->Code, "00000001");
   strcpy(pAgency->Agency, "GENERAL TAX");
   pAgency->TC_Flag[0] = '1';
   Tax_CreateAgencyCsv(acRec, (TAXAGENCY *)&acAgencyRec);
   fputs(acRec, fdAgency);
   strcpy(pAgency->Code, "00000002");
   strcpy(pAgency->Agency, "GENERAL SUPPLEMENTAL TAX");
   Tax_CreateAgencyCsv(acRec, (TAXAGENCY *)&acAgencyRec);
   fputs(acRec, fdAgency);
   
   // Merge loop 
   while (!feof(fdIn))
   {
      pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdIn);
      if (!pTmp)
         break;

      // Create Items & Agency record
      iRet = Sbd_ParseTaxDetailEx(acItemsRec, acAgencyRec, acRec);
      if (!iRet)
      {
         // Create TaxBase record
         Tax_CreateDetailCsv(acRec, (TAXDETAIL *)&acItemsRec);
         lItems++;
         fputs(acRec, fdItems);

         // Create Agency record
         Tax_CreateAgencyCsv(acRec, (TAXAGENCY *)&acAgencyRec);
         fputs(acRec, fdAgency);
      } else
      {
         LogMsg0("---> Drop record %d [%.80s]", lCnt, acRec); 
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdIn)
      fclose(fdIn);
   if (fdItems)
      fclose(fdItems);
   if (fdAgency)
      fclose(fdAgency);

   LogMsg("Total records processed:    %u", lCnt);
   LogMsg("Total Items records:        %u", lItems);

   LogMsg("Dedup Agency file %s to %s", acTmpFile, acAgencyFile);
   iRet = sortFile(acTmpFile, acAgencyFile, "S(#1,C,A) DUPOUT F(TXT)");

   // Import into SQL
   if (bImport)
   {
      iRet = doTaxImport(myCounty.acCntyCode, TAX_DETAIL);
      if (!iRet)
         iRet = doTaxImport(myCounty.acCntyCode, TAX_AGENCY);

      // Update BillNum on Tax_Items using data from Tax_Base
      if (!iRet)
      {
         LogMsg0("Update BillNum on Tax_Items using data from Tax_Base");
         iRet = update_TI_BillNum(myCounty.acCntyCode, "Ex");
      }
   } else
      iRet = 0;

   return iRet;
}

/********************************* Sbd_ExtrTRI ******************************
 *
 * Extract tax roll info from TRP.BDROM.TRB02.BIN.
 * This file contains a mix of different record type.  We have to extract based
 * on record header of each type ROLPARC, ROLBILL, ROLINST, ROLPYMT, ROLDELQ...
 *
 ****************************************************************************/

int CreateParcRec(unsigned char *pOutBuf, unsigned char *pInBuf)
{
   PARCREC  *pOutRec = (PARCREC *)pOutBuf;
   ROLPARC  *pInRec = (ROLPARC *)pInBuf;

   ebc2asc((unsigned char *)&pOutRec->APN[0], (unsigned char *)&pInRec->APN[0], iApnLen);

   // Ignore unsecured bill
   if (pOutRec->APN[9] != '0')
      return -1;
   pOutRec->ParcelStatus = EBC2ASC_table[pInRec->ParcelStatus];
   pOutRec->TaxStatus = EBC2ASC_table[pInRec->TaxStatus];
   pOutRec->TaxDelqInd = EBC2ASC_table[pInRec->TaxDelqInd];
   pOutRec->BadCheckInd = EBC2ASC_table[pInRec->BadCheckInd];
   pOutRec->DeededInd = EBC2ASC_table[pInRec->DeededInd];

   return 0;
}

int CreateBillRec(unsigned char *pOutBuf, unsigned char *pInBuf)
{
   BILLREC  *pOutRec = (BILLREC *)pOutBuf;
   ROLBILL  *pInRec = (ROLBILL *)pInBuf;
   int      iRet, iTmp;
   char     acTmp[32];

#ifdef _DEBUG
   //ebc2asc((unsigned char *)&acTmp[0], (unsigned char *)&pInRec->Header[8], 13);
   //if (!memcmp(acTmp, "020005132", 9))
   //   iTmp = 0;
#endif

   if (EBC2ASC_table[pInRec->Billed_Ind] != 'Y')
      return -1;
   if (EBC2ASC_table[pInRec->Eligibility_Ind] != 'A' && EBC2ASC_table[pInRec->Eligibility_Ind] != 'J')
      return -2;
   if (EBC2ASC_table[pInRec->TaxType[1]] != 'S')
      return -3;

   memset(pOutBuf, ' ', sizeof(BILLREC));
   pOutRec->Eligibility_Ind = EBC2ASC_table[pInRec->Eligibility_Ind];
   pOutRec->Billed_Ind = EBC2ASC_table[pInRec->Billed_Ind];

   // Roll year
   iTmp = F_Bin2Asc((LPSTR)pInRec->RollYear, (LPSTR)pOutRec->BillNum, 2);
   ebc2asc((unsigned char *)&pOutRec->BillNum[4], pInRec->BillSeq, 7);
   iTmp = F_Pd2Num((char *)pInRec->BillDate, 6);
   iRet = sprintf(acTmp, "%d", iTmp); 
   memcpy(pOutRec->BillDate, acTmp, iRet);
   ebc2asc((unsigned char *)&pOutRec->Supp_Nbr[0], pInRec->Supp_Nbr, 2);
   pOutRec->Supp_Type = EBC2ASC_table[pInRec->Supp_Type];
   pOutRec->RollType = EBC2ASC_table[pInRec->TaxType[0]];
   pOutRec->Legal_Status = EBC2ASC_table[pInRec->TaxType[1]];
   pOutRec->Appor_Status = EBC2ASC_table[pInRec->Appor_Status];
   iTmp = F_Bin2Asc((LPSTR)pInRec->RateYear, (LPSTR)pOutRec->RateYear, 2);
   iTmp = F_Bin2Asc((LPSTR)pInRec->AsmntYear, (LPSTR)pOutRec->AsmntYear, 2);

   // TRA
   pd2num((char *)&acTmp[0], (char *)&pInRec->TRA, 5);
   memcpy((char *)&pOutRec->TRA, &acTmp[3], 6);

   // Tax rate
   pd2num((char *)&acTmp[0], (char *)&pInRec->TaxRate, 4);
   iTmp = atol(acTmp);
   sprintf(acTmp, "%.6f", ((double)iTmp/1000000));
   memcpy((char *)&pOutRec->TaxRate, acTmp, 8);

   // Tax amount
   pd2num((char *)&acTmp[0], (char *)&pInRec->TaxAmt, 6);
   iTmp = atol(acTmp);
   iRet = sprintf(acTmp, "%.2f", ((double)iTmp/100));
   memcpy((char *)&pOutRec->TaxAmt, acTmp, iRet);

   // Special asmnt amt
   pd2num((char *)&acTmp[0], (char *)&pInRec->SpclAsmntAmt, 6);
   iTmp = atol(acTmp);
   iRet = sprintf(acTmp, "%.2f", ((double)iTmp/100));
   memcpy((char *)&pOutRec->SpclAsmntAmt, acTmp, iRet);

   // Months prorated
   ebc2asc((unsigned char *)&pOutRec->MonthsProRated[0], pInRec->MonthsProRated, 2);

   // ROLBILL-DELQ-PROC-COST
   pd2num((char *)&acTmp[0], (char *)&pInRec->DelqProcCost, 3);
   iTmp = atol(acTmp);
   iRet = sprintf(acTmp, "%.2f", ((double)iTmp/100));
   memcpy((char *)&pOutRec->DelqProcCost, acTmp, iRet);

   // Special Pay plan indicator
   pOutRec->SpclPayPlan = EBC2ASC_table[pInRec->SpclPayPlan];

   // Number of installments
   ebc2asc((unsigned char *)&pOutRec->NumInst[0], pInRec->NumInst, 2);

   // Payment status
   ebc2asc((unsigned char *)&pOutRec->PaymentStatus[0], pInRec->PaymentStatus, 2);

   // Tax roll status
   pOutRec->TaxRollStatusType = EBC2ASC_table[pInRec->TaxRollStatusType];
   pOutRec->TaxRollStatusRoll = EBC2ASC_table[pInRec->TaxRollStatusRoll];

   // Bill correction indicator
   pOutRec->BillCorrInd = EBC2ASC_table[pInRec->BillCorrInd];

   // Correction from
   iTmp = F_Bin2Asc((LPSTR)pInRec->BillFromYear, (LPSTR)pOutRec->BillFromYear, 2);
   if (iTmp > 0)
      ebc2asc((unsigned char *)&pOutRec->BillFromSeq[0], pInRec->BillFromSeq, 7);
   else
      pInRec->BillFromYear[0] = ' ';

   // Correction to
   iTmp = F_Bin2Asc((LPSTR)pInRec->BillToYear, (LPSTR)pOutRec->BillToYear, 2);
   if (iTmp > 0)
      ebc2asc((unsigned char *)&pOutRec->BillToSeq[0], pInRec->BillToSeq, 7);
   else
      pInRec->BillToYear[0] = ' ';
   
   // Roll extend date
   iTmp = F_Pd2Num((char *)pInRec->RollExtDate, 6);
   iRet = sprintf(acTmp, "%d", iTmp); 
   memcpy(pOutRec->RollExtDate, acTmp, iRet);

   // Bill adjustment
   pd2num((char *)&acTmp[0], (char *)&pInRec->AdjAmt, 6);
   iTmp = atol(acTmp);
   iRet = sprintf(acTmp, "%.2f", ((double)iTmp/100));
   memcpy((char *)&pOutRec->AdjAmt, acTmp, iRet);

   // Refund ind
   pOutRec->RefundInd = EBC2ASC_table[pInRec->RefundInd];

   // Refund date
   ebc2asc((unsigned char *)&pOutRec->RefundDate[0], pInRec->RefundDate, 6);

   // Refund amt
   pd2num((char *)&acTmp[0], (char *)&pInRec->RefundAmt, 6);
   iTmp = atol(acTmp);
   iRet = sprintf(acTmp, "%.2f", ((double)iTmp/100));
   memcpy((char *)&pOutRec->RefundAmt, acTmp, iRet);

   // Tax deferred indicator
   pOutRec->TaxDeferInd = EBC2ASC_table[pInRec->TaxDeferInd];

   // Bill delq date
   iTmp = F_Pd2Num((char *)pInRec->BillDelqDate, 6);
   iRet = sprintf(acTmp, "%d", iTmp); 
   memcpy(pOutRec->BillDelqDate, acTmp, iRet);

   // Bill delq indicator
   pOutRec->BillDelqInd = EBC2ASC_table[pInRec->BillDelqInd];

   // Days prorated
   ebc2asc((unsigned char *)&pOutRec->DaysProRated[0], pInRec->DaysProRated, 3);

   return 0;
}

int CreateInstRec(unsigned char *pInst1, unsigned char *pInst2, unsigned char *pInBuf)
{
   INSTREC  *pOutRec;
   ROLINST  *pInRec = (ROLINST *)pInBuf;
   int      iRet, iTmp;
   char     acTmp[32];

   // Inst#
   ebc2asc((unsigned char *)&acTmp[0], pInRec->InstNbr, 2);
   if (!memcmp(acTmp, "01", 2))
      pOutRec = (INSTREC *)pInst1;
   else
      pOutRec = (INSTREC *)pInst2;

   memcpy(pOutRec->InstNbr, acTmp, 2);

   // Inst amt
   pd2num((char *)&acTmp[0], (char *)&pInRec->InstAmt, 6);
   iTmp = atol(acTmp);
   iRet = sprintf(acTmp, "%.2f", ((double)iTmp/100));
   memcpy((char *)&pOutRec->InstAmt, acTmp, iRet);

   // Interest amt
   pd2num((char *)&acTmp[0], (char *)&pInRec->IntrAmt, 6);
   iTmp = atol(acTmp);
   iRet = sprintf(acTmp, "%.2f", ((double)iTmp/100));
   memcpy(pOutRec->IntrAmt, acTmp, iRet);

   // Discount amt
   pd2num((char *)&acTmp[0], (char *)&pInRec->DisAmt, 6);
   iTmp = atol(acTmp);
   iRet = sprintf(acTmp, "%.2f", ((double)iTmp/100));
   memcpy((char *)&pOutRec->DisAmt, acTmp, iRet);

   // Penalty amt
   pd2num((char *)&acTmp[0], (char *)&pInRec->PenAmt, 6);
   iTmp = atol(acTmp);
   iRet = sprintf(acTmp, "%.2f", ((double)iTmp/100));
   memcpy((char *)&pOutRec->PenAmt, acTmp, iRet);

   // Inst delq date
   iTmp = F_Pd2Num((char *)pInRec->DueDate, 6);
   if (iTmp > 19000000)
   {
      iRet = sprintf(acTmp, "%d", iTmp); 
      memcpy(pOutRec->DueDate, acTmp, iRet);
   }

   // Penalty indicator
   pOutRec->PenaltyInd = EBC2ASC_table[pInRec->PenaltyInd];

   // Discount code
   pOutRec->DiscountCode = EBC2ASC_table[pInRec->DiscountCode];

   // Appor year
   iTmp = F_Bin2Asc((LPSTR)pInRec->ApporYear, (LPSTR)acTmp, 2);
   if (iTmp > 1900)
   {
      iRet = sprintf(acTmp, "%d", iTmp); 
      memcpy(pOutRec->ApporYear, acTmp, iRet);
   }

   // Appor ind
   pOutRec->ApporInd = EBC2ASC_table[pInRec->ApporInd];

   // Pen charge ind
   pOutRec->PenChrgInd = EBC2ASC_table[pInRec->PenChrgInd];

   return 0;
}

int CreateDelqRec(unsigned char *pOutBuf, unsigned char *pInBuf)
{
   DELQREC  *pOutRec = (DELQREC *)pOutBuf;
   ROLDELQ  *pInRec = (ROLDELQ *)pInBuf;
   int      iRet, iTmp;
   char     acTmp[32];

   memset(pOutBuf, ' ', sizeof(DELQREC));

   // Delq type
   pOutRec->DelqType = EBC2ASC_table[pInRec->DelqType];

   // Delq Bill number
   iTmp = F_Bin2Asc((LPSTR)pInRec->RollYear, (LPSTR)acTmp, 2);
   if (iTmp > 0)
      memcpy(pOutRec->BillNum, acTmp, 4);
   ebc2asc((unsigned char *)&pOutRec->BillNum[4], pInRec->BillSeq, 7);

   // Redemption status
   pOutRec->RedempStatus = EBC2ASC_table[pInRec->RedempStatus];

   // Default date
   iTmp = F_Pd2Num((char *)pInRec->DefaultDate, 6);
   if (iTmp > 19000000)
   {
      iRet = sprintf(acTmp, "%d", iTmp); 
      memcpy(pOutRec->DefaultDate, acTmp, iRet);
   }

   // Original APN
   ebc2asc((unsigned char *)&pOutRec->OrgApn[0], pInRec->OrgApn, 13);

   // Pay plan code
   ebc2asc((unsigned char *)&pOutRec->PayPlanCode[0], pInRec->PayPlanCode, 2);

   // Pay plan seq
   ebc2asc((unsigned char *)&pOutRec->PayPlanSeq[0], pInRec->PayPlanSeq, 2);

   // Number of installments
   ebc2asc((unsigned char *)&pOutRec->NumOfInst[0], pInRec->NumOfInst, 2);

   // Payment status
   ebc2asc((unsigned char *)&pOutRec->PaymentStatus[0], pInRec->PaymentStatus, 2);

   // Next pay/due date
   iTmp = F_Pd2Num((char *)pInRec->NextPayDueDate, 6);
   if (iTmp > 19000000)
   {
      iRet = sprintf(acTmp, "%d", iTmp); 
      memcpy(pOutRec->NextPayDueDate, acTmp, iRet);
   }

   // Total redemption amt
   pd2num((char *)&acTmp[0], (char *)&pInRec->RedempAmt, 6);
   iTmp = atol(acTmp);
   iRet = sprintf(acTmp, "%.2f", ((double)iTmp/100));
   memcpy((char *)&pOutRec->RedempAmt, acTmp, iRet);

   // Redemption month
   ebc2asc((unsigned char *)&pOutRec->RedempMonth[0], pInRec->RedempMonth, 2);

   // Total fee & interest
   ebc2asc((unsigned char *)&acTmp[0], pInRec->TotalFeeIntrAmt, 6);
   iTmp = atoin(acTmp, 6);
   iRet = sprintf(acTmp, "%.2f", ((double)iTmp/100));
   memcpy((char *)&pOutRec->TotalFeeIntrAmt, acTmp, iRet);

   // Pay plan post flag
   pOutRec->PayPlanPostFlag = EBC2ASC_table[pInRec->PayPlanPostFlag];

   return 0;
}

int CreateAgacRec(unsigned char *pOutBuf, unsigned char *pInBuf)
{
   PARCREC  *pOutRec = (PARCREC *)pOutBuf;
   ROLPARC  *pInRec = (ROLPARC *)pInBuf;

   ebc2asc((unsigned char *)&pOutRec->APN[0], (unsigned char *)&pInRec->APN[0], iApnLen);

   // Ignore unsecured bill
   if (pOutRec->APN[9] != '0')
      return -1;
   pOutRec->ParcelStatus = EBC2ASC_table[pInRec->ParcelStatus];
   pOutRec->TaxStatus = EBC2ASC_table[pInRec->TaxStatus];
   pOutRec->TaxDelqInd = EBC2ASC_table[pInRec->TaxDelqInd];
   pOutRec->BadCheckInd = EBC2ASC_table[pInRec->BadCheckInd];
   pOutRec->DeededInd = EBC2ASC_table[pInRec->DeededInd];

   return 0;
}

int CreateValueRec(unsigned char *pOutBuf, unsigned char *pInBuf)
{
   LIENEXTR *pOutRec= (LIENEXTR *)pOutBuf;
   ROLVALU  *pInRec = (ROLVALU *)pInBuf;
   char     acTmp[256];
   int      iRet;
   ULONG    lLand, lImpr, lPersProp, lNet, lPen, lGross;

   memset(pOutBuf, ' ', sizeof(LIENEXTR));
   ebc2asc((unsigned char *)&pOutRec->acApn, pInRec->APN, TSIZ_APN);
   
   pOutRec->SpclFlag = EBC2ASC_table[pInRec->ValueType];
   pd2num((char *)&acTmp[0], (char *)&pInRec->LandVal, 6);
   lLand = atol(acTmp);
   if (lLand > 0)
   {
      iRet = sprintf(acTmp, "%*u", sizeof(pOutRec->acLand), lLand);
      memcpy((char *)&pOutRec->acLand, acTmp, iRet);
   }

   pd2num((char *)&acTmp[0], (char *)&pInRec->ImprVal, 6);
   lImpr = atol(acTmp);
   if (lImpr > 0)
   {
      iRet = sprintf(acTmp, "%*u", sizeof(pOutRec->acImpr), lImpr);
      memcpy((char *)&pOutRec->acImpr, acTmp, iRet);
   }

   pd2num((char *)&acTmp[0], (char *)&pInRec->PersPropVal, 6);
   lPersProp = atol(acTmp);
   if (lPersProp > 0)
   {
      iRet = sprintf(acTmp, "%*u", sizeof(pOutRec->acPP_Val), lPersProp);
      memcpy((char *)&pOutRec->acPP_Val, acTmp, iRet);
   }

   lGross = lLand+lImpr+lPersProp;
   if (lGross > 0)
   {
      iRet = sprintf(acTmp, "%*u", sizeof(pOutRec->acGross), lGross);
      memcpy((char *)&pOutRec->acGross, acTmp, iRet);
   }

   pd2num((char *)&acTmp[0], (char *)&pInRec->NetVal, 6);
   lNet = atol(acTmp);
   iRet = sprintf(acTmp, "%*u", sizeof(pOutRec->acNetTaxValue), lNet);
   memcpy((char *)&pOutRec->acNetTaxValue, acTmp, iRet);

   pd2num((char *)&acTmp[0], (char *)&pInRec->Impr_Pen, 6);
   lPen = atol(acTmp);
   pd2num((char *)&acTmp[0], (char *)&pInRec->PP_Pen, 6);
   lPen += atol(acTmp);

   //iRet = sprintf(acTmp, "%u", lPen);
   //memcpy((char *)&pOutRec->acOther, acTmp, iRet);

   long lExe = (lLand+lImpr+lPersProp+lPen) - lNet;
   pOutRec->acHO[0] = '2';
   if (lExe > 0)
   {
      iRet = sprintf(acTmp, "%*u", sizeof(pOutRec->acExAmt), lExe);
      memcpy((char *)&pOutRec->acExAmt, acTmp, iRet);
      if (lExe == 7000)
         pOutRec->acHO[0] = '1';
   }

   return 0;
}

int CreateExemRec(unsigned char *pOutBuf, unsigned char *pInBuf)
{
   LIENEXTR *pOutRec= (LIENEXTR *)pOutBuf;
   ROLEXEM  *pInRec = (ROLEXEM *)pInBuf;
   char     acTmp[256];
   int      iRet;
   long     lExe;

   ebc2asc((unsigned char *)&acTmp, pInRec->APN, TSIZ_APN);
   if (memcmp(acTmp, pOutRec->acApn, TSIZ_APN))
      return 1;

   pd2num((char *)&acTmp[0], (char *)&pInRec->ExeAmt, 6);
   lExe = atol(acTmp);

   if (lExe > 0)
   {
      ebc2asc((unsigned char *)&acTmp, pInRec->ExeType, 2);
      if (!memcmp(acTmp, "00", 2))
         pOutRec->acHO[0] = '1';
      else
      {
         pOutRec->acHO[0] = '2';
         memcpy(pOutRec->acExCode, acTmp, 2);
      }

      iRet = sprintf(acTmp, "%*u", sizeof(pOutRec->acExAmt), lExe);
      if (memcmp(acTmp, pOutRec->acExAmt, iRet))
      {
         memcpy((char *)&pOutRec->acExAmt, acTmp, iRet);
         if (bDebug)
            LogMsg("New Exe Amt: %s [%.13s]", acTmp, pOutRec->acApn);
      }
   }

   return 0;
}

/******************************************************************************
 *
 * Convert TRP.BDROM.TRB02.BIN to ASCII file for loading tax base & delq.
 *
 ******************************************************************************/

int Sbd_ExtrTRI()
{
   char  acInFile[_MAX_PATH], acBaseExtr[_MAX_PATH], acDelqExtr[_MAX_PATH], acBaseTmp[_MAX_PATH], 
         acDelqTmp[_MAX_PATH], acValuTmp[_MAX_PATH], acValuExtr[_MAX_PATH];
   unsigned char acOutBuf[512], acInBuf[512], acTaxRec[2048], iLen;

   int   iTmp, iRet, iParcCnt, iBillCnt, iNameCnt, iValuCnt, iPymtCnt, iHistCnt, iInstCnt, iOthers, iRecOut,
         iAgencyCnt, iCollCnt, iCorrCnt, iCortCnt, iDBilCnt, iDelqCnt, iFeesCnt, iPrtyCnt, iWaivCnt, iSaleCnt, 
         iPadjCnt, iAddrCnt, iExemCnt, iRadjCnt, iTaxYear;
   int   lCnt=0;

   FILE     *fdIn, *fdBase, *fdDelq, *fdValu;
   SBDTAX   *pTaxRec = (SBDTAX *)acTaxRec;
   DELQREC  *pDelqRec = (DELQREC *)acOutBuf;
   LIENEXTR sValuRec;

   LogMsg0("Extract Tax Roll Info");

   // Tax roll info
   GetIniString(myCounty.acCntyCode, "TaxRollInfo", "", acInFile, _MAX_PATH, acIniFile);
   GetIniString(myCounty.acCntyCode, "TaxBaseExtr", "", acBaseExtr, _MAX_PATH, acIniFile);
   GetIniString(myCounty.acCntyCode, "TaxDelqExtr", "", acDelqExtr, _MAX_PATH, acIniFile);
   GetIniString(myCounty.acCntyCode, "TaxValuExtr", "", acValuExtr, _MAX_PATH, acIniFile);

   // Open tax roll file
   LogMsg("Open Tax Roll file %s", acInFile);
   fdIn = fopen(acInFile, "rb");
   if (fdIn == NULL)
   {
      LogMsg("***** Error opening tax roll file: %s\n", acInFile);
      return -1;
   }
   // Skip header
   iTmp = fread(acInFile, 1, 224, fdIn);

   // Open Output base file
   sprintf(acBaseTmp, "%s\\%s\\Base_Extr.txt", acTmpPath, myCounty.acCntyCode);
   LogMsg("Open base temp file %s", acBaseTmp);
   fdBase = fopen(acBaseTmp, "w");
   if (fdBase == NULL)
   {
      LogMsg("***** Error creating base extract file: %s\n", acBaseTmp);
      return -2;
   }

   // Open Output delq file
   sprintf(acDelqTmp, "%s\\%s\\Delq_Extr.txt", acTmpPath, myCounty.acCntyCode);
   LogMsg("Open Delq temp file %s", acDelqTmp);
   fdDelq = fopen(acDelqTmp, "w");
   if (fdDelq == NULL)
   {
      LogMsg("***** Error creating delq extract file: %s\n", acDelqTmp);
      return -3;
   }

   // Open Output value file
   sprintf(acValuTmp, "%s\\%s\\Valu_Extr.txt", acTmpPath, myCounty.acCntyCode);
   LogMsg("Open base temp file %s", acValuTmp);
   fdValu = fopen(acValuTmp, "w");
   if (fdValu == NULL)
   {
      LogMsg("***** Error creating value extract file: %s\n", acValuTmp);
      return -2;
   }

   // Init rec counts
   iParcCnt=iBillCnt=iNameCnt=iValuCnt=iPymtCnt=iHistCnt=iInstCnt=iOthers=iRecOut=iAddrCnt=iExemCnt=iRadjCnt=0;
   iAgencyCnt=iCollCnt=iCorrCnt=iCortCnt=iDBilCnt=iDelqCnt=iFeesCnt=iPrtyCnt=iWaivCnt=iSaleCnt=iPadjCnt=0;
   memset(acTaxRec, ' ', sizeof(SBDTAX));
   sValuRec.acApn[0] = 0;

   // Extract loop
   while (!feof(fdIn))
   {
      // Get rec len
      iTmp = fread(acInBuf, 1, 2, fdIn);
      iLen = acInBuf[0];
      if (iLen > 68)
      {
         iLen -= 2;
         iTmp = fread(acInBuf, 1, iLen, fdIn);
         if (iTmp != iLen)
            break;
      } else if (iLen == 8)      // EOF
      {
         break;
      } else
      {
         LogMsg("***** Bad record %u", iLen);
         break;
      }

      ebc2asc((unsigned char *)&acOutBuf[0], (unsigned char *)&acInBuf[0], 8);
      if (!memcmp(acOutBuf, "ROLPARC", 7))
      {
         // Output prev bill
         iTmp = atoin(pTaxRec->Bill.AsmntYear, 4);
         if (iTmp == lTaxYear)
         {
            pTaxRec->CrLf[0] = 10;
            pTaxRec->CrLf[1] = 0;

            iRecOut++;
            fputs((char *)acTaxRec, fdBase);
         }

         // New parcel
         memset(acTaxRec, ' ', sizeof(SBDTAX));

         iRet = CreateParcRec((unsigned char *)&pTaxRec->Parcel.APN[0], acInBuf);
         if (!iRet)
            iParcCnt++;
         else
            acTaxRec[0] = 0;
#ifdef _DEBUG
         //if (!memcmp(pTaxRec->Parcel.APN, "0108301030000", 10))
         //   iTmp = 0;
#endif
      } else if (acTaxRec[0] >= '0' && !memcmp(acOutBuf, "ROLBILL", 7))
      {
         iTaxYear = atoin(pTaxRec->Bill.AsmntYear, 4);
         if (iTaxYear == lTaxYear)
         {
            pTaxRec->CrLf[0] = 10;
            pTaxRec->CrLf[1] = 0;
            
            iRecOut++;
            fputs((char *)acTaxRec, fdBase);

            // Reset bill
            memset(pTaxRec->Bill.AsmntYear, ' ', 4);
         }

         iRet = CreateBillRec((unsigned char *)&pTaxRec->Bill.BillNum[0], acInBuf);
         iBillCnt++;
      } else if (acTaxRec[0] >= '0' && !memcmp(acOutBuf, "ROLINST", 7))
      {
         iRet = CreateInstRec((unsigned char *)&pTaxRec->Bill.Inst1.InstNbr[0], (unsigned char *)&pTaxRec->Bill.Inst2.InstNbr[0], acInBuf);
         iInstCnt++;
      } else if (!memcmp(acOutBuf, "ROLDELQ", 7))
      {
         iRet = CreateDelqRec((unsigned char *)&acOutBuf, acInBuf);
         if (!iRet)
         {
            pDelqRec->CrLf[0] = 10;
            pDelqRec->CrLf[1] = 0;
            
            fputs((char *)acOutBuf, fdDelq);
            iDelqCnt++;
         }
      } else if (!memcmp(acOutBuf, "ROLPYMT", 7))
      {
         acOutBuf[0] = 0;
         iPymtCnt++;
      } else if (!memcmp(acOutBuf, "ROLHIST", 7))
      {
         acOutBuf[0] = 0;
         iHistCnt++;
      } else if (!memcmp(acOutBuf, "ROLNAME", 7))
      {
         acOutBuf[0] = 0;
         iNameCnt++;
      } else if (!memcmp(acOutBuf, "ROLAGAC", 7))
      {
         //iRet = CreateAgacRec((unsigned char *)&acOutBuf, acInBuf);

         acOutBuf[0] = 0;
         iAgencyCnt++;
      } else if (!memcmp(acOutBuf, "ROLCOLL", 7))
      {
         acOutBuf[0] = 0;
         iCollCnt++;
      } else if (!memcmp(acOutBuf, "ROLCORR", 7))
      {
         acOutBuf[0] = 0;
         iCorrCnt++;
      } else if (!memcmp(acOutBuf, "ROLCORT", 7))
      {
         acOutBuf[0] = 0;
         iCortCnt++;
      } else if (!memcmp(acOutBuf, "ROLDBIL", 7) || !memcmp(acOutBuf, "ROLBILL", 7))
      {
         acOutBuf[0] = 0;
         iDBilCnt++;
      } else if (!memcmp(acOutBuf, "ROLPADJ", 7))
      {
         acOutBuf[0] = 0;
         iPadjCnt++;
      } else if (!memcmp(acOutBuf, "ROLADJS", 7))
      {
         acOutBuf[0] = 0;
         iRadjCnt++;
      } else if (!memcmp(acOutBuf, "ROLFEES", 7))
      {
         acOutBuf[0] = 0;
         iFeesCnt++;
      } else if (!memcmp(acOutBuf, "ROLPRTY", 7))
      {
         acOutBuf[0] = 0;
         iPrtyCnt++;
      } else if (!memcmp(acOutBuf, "ROLWAIV", 7))
      {
         acOutBuf[0] = 0;
         iWaivCnt++;
      } else if (!memcmp(acOutBuf, "ROLSALE", 7))
      {
         acOutBuf[0] = 0;
         iSaleCnt++;
      } else if (!memcmp(acOutBuf, "ROLADDR", 7))
      {
         acOutBuf[0] = 0;
         iAddrCnt++;
      } else if (!memcmp(acOutBuf, "ROLNOTE", 7))
      {
         acOutBuf[0] = 0;
         iOthers++;
      } else if (!memcmp(acOutBuf, "ROLVALU", 7))
      {
         LIENEXTR *pOutRec= (LIENEXTR *)acOutBuf;

#ifdef _DEBUG
         //if (!memcmp(pTaxRec->Parcel.APN, "0108301030000", 10))
         //   iTmp = 0;
#endif
         iTaxYear = atoin(pTaxRec->Bill.AsmntYear, 4);
         //if (iTaxYear == lTaxYear)
         //{
            iRet = CreateValueRec((unsigned char *)&sValuRec, acInBuf);
            memcpy(sValuRec.acYear, pTaxRec->Bill.AsmntYear, 4);
            sValuRec.LF[0] = 10;
            sValuRec.LF[1] = 0;           
            if (sValuRec.acApn[0] >= '0')
            {
               fputs((char *)&sValuRec, fdValu);
               iValuCnt++;
            }
         //}
      } else if (!memcmp(acOutBuf, "ROLEXEM", 7))
      {
         iTaxYear = atoin(pTaxRec->Bill.AsmntYear, 4);
         //if (iTaxYear == lTaxYear)
         //{
         //   // Temporary ignore otput from this 
         //   iRet = CreateExemRec((unsigned char *)&sValuRec, acInBuf);
         //   iExemCnt++;
         //}
      } else if (acOutBuf[0] > '0')
      {
         if (memcmp(acOutBuf, "ROLINST", 7))
         iOthers++;
      }

#ifdef _DEBUG
      //if (!memcmp(acRollRec, "041 41980370", 12))
      //   iTmp = 0;
#endif
      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Output last bill
   iTmp = atoin(pTaxRec->Bill.AsmntYear, 4);
   if (iTmp == lTaxYear)
   {
      pTaxRec->CrLf[0] = 10;
      pTaxRec->CrLf[1] = 0;

      iRecOut++;
      fputs((char *)acTaxRec, fdBase);
   }

   // Close files
   if (fdIn)
      fclose(fdIn);
   if (fdBase)
      fclose(fdBase);
   if (fdDelq)
      fclose(fdDelq);
   if (fdValu)
      fclose(fdValu);

   // Sort base extract
   iRecOut = sortFile(acBaseTmp, acBaseExtr, "S(1,13,C,A) OMIT(15,1,C,NE,\"A\") F(TXT)");

   // Sort delq extract
   iRet = sortFile(acDelqTmp, acDelqExtr, "S(22,13,C,A,14,8,C,A) F(TXT) ");

   // Sort Value extract
   iRet = sortFile(acValuTmp, acValuExtr, "S(1,18,C,A) F(TXT) ");

   LogMsg("Total records processed:     %d", lCnt);
   LogMsg("    Base records output:     %d", iRecOut);
   LogMsg("    Delq records output:     %d\n", iRet);
   LogMsg(" Parcel:     %d", iParcCnt);
   LogMsg("   Bill:     %d", iBillCnt);
   LogMsg("Payment:     %d", iPymtCnt);
   LogMsg("PadjAdj:     %d", iPadjCnt);
   LogMsg("RollAdj:     %d", iRadjCnt);
   LogMsg("History:     %d", iHistCnt);
   LogMsg("   Name:     %d", iNameCnt);
   LogMsg("  Value:     %d", iValuCnt);
   LogMsg("   Inst:     %d", iInstCnt);
   LogMsg(" Agency:     %d", iAgencyCnt);
   LogMsg("   Coll:     %d", iCollCnt);
   LogMsg("   Corr:     %d", iCorrCnt);
   LogMsg(" Cortac:     %d", iCortCnt);
   LogMsg("DelqBil:     %d", iDBilCnt);
   LogMsg("   Delq:     %d", iDelqCnt);
   LogMsg("   Fees:     %d", iFeesCnt);
   LogMsg("IntPrty:     %d", iPrtyCnt);
   LogMsg(" Waiver:     %d", iWaivCnt);
   LogMsg("   Sale:     %d", iSaleCnt);
   LogMsg("   Addr:     %d", iAddrCnt);
   LogMsg("   Exem:     %d", iExemCnt);
   LogMsg(" Others:     %d\n", iOthers);

   if (iRecOut < 100000)
      return -1;
   else
      return 0;
}

int Sbd_ExtrVal()
{
   char  acInFile[_MAX_PATH], acValuTmp[_MAX_PATH], acValuExtr[_MAX_PATH];
   unsigned char acOutBuf[512], acInBuf[512], acTaxRec[2048], iLen;

   int   iTmp, iRet, iParcCnt, iBillCnt, iValuCnt, iValuOut, iExemCnt, iOthers, iTaxYear;
   int   lCnt=0;

   FILE     *fdIn, *fdValu;
   SBDTAX   *pTaxRec = (SBDTAX *)acTaxRec;
   LIENEXTR sValuRec;

   LogMsg0("Extracting value from tax file.");

   // Tax roll info
   GetIniString(myCounty.acCntyCode, "TaxRollInfo", "", acInFile, _MAX_PATH, acIniFile);
   sprintf(acValuExtr, acValueTmpl, myCounty.acCntyCode, myCounty.acCntyCode);

   // Open tax roll file
   LogMsg("Open Tax Roll file %s", acInFile);
   fdIn = fopen(acInFile, "rb");
   if (fdIn == NULL)
   {
      LogMsg("***** Error opening tax roll file: %s\n", acInFile);
      return -1;
   }
   // Skip header
   iTmp = fread(acInFile, 1, 224, fdIn);

   // Open Output value file
   sprintf(acValuTmp, "%s\\%s\\Valu_Extr.txt", acTmpPath, myCounty.acCntyCode);
   LogMsg("Open base temp file %s", acValuTmp);
   fdValu = fopen(acValuTmp, "w");
   if (fdValu == NULL)
   {
      LogMsg("***** Error creating value extract file: %s\n", acValuTmp);
      return -2;
   }

   // Init rec counts
   iParcCnt=iBillCnt=iValuCnt=iValuOut=iExemCnt=iOthers=0;
   memset(acTaxRec, ' ', sizeof(SBDTAX));
   sValuRec.acApn[0] = 0;

   // Extract loop
   while (!feof(fdIn))
   {
      // Get rec len
      iTmp = fread(acInBuf, 1, 2, fdIn);
      iLen = acInBuf[0];
      if (iLen > 68)
      {
         iLen -= 2;
         iTmp = fread(acInBuf, 1, iLen, fdIn);
         if (iTmp != iLen)
            break;
      } else if (iLen == 8)      // EOF
      {
         break;
      } else
      {
         LogMsg("***** Bad record %u", iLen);
         break;
      }

      ebc2asc((unsigned char *)&acOutBuf[0], (unsigned char *)&acInBuf[0], 8);
      if (!memcmp(acOutBuf, "ROLPARC", 7))
      {
         iTaxYear = atoin(sValuRec.acYear, 4);
         if (iTaxYear == lTaxYear && sValuRec.acApn[0] >= '0')
         {
            sValuRec.LF[0] = 10;
            sValuRec.LF[1] = 0;           
            fputs((char *)&sValuRec, fdValu);
            iValuOut++;
            sValuRec.acApn[0] = 0;
         } else if (sValuRec.acApn[0] >= '0')
         {
            LogMsg("** Drop %d ROLPARC: %.14s ROLVALU: %.14s", iTaxYear, pTaxRec->Parcel.APN, sValuRec.acApn);
         }

         // New parcel
         memset(acTaxRec, ' ', sizeof(SBDTAX));

         iRet = CreateParcRec((unsigned char *)&pTaxRec->Parcel.APN[0], acInBuf);
         if (!iRet)
            iParcCnt++;
         else
            acTaxRec[0] = 0;
#ifdef _DEBUG
         //if (!memcmp(pTaxRec->Parcel.APN, "0108322020000", 10))
         //   iTmp = 0;
#endif
      } else if (acTaxRec[0] >= '0' && !memcmp(acOutBuf, "ROLBILL", 7))
      {
         iTaxYear = atoin(pTaxRec->Bill.AsmntYear, 4);
         if (iTaxYear == lTaxYear)
         {
            // Reset bill
            memset(pTaxRec->Bill.AsmntYear, ' ', 4);
         }

         iRet = CreateBillRec((unsigned char *)&pTaxRec->Bill.BillNum[0], acInBuf);
         iBillCnt++;
      } else if (!memcmp(acOutBuf, "ROLVALU", 7))
      {
         //LIENEXTR *pOutRec= (LIENEXTR *)acOutBuf;

#ifdef _DEBUG
         //if (!memcmp(pTaxRec->Parcel.APN, "0108301200000", 10))
         //   iTmp = 0;
#endif
         iTaxYear = atoin(pTaxRec->Bill.AsmntYear, 4);
         if (iTaxYear == lTaxYear && pTaxRec->Bill.RollType == 'A')
         {
            iRet = CreateValueRec((unsigned char *)&sValuRec, acInBuf);
            memcpy(sValuRec.acYear, pTaxRec->Bill.AsmntYear, 4);
            memcpy(sValuRec.acTRA, pTaxRec->Bill.TRA, 6);
            iValuCnt++;
#ifdef _DEBUG
            //if (!memcmp(sValuRec.acApn, "0108301200000", 10))
            //   iTmp = 0;
#endif
         }
      } else if (!memcmp(acOutBuf, "ROLEXEM", 7))
      {
         iTaxYear = atoin(pTaxRec->Bill.AsmntYear, 4);
         if (iTaxYear == lTaxYear)
         {
            iRet = CreateExemRec((unsigned char *)&sValuRec, acInBuf);
            iExemCnt++;
         }
      } else if (acOutBuf[0] > '0')
      {
         iOthers++;
      }

#ifdef _DEBUG
      //if (!memcmp(acRollRec, "041 41980370", 12))
      //   iTmp = 0;
#endif
      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdIn)
      fclose(fdIn);
   if (fdValu)
      fclose(fdValu);

   // Sort Value extract on APN+YEAR
   iRet = sortFile(acValuTmp, acValuExtr, "S(1,18,C,A) F(TXT) ");

   LogMsg("Total records processed:   %d", lCnt);
   LogMsg("           Parcel count:   %d", iParcCnt);
   LogMsg("           Exempt count:   %d", iExemCnt);
   LogMsg("           Value count:    %d", iValuCnt);
   LogMsg("           Bill count:     %d", iBillCnt);
   LogMsg("           Value output:   %d", iValuOut);

   if (iValuCnt < 100000)
      return -1;
   else
      return 0;
}

/***************************** Sbd_ParseTaxBase *****************************
 *
 * Create TaxBase output
 * Use TaxAmt to create detail record for general county tax
 *
 * Return 0 if success
 *
 ****************************************************************************/

int Sbd_ParseTaxBase(char *pOutbuf, char *pDetail, char *pInbuf)
{
   int      iTmp;
   long     taxYear;
   double	dTmp, dTax1, dTax2, dTaxTotal, dDue1, dDue2, dIntr1, dIntr2, dPen1, dPen2, dDisc1, dDisc2;

   TAXBASE  *pOutRec = (TAXBASE *)pOutbuf;
   SBDTAX   *pInRec  = (SBDTAX *)pInbuf;

   // Clear output buffer
   memset(pOutbuf, 0, sizeof(TAXBASE));

   // APN
   memcpy(pOutRec->Apn, pInRec->Parcel.APN, TSIZ_APN);

   // Tax Year
   memcpy(pOutRec->TaxYear, pInRec->Bill.AsmntYear, 4);
   taxYear = atol(pOutRec->TaxYear);

   // Defaulted year

   // Tax rate
   dTmp = atofn(pInRec->Bill.TaxRate, TBSIZ_RATE);
   sprintf(pOutRec->TotalRate, "%.6f", dTmp*100.0);

   // Bill Number 
   memcpy(pOutRec->BillNum, &pInRec->Bill.BillNum[2], TBSIZ_BILLNUM-2);

   // TRA 
   memcpy(pOutRec->TRA, &pInRec->Bill.TRA, TBSIZ_TRA);

#ifdef _DEBUG
   //if (!memcmp(pOutRec->Apn, "060097011", 9))
   //   iTmp = 0;
#endif

   // Due date
   memcpy(pOutRec->DueDate1, pInRec->Bill.Inst1.DueDate, TBSIZ_YYYYMMDD);
   memcpy(pOutRec->DueDate2, pInRec->Bill.Inst2.DueDate, TBSIZ_YYYYMMDD);

   dDue1=dDue2=dIntr1=dIntr2=dPen1=dPen2=dDisc1=dDisc2 = 0;

   // Check for Tax amount
   dTaxTotal = atofn(pInRec->Bill.TaxAmt, TBSIZ_AMT);
   if (dTaxTotal > 0.0)
   {
      TAXDETAIL  *pDetailRec = (TAXDETAIL *)pDetail;
      memset(pDetail, 0, sizeof(TAXDETAIL));

      // Create detail record
      memcpy(pDetailRec->Apn, pInRec->Parcel.APN, TSIZ_APN);
      memcpy(pDetailRec->BillNum, &pInRec->Bill.BillNum[2], TBSIZ_BILLNUM-2);

      memcpy(pDetailRec->TaxYear, pInRec->Bill.AsmntYear, 4);
      sprintf(pDetailRec->TaxAmt, "%.2f", dTaxTotal);
      if (pInRec->Bill.RollType == 'A')
         strcpy(pDetailRec->TaxCode, "00000001");
      else
         strcpy(pDetailRec->TaxCode, "00000002");
      pDetailRec->TC_Flag[0] = '1';
   } else
      *pDetail = 0;

   dTaxTotal += atofn(pInRec->Bill.SpclAsmntAmt, TBSIZ_AMT);
   dTax1 = atofn(pInRec->Bill.Inst1.InstAmt, TBSIZ_AMT);
   dTax2 = atofn(pInRec->Bill.Inst2.InstAmt, TBSIZ_AMT);

   // Interest
   if ((pInRec->Bill.Inst1.PenaltyInd > ' ' && pInRec->Bill.Inst1.PenaltyInd != 'P' && pInRec->Bill.Inst1.PenaltyInd != 'W')
      || pInRec->Bill.Inst1.PenChrgInd > ' ')
   {
      dIntr1 = atofn(pInRec->Bill.Inst1.IntrAmt, TBSIZ_AMT);
      dIntr2 = atofn(pInRec->Bill.Inst2.IntrAmt, TBSIZ_AMT);
      if (dIntr1 > 0 || dIntr2 > 0)
         iTmp = 0;

      // Penalty
      dPen1 = atofn(pInRec->Bill.Inst1.PenAmt, TBSIZ_AMT);
      dPen2 = atofn(pInRec->Bill.Inst2.PenAmt, TBSIZ_AMT);
      if (dPen1 > 0 || dPen2 > 0)
         iTmp = 0;
   }

   // Discount
   dDisc1 = atofn(pInRec->Bill.Inst1.DisAmt, TBSIZ_AMT);
   dDisc2 = atofn(pInRec->Bill.Inst2.DisAmt, TBSIZ_AMT);
   if (dDisc1 > 0 || dDisc2 > 0)
      iTmp = 0;

   dDue1=dDue2=0.0;

   iTmp = atoin(pInRec->Bill.PaymentStatus, TSIZ_PAIDSTATUS);
   if (!iTmp)
   {  // Nothing paid
      if (ChkDueDate(1, taxYear))
      {
         dDue1 = dTax1;
         dDue2 = dTax2;
         pOutRec->Inst1Status[0] = TAX_BSTAT_PASTDUE;
      } else
      {
         pOutRec->Inst1Status[0] = TAX_BSTAT_UNPAID;
         if (dTax2 > 0)
            pOutRec->Inst2Status[0] = TAX_BSTAT_UNPAID;
      }
   } else if (iTmp == 1)
   {  // Inst1 paid
      pOutRec->Inst1Status[0] = TAX_BSTAT_PAID;
      sprintf(pOutRec->PaidAmt1, "%.2f", dTax1);
      if (ChkDueDate(2, taxYear))
      {
         dDue2 = dTax2;
         pOutRec->Inst2Status[0] = TAX_BSTAT_PASTDUE;
      } else
         pOutRec->Inst2Status[0] = TAX_BSTAT_UNPAID;
   } else if (iTmp == 2)
   {  // Both paid
      pOutRec->Inst1Status[0] = TAX_BSTAT_PAID;
      pOutRec->Inst2Status[0] = TAX_BSTAT_PAID;
      sprintf(pOutRec->PaidAmt1, "%.2f", dTax1);
      sprintf(pOutRec->PaidAmt2, "%.2f", dTax2);
   } else
   {
      LogMsg("??? Questionable paid status on APN= %s:%.2d", pOutRec->Apn, iTmp);
   }

#ifdef _DEBUG
   //if ((int)dTaxTotal != (int)(dTax1+dTax2))
   //   iRet = 0;
#endif
   if (dTaxTotal > 0.0)
   {
      sprintf(pOutRec->TaxAmt1, "%.2f", dTax1);
      sprintf(pOutRec->TaxAmt2, "%.2f", dTax2);
      sprintf(pOutRec->TotalTaxAmt, "%.2f", dTaxTotal);
   }

   // Total due
   dTaxTotal = dDue1+dDue2;
   if (dTaxTotal > 0.0)
      sprintf(pOutRec->TotalDue, "%.2f", dTaxTotal);

   if (pInRec->Bill.RollType == 'A')
   {
      pOutRec->BillType[0] = BILLTYPE_SECURED;
      pOutRec->isSecd[0] = '1';
      pOutRec->isSupp[0] = '0';
   } else if (pInRec->Bill.Supp_Type > ' ')
   {
      pOutRec->BillType[0] = BILLTYPE_SECURED_SUPPL;
      pOutRec->isSecd[0] = '0';
      pOutRec->isSupp[0] = '1';
   } else
      LogMsg("*** Unknown bill type: %c%s [%.13s]", pInRec->Bill.RollType, pInRec->Bill.Supp_Type, pInRec->Parcel.APN);

   return 0;
}

/***************************** Sbd_Load_TaxBase ******************************
 *
 * Create import file from tax base extract Base_Extr.txt and import into SQL 
 *
 * Return 0 if success.
 *
 *****************************************************************************/

int Sbd_Load_TaxBase(bool bImport)
{
   char     *pTmp, acBase[1024], acDetail[1024], acRec[512],
            acBaseFile[_MAX_PATH], acItemsFile[_MAX_PATH], acInFile[_MAX_PATH], acOutbuf[MAX_RECSIZE];
            
   int      iRet, iDrop=0;
   long     lBase=0, lCnt=0;
   FILE     *fdBase, *fdItems, *fdIn;
   TAXBASE  *pBase = (TAXBASE *)acBase;

   LogMsg0("Loading Tax Base for SBD");
   NameTaxCsvFile(acBaseFile, myCounty.acCntyCode, "Base");

   // Check file date 
   GetIniString(myCounty.acCntyCode, "TaxRollInfo", "", acInFile, _MAX_PATH, acIniFile);
   lLastTaxFileDate = getFileDate(acInFile);
   // Only process if new tax file
   iRet = isNewTaxFile(NULL, myCounty.acCntyCode);
   if (iRet <= 0)
   {
      LogMsg("*** Skip loading Tax Base. Set ChkFileDate=N to bypass new file check");
      lLastTaxFileDate = 0;
      return iRet;
   }

   // Convert BIN file to ASCII
   GetIniString(myCounty.acCntyCode, "TaxBaseExtr", "", acInFile, _MAX_PATH, acIniFile);
   iRet = getFileDate(acInFile);
   if (iRet < lLastTaxFileDate)
      iRet = Sbd_ExtrTRI();
   else
      LogMsg("TRI file is up to date, no need to convert BIN2ASC");

   // Open input file
   LogMsg("Open tax file %s", acInFile);
   fdIn = fopen(acInFile, "r");
   if (fdIn == NULL)
   {
      LogMsg("***** Error opening tax file: %s\n", acInFile);
      return -2;
   }  

   // Open Output file
   LogMsg("Create Base file %s", acBaseFile);
   fdBase = fopen(acBaseFile, "w");
   if (fdBase == NULL)
   {
      LogMsg("***** Error creating output file: %s\n", acBaseFile);
      return -4;
   }

   // Open Items file
   NameTaxCsvFile(acItemsFile, myCounty.acCntyCode, "Items");
   LogMsg("Create Items file %s", acItemsFile);
   fdItems = fopen(acItemsFile, "w");
   if (fdItems == NULL)
   {
      LogMsg("***** Error creating Items file: %s\n", acItemsFile);
      return -4;
   }

   // Merge loop 
   while (!feof(fdIn))
   {
      pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdIn);
      if (!pTmp)
         break;

      // Create new R01 record
      iRet = Sbd_ParseTaxBase(acBase, acDetail, acRec);
      if (!iRet)
      {
         // Create TaxBase record
         Tax_CreateTaxBaseCsv(acOutbuf, (TAXBASE *)&acBase);

         // Output record			
         lBase++;
         fputs(acOutbuf, fdBase);

         if (acDetail[0] >= '0')
         {
            // Create TaxDetail record
            Tax_CreateDetailCsv(acRec, (TAXDETAIL *)&acDetail);
            fputs(acRec, fdItems);
         }
      } else
      {
         LogMsg("---> Drop record %d [%.40s]", lCnt, acRec); 
         iDrop++;
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdIn)
      fclose(fdIn);
   if (fdBase)
      fclose(fdBase);
   if (fdItems)
      fclose(fdItems);

   LogMsg("Total records processed:   %u", lCnt);
   LogMsg("    base records output:   %u", lBase);

   if (iDrop > 5)
   {
      LogMsg("Total records dropped: %u", iDrop);
      return -5;
   }

   // Import into SQL
   if (bImport && lBase > 100000)
   {
      iRet = doTaxImport(myCounty.acCntyCode, TAX_BASE);
   } else
      iRet = 0;

   return iRet;
}

/***************************** Sbd_ParseTaxDelqExtr **************************
 *
 * Create import file from tax Delq extract Delq_Extr.txt and import into SQL 
 *
 * Return 0 if success.
 *
 *****************************************************************************/

int Sbd_ParseTaxDelqExtr(char *pOutbuf, char *pInbuf)
{
   int      iTmp;
   double	dIntr, dTaxTotal;

   TAXDELQ  *pOutRec = (TAXDELQ *)pOutbuf;
   DELQREC  *pInRec  = (DELQREC *)pInbuf;

   // Ignore redeemed and unsecured.  Process secured records only
   if (pInRec->DelqType != 'S' || pInRec->RedempStatus == 'R')
      return -1;

   // Clear output buffer
   memset(pOutbuf, 0, sizeof(DELQREC));

   // No tax?  Ignore it
   dTaxTotal = atofn(pInRec->RedempAmt, 10);
   if (!dTaxTotal)
      return -2;

   // APN
   memcpy(pOutRec->Apn, pInRec->OrgApn, TSIZ_APN);

   // Tax Year
   //memcpy(pOutRec->TaxYear, pInRec->Bill.AsmntYear, 4);
   //taxYear = atol(pOutRec->TaxYear);

   // Defaulted date
   memcpy(pOutRec->Def_Date, pInRec->DefaultDate, 8);

   // Bill Number 
   memcpy(pOutRec->BillNum, &pInRec->BillNum[2], 9);


#ifdef _DEBUG
   //if (memcmp(pInRec->PaymentStatus, "00", 2))
   //   iTmp = 0;
#endif

   // Fee & interest
   dIntr = atofn(pInRec->TotalFeeIntrAmt, 10);
   if (dIntr > 0)
      dTaxTotal += dIntr;

   if (pInRec->RedempStatus > ' ')
      iTmp = 0;

   //iTmp = atoin(pInRec->Bill.PaymentStatus, TSIZ_PAIDSTATUS);
   //if (!iTmp)
   //{  // Nothing paid
   //   if (ChkDueDate(1, taxYear))
   //   {
   //      dDue1 = dTax1;
   //      dDue2 = dTax2;
   //      pOutRec->Inst1Status[0] = TAX_BSTAT_PASTDUE;
   //   } else
   //      pOutRec->Inst1Status[0] = TAX_BSTAT_UNPAID;
   //} else if (iTmp == 1)
   //{  // Inst1 paid
   //   pOutRec->Inst1Status[0] = TAX_BSTAT_PAID;
   //   sprintf(pOutRec->PaidAmt1, "%.2f", dTax1);
   //   if (ChkDueDate(2, taxYear))
   //   {
   //      dDue2 = dTax2;
   //      pOutRec->Inst2Status[0] = TAX_BSTAT_PASTDUE;
   //   } else
   //      pOutRec->Inst2Status[0] = TAX_BSTAT_UNPAID;
   //} else if (iTmp == 2)
   //{  // Both paid
   //   pOutRec->Inst1Status[0] = TAX_BSTAT_PAID;
   //   pOutRec->Inst2Status[0] = TAX_BSTAT_PAID;
   //   sprintf(pOutRec->PaidAmt1, "%.2f", dTax1);
   //   sprintf(pOutRec->PaidAmt2, "%.2f", dTax2);
   //} else
   //{
   //   LogMsg("??? Questionable paid status on APN= %s:%.2d", pOutRec->Apn, iTmp);
   //}

#ifdef _DEBUG
   //if ((int)dTaxTotal != (int)(dTax1+dTax2))
   //   iRet = 0;
#endif

   pOutRec->DelqStatus[0] = TAX_STAT_UNPAID;
   pOutRec->isSecd[0] = '1';
   pOutRec->isSupp[0] = '0';

   return 0;
}

/***************************** Sbd_ParseTaxDelq ******************************
 *
 * Create import file from tax Delq TR345PC.txt 
 *
 * Return 0 if success.
 *
 *****************************************************************************/

 int Sbd_ParseTaxDelq(char *pOutbuf, char *pInbuf)
{
   int      iTmp, iYear=0;
   double	dIntr, dTaxTotal;
   char     sTmp[256], *pTmp;

   TAXDELQ  *pOutRec = (TAXDELQ *)pOutbuf;
   SCNDELQ  *pInRec  = (SCNDELQ *)pInbuf;

   // Ignore redeemed and unsecured.  Process secured records only
   if (pInRec->DelqType != 'S' || pInRec->RedempStatus == 'R')
      return -1;

   // Keep DELQ record only
   if (memcmp(pInRec->RecordType, "SCNDELQ", 7))
      return -2;

   // Clear output buffer
   memset(pOutbuf, 0, sizeof(TAXDELQ));

   // No tax?  Ignore it
   dTaxTotal = atofn(pInRec->RedempAmt, 13);
   if (!dTaxTotal)
      return -2;

   // APN
   memcpy(pOutRec->Apn, pInRec->ParcelNumber, TSIZ_APN);

   // Defaulted date
   pTmp = dateConversion(pInRec->Delq_Tax_Default_Date, sTmp, YYMMDD2);
   if (pTmp)
   {
      memcpy(pOutRec->Def_Date, sTmp, 8);
      iYear = atoin(sTmp, 4);
      sprintf(sTmp, "%d", iYear);
      memcpy(pOutRec->TaxYear, sTmp, 4);
   }
   if (iYear < 2000)
      return 1;

   // Bill Number 
   iTmp = atoin(pInRec->BillNumber, 9);
   if (iTmp > 0)
      memcpy(pOutRec->BillNum, &pInRec->BillNumber, 9);

#ifdef _DEBUG
   //if (memcmp(pInRec->PaymentStatus, "00", 2))
   //   iTmp = 0;
#endif

   // Fee & interest
   dIntr = atofn(pInRec->TotalFeeIntrAmt, 10);
   if (dIntr > 0)
      dTaxTotal += dIntr;

   // Default Amt
   iTmp = sprintf(sTmp, "%.2f", dTaxTotal);
   memcpy(pOutRec->Def_Amt, sTmp, iTmp);

   pOutRec->isDelq[0] = '1';
   iTmp = atoin(pInRec->PaymentStatus, TSIZ_PAIDSTATUS);
   if (!iTmp)
   {  // Nothing paid
      pOutRec->DelqStatus[0] = TAX_STAT_UNPAID;
   } else if (iTmp > 0)
   {  // Inst1 paid
      //if (iTmp == atoin(pInRec->Pay_Plan_Nbr_Of_Insts, 2))
      //   pOutRec->DelqStatus[0] = TAX_STAT_REDEEMED; // 0134071560000
      //else 
      if (pInRec->Special_Pay_Plan_Ind == 'P')
      {
         pOutRec->DelqStatus[0] = TAX_STAT_ONPAYMENT;
         pOutRec->isDelq[0] = '0';
      } else
         pOutRec->DelqStatus[0] = TAX_BSTAT_PAID;
   } else if (iTmp == 2)
   {  // Both paid
      pOutRec->DelqStatus[0] = TAX_BSTAT_PAID;
   } else if (iTmp > 0)
   {
      LogMsg("??? Questionable paid status on APN= %s:%.2d", pOutRec->Apn, iTmp);
   }

#ifdef _DEBUG
   //if ((int)dTaxTotal != (int)(dTax1+dTax2))
   //   iRet = 0;
#endif

   pOutRec->isSecd[0] = '1';
   pOutRec->isSupp[0] = '0';

   return 0;
}

int Sbd_Load_TaxDelq(bool bImport)
{
   char     *pTmp, acDelq[1024], acRec[512],
            acDelqFile[_MAX_PATH], acInFile[_MAX_PATH], acOutbuf[MAX_RECSIZE];
            
   int      iRet, iDrop=0;
   long     lDelq=0, lCnt=0;
   FILE     *fdDelq, *fdIn;
   TAXDELQ  *pDelq = (TAXDELQ *)acDelq;

   LogMsg0("Loading Tax Delq for SBD");

   // Open input file
   iRet = GetIniString(myCounty.acCntyCode, "TaxDelq", "", acInFile, _MAX_PATH, acIniFile);
   if (!iRet || _access(acInFile, 0))
   {
      LogMsg("*** Delq tax file is not available.  Skip loading Delq data");
      return 1;
   }

   LogMsg("Open Delq tax file %s", acInFile);
   lLastTaxFileDate = getFileDate(acInFile);
   fdIn = fopen(acInFile, "r");
   if (fdIn == NULL)
   {
      LogMsg("***** Error opening tax file: %s\n", acInFile);
      return -2;
   }  

   // Open Output file
   NameTaxCsvFile(acDelqFile, myCounty.acCntyCode, "Delq");
   LogMsg("Create Delq file %s", acDelqFile);
   fdDelq = fopen(acDelqFile, "w");
   if (fdDelq == NULL)
   {
      LogMsg("***** Error creating output file: %s\n", acDelqFile);
      return -4;
   }

   // Merge loop 
   while (!feof(fdIn))
   {
      pTmp = fgets((char *)&acRec[0], 512, fdIn);
      if (!pTmp)
         break;

      // Create new R01 record
      iRet = Sbd_ParseTaxDelq(acDelq, acRec);
      if (!iRet)
      {
         // Create TaxDelq record
         Tax_CreateDelqCsv(acOutbuf, (TAXDELQ *)&acDelq);

         // Output record			
         lDelq++;
         fputs(acOutbuf, fdDelq);
      } else
      {
         //LogMsg("---> Drop record %d [%.40s]", lCnt, acRec); 
         iDrop++;
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdIn)
      fclose(fdIn);
   if (fdDelq)
      fclose(fdDelq);

   printf("\n");
   LogMsgD("Total records processed:   %u", lCnt);
   LogMsg("    Delq records output:   %u", lDelq);

   if (iDrop > 5)
      LogMsg("Total records dropped: %u", iDrop);

   // Import into SQL
   if (bImport)
   {
      iRet = doTaxImport(myCounty.acCntyCode, TAX_DELINQUENT);
   } else
      iRet = 0;

   return iRet;
}

/******************************* Sbd_ExtrProp8 ******************************
 *
 * Extract prop 8
 *
 ****************************************************************************/

int Sbd_ExtrProp8(char *pProp8File)
{
   char  *pTmp, acBuf[MAX_RECSIZE], acRollRec[MAX_RECSIZE];
   long  lCnt=0, iProp8Cnt=0;
   FILE  *fdOut;

   LogMsg0("Extract Prop8 flag");

   // Open roll file
   fdProp8 = fopen(pProp8File, "r");
   if (fdProp8 == NULL)
   {
      LogMsg("***** Error opening Prop8 file: %s\n", pProp8File);
      return 2;
   }

   // Open Output file
   sprintf(acProp8File, acProp8Tmpl, myCounty.acCntyCode, myCounty.acCntyCode, myCounty.acYearAssd);
   LogMsg("Create prop8 output file %s", acProp8File);
   fdOut = fopen(acProp8File, "w");
   if (fdOut == NULL)
   {
      LogMsg("***** Error opening Prop8 extract file: %s\n", acProp8File);
      return 4;
   }

   // print header
   fputs("APN,Prop8_Flg\n", fdOut);

   // Merge loop
   while (!feof(fdProp8))
   {
      // Get roll rec
      pTmp = fgets(acRollRec, MAX_RECSIZE, fdProp8);
      if (!pTmp)
         break;

      if (acRollRec[iApnLen] == ',')
      {
         acRollRec[iApnLen] = 0;
         sprintf(acBuf, "%s,Y\n", acRollRec);

         fputs(acBuf, fdOut);
         iProp8Cnt++;
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdProp8)
      fclose(fdProp8);
   if (fdOut)
      fclose(fdOut);

   LogMsgD("\nTotal prop8 records output: %u", iProp8Cnt);
   LogMsgD("Total records processed:    %u\n", lCnt);

   return 0;
}

int Sbd_ExtractTaxStatus(char *pRollFile)
{
   char     *pTmp, acBuf[256], acRec[MAX_RECSIZE];
   char     acOutFile[_MAX_PATH], acApn[16];

   int      iRet, lCnt=0;
   FILE     *fdOut;

   LogMsg0("Extract Tax Status from %s", pRollFile);

   // Open roll file
   LogMsg("Open input file %s", pRollFile);
   fdRoll = fopen(pRollFile, "r");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", pRollFile);
      return -1;
   }

   // Open Output file
   sprintf(acOutFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "TaxStat");
   LogMsg("Create output file %s", acOutFile);
   fdOut = fopen(acOutFile, "w");
   if (fdOut == NULL)
   {
      LogMsg("***** Error creating output file: %s\n", acOutFile);
      return -1;
   }

   // Merge loop
   while (!feof(fdRoll))
   {
      pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdRoll);
      if (!pTmp)
         break;
      lCnt++;

      // Parse input string
      iTokens = ParseStringIQ(acRec, cDelim, MAX_FLD_TOKEN, apTokens);
      if (iTokens < SBD_ROLL_FLDS)
      {
         LogMsg("***** Error: bad input record for APN=%s-%s-%s-%s%s", 
            apTokens[SBD_ROLL_PAR_BOOK], apTokens[SBD_ROLL_PAR_PAGE], 
            apTokens[SBD_ROLL_PAR_LINE], apTokens[SBD_ROLL_PAR_TYPE], apTokens[SBD_ROLL_PAR_SEQ]);
         break;
      }

      iRet = sprintf(acApn, "%s%s%s%s%s", apTokens[SBD_ROLL_PAR_BOOK], apTokens[SBD_ROLL_PAR_PAGE], 
         apTokens[SBD_ROLL_PAR_LINE], apTokens[SBD_ROLL_PAR_TYPE], apTokens[SBD_ROLL_PAR_SEQ]);
      if (iRet > iApnLen)
         LogMsg("*** Invalid APN: %s", acApn);

      sprintf(acBuf, "%13s %s\n", acApn, apTokens[SBD_ROLL_TAX_STAT]);
      fputs(acBuf, fdOut);

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdRoll)
      fclose(fdRoll);
   if (fdOut)
      fclose(fdOut);

   LogMsg("Total records output: %u", lCnt);

   return 0;
}

/******************************* Sbd_ExtrProp8 ******************************
 *
 * Extract tax items from PI347FTP.DAT
 *
 ****************************************************************************/

int Sbd_ExtrTaxItems(char *pInFile, char *pOutFile)
{
   char     *pTmp, acBuf[256], acRec[MAX_RECSIZE], acOutFile[_MAX_PATH], acApn[32], acCode[16], acDesc[256], acTmp[256];
   bool     bPageStart;
   int      iCnt=0, iPage=0;
   double   dAmt, dCost;
   FILE     *fdIn, *fdOut;
   TAXAGENCY  *pResult;

   LogMsg0("Extract Tax Items from %s", pInFile);

   // Open roll file
   LogMsg("Open tax file: %s", pInFile);
   fdIn = fopen(pInFile, "r");
   if (fdIn == NULL)
   {
      LogMsg("***** Error opening tax file: %s\n", pInFile);
      return -1;
   }

   // Open Output file
   sprintf(acOutFile, "%s\\%s\\TaxItems.tmp", acTmpPath, myCounty.acCntyCode);
   LogMsg("Create output file %s", acOutFile);
   fdOut = fopen(acOutFile, "w");
   if (fdOut == NULL)
   {
      LogMsg("***** Error creating output file: %s\n", acOutFile);
      return -1;
   }
   bPageStart = false;

   // Merge loop
   while (!feof(fdIn))
   {
      pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdIn);
      if (!pTmp)
         break;

      // Check for BOP
      if (*pTmp == '1')
      {
         bPageStart = false;
         fgets((char *)&acRec[0], MAX_RECSIZE, fdIn);
         iPage = atol(&acRec[127]);
         // Drop 1 line
         fgets((char *)&acRec[0], MAX_RECSIZE, fdIn);
         // Get Agency
         fgets((char *)&acRec[0], MAX_RECSIZE, fdIn);
         // Strip off time
         if (pTmp = strstr(acRec, "   "))
            *pTmp = 0;
      }

      // Parse input string
      if (!bPageStart && !memcmp(&acRec[1], "AGENCY:", 7))
      {
         bPageStart = true;
         memcpy(acCode, &acRec[9], 4);
         strcpy(acDesc, &acRec[14]);
         fgets((char *)&acRec[0], MAX_RECSIZE, fdIn);
         pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdIn);
         // Check for Account:
         if (!memcmp(&acRec[84], "ACCOUNT:", 8))
         {
            memcpy(&acCode[4], &acRec[93], 4);
            acCode[8] = 0;
         } else
         {
            LogMsg("***** Page misaligned: %d", iPage);
            break;
         }

         // Check for known code
         pResult = findTaxAgency(acCode);
         if (pResult)
         {
            strcpy(acDesc, pResult->Agency);
         } else
         {
            LogMsg0("*** Unknown Agency: %s-%s (%d)", acCode, acDesc, iPage);
         }
      } else if (isdigit(acRec[5]))
      {
         // Get APN
         memcpy(acApn, &acRec[5], 17);
         acApn[17] = 0;
         remChar(acApn, ' ');
         acRec[75] = 0;
         strcpy(acTmp, &acRec[60]);
         remChar(acTmp, ',');
         dAmt = atof(acTmp);
         strcpy(acTmp, &acRec[89]);
         remChar(acTmp, ',');
         dCost = atof(acTmp);

         //dAmt  = dollar2Double(&acRec[60]);
         //dCost = dollar2Double(&acRec[89]);
         dAmt += dCost;
         sprintf(acBuf, "%s|%s|%s|%.2s|%.2f\n", acApn, acCode, acDesc, &acRec[39], dAmt);
         fputs(acBuf, fdOut);
      }

      if (!(++iCnt % 1000))
         printf("\r%u", iCnt);
   }

   // Close files
   if (fdIn)
      fclose(fdIn);
   if (fdOut)
      fclose(fdOut);

   LogMsg("Number of pages read:     %u", iPage);
   LogMsg("          records output: %u", iCnt);

   if (iPage > 51000)
   {
      // Sort output file
      iCnt = sortFile(acOutFile, pOutFile, "S(#1,C,A,#2,C,A,#4,C,A) F(TXT) DEL(124) DUPOUT(#1,#2,#4) ");
   }

   return 0;
}

/*********************************** loadSbd ********************************
 *
 * Options:
 *    -CSBD -L -Xa -Xd -Mg -Ms -Mr (load LDR)
 *    -CSBD -U [-O] [-Mg] [-Ms|-Xs] [-X8] [-Xa] [-XC] [-Ma] [-Mr] [-Dn] (load update)
 *    -CSBD -T [-O] (load tax)
 *    -CSBD -Xf (extract final value)
 *
 ****************************************************************************/

int loadSbd(int iSkip)
{
   int   iRet=0;
   char  acTmpFile[_MAX_PATH], acXferFile[_MAX_PATH], *pTmp;
   char  acOwnerFile[_MAX_PATH], acExtrFile[_MAX_PATH];

   iApnLen = myCounty.iApnLen;

   // Load tax
   if (iLoadTax == TAX_LOADING)                    // -T or -Lt
   {
      iRet = GetIniString(myCounty.acCntyCode, "TaxDist", "", acTmpFile, _MAX_PATH, acIniFile);
      if (iRet > 0)
         iRet = LoadTaxCodeTable(acTmpFile);

      if (iRet > 0)
      {
         // Load Tax base 
         iRet = Sbd_Load_TaxBase(bTaxImport);
         if (!iRet)
         {
            // Load Delq
            iRet = Sbd_Load_TaxDelq(bTaxImport);
            if (!iRet)
               iRet = updateDelqFlag(myCounty.acCntyCode);

            // Load Tax detail & Agency
            GetIniString(myCounty.acCntyCode, "AsmtExtr", "", acTmpFile, _MAX_PATH, acIniFile);
            if (!_access(acTmpFile, 0))
            {
               GetIniString(myCounty.acCntyCode, "TaxItemExtr", "", acExtrFile, _MAX_PATH, acIniFile);
               iRet = Sbd_ExtrTaxItems(acTmpFile, acExtrFile);
               if (!iRet)
                  iRet = Sbd_Load_TaxDetailEx(acExtrFile, bTaxImport);
            } else
            {
               LogMsg("***** Missing AsmtExtr file.  Please check INI file.");
               return -1;
            }
         }

         // Loading PI344.txt (2022)
         //iRet = Sbd_Load_TaxDetail(bTaxImport);
      }
   }

   // Extract final value
   if (lOptExtr & EXTR_FVAL)                       // -Xf
      Sbd_ExtrVal();

   // Exit if load/update tax only
   if (!lOptExtr && (!iLoadFlag || iLoadTax))
      return iRet;

   // Extract Prop8 parcels
   if (lOptProp8 & MYOPT_EXT)                      // -X8 Extract prop8 flag to text file
   {
      GetIniString(myCounty.acCntyCode, "Prop8File", "", acTmpFile, _MAX_PATH, acIniFile);
      iRet = Sbd_ExtrProp8(acTmpFile);
   }

   // Extract owner names
   if (lOptExtr & EXTR_OWNER)                      // -Xo
   {
      LogMsg0("Extract owner for bulk client");
      GetIniString(myCounty.acCntyCode, "OwnerFile", "", acOwnerFile, 255, acIniFile);
      if (_access(acOwnerFile, 0))
      {
         LogMsg("*** Missing Owner file.  Skip extracting owner.");
      } else
      {
         // Sort owner file
         sprintf(acTmpFile, "%s\\%s\\Sbd_Owner.txt", acTmpPath, myCounty.acCntyCode);
         LogMsg("Sort owner file %s to %s", acOwnerFile, acTmpFile);
         iRet = sortFile(acOwnerFile, acTmpFile, "S(1,13,C,A,20,4,C,D,14,2,C,D,17,2,C,D) F(TXT) ");

         iRet = GetIniString(myCounty.acCntyCode, "OwnerExtr", "", acExtrFile, 255, acIniFile);
         if (iRet > 0)
            iRet = Sbd_ExtractOwnerCsv(acTmpFile, acExtrFile);

         sprintf(acExtrFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "Owner");
         iRet = Sbd_ExtractOwners(acTmpFile, acExtrFile);
      }
   }

   // Extract legal.  Use as needed
   if (iLoadFlag & EXTR_DESC)                      // -Xd
      iRet = Sbd_LegalExtr();

	// Extract XC Char file - San Bernadino, CA XC.txt
   if (lOptExtr & EXTR_XCHAR)                      // -XC
   {
      iRet = Sbd_ExtrChar(acCharFile);
      if (iRet > 0 )
         iRet = 0;
   }

   // Extract chars from new char file
   if (iLoadFlag & EXTR_ATTR)                      // -Xa
   {
Reload_Char:
      // Load lookup table
      //iRet = LoadLUTable(acLookupTbl, "[Quality]", NULL, MAX_ATTR_ENTRIES);
      //if (!iRet)
      //{
      //   LogMsg("*** Error Looking for table [Quality] in %s", acLookupTbl);
      //   return -1;
      //}

      // 08/12/2024 
      iRet = Sbd_ConvStdChar(acCharFile);
      //iRet = Sbd_ConvStdChar1(acCharFile);
   } else if (iLoadFlag & LOAD_UPDT)
   {
      iRet = chkFileDate(acCharFile, acCChrFile);
      if (iRet == 1)
         goto Reload_Char;
   }

   // Extract sales 
   if (iLoadFlag & EXTR_SALE)                      // -Xs
   {
Reload_Sale:
      LogMsg0("Extract %s sale history file", myCounty.acCntyCode);
      sprintf(acXferFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "NewSale");
      if (!_access(acXferFile, 0))
         DeleteFile(acXferFile);

      iRet = Sbd_ExtrSale(SALE_TYPE_SFR, acXferFile);
      iRet |= Sbd_ExtrSale(SALE_TYPE_MH, acXferFile);
      iRet |= Sbd_ExtrSale(SALE_TYPE_LAND, acXferFile);
      iRet |= Sbd_ExtrSale(SALE_TYPE_DOCK, acXferFile);
      iRet |= Sbd_ExtrSale(SALE_TYPE_OTH, acXferFile);
      iRet |= Sbd_ExtrMPSale(acXferFile);

      if (!iRet)
      {
         // Combine all sales to cum file
         if (!_access(acCSalFile, 0))
            sprintf(acTmpFile, "%s+%s", acXferFile, acCSalFile);
         else
            strcpy(acTmpFile, acXferFile);

         // Sort/merge sale file - APN (asc), Saledate (asc), DocNum (asc)
         char acSortCtl[256], acSortFile[_MAX_PATH];
         sprintf(acSortCtl, "S(1,%d,C,A,27,8,C,A,15,%d,C,A,57,10,C,D) DUPO(1,34)", iApnLen, SALE_SIZ_DOCNUM);
         sprintf(acSortFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "srt");
         iRet = sortFile(acTmpFile, acSortFile, acSortCtl);
         if (iRet > 0)
         {
            sprintf(acTmpFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "sav");
            if (!_access(acTmpFile, 0))
               DeleteFile(acTmpFile);

            if (!_access(acCSalFile, 0))
            {
               LogMsg("Rename %s to %s", acCSalFile, acTmpFile);
               iRet = rename(acCSalFile, acTmpFile);
            }
            // Rename srt to SLS file
            LogMsg("Rename %s to %s", acSortFile, acCSalFile);
            iRet = rename(acSortFile, acCSalFile);
            if (!iRet)
               iLoadFlag |= MERG_CSAL;
         }
      }
   } else if (iLoadFlag & LOAD_UPDT)
   {
      GetIniString(myCounty.acCntyCode, "SfrSale", "", acSaleFile, _MAX_PATH, acIniFile);    
      iRet = chkFileDate(acSaleFile, acCSalFile);
      if (iRet == 1)
         goto Reload_Sale;
   }

   // Extract NDC recorder sale 
   if (iLoadFlag & EXTR_NRSAL)                     // -Xn
   {
      iRet = GetIniString(myCounty.acCntyCode, "NdcSale", "", acSaleFile, _MAX_PATH, acIniFile);
      if (!_access(acSaleFile, 0))
      {
         iRet = NR_CreateSCSale(myCounty.acCntyCode, acSaleFile, iApnLen);
         if (iRet)
            return iRet;
      } else
      {
         LogMsg("*** Sale file not available.  Please verify: %s", acSaleFile);
         return -1;
      }
   }

   if (iLoadFlag & (LOAD_LIEN|EXTR_LIEN))  
   {
      LogMsg0("Rebuild LDR file");

      // Rebuild CSV file to fix broken lines
      sprintf(acTmpFile, "%s\\SBD\\SBD_LDR.txt", acTmpPath);
      iRet = RebuildCsv_IQ(acRollFile, acTmpFile, '~', SBD_ROLL_FLDS);
      if (iRet < 800000)
         return iRet;

      // Sort LDR file
      LogMsg("Sort LDR file");
      if (pTmp = strrchr(acRollFile, '.'))
         strcpy(pTmp, ".srt");
      else
         strcat(acRollFile, ".srt");
      iRet = sortFile(acTmpFile, acRollFile, "S(#1,C,A,#2,C,A,#3,C,A,#4,C,A,#5,C,A) DEL(126) OMIT(1,1,C,GT,\"9\") F(TXT) ");
   }

   // Extract Lien value
   if (iLoadFlag & EXTR_LIEN)                      // -Xl
      iRet = Sbd_ExtrLien();

   if (iLoadFlag & LOAD_LIEN)                      // -L
   {
      LogMsg0("Load %s Lien file", myCounty.acCntyCode);

      // Create Lien file
      iRet = Sbd_Load_LDR(iSkip);
   } else if (iLoadFlag & LOAD_UPDT)               // -U
   {
      LogMsg0("Load %s roll update file", myCounty.acCntyCode);

      // Sort roll file
      LogMsg("Sort roll file: %s", acRollFile);
      strcpy(acTmpFile, acRollFile);
      if (pTmp = strrchr(acRollFile, '.'))
         strcpy(pTmp, ".srt");
      else
         strcat(acRollFile, ".srt");
      iRet = sortFile(acTmpFile, acRollFile, "S(#2,C,A) DEL(126) OMIT(1,1,C,GT,\"9\") F(TXT) ");

      // Load update roll
      iRet = Sbd_Load_Roll(iSkip);
   }

   if (!iRet && (iLoadFlag & MERG_ATTR))           // -Ma
   {
      // This function return number of records updated
      iRet = Sbd_MergeChar(iSkip);
      if (iRet > 0)
         iRet = 0;
   }

   // Apply cum sale file to R01
   if (!iRet && (iLoadFlag & MERG_CSAL) )          // -Ms
   {
      // Apply Sbd_Sale.sls to R01 file
      sprintf(acCSalFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "sls");
      iRet = ApplyCumSale(iSkip, acCSalFile, false, SALE_USE_SCUPDXFR, DONT_CHK_DOCNUM|CLEAR_OLD_SALE);
   }

   // Apply NDC sale file to R01
   if (!iRet && (iLoadFlag & UPDT_XSAL))           // -Mn
   {
      // Apply Sbd_Ash.sls to R01 file
      GetIniString("Data", "ASH_File", acESalTmpl, acESalTmpl, _MAX_PATH, acIniFile);
      sprintf(acCSalFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "sls");
      iRet = ApplyCumSaleNR(iSkip, acCSalFile, SALE_USE_SCUPDXFR, 0);
      if (!iRet)
         iLoadFlag |= LOAD_UPDT;
   }

   // Merge GrGr
   if (!iRet && (iLoadFlag & MERG_GRGR))           // -Mg
   {
      GetIniString(myCounty.acCntyCode, "XSalFile", "", acXferFile, _MAX_PATH, acIniFile);    
      iRet = Sbd_MergeDocExt(myCounty.acCntyCode, acXferFile);
   }
              
   //if (!iRet && (iLoadFlag & EXTR_IGRGR))          // -Mgi or -Ig
   //{
   //   sprintf(sDbName, "UPD%d", lLienYear);
   //   GetIniString(myCounty.acCntyCode, "XSalFile", "", acXferFile, _MAX_PATH, acIniFile);    
   //   GetIniString("Data", "SqlGrgrFile", "", acTmpFile, _MAX_PATH, acIniFile);
   //   sprintf(acRawFile, acTmpFile, sDbName, myCounty.acCntyCode);
   //   iRet = createWebSaleImport(myCounty.acCntyCode, acXferFile, acRawFile, 2, false, (IDX_TBL5 *)&SBD_DocCode[0]);
   //   if (iRet > 0)
   //      iRet = doSaleImport(myCounty.acCntyCode, sDbName, acRawFile, 1);
   //   iLoadFlag ^= EXTR_IGRGR;    // Remove flag to avoid import again in LoadOne.cpp
   //}

   // Update Usecode
   if (iLoadFlag & UPDT_SUSE)
      iRet = updateR01StdUse(myCounty.acCntyCode, iSkip);

   // Fix TRA
   if (!iRet && (lOptMisc & M_OPT_FIXTRA))
      iRet = PQ_FixR01(myCounty.acCntyCode, PQ_FIX_TRA, 0, true);

   return iRet;
}
