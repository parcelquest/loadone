/**************************************************************************
 *
 * Notes:
 *    - May need to handle situs strNo specifically for IMP.
 *    - IMP is annual county, no need to create update file
 *    - Look for new Imp_Exe.dat when loading LDR
 *    - Output file must be sorted.  If not, verification will pull up wrong record.
 *
 * Options:
 *    -CIMP -L (load lien)
 *    -CIMP -U (load update)
 *
 * 03/14/2008 1.5.6     Use standard function to update usecode
 * 03/24/2008 1.5.7.2   Format IMAPLINK.
 * 07/11/2008 8.0.3     Modify Imp_MergeExe() to handle new exemption file format
 *                      Modify Imp_MergeRoll() to handle new LDR format, adding Taxability code
 *                      Modify Imp_MergeAdr() to make situs city more accurate.
 *                      - First check to see if S_ADDR2 is provided
 *                      - Second check to see if street name and number match mailings, if so use mail city
 *                      - Third check to see if city code is available, if so translate it
 *                      - Last match TRA with city table.  This method is only 90% accurate.
 * 07/14/2008 8.0.4     Change load logic since LDR file cannot be sorted.  We load it
 *                      first, sort it, then merge EXE file later.  This is the only way 
 *                      we can match 100% of the EXE data.
 * 12/08/2008 8.4.8     Copy TRA as is (should be 6 bytes).  Append USE_CODE2 to 
 *                      USE_CODE1 for LAND_USE field.  (See Grant's email 17/07/2008)
 * 07/13/2009 9.1.2     Modify Imp_MergeExe() & Imp_MergeRoll() to work with new 2009 layout.
 *                      Sort Exemption file before processing.
 * 02/06/2010 9.4.0     Extending LEGAL to LEGAL1
 * 07/15/2010 10.1.3    Replace myGetStrDC() with myGetStrEQ() in Imp_Load_LDR() so it will 
 *                      work with 2010 LDR data file.
 * 07/29/2011 11.0.5    Add S_HSENO.  Modify Imp_MergeExe() due to data format change.
 * 01/28/2012 11.6.4    Fix bug in Imp_MergeRoll() where null char is put in OFF_STATUS.
 *
 ***************************************************************************/

#include "stdafx.h"
#include "Prodlib.h"
#include "CountyInfo.h"
#include "R01.h"
#include "RecDef.h"
#include "Tables.h"
#include "SaleRec.h"
#include "Logs.h"
#include "LoadOne.h"
#include "Utils.h"
#include "XlatTbls.h"
#include "doSort.h"
#include "doOwner.h"
#include "formatApn.h"
#include "MergeImp.h"
#include "UseCode.h"
#include "LOExtrn.h"

static   FILE  *fdExe;
long     lExeMatch, lExeSkip;

/******************************** Imp_MergeOwner *****************************
 *
 * Return 0 if successful, >0 if error
 *
 *****************************************************************************/

void Imp_MergeOwner(char *pOutbuf, char *pNames)
{
   int   iTmp, iTmp1;
   char  acOwners[128], acTmp[128], acSave[64], *pTmp, *pTmp1;
   char  acName1[64], acName2[64], acVesting[8];

   OWNER myOwner;

   // Clear output buffer if needed
   memset(pOutbuf+OFF_NAME1, ' ', SIZ_NAME1+SIZ_NAME2);
   memset(pOutbuf+OFF_VEST, ' ', SIZ_VEST);
   memset(pOutbuf+OFF_NAME_SWAP, ' ', SIZ_NAME_SWAP);
   memset(pOutbuf+OFF_NAME_TYPE1, ' ', SIZ_NAME_TYPE1);
   memset(pOutbuf+OFF_NAME_TYPE2, ' ', SIZ_NAME_TYPE2);

   // Initialize
   memset((void *)&myOwner, 0, sizeof(OWNER));

   // Remove multiple spaces
   pTmp = strcpy(acOwners, pNames);
   iTmp = 0;
   while (*pTmp)
   {
      // skip unwanted chars at beginning of name
      if (*pTmp < '1' && iTmp == 0)
      {
         pTmp++;
         continue;
      }

      acTmp[iTmp++] = *pTmp;
      // Remove multi-space
      if (*pTmp == ' ')
         while (*pTmp == ' ') pTmp++;
      else
         pTmp++;

      // Remove '.' too
      if (*pTmp == '.')
         pTmp++;
   }
   if (acTmp[iTmp-1] == ' ')
      iTmp -= 1;
   acTmp[iTmp] = 0;

   // Filter out words
   if (pTmp=strstr(acTmp, " 1/2 INT"))
   {
      *pTmp = 0;
      if (*(pTmp+8))
         strcat(acTmp, pTmp+8);
   }

   // Drop everything from these words - DO NOT MOVE THIS SECTION
   if (pTmp=strstr(acTmp, " AKA "))
      *pTmp = 0;
   else if (pTmp=strstr(acTmp, " SUCCESSOR"))
      *pTmp = 0;
   else if (pTmp=strstr(acTmp, " SUCC "))
      *pTmp = 0;
   else if (pTmp=strstr(acTmp, " SUC TR"))
      *pTmp = 0;
   else if (pTmp=strstr(acTmp, " CO TR"))
      *pTmp = 0;
   else if (pTmp=strstr(acTmp, " CO-TR"))
      *pTmp = 0;
   else if (pTmp=strstr(acTmp, " TRUSTE"))
      *pTmp = 0;
   else if (pTmp=strstr(acTmp, " TTEE"))
      *pTmp = 0;
   else if (pTmp=strstr(acTmp, " TRSTEE"))
      *pTmp = 0;
   else if (pTmp=strstr(acTmp, " INT ETAL"))
      *pTmp = 0;
   else if (pTmp=strstr(acTmp, " TR "))
   {
      *pTmp = 0;
      if (pTmp1 = strchr(pTmp+3, '&'))
         strcat(acTmp, pTmp+3);

   } else if (pTmp=strstr(acTmp, " TRS "))
   {
      *pTmp = 0;
      if (pTmp1 = strchr(pTmp+4, '&'))
         strcat(acTmp, pTmp+4);
   } else if (pTmp=strstr(acTmp, " INTERVIVOS "))
      *pTmp = 0;
   else if (pTmp=strstr(acTmp, " EST OF"))
      *pTmp = 0;

   // Save data to append later
   if (pTmp1=strstr(acTmp, " TRUST"))
   {
      if (pTmp=strstr(acTmp, " FAM"))
      {
         strcpy(acSave, pTmp);
         *pTmp = 0;
      } else if (pTmp=strstr(acTmp, " REVOCABLE"))
      {
         strcpy(acSave, pTmp);
         *pTmp = 0;
      } else if (pTmp=strstr(acTmp, " INDIVIDUAL"))
      {
         strcpy(acSave, pTmp);
         *pTmp = 0;
      } else if (pTmp=strstr(acTmp, " LIV"))
      {
         strcpy(acSave, pTmp);
         *pTmp = 0;
      } else if (pTmp=strstr(acTmp, " AMENDED "))
      {
         strcpy(acSave, pTmp);
         *pTmp = 0;
      } else if (pTmp=strstr(acTmp, " GBAC "))
      {
         strcpy(acSave, pTmp);
         *pTmp = 0;
      } else
      {
         strcpy(acSave, --pTmp1);
         *pTmp1 = 0;
      }
   } else if (pTmp=strstr(acTmp, "FAM TR"))
   {
      strcpy(acSave, --pTmp);
      *pTmp = 0;
   } else if (pTmp=strstr(acTmp, "FAMILY TR"))
   {
      strcpy(acSave, --pTmp);
      *pTmp = 0;
   } else if (pTmp=strstr(acTmp, "LIV TR"))
   {
      strcpy(acSave, --pTmp);
      *pTmp = 0;
   } else if (pTmp=strstr(acTmp, "REV TR"))
   {
      strcpy(acSave, --pTmp);
      *pTmp = 0;
   } else if (pTmp=strstr(acTmp, "REVOC TR"))
   {
      strcpy(acSave, --pTmp);
      *pTmp = 0;
   } else if (pTmp=strstr(acTmp, "PERSONAL TR"))
   {
      strcpy(acSave, --pTmp);
      *pTmp = 0;
   } else
      acSave[0] = 0;

   // Check for vesting
   acVesting[0] = 0;
   if ((pTmp=strstr(acTmp, " H/")) || (pTmp=strstr(acTmp, " W/")))
   {
      *pTmp = 0;
      strcpy(acVesting, "HW");
   } else if (pTmp=strstr(acTmp, " M/M"))
   {
      *pTmp = 0;
      strcpy(acVesting, "MM");
   } else if (pTmp=strstr(acTmp, " M/W"))
   {
      *pTmp = 0;
      strcpy(acVesting, "MW");
   } else if (pTmp=strstr(acTmp, " U/M"))
   {
      *pTmp = 0;
      strcpy(acVesting, "UM");
   } else if (pTmp=strstr(acTmp, " U/W"))
   {
      *pTmp = 0;
      strcpy(acVesting, "UW");
   } else if (pTmp=strstr(acTmp, " S/M"))
   {
      *pTmp = 0;
      strcpy(acVesting, "SM");
   } else if (pTmp=strstr(acTmp, " S/P"))
   {
      *pTmp = 0;
      strcpy(acVesting, "SP");
   } else if (pTmp=strstr(acTmp, " S/W"))
   {
      *pTmp = 0;
      strcpy(acVesting, "SW");
   } else if (pTmp=strstr(acTmp, " JT"))
   {
      strcpy(acVesting, "JT");
      *pTmp = 0;

      pTmp1 = strstr(pTmp+3, " JT");
      if (pTmp1)
      {  // IMP - NELSON C E & V A JT & BISHOP B B & K E JT
         *pTmp1 = 0;
         strcat(acTmp, pTmp+3);
         if (*(pTmp1+3))
            strcat(acTmp, pTmp1+3);
      } else if (!memcmp(pTmp+strlen(pTmp+2), "JT", 2))
      {  // GILLESPIE J J & M A JT & GILLESPIE W J & DJT
         *(pTmp+strlen(pTmp+2)) = 0;
         strcat(acTmp, pTmp+3);
      } else
      {  // SCHMELZLE R & B L JT 1/2 & CRONE STELLA 1/2
         if (pTmp1 = strchr(pTmp+3, '&'))
            strcat(acTmp, pTmp+3);
         else if (pTmp1 = strstr(pTmp+3, " AND "))
         {  // SOEGAARD J & M JT AND SOEGAARD ANDREW G
            *pTmp1 = 0;
            strcat(acTmp, pTmp+3);
            strcat(acTmp, " &");
            strcat(acTmp, pTmp1+4);
         }
      }
   }

   // Check for year that goes before TRUST
   iTmp = iTmp1 =0;
   while (acTmp[iTmp])
   {
      if (!isdigit(acTmp[iTmp]) && acTmp[iTmp] != '/' && acTmp[iTmp] != '%')
         acOwners[iTmp1++] = acTmp[iTmp];
      iTmp++;
   }
   acOwners[iTmp1] = 0;

   // Chop off second '&'
   acName1[0] = 0;
   acName2[0] = 0;
   if (pTmp = strchr(acOwners, '&'))
   {
      if (pTmp1 = strchr(++pTmp, '&'))
      {
         iTmp = iTmp1 =0;
         while (acOwners[iTmp])
         {
            acName1[iTmp1++] = acOwners[iTmp];
            if (acOwners[iTmp] == '&' && acOwners[iTmp+1] != ' ')
               acName1[iTmp1++] = ' ';
            else if (acOwners[iTmp] != ' ' && acOwners[iTmp+1] == '&')
               acName1[iTmp1++] = ' ';
            iTmp++;
         }
         acName1[iTmp1] = 0;
         //strcpy(acName1, acOwners);
         *pTmp1 = 0;
      }
   }

   // Now parse owners
   splitOwner(acOwners, &myOwner, 0);
   if (acName1[0])
   {
      // WRIGHT W R JR & D JT & WRIGHT G G & G E JT
      myOwner.acTitle[0] = 0;
   } else
   {
      strcpy(acName1, myOwner.acName1);
      if (myOwner.acName2[0] && strcmp(myOwner.acName1, myOwner.acName2))
      {
         iTmp = strlen(myOwner.acName2);
         if (iTmp > SIZ_NAME2)
            iTmp = SIZ_NAME2;
         memcpy(pOutbuf+OFF_NAME2, myOwner.acName2, iTmp);
      }
   }

   // Name1
   if (acSave[0])
      strcat(acName1, acSave);
   iTmp = strlen(acName1);
   if (iTmp > SIZ_NAME1)
      iTmp = SIZ_NAME1;
   memcpy(pOutbuf+OFF_NAME1, acName1, iTmp);

   iTmp = strlen(myOwner.acSwapName);
   if (iTmp > SIZ_NAME_SWAP)
      iTmp = SIZ_NAME_SWAP;
   memcpy(pOutbuf+OFF_NAME_SWAP, myOwner.acSwapName, iTmp);

   if (myOwner.acVest[0] > ' ')
      memcpy(pOutbuf+OFF_VEST, myOwner.acVest, strlen(myOwner.acVest));
   else if (acVesting[0])
      memcpy(pOutbuf+OFF_VEST, acVesting, strlen(acVesting));

}

/******************************** Imp_ParseSAdr ******************************
 *
 *
 *****************************************************************************/

void Imp_ParseSAdr(ADR_REC *pAdrRec, char *pAdr1)
{
   int   iTmp, iCnt, iStrNo, iLen;
   char  acAdr1[64], acTmp[64], *apItems[16];
   bool  bSkip=false;

   iTmp = 0;
   while (*pAdr1)
   {
      // Skip '.' and what inside parenthesis
      if (*pAdr1 != '.')
      {
         if (*pAdr1 == '(')
            bSkip = true;
         else if (*pAdr1 == ')')
         {
            bSkip = false;
            if (*(pAdr1+1) == ' ' && acAdr1[iTmp-1] == ' ')
               pAdr1++;
         } else if (!bSkip)
            acAdr1[iTmp++] = *pAdr1;
      }
      pAdr1++;
   }
   acAdr1[iTmp] = 0;
   strcpy(acTmp, acAdr1);

   iCnt = ParseString(acAdr1, 32, 16, apItems);
   if (iCnt > 2)
   {
      for (iTmp = 0; iTmp < iCnt; iTmp++)
      {

         if (*apItems[iTmp] == '&')
         {
            continue;
         }
         iStrNo = atoi(apItems[iTmp]);
         if (!iStrNo)
            break;

         iLen = strlen(apItems[iTmp]);
         if (iLen > 1 && !isdigit(apItems[iTmp][iLen-2]))
            break;
      }

      strcpy(acTmp, apItems[0]);
      while (iTmp < iCnt)
      {
         if (*apItems[iTmp])
         {
            if (*apItems[iTmp] != '&')
            {
               strcat(acTmp, " ");
               strcat(acTmp, apItems[iTmp++]);
            } else if (!isdigit(*apItems[iTmp-1]) && isdigit(*apItems[iTmp+1]))
               break;
            else
               iTmp++;
         } else
            break;
      }

   } else if (iCnt == 1)
   {
      strcpy(pAdrRec->strName, acAdr1);
      return;
   }

   parseMAdr1(pAdrRec, acTmp);

}

/********************************* Imp_MergeAdr ******************************
 *
 * Updated with 2006 layout
 *
 *****************************************************************************/

void Imp_MergeAdr(char *pOutbuf)
{
   char     acTmp[256], acAddr1[64], acAddr2[64];
   int      iTmp, iStrNo;
   ADR_REC  sMailAdr;
   ADR_REC  sSitusAdr;

   // Clear old Mailing
   memset(pOutbuf+OFF_M_STRNUM, ' ', SIZ_M_ADDR);
   memset(pOutbuf+OFF_M_ADDR_D, ' ', SIZ_M_ADDRB_D);
   memset((void *)&sMailAdr, 0, sizeof(ADR_REC));
   memset((void *)&sSitusAdr, 0, sizeof(ADR_REC));

   // Mail address
   strcpy(acAddr1, strupr(apTokens[IMP_ROLL_M_ADDR]));
   memcpy(pOutbuf+OFF_M_ADDR_D, acAddr1, strlen(acAddr1));
   iTmp = sprintf(acAddr2, "%s, %s %s", strupr(apTokens[IMP_ROLL_M_CITY]), strupr(apTokens[IMP_ROLL_M_STATE]), apTokens[IMP_ROLL_M_ZIP]);
   memcpy(pOutbuf+OFF_M_CTY_ST_D, acAddr2, iTmp);

   // Parse mail address
   parseMAdr1(&sMailAdr, acAddr1);

   if (sMailAdr.lStrNum > 0)
   {
      iTmp = sprintf(acTmp, "%d", sMailAdr.lStrNum);
      memcpy(pOutbuf+OFF_M_STRNUM, acTmp, iTmp);
      memcpy(pOutbuf+OFF_M_STR_SUB, sMailAdr.strSub, strlen(sMailAdr.strSub));
   }
   memcpy(pOutbuf+OFF_M_DIR, sMailAdr.strDir, strlen(sMailAdr.strDir));
   memcpy(pOutbuf+OFF_M_STREET, sMailAdr.strName, strlen(sMailAdr.strName));
   memcpy(pOutbuf+OFF_M_SUFF, sMailAdr.strSfx, strlen(sMailAdr.strSfx));

   // Mail city/st
   if (*apTokens[IMP_ROLL_M_STATE] >= 'A' && 2 == strlen(apTokens[IMP_ROLL_M_STATE]))
   {
      memcpy(pOutbuf+OFF_M_CITY, apTokens[IMP_ROLL_M_CITY], strlen(apTokens[IMP_ROLL_M_CITY]));
      memcpy(pOutbuf+OFF_M_ST, apTokens[IMP_ROLL_M_STATE], 2);
   } else
   {
      parseAdr2_1(&sMailAdr, apTokens[IMP_ROLL_M_CITY]);
      if (sMailAdr.State[0] > ' ')
         memcpy(pOutbuf+OFF_M_ST, sMailAdr.State, SIZ_M_ST);
      memcpy(pOutbuf+OFF_M_CITY, sMailAdr.City, strlen(sMailAdr.City));
   }

   // Mail zip
   long lZip = atoin(apTokens[IMP_ROLL_M_ZIP], 5);
   if (lZip > 400)
   {
      sprintf(acTmp, "%0.5d", lZip);
      memcpy(pOutbuf+OFF_M_ZIP, acTmp, 5);
      strcpy(sMailAdr.Zip, acTmp);
   }

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "001022001", 9))
   //   iTmp = 0;
#endif

   // Situs
   memcpy(pOutbuf+OFF_S_ADDR_D, strupr(apTokens[IMP_ROLL_S_ADDR1]), strlen(apTokens[IMP_ROLL_S_ADDR1]));
   strcpy(acAddr1, apTokens[IMP_ROLL_S_STREET]);
   parseAdr1C(&sSitusAdr, acAddr1);

   iStrNo = atol(apTokens[IMP_ROLL_S_STRNUM]);
   if (iStrNo > 0)
   {                      
      // Remove space from StrNum
      remChar(apTokens[IMP_ROLL_S_STRNUM], ' ');
      sprintf(acAddr1, "%s              ", apTokens[IMP_ROLL_S_STRNUM]);
      memcpy(pOutbuf+OFF_S_STRNUM, acAddr1, SIZ_S_STRNUM);
      memcpy(pOutbuf+OFF_S_HSENO, acAddr1, SIZ_S_HSENO);

      if (*apTokens[IMP_ROLL_S_DIR] > ' ')
         memcpy(pOutbuf+OFF_S_DIR, apTokens[IMP_ROLL_S_DIR], strlen(apTokens[IMP_ROLL_S_DIR]));
      sSitusAdr.lStrNum = iStrNo;
   } else if (sSitusAdr.lStrNum > 0)
   {
      memcpy(pOutbuf+OFF_S_STRNUM, sSitusAdr.strNum, strlen(sSitusAdr.strNum));
      memcpy(pOutbuf+OFF_S_HSENO, sSitusAdr.strNum, strlen(sSitusAdr.strNum));
      if (sSitusAdr.strDir[0] > ' ')
         memcpy(pOutbuf+OFF_S_DIR, sSitusAdr.strDir, strlen(sSitusAdr.strDir));
   }

   memcpy(pOutbuf+OFF_S_STREET, sSitusAdr.strName, strlen(sSitusAdr.strName));
   if (sSitusAdr.strSfx[0] > ' ')
      memcpy(pOutbuf+OFF_S_SUFF, sSitusAdr.strSfx, strlen(sSitusAdr.strSfx));
   strcpy(acTmp, apTokens[IMP_ROLL_S_UNIT]);
   blankRem(acTmp);
   if (acTmp[0] > ' ')
   {
      iTmp = strlen(acTmp);
      // If unit# is longer than defined size, it probably not good.  So drop it.
      if (iTmp <= SIZ_S_UNITNO)
         memcpy(pOutbuf+OFF_S_UNITNO, acTmp, iTmp);
   }

   // Situs city
   //    - First check to see if S_ADDR2 is provided
   //    - Second check to see if street name and number match mailings, if so use mail city
   //    - Third check to see if city code is available, if so translate it
   //    - Last match TRA with city table.  This method is only 90% accurate.
   memcpy(pOutbuf+OFF_S_ST, "CA", 2);
   if (*apTokens[IMP_ROLL_S_ADDR2] >= 'A')
   {
      strcpy(acAddr2, apTokens[IMP_ROLL_S_ADDR2]);
      strupr(acAddr2);
      parseAdr2_1(&sSitusAdr, acAddr2);
   } else if (sMailAdr.lStrNum > 0 && sMailAdr.strName[0] > ' ' &&
              (sMailAdr.lStrNum == sSitusAdr.lStrNum) &&
              (!strcmp(myTrim(sMailAdr.strName), myTrim(sSitusAdr.strName))) &&
              (*apTokens[IMP_ROLL_M_CITY] > ' ') )
   {
      strcpy(sSitusAdr.City, apTokens[IMP_ROLL_M_CITY]);
      strcpy(sSitusAdr.Zip, sMailAdr.Zip);
   } else if (*apTokens[IMP_ROLL_S_CITY] >= 'A')
   {
      // void Abbr2Code(char *pCityAbbr, char *pCityCode, char *pCityName, char *pApn)
      Abbr2Code(apTokens[IMP_ROLL_S_CITY], acTmp, sSitusAdr.City, pOutbuf);
   } else
   {
      int iTra = atoi(apTokens[IMP_ROLL_TRA]);
      iTmp = 0;
      acAddr2[0] = 0;
      while (iTra > asImpCity[iTmp].iHiTra)
         iTmp++;
      if (iTra >= asImpCity[iTmp].iLoTra && *asImpCity[iTmp].pCity > ' ')
      {
         strcpy(sSitusAdr.City, asImpCity[iTmp].pCity);
         if (*asImpCity[iTmp].pZip > ' ')
            strcpy(sSitusAdr.Zip, asImpCity[iTmp].pZip);
      } else if (iTra > 0 && sSitusAdr.lStrNum > 0)
      {
         sSitusAdr.City[0] = 0;
         iBadCity++;
         if (bDebug)
            LogMsg0("Unknown TRA: %s APN: %.*s", apTokens[IMP_ROLL_TRA], iApnLen, pOutbuf);
      }
   }

   if (sSitusAdr.City[0] >= 'A')
   {
      City2Code(sSitusAdr.City, acTmp, pOutbuf);
      memcpy(pOutbuf+OFF_S_CITY, acTmp, SIZ_S_CITY);
      iTmp = atol(sSitusAdr.Zip);
      if (iTmp >= 90000)
         memcpy(pOutbuf+OFF_S_ZIP, sSitusAdr.Zip, SIZ_S_ZIP);
      else
         City2Zip(sSitusAdr.City, sSitusAdr.Zip);

      iTmp = sprintf(acAddr2, "%s, CA %s", GetCityName(atoi(acTmp)), sSitusAdr.Zip);
      if (iTmp > SIZ_S_CTY_ST_D) iTmp = SIZ_S_CTY_ST_D;
      memcpy(pOutbuf+OFF_S_CTY_ST_D, acAddr2, iTmp);
   }
}
/* 2005 version
void Imp_MergeAdr(char *pOutbuf)
{
   char     acTmp[256], acAddr1[64], acAddr2[64], acCode[32];
   int      iTmp, iStrNo, iCnt;
   ADR_REC  sMailAdr;
   ADR_REC  sSitusAdr;

   // Clear old Mailing
   memset(pOutbuf+OFF_M_STRNUM, ' ', SIZ_M_ADDR);
   memset(pOutbuf+OFF_M_ADDR_D, ' ', SIZ_M_ADDRB_D);
   memset(pOutbuf+OFF_CARE_OF, ' ', SIZ_CARE_OF);
   memset((void *)&sMailAdr, 0, sizeof(ADR_REC));
   memset((void *)&sSitusAdr, 0, sizeof(ADR_REC));



   // Mail address
   if (!memcmp(apTokens[IMP_ROLL_M_CITYST], "ATTN", 4))
   {
      strcpy(acAddr1, apTokens[IMP_ROLL_CAREOF]+4);
      strcpy(acAddr2, apTokens[IMP_ROLL_M_ADDR]);

      if (*(apTokens[IMP_ROLL_M_CITYST]+4) == ':')
         strcpy(acTmp, apTokens[IMP_ROLL_M_CITYST]+6);
      else
         strcpy(acTmp, apTokens[IMP_ROLL_M_CITYST]+5);
      memcpy(pOutbuf+OFF_CARE_OF, acTmp, strlen(acTmp));
   } else
   {
      // Check for C/O
      iTmp = strlen(apTokens[IMP_ROLL_CAREOF]);
      if (iTmp > 0)
      {
         if (!memcmp(apTokens[IMP_ROLL_CAREOF], "C/O ", 4)
            || !memcmp(apTokens[IMP_ROLL_CAREOF], "C/0 ", 4)
            || !memcmp(apTokens[IMP_ROLL_CAREOF], "C/B ", 4)
            || !memcmp(apTokens[IMP_ROLL_CAREOF], "CB: ", 4) )
            strcpy(acTmp, apTokens[IMP_ROLL_CAREOF]+4);
         else if (!memcmp(apTokens[IMP_ROLL_CAREOF], "CB ", 3))
            strcpy(acTmp, apTokens[IMP_ROLL_CAREOF]+3);
         else if (!memcmp(apTokens[IMP_ROLL_CAREOF], "ATTN: ", 6)
            || !memcmp(apTokens[IMP_ROLL_CAREOF], "ATTN  ", 6))
            strcpy(acTmp, apTokens[IMP_ROLL_CAREOF]+6);
         else if (!memcmp(apTokens[IMP_ROLL_CAREOF], "ATTN:", 5)
            || !memcmp(apTokens[IMP_ROLL_CAREOF], "ATTN ", 5))
            strcpy(acTmp, apTokens[IMP_ROLL_CAREOF]+5);
         else if (!memcmp(apTokens[IMP_ROLL_CAREOF], "ATTENTION ", 10))
            strcpy(acTmp, apTokens[IMP_ROLL_CAREOF]+10);
         else
            strcpy(acTmp, apTokens[IMP_ROLL_CAREOF]);
         acTmp[SIZ_CARE_OF] = 0;
         memcpy(pOutbuf+OFF_CARE_OF, acTmp, strlen(acTmp));
      }

      strcpy(acAddr1, apTokens[IMP_ROLL_M_ADDR]);
      strcpy(acAddr2, apTokens[IMP_ROLL_M_CITYST]);
   }

   memcpy(pOutbuf+OFF_M_ADDR_D, acAddr1, strlen(acAddr1));
   memcpy(pOutbuf+OFF_M_CTY_ST_D, acAddr2, strlen(acAddr2));

   // Parse mail address
   parseMAdr1(&sMailAdr, acAddr1);
   parseMAdr2(&sMailAdr, acAddr2);

   if (sMailAdr.lStrNum > 0)
   {
      iTmp = sprintf(acTmp, "%*d", SIZ_M_STRNUM, sMailAdr.lStrNum);
      memcpy(pOutbuf+OFF_M_STRNUM, acTmp, iTmp);
      memcpy(pOutbuf+OFF_M_STR_SUB, sMailAdr.strSub, strlen(sMailAdr.strSub));
   }
   memcpy(pOutbuf+OFF_M_DIR, sMailAdr.strDir, strlen(sMailAdr.strDir));
   memcpy(pOutbuf+OFF_M_STREET, sMailAdr.strName, strlen(sMailAdr.strName));
   memcpy(pOutbuf+OFF_M_SUFF, sMailAdr.strSfx, strlen(sMailAdr.strSfx));
   memcpy(pOutbuf+OFF_M_CITY, sMailAdr.City, strlen(sMailAdr.City));
   if (2 == strlen(sMailAdr.State))
      memcpy(pOutbuf+OFF_M_ST, sMailAdr.State, 2);

   iTmp = atoi(apTokens[IMP_ROLL_M_ZIP]);
   if (iTmp > 400)
   {
      sprintf(acTmp, "%0.5d", iTmp);
      memcpy(pOutbuf+OFF_M_ZIP, acTmp, SIZ_M_ZIP);
      strcpy(sMailAdr.Zip, acTmp);
   }

   // Situs
   strcpy(acAddr1, apTokens[IMP_ROLL_S_ADDR]);
   memcpy(pOutbuf+OFF_S_ADDR_D, acAddr1, strlen(acAddr1));

   Imp_ParseSAdr(&sSitusAdr, acAddr1);

   if (sSitusAdr.lStrNum > 0)
   {
      memcpy(pOutbuf+OFF_S_STRNUM, sSitusAdr.strNum, strlen(sSitusAdr.strNum));
      if (sSitusAdr.strSub[0] > ' ')
         memcpy(pOutbuf+OFF_S_STR_SUB, sSitusAdr.strSub, strlen(sSitusAdr.strSub));
      if (sSitusAdr.strDir[0] > ' ')
         memcpy(pOutbuf+OFF_S_DIR, sSitusAdr.strDir, strlen(sSitusAdr.strDir));
   }

   memcpy(pOutbuf+OFF_S_STREET, sSitusAdr.strName, strlen(sSitusAdr.strName));
   if (sSitusAdr.strSfx[0] > ' ')
   {
      iTmp = GetSfxCodeX(sSitusAdr.strSfx, acTmp);
      if (iTmp > 0)
         Sfx2Code(acTmp, acCode);
      else
      {
         LogMsg0("*** Invalid suffix: %s", sSitusAdr.strSfx);
         iBadSuffix++;
         memset(acCode, ' ', SIZ_S_SUFF);
      }
      memcpy(pOutbuf+OFF_S_SUFF, acCode, SIZ_S_SUFF);
   }
   if (sSitusAdr.Unit[0] > ' ')
      memcpy(pOutbuf+OFF_S_UNITNO, sSitusAdr.Unit, strlen(sSitusAdr.Unit));

   // Situs city
   int iTra = atoi(apTokens[IMP_ROLL_TRA]);
   iTmp = 0;
   acAddr2[0] = 0;
   while (iTra > asImpCity[iTmp].iHiTra)
      iTmp++;
   if (iTra >= asImpCity[iTmp].iLoTra && *asImpCity[iTmp].pCity > ' ')
   {
      City2Code(asImpCity[iTmp].pCity, acTmp);
      memcpy(pOutbuf+OFF_S_CITY, acTmp, SIZ_S_CITY);
      sprintf(acAddr2, "%s CA", asImpCity[iTmp].pCity);
      memcpy(pOutbuf+OFF_S_ST, "CA", 2);
   } else if (sMailAdr.lStrNum > 0 && sMailAdr.strName[0] > ' ' &&
              (sMailAdr.lStrNum == sSitusAdr.lStrNum) &&
              (!strcmp(sMailAdr.strName, sSitusAdr.strName)) &&
              (sMailAdr.City[0] > ' ') )
   {
      City2Code(sMailAdr.City, acTmp);
      memcpy(pOutbuf+OFF_S_CITY, acTmp, SIZ_S_CITY);
      sprintf(acAddr2, "%s CA", sMailAdr.City);
      memcpy(pOutbuf+OFF_S_ST, "CA", 2);
      if (acTmp[0] > '0')
      {
         LogMsg0("Use Mail city %s for %.12s", sMailAdr.City, pOutbuf);
         memcpy(pOutbuf+OFF_S_ZIP, pOutbuf+OFF_M_ZIP, SIZ_S_ZIP);
      } else
         LogMsg0("*** Invalid Mail city %s", sMailAdr.City);
   } else if (iTra > 0)
      iBadCity++;

   memcpy(pOutbuf+OFF_S_CTY_ST_D, acAddr2, strlen(acAddr2));
}
*/
/******************************** Imp_UpdateAdr ******************************
 *
 * Return 0 if successful, >0 if error
 *
 *****************************************************************************/

void Imp_UpdateAdr(char *pOutbuf)
{
}

/******************************** Imp_MergeExe *******************************
 *
 * 2009 data is a delimited record with double quote as string qualifier
 *
 *****************************************************************************/

int Imp_MergeExe(char *pOutbuf)
{
   static   char acRec[512], *pRec=NULL;
   char     acTmp[256];
   long     lTmp;
   int      iRet=0, iTmp;

   // Get rec
   if (!pRec)
   {
      // Skip header record
      //pRec = fgets(acRec, 512, fdExe);
      // Get first rec
      pRec = fgets(acRec, 512, fdExe);
   }

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "023443003", 9))
   //   iTmp = 0;
#endif
   do
   {
      if (!pRec)
      {
         fclose(fdExe);
         fdExe = NULL;
         *(pOutbuf+OFF_HO_FL) = '2';
         return 1;      // EOF
      }

      // Skip quote to copare
      iTmp = memcmp(pOutbuf, pRec+1, iApnLen);
      if (iTmp > 0)
      {
         if (bDebug)
            LogMsg0("Skip Exe rec %.*s", iApnLen, pRec+1);
         pRec = fgets(acRec, 512, fdExe);
         lExeSkip++;
      }
   } while (iTmp > 0);

   // If not match, return
   if (iTmp)
   {
      // Assume not owner occupy
      *(pOutbuf+OFF_HO_FL) = '2';      // 'N'
      return 1;
   }

   iRet = ParseStringNQ(pRec, ',', MAX_FLD_TOKEN, apTokens);
   if (iRet < IMP_EXE_USERID)
   {
      LogMsg("***** Error: bad Exe record for APN=%.*s (#tokens=%d)", iApnLen, pOutbuf, iRet);
      pRec = fgets(acRec, 512, fdExe);
      return -1;
   }

   // HO Exe - 2010 values are "True" or "False".  Prior year are "1" or "0"
   if (*apTokens[IMP_EXE_HOEXE] == '1' || *apTokens[IMP_EXE_HOEXE] == 'T')
      *(pOutbuf+OFF_HO_FL) = '1';      // 'Y'
   else
      *(pOutbuf+OFF_HO_FL) = '2';      // 'N'

   // Exe Amt
   lTmp = atol(apTokens[IMP_EXE_EXEAMT]);
   if (lTmp > 0 && memcmp(apTokens[IMP_EXE_EXEAMT], "99999", 5))
   {
      sprintf(acTmp, "%*d", SIZ_EXE_TOTAL, lTmp);
      memcpy(pOutbuf+OFF_EXE_TOTAL, acTmp, SIZ_EXE_TOTAL);
   }

   // Asmt status
   //if (*apTokens[IMP_EXE_ASMTSTATUS] > ' ')
   //*(pOutbuf+OFF_STATUS) = *apTokens[IMP_EXE_ASMTSTATUS];

   // Get next record
   pRec = fgets(acRec, 512, fdExe);
   lExeMatch++;

   return 0;
}

/********************************* Imp_MergeRoll *****************************
 *
 * Return 0 if successful, < 0 if error
 *        1 retired record, not use
 *
 *****************************************************************************/

int Imp_MergeRoll(char *pOutbuf, char *pRollRec, int iLen, int iFlag)
{
   char     acTmp[256], acTmp1[256];
   long     lTmp;
   int      iRet=0, iTmp;

   // Replace tab char with 0
   iRet = ParseStringNQ(pRollRec, ',', MAX_FLD_TOKEN, apTokens);
   if (iRet < IMP_ROLL_TAXABILITY)
   {
      LogMsg("***** Error: bad input record for APN=%s", apTokens[iApnFld]);
      return -1;
   }

   if (iFlag & CREATE_R01)
   {
      // Clear output buffer
      memset(pOutbuf, ' ', iRecLen);

      // Start copying data
      memcpy(pOutbuf, apTokens[iApnFld], strlen(apTokens[iApnFld]));

      // Format APN
      iRet = formatApn(apTokens[iApnFld], acTmp, &myCounty);
      memcpy(pOutbuf+OFF_APN_D, acTmp, iRet);

      // Create MapLink and output new record
      iRet = formatMapLink(apTokens[iApnFld], acTmp, &myCounty);
      memcpy(pOutbuf+OFF_MAPLINK, acTmp, iRet);

      // Create index map link
      if (getIndexPage(acTmp, acTmp1, &myCounty))
         memcpy(pOutbuf+OFF_IMAPLINK, acTmp1, iRet);

      // County code
      memcpy(pOutbuf+OFF_CO_NUM, "13IMP", 5);
      if (*apTokens[IMP_ROLL_STATUS] > ' ')
         *(pOutbuf+OFF_STATUS) = *apTokens[IMP_ROLL_STATUS];

      // Year assessed
      memcpy(pOutbuf+OFF_YR_ASSD, myCounty.acYearAssd, 4);

      // TRA
      iTmp = atoi(apTokens[IMP_ROLL_TRA]);
      if (iTmp > 0)
      {
         memcpy(pOutbuf+OFF_TRA, apTokens[IMP_ROLL_TRA], strlen(apTokens[IMP_ROLL_TRA]));
      }

      // HO Exempt - Use values from exemption file in Imp_MergeExe()

      // Land
      long lLand = atoi(apTokens[IMP_ROLL_LAND]);
      if (lLand > 0)
      {
         sprintf(acTmp, "%*d", SIZ_LAND, lLand);
         memcpy(pOutbuf+OFF_LAND, acTmp, SIZ_LAND);
      }

      // Improve
      long lImpr = atoi(apTokens[IMP_ROLL_IMPR]);
      if (lImpr > 0)
      {
         sprintf(acTmp, "%*d", SIZ_IMPR, lImpr);
         memcpy(pOutbuf+OFF_IMPR, acTmp, SIZ_IMPR);
      }

      // Other value: TradeFixture, LivingImp, PPDeclared, PPUnitVal, PPMH
      lTmp = atoi(apTokens[IMP_ROLL_PPVALUE]);
      if (lTmp > 0)
      {
         sprintf(acTmp, "%*d", SIZ_OTHER, lTmp);
         memcpy(pOutbuf+OFF_OTHER, acTmp, SIZ_OTHER);

         sprintf(acTmp, "%d          ", lTmp);
         memcpy(pOutbuf+OFF_PERSPROP, acTmp, SIZ_OTH_VALUE);
      }

      // Gross total
      lTmp += lLand+lImpr;
      if (lTmp > 0)
      {
         sprintf(acTmp, "%*d", SIZ_GROSS, lTmp);
         memcpy(pOutbuf+OFF_GROSS, acTmp, SIZ_GROSS);
      }

      // Ratio
      if (lImpr > 0)
      {
         sprintf(acTmp, "%*d", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
         memcpy(pOutbuf+OFF_RATIO, acTmp, SIZ_RATIO);
      }

      // Legal
      if (*apTokens[IMP_ROLL_LEGAL] > ' ')
         iTmp = updateLegal(pOutbuf, apTokens[IMP_ROLL_LEGAL]);

      // Tax code
      if (memcmp(apTokens[IMP_ROLL_TAXABILITY], "000", 3) > 0)
         memcpy(pOutbuf+OFF_TAX_CODE, apTokens[IMP_ROLL_TAXABILITY], 3);
   }

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "001041005", 9))
   //   iTmp = 0;
#endif

   // UseCode
   sprintf(acTmp, "%s%s", apTokens[IMP_ROLL_USECODE1], apTokens[IMP_ROLL_USECODE2]);
   myTrim(acTmp);
   strupr(acTmp);
   if (acTmp[0] > ' ')
   {
      memcpy(pOutbuf+OFF_USE_CO, acTmp, strlen(acTmp));
      memset(pOutbuf+OFF_USE_STD, ' ', SIZ_USE_STD);
      updateStdUse(pOutbuf+OFF_USE_STD, acTmp, strlen(acTmp));
   }

   // Owner
   try {
      Imp_MergeOwner(pOutbuf, apTokens[IMP_ROLL_OWNER]);
   } catch(...) {
      LogMsg("***** Execption occured in Imp_MergeOwner()");
   }

   // Mailing - Situs
   try {
      Imp_MergeAdr(pOutbuf);
   } catch(...) {
      LogMsg("***** Execption occured in Imp_MergeAdr()");
   }

   // CareOf
   memset(pOutbuf+OFF_CARE_OF, ' ', SIZ_CARE_OF);
   if (*apTokens[IMP_ROLL_CAREOF] > ' ')
   {
      iTmp = strlen(apTokens[IMP_ROLL_CAREOF]);
      if (iTmp > SIZ_CARE_OF)
         iTmp = SIZ_CARE_OF;
      memcpy(pOutbuf+OFF_CARE_OF, apTokens[IMP_ROLL_CAREOF], iTmp);
   }

   // DBA

   // Recorded Doc
   if (*apTokens[IMP_ROLL_DOCNUM] > '0' && *apTokens[IMP_ROLL_DOCDATE] > ' ')
   {
      iTmp = strlen(apTokens[IMP_ROLL_DOCNUM]);
      if (iTmp > SIZ_TRANSFER_DOC)
      {
         memcpy(pOutbuf+OFF_TRANSFER_DOC, apTokens[IMP_ROLL_DOCNUM], 4);
         memcpy(pOutbuf+OFF_TRANSFER_DOC+4, &apTokens[IMP_ROLL_DOCNUM][5], iTmp-5);
      } else
         memcpy(pOutbuf+OFF_TRANSFER_DOC, apTokens[IMP_ROLL_DOCNUM], iTmp);

      dateConversion(apTokens[IMP_ROLL_DOCDATE], acTmp, 0);
      memcpy(pOutbuf+OFF_TRANSFER_DT, acTmp, SIZ_TRANSFER_DT);
      lTmp = atol(acTmp);
      if (lTmp > lLastRecDate && lTmp < lToday)
         lLastRecDate = lTmp;
   }
   return 0;
}

/********************************** UpdateImpRec *****************************
 *
 * Return 0 if successful, >0 if error
 *
 *****************************************************************************/

int UpdateImpRec(char *pOutbuf, char *pRollRec, int iLen, int iFlag)
{
   return 0;
}

/********************************** Imp_Load_Roll *****************************
 *
 *
 ******************************************************************************/

int Imp_Load_Roll(int iSkip)
{
   char     *pTmp, acRec[MAX_RECSIZE], acBuf[MAX_RECSIZE], acRollRec[MAX_RECSIZE];
   char     cFileCnt=1;
   char     acRawFile[_MAX_PATH], acOutFile[_MAX_PATH], acTmpFile[_MAX_PATH], acTmp[256];

   HANDLE   fhIn, fhOut;
   FILE     *fdRoll;

   int      iRet, iTmp, iRollUpd=0, iNewRec=0, iRetiredRec=0;
   DWORD    nBytesRead;
   DWORD    nBytesWritten;
   BOOL     bRet, bEof;
   long     lRet=0, lCnt=0;

   sprintf(acRawFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "S01");
   sprintf(acOutFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "R01");
   sprintf(acTmpFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "TMP");

   // Rename files for processing
   if (_access(acRawFile, 0))
   {
      if (!_access(acOutFile, 0))
         rename(acOutFile, acRawFile);
      else
      {
         LogMsg("Missing input file %s.  Please recheck!", acRawFile);
         return 1;
      }
   }

   // Open updated roll file
   LogMsg("Open Roll file %s", acRollFile);
   fdRoll = fopen(acRollFile, "r");
   if (fdRoll == NULL)
   {
      LogMsg("Error opening roll file: %s\n", acRollFile);
      return 2;
   }

   // Open Input file
   LogMsg("Open input file %s", acRawFile);
   fhIn = CreateFile(acRawFile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhIn == INVALID_HANDLE_VALUE)
      return 3;

   // Open Output file - use tmp file so we can sort at the end to R01 file
   LogMsg("Open output file %s", acTmpFile);
   fhOut = CreateFile(acTmpFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
      return 4;

   // Get first RollRec
   fgets((char *)&acRollRec[0], iRollLen, fdRoll);
   pTmp = fgets((char *)&acRollRec[0], iRollLen, fdRoll);
   bEof = (pTmp ? false:true);

   // Copy skip record
   memset(acBuf, ' ', iRecLen);
   while (iSkip-- > 0)
   {
      ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);
      // Ignore record start with 99999999
      //if (memcmp(acBuf, "99999999", 8))
         bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
   }

   iNoMatch=iBadCity=iBadSuffix=0;

   // Merge loop
   while (!bEof)
   {
      bRet = ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);

      // Check for EOF
      if (!bRet)
      {
         LogMsg("Error reading input file %s (%f)", acRawFile, GetLastError());
         break;
      }

      // EOF ?
      if (!nBytesRead)
         break;

      NextRollRec:
      iTmp = memcmp(acBuf, (char *)&acRollRec[1], iApnLen);
      if (!iTmp)
      {
         // Merge roll data
         iRet = UpdateImpRec(acBuf, acRollRec, iRollLen, UPDATE_R01);
         iRollUpd++;

         // Read next roll record
         pTmp = fgets(acRollRec, iRollLen, fdRoll);

         if (!pTmp)
            bEof = true;    // Signal to stop
      } else if (iTmp > 0)       // Roll not match, new roll record?
      {
         LogMsg0("***** New roll record : %.*s (%d) *****", iApnLen, acRollRec, lCnt);

         // Create new R01 record
         iRet = UpdateImpRec(acRec, acRollRec, iRollLen, CREATE_R01);
         iNewRec++;

         bRet = WriteFile(fhOut, acRec, iRecLen, &nBytesWritten, NULL);
         lCnt++;

         // Get next roll record
         pTmp = fgets(acRollRec, iRollLen, fdRoll);

         if (!pTmp)
            bEof = true;    // Signal to stop
         else
            goto NextRollRec;
      } else
      {
         // Record may be retired
         LogMsg0("*** Roll not match (retired record?) : R01->%.*s < Roll->%.*s (%d) ***", iApnLen, acBuf, iApnLen, acRollRec, lCnt);
         iRetiredRec++;
         continue;
      }

      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);

      if (!bRet)
      {
         LogMsg("Error occurs: %d\n", GetLastError());
         lRet = WRITE_ERR;
         break;
      }
   }

   // Do the rest of the file
   while (!bEof)
   {
      LogMsg0("*** New roll record : %.*s (%d) ***", iApnLen, acRollRec, lCnt);

      // Create new R01 record
      Imp_MergeRoll(acRec, acRollRec, iRollLen, CREATE_R01);
      iNewRec++;

      bRet = WriteFile(fhOut, acRec, iRecLen, &nBytesWritten, NULL);
      lCnt++;

      // Get next roll record
      pTmp = fgets(acRollRec, MAX_RECSIZE, fdRoll);

      if (!pTmp)
         bEof = true;    // Signal to stop
   }

   // Close files
   if (fdRoll)
      fclose(fdRoll);
   if (fhOut)
      CloseHandle(fhOut);
   if (fhIn)
      CloseHandle(fhIn);

   // Sort output file
   sprintf(acTmp, "S(1,14,C,A) F(FIX,%d) B(%d,R)", iRecLen, iRecLen);
   lRet = sortFile(acTmpFile, acOutFile, acTmp);

   LogMsg("Total output records:       %u", lCnt);
   LogMsg("Total new records:          %u", iNewRec);
   LogMsg("Total retired records:      %u", iRetiredRec);
   LogMsg("Total bad-city records:     %u", iBadCity);
   LogMsg("Total bad-suffix records:   %u", iBadSuffix);

   printf("\nTotal output records: %u", lCnt);

   lRecCnt = lCnt;

   return 0;
}

/********************************* Imp_Load_LDR *****************************
 *
 * Problem: Input file cannot be sorted since one record may spread over multiple lines.
 *          We have to sort output then merge exemption data later.
 *
 ****************************************************************************/

int Imp_Load_LDR(int iFirstRec /* 1=create header rec */)
{
   char     *pTmp, acBuf[MAX_RECSIZE], acRollRec[MAX_RECSIZE];
   char     acOutFile[_MAX_PATH];

   HANDLE   fhOut;
   FILE     *fdRoll;

   int      iRet, iNewRec=0, iRetiredRec=0;
   DWORD    nBytesWritten;
   BOOL     bRet;
   long     lRet=0, lCnt=0;

   sprintf(acOutFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "TMP");

   // Open roll file
   LogMsg("Open Roll file %s", acRollFile);
   fdRoll = fopen(acRollFile, "r");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", acRollFile);
      return 2;
   }

   // Open Output file
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error creating output file: %s\n", acOutFile);
      return 4;
   }

   // Output first header record
   if (iFirstRec > 0)
   {
      memset(acBuf, '9', iRecLen);
      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
   }

   // Skip header
   pTmp = fgets((char *)&acRollRec[0], MAX_RECSIZE, fdRoll);
   // Get first RollRec
   //pTmp = fgets((char *)&acRollRec[0], MAX_RECSIZE, fdRoll);
   // 2008 - data may be on multiple rows
   //iRet = myGetStrQC(acRollRec, iRollLen, fdRoll);
   // 2009 - data may be on multiple rows
   //iRet = myGetStrDC(acRollRec, iRollLen, fdRoll);
   // 2010 - New format and myGetStrDC won't work
   iRet = myGetStrEQ(acRollRec, iRollLen, fdRoll);

   // Init variables
   iNoMatch=iBadCity=iBadSuffix=0;

   // Merge loop
   while (iRet > 0)
   {
      lLDRRecCount++;

      // Create new R01 record
      iRet = Imp_MergeRoll(acBuf, acRollRec, iRollLen, CREATE_R01|CREATE_LIEN);
      if (!iRet)
      {
         iNewRec++;

         bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
         if (!(++lCnt % 1000))
            printf("\r%u", lCnt);

         if (!bRet)
         {
            LogMsg("***** Error writing to output file at record %d\n", lCnt);
            lRet = WRITE_ERR;
            break;
         }
      } else
         iRetiredRec++;

      // Get next roll record
      //pTmp = fgets(acRollRec, MAX_RECSIZE, fdRoll);
      iRet = myGetStrEQ(acRollRec, iRollLen, fdRoll);
   }

   // Close files
   if (fdRoll)
      fclose(fdRoll);
   if (fhOut)
      CloseHandle(fhOut);

   // Sort output file
   sprintf(acRollRec, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "R01");
   LogMsg("Sort output file %s to %s", acOutFile, acRollRec);
   sprintf(acBuf, "S(1,12,C,A) F(FIX,%d) B(%d,R)", iRecLen, iRecLen);
   iRet = sortFile(acOutFile, acRollRec, acBuf);

   LogMsg("Total input records:        %u", lLDRRecCount);
   LogMsg("Total output records:       %u", lCnt);
   LogMsg("Total bad input records:    %u", iRetiredRec);
   LogMsg("Total bad-city records:     %u", iBadCity);
   LogMsg("Total bad-suffix records:   %u", iBadSuffix);
   LogMsg("Last recording date:        %u", lLastRecDate);
   printf("\nTotal output records: %u\n", lCnt);

   lRecCnt = lCnt;
   return lRet;
}

/******************************* Imp_MergeExeFile *****************************
 *
 *
 ******************************************************************************/

int Imp_MergeExeFile(int iSkip)
{
   char     acRawFile[_MAX_PATH], acOutFile[_MAX_PATH], acTmpFile[_MAX_PATH];
   char     acBuf[MAX_RECSIZE], *pTmp;

   HANDLE   fhIn, fhOut;
   int      iRet;
   DWORD    nBytesRead;
   DWORD    nBytesWritten;
   BOOL     bRet;
   long     lCnt=0, lRet=0;

   // Open Exemption file
   strcpy(acTmpFile, acExeFile);
   if (pTmp = strrchr(acTmpFile, '.'))
      replStr(acTmpFile, pTmp, ".srt");
   else
      strcat(acTmpFile, ".srt");

   LogMsg("Sort Exe file %s --> %s", acExeFile, acTmpFile);
   iRet = sortFile(acExeFile, acTmpFile, "S(#1,C,A) F(TXT)");
   if (iRet < 1000)
      return -1;

   LogMsg("Open Exe file %s", acTmpFile);
   fdExe = fopen(acTmpFile, "r");
   if (fdExe == NULL)
   {
      LogMsg("***** Error opening Exe file: %s\n", acTmpFile);
      return 2;
   }

   // Open Input file
   sprintf(acOutFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "R01");
   sprintf(acTmpFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "TMP");

   LogMsg("Open input file %s", acOutFile);
   fhIn = CreateFile(acOutFile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhIn == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening input file: %s\n", acOutFile);
      return 3;
   }

   // Open Output file - use tmp file so we can sort at the end to R01 file
   LogMsg("Open output file %s", acTmpFile);
   fhOut = CreateFile(acTmpFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error creating output file: %s\n", acTmpFile);
      return 4;
   }

   // Copy skip record
   memset(acBuf, ' ', iRecLen);
   while (iSkip-- > 0)
   {
      ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);
      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
   }

   // Merge loop
   while (1)
   {
      bRet = ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);

      // Check for EOF
      if (!bRet)
      {
         LogMsg("Error reading input file %s (%f)", acRawFile, GetLastError());
         break;
      }

      // EOF ?
      if (iRecLen != nBytesRead)
         break;

      // Merge Exe
      if (fdExe)
         iRet = Imp_MergeExe(acBuf);


      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);

      if (!bRet)
      {
         LogMsg("***** Error occurs: %d\n", GetLastError());
         lRet = WRITE_ERR;
         break;
      }
   }

   // Close files
   if (fdExe)
      fclose(fdExe);
   if (fhOut)
      CloseHandle(fhOut);
   if (fhIn)
      CloseHandle(fhIn);

   // Rename output file
   remove(acOutFile);
   iRet = rename(acTmpFile, acOutFile);

   LogMsg("Total output records:       %u", lCnt);
   LogMsg("Total Exe records matched:  %u", lExeMatch);
   LogMsg("Total Exe records skipped:  %u", lExeSkip);

   printf("\nTotal output records: %u\n", lCnt);

   return lRet;
}

/*********************************** loadImp ********************************
 *
 * Options:
 *    -CIMP -L (load lien)
 *    -CIMP -U (load update)
 *
 ****************************************************************************/

int loadImp(int iSkip)
{
   int   iRet=0;

   // Set default APN field
   if (iApnFld == -1)
      iApnFld = IMP_ROLL_APN;

   iApnLen = myCounty.iApnLen;
   GetPrivateProfileString(myCounty.acCntyCode, "ExeFile", "", acExeFile, _MAX_PATH, acIniFile);

   if (iLoadFlag & LOAD_LIEN)
   {
      //char  acTmpFile[_MAX_PATH];

      // Sort input file
      // Problem: Input file cannot be sorted since one record may spread over multiplt line.
      // We have to sort output then merge exemption data
      /*
      sprintf(acTmpFile, "%s\\Imp\\Imp_Lien.srt", acTmpPath);
      LogMsg("Sort roll file %s to %s", acRollFile, acTmpFile);
      iRet = sortFile(acRollFile, acTmpFile, "S(#2,C,A) Omit(#2,C,GE,\"A\")");
      if (iRet > 10000)
      {
         strcpy(acRollFile, acTmpFile);

         // Create Lien file
         LogMsg("Load %s Lien file", myCounty.acCntyCode);
         iRet = CreateImpRoll(iSkip);
      }
      */
      // Create Lien file
      LogMsg("Load %s Lien file", myCounty.acCntyCode);
      iRet = Imp_Load_LDR(iSkip);
      if (!iRet)
      {
         // Merge Exemption
         iRet = Imp_MergeExeFile(iSkip);
      }
   } else if (iLoadFlag & LOAD_UPDT)
   {
      LogMsg("Load %s roll update file", myCounty.acCntyCode);
      iRet = Imp_Load_Roll(iSkip);
   }

   return 0;
}