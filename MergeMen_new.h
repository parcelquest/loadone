#ifndef  _MERGE_MEN_H
#define  _MERGE_MEN_H

// Aumentum layout
// CA-Mendo-AsmtRoll.csv 04/26/2022
#define  R_APN                                     0
#define  R_CONVENYANCENUMBER                       1
#define  R_CONVEYANCEMONTH                         2
#define  R_CONVEYANCEYEAR                          3
#define  R_CLASSDESCRIPTION                        4
#define  R_ROLLCAT                                 5
#define  R_BASEYEAR                                6
#define  R_BUSINESSUSECODEDESCRIPTION              7
#define  R_DOINGBUSINESSAS                         8
#define  R_MAILNAME                                9
#define  R_MAILADDRESS                             10
#define  R_MAILCITY                                11
#define  R_MAILSTATE                               12
#define  R_MAILZIPCODE                             13
#define  R_SITUSSTREETNUMBER                       14
#define  R_SITUSSTREETNUMBERSUFFIX                 15
#define  R_SITUSSTREETPREDIRECTIONAL               16
#define  R_SITUSSTREETNAME                         17
#define  R_SITUSSTREETTYPE                         18
#define  R_SITUSUNITNUMBER                         19
#define  R_SITUSZIPCODE                            20
#define  R_SITUSCITYNAME                           21
#define  R_ASSESSMENTDESCRIPTION                   22
#define  R_ISTAXABLE                               23
#define  R_TRA                                     24
#define  R_GEO                                     25
#define  R_RECORDERSTYPE                           26
#define  R_RECORDERSBOOK                           27
#define  R_RECORDERSPAGE                           28
#define  R_LOTTYPE                                 29
#define  R_LOT                                     30
#define  R_BLOCK                                   31
#define  R_PORTIONDIRECTION                        32
#define  R_SUBDIVISIONNAMEORTRACT                  33
#define  R_ACREAGEAMOUNT                           34
#define  R_FIRSTSECTION                            35
#define  R_FIRSTTOWNSHIP                           36
#define  R_FIRSTRANGE                              37
#define  R_FIRSTRANGEDIRECTION                     38
#define  R_LEGALPARTY1                             39
#define  R_LEGALPARTY2                             40
#define  R_LEGALPARTY3                             41
#define  R_LEGALPARTY4                             42
#define  R_LAND                                    43
#define  R_IMPR                                    44
#define  R_LIVINGIMPROVEMENTS                      45
#define  R_PERSONALVALUE                           46
#define  R_TRADEFIXTURESAMOUNT                     47
#define  R_PERSONALPROPERTYAPPRAISED               48
#define  R_TENPERCENTASSESSEDPENALTY               49
#define  R_HOX                                     50
#define  R_VETERANSEXEMPTION                       51
#define  R_DISABLEDVETERANSEXEMPTION               52
#define  R_CHURCHEXEMPTION                         53
#define  R_RELIGIOUSEXEMPTION                      54
#define  R_CEMETERYEXEMPTION                       55
#define  R_PUBLICSCHOOLEXEMPTION                   56
#define  R_PUBLICLIBRARYEXEMPTION                  57
#define  R_PUBLICMUSEUMEXEMPTION                   58
#define  R_WELFARECOLLEGEEXEMPTION                 59
#define  R_WELFAREHOSPITALEXEMPTION                60
#define  R_WELFAREPRIVATEPAROCHIALSCHOOLEXEMPTION  61
#define  R_WELFARECHARITYRELIGIOUSEXEMPTION        62
#define  R_OTHEREXEMPTION                          63
#define  R_CAMEFROM                                64
#define  R_CLASSCODE                               65
#define  R_COLS                                    66

// CA-Mendo-Sales.csv
#define  S_PIN                                     0
#define  S_GEO                                     1
#define  S_CONVEYANCEMONTH                         2
#define  S_CONVEYANCEYEAR                          3
#define  S_CONVEYANCENUMBER                        4
#define  S_CONVEYANCEDATE                          5
#define  S_FORMSENTDATE                            6
#define  S_SALELETTERDATE                          7
#define  S_TRANSFERTYPECODE                        8
#define  S_INDICATEDSALEPRICE                      9
#define  S_ADJUSTEDSALEPRICE                       10
#define  S_CLASSDESCRIPTION                        11
#define  S_BASEYEAR                                12
#define  S_BASEYEARFLAG                            13
#define  S_SITUSLINEONE                            14
#define  S_SITUSLINETWO                            15
#define  S_TOTALACRES                              16
#define  S_DESIGNTYPE101                           17
#define  S_CONSTRUCTIONTYPE                        18
#define  S_AREA_1_1                                19
#define  S_COSTAREA                                20
#define  S_TOTALAREA                               21
#define  S_EFFECTIVEAGE                            22
#define  S_CARPORT                                 23
#define  S_ATTGARAGE                               24
#define  S_DETGARAGE                               25
#define  S_POOLIND                                 26
#define  S_BEDROOMS                                27
#define  S_HALFBATHROOM                            28
#define  S_THREEQTRBATHROOM                        29
#define  S_FULLBATHROOM                            30
#define  S_TOTALBATHROOMS                          31
#define  S_SPECADJUSTMENT                          32
#define  S_LANDVALUE                               33
#define  S_STRUCTUREVALUE                          34
#define  S_TREEVINEVALUE                           35
#define  S_TRANSFERFEE                             36
#define  S_COLS                                    37

// CA-Mendo-PropChar.csv
#define  C_APN                                     0
#define  C_CLASSCODEDESCRIPTION                    1
#define  C_SITUSSTREETNUMBER                       2
#define  C_SITUSSTREETNUMBERSUFFIX                 3
#define  C_SITUSSTREETPREDIRECTIONAL               4
#define  C_SITUSSTREETNAME                         5
#define  C_SITUSSTREETTYPE                         6
#define  C_SITUSUNITNUMBER                         7
#define  C_SITUSZIPCODE                            8
#define  C_SITUSCITYNAME                           9
#define  C_STREETSURFACED                          10
#define  C_SEWER                                   11
#define  C_DOMESTICWATER                           12
#define  C_IRRIGATIONWATER                         13
#define  C_WELLWATER                               14
#define  C_UTILITYELECTRIC                         15
#define  C_UTILITYGAS                              16
#define  C_EFFYEARBUILT                            17
#define  C_YEARBUILT                               18
#define  C_NUMBEROFSTORIES                         19
#define  C_ROOFTYPE                                20
#define  C_GARAGETYPE                              21
#define  C_GARAGEAREA                              22
#define  C_CARPORTSIZE                             23
#define  C_GARAGE2TYPE                             24
#define  C_GARAGE2AREA                             25
#define  C_CARPORT2SIZE                            26
#define  C_HASFIREPLACE                            27
#define  C_BATHSFULL                               28
#define  C_BATHS3_4                                29
#define  C_BATHSHALF                               30
#define  C_TOTALBATHS                              31
#define  C_HASCENTRALHEATING                       32
#define  C_HASCENTRALCOOLING                       33
#define  C_DESIGNTYPE                              34
#define  C_CONSTRUCTIONTYPE                        35
#define  C_QUALITYCODE                             36
#define  C_SHAPECODE                               37
#define  C_LIVINGAREA                              38
#define  C_ACTUALAREA                              39
#define  C_HASPOOL                                 40
#define  C_BEDROOMCOUNT                            41
#define  C_FAIRWAY                                 42
#define  C_WATERFRONT                              43
#define  C_ACREAGE                                 44    // Added 04/10/2020
#define  C_LANDUNITTYPE                            45
#define  C_COLS                                    46

static XLAT_CODE  asRoofType[] =
{
   // Value, lookup code, value length
   "GABLE",                "G", 3,      
   "HIPVALLEY",            "O", 4,      
   "HIP",                  "H", 2,      
   "SHED",                 "K", 2,      
   "OTHER",                "J", 2,      
   "FLAT",                 "F", 2,      
   "GAMBREL",              "N", 3,      
   "AFRAME",               "A", 2,      
   "DORMER",               "K", 2,      
   "MANSARD",              "M", 2,      
   "PEAKED",               "J", 2,      // Other
   "",   "",  0
};

static XLAT_CODE  asConstType[] =
{
   // Value, lookup code, value length
   "MASONRY BEARING WALLS", "C", 2,
   "POLE FRAME",            "M", 2,    // Special
   "PRE-ENGINEERED STEEL",  "A", 2,
   "WOOD OR LIGHT STEEL",   "Z", 2,
   "",   "",  0
};

// CA-Mendo-CurrentAmounts.csv
#define  CA_PIN                                    0
#define  CA_BILLNUMBER                             1
#define  CA_TAXBILLID                              2
#define  CA_INST1DUEDATE                           3
#define  CA_INST2DUEDATE                           4
#define  CA_INST1TAXCHARGE                         5
#define  CA_INST1PENALTYCHARGE                     6
#define  CA_INST1FEECHARGE                         7
#define  CA_INST1INTERESTCHARGE                    8
#define  CA_INST1TAXPAYMENT                        9         // Paid value as negative
#define  CA_INST1PENALTYPAYMENT                    10
#define  CA_INST1FEEPAYMENT                        11
#define  CA_INST1INTERESTPAYMENT                   12
#define  CA_INST1AMOUNTDUE                         13
#define  CA_INST2TAXCHARGE                         14
#define  CA_INST2PENALTYCHARGE                     15
#define  CA_INST2FEECHARGE                         16
#define  CA_INST2INTERESTCHARGE                    17
#define  CA_INST2TAXPAYMENT                        18
#define  CA_INST2PENALTYPAYMENT                    19
#define  CA_INST2FEEPAYMENT                        20
#define  CA_INST2INTERESTPAYMENT                   21
#define  CA_INST2AMOUNTDUE                         22
#define  CA_COLS                                   23

// CA-Mendo-CurrentDistricts.csv
#define  CD_PIN                                    0
#define  CD_TAXRATEAREA                            1
#define  CD_TAXYEAR                                2
#define  CD_BILLNUMBER                             3
#define  CD_BILLTYPE                               4
#define  CD_TAXBILLID                              5
#define  CD_DETAILS                                6
#define  CD_TAXCODE                                7
#define  CD_RATE                                   8
#define  CD_TOTAL                                  9
#define  CD_COLS                                   10

// CA-Mendo-PriorYear.csv
#define  PY_PIN                                    0
#define  PY_DEFAULTNUMBER                          1
#define  PY_DEFAULTYEAR                            2
#define  PY_BILLTYPE                               3
#define  PY_AMOUNTDUE                              4
#define  PY_COLS                                   5

// Old layout ------------------------------------------------------
// 2021 Roll corretion
#define  MEN_COR_AIN                   0
#define  MEN_COR_PIN                   1
#define  MEN_COR_TAXYEAR               2
#define  MEN_COR_NEXTYEAR              3
#define  MEN_COR_ASMTTYPE              4
#define  MEN_COR_ROLLTYPE              5
#define  MEN_COR_TAG                   6
#define  MEN_COR_CLASSCD               7
#define  MEN_COR_COUNTYNAME            8
#define  MEN_COR_PRIMARYOWNER          9
#define  MEN_COR_RECIPIENT             10
#define  MEN_COR_DELIVERYADDR          11
#define  MEN_COR_LASTLINE              12
#define  MEN_COR_SITUSADDR             13
#define  MEN_COR_SITUSCITY             14
#define  MEN_COR_SITUSPOSTALCD         15
#define  MEN_COR_SITUSCOMPLETE         16
#define  MEN_COR_TOTALMARKET           17
#define  MEN_COR_TOTALFBYV             18
#define  MEN_COR_HOX                   19
#define  MEN_COR_DVX                   20
#define  MEN_COR_LDVX                  21
#define  MEN_COR_OTHEREXMPT            22
#define  MEN_COR_ASSDLAND              23
#define  MEN_COR_ASSDIMP               24
#define  MEN_COR_ASSDPERSONAL          25
#define  MEN_COR_ASSDFIXTURES          26
#define  MEN_COR_ASSDLIVIMP            27
#define  MEN_COR_ASSESSEDFULL          28
#define  MEN_COR_TOTALEXMPT            29
#define  MEN_COR_NETTAXABLE            30
#define  MEN_COR_ASMTTRANID            31
#define  MEN_COR_ID                    32
#define  MEN_COR_REVOBJID              33
#define  MEN_COR_PRINTDATE             34
#define  MEN_COR_AMAILWITHCOUNTYOF     35
#define  MEN_COR_AMAILWITHCOUNTY       36
#define  MEN_COR_AMAILCOMPLETE         37
#define  MEN_COR_ATTENTIONLINE         38
#define  MEN_COR_MAILCOMPLETE          39
#define  MEN_COR_MAILCOMPWITHATTN      40
#define  MEN_COR_FLDS                  41

// 2021 ANNUAL ROLLS - SEPARATED.TXT
#define  L_APN                      0
#define  L_TAXYEAR                  1
#define  L_NEXTYEAR                 2
#define  L_ASMTTYPE                 3
#define  L_ROLLTYPE                 4
#define  L_TRA                      5
#define  L_USECODEDESC              6
#define  L_OWNERX                   7        // Owner name - last name first, mostly blank
#define  L_OWNER                    8
#define  L_MADDR1                   9
#define  L_MADDR2                   10
#define  L_SADDR1                   11
#define  L_SCITY                    12
#define  L_SZIP                     13
#define  L_SITUS                    14
#define  L_TOTALMARKET              15
#define  L_TOTALFBYV                16
#define  L_HOEXE                    17
#define  L_EXE1                     18
#define  L_EXE2                     19
#define  L_OTHEREXE                 20
#define  L_LAND                     21
#define  L_IMPR                     22
#define  L_PPVAL                    23
#define  L_FIXTURES                 24
#define  L_LIVINGIMPR               25
#define  L_ASSDVAL                  26
#define  L_TOTALEXE                 27
#define  L_NETVAL                   28
#define  L_REVOBJID                 29
#define  L_ASMTTRANID               30
#define  L_ASMTID                   31
#define  L_PRINTDATE                32
#define  L_FLDS                     33

// Current secured tax file
#define  MEN_SEC_APN                0
#define  MEN_SEC_TRA                1
#define  MEN_SEC_NAME1              2
#define  MEN_SEC_BILL_NUM           3
#define  MEN_SEC_NAME2              4
#define  MEN_SEC_CAREOF             5
#define  MEN_SEC_EXCEPTION          6
#define  MEN_SEC_VESTING            7
#define  MEN_SEC_DBA                8
#define  MEN_SEC_MAR_ONE            9
#define  MEN_SEC_M_ADDR             10
#define  MEN_SEC_M_CITYST           11
#define  MEN_SEC_M_ZIP              12
#define  MEN_SEC_LANDUSE            13
#define  MEN_SEC_S_ADDR             14
#define  MEN_SEC_CORTAC             15
#define  MEN_SEC_LAND               16
#define  MEN_SEC_IMPR               17
#define  MEN_SEC_PERSPROP           18
#define  MEN_SEC_FIXTR              19
#define  MEN_SEC_OTHER              20
#define  MEN_SEC_HOME_EXE           21
#define  MEN_SEC_MISC_EXE           22
#define  MEN_SEC_ECODE              23    // Exe_Code
#define  MEN_SEC_EVET               24
#define  MEN_SEC_EMISC              25
#define  MEN_SEC_ACRES              26
#define  MEN_SEC_STS_DATE           27    // Default date offset 474
#define  MEN_SEC_SEG_FLAG           28
#define  MEN_SEC_GROSS_TAX          29
#define  MEN_SEC_HO_REF             30
#define  MEN_SEC_MISC_REF           31
#define  MEN_SEC_NET_TAX            32    // Decimal value
#define  MEN_SEC_TOT_BOND           33
#define  MEN_SEC_TOT_DIR            34
#define  MEN_SEC_TOT_PEN            35
#define  MEN_SEC_TOT_RATE           36
#define  MEN_SEC_AS_CODE_1          37
#define  MEN_SEC_AMT1_1             38
#define  MEN_SEC_AMT2_1             39
#define  MEN_SEC_AS_CODE_2          40
#define  MEN_SEC_AMT1_2             41
#define  MEN_SEC_AMT2_2             42
#define  MEN_SEC_AS_CODE_3          43
#define  MEN_SEC_AMT1_3             44
#define  MEN_SEC_AMT2_3             45
#define  MEN_SEC_AS_CODE_4          46
#define  MEN_SEC_AMT1_4             47
#define  MEN_SEC_AMT2_4             48
#define  MEN_SEC_AS_CODE_5          49
#define  MEN_SEC_AMT1_5             50
#define  MEN_SEC_AMT2_5             51
#define  MEN_SEC_AS_CODE_6          52
#define  MEN_SEC_AMT1_6             53
#define  MEN_SEC_AMT2_6             54
#define  MEN_SEC_AS_CODE_7          55
#define  MEN_SEC_AMT1_7             56
#define  MEN_SEC_AMT2_7             57
#define  MEN_SEC_AS_CODE_8          58
#define  MEN_SEC_AMT1_8             59
#define  MEN_SEC_AMT2_8             60
#define  MEN_SEC_AS_CODE_9          61
#define  MEN_SEC_AMT1_9             62
#define  MEN_SEC_AMT2_9             63
#define  MEN_SEC_AS_CODE_10         64
#define  MEN_SEC_AMT1_10            65
#define  MEN_SEC_AMT2_10            66
#define  MEN_SEC_AS_CODE_11         67
#define  MEN_SEC_AMT1_11            68
#define  MEN_SEC_AMT2_11            69
#define  MEN_SEC_AS_CODE_12         70
#define  MEN_SEC_AMT1_12            71
#define  MEN_SEC_AMT2_12            72
#define  MEN_SEC_AS_CODE_13         73
#define  MEN_SEC_AMT1_13            74
#define  MEN_SEC_AMT2_13            75
#define  MEN_SEC_AS_CODE_14         76
#define  MEN_SEC_AMT1_14            77
#define  MEN_SEC_AMT2_14            78
#define  MEN_SEC_AS_CODE_15         79
#define  MEN_SEC_AMT1_15            80
#define  MEN_SEC_AMT2_15            81

#define  MEN_SEC_I1_STAT            82    // P(aid), blank=unpaid, N(one) tax, C(orrection/cancel) offset 964
#define  MEN_SEC_I1_TAX             83    // V99
#define  MEN_SEC_I1_PEN             84    // V99
#define  MEN_SEC_I1_DUEDATE         85
#define  MEN_SEC_I1_PAIDDATE        86
#define  MEN_SEC_I1_BATCH           87

#define  MEN_SEC_I2_STAT            88
#define  MEN_SEC_I2_TAX             89
#define  MEN_SEC_I2_PEN             90
#define  MEN_SEC_I2_COST            91
#define  MEN_SEC_I2_DUEDATE         92
#define  MEN_SEC_I2_PAIDDATE        93
#define  MEN_SEC_I2_BATCH           94

#define  MEN_SEC_PAYOR              95
#define  MEN_SEC_CORR_TYPE          96
#define  MEN_SEC_BILL_ADJ           97
#define  MEN_SEC_ORIG_PRCL          98
#define  MEN_SEC_DMND_STMNT         99
#define  MEN_SEC_FD_02              100
#define  MEN_SEC_DRCT_ASSMT_RMVD    101
#define  MEN_SEC_LANDSCAPE          102
#define  MEN_SEC_ST_LTG_SUBDIV      103
#define  MEN_SEC_LAST_ACT_DATE      104
#define  MEN_SEC_PEN_CODE           105
#define  MEN_SEC_PEND_APPR          106
#define  MEN_SEC_BRUP               107

// Delinquent file
#define  MEN_DELQ_NAME              0
#define  MEN_DELQ_PRCL              1
#define  MEN_DELQ_NAME2             2
#define  MEN_DELQ_CAREOF            3
#define  MEN_DELQ_ADDR              4
#define  MEN_DELQ_CTY_ST            5
#define  MEN_DELQ_ZIP               6
#define  MEN_DELQ_ORIG_OWNR         7
#define  MEN_DELQ_ORIG_PRCL         8
#define  MEN_DELQ_A0_ASSMT_NBR      9
#define  MEN_DELQ_A0_YR             10
#define  MEN_DELQ_A0_PRCL           11    // Orig. parcel
#define  MEN_DELQ_A0_TYPE           12    // Secu(R)ed, (S)uplemental
#define  MEN_DELQ_A0_PAY_FLAG       13    // N ???
#define  MEN_DELQ_A0_AREA           14    // TRA
#define  MEN_DELQ_A0_TAX            15
#define  MEN_DELQ_A0_PEN            16
#define  MEN_DELQ_A0_COST           17
#define  MEN_DELQ_A0_AUDIT_AMT      18
#define  MEN_DELQ_A1_ASSMT_NBR      19
#define  MEN_DELQ_A1_YR             20
#define  MEN_DELQ_A1_PRCL           21
#define  MEN_DELQ_A1_TYPE           22
#define  MEN_DELQ_A1_PAY_FLAG       23
#define  MEN_DELQ_A1_AREA           24
#define  MEN_DELQ_A1_TAX            25
#define  MEN_DELQ_A1_PEN            26
#define  MEN_DELQ_A1_COST           27
#define  MEN_DELQ_A1_AUDIT_AMT      28
#define  MEN_DELQ_A2_ASSMT_NBR      29
#define  MEN_DELQ_A2_YR             30
#define  MEN_DELQ_A2_PRCL           31
#define  MEN_DELQ_A2_TYPE           32
#define  MEN_DELQ_A2_PAY_FLAG       33
#define  MEN_DELQ_A2_AREA           34
#define  MEN_DELQ_A2_TAX            35
#define  MEN_DELQ_A2_PEN            36
#define  MEN_DELQ_A2_COST           37
#define  MEN_DELQ_A2_AUDIT_AMT      38
#define  MEN_DELQ_A3_ASSMT_NBR      39
#define  MEN_DELQ_A3_YR             40
#define  MEN_DELQ_A3_PRCL           41
#define  MEN_DELQ_A3_TYPE           42
#define  MEN_DELQ_A3_PAY_FLAG       43
#define  MEN_DELQ_A3_AREA           44
#define  MEN_DELQ_A3_TAX            45
#define  MEN_DELQ_A3_PEN            46
#define  MEN_DELQ_A3_COST           47
#define  MEN_DELQ_A3_AUDIT_AMT      48
#define  MEN_DELQ_A4_ASSMT_NBR      49
#define  MEN_DELQ_A4_YR             50
#define  MEN_DELQ_A4_PRCL           51
#define  MEN_DELQ_A4_TYPE           52
#define  MEN_DELQ_A4_PAY_FLAG       53
#define  MEN_DELQ_A4_AREA           54
#define  MEN_DELQ_A4_TAX            55
#define  MEN_DELQ_A4_PEN            56
#define  MEN_DELQ_A4_COST           57
#define  MEN_DELQ_A4_AUDIT_AMT      58
#define  MEN_DELQ_A5_ASSMT_NBR      59
#define  MEN_DELQ_A5_YR             60
#define  MEN_DELQ_A5_PRCL           61
#define  MEN_DELQ_A5_TYPE           62
#define  MEN_DELQ_A5_PAY_FLAG       63
#define  MEN_DELQ_A5_AREA           64
#define  MEN_DELQ_A5_TAX            65
#define  MEN_DELQ_A5_PEN            66
#define  MEN_DELQ_A5_COST           67
#define  MEN_DELQ_A5_AUDIT_AMT      68
#define  MEN_DELQ_A6_ASSMT_NBR      69
#define  MEN_DELQ_A6_YR             70
#define  MEN_DELQ_A6_PRCL           71
#define  MEN_DELQ_A6_TYPE           72
#define  MEN_DELQ_A6_PAY_FLAG       73
#define  MEN_DELQ_A6_AREA           74
#define  MEN_DELQ_A6_TAX            75
#define  MEN_DELQ_A6_PEN            76
#define  MEN_DELQ_A6_COST           77
#define  MEN_DELQ_A6_AUDIT_AMT      78
#define  MEN_DELQ_A7_ASSMT_NBR      79
#define  MEN_DELQ_A7_YR             80
#define  MEN_DELQ_A7_PRCL           81
#define  MEN_DELQ_A7_TYPE           82
#define  MEN_DELQ_A7_PAY_FLAG       83
#define  MEN_DELQ_A7_AREA           84
#define  MEN_DELQ_A7_TAX            85
#define  MEN_DELQ_A7_PEN            86
#define  MEN_DELQ_A7_COST           87
#define  MEN_DELQ_A7_AUDIT_AMT      88
#define  MEN_DELQ_A8_ASSMT_NBR      89
#define  MEN_DELQ_A8_YR             90
#define  MEN_DELQ_A8_PRCL           91
#define  MEN_DELQ_A8_TYPE           92
#define  MEN_DELQ_A8_PAY_FLAG       93
#define  MEN_DELQ_A8_AREA           94
#define  MEN_DELQ_A8_TAX            95
#define  MEN_DELQ_A8_PEN            96
#define  MEN_DELQ_A8_COST           97
#define  MEN_DELQ_A8_AUDIT_AMT      98
#define  MEN_DELQ_A9_ASSMT_NBR      99
#define  MEN_DELQ_A9_YR             100
#define  MEN_DELQ_A9_PRCL           101
#define  MEN_DELQ_A9_TYPE           102
#define  MEN_DELQ_A9_PAY_FLAG       103
#define  MEN_DELQ_A9_AREA           104
#define  MEN_DELQ_A9_TAX            105
#define  MEN_DELQ_A9_PEN            106
#define  MEN_DELQ_A9_COST           107
#define  MEN_DELQ_A9_AUDIT_AMT      108
#define  MEN_DELQ_STATE_FEE         109
#define  MEN_DELQ_TAX_DEF_AMT       110
#define  MEN_DELQ_TAX_DEF_DATE      111
#define  MEN_DELQ_TAX_DEF_NR        112
#define  MEN_DELQ_PLAN_DATE         113
#define  MEN_DELQ_ACT_DATE          114
#define  MEN_DELQ_RDM_DATE          115
#define  MEN_DELQ_RDM_AMT           116
#define  MEN_DELQ_TOT_TAX           117
#define  MEN_DELQ_TOT_PEN           118
#define  MEN_DELQ_TOT_COST          119
#define  MEN_DELQ_TDL_FEE           120
#define  MEN_DELQ_REDEEM_FEE        121
#define  MEN_DELQ_LAST_ACT          122      // P=Paid, R=Redeem (bad check)
#define  MEN_DELQ_ACT_BATCH         123
#define  MEN_DELQ_ORIG_PLAN_DATE    124
#define  MEN_DELQ_BRUP              125
#define  MEN_DELQ_FD_02             126
#define  MEN_DELQ_DRCT_ASSMT_RMVD   127      // No data
#define  MEN_DELQ_ADJ_CODE          128      // No data
#define  MEN_DELQ_ADJ_AMT           129
#define  MEN_DELQ_MISC_FLAG         130      // A,V,S=Skip all penalties
#define  MEN_DELQ_RDM_SW            131
#define  MEN_DELQ_RDM_STAT          132      // P=paid
#define  MEN_DELQ_INST_INT_X        133
#define  MEN_DELQ_REM               134

#define  OFF_DELQ_NAME              1
#define  OFF_DELQ_PRCL              32-1
#define  OFF_DELQ_NAME2             47
#define  OFF_DELQ_CAREOF            78
#define  OFF_DELQ_ADDR              104
#define  OFF_DELQ_CTY_ST            135
#define  OFF_DELQ_ZIP               161
#define  OFF_DELQ_ORIG_OWNR         172
#define  OFF_DELQ_ORIG_PRCL         233
#define  OFF_DELQ_A0_ASSMT_NBR      244
#define  OFF_DELQ_A0_YR             251
#define  OFF_DELQ_A0_PRCL           256    
#define  OFF_DELQ_A0_TYPE           267
#define  OFF_DELQ_A0_PAY_FLAG       269
#define  OFF_DELQ_A0_AREA           271
#define  OFF_DELQ_A0_TAX            278
#define  OFF_DELQ_A0_PEN            288
#define  OFF_DELQ_A0_COST           297
#define  OFF_DELQ_A0_AUDIT_AMT      303
#define  OFF_DELQ_A1_ASSMT_NBR      313
#define  OFF_DELQ_A2_ASSMT_NBR      382
#define  OFF_DELQ_A3_ASSMT_NBR      451
#define  OFF_DELQ_A4_ASSMT_NBR      520
#define  OFF_DELQ_A5_ASSMT_NBR      589
#define  OFF_DELQ_A6_ASSMT_NBR      658
#define  OFF_DELQ_A7_ASSMT_NBR      727
#define  OFF_DELQ_A8_ASSMT_NBR      796
#define  OFF_DELQ_A9_ASSMT_NBR      865
#define  OFF_DELQ_STATE_FEE         934
#define  OFF_DELQ_TAX_DEF_AMT       940
#define  OFF_DELQ_TAX_DEF_DATE      950
#define  OFF_DELQ_TAX_DEF_NR        959
#define  OFF_DELQ_PLAN_DATE         964
#define  OFF_DELQ_ACT_DATE          973
#define  OFF_DELQ_RDM_DATE          982
#define  OFF_DELQ_RDM_AMT           991
#define  OFF_DELQ_TOT_TAX           1001
#define  OFF_DELQ_TOT_PEN           1012
#define  OFF_DELQ_TOT_COST          1021
#define  OFF_DELQ_TDL_FEE           1030
#define  OFF_DELQ_REDEEM_FEE        1033
#define  OFF_DELQ_LAST_ACT          1036
#define  OFF_DELQ_ACT_BATCH         1038
#define  OFF_DELQ_ORIG_PLAN_DATE    1045
#define  OFF_DELQ_BRUP              1054
#define  OFF_DELQ_FD_02             1070
#define  OFF_DELQ_DRCT_ASSMT_RMVD   1091
#define  OFF_DELQ_ADJ_CODE          1093
#define  OFF_DELQ_ADJ_AMT           1095
#define  OFF_DELQ_MISC_FLAG         1106
#define  OFF_DELQ_RDM_SW            1108
#define  OFF_DELQ_RDM_STAT          1110
#define  OFF_DELQ_INST_INT_X        1112
#define  OFF_DELQ_REM               1122

// Supplement file
#define  MEN_SUPP_APN               1
#define  MEN_SUPP_BILLNUM           15
#define  MEN_SUPP_NETTAX            21
#define  MEN_SUPP_TOLALPEN          29
#define  MEN_SUPP_I1_STAT           36
#define  MEN_SUPP_I1_TAXAMT         37
#define  MEN_SUPP_I1_PENAMT         45
#define  MEN_SUPP_I1_DELQDATE       52
#define  MEN_SUPP_I1_PAIDDATE       60
#define  MEN_SUPP_I1_BATCH          68
#define  MEN_SUPP_I2_STAT           74
#define  MEN_SUPP_I2_TAXAMT         75
#define  MEN_SUPP_I2_PENAMT         83
#define  MEN_SUPP_I2_COST           90
#define  MEN_SUPP_I2_DELQDATE       94
#define  MEN_SUPP_I2_PAIDDATE       102
#define  MEN_SUPP_I2_BATCH          110
#define  MEN_SUPP_USECODE           116
#define  MEN_SUPP_EVENTDATE         124
#define  MEN_SUPP_NOTEDATE          132
#define  MEN_SUPP_DUEDATE           140
#define  MEN_SUPP_TYPE              148
#define  MEN_SUPP_ROLLYEAR          149
#define  MEN_SUPP_ROLLFCTR          153
#define  MEN_SUPP_ROLLSUPP          156

#define  MEN_SSIZ_APN               10
#define  MEN_SSIZ_APNSEQ            2
#define  MEN_SSIZ_CPP_FLG           2
#define  MEN_SSIZ_BILLNUM           6
#define  MEN_SSIZ_NETTAX            8
#define  MEN_SSIZ_TOLALPEN          7
#define  MEN_SSIZ_I1_STAT           1
#define  MEN_SSIZ_I1_TAXAMT         8
#define  MEN_SSIZ_I1_PENAMT         7
#define  MEN_SSIZ_I1_DELQDATE       8
#define  MEN_SSIZ_I1_PAIDDATE       8
#define  MEN_SSIZ_I1_BATCH          6
#define  MEN_SSIZ_I2_STAT           1
#define  MEN_SSIZ_I2_TAXAMT         8
#define  MEN_SSIZ_I2_PENAMT         7
#define  MEN_SSIZ_I2_COST           4
#define  MEN_SSIZ_I2_DELQDATE       8
#define  MEN_SSIZ_I2_PAIDDATE       8
#define  MEN_SSIZ_I2_BATCH          6
#define  MEN_SSIZ_USECODE           8
#define  MEN_SSIZ_EVENTDATE         8
#define  MEN_SSIZ_NOTEDATE          8
#define  MEN_SSIZ_DUEDATE           8
#define  MEN_SSIZ_TYPE              1
#define  MEN_SSIZ_ROLLYEAR          4
#define  MEN_SSIZ_ROLLFCTR          3
#define  MEN_SSIZ_ROLLSUPP          1

typedef struct _tMenSupp
{
   char  Apn[10];          // Parcel is 10 characters
   char  ApnSeq[2];        // Seq is 2 characters
   char  CP_Indicator[2];  // current/prior/prorate indicators 2 characters
   char  BillNum[6];       // 
   char  NetTax[8];        // last byte sometimes contains alpha which indicates negative value.
   char  TotalPen[7];      // Total Penalty if Applicable
   char  I1_Status[1];     // P= paid, N=not billed, R=Refund, " " =Unpaid, C=Canceled
                           // If this is R, NetTax often negative value
   char  I1_TaxAmt[8];     // last byte sometimes alpha ???
   char  I1_PenAmt[7];
   char  I1_DelqDate[8];   // Installment 1 Payment Date to Avoid Penalty Amt above          
   char  I1_PaidDate[8];
   char  I1_Batch[6];
   char  I2_Status[1];     // P= paid, N=not billed, R=Refund, " " =Unpaid, C=Canceled
   char  I2_TaxAmt[8];     // last byte may be alpha which indicates negative value
   char  I2_PenAmt[7];
   char  I2_Cost[4];
   char  I2_DelqDate[8];
   char  I2_PaidDate[8];
   char  I2_Batch[6];
   char  UseCode[8];       // Property's Use Code e.g. Cxxx - Commerical   R - xxx Residential
   char  EventDate[8];     // When the transfer or new construction occurred
   char  NoteDate[8];      // When notice was sent that a bill was forth coming
   char  DueDate[8];       // Date bill was due (from this date till penalty date bill can be paid w/o penalty)
   char  Type[1];          // Denotes 'S'ecured or 'U'nsecured property being billed
   char  RollYear[4];      // i.e. 2009 for 2009-2010
   char  RollFctr[3];      // Factor for the part of the year the billing is for
   char  RollSupp[1];      // 'C'urrent, 'P'rior, or 'N'ext roll
   char  CrLf[2];
} MEN_SUPP;


#define HSIZ_APN                      10
#define HSIZ_TYPE                     3
#define HSIZ_REC_BOOK                 4
#define HSIZ_REC_PAGE                 3
#define HSIZ_DEED_NO                  5
#define HSIZ_REC_DATE                 8
#define HSIZ_NAME                     28
#define HSIZ_VESTING                  10
#define HSIZ_PER_OWNERSHIP            5
#define HSIZ_STAMP_AMT                6
#define HSIZ_DTT_CODE                 1

#define HOFF_APN                      1
#define HOFF_TYPE                     11
#define HOFF_REC_BOOK                 14
#define HOFF_REC_PAGE                 18
#define HOFF_DEED_NO                  21
#define HOFF_REC_DATE                 26
#define HOFF_NAME                     34
#define HOFF_VESTING                  62
#define HOFF_PER_OWNERSHIP            72
#define HOFF_STAMP_AMT                77
#define HOFF_DTT_CODE                 83

typedef struct _tMenHist
{
   char Apn[HSIZ_APN];
   char Type[HSIZ_TYPE];               // A=assessed, P=purchase
   char Rec_Book[HSIZ_REC_BOOK];
   char Rec_Page[HSIZ_REC_PAGE];
   char Deed_No[HSIZ_DEED_NO];
   char Rec_Date[HSIZ_REC_DATE];
   char Name[HSIZ_NAME];
   char Vesting[HSIZ_VESTING];
   char Pct_Own[HSIZ_PER_OWNERSHIP];
   char Stamp_Amt[HSIZ_STAMP_AMT];
   char Dtt_Code[HSIZ_DTT_CODE];
   char Filler1[9];
} MEN_HIST;

#define AHSIZ_APN                      10
#define AHSIZ_TYPE                     3
#define AHSIZ_REC_BOOK                 4
#define AHSIZ_REC_PAGE                 3
#define AHSIZ_DEED_NO                  5
#define AHSIZ_REC_DATE                 8
#define AHSIZ_NAME                     28
#define AHSIZ_VESTING                  10
#define AHSIZ_PER_OWNERSHIP            6
#define AHSIZ_STAMP_AMT                7
#define AHSIZ_DTT_CODE                 1
#define AHSIZ_SALE_AMT                 10
#define AHSIZ_FILLER                   5

#define AHOFF_APN                      1
#define AHOFF_TYPE                     11
#define AHOFF_REC_BOOK                 14
#define AHOFF_REC_PAGE                 18
#define AHOFF_DEED_NO                  21
#define AHOFF_REC_DATE                 26
#define AHOFF_NAME                     34
#define AHOFF_VESTING                  62
#define AHOFF_PER_OWNERSHIP            72
#define AHOFF_STAMP_AMT                78
#define AHOFF_DTT_CODE                 85
#define AHOFF_SALE_AMT                 86
#define AHOFF_FILLER                   96

typedef struct _tSalerec
{
   char Rec_Book[AHSIZ_REC_BOOK];
   char Rec_Page[AHSIZ_REC_PAGE];
   char Deed_No[AHSIZ_DEED_NO];
   char Rec_Date[AHSIZ_REC_DATE];
   char Name[AHSIZ_NAME];
   char Vesting[AHSIZ_VESTING];
   char Pct_Own[AHSIZ_PER_OWNERSHIP];  // V99
   char Stamp_Amt[AHSIZ_STAMP_AMT];    // V99
   char Dtt_Code[AHSIZ_DTT_CODE];
   char SaleAmt[AHSIZ_SALE_AMT];
} MEN_SALE;

typedef struct _tMenHist_Assr
{  // 100-byte
   char Apn[AHSIZ_APN];
   char Type[AHSIZ_TYPE];
   char Rec_Book[AHSIZ_REC_BOOK];
   char Rec_Page[AHSIZ_REC_PAGE];
   char Deed_No[AHSIZ_DEED_NO];
   char Rec_Date[AHSIZ_REC_DATE];
   char Name[AHSIZ_NAME];
   char Vesting[AHSIZ_VESTING];
   char Pct_Own[AHSIZ_PER_OWNERSHIP];  // V99
   char Stamp_Amt[AHSIZ_STAMP_AMT];    // V99
   char Dtt_Code[AHSIZ_DTT_CODE];
   char SaleAmt[AHSIZ_SALE_AMT];
   char Filler[AHSIZ_FILLER];
} MEN_AHIST;

#define USIZ_STATUS                        1
#define USIZ_STATEMENT_TYPE                1
#define USIZ_OWNER                         44
#define USIZ_BUS_CODE                      3
#define USIZ_ACCT_NUMBER                   7
#define USIZ_ACCT_SUB_NUMBER               3
#define USIZ_DBA                           44
#define USIZ_ALT_ACCT                      13
#define USIZ_CAREOF                        44
#define USIZ_MAIL_ADDR                     25
#define USIZ_MAIL_CITY_STATE               25
#define USIZ_MAIL_ZIP                      9
#define USIZ_PROPERTY_LEGAL_DESC           24
#define USIZ_TRA                           6
#define USIZ_LAND                          9
#define USIZ_IMPR                          9
#define USIZ_PERS_PROP                     9
#define USIZ_TRADE_FIXT                    9
#define USIZ_EXEMP                         9
#define USIZ_COST                          9
#define USIZ_BOAT_VALUE                    9
#define USIZ_EXEMP_CODE                    2
#define USIZ_BOAT_BUILD_YEAR               4
#define USIZ_APN                           10
#define USIZ_SBENR_PREFIX                  2
#define USIZ_SBENR_SUFFIX                  6
#define USIZ_IND_CODE_1                    1
#define USIZ_IND_CODE_2                    1
#define USIZ_PERS_PROP_PENALTY_CODE        2
#define USIZ_TAX_RATE                      7
#define USIZ_TAX_AMT                       9
#define USIZ_INT_RATE                      5
#define USIZ_INT_AMT                       9
#define USIZ_BILL_NU                       5
#define USIZ_BILL_DATE                     8
#define USIZ_MAINT_DATE                    8
#define USIZ_STATEMENT_DATE                8
#define USIZ_EXEMP_PERCENT                 2
#define USIZ_STREET_NAME                   30
#define USIZ_STREET_NUMBER                 5
#define USIZ_UNIT                          2
#define USIZ_BLOCK_LEGAL                   3
#define USIZ_LOT_LEGAL                     3
#define USIZ_CITY_CODE                     2
#define USIZ_BENAS_CODE_1                  1
#define USIZ_BENAS_CODE_2                  1
#define USIZ_BAD_EX_CODE_1                 1
#define USIZ_BAD_EX_CODE_2                 1
#define USIZ_FMT                           1
#define USIZ_BPS_DATE                      8
#define USIZ_PHONE                         10

#define UOFF_STATUS                        1
#define UOFF_STATEMENT_TYPE                2
#define UOFF_OWNER                         3
#define UOFF_BUS_CODE                      47
#define UOFF_ACCT_NUMBER                   50
#define UOFF_ACCT_SUB_NUMBER               57
#define UOFF_DBA                           60
#define UOFF_ALT_ACCT                      104
#define UOFF_CAREOF                        117
#define UOFF_MAIL_ADDR                     161
#define UOFF_MAIL_CITY_STATE               186
#define UOFF_MAIL_ZIP                      211
#define UOFF_PROPERTY_LEGAL_DESC           220
#define UOFF_TRA                           244
#define UOFF_LAND                          250
#define UOFF_IMPR                          259
#define UOFF_PERS_PROP                     268
#define UOFF_TRADE_FIXT                    277
#define UOFF_EXEMP                         286
#define UOFF_COST                          295
#define UOFF_BOAT_VALUE                    304
#define UOFF_EXEMP_CODE                    313
#define UOFF_BOAT_BUILD_YEAR               315
#define UOFF_APN                           319
#define UOFF_SBENR_PREFIX                  329
#define UOFF_SBENR_SUFFIX                  331
#define UOFF_IND_CODE_1                    337
#define UOFF_IND_CODE_2                    338
#define UOFF_PERS_PROP_PENALTY_CODE        339
#define UOFF_TAX_RATE                      341
#define UOFF_TAX_AMT                       348
#define UOFF_INT_RATE                      357
#define UOFF_INT_AMT                       362
#define UOFF_BILL_NU                       371
#define UOFF_BILL_DATE                     376
#define UOFF_MAINT_DATE                    384
#define UOFF_STATEMENT_DATE                392
#define UOFF_EXEMP_PERCENT                 400
#define UOFF_STREET_NAME                   402
#define UOFF_STREET_NUMBER                 432
#define UOFF_UNIT                          437
#define UOFF_BLOCK_LEGAL                   439
#define UOFF_LOT_LEGAL                     442
#define UOFF_CITY_CODE                     445
#define UOFF_BENAS_CODE_1                  447
#define UOFF_BENAS_CODE_2                  448
#define UOFF_BAD_EX_CODE_1                 449
#define UOFF_BAD_EX_CODE_2                 450
#define UOFF_FMT                           451
#define UOFF_BPS_DATE                      452
#define UOFF_PHONE                         460
#define UOFF_FILLER1                       470

typedef struct _tMenUnsecRoll
{
   char Status[USIZ_STATUS];
   char Statement_Type[USIZ_STATEMENT_TYPE];
   char Owner[USIZ_OWNER];
   char Bus_Code[USIZ_BUS_CODE];
   char Acct_Number[USIZ_ACCT_NUMBER];
   char Acct_Sub_Number[USIZ_ACCT_SUB_NUMBER];
   char Dba[USIZ_DBA];
   char Alt_Acct[USIZ_ALT_ACCT];
   char Careof[USIZ_CAREOF];
   char Mail_Addr[USIZ_MAIL_ADDR];
   char Mail_City_State[USIZ_MAIL_CITY_STATE];
   char Mail_Zip[USIZ_MAIL_ZIP];
   char Property_Legal_Desc[USIZ_PROPERTY_LEGAL_DESC];
   char Tra[USIZ_TRA];
   char Land[USIZ_LAND];
   char Impr[USIZ_IMPR];
   char Pers_Prop[USIZ_PERS_PROP];
   char Trade_Fixt[USIZ_TRADE_FIXT];
   char Exemp[USIZ_EXEMP];
   char Cost[USIZ_COST];
   char Exemp_Code[USIZ_EXEMP_CODE];
   char Boat_Value[USIZ_BOAT_VALUE];
   char Boat_Build_Year[USIZ_BOAT_BUILD_YEAR];
   char Apn[USIZ_APN];
   char Sbenr_Prefix[USIZ_SBENR_PREFIX];
   char Sbenr_Suffix[USIZ_SBENR_SUFFIX];
   char Ind_Code_1[USIZ_IND_CODE_1];
   char Ind_Code_2[USIZ_IND_CODE_2];
   char Pers_Prop_Penalty_Code[USIZ_PERS_PROP_PENALTY_CODE];
   char Tax_Rate[USIZ_TAX_RATE];
   char Tax_Amt[USIZ_TAX_AMT];
   char Int_Rate[USIZ_INT_RATE];
   char Int_Amt[USIZ_INT_AMT];
   char Bill_Nu[USIZ_BILL_NU];
   char Bill_Date[USIZ_BILL_DATE];
   char Maint_Date[USIZ_MAINT_DATE];
   char Statement_Date[USIZ_STATEMENT_DATE];
   char Exemp_Percent[USIZ_EXEMP_PERCENT];
   char Street_Name[USIZ_STREET_NAME];
   char Street_Number[USIZ_STREET_NUMBER];
   char Unit[USIZ_UNIT];
   char Block_Legal[USIZ_BLOCK_LEGAL];
   char Lot_Legal[USIZ_LOT_LEGAL];
   char City_Code[USIZ_CITY_CODE];
   char Benas_Code_1[USIZ_BENAS_CODE_1];
   char Benas_Code_2[USIZ_BENAS_CODE_2];
   char Bad_Ex_Code_1[USIZ_BAD_EX_CODE_1];
   char Bad_Ex_Code_2[USIZ_BAD_EX_CODE_2];
   char Fmt[USIZ_FMT];
   char Bps_Date[USIZ_BPS_DATE];
   char Phone[USIZ_PHONE];
   char Filler1[14];
} MEN_UNS;

#define AUSIZ_STATUS                        1
#define AUSIZ_STATEMENT_TYPE                1
#define AUSIZ_OWNER                         44
#define AUSIZ_BUS_CODE                      3
#define AUSIZ_ACCT_NUMBER                   7
#define AUSIZ_ACCT_SUB_NUMBER               3
#define AUSIZ_DBA                           44
#define AUSIZ_ALT_ACCT                      13
#define AUSIZ_CAREOF                        44
#define AUSIZ_MAIL_ADDR                     25
#define AUSIZ_MAIL_CITY_STATE               25
#define AUSIZ_MAIL_ZIP                      9
#define AUSIZ_PROPERTY_LEGAL_DESC           24
#define AUSIZ_TRA                           6
#define AUSIZ_LAND                          9
#define AUSIZ_IMPR                          9
#define AUSIZ_PERS_PROP                     9
#define AUSIZ_TRADE_FIXT                    9
#define AUSIZ_EXEMP                         9
#define AUSIZ_COST                          9
#define AUSIZ_EXEMP_CODE                    2
#define AUSIZ_BOAT_VALUE                    9
#define AUSIZ_BOAT_BUILD_YEAR               4
#define AUSIZ_APN                           10
#define AUSIZ_SBENR_PREFIX                  2
#define AUSIZ_SBENR_SUFFIX                  6
#define AUSIZ_IND_CODE_1                    1
#define AUSIZ_IND_CODE_2                    1
#define AUSIZ_PERS_PROP_PENALTY_CODE        2

#define AUSIZ_TAX_RATE                      8
#define AUSIZ_TAX_AMT                       10
#define AUSIZ_INT_RATE                      6
#define AUSIZ_INT_AMT                       10
#define AUSIZ_BILL_NU                       5
#define AUSIZ_BILL_DATE                     8
#define AUSIZ_MAINT_DATE                    8
#define AUSIZ_STATEMENT_DATE                8
#define AUSIZ_EXEMP_PERCENT                 2
#define AUSIZ_STREET_NAME                   30
#define AUSIZ_STREET_NUMBER                 5
#define AUSIZ_UNIT                          2
#define AUSIZ_BLOCK_LEGAL                   3
#define AUSIZ_LOT_LEGAL                     3
#define AUSIZ_CITY_CODE                     2
#define AUSIZ_BENAS_CODE_1                  1
#define AUSIZ_BENAS_CODE_2                  1
#define AUSIZ_BAD_EX_CODE_1                 1
#define AUSIZ_BAD_EX_CODE_2                 1
#define AUSIZ_FMT                           1
#define AUSIZ_BPS_DATE                      8
#define AUSIZ_PHONE                         10
#define AUSIZ_FILLER1                       3
#define AUSIZ_GROSS                         9
#define AUSIZ_NET                           9
#define AUSIZ_LAND_IMPR                     9
#define AUSIZ_RATIO                         3
#define AUSIZ_FILLER2                       6


#define AUOFF_STATUS                        1
#define AUOFF_STATEMENT_TYPE                2
#define AUOFF_OWNER                         3
#define AUOFF_BUS_CODE                      47
#define AUOFF_ACCT_NUMBER                   50
#define AUOFF_ACCT_SUB_NUMBER               57
#define AUOFF_DBA                           60
#define AUOFF_ALT_ACCT                      104
#define AUOFF_CAREOF                        117
#define AUOFF_MAIL_ADDR                     161
#define AUOFF_MAIL_CITY_STATE               186
#define AUOFF_MAIL_ZIP                      211
#define AUOFF_PROPERTY_LEGAL_DESC           220
#define AUOFF_TRA                           244
#define AUOFF_LAND                          250
#define AUOFF_IMPR                          259
#define AUOFF_PERS_PROP                     268
#define AUOFF_TRADE_FIXT                    277
#define AUOFF_EXEMP                         286
#define AUOFF_COST                          295
#define AUOFF_BOAT_VALUE                    304
#define AUOFF_EXEMP_CODE                    313
#define AUOFF_BOAT_BUILD_YEAR               315
#define AUOFF_APN                           319
#define AUOFF_SBENR_PREFIX                  329
#define AUOFF_SBENR_SUFFIX                  331
#define AUOFF_IND_CODE_1                    337
#define AUOFF_IND_CODE_2                    338
#define AUOFF_PERS_PROP_PENALTY_CODE        339
#define AUOFF_TAX_RATE                      341
#define AUOFF_TAX_AMT                       349
#define AUOFF_INT_RATE                      359
#define AUOFF_INT_AMT                       365
#define AUOFF_BILL_NU                       375
#define AUOFF_BILL_DATE                     380
#define AUOFF_MAINT_DATE                    388
#define AUOFF_STATEMENT_DATE                396
#define AUOFF_EXEMP_PERCENT                 404
#define AUOFF_STREET_NAME                   406
#define AUOFF_STREET_NUMBER                 436
#define AUOFF_UNIT                          441
#define AUOFF_BLOCK_LEGAL                   443
#define AUOFF_LOT_LEGAL                     446
#define AUOFF_CITY_CODE                     449
#define AUOFF_BENAS_CODE_1                  451
#define AUOFF_BENAS_CODE_2                  452
#define AUOFF_BAD_EX_CODE_1                 453
#define AUOFF_BAD_EX_CODE_2                 454
#define AUOFF_FMT                           455
#define AUOFF_BPS_DATE                      456
#define AUOFF_PHONE                         464
#define AUOFF_FILLER1                       474
#define AUOFF_GROSS                         477
#define AUOFF_NET                           486
#define AUOFF_LAND_IMPR                     495
#define AUOFF_RATIO                         504
#define AUOFF_FILLER2                       507

typedef struct _tMenUnsecRoll_Assr
{  // 512-byte
   char Status[AUSIZ_STATUS];
   char Statement_Type[AUSIZ_STATEMENT_TYPE];
   char Owner[AUSIZ_OWNER];
   char Bus_Code[AUSIZ_BUS_CODE];
   char Acct_Number[AUSIZ_ACCT_NUMBER];
   char Acct_Sub_Number[AUSIZ_ACCT_SUB_NUMBER];
   char Dba[AUSIZ_DBA];
   char Alt_Acct[AUSIZ_ALT_ACCT];
   char Careof[AUSIZ_CAREOF];
   char Mail_Addr[AUSIZ_MAIL_ADDR];
   char Mail_City_State[AUSIZ_MAIL_CITY_STATE];
   char Mail_Zip[AUSIZ_MAIL_ZIP];
   char Property_Legal_Desc[AUSIZ_PROPERTY_LEGAL_DESC];
   char Tra[AUSIZ_TRA];
   char Land[AUSIZ_LAND];
   char Impr[AUSIZ_IMPR];
   char Pers_Prop[AUSIZ_PERS_PROP];
   char Trade_Fixt[AUSIZ_TRADE_FIXT];
   char Exemp[AUSIZ_EXEMP];
   char Cost[AUSIZ_COST];
   char Boat_Value[AUSIZ_BOAT_VALUE];
   char Exemp_Code[AUSIZ_EXEMP_CODE];
   char Boat_Build_Year[AUSIZ_BOAT_BUILD_YEAR];
   char Apn[AUSIZ_APN];
   char Sbenr_Prefix[AUSIZ_SBENR_PREFIX];
   char Sbenr_Suffix[AUSIZ_SBENR_SUFFIX];
   char Ind_Code_1[AUSIZ_IND_CODE_1];
   char Ind_Code_2[AUSIZ_IND_CODE_2];
   char Pers_Prop_Penalty_Code[AUSIZ_PERS_PROP_PENALTY_CODE];
   char Tax_Rate[AUSIZ_TAX_RATE];         // .99999
   char Tax_Amt[AUSIZ_TAX_AMT];           // .99
   char Int_Rate[AUSIZ_INT_RATE];         // .9999
   char Int_Amt[AUSIZ_INT_AMT];           // .99
   char Bill_Nu[AUSIZ_BILL_NU];
   char Bill_Date[AUSIZ_BILL_DATE];
   char Maint_Date[AUSIZ_MAINT_DATE];
   char Statement_Date[AUSIZ_STATEMENT_DATE];
   char Exemp_Percent[AUSIZ_EXEMP_PERCENT];
   char Street_Name[AUSIZ_STREET_NAME];
   char Street_Number[AUSIZ_STREET_NUMBER];
   char Unit[AUSIZ_UNIT];
   char Block_Legal[AUSIZ_BLOCK_LEGAL];
   char Lot_Legal[AUSIZ_LOT_LEGAL];
   char City_Code[AUSIZ_CITY_CODE];
   char Benas_Code_1[AUSIZ_BENAS_CODE_1];
   char Benas_Code_2[AUSIZ_BENAS_CODE_2];
   char Bad_Ex_Code_1[AUSIZ_BAD_EX_CODE_1];
   char Bad_Ex_Code_2[AUSIZ_BAD_EX_CODE_2];
   char Fmt[AUSIZ_FMT];
   char Bps_Date[AUSIZ_BPS_DATE];
   char Phone[AUSIZ_PHONE];
   char Filler1[AUSIZ_FILLER1];
   char Gross[AUSIZ_GROSS];
   char Net[AUSIZ_NET];
   char LandImpr[AUSIZ_LAND_IMPR];
   char Ratio[AUSIZ_RATIO];
   char Filler2[AUSIZ_FILLER2];
} MEN_AUNS;

#define RSIZ_STATUS                    1
#define RSIZ_NAME1                     30
#define RSIZ_APN                       10
#define RSIZ_NAME2                     30
#define RSIZ_CAREOF                    30
#define RSIZ_EXCEPTION                 12
#define RSIZ_DBA                       11
#define RSIZ_UNSEC_PP                  9
#define RSIZ_ZONING                    6
#define RSIZ_MINSIZE                   4
#define RSIZ_LIEN_NAME1                30
#define RSIZ_LIEN_NAME2                30
#define RSIZ_MAIL_ADDR                 30
#define RSIZ_MAIL_CITY_STATE           25
#define RSIZ_MAIL_ZIP                  9
#define RSIZ_SITUS_STREET_NAME         30
#define RSIZ_SITUS_STREET_NUMBER       5
#define RSIZ_SITUS_DIR                 2
#define RSIZ_SITUS_STR_SUB             3
#define RSIZ_SITUS_UNIT                3
#define RSIZ_CITY_CODE                 2
#define RSIZ_THIRD_APN                 10
#define RSIZ_TRA                       6
#define RSIZ_LAND                      9
#define RSIZ_STRUCTURE                 9
#define RSIZ_TREES                     9
#define RSIZ_TRADE_FIXT                9
#define RSIZ_PERS_PROP                 9
#define RSIZ_HOMEOWNER_EXEMP           9
#define RSIZ_OTHER_EXEMP               9
#define RSIZ_WILLIAMSON_LAND           9
#define RSIZ_WILLIAMSON_TREES          9
#define RSIZ_MSG_CODE_1                1
#define RSIZ_MSG_CODE_2                1
#define RSIZ_EXEMP_CODE                1
#define RSIZ_VETERAN_CODE              1
#define RSIZ_USE_CODE_1                4
#define RSIZ_USE_CODE_2                4
#define RSIZ_ACRES                     7
#define RSIZ_REC_BOOK                  4
#define RSIZ_REC_PAGE                  3
#define RSIZ_DEED_NO                   5
#define RSIZ_REC_DATE                  8
#define RSIZ_SUBDIVISION_CODE          1
#define RSIZ_SPLIT_APN_1               10
#define RSIZ_SPLIT_APN_2               10
#define RSIZ_SPLIT_APN_3               10
#define RSIZ_SPLIT_APN_4               10
#define RSIZ_APPRAISAL_DATE            8
#define RSIZ_MAINT_DATE                8
#define RSIZ_PRIOR_YEAR_CODE           1
#define RSIZ_V_CHG_CODE                2
#define RSIZ_BEN_ASS_CODE              2
#define RSIZ_SQFT_1                    7
#define RSIZ_SQFT_2                    7
#define RSIZ_SQFT_3                    7
#define RSIZ_ETAL_FLAG                 1
#define RSIZ_PRIOR_LAND                9
#define RSIZ_PRIOR_STRUC               9
#define RSIZ_PRIOR_TREES               9
#define RSIZ_PRIOR_TRADE_FIXT          9
#define RSIZ_PRIOR_PERS_PROP           9
#define RSIZ_PRIOR_HOMEOWNER_EXEMP     9
#define RSIZ_PRIOR_OTHER_EXEMP         9
#define RSIZ_PRIOR_MSG_CODE_1          1
#define RSIZ_PRIOR_MSG_CODE_2          1
#define RSIZ_APPR_OPEN_DT_1            8
#define RSIZ_APPR_CLOSE_DT_1           8
#define RSIZ_APPR_OPEN_DT_2            8
#define RSIZ_APPR_CLOSE_DT_2           8
#define RSIZ_APPR_OPEN_DT_3            8
#define RSIZ_APPR_CLOSE_DT_3           8
#define RSIZ_APPR_OPEN_DT_4            8
#define RSIZ_APPR_CLOSE_DT_4           8
#define RSIZ_ASSMT_STAT                1
#define RSIZ_STATEMENT_TYPE            1
#define RSIZ_STATEMENT_DATE            8
#define RSIZ_PENALTY_CODE              2
#define RSIZ_EXTENDED_FLAG             1
#define RSIZ_BUS_CODE                  3
#define RSIZ_SQFT_4                    7
#define RSIZ_APPRAISER_NUMBER_1        2
#define RSIZ_APPRAISAL_TYPE_1          1
#define RSIZ_APPRAISER_NUMBER_2        2
#define RSIZ_APPRAISAL_TYPE_2          1
#define RSIZ_APPRAISER_NUMBER_3        2
#define RSIZ_APPRAISAL_TYPE_3          1
#define RSIZ_APPRAISER_NUMBER_4        2
#define RSIZ_APPRAISAL_TYPE_4          1
#define RSIZ_PARCEL_ADD_DATE           8
#define RSIZ_PARCEL_RETIRE_DATE        8
#define RSIZ_BD_EX_CD_1                1
#define RSIZ_BD_EX_CD_2                1
#define RSIZ_SUSPENSION_CODE           1
#define RSIZ_BASE_YEAR_VALUE_SEQUENCE  5
#define RSIZ_STATEMENT_FLAG            1
#define RSIZ_TV_SEQ                    5
#define RSIZ_SUPPLEMENTAL_ROLL_FLAG    1
#define RSIZ_BASE_YEAR_VALUE_POST_FLA  1
#define RSIZ_APPR_PRMT_ID1             7
#define RSIZ_APPR_PRMT_ID2             7
#define RSIZ_APPR_PRMT_ID3             7
#define RSIZ_APPR_PRMT_ID4             7
#define RSIZ_CORTAC_NUMBER             4
#define RSIZ_CSHTST                    9
#define RSIZ_REWORK_FLAG               1
#define RSIZ_PROP8_FLAG                1
#define RSIZ_AG_PRESERVE_NO            4
#define RSIZ_FILLER1                   6
#define RSIZ_MULTIAPN_FLAG             1
#define RSIZ_FILLER2                   2

#define ROFF_STATUS                    1-1
#define ROFF_NAME1                     2-2
#define ROFF_APN                       32-1
#define ROFF_NAME2                     42-1
#define ROFF_CAREOF                    72-1
#define ROFF_EXCEPTION                 102-1
#define ROFF_DBA                       114-1
#define ROFF_UNSEC_PP                  125-1
#define ROFF_ZONING                    134-1
#define ROFF_LIEN_NAME1                144-1
#define ROFF_LIEN_NAME2                174-1
#define ROFF_MAIL_ADDR                 204-1
#define ROFF_MAIL_CITY_STATE           234-1
#define ROFF_MAIL_ZIP                  259-1
#define ROFF_SITUS_STREET_NAME         268-1
#define ROFF_SITUS_STREET_NUMBER       298-1
#define ROFF_SITUS_DIR                 303-1
#define ROFF_SITUS_STR_SUB             305-1
#define ROFF_SITUS_UNIT                308-1
#define ROFF_CITY_CODE                 311-1
#define ROFF_THIRD_APN                 313-1
#define ROFF_TRA                       323-1
#define ROFF_LAND                      329-1
#define ROFF_STRUCTURE                 338-1
#define ROFF_TREES                     347-1
#define ROFF_TRADE_FIXT                356-1
#define ROFF_PERS_PROP                 365-1
#define ROFF_HOMEOWNER_EXEMP           374-1
#define ROFF_OTHER_EXEMP               383-1
#define ROFF_WILLIAMSON_LAND           392-1
#define ROFF_WILLIAMSON_TREES          401-1
#define ROFF_MSG_CODE_1                410-1
#define ROFF_MSG_CODE_2                411-1
#define ROFF_EXEMP_CODE                412-1
#define ROFF_VETERAN_CODE              413-1
#define ROFF_USE_CODE_1                414-1
#define ROFF_USE_CODE_2                418-1
#define ROFF_ACRES                     422-1
#define ROFF_REC_BOOK                  429-1
#define ROFF_REC_PAGE                  433-1
#define ROFF_DEED_NO                   436-1
#define ROFF_REC_DATE                  441-1
#define ROFF_SUBDIVISION_CODE          449-1
#define ROFF_SPLIT_APN_1               450-1
#define ROFF_SPLIT_APN_2               460-1
#define ROFF_SPLIT_APN_3               470-1
#define ROFF_SPLIT_APN_4               480-1
#define ROFF_APPRAISAL_DATE            490-1
#define ROFF_MAINT_DATE                498-1
#define ROFF_PRIOR_YEAR_CODE           506-1
#define ROFF_V_CHG_CODE                507-1
#define ROFF_BEN_ASS_CODE              509-1
#define ROFF_SQFT_1                    511-1
#define ROFF_SQFT_2                    518-1
#define ROFF_SQFT_3                    525-1
#define ROFF_ETAL_FLAG                 532-1
#define ROFF_PRIOR_LAND                533-1
#define ROFF_PRIOR_STRUC               542-1
#define ROFF_PRIOR_TREES               551-1
#define ROFF_PRIOR_TRADE_FIXT          560-1
#define ROFF_PRIOR_PERS_PROP           569-1
#define ROFF_PRIOR_HOMEOWNER_EXEMP     578-1
#define ROFF_PRIOR_OTHER_EXEMP         587-1
#define ROFF_PRIOR_MSG_CODE_1          596-1
#define ROFF_PRIOR_MSG_CODE_2          597-1
#define ROFF_APPR_OPEN_DT_1            598-1
#define ROFF_APPR_CLOSE_DT_1           606-1
#define ROFF_APPR_OPEN_DT_2            614-1
#define ROFF_APPR_CLOSE_DT_2           622-1
#define ROFF_APPR_OPEN_DT_3            630-1
#define ROFF_APPR_CLOSE_DT_3           638-1
#define ROFF_APPR_OPEN_DT_4            646-1
#define ROFF_APPR_CLOSE_DT_4           654-1
#define ROFF_ASSMT_STAT                662-1
#define ROFF_STATEMENT_TYPE            663-1
#define ROFF_STATEMENT_DATE            664-1
#define ROFF_PENALTY_CODE              672-1
#define ROFF_EXTENDED_FLAG             674-1
#define ROFF_BUS_CODE                  675-1
#define ROFF_SQFT_4                    678-1
#define ROFF_APPRAISER_NUMBER_1        685-1
#define ROFF_APPRAISAL_TYPE_1          687-1
#define ROFF_APPRAISER_NUMBER_2        688-1
#define ROFF_APPRAISAL_TYPE_2          690-1
#define ROFF_APPRAISER_NUMBER_3        691-1
#define ROFF_APPRAISAL_TYPE_3          693-1
#define ROFF_APPRAISER_NUMBER_4        694-1
#define ROFF_APPRAISAL_TYPE_4          696-1
#define ROFF_PARCEL_ADD_DATE           697-1
#define ROFF_PARCEL_RETIRE_DATE        705-1
#define ROFF_BD_EX_CD_1                713-1
#define ROFF_BD_EX_CD_2                714-1
#define ROFF_SUSPENSION_CODE           715-1
#define ROFF_BASE_YEAR_VALUE_SEQUENCE  716-1
#define ROFF_STATEMENT_FLAG            721-1
#define ROFF_TV_SEQ                    722-1
#define ROFF_SUPPLEMENTAL_ROLL_FLAG    727-1
#define ROFF_BASE_YEAR_VALUE_POST_FLA  728-1
#define ROFF_APPR_PRMT_ID1             729-1
#define ROFF_APPR_PRMT_ID2             736-1
#define ROFF_APPR_PRMT_ID3             743-1
#define ROFF_APPR_PRMT_ID4             750-1
#define ROFF_CORTAC_NUMBER             757-1
#define ROFF_CSHTST                    761-1
#define ROFF_REWORK_FLAG               770-1
#define ROFF_PROP8_FLAG                771-1 // Check for 'Y'
#define ROFF_AG_PRESERVE_NO            772-1
#define ROFF_FILLER1                   776-1
#define ROFF_MULTIAPN_FLAG             782-1
#define ROFF_FILLER2                   783-1

typedef struct _tMenSecRoll
{
   char Status;   // A=Active, P=Pending, R=Retired, U=Unassessed
   char Name1[RSIZ_NAME1];
   char Apn[RSIZ_APN];
   char Name2[RSIZ_NAME2];
   char Careof[RSIZ_CAREOF];
   char Exception[RSIZ_EXCEPTION];
   char Dba[RSIZ_DBA];
   char Unsec_PP[RSIZ_UNSEC_PP];
   char Zoning[RSIZ_ZONING];
   char MinSize[RSIZ_MINSIZE];
   char Lien_Name1[RSIZ_LIEN_NAME1];
   char Lien_Name2[RSIZ_LIEN_NAME2];      // Name2 or correction comment
   char Mail_Addr[RSIZ_MAIL_ADDR];
   char Mail_City_State[RSIZ_MAIL_CITY_STATE];
   char Mail_Zip[RSIZ_MAIL_ZIP];
   char Situs_Street_Name[RSIZ_SITUS_STREET_NAME];
   char Situs_Street_Number[RSIZ_SITUS_STREET_NUMBER];
   char Situs_Dir[RSIZ_SITUS_DIR];
   char Situs_StrSub[RSIZ_SITUS_STR_SUB];
   char Situs_Unit[RSIZ_SITUS_UNIT];
   char City_Code[RSIZ_CITY_CODE];
   char Third_Apn[RSIZ_THIRD_APN];
   char Tra[RSIZ_TRA];
   char Land[RSIZ_LAND];
   char Structure[RSIZ_STRUCTURE];
   char Trees[RSIZ_TREES];
   char Trade_Fixt[RSIZ_TRADE_FIXT];
   char Pers_Prop[RSIZ_PERS_PROP];
   char Homeowner_Exemp[RSIZ_HOMEOWNER_EXEMP];
   char Other_Exemp[RSIZ_OTHER_EXEMP];
   char Williamson_Land[RSIZ_WILLIAMSON_LAND];
   char Williamson_Trees[RSIZ_WILLIAMSON_TREES];
   char Msg_Code_1[RSIZ_MSG_CODE_1];
   char Msg_Code_2[RSIZ_MSG_CODE_2];
   char Exemp_Code[RSIZ_EXEMP_CODE];               // 412
   char Veteran_Code[RSIZ_VETERAN_CODE];
   char Use_Code_1[RSIZ_USE_CODE_1];
   char Use_Code_2[RSIZ_USE_CODE_2];
   char Acres[RSIZ_ACRES];
   char Rec_Book[RSIZ_REC_BOOK];
   char Rec_Page[RSIZ_REC_PAGE];
   char Deed_No[RSIZ_DEED_NO];
   char Rec_Date[RSIZ_REC_DATE];
   char Subdivision_Code[RSIZ_SUBDIVISION_CODE];
   char Split_Apn_1[RSIZ_SPLIT_APN_1];
   char Split_Apn_2[RSIZ_SPLIT_APN_2];
   char Split_Apn_3[RSIZ_SPLIT_APN_3];
   char Split_Apn_4[RSIZ_SPLIT_APN_4];
   char Appraisal_Date[RSIZ_APPRAISAL_DATE];
   char Maint_Date[RSIZ_MAINT_DATE];
   char Prior_Year_Code[RSIZ_PRIOR_YEAR_CODE];
   char V_Chg_Code[RSIZ_V_CHG_CODE];
   char Ben_Ass_Code[RSIZ_BEN_ASS_CODE];
   char Sqft_1[RSIZ_SQFT_1];
   char Sqft_2[RSIZ_SQFT_2];
   char Sqft_3[RSIZ_SQFT_3];
   char Etal_Flag[RSIZ_ETAL_FLAG];
   char Prior_Land[RSIZ_PRIOR_LAND];
   char Prior_Struc[RSIZ_PRIOR_STRUC];
   char Prior_Trees[RSIZ_PRIOR_TREES];
   char Prior_Trade_Fixt[RSIZ_PRIOR_TRADE_FIXT];
   char Prior_Pers_Prop[RSIZ_PRIOR_PERS_PROP];
   char Prior_Homeowner_Exemp[RSIZ_PRIOR_HOMEOWNER_EXEMP];
   char Prior_Other_Exemp[RSIZ_PRIOR_OTHER_EXEMP];
   char Prior_Msg_Code_1[RSIZ_PRIOR_MSG_CODE_1];
   char Prior_Msg_Code_2[RSIZ_PRIOR_MSG_CODE_2];
   char Appr_Open_Dt_1[RSIZ_APPR_OPEN_DT_1];
   char Appr_Close_Dt_1[RSIZ_APPR_CLOSE_DT_1];
   char Appr_Open_Dt_2[RSIZ_APPR_OPEN_DT_2];
   char Appr_Close_Dt_2[RSIZ_APPR_CLOSE_DT_2];
   char Appr_Open_Dt_3[RSIZ_APPR_OPEN_DT_3];
   char Appr_Close_Dt_3[RSIZ_APPR_CLOSE_DT_3];
   char Appr_Open_Dt_4[RSIZ_APPR_OPEN_DT_4];
   char Appr_Close_Dt_4[RSIZ_APPR_CLOSE_DT_4];
   char Assmt_Stat[RSIZ_ASSMT_STAT];      // P=Pending, S=Suspend, X=printed
   char Statement_Type[RSIZ_STATEMENT_TYPE];
   char Statement_Date[RSIZ_STATEMENT_DATE];
   char Penalty_Code[RSIZ_PENALTY_CODE];
   char Extended_Flag[RSIZ_EXTENDED_FLAG];
   char Bus_Code[RSIZ_BUS_CODE];
   char Sqft_4[RSIZ_SQFT_4];
   char Appraiser_Number_1[RSIZ_APPRAISER_NUMBER_1];
   char Appraisal_Type_1[RSIZ_APPRAISAL_TYPE_1];
   char Appraiser_Number_2[RSIZ_APPRAISER_NUMBER_2];
   char Appraisal_Type_2[RSIZ_APPRAISAL_TYPE_2];
   char Appraiser_Number_3[RSIZ_APPRAISER_NUMBER_3];
   char Appraisal_Type_3[RSIZ_APPRAISAL_TYPE_3];
   char Appraiser_Number_4[RSIZ_APPRAISER_NUMBER_4];
   char Appraisal_Type_4[RSIZ_APPRAISAL_TYPE_4];
   char Parcel_Add_Date[RSIZ_PARCEL_ADD_DATE];
   char Parcel_Retire_Date[RSIZ_PARCEL_RETIRE_DATE];
   char Bd_Ex_Cd_1[RSIZ_BD_EX_CD_1];
   char Bd_Ex_Cd_2[RSIZ_BD_EX_CD_2];
   char Suspension_Code[RSIZ_SUSPENSION_CODE];
   char Base_Year_Value_Sequence[RSIZ_BASE_YEAR_VALUE_SEQUENCE];
   char Statement_Flag[RSIZ_STATEMENT_FLAG];
   char Tv_Seq[RSIZ_TV_SEQ];
   char Supplemental_Roll_Flag[RSIZ_SUPPLEMENTAL_ROLL_FLAG];
   char Base_Year_Value_Post_Fla[RSIZ_BASE_YEAR_VALUE_POST_FLA];
   char Appr_Prmt_Id1[RSIZ_APPR_PRMT_ID1];
   char Appr_Prmt_Id2[RSIZ_APPR_PRMT_ID2];
   char Appr_Prmt_Id3[RSIZ_APPR_PRMT_ID3];
   char Appr_Prmt_Id4[RSIZ_APPR_PRMT_ID4];
   char Cortac_Number[RSIZ_CORTAC_NUMBER];
   char Cshtst[RSIZ_CSHTST];
   char Rework_Flag;
   char Prop8_Flag;
   char Ag_Preserve_No[RSIZ_AG_PRESERVE_NO];
   char Filler1[RSIZ_FILLER1];
   char MultiApn_Flag[RSIZ_MULTIAPN_FLAG];        // A=multi apn
   char Filler2[RSIZ_FILLER2];
   char CRLF[2];
} MEN_ROLL;

#define ARSIZ_STATUS                    1
#define ARSIZ_NAME1                     30
#define ARSIZ_APN                       10
#define ARSIZ_NAME2                     30
#define ARSIZ_CAREOF                    30
#define ARSIZ_EXCEPTION                 12
#define ARSIZ_DBA                       30
#define ARSIZ_LIEN_NAME1                30
#define ARSIZ_LIEN_NAME2                30
#define ARSIZ_MAIL_ADDR                 30
#define ARSIZ_MAIL_CITY_STATE           25
#define ARSIZ_MAIL_ZIP                  9
#define ARSIZ_SITUS_STREET_NAME         30
#define ARSIZ_SITUS_STREET_NUMBER       5
#define ARSIZ_SITUS_DIR                 2
#define ARSIZ_SITUS_STR_SUB             3
#define ARSIZ_SITUS_UNIT                3
//#define ARSIZ_UNIT                      2
//#define ARSIZ_BLOCK_LEGAL               3
//#define ARSIZ_LOT_LEGAL                 3
#define ARSIZ_CITY_CODE                 2
#define ARSIZ_THIRD_APN                 10
#define ARSIZ_TRA                       6
#define ARSIZ_LAND                      9
#define ARSIZ_STRUCTURE                 9
#define ARSIZ_TREES                     9
#define ARSIZ_TRADE_FIXT                9
#define ARSIZ_PERS_PROP                 9
#define ARSIZ_HOMEOWNER_EXEMP           9
#define ARSIZ_OTHER_EXEMP               9
#define ARSIZ_WILLIAMSON_LAND           9
#define ARSIZ_WILLIAMSON_TREES          9
#define ARSIZ_MSG_CODE_1                1
#define ARSIZ_MSG_CODE_2                1
#define ARSIZ_EXEMP_CODE                1
#define ARSIZ_VETERAN_CODE              1
#define ARSIZ_USE_CODE_1                4
#define ARSIZ_USE_CODE_2                4
#define ARSIZ_ACRES                     7
#define ARSIZ_REC_BOOK                  4
#define ARSIZ_REC_PAGE                  3
#define ARSIZ_DEED_NO                   5
#define ARSIZ_REC_DATE                  8
#define ARSIZ_SUBDIVISION_CODE          1
#define ARSIZ_SPLIT_APN_1               10
#define ARSIZ_SPLIT_APN_2               10
#define ARSIZ_SPLIT_APN_3               10
#define ARSIZ_SPLIT_APN_4               10
#define ARSIZ_APPRAISAL_DATE            8
#define ARSIZ_MAINT_DATE                8
#define ARSIZ_PRIOR_YEAR_CODE           1
#define ARSIZ_V_CHG_CODE                2
#define ARSIZ_BEN_ASS_CODE              2
#define ARSIZ_SQFT_1                    7
#define ARSIZ_SQFT_2                    7
#define ARSIZ_SQFT_3                    7
#define ARSIZ_ETAL_FLAG                 1
#define ARSIZ_PRIOR_LAND                9
#define ARSIZ_PRIOR_STRUC               9
#define ARSIZ_PRIOR_TREES               9
#define ARSIZ_PRIOR_TRADE_FIXT          9
#define ARSIZ_PRIOR_PERS_PROP           9
#define ARSIZ_PRIOR_HOMEOWNER_EXEMP     9
#define ARSIZ_PRIOR_OTHER_EXEMP         9
#define ARSIZ_PRIOR_MSG_CODE_1          1
#define ARSIZ_PRIOR_MSG_CODE_2          1
#define ARSIZ_APPR_OPEN_DT_1            8
#define ARSIZ_APPR_CLOSE_DT_1           8
#define ARSIZ_APPR_OPEN_DT_2            8
#define ARSIZ_APPR_CLOSE_DT_2           8
#define ARSIZ_APPR_OPEN_DT_3            8
#define ARSIZ_APPR_CLOSE_DT_3           8
#define ARSIZ_APPR_OPEN_DT_4            8
#define ARSIZ_APPR_CLOSE_DT_4           8
#define ARSIZ_ASSMT_STAT                1
#define ARSIZ_STATEMENT_TYPE            1
#define ARSIZ_STATEMENT_DATE            8
#define ARSIZ_PENALTY_CODE              2
#define ARSIZ_EXTENDED_FLAG             1
#define ARSIZ_BUS_CODE                  3
#define ARSIZ_SQFT_4                    7
#define ARSIZ_APPRAISER_NUMBER_1        2
#define ARSIZ_APPRAISAL_TYPE_1          1
#define ARSIZ_APPRAISER_NUMBER_2        2
#define ARSIZ_APPRAISAL_TYPE_2          1
#define ARSIZ_APPRAISER_NUMBER_3        2
#define ARSIZ_APPRAISAL_TYPE_3          1
#define ARSIZ_APPRAISER_NUMBER_4        2
#define ARSIZ_APPRAISAL_TYPE_4          1
#define ARSIZ_PARCEL_ADD_DATE           8
#define ARSIZ_PARCEL_RETIRE_DATE        8
#define ARSIZ_BD_EX_CD_1                1
#define ARSIZ_BD_EX_CD_2                1
#define ARSIZ_SUSPENSION_CODE           1
#define ARSIZ_BASE_YEAR_VALUE_SEQUENCE  5
#define ARSIZ_STATEMENT_FLAG            1
#define ARSIZ_TV_SEQ                    5
#define ARSIZ_SUPPLEMENTAL_ROLL_FLAG    1
#define ARSIZ_BASE_YEAR_VALUE_POST_FLA  1
#define ARSIZ_APPR_PRMT_ID1             7
#define ARSIZ_APPR_PRMT_ID2             7
#define ARSIZ_APPR_PRMT_ID3             7
#define ARSIZ_APPR_PRMT_ID4             7
#define ARSIZ_CORTAC_NUMBER             4
#define ARSIZ_CSHTST                    9
#define ARSIZ_REWORK_FLAG               1
#define ARSIZ_FMT                       1
#define ARSIZ_AG_PRESERVE_NO            4
#define ARSIZ_FLAGS                     10
#define ARSIZ_GROSS                     10
#define ARSIZ_TOTAL_EXE                 10
#define ARSIZ_NET                       10
#define ARSIZ_LAND_IMPR                 10
#define ARSIZ_LI_RATIO                  3
#define ARSIZ_LAND_IMPR_TREE            10
#define ARSIZ_LIT_RATIO                 3
#define ARSIZ_LOTACRES                  8
#define ARSIZ_LOTSQFT                   10
#define ARSIZ_LANDVAL_ACRE              12
#define ARSIZ_LANDVAL_SQFT              12

#define ARSIZ_SALE_RECBK                4
#define ARSIZ_SALE_RECPG                3
#define ARSIZ_SALE_DEEDNO               5
#define ARSIZ_SALE_RECDT                8
#define ARSIZ_SALE_NAME                 28
#define ARSIZ_SALE_VESTING              10
#define ARSIZ_SALE_PCT_OWN              6
#define ARSIZ_SALE_DTT_AMT              7
#define ARSIZ_SALE_DTT_CODE             1
#define ARSIZ_SALE_PRICE                10

#define AROFF_STATUS                    1
#define AROFF_NAME1                     2
#define AROFF_APN                       32
#define AROFF_NAME2                     42
#define AROFF_CAREOF                    72
#define AROFF_EXCEPTION                 102
#define AROFF_DBA                       114
#define AROFF_LIEN_NAME1                144
#define AROFF_LIEN_NAME2                174
#define AROFF_MAIL_ADDR                 204
#define AROFF_MAIL_CITY_STATE           234
#define AROFF_MAIL_ZIP                  259
#define AROFF_SITUS_STREET_NAME         268
#define AROFF_SITUS_STREET_NUMBER       298
#define AROFF_SITUS_DIR                 303
#define AROFF_SITUS_STR_SUB             305
#define AROFF_SITUS_UNIT                308
//#define AROFF_UNIT                      303
//#define AROFF_BLOCK_LEGAL               305
//#define AROFF_LOT_LEGAL                 308
#define AROFF_CITY_CODE                 311
#define AROFF_THIRD_APN                 313
#define AROFF_TRA                       323
#define AROFF_LAND                      329
#define AROFF_STRUCTURE                 338
#define AROFF_TREES                     347
#define AROFF_TRADE_FIXT                356
#define AROFF_PERS_PROP                 365
#define AROFF_HOMEOWNER_EXEMP           374
#define AROFF_OTHER_EXEMP               383
#define AROFF_WILLIAMSON_LAND           392
#define AROFF_WILLIAMSON_TREES          401
#define AROFF_MSG_CODE_1                410
#define AROFF_MSG_CODE_2                411
#define AROFF_EXEMP_CODE                412
#define AROFF_VETERAN_CODE              413
#define AROFF_USE_CODE_1                414
#define AROFF_USE_CODE_2                418
#define AROFF_ACRES                     422
#define AROFF_REC_BOOK                  429
#define AROFF_REC_PAGE                  433
#define AROFF_DEED_NO                   436
#define AROFF_REC_DATE                  441
#define AROFF_SUBDIVISION_CODE          449
#define AROFF_SPLIT_APN_1               450
#define AROFF_SPLIT_APN_2               460
#define AROFF_SPLIT_APN_3               470
#define AROFF_SPLIT_APN_4               480
#define AROFF_APPRAISAL_DATE            490
#define AROFF_MAINT_DATE                498
#define AROFF_PRIOR_YEAR_CODE           506
#define AROFF_V_CHG_CODE                507
#define AROFF_BEN_ASS_CODE              509
#define AROFF_SQFT_1                    511
#define AROFF_SQFT_2                    518
#define AROFF_SQFT_3                    525
#define AROFF_ETAL_FLAG                 532
#define AROFF_PRIOR_LAND                533
#define AROFF_PRIOR_STRUC               542
#define AROFF_PRIOR_TREES               551
#define AROFF_PRIOR_TRADE_FIXT          560
#define AROFF_PRIOR_PERS_PROP           569
#define AROFF_PRIOR_HOMEOWNER_EXEMP     578
#define AROFF_PRIOR_OTHER_EXEMP         587
#define AROFF_PRIOR_MSG_CODE_1          596
#define AROFF_PRIOR_MSG_CODE_2          597
#define AROFF_APPR_OPEN_DT_1            598
#define AROFF_APPR_CLOSE_DT_1           606
#define AROFF_APPR_OPEN_DT_2            614
#define AROFF_APPR_CLOSE_DT_2           622
#define AROFF_APPR_OPEN_DT_3            630
#define AROFF_APPR_CLOSE_DT_3           638
#define AROFF_APPR_OPEN_DT_4            646
#define AROFF_APPR_CLOSE_DT_4           654
#define AROFF_ASSMT_STAT                662
#define AROFF_STATEMENT_TYPE            663
#define AROFF_STATEMENT_DATE            664
#define AROFF_PENALTY_CODE              672
#define AROFF_EXTENDED_FLAG             674
#define AROFF_BUS_CODE                  675
#define AROFF_SQFT_4                    678
#define AROFF_APPRAISER_NUMBER_1        685
#define AROFF_APPRAISAL_TYPE_1          687
#define AROFF_APPRAISER_NUMBER_2        688
#define AROFF_APPRAISAL_TYPE_2          690
#define AROFF_APPRAISER_NUMBER_3        691
#define AROFF_APPRAISAL_TYPE_3          693
#define AROFF_APPRAISER_NUMBER_4        694
#define AROFF_APPRAISAL_TYPE_4          696
#define AROFF_PARCEL_ADD_DATE           697
#define AROFF_PARCEL_RETIRE_DATE        705
#define AROFF_BD_EX_CD_1                713
#define AROFF_BD_EX_CD_2                714
#define AROFF_SUSPENSION_CODE           715
#define AROFF_BASE_YEAR_VALUE_SEQUENCE  716
#define AROFF_STATEMENT_FLAG            721
#define AROFF_TV_SEQ                    722
#define AROFF_SUPPLEMENTAL_ROLL_FLAG    727
#define AROFF_BASE_YEAR_VALUE_POST_FLA  728
#define AROFF_APPR_PRMT_ID1             729
#define AROFF_APPR_PRMT_ID2             736
#define AROFF_APPR_PRMT_ID3             743
#define AROFF_APPR_PRMT_ID4             750
#define AROFF_CORTAC_NUMBER             757
#define AROFF_CSHTST                    761
#define AROFF_REWORK_FLAG               770
#define AROFF_FMT                       771
#define AROFF_AG_PRESERVE_NO            772
#define AROFF_FLAGS                     776
#define AROFF_GROSS                     786
#define AROFF_TOTAL_EXE                 796
#define AROFF_NET                       806
#define AROFF_LAND_IMPR                 816
#define AROFF_LI_RATIO                  826
#define AROFF_LAND_IMPR_TREE            829
#define AROFF_LIT_RATIO                 839
#define AROFF_LOTACRES                  842
#define AROFF_LOTSQFT                   850
#define AROFF_LANDVAL_ACRE              860
#define AROFF_LANDVAL_SQFT              872
#define AROFF_SALE1_RECBK               884
#define AROFF_SALE1_RECPG               888
#define AROFF_SALE1_DEEDNO              891
#define AROFF_SALE1_RECDT               896
#define AROFF_SALE1_NAME                904
#define AROFF_SALE1_VESTING             932
#define AROFF_SALE1_PCT_OWN             942
#define AROFF_SALE1_DTT_AMT             948
#define AROFF_SALE1_DTT_CODE            955
#define AROFF_SALE1_PRICE               956
#define AROFF_SALE2_RECBK               966
#define AROFF_SALE2_RECPG               970
#define AROFF_SALE2_DEEDNO              973
#define AROFF_SALE2_RECDT               978
#define AROFF_SALE2_NAME                986
#define AROFF_SALE2_VESTING             1014
#define AROFF_SALE2_PCT_OWN             1024
#define AROFF_SALE2_DTT_AMT             1030
#define AROFF_SALE2_DTT_CODE            1037
#define AROFF_SALE2_PRICE               1038
#define AROFF_SALE3_RECBK               1048
#define AROFF_SALE3_RECPG               1052
#define AROFF_SALE3_DEEDNO              1055
#define AROFF_SALE3_RECDT               1060
#define AROFF_SALE3_NAME                1068
#define AROFF_SALE3_VESTING             1096
#define AROFF_SALE3_PCT_OWN             1106
#define AROFF_SALE3_DTT_AMT             1112
#define AROFF_SALE3_DTT_CODE            1119
#define AROFF_SALE3_PRICE               1120
#define AROFF_FILLER                    1130
#define ARSIZ_FILLER                    23

typedef struct _tMenSecRoll_Assr
{  // 1152-byte
   char Status;
   char Name1[ARSIZ_NAME1];
   char Apn[ARSIZ_APN];
   char Name2[ARSIZ_NAME2];
   char Careof[ARSIZ_CAREOF];
   char Exception[ARSIZ_EXCEPTION];
   char Dba[RSIZ_DBA];
   char Unsec_PP[RSIZ_UNSEC_PP];
   char Zoning[RSIZ_ZONING];
   char Lien_Name1[ARSIZ_LIEN_NAME1];
   char Lien_Name2[ARSIZ_LIEN_NAME2];
   char Mail_Addr[ARSIZ_MAIL_ADDR];
   char Mail_City_State[ARSIZ_MAIL_CITY_STATE];
   char Mail_Zip[ARSIZ_MAIL_ZIP];
   char Situs_Street_Name[ARSIZ_SITUS_STREET_NAME];
   char Situs_Street_Number[ARSIZ_SITUS_STREET_NUMBER];
   char Situs_Dir[RSIZ_SITUS_DIR];
   char Situs_StrSub[RSIZ_SITUS_STR_SUB];
   char Situs_Unit[RSIZ_SITUS_UNIT];
   //char Unit[ARSIZ_UNIT];
   //char Block_Legal[ARSIZ_BLOCK_LEGAL];
   //char Lot_Legal[ARSIZ_LOT_LEGAL];
   char City_Code[ARSIZ_CITY_CODE];
   char Third_Apn[ARSIZ_THIRD_APN];
   char Tra[ARSIZ_TRA];
   char Land[ARSIZ_LAND];
   char Structure[ARSIZ_STRUCTURE];
   char Trees[ARSIZ_TREES];
   char Trade_Fixt[ARSIZ_TRADE_FIXT];
   char Pers_Prop[ARSIZ_PERS_PROP];
   char Homeowner_Exemp[ARSIZ_HOMEOWNER_EXEMP];
   char Other_Exemp[ARSIZ_OTHER_EXEMP];
   char Williamson_Land[ARSIZ_WILLIAMSON_LAND];
   char Williamson_Trees[ARSIZ_WILLIAMSON_TREES];
   char Msg_Code_1[ARSIZ_MSG_CODE_1];
   char Msg_Code_2[ARSIZ_MSG_CODE_2];
   char Exemp_Code[ARSIZ_EXEMP_CODE];
   char Veteran_Code[ARSIZ_VETERAN_CODE];
   char Use_Code_1[ARSIZ_USE_CODE_1];
   char Use_Code_2[ARSIZ_USE_CODE_2];
   char Acres[ARSIZ_ACRES];
   char Rec_Book[ARSIZ_REC_BOOK];
   char Rec_Page[ARSIZ_REC_PAGE];
   char Deed_No[ARSIZ_DEED_NO];
   char Rec_Date[ARSIZ_REC_DATE];
   char Subdivision_Code[ARSIZ_SUBDIVISION_CODE];
   char Split_Apn_1[ARSIZ_SPLIT_APN_1];
   char Split_Apn_2[ARSIZ_SPLIT_APN_2];
   char Split_Apn_3[ARSIZ_SPLIT_APN_3];
   char Split_Apn_4[ARSIZ_SPLIT_APN_4];
   char Appraisal_Date[ARSIZ_APPRAISAL_DATE];
   char Maint_Date[ARSIZ_MAINT_DATE];
   char Prior_Year_Code[ARSIZ_PRIOR_YEAR_CODE];
   char V_Chg_Code[ARSIZ_V_CHG_CODE];
   char Ben_Ass_Code[ARSIZ_BEN_ASS_CODE];
   char Sqft_1[ARSIZ_SQFT_1];
   char Sqft_2[ARSIZ_SQFT_2];
   char Sqft_3[ARSIZ_SQFT_3];
   char Etal_Flag[ARSIZ_ETAL_FLAG];
   char Prior_Land[ARSIZ_PRIOR_LAND];
   char Prior_Struc[ARSIZ_PRIOR_STRUC];
   char Prior_Trees[ARSIZ_PRIOR_TREES];
   char Prior_Trade_Fixt[ARSIZ_PRIOR_TRADE_FIXT];
   char Prior_Pers_Prop[ARSIZ_PRIOR_PERS_PROP];
   char Prior_Homeowner_Exemp[ARSIZ_PRIOR_HOMEOWNER_EXEMP];
   char Prior_Other_Exemp[ARSIZ_PRIOR_OTHER_EXEMP];
   char Prior_Msg_Code_1[ARSIZ_PRIOR_MSG_CODE_1];
   char Prior_Msg_Code_2[ARSIZ_PRIOR_MSG_CODE_2];
   char Appr_Open_Dt_1[ARSIZ_APPR_OPEN_DT_1];
   char Appr_Close_Dt_1[ARSIZ_APPR_CLOSE_DT_1];
   char Appr_Open_Dt_2[ARSIZ_APPR_OPEN_DT_2];
   char Appr_Close_Dt_2[ARSIZ_APPR_CLOSE_DT_2];
   char Appr_Open_Dt_3[ARSIZ_APPR_OPEN_DT_3];
   char Appr_Close_Dt_3[ARSIZ_APPR_CLOSE_DT_3];
   char Appr_Open_Dt_4[ARSIZ_APPR_OPEN_DT_4];
   char Appr_Close_Dt_4[ARSIZ_APPR_CLOSE_DT_4];
   char Assmt_Stat[ARSIZ_ASSMT_STAT];
   char Statement_Type[ARSIZ_STATEMENT_TYPE];
   char Statement_Date[ARSIZ_STATEMENT_DATE];
   char Penalty_Code[ARSIZ_PENALTY_CODE];
   char Extended_Flag[ARSIZ_EXTENDED_FLAG];
   char Bus_Code[ARSIZ_BUS_CODE];
   char Sqft_4[ARSIZ_SQFT_4];
   char Appraiser_Number_1[ARSIZ_APPRAISER_NUMBER_1];
   char Appraisal_Type_1[ARSIZ_APPRAISAL_TYPE_1];
   char Appraiser_Number_2[ARSIZ_APPRAISER_NUMBER_2];
   char Appraisal_Type_2[ARSIZ_APPRAISAL_TYPE_2];
   char Appraiser_Number_3[ARSIZ_APPRAISER_NUMBER_3];
   char Appraisal_Type_3[ARSIZ_APPRAISAL_TYPE_3];
   char Appraiser_Number_4[ARSIZ_APPRAISER_NUMBER_4];
   char Appraisal_Type_4[ARSIZ_APPRAISAL_TYPE_4];
   char Parcel_Add_Date[ARSIZ_PARCEL_ADD_DATE];
   char Parcel_Retire_Date[ARSIZ_PARCEL_RETIRE_DATE];
   char Bd_Ex_Cd_1[ARSIZ_BD_EX_CD_1];
   char Bd_Ex_Cd_2[ARSIZ_BD_EX_CD_2];
   char Suspension_Code[ARSIZ_SUSPENSION_CODE];
   char Base_Year_Value_Sequence[ARSIZ_BASE_YEAR_VALUE_SEQUENCE];
   char Statement_Flag[ARSIZ_STATEMENT_FLAG];
   char Tv_Seq[ARSIZ_TV_SEQ];
   char Supplemental_Roll_Flag[ARSIZ_SUPPLEMENTAL_ROLL_FLAG];
   char Base_Year_Value_Post_Fla[ARSIZ_BASE_YEAR_VALUE_POST_FLA];
   char Appr_Prmt_Id1[ARSIZ_APPR_PRMT_ID1];
   char Appr_Prmt_Id2[ARSIZ_APPR_PRMT_ID2];
   char Appr_Prmt_Id3[ARSIZ_APPR_PRMT_ID3];
   char Appr_Prmt_Id4[ARSIZ_APPR_PRMT_ID4];
   char Cortac_Number[ARSIZ_CORTAC_NUMBER];
   char Cshtst[ARSIZ_CSHTST];
   char Rework_Flag[ARSIZ_REWORK_FLAG];
   char Fmt[ARSIZ_FMT];
   char Ag_Preserve_No[ARSIZ_AG_PRESERVE_NO];
   char Flags[ARSIZ_FLAGS];
   char Gross[ARSIZ_GROSS];
   char TotalExe[ARSIZ_TOTAL_EXE];
   char Net[ARSIZ_NET];
   char LandImpr[ARSIZ_LAND_IMPR];           // Land+Structure
   char LI_Ratio[ARSIZ_LI_RATIO];
   char LandImprTree[ARSIZ_LAND_IMPR_TREE];  // Land+Structure+Tree
   char LIT_Ratio[ARSIZ_LIT_RATIO];
   char LotAcres[ARSIZ_LOTACRES];            // 9(5)V99
   char LotSqft[ARSIZ_LOTSQFT];
   char Landval_Acre[ARSIZ_LANDVAL_ACRE];
   char Landval_Sqft[ARSIZ_LANDVAL_SQFT];
   MEN_SALE Sales[3];
   char Filler[ARSIZ_FILLER];
} MEN_ASEC;

/* See LIENEXTR
typedef struct _tMenLienExtr1
{
   char  acApn[14];
   char  acExCode[4];      // Pos:410 - Msg1, Msg2, ExeCode, VetCode
   char  acLand[10];
   char  acImpr[10];
   char  acPP_Val[10];
   char  acME_Val[10];     // Fixture or manufacturing equipment
   char  acTV_Val[10];     // Trees/Vines
   char  acWil_TV[10];     // Williamson trees/vines
   char  acWil_Land[10];   // Williamson land
   char  acHOExe[10];
   char  acOtherExe[10];

   // Added for additional info
   char  acExAmt[10];      // Total Exe
   char  acGross[10];
   char  acOther[10];
   char  acRatio[3];
   char  acHO[1];
   char  LF[2];
} MEN_LIEN;
*/

USEXREF  Men_UseTbl[] =
{
   "00","120",      //VACANT - RESIDENTIAL
   "01","101",      //SINGLE FAMILY RESIDENCE
   "02","103",      //DUPLEX OR DOUBLE RESIDENTIAL
   "03","100",      //MULTIPLE RESIDENTIAL
   "04","106",      //RESIDENTIAL - COURTS
   "05","115",      //MOBILE HOME (0 TO 5 ACRES)
   "06","600",      //RECREATIONAL RESID (5-40 AC)
   "10","833",      //VACANT - COMMERCIAL PROPERTY
   "11","200",      //RETAIL STORE/USED CAR LOT
   "12","200",      //RETAIL STORE W/OFFICE OVER
   "13","200",      //OFFICES, NON-PROFESSIONAL
   "14","200",      //RESTAURANT OR BAR
   "15","200",      //COMMERCIAL - SERVICE SHOP
   "16","200",      //HOTEL / MOTEL
   "17","234",      //SERVICE STATION
   "18","200",      //COMMERCIAL RECREATIONAL PROP
   "19","200",      //COMMERCIAL WHOLESALE OUTLET
   "21","200",      //COMMERCIAL - TRAILER PARK
   "22","200",      //PROF. BLDGS (MD,DDS,LAW ETC)
   "23","246",      //NURSERY
   "24","312",      //BANK
   "25","240",      //COMMERCIAL - PARKING LOT
   "26","226",      //SHOPPING CENTER
   "27","419",      //COMMERCIAL - AIRPORT
   "28","200",      //NEWSPAPER OR RADIO STATION
   "29","200",      //SHIPYARD, WHARF OR DOCKS
   "30","829",      //VACANT - INDUSTRIAL PROPERTY
   "31","402",      //LIGHT MANUFACTURING
   "32","403",      //HEAVY MANUFACTURING
   "33","415",      //INDUSTRIAL - PACKING PLANT
   "34","420",      //INDUSTRIAL MINERAL EXTRACTN
   "35","404",      //INDUSTRIAL - WAREHOUSING
   "36","400",      //INDUSTRIAL - JUNK YARD
   "40","175",      //IRRIGATED RURAL PROPERTY
   "41","175",      //RURAL - IRRIGATED ORCHARD
   "42","175",      //RURAL - IRRIGATED VINEYARD
   "43","175",      //RURAL - IRRIGATED PASTURE
   "44","175",      //RURAL - IRRIGATED ROW CROP
   "45","175",      //RECREATIONAL RESID.40-160 AC
   "50","175",      //DRY RURAL PROPERTY
   "51","175",      //RURAL - DRY ORCHARD
   "52","175",      //RURAL - DRY VINEYARD
   "53","175",      //RURAL - DRY CLASSIFIED LAND
   "54","175",      //RURAL - DRY RANGE LAND
   "55","175",      //RECREATIONAL RESID.40-160 AC
   "60","536",      //TIMBER
   "61","536",      //TIMBER:ASSESSABLE OLD-GROWTH
   "62","536",      //TIMBER:ASSESSABLE YNG-GROWTH
   "63","536",      //TIMBER: EXEMPT OLD-GROWTH
   "64","536",      //TIMBER: EXEMPT YOUNG-GROWTH
   "65","536",      //TIMBER:EXEMPT YNG/OLD GROWTH
   "66","536",      //RECREATNL RES TAXABLE TIMBER
   "67","536",      //RECREATNL RESD EXEMPT TIMBER
   "70","826",      //VACANT - INSTITUTIONAL PROP.
   "71","726",      //CHURCH
   "72","807",      //SCHOOL
   "73","753",      //HOSPITAL
   "74","752",      //CONVALESCENT HOSPITAL
   "75","752",      //REST HOME
   "76","751",      //ORPHANAGE,BRDING SCH,ACADEMY
   "77","758",      //MORTUARY
   "78","758",      //CEMETERY,MAUSOLEUM,CREMATORY
   "79","600",      //RECREATIONAL NON-PROFIT ORG.
   "80","776",      //MISCELLANEOUS
   "81","780",      //WASTE LAND
   "82","776",      //RIGHT-OF-WAY
   "83","783",      //MINERAL RIGHTS
   "84","536",      //TIMBER RIGHTS
   "85","789",      //UTILITY
   "", ""
};

#endif