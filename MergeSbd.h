#ifndef _MERGESBD_H_
#define _MERGESBD_H_

// LIEN
#define  SBD_ROLL_PAR_BOOK          0
#define  SBD_ROLL_PAR_PAGE          1
#define  SBD_ROLL_PAR_LINE          2
#define  SBD_ROLL_PAR_TYPE          3
#define  SBD_ROLL_PAR_SEQ           4
#define  SBD_ROLL_ROLL_YEAR         5
#define  SBD_ROLL_ROLL_TYPE         6     // SEC=secured, TAX=tax exempt
#define  SBD_ROLL_PAR_STAT          7     // (A)ctive or (I)nactive
#define  SBD_ROLL_TAX_STAT          8     // 1=assd by county, 2=exempt, 3=assd by SBE, 4=common area, not assd
#define  SBD_ROLL_DIST              9
#define  SBD_ROLL_RESP_GRP          10    // D=real prop, P=personal prop, S=special prop
#define  SBD_ROLL_RESP_UNIT         11
#define  SBD_ROLL_BOAT_PLANE_ID     12
#define  SBD_ROLL_LAND_TYPE         13    // 01 � Industrial
                                          // 02 � Administrative/Professional
                                          // 03 � Commercial
                                          // 04 � Public Facilities
                                          // 05 � Single Family Residential
                                          // 06 � Multi Family Residential
                                          // 07 � Agricultural
                                          // 08 � Multiple Zonings
                                          // 09 - Restricted
#define  SBD_ROLL_SIZE_RANGE        14
#define  SBD_ROLL_USECODE           15
#define  SBD_ROLL_TRA               16
#define  SBD_ROLL_VAL_BASE_YR       17
#define  SBD_ROLL_OWNER_ROLL_YR     18
#define  SBD_ROLL_DIR_BILL          19
#define  SBD_ROLL_LAND_VALUE        20
#define  SBD_ROLL_IMPR_VALUE        21
#define  SBD_ROLL_IMPR_PEN          22
#define  SBD_ROLL_PER_PRO_VAL       23
#define  SBD_ROLL_PER_PRO_PEN       24
#define  SBD_ROLL_EX1_TYPE          25
#define  SBD_ROLL_EX1_AMT           26
#define  SBD_ROLL_EX2_TYPE          27
#define  SBD_ROLL_EX2_AMT           28
#define  SBD_ROLL_HOX_AMT           29
#define  SBD_ROLL_GRO_LEASE         30
#define  SBD_ROLL_SITUS_COMM        31
#define  SBD_ROLL_SITUS_NBR         32
#define  SBD_ROLL_SITUS_DIR         33
#define  SBD_ROLL_SITUS_NAME        34
#define  SBD_ROLL_SITUS_SUFF        35
#define  SBD_ROLL_SITUS_QUAL        36
#define  SBD_ROLL_SITUS_CITY        37
#define  SBD_ROLL_SITUS_ZIP1        38
#define  SBD_ROLL_SITUS_ZIP2        39
#define  SBD_ROLL_OWNER             40
#define  SBD_ROLL_ETAL_FLAG         41    // Y if there are 3 or more owners; blank otherwise
#define  SBD_ROLL_VESTING           42
#define  SBD_ROLL_CO_OWNER          43
#define  SBD_ROLL_CO_OWN_MAIL       44    // Y if co-owner is a joint mail owner; blank otherwise
#define  SBD_ROLL_CO_VESTING        45
#define  SBD_ROLL_CPUC_OWNER        46
#define  SBD_ROLL_CPUC_TYPE         47
#define  SBD_ROLL_CPUC_REL          48
#define  SBD_ROLL_DBA_OWNER         49
#define  SBD_ROLL_DBA_REL           50
#define  SBD_ROLL_CAREOF            51
#define  SBD_ROLL_MAIL_LINE1        52
#define  SBD_ROLL_MAIL_LINE2        53
#define  SBD_ROLL_MAIL_ZIP1         54
#define  SBD_ROLL_MAIL_ZIP2         55
#define  SBD_ROLL_LGL_TRACT         56
#define  SBD_ROLL_LGL_LOT           57
#define  SBD_ROLL_LGL_BLK           58
// 2009 - SBD no longer sending us LGL_DESC
// 2010 - SBD now with legal
#define  SBD_ROLL_LGL_DESC1         59
#define  SBD_ROLL_LGL_DESC2         60
#define  SBD_ROLL_LGL_DESC3         61
#define  SBD_ROLL_LGL_DESC4         62
#define  SBD_ROLL_LGL_DESC5         63
#define  SBD_ROLL_LGL_DESC6         64
#define  SBD_ROLL_LGL_DESC7         65
#define  SBD_ROLL_LGL_DESC8         66
#define  SBD_ROLL_LGL_DESC9         67
#define  SBD_ROLL_LGL_DESC10        68
#define  SBD_ROLL_LGL_DESC11        69
#define  SBD_ROLL_LGL_DESC12        70
// There are 35-part legal desc.  But we can only take about 10.
// due to record length limitation
#define  SBD_ROLL_LGL_DESC35        93
#define  SBD_ROLL_FLDS              94

// Update roll
#define  SBD_RU_REPORT_NBR          0
#define  SBD_RU_PARCEL              1
#define  SBD_RU_NAME                2
#define  SBD_RU_SITUS_NBR           3
#define  SBD_RU_SITUS_DIR           4
#define  SBD_RU_SITUS_NAME          5
#define  SBD_RU_SITUS_SUFF          6
#define  SBD_RU_SITUS_COMM          7
#define  SBD_RU_SITUS_ZIP           8
#define  SBD_RU_MAIL_LINE1          9
#define  SBD_RU_MAIL_LINE2          10
#define  SBD_RU_MAIL_ZIP            11
#define  SBD_RU_LEG1                12
#define  SBD_RU_LEG2                13
#define  SBD_RU_LEG3                14
#define  SBD_RU_LEG4                15
#define  SBD_RU_LAND_TYPE           16
#define  SBD_RU_SIZE_RANGE          17
#define  SBD_RU_USECODE             18
#define  SBD_RU_LAND_VALUE          19
#define  SBD_RU_IMPR_VALUE          20
#define  SBD_RU_PERSPROP_VALUE      21
#define  SBD_RU_EXEM_VALUE          22
#define  SBD_RU_HOX_INDICATOR       23
#define  SBD_RU_TRA                 24
#define  SBD_RU_AREA_ACRE           25
#define  SBD_RU_TAX_STATUS          26    // Value 1-5
#define  SBD_RU_SITUS_QUAL          27    // Unit# or sp#
#define  SBD_RU_DBA_NAME            28
#define  SBD_RU_BASE_YEAR           29

// Update roll
#define  ROFF_PAR_SEQ            1
#define  ROFF_PAR_BOOK           11
//#define  ROFF_PAR_PAGE           15
//#define  ROFF_PAR_LINE           18
//#define  ROFF_PAR_TYPE           20
//#define  ROFF_OWNER              24
//#define  ROFF_SITUS_NBR          60
//#define  ROFF_SITUS_DIR          67
//#define  ROFF_SITUS_NAME         68
//#define  ROFF_SITUS_SUFF         88
//#define  ROFF_SITUS_CITY         92
//#define  ROFF_SITUS_ZIP          95
//#define  ROFF_SITUS_ZIP4         100
//#define  ROFF_MAIL_LINE1         104
//#define  ROFF_MAIL_LINE2         152
//#define  ROFF_MAIL_ZIP           191
//#define  ROFF_MAIL_ZIP4          196
//#define  ROFF_LGL_DESC1          200
//#define  ROFF_LGL_DESC2          243
//#define  ROFF_LGL_DESC3          286
//#define  ROFF_LGL_DESC4          329
//#define  ROFF_LAND_TYPE          372
//#define  ROFF_SIZE_RANGE         374
//#define  ROFF_USECODE            376
//#define  ROFF_LAND               380
//#define  ROFF_IMPR               389
//#define  ROFF_PERS               398
//#define  ROFF_EXE_AMT            407
//#define  ROFF_HO_FLG             416   // 0 (no) or 1 (yes)
//#define  ROFF_TRA                417
//#define  ROFF_ACRES              424   // 9(5)V99
//#define  ROFF_TAX_STAT           431
//#define  ROFF_FILLER             432

#define  RSIZ_PAR_SEQ            10
#define  RSIZ_PAR_BOOK           4
#define  RSIZ_PAR_PAGE           3
#define  RSIZ_PAR_LINE           2
#define  RSIZ_PAR_TYPE           1
#define  RSIZ_PAR_SUFFIX         3
#define  RSIZ_OWNER              36
#define  RSIZ_SITUS_NBR          7
#define  RSIZ_SITUS_DIR          1
#define  RSIZ_SITUS_NAME         20
#define  RSIZ_SITUS_SUFF         4
#define  RSIZ_SITUS_CITY         3
#define  RSIZ_SITUS_ZIP          5
#define  RSIZ_SITUS_ZIP4         4
#define  RSIZ_MAIL_LINE1         48
#define  RSIZ_MAIL_LINE2         39
#define  RSIZ_MAIL_ZIP           5
#define  RSIZ_MAIL_ZIP4          4
#define  RSIZ_LGL_DESC1          43
#define  RSIZ_LGL_DESC2          43
#define  RSIZ_LGL_DESC3          43
#define  RSIZ_LGL_DESC4          43
#define  RSIZ_LAND_TYPE          2
#define  RSIZ_SIZE_RANGE         2
#define  RSIZ_USECODE            4
#define  RSIZ_LAND               9
#define  RSIZ_IMPR               9
#define  RSIZ_PERS               9
#define  RSIZ_EXE_AMT            9
#define  RSIZ_HO_FLG             1
#define  RSIZ_TRA                7
#define  RSIZ_ACRES              7
#define  RSIZ_TAX_STATUS         1
#define  RSIZ_FILLER             9

typedef struct _tSBD_UpdateRoll
{
   char ParSeq[RSIZ_PAR_SEQ];
   char ParBook[RSIZ_PAR_BOOK];
   char ParPage[RSIZ_PAR_PAGE];
   char ParLine[RSIZ_PAR_LINE];
   char ParType[RSIZ_PAR_TYPE];
   char ParSfx[RSIZ_PAR_SUFFIX];
   char Owner[RSIZ_OWNER];
   char S_StrNo[RSIZ_SITUS_NBR];
   char S_StrDir[RSIZ_SITUS_DIR];
   char S_StrName[RSIZ_SITUS_NAME];
   char S_StrSfx[RSIZ_SITUS_SUFF];
   char S_City[RSIZ_SITUS_CITY];
   char S_Zip[RSIZ_SITUS_ZIP];
   char S_Zip4[RSIZ_SITUS_ZIP4];
   char M_Adr1[RSIZ_MAIL_LINE1];
   char M_Adr2[RSIZ_MAIL_LINE2];
   char M_Zip[RSIZ_MAIL_ZIP];
   char M_Zip4[RSIZ_MAIL_ZIP4];
   char Legal1[RSIZ_LGL_DESC1];
   char Legal2[RSIZ_LGL_DESC2];
   char Legal3[RSIZ_LGL_DESC3];
   char Legal4[RSIZ_LGL_DESC4];
   char LandType[RSIZ_LAND_TYPE];
   char SizeRng[RSIZ_SIZE_RANGE];
   char UseCode[RSIZ_USECODE];
   char Land[RSIZ_LAND];
   char Impr[RSIZ_IMPR];
   char PersProp[RSIZ_PERS];
   char ExeAmt[RSIZ_EXE_AMT];
   char HO_Flg[RSIZ_HO_FLG];
   char TRA[RSIZ_TRA];
   char Acres[RSIZ_ACRES];
   char TaxStatus[RSIZ_TAX_STATUS];
   char Filler[RSIZ_FILLER];
} SBD_UPD;

STRSFX   Sbd_Sfx[]=
{
// Sfx,    OrgSfx,  SfxCode, CodeLen
   "ST",    "ST",    "1",  1,
   "AVE",   "AVE",   "3",  1,
   "DR",    "DR",    "5",  1,
   "RD",    "RD",    "2",  1,
   "CT",    "CT",    "14", 2,
   "LN",    "LN",    "21", 2,
   "PL",    "PL",    "27", 2,
   "WAY",   "WY",    "38", 2,
   "WAY",   "WAY",   "38", 2,
   "BLVD",  "BLVD",  "4",  1,
   "TRL",   "TRL",   "35", 2,
   "TRL",   "TR",    "35", 2,
   "CIR",   "CIR",   "10", 2,
   "HWY",   "HWY",   "20", 2,
   "PKWY",  "PKWY",  "28", 2,
   "TER",   "TER",   "33", 2,
   "RT",    "RTE",   "30", 2,
   "SQ",    "SQ",    "32", 2,
   "PT",    "PT",    "169",3,  
   "CYN",   "CYN",   "11", 2,
   "PASS",  "PASS",  "160",3,
   "LOOP",  "LP",    "22", 2,
   "CRES",  "CRES",  "12", 2,
   "BND",   "BND",   "71", 2,
   "BND",   "BEND",  "71", 2,
   "VW",    "VW",    "205",3,
   "CTR",   "CTR",   "40", 2,
   "PARK",  "PK",    "26", 2,
   "PARK",  "PARK",  "26", 2,
   "LNDG",  "LNDG",  "147",3,
   "GDNS",  "GDNS",  "116",3,
   "PLZ",   "PLZ",   "29", 2,
   "PATH",  "PTH",   "24", 2,
   "PATH",  "PATH",  "24", 2,
   "EXPY",  "EXPY",  "17", 2,
   "EXPY",  "EXWY",  "17", 2,
   "FWY",   "FRWY",  "28", 2,
   "FWY",   "FWY",   "28", 2,
   "LOOP",  "LOOP",  "22", 2,
   "ALY",   "AL",    "7",  1,
   "ALY",   "ALY",   "7",  1,
   "WALK",  "WALK",  "207",3,
   "","","",0
};

// New Char layout
#define	SBD_CHR_CREATE_DATE					0
#define	SBD_CHR_PARCEL_NBR					1
#define	SBD_CHR_DISTRICT						2
#define	SBD_CHR_RESP_GRP						3
#define	SBD_CHR_RESP_UNIT						4
#define	SBD_CHR_LAND_TYPE						5
#define	SBD_CHR_SIZE_RANGE					6
#define	SBD_CHR_USE_CODE						7
#define	SBD_CHR_TRA								8
#define	SBD_CHR_PROL_IMPR_VAL				9
#define	SBD_CHR_PROL_LAND_VAL				10
#define	SBD_CHR_EXEM1_TYPE					11
#define	SBD_CHR_EXEM2_TYPE					12
#define	SBD_CHR_EXEMHOX_TOT_AMT				13
#define	SBD_CHR_SITUS_STREET_NBR			14
#define	SBD_CHR_SITUS_STREET_FRA			15
#define	SBD_CHR_SITUS_STREET_DIR			16
#define	SBD_CHR_SITUS_STREET_NAME			17
#define	SBD_CHR_SITUS_STREET_SUFF			18
#define	SBD_CHR_SITUS_STREET_QUAL			19
#define	SBD_CHR_SITUS_COMM					20
#define	SBD_CHR_SITUS_CITY					21
#define	SBD_CHR_SITUS_ZIP1					22
#define	SBD_CHR_SITUS_ZIP2					23
#define	SBD_CHR_OWNR_RELATION				24
#define	SBD_CHR_OWNR_DOC_DATE				25
#define	SBD_CHR_OWNR_DOC_NBR					26
#define	SBD_CHR_OWNR_ACQ_DATE				27
#define	SBD_CHR_OWNR_VAL_YR					28
#define	SBD_CHR_OWNR_CHG_YR					29
#define	SBD_CHR_MAIL_1STOWNR					30
#define	SBD_CHR_MAIL_2NDOWNR					31
#define	SBD_CHR_MAIL_ADDR_LN1				32
#define	SBD_CHR_MAIL_ADDR_LN2				33
#define	SBD_CHR_LAND_LOT_WIDTH				34
#define	SBD_CHR_LAND_LOT_DEPTH				35
#define	SBD_CHR_LAND_LOT_FOOTAGE			36
#define	SBD_CHR_GROSS_ACRE					37
#define	SBD_CHR_LAND_NET_ACRE				38
#define	SBD_CHR_LAND_ACCESS					39
#define	SBD_CHR_LAND_SLOPE_DIR				40
#define	SBD_CHR_LAND_SLOPE_DEGRE			41
#define	SBD_CHR_LAND_VIEW_QUALTY			42
#define	SBD_CHR_LAND_VIEW_TYPE				43
#define	SBD_CHR_LAND_SEWER					44
#define	SBD_CHR_LAND_WATER					45
#define	SBD_CHR_LAND_ELECTRICITY			46
#define	SBD_CHR_LAND_GAS						47
#define	SBD_CHR_LAND_ENCROACHMNT			48
#define	SBD_CHR_LAND_NUISANCE1				49
#define	SBD_CHR_LAND_NUISANCE2				50
#define	SBD_CHR_LAND_INFLUENCE1				51
#define	SBD_CHR_LAND_INFLUENCE2				52
#define	SBD_CHR_SFRS_SEQ						53
#define	SBD_CHR_SFRS_CONST_TYPE				54
#define	SBD_CHR_SFRS_QUALITY					55
#define	SBD_CHR_SFRS_SHAPE					56
#define	SBD_CHR_SFRS_COST_TABLE				57
#define	SBD_CHR_SFRS_MH_MAKE					58
#define	SBD_CHR_SFRS_SIZE						59
#define	SBD_CHR_SFRS_CONST_YEAR				60
#define	SBD_CHR_SFRS_EFF_YEAR				61
#define	SBD_CHR_SFRS_NBR_BATH				62
#define	SBD_CHR_SFRS_NBR_BDRM				63
#define	SBD_CHR_SFRS_FAMILY_DEN				64
#define	SBD_CHR_SFRS_TOTAL_ROOM				65
#define	SBD_CHR_SFRS_ROOF_TYPE				66
#define	SBD_CHR_SFRS_LANDSC_QUAL			67
#define	SBD_CHR_SFRS_FUNC_OBS				68
#define	SBD_CHR_SFRS_TMS_SEASON				69
#define	SBD_CHR_SFRS_COST_MOD				70
#define	SBD_CHR_SFRS_FUN_OBS_PCT			71
#define	SBD_CHR_SFRS_ECO_OBS_PCT			72
#define	SBD_CHR_SFRS_DEFR_MAINT				73
#define	SBD_CHR_SFRS_FLR1_SIZE				74
#define	SBD_CHR_SFRS_FLR2_SIZE				75
#define	SBD_CHR_SFRS_FLR2_FACTOR			76
#define	SBD_CHR_SFRS_FLR3_SIZE				77
#define	SBD_CHR_SFRS_FLR3_FACTOR			78
#define	SBD_CHR_SFRS_BAS1_SIZE				79
#define	SBD_CHR_SFRS_BAS1_FACTOR			80
#define	SBD_CHR_SFRS_BAS2_SIZE				81
#define	SBD_CHR_SFRS_BAS2_FACTOR			82
#define	SBD_CHR_SFRS_ADD1_SIZE				83
#define	SBD_CHR_SFRS_ADD1_FACTOR			84
#define	SBD_CHR_SFRS_ADD2_SIZE				85
#define	SBD_CHR_SFRS_ADD2_FACTOR			86
#define	SBD_CHR_SFRS_ADD3_SIZE				87
#define	SBD_CHR_SFRS_ADD3_FACTOR			88
#define	SBD_CHR_SFRS_PCH1_SIZE				89
#define	SBD_CHR_SFRS_PCH1_FACTOR			90
#define	SBD_CHR_SFRS_PCH2_SIZE				91
#define	SBD_CHR_SFRS_PCH2_FACTOR			92
#define	SBD_CHR_SFRS_PCH3_SIZE				93
#define	SBD_CHR_SFRS_PCH3_FACTOR			94
#define	SBD_CHR_SFRS_DECK_SQF				95
#define	SBD_CHR_SFRS_DECK_FACTOR			96
#define	SBD_CHR_SFRS_DECK_STALL				97
#define	SBD_CHR_SFRS_PORT_SQF				98
#define	SBD_CHR_SFRS_PORT_FACTOR			99
#define	SBD_CHR_SFRS_PORT_STALL				100
#define	SBD_CHR_SFRS_GARG_SQF				101
#define	SBD_CHR_SFRS_GARG_CLASS				102
#define	SBD_CHR_SFRS_GARG_STALL				103
#define	SBD_CHR_SFRS_AC_RCN					104
#define	SBD_CHR_SFRS_HEAT						105
#define	SBD_CHR_SFRS_COOL						106
#define	SBD_CHR_SFRS_FIRPLC_RCN				107
#define	SBD_CHR_SFRS_NBR_FIRPLC				108
#define	SBD_CHR_SFRS_YRD_IMP_RCN			109
#define	SBD_CHR_SFRS_MISC_RCN				110
#define	SBD_CHR_SFRS_POOL_RCN				111
#define	SBD_CHR_SFRS_POOL_TYPE				112
#define	SBD_CHR_SFRS_SPC_IMP_RCN			113
#define	SBD_CHR_SFRS_SPC_IMP1				114
#define	SBD_CHR_SFRS_SPC_IMP2				115
#define	SBD_CHR_SFRS_SPC_IMP3				116
#define	SBD_CHR_SFRS_MH_SIDING				117
#define	SBD_CHR_SFRS_MH_FND_LF				118
#define	SBD_CHR_SFRS_MH_FND_TYPE			119
#define	SBD_CHR_SFRS_MH_SKT_LF				120
#define	SBD_CHR_SFRS_MH_SKT_TYPE			121
#define	SBD_CHR_SFRS_STREET_NBR				122
#define	SBD_CHR_SFRS_STREET_FRA				123
#define	SBD_CHR_SFRS_STREET_DIR				124
#define	SBD_CHR_SFRS_STREET_NAME			125
#define	SBD_CHR_SFRS_STREET_SUFF			126
#define	SBD_CHR_SFRS_STREET_QUAL			127
#define	SBD_CHR_SFRS_COMM						128
#define	SBD_CHR_SFRS_CITY						129
#define	SBD_CHR_SFRS_ZIP1						130
#define	SBD_CHR_SFRS_ZIP2						131
#define	SBD_CHR_OTHR_SEQ						132
#define  SBD_CHR_OTHR_USE                 133
#define  SBD_CHR_OTHR_STRUC_QUAL          134
#define  SBD_CHR_OTHR_STRUC_TYPE          135
#define  SBD_CHR_OTHR_GRO_LEASE           136
#define  SBD_CHR_OTHR_NET_LEASE           137
#define  SBD_CHR_OTHR_CONST_YEAR          138
#define  SBD_CHR_OTHR_EFF_YEAR            139
#define  SBD_CHR_OTHR_NBR_UNITS           140
#define  SBD_CHR_OTHR_AVG_SF_UNITS        141
#define  SBD_CHR_OTHR_ADJ_AVG_SQF         142
#define  SBD_CHR_OTHR_NBR_STORIES         143
#define  SBD_CHR_OTHR_STORY_HIGHT         144
#define  SBD_CHR_OTHR_ELEVATOR            145
#define  SBD_CHR_OTHR_LANDSC_QUAL         146
#define  SBD_CHR_OTHR_FUNC_OBSOL          147
#define  SBD_CHR_OTHR_LB_RATIO            148
#define  SBD_CHR_OTHR_ADJ_LB_RATIO        149
#define  SBD_CHR_OTHR_APT1_UNITS          150
#define  SBD_CHR_OTHR_APT1_BDRM           151
#define  SBD_CHR_OTHR_APT1_BATH           152
#define  SBD_CHR_OTHR_APT2_UNITS          153
#define  SBD_CHR_OTHR_APT2_BDRM           154
#define  SBD_CHR_OTHR_APT2_BATH           155
#define  SBD_CHR_OTHR_APT3_UNITS          156
#define  SBD_CHR_OTHR_APT3_BDRM           157
#define  SBD_CHR_OTHR_APT3_BATH           158
#define  SBD_CHR_OTHR_APT4_UNITS          159
#define  SBD_CHR_OTHR_APT4_BDRM           160
#define  SBD_CHR_OTHR_APT4_BATH           161
#define  SBD_CHR_OTHR_APT5_UNITS          162
#define  SBD_CHR_OTHR_APT5_BDRM           163
#define  SBD_CHR_OTHR_APT5_BATH           164
#define  SBD_CHR_OTHR_GARAGE              165
#define  SBD_CHR_OTHR_CARPORT             166
#define  SBD_CHR_OTHR_OPEN_SPACES         167
#define  SBD_CHR_OTHR_RV_SPACES           168
#define  SBD_CHR_OTHR_LAUNDRY             169
#define  SBD_CHR_OTHR_REC_BLDG            170
#define  SBD_CHR_OTHR_APT_POOL            171
#define  SBD_CHR_OTHR_APT_SPA             172
#define  SBD_CHR_OTHR_TENNIS              173
#define  SBD_CHR_OTHR_SEC_GATE            174
#define  SBD_CHR_OTHR_OFFICE_PCNT         175
#define  SBD_CHR_OTHR_MEZZ_SQF            176
#define  SBD_CHR_OTHR_RESTAURANT          177
#define  SBD_CHR_OTHR_HOTEL_POOL          178
#define  SBD_CHR_OTHR_HOTEL_SPA           179
#define  SBD_CHR_OTHR_CONF_ROOM           180
#define  SBD_CHR_OTHR_STREET_NBR          181
#define  SBD_CHR_OTHR_STREET_FRA          182
#define  SBD_CHR_OTHR_STREET_DIR          183
#define  SBD_CHR_OTHR_STREET_NAME         184
#define  SBD_CHR_OTHR_STREET_SUFF         185
#define  SBD_CHR_OTHR_STREET_QUAL         186
#define  SBD_CHR_OTHR_COMM                187
#define  SBD_CHR_OTHR_CITY                188
#define  SBD_CHR_OTHR_ZIP1                189
#define  SBD_CHR_OTHR_ZIP2                190
#define  SBD_CHR_NEIGHBORHOOD             191

// Sale SFR - fixed length
#define	OFF_SFR_SALE_PARCEL_NUMBER 		0
#define	OFF_SFR_SALE_SORT_PARCEL_TYPE    13
#define	OFF_SFR_SALE_SORT_USE_CODE			14
#define	OFF_SFR_SALE_SORT_COST_TABLE  	15
#define	OFF_SFR_SALE_SITUS_STREET_NUMBER	16
#define	OFF_SFR_SALE_SITUS_STREET_NAME	23
#define	OFF_SFR_SALE_DISTRICT				43
#define	OFF_SFR_SALE_RESPONSIBLE_GROUP	45
#define	OFF_SFR_SALE_RESPONSIBLE_UNIT		46
#define	OFF_SFR_SALE_LAND_TYPE				49
#define	OFF_SFR_SALE_SIZE_RANGE				51
#define	OFF_SFR_SALE_USE_CODE				53
#define	OFF_SFR_SALE_USE_CODE_LITERAL		57
#define	OFF_SFR_SALE_LAND_TYPE_LITERAL	67
#define	OFF_SFR_SALE_NBR_OF_FLOORS			77
#define	OFF_SFR_SALE_CONSTRUCTION_TYPE	78
#define	OFF_SFR_SALE_QUALITY					79
#define	OFF_SFR_SALE_SHAPE					82
#define	OFF_SFR_SALE_COST_TABLE_LITERAL	83
#define	OFF_SFR_SALE_MH_MAKE_LITRAL		88
#define	OFF_SFR_SALE_AREA_OF_INFLUENCE	96
#define	OFF_SFR_SALE_OTHER_AREA				104
#define	OFF_SFR_SALE_CONSTRUCTION_YEAR	112
#define	OFF_SFR_SALE_EFFECTIVE_YEAR		116
#define	OFF_SFR_SALE_NUMBER_OF_BATHS		120      // 6.2
#define	OFF_SFR_SALE_NUMBER_OF_BEDS		126
#define	OFF_SFR_SALE_FAMILY_ROOM_DEN		128
#define	OFF_SFR_SALE_TOTAL_ROOMS			129
#define	OFF_SFR_SALE_HEATING_SYSTEM		132
#define	OFF_SFR_SALE_COOLING_SYSTEM		133
#define	OFF_SFR_SALE_NUM_OF_FIREPLACES	134
#define	OFF_SFR_SALE_ROOF_TYPE_LITERAL	139
#define	OFF_SFR_SALE_FUNC_OBS_LITERAL		145
#define	OFF_SFR_SALE_TIMESHARE_LITERAL	151
#define	OFF_SFR_SALE_PORCH_SQ_FT			157
#define	OFF_SFR_SALE_GARAGE_SQF				162
#define	OFF_SFR_SALE_GARAGE_CLASS			167
#define	OFF_SFR_SALE_GARAGE_STALLS			170
#define	OFF_SFR_SALE_CARPORT_SQ_FT			172
#define	OFF_SFR_SALE_CARPORT_FACTOR		177
#define	OFF_SFR_SALE_CARPORT_STALLS		180
#define	OFF_SFR_SALE_PARKING_DECK_SQF		182
#define	OFF_SFR_SALE_PARKING_DECK_FACTOR	187
#define	OFF_SFR_SALE_PARKING_DECK_STALLS	190
#define	OFF_SFR_SALE_MISC_RCN				192
#define	OFF_SFR_SALE_POOL_TYPE_LITERAL	198
#define	OFF_SFR_SALE_SPCL_IMPR_RCN			203
#define	OFF_SFR_SALE_SPCL_IMPR1_LITERAL	209
#define	OFF_SFR_SALE_SPCL_IMPR2_LITERAL	214
#define	OFF_SFR_SALE_SPCL_IMPR3_LITERAL	219
#define	OFF_SFR_SALE_SPCL_IMPR4_LITERAL	224
#define	OFF_SFR_SALE_MH_FOUNDATION_LIT	229
#define	OFF_SFR_SALE_ZONING					234
#define	OFF_SFR_SALE_LOT_WIDTH     		242      // 7.2
#define	OFF_SFR_SALE_LOT_DEPTH     		249      // 7.2
#define	OFF_SFR_SALE_LOT_SIZE      		256      // 11.3
#define	OFF_SFR_SALE_LOT_SIZE_UNITS		267      
#define	OFF_SFR_SALE_GROSS_ACRES    		269      // 8.3
#define	OFF_SFR_SALE_NET_ACRES      		277      // 8.3
#define	OFF_SFR_SALE_ACCESS_LITERAL    	285
#define	OFF_SFR_SALE_SLOPE_DIR_LITERAL	291
#define	OFF_SFR_SALE_VIEW_LITERAL      	297
#define	OFF_SFR_SALE_SEWER_LITERAL     	306
#define	OFF_SFR_SALE_WATER_LITERAL     	312
#define	OFF_SFR_SALE_ELECTRIC_LITERAL  	318
#define	OFF_SFR_SALE_GAS_LITERAL       	324
#define	OFF_SFR_SALE_OFFSITES_LITERAL  	330
#define	OFF_SFR_SALE_ENCROACH_LITERAL  	336
#define	OFF_SFR_SALE_NUISANCE1_LITERAL	342
#define	OFF_SFR_SALE_NUISANCE2_LITERAL	348
#define	OFF_SFR_SALE_INFLUENCE1_LITERAL	354
#define	OFF_SFR_SALE_INFLUENCE2_LITERAL	360
#define	OFF_SFR_SALE_DOCK_RIGHTS_LITERAL	366
#define	OFF_SFR_SALE_LAND_LSE_EXPR_DATE	372
#define	OFF_SFR_SALE_NUMBER_OF_LOTS		382
#define	OFF_SFR_SALE_MAP_STATUS_LITERAL	386
#define	OFF_SFR_SALE_DEV_STATUS_LITERAL	396
#define	OFF_SFR_SALE_SALE_DATE				406
#define	OFF_SFR_SALE_SALE_TYPE				416      // A,P,T,U
#define	OFF_SFR_SALE_SALE_PRICE				417
#define	OFF_SFR_SALE_BAD_SALE_LITERAL		428      // REO, FCL, ERR, NAL
#define	OFF_SFR_SALE_PRICE_PER_SF			434      // 9.2
#define	OFF_SFR_SALE_PRICE_PER_SF_AST  	443
#define	OFF_SFR_SALE_PRICE_PER_UNIT		444
#define	OFF_SFR_SALE_PRICE_PER_UNIT_AST	455
#define	OFF_SFR_SALE_CONDITION_LITERAL	456
#define	OFF_SFR_SALE_SALE_USE_CODE			462
#define	OFF_SFR_SALE_UNUSED					466
#define	OFF_SFR_SALE_REMARK_1_LITERAL		471
#define	OFF_SFR_SALE_REMARK_2_LITERAL		479
#define	OFF_SFR_SALE_REMARK_3_LITERAL		487
#define	OFF_SFR_SALE_REMARK_4_LITERAL		495
#define	OFF_SFR_SALE_REMARK_5_LITERAL		503
#define	OFF_SFR_SALE_PRIOR_SALE_DATE		511
#define	OFF_SFR_SALE_PRIOR_SALE_PRICE		521
#define	OFF_SFR_SALE_PCT_CHG_PER_MONTH	532
#define	OFF_SFR_SALE_APPR_CMPL_TYPE		541
#define	OFF_SFR_SALE_APPR_CMPL_LITERAL	544
#define	OFF_SFR_SALE_SITUS_ZIP_CODE		554
#define	OFF_SFR_SALE_SITUS_ZIP_PLUS4		559
#define	OFF_SFR_SALE_REQUEST_DATE			563
#define	OFF_SFR_SALE_BAD_SALE_CODE			573
#define	OFF_SFR_SALE_IMPR_IND				574
#define	OFF_SFR_SALE_EVENT_GROUP			575
#define	OFF_SFR_SALE_EXCL_IND				578
#define	OFF_SFR_SALE_ATTACHED_PRCLS_IND	579
#define	OFF_SFR_SALE_SYSTEM_DATA			580
#define	OFF_SFR_SALE_DOCNUM			      606
#define	OFF_SFR_SALE_DOCTYPE		         619
#define	OFF_SFR_SALE_SELLER		         623
#define	OFF_SFR_SALE_PRIOR_DOCNUM			697
#define	OFF_SFR_SALE_PRIOR_DOCTYPE		   710
#define	OFF_SFR_SALE_PRIOR_SELLER		   714
#define	OFF_SFR_SALE_LENGTH		         788

#define	SIZ_SFR_SALE_PARCEL_NUMBER 		13
#define	SIZ_SFR_SALE_SORT_PARCEL_TYPE    1
#define	SIZ_SFR_SALE_SORT_USE_CODE			1
#define	SIZ_SFR_SALE_SORT_COST_TABLE  	1
#define	SIZ_SFR_SALE_SITUS_STREET_NUMBER	7
#define	SIZ_SFR_SALE_SITUS_STREET_NAME	20
#define	SIZ_SFR_SALE_DISTRICT				2
#define	SIZ_SFR_SALE_RESPONSIBLE_GROUP	1
#define	SIZ_SFR_SALE_RESPONSIBLE_UNIT		3
#define	SIZ_SFR_SALE_LAND_TYPE				2
#define	SIZ_SFR_SALE_SIZE_RANGE				2
#define	SIZ_SFR_SALE_USE_CODE				4
#define	SIZ_SFR_SALE_USE_CODE_LITERAL		10
#define	SIZ_SFR_SALE_LAND_TYPE_LITERAL	10
#define	SIZ_SFR_SALE_NBR_OF_FLOORS			1
#define	SIZ_SFR_SALE_CONSTRUCTION_TYPE	1
#define	SIZ_SFR_SALE_QUALITY					3
#define	SIZ_SFR_SALE_SHAPE					1
#define	SIZ_SFR_SALE_COST_TABLE_LITERAL	5
#define	SIZ_SFR_SALE_MH_MAKE_LITRAL		8
#define	SIZ_SFR_SALE_AREA_OF_INFLUENCE	8
#define	SIZ_SFR_SALE_OTHER_AREA				8
#define	SIZ_SFR_SALE_CONSTRUCTION_YEAR	4
#define	SIZ_SFR_SALE_EFFECTIVE_YEAR		4
#define	SIZ_SFR_SALE_NUMBER_OF_BATHS		6
#define	SIZ_SFR_SALE_NUMBER_OF_BEDS		2
#define	SIZ_SFR_SALE_FAMILY_ROOM_DEN		1
#define	SIZ_SFR_SALE_TOTAL_ROOMS			3
#define	SIZ_SFR_SALE_HEATING_SYSTEM		1
#define	SIZ_SFR_SALE_COOLING_SYSTEM		1
#define	SIZ_SFR_SALE_NUM_OF_FIREPLACES	5
#define	SIZ_SFR_SALE_ROOF_TYPE_LITERAL	6
#define	SIZ_SFR_SALE_FUNC_OBS_LITERAL		6
#define	SIZ_SFR_SALE_TIMESHARE_LITERAL	6
#define	SIZ_SFR_SALE_PORCH_SQ_FT			5
#define	SIZ_SFR_SALE_GARAGE_SQF				5
#define	SIZ_SFR_SALE_GARAGE_CLASS			3
#define	SIZ_SFR_SALE_GARAGE_STALLS			2
#define	SIZ_SFR_SALE_CARPORT_SQ_FT			5
#define	SIZ_SFR_SALE_CARPORT_FACTOR		3
#define	SIZ_SFR_SALE_CARPORT_STALLS		2
#define	SIZ_SFR_SALE_PARKING_DECK_SQF		5
#define	SIZ_SFR_SALE_PARKING_DECK_FACTOR	3
#define	SIZ_SFR_SALE_PARKING_DECK_STALLS	2
#define	SIZ_SFR_SALE_MISC_RCN				6
#define	SIZ_SFR_SALE_POOL_TYPE_LITERAL	5
#define	SIZ_SFR_SALE_SPCL_IMPR_RCN			6
#define	SIZ_SFR_SALE_SPCL_IMPR1_LITERAL	5
#define	SIZ_SFR_SALE_SPCL_IMPR2_LITERAL	5
#define	SIZ_SFR_SALE_SPCL_IMPR3_LITERAL	5
#define	SIZ_SFR_SALE_SPCL_IMPR4_LITERAL	5
#define	SIZ_SFR_SALE_MH_FOUNDATION_LIT	5
#define	SIZ_SFR_SALE_ZONING					8
#define	SIZ_SFR_SALE_LOT_WIDTH     		7
#define	SIZ_SFR_SALE_LOT_DEPTH     		7
#define	SIZ_SFR_SALE_LOT_SIZE      		11
#define	SIZ_SFR_SALE_LOT_SIZE_UNITS		2
#define	SIZ_SFR_SALE_GROSS_ACRES    		8
#define	SIZ_SFR_SALE_NET_ACRES      		8
#define	SIZ_SFR_SALE_ACCESS_LITERAL    	6
#define	SIZ_SFR_SALE_SLOPE_DIR_LITERAL	6
#define	SIZ_SFR_SALE_VIEW_LITERAL      	9
#define	SIZ_SFR_SALE_SEWER_LITERAL     	6
#define	SIZ_SFR_SALE_WATER_LITERAL     	6
#define	SIZ_SFR_SALE_ELECTRIC_LITERAL  	6
#define	SIZ_SFR_SALE_GAS_LITERAL       	6
#define	SIZ_SFR_SALE_OFFSITES_LITERAL  	6
#define	SIZ_SFR_SALE_ENCROACH_LITERAL  	6
#define	SIZ_SFR_SALE_NUISANCE1_LITERAL	6
#define	SIZ_SFR_SALE_NUISANCE2_LITERAL	6
#define	SIZ_SFR_SALE_INFLUENCE1_LITERAL	6
#define	SIZ_SFR_SALE_INFLUENCE2_LITERAL	6
#define	SIZ_SFR_SALE_DOCK_RIGHTS_LITERAL	6
#define	SIZ_SFR_SALE_LAND_LSE_EXPR_DATE	10
#define	SIZ_SFR_SALE_NUMBER_OF_LOTS		4
#define	SIZ_SFR_SALE_MAP_STATUS_LITERAL	10
#define	SIZ_SFR_SALE_DEV_STATUS_LITERAL	10
#define	SIZ_SFR_SALE_SALE_DATE				10
#define	SIZ_SFR_SALE_SALE_TYPE				1
#define	SIZ_SFR_SALE_SALE_PRICE				11
#define	SIZ_SFR_SALE_BAD_SALE_LITERAL		6
#define	SIZ_SFR_SALE_PRICE_PER_SF			9
#define	SIZ_SFR_SALE_PRICE_PER_SF_AST  	1
#define	SIZ_SFR_SALE_PRICE_PER_UNIT		11
#define	SIZ_SFR_SALE_PRICE_PER_UNIT_AST	1
#define	SIZ_SFR_SALE_CONDITION_LITERAL	6
#define	SIZ_SFR_SALE_SALE_USE_CODE			4
#define	SIZ_SFR_SALE_UNUSED					5
#define	SIZ_SFR_SALE_REMARK_1_LITERAL		8
#define	SIZ_SFR_SALE_REMARK_2_LITERAL		8
#define	SIZ_SFR_SALE_REMARK_3_LITERAL		8
#define	SIZ_SFR_SALE_REMARK_4_LITERAL		8
#define	SIZ_SFR_SALE_REMARK_5_LITERAL		8
#define	SIZ_SFR_SALE_PRIOR_SALE_DATE		10
#define	SIZ_SFR_SALE_PRIOR_SALE_PRICE		11
#define	SIZ_SFR_SALE_PCT_CHG_PER_MONTH	9
#define	SIZ_SFR_SALE_APPR_CMPL_TYPE		3
#define	SIZ_SFR_SALE_APPR_CMPL_LITERAL	10
#define	SIZ_SFR_SALE_SITUS_ZIP_CODE		5
#define	SIZ_SFR_SALE_SITUS_ZIP_PLUS4		4
#define	SIZ_SFR_SALE_REQUEST_DATE			10
#define	SIZ_SFR_SALE_BAD_SALE_CODE			1
#define	SIZ_SFR_SALE_IMPR_IND				1
#define	SIZ_SFR_SALE_EVENT_GROUP			3
#define	SIZ_SFR_SALE_EXCL_IND				1
#define	SIZ_SFR_SALE_ATTACHED_PRCLS_IND	1
#define	SIZ_SFR_SALE_SYSTEM_DATA			26
#define	SIZ_SFR_SALE_DOCNUM			      13
#define	SIZ_SFR_SALE_DOCTYPE		         4
#define	SIZ_SFR_SALE_SELLER		         74

// Sale MH
#define  OFF_MH_SALE_PARCEL_NUMBER        0
#define  OFF_MH_SALE_SORT_PARCEL_TYPE     13
#define  OFF_MH_SALE_SITUS_STREET_NUMBER  14
#define  OFF_MH_SALE_SITUS_STREET_NAME    21
#define  OFF_MH_SALE_DISTRICT             41
#define  OFF_MH_SALE_RESPONSIBLE_GROUP    43
#define  OFF_MH_SALE_RESPONSIBLE_UNIT     44
#define  OFF_MH_SALE_LAND_TYPE            47
#define  OFF_MH_SALE_SIZE_RANGE           49
#define  OFF_MH_SALE_USE_CODE             51
#define  OFF_MH_SALE_USE_CODE_LITERAL     55
#define  OFF_MH_SALE_LAND_TYPE_LITERAL    65
#define  OFF_MH_SALE_NUMBER_OF_FLOORS     75
#define  OFF_MH_SALE_CONSTRUCTION_TYPE    76
#define  OFF_MH_SALE_QUALITY              77
#define  OFF_MH_SALE_SHAPE                80
#define  OFF_MH_SALE_COST_TABLE_LITERAL   81
#define  OFF_MH_SALE_MH_MAKE_LITRAL       86
#define  OFF_MH_SALE_AREA_OF_INFLUENCE    94
#define  OFF_MH_SALE_OTHER_AREA           102
#define  OFF_MH_SALE_CONSTRUCTION_YEAR    110
#define  OFF_MH_SALE_EFFECTIVE_YEAR       114
#define  OFF_MH_SALE_NUMBER_OF_BATHROOMS  118      // Z6.2
#define  OFF_MH_SALE_NUMBER_OF_BEDROOMS   124
#define  OFF_MH_SALE_FAMILY_ROOM_DEN      126
#define  OFF_MH_SALE_TOTAL_ROOMS          127
#define  OFF_MH_SALE_HEAT ING_SYSTEM      130
#define  OFF_MH_SALE_COOLING_SYSTEM       131
#define  OFF_MH_SALE_NUM_OF_FIREPLACES    132
#define  OFF_MH_SALE_ROOF_TYPE_LITERAL    137
#define  OFF_MH_SALE_FUNC_OB_LITRAL       143
#define  OFF_MH_SALE_TS_SEASON_LITERAL    149
#define  OFF_MH_SALE_PORCH_SQ_FT          155
#define  OFF_MH_SALE_GARAGE_SQ_FT         160
#define  OFF_MH_SALE_GARAGE_CLASS         165
#define  OFF_MH_SALE_GARAGE_STALLS        168
#define  OFF_MH_SALE_CARPORT_SQ_FT        170
#define  OFF_MH_SALE_CARPORT_FACTOR       175
#define  OFF_MH_SALE_CARPORT_STALLS       178
#define  OFF_MH_SALE_PARKING_DECK_SQ_FT   180
#define  OFF_MH_SALE_PARKING_DECK_FACTOR  185
#define  OFF_MH_SALE_PARKING_DECK_STALLS  188
#define  OFF_MH_SALE_MISCELLANEOUS_RCN    190
#define  OFF_MH_SALE_POOL_TYPE_LITERAL    196
#define  OFF_MH_SALE_SPCL_IMPR_RCN        201
#define  OFF_MH_SALE_SPCL_IMPR1_LITERAL   207
#define  OFF_MH_SALE_SPCL_IMPR2_LITERAL   212
#define  OFF_MH_SALE_SPCL_IMPR3_LITERAL   217
#define  OFF_MH_SALE_SPCL_IMPR4_LITERAL   222
#define  OFF_MH_SALE_MH_FOUNDATION_LITR   227
#define  OFF_MH_SALE_SALE_DATE            232
#define  OFF_MH_SALE_SALE_TYPE            242
#define  OFF_MH_SALE_SALE_PRICE           243
#define  OFF_MH_SALE_BAD_SALE_LITERAL     254
#define  OFF_MH_SALE_PRICE_PER_SQFT       260
#define  OFF_MH_SALE_PRICE_PER_SQFT_AST   269
#define  OFF_MH_SALE_PRICE_PER_UNIT       270
#define  OFF_MH_SALE_PRICE_PER_UNIT_AST   281
#define  OFF_MH_SALE_CONDITION_LITERAL    282
#define  OFF_MH_SALE_SALE_USE_CODE        288
#define  OFF_MH_SALE_UNUSED               292
#define  OFF_MH_SALE_REMARK_1_LITERAL     297
#define  OFF_MH_SALE_REMARK_2_LITERAL     305
#define  OFF_MH_SALE_REMARK_3_LITERAL     313
#define  OFF_MH_SALE_REMARK_4_LITERAL     321
#define  OFF_MH_SALE_REMARK_5_LITERAL     329
#define  OFF_MH_SALE_PRIOR_SALE_DATE      337
#define  OFF_MH_SALE_PRIOR_SALE_PRICE     347
#define  OFF_MH_SALE_PCT_CHG_PER_MONTH    358
#define  OFF_MH_SALE_APPRL_CMPL_TYPE      367
#define  OFF_MH_SALE_APPRL_CMPL_LITERAL   370
#define  OFF_MH_SALE_SITUS_ZIP_CODE       380
#define  OFF_MH_SALE_SITUS_ZIP_PLUS4      385
#define  OFF_MH_SALE_LAND_VALUE           389
#define  OFF_MH_SALE_IMPR_VALUE           400
#define  OFF_MH_SALE_TOTAL_VALUE          411
#define  OFF_MH_SALE_REQUEST_DATE         422
#define	OFF_MH_SALE_DOCNUM			      432
#define	OFF_MH_SALE_DOCTYPE		         445
#define	OFF_MH_SALE_SELLER		         449
#define	OFF_MH_SALE_PRIOR_DOCNUM			523
#define	OFF_MH_SALE_PRIOR_DOCTYPE		   536
#define	OFF_MH_SALE_PRIOR_SELLER		   540

#define  SIZ_MH_SALE_PARCEL_NUMBER        13
#define  SIZ_MH_SALE_SORT_PARCEL_TYPE     1
#define  SIZ_MH_SALE_SITUS_STREET_NUMBER  7
#define  SIZ_MH_SALE_SITUS_STREET_NAME    20
#define  SIZ_MH_SALE_DISTRICT             2
#define  SIZ_MH_SALE_RESPONSIBLE_GROUP    1
#define  SIZ_MH_SALE_RESPONSIBLE_UNIT     3
#define  SIZ_MH_SALE_LAND_TYPE            2
#define  SIZ_MH_SALE_SIZE_RANGE           2
#define  SIZ_MH_SALE_USE_CODE             4
#define  SIZ_MH_SALE_USE_CODE_LITERAL     10
#define  SIZ_MH_SALE_LAND_TYPE_LITERAL    10
#define  SIZ_MH_SALE_NUMBER_OF_FLOORS     1
#define  SIZ_MH_SALE_CONSTRUCTION_TYPE    1
#define  SIZ_MH_SALE_QUALITY              3
#define  SIZ_MH_SALE_SHAPE                1
#define  SIZ_MH_SALE_COST_TABLE_LITERAL   5
#define  SIZ_MH_SALE_MH_MAKE_LITRAL       8
#define  SIZ_MH_SALE_AREA_OF_INFLUENCE    8
#define  SIZ_MH_SALE_OTHER_AREA           8
#define  SIZ_MH_SALE_CONSTRUCTION_YEAR    4
#define  SIZ_MH_SALE_EFFECTIVE_YEAR       4
#define  SIZ_MH_SALE_NUMBER_OF_BATHROOMS  6
#define  SIZ_MH_SALE_NUMBER_OF_BEDROOMS   2
#define  SIZ_MH_SALE_FAMILY_ROOM_DEN      1
#define  SIZ_MH_SALE_TOTAL_ROOMS          3
#define  SIZ_MH_SALE_HEAT ING_SYSTEM      1
#define  SIZ_MH_SALE_COOLING_SYSTEM       1
#define  SIZ_MH_SALE_NUM_OF_FIREPLACES    5
#define  SIZ_MH_SALE_ROOF_TYPE_LITERAL    6
#define  SIZ_MH_SALE_FUNC_OB_LITRAL       6
#define  SIZ_MH_SALE_TS_SEASON_LITERAL    6
#define  SIZ_MH_SALE_PORCH_SQ_FT          5
#define  SIZ_MH_SALE_GARAGE_SQ_FT         5
#define  SIZ_MH_SALE_GARAGE_CLASS         3
#define  SIZ_MH_SALE_GARAGE_STALLS        2
#define  SIZ_MH_SALE_CARPORT_SQ_FT        5
#define  SIZ_MH_SALE_CARPORT_FACTOR       3
#define  SIZ_MH_SALE_CARPORT_STALLS       2
#define  SIZ_MH_SALE_PARKING_DECK_SQ_FT   5
#define  SIZ_MH_SALE_PARKING_DECK_FACTOR  3
#define  SIZ_MH_SALE_PARKING_DECK_STALLS  2
#define  SIZ_MH_SALE_MISCELLANEOUS_RCN    6
#define  SIZ_MH_SALE_POOL_TYPE_LITERAL    5
#define  SIZ_MH_SALE_SPCL_IMPR_RCN        6
#define  SIZ_MH_SALE_SPCL_IMPR1_LITERAL   5
#define  SIZ_MH_SALE_SPCL_IMPR2_LITERAL   5
#define  SIZ_MH_SALE_SPCL_IMPR3_LITERAL   5
#define  SIZ_MH_SALE_SPCL_IMPR4_LITERAL   5
#define  SIZ_MH_SALE_MH_FOUNDATION_LITR   5
#define  SIZ_MH_SALE_SALE_DATE            10
#define  SIZ_MH_SALE_SALE_TYPE            1
#define  SIZ_MH_SALE_SALE_PRICE           11
#define  SIZ_MH_SALE_BAD_SALE_LITERAL     6
#define  SIZ_MH_SALE_PRICE_PER_SQFT       9
#define  SIZ_MH_SALE_PRICE_PER_SQFT_AST   1
#define  SIZ_MH_SALE_PRICE_PER_UNIT       11
#define  SIZ_MH_SALE_PRICE_PER_UNIT_AST   1
#define  SIZ_MH_SALE_CONDITION_LITERAL    6
#define  SIZ_MH_SALE_SALE_USE_CODE        4
#define  SIZ_MH_SALE_UNUSED               5
#define  SIZ_MH_SALE_REMARK_1_LITERAL     8
#define  SIZ_MH_SALE_REMARK_2_LITERAL     8
#define  SIZ_MH_SALE_REMARK_3_LITERAL     8
#define  SIZ_MH_SALE_REMARK_4_LITERAL     8
#define  SIZ_MH_SALE_REMARK_5_LITERAL     8
#define  SIZ_MH_SALE_PRIOR_SALE_DATE      10
#define  SIZ_MH_SALE_PRIOR_SALE_PRICE     11
#define  SIZ_MH_SALE_PCT_CHG_PER_MONTH    9
#define  SIZ_MH_SALE_APPRL_CMPL_TYPE      3
#define  SIZ_MH_SALE_APPRL_CMPL_LITERAL   10
#define  SIZ_MH_SALE_SITUS_ZIP_CODE       5
#define  SIZ_MH_SALE_SITUS_ZIP_PLUS4      4
#define  SIZ_MH_SALE_LAND_VALUE           11
#define  SIZ_MH_SALE_IMPR_VALUE           11
#define  SIZ_MH_SALE_TOTAL_VALUE          11
#define  SIZ_MH_SALE_REQUEST_DATE         10
                                          
// Land
#define  OFF_LAND_SALE_PARCEL_NUMBER      0
#define  OFF_LAND_SALE_SORT_PARCEL_TYPE   13
#define  OFF_LAND_SALE_SITUS_STREET_NBR   14
#define  OFF_LAND_SALE_SITUS_STREET_NAME  21
#define  OFF_LAND_SALE_DISTRICT           41
#define  OFF_LAND_SALE_RESPONSIBLE_GROUP  43 
#define  OFF_LAND_SALE_RESPONSIBLE_UNIT   44
#define  OFF_LAND_SALE_LAND_TYPE          47
#define  OFF_LAND_SALE_SIZE_RANGE         49
#define  OFF_LAND_SALE_USE_CODE           51
#define  OFF_LAND_SALE_USE_CODE_LITERAL   55
#define  OFF_LAND_SALE_LAND_TYPE_LITERAL  65
#define  OFF_LAND_SALE_ZONING             75
#define  OFF_LAND_SALE_LOT_WIDTH          83
#define  OFF_LAND_SALE_LOT_DEPTH          90
#define  OFF_LAND_SALE_LOT_SIZE           97
#define  OFF_LAND_SALE_LOT_SIZE_UNIT      108
#define  OFF_LAND_SALE_GROSS_ACRES        110
#define  OFF_LAND_SALE_NET_ACRES          118
#define  OFF_LAND_SALE_ACCESS_LITERAL     126
#define  OFF_LAND_SALE_SLOPE_DIR_LITERAL  132
#define  OFF_LAND_SALE_VIEW_LITERAL       138
#define  OFF_LAND_SALE_SEWER_LITERAL      147
#define  OFF_LAND_SALE_WATER_LITERAL      153
#define  OFF_LAND_SALE_ELECTRIC_LITERAL   159
#define  OFF_LAND_SALE_GAS_LITERAL        165
#define  OFF_LAND_SALE_OFFSITES_LITERAL   171
#define  OFF_LAND_SALE_ENCROACH_LITERAL   177
#define  OFF_LAND_SALE_NUISANCE1_LITERAL  183
#define  OFF_LAND_SALE_NUISANCE2_LITERAL  189
#define  OFF_LAND_SALE_INFLUENCE1_LITERAL 195
#define  OFF_LAND_SALE_INFLUENCE2_LITERAL 201
#define  OFF_LAND_SALE_DOCK_RIGHT_LITERAL 207
#define  OFF_LAND_SALE_LAND_LSE_EXPR_DATE 213
#define  OFF_LAND_SALE_NUMBER_OF_LOTS     223
#define  OFF_LAND_SALE_MAP_STATUS_LITERAL 227
#define  OFF_LAND_SALE_DEV_STATUS_LITERAL 237
#define  OFF_LAND_SALE_SALE_DATE          247
#define  OFF_LAND_SALE_SALE_TYPE          257
#define  OFF_LAND_SALE_SALE_PRICE         258
#define  OFF_LAND_SALE_BAD_SALE_LITERAL   269  
#define  OFF_LAND_SALE_PRICE_PER_SQFT     275
#define  OFF_LAND_SALE_PRICE_PER_SQFT_AST 284   
#define  OFF_LAND_SALE_PRICE_PER_UNIT     285  
#define  OFF_LAND_SALE_PRICE_PER_UNIT_AST 296  
#define  OFF_LAND_SALE_CONDITION_LITERAL  297  
#define  OFF_LAND_SALE_SALE_USE_CODE      303
#define  OFF_LAND_SALE_UNUSED             307
#define  OFF_LAND_SALE_REMARK_1_LITERAL   312  
#define  OFF_LAND_SALE_REMARK_2_LITERAL   320  
#define  OFF_LAND_SALE_REMARK_3_LITERAL   328  
#define  OFF_LAND_SALE_REMARK_4_LITERAL   336  
#define  OFF_LAND_SALE_REMARK_5_LITERAL   344  
#define  OFF_LAND_SALE_PRIOR_SALE_DATE    352
#define  OFF_LAND_SALE_PRIOR_SALE_PRICE   362   
#define  OFF_LAND_SALE_PCT_CHG_PER_MONTH  373  
#define  OFF_LAND_SALE_REQUEST_DATE       382
#define  OFF_LAND_SALE_AS_PRIOR_YR        392
#define  OFF_LAND_SALE_AS_PRIOR_LAND      396
#define  OFF_LAND_SALE_AS_PRIOR_IMPR      407
#define  OFF_LAND_SALE_AS_PRIOR_SPCL_EXE  418
#define  OFF_LAND_SALE_AS_CURR_YR         429
#define  OFF_LAND_SALE_AS_CURR_LAND       433
#define  OFF_LAND_SALE_AS_CURR_IMPR       444
#define  OFF_LAND_SALE_AS_CURR_SPCL_EXE   455
#define  OFF_LAND_SALE_SYSTEM_DATA        466
#define  OFF_LAND_SALE_PRICE_PER_ACRE     492
#define	OFF_LAND_SALE_DOCNUM			      503
#define	OFF_LAND_SALE_DOCTYPE		      516
#define	OFF_LAND_SALE_SELLER		         520
#define	OFF_LAND_SALE_PRIOR_DOCNUM			594
#define	OFF_LAND_SALE_PRIOR_DOCTYPE      607
#define	OFF_LAND_SALE_PRIOR_SELLER		   611

#define  SIZ_LAND_SALE_PARCEL_NUMBER      13
#define  SIZ_LAND_SALE_SORT_PARCEL_TYPE   1
#define  SIZ_LAND_SALE_SITUS_STREET_NBR   7
#define  SIZ_LAND_SALE_SITUS_STREET_NAME  20
#define  SIZ_LAND_SALE_DISTRICT           2
#define  SIZ_LAND_SALE_RESPONSIBLE_GROUP  1
#define  SIZ_LAND_SALE_RESPONSIBLE_UNIT   3
#define  SIZ_LAND_SALE_LAND_TYPE          2
#define  SIZ_LAND_SALE_SIZE_RANGE         2
#define  SIZ_LAND_SALE_USE_CODE           4
#define  SIZ_LAND_SALE_USE_CODE_LITERAL   10
#define  SIZ_LAND_SALE_LAND_TYPE_LITERAL  10
#define  SIZ_LAND_SALE_ZONING             8
#define  SIZ_LAND_SALE_LOT_WIDTH          7
#define  SIZ_LAND_SALE_LOT_DEPTH          7
#define  SIZ_LAND_SALE_LOT_SIZE           11
#define  SIZ_LAND_SALE_LOT_SIZE_UNIT      2
#define  SIZ_LAND_SALE_GROSS_ACRES        8
#define  SIZ_LAND_SALE_NET_ACRES          8
#define  SIZ_LAND_SALE_ACCESS_LITERAL     6
#define  SIZ_LAND_SALE_SLOPE_DIR_LITERAL  6
#define  SIZ_LAND_SALE_VIEW_LITERAL       9
#define  SIZ_LAND_SALE_SEWER_LITERAL      6
#define  SIZ_LAND_SALE_WATER_LITERAL      6
#define  SIZ_LAND_SALE_ELECTRIC_LITERAL   6
#define  SIZ_LAND_SALE_GAS_LITERAL        6
#define  SIZ_LAND_SALE_OFFSITES_LITERAL   6
#define  SIZ_LAND_SALE_ENCROACH_LITERAL   6
#define  SIZ_LAND_SALE_NUISANCE1_LITERAL  6
#define  SIZ_LAND_SALE_NUISANCE2_LITERAL  6
#define  SIZ_LAND_SALE_INFLUENCE1_LITERAL 6
#define  SIZ_LAND_SALE_INFLUENCE2_LITERAL 6
#define  SIZ_LAND_SALE_DOCK_RIGHT_LITERAL 6
#define  SIZ_LAND_SALE_LAND_LSE_EXPR_DATE 10
#define  SIZ_LAND_SALE_NUMBER_OF_LOTS     4
#define  SIZ_LAND_SALE_MAP_STATUS_LITERAL 10
#define  SIZ_LAND_SALE_DEV_STATUS_LITERAL 10
#define  SIZ_LAND_SALE_SALE_DATE          10
#define  SIZ_LAND_SALE_SALE_TYPE          1
#define  SIZ_LAND_SALE_SALE_PRICE         11
#define  SIZ_LAND_SALE_BAD_SALE_LITERAL   6 
#define  SIZ_LAND_SALE_PRICE_PER_SQFT     9
#define  SIZ_LAND_SALE_PRICE_PER_SQFT_AST 1  
#define  SIZ_LAND_SALE_PRICE_PER_UNIT     11 
#define  SIZ_LAND_SALE_PRICE_PER_UNIT_AST 1 
#define  SIZ_LAND_SALE_CONDITION_LITERAL  6 
#define  SIZ_LAND_SALE_SALE_USE_CODE      4
#define  SIZ_LAND_SALE_UNUSED             5
#define  SIZ_LAND_SALE_REMARK_1_LITERAL   8 
#define  SIZ_LAND_SALE_REMARK_2_LITERAL   8 
#define  SIZ_LAND_SALE_REMARK_3_LITERAL   8 
#define  SIZ_LAND_SALE_REMARK_4_LITERAL   8 
#define  SIZ_LAND_SALE_REMARK_5_LITERAL   8 
#define  SIZ_LAND_SALE_PRIOR_SALE_DATE    10
#define  SIZ_LAND_SALE_PRIOR_SALE_PRICE   11  
#define  SIZ_LAND_SALE_PCT_CHG_PER_MONTH  9 
#define  SIZ_LAND_SALE_REQUEST_DATE       10
#define  SIZ_LAND_SALE_AS_PRIOR_YR        4
#define  SIZ_LAND_SALE_AS_PRIOR_LAND      11
#define  SIZ_LAND_SALE_AS_PRIOR_IMPR      11
#define  SIZ_LAND_SALE_AS_PRIOR_SPCL_EXE  11
#define  SIZ_LAND_SALE_AS_CURR_YR         4
#define  SIZ_LAND_SALE_AS_CURR_LAND       11
#define  SIZ_LAND_SALE_AS_CURR_IMPR       11
#define  SIZ_LAND_SALE_AS_CURR_SPCL_EXE   11
#define  SIZ_LAND_SALE_SYSTEM_DATA        26
#define  SIZ_LAND_SALE_PRICE_PER_ACRE     11

// Multi-parcel sale
#define  OFF_MP_SALE_LEAD_PARCEL_NUMBER   0
#define  OFF_MP_SALE_SORT_DIST            13
#define  OFF_MP_SALE_SORT_DATE            15
#define  OFF_MP_SALE_PARCEL_NUMBER        25
#define  OFF_MP_SALE_CHAR_TYPE            38
#define  OFF_MP_SALE_CHAR_SEQUENCE_NBR    39
#define  OFF_MP_SALE_IMPROVED_LITERAL     43 
#define  OFF_MP_SALE_USE_CODE             46 
#define  OFF_MP_SALE_SALE_DATE            50 
#define  OFF_MP_SALE_SALE_TYPE            60 
#define  OFF_MP_SALE_SALE_PRICE           61 
#define  OFF_MP_SALE_BAD_SALE_LITERAL     72 
#define  OFF_MP_SALE_PRICE_SQF            78 
#define  OFF_MP_SALE_PRICE_SQF_AST        87 
#define  OFF_MP_SALE_PRICE_UNIT           88 
#define  OFF_MP_SALE_PRICE_UNIT_AST       99 
#define  OFF_MP_SALE_CONDITION_LITERAL    100 
#define  OFF_MP_SALE_SALE_USE_CODE        106
#define  OFF_MP_SALE_UNUSED               110
#define  OFF_MP_SALE_REMARK_1_LITERAL     115
#define  OFF_MP_SALE_REMARK_2_LITERAL     123
#define  OFF_MP_SALE_REMARK_3_LITERAL     131
#define  OFF_MP_SALE_REMARK_4_LITERAL     139
#define  OFF_MP_SALE_REMARK_5_LITERAL     147
#define  OFF_MP_SALE_PRIOR_SALE_DATE      155
#define  OFF_MP_SALE_PRIOR_SALE_PRICE     165
#define  OFF_MP_SALE_PCT_CHG_PER_MONTH    176
#define  OFF_MP_SALE_SITUS_STREET_NBR     185
#define  OFF_MP_SALE_SITUS_STREET_NAME    192
#define  OFF_MP_SALE_DISTRICT             212
#define  OFF_MP_SALE_RESPONSIBLE_GROUP    214
#define  OFF_MP_SALE_RESPONSIBLE_UNIT     215
#define  OFF_MP_SALE_LAND_TYPE            218
#define  OFF_MP_SALE_SIZE_RANGE           220
#define  OFF_MP_SALE_PARCEL_USE_CODE      222
#define  OFF_MP_SALE_PARCEL_USE_LITERAL   226
#define  OFF_MP_SALE_LAND_TYPE_LITERAL    236
#define  OFF_MP_SALE_QUALITY_STRU_TYPE    246
#define  OFF_MP_SALE_OTH_STRU_USE_LITERAL 248
#define  OFF_MP_SALE_GROSS_LEASABLE_AREA  258
#define  OFF_MP_SALE_NUMBER_OF_UNITS      266
#define  OFF_MP_SALE_OTH_STRU_EFF_YEAR    270
#define  OFF_MP_SALE_CONSTRUCTION_TYPE    274
#define  OFF_MP_SALE_QUALITY              275
#define  OFF_MP_SALE_SHAPE                278
#define  OFF_MP_SALE_SUM_OF_AREA          279
#define  OFF_MP_SALE_SFR_EFFECTIVE_YEAR   287
#define  OFF_MP_SALE_ZONING               291
#define  OFF_MP_SALE_LOT_WIDTH            299
#define  OFF_MP_SALE_LOT_DEPTH            306
#define  OFF_MP_SALE_LOT_SIZE             313
#define  OFF_MP_SALE_LOT_SIZE_UNIT        324      // SQ or blank
#define  OFF_MP_SALE_GROSS_ACRES          326
#define  OFF_MP_SALE_NET_ACRES            334
#define  OFF_MP_SALE_ACCESS_LITERAL       342
#define  OFF_MP_SALE_SLOPE_DIR_LITERAL    348
#define  OFF_MP_SALE_VIEW_LITERAL         354
#define  OFF_MP_SALE_SEWER_LITERAL        363
#define  OFF_MP_SALE_WATER_LITERAL        369
#define  OFF_MP_SALE_ELECTRIC_LITERAL     375
#define  OFF_MP_SALE_GAS_LITERAL          381
#define  OFF_MP_SALE_OFFSITES_LITERAL     387
#define  OFF_MP_SALE_EASE_ENCR_LITERAL    393
#define  OFF_MP_SALE_NUISANCE1_LITERAL    399
#define  OFF_MP_SALE_NUISANCE2_LITERAL    405
#define  OFF_MP_SALE_INFLUENCE1_LITERAL   411
#define  OFF_MP_SALE_INFLUENCE2_LITERAL   417
#define  OFF_MP_SALE_DOCK_RGHTS_LITERAL   423
#define  OFF_MP_SALE_LAND_LEASE_EXPR_DATE 429
#define  OFF_MP_SALE_NUMBER_OF_LOTS       439
#define  OFF_MP_SALE_MAP_STATUS           443
#define  OFF_MP_SALE_DEVELOPMENT_STATUS   453
#define  OFF_MP_SALE_REQUEST_DATE         463
#define	OFF_MP_SALE_DOCNUM			      473
#define	OFF_MP_SALE_DOCTYPE		         486
#define	OFF_MP_SALE_SELLER		         490
#define	OFF_MP_SALE_PRIOR_DOCNUM			564
#define	OFF_MP_SALE_PRIOR_DOCTYPE		   577
#define	OFF_MP_SALE_PRIOR_SELLER		   581

#define  SIZ_MP_SALE_LEAD_PARCEL_NUMBER   13
#define  SIZ_MP_SALE_SORT_DIST            2
#define  SIZ_MP_SALE_SORT_DATE            10
#define  SIZ_MP_SALE_PARCEL_NUMBER        13
#define  SIZ_MP_SALE_CHAR_TYPE            1
#define  SIZ_MP_SALE_CHAR_SEQUENCE_NBR    4
#define  SIZ_MP_SALE_IMPROVED_LITERAL     3
#define  SIZ_MP_SALE_USE_CODE             4
#define  SIZ_MP_SALE_SALE_DATE            10
#define  SIZ_MP_SALE_SALE_TYPE            1
#define  SIZ_MP_SALE_SALE_PRICE           11
#define  SIZ_MP_SALE_BAD_SALE_LITERAL     6
#define  SIZ_MP_SALE_PRICE_SQF            9
#define  SIZ_MP_SALE_PRICE_SQF_AST        1
#define  SIZ_MP_SALE_PRICE_UNIT           11
#define  SIZ_MP_SALE_PRICE_UNIT_AST       1
#define  SIZ_MP_SALE_CONDITION_LITERAL    6
#define  SIZ_MP_SALE_SALE_USE_CODE        4
#define  SIZ_MP_SALE_UNUSED               5
#define  SIZ_MP_SALE_REMARK_1_LITERAL     8
#define  SIZ_MP_SALE_REMARK_2_LITERAL     8
#define  SIZ_MP_SALE_REMARK_3_LITERAL     8
#define  SIZ_MP_SALE_REMARK_4_LITERAL     8
#define  SIZ_MP_SALE_REMARK_5_LITERAL     8
#define  SIZ_MP_SALE_PRIOR_SALE_DATE      10
#define  SIZ_MP_SALE_PRIOR_SALE_PRICE     11
#define  SIZ_MP_SALE_PCT_CHG_PER_MONTH    9
#define  SIZ_MP_SALE_SITUS_STREET_NBR     7
#define  SIZ_MP_SALE_SITUS_STREET_NAME    20
#define  SIZ_MP_SALE_DISTRICT             2
#define  SIZ_MP_SALE_RESPONSIBLE_GROUP    1
#define  SIZ_MP_SALE_RESPONSIBLE_UNIT     3
#define  SIZ_MP_SALE_LAND_TYPE            2
#define  SIZ_MP_SALE_SIZE_RANGE           2
#define  SIZ_MP_SALE_PARCEL_USE_CODE      4
#define  SIZ_MP_SALE_PARCEL_USE_LITERAL   10
#define  SIZ_MP_SALE_LAND_TYPE_LITERAL    10
#define  SIZ_MP_SALE_QUALITY_STRU_TYPE    2
#define  SIZ_MP_SALE_OTH_STRU_USE_LITERAL 10
#define  SIZ_MP_SALE_GROSS_LEASABLE_AREA  8
#define  SIZ_MP_SALE_NUMBER_OF_UNITS      4
#define  SIZ_MP_SALE_OTH_STRU_EFF_YEAR    4
#define  SIZ_MP_SALE_CONSTRUCTION_TYPE    1
#define  SIZ_MP_SALE_QUALITY              3
#define  SIZ_MP_SALE_SHAPE                1
#define  SIZ_MP_SALE_SUM_OF_AREA          8
#define  SIZ_MP_SALE_SFR_EFFECTIVE_YEAR   4
#define  SIZ_MP_SALE_ZONING               8
#define  SIZ_MP_SALE_LOT_WIDTH            7
#define  SIZ_MP_SALE_LOT_DEPTH            7
#define  SIZ_MP_SALE_LOT_SIZE             11
#define  SIZ_MP_SALE_LOT_SIZE_UNIT        2
#define  SIZ_MP_SALE_GROSS_ACRES          8
#define  SIZ_MP_SALE_NET_ACRES            8
#define  SIZ_MP_SALE_ACCESS_LITERAL       6
#define  SIZ_MP_SALE_SLOPE_DIR_LITERAL    6
#define  SIZ_MP_SALE_VIEW_LITERAL         9
#define  SIZ_MP_SALE_SEWER_LITERAL        6
#define  SIZ_MP_SALE_WATER_LITERAL        6
#define  SIZ_MP_SALE_ELECTRIC_LITERAL     6
#define  SIZ_MP_SALE_GAS_LITERAL          6
#define  SIZ_MP_SALE_OFFSITES_LITERAL     6
#define  SIZ_MP_SALE_EASE_ENCR_LITERAL    6
#define  SIZ_MP_SALE_NUISANCE1_LITERAL    6
#define  SIZ_MP_SALE_NUISANCE2_LITERAL    6
#define  SIZ_MP_SALE_INFLUENCE1_LITERAL   6
#define  SIZ_MP_SALE_INFLUENCE2_LITERAL   6
#define  SIZ_MP_SALE_DOCK_RGHTS_LITERAL   6
#define  SIZ_MP_SALE_LAND_LEASE_EXPR_DATE 10
#define  SIZ_MP_SALE_NUMBER_OF_LOTS       4
#define  SIZ_MP_SALE_MAP_STATUS           10
#define  SIZ_MP_SALE_DEVELOPMENT_STATUS   10
#define  SIZ_MP_SALE_REQUEST_DATE         10

// Dock sale
#define  OFF_DOCK_SALE_PARCEL_NUMBER      0
#define  OFF_DOCK_SALE_DISTRICT           13
#define  OFF_DOCK_SALE_SALE_DATE          15
#define  OFF_DOCK_SALE_SALE_TYPE          25
#define  OFF_DOCK_SALE_SALE_PRICE         26
#define  OFF_DOCK_SALE_DOCK_RIGHTS        37
#define  OFF_DOCK_SALE_DOCK_RIGHTS_DESC   38
#define  OFF_DOCK_SALE_LA_ASSOC_ID        44
#define  OFF_DOCK_SALE_PARCEL_NUMBER_FROM 59
#define	OFF_DOCK_SALE_DOCNUM			      72
#define	OFF_DOCK_SALE_DOCTYPE		      85
#define	OFF_DOCK_SALE_SELLER		         89
#define	OFF_DOCK_SALE_PRIOR_DOCNUM			163
#define	OFF_DOCK_SALE_PRIOR_DOCTYPE		176
#define	OFF_DOCK_SALE_PRIOR_SELLER		   180

#define  SIZ_DOCK_SALE_PARCEL_NUMBER      13
#define  SIZ_DOCK_SALE_DISTRICT           2
#define  SIZ_DOCK_SALE_SALE_DATE          10
#define  SIZ_DOCK_SALE_SALE_TYPE          1
#define  SIZ_DOCK_SALE_SALE_PRICE         11
#define  SIZ_DOCK_SALE_DOCK_RIGHTS        1
#define  SIZ_DOCK_SALE_DOCK_RIGHTS_DESC   6
#define  SIZ_DOCK_SALE_LA_ASSOC_ID        15
#define  SIZ_DOCK_SALE_PARCEL_NUMBER_FROM 13

// Other sale properties
#define  OFF_OTH_SALE_PARCEL_NUMBER       1   -1
#define  OFF_OTH_SALE_CHAR_TYPE           14  -1
#define  OFF_OTH_SALE_CHAR_SEQUENCE_NBR   15  -1
#define  OFF_OTH_SALE_SITUS_STREET_NUMBER 19  -1
#define  OFF_OTH_SALE_SITUS_STREET_NAME   26  -1
#define  OFF_OTH_SALE_DISTRICT            47  -1
#define  OFF_OTH_SALE_RESPONSIBLE_GROUP   48  -1
#define  OFF_OTH_SALE_RESPONSIBLE_UNIT    49  -1
#define  OFF_OTH_SALE_LAND_TYPE           52  -1
#define  OFF_OTH_SALE_SIZE_RANGE          54  -1
#define  OFF_OTH_SALE_USE_CODE            56  -1 
#define  OFF_OTH_SALE_USE_CODE_LITERAL    60  -1
#define  OFF_OTH_SALE_LAND_TYPE_LITERAL   70  -1
#define  OFF_OTH_SALE_STRUCT_QUALITY_TYPE 80  -1
#define  OFF_OTH_SALE_USE_LITERAL         82  -1
#define  OFF_OTH_SALE_GROSS_LSEABLE_AREA  92  -1
#define  OFF_OTH_SALE_NET_LEASEABLE_AREA  100 -1
#define  OFF_OTH_SALE_CONSTRUCTION_YEAR   108 -1
#define  OFF_OTH_SALE_EFFECTIVE_YEAR      112 -1
#define  OFF_OTH_SALE_NUMBER_OF_UNITS     116 -1
#define  OFF_OTH_SALE_UNIT_AVERAGE_SQ_FT  120 -1
#define  OFF_OTH_SALE_NUMBER_OF_STORIES   128 -1
#define  OFF_OTH_SALE_STORY_HEIGHT        130 -1
#define  OFF_OTH_SALE_ELEVATORS           132 -1
#define  OFF_OTH_SALE_FUNC_OBS_LITRAL     134 -1
#define  OFF_OTH_SALE_LAND_TO_BLDG_RATIO  140 -1
#define  OFF_OTH_SALE_LAND_LSE_EXPR_DATE  146 -1
#define  OFF_OTH_SALE_OTHRPOOL_LITERAL    157 -1
#define  OFF_OTH_SALE_OTHER_SPCL_IMPRS    160 -1
#define  OFF_OTH_SALE_OTHER_SPCL_IMPR1    166 -1
#define  OFF_OTH_SALE_OTHER_SPCL_IMPR2    171 -1
#define  OFF_OTH_SALE_OTHER_SPCL_IMPR3    176 -1
#define  OFF_OTH_SALE_OTHER_SPCL_IMPR4    181 -1
#define  OFF_OTH_SALE_NUMBER_ROOMS        186 -1
#define  OFF_OTH_SALE_NUMBER_DOORS        190 -1
#define  OFF_OTH_SALE_TENANT_IMPR_PCT     194 -1
#define  OFF_OTH_SALE_CONSTRUCTION_TYPE   197 -1
#define  OFF_OTH_SALE_QUALITY             198 -1
#define  OFF_OTH_SALE_SHAPE               201 -1
#define  OFF_OTH_SALE_AREA_OF_INFLUENCE   202 -1
#define  OFF_OTH_SALE_SFR_YRBLT           210 -1
#define  OFF_OTH_SALE_SFR_YREFF           214 -1
#define  OFF_OTH_SALE_OTHER_AREA          218 -1
#define  OFF_OTH_SALE_BATHROOMS           226 -1
#define  OFF_OTH_SALE_BEDROOMS            232 -1
#define  OFF_OTH_SALE_FAMILY_ROOM_DEN     234 -1
#define  OFF_OTH_SALE_GARAGE_SQ_FT        235 -1
#define  OFF_OTH_SALE_GARAGE_CLASS        240 -1
#define  OFF_OTH_SALE_GARAGE_STALLS       243 -1
#define  OFF_OTH_SALE_SFRPOOL_LITERAL     245 -1
#define  OFF_OTH_SALE_SFR_SPCL_IMPRS      250 -1
#define  OFF_OTH_SALE_SFR_SPCL_IMPR1      256 -1
#define  OFF_OTH_SALE_SFR_SPCL_IMPR2      261 -1
#define  OFF_OTH_SALE_SFR_SPCL_IMPR3      266 -1
#define  OFF_OTH_SALE_SFR_SPCL_IMPR4      271 -1
#define  OFF_OTH_SALE_ZONING              276 -1
#define  OFF_OTH_SALE_LOT_WIDTH           284 -1
#define  OFF_OTH_SALE_LOT_DEPTH           291 -1
#define  OFF_OTH_SALE_LOT_SIZE            298 -1
#define  OFF_OTH_SALE_LOT_SIZE_UNIT       309 -1
#define  OFF_OTH_SALE_GROSS_ACRES         311 -1
#define  OFF_OTH_SALE_NET_ACRES           319 -1
#define  OFF_OTH_SALE_ACCESS_LITERAL      327 -1
#define  OFF_OTH_SALE_SLOPE_DIR_LITERAL   333 -1
#define  OFF_OTH_SALE_VIEW_LITERAL        339 -1
#define  OFF_OTH_SALE_SEWER_LITERAL       348 -1
#define  OFF_OTH_SALE_WATER_LITERAL       354 -1
#define  OFF_OTH_SALE_ELECTRIC_LITERAL    360 -1
#define  OFF_OTH_SALE_GAS_LITERAL         366 -1
#define  OFF_OTH_SALE_OFFSITES_LITERAL    372 -1
#define  OFF_OTH_SALE_ENCROACH_LITERAL    378 -1
#define  OFF_OTH_SALE_NUISANCE1_LITERAL   384 -1
#define  OFF_OTH_SALE_NUISANCE2_LITERAL   390 -1
#define  OFF_OTH_SALE_INFLUENCE1_LITERAL  396 -1
#define  OFF_OTH_SALE_INFLUENCE2_LITERAL  402 -1
#define  OFF_OTH_SALE_DOCK_RGHTS_LITERAL  408 -1
#define  OFF_OTH_SALE_LEASE_EXPIR         414 -1
#define  OFF_OTH_SALE_NUMBER_LOTS         424 -1
#define  OFF_OTH_SALE_MAP_STATUS_LITERAL  428 -1
#define  OFF_OTH_SALE_DEV_STATUS_LITERAL  438 -1
#define  OFF_OTH_SALE_SALE_DATE           448 -1
#define  OFF_OTH_SALE_SALE_TYPE           458 -1
#define  OFF_OTH_SALE_SALE_PRICE          459 -1
#define  OFF_OTH_SALE_BAD_SALE_LITERAL    470 -1
#define  OFF_OTH_SALE_PRICE_PER_SQ_FT     476 -1
#define  OFF_OTH_SALE_PRICE_PER_SQ_FT_AST 485 -1
#define  OFF_OTH_SALE_PRICE_PER_UNIT      486 -1
#define  OFF_OTH_SALE_PRICE_PER_UNIT_AST  497 -1
#define  OFF_OTH_SALE_CONDITION_LITERAL   498 -1
#define  OFF_OTH_SALE_SALE_USE_CODE       504 -1
#define  OFF_OTH_SALE_UNUSED              508 -1
#define  OFF_OTH_SALE_REMARK_1_LITERAL    513 -1
#define  OFF_OTH_SALE_REMARK_2_LITERAL    521 -1
#define  OFF_OTH_SALE_REMARK_3_LITERAL    529 -1
#define  OFF_OTH_SALE_REMARK_4_LITERAL    537 -1
#define  OFF_OTH_SALE_REMARK_5_LITERAL    545 -1
#define  OFF_OTH_SALE_PRIOR_SALE_DATE     553 -1
#define  OFF_OTH_SALE_PRIOR_SALE_PRICE    563 -1
#define  OFF_OTH_SALE_PCT_CHG_PER_MONTH   574 -1
#define  OFF_OTH_SALE_REQUEST_DATE        583 -1
#define  OFF_OTH_SALE_AS_PRIOR_YR         593 -1
#define  OFF_OTH_SALE_AS_PRIOR_LAND       597 -1
#define  OFF_OTH_SALE_AS_PRIOR_IMPR       608 -1
#define  OFF_OTH_SALE_AS_PRIOR_SPCL_EXE   619 -1
#define  OFF_OTH_SALE_AS_CURR_YR          630 -1
#define  OFF_OTH_SALE_AS_CURR_LAND        634 -1
#define  OFF_OTH_SALE_AS_CURR_IMPR        645 -1
#define  OFF_OTH_SALE_AS_CURR_SPCL_EXE    656 -1
#define  OFF_OTH_SALE_DOCNUM              667 -1
#define  OFF_OTH_SALE_DOCTYPE             680 -1
#define  OFF_OTH_SALE_SELLER              684 -1
#define  OFF_OTH_SALE_PRIOR_DOCNUM        758 -1
#define  OFF_OTH_SALE_PRIOR_DOCTYPE       771 -1
#define  OFF_OTH_SALE_PRIOR_SELLER        775 -1

#define  SIZ_OTH_SALE_PARCEL_NUMBER       13
#define  SIZ_OTH_SALE_CHAR_TYPE           1
#define  SIZ_OTH_SALE_CHAR_SEQUENCE_NBR   4
#define  SIZ_OTH_SALE_SITUS_STREET_NUMBER 7
#define  SIZ_OTH_SALE_SITUS_STREET_NAME   20
#define  SIZ_OTH_SALE_DISTRICT            2
#define  SIZ_OTH_SALE_RESPONSIBLE_GROUP   1
#define  SIZ_OTH_SALE_RESPONSIBLE_UNIT    3
#define  SIZ_OTH_SALE_LAND_TYPE           2
#define  SIZ_OTH_SALE_SIZE_RANGE          2
#define  SIZ_OTH_SALE_USE_CODE            4
#define  SIZ_OTH_SALE_USE_CODE_LITERAL    10
#define  SIZ_OTH_SALE_LAND_TYPE_LITERAL   10
#define  SIZ_OTH_SALE_STRUCT_QUALITY_TYPE 2
#define  SIZ_OTH_SALE_USE_LITERAL         10
#define  SIZ_OTH_SALE_GROSS_LSEABLE_AREA  8
#define  SIZ_OTH_SALE_NET_LEASEABLE_AREA  8
#define  SIZ_OTH_SALE_CONSTRUCTION_YEAR   4
#define  SIZ_OTH_SALE_EFFECTIVE_YEAR      4
#define  SIZ_OTH_SALE_NUMBER_OF_UNITS     4
#define  SIZ_OTH_SALE_UNIT_AVERAGE_SQ_FT  8
#define  SIZ_OTH_SALE_NUMBER_OF_STORIES   2
#define  SIZ_OTH_SALE_STORY_HEIGHT        2
#define  SIZ_OTH_SALE_ELEVATORS           2
#define  SIZ_OTH_SALE_FUNC_OBS_LITRAL     6
#define  SIZ_OTH_SALE_LAND_TO_BLDG_RATIO  6
#define  SIZ_OTH_SALE_LAND_LSE_EXPR_DATE  10
#define  SIZ_OTH_SALE_OTHRPOOL_LITERAL    4
#define  SIZ_OTH_SALE_OTHER_SPCL_IMPRS    6
#define  SIZ_OTH_SALE_OTHER_SPCL_IMPR1    5
#define  SIZ_OTH_SALE_OTHER_SPCL_IMPR2    5
#define  SIZ_OTH_SALE_OTHER_SPCL_IMPR3    5
#define  SIZ_OTH_SALE_OTHER_SPCL_IMPR4    5
#define  SIZ_OTH_SALE_NUMBER_ROOMS        4
#define  SIZ_OTH_SALE_NUMBER_DOORS        4
#define  SIZ_OTH_SALE_TENANT_IMPR_PCT     3
#define  SIZ_OTH_SALE_CONSTRUCTION_TYPE   1
#define  SIZ_OTH_SALE_QUALITY             3
#define  SIZ_OTH_SALE_SHAPE               1
#define  SIZ_OTH_SALE_AREA_OF_INFLUENCE   8
#define  SIZ_OTH_SALE_SFR_YRBLT           4
#define  SIZ_OTH_SALE_SFR_YREFF           4
#define  SIZ_OTH_SALE_OTHER_AREA          8
#define  SIZ_OTH_SALE_BATHROOMS           6
#define  SIZ_OTH_SALE_BEDROOMS            2
#define  SIZ_OTH_SALE_FAMILY_ROOM_DEN     1
#define  SIZ_OTH_SALE_GARAGE_SQ_FT        5
#define  SIZ_OTH_SALE_GARAGE_CLASS        3
#define  SIZ_OTH_SALE_GARAGE_STALLS       2
#define  SIZ_OTH_SALE_SFRPOOL_LITERAL     5
#define  SIZ_OTH_SALE_SFR_SPCL_IMPRS      6
#define  SIZ_OTH_SALE_SFR_SPCL_IMPR1      5
#define  SIZ_OTH_SALE_SFR_SPCL_IMPR2      5
#define  SIZ_OTH_SALE_SFR_SPCL_IMPR3      5
#define  SIZ_OTH_SALE_SFR_SPCL_IMPR4      5
#define  SIZ_OTH_SALE_ZONING              8
#define  SIZ_OTH_SALE_LOT_WIDTH           7
#define  SIZ_OTH_SALE_LOT_DEPTH           7
#define  SIZ_OTH_SALE_LOT_SIZE            11
#define  SIZ_OTH_SALE_LOT_SIZE_UNIT       2
#define  SIZ_OTH_SALE_GROSS_ACRES         8
#define  SIZ_OTH_SALE_NET_ACRES           8
#define  SIZ_OTH_SALE_ACCESS_LITERAL      6
#define  SIZ_OTH_SALE_SLOPE_DIR_LITERAL   6
#define  SIZ_OTH_SALE_VIEW_LITERAL        9
#define  SIZ_OTH_SALE_SEWER_LITERAL       6
#define  SIZ_OTH_SALE_WATER_LITERAL       6
#define  SIZ_OTH_SALE_ELECTRIC_LITERAL    6
#define  SIZ_OTH_SALE_GAS_LITERAL         6
#define  SIZ_OTH_SALE_OFFSITES_LITERAL    6
#define  SIZ_OTH_SALE_ENCROACH_LITERAL    6
#define  SIZ_OTH_SALE_NUISANCE1_LITERAL   6
#define  SIZ_OTH_SALE_NUISANCE2_LITERAL   6
#define  SIZ_OTH_SALE_INFLUENCE1_LITERAL  6
#define  SIZ_OTH_SALE_INFLUENCE2_LITERAL  6
#define  SIZ_OTH_SALE_DOCK_RGHTS_LITERAL  6
#define  SIZ_OTH_SALE_LEASE_EXPIR         10
#define  SIZ_OTH_SALE_NUMBER_LOTS         4
#define  SIZ_OTH_SALE_MAP_STATUS_LITERAL  10
#define  SIZ_OTH_SALE_DEV_STATUS_LITERAL  10
#define  SIZ_OTH_SALE_SALE_DATE           10
#define  SIZ_OTH_SALE_SALE_TYPE           1
#define  SIZ_OTH_SALE_SALE_PRICE          11
#define  SIZ_OTH_SALE_BAD_SALE_LITERAL    6
#define  SIZ_OTH_SALE_PRICE_PER_SQ_FT     9
#define  SIZ_OTH_SALE_PRICE_PER_SQ_FT_AST 1
#define  SIZ_OTH_SALE_PRICE_PER_UNIT      11
#define  SIZ_OTH_SALE_PRICE_PER_UNIT_AST  1 
#define  SIZ_OTH_SALE_CONDITION_LITERAL   6 
#define  SIZ_OTH_SALE_SALE_USE_CODE       4
#define  SIZ_OTH_SALE_UNUSED              5
#define  SIZ_OTH_SALE_REMARK_1_LITERAL    8 
#define  SIZ_OTH_SALE_REMARK_2_LITERAL    8 
#define  SIZ_OTH_SALE_REMARK_3_LITERAL    8 
#define  SIZ_OTH_SALE_REMARK_4_LITERAL    8 
#define  SIZ_OTH_SALE_REMARK_5_LITERAL    8 
#define  SIZ_OTH_SALE_PRIOR_SALE_DATE     10
#define  SIZ_OTH_SALE_PRIOR_SALE_PRICE    11  
#define  SIZ_OTH_SALE_PCT_CHG_PER_MONTH   9 
#define  SIZ_OTH_SALE_REQUEST_DATE        10
#define  SIZ_OTH_SALE_AS_PRIOR_YR         4
#define  SIZ_OTH_SALE_AS_PRIOR_LAND       11
#define  SIZ_OTH_SALE_AS_PRIOR_IMPR       11
#define  SIZ_OTH_SALE_AS_PRIOR_SPCL_EXE   11
#define  SIZ_OTH_SALE_AS_CURR_YR          4
#define  SIZ_OTH_SALE_AS_CURR_LAND        11
#define  SIZ_OTH_SALE_AS_CURR_IMPR        11
#define  SIZ_OTH_SALE_AS_CURR_SPECEXM     11

// Owner file layout NPP.OWNER.TRANSFER.TXT
#define  OFF_OWNER_PARCEL_NUMBER          1
#define  OFF_OWNER_TRANSFER_DATE          14
#define  OFF_OWNER_DOC_NUMBER             24
#define  OFF_OWNER_PERCENT                35
#define  OFF_OWNER_NAME                   40

#define  SIZ_OWNER_PARCEL_NUMBER          13
#define  SIZ_OWNER_TRANSFER_DATE          10    // MM/DD/YYYY
#define  SIZ_OWNER_DOC_NUMBER             11
#define  SIZ_OWNER_PERCENT                5
#define  SIZ_OWNER_NAME                   70    // Variable up to 70 chars

typedef struct _tSBD_Owner
{
   char  Apn[SIZ_OWNER_PARCEL_NUMBER];
   char  XferDate[SIZ_OWNER_TRANSFER_DATE];
   char  XferDoc[SIZ_OWNER_DOC_NUMBER];
   char  XferPct[SIZ_OWNER_PERCENT];
   char  Owner[SIZ_OWNER_NAME];
   char  TaxStat;
   char  CrLf[2];
} SBD_OWNER;

typedef struct _tOwnerEx
{
   char  Apn[SIZ_APN_S];
   char  XferDate[SIZ_TRANSFER_DT];
   char  XferDoc[SIZ_TRANSFER_DOC];
   char  Owners[4][SIZ_OWNER_NAME];
   char  Etal;
   char  NameCnt[4];
   char  CrLf[2];
} OWNEREX;

static XLAT_CODE  asSewer[] =
{
   // Value, lookup code, value length
   "0", "X", 1,                // NONE
   "1", "P", 1,                // Public
   "2", "Z", 1,                // Private
   "3", "I", 1,                // Individual
   "9", "Y", 1,                // Check
   "",  "",  0
};

static XLAT_CODE  asWater[] =
{
   // Value, lookup code, value length
   "0", "N", 1,                // NONE
   "1", "P", 1,                // Public
   "2", "W", 1,                // Well
   "3", "A", 1,                // Hauled
   "9", "Y", 1,                // Check
   "",  "",  0
};

static XLAT_CODE  asView[] =
{
   // Value, lookup code, value length
   "0", "N", 1,                // NONE
   "1", "R", 1,                // VALLEY
   "2", "M", 1,                // GOLF COURSE
   "3", "F", 1,                // LAKE
   "4", "Y", 1,                // DESERT
   "5", "G", 1,                // FOREST
   "6", "Y", 1,                // Check
   "7", "Y", 1,                // Check
   "9", "Y", 1,                // Check
   "",  "",  0
};

static XLAT_CODE  asPool[] =
{
   // Value, lookup code, value length
   "0", "N", 1,                // NONE
   "1", "S", 1,                // Spa
   "2", "P", 1,                // Unheated Pool
   "3", "H", 1,                // Heated Pool
   "4", "C", 1,                // Unheated pool with spa
   "5", "C", 1,                // Heated pool with spa
   "9", "U", 1,                // Check
   "",  "",  0
};

static XLAT_CODE  asCool[] =
{
   // Value, lookup code, value length
   "0", "N", 1,                // NONE
   "1", "C", 1,                // Refrigerative, Central
   "2", "A", 1,                // Refrigerative, Non-central
   "3", "E", 1,                // Evaporative, Non-central
   "4", "C", 1,                // Central Plus Evaporative
   "5", "C", 1,                // Evaporative, Central
   "8", "X", 1,                // Other
   "9", "U", 1,                // Check
   "",  "",  0
};

static XLAT_CODE  asHeat[] =
{
   // Value, lookup code, value length
   "0", "L", 1,                // NONE
   "1", "Z", 1,                // Central
   "2", "M", 1,                // Wall/Floor
   "3", "K", 1,                // Solar
   "4", "F", 1,                // Electric baseboard
   "8", "X", 1,                // Other
   "9", "Y", 1,                // Check
   "",  "",  0
};

IDX_TBL5 SBD_DocTitle[] =
{
   "DEED - GRANT",      "1 ", 'N', 10, 2,
   "TRUSTEE'S DEED",    "27", 'Y', 12, 2,
   "DEED   ",           "13", 'N', 7,  2,
   "TAX DEED",          "67", 'Y', 8,  2,
   "DEED - QUITCLAIM",  "4 ", 'Y', 11, 2,
   "DEFAULT NOTICE",    "52", 'Y', 14, 2,
   "DEED IN LIEU OF F", "77", 'Y', 12, 2,
   "DECREE OF DIST",    "80", 'Y', 12, 2,
   "DEED - EXECUTOR'S", "15", 'Y', 12, 2,
   "DEED - WARRANTY",   "28", 'Y', 12, 2,
   "","",0,0
};
IDX_TBL5 SBD_DocType[] =
{
   "0001",              "1 ", 'N', 4,  2,     // Deed - Grant
   "0003",              "4 ", 'Y', 4,  2,     // Deed - Quitclaim
   "0004",              "67", 'N', 4,  2,     // Tax Deed Prchsr Tx Dfltd Prpty
   "0015",              "19", 'Y', 4,  2,     // Lease
   "0018",              "19", 'Y', 4,  2,     // Termination Lease
   "0022",              "19", 'Y', 4,  2,     // Ntc of Non Acceptance of Deed
   "0023",              "19", 'Y', 4,  2,     // Ntc
   "0025",              "19", 'Y', 4,  2,     // Judgment
   "0026",              "19", 'Y', 4,  2,     // Mechanic's Lien
   "0029",              "19", 'Y', 4,  2,     // Agreement
   "0037",              "19", 'Y', 4,  2,     // Judgment/Decree Quieting Title
   "0044",              "19", 'Y', 4,  2,     // Cancel Default Not/Nt of Recsn
   "0045",              "8 ", 'N', 4,  2,     // Agreement of Sale
   "0046",              "80", 'N', 4,  2,     // Order
   "0070",              "19", 'Y', 4,  2,     // Letters of Guardianship
   "0074",              "19", 'Y', 4,  2,     // Certificate
   "0075",              "19", 'Y', 4,  2,     // Certificate of Compliance     
   "0078",              "8 ", 'N', 4,  2,     // Certificate of Sale
   "0081",              "19", 'Y', 4,  2,     // Contract
   "0100",              "74", 'Y', 4,  2,     // MISC - Lot Line Adjustment           
   "0131",              "27", 'N', 4,  2,     // Trustees Deed upon sale
   "0132",              "19", 'Y', 4,  2,     // Request Notice of Delinquency
   "0138",              "6 ", 'Y', 4,  2,     // Affidavit
   "0139",              "6 ", 'Y', 4,  2,     // Affidavit - Death Joint Tenant
   "0140",              "19", 'Y', 4,  2,     // Acceptance
   "0141",              "19", 'Y', 4,  2,     // Addendum/Amend Nt of Assessmnt
   "0151",              "19", 'Y', 4,  2,     // Amend Lease
   "0165",              "19", 'Y', 4,  2,     // Condo Plan
   "0182",              "44", 'Y', 4,  2,     // Lease Agreement
   "0183",              "19", 'Y', 4,  2,     // Letters of Administration
   "0184",              "19", 'Y', 4,  2,     // Letters Testamentary
   "0195",              "19", 'Y', 4,  2,     // Ntc of Recission
   "0204",              "19", 'Y', 4,  2,     // Recission of Trustee's Deed
   "0221",              "19", 'Y', 4,  2,     // Trust Agreement
   "0225",              "19", 'Y', 4,  2,     // Writ of Sale
   "0231",              "6 ", 'N', 4,  2,     // Affidavits Death
   "0235",              "19", 'Y', 4,  2,     // Agm & Accept (VA)
   "0240",              "19", 'Y', 4,  2,     // Ntc of Lien
   "0244",              "19", 'Y', 4,  2,     // Ntc of Cancellation
   "0247",              "19", 'Y', 4,  2,     // Letters of Conservatorship
   "0264",              "55", 'Y', 4,  2,     // Patent
   "0301",              "19", 'Y', 4,  2,     // Cancellation/Recission
   "0302",              "29", 'Y', 4,  2,     // Assignment of Leases
   "0400",              "13", 'N', 4,  2,     // DEED
   "0509",              "9 ", 'Y', 4,  2,     // Correction
   "0513",              "19", 'Y', 4,  2,     // Termination
   "0522",              "19", 'Y', 4,  2,     // Recission/Cancel of Tax Deed
   "0523",              "19", 'Y', 4,  2,     // Release of Lien
   "0526",              "19", 'Y', 4,  2,     // Sublease
   "0544",              "19", 'Y', 4,  2,     // Release Tax Lien - State
   "0555",              "19", 'Y', 4,  2,     // Memorandum Agreement of Sale
   "0569",              "19", 'Y', 4,  2,     // Assignment of Sublease
   "0580",              "19", 'Y', 4,  2,     // Certification of Trust
   "0581",              "19", 'Y', 4,  2,     // Rel Conveyance Easement Determ
   "0582",              "19", 'Y', 4,  2,     // Conveyance Easement Determnble
   "0590",              "6 ", 'Y', 4,  2,     // Affidavit Death of Trustee
   "0600",              "40", 'Y', 4,  2,     // Easement
   "0611",              "19", 'Y', 4,  2,     // Rescission
   "0617",              "6 ", 'N', 4,  2,     // Affidavit Death of Life Estate
   "0630",              "19", 'Y', 4,  2,     // Rec Tx Dd prchsr tx defltd prp
   "0632",              "19", 'Y', 4,  2,     // Order of Criminal Restitution
   "0634",              "6 ", 'Y', 4,  2,     // Affidavit Successor Trustee
   "0647",              "19", 'Y', 4,  2,     // Treasur Tx Dd Prchsr Tx Dfltd
   "0665",              "6 ", 'N', 4,  2,     // Affidavit of Death
   "0669",              "19", 'Y', 4,  2,     // Aff Smll Value $20,000 or Less
   "0770",              "19", 'Y', 4,  2,     // Cert Sale Judicial Foreclosure
   "0771",              "19", 'Y', 4,  2,     // Cert Sale NonJudicial Fcl
   "0773",              "19", 'Y', 4,  2,     // Cert Sale Sheriff or Marshall
   "0782",              "34", 'N', 4,  2,     // Deed -Commissioners 
   "0783",              "13", 'N', 4,  2,     // Deed -Community Property 
   "0788",              "19", 'Y', 4,  2,     // Land Contract
   "0810",              "19", 'Y', 4,  2,     // Rescission of Deed
   "0811",              "19", 'Y', 4,  2,     // Revocation of Deed
   "0812",              "28", 'N', 4,  2,     // Deed -Warranty
   "0813",              "78", 'N', 4,  2,     // Deed in Lieu of Foreclosure
   "0817",              "15", 'N', 4,  2,     // Deed -Executor'S 
   "0899",              "6 ", 'Y', 4,  2,     // Aff Change/Successor Trustee
   "0905",              "6 ", 'Y', 4,  2,     // Aff of Right Surviving Spouse 
   "0909",              "6 ", 'Y', 4,  2,     // Affidavit Small Value $50,000 of Less
   "0910",              "6 ", 'Y', 4,  2,     // Affidavit Estate not more than $150,000
   "0914",              "19", 'Y', 4,  2,     // Acceptance of Assignment
   "0917",              "40", 'Y', 4,  2,     // Grant of Easement
   "","",0,0
};

IDX_SALE SBD_DocCode1[] =
{// DOCTITLE, DOCTYPE, ISSALE, ISTRANSFER, CODELEN, TYPELEN
   "DEED OF TRUST",                 "65",'N','N',10,2,
   "DEED - GRANT",                  "1 ",'Y','Y',10,2,
   "DEED - QUITCLAIM",              "4 ",'N','Y',8 ,2,
   "DEED   ",                       "13",'Y','Y',7 ,2,
   "NTC OF TRUSTEE'S SALE",         "52",'N','N',20,2,
   "TAX LIEN - COUNTY",             "46",'N','Y',8 ,2,
   "MECHANIC'S LIEN",               "46",'N','Y',12,2,
   "LIEN - COUNTY",                 "46",'N','Y',9 ,2,
   "LIEN",                          "46",'N','Y',4 ,2,
   "TRUSTEE'S DEED",                "27",'N','Y',14,2,
   "SUBSTITUTION OF TRUSTEE",       "74",'N','N',23,2,
   "NTC OF LIEN",                   "46",'N','N',11,2,
   "RELEASE OF LIEN",               "46",'N','N',15,2,
   "ASSIGNMENT OF DEED OF TRUST",   "7 ",'N','N',25,2,
   "DEFAULT NOTICE/NT DEFAULT",     "52",'N','N',20,2,
   "NTC PWR TO SELL TX DFT PROPRTY","74",'N','N',12,2,
   "NTC OF DEFLT AND FORCLOSR SALE","52",'N','N',12,2,
   "AFFIDAVIT - DEATH JOINT TENANT","6 ",'N','Y',9 ,2,
   "AFFIDAVIT DEATH OF TRUSTEE",    "6 ",'N','Y',9 ,2,
   "EASEMENT",                      "40",'N','N',8 ,2,
   "HOMESTEAD",                     "74",'N','N',9 ,2,
   "RECONVEYANCE",                  "74",'N','N',12,2,
   "","",0,0,0
};

IDX_TBL5 SBD_DocCode2[] =
{// DOCCODE, DOCTYPE, NONSALE, CODELEN, TYPELEN
   "AGREEMENT",                     "74",'Y',9 ,2,
   "ASSIGNMENT OF RENTS",           "74",'Y',19,2,
   "ASSIGNMENT",                    "74",'Y',9 ,2,
   "CANCEL DEFAULT NOT/NT OF RECSN","74",'Y',18,2,
   "CERT OF DELINQUENT WATER CHGS", "74",'Y',24,2,
   "CERT SALE JUDICIAL FORECLOSURE","74",'Y',9 ,2,
   "CERT SALE NONJUDICIAL FCL",     "74",'Y',9 ,2,
   "CERT SALE SHERIFF OR MARSHALL", "74",'Y',9 ,2,
   "CERT SUBORD FED TAX LIEN",      "74",'Y',9 ,2,
   "CERTIFICATE OF COMPLETION",     "74",'Y',14,2,
   "CERTIFICATE OF COMPLIANCE",     "74",'Y',14,2,
   "CERTIFICATE OF CORRECTION",     "74",'Y',14,2,
   "CERTIFICATE OF DEATH - ABROAD", "74",'Y',14,2,
   "CERTIFICATE OF DEDICATION",     "74",'Y',14,2,
   "CERTIFICATE OF DISCHARGE",      "74",'Y',14,2,
   "CERTIFICATE OF DISCHARGE MTG",  "74",'Y',14,2,
   "CERTIFICATE OF FILING",         "74",'Y',14,2,
   "CERTIFICATE OF LIEN",           "74",'Y',14,2,
   "CERTIFICATE OF REDEMPTION-FCL", "74",'Y',14,2,
   "CERTIFICATE OF SALE",           "74",'Y',14,2,
   "DECREE OF DISTRIBUTION",        "79",'Y',14,2,
   "GRANT OF EASEMENT",             "74",'Y',12,2,
   "GRANT OF LIEN",                 "74",'Y',12,2,
   "JUDGMENT",                      "74",'Y',8 ,2,
   "LEASE AGREEMENT",               "44",'Y',9 ,2,
   "LETTERS OF ADMINISTRATION",     "74",'Y',9 ,2,
   "LETTERS OF CONSERVATORSHIP",    "74",'Y',9 ,2,
   "LETTERS OF GUARDIANSHIP",       "74",'Y',9 ,2,
   "MODIFICATION OF DEED OF TRUST", "74",'Y',26,2,
   "MODIFICATION AGREEMENT",        "74",'Y',22,2,
   "NOTICE OF ABATE",               "74",'Y',9 ,2,
   "NOTICE OF ABATEMENT LIEN",      "74",'Y',9 ,2,
   "NOTICE OF ASSESSMENT LIEN",     "74",'Y',9 ,2,
   "NOTICE OF CESSATION",           "74",'Y',9 ,2,
   "NOTICE OF INTENT TO HOLD",      "74",'Y',9 ,2,
   "NOTICE OF LEVY/SALE",           "74",'Y',9 ,2,
   "NOTICE OF NON RENEWAL",         "74",'Y',9 ,2,
   "NOTICE OF PENDING ACTION",      "74",'Y',9 ,2,
   "NOTICE OF SUPPORT JUDGMENT",    "74",'Y',9 ,2,
   "NTC INT TO TERM DELINQUENT",    "74",'Y',19,2,
   "NTC OF ADM PROCEED/NTC OF ABAT","74",'Y',18,2,
   "NTC OF ASSESSMENT & LIEN",      "74",'Y',9 ,2,
   "NTC OF ASSESSMENT-DISTRICT",    "74",'Y',9 ,2,
   "NTC OF ASSESSMNT-HOME OWN ASSC","74",'Y',9 ,2,
   "NTC OF BULK TRANSFER/SALE",     "74",'Y',9 ,2,
   "NTC OF CANCEL SPECIAL TAX LIEN","74",'Y',9 ,2,
   "NTC OF CANCELLATION",           "74",'Y',9 ,2,
   "NTC OF CESS OF SPEC TAX LIEN",  "74",'Y',9 ,2,
   "NTC OF EASEMENT",               "74",'Y',9 ,2,
   "NTC OF NON ACCEPTANCE OF DEED", "74",'Y',9 ,2,
   "NTC OF RECISSION",              "74",'Y',9 ,2,
   "NTC OF REL OF ASSESSMENT LIEN", "74",'Y',9 ,2,
   "NTC OF REN LAND CONSERV CONTR", "74",'Y',9 ,2,
   "NTC OF RESC. FORECLOSURE DEED", "74",'Y',9 ,2,
   "NTC OF REVOCATION OF CONSENT",  "74",'Y',9 ,2,
   "NTC OF SPECIAL TAX LIEN",       "74",'Y',9 ,2,
   "NTC OF TAX LIEN - IRS",         "74",'Y',9 ,2,
   "NTC OF UTILITY LIEN",           "74",'Y',9 ,2,
   "PARTIAL RELEASE",               "74",'Y',15,2,
   "PRENUPTIAL AGREEMENT",          "74",'Y',9 ,2,
   "POSTNUPTIAL AGREEMENT",         "74",'Y',9 ,2,
   "REC NTC OF PWR TO SELL TX DFT", "74",'Y',20,2,
   "REC OF DEED OF RECONVEYANCE",   "74",'Y',20,2,
   "REC OF DEED REF OF REV OF DEED","74",'Y',20,2,
   "REC TX DD PRCHSR TX DEFLTD PRP","74",'Y',16,2,
   "RECISSION OF TRUSTEE'S DEED",   "74",'Y',18,2,
   "RECISSION/CANCEL OF TAX DEED",  "74",'Y',18,2,
   "REINSTATEMENT OF DEED OF TRUST","74",'Y',20,2,
   "REL OF LIEN NUISANCE ABATE",    "74",'Y',11,2,
   "REL OF NTC OF SPEC TAX LIEN",   "74",'Y',11,2,
   "REL OF NTC PENDENCY ADMN PROC", "74",'Y',19,2,
   "RELEASE OF MECHANICS LIEN",     "74",'Y',15,2,
   "RELEASE OF MORTGAGE",           "50",'Y',15,2,
   "RELEASE TAX LIEN - CITY",       "74",'Y',16,2,
   "RELEASE TAX LIEN - COUNTY",     "74",'Y',16,2,
   "RELEASE TAX LIEN - FEDERAL",    "74",'Y',16,2,
   "RELEASE TAX LIEN - STATE",      "74",'Y',16,2,
   "REQ FOR NTC OF TRUSTEE'S DEED", "74",'Y',22,2,
   "REQUEST FOR NOTICE OF DEFAULT", "74",'Y',25,2,
   "REQUEST NOTICE OF DELINQUENCY", "74",'Y',21,2,
   "SUBORDINATION AGREEMENT",       "74",'Y',18,2,
   "TAX DEED PRCHSR TX DFLTD PRPTY","74",'Y',21,2,
   "WITHDRAWAL FEDERAL TAX LIEN",   "74",'Y',24,2,
   "WITHDRAWAL OF LIEN",            "74",'Y',18,2,
   "WITHDRAWAL OF MECHANIC LIEN",   "74",'Y',24,2,
   "WITHRAWAL OF ABSTRACT JUDGMNT", "74",'Y',24,2,
   "","",0,0,0
};

IDX_TBL5 SBD_DocCode[] =
{// DOCCODE, DOCTYPE, NONSALE, CODELEN, TYPELEN
   "DEED OF TRUST",                 "65",'Y',10,2,
   "DEED - GRANT",                  "1 ",'N',10,2,
   "DEED - QUITCLAIM",              "4 ",'Y',8 ,2,
   "DEED   ",                       "13",'N',4 ,2,
   "NTC OF TRUSTEE'S SALE",         "52",'Y',20,2,
   "TAX LIEN - COUNTY",             "46",'Y',8 ,2,
   "MECHANIC'S LIEN",               "46",'Y',12,2,
   "LIEN - COUNTY",                 "46",'Y',9 ,2,
   "LIEN",                          "46",'Y',4 ,2,
   "TRUSTEE'S DEED",                "27",'Y',14,2,
   "SUBSTITUTION OF TRUSTEE",       "74",'Y',23,2,
   "NTC OF LIEN",                   "46",'Y',11,2,
   "RELEASE OF LIEN",               "46",'Y',15,2,
   "ASSIGNMENT OF DEED OF TRUST",   "7 ",'Y',25,2,
   "DEFAULT NOTICE/NT DEFAULT",     "52",'Y',20,2,
   "NTC PWR TO SELL TX DFT PROPRTY","74",'Y',12,2,
   "NTC OF DEFLT AND FORCLOSR SALE","52",'Y',12,2,
   "AFFIDAVIT - DEATH JOINT TENANT","6 ",'Y',9 ,2,
   "AFFIDAVIT DEATH OF TRUSTEE",    "6 ",'Y',9 ,2,
   "EASEMENT",                      "40",'Y',8 ,2,
   "HOMESTEAD",                     "74",'Y',9 ,2,
   "RECONVEYANCE",                  "74",'Y',12,2,
   "AGREEMENT",                     "74",'Y',9 ,2,
   "ASSIGNMENT OF RENTS",           "74",'Y',19,2,
   "ASSIGNMENT",                    "74",'Y',9 ,2,
   "CANCEL DEFAULT NOT/NT OF RECSN","74",'Y',18,2,
   "CERT OF DELINQUENT WATER CHGS", "74",'Y',24,2,
   "CERT SALE JUDICIAL FORECLOSURE","74",'Y',9 ,2,
   "CERT SALE NONJUDICIAL FCL",     "74",'Y',9 ,2,
   "CERT SALE SHERIFF OR MARSHALL", "74",'Y',9 ,2,
   "CERT SUBORD FED TAX LIEN",      "74",'Y',9 ,2,
   "CERTIFICATE OF COMPLETION",     "74",'Y',14,2,
   "CERTIFICATE OF COMPLIANCE",     "74",'Y',14,2,
   "CERTIFICATE OF CORRECTION",     "74",'Y',14,2,
   "CERTIFICATE OF DEATH - ABROAD", "74",'Y',14,2,
   "CERTIFICATE OF DEDICATION",     "74",'Y',14,2,
   "CERTIFICATE OF DISCHARGE",      "74",'Y',14,2,
   "CERTIFICATE OF DISCHARGE MTG",  "74",'Y',14,2,
   "CERTIFICATE OF FILING",         "74",'Y',14,2,
   "CERTIFICATE OF LIEN",           "74",'Y',14,2,
   "CERTIFICATE OF REDEMPTION-FCL", "74",'Y',14,2,
   "CERTIFICATE OF SALE",           "74",'Y',14,2,
   "DECREE OF DISTRIBUTION",        "79",'Y',14,2,
   "GRANT OF EASEMENT",             "74",'Y',12,2,
   "GRANT OF LIEN",                 "74",'Y',12,2,
   "JUDGMENT",                      "74",'Y',8 ,2,
   "LEASE AGREEMENT",               "44",'Y',9 ,2,
   "LETTERS OF ADMINISTRATION",     "74",'Y',9 ,2,
   "LETTERS OF CONSERVATORSHIP",    "74",'Y',9 ,2,
   "LETTERS OF GUARDIANSHIP",       "74",'Y',9 ,2,
   "MODIFICATION OF DEED OF TRUST", "74",'Y',26,2,
   "MODIFICATION AGREEMENT",        "74",'Y',22,2,
   "NOTICE OF ABATE",               "74",'Y',9 ,2,
   "NOTICE OF ABATEMENT LIEN",      "74",'Y',9 ,2,
   "NOTICE OF ASSESSMENT LIEN",     "74",'Y',9 ,2,
   "NOTICE OF CESSATION",           "74",'Y',9 ,2,
   "NOTICE OF INTENT TO HOLD",      "74",'Y',9 ,2,
   "NOTICE OF LEVY/SALE",           "74",'Y',9 ,2,
   "NOTICE OF NON RENEWAL",         "74",'Y',9 ,2,
   "NOTICE OF PENDING ACTION",      "74",'Y',9 ,2,
   "NOTICE OF SUPPORT JUDGMENT",    "74",'Y',9 ,2,
   "NTC INT TO TERM DELINQUENT",    "74",'Y',19,2,
   "NTC OF ADM PROCEED/NTC OF ABAT","74",'Y',18,2,
   "NTC OF ASSESSMENT & LIEN",      "74",'Y',9 ,2,
   "NTC OF ASSESSMENT-DISTRICT",    "74",'Y',9 ,2,
   "NTC OF ASSESSMNT-HOME OWN ASSC","74",'Y',9 ,2,
   "NTC OF BULK TRANSFER/SALE",     "74",'Y',9 ,2,
   "NTC OF CANCEL SPECIAL TAX LIEN","74",'Y',9 ,2,
   "NTC OF CANCELLATION",           "74",'Y',9 ,2,
   "NTC OF CESS OF SPEC TAX LIEN",  "74",'Y',9 ,2,
   "NTC OF EASEMENT",               "74",'Y',9 ,2,
   "NTC OF NON ACCEPTANCE OF DEED", "74",'Y',9 ,2,
   "NTC OF RECISSION",              "74",'Y',9 ,2,
   "NTC OF REL OF ASSESSMENT LIEN", "74",'Y',9 ,2,
   "NTC OF REN LAND CONSERV CONTR", "74",'Y',9 ,2,
   "NTC OF RESC. FORECLOSURE DEED", "74",'Y',9 ,2,
   "NTC OF REVOCATION OF CONSENT",  "74",'Y',9 ,2,
   "NTC OF SPECIAL TAX LIEN",       "74",'Y',9 ,2,
   "NTC OF TAX LIEN - IRS",         "74",'Y',9 ,2,
   "NTC OF UTILITY LIEN",           "74",'Y',9 ,2,
   "PARTIAL RELEASE",               "74",'Y',15,2,
   "PRENUPTIAL AGREEMENT",          "74",'Y',9 ,2,
   "POSTNUPTIAL AGREEMENT",         "74",'Y',9 ,2,
   "REC NTC OF PWR TO SELL TX DFT", "74",'Y',20,2,
   "REC OF DEED OF RECONVEYANCE",   "74",'Y',20,2,
   "REC OF DEED REF OF REV OF DEED","74",'Y',20,2,
   "REC TX DD PRCHSR TX DEFLTD PRP","74",'Y',16,2,
   "RECISSION OF TRUSTEE'S DEED",   "74",'Y',18,2,
   "RECISSION/CANCEL OF TAX DEED",  "74",'Y',18,2,
   "REINSTATEMENT OF DEED OF TRUST","74",'Y',20,2,
   "REL OF LIEN NUISANCE ABATE",    "74",'Y',11,2,
   "REL OF NTC OF SPEC TAX LIEN",   "74",'Y',11,2,
   "REL OF NTC PENDENCY ADMN PROC", "74",'Y',19,2,
   "RELEASE OF MECHANICS LIEN",     "74",'Y',15,2,
   "RELEASE OF MORTGAGE",           "50",'Y',15,2,
   "RELEASE TAX LIEN - CITY",       "74",'Y',16,2,
   "RELEASE TAX LIEN - COUNTY",     "74",'Y',16,2,
   "RELEASE TAX LIEN - FEDERAL",    "74",'Y',16,2,
   "RELEASE TAX LIEN - STATE",      "74",'Y',16,2,
   "REQ FOR NTC OF TRUSTEE'S DEED", "74",'Y',22,2,
   "REQUEST FOR NOTICE OF DEFAULT", "74",'Y',25,2,
   "REQUEST NOTICE OF DELINQUENCY", "74",'Y',21,2,
   "SUBORDINATION AGREEMENT",       "74",'Y',18,2,
   "TAX DEED PRCHSR TX DFLTD PRPTY","74",'Y',21,2,
   "WITHDRAWAL FEDERAL TAX LIEN",   "74",'Y',24,2,
   "WITHDRAWAL OF LIEN",            "74",'Y',18,2,
   "WITHDRAWAL OF MECHANIC LIEN",   "74",'Y',24,2,
   "WITHRAWAL OF ABSTRACT JUDGMNT", "74",'Y',24,2,
   "","",0,0,0
};

#define  SALE_TYPE_SFR        0x34
#define  SALE_TYPE_MP         0x35
#define  SALE_TYPE_MH         0x36
#define  SALE_TYPE_LAND       0x37
#define  SALE_TYPE_DOCK       0x38
#define  SALE_TYPE_OTH        0x39

// Tax file TR340PC.txt - no longer available 11/10/2019
#define  TSIZ_APN             13
#define  TSIZ_ROLLYEAR        2
#define  TSIZ_BILLNUM         7
#define  TSIZ_PAIDSTATUS      2
#define  TSIZ_CORTACNUM       13
#define  TSIZ_TRA             7
#define  TSIZ_INSTAMT         13
#define  TSIZ_ROLLTYPE        2
#define  TSIZ_CNTYCODE        2

typedef  struct _tTR340PC
{
   char  filler1[5];
   char  Apn[TSIZ_APN];                // 6
   char  RollYear[TSIZ_ROLLYEAR];      // 19
   char  BillNum[TSIZ_BILLNUM];        // 21
   char  PaidStatus[TSIZ_PAIDSTATUS];  // 28 - 00=unpaid, 01=Inst1 paid, 02=both paid
   char  filler2;                      // 30
   char  CortacNum[TSIZ_CORTACNUM];    // 31
   char  TRA[TSIZ_TRA];                // 44
   char  TaxAmt1[TSIZ_INSTAMT];        // 51 - V99
   char  TaxAmt2[TSIZ_INSTAMT];        // 64
   char  RollType[TSIZ_ROLLTYPE];      // 77
   char  CntyCode[TSIZ_CNTYCODE];      // 79
   char  CrLf[2];
} SBD_TR340;

// Tax file PI344
#define  TSIZ_TAXCODE         8
#define  TSIZ_ASMNT_SEQ       2
#define  TSIZ_ASMNT_AMT       11
#define  TSIZ_COST            5
#define  TSIZ_I_ROLLYEAR      4
#define  TSIZ_TAX_RATE_AREA   9

typedef  struct _tPI344
{
   char  TaxCode[TSIZ_TAXCODE];        // 1
   char  Apn[TSIZ_APN];                // 9
   char  AsmntSeq[TSIZ_ASMNT_SEQ];     // 22
   char  AsmntAmt[TSIZ_ASMNT_AMT];     // 24 - V99
   char  AsmntAmtSign;                 // 35
   char  ProcessCost[TSIZ_COST];       // 36 - V99
   char  ProcessCostSign;              // 41
   char  ActiveStatus;                 // 42
   char  TaxStatus;                    // 43
   char  RollYear[TSIZ_I_ROLLYEAR];    // 44
   char  TRA[TSIZ_TRA];                // 48
   char  UtilityFlg;                   // 57
   char  UtilityApn[TSIZ_APN];         // 58
   char  CrLf[2];
} SBD_PI344;

// Tax Items file Item_Extr.txt
#define  TI_APN               0
#define  TI_TAXCODE           1
#define  TI_TAXAGENCY         2
#define  TI_BILLSEQ           3
#define  TI_TAXAMT            4
#define  TI_COLS              5


typedef  struct _tROLPARC
{
   unsigned char  Header[68];
   unsigned char  APN[TSIZ_APN];
   unsigned char  ParcelStatus;             
   unsigned char  TaxStatus;             
   unsigned char  TaxDelqInd;
   unsigned char  DeededInd;
   unsigned char  BadCheckInd;
   unsigned char  BondYear[2];             
   unsigned char  Filler[10];   
} ROLPARC;

typedef  struct _tROLVALU
{
   unsigned char  Header[8];
   unsigned char  APN[TSIZ_APN];
   unsigned char  filler[47];
   unsigned char  ValueType;     // 'A'lloc, 'E'xtend, 'O'ld, 'N'ew
   unsigned char  LandVal[6];
   unsigned char  PersPropVal[6];
   unsigned char  ImprVal[6];
   unsigned char  NetVal[6];
   unsigned char  PP_Pen[6];
   unsigned char  Impr_Pen[6];
   unsigned char  Filler[13];
} ROLVALU;

typedef  struct _tROLEXEM
{
   unsigned char  Header[8];
   unsigned char  APN[TSIZ_APN];
   unsigned char  filler[47];
   unsigned char  ExeType[2];    // "00"=HO, "10"-"19"=VET
   unsigned char  ExeAmt[6];
   unsigned char  Filler[8];
} ROLEXEM;

typedef  struct _tINSTREC
{  // 58-byte
   char  InstNbr[2];                   // 198,256 
   char  InstAmt[10];                  // 200 - installment amt
   char  IntrAmt[10];                  // 210 - interest amt
   char  DisAmt[10];                   // 220 - discount amt
   char  PenAmt[10];                   // 230 - penalty amt
   char  DueDate[8];                   // 240 
   char  PenaltyInd;                   // W=waive, P=paid
   char  DiscountCode;                 // Y=discount taken
   char  ApporYear[4];                 // 250
   char  ApporInd;                     // 254
   char  PenChrgInd;                   // A=add to charge, C=sub from charge
} INSTREC;

typedef  struct _tROLBILL
{
   unsigned char  Header[68];
   unsigned char  RollYear[2];
   unsigned char  BillSeq[7];
   unsigned char  BillDate[6];
   unsigned char  Supp_Nbr[2];
   unsigned char  Supp_Type;
   unsigned char  TaxType[2];
   unsigned char  Appor_Status;
   unsigned char  RateYear[2];
   unsigned char  AsmntYear[2];
   unsigned char  Eligibility_Ind;
   unsigned char  Billed_Ind;
   unsigned char  TRA[5];
   unsigned char  TaxRate[4];
   unsigned char  TaxAmt[6];
   unsigned char  SpclAsmntAmt[6];
   unsigned char  MonthsProRated[2];
   unsigned char  CPUC_Owner_Seq[2];
   unsigned char  DBA_Owner_Seq[2];
   unsigned char  CURR_Owner_Seq[2];
   unsigned char  DelqProcCost[3];
   unsigned char  SpclPayPlan;
   unsigned char  CortacPresent;
   unsigned char  Billed_Owner_Seq[2];
   unsigned char  Co_Owner_Seq[2];
   unsigned char  NumInst[2];
   unsigned char  PaymentStatus[2];
   unsigned char  TaxRollStatusType;
   unsigned char  TaxRollStatusRoll;
   unsigned char  ProblemCode[2];
   unsigned char  Tax_Accntg_Ind;
   unsigned char  BillCorrInd;
   unsigned char  BillFromYear[2];
   unsigned char  BillFromSeq[7];
   unsigned char  BillToYear[2];
   unsigned char  BillToSeq[7];
   unsigned char  RollExtDate[6];
   unsigned char  AdjAmt[6];
   unsigned char  RefundInd;
   unsigned char  RefundDate[6];
   unsigned char  RefundAmt[6];
   unsigned char  TaxDeferInd;
   unsigned char  BillDelqDate[6];
   unsigned char  Prev_Eligibility_Ind;
   unsigned char  BillDelqInd;
   unsigned char  DaysProRated[3];
   unsigned char  Filler[10];
} ROLBILL;

typedef  struct _tROLDELQ
{
   unsigned char  Header[68];          
   unsigned char  DelqType;            // S=secured, U=unsecured
   unsigned char  RollYear[2];         
   unsigned char  BillSeq[7];          
   unsigned char  RedempStatus;
   unsigned char  DefaultDate[6];
   unsigned char  OrgApn[13];
   unsigned char  PayPlanCode[2];
   unsigned char  PayPlanSeq[2];
   unsigned char  NumOfInst[2];
   unsigned char  PaymentStatus[2];
   unsigned char  NextPayDueDate[6];
   unsigned char  RedempAmt[6];
   unsigned char  RedempMonth[2];
   unsigned char  TotalFeeIntrAmt[6];
   unsigned char  PayPlanPostFlag;
   unsigned char  Filler[1];
} ROLDELQ;

typedef  struct _tDELQREC
{  // 76-byte
   char  DelqType;                     // 1 - S=sec, U=unsec, R=redeemed
   char  BillNum[11];                  // 2
   char  RedempStatus;                 // 13
   char  DefaultDate[8];               // 14
   char  OrgApn[14];                   // 22
   char  PayPlanCode[2];               // 36
   char  PayPlanSeq[2];                // 38
   char  NumOfInst[2];                 // 40
   char  PaymentStatus[2];             // 42
   char  NextPayDueDate[8];            // 44
   char  RedempAmt[10];                // 52
   char  RedempMonth[2];               // 62
   char  TotalFeeIntrAmt[10];          // 64
   char  PayPlanPostFlag;              // 74
   char  CrLf[2];
} DELQREC;

typedef  struct _tROLINST
{
   unsigned char  Header[68];
   unsigned char  InstNbr[2];
   unsigned char  InstAmt[6];
   unsigned char  IntrAmt[6];
   unsigned char  DisAmt[6];
   unsigned char  PenAmt[6];
   unsigned char  DueDate[6];
   unsigned char  PenaltyInd;          // W=waive, P=paid
   unsigned char  DiscountCode;        // Y=discount taken
   unsigned char  ApporYear[2];
   unsigned char  ApporInd;
   unsigned char  PenChrgInd;          // A=add to charge, C=sub from charge
   unsigned char  Filler[4];
} ROLINST;

#define  TBSIZ_APN                  13
#define  TBSIZ_TRA                  6
#define  TBSIZ_YEAR                 4
#define  TBSIZ_YYYYMMDD             8  // YYYYMMDD
#define  TBSIZ_YYMMDD               6  // YYMMDD
#define  TBSIZ_BILLNUM              11
#define  TBSIZ_BILLSEQ              7
#define  TBSIZ_AMT                  10
#define  TBSIZ_RATE                 10
#define  TBSIZ_NUMINST              2
#define  TBSIZ_NAMESEQ              2
#define  TBSIZ_DELQCOST             6

typedef  struct _tPARCREC
{  // 23-byte
   char  APN[TBSIZ_APN];               // 1
   char  filler;
   char  ParcelStatus;                 // 15
   char  TaxStatus;                    // 16
   char  TaxDelqInd;                   // 17
   char  DeededInd;                    // 18
   char  BadCheckInd;                  // 19
   char  BondYear[TBSIZ_YEAR];         // 20
} PARCREC;

//typedef  struct _tBillNum1
//{
//   char  RollYear[4];                  // 20
//   char  BillSeq[7];                   // 24
//} BILLNUM1;
//typedef  struct _tBillNum2
//{
//   char  RollCC[2];                    // 20
//   char  BillNum[9];                   // 22
//} BILLNUM2;
typedef  struct _tBILLREC
{
   char  BillNum[TBSIZ_BILLNUM];          // 24
   char  BillDate[TBSIZ_YYYYMMDD];        // 35
   char  Supp_Nbr[2];                     // 43
   char  Supp_Type;                       // 45 
         // 4=allocated value, 1=property xfer, 2=complete const, 6=impr removed
   char  RollType;                        // 46
         // Roll-Type:   : I=interim supp val, D=deeded val, R=reg supp val, V=Adv uns val, A=annual val
   char  Legal_Status;                    // 47
         // Legal-Status : W=uns air not lien, S=sec lien, U=unsec not lien,T=util lien,Z=sec not lien
   char  Appor_Status;                    // 48
         // C=current status, D=delq status, P=prior status
   char  RateYear[TBSIZ_YEAR];            // 49
   char  AsmntYear[TBSIZ_YEAR];           // 53
   char  Eligibility_Ind;                 // 57
         // ELIG-EXTENDED VALUE           'A'
         // UNELIG-PENDING VALUE          'W'.
         // UNELIG-CANCELLED VALUE        'N'.
         // UNELIG-ALLOCATED VALUE        'U'.
         // UNELIG-INSUFF VALUE           'P'.
         // UNELIG-DIV-SEPT VALUE         'O'.
         // UNELIG-CORR-REF VALUE         'Q'.
         // UNELIG-ESCAPE-TOTAL VALUE     'L'.
         // UNELIG-REFUND-DUE VALUE       'R'.
         // UNELIG-CAN-ASSESS VALUE       'Z'.
         // UNELIG-ESCAPE-BEFEXT VALUE    'V'.
         // UNELIG-CORR-EXTEND VALUE      'X'.
         // UNELIG-CORRECTED VALUE        'M'. 
         // UNELIG-DEEDED VALUE           'J'.
         // UNELIG-CORR-TO-REFUND VALUE   'K'.
         // BILL-IS-ELIG VALUE            'A' 'J'.
         // COULD-BE-CHARGE VALUE         'P' 'K'.
         // IS-A-CHARGE VALUE             'A' 'J' 'L' 'Q'.
         // WAS-A-CHARGE VALUE            'M' 'N' 'O'.
   char  Billed_Ind;                      // 58 - Y
   char  TRA[TBSIZ_TRA];                  // 59
   char  TaxRate[TBSIZ_AMT];              // 65
   char  TaxAmt[TBSIZ_AMT];               // 75
   char  SpclAsmntAmt[TBSIZ_AMT];         // 85
   char  MonthsProRated[2];               // 95
   char  CPUC_Owner_Seq[TBSIZ_NAMESEQ];   // 97
   char  DBA_Owner_Seq[TBSIZ_NAMESEQ];    // 99
   char  CURR_Owner_Seq[TBSIZ_NAMESEQ];   // 101
   char  DelqProcCost[TBSIZ_DELQCOST];    // 103
   char  SpclPayPlan;                     // 109
   char  CortacPresent;                   // 110
   char  Billed_Owner_Seq[TBSIZ_NAMESEQ]; // 111
   char  Co_Owner_Seq[TBSIZ_NAMESEQ];     // 113
   char  NumInst[TBSIZ_NUMINST];          // 115
   char  PaymentStatus[2];                // 117   01=first paid, 02=second paid
   char  TaxRollStatusType;               // 119   C=current, D=delq, P=prior bill
   char  TaxRollStatusRoll;               // 120
   char  ProblemCode[2];                  // 121
   char  Tax_Accntg_Ind;
   char  BillCorrInd;                     // 124
         // CORRECTION-BILLING            'C'.
         // CORRECTION-TAX-CHARGE         'C' 'D' 'E' 'F' 'G'.
         // DIV-SEP-PENDING               'U'.
         // SEPERATED-BILLING             'G'.
         // DIVIDED-INTEREST              'D'.
         // CORRECTION-NO                 ' '.
         // CORRECTION-PENDING            'P'.
         // ESCAPED-LEVY                  'E'.
         // CORRECTION-IS-PENDING         'P' 'U'.
         // INC-DEC-TOTALLY-PAID          'F'.
         // CORRECTION-ORIG-CHG           ' ' 'P' 'U' 'V' 'X'.
   char  BillFromYear[TBSIZ_YEAR];        // 125
   char  BillFromSeq[TBSIZ_BILLSEQ];
   char  BillToYear[TBSIZ_YEAR];          // 136
   char  BillToSeq[TBSIZ_BILLSEQ];
   char  RollExtDate[TBSIZ_YYYYMMDD];     // 147
   char  AdjAmt[TBSIZ_AMT];               // 155
   char  RefundInd;                       // 165
   char  RefundDate[TBSIZ_YYYYMMDD];      // 166
   char  RefundAmt[TBSIZ_AMT];            // 174
   char  TaxDeferInd;                     // 184
   char  BillDelqDate[TBSIZ_YYYYMMDD];    // 185
   char  Prev_Eligibility_Ind;            // 193
   char  BillDelqInd;                     // 194
   char  DaysProRated[3];                 // 195
   INSTREC  Inst1;
   INSTREC  Inst2;
} BILLREC;

typedef struct _tSbdTax
{
   PARCREC Parcel;                        // 1
   BILLREC Bill;                          // 24
   char  CrLf[2];
} SBDTAX;

// TR345PC.TXT
typedef  struct _tSCNDELQ
{
   char  RecordType            [8];    // 01 - SCNDELQ/SCNINST
   char  ParcelNumber          [13];   // 09
   char  DelqType;                     // 22 - S/U
   char  BillNumber            [9];    // 23
   char  RedempStatus;                 // 32
   char  Delq_Tax_Default_Date [6];    // 33 - YYMMDD
   char  Tax_Roll_Status_Type;         // 39
   char  Tax_Roll_Status_Roll;         // 40
   char  Delq_Bill_Eff_Date    [6];    // 41 - MMDDYY
   char  Delq_Supp_Roll_Number [2];    // 47
   char  Tax_Rate_Area         [10];   // 49 - All 0
   char  Special_Pay_Plan_Ind;         // 59 - P/N
   char  Pay_Plan_Code         [2];    // 60 - 05
   char  Pay_Plan_Seq          [2];    // 62
   char  Pay_Plan_Nbr_Of_Insts [2];    // 64
   char  PaymentStatus         [2];    // 66
   char  Pay_Plan_Post_Flag;           // 68
   char  Pay_Plan_Next_Due_Dt  [6];    // 69 - MMDDYY
   char  RedempAmt             [13];   // 75 - $
   char  TR810_Redemption_Month[2];    // 88 
   char  TotalFeeIntrAmt       [11];   // 90 - (+-)
   char  filler[52];
} SCNDELQ;

// Standard codes
// H=Homeowners
// D=Disabled Veterans
// V=Veterans
// C=Church
// L=Library
// M=Museums
// R=Religion
// T=Tribal Housing
// W=Welfare
// U=College
// P=Public School
// E=Cemetery
// I=Hospital
// S=School
// X=Misc exempt
// Y=Military

IDX_TBL4 SBD_Exemption[] = 
{
   "00", "H", 2,1,     // HOMEOWNER'S EXEMPTION
   "10", "D", 2,1,     // DISABLED VET $119,285 OR 100% WHICHEVER IS LESS   
   "11", "D", 2,1,     // LATE FILE DISABLED VET $80,000 OR 80% (LESSER OF) 
   "12", "D", 2,1,     // DISABLED VET $178,929 OR 100% WHICHEVER IS LESS   
   "13", "D", 2,1,     // LATE FILE DISABLED VET $120,000 OR 80% (LESSER OF)
   "14", "D", 2,1,     // DISABLED VETERAN - BLIND $60,000                  
   "15", "D", 2,1,     // LATE FILE DISABLED VET $107,357 OR 90% (LESSER OF)
   "16", "D", 2,1,     // LATE FILE DISABLED VET $161,036 OR 90% (LESSER OF)
   "17", "D", 2,1,     // LATE FILE DISABLED VET $101,392 OR 85% (LESSER OF)
   "18", "D", 2,1,     // LATE FILE DISABLED VET $152,090 OR 85% (LESSER OF)
   "20", "W", 2,1,     // WELFARE                                           
   "21", "I", 2,1,     // HOSPITAL                                          
   "22", "R", 2,1,     // RELIGIOUS                                         
   "24", "R", 2,1,     // LATE FILING 85% RELIGIOUS                         
   "25", "R", 2,1,     // LATE FILING 90% RELIGIOUS                         
   "26", "I", 2,1,     // LATE FILING 85% HOSPITAL                          
   "27", "I", 2,1,     // LATE FILING 90% HOSPITAL                          
   "28", "W", 2,1,     // LATE FILING 85% WELFARE                           
   "29", "W", 2,1,     // LATE FILING 90% WELFARE                           
   "30", "C", 2,1,     // CHURCH                                            
   "32", "X", 2,1,     // CERTAIN AIRCRAFT                                  
   "35", "X", 2,1,     // HISTORICAL AIRCRAFT                               
   "36", "X", 2,1,     // LATE FILING 80% HISTORICAL AIRCRAFT               
   "38", "C", 2,1,     // LATE FILING 85% CHURCH                            
   "39", "C", 2,1,     // LATE FILING 90% CHURCH                            
   "40", "E", 2,1,     // CEMETERY                                          
   "48", "E", 2,1,     // LATE FILING 85% CEMETERY                          
   "49", "E", 2,1,     // LATE FILING 90% CEMETERY                          
   "51", "X", 2,1,     // LESSOR                                            
   "52", "X", 2,1,     // LATE FILING 90% LESSOR                            
   "53", "X", 2,1,     // LATE FILING 85% LESSOR                            
   "60", "U", 2,1,     // COLLEGE                                           
   "68", "U", 2,1,     // LATE FILING 85% COLLEGE                           
   "69", "U", 2,1,     // LATE FILING 90% COLLEGE                           
   "70", "V", 2,1,     // REGULAR VETERAN $4,000 OR 100% WHICHEVER IS LESS  
   "79", "V", 2,1,     // LATE FILE REGULAR VET $3,200 OR 80% (LESSER OF)   
   "81", "P", 2,1,     // PUBLIC SCHOOLS                                    
   "82", "S", 2,1,     // PRIVATE SCHOOL                                    
   "83", "P", 2,1,     // LATE FILING 90% PUBLIC SCHOOLS                    
   "84", "Y", 2,1,     // SERVICEMEMBERS                                    
   "85", "P", 2,1,     // LATE FILING 85% PUBLIC SCHOOLS                    
   "86", "V", 2,1,     // VETERANS' ORGANIZATION                            
   "87", "L", 2,1,     // PUBLIC LIBRARY                                    
   "88", "M", 2,1,     // LIBRARY/MUSEUM                                    
   "89", "V", 2,1,     // LATE FILING 90% VETERANS' ORGANIZATION            
   "90", "M", 2,1,     // LATE FILING 90% LIBRARY/MUSEUM                    
   "91", "V", 2,1,     // LATE FILING 85% VETERANS' ORGANIZATION            
   "92", "M", 2,1,     // LATE FILING 85% LIBRARY/MUSEUM                    
   "","",0,0
};

#endif
