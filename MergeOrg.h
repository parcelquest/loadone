#ifndef  _MERGEORG_H_
#define  _MERGEORG_H_

#define  ORG_ROLL_APN               0
#define  ORG_ROLL_TRA               1
#define  ORG_ROLL_OWNERNAME         2
#define  ORG_ROLL_VESTINGCODE       3
#define  ORG_ROLL_NOLAND_VAL        4
#define  ORG_ROLL_M_ZIP             5
#define  ORG_ROLL_FIELD_1           6
#define  ORG_ROLL_FIELD_2           7
#define  ORG_ROLL_PROPERTYTYPE      8
#define  ORG_ROLL_SOLDTOSTATE       9
#define  ORG_ROLL_VETELIGIBILITY    10
#define  ORG_ROLL_DOCNUM            11
#define  ORG_ROLL_LAND              12
#define  ORG_ROLL_IMPR              13
#define  ORG_ROLL_OTHERIMPR         14
#define  ORG_ROLL_PP_VAL            15
#define  ORG_ROLL_GROSS             16
#define  ORG_ROLL_NET               17
#define  ORG_ROLL_EXE1_CD           18
#define  ORG_ROLL_EXE1_AMT          19
#define  ORG_ROLL_EXE2_CD           20
#define  ORG_ROLL_EXE2_AMT          21
#define  ORG_ROLL_COL023            22
#define  ORG_ROLL_LAND_BASEYR       23
#define  ORG_ROLL_IMPR_BASEYR       24
#define  ORG_ROLL_FIELD_4           25
#define  ORG_ROLL_FIELD_5           26
#define  ORG_ROLL_SUPPASMTCODE      27
#define  ORG_ROLL_PENDINGAPL_FLAG   28
#define  ORG_ROLL_NAD_B             29    // Owner name2
#define  ORG_ROLL_NAD_C             30    // Owner name3
#define  ORG_ROLL_NAD_D             31    // Owner name4
#define  ORG_ROLL_NAD_E             32    // New owner name recorded after 1/1
#define  ORG_ROLL_NAD_F             33
#define  ORG_ROLL_NAD_G             34
#define  ORG_ROLL_NAD_H             35
#define  ORG_ROLL_NAD_J             36    // M_Addr J-N
#define  ORG_ROLL_NAD_K             37
#define  ORG_ROLL_NAD_L             38
#define  ORG_ROLL_NAD_M             39
#define  ORG_ROLL_NAD_N             40
#define  ORG_ROLL_NAD_S             41    // Situs
#define  ORG_ROLL_NAD_T             42
#define  ORG_ROLL_NAD_L1            43    // Legal
#define  ORG_ROLL_NAD_L2            44
#define  ORG_ROLL_NAD_L3            45
#define  ORG_ROLL_NAD_L4            46
#define  ORG_ROLL_NAD_L5            47
#define  ORG_ROLL_MISCCODE1         48
#define  ORG_ROLL_MISCMSG1          49
#define  ORG_ROLL_MISCCODE2         50
#define  ORG_ROLL_MISCMSG2          51

typedef struct _Situs1
{
   char  acHse[7];
   char  acDir[2];
   char  acStreet[16];
} SITUS1;

typedef struct _Situs2
{
   char  acFiller[9];
   char  acSuffix[2];
   char  acSuite[5];
   char  acCityCode[2];
} SITUS2;

struct MyCodeTable
{
   char  *pCode;
   char  *pName;
   int   iPriTra;
};

struct MyCodeTable CityTbl[] =
{
   {"AN", "ANAHEIM",       	      1},
   {"AV", "ALISO VIEJO",   	      34},
   {"BP", "BUENA PARK",    			14},
   {"BR", "BREA",          			2},
   {"CC", "COTO DE CAZA",				-1},
   {"CD", "CORONA DEL MAR",			-1},
   {"CM", "COSTA MESA",					15},
   {"CP", "CYPRESS",						19},
   {"DC", "DOVE CANYON",				-1},
   {"DP", "DANA POINT",					28},
   {"FL", "FULLERTON",					3},
   {"FR", "FOOTHILL RANCH",			-1},
   {"FV", "FOUNTAIN VALLEY",			21},
   {"GG", "GARDEN GROVE",				18},
   {"HB", "HUNTINGTON BEACH",			4},
   {"IR", "IRVINE",						26},
   {"LA", "LOS ALAMITOS",				22},
   {"LB", "LAGUNA BEACH",				5},
   {"LH", "LA HABRA",					6},
   {"LK", "LAKE FOREST",				30},
   {"LL", "LAGUNA HILLS",				31},
   {"LN", "LAGUNA NIGUEL",				29},
   {"LP", "LA PALMA",					16},
   {"LW", "LAGUNA WOODS",				32},
   {"MC", "MIDWAY CITY",				-1},
   {"MO", "MODJESKA",					-1},
   {"MV", "MISSION VIEJO",				27},
   {"NB", "NEWPORT BEACH",				7},
   {"NC", "NEWPORT COAST",				-1},
   {"OR", "ORANGE",						8},
   {"PL", "PLACENTIA",					9},
   {"RM", "RANCHO SANTA MARGARITA", 33},
   {"SA", "SANTA ANA",					11},
   {"SB", "SEAL BEACH",					12},
   {"SC", "SAN CLEMENTE",				10},
   {"SI", "SILVERADO",					-1},
   {"SJ", "SAN JUAN CAPISTRANO",		23},
   {"ST", "STANTON",						17},
   {"SU", "SUNSET BEACH",				-1},
   {"TC", "TRABUCO CANYON",			-1},
   {"TU", "TUSTIN",						13},
   {"VP", "VILLA PARK",					24},
   {"WS", "WESTMINSTER",				20},
   {"YL", "YORBA LINDA",				25},
   {"ZZ", "", 								0}
};

struct MyCodeTable Tra2City[] =
{
   {"AN", "ANAHEIM",       			1},
   {"BR", "BREA",          			2},
   {"FL", "FULLERTON",					3},
   {"HB", "HUNTINGTON BEACH",			4},
   {"LB", "LAGUNA BEACH",				5},
   {"LH", "LA HABRA",					6},
   {"NB", "NEWPORT BEACH",				7},
   {"OR", "ORANGE",						8},
   {"PL", "PLACENTIA",					9},
   {"SC", "SAN CLEMENTE",				10},
   {"SA", "SANTA ANA",					11},
   {"SB", "SEAL BEACH",					12},
   {"TU", "TUSTIN",						13},
   {"BP", "BUENA PARK",    			14},
   {"CM", "COSTA MESA",					15},
   {"LP", "LA PALMA",					16},
   {"ST", "STANTON",						17},
   {"GG", "GARDEN GROVE",				18},
   {"CP", "CYPRESS",						19},
   {"WS", "WESTMINSTER",				20},
   {"FV", "FOUNTAIN VALLEY",			21},
   {"LA", "LOS ALAMITOS",				22},
   {"SJ", "SAN JUAN CAPISTRANO",		23},
   {"VP", "VILLA PARK",					24},
   {"YL", "YORBA LINDA",				25},
   {"IR", "IRVINE",						26},
   {"MV", "MISSION VIEJO",				27},
   {"DP", "DANA POINT",					28},
   {"LN", "LAGUNA NIGUEL",				29},
   {"LK", "LAKE FOREST",				30},
   {"LL", "LAGUNA HILLS",				31},
   {"LW", "LAGUNA WOODS",				32},
   {"RM", "RANCHO SANTA MARGARITA", 33},
   {"AV", "ALISO VIEJO",   			34},
   {"ZZ", "", 								0}
};

#define MAX_PRI_TRA                 34

char *asPropType[] =
{
   "106",   // Mobile home
   "101",   // RESID. SINGLE FAMILY
   "102",   // RESID. MULTIPLE FAMILY
   "200",   // COMMERCIAL
   "300",   // INDUSTRIAL
   "600",   // RURAL IMPR
   "600",   // MISCELLANEOUS
   "600",
   "602",   // EXEMPT
   "999"    // UNASSIGNED
};

IDX_SALE ORG_DocCode[] =
{// DOCTITLE, DOCTYPE, ISSALE, ISXFER, TITLELEN, TYPELEN
   "ABAND HOMESTEAD" ,"74",'N','N',15,2,  // 
   "ABSTRACT"        ,"74",'N','N',8 ,2,  // 
   "ACCEPTANCE"      ,"74",'N','N',10,2,  // 
   "ADDEND NT ASSMT" ,"74",'N','N',12,2,  // 
   "AFDVT SUCCSR TR" ,"6 ",'N','Y',12,2,  // 
   "AFFIDAVIT"       ,"74",'N','Y',8 ,2,  // 
   "AGREEMENT SALE"  ,"8 ",'N','Y',12,2,  // 
   "AGREEMENT"       ,"74",'N','N',9 ,2,  // 
   "AMD COVEN/RESTR" ,"74",'N','N',14,2,  // Restrictive Covenant
   "AMD EASEMNT AGM" ,"74",'N','N',14,2,  // 
   "AMD GRANT DEED"  ,"74",'N','Y',14,2,  // 
   "AMD MEMO LEASE"  ,"74",'N','N',14,2,  // 
   "AMD RSRV EASMT"  ,"74",'N','N',14,2,  // 
   "AMD TRUST DEED"  ,"74",'N','N',14,2,  // 
   "AMEND"           ,"74",'N','N',5,2,  // 
   "APPL RNWL JDGT"  ,"74",'N','N',10,2,  // 
   "ASGT OF RIGHTS"  ,"74",'N','N',10,2,  // 
   "ASGT SUBLEASE"   ,"74",'N','N',10,2,  // 
   "ASGT TRUST DEED" ,"74",'N','N',13,2,  // 
   "ASSESSMENT"      ,"74",'N','N',10,2,  // 
   "ASSIGNMENT"      ,"7 ",'N','N',10,2,  // 
   "ASSUMPTION AGM"  ,"74",'N','N',10,2,  // 
   "BOND"            ,"74",'N','N',4 ,2,  // 
   "CERTIF COMPLY"   ,"74",'N','N',10,2,  // 
   "CERTIF SALE"    ,"74",'N','N',10,2,  // 
   "CONDOMINIUM PLN" ,"74",'N','N',10,2,  // 
   "CONSENT"         ,"74",'N','N',7 ,2,  // 
   "CONSERV EASEMNT" ,"74",'N','N',10,2,  // 
   "CONSTR TRUST DD" ,"74",'N','N',10,2,  // 
   "CONTRACT"        ,"74",'N','N',8 ,2,  // 
   "COVEN & RESTR"   ,"74",'N','N',10,2,  // 
   "COVENANT"        ,"74",'N','N',8 ,2,  // 
   "CTF AGREEMENT"   ,"74",'N','N',10,2,  // 
   "CTF CORRECTION"  ,"74",'N','N',10,2,  // 
   "CTF OF TRUST"    ,"74",'N','N',10,2,  // 
   "DECLARATION"     ,"74",'N','N',10,2,  // 
   "DECLN EASEMENT"  ,"74",'N','N',10,2,  // 
   "DECLN HOMESTEAD" ,"74",'N','N',10,2,  // 
   "DECLN RESTR"     ,"74",'N','N',10,2,  // 
   "DECLN TRUST"     ,"74",'N','N',10,2,  // 
   "DEDICATION"      ,"74",'N','N',10,2,  // 
   "DELINQUENT CTF"  ,"74",'N','N',10,2,  // 
   "DEED RESTRICTN"  ,"74",'N','Y',10,2,  // 
   "DEED"            ,"13",'Y','Y',4 ,2,  // 
   "EASEMENT"        ,"40",'N','N',8 ,2,  // 
   "GRANT DEED"      ,"1 ",'Y','Y',10,2,  // 
   "GRANT OF LIEN"   ,"74",'N','N',10,2,  // 
   "JUDGMENT"        ,"74",'N','N',8 ,2,  // 
   "LAND CONTRACT"   ,"74",'N','N',10,2,  // 
   "LEASE"           ,"74",'N','N',5 ,2,  // 
   "LETTER ADMN"     ,"74",'N','N',10,2,  // 
   "LIEN"            ,"46",'N','Y',4 ,2,  // 
   "LIS PENDENS"     ,"74",'N','Y',10,2,  // a written notice that a lawsuit has been filed concerning real estate
   "LTD PARTNERSHIP" ,"74",'N','N',10,2,  // 
   "LTD PTNSHP"      ,"74",'N','N',10,2,  // 
   "MDF AGREEMENT"   ,"74",'N','N',10,2,  // 
   "MECHANIC LIEN"   ,"46",'N','Y',10,2,  // A mechanics lien is a "hold" against your property, filed by an unpaid contractor, ...
   "MEMO "           ,"74",'N','N',4 ,2,  // 
   "MERGER"          ,"74",'N','N',6 ,2,  // 
   "MISCELLANEOUS"   ,"74",'N','N',8 ,2,  // 
   "MODIF TRUST DD"  ,"74",'N','N',10,2,  // 
   "MORTGAGE"        ,"74",'N','N',8 ,2,  // 
   "NOTARY BOND"     ,"74",'N','N',10,2,  // 
   "NOTICE"          ,"74",'N','N',6 ,2,  // 
   "NT ASSESSMENT"   ,"74",'N','N',10,2,  // 
   "NT COMPLETION"   ,"74",'N','N',10,2,  // 
   "NT DELQNT ASSMT" ,"74",'N','N',10,2,  // 
   "NT INTEND TRANS" ,"74",'N','N',10,2,  // 
   "NT OF AGREEMENT" ,"74",'N','N',10,2,  // 
   "NT RESCISSION"   ,"74",'N','N',10,2,  // 
   "NT RESTRCTN AGM" ,"74",'N','N',10,2,  // 
   "NT RNWL JDGT"    ,"74",'N','N',10,2,  // 
   "NT SUPPORT JDGT" ,"74",'N','N',10,2,  // 
   "NT TERM"         ,"74",'N','N',7 ,2,  // 
   "NT TO CREDITORS" ,"74",'N','N',10,2,  // 
   "NT TRUSTEE SALE" ,"74",'N','N',10,2,  // 
   "OPTION"          ,"80",'N','N',5 ,2,  // 
   "ORDER"           ,"80",'N','N',5 ,2,  // 
   "PARTIAL Q C"     ,"74",'N','N',10,2,  // 
   "PARTIAL"         ,"74",'N','N',7 ,2,  // 
   "PARTL RECONVEY"  ,"74",'N','N',10,2,  // 
   "PAYMENT CONTR AS","74",'N','N',10,2,  // 
   "PETITION"        ,"74",'N','N',8 ,2,  // 
   "POWER ATTORNEY"  ,"74",'N','N',10,2,  // 
   "QUITCLAIM"       ,"4 ",'N','Y',8 ,2,  // 
   "RECONVEYANCE"    ,"74",'N','N',10,2,  // 
   "RECORD SURVEY"   ,"74",'N','N',10,2,  // 
   "RELEASE"         ,"74",'N','N',7 ,2,  // 
   "REMOV INVL LIEN" ,"74",'N','N',10,2,  // 
   "REPURCHASE OPT"  ,"74",'N','N',10,2,  // 
   "REQ NT DELINQ"   ,"74",'N','N',10,2,  // 
   "REQUEST NOTICE"  ,"74",'N','N',10,2,  // 
   "RESCISSION"      ,"74",'N','N',10,2,  // 
   "RESOLUTION"      ,"74",'N','N',10,2,  // 
   "REVOC PWR ATTY"  ,"74",'N','N',10,2,  // 
   "REVOCABLE TOD DE","74",'N','Y',13,2,  // 
   "REVOCATION DEED" ,"74",'N','N',13,2,  // 
   "REVOCATION TOD D","74",'N','N',13,2,  // 
   "REVOCATION"      ,"74",'N','N',10,2,  // 
   "RSRV EASEMENT"   ,"74",'N','N',10,2,  // 
   "SPCL POWER ATTY" ,"74",'N','N',10,2,  // 
   "RSRV EASEMENT"   ,"74",'N','N',10,2,  // 
   "STATEMNT PTNSHP" ,"74",'N','N',10,2,  // 
   "STREET DEED"     ,"74",'N','N',10,2,  // 
   "SUBDIVISION MAP" ,"74",'N','N',14,2,  // 
   "SUBDN AGREEMENT" ,"74",'N','N',10,2,  // 
   "SUBLEASE"        ,"74",'N','N',8 ,2,  // 
   "SUBORDINATION"   ,"74",'N','N',10,2,  // 
   "SUBSTITUTION TR" ,"74",'N','N',10,2,  // 
   "TERM OPTION"     ,"74",'N','N',10,2,  // 
   "TERMINATION"     ,"74",'N','N',10,2,  // 
   "TERMINTN EASEMT" ,"74",'N','N',10,2,  // 
   "TRUST DEED"      ,"65",'N','N',10,2,  // 
   "TRUSTEES DEED"   ,"27",'N','Y',10,2,  // 
   "UCC - AMENDMENT" ,"74",'N','N',10,2,  // 
   "UCC - ASSIGN"    ,"74",'N','N',10,2,  // 
   "UCC - CONT"      ,"74",'N','N',10,2,  // 
   "UCC - F S"       ,"74",'N','N',9 ,2,  // 
   "UCC - TERM"      ,"74",'N','N',10,2,  // 
   "WATER QLTY PLAN" ,"74",'N','N',10,2,  // 
   "WITHDRAWAL"      ,"74",'N','N',10,2,  // 
   "WRIT EXECUTION"  ,"74",'N','N',10,2,  // 
   "","",0,0,0
};

// Document.txt
// Early 2011 and prior
// Fixed length (178+CRLF) and has trailing space
#define  ORG_DOC_DOCKEY    1
#define  ORG_DOC_DOCNUM    2
#define  ORG_DOC_DOCDATE   3
#define  ORG_DOC_DOCTITLE  4
#define  ORG_DOC_DOCSUBT   5
#define  ORG_DOC_GCODE     6
#define  ORG_DOC_NAME1     7
#define  ORG_DOC_REFNAME   8
#define  ORG_DOC_TITLEFLG  9     // Title confidential
#define  ORG_DOC_NAMEFLG   10    // Name confidential

// Late 2011 & 2012
// 2022
#define  ORG_DOC1_DOCKEY   1
#define  ORG_DOC1_DOCNUM   2
#define  ORG_DOC1_DOCDATE  3
#define  ORG_DOC1_DOCTITLE 4
#define  ORG_DOC1_GCODE    5
#define  ORG_DOC1_NAME1    6
#define  ORG_DOC1_REFNAME  7
#define  ORG_DOC1_TITLEFLG 8
#define  ORG_DOC1_NAMEFLG  9

// 20120409-
// Variable length (<170) and no trailing space
#define  ORG_DOC2_DOCKEY   1
#define  ORG_DOC2_DOCSEQ   2
#define  ORG_DOC2_DOCNUM   3
#define  ORG_DOC2_DOCDATE  4
#define  ORG_DOC2_DOCTITLE 5
#define  ORG_DOC2_GCODE    6
#define  ORG_DOC2_NAME1    7
#define  ORG_DOC2_REFNAME  8
#define  ORG_DOC2_TITLEFLG 9
#define  ORG_DOC2_NAMEFLG  10


// Deed.txt file
#define  ORG_DEED_DOCKEY   0
#define  ORG_DEED_DOCNUM   1
#define  ORG_DEED_DOCTAX   2
#define  ORG_DEED_DOCCODE  3
#define  ORG_DEED_PARCEL   4
#define  ORG_DEED_NAME     5
#define  ORG_DEED_ADDR1    6
#define  ORG_DEED_CITY     7
#define  ORG_DEED_STATE    8
#define  ORG_DEED_ZIP      9
#define  ORG_DEED_LOT      10
#define  ORG_DEED_UNIT     11
#define  ORG_DEED_TRACT    12
#define  ORG_DEED_BLOCK    13
#define  ORG_DEED_NDTAX    14

#define  ORG_DEED1_DOCKEY   0
#define  ORG_DEED1_DOCNUM   1
#define  ORG_DEED1_DOCTAX   2
#define  ORG_DEED1_DOCCODE  3
#define  ORG_DEED1_PARCEL   4
#define  ORG_DEED1_ADDR1    5
#define  ORG_DEED1_CITY     6
#define  ORG_DEED1_STATE    7
#define  ORG_DEED1_ZIP      8
#define  ORG_DEED1_LOT      9
#define  ORG_DEED1_UNIT     10
#define  ORG_DEED1_TRACT    11
#define  ORG_DEED1_BLOCK    12
#define  ORG_DEED1_NDTAX    13

// Secured roll amendment
#define  ORG_AMNT_OWNERNAME         1
#define  ORG_AMNT_APN               2
#define  ORG_AMNT_LAND              3
#define  ORG_AMNT_IMPR              4
#define  ORG_AMNT_GROSS             5
#define  ORG_AMNT_EXEAMT            6
#define  ORG_AMNT_EXE_TYPE          7
#define  ORG_AMNT_NET               8
#define  ORG_AMNT_COR_EXE           9
#define  ORG_AMNT_COR_NET          10

// Characteristics
#define  ORG_CHAR_APN					0
#define  ORG_CHAR_LND_SQFT				1
#define  ORG_CHAR_IMP_SQFT				2
#define  ORG_CHAR_GARAGE_SQFT			3
#define  ORG_CHAR_CARPORT_SQFT		4
#define  ORG_CHAR_NBR_BED				5
#define  ORG_CHAR_NBR_BTH				6
#define  ORG_CHAR_NBR_HLF_BTH			7
#define  ORG_CHAR_CNS_YR				8
#define  ORG_CHAR_POOL_FLG				9
#define  ORG_CHAR_SPA_FLG			  10
#define  ORG_CHAR_USE				  11

// 2024/01/24
#define  OFF_T_APN               1
#define  OFF_T_APN_SUFF          9
#define  OFF_T_BRD_NBR           11
#define  OFF_T_BRD_DTE           19
#define  OFF_T_FST_INS_AMT       27
#define  OFF_T_FST_PAID_DTE      39
#define  OFF_T_FST_PAID_SUB      47
#define  OFF_T_FST_BTCH_NBR      49
#define  OFF_T_FST_BTCH_SQN      53
#define  OFF_T_FST_PEN_FLG       58
#define  OFF_T_SND_INS_AMT       59
#define  OFF_T_SND_PAID_DTE      71
#define  OFF_T_SND_PAID_SUB      79
#define  OFF_T_SND_BTCH_NBR      81
#define  OFF_T_SND_BTCH_SQN      85
#define  OFF_T_SND_PEN_FLG       90
#define  OFF_T_BOR_RSN_CDE       91
#define  OFF_T_BAD_CHK_CNT       93
#define  OFF_T_TOT_BAD_CHK_AMT   97
#define  OFF_T_FILLER            103
//#define  OFF_T_APN               1
//#define  OFF_T_APN_SUFF          9
//#define  OFF_T_BRD_NBR           11
//#define  OFF_T_BRD_DTE           19
//#define  OFF_T_FST_INS_AMT       27
//#define  OFF_T_FST_PAID_DTE      37
//#define  OFF_T_FST_BTCH_NBR      47
//#define  OFF_T_FST_BTCH_SQN      51
//#define  OFF_T_FST_PEN_FLG       56
//#define  OFF_T_SND_INS_AMT       57
//#define  OFF_T_SND_PAID_DTE      67
//#define  OFF_T_SND_BTCH_NBR      77
//#define  OFF_T_SND_BTCH_SQN      81
//#define  OFF_T_SND_PEN_FLG       86
//#define  OFF_T_BOR_RSN_CDE       87
//#define  OFF_T_BAD_CHK_CNT       89
//#define  OFF_T_TOT_BAD_CHK_AMT   92
//#define  OFF_T_FILLER            97

#define  SIZ_T_APN               8
#define  SIZ_T_APN_SUFF          2
#define  SIZ_T_BRD_NBR           8
#define  SIZ_T_BRD_DTE           8
//#define  SIZ_T_AMT               10    //8.2
//#define  SIZ_T_PAIDDATE          10
#define  SIZ_T_AMT               12    // +-8.2
#define  SIZ_T_PAIDDATE          8     // YYYYMMDD
#define  SIZ_T_PAIDSUB           2
#define  SIZ_T_DATE              8
//#define  SIZ_T_BTCH_NBR          4
//#define  SIZ_T_BTCH_SQN          5
//#define  SIZ_T_BOR_RSN_CDE       2
//#define  SIZ_T_BAD_CHK_CNT       3
//#define  SIZ_T_TOT_BAD_CHK_AMT   5     //3.2
#define  SIZ_T_BTCH_NBR          4
#define  SIZ_T_BTCH_SQN          5
#define  SIZ_T_BOR_RSN_CDE       2
#define  SIZ_T_BAD_CHK_CNT       4
#define  SIZ_T_TOT_BAD_CHK_AMT   7     // +-3.2

// Tax file TC01CT01.TXT
typedef struct _tORG_TaxBill
{
   char  Apn      [SIZ_T_APN];
   char  Apn_Suff [SIZ_T_APN_SUFF];
   char  BoardNo  [SIZ_T_BRD_NBR];
   char  BoardDate[SIZ_T_BRD_DTE];
   char  Inst1_Amt[SIZ_T_AMT];
   char  PaidDate1[SIZ_T_PAIDDATE];
   char  PaidSub1 [SIZ_T_PAIDSUB];
   char  BatchNo1 [SIZ_T_BTCH_NBR];
   char  BatchSeq1[SIZ_T_BTCH_SQN];
   char  Pen_Flg1;
   char  Inst2_Amt[SIZ_T_AMT];
   char  PaidDate2[SIZ_T_PAIDDATE];
   char  PaidSub2 [SIZ_T_PAIDSUB];
   char  BatchNo2 [SIZ_T_BTCH_NBR];
   char  BatchSeq2[SIZ_T_BTCH_SQN];
   char  Pen_Flg2;
   char  BoardRsn [SIZ_T_BOR_RSN_CDE];
   char  BadChkCnt[SIZ_T_BAD_CHK_CNT];       // +-999
   char  BadChkAmt[SIZ_T_TOT_BAD_CHK_AMT];   // +-999.99
   char  Filler[10];
   char  CrLf[2];
} ORG_TAX;

// Special Assessment file TC01CT02.TXT
#define  OFF_T_RECTYPE           1     // 1=Tax Type rec
                                       // 2=Dist Type rec
                                       // 3=Detail Type rec
#define  OFF_T1_TBLCODE          2
#define  OFF_T1_TBLDESC          6

#define  SIZ_T1_TBLCODE          4
#define  SIZ_T1_TBLDESC          34    // Up to 34 characters

#define  OFF_T3_APN              2
#define  OFF_T3_APN_SUFF         10
#define  OFF_T3_TAXTYPE          14
#define  OFF_T3_DIST_ID          16
#define  OFF_T3_FST_TAX_AMT      20
#define  OFF_T3_SND_TAX_AMT      30

#define  SIZ_T3_APN_SUFF         4
#define  SIZ_T3_TAXTYPE          2
#define  SIZ_T3_DIST_ID          4

typedef struct _tORG_SpecialAssessment
{
   char  RecType;                      // 1=Tax type, 2=Dist type, 3=Detail
   char  Apn      [SIZ_T_APN];
   char  Apn_Suff [SIZ_T3_APN_SUFF];
   char  TaxType  [SIZ_T3_TAXTYPE];
   char  DistID   [SIZ_T3_DIST_ID];
   char  TaxAmt1  [SIZ_T_AMT];
   char  TaxAmt2  [SIZ_T_AMT];
   char  CrLf[2];
} ORG_SPCASMT;

// 2024/01/24 - TC02CA01.txt
#define  OFF_TD_APN            1
#define  OFF_TD_APN_SUFF       9
#define  OFF_TD_TDN            11
#define  OFF_TD_REC_TYP        20
#define  OFF_TD_STS_CDE        21
#define  OFF_TD_CURR_TRA       22
#define  OFF_TD_PREV_TRA       27
#define  OFF_TD_PREV_APN       32
#define  OFF_TD_PREV_APN_SUF   40
#define  OFF_TD_ORIG_TRA       42
#define  OFF_TD_ORIG_APN       47
#define  OFF_TD_ORIG_APN_SUF   55
#define  OFF_TD_SITUS          57
#define  OFF_TD_MULT_FLAG      100
#define  OFF_TD_COMB_PARCEL    101
#define  OFF_TD_DTE_DEEDED     102
#define  OFF_TD_DOCDATE        110
#define  OFF_TD_DOCNUM         118
#define  OFF_TD_UPD_FLG        129
#define  OFF_TD_PAID_FLG       130
#define  OFF_TD_INST_START     131     // yyyyMM
#define  OFF_TD_POINT_BILL_IND 137
#define  OFF_TD_LOST_HIST_YR   138
#define  OFF_TD_UPD_DATE       142     // yyyyMMdd
#define  OFF_TD_INST_PAID_DTE  150
#define  OFF_TD_INST_PAID      158
#define  OFF_TD_NO_XREF_FLAG   160
#define  OFF_TD_PRE_JPA_FLG    161
#define  OFF_TD_TDN_STS_CDE    162
#define  OFF_TD_BOR_NBR        164
#define  OFF_TD_BOR_DTE        176
#define  OFF_TD_BOR_RSN_CDE    184
#define  OFF_TD_FNDS_ON_DEPOS  186
#define  OFF_TD_INST_PAYOFFAMT 199
#define  OFF_TD_FILLER         202

#define  SIZ_TD_APN            8
#define  SIZ_TD_APN_SUFF       2
#define  SIZ_TD_TDN            9
#define  SIZ_TD_REC_TYP        1
#define  SIZ_TD_STS_CDE        1
#define  SIZ_TD_TRA            5
#define  SIZ_TD_SITUS          43
#define  SIZ_TD_MULT_FLAG      1
#define  SIZ_TD_COMB_PARCEL    1
#define  SIZ_TD_DATE           8
#define  SIZ_TD_DOC_REF_NBR    11
#define  SIZ_TD_UPD_FLG        1
#define  SIZ_TD_PAID_FLG       1
#define  SIZ_TD_INST_START     6
#define  SIZ_TD_POINT_BILL_IND 1
#define  SIZ_TD_YEAR           4
#define  SIZ_TD_INST_PAID      2
#define  SIZ_TD_NO_XREF_FLAG   1
#define  SIZ_TD_PRE_JPA_FLG    1
#define  SIZ_TD_TDN_STS_CDE    2
#define  SIZ_TD_BOR_NBR        12
#define  SIZ_TD_BOR_RSN_CDE    2
#define  SIZ_TD_FNDS_ON_DEPOS  13
#define  SIZ_TD_INST_PAYOFFAMT 13
#define  SIZ_TD_SEQNUM         2
#define  SIZ_TD_PAYMENT_STUB   2
#define  SIZ_TD_BTCH_TYPE      1
#define  SIZ_TD_REDEMP_FEE     4
#define  SIZ_TD_TAX_CODE       2

//#define  OFF_TD_APN            1
//#define  OFF_TD_APN_SUFF       9
//#define  OFF_TD_TDN            11
//#define  OFF_TD_REC_TYP        20
//#define  OFF_TD_STS_CDE        21
//#define  OFF_TD_CURR_TRA       22
//#define  OFF_TD_PREV_TRA       27
//#define  OFF_TD_PREV_APN       32
//#define  OFF_TD_PREV_APN_SUF   40
//#define  OFF_TD_ORIG_TRA       42
//#define  OFF_TD_ORIG_APN       47
//#define  OFF_TD_ORIG_APN_SUF   55
//#define  OFF_TD_SITUS          57
//#define  OFF_TD_MULT_FLAG      100
//#define  OFF_TD_COMB_PARCEL    101
//#define  OFF_TD_DTE_DEEDED     102
//#define  OFF_TD_DOCDATE        110
//#define  OFF_TD_DOCNUM         118
//#define  OFF_TD_UPD_FLG        129
//#define  OFF_TD_PAID_FLG       130
//#define  OFF_TD_INST_START     131     // yyyyMM
//#define  OFF_TD_POINT_BILL_IND 137
//#define  OFF_TD_LOST_HIST_YR   138
//#define  OFF_TD_UPD_DATE       142     // yyyyMMdd
//#define  OFF_TD_INST_PAID_DTE  150
//#define  OFF_TD_INST_PAID      158
//#define  OFF_TD_NO_XREF_FLAG   160
//#define  OFF_TD_PRE_JPA_FLG    161
//#define  OFF_TD_TDN_STS_CDE    162
//#define  OFF_TD_BOR_NBR        164
//#define  OFF_TD_BOR_DTE        176
//#define  OFF_TD_BOR_RSN_CDE    184
//#define  OFF_TD_FNDS_ON_DEPOS  186
//#define  OFF_TD_INST_PAYOFFAMT 199
//
//#define  SIZ_TD_APN            8
//#define  SIZ_TD_APN_SUFF       2
//#define  SIZ_TD_TDN            9
//#define  SIZ_TD_REC_TYP        1
//#define  SIZ_TD_STS_CDE        1
//#define  SIZ_TD_TRA            5
//#define  SIZ_TD_SITUS          43
//#define  SIZ_TD_MULT_FLAG      1
//#define  SIZ_TD_COMB_PARCEL    1
//#define  SIZ_TD_DATE           8
//#define  SIZ_TD_DOC_REF_NBR    11
//#define  SIZ_TD_UPD_FLG        1
//#define  SIZ_TD_PAID_FLG       1
//#define  SIZ_TD_INST_START     6
//#define  SIZ_TD_POINT_BILL_IND 1
//#define  SIZ_TD_YEAR           4
//#define  SIZ_TD_INST_PAID      2
//#define  SIZ_TD_NO_XREF_FLAG   1
//#define  SIZ_TD_PRE_JPA_FLG    1
//#define  SIZ_TD_TDN_STS_CDE    2
//#define  SIZ_TD_BOR_NBR        12
//#define  SIZ_TD_BOR_RSN_CDE    2
//#define  SIZ_TD_FNDS_ON_DEPOS  13
//#define  SIZ_TD_INST_PAYOFFAMT 13
//#define  SIZ_TD_SEQNUM         2
//#define  SIZ_TD_PAYMENT_STUB   2
//#define  SIZ_TD_BTCH_TYPE      1
//#define  SIZ_TD_REDEMP_FEE     4
//#define  SIZ_TD_TAX_CODE       2

typedef struct _tORG_TaxDefault
{  // Layout for record type 1
   char  Apn            [SIZ_TD_APN];
   char  Apn_Suff       [SIZ_TD_APN_SUFF];
   char  Default_No     [SIZ_TD_TDN];              // 11-19
   char  RecType;                                  // 1=General Description
                                                   // 4=Legal Description
                                                   // 7=Tax Charges
                                                   // 9=Payments
   char  RecStsCde;                                // (A)ctive, (I)nactive, (D)elete
   char  CurrTRA        [SIZ_TD_TRA];
   char  PrevTRA        [SIZ_TD_TRA];
   char  PrevApn        [SIZ_TD_APN];
   char  PrevApn_Suff   [SIZ_TD_APN_SUFF];
   char  OrigTRA        [SIZ_TD_TRA];
   char  OrigApn        [SIZ_TD_APN];
   char  OrigApn_Suff   [SIZ_TD_APN_SUFF];
   char  Situs          [SIZ_TD_SITUS];
   char  Mult_Flag;
   char  Comb_Parcel;
   char  Deeded_Date    [SIZ_TD_DATE];
   char  DocDate        [SIZ_TD_DATE];
   char  DocNum         [SIZ_TD_DOC_REF_NBR];
   char  Update_Flag;                              // 129
   char  Paid_Flag;                                // 130 - P or blank
   char  Inst_Start     [SIZ_TD_INST_START];
   char  PointBillInd;
   char  Lost_Hist_Yr   [SIZ_TD_YEAR];
   char  Updated_Date   [SIZ_TD_DATE];
   char  InstPaid_Date  [SIZ_TD_DATE];
   char  InstPaid       [SIZ_TD_INST_PAID];
   char  No_Xref_Flag;                             // 160
   char  Pre_Jpa_Flag;
   char  DelqStatus    [SIZ_TD_TDN_STS_CDE];       // 162-163
                                                   // 10 : Unpaid
                                                   // 50 : Active Installment Plan
                                                   // 60 : Defaulted Installment Plan
                                                   // 90 : Paid
                                                   // 95 : Cancelled
   char  BoardNo        [SIZ_TD_BOR_NBR];
   char  Board_Date     [SIZ_TD_DATE];
   char  Board_Rsn_Cde  [SIZ_TD_BOR_RSN_CDE];
   char  FundOnDeposit  [SIZ_TD_FNDS_ON_DEPOS];    // 186
   char  PayOffAmt      [SIZ_TD_INST_PAYOFFAMT];   // 197
} ORG_DFLT;

typedef struct _t_ORG_TaxAsmtItem
{
   char  TaxCode        [SIZ_TD_TAX_CODE];
   char  TaxAmt         [SIZ_T_AMT];
   char  InstType;                     // 2, 3, or blank
} ORG_TAXASMT;

#define  ORG_TD_MAX_ITEMS     10
typedef struct _t_ORG_TaxItem
{  // TC02CA01 record type 7
   char  Apn            [SIZ_TD_APN];
   char  Apn_Suff       [SIZ_TD_APN_SUFF];
   char  TDN            [SIZ_TD_TDN];
   char  RecType;                                  // 7
   char  TaxYear        [SIZ_TD_YEAR];
   char  SeqNum         [SIZ_TD_SEQNUM];
   ORG_TAXASMT asTaxAsmt[ORG_TD_MAX_ITEMS];
   char  BadChkCnt      [SIZ_T_BAD_CHK_CNT];
   char  BadChkAmt      [SIZ_T_TOT_BAD_CHK_AMT];
   char  Filler2[43];
   char  CrLf[2];
} ORG_TAXITEM;

typedef struct _t_ORG_PaidInfo
{  // TC02CA01 record type 9
   char  Apn            [SIZ_TD_APN];
   char  Apn_Suff       [SIZ_TD_APN_SUFF];
   char  TDN            [SIZ_TD_TDN];
   char  RecType;                                  // 20
   char  PaySeqNum      [SIZ_TD_SEQNUM];           // 21
   char  PaidDate       [SIZ_TD_DATE];             // 23
   char  PaymentStub    [SIZ_TD_PAYMENT_STUB];
   char  BatchNo        [SIZ_T_BTCH_NBR];          // 33
   char  BatchType      [SIZ_TD_BTCH_TYPE];        // 37
   char  PaidTaxAmt     [SIZ_T_AMT];               // 38-49
   char  PaidPenAmt     [SIZ_T_AMT];               // 50-61
   char  Redemp_Fee     [SIZ_TD_REDEMP_FEE];       // 62-67 +-99.99
   char  BatchSeq       [SIZ_T_BTCH_SQN];
   char  CrLf[2];
} ORG_TAXPAID;

// TC11CT01CurrentYearSupplemental
//#define  OFF_TS_APN            1  
//#define  OFF_TS_APN_SUF        9
//#define  OFF_TS_BRD_NBR        13
//#define  OFF_TS_ASM_DTE        21
//#define  OFF_TS_SYS_IND        29
//#define  OFF_TS_ASM_YEAR       31
//#define  OFF_TS_EFF_DTE        35
//#define  OFF_TS_FST_INS_AMT    43
//#define  OFF_TS_FST_PAID_DTE   55
//#define  OFF_TS_FST_BATCH_NBR  65
//#define  OFF_TS_FST_BATCH_SQN  69
//#define  OFF_TS_FST_PEN_FLG    74
//#define  OFF_TS_FST_DLQ_DTE    75
//#define  OFF_TS_SND_INS_AMT    83
//#define  OFF_TS_SND_PAID_DTE   95
//#define  OFF_TS_SND_BATCH_NBR  105
//#define  OFF_TS_SND_BATCH_SQN  109
//#define  OFF_TS_SND_PEN_FLG    114
//#define  OFF_TS_SND_DLQ_DTE    115
//#define  OFF_TS_ASE_NME        123
//
//#define  SIZ_TS_APN            8
//#define  SIZ_TS_APN_SUF        4
//#define  SIZ_TS_BRD_NBR        8
//#define  SIZ_TS_ASM_DTE        8
//#define  SIZ_TS_SYS_IND        2
//#define  SIZ_TS_ASM_YEAR       4
//#define  SIZ_TS_EFF_DTE        8
//#define  SIZ_TS_INS_AMT        12
//#define  SIZ_TS_PAID_DTE       8
//#define  SIZ_TS_BATCH_NBR      6
//#define  SIZ_TS_BATCH_SQN      5
//#define  SIZ_TS_PEN_FLG        1
//#define  SIZ_TS_DLQ_DTE        8
//#define  SIZ_TS_ASE_NME        25
//#define  SIZ_TS_FILLER         3

// 2024/01/24 - TC11CT01.txt
#define  OFF_TS_APN            1  
#define  OFF_TS_APN_SUF        9
#define  OFF_TS_BRD_NBR        13
#define  OFF_TS_ASM_DTE        21
#define  OFF_TS_SYS_IND        29
#define  OFF_TS_ASM_YEAR       31
#define  OFF_TS_EFF_DTE        35
#define  OFF_TS_FST_INS_AMT    43
#define  OFF_TS_FST_PAID_DTE   55
#define  OFF_TS_FST_PAID_SUB   63
#define  OFF_TS_FST_BATCH_NBR  65
#define  OFF_TS_FST_BATCH_SQN  69
#define  OFF_TS_FST_PEN_FLG    74
#define  OFF_TS_FST_DLQ_DTE    75
#define  OFF_TS_SND_INS_AMT    83
#define  OFF_TS_SND_PAID_DTE   95
#define  OFF_TS_SND_PAID_SUB   103
#define  OFF_TS_SND_BATCH_NBR  105
#define  OFF_TS_SND_BATCH_SQN  109
#define  OFF_TS_SND_PEN_FLG    114
#define  OFF_TS_SND_DLQ_DTE    115
#define  OFF_TS_ASE_NME        123

#define  SIZ_TS_APN            8
#define  SIZ_TS_APN_SUF        4
#define  SIZ_TS_BRD_NBR        8
#define  SIZ_TS_ASM_DTE        8
#define  SIZ_TS_SYS_IND        2
#define  SIZ_TS_ASM_YEAR       4
#define  SIZ_TS_EFF_DTE        8
#define  SIZ_TS_INS_AMT        12
#define  SIZ_TS_PAID_DTE       8
#define  SIZ_TS_PAID_SUB       2
#define  SIZ_TS_BATCH_NBR      4
#define  SIZ_TS_BATCH_SQN      5
#define  SIZ_TS_PEN_FLG        1
#define  SIZ_TS_DLQ_DTE        8
#define  SIZ_TS_ASE_NME        25
#define  SIZ_TS_FILLER         3

typedef struct _t_ORG_Supp
{
   char  Apn          [SIZ_TS_APN          ];
   char  Apn_Suf      [SIZ_TS_APN_SUF      ];
   char  Brd_Nbr      [SIZ_TS_BRD_NBR      ];
   char  Asm_Dte      [SIZ_TS_ASM_DTE      ];
   char  Sys_Ind      [SIZ_TS_SYS_IND      ];
   char  Asm_Year     [SIZ_TS_ASM_YEAR     ];
   char  Eff_Dte      [SIZ_TS_EFF_DTE      ];
   char  Fst_Ins_Amt  [SIZ_TS_INS_AMT      ];
   char  Fst_Paid_Dte [SIZ_TS_PAID_DTE     ];      // Paid date
   char  Fst_Paid_Sub [SIZ_TS_PAID_SUB     ];      // 
   char  Fst_Batch_Nbr[SIZ_TS_BATCH_NBR    ];
   char  Fst_Batch_Sqn[SIZ_TS_BATCH_SQN    ];
   char  Fst_Pen_Flg  [SIZ_TS_PEN_FLG      ];
   char  Fst_Dlq_Dte  [SIZ_TS_DLQ_DTE      ];      // Due date
   char  Snd_Ins_Amt  [SIZ_TS_INS_AMT      ];
   char  Snd_Paid_Dte [SIZ_TS_PAID_DTE     ];
   char  Snd_Paid_Sub [SIZ_TS_PAID_SUB     ];
   char  Snd_Batch_Nbr[SIZ_TS_BATCH_NBR    ];
   char  Snd_Batch_Sqn[SIZ_TS_BATCH_SQN    ];
   char  Snd_Pen_Flg  [SIZ_TS_PEN_FLG      ];
   char  Snd_Dlq_Dte  [SIZ_TS_DLQ_DTE      ];
   char  Ase_Nme      [SIZ_TS_ASE_NME      ];
   char  Filler       [SIZ_TS_FILLER       ];
   char  CrLf[2];
} ORG_SUPP;

// TC04TY30.TXT
#define  ORG_TD_APN           0
#define  ORG_TD_CODE          1
#define  ORG_TD_AGENCY        2
#define  ORG_TD_TRA           3
#define  ORG_TD_RATE          4
#define  ORG_TD_AMOUNT        5
#define  ORG_TD_FLDS          6

// Standard codes
// H=Homeowners
// D=Disabled Veterans
// V=Veterans
// C=Church
// L=Library
// M=Museums
// R=Religion
// T=Tribal Housing
// W=Welfare
// U=College
// P=Public School
// E=Cemetery
// I=Hospital
// S=School
// X=Misc exempt
// Y=Military
// Z=Full exemption

IDX_TBL4 ORG_Exemption[] = 
{
   "0", "V", 1,1,    // Unknown
   "1", "V", 1,1,
   "2", "C", 1,1,    // CHURCH
   "3", "R", 1,1,    // RELIGIOUS
   "4", "U", 1,1,    // COLLEGE
   "5", "W", 1,1,    // WELFARE
   "6", "X", 1,1,    // OTHER
   "7", "H", 1,1,    // HOMEOWNER
   "8", "D", 1,1,    // DISABLED VETERAN
   "X", "Z", 1,1,    // WHOLLY EXEMPT
   "","",0,0
};

#endif