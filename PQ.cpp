/*****************************************************************************
 *
 * Notes: 
 *    - This module is for lien extract related functions
 *    - Merge lien value to 1900-byte record
 *    - Fix APN, TRA, ... in R01 file
 *
 * 01/28/2009 Modify PQ_FixR01() to add option to clear characteristics from R01 file.
 * 01/29/2009 Fix PQ_MergeLienRec() by reformating TRA value.
 * 02/03/2009 Modify PQ_MergeOthers() to keep original file as .T01
 * 10/29/2010 Modify PQ_FixR01() to add option to remove saleamt < 10.
 * 12/21/2010 Modify PQ_FixR01() to add option to fix Full Exemption flag
 * 04/25/2012 Modify PQ_MergeLienRec() to update ExeAmt and PQ_MergeOtherRec()
 *            to update EXE_CD? from LDR extract file.
 * 12/19/2012 Modify PQ_ChkBadR01() to check all 3 files for LAX.
 * 04/11/2013 Add bSetFlg to PQ_MergeLien() to allow caller to reset EXEAMT
 *            if LDR roll didn't have it.
 * 07/01/2013 Fix bug in PQ_MergeOtherRec() which turns ON FullExe flag even when 
 *            it is blank (STA). Check last nibble only.
 * 10/28/2013 Increase buffer length to make it safe working in 64-bit system.
 * 08/01/2017 Add PQ_MergePrevApn()
 * 08/24/2018 Add PQ_MergeLotArea() to replace MergeArea() in R01.cpp & MergeLotArea() in LoadOne.cpp
 * 09/29/2018 Modify sort command in PQ_MergeLotArea() to OMIT blank APN
 * 11/04/2018 Modify PQ_FixR01() to add option to remove all sales from R01 record.
 * 12/15/2018 Modify PQ_MergeOtherRec() to reset all values to blank of not available.
 * 01/10/2019 Modify PQ_MergeLienRec() to populate FIXTR & PERSPROP
 * 01/22/2019 Modify PQ_MergeOtherRec() to remove FIXTR & PERSPROP since it's already done in PQ_MergeLienRec()
 * 01/23/2019 Replace generic SIZ_OTH_VALUE with separate one for each field.
 * 06/20/2019 Add PQ_FixBadNames() & PQ_FixName() to remove known bad char from owner name.
 *            Modify PQ_FixBadChar() to call PQ_FixBadNames() as part of its cleanup process.
 * 06/25/2019 Modify PQ_FixBadNames() to fix DBA and CAREOF too.
 * 06/25/2019 Modify PQ_FixBadName() to replace double quote and backlask only, keep single quote.
 * 07/14/2020 Fix bug in PQ_MergeLotArea()
 * 10/29/2020 Add PQ_SetDefaultPQZoning() to set default PQZoning.
 * 10/12/2020 Modify PQ_SetPQZoning() to remove PQZoning before copy county Zoning over.
 * 05/08/2021 Modify PQ_FixR01() to fix FirePlace by setting FirePlace='M' when FP > 9.
 * 06/02/2021 Modify PQ_MergeLotArea() to support both input formats area file.
 * 06/25/2021 Modify PQ_FixBadNames() to remove bad char in NAME_SWAP too.
 * 06/30/2021 Remove extra space in PQ_FixName().
 * 07/16/2021 Increase read buffer in PQ_MergeLotArea() from 200 to 512.
 * 07/27/2021 Modify PQ_MergeLotArea() to handle various layout of area files.
 * 09/30/2021 Fix LotArea in PQ_MergeLotArea().
 * 02/25/2022 Add PQ_MergeValueExt().
 * 07/14/2022 Add PQ_CopyOldR01() to copy data from old O01 to new R01 record.
 * 08/17/2022 Populate Exe_code, HO_Flg, and FullExe_Flg
 * 11/11/2022 Add PQ_RemoveQZoning().
 * 11/27/2022 Add PQ_RemoveValues().
 * 12/05/2022 Modify PQ_MergeOtherRec() to put LivingImpr on OTH_IMPR instead of TREEVINES in R01 record.
 *            Modify PQ_MergeValueRec() to support MB & RIV.
 * 12/07/2022 Modify PQ_MergeOtherRec() for MEN.
 * 12/08/2022 Modify PQ_MergeOtherRec() for SUT.
 * 01/23/2023 Modify PQ_MergeValueExt() to return 0 if success.
 * 01/31/2023 Modify PQ_FixR01() to add option PQ_REM_XFER in PQ_FixR01() to remove all sales and transfer.
 * 07/09/2023 Modify PQ_MergeLotArea() to merge only if both LotSqft and LotAcres are not on R01 record.
 * 09/14/2023 Modify PQ_FixName() to remove single quote.
 * 10/29/2023 Modify PQ_MergeLotArea() to standardize two type of lot area file.  If the file is
 *            defined in INI section, it's no need to resort.
 * 01/03/2024 Modify PQ_MergeValueRec() to set HO_Flg only if Gross > 0
 * 03/27/2024 Modify PQ_MergeLotArea() to allow customization of LotArea file.
 * 05/22/2024 Add PQ_UpdateBpp() to update PP values using BPP extracted from roll update.
 * 07/09/2024 Modify PQ_MergeLotArea() to use .DAT when .TXT or .CSV not available.
 * 07/17/2024 Modify PQ_CopyOldR01() to add option co copy county Zoning.
 * 10/02/2024 Modify PQ_MergeLienRec() to bypass merge other value for MEN.
 * 10/30/2024 Rename PQ_RemoveQZoning() to PQ_RemovePQZoning().
 *
 *****************************************************************************/

#include "stdafx.h"
#include "Prodlib.h"
#include "R01.h"
#include "Recdef.h"
#include "CountyInfo.h"
#include "FormatApn.h"
#include "Utils.h"
#include "DoSort.h"
#include "Logs.h"
#include "PQ.h"

extern char  acRawTmpl[], acLienTmpl[], acValueTmpl[], acIniFile[], acTmpPath[];
extern int   iRecLen, iApnLen, iNoMatch;
extern long  lRecCnt;
extern bool  bDebug;
extern void  ClearOldSale(char *pOutbuf, bool bDelXfer=false);

/******************************* PQ_MergeOtherRec *****************************
 *
 * Merge uncommon fields in LIENEXT record
 *
 ******************************************************************************/

void PQ_MergeOtherRec(char *pOutbuf, char *pLienRec, int iGrp)
{
   long  lTmp;
   char  acTmp[_MAX_PATH];

   LIENEXTR *pLien = (LIENEXTR *)pLienRec;

   if (!iGrp)
      return;

   // Fixture
   //lTmp = atoin(pLien->acME_Val, SIZ_LIEN_FIXT);
   //if (lTmp > 0)
   //{
   //   sprintf(acTmp, "%d         ", lTmp);
   //   memcpy(pOutbuf+OFF_FIXTR, acTmp, SIZ_FIXTR);
   //} else
   //   memset(pOutbuf+OFF_FIXTR, ' ', SIZ_FIXTR);


   // PP Value
   //lTmp = atoin(pLien->acPP_Val, SIZ_LIEN_FIXT);
   //if (lTmp > 0)
   //{
   //   sprintf(acTmp, "%d         ", lTmp);
   //   memcpy(pOutbuf+OFF_PERSPROP, acTmp, SIZ_PERSPROP);
   //} else
   //   memset(pOutbuf+OFF_PERSPROP, ' ', SIZ_PERSPROP);

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "002006058", 9))
   //   lTmp = 0;
#endif

   // Full Exemption flag
   if (pLien->SpclFlag > ' ' && (pLien->SpclFlag & LX_FULLEXE_FLG & 0x0F))
      *(pOutbuf+OFF_FULL_EXEMPT) = 'Y';
   else
      *(pOutbuf+OFF_FULL_EXEMPT) = ' ';

   // Prop8 flag
   if (pLien->SpclFlag > ' ' && (pLien->SpclFlag & LX_PROP8_FLG & 0x0F))
      *(pOutbuf+OFF_PROP8_FLG) = 'Y';
   else
      *(pOutbuf+OFF_PROP8_FLG) = ' ';

   switch (iGrp)
   {
      case GRP_MB:
         // PP_MH Value
         lTmp = atoin(pLien->extra.MB.PP_MobileHome, SIZ_LIEN_FIXT);
         if (lTmp > 0)
         {
            sprintf(acTmp, "%d         ", lTmp);
            memcpy(pOutbuf+OFF_PP_MH, acTmp, SIZ_PP_MH);
         } else
            memset(pOutbuf+OFF_PP_MH, ' ', SIZ_PP_MH);

         // Bus Inventory or Bus PP or Household PP
         lTmp = atoin(pLien->extra.MB.BusProp, SIZ_LIEN_FIXT);
         if (lTmp > 0)
         {
            sprintf(acTmp, "%d         ", lTmp);
            memcpy(pOutbuf+OFF_BUSINV, acTmp, SIZ_BUSINV);
         } else
            memset(pOutbuf+OFF_BUSINV, ' ', SIZ_BUSINV);

         // Growing impr
         lTmp = atoin(pLien->extra.MB.GrowImpr, SIZ_LIEN_FIXT);
         if (lTmp > 0)
         {
            sprintf(acTmp, "%d         ", lTmp);
            memcpy(pOutbuf+OFF_GR_IMPR, acTmp, SIZ_GR_IMPR);
         } else
            memset(pOutbuf+OFF_GR_IMPR, ' ', SIZ_GR_IMPR);

         // EXE Code
         memcpy(pOutbuf+OFF_EXE_CD1, pLien->extra.MB.ExeCode1, SIZ_LIEN_EXECODEX);
         memcpy(pOutbuf+OFF_EXE_CD2, pLien->extra.MB.ExeCode2, SIZ_LIEN_EXECODEX);
         memcpy(pOutbuf+OFF_EXE_CD3, pLien->extra.MB.ExeCode3, SIZ_LIEN_EXECODEX);

         break;

      case GRP_CRES:
         // MH
         lTmp = atoin(pLien->extra.Cres.PP_MobileHome, SIZ_LIEN_FIXT);
         if (lTmp > 0)
         {
            sprintf(acTmp, "%d         ", lTmp);
            memcpy(pOutbuf+OFF_PP_MH, acTmp, SIZ_PP_MH);
         } else
            memset(pOutbuf+OFF_PP_MH, ' ', SIZ_PP_MH);
         // Lease val
         lTmp = atoin(pLien->extra.Cres.LeaseVal, SIZ_LIEN_FIXT);
         if (lTmp > 0)
         {
            sprintf(acTmp, "%d         ", lTmp);
            memcpy(pOutbuf+OFF_GR_IMPR, acTmp, SIZ_GR_IMPR);
         } else
            memset(pOutbuf+OFF_GR_IMPR, ' ', SIZ_GR_IMPR);
         // Pers fixtr
         lTmp = atoin(pLien->extra.Cres.PersFixtr, SIZ_LIEN_FIXT);
         if (lTmp > 0)
         {
            sprintf(acTmp, "%d         ", lTmp);
            memcpy(pOutbuf+OFF_FIXTR_RP, acTmp, SIZ_FIXTR_RP);
         } else
            memset(pOutbuf+OFF_FIXTR_RP, ' ', SIZ_FIXTR_RP);
         break;

      case GRP_ALA:
         // CLCA market value
         lTmp = atoin(pLien->extra.Ala.CLCA_Impr, SIZ_LIEN_FIXT);
         if (lTmp > 0)
         {
            sprintf(acTmp, "%d         ", lTmp);
            memcpy(pOutbuf+OFF_CLCA_IMPR, acTmp, SIZ_CLCA_IMPR);
         } else
            memset(pOutbuf+OFF_CLCA_IMPR, ' ', SIZ_CLCA_IMPR);

         // CLCA land value
         lTmp = atoin(pLien->extra.Ala.CLCA_Land, SIZ_LIEN_FIXT);
         if (lTmp > 0)
         {
            sprintf(acTmp, "%d         ", lTmp);
            memcpy(pOutbuf+OFF_CLCA_LAND, acTmp, SIZ_CLCA_LAND);
         } else
            memset(pOutbuf+OFF_CLCA_LAND, ' ', SIZ_CLCA_LAND);

         // Hpp value
         lTmp = atoin(pLien->extra.Ala.Hpp, SIZ_LIEN_FIXT);
         if (lTmp > 0)
         {
            sprintf(acTmp, "%d         ", lTmp);
            memcpy(pOutbuf+OFF_BUSINV, acTmp, SIZ_BUSINV);
         } else
            memset(pOutbuf+OFF_BUSINV, ' ', SIZ_BUSINV);
         break;

      case GRP_FRE:
         // MH Value
         lTmp = atoin(pLien->extra.Fre.MH_Val, SIZ_LIEN_FIXT);
         if (lTmp > 0)
         {
            sprintf(acTmp, "%d         ", lTmp);
            memcpy(pOutbuf+OFF_PP_MH, acTmp, SIZ_PP_MH);
         } else
            memset(pOutbuf+OFF_PP_MH, ' ', SIZ_PP_MH);
         break;

      case GRP_KER:
         // Mineral right
         lTmp = atoin(pLien->extra.Ker.acMineral, SIZ_LIEN_FIXT);
         if (lTmp > 0)
         {
            sprintf(acTmp, "%d         ", lTmp);
            memcpy(pOutbuf+OFF_MINERAL, acTmp, SIZ_MINERAL);
         } else
            memset(pOutbuf+OFF_MINERAL, ' ', SIZ_MINERAL);
         break;

      case GRP_MEN:
         // Trees/Vines, LivingImpr (12/07/2022)
         lTmp = atoin(pLien->extra.Men.LivingImpr, SIZ_LIEN_FIXT);
         if (lTmp > 0)
         {
            sprintf(acTmp, "%d         ", lTmp);
            memcpy(pOutbuf+OFF_OTH_IMPR, acTmp, SIZ_OTH_IMPR);
         } else
            memset(pOutbuf+OFF_OTH_IMPR, ' ', SIZ_OTH_IMPR);

         // Williamson trees/vines
         lTmp = atoin(pLien->extra.Men.acWil_TV, SIZ_LIEN_FIXT);
         if (lTmp > 0)
         {
            sprintf(acTmp, "%d         ", lTmp);
            memcpy(pOutbuf+OFF_CLCA_IMPR, acTmp, SIZ_CLCA_IMPR);
         } else
            memset(pOutbuf+OFF_CLCA_IMPR, ' ', SIZ_CLCA_IMPR);

         // Williamson land
         lTmp = atoin(pLien->extra.Men.acWil_Land, SIZ_LIEN_FIXT);
         if (lTmp > 0)
         {
            sprintf(acTmp, "%d         ", lTmp);
            memcpy(pOutbuf+OFF_CLCA_LAND, acTmp, SIZ_CLCA_LAND);
         } else
            memset(pOutbuf+OFF_CLCA_LAND, ' ', SIZ_CLCA_LAND);
         break;

      case GRP_MNO:
         // MH Value
         lTmp = atoin(pLien->extra.Mno.MH_Val, SIZ_LIEN_FIXT);
         if (lTmp > 0)
         {
            sprintf(acTmp, "%d         ", lTmp);
            memcpy(pOutbuf+OFF_PP_MH, acTmp, SIZ_PP_MH);
         } else
            memset(pOutbuf+OFF_PP_MH, ' ', SIZ_PP_MH);
         break;

      case GRP_MPA:
         // Timber
         lTmp = atoin(pLien->extra.Mpa.Timber, SIZ_LIEN_FIXT);
         if (lTmp > 0)
         {
            sprintf(acTmp, "%d         ", lTmp);
            memcpy(pOutbuf+OFF_TIMBER_VAL, acTmp, SIZ_TIMBER_VAL);
         } else
            memset(pOutbuf+OFF_TIMBER_VAL, ' ', SIZ_TIMBER_VAL);

         // Tree/Vines
         lTmp = atoin(pLien->extra.Mpa.TV_Val, SIZ_LIEN_FIXT);
         if (lTmp > 0)
         {
            sprintf(acTmp, "%d         ", lTmp);
            memcpy(pOutbuf+OFF_TREEVINES, acTmp, SIZ_TREEVINES);
         } else
            memset(pOutbuf+OFF_TREEVINES, ' ', SIZ_TREEVINES);

         // Williamson market value
         lTmp = atoin(pLien->extra.Mpa.Wil_Mkt, SIZ_LIEN_FIXT);
         if (lTmp > 0)
         {
            sprintf(acTmp, "%d         ", lTmp);
            memcpy(pOutbuf+OFF_CLCA_IMPR, acTmp, SIZ_CLCA_IMPR);
         } else
            memset(pOutbuf+OFF_CLCA_IMPR, ' ', SIZ_CLCA_IMPR);

         // Williamson land value
         lTmp = atoin(pLien->extra.Mpa.Wil_Land, SIZ_LIEN_FIXT);
         if (lTmp > 0)
         {
            sprintf(acTmp, "%d         ", lTmp);
            memcpy(pOutbuf+OFF_CLCA_LAND, acTmp, SIZ_CLCA_LAND);
         } else
            memset(pOutbuf+OFF_CLCA_LAND, ' ', SIZ_CLCA_LAND);
         break;

      case GRP_MRN:
         lTmp = atoin(pLien->extra.Mrn.Bus_Inv, SIZ_LIEN_FIXT);
         if (lTmp > 0)
         {
            sprintf(acTmp, "%d         ", lTmp);
            memcpy(pOutbuf+OFF_BUSINV, acTmp, SIZ_BUSINV);
         } else
            memset(pOutbuf+OFF_BUSINV, ' ', SIZ_BUSINV);
         break;

      case GRP_ORG:
         // Other improve
         lTmp = atoin(pLien->extra.Org.OtherImpr, SIZ_LIEN_FIXT);
         if (lTmp > 0)
         {
            sprintf(acTmp, "%d         ", lTmp);
            memcpy(pOutbuf+OFF_OTH_IMPR, acTmp, SIZ_OTH_IMPR);
         } else
            memset(pOutbuf+OFF_OTH_IMPR, ' ', SIZ_OTH_IMPR);
         break;

      case GRP_RIV:
         // Tree/Vines, Living Impr
         lTmp = atoin(pLien->extra.Riv.LivingImpr, SIZ_LIEN_FIXT);
         if (lTmp > 0)
         {
            sprintf(acTmp, "%d         ", lTmp);
            memcpy(pOutbuf+OFF_OTH_IMPR, acTmp, SIZ_OTH_IMPR);
         } else
            memset(pOutbuf+OFF_OTH_IMPR, ' ', SIZ_OTH_IMPR);

         // Bus Inventory
         lTmp = atoin(pLien->extra.Riv.Bus_Inv, SIZ_LIEN_FIXT);
         if (lTmp > 0)
         {
            sprintf(acTmp, "%d         ", lTmp);
            memcpy(pOutbuf+OFF_BUSINV, acTmp, SIZ_BUSINV);
         } else
            memset(pOutbuf+OFF_BUSINV, ' ', SIZ_BUSINV);
         break;

      case GRP_SBX:
         // PP_MH
         lTmp = atoin(pLien->extra.Sbx.PP_MH, SIZ_LIEN_FIXT);
         if (lTmp > 0)
         {
            sprintf(acTmp, "%d         ", lTmp);
            memcpy(pOutbuf+OFF_PP_MH, acTmp, SIZ_PP_MH);
         } else
            memset(pOutbuf+OFF_PP_MH, ' ', SIZ_PP_MH);
         // Living Impr
         lTmp = atoin(pLien->extra.Sbx.LivImpr, SIZ_LIEN_FIXT);
         if (lTmp > 0)
         {
            sprintf(acTmp, "%d         ", lTmp);
            memcpy(pOutbuf+OFF_GR_IMPR, acTmp, SIZ_GR_IMPR);
         } else
            memset(pOutbuf+OFF_GR_IMPR, ' ', SIZ_GR_IMPR);
         // Unit value
         /* This value has been added to PERSPROP 20091001 -sn
         lTmp = atoin(pLien->extra.Sbx.UnitVal, SIZ_LIEN_FIXT);
         if (lTmp > 0)
         {
            sprintf(acTmp, "%d         ", lTmp);
            memcpy(pOutbuf+OFF_OTH_IMPR, acTmp, SIZ_OTH_IMPR);
         }
         */
         break;

      case GRP_SJX:
         // Homesite
         lTmp = atoin(pLien->extra.Sjx.HomeSite, SIZ_LIEN_FIXT);
         if (lTmp > 0)
         {
            sprintf(acTmp, "%d         ", lTmp);
            memcpy(pOutbuf+OFF_HOMESITE, acTmp, SIZ_HOMESITE);
         } else
            memset(pOutbuf+OFF_HOMESITE, ' ', SIZ_HOMESITE);

         // Trees/Vines
         lTmp = atoin(pLien->extra.Sjx.Tree, SIZ_LIEN_FIXT);
         if (lTmp > 0)
         {
            sprintf(acTmp, "%d         ", lTmp);
            memcpy(pOutbuf+OFF_TREEVINES, acTmp, SIZ_TREEVINES);
         } else
            memset(pOutbuf+OFF_TREEVINES, ' ', SIZ_TREEVINES);
         break;

      case GRP_SOL:
         // Tree/Vines
         lTmp = atoin(pLien->extra.Sol.Tree, SIZ_LIEN_FIXT);
         if (lTmp > 0)
         {
            sprintf(acTmp, "%d         ", lTmp);
            memcpy(pOutbuf+OFF_TREEVINES, acTmp, SIZ_TREEVINES);
         } else
            memset(pOutbuf+OFF_TREEVINES, ' ', SIZ_TREEVINES);
         // Mineral right
         lTmp = atoin(pLien->extra.Sol.Mineral, SIZ_LIEN_LAND);
         if (lTmp > 0)
         {
            sprintf(acTmp, "%d         ", lTmp);
            memcpy(pOutbuf+OFF_MINERAL, acTmp, SIZ_MINERAL);
         } else
            memset(pOutbuf+OFF_MINERAL, ' ', SIZ_MINERAL);
         break;

      case GRP_SUT:
         // Tree/Vines
         lTmp = atoin(pLien->extra.Sut.LivingImpr, SIZ_LIEN_FIXT);
         if (lTmp > 0)
         {
            sprintf(acTmp, "%d         ", lTmp);
            memcpy(pOutbuf+OFF_OTH_IMPR, acTmp, SIZ_OTH_IMPR);
         } else
            memset(pOutbuf+OFF_OTH_IMPR, ' ', SIZ_OTH_IMPR);
         break;

      case GRP_TUL:
         // GrowImpr
         lTmp = atoin(pLien->extra.Tul.GrowImpr, SIZ_LIEN_FIXT);
         if (lTmp > 0)
         {
            sprintf(acTmp, "%d         ", lTmp);
            memcpy(pOutbuf+OFF_GR_IMPR, acTmp, SIZ_GR_IMPR);
         } else
            memset(pOutbuf+OFF_GR_IMPR, ' ', SIZ_OTH_IMPR);

         // Bus Inventory
         lTmp = atoin(pLien->extra.Tul.Bus_Inv, SIZ_LIEN_FIXT);
         if (lTmp > 0)
         {
            sprintf(acTmp, "%d         ", lTmp);
            memcpy(pOutbuf+OFF_BUSINV, acTmp, SIZ_BUSINV);
         } else
            memset(pOutbuf+OFF_BUSINV, ' ', SIZ_BUSINV);
         break;

      case GRP_VEN:
         // Tree/Vines
         lTmp = atoin(pLien->extra.Ven.Tree, SIZ_LIEN_FIXT);
         if (lTmp > 0)
         {
            sprintf(acTmp, "%d         ", lTmp);
            memcpy(pOutbuf+OFF_TREEVINES, acTmp, SIZ_TREEVINES);
         } else
            memset(pOutbuf+OFF_TREEVINES, ' ', SIZ_TREEVINES);

         // Mineral right
         lTmp = atoin(pLien->extra.Ven.Mineral, SIZ_LIEN_LAND);
         if (lTmp > 0)
         {
            sprintf(acTmp, "%d         ", lTmp);
            memcpy(pOutbuf+OFF_MINERAL, acTmp, SIZ_MINERAL);
         } else
            memset(pOutbuf+OFF_MINERAL, ' ', SIZ_MINERAL);
         break;

      default:
         break;
   }

}

/******************************* PQ_MergeLienRec ******************************
 *
 *
 ******************************************************************************/

void PQ_MergeLienRec(char *pOutbuf, char *pLienRec, int iGrp, bool bSetFlg)
{
   LIENEXTR *pLien = (LIENEXTR *)pLienRec;
   long     lTmp;
   char     sTmp[32];

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "00122105", 8) )
   //   lTmp = 0;
#endif

   // HO exempt - Some MB counties do not have exemption in lien roll
   if (iGrp == GRP_MRN)
   {
      *(pOutbuf+OFF_HO_FL) = pLien->acHO[0];
      memcpy(pOutbuf+OFF_EXE_TOTAL, pLien->acExAmt, SIZ_EXE_TOTAL);
   } else
   {
      lTmp = atoin(pLien->acExAmt, SIZ_EXE_TOTAL);
      if (lTmp > 0)
      {
         *(pOutbuf+OFF_HO_FL) = pLien->acHO[0];
         memcpy(pOutbuf+OFF_EXE_TOTAL, pLien->acExAmt, SIZ_EXE_TOTAL);
      } else if (bSetFlg == true)
      {
         *(pOutbuf+OFF_HO_FL) = '2';
         memset(pOutbuf+OFF_EXE_TOTAL, ' ', SIZ_EXE_TOTAL);
      }
   }

   // Land
   memcpy(pOutbuf+OFF_LAND, pLien->acLand, SIZ_LAND);

   // Improve
   memcpy(pOutbuf+OFF_IMPR, pLien->acImpr, SIZ_IMPR);

   // Others
   memcpy(pOutbuf+OFF_OTHER, pLien->acOther, SIZ_OTHER);

   // Gross
   memcpy(pOutbuf+OFF_GROSS, pLien->acGross, SIZ_GROSS);

   // Ratio
   memcpy(pOutbuf+OFF_RATIO, pLien->acRatio, SIZ_RATIO);

   // Year
   memcpy(pOutbuf+OFF_YR_ASSD, pLien->acYear, SIZ_YR_ASSD);

   // Fixtr
   memcpy(pOutbuf+OFF_FIXTR, pLien->acME_Val, SIZ_FIXTR);

   // Persprop
   memcpy(pOutbuf+OFF_PERSPROP, pLien->acPP_Val, SIZ_PERSPROP);

   // 2024 - MEN doesn't need other info
   if (iGrp == GRP_MEN)
      return;

   // TRA
   if (pLien->acTRA[0] > ' ')
   {
      memcpy(sTmp, pLien->acTRA, SIZ_TRA);
      sTmp[SIZ_TRA] = 0;
      if (strchr(sTmp, '-'))
         remChar(sTmp, '-');
      lTmp = atol(sTmp);
      sprintf(sTmp, "%.6d", lTmp);
      memcpy(pOutbuf+OFF_TRA, sTmp, 6);
   }

   PQ_MergeOtherRec(pOutbuf, pLienRec, iGrp);
}

/******************************* PQ_ConvLienExt ******************************
 *
 * Convert Lien Extract from old to new format.
 * Return 0 if successful
 *
 *****************************************************************************/

int PQ_ConvLienExt(char *pCnty)
{
   char     *pTmp, acLienRec1[256], acLienRec[512];
   char     acOutFile[_MAX_PATH], acLienExtr[_MAX_PATH];

   LIENEXT1 *pLien1= (LIENEXT1 *)&acLienRec1[0];
   LIENEXTR *pLien = (LIENEXTR *)&acLienRec[0];
   FILE     *fdLien, *fdOut;
   long     iTmp, lCnt=0;

   // Setup file names
   sprintf(acOutFile, acLienTmpl, pCnty, "out");
   sprintf(acLienExtr, acLienTmpl, pCnty, pCnty);

   LogMsg0("Convert Lien Extract file %s", acLienExtr);

   // Open Lien extract file
   LogMsg("Open Lien extract file %s", acLienExtr);
   fdLien = fopen(acLienExtr, "r");
   if (fdLien == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", acLienExtr);
      return -1;
   }

   // Open Output file
   LogMsg("Open output file %s", acOutFile);
   fdOut = fopen(acOutFile, "w");
   if (fdOut == NULL)
   {
      LogMsg("***** Error creating output file: %s\n", acOutFile);
      return -2;
   }

   // Get first lien rec
   pTmp = fgets((char *)&acLienRec1[0], 256, fdLien);
   memset(acLienRec, ' ', sizeof(LIENEXTR));
   pLien->LF[0] = 10;
   pLien->LF[1] = 0;

   // Merge loop
   while (pTmp)
   {
      // Move data from old record to new record
      memcpy(pLien->acApn, pLien1->acApn, sizeof(pLien1->acApn));
      memcpy(pLien->acExCode, pLien1->acExCode, sizeof(pLien1->acExCode));
      memcpy(pLien->acExAmt, pLien1->acExAmt, sizeof(pLien1->acExAmt));
      memcpy(pLien->acLand, pLien1->acLand, sizeof(pLien1->acLand));
      memcpy(pLien->acImpr, pLien1->acImpr, sizeof(pLien1->acImpr));
      memcpy(pLien->acPP_Val, pLien1->acPP_Val, sizeof(pLien1->acPP_Val));
      memcpy(pLien->acME_Val, pLien1->acME_Val, sizeof(pLien1->acME_Val));
      memcpy(pLien->acHO, pLien1->acHO, sizeof(pLien1->acHO));
      memcpy(pLien->acGross, pLien1->acGross, sizeof(pLien1->acGross));
      memcpy(pLien->acOther, pLien1->acOther, sizeof(pLien1->acOther));
      memcpy(pLien->acRatio, pLien1->acRatio, sizeof(pLien1->acRatio));
      memcpy(pLien->acTRA, pLien1->acTRA, sizeof(pLien1->acTRA));
      memcpy(pLien->acYear, pLien1->acYear, sizeof(pLien1->acYear));

      fputs(acLienRec, fdOut);

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);

      // Read next roll record
      pTmp = fgets(acLienRec1, 1024, fdLien);
   }

   // Close files
   if (fdLien)
      fclose(fdLien);
   if (fdOut)
      fclose(fdOut);

   // Rename output file
   iTmp = remove(acLienExtr);
   if (!iTmp)
   {
      iTmp = rename(acOutFile, acLienExtr);
      if (iTmp < 0)
      {
         LogMsgD("***** Error renaming file %s to %s", acOutFile, acLienExtr);
         iTmp = -1;
      }
   } else
   {
      LogMsgD("***** Error removing file %s", acLienExtr);
      iTmp = -1;
   }

   LogMsgD("\nTotal output records:       %u", lCnt);

   return iTmp;
}

/********************************* PQ_MergeLien ******************************
 *
 * Return 0 if successful, 1 if not matched, -1 if eof.
 *
 *****************************************************************************/

int PQ_MergeLien(char *pOutbuf, FILE *fd, int iGrp, bool bSetFlg)
{
   static   char  acRec[1024], *pRec=NULL;
   int      iLoop;
   LIENEXTR *pLienRec;

   // Get first Sale rec for first call
   if (!pRec)
      pRec = fgets(acRec, 1024, fd);
   pLienRec = (LIENEXTR *)&acRec[0];

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "003061006", 9))
   //   iLoop = 0;
#endif
   do
   {
      // Compare Apn
      iLoop = memcmp(pOutbuf, pLienRec->acApn, SIZ_LIEN_APN);
      if (iLoop > 0)
      {
         pRec = fgets(acRec, 1024, fd);
         if (!pRec)
         {
            fclose(fd);
            fd = NULL;
            return -1;      // EOF
         }
      }
   } while (iLoop > 0);

   if (iLoop)
      return 1;
   
   PQ_MergeLienRec(pOutbuf, acRec, iGrp, bSetFlg);

   pRec = fgets(acRec, 1024, fd);
   if (!pRec)
   {
      fclose(fd);
      fd = NULL;
   }

   return 0;
}

/********************************* PQ_MergeLien *******************************
 *
 *
 ******************************************************************************/

int PQ_MergeLienExt(char *pCnty, int iGrp, int iSkip)
{
   char     *pTmp, acBuf[MAX_RECSIZE], acLienRec[MAX_RECSIZE];
   char     cFileCnt=1;
   char     acRawFile[_MAX_PATH], acOutFile[_MAX_PATH], acLienExtr[_MAX_PATH];

   HANDLE   fhIn, fhOut;
   FILE     *fdLien;

   int      iTmp, iLienMatch=0, iLienDrop=0, iLienMiss=0;
   DWORD    nBytesRead, nBytesWritten;
   BOOL     bRet, bEof;
   long     lRet=0, lCnt=0;

   LogMsg0("Merge lien extract to current roll for %s", pCnty);

   sprintf(acRawFile, acRawTmpl, pCnty, pCnty, "R01");
   sprintf(acOutFile, acRawTmpl, pCnty, pCnty, "M01");

   // Rename files for processing
   if (_access(acRawFile, 0))
   {
      if (!_access(acOutFile, 0))
         rename(acOutFile, acRawFile);
      else
      {
         LogMsg("Missing input file %s.  Please recheck!", acRawFile);
         return -1;
      }
   }

   // Open Lien extract file
   sprintf(acLienExtr, acLienTmpl, pCnty, pCnty);
   LogMsg("Open Lien extract file %s", acLienExtr);
   fdLien = fopen(acLienExtr, "r");
   if (fdLien == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", acLienExtr);
      return -2;
   }

   // Open Input file
   LogMsg("Open input file %s", acRawFile);
   fhIn = CreateFile(acRawFile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhIn == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening raw file: %s\n", acRawFile);
      return -3;
   }

   // Open Output file
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error creating output file: %s\n", acOutFile);
      return -4;
   }

   // Get first lien rec
   pTmp = fgets((char *)&acLienRec[0], 1024, fdLien);
   bEof = (pTmp ? false:true);

   // Copy skip record
   memset(acBuf, ' ', iRecLen);
   while (iSkip-- > 0)
   {
      ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);
      WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
   }

   // Merge loop
   while (!bEof)
   {
      bRet = ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);

      // Check for EOF
      if (!bRet)
      {
         LogMsg("Error reading input file %s (%f)", acRawFile, GetLastError());
         break;
      }

      // EOF ?
      if (!nBytesRead)
         break;

NextLienRec:
      iTmp = memcmp(acBuf, acLienRec, iApnLen);
      if (!iTmp)
      {
         iLienMatch++;
         // Merge roll data
         PQ_MergeLienRec(acBuf, acLienRec, iGrp);

         // Read next roll record
         pTmp = fgets(acLienRec, 1024, fdLien);

         if (!pTmp)
            bEof = true;    // Signal to stop
      } else if (iTmp > 0)       
      {  // Roll not match, drop lien record?
         iLienDrop++;
         // Get next lien record
         pTmp = fgets(acLienRec, 1024, fdLien);

         if (!pTmp)
            bEof = true;    // Signal to stop
         else
            goto NextLienRec;
      } else
      {
         iLienMiss++;
         LogMsg0("*** New rec? %.10s", acBuf);
      }

      WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);

      if (!bRet)
      {
         LogMsg("***** Error occurs: %d\n", GetLastError());
         lRet = -1;
         break;
      }
   }

   // Close files
   if (fdLien)
      fclose(fdLien);
   if (fhOut)
      CloseHandle(fhOut);
   if (fhIn)
      CloseHandle(fhIn);

   // Rename output file
   iTmp = remove(acRawFile);
   if (!iTmp)
   {
      iTmp = rename(acOutFile, acRawFile);
      if (iTmp < 0)
      {
         LogMsgD("***** Error renaming file %s to %s", acOutFile, acRawFile);
         iTmp = -1;
      }
   } else
   {
      LogMsgD("***** Error removing file %s", acRawFile);
      iTmp = -1;
   }

   LogMsg("Total output records:       %u", lCnt);
   LogMsg("Total records matched:      %u", iLienMatch);
   LogMsg("Total records missed:       %u", iLienMiss);
   LogMsg("Total lien records dropped: %u", iLienDrop);

   printf("\nTotal output records: %u", lCnt);

   lRecCnt = lCnt;
   return iTmp;
}

/********************************* PQ_UpdatePP ********************************
 *
 * Return 0 if successful, 1 if not matched, -1 if eof.
 *
 ******************************************************************************/

void PQ_UpdatePP(char *pOutbuf, char *pInrec, int iGrp)
{
   BPP_REC *pRec = (BPP_REC *)pInrec;
   ULONG    lLand, lImpr, lPP, lFixtr, lGrImpr, lOthers, lGross;
   char     sTmp[32];
   int      iTmp;

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "009619106", 9) )
   //   iTmp = 0;
#endif

   memset(pOutbuf+OFF_OTHER, ' ', SIZ_OTHER);
   memset(pOutbuf+OFF_FIXTR, ' ', SIZ_FIXTR);
   memset(pOutbuf+OFF_PERSPROP, ' ', SIZ_PERSPROP);
   memset(pOutbuf+OFF_GROSS, ' ', SIZ_GROSS);

   // Land
   lLand = atoln(pOutbuf+OFF_LAND, SIZ_LAND);

   // Improve
   lImpr = atoln(pOutbuf+OFF_IMPR, SIZ_IMPR);

   // Growing Improve
   lGrImpr = atoln(pOutbuf+OFF_GR_IMPR, SIZ_GR_IMPR);

   lFixtr = atoln(pRec->Fixtr, SIZ_LIEN_FIXT);
   if (lFixtr > 0)
   {
      iTmp = sprintf(sTmp, "%lu", lFixtr);
      memcpy(pOutbuf+OFF_FIXTR, sTmp, iTmp);
   }
   lPP = atoln(pRec->PersProp, SIZ_LIEN_FIXT);
   if (lPP > 0)
   {
      iTmp = sprintf(sTmp, "%lu", lPP);
      memcpy(pOutbuf+OFF_PERSPROP, sTmp, iTmp);
   }

   // Others
   lOthers = lFixtr + lPP + lGrImpr;
   iTmp = sprintf(sTmp, "%lu", lOthers);
   memcpy(pOutbuf+OFF_OTHER, sTmp, iTmp);

   // Gross
   lGross = lOthers + lLand + lImpr;
   iTmp = sprintf(sTmp, "%lu", lGross);
   memcpy(pOutbuf+OFF_GROSS, sTmp, iTmp);

   // Ratio
   if (lImpr > 0)
   {
      sprintf(sTmp, "%*u", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
      memcpy(pOutbuf+OFF_RATIO, sTmp, SIZ_RATIO);
   }
}

/********************************* PQ_UpdateBpp *******************************
 *
 * Update PP from BPP extract
 * Return 0 if successful.
 *
 ******************************************************************************/

int PQ_UpdateBpp(char *pCnty, int iGrp, int iSkip)
{
   char     *pTmp, acBuf[MAX_RECSIZE], acBppRec[MAX_RECSIZE];
   char     cFileCnt=1;
   char     acRawFile[_MAX_PATH], acOutFile[_MAX_PATH], acBppFile[_MAX_PATH];

   HANDLE   fhIn, fhOut;
   FILE     *fdBpp;

   int      iTmp, iBppMatch=0, iBppDrop=0, iBppMiss=0;
   DWORD    nBytesRead, nBytesWritten;
   BOOL     bRet, bEof;
   long     lRet=0, lCnt=0;

   LogMsg0("Merge PP extract to current roll for %s", pCnty);

   sprintf(acRawFile, acRawTmpl, pCnty, pCnty, "R01");
   sprintf(acOutFile, acRawTmpl, pCnty, pCnty, "M01");

   // Rename files for processing
   if (_access(acRawFile, 0))
   {
      if (!_access(acOutFile, 0))
         rename(acOutFile, acRawFile);
      else
      {
         LogMsg("Missing input file %s.  Please recheck!", acRawFile);
         return -1;
      }
   }

   // Open Bpp extract file
   GetIniString("Data", "BppFile", ".\\", acBppRec, _MAX_PATH, acIniFile);
   sprintf(acBppFile, acBppRec, pCnty, pCnty);

   LogMsg("Open Bpp extract file %s", acBppFile);
   fdBpp = fopen(acBppFile, "r");
   if (fdBpp == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", acBppFile);
      return -2;
   }

   // Open Input file
   LogMsg("Open input file %s", acRawFile);
   fhIn = CreateFile(acRawFile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhIn == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening raw file: %s\n", acRawFile);
      return -3;
   }

   // Open Output file
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error creating output file: %s\n", acOutFile);
      return -4;
   }

   // Get first Bpp rec
   pTmp = fgets((char *)&acBppRec[0], 1024, fdBpp);
   bEof = (pTmp ? false:true);

   // Copy skip record
   memset(acBuf, ' ', iRecLen);
   while (iSkip-- > 0)
   {
      ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);
      WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
   }

   // Merge loop
   while (!bEof)
   {
      bRet = ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);

      // Check for EOF
      if (!bRet)
      {
         LogMsg("Error reading input file %s (%f)", acRawFile, GetLastError());
         break;
      }

      // EOF ?
      if (!nBytesRead)
         break;

NextBppRec:
      iTmp = memcmp(acBuf, acBppRec, iApnLen);
      if (!iTmp)
      {
         iBppMatch++;

         // Merge PP data
         PQ_UpdatePP(acBuf, acBppRec, iGrp);

         // Read next PP record
         pTmp = fgets(acBppRec, 1024, fdBpp);

         if (!pTmp)
            bEof = true;    // Signal to stop
      } else if (iTmp > 0)       
      {  // Roll not match, drop Bpp record?
         iBppDrop++;
         // Get next Bpp record
         pTmp = fgets(acBppRec, 1024, fdBpp);

         if (!pTmp)
            bEof = true;    // Signal to stop
         else
            goto NextBppRec;
      }

      WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);

      if (!bRet)
      {
         LogMsg("***** Error occurs: %d\n", GetLastError());
         lRet = -1;
         break;
      }
   }

   // Close files
   if (fdBpp)
      fclose(fdBpp);
   if (fhOut)
      CloseHandle(fhOut);
   if (fhIn)
      CloseHandle(fhIn);

   // Rename output file
   iTmp = remove(acRawFile);
   if (!iTmp)
   {
      iTmp = rename(acOutFile, acRawFile);
      if (iTmp < 0)
      {
         LogMsgD("***** Error renaming file %s to %s", acOutFile, acRawFile);
         iTmp = -1;
      }
   } else
   {
      LogMsgD("***** Error removing file %s", acRawFile);
      iTmp = -1;
   }

   LogMsg("Total output records:       %u", lCnt);
   LogMsg("Total records matched:      %u", iBppMatch);
   LogMsg("Total records missed:       %u", iBppMiss);
   LogMsg("Total Bpp records dropped: %u", iBppDrop);

   lRecCnt = lCnt;
   return iTmp;
}

/******************************* PQ_RemoveValues ******************************
 *
 * 11/21/2022 Remove all values and exemption
 *
 ******************************************************************************/

void PQ_RemoveValues(char *pOutbuf)
{
   // Exemption
   memset(pOutbuf+OFF_EXE_TOTAL, ' ', SIZ_EXE_TOTAL);

   // Exemption code
   memset(pOutbuf+OFF_EXE_CD1, ' ', SIZ_EXE_CD1);
   memset(pOutbuf+OFF_EXE_CD2, ' ', SIZ_EXE_CD1);
   memset(pOutbuf+OFF_EXE_CD3, ' ', SIZ_EXE_CD1);

   // HOX Flag
   *(pOutbuf+OFF_HO_FL) = ' ';

   // Land
   memset(pOutbuf+OFF_LAND, ' ', SIZ_LAND);

   // Improve
   memset(pOutbuf+OFF_IMPR, ' ', SIZ_IMPR);

   // CLCA Land
   memset(pOutbuf+OFF_CLCA_LAND, ' ', SIZ_CLCA_LAND);

   // CLCA Impr
   memset(pOutbuf+OFF_CLCA_IMPR, ' ', SIZ_CLCA_IMPR);

   // Others
   memset(pOutbuf+OFF_OTHER, ' ', SIZ_OTHER);

   // Gross
   memset(pOutbuf+OFF_GROSS, ' ', SIZ_GROSS);

   // Year
   memset(pOutbuf+OFF_YR_ASSD, ' ', SIZ_YR_ASSD);

   // Fixtr
   memset(pOutbuf+OFF_FIXTR, ' ', SIZ_FIXTR);

   // Persprop
   memset(pOutbuf+OFF_PERSPROP, ' ', SIZ_PERSPROP);

   // Growing Impr
   memset(pOutbuf+OFF_GR_IMPR, ' ', SIZ_GR_IMPR);

   // Other Impr
   memset(pOutbuf+OFF_OTH_IMPR, ' ', SIZ_OTH_IMPR);

   // Fixtr_RP
   memset(pOutbuf+OFF_FIXTR_RP, ' ', SIZ_FIXTR_RP);

   // Mineral
   memset(pOutbuf+OFF_MINERAL, ' ', SIZ_MINERAL);

   // Homesite
   memset(pOutbuf+OFF_HOMESITE, ' ', SIZ_HOMESITE);

   // Timber value
   memset(pOutbuf+OFF_TIMBER_VAL, ' ', SIZ_TIMBER_VAL);

   // Treevines
   memset(pOutbuf+OFF_TREEVINES, ' ', SIZ_TREEVINES);

   // Bus_PP or Business inventory
   memset(pOutbuf+OFF_BUSINV, ' ', SIZ_BUSINV);

   // Household PP  (ALA)
   memset(pOutbuf+OFF_HOUSEHOLD_PP, ' ', SIZ_HOUSEHOLD_PP);

}

/******************************* PQ_MergeValueRec ******************************
 *
 * 08/17/2022 Populate Exe_code, HO_Flg, and FullExe_Flg
 * 01/03/2024 Set HO_Flg only if Gross > 0
 *
 ******************************************************************************/

void PQ_MergeValueRec(char *pOutbuf, char *pLienRec, int iGrp)
{
   LIENEXTR *pLien = (LIENEXTR *)pLienRec;
   ULONG    lGross, lTotalExe;

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "002660005", 9) )
   //   lGross = 0;
#endif

   // Cleanup old values
   PQ_RemoveValues(pOutbuf);

   // Exemption
   memcpy(pOutbuf+OFF_EXE_TOTAL, pLien->acExAmt, SIZ_EXE_TOTAL);
   lTotalExe = atoln(pLien->acExAmt, SIZ_EXE_TOTAL);
   lGross = atoln(pLien->acGross, SIZ_GROSS);
   if (lTotalExe == lGross)
       *(pOutbuf+OFF_FULL_EXEMPT) = 'Y';

   // Exemption code
   memcpy(pOutbuf+OFF_EXE_CD1, pLien->acExCode, SIZ_LIEN_EXECODE);

   // HOX Flag
   if (lGross > 0)
      *(pOutbuf+OFF_HO_FL) = pLien->acHO[0];

   // Land
   memcpy(pOutbuf+OFF_LAND, pLien->acLand, SIZ_LAND);

   // Improve
   memcpy(pOutbuf+OFF_IMPR, pLien->acImpr, SIZ_IMPR);

   // Others
   memcpy(pOutbuf+OFF_OTHER, pLien->acOther, SIZ_OTHER);

   // Gross
   memcpy(pOutbuf+OFF_GROSS, pLien->acGross, SIZ_GROSS);

   // Year
   memcpy(pOutbuf+OFF_YR_ASSD, pLien->acYear, SIZ_YR_ASSD);

   // Fixtr
   memcpy(pOutbuf+OFF_FIXTR, pLien->acME_Val, SIZ_FIXTR);

   // Persprop
   memcpy(pOutbuf+OFF_PERSPROP, pLien->acPP_Val, SIZ_PERSPROP);

   switch (iGrp)
   {
      case GRP_MB:
         // Persprop MH
         memcpy(pOutbuf+OFF_PP_MH, pLien->extra.MB.PP_MobileHome, SIZ_PP_MH);

         // Fixtr_RP
         memcpy(pOutbuf+OFF_FIXTR_RP, pLien->extra.MB.FixtureRP, SIZ_FIXTR_RP);

         // Grow Improve
         memcpy(pOutbuf+OFF_GR_IMPR, pLien->extra.MB.GrowImpr, SIZ_GR_IMPR);
         break;

      case GRP_MEN:
         // Living Improve
         memcpy(pOutbuf+OFF_OTH_IMPR, pLien->extra.Men.LivingImpr, SIZ_OTH_IMPR);
         break;

      case GRP_RIV:
         // Living Improve
         memcpy(pOutbuf+OFF_OTH_IMPR, pLien->extra.Riv.LivingImpr, SIZ_OTH_IMPR);
         break;

      case GRP_SUT:
         // Living Improve
         memcpy(pOutbuf+OFF_OTH_IMPR, pLien->extra.Sut.LivingImpr, SIZ_OTH_IMPR);
         break;
      default:
         break;
   }
}

/****************************** PQ_MergeValueExt ******************************
 *
 * If R01 file is not available, use S01.
 * Return 0 if success.
 *
 ******************************************************************************/

int PQ_MergeValueExt(char *pCnty, int iGrp, int iSkip)
{
   char     *pTmp, acBuf[MAX_RECSIZE], acLienRec[MAX_RECSIZE], acValueFile[_MAX_PATH],
            acRawFile[_MAX_PATH], acOutFile[_MAX_PATH];
   char     cFileCnt=1;

   HANDLE   fhIn, fhOut;
   FILE     *fdValue;

   int      iTmp, iLienMatch=0, iLienDrop=0, iLienMiss=0;
   DWORD    nBytesRead, nBytesWritten;
   BOOL     bRet, bEof;
   long     lRet=0, lCnt=0;

   LogMsg0("Merge value extract to current roll for %s", pCnty);

   sprintf(acOutFile, acRawTmpl, pCnty, pCnty, "V01");

   // Open value extract file
   sprintf(acValueFile, acValueTmpl, pCnty, pCnty);
   LogMsg("Open Value extract file %s", acValueFile);
   fdValue = fopen(acValueFile, "r");
   if (fdValue == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", acValueFile);
      return -2;
   }

   // Open Input file
   sprintf(acRawFile, acRawTmpl, pCnty, pCnty, "R01");
   if (_access(acRawFile, 0))
      sprintf(acRawFile, acRawTmpl, pCnty, pCnty, "S01");

   LogMsg("Open input file %s", acRawFile);
   fhIn = CreateFile(acRawFile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhIn == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening raw file: %s\n", acRawFile);
      return -3;
   }

   // Open Output file
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error creating output file: %s\n", acOutFile);
      return -4;
   }

   // Get first lien rec
   pTmp = fgets((char *)&acLienRec[0], 1024, fdValue);
   bEof = (pTmp ? false:true);

   // Copy skip record
   memset(acBuf, ' ', iRecLen);
   while (iSkip-- > 0)
   {
      ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);
      WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
   }

   // Merge loop
   while (!bEof)
   {
      bRet = ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);

      // Check for EOF
      if (!bRet)
      {
         LogMsg("Error reading input file %s (%f)", acRawFile, GetLastError());
         break;
      }

      // EOF ?
      if (!nBytesRead)
      {
         // EOF
         cFileCnt++;
         acRawFile[strlen(acRawFile)-1] = cFileCnt | 0x30;
         acOutFile[strlen(acOutFile)-1] = cFileCnt | 0x30;
         CloseHandle(fhIn);
         CloseHandle(fhOut);
         fhOut = 0;
         fhIn = 0;

         // If no more file, get out
         if (_access(acRawFile, 0))
            break;

         // Open next Input file
         LogMsg("Open input file %s", acRawFile);
         fhIn = CreateFile(acRawFile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
               FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

         if (fhIn == INVALID_HANDLE_VALUE)
            break;
         bRet = ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);

         // Open Output file
         LogMsg("Open output file %s", acOutFile);
         fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
               FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);
         if (fhOut == INVALID_HANDLE_VALUE)
            break;
      }

NextLienRec:
      iTmp = memcmp(acBuf, acLienRec, iApnLen);
      if (!iTmp)
      {
         iLienMatch++;
         // Merge value
         PQ_MergeValueRec(acBuf, acLienRec, iGrp);

         // Read next roll record
         pTmp = fgets(acLienRec, 1024, fdValue);

         if (!pTmp)
            bEof = true;    // Signal to stop
      } else if (iTmp > 0)       
      {  // Roll not match, drop value record?
         iLienDrop++;
         if (bDebug)
            LogMsg("*** Unmatched value rec: %.14s", acLienRec);

         // Get next value record
         pTmp = fgets(acLienRec, 1024, fdValue);

         if (!pTmp)
            bEof = true;    // Signal to stop
         else
            goto NextLienRec;
      } else
      {
         iLienMiss++;
         if (bDebug)
            LogMsg0("*** Missing value rec %.14s", acBuf);
      }

      WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);

      if (!bRet)
      {
         LogMsg("***** Error occurs: %d\n", GetLastError());
         lRet = -1;
         break;
      }
   }

   // Close files
   if (fdValue)
      fclose(fdValue);
   if (fhOut)
      CloseHandle(fhOut);
   if (fhIn)
      CloseHandle(fhIn);

   LogMsg("Total output records:     %u", lCnt);
   LogMsg("      records matched:    %u", iLienMatch);
   LogMsg("      records missed:     %u", iLienMiss);
   LogMsg("      value recs dropped: %u", iLienDrop);

   printf("\nTotal output records: %u", lCnt);

   lRecCnt = lCnt;
   return 0;
}

/********************************* PQ_FixLienExt ******************************
 *
 * Insert leading zero to TRA.
 *
 ******************************************************************************/

int PQ_FixLienExt(char *pCnty)
{
   char     *pTmp, acLienRec[1024];
   char     acOutFile[_MAX_PATH], acLienExtr[_MAX_PATH];

   LIENEXTR *pLien = (LIENEXTR *)&acLienRec[0];
   FILE     *fdLien;
   HANDLE   fhOut;
   DWORD    nBytesWritten;
   BOOL     bRet;
   int      iTmp, iLen;
   long     lRet=0, lCnt=0;

   // Setup file names
   sprintf(acOutFile, acLienTmpl, pCnty, "out");
   sprintf(acLienExtr, acLienTmpl, pCnty, pCnty);

   LogMsgD("Fix Lien Extract file %s", acLienExtr);

   // Open Lien extract file
   LogMsg("Open Lien extract file %s", acLienExtr);
   fdLien = fopen(acLienExtr, "r");
   if (fdLien == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", acLienExtr);
      return 2;
   }

   // Open Output file
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error creating output file: %s\n", acOutFile);
      return 4;
   }

   // Get first lien rec
   pTmp = fgets((char *)&acLienRec[0], 1024, fdLien);
   iLen = sizeof(LIENEXTR);

   // Merge loop
   while (pTmp)
   {
      // Merge roll data
      memcpy(&pLien->acTRA[1], pLien->acTRA, 5);
      pLien->acTRA[0] = '0';

      bRet = WriteFile(fhOut, acLienRec, iLen, &nBytesWritten, NULL);
      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);

      if (!bRet)
      {
         LogMsg("***** Error occurs: %d\n", GetLastError());
         lRet = -1;
         break;
      }

      // Read next roll record
      pTmp = fgets(acLienRec, 1024, fdLien);
   }

   // Close files
   if (fdLien)
      fclose(fdLien);
   if (fhOut)
      CloseHandle(fhOut);

   if (!lRet)
   {
      // Rename output file
      iTmp = remove(acLienExtr);
      if (!iTmp)
      {
         iTmp = rename(acOutFile, acLienExtr);
         if (iTmp < 0)
         {
            LogMsgD("***** Error renaming file %s to %s", acOutFile, acLienExtr);
            iTmp = -1;
         }
      } else
      {
         LogMsgD("***** Error removing file %s", acLienExtr);
         iTmp = -1;
      }
   }

   LogMsg("Total output records:       %u", lCnt);
   printf("\nTotal output records: %u", lCnt);

   return iTmp;
}

/************************************ PQ_FixTRA *******************************
 *
 * Insert leading zero to TRA.
 *
 ******************************************************************************/

int PQ_FixTRA(char *pCnty)
{
   char     *pTmp, acLienRec[1024];
   char     acOutFile[_MAX_PATH], acLienExtr[_MAX_PATH];

   LIENEXTR *pLien = (LIENEXTR *)&acLienRec[0];
   FILE     *fdLien;
   HANDLE   fhOut;
   DWORD    nBytesWritten;
   BOOL     bRet;
   int      iTmp, iLen;
   long     lRet=0, lCnt=0;

   // Setup file names
   sprintf(acOutFile, acLienTmpl, pCnty, "out");
   sprintf(acLienExtr, acLienTmpl, pCnty, pCnty);

   LogMsgD("Fix Lien Extract file %s.  Correct TRA", acLienExtr);

   // Open Lien extract file
   LogMsg("Open Lien extract file %s", acLienExtr);
   fdLien = fopen(acLienExtr, "r");
   if (fdLien == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", acLienExtr);
      return 2;
   }

   // Open Output file
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error creating output file: %s\n", acOutFile);
      return 4;
   }

   // Get first lien rec
   pTmp = fgets((char *)&acLienRec[0], 1024, fdLien);
   iLen = sizeof(LIENEXTR);

   // Merge loop
   while (pTmp)
   {
      // Merge roll data
      memcpy(&pLien->acTRA[1], pLien->acTRA, 5);
      pLien->acTRA[0] = '0';

      bRet = WriteFile(fhOut, acLienRec, iLen, &nBytesWritten, NULL);
      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);

      if (!bRet)
      {
         LogMsg("***** Error occurs: %d\n", GetLastError());
         lRet = -1;
         break;
      }

      // Read next roll record
      pTmp = fgets(acLienRec, 1024, fdLien);
   }

   // Close files
   if (fdLien)
      fclose(fdLien);
   if (fhOut)
      CloseHandle(fhOut);

   if (!lRet)
   {
      // Rename output file
      iTmp = remove(acLienExtr);
      if (!iTmp)
      {
         iTmp = rename(acOutFile, acLienExtr);
         if (iTmp < 0)
         {
            LogMsgD("***** Error renaming file %s to %s", acOutFile, acLienExtr);
            iTmp = -1;
         }
      } else
      {
         LogMsgD("***** Error removing file %s", acLienExtr);
         iTmp = -1;
      }
   }

   LogMsg("Total output records:       %u", lCnt);
   printf("\nTotal output records: %u", lCnt);

   return iTmp;
}

/********************************* PQ_FixLienExt ******************************
 *
 * Adding trailing 0 to APN and/or insert leading zero to TRA.
 * Parameters:
 *    iFixTRA = number of 0 to be inserted
 *    iFixAPN = number of 0 to be added
 *
 ******************************************************************************/

int PQ_FixLienExt(char *pCnty, int iFixTRA, int iFixAPN)
{
   char     *pTmp, acLienRec[1024];
   char     acOutFile[_MAX_PATH], acLienExtr[_MAX_PATH];

   LIENEXTR *pLien = (LIENEXTR *)&acLienRec[0];
   FILE     *fdLien, *fdOut;
   int      iTmp, iLen, iApnOffset, iTraAdd;
   long     lCnt=0;

   // Setup file names
   sprintf(acOutFile, acLienTmpl, pCnty, "out");
   sprintf(acLienExtr, acLienTmpl, pCnty, pCnty);

   LogMsgD("Fix Lien Extract file %s.  Correcting APN", acLienExtr);

   // Open Lien extract file
   LogMsg("Open Lien extract file %s", acLienExtr);
   fdLien = fopen(acLienExtr, "r");
   if (fdLien == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", acLienExtr);
      return 2;
   }

   // Open Output file
   LogMsg("Open output file %s", acOutFile);
   fdOut = fopen(acOutFile, "w");
   if (fdOut == NULL)
   {
      LogMsg("***** Error creating output file: %s\n", acOutFile);
      return 4;
   }

   // Get first lien rec
   pTmp = fgets((char *)&acLienRec[0], 1024, fdLien);
   iLen = sizeof(LIENEXTR);
   iApnOffset = iApnLen - iFixAPN;
   iTraAdd = DEFAULT_TRA_LEN - iFixTRA;   // Number of bytes to keep

   // Merge loop
   while (pTmp)
   {
      // Fix data
      if (iFixTRA > 0)
      {
         memcpy(&pLien->acTRA[0], "0000", iFixTRA);
         memcpy(&pLien->acTRA[iFixTRA], pLien->acTRA, iTraAdd);
      }
      if (iFixAPN > 0)
         memcpy(&pLien->acApn[iApnOffset], "0000", iFixAPN);

      fputs(acLienRec, fdOut);
      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);

      // Read next roll record
      pTmp = fgets(acLienRec, 1024, fdLien);
   }

   // Close files
   if (fdLien)
      fclose(fdLien);
   if (fdOut)
      fclose(fdOut);

   // Rename output file
   iTmp = remove(acLienExtr);
   if (!iTmp)
   {
      iTmp = rename(acOutFile, acLienExtr);
      if (iTmp < 0)
      {
         LogMsgD("***** Error renaming file %s to %s", acOutFile, acLienExtr);
         iTmp = -1;
      }
   } else
   {
      LogMsgD("***** Error removing file %s", acLienExtr);
      iTmp = -1;
   }

   LogMsg("Total output records:       %u", lCnt);
   printf("\nTotal output records: %u", lCnt);

   return iTmp;
}

/******************************** PQ_ClearChars *******************************
 *
 *
 ******************************************************************************/

void PQ_ClearChars(char *pBuf)
{
   memset(pBuf+OFF_YR_BLT,    ' ', SIZ_YR_BLT);
   memset(pBuf+OFF_YR_EFF,    ' ', SIZ_YR_EFF);
   memset(pBuf+OFF_BLDG_SF,   ' ', SIZ_BLDG_SF);
   memset(pBuf+OFF_UNITS,     ' ', SIZ_UNITS);
   memset(pBuf+OFF_STORIES,   ' ', SIZ_STORIES);
   memset(pBuf+OFF_BEDS,      ' ', SIZ_BEDS);
   memset(pBuf+OFF_BATH_F,    ' ', SIZ_BATH_F);
   memset(pBuf+OFF_BATH_H,    ' ', SIZ_BATH_H);
   memset(pBuf+OFF_ROOMS,     ' ', SIZ_ROOMS);
   memset(pBuf+OFF_BLDG_CLASS,' ', SIZ_BLDG_CLASS+SIZ_BLDG_QUAL);
   memset(pBuf+OFF_IMPR_COND, ' ', SIZ_IMPR_COND);
   memset(pBuf+OFF_FIRE_PL,   ' ', SIZ_FIRE_PL);
   memset(pBuf+OFF_AIR_COND,  ' ', SIZ_AIR_COND);
   memset(pBuf+OFF_HEAT,      ' ', SIZ_HEAT);
   memset(pBuf+OFF_GAR_SQFT,  ' ', SIZ_GAR_SQFT);
   memset(pBuf+OFF_PARK_TYPE, ' ', SIZ_PARK_TYPE);
   memset(pBuf+OFF_PARK_SPACE,' ', SIZ_PARK_SPACE);
   memset(pBuf+OFF_VIEW,      ' ', SIZ_VIEW);
   memset(pBuf+OFF_WATER,     ' ', SIZ_WATER);
   memset(pBuf+OFF_SEWER,     ' ', SIZ_SEWER);
   memset(pBuf+OFF_POOL,      ' ', SIZ_POOL);
}

/*********************************** PQ_FixR01 ********************************
 *
 * Adding trailing 0 to APN and/or insert leading zero to TRA.
 * Parameters:
 *    iFixTRA = use DEFAULT_TRA_LEN
 *    iFixAPN = number of 0 to be added
 *
 * Input/Output:  R01 file
 *
 ******************************************************************************/

extern   COUNTY_INFO myCounty;
int PQ_FixR01(char *pCnty, int iFixOpts, int iFixLen, bool bRemDash, int iSkip)
{
   char     acBuf[MAX_RECSIZE], acTmp[_MAX_PATH];
   char     acOutFile[_MAX_PATH], acRawFile[_MAX_PATH];

   HANDLE   fhIn, fhOut;
   DWORD    nBytesRead, nBytesWritten;
   BOOL     bRet;
   int      iRet, iApnOffset, iTra, iTmp;
   long     lCnt=0;

   //LogMsg("***** Please review data before calling this function *****");
   //return -1;

   // Setup file names
   sprintf(acRawFile, acRawTmpl, pCnty, pCnty, "R01");
   sprintf(acOutFile, acRawTmpl, pCnty, pCnty, "TMP");

   if (_access(acRawFile, 0))
      sprintf(acRawFile, acRawTmpl, pCnty, pCnty, "S01");

   if ((iFixOpts & PQ_FIX_APN) && (iFixOpts & PQ_FIX_TRA))
      LogMsg0("Fix %s file for %s.  Correcting APN & TRA", acRawFile, pCnty);
   else if (iFixOpts & PQ_FIX_APN)
      LogMsg0("Fix %s file for %s.  Correcting APN", acRawFile, pCnty);
   else if (iFixOpts & PQ_FIX_TRA)
      LogMsg0("Fix %s file for %s.  Correcting TRA", acRawFile, pCnty);
   else if (iFixOpts & PQ_CLR_CHARS)
      LogMsg0("Fix %s file for %s.  Remove all characteristics", acRawFile, pCnty);
   else if (iFixOpts & PQ_REM_AMT9)
      LogMsg0("Fix %s file for %s.  Remove all hidden sale amounts", acRawFile, pCnty);
   else if (iFixOpts & PQ_FIX_FULLEXE)
      LogMsg0("Fix %s file for %s.  Correct Full Exemption flag", acRawFile, pCnty);
   else if (iFixOpts & (PQ_REM_SALES|PQ_REM_XFER))
      LogMsg0("Remove sales from %s for %s.", acRawFile, pCnty);
   else if (iFixOpts & PQ_FIX_FP)
      LogMsg0("Fix %s file for %s.  Fix FirePlace code", acRawFile, pCnty);
   else 
   {
      LogMsg("***** Please review data before calling this function PQ_FixR01() *****");
      return 0;
   }

   // Open R01 file
   LogMsg("Open R01 file %s", acRawFile);
   fhIn = CreateFile(acRawFile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhIn == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening input file: %s\n", acRawFile);
      return -3;
   }

   // Open Output file
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening output file: %s\n", acOutFile);
      return -4;
   }

   // Copy skip record
   bRet = true;
   memset(acBuf, ' ', iRecLen);
   while (iSkip-- > 0)
   {
      ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);
      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
   }

   iApnOffset = iApnLen - iFixLen;

   // Merge loop
   while (bRet)
   {
      bRet = ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);

      // Check for EOF
      if (!bRet)
      {
         LogMsg("Error reading input file %s (%f)", acRawFile, GetLastError());
         iRet = -1;
         break;
      }

      // EOF ?
      if (iRecLen != nBytesRead)
         break;

      // Fix data
      if (iFixOpts & PQ_FIX_TRA)
      {
         if (bRemDash)
         {
            memcpy(acTmp, &acBuf[OFF_TRA], DEFAULT_TRA_LEN);
            acTmp[DEFAULT_TRA_LEN] = 0;
            remChar(acTmp, '-');
            iTra = atol(acTmp);
         } else
            iTra = atoin(&acBuf[OFF_TRA], DEFAULT_TRA_LEN);
         sprintf(acTmp, "%.*d", DEFAULT_TRA_LEN, iTra);
         memcpy(&acBuf[OFF_TRA], acTmp, DEFAULT_TRA_LEN);
      }

      if (iFixOpts & PQ_FIX_APN)
      {
         memcpy(&acBuf[iApnOffset], "0000", iFixLen);
         // Format APN
         iRet = formatApn(acBuf, acTmp, &myCounty);
         memcpy(&acBuf[OFF_APN_D], acTmp, iRet);
      }

      if (iFixOpts & PQ_CLR_CHARS)
      {
         PQ_ClearChars(acBuf);
      }

      if (iFixOpts & PQ_REM_AMT9)
      {
         iTmp = atoin(&acBuf[OFF_SALE1_AMT], SIZ_SALE1_AMT);
         if (iTmp > 0 && iTmp < 10)
            memset(&acBuf[OFF_SALE1_AMT], ' ', SIZ_SALE1_AMT);

         iTmp = atoin(&acBuf[OFF_SALE2_AMT], SIZ_SALE2_AMT);
         if (iTmp > 0 && iTmp < 10)
            memset(&acBuf[OFF_SALE2_AMT], ' ', SIZ_SALE2_AMT);

         iTmp = atoin(&acBuf[OFF_SALE3_AMT], SIZ_SALE3_AMT);
         if (iTmp > 0 && iTmp < 10)
            memset(&acBuf[OFF_SALE3_AMT], ' ', SIZ_SALE3_AMT);
      }

      if (iFixOpts & PQ_FIX_FULLEXE)
      {
         long lGross = atoin(&acBuf[OFF_GROSS], SIZ_GROSS);
         long lExe   = atoin(&acBuf[OFF_EXE_TOTAL], SIZ_EXE_TOTAL);
         if (lExe >= lGross)
            acBuf[OFF_FULL_EXEMPT] = 'Y';
         else
            acBuf[OFF_FULL_EXEMPT] = ' ';
      }

      if (iFixOpts & PQ_REM_SALES)
         ClearOldSale(acBuf, false);
      else if (iFixOpts & PQ_REM_XFER)
         ClearOldSale(acBuf, true);

      // Fix Fireplace
      if (iFixOpts & PQ_FIX_FP)
      {
         iTmp = atoin(&acBuf[OFF_FIRE_PL], SIZ_FIRE_PL);
         memset(&acBuf[OFF_FIRE_PL], ' ', SIZ_FIRE_PL);
         if (iTmp > 9)
            acBuf[OFF_FIRE_PL] = 'M';
         else if (iTmp > 0)
            acBuf[OFF_FIRE_PL] = '0' | iTmp;
         else if (acBuf[OFF_FIRE_PL] > '0')
            LogMsg("*** Bad FirePlace %.2s [%.*s]", &acBuf[OFF_FIRE_PL], iApnLen, acBuf);
      }

      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }
   printf("\n");

   // Close files
   if (fhOut)
      CloseHandle(fhOut);
   if (fhIn)
      CloseHandle(fhIn);

   // Rename output file
   sprintf(acTmp, acRawTmpl, pCnty, pCnty, "T01");
   if (!_access(acTmp, 0))
      remove(acTmp);
   LogMsg("Rename %s to %s", acRawFile, acTmp);
   iRet = rename(acRawFile, acTmp);
   LogMsg("Rename %s to %s", acOutFile, acRawFile);
   iRet = rename(acOutFile, acRawFile);

   LogMsgD("Total output records:       %u", lCnt);

   return iRet;
}

/******************************** PQ_MergeOthers ******************************
 *
 *
 ******************************************************************************/

int PQ_MergeOthers(char *pCnty, char *pLienExt, int iGrp, int iSkip)
{
   char     *pTmp, acBuf[MAX_RECSIZE], acLienRec[MAX_RECSIZE];
   char     cFileCnt=1;
   char     acRawFile[_MAX_PATH], acOutFile[_MAX_PATH];

   HANDLE   fhIn, fhOut;
   FILE     *fdLien;

   int      iTmp, iLienMatch=0, iLienDrop=0, iLienMiss=0;
   DWORD    nBytesRead, nBytesWritten;
   BOOL     bRet, bEof;
   long     lRet=0, lCnt=0;

   sprintf(acRawFile, acRawTmpl, pCnty, pCnty, "R01");
   sprintf(acOutFile, acRawTmpl, pCnty, pCnty, "M01");

   // Rename files for processing
   if (_access(acRawFile, 0))
   {
      LogMsg("***** Missing input file %s.  Please recheck!", acRawFile);
      return 1;
   }

   // Open Lien extract file
   LogMsg("Open Lien extract file %s", pLienExt);
   fdLien = fopen(pLienExt, "r");
   if (fdLien == NULL)
   {
      LogMsg("***** Error opening Lien extract file: %s\n", pLienExt);
      return 2;
   }

   // Open Input file
   LogMsg("Open input file %s", acRawFile);
   fhIn = CreateFile(acRawFile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhIn == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening raw file: %s\n", acRawFile);
      return 3;
   }

   // Open Output file
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error creating output file: %s\n", acOutFile);
      return 4;
   }

   // Get first lien rec
   pTmp = fgets((char *)&acLienRec[0], 1024, fdLien);
   bEof = (pTmp ? false:true);

   // Copy skip record
   memset(acBuf, ' ', iRecLen);
   while (iSkip-- > 0)
   {
      ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);
      WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
   }

   // Merge loop
   while (!bEof)
   {
      bRet = ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);

      // Check for EOF
      if (!bRet)
      {
         LogMsg("Error reading input file %s (%f)", acRawFile, GetLastError());
         break;
      }

      // EOF ?
      if (iRecLen != nBytesRead)
      {
         CloseHandle(fhIn);
         CloseHandle(fhOut);
         fhIn = 0;
         fhOut = 0;

         bRet = DeleteFile(acRawFile);
         if (bRet)
         {
            LogMsg("Rename output file %s to %s", acOutFile, acRawFile);
            bRet = MoveFile(acOutFile, acRawFile);
            if (!bRet)
            {
               LogMsg("***** Error renaming file %s to %s", acOutFile, acRawFile);
               break;
            } 
         } else
         {
            LogMsg("***** Error removing file %s", acRawFile);
            break;
         }

         // Check for next input
         cFileCnt++;
         acRawFile[strlen(acRawFile)-1] = cFileCnt | 0x30;
         if (!_access(acRawFile, 0))
         {
            // Open next Input file
            LogMsg("Open input file %s", acRawFile);
            fhIn = CreateFile(acRawFile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
                  FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

            if (fhIn == INVALID_HANDLE_VALUE)
            {
               LogMsg("***** Error opening input file to merge Other values: %s\n", acRawFile);
               break;
            }
            bRet = ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);

            // Open Output file
            acOutFile[strlen(acRawFile)-1] = cFileCnt | 0x30;
            LogMsg("Open output file %s", acOutFile);
            fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
                  FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);
            if (fhOut == INVALID_HANDLE_VALUE)
            {
               LogMsg("***** Error opening output file to merge Other values: %s\n", acOutFile);
               break;
            }
         } else
            break;
      }

NextLienRec:
#ifdef _DEBUG
      //if (!memcmp(acBuf, "00060032", 8))
      //   iTmp = 0;
#endif
      iTmp = memcmp(acBuf, acLienRec, iApnLen);
      if (!iTmp)
      {
         iLienMatch++;

         // Merge others data
         PQ_MergeOtherRec(acBuf, acLienRec, iGrp);

         // Read next lien extract record
         pTmp = fgets(acLienRec, 1024, fdLien);

         if (!pTmp)
            bEof = true;    // Signal to stop
      } else if (iTmp > 0)       
      {  // Roll not match, drop lien record?
         iLienDrop++;
         // Get next lien record
         pTmp = fgets(acLienRec, 1024, fdLien);

         if (!pTmp)
            bEof = true;    // Signal to stop
         else
            goto NextLienRec;
      } else
      {
         iLienMiss++;
      }

      WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);

      if (!bRet)
      {
         LogMsg("***** Error occurs: %d\n", GetLastError());
         lRet = -1;
         break;
      }
   }

   // Close files
   if (fdLien)
      fclose(fdLien);
   if (fhIn)
      CloseHandle(fhIn);

   if (fhOut)
   {
      CloseHandle(fhOut);
      // Rename output file  
      iTmp = -1;

      sprintf(acBuf, acRawTmpl, pCnty, pCnty, "T01");
      if (!_access(acBuf, 0))
         bRet = DeleteFile(acBuf);
      bRet = MoveFile(acRawFile, acBuf);
      if (bRet)
      {
         bRet = MoveFile(acOutFile, acRawFile);
         if (!bRet)
            LogMsgD("***** Error renaming file %s to %s", acOutFile, acRawFile);
         else
            iTmp = 0;
      } else
         LogMsgD("***** Error removing file %s", acRawFile);
   }

   LogMsg("Total output records:       %u", lCnt);
   LogMsg("Total records matched:      %u", iLienMatch);
   LogMsg("Total records missed:       %u", iLienMiss);
   LogMsg("Total lien records dropped: %u", iLienDrop);

   printf("\nTotal output records: %u", lCnt);

   lRecCnt = lCnt;
   return iTmp;
}

/******************************** PQ_FixBadNames ******************************
 *
 * Replace backslash with space.  Remove double quote only.
 *
 * Input/Output:  string
 * Return 0 if no change.
 *
 ******************************************************************************/

int PQ_FixName(LPSTR pBuf, int iLen)
{
   int   iTmp, iRet=-1, iIdx=0;
   char  sTmp[256], *pTmp;
   pTmp = pBuf;  

   for (iTmp = 0; iTmp < iLen; iTmp++, pTmp++)
   {
      // Drop backslash (json error) or double/single quote
      if (*pTmp == '\\' || *pTmp == 34)
      {
         iRet = iTmp;
         sTmp[iIdx++] = ' ';
      } else if (*pTmp != 39)
         sTmp[iIdx++] = *pTmp;
      else
         iRet += 2;
   }

   if (iRet != -1)
   {
      sTmp[iIdx] = 0;
      blankRem(sTmp);
      blankPad(sTmp, iLen);
      memcpy(pBuf, sTmp, iLen);
   } else
      iRet = 0;

   return iRet;
}

int PQ_FixBadNames(LPSTR pBuf)
{
   int   iRet;

   iRet = PQ_FixName(pBuf+OFF_NAME1, SIZ_NAME1);
   if (*(pBuf+OFF_NAME2) > ' ')
      iRet += PQ_FixName(pBuf+OFF_NAME2, SIZ_NAME2);
   if (iRet > 0)
      PQ_FixName(pBuf+OFF_NAME_SWAP, SIZ_NAME_SWAP);
   if (*(pBuf+OFF_DBA) > ' ')
      iRet += PQ_FixName(pBuf+OFF_DBA, SIZ_DBA);
   if (*(pBuf+OFF_CARE_OF) > ' ')
      iRet += PQ_FixName(pBuf+OFF_CARE_OF, SIZ_CARE_OF);

   return iRet;
}

int PQ_FixLegal(LPSTR pBuf)
{
   int   iRet;

   iRet = PQ_FixName(pBuf+OFF_NAME1, SIZ_NAME1);
   if (*(pBuf+OFF_NAME2) > ' ')
      iRet += PQ_FixName(pBuf+OFF_NAME2, SIZ_NAME2);
   if (*(pBuf+OFF_DBA) > ' ')
      iRet += PQ_FixName(pBuf+OFF_DBA, SIZ_DBA);
   if (*(pBuf+OFF_CARE_OF) > ' ')
      iRet += PQ_FixName(pBuf+OFF_CARE_OF, SIZ_CARE_OF);

   return iRet;
}

/******************************** PQ_FixBadChar *******************************
 *
 * Replace non-ASCII with a char.  If char is not provided, scan only.
 * Return number of bad records found
 * Input/Output:  R01 file
 *
 ******************************************************************************/

extern   COUNTY_INFO myCounty;
int PQ_FixBadChar(char *pFilename, char cReplChar, int iRecLen)
{
   char     acBuf[MAX_RECSIZE], acTmp[_MAX_PATH], acOutFile[_MAX_PATH];

   HANDLE   fhIn, fhOut;
   DWORD    nBytesRead, nBytesWritten;
   BOOL     bRet=true;
   int      iRet, iRet1;
   long     lCnt=0, lBadCnt=0;

   LogMsg0("PQ_FixBadChar(): Finding bad char in %s", pFilename);

   if (_access(pFilename, 0))
   {
      LogMsg("***** Error: missing input file %s", pFilename);
      return-1;
   }

   // Open R01 file
   LogMsg("Open input file %s", pFilename);
   fhIn = CreateFile(pFilename, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhIn == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening input file: %s\n", pFilename);
      return -3;
   }

   // Open Output file
   if (cReplChar > 0)
   {
      sprintf(acOutFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "TMP");
      LogMsg("Open output file %s", acOutFile);
      fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
             FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

      if (fhOut == INVALID_HANDLE_VALUE)
      {
         LogMsg("***** Error opening output file: %s\n", acOutFile);
         return -4;
      }
   } else
      fhOut = 0;

   // Merge loop
   while (bRet)
   {
      bRet = ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);

      // Check for EOF
      if (!bRet)
      {
         LogMsg("Error reading input file %s (%f)", pFilename, GetLastError());
         iRet = -1;
         break;
      }

      // EOF ?
      if (iRecLen != nBytesRead)
         break;

      // Fix data
      if (cReplChar > 0)
      {
         iRet = replUnPrtChar(acBuf, cReplChar, iRecLen);
         iRet1 = PQ_FixBadNames(acBuf);
         bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
      } else
      {
         iRet1 = 0;
         iRet = findUnPrtChar(acBuf, iRecLen);
      }

      if (iRet > 0 || iRet1 > 0)
      {
         if (bDebug)
         {
            if (iRet > 0)
               LogMsg("Bad char found at %d on record %d  for APN=%.16s", iRet, lCnt+1, acBuf);
            else
               LogMsg("Bad char found in owner name, careof, or DBA APN=%.14s", acBuf);
         }
         lBadCnt++;
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fhOut)
      CloseHandle(fhOut);
   if (fhIn)
      CloseHandle(fhIn);

   if (lBadCnt > 0)
   {
      LogMsg("Number of records with bad char: %d", lBadCnt);

      if (cReplChar > 0)
      {
         // Rename output file
         sprintf(acTmp, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "N0?");
         acTmp[strlen(acTmp)-1] = pFilename[strlen(pFilename)-1];

         if (!_access(acTmp, 0))
            remove(acTmp);
         LogMsg("Rename %s to %s", pFilename, acTmp);
         iRet = rename(pFilename, acTmp);
         LogMsg("Rename %s to %s", acOutFile, pFilename);
         iRet = rename(acOutFile, pFilename);
      }
   } else
   {
      LogMsg("No bad char found in %s", pFilename);
      DeleteFile(acOutFile);
   }

   LogMsgD("\nTotal records processed:        %u", lCnt);

   return lBadCnt;
}

/******************************** PQ_ChkBadR01 ******************************
 *
 * Check for bad char in R01 file with option to replace with a char.
 *
 ****************************************************************************/

int PQ_ChkBadR01(char *pCnty, char *pRawTmpl, int iRecordLen, char cRepl)
{
   char  acR01[_MAX_PATH];
   int   iRet;

   sprintf(acR01, pRawTmpl, pCnty, pCnty, "R01");
   iRet = PQ_FixBadChar(acR01, cRepl, iRecordLen);

   // Scan for bad characters in R01 file
   if (!_memicmp(pCnty, "LAX", 3))
   {
      sprintf(acR01, pRawTmpl, pCnty, pCnty, "R02");
      iRet += PQ_FixBadChar(acR01, cRepl, iRecordLen);
      sprintf(acR01, pRawTmpl, pCnty, pCnty, "R03");
      iRet += PQ_FixBadChar(acR01, cRepl, iRecordLen);
   }

   return iRet;
}

/***************************** PQ_MergePrevApn ******************************
 *
 * Copy PREV_APN from last year file
 *
 ****************************************************************************/

int PQ_MergePrevApn(char *pOutbuf, int iPrevApnLen, FILE *fd)
{
   static   char  acRec[2048], *pRec=NULL;
   int      iLoop, iCnt;

   // Get first Sale rec for first call
   if (!pRec)
   {
      // Skip header
      iCnt = fread(acRec, 1, iRecLen, fd);
      // Get first record
      iCnt = fread(acRec, 1, iRecLen, fd);
      pRec = &acRec[0];
   }

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "003061006", 9))
   //   iLoop = 0;
#endif
   do
   {
      // Compare Apn
      iLoop = memcmp(pOutbuf, acRec, iApnLen);
      if (iLoop > 0)
      {
         iCnt = fread(acRec, 1, iRecLen, fd);
         if (iCnt < iRecLen)
         {
            fclose(fd);
            fd = NULL;
            return -1;      // EOF
         }
      }
   } while (iLoop > 0);

   if (iLoop)
      return 1;
   
   memcpy(pOutbuf+OFF_PREV_APN, &acRec[OFF_PREV_APN], iPrevApnLen);
   iCnt = fread(acRec, 1, iRecLen, fd);
   if (iCnt < iRecLen)
   {
      fclose(fd);
      fd = NULL;
   }

   return 0;
}

/******************************* PQ_MergeLotArea *****************************
 *
 * Modify to handle both file formats
 *    Type 1: Cnty, Apn, LotSqft (comma delimited)
 *    Type 2: Apn, LotSqft. Acre, Count (vertical bar delimited)
 *
 *    - Input record can be longer than 512 bytes, watch out.
 *
 * Return 0 if success.
 *
 *****************************************************************************/

int PQ_MergeLotArea(char *pCnty, bool bOverWrite)
{
   char     *pTmp, acBuf[MAX_RECSIZE], acTmp[1024], acLotArea[1024];
   char     cLotDelim, cFileCnt=1;
   char     acRawFile[_MAX_PATH], acOutFile[_MAX_PATH], *apItems[8];

   HANDLE   fhIn, fhOut;
   FILE     *fd;
   int      iTmp, iFldApn, iFldSqft, iFldAcre, iRecUpd=0, iRecClr=0, lRet=0, lCnt=0;
   DWORD    nBytesRead;
   DWORD    nBytesWritten;
   double   dTmp;
   BOOL     bRet, bEof, bNoParse;
   ULONG    lTmp, lSqft, lLotAcres;

   LogMsg0("Merge lot area");



   // Check LotArea file
   iTmp = GetIniString(pCnty, "AreaFile", "", acLotArea, _MAX_PATH, acIniFile);
   if (iTmp > 0 && !_access(acLotArea, 0))
   {
      iFldApn = GetPrivateProfileInt(pCnty, "LotApn", 0, acIniFile);
      iFldAcre = GetPrivateProfileInt(pCnty, "LotAcre", 0, acIniFile);
      iFldSqft = GetPrivateProfileInt(pCnty, "LotSqft", 0, acIniFile);
      GetPrivateProfileString(pCnty, "LotDelim", ",", acTmp, _MAX_PATH, acIniFile);   
      cLotDelim = acTmp[0];
      // If .dat file, no need to resort
      if (!strstr(acLotArea, ".dat"))
      {
         sprintf(acOutFile, acTmp, pCnty, "dat");
         sprintf(acBuf, "S(#%d,C,A) DEL(%c) F(TXT) DUPOUT(#%d) ", iFldApn, cLotDelim, iFldApn);
         lTmp = sortFile(acLotArea, acOutFile, acBuf);
      } else
         strcpy(acOutFile, acLotArea);
   } else
   {
      iTmp = GetIniString("Data", "AreaFile", "", acTmp, _MAX_PATH, acIniFile);
      if (iTmp > 0)
      {
         sprintf(acLotArea, acTmp, pCnty, "csv");
         if (_access(acLotArea, 0))
         {
            sprintf(acLotArea, acTmp, pCnty, "txt");
            strcpy(acBuf, "S(#2,C,A) DEL(44) F(TXT) OMIT(#2,C,GT,\"9\",OR,#2,C,LT,\"0\") DUPOUT(#2) ");
            iFldApn  = 1;
            iFldSqft = 2;
            iFldAcre = 0;
            cLotDelim = ',';
         } else
         {
            strcpy(acBuf, "S(#1,C,A) DEL(124) F(TXT) OMIT(#1,C,GT,\"9\",OR,#1,C,LT,\"0\") DUPOUT(#1)");
            iFldApn  = 0;
            iFldSqft = 1;
            iFldAcre = 2;
            cLotDelim = '|';
         }
         sprintf(acOutFile, acTmp, pCnty, "dat");
         if (!_access(acLotArea, 0))
         {
            lTmp = sortFile(acLotArea, acOutFile, acBuf);
         } else
            LogMsg("*** No new basemap file: %s.  Use existing one: %s", acLotArea, acOutFile);

      } else
      {
         LogMsg("***** Error AreaFile not defined in INI file.  Please add it to [Data] section");
         return 1;
      }
   }

   LogMsg("Open Lot Area file %s", acOutFile);
   fd = fopen(acOutFile, "r");
   if (fd == NULL)
   {
      LogMsg("***** Error opening lot area file: %s\n", acOutFile);
      return 2;
   }

   // Get first record
   pTmp = fgets(acLotArea, 1024, fd);
   if (!pTmp || feof(fd))
   {
      fclose(fd);
      LogMsg("***** Lot Area file is empty");
      return 1;
   }

   // Open Input file
   sprintf(acRawFile, acRawTmpl, pCnty, pCnty, "R01");
   LogMsg("Open input file %s", acRawFile);
   fhIn = CreateFile(acRawFile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhIn == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening input file to merge LotArea: %s\n", acRawFile);
      return 3;
   }

   // Open Output file
   sprintf(acOutFile, acRawTmpl, pCnty, pCnty, "N01");
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening output file to merge LotArea: %s\n", acOutFile);
      return 4;
   }

   // Copy skip record
   ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);
   bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);

   iNoMatch=0;
   bNoParse=bEof = false;

   // Merge loop
   while (!bEof)
   {
      bRet = ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);
      // Check for EOF
      if (!bRet)
      {
         LogMsg("***** Error reading input file %s (%f)", acRawFile, GetLastError());
         break;
      }

      if (iRecLen != nBytesRead)
      {
         // EOF
         CloseHandle(fhIn);
         CloseHandle(fhOut);
         fhIn = 0;
         fhOut = 0;

         LogMsg("Rename output file %s to %s", acOutFile, acRawFile);
         sprintf(acTmp, "M0%c", cFileCnt|0x30);
         sprintf(acBuf, acRawTmpl, pCnty, pCnty, acTmp);
         if (!_access(acBuf, 0))
            remove(acBuf);
         iTmp = rename(acRawFile, acBuf);
         iTmp = rename(acOutFile, acRawFile);

         // Check for next input
         cFileCnt++;
         acRawFile[strlen(acRawFile)-1] = cFileCnt | 0x30;
         if (!_access(acRawFile, 0))
         {
            // Open next Input file
            LogMsg("Open input file %s", acRawFile);
            fhIn = CreateFile(acRawFile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
                  FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

            if (fhIn == INVALID_HANDLE_VALUE)
            {
               LogMsg("***** Error opening input file to merge Lot Area: %s\n", acRawFile);
               break;
            }
            bRet = ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);

            // Open Output file
            acOutFile[strlen(acRawFile)-1] = cFileCnt | 0x30;
            LogMsg("Open output file %s", acOutFile);
            fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
                  FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);
            if (fhOut == INVALID_HANDLE_VALUE)
            {
               LogMsg("***** Error opening output file to merge Lot Area: %s\n", acOutFile);
               break;
            }
         } else
            break;
      }

      // Update data
Area_ReLoad:
#ifdef _DEBUG
      //if (!memcmp(acBuf, "3231011082", 10))
      //   iTmp = 0;
#endif

      // Parse input
      if (!bNoParse)
      {
         iTmp = ParseString(acLotArea, cLotDelim, 8, apItems);
         if (iTmp < 3)
         {
            LogMsg("***** Invalid LotArea record %s (%d)", acLotArea, iTmp);
            break;
         }
         bNoParse = true;
      }

      iTmp = memcmp(acBuf, apItems[iFldApn], iApnLen);
      if (!iTmp)
      {
         dTmp = strtod(apItems[iFldSqft], NULL);
         lSqft  = atoln(&acBuf[OFF_LOT_SQFT], SIZ_LOT_SQFT);
         lLotAcres = atoln(&acBuf[OFF_LOT_ACRES], SIZ_LOT_SQFT);

         // Don't overwrite existing value
         if ((dTmp > 0.0001 && !lSqft && !lLotAcres) || bOverWrite)
         {
            // Merge data
            sprintf(acTmp, "%*u", SIZ_LOT_SQFT, (ULONG)(dTmp + 0.5));
            memcpy(&acBuf[OFF_LOT_SQFT], acTmp, SIZ_LOT_SQFT);
            if (iFldAcre > 0)
            {
               dTmp = strtod(apItems[iFldAcre], NULL);
               sprintf(acTmp, "%*u", SIZ_LOT_ACRES, (ULONG)(dTmp * 1000));
            } else
            {
               lTmp = (ULONG)((double)dTmp*SQFT_MF_1000);
               sprintf(acTmp, "%*u", SIZ_LOT_ACRES, lTmp);
            }
            memcpy(&acBuf[OFF_LOT_ACRES], acTmp, SIZ_LOT_ACRES);
            iRecUpd++;
         }

         // Read next record
         pTmp = fgets(acLotArea, 512, fd);
         if (!pTmp)
            bEof = false;     // Signal to stop sale update
         else
            bNoParse = false;
      } else
      {
         if (iTmp > 0)        // Not match, advance to next record
         {
            //LogMsg("+++ Unmatched %s", apItems[iFldApn]);
            iNoMatch++;
            pTmp = fgets(acLotArea, 512, fd);
            if (!pTmp)
               bEof = true;   // Signal to stop sale update
            else
            {
               bNoParse = false;
               goto Area_ReLoad;
            }
         } else if (bOverWrite)
         {
            memset(&acBuf[OFF_LOT_SQFT], ' ', SIZ_LOT_SQFT);
            memset(&acBuf[OFF_LOT_ACRES], ' ', SIZ_LOT_ACRES);
            iRecClr++;
         } 
      }

      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);

      if (!bRet)
      {
         LogMsg("***** Error occurs: %d\n", GetLastError());
         lRet = -1;
         break;
      }
   }

   // Do the rest of the file
   while (nBytesRead>0 && ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL))
   {
      if (!nBytesRead)
         break;

      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
      lCnt++;
   }

   // Close files
   if (fd)
      fclose(fd);
   if (fhOut)
      CloseHandle(fhOut);
   if (fhIn)
   {
      CloseHandle(fhIn);

      // Rename outfile
      LogMsg("Rename output file %s to %s", acOutFile, acRawFile);
      sprintf(acTmp, "M0%c", cFileCnt|0x30);
      sprintf(acBuf, acRawTmpl, pCnty, pCnty, acTmp);
      if (!_access(acBuf, 0))
         remove(acBuf);
      iTmp = rename(acRawFile, acBuf);
      iTmp = rename(acOutFile, acRawFile);
   }

   // Update output record count
   if (lCnt > 1000)
      lRecCnt = lCnt;

   LogMsg("Total output records:  %u", lCnt);
   LogMsg("      records updated: %u", iRecUpd);
   LogMsg("      records cleaned: %u", iRecClr);
   LogMsg("      records skipped: %u\n", iNoMatch);

   return 0;
}

/****************************** PQ_SetPQZoning ******************************
 *
 * Preset PQZoning with county zoning
 *
 ****************************************************************************/

int PQ_SetPQZoning(char *pFilename)
{
   char     acBuf[MAX_RECSIZE], acTmp[_MAX_PATH], acOutFile[_MAX_PATH];

   HANDLE   fhIn, fhOut;
   DWORD    nBytesRead, nBytesWritten;
   BOOL     bRet=true;
   int      iRet;
   long     lCnt=0, lUpdCnt=0;

   LogMsg0("PQ_SetPQZoning for %s", pFilename);

   if (_access(pFilename, 0))
   {
      LogMsg("***** Error: missing input file %s", pFilename);
      return-1;
   }

   // Open R01 file
   LogMsg("Open input file %s", pFilename);
   fhIn = CreateFile(pFilename, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhIn == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening input file: %s\n", pFilename);
      return -3;
   }

   // Open Output file
   sprintf(acOutFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "TMP");
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
            FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening output file: %s\n", acOutFile);
      return -4;
   }

   // Merge loop
   while (bRet)
   {
      bRet = ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);

      // Check for EOF
      if (!bRet)
      {
         LogMsg("Error reading input file %s (%f)", pFilename, GetLastError());
         iRet = -1;
         break;
      }

      // EOF ?
      if (iRecLen != nBytesRead)
         break;

      // Remove old PQZoning
      removeZoning(acBuf);

      // Copy data
      if (acBuf[OFF_ZONE] > ' ')
      {
         memcpy(&acBuf[OFF_ZONE_X1], &acBuf[OFF_ZONE], SIZ_ZONE);
         lUpdCnt++;
      } 
      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fhOut)
      CloseHandle(fhOut);
   if (fhIn)
      CloseHandle(fhIn);

   if (lUpdCnt > 0)
   {
      LogMsg("Number of records updated: %u", lUpdCnt);

      // Rename output file
      sprintf(acTmp, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "Z0?");
      acTmp[strlen(acTmp)-1] = pFilename[strlen(pFilename)-1];

      if (!_access(acTmp, 0))
         remove(acTmp);
      LogMsg("Rename %s to %s", pFilename, acTmp);
      iRet = rename(pFilename, acTmp);
      LogMsg("Rename %s to %s", acOutFile, pFilename);
      iRet = rename(acOutFile, pFilename);
   } else
   {
      LogMsg("No Zoning update in %s", pFilename);
      DeleteFile(acOutFile);
   }

   LogMsg("Total records processed:   %u", lCnt);
   LogMsg("        records updated:   %u\n", lUpdCnt);

   return lUpdCnt;
}

int PQ_SetDefaultPQZoning(char *pCnty)
{
   char  acR01[_MAX_PATH];
   int   iRet;

   sprintf(acR01, acRawTmpl, pCnty, pCnty, "R01");
   iRet = PQ_SetPQZoning(acR01);

   // Scan for bad characters in R01 file
   if (!_memicmp(pCnty, "LAX", 3))
   {
      sprintf(acR01, acRawTmpl, pCnty, pCnty, "R02");
      iRet += PQ_SetPQZoning(acR01);
      sprintf(acR01, acRawTmpl, pCnty, pCnty, "R03");
      iRet += PQ_SetPQZoning(acR01);
   }

   return iRet;
}

/**************************** PQ_RemovePQZoning *****************************
 *
 * Remove PQZoning 
 *
 ****************************************************************************/

int PQ_RemovePQZoning(char *pCnty, char *pFilename, int iSkip)
{
   char     acBuf[MAX_RECSIZE], acRawFile[_MAX_PATH], acOutFile[_MAX_PATH];

   HANDLE   fhIn, fhOut;
   DWORD    nBytesRead, nBytesWritten;
   BOOL     bRet=true;
   int      iRet=0;
   long     lCnt=0, lUpdCnt=0;

   LogMsg0("PQ_RemovePQZoning for %s", pFilename);
   strcpy(acRawFile, pFilename);

   // Open Input file
   LogMsg("Open input file %s", acRawFile);
   fhIn = CreateFile(acRawFile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);
   if (fhIn == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening input file to merge GrGr: %s\n", acRawFile);
      return 3;
   }

   // Open Output file
   sprintf(acOutFile, acRawTmpl, pCnty, pCnty, "TMP");
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);
   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening output file: %s\n", acOutFile);
      return -4;
   }

   // Skip header
   if (iSkip > 0)
   {
      bRet = ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);
      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
   }

   // Merge loop
   while (bRet)
   {
      bRet = ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);

      // Check for EOF
      if (!bRet)
      {
         LogMsg("Error reading input file, error=%f)", GetLastError());
         iRet = -1;
         break;
      }

      // EOF ?
      if (iRecLen != nBytesRead)
         break;

      // Remove old PQZoning
      removeZoning(acBuf);

      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fhOut)
      CloseHandle(fhOut);
   if (fhIn)
      CloseHandle(fhIn);

   if (!iRet)
   {
      // Rename output file
      iRet = sprintf(acBuf, acRawTmpl, pCnty, pCnty, "Z0?");
      acBuf[iRet-1] = acRawFile[strlen(acRawFile)-1];

      if (!_access(acBuf, 0))
         remove(acBuf);
      LogMsg("Rename %s to %s", acRawFile, acBuf);
      iRet = rename(acRawFile, acBuf);
      LogMsg("Rename %s to %s", acOutFile, acRawFile);
      iRet = rename(acOutFile, acRawFile);
   }

   LogMsg("Total records processed:   %u\n", lCnt);

   return iRet;
}

int PQ_RemovePQZoning(char *pCnty, int iSkip)
{
   char  acR01[_MAX_PATH];
   int   iRet;
   bool  bUseR01 = true;

   sprintf(acR01, acRawTmpl, pCnty, pCnty, "R01");
   if (_access(acR01, 0))
   {
      bUseR01 = false;
      sprintf(acR01, acRawTmpl, pCnty, pCnty, "S01");
   }
   iRet = PQ_RemovePQZoning(pCnty, acR01, iSkip);

   // Scan for bad characters in R01 file
   if (!_memicmp(pCnty, "LAX", 3))
   {
      if (bUseR01)
         sprintf(acR01, acRawTmpl, pCnty, pCnty, "R02");
      else
         sprintf(acR01, acRawTmpl, pCnty, pCnty, "S02");
      iRet += PQ_RemovePQZoning(pCnty, acR01, 0);

      if (bUseR01)
         sprintf(acR01, acRawTmpl, pCnty, pCnty, "R03");
      else
         sprintf(acR01, acRawTmpl, pCnty, pCnty, "S03");
      iRet += PQ_RemovePQZoning(pCnty, acR01, 0);
   }

   return iRet;
}

/******************************** PQ_CopyOldR01 ******************************
 *
 * Merge data from old file
 * Type: 1=Legal
 *       2=UseCode
 *       4=Zoning
 *       8=
 *
 * Return 0 if successful, 1 if not matched, -1 if eof.
 *
 *****************************************************************************/

int PQ_CopyOldR01(char *pOutbuf, HANDLE fhIn, int iType, int iSkip)
{
   static   char  acRec[MAX_RECSIZE], *pRec=NULL;
   int      iLoop;
   DWORD    nBytesRead;

   // Get first Sale rec for first call
   if (!pRec)
   {
      if (iSkip > 0)
         ReadFile(fhIn, acRec, iRecLen, &nBytesRead, NULL);          // Skip header
      ReadFile(fhIn, acRec, iRecLen, &nBytesRead, NULL);
   }
   pRec = acRec;

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "003061006", 9))
   //   iLoop = 0;
#endif
   do
   {
      // Compare Apn
      iLoop = memcmp(pOutbuf, acRec, SIZ_LIEN_APN);
      if (iLoop > 0)
      {
         ReadFile(fhIn, acRec, iRecLen, &nBytesRead, NULL);
         if (nBytesRead < iRecLen)
         {
            return -1;           // EOF
         }
      }
   } while (iLoop > 0);

   if (iLoop)
      return 1;
   
   // Merge data
   if (iType & PQ_UPD_LEGAL)
   {
      memcpy(pOutbuf+OFF_LEGAL, pRec+OFF_LEGAL, SIZ_LEGAL);
      memcpy(pOutbuf+OFF_LEGAL1, pRec+OFF_LEGAL1, SIZ_LEGAL1);
      memcpy(pOutbuf+OFF_LEGAL2, pRec+OFF_LEGAL2, SIZ_LEGAL2);
   }

   if (iType & PQ_UPD_USECODE)
   {
      memcpy(pOutbuf+OFF_USE_CO, pRec+OFF_USE_CO, SIZ_USE_CO);
      memcpy(pOutbuf+OFF_USE_STD, pRec+OFF_USE_STD, SIZ_USE_STD);
   }

   if (iType & PQ_UPD_ZONING)
   {
      memcpy(pOutbuf+OFF_ZONE, pRec+OFF_ZONE, SIZ_ZONE);
   }

   ReadFile(fhIn, acRec, iRecLen, &nBytesRead, NULL);
   if (nBytesRead < iRecLen)
      return -1;
   else
      return 0;
}
