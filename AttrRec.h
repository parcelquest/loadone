#define  SIZ_ATTR_APN         11
#define  SIZ_ATTR_BLDQ        3
#define  SIZ_ATTR_IMPS        12
#define  SIZ_ATTR_BEDS        7
#define  SIZ_ATTR_YRBLT       4
#define  SIZ_ATTR_YREFF       4
#define  SIZ_ATTR_GARSIZE     12
#define  SIZ_ATTR_CPSIZE      12
#define  SIZ_ATTR_ROOMS       7
#define  SIZ_ATTR_UNITS       7
#define  SIZ_ATTR_LOTSIZE     13
#define  SIZ_ATTR_LOTACRE     13

typedef struct _tAttrRec
{
   char Apn[SIZ_ATTR_APN];                // 0
   char BldgClass[1];                     // 11
   char BldgQual[SIZ_ATTR_BLDQ];          // 12
   char filler1[1];                       // 15
   char Imps[SIZ_ATTR_IMPS];              // 16
   char Beds[SIZ_ATTR_BEDS];              // 28
   char Bath[1];                          // 35
   char HBath[1];                         // 36
   char YrBlt[SIZ_ATTR_YRBLT];            // 37
   char RoofType[1];                      // 41
   char GarSize[SIZ_ATTR_GARSIZE];        // 42
   char Pool[1];                          // 54
   char OutBldg[1];                       // 55
   char ViewFlag[1];                      // 56
   char Const[1];                         // 57
   char CarPortSF[SIZ_ATTR_CPSIZE];       // 58
   char YrEff[SIZ_ATTR_YREFF];            // 70
   char Spa[1];                           // 74
   char Rooms[SIZ_ATTR_ROOMS];            // 75
   char Units[SIZ_ATTR_UNITS];            // 82
   char FirePlace[1];                     // 89
   char Heat[1];                          // 90
   char LotSize[SIZ_ATTR_LOTSIZE];        // 91
   char LotAcre[SIZ_ATTR_LOTACRE];        // 104
   char Garage[1];                        // 117
   char CarPort[1];                       // 118
   char SizeFlag[1];                      // 119
   char Comment[62];                      // 120
   char CRLF[2];                          // 182
} ATTR_REC;

