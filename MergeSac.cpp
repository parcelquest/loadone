/******************************************************************************
 *
 * Input files:
 *    - gae7000_rollpubl.txt        (roll file, 375-byte ebcdic)
 *    - gax2100_apprpubl.txt        (char file, 750-byte ebcdic)
 *    - gax9300_salesmstr.txt       (sale file, 850-byte ebcdic)
 *    - landsize.txt                (addl attr file, delimited ascii)
 *    - aimsab_public.txt           (LDR file, 299-byte ascii)
 *
 * Following are tasks to be done here:
 *    - Convert ebcdic to ascii
 *    - Convert landsize.txt (delimited) to landsize.dat (fixed length) -Ll
 *    - Normal update: LoadOne -U -Us -Ll [-T]
 *    - Load Lien: LoadOne -CSAC -L -Ms -Xl -Ll -Dr -Xa -Xd
 *    - Update situs city using MergeAdr -CSAC -A
 *
 * Revision:
 * 10/13/2005 1.0       First version
 * 08/07/2006 1.2.28.2  Add log messages
 * 03/29/2007 1.4.7     Add unsecured data
 * 07/19/2007 1.4.20.1  Remove undecured data
 * 10/18/2007 TCB       Create cum sale file and update it every time the program run.
 *                      On next LDR, this cum sale will be used to update.
 *                      Extract lien values.
 * 11/02/2007 1.4.28    Use 05AIMSAB_PUBLIC.TXT to update LDR values.  This is
 *                      to fix OtherValues problem since roll file doesn't have it.
 * 03/20/2008 1.5.7.1   Use standard function to update usecode.  Also fix bug in
 *                      Sac_MergeAdr() where city name overwrites state.  Use
 *                      custom suffix table Sac_Sfx.txt to fix special case of addr.
 * 03/24/2008 1.5.7.2   Format IMAPLINK.
 * 07/09/2008 8.0.3     Do not load UNSEC file when loading LDR.
 * 11/17/2008 8.4.3     UNSEC file needs resort before merging.  To load fully, we need to
 *                      extend APN to 18 bytes.  We cannot do this at this time due
 *                      to record restructuring, but will do in the future.
 * 01/17/2009 8.5.6     Fix TRA
 * 03/16/2009 8.7.0     Format STORIES as 99.0.  We no longer multiply by 10.
 * 04/25/2009 8.7.2     Adding extract lien and merge other values.
 * 05/11/2009 8.8       Set full exemption flag
 * 06/02/2009 8.8.2     Adding Prop8 flag, change FullExeFlag (Y/N) in LIENEXTR to SpclFlag (bitmap)
 *                      Fix Sac_ExtrLien() by pulling data from LDR file instead of Roll file.
 * 06/14/2009 8.8.4     Convert all DocNum from PAGE # to YYMMDD#### format.
 * 07/13/2009 9.1.2     Drop Roof material & foundation type.  Fix Sac_MergeLVal() and
 *                      Sac_CreateLienRec() to add other values.
 * 10/25/2009 9.2.4     Use -Ur to replace LDR values with current values for Prop8 report
 * 02/23/2010 9.4.3     Fix CARE_OF issue.
 * 12/09/2010 10.3.5    Fix MergeOwner() bug that left bad character in owner name.
 * 01/10/2011 10.4.1    Add ExtractOwnerCode() to extract APN+OWNERCODE for Muni.
 * 03/08/2011 10.4.4    Change cum sale format to SCSAL_REC.
 *                      Modify Sac_FormatSale() to format DocNum same as county format.
 * 04/05/2011 10.5.2    Put StampCode (instead of SaleType) into SaleCode.  Values are 
 *                      (P)artial, (F)ull, or (T)ransfer.
 * 05/17/2011 10.5.11   Change output format from -Xc option to use SCSAL_REC.
 * 07/05/2011 11.0.1    Add S_HSENO. Found no range in StrNum.
 * 12/19/2011 11.5.17   Add Sac_MergeAltApn() to merge ALTAPN into R01 file.
 * 12/20/2011 11.5.17.2 Bug fix to populate ALTAPN when APN file ended before roll file.
 * 07/05/2012 12.1.1    Fix lApnMatch & lApnSkip in Sac_MergeAltApn().
 * 07/11/2012 12.1.1    Add special case for USE_CO "AG009A" in Sac_MergeRoll().
 * 08/31/2012 12.2.6    Remove unused functions Sac_ExtrSale1() & Sac_UpdateSaleHist().
 *                      Modify Sac_FormatSale() to match with new record layout.
 *                      Convert sale file to ASCII using OTSORT.
 * 02/08/2013 12.4.5    Add Sac_FixCumSale() to move DocType to DocCode and translate
 *                      it to standard DocType using SAC_DocTbl[]. Modify Sac_FormatSale()
 *                      to update DocType & DocCode correctly.
 * 05/07/2013 12.6.3    Set Ag_pre based on UseCode 'H' in Sac_MergeRoll().
 * 09/17/2013 13.8.2    Put Roof material back on for LPS/Lexis
 * 10/08/2013 13.10.4.2 Use updateVesting() to update Vesting and Etal flag.
 *                      Fractional bath not avail.
 * 11/15/2013 13.11.9   Add Sac_Load_LDR1() to create R01 from AIMSAB_PUBLIC.TXT
 *                      This special version is for Lexis extract only.
 * 01/13/2014 13.11.13  Update DocCode[] table and option -Fx to fix DocType. Save last version of cumsale file
 * 02/11/2014 13.11.16  Add funcs Sac_LoadCChar() & Sac_MergeCChr() to add comm char to R01.
 *                      Add -Xa option to extract comm char from XML file.
 * 02/12/2014 13.11.18  Fix Stories bug.
 * 03/04/2014 13.12.2   Auto load CommChar if file available even if -Xa not specified.
 * 05/01/2014 13.14.1   Modify Sac_LoadCChar() to output sorted file since input file is not sorted.
 * 05/08/2014 13.14.3   Fix LotSqft bug in Sac_LoadLand() that causes negative lotsqft.
 * 08/21/2014 14.4.1    First try to get City & Zip code from GIS extract. If not found,
 *                      and Zip code is in roll, look up city name from USPS list.
 * 07/02/2015 15.0.1    Modify Sac_Load_LDR() to load Zip2City.
 * 08/25/2015 15.1.2    Fine tune Sac_MergeAdr() to copy mail addr to situs when StrNum and StrName are matched.
 * 08/28/2015 15.1.3    Fine tune Sac_MergeAdr() & Sac_ParseAdr().
 * 08/31/2015 15.1.3.1  Use Sac_ParseAdr() to parse mailing instead of ParseMAdr1()
 * 09/02/2015 15.2.0    Add -Dt option to bypass translation from EBCDIC to ASCII.
 * 03/26/2016 15.8.1    Move dispError() to Logs.cpp
 * 04/08/2016 15.14.5   Load TC file
 * 07/04/2016 16.0.3    Add CommChar to Load_LDR()
 * 11/18/2016 16.7.0    Modify Sac_FormatSale() to update Full/Partial sale on Sale_Code
 * 01/15/2017 16.9.3    Add Sac_Load_Cortac() to process new county tax file.
 *            16.9.3.1  Change logic: 1) if PaidStatus='R', consider unpaid. 2) Set DueAmt
 *                      only if tax is unpaid after due date.
 * 03/15/2017 16.12.1   Modify Sac_UpdateTaxBase(), Sac_ParseCortac(), and Sac_Load_Cortac()
 * 04/03/2017 16.13.5.1 Modify Sac_ParseCortac() to skip update Tax_Base using TC file if it's 
 *                      older than CORTAC file.
 * 04/21/2017 16.14.3   Modify Sac_Load_Cortac() to ignore CANCELLED tax bill.
 *                      If TC_UPD_DATE is available, use it.
 * 04/24/2017 16.14.4   Modify Sac_Load_Cortac() to sort SAC.TC file ignoring records without TAX_YEAR.
 * 04/25/2017 16.14.5   Modify Sac_UpdateTaxBase() to set update date using UPD_DATE from SAC.TC and reset
 *                      tax value if it's been cancelled.  Fix TotalDue in Sac_ParseCortac().
 * 04/26/2017 16.14.5.1 Fix bug in Sac_UpdateTaxBase() due to UPD_DATE format change.
 * 05/05/2017 16.14.8   Modify Sac_MergeChar() to add Floor Sqft to R01 file.
 * 06/08/2017 16.15.0   Add code to load tax using scraped data.  Use Load_TCC() for special case.
 * 08/10/2017 17.2.0    Modify Sac_MergeChar() to add NBH_CODE to R01 record. Add Sac_ConvStdChar()
 *                      Name Comm Char extract as SAC_ATTR.DAT to differentiate it from Res Char SAC_CHAR.DAT
 * 10/20/2017 17.4.1    Add -Xd option and function Sac_LegalExtr() to extract legal.
 * 11/21/2017 17.5.0    Call updateDelqFlg() to populate delqYear & defaultAmt in Tax_Base table.
 * 05/17/2018 17.10.11  Add -Ut option to update tax.
 * 06/20/2018 17.12.4   Add Sac_MergeUnitNo() & Sac_ConvGisFile().  Modify Sac_Load_Roll() & Sac_Load_LDR()  
 *                      to update situs UnitNo from GISX0105 file.
 * 06/21/2018 17.12.5   Modify Sac_MergeUnitNo() to include '#' in UnitNo and reformat S_ADDR_D to include UnitNo
 * 06/29/2018 18.0.0    Add Sac_MergeCChar() to merge CHAR from both comm & res char info.
 *                      Modify Sac_Load_Roll() & Sac_Load_LDR() to use Sac_MergeCChar().
 *                      Modify Sac_ConvStdChar() to append to existing SAC_Char.dat and fine tune 
 *                      Stories, Baths, FirePlace, RoofMat, Zoning, and BsmtSqft.
 * 07/09/2018 18.1.1    Remove unused function Sac_MergeCChr()
 * 07/20/2018 18.2.2    Modify Sac_ConvGisFile() to pull Unit# from MSTREET since GIS_SUB was truncated to only 4 digits.
 * 08/24/2014 18.4.0    Add -Mr option to update lot area using SAC_Basemap.txt.
 * 09/29/2018 18.5.1    Move -Mr option to LoadOne.cpp
 * 10/22/2018 18.5.7    Modify Sac_ConvCommChar() to populate LotAcre even when LotSqft is < 1 acre.
 *                      Modify Sac_ConvStdChar() to change sort order of output CHAR file.
 * 01/09/2019 18.8.0    Add new functions to support new file formats.
 *            18.8.1    Fine tune Sac_Load_Roll2() by adding GIS, Land, AltApn data.
 *            18.8.1.1  Modify Sac_ExtrSale() to skip Buyer Name if not available.
 *            18.8.1.4  Fine tuning MergeSitus() & MergeMailing().
 * 01/16/2019 18.8.3    Fix ImprCond & RoofMat in Sac_ConvStdChar2().  Fix BldgQual in Sac_ConvCommChar2().
 * 02/12/2019 18.9.3    Fix "PO BOX" issue in Sac_MergeMailing(). Add Sac_MergeEVal() to merge equalized values
 *                      to LDR. Modify Sac_Load_LDR() to add option to merge equalized values instead of LDR values.
 * 02/13/2019 18.9.4    Add Sac_MergeEUVal() and Sac_MergeELVal() to replace Sac_MergeEVal()
 *                      Modify Sac_Load_LDR1() to use equalized roll for updated values.
 *                      Modify Sac_Load_Roll2() & Sac_MergeRoll2() to ignore obsolete (or inactive) parcels.
 *                      Modify Sac_MergeOwner() to ignore record with OWNER_ID=0.
 *                      Modify Sac_Load_Roll2() to ignore records without owner name.
 * 07/09/2019 19.0.2    Add Sac_ExtrLien2(), Sac_CreateLienRec2(), Sac_Load_LDR2(), Sac_CreateRollRec2(), 
 *                      Sac_MergeMAdr() to support new LDR format. Fix situs street in Sac_MergeSitus().
 * 07/24/2019 19.0.5    Modify Sac_ConvStdChar2() & Sac_MergeCChar() to fix FLOOR_1 & adding MisfSqft.
 * 11/21/2019 19.5.5    Check for existing of city_full.tar and untar it before processing.
 * 02/21/2020 19.6.6    Add log msg and comments
 * 02/27/2020 19.7.1    Modify Sac_MergeOwnerNames() to ignore GRANTOR.  Modify Sac_Load_Roll2() to accept
 *                      new records without owner name.
 * 04/04/2020 19.8.8    Check for [TaxRate] file and use it to update Sac_Tax_Base.tb_TotalRate.
 * 07/15/2020 20.1.5    Add Sac_UpdateZoning(), Sac_ExtrLegal(), Sac_MergeLegal().  Modify Sac_ExtrLien2() to 
 *                      add legal to LDR process.  Modify Sac_CreateRollRec2() to add exemption code using owner code data.
 *                      Clean up and fine tune loadSac().
 * 10/12/2020 20.2.15   Check tax input file on county section first then [Data] section in INI file.
 * 10/26/2020 20.3.6    Modify Sac_UpdateZoning() to populate PQZoning if it's blank.
 * 02/25/2021 20.7.8    Fix Sac_ParseCortac() to populate TotalTaxRate using data from [TaxRate] in INI file.
 * 04/27/2021 20.7.16   Change sort command in Sac_ConvStdChar2() to remove duplicate records.
 *                      Modify Sac_ExtrSale() to treat sale price as unsigned long to avoid negative sale price.
 * 07/02/2021 21.0.1    Modify Sac_ConvStdChar2() & Sac_MergeCChar() to populate QualityClass.
 * 08/19/2021 21.1.5    Modify Sac_ParseCortac() to ignore prior year tax bill. Rename Sac_Load_Cortac() to 
 *                      Sac_Load_CortacX() and modify Sac_Load_Cortac() not to use TC file.
 * 02/01/2022 21.4.11   Use RebuildCsv_IQ() to rebuild roll file.  This version ignores duoble quote in LEGAL.
 * 03/23/2022 21.8.5    Add -Xf option and Sac_ExtrVal() to extract final value from equalized roll.
 * 04/15/2022 21.8.8    Modify Sac_MergeELVal() to remove all LDR values before update.
 *                      Add Sac_Load_LDR3() to load LDR with final value.  To load original LDR, just 
 *                      comment out "FinalEQRFile" in INI file.
 * 08/30/2022 22.2.0    Modify -Ut option to merge updated tax files. Add Sac_Update_TaxBase() & Sac_Update_TaxDelq().
 *                      Replace Sac_Update_TaxDelq() with Sac_Load_TaxDelq() to load full Delq file every time.
 * 10/25/2022 22.2.10   Modify Sac_MergeCChar() to add UNITS to R01 record.
 * 10/31/2022 22.2.11   Modify Sac_ExtrVal() to extract value from FinalEQRFile.
 * 07/04/2023 23.0.0    Remove comma after city name for M_CTY_ST_D in Sac_MergeMAdr() to make it look similar to Sac_MergeMailing().
 * 08/09/2023 23.1.3    Modify loadSac() to remove code that uses TC files.  Check TaxRoll file size
 *                      to determine whether to do full load or just update Base table.
 *                      Modify Sac_Update_TaxBase() to generate county specific Tmp???_Base file.
 * 11/03/2023 23.4.1    Modify Sac_ExtrVal() to check for final value.  If not found, use current.
 * 01/11/2024 23.5.5    Fix M_UNITNOX in Sac_MergeMailing(), Sac_ParseAdr(), Sac_MergeAdr().
 * 07/10/2024 24.0.2    Modify Sac_CreateRollRec2() to add ExeType.
 * 10/10/2024 24.1.7    Modify Sac_Load_LDR3() to use altenate EQ file when final file is not available.
 *
 ******************************************************************************/

#include "stdafx.h"
#include "CountyInfo.h"
#include "hlAdo.h"
#include "Prodlib.h"
#include "R01.h"
#include "RecDef.h"
#include "Tables.h"
#include "Logs.h"
#include "LoadDef.h"
#include "Utils.h"
#include "XlatTbls.h"
#include "doSort.h"
#include "doOwner.h"
#include "formatApn.h"
#include "SaleRec.h"
#include "Markup.h"
#include "CharRec.h"

#include "LoadOne.h"
#include "UseCode.h"
#include "LOExtrn.h"
#include "PQ.h"
#include "MergeSac.h"
#include "Situs.h"
#include "Tax.h"

static LU_ENTRY tblAttr[MAX_ATTR_ENTRIES];
static FILE     *fdChar, *fdLand, *fdValue, *fdApn, *fdCChr, *fdGis, *fdSitus, *fdMail, *fdOwner, *fdEq, *fdLegal;
static int      iLandSize, iLValSize, iNoStrNum;
static long     lSitusSkip, lMailSkip, lOwnerSkip, lCharSkip, lCChrSkip, lSaleSkip, lLandSkip, lLValSkip, lApnSkip, lTaxSkip, lTaxMatch, lTCDate;
static long     lSitusMatch, lMailMatch, lOwnerMatch, lCharMatch, lCChrMatch, lSaleMatch, lLandMatch, lLValMatch, lApnMatch, lUseGis, 
                lUseMailCity, lUnitMatch, lLegalMatch;
static char     acLongLegalFile[_MAX_PATH];

/******************************** Sac_MergeSitus ******************************
 *
 * Merge Situs address
 * Return 0 if successful, otherwise error.
 *
 *****************************************************************************/

int Sac_MergeSitus(char *pOutbuf)
{
   static   char acRec[512], *pRec=NULL;
   char     acTmp[256], acCode[32], acAddr1[256], acStrName[256], *pTmp, *apItems[32], *pApn;
   long     lTmp;
   int      iRet=0, iTmp;

   if (!pRec)
   {
      pRec = fgets(acRec, 512, fdSitus);
   }

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "201010001800", 10))
   //   iTmp = 0;
#endif

   do 
   {
      if (!pRec)
      {
         fclose(fdSitus);
         fdSitus = NULL;
         return 1;      // EOF 
      }

      pApn = strchr(pRec, '|');
      iTmp = memcmp(pOutbuf, pApn+1, iApnLen);
      if (iTmp > 0)
      {
         if (bDebug)
            LogMsg0("Skip Situs rec: %s", pRec);
         pRec = fgets(acRec, 512, fdSitus);
         lSitusSkip++;
      }
   } while (iTmp > 0);

   // If not match, return
   if (iTmp)
      return 1;

   // Parse input
   iRet = ParseStringIQ(pRec, '|', SITUS_COLS, apItems);
   if (iRet < SITUS_COLS)
   {
      LogMsg("***** Error: bad situs record for APN=%.*s (#tokens=%d)", iApnLen, pOutbuf, iRet);
      return -1;
   }

   // Clear old data
   removeSitus(pOutbuf);

   // Merge data
   acAddr1[0] = 0;
   lTmp = atol(apItems[SITUS_STREET_NUMBER]);
   if (lTmp > 0)
   {
      iTmp = sprintf(acTmp, "%d", lTmp);
      memcpy(pOutbuf+OFF_S_STRNUM, acTmp, iTmp);

      strcpy(acAddr1, apItems[SITUS_STREET_NUMBER]);
      iTmp = blankRem(acAddr1);

      // Save original StrNum
      if ((pTmp = strchr(acAddr1, '-')) && (*(pTmp+1) >= ' '))
      {
         // Remove all space in between
         iTmp = remChar(acAddr1, ' ');
         memcpy(pOutbuf+OFF_S_HSENO, acAddr1, iTmp);
      } else
      {
         if (pTmp = strchr(acAddr1, ' '))
         {
            if (acAddr1[iTmp-1] == '&')
               acAddr1[iTmp-1] = ' ';
            else
               replStr(acAddr1, "AND", "&");
            memcpy(pOutbuf+OFF_S_HSENO, acAddr1, strlen(acAddr1));
         } else
            memcpy(pOutbuf+OFF_S_HSENO, pOutbuf+OFF_S_STRNUM, SIZ_S_STRNUM);
      }   

      strcat(acAddr1, " ");
      if (*apItems[SITUS_PRE_DIRECTION] > ' ')
      {
         strcat(acAddr1, apItems[SITUS_PRE_DIRECTION]);
         strcat(acAddr1, " ");
         memcpy(pOutbuf+OFF_S_DIR, apItems[SITUS_PRE_DIRECTION], strlen(apItems[SITUS_PRE_DIRECTION]));
      }
   }

   // Correction
   if (!memcmp(apItems[SITUS_STREET_NAME], "NO", 2) && !memcmp(apItems[SITUS_STREET_SUFFIX], "NAME", 4))
   {
      strcpy(acStrName, "NO NAME");
      *apItems[SITUS_STREET_SUFFIX] = ' ';
      acAddr1[0] = 0;
   } else if (!strcmp(apItems[SITUS_STREET_SUFFIX], "PARK"))
   {
      sprintf(acStrName, "%s PARK", apItems[SITUS_STREET_NAME]);
      strcat(acAddr1, acStrName);
      *apItems[SITUS_STREET_SUFFIX] = ' ';
   } else 
   {
      strcpy(acStrName, apItems[SITUS_STREET_NAME]);
      strcat(acAddr1, apItems[SITUS_STREET_NAME]);
   }

   if (*apItems[SITUS_STREET_SUFFIX] > ' ')
   {
      char sSfx[32];

      if (!strcmp(apItems[SITUS_STREET_SUFFIX], "R"))
         strcpy(sSfx, "RD");
      else
         strcpy(sSfx, apItems[SITUS_STREET_SUFFIX]);

      strcat(acAddr1, " ");
      strcat(acAddr1, sSfx);

      if (sSfx[0] >= 'A')
      {
         iTmp = GetSfxCodeX(sSfx, acTmp);
         if (iTmp > 0)
            Sfx2Code(acTmp, acCode);
         else
         {
            LogMsg0("*** Invalid suffix: %s, APN=%.14s.  Add it to StrName", sSfx, pOutbuf);
            strcat(acStrName, " ");
            strcat(acStrName, sSfx);

            iBadSuffix++;
            memset(acCode, ' ', SIZ_S_SUFF);
         }
         memcpy(pOutbuf+OFF_S_SUFF, acCode, SIZ_S_SUFF);
      }
   }

   vmemcpy(pOutbuf+OFF_S_STREET, acStrName, SIZ_S_STREET);

   pTmp = myLTrim(apItems[SITUS_SEC_UNIT_NUMBER]);
   if (*pTmp > ' ')
   {
      char sUnitNo[32];

      if (*pTmp == '0')
         iTmp = sprintf(sUnitNo, " #%d", atol(pTmp));
      else if (*apItems[SITUS_SEC_UNIT_NUMBER] == '#' || 
         !memcmp(apItems[SITUS_SEC_UNIT_INDICATOR], "APT", 3) ||
         !memcmp(apItems[SITUS_SEC_UNIT_INDICATOR], "SPC", 3) ||
         !memcmp(apItems[SITUS_SEC_UNIT_INDICATOR], "UNIT", 3) ||
         !memcmp(apItems[SITUS_SEC_UNIT_INDICATOR], "STE", 3))
         iTmp = sprintf(sUnitNo, " #%s", pTmp);
      else
         iTmp = sprintf(sUnitNo, " %s", pTmp);

      strcat(acAddr1, sUnitNo);
      vmemcpy(pOutbuf+OFF_S_UNITNO, &sUnitNo[1], SIZ_S_UNITNO, iTmp-1);
   }

   iTmp = blankRem(acAddr1);
   memcpy(pOutbuf+OFF_S_ADDR_D, acAddr1, iTmp);

   // Situs city
   if (*apItems[SITUS_CITY] > ' ')
   {
      City2Code(apItems[SITUS_CITY], acTmp);
      memcpy(pOutbuf+OFF_S_CITY, acTmp, SIZ_S_CITY);
      memcpy(pOutbuf+OFF_S_ST, "CA", 2);

      // Zip
      lTmp = atol(apItems[SITUS_ZIP_CODE]);
      if (lTmp > 10000 && lTmp < 99999)
         memcpy(pOutbuf+OFF_S_ZIP, apItems[SITUS_ZIP_CODE], 5);

      iTmp = sprintf(acTmp, "%s CA %.5s", apItems[SITUS_CITY], pOutbuf+OFF_S_ZIP);
      vmemcpy(pOutbuf+OFF_S_CTY_ST_D, acTmp, SIZ_S_CTY_ST_D);
   }

   lSitusMatch++;

   // Get next record
   pRec = fgets(acRec, 512, fdSitus);

   return 0;
}

/****************************** Sac_MergeMailing ******************************
 *
 * Merge Mailing address
 * Return 0 if successful, otherwise error.
 *
 *****************************************************************************/

int Sac_MergeMailing(char *pOutbuf)
{
   static   char acRec[512], *pRec=NULL;
   char     acTmp[256], acAddr1[256], *apItems[32], *pTmp;
   long     lTmp;
   int      iRet=0, iTmp;

   if (!pRec)
   {
      pRec = fgets(acRec, 512, fdMail);
   }

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "29402200020027", 14))
   //   iTmp = 0;
#endif

   do 
   {
      if (!pRec)
      {
         fclose(fdMail);
         fdMail = NULL;
         return 1;      // EOF 
      }

      pTmp = strchr(pRec, '|');
      iTmp = memcmp(pOutbuf, pTmp+1, iApnLen);
      if (iTmp > 0)
      {
         if (bDebug)
            LogMsg0("Skip Situs rec: %s", pRec);
         pRec = fgets(acRec, 512, fdMail);
         lMailSkip++;
      }
   } while (iTmp > 0);

   // If not match, return
   if (iTmp)
      return 1;

   // Parse input
   iRet = ParseStringIQ(pRec, '|', MAILING_COLS, apItems);
   if (iRet < MAILING_COLS)
   {
      LogMsg("***** Error: bad mailing record for APN=%.*s (#tokens=%d)", iApnLen, pOutbuf, iRet);
      return -1;
   }

   // Clear old data
   removeMailing(pOutbuf);

   // Merge data
   acAddr1[0] = 0;
   lTmp = atol(apItems[MAILING_STREET_NUMBER]);
   if (lTmp > 0)
   {
      iTmp = sprintf(acTmp, "%d", lTmp);
      memcpy(pOutbuf+OFF_M_STRNUM, acTmp, iTmp);

      strcpy(acAddr1, apItems[MAILING_STREET_NUMBER]);
      strcat(acAddr1, " ");
      if (*apItems[MAILING_PRE_DIRECTION] > ' ')
      {
         strcat(acAddr1, apItems[MAILING_PRE_DIRECTION]);
         strcat(acAddr1, " ");
         memcpy(pOutbuf+OFF_M_DIR, apItems[MAILING_PRE_DIRECTION], strlen(apItems[MAILING_PRE_DIRECTION]));
      }
   }

#ifdef _DEBUG
   //if (*apItems[MAILING_STREET_NAME] == '*')
   //   iTmp = 0;
#endif

   // Street name or PO Box
   if (*apItems[MAILING_STREET_NAME] == '`' || *apItems[MAILING_STREET_NAME] == ' ')
      strcpy(acTmp, apItems[MAILING_STREET_NAME]+1);
   else
      strcpy(acTmp, apItems[MAILING_STREET_NAME]);
   blankRem(acTmp);
   if (!memcmp(acTmp, "PO BOX", 6))
   {
      sprintf(acAddr1, "%s %s", acTmp, apItems[MAILING_PO_BOX_NUMBER]);
      vmemcpy(pOutbuf+OFF_M_STREET, acAddr1, SIZ_M_STREET);
   } else if (acTmp[0] > '0')
   {
      vmemcpy(pOutbuf+OFF_M_STREET, acTmp, SIZ_M_STREET);
      strcat(acAddr1, acTmp);
      if (*apItems[MAILING_STREET_SUFFIX] > '0')
      {
         strcat(acAddr1, " ");
         strcat(acAddr1, apItems[MAILING_STREET_SUFFIX]);
         vmemcpy(pOutbuf+OFF_M_SUFF, apItems[MAILING_STREET_SUFFIX], SIZ_M_SUFF);
      }
   }

   // Copy unit#
   pTmp = myLTrim(apItems[MAILING_SEC_UNIT_NUMBER]);
   if (*pTmp > ' ')
   {
      char sUnitNo[32];

      if (*pTmp == '0')
         iTmp = sprintf(sUnitNo, " #%d", atol(pTmp));
      else if (*apItems[MAILING_SEC_UNIT_NUMBER] == '#')
         iTmp = sprintf(sUnitNo, " %s", pTmp);
      else if (!memcmp(apItems[MAILING_SEC_UNIT_INDICATOR], "APT", 3) ||
         !memcmp(apItems[MAILING_SEC_UNIT_INDICATOR], "SPC", 3) ||
         !memcmp(apItems[MAILING_SEC_UNIT_INDICATOR], "UNIT", 3) ||
         !memcmp(apItems[MAILING_SEC_UNIT_INDICATOR], "STE", 3))
         iTmp = sprintf(sUnitNo, " #%s", pTmp);
      else if (!memcmp(apItems[MAILING_SEC_UNIT_INDICATOR], "FL", 2))
         iTmp = sprintf(sUnitNo, " FL %s", pTmp);
      else
         iTmp = sprintf(sUnitNo, " #%s", pTmp);

      strcat(acAddr1, sUnitNo);
      vmemcpy(pOutbuf+OFF_M_UNITNO, &sUnitNo[1], SIZ_M_UNITNO, iTmp-1);
      vmemcpy(pOutbuf+OFF_M_UNITNOX, &sUnitNo[1], SIZ_M_UNITNOX, iTmp-1);
   }

   iTmp = blankRem(acAddr1);
   vmemcpy(pOutbuf+OFF_M_ADDR_D, acAddr1, SIZ_M_ADDR_D, iTmp);

   // Situs city
   if (*apItems[MAILING_CITY] > ' ')
   {
      vmemcpy(pOutbuf+OFF_M_CITY, apItems[MAILING_CITY], SIZ_M_CITY);
      vmemcpy(pOutbuf+OFF_M_ST, apItems[MAILING_STATE], 2);

      // Zip
      lTmp = atol(apItems[MAILING_ZIP_CODE]);
      if (lTmp > 10000 && lTmp < 99999)
         memcpy(pOutbuf+OFF_M_ZIP, apItems[MAILING_ZIP_CODE], 5);

      sprintf(acTmp, "%s %s %s", apItems[MAILING_CITY], apItems[MAILING_STATE], apItems[MAILING_ZIP_CODE]);
      iTmp = blankRem(acTmp);
      vmemcpy(pOutbuf+OFF_M_CTY_ST_D, acTmp, SIZ_M_CTY_ST_D, iTmp);
   }

   lMailMatch++;

   // Get next record
   pRec = fgets(acRec, 512, fdMail);

   return 0;
}

/******************************** Sac_XlatUseCode ****************************
 *
 * Return pointer to the correct use code.
 *
 *****************************************************************************

char *Sac_XlatUseCode(char *pCntyUse)
{
   int   iTmp;

   iTmp = 0;
   while (Sac_UseTbl3[iTmp].acCntyUse[0])
      if (!memcmp(Sac_UseTbl3[iTmp].acCntyUse, pCntyUse, 3))
         return (char *)&Sac_UseTbl3[iTmp].acStdUse[0];
      else
         iTmp++;

   iTmp = 0;
   while (Sac_UseTbl2[iTmp].acCntyUse[0])
      if (!memcmp(Sac_UseTbl2[iTmp].acCntyUse, pCntyUse, 2))
         return (char *)&Sac_UseTbl2[iTmp].acStdUse[0];
      else
         iTmp++;

   iTmp = 0;
   while (Sac_UseTbl1[iTmp].acCntyUse[0])
      if (Sac_UseTbl1[iTmp].acCntyUse[0] == *pCntyUse)
         return (char *)&Sac_UseTbl1[iTmp].acStdUse[0];
      else
         iTmp++;

   return NULL;
}

/******************************** Sac_MergeOwner *****************************
 *
 * Return 0 if successful, >0 if error
 *
 *****************************************************************************/

void Sac_MergeOwner(char *pOutbuf, char *pNames)
{
   int   iTmp, iTmp1, iRet, iParseType=3;
   char  acTmp1[128], acTmp2[128], acTmp[128], acSave1[64], *pTmp, *pTmp1;
   char  acName1[64], acName2[64], acNames[64];
   bool  bTrust=false;

   OWNER myOwner;

   // Clear output buffer if needed
   removeNames(pOutbuf, true);

   // Initialize
   strcpy(acTmp, pNames);
   blankRem(acTmp);
   pTmp = _strupr(acTmp);
   acName2[0] = 0;
   acSave1[0] = 0;

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "22903110020000", 10))
   //   iTmp = 0;
#endif

   // Update vesting
   iTmp = updateVesting(myCounty.acCntyCode, acTmp, pOutbuf+OFF_VEST, pOutbuf+OFF_ETAL_FLG);

   if (acTmp[0] == '(')
   {
      // Move to the end
      if (pTmp = strchr(acTmp, ')'))
      {
         *(pTmp+1) = 0;
         pTmp += 2;
         strcat(pTmp, " ");
         strcat(pTmp, acTmp);
         strcpy(acTmp, pTmp);
         memcpy(pOutbuf+OFF_VEST, "ES", 2);
      }
   } 
   //else if (pTmp = strchr(acTmp, '('))
   //   *pTmp = 0;        // Drop what in parenthesis

   // Replace '\' with '/'
   iTmp = replChar(acTmp, 92, 47);

   // Save this for Name1 as needed
   strcpy(acNames, acTmp);

   // Eliminate ETAL
   pTmp = strrchr(acTmp, '/');
   if (pTmp)
   {
      if (!memcmp(pTmp, "/ETAL", 5))
      {
         *pTmp = 0;
         pTmp = strrchr(acTmp, '/');
      }
      if (pTmp)
      {
         if (!memcmp(pTmp, "/TR", 3))
         {
            bTrust = true;
            *pTmp = 0;
         }
      }
   }

   // Drop filtered words
   // KURASAKI JOHN/EST OF/KAZUKO  KURASAKI/ETAL
   if ((pTmp=strstr(acTmp, " TR "))  || (pTmp=strstr(acTmp, " TRUSTEE")) ||
       (pTmp=strstr(acTmp, " FBO ")) || (pTmp=strstr(acTmp, "/EST OF"))  ||
       (pTmp=strstr(acTmp, " FAM ")) || (pTmp=strstr(acTmp, " REV "))    ||
       (pTmp=strstr(acTmp, " TRST")) )
   {
      bTrust = true;
      *pTmp = 0;
   }

   // Replace / with & and move it to name1 and name2
   pTmp = (char *)&acTmp[0];
   pTmp1 = (char *)&acTmp1[0];
   iTmp = 0;
   while (*pTmp)
   {
      if (*pTmp == '/')
      {
         iTmp++;
         if (iTmp == 2)
         {
            iTmp = 0;
            *pTmp1 = 0;
            pTmp1 = (char *)&acName2[0];
         } else
         {
            strcpy(pTmp1, " & ");
            pTmp1 += 3;
         }
      } else
         *pTmp1++ = *pTmp;
      pTmp++;
   }
   *pTmp1 = 0;


   if (pTmp=strstr(acTmp1, " TRUST &"))
   {
      bTrust = true;
      *(pTmp+6) = 0;
      strcpy(acName1, acTmp1);
      if (acName2[0])
      {
         sprintf(acTmp, "%s & %s", pTmp+9, acName2);
         strcpy(acName2, acTmp);
      } else
         strcpy(acName2, pTmp+9);
   } else if (pTmp=strstr(acTmp1, " LLC &"))
   {
      bTrust = true;
      *(pTmp+4) = 0;
      strcpy(acName1, acTmp1);
      strcpy(acName2, pTmp+7);
   } else
      strcpy(acName1, acTmp1);

   if (pTmp=strstr(acName1, " TRUST"))
   {
      bTrust = true;
      if ((pTmp1=strstr(acName1, " FAMILY")) || (pTmp1=strstr(acName1, " REVOCABLE")) )
      {
         pTmp = acName1;
         while (pTmp < pTmp1 && !isdigit(*(pTmp)))
            pTmp++;
         if (pTmp < pTmp1)
         {
            strcpy(acSave1, pTmp);
            *pTmp = 0;
         } else
         {
            strcpy(acSave1, pTmp1);
            *pTmp1 = 0;
         }
      } else if (pTmp1=strstr(acName1, " LIVING"))
      {
         strcpy(acSave1, pTmp1);
         *pTmp1 = 0;
      } else if (pTmp1=strstr(acName1, " TEST "))
      {
         strcpy(acSave1, pTmp1);
         *pTmp1 = 0;
      } else
      {
         strcpy(acSave1, pTmp);
         *pTmp = 0;
      }
   }

   // Try to break name
   if (!bTrust)
   {
      char *apTmp1[8], *apTmp2[8];

      pTmp = strchr(acName1, '&');
      if (pTmp)
      {
         *pTmp = 0;
         strcpy(acTmp, acName1);
         iTmp = ParseString(acTmp, ' ', 8, apTmp1);
         *pTmp = '&';
      }

      // We don't break HENRY M/LUPE C GONZALES
      if (pTmp && (iTmp > 2 || (iTmp > 1 && strlen(apTmp1[1]) > 1)) )
      {

         strcpy(acTmp2, pTmp+2);
         iTmp = ParseString(acTmp2, ' ', 8, apTmp2);

         // BUI DAVID PHUC H/VIVIAN PHUONG PHAN
         // VELASQUEZ ANTHONY M/CARRIE ANN A
         if (iTmp > 1 && strlen(apTmp2[iTmp-1]) > 1)
         {
            // If same last name, drop it
            if (!memcmp(apTmp2[iTmp-1], acName1, strlen(apTmp2[iTmp-1])))
               acTmp1[0] = 0;
            else
               // Use last word as last name
               strcpy(acTmp1, apTmp2[iTmp-1]);

            for (iTmp1=0; iTmp1 < iTmp-1; iTmp1++)
            {
               strcat(acTmp1, " ");
               strcat(acTmp1, apTmp2[iTmp1]);
            }

            // Sale last name, append to name1, else make it name2
            if (acTmp1[0] == ' ')
               strcpy(pTmp+1, acTmp1);
            else
            {
               // Terminate name1
               *(pTmp-1) = 0;

               iParseType=4;
               // If name2 already have data, append it to acTmp1
               if (acName2[0])
               {
                  sprintf(acTmp, "%s & %s", acTmp1, acName2);
                  strcpy(acName2, acTmp);
               } else
                  strcpy(acName2, acTmp1);
            }
         }
      }
   }

   // Now parse owners
   memset((void *)&myOwner, 0, sizeof(OWNER));
   iRet = 0;
   if (strchr(acName1, ' '))
   {
      iRet = splitOwner(acName1, &myOwner, iParseType);
      if (iRet == 1 || !bTrust)
         strcpy(acName1, myOwner.acName1);

      // Clear name2 if it is the same as name1
      if (myOwner.acName2[0] && !strcmp(myOwner.acName1, myOwner.acName2))
         myOwner.acName2[0] = 0;

      if (myOwner.acVest[0] > ' ')
         memcpy(pOutbuf+OFF_VEST, myOwner.acVest, strlen(myOwner.acVest));

      // Concat what in saved buffer
      if (acSave1[0])
      {
         sprintf(acTmp1, "%s %s", acName1, acSave1);
         blankRem(acTmp1);
         strcpy(acName1, acTmp1);
      }

      // If name is not swapable, use Name1 for it
      if (!myOwner.acSwapName[0] || (bTrust && iRet != 1))
         strcpy(myOwner.acSwapName, acName1);

   } else if (acSave1[0])
   {
      strcat(acName1, acSave1);
      strcpy(myOwner.acSwapName, acName1);
   } else
   {
      // Couldn't split names
      strcpy(myOwner.acSwapName, acName1);
   }

   // Keep Name1 as is
   //if (bTrust)
   //{
   //   iTmp = strlen(acNames);
   //   if (iTmp > SIZ_NAME1)
   //      iTmp = SIZ_NAME1;
   //   memcpy(pOutbuf+OFF_NAME1, acNames, iTmp);
   //} else
   //{
   //   iTmp = strlen(acName1);
   //   if (iTmp > SIZ_NAME1)
   //      iTmp = SIZ_NAME1;
   //   memcpy(pOutbuf+OFF_NAME1, acName1, iTmp);

   //   if (myOwner.acName2[0])
   //   {
   //      iTmp = strlen(myOwner.acName2);
   //      if (iTmp > SIZ_NAME2)
   //         iTmp = SIZ_NAME2;
   //      memcpy(pOutbuf+OFF_NAME2, myOwner.acName2, iTmp);
   //   } else if (acName2[0] && strchr((char *)&acName2[1], ' '))
   //   {  // Process Name2 when there is more than one word
   //      iRet = splitOwner(acName2, &myOwner, iParseType);
   //      if (iRet >= 0)
   //         strcpy(acName2, myOwner.acName1);

   //      iTmp = strlen(acName2);
   //      if (iTmp > SIZ_NAME2)
   //         iTmp = SIZ_NAME2;
   //      memcpy(pOutbuf+OFF_NAME2, acName2, iTmp);
   //   }
   //}

   vmemcpy(pOutbuf+OFF_NAME1, acNames, SIZ_NAME1);
   vmemcpy(pOutbuf+OFF_NAME_SWAP, myOwner.acSwapName, SIZ_NAME_SWAP);

#ifdef _DEBUG
   //if (myOwner.acName2[0])
   //{
   //   LogMsg("1:%.10s: %s", pOutbuf, myOwner.acName2);
   //} else if (acName2[0])
   //{  // Process Name2 when there is more than one word
   //   LogMsg("2:%.10s: %s", pOutbuf, acName2);
   //}
#endif
}

/************************** Sac_MergeOwnerNames ******************************
 *
 * Merge Owner names from city_full_owner.txt
 * Return 0 if successful, 1 if APN not found, -1 if APN found but no owner name.
 *
 *****************************************************************************/

int Sac_MergeOwnerNames(char *pOutbuf)
{
   static   char acRec[512], *pRec=NULL;
   char     acTmp[256], *apItems[32], *pApn;
   int      iRet=0, iCmp, iNameIdx=0;

   if (!pRec)
   {
      pRec = fgets(acRec, 512, fdOwner);
   }

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "0040370046", 10))
   //   iCmp = 0;
#endif

   do 
   {
      if (!pRec)
      {
         fclose(fdOwner);
         fdOwner = NULL;
         return 1;      // EOF 
      }

      pApn = strchr(pRec, '|');
      iCmp = memcmp(pOutbuf, pApn+1, iApnLen);
      if (iCmp > 0)
      {
         if (bDebug)
            LogMsg0("Skip Owner rec: %s", pRec);
         pRec = fgets(acRec, 512, fdOwner);
         lOwnerSkip++;
      }
   } while (iCmp > 0);

   // If not match, return
   if (iCmp)
      return 1;

   while (iNameIdx < 2 && !iCmp)
   {
      // Parse input
      iRet = ParseStringIQ(pRec, '|', O_COLS, apItems);
      if (iRet < O_COLS)
      {
         LogMsg("***** Error: bad Owner record for APN=%.*s (#tokens=%d)", iApnLen, pOutbuf, iRet);
         return -1;
      }

      if (*apItems[O_OWNER_ID] == '0' || !memcmp(apItems[O_OWNER_ROLE], "INACTIVE", 2) || !memcmp(apItems[O_OWNER_ROLE], "GRANTOR", 6))
      {
         pRec = fgets(acRec, 512, fdOwner);
         pApn = strchr(pRec, '|');
         iCmp = memcmp(pOutbuf, pApn+1, iApnLen);
         if (!iCmp)
            continue;
         break;
      }

      sprintf(acTmp, "%s %s %s", apItems[O_OWNER_LAST_NAME], apItems[O_OWNER_FIRST_NAME], apItems[O_OWNER_MIDDLE_NAME]);
      blankRem(acTmp);

      if (iNameIdx == 0)
      {
         // Update vesting
         updateVesting(myCounty.acCntyCode, acTmp, pOutbuf+OFF_VEST, pOutbuf+OFF_ETAL_FLG);

         // Merge name 1
         Sac_MergeOwner(pOutbuf, acTmp);
      } else
      {
         // Merge Name 2
         vmemcpy(pOutbuf+OFF_NAME2, acTmp, SIZ_NAME2);
      }

      iNameIdx++;
      pRec = fgets(acRec, 512, fdOwner);
      pApn = strchr(pRec, '|');
      iCmp = memcmp(pOutbuf, pApn+1, iApnLen);
   }

   lOwnerMatch++;
   if (!iNameIdx)
   {
      iRet = -1;
      LogMsg("*** No Owner: %.14s", pOutbuf);
   }

   return iRet;
}

/********************************* Sac_MergeAdr ******************************
 *
 * Special case where LAND, PARK, and DR are all legal suffix.
 * 5100   S LAND PARK DR  95822
 * Use custom version of Sac_Sfx.txt to fix this problem
 *
 *****************************************************************************

void Sac_MergeAdr(char *pOutbuf, char *pRollRec)
{
   SAC_ROLL *pRec;
   char     acTmp[256], acAddr1[256], acAddr2[256];
   int      iTmp, iStrNo;
   long     lTmp;
   bool     bNoMail = false;

   ADR_REC  sMailAdr;
   ADR_REC  sSitusAdr;

   pRec = (SAC_ROLL *)pRollRec;

   // Clear old Mailing
   memset(pOutbuf+OFF_M_STRNUM, ' ', SIZ_M_ADDR);
   memset(pOutbuf+OFF_M_ADDR_D, ' ', SIZ_M_ADDRB_D);

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "0160231005", 10) )
   //   iTmp = 0;
#endif
   // Check for blank address
   if (memcmp(pRec->M_Addr1, "     ", 5))
   {
      memcpy(acAddr1, pRec->M_Addr1, RSIZ_M_STREET);
      blankRem(acAddr1, RSIZ_M_STREET);

      // Parsing mail address
      memset((void *)&sMailAdr, 0, sizeof(ADR_REC));
      parseMAdr1(&sMailAdr, acAddr1);
      //parseAdr1U(&sMailAdr, acAddr1, false);

      if (sMailAdr.lStrNum > 0)
      {
         sprintf(acTmp, "%d       ", sMailAdr.lStrNum);
         memcpy(pOutbuf+OFF_M_STRNUM, acTmp, SIZ_M_STRNUM);
         if (sMailAdr.strSub[0] > '0')
         {
            sprintf(acTmp, "%s  ", sMailAdr.strSub);
            memcpy(pOutbuf+OFF_M_STR_SUB, acTmp, SIZ_M_STR_SUB);
         }
      }
      memcpy(pOutbuf+OFF_M_DIR, sMailAdr.strDir, strlen(sMailAdr.strDir));
      memcpy(pOutbuf+OFF_M_STREET, sMailAdr.strName, strlen(sMailAdr.strName));
      memcpy(pOutbuf+OFF_M_SUFF, sMailAdr.strSfx, strlen(sMailAdr.strSfx));

      if (sMailAdr.lStrNum > 0)
      {
         sprintf(acAddr1, "%d %s %s %s %s", sMailAdr.lStrNum, sMailAdr.strSub, sMailAdr.strDir,
            sMailAdr.strName, sMailAdr.strSfx);
         blankRem(acAddr1);
      }

      // Copy unit#
      if (sMailAdr.Unit[0] > ' ')
      {
         if (sMailAdr.Unit[0] == '#')
            iTmp = sprintf(acTmp, " %s", sMailAdr.Unit);
         else
            iTmp = sprintf(acTmp, " #%s", sMailAdr.Unit);
         memcpy(pOutbuf+OFF_M_UNITNO, &acTmp[1], iTmp-1);
         strcat(acAddr1, acTmp);
      }

      memcpy(pOutbuf+OFF_M_ADDR_D, acAddr1, strlen(acAddr1));

      memcpy(sMailAdr.City, pRec->M_City, RSIZ_M_CITY);
      blankRem(sMailAdr.City, SIZ_M_CITY);
      memcpy(pOutbuf+OFF_M_CITY, sMailAdr.City, strlen(sMailAdr.City));
      memcpy(pOutbuf+OFF_M_ST, pRec->M_St, SIZ_M_ST);

      iTmp = atoin(pRec->M_Zip, RSIZ_M_ZIP);
      if (iTmp > 500)
         memcpy(pOutbuf+OFF_M_ZIP, pRec->M_Zip, SIZ_M_ZIP);
      else
         memcpy(pOutbuf+OFF_M_ZIP, BLANK32, SIZ_M_ZIP);

      sMailAdr.City[SIZ_M_CITY] = 0;
      iTmp = sprintf(acAddr2, "%s %.2s %.5s", myTrim(sMailAdr.City), pRec->M_St, pRec->M_Zip);
      memcpy(pOutbuf+OFF_M_CTY_ST_D, acAddr2, iTmp);
   } else
      bNoMail = true;

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "0040084013", 10) )
   //   iTmp = 0;
#endif

   // Situs
   acAddr1[0] = 0;
   acAddr2[0] = 0;
   memset((void *)&sSitusAdr, 0, sizeof(ADR_REC));
   memset(pOutbuf+OFF_S_STRNUM, ' ', SIZ_S_ADDR);
   memset(pOutbuf+OFF_S_ADDR_D, ' ', SIZ_S_ADDRB_D);

   // Copy StrNum
   iStrNo = atoin(pRec->S_StreetNum, RSIZ_S_STRNUM);
   if (iStrNo > 0)
   {
      // Save original HSENO
      memcpy(pOutbuf+OFF_S_HSENO, pRec->S_StreetNum, RSIZ_S_STRNUM);
#ifdef _DEBUG
      //if (isCharIncluded(pRec->S_StreetNum, '-', RSIZ_S_STRNUM))
      //   lRecCnt++;
#endif
      iTmp = sprintf(acAddr1, "%d ", iStrNo);
      memcpy(pOutbuf+OFF_S_STRNUM, acAddr1, iTmp);

      if (pRec->S_StreetFract[1] == '/')
      {
         memcpy(pOutbuf+OFF_S_STR_SUB, pRec->S_StreetFract, SIZ_S_STR_SUB);
         sprintf(acAddr1, "%d %.3s ", iStrNo, pRec->S_StreetFract);
      }
   }

   if (pRec->S_StreetName[0] > ' ')
   {
      // Copy street name
      memcpy(acTmp, pRec->S_StreetName, RSIZ_S_STRNAME);
      blankRem(acTmp, RSIZ_S_STRNAME);

      parseAdr1U(&sSitusAdr, acTmp, true);

      if (sSitusAdr.strDir[0] > ' ')
         memcpy(pOutbuf+OFF_S_DIR, sSitusAdr.strDir, strlen(sSitusAdr.strDir));
      if (sSitusAdr.strName[0] > ' ')
         memcpy(pOutbuf+OFF_S_STREET, sSitusAdr.strName, strlen(sSitusAdr.strName));
      if (sSitusAdr.SfxCode[0] > ' ')
         memcpy(pOutbuf+OFF_S_SUFF, sSitusAdr.SfxCode, strlen(sSitusAdr.SfxCode));

      if (iStrNo > 0)
      {
         sprintf(acAddr1, "%d %s %s %s %s", iStrNo, sSitusAdr.strSub, sSitusAdr.strDir,
            sSitusAdr.strName, sSitusAdr.strSfx);
         blankRem(acAddr1);
      } else
         strcpy(acAddr1, acTmp);

      // Copy unit#
      if (sSitusAdr.Unit[0] > ' ')
      {
         if (sSitusAdr.Unit[0] == '#')
            iTmp = sprintf(acTmp, " %s", sSitusAdr.Unit);
         else
            iTmp = sprintf(acTmp, " #%s", sSitusAdr.Unit);
         memcpy(pOutbuf+OFF_S_UNITNO, &acTmp[1], iTmp-1);
         strcat(acAddr1, acTmp);
      }
      memcpy(pOutbuf+OFF_S_ADDR_D, acAddr1, strlen(acAddr1));
   }

   // state
   memcpy(pOutbuf+OFF_S_ST, "CA", 2);

   // Use mail city and unit# for situs if they have the same addr
   if (!bNoMail && sMailAdr.City[0] > ' ' &&
       !memcmp(pOutbuf+OFF_S_STRNUM, pOutbuf+OFF_M_STRNUM, SIZ_M_STRNUM) &&
       !memcmp(pOutbuf+OFF_S_STREET, pOutbuf+OFF_M_STREET, 8) )
   {
      // Assuming that no situs city and we have to use mail city
      City2Code(sMailAdr.City, sSitusAdr.City);
      if (pRec->S_Zip[0] < '9' && sSitusAdr.City[0] > ' ')
         memcpy(sSitusAdr.Zip, pRec->M_Zip, SIZ_S_ZIP);

      if (*(pOutbuf+OFF_S_UNITNO) == ' ' && *(pOutbuf+OFF_M_UNITNO) > ' ')
      {
         memcpy(pOutbuf+OFF_S_UNITNO, pOutbuf+OFF_M_UNITNO, SIZ_S_UNITNO);
         sprintf(acTmp, " %.*s", SIZ_M_UNITNO, pOutbuf+OFF_M_UNITNO);
         strcat(acAddr1, acTmp);
      }
   }

   lTmp = atoin(pRec->S_Zip, RSIZ_S_ZIP);
   if (lTmp > 500 && sSitusAdr.Zip[0] <= '0')
      memcpy(sSitusAdr.Zip, pRec->S_Zip, SIZ_S_ZIP);

   if (sSitusAdr.City[0] > ' ')
   {
      memcpy(pOutbuf+OFF_S_CITY, sSitusAdr.City, strlen(sSitusAdr.City));
      memcpy(pOutbuf+OFF_S_ZIP, sSitusAdr.Zip, SIZ_S_ZIP);
      iTmp = sprintf(acAddr2, "%s CA %.5s", sMailAdr.City, sSitusAdr.Zip);
      memcpy(pOutbuf+OFF_S_CTY_ST_D, acAddr2, iTmp);
   } else if (sSitusAdr.Zip[0] > '0')
   {
      memcpy(pOutbuf+OFF_S_ZIP, sSitusAdr.Zip, SIZ_S_ZIP);
      iTmp = sprintf(acAddr2, " CA %.5s", sSitusAdr.Zip);
      memcpy(pOutbuf+OFF_S_CTY_ST_D, acAddr2, iTmp);
   }

   // Populate mailing address with situs.
   if (bNoMail)
   {
      // Do this at the MergeAdr step instead.
      memcpy(pOutbuf+OFF_M_STRNUM, pOutbuf+OFF_S_STRNUM, SIZ_S_STRNUM);
      memcpy(pOutbuf+OFF_M_STR_SUB, pOutbuf+OFF_S_STR_SUB, SIZ_M_STR_SUB);
      memcpy(pOutbuf+OFF_M_DIR, pOutbuf+OFF_S_DIR, SIZ_M_DIR);
      memcpy(pOutbuf+OFF_M_STREET, pOutbuf+OFF_S_STREET, SIZ_M_STREET);
      memcpy(pOutbuf+OFF_M_UNITNO, pOutbuf+OFF_S_UNITNO, SIZ_S_UNITNO);

      // Translate suffix
      iTmp = atoin(pOutbuf+OFF_S_SUFF, 3);
      if (iTmp > 0)
      {
         iTmp = sprintf(acTmp, "%s", GetSfxStr(iTmp));
         memcpy(pOutbuf+OFF_M_SUFF, acTmp, iTmp);
      }

      // Translate city
      iTmp = atoin(pOutbuf+OFF_S_CITY, 3);
      if (iTmp > 0)
      {
         iTmp = sprintf(acTmp, "%s", GetCityName(iTmp));
         memcpy(pOutbuf+OFF_M_CITY, acTmp, iTmp);
      }
      memcpy(pOutbuf+OFF_M_ST, pOutbuf+OFF_S_ST, SIZ_M_ST);
      memcpy(pOutbuf+OFF_M_ZIP, pOutbuf+OFF_S_ZIP, SIZ_M_ZIP);
      memcpy(pOutbuf+OFF_M_ADDR_D, pOutbuf+OFF_S_ADDR_D, SIZ_M_ADDR_D);
      memcpy(pOutbuf+OFF_M_CTY_ST_D, pOutbuf+OFF_S_CTY_ST_D, SIZ_M_CTY_ST_D);
   }
}

*/

/******************************* Sac_ParseAdr() *****************************
 *
 * Parse address with unit number without mark
 * 123 MAIN DR A
 * If last char is an alpha in "N,S,E,W", keep them in street name
 *
 * Develop for SAC.  Assuming there is no COUNTY RD
 *
 ****************************************************************************/

void Sac_ParseAdr(ADR_REC *pAdrRec, char *pAddr, bool bNoStrNum)
{
   char  acAdr[256], acStr[256], acTmp[256], acSfx[64], *apItems[16], *pTmp;
   char *pAdr;
   int   iCnt, iTmp, iIdx, iDir;
   bool  bRet, bDir;

   // Check for PO Box
   if (!memcmp(pAddr, "PO B", 4) ||
       !memcmp(pAddr, "P.O. B", 6) ||
       !memcmp(pAddr, "P O B", 5) )
   {
      strcpy(pAdrRec->strName, pAddr);
      return;
   }

   pAdr = pAddr;
   if (*pAdr == '%')
   {
      pTmp = strchr(pAdr+3, ' ');
      if (pTmp && isdigit(*++pTmp))
         pAdr = pTmp;
      else
         return;        // bad address
   }

   // Remove leading space - SFX
   while (*pAdr && *pAdr == ' ') pAdr++;

   // Remove '.' and extra space
   iTmp = 0;
   while (*pAdr)
   {
      // Also remove single quote
      if (*pAdr != '.' && *pAdr != ',' && *pAdr != 39)
         acAdr[iTmp++] = *pAdr;

      // Remove multi-space
      if (*pAdr == ' ')
         while (*pAdr == ' ') pAdr++;
      else
         pAdr++;
   }

   acAdr[iTmp] = 0;
   acStr[0] = 0;

   // Tokenize address
   iCnt = ParseString(acAdr, 32, 16, apItems);

   // If strNum is 0, do not parse address
   iTmp = atoi(apItems[0]);
   if ((iTmp == 0 && !bNoStrNum) || iCnt == 1)
   {
      strcpy(acStr, pAddr);
      pAdrRec->lStrNum = 0;
   } else
   {
      if (!bNoStrNum && iTmp > 0)
      {
         pAdrRec->lStrNum = iTmp;
         sprintf(acTmp, "%d", iTmp);
         strcpy(pAdrRec->strNum, acTmp);
         iIdx = 1;
      } else
         iIdx = 0;

      // Fraction - Make sure this is not the last token
      if (strchr(apItems[iIdx], '/') && (iCnt > (iIdx+1)) && (strlen(apItems[iIdx]) <= SIZ_S_STR_SUB))
         strcpy(pAdrRec->strSub, apItems[iIdx++]);

      // Unit is sometimes placed after strnum as in: 9416 #A CARLTON OAKS
      if (apItems[iIdx][0] == '#')
      {
         strncpy(pAdrRec->Unit, apItems[iIdx], SIZ_M_UNITNO);
         strncpy(pAdrRec->UnitNox, apItems[iIdx], SIZ_M_UNITNOX);
         iIdx++;
      } else if (iCnt > 2 && (iTmp=GetSfxDev(apItems[iCnt-2])) && !isDir(apItems[iCnt-1]))
      {
         // Assume there is no COUNTY RD
         if (memcmp(apItems[iCnt-2], "RT", 2) &&
             memcmp(apItems[iCnt-2], "ROUTE", 5) &&
             memcmp(apItems[iCnt-2], "HWY", 3) && 
             memcmp(apItems[iCnt-2], "HIGHWAY", 7) &&
            (isdigit(*apItems[iCnt-1]) || strlen(apItems[iCnt-1]) == 1) // Unit is number or single letter
            )
         {
            strncpy(pAdrRec->Unit, apItems[iCnt-1], SIZ_M_UNITNO);
            strncpy(pAdrRec->UnitNox, apItems[iCnt-1], SIZ_M_UNITNOX);
            iCnt--;
         }
      } else if (isDir(apItems[iCnt-1]))
      {
         acStr[0] = 0;
         for (;iIdx < iCnt; iIdx++)
         {
            strcat(acStr, apItems[iIdx]);
            strcat(acStr, " ");
         }
         strcpy(pAdrRec->strName, acStr);
         return;
      }


      if (iIdx >= iCnt)
      {
         // Bad address
         return;
      }

      // Dir
      iDir = 0;
      bDir = false;
      strcpy(acSfx, apItems[iIdx]);
      if (strlen(apItems[iIdx]) < 3)
      {
         if (acSfx[1] == '.')
            acSfx[1] = 0;

         while (iDir <= 7)
         {
            if (!strcmp(acSfx, asDir[iDir]))
            {
               bDir = true;
               break;
            }
            iDir++;
         }
      } else
      {
         // Check for long direction
         while (iDir < 4)
         {
            if (!strcmp(apItems[iIdx], asDir1[iDir]))
            {
               bDir = true;
               break;
            }
            iDir++;
         }
      }

      // Possible cases E ST or W RD or E AL CT
      // Check for suffix
      bRet = false;
      if (bDir && iCnt > 2)
      {
         // If next item is suffix, then this one is not direction
         // if (iTmp=GetSfxCode(apItems[iIdx+1]) && iCnt==iIdx+2)
         // The above statement won't work if Unit# is included.
         // Watch for S LAND PARK DR (SAC - 0160042004)
         if (iTmp=GetSfxCodeX(apItems[iIdx+1], acSfx))
         {
            strcpy(acStr, apItems[iIdx]);
            sprintf(pAdrRec->SfxCode, "%d", iTmp);
            strcpy(pAdrRec->strSfx, acSfx);
            strcpy(pAdrRec->SfxName, acSfx);
            bRet = true;         // Done
         } else
         {
            // This is direction
            strcpy(pAdrRec->strDir, asDir[iDir]);
            iIdx++;
         }
      }

      // Check for unit #
      if (apItems[iCnt-1][0] == '#')
      {
         strncpy(pAdrRec->Unit, (char *)&apItems[iCnt-1][0], SIZ_M_UNITNO);
         iCnt--;

         // If last token is STE or SUITE, remove it.
         if (!strcmp(apItems[iCnt-1], "STE") || !strcmp(apItems[iCnt-1], "SUITE")
            || !strcmp(apItems[iCnt-1], "UNIT") || !strcmp(apItems[iCnt-1], "APT")
            || !strcmp(apItems[iCnt-1], "SP") || !strcmp(apItems[iCnt-1], "RM") )
            iCnt--;
      } else if (!strcmp(apItems[iCnt-2], "STE") || !strcmp(apItems[iCnt-2], "SUITE")
         || !strcmp(apItems[iCnt-2], "UNIT") || !strcmp(apItems[iCnt-2], "UNIT#")
         || !strcmp(apItems[iCnt-2], "APT") || !strcmp(apItems[iCnt-2], "APT#")
         || !strcmp(apItems[iCnt-2], "RM") || !strcmp(apItems[iCnt-2], "SP") )
      {
         sprintf(acTmp, "#%s", apItems[iCnt-1]);
         strncpy(pAdrRec->Unit, acTmp, SIZ_M_UNITNO);
         strncpy(pAdrRec->UnitNox, acTmp, SIZ_M_UNITNOX);
         iCnt -= 2;
      } else if ((pTmp=strchr(apItems[iCnt-1], '#')))
      {  // 15025 NE SACRAMENTO ST#54, 838 S GRAND AVE APT#611
         if (!memcmp(apItems[iCnt-1], "SP", 2))
         {
            *pTmp = ' ';
         } else
         {
            strncpy(pAdrRec->Unit, pTmp, SIZ_M_UNITNO);
            strncpy(pAdrRec->UnitNox, pTmp, SIZ_M_UNITNOX);
            *pTmp = 0;
            if (!strcmp(apItems[iCnt-1], "APT"))
               iCnt--;
         }
      } else if (!strcmp(apItems[iCnt-2], "LOT") && atoi(apItems[iCnt-1]) > 0)
      {  // 245 WILDWOOD DR LOT 183, 45055 E FLORIDA AVE SP 36
         sprintf(acTmp, "#%s     ", apItems[iCnt-1]);
         strncpy(pAdrRec->Unit, acTmp, SIZ_M_UNITNO);
         strncpy(pAdrRec->UnitNox, acTmp, SIZ_M_UNITNOX);
         iCnt -= 2;
      } else if (iCnt > 3 && atoi(apItems[iCnt-1]) > 0)
      {  // 555 N DANEBO 33
         // Not 21051 STATE ROUTE 772, 5323 HERMITAGE AVE# 1, 3100-R WEST HWY 86
         if (strcmp(apItems[iCnt-2], "ROUTE") && strcmp(apItems[iCnt-2], "HWY")
            && strcmp(apItems[iCnt-2], "BOX") && strcmp(apItems[iCnt-2], "ROAD")
            && strcmp(apItems[iCnt-2], "SP")  && strcmp(apItems[iCnt-2], "HIGHWAY"))   // MER
         {
            // Check for 123 COUNTY RD 105 on SIS->001110050000
            if (!strstr(pAddr, "COUNTY RD"))
            {
               sprintf(acTmp, "#%s     ", apItems[iCnt-1]);
               strncpy(pAdrRec->Unit, acTmp, SIZ_M_UNITNO);
               strncpy(pAdrRec->UnitNox, acTmp, SIZ_M_UNITNOX);
               iCnt--;
            } else
            {
               sprintf(acStr, "COUNTY ROAD %s", apItems[iCnt-1]);
               bRet = true;
            }

         }

         if (pTmp=strchr(apItems[iCnt-1], '#'))
            *pTmp = 0;
      }

      // Not direction, token is str name
      if (!bRet && iIdx < iCnt)
      {
         // Check suffix
         if (iTmp=GetSfxCodeX(apItems[iCnt-1], acSfx))
         {
            sprintf(pAdrRec->SfxCode, "%d", iTmp);
            strcpy(pAdrRec->strSfx, acSfx);
            strcpy(pAdrRec->SfxName, acSfx);
            while (iIdx < iCnt-1)
            {
               // If multiple address, take the first one.
               // This loop break after first suffix found, ignore the rest.
               // We also must avoid strname like 123 ST PAUL BLVD.  StrSfx must
               // be indexed > 1
               if ((iIdx > 1) &&
                   (iIdx < iCnt-2) &&
                   (iTmp=GetSfxCodeX(apItems[iIdx], acSfx)) )
               {
                  sprintf(pAdrRec->SfxCode, "%d", iTmp);
                  strcpy(pAdrRec->strSfx, acSfx);
                  strcpy(pAdrRec->SfxName, acSfx);
                  break;
               }
               strcat(acStr, apItems[iIdx++]);
               if (iIdx < iCnt)
                  strcat(acStr, " ");
            }
         } else
         {
            // Scan for suffix in multi address cases.  Keep the first one
            // 123 MAIN ST 540 N SANTA ANA BLVD
            while (iIdx < iCnt)
            {
               iTmp=GetSfxCodeX(apItems[iIdx], acSfx);
               if (iIdx > 1 && iTmp > 0 && strcmp(acSfx, "HWY") && strcmp(acSfx, "RT"))
               {
                  sprintf(pAdrRec->SfxCode, "%d", iTmp);
                  strcpy(pAdrRec->strSfx, acSfx);
                  strcpy(pAdrRec->SfxName, acSfx);
                  break;
               }

               strcat(acStr, apItems[iIdx++]);
               if (!strcmp(acSfx, "HWY") || !strcmp(acSfx, "RT"))
               {
                  if (!strcmp(apItems[iIdx], "16"))
                     strcpy(apItems[iIdx], "160");
                  if (!strcmp(acSfx, "RT"))
                     iTmp = 0;
               }

               if (iIdx < iCnt)
                  strcat(acStr, " ");
            }
         }
      }
   }
   strncpy(pAdrRec->strName, acStr, SIZ_M_STREET);
}

/********************************* Sac_MergeAdr ******************************
 *
 * Special case where LAND, PARK, and DR are all legal suffix.
 * 5100   S LAND PARK DR  95822
 * Use custom version of Sac_Sfx.txt to fix this problem
 * 08/21/2014 First try to get City & Zip code from GIS extract. If not found,
 *            and Zip code is in roll, look up city name from USPS list.
 *
 *****************************************************************************/

void Sac_MergeAdr(char *pOutbuf, SAC_MADR *pMailing, SAC_SADR *pSitus)
{
   char     acTmp[256], acAddr1[256], acAddr2[256], acCode[32], acCity[32], *pTmp, acZip[16];
   int      iTmp, iStrNo, iStrNameCmpLen;
   bool     bNoMail = false;

   ADR_REC  sMailAdr;
   ADR_REC  sSitusAdr;

   // Clear old Mailing
   removeMailing(pOutbuf, false);

#ifdef _DEBUG
   // (!memcmp(pOutbuf, "078041001400", 10) )
   //   iTmp = 0;
#endif
   // Check for blank address
   if (memcmp(pMailing->M_Addr1, "     ", 5))
   {
      memcpy(acAddr1, pMailing->M_Addr1, RSIZ_M_STREET);
      blankRem(acAddr1, RSIZ_M_STREET);

      // Parsing mail address
      memset((void *)&sMailAdr, 0, sizeof(ADR_REC));
      //parseMAdr1(&sMailAdr, acAddr1);
      Sac_ParseAdr(&sMailAdr, acAddr1, false);

      if (sMailAdr.lStrNum > 0)
      {
         sprintf(acTmp, "%d       ", sMailAdr.lStrNum);
         memcpy(pOutbuf+OFF_M_STRNUM, acTmp, SIZ_M_STRNUM);
         if (sMailAdr.strSub[0] > '0')
         {
            sprintf(acTmp, "%s  ", sMailAdr.strSub);
            memcpy(pOutbuf+OFF_M_STR_SUB, acTmp, SIZ_M_STR_SUB);
         }
      }
      memcpy(pOutbuf+OFF_M_DIR, sMailAdr.strDir, strlen(sMailAdr.strDir));
      memcpy(pOutbuf+OFF_M_STREET, sMailAdr.strName, strlen(sMailAdr.strName));
      memcpy(pOutbuf+OFF_M_SUFF, sMailAdr.strSfx, strlen(sMailAdr.strSfx));

      if (sMailAdr.lStrNum > 0)
      {
         sprintf(acAddr1, "%d %s %s %s %s", sMailAdr.lStrNum, sMailAdr.strSub, sMailAdr.strDir,
            sMailAdr.strName, sMailAdr.strSfx);
         blankRem(acAddr1);
      }

      // Copy unit#
      if (sMailAdr.Unit[0] > ' ')
      {
         vmemcpy(pOutbuf+OFF_M_UNITNOX, sMailAdr.UnitNox, SIZ_M_UNITNOX);
         if (sMailAdr.Unit[0] == '#')
            iTmp = sprintf(acTmp, " %s", sMailAdr.Unit);
         else
            iTmp = sprintf(acTmp, " #%s", sMailAdr.Unit);
         vmemcpy(pOutbuf+OFF_M_UNITNO, &acTmp[1], SIZ_M_UNITNO, iTmp-1);
         strcat(acAddr1, acTmp);
      }

      memcpy(pOutbuf+OFF_M_ADDR_D, acAddr1, strlen(acAddr1));
      memcpy(sMailAdr.City, pMailing->M_City, RSIZ_M_CITY);
      blankRem(sMailAdr.City, SIZ_M_CITY);
      memcpy(pOutbuf+OFF_M_CITY, sMailAdr.City, strlen(sMailAdr.City));
      memcpy(pOutbuf+OFF_M_ST, pMailing->M_St, SIZ_M_ST);

      iTmp = atoin(pMailing->M_Zip, RSIZ_M_ZIP);
      if (iTmp > 500)
         memcpy(pOutbuf+OFF_M_ZIP, pMailing->M_Zip, SIZ_M_ZIP);
      else
         memcpy(pOutbuf+OFF_M_ZIP, BLANK32, SIZ_M_ZIP);

      sMailAdr.City[SIZ_M_CITY] = 0;
      iTmp = sprintf(acAddr2, "%s %.2s %.5s", myTrim(sMailAdr.City), pMailing->M_St, pMailing->M_Zip);
      memcpy(pOutbuf+OFF_M_CTY_ST_D, acAddr2, iTmp);
   } else
      bNoMail = true;

#ifdef _DEBUG
   // 30   E AL CT
   //if (!memcmp(pOutbuf, "23704430090000", 10) )
   //   iTmp = 0;
#endif

   // Situs
   memset((void *)&sSitusAdr, 0, sizeof(ADR_REC));
   removeSitus(pOutbuf);

   // Copy Mail adr to situs if strnum and first word in strname are matched
   iStrNo = atoin(pSitus->S_StreetNum, RSIZ_S_STRNUM);
   pTmp = strchr(pOutbuf+OFF_M_STREET, ' ');
   iStrNameCmpLen = pTmp-(pOutbuf+OFF_M_STREET-2);
   if (iStrNo > 0)
   {
      acAddr1[0] = 0;
      iTmp = sprintf(acAddr1, "%d ", iStrNo);
      memcpy(pOutbuf+OFF_S_STRNUM, acAddr1, iTmp);

      // Save original HSENO
      memcpy(pOutbuf+OFF_S_HSENO, pSitus->S_StreetNum, RSIZ_S_STRNUM);

      if (pSitus->S_StreetFract[1] == '/')
      {
         memcpy(pOutbuf+OFF_S_STR_SUB, pSitus->S_StreetFract, SIZ_S_STR_SUB);
         sprintf(acAddr1, "%d %.3s ", iStrNo, pSitus->S_StreetFract);
      }

      if (pSitus->S_StreetName[0] > ' ')
      {
         // Copy street name
         memcpy(acTmp, pSitus->S_StreetName, RSIZ_S_STRNAME);
         blankRem(acTmp, RSIZ_S_STRNAME);

         // Street name got chopped off
         // CAMINO DEL REY S --> CAMINO DEL REY ST
         if (!strcmp(acTmp, "CAMINO DEL REY S"))
            strcat(acTmp, "T");
         else if (!strcmp(acTmp, "STANFORD COURT L"))
            strcat(acTmp, "N");
         else if (!strcmp(acTmp, "LAGUNA SPRINGS W"))
            strcat(acTmp, "Y");
         else if (!strcmp(acTmp, "X ST BROADWAY AL"))
            strcat(acTmp, "LEY");
         else if (!strcmp(acTmp, "BRIGHTON BEACH W") || !strcmp(acTmp, "BRIGHTON BCH W"))
            strcpy(acTmp, "BRIGHTON BEACH WY");
         else if (!strcmp(acTmp, "PLEASANT GROVE S") || !strcmp(acTmp, "PLEASANT GRV SCH"))
            strcpy(acTmp, "PLEASANT GROVE SCHOOL RD");
         else if (!strcmp(acTmp, "HOLLOW SPRINGS W"))
            strcat(acTmp, "Y");
         else if (!strcmp(acTmp, "ARELLANO CREEK C"))
            strcat(acTmp, "T");
         else if (!strcmp(acTmp, "VIOLET SPRINGS C"))
            strcat(acTmp, "T");
         else if (!memcmp(acTmp, "MURIETA SOUTH P", 15))
            strcpy(acTmp, "MURIETA SOUTH PKWY");
         else if (!strcmp(acTmp, "ELK GROVE FLORIN"))
            strcat(acTmp, " RD");
         else if (!strcmp(acTmp, "RANDALL ISLAND R"))
            strcat(acTmp, "D");
         else if (!strcmp(acTmp, "POINT PLEASANT R"))
            strcat(acTmp, "D");
         else if (!strcmp(acTmp, "SAGE THRASHER CI"))
            strcat(acTmp, "R");
         else if (!strcmp(acTmp, "CINNAMON TEAL CI"))
            strcat(acTmp, "R");
         else if (!strcmp(acTmp, "SPRING BLOSSOM P"))
            strcat(acTmp, "L");
         else if (!strcmp(acTmp, "CYPRESS GARDEN L"))
            strcat(acTmp, "N");
         else if (!strcmp(acTmp, "MOUNTAIN VISTA C"))
            strcat(acTmp, "IR");
         else if (!strcmp(acTmp, "ORCHARD VALLEY D"))
            strcat(acTmp, "R");
         else if (!strcmp(acTmp, "VALENSIN RANCH R"))
            strcat(acTmp, "D");
         else if (!strcmp(acTmp, "PARSONS LANDING"))
            strcat(acTmp, " ST");
         else if (!strcmp(acTmp, "TAVERNOR TRAIL L"))
            strcat(acTmp, "N");
         else if (!strcmp(acTmp, "ALTA MESA EAST R"))
            strcat(acTmp, "D");
         else if (!strcmp(acTmp, "HOWARD LANDING R"))
            strcat(acTmp, "D");
         else if (!strcmp(acTmp, "WALKER LANDING R"))
            strcat(acTmp, "D");
         else if (!strcmp(acTmp, "W WALKER LANDING"))
            strcat(acTmp, " RD");
         else if (!strcmp(acTmp, "CRYSTAL SPRING L"))
            strcat(acTmp, "N");
         else if (!strcmp(acTmp, "STERLING GROVE D"))
            strcat(acTmp, "R");
         else if (!strcmp(acTmp, "SILVER CYPRESS C"))
            strcat(acTmp, "T");
         else if (!strcmp(acTmp, "MORGANS RANCH CI"))
            strcat(acTmp, "R");
         else if (!strcmp(acTmp, "GOLDEN HEIGHTS D"))
            strcat(acTmp, "R");
         else if (!strcmp(acTmp, "VICTORIAN PARK D"))
            strcat(acTmp, "R");
         else if (!strcmp(acTmp, "JACKSON SLOUGH R"))
            strcat(acTmp, "D");
         else if (!strcmp(acTmp, "BRANNAN ISLAND R"))
            strcat(acTmp, "D");
         else if (!strcmp(acTmp, "W BRANNON ISLAND"))
            strcat(acTmp, " RD");
         else if (!strcmp(acTmp, "TYLER ISLAND BRI") || !memcmp(acTmp, "TYLER IS B", 10) || !strcmp(acTmp, "W TYLER ISLAND B"))
            strcpy(acTmp, "W TYLER ISLAND BRIDGE RD");
         else if (!strcmp(acTmp, "SHERMAN ISLAND C"))
            strcpy(acTmp, "SHERMAN ISLAND CRS RD");
         else if (!strcmp(acTmp, "FREDERICKSBURG W"))
            strcat(acTmp, "Y");
         else if (!strcmp(acTmp, "MOUNT STEPHENS C"))
            strcat(acTmp, "T");
         else if (!strcmp(acTmp, "COVERED WAGON CI"))
            strcat(acTmp, "R");
         else if (!strcmp(acTmp, "ANTELOPE NORTH R"))
            strcat(acTmp, "D");
         else if (!strcmp(acTmp, "OLD ANTELOPE NOR"))
            strcpy(acTmp, "OLD ANTELOPE N RD");
         else if (!strcmp(acTmp, "ANTELOPE HILLS D"))
            strcat(acTmp, "R");
         else if (!strcmp(acTmp, "MCCLELLAN PARK D"))
            strcat(acTmp, "R");
         else if (!strcmp(acTmp, "ROCK HOUSE CRS"))
            strcpy(acTmp, "ROCK HOUSE CIR S");
         else if (!strcmp(acTmp, "WILHAGGIN PARK L"))
            strcat(acTmp, "N");         
         else if (!strcmp(acTmp, "39TH ST 40TH ST"))
            strcpy(acTmp, "39TH ST");
         else if (!strcmp(acTmp, "MARTIN LUTHER KI"))
            strcat(acTmp, "NG JR BLVD");         
         else if (!strcmp(acTmp, "MARTIN L KING BL"))
            strcpy(acTmp, "MARTIN LUTHER KING JR BLVD");
         else if (!strcmp(acTmp, "PARK PL"))
            strcpy(acTmp, "PARK PLACE CT");
         else if (!strcmp(acTmp, "LE PAGE CT"))
            strcpy(acTmp, "LEPAGE CT");
         else if (!strcmp(acTmp, "MC DERBY CT"))
            strcpy(acTmp, "MCDERBY CT");
         else if (!strcmp(acTmp, "BITTER CREEK DR"))
            strcpy(acTmp, "BITTERCREEK DR");
         else if (!strcmp(acTmp, "GLEN HOLLY WY"))
            strcpy(acTmp, "GLENN HOLLY WY");
         else if (!strcmp(acTmp, "BLACK BIRD DR"))
            strcpy(acTmp, "BLACKBIRD LN");
         else if (!strcmp(acTmp, "PARKCITY DR"))
            strcpy(acTmp, "PARK CITY DR");
         else if (!strcmp(acTmp, "RIVERGLADE CT"))
            strcpy(acTmp, "RIVER GLADE CT");
         else if (!strcmp(acTmp, "DOWNRIVER CT"))
            strcpy(acTmp, "DOWN RIVER CT");
         else if (!strcmp(acTmp, "SEA GULL WY"))
            strcpy(acTmp, "SEAGULL WY");
         else if (!strcmp(acTmp, "STILLBAY CT"))
            strcpy(acTmp, "STILL BAY CT");
         else if (!strcmp(acTmp, "DESERT WOOD CT"))
            strcpy(acTmp, "DESERTWOOD CT");
         else if (!strcmp(acTmp, "MAC FADDEN DR"))
            strcpy(acTmp, "MACFADDEN DR");
         else if (!strcmp(acTmp, "ELMANTO DR"))
            strcpy(acTmp, "EL MANTO DR");
         else if (!strcmp(acTmp, "FLORIN PRKNS RD"))
            strcpy(acTmp, "FLORIN PERKINS RD");
         else if (!strcmp(acTmp, "ACCORDION WY"))
            strcpy(acTmp, "ACCORDIAN WY");
         else if (!strcmp(acTmp, "KINGSPOINT CT"))
            strcpy(acTmp, "KINGS POINT CT");

         else if (!strcmp(acTmp, "JADE SPRING WY"))
            strcpy(acTmp, "JADE SPRINGS WY");
         else if (!strcmp(acTmp, "COUNTRY GRDN DR"))
            strcpy(acTmp, "COUNTRY GARDEN DR");
         else if (!strcmp(acTmp, "APPLESBURY CT"))
            strcpy(acTmp, "APPLEBURY CT");
         else if (!strcmp(acTmp, "BLECKLEY ST"))
            strcpy(acTmp, "BLECKELY ST");
         else if (!strcmp(acTmp, "HEAVY TREE CT"))
            strcpy(acTmp, "HEAVYTREE CT");
         else if (!strcmp(acTmp, "BRILLANCE PL"))
            strcpy(acTmp, "BRILLIANCE PL");

         else if (!strcmp(acTmp, "STARGLOW PL"))
            strcpy(acTmp, "STAR GLOW PL");
         else if (!strcmp(acTmp, "DE BEERS PL"))
            strcpy(acTmp, "DEBEERS PL");
         else if (!strcmp(acTmp, "STERLING STNE PL"))
            strcpy(acTmp, "STERLING STONE PL");
         else if (!strcmp(acTmp, "GOLDFLAKE CT"))
            strcpy(acTmp, "GOLD FLAKE CT");
         else if (!strcmp(acTmp, "DRAKESHIRE SQ"))
            strcpy(acTmp, "DRAKE SHIRE SQ");
         else if (!strcmp(acTmp, "GOLDENEYE CT"))
            strcpy(acTmp, "GOLDEN EYE CT");
         else if (!strcmp(acTmp, "STONE MILL DR"))
            strcpy(acTmp, "STONEMILL DR");
         else if (!strcmp(acTmp, "SUMMERCLOUD CT"))
            strcpy(acTmp, "SUMMER CLOUD CT");
         else if (!strcmp(acTmp, "WOODGLENN DR"))
            strcpy(acTmp, "WOODGLEN DR");
         
         if (!strcmp(acTmp, "TALLGRASS CT"))
            strcpy(acTmp, "TALL GRASS CT");
         else if (!strcmp(acTmp, "HOMESTEAD HLS CT"))
            strcpy(acTmp, "HOMESTEAD HILLS CT");
         else if (!strcmp(acTmp, "NOAHBLOMQUIST WY"))
            strcpy(acTmp, "NOAH BLOMQUIST WY");
         else if (!strcmp(acTmp, "LA ESPOSO CT"))
            strcpy(acTmp, "LA ESPOSA CT");
         else if (!strcmp(acTmp, "OLD PLCRVILLE RD") || !strcmp(acTmp, "OLD PLACERVLE RD"))
            strcpy(acTmp, "OLD PLACERVILLE RD");
         else if (!strcmp(acTmp, "ABBOT CT"))
            strcpy(acTmp, "ABBOTT CT");
         else if (!strcmp(acTmp, "SALMON FLLS DR"))
            strcpy(acTmp, "SALMON FALLS DR");
         else if (!strcmp(acTmp, "BRIAR CLIFF WY"))
            strcpy(acTmp, "BRIARCLIFF WY");
         else if (!strcmp(acTmp, "ORCHARD WDS CR"))
            strcpy(acTmp, "ORCHARD WOODS CIR");
         else if (!strcmp(acTmp, "DEROSA CT"))
            strcpy(acTmp, "DE ROSA CT");
         else if (!strcmp(acTmp, "ELK POINT CT"))
            strcpy(acTmp, "ELKPOINT CT");
         else if (!strcmp(acTmp, "SUN COVE LN"))
            strcpy(acTmp, "SUNCOVE LN");
         else if (!strcmp(acTmp, "BOTTLE BRUSH CT"))
            strcpy(acTmp, "BOTTLEBRUSH CT");
         else if (!strcmp(acTmp, "LOGAN BERRY PL"))
            strcpy(acTmp, "LOGANBERRY PL");
         else if (!strcmp(acTmp, "SUMMERSUN WY"))
            strcpy(acTmp, "SUMMER SUN WY");

         else if (!strcmp(acTmp, "WETHERLY CT"))
            strcpy(acTmp, "WEATHERLY CT");
         else if (!strcmp(acTmp, "WIMBLY CT"))
            strcpy(acTmp, "WIMBLEY CT");
         else if (!strcmp(acTmp, "CASTLE HAVEN CT"))
            strcpy(acTmp, "CASTLEHAVEN CT");
         else if (!strcmp(acTmp, "LAGUNA SPGS DR") || !strcmp(acTmp, "LAGUNA SPRINGS D"))
            strcpy(acTmp, "LAGUNA SPRINGS DR");
         else if (!strcmp(acTmp, "VILLAGE CNTR DR") || !strcmp(acTmp, "VILLAGE CENTRE D"))
            strcpy(acTmp, "VILLAGE CENTRE DR");
         
         if (!strcmp(acTmp, "FOX TROT CT"))
            strcpy(acTmp, "FOXTROT CT");
         else if (!strcmp(acTmp, "WHITEHORSE WY"))
            strcpy(acTmp, "WHITE HORSE WY");
         else if (!strcmp(acTmp, "SIDE SADDLE DR"))
            strcpy(acTmp, "SIDESADDLE DR");
         else if (!strcmp(acTmp, "WOOD LILLY WY"))
            strcpy(acTmp, "WOOD LILY WY");
         else if (!strcmp(acTmp, "SILOUETTE CT"))
            strcpy(acTmp, "SILHOUETTE CT");
         else if (!strcmp(acTmp, "ELKTREE WY"))
            strcpy(acTmp, "ELK TREE WY");
         else if (!strcmp(acTmp, "PARKGROVE CT"))
            strcpy(acTmp, "PARK GROVE CT");
         else if (!strcmp(acTmp, "PARKGLEN CT"))
            strcpy(acTmp, "PARK GLEN CT");
         else if (!strcmp(acTmp, "MEADOWTREE CT"))
            strcpy(acTmp, "MEADOW TREE CT");
         else if (!strcmp(acTmp, "MEADOWCREST CT"))
            strcpy(acTmp, "MEADOW CREST CT");
         else if (!strcmp(acTmp, "RUBY MERILOT PL"))
            strcpy(acTmp, "RUBY MERLOT PL");
         else if (!strcmp(acTmp, "BEAVER PKWY"))
            strcpy(acTmp, "BEAVER PARK WAY");
         else if (!strcmp(acTmp, "WHITTEMORE CT"))
            strcpy(acTmp, "WHITMORE CT");

         if (!strcmp(acTmp, "SILVER SPGS LN"))
            strcpy(acTmp, "SILVER SPRINGS LN");
         else if (!strcmp(acTmp, "VISTARIDGE DR"))
            strcpy(acTmp, "VISTA RIDGE DR");
         else if (!strcmp(acTmp, "PINEGROVE WY"))
            strcpy(acTmp, "PINE GROVE WY");
         else if (!strcmp(acTmp, "BOULDER CNYN WY") || !strcmp(acTmp, "BOULDER CANYON W"))
            strcpy(acTmp, "BOULDER CANYON WY");
         else if (!strcmp(acTmp, "AMERICAN RIVER C"))
            strcpy(acTmp, "AMERICAN RIVER CANYON DR");
         else if (!strcmp(acTmp, "INDIAN SPGS WY"))
            strcpy(acTmp, "INDIAN SPRINGS WY");
         else if (!strcmp(acTmp, "PHEASANT HLW PL"))
            strcpy(acTmp, "PHEASANT HOLLOW PL");
         else if (!strcmp(acTmp, "SAN FILIPPO WY"))
            strcpy(acTmp, "SAN FILIPO WY");
         else if (!strcmp(acTmp, "TEMPERENCE RV CT"))
            strcpy(acTmp, "TEMPERENCE RIVER CT");

         // Standardize CR to CIR
         replStrAll(acTmp, " CR ", " CIR ");
         Sac_ParseAdr(&sSitusAdr, acTmp, true);

         if (sSitusAdr.strDir[0] > ' ')
            memcpy(pOutbuf+OFF_S_DIR, sSitusAdr.strDir, strlen(sSitusAdr.strDir));
         if (sSitusAdr.strName[0] > ' ')
            memcpy(pOutbuf+OFF_S_STREET, sSitusAdr.strName, strlen(sSitusAdr.strName));

         if (sSitusAdr.SfxCode[0] > ' ')
            memcpy(pOutbuf+OFF_S_SUFF, sSitusAdr.SfxCode, strlen(sSitusAdr.SfxCode));

         sprintf(acAddr1, "%d %s %s %s %s", iStrNo, sSitusAdr.strSub, sSitusAdr.strDir, sSitusAdr.strName, sSitusAdr.strSfx);
         blankRem(acAddr1);

         // Copy unit#
         if (sSitusAdr.Unit[0] > ' ')
         {
            vmemcpy(pOutbuf+OFF_S_UNITNOX, sSitusAdr.UnitNox, SIZ_S_UNITNOX);
            if (sSitusAdr.Unit[0] == '#')
               iTmp = sprintf(acTmp, " %s", sSitusAdr.Unit);
            else
               iTmp = sprintf(acTmp, " #%s", sSitusAdr.Unit);
            memcpy(pOutbuf+OFF_S_UNITNO, &acTmp[1], iTmp-1);
            strcat(acAddr1, acTmp);
         }
         memcpy(pOutbuf+OFF_S_ADDR_D, acAddr1, strlen(acAddr1));
      }
   } else if (!bNoMail && iStrNo == sMailAdr.lStrNum && 
      !memcmp(pSitus->S_StreetName, sMailAdr.strName, iStrNameCmpLen) &&
      sMailAdr.Unit[0] <= ' ' &&
      !memcmp(pMailing->M_St, "CA", 2))
   {
      lUseMailCity++;
      memcpy(pOutbuf+OFF_S_STRNUM, pOutbuf+OFF_M_STRNUM, SIZ_M_ADDR1);
      memset(pOutbuf+OFF_S_SUFF, ' ', SIZ_S_SUFF);
      memcpy(pOutbuf+OFF_S_SUFF, sMailAdr.SfxCode, strlen(sMailAdr.SfxCode));
      memcpy(pOutbuf+OFF_S_HSENO, sMailAdr.HseNo, strlen(sMailAdr.HseNo));
      memcpy(pOutbuf+OFF_S_ADDR_D, acAddr1, strlen(acAddr1));

      City2Code(sMailAdr.City, sSitusAdr.City);
      if (sSitusAdr.City[0] > '0')
      {
         memcpy(pOutbuf+OFF_S_CITY, sSitusAdr.City, strlen(sSitusAdr.City));
         memcpy(pOutbuf+OFF_S_ZIP, pOutbuf+OFF_M_ZIP, SIZ_M_ZIP);
      }
      memcpy(pOutbuf+OFF_S_CTY_ST_D, acAddr2, strlen(acAddr2));
   } else 
   {
      memcpy(acAddr1, pSitus->S_StreetName, RSIZ_S_STRNAME);
      iTmp = blankRem(acAddr1, RSIZ_S_STRNAME);
      memcpy(pOutbuf+OFF_S_STREET, acAddr1, iTmp);
      memcpy(pOutbuf+OFF_S_ADDR_D, acAddr1, iTmp);
      iNoStrNum++;
   }
   
   if (*(pOutbuf+OFF_S_CITY) == ' ')
   {      
      if (pSitus->S_Zip[0] > ' ')
      {
         ZIP_CITY *psZip;

         // Get city name from known city-zip list
         psZip = getCity(pSitus->S_Zip); 
         if (psZip)
         {
            City2Code(psZip->acCity, acCode, pOutbuf);
            memcpy(pOutbuf+OFF_S_CITY, acCode, 3);
            memcpy(pOutbuf+OFF_S_ZIP, pSitus->S_Zip, SIZ_S_ZIP);
            iTmp = sprintf(acAddr2, "%s CA %d", psZip->acCity, psZip->iZipCode);
         } else if (pSitus->S_Zip[0] == '9')
         {
            memcpy(pOutbuf+OFF_S_ZIP, pSitus->S_Zip, SIZ_S_ZIP);
            iTmp = sprintf(acAddr2, " CA %.5s", pSitus->S_Zip);
            LogMsg("Unknown zipcode: %.5s APN=%.10s", pSitus->S_Zip, pOutbuf);
         } else if (pSitus->S_Zip[0] > '0')
         {
            LogMsg("Bad CA zipcode: %.5s APN=%.10s", pSitus->S_Zip, pOutbuf);
            iTmp = 0;
            acAddr2[0] = 0;
         }
         vmemcpy(pOutbuf+OFF_S_CTY_ST_D, acAddr2, SIZ_S_CTY_ST_D, iTmp);
      }       
   }

   // If still not found city yet, try GIS
   if (*(pOutbuf+OFF_S_CITY) == ' ')
   {
      if (fdCity && !getCityZip(pOutbuf, acCity, acZip, iApnLen))
      {
         // Get City & Zip from GIS
         City2Code(acCity, acCode, pOutbuf);
         memcpy(pOutbuf+OFF_S_CITY, acCode, 3);
         memcpy(pOutbuf+OFF_S_ZIP, acZip, strlen(acZip));
         iTmp = sprintf(acAddr2, "%s CA %s", acCity, acZip);
         memcpy(pOutbuf+OFF_S_CTY_ST_D, acAddr2, iTmp);  
         lUseGis++;
      }
   }

   // state
   memcpy(pOutbuf+OFF_S_ST, "CA", 2);

   // Populate mailing address with situs.
   if (bNoMail)
   {
      // Do this at the MergeAdr step instead.
      memcpy(pOutbuf+OFF_M_STRNUM, pOutbuf+OFF_S_STRNUM, SIZ_S_STRNUM);
      memcpy(pOutbuf+OFF_M_STR_SUB, pOutbuf+OFF_S_STR_SUB, SIZ_M_STR_SUB);
      memcpy(pOutbuf+OFF_M_DIR, pOutbuf+OFF_S_DIR, SIZ_M_DIR);
      memcpy(pOutbuf+OFF_M_STREET, pOutbuf+OFF_S_STREET, SIZ_M_STREET);
      memcpy(pOutbuf+OFF_M_UNITNO, pOutbuf+OFF_S_UNITNO, SIZ_S_UNITNO);
      memcpy(pOutbuf+OFF_M_UNITNOX, pOutbuf+OFF_S_UNITNOX, SIZ_M_UNITNOX);

      // Translate suffix
      iTmp = atoin(pOutbuf+OFF_S_SUFF, 3);
      if (iTmp > 0)
      {
         iTmp = sprintf(acTmp, "%s", GetSfxStr(iTmp));
         memcpy(pOutbuf+OFF_M_SUFF, acTmp, iTmp);
      }

      // Translate city
      iTmp = atoin(pOutbuf+OFF_S_CITY, 3);
      if (iTmp > 0)
      {
         iTmp = sprintf(acTmp, "%s", GetCityName(iTmp));
         memcpy(pOutbuf+OFF_M_CITY, acTmp, iTmp);
      }
      memcpy(pOutbuf+OFF_M_ST, pOutbuf+OFF_S_ST, SIZ_M_ST);
      memcpy(pOutbuf+OFF_M_ZIP, pOutbuf+OFF_S_ZIP, SIZ_M_ZIP);
      memcpy(pOutbuf+OFF_M_ADDR_D, pOutbuf+OFF_S_ADDR_D, SIZ_M_ADDR_D);
      memcpy(pOutbuf+OFF_M_CTY_ST_D, pOutbuf+OFF_S_CTY_ST_D, SIZ_M_CTY_ST_D);
   }
}

void Sac_MergeMAdr(char *pOutbuf, char *pAddr1, char *pCity, char *pState, char *pZip)
{
   char     acTmp[256], acAddr1[256], acAddr2[256];
   int      iTmp;
   bool     bNoMail = false;

   ADR_REC  sMailAdr;

   // Clear old Mailing
   removeMailing(pOutbuf, false);

#ifdef _DEBUG
   // (!memcmp(pOutbuf, "078041001400", 10) )
   //   iTmp = 0;
#endif
   // Check for blank address
   if (memcmp(pAddr1, "     ", 5))
   {
      strcpy(acAddr1, pAddr1);

      // Parsing mail address
      memset((void *)&sMailAdr, 0, sizeof(ADR_REC));
      Sac_ParseAdr(&sMailAdr, acAddr1, false);

      if (sMailAdr.lStrNum > 0)
      {
         sprintf(acTmp, "%d       ", sMailAdr.lStrNum);
         memcpy(pOutbuf+OFF_M_STRNUM, acTmp, SIZ_M_STRNUM);
         if (sMailAdr.strSub[0] > '0')
         {
            sprintf(acTmp, "%s  ", sMailAdr.strSub);
            memcpy(pOutbuf+OFF_M_STR_SUB, acTmp, SIZ_M_STR_SUB);
         }
      }
      memcpy(pOutbuf+OFF_M_DIR, sMailAdr.strDir, strlen(sMailAdr.strDir));
      memcpy(pOutbuf+OFF_M_STREET, sMailAdr.strName, strlen(sMailAdr.strName));
      memcpy(pOutbuf+OFF_M_SUFF, sMailAdr.strSfx, strlen(sMailAdr.strSfx));

      if (sMailAdr.lStrNum > 0)
      {
         sprintf(acAddr1, "%d %s %s %s %s", sMailAdr.lStrNum, sMailAdr.strSub, sMailAdr.strDir,
            sMailAdr.strName, sMailAdr.strSfx);
         blankRem(acAddr1);
      }

      // Copy unit#
      if (sMailAdr.Unit[0] > ' ')
      {
         vmemcpy(pOutbuf+OFF_M_UNITNO, sMailAdr.UnitNox, SIZ_M_UNITNOX);
         if (sMailAdr.Unit[0] == '#')
            iTmp = sprintf(acTmp, " %s", sMailAdr.Unit);
         else
            iTmp = sprintf(acTmp, " #%s", sMailAdr.Unit);
         vmemcpy(pOutbuf+OFF_M_UNITNO, &acTmp[1], SIZ_M_UNITNO, iTmp-1);
         strcat(acAddr1, acTmp);
      }

      memcpy(pOutbuf+OFF_M_ADDR_D, acAddr1, strlen(acAddr1));
      vmemcpy(pOutbuf+OFF_M_CITY, pCity, SIZ_M_CITY);
      vmemcpy(pOutbuf+OFF_M_ST, pState, SIZ_M_ST);

      iTmp = atol(pZip);
      if (iTmp > 500)
         vmemcpy(pOutbuf+OFF_M_ZIP, pZip, SIZ_M_ZIP);

      iTmp = sprintf(acAddr2, "%s %s %s", pCity, pState, pZip);
      vmemcpy(pOutbuf+OFF_M_CTY_ST_D, acAddr2, SIZ_M_CTY_ST_D, iTmp);
   } 
}

/****************************** Sac_MergeAdrUns ******************************
 *
 *
 *
 *****************************************************************************/

void Sac_MergeAdrUns(char *pOutbuf, char *pRollRec)
{
   SAC_UNS *pRec;
   char     acTmp[256], acAddr1[256], acAddr2[256];
   int      iTmp, iStrNo;
   long     lTmp;
   bool     bNoMail = false;

   ADR_REC  sMailAdr;
   ADR_REC  sSitusAdr;

   pRec = (SAC_UNS *)pRollRec;

   // Clear old Mailing
   removeMailing(pOutbuf, false);
   memset((void *)&sMailAdr, 0, sizeof(ADR_REC));

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "0010020008", 10) )
   //   iTmp = 0;
#endif
   // Check for blank address
   if (memcmp(pRec->M_StreetNum, "     ", 5))
   {
      if (!memcmp(pRec->M_StreetNum, "  ONE", 5))
         sMailAdr.lStrNum = 1;
      else
         sMailAdr.lStrNum = atoin(pRec->M_StreetNum, USIZ_M_STRNUM);

      if (sMailAdr.lStrNum > 0)
      {
         sprintf(acTmp, "%d       ", sMailAdr.lStrNum);
         memcpy(pOutbuf+OFF_M_STRNUM, acTmp, SIZ_M_STRNUM);
      }
   }

   if (pRec->M_StreetSub[0] == 'N' || pRec->M_StreetSub[0] == 'S' ||
       pRec->M_StreetSub[0] == 'E' || pRec->M_StreetSub[0] == 'W')
   {
      if ((pRec->M_StreetName[0] != pRec->M_StreetSub[0]) || (pRec->M_StreetName[0] != ' ')  )
      {
         *(pOutbuf+OFF_M_DIR) = pRec->M_StreetSub[0];
         sMailAdr.strDir[0] = pRec->M_StreetSub[0];
      }
   }

   // strName
   memcpy(pOutbuf+OFF_M_STREET, pRec->M_StreetName, USIZ_S_STRNAME);

   if (sMailAdr.lStrNum > 0)
   {
      if (sMailAdr.strDir[0] > ' ')
         sprintf(acAddr1, "%d %s %s", sMailAdr.lStrNum, sMailAdr.strDir, sMailAdr.strName);
      else
         sprintf(acAddr1, "%d %s %s", sMailAdr.lStrNum, sMailAdr.strDir, sMailAdr.strName);
   } else if (pRec->M_StreetName[0] > ' ')
      sprintf(acAddr1, "%.*s", USIZ_M_STRNAME, pRec->M_StreetName);
   else
      acAddr1[0] = 0;

   memcpy(pOutbuf+OFF_M_ADDR_D, acAddr1, strlen(acAddr1));

   memcpy(sMailAdr.City, pRec->M_City, USIZ_M_CITY);
   blankRem(sMailAdr.City, USIZ_M_CITY);
   memcpy(pOutbuf+OFF_M_CITY, pRec->M_City, USIZ_M_CITY);
   memcpy(pOutbuf+OFF_M_ST, pRec->M_St, SIZ_M_ST);

   iTmp = atoin(pRec->M_Zip, USIZ_M_ZIP);
   if (iTmp > 500)
      memcpy(pOutbuf+OFF_M_ZIP, pRec->M_Zip, USIZ_M_ZIP);
   else
      memcpy(pOutbuf+OFF_M_ZIP, BLANK32, SIZ_M_ZIP);

   sMailAdr.City[SIZ_M_CITY] = 0;
   iTmp = sprintf(acAddr2, "%s %.2s %.5s", sMailAdr.City, pRec->M_St, pRec->M_Zip);
   memcpy(pOutbuf+OFF_M_CTY_ST_D, acAddr2, iTmp);

   // Situs
   acAddr1[0] = 0;
   acAddr2[0] = 0;
   memset((void *)&sSitusAdr, 0, sizeof(ADR_REC));
   removeSitus(pOutbuf);

   iStrNo = atoin(pRec->S_StreetNum, USIZ_M_STRNUM);
   if (iStrNo > 0)
   {
      sprintf(acTmp, "%d       ", iStrNo);
      memcpy(pOutbuf+OFF_S_STRNUM, acTmp, SIZ_S_STRNUM);
   }

   // strName
   if (!memcmp(pRec->S_StreetNum, " BLDG", 5))
   {
      memcpy(pOutbuf+OFF_S_STREET, "BLDG ", 5);
      memcpy(pOutbuf+OFF_S_STREET+5, pRec->S_StreetName, USIZ_S_STRNAME);
   } else if (pRec->S_StreetName[0] > ' ')
   {
      // Copy street name
      memcpy(acTmp, pRec->S_StreetName, USIZ_S_STRNAME);
      blankRem(acTmp, USIZ_S_STRNAME);

      parseAdr1N(&sSitusAdr, acTmp);

      if (sSitusAdr.strDir[0] > ' ')
         memcpy(pOutbuf+OFF_S_DIR, sSitusAdr.strDir, strlen(sSitusAdr.strDir));
      if (sSitusAdr.strName[0] > ' ')
         memcpy(pOutbuf+OFF_S_STREET, sSitusAdr.strName, strlen(sSitusAdr.strName));
      if (sSitusAdr.SfxCode[0] > ' ')
         memcpy(pOutbuf+OFF_S_SUFF, sSitusAdr.SfxCode, strlen(sSitusAdr.SfxCode));

      if (iStrNo > 0)
      {
         sprintf(acAddr1, "%d %s", iStrNo, acTmp);
         blankRem(acAddr1);
      } else
         strcpy(acAddr1, acTmp);

      if (sSitusAdr.Unit[0] > ' ')
      {
         if (sSitusAdr.Unit[0] == '#')
            iTmp = sprintf(acTmp, " %s", sSitusAdr.Unit);
         else
            iTmp = sprintf(acTmp, " #%s", sSitusAdr.Unit);
         memcpy(pOutbuf+OFF_S_UNITNO, &acTmp[1], iTmp-1);
         strcat(acAddr1, acTmp);
      }
      memcpy(pOutbuf+OFF_S_ADDR_D, acAddr1, strlen(acAddr1));
   }

   // state
   memcpy(pOutbuf+OFF_S_ST, "CA", 2);

   lTmp = atoin(pRec->S_Zip, SIZ_S_ZIP);
   if (lTmp > 89999)
   {
      memcpy(pOutbuf+OFF_S_ZIP, pRec->S_Zip, SIZ_S_ZIP);
      iTmp = sprintf(acAddr2, " CA %.5s", sSitusAdr.Zip);
      memcpy(pOutbuf+OFF_S_CTY_ST_D, acAddr2, iTmp);
   }
}

/********************************* Sac_MergeChar *****************************
 *
 * This function has been replaced by Sac_MergeCChar()
 *
 *****************************************************************************/

int Sac_MergeChar(char *pOutbuf)
{
   static   char acRec[1024], *pRec=NULL;
   char     acTmp[256], acCode[32], cTmp1, cTmp2;
   long     lTmp, lBldgSqft, lGarSqft;
   int      iRet, iTmp, iLoop, iBeds, iFBath, iHBath, iFp;
   SAC_CHAR *pChar;

   iRet=iBeds=iFBath=iHBath=iFp=0;
   lBldgSqft=lGarSqft=0;

   // Get first Char rec for first call
   if (!pRec && !lCharMatch)
   {
      pRec = fgets(acRec, 1024, fdChar);
   }

   do
   {
      if (!pRec)
      {
         fclose(fdChar);
         fdChar = NULL;
         return 1;      // EOF
      }

      // Compare Apn
      iLoop = memcmp(pOutbuf, pRec, myCounty.iApnLen);
      if (iLoop > 0)
      {
         if (bDebug)
            LogMsg0("Skip Char rec  %.*s", myCounty.iApnLen, pRec);
         pRec = fgets(acRec, 1024, fdChar);
         lCharSkip++;
      }
   } while (iLoop > 0);

   // If not match, return
   if (iLoop)
      return 0;

   pChar = (SAC_CHAR *)pRec;

   // Quality Class
   if (isalpha(pChar->QualityClass[0]))
   {  // D045A
      *(pOutbuf+OFF_BLDG_CLASS) = pChar->QualityClass[0];
      iTmp = atoin((char *)&pChar->QualityClass[1], 2);
      sprintf(acTmp, "%d.%c", iTmp, pChar->QualityClass[3]);
      iRet = Value2Code(acTmp, acCode, NULL);
      blankPad(acCode, SIZ_BLDG_QUAL);
      memcpy(pOutbuf+OFF_BLDG_QUAL, acCode, SIZ_BLDG_QUAL);
   }

   // Condition
   if (pChar->Imprv_Condition_Code > '0')
   {
      switch (pChar->Imprv_Condition_Code)
      {
         case '1':
            cTmp1 = 'P';    // Poor
            break;
         case '2':
            cTmp1 = 'F';    // Fair
            break;
         case '3':
            cTmp1 = 'A';    // Average
            break;
         case '4':
            cTmp1 = 'G';    // Good
            break;
         default:
            cTmp1 = ' ';
      }
      *(pOutbuf+OFF_IMPR_COND) = cTmp1;
   }

   // YrBlt
   lTmp = atoin((char *)&pChar->YearBuilt[1], 4);
   if (lTmp > 1700)
      memcpy(pOutbuf+OFF_YR_BLT, (char *)&pChar->YearBuilt[1], SIZ_YR_BLT);

   // YrEff
   lTmp = atoin((char *)&pChar->EffYear[1], 4);
   if (lTmp > 1700)
      memcpy(pOutbuf+OFF_YR_EFF, (char *)&pChar->EffYear[1], SIZ_YR_BLT);

   // Stories
   iTmp = atoin(pChar->Num_Stories, ASIZ_NUM_STORIES);
   if (iTmp > 0)
   {
      if (iTmp > 9 && bDebug)
         LogMsg("%.*s : %d", iApnLen, pOutbuf, iTmp);
      sprintf(acTmp, "%d.0  ", iTmp);
      memcpy(pOutbuf+OFF_STORIES, acTmp, SIZ_STORIES);
   }

   // BldgSqft
   lBldgSqft = atoin(pChar->Total_Res_Area, ASIZ_AREA_SQFT);
   if (lBldgSqft > 10)
   {
      sprintf(acTmp, "%*u", SIZ_BLDG_SF, lBldgSqft);
      memcpy(pOutbuf+OFF_BLDG_SF, acTmp, SIZ_BLDG_SF);
   }

   // FloorSqft
   lTmp = atoin(pChar->Orig_1St_Flr_Area, ASIZ_AREA_SQFT);
   if (lTmp > 1)
   {
      iTmp = sprintf(acTmp, "%d", lTmp);
      memcpy(pOutbuf+OFF_FLOOR1_SF, acTmp, iTmp);
   }
   lTmp = atoin(pChar->Orig_2Nd_Flr_Area, ASIZ_AREA_SQFT);
   if (lTmp > 1)
   {
      iTmp = sprintf(acTmp, "%d", lTmp);
      memcpy(pOutbuf+OFF_FLOOR2_SF, acTmp, iTmp);
   }

   // Garage Sqft
   lGarSqft = atoin(pChar->SqFTGarage, ASIZ_AREA_SQFT);
   if (lGarSqft > 10)
   {
      sprintf(acTmp, "%*u", SIZ_GAR_SQFT, lGarSqft);
      memcpy(pOutbuf+OFF_GAR_SQFT, acTmp, SIZ_GAR_SQFT);
      // This is to cover where Garage_Spcs is 0
      *(pOutbuf+OFF_PARK_TYPE) = 'Z';
   }

   // Garage spaces - Normally when there is park space and no garage Sqft
   // we translate it to "COVERED" or "GARAGE/CARPORT".  In SAC, I make it
   // "GARAGE" to be compatible with Fernando.
   if (pChar->Garage_Spcs > '0')
   {
      *(pOutbuf+OFF_PARK_TYPE) = 'Z';                 // Garage
      *(pOutbuf+OFF_PARK_SPACE) = pChar->Garage_Spcs;
   }

   // Central Heating-Cooling
   cTmp1=cTmp2=' ';
   if (pChar->Central_Ac_Heating > '0')
   {  // cTmp1 = Heat, cTmp2 = AC
      switch (pChar->Central_Ac_Heating)
      {
         //case '1':         // None
         //   cTmp1 = 'L';
         //   cTmp2 = 'N';
         //   break;
         case '2':
            cTmp1 = 'Z';   // Heat
            //cTmp2 = 'N';
            break;
         case '3':
            cTmp1 = 'Z';   // Heat and AC
            cTmp2 = 'C';
            break;
         default:
            break;
      }
      *(pOutbuf+OFF_HEAT) = cTmp1;
      *(pOutbuf+OFF_AIR_COND) = cTmp2;
   }

   if (cTmp1 == ' ')
   {
      if (pChar->Solar_Space_Heater == '1')
         *(pOutbuf+OFF_HEAT) = 'K';
   }

   // Rooms
   iTmp = atoin(pChar->Total_Rooms, ASIZ_TOTAL_ROOMS);
   if (iTmp > 0)
   {
      sprintf(acTmp, "%*u", SIZ_ROOMS, iTmp);
      memcpy(pOutbuf+OFF_ROOMS, acTmp, SIZ_ROOMS);
   }

   // Beds
   iBeds = atoin(pChar->NumBedrooms, ASIZ_NUM_BEDROOMS);
   if (iBeds > 0)
   {
      sprintf(acTmp, "%*u", SIZ_BEDS, iBeds);
      memcpy(pOutbuf+OFF_BEDS, acTmp, SIZ_BEDS);
   }

   // Bath
   if (pChar->NumBaths[0] > '0')
   {
      iFBath = pChar->NumBaths[0] & 0x0F;
      sprintf(acTmp, "%*u", SIZ_BATH_F, iFBath);
      memcpy(pOutbuf+OFF_BATH_F, acTmp, SIZ_BATH_F);

      // Half bath
      if (pChar->NumBaths[1] > '0')
      {
         memcpy(pOutbuf+OFF_BATH_H, " 1", 2); 
         // Fraction bath not avail.
         //if (pChar->NumBaths[1] != '5')
         //   iTmp = 0;
      }
   }

   // Fireplace
   iTmp = atoin(pChar->Fireplace_Code, ASIZ_CODE);
   if (iTmp > 0)
      memcpy(pOutbuf+OFF_FIRE_PL, " 1", 2);

   // Roof
   if (pChar->Roof > '0')
   {
      switch (pChar->Roof)
      {
         case '1':
            cTmp1 = 'C';   // COMPOSITION SHINGLE
            break;
         case '2':
            cTmp1 = 'A';   // WOOD SHINGLE
            break;
         case '3':
            cTmp1 = 'B';   // WOOD SHAKE
            break;
         default:
            LogMsg("*** Unknown Roof material: %c - %.14s", pChar->Roof, pOutbuf);
            cTmp1 = ' ';
      }
      *(pOutbuf+OFF_ROOF_MAT) = cTmp1;
   }

   // Floor - Other values are not known 0 and 1
   /* not use
   if (pChar->Floor_Type == '2')
      *(pOutbuf+OFF_FNDN_TYPE) = 'S';
   */

   // Pool/Spa
   iTmp = atoin(pChar->Swimming_Pool_Code, ASIZ_CODE);
   if (iTmp > 0)
   {
      if (pChar->Spa == '2')
         *(pOutbuf+OFF_POOL) = 'C';    // Pool/Spa
      else
         *(pOutbuf+OFF_POOL) = 'P';    // Pool
   } else if (pChar->Spa == '2')
      *(pOutbuf+OFF_POOL) = 'S';       // Spa

   // Lot sqft - Lot Acres
   lTmp = atoin(pChar->Lot_Sqft, ASIZ_LOT_SQFT);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*u", SIZ_LOT_SQFT, lTmp);
      memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);
      lTmp = (lTmp*ACRES_FACTOR)/SQFT_PER_ACRE;
      sprintf(acTmp, "%*u", SIZ_LOT_SQFT, lTmp);
      memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_SQFT);
   } else
   {
      lTmp = atoin(pChar->Lot_Acres, ASIZ_ACRES);
      if (lTmp > 0)
      {
         lBldgSqft = (lTmp*SQFT_PER_ACRE)/100;
         sprintf(acTmp, "%*u", SIZ_LOT_SQFT, lBldgSqft);
         memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);

         lTmp *= 10;
         sprintf(acTmp, "%*u", SIZ_LOT_SQFT, lTmp);
         memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_SQFT);
      }
   }

   // Neighborhood code
   memcpy(pOutbuf+OFF_NBH_CODE, pChar->Nbh_Code, ASIZ_NBH_CODE);

   // Zoning

   // Usecode

   // Basement Sqft - not use
   // View - not avail.
   // Units - not avail.
   // Buildings - not avail

   // Others

   lCharMatch++;

   // Get next Char rec
   pRec = fgets(acRec, 1024, fdChar);

   return 0;
}

/********************************* Sac_MergeCChar ****************************
 *
 * Merge standard chars into R01
 *
 *****************************************************************************/

int Sac_MergeCChar(char *pOutbuf)
{
   static   char acRec[2048], *pRec=NULL;
   char     acTmp[256];
   long     lTmp, lGarSqft;
   int      iTmp, iLoop, iBeds, iFBath;

   STDCHAR  *pChar;

   iBeds=iFBath=lGarSqft=0;

   // Get first Char rec for first call
   if (!pRec)
   {
      pRec = fgets(acRec, 2048, fdCChr);
   }

   do
   {
      if (!pRec)
      {
         fclose(fdCChr);
         fdCChr = NULL;
         return 1;      // EOF
      }

      // Compare Apn
      iLoop = memcmp(pOutbuf, pRec, myCounty.iApnLen);
      if (iLoop > 0)
      {
         if (bDebug)
            LogMsg0("Skip Char rec  %.*s", myCounty.iApnLen, pRec);
         pRec = fgets(acRec, 2048, fdCChr);
         lCharSkip++;
      }
   } while (iLoop > 0);

   // If not match, return
   if (iLoop)
      return 0;

   pChar = (STDCHAR *)pRec;

   // Quality Class
   if (pChar->BldgClass > ' ')
   {  
      *(pOutbuf+OFF_BLDG_CLASS) = pChar->BldgClass;
      *(pOutbuf+OFF_BLDG_QUAL)  = pChar->BldgQual;
      memcpy(pOutbuf+OFF_QUALITYCLASS, pChar->QualityClass, SIZ_CHAR_QCLS);
   }

   // Condition
   if (pChar->ImprCond[0] > '0')
      *(pOutbuf+OFF_IMPR_COND) = pChar->ImprCond[0];

   // YrBlt
   if (pChar->YrBlt[0] > '0')
      memcpy(pOutbuf+OFF_YR_BLT, (char *)&pChar->YrBlt, SIZ_YR_BLT);

   // YrEff
   if (pChar->YrEff[0] > '0')
      memcpy(pOutbuf+OFF_YR_EFF, (char *)&pChar->YrEff, SIZ_YR_BLT);

   // Stories
   if (pChar->Stories[0] > '0')
   {
      iTmp = sprintf(acTmp, "%.1f", atofn(pChar->Stories, SIZ_STORIES));
      memcpy(pOutbuf+OFF_STORIES, acTmp, iTmp);
   }

   // BldgSqft
   lTmp = atoin(pChar->BldgSqft, SIZ_CHAR_SQFT);
   if (lTmp > 10)
   {
      sprintf(acTmp, "%*u", SIZ_BLDG_SF, lTmp);
      memcpy(pOutbuf+OFF_BLDG_SF, acTmp, SIZ_BLDG_SF);
   }

   // FloorSqft
   lTmp = atoin(pChar->Sqft_1stFl, SIZ_CHAR_SQFT);
   if (lTmp > 1)
   {
      iTmp = sprintf(acTmp, "%d", lTmp);
      memcpy(pOutbuf+OFF_FLOOR1_SF, acTmp, iTmp);
   }
   lTmp = atoin(pChar->Sqft_2ndFl, SIZ_CHAR_SQFT);
   if (lTmp > 1)
   {
      iTmp = sprintf(acTmp, "%d", lTmp);
      memcpy(pOutbuf+OFF_FLOOR2_SF, acTmp, iTmp);
   }

   // Additional area - MiscSqft
   lTmp = atoin(pChar->MiscSqft, SIZ_CHAR_SQFT);
   if (lTmp > 1)
   {
      iTmp = sprintf(acTmp, "%d", lTmp);
      memcpy(pOutbuf+OFF_MISCIMPR_SF, acTmp, iTmp);
   }

   // Garage Sqft
   lGarSqft = atoin(pChar->GarSqft, SIZ_CHAR_SQFT);
   if (lGarSqft > 10)
   {
      sprintf(acTmp, "%*u", SIZ_GAR_SQFT, lGarSqft);
      memcpy(pOutbuf+OFF_GAR_SQFT, acTmp, SIZ_GAR_SQFT);
      // This is to cover where Garage_Spcs is 0
      *(pOutbuf+OFF_PARK_TYPE) = 'Z';
   }

   // Garage spaces - Normally when there is park space and no garage Sqft
   // we translate it to "COVERED" or "GARAGE/CARPORT".  In SAC, I make it
   // "GARAGE" to be compatible with Fernando.
   if (pChar->ParkSpace[0] > '0')
   {
      *(pOutbuf+OFF_PARK_TYPE) = 'Z';                 // Garage
      *(pOutbuf+OFF_PARK_SPACE) = pChar->ParkSpace[0];
   }

   // Central Heating-Cooling
   if (pChar->Heating[0] > ' ')
      *(pOutbuf+OFF_HEAT) = pChar->Heating[0];
   if (pChar->Cooling[0] > ' ')
      *(pOutbuf+OFF_AIR_COND) = pChar->Cooling[0];

   // Rooms
   iTmp = atoin(pChar->Rooms, SIZ_CHAR_ROOMS);
   if (iTmp > 0)
   {
      sprintf(acTmp, "%*u", SIZ_ROOMS, iTmp);
      memcpy(pOutbuf+OFF_ROOMS, acTmp, SIZ_ROOMS);
   }

   // Beds
   iBeds = atoin(pChar->Beds, SIZ_CHAR_BEDS);
   if (iBeds > 0)
   {
      sprintf(acTmp, "%*u", SIZ_BEDS, iBeds);
      memcpy(pOutbuf+OFF_BEDS, acTmp, SIZ_BEDS);
   }

   // Bath
   if (pChar->FBaths[0] > '0')
   {
      iFBath = atoin(pChar->FBaths, SIZ_CHAR_BATHS);
      sprintf(acTmp, "%*u", SIZ_BATH_F, iFBath);
      memcpy(pOutbuf+OFF_BATH_F, acTmp, SIZ_BATH_F);
      memcpy(pOutbuf+OFF_BATH_4Q, acTmp, SIZ_BATH_F);
   }
   if (pChar->Bath_2Q[0] > '0')
   {
      memcpy(pOutbuf+OFF_BATH_H, " 1", 2);
      memcpy(pOutbuf+OFF_BATH_2Q, " 1", 2);
   } else if (pChar->Bath_1Q[0] > '0')
   {
      memcpy(pOutbuf+OFF_BATH_1Q, " 1", 2);
   } else if (pChar->HBaths[0] > '0')
   {
      memcpy(pOutbuf+OFF_BATH_H, " 1", 2);
   }

   // Fireplace
   *(pOutbuf+OFF_FIRE_PL) = pChar->Fireplace[0];

   // Roof
   if (pChar->RoofMat[0] > ' ')
      *(pOutbuf+OFF_ROOF_MAT) = pChar->RoofMat[0];

   // Pool/Spa
   *(pOutbuf+OFF_POOL) = pChar->Pool[0];

   // Lot sqft - Lot Acres
   if (pChar->LotSqft[0] > '0')
   {
      memcpy(pOutbuf+OFF_LOT_SQFT, pChar->LotSqft, SIZ_LOT_SQFT);
      memcpy(pOutbuf+OFF_LOT_ACRES, pChar->LotAcre, SIZ_LOT_SQFT);
   }

   // Neighborhood code
   if (*(pOutbuf+OFF_NBH_CODE) == ' ' && pChar->Nbh_Code[0] > ' ')
      memcpy(pOutbuf+OFF_NBH_CODE, pChar->Nbh_Code, SIZ_NBH_CODE);

   // Zoning
   if (*(pOutbuf+OFF_ZONE) == ' ' && pChar->Zoning[0] > ' ')
      memcpy(pOutbuf+OFF_ZONE, pChar->Zoning, SIZ_ZONE);

   // Basement Sqft
   lTmp = atoin(pChar->BsmtSqft, SIZ_CHAR_SQFT);
   if (lTmp > 50)
   {
      iTmp = sprintf(acTmp, "%d", lTmp);
      memcpy(pOutbuf+OFF_BSMT_SF, acTmp, iTmp);
   }

   // Units - number of buildings
   lTmp = atoin(pChar->Units, SIZ_CHAR_UNITS);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_UNITS, lTmp);
      memcpy(pOutbuf+OFF_UNITS, acTmp, SIZ_UNITS);
   }

   // View - not avail.
   // Others

   lCharMatch++;

   // Get next Char rec
   pRec = fgets(acRec, 2048, fdCChr);

   return 0;
}

/********************************* Sac_MergeLand *****************************
 *
 *
 *
 *****************************************************************************/

int Sac_MergeLand(char *pOutbuf)
{
   static   char     acRec[64];
   static   int      iCnt=0;
   int      iLoop;
   SAC_LAND *pLand;

   // Get first Char rec for first call
   if (!iCnt)
   {
      iCnt = fread(acRec, 1, iLandSize, fdLand);
   }

   pLand = (SAC_LAND *)&acRec[0];
   do
   {
      if (iCnt != iLandSize)
      {
         fclose(fdLand);
         fdLand = NULL;
         return 1;      // EOF
      }

      // Compare Apn
      iLoop = memcmp(pOutbuf, pLand->Apn, myCounty.iApnLen);
      if (iLoop > 0)
      {
         if (bDebug)
            LogMsg0("Skip Land rec  %.*s", myCounty.iApnLen, pLand->Apn);
         iCnt = fread(acRec, 1, iLandSize, fdLand);
         lLandSkip++;
      }
   } while (iLoop > 0);

   // If not match, return
   if (iLoop)
      return 0;

   memcpy(pOutbuf+OFF_LOT_SQFT, pLand->LotSqft, SIZ_LOT_SQFT);
   memcpy(pOutbuf+OFF_LOT_ACRES, pLand->LotAcres, SIZ_LOT_ACRES);

   lLandMatch++;

   // Get next Land rec
   iCnt = fread(acRec, 1, iLandSize, fdLand);

   return 0;
}

/******************************* Sac_MergeAltApn *****************************
 *
 *
 *
 *****************************************************************************/

int Sac_MergeAltApn(char *pOutbuf)
{
   static   char acRec[1024], *pRec=NULL;
   int      iRet, iLoop;

   iRet=0;

   // Get first Char rec for first call
   if (!pRec)
   {
      // Skip header
      pRec = fgets(acRec, 1024, fdApn);
      // Get first rec
      pRec = fgets(acRec, 1024, fdApn);
   }

#ifdef _DEBUG
   //char *pTmp = pRec;

   //if (!memcmp(pOutbuf, "0020041077", 10))
   //   iRet = 0;
#endif

   do
   {
      if (!pRec)
      {
         fclose(fdApn);
         fdApn = NULL;
         return 1;      // EOF
      }

      // Compare Apn
      iLoop = memcmp(pOutbuf, pRec, iApnLen);
      if (iLoop > 0)
      {
         if (bDebug)
            LogMsg0("Skip APN rec  %.*s", iApnLen, pRec);
         pRec = fgets(acRec, 1024, fdApn);
         lApnSkip++;
      }
   } while (iLoop > 0);

   // If not match, return
   if (iLoop)
      return 1;

   memcpy(pOutbuf+OFF_ALT_APN, pRec+iApnLen+1, iApnLen);
   lApnMatch++;

   // Get next Char rec
   pRec = fgets(acRec, 1024, fdApn);

   return 0;
}

/********************************* Sac_MergeLVal *****************************
 *
 * Merge LDR values to roll record.  We have to use these values since those
 * in the roll files are incomplete.
 *
 *****************************************************************************/

int Sac_MergeLVal(char *pOutbuf)
{
   static   char     acRec[512];                // RecSize is 299
   static   SAC_LVAL *pRec=(SAC_LVAL *)NULL;
   int      iLoop;
   char     acTmp[32];

   // Get first rec for first call
   if (!pRec)
      pRec = (SAC_LVAL *)fgets(acRec, 512, fdValue);

   do
   {
      if (!pRec)
      {
         fclose(fdValue);
         fdValue = NULL;
         return 1;      // EOF
      }

      // Compare Apn
      iLoop = memcmp(pOutbuf, pRec->Apn, myCounty.iApnLen);
      if (iLoop > 0)
      {
         if (bDebug)
            LogMsg0("Skip LDR value rec  %.*s", myCounty.iApnLen, pRec->Apn);
         pRec = (SAC_LVAL *)fgets(acRec, 512, fdValue);
         lLValSkip++;
      }
   } while (iLoop > 0);

   // If not match, return
   if (iLoop)
      return 0;

   // HO Exempt
   long lHO_Exe = atoin(pRec->HO_Exe, LSIZ_HO_EXE);
   if (lHO_Exe > 0)
   {
      *(pOutbuf+OFF_HO_FL) = '1';      // 'Y'
      sprintf(acTmp, "%*u", SIZ_EXE_TOTAL, lHO_Exe);
      memcpy(pOutbuf+OFF_EXE_TOTAL, acTmp, SIZ_EXE_TOTAL);
   } else
      *(pOutbuf+OFF_HO_FL) = '2';      // 'N'

   // Exemp total - this overides Home Owner exempt
   long lTotalExe = atoin(pRec->Total_Exe, LSIZ_HO_EXE);
   if (lTotalExe > 0)
   {
      sprintf(acTmp, "%*u", SIZ_EXE_TOTAL, lTotalExe);
      memcpy(pOutbuf+OFF_EXE_TOTAL, acTmp, SIZ_EXE_TOTAL);
   }

   // Land
   long lLand = atoin(pRec->Land, LSIZ_LAND);
   if (lLand > 0)
   {
      sprintf(acTmp, "%*u", SIZ_LAND, lLand);
      memcpy(pOutbuf+OFF_LAND, acTmp, SIZ_LAND);
   }

   // Improve
   long lImpr = atoin(pRec->Impr, LSIZ_LAND);
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*u", SIZ_IMPR, lImpr);
      memcpy(pOutbuf+OFF_IMPR, acTmp, SIZ_IMPR);
   }

   // Other value
   long lFixture = atoin(pRec->Fixt_Val, LSIZ_LAND);
   long lPers = atoin(pRec->PP_Val, LSIZ_LAND);
   long lOther = lPers + lFixture;
   if (lOther > 0)
   {
      sprintf(acTmp, "%*u", SIZ_OTHER, lOther);
      memcpy(pOutbuf+OFF_OTHER, acTmp, SIZ_OTHER);

      if (lPers > 0)
      {
         sprintf(acTmp, "%d          ", lPers);
         memcpy(pOutbuf+OFF_PERSPROP, acTmp, SIZ_PERSPROP);
      }
      if (lFixture > 0)
      {
         sprintf(acTmp, "%d          ", lFixture);
         memcpy(pOutbuf+OFF_FIXTR, acTmp, SIZ_FIXTR);
      }
   }

   // Prop 8 flag
   if (pRec->ActionCode[0] == 'D')
      *(pOutbuf+OFF_PROP8_FLG) = 'Y';

   // Gross total
   lOther += lLand+lImpr;
   if (lOther > 0)
   {
      sprintf(acTmp, "%*u", SIZ_GROSS, lOther);
      memcpy(pOutbuf+OFF_GROSS, acTmp, SIZ_GROSS);

      // Ratio
      if (lImpr > 0)
      {
         sprintf(acTmp, "%*u", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
         memcpy(pOutbuf+OFF_RATIO, acTmp, SIZ_RATIO);
      }
   }

   lLValMatch++;

   // Get next rec
   pRec = (SAC_LVAL *)fgets(acRec, 512, fdValue);

   return 0;
}

/******************************** Sac_MergeEUVal *****************************
 *
 * Merge equalized values to roll record using city_full_equalized_roll.dos
 *
 *
 *****************************************************************************/

int Sac_MergeEUVal(char *pOutbuf)
{
   static   char     acRec[512], *pRec=NULL;               
   int      iLoop, iRet;
   char     acTmp[256], *apItems[16];

   // Get first rec for first call
   if (!pRec)
      pRec = fgets(acRec, 512, fdEq);

   do
   {
      if (!pRec)
      {
         fclose(fdEq);
         fdEq = NULL;
         return 1;      // EOF
      }

      // Compare Apn
      iLoop = memcmp(pOutbuf, pRec, myCounty.iApnLen);
      if (iLoop > 0)
      {
         if (bDebug)
            LogMsg0("Skip EQR rec  %.*s", myCounty.iApnLen, pRec);
         pRec = fgets(acRec, 512, fdEq);
         lLValSkip++;
      }
   } while (iLoop > 0);

   // If not match, return
   if (iLoop)
      return 0;

   // Parse record
   iRet = ParseStringIQ(acRec, '|', EU_COLS+1, apItems);
   if (iRet < EU_COLS)
   {
      LogMsg("***** Error: bad input record for APN=%s", apItems[0]);
      return -1;
   }

   // HO Exempt
   long lHO_Exe = atol(apItems[EU_HOMEOWNERS_EXEMP_VALUE]);
   if (lHO_Exe > 0)
   {
      *(pOutbuf+OFF_HO_FL) = '1';      // 'Y'
      sprintf(acTmp, "%*u", SIZ_EXE_TOTAL, lHO_Exe);
      memcpy(pOutbuf+OFF_EXE_TOTAL, acTmp, SIZ_EXE_TOTAL);
   } else
      *(pOutbuf+OFF_HO_FL) = '2';      // 'N'

   // Other Exemption 
   long lOtherExe = atol(apItems[EU_OTHER_EXEMP_VALUE]);
   if (lOtherExe > 0)
   {
      sprintf(acTmp, "%*u", SIZ_EXE_TOTAL, lOtherExe);
      memcpy(pOutbuf+OFF_EXE_TOTAL, acTmp, SIZ_EXE_TOTAL);
   }

   // Land
   long lLand = atol(apItems[EU_LAND_VALUE]);
   if (lLand > 0)
   {
      sprintf(acTmp, "%*u", SIZ_LAND, lLand);
      memcpy(pOutbuf+OFF_LAND, acTmp, SIZ_LAND);
   }

   // Improve
   long lImpr = atol(apItems[EU_IMPROVEMENT_VALUE]);
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*u", SIZ_IMPR, lImpr);
      memcpy(pOutbuf+OFF_IMPR, acTmp, SIZ_IMPR);
   }

   // Other value
   long lFixture = atol(apItems[EU_FIXTURE_VALUE]);
   long lPers = atol(apItems[EU_PERSONAL_PROP_VALUE]);
   long lOther = lPers + lFixture;
   if (lOther > 0)
   {
      sprintf(acTmp, "%*u", SIZ_OTHER, lOther);
      memcpy(pOutbuf+OFF_OTHER, acTmp, SIZ_OTHER);

      if (lPers > 0)
      {
         sprintf(acTmp, "%d          ", lPers);
         memcpy(pOutbuf+OFF_PERSPROP, acTmp, SIZ_PERSPROP);
      }
      if (lFixture > 0)
      {
         sprintf(acTmp, "%d          ", lFixture);
         memcpy(pOutbuf+OFF_FIXTR, acTmp, SIZ_FIXTR);
      }
   }

   // Gross total
   lOther += lLand+lImpr;
   if (lOther > 0)
   {
      sprintf(acTmp, "%*u", SIZ_GROSS, lOther);
      memcpy(pOutbuf+OFF_GROSS, acTmp, SIZ_GROSS);

      // Ratio
      if (lImpr > 0)
      {
         sprintf(acTmp, "%*u", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
         memcpy(pOutbuf+OFF_RATIO, acTmp, SIZ_RATIO);
      }
   }

   lLValMatch++;

   // Get next rec
   pRec = fgets(acRec, 512, fdEq);

   return 0;
}

/******************************** Sac_MergeELVal *****************************
 *
 * Merge equalized values to roll record using 2018_equalized_roll.txt
 *
 *****************************************************************************/

int Sac_MergeELVal(char *pOutbuf)
{
   static   char     acRec[512], *pRec=NULL;               
   int      iLoop, iRet;
   char     acTmp[256], *apItems[16];

   // Get first rec for first call
   if (!pRec)
      pRec = fgets(acRec, 512, fdEq);

   do
   {
      if (!pRec)
      {
         fclose(fdEq);
         fdEq = NULL;
         return 1;      // EOF
      }

      strncpy(acTmp, acRec, 17);
      remChar(acTmp, ';', 17);

      // Compare Apn
      iLoop = memcmp(pOutbuf, acTmp, myCounty.iApnLen);
      if (iLoop > 0)
      {
         if (bDebug)
            LogMsg0("Skip EQR rec  %s", acTmp);
         pRec = fgets(acRec, 512, fdEq);
         lLValSkip++;
      }
   } while (iLoop > 0);

   // If not match, return
   if (iLoop)
      return 0;

   // Parse record
   iRet = ParseStringIQ(acRec, ';', EL_COLS+1, apItems);
   if (iRet < EL_COLS)
   {
      LogMsg("***** Error: bad input record for APN=%s", apItems[0]);
      return -1;
   }

   // Reset value
   removeLdrValue(pOutbuf);

   // Land
   long lLand = atol(apItems[EL_LAND]);
   if (lLand > 0)
   {
      sprintf(acTmp, "%*u", SIZ_LAND, lLand);
      memcpy(pOutbuf+OFF_LAND, acTmp, SIZ_LAND);
   }

   // Improve
   long lImpr = atol(apItems[EL_IMPR]);
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*u", SIZ_IMPR, lImpr);
      memcpy(pOutbuf+OFF_IMPR, acTmp, SIZ_IMPR);
   }

   // Other value
   long lFixture = atol(apItems[EL_FIXTURE]);
   long lPers = atol(apItems[EL_PP]);
   long lOther = lPers + lFixture;
   if (lOther > 0)
   {
      sprintf(acTmp, "%*u", SIZ_OTHER, lOther);
      memcpy(pOutbuf+OFF_OTHER, acTmp, SIZ_OTHER);

      if (lPers > 0)
      {
         sprintf(acTmp, "%d          ", lPers);
         memcpy(pOutbuf+OFF_PERSPROP, acTmp, SIZ_PERSPROP);
      }
      if (lFixture > 0)
      {
         sprintf(acTmp, "%d          ", lFixture);
         memcpy(pOutbuf+OFF_FIXTR, acTmp, SIZ_FIXTR);
      }
   }

   // Gross total
   lOther += lLand+lImpr;
   if (lOther > 0)
   {
      sprintf(acTmp, "%*u", SIZ_GROSS, lOther);
      memcpy(pOutbuf+OFF_GROSS, acTmp, SIZ_GROSS);

      // Ratio
      if (lImpr > 0)
      {
         sprintf(acTmp, "%*u", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
         memcpy(pOutbuf+OFF_RATIO, acTmp, SIZ_RATIO);
      }
   }

   // HO Exempt
   long lHO_Exe = atol(apItems[EL_HO_EXE]);
   if (lHO_Exe > 0)
   {
      *(pOutbuf+OFF_HO_FL) = '1';      // 'Y'
      sprintf(acTmp, "%*u", SIZ_EXE_TOTAL, lHO_Exe);
      memcpy(pOutbuf+OFF_EXE_TOTAL, acTmp, SIZ_EXE_TOTAL);
   } else
      *(pOutbuf+OFF_HO_FL) = '2';      // 'N'

   // Other Exemption 
   long lTotalExe = atol(apItems[EL_TOTAL_EXE]);
   if (lTotalExe > lOther)
      lTotalExe = lOther;              // Exemption cannot be greater than total value

   if (lTotalExe > 0 && lTotalExe != lHO_Exe)
   {
      sprintf(acTmp, "%*u", SIZ_EXE_TOTAL, lTotalExe);
      memcpy(pOutbuf+OFF_EXE_TOTAL, acTmp, SIZ_EXE_TOTAL);
   }

   lLValMatch++;

   // Get next rec
   pRec = fgets(acRec, 512, fdEq);

   return 0;
}

/********************************* Sac_MergeSale *****************************
 *
 *
 *
 *****************************************************************************/

int Sac_MergeSale1(char *pOutbuf, char *pSaleRec, int iCnt)
{
   SAC_CSAL *pRec;
   long     lDttAmt, lRecDate, lDocNum, lTmp;
   int      iTmp;
   char     *pDate, *pAmt, *pDoc, *pDocType, *pSaleDate;
   char     *pNxtDate, *pNxtAmt, *pNxtDoc, *pNxtDocType;
   char     acDocType[32], acDocNum[32];

   pRec = (SAC_CSAL *)pSaleRec;
   pSaleDate = pRec->RecDate;

   // Check for date
   lRecDate = atoin(pSaleDate, SIZ_SALE1_DT);
   if (!lRecDate)
      return 1;

   switch (iCnt)
   {
      case 1:
         pAmt    = pOutbuf+OFF_SALE1_AMT;
         pDate   = pOutbuf+OFF_SALE1_DT;
         pDoc    = pOutbuf+OFF_SALE1_DOC;
         pDocType= pOutbuf+OFF_SALE1_DOCTYPE;
         pNxtAmt = pOutbuf+OFF_SALE2_AMT;
         pNxtDate= pOutbuf+OFF_SALE2_DT;
         pNxtDoc = pOutbuf+OFF_SALE2_DOC;
         pNxtDocType = pOutbuf+OFF_SALE2_DOCTYPE;
         break;
      case 2:
         pAmt    = pOutbuf+OFF_SALE2_AMT;
         pDate   = pOutbuf+OFF_SALE2_DT;
         pDoc    = pOutbuf+OFF_SALE2_DOC;
         pDocType= pOutbuf+OFF_SALE2_DOCTYPE;
         pNxtAmt = pOutbuf+OFF_SALE3_AMT;
         pNxtDate= pOutbuf+OFF_SALE3_DT;
         pNxtDoc = pOutbuf+OFF_SALE3_DOC;
         pNxtDocType = pOutbuf+OFF_SALE3_DOCTYPE;
         break;
      case 3:
         pAmt    = pOutbuf+OFF_SALE3_AMT;
         pDate   = pOutbuf+OFF_SALE3_DT;
         pDoc    = pOutbuf+OFF_SALE3_DOC;
         pDocType= pOutbuf+OFF_SALE3_DOCTYPE;
         pNxtAmt = NULL;
         pNxtDate= NULL;
         pNxtDoc = NULL;
         pNxtDocType = NULL;
         break;
      default:
         return 1;
   }

#ifdef _DEBUG
   //if (!memcmp(pRec->Apn, "00801440180000", 10) && iCnt == 3)
   //   lTmp = 0;
#endif

   lDocNum = atoin(pRec->RecPage, SSIZ_REC_PAGE);
   lDttAmt = atoin(pRec->SalePrice, SIZ_SALE1_AMT);
   //sprintf(acDocNum, "PAGE %u           ", lDocNum);
   sprintf(acDocNum, "%.6s%.4d    ", &pRec->RecDate[2], lDocNum);

   if ((iTmp = memcmp(pDate, pSaleDate, SIZ_SALE1_DT)) > 0)
   {
      if (iCnt == 2 && lDttAmt > 0)
      {
         // Check next sale record
         pAmt  = pOutbuf+OFF_SALE3_AMT;
         pDate = pOutbuf+OFF_SALE3_DT;
         pDoc  = pOutbuf+OFF_SALE3_DOC;
         pDocType= pOutbuf+OFF_SALE3_DOCTYPE;
         pNxtAmt = NULL;
         pNxtDate= NULL;
         pNxtDoc = NULL;
         pNxtDocType = NULL;
         iTmp = memcmp(pDate, pSaleDate, SIZ_SALE1_DT);
      }
      if (iTmp > 0)
         return iTmp;   // sale rec is older than current
   }

   // If same date, update sale amt and Doc#
   if (!iTmp)
   {
      lTmp = atoin(pAmt, SIZ_SALE1_AMT);
      if (!lTmp && lDttAmt > 0)
      {
         memcpy(pAmt, pRec->SalePrice, SIZ_SALE1_AMT);
         if (pRec->StampCode == 'P')
            *(pAmt+SIZ_SALE1_AMT) = pRec->StampCode;
         else
            *(pAmt+SIZ_SALE1_AMT) = 'F';
      }

      if (!*pDoc && lDocNum > 0)
         memcpy(pDoc, acDocNum, SIZ_SALE1_DOC);
   } else
   {
      // New sale - move current sale to next
      if (pNxtDate && *pDate > '0')
      {
         lTmp = atoin(pAmt, SIZ_SALE1_AMT);
         if (lTmp > 0 && lTmp != lDttAmt)
         {
            memcpy(pNxtAmt, pAmt, SIZ_SALE1_AMT);
            *(pNxtAmt+SIZ_SALE1_AMT) = *(pAmt+SIZ_SALE1_AMT);  // Sale code
         }
         memcpy(pNxtDoc, pDoc, SIZ_SALE1_DOC);
         memcpy(pNxtDate, pDate, SIZ_SALE1_DT);
      }

      memcpy(pDate, pSaleDate, SIZ_SALE1_DT);
      if (lDttAmt > 0)
      {
         memcpy(pAmt, pRec->SalePrice, SIZ_SALE1_AMT);
         if (pRec->StampCode == 'P')
            *(pAmt+SIZ_SALE1_AMT) = pRec->StampCode;
         else
            *(pAmt+SIZ_SALE1_AMT) = 'F';
      }

      if (lDocNum > 0)
         memcpy(pDoc, acDocNum, SIZ_SALE1_DOC);
      else
         memset(pDoc, ' ', SIZ_SALE1_DOC);

      // Translate doctype
      iTmp = XrefCode2Idx((XREFTBL *)&asDeed[0], pRec->DeedType, iNumDeeds);
      if (iTmp > 0)
      {
         iTmp = sprintf(acDocType, "%d     ", iTmp);
         memcpy(pDocType, acDocType, iTmp);
      }

      // Update transfer on first sale
      if (1 == iCnt)
      {
         if (memcmp(pSaleDate, pOutbuf+OFF_TRANSFER_DT, SIZ_TRANSFER_DT) > 0)
         {
            memcpy(pOutbuf+OFF_TRANSFER_DT, pSaleDate, SIZ_TRANSFER_DT);
            if (lDocNum > 0)
               memcpy(pOutbuf+OFF_TRANSFER_DOC, acDocNum, SIZ_SALE1_DOC);
            else
               memset(pOutbuf+OFF_TRANSFER_DOC, ' ', SIZ_SALE1_DOC);
         }

         // Update seller
         quoteRem(pRec->Grantor, SIZ_SELLER);
         memcpy(pOutbuf+OFF_SELLER, pRec->Grantor, SIZ_SELLER);

         // Set multi parcels
         if ((iTmp = atoin(pRec->NumParcels, SSIZ_NUM_PARCELS)) > 1)
            *(pOutbuf+OFF_MULTI_APN) = 'Y';
         else
            *(pOutbuf+OFF_MULTI_APN) = ' ';
      }
   }

   return 0;
}

// Cum sale file is a fixed length 144-byte record
int Sac_MergeSale(char *pOutbuf)
{
   static   char  acRec[1024], *pRec=NULL;
   static   int   iTmpSaleLen;

   int      iRet, iLoop;
   int      iSaleCnt;

   // Get first Char rec for first call
   if (!pRec && !lSaleMatch)
   {
      iTmpSaleLen = fread(acRec, 1, iCSalLen, fdSale);
      //pRec = fgets(acRec, 1024, fdSale);
   }

   do
   {
      if (iTmpSaleLen != iCSalLen)
      {
         fclose(fdSale);
         fdSale = NULL;
         return 1;      // EOF
      }

      // Compare Apn
      iLoop = memcmp(pOutbuf, acRec, myCounty.iApnLen);
      if (iLoop > 0)
      {
         if (bDebug)
            LogMsg0("Skip Sale rec  %.*s", myCounty.iApnLen, acRec);
         //pRec = fgets(acRec, 1024, fdSale);
         iTmpSaleLen = fread(acRec, 1, iCSalLen, fdSale);
         lSaleSkip++;
      }
   } while (iLoop > 0);

   // If not match, return
   if (iLoop)
      return 0;

   iSaleCnt = 0;
   do
   {
#ifdef _DEBUG
      //if (!memcmp(pOutbuf, "001015008", 9))
      //   iRet = 0;
#endif
      iSaleCnt++;
      iRet = Sac_MergeSale1(pOutbuf, acRec, iSaleCnt);
      // Return value > 0 if sale rec is older than current
      if (iRet)
         break;

      // Get next sale record
      iTmpSaleLen = fread(acRec, 1, iCSalLen, fdSale);
      if (iTmpSaleLen != iCSalLen)
      {
         fclose(fdSale);
         fdSale = NULL;
         break;
      }

      // Update flag
      *(pOutbuf+OFF_AR_CODE1+iSaleCnt-1) = 'A';           // Assessor-Recorder code
   } while (!memcmp(pOutbuf, acRec, myCounty.iApnLen) && iSaleCnt < 3);

   lSaleMatch++;

   return 0;
}

/********************************* Sac_MergeRoll *****************************
 *
 * Return 0 if successful, >0 if error
 *
 *****************************************************************************/

void Sac_MergeRoll(char *pOutbuf, char *pRollRec, int iLen, int iFlag)
{
   SAC_ROLL *pRec;
   char     acTmp[256], acTmp1[256];
   long     lTmp;
   int      iRet;

   pRec = (SAC_ROLL *)pRollRec;
   if (iFlag & CREATE_R01)
   {
      // Clear output buffer
      memset(pOutbuf, ' ', iRecLen);

      // Start copying data
      memcpy(pOutbuf, pRec->Apn, RSIZ_APN);
      memcpy(pOutbuf+OFF_CO_NUM, "34SAC", 5);

      // Format APN
      iRet = formatApn(pRec->Apn, acTmp, &myCounty);
      memcpy(pOutbuf+OFF_APN_D, acTmp, iRet);

      // Create MapLink
      iRet = formatMapLink(pRec->Apn, acTmp, &myCounty);
      memcpy(pOutbuf+OFF_MAPLINK, acTmp, iRet);

      // Create index map link
      if (getIndexPage(acTmp, acTmp1, &myCounty))
         memcpy(pOutbuf+OFF_IMAPLINK, acTmp1, iRet);

      // Year assessed
      memcpy(pOutbuf+OFF_YR_ASSD, myCounty.acYearAssd, 4);

      // HO Exempt
      long lHoe = atoin(pRec->HO_Exe, RSIZ_HO_EXE);
      if (lHoe > 0)
         *(pOutbuf+OFF_HO_FL) = '1';      // 'Y'
      else
         *(pOutbuf+OFF_HO_FL) = '2';      // 'N'

      // Veteran exempt
      long lVete = atoin(pRec->Vet_Exe, RSIZ_HO_EXE);
      // Religious exempt
      long lRele = atoin(pRec->Rel_Exe, RSIZ_HO_EXE);

      // Exemp total
      *(pOutbuf+OFF_FULL_EXEMPT) = ' ';
      long lExe = lHoe+lVete+lRele;
      if (lExe > 0)
      {
         sprintf(acTmp, "%*u", SIZ_EXE_TOTAL, lExe);
         memcpy(pOutbuf+OFF_EXE_TOTAL, acTmp, SIZ_EXE_TOTAL);

         // Set full exemption flag
         //*(pOutbuf+OFF_FULL_EXEMPT) = 'Y';          // ?????
      }

      // Land
      long lLand = atoin(pRec->Land, RSIZ_LAND);
      if (lLand > 0)
      {
         sprintf(acTmp, "%*u", SIZ_LAND, lLand);
         memcpy(pOutbuf+OFF_LAND, acTmp, SIZ_LAND);
      }

      // Improve
      long lImpr = atoin(pRec->Impr, RSIZ_LAND);
      if (lImpr > 0)
      {
         sprintf(acTmp, "%*u", SIZ_IMPR, lImpr);
         memcpy(pOutbuf+OFF_IMPR, acTmp, SIZ_IMPR);
      }

      // Other value
      long lFixture = atoin(pRec->Fixt_Val, RSIZ_LAND);
      long lPers = atoin(pRec->PP_Val, RSIZ_LAND);
      long lBusInv = atoin(pRec->Bus_Inv, RSIZ_LAND);
      lTmp = lPers + lFixture + lBusInv;
      if (lTmp > 0)
      {
         sprintf(acTmp, "%*u", SIZ_OTHER, lTmp);
         memcpy(pOutbuf+OFF_OTHER, acTmp, SIZ_OTHER);

         if (lPers > 0)
         {
            sprintf(acTmp, "%d          ", lPers);
            memcpy(pOutbuf+OFF_PERSPROP, acTmp, SIZ_PERSPROP);
         }
         if (lFixture > 0)
         {
            sprintf(acTmp, "%d          ", lFixture);
            memcpy(pOutbuf+OFF_FIXTR, acTmp, SIZ_FIXTR);
         }
         if (lBusInv > 0)
         {
            sprintf(acTmp, "%d         ", lBusInv);
            memcpy(pOutbuf+OFF_BUSINV, acTmp, SIZ_BUSINV);
         }
      }

      // Gross total
      lTmp += lLand+lImpr;
      if (lTmp > 0)
      {
         sprintf(acTmp, "%*u", SIZ_GROSS, lTmp);
         memcpy(pOutbuf+OFF_GROSS, acTmp, SIZ_GROSS);

         // Ratio
         if (lImpr > 0)
         {
            sprintf(acTmp, "%*u", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
            memcpy(pOutbuf+OFF_RATIO, acTmp, SIZ_RATIO);
         }
      }
   } else if (bReplValue)
   {
      // HO Exempt
      long lHoe = atoin(pRec->HO_Exe, RSIZ_HO_EXE);
      if (lHoe > 0)
         *(pOutbuf+OFF_HO_FL) = '1';      // 'Y'
      else
         *(pOutbuf+OFF_HO_FL) = '2';      // 'N'

      // Veteran exempt
      long lVete = atoin(pRec->Vet_Exe, RSIZ_HO_EXE);
      // Religious exempt
      long lRele = atoin(pRec->Rel_Exe, RSIZ_HO_EXE);

      // Exemp total
      *(pOutbuf+OFF_FULL_EXEMPT) = ' ';
      long lExe = lHoe+lVete+lRele;
      if (lExe > 0)
      {
         sprintf(acTmp, "%*u", SIZ_EXE_TOTAL, lExe);
         memcpy(pOutbuf+OFF_EXE_TOTAL, acTmp, SIZ_EXE_TOTAL);

         // Set full exemption flag
         //*(pOutbuf+OFF_FULL_EXEMPT) = 'Y';          // ?????
      }

      // Land
      long lLand = atoin(pRec->Land, RSIZ_LAND);
      if (lLand > 0)
      {
         sprintf(acTmp, "%*u", SIZ_LAND, lLand);
         memcpy(pOutbuf+OFF_LAND, acTmp, SIZ_LAND);
      }

      // Improve
      long lImpr = atoin(pRec->Impr, RSIZ_LAND);
      if (lImpr > 0)
      {
         sprintf(acTmp, "%*u", SIZ_IMPR, lImpr);
         memcpy(pOutbuf+OFF_IMPR, acTmp, SIZ_IMPR);
      }

      // Other value
      long lFixture = atoin(pRec->Fixt_Val, RSIZ_LAND);
      long lPers = atoin(pRec->PP_Val, RSIZ_LAND);
      long lBusInv = atoin(pRec->Bus_Inv, RSIZ_LAND);
      lTmp = lPers + lFixture + lBusInv;
      if (lTmp > 0)
      {
         sprintf(acTmp, "%*u", SIZ_OTHER, lTmp);
         memcpy(pOutbuf+OFF_OTHER, acTmp, SIZ_OTHER);

         if (lPers > 0)
         {
            sprintf(acTmp, "%d          ", lPers);
            memcpy(pOutbuf+OFF_PERSPROP, acTmp, SIZ_PERSPROP);
         }
         if (lFixture > 0)
         {
            sprintf(acTmp, "%d          ", lFixture);
            memcpy(pOutbuf+OFF_FIXTR, acTmp, SIZ_FIXTR);
         }
         if (lBusInv > 0)
         {
            sprintf(acTmp, "%d         ", lBusInv);
            memcpy(pOutbuf+OFF_BUSINV, acTmp, SIZ_BUSINV);
         }
      }

      // Gross total
      lTmp += lLand+lImpr;
      if (lTmp > 0)
      {
         sprintf(acTmp, "%*u", SIZ_GROSS, lTmp);
         memcpy(pOutbuf+OFF_GROSS, acTmp, SIZ_GROSS);

         // Ratio
         if (lImpr > 0)
         {
            sprintf(acTmp, "%*u", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
            memcpy(pOutbuf+OFF_RATIO, acTmp, SIZ_RATIO);
         }
      }
   }

   // TRA
   lTmp = atoin(pRec->TRA, RSIZ_TRA);
   if (lTmp > 0)
   {
      iRet = sprintf(acTmp, "%.6d", lTmp);
      memcpy(pOutbuf+OFF_TRA, acTmp, iRet);
   }

   // Set full exemption flag
   long lGross = atoin(pOutbuf+OFF_GROSS, SIZ_GROSS);
   long lExe   = atoin(pOutbuf+OFF_EXE_TOTAL, SIZ_EXE_TOTAL);
   if (lExe >= lGross)
      *(pOutbuf+OFF_FULL_EXEMPT) = 'Y';

   // Merge Addr
   Sac_MergeAdr(pOutbuf, &pRec->Mailing, &pRec->Situs);

   // UseCode
   if (pRec->UseCode[0] > ' ')
   {
      memcpy(pOutbuf+OFF_USE_CO, pRec->UseCode, RSIZ_LAND_USE);
      memset(pOutbuf+OFF_USE_STD, ' ', SIZ_USE_STD);

      // Special case for AG009A
      if (!memcmp(pRec->UseCode, "AG009A", 6))
         memcpy(pOutbuf+OFF_USE_STD, "102", 3);
      else
         updateStdUse(pOutbuf+OFF_USE_STD, pRec->UseCode, RSIZ_LAND_USE, pOutbuf);

      // Check for Ag preserved
      if (pRec->UseCode[0] == 'H' && pRec->UseCode[5] == 'G')
      {
         *(pOutbuf+OFF_AG_PRE) = 'Y';
         memcpy(pOutbuf+OFF_USE_STD, "401", 3);
      }
   } else
      memcpy(pOutbuf+OFF_USE_STD, USE_UNASGN, SIZ_USE_STD);

   // Owner
   memcpy(acTmp, pRec->OwnerName, RSIZ_OWNER_NAME);
   Sac_MergeOwner(pOutbuf, myTrim(acTmp, RSIZ_OWNER_NAME));

   // Care Of
   if (pRec->CareOf[0] > ' ')
      updateCareOf(pOutbuf, pRec->CareOf, RSIZ_CO_NAME);

   // Legal

   // Zoning
   if (pRec->Zoning[0] > ' ')
      memcpy(pOutbuf+OFF_ZONE, pRec->Zoning, RSIZ_ZONING);

   // Transfer - YYMMDD
   if (memcmp(pRec->RecDate, "000000", 6))
   {
      if (memcmp(pRec->RecDate, "30", 2) < 0)
         sprintf(acTmp, "20%.6s", pRec->RecDate);
      else
         sprintf(acTmp, "19%.6s", pRec->RecDate);

      lTmp = atoin(pRec->RecPage, RSIZ_REC_PAGE);
      if (lTmp > 0)
      {
         memcpy(pOutbuf+OFF_TRANSFER_DT, acTmp, SIZ_TRANSFER_DT);
         // Standard format should be YYMMDD-PAGE.
         // Here we format this way just to be compatible with what Fernando did
         //sprintf(acTmp, "PAGE %d      ", lTmp);
         sprintf(acTmp, "%.*s    ", SSIZ_DOCNUM, pRec->RecDate);
         memcpy(pOutbuf+OFF_TRANSFER_DOC, acTmp, SIZ_TRANSFER_DOC);
      }
   }
}

void Sac_CreateRollRec(char *pOutbuf, char *pRollRec)
{
   SAC_LVAL *pRec;
   char     acTmp[256], acTmp1[256];
   long     lTmp;
   int      iRet;

   pRec = (SAC_LVAL *)pRollRec;
   // Clear output buffer
   memset(pOutbuf, ' ', iRecLen);

   // Start copying data
   memcpy(pOutbuf, pRec->Apn, LSIZ_APN);
   memcpy(pOutbuf+OFF_CO_NUM, "34SAC", 5);

   // Format APN
   iRet = formatApn(pRec->Apn, acTmp, &myCounty);
   memcpy(pOutbuf+OFF_APN_D, acTmp, iRet);

   // Create MapLink
   iRet = formatMapLink(pRec->Apn, acTmp, &myCounty);
   memcpy(pOutbuf+OFF_MAPLINK, acTmp, iRet);

   // Create index map link
   if (getIndexPage(acTmp, acTmp1, &myCounty))
      memcpy(pOutbuf+OFF_IMAPLINK, acTmp1, iRet);

   // Year assessed
   memcpy(pOutbuf+OFF_YR_ASSD, myCounty.acYearAssd, 4);

   // Land
   long lLand = atoin(pRec->Land, LSIZ_LAND);
   if (lLand > 0)
   {
      sprintf(acTmp, "%*u", SIZ_LAND, lLand);
      memcpy(pOutbuf+OFF_LAND, acTmp, SIZ_LAND);
   }

   // Improve
   long lImpr = atoin(pRec->Impr, LSIZ_LAND);
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*u", SIZ_IMPR, lImpr);
      memcpy(pOutbuf+OFF_IMPR, acTmp, SIZ_IMPR);
   }

   // Other value
   long lFixture = atoin(pRec->Fixt_Val, LSIZ_LAND);
   long lPers = atoin(pRec->PP_Val, LSIZ_LAND);
   lTmp = lPers + lFixture;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*u", SIZ_OTHER, lTmp);
      memcpy(pOutbuf+OFF_OTHER, acTmp, SIZ_OTHER);

      if (lPers > 0)
      {
         sprintf(acTmp, "%d          ", lPers);
         memcpy(pOutbuf+OFF_PERSPROP, acTmp, SIZ_PERSPROP);
      }
      if (lFixture > 0)
      {
         sprintf(acTmp, "%d          ", lFixture);
         memcpy(pOutbuf+OFF_FIXTR, acTmp, SIZ_FIXTR);
      }
   }

   // Gross total
   lTmp += lLand+lImpr;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*u", SIZ_GROSS, lTmp);
      memcpy(pOutbuf+OFF_GROSS, acTmp, SIZ_GROSS);

      // Ratio
      if (lImpr > 0)
      {
         sprintf(acTmp, "%*u", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
         memcpy(pOutbuf+OFF_RATIO, acTmp, SIZ_RATIO);
      }
   }

   // HO Exempt
   long lHoe = atoin(pRec->HO_Exe, LSIZ_HO_EXE);
   if (lHoe > 0)
      *(pOutbuf+OFF_HO_FL) = '1';      // 'Y'
   else
      *(pOutbuf+OFF_HO_FL) = '2';      // 'N'

   // Exemp total
   *(pOutbuf+OFF_FULL_EXEMPT) = ' ';
   long lExe = atoin(pRec->Total_Exe, LSIZ_HO_EXE);
   if (lExe > 0)
   {
      sprintf(acTmp, "%*u", SIZ_EXE_TOTAL, lExe);
      memcpy(pOutbuf+OFF_EXE_TOTAL, acTmp, SIZ_EXE_TOTAL);
   }

   // Set full exemption flag
   long lGross = atoin(pOutbuf+OFF_GROSS, SIZ_GROSS);
   if (lExe >= lGross)
      *(pOutbuf+OFF_FULL_EXEMPT) = 'Y';

   // TRA
   lTmp = atoin(pRec->TRA, LSIZ_TRA);
   if (lTmp > 0)
   {
      iRet = sprintf(acTmp, "%.6d", lTmp);
      memcpy(pOutbuf+OFF_TRA, acTmp, iRet);
   }

   if (pRec->UseCode[0] > ' ')
   {
      memcpy(pOutbuf+OFF_USE_CO, pRec->UseCode, LSIZ_LAND_USE);

      // Special case for AG009A
      if (!memcmp(pRec->UseCode, "AG009A", 6))
         memcpy(pOutbuf+OFF_USE_STD, "102", 3);
      // Check for Ag preserved
      else if (pRec->UseCode[0] == 'H' && pRec->UseCode[5] == 'G')
      {
         *(pOutbuf+OFF_AG_PRE) = 'Y';
         memcpy(pOutbuf+OFF_USE_STD, "401", 3);
      } else
         updateStdUse(pOutbuf+OFF_USE_STD, pRec->UseCode, LSIZ_LAND_USE, pOutbuf);
   } else
      memcpy(pOutbuf+OFF_USE_STD, USE_UNASGN, SIZ_USE_STD);

   // Merge Addr
   //Sac_MergeAdr(pOutbuf, pRollRec);
   Sac_MergeAdr(pOutbuf, &pRec->Mailing, &pRec->Situs);

   // Owner
   memcpy(acTmp, pRec->OwnerName, LSIZ_OWNER_NAME);
   Sac_MergeOwner(pOutbuf, myTrim(acTmp, LSIZ_OWNER_NAME));

   // Care Of
   if (pRec->CareOf[0] > ' ')
      updateCareOf(pOutbuf, pRec->CareOf, LSIZ_CO_NAME);

   // Legal

   // Zoning
   if (pRec->Zoning[0] > ' ')
      memcpy(pOutbuf+OFF_ZONE, pRec->Zoning, LSIZ_ZONING);

   // Transfer - YYMMDD
   if (memcmp(pRec->RecDate, "000000", 6))
   {
      if (memcmp(pRec->RecDate, "30", 2) < 0)
         sprintf(acTmp, "20%.6s", pRec->RecDate);
      else
         sprintf(acTmp, "19%.6s", pRec->RecDate);
      memcpy(pOutbuf+OFF_TRANSFER_DT, acTmp, SIZ_TRANSFER_DT);

      lTmp = atoin(pRec->RecPage, LSIZ_REC_PAGE);
      if (lTmp > 0)
      {
         // Standard format should be YYMMDD-PAGE.
         // Here we format this way just to be compatible with what Fernando did
         //sprintf(acTmp, "PAGE %d      ", lTmp);
         sprintf(acTmp, "%.*s    ", SSIZ_DOCNUM, pRec->RecDate);
         memcpy(pOutbuf+OFF_TRANSFER_DOC, acTmp, SIZ_TRANSFER_DOC);
      }
   }

}

/****************************** Sac_MergeRollUns *****************************
 *
 * Create/merge unsecured record
 *
 *****************************************************************************/

void Sac_MergeUnsRec(char *pOutbuf, char *pRollRec, int iLen, int iFlag)
{
   SAC_UNS  *pRec;
   char     acTmp[256], acApn[32], acCode[32], *pTmp;
   long     lTmp;
   int      iRet;

   pRec = (SAC_UNS *)pRollRec;
   if (iFlag & CREATE_R01)
   {
      // Clear output buffer
      memset(pOutbuf, ' ', iRecLen);

      // Start copying data
      memcpy(acApn, pRec->Apn, USIZ_APN);
      memcpy(&acApn[USIZ_APN], pRec->SubParcelUns, USIZ_SUBPARCEL_UNS);
      acApn[RSIZ_APN] = 0;
      memcpy(pOutbuf, acApn, RSIZ_APN);
      memcpy(pOutbuf+OFF_CO_NUM, "34SAC", 5);

      // Format APN
      iRet = formatApn(acApn, acTmp, &myCounty);
      memcpy(pOutbuf+OFF_APN_D, acTmp, iRet);

      // Year assessed
      memcpy(pOutbuf+OFF_YR_ASSD, myCounty.acYearAssd, 4);

      // Unsecured flag
      *(pOutbuf+OFF_UNS_FLG) = 'Y';

      // HO Exempt
      long lHoe = atoin(pRec->HO_Exe, USIZ_HO_EXE);
      if (lHoe > 0)
         *(pOutbuf+OFF_HO_FL) = '1';      // 'Y'
      else
         *(pOutbuf+OFF_HO_FL) = '2';      // 'N'

      // Veteran exempt
      long lVete = atoin(pRec->Vet_Exe, USIZ_VET_EXE);
      // Religious exempt
      long lRele = atoin(pRec->Rel_Exe, USIZ_REL_EXE);

      // Exemp total
      lTmp = lHoe+lVete+lRele;
      if (lTmp > 0)
      {
         sprintf(acTmp, "%*u", SIZ_EXE_TOTAL, lTmp);
         memcpy(pOutbuf+OFF_EXE_TOTAL, acTmp, SIZ_EXE_TOTAL);

         // Set full exemption flag
         *(pOutbuf+OFF_FULL_EXEMPT) = 'Y';
      }

      // Land
      long lLand = atoin(pRec->Land, USIZ_LAND);
      if (lLand > 0)
      {
         sprintf(acTmp, "%*u", SIZ_LAND, lLand);
         memcpy(pOutbuf+OFF_LAND, acTmp, SIZ_LAND);
      }

      // Improve - Unsecured parcel -> everything is impr
      long lImpr    = atoin(pRec->Impr, USIZ_LAND);
      long lFixture = atoin(pRec->Fixt_Val, USIZ_LAND);
      long lPers    = atoin(pRec->PP_Val, USIZ_LAND);
      long lBoat    = atoin(pRec->Boat, USIZ_LAND);
      long lAirCrft = atoin(pRec->Aircraft, USIZ_LAND);
      lImpr = lImpr + lPers + lFixture + lBoat + lAirCrft;
      if (lImpr > 0)
      {
         sprintf(acTmp, "%*u", SIZ_IMPR, lImpr);
         memcpy(pOutbuf+OFF_IMPR, acTmp, SIZ_IMPR);
      }

      // Gross total
      lTmp += lLand+lImpr;
      if (lTmp > 0)
      {
         sprintf(acTmp, "%*u", SIZ_GROSS, lTmp);
         memcpy(pOutbuf+OFF_GROSS, acTmp, SIZ_GROSS);

         // Ratio
         if (lImpr > 0)
         {
            sprintf(acTmp, "%*u", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
            memcpy(pOutbuf+OFF_RATIO, acTmp, SIZ_RATIO);
         }
      }
   }

   // Merge Addr
   Sac_MergeAdrUns(pOutbuf, pRollRec);

   // Property class
   memcpy(acTmp, pRec->PropCls, USIZ_PROPCLS);
   acTmp[USIZ_PROPCLS] = 0;
   iRet = Value2Code(acTmp, acCode, Sac_PropCls);
   if (iRet >= 0)
      memcpy(pOutbuf+OFF_IMPR_TYPE, acCode, strlen(acCode));

   // TRA
   if (pRec->TRA[0] > ' ')
   {
      iRet = sprintf(acTmp, "0%.5s", pRec->TRA);
      memcpy(pOutbuf+OFF_TRA, acTmp, iRet);
   }

   // Owner
   memcpy(acTmp, pRec->OwnerName, USIZ_OWNER_NAME);
   myTrim(acTmp, USIZ_OWNER_NAME);
   if (pTmp = strchr(acTmp, '//'))
   {
      if (*(pTmp+1) == '//')
      {
         *pTmp = 0;
         pTmp += 2;
         memcpy(pOutbuf+OFF_NAME2, acTmp, strlen(acTmp));
      }
   }
   memcpy(pOutbuf+OFF_NAME1, acTmp, strlen(acTmp));

   // Care Of
   if (pRec->CareOf[0] > ' ')
   {
      /*
      memcpy(acTmp, pRec->CareOf, USIZ_CO_NAME);
      acTmp[USIZ_CO_NAME] = 0;
      lTmp = strlen(acTmp);
      if (lTmp > SIZ_CARE_OF)
         lTmp = SIZ_CARE_OF;
      memcpy(pOutbuf+OFF_CARE_OF, acTmp, lTmp);
      */
      updateCareOf(pOutbuf, pRec->CareOf, USIZ_CO_NAME);
   }

   // Legal
   iRet = 0;
   if (pRec->AircraftId[0] > ' ')
      iRet = sprintf(acTmp, "AIRCRAFT ID# %s", pRec->AircraftId);
   else if (pRec->BoatId[0] > ' ')
      iRet = sprintf(acTmp, "BOAT ID# %s", pRec->Boat);
   if (iRet > 0)
   {
      updateLegal(pOutbuf, acTmp);
      if (iRet > iMaxLegal)
         iMaxLegal = iRet;
   }
}

/******************************* Sac_MergeUnsRoll *****************************
 *
 * Merge unsecured roll.  Assuming county issues once a year.
 * Return 0 if success.
 *
 ******************************************************************************/

int Sac_MergeUnsRoll(char *pUnsFile, int iSkip)
{
   char     *pTmp, acRec[MAX_RECSIZE], acBuf[MAX_RECSIZE], acRollRec[MAX_RECSIZE];
   char     acRawFile[_MAX_PATH], acUnsFile[_MAX_PATH], acOutFile[_MAX_PATH], acApn[32];

   HANDLE   fhIn, fhOut;

   int      iTmp, iRollUpd=0, iNewRec=0;
   DWORD    nBytesRead;
   DWORD    nBytesWritten;
   BOOL     bRet, bEof;
   long     lRet=0, lCnt=0;

   LogMsg("Loading Unsecured file %s", pUnsFile);

   // Create duplicate list Uns_Public.del
   strcpy(acOutFile, pUnsFile);
   strcpy(&acOutFile[strlen(acOutFile)-3], "DEL");
   iTmp = sortFile(pUnsFile, acOutFile, "S(1,10,C,A,15,4,C,A) F(TXT) DUPSURPLUS(1,10,15,4)");

   // Resort Uns_Public.txt
   strcpy(acOutFile, pUnsFile);
   strcpy(&acOutFile[strlen(acOutFile)-3], "SRT");
   iTmp = sortFile(pUnsFile, acOutFile, "S(1,10,C,A,15,4,C,A) F(TXT) DUPOUT(1,10,15,4)");
   if (iTmp <= 0)
   {
      LogMsg("***** Error sorting %s", pUnsFile);
      return -1;
   }

   // Make sorted file input file for this process
   strcpy(acUnsFile, acOutFile);

   sprintf(acRawFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "R01");
   sprintf(acOutFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "U01");

   // Rename files for processing
   if (_access(acRawFile, 0))
   {
      sprintf(acRawFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "S01");
      if (_access(acRawFile, 0))
      {
         LogMsg("***** Missing input file %s.  Please recheck!", acRawFile);
         return -2;
      }
   }

   // Open Roll file
   LogMsg("Open Unsecured file %s", acUnsFile);
   fdRoll = fopen(acUnsFile, "r");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", acUnsFile);
      return -3;
   }

   // Open Input file
   LogMsg("Open input file %s", acRawFile);
   fhIn = CreateFile(acRawFile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhIn == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening raw input file: %s\n", acRawFile);
      return -4;
   }

   // Open Output file
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error creating output file: %s\n", acOutFile);
      return -4;
   }

   // Get first RollRec
   pTmp = fgets((char *)&acRollRec[0], MAX_RECSIZE, fdRoll);
   bEof = (pTmp ? false:true);

   // Copy skip record
   memset(acBuf, ' ', iRecLen);
   while (iSkip-- > 0)
   {
      ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);
      // Ignore record start with 99999999
      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
   }

   iNoMatch=iBadCity=iBadSuffix=0;

   // Merge loop
   while (!bEof)
   {
      bRet = ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);

      // Check for EOF
      if (!bRet)
      {
         LogMsg("***** Error reading input file %s (%f)", acRawFile, GetLastError());
         break;
      }

      // EOF ?
      if (iRecLen != nBytesRead)
         break;

      NextUnsRec:
      memcpy(acApn, acRollRec, USIZ_APN);
      memcpy(&acApn[USIZ_APN], &acRollRec[UOFF_SUBPARCEL_UNS], USIZ_SUBPARCEL_UNS);
      acApn[RSIZ_APN] = 0;

      iTmp = memcmp(acBuf, acApn, iApnLen);
      if (!iTmp)
      {
         // Merge roll data
         //Sac_MergeUnsRec(acBuf, acRollRec, iRollLen, UPDATE_R01);
         iRollUpd++;

         // Read next roll record
         pTmp = fgets(acRollRec, MAX_RECSIZE, fdRoll);

         if (!pTmp)
            bEof = true;    // Signal to stop
      } else if (iTmp > 0)
      {
         // Create new R01 record
         Sac_MergeUnsRec(acRec, acRollRec, iRollLen, CREATE_R01);
         iNewRec++;

         bRet = WriteFile(fhOut, acRec, iRecLen, &nBytesWritten, NULL);
         lCnt++;

         // Get next roll record
         pTmp = fgets(acRollRec, MAX_RECSIZE, fdRoll);

         if (!pTmp)
            bEof = true;    // Signal to stop
         else
            goto NextUnsRec;
      }

      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);

      if (!bRet)
      {
         LogMsg("***** Error occurs: %d\n", GetLastError());
         lRet = WRITE_ERR;
         break;
      }
   }

   // Do the rest of the file
   while (!bEof && !lRet)
   {
      // Create new R01 record
      Sac_MergeUnsRec(acRec, acRollRec, iRollLen, CREATE_R01);
      iNewRec++;

      bRet = WriteFile(fhOut, acRec, iRecLen, &nBytesWritten, NULL);
      lCnt++;

      // Get next roll record
      pTmp = fgets(acRollRec, MAX_RECSIZE, fdRoll);

      if (!pTmp)
         bEof = true;    // Signal to stop
   }

   // Do the rest of the file
   while (nBytesRead>0 && ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL))
   {
      if (!nBytesRead)
         break;

      /*
      iRet = formatApn(acBuf, acTmp, &myCounty);
      memcpy((char *)&acBuf[OFF_APN_D], acTmp, iRet);
      iRet = formatMapLink(acBuf, acTmp, &myCounty);
      memset((char *)&acTmp[iRet], ' ', SIZ_MAPLINK-iRet);
      memcpy((char *)&acBuf[OFF_MAPLINK], acTmp, SIZ_MAPLINK);
      */
      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
      lCnt++;
   }

   // Close files
   if (fdRoll)
      fclose(fdRoll);

   if (fhOut)
      CloseHandle(fhOut);
   if (fhIn)
      CloseHandle(fhIn);

   // Rename output file
   if (!lRet)
   {
      sprintf(acRawFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "R01");
      if (!_access(acRawFile, 0))
         remove(acRawFile);
      lRet = rename(acOutFile, acRawFile);
   }

   LogMsg("Total output records:       %u", lCnt);
   LogMsg("Total new records:          %u", iNewRec);
   LogMsg("Total updated records:      %u", iRollUpd);
   LogMsg("Total bad-city records:     %u", iBadCity);
   LogMsg("Total bad-suffix records:   %u\n", iBadSuffix);

   printf("\nTotal output records: %u\n", lCnt);

   lRecCnt = lCnt;
   return lRet;
}

/******************************* Sac_MergeUnitNo *****************************
 *
 * Merge UnitNo from GIS file
 *
 *****************************************************************************/

int Sac_MergeUnitNo(char *pOutbuf)
{
   static   char acRec[1024], *pRec=NULL;
   int      iLoop;
   XADR_REC *pXAdr=(XADR_REC *)&acRec[0];

   // Get first Char rec for first call
   if (!pRec)
   {
      pRec = fgets(acRec, 1024, fdGis);
   }

   do
   {
      if (!pRec)
      {
         fclose(fdGis);
         fdGis = NULL;
         return 1;      // EOF
      }

      // Compare Apn
      iLoop = memcmp(pOutbuf, pRec, myCounty.iApnLen);
      if (iLoop > 0)
      {
         if (bDebug)
            LogMsg0("Skip Char rec  %.*s", myCounty.iApnLen, pRec);
         pRec = fgets(acRec, 1024, fdGis);
      }
   } while (iLoop > 0);

   // If not match, return
   if (iLoop)
      return 1;

   if (pXAdr->Unit[0] >  ' ')
   {
      char sAddr1[256], sUnit[16];
      int  iStrNo = atoin(pXAdr->strNum, SIZ_S_STRNUM);
      if (pXAdr->Unit[3] > ' ')
      {
         sprintf(sUnit, "%.*s", SIZ_S_UNITNO, pXAdr->Unit);
         sprintf(sAddr1, "%d%.3s %.37s", iStrNo, pXAdr->strSub, pXAdr->strDir);
      } else
      {
         sprintf(sUnit, "#%.*s", SIZ_S_UNITNO, pXAdr->Unit);
         sprintf(sAddr1, "%d%.3s %.31s #%.6s", iStrNo, pXAdr->strSub, pXAdr->strDir, pXAdr->Unit);
      }
      blankRem(sAddr1);

      memcpy(pOutbuf+OFF_S_UNITNO, sUnit, SIZ_S_UNITNO);
      // Update S_ADDR_D
      memset(pOutbuf+OFF_S_ADDR_D, ' ', SIZ_S_ADDR_D);
      vmemcpy(pOutbuf+OFF_S_ADDR_D, sAddr1, SIZ_S_ADDR_D);

      lUnitMatch++;
   }

   // Get next Char rec
   pRec = fgets(acRec, 1024, fdGis);

   return 0;
}

/********************************** Sac_Load_Roll *****************************
 *
 *
 ******************************************************************************/

int Sac_Load_Roll(int iSkip)
{
   char     *pTmp, acRec[MAX_RECSIZE], acBuf[MAX_RECSIZE], acRollRec[MAX_RECSIZE], acGisFile[_MAX_PATH],
            acRawFile[_MAX_PATH], acOutFile[_MAX_PATH], acLandFile[_MAX_PATH], acTmp[256];

   HANDLE   fhIn, fhOut;

   int      iRet, iTmp, iRollUpd=0, iNewRec=0, iRetiredRec=0;
   DWORD    nBytesRead;
   DWORD    nBytesWritten;
   BOOL     bRet, bEof;
   long     lRet=0, lCnt=0;

   sprintf(acRawFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "S01");
   sprintf(acOutFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "R01");

   // Rename files for processing
   if (_access(acRawFile, 0))
   {
      if (!_access(acOutFile, 0))
         rename(acOutFile, acRawFile);
      else
      {
         LogMsg("***** Missing input file %s.  Please recheck!", acRawFile);
         return -1;
      }
   }

   // Open Roll file
   LogMsg("Open Roll file %s", acRollFile);
   fdRoll = fopen(acRollFile, "r");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", acRollFile);
      return -2;
   }

   // Open Char file
   //if (!_access(acCharFile, 0))
   //{
   //   LogMsg("Open Char file %s", acCharFile);
   //   fdChar = fopen(acCharFile, "r");
   //   if (fdChar == NULL)
   //   {
   //      LogMsg("***** Error opening Char file: %s\n", acCharFile);
   //      return -2;
   //   }
   //}

   // Open Comm Char file
   if (!_access(acCChrFile, 0))
   {
      LogMsg("Open Char file %s", acCChrFile);
      fdCChr = fopen(acCChrFile, "r");
      if (fdCChr == NULL)
      {
         LogMsg("***** Error opening Char file: %s\n", acCChrFile);
         return -2;
      }
   }

   // Open CityZip file
   iRet = initSitus(acIniFile, myCounty.acCntyCode);

   // Load Zip2City file
   iRet = loadZip2City(acIniFile, myCounty.acCntyCode);

   // Open Land file
   GetIniString(myCounty.acCntyCode, "LandFile", "", acLandFile, _MAX_PATH, acIniFile);
   if (!_access(acLandFile, 0))
   {
      LogMsg("Open Landsize file %s", acLandFile);
      fdLand = fopen(acLandFile, "r");
      if (fdLand == NULL)
      {
         LogMsg("***** Error opening Landsize file: %s\n", acLandFile);
         return 2;
      }
      iLandSize = GetPrivateProfileInt(myCounty.acCntyCode, "LandRecSize", 32, acIniFile);
   }

   // Open AltApn file
   GetIniString(myCounty.acCntyCode, "AltApn", "", acTmp, _MAX_PATH, acIniFile);
   if (!_access(acTmp, 0))
   {
      LogMsg("Open AltApn file %s", acTmp);
      fdApn = fopen(acTmp, "r");
      if (fdApn == NULL)
      {
         LogMsg("***** Error opening AltApn file: %s\n", acTmp);
         return 2;
      }
   } else
      LogMsg("*** AltApn file is not available (%s).  Skip update AltApn.", acTmp);

   // Open GIS file
   sprintf(acGisFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "GIS");
   if (!_access(acGisFile, 0))
   {
      LogMsg("Open situs (GIS) file %s", acGisFile);
      fdGis = fopen(acGisFile, "r");
      if (fdGis == NULL)
      {
         LogMsg("***** Error opening Landsize file: %s\n", acGisFile);
         return 2;
      }
   } else
      fdGis = NULL;

   // Open Input file
   LogMsg("Open input file %s", acRawFile);
   fhIn = CreateFile(acRawFile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhIn == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening raw input file: %s\n", acRawFile);
      return -3;
   }

   // Open Output file
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error creating output file: %s\n", acOutFile);
      return -4;
   }

   // Get first RollRec
   pTmp = fgets((char *)&acRollRec[0], MAX_RECSIZE, fdRoll);
   bEof = (pTmp ? false:true);

   // Copy skip record
   memset(acBuf, ' ', iRecLen);
   while (iSkip-- > 0)
   {
      ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);
      // Ignore record start with 99999999
      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
   }

   iNoMatch=iBadCity=iBadSuffix=0;

   // Merge loop
   while (!bEof)
   {
      bRet = ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);

      // Check for EOF
      if (!bRet)
      {
         LogMsg("***** Error reading input file %s (%f)", acRawFile, GetLastError());
         break;
      }

      // EOF ?
      if (iRecLen != nBytesRead)
         break;

      NextRollRec:

      // Clear old sales
      if (bClearSales)
         ClearOldSale(acBuf);

#ifdef _DEBUG
      //if (!memcmp(acBuf, "0010011003", 10))
      //   iRet = 0;
#endif

      iTmp = memcmp(acBuf, acRollRec, iApnLen);
      if (!iTmp)
      {
         // Merge roll data
         Sac_MergeRoll(acBuf, acRollRec, iRollLen, UPDATE_R01);
         iRollUpd++;

         // Merge UnitNo
         if (fdGis)
            iRet = Sac_MergeUnitNo(acBuf);

         // Merge Char
         //if (fdChar)
         //   iRet = Sac_MergeChar(acBuf);

         // Merge LotSize
         if (fdLand)
            iRet = Sac_MergeLand(acBuf);

         // Merge CommChar
         if (fdCChr)
            iRet = Sac_MergeCChar(acBuf);

         // Merge AltApn
         if (fdApn)
         {
            iRet = Sac_MergeAltApn(acBuf);
            // If no match, copy APN to AltApn
            if (iRet)
               memcpy(&acBuf[OFF_ALT_APN], &acBuf[OFF_APN_S], iApnLen);
         } else
            memcpy(&acBuf[OFF_ALT_APN], &acBuf[OFF_APN_S], iApnLen);

         // Read next roll record
         pTmp = fgets(acRollRec, MAX_RECSIZE, fdRoll);

         if (!pTmp)
            bEof = true;    // Signal to stop
      } else if (iTmp > 0)       // Roll not match, new roll record?
      {
         if (bDebug)
            LogMsg0("*** New roll record : %.*s (%d) ***", iApnLen, acRollRec, lCnt);

         // Create new R01 record
         Sac_MergeRoll(acRec, acRollRec, iRollLen, CREATE_R01);
         iNewRec++;

         // Merge UnitNo
         if (fdGis)
            iRet = Sac_MergeUnitNo(acRec);

         // Merge Char
         //if (fdChar)
         //   iRet = Sac_MergeChar(acRec);

         // Merge LotSize
         if (fdLand)
            iRet = Sac_MergeLand(acRec);

         // Merge Comm Char
         if (fdCChr)
            iRet = Sac_MergeCChar(acRec);

         // Merge AltApn
         if (fdApn)
         {
            iRet = Sac_MergeAltApn(acRec);
            if (iRet)
               memcpy(&acRec[OFF_ALT_APN], &acRec[OFF_APN_S], iApnLen);
         } else
            memcpy(&acRec[OFF_ALT_APN], &acRec[OFF_APN_S], iApnLen);

         // Save last recording date
         iRet = atoin((char *)&acRec[OFF_TRANSFER_DT], 8);
         if (iRet > lLastRecDate && iRet < lToday)
            lLastRecDate = iRet;

         bRet = WriteFile(fhOut, acRec, iRecLen, &nBytesWritten, NULL);
         lCnt++;

         // Get next roll record
         pTmp = fgets(acRollRec, MAX_RECSIZE, fdRoll);

         if (!pTmp)
            bEof = true;    // Signal to stop
         else
            goto NextRollRec;
      } else if (acBuf[OFF_UNS_FLG] != 'Y') // Check for unsecured rec
      {
         // Record may be retired
         if (bDebug)
            LogMsg0("*** Roll not match (retired record?) : R01->%.*s < Roll->%.*s (%d) ***", iApnLen, acBuf, iApnLen, acRollRec, lCnt);
         iRetiredRec++;
         continue;
      }

      // Save last recording date
      iRet = atoin((char *)&acBuf[OFF_TRANSFER_DT], 8);
      if (iRet > lLastRecDate && iRet < lToday)
         lLastRecDate = iRet;

      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);

      if (!bRet)
      {
         LogMsg("***** Error occurs: %d\n", GetLastError());
         lRet = WRITE_ERR;
         break;
      }
   }

   // Do the rest of the file
   while (!bEof)
   {
      if (bDebug)
         LogMsg0("*** New roll record : %.*s (%d) ***", iApnLen, acRollRec, lCnt);

      // Create new R01 record
      Sac_MergeRoll(acRec, acRollRec, iRollLen, CREATE_R01);
      iNewRec++;

      // Merge UnitNo
      if (fdGis)
         iRet = Sac_MergeUnitNo(acBuf);

      // Merge Char
      //if (fdChar)
      //   iRet = Sac_MergeChar(acRec);

      // Merge LotSize
      if (fdLand)
         iRet = Sac_MergeLand(acRec);

      // Merge Comm Char
      if (fdCChr)
         iRet = Sac_MergeCChar(acRec);

      // Merge AltApn
      if (fdApn)
      {
         iRet = Sac_MergeAltApn(acRec);
         if (iRet)
            memcpy(&acRec[OFF_ALT_APN], &acRec[OFF_APN_S], iApnLen);
      } else
         memcpy(&acRec[OFF_ALT_APN], &acRec[OFF_APN_S], iApnLen);

      // Save last recording date
      iRet = atoin((char *)&acRec[OFF_TRANSFER_DT], 8);
      if (iRet > lLastRecDate && iRet < lToday)
         lLastRecDate = iRet;

      bRet = WriteFile(fhOut, acRec, iRecLen, &nBytesWritten, NULL);
      lCnt++;

      // Get next roll record
      pTmp = fgets(acRollRec, MAX_RECSIZE, fdRoll);

      if (!pTmp)
         bEof = true;    // Signal to stop
   }

   // Do the rest of the file
   while (nBytesRead>0 && ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL))
   {
      if (!nBytesRead)
         break;

      if (bDebug)
         LogMsg("*** No match R01= %.*s Roll=%.*s", iApnLen, acBuf, iApnLen, acRollRec);

      iRet = formatApn(acBuf, acTmp, &myCounty);
      memcpy((char *)&acBuf[OFF_APN_D], acTmp, iRet);
      iRet = formatMapLink(acBuf, acTmp, &myCounty);
      memset((char *)&acTmp[iRet], ' ', SIZ_MAPLINK-iRet);
      memcpy((char *)&acBuf[OFF_MAPLINK], acTmp, SIZ_MAPLINK);
      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
      iNoMatch++;
      lCnt++;
   }

   // Close files
   if (fdRoll)
      fclose(fdRoll);
   //if (fdChar)
   //   fclose(fdChar);
   if (fdSale)
      fclose(fdSale);
   if (fdLand)
      fclose(fdLand);
   if (fdApn)
      fclose(fdApn);
   if (fdCChr)
      fclose(fdCChr);
   if (fdGis)
      fclose(fdGis);

   if (fhOut)
      CloseHandle(fhOut);
   if (fhIn)
      CloseHandle(fhIn);
   closeSitus();

   LogMsg("Total output records:       %u", lCnt);
   LogMsg("Total new records:          %u", iNewRec);
   LogMsg("Total retired records:      %u", iRetiredRec);
   LogMsg("Total bad-city records:     %u", iBadCity);
   LogMsg("Total bad-suffix records:   %u\n", iBadSuffix);
   
   LogMsg("Total Char matched:         %u", lCharMatch);
   //LogMsg("Total CChar matched:        %u", lCChrMatch);
   LogMsg("Total Land matched:         %u", lLandMatch);
   LogMsg("Total AltApn matched:       %u", lApnMatch);
   LogMsg("Total Unit matched:         %u\n", lUnitMatch);

   LogMsg("Total Char skipped:         %u", lCharSkip);
   //LogMsg("Total CChar skipped:        %u", lCChrSkip);
   LogMsg("Total Land skipped:         %u", lLandSkip);
   LogMsg("Total AltApn skipped:       %u\n", lApnSkip);

   LogMsg("Parcels with no StrNum:     %u", iNoStrNum);
   LogMsg("Use GIS city:               %u", lUseGis);
   LogMsg("Use Mail city:              %u\n", lUseMailCity);

   printf("\nTotal output records: %u\n", lCnt);

   lRecCnt = lCnt;
   return lRet;
}

/******************************* Sac_UpdateZoning ****************************
 *
 * Merge Zoning into 1900-byte record.  If zoning is too long, keep first one OFF_ZONE.
 * Copy the full text to PQZoning if it's empty (ZONE_X1, ZONE_X2, ZONE_X3, ZONE_X4)
 *
 *****************************************************************************/

void Sac_UpdateZoning(char *pOutbuf, char *pZoneCode, unsigned char cSep)
{
   char  *pNextZone;
   int   iTmp;

   iTmp = strlen(pZoneCode);
   if (iTmp > 0)
   {
      vmemcpy(pOutbuf+OFF_ZONE, pZoneCode, SIZ_ZONE, iTmp);

      // Copy to PQZoning if it is blank
      if (*(pOutbuf+OFF_ZONE_X1) == ' ')
      {
         vmemcpy(pOutbuf+OFF_ZONE_X1, pZoneCode, SIZ_ZONE_X1, iTmp);
         if (iTmp > SIZ_ZONE_X1)
         {
            pNextZone = pZoneCode+SIZ_ZONE_X1;
            iTmp = iTmp - SIZ_ZONE_X1;
            vmemcpy(pOutbuf+OFF_ZONE_X2, pNextZone, SIZ_ZONE_X2, iTmp);
            if (iTmp > SIZ_ZONE_X2)
            {
               pNextZone += SIZ_ZONE_X2;
               iTmp = iTmp - SIZ_ZONE_X2;
               vmemcpy(pOutbuf+OFF_ZONE_X3, pNextZone, SIZ_ZONE_X3, iTmp);
               if (iTmp > SIZ_ZONE_X3)
               {
                  vmemcpy(pOutbuf+OFF_ZONE_X4, pNextZone+SIZ_ZONE_X3, SIZ_ZONE_X4);
#ifdef _DEBUG
                  LogMsg0("APN=%.14s Zoning=\"%s\"", pOutbuf, pZoneCode);
#endif
               }
            }
         }
      }
   }
}

/******************************** Sac_MergeRoll2 *****************************
 *
 * Return 0 if successful, < 0 if error
 *        1 retired record, not use
 *
 *****************************************************************************/

int Sac_MergeRoll2(char *pOutbuf, char *pRollRec, int iLen, int iFlag)
{
   char     acTmp[256], acTmp1[256], *pTmp;
   long     lTmp;
   double   dTmp;
   int      iRet=0, iTmp;

   // Parse input record
   iRet = ParseStringIQ(pRollRec, '|', MAX_FLD_TOKEN, apTokens);
   if (iRet < P_LOAD_DATE)
   {
      LogMsg("***** Error: bad input record for APN=%s", apTokens[iApnFld]);
      return -1;
   }

#ifdef _DEBUG
   //if (!memcmp(apTokens[PARCEL_NUMBER], "0040370046", 10))
   //{
   //   iTmp = 0;
   //}
#endif

   if (iFlag & CREATE_R01)
   {
      // Clear output buffer
      memset(pOutbuf, ' ', iRecLen);

      // Start copying data
      memcpy(pOutbuf, apTokens[PARCEL_NUMBER], strlen(apTokens[PARCEL_NUMBER]));
      pTmp = dateConversion(apTokens[P_OBSOLETE_DATE], acTmp, 4000);
      lTmp = atol(acTmp);
      if (lTmp > lToday)
         *(pOutbuf+OFF_STATUS) = 'A';
      else
         //*(pOutbuf+OFF_STATUS) = 'I';
         return 1;

      // Format APN
      iRet = formatApn(apTokens[PARCEL_NUMBER], acTmp, &myCounty);
      memcpy(pOutbuf+OFF_APN_D, acTmp, iRet);


      // Create MapLink and output new record
      iRet = formatMapLink(apTokens[PARCEL_NUMBER], acTmp, &myCounty);
      memcpy(pOutbuf+OFF_MAPLINK, acTmp, iRet);

      // Create index map link
      if (getIndexPage(acTmp, acTmp1, &myCounty))
         memcpy(pOutbuf+OFF_IMAPLINK, acTmp1, iRet);

      // County code
      memcpy(pOutbuf+OFF_CO_NUM, "34SAC", 5);

      // Year assessed
      memcpy(pOutbuf+OFF_YR_ASSD, myCounty.acYearAssd, 4);
   }

   // TRA
   iTmp = atol(apTokens[P_PARCEL_TRA]);
   sprintf(acTmp, "%.6d", iTmp);
   memcpy(pOutbuf+OFF_TRA, acTmp, 6);

   // Legal
   if (*apTokens[P_SHORT_LEGAL] > ' ')
   {
      replStr(apTokens[P_SHORT_LEGAL], ":: ", " ");
      if (pTmp = strstr(apTokens[P_SHORT_LEGAL], "Name: "))
         updateLegal(pOutbuf, pTmp+6);
      else
         updateLegal(pOutbuf, apTokens[P_SHORT_LEGAL]);
   } else if (*apTokens[P_LONG_LEGAL] > ' ')
      updateLegal(pOutbuf, apTokens[P_LONG_LEGAL]);

   // Zoning
   if (*apTokens[P_PARCEL_ZONE_CODE] > ' ')
      Sac_UpdateZoning(pOutbuf, apTokens[P_PARCEL_ZONE_CODE], ' ');

   // Neighborhood code
   if (*apTokens[P_PARCEL_NBH_CODE] > ' ')
      vmemcpy(pOutbuf+OFF_NBH_CODE, apTokens[P_PARCEL_NBH_CODE], SIZ_NBH_CODE);

   // UseCode
   strcpy(acTmp, apTokens[P_PARCEL_USE_CODE]);
   acTmp[SIZ_USE_CO] = 0;

   // Standard UseCode
   if (acTmp[0] > ' ')
   {
      _strupr(acTmp);
      iTmp = strlen(acTmp);
      memcpy(pOutbuf+OFF_USE_CO, acTmp, iTmp);
      updateStdUse(pOutbuf+OFF_USE_STD, acTmp, iTmp, pOutbuf);
   } else
      memcpy(pOutbuf+OFF_USE_STD, USE_UNASGN, SIZ_USE_STD);

   // Acres
   lTmp = atol(apTokens[P_PARCEL_LOT_SQFT]);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*u", SIZ_LOT_SQFT, lTmp);
      memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);

      // Format Acres
      dTmp = (double)lTmp / SQFT_FACTOR_1000;
      sprintf(acTmp, "%*u", SIZ_LOT_ACRES, (long)dTmp);
      memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);
   }

   return 0;
}

/**************************** Sac_Load_Roll2() ******************************
 *
 * Loading update roll using new delimited data format
 *
 ****************************************************************************/

int Sac_Load_Roll2(int iFirstRec /* 1=create header rec */)
{
   char     *pTmp, acBuf[MAX_RECSIZE], acRec[MAX_RECSIZE*2], acTmp[256];
   char     acOutFile[_MAX_PATH], acLienFile[_MAX_PATH], acTmpFile[_MAX_PATH], acLandFile[_MAX_PATH],
            acSitusFile[_MAX_PATH], acMailingFile[_MAX_PATH], acOwnerFile[_MAX_PATH], acGisFile[_MAX_PATH];

   HANDLE   fhOut;

   int      iRet, iRollUpd=0, iRetiredRec=0, iNoOwner=0;
   DWORD    nBytesWritten;
   BOOL     bRet, bEof;
   long     lRet=0, lCnt=0;

   LogMsg0("Loading Roll update file %s", acRollFile);

   // Open roll file
   LogMsg("Open Roll file %s", acRollFile);
   fdRoll = fopen(acRollFile, "r");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", acRollFile);
      return -2;
   }  

   // Open Char file
   LogMsg("Open Char file %s", acCChrFile);
   fdCChr = fopen(acCChrFile, "r");
   if (fdCChr == NULL)
   {
      LogMsg("***** Error opening Char file: %s\n", acCChrFile);
      return -2;
   }

   // Open Situs file
   iRet = GetIniString(myCounty.acCntyCode, "SitusFile", "", acSitusFile, _MAX_PATH, acIniFile);
   if (iRet < 10)
   {
      LogMsg("***** Error Situs file not defined in INI file");
      return -3;
   }
   LogMsg("Open Situs file %s", acSitusFile);
   sprintf(acTmpFile, "%s\\%s\\%s_Situs.srt", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   lRet = sortFile(acSitusFile, acTmpFile, "S(#2,C,A) DEL(124) ");
   fdSitus = fopen(acTmpFile, "r");
   if (fdSitus == NULL)
   {
      LogMsg("***** Error opening Situs file: %s\n", acTmpFile);
      return -2;
   }

   // Open Mailing file
   iRet = GetIniString(myCounty.acCntyCode, "MailFile", "", acMailingFile, _MAX_PATH, acIniFile);
   if (iRet < 10)
   {
      LogMsg("***** Error Mailing file not defined in INI file");
      return -3;
   }
   LogMsg("Open Mailing file %s", acMailingFile);
   sprintf(acTmpFile, "%s\\%s\\%s_Mail.srt", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   lRet = sortFile(acMailingFile, acTmpFile, "S(#2,C,A) DEL(124) ");
   fdMail = fopen(acTmpFile, "r");
   if (fdMail == NULL)
   {
      LogMsg("***** Error opening Mailing file: %s\n", acTmpFile);
      return -2;
   }

   // Open Owner file - Sort by APN, Primary flag
   iRet = GetIniString(myCounty.acCntyCode, "OwnerFile", "", acOwnerFile, _MAX_PATH, acIniFile);
   if (iRet < 10)
   {
      LogMsg("***** Error Owner file not defined in INI file");
      return -3;
   }
   LogMsg("Open Owner file %s", acOwnerFile);
   sprintf(acTmpFile, "%s\\%s\\%s_Owner.srt", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   lRet = sortFile(acOwnerFile, acTmpFile, "S(#2,C,A,#12,C,D,#10,C,A) DEL(124) ");
   fdOwner = fopen(acTmpFile, "r");
   if (fdOwner == NULL)
   {
      LogMsg("***** Error opening Owner file: %s\n", acTmpFile);
      return -2;
   }

   // Open Land file
   GetIniString(myCounty.acCntyCode, "LandFile", "", acLandFile, _MAX_PATH, acIniFile);
   if (!_access(acLandFile, 0))
   {
      LogMsg("Open Landsize file %s", acLandFile);
      fdLand = fopen(acLandFile, "r");
      if (fdLand == NULL)
      {
         LogMsg("***** Error opening Landsize file: %s\n", acLandFile);
         return 2;
      }
      iLandSize = GetPrivateProfileInt(myCounty.acCntyCode, "LandRecSize", 32, acIniFile);
   }

   // Open AltApn file
   GetIniString(myCounty.acCntyCode, "AltApn", "", acTmp, _MAX_PATH, acIniFile);
   if (!_access(acTmp, 0))
   {
      LogMsg("Open AltApn file %s", acTmp);
      fdApn = fopen(acTmp, "r");
      if (fdApn == NULL)
      {
         LogMsg("***** Error opening AltApn file: %s\n", acTmp);
         return 2;
      }
   } else
      LogMsg("*** AltApn file is not available (%s).  Skip update AltApn.", acTmp);

   // Open GIS file to get Unit#
   sprintf(acGisFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "GIS");
   if (!_access(acGisFile, 0))
   {
      LogMsg("Open situs (GIS) file %s (for Unit#)", acGisFile);
      fdGis = fopen(acGisFile, "r");
      if (fdGis == NULL)
      {
         LogMsg("***** Error opening GIS file: %s\n", acGisFile);
         return 2;
      }
   } else
      fdGis = NULL;

   // Open Lien file
   sprintf(acLienFile, acLienTmpl, myCounty.acCntyCode, myCounty.acCntyCode);
   LogMsg("Open Lien extract file %s", acLienFile);
   FILE *fdLienExt = fopen(acLienFile, "r");
   if (fdLienExt == NULL)
   {
      LogMsg("***** Error opening Lien extract file: %s\n", acLienFile);
      return -2;
   }

   // Open Output file
   sprintf(acOutFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "R01");
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening output file: %s\n", acOutFile);
      return -4;
   }

   // Output first header record
   if (iFirstRec > 0)
   {
      memset(acBuf, '9', iRecLen);
      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
   }

   // Skip header record
   //for (iRet = 0; iRet < iHdrRows; iRet++)
   //   pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdRoll);

   // Get 1st rec
   pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdRoll);
   bEof = (pTmp ? false:true);

   // Init variables
   iNoMatch=iBadCity=iBadSuffix=0;

   // Merge loop 
   while (!feof(fdRoll))
   {
#ifdef _DEBUG
      //if (!memcmp(acBuf, "23002220450000", 12))
      //   iRet = 0;
#endif

      // Create new R01 record
      iRet = Sac_MergeRoll2(acBuf, acRec, 0, CREATE_R01);

      if (!iRet)
      {
         // Merge LDR values
         if (fdLienExt)
            iRet = PQ_MergeLien(acBuf, fdLienExt, 0, true);

         // Merge Owner
         if (fdOwner)
         {
            iRet = Sac_MergeOwnerNames(acBuf);
            if (iRet == -1)
               iNoOwner++;
         }

         // Merge Situs
         if (fdSitus)
            iRet = Sac_MergeSitus(acBuf);

         // Merge Mailing
         if (fdMail)
            iRet = Sac_MergeMailing(acBuf);

         // Merge Char 
         if (fdCChr)
            iRet = Sac_MergeCChar(acBuf);

         // Merge UnitNo
         if (fdGis)
            iRet = Sac_MergeUnitNo(acBuf);

         // Merge LotSize
         if (fdLand)
            iRet = Sac_MergeLand(acBuf);

         // Merge AltApn
         if (fdApn)
         {
            iRet = Sac_MergeAltApn(acBuf);
            // If no match, copy APN to AltApn
            if (iRet)
               memcpy(&acBuf[OFF_ALT_APN], &acBuf[OFF_APN_S], iApnLen);
         } else
            memcpy(&acBuf[OFF_ALT_APN], &acBuf[OFF_APN_S], iApnLen);

         //iRet = replChar(acBuf, 0, ' ', iRecLen);
         //if (iRet)
         //   LogMsg("*** Null char found at pos %d on APN=%.12s", iRet, acBuf);

         // Save last recording date
         lRet = atoin((char *)&acBuf[OFF_TRANSFER_DT], SIZ_TRANSFER_DT);
         if (lRet > lLastRecDate && lRet < lToday)
            lLastRecDate = lRet;
         
         //if (acBuf[OFF_NAME1] > ' ')
         //{
            bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
            if (!bRet)
            {
               LogMsg("Error writing to output file at record %d\n", lCnt);
               lRet = WRITE_ERR;
               break;
            }
            iRollUpd++;
         //} else
            //iRetiredRec++;
      } else
      {
         if (!(pTmp=fgets(acRec, MAX_RECSIZE*2, fdRoll)) )
            break;
         lCnt++;
         continue;
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);

      // Get next roll record
      pTmp = fgets(acRec, MAX_RECSIZE*2, fdRoll);
      if (!pTmp || acRec[0] > '9')
         break;
   }

   // Close files
   if (fdLienExt)
      fclose(fdLienExt);
   if (fdRoll)
      fclose(fdRoll);
   if (fdCChr)
      fclose(fdCChr);
   if (fdSitus)
      fclose(fdSitus);
   if (fdMail)
      fclose(fdMail);
   if (fdOwner)
      fclose(fdOwner);
   if (fdLand)
      fclose(fdLand);
   if (fdApn)
      fclose(fdApn);
   if (fdGis)
      fclose(fdGis);
   if (fhOut)
      CloseHandle(fhOut);

   LogMsg("Total records processed:    %u", lCnt);
   LogMsg("Total output records:       %u", iRollUpd);
   //LogMsg("Total retired records:      %u", iRetiredRec);
   LogMsg("Total bad-city records:     %u", iBadCity);
   LogMsg("Total bad-suffix records:   %u", iBadSuffix);
   LogMsg("Use GIS city:               %u", lUseGis);
   LogMsg("Use Mail city:              %u\n", lUseMailCity);

   LogMsg("Parcels with no StrNum:     %u", iNoStrNum);
   LogMsg("Parcels with no owner:      %u\n", iNoOwner);

   LogMsg("Total Unit matched:         %u", lUnitMatch);
   LogMsg("Total Char matched:         %u", lCharMatch);
   LogMsg("Total Char skipped:         %u", lCharSkip);
   LogMsg("Total Land matched:         %u", lLandMatch);
   LogMsg("Total Land skipped:         %u", lLandSkip);
   LogMsg("Total AltApn matched:       %u", lApnMatch);
   LogMsg("Total AltApn skipped:       %u", lApnSkip);
   LogMsg("Total owner matched:        %u", lOwnerMatch);
   LogMsg("Total Situs matched:        %u", lSitusMatch);
   LogMsg("Total Situs skipped:        %u", lSitusSkip);
   LogMsg("Total Mailing matched:      %u", lMailMatch);
   LogMsg("Total Mailing skipped:      %u\n", lMailSkip);

   LogMsg("Last recording date:        %u", lLastRecDate);
   printf("\nTotal output records: %u", lCnt);
   lRecCnt = iRollUpd;
   return 0;
}

/********************************* Sac_Load_LDR ****************************
 *
 * This version using gae7000_rollpubl.txt as input
 *
 ****************************************************************************/

int Sac_Load_LDR(int iFirstRec /* 1=create header rec */)
{
   char     *pTmp, acBuf[MAX_RECSIZE], acRollRec[MAX_RECSIZE], acGisFile[_MAX_PATH],
            acOutFile[_MAX_PATH], acLandFile[_MAX_PATH], acLValFile[_MAX_PATH];

   HANDLE   fhOut;

   int      iRet;
   DWORD    nBytesWritten;
   BOOL     bRet;
   long     lRet=0, lCnt=0;

   // Open roll file
   LogMsg("Open Roll file %s", acRollFile);
   fdRoll = fopen(acRollFile, "r");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", acRollFile);
      return -2;
   }

   // Open Equalized file
   // Set EQRollFile in INI file to create special LDR version
   GetIniString(myCounty.acCntyCode, "EQRollFile", "", acLValFile, _MAX_PATH, acIniFile);
   if (!_access(acLValFile, 0))
   {
      sprintf(acBuf, "%s\\%s\\%s_EQRoll.srt", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
      lRet = sortFile(acLValFile, acBuf, "S(#1,C,A) DEL(124) ");

      LogMsg("Open Equalized file %s", acBuf);
      fdEq = fopen(acBuf, "r");
      if (fdEq == NULL)
      {
         LogMsg("***** Error opening Equalized file: %s\n", acBuf);
         return -2;
      }
   } else
      fdEq = NULL;

   // Open LDR value file
   GetIniString(myCounty.acCntyCode, "LValFile", "", acLValFile, _MAX_PATH, acIniFile);
   if (!fdEq && !_access(acLValFile, 0))
   {
      LogMsg("Open LDR values file %s", acLValFile);
      fdValue = fopen(acLValFile, "r");
      if (fdValue == NULL)
      {
         LogMsg("***** Error opening Landsize file: %s\n", acLValFile);
         return -2;
      }
      iLValSize = GetPrivateProfileInt(myCounty.acCntyCode, "LValRecSize", 299, acIniFile);
   } else
      fdValue = NULL;

   // Open Char file
   if (!_access(acCChrFile, 0))
   {
      LogMsg("Open Char file %s", acCChrFile);
      fdCChr = fopen(acCChrFile, "r");
      if (fdCChr == NULL)
      {
         LogMsg("***** Error opening Char file: %s\n", acCChrFile);
         return -2;
      }
   }

   // Open CityZip file
   if ((iRet=initSitus(acIniFile, myCounty.acCntyCode)) < 0)
      return iRet;

   // Load Zip2City file
   iRet = loadZip2City(acIniFile, myCounty.acCntyCode);

   // Open Land file
   GetIniString(myCounty.acCntyCode, "LandFile", "", acLandFile, _MAX_PATH, acIniFile);
   if (!_access(acLandFile, 0))
   {
      LogMsg("Open Landsize file %s", acLandFile);
      fdLand = fopen(acLandFile, "r");
      if (fdLand == NULL)
      {
         LogMsg("***** Error opening Landsize file: %s\n", acLandFile);
         return -2;
      }
      iLandSize = GetPrivateProfileInt(myCounty.acCntyCode, "LandRecSize", 32, acIniFile);
   }

   // Open AltApn file
   GetIniString(myCounty.acCntyCode, "AltApn", "", acOutFile, _MAX_PATH, acIniFile);
   if (!_access(acOutFile, 0))
   {
      LogMsg("Open AltApn file %s", acOutFile);
      fdApn = fopen(acOutFile, "r");
      if (fdApn == NULL)
      {
         LogMsg("***** Error opening AltApn file: %s\n", acOutFile);
         return 2;
      }
   } else
      LogMsg("*** AltApn file is not available (%s).  Skip update AltApn.");

   // Open GIS file
   sprintf(acGisFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "GIS");
   if (!_access(acGisFile, 0))
   {
      LogMsg("Open Landsize file %s", acGisFile);
      fdGis = fopen(acGisFile, "r");
      if (fdGis == NULL)
      {
         LogMsg("***** Error opening Landsize file: %s\n", acGisFile);
         return 2;
      }
   } else
      fdGis = NULL;

   // Open Output file
   sprintf(acOutFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "R01");
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error creating output file: %s\n", acOutFile);
      return -4;
   }

   // Output first header record
   if (iFirstRec > 0)
   {
      memset(acBuf, '9', iRecLen);
      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
   }

   // Init variables
   iNoMatch=iBadCity=iBadSuffix=0;
   lApnSkip = 0;
   lApnMatch= 0;

   // Merge loop
   while (!feof(fdRoll))
   {
      pTmp = fgets(acRollRec, MAX_RECSIZE, fdRoll);
      if (!pTmp)
         break;

      lLDRRecCount++;
      // Create new R01 record
      Sac_MergeRoll(acBuf, acRollRec, iRollLen, CREATE_R01|CREATE_LIEN);

      // Merge UnitNo
      if (fdGis)
         iRet = Sac_MergeUnitNo(acBuf);

      // Merge LDR values
      //if (fdValue)
      //   iRet = Sac_MergeLVal(acBuf);

      // Merge Equalized record
      if (fdEq)
         iRet = Sac_MergeELVal(acBuf);
      else if (fdValue)
         iRet = Sac_MergeLVal(acBuf);

      // Merge Taxes
      if (fdLand)
         iRet = Sac_MergeLand(acBuf);

      // Merge CommChar
      if (fdCChr)
         iRet = Sac_MergeCChar(acBuf);

      // Merge AltApn
      if (fdApn)
      {
         iRet = Sac_MergeAltApn(acBuf);
         // If no match, copy APN to AltApn
         if (iRet)
            memcpy(&acBuf[OFF_ALT_APN], &acBuf[OFF_APN_S], iApnLen);
      } else
         memcpy(&acBuf[OFF_ALT_APN], &acBuf[OFF_APN_S], iApnLen);

      // Save last recording date
      iRet = atoin((char *)&acBuf[OFF_TRANSFER_DT], 8);
      if (iRet > lLastRecDate && iRet < lToday)
         lLastRecDate = iRet;

      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);

      if (!bRet)
      {
         LogMsg("Error writing to output file at record %d\n", lCnt);
         lRet = WRITE_ERR;
         break;
      }
   }

   // Close files
   if (fdRoll)
      fclose(fdRoll);
   if (fdEq)
      fclose(fdEq);
   if (fdCChr)
      fclose(fdCChr);
   if (fdLand)
      fclose(fdLand);
   if (fdApn)
      fclose(fdApn);
   if (fdValue)
      fclose(fdValue);
   if (fdGis)
      fclose(fdGis);

   if (fhOut)
      CloseHandle(fhOut);

   LogMsg("Total input records:        %u", lLDRRecCount);
   LogMsg("Total output records:       %u", lCnt);
   LogMsg("Total bad-city records:     %u", iBadCity);
   LogMsg("Total bad-suffix records:   %u\n", iBadSuffix);

   LogMsg("Total Char matched:         %u", lCharMatch);
   LogMsg("Total Land matched:         %u", lLandMatch);
   LogMsg("Total AltApn matched:       %u", lApnMatch);
   LogMsg("Total Unit matched:         %u\n", lUnitMatch);

   LogMsg("Total Char skipped:         %u", lCharSkip);
   LogMsg("Total Land skipped:         %u", lLandSkip);
   LogMsg("Total AltApn skipped:       %u\n", lApnSkip);

   LogMsg("Total LDR value matched:    %u", lLValMatch);
   LogMsg("Total LDR value skipped:    %u\n", lLValSkip);

   printf("\nTotal output records: %u\n", lCnt);

   lRecCnt = lCnt;
   return 0;
}

/********************************* Sac_Load_LDR ****************************
 *
 * This function use AIMSAB_PUBLIC.TXT as input.  Some records won't have owner name.
 *
 ****************************************************************************/

int Sac_Load_LDR1(int iFirstRec /* 1=create header rec */)
{
   char     *pTmp, acBuf[MAX_RECSIZE], acRollRec[MAX_RECSIZE];
   char     acOutFile[_MAX_PATH], acLandFile[_MAX_PATH], acLValFile[_MAX_PATH];

   HANDLE   fhOut;

   int      iRet;
   DWORD    nBytesWritten;
   BOOL     bRet;
   long     lRet=0, lCnt=0;

   LogMsg("Loading SAC LDR using AIMSAB_PUBLIC.TXT");

   // Open LDR value file
   GetIniString(myCounty.acCntyCode, "LValFile", "", acLValFile, _MAX_PATH, acIniFile);
   LogMsg("Open LDR values file %s", acLValFile);
   fdRoll = fopen(acLValFile, "r");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening LDR file: %s\n", acLValFile);
      return -2;
   }
   iRollLen = GetPrivateProfileInt(myCounty.acCntyCode, "LValRecSize", 299, acIniFile);

   // Open Char file
   if (!_access(acCChrFile, 0))
   {
      LogMsg("Open Char file %s", acCChrFile);
      fdCChr = fopen(acCChrFile, "r");
      if (fdCChr == NULL)
      {
         LogMsg("***** Error opening Char file: %s\n", acCChrFile);
         return -2;
      }
   }

   // Open Land file
   GetIniString(myCounty.acCntyCode, "LandFile", "", acLandFile, _MAX_PATH, acIniFile);
   if (!_access(acLandFile, 0))
   {
      LogMsg("Open Landsize file %s", acLandFile);
      fdLand = fopen(acLandFile, "r");
      if (fdLand == NULL)
      {
         LogMsg("***** Error opening Landsize file: %s\n", acLandFile);
         return -2;
      }
      iLandSize = GetPrivateProfileInt(myCounty.acCntyCode, "LandRecSize", 32, acIniFile);
   }

   // Open AltApn file
   GetIniString(myCounty.acCntyCode, "AltApn", "", acOutFile, _MAX_PATH, acIniFile);
   if (!_access(acOutFile, 0))
   {
      LogMsg("Open AltApn file %s", acOutFile);
      fdApn = fopen(acOutFile, "r");
      if (fdApn == NULL)
      {
         LogMsg("***** Error opening AltApn file: %s\n", acOutFile);
         return 2;
      }
   } else
      LogMsg("*** AltApn file is not available (%s).  Skip update AltApn.");

   // Open Equalized file
   // Set EQRollFile in INI file to create special LDR version
   GetIniString(myCounty.acCntyCode, "EQRollFile", "", acLValFile, _MAX_PATH, acIniFile);
   if (!_access(acLValFile, 0))
   {
      sprintf(acBuf, "%s\\%s\\%s_EQRoll.srt", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
      lRet = sortFile(acLValFile, acBuf, "S(#1,C,A) DEL(124) ");

      LogMsg("Open Equalized file %s", acBuf);
      fdEq = fopen(acBuf, "r");
      if (fdEq == NULL)
      {
         LogMsg("***** Error opening Equalized file: %s\n", acBuf);
         return -2;
      }
   } else
      fdEq = NULL;

   // Open Output file
   sprintf(acOutFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "R01");
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error creating output file: %s\n", acOutFile);
      return -4;
   }

   // Output first header record
   if (iFirstRec > 0)
   {
      memset(acBuf, '9', iRecLen);
      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
   }

   // Init variables
   iNoMatch=iBadCity=iBadSuffix=0;
   lApnSkip = 0;
   lApnMatch= 0;

   // Merge loop
   while (!feof(fdRoll))
   {
      pTmp = fgets(acRollRec, MAX_RECSIZE, fdRoll);
      if (!pTmp)
         break;

      lLDRRecCount++;
      // Create new R01 record
      Sac_CreateRollRec(acBuf, acRollRec);

      // Merge Equalized record
      if (fdEq)
         iRet = Sac_MergeELVal(acBuf);

      // Merge Char
      //if (fdChar)
      //   iRet = Sac_MergeChar(acBuf);
      if (fdCChr)
         iRet = Sac_MergeCChar(acBuf);

      // Merge Taxes
      if (fdLand)
         iRet = Sac_MergeLand(acBuf);

      // Merge AltApn
      if (fdApn)
      {
         iRet = Sac_MergeAltApn(acBuf);
         // If no match, copy APN to AltApn
         if (iRet)
            memcpy(&acBuf[OFF_ALT_APN], &acBuf[OFF_APN_S], iApnLen);
      } else
         memcpy(&acBuf[OFF_ALT_APN], &acBuf[OFF_APN_S], iApnLen);

      // Save last recording date
      iRet = atoin((char *)&acBuf[OFF_SALE1_DT], 8);
      if (iRet > lLastRecDate && iRet < lToday)
         lLastRecDate = iRet;

      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);

      if (!bRet)
      {
         LogMsg("Error writing to output file at record %d\n", lCnt);
         lRet = WRITE_ERR;
         break;
      }
   }

   // Close files
   if (fdRoll)
      fclose(fdRoll);
   if (fdCChr)
      fclose(fdCChr);
   if (fdLand)
      fclose(fdLand);
   if (fdApn)
      fclose(fdApn);
   if (fdEq)
      fclose(fdEq);

   if (fhOut)
      CloseHandle(fhOut);

   LogMsg("Total input records:        %u", lLDRRecCount);
   LogMsg("Total output records:       %u", lCnt);
   LogMsg("Total bad-city records:     %u", iBadCity);
   LogMsg("Total bad-suffix records:   %u\n", iBadSuffix);

   LogMsg("Total Char matched:         %u", lCharMatch);
   LogMsg("Total Char skipped:         %u\n", lCharSkip);
   LogMsg("Total Land matched:         %u", lLandMatch);
   LogMsg("Total Land skipped:         %u\n", lLandSkip);
   LogMsg("Total AltApn matched:       %u", lApnMatch);
   LogMsg("Total AltApn skipped:       %u\n", lApnSkip);

   printf("\nTotal output records: %u\n", lCnt);

   lRecCnt = lCnt;
   return 0;
}

/******************************* Sac_LoadLand ********************************
 *
 * Format both Acres and Sqft.  If Acres > 10000, do not format Sqft.
 * similarly if Sqft < 500, do not format Acres.
 *
 * Return 0 if successful, >0 if error
 *
 *****************************************************************************/

int Sac_LoadLand(char *pLandFile)
{
   char     acOutFile[_MAX_PATH], acBuf[256];
   char     acTmp[256], *pTmp, *apTokens[8];

   FILE     *fdOut, *fdIn;

   int      iTmp;
   long     lCnt=0, lOutRecs=0, lAcres, lSqft, lLandSize=sizeof(SAC_LAND);
   CString  strFld;

   LogMsg0("Creating Land Size file for %s", myCounty.acCntyCode);

   // Open input file
   LogMsg("Open land size input file %s", pLandFile);
   fdIn = fopen(pLandFile, "r");
   if (fdIn == NULL)
   {
      LogMsg("*** Land file not available: %s\n", pLandFile);
      return -2;
   }
   // Drop header record
   pTmp = fgets(acTmp, 256, fdIn);


   // Open Output file
   strcpy(acOutFile, pLandFile);
   if (pTmp = strchr(acOutFile, '.'))
      strcpy(pTmp, ".DAT");
   else
      strcat(acOutFile, ".DAT");

   LogMsg("Open output file %s", acOutFile);
   fdOut = fopen(acOutFile, "w");
   if (fdOut == NULL)
   {
      printf("Error creating GrGr output file: %s\n", acOutFile);
      return -2;
   }

   // Loop through record set
   while (!feof(fdIn))
   {
      pTmp = fgets(acTmp, 256, fdIn);
      if (!pTmp)
         break;

      // Reset output record
      memset(acBuf, ' ', sizeof(SAC_LAND));

      // Parse input string
      iTmp = ParseString(acTmp, ';', 8, apTokens);

#ifdef _DEBUG
      //if (!memcmp(apTokens[1], "0020", 4) && !memcmp(apTokens[2], "009", 3))
      //   lAcres = 0;
#endif

      lAcres = astol(apTokens[4]);
      lSqft  = astol(apTokens[5]);
      if (!lSqft)
         lSqft = (long)(((double)lAcres/100.0)*SQFT_PER_ACRE);
      if (!lAcres)
         lAcres = (long)(((double)lSqft*ACRES_FACTOR)/SQFT_PER_ACRE);
      else
         lAcres *= 10;
      if (lSqft)
      {
         sprintf(acBuf, "%s%s%s%s%9d%9d", apTokens[0], apTokens[1], apTokens[2], apTokens[3], lAcres, lSqft);
         iTmp = fwrite(acBuf, 1, lLandSize, fdOut);
         lOutRecs++;
      }


      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   fclose(fdIn);
   fclose(fdOut);

   // Sort output file
   LogMsg("Total output records:    %u", lOutRecs);
   LogMsg("Total processed records: %u\n", lCnt);

   return iTmp;
}

/********************************* formatCSale *******************************
 *
 *****************************************************************************

void formatCSale(char *pFmtSale, char *pSaleRec)
{
   SAC_SALE *pSale = (SAC_SALE *)pSaleRec;
   SAC_CSAL *pHSale= (SAC_CSAL *)pFmtSale;

   char     acTmp[32];

   // Inititalize
   memset(pFmtSale, ' ', sizeof(SAC_CSAL));

   // APN
   memcpy(pHSale->Apn, pSale->Apn, sizeof(pHSale->Apn));

   // ActualSalePrice
   memcpy(pHSale->ActualSalePrice, pSale->SalePrice, sizeof(pHSale->ActualSalePrice));

   // RecDate
   if (pSale->RecDate[0] < '5')
      sprintf(acTmp, "20%.6s", pSale->RecDate);
   else
      sprintf(acTmp, "19%.6s", pSale->RecDate);
   memcpy(pHSale->RecDate, acTmp, sizeof(pHSale->RecDate));

   // RecPage
   memcpy(pHSale->RecPage, pSale->RecPage, sizeof(pHSale->RecPage));

   // ReliabilityCode
   memcpy(pHSale->ReliabilityCode, pSale->ReliabilityCode, sizeof(pHSale->ReliabilityCode));

   // SaleType
   memcpy(pHSale->SaleType, pSale->SaleType, sizeof(pHSale->SaleType));

   // AdjSalePrice
   memcpy(pHSale->AdjSalePrice, pSale->CashPrice, sizeof(pHSale->AdjSalePrice));

   // NumParcels
   memcpy(pHSale->NumParcels, pSale->NumParcels, sizeof(pHSale->NumParcels));

   // DeedDate
   //if (pSale->DeedDate[0] < '5')
   //   sprintf(acTmp, "20%.6s", pSale->DeedDate);
   //else
   //   sprintf(acTmp, "19%.6s", pSale->DeedDate);
   //memcpy(pHSale->DeedDate, acTmp, sizeof(pHSale->DeedDate));

   // PctTransfer
   memcpy(pHSale->PctTransfer, pSale->PctTransfer, sizeof(pHSale->PctTransfer));

   // DeedType
   memcpy(pHSale->DeedType, pSale->DeedType, sizeof(pHSale->DeedType));

   // StampAmt
   long lTmp = atoin(pSale->StampAmt, SSIZ_STAMP_AMOUNT);
   if (lTmp > 0)
   {
      double dTmp;
      dTmp = lTmp / 100;
      sprintf(acTmp, "%9.2f", dTmp);
      memcpy(pHSale->StampAmt, acTmp, 9);

      // SalePrice
      lTmp *= SALE_FACTOR_100;
      if (lTmp < 100)
         sprintf(acTmp, "%*u", SIZ_SALE1_AMT, lTmp);
      else
         sprintf(acTmp, "%*u00", SIZ_SALE1_AMT-2, lTmp/100);

      memcpy(pHSale->SalePrice, acTmp, SIZ_SALE1_AMT);
   }

   // StampCode
   pHSale->StampCode = pSale->StampCode[0];

   // Grantor
   memcpy(pHSale->Grantor, pSale->Grantor, sizeof(pHSale->Grantor));

   // Grantee
   memcpy(pHSale->Grantee, pSale->Grantee, sizeof(pHSale->Grantee));

   // S_Zip
   //memcpy(pHSale->S_Zip, pSale->S_Zip, sizeof(pHSale->S_Zip));
}

/****************************** Sac_UpdateSaleHist ***************************
 *
 * Update history sale.  This function adds new sale record to cumulative file.
 *
 * Return 0 if successful, >0 if error
 *
 *****************************************************************************

int Sac_UpdateSaleHist(void)
{
   SAC_SALE *pCurSale;
   SAC_CSAL *pHistSale, *pNewSale;
   char     acNewSale[512];
   char     *pTmp, acHSaleRec[512], acCSaleRec[1024], acSaleDate[32], acTmp[256];
   char     acOutFile[_MAX_PATH];

   HANDLE   fhIn, fhOut;

   int      iRet, iTmp, iUpdateSale=0, iNewSale=0;
   DWORD    nBytesRead;
   DWORD    nBytesWritten;
   BOOL     bRet, bEof;
   long     lCnt=0;

   // Check current sale file
   if (_access(acSaleFile, 0))
   {
      LogMsg("***** Missing current sale file %s", acSaleFile);
      return -1;
   }

   // Check cumulative sale file
   if (_access(acCSalFile, 0))
   {
      LogMsg("*** Missing cumulative file %s.", acCSalFile);
      return -1;
   }

   // Create tmp output file
   strcpy(acOutFile, acCSalFile);
   if (pTmp = strrchr(acOutFile, '.'))
      strcpy(pTmp, ".tmp");
   else
      strcat(acOutFile, ".tmp");

   // Open current sale
   LogMsg("Open current sale file %s", acSaleFile);
   fdCSale = fopen(acSaleFile, "r");
   if (fdCSale == NULL)
   {
      LogMsg("***** Error opening current sale file: %s\n", acSaleFile);
      return 2;
   }

   // Open Input file - cumulative sale file
   LogMsg("Open cumulative sale file %s", acCSalFile);
   fhIn = CreateFile(acCSalFile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhIn == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error open current sale file: %s", acCSalFile);
      return 3;
   }

   // Open Output file
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error open output file: %s", acOutFile);
      return 4;
   }

   // Read first history sale
   bRet = ReadFile(fhIn, acHSaleRec, iCSalLen, &nBytesRead, NULL);

   pCurSale  = (SAC_SALE *)&acCSaleRec[0];
   pHistSale = (SAC_CSAL *)&acHSaleRec[0];
   pNewSale  = (SAC_CSAL *)&acNewSale[0];
   bEof = false;
   acSaleDate[0] = 0;
   lLastRecDate = 0;

   // Merge loop
   while (!bEof)
   {
      // Get current sale
      pTmp = fgets(acCSaleRec, 1024, fdCSale);
      if (!pTmp)
         break;

      NextSaleRec:
      iTmp = memcmp(acCSaleRec, acHSaleRec, iApnLen);
      if (!iTmp)
      {
         // Check for duplicate sale
         if (!memcmp((char *)&acSaleDate[2], pCurSale->RecDate, 10))
         {
            LogMsg0("Dup sale: %.14s - %s", pCurSale->Apn, acSaleDate);
            continue;
         }

         // If current sale is newer, add to out file
         if (pCurSale->RecDate[0] < '5')
            sprintf(acSaleDate, "20%.10s", pCurSale->RecDate);
         else
            sprintf(acSaleDate, "19%.10s", pCurSale->RecDate);

         if (memcmp(acSaleDate, pHistSale->RecDate, SIZ_SALE1_DT) > 0)
         {
            // Format cumsale record
            formatCSale(acNewSale, acCSaleRec);

            // Save last recording date
            iRet = atoin(acSaleDate, 8);
            if (iRet < lToday && lLastRecDate < iRet)
               lLastRecDate = iRet;

            // Output current sale record
            bRet = WriteFile(fhOut, acNewSale, iCSalLen, &nBytesWritten, NULL);
            iUpdateSale++;
         }
      } else if (iTmp > 0)
      {
         // Write out all history sale record for current APN
         while (iTmp > 0)
         {
            bRet = WriteFile(fhOut, acHSaleRec, iCSalLen, &nBytesWritten, NULL);
            bRet = ReadFile(fhIn, acHSaleRec, iCSalLen, &nBytesRead, NULL);
            if (!bRet || !nBytesRead)
            {
               bEof = true;
               break;
            }
            iTmp = memcmp(acCSaleRec, acHSaleRec, iApnLen);
         }

         memset(acSaleDate, ' ', 8);
         if (!bEof)
            goto NextSaleRec;
      } else // CurApn < HistApn
      {
         // Format cumsale record
         formatCSale(acNewSale, acCSaleRec);

         // Save last recording date
         iRet = atoin(pNewSale->RecDate, 8);
         if (iRet < lToday && lLastRecDate < iRet)
            lLastRecDate = iRet;

         // New sale record for this APN
         bRet = WriteFile(fhOut, acNewSale, iCSalLen, &nBytesWritten, NULL);
         iNewSale++;
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);

      if (!bRet)
      {
         LogMsg("***** Error occurs: %d\n", GetLastError());
         break;
      }
   }

   // Do the rest of the file
   while (nBytesRead > 0)
   {
      bRet = WriteFile(fhOut, acHSaleRec, iCSalLen, &nBytesWritten, NULL);
      bRet = ReadFile(fhIn, acHSaleRec, iCSalLen, &nBytesRead, NULL);

      if (!bRet)
         break;
   }

   // Close files
   if (fdCSale)
      fclose(fdCSale);
   if (fhOut)
      CloseHandle(fhOut);
   if (fhIn)
      CloseHandle(fhIn);

   // Rename output file
   strcpy(acTmp, acCSalFile);
   if (pTmp = strrchr(acTmp, '.'))
   {
      strcpy(pTmp, ".sav");
      if (!_access(acTmp, 0))
         remove(acTmp);
   }

   iRet = rename(acCSalFile, acTmp);
   if (iRet)
   {
      LogMsg("***** Error renaming %s to %s: %d", acCSalFile, acTmp, errno);
   } else
   {
      // Sort output file
      sprintf(acTmp, "S(1,14,C,A,25,12,C,D) F(FIX,%d)", iCSalLen);
      iTmp = sortFile(acOutFile, acCSalFile, acTmp);
   }

   LogMsg("Total sale records processed : %u", lCnt);
   LogMsg("      cumulative sale records: %u", iTmp);
   LogMsg("             new sale records: %u", iNewSale);
   LogMsg("         sale records updated: %u", iUpdateSale);
   LogMsg("             Latest sale date: %u", lLastRecDate);

   LogMsg("Update Sale History completed.");
   return lCnt;
}

/********************************* Sac_FormatSale ****************************
 *
 * Return -1 if bad record.  0 if OK.
 *
 *****************************************************************************/

int Sac_FormatSale(char *pFmtSale, char *pSale)
{
   SCSAL_REC *pCSale =  (SCSAL_REC *)pFmtSale;
   SAC_SALE  *pSaleRec= (SAC_SALE *)pSale;
   char     acTmp[32];
   long     lTmp, iTmp;
   double   dTmp;

   memset(pFmtSale, ' ', sizeof(SCSAL_REC));

   memcpy(pCSale->Apn, pSaleRec->Apn, RSIZ_APN);
#ifdef _DEBUG
   //if (!memcmp(pCSale->Apn, "81600070440000", 12))
   //	iTmp = 0;
#endif

   // Recording date
   if (pSaleRec->RecDate[0] > ' ')
   {
      // DocNum
      lTmp = atoin(pSaleRec->RecPage, SSIZ_REC_PAGE);
      iTmp = sprintf(acTmp, "%.6s%.4d", pSaleRec->RecDate, lTmp);
      memcpy(pCSale->DocNum, acTmp, iTmp);

      // RecDate
      if (pSaleRec->RecDate[0] < '5')
         lTmp = sprintf(acTmp, "20%.6s", pSaleRec->RecDate);
      else
         lTmp = sprintf(acTmp, "19%.6s", pSaleRec->RecDate);
      memcpy(pCSale->DocDate, acTmp, lTmp);

      // Save last recording date
      lTmp = atoin(acTmp, 8);
      if (lTmp < lToday && lLastRecDate < lTmp)
         lLastRecDate = lTmp;
   } else
      return -1;

   // DeedType
   if (pSaleRec->DeedType[0] > ' ')
   {
      memcpy(pCSale->DocCode, pSaleRec->DeedType, SALE_SIZ_DOCCODE);
      iTmp = findDocType(pSaleRec->DeedType, (IDX_TBL5 *)&SAC_DocCode[0]);
      if (iTmp >= 0)
      {
         memcpy(pCSale->DocType, SAC_DocCode[iTmp].pCode, SAC_DocCode[iTmp].iCodeLen);
         pCSale->NoneSale_Flg = SAC_DocCode[iTmp].flag;
      } 

   }

   memcpy(pCSale->Seller1, pSaleRec->Grantor, SSIZ_GRANTOR);
   memcpy(pCSale->Name1, pSaleRec->Grantee, SSIZ_GRANTEE);

   // StampAmt
   lTmp = atoin(pSaleRec->StampAmt, SSIZ_STAMP_AMOUNT);
   if (lTmp > 0)
   {
      dTmp = lTmp / 100;
      iTmp = sprintf(acTmp, "%9.2f", dTmp);
      memcpy(pCSale->StampAmt, acTmp, iTmp);

      // SalePrice
      lTmp = (long)((double)lTmp * SALE_FACTOR_100);
      if (lTmp < 100)
         sprintf(acTmp, "%*u", SALE_SIZ_SALEPRICE, lTmp);
      else
         sprintf(acTmp, "%*u00", SALE_SIZ_SALEPRICE-2, lTmp/100);

      memcpy(pCSale->SalePrice, acTmp, SALE_SIZ_SALEPRICE);

      // Salecode: (F)ull, (P)artial, (T)ransfer
      pCSale->SaleCode[0] = pSaleRec->StampCode[0];
   }

   // ActualSalePrice
   lTmp = atoin(pSaleRec->SalePrice, SSIZ_SALE_PRICE);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*u", SALE_SIZ_SALEPRICE, lTmp);
      memcpy(pCSale->ConfirmedSalePrice, acTmp, SALE_SIZ_SALEPRICE);
   }

   // AdjSalePrice
   lTmp = atoin(pSaleRec->CashPrice, SSIZ_SALE_PRICE);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*u", SALE_SIZ_SALEPRICE, lTmp);
      memcpy(pCSale->AdjSalePrice, acTmp, SALE_SIZ_SALEPRICE);
   }

   // PctTransfer
   lTmp = atoin(pSaleRec->PctTransfer, SSIZ_PARTIAL_INT);
   if (lTmp > 0)
   {
      memcpy(pCSale->PctXfer, pSaleRec->PctTransfer, SSIZ_PARTIAL_INT);
      if (lTmp == 100)
         pCSale->SaleCode[0] = 'F';
      else
         pCSale->SaleCode[0] = 'P';
      pCSale->SaleCode[1] = ' ';
   }

   // # of parcels
   iTmp = atoin(pSaleRec->NumParcels, sizeof(pSaleRec->NumParcels));
   if (iTmp > 1)
   {
      iTmp = sprintf(acTmp, "%d", iTmp);
      memcpy(pCSale->NumOfPrclXfer, acTmp, iTmp);
      pCSale->MultiSale_Flg = 'Y';
   }

   pCSale->CRLF[0] = '\n';
   pCSale->CRLF[1] = 0;

   return 0;
}

/****************************** Sac_UpdateCumSale ****************************
 *
 * Update history sale.  This function adds new sale record to cumulative file.
 *
 * Return 0 if successful, otherwise error
 *
 *****************************************************************************/

int Sac_UpdateCumSale(void)
{
   char     *pTmp, acCSalRec[512], acSaleRec[1024], acSaleDate[32], acTmp[256];
   char     acOutFile[_MAX_PATH], acHistSale[_MAX_PATH], acTmpSale[_MAX_PATH];

   long     lCnt=0;
   int		iRet, iTmp, iUpdateSale=0;


   // Check current sale file
   if (_access(acSaleFile, 0))
   {
      LogMsg("***** Missing current sale file %s", acSaleFile);
      return -1;
   }

   // Open current sale
   LogMsg("Open current sale file %s", acSaleFile);
   fdSale = fopen(acSaleFile, "r");
   if (fdSale == NULL)
   {
      LogMsg("***** Error opening current sale file: %s\n", acSaleFile);
      return -2;
   }

   // Create output file
   sprintf(acTmpSale, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "tmp");
   LogMsg("Create updated sale file %s", acTmpSale);
   fdCSale = fopen(acTmpSale, "w");
   if (fdCSale == NULL)
   {
      LogMsg("***** Error creating updated sale file: %s\n", acTmpSale);
      return -3;
   }

   // Read first history sale
   acSaleDate[0] = 0;
   acCSalRec[0] = 0;

   // Merge loop
   while (!feof(fdSale))
   {

      // Get current sale
      pTmp = fgets(acSaleRec, 1024, fdSale);
      if (!pTmp)
         break;

      iRet = Sac_FormatSale(acCSalRec, acSaleRec);
      if (!iRet)
      {
         fputs(acCSalRec, fdCSale);
         iUpdateSale++;
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);

   }

   // Close files
   if (fdCSale)
      fclose(fdCSale);
   if (fdSale)
      fclose(fdSale);
   fdSale = NULL;

   // Update sale history file
   sprintf(acHistSale, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "sls");
   sprintf(acOutFile,  acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "dat");

   // Sort/merge sale file - APN (asc), Saledate (asc), DocNum (asc)
   sprintf(acTmp, "S(1,%d,C,A,27,8,C,A,15,%d,C,A,57,10,C,D) DUPO(1,34)", iApnLen, SALE_SIZ_DOCNUM);
   iTmp = sortFile(acTmpSale, acOutFile, acTmp);
   if (iTmp > 0)
   {
      strcpy(acSaleFile, acOutFile);
      if (!_access(acHistSale, 0))
      {
         sprintf(acSaleRec, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "srt");
         sprintf(acTmpSale, "%s+%s", acOutFile, acHistSale);
         iTmp = sortFile(acTmpSale, acSaleRec, acTmp);
         if (iTmp > 0)
         {
            sprintf(acTmpSale, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "sav");
            if (!_access(acTmpSale, 0))
               DeleteFile(acTmpSale);
      
            // Rename sls to sav then srt to SLS file
            rename(acHistSale, acTmpSale);
            rename(acSaleRec, acHistSale);
         }
      } else
      {
         CopyFile(acOutFile, acHistSale, false);
      }
   }

   LogMsg("Total sale records processed:   %u", lCnt);
   LogMsg("Total cumulative sale records:  %u", iTmp);
   LogMsg("Total sale records updated:     %u", iUpdateSale);

   LogMsg("Update Sale History completed.");

   return 0;
}

/******************************* Sac_CreateLienRec ***************************
 *
 * Return 0 if successful, >0 if error
 *
 *****************************************************************************/

int Sac_CreateLienRec(char *pOutbuf, char *pInRec)
{
   char     acTmp[256];
   long     iTmp;
   LONGLONG lTmp;
   LIENEXTR *pLienRec = (LIENEXTR *)pOutbuf;
   SAC_LVAL *pRec = (SAC_LVAL *)pInRec;

   // Clear output buffer
   memset(pOutbuf, ' ', sizeof(LIENEXTR));

   // Start copying data
   memcpy(pLienRec->acApn, pRec->Apn, LSIZ_APN);

   // TRA
   lTmp = atoin(pRec->TRA, LSIZ_TRA);
   if (lTmp > 0)
   {
      iTmp = sprintf(acTmp, "%.6d", lTmp);
      memcpy(pLienRec->acTRA, acTmp, iTmp);
   }

   // Year assessed
   memcpy(pLienRec->acYear, myCounty.acYearAssd, 4);

   // Land
   long lLand = atoin(pRec->Land, LSIZ_LAND);
   if (lLand > 0)
   {
      iTmp = sprintf(acTmp, "%*u", sizeof(pLienRec->acLand), lLand);
      memcpy(pLienRec->acLand, acTmp, iTmp);
   }

   // Improve
   long lImpr = atoin(pRec->Impr, LSIZ_LAND);
   if (lImpr > 0)
   {
      iTmp = sprintf(acTmp, "%*u", sizeof(pLienRec->acImpr), lImpr);
      memcpy(pLienRec->acImpr, acTmp, iTmp);
   }

   // PP value
   long lPers = atoin(pRec->PP_Val, LSIZ_LAND);

   // Fixture or ME val
   long lFixture = atoin(pRec->Fixt_Val, LSIZ_LAND);

   // Other value total
   lTmp = lPers + lFixture;
   if (lTmp > 0)
   {
      iTmp = sprintf(acTmp, "%*u", sizeof(pLienRec->acOther), lTmp);
      memcpy(pLienRec->acOther, acTmp, iTmp);

      if (lPers > 0)
      {
         iTmp = sprintf(acTmp, "%*u", sizeof(pLienRec->acPP_Val), lPers);
         memcpy(pLienRec->acPP_Val, acTmp, iTmp);
      }
      if (lFixture > 0)
      {
         iTmp = sprintf(acTmp, "%*u", sizeof(pLienRec->acME_Val), lFixture);
         memcpy(pLienRec->acME_Val, acTmp, iTmp);
      }
   }

   // Gross total
   lTmp += lLand+lImpr;
   if (lTmp > 0)
   {
      iTmp = sprintf(acTmp, "%*u", sizeof(pLienRec->acGross), lTmp);
      memcpy(pLienRec->acGross, acTmp, iTmp);

      // Ratio
      if (lImpr > 0)
      {
         iTmp = sprintf(acTmp, "%*u", sizeof(pLienRec->acRatio), (LONGLONG)lImpr*100/(lLand+lImpr));
         memcpy(pLienRec->acRatio, acTmp, iTmp);
      }
   }

   // HO Exempt
   long lHOExe = atoin(pRec->HO_Exe, LSIZ_LAND);
   if (lHOExe > 0)
   {
      pLienRec->acHO[0] = '1';      // 'Y'
      iTmp = sprintf(acTmp, "%*u", sizeof(pLienRec->acExAmt), lHOExe);
      memcpy(pLienRec->acExAmt, acTmp, iTmp);
   } else
      pLienRec->acHO[0] = '2';      // 'N'

   // Total Exe
   long lTotalExe = atoin(pRec->Total_Exe, LSIZ_LAND);
   if (lTotalExe > 0)
   {
      iTmp = sprintf(acTmp, "%*u", sizeof(pLienRec->acExAmt), lTmp);
      memcpy(pLienRec->acExAmt, acTmp, iTmp);
   }

   // Full exemption flag ?????

   // Prop 8 flag
   if (pRec->ActionCode[0] == 'D')
      pLienRec->SpclFlag |= LX_PROP8_FLG;

   if (pRec->ActionCode[0] > ' ')
      memcpy(pLienRec->extra.Sac.ActionCode, pRec->ActionCode, LSIZ_ACTION_CODE);

   pLienRec->LF[0] = '\n';
   pLienRec->LF[1] = 0;
   return 0;
}

/********************************* Sac_ExtrLien *****************************
 *
 *
 ****************************************************************************/

int Sac_ExtrLien()
{
   char  *pTmp, acBuf[1024], acRollRec[1024], acOutFile[_MAX_PATH];
   long  lCnt=0;
   FILE  *fdLien;

   // Open LDR value file
   GetIniString(myCounty.acCntyCode, "LValFile", "", acOutFile, _MAX_PATH, acIniFile);
   if (acOutFile[0] > ' ' && !_access(acOutFile, 0))
   {
      LogMsg("Open LDR values file %s", acOutFile);
      fdValue = fopen(acOutFile, "r");
      if (fdValue == NULL)
      {
         LogMsg("***** Error opening Landsize file: %s\n", acOutFile);
         return 2;
      }
   } else
   {
      LogMsg("***** Missing input file: %s", acOutFile);
      return -1;
   }

   // Open Output file
   sprintf(acOutFile, acLienTmpl, myCounty.acCntyCode, myCounty.acCntyCode);
   LogMsg("Open output file %s", acOutFile);
   fdLien = fopen(acOutFile, "w");
   if (fdLien == NULL)
   {
      LogMsg("***** Error opening Lien extract file: %s\n", acOutFile);
      return 4;
   }

   // Merge loop
   while (!feof(fdValue))
   {
      // Get roll rec
      pTmp = fgets(acRollRec, 1024, fdValue);
      if (!pTmp)
         break;

      // Create new lien record
      Sac_CreateLienRec(acBuf, acRollRec);

      fputs(acBuf, fdLien);
      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdValue)
      fclose(fdValue);
   if (fdLien)
      fclose(fdLien);

   fdValue = NULL;

   LogMsgD("\nTotal records output:     %d\n", lCnt);

   lRecCnt = lCnt;
   return 0;
}

/******************************* Sac_CreateLienRec ***************************
 *
 * Create lien record from delimited record.
 *
 * Return 0 if successful, >0 if error
 *
 *****************************************************************************/

int Sac_CreateLienRec2(char *pOutbuf, char *pRollRec)
{
   char     acTmp[256], acApn[256];
   LONGLONG lTmp;
   int      iTmp;
   LIENEXTR *pLienRec = (LIENEXTR *)pOutbuf;

   iTmp = ParseStringNQ(pRollRec, ';', MAX_FLD_TOKEN, apTokens);
   if (iTmp < SAC_L_FLDS)
   {
      LogMsg("***** Error: bad input record for APN=%s-%s-%s-%s", apTokens[0], apTokens[1], apTokens[2], apTokens[3]);
      return -1;
   }

   // Clear output buffer
   memset(pOutbuf, ' ', sizeof(LIENEXTR));

   // Format APN
   iTmp = sprintf(acApn, "%s%s%s%s", apTokens[0], apTokens[1], apTokens[2], apTokens[3]);
   memcpy(pLienRec->acApn, acApn, iTmp);

   // Format TRA
   lTmp = atol(apTokens[SAC_L_TAX_RATE_AREA]);
   iTmp = sprintf(acTmp, "%.6d", lTmp);
   memcpy(pLienRec->acTRA, acTmp, iTmp);

#ifdef _DEBUG
   //if (!memcmp(pLienRec->acApn, "00102315", 8) )
   //   iTmp = 0;
#endif

   // Year assessed
   memcpy(pLienRec->acYear, myCounty.acYearAssd, 4);

   // Land
   long lLand = atol(apTokens[SAC_L_LAND]);
   if (lLand > 0)
   {
      iTmp = sprintf(acTmp, "%*u", sizeof(pLienRec->acLand), lLand);
      memcpy(pLienRec->acLand, acTmp, iTmp);
   }

   // Improve - Ratio
   long lImpr = atol(apTokens[SAC_L_IMPR]);
   if (lImpr > 0)
   {
      iTmp = sprintf(acTmp, "%*u", sizeof(pLienRec->acImpr), lImpr);
      memcpy(pLienRec->acImpr, acTmp, iTmp);

      iTmp = sprintf(acTmp, "%*u", sizeof(pLienRec->acRatio), (LONGLONG)lImpr*100/(lLand+lImpr));
      memcpy(pLienRec->acRatio, acTmp, iTmp);
   }

   // PP value
   long lPers = atol(apTokens[SAC_L_PP]);

   // Fixture
   long lFixt = atol(apTokens[SAC_L_FIXTURE]);

   // Total other
   long lOthers = lPers + lFixt;
   if (lOthers > 0)
   {
      iTmp = sprintf(acTmp, "%*u", sizeof(pLienRec->acOther), lOthers);
      memcpy(pLienRec->acOther, acTmp, iTmp);

      if (lPers > 0)
      {
         iTmp = sprintf(acTmp, "%u", lPers);
         memcpy(pLienRec->acPP_Val, acTmp, iTmp);
      }
      if (lFixt > 0)
      {
         iTmp = sprintf(acTmp, "%u", lFixt);
         memcpy(pLienRec->acME_Val, acTmp, iTmp);
      }
   }

   // Gross
   lTmp = lOthers + lLand + lImpr;
   if (lTmp > 0)
   {
      iTmp = sprintf(acTmp, "%*u", sizeof(pLienRec->acGross), lTmp);
      memcpy(pLienRec->acGross, acTmp, iTmp);
   }

   // HO Exe 
   long lExe = atol(apTokens[SAC_L_HO_EX]);
   if (lExe > 0)
   {
      iTmp = sprintf(acTmp, "%d", lExe);
      memcpy(pLienRec->acExAmt, acTmp, iTmp);
      pLienRec->acHO[0] = '1';         // Y
   } else
      pLienRec->acHO[0] = '2';         // N

   lTmp = atol(apTokens[SAC_L_OTHER_EXE]);
   if (lTmp > 0)
   {
      iTmp = sprintf(acTmp, "%u", lTmp+lExe);
      memcpy(pLienRec->acExAmt, acTmp, iTmp);
      if (lTmp == 7000)
         pLienRec->acHO[0] = '1';      // Y
   } 

   // Prop 8 flag
   if (*apTokens[SAC_L_ACTION_CODE] == 'D')
      pLienRec->SpclFlag |= LX_PROP8_FLG;

   if (*apTokens[SAC_L_ACTION_CODE] > ' ')
      vmemcpy(pLienRec->extra.Sac.ActionCode, apTokens[SAC_L_ACTION_CODE], LSIZ_ACTION_CODE);

   pLienRec->LF[0] = '\n';
   pLienRec->LF[1] = 0;
   return 0;
}

/******************************** Sac_ExtrLien2 *****************************
 *
 * Extract lien from CSV format file (2019_secured_roll.txt)
 *
 ****************************************************************************/

int Sac_ExtrLien2(char *pInFile)
{
   char  acBuf[MAX_RECSIZE], acRollRec[MAX_RECSIZE], acOutFile[_MAX_PATH];
   char  *pTmp;
   FILE  *fdOut;
   long  lCnt=0, lOut=0, iTmp;

   LogMsg0("Extract lien for SAC");

   // Open roll file
   LogMsg("Open Roll file %s", pInFile);
   fdRoll = fopen(pInFile, "r");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", pInFile);
      return 2;
   }

   // Open Output file
   sprintf(acOutFile, acLienTmpl, myCounty.acCntyCode, myCounty.acCntyCode);
   LogMsg("Open output file %s", acOutFile);
   fdOut = fopen(acOutFile, "w");
   if (fdOut == NULL)
   {
      LogMsg("***** Error opening Lien extract file: %s\n", acOutFile);
      return 4;
   }

   // Skip header
   pTmp = fgets(acRollRec, MAX_RECSIZE, fdRoll);

   // Merge loop
   while (!feof(fdRoll))
   {
      // Get roll rec
      if (!(pTmp = fgets(acRollRec, MAX_RECSIZE, fdRoll) ))
         break;

      // Create new lien record
      iTmp = Sac_CreateLienRec2(acBuf, acRollRec);
      if (!iTmp)
      {
         lOut++;
         fputs(acBuf, fdOut);
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdRoll)
      fclose(fdRoll);
   if (fdOut)
      fclose(fdOut);

   LogMsg("Total records processed:     %d", lCnt);
   LogMsg("         records output:     %d\n", lOut);

   return 0;
}

/***************************** ExtractOwnerCode *****************************
 *
 * Input file:
 *    - aimsab_public.txt           (LDR file, 299-byte ascii)
 *
 * Output file:
 *    - Sac_OwnerCode.txt
 *
 ****************************************************************************/

void ExtractOwnerCode()
{
   char  acTmp[_MAX_PATH], acRec[512], *pTmp;
   FILE  *fdOut;
   int   lCnt=0;

   LogMsg("Extract Owner code");

   // Open LDR value file
   GetIniString(myCounty.acCntyCode, "LValFile", "", acTmp, _MAX_PATH, acIniFile);
   if (!_access(acTmp, 0))
   {
      LogMsg("Open LDR values file %s", acTmp);
      fdValue = fopen(acTmp, "r");
      if (fdValue == NULL)
      {
         LogMsg("***** Error opening Landsize file: %s\n", acTmp);
         return;
      }
   } else
   {
      LogMsg("***** LDR value file is missing: %s", acTmp);
      return;
   }

   // Create output file
   GetIniString(myCounty.acCntyCode, "OCFile", "", acTmp, _MAX_PATH, acIniFile);
   LogMsg("Open owner code file %s", acTmp);
   fdOut = fopen(acTmp, "w");
   if (!fdOut)
      return;

   while (!feof(fdValue))
   {
      pTmp = fgets(acRec, 512, fdValue);
      if (pTmp)
      {
         sprintf(acTmp, "%.14s,%.2s\n", pTmp, pTmp+LOFF_OWNER_CODE);
         fputs(acTmp, fdOut);
      }
      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   } 

   fclose(fdValue);
   fclose(fdOut);
}

/****************************** Sac_ConvCommChar *****************************
 *
 * Convert commercial char file CommCharExtract.xml to standard char format
 *
 * Return number of output record.  <0 if error.
 *
 ****************************************************************************/

int Sac_ConvCommChar(LPCSTR pXmlFile) 
{
   CString  csText;
   CFile    file;
   char     acTmp[256], acOutFile[_MAX_PATH];
   int      iRet, iCnt=0, iOut=0, lTmp;
   double   dTmp;

   LogMsgD("\nConverting Commercial Char file: %s", pXmlFile);

   if (!file.Open( pXmlFile, CFile::modeRead ) )
   {
      LogMsg("***** Error unable to open file: %s", pXmlFile);
      return -1;
   }
   int nFileLen = (int)file.GetLength();

   // Allocate buffer for binary file data
   unsigned char* pBuffer = new unsigned char[nFileLen + 2];
   nFileLen = file.Read( pBuffer, nFileLen );
   file.Close();
   pBuffer[nFileLen] = '\0';
   pBuffer[nFileLen+1] = '\0'; // in case 2-byte encoded

   // Windows Unicode file is detected if starts with FEFF
   if (pBuffer[0] == 0xff && pBuffer[1] == 0xfe )
   {
      // Contains byte order mark, so assume wide char content
      // non _UNICODE builds should perform UCS-2 (wide char) to UTF-8 conversion here
      csText = (LPCWSTR)(&pBuffer[2]);
      LogMsg("---> File starts with hex FFFE, assumed to be wide char format.");
   } else
   {
      // _UNICODE builds should perform UTF-8 to UCS-2 (wide char) conversion here
      csText = (LPCSTR)pBuffer;
   }
   delete [] pBuffer;

   // If it is too short, assume it got truncated due to non-text content
   if (csText.GetLength() < nFileLen / 2 - 20 )
   {
      LogMsg("***** Error converting file to string (may contain binary data)");
      return -1;
   }

   // Create output file
   sprintf(acOutFile, "%s\\SAC\\Sac_Char.out", acTmpPath);
   if (!file.Open(acOutFile, CFile::modeCreate|CFile::modeWrite ) )
   {
      LogMsg("***** Error unable to create file: %s", acOutFile);
      return -1;
   }

   // Parse
   CMarkup xml;
   BOOL bResult = xml.SetDoc( csText );

   STDCHAR  myChar;
   CString  sTmp, sStrNum, sPreDir, sStrName, sStrSfx, sPostDir, sUnitType, sUnitNum, sCity, sState, sZip;
   
   // Look for char data
   while (xml.FindChildElem( _T("V_COMM_CHAR_EXTRACT") ) )
   {

      Check(xml.IntoElem(), "Look into V_COMM_CHAR_EXTRACT");
      
      // Get APN
      if (xml.FindChildElem( _T("PARCEL_NUMBER") ))
      {
         memset(&myChar, ' ', sizeof(STDCHAR));

         sTmp = xml.GetChildData();
         memcpy(myChar.Apn_D, sTmp.GetBuffer(0), sTmp.GetLength());
         sTmp.Remove('-');
         memcpy(myChar.Apn, sTmp.GetBuffer(0), sTmp.GetLength());

         //if (xml.FindChildElem( _T("STREET_NUMBER") ))
         //{
         //   sStrNum = xml.GetChildData();
         //   memcpy(myChar.Misc.sAdr.strNum, sStrNum.GetBuffer(0), sStrNum.GetLength());
         //}
         //if (xml.FindChildElem( _T("PRE_DIRECTIONAL") ))
         //{
         //   sPreDir = xml.GetChildData();
         //   memcpy(myChar.Misc.sAdr.strDir, sPreDir.GetBuffer(0), sPreDir.GetLength());
         //}
         //if (xml.FindChildElem( _T("STREET_NAME") ))
         //{
         //   sStrName = xml.GetChildData();
         //   memcpy(myChar.Misc.sAdr.strName, sStrName.GetBuffer(0), sStrName.GetLength());
         //}
         //if (xml.FindChildElem( _T("STREET_SUFFIX") ))
         //{
         //   sStrSfx = xml.GetChildData();
         //   memcpy(myChar.Misc.sAdr.strSfx, sStrSfx.GetBuffer(0), sStrSfx.GetLength());
         //}
         //if (xml.FindChildElem( _T("SEC_UNIT_NUMBER") ))
         //{
         //   sUnitNum = xml.GetChildData();
         //   memcpy(myChar.Misc.sAdr.Unit, sUnitNum.GetBuffer(0), sUnitNum.GetLength());
         //}
         //if (xml.FindChildElem( _T("CITY") ))
         //{
         //   sCity = xml.GetChildData();
         //   memcpy(myChar.Misc.sAdr.City, sCity.GetBuffer(0), sCity.GetLength());
         //}
         //if (xml.FindChildElem( _T("STATE") ))
         //{
         //   sState = xml.GetChildData();
         //   memcpy(myChar.Misc.sAdr.State, sState.GetBuffer(0), sState.GetLength());
         //}
         //if (xml.FindChildElem( _T("ZIP_CODE") ))
         //{
         //   sZip = xml.GetChildData();
         //   memcpy(myChar.Misc.sAdr.Zip, sZip.GetBuffer(0), sZip.GetLength());
         //}
         //xml.FindChildElem( _T("OWNER") );
         //sTmp = xml.GetChildData();
         //memcpy(myChar.Misc.sAdr.strNum, sTmp.GetBuffer(0), sTmp.GetLength());

         if (xml.FindChildElem( _T("ZONING") ))
         {
            sTmp = xml.GetChildData();
            memcpy(myChar.Zoning, sTmp.GetBuffer(0), sTmp.GetLength());
         }
         if (xml.FindChildElem( _T("BUILDING_SF") ))
         {
            sTmp = xml.GetChildData();
            memcpy(myChar.BldgSqft, sTmp.GetBuffer(0), sTmp.GetLength());
         }
         if (xml.FindChildElem( _T("PROPERTY_SF") ))
         {
            sTmp = xml.GetChildData();
            memcpy(myChar.LotSqft, sTmp.GetBuffer(0), sTmp.GetLength());
            lTmp = atol(sTmp);
            dTmp = SQFT_MF_1000 * lTmp;
            iRet = sprintf(acTmp, "%d", (long)(lTmp*SQFT_MF_1000));
            memcpy(myChar.LotAcre, acTmp, iRet);
         }
         if (xml.FindChildElem( _T("LAND_USE_CODE") ))
         {
            sTmp = xml.GetChildData();
            memcpy(myChar.LandUse, sTmp.GetBuffer(0), sTmp.GetLength());
         }
         if (xml.FindChildElem( _T("YEAR_BUILT") ))
         {
            sTmp = xml.GetChildData();
            memcpy(myChar.YrBlt, sTmp.GetBuffer(0), sTmp.GetLength());
         }
         if (xml.FindChildElem( _T("EFFECTIVE_YEAR") ))
         {
            sTmp = xml.GetChildData();
            memcpy(myChar.YrEff, sTmp.GetBuffer(0), sTmp.GetLength());
         }
         if (xml.FindChildElem( _T("NUMBER_OF_STORIES") ))
         {
            sTmp = xml.GetChildData();
            memcpy(myChar.Stories, sTmp.GetBuffer(0), sTmp.GetLength());
         }
         if (xml.FindChildElem( _T("QUALITY_CLASS") ))
         {
            sTmp = xml.GetChildData();
            iRet = sTmp.GetLength();
            if (iRet > 3)
            {
               myChar.BldgQual = sTmp.GetAt(0);
               // Change Low Cost to Fair to match with standard table
               if (myChar.BldgQual == 'L')
                  myChar.BldgQual = 'F';              
               myChar.BldgClass = sTmp.GetAt(iRet-1);
            }
         }
         if (xml.FindChildElem( _T("GROUND_FLOOR_GROSS") ))
         {
            sTmp = xml.GetChildData();
            vmemcpy(myChar.Sqft_1stFl, sTmp.GetBuffer(0), SIZ_CHAR_SQFT);
         }
         if (xml.FindChildElem( _T("NET_RENTAL") ))
         {
            sTmp = xml.GetChildData();
            vmemcpy(myChar.NetRental, sTmp.GetBuffer(0), SIZ_CHAR_SQFT);
         }
         myChar.CRLF[0] = 13;
         myChar.CRLF[1] = 10;
         file.Write((char *)&myChar.Apn[0], sizeof(STDCHAR));
         iOut++;
      }

      Check(xml.OutOfElem(), "Out of ");
      if (!(++iCnt % 1000))
         printf("\r%u", iCnt);
   }

   file.Close();

   // Sort output
   iOut = sortFile(acOutFile, acCChrFile, "S(1,14,C,A) ");

   LogMsg("Total records processed: %d", iCnt);
   LogMsg("              output:    %d", iOut);

   return iOut;
}

/****************************** Sac_ConvCommChar2 ****************************
 *
 * Convert commercial char file city_full_comm_char.dos to standard char format
 * Data from this file is similar to CommCharExtract.xml (except Owner, Zoning, LandUse & Situs)
 *
 * Return number of output record.  <0 if error.
 *
 ****************************************************************************/

int Sac_ConvCommChar2(LPCSTR pInfile) 
{
   FILE     *fdIn, *fdOut;
   char     acBuf[4096], acTmpFile[256], acTmp[256], *pRec;
   int      iRet, iTmp, iFldCnt, iCnt=0;
   STDCHAR  myCharRec;

   LogMsg0("\nConverting commercial char file %s", pInfile);
   if (!(fdIn = fopen(pInfile, "r")))
      return -1;

   sprintf(acTmpFile, "%s\\%s\\%s_char.tmp", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   if (!(fdOut = fopen(acTmpFile, "w")))
   {
      fclose(fdIn);
      LogMsg("***** Error creating output file %s", acTmpFile);
      return -2;
   }

   // Skip first record - header
   pRec = fgets(acBuf, 4096, fdIn);

   while (!feof(fdIn))
   {
      pRec = fgets(acBuf, 4096, fdIn);

      if (!pRec)
         break;

      // Replace NULL with empty string
      iFldCnt = ParseStringIQ(pRec, '|', MAX_FLD_TOKEN, apTokens);
      if (iFldCnt < CC_NET_RENTAL)
      {
         if (iFldCnt > 1)
            LogMsg("*** Bad COMM CHAR record (%d): %s [%d]", iCnt, pRec, iFldCnt);
         continue;
      }

      memset((void *)&myCharRec, ' ', sizeof(STDCHAR));
      memcpy(myCharRec.Apn, apTokens[PARCEL_NUMBER], strlen(apTokens[PARCEL_NUMBER]));
      // Format APN
      iRet = formatApn(apTokens[PARCEL_NUMBER], acTmp, &myCounty);
      memcpy(myCharRec.Apn_D, acTmp, iRet);

      // QualityClass
      iTmp = blankRem(_strupr(apTokens[CC_QUALITY_CLASS]));
      if (*apTokens[CC_QUALITY_CLASS] > ' ' && *apTokens[CC_QUALITY_CLASS] < 'Z')
      {
         vmemcpy(myCharRec.QualityClass, apTokens[CC_QUALITY_CLASS], SIZ_CHAR_QCLS);

         strcpy(acTmp, apTokens[CC_QUALITY_CLASS]);
         myCharRec.BldgClass = acTmp[iTmp-1];
         if (acTmp[0] == 'L') 
            myCharRec.BldgQual = 'F';
         else
            myCharRec.BldgQual = acTmp[0];
      }

      // YrBlt
      int iYrBlt = atoi(apTokens[CC_YEAR_BUILT]);
      if (iYrBlt > 1600 && iYrBlt <= lToyear)
      {
         iRet = sprintf(acTmp, "%d", iYrBlt);
         memcpy(myCharRec.YrBlt, acTmp, iRet);
      }

      // YrEff
      iTmp = atoi(apTokens[CC_EFFECTIVE_YEAR]);
      if (iTmp > 1900 && iTmp >= iYrBlt && iTmp <= lToyear)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.YrEff, acTmp, iRet);
      }

#ifdef _DEBUG
      //if (!memcmp(myCharRec.Apn, "003351003000", 9))
      //   iRet = 0;
#endif

      // BldgSize
      int iBldgSize = atoi(apTokens[CC_BUILDING_SF]);
      if (iBldgSize > 100)
      {
         iRet = sprintf(acTmp, "%d", iBldgSize);
         memcpy(myCharRec.BldgSqft, acTmp, iRet);
      }

      // First floor area
      iTmp = atoi(apTokens[CC_GROUND_FLOOR_GROSS]);
      if (iTmp > 100)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.Sqft_1stFl, acTmp, iRet);
      }
      
      // Stories/NumFloors
      iTmp = atoi(apTokens[CC_NUMBER_OF_STORIES]);
      if (iTmp > 0)
      {
         iRet = sprintf(acTmp, "%d.0", iTmp);
         memcpy(myCharRec.Stories, acTmp, iRet);
      }

      // Net rental
      iTmp = atoi(apTokens[CC_NET_RENTAL]);
      if (iTmp > 0)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.NetRental, acTmp, iRet);
      }

      myCharRec.CRLF[0] = '\n';
      myCharRec.CRLF[1] = '\0';
      fputs((char *)&myCharRec.Apn[0], fdOut);

      if (!(++iCnt % 1000))
         printf("\r%u", iCnt);
   }

   if (fdIn) fclose(fdIn);
   if (fdOut) fclose(fdOut);

   LogMsg("Number of records processed: %d", iCnt);

   // Sort output on ASMT
   if (iCnt > 100)
   {
      LogMsg("Sorting char file %s --> %s", acTmpFile, acCChrFile);
      iRet = sortFile(acTmpFile, acCChrFile, "S(1,12,C,A,81,4,C,A,57,4,C,D) DUPO(B2000,)");
   } else
   {
      printf("\n");
      iRet = 0;
   }
   return iRet;
}

/****************************** Sac_UpdateTaxBase *****************************
 *
 * Update Tax Base using SAC.TC
 *
 ******************************************************************************/

int Sac_UpdateTaxBase(char *pBaseBuf, FILE *fd)
{
   static   char  acRec[MAX_RECSIZE], *pRec=NULL;
   static   bool  bNoParse=false;
   int      iLoop;
   char     acTmp[256];
   double   dTmp;

   TAXBASE   *pBaseRec = (TAXBASE *)pBaseBuf;

   if (!pRec)
      pRec = fgets(acRec, MAX_RECSIZE, fd);        // Get first rec

#ifdef _DEBUG
   //int iRet;
   //if (!memcmp(pBaseRec->Apn, "00100120150000", 10))
   //   iRet = 0;
#endif

   do
   {
      // Compare Apn
      iLoop = memcmp(pBaseRec->Apn, acRec, iApnLen);
      if (iLoop > 0) 
      {
         lTaxSkip++;
         if (bDebug)
            LogMsg("*** No Base rec %d: %.80s", lTaxSkip, acRec);
         pRec = fgets(acRec, MAX_RECSIZE, fd);
         if (!pRec)
         {
            fclose(fd);
            fd = NULL;
            return 1;      // EOF
         }
         bNoParse = false;
         iLoop = 1;
      }
   } while (iLoop > 0);

   if (iLoop)
      return 1;

   // Parse input string
   if (!bNoParse)
   {
      iTokens = ParseStringIQ(acRec, '|', 128, apTokens);
      if (iTokens < TC_NET_VAL)
      {
         LogMsg("***** Error: bad Tax record for APN=%.100s (#tokens=%d)", acTmp, iTokens);
         return -1;
      }
   }

   // Update date
   if (iTokens > TC_UPD_DATE && *apTokens[TC_UPD_DATE] > ' ' && dateConversion(apTokens[TC_UPD_DATE], acTmp, MM_DD_YYYY_1))
      strcpy(pBaseRec->Upd_Date, acTmp);
   else
      sprintf(pBaseRec->Upd_Date, "%d", lTCDate);

   if (!memcmp(apTokens[TC_PAID_STAT1], "CA", 2))
   {
      pBaseRec->Inst1Status[0] = TAX_BSTAT_CANCEL;
      pBaseRec->Inst2Status[0] = TAX_BSTAT_CANCEL;
      pBaseRec->TotalDue[0] = 0;
      pBaseRec->PaidAmt1[0] = 0;
      pBaseRec->PaidAmt2[0] = 0;
      return 1;
   }

   // Matching up billNum before update
   if (strcmp(pBaseRec->BillNum, apTokens[TC_BILL_NUM]))
   {
      // Bill may have been cancelled.  Mark cancel and add new bill
      LogMsg("*** Skip update APN=%s, Base.BillNum=%s, TC.BillNum=%s, Status=%s", pBaseRec->Apn, pBaseRec->BillNum, apTokens[TC_BILL_NUM], apTokens[TC_PAID_STAT1]);
      bNoParse = true;
      return 1;
   }

   // Do not update cancelled bill
   if (pBaseRec->Inst1Status[0] == TAX_BSTAT_CANCEL && pBaseRec->Inst2Status[0] == TAX_BSTAT_CANCEL)
   {
      bNoParse = true;
      return 1;
   }

   // Update paid info
   if (!memcmp(apTokens[TC_PAID_STAT1], "PA", 2))
   {
      pBaseRec->Inst1Status[0] = TAX_BSTAT_PAID;
      strcpy(pBaseRec->PaidAmt1, apTokens[TC_AMT_PAID1]);
      if (*apTokens[TC_DUE_PAID_DT1] >= '0' && dateConversion(apTokens[TC_DUE_PAID_DT1], acTmp, MM_DD_YYYY_1))
         strcpy(pBaseRec->PaidDate1, acTmp);
      else
         LogMsg("*** Bad PaidDate 1: %s [%s]", apTokens[TC_DUE_PAID_DT1], apTokens[TC_APN_S]);
   } else if (!memcmp(apTokens[TC_PAID_STAT1], "PE", 2))
   {
      pBaseRec->Inst1Status[0] = TAX_BSTAT_PENDING;
      strcpy(pBaseRec->PaidAmt1, apTokens[TC_AMT_PAID1]);
      if (*apTokens[TC_DUE_PAID_DT1] >= '0' && dateConversion(apTokens[TC_DUE_PAID_DT1], acTmp, MM_DD_YYYY_1))
         strcpy(pBaseRec->PaidDate1, acTmp);
   } else if (*apTokens[TC_PAID_STAT1] == 'U')
   {
      pBaseRec->Inst1Status[0] = TAX_BSTAT_UNPAID;
      if (*apTokens[TC_DUE_PAID_DT1] >= '0' && dateConversion(apTokens[TC_DUE_PAID_DT1], acTmp, MM_DD_YYYY_1))
         strcpy(pBaseRec->DueDate1, acTmp);

      // Penalty
      dTmp = atof(apTokens[TC_PEN_DUE1]);
      if (dTmp > 0.0)
         sprintf(pBaseRec->PenAmt1, "%.2f", dTmp);
      else
         memset(pBaseRec->PenAmt1, 0, TAX_AMT);
   } else if (!memcmp(apTokens[TC_PAID_STAT1], "CA", 2))
   {
      pBaseRec->Inst1Status[0] = TAX_BSTAT_CANCEL;
   }

   if (!memcmp(apTokens[TC_PAID_STAT2], "PA", 2))
   {
      pBaseRec->Inst2Status[0] = TAX_BSTAT_PAID;
      strcpy(pBaseRec->PaidAmt2, apTokens[TC_AMT_PAID2]);
      if (*apTokens[TC_DUE_PAID_DT2] >= '0' && dateConversion(apTokens[TC_DUE_PAID_DT2], acTmp, MM_DD_YYYY_1))
         strcpy(pBaseRec->PaidDate2, acTmp);
      else
         LogMsg("*** Bad PaidDate 2: %s [%s]", apTokens[TC_DUE_PAID_DT2], apTokens[TC_APN_S]);
   } else if (!memcmp(apTokens[TC_PAID_STAT2], "PE", 2))
   {
      pBaseRec->Inst2Status[0] = TAX_BSTAT_PENDING;
      strcpy(pBaseRec->PaidAmt2, apTokens[TC_AMT_PAID2]);
      if (*apTokens[TC_DUE_PAID_DT2] >= '0' && dateConversion(apTokens[TC_DUE_PAID_DT2], acTmp, MM_DD_YYYY_1))
         strcpy(pBaseRec->PaidDate2, acTmp);
   } else if (*apTokens[TC_PAID_STAT2] == 'U')
   {
      pBaseRec->Inst2Status[0] = TAX_BSTAT_UNPAID;
      if (*apTokens[TC_DUE_PAID_DT2] >= '0' && dateConversion(apTokens[TC_DUE_PAID_DT2], acTmp, MM_DD_YYYY_1))
         strcpy(pBaseRec->DueDate2, acTmp);

      dTmp = atof(apTokens[TC_PEN_DUE2]);
      if (dTmp > 0.0)
         sprintf(pBaseRec->PenAmt2, "%.2f", dTmp);
      else
         memset(pBaseRec->PenAmt2, 0, TAX_AMT);
   } else if (!memcmp(apTokens[TC_PAID_STAT2], "CA", 2))
   {
      pBaseRec->Inst2Status[0] = TAX_BSTAT_CANCEL;
   } 

   // Fee Paid
   dTmp = atof(apTokens[TC_OTHER_FEES]);
   if (dTmp > 0.0)
      sprintf(pBaseRec->TotalFees, "%.2f", dTmp);

   // Calculate unpaid amt
   dTmp = atof(apTokens[TC_TOTAL_DUE]);
   if (dTmp > 0.0)
      sprintf(pBaseRec->TotalDue, "%.2f", dTmp);

   lTaxMatch++;

   // Get next record
   pRec = fgets(acRec, MAX_RECSIZE, fd);
   if (!pRec)
   {
      fclose(fd);
      fd = NULL;
   }
   bNoParse = false;

   return 0;
}

/***************************** Sac_ParseCortac ******************************
 *
 * Prepare for SAC
 *
 * Return 0 if success
 *
 ****************************************************************************/

int Sac_ParseCortac(char *pOutbuf, char *pInbuf)
{
   int      iRet, iTRA;
   long     taxYear;
   double	dTax1, dTax2, dTaxTotal, dPen1, dPen2, dDue1, dDue2;

   TAXBASE     *pOutRec = (TAXBASE *)pOutbuf;
   SAC_CORTAC  *pInRec  = (SAC_CORTAC *)pInbuf;

   // Clear output buffer
   memset(pOutbuf, 0, sizeof(TAXBASE));

   // Tax Year
   taxYear = 2000+atoin(pInRec->AsmtYear, TSIZ_YEAR);
   if (taxYear != lLienYear)
      return -1;

   sprintf(pOutRec->TaxYear, "%d", taxYear);

   // Defaulted year
   if (pInRec->DfltYear[0] >  ' ')
      sprintf(pOutRec->DelqYear, "20%.2s", pInRec->DfltYear);

   // APN
   memcpy(pOutRec->Apn, pInRec->Apn, TSIZ_APN);

   // Bill Number
   memcpy(pOutRec->BillNum, pInRec->BillNum, TSIZ_BILLNUM);

   // TRA
   if (pInRec->TRA[0] > ' ')
   {
      iTRA = atoin(pInRec->TRA, TSIZ_TRA);
      sprintf(pOutRec->TRA, "%.6d", iTRA);

      // Find tax rate
      if (iNumTRADist > 0)
      {
         TRADIST *pResult = findTRADist(pOutRec->TRA);
         if (pResult)
            strcpy(pOutRec->TotalRate, pResult->TaxRate);
         else
            LogMsg("Invalid TRA: %s", pOutRec->TRA);
      }
   }

   // Due date
   iRet = atoin(pInRec->Inst1_DueDate, 6);
   if (iRet > 0)
   {
      sprintf(pOutRec->DueDate1, "20%.6s", pInRec->Inst1_DueDate);
      sprintf(pOutRec->DueDate2, "20%.6s", pInRec->Inst2_DueDate);
   } else
   {
      InstDueDate(pOutRec->DueDate1, 1, taxYear);
      InstDueDate(pOutRec->DueDate2, 2, taxYear);
   }

#ifdef _DEBUG
   //if (!memcmp(pOutRec->Apn, "0010011003", 10))
   //   dDue1 = 0;
#endif

   // Check for Tax amount
   dTax1 = atoin(pInRec->TaxAmt1, TSIZ_AMT)/100.0;
   dTax2 = atoin(pInRec->TaxAmt2, TSIZ_AMT)/100.0;
   dDue1 = dDue2 = dPen1 = dPen2 = 0.0;
   if (pInRec->PaidStatus1 == ' ')
   {
      dDue1 = dTax1;
      if (ChkDueDate(1, pOutRec->DueDate1))
      {
         dPen1 = atoin(pInRec->PenAmt1, TSIZ_AMT)/100.0;
         dDue1 += dPen1;
         sprintf(pOutRec->PenAmt1, "%.2f", dPen1);
         pOutRec->PaidStatus[0] = TAX_BSTAT_PASTDUE; 
      } else
         pOutRec->Inst1Status[0] = TAX_BSTAT_UNPAID;
   } else if (pInRec->PaidStatus1 == 'P' || pInRec->PaidStatus1 == '&')
   {
      pOutRec->Inst1Status[0] = TAX_BSTAT_PAID;
      sprintf(pOutRec->PaidAmt1, "%.2f", dTax1);
   } else if (pInRec->PaidStatus1 == 'L' || pInRec->PaidStatus1 == 'Z')
      pOutRec->Inst1Status[0] = TAX_BSTAT_CANCEL;
   else if (pInRec->PaidStatus1 == 'R')
      pOutRec->Inst1Status[0] = TAX_BSTAT_INSTPMNT;   // Installment payment
   else
   {
      pOutRec->PaidStatus[0] = pInRec->PaidStatus1;
      LogMsg("??? Questionable paid status1 on APN= %s:%c", pOutRec->Apn, pInRec->PaidStatus1);
   }

   // If payment reverse, it considered not paid
   if (pInRec->PaidStatus2 == ' ')
   {
      dDue2 = dTax2;
      if (ChkDueDate(2, pOutRec->DueDate2))
      {
         dPen2 = atoin(pInRec->PenAmt2, TSIZ_AMT)/100.0;
         dDue2 += dPen2;
         sprintf(pOutRec->PenAmt2, "%.2f", dPen2);
         pOutRec->Inst2Status[0] = TAX_BSTAT_PASTDUE; // Past due
      } else
         pOutRec->Inst2Status[0] = TAX_BSTAT_UNPAID; 
   } else if (pInRec->PaidStatus2 == 'P' || pInRec->PaidStatus2 == '&')
   {
      pOutRec->Inst2Status[0] = TAX_BSTAT_PAID;
      sprintf(pOutRec->PaidAmt2, "%.2f", dTax2);
   } else if (pInRec->PaidStatus2 == 'L' || pInRec->PaidStatus2 == 'Z')
      pOutRec->Inst2Status[0] = TAX_BSTAT_CANCEL;
   else if (pInRec->PaidStatus2 == 'R')
      pOutRec->Inst2Status[0] = TAX_BSTAT_INSTPMNT;   // Installment payment
   else
   {
      pOutRec->Inst2Status[0] = pInRec->PaidStatus2;
      LogMsg("??? Questionable paid status2 on APN= %s:%c", pOutRec->Apn, pInRec->PaidStatus2);
   }

   // Total tax
   dTaxTotal = atoin(pInRec->BillAmt, TSIZ_AMT)/100.0;

#ifdef _DEBUG
   //if ((int)dTaxTotal != (int)(dTax1+dTax2))
   //   iRet = 0;
#endif
   if (dTaxTotal > 0.0)
   {
      sprintf(pOutRec->TaxAmt1, "%.2f", dTax1);
      sprintf(pOutRec->TaxAmt2, "%.2f", dTax2);
      sprintf(pOutRec->TotalTaxAmt, "%.2f", dTaxTotal);
   }

   // Total due
   dTaxTotal = dDue1+dDue2;
   if (dTaxTotal > 0.0)
      sprintf(pOutRec->TotalDue, "%.2f", dTaxTotal);

   pOutRec->isSecd[0] = '0';
   pOutRec->isSupp[0] = '0';

   iRet = atoin(pInRec->AccessType, TSIZ_TYPE);
   switch (iRet)
   {
      case 0:
      case 3:
         pOutRec->isSecd[0] = '1';
         pOutRec->BillType[0] = BILLTYPE_SECURED;
         break;
      case 1:
      case 2:
      case 11:
         pOutRec->isSupp[0] = '1';
         pOutRec->BillType[0] = BILLTYPE_SECURED_SUPPL;
         break;
      default:
         LogMsg("*** Unhandle AccessType: %.2s on %s", pInRec->AccessType, pOutRec->Apn);
   }

   // Update date
   sprintf(pOutRec->Upd_Date, "%d", lLastTaxFileDate);

   return 0;
}

/***************************** Sac_ParseTaxDetail ****************************
 *
 * Generate TAXDETAIL & TAXAGENCY
 *
 * Return number of detail records created if success
 *
 *****************************************************************************/

int Sac_ParseTaxDetail(char *pInbuf, FILE *fdDetail, FILE *fdAgency)
{
   char     acTmp[512], acDetail[1024], acAgency[1024];
   int      iCnt, iIdx;
   double   dTmp;

   TAXDETAIL   *pDetail = (TAXDETAIL *)&acDetail[0];
   TAXAGENCY   *pResult, *pAgency = (TAXAGENCY *)&acAgency[0];
   SAC_CORTAC  *pInRec  = (SAC_CORTAC *)pInbuf;

   // Clear output buffer
   memset(acDetail, 0, sizeof(TAXDETAIL));

   // APN
   memcpy(pDetail->Apn, pInRec->Apn, TSIZ_APN);

   // Bill Number
   memcpy(pDetail->BillNum, pInRec->BillNum, TSIZ_BILLNUM);

   // Tax Year
   sprintf(pDetail->TaxYear, "%d", lTaxYear);

   // Base amt
   iIdx = 1;
   for (iCnt = 0; iCnt < TSIZ_MAX_BASE; iCnt++)
   {
      dTmp =  atoin(pInRec->BaseAmt[iCnt], TSIZ_AMT)/100.0;
      if (dTmp > 0.0)
      {
         sprintf(pDetail->TaxAmt, "%.2f", dTmp);
         sprintf(pDetail->TaxCode, "%.4d", iIdx++);
         // Generate csv line and write to file
         Tax_CreateDetailCsv(acTmp, pDetail);
         fputs(acTmp, fdDetail);

         // Tax Desc
         pResult = findTaxAgency(pDetail->TaxCode);
         if (pResult)
         {
            memset(acAgency, 0, sizeof(TAXAGENCY));
            strcpy(pAgency->Code,  pResult->Code);
            strcpy(pAgency->Agency,pResult->Agency);
            Tax_CreateAgencyCsv(acTmp, (TAXAGENCY *)&acAgency);
            fputs(acTmp, fdAgency);
         }
      } else
         break;
   }

   // Loop through tax items
   for (iCnt = 0; iCnt < TSIZ_MAX_LEVIES && memcmp(pInRec->asLevies[iCnt].LevyNum, "0000", 4); iCnt++)
   {      
      // Tax Amt
      dTmp =  atoin(pInRec->asLevies[iCnt].LevyAmt, TSIZ_AMT)/100.0;
      if (dTmp > 0.0)
      {
         sprintf(pDetail->TaxAmt, "%.2f", dTmp);

         // Agency 
         memcpy(pDetail->TaxCode, pInRec->asLevies[iCnt].LevyNum, TSIZ_LEVYNUM);

         // Generate csv line and write to file
         Tax_CreateDetailCsv(acTmp, pDetail);
         fputs(acTmp, fdDetail);

         // Tax Desc
         pResult = findTaxAgency(pDetail->TaxCode);
         if (pResult)
         {
            memset(acAgency, 0, sizeof(TAXAGENCY));
            strcpy(pAgency->Code,  pResult->Code);
            strcpy(pAgency->Agency,pResult->Agency);
            strcpy(pAgency->Phone, pResult->Phone);
            Tax_CreateAgencyCsv(acTmp, (TAXAGENCY *)&acAgency);
            fputs(acTmp, fdAgency);
         } else
            LogMsg("*** Unknown Agency: %s [%s]", pDetail->TaxCode, pDetail->Apn);
      }
   }

   return iCnt;
}

/****************************** Sac_UpdateTaxDelq *****************************
 *
 * Update Tax Delq using RDMJ3410-2.txt.  If DefaultNo is different from Base, update Base record too.
 *
 * Update fields: DefaultDate, DefaultAmt, Redeem flag, DefaultNo
 *
 ******************************************************************************/

int Sac_UpdateTaxDelq(char *pBaseBuf, char *pDelqBuf, FILE *fd)
{
   static   char  acRec[MAX_RECSIZE], *pRec=NULL;
   int      iLoop, iTmp;
   double   dTmp;

   TAXBASE *pBaseRec = (TAXBASE *)pBaseBuf;
   TAXDELQ *pDelqRec = (TAXDELQ *)pDelqBuf;
   SAC_RDM *pInRec   = (SAC_RDM *)acRec;

   if (!pRec)
      pRec = fgets(acRec, MAX_RECSIZE, fd);        // Get first rec

   do
   {
      // Compare Apn
      iLoop = memcmp(pBaseRec->Apn, pInRec->Apn, iApnLen);
      if (iLoop > 0)
      {
         pRec = fgets(acRec, MAX_RECSIZE, fd);
         if (!pRec)
         {
            fclose(fd);
            fd = NULL;
            return 1;      // EOF
         }
      }
   } while (iLoop > 0);

   if (iLoop)
      return 1;

   // Check DefaultNo
   //if (memcmp(pDelqRec->Default_No, pInRec->DefaultNo, TSIZ_DEFAULTNO-1))
   //{
   //   LogMsg0("*** Sold4Tax: DefaultNo not matched: %s vs %.7s [%s]", pDelqRec->Default_No, pInRec->DefaultNo, pDelqRec->Apn);
   //   memcpy(pDelqRec->Default_No, pInRec->DefaultNo, TSIZ_DEFAULTNO);
   //}

   memset(pDelqRec, 0, sizeof(TAXDELQ));

#ifdef _DEBUG
   //if (!memcmp(pBaseRec->Apn, "00102000400000", iApnLen))
   //   iTmp = 0;
#endif

   strcpy(pDelqRec->Apn, pBaseRec->Apn);
   strcpy(pDelqRec->TaxYear, pBaseRec->TaxYear);
   strcpy(pDelqRec->Upd_Date, pBaseRec->Upd_Date);
   sprintf(pDelqRec->Def_Date, "20%.6s", pInRec->Def_Date);
   memcpy(pBaseRec->DelqYear, pDelqRec->Def_Date, 4);
   memcpy(pDelqRec->Default_No, pInRec->Def_Num, TSIZ_DFLTNUM);

   // Updated date
   sprintf(pDelqRec->Upd_Date, "%d", lLastTaxFileDate);

   // Update DefAmt
   dTmp = Zone2Dbl(pInRec->DueAmt, TSIZ_AMT);
   if (dTmp > 0)
      sprintf(pDelqRec->Def_Amt, "%.2f", dTmp);

   // Penalty
   dTmp = Zone2Dbl(pInRec->PenAmt, TSIZ_AMT);
   if (dTmp > 0)
      sprintf(pDelqRec->Pen_Amt, "%.2f", dTmp);

   // Fee
   dTmp  = Zone2Dbl(pInRec->StateFee, TSIZ_FEE);
   dTmp += Zone2Dbl(pInRec->LienSrchFee, TSIZ_FEE);
   dTmp += Zone2Dbl(pInRec->RelFee, TSIZ_FEE);
   if (dTmp > 0)
      sprintf(pDelqRec->Fee_Amt, "%.2f", dTmp);

   // Default Status - 30=open,31=cancel,32=paid,35=corrd,38=minbill
   iTmp = atoin(pInRec->Def_Stat, TSIZ_TYPE);
   if (iTmp < 31 || iTmp > 32)
   {
      memcpy(pBaseRec->Def_Date, pDelqRec->Def_Date, 8);
      memcpy(pBaseRec->DelqYear, pDelqRec->Def_Date, 4);
      pBaseRec->isDelq[0] = '1';
      pDelqRec->isDelq[0] = '1';
      pDelqRec->DelqStatus[0] = '7';
      pBaseRec->DelqStatus[0] = '7';
   } else 
   {
      if (iTmp == 31)
      {
         pDelqRec->DelqStatus[0] = '5';
         pBaseRec->DelqStatus[0] = '5';
      } else
      {
         pDelqRec->DelqStatus[0] = '1';
         pBaseRec->DelqStatus[0] = '1';
      }
      pDelqRec->isDelq[0] = 0;
      pBaseRec->isDelq[0] = 0;
   }

   // Get next record
   pRec = fgets(acRec, MAX_RECSIZE, fd);
   if (!pRec)
   {
      fclose(fd);
      fd = NULL;
   }

   return 0;
}

/**************************** Sac_Load_Cortac ******************************
 *
 * Create import file from CortacFile and import into SQL Tax Base table
 * Do not output cancelled bill.
 *
 * 08/19/2021 Remove update using TC file.
 *
 * Return 0 if success.
 *
 *****************************************************************************/

int Sac_Load_Cortac(bool bImport)
{
   char     *pTmp, acDetailFile[_MAX_PATH], acAgencyFile[_MAX_PATH], acDelqFile[_MAX_PATH],
            acTmpFile[_MAX_PATH], acBaseFile[_MAX_PATH], acInFile[_MAX_PATH], acOutbuf[MAX_RECSIZE],
            acBase[1024], acRec[MAX_RECSIZE], acDelq[1024];
   int      iRet, iDrop=0, iCancel=0;
   long     lBase=0, lCnt=0;
   FILE     *fdBase, *fdIn, *fdDetail, *fdAgency, *fdRdm, *fdDelq;

   TAXBASE     *pBase = (TAXBASE *)acBase;
   SAC_CORTAC  *pInRec  = (SAC_CORTAC *)acRec;

   LogMsg0("Loading Tax for SAC");
   NameTaxCsvFile(acBaseFile, myCounty.acCntyCode, "Base");
   NameTaxCsvFile(acDetailFile, myCounty.acCntyCode, "Items");
   NameTaxCsvFile(acAgencyFile, myCounty.acCntyCode, "Agency");
   NameTaxCsvFile(acDelqFile, myCounty.acCntyCode, "Delq");
   pTmp = strrchr(acBaseFile, '\\');
   *pTmp = 0;
   if (_access(acBaseFile, 0))
      _mkdir(acBaseFile);
   *pTmp = '\\';

   // Open input file
   GetIniString(myCounty.acCntyCode, "TaxRollInfo", "", acInFile, _MAX_PATH, acIniFile);
   lLastTaxFileDate = getFileDate(acInFile);
   iRet = isNewTaxFile(NULL, myCounty.acCntyCode, lLastTaxFileDate);
   if (iRet <= 0)
   {
      lLastTaxFileDate = 0;
      return iRet;
   }

   // Sort on APN, BillNum - ignore record without tax year
   sprintf(acBase, "%s\\%s\\%s_Cortac.srt", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   if (chkFileDate(acInFile, acBase) != 2)
      iRet = sortFile(acInFile, acBase, "S(1,14,C,A,663,8,C,A)");   
   LogMsg("Open Secured tax file %s", acBase);
   fdIn = fopen(acBase, "r");
   if (fdIn == NULL)
   {
      LogMsg("***** Error opening Secured tax file: %s\n", acBase);
      return -2;
   }  

   // Delinquent file
   GetIniString(myCounty.acCntyCode, "Redemption", "", acInFile, _MAX_PATH, acIniFile);
   sprintf(acRec, "%s\\%s\\Redemption.srt", acTmpPath, myCounty.acCntyCode);
   // Sort APN ascending, Default date descending
   iRet = sortFile(acInFile, acRec, "S(1,14,C,A) ", &iRet);
   if (iRet > 0)
   {
      LogMsg("Open delinquent file #1 %s", acRec);
      fdRdm = fopen(acRec, "r");
      if (fdRdm == NULL)
      {
         LogMsg("***** Error opening delinquent file #1: %s\n", acRec);
         return -2;
      }  
   } else
      return -2;

   // Open Output file
   LogMsg("Create Base file %s", acBaseFile);
   fdBase = fopen(acBaseFile, "w");
   if (fdBase == NULL)
   {
      LogMsg("***** Error creating output file: %s\n", acBaseFile);
      return -4;
   }

   // Open Detail file
   LogMsg("Open Detail output file %s", acDetailFile);
   fdDetail = fopen(acDetailFile, "w");
   if (fdDetail == NULL)
   {
      LogMsg("***** Error creating Detail output file: %s\n", acDetailFile);
      return -4;
   }

   // Open Agency file
   sprintf(acTmpFile, "%s\\%s\\%s_Agency.tmp", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   LogMsg("Create Agency file %s", acTmpFile);
   fdAgency = fopen(acTmpFile, "w");
   if (fdAgency == NULL)
   {
      LogMsg("***** Error creating Agency file: %s\n", acTmpFile);
      return -4;
   }

   // Create import Delq file
   LogMsg("Open Delq file %s", acDelqFile);
   fdDelq = fopen(acDelqFile, "w");
   if (fdDelq == NULL)
   {
      LogMsg("***** Error creating Delq file: %s\n", acDelqFile);
      return -4;
   }

   // Merge loop 
   lTaxSkip=lTaxMatch=0;
   while (!feof(fdIn))
   {
      pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdIn);
      if (!pTmp || *pTmp < '0')
         break;

      if (pInRec->PaidStatus1 == 'L' || pInRec->PaidStatus1 == 'Z')
      {
         iCancel++;
         lCnt++;
         continue;
      }

      // Create new base record
      iRet = Sac_ParseCortac(acBase, acRec);
      if (!iRet)
      {
         // Create Delq record
         if (fdRdm)
         {
            iRet = Sac_UpdateTaxDelq(acBase, acDelq, fdRdm);
            if (!iRet)
            {
               Tax_CreateDelqCsv(acOutbuf, (TAXDELQ *)&acDelq);
               fputs(acOutbuf, fdDelq);
            }
         }

         // Create detail & agency records
         Sac_ParseTaxDetail(acRec, fdDetail, fdAgency);

         // Create TaxBase record
         Tax_CreateTaxBaseCsv(acOutbuf, (TAXBASE *)&acBase);

         // Output record			
         lBase++;
         fputs(acOutbuf, fdBase);
      } else
      {
         if (bDebug)
            LogMsg("---> Drop record [APN=%.13s], [AsmtYear=%.2s]", pInRec->Apn, pInRec->AsmtYear); 
         iDrop++;
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdIn)
      fclose(fdIn);
   if (fdRdm)
      fclose(fdRdm);
   if (fdDelq)
      fclose(fdDelq);
   if (fdBase)
      fclose(fdBase);
   if (fdDetail)
      fclose(fdDetail);
   if (fdAgency)
      fclose(fdAgency);

   printf("\nTotal records processed:     %u\n", lCnt);
   LogMsg("Total records processed:       %u", lCnt);
   LogMsg("Total base records output:     %u", lBase);

   if (iDrop > 0 || iCancel > 0)
   {
      LogMsg("Total CORTAC records dropped:  %u", iDrop);
      LogMsg("Total cancelled records:       %u", iCancel);
   }

   // Import into SQL
   if (bImport)
   {
      iRet = doTaxImport(myCounty.acCntyCode, TAX_BASE);
      if (!iRet)
         iRet = doTaxImport(myCounty.acCntyCode, TAX_DETAIL);
      if (!iRet)
      {
         // Dedup Agency file before import
         LogMsg("Dedup Agency file %s to %s", acTmpFile, acAgencyFile);
         iRet = sortFile(acTmpFile, acAgencyFile, "S(#1,C,A) DUPOUT F(TXT)");
         if (iRet > 0)
            iRet = doTaxImport(myCounty.acCntyCode, TAX_AGENCY);
      }
      if (!iRet)
         iRet = doTaxImport(myCounty.acCntyCode, TAX_DELINQUENT);
   } else
      iRet = 0;

   return iRet;
}

/**************************** Sac_Update_TaxBase *****************************
 *
 * Update TaxBase using 20220722_TAXJ2710.iwr file.  Import into Tmp table then update Tax_Base
 *
 * Return 0 if success.
 *
 *****************************************************************************/

int Sac_Update_TaxBase(bool bImport)
{
   char     *pTmp, acBaseFile[_MAX_PATH], acInFile[_MAX_PATH], acOutbuf[MAX_RECSIZE],
            acBase[1024], acRec[MAX_RECSIZE];
   int      iRet, iDrop=0, iCancel=0;
   long     lBase=0, lCnt=0;
   FILE     *fdBase, *fdIn;

   TAXBASE     *pBase = (TAXBASE *)acBase;
   SAC_CORTAC  *pInRec  = (SAC_CORTAC *)acRec;

   LogMsg0("Loading Tax Base for SAC");
   NameTaxCsvFile(acBaseFile, "TmpSac", "Base");
   pTmp = strrchr(acBaseFile, '\\');
   *pTmp = 0;
   if (_access(acBaseFile, 0))
      _mkdir(acBaseFile);
   *pTmp = '\\';

   // Open input file
   GetIniString(myCounty.acCntyCode, "TaxRollInfo", "", acInFile, _MAX_PATH, acIniFile);
   lLastTaxFileDate = getFileDate(acInFile);
   iRet = isNewTaxFile(NULL, myCounty.acCntyCode, lLastTaxFileDate);
   if (iRet <= 0)
   {
      lLastTaxFileDate = 0;
      return iRet;
   }

   // Sort on APN, BillNum - ignore record without tax year
   sprintf(acBase, "%s\\%s\\%s_Cortac.srt", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   if (chkFileDate(acInFile, acBase) != 2)
      iRet = sortFile(acInFile, acBase, "S(1,14,C,A,663,8,C,A)");   
   LogMsg("Open Secured tax file %s", acBase);
   fdIn = fopen(acBase, "r");
   if (fdIn == NULL)
   {
      LogMsg("***** Error opening Secured tax file: %s\n", acBase);
      return -2;
   }  

   // Open import Base file
   LogMsg("Create Base file %s", acBaseFile);
   fdBase = fopen(acBaseFile, "w");
   if (fdBase == NULL)
   {
      LogMsg("***** Error creating output file: %s\n", acBaseFile);
      return -4;
   }

   // Merge loop 
   lTaxSkip=lTaxMatch=0;
   while (!feof(fdIn))
   {
      pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdIn);
      if (!pTmp || *pTmp < '0')
         break;

      if (pInRec->PaidStatus1 == 'L' || pInRec->PaidStatus1 == 'Z')
      {
         iCancel++;
         lCnt++;
         continue;
      }

      // Create new base record
      iRet = Sac_ParseCortac(acBase, acRec);
      if (!iRet)
      {
         // Create TaxBase record
         Tax_CreateTaxBaseCsv(acOutbuf, (TAXBASE *)&acBase);

         // Output record			
         lBase++;
         fputs(acOutbuf, fdBase);
      } else
      {
         if (bDebug)
            LogMsg("---> Drop record [APN=%.13s], [AsmtYear=%.2s]", pInRec->Apn, pInRec->AsmtYear); 
         iDrop++;
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdIn)
      fclose(fdIn);
   if (fdBase)
      fclose(fdBase);

   printf("\nTotal records processed:     %u\n", lCnt);
   LogMsg("Total records processed:       %u", lCnt);
   LogMsg("Total base records output:     %u", lBase);

   if (iDrop > 0 || iCancel > 0)
   {
      LogMsg("Total CORTAC records dropped:  %u", iDrop);
      LogMsg("Total cancelled records:       %u", iCancel);
   }

   // Import into SQL
   if (bImport)
      iRet = doTaxImport("TmpSac", TAX_BASE);
   else
      iRet = 0;

   return iRet;
}

/**************************** Sac_Update_TaxDelq *****************************
 *
 * Update TaxDelq using 20220723_RDMJ3410.txt file.  Import into Tmp table then update Tax_Delq
 *
 * Return 0 if success.
 *
 *****************************************************************************/

int Sac_CreateTaxDelqRec(char *pInBuf, char *pDelqBuf)
{
   static   char  acRec[MAX_RECSIZE], *pRec=NULL;
   int      iTmp;
   double   dTmp;

   TAXDELQ *pDelqRec = (TAXDELQ *)pDelqBuf;
   SAC_RDM *pInRec   = (SAC_RDM *)pInBuf;

   memset(pDelqRec, 0, sizeof(TAXDELQ));

#ifdef _DEBUG
   //if (!memcmp(pBaseRec->Apn, "00102000400000", iApnLen))
   //   iTmp = 0;
#endif

   memcpy(pDelqRec->Apn, pInRec->Apn, iApnLen);
   sprintf(pDelqRec->TaxYear, "%d", lTaxYear);
   sprintf(pDelqRec->Def_Date, "20%.6s", pInRec->Def_Date);
   memcpy(pDelqRec->Default_No, pInRec->Def_Num, TSIZ_DFLTNUM);

   // Updated date
   sprintf(pDelqRec->Upd_Date, "%d", lLastTaxFileDate);

   // Update DefAmt
   dTmp = Zone2Dbl(pInRec->DueAmt, TSIZ_AMT);
   if (dTmp > 0)
      sprintf(pDelqRec->Def_Amt, "%.2f", dTmp);

   // Penalty
   dTmp = Zone2Dbl(pInRec->PenAmt, TSIZ_AMT);
   if (dTmp > 0)
      sprintf(pDelqRec->Pen_Amt, "%.2f", dTmp);

   // Fee
   dTmp  = Zone2Dbl(pInRec->StateFee, TSIZ_FEE);
   dTmp += Zone2Dbl(pInRec->LienSrchFee, TSIZ_FEE);
   dTmp += Zone2Dbl(pInRec->RelFee, TSIZ_FEE);
   if (dTmp > 0)
      sprintf(pDelqRec->Fee_Amt, "%.2f", dTmp);

   // Default Status - 30=open,31=cancel,32=paid,35=corrd,38=minbill
   iTmp = atoin(pInRec->Def_Stat, TSIZ_TYPE);
   if (iTmp < 31 || iTmp > 32)
   {
      pDelqRec->isDelq[0] = '1';
      pDelqRec->DelqStatus[0] = '7';
   } else 
   {
      if (iTmp == 31)
         pDelqRec->DelqStatus[0] = '5';
      else
         pDelqRec->DelqStatus[0] = '1';
      pDelqRec->isDelq[0] = 0;
   }

   return 0;
}

/**************************** Sac_Load_TaxDelq *******************************
 *
 * Load TaxDelq file 20220723_RDMJ3410.txt
 * Return 0 if success.
 *
 *****************************************************************************/

int Sac_Load_TaxDelq(bool bImport)
{
   char     *pTmp, acDelqFile[_MAX_PATH], acInFile[_MAX_PATH], acOutbuf[MAX_RECSIZE], acRec[MAX_RECSIZE], acDelq[1024];
   int      iRet;
   long     lDelq=0, lCnt=0;
   FILE     *fdRdm, *fdDelq;

   LogMsg0("Loading Tax Delq for SAC");
   NameTaxCsvFile(acDelqFile, myCounty.acCntyCode, "Delq");

   // Delinquent file
   GetIniString(myCounty.acCntyCode, "Redemption", "", acInFile, _MAX_PATH, acIniFile);
   lLastTaxFileDate = getFileDate(acInFile);

   sprintf(acRec, "%s\\%s\\Redemption.srt", acTmpPath, myCounty.acCntyCode);
   // Sort APN ascending, Default date descending
   iRet = sortFile(acInFile, acRec, "S(1,14,C,A) ", &iRet);
   if (iRet > 0)
   {
      LogMsg("Open delinquent file #1 %s", acRec);
      fdRdm = fopen(acRec, "r");
      if (fdRdm == NULL)
      {
         LogMsg("***** Error opening delinquent file #1: %s\n", acRec);
         return -2;
      }  
   } else
      return -2;

   // Create import Delq file
   LogMsg("Open Delq file %s", acDelqFile);
   fdDelq = fopen(acDelqFile, "w");
   if (fdDelq == NULL)
   {
      LogMsg("***** Error creating Delq file: %s\n", acDelqFile);
      return -4;
   }

   // Merge loop 
   while (!feof(fdRdm))
   {
      pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdRdm);
      if (!pTmp || *pTmp < '0')
         break;

      // Create Delq record
      if (fdRdm)
      {
         iRet = Sac_CreateTaxDelqRec(acRec, acDelq);
         if (!iRet)
         {
            Tax_CreateDelqCsv(acOutbuf, (TAXDELQ *)&acDelq);
            fputs(acOutbuf, fdDelq);
            lDelq++;
         }
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdRdm)
      fclose(fdRdm);
   if (fdDelq)
      fclose(fdDelq);

   LogMsg("Total records processed:       %u", lCnt);
   LogMsg("Total delq records output:     %u", lDelq);

   // Import into SQL
   if (bImport)
      iRet = doTaxImport(myCounty.acCntyCode, TAX_DELINQUENT);
   else
      iRet = 0;

   return iRet;
}

//int Sac_Load_CortacX(bool bImport)
//{
//   char     *pTmp, acDetailFile[_MAX_PATH], acAgencyFile[_MAX_PATH], acDelqFile[_MAX_PATH],
//            acTmpFile[_MAX_PATH], acBaseFile[_MAX_PATH], acInFile[_MAX_PATH], acOutbuf[MAX_RECSIZE],
//            acBase[1024], acRec[MAX_RECSIZE], acDelq[1024], acTCFile[_MAX_PATH];
//   int      iRet, iDrop=0, iCancel=0;
//   long     lBase=0, lCnt=0;
//   FILE     *fdBase, *fdIn, *fdTC, *fdDetail, *fdAgency, *fdRdm, *fdDelq;
//
//   TAXBASE     *pBase = (TAXBASE *)acBase;
//   SAC_CORTAC  *pInRec  = (SAC_CORTAC *)acRec;
//
//   LogMsg0("Loading Tax for SAC");
//   sprintf(acBaseFile, sTaxOutTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "Base");
//   sprintf(acDetailFile, sTaxOutTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "Items");
//   sprintf(acAgencyFile, sTaxOutTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "Agency");
//   sprintf(acDelqFile, sTaxOutTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "Delq");
//   pTmp = strrchr(acBaseFile, '\\');
//   *pTmp = 0;
//   if (_access(acBaseFile, 0))
//      _mkdir(acBaseFile);
//   *pTmp = '\\';
//
//   // Get TC file
//   GetIniString(myCounty.acCntyCode, "Main_TC", "", acTCFile, _MAX_PATH, acIniFile);
//   lTCDate = getFileDate(acTCFile);
//
//   // Open input file
//   GetIniString(myCounty.acCntyCode, "TaxRollInfo", "", acInFile, _MAX_PATH, acIniFile);
//   lLastTaxFileDate = getFileDate(acInFile);
//   if (lLastTaxFileDate < lTCDate)
//      iRet = isNewTaxFile(NULL, myCounty.acCntyCode, lTCDate);
//   else
//      iRet = isNewTaxFile(NULL, myCounty.acCntyCode, lLastTaxFileDate);
//   if (iRet <= 0)
//   {
//      lLastTaxFileDate = 0;
//      return iRet;
//   }
//
//   // Sort on APN, BillNum - ignore record without tax year
//   sprintf(acBase, "%s\\%s\\%s_Cortac.srt", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
//   if (chkFileDate(acInFile, acBase) != 2)
//      iRet = sortFile(acInFile, acBase, "S(1,14,C,A,663,8,C,A)");   
//   LogMsg("Open Secured tax file %s", acBase);
//   fdIn = fopen(acBase, "r");
//   if (fdIn == NULL)
//   {
//      LogMsg("***** Error opening Secured tax file: %s\n", acBase);
//      return -2;
//   }  
//
//   // Open TC file - sort on APN, BillNum
//   if (lTCDate > lLastTaxFileDate)
//   {
//      sprintf(acBase, "%s\\%s\\%s_TC.srt", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
//      if (chkFileDate(acTCFile, acBase) != 2)
//         iRet = sortFile(acTCFile, acBase, "S(#1,C,A, #6,C,A) OMIT(#5,C,LE,\"0\") DEL(124) ");   
//      LogMsg("Open TC file %s (%d records)", acBase, iRet-1);
//      fdTC = fopen(acBase, "r");
//      if (fdTC == NULL)
//      {
//         LogMsg("***** Error opening Current TC file: %s\n", acBase);
//         return -2;
//      }  
//   } else
//      // TC file not always available so we cannot rely on it.
//      fdTC = NULL;
//
//   // Delinquent file
//   GetIniString(myCounty.acCntyCode, "Redemption", "", acInFile, _MAX_PATH, acIniFile);
//   sprintf(acRec, "%s\\%s\\Redemption.srt", acTmpPath, myCounty.acCntyCode);
//   // Sort APN ascending, Default date descending
//   iRet = sortFile(acInFile, acRec, "S(1,14,C,A) ", &iRet);
//   if (iRet > 0)
//   {
//      LogMsg("Open delinquent file #1 %s", acRec);
//      fdRdm = fopen(acRec, "r");
//      if (fdRdm == NULL)
//      {
//         LogMsg("***** Error opening delinquent file #1: %s\n", acRec);
//         return -2;
//      }  
//   } else
//      return -2;
//
//   // Open Output file
//   LogMsg("Create Base file %s", acBaseFile);
//   fdBase = fopen(acBaseFile, "w");
//   if (fdBase == NULL)
//   {
//      LogMsg("***** Error creating output file: %s\n", acBaseFile);
//      return -4;
//   }
//
//   // Open Detail file
//   LogMsg("Open Detail output file %s", acDetailFile);
//   fdDetail = fopen(acDetailFile, "w");
//   if (fdDetail == NULL)
//   {
//      LogMsg("***** Error creating Detail output file: %s\n", acDetailFile);
//      return -4;
//   }
//
//   // Open Agency file
//   sprintf(acTmpFile, "%s\\%s\\%s_Agency.tmp", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
//   LogMsg("Create Agency file %s", acTmpFile);
//   fdAgency = fopen(acTmpFile, "w");
//   if (fdAgency == NULL)
//   {
//      LogMsg("***** Error creating Agency file: %s\n", acTmpFile);
//      return -4;
//   }
//
//   // Create import Delq file
//   LogMsg("Open Delq file %s", acDelqFile);
//   fdDelq = fopen(acDelqFile, "w");
//   if (fdDelq == NULL)
//   {
//      LogMsg("***** Error creating Delq file: %s\n", acDelqFile);
//      return -4;
//   }
//
//   // Merge loop 
//   lTaxSkip=lTaxMatch=0;
//   while (!feof(fdIn))
//   {
//      pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdIn);
//      if (!pTmp || *pTmp < '0')
//         break;
//
//      if (pInRec->PaidStatus1 == 'L' || pInRec->PaidStatus1 == 'Z')
//      {
//         iCancel++;
//         lCnt++;
//         continue;
//      }
//
//      // Create new base record
//      iRet = Sac_ParseCortac(acBase, acRec);
//      if (!iRet)
//      {
//         // Update TaxBase record using Sac.TC
//         if (fdTC)
//            Sac_UpdateTaxBase(acBase, fdTC);
//
//         // Create Delq record
//         if (fdRdm)
//         {
//            iRet = Sac_UpdateTaxDelq(acBase, acDelq, fdRdm);
//            if (!iRet)
//            {
//               Tax_CreateDelqCsv(acOutbuf, (TAXDELQ *)&acDelq);
//               fputs(acOutbuf, fdDelq);
//            }
//         }
//
//         // Create detail & agency records
//         Sac_ParseTaxDetail(acRec, fdDetail, fdAgency);
//
//         // Create TaxBase record
//         Tax_CreateTaxBaseCsv(acOutbuf, (TAXBASE *)&acBase);
//
//         // Output record			
//         lBase++;
//         fputs(acOutbuf, fdBase);
//      } else
//      {
//         LogMsg("---> Drop record %d [%.40s]", lCnt, acRec); 
//         iDrop++;
//      }
//
//      if (!(++lCnt % 1000))
//         printf("\r%u", lCnt);
//   }
//
//   // Close files
//   if (fdIn)
//      fclose(fdIn);
//   if (fdTC)
//      fclose(fdTC);
//   if (fdRdm)
//      fclose(fdRdm);
//   if (fdDelq)
//      fclose(fdDelq);
//   if (fdBase)
//      fclose(fdBase);
//   if (fdDetail)
//      fclose(fdDetail);
//   if (fdAgency)
//      fclose(fdAgency);
//
//   printf("\nTotal records processed:     %u\n", lCnt);
//   LogMsg("Total records processed:   %u", lCnt);
//   LogMsg("Total base records output: %u", lBase);
//   //LogMsg("        Skip TC records:   %u", lTaxSkip);
//   //LogMsg("       TC records match:   %u", lTaxMatch);
//
//   if (iDrop > 0 || iCancel > 0)
//   {
//      LogMsg("Total CORTAC records dropped: %u", iDrop);
//      LogMsg("Total cancelled records:      %u", iCancel);
//   }
//
//   // Import into SQL
//   if (bImport)
//   {
//      iRet = doTaxImport(myCounty.acCntyCode, TAX_BASE);
//      if (!iRet)
//         iRet = doTaxImport(myCounty.acCntyCode, TAX_DETAIL);
//      if (!iRet)
//      {
//         // Dedup Agency file before import
//         LogMsg("Dedup Agency file %s to %s", acTmpFile, acAgencyFile);
//         iRet = sortFile(acTmpFile, acAgencyFile, "S(#1,C,A) DUPOUT F(TXT)");
//         if (iRet > 0)
//            iRet = doTaxImport(myCounty.acCntyCode, TAX_AGENCY);
//      }
//      if (!iRet)
//         iRet = doTaxImport(myCounty.acCntyCode, TAX_DELINQUENT);
//   } else
//      iRet = 0;
//
//   return iRet;
//}

/******************************* Sac_ConvStdChar *****************************
 *
 * Convert char file "gax2100_apprpubl.txt" to STD_CHAR format
 *
 *****************************************************************************/

int Sac_ConvStdChar(char *pInfile)
{
   char     acInbuf[1024], acOutbuf[2048], *pRec=NULL;
   char     acTmpFile[256], acTmp[256], acCode[32], cTmp1, cTmp2;
   long     lTmp, lSqft;
   int      iRet, iTmp, iBeds, iFBath, iHBath, iBath1Q, iCnt;
   double   dTmp;

   SAC_CHAR *pCharRec = (SAC_CHAR *)acInbuf;
   STDCHAR  *pStdChar = (STDCHAR *)acOutbuf;
   FILE     *fdIn, *fdOut;

   LogMsg("\nConverting standard char file %s\n", pInfile);
   if (!(fdIn = fopen(pInfile, "r")))
      return -1;

   sprintf(acTmpFile, "%s\\%s\\%s_char.tmp", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   if (!(fdOut = fopen(acTmpFile, "w")))
   {
      fclose(fdIn);
      LogMsg("***** Error creating output file %s", acTmpFile);
      return -2;
   }

   iCnt = 0;
   while (!feof(fdIn))
   {
      pRec = fgets(acInbuf, 1024, fdIn);
      if (!pRec) break;

      memset(acOutbuf, ' ', sizeof(STDCHAR));

      // Apn
      memcpy(pStdChar->Apn, pCharRec->Apn, ASIZ_APN);

      // Quality Class
      if (isalpha(pCharRec->QualityClass[0]))
      {  // D045A
         memcpy(pStdChar->QualityClass, pCharRec->QualityClass, ASIZ_QUALITY_CLASS);
         pStdChar->BldgClass = pCharRec->QualityClass[0];
         iTmp = atoin((char *)&pCharRec->QualityClass[1], 2);
         sprintf(acTmp, "%d.%c", iTmp, pCharRec->QualityClass[3]);
         iRet = Value2Code(acTmp, acCode, NULL);
         if (iRet >= 0)
            pStdChar->BldgQual = acCode[0];
      }

#ifdef _DEBUG
      //if (!memcmp(pCharRec->Apn, "0780221029", 10))
      //   iRet = 0;
#endif
      // Condition
      if (pCharRec->Imprv_Condition_Code > '0')
      {
         switch (pCharRec->Imprv_Condition_Code)
         {
            case '1':
               cTmp1 = 'P';    // Poor
               break;
            case '2':
               cTmp1 = 'F';    // Fair
               break;
            case '3':
               cTmp1 = 'A';    // Average
               break;
            case '4':
               cTmp1 = 'G';    // Good
               break;
            default:
               cTmp1 = ' ';
         }
         pStdChar->ImprCond[0] = cTmp1;
      }

      // YrBlt
      lTmp = atoin((char *)&pCharRec->YearBuilt[1], SIZ_YR_BLT);
      if (lTmp > 1700)
         memcpy(pStdChar->YrBlt, (char *)&pCharRec->YearBuilt[1], SIZ_YR_BLT);

      // YrEff
      lTmp = atoin((char *)&pCharRec->EffYear[1], SIZ_YR_BLT);
      if (lTmp > 1700)
         memcpy(pStdChar->YrEff, (char *)&pCharRec->EffYear[1], SIZ_YR_BLT);

      // Stories
      lTmp = atoin(pCharRec->Num_Stories, ASIZ_NUM_STORIES);
      if (lTmp > 0)
      {
         switch (lTmp)
         {
            case 1:
               dTmp = 1;
               break;
            case 2:
               dTmp = 2;
               break;
            case 3:
               dTmp = 1.5;
               break;
            case 4:
               dTmp = 2;
               break;
            default:
               dTmp = 0;
               LogMsg("%.*s : %d", iApnLen, pStdChar->Apn, lTmp);
         }
         if (dTmp > 0.0)
         {
            iTmp = sprintf(acTmp, "%.1f", dTmp);
            memcpy(pStdChar->Stories, acTmp, iTmp);
         }
      }

      // BldgSqft
      lSqft = atoin(pCharRec->Total_Res_Area, ASIZ_AREA_SQFT);
      if (lSqft > 10)
      {
         iTmp = sprintf(acTmp, "%d", lSqft);
         memcpy(pStdChar->BldgSqft, acTmp, iTmp);
      }

      // FloorSqft
      lSqft = atoin(pCharRec->Orig_1St_Flr_Area, ASIZ_AREA_SQFT);
      if (lSqft > 1)
      {
         iTmp = sprintf(acTmp, "%d", lSqft);
         memcpy(pStdChar->Sqft_1stFl, acTmp, iTmp);
      }
      lSqft = atoin(pCharRec->Orig_2Nd_Flr_Area, ASIZ_AREA_SQFT);
      if (lSqft > 1)
      {
         iTmp = sprintf(acTmp, "%d", lSqft);
         memcpy(pStdChar->Sqft_2ndFl, acTmp, iTmp);
      }

      // Garage Sqft
      lSqft = atoin(pCharRec->SqFTGarage, ASIZ_AREA_SQFT);
      if (lSqft > 10)
      {
         iTmp = sprintf(acTmp, "%d", lSqft);
         memcpy(pStdChar->GarSqft, acTmp, iTmp);
         // This is to cover where Garage_Spcs is 0
         pStdChar->ParkType[0] = 'Z';
      }

      // Garage spaces - Normally when there is park space and no garage Sqft
      // we translate it to "COVERED" or "GARAGE/CARPORT".  In SAC, I make it
      // "GARAGE" to be compatible with Fernando.
      if (pCharRec->Garage_Spcs > '0')
      {
         pStdChar->ParkType[0] = 'Z';                 // Garage
         pStdChar->ParkSpace[0] = pCharRec->Garage_Spcs;
      }

      // Central Heating-Cooling
      cTmp1=cTmp2=' ';
      if (pCharRec->Central_Ac_Heating > '0')
      {  // cTmp1 = Heat, cTmp2 = AC
         switch (pCharRec->Central_Ac_Heating)
         {
            //case '1':         // None
            //   cTmp1 = 'L';
            //   cTmp2 = 'N';
            //   break;
            case '2':
               cTmp1 = 'Z';   // Heat
               //cTmp2 = 'N';
               break;
            case '3':
               cTmp1 = 'Z';   // Heat and AC
               cTmp2 = 'C';
               break;
            default:
               break;
         }
         pStdChar->Heating[0] = cTmp1;
         pStdChar->Cooling[0] = cTmp2;
      }
      if (cTmp1 == ' ')
      {
         if (pCharRec->Solar_Space_Heater == '1')
            pStdChar->Heating[0] = 'K';
      }

      // Rooms
      lTmp = atoin(pCharRec->Total_Rooms, ASIZ_TOTAL_ROOMS);
      if (lTmp > 0)
      {
         iTmp = sprintf(acTmp, "%d", lTmp);
         memcpy(pStdChar->Rooms, acTmp, iTmp);
      }

      // Beds
      iBeds = atoin(pCharRec->NumBedrooms, ASIZ_NUM_BEDROOMS);
      if (iBeds > 0)
      {
         iTmp = sprintf(acTmp, "%*u", SIZ_BEDS, iBeds);
         memcpy(pStdChar->Beds, acTmp, iTmp);
      }

      // Bath
      if (pCharRec->NumBaths[0] > '0')
      {
#ifdef _DEBUG
         iFBath=iHBath=iBath1Q = 0;
         iTmp = atoin(pCharRec->NumBaths, 3);
         switch (iTmp)
         {
            case 100:
               iFBath = 1;
               break;
            case 150:
            case 105:
               iFBath = 1;
               iHBath = 1;
               break;
            case 200:
               iFBath = 2;
               break;
            case 225:
               iFBath = 2;
               iBath1Q = 1;
               break;
            case 250:
               iFBath = 2;
               iHBath = 1;
               break;
            case 300:
               iFBath = 3;
               break;
            case 350:
               iFBath = 3;
               iHBath = 1;
               break;
            case 400:
               iFBath = 4;
               break;
            case 450:
               iFBath = 4;
               iHBath = 1;
               break;
            case 500:
               iFBath = 5;
               break;
            case 550:
               iFBath = 5;
               iHBath = 1;
               break;
            case 600:
               iFBath = 6;
               break;
            case 650:
               iFBath = 6;
               iHBath = 1;
               break;
            case 700:
               iFBath = 7;
               break;
            case 750:
               iFBath = 7;
               iHBath = 1;
               break;
            case 800:
               iFBath = 8;
               break;
            case 850:
               iFBath = 8;
               iHBath = 1;
               break;
            case 900:
               iFBath = 9;
               break;
            case 950:
               iFBath = 9;
               iHBath = 1;
               break;
            default:
               iFBath = pCharRec->NumBaths[0] & 0x0F;
               if (pCharRec->NumBaths[1] > '0')
                  iHBath = 1;
               break;
         }
         iTmp = sprintf(acTmp, "%d", iFBath);
         memcpy(pStdChar->FBaths, acTmp, iTmp);
         memcpy(pStdChar->Bath_4Q, acTmp, iTmp);
         if (iHBath > 0)
         {
            pStdChar->HBaths[0] = '1';
            pStdChar->Bath_2Q[0] = '1';
         } else if (iBath1Q > 0)
         {
            pStdChar->HBaths[0] = '1';
            pStdChar->Bath_1Q[0] = '1';
         }

#else
         iFBath = pCharRec->NumBaths[0] & 0x0F;
         iTmp = sprintf(acTmp, "%d", iFBath);
         memcpy(pStdChar->FBaths, acTmp, iTmp);

         // Half bath
         if (pCharRec->NumBaths[1] > '0')
            pStdChar->HBaths[0] = '1';
#endif
      }

      // Fireplace - need translation table
      iTmp = atoin(pCharRec->Fireplace_Code, ASIZ_CODE);
      if (iTmp > 0)
         pStdChar->Fireplace[0] = 'Y';

      // Roof
      if (pCharRec->Roof > '0')
      {
         switch (pCharRec->Roof)
         {
            case '1':
               cTmp1 = 'C';   // COMPOSITION SHINGLE
               break;
            case '2':
               cTmp1 = 'A';   // WOOD SHINGLE
               break;
            case '3':
               cTmp1 = 'B';   // WOOD SHAKE
               break;
            default:
               LogMsg("*** Unknown Roof material: %c - %.14s", pCharRec->Roof, pStdChar->Apn);
               cTmp1 = ' ';
         }
         pStdChar->RoofMat[0] = cTmp1;
      }

      // Floor - Other values are not known 0 and 1

      // Pool/Spa
      iTmp = atoin(pCharRec->Swimming_Pool_Code, ASIZ_CODE);
      if (iTmp > 0)
      {
         if (pCharRec->Spa == '2')
            pStdChar->Pool[0] = 'C';      // Pool/Spa
         else
            pStdChar->Pool[0] = 'P';      // Pool
      } else if (pCharRec->Spa == '2')
         pStdChar->Pool[0] = 'S';         // Spa

#ifdef _DEBUG
      //if (!memcmp(pCharRec->Apn, "0780221029", 10))
      //   iRet = 0;
#endif

      // Lot sqft - Lot Acres
      lSqft = atoin(pCharRec->Lot_Sqft, ASIZ_LOT_SQFT);
      if (lSqft > 0)
      {
         iTmp = sprintf(acTmp, "%d", lSqft);
         memcpy(pStdChar->LotSqft, acTmp, iTmp);
         lTmp = (lSqft*ACRES_FACTOR)/SQFT_PER_ACRE;
         iTmp = sprintf(acTmp, "%d", lTmp);
         memcpy(pStdChar->LotAcre, acTmp, iTmp);
      } else
      {
         lTmp = atoin(pCharRec->Lot_Acres, ASIZ_ACRES);
         if (lTmp > 0)
         {
            lSqft = (lTmp*SQFT_PER_ACRE)/100;
            iTmp = sprintf(acTmp, "%d", lSqft);
            memcpy(pCharRec->Lot_Sqft, acTmp, iTmp);

            lTmp *= 10;
            iTmp = sprintf(acTmp, "%d", lTmp);
            memcpy(pCharRec->Lot_Acres, acTmp, iTmp);
         }
      }

      // Neighborhood code
      memcpy(pStdChar->Nbh_Code, pCharRec->Nbh_Code, ASIZ_NBH_CODE);

      // Zoning
      if (pCharRec->Zone[0] > ' ')
         memcpy(pStdChar->Zoning, pCharRec->Zone, ASIZ_ZONE);

      // Usecode

      // Basement Sqft 
      lTmp = atoin(pCharRec->Finished_Basement_Area, ASIZ_AREA_SQFT);
      if (lTmp > 0)
      {
         iTmp = sprintf(acTmp, "%d", lTmp);
         memcpy(pStdChar->BsmtSqft, acTmp, iTmp);
      }

      // View - not avail.
      // Units - not avail.
      // Buildings - not avail
      // Others

      pStdChar->CRLF[0] = '\n';
      pStdChar->CRLF[1] = '\0';
      fputs(acOutbuf, fdOut);

      if (!(++iCnt % 1000))
         printf("\r%u", iCnt);
   }

   if (fdIn) fclose(fdIn);
   if (fdOut) fclose(fdOut);

   // Sort output on ASMT
   if (iCnt > 100)
   {
      // Rename old CHAR file if needed
      if (!_access(acCChrFile, 0))
      {
         strcpy(acInbuf, acCChrFile);
         replStr(acInbuf, ".dat", ".sav");
         if (!_access(acInbuf, 0))
         {
            LogMsg("Delete old %s", acInbuf);
            DeleteFile(acInbuf);
         }
         LogMsg("Rename %s to %s", acCChrFile, acInbuf);
         RenameToExt(acCChrFile, "sav");
         sprintf(acTmp, "%s+%s", acInbuf, acTmpFile);
      } else
         strcpy(acTmp, acTmpFile);
      LogMsgD("\nSorting char file %s --> %s", acTmp, acCChrFile);
      // Sort on APN, YrBlt to keep the latest record on top
      iRet = sortFile(acTmp, acCChrFile, "S(1,14,C,A,53,4,C,D) F(TXT) ");
   } else
   {
      printf("\n");
      iRet = 0;
   }

   LogMsg("Number of records output: %d", iCnt);
   return iRet;
}

/****************************** Sac_ConvStdChar2 *****************************
 *
 * Convert char file "city_full_characteristics.dos" to STD_CHAR format
 *
 *****************************************************************************/

int Sac_ConvStdChar2(char *pInfile)
{
   FILE     *fdIn, *fdOut;
   char     acBuf[4096], acTmpFile[256], acTmp[256], acCode[4], *pRec;
   int      iRet, iTmp, iFldCnt, iCnt=0;
   STDCHAR  myCharRec;

   LogMsg0("Converting residential char file %s", pInfile);
   if (!(fdIn = fopen(pInfile, "r")))
      return -1;

   sprintf(acTmpFile, "%s\\%s\\%s_char.tmp", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   if (!(fdOut = fopen(acTmpFile, "w")))
   {
      fclose(fdIn);
      LogMsg("***** Error creating output file %s", acTmpFile);
      return -2;
   }

   // Skip first record - header
   pRec = fgets(acBuf, 4096, fdIn);

   while (!feof(fdIn))
   {
      pRec = fgets(acBuf, 4096, fdIn);

      if (!pRec)
         break;

      // Replace NULL with empty string
      iFldCnt = ParseStringIQ(pRec, '|', MAX_FLD_TOKEN, apTokens);
      if (iFldCnt < C_USE_CODE)
      {
         if (iFldCnt > 1)
            LogMsg("*** Bad CHAR record (%d): %s [%d]", iCnt, pRec, iFldCnt);
         continue;
      }

      memset((void *)&myCharRec, ' ', sizeof(STDCHAR));
      memcpy(myCharRec.Apn, apTokens[C_PARCEL_NUMBER], strlen(apTokens[C_PARCEL_NUMBER]));
      
      // Format APN
      iRet = formatApn(apTokens[C_PARCEL_NUMBER], acTmp, &myCounty);
      memcpy(myCharRec.Apn_D, acTmp, iRet);

      // Land Use
      if (*apTokens[C_USE_CODE] >= ' ')
      {
         iRet = iTrim(apTokens[C_USE_CODE]);
         memcpy(myCharRec.LandUse, apTokens[C_USE_CODE], iRet);
      }

      // Pool
      iTmp = atoi(apTokens[C_POOL_DATE]);
      if (iTmp > 1)
         myCharRec.Pool[0] = 'P';

      // Spa
      if (*apTokens[C_SPA_HOT_TUB] > '0')
      {
         if (iTmp > 1)
            myCharRec.Pool[0] = 'C';
         else
            myCharRec.Pool[0] = 'S';
      }

      // QualityClass
      iTmp = blankRem(apTokens[C_QUALITY_CLASS]);
      if (*apTokens[C_QUALITY_CLASS] > ' ' && *apTokens[C_QUALITY_CLASS] < 'Z')
      {
         vmemcpy(myCharRec.QualityClass, apTokens[C_QUALITY_CLASS], SIZ_CHAR_QCLS, iTmp);

         acCode[0] = ' ';
         strcpy(acTmp, apTokens[C_QUALITY_CLASS]);
         if (isalpha(acTmp[0])) 
         {
            myCharRec.BldgClass = acTmp[0];

            if (isdigit(acTmp[1]))
               iRet = Quality2Code((char *)&acTmp[1], acCode, NULL);
         } else if (isdigit(acTmp[0]))
            iRet = Quality2Code(acTmp, acCode, NULL);
         else
            LogMsg("*** Please check QUALITYCLASS: '%s' in [%s]", apTokens[C_QUALITY_CLASS], apTokens[C_PARCEL_NUMBER]);

         if (acCode[0] > ' ')
            myCharRec.BldgQual = acCode[0];
      }

#ifdef _DEBUG
      //if (!memcmp(myCharRec.Apn, "0780221029", 10))
      //   iRet = 0;
#endif

      // Improved Condition
      if (*apTokens[C_CONDITION] >= 'A')
      {
         pRec = findXlatCode(apTokens[C_CONDITION], &asCond[0]);
         if (pRec)
            myCharRec.ImprCond[0] = *pRec;
      }

      // YrBlt
      int iYrBlt = atoi(apTokens[C_YEAR_BUILT]);
      if (iYrBlt > 1600 && iYrBlt <= lToyear)
      {
         iRet = sprintf(acTmp, "%d", iYrBlt);
         memcpy(myCharRec.YrBlt, acTmp, iRet);
      }

      // YrEff
      iTmp = atoi(apTokens[C_EFFECTIVE_YEAR]);
      if (iTmp > 1900 && iTmp >= iYrBlt && iTmp <= lToyear)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.YrEff, acTmp, iRet);
      }

      // BldgSize
      int iBldgSize = atoi(apTokens[C_TOTAL_LIVING_AREA]);
      if (iBldgSize > 100)
      {
         iRet = sprintf(acTmp, "%d", iBldgSize);
         memcpy(myCharRec.BldgSqft, acTmp, iRet);
      }

      // First floor area
      iTmp = atoi(apTokens[C_FIRST_FLOOR_AREA]);
      if (iTmp > 100)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.Sqft_1stFl, acTmp, iRet);
      }
      
      // Second floor area
      iTmp = atoi(apTokens[C_SECOND_FLOOR_AREA]);
      if (iTmp > 100)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.Sqft_2ndFl, acTmp, iRet);
      }

      // Above Second floor area
      iTmp = atoi(apTokens[C_FLOOR_AREA_ABOVE_2]);
      if (iTmp > 100)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.Sqft_Above2nd, acTmp, iRet);
      }

      // Additional area
      iTmp = atoi(apTokens[C_ADDITION_AREA]);
      if (iTmp > 100)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.MiscSqft, acTmp, iRet);
      }

      // Units Count
      iTmp = atoi(apTokens[C_BUILDING_COUNT]);
      if (iTmp > 1)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.Units, acTmp, iRet);
      } 

      // Garage Sqft
      int iAttGar = atoi(apTokens[C_GARAGE_TOTAL_AREA]);
      if (iAttGar > 100)
      {
         iRet = sprintf(acTmp, "%d", iAttGar);
         memcpy(myCharRec.GarSqft, acTmp, iRet);
         myCharRec.ParkType[0] = 'I';
      }

      // Carport SF
      int iCarport = atoi(apTokens[C_CARPORT_AREA]);
      if (iCarport > 100)
      {
         iRet = sprintf(acTmp, "%d", iCarport);
         memcpy(myCharRec.Misc.sExtra.CarportSqft, acTmp, iRet);
         if (myCharRec.ParkType[0] == ' ')
         {
            myCharRec.ParkType[0] = 'C';
            memcpy(myCharRec.GarSqft, acTmp, iRet);
         }
      }

      // Park space
      iTmp = atoi(apTokens[C_PARKING_SPACE]);
      if (iTmp > 0)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.ParkSpace, acTmp, iRet);
      }

      // Stories/NumFloors
      iTmp = atoi(apTokens[C_NUMBER_OF_STORIES]);
      if (iTmp > 0 && iTmp < 100)
      {
         iRet = sprintf(acTmp, "%d.0", iTmp);
         memcpy(myCharRec.Stories, acTmp, iRet);
      }

#ifdef _DEBUG
      //if (myCharRec.Apn[0] < '0' || !memcmp(myCharRec.Apn, "003351003000", 9))
      //   iRet = 0;
#endif

      // Heating/Cooling
      iTmp = blankRem(apTokens[C_AIR_CONDITIONING]);
      if (*apTokens[C_AIR_CONDITIONING] == 'B')
      {
         myCharRec.Heating[0] = 'Z';
         myCharRec.Cooling[0] = 'C';
      } else if (*apTokens[C_AIR_CONDITIONING] == 'N')
      {
         myCharRec.Heating[0] = 'L';
         myCharRec.Cooling[0] = 'N';
      } else if (*apTokens[C_AIR_CONDITIONING] == 'H')
         myCharRec.Heating[0] = 'Y';
      else if (*apTokens[C_AIR_CONDITIONING] == 'U')
      {
         myCharRec.Heating[0] = '9';
         myCharRec.Cooling[0] = '9';
      }

      // Rooms
      iTmp = atoi(apTokens[C_TOTAL_ROOMS]);
      if (iTmp > 0)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.Rooms, acTmp, iRet);
      }

      // Beds
      iTmp = atoi(apTokens[C_NUMBER_OF_BEDROOMS]);
      if (iTmp > 0)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.Beds, acTmp, iRet);
      }

      // Full baths
      iTmp = atoi(apTokens[C_NUMBER_OF_FULL_BATHS]);
      if (iTmp > 0)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.FBaths, acTmp, iRet);
         memcpy(myCharRec.Bath_4Q, acTmp, iRet);
      }

      // Half bath
      iTmp = atoi(apTokens[C_NUMBER_OF_HALF_BATHS]);
      if (iTmp > 0)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.HBaths, acTmp, iRet);
         memcpy(myCharRec.Bath_2Q, acTmp, iRet);
      }

      // FirePlace
      iTmp = atoi(apTokens[C_FIREPLACE_COUNT]);
      if (iTmp > 0 && iTmp <= 9)
         myCharRec.Fireplace[0] = *apTokens[C_FIREPLACE_COUNT];
      //else if (*apTokens[C_FIREPLACE_COUNT] == '0')
      //   myCharRec.Fireplace[0] = 'N';

      // Roof
      if (*apTokens[C_ROOF_COVER] >= 'A')
      {
         _strupr(apTokens[C_ROOF_COVER]);
         pRec = findXlatCodeA(apTokens[C_ROOF_COVER], &asRoof[0]);
         if (pRec)
            myCharRec.RoofMat[0] = *pRec;
      }

      myCharRec.CRLF[0] = '\n';
      myCharRec.CRLF[1] = '\0';
      fputs((char *)&myCharRec.Apn[0], fdOut);

      if (!(++iCnt % 1000))
         printf("\r%u", iCnt);
   }

   if (fdIn) fclose(fdIn);
   if (fdOut) fclose(fdOut);

   LogMsg("Number of records processed: %d", iCnt);

   // Sort output on ASMT
   if (iCnt > 100)
   {
      // Rename old CHAR file if needed
      if (!_access(acCChrFile, 0))
      {
         strcpy(acBuf, acCChrFile);
         replStr(acBuf, ".dat", ".sav");
         if (!_access(acBuf, 0))
         {
            LogMsg("Delete old %s", acBuf);
            DeleteFile(acBuf);
         }
         LogMsg("Rename %s to %s", acCChrFile, acBuf);
         RenameToExt(acCChrFile, "sav");
         sprintf(acTmp, "%s+%s", acBuf, acTmpFile);
      } else
         strcpy(acTmp, acTmpFile);
      LogMsgD("\nSorting char file %s --> %s", acTmp, acCChrFile);
      // Sort on APN, YrBlt to keep the latest record on top
      iRet = sortFile(acTmp, acCChrFile, "S(1,14,C,A,53,8,C,D) F(TXT) DUPO(B4096,1,60)");
   } else
   {
      printf("\n");
      iRet = 0;
   }

   return iRet;
}

/******************************** Sac_LegalExtr *****************************
 *
 * Extract legal from CSV file
 * Return number of records extracted. <0 if error.
 *
 ****************************************************************************/

int Sac_ExtrLegal(char *pInfile)
{
   char     *pTmp, *pLegal,  acBuf[6000], acOutFile[_MAX_PATH], acInRec[6000];

   long     iRet, lCnt=0, lOutRecs=0, iTmp;
   FILE     *fdIn, *fdOut;

   LogMsg0("Extract Legal");

   // Open long legal file
   LogMsg("Open long legal file %s", pInfile);
   fdIn = fopen(pInfile, "r");
   if (fdIn == NULL)
   {
      LogMsg("***** Error opening long legal file: %s\n", pInfile);
      return -1;
   }

   // Create Output file
   sprintf(acOutFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "Legal");
   LogMsg("Open output file %s", acOutFile);
   fdOut = fopen(acOutFile, "w");
   if (fdOut == NULL)
   {
      LogMsg("***** Error creating output legal file: %s\n", acOutFile);
      return -3;
   }

   // Output header - FIPS|APN_D|LEGAL1
   fputs("APN|LEGAL\n", fdOut);

   // Merge loop
   while (!feof(fdIn))
   {
      pTmp = fgets(acInRec, 6000, fdIn);
      if (!pTmp)
         break;

      // Parse input record
      iRet = ParseStringIQ(acInRec, '|', MAX_FLD_TOKEN, apTokens);
      if (iRet < P_LOAD_DATE)
      {
         LogMsg("***** Error: bad input record for APN=%s", apTokens[iApnFld]);
         return -1;
      }

#ifdef _DEBUG      
      //if (!memcmp(acInRec, "2079011016", 10))
      //   iRet = 0;
#endif

      // Legal
      if (*apTokens[P_SHORT_LEGAL] > ' ')
      {
         replStr(apTokens[P_SHORT_LEGAL], ":: ", " ");
         if (pTmp = strstr(apTokens[P_SHORT_LEGAL], "Name: "))
            pLegal = pTmp + 6;
         else
            pLegal = apTokens[P_SHORT_LEGAL];
      } else if (*apTokens[P_LONG_LEGAL] > ' ')
         pLegal = apTokens[P_LONG_LEGAL];
      else
         pLegal = NULL;

      if (pLegal)
      {
         iTmp = strlen(pLegal);
         if (iTmp > iMaxLegal)
            iMaxLegal = iTmp;

         sprintf(acBuf, "%s|%s\n", apTokens[PARCEL_NUMBER], pLegal);   
         fputs(acBuf, fdOut);  
         lOutRecs++;
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdIn)
      fclose(fdIn);
   if (fdOut)
      fclose(fdOut);

   LogMsg("Total input records:  %u", lCnt);
   LogMsg("Total output records: %u", lOutRecs);
   LogMsg("Max legal length:     %u", iMaxLegal);

   return lOutRecs;
}

int Sac_LegalExtr(char *pInfile, char *pOutfile)
{
   char     *pTmp, acBuf[8192], acApn[32], acLastApn[32], acInRec[512];

   long     iRet, lCnt=0, lOutRecs=0, iTmp;
   FILE     *fdIn, *fdOut;
   SACLEGAL *pRec = (SACLEGAL *)&acInRec[0];
   BOOL     bExtrAll;

   LogMsg0("Extract Legal");
   bExtrAll = false;

   // Open long legal file
   LogMsg("Open long legal file %s", pInfile);
   fdIn = fopen(pInfile, "r");
   if (fdIn == NULL)
   {
      LogMsg("***** Error opening long legal file: %s\n", pInfile);
      return -1;
   }

   // Create Output file
   LogMsg("Open output file %s", pOutfile);
   fdOut = fopen(pOutfile, "w");
   if (fdOut == NULL)
   {
      LogMsg("***** Error creating output legal file: %s\n", pOutfile);
      return -3;
   }

   // Output header - FIPS|APN_D|LEGAL1
   fputs("FIPS|APN_D|LEGAL\n", fdOut);

   // Get first rec
   pTmp = fgets(acInRec, 512, fdIn);
   blankRem(pRec->LglDesc, LLS_LEGAL_DESC);
   formatApn(pRec->APN, acApn, &myCounty);
   sprintf(acBuf, "06067|%s|%s", acApn, pRec->LglDesc);   
   memcpy(acLastApn, acInRec, iApnLen);

   // Merge loop
   while (!feof(fdIn))
   {
      pTmp = fgets(acInRec, 512, fdIn);

      // Check for EOF
      if (!pTmp)
      {
         strcat(acBuf, "\n");
         iTmp = strlen(acBuf);
         if (iTmp > iMaxLegal)
            iMaxLegal = iTmp;

         fputs(acBuf, fdOut);  
         lOutRecs++;
         break;
      }

#ifdef _DEBUG      
      //if (!memcmp(acInRec, "2079011016", 10))
      //   iRet = 0;
#endif

      if (memcmp(acLastApn, acInRec, iApnLen))
      {
         strcat(acBuf, "\n");
         iTmp = strlen(acBuf);
         if (iTmp > iMaxLegal)
            iMaxLegal = iTmp;

         fputs(acBuf, fdOut);  
         lOutRecs++;
         memcpy(acLastApn, acInRec, iApnLen);

         // Create new record
         blankRem(pRec->LglDesc, LLS_LEGAL_DESC);
         iRet = formatApn(pRec->APN, acApn, &myCounty);
         sprintf(acBuf, "06067|%s|%s", acApn, pRec->LglDesc);   
      } else
      {
         blankRem(pRec->LglDesc, LLS_LEGAL_DESC);
         strcat(acBuf, " ");
         strcat(acBuf, pRec->LglDesc);
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdIn)
      fclose(fdIn);
   if (fdOut)
      fclose(fdOut);

   LogMsg("Total input records:  %u", lCnt);
   LogMsg("Total output records: %u", lOutRecs);
   LogMsg("Max legal length:     %u", iMaxLegal);

   return lOutRecs;
}

/**************************** Sac_ConvGisFile ********************************
 *
 * 
 *****************************************************************************/

int Sac_ConvGisFile(char *pInfile)
{
   FILE     *fdIn, *fdOut;
   char     acInbuf[4096], acOutbuf[1024], acTmpFile[256], acTmp[256], *pRec;
   int      iRet, iFldCnt, iCnt=0;
   XADR_REC *pXAdr = (XADR_REC *)&acOutbuf[0];

   LogMsg("\nConverting GIS file %s", pInfile);

   // Sort input file
   sprintf(acTmpFile, "%s\\%s\\%s_gis.srt", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   LogMsg("Sort gis file %s to %s", pInfile, acTmpFile);
   sprintf(acTmp, "S(#%d,C,A)", GIS_APNBR+1);
   iRet = sortFile(pInfile, acTmpFile, acTmp);
   if (iRet < 500)
   {
      LogMsg("***** Input file is too small.");
      return 1;
   }

   if (!(fdIn = fopen(acTmpFile, "r")))
      return -1;

   // Skip header
   pRec = fgets(acInbuf, 4096, fdIn);

   // Open output file
   sprintf(acTmpFile, "%s\\%s\\%s_gis.tmp", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   if (!(fdOut = fopen(acTmpFile, "w")))
   {
      fclose(fdIn);
      LogMsg("***** Error creating output file %s", acTmpFile);
      return -2;
   }

   // Start loop
   while (!feof(fdIn))
   {
      // Get next record
      pRec = fgets(acInbuf, 4096, fdIn);
      if (!pRec || acInbuf[0] > '9')
         break;

      iFldCnt = ParseStringNQ(pRec, ';', MAX_FLD_TOKEN, apTokens);
      if (iFldCnt < GIS_APNBR)
      {
         if (iFldCnt > 1)
            LogMsg("*** Bad GIS record (%d): %s [%d]", iCnt, pRec, iFldCnt);
         continue;
      }

      if (*apTokens[GIS_STREET] < '1')
         continue;

      memset(acOutbuf, ' ', sizeof(XADR_REC));
      memcpy(pXAdr->Apn, apTokens[GIS_APNBR], strlen(apTokens[GIS_APNBR]));

#ifdef _DEBUG
      //if (!memcmp(pXAdr->Apn, "22523700040012", 14))
      //   iRet = 0;
#endif
      ADR_REC *pAdrRec = (ADR_REC *)&acTmp;
      int      iStrNum1, iStrNum2;
      iStrNum1 = atol(apTokens[GIS_NUMB]);
      iStrNum2 = atol(apTokens[GIS_MSTREET]);
      memset(acTmp, 0, sizeof(ADR_REC));
      if (iStrNum1 > 0 && iStrNum1 == iStrNum2 && strlen(apTokens[GIS_SUB]) > 3 && isdigit(*apTokens[GIS_SUB]))
      {
         parseMAdr1_3(pAdrRec, apTokens[GIS_MSTREET], false);
      }

      if (!memcmp(pAdrRec->Unit, apTokens[GIS_SUB], 4))
      {
         memcpy(pXAdr->strName, pAdrRec->strName, strlen(pAdrRec->strName));
         memcpy(pXAdr->strSfx, pAdrRec->strSfx, strlen(pAdrRec->strSfx));
         memcpy(pXAdr->strDir, pAdrRec->strDir, strlen(pAdrRec->strDir));
         memcpy(pXAdr->Unit, pAdrRec->Unit, strlen(pAdrRec->Unit));
      } else
      {
         memcpy(pXAdr->strName, apTokens[GIS_STREET], strlen(apTokens[GIS_STREET]));
         memcpy(pXAdr->Unit, apTokens[GIS_SUB], strlen(apTokens[GIS_SUB]));
      }
      memcpy(pXAdr->strNum, apTokens[GIS_NUMB], strlen(apTokens[GIS_NUMB]));
      memcpy(pXAdr->Zip, apTokens[GIS_ZIP], strlen(apTokens[GIS_ZIP]));
      if (!strcmp(apTokens[GIS_ZIP], apTokens[GIS_MZIP]) && !memcmp(apTokens[GIS_MSTATE], "CA", 2))
         memcpy(pXAdr->City, apTokens[GIS_MCITY], strlen(apTokens[GIS_MCITY]));

      pXAdr->CRLF[0] = '\n';
      pXAdr->CRLF[1] = '\0';
      fputs(acOutbuf, fdOut);

      if (!(++iCnt % 1000))
         printf("\r%u", iCnt);
   }

   if (fdIn) fclose(fdIn);
   if (fdOut) fclose(fdOut);

   LogMsg("Number of records processed: %d", iCnt);

   // Sort output on ASMT
   if (iCnt > 100)
   {
      sprintf(acOutbuf, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "GIS");
      LogMsgD("\nSorting char file %s --> %s", acTmpFile, acOutbuf);
      // Asmt
      iRet = sortFile(acTmpFile, acOutbuf, "S(1,12,C,A) OMIT(1,1,C,EQ,\" \") DUPO(B2000,)");
   } else
   {
      printf("\n");
      iRet = 0;
   }

   return iRet;
}

/******************************** Sac_ExtrSale ******************************
 *
 * Create standard sale file from city_full_salesmaster.txt
 *
 ****************************************************************************/

int Sac_ExtrSale()
{
   char     acTmpFile[_MAX_PATH];
   char     acTmp[256], acRec[1024], acSaleRec[1024], *pTmp;

   FILE      *fdOut;
   SCSAL_REC *pSale = (SCSAL_REC *)&acSaleRec[0];

   int      iTmp;
   double   dTmp;
   long     lCnt=0;
   unsigned long lPrice, lTmp;

   LogMsg0("Creating standard sale file for %s", myCounty.acCntyCode);

   // Open Sales file
   LogMsg("Open Sales file %s", acSaleFile);
   fdSale = fopen(acSaleFile, "r");
   if (fdSale == NULL)
   {
      LogMsg("***** Error opening Sales file: %s\n", acSaleFile);
      return -1;
   }

   // Open Output file
   sprintf(acTmpFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "TMP");

   LogMsg("Open output file %s", acTmpFile);
   fdOut = fopen(acTmpFile, "w");
   if (fdOut == NULL)
   {
      LogMsg("***** Error creating sale output file: %s\n", acTmpFile);
      return -2;
   }

   // Loop through record set
   while (!feof(fdSale))
   {  
      if (!(pTmp = fgets(acRec, 1024, fdSale)))
         break;

      if (*pTmp > '9')
         continue;

      // Parse input rec
      iTokens = ParseStringIQ(acRec, '|', S_COLS+1, apTokens);
      if (iTokens < S_COLS-1)
      {
         LogMsg0("*** Error: bad sale record %.16s (#tokens=%d)", acRec, iTokens);
         continue;
      }

      // Reset output record
      memset(acSaleRec, ' ', sizeof(SCSAL_REC));

      // APN
      memcpy(pSale->Apn, apTokens[PARCEL_NUMBER], strlen(apTokens[PARCEL_NUMBER]));

      // Doc date
      vmemcpy(pSale->DocDate, apTokens[S_DOCUMENT_DT], 8);

      // Docnum
      iTmp = sprintf(acTmp, "%.6s%.4s", &pSale->DocDate[2], apTokens[S_DOCUMENT_PAGE]);
      memcpy(pSale->DocNum, acTmp, iTmp);

#ifdef _DEBUG
      //if (!memcmp(pSale->Apn, "027071031000", 9))
      //   lPrice = 0;
#endif

      // Sale price
      lPrice = (unsigned long)atol(apTokens[S_EST_SALES_PRICE]);

      // Tax
      dollar2Num(apTokens[S_TRANSFER_TAX_AMOUNT], acTmp);
      if (acTmp[0] > '0')
      {
         dTmp = atof(acTmp);
         lTmp = (unsigned long)(dTmp * SALE_FACTOR);

         iTmp = sprintf(acTmp, "%*.2f", SALE_SIZ_STAMPAMT, dTmp);
         memcpy(pSale->StampAmt, acTmp, iTmp);

         // Ignore sale price if less than 1000
         if (lPrice >= 10000)
            sprintf(acTmp, "%*u00", SALE_SIZ_SALEPRICE-2, lPrice/100);
         else if (lPrice >= 1000)
            sprintf(acTmp, "%*u", SALE_SIZ_SALEPRICE, lPrice);
         else if (lTmp >= 1000)
            sprintf(acTmp, "%*u", SALE_SIZ_SALEPRICE, lTmp);
         else
            memset(acTmp, ' ', SALE_SIZ_SALEPRICE);
         memcpy(pSale->SalePrice, acTmp, SALE_SIZ_SALEPRICE);

         if (*apTokens[S_TRANSFER_TAX_CODE] == 'F' || *apTokens[S_TRANSFER_TAX_CODE] == 'P' || *apTokens[S_TRANSFER_TAX_CODE] == 'U')
            pSale->SaleCode[0] = *apTokens[S_TRANSFER_TAX_CODE];
         else if (*apTokens[S_TRANSFER_TAX_CODE] == 'T' && lTmp > 1000)
            pSale->SaleCode[0] = 'F';
         else
            pSale->SaleCode[0] = 'U';
      }

      // Doc code 
      vmemcpy(pSale->DocCode, apTokens[S_DOCUMENT_TYPE], SALE_SIZ_DOCCODE);
      sprintf(acTmp, "%s  ", apTokens[S_DOCUMENT_TYPE]);
      iTmp = findDocType(acTmp, (IDX_TBL5 *)&SAC_DocCode[0]);
      if (iTmp >= 0)
      {
         memcpy(pSale->DocType, SAC_DocCode[iTmp].pCode, SAC_DocCode[iTmp].iCodeLen);
         pSale->NoneSale_Flg = SAC_DocCode[iTmp].flag;
      } else if (lPrice > 100)
         pSale->DocType[0] = '1';

      // Multi-Apn sale
      iTmp = atol(apTokens[S_PARCEL_CNT]);
      if (iTmp > 1)
      {
         pSale->MultiSale_Flg = 'Y';

         if (iTmp > 99) iTmp = 99;
         sprintf(acTmp, "%d", iTmp);
         vmemcpy(pSale->NumOfPrclXfer, acTmp, SALE_SIZ_NOPRCLXFR);
      }
      
#ifdef _DEBUG
      //if (!memcmp(pSale->Apn, "001160016000", 9))
      //   iTmp = 0;
#endif

      // Only output record with DocDate
      if (pSale->DocDate[0] > ' ')
      {
         // Seller
         strcpy(acTmp, apTokens[S_GRANTOR]);
         iTmp = blankRem(acTmp);
         vmemcpy(pSale->Seller1, acTmp, SALE_SIZ_SELLER);

         // Buyer
         if (iTokens == S_COLS)
         {
            strcpy(acTmp, apTokens[S_GRANTEE]);
            iTmp = blankRem(acTmp);
            vmemcpy(pSale->Name1, acTmp, SALE_SIZ_BUYER);
         }

         if (lPrice < 4000000000)
         {
            pSale->CRLF[0] = 10;
            pSale->CRLF[1] = 0;
            fputs(acSaleRec, fdOut);
         } else
            LogMsg("*** Skip sale rec - est price is questionable: %d [%s]", lPrice, apTokens[PARCEL_NUMBER]);
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   fclose(fdSale);
   fclose(fdOut);
   
   // Sort output file
   LogMsg("Total processed records: %u\n", lCnt);

   // Sort output file and dedup on APN asc, DocDate asc, DocNum asc
   sprintf(acTmp, "S(1,14,C,A,27,8,C,A,15,12,C,A) F(TXT) DUPO(1,34) ");
   lTmp = sortFile(acTmpFile, acCSalFile, acTmp);

   if (!lTmp)
      iTmp = -1;
   else
      iTmp = 0;

   LogMsg("Number of Sale records processed: %d.", lCnt);
   LogMsg("                       output:    %d.", lTmp);
   return iTmp;
}

/******************************* Sac_MergeLegal ******************************
 *
 * Merge legal using data extracted from roll file
 *
 *****************************************************************************/

int Sac_MergeLegal(char *pOutbuf)
{
   static   char acRec[6000], *pRec=NULL;
   int      iRet, iLoop;

   iRet=0;

   // Get first Char rec for first call
   if (!pRec)
   {
      // Get first rec
      pRec = fgets(acRec, 6000, fdLegal);
      pRec = fgets(acRec, 6000, fdLegal);
   }

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "0020041077", 10))
   //   iRet = 0;
#endif

   do
   {
      if (!pRec)
      {
         fclose(fdLegal);
         fdLegal = NULL;
         return 1;      // EOF
      }

      // Compare Apn
      iLoop = memcmp(pOutbuf, pRec, iApnLen);
      if (iLoop > 0)
      {
         if (bDebug)
            LogMsg0("Skip APN rec  %.*s", iApnLen, pRec);
         pRec = fgets(acRec, 6000, fdLegal);
         lApnSkip++;
      }
   } while (iLoop > 0);

   // If not match, return
   if (iLoop)
      return 1;

   updateLegal(pOutbuf, &acRec[iApnLen+1]);
   lLegalMatch++;

   // Get next Char rec
   pRec = fgets(acRec, 6000, fdLegal);

   return 0;
}

/****************************** Sac_CreateRollRec2 ****************************
 *
 * Parse LDR record for 2019.
 * Use situs from current situs file, not from LDR record
 *
 ******************************************************************************/

int Sac_CreateRollRec2(char *pOutbuf, char *pRollRec)
{
   char     acTmp[256], acTmp1[256], acApn[32], *pTmp;
   LONGLONG lTmp;
   int      iRet=0, iTmp;

   // Parse input record
   iTokens = ParseStringNQ(pRollRec, ';', MAX_FLD_TOKEN, apTokens);
   if (iTokens < SAC_L_FLDS)
   {
      LogMsg("***** Error: bad input record for APN=%s-%s-%s-%s", 
         apTokens[SAC_L_MAPB], apTokens[SAC_L_PG], apTokens[SAC_L_PCL], apTokens[SAC_L_PSUB]);
      return -1;
   }

   // Clear output buffer
   memset(pOutbuf, ' ', iRecLen);

   // Format APN
   iTmp = sprintf(acApn, "%s%s%s%s", apTokens[0], apTokens[1], apTokens[2], apTokens[3]);
   memcpy(pOutbuf, acApn, iTmp);
   iTmp = sprintf(acTmp, "%s-%s-%s-%s", apTokens[0], apTokens[1], apTokens[2], apTokens[3]);
   memcpy(pOutbuf+OFF_APN_D, acTmp, iTmp);

   // Create MapLink and output new record
   iTmp = formatMapLink(acApn, acTmp, &myCounty);
   memcpy(pOutbuf+OFF_MAPLINK, acTmp, iTmp);

   // Create index map link
   if (getIndexPage(acTmp, acTmp1, &myCounty))
      memcpy(pOutbuf+OFF_IMAPLINK, acTmp1, strlen(acTmp1));

   // County code
   memcpy(pOutbuf+OFF_CO_NUM, "34SAC", 5);
   *(pOutbuf+OFF_STATUS) = 'A';

   // Year assessed
   memcpy(pOutbuf+OFF_YR_ASSD, myCounty.acYearAssd, 4);

   // Format TRA
   lTmp = atol(apTokens[SAC_L_TAX_RATE_AREA]);
   iTmp = sprintf(acTmp, "%.6d", lTmp);
   memcpy(pOutbuf+OFF_TRA, acTmp, iTmp);

   // Land
   long lLand = atol(apTokens[SAC_L_LAND]);
   if (lLand > 0)
   {
      sprintf(acTmp, "%*u", SIZ_LAND, lLand);
      memcpy(pOutbuf+OFF_LAND, acTmp, SIZ_LAND);
   }

   // Improve - Ratio
   long lImpr = atol(apTokens[SAC_L_IMPR]);
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*u", SIZ_IMPR, lImpr);
      memcpy(pOutbuf+OFF_IMPR, acTmp, SIZ_IMPR);
      sprintf(acTmp, "%*u", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
      memcpy(pOutbuf+OFF_RATIO, acTmp, SIZ_RATIO);
   }

   // PP value
   long lPers = atol(apTokens[SAC_L_PP]);

   // Fixture
   long lFixt = atol(apTokens[SAC_L_FIXTURE]);

   // Total other
   LONGLONG lOthers = lPers + lFixt;
   if (lOthers > 0)
   {
      sprintf(acTmp, "%*u", SIZ_OTHER, lOthers);
      memcpy(pOutbuf+OFF_OTHER, acTmp, SIZ_OTHER);

      if (lPers > 0)
      {
         sprintf(acTmp, "%*u", SIZ_PERSPROP, lPers);
         memcpy(pOutbuf+OFF_PERSPROP, acTmp, SIZ_PERSPROP);
      }
      if (lFixt > 0)
      {
         sprintf(acTmp, "%*u", SIZ_FIXTR, lFixt);
         memcpy(pOutbuf+OFF_FIXTR, acTmp, SIZ_FIXTR);
      }
   }

   // Gross
   lTmp = lOthers + lLand + lImpr;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*u", SIZ_GROSS, lTmp);
      memcpy(pOutbuf+OFF_GROSS, acTmp, SIZ_GROSS);
   }

   // HO Exempt
   long lHOExe = atol(apTokens[SAC_L_HO_EX]);
   long lOtherExe = atol(apTokens[SAC_L_OTHER_EXE]);
   *(pOutbuf+OFF_HO_FL) = '2';         // N
   if (lHOExe > 0 || lOtherExe > 0)
   {
      // Use owner code for exemption code.  There is no combined exemption in SAC.
      memcpy(pOutbuf+OFF_EXE_CD1, apTokens[SAC_L_OWNER_CODE], strlen(apTokens[SAC_L_OWNER_CODE]));
      sprintf(acTmp, "%*u", SIZ_EXE_TOTAL, lHOExe+lOtherExe);
      memcpy(pOutbuf+OFF_EXE_TOTAL, acTmp, SIZ_EXE_TOTAL);
      if (lHOExe > 0)
         *(pOutbuf+OFF_HO_FL) = '1';         // Y

      // Create exemption type
      pTmp = makeExeType(pOutbuf+OFF_EXE_TYPE, pOutbuf+OFF_EXE_CD1, pOutbuf+OFF_EXE_CD2, pOutbuf+OFF_EXE_CD3, (IDX_TBL4 *)&SAC_Exemption);
      if (!pTmp)
         LogMsg("*** Unknown ExeCode: %s [%.14s]", apTokens[SAC_L_OWNER_CODE], pOutbuf);
   } 

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "019342302", 9))
   //   iTmp = 0;
#endif

   if (*apTokens[SAC_L_LAND_USE_CODE] > ' ')
   {
      vmemcpy(pOutbuf+OFF_USE_CO, apTokens[SAC_L_LAND_USE_CODE], SIZ_USE_CO);

      // Special case for AG009A
      if (!memcmp(apTokens[SAC_L_LAND_USE_CODE], "AG009A", 6))
         memcpy(pOutbuf+OFF_USE_STD, "102", 3);
      // Check for Ag preserved
      else if (*apTokens[SAC_L_LAND_USE_CODE] == 'H' && apTokens[SAC_L_LAND_USE_CODE][5] == 'G')
      {
         *(pOutbuf+OFF_AG_PRE) = 'Y';
         memcpy(pOutbuf+OFF_USE_STD, "401", 3);
      } else
         updateStdUse(pOutbuf+OFF_USE_STD, apTokens[SAC_L_LAND_USE_CODE], LSIZ_LAND_USE, pOutbuf);
   } else
      memcpy(pOutbuf+OFF_USE_STD, USE_UNASGN, SIZ_USE_STD);

   // Merge Situs Addr
   //Sac_MergeSAdr(pOutbuf, apTokens[SAC_L_SITUS_NUMBER], apTokens[SAC_L_SITUS_STREET], apTokens[SAC_L_SITUS_CITY], apTokens[SAC_L_SITUS_ZIP]);

   // Merge Mailing Addr
   Sac_MergeMAdr(pOutbuf, apTokens[SAC_L_MAIL_ADDRESS], apTokens[SAC_L_MAIL_CITY], apTokens[SAC_L_MAIL_STATE], apTokens[SAC_L_MAIL_ZIP]);

   // Owner
   Sac_MergeOwner(pOutbuf, apTokens[SAC_L_OWNER]);

   // Care Of
   if (*apTokens[SAC_L_CARE_OF] > ' ')
      updateCareOf(pOutbuf, apTokens[SAC_L_CARE_OF], strlen(apTokens[SAC_L_CARE_OF]));

   // Neighborhood code
   vmemcpy(pOutbuf+OFF_NBH_CODE, apTokens[SAC_L_NGH], SIZ_NBH_CODE);

   // Zoning
   if (*apTokens[SAC_L_ZONING] > ' ')
      Sac_UpdateZoning(pOutbuf, apTokens[SAC_L_ZONING], ' ');

   // Transfer - YYYYMMDD
   if (memcmp(apTokens[SAC_L_RECORDING_DATE], "000000", 6))
   {
      memcpy(pOutbuf+OFF_TRANSFER_DT, apTokens[SAC_L_RECORDING_DATE], SIZ_TRANSFER_DT);

      lTmp = atol(apTokens[SAC_L_RECORDING_DATE]);
      if (lTmp > 19000101)
      {
         iTmp = sprintf(acTmp, "%s%s", apTokens[SAC_L_RECORDING_DATE]+2, apTokens[SAC_L_RECORDING_PAGE]);
         memcpy(pOutbuf+OFF_TRANSFER_DOC, acTmp, iTmp);
      }
   }

   return 0;
}

/******************************** Sac_Load_LDR ******************************
 *
 * Load LDR file in CSV format 2019_secured_roll.txt
 *
 ****************************************************************************/

int Sac_Load_LDR2(char *pInFile, int iFirstRec /* 1=create header rec */)
{
   char     *pTmp, acBuf[MAX_RECSIZE], acRollRec[MAX_RECSIZE], acTmpFile[_MAX_PATH];
   char     acOutFile[_MAX_PATH], acSitusFile[_MAX_PATH], acLandFile[_MAX_PATH], acLegalFile[_MAX_PATH];

   HANDLE   fhOut;

   int      iRet;
   DWORD    nBytesWritten;
   BOOL     bRet;
   long     lRet=0, lCnt=0;

   LogMsg0("Loading SAC lien roll ...");

   // Open LDR file
   LogMsg("Open LDR file %s", pInFile);
   fdRoll = fopen(pInFile, "r");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening LDR file: %s\n", pInFile);
      return -2;
   }

   // Open Char file
   if (!_access(acCChrFile, 0))
   {
      LogMsg("Open Char file %s", acCChrFile);
      fdCChr = fopen(acCChrFile, "r");
      if (fdCChr == NULL)
      {
         LogMsg("***** Error opening Char file: %s\n", acCChrFile);
         return -2;
      }
   }

   // Open AltApn file
   GetIniString(myCounty.acCntyCode, "AltApn", "", acOutFile, _MAX_PATH, acIniFile);
   if (!_access(acOutFile, 0))
   {
      LogMsg("Open AltApn file %s", acOutFile);
      fdApn = fopen(acOutFile, "r");
      if (fdApn == NULL)
      {
         LogMsg("***** Error opening AltApn file: %s\n", acOutFile);
         return 2;
      }
   } else
      LogMsg("*** AltApn file is not available (%s).  Skip update AltApn.");

   // Open Situs file
   iRet = GetIniString(myCounty.acCntyCode, "SitusFile", "", acSitusFile, _MAX_PATH, acIniFile);
   if (iRet < 10)
   {
      LogMsg("***** Error Situs file not defined in INI file");
      return -3;
   }
   LogMsg("Open Situs file %s", acSitusFile);
   sprintf(acTmpFile, "%s\\%s\\%s_Situs.srt", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   lRet = sortFile(acSitusFile, acTmpFile, "S(#2,C,A) DEL(124) ");
   fdSitus = fopen(acTmpFile, "r");
   if (fdSitus == NULL)
   {
      LogMsg("***** Error opening Situs file: %s\n", acTmpFile);
      return -2;
   }

   // Lot size from county
   GetIniString(myCounty.acCntyCode, "LandFile", "", acLandFile, _MAX_PATH, acIniFile);
   if (!_access(acLandFile, 0))
   {
      LogMsg("Open Landsize file %s", acLandFile);
      fdLand = fopen(acLandFile, "r");
      if (fdLand == NULL)
      {
         LogMsg("***** Error opening Landsize file: %s\n", acLandFile);
         return 2;
      }
      iLandSize = GetPrivateProfileInt(myCounty.acCntyCode, "LandRecSize", 32, acIniFile);
   }

   // Legal
   sprintf(acLegalFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "Legal");
   LogMsg("Open legal file %s", acLegalFile);
   fdLegal = fopen(acLegalFile, "r");
   if (fdLegal == NULL)
   {
      LogMsg("***** Error opening legal file: %s\n", acLegalFile);
      return 2;
   }

   // Open Output file
   sprintf(acOutFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "R01");
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error creating output file: %s\n", acOutFile);
      return -4;
   }

   // Output first header record
   if (iFirstRec > 0)
   {
      memset(acBuf, '9', iRecLen);
      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
   }

   // Skip header
   pTmp = fgets(acRollRec, MAX_RECSIZE, fdRoll);

   // Init variables
   iNoMatch=iBadCity=iBadSuffix=0;
   lApnSkip = 0;
   lApnMatch= 0;

   // Merge loop
   while (!feof(fdRoll))
   {
      pTmp = fgets(acRollRec, MAX_RECSIZE, fdRoll);
      if (!pTmp)
         break;

      // Create new R01 record
      iRet = Sac_CreateRollRec2(acBuf, acRollRec);
      if (!iRet)
      {
         // Merge Char
         if (fdCChr)
            iRet = Sac_MergeCChar(acBuf);

         // Merge Situs
         if (fdSitus)
            iRet = Sac_MergeSitus(acBuf);

         // Merge lot area
         if (fdLand)
            iRet = Sac_MergeLand(acBuf);

         // Merge legal
         if (fdLegal)
            iRet = Sac_MergeLegal(acBuf);

         // Merge AltApn
         if (fdApn)
         {
            iRet = Sac_MergeAltApn(acBuf);
            // If no match, copy APN to AltApn
            if (iRet)
               memcpy(&acBuf[OFF_ALT_APN], &acBuf[OFF_APN_S], iApnLen);
         } else
            memcpy(&acBuf[OFF_ALT_APN], &acBuf[OFF_APN_S], iApnLen);

         // Save last recording date
         iRet = atoin((char *)&acBuf[OFF_TRANSFER_DT], 8);
         if (iRet > lLastRecDate && iRet < lToday)
            lLastRecDate = iRet;

         lLDRRecCount++;
         bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
         if (!bRet)
         {
            LogMsg("Error writing to output file at record %d\n", lCnt);
            lRet = WRITE_ERR;
            break;
         }
      } else
         LogMsg("*** Drop LDR record %d (%.20s)", lCnt+1, acRollRec);

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdApn)
      fclose(fdApn);
   if (fdRoll)
      fclose(fdRoll);
   if (fdCChr)
      fclose(fdCChr);
   if (fdSitus)
      fclose(fdSitus);
   if (fdLand)
      fclose(fdLand);

   if (fhOut)
      CloseHandle(fhOut);

   LogMsg("Total input records:        %u", lCnt);
   LogMsg("Total output records:       %u", lLDRRecCount);
   LogMsg("Total bad-city records:     %u", iBadCity);
   LogMsg("Total bad-suffix records:   %u\n", iBadSuffix);

   LogMsg("Total Char matched:         %u", lCharMatch);
   LogMsg("Total Char skipped:         %u\n", lCharSkip);
   LogMsg("Total Situs matched:        %u", lSitusMatch);
   LogMsg("Total Situs skipped:        %u\n", lSitusSkip);
   LogMsg("Total Land matched:         %u", lLandMatch);
   LogMsg("Total Land skipped:         %u\n", lLandSkip);
   LogMsg("Total AltApn matched:       %u", lApnMatch);
   LogMsg("Total AltApn skipped:       %u\n", lApnSkip);
   LogMsg("Total legal matched:        %u", lLegalMatch);
   LogMsg("Last recording date:        %u\n", lLastRecDate);

   printf("\nTotal output records: %u\n", lLDRRecCount);

   lRecCnt = lLDRRecCount;
   return 0;
}

/****************************** Sac_Load_LDR3 *******************************
 *
 * Load LDR file in CSV format yyyy_secured_roll.txt using values from yyyy_equalized_roll.txt
 *
 ****************************************************************************/

int Sac_Load_LDR3(char *pInFile, int iFirstRec /* 1=create header rec */)
{
   char     *pTmp, acBuf[MAX_RECSIZE], acRollRec[MAX_RECSIZE], acTmpFile[_MAX_PATH], acEQRFile[_MAX_PATH],
            acOutFile[_MAX_PATH], acSitusFile[_MAX_PATH], acLandFile[_MAX_PATH], acLegalFile[_MAX_PATH];

   HANDLE   fhOut;

   int      iRet;
   DWORD    nBytesWritten;
   BOOL     bRet, bUseAlt;
   long     lRet=0, lCnt=0;

   LogMsg0("Loading SAC lien roll ...");

   // Open LDR file
   LogMsg("Open LDR file %s", pInFile);
   fdRoll = fopen(pInFile, "r");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening LDR file: %s\n", pInFile);
      return -2;
   }

   // Equalized roll - 2021
   GetIniString(myCounty.acCntyCode, "FinalEQRFile", "", acEQRFile, _MAX_PATH, acIniFile);
   if (_access(acEQRFile, 0))
   {
      LogMsg("*** Final EQ file \"%s\"not present.\n    Use current EQ roll instead.", acEQRFile);
      GetIniString(myCounty.acCntyCode, "EQRollFile", "", acEQRFile, _MAX_PATH, acIniFile);
      bUseAlt = true;
   }

   if (!_access(acEQRFile, 0))
   {
      sprintf(acBuf, "%s\\%s\\%s_EQRoll.srt", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
      lRet = sortFile(acEQRFile, acBuf, "S(#1,C,A,#2,C,A,#3,C,A,#4,C,A) DEL(124) ");

      LogMsg("Open Equalized file %s", acBuf);
      fdEq = fopen(acBuf, "r");
      if (fdEq == NULL)
      {
         LogMsg("***** Error opening Equalized file: %s\n", acBuf);
         return -2;
      }
   } else
      fdEq = NULL;

   // Open Char file
   if (!_access(acCChrFile, 0))
   {
      LogMsg("Open Char file %s", acCChrFile);
      fdCChr = fopen(acCChrFile, "r");
      if (fdCChr == NULL)
      {
         LogMsg("***** Error opening Char file: %s\n", acCChrFile);
         return -2;
      }
   }

   // Open AltApn file
   GetIniString(myCounty.acCntyCode, "AltApn", "", acOutFile, _MAX_PATH, acIniFile);
   if (!_access(acOutFile, 0))
   {
      LogMsg("Open AltApn file %s", acOutFile);
      fdApn = fopen(acOutFile, "r");
      if (fdApn == NULL)
      {
         LogMsg("***** Error opening AltApn file: %s\n", acOutFile);
         return 2;
      }
   } else
      LogMsg("*** AltApn file is not available (%s).  Skip update AltApn.");

   // Open Situs file
   iRet = GetIniString(myCounty.acCntyCode, "SitusFile", "", acSitusFile, _MAX_PATH, acIniFile);
   if (iRet < 10)
   {
      LogMsg("***** Error Situs file not defined in INI file");
      return -3;
   }
   LogMsg("Open Situs file %s", acSitusFile);
   sprintf(acTmpFile, "%s\\%s\\%s_Situs.srt", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   lRet = sortFile(acSitusFile, acTmpFile, "S(#2,C,A) DEL(124) ");
   fdSitus = fopen(acTmpFile, "r");
   if (fdSitus == NULL)
   {
      LogMsg("***** Error opening Situs file: %s\n", acTmpFile);
      return -2;
   }

   // Lot size from county
   GetIniString(myCounty.acCntyCode, "LandFile", "", acLandFile, _MAX_PATH, acIniFile);
   if (!_access(acLandFile, 0))
   {
      LogMsg("Open Landsize file %s", acLandFile);
      fdLand = fopen(acLandFile, "r");
      if (fdLand == NULL)
      {
         LogMsg("***** Error opening Landsize file: %s\n", acLandFile);
         return 2;
      }
      iLandSize = GetPrivateProfileInt(myCounty.acCntyCode, "LandRecSize", 32, acIniFile);
   }

   // Legal
   sprintf(acLegalFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "Legal");
   LogMsg("Open legal file %s", acLegalFile);
   fdLegal = fopen(acLegalFile, "r");
   if (fdLegal == NULL)
   {
      LogMsg("***** Error opening legal file: %s\n", acLegalFile);
      return 2;
   }

   // Open Output file
   sprintf(acOutFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "R01");
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error creating output file: %s\n", acOutFile);
      return -4;
   }

   // Output first header record
   if (iFirstRec > 0)
   {
      memset(acBuf, '9', iRecLen);
      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
   }

   // Skip header
   pTmp = fgets(acRollRec, MAX_RECSIZE, fdRoll);

   // Init variables
   iNoMatch=iBadCity=iBadSuffix=0;
   lApnSkip = 0;
   lApnMatch= 0;

   // Merge loop
   while (!feof(fdRoll))
   {
      pTmp = fgets(acRollRec, MAX_RECSIZE, fdRoll);
      if (!pTmp)
         break;

      // Create new R01 record
      iRet = Sac_CreateRollRec2(acBuf, acRollRec);
      if (!iRet)
      {
         // Merge Equalized record
         if (fdEq)
         {
            if (bUseAlt)
               iRet = Sac_MergeEUVal(acBuf);
            else
               iRet = Sac_MergeELVal(acBuf);
         }

         // Merge Char
         if (fdCChr)
            iRet = Sac_MergeCChar(acBuf);

         // Merge Situs
         if (fdSitus)
            iRet = Sac_MergeSitus(acBuf);

         // Merge lot area
         if (fdLand)
            iRet = Sac_MergeLand(acBuf);

         // Merge legal
         if (fdLegal)
            iRet = Sac_MergeLegal(acBuf);

         // Merge AltApn
         if (fdApn)
         {
            iRet = Sac_MergeAltApn(acBuf);
            // If no match, copy APN to AltApn
            if (iRet)
               memcpy(&acBuf[OFF_ALT_APN], &acBuf[OFF_APN_S], iApnLen);
         } else
            memcpy(&acBuf[OFF_ALT_APN], &acBuf[OFF_APN_S], iApnLen);

         // Save last recording date
         iRet = atoin((char *)&acBuf[OFF_TRANSFER_DT], 8);
         if (iRet > lLastRecDate && iRet < lToday)
            lLastRecDate = iRet;

         lLDRRecCount++;
         bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
         if (!bRet)
         {
            LogMsg("Error writing to output file at record %d\n", lCnt);
            lRet = WRITE_ERR;
            break;
         }
      } else
         LogMsg("*** Drop LDR record %d (%.20s)", lCnt+1, acRollRec);

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdApn)
      fclose(fdApn);
   if (fdEq)
      fclose(fdEq);
   if (fdRoll)
      fclose(fdRoll);
   if (fdCChr)
      fclose(fdCChr);
   if (fdSitus)
      fclose(fdSitus);
   if (fdLand)
      fclose(fdLand);

   if (fhOut)
      CloseHandle(fhOut);

   LogMsg("Total input records:        %u", lCnt);
   LogMsg("Total output records:       %u", lLDRRecCount);
   LogMsg("Total bad-city records:     %u", iBadCity);
   LogMsg("Total bad-suffix records:   %u\n", iBadSuffix);

   LogMsg("Total Char matched:         %u", lCharMatch);
   LogMsg("Total Char skipped:         %u\n", lCharSkip);
   LogMsg("Total Situs matched:        %u", lSitusMatch);
   LogMsg("Total Situs skipped:        %u\n", lSitusSkip);
   LogMsg("Total Land matched:         %u", lLandMatch);
   LogMsg("Total Land skipped:         %u\n", lLandSkip);
   LogMsg("Total AltApn matched:       %u", lApnMatch);
   LogMsg("Total AltApn skipped:       %u\n", lApnSkip);
   LogMsg("Total legal matched:        %u", lLegalMatch);
   LogMsg("Last recording date:        %u\n", lLastRecDate);

   printf("\nTotal output records: %u\n", lLDRRecCount);

   lRecCnt = lLDRRecCount;
   return 0;
}

/************************* Sac_CreateFinalValueRec **************************
 *
 * Extract values from 2021_Equalized_Roll.txt
 *
 ****************************************************************************/

int Sac_CreateFinalValueRec(char *pOutbuf, char *pRollRec)
{
   char     acTmp[256], acApn[256];
   LONGLONG lTmp;
   int      iTmp;
   LIENEXTR *pLienRec = (LIENEXTR *)pOutbuf;

   iTmp = ParseStringNQ(pRollRec, ';', MAX_FLD_TOKEN, apTokens);
   if (iTmp < EL_COLS)
   {
      LogMsg("***** Error: bad input record for APN=%s-%s-%s-%s", apTokens[0], apTokens[1], apTokens[2], apTokens[3]);
      return -1;
   }

   // Clear output buffer
   memset(pOutbuf, ' ', sizeof(LIENEXTR));

   // Format APN
   iTmp = sprintf(acApn, "%s%s%s%s", apTokens[0], apTokens[1], apTokens[2], apTokens[3]);
   memcpy(pLienRec->acApn, acApn, iTmp);

   // Format TRA
   lTmp = atol(apTokens[EL_TRA]);
   iTmp = sprintf(acTmp, "%.6d", lTmp);
   memcpy(pLienRec->acTRA, acTmp, iTmp);

#ifdef _DEBUG
   //if (!memcmp(pLienRec->acApn, "00102315", 8) )
   //   iTmp = 0;
#endif

   // Year assessed
   memcpy(pLienRec->acYear, myCounty.acYearAssd, 4);

   // Land
   long lLand = atol(apTokens[EL_LAND]);
   if (lLand > 0)
   {
      iTmp = sprintf(acTmp, "%*u", sizeof(pLienRec->acLand), lLand);
      memcpy(pLienRec->acLand, acTmp, iTmp);
   }

   // Improve - Ratio
   long lImpr = atol(apTokens[EL_IMPR]);
   if (lImpr > 0)
   {
      iTmp = sprintf(acTmp, "%*u", sizeof(pLienRec->acImpr), lImpr);
      memcpy(pLienRec->acImpr, acTmp, iTmp);

      iTmp = sprintf(acTmp, "%*u", sizeof(pLienRec->acRatio), (LONGLONG)lImpr*100/(lLand+lImpr));
      memcpy(pLienRec->acRatio, acTmp, iTmp);
   }

   // PP value
   long lPers = atol(apTokens[EL_PP]);

   // Fixture
   long lFixt = atol(apTokens[EL_FIXTURE]);

   // Total other
   long lOthers = lPers + lFixt;
   if (lOthers > 0)
   {
      iTmp = sprintf(acTmp, "%*u", sizeof(pLienRec->acOther), lOthers);
      memcpy(pLienRec->acOther, acTmp, iTmp);

      if (lPers > 0)
      {
         iTmp = sprintf(acTmp, "%u", lPers);
         memcpy(pLienRec->acPP_Val, acTmp, iTmp);
      }
      if (lFixt > 0)
      {
         iTmp = sprintf(acTmp, "%u", lFixt);
         memcpy(pLienRec->acME_Val, acTmp, iTmp);
      }
   }

   // Gross
   lTmp = lOthers + lLand + lImpr;
   if (lTmp > 0)
   {
      iTmp = sprintf(acTmp, "%*u", sizeof(pLienRec->acGross), lTmp);
      memcpy(pLienRec->acGross, acTmp, iTmp);
   }

   // HO Exe 
   lTmp = atol(apTokens[EL_HO_EXE]);
   if (lTmp > 0)
   {
      iTmp = sprintf(acTmp, "%d", lTmp);
      memcpy(pLienRec->acExAmt, acTmp, iTmp);
      pLienRec->acHO[0] = '1';         // Y
   } else
      pLienRec->acHO[0] = '2';         // N

   lTmp = atol(apTokens[EL_TOTAL_EXE]);
   if (lTmp > 0)
   {
      iTmp = sprintf(acTmp, "%u", lTmp);
      memcpy(pLienRec->acExAmt, acTmp, iTmp);
      if (lTmp == 7000)
         pLienRec->acHO[0] = '1';      // Y
   } 

   pLienRec->LF[0] = '\n';
   pLienRec->LF[1] = 0;
   return 0;
}

/*************************** Sac_CreateAltValueRec **************************
 *
 * Extract values from city_full_equalized_roll.dos
 *
 ****************************************************************************/

int Sac_CreateAltValueRec(char *pOutbuf, char *pRollRec)
{
   char     acTmp[256];
   LONGLONG lTmp;
   int      iTmp;
   LIENEXTR *pLienRec = (LIENEXTR *)pOutbuf;

   iTmp = ParseStringNQ(pRollRec, '|', MAX_FLD_TOKEN, apTokens);
   if (iTmp < EC_COLS)
   {
      LogMsg("***** Error: bad input record for APN=%s", apTokens[0]);
      return -1;
   }

   // Clear output buffer
   memset(pOutbuf, ' ', sizeof(LIENEXTR));

   // Format APN
   memcpy(pLienRec->acApn, apTokens[EC_APN], strlen(apTokens[EC_APN]));

   // Format TRA
   //lTmp = atol(apTokens[SAC_L_TAX_RATE_AREA]);
   //iTmp = sprintf(acTmp, "%.6d", lTmp);
   //memcpy(pLienRec->acTRA, acTmp, iTmp);

#ifdef _DEBUG
   //if (!memcmp(pLienRec->acApn, "00102315", 8) )
   //   iTmp = 0;
#endif

   // Year assessed
   memcpy(pLienRec->acYear, myCounty.acYearAssd, 4);

   // Land
   long lLand = atol(apTokens[EC_LAND]);
   if (lLand > 0)
   {
      iTmp = sprintf(acTmp, "%*u", sizeof(pLienRec->acLand), lLand);
      memcpy(pLienRec->acLand, acTmp, iTmp);
   }

   // Improve - Ratio
   long lImpr = atol(apTokens[EC_IMPR]);
   if (lImpr > 0)
   {
      iTmp = sprintf(acTmp, "%*u", sizeof(pLienRec->acImpr), lImpr);
      memcpy(pLienRec->acImpr, acTmp, iTmp);

      iTmp = sprintf(acTmp, "%*u", sizeof(pLienRec->acRatio), (LONGLONG)lImpr*100/(lLand+lImpr));
      memcpy(pLienRec->acRatio, acTmp, iTmp);
   }

   // PP value
   long lPers = atol(apTokens[EC_PP]);

   // Fixture
   long lFixt = atol(apTokens[EC_FIXTURE]);

   // Total other
   long lOthers = lPers + lFixt;
   if (lOthers > 0)
   {
      iTmp = sprintf(acTmp, "%*u", sizeof(pLienRec->acOther), lOthers);
      memcpy(pLienRec->acOther, acTmp, iTmp);

      if (lPers > 0)
      {
         iTmp = sprintf(acTmp, "%u", lPers);
         memcpy(pLienRec->acPP_Val, acTmp, iTmp);
      }
      if (lFixt > 0)
      {
         iTmp = sprintf(acTmp, "%u", lFixt);
         memcpy(pLienRec->acME_Val, acTmp, iTmp);
      }
   }

   // Gross
   lTmp = lOthers + lLand + lImpr;
   if (lTmp > 0)
   {
      iTmp = sprintf(acTmp, "%*u", sizeof(pLienRec->acGross), lTmp);
      memcpy(pLienRec->acGross, acTmp, iTmp);
   }

   // HO Exe 
   lTmp = atol(apTokens[EC_HO_EXE]);
   if (lTmp > 0)
   {
      pLienRec->acHO[0] = '1';         // Y
      iTmp = sprintf(acTmp, "%u", lTmp);
      memcpy(pLienRec->acExAmt, acTmp, iTmp);
   } else
   {
      pLienRec->acHO[0] = '2';         // N
      lTmp = atol(apTokens[EC_OTH_EXE]);
      if (lTmp > 0)
      {
         iTmp = sprintf(acTmp, "%u", lTmp);
         memcpy(pLienRec->acExAmt, acTmp, iTmp);
      } 
   }

   pLienRec->LF[0] = '\n';
   pLienRec->LF[1] = 0;
   return 0;
}

/********************************* Sac_ExtrVal ******************************
 *
 * Extract values from EQ roll
 *
 ****************************************************************************/

int Sac_ExtrVal()
{
   char     *pTmp, acBuf[1024], acRec[MAX_RECSIZE], acTmpFile[_MAX_PATH];
   int      iRet;
   bool     bUseAlt=false;
   long     lOut=0, lCnt=0;
   FILE     *fdOut, *fdIn;

   LogMsg0("Extracting final values from equalized file.");

   // Open input file
   GetIniString(myCounty.acCntyCode, "FinalEQRFile", "", acTmpFile, _MAX_PATH, acIniFile);
   if (_access(acTmpFile, 0))
   {
      LogMsg("*** Final EQ file \"%s\"not present.\n    Use current EQ roll instead.", acTmpFile);
      GetIniString(myCounty.acCntyCode, "EQRollFile", "", acTmpFile, _MAX_PATH, acIniFile);
      bUseAlt = true;
   }

   LogMsg("Open %s for input", acTmpFile);
   fdIn = fopen(acTmpFile, "r");
   if (fdIn == NULL)
   {
      LogMsg("***** Error opening input file: %s\n", acTmpFile);
      return -2;
   }  

   // Open out file
   sprintf(acTmpFile, acValueTmpl, myCounty.acCntyCode, myCounty.acCntyCode);
   LogMsg("Open file %s for output", acTmpFile);
   fdOut = fopen(acTmpFile, "w");
   if (fdOut == NULL)
   {
      LogMsg("***** Error creating output file: %s\n", acTmpFile);
      return -4;
   }

   // Drop header
   pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdIn);

   // Start loop 
   while (!feof(fdIn))
   {
      pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdIn);
      if (!pTmp)
         break;

      // Create FV record
      if (bUseAlt)
         iRet = Sac_CreateAltValueRec(acBuf, acRec);
      else
         iRet = Sac_CreateFinalValueRec(acBuf, acRec);
      if (!iRet)
      {
         fputs(acBuf, fdOut);
         lOut++;
      } else
      {
         LogMsg("---> Drop record %d ", lCnt); 
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   LogMsg("Total records processed: %u", lCnt);
   LogMsg("Total records output:    %u", lOut);

   // Close files
   if (fdIn)
      fclose(fdIn);
   if (fdOut)
      fclose(fdOut);

   return iRet;
}

/*********************************** loadSac ********************************
 *
 * Input files:
 *    - gae7000_rollpubl.txt        (roll file, 375-byte ebcdic)
 *    - gax2100_apprpubl.txt        (char file, 750-byte ebcdic)
 *    - gax9300_salesmstr.txt       (sale file, 850-byte ebcdic)
 *    - landsize.txt                (addl attr file, delimited ascii)
 *    - aimsab_public.txt           (LDR file, 299-byte ascii)
 *
 * Following are tasks to be done here:
 *    - Convert ebcdic to ascii
 *    - Convert landsize.txt (delimited) to landsize.dat (fixed length) -Ll
 *    - Normal update: LoadOne -U -Us -Ll [-Xa] [-T] [-Mr]
 *    - Load Lien: LoadOne -CSAC -L -Us -Ll -Xl [-Mr]
 *    - Update situs city using MergeAdr -CSAC -A
 *    - Use -Ur to replace LDR values with current values for Prop8 report
 *
 ****************************************************************************/

int loadSac(int iSkip)
{
   int   iRet=0, iTmp;
   char  acTmpFile[_MAX_PATH], acTmp[_MAX_PATH];

   iApnLen = myCounty.iApnLen;

   // To be used when needed
   //ExtractOwnerCode();

   if (iLoadTax == TAX_LOADING)                    // -T or -Lt
   {
      // Loading tax district file
      iRet = GetIniString(myCounty.acCntyCode, "TaxDist", "", acTmpFile, _MAX_PATH, acIniFile);
      if (iRet > 0)
         iRet = LoadTaxCodeTable(acTmpFile);

      // Loading tax rate file if available
      iRet = GetIniString(myCounty.acCntyCode, "TaxRate", "", acTmpFile, _MAX_PATH, acIniFile);
      if (iRet > 0 && !_access(acTmpFile, 0))
      {
         iRet = LoadTaxRateTable(acTmpFile);
         if (iRet < 100)
            LogMsg("***** Bad tax rate table %s.  Please verify", acTmpFile);
      }

      iRet = GetIniString(myCounty.acCntyCode, "TaxRollInfo", "", acTmpFile, _MAX_PATH, acIniFile);
      iTmp = (UINT)getFileSize(acTmpFile);
      if (iTmp > 300000000)
      {
         iRet = Sac_Load_Cortac(bTaxImport);
         if (!iRet)
            iRet = updateDelqFlag(myCounty.acCntyCode);
      } else
      {
UpdateTax:         
         // Update tax base using yyyymmdd_TAXJ2710.txt
         iRet = Sac_Update_TaxBase(bTaxImport);

         // Update tax delq using yyyymmdd_RDMJ3410.txt
         iRet = Sac_Load_TaxDelq(bTaxImport);

         // Merge Tmp table to Base 
         if (!iRet)
            iRet = doTaxMerge(myCounty.acCntyCode, false, false, false);

         if (!iRet)
            iRet = updateDelqFlag(myCounty.acCntyCode, true, true, 2);
      }
   } else if (iLoadTax == TAX_UPDATING) // -Ut
      goto UpdateTax;

   if (!iLoadFlag && !lOptExtr)
      return 0;

   // Extract final value
   if (lOptExtr & EXTR_FVAL)                       // -Xf
      iRet = Sac_ExtrVal();

   // Prepare lien file
   if (iLoadFlag & (EXTR_LIEN|LOAD_LIEN))
   {
      // Rebuild CSV file to fix broken lines
      sprintf(acTmpFile, "%s\\SAC\\SAC_LDR.txt", acTmpPath);
      iRet = RebuildCsv(acLienFile, acTmpFile, ';', SAC_L_FLDS);
      if (iRet < 400000)
         return iRet;

      strcpy(acLienFile, acTmpFile);
   }

   // Prepare roll file
   iRet = 0;
   if (iLoadFlag & (EXTR_DESC|LOAD_UPDT))                      
   {
      // Rebuild CSV file to fix broken lines
      GetIniString(myCounty.acCntyCode, "RollFile", "", acRollFile, _MAX_PATH, acIniFile);
      sprintf(acTmpFile, "%s\\SAC\\Sac_Roll.txt", acTmpPath);
      iRet = RebuildCsv_IQ(acRollFile, acTmpFile, '|', P_LOAD_DATE+1);
      if (iRet > 400000)
      {
         sprintf(acTmp, "%s\\SAC\\Sac_Roll.srt", acTmpPath);
         // Sort new file
         iTmp = sortFile(acTmpFile, acTmp, "S(#2,C,A) DEL(124) F(TXT)");
         if (iTmp <= 0)
            return -1;
         strcpy(acRollFile, acTmp);
      } else
         return -1;
   }

   // Extract legal.  Use as needed
   if (iLoadFlag & EXTR_DESC)                      // -Xd
   {
      iRet = Sac_ExtrLegal(acRollFile);
   }

   // Old format pre 2019
   // char acDefFile[_MAX_PATH];
   //if (iLoadFlag & EXTR_DESC)                      // -Xd
   //{
   //   // Translate long legal file ebcdic to ascii if new file available
   //   sprintf(acLongLegalFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "Legal");
   //   GetIniString(myCounty.acCntyCode, "LongLegal", "", acTmpFile, _MAX_PATH, acIniFile);
   //   if (!_access(acTmpFile, 0))
   //   {
   //      iTmp = GetPrivateProfileInt(myCounty.acCntyCode, "LegalLen", 175, acIniFile);
   //      LogMsg("Translate %s to Ascii %s", acTmpFile, acLongLegalFile);
   //      iRet = F_Ebc2Asc(acTmpFile, acLongLegalFile, iTmp);
   //      if (iRet < 0)
   //      {
   //         dispError(iRet, acTmpFile, acCharFile, acDefFile);
   //         return 1;
   //      }
   //   }

   //   if (!_access(acLongLegalFile, 0))
   //   {
   //      iRet = GetIniString(myCounty.acCntyCode, "LegalFile", "", acTmpFile, _MAX_PATH, acIniFile);
   //      iRet = Sac_LegalExtr(acLongLegalFile, acTmpFile);
   //   } else
   //      LogMsg("*** Legal file (%s) not available.  Ignore -Xd", acLongLegalFile);
   //}

   // Load tables
   //if (!LoadLUTable((char *)&acLookupTbl[0], "[Quality]", NULL, MAX_ATTR_ENTRIES))
   //{
   //   LogMsg("*** Error Looking for table [Quality] in %s", acLookupTbl);
   //   return 1;
   //}

   // Fix DocType whenever SAC_DocCode table is changed
   if (lOptMisc & M_OPT_FIXDOCT)
   {
      iRet = FixDocType(acCSalFile, (IDX_TBL5 *)&SAC_DocCode[0], true /* remove rec w/o DocNum */);
      if (iRet)
         return iRet;
   }

   // This has to be done before roll update
   if (iLoadFlag & EXTR_SALE)                      // -Xs
   {
      // New format
      //sprintf(acTmpFile, "%s\\%s\\%s_Sales.asc", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode); 
      //iRet = sortFile(acSaleFile, acTmpFile, "S(#2,C,A) DEL(124)");
      //if (iRet <= 0)
      //   return 1;
      //strcpy(acSaleFile, acTmpFile);

      // Update history sale sale
      iRet = Sac_ExtrSale();                       // Output Sac_Sales.sls       using SCSAL_REC
      if (!iRet)
         iLoadFlag |= MERG_CSAL;
   }

   //if (iLoadFlag & UPDT_SALE)                      // -Us
   //{
      // 20180102 - Old format
      //sprintf(acTmpFile, "%s\\%s\\%s_Sales.asc", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode); 
      //sprintf(acTmp, "S(1,14,C,A) F(FIX,%d) OUTREC(%%ETOA,1,%d,CRLF)  ALT(C:\\TOOLS\\EBCDIC.ALT)", iSaleLen, iSaleLen);
      //iRet = sortFile(acSaleFile, acTmpFile, acTmp);
      //if (iRet <= 0)
      //   return 1;

      //// Update history sale sale
      //strcpy(acSaleFile, acTmpFile);
      //LogMsg("Update %s sale history file", myCounty.acCntyCode);
      //iRet = Sac_UpdateCumSale();      // Output Sac_Sales.sls       using SCSAL_REC
      //if (!iRet)
      //   iLoadFlag |= MERG_CSAL;
   //}

   // Translate to ascii
   //if (iLoadFlag & (LOAD_LIEN|LOAD_UPDT))
   //{
      // Translate Roll file ebcdic to ascii
      //GetIniString(myCounty.acCntyCode, "RollDef", "", acDefFile, _MAX_PATH, acIniFile);
      //if (_access(acDefFile, 0))
      //{
      //   LogMsg("***** Error: missing definition file %s", acDefFile);
      //   return 1;
      //}

      //strcpy(acTmpFile, acRollFile);
      //pTmp = strrchr(acRollFile, '.');
      //strcpy(++pTmp, "ASC");
      //if (bEbc2Asc)
      //{
      //   LogMsg("Translate %s to Ascii %s", acTmpFile, acRollFile);
      //   iRet = F_Ebc2Asc(acTmpFile, acRollFile, acDefFile, 0);
      //   if (iRet < 0)
      //   {
      //      dispError(iRet, acTmpFile, acRollFile, acDefFile);
      //      return 1;
      //   }
      //}

   //   // Translate char file ebcdic to ascii
   //   GetIniString(myCounty.acCntyCode, "CharDef", "", acDefFile, _MAX_PATH, acIniFile);
   //   if (_access(acDefFile, 0))
   //   {
   //      LogMsg("***** Error: missing definition file %s", acDefFile);
   //      return 1;
   //   }

   //   strcpy(acTmpFile, acCharFile);
   //   pTmp = strrchr(acCharFile, '.');
   //   strcpy(++pTmp, "ASC");
   //   if (bEbc2Asc)
   //   {
   //      LogMsg("Translate %s to Ascii %s", acTmpFile, acCharFile);
   //      iRet = F_Ebc2Asc(acTmpFile, acCharFile, acDefFile, 0);
   //      if (iRet < 0)
   //      {
   //         dispError(iRet, acTmpFile, acCharFile, acDefFile);
   //         return 1;
   //      }
   //   }
   //}

	// Load char file
   if (iLoadFlag & EXTR_ATTR)                      // -Xa
   {
      // Load comm char
      GetIniString(myCounty.acCntyCode, "CommChar", "", acTmpFile, _MAX_PATH, acIniFile);
      if (!_access(acTmpFile, 0))
         iRet = Sac_ConvCommChar2(acTmpFile);

      // Load residential char
      iRet = Sac_ConvStdChar2(acCharFile);
      if (iRet > 0)
         iLoadFlag |= MERG_ATTR;
   }

   if (iLoadFlag & EXTR_LIEN)                      // -Xl
      iRet = Sac_ExtrLien2(acLienFile);

   // Load land file
   if (iLoadFlag & LOAD_LAND)                      // -Ll
   {
      GetIniString(myCounty.acCntyCode, "LandCntyFile", "", acTmpFile, _MAX_PATH, acIniFile);
      iRet = Sac_LoadLand(acTmpFile);
   }

   // Merge roll file
   if (iLoadFlag & LOAD_LIEN)                      // -L
   {
      // Use this version to create extract for Lexis
      //iRet =Sac_Load_LDR1(iSkip); 

      // Set EQRollFile in INI file to create equalized LDR version
      //iRet = Sac_Load_LDR(iSkip);

      // 02/13/2019
      //iRet = Sac_Load_LDR1(iSkip);

      // 07/08/2019
      //iRet = Sac_Load_LDR2(acLienFile, iSkip);

      // 04/13/2022
      iRet = Sac_Load_LDR3(acLienFile, iSkip);
   } else if (iLoadFlag & LOAD_UPDT)               // -U
   {
      //if (iLoadFlag & LOAD_UNSEC)
      //{
         // Merge unsecured roll if exist.  Do not merge Unsec file into LDR
      //   GetIniString("SAC", "UnsFile", "", acTmpFile, _MAX_PATH, acIniFile);
      //   if (!_access(acTmpFile, 0))
      //      iRet= Sac_MergeUnsRoll(acTmpFile, iSkip);
      //}

      //GetIniString(myCounty.acCntyCode, "GisFile", "", acTmpFile, _MAX_PATH, acIniFile);
      //if (!_access(acTmpFile, 0))
      //   Sac_ConvGisFile(acTmpFile);

      // untar input file
      //GetIniString(myCounty.acCntyCode, "TarFile", "", acTmpFile, _MAX_PATH, acIniFile);
      //if (!_access(acTmpFile, 0))
      //{
      //   LogMsg0("Untar %s", acTmpFile);
      //   unTar("G:\\CO_DATA\\SAC\\city_full.tar");
      //}

      //// Rebuild CSV file
      //lLastFileDate = getFileDate(acRollFile);
      //sprintf(acTmpFile, "%s\\SAC\\Sac_Roll.txt", acTmpPath);
      //iRet = RebuildCsv(acRollFile, acTmpFile, '|', P_LOAD_DATE+1);
      //if (iRet > 400000)
      //{
      //   sprintf(acTmp, "%s\\SAC\\Sac_Roll.srt", acTmpPath);
      //   // Sort new file
      //   iTmp = sortFile(acTmpFile, acTmp, "S(#2,C,A) DEL(124) F(TXT)");
      //   if (iTmp <= 0)
      //      return -1;
      //   strcpy(acRollFile, acTmp);
      //} else
      //   return -1;

      iRet = Sac_Load_Roll2(iSkip);
      //iRet = Sac_Load_Roll(iSkip);
   }

   // Apply cum sales
   if (!iRet && (iLoadFlag & MERG_CSAL) )          // -Ms
   {
      sprintf(acCSalFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "sls");
      iRet = ApplyCumSale(iSkip, acCSalFile, false, SALE_USE_SCUPDXFR);
   }

   // Update Usecode
   if (!iRet && (iLoadFlag & UPDT_SUSE))
      iRet = updateR01StdUse(myCounty.acCntyCode, iSkip);
 
      // Fix TRA
   if (!iRet && (lOptMisc & M_OPT_FIXTRA))
      iRet = PQ_FixR01(myCounty.acCntyCode, PQ_FIX_TRA, 0, true);

   if (!iRet && bMergeOthers)
   {
      sprintf(acTmp, acLienTmpl, myCounty.acCntyCode, myCounty.acCntyCode);
      if (!_access(acTmp, 0))
         iRet = PQ_MergeOthers(myCounty.acCntyCode, acTmp, GRP_SAC, iSkip);
      else
         LogMsg("***** Lien extract is missing: %s.  Please locate the file and retry", acTmp);
   }

   return iRet;
}