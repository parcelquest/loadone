/*********************************** loadScr ********************************
 *
 * Input files:
 *    - AssessmentRollEbcdic.TXT    (roll file, ebcdic)
 *    - Scr_Utl_roll.txt            (public prop., ebcdic)
 *    - TransfersEbcdic.TXT         (sale/transfer file, ebcdic)
 *    - CD_DataMonthlySantaCruz.mdb (attribute file)
 *
 * Following are tasks to be done here:
 *    - Merge/sort roll file with SCR_UTIL if not already included
 *    - Normal update: LoadOne -U -Xsi -Xa
 *    - Load Lien: LoadOne -L -Xsi -Xa -Xl
 *    - Extract attr only: LoadOne -Xa
 * Notes:
 *    - MergeOwner needs rework to handle Name2 correctly (002-521-04)
 *
 *
 * 11/10/2007  1.4.30   If char file is a zip file, unzip it first before extract.
 * 02/18/2008  1.5.5.1  Fix bug that creates duplicate records when merging Lien
 *                      roll with public records.  Adding support Std Usecode.
 * 02/20/2008           Modify Scr_MergeAdr() to fix zipcode problem.  Also add
 *                      code to update Std Usecode for public/retired parcels.
 * 03/24/2008 1.5.7.2   Format IMAPLINK.
 * 03/25/2008 1.5.8     Remove retired parcels
 * 08/29/2008 8.1.5     Modify Scr_MergeOwner() to make Name1 the same as county provided.
 *                      Merge public properties on update also.
 * 12/11/2008 8.5.1     Make StrNum left justified
 * 01/17/2009 8.5.6     Fix TRA
 * 05/11/2009 8.8       Set full exemption flag
 * 08/06/2009 9.1.5     Add Scr_ExtrLien(), DO not display 0 LotSqft.  Populate other values.
 * 04/16/2010 9.5.4.1   Set NumOfPrclXfer='M' instead of actual number when multi prcl involved.
 * 07/28/2010 10.1.6    Remove Scr_MergeSale() from Scr_Load_LDR() and Scr_Load_Roll() and
 *                      use ApplyCumSale() to populate sale history. Modify Scr_ExtrSale()
 *                      to reformat DocNum (drop year) and use standard format SCSAL_REC instead
 *                      of SALE_REC.
 * 12/17/2010 10.3.7    Change logic to set FULL_EXEMPT='Y' where GROSS <= 2000.
 * 07/29/2011 11.0.5    Add S_HSENO
 * 10/07/2011 11.4.13   Remove reference to COO_Flag since there is no use for this.
 * 05/10/2012 11.9.5    Adding Scr_Load_Roll_New() to support upcoming data from SCR.
 *                      Change "NewLayout" value in INI file to activate this. This has
 *                      not been tested thoroughly.
 * 09/05/2012 12.2.7    Add Scr_ExtractProp8() to support -X8 option.
 * 01/22/2013 12.4.4    Tested with new data.
 *                      Fix bug in Scr_ExtrSale1() and coded QUIT CLAIM as DEED to be
 *                      compatible with county website.
 * 03/05/2013 12.4.7    Add CARE_OF.
 * 06/12/2013 12.7.2    Check for PUBL_FLG in parcel status file.
 * 06/24/2013 12.7.3    Replace all local use of apTokens[] with apItems[] to avoid conflict
 *                      with global use of this variables.
 * 06/25/2013 12.7.4    Fix duplicate APN problem in Scr_Load_Roll_New()
 * 07/29/2013 13.3.9    Add Scr_Load_LDR2() and Scr_ExtrLien2() for LDR 2013 new record layout.
 *                      Layout for value file change. Update CareOf and fix known bad char
 *                      in Scr_MergeMailAdr(). Change logic for calculating OtherValue by adding 
 *                      Personal+LivingImpr. Change logic for calculating Exempt amount by
 *                      subtracting Gross-NetValue. Removing leading space for many fields in
 *                      Scr_MergeChar2(). Pool is now contain number instead of Y/N. Heat is
 *                      now a description instead of code. Adding BldgCls, BldgQual, Stories & Topo.
 *                      Modify Scr_MergeLegalN() to update Status only when record is created.
 *                      Fix known bad char in Owner name.
 * 08/15/2013 13.5.11   Adding HVAC and Roof material.
 * 08/23/2013 13.6.14   Modify Scr_MergeLegalN() to exclude unsecured parcels.
 * 08/23/2013 13.7.13.1 Correct the number of output records
 * 10/10/2013 13.10.4.3 Use updateVesting() to update Vesting and Etal flag.
 *                      Add Scr_MergeEtal() but not using yet. Customers are ok with current data.
 * 07/30/2014 14.2.1    Modify Scr_Load_LDR2() to deal with broken records.  Also modify Scr_MergeChar2,
 *                      Scr_MergeTax2, Scr_MergeValue, Scr_MergeSitusAdr, and Scr_MergeMailAdr to
 *                      correctly compare variable length APN. This gives us much better match rate.
 * 01/29/2015 14.12.0   Break M_StrSub from M_StrNum and put it in appropriate place.
 * 07/13/2015 15.0.4    Bug fix in Scr_Load_LDR2() that sends wrong number of items to RebuildCsv().
 * 07/30/2015 15.0.7    Modify Scr_Load_Roll2() to fix legal before sorting it (some records are broken)
 * 10/16/2015 15.3.2    Fix bug in Scr_MergeTax2() by using ParseStringIQ() to avoid embedded double quote 
 * 04/06/2016 15.8.3    Load Tax data.
 * 12/19/2016 16.8.3    Set VIEW='A' when HasView='Y'
 * 01/16/2017 16.9.5    Add functions to load new tax files.  Create Tax_Base, Tax_Items, & Tax_Agency.
 *                      SCC_GetDefaultedRedeemedInfoForTaxServices.txt needs review, will process later.
 * 03/12/2017 16.12.0   Modify Scr_ParseTaxBase() to update TotalDue & instStatus.
 * 06/02/2017 16.14.16  Modify Scr_Load_TaxRoll() to update TRA using R01 file.
 * 06/10/2017 16.14.17  Add Scr_Load_TaxDelq() to load delq file.
 * 01/11/2018 17.6.0    Modify Scr_ParseTaxDetail() to trim all fields. Set lTaxYear in Scr_ParseTaxBase().
 * 07/18/2018 18.2.1    Modify Scr_MergeChar2() to populate BATH_2Q and BATH_4Q on R01
 * 08/08/2018 18.2.7    Update Tax_Base only if number of records is greater than 90,000 to fix problem
 *                      when the file is empty.
 * 07/09/2020 20.1.3    Modify Scr_MergeOwner() & Scr_MergeSitusAdr() to remove/replace special char.
 * 07/12/2020 20.1.4    Modify Scr_MergeValue() & Scr_CreateLienRec2() to ass exemption code.
 *                      Modify Scr_ExtrChar() to output standard Scr_Char.dat instead of Scr_Attr.dat
 * 10/30/2020 20.4.1    Modify Scr_MergeChar2() to populate PQZoning.
 * 07/12/2021 21.0.3    Modify Scr_MergeChar2() & Scr_ExtrChar() to add more available items (Heating, Cooling, BldgSeqNo, ...).
 * 02/15/2022 21.4.12   Validate DocNum in Scr_ExtrSale2() and modify Scr_Load_Roll2() to use RebuildCsv_IQ().
 * 03/03/2022 21.6.0    Use updateTotalTaxRate() to update tb_TotalRate().
 * 09/06/2022 22.2.1    Modify Scr_MergeValue() to fix OTH_IMPR that over flow into FIXTR_RP.
 *                      Modify Scr_CreateLienRec2() to reset buffer before update.
 * 09/14/2022 22.2.3    Modify Scr_MergeSitusAdr() to fix UNIT# and accept situs even no STRNUM.
 *                      Fix mail addr in Scr_MergeMailAdr() when no STRNAME.  The county put PO BOX into STRNUM.
 * 10/31/2022 22.2.11   Add -Xf opyion and Scr_ExtrVal() () to extract final value.
 * 02/02/2023 22.4.5    Modify Scr_MergeSitusAdr() to limit street name size to SIZ_S_STREETNAME.
 * 03/24/2023 22.6.0    Replace updateTotalTaxRate() with doUpdateTotalRate().
 * 07/31/2023 23.1.0    Add Scr_Load_LDR3(), Scr_UpdateValue(), Scr_MergeStdChar() and Scr_ConvertStdChar().
 *                      Extract part 5 of CHAR file for processing.
 * 08/02/2023 23.1.1    Modify Scr_ConvertStdChar() to add formatted APN_D to STDCHAR record.
 * 08/03/2023 23.1.2    Fix GROSS amount in Scr_ExtrLienX().
 * 08/12/2023 23.1.3    Remove Scr_MergeChar2() and use Scr_MergeStdChar() instead.
 * 10/20/2023 23.3.5    Modify sort command in Scr_ExtrSale2() to keep seller from old file
 *                      if new sale file drops it.
 * 12/15/2023 23.4.6    Modify Scr_MergeSitusAdr() to hardcode S_STRNUM for "10015101" to match county online.
 *                      Modify Scr_Load_Roll2() to fix some S_STRNUM=0 by checking next situs record.
 * 12/18/2023 23.4.6.4  Modify Scr_Load_Roll2() to improve situs issue.
 * 02/02/2024 23.6.0    Modify Scr_MergeMAdr() & Scr_MergeSitusAdr() to populate UnitNox.
 * 07/12/2024 24.0.2    Modify Scr_UpdateValue() to add ExeType.  Add Scr_ExtrLien3().
 * 07/14/2024 24.0.3    Modify Scr_GetParcelStatus() to make sure APN is compare with correct length.
 *                      Modify Scr_Load_LDR3(), Scr_UpdateValue() to ignore parcels with AsmtType=Unsecured.
 *                      Modify Scr_ExtrLien3() to add AsmtType to LienRec.
 *            24.0.3.1  Modify Scr_MergeValue() to return 2 if either RevObjType or AsmtType is unsecured.
 *                      Modify Scr_Load_Roll2() to ignore unsecured records.
 *
 ****************************************************************************/

#include "stdafx.h"
#include "CountyInfo.h"
#include "hlAdo.h"
#include "Prodlib.h"
#include "R01.h"
#include "RecDef.h"
#include "Tables.h"
#include "Logs.h"
#include "LoadDef.h"
#include "Utils.h"
#include "XlatTbls.h"
#include "doSort.h"
#include "doOwner.h"
#include "doZip.h"
#include "formatApn.h"
#include "SaleRec.h"
#include "AttrRec.h"
#include "CharRec.h"
#include "LoadOne.h"
#include "MergeScr.h"
#include "UseCode.h"
#include "LOExtrn.h"
#include "PQ.h"
#include "Tax.h"

IDX_TBL5 SCR_DocTbl[] =
{
   "GRANT DEED",                    "01", 'N', 5,  2,
   "OTHER",                         "19", 'Y', 5,  2,
   "DEED IN LIEU OF FORECLOSURE",   "78", 'N', 20, 2,     // Short sale
   "DEED",                          "13", 'N', 4,  2,
   "QUIT CLAIM",                    "13", 'N', 4,  2,
   "ADMINISTRATOR'S DEED",          "05", 'N', 10, 2,
   "TRUSTEES DEED",                 "27", 'N', 13, 2,
   "DECREE OF DISTRIBUTION",        "79", 'Y', 12, 2,
   "GIFT DEED",                     "16", 'Y', 6,  2,
   "EXECUTOR'S DEED",               "15", 'N', 12, 2,
   "CONTRACT OF SALE OF REAL PROP", "11", 'N', 20, 2,
   "AFFIDAVIT",                     "06", 'Y', 8,  2,
   "GUARDIAN'S DEED",               "14", 'N', 8,  2,
   "ORDER",                         "19", 'Y', 5,  2,
   "CORRECTIVE DEED",               "09", 'Y', 12, 2,
   "TRUST TRANSFER",                "75", 'Y', 10, 2,
   "REPORT OF DEATH",               "19", 'Y', 12, 2,
   "INTERSPOUSAL DEED",             "18", 'N', 15, 2,
   "CERTIFICATE OF SALE",           "25", 'N', 17, 2,     // SHERIFF'S DEED
   "LETTERS OF ADMIN",              "19", 'Y', 7,  2,
   "JUDGMENT",                      "19", 'Y', 8,  2,
   "", "", 0, 0, 0
};

static   LU_ENTRY tblAttr[MAX_ATTR_ENTRIES];
static   FILE  *fdChar, *fdValue, *fdSitus, *fdMail, *fdEtal, *fdLegal, *fdTax, *fdStat, *fdDelq;
static   long  lCharSkip, lSaleSkip;
static   long  lCharMatch, lSaleMatch;

static   long  lSitusMatch, lMailMatch, lTaxMatch, lValueMatch, lEtalMatch, lStatMatch;
static   long  lSitusSkip, lMailSkip, lTaxSkip, lValueSkip, lEtalSkip, lStatSkip;

void Scr_MergeOwner(char *pOutbuf, char *pNames);

/******************************* Scr_MergeMailAdr *****************************
 *
 * Update mail addr
 *
 *****************************************************************************/

int Scr_MergeMailAdr(char *pOutbuf)
{
   static   char acRec[512], *pRec=NULL;
   char     *pTmp, *apItems[MAX_FLD_TOKEN], acTmp[64];
   int      iLoop, iTmp;

   // Get first rec for first call
   if (!pRec)
   {
      // Skip header record
      pRec = fgets(acRec, 512, fdMail);
      if (acRec[0] > '9')
         pRec = fgets(acRec, 512, fdMail);
   }

   do
   {
      if (!pRec)
      {
         fclose(fdMail);
         fdMail = NULL;
         return 1;      // EOF
      }

      // Compare APN - Since APN length is varied, we have to pad it with space
      memcpy(acTmp, pRec, iApnLen);
      replChar(acTmp, cDelim, ' ', iApnLen);
      iLoop = memcmp(pOutbuf, acTmp, iApnLen);
      if (iLoop > 0)
      {
         if (bDebug)
            LogMsg0("Skip mail rec  %.*s", iApnLen, pRec);
         pRec = fgets(acRec, 512, fdMail);
         lMailSkip++;
      }
   } while (iLoop > 0);

   // If not match, return
   if (iLoop)
      return 1;

   // Parse input record
   iTmp = SCR_M_2ND_ADDRRANGE+1;
   if (cDelim == ',')
      iLoop = ParseStringNQ(pRec, cDelim, iTmp, apItems);
   else
      iLoop = ParseStringIQ(pRec, cDelim, iTmp, apItems);
   if (iLoop < iTmp)
   {
      LogMsg("***** Error: bad mail record for APN=%.*s (#tokens=%d)", iApnLen, pOutbuf, iLoop);
      return -1;
   }

   // Check for blank address
   if (memcmp(apItems[SCR_M_DELIVERYADDR], "     ", 5))
   {
      // Clear old Mailing
      removeMailing(pOutbuf, true);

      if (*apItems[SCR_M_ATTN] > '0')
      {
         // Check for known bad char
         if (pTmp=strchr(apItems[SCR_M_ATTN], 0xD5))
            *pTmp = 'C';
         if (pTmp=strchr(apItems[SCR_M_ATTN], 0xC9))
            *pTmp = 'E';

         updateCareOf(pOutbuf, apItems[SCR_M_ATTN], strlen(apItems[SCR_M_ATTN]));
      }

      if (isdigit(*apItems[SCR_M_STRNUM]) && *apItems[SCR_M_STRNAME] > '0')
      {
         // Break House Alpha from StrNum
         if (pTmp = strstr(apItems[SCR_M_STRNUM], " - "))
         {
            *pTmp = 0;
            pTmp += 3;
            iTmp = strlen(pTmp);
            if (iTmp > SIZ_M_STR_SUB) iTmp = SIZ_M_STR_SUB;
            memcpy(pOutbuf+OFF_M_STR_SUB, pTmp, iTmp);
         } else if ((pTmp = strstr(apItems[SCR_M_STRNUM], " -")) || 
                    (pTmp = strstr(apItems[SCR_M_STRNUM], "- ")) ||
                    (pTmp = strstr(apItems[SCR_M_STRNUM], "  ")) )
         {
            *pTmp = 0;
            pTmp += 2;
            iTmp = strlen(pTmp);
            if (iTmp > SIZ_M_STR_SUB) iTmp = SIZ_M_STR_SUB;
            memcpy(pOutbuf+OFF_M_STR_SUB, pTmp, iTmp);
         } else if ((pTmp = strchr(apItems[SCR_M_STRNUM], ' ')) ||
             ((pTmp = strchr(apItems[SCR_M_STRNUM], '-')) && *(pTmp+1) >= 'A' ) )
         {
            *pTmp++ = 0;
            quoteRem(pTmp);
            iTmp = strlen(pTmp);
            if (iTmp <= SIZ_M_STR_SUB) 
               memcpy(pOutbuf+OFF_M_STR_SUB, pTmp, iTmp);
         } else if (pTmp = strchr(apItems[SCR_M_STRNUM], '/')) 
         {
            if (*(pTmp-2) == '-')
               *(pTmp-2) = 0;
            pTmp--;

            iTmp = strlen(pTmp);
            if (iTmp > SIZ_M_STR_SUB) 
               iTmp = SIZ_M_STR_SUB;
            memcpy(pOutbuf+OFF_M_STR_SUB, pTmp, iTmp);
            if (iTmp > 0)
               *(pTmp-1) = 0;
         }
         vmemcpy(pOutbuf+OFF_M_STRNUM, apItems[SCR_M_STRNUM], SIZ_M_STRNUM);
      }

      if (*apItems[SCR_M_STRNAME] > '0')
      {
         memcpy(pOutbuf+OFF_M_STREET, apItems[SCR_M_STRNAME], strlen(apItems[SCR_M_STRNAME]));
         if (*apItems[SCR_M_PRE_DIR] > '0')
            memcpy(pOutbuf+OFF_M_DIR, apItems[SCR_M_PRE_DIR], strlen(apItems[SCR_M_PRE_DIR]));
         if (*apItems[SCR_M_STRTYPE] > '0')
            memcpy(pOutbuf+OFF_M_SUFF, apItems[SCR_M_STRTYPE], strlen(apItems[SCR_M_STRTYPE]));
         if (*apItems[SCR_M_POSTDIR] > '0')
            memcpy(pOutbuf+OFF_M_POSTDIR, apItems[SCR_M_POSTDIR], strlen(apItems[SCR_M_POSTDIR]));
         if (*apItems[SCR_M_2ND_ADDRRANGE] > '0')
         {
            myTrim(apItems[SCR_M_2ND_ADDRRANGE]);
            vmemcpy(pOutbuf+OFF_M_UNITNO, apItems[SCR_M_2ND_ADDRRANGE], SIZ_M_UNITNO);
            vmemcpy(pOutbuf+OFF_M_UNITNOX, apItems[SCR_M_2ND_ADDRRANGE], SIZ_M_UNITNOX);
         } else if (*apItems[SCR_M_2ND_ADDRUNIT] > '0')
         {
            vmemcpy(pOutbuf+OFF_M_UNITNO, apItems[SCR_M_2ND_ADDRUNIT], SIZ_M_UNITNO);
            vmemcpy(pOutbuf+OFF_M_UNITNOX, apItems[SCR_M_2ND_ADDRUNIT], SIZ_M_UNITNOX);
         }
      } else if (*apItems[SCR_M_DELIVERYADDR] > '0')
         memcpy(pOutbuf+OFF_M_STREET, apItems[SCR_M_DELIVERYADDR], strlen(apItems[SCR_M_DELIVERYADDR]));


      if (*apItems[SCR_M_CITY] > '0')
      {
         if (pTmp=strchr(apItems[SCR_M_CITY], 0xBB))
            *pTmp = 'E';
         memcpy(pOutbuf+OFF_M_CITY, apItems[SCR_M_CITY], strlen(apItems[SCR_M_CITY]));
      }

      if (*apItems[SCR_M_STATE] > '0')
         memcpy(pOutbuf+OFF_M_ST, apItems[SCR_M_STATE], strlen(apItems[SCR_M_STATE]));

      if (*apItems[SCR_M_POSTALCD] > '0')
         memcpy(pOutbuf+OFF_M_ZIP, apItems[SCR_M_POSTALCD], strlen(apItems[SCR_M_POSTALCD]));

      memcpy(pOutbuf+OFF_M_ADDR_D, apItems[SCR_M_DELIVERYADDR], strlen(apItems[SCR_M_DELIVERYADDR]));

      if (pTmp=strchr(apItems[SCR_M_LASTLINE], 0xBB))
         *pTmp = 'E';
      iTmp = blankRem(apItems[SCR_M_LASTLINE]);
      memcpy(pOutbuf+OFF_M_CTY_ST_D, apItems[SCR_M_LASTLINE], iTmp);
   }

   lMailMatch++;

   // Get next Mail rec
   pRec = fgets(acRec, 512, fdMail);

   return 0;
}

/******************************* Scr_MergeSitusAdr *****************************
 *
 * Update situs addr
 *
 *****************************************************************************/

int Scr_MergeSitusAdr(char *pOutbuf)
{
   static   char acRec[512], *pRec=NULL;
   int      iLoop, iTmp, iZip;
   char     acTmp[256], *apItems[MAX_FLD_TOKEN], *pTmp;

   // Get first rec for first call
   if (!pRec)
   {
      // Skip header record
      pRec = fgets(acRec, 512, fdSitus);
      if (acRec[0] > '9')
         pRec = fgets(acRec, 512, fdSitus);
   }

   do
   {
      if (!pRec)
      {
         fclose(fdSitus);
         fdSitus = NULL;
         return 1;      // EOF
      }

      // Compare APN - Since APN length is varied, we have to pad it with space
      memcpy(acTmp, pRec, iApnLen);
      replChar(acTmp, cDelim, ' ', iApnLen);
      iLoop = memcmp(pOutbuf, acTmp, iApnLen);
      if (iLoop > 0)
      {
         if (bDebug)
            LogMsg0("Skip situs rec  %.*s", iApnLen, pRec);
         pRec = fgets(acRec, 512, fdSitus);
         lSitusSkip++;
      }
   } while (iLoop > 0);

   // If not match, return
   if (iLoop)
      return 1;

   // Parse input record
   iTmp = SCR_A_CDLDESCR+1;
   if (cDelim == ',')
      iLoop = ParseStringNQ(pRec, cDelim, iTmp, apItems);
   else
      iLoop = ParseStringIQ(pRec, cDelim, iTmp, apItems);
   if (iLoop < iTmp)
   {
      LogMsg("***** Error: bad mail record for APN=%.*s (#tokens=%d)", iApnLen, pOutbuf, iLoop);
      return -1;
   }

   // Clear old situs
   removeSitus(pOutbuf);

   // Special case - 12/16/2023
   if (!memcmp(apItems[SCR_A_PIN], "10015101", 8) && !memcmp(apItems[SCR_A_STRNUM], "100", 3))
      strcpy(apItems[SCR_A_STRNUM], "201");

   // Check for blank address
   if (*apItems[SCR_A_STRNUM] > '0' && *apItems[SCR_A_STRNAME] > ' ')
   {
      memcpy(pOutbuf+OFF_S_STRNUM, apItems[SCR_A_STRNUM], strlen(apItems[SCR_A_STRNUM]));
      memcpy(pOutbuf+OFF_S_HSENO, apItems[SCR_A_STRNUM], strlen(apItems[SCR_A_STRNUM]));

      if ((unsigned char)(*apItems[SCR_A_STRSUB]) > '0')
      {
         if ((unsigned char)(*apItems[SCR_A_STRSUB]) == 0xBD)
            memcpy(pOutbuf+OFF_S_STR_SUB, "1/2", 3);
         else
            vmemcpy(pOutbuf+OFF_S_STR_SUB, apItems[SCR_A_STRSUB], SIZ_S_STR_SUB);
      }
   }

   if (*apItems[SCR_A_PREDIR] > '0')
      vmemcpy(pOutbuf+OFF_S_DIR, apItems[SCR_A_PREDIR], SIZ_S_DIR);

   if (!memcmp(apItems[SCR_A_STRNAME], "PI ", 3))
   {
      if (pTmp = strchr(apItems[SCR_A_STRNAME], '-'))
      {
         vmemcpy(pOutbuf+OFF_S_STREET, pTmp+2, SIZ_S_STREET);
         apItems[SCR_A_STRNAME] = pTmp+2;
      }
   } else if (*apItems[SCR_A_STRNAME] > '0')
      vmemcpy(pOutbuf+OFF_S_STREET, apItems[SCR_A_STRNAME], SIZ_S_STREET);

   // Translate required
   if (*apItems[SCR_A_STRTYPE] > '0')
   {
      int iSfxCode = GetSfxDev(apItems[SCR_A_STRTYPE]);
      if (iSfxCode)
      {
         iTmp = sprintf(acTmp, "%d", GetSfxCode(GetSfxStr(iSfxCode)));
         memcpy(pOutbuf+OFF_S_SUFF, acTmp, iTmp);
      }
   }

   if (*apItems[SCR_A_POSTDIR] > '0')
      memcpy(pOutbuf+OFF_S_POSTDIR, apItems[SCR_A_POSTDIR], strlen(apItems[SCR_A_POSTDIR]));
         
   // Use first matched, may consider range later by looping thru all matched PIN
   if (*apItems[SCR_A_UNITNUM] > ' ')
   {
      remChar(apItems[SCR_A_UNITNUM], ' ');
      vmemcpy(pOutbuf+OFF_S_UNITNO, apItems[SCR_A_UNITNUM], SIZ_S_UNITNO);
      vmemcpy(pOutbuf+OFF_S_UNITNOX, apItems[SCR_A_UNITNUM], SIZ_S_UNITNOX);
   }

   sprintf(acTmp, "%s %.3s %s %s %s %s %s", apItems[SCR_A_STRNUM], pOutbuf+OFF_S_STR_SUB, apItems[SCR_A_PREDIR],
      apItems[SCR_A_STRNAME], apItems[SCR_A_STRTYPE],apItems[SCR_A_POSTDIR], apItems[SCR_A_UNITNUM]);
   iTmp = blankRem(acTmp);

   memcpy(pOutbuf+OFF_S_ADDR_D, acTmp, iTmp);
   iZip = atol(apItems[SCR_A_POSTALCD]);

   // Translate required
   if (*apItems[SCR_A_CITY] > '0')
   {
      // Get city code
      iTmp = City2Code(_strupr(apItems[SCR_A_CITY]), acTmp, apItems[SCR_A_PIN]);
      if (iTmp > 0)
         memcpy(pOutbuf+OFF_S_CITY, acTmp, strlen(acTmp));

      sprintf(acTmp, "%s CA ", apItems[SCR_A_CITY]);
      if (iZip > 90000 && iZip < 99999)
         strcat(acTmp, apItems[SCR_A_POSTALCD]);

      iTmp = blankRem(acTmp);
      memcpy(pOutbuf+OFF_S_CTY_ST_D, acTmp, iTmp);
   }

   memcpy(pOutbuf+OFF_S_ST, "CA", 2);
   if (iZip > 90000 && iZip < 99999)
      vmemcpy(pOutbuf+OFF_S_ZIP, apItems[SCR_A_POSTALCD], SIZ_S_ZIP);

   lSitusMatch++;

   // Get next situs rec
   pRec = fgets(acRec, 512, fdSitus);

   return 0;
}

/******************************** Scr_MergeValue *****************************
 *
 * Update values
 *
 * Available Exemptions:
      CE          Cemetery Exemption
      CH          Church Exemption
      CO          College Exemption
      DV          Disabled Veterans Exemption
      DVPY        Disabled Veterans Prior Year Exemption
      MU          Free Public Library/Museum Exemption
      HO          Homeowners Exemption
      LI          Low Income Disabled Vet Exemption
      LV          Low Value Parcel
      SC          Public School Exemption
      RE          Religious Exemption
      WE          Welfare Exemption
 *
 *****************************************************************************/

int Scr_MergeValue(char *pOutbuf)
{
   static   char acRec[512], *pRec=NULL;
   char     acTmp[64], *apItems[MAX_FLD_TOKEN];
   int      iLoop;
   long     lLand, lImpr, lPP, lLivImpr;
   LONGLONG lTmp;

   // Get first rec for first call
   if (!pRec)
   {
      // Skip header record
      pRec = fgets(acRec, 512, fdValue);
      if (acRec[0] > '9')
         pRec = fgets(acRec, 512, fdValue);
   }

   do
   {
      if (!pRec)
      {
         fclose(fdValue);
         fdValue = NULL;
         return 1;      // EOF
      }

      // Compare APN - Since APN length is varied, we have to pad it with space
      memcpy(acTmp, pRec, iApnLen);
      replChar(acTmp, cDelim, ' ', iApnLen);
      iLoop = memcmp(pOutbuf, acTmp, iApnLen);
      if (iLoop > 0)
      {
         if (bDebug)
            LogMsg0("Skip Stat rec  %.*s", iApnLen, pRec);
         pRec = fgets(acRec, 512, fdValue);
         lValueSkip++;
      }
   } while (iLoop > 0);

   // If not match, return
   if (iLoop)
      return 1;

   // Parse input record
   if (cDelim == ',')
      iLoop = ParseStringNQ(pRec, cDelim, MAX_FLD_TOKEN, apItems);
   else
      iLoop = ParseStringIQ(pRec, cDelim, MAX_FLD_TOKEN, apItems);
   if (iLoop < SCR_V_EXE_AMT+1)
   {
      LogMsg("***** Error: bad value record for APN=%.*s (#tokens=%d)", iApnLen, pOutbuf, iLoop);
      return -1;
   }

   if (*apItems[SCR_V_OBJTYPE] == 'U' || *apItems[SCR_V_TYPE] == 'U')
      return 2;

   // Verify tax year
   if (memcmp(myCounty.acYearAssd, apItems[SCR_V_TAXYEAR], 4))
      LogMsg("*** Prev tax year %.4s <> %s for APN=%s.  Please verify!", myCounty.acYearAssd, apItems[SCR_V_TAXYEAR], apItems[SCR_V_PIN]);

   // Year assessed
   memcpy(pOutbuf+OFF_YR_ASSD, apItems[SCR_V_TAXYEAR], 4);

   // Alt APN - Tax ID
   memcpy(pOutbuf+OFF_ALT_APN, apItems[SCR_V_AIN], strlen(apItems[SCR_V_AIN]));

   // Exemption
   *(pOutbuf+OFF_HO_FL) = '2';      // 'N'
   if (*apItems[SCR_V_EXE_NAME] == 'H')
   {
      *(pOutbuf+OFF_HO_FL) = '1';      // 'Y'
      memcpy(pOutbuf+OFF_EXE_CD1, "HO", 2);
   } else if (*apItems[SCR_V_EXE_NAME] > ' ')
   {
      if (!_memicmp(apItems[SCR_V_EXE_NAME], "CE", 2))
         memcpy(pOutbuf+OFF_EXE_CD1, "CE", 2);
      else if (!_memicmp(apItems[SCR_V_EXE_NAME], "CH", 2))
         memcpy(pOutbuf+OFF_EXE_CD1, "CH", 2);
      else if (!_memicmp(apItems[SCR_V_EXE_NAME], "CO", 2))
         memcpy(pOutbuf+OFF_EXE_CD1, "CO", 2);
      else if (!_memicmp(apItems[SCR_V_EXE_NAME], "DI", 2))
      {
         if (*(apItems[SCR_V_EXE_NAME]+19) == 'P')
            memcpy(pOutbuf+OFF_EXE_CD1, "DVPY", 4);
         else
            memcpy(pOutbuf+OFF_EXE_CD1, "DV", 2);
      } else if (!_memicmp(apItems[SCR_V_EXE_NAME], "FR", 2))
         memcpy(pOutbuf+OFF_EXE_CD1, "MU", 2);
      else if (!_memicmp(apItems[SCR_V_EXE_NAME], "LOW I", 4))
         memcpy(pOutbuf+OFF_EXE_CD1, "LI", 2);
      else if (!_memicmp(apItems[SCR_V_EXE_NAME], "LOW V", 4))
         memcpy(pOutbuf+OFF_EXE_CD1, "LV", 2);
      else if (!_memicmp(apItems[SCR_V_EXE_NAME], "PU", 2))
         memcpy(pOutbuf+OFF_EXE_CD1, "SC", 2);
      else if (!_memicmp(apItems[SCR_V_EXE_NAME], "RE", 2))
         memcpy(pOutbuf+OFF_EXE_CD1, "RE", 2);
      else if (!_memicmp(apItems[SCR_V_EXE_NAME], "WE", 2))
         memcpy(pOutbuf+OFF_EXE_CD1, "WE", 2);
      else
         iLoop = 0;
   }

   // Land
   dollar2Num(apItems[SCR_V_LAND], acTmp);
   lLand = atol(acTmp);
   if (lLand > 0)
   {
      sprintf(acTmp, "%*u", SIZ_LAND, lLand);
      memcpy(pOutbuf+OFF_LAND, acTmp, SIZ_LAND);
   }

   // Improve
   dollar2Num(apItems[SCR_V_IMPR], acTmp);
   lImpr = atol(acTmp);
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*u", SIZ_IMPR, lImpr);
      memcpy(pOutbuf+OFF_IMPR, acTmp, SIZ_IMPR);
   }

   // Living Improve
   dollar2Num(apItems[SCR_V_LIVINGIMPR], acTmp);
   lLivImpr = atol(acTmp);
   if (lLivImpr > 0)
   {
      sprintf(acTmp, "%*u", SIZ_OTH_IMPR, lLivImpr);
      memcpy(pOutbuf+OFF_OTH_IMPR, acTmp, SIZ_OTH_IMPR);
   }

   // Personal property
   dollar2Num(apItems[SCR_V_PERSONAL], acTmp);
   lPP = atol(acTmp);
   if (lPP > 0)
   {
      sprintf(acTmp, "%*u", SIZ_PERSPROP, lPP);
      memcpy(pOutbuf+OFF_PERSPROP, acTmp, SIZ_PERSPROP);
   }

   // Other values
   lTmp = lPP + lLivImpr;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*u", SIZ_OTHER, lTmp);
      memcpy(pOutbuf+OFF_OTHER, acTmp, SIZ_OTHER);
   }

   // Gross total
   lTmp += lLand+lImpr;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*u", SIZ_GROSS, lTmp);
      memcpy(pOutbuf+OFF_GROSS, acTmp, SIZ_GROSS);
   }

   // Set full exemption flag, See Travis email 12/15/2010
   if (lTmp <= 2000)
      *(pOutbuf+OFF_FULL_EXEMPT) = 'Y';

   // Net taxable value
   dollar2Num(apItems[SCR_V_NET_VALUE], acTmp);
   long lNet = atol(acTmp);

   // Exemption
   LONGLONG lTotalExe = lTmp - lNet;
   if (lTotalExe > 0)
   {
      sprintf(acTmp, "%*u", SIZ_EXE_TOTAL, lTotalExe);
      memcpy(pOutbuf+OFF_EXE_TOTAL, acTmp, SIZ_EXE_TOTAL);
   }

   // Ratio
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*u", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
      memcpy(pOutbuf+OFF_RATIO, acTmp, SIZ_RATIO);
   }

   lValueMatch++;

   // Get next Value rec
   pRec = fgets(acRec, 512, fdValue);

   return 0;
}

/******************************** Scr_MergeTax *******************************
 *
 * Keep first match only
 *
 *
 *****************************************************************************

int Scr_MergeTax(char *pOutbuf)
{
   static   char acRec[512], *pRec=NULL;
   char     acTmp[64];
   int      iLoop, iTmp;

   // Get first rec for first call
   if (!pRec)
   {
      // Skip header record
      for (iTmp = 0; iTmp < iHdrRows; iTmp++)
         pRec = fgets(acRec, 512, fdTax);
      pRec = fgets(acRec, 512, fdTax);
   }

   do
   {
      if (!pRec)
      {
         fclose(fdTax);
         fdTax = NULL;
         return 1;      // EOF
      }

      // Compare APN
      iLoop = memcmp(pOutbuf, pRec, iApnLen);
      if (iLoop > 0)
      {
         if (bDebug)
            LogMsg0("Skip Tax rec  %.*s", iApnLen, pRec);
         pRec = fgets(acRec, 512, fdTax);
         lTaxSkip++;
      }
   } while (iLoop > 0);

   // If not match, return
   if (iLoop)
      return 1;

   // Parse input record
   iTmp = SCR_T_BILLTYPE+1;
   if (cDelim == ',')
      iLoop = ParseStringNQ(pRec, cDelim, iTmp, apTokens);
   else
      iLoop = ParseStringIQ(pRec, cDelim, iTmp, apTokens);
   if (iLoop < iTmp)
   {
      LogMsg("***** Error: bad value record for APN=%.*s (#tokens=%d)", iApnLen, pOutbuf, iLoop);
      return -1;
   }

   double   dTax;
   dTax = atof(apTokens[SCR_T_FIRSTINSTAMOUNT]);
   dTax += atof(apTokens[SCR_T_SECONDINSTAMOUNT]);
   if (dTax > 0)
   {
      iTmp = sprintf(acTmp, "%*u", SIZ_TAX_AMT, (long)(dTax*100));
      memcpy(pOutbuf+OFF_TAX_AMT, acTmp, iTmp);
   }

   lTaxMatch++;

   // Get next Value rec
   pRec = fgets(acRec, 512, fdTax);

   return 0;
}
*/

int Scr_MergeTax2(char *pOutbuf)
{
   static   char acRec[1024], *pRec=NULL;
   char     acTmp[64], *apItems[MAX_FLD_TOKEN];
   int      iLoop, iTmp;

   // Get first rec for first call
   if (!pRec)
   {
      // Skip header record
      pRec = fgets(acRec, 1024, fdTax);
      if (acRec[0] > '9')
         pRec = fgets(acRec, 1024, fdTax);
   }

   //char *pTmp = pRec;
   do
   {
      if (!pRec)
      {
         fclose(fdTax);
         fdTax = NULL;
         return 1;      // EOF
      }

      // Compare APN - Since APN length is varied, we have to pad it with space
      memcpy(acTmp, pRec, iApnLen);
      replChar(acTmp, cDelim, ' ', iApnLen);
      iLoop = memcmp(pOutbuf, acTmp, iApnLen);
      if (iLoop > 0)
      {
         if (bDebug)
            LogMsg0("Skip Tax rec  %.*s", iApnLen, pRec);
         pRec = fgets(acRec, 1024, fdTax);
         lTaxSkip++;
      }
   } while (iLoop > 0);

   // If not match, return
   if (iLoop)
      return 1;

   // Parse input record
   iTmp = SCR_T_SITUSFULLADDRESS+1;
   iLoop = ParseStringIQ(pRec, cDelim, iTmp, apItems);
   if (iLoop < iTmp)
   {
      LogMsg("***** Error: bad value record for APN=%.*s (#tokens=%d)", iApnLen, pOutbuf, iLoop);
      return -1;
   }

   double   dTax;
   dTax = atof(apItems[SCR_T_FIRSTINSTALLMENTAMOUNT]);
   dTax += atof(apItems[SCR_T_SECONDINSTALLMENTAMOUNT]);
   if (dTax > 0)
   {
      iTmp = sprintf(acTmp, "%*u", SIZ_TAX_AMT, (long)(dTax*100));
      memcpy(pOutbuf+OFF_TAX_AMT, acTmp, iTmp);
   }

   lTaxMatch++;

   // Get next tax rec
   pRec = fgets(acRec, 1024, fdTax);

   return 0;
}

/******************************** Scr_MergeChar2 *****************************
 *
 * Missing: BLDGQUAL
 *
 *****************************************************************************/

//int Scr_MergeChar2(char *pOutbuf)
//{
//   static   char acRec[1024], *pRec=NULL;
//   char     acTmp[64], acCode[32], *apItems[MAX_FLD_TOKEN];
//   int      iLoop, iTmp, lTmp;
//   double   dTmp;
//
//   // Get first rec for first call
//   if (!pRec)
//   {
//      // Skip header record     
//      pRec = fgets(acRec, 1024, fdChar);
//      if (acRec[0] > '9')
//         pRec = fgets(acRec, 1024, fdChar);
//   }
//
//   do
//   {
//      if (!pRec)
//      {
//         fclose(fdChar);
//         fdChar = NULL;
//         return 1;      // EOF
//      }
//
//      // Compare APN - Since APN length is varied, we have to pad it with space
//      memcpy(acTmp, pRec, iApnLen);
//      replChar(acTmp, cDelim, ' ', iApnLen);
//      iLoop = memcmp(pOutbuf, acTmp, iApnLen);
//      if (iLoop > 0)
//      {
//         if (bDebug)
//            LogMsg0("Skip CHAR rec  %.*s", iApnLen, pRec);
//         pRec = fgets(acRec, 1024, fdChar);
//         lCharSkip++;
//      }
//   } while (iLoop > 0);
//
//   // If not match, return
//   if (iLoop)
//      return 1;
//
//   // Parse input record
//   iTmp = SCR_C_GARSQFT+1;
//   if (cDelim == ',')
//      iLoop = ParseStringNQ(pRec, cDelim, iTmp, apItems);
//   else
//      iLoop = ParseStringIQ(pRec, cDelim, iTmp, apItems);
//   if (iLoop < iTmp)
//   {
//      LogMsg("***** Error: bad CHAR record for APN=%.*s (#tokens=%d)", iApnLen, pOutbuf, iLoop);
//      return -1;
//   }
//
//   // YrBlt, YrEff  OFF_YR_BLT
//   lTmp = atol(apItems[SCR_C_YRBLT]);
//   if (lTmp > 0)
//      memcpy(pOutbuf+OFF_YR_BLT, apItems[SCR_C_YRBLT], strlen(apItems[SCR_C_YRBLT]));
//   lTmp = atol(apItems[SCR_C_EFFYR]);
//   if (lTmp > 0)
//      memcpy(pOutbuf+OFF_YR_EFF, apItems[SCR_C_EFFYR], strlen(apItems[SCR_C_EFFYR]));
//
//   // Units
//   lTmp = atol(apItems[SCR_C_UNITS]);
//   if (lTmp > 0)
//   {
//      sprintf(acTmp, "%*u  ", SIZ_UNITS, lTmp);
//      memcpy(pOutbuf+OFF_UNITS, acTmp, SIZ_UNITS);
//   }
//
//   // View
//   blankRem(apItems[SCR_C_VIEW]);
//   if (!memcmp(apItems[SCR_C_VIEW], "NO", 2))
//      *(pOutbuf+OFF_VIEW) = 'N';
//   else if (!memcmp(apItems[SCR_C_VIEW], "CITY", 4))
//      *(pOutbuf+OFF_VIEW) = 'S';
//   else if (!memcmp(apItems[SCR_C_VIEW], "OCEA", 4))
//      *(pOutbuf+OFF_VIEW) = 'B';
//   else if (!memcmp(apItems[SCR_C_VIEW], "MOUN", 4))
//      *(pOutbuf+OFF_VIEW) = 'T';
//   else if (!memcmp(apItems[SCR_C_VIEW], "VALL", 4))
//      *(pOutbuf+OFF_VIEW) = 'R';
//
//   // Improve SF - OFF_BLDG_SF
//   lTmp = atol(apItems[SCR_C_BLDGSQFT]);
//   if (lTmp > 0)
//   {
//      sprintf(acTmp, "%*u  ", SIZ_BLDG_SF, lTmp);
//      memcpy(pOutbuf+OFF_BLDG_SF, acTmp, SIZ_BLDG_SF);
//   }
//
//   // Rooms
//   lTmp = atol(apItems[SCR_C_ROOMS]);
//   if (lTmp > 0)
//   {
//      sprintf(acTmp, "%*u  ", SIZ_ROOMS, lTmp);
//      memcpy(pOutbuf+OFF_ROOMS, acTmp, SIZ_ROOMS);
//   }
//
//   // Beds
//   lTmp = atol(apItems[SCR_C_BEDROOMS]);
//   if (lTmp > 0)
//   {
//      sprintf(acTmp, "%*u  ", SIZ_BEDS, lTmp);
//      memcpy(pOutbuf+OFF_BEDS, acTmp, SIZ_BEDS);
//   }
//
//   // Baths/Half Baths  OFF_BATH_F
//   dTmp = atof(apItems[SCR_C_FULLBATH]);
//   if (dTmp > 0)
//   {
//      lTmp = (int)dTmp;
//      if (lTmp > 99)
//      {
//         strcpy(acTmp, "99");
//         iTmp = 2;
//      } else
//         iTmp = sprintf(acTmp, "%d", lTmp);
//      memcpy(pOutbuf+OFF_BATH_F, acTmp, iTmp);
//      memcpy(pOutbuf+OFF_BATH_4Q, acTmp, iTmp);
//   }
//   if (*apItems[SCR_C_HALFBATH] > '0')
//   {
//      *(pOutbuf+OFF_BATH_H) = ' ';
//      *(pOutbuf+OFF_BATH_H+1) = *apItems[SCR_C_HALFBATH];
//      *(pOutbuf+OFF_BATH_2Q) = *apItems[SCR_C_HALFBATH];
//   } else if ((dTmp - (double)lTmp) == 0.5)
//   {
//      memcpy(pOutbuf+OFF_BATH_H, " 1", 2);
//      memcpy(pOutbuf+OFF_BATH_2Q, " 1", 2);
//   }
//
//   // Fp, Heat type OFF_FIRE_PL
//   if (*apItems[SCR_C_FIREPLACES] > '0')
//   {
//      *(pOutbuf+OFF_FIRE_PL) = ' ';
//      *(pOutbuf+OFF_FIRE_PL+1) = *apItems[SCR_C_FIREPLACES];
//   }
//
//   // Heat - Possible value C, N, O, W ?
//   // BASEBOARD, CENTRAL, CENTRAL A/C, FLOOR, FORCED, OTHER, RADIENT, WALL
//   blankRem(apItems[SCR_C_HVAC]);
//   blankRem(apItems[SCR_C_HEAT]);
//   if (*apItems[SCR_C_HEAT] > ' ')
//   {
//      pRec = findXlatCodeA(apItems[SCR_C_HEAT], &asHeating[0]);
//      if (pRec)
//      {
//         *(pOutbuf+OFF_HEAT) = *pRec;
//         if (strstr(apItems[SCR_C_HEAT], "A/C"))
//         {
//            if (*apItems[SCR_C_HEAT] == 'W')
//               *(pOutbuf+OFF_AIR_COND) = 'L';
//            else
//               *(pOutbuf+OFF_AIR_COND) = 'C';
//         }
//      } 
//   } else if (*apItems[SCR_C_HVAC] > ' ')
//   {
//      if (*apItems[SCR_C_HVAC] == 'C')
//         *(pOutbuf+OFF_HEAT) = '3';    
//      else if (*apItems[SCR_C_HVAC] == 'S')
//         *(pOutbuf+OFF_HEAT) = 'J';    
//      else if (*apItems[SCR_C_HVAC] == 'W')
//         *(pOutbuf+OFF_HEAT) = 'X';    
//   }
//
//   // HVAC -  COMPLETE HVAC,  ELECTRIC, ELECTRIC WALL, FLOOR FURNACE, FORCED AIR UNIT, NONE
//   //         PACKAGE UNIT, SPACE HEATERS, SPACE HEATERS-RADIANT, VENTILATION, WALL FURNACE, WARMED AND COOLED AIR,
//   if (*apItems[SCR_C_HVAC] > ' ')
//   {
//      pRec = findXlatCodeA(apItems[SCR_C_HVAC], &asCooling[0]);
//      if (pRec)
//         *(pOutbuf+OFF_AIR_COND) = *pRec;
//   }
//  
//   // Zoning
//   iTmp = strlen(apItems[SCR_C_ZONING]);
//   if (iTmp > 0)
//   {
//      vmemcpy(pOutbuf+OFF_ZONE, apItems[SCR_C_ZONING], SIZ_ZONE, iTmp);
//      if (*(pOutbuf+OFF_ZONE_X1) == ' ')
//         vmemcpy(pOutbuf+OFF_ZONE_X1, apItems[SCR_C_ZONING], SIZ_ZONE_X1, iTmp);
//   }
//
//   // Pool/Spa
//   if (*apItems[SCR_C_POOL] > '0' && *apItems[SCR_C_SPA] > '0')
//      *(pOutbuf+OFF_POOL) = 'C';
//   else if (*apItems[SCR_C_POOL] > '0')
//      *(pOutbuf+OFF_POOL) = 'P';
//   else if (*apItems[SCR_C_SPA] > '0')
//      *(pOutbuf+OFF_POOL) = 'S';
//
//   // Garage size, Park type
//   int iGarSiz= atol(apItems[SCR_C_GARSQFT]);
//   int iCarSiz= atol(apItems[SCR_C_CARSQFT]);
//   if (iGarSiz > 0)
//   {
//      sprintf(acTmp, "%*u ", SIZ_GAR_SQFT, iGarSiz);
//      memcpy(pOutbuf+OFF_GAR_SQFT, acTmp, SIZ_GAR_SQFT);
//
//      if (iCarSiz > 0)
//         *(pOutbuf+OFF_PARK_TYPE) = '2';     // GARAGE/CARPORT
//      else
//         *(pOutbuf+OFF_PARK_TYPE) = 'Z';     // GARAGE
//   } else if (iCarSiz > 0)
//      *(pOutbuf+OFF_PARK_TYPE) = 'C';        // CARPORT
//
//
//   // LotSize, LotAcre
//   long lLotSqft, lLotAcres;
//
//   dTmp = atof(apItems[SCR_C_LOTACRE])*1000;
//   if (dTmp > 0)
//   {
//      lLotAcres = (long)dTmp;
//      sprintf(acTmp, "%*u  ", SIZ_LOT_ACRES, lLotAcres);
//      memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);
//   } else
//      lLotAcres = 0;
//
//   dTmp = atof(apItems[SCR_C_LOTSQFT]);           // round up
//   if (dTmp > 0)
//   {
//      sprintf(acTmp, "%*u  ", SIZ_LOT_SQFT, (long)(dTmp+0.5));
//      memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);
//   } else if (lLotAcres > 0)
//   {
//      lLotSqft = (long)(lLotAcres * SQFT_FACTOR_1000);
//      sprintf(acTmp, "%*u  ", SIZ_LOT_SQFT, lLotSqft);
//      memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);
//   }
//
//#ifdef _DEBUG
//   //if (!memcmp(pOutbuf, "0346510468", 10))
//   //   iTmp = 0;
//#endif
//
//   // Roof Material - COMP, METAL, OTHER, COMP SHINGLE, CONC SHINGLE, SHAKE, TILE
//   if (apItems[SCR_C_ROOF][1] > ' ')
//   {
//      if (!_stricmp(apItems[SCR_C_ROOF], " COMP"))
//         *(pOutbuf+OFF_ROOF_MAT) = 'X';    
//      else if (!_memicmp(apItems[SCR_C_ROOF], " COMP", 4))
//         *(pOutbuf+OFF_ROOF_MAT) = 'C';    
//      else if (!_memicmp(apItems[SCR_C_ROOF], " CONC", 4))
//         *(pOutbuf+OFF_ROOF_MAT) = 'J';    
//      else if (apItems[SCR_C_ROOF][1] == 'T')
//         *(pOutbuf+OFF_ROOF_MAT)  = 'I';
//      else if (apItems[SCR_C_ROOF][1] == 'S')
//         *(pOutbuf+OFF_ROOF_MAT)  = 'B';
//      else if (apItems[SCR_C_ROOF][1] == 'M')
//         *(pOutbuf+OFF_ROOF_MAT)  = 'L';
//      else if (apItems[SCR_C_ROOF][1] == 'O')
//         *(pOutbuf+OFF_ROOF_MAT)  = 'Z';
//   }
//
//   // Const Type OFF_CONS_TYPE
//   //*(pOutbuf+OFF_CONS_TYPE) = pRec->Const[0];
//
//   // Bldg Class/Bldg Quality
//   if (*apItems[SCR_C_BLDGCLS] > ' ')
//      *(pOutbuf+OFF_BLDG_CLASS) = *apItems[SCR_C_BLDGCLS];
//   if (*apItems[SCR_C_BLDGQUAL] > ' ')
//   {
//      iTmp = Value2Code(apItems[SCR_C_BLDGQUAL], acCode, &tblAttr[0]);
//      if (iTmp > 0)
//      {
//         blankPad(acCode, SIZ_BLDG_QUAL);
//         memcpy(pOutbuf+OFF_BLDG_QUAL, acCode, SIZ_BLDG_QUAL);
//      }
//   }
//
//   // Condition
//   blankRem(apItems[SCR_C_CONDITION]);
//   if (*apItems[SCR_C_CONDITION] > ' ')
//   {
//      switch (*apItems[SCR_C_CONDITION])
//      {
//         case 'P':
//            *(pOutbuf+OFF_IMPR_COND) = *apItems[SCR_C_CONDITION];    // Poor
//            break;
//         case 'F':
//            *(pOutbuf+OFF_IMPR_COND) = *apItems[SCR_C_CONDITION];    // Fair
//            break;
//         case 'A':
//            *(pOutbuf+OFF_IMPR_COND) = *apItems[SCR_C_CONDITION];    // Average
//            break;
//         case 'G':
//            *(pOutbuf+OFF_IMPR_COND) = *apItems[SCR_C_CONDITION];    // Good
//            break;
//         case 'E':
//            *(pOutbuf+OFF_IMPR_COND) = *apItems[SCR_C_CONDITION];    // Excellent
//            break;
//         default:
//            *(pOutbuf+OFF_IMPR_COND) = ' ';
//      }
//   }
//
//   // Stories
//   dTmp = atof(apItems[SCR_C_STORIES]);
//   if (dTmp > 0)
//   {
//      sprintf(acTmp, "%.1f  ", dTmp);
//      memcpy(pOutbuf+OFF_STORIES, acTmp, SIZ_STORIES);
//   }
//
//   // Topography
//   blankRem(apItems[SCR_C_TOPOGRAPHY]);
//   if (*apItems[SCR_C_TOPOGRAPHY] > ' ')
//   {
//      if (!_memicmp(apItems[SCR_C_TOPOGRAPHY], "SL", 2))
//         *(pOutbuf+OFF_TOPO) = 'S';    // Slope
//      else if (*apItems[SCR_C_TOPOGRAPHY] == 'L')
//         *(pOutbuf+OFF_TOPO) = 'L';    // Level
//      else if (*apItems[SCR_C_TOPOGRAPHY] == 'S')
//         *(pOutbuf+OFF_TOPO) = 'T';    // Steep
//      else if (*apItems[SCR_C_TOPOGRAPHY] == 'V')
//         *(pOutbuf+OFF_TOPO) = 'V';    // Varies
//      else
//         *(pOutbuf+OFF_TOPO) = ' ';
//   }
//
//   lCharMatch++;
//
//   // Get next Value rec
//   pRec = fgets(acRec, 1024, fdChar);
//
//   return 0;
//}

/******************************* Scr_MergeChar *******************************
 *
 * Merge Org_Char.dat in STDCHAR format
 *
 *****************************************************************************/

int Scr_MergeStdChar(char *pOutbuf)
{
   static   char acRec[2048], *pRec=NULL;
   char     acTmp[256];
   ULONG    lLotSqft, lBldgSqft, lGarSqft, lCarSqft, lTmp;
   int      iLoop, iBeds, iFBath, iHBath;
   STDCHAR *pChar;

   // Get first Char rec for first call
   if (!pRec)
      pRec = fgets(acRec, 1024, fdChar);

   pChar = (STDCHAR *)pRec;

   do
   {
      if (!pRec)
      {
         fclose(fdChar);
         fdChar = NULL;
         return 1;      // EOF
      }

      // Compare APN
      iLoop = memcmp(pOutbuf, pRec, iApnLen);
      if (iLoop > 0)
      {
         if (bDebug)
            LogMsg0("Skip Char rec  %.*s", iApnLen, pRec);
         pRec = fgets(acRec, 1024, fdChar);
         lCharSkip++;
      }
   } while (iLoop > 0);

   // If not match, return
   if (iLoop)
      return 1;

   // Yrblt
   lTmp = atoin(pChar->YrBlt, SIZ_CHAR_YRBLT);
   if (lTmp > 1600 && lTmp <= lToyear)
      memcpy(pOutbuf+OFF_YR_BLT, pChar->YrBlt, SIZ_YR_BLT);

   // EffYr
   lTmp = atoin(pChar->YrEff, SIZ_CHAR_YRBLT);
   if (lTmp > 1600 && lTmp <= lToyear)
      memcpy(pOutbuf+OFF_YR_EFF, pChar->YrEff, SIZ_YR_BLT);

   // Number of Units
   lTmp = atoin(pChar->Units, SIZ_CHAR_UNITS);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*u", SIZ_UNITS, lTmp);
      memcpy(pOutbuf+OFF_UNITS, acTmp, SIZ_UNITS);
   }

   // BldgSqft
   lBldgSqft = atoin(pChar->BldgSqft, SIZ_CHAR_SQFT);
   if (lBldgSqft > 10)
   {
      sprintf(acTmp, "%*u", SIZ_BLDG_SF, lBldgSqft);
      memcpy(pOutbuf+OFF_BLDG_SF, acTmp, SIZ_BLDG_SF);
   }

   // Garage Sqft
   lGarSqft = atoin(pChar->GarSqft, SIZ_CHAR_SQFT);
   lCarSqft = atoin(pChar->CarpSqft, SIZ_CHAR_SQFT);
   if (lGarSqft > 10)
   {
      sprintf(acTmp, "%*u", SIZ_GAR_SQFT, lGarSqft);
      memcpy(pOutbuf+OFF_GAR_SQFT, acTmp, SIZ_GAR_SQFT);
      *(pOutbuf+OFF_PARK_TYPE) = 'Z';  
   } else if (lCarSqft > 10)
   {
      sprintf(acTmp, "%*u", SIZ_GAR_SQFT, lCarSqft);
      memcpy(pOutbuf+OFF_GAR_SQFT, acTmp, SIZ_GAR_SQFT);
      *(pOutbuf+OFF_PARK_TYPE) = 'C';  
   }

   // Lot Sqft
   lLotSqft = atoin(pChar->LotSqft, SIZ_CHAR_SQFT);
   if (lLotSqft > 10)
   {
      if (lLotSqft < 999999999)
      {
         sprintf(acTmp, "%*u", SIZ_LOT_SQFT, lLotSqft);
         memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);
      }
      lTmp = atol(pChar->LotAcre);
      sprintf(acTmp, "%*u", SIZ_LOT_ACRES, lTmp);
      memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);
   }

   // Pools
   *(pOutbuf+OFF_POOL) = pChar->Pool[0];

   // Beds
   iBeds = atoin(pChar->Beds, SIZ_CHAR_BEDS);
   if (iBeds > 0)
   {
      sprintf(acTmp, "%*u", SIZ_BEDS, iBeds);
      memcpy(pOutbuf+OFF_BEDS, acTmp, SIZ_BEDS);
   }

   // Bath
   iFBath = atoin(pChar->FBaths, SIZ_CHAR_BATHS);
   if (iFBath > 0)
   {
      sprintf(acTmp, "%*u", SIZ_BATH_F, iFBath);
      memcpy(pOutbuf+OFF_BATH_F, acTmp, SIZ_BATH_F);
   }

   // Half bath
   iHBath = atoin(pChar->HBaths, SIZ_CHAR_BATHS);
   if (iHBath > 0)
   {
      sprintf(acTmp, "%*u", SIZ_BATH_H, iHBath);
      memcpy(pOutbuf+OFF_BATH_H, acTmp, SIZ_BATH_H);
   }

   // Total Rooms
   memcpy(pOutbuf+OFF_ROOMS, pChar->Rooms, SIZ_ROOMS);

   // FirePlace
   memcpy(pOutbuf+OFF_FIRE_PL, pChar->Fireplace, SIZ_FIRE_PL);

   // Stories
   memcpy(pOutbuf+OFF_STORIES, pChar->Stories, SIZ_STORIES);

   // BldgCls/Condition
   *(pOutbuf+OFF_BLDG_CLASS) = pChar->BldgClass;
   *(pOutbuf+OFF_BLDG_QUAL) = pChar->BldgQual;

   // Roof
   *(pOutbuf+OFF_ROOF_MAT) = pChar->RoofMat[0];

   // Condition
   *(pOutbuf+OFF_IMPR_COND) = pChar->ImprCond[0];

   // Heat/Air
   *(pOutbuf+OFF_HEAT) = pChar->Heating[0];
   *(pOutbuf+OFF_AIR_COND) = pChar->Cooling[0];

   // View
   *(pOutbuf+OFF_VIEW) = pChar->View[0];

   // Slope
   *(pOutbuf+OFF_TOPO) = pChar->Topo[0];

   // Water
   *(pOutbuf+OFF_WATER) = pChar->Water;

   // Sewer
   *(pOutbuf+OFF_SEWER) = pChar->Sewer;

   // Zoning
   memcpy(pOutbuf+OFF_ZONE, pChar->Zoning, SIZ_ZONE);

   // Deck
   memcpy(pOutbuf+OFF_DECK_SF, pChar->DeckSqft, SIZ_DECK_SF);

   lCharMatch++;

   // Get next Char rec
   pRec = fgets(acRec, 1024, fdChar);

   return 0;
}

/****************************** Scr_GetParcelStatus **************************
 *
 * Return 0 if found, 1 otherwise
 *
 *****************************************************************************/
#define  SCR_GETSTATUS \
      pRec = fgets(acRec, 512, fdStat);\
      if (pRec) \
      {\
         iTmp = ParseStringNQ(pRec, cDelim, MAX_FLD_TOKEN, apItems);\
         if (iTmp < SCR_P_PUBLIC)\
         {\
            LogMsg("***** Error: GetParcelStatus() -> bad input record for APN=%s", apItems[SCR_L_PIN]);\
            return -1;\
         }\
      }

int Scr_GetParcelStatus(char *pApn, char *pStatus, char *pPublic, int iLen)
{
   static   char acRec[512], *apItems[MAX_FLD_TOKEN], *pRec=NULL;
   int      iLoop, iTmp;

   // Get first rec for first call
   if (!pRec)
   {
      // Skip header record
      for (iTmp = 0; iTmp < iHdrRows; iTmp++)
         pRec = fgets(acRec, 512, fdStat);
      SCR_GETSTATUS
   }

   *pStatus = ' ';
   *pPublic = ' ';
   do
   {
      if (!pRec)
      {
         fclose(fdStat);
         fdStat = NULL;
         return 1;      // EOF
      }

      // Compare APN
      iLoop = memcmp(pApn, apItems[SCR_P_PIN], iLen);
      if (iLoop > 0)
      {
         if (bDebug)
            LogMsg0("Skip Status rec  %s", pRec);
         SCR_GETSTATUS
         lStatSkip++;
      }
   } while (iLoop > 0);

   // If not match, return
   if (iLoop)
      return 1;
   
   *pStatus = *apItems[SCR_P_STATUS];
   if (iTmp > SCR_P_PUBLIC && *apItems[SCR_P_PUBLIC] == '1')
      *pPublic = 'Y';

   lStatMatch++;

   // Get next STAT rec
   SCR_GETSTATUS
   return 0;
}

/******************************** Scr_MergeLegalN ****************************
 *
 * Return 0 if successful
 *
 *****************************************************************************/

int Scr_MergeLegalN(char *pOutbuf, char *pRollRec, int iFlag)
{
   char     acTmp[256], acTmp1[256], *pTmp, *apItems[MAX_FLD_TOKEN];
   long     lTmp;
   int      iRet, iTmp;

   // Parse record
   iRet = ParseStringNQ(pRollRec, cDelim, MAX_FLD_TOKEN, apItems);
   if (iRet <= SCR_LE_PRIOWNER)
   {
      LogMsg("***** Error: bad input record for APN=%s", apItems[SCR_LE_PIN]);
      return -1;
   }

   // Ignore invalid APN
   if (*apItems[SCR_LE_PIN] > '9' || !memcmp(apItems[SCR_LE_PIN], "000000", 6))
      return -2;

   // Ignore unsecured parcels
   if (*apItems[SCR_LE_TYPE] == 'U' )
      return 1;

#ifdef _DEBUG
   //if (!memcmp(apItems[SCR_LE_PIN], "24805005", 8))
   //   iTmp = 0;
#endif

   // Check status.  If found and not 'A', ignore it
   iTmp = strlen(apItems[SCR_LE_PIN]);
   iRet = Scr_GetParcelStatus(apItems[SCR_LE_PIN], acTmp, acTmp1, iTmp);
   if (!iRet && acTmp[0] != 'A')
      return 2;

   if (iFlag & CREATE_R01)
   {
      // Clear output buffer
      memset(pOutbuf, ' ', iRecLen);
      if (!iRet)
      {
         *(pOutbuf+OFF_STATUS) = acTmp[0];
         *(pOutbuf+OFF_PUBL_FLG) = acTmp1[0];
      }

      // Start copying data
      memcpy(pOutbuf, apItems[SCR_LE_PIN], strlen(apItems[SCR_LE_PIN]));

      // Format APN
      iRet = formatApn(apItems[SCR_LE_PIN], acTmp, &myCounty);
      memcpy(pOutbuf+OFF_APN_D, acTmp, iRet);

      // Create MapLink and output new record
      iRet = formatMapLink(apItems[SCR_LE_PIN], acTmp, &myCounty);
      memcpy(pOutbuf+OFF_MAPLINK, acTmp, iRet);

      // Create index map link
      if (getIndexPage(acTmp, acTmp1, &myCounty))
         memcpy(pOutbuf+OFF_IMAPLINK, acTmp1, iRet);

      // County code
      memcpy(pOutbuf+OFF_CO_NUM, "44SCR", 5);

      // Year assessed
      memcpy(pOutbuf+OFF_YR_ASSD, myCounty.acYearAssd, 4);
   }

   // TRA
   lTmp = atol(apItems[SCR_LE_TRA]);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%.6d", lTmp);
      memcpy(pOutbuf+OFF_TRA, acTmp, 6);
   }

   // Legal - ignore since it only contains supervisor district
   //updateLegal(pOutbuf, apItems[SCR_LE_LEGAL]);

   // Standard UseCode
   if (*apItems[SCR_LE_USECODE] > ' ')
   {
      strcpy(acTmp, apItems[SCR_LE_USECODE]);
      if (pTmp = strchr(acTmp, '-'))
         *pTmp = 0;
      else
         acTmp[3] = 0;
      iTmp = strlen(acTmp);
      if (iTmp > SIZ_USE_CO) iTmp = SIZ_USE_CO;
      memcpy(pOutbuf+OFF_USE_CO, acTmp, iTmp);
      if (iNumUseCodes > 0)
      {
         pTmp = Cnty2StdUse(acTmp);
         if (pTmp)
            memcpy(pOutbuf+OFF_USE_STD, pTmp, 3);
         else
         {
            memcpy(pOutbuf+OFF_USE_STD, USE_MISC, 3);
            logUsecode(apItems[SCR_LE_USECODE], pOutbuf);
         }
      }
   }

   // Owner
   if (apItems[SCR_LE_PRIOWNER])
   {
      if (pTmp=strchr(apItems[SCR_LE_PRIOWNER], 0xD5))
         *pTmp = 'C';
      Scr_MergeOwner(pOutbuf, apItems[SCR_LE_PRIOWNER]);
   } else
      LogMsg("*** Missing owner name: %.14s", pOutbuf);

   return 0;
}

/********************************* Scr_MergeAttr *****************************
 *
 * Return 0 if successful, >0 if error
 *
 *****************************************************************************/

int Scr_MergeAttr(char *pOutbuf, char *pAttrRec)
{
   ATTR_REC *pRec;
   char     acTmp[256], acCode[8];
   long     lTmp, lLotAcres, lLotSqft;
   double   dTmp;
   int      iRet, iTmp, iTmp1;

   pRec = (ATTR_REC *)pAttrRec;
   pRec->filler1[0] = ' ';
   pRec->CRLF[0] = 0;

   // Bldg Class/Bldg Quality
   *(pOutbuf+OFF_BLDG_CLASS) = pRec->BldgClass[0];
   memcpy(acTmp, pRec->BldgQual, SIZ_ATTR_BLDQ);
   acTmp[SIZ_ATTR_BLDQ] = 0;
   iRet = Value2Code(acTmp, acCode, &tblAttr[0]);
   if (iRet > 0)
   {
      blankPad(acCode, SIZ_BLDG_QUAL);
      memcpy(pOutbuf+OFF_BLDG_QUAL, acCode, SIZ_BLDG_QUAL);
   }

   // Improve SF - OFF_BLDG_SF
   memcpy(acTmp, pRec->Imps, SIZ_ATTR_IMPS);
   acTmp[SIZ_ATTR_IMPS] = 0;
   lTmp = atol(acTmp);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*u  ", SIZ_BLDG_SF, lTmp);
      memcpy(pOutbuf+OFF_BLDG_SF, acTmp, SIZ_BLDG_SF);
   }

   // LotSize, LotAcre
   lLotAcres=lLotSqft = 0;
   memcpy(acTmp, pRec->LotAcre, SIZ_ATTR_LOTACRE);
   acTmp[SIZ_ATTR_LOTACRE] = 0;
   dTmp = atof(acTmp)*1000;
   if (dTmp > 0)
   {
      lLotAcres = (long)dTmp;
      sprintf(acTmp, "%*u  ", SIZ_LOT_ACRES, lLotAcres);
      memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);
   }

   memcpy(acTmp, pRec->LotSize, SIZ_ATTR_LOTSIZE);
   acTmp[SIZ_ATTR_LOTSIZE] = 0;
   dTmp = atof(acTmp);           // round up
   if (dTmp > 0)
   {
      sprintf(acTmp, "%*u  ", SIZ_LOT_SQFT, (long)(dTmp+0.5));
      memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);
   } else if (lLotAcres > 0)
   {
      lLotSqft = (long)(lLotAcres * SQFT_FACTOR_1000);
      sprintf(acTmp, "%*u  ", SIZ_LOT_SQFT, lLotSqft);
      memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);
   }

   // Rooms
   iTmp = atoin(pRec->Rooms, SIZ_ATTR_ROOMS);
   if (iTmp > 0)
   {
      sprintf(acTmp, "%*u  ", SIZ_ROOMS, iTmp);
      memcpy(pOutbuf+OFF_ROOMS, acTmp, SIZ_ROOMS);
   }

   // Beds
   iTmp = atoin(pRec->Beds, SIZ_ATTR_BEDS);
   if (iTmp > 0)
   {
      sprintf(acTmp, "%*u  ", SIZ_BEDS, iTmp);
      memcpy(pOutbuf+OFF_BEDS, acTmp, SIZ_BEDS);
   }

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "0346510468", 10))
   //   iTmp = 0;
#endif
   // Baths/Half Baths  OFF_BATH_F
   if (pRec->Bath[0] > '0')
   {
      *(pOutbuf+OFF_BATH_F) = ' ';
      *(pOutbuf+OFF_BATH_F+1) = pRec->Bath[0];
   }
   if (pRec->HBath[0] > '0')
   {
      *(pOutbuf+OFF_BATH_H) = ' ';
      *(pOutbuf+OFF_BATH_H+1) = pRec->HBath[0];
   }

   // YrBlt, YrEff  OFF_YR_BLT
   if (pRec->YrBlt[0] > '0')
      memcpy(pOutbuf+OFF_YR_BLT, pRec->YrBlt, SIZ_YR_BLT);
   if (pRec->YrEff[0] > '0')
      memcpy(pOutbuf+OFF_YR_EFF, pRec->YrEff, SIZ_YR_EFF);

   // Roof Type
   // *(pOutbuf+OFF_ROOF_TYPE) = pRec->RoofType[0];

   // Garage size, Park type
   iTmp = atoin(pRec->GarSize, SIZ_ATTR_GARSIZE);
   iTmp1 = atoin(pRec->CarPortSF, SIZ_ATTR_CPSIZE);
   if (iTmp > 0)
   {
      sprintf(acTmp, "%*u ", SIZ_GAR_SQFT, iTmp);
      if (iTmp1 > 0)
         *(pOutbuf+OFF_PARK_TYPE) = '2';     // GARAGE/CARPORT
      else
         *(pOutbuf+OFF_PARK_TYPE) = 'Z';     // GARAGE
   } else if (pRec->CarPort[0] == 'N' && pRec->Garage[0] != 'N')
      *(pOutbuf+OFF_PARK_TYPE) = 'Z';
   else if (iTmp1 > 0 || (pRec->CarPort[0] != 'N' && pRec->Garage[0] == 'N'))
      *(pOutbuf+OFF_PARK_TYPE) = 'C';        // CARPORT

   // Pool, Spa
   if (pRec->Pool[0] == 'Y' && pRec->Spa[0] == 'Y')
      *(pOutbuf+OFF_POOL) = 'C';
   else if (pRec->Pool[0] == 'Y')
      *(pOutbuf+OFF_POOL) = 'P';
   else if (pRec->Spa[0] == 'Y')
      *(pOutbuf+OFF_POOL) = 'S';

   // Const Type OFF_CONS_TYPE
   *(pOutbuf+OFF_CONS_TYPE) = pRec->Const[0];

   // Units
   iTmp = atoin(pRec->Units, SIZ_ATTR_UNITS);
   if (iTmp > 0)
   {
      sprintf(acTmp, "%*u  ", SIZ_UNITS, iTmp);
      memcpy(pOutbuf+OFF_UNITS, acTmp, SIZ_UNITS);
   }

   // Fp, Heat type OFF_FIRE_PL
   if (pRec->FirePlace[0] > '0')
   {
      *(pOutbuf+OFF_FIRE_PL) = ' ';
      *(pOutbuf+OFF_FIRE_PL+1) = pRec->FirePlace[0];
   }
   *(pOutbuf+OFF_HEAT) = pRec->Heat[0];

   // View
   //if (pRec->ViewFlag[0] != 'N')
   //   *(pOutbuf+OFF_VIEW) = pRec->ViewFlag[0];
   switch (pRec->ViewFlag[0])
   {
      case 'C':   // City view
         *(pOutbuf+OFF_VIEW) = 'S';
         break;
         
      case 'M':   // Mountain view
         *(pOutbuf+OFF_VIEW) = 'T';
         break;

      case 'O':   // Ocean view
         *(pOutbuf+OFF_VIEW) = 'B';
         break;

      case 'V':   // Has view
      case 'Y':
         *(pOutbuf+OFF_VIEW) = 'A';    // 12/19/2016 sn
         break;

      case 'N':   // No view
         *(pOutbuf+OFF_VIEW) = 'N';
         break;

      default:
         *(pOutbuf+OFF_VIEW) = ' ';
         break;
   }

   return 0;
}

/******************************** Scr_MergeEtal2 *****************************
 *
 * Add name2 only, ignore the rest
 *
 *****************************************************************************/

int Scr_MergeEtal2(char *pOutbuf)
{
   static   char acRec[256], *pRec=NULL;
   char     *apItems[4];
   int      iLoop, iTmp;

   // Get first rec for first call
   if (!pRec)
   {
      // Skip header record
      do {
         pRec = fgets(acRec, 256, fdEtal);
      } while (pRec && *pRec > '9');
   }

   //char *pTmp = pRec;
   do
   {
      if (!pRec)
      {
         fclose(fdEtal);
         fdEtal = NULL;
         return 1;      // EOF
      }

      // Compare APN
      iLoop = memcmp(pOutbuf, pRec, iApnLen);
      if (iLoop > 0)
      {
         if (bDebug)
            LogMsg0("Skip Tax rec  %.*s", iApnLen, pRec);
         pRec = fgets(acRec, 256, fdEtal);
         lEtalSkip++;
      }
   } while (iLoop > 0);

   // If not match, return
   if (iLoop)
      return 1;

   if (*(pOutbuf+OFF_NAME2) == ' ')
   {
      // Parse input record
      iLoop = ParseString(pRec, cDelim, 4, apItems);
      if (iLoop < 2)
      {
         LogMsg("***** Error: bad value record for APN=%.*s (#tokens=%d)", iApnLen, pOutbuf, iLoop);
         return -1;
      }

      iTmp = blankRem(apItems[1]);
      if (iTmp > SIZ_NAME2) iTmp = SIZ_NAME2;
      memcpy(pOutbuf+OFF_NAME2, apItems[1], iTmp);
   }

   lEtalMatch++;

   // Get next tax rec
   pRec = fgets(acRec, 256, fdEtal);

   return 0;
}

/******************************** Scr_MergeOwner *****************************
 *
 * Return 0 if successful, >0 if error
 * 08/29/2008 Keep Name1 the same as county provided
 *
 *****************************************************************************/

void Scr_MergeOwner(char *pOutbuf, char *pNames)
{
   int   iTmp;
   char  acOwners[128], acTmp[128], acSave[64];
   char  acVesting[8];
   char  *pTmp, *pTmp1;

   OWNER myOwner;

   // Clear output buffer if needed
   removeNames(pOutbuf, false);

   // Remove multiple spaces
   memcpy(acTmp, pNames, SIZ_SCR_NAME1);
   blankRem(acTmp, SIZ_SCR_NAME1);
   replChar(acTmp, '\\', '/');
   replChar(acTmp, 0xD1, 'N');

   // Update vesting
   iTmp = updateVesting(myCounty.acCntyCode, acTmp, pOutbuf+OFF_VEST, pOutbuf+OFF_ETAL_FLG);

   // Save owner
   strcpy(acOwners, acTmp);

   // Drop everything from these words
   if (pTmp=strstr(acTmp, " AKA "))
      *pTmp = 0;
   if (pTmp=strstr(acTmp, " SUCCESSOR"))
      *pTmp = 0;
   if ((pTmp=strstr(acTmp, " TRUSTE")) || (pTmp=strstr(acTmp, " TTEE")) ||
       (pTmp=strstr(acTmp, " TRSTEE")) )
      *pTmp = 0;
   if ((pTmp=strstr(acTmp, " ETAL ")) || (pTmp=strstr(acTmp, " ET AL")) )
      *pTmp = 0;
   if ((pTmp=strstr(acTmp, " MD TR ")) || (pTmp=strstr(acTmp, " CO-TRUS")) ||
      (pTmp=strstr(acTmp, " INTERVIVOS ")) || (pTmp=strstr(acTmp, " EST OF")) )
      *pTmp = 0;
   if (!memcmp((char *)&acTmp[36], "TRUS", 4) || !memcmp((char *)&acTmp[36], "CO-T", 4))
      acTmp[36] = 0;

   // Save data to append later
   if (pTmp1=strstr(acTmp, "TRUST"))
   {
      if (pTmp=strstr(acTmp, " FAM"))
      {
         strcpy(acSave, ++pTmp);
         *pTmp = 0;
      } else if (pTmp=strstr(acTmp, " REVOCABLE"))
      {
         strcpy(acSave, ++pTmp);
         *pTmp = 0;
      } else if (pTmp=strstr(acTmp, " INDIVIDUAL"))
      {
         strcpy(acSave, ++pTmp);
         *pTmp = 0;
      } else if (pTmp=strstr(acTmp, " LIV"))
      {
         strcpy(acSave, ++pTmp);
         *pTmp = 0;
      } else if (pTmp=strstr(acTmp, " AMENDED "))
      {
         strcpy(acSave, ++pTmp);
         *pTmp = 0;
      } else
      {
         strcpy(acSave, pTmp1);
         *pTmp1 = 0;
      }
   } else if (pTmp=strstr(acTmp, " FAM TR"))
   {
      strcpy(acSave, ++pTmp);
      *pTmp = 0;
   } else if (pTmp=strstr(acTmp, " FAMILY TR"))
   {
      strcpy(acSave, ++pTmp);
      *pTmp = 0;
   } else if (pTmp=strstr(acTmp, " LIV TR"))
   {
      strcpy(acSave, ++pTmp);
      *pTmp = 0;
   } else if (pTmp=strstr(acTmp, " LIVING TR"))
   {
      strcpy(acSave, ++pTmp);
      *pTmp = 0;
   } else if (pTmp=strstr(acTmp, " REV TR"))
   {
      strcpy(acSave, ++pTmp);
      *pTmp = 0;
   } else if (pTmp=strstr(acTmp, " REVOC TR"))
   {
      strcpy(acSave, ++pTmp);
      *pTmp = 0;
   } else if (pTmp=strstr(acTmp, " PERSONAL TR"))
   {
      strcpy(acSave, ++pTmp);
      *pTmp = 0;
   } else
      acSave[0] = 0;

   // Check for vesting
   acVesting[0] = 0;
   if ((pTmp=strstr(acTmp, " H/")) || (pTmp=strstr(acTmp, " W/")))
   {
      *pTmp = 0;
      strcpy(acVesting, "HW");
   } else if (pTmp=strstr(acTmp, " M/M"))
   {
      *pTmp = 0;
      strcpy(acVesting, "MM");
   } else if (pTmp=strstr(acTmp, " M/W"))
   {
      *pTmp = 0;
      strcpy(acVesting, "MW");
   } else if (pTmp=strstr(acTmp, " U/M"))
   {
      *pTmp = 0;
      strcpy(acVesting, "UM");
   } else if (pTmp=strstr(acTmp, " U/W"))
   {
      *pTmp = 0;
      strcpy(acVesting, "UW");
   } else if (pTmp=strstr(acTmp, " S/M"))
   {
      *pTmp = 0;
      strcpy(acVesting, "SM");
   } else if (pTmp=strstr(acTmp, " S/P"))
   {
      *pTmp = 0;
      strcpy(acVesting, "SP");
   } else if (pTmp=strstr(acTmp, " S/W"))
   {
      *pTmp = 0;
      strcpy(acVesting, "SW");
   } else if (pTmp=strstr(acTmp, " JT"))
   {
      *pTmp = 0;
      strcpy(acVesting, "JT");
   }


   // Now parse owners
   acTmp[63] = 0;
   splitOwner(acTmp, &myOwner, 3);
   if (acSave[0])
      strcat(myOwner.acName1, acSave);

   // Name1
   vmemcpy(pOutbuf+OFF_NAME1, acOwners, SIZ_NAME1);

   // Swapped name
   if (!strchr(myOwner.acSwapName, ' '))
      strcpy(myOwner.acSwapName, acOwners);
   vmemcpy(pOutbuf+OFF_NAME_SWAP, myOwner.acSwapName, SIZ_NAME_SWAP);
}

/********************************* Scr_MergeAdr ******************************
 *
 * Return 0 if successful, >0 if error
 *
 *****************************************************************************/

void Scr_MergeAdr(char *pOutbuf, char *pRollRec)
{
   SCR_ROLL *pRec;
   char     *pAddr1, acTmp[256], acAddr1[64], acAddr2[64];
   int      iTmp, iStrNo;
   ADR_REC  sMailAdr;
   ADR_REC  sSitusAdr;

   pRec = (SCR_ROLL *)pRollRec;

   // Clear old addr
   removeMailing(pOutbuf, false);
   removeSitus(pOutbuf);

   // Check for blank address
   if (memcmp(pRec->M_Addr1, "     ", 5))
   {
      memcpy(acAddr1, pRec->M_Addr1, SIZ_SCR_MAILADDR);
      pAddr1 = myTrim(acAddr1, SIZ_SCR_MAILADDR);

      // Parsing mail address
      memset((void *)&sMailAdr, 0, sizeof(ADR_REC));
      parseAdr1(&sMailAdr, acAddr1);

      if (sMailAdr.lStrNum > 0)
      {
         iTmp = sprintf(acTmp, "%d", sMailAdr.lStrNum);
         memcpy(pOutbuf+OFF_M_STRNUM, acTmp, iTmp);
         if (sMailAdr.strSub[0] > '0')
         {
            sprintf(acTmp, "%s  ", sMailAdr.strSub);
            memcpy(pOutbuf+OFF_M_STR_SUB, acTmp, SIZ_M_STR_SUB);
         }
      }
      blankPad(sMailAdr.strDir, SIZ_M_DIR);
      memcpy(pOutbuf+OFF_M_DIR, sMailAdr.strDir, SIZ_M_DIR);
      blankPad(sMailAdr.strName, SIZ_M_STREET);
      memcpy(pOutbuf+OFF_M_STREET, sMailAdr.strName, SIZ_M_STREET);
      blankPad(sMailAdr.strSfx, SIZ_M_SUFF);
      memcpy(pOutbuf+OFF_M_SUFF, sMailAdr.strSfx, SIZ_M_SUFF);
      memcpy(pOutbuf+OFF_M_ADDR_D, acAddr1, strlen(acAddr1));

      memcpy(sMailAdr.City, pRec->M_City, SIZ_SCR_MAILCITY);
      blankPad(sMailAdr.City, SIZ_M_CITY);
      memcpy(pOutbuf+OFF_M_CITY, sMailAdr.City, SIZ_M_CITY);
      memcpy(pOutbuf+OFF_M_ST, pRec->M_St, SIZ_M_ST);

      iTmp = atoin(pRec->M_Zip, 5);
      if (iTmp > 500)
         memcpy(pOutbuf+OFF_M_ZIP, pRec->M_Zip, SIZ_M_ZIP);
      else
         memcpy(pOutbuf+OFF_M_ZIP, BLANK32, SIZ_M_ZIP);

      sMailAdr.City[SIZ_M_CITY] = 0;
      iTmp = sprintf(acAddr2, "%s %.2s %.5s", myTrim(sMailAdr.City), pRec->M_St, pRec->M_Zip);
      memcpy(pOutbuf+OFF_M_CTY_ST_D, acAddr2, iTmp);
   }

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "00116153", 8))
   //   iTmp = 0;
#endif

   // Situs
   acAddr1[0] = 0;
   acAddr2[0] = 0;
   memset((void *)&sSitusAdr, 0, sizeof(ADR_REC));

   iStrNo = atoin(pRec->S_StreetNum, SIZ_SCR_S_STRNUM);
   if (iStrNo > 0 && pRec->S_StreetName[0] >= ' ')
   {
      int   iIdx=0;

#ifdef _DEBUG
      //if (isCharIncluded(pRec->S_StreetNum, '/', SIZ_SCR_S_STRNUM))
      //   iTmp = 0;
#endif
      // Copy StrNum
      //memset(pOutbuf+OFF_S_HSENO, ' ', SIZ_S_HSENO);
      //memcpy(acAddr1, pRec->S_StreetNum, SIZ_SCR_S_STRNUM);
      //iTmp = blankRem(acAddr1, SIZ_SCR_S_STRNUM);

      sprintf(acAddr1, "%d       ", iStrNo);
      memcpy(pOutbuf+OFF_S_STRNUM, acAddr1, SIZ_S_STRNUM);
      memcpy(pOutbuf+OFF_S_HSENO, acAddr1, SIZ_S_STRNUM);

      // Copy street name
      memcpy(acTmp, pRec->S_StreetName, SIZ_SCR_S_STRNAME);
      acTmp[SIZ_SCR_S_STRNAME] = 0;
      parseAdr1S(&sSitusAdr, myTrim(acTmp));

      // Forming addr1
      if (pRec->S_StreetFract[0] > '0')
      {
         memcpy(pOutbuf+OFF_S_STR_SUB, pRec->S_StreetFract, SIZ_SCR_S_FRACT);
         sprintf(acAddr2, " %.*s ", SIZ_SCR_S_FRACT, pRec->S_StreetFract);
         strcat(acAddr1, acAddr2);
      }
      strcat(acAddr1, acTmp);

      if (sSitusAdr.strDir[0] > ' ')
         memcpy(pOutbuf+OFF_S_DIR, sSitusAdr.strDir, strlen(sSitusAdr.strDir));
      if (sSitusAdr.strName[0] > ' ')
         memcpy(pOutbuf+OFF_S_STREET, sSitusAdr.strName, strlen(sSitusAdr.strName));
      if (sSitusAdr.strSfx[0] > ' ')
         memcpy(pOutbuf+OFF_S_SUFF, sSitusAdr.strSfx, strlen(sSitusAdr.strSfx));

      // Copy unit#
      if (pRec->S_Unit[0] > ' ')
      {
         memcpy(pOutbuf+OFF_S_UNITNO, pRec->S_Unit, SIZ_S_UNITNO);
         memcpy(acTmp, pRec->S_Unit, SIZ_SCR_S_UNIT);
         myTrim(acTmp, SIZ_SCR_S_UNIT);
         strcat(acAddr1, " #");
         strcat(acAddr1, acTmp);
      }

      iTmp = blankRem(acAddr1);
      memcpy(pOutbuf+OFF_S_ADDR_D, acAddr1, iTmp);

      // City & state
      memcpy(pOutbuf+OFF_S_ST, "CA", 2);
      if (pRec->S_City[0] >= 'A')
      {
         memcpy(acTmp, pRec->S_City, SIZ_SCR_S_CITY);
         acTmp[SIZ_SCR_S_CITY] = 0;
         City2Code(myTrim(acTmp), sSitusAdr.City);
         if (pRec->S_Zip[0] == '9')
         {
            memcpy(sSitusAdr.Zip, pRec->S_Zip, SIZ_S_ZIP);
            if (isdigit(pRec->S_Zip4[0]))
               memcpy(sSitusAdr.Zip4, pRec->S_Zip4, SIZ_S_ZIP4);
         }
      } else if (!strcmp(myTrim(sMailAdr.strName), myTrim(sSitusAdr.strName)) && sMailAdr.City[0] > ' ')
      {
         // Assuming that no situs city and we have to use mail city
         City2Code(sMailAdr.City, sSitusAdr.City);
         strcpy(acTmp, sMailAdr.City);
         if (sSitusAdr.City[0] > ' ')
            memcpy(sSitusAdr.Zip, pRec->M_Zip, 5);    // Zip only
      } else
      {
         if ((sMailAdr.City[0] >= 'A') && (pRec->M_Zip[0] == '9') && pRec->ExeCode == 'H')
         {
            // Get city code
            City2Code(sMailAdr.City, sSitusAdr.City);
            strcpy(acTmp, sMailAdr.City);
            if (sSitusAdr.City[0] > ' ')
               memcpy(sSitusAdr.Zip, pRec->M_Zip, 5); // Zip only
         }
      }

      if (sSitusAdr.City[0] > ' ')
      {
         memcpy(pOutbuf+OFF_S_CITY, sSitusAdr.City, sizeof(sSitusAdr.City));
         memcpy(pOutbuf+OFF_S_ST, "CA", 2);
         if (sSitusAdr.Zip[0] == '9')
         {
            memcpy(pOutbuf+OFF_S_ZIP, sSitusAdr.Zip, SIZ_S_ZIP);
            if (sSitusAdr.Zip4[0] > ' ')
            {
               memcpy(pOutbuf+OFF_S_ZIP4, sSitusAdr.Zip4, SIZ_S_ZIP4);
               iTmp = sprintf(acAddr2, "%s CA %.5s-%.4s", acTmp, sSitusAdr.Zip, sSitusAdr.Zip4);
            } else
               iTmp = sprintf(acAddr2, "%s CA %.5s", acTmp, sSitusAdr.Zip);
         } else
            iTmp = sprintf(acAddr2, "%s CA", acTmp);
         memcpy(pOutbuf+OFF_S_CTY_ST_D, acAddr2, iTmp);
      }
   }
}

/********************************* Scr_MergeChar *****************************
 *
 * Merge Char record into roll file
 *
 *****************************************************************************/

int Scr_MergeChar(char *pOutbuf)
{
   static   char acRec[1024], *pRec=NULL;
   int      iRet, iLoop;
   ATTR_REC *pChar;

   // Get first Char rec for first call
   if (!pRec)
   {
      pRec = fgets(acRec, 1024, fdChar);
   }
   pChar = (ATTR_REC *)pRec;

   do
   {
      if (!pRec)
      {
         fclose(fdChar);
         fdChar = NULL;
         return 1;      // EOF
      }

      // Compare Apn
      iLoop = memcmp(pOutbuf, pChar->Apn, iApnLen);
      if (iLoop > 0)
      {
         if (bDebug)
            LogMsg0("Skip Char rec  %.*s", iApnLen, pChar->Apn);
         pRec = fgets(acRec, 1024, fdChar);
         lCharSkip++;
      }
   } while (iLoop > 0);

   // If not match, return
   if (iLoop)
      return 0;

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "0346510468", 9))
   //   iRet = 0;
#endif

   iRet = Scr_MergeAttr(pOutbuf, acRec);

   lCharMatch++;

   // Get next Char rec
   pRec = fgets(acRec, 1024, fdChar);

   return 0;
}

/********************************** Scr_MergeRoll ****************************
 *
 * Return 0 if successful, >0 if error
 *
 *****************************************************************************/

int Scr_MergeRoll(char *pOutbuf, char *pRollRec, int iFlag)
{
   SCR_ROLL *pRec;
   char     *pTmp, acTmp[256], acTmp1[256];
   long     lTmp;
   int      iRet;

   // Replace tab char with 0
   pTmp = pRollRec;

   pRec = (SCR_ROLL *)pRollRec;
   if (iFlag & CREATE_R01)
   {
      // Clear output buffer
      memset(pOutbuf, ' ', iRecLen);

      // Start copying data
      memcpy(pOutbuf, pRec->Apn, SIZ_SCR_APN);
      memcpy(pOutbuf+OFF_CO_NUM, "44SCR", 5);

      // Format APN
      iRet = formatApn(pRec->Apn, acTmp, &myCounty);
      memcpy(pOutbuf+OFF_APN_D, acTmp, iRet);

      // Create MapLink
      iRet = formatMapLink(pRec->Apn, acTmp, &myCounty);
      memcpy(pOutbuf+OFF_MAPLINK, acTmp, iRet);

      // Create index map link
      if (getIndexPage(acTmp, acTmp1, &myCounty))
         memcpy(pOutbuf+OFF_IMAPLINK, acTmp1, iRet);

      // Year assessed
      memcpy(pOutbuf+OFF_YR_ASSD, myCounty.acYearAssd, 4);

      // HO Exempt
      if (pRec->ExeCode == 'H')
         *(pOutbuf+OFF_HO_FL) = '1';      // 'Y'
      else
         *(pOutbuf+OFF_HO_FL) = '2';      // 'N'

      // Land
      long lLand = atoin(pRec->Land, SIZ_SCR_LAND);
      if (lLand > 0)
      {
         sprintf(acTmp, "%*u", SIZ_LAND, lLand);
         memcpy(pOutbuf+OFF_LAND, acTmp, SIZ_LAND);
      }

      // Improve
      long lImpr = atoin(pRec->Impr, SIZ_SCR_IMPR);
      if (lImpr > 0)
      {
         sprintf(acTmp, "%*u", SIZ_IMPR, lImpr);
         memcpy(pOutbuf+OFF_IMPR, acTmp, SIZ_IMPR);
      }

      // Other value
      long lFixture = atoin(pRec->ME_Val, SIZ_SCR_ME_VAL);
      long lPers = atoin(pRec->PP_Val, SIZ_SCR_PP_VAL);
      lTmp = lPers + lFixture;
      if (lTmp > 0)
      {
         sprintf(acTmp, "%*u", SIZ_OTHER, lTmp);
         memcpy(pOutbuf+OFF_OTHER, acTmp, SIZ_OTHER);

         if (lPers > 0)
         {
            sprintf(acTmp, "%d         ", lPers);
            memcpy(pOutbuf+OFF_PERSPROP, acTmp, SIZ_PERSPROP);
         }
         if (lFixture > 0)
         {
            sprintf(acTmp, "%d         ", lFixture);
            memcpy(pOutbuf+OFF_FIXTR, acTmp, SIZ_FIXTR);
         }
      }

      // Gross total
      lTmp += lLand+lImpr;
      if (lTmp > 0)
      {
         sprintf(acTmp, "%*u", SIZ_GROSS, lTmp);
         memcpy(pOutbuf+OFF_GROSS, acTmp, SIZ_GROSS);
      }

      // Ratio
      if (lImpr > 0)
      {
         sprintf(acTmp, "%*u", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
         memcpy(pOutbuf+OFF_RATIO, acTmp, SIZ_RATIO);
      }

      // Set full exemption flag, See Travis email 12/15/2010
      if (lTmp <= 2000)
         *(pOutbuf+OFF_FULL_EXEMPT) = 'Y';

      // Exemp total
      lTmp = atoin(pRec->HOEX, SIZ_SCR_EXEMP_AMT);
      if (lTmp > 0)
      {
         sprintf(acTmp, "%*u", SIZ_EXE_TOTAL, lTmp);
         memcpy(pOutbuf+OFF_EXE_TOTAL, acTmp, SIZ_EXE_TOTAL);
      }
   }

   // TRA
   lTmp = atoin(pRec->TRA, SIZ_SCR_TRA);
   if (lTmp > 0)
   {
      iRet = sprintf(acTmp, "%.6d", lTmp);
      memcpy(pOutbuf+OFF_TRA, acTmp, iRet);
   }

   // UseCode
   if (pRec->UseCode[0] > ' ')
   {
      memcpy(pOutbuf+OFF_USE_CO, pRec->UseCode, SIZ_SCR_USECODE);
      updateStdUse(pOutbuf+OFF_USE_STD, pRec->UseCode, SIZ_SCR_USECODE, pOutbuf);
   } else
      memcpy(pOutbuf+OFF_USE_STD, USE_UNASGN, SIZ_USE_STD);

   // Set prop8 flag  9/5/2012
   if (!memcmp(pRec->LandCode, "52", 2) || !memcmp(pRec->LandCode, "53", 2))
      *(pOutbuf+OFF_PROP8_FLG ) = 'Y';

   // Owner
   Scr_MergeOwner(pOutbuf, pRec->Name1);

   // Care Of
   //if (pRec->CareOf[0] > ' ')
      //memcpy(pOutbuf+OFF_CARE_OF, pRec->CareOf, SIZ_CARE_OF);
   iRet = updateCareOf(pOutbuf, pRec->CareOf, SIZ_SCR_CAREOF);

   // Mailing - Situs
   Scr_MergeAdr(pOutbuf, pRollRec);

   // Legal

   // Acres

   return 0;
}

/********************************* Scr_MergeSale ******************************
 *
 * Input: Sale file is output from Scr_ExtrSale().
 * Notes: Since we don't have translation table for DocType yet, we temporarily
 *        not merge it by blank oyt DocType before calling MergeSale1().
 *
 ******************************************************************************/

int Scr_MergeSale(char *pOutbuf)
{
   static   char  acRec[1024], *pRec=NULL;
   int      iRet, iLoop;
   SALE_REC *pSale;

   // Get first Char rec for first call
   if (!pRec)
      pRec = fgets(acRec, 1024, fdSale);

   pSale = (SALE_REC *)&acRec[0];
   do
   {
      if (!pRec)
      {
         fclose(fdSale);
         fdSale = NULL;
         return 1;      // EOF
      }

      // Compare Apn
      iLoop = memcmp(pOutbuf, pSale->acApn, iApnLen);
      if (iLoop > 0)
      {
         if (bDebug)
            LogMsg0("Skip Sale rec  %.*s", iApnLen, pSale->acApn);
         pRec = fgets(acRec, 1024, fdSale);
         lSaleSkip++;
      }
   } while (iLoop > 0);

   // If not match, return
   if (iLoop)
      return 0;

   do
   {
#ifdef _DEBUG
//      if (!memcmp(pOutbuf, "10145008", 8))
//         iRet = 0;
#endif

      memset(pSale->acDocType, ' ', 2);
      iRet = MergeSale1((SALE_REC *)&acRec[0], pOutbuf, true);

      // Get next sale record
      pRec = fgets(acRec, 1024, fdSale);
      if (!pRec)
      {
         fclose(fdSale);
         fdSale = NULL;
         break;
      }

   } while (!memcmp(pOutbuf, pSale->acApn, iApnLen));

   // Save last recording date
   long lRet = atoin(pOutbuf+OFF_SALE1_DT, 8);
   if (lRet > lLastRecDate && lRet < lToday)
      lLastRecDate = lRet;

   lSaleMatch++;

   return 0;
}

/********************************* Scr_Load_Roll ******************************
 *
 *
 ******************************************************************************/

int Scr_Load_Roll(int iSkip)
{
   char     *pTmp, acRec[MAX_RECSIZE], acBuf[MAX_RECSIZE], acRollRec[MAX_RECSIZE];
   char     acRawFile[_MAX_PATH], acOutFile[_MAX_PATH];

   HANDLE   fhIn, fhOut;

   int      iRet, iTmp, iRollUpd=0, iNewRec=0, iRetiredRec=0;
   DWORD    nBytesRead;
   DWORD    nBytesWritten;
   BOOL     bRet, bEof;
   long     lRet=0, lCnt=0;

   LogMsg0("Update Roll with Scr_Load_Roll()");

   sprintf(acRawFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "S01");
   sprintf(acOutFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "R01");

   // Rename files for processing
   if (_access(acRawFile, 0))
   {
      if (!_access(acOutFile, 0))
         rename(acOutFile, acRawFile);
      else
      {
         LogMsg("Missing input file %s.  Please recheck!", acRawFile);
         return 1;
      }
   }

   // Open Roll file
   LogMsg("Open Roll file %s", acRollFile);
   fdRoll = fopen(acRollFile, "r");
   if (fdRoll == NULL)
   {
      printf("Error opening roll file: %s\n", acRollFile);
      return 2;
   }

   // Open Char file
   sprintf(acCharFile, acAttrTmpl, myCounty.acCntyCode, "DAT");
   LogMsg("Open Char file %s", acCharFile);
   fdChar = fopen(acCharFile, "r");
   if (fdChar == NULL)
   {
      LogMsg("***** Error opening Char file: %s\n", acCharFile);
      return 2;
   }

   // Open Sale file
   //sprintf(acSaleFile, acESalTmpl, myCounty.acCntyCode, "DAT");
   //LogMsg("Open sale file %s", acSaleFile);
   //fdSale = fopen(acSaleFile, "r");
   //if (fdSale == NULL)
   //{
   //   LogMsg("***** Error opening sale file: %s\n", acSaleFile);
   //   return 2;
   //}

   // Open Input file
   LogMsg("Open input file %s", acRawFile);
   fhIn = CreateFile(acRawFile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhIn == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening input file: %s\n", acRawFile);
      return 3;
   }

   // Open Output file
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening output file: %s\n", acOutFile);
      return 4;
   }

   // Get first RollRec
   pTmp = fgets((char *)&acRollRec[0], iRollLen, fdRoll);
   bEof = (pTmp ? false:true);

   // Copy skip record
   memset(acBuf, ' ', iRecLen);
   while (iSkip-- > 0)
   {
      ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);
      // Ignore record start with 99999999
      //if (memcmp(acBuf, "99999999", 8))
         bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
   }

   iNoMatch=iBadCity=iBadSuffix=0;
   lCharMatch=lCharSkip=lSaleMatch=lSaleSkip=0;

   // Merge loop
   while (!bEof)
   {
      bRet = ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);

      // Check for EOF
      if (!bRet)
      {
         LogMsg("***** Error reading input file %s (%f)", acRawFile, GetLastError());
         break;
      }

      // EOF ?
      if (!nBytesRead)
         break;

      NextRollRec:
      iTmp = memcmp(acBuf, acRollRec, iApnLen);
      if (!iTmp)
      {
         // Merge roll data
         iRet = Scr_MergeRoll(acBuf, acRollRec, UPDATE_R01);
         iRollUpd++;

         // Merge Char
         if (fdChar)
            iRet = Scr_MergeChar(acBuf);

         // Merge Sale
         //if (fdSale)
         //   iRet = Scr_MergeSale(acBuf);

         // Read next roll record
         pTmp = fgets(acRollRec, iRollLen, fdRoll);

         if (!pTmp)
            bEof = true;         // Signal to stop
      } else if (iTmp > 0)       // Roll not match, new roll record?
      {
         if (bDebug)
            LogMsg0("New roll record : %.*s (%d)", iApnLen, acRollRec, lCnt);

         // Create new R01 record
         iRet = Scr_MergeRoll(acRec, acRollRec, CREATE_R01);
         iNewRec++;

         // Merge Char
         if (fdChar)
            iRet = Scr_MergeChar(acRec);

         // Merge Sale
         //if (fdSale)
         //   iRet = Scr_MergeSale(acRec);

         bRet = WriteFile(fhOut, acRec, iRecLen, &nBytesWritten, NULL);
         lCnt++;

         // Get next roll record
         pTmp = fgets(acRollRec, iRollLen, fdRoll);

         if (!pTmp)
            bEof = true;    // Signal to stop
         else
            goto NextRollRec;
      } else
      {
         // Record may be retired
         if (bDebug)
            LogMsg0("Roll not match (retired record?) : R01->%.*s < Roll->%.*s (%d)", iApnLen, acBuf, iApnLen, acRollRec, lCnt);
         iRetiredRec++;
         continue;
      }

      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);

      if (!bRet)
      {
         LogMsg("Error occurs: %d\n", GetLastError());
         lRet = WRITE_ERR;
         break;
      }
   }

   // Do the rest of the file
   while (!bEof)
   {
      if (bDebug)
         LogMsg0("New roll record : %.*s (%d)", iApnLen, acRollRec, lCnt);

      // Create new R01 record
      iRet = Scr_MergeRoll(acRec, acRollRec, CREATE_R01);
      iNewRec++;

      // Merge Char
      if (fdChar)
         iRet = Scr_MergeChar(acRec);

      // Merge Sale
      //if (fdSale)
      //   iRet = Scr_MergeSale(acRec);

      bRet = WriteFile(fhOut, acRec, iRecLen, &nBytesWritten, NULL);
      lCnt++;

      // Get next roll record
      if (!(pTmp = fgets(acRollRec, iRollLen, fdRoll)))
         bEof = true;    // Signal to stop
   }

   // Do the rest of the file
   /*
   while (nBytesRead>0 && ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL))
   {
      if (!nBytesRead)
         break;

      LogMsg("*** End of old R01= %.*s Roll=%.*s", iApnLen, acBuf, iApnLen, acRollRec);

      // Merge Char
      if (fdChar)
         iRet = Scr_MergeChar(acBuf);

      // Merge Sale
      if (fdSale)
         iRet = Scr_MergeSale(acBuf);

      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
      iNoMatch++;
      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }
   */

   // Close files
   if (fdRoll)
      fclose(fdRoll);
   if (fdChar)
      fclose(fdChar);
   //if (fdSale)
   //   fclose(fdSale);
   if (fhOut)
      CloseHandle(fhOut);
   if (fhIn)
      CloseHandle(fhIn);

   LogMsg("Total output records:       %u", lCnt);
   LogMsg("Total new records:          %u", iNewRec);
   LogMsg("Total retired records:      %u\n", iRetiredRec);

   LogMsg("Total Char matched:         %u", lCharMatch);
   LogMsg("Total Char skiped:          %u\n", lCharSkip);

   //LogMsg("Total Sale matched:         %u", lSaleMatch);
   //LogMsg("Total Sale skiped:          %u\n", lSaleSkip);

   LogMsg("Total bad-city records:     %u", iBadCity);
   LogMsg("Total bad-suffix records:   %u\n", iBadSuffix);

   printf("\nTotal output records: %u\n", lCnt);

   lRecCnt = lCnt;
   return 0;
}

/********************************* Scr_Load_Roll ******************************
 *
   1.LegalFile=SCC_ParcelQuest_Get_PrimaryOwner_TRA_UseCode_LegalDescr.txt
   2.StatFile=SCC_ParcelQuest_Get_ParcelStatus.txt
   3.ValueFile=SCC_ParcelQuest_Get_AssessmentValues_Exemptions.txt
   4.OwnerFile=SCC_ParcelQuest_Get_PrimaryOwnerMailingAddress.txt
   5.SitusFile=SCC_ParcelQuest_Get_SitusAddresses.txt
   6.CharFile=SCC_ParcelQuest_Get_Characteristics.txt
   7.TaxFile=SCC_ParcelQuest_Get_TaxYearTaxesDue_Paid.txt
   8.DelqFile=SCC_ParcelQuest_Get_DelinquentBills.txt
   9.EtalFile=SCC_ParcelQuest_Get_Etals.txt
   X.SaleFile=SCC_ParcelQuest_Get_SalesTransfers.txt
 *
 * SCR now provide a group of files to update our roll.  We have to merge them
 * to form the roll.  Legal file will be used as based roll file then add other 
 * data to form a roll record.  Sale file is extracted for historical update.
 *
 ******************************************************************************/

int Scr_Load_Roll2(int iSkip)
{
   char     *pTmp, acRec[MAX_RECSIZE], acBuf[MAX_RECSIZE], acRollRec[MAX_RECSIZE];
   char     acRawFile[_MAX_PATH], acOutFile[_MAX_PATH], acTmp[256];

   HANDLE   fhIn, fhOut;

   int      iRet, iTmp, iRollUpd=0, iNewRec=0, iRetiredRec=0;
   DWORD    nBytesRead;
   DWORD    nBytesWritten;
   BOOL     bRet, bEof;
   long     lRet=0, lCnt=0;

   LogMsg0("Update Roll with Scr_Load_Roll2()");

   sprintf(acRawFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "S01");
   sprintf(acOutFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "R01");

   // Rename files for processing
   if (_access(acRawFile, 0))
   {
      if (!_access(acOutFile, 0))
         rename(acOutFile, acRawFile);
      else
      {
         LogMsg("***** Missing input file %s.  Please recheck!", acRawFile);
         return -1;
      }
   }

   // Open Legal file
   GetIniString(myCounty.acCntyCode, "LegalFile", "", acRec, 256, acIniFile);
   sprintf(acTmp, "%s\\SCR\\SCR_Legal.txt", acTmpPath);
   iRet = RebuildCsv_IQ(acRec, acTmp, cDelim, 6);
   if (iRet <= 0)
      return iRet;

   sprintf(acRec, "%s\\SCR\\SCR_Legal.srt", acTmpPath);
   sprintf(acBuf, "S(#1,C,A) DEL(%d) OMIT(1,1,C,GT,\"9\") F(TXT) ", cDelim);
   iTmp = sortFile(acTmp, acRec, acBuf);
   LogMsg("Open Legal file %s", acRec);
   fdLegal = fopen(acRec, "r");
   if (fdLegal == NULL)
   {
      LogMsg("***** Error opening Legal file: %s\n", acRec);
      return -2;
   }

   // Open Status file
   GetIniString(myCounty.acCntyCode, "StatFile", "", acRec, 256, acIniFile);
   sprintf(acTmp, "%s\\SCR\\SCR_Status.txt", acTmpPath);
   iTmp = sortFile(acRec, acTmp, "S(#1,C,A) DEL(124) F(TXT) ");
   LogMsg("Open Status file %s", acTmp);
   fdStat = fopen(acTmp, "r");
   if (fdStat == NULL)
   {
      LogMsg("***** Error opening Status file: %s\n", acTmp);
      return -2;
   }

   // Open Value file
   GetIniString(myCounty.acCntyCode, "ValueFile", "", acRec, 256, acIniFile);
   sprintf(acTmp, "%s\\SCR\\SCR_Values.txt", acTmpPath);
   iTmp = sortFile(acRec, acTmp, "S(#1,C,A) DEL(124) F(TXT) ");
   LogMsg("Open Value file %s", acTmp);
   fdValue = fopen(acTmp, "r");
   if (fdValue == NULL)
   {
      LogMsg("***** Error opening Value file: %s\n", acTmp);
      return -2;
   }

   // Open Owner mail file
   GetIniString(myCounty.acCntyCode, "MailFile", "", acRec, 256, acIniFile);
   sprintf(acTmp, "%s\\SCR\\SCR_Mailing.txt", acTmpPath);
   iTmp = sortFile(acRec, acTmp, "S(#1,C,A) DEL(124) F(TXT) ");
   LogMsg("Open Mail file %s", acTmp);
   fdMail = fopen(acTmp, "r");
   if (fdMail == NULL)
   {
      LogMsg("***** Error opening Mail file: %s\n", acTmp);
      return -2;
   }

   // Open Situs file
   GetIniString(myCounty.acCntyCode, "SitusFile", "", acRec, 256, acIniFile);
   sprintf(acTmp, "%s\\SCR\\SCR_Situs.txt", acTmpPath);
   iTmp = sortFile(acRec, acTmp, "S(#1,C,A,#2,C,A,#3,C,A,#8,C,A) DEL(124) F(TXT) ");
   LogMsg("Open Situs file %s", acTmp);
   fdSitus = fopen(acTmp, "r");
   if (fdSitus == NULL)
   {
      LogMsg("***** Error opening Situs file: %s\n", acTmp);
      return -2;
   }

   // Open Char file
   LogMsg("Open Char file %s", acCChrFile);
   fdChar = fopen(acCChrFile, "r");
   if (fdChar == NULL)
   {
      LogMsg("***** Error opening Char file: %s\n", acCChrFile);
      return -2;
   }

   // Open Tax file
   GetIniString(myCounty.acCntyCode, "TaxRoll", "", acRec, 256, acIniFile);
   sprintf(acTmp, "%s\\SCR\\SCR_Tax.txt", acTmpPath);
   iTmp = sortFile(acRec, acTmp, "S(#1,C,A) DEL(124) F(TXT) ");
   LogMsg("Open Tax file %s", acTmp);
   fdTax = fopen(acTmp, "r");
   if (fdTax == NULL)
   {
      LogMsg("***** Error opening Tax file: %s\n", acTmp);
      return -2;
   }
   
   // Open Input file
   LogMsg("Open input file %s", acRawFile);
   fhIn = CreateFile(acRawFile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhIn == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening input file: %s\n", acRawFile);
      return -3;
   }

   // Open Output file
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening output file: %s\n", acOutFile);
      return -4;
   }

   // Get first RollRec
   pTmp = fgets((char *)&acRollRec[0], MAX_RECSIZE, fdLegal);
   bEof = (pTmp ? false:true);

   // Copy skip record
   memset(acBuf, ' ', iRecLen);
   while (iSkip-- > 0)
   {
      ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);
      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
   }

   iNoMatch=iBadCity=iBadSuffix=0;
   lCharMatch=lCharSkip=0;

   // Merge loop
   while (!bEof)
   {
      bRet = ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);

      // Check for EOF
      if (!bRet)
      {
         LogMsg("***** Error reading input file %s (%f)", acRawFile, GetLastError());
         break;
      }

      // EOF ?
      if (!nBytesRead)
         break;

      NextRollRec:
#ifdef _DEBUG
      //if (!memcmp(acRollRec, "24805005", 10))
      //   iTmp = 0;
#endif

      iApnLen = strchr(acRollRec, cDelim) - &acRollRec[0];
      sprintf(acTmp, "%.*s     ", iApnLen, acRollRec);
      
      iTmp = memcmp(acBuf, acTmp, 12);
      if (!iTmp)
      {
         // Merge roll data
         iRet = Scr_MergeLegalN(acBuf, acRollRec, UPDATE_R01);
         if (!iRet)
         {
            // Merge Situs
            if (fdSitus)
            {
               iRet = Scr_MergeSitusAdr(acBuf);
               if (acBuf[OFF_S_STRNUM] == ' ' && acBuf[OFF_S_STREET] > ' ')
                  iRet = Scr_MergeSitusAdr(acBuf);
            }

            // Merge Owner mail addr
            if (fdMail)
               iRet = Scr_MergeMailAdr(acBuf);

            // Merge Char
            if (fdChar)
               iRet = Scr_MergeStdChar(acBuf);

            // Merge Tax
            if (fdTax)
               iRet = Scr_MergeTax2(acBuf);

            // Merge Etal
            if (fdEtal)
               iRet = Scr_MergeEtal2(acBuf);

            bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
            iRollUpd++;
         } else
            iTmp = 0;

         // Read next roll record
         pTmp = fgets(acRollRec, MAX_RECSIZE, fdLegal);
         lCnt++;

         if (!pTmp)
            bEof = true;         // Signal to stop
      } else if (iTmp > 0)       // New roll record?
      {
         if (acRollRec[0] != '6')
         {
            // Create new R01 record
            iRet = Scr_MergeLegalN(acRec, acRollRec, CREATE_R01);
            if (!iRet)
            {
               // Merge values
               if (fdValue)
                  iRet = Scr_MergeValue(acRec);
         
               if (iRet != 2)
               {
                  iNewRec++;

                  // Merge Situs
                  if (fdSitus)
                     iRet = Scr_MergeSitusAdr(acRec);

                  // Merge Owner mail addr
                  if (fdMail)
                     iRet = Scr_MergeMailAdr(acRec);

                  // Merge Char
                  if (fdChar)
                     iRet = Scr_MergeStdChar(acRec);

                  // Merge Tax
                  if (fdTax)
                     iRet = Scr_MergeTax2(acRec);

                  // Merge Etal
                  if (fdEtal)
                     iRet = Scr_MergeEtal2(acRec);

                  bRet = WriteFile(fhOut, acRec, iRecLen, &nBytesWritten, NULL);
               }
            }
         }

         // Get next roll record
         pTmp = fgets(acRollRec, MAX_RECSIZE, fdLegal);
         lCnt++;

         if (!pTmp)
            bEof = true;    // Signal to stop
         else
            goto NextRollRec;
      } else
      {
         // Record may be retired
         if (bDebug)
            LogMsg0("Roll not match (retired record?) : R01->%.*s < Roll->%.*s (%d)", iApnLen, acBuf, iApnLen, acRollRec, lCnt);
         iRetiredRec++;
         continue;
      }

      if (!(lCnt % 1000))
         printf("\r%u", lCnt);

      if (!bRet)
      {
         LogMsg("Error occurs: %d\n", GetLastError());
         lRet = WRITE_ERR;
         break;
      }
   }

   // Do the rest of the file
   while (!bEof && acRollRec[0] <= '9')
   {
      // Skip unsecured parcels
      if (acRollRec[0] != '6')
      {
         if (bDebug)
            LogMsg0("New roll record : %.*s (%d)", iApnLen, acRollRec, lCnt);

         // Create new R01 record
         iRet = Scr_MergeLegalN(acRec, acRollRec, CREATE_R01);
         if (!iRet)
         {
            // Merge values
            if (fdValue)
               iRet = Scr_MergeValue(acRec);
            
            if (iRet != 2)
            {
               iNewRec++;

               // Merge Situs
               if (fdSitus)
                  iRet = Scr_MergeSitusAdr(acRec);

               // Merge Owner mail addr
               if (fdMail)
                  iRet = Scr_MergeMailAdr(acRec);
           
               // Merge Char
               if (fdChar)
                  iRet = Scr_MergeStdChar(acRec);

               // Merge Tax
               if (fdTax)
                  iRet = Scr_MergeTax2(acRec);

               // Merge Etal
               if (fdEtal)
                  iRet = Scr_MergeEtal2(acRec);

               bRet = WriteFile(fhOut, acRec, iRecLen, &nBytesWritten, NULL);
            }
         }
      }
      lCnt++;

      // Get next roll record
      if (!(pTmp = fgets(acRollRec, MAX_RECSIZE, fdLegal)))
         bEof = true;    // Signal to stop
   }

   // Close files
   if (fdLegal)
      fclose(fdLegal);
   if (fdStat)
      fclose(fdStat);
   if (fdValue)
      fclose(fdValue);
   if (fdMail)
      fclose(fdMail);
   if (fdSitus)
      fclose(fdSitus);
   if (fdChar)
      fclose(fdChar);
   if (fdTax)
      fclose(fdTax);
   if (fdEtal)
      fclose(fdEtal);

   if (fhOut)
      CloseHandle(fhOut);
   if (fhIn)
      CloseHandle(fhIn);

   lRecCnt = iNewRec+iRollUpd;
   LogMsg("Total records processed:    %u", lCnt);
   LogMsg("Total output records:       %u", lRecCnt);
   LogMsg("Total new records:          %u", iNewRec);
   LogMsg("Total retired records:      %u\n", iRetiredRec);

   LogMsg("Total Stat matched:         %u", lStatMatch);
   LogMsg("Total Stat skiped:          %u\n", lStatSkip);

   LogMsg("Total Char matched:         %u", lCharMatch);
   LogMsg("Total Char skiped:          %u\n", lCharSkip);

   LogMsg("Total Mail matched:         %u", lMailMatch);
   LogMsg("Total Mail skiped:          %u\n", lMailSkip);

   LogMsg("Total Situs matched:        %u", lSitusMatch);
   LogMsg("Total Situs skiped:         %u\n", lSitusSkip);

   LogMsg("Total Tax matched:          %u", lTaxMatch);
   LogMsg("Total Tax skiped:           %u\n", lTaxSkip);

   LogMsg("Total bad-city records:     %u", iBadCity);
   LogMsg("Total bad-suffix records:   %u\n", iBadSuffix);

   printf("\nTotal output records: %u\n", lRecCnt);

   return 0;
}

/********************************* Scr_Load_LDR *****************************
 *
 *
 ****************************************************************************/

int Scr_Load_LDR(int iFirstRec /* 1=create header rec */)
{
   char     *pTmp, acBuf[MAX_RECSIZE], acRollRec[MAX_RECSIZE];
   char     acOutFile[_MAX_PATH];

   HANDLE   fhOut;

   int      iRet;
   DWORD    nBytesWritten;
   BOOL     bRet;
   long     lRet=0, lCnt=0;

   LogMsg0("Loading LDR using Scr_Load_LDR()");

   // Open roll file
   LogMsg("Open Roll file %s", acRollFile);
   fdRoll = fopen(acRollFile, "r");
   if (fdRoll == NULL)
   {
      printf("Error opening roll file: %s\n", acRollFile);
      return 2;
   }

   // Open Char file
   sprintf(acCharFile, acAttrTmpl, myCounty.acCntyCode, "DAT");
   LogMsg("Open Char file %s", acCharFile);
   fdChar = fopen(acCharFile, "r");
   if (fdChar == NULL)
   {
      LogMsg("***** Error opening Char file: %s\n", acCharFile);
      return 2;
   }

   // Open Sale file
   /*
   sprintf(acSaleFile, acSaleTmpl, myCounty.acCntyCode, "DAT");
   LogMsg("Open sale file %s", acSaleFile);
   fdSale = fopen(acSaleFile, "r");
   if (fdSale == NULL)
   {
      LogMsg("***** Error opening sale file: %s\n", acSaleFile);
      return 2;
   }
   */

   // Open Output file
   sprintf(acOutFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "R01");
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening output file: %s\n", acOutFile);
      return 4;
   }

   // Output first header record
   if (iFirstRec > 0)
   {
      memset(acBuf, '9', iRecLen);
      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
   }

   // Init variables
   iNoMatch=iBadCity=iBadSuffix=0;
   lCharMatch=lCharSkip=lSaleMatch=lSaleSkip=0;

   // Merge loop
   while (!feof(fdRoll))
   {
      // Get roll rec
      pTmp = fgets(acRollRec, MAX_RECSIZE, fdRoll);

      if (!pTmp)
         break;

      // Create new R01 record
      iRet = Scr_MergeRoll(acBuf, acRollRec, CREATE_R01);

      // Merge Char
      if (fdChar)
         iRet = Scr_MergeChar(acBuf);

      // Merge Sale
      //if (fdSale)
      //   iRet = Scr_MergeSale(acBuf);

      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);

      if (!bRet)
      {
         LogMsg("Error writing to output file at record %d\n", lCnt);
         lRet = WRITE_ERR;
         break;
      }
   }

   // Close files
   if (fdRoll)
      fclose(fdRoll);
   if (fdChar)
      fclose(fdChar);
   //if (fdSale)
   //   fclose(fdSale);

   if (fhOut)
      CloseHandle(fhOut);

   LogMsg("Total input records:        %u", lLDRRecCount);
   LogMsg("Total output records:       %u", lCnt);
   LogMsg("Total Char matched:         %u", lCharMatch);
   LogMsg("Total Char skiped:          %u\n", lCharSkip);

   //LogMsg("Total Sale matched:         %u", lSaleMatch);
   //LogMsg("Total Sale skiped:          %u\n", lSaleSkip);

   LogMsg("Total bad-city records:     %u", iBadCity);
   LogMsg("Total bad-suffix records:   %u\n", iBadSuffix);

   printf("\nTotal output records: %u\n", lCnt);

   lRecCnt = lCnt;
   lLDRRecCount = lCnt;

   return 0;
}

/****************************** Scr_Load_LDR2 *********************************
 *
   1.LegalFile=SCC_ParcelQuest_Get_PrimaryOwner_TRA_UseCode_LegalDescr.txt
   2.StatFile=SCC_ParcelQuest_Get_ParcelStatus.txt
   3.ValueFile=SCC_ParcelQuest_Get_AssessmentValues_Exemptions.txt
   4.OwnerFile=SCC_ParcelQuest_Get_PrimaryOwnerMailingAddress.txt
   5.SitusFile=SCC_ParcelQuest_Get_SitusAddresses.txt
   6.CharFile=SCC_ParcelQuest_Get_Characteristics.txt
   7.TaxFile=SCC_ParcelQuest_Get_TaxYearTaxesDue_Paid.txt
   8.DelqFile=SCC_ParcelQuest_Get_DelinquentBills.txt
   9.EtalFile=SCC_ParcelQuest_Get_Etals.txt
   X.SaleFile=SCC_ParcelQuest_Get_SalesTransfers.txt
 *
 * SCR now provide a group of files to update our roll.  We have to merge them
 * to form the roll.  Legal file will be used as based roll file then add other 
 * data to form a roll record.  Sale file is extracted for historical update.
 *
 ******************************************************************************/

int Scr_Load_LDR2(int iSkip)
{
   char     *pTmp, acRec[MAX_RECSIZE], acRollRec[MAX_RECSIZE];
   char     acOutFile[_MAX_PATH], acTmp[256];

   HANDLE   fhOut;

   int      iRet, iTmp;
   DWORD    nBytesWritten;
   BOOL     bRet;
   long     lCnt=0;

   LogMsg0("Loading LDR using Scr_Load_LDR2()");

   GetIniString(myCounty.acCntyCode, "LegalFile", "", acRec, 256, acIniFile);
   // Rebuild legal input file since records may be on multiple row.
   // This function will merge them onto one row so it can be sorted.
   sprintf(acOutFile, "%s\\SCR\\SCR_Legal.txt", acTmpPath);
   iRet = RebuildCsv(acRec, acOutFile, cDelim, SCR_LE_PRIOWNER+1);

   // Sort legal file
   sprintf(acTmp, "%s\\SCR\\SCR_Legal.srt", acTmpPath);
   iTmp = sortFile(acOutFile, acTmp, "S(#1,C,A) DEL(124) F(TXT) ");

   // Open Legal file
   LogMsg("Open Legal file %s", acTmp);
   fdLegal = fopen(acTmp, "r");
   if (fdLegal == NULL)
   {
      LogMsg("***** Error opening Legal file: %s\n", acTmp);
      return -2;
   }

   // Open Status file
   GetIniString(myCounty.acCntyCode, "StatFile", "", acRec, 256, acIniFile);
   sprintf(acTmp, "%s\\SCR\\SCR_Status.txt", acTmpPath);
   iTmp = sortFile(acRec, acTmp, "S(#1,C,A) DEL(124) F(TXT) ");
   LogMsg("Open Status file %s", acTmp);
   fdStat = fopen(acTmp, "r");
   if (fdStat == NULL)
   {
      LogMsg("***** Error opening Status file: %s\n", acTmp);
      return -2;
   }

   // Open Value file
   GetIniString(myCounty.acCntyCode, "ValueFile", "", acRec, 256, acIniFile);
   sprintf(acTmp, "%s\\SCR\\SCR_Values.txt", acTmpPath);
   iTmp = sortFile(acRec, acTmp, "S(#1,C,A) DEL(124) F(TXT) ");
   LogMsg("Open Value file %s", acTmp);
   fdValue = fopen(acTmp, "r");
   if (fdValue == NULL)
   {
      LogMsg("***** Error opening Value file: %s\n", acTmp);
      return -2;
   }

   // Open Owner mail file
   GetIniString(myCounty.acCntyCode, "MailFile", "", acRec, 256, acIniFile);
   sprintf(acTmp, "%s\\SCR\\SCR_Mailing.txt", acTmpPath);
   iTmp = sortFile(acRec, acTmp, "S(#1,C,A) DEL(124) F(TXT) ");
   LogMsg("Open Mail file %s", acTmp);
   fdMail = fopen(acTmp, "r");
   if (fdMail == NULL)
   {
      LogMsg("***** Error opening Mail file: %s\n", acTmp);
      return -2;
   }

   // Open Situs file
   GetIniString(myCounty.acCntyCode, "SitusFile", "", acRec, 256, acIniFile);
   sprintf(acTmp, "%s\\SCR\\SCR_Situs.txt", acTmpPath);
   iTmp = sortFile(acRec, acTmp, "S(#1,C,A) DEL(124) F(TXT) ");
   LogMsg("Open Situs file %s", acTmp);
   fdSitus = fopen(acTmp, "r");
   if (fdSitus == NULL)
   {
      LogMsg("***** Error opening Situs file: %s\n", acTmp);
      return -2;
   }

   // Open Char file
   GetIniString(myCounty.acCntyCode, "CharFile", "", acRec, 256, acIniFile);
   sprintf(acTmp, "%s\\SCR\\SCR_Char.txt", acTmpPath);
   iTmp = sortFile(acRec, acTmp, "S(#1,C,A) DEL(124) F(TXT) ");
   LogMsg("Open Char file %s", acTmp);
   fdChar = fopen(acTmp, "r");
   if (fdChar == NULL)
   {
      LogMsg("***** Error opening Char file: %s\n", acTmp);
      return -2;
   }

   // Open Tax file
   GetIniString(myCounty.acCntyCode, "TaxRoll", "", acRec, 256, acIniFile);
   sprintf(acTmp, "%s\\SCR\\SCR_Tax.txt", acTmpPath);
   iTmp = sortFile(acRec, acTmp, "S(#1,C,A) DEL(124) F(TXT) ");
   LogMsg("Open Tax file %s", acTmp);
   fdTax = fopen(acTmp, "r");
   if (fdTax == NULL)
   {
      LogMsg("***** Error opening Tax file: %s\n", acTmp);
      return -2;
   }
   
   // Open Output file
   sprintf(acOutFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "R01");
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening output file: %s\n", acOutFile);
      return -4;
   }

   // Skip header record
   for (iRet = 0; iRet < iHdrRows; iRet++)
      pTmp = fgets((char *)&acRollRec[0], MAX_RECSIZE, fdLegal);

   // Create test record
   memset(acRec, '9', iRecLen);
   bRet = WriteFile(fhOut, acRec, iRecLen, &nBytesWritten, NULL);

   iNoMatch=iBadCity=iBadSuffix=0;
   lRecCnt=lCharMatch=lCharSkip=lValueSkip=lValueMatch=0;

   // Merge loop
   while (pTmp = fgets(acRollRec, MAX_RECSIZE, fdLegal))
   {
      // Skip unsecured parcel
      if (acRollRec[0] != '6' && acRollRec[0] <= '9')
      {
         iApnLen = strchr(acRollRec, cDelim) - &acRollRec[0];

         // Create new R01 record
         iRet = Scr_MergeLegalN(acRec, acRollRec, CREATE_R01);
         if (!iRet)
         {
#ifdef _DEBUG
            //if (!memcmp(acRec, "02289113", 8))
            //   iTmp = 0;
#endif
            // Merge Situs
            if (fdSitus)
               iRet = Scr_MergeSitusAdr(acRec);

            // Merge Owner mail addr
            if (fdMail)
               iRet = Scr_MergeMailAdr(acRec);

            // Merge values
            if (fdValue)
               Scr_MergeValue(acRec);
      
            // Merge Char
            if (fdChar)
               iRet = Scr_MergeStdChar(acRec);

            // Merge Tax
            if (fdTax)
               iRet = Scr_MergeTax2(acRec);

            // Write output record
            bRet = WriteFile(fhOut, acRec, iRecLen, &nBytesWritten, NULL);
            if (!bRet)
            {
               LogMsg("***** Error occurs: %d\n", GetLastError());
               break;
            }
            lRecCnt++;
         } else if (bDebug)
            LogMsg("*** Ignore: %.50s", acRollRec);
      }

      // Get next roll record
      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdLegal)
      fclose(fdLegal);
   if (fdStat)
      fclose(fdStat);
   if (fdValue)
      fclose(fdValue);
   if (fdMail)
      fclose(fdMail);
   if (fdSitus)
      fclose(fdSitus);
   if (fdChar)
      fclose(fdChar);
   if (fdTax)
      fclose(fdTax);

   if (fhOut)
      CloseHandle(fhOut);

   LogMsg("Total records processed:    %u", lCnt);
   LogMsg("Total output records:       %u\n", lRecCnt);

   LogMsg("Total Value matched:        %u", lValueMatch);
   LogMsg("Total Value skiped:         %u\n", lValueSkip);

   LogMsg("Total Stat matched:         %u", lStatMatch);
   LogMsg("Total Stat skiped:          %u\n", lStatSkip);

   LogMsg("Total Char matched:         %u", lCharMatch);
   LogMsg("Total Char skiped:          %u\n", lCharSkip);

   LogMsg("Total Mail matched:         %u", lMailMatch);
   LogMsg("Total Mail skiped:          %u\n", lMailSkip);

   LogMsg("Total Situs matched:        %u", lSitusMatch);
   LogMsg("Total Situs skiped:         %u\n", lSitusSkip);

   LogMsg("Total Tax matched:          %u", lTaxMatch);
   LogMsg("Total Tax skiped:           %u\n", lTaxSkip);

   LogMsg("Total bad-city records:     %u", iBadCity);
   LogMsg("Total bad-suffix records:   %u\n", iBadSuffix);

   printf("\nTotal output records: %u\n", lCnt);

   lLDRRecCount = lRecCnt;

   return 0;
}

int Scr_UpdateValue(char *pOutbuf)
{
   static   char acRec[1024], *pRec=NULL;
   int      iLoop, iTmp;

   LIENEXTR *pLien = (LIENEXTR *)&acRec[0];

   // Get first Char rec for first call
   if (!pRec && !lValueMatch)
      pRec = fgets(acRec, 1024, fdValue);

   do
   {
      if (!pRec)
      {
         fclose(fdValue);
         fdValue = NULL;
         return 1;      // EOF
      }

      // Compare Apn
      iLoop = memcmp(pOutbuf, pLien->acApn, iApnLen);
      if (iLoop > 0)
      {
         if (bDebug)
            LogMsg0("Skip Lien rec  %.12s", pLien->acApn);
         pRec = fgets(acRec, 1024, fdValue);
         lValueSkip++;
      }
   } while (iLoop > 0);

   // If not match, return
   if (iLoop)
      return 1;

   // Ignore unsecured
   if (pLien->AsmtType == 'U')
      return 2;

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "00102208", 8))
   //   iTmp = 0;
#endif

   memcpy(pOutbuf+OFF_ALT_APN, pLien->acAltApn, iApnLen);
   memcpy(pOutbuf+OFF_LAND, pLien->acLand, SIZ_LAND);
   memcpy(pOutbuf+OFF_IMPR, pLien->acImpr, SIZ_IMPR);
   memcpy(pOutbuf+OFF_OTHER, pLien->acOther, SIZ_OTHER);
   memcpy(pOutbuf+OFF_GROSS, pLien->acGross, SIZ_GROSS);
   memcpy(pOutbuf+OFF_RATIO, pLien->acRatio, SIZ_RATIO);
   memcpy(pOutbuf+OFF_EXE_TOTAL, pLien->acExAmt, SIZ_EXE_TOTAL);
   *(pOutbuf+OFF_FULL_EXEMPT) = pLien->SpclFlag;
   *(pOutbuf+OFF_HO_FL) = pLien->acHO[0];
   memcpy(pOutbuf+OFF_EXE_CD1, pLien->acExCode, SIZ_LIEN_EXECODE);

   // Create exemption type
   makeExeType(pOutbuf+OFF_EXE_TYPE, pOutbuf+OFF_EXE_CD1, pOutbuf+OFF_EXE_CD2, pOutbuf+OFF_EXE_CD3, (IDX_TBL4 *)&SCR_Exemption);

   //// TRA
   //memcpy(pOutbuf+OFF_TRA, pLien->acTRA, SIZ_TRA);

   //// Land
   //long lLand = atoin(pLien->acLand, SIZ_LIEN_FIXT);
   //if (lLand > 0)
   //{
   //   sprintf(acTmp, "%*u", SIZ_LAND, lLand);
   //   memcpy(pOutbuf+OFF_LAND, acTmp, SIZ_LAND);
   //}

   //// Improve
   //long lImpr = atoin(pLien->acImpr, SIZ_LIEN_FIXT);
   //if (lImpr > 0)
   //{
   //   sprintf(acTmp, "%*u", SIZ_IMPR, lImpr);
   //   memcpy(pOutbuf+OFF_IMPR, acTmp, SIZ_IMPR);
   //}

   //// Other value
   //long  lOthValue = atoin(pLien->acOther, SIZ_LIEN_FIXT);
   //if (lOthValue > 0)
   //{
   //   sprintf(acTmp, "%*u", SIZ_OTHER, lOthValue);
   //   memcpy(pOutbuf+OFF_OTHER, acTmp, SIZ_OTHER);
   //}

   //// Gross total
   //ULONG lGross = lLand+lImpr+lOthValue;
   //if (lGross > 0)
   //{
   //   sprintf(acTmp, "%*u", SIZ_GROSS, lGross);
   //   memcpy(pOutbuf+OFF_GROSS, acTmp, SIZ_GROSS);

   //   // Ratio
   //   if (lImpr > 0)
   //   {
   //      sprintf(acTmp, "%*u", SIZ_RATIO, (ULONG)lImpr*100/(lLand+lImpr));
   //      memcpy(pOutbuf+OFF_RATIO, acTmp, SIZ_RATIO);
   //   }
   //}

   //// HO Exempt
   //*(pOutbuf+OFF_HO_FL) = pLien->acHO[0];
   //memcpy(pOutbuf+OFF_EXE_CD1, pLien->acExCode, SIZ_LIEN_EXECODE);

   //lTmp = atoin(pLien->acExAmt, SIZ_LIEN_EXEAMT);
   //if (lTmp > 0)
   //{
   //   sprintf(acTmp, "%*u", SIZ_EXE_TOTAL, lTmp);
   //   memcpy(pOutbuf+OFF_EXE_TOTAL, pLien->acExAmt, SIZ_EXE_TOTAL);
   //   *(pOutbuf+OFF_FULL_EXEMPT) = pLien->SpclFlag;
   //}
   lValueMatch++;

   // Get next lien rec
   pRec = fgets(acRec, 1024, fdValue);
   return 0;
}

/******************************************************************************
 *
 * SCC_ParcelQuest_Get_AssessmentValues_Exemptions.txt
 *
 ******************************************************************************/

int Scr_Load_LDR3(int iSkip)
{
   char     *pTmp, acRec[MAX_RECSIZE], acRollRec[MAX_RECSIZE];
   char     acOutFile[_MAX_PATH], acTmp[256];

   HANDLE   fhOut;

   int      iRet, iTmp;
   DWORD    nBytesWritten;
   BOOL     bRet;
   long     lCnt=0;

   LogMsg0("Loading LDR using Scr_Load_LDR3()");

   GetIniString(myCounty.acCntyCode, "LegalFile", "", acRec, 256, acIniFile);
   // Rebuild legal input file since records may be on multiple row.
   // This function will merge them onto one row so it can be sorted.
   sprintf(acOutFile, "%s\\SCR\\SCR_Legal.txt", acTmpPath);
   iRet = RebuildCsv(acRec, acOutFile, cDelim, SCR_LE_PRIOWNER+1);

   sprintf(acTmp, "%s\\SCR\\SCR_Legal.srt", acTmpPath);
   iTmp = sortFile(acOutFile, acTmp, "S(#1,C,A,#2,C,A)  F(TXT) DEL(124) OMIT(#2,C,GT,\"U\",OR,#1,C,GT,\"A\")");

   // Open Legal file
   LogMsg("Open Legal file %s", acTmp);
   fdLegal = fopen(acTmp, "r");
   if (fdLegal == NULL)
   {
      LogMsg("***** Error opening Legal file: %s\n", acTmp);
      return -2;
   }

   // Open Status file
   GetIniString(myCounty.acCntyCode, "StatFile", "", acRec, 256, acIniFile);
   sprintf(acTmp, "%s\\SCR\\SCR_Status.txt", acTmpPath);
   iTmp = sortFile(acRec, acTmp, "S(#1,C,A) DEL(124) F(TXT) OMIT(#1,C,GT,\"A\") ");
   LogMsg("Open Status file %s", acTmp);
   fdStat = fopen(acTmp, "r");
   if (fdStat == NULL)
   {
      LogMsg("***** Error opening Status file: %s\n", acTmp);
      return -2;
   }

   // Open Value file
   sprintf(acTmp, acLienTmpl, myCounty.acCntyCode, myCounty.acCntyCode);
   LogMsg("Open Value file %s", acTmp);
   fdValue = fopen(acTmp, "r");
   if (fdValue == NULL)
   {
      LogMsg("***** Error opening Value file: %s\n", acTmp);
      return -2;
   }

   // Open Owner mail file
   GetIniString(myCounty.acCntyCode, "MailFile", "", acRec, 256, acIniFile);
   sprintf(acTmp, "%s\\SCR\\SCR_Mailing.txt", acTmpPath);
   iTmp = sortFile(acRec, acTmp, "S(#1,C,A) DEL(124) F(TXT) OMIT(#1,C,GT,\"A\") ");
   LogMsg("Open Mail file %s", acTmp);
   fdMail = fopen(acTmp, "r");
   if (fdMail == NULL)
   {
      LogMsg("***** Error opening Mail file: %s\n", acTmp);
      return -2;
   }

   // Open Situs file
   GetIniString(myCounty.acCntyCode, "SitusFile", "", acRec, 256, acIniFile);
   sprintf(acTmp, "%s\\SCR\\SCR_Situs.txt", acTmpPath);
   iTmp = sortFile(acRec, acTmp, "S(#1,C,A) DEL(124) F(TXT) OMIT(#1,C,GT,\"A\")");
   LogMsg("Open Situs file %s", acTmp);
   fdSitus = fopen(acTmp, "r");
   if (fdSitus == NULL)
   {
      LogMsg("***** Error opening Situs file: %s\n", acTmp);
      return -2;
   }

   // Open Char file
   LogMsg("Open Char file %s", acCChrFile);
   fdChar = fopen(acCChrFile, "r");
   if (fdChar == NULL)
   {
      LogMsg("***** Error opening Char file: %s\n", acCChrFile);
      return -2;
   }
  
   // Open Output file
   sprintf(acOutFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "R01");
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening output file: %s\n", acOutFile);
      return -4;
   }

   // Create test record
   memset(acRec, '9', iRecLen);
   bRet = WriteFile(fhOut, acRec, iRecLen, &nBytesWritten, NULL);

   iNoMatch=iBadCity=iBadSuffix=0;
   lRecCnt=lCharMatch=lCharSkip=lValueSkip=lValueMatch=0;

   // Merge loop
   while (pTmp = fgets(acRollRec, MAX_RECSIZE, fdLegal))
   {
      iApnLen = strchr(acRollRec, cDelim) - &acRollRec[0];

      // Create new R01 record
      iRet = Scr_MergeLegalN(acRec, acRollRec, CREATE_R01);
      if (!iRet)
      {
#ifdef _DEBUG
         //if (!memcmp(acRec, "02289113", 8))
         //   iTmp = 0;
#endif
         // Merge values
         if (fdValue)
            iRet = Scr_UpdateValue(acRec);
      
         // Only process secured parcel
         if (iRet != 2)
         {
            // Merge Situs
            if (fdSitus)
               iRet = Scr_MergeSitusAdr(acRec);

            // Merge Owner mail addr
            if (fdMail)
               iRet = Scr_MergeMailAdr(acRec);

            // Merge Char
            if (fdChar)
               iRet = Scr_MergeStdChar(acRec);

            // Write output record
            bRet = WriteFile(fhOut, acRec, iRecLen, &nBytesWritten, NULL);
            if (!bRet)
            {
               LogMsg("***** Error occurs: %d\n", GetLastError());
               break;
            }
            lRecCnt++;
         }
      } else if (bDebug)
         LogMsg("*** Ignore: %.50s", acRollRec);

      // Get next roll record
      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdLegal)
      fclose(fdLegal);
   if (fdStat)
      fclose(fdStat);
   if (fdValue)
      fclose(fdValue);
   if (fdMail)
      fclose(fdMail);
   if (fdSitus)
      fclose(fdSitus);
   if (fdChar)
      fclose(fdChar);
   if (fdTax)
      fclose(fdTax);

   if (fhOut)
      CloseHandle(fhOut);

   LogMsg("Total records processed:    %u", lCnt);
   LogMsg("Total output records:       %u\n", lRecCnt);

   LogMsg("Total Value matched:        %u", lValueMatch);
   LogMsg("Total Value skiped:         %u\n", lValueSkip);

   LogMsg("Total Stat matched:         %u", lStatMatch);
   LogMsg("Total Stat skiped:          %u\n", lStatSkip);

   LogMsg("Total Char matched:         %u", lCharMatch);
   LogMsg("Total Char skiped:          %u\n", lCharSkip);

   LogMsg("Total Mail matched:         %u", lMailMatch);
   LogMsg("Total Mail skiped:          %u\n", lMailSkip);

   LogMsg("Total Situs matched:        %u", lSitusMatch);
   LogMsg("Total Situs skiped:         %u\n", lSitusSkip);

   LogMsg("Total bad-city records:     %u", iBadCity);
   LogMsg("Total bad-suffix records:   %u\n", iBadSuffix);

   printf("\nTotal output records: %u\n", lCnt);

   lLDRRecCount = lRecCnt;

   return 0;
}

/******************************* Scr_ExtrAttr ********************************
 *
 * Extract characteristics from CD_DataMonthlySantaCruz.mdb
 * Return 0 if successful, >0 if error
 *
 *****************************************************************************/

int Scr_ExtrAttr(void)
{
   ATTR_REC AttrRec;
   char     acOutFile[_MAX_PATH], acAttrFile[_MAX_PATH];
   char     acTmp[256], *pTmp;

   hlAdo    hAttr;
   hlAdoRs  rsAttr;

   FILE     *fdOut;

   int      iTmp;
   long     lCnt=0;
   CString  strFld;

   LogMsg("Creating Attr export file for %s", myCounty.acCntyCode);
   printf("Creating Attr export file for %s\n", myCounty.acCntyCode);

   // Get db provider
   GetIniString("Database", "MdbProvider", "", acAttrFile, 256, acIniFile);

   // Open attr file
   GetIniString(myCounty.acCntyCode, "AttrFile", "", acTmp, _MAX_PATH, acIniFile);
   // If input is zip file, unzip it
   if (strstr(acTmp, ".zip"))
   {
      // unzip file
      LogMsg("Unzip char file %s", acTmp);

      // Initialize Zip server
      doZipInit();

      // Set unzip folder
      strcpy(acOutFile, acTmp);
      pTmp = strrchr(acOutFile, '\\');
      if (pTmp)
      {
         *pTmp = 0;
         setUnzipToFolder(acOutFile);
         setReplaceIfExist(true);

         iTmp = startUnzip(acTmp);
         if (iTmp)
         {
            LogMsg("***** Error unzipping %s.  Program terminated", acTmp);
            return -1;
         }

         pTmp = strrchr(acTmp, '.');
         strcpy(pTmp, ".mdb");
      }

      // Unload Zip DLL
      doZipShutdown();
   }

   if (_access(acTmp, 0))
   {
      LogMsg("Input sale file missing %s", acTmp);
      return -1;
   }
   strcat(acAttrFile, acTmp);

   LogMsg("Open Attr table LDS_PRCH.");
   sprintf(acTmp, "SELECT * FROM LDS_PRCH ORDER BY PRCHAPNO");
   try
   {
      bool bRet = hAttr.Connect(acAttrFile);
      if (bRet)
      {
         LogMsg("%s", acTmp);
         rsAttr.Open(hAttr, acTmp);
         rsAttr.next();             // Set to begining of data set
      }
   }
   AdoCatch(e)
   {
      LogMsg("***** Error open %s db: %s", acAttrFile, ComError(e));
      return -2;
   }

   // Open Output file
   sprintf(acOutFile, acAttrTmpl, myCounty.acCntyCode, "DAT");
   LogMsg("Open output file %s", acOutFile);
   fdOut = fopen(acOutFile, "w");
   if (fdOut == NULL)
   {
      printf("Error creating CHAR output file: %s\n", acOutFile);
      return -2;
   }

   // Loop through record set
   while (rsAttr.next())
   {
      memset((void *)&AttrRec, ' ', sizeof(ATTR_REC));

      strFld = rsAttr.GetItem("PRCHAPNO");
      memcpy(AttrRec.Apn, strFld, strFld.GetLength());

#ifdef _DEBUG
      //if (!memcmp(AttrRec.Apn, "0346510468", 10))
      //   iTmp = 0;
#endif

      strFld = rsAttr.GetItem("PRCHQUAL");
      memcpy(AttrRec.BldgClass, strFld, strFld.GetLength());
      strFld = rsAttr.GetItem("PRCHIMPS");
      memcpy(AttrRec.Imps, strFld, strFld.GetLength());
      strFld = rsAttr.GetItem("PRCHBDRM");
      memcpy(AttrRec.Beds, strFld, strFld.GetLength());
      strFld = rsAttr.GetItem("PRCHBARM");
      if (atoi(strFld) > 0)
         memcpy(AttrRec.Bath, strFld, strFld.GetLength());
      else
         AttrRec.Bath[0] = '0';

      strFld = rsAttr.GetItem("PRCHHBRM");
      if (isdigit(strFld.GetAt(0)) )
         memcpy(AttrRec.HBath, strFld, strFld.GetLength());

      strFld = rsAttr.GetItem("PRCHYRBT");
      if (atoi(strFld) > 1700)
         memcpy(AttrRec.YrBlt, strFld, strFld.GetLength());

      strFld = rsAttr.GetItem("PRCHROOF");
      memcpy(AttrRec.RoofType, strFld, strFld.GetLength());
      strFld = rsAttr.GetItem("PRCHGARG");
      memcpy(AttrRec.GarSize, strFld, strFld.GetLength());
      strFld = rsAttr.GetItem("PRCHPOOL");
      memcpy(AttrRec.Pool, strFld, strFld.GetLength());
      strFld = rsAttr.GetItem("PRCHOBLD");
      memcpy(AttrRec.OutBldg, strFld, strFld.GetLength());
      strFld = rsAttr.GetItem("PRCHVIEW");
      memcpy(AttrRec.ViewFlag, strFld, strFld.GetLength());
      strFld = rsAttr.GetItem("PRCHCONST");
      memcpy(AttrRec.Const, strFld, strFld.GetLength());
      strFld = rsAttr.GetItem("PRCHCARP");
      memcpy(AttrRec.CarPortSF, strFld, strFld.GetLength());
      strFld = rsAttr.GetItem("PRCHEFYR");
      iTmp = atoi(strFld);
      if (iTmp > 0)
         memcpy(AttrRec.YrEff, strFld, strFld.GetLength());
      strFld = rsAttr.GetItem("PRCHSPA");
      memcpy(AttrRec.Spa, strFld, strFld.GetLength());
      strFld = rsAttr.GetItem("PRCHROOM");
      memcpy(AttrRec.Rooms, strFld, strFld.GetLength());
      strFld = rsAttr.GetItem("PRCHUNIT");
      memcpy(AttrRec.Units, strFld, strFld.GetLength());
      strFld = rsAttr.GetItem("PRCHFIRE");
      memcpy(AttrRec.FirePlace, strFld, strFld.GetLength());
      strFld = rsAttr.GetItem("PRCHHEAT");
      memcpy(AttrRec.Heat, strFld, strFld.GetLength());
      strFld = rsAttr.GetItem("PRCHEMSZ");
      memcpy(AttrRec.LotSize, strFld, strFld.GetLength());
      strFld = rsAttr.GetItem("PRCHEMAC");
      memcpy(AttrRec.LotAcre, strFld, strFld.GetLength());
      strFld = rsAttr.GetItem("PRCHGARFL");
      memcpy(AttrRec.Garage, strFld, strFld.GetLength());
      strFld = rsAttr.GetItem("PRCHCRPFL");
      memcpy(AttrRec.CarPort, strFld, strFld.GetLength());
      strFld = rsAttr.GetItem("PRCHSZFL");
      memcpy(AttrRec.SizeFlag, strFld, strFld.GetLength());

      AttrRec.CRLF[0] = 10;
      AttrRec.CRLF[1] = 0;
      fputs((char *)&AttrRec,fdOut);

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   rsAttr.Close();
   fclose(fdOut);

   // Sort output file
   LogMsg("Total processed records: %u\n", lCnt);
   return 0;
}

/******************************* Scr_ExtrChar ********************************
 *
 * Extract characteristics from SCC_ParcelQuest_Get_Characteristics.txt
 * Return 0 if successful, >0 if error
 *
 * to be completed ....
 *
 *****************************************************************************/

int Scr_ExtrChar(char *pCharFile)
{
   STDCHAR  AttrRec;
   char     acRec[1024], acTmp[256], acCode[16], *pRec;
   int      iTmp, iRet;
   long     lTmp, lCnt=0;
   FILE     *fdOut;

   LogMsg0("Extracting CHAR data");

   // Open Char file
   LogMsg("Open Char file %s", pCharFile);
   fdChar = fopen(pCharFile, "r");
   if (fdChar == NULL)
   {
      LogMsg("***** Error opening Char file: %s\n", pCharFile);
      return -2;
   }

   // Open Output file
   //sprintf(acOutFile, acAttrTmpl, myCounty.acCntyCode, "DAT");
   LogMsg("Open output file %s", acCChrFile);
   fdOut = fopen(acCChrFile, "w");
   if (fdOut == NULL)
   {
      printf("Error creating CHAR output file: %s\n", acCChrFile);
      return -2;
   }

   // Skip header record
   //for (iTmp = 0; iTmp < iHdrRows; iTmp++)
   //   pRec = fgets(acRec, 1024, fdChar);
   do {
      pRec = fgets(acRec, 1024, fdChar);
   } while (!isdigit(acRec[0]) && _memicmp(acRec, "Strap", 5));

   while (!feof(fdChar))
   {
      // Get next Value rec
      if (!(pRec = fgets(acRec, 1024, fdChar)))
         break;

      // Parse input record
      iTmp = ParseStringIQ(pRec, cDelim, SCR_C_GARSQFT+1, apTokens);
      if (iTmp < SCR_C_GARSQFT+1)
      {
         LogMsg("***** Error: bad CHAR record for APN=%.*s (#tokens=%d)", iApnLen, pRec, iTmp);
         iRet = -1;
         break;
      }
                           
      // Reset buffer
      memset((void *)&AttrRec, ' ', sizeof(STDCHAR));

      // APN
      memcpy(AttrRec.Apn, apTokens[SCR_C_APN], strlen(apTokens[SCR_C_APN]));

      // YrBlt, YrEff  OFF_YR_BLT
      lTmp = atol(apTokens[SCR_C_YRBLT]);
      if (lTmp > 0)
         memcpy(AttrRec.YrBlt, apTokens[SCR_C_YRBLT], strlen(apTokens[SCR_C_YRBLT]));
      lTmp = atol(apTokens[SCR_C_EFFYR]);
      if (lTmp > 0)
         memcpy(AttrRec.YrEff, apTokens[SCR_C_EFFYR], strlen(apTokens[SCR_C_EFFYR]));

      // Bldg #
      lTmp = atol(apTokens[SCR_C_BLDGNUM]);
      if (lTmp > 0)
      {
         iTmp = sprintf(acTmp, "%u", lTmp);
         vmemcpy(AttrRec.BldgSeqNo, acTmp, SIZ_CHAR_SIZE2, iTmp);
      }

      // Units
      lTmp = atol(apTokens[SCR_C_UNITS]);
      if (lTmp > 0)
      {
         sprintf(acTmp, "%*u  ", SIZ_UNITS, lTmp);
         memcpy(AttrRec.Units, acTmp, SIZ_UNITS);
      }

      // View
      switch (*apTokens[SCR_C_VIEW])
      {
         case 'C':   // City view
            AttrRec.View[0] = 'S';
            break;
         
         case 'M':   // Mountain view
            AttrRec.View[0] = 'T';
            break;

         case 'O':   // Ocean view
            AttrRec.View[0] = 'B';
            break;

         case 'V':   // Has view
         case 'Y':
            AttrRec.View[0] = 'Y';
            break;

         case 'N':   // No view
            AttrRec.View[0] = 'N';
            break;
      }

      // Improve SF - OFF_BLDG_SF
      lTmp = atol(apTokens[SCR_C_BLDGSQFT]);
      if (lTmp > 0)
      {
         sprintf(acTmp, "%*u  ", SIZ_BLDG_SF, lTmp);
         memcpy(AttrRec.BldgSqft, acTmp, SIZ_BLDG_SF);
      }

      // Rooms
      lTmp = atol(apTokens[SCR_C_ROOMS]);
      if (lTmp > 0)
      {
         sprintf(acTmp, "%*u  ", SIZ_ROOMS, lTmp);
         memcpy(AttrRec.Rooms, acTmp, SIZ_ROOMS);
      }

      // Beds
      lTmp = atol(apTokens[SCR_C_BEDROOMS]);
      if (lTmp > 0)
      {
         sprintf(acTmp, "%*u  ", SIZ_BEDS, lTmp);
         memcpy(AttrRec.Beds, acTmp, SIZ_BEDS);
      }

      // Baths/Half Baths  OFF_BATH_F
      lTmp = atol(apTokens[SCR_C_FULLBATH]);
      if (*apTokens[SCR_C_FULLBATH] > '0' && *apTokens[SCR_C_FULLBATH] <= '9')
         AttrRec.FBaths[1] = *apTokens[SCR_C_FULLBATH];

      if (*apTokens[SCR_C_HALFBATH] > '0')
         AttrRec.HBaths[1] = *apTokens[SCR_C_HALFBATH];

      // Fp, Heat type OFF_FIRE_PL
      if (*apTokens[SCR_C_FIREPLACES] > '0')
         AttrRec.Fireplace[1] = *apTokens[SCR_C_FIREPLACES];

      // Heat - Possible value Baseboard, Central, Central A/C, Floor, Forced, Heatpump, Other, Radiant, Wall, Wall A/C
      if (*apTokens[SCR_C_HEAT] > ' ')
      {
         pRec = findXlatCodeA(apTokens[SCR_C_HEAT], &asHeating[0]);
         if (pRec)
         {
            AttrRec.Heating[0] = *pRec;
            if (strstr(apTokens[SCR_C_HEAT], "A/C"))
            {
               if (*apTokens[SCR_C_HEAT] == 'W')
                  AttrRec.Cooling[0] = 'L';
               else
                  AttrRec.Cooling[0] = 'C';
            }
         } else
            LogMsg("*** Unknown Heat type: %s [%s]", apTokens[SCR_C_HEAT], apTokens[SCR_C_APN]);
      }

      // HVAC -  COMPLETE HVAC,  ELECTRIC, ELECTRIC WALL, FLOOR FURNACE, FORCED AIR UNIT, NONE
      //         PACKAGE UNIT, SPACE HEATERS, SPACE HEATERS, RADIANT, VENTILATION, WALL FURNACE, WARMED AND COOLED AIR,
      if (*apTokens[SCR_C_HVAC] > ' ')
      {
         pRec = findXlatCodeA(apTokens[SCR_C_HVAC], &asCooling[0]);
         if (pRec)
            AttrRec.Heating[0] = *pRec;
         else
            LogMsg("*** Unknown A/C type: %s [%s]", apTokens[SCR_C_HVAC], apTokens[SCR_C_APN]);
      }

      // Zoning
      memcpy(AttrRec.Zoning, apTokens[SCR_C_ZONING], strlen(apTokens[SCR_C_ZONING]));

      // Pool/Spa
      if (*apTokens[SCR_C_POOL] == '1' && *apTokens[SCR_C_SPA] == '1')
      {
         AttrRec.Pool[0] = 'C';
         AttrRec.Spa[0] = 'Y';
      } else if (*apTokens[SCR_C_SPA] == '1')
      {
         AttrRec.Pool[0] = 'S';
         AttrRec.Spa[0] = 'Y';
      } else if (*apTokens[SCR_C_POOL] == '1')
         AttrRec.Pool[0] = 'P';

      // Garage size, Park type
      int iGarSiz= atol(apTokens[SCR_C_GARSQFT]);
      int iCarSiz= atol(apTokens[SCR_C_CARSQFT]);
      if (iGarSiz > 0)
      {
         sprintf(acTmp, "%*u ", SIZ_GAR_SQFT, iGarSiz);
         memcpy(AttrRec.GarSqft, acTmp, SIZ_GAR_SQFT);

         if (iCarSiz > 0)
            AttrRec.ParkType[0] = '2';     // GARAGE/CARPORT
         else
            AttrRec.ParkType[0] = 'Z';     // GARAGE
      } else if (iCarSiz > 0)
      {
         sprintf(acTmp, "%*u ", SIZ_GAR_SQFT, iCarSiz);
         memcpy(AttrRec.GarSqft, acTmp, SIZ_GAR_SQFT);
         AttrRec.ParkType[0] = 'C';        // CARPORT
      }

      // LotSize, LotAcre
      long lLotSqft, lLotAcres;
      double dTmp;

      dTmp = atof(apTokens[SCR_C_LOTACRE])*1000;
      if (dTmp > 0)
      {
         lLotAcres = (long)dTmp;
         sprintf(acTmp, "%*u  ", SIZ_LOT_ACRES, lLotAcres);
         memcpy(AttrRec.LotAcre, acTmp, SIZ_LOT_ACRES);
      } else
         lLotAcres = 0;

      dTmp = atof(apTokens[SCR_C_LOTSQFT]);           // round up
      if (dTmp > 0)
      {
         sprintf(acTmp, "%*u  ", SIZ_LOT_SQFT, (long)(dTmp+0.5));
         memcpy(AttrRec.LotSqft, acTmp, SIZ_LOT_SQFT);
      } else if (lLotAcres > 0 && lLotAcres < 100000)
      {  // Populate Sqft if lot < 100 acres
         lLotSqft = (long)(lLotAcres * SQFT_FACTOR_1000);
         sprintf(acTmp, "%*u  ", SIZ_LOT_SQFT, lLotSqft);
         memcpy(AttrRec.LotSqft, acTmp, SIZ_LOT_SQFT);
      }

   #ifdef _DEBUG
      //if (!memcmp(pOutbuf, "0346510468", 10))
      //   iTmp = 0;
   #endif

      // Roof Type
      if (*apTokens[SCR_C_ROOF] > ' ')
      {
         if (!memcmp(apTokens[SCR_C_ROOF], "CO", 2))           // Comp shingle
            AttrRec.RoofType[0] = 'C';
         else if (!memcmp(apTokens[SCR_C_ROOF], "ME", 2))      // Metal
            AttrRec.RoofType[0] = 'L';
         else if (!memcmp(apTokens[SCR_C_ROOF], "SH", 2))      // Shake/Wood shingle
            AttrRec.RoofType[0] = 'B';
         else if (!memcmp(apTokens[SCR_C_ROOF], "OT", 2))
            AttrRec.RoofType[0] = 'Z';
         else if (!memcmp(apTokens[SCR_C_ROOF], "TI", 2))      // Tile
            AttrRec.RoofType[0] = 'I';
      }

      // Const Type OFF_CONS_TYPE
      if (*apTokens[SCR_C_BLDGCLS] > ' ')
         AttrRec.BldgClass = *apTokens[SCR_C_BLDGCLS];

      // Bldg Quality 
      if (*apTokens[SCR_C_BLDGQUAL] >= '0' && *apTokens[SCR_C_BLDGQUAL] <= '9')
      {
         strcpy(acTmp, apTokens[SCR_C_BLDGQUAL]);
         iTmp = Value2Code(acTmp, acCode, &tblAttr[0]);
         if (iTmp > 0)
            AttrRec.BldgQual = acCode[0];
      } else if (*apTokens[SCR_C_BLDGQUAL] > ' ')
         vmemcpy(AttrRec.BldgType, apTokens[SCR_C_BLDGQUAL], SIZ_CHAR_BLDGTYPE);

      // Condition - Average/Fair/Good/Poor/Excellent
      if (*apTokens[SCR_C_CONDITION] > ' ')
      {
         AttrRec.ImprCond[0] = *apTokens[SCR_C_CONDITION];
      }

      // Stories
      iTmp = atol(apTokens[SCR_C_STORIES]);
      if (iTmp > 0)
      {
         sprintf(acTmp, "%d.0  ", SIZ_LOT_SQFT, iTmp);
         memcpy(AttrRec.Stories, acTmp, SIZ_CHAR_SIZE4);
      }

      // Topography:  LEVEL, VARIED, STEEP, SLOPE
      if (*apTokens[SCR_C_TOPOGRAPHY] > ' ')
      {
         if (*apTokens[SCR_C_TOPOGRAPHY] == 'L')
            AttrRec.Topo[0] = 'L';
         else if (*apTokens[SCR_C_TOPOGRAPHY] == 'V')
            AttrRec.Topo[0] = 'V';
         else if (!memcmp(apTokens[SCR_C_TOPOGRAPHY], "SL", 2))
            AttrRec.Topo[0] = 'S';
         else if (!memcmp(apTokens[SCR_C_TOPOGRAPHY], "ST", 2))
            AttrRec.Topo[0] = 'T';
      }

      // Water - BOTH PUBLIC AND WELL WATER, IWS, NO WATER, PUBLIC WATER, SHARED WELL WATER, SPRING WATER, WELL WATER

      // Sewer - NO SANITATION, PUBLIC SEWER, SEPTIC TANK

      AttrRec.CRLF[0] = 10;
      AttrRec.CRLF[1] = 0;
      fputs((char *)&AttrRec, fdOut);

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   fclose(fdOut);
   LogMsg("Total processed records: %u\n", lCnt);

   return 0;
}

// SCC_ParcelQuest_Get_Characteristics.txt
int Scr_ConvertStdChar(char *pCharFile)
{
   char     acRec[1024], acBuf[1200], acTmp[256], acCode[16], *pRec;
   int      iTmp, iRet;
   long     lTmp, lCnt=0;
   FILE     *fdOut;
   STDCHAR  *pChar = (STDCHAR *)&acBuf[0];

   LogMsg0("Converting CHAR file");

   // Sort Char file
   sprintf(acTmp, "%s\\SCR\\SCR_Chars.txt", acTmpPath);
   sortFile(pCharFile, acTmp, "S(#1,C,A,#2,C,A) F(TXT) DEL(124) OMIT(#1,C,LT,\"0\",OR,#1,C,GT,\"A\") ");

   // Open Char file
   LogMsg("Open Char file %s", acTmp);
   fdChar = fopen(acTmp, "r");
   if (fdChar == NULL)
   {
      LogMsg("***** Error opening Char file: %s\n", acTmp);
      return -2;
   }

   // Open Output file
   LogMsg("Open output file %s", acCChrFile);
   fdOut = fopen(acCChrFile, "w");
   if (fdOut == NULL)
   {
      printf("Error creating CHAR output file: %s\n", acCChrFile);
      return -2;
   }

   // Skip header record
   //for (iTmp = 0; iTmp < iHdrRows; iTmp++)
   //   pRec = fgets(acRec, 1024, fdChar);
   do {
      pRec = fgets(acRec, 1024, fdChar);
   } while (!isdigit(acRec[0]) && _memicmp(acRec, "Strap", 5));

   while (!feof(fdChar))
   {
      // Get next Value rec
      if (!(pRec = fgets(acRec, 1024, fdChar)))
         break;

      // Remove preceeding space
      replStrAll(acRec, "| ", "|");

      // Parse input record
      iTmp = ParseStringIQ(pRec, cDelim, SCR_C_FLDS, apTokens);
      if (iTmp < SCR_C_FLDS)
      {
         LogMsg("***** Error: bad CHAR record for APN=%.*s (#tokens=%d)", iApnLen, pRec, iTmp);
         iRet = -1;
         break;
      }
                           
      // Reset buffer
      memset((void *)&acBuf, ' ', sizeof(STDCHAR));

      // APN
      memcpy(pChar->Apn, apTokens[SCR_C_APN], strlen(apTokens[SCR_C_APN]));

      iRet = formatApn(apTokens[SCR_C_APN], acTmp, &myCounty);
      memcpy(pChar->Apn_D, acTmp, iRet);

      // YrBlt, YrEff  OFF_YR_BLT
      lTmp = atol(apTokens[SCR_C_YRBLT]);
      if (lTmp > 0)
         memcpy(pChar->YrBlt, apTokens[SCR_C_YRBLT], strlen(apTokens[SCR_C_YRBLT]));
      lTmp = atol(apTokens[SCR_C_EFFYR]);
      if (lTmp > 0)
         memcpy(pChar->YrEff, apTokens[SCR_C_EFFYR], strlen(apTokens[SCR_C_EFFYR]));

      // Bldg #
      lTmp = atol(apTokens[SCR_C_BLDGNUM]);
      if (lTmp > 0)
      {
         iTmp = sprintf(acTmp, "%u", lTmp);
         vmemcpy(pChar->BldgSeqNo, acTmp, SIZ_CHAR_SIZE2, iTmp);
      }

      // Units
      lTmp = atol(apTokens[SCR_C_UNITS]);
      if (lTmp > 0)
      {
         iTmp = sprintf(acTmp, "%u ", lTmp);
         memcpy(pChar->Units, acTmp, iTmp);
      }

      // View
      switch (*apTokens[SCR_C_VIEW])
      {
         case 'C':   // City view
            pChar->View[0] = 'S';
            break;
         
         case 'M':   // Mountain view
            pChar->View[0] = 'T';
            break;

         case 'O':   // Ocean view
            pChar->View[0] = 'B';
            break;

         case 'V':   // Has view
         case 'Y':
            pChar->View[0] = 'Y';
            break;

         case 'N':   // No view
            pChar->View[0] = 'N';
            break;
      }

      // Improve SF - OFF_BLDG_SF
      lTmp = atol(apTokens[SCR_C_BLDGSQFT]);
      if (lTmp > 0)
      {
         sprintf(acTmp, "%*u  ", SIZ_BLDG_SF, lTmp);
         memcpy(pChar->BldgSqft, acTmp, SIZ_BLDG_SF);
      }

      // Rooms
      lTmp = atol(apTokens[SCR_C_ROOMS]);
      if (lTmp > 0)
      {
         sprintf(acTmp, "%*u  ", SIZ_ROOMS, lTmp);
         memcpy(pChar->Rooms, acTmp, SIZ_ROOMS);
      }

      // Beds
      lTmp = atol(apTokens[SCR_C_BEDROOMS]);
      if (lTmp > 0)
      {
         sprintf(acTmp, "%*u  ", SIZ_BEDS, lTmp);
         memcpy(pChar->Beds, acTmp, SIZ_BEDS);
      }

      // Baths/Half Baths  OFF_BATH_F
      lTmp = atol(apTokens[SCR_C_FULLBATH]);
      if (*apTokens[SCR_C_FULLBATH] > '0' && *apTokens[SCR_C_FULLBATH] <= '9')
         pChar->FBaths[1] = *apTokens[SCR_C_FULLBATH];

      if (*apTokens[SCR_C_HALFBATH] > '0')
         pChar->HBaths[1] = *apTokens[SCR_C_HALFBATH];

      // Number of FIRE_PL
      if (*apTokens[SCR_C_FIREPLACES] > '0')
         pChar->Fireplace[1] = *apTokens[SCR_C_FIREPLACES];

      // Heat - Possible value Baseboard, Central, Central A/C, Floor, Forced, Heatpump, Other, Radiant, Wall, Wall A/C
      if (*apTokens[SCR_C_HEAT] > ' ')
      {
         pRec = findXlatCodeA(apTokens[SCR_C_HEAT], &asHeating[0]);
         if (pRec)
         {
            pChar->Heating[0] = *pRec;
            if (strstr(apTokens[SCR_C_HEAT], "A/C"))
            {
               if (*apTokens[SCR_C_HEAT] == 'W')
                  pChar->Cooling[0] = 'L';
               else
                  pChar->Cooling[0] = 'C';
            }
         } else
            LogMsg("*** Unknown Heat type: %s [%s]", apTokens[SCR_C_HEAT], apTokens[SCR_C_APN]);
      }

      // HVAC -  COMPLETE HVAC,  ELECTRIC, ELECTRIC WALL, FLOOR FURNACE, FORCED AIR UNIT, NONE
      //         PACKAGE UNIT, SPACE HEATERS, SPACE HEATERS, RADIANT, VENTILATION, WALL FURNACE, WARMED AND COOLED AIR,
      if (*apTokens[SCR_C_HVAC] > ' ')
      {
         pRec = findXlatCodeA(apTokens[SCR_C_HVAC], &asCooling[0]);
         if (pRec)
            pChar->Heating[0] = *pRec;
         else
            LogMsg("*** Unknown A/C type: %s [%s]", apTokens[SCR_C_HVAC], apTokens[SCR_C_APN]);
      }

      // Zoning
      memcpy(pChar->Zoning, apTokens[SCR_C_ZONING], strlen(apTokens[SCR_C_ZONING]));

      // Pool/Spa
      if (*apTokens[SCR_C_POOL] == '1' && *apTokens[SCR_C_SPA] == '1')
      {
         pChar->Pool[0] = 'C';
         pChar->Spa[0] = 'Y';
      } else if (*apTokens[SCR_C_SPA] == '1')
      {
         pChar->Pool[0] = 'S';
         pChar->Spa[0] = 'Y';
      } else if (*apTokens[SCR_C_POOL] == '1')
         pChar->Pool[0] = 'P';

      // Garage size, Park type
      int iGarSiz= atol(apTokens[SCR_C_GARSQFT]);
      int iCarSiz= atol(apTokens[SCR_C_CARSQFT]);
      if (iGarSiz > 0)
      {
         sprintf(acTmp, "%*u ", SIZ_GAR_SQFT, iGarSiz);
         memcpy(pChar->GarSqft, acTmp, SIZ_GAR_SQFT);

         if (iCarSiz > 0)
            pChar->ParkType[0] = '2';     // GARAGE/CARPORT
         else
            pChar->ParkType[0] = 'Z';     // GARAGE
      } else if (iCarSiz > 0)
      {
         sprintf(acTmp, "%*u ", SIZ_GAR_SQFT, iCarSiz);
         memcpy(pChar->GarSqft, acTmp, SIZ_GAR_SQFT);
         pChar->ParkType[0] = 'C';        // CARPORT
      }

      // LotSize, LotAcre
      long lLotSqft, lLotAcres;
      double dTmp;

      dTmp = atof(apTokens[SCR_C_LOTACRE])*1000;
      if (dTmp > 0)
      {
         lLotAcres = (ULONG)dTmp;
         iTmp = sprintf(acTmp, "%u", lLotAcres);
         memcpy(pChar->LotAcre, acTmp, iTmp);
      } else
         lLotAcres = 0;

      dTmp = atof(apTokens[SCR_C_LOTSQFT]);           // round up
      if (dTmp > 0)
      {
         iTmp = sprintf(acTmp, "%u", (ULONG)(dTmp+0.5));
         memcpy(pChar->LotSqft, acTmp, iTmp);
      } else if (lLotAcres > 0 && lLotAcres < 100000)
      {  // Populate Sqft if lot < 100 acres
         lLotSqft = (ULONG)(lLotAcres * SQFT_FACTOR_1000);
         iTmp = sprintf(acTmp, "%u", lLotSqft);
         memcpy(pChar->LotSqft, acTmp, iTmp);
      }

   #ifdef _DEBUG
      //if (!memcmp(pOutbuf, "0346510468", 10))
      //   iTmp = 0;
   #endif

      // Roof Type
      if (*apTokens[SCR_C_ROOF] > ' ')
      {
         if (!memcmp(apTokens[SCR_C_ROOF], "COM", 3))          // Comp shingle
            pChar->RoofMat[0] = 'C';
         else if (!memcmp(apTokens[SCR_C_ROOF], "CON", 3))     // Concrete shingle
            pChar->RoofMat[0] = 'J';
         else if (!memcmp(apTokens[SCR_C_ROOF], "ME", 2))      // Metal
            pChar->RoofMat[0] = 'L';
         else if (!memcmp(apTokens[SCR_C_ROOF], "SH", 2))      // Shake/Wood shingle
            pChar->RoofMat[0] = 'B';
         else if (!memcmp(apTokens[SCR_C_ROOF], "OT", 2))
            pChar->RoofMat[0] = 'Z';
         else if (!memcmp(apTokens[SCR_C_ROOF], "TI", 2))      // Tile
            pChar->RoofMat[0] = 'I';
      }

      // Const Type OFF_CONS_TYPE
      if (*apTokens[SCR_C_BLDGCLS] > ' ')
         pChar->BldgClass = *apTokens[SCR_C_BLDGCLS];

      // Bldg Quality 
      if (*apTokens[SCR_C_BLDGQUAL] >= '0' && *apTokens[SCR_C_BLDGQUAL] <= '9')
      {
         strcpy(acTmp, apTokens[SCR_C_BLDGQUAL]);
         iTmp = Value2Code(acTmp, acCode, &tblAttr[0]);
         if (iTmp > 0)
            pChar->BldgQual = acCode[0];
      } else if (*apTokens[SCR_C_BLDGQUAL] > ' ')
         vmemcpy(pChar->BldgType, apTokens[SCR_C_BLDGQUAL], SIZ_CHAR_BLDGTYPE);

      // Condition - Average/Fair/Good/Poor/Excellent
      if (*apTokens[SCR_C_CONDITION] > ' ')
      {
         pChar->ImprCond[0] = *apTokens[SCR_C_CONDITION];
      }

      // Stories
      iTmp = atol(apTokens[SCR_C_STORIES]);
      if (iTmp > 0)
      {
         sprintf(acTmp, "%d.0  ", SIZ_LOT_SQFT, iTmp);
         memcpy(pChar->Stories, acTmp, SIZ_CHAR_SIZE4);
      }

      // Topography:  LEVEL, VARIED, STEEP, SLOPE
      if (*apTokens[SCR_C_TOPOGRAPHY] > ' ')
      {
         if (*apTokens[SCR_C_TOPOGRAPHY] == 'L')
            pChar->Topo[0] = 'L';
         else if (*apTokens[SCR_C_TOPOGRAPHY] == 'V')
            pChar->Topo[0] = 'V';
         else if (!memcmp(apTokens[SCR_C_TOPOGRAPHY], "SL", 2))
            pChar->Topo[0] = 'S';
         else if (!memcmp(apTokens[SCR_C_TOPOGRAPHY], "ST", 2))
            pChar->Topo[0] = 'T';
      }

      // Water - BOTH PUBLIC AND WELL WATER, IWS, NO WATER, PUBLIC WATER, SHARED WELL WATER, SPRING WATER, WELL WATER
      if (!memcmp(apTokens[SCR_C_WATER], "PU", 2) || !memcmp(apTokens[SCR_C_WATER], "BO", 2))
         pChar->Water = 'P';
      else if (*apTokens[SCR_C_WATER] == 'W' || !memcmp(apTokens[SCR_C_WATER], "SH", 2))
         pChar->Water = 'W';
      else if (*apTokens[SCR_C_WATER] == 'N')
         pChar->Water = 'N';
      else if (*apTokens[SCR_C_WATER] == 'S')
         pChar->Water = 'S';
      else if (*apTokens[SCR_C_WATER] > ' ')   
         pChar->Water = 'Y';

      // Sewer - NO SANITATION, PUBLIC SEWER, SEPTIC TANK
      if (*apTokens[SCR_C_SANITATION] == 'P')
         pChar->Sewer = 'P';
      else if (*apTokens[SCR_C_SANITATION] == 'S')
         pChar->Sewer = 'S';
      else if (*apTokens[SCR_C_SANITATION] == 'N')
         pChar->Sewer = 'X';

      lTmp = atol(apTokens[SCR_C_DECK]);
      if (lTmp > 0)
      {
         iTmp = sprintf(acTmp, "%u", lTmp);
         memcpy(pChar->DeckSqft, acTmp, iTmp);
      }

      pChar->CRLF[0] = 10;
      pChar->CRLF[1] = 0;
      fputs(acBuf, fdOut);

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   fclose(fdOut);
   fclose(fdChar);

   LogMsg("Total processed records: %u\n", lCnt);

   return 0;
}

/****************************** Scr_ExtrSale *********************************
 *
 * Return 0 if successful, >0 if error
 *
 *****************************************************************************/

int Scr_ExtrSale(char *pSaleFile)
{
   char     acOutFile[_MAX_PATH], acTmpFile[_MAX_PATH];
   char     acTmp1[256], acTmp[256], *pTmp;

   FILE     *fdOut, *fdIn;
   SCSAL_REC SaleRec;

   int      iTmp;
   long     lCnt=0, lTmp;
   CString  strFld;

   LogMsg("Creating Sale export file for %s", myCounty.acCntyCode);

   LogMsg("Open Sale input file %s", pSaleFile);
   fdIn = fopen(pSaleFile, "r");
   if (fdIn == NULL)
   {
      LogMsg("***** Error opening sale input file: %s\n", pSaleFile);
      return -2;
   }

   // Open Output file
   sprintf(acTmpFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "TMP");
   LogMsg("Open output file %s", acTmpFile);
   fdOut = fopen(acTmpFile, "w");
   if (fdOut == NULL)
   {
      LogMsg("***** Error creating GrGr output file: %s\n", acTmpFile);
      return -2;
   }

   // Loop through record set
   while (!feof(fdIn))
   {
      pTmp = fgets(acTmp, 256, fdIn);
      if (!pTmp)
         break;

      // Skip test APN
      lTmp = atoin(pTmp, 8);
      if (lTmp < 1000 || *pTmp == ' ')
         continue;

      // Skip if no docnum
      lTmp = atoin(pTmp+SOFF_SCR_DOCNUM+5, SSIZ_SCR_DOCNUM-5);
      if (lTmp <= 0)
         continue;

      // Reset output record
      memset((void *)&SaleRec, ' ', sizeof(SCSAL_REC));

      memcpy(SaleRec.Apn, pTmp, SSIZ_SCR_APN);

      // Drop yyyy in Doc#
      if (*(pTmp+SOFF_SCR_DOCNUM+4) == '-')
      {
         memcpy(SaleRec.DocNum, pTmp+SOFF_SCR_DOCNUM+5, SSIZ_SCR_DOCNUM-5);
      } else
      {
         iTmp = 0;
         while (iTmp < SSIZ_SCR_DOCNUM)
         {
            if (*(pTmp+SOFF_SCR_DOCNUM+iTmp) > '0')
               break;
            iTmp++;
         }
         if (iTmp < SSIZ_SCR_DOCNUM)
            memcpy(SaleRec.DocNum, pTmp+SOFF_SCR_DOCNUM+iTmp, SSIZ_SCR_DOCNUM-iTmp);
      }


      lTmp = atoin(pTmp+SOFF_SCR_DOCDATE, SSIZ_SCR_DOCDATE);
      if (lTmp > 0)
         memcpy(SaleRec.DocDate, pTmp+SOFF_SCR_DOCDATE, SSIZ_SCR_DOCDATE);

      // Sale type may need translation
      iTmp = atoin(pTmp+SOFF_SCR_DEED_TYPE, SSIZ_SCR_DEED_TYPE);
      switch (iTmp)
      {
         case 1: // GRANT DEED
            memcpy(SaleRec.DocType, "01", 2);
            break;

         case 2:  // DEED
         case 3:
         case 5:
         case 6:
         case 7:
         case 8:
         case 10:
         case 11:
         case 13:
         case 14:
            memcpy(SaleRec.DocType, "13", 2);
            break;
         
         case 9:  // TRUSTEES DEED
            memcpy(SaleRec.DocType, "27", 2);
            break;
         
         default: // OTHERS
            memcpy(SaleRec.DocType, "19", 2);
            break;
      }
      memcpy(SaleRec.DocCode, pTmp+SOFF_SCR_DEED_TYPE, SSIZ_SCR_DEED_TYPE);

      lTmp = atoin(pTmp+SOFF_SCR_PRICE, SSIZ_SCR_PRICE);
      if (lTmp > 1000)
      {
         sprintf(acTmp, "%*u", SALE_SIZ_SALEPRICE, lTmp);
         memcpy(SaleRec.SalePrice, acTmp, SALE_SIZ_SALEPRICE);
      }

      // Seller
      if (*(pTmp+SOFF_SCR_PREV_OWNER) > '0')
         memcpy(SaleRec.Seller1, pTmp+SOFF_SCR_PREV_OWNER, SSIZ_SCR_PREV_OWNER);

      // Number of parcels transfer
      iTmp = atoin(pTmp+SOFF_SCR_NOPRCL_XFER, SSIZ_SCR_NOPRCL_XFER);
      if (iTmp > 1)
      {
         if (iTmp > 99)
            iTmp = 99;
         iTmp = sprintf(acTmp1, "%d", iTmp);
         memcpy(SaleRec.NumOfPrclXfer, acTmp1, iTmp);
         SaleRec.MultiSale_Flg = 'Y';
      }

      // Change of ownership flag
      //if (*(pTmp+SOFF_SCR_COO_FLAG) > '0')
      //   SaleRec.COO_Flag = *(pTmp+SOFF_SCR_COO_FLAG);

      SaleRec.CRLF[0] = 10;
      SaleRec.CRLF[1] = 0;
      fputs((char *)&SaleRec,fdOut);

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   fclose(fdIn);
   fclose(fdOut);

   // Sort output file
   LogMsg("Total sale records output: %u\n", lCnt);

   // Sort output file and dedup
   sprintf(acOutFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "Sls");

   // Sort on APN asc, DocDate asc, DocNum asc
   sprintf(acTmp, "S(1,14,C,A,27,8,C,A,15,12,C,A,57,10,C,D) OMIT(15,5,C,EQ,\"     \") F(TXT) DUPO(1,34) ");
   lCnt = sortFile(acTmpFile, acOutFile, acTmp);

   // Update cumulated sale file
   //if (lCnt > 0)
   //{
   //   sprintf(acTmpFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "SLS");
   //   LogMsg("Append %s to %s.", acOutFile, acTmpFile);
   //   iTmp = appendTxtFile(acOutFile, acTmpFile);
   //} else
   //   iTmp = -1;

   LogMsg("Create Sale export completed.");
   return iTmp;
}

/****************************** Scr_ExtrSale2 ********************************
 *
 * Extract sales from new CSV format
 *
 * Return 0 if successful, >0 if error
 *
 *****************************************************************************/

int Scr_ExtrSale2(char *pSaleFile)
{
   char     acTmpFile[_MAX_PATH], acTmp[256], acRec[1024], *pTmp;

   FILE      *fdOut;
   SCSAL_REC SaleRec, LastRec;

   int      iTmp, iGtor, iGtee;
   long     lCnt=0, lTmp;

   LogMsg0("Loading sale file using Scr_ExtrSale2()");

   // Open Sales file
   LogMsg("Open Sales file %s", pSaleFile);
   fdSale = fopen(pSaleFile, "r");
   if (fdSale == NULL)
   {
      LogMsg("***** Error opening Sales file: %s\n", pSaleFile);
      return -1;
   }

   // Open Output file
   sprintf(acTmpFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "TMP");

   LogMsg("Open output file %s", acTmpFile);
   fdOut = fopen(acTmpFile, "w");
   if (fdOut == NULL)
   {
      LogMsg("***** Error creating sale output file: %s\n", acTmpFile);
      return -2;
   }

   LastRec.Apn[0] = 0;

   // Skip header
   do {
      pTmp = fgets(acRec, 1024, fdSale);
   } while (!isdigit(*pTmp));

   // Loop through record set
   while (!feof(fdSale))
   {  
      if (!(pTmp = fgets(acRec, 1024, fdSale)))
      {
         iTmp = sprintf(acTmp, "%d", iGtee);
         memcpy(LastRec.NameCnt, acTmp, iTmp);
         LastRec.CRLF[0] = 10;
         LastRec.CRLF[1] = 0;
         fputs((char *)&LastRec,fdOut);            
         break;
      }

      if (!isdigit(*pTmp))
         continue;

      // Parse input rec
      iTokens = ParseStringIQ(acRec, cDelim, SCR_S_REAPPRAISE+1, apTokens);
      if (iTokens <= SCR_S_REAPPRAISE)
      {
         LogMsg0("*** Error: bad sale record %.16s (#tokens=%d)", acRec, iTokens);
         continue;
      }

      // Skip if no docnum
      if (*apTokens[SCR_S_DOCNUMBER] > '9')
         continue;
      lTmp = atoin(apTokens[SCR_S_DOCNUMBER]+5, 7);
      if (!lTmp)
         continue;

#ifdef _DEBUG
      // test DocType
      //if (!memcmp(apTokens[SCR_S_PIN], "09113106", 8))
      //   iTmp = 0;
#endif

      if (_memicmp(LastRec.Apn, apTokens[SCR_S_PIN], strlen(apTokens[SCR_S_PIN])))
      {
         // Save last record
         if (LastRec.Apn[0] > ' ')
         {
            iTmp = sprintf(acTmp, "%d", iGtee);
            memcpy(LastRec.NameCnt, acTmp, iTmp);
            LastRec.CRLF[0] = 10;
            LastRec.CRLF[1] = 0;
            fputs((char *)&LastRec,fdOut);            
         }

         // Reset output record
         memset((void *)&SaleRec, ' ', sizeof(SCSAL_REC));
         iGtor = 0;
         iGtee = 0;

         // APN
         memcpy(SaleRec.Apn, apTokens[SCR_S_PIN], strlen(apTokens[SCR_S_PIN]));

         // Doc date
         pTmp = dateConversion(apTokens[SCR_S_DOCDATE], acTmp, MM_DD_YYYY_1);
         if (pTmp)
            memcpy(SaleRec.DocDate, acTmp, 8);

         // Docnum
         pTmp = apTokens[SCR_S_DOCNUMBER];
         if (*(pTmp+4) == '-')
            memcpy(SaleRec.DocNum, pTmp+5, strlen(pTmp+5));
         else if (!memcmp(pTmp, acTmp, 4))
            memcpy(SaleRec.DocNum, pTmp+4, strlen(pTmp+4));            

         // Sale price
         lTmp = atol(apTokens[SCR_S_CONSIDERATION]);
         if (lTmp > 0)
         {
            iTmp = sprintf(acTmp, "%*u", SIZ_SALE1_AMT, lTmp);
            memcpy(SaleRec.SalePrice, acTmp, iTmp);
         }

         // Doc type 
         iTmp = findDocType(apTokens[SCR_S_DOCTYPE], (IDX_TBL5 *)&SCR_DocTbl[0]);
         if (iTmp >= 0)
         {
            memcpy(SaleRec.DocType, SCR_DocTbl[iTmp].pCode, SCR_DocTbl[iTmp].iCodeLen);
            SaleRec.NoneSale_Flg = SCR_DocTbl[iTmp].flag;
         } else
            SaleRec.NoneSale_Flg = 'Y';
      }

      if (_memicmp(apTokens[SCR_S_NAME], "Unknown", 7))
      {
         if (*apTokens[SCR_S_ISGRANTOR] == '1')
         {
            // Seller
            if (iGtor < 2)
            {
               strcpy(acTmp, apTokens[SCR_S_NAME]);
               iTmp = blankRem(acTmp);
               if (!iGtor)
                  vmemcpy(SaleRec.Seller1, acTmp, SALE_SIZ_SELLER);
               else
                  vmemcpy(SaleRec.Seller2, acTmp, SALE_SIZ_SELLER);
            }
            iGtor++;
         } else
         {
            // Buyer
            if (iGtee < 2)
            {
               strcpy(acTmp, apTokens[SCR_S_NAME]);
               iTmp = blankRem(acTmp);
               if (!iGtee)
                  vmemcpy(SaleRec.Name1, acTmp, SALE_SIZ_BUYER);
               else
                  vmemcpy(SaleRec.Name2, acTmp, SALE_SIZ_BUYER);
            }
            iGtee++;
         }
      }

      memcpy((void *)&LastRec, (void *)&SaleRec, sizeof(SCSAL_REC));

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   fclose(fdSale);
   fclose(fdOut);
   
   // Sort output file
   LogMsg("Total processed records: %u\n", lCnt);
   sprintf(acSaleFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "out");
   // Sort output file and dedup on APN asc, DocDate asc, DocNum asc, Seller desc
   sprintf(acTmp, "S(1,14,C,A,27,8,C,A,57,10,C,D,15,12,C,A,69,30,C,D) F(TXT) DUPO(B1024,1,34) ");
   lTmp = sortFile(acTmpFile, acSaleFile, acTmp, &iTmp);
   if (lTmp > 0)
   {
      if (!_access(acCSalFile, 0))
      {
         sprintf(acRec, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "srt");
         sprintf(acTmpFile, "%s+%s", acSaleFile, acCSalFile);
         lTmp = sortFile(acTmpFile, acRec, acTmp);
         if (lTmp > 0)
         {
            sprintf(acTmpFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "sav");
            if (!_access(acTmpFile, 0))
               DeleteFile(acTmpFile);

            // Rename SLS file to SAV
            rename(acCSalFile, acTmpFile);

            // Rename srt to SLS file
            rename(acRec, acCSalFile);
         }
      } else
      {
         rename(acSaleFile, acCSalFile);
      }
   }

   LogMsg("Number of Sale records processed: %d.", lCnt);
   LogMsg("                       output:    %d.", lTmp);

   return iTmp;
}

/******************************* Scr_CreateLienRec ***************************
 *
 * Return 0 if successful, >0 if error
 *
 *****************************************************************************/

int Scr_CreateLienRec(char *pOutbuf, char *pRollRec)
{
   char     acTmp[256];
   long     lTmp;
   int      iTmp;
   SCR_ROLL *pRec = (SCR_ROLL *)pRollRec;
   LIENEXTR *pLienRec = (LIENEXTR *)pOutbuf;

   // Clear output buffer
   memset(pOutbuf, ' ', sizeof(LIENEXTR));

   // Start copying data
   memcpy(pLienRec->acApn, pRec->Apn, SIZ_SCR_APN);

   // TRA
   lTmp = atoin(pRec->TRA, SIZ_SCR_TRA);
   if (lTmp > 0)
   {
      iTmp = sprintf(acTmp, "%.6d", lTmp);
      memcpy(pLienRec->acTRA, acTmp, iTmp);
   }

   // Year assessed
   memcpy(pLienRec->acYear, myCounty.acYearAssd, 4);

   // Land
   long lLand = atoin(pRec->Land, SIZ_SCR_LAND);
   if (lLand > 0)
   {
      iTmp = sprintf(acTmp, "%*u", sizeof(pLienRec->acLand), lLand);
      memcpy(pLienRec->acLand, acTmp, iTmp);
   }

   // Improve - Ratio
   long lImpr = atoin(pRec->Impr, SIZ_SCR_IMPR);
   if (lImpr > 0)
   {
      iTmp = sprintf(acTmp, "%*u", sizeof(pLienRec->acImpr), lImpr);
      memcpy(pLienRec->acImpr, acTmp, iTmp);

      iTmp = sprintf(acTmp, "%*u", sizeof(pLienRec->acRatio), (LONGLONG)lImpr*100/(lLand+lImpr));
      memcpy(pLienRec->acRatio, acTmp, iTmp);
   }

   // Pers Prop
   long lPers = atoin(pRec->PP_Val, SIZ_SCR_PP_VAL);

   // Fixture/Mechanical Equitment
   long lFixt = atoin(pRec->ME_Val, SIZ_SCR_ME_VAL);

   // Total other
   lTmp = lPers + lFixt;
   if (lTmp > 0)
   {
      iTmp = sprintf(acTmp, "%*u", sizeof(pLienRec->acOther), lTmp);
      memcpy(pLienRec->acOther, acTmp, iTmp);

      if (lFixt > 0)
      {
         iTmp = sprintf(acTmp, "%*u", sizeof(pLienRec->acME_Val), lFixt);
         memcpy(pLienRec->acME_Val, acTmp, iTmp);
      }
      if (lPers > 0)
      {
         iTmp = sprintf(acTmp, "%*u", sizeof(pLienRec->acPP_Val), lPers);
         memcpy(pLienRec->acPP_Val, acTmp, iTmp);
      }
   }

   // Gross
   lTmp += (lLand + lImpr);
   if (lTmp > 0)
   {
      iTmp = sprintf(acTmp, "%*u", sizeof(pLienRec->acGross), lTmp);
      memcpy(pLienRec->acGross, acTmp, iTmp);
   }

   // Set full exemption flag
   //if (pRec->ExeCode > ' ')
   if (lTmp <= 2000)    // See Travis email 12/15/2010
      pLienRec->SpclFlag = LX_FULLEXE_FLG;

   // HO Exempt
   long lExe = atoin(pRec->HOEX, SIZ_SCR_EXEMP_AMT);
   if (lExe > 0)
   {
      pLienRec->acHO[0] = '1';      // 'Y'
      iTmp = sprintf(acTmp, "%*u", SIZ_LIEN_EXEAMT, lExe);
      memcpy(pLienRec->acExAmt, acTmp, iTmp);
      pLienRec->acExCode[0], pRec->ExeCode;
   } else
      pLienRec->acHO[0] = '2';      // 'N'

   pLienRec->LF[0] = '\n';
   pLienRec->LF[1] = 0;
   return 0;
}

/********************************* Scr_ExtrLien *****************************
 *
 *
 ****************************************************************************/

int Scr_ExtrLien()
{
   char  *pTmp, acBuf[MAX_RECSIZE], acRollRec[MAX_RECSIZE], acOutFile[_MAX_PATH];
   FILE  *fdLien;
   long  lCnt=0;

   // Open roll file
   LogMsg("Open Roll file %s", acRollFile);
   fdRoll = fopen(acRollFile, "r");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", acRollFile);
      return 2;
   }

   // Open Output file
   sprintf(acOutFile, acLienTmpl, myCounty.acCntyCode, myCounty.acCntyCode);
   LogMsg("Open output file %s", acOutFile);
   fdLien = fopen(acOutFile, "w");
   if (fdLien == NULL)
   {
      LogMsg("***** Error opening Lien extract file: %s\n", acOutFile);
      return 4;
   }

   // Merge loop
   while (!feof(fdRoll))
   {
      // Get roll rec
      pTmp = fgets(acRollRec, MAX_RECSIZE, fdRoll);
      if (!pTmp)
         break;

      // Create new lien record
      Scr_CreateLienRec(acBuf, acRollRec);

      fputs(acBuf, fdLien);
      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdRoll)
      fclose(fdRoll);
   if (fdLien)
      fclose(fdLien);

   LogMsgD("Total records output:     %d\n", lCnt);

   lRecCnt = lCnt;
   return 0;
}

/******************************* Scr_CreateLienRec ***************************
 *
 * Return 0 if successful, >0 if error
 *
 *****************************************************************************/

int Scr_CreateLienRec2(char *pOutbuf, char *pRec)
{
   char     acTmp[256], *apItems[MAX_FLD_TOKEN];
   int      iTmp;
   LONGLONG lTmp, lTotalExe, lGross, lNet;
   LIENEXTR *pLienRec = (LIENEXTR *)pOutbuf;

   // Parse input record
   if (cDelim == ',')
      iTmp = ParseStringNQ(pRec, cDelim, MAX_FLD_TOKEN, apItems);
   else
      iTmp = ParseStringIQ(pRec, cDelim, MAX_FLD_TOKEN, apItems);
   if (iTmp < SCR_V_EXE_AMT+1)
   {
      LogMsg("***** Error: bad value record for APN=%.11s (#tokens=%d)", pOutbuf, iTmp);
      return -1;
   }

   // Verify tax year
   if (memcmp(myCounty.acYearAssd, apItems[SCR_V_TAXYEAR], 4))
      LogMsg("*** Prev tax year %.4s <> %s for APN=%s.  Please verify!", myCounty.acYearAssd, apItems[SCR_V_TAXYEAR], apItems[SCR_V_PIN]);

   // Reset output buffer
   memset(pOutbuf, ' ', sizeof(LIENEXTR));

   // Set APN
   memcpy(pLienRec->acApn, apItems[SCR_V_PIN], iApnLen);

   // Year assessed
   memcpy(pLienRec->acYear, apItems[SCR_V_TAXYEAR], 4);

   // HO Exempt
   pLienRec->acHO[0] = '2';      // 'N'
   lTotalExe = atol(apItems[SCR_V_EXE_AMT]);
   if (lTotalExe > 0)
   {
      iTmp = sprintf(acTmp, "%u", lTotalExe);
      memcpy(pLienRec->acExAmt, acTmp, iTmp);
      memcpy(pLienRec->extra.Scr.Exe[0].Amt, acTmp, iTmp);
   }
   if (*apItems[SCR_V_EXE_NAME] == 'H')
   {
      pLienRec->acHO[0] = '1';      // 'Y'
      memcpy(pLienRec->extra.Scr.Exe[0].Code, "HO", 2);
   } else if (*apItems[SCR_V_EXE_NAME] > ' ')
   {
      if (!_memicmp(apItems[SCR_V_EXE_NAME], "CE", 2))
         memcpy(pLienRec->extra.Scr.Exe[0].Code, "CE", 2);
      else if (!_memicmp(apItems[SCR_V_EXE_NAME], "CH", 2))
         memcpy(pLienRec->extra.Scr.Exe[0].Code, "CH", 2);
      else if (!_memicmp(apItems[SCR_V_EXE_NAME], "CO", 2))
         memcpy(pLienRec->extra.Scr.Exe[0].Code, "CO", 2);
      else if (!_memicmp(apItems[SCR_V_EXE_NAME], "DI", 2))
      {
         if (*(apItems[SCR_V_EXE_NAME]+19) == 'P')
            memcpy(pLienRec->extra.Scr.Exe[0].Code, "DVPY", 4);
         else
            memcpy(pLienRec->extra.Scr.Exe[0].Code, "DV", 2);
      } else if (!_memicmp(apItems[SCR_V_EXE_NAME], "FR", 2))
         memcpy(pLienRec->extra.Scr.Exe[0].Code, "MU", 2);
      else if (!_memicmp(apItems[SCR_V_EXE_NAME], "LOW I", 4))
         memcpy(pLienRec->extra.Scr.Exe[0].Code, "LI", 2);
      else if (!_memicmp(apItems[SCR_V_EXE_NAME], "LOW V", 4))
         memcpy(pLienRec->extra.Scr.Exe[0].Code, "LV", 2);
      else if (!_memicmp(apItems[SCR_V_EXE_NAME], "PU", 2))
         memcpy(pLienRec->extra.Scr.Exe[0].Code, "SC", 2);
      else if (!_memicmp(apItems[SCR_V_EXE_NAME], "RE", 2))
         memcpy(pLienRec->extra.Scr.Exe[0].Code, "RE", 2);
      else if (!_memicmp(apItems[SCR_V_EXE_NAME], "WE", 2))
         memcpy(pLienRec->extra.Scr.Exe[0].Code, "WE", 2);
      else
         LogMsg("*** Unknown exemption: %s", apItems[SCR_V_EXE_NAME]);
   }

   // Land
   dollar2Num(apItems[SCR_V_LAND], acTmp);
   long lLand = atol(acTmp);
   if (lLand > 0)
   {
      sprintf(acTmp, "%*u", SIZ_LIEN_LAND, lLand);
      memcpy(pLienRec->acLand, acTmp, SIZ_LIEN_LAND);
   }

   // Improve
   dollar2Num(apItems[SCR_V_IMPR], acTmp);
   long lImpr = atol(acTmp);
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*u", SIZ_LIEN_LAND, lImpr);
      memcpy(pLienRec->acImpr, acTmp, SIZ_LIEN_LAND);
   }

   // Living Improve
   dollar2Num(apItems[SCR_V_LIVINGIMPR], acTmp);
   long lLivImpr = atol(acTmp);
   if (lLivImpr > 0)
   {
      sprintf(acTmp, "%*u", SIZ_LIEN_PERS, lLivImpr);
      memcpy(pLienRec->extra.Scr.LivImpr, acTmp, SIZ_LIEN_PERS);
   }

   // Personal property
   dollar2Num(apItems[SCR_V_PERSONAL], acTmp);
   long lPP = atol(acTmp);
   if (lPP > 0)
   {
      sprintf(acTmp, "%*u", SIZ_LIEN_FIXT, lPP);
      memcpy(pLienRec->acPP_Val, acTmp, SIZ_LIEN_FIXT);
   }

   // Other values
   lTmp = lPP + lLivImpr;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*u", SIZ_LIEN_OTHERS, lTmp);
      memcpy(pLienRec->acOther, acTmp, SIZ_LIEN_OTHERS);
   }

   // Gross total
   lGross = lTmp+lLand+lImpr;
   if (lGross > 0)
   {
      sprintf(acTmp, "%*u", SIZ_LIEN_GROSS, lGross);
      memcpy(pLienRec->acGross, acTmp, SIZ_LIEN_GROSS);
   }

   // Net taxable value
   lNet = atol(apItems[SCR_V_NET_VALUE]);
   iTmp = sprintf(acTmp, "%u", lNet);
   memcpy(pLienRec->acNetTaxValue, acTmp, iTmp);

   // Set full exemption flag, See Travis email 12/15/2010
   if (lGross > 0 && (lGross <= 2000 || lNet == 0))
      pLienRec->SpclFlag = LX_FULLEXE_FLG;

   // Ratio
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*u", SIZ_LIEN_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
      memcpy(pLienRec->acRatio, acTmp, SIZ_LIEN_RATIO);
   }

   pLienRec->LF[0] = '\n';
   pLienRec->LF[1] = 0;

   return 0;
}

/****************************** Scr_ExtrLien2 *******************************
 *
 ****************************************************************************/

int Scr_ExtrLien2()
{
   char  *pTmp, acBuf[MAX_RECSIZE], acInrec[MAX_RECSIZE], acOutFile[_MAX_PATH];
   FILE  *fdLien, *fdValue;
   long  lCnt=0, lDup=0;
   LIENEXTR *pLienRec = (LIENEXTR *)&acBuf[0];

   LogMsg0("Extract Lien value");

   // Open Value file
   GetIniString(myCounty.acCntyCode, "ValueFile", "", acBuf, 256, acIniFile);
   sprintf(acInrec, "%s\\SCR\\SCR_Values.txt", acTmpPath);
   sortFile(acBuf, acInrec, "S(#1,C,A) DEL(124) F(TXT) ");
   LogMsg("Open Value file %s", acInrec);
   fdValue = fopen(acInrec, "r");
   if (fdValue == NULL)
   {
      LogMsg("***** Error opening Value file: %s\n", acInrec);
      return -2;
   }

   // Open Output file
   sprintf(acOutFile, acLienTmpl, myCounty.acCntyCode, myCounty.acCntyCode);
   LogMsg("Open output file %s", acOutFile);
   fdLien = fopen(acOutFile, "w");
   if (fdLien == NULL)
   {
      LogMsg("***** Error opening Lien extract file: %s\n", acOutFile);
      return -4;
   }

   memset(acBuf, ' ', sizeof(LIENEXTR));

   // Merge loop
   while (!feof(fdValue))
   {
      // Get roll rec
      pTmp = fgets(acInrec, MAX_RECSIZE, fdValue);
      if (!pTmp)
         break;

      iApnLen = strchr(acInrec, cDelim) - &acInrec[0];

      // Skip duplicate entry
      if (isdigit(acInrec[0]) && memcmp(acInrec, acBuf, iApnLen))
      {
         // Create new lien record
         if (!Scr_CreateLienRec2(acBuf, acInrec))
         {
            fputs(acBuf, fdLien);
            lRecCnt++;
         }
      } else
      {
         LogMsg("*** Drop dup record: %.80s", acInrec);
         lDup++;
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdValue)
      fclose(fdValue);
   if (fdLien)
      fclose(fdLien);

   LogMsg("Total records processed:  %d", lCnt);
   LogMsg("         records output:  %d", lRecCnt);
   LogMsg("     Duplicate records:   %d\n", lDup);

   return 0;
}

/****************************** Scr_ExtrLienX *******************************
 *
 * 2023: SCC_ParcelQuest_Get_AssessmentValues_Exemptions.txt
 *    - This file is very messy.  We only take what we need.  Each record may
 *      contain only one value.
 *
 ****************************************************************************/

int Scr_ExtrLienX()
{
   char  *pTmp, acBuf[1023], acInrec[1024], acTmp[_MAX_PATH];
   char  *apItems[32], acApn[32];

   FILE  *fdLien, *fdValue;
   long  lCnt=0, lDup=0, lDrop=0;
   ULONG lNet, lTotalAmt, lLand, lImpr, lLivImpr, lTotalExe;
   int   iCnt, iTmp;
   LIENEXTR *pLienRec = (LIENEXTR *)&acBuf[0];

   LogMsg0("Extract Lien value");

   // Open Value file
   GetIniString(myCounty.acCntyCode, "ValueFile", "", acBuf, 256, acIniFile);
   sprintf(acInrec, "%s\\SCR\\SCR_Values.txt", acTmpPath);
   sortFile(acBuf, acInrec, "S(#1,C,A,#17,C,A) F(TXT) DEL(124) OMIT(#1,C,LT,\"0\",OR,#1,C,GT,\"A\") ");
   LogMsg("Open Value file %s", acInrec);
   fdValue = fopen(acInrec, "r");
   if (fdValue == NULL)
   {
      LogMsg("***** Error opening Value file: %s\n", acInrec);
      return -2;
   }

   // Open Output file
   sprintf(acTmp, acLienTmpl, myCounty.acCntyCode, myCounty.acCntyCode);
   LogMsg("Open output file %s", acTmp);
   fdLien = fopen(acTmp, "w");
   if (fdLien == NULL)
   {
      LogMsg("***** Error opening Lien extract file: %s\n", acTmp);
      return -4;
   }


   // Merge loop
   while (!feof(fdValue))
   {
      // Get roll rec
      pTmp = fgets(acInrec, MAX_RECSIZE, fdValue);
      if (!pTmp)
         break;

      // Parse input record
      if (cDelim == ',')
         iCnt = ParseStringNQ(acInrec, cDelim, 32, apItems);
      else
         iCnt = ParseStringIQ(acInrec, cDelim, 32, apItems);
      if (iCnt < SCR_X_FLDS)
      {
         //LogMsg("***** Error: bad value record for APN=%s (#tokens=%d)", acInrec, iCnt);
         lDrop++;
         continue;
      }

#ifdef _DEBUG
      //if (!memcmp(acInrec, "00636114", 8))
      //   iTmp = 0;
#endif
      if (!_memicmp(apItems[SCR_L_REVOBJTYPE], "Annual", 6) && !_memicmp(apItems[SCR_X_ROLLCASTE], "Secured", 6))
      {
         if (strlen(apItems[SCR_L_PIN]) < 8)
         {
            LogMsg("***** Error: bad APN=%s (#tokens=%d)", apItems[SCR_L_PIN], iCnt);
            //lDrop++;
            continue;
         }

         memset(acBuf, ' ', sizeof(LIENEXTR));
         sprintf(acApn, "%s     ", apItems[SCR_X_PIN]);
         acApn[iApnLen] = 0;
         memcpy(pLienRec->acApn, acApn, iApnLen);
         memcpy(pLienRec->acAltApn, apItems[SCR_X_AIN], strlen(apItems[SCR_X_AIN]));

         lLand = atol(apItems[SCR_X_LAND]);
         lImpr = atol(apItems[SCR_X_IMPR]);
         lLivImpr = atol(apItems[SCR_X_LIVIMPR]);
         lTotalExe = atol(apItems[SCR_X_EXEAMT]);
         lTotalAmt = lLand+lImpr+lLivImpr;

         if (lLand > 0)
         {
            iTmp = sprintf(acTmp, "%*u", SIZ_LIEN_LAND, lLand);
            memcpy(pLienRec->acLand, acTmp, iTmp);
         }
         if (lImpr > 0)
         {
            iTmp = sprintf(acTmp, "%*u", SIZ_LIEN_LAND, lImpr);
            memcpy(pLienRec->acImpr, acTmp, iTmp);
         }
         if (lLivImpr > 0)
         {
            iTmp = sprintf(acTmp, "%*u", SIZ_LIEN_LAND, lLivImpr);
            memcpy(pLienRec->acOther, acTmp, iTmp);
            memcpy(pLienRec->extra.Scr.LivImpr, acTmp, iTmp);
         }
         if (lTotalAmt > 0)
         {
            iTmp = sprintf(acTmp, "%*u", SIZ_LIEN_LAND, lTotalAmt);
            memcpy(pLienRec->acGross, acTmp, iTmp);
         }

         // Net taxable value
         lNet = atol(apItems[SCR_X_NET]);
         if (lNet > 0)
         {
            iTmp = sprintf(acTmp, "%*u", SIZ_LIEN_LAND, lNet);
            memcpy(pLienRec->acNetTaxValue, acTmp, iTmp);
         }

         // Set full exemption flag, See Travis email 12/15/2010
         if (lTotalAmt > 0 && (lTotalAmt <= 2000 || lNet == 0))
            pLienRec->SpclFlag = LX_FULLEXE_FLG;

         // Ratio
         if (lImpr > 0)
         {
            sprintf(acTmp, "%*u", SIZ_LIEN_RATIO, (ULONG)lImpr*100/(lLand+lImpr));
            memcpy(pLienRec->acRatio, acTmp, SIZ_LIEN_RATIO);
         }

         pLienRec->acHO[0] = '2';
         if (lTotalExe > 0)
         {
            if (!memcmp(apItems[SCR_X_CLASSCATSDESC], "Homeowners", 3))
            {
               pLienRec->acHO[0] = '1';
               memcpy(pLienRec->extra.Scr.Exe[0].Code, "HO", 2);
            } else
            {
               if (strstr(apItems[SCR_X_CLASSCATSDESC], "Vet"))
                  memcpy(pLienRec->extra.Scr.Exe[0].Code, "DV", 2);
               else if (!memcmp(apItems[SCR_X_CLASSCATSDESC], "Rel", 3))
                  memcpy(pLienRec->extra.Scr.Exe[0].Code, "RE", 2);
               else if (!memcmp(apItems[SCR_X_CLASSCATSDESC], "Wel", 3))
                  memcpy(pLienRec->extra.Scr.Exe[0].Code, "WE", 2);
               else if (!memcmp(apItems[SCR_X_CLASSCATSDESC], "Chu", 3))
                  memcpy(pLienRec->extra.Scr.Exe[0].Code, "CH", 2);
               else if (!memcmp(apItems[SCR_X_CLASSCATSDESC], "Cem", 3))
                  memcpy(pLienRec->extra.Scr.Exe[0].Code, "CE", 2);
               else if (!memcmp(apItems[SCR_X_CLASSCATSDESC], "Col", 3))
                  memcpy(pLienRec->extra.Scr.Exe[0].Code, "CO", 2);
               else if (!memcmp(apItems[SCR_X_CLASSCATSDESC], "Fre", 3))
                  memcpy(pLienRec->extra.Scr.Exe[0].Code, "MU", 2);
               else if (!memcmp(apItems[SCR_X_CLASSCATSDESC], "Low", 3))
                  memcpy(pLienRec->extra.Scr.Exe[0].Code, "LV", 2);
               else if (!_memicmp(apItems[SCR_X_CLASSCATSDESC], "PU", 2))
                  memcpy(pLienRec->extra.Scr.Exe[0].Code, "SC", 2);
               else
                  LogMsg("*** Unknown exemption: %s", apItems[SCR_X_CLASSCATSDESC]);
            }

            iTmp = sprintf(acTmp, "%*u", SIZ_LIEN_LAND, lTotalExe);
            memcpy(pLienRec->acExAmt, acTmp, iTmp);
            memcpy(pLienRec->acExCode, pLienRec->extra.Scr.Exe[0].Code, SIZ_LIEN_EXECODE);
         }

         pLienRec->LF[0] = '\n';
         pLienRec->LF[1] = 0;
         fputs(acBuf, fdLien);
         lRecCnt++;
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdValue)
      fclose(fdValue);
   if (fdLien)
      fclose(fdLien);

   LogMsg("Total records processed:  %d", lCnt);
   LogMsg("         records dropped  %d", lDrop);
   LogMsg("         records output:  %d\n", lRecCnt);

   return 0;
}

/****************************** Scr_ExtrLien3 *******************************
 *
 * 2024: SCC_ParcelQuest_Get_AssessmentValues_Exemptions.txt
 *
 ****************************************************************************/

int Scr_ExtrLien3()
{
   char  *pTmp, acBuf[1023], acInrec[1024], acTmp[_MAX_PATH];
   char  *apItems[32], acApn[32];

   FILE  *fdLien, *fdValue;
   long  lCnt=0, lDup=0, lDrop=0;
   ULONG lNet, lTotalAmt, lLand, lImpr, lLivImpr, lTotalExe;
   int   iCnt, iTmp;
   LIENEXTR *pLienRec = (LIENEXTR *)&acBuf[0];

   LogMsg0("Extract Lien value");

   // Open Value file
   GetIniString(myCounty.acCntyCode, "ValueFile", "", acBuf, 256, acIniFile);
   sprintf(acInrec, "%s\\SCR\\SCR_Values.txt", acTmpPath);
   // 2024
   sortFile(acBuf, acInrec, "S(#1,C,A) F(TXT) DEL(124) OMIT(#1,C,LT,\"0\",OR,#1,C,GT,\"A\") ");
   LogMsg("Open Value file %s", acInrec);
   fdValue = fopen(acInrec, "r");
   if (fdValue == NULL)
   {
      LogMsg("***** Error opening Value file: %s\n", acInrec);
      return -2;
   }

   // Open Output file
   sprintf(acTmp, acLienTmpl, myCounty.acCntyCode, myCounty.acCntyCode);
   LogMsg("Open output file %s", acTmp);
   fdLien = fopen(acTmp, "w");
   if (fdLien == NULL)
   {
      LogMsg("***** Error opening Lien extract file: %s\n", acTmp);
      return -4;
   }

   // Merge loop
   while (!feof(fdValue))
   {
      // Get roll rec
      pTmp = fgets(acInrec, MAX_RECSIZE, fdValue);
      if (!pTmp)
         break;

      // Parse input record
      if (cDelim == ',')
         iCnt = ParseStringNQ(acInrec, cDelim, 32, apItems);
      else
         iCnt = ParseStringIQ(acInrec, cDelim, 32, apItems);
      if (iCnt < SCR_L_FLDS)
      {
         LogMsg("***** Error: bad value record for APN=%s (#tokens=%d)", acInrec, iCnt);
         lDrop++;
         continue;
      }

#ifdef _DEBUG
      //if (!memcmp(acInrec, "00636114", 8))
      //   iTmp = 0;
#endif
      if (!_memicmp(apItems[SCR_L_ROLLCASTE], "Annual", 6) && !_memicmp(apItems[SCR_L_REVOBJTYPE], "Secured", 6))
      {
         if (strlen(apItems[SCR_L_PIN]) < 8)
         {
            LogMsg("***** Error: bad APN=%s (#tokens=%d)", apItems[SCR_L_PIN], iCnt);
            //lDrop++;
            continue;
         }

         memset(acBuf, ' ', sizeof(LIENEXTR));
         sprintf(acApn, "%s     ", apItems[SCR_L_PIN]);
         acApn[iApnLen] = 0;
         memcpy(pLienRec->acApn, acApn, iApnLen);
         memcpy(pLienRec->acAltApn, apItems[SCR_L_AIN], strlen(apItems[SCR_L_AIN]));

         lLand = atol(apItems[SCR_L_LAND]);
         lImpr = atol(apItems[SCR_L_IMPR]);
         lLivImpr = atol(apItems[SCR_L_LIVIMPR]);
         lTotalExe = atol(apItems[SCR_L_EXEAMT]);
         lTotalAmt = lLand+lImpr+lLivImpr;

         if (lLand > 0)
         {
            iTmp = sprintf(acTmp, "%*u", SIZ_LIEN_LAND, lLand);
            memcpy(pLienRec->acLand, acTmp, iTmp);
         }
         if (lImpr > 0)
         {
            iTmp = sprintf(acTmp, "%*u", SIZ_LIEN_LAND, lImpr);
            memcpy(pLienRec->acImpr, acTmp, iTmp);
         }
         if (lLivImpr > 0)
         {
            iTmp = sprintf(acTmp, "%*u", SIZ_LIEN_LAND, lLivImpr);
            memcpy(pLienRec->acOther, acTmp, iTmp);
            memcpy(pLienRec->extra.Scr.LivImpr, acTmp, iTmp);
         }
         if (lTotalAmt > 0)
         {
            iTmp = sprintf(acTmp, "%*u", SIZ_LIEN_LAND, lTotalAmt);
            memcpy(pLienRec->acGross, acTmp, iTmp);
         }

         // Net taxable value
         lNet = atol(apItems[SCR_L_NET]);
         if (lNet > 0)
         {
            iTmp = sprintf(acTmp, "%*u", SIZ_LIEN_LAND, lNet);
            memcpy(pLienRec->acNetTaxValue, acTmp, iTmp);
         }

         // Set full exemption flag, See Travis email 12/15/2010
         if (lTotalAmt > 0 && (lTotalAmt <= 2000 || lNet == 0))
            pLienRec->SpclFlag = LX_FULLEXE_FLG;

         // Ratio
         if (lImpr > 0)
         {
            sprintf(acTmp, "%*u", SIZ_LIEN_RATIO, (ULONG)lImpr*100/(lLand+lImpr));
            memcpy(pLienRec->acRatio, acTmp, SIZ_LIEN_RATIO);
         }

         pLienRec->acHO[0] = '2';
         if (lTotalExe > 0)
         {
            if (!memcmp(apItems[SCR_L_EXENAME], "Homeowners", 3))
            {
               pLienRec->acHO[0] = '1';
               memcpy(pLienRec->extra.Scr.Exe[0].Code, "HO", 2);
            } else
            {
               if (strstr(apItems[SCR_L_EXENAME], "Vet"))
                  memcpy(pLienRec->extra.Scr.Exe[0].Code, "DV", 2);
               else if (!memcmp(apItems[SCR_L_EXENAME], "Rel", 3))
                  memcpy(pLienRec->extra.Scr.Exe[0].Code, "RE", 2);
               else if (!memcmp(apItems[SCR_L_EXENAME], "Wel", 3))
                  memcpy(pLienRec->extra.Scr.Exe[0].Code, "WE", 2);
               else if (!memcmp(apItems[SCR_L_EXENAME], "Chu", 3))
                  memcpy(pLienRec->extra.Scr.Exe[0].Code, "CH", 2);
               else if (!memcmp(apItems[SCR_L_EXENAME], "Cem", 3))
                  memcpy(pLienRec->extra.Scr.Exe[0].Code, "CE", 2);
               else if (!memcmp(apItems[SCR_L_EXENAME], "Col", 3))
                  memcpy(pLienRec->extra.Scr.Exe[0].Code, "CO", 2);
               else if (!memcmp(apItems[SCR_L_EXENAME], "Fre", 3))
                  memcpy(pLienRec->extra.Scr.Exe[0].Code, "MU", 2);
               else if (!memcmp(apItems[SCR_L_EXENAME], "Low Value", 5))
                  memcpy(pLienRec->extra.Scr.Exe[0].Code, "LV", 2);
               else if (!_memicmp(apItems[SCR_L_EXENAME], "PU", 2))
                  memcpy(pLienRec->extra.Scr.Exe[0].Code, "SC", 2);
               else
                  LogMsg("*** Unknown exemption: %s", apItems[SCR_L_EXENAME]);
            }

            iTmp = sprintf(acTmp, "%*u", SIZ_LIEN_LAND, lTotalExe);
            memcpy(pLienRec->acExAmt, acTmp, iTmp);
            memcpy(pLienRec->acExCode, pLienRec->extra.Scr.Exe[0].Code, SIZ_LIEN_EXECODE);
         }

         // Assessment Type
         pLienRec->AsmtType = *apItems[SCR_L_ASMTTYPE];

         pLienRec->LF[0] = '\n';
         pLienRec->LF[1] = 0;
         fputs(acBuf, fdLien);
         lRecCnt++;
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdValue)
      fclose(fdValue);
   if (fdLien)
      fclose(fdLien);

   LogMsg("Total records processed:  %d", lCnt);
   LogMsg("         records dropped  %d", lDrop);
   LogMsg("         records output:  %d\n", lRecCnt);

   return 0;
}

/******************************* Scr_ExtractProp8 *****************************
 *
 * Extract  Prop8 flag from roll file.
 *
 ******************************************************************************/

int Scr_ExtractProp8(char *pProp8File)
{
   char     *pTmp, acTmp[256], acProp8Rec[MAX_RECSIZE];
   char     acOutFile[_MAX_PATH];

   FILE     *fdProp8, *fdExt;
   SCR_ROLL *pRoll = (SCR_ROLL *)&acProp8Rec[0];

   int      iProp8Match=0;
   long     lCnt=0;

   if (lLienYear > 1900)
      sprintf(acTmp, "%d", lLienYear);
   else
      strcpy(acTmp, "yyyy");
   sprintf(acOutFile, acProp8Tmpl, myCounty.acCntyCode, myCounty.acCntyCode, acTmp, "P8");

   // Open Prop8 file
   LogMsg("Open Prop8 file %s", pProp8File);
   fdProp8 = fopen(pProp8File, "r");
   if (fdProp8 == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", pProp8File);
      return -2;
   }

   // Open Output file
   LogMsg("Open output file %s", acOutFile);
   fdExt = fopen(acOutFile, "w");
   if (fdExt == NULL)
   {
      LogMsg("***** Error creating output file: %s\n", acOutFile);
      return -4;
   }

   // Output header
   fputs("APN\n", fdExt);

   // Get first lien rec
   pTmp = fgets((char *)&acProp8Rec[0], 1024, fdProp8);

   // Merge loop
   while (pTmp)
   {
      // Set prop8 flag 
      if (!memcmp(pRoll->LandCode, "52", 2) || !memcmp(pRoll->LandCode, "53", 2))
      {
         // Update flag
         sprintf(acTmp, "%.*s\n", iApnLen, pRoll->Apn);
         fputs(acTmp, fdExt);
         iProp8Match++;
      } 

      // Read next prop8 record
      pTmp = fgets(acProp8Rec, 1024, fdProp8);

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdProp8)
      fclose(fdProp8);
   if (fdExt)
      fclose(fdExt);

   LogMsgD("Total records processed:      %u", lCnt);
   LogMsgD("Total records extracted:      %u", iProp8Match);
   return 0;
}

/******************************** Scr_ParseDelq *****************************
 *
 * Each parcel may have more than one delq record.  Create them all but set
 * Base->DelqYear to the first one.
 *
 * Return 0 if success
 *
 *****************************************************************************/

int Scr_UpdateTaxDelq(char *pBasebuf, FILE *fd)
{
   static   char  acRec[MAX_RECSIZE], *pRec=NULL;
   char     acDelqRec[1024];
   long     lRedDate, lDefDate;
   int      iLoop, iTmp;

   TAXDELQ  *pDelqRec = (TAXDELQ *)acDelqRec;
   TAXBASE  *pBaseRec = (TAXBASE *)pBasebuf;

   if (!pRec)
      pRec = fgets(acRec, MAX_RECSIZE, fd);        // Get first rec

#ifdef _DEBUG
   // 00262208: on payment plan
   // 01719242
   if (!memcmp(pBaseRec->Apn, "00229427", 8))
      iTmp = 0;
#endif

   do
   {
      // Compare Apn
      iLoop = memcmp(pBaseRec->Apn, acRec, iApnLen);
      if (iLoop > 0)
      {
         pRec = fgets(acRec, MAX_RECSIZE, fd);
         if (!pRec)
         {
            fclose(fd);
            fd = NULL;
            return 1;      // EOF
         }
      }
   } while (iLoop > 0);

   if (iLoop)
      return 1;

   // Get default record from prior year
   if (!memcmp(pBaseRec->TaxYear, pRec+iApnLen, 4))
      pRec = fgets(acRec, MAX_RECSIZE, fd);

   // Parse input tax data
   iTokens = ParseStringIQ(acRec, cDelim, MAX_FLD_TOKEN, apTokens);
   if (iTokens < SCR_DR_SITUSFULLADDRESS)
   {
      LogMsg("***** Error: bad Tax record for: %.50s (#tokens=%d)", acRec, iTokens);
      return -1;
   }

   // Clear output buffer
   memset(acDelqRec, 0, sizeof(TAXDELQ));
   lRedDate=lDefDate = 0;

   // APN
   strcpy(pDelqRec->Apn, apTokens[SCR_DR_APN]);

   // Tax Status
   if (*apTokens[SCR_DR_PMTPLANSTATUS] == 'A')
      pDelqRec->DelqStatus[0] = TAX_STAT_ONPAYMENT;
   else if (*apTokens[SCR_DR_SECONDINSTALLMENTREDEEMED] == '1')
      pDelqRec->DelqStatus[0] = TAX_STAT_REDEEMED;
   else if (*apTokens[SCR_DR_FIRSTINSTALLMENTDEFAULTED] == '1' && *apTokens[SCR_DR_SECONDINSTALLMENTDEFAULTED] == '1')
   {
      pDelqRec->InstDel[0] = '3';
      pDelqRec->DelqStatus[0] = TAX_STAT_UNPAID;
   } else if (*apTokens[SCR_DR_FIRSTINSTALLMENTDEFAULTED] == '1')
   {
      pDelqRec->InstDel[0] = '1';
      pDelqRec->DelqStatus[0] = TAX_STAT_UNPAID;
   } else if (*apTokens[SCR_DR_SECONDINSTALLMENTDEFAULTED] == '1')
   {
      pDelqRec->InstDel[0] = '2';
      pDelqRec->DelqStatus[0] = TAX_STAT_UNPAID;
   }

   // Tax Year
   strcpy(pDelqRec->TaxYear, apTokens[SCR_DR_TAXYEAR]);

   //// Default amount
   //dTaxAmt = atof(apTokens[T01_BEGINBAL]);
   //if (dTaxAmt > 0.0)
   //{
   //   sprintf(pOutRec->Def_Amt, "%.2f", dTaxAmt);
   //   pOutRec->isDelq[0] = '1';
   //}

   //// Default date
   //strcpy(pOutRec->Def_Date, apTokens[T01_DELQYEAR]);

   //double   dDueAmt, dPaidAmt;
   //dDueAmt = atof(apTokens[T01_REDAMTDUE]);
   //dPaidAmt= atof(apTokens[T01_REDAMTPAID]);

   //// Redemption date
   //if (*apTokens[T01_REDDATE] > '0')
   //{
   //   strcpy(pOutRec->Red_Date, apTokens[T01_REDDATE]);
   //   pOutRec->isDelq[0] = '0';
   //   pOutRec->DelqStatus[0] = TAX_STAT_REDEEMED;
   //   if (dPaidAmt > 0.0)
   //      strcpy(pOutRec->Red_Amt, apTokens[T01_REDAMTPAID]);      
   //} else if (dDueAmt > 0.0)
   //{
   //   strcpy(pOutRec->Red_Amt, apTokens[T01_REDAMTDUE]);
   //   if (*apTokens[T01_REDAMTPAID] > '0')
   //      pOutRec->DelqStatus[0] = TAX_STAT_ONPAYMENT;
   //   else
   //      pOutRec->DelqStatus[0] = TAX_STAT_UNPAID;
   //   pOutRec->isDelq[0] = '1';
   //} else if (dPaidAmt > 0.0)
   //{
   //   strcpy(pOutRec->Red_Amt, apTokens[T01_REDAMTPAID]);      
   //   pOutRec->DelqStatus[0] = TAX_STAT_REDEEMED;
   //   pOutRec->isDelq[0] = '0';
   //   if (*apTokens[T01_LASTPAIDDATE] > '0')
   //      strcpy(pOutRec->Red_Date, apTokens[T01_LASTPAIDDATE]);
   //}

   //// Pen Amt
   //if (*apTokens[T01_REDPEN] > '0')
   //   strcpy(pOutRec->Pen_Amt, apTokens[T01_REDPEN]);

   //// Fee Amt
   //if (*apTokens[T01_REDFEEBAL] > '0')
   //   strcpy(pOutRec->Fee_Amt, apTokens[T01_REDFEEBAL]);

   //// Due date
   //if (*apTokens[T01_DUEDATE] > '0')
   //   strcpy(pOutRec->Pts_Date, apTokens[T01_DUEDATE]);

   return 0;
}

int Scr_ParseTaxBase(char *pBaseBuf)
{
   double   dTmp, dDue1, dDue2, dTotalDue;
   char     *pTmp;

   TAXBASE   *pBaseRec = (TAXBASE *)pBaseBuf;

   // Clear output buffer
   memset(pBaseBuf, 0, sizeof(TAXBASE));

   // APN
   strcpy(pBaseRec->Apn, apTokens[SCR_T_APN]);

   // TRA - get this from roll file

   // Tax Year
   strcpy(pBaseRec->TaxYear, apTokens[SCR_T_TAXYEAR]);
   if (!lTaxYear)
      lTaxYear = atol(apTokens[SCR_T_TAXYEAR]);

   // Updated date
   sprintf(pBaseRec->Upd_Date, "%d", lLastTaxFileDate);

   // BillNum
   sprintf(pBaseRec->BillNum, "%s%c", apTokens[SCR_T_APN], *apTokens[SCR_T_BILLTYPE]);

   // BillType
   if (*apTokens[SCR_T_BILLTYPE] != 'A')
   {
      // Transfer & Secured Escape are consider Supplemental
      pBaseRec->isSupp[0] = '1';

      if (*apTokens[SCR_T_BILLTYPE] == 'E')
         pBaseRec->BillType[0] = BILLTYPE_SECURED_ESCAPE;
      else
         pBaseRec->BillType[0] = BILLTYPE_SECURED_SUPPL;
   } else
   {
      pBaseRec->isSecd[0] = '1';
      pBaseRec->BillType[0] = BILLTYPE_SECURED;
   }

#ifdef _DEBUG
   //if (!memcmp(pBaseRec->Apn, "01871105", 8))
   //   dTmp = 0;
#endif

   // Check for Tax amount
   double dTax1 = atof(apTokens[SCR_T_FIRSTINSTALLMENTAMOUNT]);
   double dTax2 = atof(apTokens[SCR_T_SECONDINSTALLMENTAMOUNT]);
   double dTotalTax = dTax1+dTax2;
   if (dTotalTax > 1.0)
   {
      sprintf(pBaseRec->TotalTaxAmt, "%.2f", dTotalTax);

      sprintf(pBaseRec->TaxAmt1, "%.2f", dTax1);
      sprintf(pBaseRec->TaxAmt2, "%.2f", dTax2);
   } else
   {
      pBaseRec->Inst1Status[0] = TAX_BSTAT_NOTAX;
      pBaseRec->Inst2Status[0] = TAX_BSTAT_NOTAX;
      return 1;
   }

   // Penalty
   double dPen1 = atof(apTokens[SCR_T_FIRSTINSTALLMENTPENALTY]);
   double dPen2 = atof(apTokens[SCR_T_SECONDINSTALLMENTPENALTY]);
   dDue1=dDue2 = 0;

   if (dPen1 > 0.0)
      strcpy(pBaseRec->PenAmt1, apTokens[SCR_T_FIRSTINSTALLMENTPENALTY]);
   if (dPen2 > 0.0)
      strcpy(pBaseRec->PenAmt2, apTokens[SCR_T_SECONDINSTALLMENTPENALTY]);

   // Due Date
   dateConversion(apTokens[SCR_T_FIRSTINSTALLMENTDUEDATE],  pBaseRec->DueDate1, YYYY_MM_DD);
   dateConversion(apTokens[SCR_T_SECONDINSTALLMENTDUEDATE], pBaseRec->DueDate2, YYYY_MM_DD);

   // Paid
   if (*apTokens[SCR_T_FIRSTINSTALLMENTPAYMENTDATE] > ' ')
   {
      pBaseRec->Inst1Status[0] = TAX_BSTAT_PAID;    
      pTmp = dateConversion(apTokens[SCR_T_FIRSTINSTALLMENTPAYMENTDATE], pBaseRec->PaidDate1, YYYY_MM_DD);
      dTmp = dPen1 + dTax1;
      sprintf(pBaseRec->PaidAmt1, "%.2f", dTmp);
      if (*apTokens[SCR_T_SECONDINSTALLMENTPAYMENTDATE] > ' ')
      {
         pTmp = dateConversion(apTokens[SCR_T_SECONDINSTALLMENTPAYMENTDATE], pBaseRec->PaidDate2, YYYY_MM_DD);
         dTmp = dPen2 + dTax2;
         sprintf(pBaseRec->PaidAmt2, "%.2f", dTmp);
         pBaseRec->Inst2Status[0] = TAX_BSTAT_PAID;    
      } else
      {
         pBaseRec->Inst2Status[0] = TAX_BSTAT_UNPAID;    
         dDue2 = dTax2 + dPen2;
      }
   } else if (*apTokens[SCR_T_FIRSTINSTALLMENTDEFAULTED] == '1')
   {
      pBaseRec->Inst1Status[0] = TAX_BSTAT_PASTDUE;    
      if (*apTokens[SCR_T_SECONDINSTALLMENTDEFAULTED] == '1')
         pBaseRec->Inst2Status[0] = TAX_BSTAT_PASTDUE;   
      else
         pBaseRec->Inst2Status[0] = TAX_BSTAT_UNPAID;    
      dDue1 = dTax1 + dPen1;
      dDue2 = dTax2 + dPen2;
   } else //if (*apTokens[SCR_T_SECONDINSTALLMENTDEFAULTED] == '1')
   {
      pBaseRec->Inst1Status[0] = TAX_BSTAT_UNPAID;    
      pBaseRec->Inst2Status[0] = TAX_BSTAT_UNPAID;    
      dDue1 = dTax1 + dPen1;
      dDue2 = dTax2 + dPen2;
   }

   // Total due
   dTotalDue = dDue1 + dDue2;
   if (dTotalDue > 0.0)
   {
      sprintf(pBaseRec->TotalDue, "%.2f", dTotalDue);
   }

   // Total fee


   // Delq year


   return 0;
}

/**************************** Scr_Load_TaxRoll *****************************
 *
 * Loading SCC_GetAnnualRollForTaxServices.txt & SCC_GetDefaultedRedeemedInfoForTaxServices.txt
 * Create TaxBase
 *
 ***************************************************************************/

int Scr_Load_TaxRoll(bool bImport)
{
   char      *pTmp, acBase[1024], acDelq[1024], acRec[MAX_RECSIZE],
             acBaseFile[_MAX_PATH], acDelqFile[_MAX_PATH], acTmpFile[_MAX_PATH];

   int       iRet, iDrop=0;
   long      lOut=0, lCnt=0;
   FILE      *fdBase, *fdDelq, *fdDefRed, *fdIn, *fdR01;
   TAXBASE   *pBase = (TAXBASE *)acBase;
   TAXDELQ   *pDelq = (TAXDELQ *)acDelq;

   LogMsg0("Loading Tax Base & Delq");

   NameTaxCsvFile(acBaseFile, myCounty.acCntyCode, "Base");
   NameTaxCsvFile(acDelqFile, myCounty.acCntyCode, "Delq");
   pTmp = strrchr(acBaseFile, '\\');
   *pTmp = 0;
   if (_access(acBaseFile, 0))
      _mkdir(acBaseFile);
   *pTmp = '\\';

   // Open input file
   GetIniString(myCounty.acCntyCode, "TaxRoll", "", acTmpFile, _MAX_PATH, acIniFile);
   lLastTaxFileDate = getFileDate(acTmpFile);
   // Only process if new tax file
   iRet = isNewTaxFile(NULL, myCounty.acCntyCode);
   if (iRet <= 0)
   {
      lLastTaxFileDate = 0;
      return iRet;
   }

   LogMsg("Open tax roll file %s", acTmpFile);
   fdIn = fopen(acTmpFile, "r");
   if (fdIn == NULL)
   {
      LogMsg("***** Error opening tax roll file: %s\n", acTmpFile);
      return -2;
   }  


   // Open Delinquent file
   GetIniString(myCounty.acCntyCode, "TaxDelq", "", acTmpFile, _MAX_PATH, acIniFile);
   LogMsg("Open tax delq/redeem file %s", acTmpFile);
   fdDefRed = fopen(acTmpFile, "r");
   if (fdDefRed == NULL)
   {
      LogMsg("***** Error opening tax delq/redeem file: %s\n", acTmpFile);
      return -2;
   }  

   // Open R01 file
   iRet = GetIniString("Data", "RawFile", "", acRec, 128, acIniFile);
   sprintf(acTmpFile, acRec, myCounty.acCntyCode, myCounty.acCntyCode, "R01");
   if (_access(acTmpFile, 0))
      sprintf(acTmpFile, acRec, myCounty.acCntyCode, myCounty.acCntyCode, "S01");
   try {
      fdR01 = fopen(acTmpFile, "rb");
   } catch (...)
   {
      LogMsg("***** Error opening R01 file: %s", acTmpFile);
      fdR01 = NULL;
   }

   // Open base file
   LogMsg("Open Base file %s", acBaseFile);
   fdBase = fopen(acBaseFile, "w");
   if (fdBase == NULL)
   {
      LogMsg("***** Error creating base file: %s\n", acBaseFile);
      return -4;
   }

   // Skip header
   pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdIn);

   // Open delq file
   LogMsg("Open Delq output file %s", acDelqFile);
   fdDelq = fopen(acDelqFile, "w");
   if (fdDelq == NULL)
   {
      LogMsg("***** Error creating Delq output file: %s\n", acDelqFile);
      return -4;
   }

   // Merge loop 
   while (!feof(fdIn))
   {
      pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdIn);
      if (!pTmp)
         break;

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);

      // Parse input record
      iTokens = ParseStringIQ(acRec, cDelim, 256, apTokens);
      if (iTokens < SCR_T_SITUSFULLADDRESS)
      {
         LogMsg("*** Bad record: %s", acRec);
         continue;
      }

#ifdef _DEBUG
      //if (!memcmp(apTokens[SCR_T_APN], "00104212", 8))
      //   iRet = 0;
#endif

      // Create new base record
      iRet = Scr_ParseTaxBase(acBase);
      if (iRet >= 0)
      {
         // Update TRA if not there yet
         if (pBase->TRA[0] < '0' && fdR01)
            UpdateTRA(acBase, fdR01);

         // Update base and create delq records
         //Scr_UpdateTaxDelq(acBase, fdDelq);

         // Create TaxBase record
         Tax_CreateTaxBaseCsv(acRec, (TAXBASE *)&acBase);
         fputs(acRec, fdBase);
         lOut++;
      } else
      {
         iDrop++;
         LogMsg("---> Drop record %d [%.40s]", lCnt, acRec); 
      }
   }

   // Close files
   if (fdIn)
      fclose(fdIn);
   if (fdBase)
      fclose(fdBase);
   if (fdDelq)
      fclose(fdDelq);
   if (fdR01)
      fclose(fdR01);

   LogMsg("Total records processed:    %u", lCnt);
   LogMsg("Total output records:       %u", lOut);

   // Import into SQL
   if (bImport && lOut > 90000)
   {
      iRet = doTaxImport(myCounty.acCntyCode, TAX_BASE);
      //if (!iRet)
      //   iRet = doTaxImport(myCounty.acCntyCode, TAX_DELINQUENT);
      doTaxPrep(myCounty.acCntyCode, TAX_DELINQUENT);
   } else
      iRet = 0;

   return iRet;
}

/***************************** Scr_ParseTaxDetail ****************************
 *
 * Generate TAXDETAIL & TAXAGENCY
 *
 * Return number of detail records created if success
 *
 *****************************************************************************/

int Scr_ParseTaxDetail(char *pDetailBuf, char *pAgencyBuf)
{
   int      iRet=1;
   double   dTmp;
   TAXDETAIL *pDetail = (TAXDETAIL *)pDetailBuf;
   TAXAGENCY *pAgency = (TAXAGENCY *)pAgencyBuf;

   // Clear output buffer
   if (*apTokens[SCR_T_INSTALLMENT] == '1')
   {
      memset(pDetailBuf, 0, sizeof(TAXDETAIL));
      memset(pAgencyBuf, 0, sizeof(TAXAGENCY));

      // APN
      strcpy(pDetail->Apn, myTrim(apTokens[SCR_T_APN]));

      // BillNum
      sprintf(pDetail->BillNum, "%sA", pDetail->Apn);

      // Tax Year
      sprintf(pDetail->TaxYear, "%d", lTaxYear);

      // Agency 
      strcpy(pDetail->TaxCode, myTrim(apTokens[SCR_T_FUND_CODE]));
      strcpy(pAgency->Code, pDetail->TaxCode);

      // Tax Desc
      strcpy(pAgency->Agency, myTrim(apTokens[SCR_T_FUND_DESC]));

      // Tax Rate
      dTmp =  atof(apTokens[SCR_T_TAXRATE]);
      if (dTmp > 0.0)
      {
         sprintf(pAgency->TaxRate, "%.6f", dTmp);
         sprintf(pDetail->TaxRate, "%.6f", dTmp);
      }

      // Tax Amt
      dTmp =  atof(apTokens[SCR_T_TAXAMT]);
      if (dTmp > 0.0)
      {
         sprintf(pDetail->TaxAmt, "%.2f", dTmp*2);
         iRet = 0;
      }
   }

   return iRet;
}

/**************************** Scr_Load_TaxDetail ******************************
 *
 * Create detail file from SCC_PTAX_TTC_APN_Breakdown_Charges_TaxYear.txt and import into SQL
 * Input file breaks bill into 2 records for each installment.  We have to resort them to group
 * related one together so we can calculate total value for each agency.
 *
 * Return 0 if success.
 *
 *****************************************************************************/

int Scr_Load_TaxDetail(bool bImport)
{
   char     *pTmp, acRec[MAX_RECSIZE];
   char     acItemsFile[_MAX_PATH], acAgencyFile[_MAX_PATH], acTmpFile[_MAX_PATH], acInFile[_MAX_PATH],
            acTmp[1024], acDetail[1024], acAgency[1024];

   int      iRet;
   long     lItems=0, lCnt=0, lAgency=0;
   FILE     *fdItems, *fdAgency, *fdIn;

   LogMsg0("Loading Detail file");

   //GetPrivateProfileString(myCounty.acCntyCode, "Delimiter", "|", acRec, _MAX_PATH, acIniFile); 
   //cDelim = acRec[0];
   GetIniString(myCounty.acCntyCode, "TaxDetail", "", acInFile, _MAX_PATH, acIniFile);
   sprintf(acTmpFile, "%s\\%s\\%s_Detail.srt", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   // Sort on APN, BillNum
   if (chkFileDate(acInFile, acTmpFile) != 2)
   {
      iRet = sortFile(acInFile, acTmpFile, "S(#1,C,A,#6,N,A,#4,C,A) del(124)");   
      if (iRet < 1000)
         return -1;
   }

   // Open input file
   LogMsg("Open Detail file %s", acTmpFile);
   fdIn = fopen(acTmpFile, "r");
   if (fdIn == NULL)
   {
      LogMsg("***** Error opening Detail file: %s\n", acTmpFile);
      return -2;
   }  

   NameTaxCsvFile(acItemsFile, myCounty.acCntyCode, "Items");
   NameTaxCsvFile(acAgencyFile, myCounty.acCntyCode, "Agency");
   sprintf(acTmpFile, "%s\\%s\\%s_Agency.tmp", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);

   // Open Items file
   LogMsg("Create Items file %s", acItemsFile);
   fdItems = fopen(acItemsFile, "w");
   if (fdItems == NULL)
   {
      LogMsg("***** Error creating Items file: %s\n", acItemsFile);
      return -4;
   }

   // Open Agency file
   LogMsg("Create Agency file %s", acTmpFile);
   fdAgency = fopen(acTmpFile, "w");
   if (fdAgency == NULL)
   {
      LogMsg("***** Error creating Agency file: %s\n", acTmpFile);
      return -4;
   }

   // Merge loop 
   while (!feof(fdIn))
   {
      pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdIn);
      if (!pTmp || *pTmp > '9')
         break;

      // Parse input
      iTokens = ParseStringIQ(acRec, cDelim, 256, apTokens);
      if (iTokens < SCR_T_TAXAMT)
      {
         LogMsg("*** Bad record: %s", apTokens[SCR_T_APN]);
         continue;
      }

      // Create Items & Agency record
      iRet = Scr_ParseTaxDetail(acDetail, acAgency);
      if (!iRet)
      {
         // Generate csv line and write to file
         Tax_CreateDetailCsv(acTmp, (TAXDETAIL *)&acDetail[0]);
         fputs(acTmp, fdItems);

         // Forming Agency record
         Tax_CreateAgencyCsv(acTmp, (TAXAGENCY *)&acAgency[0]);
         fputs(acTmp, fdAgency);
         lItems++;
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdIn)
      fclose(fdIn);
   if (fdItems)
      fclose(fdItems);
   if (fdAgency)
      fclose(fdAgency);

   // Import into SQL
   if (bImport && lItems > 90000)
   {
      iRet = doTaxImport(myCounty.acCntyCode, TAX_DETAIL);
      if (!iRet)
      {
         // Dedup Agency file before import
         LogMsg("Dedup Agency file %s to %s", acTmpFile, acAgencyFile);
         lAgency = sortFile(acTmpFile, acAgencyFile, "S(#1,C,A) DUPOUT F(TXT)");
         if (lAgency > 0)
            iRet = doTaxImport(myCounty.acCntyCode, TAX_AGENCY);
      }
   } else
      iRet = 0;

   LogMsg("Total records processed:    %u", lCnt);
   LogMsg("          Items records:    %u", lItems);
   LogMsg("         Agency records:    %u", lAgency);

   return iRet;
}

/******************************** Scr_ParseDelq *****************************
 *
 * Return 0 if delinquent, 1 if not, -1 if error
 *
 *****************************************************************************/

int Scr_ParseDelq(char *pOutbuf, char *pInbuf)
{
   long     lRedDate, lDefDate;
   double	dTaxAmt, dPenAmt;
   char     *pTmp, acTmp[256];

   TAXDELQ  *pOutRec = (TAXDELQ *)pOutbuf;

   // Parse input tax data
   iTokens = ParseStringIQ(pInbuf, cDelim, MAX_FLD_TOKEN, apTokens);
   if (iTokens < SCR_D_SITUSFULLADDR)
   {
      LogMsg("***** Error: bad Tax record for: %.50s (#tokens=%d)", pInbuf, iTokens);
      return -1;
   }

#ifdef _DEBUG
   //if (!memcmp(apTokens[SCR_T_APN], "00104212", 8))
   //   dTaxAmt = 0;
#endif

   // Check for defaulted record
   if (*apTokens[SCR_D_FIRSTINSTDEFAULTED] == '0' && *apTokens[SCR_D_SECONDINSTDEFAULTED] == '0')
      return 1;

   // Clear output buffer
   memset(pOutbuf, 0, sizeof(TAXDELQ));
   lRedDate=lDefDate = 0;

   // APN
   strcpy(pOutRec->Apn, apTokens[SCR_D_PIN]);

   // Tax Status
   if (*apTokens[SCR_D_REDEMPTIONPMTPLANFLAG] == '1')
      pOutRec->DelqStatus[0] = TAX_STAT_ONPAYMENT;
   else
   {
      if (*apTokens[SCR_D_POWERTOSELLFLAG] == '1')
         pOutRec->DelqStatus[0] = TAX_STAT_POWER2SELL;
      else
         pOutRec->DelqStatus[0] = TAX_STAT_UNPAID;
      pOutRec->isDelq[0] = '1';
   }

   // Updated date
   sprintf(pOutRec->Upd_Date, "%d", lLastTaxFileDate);

   // Tax Year
   strcpy(pOutRec->TaxYear, apTokens[SCR_D_TAXYEAR]);

   // Default amount
   dTaxAmt = atof(apTokens[SCR_D_FIRSTINSTAMOUNT])+atof(apTokens[SCR_D_SECONDINSTAMOUNT]);
   if (dTaxAmt > 0.0)
      sprintf(pOutRec->Def_Amt, "%.2f", dTaxAmt);

   // Penalty
   dPenAmt = atof(apTokens[SCR_D_FIRSTINSTPENALTY])+atof(apTokens[SCR_D_SECONDINSTPENALTY]);
   if (dPenAmt > 0.0)
      sprintf(pOutRec->Pen_Amt, "%.2f", dPenAmt);

   // Default date
   pTmp = dateConversion(apTokens[SCR_D_FIRSTINSTDEFAULTDATE], acTmp, YYYY_MM_DD);
   if (pTmp)
      strcpy(pOutRec->Def_Date, acTmp);

   // Redemption date
   if (*apTokens[SCR_D_FIRSTINSTDEFAULTED] == '0' && *apTokens[SCR_D_FIRSTINSTPAYMENTDATE] > '0')
   {
      pTmp = dateConversion(apTokens[SCR_D_FIRSTINSTPAYMENTDATE], acTmp, YYYY_MM_DD);
      if (pTmp)
         strcpy(pOutRec->Red_Date, acTmp);
      if (*apTokens[SCR_D_FIRSTINSTAMOUNT] > '0')
         strcpy(pOutRec->Red_Amt, apTokens[SCR_D_FIRSTINSTAMOUNT]);      
   }

   return 0;
}

/**************************** Scr_Load_TaxDelq *****************************
 *
 * Load SCC_GetDefaultedRedeemedInfoForTaxServices.txt
 * Create TaxDelq
 *
 ***************************************************************************/

int Scr_Load_TaxDelq(bool bImport)
{
   char     acOutFile[_MAX_PATH], acDelqFile[_MAX_PATH], acBuf[2048], acRec[2048], *pTmp;
   int      iRet, lCnt=0, lOut=0;
   FILE     *fdIn, *fdOut;

   LogMsg0("Loading Tax Delinquent");

   // Concat file1 & file2
   GetIniString(myCounty.acCntyCode, "TaxDelq", "", acDelqFile, _MAX_PATH, acIniFile);
   lLastTaxFileDate = getFileDate(acDelqFile);

   // Open input file
   LogMsg("Open tax delinquent file %s", acDelqFile);
   fdIn = fopen(acDelqFile, "r");
   if (fdIn == NULL)
   {
      LogMsg("***** Error opening Secured tax file: %s\n", acDelqFile);
      return -2;
   }  
   // Skip header
   pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdIn);


   // Open output file
   NameTaxCsvFile(acOutFile, myCounty.acCntyCode, "Delq");
   LogMsg("Open output file %s", acOutFile);
   fdOut = fopen(acOutFile, "w");
   if (fdOut == NULL)
   {
      LogMsg("***** Error creating output file: %s\n", acOutFile);
      return -4;
   }

   // Merge loop 
   while (!feof(fdIn))
   {
      pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdIn);
      if (!pTmp || *pTmp < ' ')
         break;

      // Create new Delq record
      iRet = Scr_ParseDelq(acBuf, acRec);
      if (!iRet)
      {
         // Create delimited record
         Tax_CreateDelqCsv(acRec, (TAXDELQ *)&acBuf);

         // Output record			
         lOut++;
         fputs(acRec, fdOut);
      } else if (bDebug)
      {
         LogMsg("---> Drop record %d [%.40s]", lCnt, acRec); 
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdIn)
      fclose(fdIn);
   if (fdOut)
      fclose(fdOut);

   LogMsg("Total records processed:    %u", lCnt);
   LogMsg("Total output records:       %u", lOut);

   // Rename Delq file
   sprintf(acRec, "%s.%s", acDelqFile, acToday);
   if (!_access(acRec, 0))
      DeleteFile(acRec);
   rename(acDelqFile, acRec);

   // Import into SQL
   if (bImport && lOut > 5)
      iRet = doTaxImport(myCounty.acCntyCode, TAX_DELINQUENT);
   else
      iRet = 0;

   return iRet;
}

/********************************* Scr_ExtrVal ******************************
 *
 * Extract values from tax roll SCC_GetAnnualRollForTaxServices.txt
 * This file combines LivingImprovements into Improvements and has no Exemption.
 *
 ****************************************************************************/

int Scr_CreateValueRec(char *pOutbuf)
{
   char     acTmp[256];
   LONGLONG lTmp;
   int      iTmp;
   LIENEXTR *pLienRec = (LIENEXTR *)pOutbuf;

   // Clear output buffer
   memset(pOutbuf, ' ', sizeof(LIENEXTR));

   // Start copying data
   memcpy(pLienRec->acApn, apTokens[SCR_T_APN], strlen(apTokens[SCR_T_APN]));

   // Year assessed
   memcpy(pLienRec->acYear, myCounty.acYearAssd, 4);

   // Land
   long lLand = atol(apTokens[SCR_T_LANDVALUE]);
   if (lLand > 0)
   {
      iTmp = sprintf(acTmp, "%*u", sizeof(pLienRec->acLand), lLand);
      memcpy(pLienRec->acLand, acTmp, iTmp);
   }

   // Improve
   long lImpr = atol(apTokens[SCR_T_IMPROVEMENTSVALUE]);
   if (lImpr > 0)
   {
      iTmp = sprintf(acTmp, "%*u", sizeof(pLienRec->acImpr), lImpr);
      memcpy(pLienRec->acImpr, acTmp, iTmp);
   }

   // Gross total
   lTmp = (lLand+lImpr);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*u", SIZ_LIEN_FIXT, lTmp);
      memcpy(pLienRec->acGross, acTmp, SIZ_LIEN_FIXT);
   }

   // Ratio
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*u", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
      memcpy(pLienRec->acRatio, acTmp, SIZ_RATIO);
   }

   pLienRec->LF[0] = '\n';
   pLienRec->LF[1] = 0;
   return 0;
}

int Scr_ExtrVal()
{
   char      *pTmp, acBuf[1024], acRec[MAX_RECSIZE], acTmpFile[_MAX_PATH];
   int       iRet;
   long      lOut=0, lCnt=0;
   FILE      *fdOut, *fdIn;

   LogMsg0("Extracting final values from equalized file.");

   // Open input file
   GetIniString(myCounty.acCntyCode, "TaxRoll", "", acTmpFile, _MAX_PATH, acIniFile);
   LogMsg("Open tax roll file %s", acTmpFile);
   fdIn = fopen(acTmpFile, "r");
   if (fdIn == NULL)
   {
      LogMsg("***** Error opening tax roll file: %s\n", acTmpFile);
      return -2;
   }  

   // Open out file
   sprintf(acTmpFile, acValueTmpl, myCounty.acCntyCode, myCounty.acCntyCode);
   LogMsg("Open file %s for output", acTmpFile);
   fdOut = fopen(acTmpFile, "w");
   if (fdOut == NULL)
   {
      LogMsg("***** Error creating output file: %s\n", acTmpFile);
      return -4;
   }

   // Drop header
   pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdIn);

   // Start loop 
   while (!feof(fdIn))
   {
      pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdIn);
      if (!pTmp || *pTmp > '9')
         break;

      // Parse input record
      iTokens = ParseStringIQ(acRec, cDelim, MAX_FLD_TOKEN, apTokens);
      if (iTokens < SCR_T_SITUSFULLADDRESS)
      {
         LogMsg("*** Bad record: %s", acRec);
         continue;
      }

      // Create new base record
      iRet = Scr_CreateValueRec(acBuf);
      if (!iRet)
      {
         fputs(acBuf, fdOut);
         lOut++;
      } else
      {
         LogMsg("---> Drop record %d ", lCnt); 
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   LogMsg("Total records processed: %u", lCnt);
   LogMsg("Total records output:    %u", lOut);

   // Close files
   if (fdIn)
      fclose(fdIn);
   if (fdOut)
      fclose(fdOut);

   return iRet;
}

/*********************************** loadScr ********************************
 *
 * Old Input files:
 *    - AssessmentRollEbcdic.TXT    (roll file, ebcdic)
 *    - Scr_Utl_roll.txt            (public prop., ebcdic)
 *    - TransfersEbcdic.TXT         (sale/transfer file, ebcdic)
 *    - CD_DataMonthlySantaCruz.mdb (attribute file)
 *
 * New Input files:
 *    - SCC_GetAnnualRollForTaxServices.txt
 *    - SCC_GetDefaultedRedeemedInfoForTaxServices.txt
 *    - SCC_ParcelQuest_Get_AssessmentValues_Exemptions.txt
 *    - SCC_ParcelQuest_Get_Characteristics.txt
 *    - SCC_ParcelQuest_Get_Etals.txt
 *    - SCC_ParcelQuest_Get_ParcelStatus.txt
 *    - SCC_ParcelQuest_Get_PrimaryOwnerMailingAddress.txt
 *    - SCC_ParcelQuest_Get_PrimaryOwner_TRA_UseCode_LegalDescr.txt
 *    - SCC_ParcelQuest_Get_Redescriptions.txt
 *    - SCC_ParcelQuest_Get_SalesTransfers.txt
 *    - SCC_ParcelQuest_Get_SitusAddresses.txt
 *    - SCC_ParcelQuest_Get_TaxYearTaxesDue_Paid.txt
 *    - SCC_ParcelQuest_Get_Unsecured_Values.txt
 *    - SCC_TaxYearTaxesDue_Flds.txt
 *
 * Following are tasks to be done here:
 *    - Merge/sort roll file with SCR_UTIL if not already included
 *    - Normal update: LoadOne -U -Xs[i] -Xa
 *    - Load Lien: LoadOne -L -Xs[i] -Xa -Xl
 *    - Extract attr only: LoadOne -Xa
 *
 ****************************************************************************/

int loadScr(int iSkip)
{
   int   iRet=0, iSrcLayout;
   char  acDefFile[_MAX_PATH], acSaleTmp[_MAX_PATH], acTmpFile[_MAX_PATH], *pTmp;
   //char acUnRollFile[_MAX_PATH], acRollTmp[_MAX_PATH];

   iApnLen = myCounty.iApnLen;

   // Determine which input format is in use
   iSrcLayout = GetPrivateProfileInt(myCounty.acCntyCode, "SrcLayout", 0, acIniFile);

   // Loading tax file
   if (iLoadTax == TAX_LOADING)              // -T or -Lt
   {
      //TC_SetDateFmt(MM_DD_YYYY_1);
      //iRet = Load_TC(myCounty.acCntyCode, bTaxImport);
      //iRet = GetIniString(myCounty.acCntyCode, "TaxDist", "", acTmpFile, _MAX_PATH, acIniFile);
      //if (iRet > 0)
      //   iRet = LoadTaxCodeTable(acTmpFile);

      iRet = Scr_Load_TaxRoll(bTaxImport);
      if (!iRet && lLastTaxFileDate > 0)
      {
         // Load Items and Agency
         iRet = Scr_Load_TaxDetail(bTaxImport);

         if (!iRet)
         {
            // Update total tax rate
            iRet = doUpdateTotalRate(myCounty.acCntyCode, "B");

            iRet = Scr_Load_TaxDelq(bTaxImport);
            if (!iRet)
               iRet = updateDelqFlag(myCounty.acCntyCode);
         }
      } 
   }

   if (!iLoadFlag && !lOptExtr)
      return iRet;

   // Extract final value
   if (lOptExtr & EXTR_FVAL)                       // -Xf
      iRet = Scr_ExtrVal();

   // Load Quality lookup tables
   if (!iSrcLayout)
      GetIniString("System", "LookUpTbl", "", acTmpFile, _MAX_PATH, acIniFile);
   else
      GetIniString(myCounty.acCntyCode, "LookUpTbl", "", acTmpFile, _MAX_PATH, acIniFile);
   iRet = LoadLUTable((char *)&acTmpFile[0], "[Quality]", &tblAttr[0], MAX_ATTR_ENTRIES);
   if (!iRet)
   {
      LogMsg("***** Error Looking for table [Quality] in %s", acIniFile);
      return 1;
   }

   // Extract lien values
   if (iLoadFlag & EXTR_LIEN)                   // -Xl
   {
      if (!iSrcLayout)
         iRet = Scr_ExtrLien();
      else if (lLienYear < 2023)
         iRet = Scr_ExtrLien2();
      else if (lLienYear == 2023)
         iRet = Scr_ExtrLienX();
      else
         iRet = Scr_ExtrLien3();
   }

   // Remove 07/30/2023
   // Merge lien file with public property file
   //if (!iSrcLayout && (iLoadFlag & (LOAD_LIEN|LOAD_UPDT)))
   //{
   //   GetIniString(myCounty.acCntyCode, "UtlFile", "", acTmpFile, _MAX_PATH, acIniFile);
   //   if (!_access(acTmpFile, 0) )
   //   {
   //      strcat(acRollFile, "+");
   //      strcat(acRollFile, acTmpFile);
   //      LogMsgD("Merge roll file with public property file %s", acRollFile);
   //      if (iLoadFlag & LOAD_LIEN)
   //         sprintf(acRollTmp, "%s\\%s\\SCR_Lien.bin", acTmpPath, myCounty.acCntyCode);
   //      else
   //         sprintf(acRollTmp, "%s\\%s\\SCR_Roll.bin", acTmpPath, myCounty.acCntyCode);
   //      iRet = sortFile(acRollFile, acRollTmp, "S(1,11,C,A) F(FIX,338) DUPOUT(1,11) ALT(C:\\TOOLS\\EBCDIC.ALT)");
   //      if (iRet <= 0)
   //      {
   //         LogMsg("***** Error merging roll file");
   //         return 1;
   //      }
   //      strcpy(acRollFile, acRollTmp);
   //   }
   //}

   // Translate to ascii
   //if (!iSrcLayout && (iLoadFlag & (LOAD_LIEN|LOAD_UPDT|EXTR_LIEN)))
   //{
   //   GetIniString(myCounty.acCntyCode, "RollDef", "", acDefFile, _MAX_PATH, acIniFile);
   //   if (_access(acDefFile, 0))
   //   {
   //      LogMsg("***** Error: missing definition file %s", acDefFile);
   //      return 1;
   //   }
   //   LogMsg("Loading roll definition file %s", acDefFile);
   //   iRet = LoadDefFile(acDefFile);
   //   if (!iRet)
   //   {
   //      LogMsg("***** Error: bad definition file %s", acDefFile);
   //      return 2;
   //   }
   //   strcpy(acRollTmp, acRollFile);
   //   if (iLoadFlag & LOAD_LIEN)
   //      sprintf(acRollFile, "%s\\%s\\SCR_Lien.asc", acTmpPath, myCounty.acCntyCode);
   //   else
   //      sprintf(acRollFile, "%s\\%s\\SCR_Roll.asc", acTmpPath, myCounty.acCntyCode);
   //   LogMsg("Translate %s to Ascii %s", acRollTmp, acRollFile);
   //   iRet = F_Ebc2Asc(acRollTmp, acRollFile, acDefFile, 0);
   //}

   if (lOptProp8 & MYOPT_EXT)                   // -X8 Extract prop8 flag to text file
   {
      iRet = Scr_ExtractProp8(acRollFile);
   }

   // Extract characteristic data
   if (iLoadFlag & EXTR_ATTR)                   // -Xa
   {
      if (!iSrcLayout)
         iRet = Scr_ExtrAttr();
      else
         iRet = Scr_ConvertStdChar(acCharFile);
   }

   // Extract sale data
   if (iLoadFlag & EXTR_SALE)                   // -Xs
   {
      if (!iSrcLayout)
      {
         // Open sale file
         GetIniString(myCounty.acCntyCode, "SaleDef", "", acDefFile, _MAX_PATH, acIniFile);
         if (_access(acDefFile, 0))
         {
            LogMsg("***** Error: missing definition file %s", acDefFile);
            return 1;
         }

         LogMsg("Loading sale definition file %s", acDefFile);
         iRet = LoadDefFile(acDefFile);
         if (!iRet)
         {
            LogMsg("***** Error: bad definition file %s", acDefFile);
            return 2;
         }

         strcpy(acSaleTmp, acSaleFile);
         pTmp = strrchr(acSaleFile, '\\');
         strcpy(++pTmp, "SCR_SALE.ASC");
         LogMsg("Translate %s to Ascii %s", acSaleTmp, acSaleFile);
         iRet = F_Ebc2Asc(acSaleTmp, acSaleFile, acDefFile, 0);

         // Extract sale
         iRet = Scr_ExtrSale(acSaleFile);
      } else
         iRet = Scr_ExtrSale2(acSaleFile);

      if (!iRet)
         iLoadFlag |= MERG_CSAL;
   }

   // Merge roll file
   if (iLoadFlag & LOAD_LIEN)                      // -L
   {
      if (lLienYear >= 2023)
         iRet = Scr_Load_LDR3(iSkip);
      else if (!iSrcLayout)
         iRet = Scr_Load_LDR(iSkip);
      else 
         iRet = Scr_Load_LDR2(iSkip);
   }

   if (iLoadFlag & LOAD_UPDT)                      // -U
   {
      if (!iSrcLayout)
         iRet = Scr_Load_Roll(iSkip);
      else
         iRet = Scr_Load_Roll2(iSkip);
   }

   // Apply cum sale file to R01
   if (!iRet && (iLoadFlag & MERG_CSAL) )          // -Ms
   {
      // Apply Scr_Sale.sls to R01 file
      sprintf(acCSalFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "sls");
      if (!iSrcLayout)
         iRet = ApplyCumSale(iSkip, acCSalFile, true, SALE_USE_SCUPDXFR);
      else
         iRet = ApplyCumSale(iSkip, acCSalFile, false, SALE_USE_SCUPDXFR);
   }

   // Fix TRA
   if (!iRet && (lOptMisc & M_OPT_FIXTRA))
      iRet = PQ_FixR01(myCounty.acCntyCode, PQ_FIX_TRA, 0, true);

   return iRet;
}