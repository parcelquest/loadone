#ifndef  _MERGESLO_H_
#define  _MERGESLO_H_

#define  SLO_LAND    0
#define  SLO_IMPR    1
#define  SLO_PP      2
#define  SLO_FE      3
#define  SLO_MH      4

/*
#define  SLO_CHAR_APN            0
#define  SLO_CHAR_S_NUM          1
#define  SLO_CHAR_S_DIR          2
#define  SLO_CHAR_S_NAME         3
#define  SLO_CHAR_S_TYPE         4
#define  SLO_CHAR_S_UNIT         5
#define  SLO_CHAR_S_COMM         6
#define  SLO_CHAR_USECODE1       7
#define  SLO_CHAR_USEDESC1       8
#define  SLO_CHAR_USECODE2       9
#define  SLO_CHAR_USEDESC2       10
#define  SLO_CHAR_USECODE3       11
#define  SLO_CHAR_USEDESC3       12
#define  SLO_CHAR_ZONING         13
#define  SLO_CHAR_NOTUSE         14
#define  SLO_CHAR_COMM_DESC      15
#define  SLO_CHAR_SIZECODE       16
#define  SLO_CHAR_LOTSIZE        17
#define  SLO_CHAR_TOPOGRAPHY     18
#define  SLO_CHAR_UTILS          19
#define  SLO_CHAR_ACCESS         20
#define  SLO_CHAR_RESIDENT       21
#define  SLO_CHAR_REMOVED        22
#define  SLO_CHAR_CLASS          23
#define  SLO_CHAR_BLDGSQFT       24
#define  SLO_CHAR_ADDLSQFT       25
#define  SLO_CHAR_TOTALSQFT      26
#define  SLO_CHAR_ROOMS          27
#define  SLO_CHAR_BEDS           28
#define  SLO_CHAR_BATHS          29
#define  SLO_CHAR_YRBLT          30
#define  SLO_CHAR_LEVELS         31
#define  SLO_CHAR_HEATING        32
#define  SLO_CHAR_COOLING        33
#define  SLO_CHAR_FP             34
#define  SLO_CHAR_PARKTYPE       35
#define  SLO_CHAR_PARKSPCS       36
*/
#define  SLO_CSIZ_CLASS          8
#define  SLO_CSIZ_COMM           8
#define  SLO_CSIZ_TOPO           28
#define  SLO_CSIZ_ACCESS         18
#define  SLO_CSIZ_RESID          2
#define  SLO_CSIZ_LEVELS         2

typedef struct _tSloCharRec
{  // 256-bytes record
   char  Apn[SIZ_APN_S];
   char  S_StrNum[SIZ_S_STRNUM];          // 15
   char  S_StrDir[SIZ_S_DIR];             //
   char  S_StrName[SIZ_S_STREET];         // 24
   char  S_StrSfx[SIZ_S_SUFF];            // 48
   char  S_Unit[SIZ_S_UNITNO];            //
   char  S_City[SIZ_S_CITY];              // 59
   char  UseCode1[SIZ_USE_CO];            // 83
   char  Zoning[SIZ_ZONE];                // 91
   char  LotSqft[SIZ_LOT_SQFT];           // 101
   char  LotAcres[SIZ_LOT_ACRES];         // 110
   char  QualityClass[SLO_CSIZ_CLASS];    // 119
   char  Year_Blt[SIZ_YR_BLT];            // 127
   char  BldgSqft[SIZ_BLDG_SF];           // 131
   char  AddlSqft[SIZ_BLDG_SF];           // 140
   char  BuildingSize[SIZ_BLDG_SF];       // 149
   char  ParkType;                        // 158
   char  ParkSpaces[SIZ_PARK_SPACE];      // 159
   char  Heating;                         // 165
   char  Cooling;                         // 166
   char  Rooms[SIZ_ROOMS];                // 167
   char  Bedrooms[SIZ_BEDS];              // 170
   char  FullBaths[SIZ_BATH_F];           // 172
   char  HalfBaths[SIZ_BATH_H];           // 174
   char  Fireplaces[SIZ_FIRE_PL];         // 175
   char  Removed;                         // 178
   char  HasGas;                          // 179
   char  HasElec;                         // 180
   char  HasSewer;                        // 181
   char  HasWater;                        // 182
   char  CommDesc[SLO_CSIZ_COMM];         // 183
   char  UseCode2[SIZ_USE_CO];            // 191
   char  Topography[SLO_CSIZ_TOPO];       // 199
   char  Access[SLO_CSIZ_ACCESS];         // 227
   char  Resident[SLO_CSIZ_RESID];        // 245
   char  Levels[SLO_CSIZ_LEVELS];         // 247
   char  filler[6];                       // 249
   char  CRLF[2];                         // 255
} SLO_CHAR;

USEXREF  Slo_UseTbl[] =
{
   "001","826",
   "018","902",
   "019","776",
   "021","776",
   "022","776",
   "023","776",
   "025","115",
   "026","115",
   "027","115",
   "030","776",
   "031","780",
   "033","776",
   "035","903",
   "038","901",
   "039","901",
   "040","901",
   "041","428",
   "042","776",
   "050","776",
   "051","794",
   "052","794",
   "053","794",
   "060","177",
   "061","177",
   "062","177",
   "063","177",
   "064","177",
   "065","177",
   "066","177",
   "071","794",
   "072","794",
   "073","794",
   "080","177",
   "081","177",
   "082","177",
   "083","177",
   "084","177",
   "085","177",
   "086","177",
   "100","120",
   "101","826",
   "102","826",
   "103","826",
   "104","176",
   "105","176",
   "106","176",
   "107","176",
   "108","176",
   "109","176",
   "110","101",
   "111","101",
   "115","101",
   "120","107",
   "121","121",
   "125","108",
   "130","115",
   "131","115",
   "132","115",
   "133","115",
   "134","115",
   "135","114",
   "136","114",
   "137","114",
   "138","128",
   "139","115",
   "140","904",
   "145","626",
   "150","126",
   "160","101",
   "161","101",
   "162","101",
   "163","101",
   "164","101",
   "165","101",
   "166","101",
   "170","170",
   "171","170",
   "172","170",
   "173","170",
   "174","170",
   "175","170",
   "176","170",
   "200","120",
   "201","103",
   "202","104",
   "203","105",
   "205","123",
   "209","102",
   "210","129",
   "215","129",
   "216","129",
   "217","129",
   "218","129",
   "219","129",
   "222","124",
   "223","125",
   "230","130",
   "231","131",
   "235","117",
   "300","835",
   "305","248",
   "309","132",
   "310","249",
   "311","250",
   "321","211",
   "322","211",
   "324","216",
   "325","212",
   "326","200",
   "331","316",
   "332","318",
   "333","300",
   "334","300",
   "335","300",
   "336","300",
   "337","300",
   "338","305",
   "339","316",
   "341","114",
   "345","114",
   "346","114",
   "351","227",
   "352","226",
   "355","226",
   "361","110",
   "362","110",
   "363","110",
   "364","110",
   "371","110",
   "372","110",
   "373","110",
   "374","110",
   "379","110",
   "380","233",
   "381","234",
   "382","235",
   "383","236",
   "385","234",
   "389","240",
   "390","312",
   "400","830",
   "401","291",
   "402","292",
   "403","293",
   "404","905",
   "405","294",
   "406","906",
   "407","907",
   "411","251",
   "412","251",
   "415","252",
   "420","600",
   "421","611",
   "422","607",
   "423","624",
   "424","610",
   "425","601",
   "426","625",
   "427","626",
   "428","600",
   "430","829",
   "435","226",
   "440","242",
   "509","132",
   "511","402",
   "512","403",
   "515","425",
   "520","404",
   "522","406",
   "530","420",
   "531","421",
   "532","420",
   "533","420",
   "534","420",
   "535","626",
   "540","401",
   "542","401",
   "543","401",
   "544","401",
   "545","401",
   "546","401",
   "550","401",
   "552","401",
   "555","401",
   "556","401",
   "591","776",
   "600","827",
   "611","537",
   "612","537",
   "613","500",
   "614","500",
   "615","500",
   "616","500",
   "619","500",
   "620","500",
   "630","514",
   "631","510",
   "632","502",
   "636","411",
   "637","908",
   "640","515",
   "650","514",
   "660","501",
   "661","501",
   "662","500",
   "691","500",
   "692","776",
   "802","802",
   "810","726",
   "820","807",
   "850","840",
   "851","840",
   "852","840",
   "853","840",
   "854","840",
   "855","840",
   "856","840",
   "857","840",
   "858","840",
   "860","801",
   "861","815",
   "862","776",
   "863","776",
   "", ""
};

typedef struct t_SloCumSale
{
   char  acApn[8];
   char  acDocDate[8];
   char  acDocNum[8];
   char  acStamp[10];         // 9(7).99
   char  acSalePrice[9];
   char  acSaleCode[1];
   char  acDocType[2];
   char  acAdjSalePrice[9];
   char  acPriceStatus[1];    // C=CONF,A=ADJ,P=PRELIM
   char  acPctTransfer[3];
   char  acPctSign[1];
   char  acEquityCode[1];
   char  acDirectEnrlFlg[1];  // Y = DIRECT ENROLL, X = REJECTED
   char  acPctDown[4];
   char  acNeighborCode[3];
   char  acUseCode[4];
   char  filler[7];
} SLO_CSALE;

/* New format 2/12/2009 */

// Sale file
#define  SLO_SALE_SEQ         0
#define  SLO_SALE_APN         1
#define  SLO_SALE_FIL1        2
#define  SLO_SALE_DOCNUM      3
#define  SLO_SALE_RECDATE     4
#define  SLO_SALE_EFFDATE     5
#define  SLO_SALE_DOCTYPE     6
#define  SLO_SALE_CODE        7
#define  SLO_SALE_DESC        8
#define  SLO_SALE_DOCTAX      9
#define  SLO_SALE_SALEPRICE   10
#define  SLO_SALE_GTOR_NAME1  11
#define  SLO_SALE_GTOR_PCT1   12
#define  SLO_SALE_GTEE_NAME1  13
#define  SLO_SALE_GTEE_PCT1   14
#define  SLO_SALE_GTOR_NAME2  15
#define  SLO_SALE_GTOR_PCT2   16
#define  SLO_SALE_GTEE_NAME2  17
#define  SLO_SALE_GTEE_PCT2   18
#define  SLO_SALE_GTOR_NAME3  19
#define  SLO_SALE_GTOR_PCT3   20
#define  SLO_SALE_GTEE_NAME3  21
#define  SLO_SALE_GTEE_PCT3   22
#define  SLO_SALE_GTOR_NAME4  23
#define  SLO_SALE_GTOR_PCT4   24
#define  SLO_SALE_GTEE_NAME4  25
#define  SLO_SALE_GTEE_PCT4   26
#define  SLO_SALE_GTOR_NAME5  27
#define  SLO_SALE_GTOR_PCT5   28
#define  SLO_SALE_GTEE_NAME5  29
#define  SLO_SALE_GTEE_PCT5   30
#define  SLO_SALE_GTOR_NAME6  31
#define  SLO_SALE_GTOR_PCT6   32
#define  SLO_SALE_GTEE_NAME6  33
#define  SLO_SALE_GTEE_PCT6   34

typedef struct _tSloCSale
{  // 1024-bytes
   SCSAL_REC s;
   char  Grantor[4][SALE_SIZ_NAME];       // 513
   char  Grantee[6][SALE_SIZ_NAME];       // 713
   char  NameCnt[2];                      // 1013
   char  filler [8];                      // 1015
   char  CRLF[2];                         // 1023
} SLO_CSAL;

// Roll file - Pre 20110617
/*
#define  SLO_ROLL_SEQ         0
#define  SLO_ROLL_APN         1
#define  SLO_ROLL_OWNER       2
#define  SLO_ROLL_ASSESSEE    3
#define  SLO_ROLL_CAREOF      4
#define  SLO_ROLL_M_ADDR1     5
#define  SLO_ROLL_M_ADDR2     6
#define  SLO_ROLL_M_ADDR3     7
#define  SLO_ROLL_M_CITY      8
#define  SLO_ROLL_M_ST        9
#define  SLO_ROLL_M_ZIP       10
#define  SLO_ROLL_M_ZIP4      11
#define  SLO_ROLL_S_HSENO     12
#define  SLO_ROLL_S_DIR       13
#define  SLO_ROLL_S_STREET    14
#define  SLO_ROLL_S_TYPE      15
#define  SLO_ROLL_S_UNIT      16
#define  SLO_ROLL_S_COMMUNITY 17
#define  SLO_ROLL_LEGAL       18
#define  SLO_ROLL_STATUS_DT   19
#define  SLO_ROLL_BASE_DT     20
#define  SLO_ROLL_TAXCODE     21
#define  SLO_ROLL_LAND        22
#define  SLO_ROLL_IMPR        23
#define  SLO_ROLL_PERSPROP    24
#define  SLO_ROLL_FIXTURE     25
#define  SLO_ROLL_HOEXE       26

#define  SLO_ROLL_OTHEXE      27
#define  SLO_ROLL_NET         28

#define  SLO_ROLL_TRA         29
#define  SLO_ROLL_PRI_LUC     30
#define  SLO_ROLL_PRI_LUCDESC 31
#define  SLO_ROLL_2ND_LUC     32
#define  SLO_ROLL_2ND_LUCDESC 33
#define  SLO_ROLL_3RD_LUC     34
#define  SLO_ROLL_3RD_LUCDESC 35
#define  SLO_ROLL_PRI_ZONE    36

#define  SLO_ROLL_COMM_DESC   37
#define  SLO_ROLL_NBRHOOD     38

#define  SLO_ROLL_SIZE_TYPE   39
#define  SLO_ROLL_LOTSIZE     40
#define  SLO_ROLL_TOPOGRAPHY  41
#define  SLO_ROLL_UTILITY     42
#define  SLO_ROLL_ACCESS      43
#define  SLO_ROLL_RESIDENT    44
#define  SLO_ROLL_REMOVED     45
#define  SLO_ROLL_CLASS       46
#define  SLO_ROLL_BLDGSQFT    47
#define  SLO_ROLL_ADDLSQFT    48
#define  SLO_ROLL_TOTALSQFT   49
#define  SLO_ROLL_ROOMS       50
#define  SLO_ROLL_BEDS        51
#define  SLO_ROLL_BATHS       52
#define  SLO_ROLL_YRBLT       53
#define  SLO_ROLL_LEVELS      54
#define  SLO_ROLL_HEATING     55
#define  SLO_ROLL_COOLING     56
#define  SLO_ROLL_FP          57
#define  SLO_ROLL_PARKTYPE    58
#define  SLO_ROLL_PARKSPACE   59
*/

// Since 20110617
#define  SLO_ROLL_SEQ         0
#define  SLO_ROLL_APN         1
#define  SLO_ROLL_OWNER       2
#define  SLO_ROLL_ASSESSEE    3
#define  SLO_ROLL_CAREOF      4
#define  SLO_ROLL_M_ADDR1     5
#define  SLO_ROLL_M_ADDR2     6
#define  SLO_ROLL_M_ADDR3     7
#define  SLO_ROLL_M_CITY      8
#define  SLO_ROLL_M_ST        9
#define  SLO_ROLL_M_ZIP       10
#define  SLO_ROLL_M_ZIP4      11

#define  SLO_ROLL_S_HSENO     12
#define  SLO_ROLL_S_DIR       13
#define  SLO_ROLL_S_STREET    14
#define  SLO_ROLL_S_TYPE      15
#define  SLO_ROLL_S_UNIT      16
#define  SLO_ROLL_S_COMMUNITY 17
#define  SLO_ROLL_2_HSENO     18
#define  SLO_ROLL_2_DIR       19
#define  SLO_ROLL_2_STREET    20
#define  SLO_ROLL_2_TYPE      21
#define  SLO_ROLL_2_UNIT      22
#define  SLO_ROLL_2_COMMUNITY 23
#define  SLO_ROLL_3_HSENO     24
#define  SLO_ROLL_3_DIR       25
#define  SLO_ROLL_3_STREET    26
#define  SLO_ROLL_3_TYPE      27
#define  SLO_ROLL_3_UNIT      28
#define  SLO_ROLL_3_COMMUNITY 29
#define  SLO_ROLL_4_HSENO     30
#define  SLO_ROLL_4_DIR       31
#define  SLO_ROLL_4_STREET    32
#define  SLO_ROLL_4_TYPE      33
#define  SLO_ROLL_4_UNIT      34
#define  SLO_ROLL_4_COMMUNITY 35

#define  SLO_ROLL_LEGAL       36
#define  SLO_ROLL_STATUS_DT   37
#define  SLO_ROLL_BASE_DT     38
#define  SLO_ROLL_TAXCODE     39
#define  SLO_ROLL_LAND        40
#define  SLO_ROLL_IMPR        41
#define  SLO_ROLL_PERSPROP    42
#define  SLO_ROLL_FIXTURE     43
#define  SLO_ROLL_HOEXE       44
#define  SLO_ROLL_OTHEXE      45
#define  SLO_ROLL_NET         46
#define  SLO_ROLL_TRA         47
#define  SLO_ROLL_PRI_LUC     48
#define  SLO_ROLL_PRI_LUCDESC 49
#define  SLO_ROLL_2ND_LUC     50
#define  SLO_ROLL_2ND_LUCDESC 51
#define  SLO_ROLL_3RD_LUC     52
#define  SLO_ROLL_3RD_LUCDESC 53
#define  SLO_ROLL_PRI_ZONE    54
#define  SLO_ROLL_COMM_DESC   55
#define  SLO_ROLL_NBRHOOD     56
#define  SLO_ROLL_SIZE_TYPE   57
#define  SLO_ROLL_LOTSIZE     58
#define  SLO_ROLL_TOPOGRAPHY  59
#define  SLO_ROLL_UTILITY     60
#define  SLO_ROLL_ACCESS      61
#define  SLO_ROLL_RESIDENT    62
#define  SLO_ROLL_STATUS      63
#define  SLO_ROLL_CLASS       64
#define  SLO_ROLL_BLDGSQFT    65
#define  SLO_ROLL_ADDLSQFT    66
#define  SLO_ROLL_TOTALSQFT   67
#define  SLO_ROLL_ROOMS       68
#define  SLO_ROLL_BEDS        69
#define  SLO_ROLL_BATHS       70
#define  SLO_ROLL_YRBLT       71
#define  SLO_ROLL_LEVELS      72
#define  SLO_ROLL_HEATING     73
#define  SLO_ROLL_COOLING     74
#define  SLO_ROLL_FP          75
#define  SLO_ROLL_PARKTYPE    76
#define  SLO_ROLL_PARKSPACE   77
#define  SLO_ROLL_ALTAPN      78

// 1920SecRollDataFull.txt
#define  SLO_L_APN            0
#define  SLO_L_ROLL_YR        1
#define  SLO_L_VALUE_DATE     2
#define  SLO_L_TRA            3
#define  SLO_L_OSC            4
#define  SLO_L_ASSESSEE       5
#define  SLO_L_CARE_OF        6
#define  SLO_L_M_ADDR_1       7
#define  SLO_L_M_ADDR_2       8
#define  SLO_L_M_ADDR_3       9
#define  SLO_L_M_CITY         10
#define  SLO_L_M_STATE        11
#define  SLO_L_M_ZIP          12
#define  SLO_L_LEGAL_DESC     13
#define  SLO_L_BASE_DATE      14
#define  SLO_L_SENIOR_FLAG    15
#define  SLO_L_ENROLL_FLAG    16
#define  SLO_L_LAND           17
#define  SLO_L_IMPR           18

// repeat 15 times
#define  SLO_L_VALUE_CODE1    19
#define  SLO_L_VALUE_AMT1     20
#define  SLO_L_VALUE_FLAGS1   21

// Repeat 10 times
#define  SLO_L_EXEMP_CODE1    64
#define  SLO_L_EXEMP_AMT1     65

#define  SLO_L_S_STRNUM       84
#define  SLO_L_S_DIR          85
#define  SLO_L_S_STREET       86
#define  SLO_L_S_TYPE         87
#define  SLO_L_S_UNIT         88
#define  SLO_L_TAX_CODE       89

// 2024 LDR has only 91 fields
//#define  SLO_L_PRIM_ZONING    90
//#define  SLO_L_SECND_ZONING   91
//#define  SLO_L_LAND_USE       92
//#define  SLO_L_2ND_LAND_USE_1 93
//#define  SLO_L_2ND_LAND_USE_2 94
//#define  SLO_L_2ND_LAND_USE_3 95
//#define  SLO_L_FEE_ASSMT      96
//#define  SLO_L_FLDS           97
#define  SLO_L_FEE_ASSMT      90
#define  SLO_L_FLDS           91

// SecuredData.txt
#define  SLO_T_ID                0
#define  SLO_T_BILL_YEAR         1     // CCYY/YY
#define  SLO_T_BILL_NUMBER       2
#define  SLO_T_ROLL_TYPE         3     
#define  SLO_T_ROLL_YEAR         4     // CCYY/YY
#define  SLO_T_APN               5
#define  SLO_T_FEE_PARCEL        6
#define  SLO_T_TRA               7
#define  SLO_T_VAL_TITLE_1       8
#define  SLO_T_VAL_AMOUNT_1      9
#define  SLO_T_EXE_TITLE_1       10
#define  SLO_T_EXE_AMOUNT_1      11
#define  SLO_T_VAL_TITLE_2       12
#define  SLO_T_VAL_AMOUNT_2      13
#define  SLO_T_EXE_TITLE_2       14
#define  SLO_T_EXE_AMOUNT_2      15
#define  SLO_T_VAL_TITLE_3       16
#define  SLO_T_VAL_AMOUNT_3      17
#define  SLO_T_EXE_TITLE_3       18
#define  SLO_T_EXE_AMOUNT_3      19
#define  SLO_T_VAL_TITLE_4       20
#define  SLO_T_VAL_AMOUNT_4      21
#define  SLO_T_EXE_TITLE_4       22
#define  SLO_T_EXE_AMOUNT_4      23
#define  SLO_T_VAL_TITLE_5       24
#define  SLO_T_VAL_AMOUNT_5      25
#define  SLO_T_EXE_TITLE_5       26
#define  SLO_T_EXE_AMOUNT_5      27
#define  SLO_T_VAL_TITLE_6       28
#define  SLO_T_VAL_AMOUNT_6      29
#define  SLO_T_EXE_TITLE_6       30
#define  SLO_T_EXE_AMOUNT_6      31
#define  SLO_T_VAL_TITLE_7       32
#define  SLO_T_VAL_AMOUNT_7      33
#define  SLO_T_EXE_TITLE_7       34
#define  SLO_T_EXE_AMOUNT_7      35
#define  SLO_T_VAL_TITLE_8       36
#define  SLO_T_VAL_AMOUNT_8      37
#define  SLO_T_EXE_TITLE_8       38
#define  SLO_T_EXE_AMOUNT_8      39
#define  SLO_T_VAL_TITLE_9       40
#define  SLO_T_VAL_AMOUNT_9      41
#define  SLO_T_EXE_TITLE_9       42
#define  SLO_T_EXE_AMOUNT_9      43
#define  SLO_T_VAL_TITLE_10      44
#define  SLO_T_VAL_AMOUNT_10     45
#define  SLO_T_EXE_TITLE_10      46
#define  SLO_T_EXE_AMOUNT_10     47
#define  SLO_T_NET_VALUE         48
#define  SLO_T_FUND_BASE_TYPE_1  49
#define  SLO_T_FUND_NUMBER_1     50
#define  SLO_T_FUND_TITLE_1      51
#define  SLO_T_FUND_RATE_1       52
#define  SLO_T_FUND_AMOUNT_1     53
#define  SLO_T_FUND_BASE_TYPE_2  54
#define  SLO_T_FUND_NUMBER_2     55
#define  SLO_T_FUND_TITLE_2      56
#define  SLO_T_FUND_RATE_2       57
#define  SLO_T_FUND_AMOUNT_2     58
#define  SLO_T_FUND_BASE_TYPE_3  59
#define  SLO_T_FUND_NUMBER_3     60
#define  SLO_T_FUND_TITLE_3      61
#define  SLO_T_FUND_RATE_3       62
#define  SLO_T_FUND_AMOUNT_3     63
#define  SLO_T_FUND_BASE_TYPE_4  64
#define  SLO_T_FUND_NUMBER_4     65
#define  SLO_T_FUND_TITLE_4      66
#define  SLO_T_FUND_RATE_4       67
#define  SLO_T_FUND_AMOUNT_4     68
#define  SLO_T_FUND_BASE_TYPE_5  69
#define  SLO_T_FUND_NUMBER_5     70
#define  SLO_T_FUND_TITLE_5      71
#define  SLO_T_FUND_RATE_5       72
#define  SLO_T_FUND_AMOUNT_5     73
#define  SLO_T_FUND_BASE_TYPE_6  74
#define  SLO_T_FUND_NUMBER_6     75
#define  SLO_T_FUND_TITLE_6      76
#define  SLO_T_FUND_RATE_6       77
#define  SLO_T_FUND_AMOUNT_6     78
#define  SLO_T_FUND_BASE_TYPE_7  79
#define  SLO_T_FUND_NUMBER_7     80
#define  SLO_T_FUND_TITLE_7      81
#define  SLO_T_FUND_RATE_7       82
#define  SLO_T_FUND_AMOUNT_7     83
#define  SLO_T_FUND_BASE_TYPE_8  84
#define  SLO_T_FUND_NUMBER_8     85
#define  SLO_T_FUND_TITLE_8      86
#define  SLO_T_FUND_RATE_8       87
#define  SLO_T_FUND_AMOUNT_8     88
#define  SLO_T_FUND_BASE_TYPE_9  89
#define  SLO_T_FUND_NUMBER_9     90
#define  SLO_T_FUND_TITLE_9      91
#define  SLO_T_FUND_RATE_9       92
#define  SLO_T_FUND_AMOUNT_9     93
#define  SLO_T_FUND_BASE_TYPE_10 94
#define  SLO_T_FUND_NUMBER_10    95
#define  SLO_T_FUND_TITLE_10     96
#define  SLO_T_FUND_RATE_10      97
#define  SLO_T_FUND_AMOUNT_10    98
#define  SLO_T_FUND_BASE_TYPE_11 99
#define  SLO_T_FUND_NUMBER_11    100
#define  SLO_T_FUND_TITLE_11     101
#define  SLO_T_FUND_RATE_11      102
#define  SLO_T_FUND_AMOUNT_11    103
#define  SLO_T_FUND_BASE_TYPE_12 104
#define  SLO_T_FUND_NUMBER_12    105
#define  SLO_T_FUND_TITLE_12     106
#define  SLO_T_FUND_RATE_12      107
#define  SLO_T_FUND_AMOUNT_12    108
#define  SLO_T_FUND_BASE_TYPE_13 109
#define  SLO_T_FUND_NUMBER_13    110
#define  SLO_T_FUND_TITLE_13     111
#define  SLO_T_FUND_RATE_13      112
#define  SLO_T_FUND_AMOUNT_13    113
#define  SLO_T_FUND_BASE_TYPE_14 114
#define  SLO_T_FUND_NUMBER_14    115
#define  SLO_T_FUND_TITLE_14     116
#define  SLO_T_FUND_RATE_14      117
#define  SLO_T_FUND_AMOUNT_14    118
#define  SLO_T_FUND_BASE_TYPE_15 119
#define  SLO_T_FUND_NUMBER_15    120
#define  SLO_T_FUND_TITLE_15     121
#define  SLO_T_FUND_RATE_15      122
#define  SLO_T_FUND_AMOUNT_15    123
#define  SLO_T_FUND_BASE_TYPE_16 124
#define  SLO_T_FUND_NUMBER_16    125
#define  SLO_T_FUND_TITLE_16     126
#define  SLO_T_FUND_RATE_16      127
#define  SLO_T_FUND_AMOUNT_16    128
#define  SLO_T_FUND_BASE_TYPE_17 129
#define  SLO_T_FUND_NUMBER_17    130
#define  SLO_T_FUND_TITLE_17     131
#define  SLO_T_FUND_RATE_17      132
#define  SLO_T_FUND_AMOUNT_17    133
#define  SLO_T_FUND_BASE_TYPE_18 134
#define  SLO_T_FUND_NUMBER_18    135
#define  SLO_T_FUND_TITLE_18     136
#define  SLO_T_FUND_RATE_18      137
#define  SLO_T_FUND_AMOUNT_18    138
#define  SLO_T_FUND_BASE_TYPE_19 139
#define  SLO_T_FUND_NUMBER_19    140
#define  SLO_T_FUND_TITLE_19     141
#define  SLO_T_FUND_RATE_19      142
#define  SLO_T_FUND_AMOUNT_19    143
#define  SLO_T_FUND_BASE_TYPE_20 144
#define  SLO_T_FUND_NUMBER_20    145
#define  SLO_T_FUND_TITLE_20     146
#define  SLO_T_FUND_RATE_20      147
#define  SLO_T_FUND_AMOUNT_20    148
#define  SLO_T_TOTAL_FUND_RATE   149
#define  SLO_T_TOTAL_FUND_AMOUNT 150
#define  SLO_T_MAXFUNDS          20

#define  TSIZ_APN             9
#define  TSIZ_BILLNUM         9
#define  TSIZ_DEFNUM          5
#define  TSIZ_TAXAMT          11
#define  TSIZ_FEEAMT          5
#define  TSIZ_TAXYEAR         6
#define  TSIZ_TRA             6
#define  TSIZ_DATE            8
#define  TSIZ_YEAR            4

// CortacTaxData
typedef struct _tSlo_Cortac
{  // 150-bytes
   char  State[2];
   char  County[2];
   char  TRA[TSIZ_TRA];
   char  BillYear[TSIZ_TAXYEAR];       // 11 - CCYYYY
   char  BillNum[TSIZ_BILLNUM];        // 17
   char  RollYear[TSIZ_TAXYEAR];       // 26 - CCYYYY (201819 = 2018/19)
   char  Apn[TSIZ_APN];                // 32
   char  FourYearPlanInd;              // 41 Y=4-year plan
   char  Filler1[6];
   char  TaxAmt1[TSIZ_TAXAMT];         // 48
   char  Blank1[1];                    // 59
   char  DueDate1[TSIZ_DATE];
   char  PaidStatus1;                  // 68 - (P)aid, (U)npaid, (N)ot applicable
   char  PenStatus1;                   // 69 - Y=penalty included, N=No penalty
   char  TaxAmt2[TSIZ_TAXAMT];         // 70
   char  Blank2[1];                    // 81
   char  DueDate2[TSIZ_DATE];
   char  PaidStatus2;                  // 90 - (P)aid, (U)npaid, (N)ot applicable
   char  PenStatus2;                   // 91 - Y=penalty included, N=No penalty
   char  TaxAmt3[TSIZ_TAXAMT];         // 92
   char  Blank3[1];           
   char  DueDate3[TSIZ_DATE];
   char  PaidStatus3;         
   char  PenStatus3;          
   char  TaxAmt4[TSIZ_TAXAMT];         // 114
   char  Blank4[1];           
   char  DueDate4[TSIZ_DATE];
   char  PaidStatus4;         
   char  PenStatus4;                   // 135
   char  Filler2[15];                  // 136
} SLO_CORTAC;

// CortacTaxDataPrior
typedef struct _tSlo_Delq
{  // 165-bytes
   char  State[2];
   char  County[2];
   char  TRA[TSIZ_TRA];
   char  BillYear[TSIZ_TAXYEAR];       // 11 - CCYYYY
   char  BillNum[TSIZ_BILLNUM];        // 17
   char  RollYear[TSIZ_TAXYEAR];       // 26 - CCYYYY (201819 = 2018/19)
   char  Apn[TSIZ_APN];                // 32
   char  Filler1[31];                  // 41
   char  DefaultNumber[TSIZ_DEFNUM];   // 72
   char  DefaultType[1];               // 77 - Blank, C (combining assessment) or P (partial/splitting assessment)
   char  DefaultDate[TSIZ_DATE];       // 78 - CCYYMMDD
   char  RollType[3];                  // 86 - SEC, SUP, UTL (utility roll)
   char  PenBaseYear[TSIZ_YEAR];       // 89 - Penalty base year for redemption penalty calculation
   char  OrgTaxAmt[TSIZ_TAXAMT];       // 93
   char  InstPlan[2];                  // 104 - 04=four year plan, 05=five year plan, blank=inst plan is defaulted
   char  InstPlanStartDate[TSIZ_DATE]; // 106
   char  InstPlanStatus[1];            // 114 - G=in good standing, D=Defaulted
   char  RecissionFee[TSIZ_FEEAMT];    // 115
   char  MiscFee[TSIZ_TAXAMT];         // 120
   char  InstPlanCredit[TSIZ_TAXAMT];  // 131
   char  SecInstOnly[1];               // 142 - Y=2nd inst only is delqm N=both inst are delinquent
   char  Filler2[23];                  // 143
   char  CrLf[2];
} SLO_DELQ;

// Supplemental data - supp.txt
#define  SLO_SUP_ID              0
#define  SLO_SUP_BILLYR          1
#define  SLO_SUP_BILLASSMNT      2
#define  SLO_SUP_BILLTYPE        3
#define  SLO_SUP_BILLDT          4
#define  SLO_SUP_TAXAREA         5
#define  SLO_SUP_ROLLASSMNT      6
#define  SLO_SUP_LEGAL           7
#define  SLO_SUP_ASSEE           8
#define  SLO_SUP_INCAREOF        9
#define  SLO_SUP_PRORATEFACTOR   10
#define  SLO_SUP_PRORATEPERIOD   11
#define  SLO_SUP_OWNERPRORATE    12
#define  SLO_SUP_PRORATEMSG      13
#define  SLO_SUP_DOCNUM          14
#define  SLO_SUP_CHANGEDT        15
#define  SLO_SUP_NOTICEDT        16
#define  SLO_SUP_AMTDUE          17
#define  SLO_SUP_VALUEYR         18
#define  SLO_SUP_ROLLVALTOT      19
#define  SLO_SUP_NEWBASETOT      20
#define  SLO_SUP_ROLLLASTTOT     21
#define  SLO_SUP_NETROLLVALTOT   22
#define  SLO_SUP_NETTAXVAL       23
#define  SLO_SUP_NETSUPPVAL      24
#define  SLO_SUP_STATUS          25
#define  SLO_SUP_STATUSDT        26
#define  SLO_SUP_TOTORIGBILLED   27
#define  SLO_SUP_TOTBALDUE       28
#define  SLO_SUP_PLANTYPE        29
#define  SLO_SUP_PETITIONYR      30
#define  SLO_SUP_PETITION        31
#define  SLO_SUP_NORCTOTPAID     32
#define  SLO_SUP_NORCTOTBILLED   33
#define  SLO_SUP_RETCKFEE1       34
#define  SLO_SUP_RETCKFEE2       35
#define  SLO_SUP_TOTPAID         36
#define  SLO_SUP_TOTBILLED       37
#define  SLO_SUP_PRIORUNPAIDMSG  38
#define  SLO_SUP_PENALTY1        39
#define  SLO_SUP_COST1           40
#define  SLO_SUP_INTEREST1       41
#define  SLO_SUP_RETURNCKFEE1    42
#define  SLO_SUP_PAID1           43
#define  SLO_SUP_PAYDT1          44
#define  SLO_SUP_BATCH1          45
#define  SLO_SUP_REMOVEDDT1      46
#define  SLO_SUP_REMOVEDCODE1    47
#define  SLO_SUP_DUEDT2          48
#define  SLO_SUP_DELQNTDT2       49
#define  SLO_SUP_BILLED2         50
#define  SLO_SUP_DELQNT2         51
#define  SLO_SUP_PENALTY2        52
#define  SLO_SUP_COST2           53
#define  SLO_SUP_INTEREST2       54
#define  SLO_SUP_RETURNCKFEE2    55
#define  SLO_SUP_PAID2           56
#define  SLO_SUP_PAYDT2          57
#define  SLO_SUP_BATCH2          58
#define  SLO_SUP_REMOVEDDT2      59
#define  SLO_SUP_REMOVEDCODE2    60
#define  SLO_SUP_FLDS            61

// Standard codes
// H=Homeowners
// D=Disabled Veterans
// V=Veterans
// C=Church
// L=Library
// M=Museums
// R=Religion
// T=Tribal Housing
// W=Welfare
// U=College
// P=Public School
// E=Cemetery
// I=Hospital
// S=School
// X=Misc exempt
// Y=Military

IDX_TBL4 SLO_Exemption[] = 
{
   "01", "H", 2, 1,    // HOMEOWNERS EXEMPTION          
   "02", "H", 2, 1,    // HOMEOWNERS ON BOAT            
   "03", "V", 2, 1,    // VETERANS                      
   "04", "D", 2, 1,    // DISABLED VETS                 
   "05", "R", 2, 1,    // RELIGIOUS                     
   "06", "C", 2, 1,    // CHURCH                        
   "07", "W", 2, 1,    // WELFARE                       
   "08", "X", 2, 1,    // EDUCATION                     
   "09", "M", 2, 1,    // MUSEUM                        
   "10", "E", 2, 1,    // CEMETERY                      
   "11", "X", 2, 1,    // COMMERICAL FISHING            
   "12", "U", 2, 1,    // COLLEGE                       
   "13", "P", 2, 1,    // PUBLIC SCHOOL                 
   "14", "L", 2, 1,    // LIBRARY                       
   "15", "X", 2, 1,    // HISTORICAL                    
   "16", "X", 2, 1,    // HISTORICAL AIRCRAFT           
   "17", "C", 2, 1,    // CHURCH LAND                   
   "18", "C", 2, 1,    // CHURCH IMPROVMENTS            
   "19", "C", 2, 1,    // CHURCH PERSONAL PROPERTY      
   "20", "W", 2, 1,    // WELFARE LAND                  
   "21", "W", 2, 1,    // WELFARE IMPROVEMENTS          
   "22", "W", 2, 1,    // WELFARE PP & FIX              
   "23", "R", 2, 1,    // RELIGIOUS LAND                
   "24", "R", 2, 1,    // RELIGIOUS IMPROVEMENTS        
   "25", "R", 2, 1,    // RELIG PP & FIX                
   "26", "X", 2, 1,    // EDUCATION LAND                
   "27", "X", 2, 1,    // EDUCATION IMPROVEMENTS        
   "28", "X", 2, 1,    // EDUCATION PP & FIX            
   "29", "M", 2, 1,    // MUSEUM LAND                   
   "30", "M", 2, 1,    // MUSEUM IMPROVEMENTS           
   "31", "M", 2, 1,    // MUSEUM PP & FIX               
   "32", "X", 2, 1,    // HISTORICAL LAND               
   "33", "X", 2, 1,    // HISTORICAL IMPROVEMENTS       
   "34", "X", 2, 1,    // HISTORICAL PP & FIX           
   "35", "I", 2, 1,    // WELFARE HOSPITAL LAND         
   "36", "I", 2, 1,    // WELFARE HOSPITAL IMPROVEMENTS 
   "37", "I", 2, 1,    // WELFARE HOSPITAL PP & FIX     
   "38", "S", 2, 1,    // WELFARE PRIVATE SCHOOL LAND   
   "39", "S", 2, 1,    // WELFARE PRIVATE SCHOOL IMPS   
   "40", "S", 2, 1,    // WELFARE PRIVATE SCHOOL PP/FIX 
   "42", "M", 2, 1,    // MUSEUM AIRCRAFT               
   "51", "H", 2, 1,    // HOMEOWNERS EXEMPTION          
   "60", "X", 2, 1,    // SEISMIC RETROFIT EXEMPTION    
   "91", "H", 2, 1,    // HOMEOWNERS EXEMPTION          
   "99", "X", 2, 1,    // LOW VALUE EXEMPTION           
   "","",0,0
};

#endif