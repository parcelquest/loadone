#ifndef _MERGELAX_H
#define _MERGELAX_H
 
#define  ROFF_APN             1-1
#define  ROFF_PARCEL          8-1
#define  ROFF_TRA             11-1
#define  ROFF_AGENCY          16-1
#define  ROFF_LANDYEAR        22-1
#define  ROFF_LANDVAL         26-1
#define  ROFF_IMPRYEAR        35-1
#define  ROFF_IMPRVAL         39-1
#define  ROFF_SSTRNUM         48-1
#define  ROFF_SSTRSUB         53-1
#define  ROFF_SDIR            56-1
#define  ROFF_SSTRNAME        57-1
#define  ROFF_SUNITNO         89-1
#define  ROFF_SCITY           97-1
#define  ROFF_SZIP            121-1
#define  ROFF_MSTRNUM         130-1
#define  ROFF_MSTRSUB         135-1
#define  ROFF_MDIR            138-1
#define  ROFF_MSTRNAME        139-1
#define  ROFF_MUNITNO         171-1
#define  ROFF_MCITYST         179-1
#define  ROFF_MZIP            203-1
#define  ROFF_NAME1           212-1
#define  ROFF_NAME1_2         244-1
#define  ROFF_SPCNAMETYPE     276-1
#define  ROFF_SPCNAME         281-1
#define  ROFF_NAME2           313-1
#define  ROFF_DOCDATE         345-1
#define  ROFF_TAXKEY1         353-1
#define  ROFF_SOLDTOSTATE     354-1
#define  ROFF_TAXKEY2         358-1
#define  ROFF_WEED_ABAMNT     359-1
#define  ROFF_ZONE            369-1
#define  ROFF_USE             384-1
#define  ROFF_PART_INT        388-1
#define  ROFF_DOCKEY          391-1
#define  ROFF_OWNERCODE       392-1
#define  ROFF_EXCLTYPE        393-1
#define  ROFF_PPKEY           394-1
#define  ROFF_PPVAL           395-1
#define  ROFF_PPEXE           404-1
#define  ROFF_FIXTVAL         413-1
#define  ROFF_FIXTEXE         422-1
#define  ROFF_NUMOFHOE        431-1
#define  ROFF_HOEXE           434-1
#define  ROFF_RELEXE          443-1
#define  ROFF_SALE1KEY        452-1
#define  ROFF_SALE1AMT        453-1
#define  ROFF_SALE1DATE       462-1
#define  ROFF_SALE2KEY        470-1
#define  ROFF_SALE2AMT        471-1
#define  ROFF_SALE2DATE       480-1
#define  ROFF_SALE3KEY        488-1
#define  ROFF_SALE3AMT        489-1
#define  ROFF_SALE3DATE       498-1
#define  ROFF_SUBPT1          506-1
#define  ROFF_DESIGN1         510-1
#define  ROFF_CLASS1          514-1
#define  ROFF_YRBLT1          519-1
#define  ROFF_UNITS1          523-1
#define  ROFF_BEDS1           526-1
#define  ROFF_BATHS1          528-1
#define  ROFF_BLDGSQFT1       530-1
#define  ROFF_SUBPT2          537-1
#define  ROFF_DESIGN2         541-1
#define  ROFF_CLASS2          545-1
#define  ROFF_YRBLT2          550-1
#define  ROFF_UNITS2          554-1
#define  ROFF_BEDS2           557-1
#define  ROFF_BATHS2          559-1
#define  ROFF_BLDGSQFT2       561-1
#define  ROFF_SUBPT3          568-1
#define  ROFF_DESIGN3         572-1
#define  ROFF_CLASS3          576-1
#define  ROFF_YRBLT3          581-1
#define  ROFF_UNITS3          585-1
#define  ROFF_BEDMS3          588-1
#define  ROFF_BATHS3          590-1
#define  ROFF_BLDGSQFT3       592-1
#define  ROFF_SUBPT4          599-1
#define  ROFF_DESIGN4         603-1
#define  ROFF_CLASS4          607-1
#define  ROFF_YRBLT4          612-1
#define  ROFF_UNITS4          616-1
#define  ROFF_BEDS4           619-1
#define  ROFF_BATHS4          621-1
#define  ROFF_BLDGSQFT4       623-1
#define  ROFF_SUBPT5          630-1
#define  ROFF_DESIGN5         634-1
#define  ROFF_CLASS5          638-1
#define  ROFF_YRBLT5          643-1
#define  ROFF_UNITS5          647-1
#define  ROFF_BEDS5           650-1
#define  ROFF_BATHS5          652-1
#define  ROFF_BLDGSQFT5       654-1
#define  ROFF_LGLDESC         661-1
#define  ROFF_LANDYEAR2       901-1
#define  ROFF_IMPRYEAR2       905-1
#define  ROFF_LAND2           909-1
#define  ROFF_IMPR2           918-1
#define  ROFF_CLNUM           927-1
#define  ROFF_CLTYPE          929-1
#define  ROFF_CLAPPUNIT       930-1
#define  ROFF_LANDREASONKEY   932-1    // 8=prop 8 indicator
#define  ROFF_IMPRKEY         933-1
#define  ROFF_DTTSALEAMT      938-1
#define  ROFF_BLDGYEARCHG1    947-1
#define  ROFF_BLDGCOST1       951-1
#define  ROFF_BLDGRCN1        956-1
#define  ROFF_BLDGYEARCHG2    965-1
#define  ROFF_BLDGCOST2       969-1
#define  ROFF_BLDGRCN2        974-1
#define  ROFF_BLDGYEARCHG3    983-1
#define  ROFF_BLDGCOST3       987-1
#define  ROFF_BLDGRCN3        992-1
#define  ROFF_BLDGYEARCHG4    1001-1
#define  ROFF_BLDGCOST4       1005-1
#define  ROFF_BLDGRCN4        1010-1
#define  ROFF_BLDGYEARCHG5    1019-1
#define  ROFF_BLDGCOST5       1023-1
#define  ROFF_BLDGRC5         1028-1
#define  ROFF_REAPPRYEAR      1037-1
#define  ROFF_NUMUNITS        1041-1
#define  ROFF_BUYERNAME1      1044-1
#define  ROFF_BUYERNAME2      1108-1
#define  ROFF_LASTDOCNOKEY    1140-1    
#define  ROFF_LASTDOCNUM      1141-1

#define  RSIZ_LAX_AMOUNT      9

#define  RSIZ_APN             10
#define  RSIZ_TRA             5
#define  RSIZ_AGENCY          6
#define  RSIZ_LANDYEAR        4
#define  RSIZ_LANDVAL         9
#define  RSIZ_IMPRYEAR        4
#define  RSIZ_IMPRVAL         9
#define  RSIZ_SSTRNUM         5
#define  RSIZ_SSTRSUB         3
#define  RSIZ_SDIR            1
#define  RSIZ_SSTRNAME        32
#define  RSIZ_SUNITNO         8
#define  RSIZ_SCITYST         24
#define  RSIZ_SZIP            9
#define  RSIZ_MSTRNUM         5
#define  RSIZ_MSTRSUB         3
#define  RSIZ_MDIR            1
#define  RSIZ_MSTRNAME        32
#define  RSIZ_MUNITNO         8
#define  RSIZ_MCITYST         24
#define  RSIZ_MZIP            9
#define  RSIZ_NAME1           64
#define  RSIZ_NAME1_1         32
#define  RSIZ_NAME1_2         32
#define  RSIZ_SPCNAMETYPE     5
#define  RSIZ_SPCNAME         32
#define  RSIZ_NAME2           32
#define  RSIZ_DOCDATE         8
#define  RSIZ_TAXKEY1         1
#define  RSIZ_SOLDTOSTATE     4
#define  RSIZ_TAXKEY2         1
#define  RSIZ_WEED_ABAMNT     10
#define  RSIZ_ZONE            15
#define  RSIZ_USE             4
#define  RSIZ_PART_INT        3
#define  RSIZ_DOCKEY          1
#define  RSIZ_OWNERCODE       1
#define  RSIZ_EXCLTYPE        1
#define  RSIZ_PPKEY           1
#define  RSIZ_PPVAL           9
#define  RSIZ_PPEXE           9
#define  RSIZ_FIXTVAL         9
#define  RSIZ_FIXTEXE         9
#define  RSIZ_NUMOFHOE        3
#define  RSIZ_HOEXE           9
#define  RSIZ_RELEXE          9
#define  RSIZ_SALE1KEY        1
#define  RSIZ_SALE1AMT        9
#define  RSIZ_SALE1DATE       8
#define  RSIZ_SALE2KEY        1
#define  RSIZ_SALE2AMT        9
#define  RSIZ_SALE2DATE       8
#define  RSIZ_SALE3KEY        1
#define  RSIZ_SALE3AMT        9
#define  RSIZ_SALE3DATE       8
#define  RSIZ_LGLDESC         240
#define  RSIZ_LANDYEAR2       4
#define  RSIZ_IMPRYEAR2       4
#define  RSIZ_LAND2           9
#define  RSIZ_IMPR2           9
#define  RSIZ_CLNUM           2
#define  RSIZ_CLTYPE          1
#define  RSIZ_CLAPPUNIT       2
#define  RSIZ_LANDREKEY       1
#define  RSIZ_IMPRKEY         5
#define  RSIZ_DTTSALEAMT      9
#define  RSIZ_REAPPRYEAR      4
#define  RSIZ_NUMUNITS        3
#define  RSIZ_BUYERNAME1      64
#define  RSIZ_BUYERNAME2      32
#define  RSIZ_DOCNOKEY        1    
#define  RSIZ_DOCNUM          7    

// LAX Assessor output record
#define  AOFF_ROLL1           1-1     // 1-202
#define  AOFF_ROLL2           203-1   // 212-430
#define  AOFF_NAME1           203-1   // 212-430
#define  AOFF_NAME1_2         235-1
#define  AOFF_SPCNAMETYPE     267-1
#define  AOFF_SPCNAME         272-1
#define  AOFF_NAME2           304-1
#define  AOFF_DOCDATE         336-1
#define  AOFF_TAXKEY1         344-1

#define  AOFF_SOLDTOSTATE     345-1
#define  AOFF_WEED_ABAMNT     350-1
#define  AOFF_PPVAL           386-1
#define  AOFF_PPEXE           395-1
#define  AOFF_FIXTVAL         404-1
#define  AOFF_FIXTEXE         413-1
#define  AOFF_ROLL3           422-1   // 433-900
#define  AOFF_NUMOFHOE        422-1
#define  AOFF_HOEXE           423-1
#define  AOFF_RELEXE          432-1
#define  AOFF_SALE1KEY        441-1
#define  AOFF_SALE1AMT        442-1
#define  AOFF_SALE1DATE       451-1
#define  AOFF_SALE2KEY        459-1
#define  AOFF_SALE2AMT        460-1
#define  AOFF_SALE2DATE       469-1
#define  AOFF_SALE3KEY        477-1
#define  AOFF_SALE3AMT        478-1
#define  AOFF_SALE3DATE       487-1
#define  AOFF_MASEG1          495-1
#define  AOFF_MASEG2          526-1
#define  AOFF_MASEG3          557-1
#define  AOFF_MASEG4          588-1
#define  AOFF_MASEG5          619-1

#define  AOFF_M_BRK           890-1   // B, N, or blank (no mailing)
#define  AOFF_M_CA            891-1   // Y or N
#define  AOFF_M_US            892-1   // Y or N
#define  AOFF_S_YES           893-1   // S
#define  AOFF_S_BRK           894-1   // B
#define  AOFF_HAS_SALEAMT     895-1   // S
#define  AOFF_SBG             896-1   // E = Sale > gross
#define  AOFF_LI              897-1   // LAND + IMP
#define  AOFF_LIPF            907-1   // LAND + IMP + PERS + FIXT
#define  AOFF_LIH             917-1   // LAND + IMP - HOMEX
#define  AOFF_LIPF_HFP        927-1   // LAND+IMP+PERS+FIXT-(HOMEX+FIXTEX+PERSEX)
#define  AOFF_NET             937-1   // LAND+IMP+PERS+FIXT - TOTEX    (NET)
#define  AOFF_LI_HR           947-1   // LAND + IMP - (HOMEX + REL EX)
#define  AOFF_TOTEX           957-1   // HOMEX + FIXTEX + PERSEX + REL EX
#define  AOFF_RATIO           967-1   // IMP % OF LAND + IMP
#define  AOFF_ISQFT           970-1   // Total IMPR SQFT
#define  AOFF_FILLER1         979-1
#define  AOFF_IV_SQFT         980-1   // 9(7).99 - IMPROV VAL / IMP SQ FT
#define  AOFF_SV_SQFT         990-1   // 9(7).99 - SALE VAL / IMP SQ FT
#define  AOFF_PROFIT          1000-1  // (SALEAMT - NET), IF (SALEAMT > NET)
#define  AOFF_S_STRNUM        1009-1
#define  AOFF_S_STRSUB        1016-1
#define  AOFF_S_STRDIR        1019-1
#define  AOFF_S_STRNAM        1021-1
#define  AOFF_S_STRSFX        1051-1
#define  AOFF_S_UNIT          1056-1
#define  AOFF_S_TONUM         1064-1
#define  AOFF_S_CITY          1070-1
#define  AOFF_M_STRNUM        1090-1
#define  AOFF_M_STRSUB        1097-1
#define  AOFF_M_STRDIR        1100-1
#define  AOFF_M_STRNAM        1102-1
#define  AOFF_M_STRSFX        1132-1
#define  AOFF_M_UNIT          1137-1
#define  AOFF_M_TONUM         1145-1
#define  AOFF_M_CITY          1151-1
#define  AOFF_M_STATE         1168-1
#define  AOFF_M_ZIP           1170-1
#define  AOFF_M_FLG           1179-1  // 1=HAVE CITY, ST, ZIP SEPARATE (city, state & zip)
                                      // 2=HAVE ZIP, REST IS FREEFORM (no state)
                                      // 3=ALL FREEFORM (Blank)
#define  AOFF_HAS_SEQ         1180-1
#define  AOFF_MSG_NAM1        1189-1  // Name1 after massaged
#define  AOFF_CLN_NAM1        1239-1  // Normalize & cleaned Name1
#define  AOFF_SWP_NAM1        1289-1  // CLEANED/SWAPPED NAME 1 - FIRST, MIDDLE, LAST
#define  AOFF_SLTNCODE        1339-1  // 01=MR, 02=MRS, 03=MS, 04=MISS, 05=DR     
#define  AOFF_SLTN            1341-1
#define  AOFF_TITLECD         1345-1  // '01' =  ESQ
                                      // '02' =  JR
                                      // '03' =  SR
                                      // '04' =  II
                                      // '05' =  III
                                      // '06' =  IV
                                      // '20' =  MD
                                      // '21' =  DDS
                                      // '22' =  DVM
                                      // '23' =  PHD
                                      // '24' =  LLD
#define  AOFF_TITLE           1347-1
#define  AOFF_NAMEFLG         1350-1
#define  AOFF_L_LAND          1351-1
#define  AOFF_L_IMPR          1360-1
#define  AOFF_L_EXTYPE        1369-1 
#define  AOFF_L_PPKEY         1370-1
#define  AOFF_L_PPVAL         1371-1
#define  AOFF_L_PPEX          1380-1
#define  AOFF_L_FIXT          1389-1
#define  AOFF_L_FIXTEX        1398-1
#define  AOFF_L_HOECNT        1407-1
#define  AOFF_L_HOEXE         1408-1
#define  AOFF_L_RELEXE        1417-1
#define  AOFF_L_LI            1426-1  // LAND + IMP
#define  AOFF_L_LIPF          1436-1  // LAND + IMP + PERS + FIXT
#define  AOFF_L_LIH           1446-1  // LAND + IMP - HOMEX
#define  AOFF_L_LIPF_HFP      1456-1  // LAND+IMP+PERS+FIXT-(HOMEX+FIXTEX+PERSEX)
#define  AOFF_L_NET           1466-1  // LAND+IMP+PERS+FIXT - TOTEX    (NET)
#define  AOFF_L_LI_HR         1476-1  // LAND + IMP - (HOMEX + REL EX)
#define  AOFF_L_TOTEX         1486-1  // HOMEX + FIXTEX + PERSEX + REL EX
#define  AOFF_L_RATIO         1496-1  // IMP % OF LAND + IMP
#define  AOFF_L_IV_SQFT       1499-1  // 9(7).99 - IMPROV VAL / IMP SQ FT
#define  AOFF_L_SV_SQFT       1509-1  // 9(7).99 - SALE VAL / IMP SQ FT
#define  AOFF_L_PROFIT        1519-1  // (SALEAMT - NET), IF (SALEAMT > NET)
#define  AOFF_L_SBG           1528-1  // E = Sale > lien gross
#define  AOFF_SALEAMT         1529-1
#define  AOFF_SALECNT         1539-1  // COUNT OF SALE BUCKETS - MAX 3
#define  AOFF_S1_DOCNUM       1540-1
#define  AOFF_S1_DOCDATE      1547-1
#define  AOFF_S1_DOCTYPE      1555-1
#define  AOFF_S1_DTTTYPE      1556-1
#define  AOFF_S1_VER_KEY      1557-1
#define  AOFF_S1_SALEAMT      1558-1
#define  AOFF_S1_BUYER        1568-1
#define  AOFF_S1_SELLER       1600-1
#define  AOFF_S2_DOCNUM       1632-1
#define  AOFF_S2_DOCDATE      1639-1
#define  AOFF_S2_DOCTYPE      1647-1
#define  AOFF_S2_DTTTYPE      1648-1
#define  AOFF_S2_VER_KEY      1649-1
#define  AOFF_S2_SALEAMT      1650-1
#define  AOFF_S2_BUYER        1660-1
#define  AOFF_S2_SELLER       1692-1
#define  AOFF_S3_DOCNUM       1724-1
#define  AOFF_S3_DOCDATE      1731-1
#define  AOFF_S3_DOCTYPE      1739-1
#define  AOFF_S3_DTTTYPE      1740-1
#define  AOFF_S3_VER_KEY      1741-1
#define  AOFF_S3_SALEAMT      1742-1
#define  AOFF_S3_BUYER        1752-1
#define  AOFF_S3_SELLER       1784-1
#define  AOFF_ROLL4           1816-1  // 901-1147
#define  AOFF_LANDYEAR2       901+914
#define  AOFF_IMPRYEAR2       905+914
#define  AOFF_LAND2           909+914
#define  AOFF_IMPR2           918+914
#define  AOFF_CLNUM           927+914
#define  AOFF_CLTYPE          929+914
#define  AOFF_CLAPPUNIT       930+914
#define  AOFF_LANDREKEY       932+914
#define  AOFF_IMPRKEY         933+914
#define  AOFF_DTTSALEAMT      938+914
#define  AOFF_BLDGCHG1        947+914
#define  AOFF_BLDGCHG2        965+914
#define  AOFF_BLDGCHG3        983+914
#define  AOFF_BLDGCHG4        1001+914
#define  AOFF_BLDGCHG5        1019+914
#define  AOFF_REAPPRYEAR      1037+914
#define  AOFF_NUMUNITS        1041+914
#define  AOFF_BUYERNAME1      1044+914
#define  AOFF_BUYERNAME2      1108+914
#define  AOFF_LASTDOCNOKEY    1140+914    
#define  AOFF_LASTDOCNUM      1141+914

#define  ASIZ_ROLL1           202
#define  ASIZ_ROLL2           431-212
#define  ASIZ_NAME1           32
#define  ASIZ_ROLL2_1         431-244
#define  ASIZ_ROLL3           901-433
#define  ASIZ_M_BRK           1
#define  ASIZ_M_CA            1
#define  ASIZ_M_US            1
#define  ASIZ_S_YES           1
#define  ASIZ_S_BRK           1
#define  ASIZ_HAS_SALEAMT     1
#define  ASIZ_SBG             1
#define  ASIZ_LI              10
#define  ASIZ_LIPF            10
#define  ASIZ_LIH             10
#define  ASIZ_LIPF_HFP        10
#define  ASIZ_NET             10
#define  ASIZ_LI_HR           10
#define  ASIZ_TOTEX           10
#define  ASIZ_RATIO           3
#define  ASIZ_ISQFT           9
#define  ASIZ_FILLER1         1
#define  ASIZ_IV_SQFT         10
#define  ASIZ_SV_SQFT         10
#define  ASIZ_PROFIT          9
#define  ASIZ_S_STRNUM        7
#define  ASIZ_S_STRSUB        3
#define  ASIZ_S_STRDIR        2
#define  ASIZ_S_STRNAM        30
#define  ASIZ_S_STRSFX        5
#define  ASIZ_S_UNIT          8
#define  ASIZ_S_TONUM         6
#define  ASIZ_S_CITY          20
#define  ASIZ_M_STRNUM        7
#define  ASIZ_M_STRSUB        3
#define  ASIZ_M_STRDIR        2
#define  ASIZ_M_STRNAM        30
#define  ASIZ_M_STRSFX        5
#define  ASIZ_M_UNIT          8
#define  ASIZ_M_TONUM         6
#define  ASIZ_M_CITY          17
#define  ASIZ_M_STATE         2
#define  ASIZ_M_ZIP           9
#define  ASIZ_M_FLG           1
#define  ASIZ_HAS_SEQ         9
#define  ASIZ_MSG_NAM1        50
#define  ASIZ_CLN_NAM1        50
#define  ASIZ_SWP_NAM1        50
#define  ASIZ_SLTNCODE        2
#define  ASIZ_SLTN            4
#define  ASIZ_TITLECD         2
#define  ASIZ_TITLE           3
#define  ASIZ_NAMEFLG         1
#define  ASIZ_L_LAND          9
#define  ASIZ_L_IMPR          9
#define  ASIZ_L_EXTYPE        1
#define  ASIZ_L_PPKEY         1
#define  ASIZ_L_PPVAL         9
#define  ASIZ_L_PPEX          9
#define  ASIZ_L_FIXT          9
#define  ASIZ_L_FIXTEX        9
#define  ASIZ_L_HOECNT        1
#define  ASIZ_L_HOEXE         9
#define  ASIZ_L_RELEXE        9
#define  ASIZ_L_LI            10
#define  ASIZ_L_LIPF          10
#define  ASIZ_L_LIH           10
#define  ASIZ_L_LIPF_HFP      10
#define  ASIZ_L_NET           10
#define  ASIZ_L_LI_HR         10
#define  ASIZ_L_TOTEX         10
#define  ASIZ_L_RATIO         3
#define  ASIZ_L_IV_SQFT       10
#define  ASIZ_L_SV_SQFT       10
#define  ASIZ_L_PROFIT        9
#define  ASIZ_L_SBG           1
#define  ASIZ_SALEAMT         10
#define  ASIZ_SALECNT         1
#define  ASIZ_S1_DOCNUM       7
#define  ASIZ_S1_DOCDATE      8
#define  ASIZ_S1_DOCTYPE      1
#define  ASIZ_S1_DTTTYPE      1
#define  ASIZ_S1_VER_KEY      1
#define  ASIZ_S1_SALEAMT      10
#define  ASIZ_S1_BUYER        32
#define  ASIZ_S1_SELLER       32
#define  ASIZ_S2_DOCNUM       7
#define  ASIZ_S2_DOCDATE      8
#define  ASIZ_S2_DOCTYPE      1
#define  ASIZ_S2_DTTTYPE      1
#define  ASIZ_S2_VER_KEY      1
#define  ASIZ_S2_SALEAMT      10
#define  ASIZ_S2_BUYER        32
#define  ASIZ_S2_SELLER       32
#define  ASIZ_S3_DOCNUM       7
#define  ASIZ_S3_DOCDATE      8
#define  ASIZ_S3_DOCTYPE      1
#define  ASIZ_S3_DTTTYPE      1
#define  ASIZ_S3_VER_KEY      1
#define  ASIZ_S3_SALEAMT      10
#define  ASIZ_S3_BUYER        32
#define  ASIZ_S3_SELLER       32
#define  ASIZ_ROLL4           1148-901

/***********************************************************************************
 *
 * LaxRoll record layout
 *
 ***********************************************************************************/

#define  RSIZ_SUBPT           4
#define  RSIZ_DESIGN          4
#define  RSIZ_CLASS           5
#define  RSIZ_YRBLT           4
#define  RSIZ_UNITS           3
#define  RSIZ_BEDS            2 
#define  RSIZ_BATHS           2
#define  RSIZ_BLDGSQFT        7
typedef struct _tMaseg
{  // 31 bytes
   char  SubPt[RSIZ_SUBPT];   // 506
   char  Design[RSIZ_DESIGN]; // 510
   char  Class[RSIZ_CLASS];   // 514
   char  YrBlt[RSIZ_YRBLT];   // 519
   char  Units[RSIZ_UNITS];   // 523
   char  Beds[RSIZ_BEDS];     // 526
   char  Baths[RSIZ_BATHS];   // 528
   char  Sqft[RSIZ_BLDGSQFT]; // 530
} MASEG;

#define  RSIZ_BLDGYEARCHG     4
#define  RSIZ_BLDGCOST        5
#define  RSIZ_BLDGRCN         9
typedef struct _tBldgChg
{
   char  BldgYrChg[RSIZ_BLDGYEARCHG]; // Bldg year changed
   char  BldgUnsec[RSIZ_BLDGCOST];    // Bldg cost main
   char  BldgRcn[RSIZ_BLDGRCN];       // Bldg RCN main
} BLCHG;

typedef struct _tLaxRoll
{
   char  APN[RSIZ_APN];             // 1  
   char  TRA[RSIZ_TRA];             // 11
   char  AgencyNo[6];               // 16
   char  LandBaseYr[4];             // 22
   char  LandVal[RSIZ_LAX_AMOUNT];  // 26
   char  ImprBaseYr[4];             // 35
   char  ImprVal[RSIZ_LAX_AMOUNT];  // 39
   char  S_StrNo[5];                // 48
   char  S_StrSub[3];               // 53
   char  S_Dir[1];                  // 56
   char  S_StrName[32];             // 57
   char  S_Unit[8];                 // 89
   char  S_City[24];                // 97
   char  S_Zip[9];                  // 121
   char  M_StrNo[5];                // 130
   char  M_StrSub[3];               // 135
   char  M_Dir[1];                  // 138
   char  M_StrName[32];             // 139
   char  M_Unit[8];                 // 171
   char  M_City[24];                // 179
   char  M_Zip[9];                  // 203
   char  Name1[RSIZ_NAME1];         // 212
   char  SpcType[5];                // 276
   char  SpcName[32];               // 281
   char  Name2[32];                 // 313
   char  DocDate[8];                // 345
   char  TaxKey1[1];                // 353
   char  Sold2St[4];                // 354
   char  TaxKey2[1];                // 358
   char  WeedABT[10];               // 359
   char  Zone[RSIZ_ZONE];           // 369
   char  UseCode[RSIZ_USE];         // 384
   char  PartInt[3];                // 388
   char  DocKey;                    // 391
   /*   
   Document Reason Code-Reappraisal			            Document Reason Code-Non Reappraisal
   A  -  Good Transfer - No Special Instruction				1  -  Interspousal �63 Exclusion
   B  -  Parcel Change - New Owner Per Tract Map			6  -  Affiliated Corp. �64(b) Exclusion
   C  -  Parcel Change - New Transferee				      R  -  Transfer Not Changing % Interests �62(a)
   G  -  Development Rights - Acquiring Parcels				S  -  Perfection of Title �62(b)
   H  -  Development Rights - Transferring Parcels			T  -  Security Interest (any) �62(c)
   9  -  Parcel Change - Mult. B.Y. (OWN-216)				U  -  Trust �62(d)
   M  -  Partial Interest - Complete OWN-216				   V  -  Grantor Retains Life Estate - Estate for Years �62(e)
   P  -  Trustee Sale - May Not Be Market				      W  -  Joint Tenancy �62(f),  �65(c)
   Q  -  Life Estate - Goes to Grantee					      Y  -  Parent-Child Transfers �63.1 (Prop. 58)
   5  -  Base Year Value xfer (Prop. 3 - Pub Taking)  	Z  -  Other �'s 62.1, 62.2, 66
   7  -  Base Year Value xfer (Prop. 50 - Disaster Taking) �69
   8  -  Base Year Value xfer (Prop. 60 - Senior Citizen - Age 55) �69.5
   */
   char  OwnCode;                   // 392
   char  ExclType;                  // 393 Exemption claim type
   char  PPKey;
   char  PPVal[RSIZ_LAX_AMOUNT];    // 395
   char  PPExe[RSIZ_LAX_AMOUNT];    // 404
   char  FixtVal[RSIZ_LAX_AMOUNT];  // 413
   char  FixtExe[RSIZ_LAX_AMOUNT];  // 422
   char  NumOfHOE[RSIZ_NUMOFHOE];   // 431 - # of homeowner exemps
   char  HOExe[RSIZ_LAX_AMOUNT];    // 434
   char  RelExe[RSIZ_LAX_AMOUNT];   // 443 - Religious exempt
   char  Sale1Key;                  // 452
   char  Sale1Amt[RSIZ_LAX_AMOUNT]; // 453
   char  Sale1Dt[8];                // 462 - YYYYMMDD
   char  Sale2Key;
   char  Sale2Amt[RSIZ_LAX_AMOUNT]; // 471
   char  Sale2Dt[8];                // 480 - YYYYMMDD
   char  Sale3Key;
   char  Sale3Amt[RSIZ_LAX_AMOUNT]; // 489
   char  Sale3Dt[8];                // 497 - YYYYMMDD
   MASEG Maseg[5];                  // 506
   char  LglDesc[RSIZ_LGLDESC];     // 661
   char  LandYr2[4];
   char  ImprYr2[4];
   char  LandVal2[RSIZ_LAX_AMOUNT];
   char  ImprVal2[RSIZ_LAX_AMOUNT];
   char  ClNo[2];                   // Cluster No
   char  ClType;                    // Cluster type
   char  ClAppUnit[2];              // Cluster appraisal unit
   char  LandKey;                   // Land reason key
   char  ImprKey[5];                // Impr reason key
   char  DttSaleAmt[RSIZ_SALE1AMT];
   BLCHG BldgChg[5];
   char  ReApprYr[4];               // Reappraisal year
   char  NumOfUnits[3];
   char  Buyer1[RSIZ_BUYERNAME1];   // 1044
   char  Buyer2[RSIZ_BUYERNAME2];   // 1108
   char  LastDocNumKey;
   char  LastDocNum[RSIZ_DOCNUM];
   char  CrLf[2];
} LAX_ROLL;

#define  SOFF_APN              1-1
#define  SOFF_ADMIN_REGION    11-1
#define  SOFF_CLUSTER_CODE    13-1
#define  SOFF_S_KEY           18-1
#define  SOFF_S_STRNUM        19-1
#define  SOFF_S_STRDIR        24-1
#define  SOFF_S_STRNAME       25-1
#define  SOFF_CUROWNER        57-1
#define  SOFF_TRANSFEREE      89-1  // Owner after LDR
#define  SOFF_USE_CODE       121-1
#define  SOFF_RECDOC_NO      125-1
#define  SOFF_ZONING         132-1
#define  SOFF_SELLER_RECDATE 136-1
#define  SOFF_DOCTYPE        144-1
#define  SOFF_DOCTAX_TYPE    145-1  // 1=single sale, 2-9:multi sale
#define  SOFF_DOCTAX_AMT     146-1
#define  SOFF_BUYER_RECDATE  155-1
#define  SOFF_YRBLT          163-1
#define  SOFF_EFFYR          167-1
#define  SOFF_BATHS          171-1
#define  SOFF_BEDS           173-1
#define  SOFF_DESIGN_TYPE    175-1
#define  SOFF_BLDGSQFT       176-1
#define  SOFF_SALE1_KEY      183-1
#define  SOFF_SALE1_DATE     184-1
#define  SOFF_SALE1_AMT      192-1
#define  SOFF_SALE2_KEY      201-1
#define  SOFF_SALE2_DATE     202-1
#define  SOFF_SALE2_AMT      210-1
#define  SOFF_SALE3_KEY      219-1
#define  SOFF_SALE3_DATE     220-1
#define  SOFF_SALE3_AMT      228-1
#define  SOFF_FILLER         237-1

#define  SSIZ_APN            10
#define  SSIZ_ADMIN_REGION   2
#define  SSIZ_CLUSTER_CODE   5
#define  SSIZ_S_KEY          1
#define  SSIZ_S_STRNUM       5
#define  SSIZ_S_STRDIR       1
#define  SSIZ_S_STRNAME      32
#define  SSIZ_BUYER          32
#define  SSIZ_SELLER         32
#define  SSIZ_USE_CODE       4
#define  SSIZ_RECDOC_NO      7
#define  SSIZ_ZONING         4
#define  SSIZ_DOCTYPE        1
#define  SSIZ_DOCTAX_TYPE    1
#define  SSIZ_DOCTAX_AMT     9
#define  SSIZ_RECDATE        8
#define  SSIZ_YRBLT          4
#define  SSIZ_EFFYR          4
#define  SSIZ_BATHS          2
#define  SSIZ_BEDS           2
#define  SSIZ_DESIGN_TYPE    1
#define  SSIZ_BLDGSQFT       7
#define  SSIZ_SALE1_KEY      1
#define  SSIZ_SALE1_AMT      9
#define  SSIZ_SALE2_KEY      1
#define  SSIZ_SALE2_AMT      9
#define  SSIZ_SALE3_KEY      1
#define  SSIZ_SALE3_AMT      9
#define  SSIZ_FILLER         4

typedef struct _tLaxSale
{
   char  Apn[SSIZ_APN];
   char  AdminReg[SSIZ_ADMIN_REGION];
   char  ClusterCode[SSIZ_CLUSTER_CODE];
   char  S_Key;
   char  S_StrNo[SSIZ_S_STRNUM];
   char  S_Dir[SSIZ_S_STRDIR];
   char  S_StrName[SSIZ_S_STRNAME];
   char  CurOwner[SSIZ_BUYER];
   char  Transferee[SSIZ_BUYER];
   char  UseCode[SSIZ_USE_CODE];
   char  RecDocNo[SSIZ_RECDOC_NO];
   char  Zoning[SSIZ_ZONING];
   char  SellerRecDate[SSIZ_RECDATE];
   char  DocType;
   char  DttType;
   char  DttAmt[SSIZ_DOCTAX_AMT];
   char  BuyerRecDate[SSIZ_RECDATE];
   char  YrBlt[SSIZ_YRBLT];
   char  EffYr[SSIZ_EFFYR];
   char  Baths[SSIZ_BATHS];
   char  Beds[SSIZ_BEDS];
   char  DesignType[SSIZ_DESIGN_TYPE];
   char  BldgSqft[SSIZ_BLDGSQFT];
   char  Sale1Key;
   char  Sale1Date[SSIZ_RECDATE];
   char  Sale1Amt[SSIZ_SALE1_AMT];
   char  Sale2Key;
   char  Sale2Date[SSIZ_RECDATE];
   char  Sale2Amt[SSIZ_SALE2_AMT];
   char  Sale3Key;
   char  Sale3Date[SSIZ_RECDATE];
   char  Sale3Amt[SSIZ_SALE3_AMT];
   char  Filler[SSIZ_FILLER];
} LAX_SALE;


/* 2017-03-28
#define  SOFF_APN              1-1
#define  SOFF_ADMIN_REGION    11-1
#define  SOFF_CLUSTER_CODE    13-1
#define  SOFF_S_KEY           18-1
#define  SOFF_FILLER1         19
#define  SOFF_S_STRNUM        29-1
#define  SOFF_S_STRDIR        40-1
#define  SOFF_S_STRNAME       41-1
#define  SOFF_CUROWNER        73-1
#define  SOFF_TRANSFEREE     105-1  // Owner after LDR
#define  SOFF_USE_CODE       137-1
#define  SOFF_RECDOC_NO      141-1
#define  SOFF_FILLER2        148
#define  SOFF_ZONING         152-1
#define  SOFF_SELLER_RECDATE 156-1
#define  SOFF_DOCTYPE        164-1  // B,D,H,Y
#define  SOFF_DOCTAX_TYPE    165-1  // 1=single sale, 2-9:multi sale
#define  SOFF_DOCTAX_AMT     166-1
#define  SOFF_BUYER_RECDATE  175-1
#define  SOFF_YRBLT          183-1
#define  SOFF_EFFYR          187-1
#define  SOFF_BATHS          191-1
#define  SOFF_BEDS           193-1
#define  SOFF_DESIGN_TYPE    195-1
#define  SOFF_BLDGSQFT       196-1
#define  SOFF_SALE1_KEY      203-1
#define  SOFF_SALE1_DATE     204-1
#define  SOFF_SALE1_AMT      212-1
#define  SOFF_SALE2_KEY      223-1
#define  SOFF_SALE2_DATE     224-1
#define  SOFF_SALE2_AMT      232-1
#define  SOFF_SALE3_KEY      243-1
#define  SOFF_SALE3_DATE     244-1
#define  SOFF_SALE3_AMT      252-1
#define  SOFF_FILLER9        263-1

#define  SSIZ_APN            10
#define  SSIZ_ADMIN_REGION   2
#define  SSIZ_CLUSTER_CODE   5
#define  SSIZ_S_KEY          1
#define  SSIZ_FILLER1        10
#define  SSIZ_S_STRNUM       11
#define  SSIZ_S_STRDIR       1
#define  SSIZ_S_STRNAME      32
#define  SSIZ_BUYER          32
#define  SSIZ_SELLER         32
#define  SSIZ_USE_CODE       4
#define  SSIZ_RECDOC_NO      7
#define  SSIZ_FILLER2        4
#define  SSIZ_ZONING         4
#define  SSIZ_DOCTYPE        1
#define  SSIZ_DOCTAX_TYPE    1
#define  SSIZ_DOCTAX_AMT     9
#define  SSIZ_RECDATE        8
#define  SSIZ_YRBLT          4
#define  SSIZ_EFFYR          4
#define  SSIZ_BATHS          2
#define  SSIZ_BEDS           2
#define  SSIZ_DESIGN_TYPE    1
#define  SSIZ_BLDGSQFT       7
#define  SSIZ_SALE1_KEY      1
#define  SSIZ_SALE1_AMT      11
#define  SSIZ_SALE2_KEY      1
#define  SSIZ_SALE2_AMT      11
#define  SSIZ_SALE3_KEY      1
#define  SSIZ_SALE3_AMT      11
#define  SSIZ_FILLER9        20

typedef struct _tLaxSale
{
   char  Apn[SSIZ_APN];                      //  1
   char  AdminReg[SSIZ_ADMIN_REGION];        //  11
   char  ClusterCode[SSIZ_CLUSTER_CODE];     //  13
   char  S_Key;                              //  18
   char  Filler1[SSIZ_FILLER1];              //  19
   char  S_StrNo[SSIZ_S_STRNUM];             //  29
   char  S_Dir[SSIZ_S_STRDIR];               //  40
   char  S_StrName[SSIZ_S_STRNAME];          //  41
   char  CurOwner[SSIZ_BUYER];               //  73
   char  Transferee[SSIZ_BUYER];             //  105
   char  UseCode[SSIZ_USE_CODE];             //  137
   char  RecDocNo[SSIZ_RECDOC_NO];           //  141
   char  Filler2[SSIZ_FILLER2];              //  148
   char  Zoning[SSIZ_ZONING];                //  152
   char  SellerRecDate[SSIZ_RECDATE];        //  156
   char  DocType;                            //  164 - B,D,H,Y
   char  DttType;                            //  165 - 1=single sale, 2-9:multi sale
   char  DttAmt[SSIZ_DOCTAX_AMT];            //  166 - This field stores current sale price
   char  BuyerRecDate[SSIZ_RECDATE];         //  175
   char  YrBlt[SSIZ_YRBLT];                  //  183
   char  EffYr[SSIZ_EFFYR];                  //  187
   char  Baths[SSIZ_BATHS];                  //  191
   char  Beds[SSIZ_BEDS];                    //  193
   char  DesignType[SSIZ_DESIGN_TYPE];       //  195
   char  BldgSqft[SSIZ_BLDGSQFT];            //  196
   char  Sale1Key;                           //  203
   char  Sale1Date[SSIZ_RECDATE];            //  204
   char  Sale1Amt[SSIZ_SALE1_AMT];           //  212
   char  Sale2Key;                           //  223
   char  Sale2Date[SSIZ_RECDATE];            //  224
   char  Sale2Amt[SSIZ_SALE2_AMT];           //  232
   char  Sale3Key;                           //  243
   char  Sale3Date[SSIZ_RECDATE];            //  244
   char  Sale3Amt[SSIZ_SALE3_AMT];           //  252
   char  Filler[SSIZ_FILLER9];               //  263
} LAX_SALE;
*/

#define  GSIZ_DOCNUM       7
#define  GSIZ_SYSID        7
#define  GSIZ_RECDATE      8
#define  GSIZ_IDXTIME      8
#define  GSIZ_NUMPAGES     4
#define  GSIZ_DOCTITLE     40
#define  GSIZ_DOCTYPE      4
#define  GSIZ_FEETYPE      2
#define  GSIZ_NAME         79
#define  GSIZ_CITY_CD      3
#define  GSIZ_CITY         25
#define  GSIZ_TAXAMT       9

typedef struct _tLaxDoc
{  // RECORD TYPE 0
   char  DocNum[GSIZ_DOCNUM];
   char  RecordType;
   char  SysID[GSIZ_SYSID];
   char  RecDate[GSIZ_RECDATE];
   char  IdxTime[GSIZ_IDXTIME];
   char  NumPages[GSIZ_NUMPAGES];
   char  DocTitle[GSIZ_DOCTITLE];
   char  DocType[GSIZ_DOCTYPE];
   char  FeeType[GSIZ_FEETYPE];
   char  filler[7];
   char  CrLf[2];
} LAX_DOC;

typedef struct _tLaxName
{  // RECORD TYPE 2
   char  DocNum[GSIZ_DOCNUM];
   char  RecordType;
   char  NameType;               // O=grantor, E=grantee
   char  Name[GSIZ_NAME];
   char  CrLf[2];
} LAX_NAME;

typedef struct _tLaxTax
{  // RECORD TYPE 3
   char  DocNum[GSIZ_DOCNUM];
   char  RecordType;
   char  CityCode[GSIZ_CITY_CD];
   char  City[GSIZ_CITY];
   char  TaxAmt[GSIZ_TAXAMT];
   char  filler[43];
   char  CrLf[2];
} LAX_TAX;

// Use for fields with test fields 12/1/2008-12/26/2008
#define  LAX_RD1_DOCNUM    2
#define  LAX_RD1_DOCDATE   3
#define  LAX_RD1_DESC      4
#define  LAX_RD1_PGCNT     5
#define  LAX_RD1_CITY      6
#define  LAX_RD1_XTAXCD    7
#define  LAX_RD1_DOCTAX    8
#define  LAX_RD1_TORTEE    9
#define  LAX_RD1_NAME      10

// Use for files on 12/29/2008-...
#define  LAX_RD2_DOCNUM    0
#define  LAX_RD2_DOCDATE   1
#define  LAX_RD2_TITLE     2
#define  LAX_RD2_PGCNT     3
#define  LAX_RD2_CITY      4
#define  LAX_RD2_XTAXCD    5
#define  LAX_RD2_DOCTAX    6
#define  LAX_RD2_TORTEE    7
#define  LAX_RD2_NAME      8

// Xref file
#define  XSIZ_APN          10
#define  XSIZ_TRA          5
#define  XSIZ_ADM_REGION   2
#define  XSIZ_AREA_KEY     1
#define  XSIZ_SOLD2ST      4
#define  XSIZ_DOCDATE      8
#define  XSIZ_LAND         9
#define  XSIZ_IMPR         9
#define  XSIZ_PERSPROP_KEY 1
#define  XSIZ_EXEMTYPE_KEY 1
#define  XSIZ_PP_MH        9
#define  XSIZ_OTHER_VAL    9
#define  XSIZ_FIXT_VAL     9
#define  XSIZ_RE_EXE       9
#define  XSIZ_PP_EXE       9
#define  XSIZ_FIXT_EXE     9
#define  XSIZ_HO_EXE       7
#define  XSIZ_NAME1        64
#define  XSIZ_NAME1_1      34
#define  XSIZ_NAME1_2      30
#define  XSIZ_NAME2        32
#define  XSIZ_SPCNAME      32
#define  XSIZ_S_KEY        1
#define  XSIZ_S_LASTCHG    8
#define  XSIZ_S_CITYCODE   3
#define  XSIZ_S_HSENO      5
#define  XSIZ_S_FRA        3
#define  XSIZ_S_DIR        1
#define  XSIZ_S_UNIT       8
#define  XSIZ_S_ZIP        9
#define  XSIZ_S_STRNAME    32
#define  XSIZ_S_CITYST     24
#define  XSIZ_M_KEY        1
#define  XSIZ_M_LASTCHG    8
#define  XSIZ_M_CITYCODE   3
#define  XSIZ_M_HSENO      5
#define  XSIZ_M_FRA        3
#define  XSIZ_M_DIR        1
#define  XSIZ_M_UNIT       8
#define  XSIZ_M_ZIP        9
#define  XSIZ_M_STRNAME    32
#define  XSIZ_M_CITYST     24
#define  XSIZ_NARRATIVE    40
#define  XSIZ_LOT          5
#define  XSIZ_DIVISION     5
#define  XSIZ_REGION       3
#define  XSIZ_LEGAL        40
#define  XSIZ_ZONING       15
#define  XSIZ_USECODE      4
#define  XSIZ_YREFF        4
#define  XSIZ_YRBLT        4
#define  XSIZ_BLDGSQFT     7

#define  XOFF_APN            1
#define  XOFF_TRA            11
#define  XOFF_ADM_REGION     16
#define  XOFF_AREA_KEY       18
#define  XOFF_SOLD2ST        19
#define  XOFF_DOCDATE        23
#define  XOFF_LAND           31
#define  XOFF_IMPR           40
#define  XOFF_PERSPROP_KEY   49
#define  XOFF_EXEMTYPE_KEY   50
#define  XOFF_PP_MH          51     
#define  XOFF_OTHER_VAL      60
#define  XOFF_FIXT_VAL       69
#define  XOFF_RE_EXE         78
#define  XOFF_PP_EXE         87
#define  XOFF_FIXT_EXE       96
#define  XOFF_HO_EXE         105
#define  XOFF_NAME1          112
#define  XOFF_NAME1_2        146
#define  XOFF_NAME2          176
#define  XOFF_SPCL_NAME      208
#define  XOFF_S_KEY          240
#define  XOFF_S_LASTCHG      241
#define  XOFF_S_CITYCODE     249
#define  XOFF_S_HSENO        252
#define  XOFF_S_FRA          257
#define  XOFF_S_DIR          260
#define  XOFF_S_UNIT         261
#define  XOFF_S_ZIP          269
#define  XOFF_S_STRNAME      278
#define  XOFF_S_CITYST       310
#define  XOFF_M_KEY          334
#define  XOFF_M_LASTCHG      335
#define  XOFF_M_CITYCODE     343
#define  XOFF_M_HSENO        346
#define  XOFF_M_FRA          351
#define  XOFF_M_DIR          354
#define  XOFF_M_UNIT         355
#define  XOFF_M_ZIP          363
#define  XOFF_M_STRNAME      372
#define  XOFF_M_CITYST       404
#define  XOFF_NARRATIVE      428
#define  XOFF_LOT            468
#define  XOFF_DIVISION       473
#define  XOFF_REGION         478
#define  XOFF_LEGAL          481
#define  XOFF_ZONING         681
#define  XOFF_USECODE        696
#define  XOFF_YREFF          700
#define  XOFF_YRBLT          704
#define  XOFF_BLDGSQFT       708

typedef  struct t_XrefRec
{
   char  APN         [XSIZ_APN         ];    // 1
   char  TRA         [XSIZ_TRA         ];    // 11
   char  Adm_Region  [XSIZ_ADM_REGION  ];    // 16
   char  Area_Key    [XSIZ_AREA_KEY    ];    // 18
   char  Sold2St     [XSIZ_SOLD2ST     ];    // 19
   char  DocDate     [XSIZ_DOCDATE     ];    // 23
   char  LandVal     [XSIZ_LAND        ];    // 31
   char  ImprVal     [XSIZ_IMPR        ];    // 40
   char  PP_Key      [XSIZ_PERSPROP_KEY];    // 49
   char  Exe_Key     [XSIZ_EXEMTYPE_KEY];    // 50
   char  PP_Val      [XSIZ_PP_MH       ];    // 51     
   char  Other_Val   [XSIZ_OTHER_VAL   ];    // 60
   char  Fixt_Val    [XSIZ_FIXT_VAL    ];    // 69
   char  RE_Exe      [XSIZ_RE_EXE      ];    // 78
   char  PP_Exe      [XSIZ_PP_EXE      ];    // 87
   char  Fixt_Exe    [XSIZ_FIXT_EXE    ];    // 96
   char  HO_Exe      [XSIZ_HO_EXE      ];    // 105
   char  Name1       [XSIZ_NAME1       ];    // 112
   char  Name2       [XSIZ_NAME2       ];    // 176
   char  Spcl_Name   [XSIZ_SPCNAME     ];    // 208
   char  S_Key       [XSIZ_S_KEY       ];    // 240
   char  S_LastChg   [XSIZ_S_LASTCHG   ];    // 241
   char  S_CityCode  [XSIZ_S_CITYCODE  ];    // 249
   char  S_StrNo     [XSIZ_S_HSENO     ];    // 252
   char  S_StrSub    [XSIZ_S_FRA       ];    // 257
   char  S_Dir       [XSIZ_S_DIR       ];    // 260
   char  S_Unit      [XSIZ_S_UNIT      ];    // 261
   char  S_Zip       [XSIZ_S_ZIP       ];    // 269
   char  S_StrName   [XSIZ_S_STRNAME   ];    // 278
   char  S_CitySt    [XSIZ_S_CITYST    ];    // 310
   char  M_Key       [XSIZ_M_KEY       ];    // 334
   char  M_LastChg   [XSIZ_M_LASTCHG   ];    // 335
   char  M_CityCode  [XSIZ_M_CITYCODE  ];    // 343
   char  M_StrNo     [XSIZ_M_HSENO     ];    // 346
   char  M_StrSub    [XSIZ_M_FRA       ];    // 351
   char  M_Dir       [XSIZ_M_DIR       ];    // 354
   char  M_Unit      [XSIZ_M_UNIT      ];    // 355
   char  M_Zip       [XSIZ_M_ZIP       ];    // 363
   char  M_StrName   [XSIZ_M_STRNAME   ];    // 372
   char  M_CitySt    [XSIZ_M_CITYST    ];    // 404
   char  Narrative   [XSIZ_NARRATIVE   ];    // 428
   char  Lot         [XSIZ_LOT         ];    // 468
   char  Division    [XSIZ_DIVISION    ];    // 473
   char  Region      [XSIZ_REGION      ];    // 478
   char  LglDesc1    [XSIZ_LEGAL       ];    // 481
   char  LglDesc2    [XSIZ_LEGAL       ];    // 521
   char  LglDesc3    [XSIZ_LEGAL       ];    // 561
   char  LglDesc4    [XSIZ_LEGAL       ];    // 601
   char  LglDesc5    [XSIZ_LEGAL       ];    // 641
   char  Zone        [XSIZ_ZONING      ];    // 681
   char  UseCode     [XSIZ_USECODE     ];    // 696
   char  YrEff       [XSIZ_YREFF       ];    // 700
   char  YrBlt       [XSIZ_YRBLT       ];    // 704
   char  BldgSqft    [XSIZ_BLDGSQFT    ];    // 708
   char  CRLF        [2];                    // 715
} XREF_REC;  

// Tax record type 01
#define T01_RECTYPE           0     // 01 = Secured Defaulted Tax Period
#define T01_APN               1
#define T01_GRPYEAR           2     // Assessment year for cross reference parcels
#define T01_DEFCOUNT          3     // To uniquely identify a Secured Defaulted Tax Period
#define T01_TAXSTATUS         4     // Indicates whether a secured tax defaulted period is active or inactive. 
                                    // 0 = Current or less than 1 year delinquent
                                    // 1 = Tax defaulted � sold to state
                                    // 2 = Tax defaulted � 5 years, subject to power to sale
#define T01_DELQYEAR          5
#define T01_LASTPAIDDATE      6
#define T01_COMPMONTH         7     // The month and year used to compute redemption penalties.
#define T01_REDAMTDUE         8     // Accumulated amount due
#define T01_REDPEN            9     // Penalty incurred per month.
#define T01_REDAMTPAID        10    // Accumulated net paid
#define T01_REDFEEDUE         11    // A redemption fee charged by the county for administrative handling.
#define T01_REDFEEBAL         12    // Outstanding redemption fee balance
#define T01_TITLESRCHFEE      13    // The charge for title search of STPTS parcel
#define T01_RECORDFEE         14    // The charge for recording STPTS parcel with Register Recorder
#define T01_PUBFEE            15    // The cost incurred for newspaper publication of the STPTS parcel
#define T01_NOTICEFEE         16    // The cost incurred for notification cost of STPTS parcels
#define T01_PERSVISITFEE      17    // The fee incurred for personal visitation of STPTS parcels before auction
#define T01_STCNTYFEE         18    // The State fee, County fee, monument fee for STPTS parcels
#define T01_RESCISSIONFEE     19    // The fee for rescission of the STPTS status of the parcel.
#define T01_TOTALFEEPAID      20
#define T01_ACCTNO            21    // Sequentially assigned number to uniquely identify the 5-pay plan
#define T01_STATUS            22    // If Account Number field is greater than 0, the Status is following:
                                    // 0 = Active
                                    // 1 = Redeemed
                                    // 2 = Defaulted
                                    // 3 = Pending (Open account, waiting for first payment)
                                    // 4 = Cancelled
#define T01_APPLDATE          23    // The system date when the 5-pay plan is opened online
#define T01_BEGINBAL          24
#define T01_INSTAMT1          25    // Outstanding balance of taxes owed for the first installment
#define T01_DUEDATE           26    // The due date of the 5-pay plan
#define T01_TOTALPAID         27    // Accumulated Installment Paid for 5-pay plan
#define T01_TOTALINTPAID      28    // Accumulated Interest Paid for 5-pay plan
#define T01_REDDATE           29    // The date that the property (group) was redeemed. (Paid in full)
#define T01_FLDCNT            30

#define T02_RECTYPE           0     // 02 = Secured Defaulted Tax Event
#define T02_APN               1
#define T02_GRPYEAR           2     // Assessment year for cross reference parcels
#define T02_DEFCOUNT          3     // To uniquely identify a Secured Defaulted Tax Period.
#define T02_AIN               4     // The unique ID number assigned to each parcel in Los Angeles County. 
                                    // It is comprised of a mapbook-page-parcel. It is known as Assessor ID Number 
                                    // for parcels with mapbook less than 9000 and is known as Auditor ID Number for 
                                    // parcels with mapbook equal to or greater than 9000. Escapes created by 
                                    // the Auditor are sequentially assigned. Public Utility parcels (mapbook is equal
                                    // to or grater than 9000) are assigned using an algorithm by the Auditor.
#define T02_ASMTYEAR          5
#define T02_ASMTSEQNUM        6     // Identifies whether the assessment is an original valuation or supplemental from 
                                    // subsequent valuations resulting from a transfer or change of ownership. It also 
                                    // identifies the chronological order or the PDB sub segment. Possible values are:
                                    //    000   -   Rolls
                                    //    >000 -   Supplemental (Transfer or new construction)
#define T02_DEFSEQNUM         7     // Sequentially assigned number. Incremented by 1 starting at 0.  To uniquely identify a Secured Defaulted Tax Event in the same drawoff year
#define T02_BILLYEAR          8     // The year liability was added to the secured roll
#define T02_DRAWOFFYEAR       9     // The calendar year the liability was transferred from the secured roll to the tax defaulted roll
#define T02_MASTERAIN         10    // The parcel AIN that cannot be billed
#define T02_ASMTDATE          11
#define T02_ORIGINCHG         12
#define T02_REASONCHG         13
#define T02_AUTHNUM           14
#define T02_PAIDSTATUS        15
#define T02_TAXDUE            16    // The amount of liability transferred to the Tax Defaulted Roll
#define T02_TAXBAL            17    // The unpaid amount of liability transferred to the Tax Defaulted Roll
#define T02_PENDUE            18    // Outstanding total amount of the 1st penalty and 2nd penalty due amount
#define T02_PENBAL            19    // Outstanding total amount of secured penalty due
#define T02_COSTDUE           20
#define T02_COSTBAL           21
#define T02_DEFPENOWED        22    // Amount of penalty not paid as a result of a partial payment (to date as of last penalty date).
#define T02_DEFPENPAID        23    // The amount of tax defaulted penalties paid by or refunded to the taxpayer
#define T02_FLDCNT            24

#define T03_RECTYPE           0     // 03 = Secured Defaulted Direct Assessment (detail delinquent info, not use)
#define T03_APN               1
#define T03_GRPYEAR           2
#define T03_DEFCOUNT          3
#define T03_AIN               4
#define T03_ASMTYEAR          5
#define T03_ASMTSEQNUM        6
#define T03_DEFSEQNUM         7
#define T03_DAACCTNO          8
#define T03_DAAMTDUE          9
#define T03_FLDCNT            10

#define T04_RECTYPE           0     // 04 = Secured Defaulted Subject To Power To Sell (not use)
#define T04_APN               1
#define T04_GRPYEAR           2
#define T04_DEFCOUNT          3
#define T04_AIN               4
#define T04_SELLNUM           5
#define T04_ITEMNUM           6
#define T04_S2SYEAR           7     // YYYY or 0000
#define T04_FLDCNT            8

// Secured tax roll (TCSTN960.UNPACKED.FILE)
#define SEC_RECTYPE           0
#define SEC_PRCLYEAR          1     // This is used only for mapbook 8900 � 9990 parcels and is the year the parcel is added to the roll.  Otherwise is zero
#define SEC_APN               2
#define SEC_YRACTIVETBL       3
#define SEC_MININGRIGHTS      4
#define SEC_TAXSTATUS         5     // 0 = Current or less than 1 year delinquent
                                    // 1 = Tax defaulted � sold to state
                                    // 2 = Tax defaulted � 5 years, subject to power to sale
#define SEC_DELQYEAR          6     // The year the parcel first went delinquent
#define SEC_SENIORYEAR        7     // The first year a senior citizen is released from paying property taxes and began a 
                                    // tax deferment program with the state
#define SEC_FOURPAYPLAN       8     // Indicates that a four-pay payment plan is active for an "escaped assessment".  
                                    // Possible values are 00 for not active and 04 for active
#define SEC_LENDERCODE        9     // Used to indicate a lending institution or multiple ownership.
                                    // 500 � 584 = Lending Institution
                                    // 585 � 599 = Miscellaneous including non-assessables.
                                    // 700 � 799 =  Multiple ownership group.
#define SEC_LOANID            10    // The loan number submitted by the lending institution that identifies the parcel within their system.  
                                    // Also used to indicate a multiple owner lender.
#define SEC_SENDBILLFLAG      11
#define SEC_UNPAIDKEY1        12    // Year-Sequence key for up to five unpaid parcel history segments.  If there are more than five, 
                                    // the last occurrence will contain �99999�.  These values are not in nines-compliment form. 
#define SEC_UNPAIDKEY2        13
#define SEC_UNPAIDKEY3        14
#define SEC_UNPAIDKEY4        15
#define SEC_UNPAIDKEY5        16
#define SEC_PRCLHIST          17    // Identifies the roll year and whether a master or supplemental assessment
                                    // First 2 digits is year assd and last 3 digits is sequence number 
                                    // 000=master record, 010=supllemental
#define SEC_RCN               18    // Roll correction number
#define SEC_TRA               19
#define SEC_PROCDATE          20    // The System Date when the Parcel History Segment was created
#define SEC_BATCHNUM          21
#define SEC_OCD               22    // The date (YYMMDD) of an ownership change or a new construction completion date.  
                                    // Used to determine proration factor for calculating the supplemental tax amounts
#define SEC_BILLDATE          23    // The date (YYMMDD) the bill was sent.  Used to determine the year the parcel will go delinquent
#define SEC_XREF              24    // Indicates that an escape assessment is noted for the parcel and as a result, one or more cross 
                                    // reference parcels were created. For a regular parcel, the element is comprised of the 
                                    // 8900 mapbook number, beginning page and parcel number, and ending page and parcel numbers.
                                    // If only one cross-reference parcel is created for the regular parcel,
                                    // then the ending page and parcel numbers are zeros.
#define SEC_PAIDSTATUS        25    // 00 = Unpaid
                                    // 01 = Personal property paid
                                    // 02 = 1st installment only paid
                                    // 03 = Total paid, for both installments
                                    // 04 = 1st installment, 2nd adjusted
                                    // 05 = 1st and 2nd paid, 2nd adjusted
                                    // 06 = 1st paid, 2nd cancelled
                                    // 07 = Personal property paid, 2nd adjusted
                                    // 08 = 1st partially paid
                                    // 09 = 1st paid, 2nd partially paid 

#define SEC_INACTIVEKEY       26    // Blank = active
                                    // S = Shell parcel
                                    // D = Deactivated parcel
                                    // U = Transferred to Unsecured roll
                                    // R = Transferred to Secured Tax Defaulted

#define SEC_EXEMPTIONKEY      27
#define SEC_BILLTYPEKEY       28
#define SEC_FORCETAXKEY       29
#define SEC_LANDPARCELCOMP    30
#define SEC_IMPRPARCELCOMP    31
#define SEC_ESCAPETRANS       32    // The current year when an Escaped Assessment Apportionment Transaction was created.  
                                    // If an escaped assessment transaction comes into the update program, the key of the 
                                    // matching parcel history segment is compared to the current year on the county summary.  
                                    // If it is equal, no additional Escaped Assessment Apportionment Transaction will be created 
                                    // for apportionment.  If it is zero or equal to a prior year, then an Escaped Assessment Transaction is output
#define SEC_AUTHNUM           33
#define SEC_AUTHREASON        34    // Type of authorization change.  The possible values are:
                                    // Blank = No reason
                                    // A = Supplemental bill due to transfer
                                    // B = Supplemental bill due to new construction
                                    // D = Change to a roll bill due to misfortune and calamity
                                    // E = Change to a supplemental bill due to misfortune and calamity
                                    // F = Change to a roll bill due to a decline in value
                                    // G = Change to a roll bill
                                    // H = Change to a supplemental bill
                                    // J = Correction to a roll bill due to an Assessment Appeals Board Found value
                                    // K = Correction to a supplemental bill due to an Assessment Appeals Board found value
                                    // L = Correction to a roll bill due to assessor error
                                    // M = Correction to supplemental bill due to assessor error
                                    // N = Correction to a roll bill
                                    // P = Correction to a supplemental bill
                                    // R = Change to a roll bill due to an exemption penalty
                                    // S = Change to a supplemental bill due to an exemption penalty 

#define SEC_AUTHORG           35    // Source of authorization change.  The possible value are:
                                    // Blank = No origin
                                    // A = Assessor no interest paid
                                    // B = Auditor
                                    // C = Tax Collector (L,M,N)
                                    // D = Assessor interest paid
                                    // E = Prorated REX
                                    // F = (-1, 750) HOX
                                    // G = 10% REX penalty or $250 whichever is less
                                    // H = 15% REX penalty or $250 whichever is less

#define SEC_INSTAMT1          36    // Amount of taxes due in the first installment.  For mapbook 8950, this could also include half of the taxes due on personal property, if any.  
                                    // For all other mapbooks, the total of taxes due on personal property is in this installment
#define SEC_DUEAMT1           37    // Remaining balance of taxes owed for the first installment
#define SEC_PENAMT1           38
#define SEC_PAIDPENAMT1       39
#define SEC_PAIDDATE1         40
#define SEC_INSTAMT2          41
#define SEC_DUEAMT2           42
#define SEC_PENAMT2           43
#define SEC_PAIDPENAMT2       44
#define SEC_PAIDDATE2         45
#define SEC_COSTDUE           46
#define SEC_COSTPAID          47
#define SEC_NSFDUE            48
#define SEC_NSFPAID           49
#define SEC_TOTALTAXDUE       50
#define SEC_LANDVAL           51
#define SEC_MHVAL             52
#define SEC_IMPRVAL           53
#define SEC_MHOTHER           54
#define SEC_FIXTVAL           55
#define SEC_PPVAL             56
#define SEC_OTHEXEVAL         57
#define SEC_MHOTHEXEVAL       58
#define SEC_HOEXEVAL          59
#define SEC_MHEXEVAL          60
#define SEC_FIXTEXEVAL        61
#define SEC_PPEXEVAL          62
#define SEC_TAXTYPE1          63    // The extended tax amount for land only.  For a non-mobilehome parcel (mapbook not = 8950), 
                                    // this equals the product of the Tax Type 1 Rate and the Land Value.  
                                    // For a mobilehome parcel, (mapbook = 8950), this will equal zero
#define SEC_TAXTYPE2          64    // The extended tax amount for net real property, equal to the product of the Tax Type 2 Rate 
                                    // and the Net Real Value
#define SEC_TAXTYPE3          65    // The extended tax amount for real estate exemptions, equal to the product of the Tax Type 3 Rate
                                    // and the Gross Real Exemptions
#define SEC_TAXTYPE4          66    // The extended tax amount for improvements
#define SEC_TAXTYPE8          67    // The extended tax amount for personal property equal to the product of the Tax Type 8 Rate and 
                                    // the net personal property value
#define SEC_TAXCODE1          68    // The account number of the agency that requested that the AMT be placed
#define SEC_AGENCY1           69
#define SEC_TAXAMT1           70
#define SEC_PRORATEDAMT1      71
#define SEC_TAXCODE2          72
#define SEC_AGENCY2           73
#define SEC_TAXAMT2           74
#define SEC_PRORATEDAMT2      75
#define SEC_TAXCODE3          76
#define SEC_AGENCY3           77
#define SEC_TAXAMT3           78
#define SEC_PRORATEDAMT3      79
#define SEC_TAXCODE4          80
#define SEC_AGENCY4           81
#define SEC_TAXAMT4           82
#define SEC_PRORATEDAMT4      83
#define SEC_TAXCODE5          84
#define SEC_AGENCY5           85
#define SEC_TAXAMT5           86
#define SEC_PRORATEDAMT5      87
#define SEC_TAXCODE6          88
#define SEC_AGENCY6           89
#define SEC_TAXAMT6           90
#define SEC_PRORATEDAMT6      91
#define SEC_TAXCODE7          92
#define SEC_AGENCY7           93
#define SEC_TAXAMT7           94
#define SEC_PRORATEDAMT7      95
#define SEC_TAXCODE8          96
#define SEC_AGENCY8           97
#define SEC_TAXAMT8           98
#define SEC_PRORATEDAMT8      99
#define SEC_TAXCODE9          100
#define SEC_AGENCY9           101
#define SEC_TAXAMT9           102
#define SEC_PRORATEDAMT9      103
#define SEC_TAXCODE10         104
#define SEC_AGENCY10          105
#define SEC_TAXAMT10          106
#define SEC_PRORATEDAMT10     107
#define SEC_COMBINELIENKEY    108
#define SEC_MAX_TAXITEMS      10

// "Secured Tax Roll.csv"
#define STR_RECTYPE                       0
#define STR_PARCEL_YEAR_8900S             1
#define STR_PARCEL_NUMBER                 2
#define STR_APN                           2
#define STR_YEAR_ACTIVE_TABLE             3
#define STR_MINING_RIGHTS_KEY             4
#define STR_TAX_STATUS_KEY                5
#define STR_TAXSTATUS                     5     // 0 = Current or less than 1 year delinquent
                                                // 1 = Tax defaulted - sold to state
                                                // 2 = Tax defaulted - 5 years, subject to power to sale
#define STR_YEAR_TAX_DEFAULTED            6
#define STR_DELQYEAR                      6     // The year the parcel first went delinquent
#define STR_SENIOR_CITIZEN_YEAR           7     // The first year a senior citizen is released from paying property taxes and began a 
                                                // tax deferment program with the state
#define STR_4PAY                          8     // Indicates that a four-pay payment plan is active for an "escaped assessment".  
                                                // Possible values are 00 for not active and 04 for active
#define STR_500_ACCOUNT_NUMBER            9
#define STR_500_ACCOUNT_LOAN_ID           10    // The loan number submitted by the lending institution that identifies the parcel within their system.  
                                                // Also used to indicate a multiple owner lender.
#define STR_500_ACCOUNT_KEY               11
#define STR_UNPAID_YRSEQ_KEY_1            12    // Year-Sequence key for up to five unpaid parcel history segments.  If there are more than five, 
                                                // the last occurrence will contain "99999".  These values are not in nines-compliment form. 
#define STR_UNPAID_YRSEQ_KEY_2            13
#define STR_UNPAID_YRSEQ_KEY_3            14
#define STR_UNPAID_YRSEQ_KEY_4            15
#define STR_UNPAID_YRSEQ_KEY_5            16
#define STR_PARCEL_HISTORY_YRSEQ          17
#define STR_PRCLHIST                      17    // Identifies the roll year and whether a master or supplemental assessment
                                                // First 2 digits is year assd and last 3 digits is sequence number 
                                                // 000=master record, 010=supllemental
#define STR_CHANGE_NUMBER                 18
#define STR_RCN                           18
#define STR_TAX_RATE_AREA                 19
#define STR_TRA                           19
#define STR_PROCESS_DATE                  20
#define STR_BATCH_NUMBER                  21
#define STR_OWNR_CHNGE_OR_NEW_CNSTR_DATE  22
#define STR_BILL_MAIL_DATE                23
#define STR_CROSS_REFERENCE_MASTER        24
#define STR_PAID_KEY                      25    // 00 = Unpaid
                                                // 01 = PP paid
                                                // 02 = 1st payment paid
                                                // 03 = Both paid
#define STR_PAIDSTATUS                    25
#define STR_INACTIVE_KEY                  26
#define STR_EXEMPTION_CLAIM_KEY           27
#define STR_BILL_TYPE_KEY                 28
#define STR_FORCE_TAX_KEY                 29
#define STR_LAND_PARTIAL_COMPLETION       30
#define STR_IMPR_PARTIAL_COMPLETION_KEY   31
#define STR_ESCAPE_TRANSACTION_KEY        32    // The current year when an Escaped Assessment Apportionment Transaction was created.  
                                                // If an escaped assessment transaction comes into the update program, the key of the 
                                                // matching parcel history segment is compared to the current year on the county summary.  
                                                // If it is equal, no additional Escaped Assessment Apportionment Transaction will be created 
                                                // for apportionment.  If it is zero or equal to a prior year, then an Escaped Assessment Transaction is output
#define STR_AUTHORIZATION_NUMBER          33
#define STR_AUTH_REASON_KEY               34    
#define STR_AUTHREASON                    34    // Type of authorization change.  The possible values are:
                                                // Blank = No reason
                                                // A = Supplemental bill due to transfer
                                                // B = Supplemental bill due to new construction
                                                // D = Change to a roll bill due to misfortune and calamity
                                                // E = Change to a supplemental bill due to misfortune and calamity
                                                // F = Change to a roll bill due to a decline in value
                                                // G = Change to a roll bill
                                                // H = Change to a supplemental bill
                                                // J = Correction to a roll bill due to an Assessment Appeals Board Found value
                                                // K = Correction to a supplemental bill due to an Assessment Appeals Board found value
                                                // L = Correction to a roll bill due to assessor error
                                                // M = Correction to supplemental bill due to assessor error
                                                // N = Correction to a roll bill
                                                // P = Correction to a supplemental bill
                                                // R = Change to a roll bill due to an exemption penalty
                                                // S = Change to a supplemental bill due to an exemption penalty 
#define STR_AUTH_ORIGIN_KEY               35
#define STR_AUTHORG                       35    // Source of authorization change.  The possible value are:
                                                // Blank = No origin
                                                // A = Assessor no interest paid
                                                // B = Auditor
                                                // C = Tax Collector (L,M,N)
                                                // D = Assessor interest paid
                                                // E = Prorated REX
                                                // F = (-1, 750) HOX
                                                // G = 10% REX penalty or $250 whichever is less
                                                // H = 15% REX penalty or $250 whichever is less
#define STR_1ST_INSTALLMENT_TAX           36    // Amount of taxes due in the first installment.  For mapbook 8950, this could also include half of the taxes due on personal property, if any.  
                                                // For all other mapbooks, the total of taxes due on personal property is in this installment
#define STR_INSTAMT1                      36
#define STR_1ST_INSTALLMENT_BALANCE_DUE   37
#define STR_DUEAMT1                       37
#define STR_1ST_INSTALLMENT_PENALTY       38
#define STR_PENAMT1                       38
#define STR_1ST_INSTALLMENT_PENALTY_PAID  39
#define STR_PAIDPENAMT1                   39
#define STR_1ST_INSTALLMENT_PAID_DATE     40       // MM/DD/YYYY
#define STR_PAIDDATE1                     40
#define STR_2ND_INSTALLMENT_TAX           41
#define STR_INSTAMT2                      41
#define STR_2ND_INSTALLMENT_BALANCE_DUE   42
#define STR_DUEAMT2                       42
#define STR_2ND_INSTALLMENT_PEN_COST      43
#define STR_PENAMT2                       43
#define STR_2ND_INSTALLMENT_PENALTY_PAID  44
#define STR_PAIDPENAMT2                   44
#define STR_2ND_INSTALLMENT_PAID_DATE     45
#define STR_PAIDDATE2                     45
#define STR_COST_DUE                      46
#define STR_COSTDUE                       46
#define STR_COST_PAID                     47
#define STR_NONSUFFICIENT_FUNDS_DUE       48
#define STR_NONSUFFICIENT_FUNDS_PAID      49
#define STR_TOTAL_TAX_DUE                 50
#define STR_TOTALTAXDUE                   50
#define STR_LAND_VALUE                    51
#define STR_LANDVAL                       51
#define STR_MOBILEHOME_VALUE              52
#define STR_MHVAL                         52
#define STR_IMPROVEMENT_VALUE             53
#define STR_IMPRVAL                       53
#define STR_MOBILEHOME_OTHER              54
#define STR_MHOTHER                       54
#define STR_FIXTURE_VALUE                 55
#define STR_FIXTVAL                       55
#define STR_PERSONAL_PROPERTY_VALUE       56
#define STR_PPVAL                         56
//#define STR_REAL_ESTATE_EXEMPTION_VALUE   57
#define STR_OTHEXEVAL                     57
//#define STR_MOBILEHOME_OTHER_EXEMPTION    58
#define STR_MHOTHEXEVAL                   58
//#define STR_HOMEOWNERS_EXEMPTION          59
#define STR_HOEXEVAL                      59
//#define STR_MOBILE_OWNER_EXEMPTION        60
#define STR_MHEXEVAL                      60
//#define STR_FIXTURES_EXEMPTION            61
#define STR_FIXTEXEVAL                    61
//#define STR_PERSONAL_PROPERTY_EXEMPTION   62
#define STR_PPEXEVAL                      62
#define STR_TAXTYPE1                      63    // The extended tax amount for land only.  For a non-mobilehome parcel (mapbook not = 8950), 
                                                // this equals the product of the Tax Type 1 Rate and the Land Value.  
                                                // For a mobilehome parcel, (mapbook = 8950), this will equal zero
#define STR_TAXTYPE2                      64    // The extended tax amount for net real property, equal to the product of the Tax Type 2 Rate 
                                                // and the Net Real Value
#define STR_TAXTYPE3                      65    // The extended tax amount for real estate exemptions, equal to the product of the Tax Type 3 Rate
                                                // and the Gross Real Exemptions
#define STR_TAXTYPE4                      66    // The extended tax amount for improvements
#define STR_TAXTYPE8                      67    // The extended tax amount for personal property equal to the product of the Tax Type 8 Rate and 
                                                // the net personal property value
#define STR_OBJECTID                      68
#define STR_FLDS                          69

// "Liens Secured Tax Roll.csv"
#define LSTR_APN              0    
#define LSTR_PRCLYEAR         1    
#define LSTR_PRCLYEAR_SEQ     2    
#define LSTR_TAXCODE1         3    
#define LSTR_AGENCY1          4
#define LSTR_TAXAMT1          5
#define LSTR_PRORATEDAMT1     6
#define LSTR_TAXCODE2         7
#define LSTR_AGENCY2          8
#define LSTR_TAXAMT2          9
#define LSTR_PRORATEDAMT2     10
#define LSTR_TAXCODE3         11
#define LSTR_AGENCY3          12
#define LSTR_TAXAMT3          13
#define LSTR_PRORATEDAMT3     14
#define LSTR_TAXCODE4         15
#define LSTR_AGENCY4          16
#define LSTR_TAXAMT4          17
#define LSTR_PRORATEDAMT4     18
#define LSTR_TAXCODE5         19
#define LSTR_AGENCY5          20
#define LSTR_TAXAMT5          21
#define LSTR_PRORATEDAMT5     22
#define LSTR_TAXCODE6         23
#define LSTR_AGENCY6          24
#define LSTR_TAXAMT6          25
#define LSTR_PRORATEDAMT6     26
#define LSTR_TAXCODE7         27
#define LSTR_AGENCY7          28
#define LSTR_TAXAMT7          29
#define LSTR_PRORATEDAMT7     30
#define LSTR_TAXCODE8         31
#define LSTR_AGENCY8          32
#define LSTR_TAXAMT8          33
#define LSTR_PRORATEDAMT8     34
#define LSTR_TAXCODE9         35
#define LSTR_AGENCY9          36
#define LSTR_TAXAMT9          37
#define LSTR_PRORATEDAMT9     38
#define LSTR_TAXCODE10        39
#define LSTR_AGENCY10         40
#define LSTR_TAXAMT10         41
#define LSTR_PRORATEDAMT10    42
#define LSTR_TAXCODE11        43
#define LSTR_AGENCY11         44
#define LSTR_TAXAMT11         45
#define LSTR_PRORATEDAMT11    46
#define LSTR_TAXCODE12        47
#define LSTR_AGENCY12         48
#define LSTR_TAXAMT12         49
#define LSTR_PRORATEDAMT12    50
#define LSTR_TAXCODE13        51
#define LSTR_AGENCY13         52
#define LSTR_TAXAMT13         53
#define LSTR_PRORATEDAMT13    54
#define LSTR_TAXCODE14        55
#define LSTR_AGENCY14         56
#define LSTR_TAXAMT14         57
#define LSTR_PRORATEDAMT14    58
#define LSTR_TAXCODE15        59
#define LSTR_AGENCY15         60
#define LSTR_TAXAMT15         61
#define LSTR_PRORATEDAMT15    62
#define LSTR_TAXCODE16        63
#define LSTR_AGENCY16         64
#define LSTR_TAXAMT16         65
#define LSTR_PRORATEDAMT16    66
#define LSTR_TAXCODE17        67
#define LSTR_AGENCY17         68
#define LSTR_TAXAMT17         69
#define LSTR_PRORATEDAMT17    70
#define LSTR_TAXCODE18        71
#define LSTR_AGENCY18         72
#define LSTR_TAXAMT18         73
#define LSTR_PRORATEDAMT18    74
#define LSTR_TAXCODE19        75
#define LSTR_AGENCY19         76
#define LSTR_TAXAMT19         77
#define LSTR_PRORATEDAMT19    78
#define LSTR_TAXCODE20        79
#define LSTR_AGENCY20         80
#define LSTR_TAXAMT20         81
#define LSTR_PRORATEDAMT20    82
#define LSTR_TAXCODE21        83
#define LSTR_AGENCY21         84
#define LSTR_TAXAMT21         85
#define LSTR_PRORATEDAMT21    86
#define LSTR_TAXCODE22        87
#define LSTR_AGENCY22         88
#define LSTR_TAXAMT22         89
#define LSTR_PRORATEDAMT22    90
#define LSTR_TAXCODE23        91
#define LSTR_AGENCY23         92
#define LSTR_TAXAMT23         93
#define LSTR_PRORATEDAMT23    94
#define LSTR_TAXCODE24        95
#define LSTR_AGENCY24         96
#define LSTR_TAXAMT24         97
#define LSTR_PRORATEDAMT24    98
#define LSTR_TAXCODE25        99
#define LSTR_AGENCY25         100
#define LSTR_TAXAMT25         101
#define LSTR_PRORATEDAMT25    102
#define LSTR_COMBINELIENKEY   103
#define LSTR_OBJECTID         104
#define LSTR_FLDS             105
#define LSTR_MAX_TAXITEMS     25

// Standard codes
// H=Homeowners
// D=Disabled Veterans
// V=Veterans
// C=Church
// L=Library
// M=Museums
// R=Religion
// T=Tribal Housing
// W=Welfare
// U=College
// P=Public School
// E=Cemetery
// I=Hospital
// S=School
// X=Misc exempt
// Y=Military

IDX_TBL4 LAX_Exemption[] = 
{
   "10","H", 2,1,
   "0", "V", 1,1,       // VETERAN NUMBER ON FILE, NO CLAIM
   "1", "V", 1,1,
   "2", "X", 1,1,       // DELETE VETERAN EXEMPTION
   "3", "C", 1,1,       // CHURCH, WHOLLY EXEMPT
   "4", "W", 1,1,       // WELFARE, WHOLLY EXEMPT
   "5", "R", 1,1,       // FULL RELIGIOUS
   "6", "C", 1,1,       // CHURCH, PARTIALLY EXEMPT
   "7", "W", 1,1,       // WELFARE, PARTIALLY EXEMPT
   "8", "R", 1,1,       // RELIGIOUS, PARTIALLY EXEMPT
   "9", "X", 1,1,       // DELETE REAL ESTATE EXEMPTION
   "","",0,0
};

#endif