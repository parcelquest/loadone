#ifndef  _MERGESDX_H_
#define  _MERGESDX_H_

// SDX county definition MPR08
// Lien record
#define  LOFF_FILLER_1                  1-1  // 1 byte
#define  LOFF_APN                       2-1
#define  LOFF_FILLER_2                 12-1  // 2 bytes
#define  LOFF_CHECK_DIGIT              14-1
#define  LOFF_TRA                      15-1
#define  LOFF_FILLER_3                 20-1  // 1 byte
#define  LOFF_CONTRACT_CODE            21-1
#define  LOFF_FRACT_INT_CODE           22-1
#define  LOFF_MARITAL_STATUS           23-1
#define  LOFF_OWNER_STATUS             25-1
#define  LOFF_OWNER_NAME               27-1
#define  LOFF_FILLER_4                123-1  // 4 bytes
#define  LOFF_MAIL_ADDR               127-1
#define  LOFF_MAIL_ZIP                202-1
#define  LOFF_FILLER_5                207-1  // 1 byte
#define  LOFF_MAIL_CHANGE_DATE        208-1  // blank if 0 prefix
#define  LOFF_SITUS_NUMBER            214-1
#define  LOFF_SITUS_UNIT              219-1
#define  LOFF_SITUS_STREET            222-1
#define  LOFF_DESCR_CODE              262-1
#define  LOFF_PROP_DESCR              263-1
#define  LOFF_FILLER_6                328-1  // 40 bytes
#define  LOFF_CURR_NET_VALUE          368-1
#define  LOFF_CURR_BUS_INV_VALUE      378-1
#define  LOFF_CURR_LAND_VALUE         388-1
#define  LOFF_CURR_IMPR_VALUE         398-1
#define  LOFF_CURR_FIXT_VALUE         409-1
#define  LOFF_CURR_BUS_PP_VALUE       419-1
#define  LOFF_CURR_PP_VALUE           429-1
#define  LOFF_FILLER_9                439-1  // 10 bytes
#define  LOFF_APPEALS_YEAR            449-1
#define  LOFF_FILLER_10               451-1  // 6 bytes
#define  LOFF_EXE_CODE_1              457-1
#define  LOFF_EXE_VALUE_1             458-1
#define  LOFF_EXE_CODE_2              468-1
#define  LOFF_EXE_VALUE_2             469-1
#define  LOFF_EXE_CODE_3              479-1
#define  LOFF_EXE_VALUE_3             480-1
#define  LOFF_REC_DOCTYPE             490-1
#define  LOFF_REC_DOCNUM              491-1
#define  LOFF_FILLER_10a              497-1  // 1 byte
#define  LOFF_REC_DOCDATE             498-1  
#define  LOFF_MAP_NUMBER              504-1
#define  LOFF_FILLER_11               515-1  // 11 bytes
#define  LOFF_ORIG_CUT_NO             526-1
#define  LOFF_ORIG_CUT_DATE           532-1
#define  LOFF_TRA_CUT_NO              539-1
#define  LOFF_TRA_CUT_DATE            545-1
#define  LOFF_RED_CUT_NO              552-1
#define  LOFF_RED_CUT_DATE            558-1
#define  LOFF_OLD_TAX_RATE            565-1
#define  LOFF_FILLER_12               570-1  // 1 byte
#define  LOFF_OLD_PARCEL_NO           571-1
#define  LOFF_FILLER_13               581-1  // 2 bytes
#define  LOFF_ACREAGE                 583-1
#define  LOFF_UNITS                   592-1
#define  LOFF_ZONING                  596-1
#define  LOFF_USE_CODE                597-1
#define  LOFF_FILLER_14               599-1  // 7 bytes
#define  LOFF_SOLD_RED_CODE           606-1
#define  LOFF_SOLD_RED_YEAR           607-1
#define  LOFF_FILLER_15               609-1  // 4 bytes
#define  LOFF_TAX_STATUS              613-1  // T|N
#define  LOFF_FILLER_16               614-1  // 2 bytes
#define  LOFF_ASSESSMENT_YEAR         617-1  // YY
#define  LOFF_FILLER_6a               619-1  // 1 byte
#define  LOFF_TRANSACTION_DATE        620-1  // MMDDYY
#define  LOFF_FILLER_17               626-1  // 1 byte

// 2019 Secured(csv)19.txt layout
#define  LDR_APN                      1
#define  LDR_FILLER_2                 2
#define  LDR_CHECK_DIGIT              3
#define  LDR_TRA                      4
#define  LDR_FILLER_5                 5
#define  LDR_CONTRACT_CODE            6
#define  LDR_FRACT_INT_CODE           7
#define  LDR_MARITAL_STATUS           8
#define  LDR_OWNER_STATUS             9
#define  LDR_OWNER_NAME               10
#define  LDR_FILLER_11                11
#define  LDR_MAIL_ADDR                12
#define  LDR_MAIL_ZIP                 13
#define  LDR_MAIL_CHANGE_DATE         14
#define  LDR_SITUS_NUMBER             15  // May include hyphen '-'
#define  LDR_SITUS_STREET             16
#define  LDR_DESCR_CODE               17
#define  LDR_PROP_DESCR               18
#define  LDR_FILLER_19                19
#define  LDR_FILLER_20                20
#define  LDR_CURR_NET_VALUE           21
#define  LDR_CURR_BUS_INV_VALUE       22
#define  LDR_CURR_LAND_VALUE          23
#define  LDR_CURR_IMPR_VALUE          24
#define  LDR_CURR_FIXT_VALUE          25
#define  LDR_CURR_BUS_PP_VALUE        26
#define  LDR_CURR_PP_VALUE            27
#define  LDR_FILLER_28                28
#define  LDR_APPEALS_YEAR             29
#define  LDR_FILLER_30                30
#define  LDR_EXE_CODE_1               31
#define  LDR_EXE_VALUE_1              32
#define  LDR_EXE_CODE_2               33
#define  LDR_EXE_VALUE_2              34
#define  LDR_EXE_CODE_3               35
#define  LDR_EXE_VALUE_3              36
#define  LDR_REC_DOCTYPE              37
#define  LDR_REC_DOCNUM               38
#define  LDR_REC_DOCDATE              39
#define  LDR_MAP_NUMBER               40
#define  LDR_FILLER_41                41
#define  LDR_ORIG_CUT_NO              42
#define  LDR_ORIG_CUT_DATE            43
#define  LDR_TRA_CUT_NO               44
#define  LDR_TRA_CUT_DATE             45
#define  LDR_RED_CUT_NO               46
#define  LDR_RED_CUT_DATE             47
#define  LDR_OLD_TAX_RATE             48
#define  LDR_FILLER_49                49
#define  LDR_OLD_PARCEL_NO            50
#define  LDR_FILLER_51                51
#define  LDR_ACREAGE                  52
#define  LDR_UNITS                    53
#define  LDR_ZONING                   54
#define  LDR_USE_CODE                 55
#define  LDR_FILLER_56                56
#define  LDR_SOLD_RED_CODE            57
#define  LDR_SOLD_RED_YEAR            58
#define  LDR_FILLER_59                59
#define  LDR_TAX_STATUS               60
#define  LDR_FILLER_61                61
#define  LDR_ASSESSMENT_YEAR          62
#define  LDR_TRANSACTION_DATE         63

#define  LSIZ_FILLER_1                1 
#define  LSIZ_APN                     10 
#define  LSIZ_FILLER_2                2
#define  LSIZ_CHECK_DIGIT             1
#define  LSIZ_TRA                     5
#define  LSIZ_FILLER_3                1
#define  LSIZ_CONTRACT_CODE           1
#define  LSIZ_FRACT_INT_CODE          1
#define  LSIZ_MARITAL_STATUS          2
#define  LSIZ_OWNER_STATUS            2
#define  LSIZ_OWNER_NAME              96
#define  LSIZ_FILLER_4                4
#define  LSIZ_MAIL_ADDR               75
#define  LSIZ_MAIL_ZIP                5
#define  LSIZ_FILLER_5                1
#define  LSIZ_MAIL_CHANGE_DATE        6
#define  LSIZ_SITUS_NUMBER            5
#define  LSIZ_SITUS_UNIT              3
#define  LSIZ_SITUS_STREET            40
#define  LSIZ_DESCR_CODE              1
#define  LSIZ_PROP_DESCR              65
#define  LSIZ_FILLER_6                40
#define  LSIZ_NET_VALUE               10
#define  LSIZ_BUS_INV_VALUE           10
#define  LSIZ_LAND_VALUE              10
#define  LSIZ_IMPR_VALUE              11
#define  LSIZ_FIXT_VALUE              10
#define  LSIZ_BUS_PP_VALUE            10
#define  LSIZ_PP_VALUE                10
#define  LSIZ_FILLER_9                10
#define  LSIZ_APPEALS_YEAR            2
#define  LSIZ_FILLER_10               6
#define  LSIZ_EXE_CODE_1              1
#define  LSIZ_EXE_VALUE_1             10
#define  LSIZ_EXE_CODE_2              1
#define  LSIZ_EXE_VALUE_2             10
#define  LSIZ_EXE_CODE_3              1
#define  LSIZ_EXE_VALUE_3             10
#define  LSIZ_REC_DOCTYPE             1
#define  LSIZ_REC_DOCNUM              6
#define  LSIZ_REC_DOCDATE             6
#define  LSIZ_MAP_NUMBER              11
#define  LSIZ_FILLER_11               11
#define  LSIZ_ORIG_CUT_NO             6
#define  LSIZ_ORIG_CUT_DATE           7
#define  LSIZ_TRA_CUT_NO              6
#define  LSIZ_TRA_CUT_DATE            7
#define  LSIZ_RED_CUT_NO              6
#define  LSIZ_RED_CUT_DATE            7
#define  LSIZ_OLD_TAX_RATE            5
#define  LSIZ_FILLER_12               1
#define  LSIZ_OLD_PARCEL_NO           10
#define  LSIZ_FILLER_13               2
#define  LSIZ_ACREAGE                 9
#define  LSIZ_UNITS                   4
#define  LSIZ_ZONING                  1
#define  LSIZ_USE_CODE                2
#define  LSIZ_FILLER_14               7
#define  LSIZ_SOLD_RED_CODE           1
#define  LSIZ_SOLD_RED_YEAR           2
#define  LSIZ_FILLER_15               4
#define  LSIZ_TAX_STATUS              1
#define  LSIZ_FILLER_16               3
#define  LSIZ_ASSESSMENT_YEAR         2
#define  LSIZ_TRANSACTION_DATE        6
#define  LSIZ_FILLER_17               1

typedef struct _tSdxLien
{  // 628 bytes                     
   char  filler1[LSIZ_FILLER_1];
   char  Apn[LSIZ_APN];                         // 3-3-2-4
   char  filler2[LSIZ_FILLER_2];
   char  ChkDigit;
   char  TRA[LSIZ_TRA];
   char  filler3[LSIZ_FILLER_3];
   char  ContractCode;
   char  FractIntCode;
   char  Matital_Status[LSIZ_MARITAL_STATUS];
   char  Owner_Status[LSIZ_OWNER_STATUS];
   char  OwnerName[LSIZ_OWNER_NAME];
   char  filler4[LSIZ_FILLER_4];
   char  MailAddr[LSIZ_MAIL_ADDR];
   char  MailZip[LSIZ_MAIL_ZIP];
   char  filler5[LSIZ_FILLER_5];
   char  MailChgDate[LSIZ_MAIL_CHANGE_DATE];
   char  S_StrNum[LSIZ_SITUS_NUMBER];
   char  S_Unit[LSIZ_SITUS_UNIT];
   char  S_StrName[LSIZ_SITUS_STREET];          // StrName & Suffix ends with "*\"
   char  DescCode;
   char  Description[LSIZ_PROP_DESCR];
   char  filler6[LSIZ_FILLER_6];
   char  NetVal[LSIZ_NET_VALUE];
   char  BusInv[LSIZ_BUS_INV_VALUE];            // Not used
   char  Land[LSIZ_LAND_VALUE];
   char  Impr[LSIZ_IMPR_VALUE];
   char  Fixt[LSIZ_FIXT_VALUE];                 // Not used
   char  Bus_PP[LSIZ_BUS_PP_VALUE];             // Not used
   char  PP_Val[LSIZ_PP_VALUE];
   char  filler9[LSIZ_FILLER_9];
   char  AppealYr[LSIZ_APPEALS_YEAR];
   char  filler10[LSIZ_FILLER_10];
   char  Exe_Code1;
   char  Exe_Amt1[LSIZ_EXE_VALUE_1];
   char  Exe_Code2;
   char  Exe_Amt2[LSIZ_EXE_VALUE_1];
   char  Exe_Code3;
   char  Exe_Amt3[LSIZ_EXE_VALUE_1];
   char  DocType;
   char  DocNum[LSIZ_REC_DOCNUM];
   char  filler10a;
   char  DocDate[LSIZ_REC_DOCDATE];             // MMDDYY
   char  MapNum[LSIZ_MAP_NUMBER];               // TRACT OR MAP NUMBER
   char  filler11[LSIZ_FILLER_11];
   char  Orig_Cut_No[LSIZ_ORIG_CUT_NO];
   char  Orig_Cut_Date[LSIZ_ORIG_CUT_DATE];
   char  Tra_Cut_No[LSIZ_ORIG_CUT_NO];
   char  Tra_Cut_Date[LSIZ_ORIG_CUT_DATE];
   char  Redraft_Cut_No[LSIZ_ORIG_CUT_NO];
   char  Redraft_Cut_Date[LSIZ_ORIG_CUT_DATE];
   char  Old_Tra[LSIZ_OLD_TAX_RATE];
   char  filler12[LSIZ_FILLER_12];
   char  Old_Apn[LSIZ_OLD_PARCEL_NO];
   char  filler13[LSIZ_FILLER_13];
   char  Acreage[LSIZ_ACREAGE];                 // V99
   char  Units[LSIZ_UNITS];
   char  Zoning;
   char  UseCode[LSIZ_USE_CODE];
   char  filler14[LSIZ_FILLER_14];
   char  Sold_Red_Code;
   char  Sold_Red_Year[LSIZ_SOLD_RED_YEAR];     // YY
   char  filler15[LSIZ_FILLER_15];
   char  Tax_Status;
   char  filler16[LSIZ_FILLER_16];
   char  Asmnt_Yr[LSIZ_ASSESSMENT_YEAR];
   char  filler16a;
   char  Trans_Date[LSIZ_TRANSACTION_DATE];
   char  filler17[LSIZ_FILLER_17];
   char  CrLf[2];
} SDX_LIEN;

/*
// Lien record
#define  LOFF_DELETE_CODE               1-1
#define  LOFF_APN                       2-1
#define  LOFF_FILLER1                  12-1
#define  LOFF_TRA                      15-1
#define  LOFF_CONTRACT_CODE            21-1
#define  LOFF_FRACT_INT_CODE           22-1
#define  LOFF_MARITAL_STATUS           23-1
#define  LOFF_OWNER_STATUS             25-1
#define  LOFF_OWNER_NAME               27-1
#define  LOFF_FILLER1_2               123-1
#define  LOFF_MAIL_ADDR               129-1
#define  LOFF_MAIL_ZIP                204-1
#define  LOFF_MAIL_CHANGE_DATE        209-1     // packed decimal
#define  LOFF_SITUS_NUMBER            215-1
#define  LOFF_SITUS_UNIT              220-1
#define  LOFF_SITUS_STREET            223-1
#define  LOFF_DESCR_CODE              263-1
#define  LOFF_PROP_DESCR              264-1
#define  LOFF_FILLER2                 329-1
#define  LOFF_NET_VALUE               369-1
#define  LOFF_BUS_INV_VALUE           378-1     // Not used
#define  LOFF_LAND_VALUE              383-1
#define  LOFF_IMPS_VALUE              392-1
#define  LOFF_FILLER2_1               401-1
#define  LOFF_PP_VALUE                411-1
#define  LOFF_FILLER3                 420-1
#define  LOFF_APPEALS_YEAR            430-1
#define  LOFF_FILLER4                 432-1
#define  LOFF_EXE_CODE_1              438-1
#define  LOFF_EXE_VALUE_1             439-1
#define  LOFF_EXE_CODE_2              448-1
#define  LOFF_EXE_VALUE_2             449-1
#define  LOFF_EXE_CODE_3              458-1
#define  LOFF_EXE_VALUE_3             459-1
#define  LOFF_REC_DOCTYPE             468-1
#define  LOFF_REC_DOCNUM              469-1
#define  LOFF_FILLER5                 475-1
#define  LOFF_REC_DOCDATE             476-1
#define  LOFF_MAP_NUMBER              482-1
#define  LOFF_FILLER6                 493-1
#define  LOFF_OLD_TAX_RATE            534-1
#define  LOFF_OLD_APN                 540-1
#define  LOFF_FILLER7                 550-1
#define  LOFF_ACREAGE                 552-1
#define  LOFF_UNITS                   559-1
#define  LOFF_ZONING                  563-1
#define  LOFF_USE_CODE                564-1
#define  LOFF_FILLER8                 566-1
#define  LOFF_SOLD_RED_CODE           573-1
#define  LOFF_SOLD_RED_YEAR           574-1
#define  LOFF_FILLER9                 576-1
#define  LOFF_TAX_STATUS              580-1
#define  LOFF_FILLER10                581-1
#define  LOFF_ASSESSMENT_YEAR         583-1     // YY
#define  LOFF_FILLER10_1              585-1
#define  LOFF_TRANSACTION_DATE        586-1     // MMDDYY

#define  LSIZ_DELETE_CODE             1
#define  LSIZ_APN                     10
#define  LSIZ_FILLER1                 3
#define  LSIZ_TRA                     6
#define  LSIZ_CONTRACT_CODE           1
#define  LSIZ_FRACT_INT_CODE          1
#define  LSIZ_MARITAL_STATUS          2
#define  LSIZ_OWNER_STATUS            2
#define  LSIZ_OWNER_NAME              96
#define  LSIZ_FILLER1_2               6
#define  LSIZ_MAIL_ADDR               75
#define  LSIZ_MAIL_ZIP                5
#define  LSIZ_MAIL_CHANGE_DATE        6
#define  LSIZ_SITUS_NUMBER            5
#define  LSIZ_SITUS_UNIT              3
#define  LSIZ_SITUS_STREET            40
#define  LSIZ_DESCR_CODE              1
#define  LSIZ_PROP_DESCR              65
#define  LSIZ_FILLER2                 40
#define  LSIZ_NET_VALUE               9
#define  LSIZ_BUS_INV_VALUE           5
#define  LSIZ_LAND_VALUE              9
#define  LSIZ_IMPS_VALUE              9
#define  LSIZ_FILLER2_1               10
#define  LSIZ_PP_VALUE                9
#define  LSIZ_FILLER3                 10
#define  LSIZ_APPEALS_YEAR            2
#define  LSIZ_FILLER4                 6
#define  LSIZ_EXE_AMT                 9
#define  LSIZ_REC_DOCTYPE             1
#define  LSIZ_REC_DOCNUM              6
#define  LSIZ_FILLER5                 1
#define  LSIZ_REC_DOCDATE             6
#define  LSIZ_MAP_NUMBER              11
#define  LSIZ_FILLER6                 41
#define  LSIZ_OLD_TAX_RATE            6
#define  LSIZ_OLD_APN                 10
#define  LSIZ_FILLER7                 2
#define  LSIZ_ACREAGE                 7
#define  LSIZ_UNITS                   4
#define  LSIZ_ZONING                  1
#define  LSIZ_USE_CODE                2
#define  LSIZ_FILLER8                 7
#define  LSIZ_SOLD_RED_CODE           1
#define  LSIZ_SOLD_RED_YEAR           2
#define  LSIZ_FILLER9                 4
#define  LSIZ_TAX_STATUS              1
#define  LSIZ_FILLER10                2
#define  LSIZ_ASSESSMENT_YEAR         2
#define  LSIZ_FILLER10_1              1
#define  LSIZ_TRANSACTION_DATE        6

typedef struct _tSdxLien
{  // 589 bytes
   char  DelCode;
   char  Apn[LSIZ_APN];                         // 3-3-2-4
   char  Filler1[LSIZ_FILLER1];
   char  TRA[LSIZ_TRA];
   char  ContractCode;
   char  FractIntCode;
   char  Matital_Status[LSIZ_MARITAL_STATUS];
   char  Owner_Status[LSIZ_OWNER_STATUS];
   char  OwnerName[LSIZ_OWNER_NAME];
   char  filler1_2[LSIZ_FILLER1_2];
   char  MailAddr[LSIZ_MAIL_ADDR];
   char  MailZip[LSIZ_MAIL_ZIP];
   char  MailChgDate[LSIZ_MAIL_CHANGE_DATE];
   char  S_StrNum[LSIZ_SITUS_NUMBER];
   char  S_Unit[LSIZ_SITUS_UNIT];
   char  S_StrName[LSIZ_SITUS_STREET];          // StrName & Suffix ends with "*\"
   char  DescCode;
   char  Description[LSIZ_PROP_DESCR];
   char  filler2[LSIZ_FILLER2];
   char  NetVal[LSIZ_LAND_VALUE];
   char  BusInv[LSIZ_BUS_INV_VALUE];       // Not used
   char  Land[LSIZ_LAND_VALUE];
   char  Impr[LSIZ_LAND_VALUE];
   char  Filler2_1[LSIZ_FILLER2_1];  
   char  PP_Val[LSIZ_LAND_VALUE];
   char  filler3[LSIZ_FILLER3];
   char  AppealYr[LSIZ_APPEALS_YEAR];
   char  filler4[LSIZ_FILLER4];
   char  Exe_Code1;
   char  Exe_Amt1[LSIZ_EXE_AMT];
   char  Exe_Code2;
   char  Exe_Amt2[LSIZ_EXE_AMT];
   char  Exe_Code3;
   char  Exe_Amt3[LSIZ_EXE_AMT];
   char  DocType;
   char  DocNum[LSIZ_REC_DOCNUM];
   char  filler5;
   char  DocDate[LSIZ_REC_DOCDATE];             // MMDDYY
   char  MapNum[LSIZ_MAP_NUMBER];               // TRACT OR MAP NUMBER
   char  filler6[LSIZ_FILLER6];
   char  Old_Tra[LSIZ_TRA];
   char  Old_Apn[LSIZ_APN];
   char  filler7[LSIZ_FILLER7];
   char  Acreage[LSIZ_ACREAGE];                   // V99
   char  Units[LSIZ_UNITS];
   char  Zoning;
   char  UseCode[LSIZ_USE_CODE];
   char  filler8[LSIZ_FILLER8];
   char  Sold_Red_Code;
   char  Sold_Red_Year[LSIZ_SOLD_RED_YEAR];     // YY
   char  filler9[LSIZ_FILLER9];
   char  Tax_Status;
   char  filler10[LSIZ_FILLER10];
   char  Asmnt_Yr[LSIZ_ASSESSMENT_YEAR];
   char  filler10_1;
   char  Trans_Date[LSIZ_TRANSACTION_DATE];
   char  CrLf[2];
} SDX_LIEN;
*/

// Roll record
#define  ROFF_DELETE_CODE               1-1
#define  ROFF_APN                       2-1
#define  ROFF_CHECK_DIGIT              14-1
#define  ROFF_TRA                      15-1
#define  ROFF_CONTRACT_CODE            21-1
#define  ROFF_FRACT_INT_CODE           22-1
#define  ROFF_MARITAL_STATUS           23-1
#define  ROFF_OWNER_STATUS             25-1
#define  ROFF_OWNER_NAME               27-1
#define  ROFF_MAIL_ADDR               129-1
#define  ROFF_MAIL_ZIP                204-1
#define  ROFF_MAIL_CHANGE_DATE        209-1
#define  ROFF_SITUS_NUMBER            215-1
#define  ROFF_SITUS_UNIT              220-1
#define  ROFF_SITUS_STREET            223-1
#define  ROFF_DESCR_CODE              263-1
#define  ROFF_PROP_DESCR              264-1
#define  ROFF_CURR_NET_VALUE          397-1
#define  ROFF_CURR_BUS_INV_VALUE      406-1
#define  ROFF_CURR_LAND_VALUE         415-1
#define  ROFF_CURR_IMPS_VALUE         424-1
#define  ROFF_CURR_FIXT_VALUE         433-1
#define  ROFF_CURR_BUS_PP_VALUE       442-1
#define  ROFF_CURR_PPY_VALUE          451-1
#define  ROFF_APPEALS_YEAR            470-1
#define  ROFF_EXE_CODE_1              480-1
#define  ROFF_EXE_VALUE_1             481-1
#define  ROFF_EXE_CODE_2              490-1
#define  ROFF_EXE_VALUE_2             491-1
#define  ROFF_EXE_CODE_3              500-1
#define  ROFF_EXE_VALUE_3             501-1
#define  ROFF_REC_DOCTYPE             510-1
#define  ROFF_REC_DOCNUM              511-1
#define  ROFF_REC_DOCDATE             517-1
#define  ROFF_MAP_NUMBER              523-1
#define  ROFF_ORIG_CUT_NO             553-1
#define  ROFF_ORIG_CUT_DATE           559-1
#define  ROFF_TRA_CUT_NO              565-1
#define  ROFF_TRA_CUT_DATE            571-1
#define  ROFF_RED_CUT_NO              577-1
#define  ROFF_RED_CUT_DATE            583-1
#define  ROFF_OLD_TAX_RATE            589-1
#define  ROFF_OLD_PARCEL_NO           595-1
#define  ROFF_ACREAGE                 607-1
#define  ROFF_UNITS                   614-1
#define  ROFF_ZONING                  618-1
#define  ROFF_USE_CODE                619-1
#define  ROFF_SOLD_RED_CODE           628-1
#define  ROFF_SOLD_RED_YEAR           629-1
#define  ROFF_TAX_STATUS              635-1
#define  ROFF_ASSESSMENT_YEAR         638-1     // YY
#define  ROFF_TRANSACTION_DATE        640-1     // MMDDYY
#define  ROFF_LAST_BYTE_VALUE_Z       647-1

#define  RSIZ_DELETE_CODE               1
#define  RSIZ_APN                      10
#define  RSIZ_FILLER                    3
#define  RSIZ_TRA                       6
#define  RSIZ_CONTRACT_CODE             1
#define  RSIZ_FRACT_INT_CODE            1
#define  RSIZ_MARITAL_STATUS            2
#define  RSIZ_OWNER_STATUS              2
#define  RSIZ_OWNER_NAME               96
#define  RSIZ_MAIL_ADDR                75
#define  RSIZ_MAIL_ZIP                  5
#define  RSIZ_MAIL_CHANGE_DATE          6
#define  RSIZ_SITUS_NUMBER              5
#define  RSIZ_SITUS_UNIT                3
#define  RSIZ_SITUS_STREET             40
#define  RSIZ_DESCR_CODE                1
#define  RSIZ_DESCRIPTION              65
#define  RSIZ_CURR_NET_VALUE            9
#define  RSIZ_CURR_BUS_INV_VALUE        9
#define  RSIZ_LAND                      9
#define  RSIZ_CURR_IMPS_VALUE           9
#define  RSIZ_CURR_FIXT_VALUE           9
#define  RSIZ_CURR_BUS_PP_VALUE         9
#define  RSIZ_CURR_PPY_VALUE            9
#define  RSIZ_APPEALS_YEAR              2
#define  RSIZ_EXE_CODE                  1
#define  RSIZ_EXE_AMT                   9
#define  RSIZ_REC_DOCTYPE               1
#define  RSIZ_REC_DOCNUM                6
#define  RSIZ_REC_DOCDATE               6
#define  RSIZ_MAP_NUMBER               11
#define  RSIZ_ORIG_CUT_NO               6
#define  RSIZ_ORIG_CUT_DATE             6
#define  RSIZ_TRA_CUT_NO                6
#define  RSIZ_TRA_CUT_DATE              6
#define  RSIZ_RED_CUT_NO                6
#define  RSIZ_RED_CUT_DATE              6
#define  RSIZ_OLD_TAX_RATE              6
#define  RSIZ_OLD_PARCEL_NO            12
#define  RSIZ_ACREAGE                   7
#define  RSIZ_UNITS                     4
#define  RSIZ_ZONING                    1
#define  RSIZ_USE_CODE                  2
#define  RSIZ_SOLD_RED_CODE             1
#define  RSIZ_SOLD_RED_YEAR             2
#define  RSIZ_TAX_STATUS                1
#define  RSIZ_ASSESSMENT_YEAR           2
#define  RSIZ_TRANSACTION_DATE          6
#define  RSIZ_LAST_BYTE_VALUE_Z         1

typedef struct _tSdxRoll
{  // 649 bytes                     
   char  DelCode;
   char  Apn[RSIZ_APN];                         // 3-3-2-4
   char  filler[RSIZ_FILLER];
   char  TRA[RSIZ_TRA];
   char  ContractCode;
   char  FractIntCode;
   char  Matital_Status[RSIZ_MARITAL_STATUS];
   char  Owner_Status[RSIZ_OWNER_STATUS];
   char  OwnerName[RSIZ_OWNER_NAME];
   char  filler1[6];
   char  MailAddr[RSIZ_MAIL_ADDR];              // 129
   char  MailZip[RSIZ_MAIL_ZIP];
   char  MailChgDate[RSIZ_MAIL_CHANGE_DATE];
   char  S_StrNum[RSIZ_SITUS_NUMBER];           // 215
   char  S_Unit[RSIZ_SITUS_UNIT];
   char  S_StrName[RSIZ_SITUS_STREET];          // StrName & Suffix ends with "*\"
   char  DescCode;
   char  Description[RSIZ_DESCRIPTION];
   char  filler2[68];   
   char  NetVal[RSIZ_CURR_NET_VALUE];
   char  BusInv[RSIZ_CURR_BUS_INV_VALUE];       // Not used
   char  Land[RSIZ_LAND];
   char  Impr[RSIZ_LAND];
   char  Fixt[RSIZ_LAND];                       // Not used
   char  Bus_PP[RSIZ_LAND];                     // Not used
   char  PP_Val[RSIZ_LAND];
   char  filler3[10];   
   char  AppealYr[RSIZ_APPEALS_YEAR];
   char  filler4[8];
   char  Exe_Code1;
   char  Exe_Amt1[RSIZ_EXE_AMT];
   char  Exe_Code2;
   char  Exe_Amt2[RSIZ_EXE_AMT];
   char  Exe_Code3;
   char  Exe_Amt3[RSIZ_EXE_AMT];
   char  DocType;
   char  DocNum[RSIZ_REC_DOCNUM];
   char  DocDate[RSIZ_REC_DOCDATE];             // MMDDYY
   char  MapNum[RSIZ_MAP_NUMBER];               // TRACT OR MAP NUMBER
   char  filler5[19];
   char  Orig_Cut_No[RSIZ_ORIG_CUT_NO];
   char  Orig_Cut_Date[RSIZ_ORIG_CUT_DATE];
   char  Tra_Cut_No[RSIZ_ORIG_CUT_NO];
   char  Tra_Cut_Date[RSIZ_ORIG_CUT_DATE];
   char  Redraft_Cut_No[RSIZ_ORIG_CUT_NO];
   char  Redraft_Cut_Date[RSIZ_ORIG_CUT_DATE];
   char  Old_Tra[RSIZ_OLD_TAX_RATE];
   char  Old_Apn[RSIZ_OLD_PARCEL_NO];
   char  Acreage[RSIZ_ACREAGE];                   // V99
   char  Units[RSIZ_UNITS];
   char  Zoning;
   char  UseCode[RSIZ_USE_CODE];
   char  filler6[7];
   char  Sold_Red_Code;
   char  Sold_Red_Year[RSIZ_SOLD_RED_YEAR];     // YY
   char  filler7[4];
   char  Tax_Status;
   char  filler8[2];
   char  Asmnt_Yr[RSIZ_ASSESSMENT_YEAR];
   char  Trans_Date[RSIZ_TRANSACTION_DATE];
   char  filler9[2];
   char  CrLf[2];
} SDX_ROLL;

#define  COFF_APN                       1-1
#define  COFF_TRA                      11-1
#define  COFF_ZONING                   16-1
#define  COFF_LAND_USE                 17-1
#define  COFF_EFF_YR                   19-1
#define  COFF_BLDG_SQFT                21-1
#define  COFF_BEDROOMS                 26-1
#define  COFF_BATHS                    29-1
#define  COFF_GARAGE_STALLS            32-1
#define  COFF_HAS_POOL                 35-1
#define  COFF_HAS_VIEW                 36-1
#define  COFF_LOT_SQFT                 37-1
#define  COFF_ACREAGE                  42-1
#define  COFF_UNITS                    49-1
#define  COFF_TAXABLE                  53-1
#define  COFF_CONTRACT_CODE            54-1
#define  COFF_FRACT_INT_CODE           55-1
#define  COFF_MARITAL_STATUS           56-1
#define  COFF_OWNER_STATUS             58-1
#define  COFF_OWNER_NAME               60-1
#define  COFF_MAILING_ADDR            156-1
#define  COFF_SITUS_ADDR              236-1
#define  COFF_SITUS_NUMBER            236-1
#define  COFF_SITUS_UNIT              241-1
#define  COFF_SITUS_STREET            244-1
#define  COFF_VALUE_CHANGE_DT         284-1  // mmddyy
#define  COFF_VALUE_CHANGE_CODE       290-1
#define  COFF_VALUE_ACTION_DATE       291-1  // yymmdd
#define  COFF_VALUE_ACTION_CODE       297-1

#define  CSIZ_APN                      10
#define  CSIZ_TRA                       5
#define  CSIZ_ZONING                    1
#define  CSIZ_LAND_USE                  2
#define  CSIZ_EFF_YR                    2
#define  CSIZ_BLDG_SQFT                 5
#define  CSIZ_BEDROOMS                  3
#define  CSIZ_BATHS                     3
#define  CSIZ_GARAGE_STALLS             3
#define  CSIZ_HAS_POOL                  1
#define  CSIZ_HAS_VIEW                  1
#define  CSIZ_LOT_SQFT                  5
#define  CSIZ_ACREAGE                   7
#define  CSIZ_UNITS                     4
#define  CSIZ_TAXABLE                   1
#define  CSIZ_OWNER_NAME               96
#define  CSIZ_MAILING_ADDR             75
#define  CSIZ_MAILING_ZIP               5
#define  CSIZ_SITUS_NUMBER              5
#define  CSIZ_SITUS_UNIT                3
#define  CSIZ_SITUS_STREET             40
#define  CSIZ_CHANGE_DT                 6
#define  CSIZ_CHANGE_CODE               1
#define  CSIZ_ACTION_DATE               6
#define  CSIZ_ACTION_CODE               1

typedef struct _tSdxChar
{
   char  Apn[CSIZ_APN];
   char  Tra[CSIZ_TRA];
   char  Zoning;
   char  LandUse[CSIZ_LAND_USE];
   char  EffYear[CSIZ_EFF_YR];
   char  BldgSqft[CSIZ_BLDG_SQFT];     
   char  Beds[CSIZ_BEDROOMS];
   char  Baths[CSIZ_BATHS];            // 99V9
   char  GarStalls[CSIZ_GARAGE_STALLS];
   char  Pool;                         // Y or N
   char  View;                         // Y or N
   char  LotSqft[CSIZ_LOT_SQFT];    
   char  Acres[CSIZ_ACREAGE];          // V99
   char  Units[CSIZ_UNITS];
   char  Taxable;
   char  ContractCode;
   char  FractIntCode;
   char  Matital_Status[RSIZ_MARITAL_STATUS];
   char  Owner_Status[RSIZ_OWNER_STATUS];
   char  OwnerName[CSIZ_OWNER_NAME];
   char  MailAddr[CSIZ_MAILING_ADDR];  // addr1 and city separated by '*'
   char  MailZip[CSIZ_MAILING_ZIP];
   char  S_StrNum[CSIZ_SITUS_NUMBER];
   char  S_Unit[CSIZ_SITUS_UNIT];
   char  S_StrName[CSIZ_SITUS_STREET];
   char  ValChgDate[CSIZ_CHANGE_DT];
   char  ValChgCode;
   char  ValActDate[CSIZ_CHANGE_DT];
   char  ValActCode;
} SDX_CHAR;


/*
Format of from (INT-FROM-DATE) and to (INT-TO-DATE) dates is: �MMM nn, YYYY�.  
MMM is alpha month abbreviation, nn is day of month (with leading zero), and 
YYYY is calendar year. Range of date is two (2) years.
*/
#define  SOFF_APN                       1-1
#define  SOFF_DOCDATE                  11-1
#define  SOFF_DOCNUM                   17-1
#define  SOFF_NAME                     25-1
#define  SOFF_SITUS                    86-1
#define  SOFF_SALEPRICE               156-1
#define  SOFF_FILLER1                 165-1
#define  SOFF_LIV_AREA                166-1
#define  SOFF_BEDROOMS                171-1
#define  SOFF_BATHS                   174-1
#define  SOFF_FROM_DATE               177-1
#define  SOFF_TO_DATE                 189-1
#define  SOFF_FILLER2                 201-1

#define  SSIZ_APN                      10 
#define  SSIZ_DOCDATE                   6 
#define  SSIZ_DOCNUM                    8 
#define  SSIZ_NAME                     61 
#define  SSIZ_SITUS                    70 
#define  SSIZ_SALEPRICE                 9 
#define  SSIZ_FILLER1                   1 
#define  SSIZ_LIV_AREA                  5 
#define  SSIZ_BEDROOMS                  3 
#define  SSIZ_BATHS                     3 
#define  SSIZ_FROM_DATE                12 
#define  SSIZ_TO_DATE                  12 
#define  SSIZ_FILLER2                  20

typedef struct t_SdxSale
{  // 202-byte
   char  Apn[SSIZ_APN];
   char  DocDate[SSIZ_DOCDATE];        // YYMMDD
   char  DocNum[SSIZ_DOCNUM];
   char  Buyer[SSIZ_NAME];
   char  Situs[SSIZ_SITUS];
   char  SalePrice[SSIZ_SALEPRICE];
   char  filler1;
   char  BldgSqft[SSIZ_LIV_AREA];
   char  Beds[SSIZ_BEDROOMS];
   char  Baths[SSIZ_BATHS];
   char  FromDate[SSIZ_FROM_DATE];
   char  ToDate[SSIZ_FROM_DATE];
   char  filler2[20];
} SDX_SALE;

#define  HOFF_SALE_FLG                 1
#define  HOFF_FROM_APN                 2
#define  HOFF_APN                      12
#define  HOFF_DOCDATE                  22
#define  HOFF_DOCNUM                   28
#define  HOFF_NAME                     34
#define  HOFF_SALEPRICE                134
#define  HOFF_SITUS                    143
#define  HOFF_FILLER                   214

#define  HSIZ_SALE_FLG                 1
#define  HSIZ_FROM_APN                 10
#define  HSIZ_APN                      10
#define  HSIZ_DOCDATE                  6
#define  HSIZ_DOCNUM                   6
#define  HSIZ_NAME                     100
#define  HSIZ_SALEPRICE                9
#define  HSIZ_SITUS                    71
#define  HSIZ_FILLER                   37

// SaleHist (06/24/2006) file structure
typedef struct t_SdxHSale
{  // 250-byte
   char  SaleFlg;                      // M=multi sale
   char  FromApn[HSIZ_APN];
   char  Apn[HSIZ_APN];
   char  DocDate[HSIZ_DOCDATE];        // YYMMDD
   char  DocNum[HSIZ_DOCNUM];
   char  Buyer[HSIZ_NAME];
   char  SalePrice[HSIZ_SALEPRICE];
   char  Situs[HSIZ_SITUS];
   char  filler[HSIZ_FILLER];
} SDX_HSALE;

// CSV layout 12/16/2014
#define  SDX_GR_DOCNUM                 0
#define  SDX_GR_DOCDATE                1  // YYYYMMDD
#define  SDX_GR_DOCTYPE                2
#define  SDX_GR_DOCTYPESEQ             3  // 1-6
#define  SDX_GR_NAME                   4
#define  SDX_GR_NAMETYPE               5  // 1=grantee or indirect name
                                          // 2=legal 
                                          // 3=grantor or direct name
                                          // 4=APN
                                          // 5=link to doc
                                          // 6=secondary sequence number
                                          // 7=book and page numbers in the format BK##PG##
                                          // 8=doc number, not used
#define  SDX_GR_NAMETYPESEQ            6
#define  SDX_GR_PAGECNT                7
#define  SDX_GR_RS_CODE                8  // R(Relates to Real Property), S(Secures a Construction Loan)
#define  SDX_GR_DOCTAX                 9

#define  GOFF_NAME                     1-1
#define  GOFF_RECDATE                  39-1
#define  GOFF_RECYEAR                  47-1
#define  GOFF_RECNUMBER                51-1
#define  GOFF_MULTICODE                58-1
#define  GOFF_RECORD_TYPE              59-1
#define  GOFF_SEQUENCE_NO              60-1     
#define  GOFF_INST_TYPE                66-1     // This offset increases 2 bytes for new layout
#define  GOFF_REEL_NUMBER              69-1
#define  GOFF_IMAGE_NUMBER             74-1
#define  GOFF_NOPAGES                  79-1
#define  GOFF_RS_CODES                 82-1
#define  GOFF_NAME_CONT                83-1
#define  GOFF_APN                      115-1
#define  GOFF_DOCTAX                   125-1

#define  GSIZ_NAME                     38
#define  GSIZ_RECDATE                  8 
#define  GSIZ_RECYEAR                  4
#define  GSIZ_RECNUMBER                7
#define  GSIZ_MULTICODE                1 
#define  GSIZ_RECORD_TYPE              1 
//#define  GSIZ_SEQUENCE_NO              4
#define  GSIZ_SEQUENCE_NO              6
#define  GSIZ_INST_TYPE                3 
#define  GSIZ_REEL_NUMBER              5 
#define  GSIZ_IMAGE_NUMBER             5 
#define  GSIZ_NOPAGES                  3 
#define  GSIZ_RS_CODES                 1 
#define  GSIZ_NAME_CONT                32
#define  GSIZ_APN                      10
#define  GSIZ_DOCTAX                   9 

#define  GSIZ_GG_COUNT                 1
#define  GSIZ_DOCDATE                  8
#define  GSIZ_DOCNUM                   16
#define  GSIZ_DOCTYPE                  10
#define  GSIZ_AMT                      10
#define  GSIZ_GRANTOR                  52
#define  GSIZ_GRANTEE                  52
#define  GSIZ_LEGALDESC                100
#define  GSIZ_DOCREF                   20

#define  MAX_SALE_RECS                 3
#define  MAX_GRGR_RECS                 3

// Structure for GRGR extension at the end of record
typedef struct _tGrGrExt
{
   char  Apn[GSIZ_APN];
   char  DocDate[GSIZ_DOCDATE];
   char  DocNum[GSIZ_DOCNUM];
   char  DocType[GSIZ_DOCTYPE];
   char  SalePrice[GSIZ_AMT];
   char  Grantor[2][GSIZ_GRANTOR];
   char  Grantee[2][GSIZ_GRANTOR];
   char  ApnMatch;
   char  OwnerMatch;
   char  CRLF[2];
} GRGR_EXT;

typedef struct _tSdxGrGr
{  // 136-bytes
   char  Name        [GSIZ_NAME];
   char  RecDate     [GSIZ_RECDATE];      // YYYYMMDD
   char  RecYear     [GSIZ_RECYEAR];
   char  RecNum      [GSIZ_RECNUMBER];
   char  MultiCode;
   char  Record_Type;                     // 1=grantee, 2=descriptor, 3=grantor
                                          // F=not use, ignore this record
   char  Sequence_No [GSIZ_SEQUENCE_NO];
   char  Inst_Type   [GSIZ_INST_TYPE];
   char  Reel_Number [GSIZ_REEL_NUMBER];
   char  Image_Number[GSIZ_IMAGE_NUMBER];
   char  NoPages     [GSIZ_NOPAGES];
   char  RS_codes;                        // S=secures construction loan
                                          // R=relates to Real Property
   char  Name_Cont   [GSIZ_NAME_CONT];
   char  Apn         [GSIZ_APN];
   char  DocTax      [GSIZ_DOCTAX];       // 9(7)V99
   char  termChar;                        // Z
   char  CrLf[2];
} SDX_GRGR;

#define  OFF_SDXGR_APN           1
#define  OFF_SDXGR_DOCDATE       11
#define  OFF_SDXGR_DOCNUM        19
#define  OFF_SDXGR_INSTTYPE      26
#define  OFF_SDXGR_GRANTEE       29
#define  OFF_SDXGR_GRANTOR       133
#define  OFF_SDXGR_DOCTAX        237
#define  OFF_SDXGR_SALE          246
#define  OFF_SDXGR_TITLE         256
#define  OFF_SDXGR_APN_FLG       278
#define  OFF_SDXGR_OWN_FLG       279

typedef struct t_SdxGRec
{  // 512 bytes
   char  Apn[GSIZ_APN];
   char  DocDate[GSIZ_RECDATE];
   char  DocNum[GSIZ_RECNUMBER];
   char  InstType[GSIZ_INST_TYPE];
   char  Grantee[2][GSIZ_GRANTOR];
   char  Grantor[2][GSIZ_GRANTOR];
   char  DocTax[GSIZ_DOCTAX];
   char  SalePrice[GSIZ_AMT];
   char  DocType[SIZ_SALE1_DOCTYPE];
   char  ApnMatched;
   char  OwnerMatched;
   char  NoneSale;
   char  NoneXfer;
   char  MoreName;
   char  DocRef[GSIZ_DOCREF];
   char  LegalDesc[GSIZ_LEGALDESC];
   char  filler[127];
   char  CRLF[2];
} SDX_GREC;

typedef struct _tSdxName
{
   char  M_Flag[2];
   char  O_Flag[2];
   char  Name[96];
} SDX_NAME;

typedef struct _tSdxDocType
{
   char  DocType;
   char  DocTitle[23];
} SDX_DTYPE;

#define  AOFF_APN                   1-1
#define  AOFF_TRA                   11-1
#define  AOFF_CONTR_CODE            16-1
#define  AOFF_FRACT_INT             17-1
#define  AOFF_MARITAL_STATUS        18-1
#define  AOFF_OWNER_STATUS          20-1
#define  AOFF_OWNER_NAME            22-1
#define  AOFF_MAIL_ADDR             118-1
#define  AOFF_MAIL_ZIP              193-1
#define  AOFF_MAIL_CHGDATE          198-1
#define  AOFF_SITUS_ADDR            206-1
#define  AOFF_SITUS_STRNUM          206-1
#define  AOFF_SITUS_STRFRA          211-1
#define  AOFF_SITUS_STRNAME         214-1
#define  AOFF_DESC_CODE             254-1
#define  AOFF_LEGAL                 255-1
#define  AOFF_NET                   320-1
#define  AOFF_LAND                  329-1
#define  AOFF_IMPR                  338-1
#define  AOFF_PP_VAL                347-1
#define  AOFF_GROSS                 356-1
#define  AOFF_APPEALS_YEAR          365-1
#define  AOFF_EXEMP1_CODE           369-1
#define  AOFF_EXEMP1_VAL            370-1
#define  AOFF_EXEMP2_CODE           379-1
#define  AOFF_EXEMP2_VAL            380-1
#define  AOFF_EXEMP3_CODE           389-1
#define  AOFF_EXEMP3_VAL            390-1
#define  AOFF_DOCNUM                399-1
#define  AOFF_DOCDATE               406-1
#define  AOFF_MAP_NUMBER            414-1
#define  AOFF_ORG_CUT_NUMBER        425-1
#define  AOFF_ORG_CUT_DATE          431-1
#define  AOFF_TRA_CUT_NUMBER        439-1
#define  AOFF_TRA_CUT_DATE          445-1
#define  AOFF_REDRAFT_CUT_NUMBER    453-1
#define  AOFF_REDRAFT_CUT_DATE      459-1
#define  AOFF_OLD_TRA               467-1
#define  AOFF_OLD_APN               472-1
#define  AOFF_ACRES                 482-1
#define  AOFF_UNITS                 490-1
#define  AOFF_ZONE_USE              494-1
#define  AOFF_REDEEMED_FLAG         497-1
#define  AOFF_REDEEMED_YEAR         498-1
#define  AOFF_TAX_STATUS            502-1
#define  AOFF_ASSESSMENT_YEAR       503-1
#define  AOFF_TRANS_DATE            507-1
#define  AOFF_EFF_YEAR              515-1
#define  AOFF_LIV_AREA              519-1
#define  AOFF_BEDROOMS              524-1
#define  AOFF_BATHS                 527-1
#define  AOFF_GARAGE_STALLS         531-1
#define  AOFF_POOL                  534-1
#define  AOFF_VIEW                  535-1
#define  AOFF_LOT_SQFT              536-1
#define  AOFF_SALE_COUNT            545-1
#define  AOFF_S1_RECDATE            546-1
#define  AOFF_S1_DOCNUM             554-1
#define  AOFF_S1_PRICE              561-1
#define  AOFF_S1_MP_FLAG            571-1
#define  AOFF_S2_RECDATE            572-1
#define  AOFF_S2_DOCNUM             580-1
#define  AOFF_S2_PRICE              587-1
#define  AOFF_S2_MP_FLAG            597-1
#define  AOFF_S3_RECDATE            598-1
#define  AOFF_S3_DOCNUM             606-1
#define  AOFF_S3_PRICE              613-1
#define  AOFF_S3_MP_FLAG            623-1
#define  AOFF_DOCTYPE_CODE          624-1
#define  AOFF_DOCTYPE_TEXT          625-1
#define  AOFF_MAIL_ADDR_FLG         645-1
#define  AOFF_MAIL_ADDR_CA          646-1
#define  AOFF_MAIL_ADDR_USA         647-1
#define  AOFF_HAVE_SITUS            648-1
#define  AOFF_SITUS_BROKEN          649-1
#define  AOFF_HAVE_SALE_AMT         650-1
#define  AOFF_SALE_GT_GROSS         651-1
#define  AOFF_LAND_IMP              652-1
#define  AOFF_RATIO                 661-1
#define  AOFF_EXEMP_TOTAL           664-1
#define  AOFF_SALEAMT               673-1
#define  AOFF_SALE_LESS_GROSS       683-1
#define  AOFF_LANDVAL_ACRE          693-1
#define  AOFF_LANDVAL_LOT_SQFT      703-1
#define  AOFF_IMPRVAL_IMP_SQFT      713-1
#define  AOFF_SALEVAL_IMP_SQFT      723-1
#define  AOFF_S_STRNUM              733-1
#define  AOFF_S_STRFRA              740-1
#define  AOFF_S_STRDIR              743-1
#define  AOFF_S_STRNAME             745-1
#define  AOFF_S_STRSFX              775-1
#define  AOFF_S_UNIT                780-1
#define  AOFF_S_STRNUM2             788-1
#define  AOFF_S_CITY                794-1
#define  AOFF_CAREOF                811-1
#define  AOFF_M_STRNUM              861-1
#define  AOFF_M_STRFRA              868-1
#define  AOFF_M_STRDIR              871-1
#define  AOFF_M_STRNAME             873-1
#define  AOFF_M_STRSFX              903-1
#define  AOFF_M_UNIT                909-1
#define  AOFF_M_STRNUM2             916-1
#define  AOFF_M_CITY                922-1
#define  AOFF_M_STATE               939-1
#define  AOFF_M_ZIP                 941-1
#define  AOFF_M_FLAG                950-1
#define  AOFF_HAS_SEQNUM            951-1
#define  AOFF_NAME1_MASSAGE         957-1
#define  AOFF_NAME2_MASSAGE         1007-1
#define  AOFF_NAME1_NORM            1057-1
#define  AOFF_NAME1_FLAG            1107-1
#define  AOFF_NAME2_FLAG            1108-1
#define  AOFF_FILLER1               1109-1

#define  AOFF_GG_COUNT              1111-1
#define  AOFF_1DT                   1112-1
#define  AOFF_1DOC                  1120-1
#define  AOFF_1TYPE                 1136-1
#define  AOFF_1AMT                  1146-1
#define  AOFF_1GRANTOR              1156-1
#define  AOFF_1GRANTEE              1260-1
#define  AOFF_1APN_MATCH            1364-1
#define  AOFF_1NAME_MATCH           1365-1
#define  AOFF_2DT                   1366-1 
#define  AOFF_2DOC                  1374-1
#define  AOFF_2TYPE                 1390-1
#define  AOFF_2AMT                  1400-1
#define  AOFF_2GRANTOR              1410-1 
#define  AOFF_2GRANTEE              1514-1
#define  AOFF_2APN_MATCH            1618-1
#define  AOFF_2NAME_MATCH           1619-1
#define  AOFF_3DT                   1620-1
#define  AOFF_3DOC                  1628-1
#define  AOFF_3TYPE                 1644-1
#define  AOFF_3AMT                  1654-1
#define  AOFF_3GRANTOR              1664-1 
#define  AOFF_3GRANTEE              1768-1
#define  AOFF_3APN_MATCH            1872-1
#define  AOFF_3NAME_MATCH           1873-1
#define  AOFF_LAST_RECDT            1874-1
#define  AOFF_LAST_AMT              1882-1
#define  AOFF_FILLER2               1892-1

#define  ASIZ_REC                   1900
#define  ASIZ_GRGR_DOC              254

#define  ASIZ_APN                   10
#define  ASIZ_TRA                   5 
#define  ASIZ_CONTR_CODE            1 
#define  ASIZ_FRACT_INT             1 
#define  ASIZ_MARITAL_STATUS        2 
#define  ASIZ_OWNER_STATUS          2 
#define  ASIZ_OWNER_NAME            96
#define  ASIZ_MAIL_ADDR             75
#define  ASIZ_MAIL_ZIP              5 
#define  ASIZ_MAIL_CHGDATE          8 
#define  ASIZ_SITUS_ADDR            48 
#define  ASIZ_SITUS_STRNUM          5 
#define  ASIZ_SITUS_STRFRA          3 
#define  ASIZ_SITUS_STRNAME         40
#define  ASIZ_DESC_CODE             1 
#define  ASIZ_LEGAL                 65
#define  ASIZ_NET                   9 
#define  ASIZ_LAND                  9 
#define  ASIZ_IMPR                  9 
#define  ASIZ_PP_VAL                9 
#define  ASIZ_GROSS                 9 
#define  ASIZ_APPEALS_YEAR          4 
#define  ASIZ_EXEMP1_CODE           1 
#define  ASIZ_EXEMP1_VAL            9 
#define  ASIZ_EXEMP2_CODE           1 
#define  ASIZ_EXEMP2_VAL            9 
#define  ASIZ_EXEMP3_CODE           1 
#define  ASIZ_EXEMP3_VAL            9 
#define  ASIZ_DOCNUM                7 
#define  ASIZ_DOCDATE               8 
#define  ASIZ_MAP_NUMBER            11
#define  ASIZ_ORG_CUT_NUMBER        6 
#define  ASIZ_ORG_CUT_DATE          8 
#define  ASIZ_TRA_CUT_NUMBER        6 
#define  ASIZ_TRA_CUT_DATE          8 
#define  ASIZ_REDRAFT_CUT_NUMBER    6 
#define  ASIZ_REDRAFT_CUT_DATE      8 
#define  ASIZ_OLD_TRA               5 
#define  ASIZ_OLD_APN               10
#define  ASIZ_ACRES                 8 
#define  ASIZ_UNITS                 4 
#define  ASIZ_ZONE_USE              3 
#define  ASIZ_REDEEMED_FLAG         1 
#define  ASIZ_REDEEMED_YEAR         4 
#define  ASIZ_TAX_STATUS            1 
#define  ASIZ_ASSESSMENT_YEAR       4 
#define  ASIZ_TRANS_DATE            8 
#define  ASIZ_EFF_YEAR              4 
#define  ASIZ_LIV_AREA              5 
#define  ASIZ_BEDROOMS              3 
#define  ASIZ_BATHS                 4 
#define  ASIZ_GARAGE_STALLS         3 
#define  ASIZ_POOL                  1 
#define  ASIZ_VIEW                  1 
#define  ASIZ_LOT_SQFT              9 
#define  ASIZ_SALE_COUNT            1 
#define  ASIZ_S1_RECDATE            8 
#define  ASIZ_S1_DOCNUM             7 
#define  ASIZ_S1_PRICE              10
#define  ASIZ_S1_MP_FLAG            1 
#define  ASIZ_S2_RECDATE            8 
#define  ASIZ_S2_DOCNUM             7 
#define  ASIZ_S2_PRICE              10 
#define  ASIZ_S2_MP_FLAG            1 
#define  ASIZ_S3_RECDATE            8 
#define  ASIZ_S3_DOCNUM             7 
#define  ASIZ_S3_PRICE              10 
#define  ASIZ_S3_MP_FLAG            1 
#define  ASIZ_DOCTYPE_CODE          1 
#define  ASIZ_DOCTYPE_TEXT          20
#define  ASIZ_MAIL_ADDR_FLG         1 
#define  ASIZ_MAIL_ADDR_CA          1 
#define  ASIZ_MAIL_ADDR_USA         1 
#define  ASIZ_HAVE_SITUS            1 
#define  ASIZ_SITUS_BROKEN          1 
#define  ASIZ_HAVE_SALE_AMT         1 
#define  ASIZ_SALE_GT_GROSS         1 
#define  ASIZ_LAND_IMP              9 
#define  ASIZ_RATIO                 3 
#define  ASIZ_EXEMP_TOTAL           9 
#define  ASIZ_SALEAMT               10
#define  ASIZ_SALE_LESS_GROSS       10 
#define  ASIZ_LANDVAL_ACRE          10
#define  ASIZ_LANDVAL_LOT_SQFT      10
#define  ASIZ_IMPRVAL_IMP_SQFT      10
#define  ASIZ_SALEVAL_IMP_SQFT      10
#define  ASIZ_S_STRNUM              7 
#define  ASIZ_S_STRFRA              3 
#define  ASIZ_S_STRDIR              2 
#define  ASIZ_S_STRNAME             30
#define  ASIZ_S_STRSFX              5 
#define  ASIZ_S_UNIT                8 
#define  ASIZ_S_STRNUM2             6 
#define  ASIZ_S_CITY                17
#define  ASIZ_CAREOF                50
#define  ASIZ_M_STRNUM              7 
#define  ASIZ_M_STRFRA              3 
#define  ASIZ_M_STRDIR              2 
#define  ASIZ_M_STRNAME             30
#define  ASIZ_M_STRSFX              5 
#define  ASIZ_M_UNIT                8 
#define  ASIZ_M_STRNUM2             6 
#define  ASIZ_M_CITY                17
#define  ASIZ_M_STATE               2 
#define  ASIZ_M_ZIP                 9 
#define  ASIZ_M_FLAG                1 
#define  ASIZ_HAS_SEQNUM            6 
#define  ASIZ_NAME1_MASSAGE         50
#define  ASIZ_NAME2_MASSAGE         50
#define  ASIZ_NAME1_NORM            50
#define  ASIZ_NAME1_FLAG            1 
#define  ASIZ_NAME2_FLAG            1
#define  ASIZ_FILLER1               2
#define  ASIZ_FILLER2               8

/*
#define SALE_SIZ_APN       14
#define SALE_SIZ_DOCNUM    12
#define SALE_SIZ_DOCDATE   8
#define SALE_SIZ_DOCTYPE   22
#define SALE_SIZ_SALEPRICE 10
#define SALE_SIZ_SALECODE  2
#define SALE_SIZ_SELLER    48
#define SALE_SIZ_STAMPAMT  10
#define SALE_SIZ_NOPRCLXFR 3
#define SALE_SIZ_BUYER     52
#define SALE_SIZ_CAREOF    52
#define SALE_SIZ_M_ADR1    52
#define SALE_SIZ_M_ADR2    40
#define SALE_SIZ_M_ZIP     5

typedef struct t_CSale_Rec
{
   char  Apn[SALE_SIZ_APN];
   char  DocNum[SALE_SIZ_DOCNUM];
   char  DocDate[SALE_SIZ_DOCDATE];
   char  DocType[SALE_SIZ_DOCTYPE];
   char  SalePrice[SALE_SIZ_SALEPRICE];
   char  SaleCode[SALE_SIZ_SALECODE];
   char  Seller[SALE_SIZ_SELLER];
   char  StampAmt[SALE_SIZ_STAMPAMT];
   char  NumOfPrclXfer[SALE_SIZ_NOPRCLXFR];
   char  COO_Flag;             // Change of ownership
   char  Name1[SALE_SIZ_BUYER];
   char  Name2[SALE_SIZ_BUYER];
   char  CareOf[SALE_SIZ_CAREOF];
   char  MailAdr1[SALE_SIZ_M_ADR1];
   char  MailAdr2[SALE_SIZ_M_ADR2];
   char  MailZip[SALE_SIZ_M_ZIP];
   char  CRLF[2];
} CSAL_REC;
*/

// Secured tax file - secured_weekly_ytd_wits.dat
#define  TSIZ_APN                      10
#define  TSIZ_TRA                      5
#define  TSIZ_OWNERNAME                60
#define  TSIZ_LANDVALUE                10
#define  TSIZ_IMPRVALUE                10
#define  TSIZ_PERSPROPVALUE            10
#define  TSIZ_HOMEOWNERSEXE            10
#define  TSIZ_ALLOTHEREXE              10
#define  TSIZ_NETTAXABLEVALUE          10
#define  TSIZ_TAXRATE                  8
#define  TSIZ_BASETAXAMT               11
#define  TSIZ_SPECIALTAXAMT            12
#define  TSIZ_SPECIALASSMTS            10
#define  TSIZ_TOTALTAXAMT              12
#define  TSIZ_INST1PENALTY             9
#define  TSIZ_INST2PENALTY             9
#define  TSIZ_DELINQUENTCOST           5
#define  TSIZ_TOTALDUEAMT              12
#define  TSIZ_INST1AMT                 11
#define  TSIZ_INST1STATUS              25
#define  TSIZ_INST2AMT                 11
#define  TSIZ_INST2STATUS              25
#define  TSIZ_ROLLYEAR                 4
#define  TSIZ_INST1DELQDATE            8
#define  TSIZ_INST2DELQDATE            8
#define  TSIZ_XFERAPN                  10
#define  TSIZ_CORTACNUMBER             5
#define  TSIZ_MAPNUMBER                11
#define  TSIZ_DOCNUMBER                6
#define  TSIZ_DOCDATE                  6
#define  TSIZ_NEWOWNERNAME             42
#define  TSIZ_MAILINGADDRESS           75
#define  TSIZ_MAILINGZIP               5
#define  TSIZ_SITUSADDRESS             48
#define  TSIZ_SUPLVADDATE              6
#define  TSIZ_SUPLVADCODE              1
#define  TSIZ_SUPLMAILDATE             5
#define  TSIZ_SUPLOWNEDDAYS            3
#define  TSIZ_SUPLTOTALDAYS            3
#define  TSIZ_SUPLPRORATE              9
#define  TSIZ_SUPLPRIORLANDVALUE       10
#define  TSIZ_SUPLPRIORIMPRVALUE       10
#define  TSIZ_SUPLPRIORNETVALUE        10
#define  TSIZ_SUPLESCAPEYEAR           4
#define  TSIZ_SUPLESCAPERATECODE       15
#define  TSIZ_SUPLAUTHNUMBER           6
#define  TSIZ_SUPLOWNEDFROMDATE        5
#define  TSIZ_SUPLOWNEDTODATE          5
#define  TSIZ_SUSPENSEAMT              10
#define  TSIZ_FOURYEARPLANFLAG         1
#define  TSIZ_ORGINSTAMT               11
#define  TSIZ_B1                       11
#define  TSIZ_B2                       11
#define  TSIZ_B3                       11
#define  TSIZ_B4                       11
#define  TSIZ_PAIDDATE                 5

// Support Base, Owner, Supl, Delq
typedef struct _tSdxSecTax
{
   char APN                  [TSIZ_APN                ];    // 1
   char TRA                  [TSIZ_TRA                ];    // 11
   char OwnerName            [TSIZ_OWNERNAME          ];    // 16
   char LandValue            [TSIZ_LANDVALUE          ];    // 76
   char ImprValue            [TSIZ_IMPRVALUE          ];    // 86
   char PersPropValue        [TSIZ_PERSPROPVALUE      ];    // 96
   char HOExe                [TSIZ_HOMEOWNERSEXE      ];    // 106
   char AllOtherExe          [TSIZ_ALLOTHEREXE        ];    // 116
   char NetTaxableValue      [TSIZ_NETTAXABLEVALUE    ];    // 126
   char TaxRate              [TSIZ_TAXRATE            ];    // 136 - Total rate on net value
   char BaseTaxAmt           [TSIZ_BASETAXAMT         ];    // 144 - Total tax on net value
   char SpecialTaxAmt        [TSIZ_SPECIALTAXAMT      ];    // 155
   char SpecialAssmts        [TSIZ_SPECIALASSMTS      ];    // 167 - Fixed amount
   char TotalTaxAmt          [TSIZ_TOTALTAXAMT        ];    // 177
   char Inst1Penalty         [TSIZ_INST1PENALTY       ];    // 189
   char Inst2Penalty         [TSIZ_INST2PENALTY       ];    // 198
   char Delq_Cost            [TSIZ_DELINQUENTCOST     ];    // 207
   char TotalDueAmt          [TSIZ_TOTALDUEAMT        ];    // 212
   char Inst1Amt             [TSIZ_INST1AMT           ];    // 224
   char Inst1Status          [TSIZ_INST1STATUS        ];    // 235 - PAID ON 12/07 or DUE or NO CHARGE
   char Inst2Amt             [TSIZ_INST2AMT           ];    // 260
   char Inst2Status          [TSIZ_INST2STATUS        ];    // 271
   char RollYear             [TSIZ_ROLLYEAR           ];    // 296
   char Inst1DueDate         [TSIZ_INST1DELQDATE      ];    // 300
   char Inst2DueDate         [TSIZ_INST2DELQDATE      ];    // 308
   char XferApn              [TSIZ_XFERAPN            ];    // 316
   char CortacNumber         [TSIZ_CORTACNUMBER       ];    // 326
   char MapNumber            [TSIZ_MAPNUMBER          ];    // 331
   char DocNumber            [TSIZ_DOCNUMBER          ];    // 342
   char DocDate              [TSIZ_DOCDATE            ];    // 348
   char NewOwnerName         [TSIZ_NEWOWNERNAME       ];    // 354
   char M_Addr               [TSIZ_MAILINGADDRESS     ];    // 396
   char M_Zip                [TSIZ_MAILINGZIP         ];    // 471
   char S_Addr               [TSIZ_SITUSADDRESS       ];    // 476
   char SuplVadDate          [TSIZ_SUPLVADDATE        ];    // 524
   char SuplVadCode          [TSIZ_SUPLVADCODE        ];    // 530
   char SuplMailDate         [TSIZ_SUPLMAILDATE       ];    // 531
   char SuplOwnedDays        [TSIZ_SUPLOWNEDDAYS      ];    // 536
   char SuplTotalDays        [TSIZ_SUPLTOTALDAYS      ];    // 539
   char SuplProrate          [TSIZ_SUPLPRORATE        ];    // 542
   char SuplPriorLandValue   [TSIZ_SUPLPRIORLANDVALUE ];    // 551
   char SuplPriorImprValue   [TSIZ_SUPLPRIORIMPRVALUE ];    // 561
   char SuplPriorNetValue    [TSIZ_SUPLPRIORNETVALUE  ];    // 571
   char SuplEscapeYear       [TSIZ_SUPLESCAPEYEAR     ];    // 581
   char SuplEscapeRateCode   [TSIZ_SUPLESCAPERATECODE ];    // 585
   char SuplAuthNumber       [TSIZ_SUPLAUTHNUMBER     ];    // 600
   char SuplOwnedFromDate    [TSIZ_SUPLOWNEDFROMDATE  ];    // 606
   char SuplOwnedToDate      [TSIZ_SUPLOWNEDTODATE    ];    // 611
   char Inst1SuspenseAmt     [TSIZ_SUSPENSEAMT        ];    // 616
   char Inst2SuspenseAmt     [TSIZ_SUSPENSEAMT        ];    // 626
   char FourYearPlanFlag     [TSIZ_FOURYEARPLANFLAG   ];    // 636
   char OrgInstAmt           [TSIZ_ORGINSTAMT         ];    // 637
   char B1                   [TSIZ_B1                 ];    // 648
   char B2                   [TSIZ_B2                 ];    // 659
   char B3                   [TSIZ_B3                 ];    // 670
   char B4                   [TSIZ_B4                 ];    // 681
   char Inst1PaidDate        [TSIZ_PAIDDATE           ];    // 692 - MM/DD
   char Inst2PaidDate        [TSIZ_PAIDDATE           ];    // 697
   char Inst1Check;                                         // 702
   char Inst2Check;                                         // 703
   char CrLf[2];
} SDX_SECTAX;

// Default Secured tax - default_secured_wits.dat
#define  TDSIZ_ACCTTYPECODE           3
#define  TDSIZ_APN                    10
#define  TDSIZ_FILLER1                3
#define  TDSIZ_OWNERNAME              99
#define  TDSIZ_ORGDEFAPN              10
#define  TDSIZ_DEFAMT                 20
#define  TDSIZ_REDPENALTY             17
#define  TDSIZ_REDFEE                 6
#define  TDSIZ_RETURNEDCHECKFEES      6
#define  TDSIZ_TAXSALESFEE            6
#define  TDSIZ_COSTRECOVERYFEE        6
#define  TDSIZ_TOTALDUE               18
#define  TDSIZ_SUSPENSEAMT            17
#define  TDSIZ_DEFAULTCREDIT          17
#define  TDSIZ_TOTALAMTPAIDREG        17
#define  TDSIZ_BALANCEDUE             17
#define  TDSIZ_BILLSTATUS             24
#define  TDSIZ_REDDATE                8
#define  TDSIZ_PAYMENTPLANINT         17
#define  TDSIZ_TOTALAMTPAID5YR        17
#define  TDSIZ_BALANCEDUE5YR          17
#define  TDSIZ_NEXTINSTDUEDATE        8
#define  TDSIZ_NEXTINSTAMT            17
#define  TDSIZ_MININSTDUETODAY        17
#define  TDSIZ_FINALPAYMENTDATE       8
#define  TDSIZ_REDSTATUS              1
#define  TDSIZ_FIVEELIGIBLEFLAG       1
#define  TDSIZ_POWERTOSALESTATUS      1
#define  TDSIZ_MAILINGADDRESS         75
#define  TDSIZ_MAILINGZIP             5
#define  TDSIZ_SITUSADDRESS           48
#define  TDSIZ_LASTPAYMENTDATE5YR     8
#define  TDSIZ_PAYPLANSETUPDATE       8
#define  TDSIZ_INTDUE5YR              17
#define  TDSIZ_INTPAID5YR             17
#define  TDSIZ_SINGLEPAYMENT5YR       17
#define  TDSIZ_ORGPRINCIPLEAMT        17
#define  TDSIZ_NUMOFINSTSPAID         4
#define  TDSIZ_MONTHLYINT             15
#define  TDSIZ_FILLER2                1
#define  TDSIZ_DEFDATE                4
#define  TDSIZ_DISHONOREDCHECK        1

typedef struct _tSdxDefTax
{
   char AcctTypeCode         [TDSIZ_ACCTTYPECODE      ];    // 1
   char Apn                  [TDSIZ_APN               ];    // 4
   char Filler1              [TDSIZ_FILLER1           ];    // 14
   char OwnerName            [TDSIZ_OWNERNAME         ];    // 17
   char OrgDefApn            [TDSIZ_ORGDEFAPN         ];    // 116
   char DefAmt               [TDSIZ_DEFAMT            ];    // 126
   char RedPenalty           [TDSIZ_REDPENALTY        ];    // 146
   char RedFee               [TDSIZ_REDFEE            ];    // 163
   char ReturnedCheckFees    [TDSIZ_RETURNEDCHECKFEES ];    // 169
   char TaxSalesFee          [TDSIZ_TAXSALESFEE       ];    // 175
   char CostRecoveryFee      [TDSIZ_COSTRECOVERYFEE   ];    // 181
   char TotalDue             [TDSIZ_TOTALDUE          ];    // 187
   char SuspenseAmt          [TDSIZ_SUSPENSEAMT       ];    // 205
   char DefaultCredit        [TDSIZ_DEFAULTCREDIT     ];    // 222
   char TotalAmtPaidReg      [TDSIZ_TOTALAMTPAIDREG   ];    // 239
   char BalanceDue           [TDSIZ_BALANCEDUE        ];    // 256
   char BillStatus           [TDSIZ_BILLSTATUS        ];    // 273
   char RedDate              [TDSIZ_REDDATE           ];    // 297
   char PaymentPlanInt       [TDSIZ_PAYMENTPLANINT    ];    // 305
   char TotalAmtPaid5Yr      [TDSIZ_TOTALAMTPAID5YR   ];    // 322
   char BalanceDue5Yr        [TDSIZ_BALANCEDUE5YR     ];    // 339
   char NextinstDueDate      [TDSIZ_NEXTINSTDUEDATE   ];    // 356
   char NextinstAmt          [TDSIZ_NEXTINSTAMT       ];    // 364
   char MininstDueToday      [TDSIZ_MININSTDUETODAY   ];    // 381
   char FinalPaymentDate     [TDSIZ_FINALPAYMENTDATE  ];    // 398
   char RedStatus            [TDSIZ_REDSTATUS         ];    // 406
   char FiveEligibleFlag     [TDSIZ_FIVEELIGIBLEFLAG  ];    // 407
   char PowerToSaleStatus    [TDSIZ_POWERTOSALESTATUS ];    // 408
   char MailingAddress       [TDSIZ_MAILINGADDRESS    ];    // 409
   char MailingZip           [TDSIZ_MAILINGZIP        ];    // 484
   char SitusAddress         [TDSIZ_SITUSADDRESS      ];    // 489
   char LastPaymentDate5Yr   [TDSIZ_LASTPAYMENTDATE5YR];    // 537
   char PayPlanSetupDate     [TDSIZ_PAYPLANSETUPDATE  ];    // 545
   char IntDue5Yr            [TDSIZ_INTDUE5YR         ];    // 553
   char IntPaid5Yr           [TDSIZ_INTPAID5YR        ];    // 570
   char SinglePayment5Yr     [TDSIZ_SINGLEPAYMENT5YR  ];    // 587
   char OrgPrincipleAmt      [TDSIZ_ORGPRINCIPLEAMT   ];    // 604
   char NumOfInstsPaid       [TDSIZ_NUMOFINSTSPAID    ];    // 621
   char MonthlyInt           [TDSIZ_MONTHLYINT        ];    // 625
   char Filler2              [TDSIZ_FILLER2           ];    // 640
   char DefDate              [TDSIZ_DEFDATE           ];    // 641
   char DishonoredCheck      [TDSIZ_DISHONOREDCHECK   ];    // 645
} SDX_DEFTAX;

// TRA_RATE.DAT
#define  TRA_TRA_CODE            0
#define  TRA_FISCAL_YEAR         1
#define  TRA_FUND_NUMBER         2
#define  TRA_RATE                3
#define  TRA_AGENCY              4
#define  TRA_CODE_ID             5

// PHONE_LIST.DAT
#define  FSIZ_FUND_NUM            6
#define  FSIZ_PHONE               10
#define  FSIZ_EXTENSION           5
#define  FSIZ_FUND_PHONE_ID       18
typedef struct _tFundPhone
{
   char  FromFund[FSIZ_FUND_NUM];         // 1
   char  ToFund[FSIZ_FUND_NUM];           // 7
   char  Phone[FSIZ_PHONE];               // 13
   char  Extension[FSIZ_EXTENSION];       // 23
   char  Id[FSIZ_FUND_PHONE_ID];          // 28
} FUNDPHONE;

// Special Assessment
#define  FSIZ_APN                 10
#define  FSIZ_FUND_DESC           20
#define  FSIZ_FUND_AMT            11
#define  FSIZ_REC_KEY             16
#define  FSIZ_DATE                10
typedef struct _tSpecialAsmt
{
   char  Apn[FSIZ_APN];                   // 1
   char  FundNum[FSIZ_FUND_NUM];          // 11
   char  FundDesc[FSIZ_FUND_DESC];        // 17
   char  FundAmt[FSIZ_FUND_AMT];          // 37 - (11,2)
   char  RecKey[FSIZ_REC_KEY];            // 48
} SPECASMT;

// Special_Assessment_5-15-2018.txt
#define SA_APN                      0
#define SA_FUND_NUM                 1
#define SA_FUND_DESC                2
#define SA_FUND_AMT                 3
#define SA_REC_KEY                  4
#define SA_DATE                     5
#define SA_ID                       6

// Enrolled value file
#define EV_ASSESSMENT_YEAR          0
#define EV_APN                      1
#define EV_SITUS_ST_NBR             2
#define EV_SITUS_FRACTION           3
#define EV_SITUS_PRE_DIR            4
#define EV_SITUS_ST_NAME            5
#define EV_SITUS_ST_TYPE            6
#define EV_SITUS_POST_DIR           7
#define EV_SITUS_UNIT_NBR           8
#define EV_SITUS_COMMUNITY          9
#define EV_SITUS_STATE              10
#define EV_SITUS_ZIP                11
#define EV_LAND_VALUE               12
#define EV_IMPR_VALUE               13
#define EV_FIXT_VALUE               14
#define EV_PP_VALUE                 15
#define EV_TOTAL_ASSD_VALUE         16
#define EV_TOTAL_EXE_VALUE          17
#define EV_NET_TAXABLE_VALUE        18
#define EV_VALUE_CHNG_CODE          19

// Factor base year file
#define FV_APN                      0
#define FV_LAND_VALUE               1
#define FV_IMPR_VALUE               2
#define FV_TOTAL_VALUE              3

// Standard codes
// H=Homeowners
// D=Disabled Veterans
// V=Veterans
// C=Church
// L=Library
// M=Museums
// R=Religion
// T=Tribal Housing
// W=Welfare
// U=College
// P=Public School
// E=Cemetery
// I=Hospital
// S=School
// X=Misc exempt
// Y=Military

IDX_TBL4 SDX_Exemption[] = 
{
   "1", "D", 1,1,
   "2", "C", 1,1,
   "3", "W", 1,1,
   "4", "R", 1,1,
   "5", "P", 1,1,
   "6", "E", 1,1,
   "7", "X", 1,1,
   "8", "X", 1,1,    // MISC (LESSORS; FREE LIBRARIES; FREE MUSEUMS)
   "9", "H", 1,1,
   "","",0,0
};

#endif
