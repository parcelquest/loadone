/******************************************************************************
 *
 * To build TUL PQ4 file, we have to merge following files:
 * 1) Roll file (Tul_Roll) 650-byte format
 * 2) Situs file (Tul_Situs)
 * 3) Owner file (Tul_Owner_Cur)
 * 4) Characteristic file (Tul_Char)
 * 5) Sale file (Tul_Sale)
 * 6) Name and Addr file (Tul_Nad)
 * 6) LDR file (Tul_Lien), 2297-byte format
 *
 * These files are in EBCDIC.  They will be translated to ASCII before processing.
 *
 *
 * Run LoadOne with following options:
 *    -U -Xl: load lien
 *    -U    : monthly update
 *
 * 09/11/2006          Start working on TUL using FRE file
 * 09/21/2006 1.2.36   TUL is ready for testing
 * 05/12/2007 1.4.13   Fix problem that TUL has blank records
 * 08/09/2007 1.4.25   Rewrite Tul_LienExtr() and Tul_MergeLien() to support new
 *                     LDR format.  Also changing LDR process to extract lien
 *                     then do regular update.  Only take RollType='A' and
 *                     ignore all other types.
 * 02/15/2008 1.5.5    Adding support for Standard Usecode
 * 03/11/2008 1.5.6    Set flag bCreateUpd=true to trigger update file creation
 * 03/12/2008 1.5.6    Modify Tul_CreateRoll() to create SRT output file instead
 *                     of S01 so the old file is preserved for creation of update file UPD
 * 03/24/2008 1.5.7.2  Format IMAPLINK.
 * 08/14/2008 8.1.4    Modify for 2008 LDR
 * 12/11/2008 8.5.1    Make StrNum left justified
 * 03/16/2009 8.7.0    Format STORIES as 99.0.  We no longer multiply by 10.
 * 06/29/2009 9.1.0    Fix Tul_ExtrLien() and Tul_MergeLien() to include other values.
 * 08/13/2009 9.1.5    Rename Tul_MergeOwnerRec() to Tul_MergeOwner() and modify to
 *                     make it work with both lien & update roll.  Create Tul_Load_LDR()
 *                     and related functions to load LDR.
 * 02/06/2010 9.4.0    Extending LEGAL to LEGAL1
 * 08/03/2010 10.1.9   Add -Xd option and Tul_ExtrLegal() and Tul_MergeLegal() to extract legal
 *                     from monthly roll to populate LDR roll.  Also add EXE_CD and TAX_CODE.
 * 11/11/2010 10.3.2   Extract FEE_ASMT & ORIG_APN and merge them into R01 as ALT_APN & PREV_APN.
 * 07/29/2011 11.0.5   Add Tul_FindHeat() and Tul_FindCool() to use when Tul_FindHC()
 *                     failed to identify new spelling.  Add S_HSENO
 * 11/05/2011 11.4.15  Add Tul_ExtrSale() & Tul_FormatSale() to build history file with -Xs option.
 * 07/24/2012 12.1.3   Fix Tul_MergeMAdr() to correct CARE_OF and Tul_FormatSale()
 *                     to accept date with only month & year.
 * 08/23/2013 13.6.13  Exclude unsecured parcels from roll update.
 * 08/26/2013 13.7.13  Fix Tul_CreateRollRec() by excluding books 800-860 on Apn2.
 * 10/10/2013 13.10.4.3 Use updateVesting() to update Vesting and Etal flag. 
 * 10/11/2013 13.11.0  Adding 1-4 Qbaths to R01.
 * 05/09/2014 13.14.4  Modify Tul_MergeOwner() to keep owner name the same as county provided.
 *                     001014002000 (multi-name)
 * 07/03/2014 14.0.1   Modify Tul_MergeChar() to optimize Heat/Cool translation.
 * 07/08/2015 15.0.2   Add Tul_ExtrLien1(), Tul_CreateR01_1() and Tul_Load_LDR1() to support new layout for LDR 2015.
 * 07/12/2015 15.0.3   Fix bug in Load_Tul()
 * 03/16/2016 15.8.1   Load Tax data.
 * 06/28/2016 16.0.2   Modify Tul_CreateR01_1() to correct TOTAL_EXE, add SITUS to Tul_Load_LDR1().
 *                     Fix FBath in Tul_MergeChar().  Reorganize code in loadTul(), sort situs file for input.
 * 07/25/2016 16.1.1   Modify Tul_MergeMAdr() to support various NAD layout.
 * 06/20/2017 17.0.0   Modify Tul_MergeMAdr() to support 2017 mail addr.  Fix minor bug in Tul_MergePQ4()
 * 01/25/2018 17.6.3   Modify Tul_MergeSale() to filter out bad docs.
 * 12/05/2018 18.6.7   Removing sale update from Tul_MergePQ4() and add -Ms option to loadTul().
 * 06/24/2019 19.0.0   Increase buffer size in Tul_ExtrLien1()
 * 04/27/2020 19.9.0   Remove -Ut and use -T to load both full & partial tax file with TC_LoadTax().
 * 06/20/2020 20.0.0   Add -Xa option. Modify Tul_ExtrLien1() & Tul_CreateR01_1() to add exemption codes.  
 *                     Remove sale merge from Tul_Load_LDR1(). 
 * 07/18/2020 20.1.6   Modify Tul_MergeSitus() to verify direction before assign to S_DIR.
 * 10/30/2020 20.4.1   Modify Tul_MergeChar() & Tul_CreateRollRec() to populate PQZoning.
 * 05/07/2021 20.7.18  Set FirePlace='M' when greater than 9 in Tul_MergeChar() & Tul_FmtChar().
 * 09/13/2021 21.2.3   Modify Tul_MergeSitus() to use mail city if they have the same adr1.
 *                     Modify Tul_MergePQ4() to merge mail adr before situs.
 * 12/01/2021 21.4.2   Fix LotSqft Tul_FmtChar().
 * 06/21/2022 21.9.4   Fix bug to allow -Mz to run alone.
 *
 ******************************************************************************/

#include "stdafx.h"
#include "Prodlib.h"
#include "CountyInfo.h"
#include "R01.h"
#include "RecDef.h"
#include "Tables.h"
#include "Logs.h"
#include "LoadDef.h"
#include "Utils.h"
#include "XlatTbls.h"
#include "doSort.h"
#include "doOwner.h"
#include "formatApn.h"
#include "SaleRec.h"
#include "CharRec.h"
#include "LoadOne.h"
#include "MergeTul.h"
#include "UseCode.h"
#include "LOExtrn.h"
#include "Tax.h"

XLAT_CODE  Tul_Water[] =
{
   "CITY","M",4,
   "PRIV","C",4,
   "PUB" ,"P",3,
   "WELL","W",4,
   "Y"   ,"A",1,
   "","",0
};
XLAT_CODE  Tul_Sewer[] =
{
   "CITY","N",4,
   "PRIV","I",4,
   "PUB" ,"P",3,
   "SEP" ,"S",3,
   "Y"   ,"A",1,
   "","",0
};

static   char  acSitusFile[_MAX_PATH], acNadFile[_MAX_PATH], acOwnerFile[_MAX_PATH];
static   char  acLienExtr[_MAX_PATH], acLegalExtr[_MAX_PATH];
static   FILE  *fdLien, *fdRoll, *fdChar, *fdSale, *fdSitus, *fdNad, *fdOwner, *fdLegal;
static   long  lCharSkip, lSaleSkip, lOwnerSkip, lNadSkip, lSitusSkip, lLienSkip;
static   long  lCharMatch, lSaleMatch, lOwnerMatch, lNadMatch, lSitusMatch, lLienMatch, lLegalMatch;
static   long  lBadBath;

extern   IDX_TBL asSaleTypes[];

/******************************** Tul_MergeOwner *****************************
 *
 *
 *****************************************************************************/

void Tul_MergeOwner(char *pOutbuf, char *pOwner)
{
   int   iTmp;
   char  acOwner1[128], acSave1[128], *pTmp;
   bool  bNoMerge=false, bKeep=false;

   OWNER myOwner;

   // Clear output buffer if needed
   removeNames(pOutbuf, false);

   memcpy(acOwner1, pOwner, LSIZ_CURR_OWNER);
   acOwner1[LSIZ_CURR_OWNER] = 0;
   if (pTmp = strchr(acOwner1, '&'))
   {
      if (*(pTmp-1) != ' ')
      {
         *pTmp++ = 0;
         sprintf(acSave1, "%s & %s", acOwner1, pTmp);
         strcpy(acOwner1, acSave1);
      }
   }

   blankRem(acOwner1);
   strcpy(acSave1, acOwner1);

   // Take out ()
   if (pTmp = strchr(acOwner1, '('))
   {
      replChar(acOwner1, '(', ' ');
      replChar(acOwner1, ')', ' ');
      blankRem(acOwner1);
   }

   // Update vesting
   iTmp = updateVesting(myCounty.acCntyCode, acOwner1, pOutbuf+OFF_VEST, pOutbuf+OFF_ETAL_FLG);

   if (!memcmp(acOwner1, "THE ", 4) && (pTmp=strstr(acOwner1, " TRUST")) )
   {
      bKeep = true;
   } else if ((pTmp=strstr(acOwner1, " FAMILY T"))  ||
              (pTmp=strstr(acOwner1, " REVOCABLE")) ||
              (pTmp=strstr(acOwner1, " TRUST")))
   {
      *pTmp = 0;
      bNoMerge = true;
   }

   if ((pTmp=strstr(acOwner1, " CO-TRS"))  ||
       (pTmp=strstr(acOwner1, " TRS")) ||
       (pTmp=strstr(acOwner1, " TR ")))
      *pTmp = 0;

   // Now parse owners
   if (bNoMerge)
      memcpy(pOutbuf+OFF_NAME_SWAP, acOwner1, strlen(acOwner1));
   else if (bKeep)
      memcpy(pOutbuf+OFF_NAME_SWAP, acSave1, strlen(acSave1));
   else
   {
      iTmp = splitOwner(acOwner1, &myOwner, 5);
      if (iTmp == -1)
         memcpy(pOutbuf+OFF_NAME_SWAP, acSave1, strlen(acSave1));
      else
         memcpy(pOutbuf+OFF_NAME_SWAP, myOwner.acSwapName, strlen(myOwner.acSwapName));
   }

   // Name1
   memcpy(pOutbuf+OFF_NAME1, acSave1, strlen(acSave1));
}

/******************************** Tul_MergeOwner *****************************
 *
 * Parcel may contain multiple Owner records.  We pick first record where
 * Owner_Number is 101 and ignore the rest.
 *
 *****************************************************************************/

int Tul_MergeOwner(char *pOutbuf)
{
   static   char acRec[1024], *pRec=NULL;
   int      iLoop;

   TUL_OWNER *pOwner = (TUL_OWNER *)&acRec[0];

   // Get first Char rec for first call
   if (!pRec)
      pRec = fgets(acRec, 1024, fdOwner);

   pOwner = (TUL_OWNER *)&acRec[0];

   do
   {
      if (!pRec)
      {
         fclose(fdOwner);
         fdOwner = NULL;
         return 1;      // EOF
      }

      // Compare Apn
      iLoop = memcmp(pOutbuf, pOwner->Apn, RSIZ_APN);
      if (iLoop > 0)
      {
         //if (bDebug)
         //   LogMsg0("Skip owner rec  %.*s", RSIZ_APN, pOwner->Apn);
         pRec = fgets(acRec, 1024, fdOwner);
         lOwnerSkip++;
      }
   } while (iLoop > 0);

   // If not match, return
   if (iLoop)
      return 1;

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "003120007", 9))
   //   lTmp = 0;
#endif

   Tul_MergeOwner(pOutbuf, pOwner->Name);

   lOwnerMatch++;

   // Get next Owner rec
   pRec = fgets(acRec, 1024, fdOwner);

   // If this record has the same APN and Owner Number is 101, use this owner
   if (!memcmp(pOutbuf, pOwner->Apn, RSIZ_APN) && !memcmp(pOwner->Owner_Number, "101", 3))
      Tul_MergeOwner(pOutbuf, pOwner->Name);

   return 0;
}

/********************************* Tul_FindHC ********************************
 *
 * Use binary search to look for a value.
 * Return NULL if not found.
 *
 *****************************************************************************/

char *Tul_FindHC(char *pData)
{
   long  iMin, iMdl, iMax, iSiz;
   int   iTmp;
   char  *pRet = NULL;

   iSiz = CSIZ_HEAT_COOL_DESC;
   iMax = TUL_HC_COUNT;
   iMin = 0;

//   if (!memcmp(pData, "\ ", 2))
//      iTmp = 0;

   while (iMin <= iMax)
   {
      iMdl = (iMin + iMax) >> 1;
      iTmp = memcmp(pData, Tul_HeatCool[iMdl].pName, iSiz);
      if (iTmp == 0)
      {
         pRet = Tul_HeatCool[iMdl].pCode;
         break;
      } else if (iTmp < 0)
      {
         iMax = iMdl - 1;
      } else
      {
         iMin = iMdl + 1;
      }
   }

   return pRet;
}

char *Tul_FindHeat(char *pData)
{
   long  iIdx = 0;
   char  *pRet = NULL;

   while (Tul_Heat[iIdx].pName[0] > '0')
   {
      if (strstr(pData, Tul_Heat[iIdx].pName))
      {
         pRet = Tul_Heat[iIdx].pCode;
         break;
      }
      iIdx++;
   }
   return pRet;
}

char *Tul_FindCool(char *pData)
{
   long  iIdx = 0;
   char  *pRet = NULL;

   while (Tul_Cool[iIdx].pName[0] > '0')
   {
      if (strstr(pData, Tul_Cool[iIdx].pName))
      {
         pRet = Tul_Cool[iIdx].pCode;
         break;
      }
      iIdx++;
   }
   return pRet;
}

/********************************* Tul_MergeChar *****************************
 *
 * Will revisit to clean up beds and baths for multi units
 *
 *****************************************************************************/

int Tul_MergeChar(char *pOutbuf)
{
   static   char acRec[1024], *pRec=NULL;
   char     acTmp[256], acCode[32], *pTmp;
   ULONG    lBldgSqft, lLotAcres, lLotSqft, lGarSqft, lCarpSqft;
   int      iRet, iTmp, iLoop, iBeds, iFBath, iHBath, iUnits;
   TUL_CHAR *pChar;

   iRet=iBeds=iFBath=iHBath=0;
   lLotSqft=lBldgSqft=lLotAcres=0;

   // Get first Char rec for first call
   if (!pRec)
      pRec = fgets(acRec, 1024, fdChar);

   pChar = (TUL_CHAR *)pRec;

   do
   {
      if (!pRec)
      {
         fclose(fdChar);
         fdChar = NULL;
         return 1;      // EOF
      }

      // Compare Apn
      iLoop = memcmp(pOutbuf, pChar->Apn, RSIZ_APN);
      if (iLoop > 0)
      {
         //if (bDebug)
         //   LogMsg0("Skip Char rec  %.*s", RSIZ_APN, pChar->Apn);
         pRec = fgets(acRec, 1024, fdChar);
         lCharSkip++;
      }
   } while (iLoop > 0);

   // If not match, return
   if (iLoop)
      return 1;

   // Lot Sqft
   lLotSqft = atoln(pChar->LotSqft, CSIZ_LOT_SQFT);
   if (lLotSqft > 0)
   {
      sprintf(acTmp, "%*u", SIZ_LOT_SQFT, lLotSqft);
      memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);

      lLotAcres = (long)((double)lLotSqft*SQFT_MF_1000);
      sprintf(acTmp, "%*u", SIZ_LOT_ACRES, lLotAcres);
      memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);
   }

   // BldgSqft
   lBldgSqft = atoin(pChar->Main_Bldg_Sqft, CSIZ_MAIN_BLDG_SQFT);
   if (lBldgSqft > 10)
   {
      sprintf(acTmp, "%*d", SIZ_BLDG_SF, lBldgSqft);
      memcpy(pOutbuf+OFF_BLDG_SF, acTmp, SIZ_BLDG_SF);
   }

   // Zoning
   if (pChar->Zoning[0] > '0')
   {
      memcpy(pOutbuf+OFF_ZONE, pChar->Zoning, SIZ_ZONE);
      if (*(pOutbuf+OFF_ZONE_X1) == ' ')
         memcpy(pOutbuf+OFF_ZONE_X1, pChar->Zoning, CSIZ_ZONE);
   }

   // Bldgs
   iTmp = atoin(pChar->Number_Of_Bldgs, CSIZ_NUMBER_OF_BLDGS);
   if (iTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_BLDGS, iTmp);
      memcpy(pOutbuf+OFF_BLDGS, acTmp, SIZ_BLDGS);
   }

   // Rooms
   iTmp = atoin(pChar->Total_Rooms, CSIZ_TOTAL_ROOMS);
   if (iTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_ROOMS, iTmp);
      memcpy(pOutbuf+OFF_ROOMS, acTmp, SIZ_ROOMS);
   }

   // Units
   iUnits = atoin(pChar->Number_Of_Units, CSIZ_NUMBER_OF_UNITS);
   if (iUnits > 0)
   {
      sprintf(acTmp, "%*d", SIZ_UNITS, iUnits);
      memcpy(pOutbuf+OFF_UNITS, acTmp, SIZ_UNITS);
   }

   // Beds
   iBeds = atoin(pChar->Bedrooms, CSIZ_BEDROOMS);
   if (iBeds > 0)
   {
      sprintf(acTmp, "%*d", SIZ_BEDS, iBeds);
      memcpy(pOutbuf+OFF_BEDS, acTmp, SIZ_BEDS);
   }

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "170380003000", 9))
   //   iTmp = 0;
#endif

   // Baths V99 - 25, 50, 75
   iFBath = atoin(pChar->Bathrooms, CSIZ_BATHROOMS-2);
   iHBath = atoin(&pChar->Bathrooms[2], CSIZ_BATHROOMS-2);
   iTmp   = atoin(pChar->Bathrooms, CSIZ_BATHROOMS);
   if (iTmp > 0 && iTmp < 10)
   {
      iHBath = 0;
      iFBath = iTmp;
   } else if (iHBath == 75)
   {
      *(pOutbuf+OFF_BATH_3Q) = '1';
   } else if (iHBath > 0)
   {
      if (iHBath == 25)
         *(pOutbuf+OFF_BATH_1Q) = '1';
      else if (iHBath == 50)
         *(pOutbuf+OFF_BATH_2Q) = '1';
      else
      {
         // Value is too big or invalid
         if (iFBath > 9)
            iFBath = 0;
         iHBath = 0; 
         lBadBath++;
         if (bDebug)
            LogMsg("*** Bad bath value: %.4s", pChar->Bathrooms);
      }
   }
   if (iFBath > 0 && iFBath < 10)
   {
      *(pOutbuf+OFF_BATH_4Q) = iFBath|0x30;
      if (iHBath > 50)
         iFBath++;

      // Full bath
      sprintf(acTmp, "%*d", SIZ_BATH_F, iFBath);
      memcpy(pOutbuf+OFF_BATH_F, acTmp, SIZ_BATH_F);
   }

   if (iHBath > 0)
      memcpy(pOutbuf+OFF_BATH_H, " 1", 2);

   // Heating/Cooling
   //if (pChar->Heat_Cool_Flg == 'Y' && pChar->Heat_Cool_Desc[1] > ' ')
   if (pChar->Heat_Cool_Desc[0] > ' ' || pChar->Heat_Cool_Desc[1] > ' ')
   {
      memcpy(acTmp, pChar->Heat_Cool_Desc, CSIZ_HEAT_COOL_DESC);
      acTmp[CSIZ_HEAT_COOL_DESC] = 0;

      if (acTmp[0] != '\\')
      {
         // Check for most popular one
         //if (!memcmp(acTmp, Tul_HeatCool1[0].pName, 6))
         if (strstr(acTmp, "DUO"))
         {
            *(pOutbuf+OFF_HEAT)     = 'W';
            *(pOutbuf+OFF_AIR_COND) = 'D';
         } else if (!memcmp(acTmp, "CENT", 4))
         {
            *(pOutbuf+OFF_HEAT)     = 'Z';
            *(pOutbuf+OFF_AIR_COND) = 'C';
         } else if (!memcmp(acTmp, "FA/REF", 6) || !memcmp(acTmp, "FAF/REF", 7))
         {
            *(pOutbuf+OFF_HEAT)     = 'B';
            *(pOutbuf+OFF_AIR_COND) = 'A';
         } else if (pTmp = Tul_FindHC(acTmp))
         {
            *(pOutbuf+OFF_HEAT)     = *pTmp++;
            *(pOutbuf+OFF_AIR_COND) = *pTmp;
         } else
         {  
            if (pTmp = Tul_FindHeat(acTmp))
               *(pOutbuf+OFF_HEAT)     = *pTmp;
            else     
            {
               *(pOutbuf+OFF_HEAT)     = 'X';      // No translation for this type, set it to OTHER
               if (bDebug)
                  LogMsg("*** Unknown Heater in: %s", acTmp);
            }
            if (pTmp = Tul_FindCool(acTmp))
               *(pOutbuf+OFF_AIR_COND) = *pTmp;
            else
            {
               *(pOutbuf+OFF_AIR_COND) = 'X';
               if (bDebug)
                  LogMsg("*** Unknown Cooler in: %s", acTmp);
            }
         }
      }
   }

   // YrBlt
   iTmp = atoin(pChar->Const_Year, CSIZ_CONST_YEAR);
   if (iTmp > 1900)
      memcpy(pOutbuf+OFF_YR_BLT, pChar->Const_Year, CSIZ_CONST_YEAR);

   // Quality/class
   if (isalpha(pChar->Const_Type))
   {  // D045
      *(pOutbuf+OFF_BLDG_CLASS) = pChar->Const_Type;
      iTmp = atoin(pChar->QualClass, 2);
      if (iTmp > 0)
      {
         sprintf(acTmp, "%d.%c", iTmp, pChar->QualClass[2]);
         iRet = Value2Code(acTmp, acCode, NULL);
         blankPad(acCode, SIZ_BLDG_QUAL);
         memcpy(pOutbuf+OFF_BLDG_QUAL, acCode, SIZ_BLDG_QUAL);
      }
   }

   // Stories - changed by spn 02/25/2009 - reversed by spn 3/16/2009
   iTmp = atoin(pChar->Stories, CSIZ_STORIES);
   if (iTmp > 0 && iTmp < 99)
   {
      //if (iTmp > 9)
      //   LogMsg("%.12s : %d", pOutbuf, iTmp);
      sprintf(acTmp, "%d.0  ", iTmp);
      memcpy(pOutbuf+OFF_STORIES, acTmp, SIZ_STORIES);
   }

   // Water
   if (pChar->Water_Service[0] > ' ')
   {
      iTmp = 0;
      while (Tul_Water[iTmp].iLen > 0)
      {
         if (!memcmp(pChar->Water_Service, Tul_Water[iTmp].acSrc, Tul_Water[iTmp].iLen))
         {
            *(pOutbuf+OFF_WATER) = Tul_Water[iTmp].acCode[0];
            break;
         }
         iTmp++;
      }
   }

   // Sewer
   if (pChar->Sanitation[0] > ' ')
   {
      iTmp = 0;
      while (Tul_Sewer[iTmp].iLen > 0)
      {
         if (!memcmp(pChar->Sanitation, Tul_Sewer[iTmp].acSrc, Tul_Sewer[iTmp].iLen))
         {
            *(pOutbuf+OFF_SEWER) = Tul_Sewer[iTmp].acCode[0];
            break;
         }
         iTmp++;
      }
   }

   // Garage area
   lGarSqft = atoin(pChar->Garage_Size, CSIZ_GARAGE_SIZE);
   lCarpSqft = atoin(pChar->Carport_Size, CSIZ_GARAGE_SIZE);

   // Check for multi-unit appartment
   if (lCarpSqft > 1000 && iUnits > 4)
   {
      sprintf(acTmp, "%*d", SIZ_GAR_SQFT, lCarpSqft);
      memcpy(pOutbuf+OFF_GAR_SQFT, acTmp, SIZ_GAR_SQFT);
      *(pOutbuf+OFF_PARK_TYPE) = 'C';
   } else if (lGarSqft > 100)
   {
      sprintf(acTmp, "%*d", SIZ_GAR_SQFT, lGarSqft);
      memcpy(pOutbuf+OFF_GAR_SQFT, acTmp, SIZ_GAR_SQFT);
      *(pOutbuf+OFF_PARK_TYPE) = 'Z';
   } else if (lCarpSqft > 100)
   {
      sprintf(acTmp, "%*d", SIZ_GAR_SQFT, lCarpSqft);
      memcpy(pOutbuf+OFF_GAR_SQFT, acTmp, SIZ_GAR_SQFT);
      *(pOutbuf+OFF_PARK_TYPE) = 'C';
   }

   // Pool/Spa
   if (pChar->Pool == 'Y' && pChar->Spa == 'Y')
      *(pOutbuf+OFF_POOL) = 'C';    // Pool/Spa
   else if (pChar->Pool == 'Y')
      *(pOutbuf+OFF_POOL) = 'P';    // Pool
   else if (pChar->Spa == 'Y')
      *(pOutbuf+OFF_POOL) = 'S';    // Spa

   // Fireplace
   memset(pOutbuf+OFF_FIRE_PL, ' ', SIZ_FIRE_PL);
   iTmp = atoin(pChar->FirePlace, CSIZ_FIREPLACE);
   if (iTmp > 9)
      *(pOutbuf+OFF_FIRE_PL) = 'M';
   else if (iTmp > 0)
      *(pOutbuf+OFF_FIRE_PL) = '0' | iTmp;
   else if (pChar->FirePlace[0] > '0')
      LogMsg("*** Bad FirePlace %.*s [%.*s]", CSIZ_FIREPLACE, pChar->FirePlace, iApnLen, pChar->Apn);

   lCharMatch++;

   // Get next Char rec
   pRec = fgets(acRec, 1024, fdChar);
   return 0;
}

/***************************** Tul_MergeSaleRec ******************************
 *
 * Merge new sale data into current sale.  Move other sales accordingly.
 *
 *****************************************************************************/

int Tul_MergeSaleRec(char *pOutbuf, char *pSale)
{
   long     lCurSaleDt, lLstSaleDt, lPrice, lLastAmt;
   char     acTmp[64], *pTmp;
   int      iTmp;

   TUL_SALE *pSaleRec = (TUL_SALE *)pSale;

   lCurSaleDt = atoin(pSaleRec->DocDate, SIZ_SALE1_DT);
   lLstSaleDt = atoin(pOutbuf+OFF_SALE1_DT, SIZ_SALE1_DT);

   if (lCurSaleDt < lLstSaleDt || !lCurSaleDt)
      return -1;

   lPrice = atoin(pSaleRec->SalePrice, SSIZ_SALE_AMT);
   if (!lPrice)
   {
      lPrice = atoin(pSaleRec->Dtt_Amt, SSIZ_DTT_AMT);
      lPrice = (long)((double)lPrice*SALE_FACTOR_100);
   }

   if (lCurSaleDt == lLstSaleDt)
   {
      lLastAmt = atoin(pOutbuf+OFF_SALE1_AMT, SIZ_SALE1_AMT);
      if (!lPrice && lLastAmt > 0)
         return 0;
   } else
   {
      // Move sale2 to sale3
      memcpy(pOutbuf+OFF_SALE3_DOC, pOutbuf+OFF_SALE2_DOC, SIZ_SALE3_DOC);
      memcpy(pOutbuf+OFF_SALE3_DT, pOutbuf+OFF_SALE2_DT, SIZ_SALE3_DT);
      //memcpy(pOutbuf+OFF_SALE3_DOCTYPE, pOutbuf+OFF_SALE2_DOCTYPE, SIZ_SALE3_DOCTYPE);
      memcpy(pOutbuf+OFF_SALE3_AMT, pOutbuf+OFF_SALE2_AMT, SIZ_SALE3_AMT);
      memcpy(pOutbuf+OFF_SALE3_CODE, pOutbuf+OFF_SALE2_CODE, SIZ_SALE3_CODE);
      *(pOutbuf+OFF_AR_CODE3) = *(pOutbuf+OFF_AR_CODE2);

      // Move sale1 to sale2
      memcpy(pOutbuf+OFF_SALE2_DOC, pOutbuf+OFF_SALE1_DOC, SIZ_SALE3_DOC);
      memcpy(pOutbuf+OFF_SALE2_DT, pOutbuf+OFF_SALE1_DT, SIZ_SALE3_DT);
      //memcpy(pOutbuf+OFF_SALE2_DOCTYPE, pOutbuf+OFF_SALE1_DOCTYPE, SIZ_SALE3_DOCTYPE);
      memcpy(pOutbuf+OFF_SALE2_AMT, pOutbuf+OFF_SALE1_AMT, SIZ_SALE3_AMT);
      memcpy(pOutbuf+OFF_SALE2_CODE, pOutbuf+OFF_SALE1_CODE, SIZ_SALE3_CODE);
      *(pOutbuf+OFF_AR_CODE2) = *(pOutbuf+OFF_AR_CODE1);
   }

   // Update current sale
   memcpy(pOutbuf+OFF_SALE1_DOC, pSaleRec->DocNum, SSIZ_DOCNUM);
   memcpy(pOutbuf+OFF_SALE1_DT, pSaleRec->DocDate, SIZ_SALE1_DT);
   //if (isdigit(pSaleRec->DocType[0]))
   //   memcpy(pOutbuf+OFF_SALE1_DOCTYPE, pSaleRec->DocType, 3);
   //else if (pTmp=findDocType(pSaleRec->DocType, acTmp))
   //   memcpy(pOutbuf+OFF_SALE1_DOCTYPE, pTmp, strlen(pTmp));

   // Check for questionable sale amt
   if (lPrice > 1000)
   {
      // Transfer Type
      if (pSaleRec->Transfer_Type[0] > ' ')
      {
         iTmp = 0;
         while (iTmp < MAX_SALETYPE && *asSaleTypes[iTmp].pName)
         {
            if (!memcmp(pSaleRec->Transfer_Type, asSaleTypes[iTmp].pName, SSIZ_TRANSFER_TYPE))
            {
               *(pOutbuf+OFF_SALE1_CODE) = *asSaleTypes[iTmp].pCode;
               break;
            }
            iTmp++;
         }
      }

      sprintf(acTmp, "%*d", SIZ_SALE1_AMT, lPrice);
      memcpy(pOutbuf+OFF_SALE1_AMT, acTmp, SIZ_SALE1_AMT);
   } else
   {
      *(pOutbuf+OFF_SALE1_CODE) = ' ';
      memset(pOutbuf+OFF_SALE1_AMT, ' ', SIZ_SALE1_AMT);
      memset(pOutbuf+OFF_SELLER, ' ', SIZ_SELLER);
   }

   // Seller
   if (pSaleRec->Seller[0] > ' ')
   {
      memcpy(acTmp, pSaleRec->Seller, SIZ_SELLER);
      acTmp[SIZ_SELLER] = 0;
      if (pTmp = strchr(acTmp, '('))
         memset(pTmp, ' ', SIZ_SELLER);
      memcpy(pOutbuf+OFF_SELLER, acTmp, SIZ_SELLER);
   } else
      memset(pOutbuf+OFF_SELLER, ' ', SIZ_SELLER);

   // Update transfers
   lLstSaleDt = atoin(pOutbuf+OFF_TRANSFER_DT, SIZ_TRANSFER_DT);
   if (lCurSaleDt >= lLstSaleDt)
   {
      memcpy(pOutbuf+OFF_TRANSFER_DOC, pSaleRec->DocNum, SSIZ_DOCNUM);
      memcpy(pOutbuf+OFF_TRANSFER_DT, pSaleRec->DocDate, SIZ_TRANSFER_DT);
   }

   // Group sale
   if (pSaleRec->Group_Sale_Flag == 'Y')
      *(pOutbuf+OFF_MULTI_APN) = 'Y';
   else
      *(pOutbuf+OFF_MULTI_APN) = ' ';

   *(pOutbuf+OFF_AR_CODE1) = 'A';
   return 1;
}

/********************************* Tul_MergeSale ******************************
 *
 * Input: Sale file is output from Tul_UpdateSaleHist().
 *
 ******************************************************************************/

int Tul_MergeSale(char *pOutbuf)
{
   static   char  acRec[1024], *pRec=NULL;
   int      iRet, iLoop;
   TUL_SALE *pSale;

   // Get first Char rec for first call
   if (!pRec)
      pRec = fgets(acRec, 1024, fdSale);

   pSale = (TUL_SALE *)&acRec[0];
   //iRet = sizeof(TUL_SALE);
   do
   {
      if (!pRec)
      {
         fclose(fdSale);
         fdSale = NULL;
         return 1;      // EOF
      }

      // Compare Apn
      iLoop = memcmp(pOutbuf, pSale->Apn, RSIZ_APN);
      if (iLoop > 0)
      {
         if (bDebug)
            LogMsg0("Skip Sale rec  %.*s", RSIZ_APN, pSale->Apn);
         pRec = fgets(acRec, 1024, fdSale);
         lSaleSkip++;
      }
   } while (iLoop > 0);

   // If not match, return
   if (iLoop)
      return 1;

   do
   {
#ifdef _DEBUG
//      if (!memcmp(pOutbuf, "10145008", 8))
//         iRet = 0;
#endif

      if (pSale->DocNum[4] == 'R')  
         iRet = Tul_MergeSaleRec(pOutbuf, acRec);

      // Get next sale record
      pRec = fgets(acRec, 1024, fdSale);
      if (!pRec)
      {
         fclose(fdSale);
         fdSale = NULL;
         break;
      }

   } while (!memcmp(pOutbuf, pSale->Apn, RSIZ_APN));

   lSaleMatch++;

   return 0;
}

/******************************** Tul_MergeSitus *****************************
 *
 * Notes:
 *
 * Return 0 if successful, >0 if error
 *
 *****************************************************************************/

int Tul_MergeSitus(char *pOutbuf, char *pLienRec)
{
   char     acTmp[256], acAddr1[64], acCity[32], acCode[32], acUnit[8];
   int      iRet, iTmp, lTmp;
   TUL_LIEN *pSitus = (TUL_LIEN *)pLienRec;

   // Initialize
   acAddr1[0] = 0;
   memcpy(acUnit, pSitus->Unit, SISIZ_S_UNIT);
   blankRem(acUnit, SISIZ_S_UNIT);
   lTmp = atoin(pSitus->StrNum, SISIZ_S_STRNO);
   if (lTmp > 0)
   {
      sprintf(acAddr1, "%d                 ", lTmp);
      memcpy(pOutbuf+OFF_S_STRNUM, acAddr1, SIZ_S_STRNUM);
      memcpy(pOutbuf+OFF_S_HSENO, acAddr1, SIZ_S_HSENO);

      if (acUnit[1] == '/')
      {
         // Move to street fraction if unit is ?/?
         memcpy(pOutbuf+OFF_S_STR_SUB, acUnit, strlen(acUnit));
         sprintf(acAddr1, "%d %s %.2s %.*s %.*s", lTmp, acUnit, pSitus->StrDir, SISIZ_S_STREET, pSitus->StrName, SISIZ_S_STRTYPE, pSitus->StrType);
         acUnit[0] = 0;
      } else
         sprintf(acAddr1, "%d %.2s %.*s %.*s %s", lTmp, pSitus->StrDir, SISIZ_S_STREET, pSitus->StrName, SISIZ_S_STRTYPE, pSitus->StrType, acUnit);
      blankRem(acAddr1);
      memcpy(pOutbuf+OFF_S_ADDR_D, acAddr1, strlen(acAddr1));
   }

   if (pSitus->StrName[0] > ' ')
   {
      memcpy(pOutbuf+OFF_S_STREET, pSitus->StrName, SISIZ_S_STREET);

      if (pSitus->StrDir[0] > ' ')
         memcpy(pOutbuf+OFF_S_DIR, pSitus->StrDir, SIZ_S_DIR);
      
      if (acUnit[0] > ' ')
         memcpy(pOutbuf+OFF_S_UNITNO, acUnit, strlen(acUnit));

      if (pSitus->StrType[0] > ' ')
      {
         memcpy(acTmp, pSitus->StrType, SISIZ_S_STRTYPE);
         myTrim(acTmp, SISIZ_S_STRTYPE);
         iTmp = GetSfxDev(acTmp);
         if (iTmp > 0)
         {
            iRet = sprintf(acCode, "%d", iTmp);
            memcpy(pOutbuf+OFF_S_SUFF, acCode, iRet);
         }
      }
   }

   // City
   if (pSitus->City[0] > ' ')
   {
      memcpy(acTmp, pSitus->City, SISIZ_S_CITY);
      acTmp[3] = 0;
      Abbr2Code(acTmp, acCode, acCity);
      if (acCode[0] > ' ')
      {
         memcpy(pOutbuf+OFF_S_CITY, acCode, strlen(acCode));
         strcat(acCity, " CA");
         memcpy(pOutbuf+OFF_S_CTY_ST_D, acCity, strlen(acCity));
      }
      // State
      memcpy(pOutbuf+OFF_S_ST, "CA", 2);
   }

   return 0;
}

int Tul_MergeSitus(char *pOutbuf)
{
   static   char acRec[1024], *pRec=NULL;
   char     acTmp[256], acAddr1[64], acCity[32], acCode[32], acUnit[8];
   long     lTmp;
   int      iRet, iTmp, iLoop;

   TUL_SITUS   *pSitus = (TUL_SITUS *)&acRec[0];

   // Get first rec for first call
   if (!pRec)
      pRec = fgets(acRec, 1024, fdSitus);

   do
   {
      if (!pRec)
      {
         fclose(fdSitus);
         fdSitus = NULL;
         return 1;      // EOF
      }

      // Compare Apn
      iLoop = memcmp(pOutbuf, pSitus->Apn, RSIZ_APN);
      if (iLoop > 0)
      {
         //if (bDebug)
         //   LogMsg0("Skip situs Addr rec  %.*s", RSIZ_APN, pSitus->Apn);
         pRec = fgets(acRec, 1024, fdSitus);
         lSitusSkip++;
      }
   } while (iLoop > 0);

   // If not match, return
   if (iLoop)
      return 1;

   removeSitus(pOutbuf);

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "02314101", 9))
   //   lTmp = 0;
#endif
   // Initialize
   acAddr1[0] = 0;
   memcpy(acUnit, pSitus->Unit, SISIZ_S_UNIT);
   blankRem(acUnit, SISIZ_S_UNIT);
   lTmp = atoin(pSitus->StrNum, SISIZ_S_STRNO);
   if (lTmp > 0)
   {
      sprintf(acAddr1, "%d          ", lTmp);
      memcpy(pOutbuf+OFF_S_HSENO, acAddr1, SIZ_S_HSENO);
      memcpy(pOutbuf+OFF_S_STRNUM, acAddr1, SIZ_S_STRNUM);
      if (acUnit[1] == '/')
      {
         // Move to street fraction if unit is ?/?
         memcpy(pOutbuf+OFF_S_STR_SUB, acUnit, strlen(acUnit));
         sprintf(acAddr1, "%d %s %.2s %.*s %.*s", lTmp, acUnit, pSitus->StrDir, SISIZ_S_STREET, pSitus->StrName, SISIZ_S_STRTYPE, pSitus->StrType);
         acUnit[0] = 0;
      } else
         sprintf(acAddr1, "%d %.2s %.*s %.*s %s", lTmp, pSitus->StrDir, SISIZ_S_STREET, pSitus->StrName, SISIZ_S_STRTYPE, pSitus->StrType, acUnit);
      iTmp = blankRem(acAddr1);
      memcpy(pOutbuf+OFF_S_ADDR_D, acAddr1, iTmp);
   }

   if (pSitus->StrDir[0] > ' ' && isDirx(pSitus->StrDir))
         memcpy(pOutbuf+OFF_S_DIR, pSitus->StrDir, SIZ_S_DIR);

   if (pSitus->StrName[0] > ' ')
      memcpy(pOutbuf+OFF_S_STREET, pSitus->StrName, SISIZ_S_STREET);

   if (pSitus->StrType[0] > ' ')
   {
      memcpy(acTmp, pSitus->StrType, SISIZ_S_STRTYPE);
      myTrim(acTmp, SISIZ_S_STRTYPE);
      iTmp = GetSfxDev(acTmp);
      if (iTmp > 0)
      {
         iRet = sprintf(acCode, "%d", iTmp);
         memcpy(pOutbuf+OFF_S_SUFF, acCode, iRet);
      }
   }

   if (acUnit[0] > ' ')
      memcpy(pOutbuf+OFF_S_UNITNO, acUnit, strlen(acUnit));

   // City
   iRet = 0;
   if (!memcmp(pOutbuf+OFF_S_STRNUM, pOutbuf+OFF_M_STRNUM, SIZ_S_STRNUM) &&
       !memcmp(pOutbuf+OFF_S_STREET, pOutbuf+OFF_M_STREET, SIZ_S_STREET) &&
       *(pOutbuf+OFF_M_CITY) > ' ')
   {
      memcpy(acCity, pOutbuf+OFF_M_CITY, SIZ_M_CITY);
      blankRem(acCity, SIZ_M_CITY);
      iRet = City2Code(acCity, acCode, pOutbuf);
      if (iRet > 0)
         memcpy(pOutbuf+OFF_S_ZIP, pOutbuf+OFF_M_ZIP, SIZ_M_ZIP);
   } 
   if (!iRet && pSitus->City[0] > ' ')
   {
      memcpy(acTmp, pSitus->City, SISIZ_S_CITY);
      acTmp[3] = 0;
      iRet = Abbr2Code(acTmp, acCode, acCity);
   }

   if (iRet > 0 && acCode[0] > ' ')
   {
      memcpy(pOutbuf+OFF_S_CITY, acCode, strlen(acCode));
      iRet = sprintf(acTmp, "%s, CA %.5s", acCity, pOutbuf+OFF_S_ZIP);
      memcpy(pOutbuf+OFF_S_CTY_ST_D, acTmp, iRet);
   }

   // State
   memcpy(pOutbuf+OFF_S_ST, "CA", 2);
   lSitusMatch++;

   // Get next situs rec
   pRec = fgets(acRec, 1024, fdSitus);
   return 0;
}

/********************************* Tul_MergeMAdr *****************************
 *
 * In 2017, M_Addr is mostly on line 3 & 4. Line 1 & 2 can be blank or C/O
 *
 *****************************************************************************/

void Tul_MergeMAdr(char *pOutbuf, char *pMAdr1, char *pMAdr2, char *pMAdr3, char *pMAdr4)
{

   char     acTmp[256], acAddr1[128], acAddr2[128], acCareOf[64];
   char     *pTmp, *p1, *p2;
   int      iTmp, iAdr1, iAdr2;
   bool     bNoMail = false;

   ADR_REC  sMailAdr;

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "094120046000", 9) )
   //   iTmp = 0;
#endif

   // Clear old Mailing
   //removeMailing(pOutbuf, true);

   if (*pMAdr1 <= ' ' && *pMAdr3 <= ' ')
      return;

   // Init output
   memset((void *)&sMailAdr, 0, sizeof(ADR_REC));
   acCareOf[0] = 0;
   acAddr1[0] = 0;
   acAddr2[0] = 0;
   iAdr1 = NSIZ_ADDR_LINE_3;
   iAdr2 = NSIZ_ADDR_LINE_4;

   if (*pMAdr4 > ' ')
   {
      p2 = pMAdr4;
      if (!_memicmp(pMAdr1, "C/O", 3) || 
          !_memicmp(pMAdr1, "C / O", 5) || 
          !_memicmp(pMAdr1, "DBA ", 4) || 
          !_memicmp(pMAdr1, "ATTN:", 5) || 
          *pMAdr1 == '%')
      {
         memcpy(acCareOf, pMAdr1, NSIZ_ADDR_LINE_1);
         acCareOf[NSIZ_ADDR_LINE_1] = 0;

         // If last word is not a number, drop it
         memcpy(acTmp, pMAdr4, NSIZ_ADDR_LINE_4);
         iTmp = blankRem(acTmp, NSIZ_ADDR_LINE_4);
         if (!isdigit(acTmp[iTmp-1]))
         {
            // This is more likely foreign addr
            if (!_memicmp(pMAdr3, "PO BOX", 5))
            {
               p1 = pMAdr3;
               p2 = pMAdr4;
            } else
            {
               iAdr2 = sprintf(acAddr2, "%.*s %.*s %.*s", NSIZ_ADDR_LINE_3, pMAdr3, NSIZ_ADDR_LINE_4, pMAdr4);
               p2 = acAddr2;
               p1 = pMAdr2;
            }
         } else if (isdigit(*pMAdr3))
         {
            p1 = pMAdr3;
         } else if (isdigit(*pMAdr2))
         {
            // Merge line 2 and 3
            if (!_memicmp(pMAdr3, "PO BOX", 5))
               p1 = pMAdr2;
            else
            {
               iAdr1 = sprintf(acAddr1, "%.*s %.*s %.*s", NSIZ_ADDR_LINE_2, pMAdr2, NSIZ_ADDR_LINE_3, pMAdr3);
               p1 = acAddr1;
            }
         } else
            p1 = pMAdr3;
      } else
      {
         // If last word is not a number, may be foreign addr
         memcpy(acTmp, pMAdr4, NSIZ_ADDR_LINE_4);
         iTmp = blankRem(acTmp, NSIZ_ADDR_LINE_4);
         if (!isdigit(acTmp[iTmp-1]))
         {
            if (*pMAdr2 > ' ')
               p1 = pMAdr2;
            else
               p1 = pMAdr1;

            iAdr2 = sprintf(acAddr2, "%.*s %.*s %.*s", NSIZ_ADDR_LINE_3, pMAdr3, NSIZ_ADDR_LINE_4, pMAdr4);
            p2 = acAddr2;
         } else if (isdigit(*pMAdr3))
         {
            p1 = pMAdr3;
         } else if (isdigit(*pMAdr2))
         {
            // Merge line 2 and 3
            if (!_memicmp(pMAdr3, "PO BOX", 5))
               p1 = pMAdr2;
            else
            {
               if (*pMAdr2 > ' ')
               {
                  iAdr1 = sprintf(acAddr1, "%.*s %.*s %.*s", NSIZ_ADDR_LINE_2, pMAdr2, NSIZ_ADDR_LINE_3, pMAdr3);
                  p1 = acAddr1;
               } else
                  p1 = pMAdr1;
            }
         } else
         {
            // PO BOX
            // DRIVE 152
            p1 = pMAdr3;         
         }

         // Merge line 1 and 2 - This is more likely foreign addr
         if (isdigit(*pMAdr1))
         {
            iAdr1 = sprintf(acAddr1, "%.*s %.*s", NSIZ_ADDR_LINE_1, pMAdr1, NSIZ_ADDR_LINE_2, pMAdr2);
            p1 = acAddr1;
         } 
      }
   } else if (*pMAdr3 > ' ')
   {
      p2 = pMAdr3;
      if (!_memicmp(pMAdr1, "C/O", 3) || 
          !_memicmp(pMAdr1, "C / O", 5) || 
          !_memicmp(pMAdr1, "DBA ", 4) || 
          !_memicmp(pMAdr1, "ATTN:", 5) || 
          *pMAdr1 == '%')
      {
         memcpy(acCareOf, pMAdr1, NSIZ_ADDR_LINE_1);
         acCareOf[NSIZ_ADDR_LINE_1] = 0;
         p1 = pMAdr2;
      } else
      {
         // If last word is not a number, foreign addr
         memcpy(acTmp, pMAdr3, NSIZ_ADDR_LINE_3);
         iTmp = blankRem(acTmp, NSIZ_ADDR_LINE_3);
         if (!isdigit(acTmp[iTmp-1]))
         {
            iAdr2 = sprintf(acAddr2, "%.*s %.*s %.*s", NSIZ_ADDR_LINE_2, pMAdr2, NSIZ_ADDR_LINE_3, pMAdr3);
            p2 = acAddr2;
            p1 = pMAdr1;
            iAdr1 = NSIZ_ADDR_LINE_1;
         } else if (isdigit(*pMAdr1))
         {
            // Merge line 1 and 2
            iAdr1 = sprintf(acAddr1, "%.*s %.*s", NSIZ_ADDR_LINE_1, pMAdr1, NSIZ_ADDR_LINE_2, pMAdr2);
            p1 = acAddr1;
         } else
         {
            p1 = pMAdr2;
         }
      }
   } else if (*pMAdr2 > ' ')
   {
      p2 = pMAdr2;
      p1 = pMAdr1;
      iAdr1 = NSIZ_ADDR_LINE_1;
   } else
   {
      p2 = pMAdr1;
      p1 = NULL;
   }

   // Check for C/O
   if (acCareOf[0] > ' ')
   {
      if (!_memicmp(_strupr(acCareOf), "C / O", 5))
         replStr(acCareOf, "C / O", "C/O");

      iTmp = blankRem(acCareOf);

      if (!_memicmp(acCareOf, "DBA ", 4))
         memcpy(pOutbuf+OFF_DBA, acCareOf, iTmp);
      else
         updateCareOf(pOutbuf, acCareOf, iTmp);      
   }

   if (p1 && *p1 > ' ')
   {
      if (!acAddr1[0])
      {
         memcpy(acAddr1, p1, iAdr1);
         acAddr1[iAdr1] = 0;
      }

      blankRem(acAddr1);
      parseMAdr1(&sMailAdr, _strupr(acAddr1));

      if (sMailAdr.lStrNum > 0)
      {
         iTmp = sprintf(acTmp, "%d", sMailAdr.lStrNum);
         memcpy(pOutbuf+OFF_M_STRNUM, acTmp, iTmp);
         if (sMailAdr.strSub[0] > '0')
         {
            if (sMailAdr.strName[0] > ' ')
            {
               sprintf(acTmp, "%s  ", sMailAdr.strSub);
               memcpy(pOutbuf+OFF_M_STR_SUB, acTmp, SIZ_M_STR_SUB);
            } else
               strcpy(sMailAdr.strName, sMailAdr.strSub);
         }
      }
      blankPad(sMailAdr.strDir, SIZ_M_DIR);
      memcpy(pOutbuf+OFF_M_DIR, sMailAdr.strDir, SIZ_M_DIR);
      blankPad(sMailAdr.strName, SIZ_M_STREET);
      memcpy(pOutbuf+OFF_M_STREET, sMailAdr.strName, SIZ_M_STREET);
      blankPad(sMailAdr.strSfx, SIZ_M_SUFF);
      memcpy(pOutbuf+OFF_M_SUFF, sMailAdr.strSfx, SIZ_M_SUFF);
      if (sMailAdr.Unit[0] > ' ')
      {
         // Remove multiple #
         if (pTmp = strchr(&sMailAdr.Unit[1], '#'))
            *pTmp = 0;

         if (sMailAdr.Unit[0] == '#')
            memcpy(pOutbuf+OFF_M_UNITNO, sMailAdr.Unit, strlen(sMailAdr.Unit));
         else
         {
            *(pOutbuf+OFF_M_UNITNO) = '#';
            memcpy(pOutbuf+OFF_M_UNITNO+1, sMailAdr.Unit, strlen(sMailAdr.Unit));
         }
      }
      memcpy(pOutbuf+OFF_M_ADDR_D, acAddr1, strlen(acAddr1));
   }

   if (*p2 > ' ')
   {
      memcpy(acAddr2, p2, NSIZ_ADDR_LINE_2);
      blankRem(acAddr2, NSIZ_ADDR_LINE_2);
      // Correction
      if (!_memicmp(acAddr2, "FRESNOC A", 9))
         memcpy(acAddr2, "FRESNO CA", 9);

      parseAdr2_1(&sMailAdr, _strupr(acAddr2));
      if (sMailAdr.City[0] > ' ')
      {
         vmemcpy(pOutbuf+OFF_M_CITY, sMailAdr.City, SIZ_M_CITY);
         vmemcpy(pOutbuf+OFF_M_ST, sMailAdr.State, SIZ_M_ST);

         iTmp = sprintf(acTmp, "%s %s", sMailAdr.City, sMailAdr.State);
         memcpy(pOutbuf+OFF_M_CTY_ST_D, acTmp, iTmp);
      }

      iTmp = atoin(sMailAdr.Zip, SIZ_M_ZIP);
      if (iTmp > 400 && strlen(sMailAdr.Zip) >= SIZ_M_ZIP)
      {
         memcpy(pOutbuf+OFF_M_ZIP, sMailAdr.Zip, SIZ_M_ZIP);
         if (isdigit(sMailAdr.Zip4[0]) && strlen(sMailAdr.Zip4) == SIZ_M_ZIP4)
            memcpy(pOutbuf+OFF_M_ZIP4, sMailAdr.Zip4, SIZ_M_ZIP4);
      }
   }
}

void Tul_MergeMAdr_2016(char *pOutbuf, char *pMAdr1, char *pMAdr2, char *pMAdr3, char *pMAdr4)
{

   char     acTmp[256], acAddr1[128], acAddr2[128], acCareOf[64];
   char     *pTmp, *p1, *p2;
   int      iTmp, iAdr1;
   bool     bNoMail = false;

   ADR_REC  sMailAdr;

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "001022006", 9) )
   //   iTmp = 0;
#endif

   // Clear old Mailing
   removeMailing(pOutbuf, true);

   if (*pMAdr1 <= ' ')
      return;

   // Init output
   memset((void *)&sMailAdr, 0, sizeof(ADR_REC));
   acCareOf[0] = 0;
   acAddr1[0] = 0;
   iAdr1 = NSIZ_ADDR_LINE_2;

   if (*pMAdr4 > ' ')
   {
      p2 = pMAdr4;
      if (!_memicmp(pMAdr1, "C/O", 3) || !_memicmp(pMAdr1, "C / O", 5) || *pMAdr1 == '%')
      {
         memcpy(acCareOf, pMAdr1, NSIZ_ADDR_LINE_1);
         acCareOf[NSIZ_ADDR_LINE_1] = 0;

         // If last word is not a number, drop it
         memcpy(acTmp, pMAdr4, NSIZ_ADDR_LINE_4);
         blankRem(acTmp, NSIZ_ADDR_LINE_4);
         if (!isdigit(acTmp[strlen(acTmp)-1]))
         {
            // This is more likely foreign addr
            p2 = pMAdr3;
            p1 = pMAdr2;
         } else if (isdigit(*pMAdr2))
         {
            // Merge line 2 and 3
            iAdr1 = sprintf(acAddr1, "%.*s %.*s", NSIZ_ADDR_LINE_2, pMAdr2, NSIZ_ADDR_LINE_3, pMAdr3);
            p1 = acAddr1;
         } else
            p1 = pMAdr3;
      } else
      {
         // If last word is not a number, drop it
         memcpy(acTmp, pMAdr4, NSIZ_ADDR_LINE_4);
         blankRem(acTmp, NSIZ_ADDR_LINE_4);
         if (!isdigit(acTmp[strlen(acTmp)-1]))
            p2 = pMAdr3;

         // Merge line 1 and 2 - This is more likely foreign addr
         if (isdigit(*pMAdr1))
         {
            iAdr1 = sprintf(acAddr1, "%.*s %.*s", NSIZ_ADDR_LINE_1, pMAdr1, NSIZ_ADDR_LINE_2, pMAdr2);
            p1 = acAddr1;
         } else if (isdigit(*pMAdr2))
            p1 = pMAdr2;
         else
            p1 = pMAdr3;
      }
   } else if (*pMAdr3 > ' ')
   {
      p2 = pMAdr3;
      if (!_memicmp(pMAdr1, "C/O", 3) || !_memicmp(pMAdr1, "C / O", 5) || *pMAdr1 == '%')
      {
         memcpy(acCareOf, pMAdr1, NSIZ_ADDR_LINE_1);
         acCareOf[NSIZ_ADDR_LINE_1] = 0;

         p1 = pMAdr2;
      } else
      {
         // If last word is not a number, drop it
         memcpy(acTmp, pMAdr3, NSIZ_ADDR_LINE_3);
         blankRem(acTmp, NSIZ_ADDR_LINE_3);
         if (!isdigit(acTmp[strlen(acTmp)-1]))
         {
            p2 = pMAdr2;
            p1 = pMAdr1;
            iAdr1 = NSIZ_ADDR_LINE_1;
         } else if (isdigit(*pMAdr1))
         {
            // Merge line 2 and 3
            iAdr1 = sprintf(acAddr1, "%.*s %.*s", NSIZ_ADDR_LINE_1, pMAdr1, NSIZ_ADDR_LINE_2, pMAdr2);
            p1 = acAddr1;
         } else
         {
            p1 = pMAdr2;
            // Take everything except DBA
            if (memcmp(pMAdr1, "DBA ", 4))
            {
               memcpy(acCareOf, pMAdr1, NSIZ_ADDR_LINE_1);
               acCareOf[NSIZ_ADDR_LINE_1] = 0;
            }
         }
      }
   } else if (*pMAdr2 > ' ')
   {
      p2 = pMAdr2;
      p1 = pMAdr1;
      iAdr1 = NSIZ_ADDR_LINE_1;
   } else
   {
      p2 = pMAdr1;
      p1 = NULL;
   }

   // Check for C/O
   if (acCareOf[0])
   {
      if (!_memicmp(acCareOf, "C / O", 5))
         replStr(acCareOf, "C / O", "C/O");

      iTmp = blankRem(acCareOf);
      updateCareOf(pOutbuf, acCareOf, iTmp);
   }

   if (p1)
   {
      if (!acAddr1[0])
      {
         memcpy(acAddr1, p1, iAdr1);
         acAddr1[iAdr1] = 0;
      }

      blankRem(acAddr1);
      parseMAdr1(&sMailAdr, acAddr1);

      if (sMailAdr.lStrNum > 0)
      {
         iTmp = sprintf(acTmp, "%d", sMailAdr.lStrNum);
         memcpy(pOutbuf+OFF_M_STRNUM, acTmp, iTmp);
         if (sMailAdr.strSub[0] > '0')
         {
            sprintf(acTmp, "%s  ", sMailAdr.strSub);
            memcpy(pOutbuf+OFF_M_STR_SUB, acTmp, SIZ_M_STR_SUB);
         }
      }
      blankPad(sMailAdr.strDir, SIZ_M_DIR);
      memcpy(pOutbuf+OFF_M_DIR, sMailAdr.strDir, SIZ_M_DIR);
      blankPad(sMailAdr.strName, SIZ_M_STREET);
      memcpy(pOutbuf+OFF_M_STREET, sMailAdr.strName, SIZ_M_STREET);
      blankPad(sMailAdr.strSfx, SIZ_M_SUFF);
      memcpy(pOutbuf+OFF_M_SUFF, sMailAdr.strSfx, SIZ_M_SUFF);
      if (sMailAdr.Unit[0] > ' ')
      {
         // Remove multiple #
         if (pTmp = strchr(&sMailAdr.Unit[1], '#'))
            *pTmp = 0;

         if (sMailAdr.Unit[0] == '#')
            memcpy(pOutbuf+OFF_M_UNITNO, sMailAdr.Unit, strlen(sMailAdr.Unit));
         else
         {
            *(pOutbuf+OFF_M_UNITNO) = '#';
            memcpy(pOutbuf+OFF_M_UNITNO+1, sMailAdr.Unit, strlen(sMailAdr.Unit));
         }
      }
      memcpy(pOutbuf+OFF_M_ADDR_D, acAddr1, strlen(acAddr1));
   }

   memcpy(acAddr2, p2, NSIZ_ADDR_LINE_2);
   blankRem(acAddr2, NSIZ_ADDR_LINE_2);
   // Correction
   if (!memcmp(acAddr2, "FRESNOC A", 9))
      memcpy(acAddr2, "FRESNO CA", 9);

   parseAdr2_1(&sMailAdr, acAddr2);
   if (sMailAdr.City[0] > ' ')
   {
      iTmp = strlen(sMailAdr.City);
      if (iTmp > SIZ_M_CITY)
         iTmp = SIZ_M_CITY;
      memcpy(pOutbuf+OFF_M_CITY, sMailAdr.City, iTmp);
      memcpy(pOutbuf+OFF_M_ST, sMailAdr.State, strlen(sMailAdr.State));

      iTmp = sprintf(acTmp, "%s %s", sMailAdr.City, sMailAdr.State);
      memcpy(pOutbuf+OFF_M_CTY_ST_D, acTmp, iTmp);
   }

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "001011017", 9))
   //   iTmp = 0;
#endif

   iTmp = atoin(sMailAdr.Zip, SIZ_M_ZIP);
   if (iTmp > 400 && strlen(sMailAdr.Zip) >= SIZ_M_ZIP)
   {
      memcpy(pOutbuf+OFF_M_ZIP, sMailAdr.Zip, SIZ_M_ZIP);
      if (isdigit(sMailAdr.Zip4[0]) && strlen(sMailAdr.Zip4) == SIZ_M_ZIP4)
         memcpy(pOutbuf+OFF_M_ZIP4, sMailAdr.Zip4, SIZ_M_ZIP4);
   }
}

void Tul_MergeMAdr(char *pOutbuf, char *pNadRec)
{

   char     acTmp[256], acAddr1[128], acAddr2[128], acCareOf[64];
   char     sLine[4][50], *pTmp, *p1, *p2;
   int      iTmp, iAdr1, iLineCnt, iLineIdx=0;
   bool     bNoMail = false;

   ADR_REC  sMailAdr;
   TUL_NAD  *pRec;

   pRec = (TUL_NAD *)pNadRec;

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "001022006", 9) )
   //   iTmp = 0;
#endif

   memset(&sLine[0][0], 0, 200);
   if (pRec->Addr_Line_1[0] > ' ')
   {
      memcpy(sLine[iLineIdx], pRec->Addr_Line_1, NSIZ_ADDR_LINE_1);
      blankRem(sLine[iLineIdx], NSIZ_ADDR_LINE_1);
      _strupr(sLine[iLineIdx]);
      iLineIdx++;
   }
   if (pRec->Addr_Line_2[0] > ' ')
   {
      memcpy(sLine[iLineIdx], pRec->Addr_Line_2, NSIZ_ADDR_LINE_2);
      blankRem(sLine[iLineIdx], NSIZ_ADDR_LINE_2);
      _strupr(sLine[iLineIdx]);
      iLineIdx++;
   }
   if (pRec->Addr_Line_3[0] > ' ')
   {
      memcpy(sLine[iLineIdx], pRec->Addr_Line_3, NSIZ_ADDR_LINE_3);
      blankRem(sLine[iLineIdx], NSIZ_ADDR_LINE_3);
      _strupr(sLine[iLineIdx]);
      iLineIdx++;
   }
   if (pRec->Addr_Line_4[0] > ' ')
   {
      memcpy(sLine[iLineIdx], pRec->Addr_Line_4, NSIZ_ADDR_LINE_4);
      blankRem(sLine[iLineIdx], NSIZ_ADDR_LINE_4);
      _strupr(sLine[iLineIdx]);
      iLineIdx++;
   }

   // If no mailing, return
   if (!iLineIdx)
      return;

   iLineCnt = iLineIdx;

   // Clear old Mailing
   removeMailing(pOutbuf, true);

   // Init output
   memset((void *)&sMailAdr, 0, sizeof(ADR_REC));
   acCareOf[0] = 0;
   acAddr1[0] = 0;
   iAdr1 = NSIZ_ADDR_LINE_2;

   if (iLineCnt == 4)
   {
      p2 = sLine[3];
      if (!_memicmp(sLine[0], "C/O", 3) || !_memicmp(sLine[0], "C / O", 5) || sLine[0][0] == '%')
      {
         memcpy(acCareOf, sLine[0], NSIZ_ADDR_LINE_1);
         acCareOf[NSIZ_ADDR_LINE_1] = 0;

         // If last word is not a number, drop it
         memcpy(acTmp, sLine[3], NSIZ_ADDR_LINE_4);
         blankRem(acTmp, NSIZ_ADDR_LINE_4);
         if (!isdigit(acTmp[strlen(acTmp)-1]))
         {
            // This is more likely foreign addr
            p2 = sLine[2];
            p1 = sLine[1];
         } else if (isdigit(*sLine[1]))
         {
            // Merge line 2 and 3
            iAdr1 = sprintf(acAddr1, "%.*s %.*s", NSIZ_ADDR_LINE_2, sLine[1], NSIZ_ADDR_LINE_3, sLine[2]);
            p1 = acAddr1;
         } else
            p1 = sLine[2];
      } else
      {
         // If last word is not a number, drop it
         memcpy(acTmp, sLine[3], NSIZ_ADDR_LINE_4);
         blankRem(acTmp, NSIZ_ADDR_LINE_4);
         if (!isdigit(acTmp[strlen(acTmp)-1]))
            p2 = sLine[2];

         // Merge line 1 and 2 - This is more likely foreign addr
         if (isdigit(*sLine[0]))
         {
            iAdr1 = sprintf(acAddr1, "%.*s %.*s", NSIZ_ADDR_LINE_1, sLine[0], NSIZ_ADDR_LINE_2, sLine[1]);
            p1 = acAddr1;
         } else if (isdigit(*sLine[1]))
            p1 = sLine[1];
         else
            p1 = sLine[2];
      }
   } else if (iLineCnt == 3)
   {
      p2 = sLine[2];
      if (!_memicmp(sLine[0], "C/O", 3) || !_memicmp(sLine[0], "C / O", 5) || *sLine[0] == '%')
      {
         memcpy(acCareOf, sLine[0], NSIZ_ADDR_LINE_1);
         acCareOf[NSIZ_ADDR_LINE_1] = 0;

         p1 = sLine[1];
      } else
      {
         // If last word is not a number, drop it
         memcpy(acTmp, sLine[2], NSIZ_ADDR_LINE_3);
         blankRem(acTmp, NSIZ_ADDR_LINE_3);
         if (!isdigit(acTmp[strlen(acTmp)-1]))
         {
            p2 = sLine[1];
            p1 = sLine[0];
            iAdr1 = NSIZ_ADDR_LINE_1;
         } else if (isdigit(*sLine[0]))
         {
            // Merge line 2 and 3
            iAdr1 = sprintf(acAddr1, "%.*s %.*s", NSIZ_ADDR_LINE_1, sLine[0], NSIZ_ADDR_LINE_2, sLine[1]);
            p1 = acAddr1;
         } else
               p1 = sLine[1];
      }
   } else if (iLineCnt = 2)
   {
      p2 = sLine[1];
      p1 = sLine[0];
      iAdr1 = NSIZ_ADDR_LINE_1;
   } else
   {
      p2 = sLine[0];
      p1 = NULL;
   }

   // Check for C/O
   if (acCareOf[0])
   {
      if (!_memicmp(acCareOf, "C / O", 5))
         replStr(acCareOf, "C / O", "C/O");

      iTmp = blankRem(acCareOf);
      updateCareOf(pOutbuf, acCareOf, iTmp);
   }

   if (p1)
   {
      if (!acAddr1[0])
      {
         memcpy(acAddr1, p1, iAdr1);
         acAddr1[iAdr1] = 0;
      }

      blankRem(acAddr1);
      parseMAdr1(&sMailAdr, acAddr1);

      if (sMailAdr.lStrNum > 0)
      {
         iTmp = sprintf(acTmp, "%d", sMailAdr.lStrNum);
         memcpy(pOutbuf+OFF_M_STRNUM, acTmp, iTmp);
         if (sMailAdr.strSub[0] > '0')
         {
            sprintf(acTmp, "%s  ", sMailAdr.strSub);
            memcpy(pOutbuf+OFF_M_STR_SUB, acTmp, SIZ_M_STR_SUB);
         }
      }
      vmemcpy(pOutbuf+OFF_M_DIR, sMailAdr.strDir, SIZ_M_DIR);
      vmemcpy(pOutbuf+OFF_M_STREET, sMailAdr.strName, SIZ_M_STREET);
      vmemcpy(pOutbuf+OFF_M_SUFF, sMailAdr.strSfx, SIZ_M_SUFF);
      if (sMailAdr.Unit[0] > ' ')
      {
         // Remove multiple #
         if (pTmp = strchr(&sMailAdr.Unit[1], '#'))
            *pTmp = 0;

         if (sMailAdr.Unit[0] == '#')
            memcpy(pOutbuf+OFF_M_UNITNO, sMailAdr.Unit, strlen(sMailAdr.Unit));
         else
         {
            *(pOutbuf+OFF_M_UNITNO) = '#';
            memcpy(pOutbuf+OFF_M_UNITNO+1, sMailAdr.Unit, strlen(sMailAdr.Unit));
         }
      }
      memcpy(pOutbuf+OFF_M_ADDR_D, acAddr1, strlen(acAddr1));
   }

   memcpy(acAddr2, p2, NSIZ_ADDR_LINE_2);
   blankRem(acAddr2, NSIZ_ADDR_LINE_2);
   // Correction
   if (!memcmp(acAddr2, "FRESNOC A", 9))
      memcpy(acAddr2, "FRESNO CA", 9);

   parseAdr2_1(&sMailAdr, acAddr2);
   if (sMailAdr.City[0] > ' ')
   {
      vmemcpy(pOutbuf+OFF_M_CITY, sMailAdr.City, SIZ_M_CITY);
      memcpy(pOutbuf+OFF_M_ST, sMailAdr.State, strlen(sMailAdr.State));

      iTmp = sprintf(acTmp, "%s %s", sMailAdr.City, sMailAdr.State);
      memcpy(pOutbuf+OFF_M_CTY_ST_D, acTmp, iTmp);
   }

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "001011017", 9))
   //   iTmp = 0;
#endif

   iTmp = atoin(sMailAdr.Zip, SIZ_M_ZIP);
   if (iTmp > 400 && strlen(sMailAdr.Zip) >= SIZ_M_ZIP)
   {
      memcpy(pOutbuf+OFF_M_ZIP, sMailAdr.Zip, SIZ_M_ZIP);
      if (isdigit(sMailAdr.Zip4[0]) && strlen(sMailAdr.Zip4) == SIZ_M_ZIP4)
         memcpy(pOutbuf+OFF_M_ZIP4, sMailAdr.Zip4, SIZ_M_ZIP4);
   }
}

/******************************** Tul_MergeNad *******************************
 *
 * NAD file contains mail addr
 *
 *****************************************************************************/

int Tul_MergeNad(char *pOutbuf)
{
   static   char acRec[1024], *pRec=NULL;
   int      iLoop;

   TUL_NAD  *pNad = (TUL_NAD *)&acRec[0];

   if (!pRec)
      pRec = fgets(acRec, 1024, fdNad);

   pNad = (TUL_NAD *)pRec;

   do
   {
      if (!pRec)
      {
         fclose(fdNad);
         fdNad = NULL;
         return 1;      // EOF
      }

      // Compare Apn
      iLoop = memcmp(pOutbuf, pNad->Apn, RSIZ_APN);
      if (iLoop > 0)
      {
         //if (bDebug)
         //   LogMsg0("Skip NAD rec  %.*s", RSIZ_APN, pNad->Apn);
         pRec = fgets(acRec, 1024, fdNad);
         lNadSkip++;
      }
   } while (iLoop > 0);

   // If not match, return
   if (iLoop)
      return 1;

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "003120007", 9))
   //   lTmp = 0;
#endif

   Tul_MergeMAdr(pOutbuf, pRec);

   lNadMatch++;

   // Get next Char rec
   pRec = fgets(acRec, 1024, fdNad);
   return 0;
}

/********************************* Tul_MergeLien *****************************
 *
 * Return 0 if successful, >0 if error
 *
 *****************************************************************************/

int Tul_MergeLien(char *pOutbuf)
{
   static   char acRec[512], *pRec=NULL;
   int      iLoop;

   LIENEXTR *pLien = (LIENEXTR *)&acRec[0];

   // Get first Char rec for first call
   if (!pRec)
      pRec = fgets(acRec, 512, fdLien);

   do
   {
      if (!pRec)
      {
         fclose(fdLien);
         fdLien = NULL;
         return 1;      // EOF
      }

      // Compare Apn
      iLoop = memcmp(pOutbuf, pLien->acApn, iApnLen);
      if (iLoop > 0)
      {
         if (bDebug)
            LogMsg0("Skip Char rec  %.*s", RSIZ_APN, pLien->acApn);
         pRec = fgets(acRec, 1024, fdLien);
         lLienSkip++;
      }
   } while (iLoop > 0);

   // If not match, return
   if (iLoop)
      return 1;

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "003120007", 9))
   //   lTmp = 0;
#endif
   // H/O exempt
   *(pOutbuf+OFF_HO_FL) = pLien->acHO[0];
   memcpy(pOutbuf+OFF_EXE_TOTAL, pLien->acExAmt, SIZ_EXE_TOTAL);

   // Land
   memcpy(pOutbuf+OFF_LAND, pLien->acLand, SIZ_LAND);

   // Improve
   memcpy(pOutbuf+OFF_IMPR, pLien->acImpr, SIZ_IMPR);

   // Others
   memcpy(pOutbuf+OFF_OTHER, pLien->acOther, SIZ_OTHER);

   // Gross
   memcpy(pOutbuf+OFF_GROSS, pLien->acGross, SIZ_GROSS);

   // Ratio
   memcpy(pOutbuf+OFF_RATIO, pLien->acRatio, SIZ_RATIO);

   // Other values
   long lFixture= atoin(pLien->acME_Val, sizeof(pLien->acME_Val));
   long lPers   = atoin(pLien->acPP_Val, sizeof(pLien->acPP_Val));
   long lBusInv = atoin(pLien->extra.Tul.Bus_Inv, sizeof(pLien->extra.Tul.Bus_Inv));
   long lGrow   = atoin(pLien->extra.Tul.GrowImpr, sizeof(pLien->extra.Tul.GrowImpr));
   char acTmp[32];

   // Fixture/Machine/Equipment
   if (lFixture > 0)
   {
      sprintf(acTmp, "%d         ", lFixture);
      memcpy(pOutbuf+OFF_FIXTR, acTmp, SIZ_FIXTR);
   }
   if (lPers > 0)
   {
      sprintf(acTmp, "%d         ", lPers);
      memcpy(pOutbuf+OFF_PERSPROP, acTmp, SIZ_PERSPROP);
   }
   // Growing impr
   if (lGrow > 0)
   {
      sprintf(acTmp, "%d         ", lGrow);
      memcpy(pOutbuf+OFF_GR_IMPR, acTmp, SIZ_GR_IMPR);
   }

   // Business inventory
   if (lBusInv > 0)
   {
      sprintf(acTmp, "%d         ", lBusInv);
      memcpy(pOutbuf+OFF_BUSINV, acTmp, SIZ_BUSINV);
   }

   // PrevApn
   if (pLien->extra.Tul.PrevApn[0] > ' ')
      memcpy(pOutbuf+OFF_PREV_APN, pLien->extra.Tul.PrevApn, SIZ_LIEN_APN);

   // FeeAsmt
   if (pLien->extra.Tul.FeeAsmt[0] > ' ')
      memcpy(pOutbuf+OFF_ALT_APN, pLien->extra.Tul.FeeAsmt, SIZ_LIEN_APN);

   lLienMatch++;

   // Get next Char rec
   pRec = fgets(acRec, 1024, fdLien);
   return 0;
}

/********************************** Tul_MergePQ4 ****************************
 *
 * Merge Owner, Situs, & Chars to R01 file
 * Raw file to be update is the sorted output file from Tul_CreateRoll()
 * - We have to merge mail addr before situs since we may use mail city for situs.
 *
 ****************************************************************************/

int Tul_MergePQ4(int iSkip)
{
   char     acBuf[MAX_RECSIZE];
   char     acRawFile[_MAX_PATH], acOutFile[_MAX_PATH];

   HANDLE   fhIn, fhOut;

   int      iRet;
   DWORD    nBytesRead;
   DWORD    nBytesWritten;
   BOOL     bRet;
   long     lRet=0, lCnt=0;

   sprintf(acRawFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "TMP");
   sprintf(acOutFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "R01");
   LogMsg0("Update roll file");

   // Rename files for processing
   if (_access(acRawFile, 0))
   {
      LogMsg("***** Missing input file %s.  Please recheck!", acRawFile);
      return 1;
   }

   // Open Lien extract file
   sprintf(acLienExtr, acLienTmpl, myCounty.acCntyCode, myCounty.acCntyCode);
   LogMsg("Open Lien file %s", acLienExtr);
   fdLien = fopen(acLienExtr, "r");
   if (fdLien == NULL)
   {
      LogMsg("***** Error opening Lien file: %s\n", acLienExtr);
      return 2;
   }

   // Open Situs file
   LogMsg("Open Situs file %s", acSitusFile);
   fdSitus = fopen(acSitusFile, "r");
   if (fdSitus == NULL)
   {
      LogMsg("***** Error opening Situs file: %s\n", acSitusFile);
      return 2;
   }

   // Open Char file
   LogMsg("Open Char file %s", acCharFile);
   fdChar = fopen(acCharFile, "r");
   if (fdChar == NULL)
   {
      LogMsg("***** Error opening Char file: %s\n", acCharFile);
      return 2;
   }

   // Open NAD file
   LogMsg("Open NAD file %s", acNadFile);
   fdNad = fopen(acNadFile, "r");
   if (fdNad == NULL)
   {
      LogMsg("***** Error opening Nad file: %s\n", acNadFile);
      return 2;
   }

   // Open Owner file
   LogMsg("Open Owner file %s", acOwnerFile);
   fdOwner = fopen(acOwnerFile, "r");
   if (fdOwner == NULL)
   {
      LogMsg("***** Error opening Owner file: %s\n", acOwnerFile);
      return 2;
   }

   // Open Input file
   LogMsg("Open input file %s", acRawFile);
   fhIn = CreateFile(acRawFile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhIn == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening input file: %s\n", acRawFile);
      return 2;
   }

   // Open Output file
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error creating output file: %s\n", acOutFile);
      return 2;
   }

   // Copy skip record
   memset(acBuf, ' ', iRecLen);
   while (iSkip-- > 0)
   {
      ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);
      // Ignore record start with 99999999
      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
   }

   iNoMatch=iBadCity=iBadSuffix=0;

   // Merge loop
   while (ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL))
   {
      // EOF ?
      if (iRecLen != nBytesRead)
         break;

#ifdef _DEBUG
      //if (!memcmp(acBuf, "926000001", 9))
      //   iRet = 1;
#endif
      // Merge roll data
      if (fdLien)
         iRet = Tul_MergeLien(acBuf);

      // Merge mailing address
      if (fdNad)
         iRet = Tul_MergeNad(acBuf);

      // Merge Situs
      if (fdSitus)
         iRet = Tul_MergeSitus(acBuf);

      // Merge Char
      if (fdChar)
         iRet = Tul_MergeChar(acBuf);

      // Merge Owner
      if (fdOwner)
         iRet = Tul_MergeOwner(acBuf);

      // Save last recording date
      lRet = atoin((char *)&acBuf[OFF_TRANSFER_DT], 8);
      if (lRet > lLastRecDate && lRet < lToday)
         lLastRecDate = lRet;

      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);

      if (!bRet)
      {
         LogMsg("***** Error occurs: %d\n", GetLastError());
         lRet = WRITE_ERR;
         break;
      }
   }

   // Close files
   if (fdLien)
      fclose(fdLien);
   if (fdSitus)
      fclose(fdSitus);
   if (fdChar)
      fclose(fdChar);
   if (fdOwner)
      fclose(fdOwner);
   if (fdNad)
      fclose(fdNad);

   if (fhOut)
      CloseHandle(fhOut);
   if (fhIn)
      CloseHandle(fhIn);

   LogMsg("Total output records from MergePQ4:  %u", lCnt);
   LogMsg("Total Lien matched:                  %u", lLienMatch);
   LogMsg("Total Char matched:                  %u", lCharMatch);
   LogMsg("Total Owner matched:                 %u", lOwnerMatch);
   LogMsg("Total Nad matched:                   %u", lNadMatch);
   LogMsg("Total Situs matched:                 %u\n", lSitusMatch);

   LogMsg("Total Lien skipped:                  %u", lLienSkip);
   LogMsg("Total Char skipped:                  %u", lCharSkip);
   LogMsg("Total Owner skipped:                 %u", lOwnerSkip);
   LogMsg("Total Nad skipped:                   %u", lNadSkip);
   LogMsg("Total Situs skipped:                 %u\n", lSitusSkip);

   LogMsg("Total bad bath data:                 %u", lBadBath);
   LogMsg("Total bad-city records:              %u", iBadCity);
   LogMsg("Total bad-suffix records:            %u\n", iBadSuffix);

   printf("\nTotal output records from MergePQ4: %u\n", lCnt);

   lRecCnt = lCnt;

   return 0;
}

/****************************** Tul_CreateRollRec ****************************
 *
 * There are 3 APN fields in roll file.  APN1 & APN2 are mostly the same.  If they
 * are different, use APN2 and check for SB813 in comments.  Reject the record if found.
 *
 * Return 0 if successful, 1 if SB813 transfer, 2 if unsecured
 *
 *****************************************************************************/

int Tul_CreateRollRec(char *pOutbuf, char *pRollRec)
{
   TUL_ROLL *pRec;
   char     acTmp[256], acTmp1[256];
   long     lLand, lMktLand, lRstLand, lImpr, lGr, lMktGr, lRstGr, lFixt, lPP1, lPP2;
   long     lGross, lOther, lTotalExe, lTmp;
   int      iRet, iTmp, iCnt;

   pRec = (TUL_ROLL *)pRollRec;

   // Select APN - if APN1 and APN2 are not the same, check for SB813
   if (memcmp(pRec->Apn, pRec->Apn2, RSIZ_APN))
   {
      memcpy(acTmp, pRec->Comments, RSIZ_COMMENTS_PT);
      acTmp[RSIZ_COMMENTS_PT] = 0;
      if (strstr(acTmp, "*** SB813"))
         return 1;
   }

   // Check for unsecured
   iTmp = atoin(pRec->Apn2, 3);
   if (iTmp >= 800 && iTmp <= 860)
      return 2;

   // Clear output buffer
   memset(pOutbuf, ' ', iRecLen);

   // Start copying data
   memcpy(pOutbuf, pRec->Apn2, RSIZ_APN);
   memcpy(pOutbuf+OFF_CO_NUM, "54TUL", 5);
   *(pOutbuf+OFF_STATUS) = pRec->Status;

   // Format APN
   iRet = formatApn(pRec->Apn2, acTmp, &myCounty);
   memcpy(pOutbuf+OFF_APN_D, acTmp, iRet);

   // Create MapLink
   iRet = formatMapLink(pRec->Apn2, acTmp, &myCounty);
   memcpy(pOutbuf+OFF_MAPLINK, acTmp, iRet);

   // Create index map link
   if (getIndexPage(acTmp, acTmp1, &myCounty))
      memcpy(pOutbuf+OFF_IMAPLINK, acTmp1, iRet);

   // Year assessed
   memcpy(pOutbuf+OFF_YR_ASSD, myCounty.acYearAssd, 4);

   lGr=lTotalExe=lLand=lRstLand=lMktLand=lImpr=lMktGr=lRstGr=lFixt=lPP1=lPP2 = 0;
   iCnt = atoin(pRec->Value_Index, RSIZ_VALUE_INDEX);
   if (iCnt > MAX_TUL_VAL)
   {
      LogMsg("*** Warning: Invalid Value Index count %d APN=%.12", iCnt, pRec->Apn2);
      iCnt = MAX_TUL_VAL;
   }

   for (iTmp = 0; iTmp < iCnt; iTmp++)
   {
      if (!memcmp(pRec->Value[iTmp].Code, "V01", 3))
         lMktLand = atoin(pRec->Value[iTmp].Amount, RSIZ_VALUE_AMT);
      else if (!memcmp(pRec->Value[iTmp].Code, "V02", 3))
         lRstLand = atoin(pRec->Value[iTmp].Amount, RSIZ_VALUE_AMT);
      else if (!memcmp(pRec->Value[iTmp].Code, "V11", 3))
         lImpr = atoin(pRec->Value[iTmp].Amount, RSIZ_VALUE_AMT);
      else if (!memcmp(pRec->Value[iTmp].Code, "V12", 3))
         lMktGr = atoin(pRec->Value[iTmp].Amount, RSIZ_VALUE_AMT);
      else if (!memcmp(pRec->Value[iTmp].Code, "V13", 3))
         lRstGr = atoin(pRec->Value[iTmp].Amount, RSIZ_VALUE_AMT);
      else if (!memcmp(pRec->Value[iTmp].Code, "V14", 3))
         lFixt = atoin(pRec->Value[iTmp].Amount, RSIZ_VALUE_AMT);
      else if (!memcmp(pRec->Value[iTmp].Code, "V21", 3))
         lPP1 = atoin(pRec->Value[iTmp].Amount, RSIZ_VALUE_AMT);
      else if (!memcmp(pRec->Value[iTmp].Code, "V31", 3))
         lPP2 = atoin(pRec->Value[iTmp].Amount, RSIZ_VALUE_AMT);
      else
         LogMsg("*** Unknown value %.3s -> %.24s", pRec->Value[iTmp].Code, pRec->Apn);
   }

   // Exempt
   iCnt = atoin(pRec->Exemp_Index, RSIZ_EXEMP_INDEX);
   if (iCnt > MAX_TUL_VAL)
   {
      LogMsg("*** Warning: Invalid Exempt Index count %d APN=%.12", iCnt, pRec->Apn2);
      iCnt = MAX_TUL_VAL;
   }

   lTotalExe = 0;
   *(pOutbuf+OFF_HO_FL) = '2';
   for (iTmp = 0; iTmp < iCnt; iTmp++)
   {
      if (!memcmp(pRec->Exemp[iTmp].Code, "E01", 3))
         *(pOutbuf+OFF_HO_FL) = '1';      // HO Exempt
      lTotalExe += atoin(pRec->Exemp[iTmp].Amount, RSIZ_VALUE_AMT);

      // Since Exe Code here is different from LDR, we only use LDR version 6/22/2020 - spn
      //if (iTmp == 0)
      //   memcpy(pOutbuf+OFF_EXE_CD1, pRec->Exemp[iTmp].Code, RSIZ_EXEMP_CODE);
      //else if (iTmp == 1)
      //   memcpy(pOutbuf+OFF_EXE_CD2, pRec->Exemp[iTmp].Code, RSIZ_EXEMP_CODE);
      //else if (iTmp == 2)
      //   memcpy(pOutbuf+OFF_EXE_CD3, pRec->Exemp[iTmp].Code, RSIZ_EXEMP_CODE);
   }
   if (lTotalExe > 0)
   {
      sprintf(acTmp, "%*d", SIZ_EXE_TOTAL, lTotalExe);
      memcpy(pOutbuf+OFF_EXE_TOTAL, acTmp, SIZ_EXE_TOTAL);
   }

   // Full exemption
   if (!memcmp(pRec->Exemp[0].Amount, "99999999", 8))
      *(pOutbuf+OFF_FULL_EXEMPT) = 'Y';

   // Set Land value
   if (pRec->Tax_Code[1] == '5')
   {
      lLand = lRstLand;
      lGr = lRstGr;
      *(pOutbuf+OFF_AG_PRE) = 'Y';
   } else
   {
      lLand = lMktLand;
      lGr = lMktGr;
   }

   if (lLand > 0)
   {
      sprintf(acTmp, "%*d", SIZ_LAND, lLand);
      memcpy(pOutbuf+OFF_LAND, acTmp, SIZ_LAND);
   }

#ifdef _DEBUG
   //if (!memcmp(acApn, "914930455", 9))
   //   iRet = 1;
#endif

   // Impr - Ratio
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*d", SIZ_LAND, lImpr);
      memcpy(pOutbuf+OFF_IMPR, acTmp, SIZ_LAND);

      sprintf(acTmp, "%*d", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
      memcpy(pOutbuf+OFF_RATIO, acTmp, SIZ_RATIO);
   }

   // Total other
   lOther = lFixt + lPP1 + lPP2 + lGr;
   if (lOther > 0)
   {
      sprintf(acTmp, "%*d", SIZ_LAND, lOther);
      memcpy(pOutbuf+OFF_OTHER, acTmp, SIZ_LAND);
   }

   // Gross
   lGross = lOther + lLand + lImpr;
   if (lGross > 0)
   {
      sprintf(acTmp, "%*d", SIZ_LAND, lGross);
      memcpy(pOutbuf+OFF_GROSS, acTmp, SIZ_LAND);
   }

   // TRA
   if (pRec->Tra_Sec[0] > ' ')
      memcpy(pOutbuf+OFF_TRA, pRec->Tra_Sec, RSIZ_TRA);
   else if (pRec->Tra_Unsec[0] > ' ')
      memcpy(pOutbuf+OFF_TRA, pRec->Tra_Unsec, RSIZ_TRA);

   // Legal
   if (pRec->Legal[0] > ' ')
   {
      memcpy(acTmp, pRec->Legal, RSIZ_LEGAL);
      acTmp[RSIZ_LEGAL] = 0;
      iRet = updateLegal(pOutbuf, acTmp);
      if (iRet > iMaxLegal)
         iMaxLegal = iRet;
   }

   // Tax code
   if (pRec->Tax_Code[0] > ' ')
      memcpy(pOutbuf+OFF_TAX_CODE, pRec->Tax_Code, SIZ_TAX_CODE);

   // Zoning
   memcpy(pOutbuf+OFF_ZONE, pRec->Zoning, RSIZ_ZONING);
   memcpy(pOutbuf+OFF_ZONE_X1, pRec->Zoning, RSIZ_ZONING);

   // Land Use
   if (pRec->Primary_Land_Use[0] > ' ')
   {
      memcpy(pOutbuf+OFF_USE_CO, pRec->Primary_Land_Use, RSIZ_LAND_USE);
      updateStdUse(pOutbuf+OFF_USE_STD, pRec->Primary_Land_Use, RSIZ_LAND_USE, pOutbuf);
   } else
      memcpy(pOutbuf+OFF_USE_STD, USE_UNASGN, SIZ_USE_STD);

   // Lot Acres - V99
   long lLotAcres = atoin(pRec->Acres, RSIZ_ACRES);
   if (lLotAcres > 1)
   {
      sprintf(acTmp, "%*d", SIZ_LOT_ACRES, lLotAcres*10);
      memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);

      lTmp = (long)((double)lLotAcres*SQFT_FACTOR_100);
      sprintf(acTmp, "%*d", SIZ_LOT_SQFT, lTmp);
      memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);
   }

   // Transfer doc
   lTmp = atoin(pRec->Curr_Docdate, 8);
   if (lTmp > 19000000 && lTmp < lToday)
   {
      memcpy(pOutbuf+OFF_TRANSFER_DT, pRec->Curr_Docdate, 8);
      memcpy(pOutbuf+OFF_TRANSFER_DOC, pRec->Curr_DocNum, RSIZ_CURR_DOCNUM);
   }

   return 0;
}

/******************************** Tul_Load_Roll *****************************
 *
 * This function will create the 1900-byte record only.  Next step you have
 * to merge this output with other files to produce R01 file.
 *
 ****************************************************************************/

int Tul_Load_Roll(int iFirstRec)
{
   char     *pTmp, acBuf[MAX_RECSIZE], acRollRec[MAX_RECSIZE];
   char     acOutFile[_MAX_PATH];

   HANDLE   fhOut;
   FILE     *fdRoll;
   TUL_ROLL *pRec;

   DWORD    nBytesWritten;
   BOOL     bRet;
   long     lCnt=0, lSB813=0, lSkip=0, lTotal=0, lUnsec=0;
   int      iRet;

   LogMsg0("Create roll file");
   sprintf(acOutFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "TMP");

   // Open Roll file
   LogMsg("Open Roll file %s", acRollFile);
   fdRoll = fopen(acRollFile, "r");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", acRollFile);
      return 2;
   }

   // Open Output file
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error creating output file: %s\n", acOutFile);
      return 2;
   }

   // Create header record
   if (iFirstRec)
   {
      memset(acBuf, '9', iRecLen);
      // Ignore record start with 99999999
      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
   }

   // Merge loop
   pRec = (TUL_ROLL *)&acRollRec[0];
   while (!feof(fdRoll))
   {
      do
      {
         lSkip++;
         pTmp = fgets(acRollRec, 1024, fdRoll);
      } while (pTmp && (pRec->Status == 'I' || pRec->Status == 'D' || pRec->Apn[0] == ' '));
      lSkip--;

      if (!pTmp)
         break;         // EOF

#ifdef _DEBUG
      //if (!memcmp(pRec->Apn2, "926000001", 9))
      //   iRet = 0;
#endif
      // Create new R01 record
      iRet = Tul_CreateRollRec(acBuf, acRollRec);
      if (!iRet)
      {
         bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
         if (!bRet)
         {
            LogMsg("***** Error occurs: %d\n", GetLastError());
            break;
         }
         lCnt++;
      } else if (iRet == 1)
         lSB813++;
      else if (iRet == 2)
         lUnsec++;

      if (!(lTotal++ % 1000))
         printf("\r%u", lTotal);
   }

   // Close files
   if (fdRoll)
      fclose(fdRoll);

   if (fhOut)
      CloseHandle(fhOut);

   LogMsg("Total processed records:       %u", lTotal);
   LogMsg("Total output records:          %u", lCnt);
   LogMsg("Total SB813 records skipped:   %u", lSB813);
   LogMsg("Total unsec records skipped:   %u", lUnsec);
   LogMsg("Total Inactive records:        %u", lSkip);

   printf("\nTotal output records: %u\n", lCnt);

   lRecCnt = lCnt;
   return 0;
}

/********************************* Tul_ExtrLien *****************************
 *
         DC    C'V01',CL8'MKT LND '
         DC    C'V02',CL8'RST LND '
         DC    C'V11',CL8'STR IMP '
         DC    C'V12',CL8'MKT GR I'
         DC    C'V13',CL8'RES GR I'
         DC    C'V14',CL8'FIXTURES'
         DC    C'V21',CL8'PP OTHER'
         DC    C'V31',CL8'PPO     '
         DC    C'E01',CL8'HO      '
 *
 ****************************************************************************

int Tul_ExtrLienOld()
{
   int   iRet=0, iTmp, iCnt;
   char  acRec[1024], acTmp[256], acTmpFile[_MAX_PATH], acPrevApn[16], *pTmp;
   long  lLand, lMktLand, lRstLand, lImpr, lGr, lMktGr, lRstGr, lFixt, lPP1, lPP2;
   long  lGross, lOther, lTotalExe, lTmp, lCnt;

   FILE     *fdExtr;
   TUL_ROLL *pRec;
   TUL_LIEN sExtr;

   sprintf(acTmpFile, "%s\\%s\\%s_Lien.ext", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   // Open Roll file
   LogMsg("Open Roll file %s", acRollFile);
   fdRoll = fopen(acRollFile, "r");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", acRollFile);
      return 2;
   }

   // Open extr file
   LogMsg("Open Lien extract file %s", acTmpFile);
   fdExtr = fopen(acTmpFile, "w");
   if (fdExtr == NULL)
   {
      LogMsg("***** Error creating extract file: %s\n", acTmpFile);
      return 2;
   }

   pRec = (TUL_ROLL *)&acRec[0];
   acPrevApn[0] = 0;
   lTmp = lCnt = 0;
   sExtr.CRLF[0] = '\n';
   sExtr.CRLF[1] = 0;

   while (!feof(fdRoll))
   {
      do
      {
         pTmp = fgets(acRec, 1024, fdRoll);
      } while (pRec->Status == 'I' || pRec->Status == 'D');

      // Reset output buffer
      memset((void *)&sExtr, ' ', sizeof(TUL_LIEN));

      // Select APN - if APN1 and APN2 are not the same, check for SB813
      if (memcmp(pRec->Apn, pRec->Apn2, RSIZ_APN))
      {
         memcpy(acTmp, pRec->Comments, RSIZ_COMMENTS_PT);
         acTmp[RSIZ_COMMENTS_PT] = 0;
         if (strstr(acTmp, "*** SB813"))
         {
            lTmp++;
            continue;
         }

         // Save current APN
         memcpy(sExtr.Apn, pRec->Apn2, RSIZ_APN);
         memcpy(sExtr.Apn2, pRec->Apn, RSIZ_APN);
      } else
      {
         // Save current APN
         memcpy(acPrevApn, pRec->Apn, RSIZ_APN);
         memcpy(sExtr.Apn, pRec->Apn, RSIZ_APN+RSIZ_APN);
      }

      lGr=lTotalExe=lLand=lRstLand=lMktLand=lImpr=lMktGr=lRstGr=lFixt=lPP1=lPP2 = 0;

      iCnt = atoin(pRec->Value_Index, RSIZ_VALUE_INDEX);
      for (iTmp = 0; iTmp < iCnt; iTmp++)
      {
         if (!memcmp(pRec->Value[iTmp].Code, "V01", 3))
            lMktLand = atoin(pRec->Value[iTmp].Amount, RSIZ_VALUE_AMT);
         else if (!memcmp(pRec->Value[iTmp].Code, "V02", 3))
            lRstLand = atoin(pRec->Value[iTmp].Amount, RSIZ_VALUE_AMT);
         else if (!memcmp(pRec->Value[iTmp].Code, "V11", 3))
            lImpr = atoin(pRec->Value[iTmp].Amount, RSIZ_VALUE_AMT);
         else if (!memcmp(pRec->Value[iTmp].Code, "V12", 3))
            lMktGr = atoin(pRec->Value[iTmp].Amount, RSIZ_VALUE_AMT);
         else if (!memcmp(pRec->Value[iTmp].Code, "V13", 3))
            lRstGr = atoin(pRec->Value[iTmp].Amount, RSIZ_VALUE_AMT);
         else if (!memcmp(pRec->Value[iTmp].Code, "V14", 3))
            lFixt = atoin(pRec->Value[iTmp].Amount, RSIZ_VALUE_AMT);
         else if (!memcmp(pRec->Value[iTmp].Code, "V21", 3))
            lPP1 = atoin(pRec->Value[iTmp].Amount, RSIZ_VALUE_AMT);
         else if (!memcmp(pRec->Value[iTmp].Code, "V31", 3))
            lPP2 = atoin(pRec->Value[iTmp].Amount, RSIZ_VALUE_AMT);
         else
            LogMsg("*** Unknown value %.3s -> %.24s", pRec->Value[iTmp].Code, pRec->Apn);
      }

      // Exempt
      iCnt = atoin(pRec->Exemp_Index, RSIZ_EXEMP_INDEX);
      lTotalExe = 0;
      sExtr.HO_Flg = '2';
      for (iTmp = 0; iTmp < iCnt; iTmp++)
      {
         if (!memcmp(pRec->Exemp[iTmp].Code, "E01", 3))
            sExtr.HO_Flg = '1';
         lTotalExe += atoin(pRec->Exemp[iTmp].Amount, RSIZ_VALUE_AMT);
      }
      if (lTotalExe > 0)
      {
         sprintf(acTmp, "%*d", SIZ_LAND, lTotalExe);
         memcpy(sExtr.TotalExe, acTmp, SIZ_LAND);
      }

      // Land
      if (lMktLand > 0)
      {
         sprintf(acTmp, "%*d", SIZ_LAND, lMktLand);
         memcpy(sExtr.MktLand, acTmp, SIZ_LAND);
      }

      if (lRstLand > 0)
      {
         sprintf(acTmp, "%*d", SIZ_LAND, lRstLand);
         memcpy(sExtr.RstLand, acTmp, SIZ_LAND);
      }

      if (pRec->Tax_Code[1] == '5')
      {
         lLand = lRstLand;
         lGr = lRstGr;
      } else
      {
         lLand = lMktLand;
         lGr = lMktGr;
      }

      if (lLand > 0)
      {
         sprintf(acTmp, "%*d", SIZ_LAND, lLand);
         memcpy(sExtr.Land, acTmp, SIZ_LAND);
      }

      // Impr - Ratio
      if (lImpr > 0)
      {
         sprintf(acTmp, "%*d", SIZ_LAND, lImpr);
         memcpy(sExtr.StrImpr, acTmp, SIZ_LAND);
         memcpy(sExtr.Impr, acTmp, SIZ_LAND);

         sprintf(acTmp, "%*d", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
         memcpy(sExtr.Ratio, acTmp, SIZ_RATIO);
      }

      // Grow Impr
      if (lMktGr > 0)
      {
         sprintf(acTmp, "%*d", SIZ_LAND, lMktGr);
         memcpy(sExtr.MktGrowImpr, acTmp, SIZ_LAND);
      }
      if (lRstGr > 0)
      {
         sprintf(acTmp, "%*d", SIZ_LAND, lRstGr);
         memcpy(sExtr.RstGrowImpr, acTmp, SIZ_LAND);
      }

      // Fixture
      if (lFixt > 0)
      {
         sprintf(acTmp, "%*d", SIZ_LAND, lFixt);
         memcpy(sExtr.Fixture, acTmp, SIZ_LAND);
      }

      // Pers Prop
      if (lPP1 > 0)
      {
         sprintf(acTmp, "%*d", SIZ_LAND, lPP1);
         memcpy(sExtr.PersProp1, acTmp, SIZ_LAND);
      }
      if (lPP2 > 0)
      {
         sprintf(acTmp, "%*d", SIZ_LAND, lPP2);
         memcpy(sExtr.PersProp2, acTmp, SIZ_LAND);
      }

      // Total other
      lOther = lFixt + lPP1 + lPP2 + lGr;
      if (lOther > 0)
      {
         sprintf(acTmp, "%*d", SIZ_LAND, lOther);
         memcpy(sExtr.OtherVal, acTmp, SIZ_LAND);
      }

      // Gross
      lGross = lOther + lLand + lImpr;
      if (lGross > 0)
      {
         sprintf(acTmp, "%*d", SIZ_LAND, lGross);
         memcpy(sExtr.GrossVal, acTmp, SIZ_LAND);
      }

      sExtr.Status = pRec->Status;
      memcpy(sExtr.TaxCode, pRec->Tax_Code, RSIZ_TAX_CODE);
      sExtr.CRLF[0] = '\n';
      sExtr.CRLF[1] = 0;

      // Write to file
      fputs((char *)&sExtr, fdExtr);

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdRoll)
      fclose(fdRoll);
   if (fdExtr)
      fclose(fdExtr);

   LogMsg("Total records output:     %d", lCnt);
   LogMsg("Total records SB813 skip: %d", lTmp);

   // Sort output file
   sprintf(acLienExtr, acLienTmpl, myCounty.acCntyCode, myCounty.acCntyCode);
   sprintf(acTmp, "S(1,12,C,A) F(TXT) DUPO(1,12)");
   iRet = sortFile(acTmpFile, acLienExtr, acTmp);
   if (iRet <= 0)
   {
      LogMsg("***** ERROR sorting lien extract file %s to %s", acTmpFile, acLienExtr);
      return -1;
   }

   LogMsg("Total records after sort: %d", iRet);

   return iRet;
}

/******************************** Tul_ExtrLien ******************************
 *
 * Extract lien values to LIENEXTR format:
 *
 ****************************************************************************/

int Tul_ExtrLien()
{
   char  acRec[2560], acOutbuf[512], acTmp[256], acTmpFile[_MAX_PATH], acPrevApn[16], *pTmp;

   FILE     *fdExtr;
   LIENEXTR *pLienExt = (LIENEXTR *)&acOutbuf;

   LogMsg("Extract lien values");

   // Open Roll file
   LogMsg("Open LDR file %s", acLienFile);
   fdRoll = fopen(acLienFile, "r");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening LDR file: %s\n", acLienFile);
      return -1;
   }

   // Open extr file
   sprintf(acTmpFile, acLienTmpl, myCounty.acCntyCode, myCounty.acCntyCode);
   LogMsg("Open Lien extract file %s", acTmpFile);
   fdExtr = fopen(acTmpFile, "w");
   if (fdExtr == NULL)
   {
      LogMsg("***** Error creating extract file: %s\n", acTmpFile);
      return -2;
   }

   long  lLand, lImpr, lGrow, lFixt, lPP_Val, lBusInv;
   long  lGross, lOther, lTotalExe, lCnt, lSkip, lSameApn;

   acPrevApn[0] = 0;
   lSameApn = lSkip = lCnt = 0;

   while (!feof(fdRoll))
   {
      pTmp = fgets(acRec, 2500, fdRoll);
      if (!pTmp)
         break;

      // Take active parcels only
      if (acRec[AOFF_ROLL_TYPE] != 'A')
      {
         lSkip++;
         continue;
      }

      if (!memcmp(acPrevApn, acRec, iApnLen))
      {
         LogMsg("Same APN: %.12s", acRec);
         lSameApn++;
         continue;
      }

      // Reset output buffer
      memset(acOutbuf, ' ', sizeof(LIENEXTR));

      // Save current APN
      memcpy(acPrevApn, acRec, RSIZ_APN);
      memcpy(pLienExt->acApn, acRec, RSIZ_APN);      
      // Year assessed
      memcpy(pLienExt->acYear, myCounty.acYearAssd, 4);

      // TRA
      memcpy(pLienExt->acTRA, &acRec[AOFF_TRA], LSIZ_TRA);      

      // ALT_APN
      if (acRec[AOFF_ORIG_ASMT_NO] > ' ')
         memcpy(pLienExt->extra.Tul.PrevApn, &acRec[AOFF_ORIG_ASMT_NO], LSIZ_APN);
      if (acRec[AOFF_FEE_ASMT_NO] > ' ')
         memcpy(pLienExt->extra.Tul.FeeAsmt, &acRec[AOFF_FEE_ASMT_NO], LSIZ_APN);

      lLand  = atoin(&acRec[AOFF_LAND], RSIZ_VALUE_AMT);
      lImpr  = atoin(&acRec[AOFF_STRUCTURE_IMPR], RSIZ_VALUE_AMT);
      lFixt  = atoin(&acRec[AOFF_FIXT_IMPR], RSIZ_VALUE_AMT);
      lGrow  = atoin(&acRec[AOFF_GROWING_IMPR], RSIZ_VALUE_AMT);
      lPP_Val= atoin(&acRec[AOFF_PERS_IMPR], RSIZ_VALUE_AMT);
      lBusInv= atoin(&acRec[AOFF_BUSINESS_INV], RSIZ_VALUE_AMT);

      // Land
      if (lLand > 0)
      {
         sprintf(acTmp, "%*d", SIZ_LIEN_FIXT, lLand);
         memcpy(pLienExt->acLand, acTmp, SIZ_LIEN_FIXT);
      }

      // Structure impr
      if (lImpr > 0)
      {
         sprintf(acTmp, "%*d", SIZ_LIEN_FIXT, lImpr);
         memcpy(pLienExt->acImpr, acTmp, SIZ_LIEN_FIXT);
      }

      // Other impr
      lOther = lPP_Val + lGrow + lFixt + lBusInv;
      if (lOther > 0)
      {
         sprintf(acTmp, "%*d", SIZ_LIEN_FIXT, lOther);
         memcpy(pLienExt->acOther, acTmp, SIZ_LIEN_FIXT);

         // Fixture/Machine/Equipment
         if (lFixt > 0)
         {
            sprintf(acTmp, "%*d", SIZ_LIEN_FIXT, lFixt);
            memcpy(pLienExt->acME_Val, acTmp, SIZ_LIEN_FIXT);
         }

         // Personal property
         if (lPP_Val > 0)
         {
            sprintf(acTmp, "%*d", SIZ_LIEN_FIXT, lPP_Val);
            memcpy(pLienExt->acPP_Val, acTmp, SIZ_LIEN_FIXT);
         }
         // Growing impr
         if (lGrow > 0)
         {
            sprintf(acTmp, "%*d", SIZ_LIEN_FIXT, lGrow);
            memcpy(pLienExt->extra.Tul.GrowImpr, acTmp, SIZ_LIEN_FIXT);
         }

         // Business inventory
         if (lBusInv > 0)
         {
            sprintf(acTmp, "%*d", SIZ_LIEN_FIXT, lPP_Val);
            memcpy(pLienExt->extra.Tul.Bus_Inv, acTmp, SIZ_LIEN_FIXT);
         }
      }


      // Gross
      lGross = lOther + lLand + lImpr;
      if (lGross > 0)
      {
         sprintf(acTmp, "%*d", SIZ_LAND, lGross);
         memcpy(pLienExt->acGross, acTmp, SIZ_LAND);
      }

      // Impr - Ratio
      if (lImpr > 0)
      {
         sprintf(acTmp, "%*d", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
         memcpy(pLienExt->acRatio, acTmp, SIZ_RATIO);
      }

      // Exempt
      lTotalExe = atoin(&acRec[AOFF_EXEMP_AMT], ASIZ_EXEMP_AMT);
      pLienExt->acHO[0] = '2';
      if (lTotalExe > 0)
      {
         sprintf(acTmp, "%*d", SIZ_EXE_TOTAL, lTotalExe);
         memcpy(pLienExt->acExAmt, acTmp, SIZ_EXE_TOTAL);
         if (acRec[AOFF_EXEMP_CODE] == 'H')
            pLienExt->acHO[0] = '1';
      }

      pLienExt->LF[0] = '\n';
      pLienExt->LF[1] = 0;

      // Write to file
      fputs(acOutbuf, fdExtr);

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdRoll)
      fclose(fdRoll);
   if (fdExtr)
      fclose(fdExtr);

   LogMsg("Total records output:     %d", lCnt);
   LogMsg("Total records same APN:   %d", lSameApn);
   LogMsg("Total records skip:       %d", lSkip);

   // Sort output file
   //sprintf(acLienExtr, acLienTmpl, myCounty.acCntyCode, myCounty.acCntyCode);
   //sprintf(acTmp, "S(1,12,C,A) F(TXT) DUPO(1,12)");

   //LogMsg("Sort output file %s --> %s", acTmpFile, acLienExtr);
   //int iRet = sortFile(acTmpFile, acLienExtr, acTmp);
   //if (iRet <= 0)
   //{
   //   LogMsg("***** ERROR sorting lien extract file %s to %s", acTmpFile, acLienExtr);
   //   iRet = -1;
   //} else
   //   LogMsg("Total records after sort: %d", iRet);

   return 0;
}

/******************************** Tul_ExtrLien1 ******************************
 *
 * Extract lien values to LIENEXTR for LDR 2015 format
 *
 *****************************************************************************/

int Tul_ExtrLien1()
{
   char     acRec[MAX_RECSIZE], acOutbuf[512], acTmp[256], acTmpFile[_MAX_PATH], acPrevApn[16], *pTmp;

   FILE     *fdExtr;
   LIENEXTR *pLienExt = (LIENEXTR *)&acOutbuf;

   LogMsg0("Extract lien values");

   // Open Roll file
   LogMsg("Open LDR file %s", acLienFile);
   fdRoll = fopen(acLienFile, "r");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening LDR file: %s\n", acLienFile);
      return -1;
   }

   // Open extr file
   sprintf(acTmpFile, acLienTmpl, myCounty.acCntyCode, myCounty.acCntyCode);
   LogMsg("Open Lien extract file %s", acTmpFile);
   fdExtr = fopen(acTmpFile, "w");
   if (fdExtr == NULL)
   {
      LogMsg("***** Error creating extract file: %s\n", acTmpFile);
      return -2;
   }

   long  lLand, lImpr, lGrow, lFixt, lPP_Val, lBusInv;
   long  lGross, lOther, lTotalExe, lCnt, lSkip, lSameApn;

   acPrevApn[0] = 0;
   lSameApn = lSkip = lCnt = 0;

   while (!feof(fdRoll))
   {
      pTmp = fgets(acRec, 2500, fdRoll);
      if (!pTmp)
         break;
      lCnt++;

      // Take active parcels only
      if (acRec[LOFF_1_ROLL_TYPE] != 'A')
      {
         lSkip++;
         continue;
      }

      if (!memcmp(acPrevApn, acRec, iApnLen))
      {
         LogMsg("Same APN: %.12s", acRec);
         lSameApn++;
         continue;
      }

      // Reset output buffer
      memset(acOutbuf, ' ', sizeof(LIENEXTR));

      // Save current APN
      memcpy(acPrevApn, acRec, RSIZ_APN);
      memcpy(pLienExt->acApn, acRec, RSIZ_APN);      
      // Year assessed
      memcpy(pLienExt->acYear, myCounty.acYearAssd, 4);

      // TRA
      memcpy(pLienExt->acTRA, &acRec[LOFF_1_TAX_RATE_AREA], LSIZ_TRA);      

      // ALT_APN
      if (acRec[LOFF_1_ORIG_ASSMT_X] > ' ')
         memcpy(pLienExt->extra.Tul.PrevApn, &acRec[LOFF_1_ORIG_ASSMT_X], LSIZ_APN);
      if (acRec[LOFF_1_FEE_ASSMT_X] > ' ')
         memcpy(pLienExt->extra.Tul.FeeAsmt, &acRec[LOFF_1_FEE_ASSMT_X], LSIZ_APN);

      lLand  = atoin(&acRec[LOFF_1_MARKET_LAND_VAL], RSIZ_VALUE_AMT);
      lImpr  = atoin(&acRec[LOFF_1_STRUCT_IMPR_VAL], RSIZ_VALUE_AMT);
      lFixt  = atoin(&acRec[LOFF_1_FIXED_IMPR_VAL], RSIZ_VALUE_AMT);
      lGrow  = atoin(&acRec[LOFF_1_GROWING_IMPR_VAL], RSIZ_VALUE_AMT);
      lPP_Val= atoin(&acRec[LOFF_1_PERSON_PROP_VAL], RSIZ_VALUE_AMT);
      lBusInv= atoin(&acRec[LOFF_1_BUSINESS_INV], RSIZ_VALUE_AMT);

      // Land
      if (lLand > 0)
      {
         sprintf(acTmp, "%*d", SIZ_LIEN_FIXT, lLand);
         memcpy(pLienExt->acLand, acTmp, SIZ_LIEN_FIXT);
      }

      // Structure impr
      if (lImpr > 0)
      {
         sprintf(acTmp, "%*d", SIZ_LIEN_FIXT, lImpr);
         memcpy(pLienExt->acImpr, acTmp, SIZ_LIEN_FIXT);
      }

      // Other impr
      lOther = lPP_Val + lGrow + lFixt + lBusInv;
      if (lOther > 0)
      {
         sprintf(acTmp, "%*d", SIZ_LIEN_FIXT, lOther);
         memcpy(pLienExt->acOther, acTmp, SIZ_LIEN_FIXT);

         // Fixture/Machine/Equipment
         if (lFixt > 0)
         {
            sprintf(acTmp, "%*d", SIZ_LIEN_FIXT, lFixt);
            memcpy(pLienExt->acME_Val, acTmp, SIZ_LIEN_FIXT);
         }

         // Personal property
         if (lPP_Val > 0)
         {
            sprintf(acTmp, "%*d", SIZ_LIEN_FIXT, lPP_Val);
            memcpy(pLienExt->acPP_Val, acTmp, SIZ_LIEN_FIXT);
         }
         // Growing impr
         if (lGrow > 0)
         {
            sprintf(acTmp, "%*d", SIZ_LIEN_FIXT, lGrow);
            memcpy(pLienExt->extra.Tul.GrowImpr, acTmp, SIZ_LIEN_FIXT);
         }

         // Business inventory
         if (lBusInv > 0)
         {
            sprintf(acTmp, "%*d", SIZ_LIEN_FIXT, lPP_Val);
            memcpy(pLienExt->extra.Tul.Bus_Inv, acTmp, SIZ_LIEN_FIXT);
         }
      }

      // Gross
      lGross = lOther + lLand + lImpr;
      if (lGross > 0)
      {
         sprintf(acTmp, "%*d", SIZ_LAND, lGross);
         memcpy(pLienExt->acGross, acTmp, SIZ_LAND);
      }

      // Impr - Ratio
      if (lImpr > 0)
      {
         sprintf(acTmp, "%*d", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
         memcpy(pLienExt->acRatio, acTmp, SIZ_RATIO);
      }

      // Exempt - we populate exe code regardless of there is any amount 6/22/2020 - spn
      pLienExt->extra.Tul.Exe_Code1[0] = acRec[LOFF_1_EXMPT_CODE_1];
      pLienExt->extra.Tul.Exe_Code2[0] = acRec[LOFF_1_EXMPT_CODE_2];
      pLienExt->extra.Tul.Exe_Code3[0] = acRec[LOFF_1_EXMPT_CODE_3];

      long lExe1 = atoin(&acRec[LOFF_1_PROP_EXMPT_AMT_1], LSIZ_1_PROP_EXMPT_AMT_1);
      long lExe2 = atoin(&acRec[LOFF_1_PROP_EXMPT_AMT_2], LSIZ_1_PROP_EXMPT_AMT_2);
      long lExe3 = atoin(&acRec[LOFF_1_PROP_EXMPT_AMT_3], LSIZ_1_PROP_EXMPT_AMT_3);
#ifdef _DEBUG
      //if (lExe2 > 0 || lExe3 > 0)
      //   lExe2 = lExe2;
#endif
      lTotalExe = lExe1+lExe2+lExe3;
      pLienExt->acHO[0] = '2';
      if (lTotalExe > 0)
      {
         sprintf(acTmp, "%d", lTotalExe);
         vmemcpy(pLienExt->acExAmt, acTmp, SIZ_EXE_TOTAL);
         sprintf(acTmp, "%d", lExe1);
         vmemcpy(pLienExt->extra.Tul.Exe_Amt1, acTmp, SIZ_LIEN_EXEVAL);
         if (acRec[LOFF_1_EXMPT_CODE_1] == 'H')
            pLienExt->acHO[0] = '1';
         if (lExe2 > 0)
         {
            sprintf(acTmp, "%d", lExe2);
            vmemcpy(pLienExt->extra.Tul.Exe_Amt2, acTmp, SIZ_LIEN_EXEVAL);
         }
         if (lExe3 > 0)
         {
            sprintf(acTmp, "%d", lExe3);
            vmemcpy(pLienExt->extra.Tul.Exe_Amt3, acTmp, SIZ_LIEN_EXEVAL);
         }

         // Full exemption
         if (lTotalExe >= lGross)
            pLienExt->SpclFlag = LX_FULLEXE_FLG;
      }

      pLienExt->LF[0] = '\n';
      pLienExt->LF[1] = 0;

      // Write to file
      fputs(acOutbuf, fdExtr);

      if (!(lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdRoll)
      fclose(fdRoll);
   if (fdExtr)
      fclose(fdExtr);

   LogMsg("Total records output:     %d", lCnt);
   LogMsg("Total records same APN:   %d", lSameApn);
   LogMsg("Total records skip:       %d", lSkip);

   // Sort output file
   //sprintf(acLienExtr, acLienTmpl, myCounty.acCntyCode, myCounty.acCntyCode);
   //sprintf(acTmp, "S(1,12,C,A) F(TXT) DUPO(1,12)");

   //LogMsg("Sort output file %s --> %s", acTmpFile, acLienExtr);
   //int iRet = sortFile(acTmpFile, acLienExtr, acTmp);
   //if (iRet <= 0)
   //{
   //   LogMsg("***** ERROR sorting lien extract file %s to %s", acTmpFile, acLienExtr);
   //   iRet = -1;
   //} else
   //   LogMsg("Total records after sort: %d", iRet);

   return 0;
}

/****************************** Tul_CreateR01 ********************************
 *
 *
 *****************************************************************************/

int Tul_CreateR01(char *pOutbuf, char *pRollRec)
{
   TUL_LIEN *pRec;
   char     acTmp[256], acTmp1[256];
   int      iRet;

   pRec = (TUL_LIEN *)pRollRec;

   // Clear output buffer
   memset(pOutbuf, ' ', iRecLen);

   // Start copying data
   memcpy(pOutbuf, pRec->Apn, LSIZ_APN);
   memcpy(pOutbuf+OFF_CO_NUM, "54TUL", 5);
   *(pOutbuf+OFF_STATUS) = 'A';

   // Format APN
   iRet = formatApn(pRec->Apn, acTmp, &myCounty);
   memcpy(pOutbuf+OFF_APN_D, acTmp, iRet);

   // Create MapLink
   iRet = formatMapLink(pRec->Apn, acTmp, &myCounty);
   memcpy(pOutbuf+OFF_MAPLINK, acTmp, iRet);

   // Create index map link
   if (getIndexPage(acTmp, acTmp1, &myCounty))
      memcpy(pOutbuf+OFF_IMAPLINK, acTmp1, iRet);

   // Year assessed
   memcpy(pOutbuf+OFF_YR_ASSD, myCounty.acYearAssd, 4);

   // Fee Asmt
   memcpy(pOutbuf+OFF_ALT_APN, pRec->Fee_Asmt_No, LSIZ_FEE_ASMT_NO);

   // Original APN
   memcpy(pOutbuf+OFF_PREV_APN, pRec->Orig_Asmt_No, LSIZ_ORIG_ASMT_NO);

   // Lien values
   long lLand  = atoin(pRec->Land, RSIZ_VALUE_AMT);
   long lImpr  = atoin(pRec->Structure, RSIZ_VALUE_AMT);
   long lFixt  = atoin(pRec->Fixture, RSIZ_VALUE_AMT);
   long lGrow  = atoin(pRec->Growing, RSIZ_VALUE_AMT);
   long lPP_Val= atoin(pRec->PersProp, RSIZ_VALUE_AMT);
   long lBusInv= atoin(pRec->BusProp, RSIZ_VALUE_AMT);

   // Land
   if (lLand > 0)
   {
      sprintf(acTmp, "%*d", SIZ_LAND, lLand);
      memcpy(pOutbuf+OFF_LAND, acTmp, SIZ_LAND);
   }

   // Structure impr
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*d", SIZ_LAND, lImpr);
      memcpy(pOutbuf+OFF_IMPR, acTmp, SIZ_LAND);

      sprintf(acTmp, "%*d", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
      memcpy(pOutbuf+OFF_RATIO, acTmp, SIZ_RATIO);
   }

   // Other impr
   long lOther = lPP_Val + lGrow + lFixt + lBusInv;
   if (lOther > 0)
   {
      sprintf(acTmp, "%*d", SIZ_LAND, lOther);
      memcpy(pOutbuf+OFF_OTHER, acTmp, SIZ_LAND);

      // Fixture/Machine/Equipment
      if (lFixt > 0)
      {
         sprintf(acTmp, "%d         ", lFixt);
         memcpy(pOutbuf+OFF_FIXTR, acTmp, SIZ_FIXTR);
      }
      // Personal property
      if (lPP_Val > 0)
      {
         sprintf(acTmp, "%d         ", lPP_Val);
         memcpy(pOutbuf+OFF_PERSPROP, acTmp, SIZ_PERSPROP);
      }
      // Growing impr
      if (lGrow > 0)
      {
         sprintf(acTmp, "%d         ", lGrow);
         memcpy(pOutbuf+OFF_GR_IMPR, acTmp, SIZ_GR_IMPR);
      }

      // Business inventory
      if (lBusInv > 0)
      {
         sprintf(acTmp, "%d         ", lBusInv);
         memcpy(pOutbuf+OFF_BUSINV, acTmp, SIZ_BUSINV);
      }
   }

   // Gross
   long lGross = lOther + lLand + lImpr;
   if (lGross > 0)
   {
      sprintf(acTmp, "%*d", SIZ_LAND, lGross);
      memcpy(pOutbuf+OFF_GROSS, acTmp, SIZ_LAND);
   }

   // Tax code
   if (pRec->TaxCode[0] > ' ')
      memcpy(pOutbuf+OFF_TAX_CODE, pRec->TaxCode, LSIZ_TAXCODE);

   // Exemption
   long lTmp = atoin(pRec->Exe_Amt, ASIZ_EXEMP_AMT);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_EXE_TOTAL, lTmp);
      memcpy(pOutbuf+OFF_EXE_TOTAL, acTmp, SIZ_EXE_TOTAL);
   }

   if (pRec->Exe_Code[0] == 'H')
      *(pOutbuf+OFF_HO_FL) = '1';      // HO Exempt
   else
      *(pOutbuf+OFF_HO_FL) = '2';

   // Exemption code
   if (pRec->Exe_Code[0] > ' ')
      *(pOutbuf+OFF_EXE_CD1) = pRec->Exe_Code[0];

   // Ag preserve
   if (pRec->Ag_Preserve == 'Y')
      *(pOutbuf+OFF_AG_PRE) = 'Y';

#ifdef _DEBUG
   //if (!memcmp(acApn, "914930455", 9))
   //   iRet = 1;
#endif

   // TRA
   memcpy(pOutbuf+OFF_TRA, pRec->Tra, LSIZ_TRA);

   // Land Use
   if (pRec->UseCode[0] > ' ')
   {
      memcpy(pOutbuf+OFF_USE_CO, pRec->UseCode, LSIZ_USECODE);
      updateStdUse(pOutbuf+OFF_USE_STD, pRec->UseCode, LSIZ_USECODE, pOutbuf);
   } else
      memcpy(pOutbuf+OFF_USE_STD, USE_UNASGN, SIZ_USE_STD);

   // Lot Acres - V99
   long lLotAcres = atoin(pRec->Acres, ASIZ_ACRES);
   if (lLotAcres > 1)
   {
      sprintf(acTmp, "%*d", SIZ_LOT_ACRES, lLotAcres*10);
      memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);

      lTmp = (long)((double)lLotAcres*SQFT_FACTOR_100);
      sprintf(acTmp, "%*d", SIZ_LOT_SQFT, lTmp);
      memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);
   }

   // Transfer doc
   lTmp = atoin(pRec->DocDate, 8);
   if (lTmp > 19000000 && lTmp < lToday)
   {
      memcpy(pOutbuf+OFF_TRANSFER_DT, pRec->DocDate, 8);
      memcpy(pOutbuf+OFF_TRANSFER_DOC, pRec->DocNum, LSIZ_DEED_NUMBER);
   }

   // Delq date - Data is all '00000000'

   // Owner
   Tul_MergeOwner(pOutbuf, pRec->Curr_Owner);

   // Mail 
   Tul_MergeMAdr(pOutbuf, pRec->MailAdr1, pRec->MailAdr2, pRec->MailAdr3, pRec->MailAdr4);

   // Situs - use situs file
   Tul_MergeSitus(pOutbuf, pRollRec);

   return 0;
}

/********************************* Tul_MergeLegal ****************************
 *
 * Merge legal into R01 record.
 *
 *****************************************************************************/

int Tul_MergeLegal(char *pOutbuf)
{
   static char acRec[1024], *pRec=NULL;
   int         iLoop;

   // Get first  rec for first call
   if (!pRec)
      pRec = fgets(acRec,  1024, fdLegal);

   do
   {
      iLoop = memcmp(pOutbuf, acRec, myCounty.iApnLen);
      if (iLoop > 0)
      {
         if (bDebug)
            LogMsg0("*** Skip legal: %s", acRec);

         pRec = fgets(acRec,  1024, fdLegal);
         if (!pRec)
         {
            fclose(fdLegal);
            fdLegal = NULL;
            return 1;      // EOF
         }
      }
   } while (iLoop > 0);

   // If not match, return
   if (iLoop)
      return 1;

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "004191410", 9))
   //   iLoop = 0;
#endif

   if (acRec[myCounty.iApnLen+1] > ' ')
   {
      updateLegal(pOutbuf, &acRec[myCounty.iApnLen+1]);
      lLegalMatch++;
   }

   // Get next Char rec
   pRec = fgets(acRec,  1024, fdLegal);

   return 0;
}

/******************************** Tul_Load_LDR ******************************
 *
 * Create R01 from LDR file, merge Sales & Chars to produce LDR roll
 *
 ****************************************************************************/

int Tul_Load_LDR(int iFirstRec /* 1=create header rec */)
{
   char     *pTmp, acBuf[MAX_RECSIZE], acRollRec[MAX_RECSIZE];
   char     acOutFile[_MAX_PATH];

   HANDLE   fhOut;
   TUL_LIEN *pRec = (TUL_LIEN *)&acRollRec[0];

   DWORD    nBytesWritten;
   BOOL     bRet;
   long     lCnt=0, lTmp;
   int      iRet;

   sprintf(acOutFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "R01");

   // Open Roll file
   LogMsg("Open LDR file %s", acLienFile);
   fdRoll = fopen(acLienFile, "r");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", acLienFile);
      return -1;
   }

   // Open Char file
   LogMsg("Open Char file %s", acCharFile);
   fdChar = fopen(acCharFile, "r");
   if (fdChar == NULL)
   {
      LogMsg("***** Error opening Char file: %s\n", acCharFile);
      return -2;
   }

   // Open Sale file
   LogMsg("Open sale file %s", acSaleFile);
   fdSale = fopen(acSaleFile, "r");
   if (fdSale == NULL)
   {
      LogMsg("***** Error opening cumulative sale file: %s\n", acSaleFile);
      return -3;
   }

   // Open legal file
   lLegalMatch = 0;
   if (acLegalExtr[0] >= 'A')
   {
      fdLegal = fopen(acLegalExtr, "r");
      if (fdLegal == NULL)
      {
         LogMsg("***** Error opening legal extract file: %s\n", acLegalExtr);
         return -3;
      }
   } else
      fdLegal = NULL;

   // Open Output file
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error creating output file: %s\n", acOutFile);
      return -4;
   }

   // Create header record
   if (iFirstRec)
   {
      memset(acBuf, '9', iRecLen);
      // Ignore record start with 99999999
      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
   }

   // Merge loop
   while (!feof(fdRoll))
   {
      if (!(pTmp = fgets(acRollRec, MAX_RECSIZE, fdRoll)))
         break;         // EOF
#ifdef _DEBUG
      //if (!memcmp(pRec->Apn2, "926000001", 9))
      //   iRet = 0;
#endif

      // Create new R01 record
      iRet = Tul_CreateR01(acBuf, acRollRec);

      if (!iRet)
      {
         // Merge Char
         if (fdChar)
            iRet = Tul_MergeChar(acBuf);

         // Merge Sale
         if (fdSale)
            iRet = Tul_MergeSale(acBuf);

         if (fdLegal)
            iRet = Tul_MergeLegal(acBuf);

         // Save last recording date
         lTmp = atoin((char *)&acBuf[OFF_SALE1_DT], 8);
         if (lTmp > lLastRecDate && lTmp < lToday)
            lLastRecDate = lTmp;

         bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
         if (!bRet)
         {
            LogMsg("***** Error occurs: %d\n", GetLastError());
            break;
         }
         lCnt++;
      }

      if (!(lLDRRecCount++ % 1000))
         printf("\r%u", lLDRRecCount);
   }

   // Close files
   if (fdRoll)
      fclose(fdRoll);
   if (fdChar)
      fclose(fdChar);
   if (fdSale)
      fclose(fdSale);
   if (fdLegal)
      fclose(fdLegal);
   if (fhOut)
      CloseHandle(fhOut);

   LogMsg("Total input records:           %u", lLDRRecCount);
   LogMsg("Total output records:          %u", lCnt);
   LogMsg("Total Sale matched:            %u", lSaleMatch);
   LogMsg("Total Char matched:            %u", lCharMatch);
   LogMsg("Total Legal matched:           %u", lLegalMatch);
   LogMsg("Total Sale skipped:            %u", lSaleSkip);
   LogMsg("Total Char skipped:            %u", lCharSkip);

   printf("\nTotal output records: %u\n", lCnt);

   lRecCnt = lCnt;
   return 0;
}


/****************************** Tul_CreateR01_1 ******************************
 *
 *
 *****************************************************************************/

int Tul_CreateR01_1(char *pOutbuf, char *pRollRec)
{
   char     acTmp[256], acTmp1[256];
   int      iRet;
   long     lTmp;

   // Check active parcel
   if (*(pRollRec+LOFF_1_ROLL_TYPE) != 'A')
      return -1;

   // Clear output buffer
   memset(pOutbuf, ' ', iRecLen);

   // Start copying data
   memcpy(pOutbuf, pRollRec+LOFF_1_APN, iApnLen);
   memcpy(pOutbuf+OFF_CO_NUM, "54TUL", 5);
   *(pOutbuf+OFF_STATUS) = 'A';

   // Format APN
   iRet = formatApn(pRollRec+LOFF_1_APN, acTmp, &myCounty);
   memcpy(pOutbuf+OFF_APN_D, acTmp, iRet);

   // Create MapLink
   iRet = formatMapLink(pRollRec+LOFF_1_APN, acTmp, &myCounty);
   memcpy(pOutbuf+OFF_MAPLINK, acTmp, iRet);

   // Create index map link
   if (getIndexPage(acTmp, acTmp1, &myCounty))
      memcpy(pOutbuf+OFF_IMAPLINK, acTmp1, iRet);

   // Year assessed
   memcpy(pOutbuf+OFF_YR_ASSD, myCounty.acYearAssd, 4);

   // Fee Asmt
   memcpy(pOutbuf+OFF_ALT_APN, pRollRec+LOFF_1_FEE_ASSMT_X, LSIZ_1_FEE_ASSMT_X);

   // Original APN
   memcpy(pOutbuf+OFF_PREV_APN, pRollRec+LOFF_1_ORIG_ASSMT_X, LSIZ_1_ORIG_ASSMT_X);

   // Lien values
   long lLand  = atoin(pRollRec+LOFF_1_MARKET_LAND_VAL, RSIZ_VALUE_AMT);
   long lImpr  = atoin(pRollRec+LOFF_1_STRUCT_IMPR_VAL, RSIZ_VALUE_AMT);
   long lFixt  = atoin(pRollRec+LOFF_1_FIXED_IMPR_VAL, RSIZ_VALUE_AMT);
   long lGrow  = atoin(pRollRec+LOFF_1_GROWING_IMPR_VAL, RSIZ_VALUE_AMT);
   long lPP_Val= atoin(pRollRec+LOFF_1_PERSON_PROP_VAL, RSIZ_VALUE_AMT);
   long lBusInv= atoin(pRollRec+LOFF_1_BUSINESS_INV, RSIZ_VALUE_AMT);

   // Land
   if (lLand > 0)
   {
      sprintf(acTmp, "%*d", SIZ_LAND, lLand);
      memcpy(pOutbuf+OFF_LAND, acTmp, SIZ_LAND);
   }

   // Structure impr
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*d", SIZ_LAND, lImpr);
      memcpy(pOutbuf+OFF_IMPR, acTmp, SIZ_LAND);

      sprintf(acTmp, "%*d", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
      memcpy(pOutbuf+OFF_RATIO, acTmp, SIZ_RATIO);
   }

   // Other impr
   long lOther = lPP_Val + lGrow + lFixt + lBusInv;
   if (lOther > 0)
   {
      sprintf(acTmp, "%*d", SIZ_LAND, lOther);
      memcpy(pOutbuf+OFF_OTHER, acTmp, SIZ_LAND);

      // Fixture/Machine/Equipment
      if (lFixt > 0)
      {
         sprintf(acTmp, "%d         ", lFixt);
         memcpy(pOutbuf+OFF_FIXTR, acTmp, SIZ_FIXTR);
      }
      // Personal property
      if (lPP_Val > 0)
      {
         sprintf(acTmp, "%d         ", lPP_Val);
         memcpy(pOutbuf+OFF_PERSPROP, acTmp, SIZ_PERSPROP);
      }
      // Growing impr
      if (lGrow > 0)
      {
         sprintf(acTmp, "%d         ", lGrow);
         memcpy(pOutbuf+OFF_GR_IMPR, acTmp, SIZ_GR_IMPR);
      }

      // Business inventory
      if (lBusInv > 0)
      {
         sprintf(acTmp, "%d         ", lBusInv);
         memcpy(pOutbuf+OFF_BUSINV, acTmp, SIZ_BUSINV);
      }
   }

   // Gross
   long lGross = lOther + lLand + lImpr;
   if (lGross > 0)
   {
      sprintf(acTmp, "%*d", SIZ_LAND, lGross);
      memcpy(pOutbuf+OFF_GROSS, acTmp, SIZ_LAND);
   }

   // Tax code
   //if (pRec->TaxCode[0] > ' ')
   //   memcpy(pOutbuf+OFF_TAX_CODE, pRec->TaxCode, LSIZ_TAXCODE);

   // Exemption
   lTmp  = atoin(pRollRec+LOFF_1_PROP_EXMPT_AMT_1, LSIZ_1_PROP_EXMPT_AMT_1);
   lTmp += atoin(pRollRec+LOFF_1_PROP_EXMPT_AMT_2, LSIZ_1_PROP_EXMPT_AMT_2);
   lTmp += atoin(pRollRec+LOFF_1_PROP_EXMPT_AMT_3, LSIZ_1_PROP_EXMPT_AMT_3);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_EXE_TOTAL, lTmp);
      memcpy(pOutbuf+OFF_EXE_TOTAL, acTmp, SIZ_EXE_TOTAL);

      // Full exemption
      if (lTmp >= lGross)
         *(pOutbuf+OFF_FULL_EXEMPT) = 'Y';
   }

   if (*(pRollRec+LOFF_1_EXMPT_CODE_1) == 'H')
      *(pOutbuf+OFF_HO_FL) = '1';      // HO Exempt
   else
      *(pOutbuf+OFF_HO_FL) = '2';

   // Exemption code
   *(pOutbuf+OFF_EXE_CD1) = *(pRollRec+LOFF_1_EXMPT_CODE_1);
   *(pOutbuf+OFF_EXE_CD2) = *(pRollRec+LOFF_1_EXMPT_CODE_2);
   *(pOutbuf+OFF_EXE_CD3) = *(pRollRec+LOFF_1_EXMPT_CODE_3);

   // Ag preserve
   if (*(pRollRec+LOFF_1_AG_PRESERVE_FLAG) == 'Y')
      *(pOutbuf+OFF_AG_PRE) = 'Y';

#ifdef _DEBUG
   //if (!memcmp(acApn, "914930455", 9))
   //   iRet = 1;
#endif

   // TRA
   memcpy(pOutbuf+OFF_TRA, pRollRec+LOFF_1_TAX_RATE_AREA, LSIZ_1_TAX_RATE_AREA);

   // Land Use
   if (*(pRollRec+LOFF_1_USE_CODE) > ' ')
   {
      memcpy(pOutbuf+OFF_USE_CO, pRollRec+LOFF_1_USE_CODE, LSIZ_1_USE_CODE);
      updateStdUse(pOutbuf+OFF_USE_STD, pRollRec+LOFF_1_USE_CODE, LSIZ_1_USE_CODE, pOutbuf);
   } else
      memcpy(pOutbuf+OFF_USE_STD, USE_UNASGN, SIZ_USE_STD);

   // Lot Acres - V99
   long lLotAcres = atoin(pRollRec+LOFF_1_ACRES, LSIZ_1_ACRES);
   if (lLotAcres > 1)
   {
      sprintf(acTmp, "%*d", SIZ_LOT_ACRES, lLotAcres*10);
      memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);

      lTmp = (long)((double)lLotAcres*SQFT_FACTOR_100);
      sprintf(acTmp, "%*d", SIZ_LOT_SQFT, lTmp);
      memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);
   }

   // Transfer doc
   lTmp = atoin(pRollRec+LOFF_1_CURRENT_DEED_DATE, 8);
   if (lTmp > 19000000 && lTmp < lToday)
   {
      memcpy(pOutbuf+OFF_TRANSFER_DT, pRollRec+LOFF_1_CURRENT_DEED_DATE, 8);
      memcpy(pOutbuf+OFF_TRANSFER_DOC, pRollRec+LOFF_1_CURRENT_DEED_NUM, LSIZ_1_CURRENT_DEED_NUM);
   }

   // Delq date - Data is all '00000000'

   // Owner
   Tul_MergeOwner(pOutbuf, pRollRec+LOFF_1_CURR_OWNER_NAME);

   // Mail 
   Tul_MergeMAdr(pOutbuf, pRollRec+LOFF_1_MAIL_ADDRESS_1, pRollRec+LOFF_1_MAIL_ADDRESS_2, pRollRec+LOFF_1_MAIL_ADDRESS_3, pRollRec+LOFF_1_MAIL_ADDRESS_4);

   // Situs - use situs file
   //Tul_MergeSitus(pOutbuf, pRollRec);
   
   return 0;
}

/******************************* Tul_Load_LDR1 ******************************
 *
 * Support LDR 2015
 * Create R01 from LDR file, merge situs & Chars to produce LDR roll
 *
 ****************************************************************************/

int Tul_Load_LDR1(int iFirstRec /* 1=create header rec */)
{
   char     *pTmp, acBuf[MAX_RECSIZE], acRollRec[MAX_RECSIZE];
   char     acOutFile[_MAX_PATH];

   HANDLE   fhOut;
   DWORD    nBytesWritten;
   BOOL     bRet;
   long     lCnt=0, lTmp;
   int      iRet;

   LogMsg0("Loading LDR %d for TUL", lLienYear);

   // Open Roll file
   LogMsg("Open LDR file %s", acLienFile);
   fdRoll = fopen(acLienFile, "r");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", acLienFile);
      return -1;
   }

   // Open Char file
   LogMsg("Open Char file %s", acCharFile);
   fdChar = fopen(acCharFile, "r");
   if (fdChar == NULL)
   {
      LogMsg("***** Error opening Char file: %s\n", acCharFile);
      return -2;
   }

   // Open situs
   LogMsg("Open Situs file %s", acSitusFile);
   fdSitus = fopen(acSitusFile, "r");
   if (fdSitus == NULL)
   {
      LogMsg("***** Error opening Situs file: %s\n", acSitusFile);
      return -3;
   }

   // Open legal file
   lLegalMatch = 0;
   if (acLegalExtr[0] >= 'A')
   {
      LogMsg("Open legal file %s", acLegalExtr);
      fdLegal = fopen(acLegalExtr, "r");
      if (fdLegal == NULL)
      {
         LogMsg("***** Error opening legal extract file: %s\n", acLegalExtr);
         return -3;
      }
   } else
      fdLegal = NULL;

   // Open Output file
   sprintf(acOutFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "R01");
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error creating output file: %s\n", acOutFile);
      return -4;
   }

   // Create header record
   if (iFirstRec)
   {
      memset(acBuf, '9', iRecLen);
      // Ignore record start with 99999999
      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
   }

   // Merge loop
   while (!feof(fdRoll))
   {
      if (!(pTmp = fgets(acRollRec, MAX_RECSIZE, fdRoll)))
         break;         // EOF
#ifdef _DEBUG
      //if (!memcmp(pRec->Apn2, "926000001", 9))
      //   iRet = 0;
#endif

      // Create new R01 record
      iRet = Tul_CreateR01_1(acBuf, acRollRec);

      if (!iRet)
      {
         // Merge Char
         if (fdChar)
            iRet = Tul_MergeChar(acBuf);

         // Merge Situs
         if (fdSitus)
            iRet = Tul_MergeSitus(acBuf);

         // Merge Legal
         if (fdLegal)
            iRet = Tul_MergeLegal(acBuf);

         // Save last recording date
         lTmp = atoin((char *)&acBuf[OFF_SALE1_DT], 8);
         if (lTmp > lLastRecDate && lTmp < lToday)
            lLastRecDate = lTmp;

         bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
         if (!bRet)
         {
            LogMsg("***** Error occurs: %d\n", GetLastError());
            break;
         }
         lLDRRecCount++;
      }

      if (!(lCnt++ % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdRoll)
      fclose(fdRoll);
   if (fdChar)
      fclose(fdChar);
   if (fdLegal)
      fclose(fdLegal);
   if (fdSitus)
      fclose(fdSitus);
   if (fhOut)
      CloseHandle(fhOut);

   LogMsg("Total input records:           %u", lCnt);
   LogMsg("Total output records:          %u\n", lLDRRecCount);
   LogMsg("Total Char matched:            %u", lCharMatch);
   LogMsg("Total Legal matched:           %u", lLegalMatch);
   LogMsg("Total Situs matched:           %u\n", lSitusMatch);
   LogMsg("Total Char skipped:            %u", lCharSkip);
   LogMsg("Total Situs skipped:           %u\n", lSitusSkip);

   printf("\nTotal output records: %u\n", lLDRRecCount);

   lRecCnt = lLDRRecCount;
   return 0;
}

/******************************* Tul_LegalExtr ******************************
 *
 * Extract legal from monthly update roll to populate LDR roll.
 * Only output record with legal, skip the rest.
 *
 ****************************************************************************/

int Tul_LegalExtr(void)
{
   char     *pTmp, acBuf[1024], acRec[1024];
   long     iRet, lCnt=0;

   TUL_ROLL *pRec;

   if (acLegalExtr[0] < 'A')
      return -1;

   // Open roll file
   LogMsg("Open Roll file %s", acRollFile);
   fdRoll = fopen(acRollFile, "r");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", acRollFile);
      return -1;
   }

   // Open Output file
   LogMsg("Open output file %s", acLegalExtr);
   fdLegal = fopen(acLegalExtr, "w");
   if (fdLegal == NULL)
   {
      LogMsg("***** Error creating legal file: %s\n", acLegalExtr);
      return -2;
   }

   pRec = (TUL_ROLL *)&acRec[0];
   while (!feof(fdRoll))
   {
      do
      {
         pTmp = fgets(acRec, 1024, fdRoll);
      } while (pTmp && (pRec->Status == 'I' || pRec->Status == 'D' || pRec->Apn[0] == ' '));

      if (!pTmp)
         break;         // EOF

#ifdef _DEBUG
      //if (!memcmp(pRec->Apn2, "926000001", 9))
      //   iRet = 0;
#endif
      // Create legal record
      iRet = blankRem(pRec->Legal, RSIZ_LEGAL);
      if (iRet > 5)
      {
         sprintf(acBuf, "%.*s|%s\n", RSIZ_APN, pRec->Apn2, pRec->Legal);

         // Output to file
         fputs(acBuf, fdLegal);
         if (!(++lCnt % 1000))
            printf("\r%u", lCnt);
         if (iRet > iMaxLegal)
            iMaxLegal = iRet;
      }
   }

   // Close files
   if (fdRoll)
      fclose(fdRoll);
   if (fdLegal)
      fclose(fdLegal);

   LogMsgD("\nTotal output records:       %u", lCnt);
   LogMsg("Max legal length:           %d", iMaxLegal);

   return 0;
}

/********************************* Tul_FormatSale ****************************
 *
 * Return -1 if bad record.  0 if OK.
 *
 *****************************************************************************/

int Tul_FormatSale(char *pFmtSale, char *pSale)
{
   SCSAL_REC *pCSale = (SCSAL_REC *)pFmtSale;
   TUL_SALE  *pSaleRec=(TUL_SALE  *)pSale;
   
   char     acTmp[32];
   long     lPrice, lTax, iTmp;

   // Drop records without DocDate & DocNum
   if (pSaleRec->Apn[0] == ' ' || pSaleRec->DocDate[0] == ' ')
      return -1;

   memset(pFmtSale, ' ', sizeof(SCSAL_REC));

   // APN
   memcpy(pCSale->Apn, pSaleRec->Apn, iApnLen);

   // DocNum
   memcpy(pCSale->DocNum, pSaleRec->DocNum, SSIZ_DOCNUM);

   // DocDate
   if (isValidYMD(pSaleRec->DocDate, false))
      memcpy(pCSale->DocDate, pSaleRec->DocDate, SIZ_SALE1_DT);
   else
   {
      memcpy(pSaleRec->DocDate, pSaleRec->DocNum, 4); 
      if (isValidYMD(pSaleRec->DocDate, false))
         memcpy(pCSale->DocDate, pSaleRec->DocDate, SIZ_SALE1_DT);
      else
      {
         LogMsg("*** Invalid sale date APN=%.*s: %.8s", iApnLen, pSaleRec->Apn, pSaleRec->DocDate);
         return -1;
      }
   }

   iTmp = atoin(pCSale->DocDate, 8);
   if (iTmp > lLastRecDate)
   {
      if (iTmp >= lToday)
      {
         LogMsg("*** Bad sale date: %d [%.12s]", iTmp, pCSale->Apn);
         return -1;
      } else
         lLastRecDate = iTmp;
   }

   // Doc Tax
   lTax = atoin(pSaleRec->Dtt_Amt, SSIZ_DTT_AMT);
   if (lTax > 550)
   {
      iTmp = sprintf(acTmp, "%.2f", (double)lTax/100.0);
      if (iTmp > 9)
      {
         LogMsg("??? Questionable tax amount APN=%.*s: %s", iApnLen, pSaleRec->Apn, acTmp);
         lTax = 0;
      } else
         memcpy(pCSale->StampAmt, acTmp, iTmp);
   }

   // Sale Price
   lPrice = atoin(pSaleRec->SalePrice, SSIZ_SALE_AMT);
   if (!lPrice)
      lPrice = (long)((double)lTax*SALE_FACTOR_100);

   if (lPrice > 1000)
   {
      sprintf(acTmp, "%*d", SIZ_SALE1_AMT, lPrice);
      memcpy(pCSale->SalePrice, acTmp, SIZ_SALE1_AMT);
   }

   // Transfer Type
   if (pSaleRec->Transfer_Type[0] > ' ')
   {
      iTmp = 0;
      while (iTmp < MAX_SALETYPE && *asSaleTypes[iTmp].pName)
      {
         if (!memcmp(pSaleRec->Transfer_Type, asSaleTypes[iTmp].pName, SSIZ_TRANSFER_TYPE))
         {
            pCSale->SaleCode[0] = *asSaleTypes[iTmp].pCode;
            break;
         }
         iTmp++;
      }
   }

   // Seller
   memcpy(pCSale->Seller1, pSaleRec->Seller, SSIZ_SELLER);

   // Owner
   memcpy(pCSale->Name1, pSaleRec->Owner, SSIZ_OWNER);

   // Group sale
   if (pSaleRec->Group_Sale_Flag > ' ')
   {
      pCSale->MultiSale_Flg = pSaleRec->Group_Sale_Flag;
      memcpy(pCSale->PrimaryApn, pSaleRec->Group_Apn, SSIZ_GROUP_APN);
   }

   pCSale->ARCode = 'A';
   pCSale->CRLF[0] = '\n';
   pCSale->CRLF[1] = 0;

   return 0;
}

/****************************** Tul_ExtrSale *********************************
 *
 * Extract history sale.  
 *
 * Return 0 if successful, otherwise error
 *
 *****************************************************************************/

int Tul_ExtrSale(void)
{
   char     *pTmp, acCSalRec[1024], acSaleRec[1024], acTmp[256];
   char     acTmpSale[_MAX_PATH];

   long     lCnt=0;
   int		iRet, iTmp, iUpdateSale=0;

   lLastRecDate = 0;

   // Check current sale file
   if (_access(acSaleFile, 0))
   {
      LogMsg("***** Missing current sale file %s", acSaleFile);
      return -1;
   }

   // Open current sale
   LogMsg("Open current sale file %s", acSaleFile);
   fdSale = fopen(acSaleFile, "r");
   if (fdSale == NULL)
   {
      LogMsg("***** Error opening current sale file: %s\n", acSaleFile);
      return -2;
   }

   // Create output file
   sprintf(acTmpSale, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "tmp");
   LogMsg("Create updated sale file %s", acTmpSale);
   fdCSale = fopen(acTmpSale, "w");
   if (fdCSale == NULL)
   {
      LogMsg("***** Error creating output sale file: %s\n", acTmpSale);
      return -3;
   }

   // Merge loop
   while (!feof(fdSale))
   {

      // Get current sale
      pTmp = fgets(acSaleRec, 1024, fdSale);
      if (!pTmp)
         break;

      iRet = Tul_FormatSale(acCSalRec, acSaleRec);
      if (!iRet)
      {
         fputs(acCSalRec, fdCSale);
         iUpdateSale++;
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);

   }

   // Close files
   if (fdCSale)
      fclose(fdCSale);
   if (fdSale)
      fclose(fdSale);

   // Update sale history file
   sprintf(acCSalFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "sls");
   if (!_access(acCSalFile, 0))
   {
      sprintf(acTmp, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "sav");
      if (!_access(acTmp, 0))
         DeleteFile(acTmp);
      LogMsg("Rename %s to %s", acCSalFile, acTmp);
      MoveFile(acCSalFile, acTmp);
   }
   // Sort/merge sale file - APN (asc), Saledate (asc), DocNum (asc)
   sprintf(acTmp, "S(1,%d,C,A,27,8,C,A,15,%d,C,A,57,10,C,D) DUPO(1,34)", iApnLen, SALE_SIZ_DOCNUM);
   iTmp = sortFile(acTmpSale, acCSalFile, acTmp);

   LogMsg("Total sale records processed:  %u", lCnt);
   LogMsg("Total sale records updated:    %u", iUpdateSale);
   LogMsg("Total extracted sale records:  %u", iTmp);
   LogMsg("         Last recording date:  %u\n", lLastRecDate);
   
   return 0;
}

/******************************** Tul_ExtrExe *******************************
 *
 * Extract Exemption code from LDR or roll file
 *
 ****************************************************************************/

int Tul_ExtrExe(char *pInfile, char cType)
{
   char  acBuf[256], acRec[MAX_RECSIZE], acOutFile[_MAX_PATH], *pTmp;
   long  lCnt=0, lOut=0, lExe;
   int   iTmp;
   TUL_ROLL *pRec = (TUL_ROLL *)acRec;

   // Open roll file
   LogMsg("Open input file %s", pInfile);
   fdRoll = fopen(pInfile, "rb");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening roll file: %s (%d)\n", pInfile, errno);
      return 2;
   }

   // Open Output file
   if (cType == 'L')
      sprintf(acOutFile, acLienTmpl, myCounty.acCntyCode, "LEX");
   else
      sprintf(acOutFile, acLienTmpl, myCounty.acCntyCode, "REX");
   LogMsg("Open output file %s", acOutFile);
   fdLien = fopen(acOutFile, "w");
   if (fdLien == NULL)
   {
      LogMsg("***** Error opening Lien extract file: %s\n", acOutFile);
      return 4;
   }

   // Merge loop
   while (!feof(fdRoll))
   {
      // Get roll rec
      if (!(pTmp = fgets(acRec, MAX_RECSIZE, fdRoll)))
         break;

      if (cType == 'L')
      {
         if (acRec[LOFF_1_EXMPT_CODE_1] > ' ')
         {
            lExe = atoin(&acRec[LOFF_1_PROP_EXMPT_AMT_1], LSIZ_1_PROP_EXMPT_AMT_1);
            iTmp = sprintf(acBuf, "%.12s|%c|%d\n", acRec, acRec[LOFF_1_EXMPT_CODE_1], lExe);
            fputs(acBuf, fdLien);
            lOut++;
         } 
         if (acRec[LOFF_1_EXMPT_CODE_2] > ' ')
         {
            lExe = atoin(&acRec[LOFF_1_PROP_EXMPT_AMT_2], LSIZ_1_PROP_EXMPT_AMT_2);
            iTmp = sprintf(acBuf, "%.12s|%c|%d\n", acRec, acRec[LOFF_1_EXMPT_CODE_2], lExe);
            fputs(acBuf, fdLien);
            lOut++;
         } 
         if (acRec[LOFF_1_EXMPT_CODE_3] > ' ')
         {
            lExe = atoin(&acRec[LOFF_1_PROP_EXMPT_AMT_3], LSIZ_1_PROP_EXMPT_AMT_3);
            iTmp = sprintf(acBuf, "%.12s|%c|%d\n", acRec, acRec[LOFF_1_EXMPT_CODE_3], lExe);
            fputs(acBuf, fdLien);
            lOut++;
         } 
      } else
      {
         // Exempt code
         if (pRec->Exemp[0].Code[0] > ' ')
         {
            lExe = atoin(pRec->Exemp[0].Amount, RSIZ_EXEMP_AMT);
            iTmp = sprintf(acBuf, "%.12s|%.3s|%d\n", pRec->Apn, pRec->Exemp[0].Code, lExe);
            fputs(acBuf, fdLien);
            lOut++;
         } 
         if (pRec->Exemp[1].Code[0] > ' ')
         {
            lExe = atoin(pRec->Exemp[1].Amount, RSIZ_EXEMP_AMT);
            iTmp = sprintf(acBuf, "%.12s|%.3s|%d\n", pRec->Apn, pRec->Exemp[1].Code, lExe);
            fputs(acBuf, fdLien);
            lOut++;
         } 
         if (pRec->Exemp[2].Code[0] > ' ')
         {
            lExe = atoin(pRec->Exemp[2].Amount, RSIZ_EXEMP_AMT);
            iTmp = sprintf(acBuf, "%.12s|%.3s|%d\n", pRec->Apn, pRec->Exemp[2].Code, lExe);
            fputs(acBuf, fdLien);
            lOut++;
         } 
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdRoll)
      fclose(fdRoll);
   if (fdLien)
      fclose(fdLien);

   LogMsg("Total records processed:     %d", lCnt);
   LogMsg("         records output:     %d\n", lOut);

   return 0;
}

/****************************** Tul_FmtChar **********************************
 *
 * Convert TUL_CHAR record to STDCHAR format
 *
 *****************************************************************************/

int Tul_FmtChar(char *pOutbuf, char *pInbuf)
{
   char     acTmp[_MAX_PATH], acCode[32], *pTmp;
   int      iTmp, iRet, iCnt;
   ULONG    lLotAcres, lLotSqft;

   STDCHAR  *pCharRec=(STDCHAR *)pOutbuf;
   TUL_CHAR *pRawRec =(TUL_CHAR *)pInbuf;
   
   // Init output buffer
   memset(pOutbuf, ' ', sizeof(STDCHAR));

   // APN
   memcpy(pCharRec->Apn, pRawRec->Apn, RSIZ_APN);

   // Format APN
   iTmp = formatApn(pRawRec->Apn, acTmp, &myCounty);
   memcpy(pCharRec->Apn_D, acTmp, iTmp);

   // Lot Sqft
   lLotSqft = atoln(pRawRec->LotSqft, CSIZ_LOT_SQFT);
   if (lLotSqft > 0)
   {
      iTmp = sprintf(acTmp, "%u", lLotSqft);
      memcpy(pCharRec->LotSqft, acTmp, iTmp);

      lLotAcres = (long)((double)lLotSqft*SQFT_MF_1000);
      iTmp = sprintf(acTmp, "%u", lLotAcres);
      memcpy(pCharRec->LotAcre, acTmp, iTmp);
   }

   // BldgSqft
   long lBldgSqft = atoin(pRawRec->Main_Bldg_Sqft, CSIZ_MAIN_BLDG_SQFT);
   if (lBldgSqft > 10)
   {
      iTmp = sprintf(acTmp, "%d", lBldgSqft);
      memcpy(pCharRec->BldgSqft, acTmp, iTmp);
   }

   // Zoning
   if (pRawRec->Zoning[0] > '0')
      memcpy(pCharRec->Zoning, pRawRec->Zoning, SIZ_ZONE);

   // Bldgs
   iCnt = atoin(pRawRec->Number_Of_Bldgs, CSIZ_NUMBER_OF_BLDGS);
   if (iCnt > 0)
   {
      iTmp = sprintf(acTmp, "%d", iCnt);
      memcpy(pCharRec->Bldgs, acTmp, iTmp);
   }

   // Rooms
   iCnt = atoin(pRawRec->Total_Rooms, CSIZ_TOTAL_ROOMS);
   if (iCnt > 0)
   {
      iTmp = sprintf(acTmp, "%d", iCnt);
      memcpy(pCharRec->Rooms, acTmp, iTmp);
   }

   // Beds
   iCnt = atoin(pRawRec->Bedrooms, CSIZ_BEDROOMS);
   if (iCnt > 0)
   {
      iTmp = sprintf(acTmp, "%d", iCnt);
      memcpy(pCharRec->Beds, acTmp, iTmp);
   }

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "001011014000", 9))
   //   iTmp = 0;
#endif

   // Baths V99 - 25, 50, 75
   int iFBath = atoin(pRawRec->Bathrooms, CSIZ_BATHROOMS-2);
   int iHBath = atoin(&pRawRec->Bathrooms[2], CSIZ_BATHROOMS-2);
   iCnt   = atoin(pRawRec->Bathrooms, CSIZ_BATHROOMS);
   if (iCnt > 0 && iCnt < 10)
   {
      iHBath = 0;
      iFBath = iCnt;
   } else if (iHBath == 75)
   {
      pCharRec->Bath_3Q[0] = '1';
   } else if (iHBath > 0)
   {
      if (iHBath == 25)
         pCharRec->Bath_1Q[0] = '1';
      else if (iHBath == 50)
         pCharRec->Bath_2Q[0] = '1';
      else
      {
         // Value is too big or invalid
         if (iFBath > 9)
            iFBath = 0;
         iHBath = 0; 
         lBadBath++;
         if (bDebug)
            LogMsg("*** Bad bath value: %.4s", pRawRec->Bathrooms);
      }
   }
   if (iFBath > 0 && iFBath < 10)
   {
      pCharRec->Bath_4Q[0] = iFBath|0x30;
      if (iHBath > 50)
         iFBath++;

      // Full bath
      iTmp = sprintf(acTmp, "%d", iFBath);
      memcpy(pCharRec->FBaths, acTmp, iTmp);
   }

   if (iHBath > 0)
      pCharRec->HBaths[0] = '1';

   // Heating/Cooling
   if (pRawRec->Heat_Cool_Desc[0] > ' ' || pRawRec->Heat_Cool_Desc[1] > ' ')
   {
      memcpy(acTmp, pRawRec->Heat_Cool_Desc, CSIZ_HEAT_COOL_DESC);
      acTmp[CSIZ_HEAT_COOL_DESC] = 0;

      if (acTmp[0] != '\\')
      {
         // Check for most popular one
         if (strstr(acTmp, "DUO"))
         {
            pCharRec->Heating[0] = 'W';
            pCharRec->Cooling[0] = 'D';
         } else if (!memcmp(acTmp, "CENT", 4))
         {
            pCharRec->Heating[0] = 'Z';
            pCharRec->Cooling[0] = 'C';
         } else if (!memcmp(acTmp, "FA/REF", 6) || !memcmp(acTmp, "FAF/REF", 7))
         {
            pCharRec->Heating[0] = 'B';
            pCharRec->Cooling[0] = 'A';
         } else if (pTmp = Tul_FindHC(acTmp))
         {
            pCharRec->Heating[0] = *pTmp++;
            pCharRec->Cooling[0] = *pTmp;
         } else
         {  
            if (pTmp = Tul_FindHeat(acTmp))
               pCharRec->Heating[0] = *pTmp;
            else     
            {
               *(pOutbuf+OFF_HEAT)     = 'X';      // No translation for this type, set it to OTHER
               if (bDebug)
                  LogMsg("*** Unknown Heater in: %s", acTmp);
            }
            if (pTmp = Tul_FindCool(acTmp))
               pCharRec->Cooling[0] = *pTmp;
            else
            {
               pCharRec->Cooling[0] = 'X';
               if (bDebug)
                  LogMsg("*** Unknown Cooler in: %s", acTmp);
            }
         }
      }
   }

   // YrBlt
   iTmp = atoin(pRawRec->Const_Year, CSIZ_CONST_YEAR);
   if (iTmp > 1900)
      memcpy(pCharRec->YrBlt, pRawRec->Const_Year, CSIZ_CONST_YEAR);

   // Quality/class
   if (isalpha(pRawRec->Const_Type))
   {  // D045
      iTmp = sprintf(acTmp, "%c%.3s", pRawRec->Const_Type, pRawRec->QualClass);
      memcpy(pCharRec->QualityClass, acTmp, iTmp);
      pCharRec->BldgClass = pRawRec->Const_Type;
      iTmp = atoin(pRawRec->QualClass, 2);
      if (iTmp > 0)
      {
         sprintf(acTmp, "%d.%c", iTmp, pRawRec->QualClass[2]);
         iRet = Value2Code(acTmp, acCode, NULL);
         if (iRet >= 0)
            pCharRec->BldgQual = acCode[0];
      }
   }

   // Stories - changed by spn 02/25/2009 - reversed by spn 3/16/2009
   iCnt = atoin(pRawRec->Stories, CSIZ_STORIES);
   if (iCnt > 0 && iCnt < 99)
   {
      iTmp = sprintf(acTmp, "%d.0", iCnt);
      memcpy(pCharRec->Stories, acTmp, iTmp);
   }

   // Water
   if (pRawRec->Water_Service[0] > ' ')
   {
      iTmp = 0;
      while (Tul_Water[iTmp].iLen > 0)
      {
         if (!memcmp(pRawRec->Water_Service, Tul_Water[iTmp].acSrc, Tul_Water[iTmp].iLen))
         {
            pCharRec->Water = Tul_Water[iTmp].acCode[0];
            break;
         }
         iTmp++;
      }
   }

   // Sewer
   if (pRawRec->Sanitation[0] > ' ')
   {
      iTmp = 0;
      while (Tul_Sewer[iTmp].iLen > 0)
      {
         if (!memcmp(pRawRec->Sanitation, Tul_Sewer[iTmp].acSrc, Tul_Sewer[iTmp].iLen))
         {
            pCharRec->Sewer = Tul_Sewer[iTmp].acCode[0];
            break;
         }
         iTmp++;
      }
   }

   // Units
   int iUnits = atoin(pRawRec->Number_Of_Units, CSIZ_NUMBER_OF_UNITS);
   if (iUnits > 0)
   {
      iTmp = sprintf(acTmp, "%d", iUnits);
      memcpy(pCharRec->Units, acTmp, iTmp);
   }

   // Garage area
   long lGarSqft = atoin(pRawRec->Garage_Size, CSIZ_GARAGE_SIZE);
   long lCarpSqft = atoin(pRawRec->Carport_Size, CSIZ_GARAGE_SIZE);

   // Check for multi-unit appartment
   if (lCarpSqft > 1000 && iUnits > 4)
   {
      iTmp = sprintf(acTmp, "%d", lCarpSqft);
      memcpy(pCharRec->GarSqft, acTmp, iTmp);
      pCharRec->ParkType[0] = 'C';
   } else if (lGarSqft > 100)
   {
      iTmp = sprintf(acTmp, "%d", lGarSqft);
      memcpy(pCharRec->GarSqft, acTmp, iTmp);
      pCharRec->ParkType[0] = 'Z';
   } else if (lCarpSqft > 100)
   {
      iTmp = sprintf(acTmp, "%d", lCarpSqft);
      memcpy(pCharRec->GarSqft, acTmp, iTmp);
      pCharRec->ParkType[0] = 'C';
   }

   // Pool/Spa
   if (pRawRec->Pool == 'Y' && pRawRec->Spa == 'Y')
      pCharRec->Pool [0]= 'C';      // Pool/Spa
   else if (pRawRec->Pool == 'Y')
      pCharRec->Pool [0]= 'P';      // Pool
   else if (pRawRec->Spa == 'Y')
      pCharRec->Pool [0]= 'S';      // Spa

   // Fireplace
   iTmp = atoin(pRawRec->FirePlace, CSIZ_FIREPLACE);
   if (iTmp > 9)
      pCharRec->Fireplace[0] = 'M';
   else if (iTmp > 0)
      pCharRec->Fireplace[0] = '0' | iTmp;
   else if (pRawRec->FirePlace[0] > '0')
      LogMsg("*** Bad FirePlace %.*s [%.*s]", CSIZ_FIREPLACE, pRawRec->FirePlace, iApnLen, pRawRec->Apn);

   pCharRec->CRLF[0] = '\n';
   pCharRec->CRLF[1] = 0;

   return 0;
}

/***************************** Tul_ConvStdChar *******************************
 *
 * Return 0 if successful, otherwise error
 *
 *****************************************************************************/

int Tul_ConvStdChar(char *pInfile)
{
   char  acCharRec[2048], acInRec[MAX_RECSIZE], *pTmp;
   long  lCnt=0, lChars=0, iRet;
   FILE  *fdIn;

   LogMsg0("Extracting Chars from %s", pInfile);

   // Check current sale file
   if (_access(pInfile, 0))
   {
      LogMsg("***** Missing input file %s", pInfile);
      return -1;
   }

   // Open char file
   LogMsg("Open char file to extract CHARS %s", pInfile);
   fdIn = fopen(pInfile, "r");
   if (fdIn == NULL)
   {
      LogMsg("***** Error opening char file: %s\n", pInfile);
      return -2;
   }

   // Create output file
   LogMsg("Create output chars file %s", acCChrFile);
   fdChar = fopen(acCChrFile, "w");
   if (fdChar == NULL)
   {
      LogMsg("***** Error creating char file: %s\n", acCChrFile);
      return -3;
   }

   // Merge loop
   while (!feof(fdIn))
   {
      // Get next record
      pTmp = fgets(acInRec, MAX_RECSIZE, fdIn);
#ifdef _DEBUG
   //if (!memcmp(acInRec, "00001018", 8) )
   //   iRet = 0;
#endif

      //iRet = replUnPrtChar(acInRec, ' ', iCharLen-1);
      //if (iRet > 0)
      //   iRet = 0;
      iRet = Tul_FmtChar(acCharRec, acInRec);

      if (!iRet)
      {
         fputs(acCharRec, fdChar);
         lChars++;
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);

   }

   // Close files
   if (fdChar)
      fclose(fdChar);
   if (fdIn)
      fclose(fdIn);

   LogMsg("Total input records processed: %u", lCnt);
   LogMsg("Total char records output:     %u", lChars);

   return iRet;
}

/*********************************** loadTul ********************************
 *
 * Run LoadOne with following options:
 *    -L -Xl -Xd                 : load lien
 *    -U [-Xs] [-Xd] [-T]        : normal update roll
 *    -T                         : load tax file
 *
 * Notes: This county requires resort of roll file.  Therefore roll file is
 *        reloading everytime.
 *
 ****************************************************************************/

int loadTul(int iSkip)
{
   int      iRet=0, iErr;
   char     acDefFile[_MAX_PATH], acTmpFile[_MAX_PATH], acTmp[_MAX_PATH];
   bool     bRollCnv=false, bLdrCnv=false;

   iApnLen = myCounty.iApnLen;

   // Load tax 
   if (iLoadTax == TAX_LOADING)                    // -T 
   {
      TC_SetDateFmt(MM_DD_YYYY_1, true);
      iRet = TC_LoadTax(myCounty.acCntyCode, bTaxImport);
   }

   // Exit if load/update tax only
   if (!iLoadFlag)
      return iRet;

   // Sale extract
   if (iLoadFlag & EXTR_SALE)                      // -Xs
   {
      // Convert sale file to ascii
      GetIniString(myCounty.acCntyCode, "SaleDef", "", acDefFile, _MAX_PATH, acIniFile);
      if (_access(acDefFile, 0))
      {
         LogMsg("***** Error: missing Sale definition file %s", acDefFile);
         return 1;
      }
      sprintf(acTmpFile, "%s\\%s\\%s_Sale.asc", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
      LogMsg("Translate %s to Ascii %s", acSaleFile, acTmpFile);
      iRet = F_Ebc2Asc(acSaleFile, acTmpFile, acDefFile, 0);
      if (!iRet)
      {
         sprintf(acSaleFile, "%s\\%s\\%s_Sale.srt", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
         // Sort sale file
         sprintf(acTmp, "S(1,12,C,A,%d,8,C,A,%d,12,C,A) DUPO(1,12,%d,8,%d,12) F(TXT)",
            SOFF_DOCDATE, SOFF_DOCNUM, SOFF_DOCDATE, SOFF_DOCNUM);
         iRet = sortFile(acTmpFile, acSaleFile, acTmp);
         if (iRet <= 0)
         {
            iLoadFlag = 0;
            LogMsg("***** ERROR sorting sale file %s to %s", acTmpFile, acSaleFile);
            return 2;
         }
      } else
      {
         LogMsg("***** Error converting EBCDIC to ASCII %s", acSaleFile);
         return 1;
      }

      LogMsg0("Extract %s sale history file", myCounty.acCntyCode);
      iRet = Tul_ExtrSale();
      if (!iRet)         
         iLoadFlag |= MERG_CSAL;                // Signal merge sale to R01
   }

   GetIniString(myCounty.acCntyCode, "LegalExtr", "", acLegalExtr, _MAX_PATH, acIniFile);
   GetIniString(myCounty.acCntyCode, "LienDataType", "", acTmp, _MAX_PATH, acIniFile);
   if (acTmp[0] == 'E')
      bLdrCnv = true;

   if (iLoadFlag & (LOAD_LIEN|EXTR_LIEN) )
   {
      if (bLdrCnv)
      {
         // Convert lien file to ascii
         GetIniString(myCounty.acCntyCode, "LienDef", "", acDefFile, _MAX_PATH, acIniFile);
         sprintf(acTmpFile, "%s\\%s\\%s_Lien.asc", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
         if (_access(acDefFile, 0))
         {
            LogMsg("***** Error: missing definition file %s", acDefFile);
            return 1;
         }

         LogMsg("Translate %s to Ascii %s", acLienFile, acTmpFile);
         iRet = F_Ebc2Asc(acLienFile, acTmpFile, acDefFile, 0);
         if (iRet)
         {
            LogMsg("***** Error: bad definition file %s", acDefFile);
            return 2;
         }
      } else
         strcpy(acTmpFile, acLienFile);

      // Sort LDR file
      sprintf(acLienFile, "%s\\%s\\%s_Lien.srt", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
      sprintf(acTmp, "S(1,%d,C,A) ", iApnLen);
      LogMsg("Sort LDR file %s -> %s", acTmpFile, acLienFile);
      iRet = sortFile(acTmpFile, acLienFile, acTmp);
      if (iRet <= 0)
      {
         LogMsg("***** ERROR sorting LDR roll %s to %s", acTmpFile, acLienFile);
         return -1;
      }
   }

   // Extract legal.  Use as needed
   if (iLoadFlag & (LOAD_UPDT|EXTR_DESC))          // -Xd or -U
   {
      // Convert Roll file to ascii
      GetIniString(myCounty.acCntyCode, "RollFile", "", acRollFile, _MAX_PATH, acIniFile);
      iRollLen = GetPrivateProfileInt(myCounty.acCntyCode, "RollRecSize", 0, acIniFile);
      GetIniString(myCounty.acCntyCode, "RollDef", "", acDefFile, _MAX_PATH, acIniFile);
      sprintf(acTmpFile, "%s\\%s\\%s_Roll.asc", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
      if (_access(acDefFile, 0))
      {
         LogMsg("***** Error: missing definition file %s", acDefFile);
         return 1;
      }

      LogMsg("Translate %s to Ascii %s", acRollFile, acTmpFile);
      iRet = F_Ebc2Asc(acRollFile, acTmpFile, acDefFile, 0);
      if (iRet)
      {
         LogMsg("***** Error: bad definition file %s", acDefFile);
         return 2;
      }
      // Sort roll file
      sprintf(acRollFile, "%s\\%s\\%s_Roll.srt", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
      sprintf(acTmp, "S(%d,%d,C,A) ", ROFF_APN2, RSIZ_APN2);
      LogMsg("Sort Roll file %s -> %s", acTmpFile, acRollFile);
      iRet = sortFile(acTmpFile, acRollFile, acTmp);
      if (iRet <= 0)
      {
         LogMsg("***** ERROR sorting LDR roll %s to %s", acTmpFile, acRollFile);
         return -1;
      }

      bRollCnv = true;
   }

   // One time run
   //iRet = Tul_ExtrExe(acRollFile, 'R');

   // Load tables
   //iRet = LoadLUTable((char *)&acLookupTbl[0], "[Quality]", NULL, MAX_ATTR_ENTRIES);
   //if (iRet <= 0)
   //{
   //   LogMsg("***** Error Looking for section [Quality] in %s", acLookupTbl);
   //   return 1;
   //}

   if (iLoadFlag & (LOAD_UPDT|EXTR_DESC))          // -Xd
      iRet = Tul_LegalExtr();

   // Extract lien data
   if (iLoadFlag & EXTR_LIEN)                      // -Xl
   {
      if (lLienYear >= 2015)
         iRet = Tul_ExtrLien1();
      else
         iRet = Tul_ExtrLien();
   }

   // Extract CHAR file
   if (iLoadFlag & (EXTR_ATTR|LOAD_UPDT))          
   {
      // Convert char file to ascii
      GetIniString(myCounty.acCntyCode, "CharDef", "", acDefFile, _MAX_PATH, acIniFile);
      if (_access(acDefFile, 0))
      {
         LogMsg("***** Error: missing CHAR definition file %s", acDefFile);
         return 1;
      }

      sprintf(acTmpFile, "%s\\%s\\%s_Char.asc", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
      LogMsg("Translate %s to Ascii %s", acCharFile, acTmpFile);
      iRet = F_Ebc2Asc(acCharFile, acTmpFile, acDefFile, 0);
      if (iRet)
      {
         LogMsg("***** Error converting Char file EBCDIC to ASCII %s", acCharFile);
         return iRet;
      }
      strcpy(acCharFile, acTmpFile);
      
      if (iLoadFlag & EXTR_ATTR)                   // -Xa
         iRet = Tul_ConvStdChar(acCharFile);
   }

   if (iLoadFlag & (LOAD_LIEN|LOAD_UPDT))
   {
      // Convert situs file to ascii
      GetIniString(myCounty.acCntyCode, "SitusDef", "", acDefFile, _MAX_PATH, acIniFile);
      if (_access(acDefFile, 0))
      {
         LogMsg("***** Error: missing Situs definition file %s", acDefFile);
         return 1;
      }

      GetIniString(myCounty.acCntyCode, "SitusFile", "", acSitusFile, _MAX_PATH, acIniFile);
      sprintf(acTmpFile, "%s\\%s\\%s_Situs.asc", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
      LogMsg("Translate %s to Ascii %s", acSitusFile, acTmpFile);
      iRet = F_Ebc2Asc(acSitusFile, acTmpFile, acDefFile, 0);
      sprintf(acSitusFile, "%s\\%s\\%s_Situs.srt", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
      iRet = sortFile(acTmpFile, acSitusFile, "S(44,12,C,A)  ", &iErr);
      if (!iRet)
      {
         LogMsg("***** Error converting EBCDIC to ASCII %s (%d)", acSitusFile, iErr);
         return iRet;
      }

      if (iLoadFlag & LOAD_UPDT)                   // -U
      {
         if (!bRollCnv)
         {
            // Convert Roll file to ascii
            GetIniString(myCounty.acCntyCode, "RollFile", "", acRollFile, _MAX_PATH, acIniFile);
            iRollLen = GetPrivateProfileInt(myCounty.acCntyCode, "RollRecSize", 0, acIniFile);
            GetIniString(myCounty.acCntyCode, "RollDef", "", acDefFile, _MAX_PATH, acIniFile);
            sprintf(acTmpFile, "%s\\%s\\%s_Roll.asc", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
            if (_access(acDefFile, 0))
            {
               LogMsg("***** Error: missing definition file %s", acDefFile);
               return 1;
            }

            LogMsg("Translate %s to Ascii %s", acRollFile, acTmpFile);
            iRet = F_Ebc2Asc(acRollFile, acTmpFile, acDefFile, 0);
            if (iRet)
            {
               LogMsg("***** Error: bad definition file %s", acDefFile);
               return 2;
            }
            // Sort roll file
            sprintf(acRollFile, "%s\\%s\\%s_Roll.srt", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
            sprintf(acTmp, "S(%d,%d,C,A) ", ROFF_APN2, RSIZ_APN2);
            LogMsg("Sort Roll file %s -> %s", acTmpFile, acRollFile);
            iRet = sortFile(acTmpFile, acRollFile, acTmp);
            if (iRet <= 0)
            {
               LogMsg("***** ERROR sorting LDR roll %s to %s", acTmpFile, acRollFile);
               return -1;
            }
         }

         // Convert Owner file to ascii
         GetIniString(myCounty.acCntyCode, "OwnerDef", "", acDefFile, _MAX_PATH, acIniFile);
         if (_access(acDefFile, 0))
         {
            LogMsg("***** Error: missing Owner definition file %s", acDefFile);
            return 1;
         }

         GetIniString(myCounty.acCntyCode, "OwnerFile", "", acOwnerFile, _MAX_PATH, acIniFile);
         sprintf(acTmpFile, "%s\\%s\\%s_Owner.asc", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
         LogMsg("Translate %s to Ascii %s", acOwnerFile, acTmpFile);
         iRet = F_Ebc2Asc(acOwnerFile, acTmpFile, acDefFile, 0);
         if (!iRet)
         {
            sprintf(acOwnerFile, "%s\\%s\\%s_Owner.dat", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
            // Sort Owner file on APN and Owner Number
            sprintf(acTmp, "S(1,%d,C,A,%d,%d,C,A) F(TXT)", OSIZ_APN, OOFF_OWNER_NUMBER, OSIZ_OWNER_NUMBER);
            iRet = sortFile(acTmpFile, acOwnerFile, acTmp);
            if (iRet <= 0)
            {
               iLoadFlag = 0;
               LogMsg("***** ERROR sorting owner file %s to %s", acTmpFile, acOwnerFile);
               return 2;
            }
         } else
         {
            LogMsg("***** Error converting Owner file EBCDIC to ASCII %s", acOwnerFile);
            return iRet;
         }

         // Convert NAD file to ascii
         GetIniString(myCounty.acCntyCode, "NadDef", "", acDefFile, _MAX_PATH, acIniFile);
         if (_access(acDefFile, 0))
         {
            LogMsg("***** Error: missing definition file %s", acDefFile);
            return 1;
         }

         GetIniString(myCounty.acCntyCode, "NadFile", "", acNadFile, _MAX_PATH, acIniFile);
         sprintf(acTmpFile, "%s\\%s\\%s_Nad.asc", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
         LogMsg("Translate %s to Ascii %s", acNadFile, acTmpFile);
         iRet = F_Ebc2Asc(acNadFile, acTmpFile, acDefFile, 0);
         if (iRet)
         {
            LogMsg("***** Error converting NAD file EBCDIC to ASCII %s", acNadFile);
            return iRet;
         }
         strcpy(acNadFile, acTmpFile);
      }  // LOAD_UPDT

      bRemTmpFile = false;
      if (iLoadFlag & LOAD_LIEN)                   // -L
      {
         if (lLienYear >= 2015)
            iRet = Tul_Load_LDR1(iSkip);
         else
            iRet = Tul_Load_LDR(iSkip);
      } else if (iLoadFlag & LOAD_UPDT)            // -U
      {
         // Create roll file .T01
         iRet = Tul_Load_Roll(iSkip);
         if (!iRet)
         {
            iRet = Tul_MergePQ4(iSkip);
         }
      }
   }

   // Update sale
   if (!iRet && (iLoadFlag & MERG_CSAL))
      iRet = ApplyCumSale(iSkip, acCSalFile, false, SALE_USE_SCUPDXFR);

   return iRet;
}