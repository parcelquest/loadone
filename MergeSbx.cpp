/**************************************************************************
 *
 * Notes:
 *    1) LDR file contains Owner2 but update roll doesn't.
 *       We just update LDR version with both name and leave the rest alone.
 *    2) SBX name and format is changed every year.  Check for APPRAISER
 *       and realign SBX_ROLL_AGPRESERVEID accordingly.
 *    3) Also watchout for date format changed in CreateSbxRec().
 *    4) Matchup roll data and web data to complete each transaction with all needed fields.
 *
 * Operating commands:
 *    -L : to load lien
 *    -U : to load update roll
 *
 * 11/27/2006 1.3.6     Fix bug in Sbx_UpdateAdr() and Sbx_MergeAdr() which
 *                      stores suffix instead of suffix code in situs component.
 * 07/30/2007 1.4.22    Reformat SBX_ROLL_DOCDATE for Transfer Date
 * 07/31/2007 1.4.22.1  Adding -Xc option to extract history sale even though
 *                      there is no SBX sale data yet.
 * 03/21/2008 1.5.7.1   Use standard function to update usecode.
 * 06/12/2008 1.6.4.2   Update lLastRecDate in CreateSbxRec() and Sbx_MergeRoll()
 * 07/03/2008 8.0.2     Modify CreateSbxRoll() to fix problem that LDR record may
 *                      consist multiple lines.  This makes fgets() unusable.
 *                      Add function Sbx_UpdCurRoll() to update current change
 *                      to LDR output file.
 * 09/12/2008 8.3.1     Modify Sbx_UpdateAdr() to fix exception caused by "C/O"
 *                      and changing sort filter to skip header row in load update.
 * 12/11/2008 8.5.1     Make StrNum left justified
 * 05/11/2009 8.8       Set full exemption flag
 * 06/28/2009 9.1.0     Create Sbx_ExtrLien() and populate other values in CreateSbxRec()
 * 07/13/2009 9.1.2     Fix bug in Sbx_UpdCurRoll() to avoid duplicate record.
 * 10/01/2009 9.2.3     Fix PERSPROP value by adding PPDECLARED with PPUNIT.
 * 02/23/2010 9.4.3     Fix CARE_OF issue.
 * 07/12/2010 10.1.1    Modify Sbx_MergeOwner() to add a new version that takes both
 *                      Owner1 & 2, one to use with Load_LDR() and one to use with Load_Roll().
 *                      Change DOCDATE format in CreateSbxRec(). Replace myGetStrDC() with 
 *                      myGetStrEQ() in Sbx_Load_LDR(). Modify Sbx_UpdCurRec() to update
 *                      Transfers only. Rename CreateSbxRoll() to Sbx_Load_LDR() and
 *                      MergeSbxRoll() to Sbx_Load_Roll().
 * 12/08/2010 10.3.4    Add Sbx_ExtrProp8() and populate Prop8 when VALREASON='V'.
 * 04/10/2011 10.5.4    Add Sbx_MergeCChr() to merge CHAR from county website.
 *            10.5.4.1  Fix bug in Sbx_MergeCChrRec() to correctly format values.
 * 04/11/2011 10.5.4.2  Use StampAmt to caculate sale1 price.  If Sale1Date==TransferDate,
 *                      copy XferDocNum to Sale1_DocNum.
 * 05/24/2011 10.5.13   Fix bad character in Sbx_MergeOwner().
 * 07/07/2011 11.0.2    Update TRANSFER using roll file when loading LDR regardless of 
 *                      cum sale update.  Adding S_HSENO in Sbx_MergeAdr() with 200 parcels 
 *                      found having hyphen in StrNum.
 * 01/18/2012 11.6.4    Create sale record off of HtmlExtr and update R01 using ApplyCumSaleDN().
 *                      This routine copy TransferDoc to Sale1Doc if they save the same date.
 * 07/05/2012 12.1.1    Replace fgets() with myGetStrEQ() in Sbx_ExtrProp8().  Fix mailzip
 *                      in Sbx_MergeAdr() to capture more info for international mail addr.
 * 01/11/2013 12.4.2    Set MERG_CSAL if there is new sale file.
 * 01/12/2013 12.4.3    Add function Sbx_UpdateSale() to update DOCNUM in sale file using data from roll file.
 * 06/06/2013 12.7.1    Remove Sbx_LoadGrGr() modify Sbx_MergeRoll() not to update AR_CODE1.
 * 07/19/2013 13.0.5    Fix DocDate in Sbx_UpdCurRec().  Modify Sbx_ExtrSale() to rename input
 *                      file only when running regular update, not when LDR.
 * 10/08/2013 13.10.4.2 Use updateVesting() to update Vesting and Etal flag. Add BATH_1Q & BATH_3Q.
 * 10/11/2013 13.11.0   Add BATH_2Q & BATH_4Q.
 * 12/08/2013 13.11.11  If read error, force exit with error in Sbx_Load_Roll().
 * 02/06/2014 13.11.15  Add Sbx_ExtrWebSale(), Sbx_ExtrRollSale(), and Sbx_MergeSaleFiles()
 *                      to create Sbx_Sale.sls
 * 04/13/2014 13.13.2   Fix bug in merge sale. Also fix Sbx_Load_Roll() to fill all records that are not updated.
 * 05/06/2014 13.14.2   Rework on owner name. Rename Sbx_MergeOwner() to Sbx_MergeOwnerX(). Add
 *                      Sbx_MergeOwner1() to perform name processing for monthly update file.
 * 07/08/2014 14.0.3    Modify Sbx_MergeOwner_2() to clean up swapname. Add Sbx_Load_LDR1(), Sbx_ExtrLien1() 
 *                      Sbx_ExtrProp8_1() and Sbx_CreateLdrRec1() to support new LDR input format. 
 *                      Modify Sbx_CreateLienRec() to use in both Sbx_ExtrLien() and Sbx_ExtrLien1().
 *                      Modify loadSbx() to use appropriate loading functions.
 * 10/29/2014 14.8.0    Fix memory violation in Sbx_MergeOwner_1().
 * 06/24/2015 15.0.0    Modify Sbx_MergeOwner_2() to remove quotes from owner name.
 *                      Modify Sbx_CreateLdrRec1() to support LDR 2015 data.  Fix swapname in MergeOwner_2()
 * 08/28/2015 15.1.3    Add -Mr option to add lot sqft & lot area
 * 09/13/2015 15.2.2    Standardize MergeArea.
 * 02/18/2016 15.6.0    Modify Sbx_MergeOwner_1() to remove bad char and fix swap name.
 *                      Modify Sbx_MargeMAdr() to fix overwrite problem.
 * 04/27/2016 15.9.2    Adding BldgSqft, YrBlt, Beds, and Baths
 * 05/02/2016 15.9.3    Add option to load tax data.
 * 06/01/2016 15.9.4    Change CharRec[] buffer size in Sbx_MergeCChr() due to expansion of EX_CHAR
 * 06/21/2016 15.9.8    Modify SORT command to dedup input file before calling Load_Roll()
 * 06/25/2016 16.0.0    Fix Name2 in Sbx_MergeOwner_2() and situs in Sbx_MergeAdr(). 
 *                      Add new functions Sbx_ExtrLien2(), Sbx_CreateLdrRec2(), Sbx_Load_LDR2(), 
 *                      Sbx_CreateLienRec2() to support new SBX layout.
 * 06/27/2016 16.0.1    Remove extra "PO BOX" in Sbx_MergeAdr() and Sbx_UpdateAdr(). Modify Sbx_CreateLdrRec2()
 *                      to set USE_STD="999" when UseCode is empty. Remove duplicate APN in Sbx_Load_LDR2().
 * 07/11/2018 18.1.2    If web sale file is not available, skip it.
 * 08/14/2018 18.4.0    Replace R01.MergeArea() with PQ_MergeLotArea() using new SBX_Basemap.txt
 * 09/29/2018 18.5.1    Move -Mr option to LoadOne.cpp
 * 10/16/2018 18.5.5    Add Sbx_Load_TaxBase() & Sbx_Load_TaxDetail() to load new tax files
 * 12/10/2018 18.6.10   Fix bug in Sbx_MergeRoll() & Sbx_Load_Roll to make sure input record
 *                      is not broken.  Also call RebuildCsv() before loading update roll.
 * 01/18/2019 18.8.4    Add HSENO to Sbx_UpdateAdr() for bulk extract.
 * 07/24/2019 19.0.5    Modify loadSbx() to rebuild lien file to fix broken record before processing.
 *                      Remove code that used Ker_Roll.txt to update current value after Loading LDR.
 * 04/27/2020 19.9.0    Remove -Ut and use -T to load both full & partial tax file with TC_LoadTax().
 * 07/02/2020 20.1.0    Modify Sbx_CreateLienRec2() & Sbx_CreateLdrRec2() to add EXE_CD1.
 *                      Fix special case of DocNum in Sbx_CreateLdrRec2()
 * 07/07/2020 20.1.2    Fix PO BOX issue of situs in Sbx_UpdateAdr().
 * 05/07/2021 20.7.18   Set FirePlace='M' when greater than 9 in Sbx_MergeCChrRec().
 * 07/21/2021 21.0.0    Modify Sbx_MergeOwner_2() to replace '\' with " & ".
 * 12/06/2021 21.4.4    Remove known bad char in owner name in Sbx_MergeOwner_2().
 *                      Force reformat MapLink for page 097\100.
 * 11/04/2022 22.3.1    Add functions to process supplemental & redemption files.
 *                      Modify Sbx_ParseTaxBase() & Sbx_ParseTaxDetail() to skip records with TaxAmt<=0.
 * 10/12/2023 23.3.3    Modify Sbx_ParseTaxSupp() to change how to create TotalDue.
 *                      Modify Sbx_Load_TaxDelq() to change sort command.
 * 02/02/2024 23.6.0    Modify Sbd_UpdateAdr() to populate UnitNox.
 * 04/17/2024 24.0.5    Modify Sbx_CreateLdrRec2() to ad ExeType.  Define LdrSep in INI file.
 * 10/09/2024 24.1.6    Modify Sbx_ParseTaxDetail() to log number of new agencies.
 *                      Modify Sbx_ParseTaxSupp() to add AdditionalPenalty to PenAmt.
 *                      Modify Sbx_Load_TaxBase() to rebuild broken CSV file and sort it.
 *                      Modify Sbx_Load_TaxDetail() and add Sbx_Load_TaxSuppDetail() to provide detail supplemental tax bill.
 * 01/31/2025 24.4.9.1  Modify Sbx_ParseTaxBase(), Sbx_ParseTaxSupp(), Sbx_ParseTaxDetail() to filter out prior year bill.
 *                      These bills some time are paid but InstallCode not changed.
 *
 ***************************************************************************/

#include "stdafx.h"
#include "Prodlib.h"
#include "CountyInfo.h"
#include "R01.h"
#include "RecDef.h"
#include "Tables.h"
#include "SaleRec.h"
#include "Logs.h"
#include "LoadOne.h"
#include "Utils.h"
#include "XlatTbls.h"
#include "doSort.h"
#include "doOwner.h"
#include "formatApn.h"
#include "MergeSbx.h"
#include "UseCode.h"
#include "FldDef.h"     // Field definition for web extract
#include "LOExtrn.h"
#include "Tax.h"
#include "PQ.h"

#define SBX_GRGR_COLS      4
#define SBX_GRGR_DOCNUM    0
#define SBX_GRGR_TITLE     1
#define SBX_GRGR_DOCDATE   2
#define SBX_GRGR_NAME      3

static int  iNameLen;
static char acLongestName[256];

/******************************** Sbx_MergeOwner *****************************
 *
 * 001311018,KASS, DAVID C, STEPHEN M & MICHELLE D LIVING TRUST 6/28/06 -> ok
 * 001311015,JAMESON SIMON/ROSE MARIE TRUSTEES (for) JAMESON FAMILTY TRUST 12-6-90 -> break to 2 names at (for)
 ~ 001300046,STEIN, J BRADLEY & CARLA V FAMILY TRUST -> STEIN J BRADLEY & CARLA V FAMILY TRUST
 * 001311022,GODFREY, ROBERT JAMES & FRANCES TIP FAMILY LIVING TRUST 3/21/06 -> ok
 * 001311036,KIMBERLIN, JOHN F/SHIRLEY J FAMILY LIVING TRUST 3/9/06 -> ok
 * 067090039,POLYCOMP TRUST CO CUSTODIAN FBO JOHN CLEYMAET IRA PCJ1200 -> ok
 *
 * 001311010,DECEDENT'S TRUST OF THE BROWN FAMILY TRUST 9/18/1986 -> DECEDENT'S TRUST/BROWN FAMILY TRUST 9/18/1986
 * 009060026,MEYER, GARY W/GERDA M TTEES OF THE MEYER FAM TR 5/12/99 -> MEYER GARY W & GERDA M TTEES/MEYER FAM TR 5/12/99
 * 005420026,BOYD, DEBORAH JULIA TTEE OF THE BOYD FAMILY 1994 REV TR 2/4/94 -> BOYD DEBORAH JULIA TTEE/BOYD FAMILY 1994 REV TR 2/4/94
 * 004017022,LOS ANGELES DISTRICT ADVISORY BOARD, CHURCH OF THE NAZARENE -> as is
 * 004017021,LOS ANGELES DISTRICT CHURCH OF THE NAZARENE ADVISORY BOARD -> as is
 * 083690019,BARBER, MARK TTEE OF THE ROBERT MARK BARBER FAM TR 7/8/03 -> BARBER MARK TTEE/ROBERT MARK BARBER FAM TR 7/8/03
 * 001230012,SMEAD, ANN BECHER QUALIFIED PERSONAL RESIDENCE TRUST FBO JOHN  BECHER 9/30/07
 *
 * 031530006,KANE FAMILY REVOCABLE LIVING TRUST 11/24/1995 & AMENDED 4/19/2012
 ~ 071153006,ALVIN LEE REECE & BETTY LOU REECE AMENDED REV TRUST TR/D 3/22/96, BYPASS TRUST
 *
 * Return 0 if successful, >0 if error
 *
 *****************************************************************************/

void Sbx_MergeOwner_1(char *pOutbuf, char *pNames)
{
   int   iTmp, iLen;
   char  acTmp[256], acTmp1[64], acSave[128];
   char  acName1[128], acName2[128];
   char  *pTmp, *pTmp1;

   OWNER myOwner;

   // Clear output buffer if needed
   removeNames(pOutbuf, false);
   if (*pNames == '/')
      pNames++;
   strcpy(acTmp, pNames);
   pTmp = _strupr(acTmp);

   // Update vesting
   updateVesting(myCounty.acCntyCode, acTmp, pOutbuf+OFF_VEST, pOutbuf+OFF_ETAL_FLG);

   // Check for declaration
   iLen = blankRem(acTmp, SIZ_NAME1);
   if (!memcmp(acTmp, "DECLARATION OF", 14))
   {
      vmemcpy(pOutbuf+OFF_NAME1, acTmp, SIZ_NAME1, iLen);
      vmemcpy(pOutbuf+OFF_NAME_SWAP, acTmp, SIZ_NAME1, iLen);
      return;
   }
   acName2[0] = 0;

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "003700029", 9))
   //   iTmp = 0;
#endif

   // Remove multiple spaces
   remCharEx(acTmp, ".,");
   if (iLen > iNameLen)
   {
      iNameLen = iLen;
      strcpy(acLongestName, acTmp);
   }

   // If there is "(for)", save it and append later after "/"
   if (pTmp=strstr(acTmp, " (FOR) "))
   {
      *pTmp = 0;
      strcpy(acName2, pTmp+7);
      strcpy(acName1, acTmp);
   } else if (pTmp=strstr(acTmp, " FOR "))
   {
      if (iLen > SIZ_NAME1 && (*(pOutbuf+OFF_VEST) != 'C' && *(pOutbuf+OFF_VEST) != 'G' && *(pOutbuf+OFF_VEST) != ' ')  )
      {
         *pTmp = 0;
         strcpy(acName2, pTmp+5);
      }
      strcpy(acName1, acTmp);
   } else if (pTmp=strstr(acTmp, " OF THE "))
   {  // If name is too long and not a church, break it into 2 names
      if (iLen > SIZ_NAME1 && (*(pOutbuf+OFF_VEST) != 'C' && *(pOutbuf+OFF_VEST) != 'G' && *(pOutbuf+OFF_VEST) != ' ')  )
      {
         *pTmp = 0;
         strcpy(acName2, pTmp+8);
      }
      strcpy(acName1, acTmp);
   } else if (pTmp=strstr(acTmp, " AMENDED "))
   {
      if (*(pTmp-1) == '&')
      {
         pTmp--;
         iLen--;
      }
      if (iLen > SIZ_NAME1)
         *pTmp = 0;
      strcpy(acName1, acTmp);
   } else if (pTmp=strstr(acTmp, " QUALIFIED"))
   {
      *pTmp = 0;
      strcpy(acName2, pTmp+11);
      strcpy(acName1, acTmp);
   } else
   {
      strcpy(acName1, acTmp);
      acName2[0] = 0;
   }

   // Drop everything from these words
   if (pTmp=strstr(acTmp, " AKA "))
      *pTmp = 0;
   else if (pTmp=strstr(acTmp, " TRUSTE"))
      *pTmp = 0;
   else if (pTmp=strstr(acTmp, " TTEE"))
      *pTmp = 0;
   else if (pTmp=strstr(acTmp, " TRSTEE"))
      *pTmp = 0;
   else if (pTmp=strstr(acTmp, " ETAL "))
      *pTmp = 0;
   else if (pTmp=strstr(acTmp, " ET AL"))
      *pTmp = 0;
   else if (pTmp=strstr(acTmp, " MD TR"))
      *pTmp = 0;
   else if (pTmp=strstr(acTmp, " CO-TR"))
      *pTmp = 0;
   else if (pTmp=strstr(acTmp, " INTERVIVOS "))
      *pTmp = 0;
   else if (pTmp=strstr(acTmp, " EST OF"))
      *pTmp = 0;

   // Save data to append later
   if (pTmp1=strstr(&acTmp[2], "TRUST"))
   {
      if ((pTmp=strstr(acTmp, " FAM"))   ||
          (pTmp=strstr(acTmp, " REVOC")) ||
          (pTmp=strstr(acTmp, " INDIV")) ||
          (pTmp=strstr(acTmp, " LIV"))   ||
          (pTmp=strstr(acTmp, " AMENDED ")) )
      {
         strcpy(acSave, pTmp);
         *pTmp = 0;
      } else
      {
         strcpy(acSave, --pTmp1);
         *pTmp1 = 0;
      }
   } else if ((pTmp=strstr(acTmp, " FAM TR"))    ||
              (pTmp=strstr(acTmp, " FAMILY TR")) ||
              (pTmp=strstr(acTmp, " LIV TR"))    ||
              (pTmp=strstr(acTmp, " LIVING TR")) ||
              (pTmp=strstr(acTmp, " REV TR"))    ||
              (pTmp=strstr(acTmp, " REVOC TR"))  ||
              (pTmp=strstr(acTmp, " PERSONAL TR")) )
   {
      strcpy(acSave, pTmp);
      *pTmp = 0;
   } else
      acSave[0] = 0;

   // Check for year that goes before TRUST
   iTmp = 0;
   while (isdigit(acTmp[iTmp]))
      iTmp++;
   while (!isdigit(acTmp[iTmp]) && (char *)&acTmp[iTmp] < pTmp)
      iTmp++;
   if ((char *)&acTmp[iTmp] < pTmp)
   {
      strcpy(acTmp1, (char *)&acTmp[--iTmp]);      // Copy the preceeding space too
      strcat(acTmp1, acSave);
      strcpy(acSave, acTmp1);
      acTmp[iTmp] = 0;
   }

   // Translate "/" to "&"
   pTmp = strchr(acTmp, '/');
   if (pTmp && !isdigit(*(pTmp+1)))
      *pTmp = '&';

   // Save owner names
   vmemcpy(pOutbuf+OFF_NAME1, acName1, SIZ_NAME1);
   if (acName2[0] && strcmp(acName1, acName2))
      vmemcpy(pOutbuf+OFF_NAME2, acName2, SIZ_NAME2);

   // Now parse owners
   acTmp[63] = 0;
   splitOwner(acTmp, &myOwner, 0);

   // If single word, use Name1
   if (!strchr(myOwner.acSwapName, ' ') || *(pOutbuf+OFF_VEST) > ' ')
      vmemcpy(pOutbuf+OFF_NAME_SWAP, acName1, SIZ_NAME_SWAP);
   else
      vmemcpy(pOutbuf+OFF_NAME_SWAP, myOwner.acSwapName, SIZ_NAME_SWAP);
}

void Sbx_MergeOwner_2(char *pOutbuf, char *pName1, char *pName2)
{
   char  acTmp[128], acOwner1[128], acOwner2[128];
   char  acName2[64];
   char  *pTmp, *pTmp1;
   int   iTmp;

   OWNER myOwner;

   // Clear output buffer if needed
   removeNames(pOutbuf, false);
   strcpy(acOwner1, pName1);
   pTmp = _strupr(acOwner1);
   // Replace known char
   replChar(acOwner1, 0xC9, 'E');

   // Update vesting
   iTmp = updateVesting(myCounty.acCntyCode, acOwner1, pOutbuf+OFF_VEST, pOutbuf+OFF_ETAL_FLG);

   // Check for declaration
   if (!memcmp(pName1, "DECLARATION", 11))
   {
      //strncpy(acOwner1, pName1, SIZ_NAME1);
      //acOwner1[SIZ_NAME1] = 0;
      vmemcpy(pOutbuf+OFF_NAME1, acOwner1, SIZ_NAME1);
      vmemcpy(pOutbuf+OFF_NAME_SWAP, acOwner1, SIZ_NAME_SWAP);
      return;
   }

   // Remove multiple spaces
   //pTmp = strcpy(acOwner1, pName1);
   iTmp = 0;
   while (*pTmp)
   {
      // skip unwanted chars at beginning of name
      if (*pTmp < '1' && iTmp == 0)
      {
         pTmp++;
         continue;
      }

      acTmp[iTmp++] = *pTmp;
      // Remove multi-space
      if (*pTmp == ' ')
         while (*pTmp == ' ') pTmp++;
      else
         pTmp++;

      // Remove '.' too
      if (*pTmp == '.' || *pTmp == 34 || *pTmp == 39)
         pTmp++;
   }
   if (acTmp[iTmp-1] == ' ')
      iTmp -= 1;
   acTmp[iTmp] = 0;
   //pTmp = _strupr(acTmp);

   // VANWINGERDEN FAMILY TRUST
#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "005460045", 9))
   //   iTmp = 0;
#endif

   // Replace special char
   iTmp = replStr(acTmp, "\\", " & ");

   // If there is "(for)", save it for Name2 "/"
   if (pTmp=strstr(acTmp, " (FOR) "))
   {
      strcpy(acName2, pTmp+7);
      *pTmp = 0;
   } else if (pTmp=strstr(acTmp, " CUSTODIAN FOR"))
   {
      strcpy(acName2, pTmp+15);
      *pTmp = 0;
   } else if (pTmp=strstr(acTmp, " OF THE "))
   {
      strcpy(acName2, pTmp+8);
      *pTmp = 0;
   } else if (pTmp=strstr(acTmp, " QUALIFIED"))
   {
      strcpy(acName2, pTmp+11);
      *pTmp = 0;
   } else
      acName2[0] = 0;

   // Save owners
   strcpy(acOwner1, acTmp);
   if (pName2 && *pName2)
      strcpy(acOwner2, pName2);
   else if (acName2[0] > '0')
      strcpy(acOwner2, acName2);
   else
      acOwner2[0] = 0;
   quoteRem(acOwner2);

   // Drop everything from these words
   if (pTmp=strstr(acTmp, " AKA "))
      *pTmp = 0;
   else if (pTmp=strstr(acTmp, " TRUSTE"))
      *pTmp = 0;
   else if (pTmp=strstr(acTmp, " TTEE"))
      *pTmp = 0;
   else if (pTmp=strstr(acTmp, " TRSTEE"))
      *pTmp = 0;
   else if (pTmp=strstr(acTmp, " ETAL "))
      *pTmp = 0;
   else if (pTmp=strstr(acTmp, " ET AL"))
      *pTmp = 0;
   //else if (pTmp=strstr(acTmp, " DATED "))
   //   *pTmp = 0;
   else if (pTmp=strstr(acTmp, " MD TR"))
      *pTmp = 0;
   else if (pTmp=strstr(acTmp, " CO-TR"))
      *pTmp = 0;
   else if (pTmp=strstr(acTmp, " INTERVIVOS "))
      *pTmp = 0;
   else if (pTmp=strstr(acTmp, " EST OF"))
      *pTmp = 0;
   else if (pTmp=strstr(acTmp, " SEPARATE PROPERTY"))
      *pTmp = 0;
   else if (pTmp=strstr(acTmp, " DECLARATION OF"))
      *pTmp = 0;
   else if (pTmp=strstr(acTmp, " SURVIVING TR"))
      *pTmp = 0;

   // Save data to append later
   if (pTmp1=strstr(&acTmp[2], "TRUST"))
   {
      if (pTmp=strstr(acTmp, " FAM"))
      {
         *pTmp = 0;
      } else if (pTmp=strstr(acTmp, " REVOC"))
      {
         *pTmp = 0;
      } else if (pTmp=strstr(acTmp, " INDIV"))
      {
         *pTmp = 0;
      } else if (pTmp=strstr(acTmp, " LIV"))
      {
         *pTmp = 0;
      } else if (pTmp=strstr(acTmp, " AMENDED "))
      {
         *pTmp = 0;
      } else
      {
         *pTmp1 = 0;
      }
   } else if (pTmp=strstr(acTmp, " FAM TR"))
   {
      *pTmp = 0;
   } else if (pTmp=strstr(acTmp, " FAMILY TR"))
   {
      *pTmp = 0;
   } else if (pTmp=strstr(acTmp, " LIV TR"))
   {
      *pTmp = 0;
   } else if (pTmp=strstr(acTmp, " LIVING TR"))
   {
      *pTmp = 0;
   } else if (pTmp=strstr(acTmp, " REV TR"))
   {
      *pTmp = 0;
   } else if (pTmp=strstr(acTmp, " REVOC TR"))
   {
      *pTmp = 0;
   } else if (pTmp=strstr(acTmp, " PERSONAL TR"))
   {
      *pTmp = 0;
   } 

   // Translate "/" to "&"
   pTmp = strchr(acTmp, '/');
   if (pTmp && !isdigit(*(pTmp+1)))
      *pTmp = '&';

   // Save Owner1 as is
   iTmp = remChar(acOwner1, ',');
   vmemcpy(pOutbuf+OFF_NAME1, acOwner1, SIZ_NAME1);

   // Now parse owners
   acTmp[63] = 0;
   iTmp = splitOwner(acTmp, &myOwner, 0);
   if (iTmp == -1)
      strcpy(myOwner.acSwapName, acOwner1);

   if (acOwner2[0] > ' ')
   {
      iTmp = remChar(acOwner2, ',');
      vmemcpy(pOutbuf+OFF_NAME2, acOwner2, SIZ_NAME2);
   //} else if (myOwner.acName2[0] > ' ' && strcmp(myOwner.acName1, myOwner.acName2))
   //{
   //   vmemcpy(pOutbuf+OFF_NAME2, myOwner.acName2, SIZ_NAME2);
   }

   iTmp = strlen(myOwner.acSwapName);
   if (myOwner.acSwapName[iTmp-1] < 'A' || myOwner.acSwapName[0] < 'A')
      memcpy(pOutbuf+OFF_NAME_SWAP, pOutbuf+OFF_NAME1, SIZ_NAME1);
   else
   {
      if (myOwner.acSwapName[iTmp-1] < '0')
         myOwner.acSwapName[--iTmp] = 0;
      vmemcpy(pOutbuf+OFF_NAME_SWAP, myOwner.acSwapName, SIZ_NAME_SWAP);
   }
}

// This function is used for SCI name xref
void Sbx_MergeOwnerX(char *pOutbuf, char *pNames)
{
   int   iTmp, iTmp1;
   char  acOwners[128], acTmp[128], acTmp1[64], acSave[64];
   char  acName2[80];
   char  *pTmp, *pTmp1;

   OWNER myOwner;

   // Clear output buffer if needed
   removeNames(pOutbuf, false);
   strcpy(acTmp, pNames);
   pTmp = _strupr(acTmp);

   // Update vesting
   updateVesting(myCounty.acCntyCode, acTmp, pOutbuf+OFF_VEST, pOutbuf+OFF_ETAL_FLG);

   // Check for declaration
   if (!memcmp(acTmp, "DECLARATION OF", 14))
   {
      strncpy(acOwners, acTmp, SIZ_NAME1);
      iTmp = blankRem(acOwners, SIZ_NAME1);
      memcpy(pOutbuf+OFF_NAME1, acOwners, iTmp);
      memcpy(pOutbuf+OFF_NAME_SWAP, acOwners, iTmp);
      return;
   }

   // Remove multiple spaces
   remCharEx(acTmp, ".,");
   iTmp = blankRem(acTmp, SIZ_NAME1);

   // If there is "(for)", save it and append later after "/"
   if (pTmp=strstr(acTmp, " (FOR) "))
   {
      strcpy(acName2, pTmp+7);
      *pTmp = 0;
   } else if (pTmp=strstr(acTmp, " CUSTODIAN FOR"))
   {
      strcpy(acName2, pTmp+15);
      *pTmp = 0;
   } else if (pTmp=strstr(acTmp, " OF THE "))
   {
      strcpy(acName2, pTmp+8);
      *pTmp = 0;
   } else if (pTmp=strstr(acTmp, " QUALIFIED"))
   {
      strcpy(acName2, pTmp+11);
      *pTmp = 0;
   } else
      acName2[0] = 0;

   // Drop everything from these words
   if (pTmp=strstr(acTmp, " AKA "))
      *pTmp = 0;
   else if (pTmp=strstr(acTmp, " TRUSTE"))
      *pTmp = 0;
   else if (pTmp=strstr(acTmp, " TTEE"))
      *pTmp = 0;
   else if (pTmp=strstr(acTmp, " TRSTEE"))
      *pTmp = 0;
   else if (pTmp=strstr(acTmp, " ETAL "))
      *pTmp = 0;
   else if (pTmp=strstr(acTmp, " ET AL"))
      *pTmp = 0;
   //else if (pTmp=strstr(acTmp, " DATED "))
   //   *pTmp = 0;
   else if (pTmp=strstr(acTmp, " MD TR"))
      *pTmp = 0;
   else if (pTmp=strstr(acTmp, " CO-TR"))
      *pTmp = 0;
   else if (pTmp=strstr(acTmp, " INTERVIVOS "))
      *pTmp = 0;
   else if (pTmp=strstr(acTmp, " EST OF"))
      *pTmp = 0;

   // Save data to append later
   if (pTmp1=strstr(&acTmp[2], "TRUST"))
   {
      if (pTmp=strstr(acTmp, " FAM"))
      {
         strcpy(acSave, pTmp);
         *pTmp = 0;
      } else if (pTmp=strstr(acTmp, " REVOC"))
      {
         strcpy(acSave, pTmp);
         *pTmp = 0;
      } else if (pTmp=strstr(acTmp, " INDIV"))
      {
         strcpy(acSave, pTmp);
         *pTmp = 0;
      } else if (pTmp=strstr(acTmp, " LIV"))
      {
         strcpy(acSave, pTmp);
         *pTmp = 0;
      } else if (pTmp=strstr(acTmp, " AMENDED "))
      {
         strcpy(acSave, pTmp);
         *pTmp = 0;
      } else
      {
         strcpy(acSave, --pTmp1);
         *pTmp1 = 0;
      }
   } else if (pTmp=strstr(acTmp, " FAM TR"))
   {
      strcpy(acSave, pTmp);
      *pTmp = 0;
   } else if (pTmp=strstr(acTmp, " FAMILY TR"))
   {
      strcpy(acSave, pTmp);
      *pTmp = 0;
   } else if (pTmp=strstr(acTmp, " LIV TR"))
   {
      strcpy(acSave, pTmp);
      *pTmp = 0;
   } else if (pTmp=strstr(acTmp, " LIVING TR"))
   {
      strcpy(acSave, pTmp);
      *pTmp = 0;
   } else if (pTmp=strstr(acTmp, " REV TR"))
   {
      strcpy(acSave, pTmp);
      *pTmp = 0;
   } else if (pTmp=strstr(acTmp, " REVOC TR"))
   {
      strcpy(acSave, pTmp);
      *pTmp = 0;
   } else if (pTmp=strstr(acTmp, " PERSONAL TR"))
   {
      strcpy(acSave, pTmp);
      *pTmp = 0;
   } else
      acSave[0] = 0;

   // Check for year that goes before TRUST
   iTmp = 0;
   while (isdigit(acTmp[iTmp]))
      iTmp++;
   while (!isdigit(acTmp[iTmp]) && (char *)&acTmp[iTmp] < pTmp)
      iTmp++;
   if ((char *)&acTmp[iTmp] < pTmp)
   {
      strcpy(acTmp1, (char *)&acTmp[--iTmp]);      // Copy the preceeding space too
      strcat(acTmp1, acSave);
      strcpy(acSave, acTmp1);
      acTmp[iTmp] = 0;
   }

   // Translate "/" to "&"
   pTmp = strchr(acTmp, '/');
   if (pTmp && !isdigit(*(pTmp+1)))
      *pTmp = '&';

   // Now parse owners
   acTmp[63] = 0;
   splitOwner(acTmp, &myOwner, 0);
   if (acSave[0])
   {
      strcat(myOwner.acName1, acSave);
      blankRem(myOwner.acName1);
   }

   if (acName2[0])
   {
      // Translate "/" to "&"
      pTmp = strchr(acName2, '/');
      if (pTmp && !isdigit(*(pTmp+1)))
         *pTmp = '&';

      iTmp = strlen(myOwner.acName1);
      myOwner.acName1[iTmp++] = '/';
      myOwner.acName1[iTmp] = 0;
      iTmp1 = strlen(acName2);
      if (iTmp+iTmp1 < SIZ_NAME1)
         strcat(myOwner.acName1, acName2);
      else
      {
         acName2[SIZ_NAME1-iTmp] = 0;
         strcat(myOwner.acName1, acName2);
      }
   }

   iTmp = strlen(myOwner.acName1);
   if (iTmp > SIZ_NAME1)
      iTmp = SIZ_NAME1;
   memcpy(pOutbuf+OFF_NAME1, myOwner.acName1, iTmp);

   if (myOwner.acName2[0] && strcmp(myOwner.acName1, myOwner.acName2))
   {
      iTmp = strlen(myOwner.acName2);
      if (iTmp > SIZ_NAME2)
         iTmp = SIZ_NAME2;
      memcpy(pOutbuf+OFF_NAME2, myOwner.acName2, iTmp);
   }

   // If single word, use Name1
   if (!strchr(myOwner.acSwapName, ' '))
      strcpy(myOwner.acSwapName, myOwner.acName1);

   iTmp = strlen(myOwner.acSwapName);
   if (iTmp > SIZ_NAME_SWAP)
      iTmp = SIZ_NAME_SWAP;
   memcpy(pOutbuf+OFF_NAME_SWAP, myOwner.acSwapName, iTmp);
}

void Sbx_MergeOwnerX(char *pOutbuf, char *pName1, char *pName2)
{
   char  acTmp[128], acOwner1[128], acOwner2[128];
   char  acName2[64];
   char  *pTmp, *pTmp1;
   int   iTmp;

   OWNER myOwner;

   // Clear output buffer if needed
   removeNames(pOutbuf, false);
   pTmp = strcpy(acTmp, pName1);

   // Update vesting
   iTmp = updateVesting(myCounty.acCntyCode, acTmp, pOutbuf+OFF_VEST, pOutbuf+OFF_ETAL_FLG);

   // Check for declaration
   if (!memcmp(pName1, "DECLARATION", 11))
   {
      strncpy(acOwner1, pName1, SIZ_NAME1);
      acOwner1[SIZ_NAME1] = 0;
      memcpy(pOutbuf+OFF_NAME1, acOwner1, strlen(acOwner1));
      return;
   }

   // Remove multiple spaces
   pTmp = strcpy(acOwner1, pName1);
   iTmp = 0;
   while (*pTmp)
   {
      // skip unwanted chars at beginning of name
      if (*pTmp < '1' && iTmp == 0)
      {
         pTmp++;
         continue;
      }

      acTmp[iTmp++] = *pTmp;
      // Remove multi-space
      if (*pTmp == ' ')
         while (*pTmp == ' ') pTmp++;
      else
         pTmp++;

      // Remove '.' too
      if (*pTmp == '.')
         pTmp++;
   }
   if (acTmp[iTmp-1] == ' ')
      iTmp -= 1;
   acTmp[iTmp] = 0;
   
   // If there is "(for)", save it and append later after "/"
   if (pTmp=strstr(acTmp, " (for) "))
   {
      strcpy(acName2, pTmp+7);
      *pTmp = 0;
   } else if (pTmp=strstr(acTmp, " (FOR) "))
   {
      strcpy(acName2, pTmp+7);
      *pTmp = 0;
   } else if (pTmp=strstr(acTmp, " CUSTODIAN FOR"))
   {
      strcpy(acName2, pTmp+15);
      *pTmp = 0;
   } else if (pTmp=strstr(acTmp, " OF THE "))
   {
      strcpy(acName2, pTmp+8);
      *pTmp = 0;
   } else if (pTmp=strstr(acTmp, " QUALIFIED"))
   {
      strcpy(acName2, pTmp+11);
      *pTmp = 0;
   } else
      acName2[0] = 0;

   // Save owners
   strcpy(acOwner1, acTmp);
   strcpy(acOwner2, pName2);

   // Drop everything from these words
   if (pTmp=strstr(acTmp, " AKA "))
      *pTmp = 0;
   else if (pTmp=strstr(acTmp, " TRUSTE"))
      *pTmp = 0;
   else if (pTmp=strstr(acTmp, " TTEE"))
      *pTmp = 0;
   else if (pTmp=strstr(acTmp, " TRSTEE"))
      *pTmp = 0;
   else if (pTmp=strstr(acTmp, " ETAL "))
      *pTmp = 0;
   else if (pTmp=strstr(acTmp, " ET AL"))
      *pTmp = 0;
   //else if (pTmp=strstr(acTmp, " DATED "))
   //   *pTmp = 0;
   else if (pTmp=strstr(acTmp, " MD TR"))
      *pTmp = 0;
   else if (pTmp=strstr(acTmp, " CO-TR"))
      *pTmp = 0;
   else if (pTmp=strstr(acTmp, " INTERVIVOS "))
      *pTmp = 0;
   else if (pTmp=strstr(acTmp, " EST OF"))
      *pTmp = 0;

   // Save data to append later
   if (pTmp1=strstr(&acTmp[2], "TRUST"))
   {
      if (pTmp=strstr(acTmp, " FAM"))
      {
         *pTmp = 0;
      } else if (pTmp=strstr(acTmp, " REVOC"))
      {
         *pTmp = 0;
      } else if (pTmp=strstr(acTmp, " INDIV"))
      {
         *pTmp = 0;
      } else if (pTmp=strstr(acTmp, " LIV"))
      {
         *pTmp = 0;
      } else if (pTmp=strstr(acTmp, " AMENDED "))
      {
         *pTmp = 0;
      } else
      {
         *pTmp1 = 0;
      }
   } else if (pTmp=strstr(acTmp, " FAM TR"))
   {
      *pTmp = 0;
   } else if (pTmp=strstr(acTmp, " FAMILY TR"))
   {
      *pTmp = 0;
   } else if (pTmp=strstr(acTmp, " LIV TR"))
   {
      *pTmp = 0;
   } else if (pTmp=strstr(acTmp, " LIVING TR"))
   {
      *pTmp = 0;
   } else if (pTmp=strstr(acTmp, " REV TR"))
   {
      *pTmp = 0;
   } else if (pTmp=strstr(acTmp, " REVOC TR"))
   {
      *pTmp = 0;
   } else if (pTmp=strstr(acTmp, " PERSONAL TR"))
   {
      *pTmp = 0;
   } 

   // Translate "/" to "&"
   pTmp = strchr(acTmp, '/');
   if (pTmp && !isdigit(*(pTmp+1)))
      *pTmp = '&';

   // Now parse owners
   acTmp[63] = 0;
   splitOwner(acTmp, &myOwner, 0);

   iTmp = remChar(acOwner1, ',');
   if (iTmp > SIZ_NAME1)
      iTmp = SIZ_NAME1;
   memcpy(pOutbuf+OFF_NAME1, acOwner1, iTmp);

   if (acOwner2[0] > ' ')
   {
      iTmp = remChar(acOwner2, ',');
      if (iTmp > SIZ_NAME2)
         iTmp = SIZ_NAME2;
      memcpy(pOutbuf+OFF_NAME2, acOwner2, iTmp);
   } else if (myOwner.acName2[0] > ' ' && strcmp(myOwner.acName1, myOwner.acName2))
   {
      iTmp = strlen(myOwner.acName2);
      if (iTmp > SIZ_NAME2)
         iTmp = SIZ_NAME2;
      memcpy(pOutbuf+OFF_NAME2, myOwner.acName2, iTmp);
   }

   iTmp = strlen(myOwner.acSwapName);
   if (iTmp > SIZ_NAME_SWAP)
      iTmp = SIZ_NAME_SWAP;
   memcpy(pOutbuf+OFF_NAME_SWAP, myOwner.acSwapName, iTmp);
}

/********************************* Sbx_MergeAdr ******************************
 *
 * Return 0 if successful, >0 if error
 *
 *****************************************************************************/

void Sbx_MergeAdr(char *pOutbuf)
{
   char     acTmp[256], acAddr1[64], acAddr2[64], *pTmp;
   int      iTmp, iStrNo;
   ADR_REC  sMailAdr;
   ADR_REC  sSitusAdr;

   // Clear old Mailing
   removeMailing(pOutbuf, true);

   memset((void *)&sMailAdr, 0, sizeof(ADR_REC));
   memset((void *)&sSitusAdr, 0, sizeof(ADR_REC));

   // Check for C/O
   if (!memcmp(apTokens[SBX_ROLL_ADDRESS1], "CO ", 3))
      pTmp = apTokens[SBX_ROLL_ADDRESS1]+3;
   else if (!memcmp(apTokens[SBX_ROLL_ADDRESS1], "C/O ", 4))
      pTmp = apTokens[SBX_ROLL_ADDRESS1]+4;
   else if (!memcmp(apTokens[SBX_ROLL_ADDRESS2], "CO ", 3))
      pTmp = apTokens[SBX_ROLL_ADDRESS2]+3;
   else if (!memcmp(apTokens[SBX_ROLL_ADDRESS2], "C/O ", 4))
      pTmp = apTokens[SBX_ROLL_ADDRESS2]+4;
   else
      pTmp = NULL;

   if (pTmp)
   {
      iTmp = sprintf(acTmp, "C/O %s", pTmp);
      updateCareOf(pOutbuf, acTmp, iTmp);
   }

   // Mail address
   acAddr1[0] = 0;
   acAddr2[0] = 0;
#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "083310018", 9))
   //   iTmp = 0;
#endif

   iStrNo = atoi(apTokens[SBX_ROLL_M_STRNUM]);
   if (iStrNo > 0)
   {
      // Parse strname to pick up suffix
      if (*apTokens[SBX_ROLL_M_STRSFX] > '0')
      {
         strcpy(sMailAdr.strSfx, apTokens[SBX_ROLL_M_STRSFX]);
         strcpy(sMailAdr.strName, apTokens[SBX_ROLL_M_STRNAME]);
      } else
      {
         parseAdr1N(&sMailAdr, apTokens[SBX_ROLL_M_STRNAME]);
      }

      strcpy(sMailAdr.strNum, apTokens[SBX_ROLL_M_STRNUM]);
      if (*apTokens[SBX_ROLL_M_STRSUB] > '0')
         strcpy(sMailAdr.strSub, apTokens[SBX_ROLL_M_STRSUB]);
      if (*apTokens[SBX_ROLL_M_STRDIR] > '0')
         strcpy(sMailAdr.strDir, apTokens[SBX_ROLL_M_STRDIR]);

      // Get Unit
      if (*apTokens[SBX_ROLL_M_UNIT] > '0')
         strcpy(sMailAdr.Unit, apTokens[SBX_ROLL_M_UNIT]);

   } else if (*apTokens[SBX_ROLL_POBOX] > '0')
   {
      _strupr(apTokens[SBX_ROLL_POBOX]);
      if (*apTokens[SBX_ROLL_POBOX] == 'P')
         strcpy(sMailAdr.strName, apTokens[SBX_ROLL_POBOX]);
      else
         sprintf(sMailAdr.strName, "PO BOX %s", apTokens[SBX_ROLL_POBOX]);
   } else if (*apTokens[SBX_ROLL_M_STRNAME] >= 'A')
   {  // In most case this is PO BOX
      strcpy(sMailAdr.strName, apTokens[SBX_ROLL_M_STRNAME]);
      //strcpy(acAddr1, sMailAdr.strName);
   } else if (*apTokens[SBX_ROLL_M_STRNAME] >= '0')
   {  // Full address may be keyed in this field
      parseAdr1_1(&sMailAdr, apTokens[SBX_ROLL_M_STRNAME]);
      iStrNo = sMailAdr.lStrNum;
   }

   if (iStrNo)
   {
      iTmp = sprintf(acAddr1, "%d", iStrNo);
      memcpy(pOutbuf+OFF_M_STRNUM, acAddr1, iTmp);
      if (sMailAdr.strSub[0] > ' ')
      {
         strcat(acAddr1, " ");
         strcat(acAddr1, sMailAdr.strSub);
         memcpy(pOutbuf+OFF_M_STR_SUB, sMailAdr.strSub, strlen(sMailAdr.strSub));
      }
      if (sMailAdr.strDir[0] > ' ')
      {
         strcat(acAddr1, " ");
         strcat(acAddr1, sMailAdr.strDir);
         memcpy(pOutbuf+OFF_M_DIR, sMailAdr.strDir, strlen(sMailAdr.strDir));
      }
      strcat(acAddr1, " ");
   }

   strcat(acAddr1, sMailAdr.strName);
   memcpy(pOutbuf+OFF_M_STREET, sMailAdr.strName, strlen(sMailAdr.strName));
   if (sMailAdr.strSfx[0] > ' ')
   {
      strcat(acAddr1, " ");
      strcat(acAddr1, sMailAdr.strSfx);
      memcpy(pOutbuf+OFF_M_SUFF, sMailAdr.strSfx, strlen(sMailAdr.strSfx));
   }
   if (sMailAdr.Unit[0] > ' ')
   {
      strcat(acAddr1, " #");
      strcat(acAddr1, sMailAdr.Unit);
      memcpy(pOutbuf+OFF_M_UNITNO, sMailAdr.Unit, strlen(sMailAdr.Unit));
   }
   iTmp = blankRem(acAddr1, 0);
   memcpy(pOutbuf+OFF_M_ADDR_D, acAddr1, iTmp);

   // Mail city
   if (*apTokens[SBX_ROLL_M_CITY] > '0')
   {
      strcpy(acAddr2, apTokens[SBX_ROLL_M_CITY]);
      vmemcpy(pOutbuf+OFF_M_CITY, acAddr2, SIZ_M_CITY);
   }
   if (*apTokens[SBX_ROLL_M_ST] > '0')
   {
      strcat(acAddr2, " ");
      strcat(acAddr2, apTokens[SBX_ROLL_M_ST]);
      memcpy(pOutbuf+OFF_M_ST, apTokens[SBX_ROLL_M_ST], SIZ_M_ST);
   }

   if (!memcmp(apTokens[SBX_ROLL_COUNTRY], "USA", 3))
   {
      iTmp = atoi(apTokens[SBX_ROLL_M_ZIP]);
      if (iTmp > 0 && iTmp < 99999)
      {
         strcpy(sMailAdr.Zip, apTokens[SBX_ROLL_M_ZIP]);
      }
      iTmp = atoi(apTokens[SBX_ROLL_M_ZIP4]);
      if (iTmp > 0)
      {
         strcpy(sMailAdr.Zip4, apTokens[SBX_ROLL_M_ZIP4]);
      }
   } else if (isNumber(apTokens[SBX_ROLL_COUNTRY]))
   {
      strcpy(sMailAdr.Zip, apTokens[SBX_ROLL_COUNTRY]);
      strcpy(sMailAdr.Zip4, apTokens[SBX_ROLL_M_ZIP4]);
   } else 
   {
      strcpy(sMailAdr.Zip, apTokens[SBX_ROLL_M_ZIP]);
      strcpy(sMailAdr.Zip4, apTokens[SBX_ROLL_M_ZIP4]);
   }

   if (sMailAdr.Zip[0] > ' ')
   {
      memcpy(pOutbuf+OFF_M_ZIP, sMailAdr.Zip, strlen(sMailAdr.Zip));
      strcat(acAddr2, " ");
      strcat(acAddr2, sMailAdr.Zip);
   }

   if (sMailAdr.Zip4[0] > ' ')
   {
      strcat(acAddr2, "-");
      strcat(acAddr2, sMailAdr.Zip4);
      memcpy(pOutbuf+OFF_M_ZIP4, sMailAdr.Zip4, strlen(sMailAdr.Zip4));
   }

   iTmp = blankRem(acAddr2);
   if (iTmp > SIZ_M_CTY_ST_D && acAddr2[iTmp-5] == '-')
      acAddr2[iTmp-5] = 0;
   vmemcpy(pOutbuf+OFF_M_CTY_ST_D, acAddr2, SIZ_M_CTY_ST_D);

   // Situs
   acAddr1[0] = 0;
   acAddr2[0] = 0;
   iStrNo = atoi(apTokens[SBX_ROLL_S_STRNUM]);
   if (iStrNo > 0)
   {
      // Save original HSENO
      memset(pOutbuf+OFF_S_HSENO, ' ', SIZ_S_HSENO);
      memcpy(pOutbuf+OFF_S_HSENO, apTokens[SBX_ROLL_S_STRNUM], strlen(apTokens[SBX_ROLL_S_STRNUM]));
#ifdef _DEBUG
      // Found 200 situs with '-'
      //if (isCharIncluded(apTokens[SBX_ROLL_S_STRNUM], '-', 0))
      //   lRecCnt++;
#endif

      // Parse strname to pick up suffix
      if (*apTokens[SBX_ROLL_S_STRSFX] > '0')
      {
         strcpy(sSitusAdr.strSfx, apTokens[SBX_ROLL_S_STRSFX]);
         strcpy(sSitusAdr.strName, apTokens[SBX_ROLL_S_STRNAME]);
      } else
      {
         parseAdr1N(&sSitusAdr, apTokens[SBX_ROLL_S_STRNAME]);
      }

      strcpy(sSitusAdr.strNum, apTokens[SBX_ROLL_S_STRNUM]);
      if (*apTokens[SBX_ROLL_S_STRSUB] > '0')
         strcpy(sSitusAdr.strSub, apTokens[SBX_ROLL_S_STRSUB]);
      if (*apTokens[SBX_ROLL_S_STRDIR] > '0')
         strcpy(sSitusAdr.strDir, apTokens[SBX_ROLL_S_STRDIR]);

      // Get Unit
      if (*apTokens[SBX_ROLL_S_UNIT] > '0')
         strcpy(sSitusAdr.Unit, apTokens[SBX_ROLL_S_UNIT]);
   } else if (*apTokens[SBX_ROLL_S_STRNAME] >= 'A')
   {  // In most case this is PO BOX
      strcpy(sSitusAdr.strName, apTokens[SBX_ROLL_S_STRNAME]);
      //strcpy(acAddr1, sSitusAdr.strName);
   } else if (*apTokens[SBX_ROLL_S_STRNAME] >= '0')
   {  // Full address may be keyed in this field
#ifdef _DEBUG
      //if (strlen(apTokens[SBX_ROLL_S_STRNAME]) < 5)
      //   iTmp = 0;
#endif
      parseAdr1_1(&sSitusAdr, apTokens[SBX_ROLL_S_STRNAME]);
      iStrNo = sSitusAdr.lStrNum;
   }

   // Exception
   if (!memcmp(sSitusAdr.strSub, "HWY", 3))
   {
      sprintf(acTmp, "%s %s", sSitusAdr.strSub, sSitusAdr.strName);
      sSitusAdr.strSub[0] = 0;
      strcpy(sSitusAdr.strName, acTmp);
   }

   if (iStrNo)
   {
      sprintf(acAddr1, "%d       ", iStrNo);
      memcpy(pOutbuf+OFF_S_STRNUM, acAddr1, SIZ_S_STRNUM);
      if (sSitusAdr.strSub[0] > ' ')
      {
         strcat(acAddr1, sSitusAdr.strSub);
         memcpy(pOutbuf+OFF_S_STR_SUB, sSitusAdr.strSub, strlen(sSitusAdr.strSub));
         strcat(acAddr1, " ");
      }
      if (sSitusAdr.strDir[0] > ' ')
      {
         strcat(acAddr1, sSitusAdr.strDir);
         memcpy(pOutbuf+OFF_S_DIR, sSitusAdr.strDir, strlen(sSitusAdr.strDir));
         strcat(acAddr1, " ");
      }
   }

   strcat(acAddr1, sSitusAdr.strName);
   vmemcpy(pOutbuf+OFF_S_STREET, sSitusAdr.strName, SIZ_S_STREET);
   if (sSitusAdr.strSfx[0] > ' ')
   {
      strcat(acAddr1, " ");
      _strupr(sSitusAdr.strSfx);
      iTmp = GetSfxCodeX(sSitusAdr.strSfx, acTmp);
      if (iTmp)
      {
         strcat(acAddr1, acTmp);
         iTmp = sprintf(acTmp, "%d", iTmp);
         memcpy(pOutbuf+OFF_S_SUFF, acTmp, iTmp);
      } else
         strcat(acAddr1, sSitusAdr.strSfx);
   }
   if (sSitusAdr.Unit[0] > ' ')
   {
      strcat(acAddr1, " #");
      strcat(acAddr1, sSitusAdr.Unit);
      memcpy(pOutbuf+OFF_S_UNITNO, sSitusAdr.Unit, strlen(sSitusAdr.Unit));
   }

   blankRem(acAddr1);
   vmemcpy(pOutbuf+OFF_S_ADDR_D, acAddr1, SIZ_S_ADDR_D);

   // Situs city
   if (*apTokens[SBX_ROLL_S_CITY] > '0')
   {
      strcpy(acAddr2, _strupr(apTokens[SBX_ROLL_S_CITY]));
      City2Code(acAddr2, acTmp, pOutbuf);
      vmemcpy(pOutbuf+OFF_S_CITY, acTmp, SIZ_S_CITY);
      strcat(acAddr2, " CA");
      memcpy(pOutbuf+OFF_S_ST, "CA", SIZ_S_ST);
   }

   // Situs zip
   iTmp = atoi(apTokens[SBX_ROLL_S_ZIP]);
   if (iTmp > 0)
   {
      strcat(acAddr2, " ");
      strcat(acAddr2, apTokens[SBX_ROLL_S_ZIP]);
      vmemcpy(pOutbuf+OFF_S_ZIP, apTokens[SBX_ROLL_S_ZIP], SIZ_S_ZIP);
   } else if (*apTokens[SBX_ROLL_COUNTRY] >= '0' && *apTokens[SBX_ROLL_COUNTRY] <= '9')
   {
      strcat(acAddr2, " ");
      strcat(acAddr2, apTokens[SBX_ROLL_COUNTRY]);
      vmemcpy(pOutbuf+OFF_S_ZIP, apTokens[SBX_ROLL_COUNTRY], SIZ_S_ZIP);
   }
   iTmp = atoi(apTokens[SBX_ROLL_S_ZIP4]);
   if (iTmp > 0)
   {
      strcat(acAddr2, "-");
      strcat(acAddr2, apTokens[SBX_ROLL_S_ZIP4]);
      vmemcpy(pOutbuf+OFF_S_ZIP4, apTokens[SBX_ROLL_S_ZIP4], SIZ_S_ZIP4);
   }
   vmemcpy(pOutbuf+OFF_S_CTY_ST_D, acAddr2, SIZ_S_CTY_ST_D);
}

/********************************* Sbx_MergeAdr3 *****************************
 *
 * For 2019 LDR
 * Return 0 if successful, >0 if error
 *
 *****************************************************************************/

//void Sbx_MergeAdr3(char *pOutbuf)
//{
//   char     acTmp[256], acAddr1[64], acAddr2[64], *pTmp;
//   int      iTmp, iStrNo;
//   ADR_REC  sMailAdr;
//   ADR_REC  sSitusAdr;
//
//   // Clear old Mailing, CareOf, DBA
//   removeMailing(pOutbuf, true, true);
//   memset((void *)&sMailAdr, 0, sizeof(ADR_REC));
//
//   // Check for C/O
//   _strupr(apTokens[SBX_2019_MAILX1]);
//   if (!memcmp(apTokens[SBX_2019_MAILX1], "CO ", 3))
//      pTmp = apTokens[SBX_2019_MAILX1]+3;
//   else if (!memcmp(apTokens[SBX_2019_MAILX1], "C/O ", 4))
//      pTmp = apTokens[SBX_2019_MAILX1]+4;
//   else
//      pTmp = NULL;
//
//   if (pTmp)
//   {
//      iTmp = sprintf(acTmp, "C/O %s", pTmp);
//      updateCareOf(pOutbuf, acTmp, iTmp);
//   }
//
//   // Check for ATTN
//   if (!memcmp(apTokens[SBX_2019_MAILX1], "ATTN", 4))
//   {
//      vmemcpy(pOutbuf+OFF_CARE_OF, apTokens[SBX_2019_MAILX1], SIZ_CARE_OF);
//   }
//
//   // Check for DBA
//   if (!memcmp(apTokens[SBX_2019_MAILX1], "DBA ", 4))
//   {
//      vmemcpy(pOutbuf+OFF_DBA, apTokens[SBX_2019_MAILX1], SIZ_DBA);
//   }
//
//   // Mail address
//   acAddr1[0] = 0;
//   acAddr2[0] = 0;
//#ifdef _DEBUG
//   //if (!memcmp(pOutbuf, "083310018", 9))
//   //   iTmp = 0;
//#endif
//
//   iStrNo = atoi(apTokens[SBX_2019_M_STRNUM]);
//   if (iStrNo > 0)
//   {
//      // Parse strname to pick up suffix
//      if (*apTokens[SBX_2019_M_STRSFX] > '0')
//      {
//         strcpy(sMailAdr.strSfx, apTokens[SBX_2019_M_STRSFX]);
//         strcpy(sMailAdr.strName, apTokens[SBX_2019_M_STRNAME]);
//      } else
//      {
//         parseAdr1N(&sMailAdr, apTokens[SBX_2019_M_STRNAME]);
//      }
//
//      strcpy(sMailAdr.strNum, apTokens[SBX_2019_M_STRNUM]);
//      if (*apTokens[SBX_2019_M_STRSUB] > '0')
//         strcpy(sMailAdr.strSub, apTokens[SBX_2019_M_STRSUB]);
//      if (*apTokens[SBX_2019_M_STRDIR] > '0')
//         strcpy(sMailAdr.strDir, apTokens[SBX_2019_M_STRDIR]);
//
//      // Get Unit
//      if (*apTokens[SBX_2019_M_UNIT] > '0')
//         strcpy(sMailAdr.Unit, apTokens[SBX_2019_M_UNIT]);
//
//   } else if (*apTokens[SBX_2019_M_POBOX] > '0')
//   {
//      _strupr(apTokens[SBX_2019_M_POBOX]);
//      if (*apTokens[SBX_2019_M_POBOX] == 'P')
//         strcpy(sMailAdr.strName, apTokens[SBX_2019_M_POBOX]);
//      else
//         sprintf(sMailAdr.strName, "PO BOX %s", apTokens[SBX_2019_M_POBOX]);
//   } else if (*apTokens[SBX_2019_M_STRNAME] >= 'A')
//   {  // In most case this is PO BOX
//      strcpy(sMailAdr.strName, apTokens[SBX_2019_M_STRNAME]);
//      //strcpy(acAddr1, sMailAdr.strName);
//   } else if (*apTokens[SBX_2019_M_STRNAME] >= '0')
//   {  // Full address may be keyed in this field
//      parseAdr1_1(&sMailAdr, apTokens[SBX_2019_M_STRNAME]);
//      iStrNo = sMailAdr.lStrNum;
//   }
//
//   if (iStrNo)
//   {
//      iTmp = sprintf(acAddr1, "%d", iStrNo);
//      memcpy(pOutbuf+OFF_M_STRNUM, acAddr1, iTmp);
//      if (sMailAdr.strSub[0] > ' ')
//      {
//         strcat(acAddr1, " ");
//         strcat(acAddr1, sMailAdr.strSub);
//         memcpy(pOutbuf+OFF_M_STR_SUB, sMailAdr.strSub, strlen(sMailAdr.strSub));
//      }
//      if (sMailAdr.strDir[0] > ' ')
//      {
//         strcat(acAddr1, " ");
//         strcat(acAddr1, sMailAdr.strDir);
//         memcpy(pOutbuf+OFF_M_DIR, sMailAdr.strDir, strlen(sMailAdr.strDir));
//      }
//      strcat(acAddr1, " ");
//   }
//
//   strcat(acAddr1, sMailAdr.strName);
//   memcpy(pOutbuf+OFF_M_STREET, sMailAdr.strName, strlen(sMailAdr.strName));
//   if (sMailAdr.strSfx[0] > ' ')
//   {
//      strcat(acAddr1, " ");
//      strcat(acAddr1, sMailAdr.strSfx);
//      memcpy(pOutbuf+OFF_M_SUFF, sMailAdr.strSfx, strlen(sMailAdr.strSfx));
//   }
//   if (sMailAdr.Unit[0] > ' ')
//   {
//      strcat(acAddr1, " #");
//      strcat(acAddr1, sMailAdr.Unit);
//      memcpy(pOutbuf+OFF_M_UNITNO, sMailAdr.Unit, strlen(sMailAdr.Unit));
//   }
//   iTmp = blankRem(acAddr1, 0);
//   memcpy(pOutbuf+OFF_M_ADDR_D, acAddr1, iTmp);
//
//   // Mail city
//   if (*apTokens[SBX_2019_M_CITY] > '0')
//   {
//      strcpy(acAddr2, apTokens[SBX_2019_M_CITY]);
//      vmemcpy(pOutbuf+OFF_M_CITY, acAddr2, SIZ_M_CITY);
//   }
//   if (*apTokens[SBX_2019_M_ST] > '0')
//   {
//      strcat(acAddr2, " ");
//      strcat(acAddr2, apTokens[SBX_2019_M_ST]);
//      memcpy(pOutbuf+OFF_M_ST, apTokens[SBX_2019_M_ST], SIZ_M_ST);
//   }
//
//   if (!memcmp(apTokens[SBX_2019_COUNTRY], "USA", 3))
//   {
//      iTmp = atoi(apTokens[SBX_2019_M_ZIP]);
//      if (iTmp > 0 && iTmp < 99999)
//      {
//         strcpy(sMailAdr.Zip, apTokens[SBX_2019_M_ZIP]);
//      }
//      iTmp = atoi(apTokens[SBX_2019_M_ZIP4]);
//      if (iTmp > 0)
//      {
//         strcpy(sMailAdr.Zip4, apTokens[SBX_2019_M_ZIP4]);
//      }
//   } else if (isNumber(apTokens[SBX_2019_COUNTRY]))
//   {
//      strcpy(sMailAdr.Zip, apTokens[SBX_2019_COUNTRY]);
//      strcpy(sMailAdr.Zip4, apTokens[SBX_2019_M_ZIP4]);
//   } else 
//   {
//      strcpy(sMailAdr.Zip, apTokens[SBX_2019_M_ZIP]);
//      strcpy(sMailAdr.Zip4, apTokens[SBX_2019_M_ZIP4]);
//   }
//
//   if (sMailAdr.Zip[0] > ' ')
//   {
//      memcpy(pOutbuf+OFF_M_ZIP, sMailAdr.Zip, strlen(sMailAdr.Zip));
//      strcat(acAddr2, " ");
//      strcat(acAddr2, sMailAdr.Zip);
//   }
//
//   if (sMailAdr.Zip4[0] > ' ')
//   {
//      strcat(acAddr2, "-");
//      strcat(acAddr2, sMailAdr.Zip4);
//      memcpy(pOutbuf+OFF_M_ZIP4, sMailAdr.Zip4, strlen(sMailAdr.Zip4));
//   }
//
//   iTmp = blankRem(acAddr2);
//   if (iTmp > SIZ_M_CTY_ST_D && acAddr2[iTmp-5] == '-')
//      acAddr2[iTmp-5] = 0;
//   vmemcpy(pOutbuf+OFF_M_CTY_ST_D, acAddr2, SIZ_M_CTY_ST_D);
//
//   // Situs
//   memset((void *)&sSitusAdr, 0, sizeof(ADR_REC));
//   acAddr1[0] = 0;
//   acAddr2[0] = 0;
//   iStrNo = atoi(apTokens[SBX_2019_S_STRNUM]);
//   if (iStrNo > 0)
//   {
//      // Save original HSENO
//      memset(pOutbuf+OFF_S_HSENO, ' ', SIZ_S_HSENO);
//      memcpy(pOutbuf+OFF_S_HSENO, apTokens[SBX_2019_S_STRNUM], strlen(apTokens[SBX_2019_S_STRNUM]));
//#ifdef _DEBUG
//      // Found 200 situs with '-'
//      //if (isCharIncluded(apTokens[SBX_2019_S_STRNUM], '-', 0))
//      //   lRecCnt++;
//#endif
//
//      // Parse strname to pick up suffix
//      if (*apTokens[SBX_2019_S_STRSFX] > '0')
//      {
//         strcpy(sSitusAdr.strSfx, apTokens[SBX_2019_S_STRSFX]);
//         strcpy(sSitusAdr.strName, apTokens[SBX_2019_S_STRNAME]);
//      } else
//      {
//         parseAdr1N(&sSitusAdr, apTokens[SBX_2019_S_STRNAME]);
//      }
//
//      strcpy(sSitusAdr.strNum, apTokens[SBX_2019_S_STRNUM]);
//      if (*apTokens[SBX_2019_S_STRSUB] > '0')
//         strcpy(sSitusAdr.strSub, apTokens[SBX_2019_S_STRSUB]);
//      if (*apTokens[SBX_2019_S_STRDIR] > '0')
//         strcpy(sSitusAdr.strDir, apTokens[SBX_2019_S_STRDIR]);
//
//      // Get Unit
//      if (*apTokens[SBX_2019_S_UNIT] > '0')
//         strcpy(sSitusAdr.Unit, apTokens[SBX_2019_S_UNIT]);
//   } else if (*apTokens[SBX_2019_S_STRNAME] >= 'A')
//   {  // In most case this is PO BOX
//      strcpy(sSitusAdr.strName, apTokens[SBX_2019_S_STRNAME]);
//      //strcpy(acAddr1, sSitusAdr.strName);
//   } else if (*apTokens[SBX_2019_S_STRNAME] >= '0')
//   {  // Full address may be keyed in this field
//#ifdef _DEBUG
//      //if (strlen(apTokens[SBX_2019_S_STRNAME]) < 5)
//      //   iTmp = 0;
//#endif
//      parseAdr1_1(&sSitusAdr, apTokens[SBX_2019_S_STRNAME]);
//      iStrNo = sSitusAdr.lStrNum;
//   }
//
//   // Exception
//   if (!memcmp(sSitusAdr.strSub, "HWY", 3))
//   {
//      sprintf(acTmp, "%s %s", sSitusAdr.strSub, sSitusAdr.strName);
//      sSitusAdr.strSub[0] = 0;
//      strcpy(sSitusAdr.strName, acTmp);
//   }
//
//   if (iStrNo)
//   {
//      sprintf(acAddr1, "%d       ", iStrNo);
//      memcpy(pOutbuf+OFF_S_STRNUM, acAddr1, SIZ_S_STRNUM);
//      if (sSitusAdr.strSub[0] > ' ')
//      {
//         strcat(acAddr1, sSitusAdr.strSub);
//         memcpy(pOutbuf+OFF_S_STR_SUB, sSitusAdr.strSub, strlen(sSitusAdr.strSub));
//         strcat(acAddr1, " ");
//      }
//      if (sSitusAdr.strDir[0] > ' ')
//      {
//         strcat(acAddr1, sSitusAdr.strDir);
//         memcpy(pOutbuf+OFF_S_DIR, sSitusAdr.strDir, strlen(sSitusAdr.strDir));
//         strcat(acAddr1, " ");
//      }
//   }
//
//   strcat(acAddr1, sSitusAdr.strName);
//   vmemcpy(pOutbuf+OFF_S_STREET, sSitusAdr.strName, SIZ_S_STREET);
//   if (sSitusAdr.strSfx[0] > ' ')
//   {
//      strcat(acAddr1, " ");
//      _strupr(sSitusAdr.strSfx);
//      iTmp = GetSfxCodeX(sSitusAdr.strSfx, acTmp);
//      if (iTmp)
//      {
//         strcat(acAddr1, acTmp);
//         iTmp = sprintf(acTmp, "%d", iTmp);
//         memcpy(pOutbuf+OFF_S_SUFF, acTmp, iTmp);
//      } else
//         strcat(acAddr1, sSitusAdr.strSfx);
//   }
//   if (sSitusAdr.Unit[0] > ' ')
//   {
//      strcat(acAddr1, " #");
//      strcat(acAddr1, sSitusAdr.Unit);
//      memcpy(pOutbuf+OFF_S_UNITNO, sSitusAdr.Unit, strlen(sSitusAdr.Unit));
//   }
//
//   blankRem(acAddr1);
//   vmemcpy(pOutbuf+OFF_S_ADDR_D, acAddr1, SIZ_S_ADDR_D);
//
//   // Situs city
//   if (*apTokens[SBX_2019_S_CITY] > '0')
//   {
//      strcpy(acAddr2, _strupr(apTokens[SBX_2019_S_CITY]));
//      City2Code(acAddr2, acTmp, pOutbuf);
//      vmemcpy(pOutbuf+OFF_S_CITY, acTmp, SIZ_S_CITY);
//      strcat(acAddr2, " CA");
//      memcpy(pOutbuf+OFF_S_ST, "CA", SIZ_S_ST);
//   }
//
//   // Situs zip
//   iTmp = atoi(apTokens[SBX_2019_S_ZIP]);
//   if (iTmp > 0)
//   {
//      strcat(acAddr2, " ");
//      strcat(acAddr2, apTokens[SBX_2019_S_ZIP]);
//      vmemcpy(pOutbuf+OFF_S_ZIP, apTokens[SBX_2019_S_ZIP], SIZ_S_ZIP);
//   } else if (*apTokens[SBX_2019_COUNTRY] >= '0' && *apTokens[SBX_2019_COUNTRY] <= '9')
//   {
//      strcat(acAddr2, " ");
//      strcat(acAddr2, apTokens[SBX_2019_COUNTRY]);
//      vmemcpy(pOutbuf+OFF_S_ZIP, apTokens[SBX_2019_COUNTRY], SIZ_S_ZIP);
//   }
//   iTmp = atoi(apTokens[SBX_2019_S_ZIP4]);
//   if (iTmp > 0)
//   {
//      strcat(acAddr2, "-");
//      strcat(acAddr2, apTokens[SBX_2019_S_ZIP4]);
//      vmemcpy(pOutbuf+OFF_S_ZIP4, apTokens[SBX_2019_S_ZIP4], SIZ_S_ZIP4);
//   }
//   vmemcpy(pOutbuf+OFF_S_CTY_ST_D, acAddr2, SIZ_S_CTY_ST_D);
//}

/******************************** Sbx_UpdateAdr ******************************
 *
 * Return 0 if successful, >0 if error
 *
 *****************************************************************************/

void Sbx_UpdateAdr(char *pOutbuf)
{
   char     acTmp[256], acAddr1[64], acAddr2[64], *pTmp;
   int      iTmp, iStrNo;
   ADR_REC  sMailAdr;
   ADR_REC  sSitusAdr;

   // Clear old addresses
   removeSitus(pOutbuf);
   removeMailing(pOutbuf, true);
   memset((void *)&sMailAdr, 0, sizeof(ADR_REC));
   memset((void *)&sSitusAdr, 0, sizeof(ADR_REC));
   
   // Check for C/O
   if (!memcmp(apTokens[SBX_UPDT_ADDRESS1], "CO ", 3))
   {
      pTmp = apTokens[SBX_UPDT_ADDRESS1];
      iTmp = 3;
   } else if (!memcmp(apTokens[SBX_UPDT_ADDRESS1], "C/O ", 4))
   {
      pTmp = apTokens[SBX_UPDT_ADDRESS1];
      iTmp = 4;
   } else if (!memcmp(apTokens[SBX_UPDT_ADDRESS2], "CO ", 3))
   {
      pTmp = apTokens[SBX_UPDT_ADDRESS2];
      iTmp = 3;
   } else if (!memcmp(apTokens[SBX_UPDT_ADDRESS2], "C/O ", 4))
   {
      pTmp = apTokens[SBX_UPDT_ADDRESS2];
      iTmp = 4;
   } else
      iTmp = 0;

   if (iTmp > 0)
   {
      iTmp = sprintf(acTmp, "C/O %s", pTmp+iTmp);
      updateCareOf(pOutbuf, acTmp, iTmp);
   }

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "097620022", 9))
   //   iTmp = 0;
#endif

   // Mail address
   acAddr1[0] = 0;
   acAddr2[0] = 0;

   iStrNo = atoi(apTokens[SBX_UPDT_M_STRNUM]);
   if (iStrNo > 0)
   {
      // Parse strname to pick up suffix
      if (*apTokens[SBX_UPDT_M_STRSFX] > '0')
      {
         strcpy(sMailAdr.strSfx, apTokens[SBX_UPDT_M_STRSFX]);
         strcpy(sMailAdr.strName, apTokens[SBX_UPDT_M_STRNAME]);
      } else
      {
         parseAdr1N(&sMailAdr, apTokens[SBX_UPDT_M_STRNAME]);
      }

      strcpy(sMailAdr.strNum, apTokens[SBX_UPDT_M_STRNUM]);
      if (*apTokens[SBX_UPDT_M_STRSUB] > '0')
         strcpy(sMailAdr.strSub, apTokens[SBX_UPDT_M_STRSUB]);
      if (*apTokens[SBX_UPDT_M_STRDIR] > '0')
         strcpy(sMailAdr.strDir, apTokens[SBX_UPDT_M_STRDIR]);

      // Get Unit
      if (*apTokens[SBX_UPDT_M_UNIT] > '0')
      {
         strcpy(sMailAdr.Unit, apTokens[SBX_UPDT_M_UNIT]);
         strcpy(sMailAdr.UnitNox, apTokens[SBX_UPDT_M_UNIT]);
      }
   } else if (*apTokens[SBX_UPDT_POBOX] > '0')
   {
      _strupr(apTokens[SBX_UPDT_POBOX]);
      if (*apTokens[SBX_UPDT_POBOX] == 'P')
         strcpy(sMailAdr.strName, apTokens[SBX_UPDT_POBOX]);
      else
         sprintf(sMailAdr.strName, "PO BOX %s", apTokens[SBX_UPDT_POBOX]);
      //strcpy(acAddr1, sMailAdr.strName);
   } else if (*apTokens[SBX_UPDT_M_STRNAME] >= 'A')
   {  // In most case this is PO BOX
      strcpy(sMailAdr.strName, apTokens[SBX_UPDT_M_STRNAME]);
      //strcpy(acAddr1, sMailAdr.strName);
   } else if (*apTokens[SBX_UPDT_M_STRNAME] >= '0')
   {  // Full address may be keyed in this field
      parseAdr1_1(&sMailAdr, apTokens[SBX_UPDT_M_STRNAME]);
      iStrNo = sMailAdr.lStrNum;
   }

   if (iStrNo)
   {
      iTmp = sprintf(acAddr1, "%d", iStrNo);
      memcpy(pOutbuf+OFF_M_STRNUM, acAddr1, iTmp);
      if (sMailAdr.strSub[0] > ' ')
      {
         strcat(acAddr1, " ");
         strcat(acAddr1, sMailAdr.strSub);
         memcpy(pOutbuf+OFF_M_STR_SUB, sMailAdr.strSub, strlen(sMailAdr.strSub));
      }
      if (sMailAdr.strDir[0] > ' ')
      {
         strcat(acAddr1, " ");
         strcat(acAddr1, sMailAdr.strDir);
         memcpy(pOutbuf+OFF_M_DIR, sMailAdr.strDir, strlen(sMailAdr.strDir));
      }
      strcat(acAddr1, " ");
   }

   strcat(acAddr1, sMailAdr.strName);
   memcpy(pOutbuf+OFF_M_STREET, sMailAdr.strName, strlen(sMailAdr.strName));
   if (sMailAdr.strSfx[0] > ' ')
   {
      strcat(acAddr1, " ");
      strcat(acAddr1, sMailAdr.strSfx);
      memcpy(pOutbuf+OFF_M_SUFF, sMailAdr.strSfx, strlen(sMailAdr.strSfx));
   }
   if (sMailAdr.Unit[0] > ' ')
   {
      strcat(acAddr1, " #");
      strcat(acAddr1, sMailAdr.Unit);
      vmemcpy(pOutbuf+OFF_M_UNITNO, sMailAdr.Unit, SIZ_M_UNITNO);
      vmemcpy(pOutbuf+OFF_M_UNITNOX, sMailAdr.UnitNox, SIZ_M_UNITNOX);
   }
   iTmp = blankRem(acAddr1, 0);
   memcpy(pOutbuf+OFF_M_ADDR_D, acAddr1, iTmp);

   // Mail city
   if (*apTokens[SBX_UPDT_M_CITY] > '0')
   {
      strcpy(acAddr2, apTokens[SBX_UPDT_M_CITY]);
      vmemcpy(pOutbuf+OFF_M_CITY, acAddr2, SIZ_M_CITY);
   }

   iTmp = atoi(apTokens[SBX_UPDT_M_ZIP]);
   if (strlen(apTokens[SBX_UPDT_M_ST]) == 2)
   {
      strcat(acAddr2, " ");
      strcat(acAddr2, apTokens[SBX_UPDT_M_ST]);
      memcpy(pOutbuf+OFF_M_ST, apTokens[SBX_UPDT_M_ST], SIZ_M_ST);
   } else if (*apTokens[SBX_UPDT_M_ST] == 'C' && iTmp > 90000)
   {
      strcat(acAddr2, " CA");
      memcpy(pOutbuf+OFF_M_ST, "CA", SIZ_M_ST);
   }

   if (iTmp > 0)
   {
      sprintf(acTmp, " %0.5d", iTmp);
      strcat(acAddr2, acTmp);
      memcpy(pOutbuf+OFF_M_ZIP, (char *)&acTmp[1], SIZ_M_ZIP);
   } else
   {
      iTmp = strlen(apTokens[SBX_UPDT_COUNTRY]);
      if (iTmp == SIZ_M_ZIP)
      {
         strcat(acAddr2, " ");
         strcat(acAddr2, apTokens[SBX_UPDT_COUNTRY]);
         memcpy(pOutbuf+OFF_M_ZIP, apTokens[SBX_UPDT_COUNTRY], SIZ_M_ZIP);
      }
   }
   iTmp = atoi(apTokens[SBX_UPDT_M_ZIP4]);
   if (iTmp > 0)
   {
      sprintf(acTmp, "-%0.4d", iTmp);
      strcat(acAddr2, acTmp);
      memcpy(pOutbuf+OFF_M_ZIP4, (char *)&acTmp[1], SIZ_M_ZIP4);
   }
   iTmp = blankRem(acAddr2);
   if (iTmp > SIZ_M_CTY_ST_D && acAddr2[iTmp-5] == '-')
      acAddr2[iTmp-5] = 0;
   vmemcpy(pOutbuf+OFF_M_CTY_ST_D, acAddr2, SIZ_M_CTY_ST_D);

   // Situs
   acAddr1[0] = 0;
   acAddr2[0] = 0;
   iStrNo = atoi(apTokens[SBX_UPDT_S_STRNUM]);
   if (iStrNo > 0)
   {
      // Parse strname to pick up suffix
      if (*apTokens[SBX_UPDT_S_STRSFX] > '0')
      {
         strcpy(sSitusAdr.strSfx, apTokens[SBX_UPDT_S_STRSFX]);
         strcpy(sSitusAdr.strName, apTokens[SBX_UPDT_S_STRNAME]);
      } else
      {
         parseAdr1N(&sSitusAdr, apTokens[SBX_UPDT_S_STRNAME]);
      }

      strcpy(sSitusAdr.strNum, apTokens[SBX_UPDT_S_STRNUM]);
      if (*apTokens[SBX_UPDT_S_STRSUB] > '0')
         strcpy(sSitusAdr.strSub, apTokens[SBX_UPDT_S_STRSUB]);
      if (*apTokens[SBX_UPDT_S_STRDIR] > '0')
         strcpy(sSitusAdr.strDir, apTokens[SBX_UPDT_S_STRDIR]);

      // Get Unit
      if (*apTokens[SBX_UPDT_S_UNIT] > '0')
         strcpy(sSitusAdr.Unit, apTokens[SBX_UPDT_S_UNIT]);

   } else if (*apTokens[SBX_UPDT_POBOX] > '0')
   {
      if (!memcmp(apTokens[SBX_UPDT_POBOX], "PO", 2))
         strcpy(sSitusAdr.strName, apTokens[SBX_UPDT_POBOX]);
      else
         sprintf(sSitusAdr.strName, "PO BOX %s", apTokens[SBX_UPDT_POBOX]);
      //strcpy(acAddr1, sSitusAdr.strName);
   } else if (*apTokens[SBX_UPDT_S_STRNAME] >= 'A')
   {  // In most case this is PO BOX
      strcpy(sSitusAdr.strName, apTokens[SBX_UPDT_S_STRNAME]);
      //strcpy(acAddr1, sSitusAdr.strName);
   } else if (*apTokens[SBX_UPDT_S_STRNAME] >= '0')
   {  // Full address may be keyed in this field
#ifdef _DEBUG
      //if (strlen(apTokens[SBX_UPDT_S_STRNAME]) < 5)
      //   iTmp = 0;
#endif
      parseAdr1_1(&sSitusAdr, apTokens[SBX_UPDT_S_STRNAME]);
      iStrNo = sSitusAdr.lStrNum;
   }

   // Exception
   if (!memcmp(sSitusAdr.strSub, "HWY", 3))
   {
      sprintf(acTmp, "%s %s", sSitusAdr.strSub, sSitusAdr.strName);
      sSitusAdr.strSub[0] = 0;
      strcpy(sSitusAdr.strName, acTmp);
   }

   if (iStrNo)
   {
      iTmp = sprintf(acAddr1, "%d", iStrNo);
      memcpy(pOutbuf+OFF_S_STRNUM, acAddr1, iTmp);
      memcpy(pOutbuf+OFF_S_HSENO, acAddr1, iTmp);
      if (sSitusAdr.strSub[0] > ' ')
      {
         strcat(acAddr1, " ");
         strcat(acAddr1, sSitusAdr.strSub);
         memcpy(pOutbuf+OFF_S_STR_SUB, sSitusAdr.strSub, strlen(sSitusAdr.strSub));
      }
      if (sSitusAdr.strDir[0] > ' ')
      {
         strcat(acAddr1, " ");
         strcat(acAddr1, sSitusAdr.strDir);
         memcpy(pOutbuf+OFF_S_DIR, sSitusAdr.strDir, strlen(sSitusAdr.strDir));
      }
      strcat(acAddr1, " ");
   }

   iTmp = strlen(sSitusAdr.strName);
   if (iTmp > 0)
   {
      vmemcpy(pOutbuf+OFF_S_STREET, sSitusAdr.strName, SIZ_S_STREET, iTmp);

      strcat(acAddr1, sSitusAdr.strName);
      if (sSitusAdr.strSfx[0] > ' ')
      {
         strcat(acAddr1, " ");
         _strupr(sSitusAdr.strSfx);
         iTmp = GetSfxCodeX(sSitusAdr.strSfx, acTmp);
         if (iTmp > 0)
         {
            strcat(acAddr1, acTmp);
            sprintf(acTmp, "%d", iTmp);
            memcpy(pOutbuf+OFF_S_SUFF, acTmp, strlen(acTmp));
         } else
            strcat(acAddr1, sSitusAdr.strSfx);
      }
      if (sSitusAdr.Unit[0] > ' ')
      {
         strcat(acAddr1, " #");
         strcat(acAddr1, sSitusAdr.Unit);
         vmemcpy(pOutbuf+OFF_S_UNITNO, sSitusAdr.Unit, SIZ_S_UNITNO);
         vmemcpy(pOutbuf+OFF_S_UNITNOX, sSitusAdr.UnitNox, SIZ_S_UNITNOX);
      }
      vmemcpy(pOutbuf+OFF_S_ADDR_D, acAddr1, SIZ_S_ADDR_D);
   }

   // Situs city
   if (*apTokens[SBX_UPDT_S_CITY] > '0')
   {
      strcpy(acAddr2, _strupr(apTokens[SBX_UPDT_S_CITY]));
      City2Code(acAddr2, acTmp);
      memcpy(pOutbuf+OFF_S_CITY, acTmp, SIZ_S_CITY);

      strcat(acAddr2, " CA");
      memcpy(pOutbuf+OFF_S_ST, "CA", SIZ_S_ST);
      iTmp = atoi(apTokens[SBX_UPDT_S_ZIP]);
      if (iTmp >= 90000)
      {
         strcat(acAddr2, " ");
         strcat(acAddr2, apTokens[SBX_UPDT_S_ZIP]);
         memcpy(pOutbuf+OFF_S_ZIP, apTokens[SBX_UPDT_S_ZIP], SIZ_S_ZIP);
      //} else if (*apTokens[SBX_UPDT_COUNTRY] >= '0' && *apTokens[SBX_UPDT_COUNTRY] <= '9')
      //{
      //   strcat(acAddr2, " ");
      //   strcat(acAddr2, apTokens[SBX_UPDT_COUNTRY]);
      //   memcpy(pOutbuf+OFF_S_ZIP, apTokens[SBX_UPDT_COUNTRY], SIZ_S_ZIP);
      }
      vmemcpy(pOutbuf+OFF_S_CTY_ST_D, acAddr2, SIZ_S_CTY_ST_D);
   }
}

/****************************** Sbx_CreateLdrRec *****************************
 *
 * Return 0 if successful, >0 if error
 *
 *****************************************************************************/

int Sbx_CreateLdrRec(char *pOutbuf, char *pRollRec)
{
   char     acTmp[256], acTmp1[256], *pTmp;
   long     lTmp;
   int      iRet=0, iTmp;

   // Parse input string
   iRet = ParseStringNQ(pRollRec, cDelim, MAX_FLD_TOKEN, apTokens);
   if (iRet < SBX_ROLL_HOXMPAMT)
   {
      LogMsg("***** Error: bad input record for APN=%.10s", pRollRec);
      return -1;
   }

   // Clear output buffer
   memset(pOutbuf, ' ', iRecLen);

   // Start copying data
   memcpy(pOutbuf, apTokens[SBX_ROLL_APN], strlen(apTokens[SBX_ROLL_APN]));

   // Format APN
   iRet = formatApn(apTokens[SBX_ROLL_APN], acTmp, &myCounty);
   memcpy(pOutbuf+OFF_APN_D, acTmp, iRet);

   // Create MapLink and output new record
   iRet = formatMapLink(apTokens[SBX_ROLL_APN], acTmp, &myCounty);
   memcpy(pOutbuf+OFF_MAPLINK, acTmp, iRet);

   // Create index map link
   if (getIndexPage(acTmp, acTmp1, &myCounty))
      memcpy(pOutbuf+OFF_IMAPLINK, acTmp1, iRet);

   // County code and Parcel Status
   memcpy(pOutbuf+OFF_CO_NUM, "42SBXA", 6);

   // Year assessed
   memcpy(pOutbuf+OFF_YR_ASSD, myCounty.acYearAssd, 4);

   // HO Exempt
   lTmp = atoi(apTokens[SBX_ROLL_HOXMPAMT]);
   if (!lTmp)
      lTmp = atol(apTokens[SBX_ROLL_OTHXMPAMT]);
   if (lTmp > 0)
   {
      *(pOutbuf+OFF_HO_FL) = '1';      // 'Y'
      sprintf(acTmp, "%*u", SIZ_EXE_TOTAL, lTmp);
      memcpy(pOutbuf+OFF_EXE_TOTAL, acTmp, SIZ_EXE_TOTAL);
   } else
      *(pOutbuf+OFF_HO_FL) = '2';      // 'N'

   // Land
   long lLand = atoi(apTokens[SBX_ROLL_LAND]);
   if (lLand > 0)
   {
      sprintf(acTmp, "%*u", SIZ_LAND, lLand);
      memcpy(pOutbuf+OFF_LAND, acTmp, SIZ_LAND);
   }

   // Improve
   long lImpr = atoi(apTokens[SBX_ROLL_STRUCTIMP]);
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*u", SIZ_IMPR, lImpr);
      memcpy(pOutbuf+OFF_IMPR, acTmp, SIZ_IMPR);
   }

   // Other value: TradeFixture, LivingImp, PPDeclared, PPUnitVal, PPMH
   long lLivImpr= atoi(apTokens[SBX_ROLL_LIVINGIMP]);
   long lFixtr  = atoi(apTokens[SBX_ROLL_TRADEFIXTURE]);
   long lPers   = atoi(apTokens[SBX_ROLL_PPDECLARED]);
   long lPP_MH  = atoi(apTokens[SBX_ROLL_PPMOBILHOME]);
   long lUnitVal= atoi(apTokens[SBX_ROLL_PPUNITVALUE]);
   lTmp = lPers + lLivImpr + lFixtr + lPP_MH + lUnitVal;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*u", SIZ_OTHER, lTmp);
      memcpy(pOutbuf+OFF_OTHER, acTmp, SIZ_OTHER);

      if (lFixtr > 0)
      {
         sprintf(acTmp, "%d         ", lFixtr);
         memcpy(pOutbuf+OFF_FIXTR, acTmp, SIZ_FIXTR);
      }

      lPers += lUnitVal;
      if (lPers > 0)
      {
         sprintf(acTmp, "%d         ", lPers);
         memcpy(pOutbuf+OFF_PERSPROP, acTmp, SIZ_PERSPROP);
      }
      if (lPP_MH > 0)
      {
         sprintf(acTmp, "%d         ", lPP_MH);
         memcpy(pOutbuf+OFF_PP_MH, acTmp, SIZ_PP_MH);
      }
      if (lLivImpr > 0)
      {
         sprintf(acTmp, "%d         ", lLivImpr);
         memcpy(pOutbuf+OFF_GR_IMPR, acTmp, SIZ_GR_IMPR);
      }

      // This should be PP_VAL 20091001 -sn
      //if (lUnitVal > 0)
      //{
      //   sprintf(acTmp, "%d         ", lUnitVal);
      //   memcpy(pOutbuf+OFF_OTH_IMPR, acTmp, SIZ_OTH_IMPR);
      //}
   }

   // Gross total
   lTmp += lLand+lImpr;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*u", SIZ_GROSS, lTmp);
      memcpy(pOutbuf+OFF_GROSS, acTmp, SIZ_GROSS);
   }

   // Ratio
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*u", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
      memcpy(pOutbuf+OFF_RATIO, acTmp, SIZ_RATIO);
   }

   // TRA
   memcpy(pOutbuf+OFF_TRA, apTokens[SBX_ROLL_TRA], strlen(apTokens[SBX_ROLL_TRA]));

   // AG preserved
   if (!memcmp(apTokens[SBX_ROLL_VALREASON], "AGP", 3))
      *(pOutbuf+OFF_AG_PRE) = 'Y';

   // Set full exemption flag
   if (*apTokens[SBX_ROLL_NONTAXCODE] > ' ')
      *(pOutbuf+OFF_FULL_EXEMPT) = 'Y';
   else
      *(pOutbuf+OFF_FULL_EXEMPT) = ' ';

   // Owner
   try {
      Sbx_MergeOwner_2(pOutbuf, apTokens[SBX_ROLL_PRIMARYOWR], apTokens[SBX_ROLL_SECONDOWNER]);
   } catch(...) {
      LogMsg("***** Exception occurs in Sbx_MergeOwner(): %.*s", pOutbuf);
   }

   // Mailing - Situs
   try {
      Sbx_MergeAdr(pOutbuf);
   } catch(...) {
      LogMsg("***** Exception occurs in Sbx_MergeAdr(): %.*s", pOutbuf);
   }

   // UseCode
   iTmp = strlen(apTokens[SBX_ROLL_USECODE]);
   if (iTmp > 0)
   {
      vmemcpy(pOutbuf+OFF_USE_CO, apTokens[SBX_ROLL_USECODE], SIZ_USE_CO, iTmp);
      updateStdUse(pOutbuf+OFF_USE_STD, apTokens[SBX_ROLL_USECODE], iTmp, pOutbuf);
   } else
      memcpy(pOutbuf+OFF_USE_STD, USE_UNASGN, SIZ_USE_STD);

   // Acres
   double dTmp = atof(apTokens[SBX_ROLL_ACREAGE]);
   if (dTmp > 0.0)
   {
      lTmp = (long)(dTmp * SQFT_PER_ACRE);
      sprintf(acTmp, "%*u", SIZ_LOT_SQFT, lTmp);
      memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);

      lTmp = (long)(dTmp * 1000.0);
      sprintf(acTmp, "%*u", SIZ_LOT_ACRES, lTmp);
      memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);
   }

   // Recorded Doc
   if (*apTokens[SBX_ROLL_DOCNUM] > ' ')
      memcpy(pOutbuf+OFF_TRANSFER_DOC, apTokens[SBX_ROLL_DOCNUM], strlen(apTokens[SBX_ROLL_DOCNUM]));
   if (*apTokens[SBX_ROLL_DOCDATE] > ' ')
   {
      // Format on lien roll 2007 as mm/dd/yyyy
      //pTmp = dateConversion(apTokens[SBX_ROLL_DOCDATE], acTmp, 0);
      // Format on LDR 2010 as yyyy-mm-dd
      pTmp = dateConversion(apTokens[SBX_ROLL_DOCDATE], acTmp, YYYY_MM_DD);
      if (pTmp)
      {
         memcpy(pOutbuf+OFF_TRANSFER_DT, acTmp, 8);
         lTmp = atol(acTmp);
         if (lTmp > lLastRecDate && lTmp < lToday)
            lLastRecDate = lTmp;
      }
      // Assessor flag - transfer data is from assessor
      *(pOutbuf+OFF_AR_CODE1) = 'A';
   }

   // Set Prop8 flag
   if (*apTokens[SBX_ROLL_VALREASON] == 'V')
      *(pOutbuf+OFF_PROP8_FLG) = 'Y';

   return 0;
}

/***************************** Sbx_CreateLdrRec1 *****************************
 *
 * For 2014 - 2015 LDR
 * Return 0 if successful, otherwise error
 *
 *****************************************************************************/

int Sbx_CreateLdrRec1(char *pOutbuf, char *pRollRec)
{
   char     acTmp[256], acTmp1[256], *pTmp;
   long     lTmp;
   int      iRet=0, iTmp;

   // Parse input string
   if (cDelim == ',')
      iTmp = ParseStringNQ(pRollRec, cDelim, MAX_FLD_TOKEN, apTokens);
   else
      iTmp = ParseStringIQ(pRollRec, cDelim, MAX_FLD_TOKEN, apTokens);

   if (iTmp < SBX_ROLL_BATHROOMS)
   {
      LogMsg("***** Error: bad input record for APN=%.10s", pRollRec);
      return -1;
   }

   // Clear output buffer
   memset(pOutbuf, ' ', iRecLen);

   // Start copying data
   memcpy(pOutbuf, apTokens[SBX_ROLL_APN], strlen(apTokens[SBX_ROLL_APN]));

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "001101018", 9))
   //   iTmp = 0;
#endif


   // Format APN
   iRet = formatApn(apTokens[SBX_ROLL_APN], acTmp, &myCounty);
   memcpy(pOutbuf+OFF_APN_D, acTmp, iRet);

   // Create MapLink and output new record
   iRet = formatMapLink(apTokens[SBX_ROLL_APN], acTmp, &myCounty);
   memcpy(pOutbuf+OFF_MAPLINK, acTmp, iRet);

   // Create index map link
   if (getIndexPage(acTmp, acTmp1, &myCounty))
      memcpy(pOutbuf+OFF_IMAPLINK, acTmp1, iRet);

   // County code and Parcel Status
   memcpy(pOutbuf+OFF_CO_NUM, "42SBXA", 6);

   // Year assessed
   memcpy(pOutbuf+OFF_YR_ASSD, myCounty.acYearAssd, 4);

   // HO Exempt
   lTmp = atoi(apTokens[SBX_ROLL_HOXMPAMT]);
   if (!lTmp)
      lTmp = atol(apTokens[SBX_ROLL_OTHXMPAMT]);
   if (lTmp > 0)
   {
      *(pOutbuf+OFF_HO_FL) = '1';      // 'Y'
      sprintf(acTmp, "%*u", SIZ_EXE_TOTAL, lTmp);
      memcpy(pOutbuf+OFF_EXE_TOTAL, acTmp, SIZ_EXE_TOTAL);
   } else
      *(pOutbuf+OFF_HO_FL) = '2';      // 'N'

   // Land
   long lLand = atoi(apTokens[SBX_ROLL_LAND]);
   if (lLand > 0)
   {
      sprintf(acTmp, "%*u", SIZ_LAND, lLand);
      memcpy(pOutbuf+OFF_LAND, acTmp, SIZ_LAND);
   }

   // Improve
   long lImpr = atoi(apTokens[SBX_ROLL_STRUCTIMP]);
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*u", SIZ_IMPR, lImpr);
      memcpy(pOutbuf+OFF_IMPR, acTmp, SIZ_IMPR);
   }

   // Other value: TradeFixture, LivingImp, PPDeclared, PPUnitVal, PPMH
   long lLivImpr= atoi(apTokens[SBX_ROLL_LIVINGIMP]);
   long lFixtr  = atoi(apTokens[SBX_ROLL_TRADEFIXTURE]);
   long lPers   = atoi(apTokens[SBX_ROLL_PPDECLARED]);
   long lPP_MH  = atoi(apTokens[SBX_ROLL_PPMOBILHOME]);
   long lUnitVal= atoi(apTokens[SBX_ROLL_PPUNITVALUE]);
   lTmp = lPers + lLivImpr + lFixtr + lPP_MH + lUnitVal;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*u", SIZ_OTHER, lTmp);
      memcpy(pOutbuf+OFF_OTHER, acTmp, SIZ_OTHER);

      if (lFixtr > 0)
      {
         sprintf(acTmp, "%d         ", lFixtr);
         memcpy(pOutbuf+OFF_FIXTR, acTmp, SIZ_FIXTR);
      }

      lPers += lUnitVal;
      if (lPers > 0)
      {
         sprintf(acTmp, "%d         ", lPers);
         memcpy(pOutbuf+OFF_PERSPROP, acTmp, SIZ_PERSPROP);
      }
      if (lPP_MH > 0)
      {
         sprintf(acTmp, "%d         ", lPP_MH);
         memcpy(pOutbuf+OFF_PP_MH, acTmp, SIZ_PP_MH);
      }
      if (lLivImpr > 0)
      {
         sprintf(acTmp, "%d         ", lLivImpr);
         memcpy(pOutbuf+OFF_GR_IMPR, acTmp, SIZ_GR_IMPR);
      }

      // This should be PP_VAL 20091001 -sn
      //if (lUnitVal > 0)
      //{
      //   sprintf(acTmp, "%d         ", lUnitVal);
      //   memcpy(pOutbuf+OFF_OTH_IMPR, acTmp, SIZ_OTH_IMPR);
      //}
   }

   // Gross total
   lTmp += lLand+lImpr;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*u", SIZ_GROSS, lTmp);
      memcpy(pOutbuf+OFF_GROSS, acTmp, SIZ_GROSS);
   }

   // Ratio
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*u", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
      memcpy(pOutbuf+OFF_RATIO, acTmp, SIZ_RATIO);
   }

   // TRA
   memcpy(pOutbuf+OFF_TRA, apTokens[SBX_ROLL_TRA], strlen(apTokens[SBX_ROLL_TRA]));

   // AG preserved
   if (!memcmp(apTokens[SBX_ROLL_VALREASON], "AGP", 3))
      *(pOutbuf+OFF_AG_PRE) = 'Y';

   // Set full exemption flag
   if (*apTokens[SBX_ROLL_NONTAXCODE] > ' ')
      *(pOutbuf+OFF_FULL_EXEMPT) = 'Y';
   else
      *(pOutbuf+OFF_FULL_EXEMPT) = ' ';

   // Owner
   try {
      Sbx_MergeOwner_2(pOutbuf, apTokens[SBX_ROLL_PRIMARYOWR], apTokens[SBX_ROLL_SECONDOWNER]);
   } catch(...) {
      LogMsg("***** Exception occurs in Sbx_MergeOwner(): %.*s", pOutbuf);
   }

   // Mailing - Situs
   try {
      Sbx_MergeAdr(pOutbuf);
   } catch(...) {
      LogMsg("***** Exception occurs in Sbx_MergeAdr(): %.*s", pOutbuf);
   }

   // UseCode
   iTmp = strlen(apTokens[SBX_ROLL_USECODE]);
   if (iTmp > 0)
   {
      vmemcpy(pOutbuf+OFF_USE_CO, apTokens[SBX_ROLL_USECODE], SIZ_USE_CO, iTmp);
      updateStdUse(pOutbuf+OFF_USE_STD, apTokens[SBX_ROLL_USECODE], iTmp, pOutbuf);
   } else
      memcpy(pOutbuf+OFF_USE_STD, USE_UNASGN, SIZ_USE_STD);

   // Acres
   double dTmp = atof(apTokens[SBX_ROLL_ACREAGE]);
   if (dTmp > 0.0)
   {
      lTmp = (long)(dTmp * SQFT_PER_ACRE);
      sprintf(acTmp, "%*u", SIZ_LOT_SQFT, lTmp);
      memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);

      lTmp = (long)(dTmp * 1000.0);
      sprintf(acTmp, "%*u", SIZ_LOT_ACRES, lTmp);
      memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);
   }

   // Recorded Doc
   if (*apTokens[SBX_ROLL_DOCNUM] > ' ')
      memcpy(pOutbuf+OFF_TRANSFER_DOC, apTokens[SBX_ROLL_DOCNUM], strlen(apTokens[SBX_ROLL_DOCNUM]));
   if (*apTokens[SBX_ROLL_DOCDATE] > ' ')
   {
      // Format on lien roll 2007 as mm/dd/yyyy
      //pTmp = dateConversion(apTokens[SBX_ROLL_DOCDATE], acTmp, 0);
      // Format on LDR 2010 as yyyy-mm-dd
      pTmp = dateConversion(apTokens[SBX_ROLL_DOCDATE], acTmp, YYYY_MM_DD);
      if (pTmp)
      {
         memcpy(pOutbuf+OFF_TRANSFER_DT, acTmp, 8);
         lTmp = atol(acTmp);
         if (lTmp > lLastRecDate && lTmp < lToday)
            lLastRecDate = lTmp;
      }
      // Assessor flag - transfer data is from assessor
      *(pOutbuf+OFF_AR_CODE1) = 'A';
   }

   // Set Prop8 flag
   if (*apTokens[SBX_ROLL_VALREASON] == 'V')
      *(pOutbuf+OFF_PROP8_FLG) = 'Y';

   // New char fields added since 2014 LDR
   // BldgSqft
   lTmp = atol(apTokens[SBX_ROLL_BLDGSQFT]);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*u", SIZ_BLDG_SF, lTmp);
      memcpy(pOutbuf+OFF_BLDG_SF, acTmp, SIZ_BLDG_SF);
   }

   // YearBuilt
   lTmp = atol(apTokens[SBX_ROLL_YRBLT]);
   if (lTmp > 0 && lTmp < lToyear)
   {
      if (lTmp > 1000)
         sprintf(acTmp, "%d", lTmp);
      else if (lTmp < 100)
      {
         pTmp = dateConversion(apTokens[SBX_ROLL_YRBLT], acTmp, YY2YYYY);
         if (!pTmp)
            memset(acTmp, ' ', SIZ_YR_BLT);
      } else
         memset(acTmp, ' ', SIZ_YR_BLT);

      memcpy(pOutbuf+OFF_YR_BLT, acTmp, SIZ_YR_BLT);
   }
   
   // Bedrooms
   lTmp = atol(apTokens[SBX_ROLL_BEDROOMS]);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*u", SIZ_BEDS, lTmp);
      memcpy(pOutbuf+OFF_BEDS, acTmp, SIZ_BEDS);
   }
   
   // Bathrooms
   lTmp = atol(apTokens[SBX_ROLL_BATHROOMS]);
   if (lTmp > 0)
   {
      int iFBaths, iHBath, i4Q, i3Q, i2Q, i1Q;

      iFBaths=i4Q= lTmp;
      iHBath=i1Q=i2Q=i3Q=0;

      if (pTmp = strchr(apTokens[SBX_ROLL_BATHROOMS], '.'))
      {
         myTrim(apTokens[SBX_ROLL_BATHROOMS]);
         strcat(apTokens[SBX_ROLL_BATHROOMS], "00");
         lTmp = atoin(++pTmp, 2);
         if (lTmp >= 70)
         {
            iFBaths++;
            i3Q = 1;
         } else if (lTmp >= 50)
            iHBath=i2Q = 1;
         else if (lTmp >= 20)
            iHBath=i1Q = 1;

         if (iHBath > 0)
         {
            memcpy(pOutbuf+OFF_BATH_H, " 1", SIZ_BATH_F);
            if (i2Q > 0)
               memcpy(pOutbuf+OFF_BATH_2Q, " 1", 2);
            else
               memcpy(pOutbuf+OFF_BATH_1Q, " 1", 2);
         }
         if (i3Q > 0)
            memcpy(pOutbuf+OFF_BATH_3Q, " 1", 2);
      }

      sprintf(acTmp, "%*u", SIZ_BATH_F, iFBaths);
      memcpy(pOutbuf+OFF_BATH_F, acTmp, SIZ_BATH_F);
      sprintf(acTmp, "%*u", SIZ_BATH_F, i4Q);
      memcpy(pOutbuf+OFF_BATH_4Q, acTmp, SIZ_BATH_F);
   }

   return 0;
}

/***************************** Sbx_CreateLdrRec2 *****************************
 *
 * For 2016-2020 LDR file CertifiedSecuredRollWithPropChar.txt
 * Return 0 if successful, otherwise error
 *
 *****************************************************************************/

int Sbx_CreateLdrRec2(char *pOutbuf, char *pRollRec)
{
   char     acTmp[256], acTmp1[256], *pTmp;
   long     lTmp;
   int      iRet=0, iTmp;

   // Parse input string with '^' as string qualifier
   iTmp = ParseStringEx(pRollRec, cLdrSep, '^', MAX_FLD_TOKEN, apTokens);
   if (iTmp < SBX_ROLL_BATHROOMS)
   {
      LogMsg("***** Error: bad input record for APN=%.10s", pRollRec);
      return -1;
   }

   // Clear output buffer
   memset(pOutbuf, ' ', iRecLen);

   // Start copying data
   memcpy(pOutbuf, apTokens[SBX_ROLL_APN], strlen(apTokens[SBX_ROLL_APN]));

   // Format APN
   iRet = formatApn(apTokens[SBX_ROLL_APN], acTmp, &myCounty);
   memcpy(pOutbuf+OFF_APN_D, acTmp, iRet);

   // Create MapLink and output new record
   iRet = formatMapLink(apTokens[SBX_ROLL_APN], acTmp, &myCounty);
   memcpy(pOutbuf+OFF_MAPLINK, acTmp, iRet);

   // Create index map link
   if (getIndexPage(acTmp, acTmp1, &myCounty))
      memcpy(pOutbuf+OFF_IMAPLINK, acTmp1, iRet);

   // County code and Parcel Status
   memcpy(pOutbuf+OFF_CO_NUM, "42SBXA", 6);

   // Year assessed
   memcpy(pOutbuf+OFF_YR_ASSD, myCounty.acYearAssd, 4);

   // HO Exempt
   int lHoExe = atol(apTokens[SBX_ROLL_HOXMPAMT]);
   iTmp = OFF_EXE_CD1;
   if (lHoExe > 0)
   {
      *(pOutbuf+OFF_HO_FL) = '1';      // 'Y'
      memcpy(pOutbuf+OFF_EXE_CD1, "HO", 2);
      iTmp = OFF_EXE_CD2;
   } else
      *(pOutbuf+OFF_HO_FL) = '2';      // 'N'

   lTmp = lHoExe + atol(apTokens[SBX_ROLL_OTHXMPAMT]);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*u", SIZ_EXE_TOTAL, lTmp);
      memcpy(pOutbuf+OFF_EXE_TOTAL, acTmp, SIZ_EXE_TOTAL);
   }

   // Exe Code
   if (*apTokens[SBX_ROLL_EXEMPTCATGRY] > '0')
      vmemcpy(pOutbuf+iTmp, apTokens[SBX_ROLL_EXEMPTCATGRY], SIZ_EXE_CD1);

   // Create exemption type
   makeExeType(pOutbuf+OFF_EXE_TYPE, pOutbuf+OFF_EXE_CD1, pOutbuf+OFF_EXE_CD2, pOutbuf+OFF_EXE_CD3, (IDX_TBL4 *)&SBX_Exemption);

   // Land
   long lLand = atoi(apTokens[SBX_ROLL_LAND]);
   if (lLand > 0)
   {
      sprintf(acTmp, "%*u", SIZ_LAND, lLand);
      memcpy(pOutbuf+OFF_LAND, acTmp, SIZ_LAND);
   }

   // Improve
   long lImpr = atoi(apTokens[SBX_ROLL_STRUCTIMP]);
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*u", SIZ_IMPR, lImpr);
      memcpy(pOutbuf+OFF_IMPR, acTmp, SIZ_IMPR);
   }

   // Other value: TradeFixture, LivingImp, PPDeclared, PPUnitVal, PPMH
   long lLivImpr= atoi(apTokens[SBX_ROLL_LIVINGIMP]);
   long lFixtr  = atoi(apTokens[SBX_ROLL_TRADEFIXTURE]);
   long lPers   = atoi(apTokens[SBX_ROLL_PPDECLARED]);
   long lPP_MH  = atoi(apTokens[SBX_ROLL_PPMOBILHOME]);
   long lUnitVal= atoi(apTokens[SBX_ROLL_PPUNITVALUE]);
   LONGLONG lGross = lPers + lLivImpr + lFixtr + lPP_MH + lUnitVal;
   if (lGross > 0)
   {
      sprintf(acTmp, "%*u", SIZ_OTHER, lGross);
      memcpy(pOutbuf+OFF_OTHER, acTmp, SIZ_OTHER);

      if (lFixtr > 0)
      {
         sprintf(acTmp, "%u         ", lFixtr);
         memcpy(pOutbuf+OFF_FIXTR, acTmp, SIZ_FIXTR);
      }

      lPers += lUnitVal;
      if (lPers > 0)
      {
         sprintf(acTmp, "%u         ", lPers);
         memcpy(pOutbuf+OFF_PERSPROP, acTmp, SIZ_PERSPROP);
      }
      if (lPP_MH > 0)
      {
         sprintf(acTmp, "%u         ", lPP_MH);
         memcpy(pOutbuf+OFF_PP_MH, acTmp, SIZ_PP_MH);
      }
      if (lLivImpr > 0)
      {
         sprintf(acTmp, "%u         ", lLivImpr);
         memcpy(pOutbuf+OFF_GR_IMPR, acTmp, SIZ_GR_IMPR);
      }
   }

   // Gross total
   lGross += lLand+lImpr;
   if (lGross > 0)
   {
      sprintf(acTmp, "%*u", SIZ_GROSS, lGross);
      memcpy(pOutbuf+OFF_GROSS, acTmp, SIZ_GROSS);
   }

   // Ratio
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*u", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
      memcpy(pOutbuf+OFF_RATIO, acTmp, SIZ_RATIO);
   }

   // TRA
   memcpy(pOutbuf+OFF_TRA, apTokens[SBX_ROLL_TRA], strlen(apTokens[SBX_ROLL_TRA]));

   // AG preserved
   if (!memcmp(apTokens[SBX_ROLL_VALREASON], "AGP", 3))
      *(pOutbuf+OFF_AG_PRE) = 'Y';

   // Set full exemption flag
   if (*apTokens[SBX_ROLL_NONTAXCODE] > ' ')
      *(pOutbuf+OFF_FULL_EXEMPT) = 'Y';
   else
      *(pOutbuf+OFF_FULL_EXEMPT) = ' ';

   // Owner
   try {
      Sbx_MergeOwner_2(pOutbuf, apTokens[SBX_ROLL_PRIMARYOWR], apTokens[SBX_ROLL_SECONDOWNER]);
   } catch(...) {
      LogMsg("***** Exception occurs in Sbx_MergeOwner(): %.*s", pOutbuf);
   }

   // Mailing - Situs
   try {
      Sbx_MergeAdr(pOutbuf);
   } catch(...) {
      LogMsg("***** Exception occurs in Sbx_MergeAdr(): %.*s", pOutbuf);
   }

   // UseCode
   iTmp = strlen(apTokens[SBX_ROLL_USECODE]);
   if (iTmp > 0)
   {
      vmemcpy(pOutbuf+OFF_USE_CO, apTokens[SBX_ROLL_USECODE], SIZ_USE_CO);
      updateStdUse(pOutbuf+OFF_USE_STD, apTokens[SBX_ROLL_USECODE], iTmp, pOutbuf);
   } else
      memcpy(pOutbuf+OFF_USE_STD, USE_UNASGN, SIZ_USE_STD);

   // Acres
   double dTmp = atof(apTokens[SBX_ROLL_ACREAGE]);
   if (dTmp > 0.0)
   {
      lTmp = (long)(dTmp * SQFT_PER_ACRE);
      sprintf(acTmp, "%*u", SIZ_LOT_SQFT, lTmp);
      memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);

      lTmp = (long)(dTmp * 1000.0);
      sprintf(acTmp, "%*u", SIZ_LOT_ACRES, lTmp);
      memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);
   }

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "003141020", 9))
   //   iTmp = 0;
#endif

   // Transfer
   if (*apTokens[SBX_ROLL_DOCDATE] > ' ')
   {
      // Format on lien roll 2007 as mm/dd/yyyy
      //pTmp = dateConversion(apTokens[SBX_ROLL_DOCDATE], acTmp, 0);
      // Format on LDR 2010 as yyyy-mm-dd
      // LDR2020 mm/dd/yyyy
      pTmp = dateConversion(apTokens[SBX_ROLL_DOCDATE], acTmp, MM_DD_YYYY_2);
      if (pTmp)
      {
         memcpy(pOutbuf+OFF_TRANSFER_DT, acTmp, 8);
         lTmp = atol(acTmp);
         if (lTmp > lLastRecDate && lTmp < lToday)
            lLastRecDate = lTmp;

         if (*apTokens[SBX_ROLL_DOCNUM] > ' ')
         {
            iTmp = replStr(apTokens[SBX_ROLL_DOCNUM], "--", "-0");
            if (iTmp > 0)
               LogMsg("***DocNum changed: %s [%s]", apTokens[SBX_ROLL_DOCNUM], apTokens[SBX_ROLL_APN]);
            memcpy(pOutbuf+OFF_TRANSFER_DOC, apTokens[SBX_ROLL_DOCNUM], strlen(apTokens[SBX_ROLL_DOCNUM]));
         }
      }
      // Assessor flag - transfer data is from assessor
      *(pOutbuf+OFF_AR_CODE1) = 'A';
   }

   // Set Prop8 flag
   if (*apTokens[SBX_ROLL_VALREASON] == 'V')
      *(pOutbuf+OFF_PROP8_FLG) = 'Y';
   else if (!memcmp(apTokens[SBX_ROLL_VALREASON], "AGP", 3))
      *(pOutbuf+OFF_AG_PRE) = 'Y';

   // New char fields added since 2014 LDR
   // BldgSqft
   lTmp = atol(apTokens[SBX_ROLL_BLDGSQFT]);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*u", SIZ_BLDG_SF, lTmp);
      memcpy(pOutbuf+OFF_BLDG_SF, acTmp, SIZ_BLDG_SF);
   }

   // YearBuilt
   lTmp = atol(apTokens[SBX_ROLL_YRBLT]);
   if (lTmp > 0 && lTmp < lToyear)
   {
      if (lTmp > 1000)
         sprintf(acTmp, "%d", lTmp);
      else if (lTmp < 100)
      {
         pTmp = dateConversion(apTokens[SBX_ROLL_YRBLT], acTmp, YY2YYYY);
         if (!pTmp)
            memset(acTmp, ' ', SIZ_YR_BLT);
      } else
         memset(acTmp, ' ', SIZ_YR_BLT);

      memcpy(pOutbuf+OFF_YR_BLT, acTmp, SIZ_YR_BLT);
   }
   
   // Bedrooms
   lTmp = atol(apTokens[SBX_ROLL_BEDROOMS]);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*u", SIZ_BEDS, lTmp);
      memcpy(pOutbuf+OFF_BEDS, acTmp, SIZ_BEDS);
   }
   
   // Bathrooms
   lTmp = atol(apTokens[SBX_ROLL_BATHROOMS]);
   if (lTmp > 0)
   {
      int iFBaths, iHBath, i4Q, i3Q, i2Q, i1Q;

      iFBaths=i4Q= lTmp;
      iHBath=i1Q=i2Q=i3Q=0;

      if (pTmp = strchr(apTokens[SBX_ROLL_BATHROOMS], '.'))
      {
         myTrim(apTokens[SBX_ROLL_BATHROOMS]);
         strcat(apTokens[SBX_ROLL_BATHROOMS], "00");
         lTmp = atoin(++pTmp, 2);
         if (lTmp >= 70)
         {
            iFBaths++;
            i3Q = 1;
         } else if (lTmp >= 50)
            iHBath=i2Q = 1;
         else if (lTmp >= 20)
            iHBath=i1Q = 1;

         if (iHBath > 0)
         {
            memcpy(pOutbuf+OFF_BATH_H, " 1", SIZ_BATH_F);
            if (i2Q > 0)
               memcpy(pOutbuf+OFF_BATH_2Q, " 1", 2);
            else
               memcpy(pOutbuf+OFF_BATH_1Q, " 1", 2);
         }
         if (i3Q > 0)
            memcpy(pOutbuf+OFF_BATH_3Q, " 1", 2);
      }

      sprintf(acTmp, "%*u", SIZ_BATH_F, iFBaths);
      memcpy(pOutbuf+OFF_BATH_F, acTmp, SIZ_BATH_F);
      sprintf(acTmp, "%*u", SIZ_BATH_F, i4Q);
      memcpy(pOutbuf+OFF_BATH_4Q, acTmp, SIZ_BATH_F);
   }

   return 0;
}

/***************************** Sbx_CreateLdrRec3 *****************************
 *
 * For 2019 LDR
 * Return 0 if successful, otherwise error
 *
 *****************************************************************************/

//int Sbx_CreateLdrRec3(char *pOutbuf, char *pRollRec)
//{
//   char     acTmp[256], acTmp1[256], *pTmp;
//   long     lTmp;
//   int      iRet=0, iTmp;
//
//   // Parse input string
//   iTmp = ParseStringEx(pRollRec, cDelim, '^', MAX_FLD_TOKEN, apTokens);
//   if (iTmp < SBX_2019_FLDS)
//   {
//      LogMsg("***** Error: bad input record for APN=%.10s", pRollRec);
//      return -1;
//   }
//
//   // Clear output buffer
//   memset(pOutbuf, ' ', iRecLen);
//
//   // Start copying data
//   memcpy(pOutbuf, apTokens[SBX_2019_APN], strlen(apTokens[SBX_2019_APN]));
//
//#ifdef _DEBUG
//   //if (!memcmp(pOutbuf, "001101018", 9))
//   //   iTmp = 0;
//#endif
//
//   // Format APN
//   iRet = formatApn(apTokens[SBX_2019_APN], acTmp, &myCounty);
//   memcpy(pOutbuf+OFF_APN_D, acTmp, iRet);
//
//   // Create MapLink and output new record
//   iRet = formatMapLink(apTokens[SBX_2019_APN], acTmp, &myCounty);
//   memcpy(pOutbuf+OFF_MAPLINK, acTmp, iRet);
//
//   // Create index map link
//   if (getIndexPage(acTmp, acTmp1, &myCounty))
//      memcpy(pOutbuf+OFF_IMAPLINK, acTmp1, iRet);
//
//   // County code and Parcel Status
//   memcpy(pOutbuf+OFF_CO_NUM, "42SBXA", 6);
//
//   // Year assessed
//   memcpy(pOutbuf+OFF_YR_ASSD, myCounty.acYearAssd, 4);
//
//   // HO Exempt
//   lTmp = atoi(apTokens[SBX_2019_HOMEOWEX]);
//   if (lTmp > 0)
//   {
//      *(pOutbuf+OFF_HO_FL) = '1';      // 'Y'
//      sprintf(acTmp, "%*u", SIZ_EXE_TOTAL, lTmp);
//      memcpy(pOutbuf+OFF_EXE_TOTAL, acTmp, SIZ_EXE_TOTAL);
//   } else
//   {
//      *(pOutbuf+OFF_HO_FL) = '2';      // 'N'
//      lTmp = atol(apTokens[SBX_2019_EXEMPTIONS]);
//      if (lTmp > 0)
//      {
//         sprintf(acTmp, "%*u", SIZ_EXE_TOTAL, lTmp);
//         memcpy(pOutbuf+OFF_EXE_TOTAL, acTmp, SIZ_EXE_TOTAL);
//         vmemcpy(pOutbuf+OFF_EXE_CD1, apTokens[SBX_2019_EXEMPCODE], SIZ_EXE_CD1);
//      }
//   }
//
//   // Land
//   long lLand = atoi(apTokens[SBX_2019_LANDVALUE]);
//   if (lLand > 0)
//   {
//      sprintf(acTmp, "%*u", SIZ_LAND, lLand);
//      memcpy(pOutbuf+OFF_LAND, acTmp, SIZ_LAND);
//   }
//
//   // Improve
//   long lImpr = atoi(apTokens[SBX_2019_STRIMPR]);
//   if (lImpr > 0)
//   {
//      sprintf(acTmp, "%*u", SIZ_IMPR, lImpr);
//      memcpy(pOutbuf+OFF_IMPR, acTmp, SIZ_IMPR);
//   }
//
//   // Other value: TradeFixture, LivingImp, PPDeclared, PPUnitVal, PPMH
//   long lLivImpr= atoi(apTokens[SBX_2019_LIVIMPR]);
//   long lFixtr  = atoi(apTokens[SBX_2019_TRADEFIX]);
//   long lPers   = atoi(apTokens[SBX_2019_PERPROPDEC]);
//   long lPP_MH  = atoi(apTokens[SBX_2019_MOBILEHOME]);
//   long lUnitVal= atoi(apTokens[SBX_2019_PERSPROPUN]);
//   lTmp = lPers + lLivImpr + lFixtr + lPP_MH + lUnitVal;
//   if (lTmp > 0)
//   {
//      sprintf(acTmp, "%*u", SIZ_OTHER, lTmp);
//      memcpy(pOutbuf+OFF_OTHER, acTmp, SIZ_OTHER);
//
//      if (lFixtr > 0)
//      {
//         sprintf(acTmp, "%d         ", lFixtr);
//         memcpy(pOutbuf+OFF_FIXTR, acTmp, SIZ_FIXTR);
//      }
//
//      lPers += lUnitVal;
//      if (lPers > 0)
//      {
//         sprintf(acTmp, "%d         ", lPers);
//         memcpy(pOutbuf+OFF_PERSPROP, acTmp, SIZ_PERSPROP);
//      }
//      if (lPP_MH > 0)
//      {
//         sprintf(acTmp, "%d         ", lPP_MH);
//         memcpy(pOutbuf+OFF_PP_MH, acTmp, SIZ_PP_MH);
//      }
//      if (lLivImpr > 0)
//      {
//         sprintf(acTmp, "%d         ", lLivImpr);
//         memcpy(pOutbuf+OFF_GR_IMPR, acTmp, SIZ_GR_IMPR);
//      }
//
//      // This should be PP_VAL 20091001 -sn
//      //if (lUnitVal > 0)
//      //{
//      //   sprintf(acTmp, "%d         ", lUnitVal);
//      //   memcpy(pOutbuf+OFF_OTH_IMPR, acTmp, SIZ_OTH_IMPR);
//      //}
//   }
//
//   // Gross total
//   lTmp += lLand+lImpr;
//   if (lTmp > 0)
//   {
//      sprintf(acTmp, "%*u", SIZ_GROSS, lTmp);
//      memcpy(pOutbuf+OFF_GROSS, acTmp, SIZ_GROSS);
//   }
//
//   // Ratio
//   if (lImpr > 0)
//   {
//      sprintf(acTmp, "%*u", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
//      memcpy(pOutbuf+OFF_RATIO, acTmp, SIZ_RATIO);
//   }
//
//   // TRA
//   memcpy(pOutbuf+OFF_TRA, apTokens[SBX_2019_TRA], strlen(apTokens[SBX_2019_TRA]));
//
//   // AG preserved
//   if (!memcmp(apTokens[SBX_2019_VALREASON], "AGP", 3))
//      *(pOutbuf+OFF_AG_PRE) = 'Y';
//
//   // Set full exemption flag
//   if (*apTokens[SBX_2019_NONTAXCODE] > ' ')
//      *(pOutbuf+OFF_FULL_EXEMPT) = 'Y';
//   else
//      *(pOutbuf+OFF_FULL_EXEMPT) = ' ';
//
//   // Owner
//   try {
//      Sbx_MergeOwner_2(pOutbuf, apTokens[SBX_2019_OWNER], apTokens[SBX_2019_CONOWNER]);
//   } catch(...) {
//      LogMsg("***** Exception occurs in Sbx_MergeOwner(): %.*s", pOutbuf);
//   }
//
//   // Mailing - Situs
//   try {
//      Sbx_MergeAdr3(pOutbuf);
//   } catch(...) {
//      LogMsg("***** Exception occurs in Sbx_MergeAdr(): %.*s", pOutbuf);
//   }
//
//   // UseCode
//   iTmp = strlen(apTokens[SBX_2019_USECODE]);
//   if (iTmp > 0)
//   {
//      vmemcpy(pOutbuf+OFF_USE_CO, apTokens[SBX_2019_USECODE], SIZ_USE_CO);
//      updateStdUse(pOutbuf+OFF_USE_STD, apTokens[SBX_2019_USECODE], iTmp, pOutbuf);
//   } else
//      memcpy(pOutbuf+OFF_USE_STD, USE_UNASGN, SIZ_USE_STD);
//
//   // Acres
//   double dTmp = atof(apTokens[SBX_2019_ACREAGE]);
//   if (dTmp > 0.0)
//   {
//      lTmp = (long)(dTmp * SQFT_PER_ACRE);
//      sprintf(acTmp, "%*u", SIZ_LOT_SQFT, lTmp);
//      memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);
//
//      lTmp = (long)(dTmp * 1000.0);
//      sprintf(acTmp, "%*u", SIZ_LOT_ACRES, lTmp);
//      memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);
//   }
//
//   // Recorded Doc
//   if (*apTokens[SBX_2019_DOCDATE] > ' ')
//   {
//      // Format on lien roll 2007 as mm/dd/yyyy
//      //pTmp = dateConversion(apTokens[SBX_2019_DOCDATE], acTmp, 0);
//      // Format on LDR 2010 as yyyy-mm-dd
//      pTmp = dateConversion(apTokens[SBX_2019_DOCDATE], acTmp, YYYY_MM_DD);
//      if (pTmp)
//      {
//         if (*apTokens[SBX_2019_DOCNUM] > ' ')
//         {
//            memcpy(pOutbuf+OFF_TRANSFER_DOC, apTokens[SBX_2019_DOCNUM], strlen(apTokens[SBX_2019_DOCNUM]));
//            memcpy(pOutbuf+OFF_TRANSFER_DT, acTmp, 8);
//         }
//         lTmp = atol(acTmp);
//         if (lTmp > lLastRecDate && lTmp < lToday)
//            lLastRecDate = lTmp;
//      }
//      // Assessor flag - transfer data is from assessor
//      *(pOutbuf+OFF_AR_CODE1) = 'A';
//   }
//
//   // Set Prop8 flag
//   if (*apTokens[SBX_2019_VALREASON] == 'V')
//      *(pOutbuf+OFF_PROP8_FLG) = 'Y';
//   else if (!memcmp(apTokens[SBX_2019_VALREASON], "AGP", 3))
//      *(pOutbuf+OFF_AG_PRE) = 'Y';
//
//   // New char fields added since 2014 LDR
//   // BldgSqft
//   lTmp = atol(apTokens[SBX_2019_SQFOOTAGE]);
//   if (lTmp > 0)
//   {
//      sprintf(acTmp, "%*u", SIZ_BLDG_SF, lTmp);
//      memcpy(pOutbuf+OFF_BLDG_SF, acTmp, SIZ_BLDG_SF);
//   }
//
//   // YearBuilt
//   lTmp = atol(apTokens[SBX_2019_YEARBUILT]);
//   if (lTmp > 0 && lTmp < lToyear)
//   {
//      if (lTmp > 1000)
//         sprintf(acTmp, "%d", lTmp);
//      else if (lTmp < 100)
//      {
//         pTmp = dateConversion(apTokens[SBX_2019_YEARBUILT], acTmp, YY2YYYY);
//         if (!pTmp)
//            memset(acTmp, ' ', SIZ_YR_BLT);
//      } else
//         memset(acTmp, ' ', SIZ_YR_BLT);
//
//      memcpy(pOutbuf+OFF_YR_BLT, acTmp, SIZ_YR_BLT);
//   }
//   
//   // Bedrooms
//   lTmp = atol(apTokens[SBX_2019_BEDROOMS]);
//   if (lTmp > 0)
//   {
//      sprintf(acTmp, "%*u", SIZ_BEDS, lTmp);
//      memcpy(pOutbuf+OFF_BEDS, acTmp, SIZ_BEDS);
//   }
//   
//   // Bathrooms
//   lTmp = atol(apTokens[SBX_2019_BATHROOMS]);
//   if (lTmp > 0)
//   {
//      int iFBaths, iHBath, i4Q, i3Q, i2Q, i1Q;
//
//      iFBaths=i4Q= lTmp;
//      iHBath=i1Q=i2Q=i3Q=0;
//
//      if (pTmp = strchr(apTokens[SBX_2019_BATHROOMS], '.'))
//      {
//         myTrim(apTokens[SBX_2019_BATHROOMS]);
//         strcat(apTokens[SBX_2019_BATHROOMS], "00");
//         lTmp = atoin(++pTmp, 2);
//         if (lTmp >= 70)
//         {
//            iFBaths++;
//            i3Q = 1;
//         } else if (lTmp >= 50)
//            iHBath=i2Q = 1;
//         else if (lTmp >= 20)
//            iHBath=i1Q = 1;
//
//         if (iHBath > 0)
//         {
//            memcpy(pOutbuf+OFF_BATH_H, " 1", SIZ_BATH_F);
//            if (i2Q > 0)
//               memcpy(pOutbuf+OFF_BATH_2Q, " 1", 2);
//            else
//               memcpy(pOutbuf+OFF_BATH_1Q, " 1", 2);
//         }
//         if (i3Q > 0)
//            memcpy(pOutbuf+OFF_BATH_3Q, " 1", 2);
//      }
//
//      sprintf(acTmp, "%*u", SIZ_BATH_F, iFBaths);
//      memcpy(pOutbuf+OFF_BATH_F, acTmp, SIZ_BATH_F);
//      sprintf(acTmp, "%*u", SIZ_BATH_F, i4Q);
//      memcpy(pOutbuf+OFF_BATH_4Q, acTmp, SIZ_BATH_F);
//   }
//
//   // AgPreserve license
//   if (*apTokens[SBX_2019_AGPRES] > ' ')
//      *(pOutbuf+OFF_AG_PRE) = 'Y';
//   return 0;
//}

/********************************* Sbx_MergeRoll *****************************
 *
 * Return 0 if successful, >0 if error
 *
 *****************************************************************************/

int Sbx_MergeRoll(char *pOutbuf, char *pRollRec, int iLen, int iFlag)
{
   char     acTmp[256], acTmp1[256], *pTmp;
   long     lTmp;
   int      iRet=0, iTmp;

   // Replace tab char with 0
   iRet = ParseStringNQ(pRollRec, ',', MAX_FLD_TOKEN, apTokens);
   if (iRet < SBX_UPDT_BATHROOMS)
      return -1;

   if (iFlag & CREATE_R01)
   {
      // Clear output buffer
      memset(pOutbuf, ' ', iRecLen);

      // Start copying data
      memcpy(pOutbuf, apTokens[SBX_UPDT_APN], strlen(apTokens[SBX_UPDT_APN]));

      // Format APN
      iRet = formatApn(apTokens[SBX_UPDT_APN], acTmp, &myCounty);
      memcpy(pOutbuf+OFF_APN_D, acTmp, iRet);

      // Create MapLink and output new record
      iRet = formatMapLink(apTokens[SBX_UPDT_APN], acTmp, &myCounty);
      memcpy(pOutbuf+OFF_MAPLINK, acTmp, iRet);

      // Create index map link
      if (getIndexPage(acTmp, acTmp1, &myCounty))
         memcpy(pOutbuf+OFF_IMAPLINK, acTmp1, iRet);

      // County code and Parcel Status
      memcpy(pOutbuf+OFF_CO_NUM, "42SBXA", 6);

      // Year assessed
      memcpy(pOutbuf+OFF_YR_ASSD, myCounty.acYearAssd, 4);
   } else if (!memcmp(pOutbuf, "097100", 6))
   {
      iRet = formatMapLink(apTokens[SBX_UPDT_APN], acTmp, &myCounty);
      memcpy(pOutbuf+OFF_MAPLINK, acTmp, iRet);
   }

   // TRA
   memcpy(pOutbuf+OFF_TRA, apTokens[SBX_UPDT_TRA], strlen(apTokens[SBX_UPDT_TRA]));

   // AG preserved
   if (!memcmp(apTokens[SBX_UPDT_VALREASON], "AGP", 3))
      *(pOutbuf+OFF_AG_PRE) = 'Y';

   // Set full exemption flag
   if (*apTokens[SBX_UPDT_NONTAXCODE] > ' ')
      *(pOutbuf+OFF_FULL_EXEMPT) = 'Y';
   else
      *(pOutbuf+OFF_FULL_EXEMPT) = ' ';

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "005460045", iApnLen))
   //   iTmp = 0;
#endif

   // Owner
   try
   {
      //Sbx_MergeOwner_1(pOutbuf, apTokens[SBX_UPDT_PRIMARYOWR]);
      Sbx_MergeOwner_2(pOutbuf, apTokens[SBX_UPDT_PRIMARYOWR], NULL);
   } catch(...)
   {
      LogMsg("*** Exception occured in Sbx_MergeOwner1()");
   }

   // Mailing - Situs
   try
   {
      Sbx_UpdateAdr(pOutbuf);
   } catch(...)
   {
      LogMsg("*** Exception occured in Sbx_UpdateAdr()");
   }

   // UseCode
   iTmp = strlen(apTokens[SBX_UPDT_USECODE]);
   if (iTmp > 0)
   {
      vmemcpy(pOutbuf+OFF_USE_CO, apTokens[SBX_UPDT_USECODE], SIZ_USE_CO, iTmp);
      updateStdUse(pOutbuf+OFF_USE_STD, apTokens[SBX_UPDT_USECODE], iTmp, pOutbuf);
   } else
      memcpy(pOutbuf+OFF_USE_STD, USE_UNASGN, SIZ_USE_STD);

   // Acres
   double dTmp = atof(apTokens[SBX_UPDT_ACREAGE]);
   if (dTmp > 0.0)
   {
      lTmp = (long)(dTmp * SQFT_PER_ACRE);
      sprintf(acTmp, "%*u", SIZ_LOT_SQFT, lTmp);
      memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);

      lTmp = (long)(dTmp * 1000.0);
      sprintf(acTmp, "%*u", SIZ_LOT_ACRES, lTmp);
      memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);
   }

   // Recorded Doc
   if (*apTokens[SBX_UPDT_DOCNUM] > ' ')
      memcpy(pOutbuf+OFF_TRANSFER_DOC, apTokens[SBX_UPDT_DOCNUM], strlen(apTokens[SBX_UPDT_DOCNUM]));
   if (*apTokens[SBX_UPDT_DOCDATE] > ' ')
   {
      // Format on Lien roll 2005 as yyyy-mm-dd, LDR 2007 m/d/yyyy
      // Format on update roll on 8/3/2005 as mm/dd/yyyy
      char *pTmp = dateConversion(apTokens[SBX_UPDT_DOCDATE], acTmp, 0);
      if (pTmp)
      {
         memcpy(pOutbuf+OFF_TRANSFER_DT, acTmp, 8);
         lTmp = atol(acTmp);
         if (lTmp > lLastRecDate && lTmp < lToday)
            lLastRecDate = lTmp;
      }

      // Assessor flag - remove 6/6/2013
      //*(pOutbuf+OFF_AR_CODE1) = 'A';
   }

   // Set Prop8 flag
   if (*apTokens[SBX_UPDT_VALREASON] == 'V')
      *(pOutbuf+OFF_PROP8_FLG) = 'Y';

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "001030035", 9))
   //   iTmp = 0;
#endif

   // BldgSqft
   lTmp = atol(apTokens[SBX_UPDT_BLDGSQFT]);
   if (lTmp > 10)
   {
      sprintf(acTmp, "%*u", SIZ_BLDG_SF, lTmp);
      memcpy(pOutbuf+OFF_BLDG_SF, acTmp, SIZ_BLDG_SF);
   }

   // YrBlt
   lTmp = atol(apTokens[SBX_UPDT_YRBLT]);
   if (lTmp > 1600 && lTmp <= lToyear)
      memcpy(pOutbuf+OFF_YR_BLT, apTokens[SBX_UPDT_YRBLT], 4);

   // BedRooms
   lTmp = atol(apTokens[SBX_UPDT_BEDROOMS]);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*u", SIZ_BEDS, lTmp);
      memcpy(pOutbuf+OFF_BEDS, acTmp, SIZ_BEDS);
   }

   // BathRooms
   dTmp = atof(apTokens[SBX_UPDT_BATHROOMS]);
   if (dTmp > 0.0)
   {
      iTmp = (int)(dTmp + 0.25);
      sprintf(acTmp, "%*u", SIZ_BATH_F, iTmp);
      memcpy(pOutbuf+OFF_BATH_F, acTmp, SIZ_BATH_F);
      memcpy(pOutbuf+OFF_BATH_4Q, acTmp, SIZ_BATH_F);

      pTmp = strchr(apTokens[SBX_UPDT_BATHROOMS], '.');
      if (pTmp++)
      {
         memcpy(pOutbuf+OFF_BATH_H, " 1", 2);
         if (*pTmp <= '3')
            *(pOutbuf+OFF_BATH_1Q) = '1';
         else if (*pTmp <= '6')
            *(pOutbuf+OFF_BATH_2Q) = '1';
         else if (*pTmp <= '9')
            *(pOutbuf+OFF_BATH_3Q) = '1';
      }
   }

   return 0;
}

/********************************** Sbx_Load_Roll *****************************
 *
 *
 ******************************************************************************/

int Sbx_Load_Roll(int iSkip)
{
   char     *pTmp, acRec[MAX_RECSIZE], acBuf[MAX_RECSIZE], acRollRec[MAX_RECSIZE];
   char     acRawFile[_MAX_PATH], acOutFile[_MAX_PATH];

   HANDLE   fhIn, fhOut;

   int      iRet, iTmp, iRollUpd=0, iNewRec=0, iRetiredRec=0;
   DWORD    nBytesRead;
   DWORD    nBytesWritten;
   BOOL     bRet, bEof;
   long     lRet=0, lCnt=0;

   sprintf(acRawFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "S01");
   sprintf(acOutFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "R01");
   //sprintf(acTmpFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "TMP");

   // Rename files for processing
   if (_access(acRawFile, 0))
   {
      if (!_access(acOutFile, 0))
         rename(acOutFile, acRawFile);
      else
      {
         LogMsg("***** Error missing input file %s.  Please recheck!", acRawFile);
         return -1;
      }
   }

   // Open updated roll file
   LogMsg("Open Roll file %s", acRollFile);
   fdRoll = fopen(acRollFile, "r");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", acRollFile);
      return -2;
   }

   // Open Input file
   LogMsg("Open input file %s", acRawFile);
   fhIn = CreateFile(acRawFile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhIn == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening input file: %s\n", acRollFile);
      return -3;
   }

   // Open Output file - use tmp file so we can sort at the end to R01 file
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error creating output file: %s\n", acOutFile);
      return -4;
   }

   // Get first RollRec
   //fgets((char *)&acRollRec[0], iRollLen, fdRoll);
   pTmp = fgets((char *)&acRollRec[0], iRollLen, fdRoll);
   bEof = (pTmp ? false:true);

   // Copy skip record
   memset(acBuf, ' ', iRecLen);
   while (iSkip-- > 0)
   {
      ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);
      // Ignore record start with 99999999
      //if (memcmp(acBuf, "99999999", 8))
         bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
   }

   iNoMatch=iBadCity=iBadSuffix=0;
   iNameLen = 0;

   // Merge loop
   while (!bEof)
   {
      bRet = ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);

      // Check for EOF
      if (!bRet)
      {
         lRet = GetLastError();
         LogMsg("***** Error reading input file %s (%d) at %d", acRawFile, lRet, lCnt);
         bEof = true;   // Force exit
         break;
      }

      // EOF ?
      if (!nBytesRead)
         break;

      NextRollRec:
      iTmp = memcmp(acBuf, (char *)&acRollRec[1], iApnLen);
      if (!iTmp)
      {
         // Merge roll data
         iRet = Sbx_MergeRoll(acBuf, acRollRec, iRollLen, UPDATE_R01);
         iRollUpd++;

         // Read next roll record
         pTmp = fgets(acRollRec, iRollLen, fdRoll);

         if (!pTmp)
            bEof = true;    // Signal to stop
      } else if (iTmp > 0)       // Roll not match, new roll record?
      {
         LogMsg0("New roll record : %.*s (%d)", iApnLen, (char *)&acRollRec[1], lCnt);

         // Create new R01 record
         iRet = Sbx_MergeRoll(acRec, acRollRec, iRollLen, CREATE_R01);
         iNewRec++;

         bRet = WriteFile(fhOut, acRec, iRecLen, &nBytesWritten, NULL);
         lCnt++;

         // Get next roll record
         pTmp = fgets(acRollRec, iRollLen, fdRoll);

         if (!pTmp)
            bEof = true;    // Signal to stop
         else
            goto NextRollRec;
      } else
      {
         // Record may be retired
         LogMsg0("Roll not match (retired record?) : R01->%.*s < Roll->%.*s (%d)", iApnLen, acBuf, iApnLen, &acRollRec[1], lCnt);
         iRetiredRec++;
         continue;
      }

      if (iRet >= 0)
         bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);

      if (!bRet)
      {
         LogMsg("***** Error occurs: %d\n", GetLastError());
         lRet = WRITE_ERR;
         break;
      }
   }

   // If there are more new records, add them
   while (!bEof)
   {
      LogMsg0("*** New roll record : %.*s (%d)", iApnLen, (char *)&acRollRec[1], lCnt);

      if (isdigit(acRollRec[1]))
      {
         // Create new R01 record
         iRet = Sbx_MergeRoll(acRec, acRollRec, iRollLen, CREATE_R01);
         bRet = WriteFile(fhOut, acRec, iRecLen, &nBytesWritten, NULL);
         lCnt++;
         iNewRec++;
      }

      // Get next roll record
      pTmp = fgets(acRollRec, iRollLen, fdRoll);

      if (!pTmp)
         bEof = true;    // Signal to stop
   }

   while (bRet)
   {
      bRet = ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);
      if (nBytesRead != iRecLen)
         break;

      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);

      if (!bRet)
      {
         LogMsg("***** Error occurs: %d\n", GetLastError());
         lRet = WRITE_ERR;
         break;
      }
   }
      
   // Close files
   if (fdRoll)
      fclose(fdRoll);
   if (fhOut)
      CloseHandle(fhOut);
   if (fhIn)
      CloseHandle(fhIn);

   LogMsgD("\nTotal output records:       %u", lCnt);
   LogMsg("Total new records:          %u", iNewRec);
   LogMsg("Total retired records:      %u", iRetiredRec);
   LogMsg("Total bad-city records:     %u", iBadCity);
   LogMsg("Total bad-suffix records:   %u", iBadSuffix);
   LogMsg("Max name length:            %u", iNameLen);
   LogMsg("Longest name:               %s\n", acLongestName);

   lRecCnt = lCnt;
   return lRet;
}

/********************************* Sbx_Load_LDR *****************************
 *
 *
 ****************************************************************************/

int Sbx_Load_LDR(int iFirstRec /* 1=create header rec */)
{
   char     *pTmp, acBuf[MAX_RECSIZE], acRollRec[MAX_RECSIZE];
   char     acOutFile[_MAX_PATH], acTmpFile[_MAX_PATH], acTmp[256];

   HANDLE   fhOut;

   int      iRet;
   DWORD    nBytesWritten;
   BOOL     bRet;
   long     lRet=0, lCnt=0;

   sprintf(acTmpFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "TMP");
   sprintf(acOutFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "R01");

   // Open roll file
   LogMsg("Open Roll file %s", acRollFile);
   fdRoll = fopen(acRollFile, "r");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", acRollFile);
      return 2;
   }

   // Open Output file
   LogMsg("Open output file %s", acTmpFile);
   fhOut = CreateFile(acTmpFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error creating output file: %s\n", acTmpFile);
      return 4;
   }

   // Output first header record
   if (iFirstRec > 0)
   {
      memset(acBuf, '9', iRecLen);
      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
   }

   // Get first RollRec - skip rec header
   pTmp = fgets((char *)&acRollRec[0], MAX_RECSIZE, fdRoll);
   iRet = myGetStrEQ(acRollRec, MAX_RECSIZE, fdRoll);

   // Init variables
   iNoMatch=iBadCity=iBadSuffix=0;

   // Merge loop
   while (iRet > 0)
   {
      lLDRRecCount++;

      // Create new R01 record
      iRet = Sbx_CreateLdrRec(acBuf, acRollRec);
      if (!iRet)
      {
         bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
         if (!(++lCnt % 1000))
            printf("\r%u", lCnt);

         if (!bRet)
         {
            LogMsg("***** Error writing to output file at record %d\n", lCnt);
            lRet = WRITE_ERR;
            break;
         }
      }

      // Get next roll record
      iRet = myGetStrEQ(acRollRec, MAX_RECSIZE, fdRoll);
   }

   // Close files
   if (fdRoll)
      fclose(fdRoll);
   if (fhOut)
      CloseHandle(fhOut);

   // Sort output file
   sprintf(acTmp, "S(1,14,C,A) F(FIX,%d) Bypass(%d,R)", iRecLen, iRecLen);
   iRet = sortFile(acTmpFile, acOutFile, acTmp);

   LogMsg("Total input records:        %u", lLDRRecCount);
   LogMsg("Total output records:       %u", lCnt);
   LogMsg("Total bad-city records:     %u", iBadCity);
   LogMsg("Total bad-suffix records:   %u", iBadSuffix);
   LogMsg("Last recording date:        %u", lLastRecDate);

   lRecCnt = lCnt;
   return lRet;
}

/******************************** Sbx_Load_LDR1 *****************************
 *
 * Load LDR file "CertifiedSecuredRollWithPropChar.txt"
 * Delim=TAB
 *
 ****************************************************************************/

int Sbx_Load_LDR1(int iFirstRec /* 1=create header rec */)
{
   char     *pTmp, acBuf[MAX_RECSIZE], acRollRec[MAX_RECSIZE];
   char     acOutFile[_MAX_PATH], acTmpFile[_MAX_PATH], acTmp[256];

   HANDLE   fhOut;

   int      iRet;
   DWORD    nBytesWritten;
   BOOL     bRet;
   long     lRet=0, lCnt=0;

   sprintf(acTmpFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "TMP");
   sprintf(acOutFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "R01");

   // Open roll file
   LogMsg("Open Roll file %s", acRollFile);
   fdRoll = fopen(acRollFile, "r");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", acRollFile);
      return -2;
   }

   // Open Output file
   LogMsg("Open output file %s", acTmpFile);
   fhOut = CreateFile(acTmpFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error creating output file: %s\n", acTmpFile);
      return -4;
   }

   // Output first header record
   if (iFirstRec > 0)
   {
      memset(acBuf, '9', iRecLen);
      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
   }

   // Get first RollRec - skip rec header
   while (iHdrRows-- > 0)
      pTmp = fgets((char *)&acRollRec[0], MAX_RECSIZE, fdRoll);
   pTmp = fgets((char *)&acRollRec[0], MAX_RECSIZE, fdRoll);

   // Init variables
   iNoMatch=iBadCity=iBadSuffix=0;

   // Merge loop
   while (pTmp)
   {
      lLDRRecCount++;

      // Create new R01 record
      iRet = Sbx_CreateLdrRec1(acBuf, acRollRec);
      if (!iRet)
      {
         bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
         if (!(++lCnt % 1000))
            printf("\r%u", lCnt);

         if (!bRet)
         {
            LogMsg("***** Error writing to output file at record %d\n", lCnt);
            lRet = WRITE_ERR;
            break;
         }
      }

      // Get next roll record
      pTmp = fgets(acRollRec, MAX_RECSIZE, fdRoll);
   }

   // Close files
   if (fdRoll)
      fclose(fdRoll);
   if (fhOut)
      CloseHandle(fhOut);

   // Sort output file
   sprintf(acTmp, "S(1,14,C,A) F(FIX,%d) Bypass(%d,R)", iRecLen, iRecLen);
   iRet = sortFile(acTmpFile, acOutFile, acTmp);

   LogMsg("Total input records:        %u", lLDRRecCount);
   LogMsg("Total output records:       %u", lCnt);
   LogMsg("Total bad-city records:     %u", iBadCity);
   LogMsg("Total bad-suffix records:   %u", iBadSuffix);
   LogMsg("Last recording date:        %u", lLastRecDate);

   lRecCnt = lCnt;
   return lRet;
}

/******************************** Sbx_Load_LDR2 *****************************
 *
 * Load LDR file "CertifiedSecuredRollWithPropChar.txt" 2016
 * Delim=comma
 *
 ****************************************************************************/

int Sbx_Load_LDR2(int iFirstRec /* 1=create header rec */)
{
   char     *pTmp, acBuf[MAX_RECSIZE], acRollRec[MAX_RECSIZE];
   char     acOutFile[_MAX_PATH], acTmpFile[_MAX_PATH], acTmp[256];

   HANDLE   fhOut;

   int      iRet;
   DWORD    nBytesWritten;
   BOOL     bRet;
   long     lRet=0, lCnt=0;

   sprintf(acTmpFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "TMP");
   sprintf(acOutFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "R01");

   // Open roll file
   LogMsg("Open Roll file %s", acRollFile);
   fdRoll = fopen(acRollFile, "r");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", acRollFile);
      return -2;
   }

   // Open Output file
   LogMsg("Open output file %s", acTmpFile);
   fhOut = CreateFile(acTmpFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error creating output file: %s\n", acTmpFile);
      return -4;
   }

   // Output first header record
   if (iFirstRec > 0)
   {
      memset(acBuf, '9', iRecLen);
      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
   }

   // Get first RollRec - skip rec header
   while (iHdrRows-- > 0)
      pTmp = fgets((char *)&acRollRec[0], MAX_RECSIZE, fdRoll);
   pTmp = fgets((char *)&acRollRec[0], MAX_RECSIZE, fdRoll);

   // Init variables
   iNoMatch=iBadCity=iBadSuffix=0;

   // Merge loop
   while (pTmp)
   {
      // Create new R01 record
      iRet = Sbx_CreateLdrRec2(acBuf, acRollRec);
      if (!iRet)
      {
         lLDRRecCount++;
         bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);

         if (!bRet)
         {
            LogMsg("***** Error writing to output file at record %d\n", lCnt);
            lRet = WRITE_ERR;
            break;
         }
      }

      // Get next roll record
      pTmp = fgets(acRollRec, MAX_RECSIZE, fdRoll);
      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdRoll)
      fclose(fdRoll);
   if (fhOut)
      CloseHandle(fhOut);

   // Sort output file
   sprintf(acTmp, "S(1,14,C,A) F(FIX,%d) B(%d,R) DUPO(B2048,1,12)", iRecLen, iRecLen);

   iRet = sortFile(acTmpFile, acOutFile, acTmp);
   if (iRet > 0)
      lLDRRecCount = iRet;

   LogMsg("Total input records:        %u", lCnt);
   LogMsg("Total output records:       %u", lLDRRecCount);
   LogMsg("Total bad-city records:     %u", iBadCity);
   LogMsg("Total bad-suffix records:   %u", iBadSuffix);
   LogMsg("Last recording date:        %u", lLastRecDate);

   lRecCnt = lLDRRecCount;
   return lRet;
}

/******************************** Sbx_Load_LDR3 *****************************
 *
 * Load LDR file "2019 Public Roll.txt"
 * Delim=tab
 *
 ****************************************************************************/

//int Sbx_Load_LDR3(int iFirstRec /* 1=create header rec */)
//{
//   char     *pTmp, acBuf[MAX_RECSIZE], acRollRec[MAX_RECSIZE];
//   char     acOutFile[_MAX_PATH], acTmpFile[_MAX_PATH], acTmp[256];
//
//   HANDLE   fhOut;
//
//   int      iRet;
//   DWORD    nBytesWritten;
//   BOOL     bRet;
//   long     lRet=0, lCnt=0;
//
//   sprintf(acTmpFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "TMP");
//   sprintf(acOutFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "R01");
//
//   // Open roll file
//   LogMsg("Open Roll file %s", acRollFile);
//   fdRoll = fopen(acRollFile, "r");
//   if (fdRoll == NULL)
//   {
//      LogMsg("***** Error opening roll file: %s\n", acRollFile);
//      return -2;
//   }
//
//   // Open Output file
//   LogMsg("Open output file %s", acTmpFile);
//   fhOut = CreateFile(acTmpFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
//          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);
//
//   if (fhOut == INVALID_HANDLE_VALUE)
//   {
//      LogMsg("***** Error creating output file: %s\n", acTmpFile);
//      return -4;
//   }
//
//   // Output first header record
//   if (iFirstRec > 0)
//   {
//      memset(acBuf, '9', iRecLen);
//      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
//   }
//
//   // Get first RollRec - skip rec header
//   pTmp = fgets((char *)&acRollRec[0], MAX_RECSIZE, fdRoll);
//   pTmp = fgets((char *)&acRollRec[0], MAX_RECSIZE, fdRoll);
//
//   // Init variables
//   iNoMatch=iBadCity=iBadSuffix=0;
//
//   // Merge loop
//   while (pTmp)
//   {
//      lLDRRecCount++;
//
//      // Create new R01 record
//      iRet = Sbx_CreateLdrRec3(acBuf, acRollRec);
//      if (!iRet)
//      {
//         bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
//         if (!(++lCnt % 1000))
//            printf("\r%u", lCnt);
//
//         if (!bRet)
//         {
//            LogMsg("***** Error writing to output file at record %d\n", lCnt);
//            lRet = WRITE_ERR;
//            break;
//         }
//      }
//
//      // Get next roll record
//      pTmp = fgets(acRollRec, MAX_RECSIZE, fdRoll);
//   }
//
//   // Close files
//   if (fdRoll)
//      fclose(fdRoll);
//   if (fhOut)
//      CloseHandle(fhOut);
//
//   // Sort output file
//   sprintf(acTmp, "S(1,14,C,A) F(FIX,%d) B(%d,R) DUPO(B2048,1,12)", iRecLen, iRecLen);
//
//   iRet = sortFile(acTmpFile, acOutFile, acTmp);
//
//   LogMsg("Total input records:        %u", lLDRRecCount);
//   LogMsg("Total output records:       %u", lCnt);
//   LogMsg("Total bad-city records:     %u", iBadCity);
//   LogMsg("Total bad-suffix records:   %u", iBadSuffix);
//   LogMsg("Last recording date:        %u", lLastRecDate);
//
//   lRecCnt = lCnt;
//   return lRet;
//}

 /********************************* Sbx_UpdCurRec *****************************
 *
 * Return 0 if successful, >0 if error
 *
 *****************************************************************************/

int Sbx_UpdCurRec(char *pOutbuf, char *pRollRec)
{
   char     acTmp[256];
   long     lTmp;
   int      iRet=0;

   // Replace tab char with 0
   iRet = ParseStringNQ(pRollRec, ',', MAX_FLD_TOKEN, apTokens);

   // AG preserved
   if (!memcmp(apTokens[SBX_UPDT_VALREASON], "AGP", 3))
      *(pOutbuf+OFF_AG_PRE) = 'Y';

   /* Update transaction doc only  07/11/2010

   // Owner
   try
   {
      Sbx_MergeOwner(pOutbuf, apTokens[SBX_UPDT_PRIMARYOWR]);
   } catch(...)
   {
      LogMsg("*** Exception occured in Sbx_MergeOwner()");
   }

   // Mailing - Situs
   try
   {
      Sbx_UpdateAdr(pOutbuf);
   } catch(...)
   {
      LogMsg("*** Exception occured in Sbx_UpdateAdr()");
   }

   // UseCode
   iTmp = strlen(apTokens[SBX_UPDT_USECODE]);
   if (iTmp > 0)
   {
      if (iTmp > SIZ_USE_CO)
         iTmp = SIZ_USE_CO;
      memcpy(pOutbuf+OFF_USE_CO, apTokens[SBX_UPDT_USECODE], iTmp);
      memset(pOutbuf+OFF_USE_STD, ' ', SIZ_USE_STD);
      updateStdUse(pOutbuf+OFF_USE_STD, apTokens[SBX_UPDT_USECODE], iTmp);
   }

   // Acres
   double dTmp = atof(apTokens[SBX_UPDT_ACREAGE]);
   if (dTmp > 0.0)
   {
      lTmp = dTmp * SQFT_PER_ACRE;
      sprintf(acTmp, "%*u", SIZ_LOT_SQFT, lTmp);
      memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);

      lTmp = dTmp * 1000;
      sprintf(acTmp, "%*u", SIZ_LOT_ACRES, lTmp);
      memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);
   }

   */

   // Recorded Doc
   if (*apTokens[SBX_UPDT_DOCNUM] > ' ')
   {
      memset(pOutbuf+OFF_TRANSFER_DOC, ' ', SIZ_TRANSFER_DOC);
      memcpy(pOutbuf+OFF_TRANSFER_DOC, apTokens[SBX_UPDT_DOCNUM], strlen(apTokens[SBX_UPDT_DOCNUM]));
   }
   if (*apTokens[SBX_UPDT_DOCDATE] > ' ')
   {
      // Format on Lien roll 2005 as yyyy-mm-dd, LDR 2007 m/d/yyyy
      // Format on update roll on 8/3/2005 as mm/dd/yyyy
      // char *pTmp = dateConversion(apTokens[SBX_UPDT_DOCDATE], acTmp, 0);
      // 2013 format yyyy-mm-dd
      char *pTmp;

      if (apTokens[SBX_UPDT_DOCDATE][4] == '-')
         pTmp = dateConversion(apTokens[SBX_UPDT_DOCDATE], acTmp, YYYY_MM_DD);
      else
         pTmp = dateConversion(apTokens[SBX_UPDT_DOCDATE], acTmp, 0);

      if (pTmp)
      {
         memcpy(pOutbuf+OFF_TRANSFER_DT, acTmp, 8);
         lTmp = atol(acTmp);
         if (lTmp > lLastRecDate && lTmp < lToday)
            lLastRecDate = lTmp;
      }

      // Assessor flag - transfer data is from assessor
      *(pOutbuf+OFF_AR_CODE1) = 'A';
   }
   return 0;
}

/******************************** Sbx_UpdCurRoll ******************************
 *
 * Update LDR with current ownership, no new record added
*
 ******************************************************************************/

int Sbx_UpdCurRoll(char *pUpdFile, int iSkip)
{
   char     *pTmp, acBuf[MAX_RECSIZE], acRollRec[MAX_RECSIZE];
   char     acRawFile[_MAX_PATH], acOutFile[_MAX_PATH];

   HANDLE   fhIn, fhOut;

   int      iRet, iTmp, iRollUpd=0, iNewRec=0, iRollDel=0;
   DWORD    nBytesRead;
   DWORD    nBytesWritten;
   BOOL     bRet, bEof;
   long     lRet=0, lCnt=0;

   sprintf(acRawFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "R01");
   sprintf(acOutFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "TMP");

   // Open updated roll file
   LogMsg("Open Roll file %s", pUpdFile);
   fdRoll = fopen(pUpdFile, "r");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", pUpdFile);
      return 2;
   }

   // Open Input file
   LogMsg("Open input file %s", acRawFile);
   fhIn = CreateFile(acRawFile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhIn == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening input file: %s\n", acRollFile);
      return 3;
   }

   // Open Output file - use tmp file so we can sort at the end to R01 file
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error creating output file: %s\n", acOutFile);
      return 4;
   }

   // Get first RollRec
   pTmp = fgets((char *)&acRollRec[0], iRollLen, fdRoll);
   bEof = (pTmp ? false:true);

   // Copy skip record
   memset(acBuf, ' ', iRecLen);
   while (iSkip-- > 0)
   {
      ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);
      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
   }

   iNoMatch=iBadCity=iBadSuffix=0;

   // Merge loop
   while (!bEof)
   {
      bRet = ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);

      // Check for EOF
      if (!bRet)
      {
         LogMsg("***** Error reading input file %s (%f)", acRawFile, GetLastError());
         break;
      }

      // EOF ?
      if (iRecLen != nBytesRead)
         break;

      NextRollRec:
      iTmp = memcmp(acBuf, (char *)&acRollRec[1], iApnLen);
      if (!iTmp)
      {
         // Merge roll data
         iRet = Sbx_UpdCurRec(acBuf, acRollRec);
         iRollUpd++;

         // Read next roll record
         pTmp = fgets(acRollRec, iRollLen, fdRoll);

         if (!pTmp)
         {
            bEof = true;    // Signal to stop
            bRet = ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);
         }
      } else if (iTmp > 0)       // Roll not match, new roll record?
      {
         // Get next roll record
         pTmp = fgets(acRollRec, iRollLen, fdRoll);
         iNewRec++;

         if (!pTmp)
            bEof = true;    // Signal to stop
         else
            goto NextRollRec;
      } else
         iRollDel++;

      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);

      if (!bRet)
      {
         LogMsg("***** Error occurs: %d\n", GetLastError());
         lRet = WRITE_ERR;
         break;
      }
   }

   // Copy the rest
   while ((iRecLen == nBytesRead))
   {
      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
      lCnt++;

      bRet = ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);
   }

   // Close files
   if (fdRoll)
      fclose(fdRoll);
   if (fhOut)
      CloseHandle(fhOut);
   if (fhIn)
      CloseHandle(fhIn);

   if (!lRet)
   {
      remove(acRawFile);
      rename(acOutFile, acRawFile);
   }

   LogMsg("Total output records:       %u", lCnt);
   LogMsg("Total updated records:      %u", iRollUpd);
   LogMsg("Total new records:          %u", iNewRec);
   LogMsg("Total retired records:      %u", iRollDel);
   LogMsg("Total bad-city records:     %u", iBadCity);
   LogMsg("Total bad-suffix records:   %u", iBadSuffix);

   lRecCnt = lCnt;
   return lRet;
}

/******************************* Sbx_CreateLienRec ***************************
 *
 * Return 0 if successful, >0 if error
 *
 *****************************************************************************/

int Sbx_CreateLienRec(char *pOutbuf, char *pRollRec)
{
   char     acTmp[256];
   long     lTmp, iTmp;
   LIENEXTR *pLienRec = (LIENEXTR *)pOutbuf;

   // Parse input string
   if (cDelim == ',')
      iTmp = ParseStringNQ(pRollRec, cDelim, MAX_FLD_TOKEN, apTokens);
   else
      iTmp = ParseStringIQ(pRollRec, cDelim, MAX_FLD_TOKEN, apTokens);

   if (iTmp < SBX_ROLL_HOXMPAMT)
   {
      LogMsg("***** Error: bad input record for APN=%.10s", pRollRec);
      return -1;
   }

   // Clear output buffer
   memset(pOutbuf, ' ', sizeof(LIENEXTR));

   // Start copying data
   memcpy(pLienRec->acApn, apTokens[SBX_ROLL_APN], strlen(apTokens[SBX_ROLL_APN]));
   // TRA
   lTmp = atol(apTokens[SBX_ROLL_TRA]);
   if (lTmp > 0)
   {
      iTmp = sprintf(acTmp, "%.6d", lTmp);
      memcpy(pLienRec->acTRA, acTmp, iTmp);
   }

   // Year assessed
   memcpy(pLienRec->acYear, myCounty.acYearAssd, 4);

   // HO Exempt
   lTmp = atoi(apTokens[SBX_ROLL_HOXMPAMT]);
   if (!lTmp)
      lTmp = atol(apTokens[SBX_ROLL_OTHXMPAMT]);
   if (lTmp > 0)
   {
      pLienRec->acHO[0] = '1';      // 'Y'
      iTmp = sprintf(acTmp, "%*u", sizeof(pLienRec->acExAmt), lTmp);
      memcpy(pLienRec->acExAmt, acTmp, iTmp);
   } else
      pLienRec->acHO[0] = '2';      // 'N'

   // Land
   long lLand = atoi(apTokens[SBX_ROLL_LAND]);
   if (lLand > 0)
   {
      iTmp = sprintf(acTmp, "%*u", sizeof(pLienRec->acLand), lLand);
      memcpy(pLienRec->acLand, acTmp, iTmp);
   }

   // Improve
   long lImpr = atoi(apTokens[SBX_ROLL_STRUCTIMP]);
   if (lImpr > 0)
   {
      iTmp = sprintf(acTmp, "%*u", sizeof(pLienRec->acImpr), lImpr);
      memcpy(pLienRec->acImpr, acTmp, iTmp);
   }

   // Other value: TradeFixture, LivingImp, PPDeclared, PPUnitVal, PPMH
   long lLivImpr= atoi(apTokens[SBX_ROLL_LIVINGIMP]);
   long lFixture= atoi(apTokens[SBX_ROLL_TRADEFIXTURE]);
   long lPers   = atoi(apTokens[SBX_ROLL_PPDECLARED]);
   long lPP_MH  = atoi(apTokens[SBX_ROLL_PPMOBILHOME]);
   long lUnitVal= atoi(apTokens[SBX_ROLL_PPUNITVALUE]);
   lTmp = lPers + lLivImpr + lFixture + lPP_MH + lUnitVal;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*u", SIZ_LIEN_FIXT, lTmp);
      memcpy(pLienRec->acOther, acTmp, SIZ_LIEN_FIXT);

      if (lFixture > 0)
      {
         iTmp = sprintf(acTmp, "%*u", sizeof(pLienRec->acME_Val), lFixture);
         memcpy(pLienRec->acME_Val, acTmp, iTmp);
      }

      lPers += lUnitVal;
      if (lPers > 0)
      {
         iTmp = sprintf(acTmp, "%*u", sizeof(pLienRec->acPP_Val), lPers);
         memcpy(pLienRec->acPP_Val, acTmp, iTmp);
      }
      if (lPP_MH > 0)
      {
         iTmp = sprintf(acTmp, "%*u", sizeof(pLienRec->extra.Sbx.PP_MH), lPP_MH);
         memcpy(pLienRec->extra.Sbx.PP_MH, acTmp, iTmp);
      }
      if (lLivImpr > 0)
      {
         iTmp = sprintf(acTmp, "%*u", sizeof(pLienRec->extra.Sbx.LivImpr), lLivImpr);
         memcpy(pLienRec->extra.Sbx.LivImpr, acTmp, iTmp);
      }

      // This should be PP_VAL 20091001 -sn
      //if (lUnitVal > 0)
      //{
      //   iTmp = sprintf(acTmp, "%*u", sizeof(pLienRec->extra.Sbx.UnitVal), lUnitVal);
      //   memcpy(pLienRec->extra.Sbx.UnitVal, acTmp, iTmp);
      //}
   }

   // Gross total
   lTmp += lLand+lImpr;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*u", SIZ_LIEN_GROSS, lTmp);
      memcpy(pLienRec->acGross, acTmp, SIZ_LIEN_GROSS);
   }

   // Ratio
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*u", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
      memcpy(pLienRec->acRatio, acTmp, SIZ_RATIO);
   }

   // TRA
   lTmp = atol(apTokens[SBX_ROLL_TRA]);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%.6d", lTmp);
      memcpy(pLienRec->acTRA, acTmp, 6);
   }

   // Set full exemption flag
   if (*apTokens[SBX_ROLL_NONTAXCODE] > ' ')
   {
      pLienRec->SpclFlag = LX_FULLEXE_FLG;
      memcpy(pLienRec->extra.Sbx.NonTaxCode, apTokens[SBX_ROLL_NONTAXCODE], SIZ_LIEN_EXECODE);
   }

   // Prop8
   if (*apTokens[SBX_ROLL_VALREASON] == 'V')
      pLienRec->SpclFlag |= LX_PROP8_FLG;

   pLienRec->LF[0] = '\n';
   pLienRec->LF[1] = 0;
   return 0;
}

int Sbx_CreateLienRec2(char *pOutbuf, char *pRollRec)
{
   char     acTmp[256];
   long     lTmp, iTmp;
   LIENEXTR *pLienRec = (LIENEXTR *)pOutbuf;

   // Parse input string
   iTmp = ParseStringEx(pRollRec, cLdrSep, '^', MAX_FLD_TOKEN, apTokens);
   if (iTmp < SBX_ROLL_HOXMPAMT)
   {
      LogMsg("***** Error: bad input record for APN=%.10s", pRollRec);
      return -1;
   }

   // Clear output buffer
   memset(pOutbuf, ' ', sizeof(LIENEXTR));

   // Start copying data
   memcpy(pLienRec->acApn, apTokens[SBX_ROLL_APN], strlen(apTokens[SBX_ROLL_APN]));
   // TRA
   lTmp = atol(apTokens[SBX_ROLL_TRA]);
   if (lTmp > 0)
   {
      iTmp = sprintf(acTmp, "%.6d", lTmp);
      memcpy(pLienRec->acTRA, acTmp, iTmp);
   }

   // Year assessed
   memcpy(pLienRec->acYear, myCounty.acYearAssd, 4);

   // HO Exempt
   int lHOExe = atoi(apTokens[SBX_ROLL_HOXMPAMT]);
   int lOthExe= atol(apTokens[SBX_ROLL_OTHXMPAMT]);
   if (lHOExe > 0)
      pLienRec->acHO[0] = '1';      // 'Y'
   else
      pLienRec->acHO[0] = '2';      // 'N'

   // Total exempt
   lTmp = lHOExe + lOthExe;
   if (lTmp > 0)
   {
      iTmp = sprintf(acTmp, "%d", lTmp);
      memcpy(pLienRec->acExAmt, acTmp, iTmp);
      if (lOthExe > 0)
      {
         iTmp = sprintf(acTmp, "%d", lTmp);
         memcpy(pLienRec->extra.Sbx.Oth_Exe, acTmp, iTmp);
      }
   }

   // Exemption code
   if (*apTokens[SBX_ROLL_EXEMPTCATGRY] > ' ')
      vmemcpy(pLienRec->extra.Sbx.Exe_Code1, apTokens[SBX_ROLL_EXEMPTCATGRY], SIZ_LIEN_EXECODE);
   else if (lHOExe > 0)
      memcpy(pLienRec->extra.Sbx.Exe_Code1, "HO", SIZ_LIEN_EXECODE);

   // Set full exemption flag
   if (*apTokens[SBX_ROLL_NONTAXCODE] > ' ')
   {
      pLienRec->SpclFlag = LX_FULLEXE_FLG;
      memcpy(pLienRec->extra.Sbx.NonTaxCode, apTokens[SBX_ROLL_NONTAXCODE], SIZ_LIEN_EXECODE);
   }

   // Prop8
   if (*apTokens[SBX_ROLL_VALREASON] == 'V')
      pLienRec->SpclFlag |= LX_PROP8_FLG;

   // Land
   long lLand = atoi(apTokens[SBX_ROLL_LAND]);
   if (lLand > 0)
   {
      iTmp = sprintf(acTmp, "%*u", sizeof(pLienRec->acLand), lLand);
      memcpy(pLienRec->acLand, acTmp, iTmp);
   }

   // Improve
   long lImpr = atoi(apTokens[SBX_ROLL_STRUCTIMP]);
   if (lImpr > 0)
   {
      iTmp = sprintf(acTmp, "%*u", sizeof(pLienRec->acImpr), lImpr);
      memcpy(pLienRec->acImpr, acTmp, iTmp);
   }

   // Other value: TradeFixture, LivingImp, PPDeclared, PPUnitVal, PPMH
   long lLivImpr= atoi(apTokens[SBX_ROLL_LIVINGIMP]);
   long lFixture= atoi(apTokens[SBX_ROLL_TRADEFIXTURE]);
   long lPers   = atoi(apTokens[SBX_ROLL_PPDECLARED]);
   long lPP_MH  = atoi(apTokens[SBX_ROLL_PPMOBILHOME]);
   long lUnitVal= atoi(apTokens[SBX_ROLL_PPUNITVALUE]);
   LONGLONG lGross = lPers + lLivImpr + lFixture + lPP_MH + lUnitVal;
   if (lGross > 0)
   {
      sprintf(acTmp, "%*u", SIZ_LIEN_FIXT, lGross);
      memcpy(pLienRec->acOther, acTmp, SIZ_LIEN_FIXT);

      if (lFixture > 0)
      {
         iTmp = sprintf(acTmp, "%*u", sizeof(pLienRec->acME_Val), lFixture);
         memcpy(pLienRec->acME_Val, acTmp, iTmp);
      }

      lPers += lUnitVal;
      if (lPers > 0)
      {
         iTmp = sprintf(acTmp, "%*u", sizeof(pLienRec->acPP_Val), lPers);
         memcpy(pLienRec->acPP_Val, acTmp, iTmp);
      }
      if (lPP_MH > 0)
      {
         iTmp = sprintf(acTmp, "%*u", sizeof(pLienRec->extra.Sbx.PP_MH), lPP_MH);
         memcpy(pLienRec->extra.Sbx.PP_MH, acTmp, iTmp);
      }
      if (lLivImpr > 0)
      {
         iTmp = sprintf(acTmp, "%*u", sizeof(pLienRec->extra.Sbx.LivImpr), lLivImpr);
         memcpy(pLienRec->extra.Sbx.LivImpr, acTmp, iTmp);
      }

      // This should be PP_VAL 20091001 -sn
      //if (lUnitVal > 0)
      //{
      //   iTmp = sprintf(acTmp, "%*u", sizeof(pLienRec->extra.Sbx.UnitVal), lUnitVal);
      //   memcpy(pLienRec->extra.Sbx.UnitVal, acTmp, iTmp);
      //}
   }

   // Gross total
   lGross += lLand+lImpr;
   if (lGross > 0)
   {
      sprintf(acTmp, "%*u", SIZ_LIEN_GROSS, lGross);
      memcpy(pLienRec->acGross, acTmp, SIZ_LIEN_GROSS);
   }

   // Ratio
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*u", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
      memcpy(pLienRec->acRatio, acTmp, SIZ_RATIO);
   }

   // TRA
   lTmp = atol(apTokens[SBX_ROLL_TRA]);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%.6d", lTmp);
      memcpy(pLienRec->acTRA, acTmp, 6);
   }

   pLienRec->LF[0] = '\n';
   pLienRec->LF[1] = 0;
   return 0;
}

//int Sbx_CreateLienRec3(char *pOutbuf, char *pRollRec)
//{
//   char     acTmp[256];
//   long     lTmp, iTmp;
//   LIENEXTR *pLienRec = (LIENEXTR *)pOutbuf;
//
//   // Parse input string
//   iTmp = ParseStringIQ(pRollRec, 9, MAX_FLD_TOKEN, apTokens);
//   if (iTmp < SBX_2019_FLDS)
//   {
//      LogMsg("***** Error: bad input record for APN=%.10s", pRollRec);
//      return -1;
//   }
//
//   // Clear output buffer
//   memset(pOutbuf, ' ', sizeof(LIENEXTR));
//
//   // Start copying data
//   memcpy(pLienRec->acApn, apTokens[SBX_2019_APN], strlen(apTokens[SBX_2019_APN]));
//   // TRA
//   lTmp = atol(apTokens[SBX_2019_TRA]);
//   if (lTmp > 0)
//   {
//      iTmp = sprintf(acTmp, "%.6d", lTmp);
//      memcpy(pLienRec->acTRA, acTmp, iTmp);
//   }
//
//   // Year assessed
//   memcpy(pLienRec->acYear, myCounty.acYearAssd, 4);
//
//   // HO Exempt
//   lTmp = atoi(apTokens[SBX_2019_HOMEOWEX]);
//   //if (!lTmp)
//   //   lTmp = atol(apTokens[SBX_2019_OTHXMPAMT]);
//   if (lTmp > 0)
//   {
//      pLienRec->acHO[0] = '1';      // 'Y'
//      iTmp = sprintf(acTmp, "%*u", sizeof(pLienRec->acExAmt), lTmp);
//      memcpy(pLienRec->acExAmt, acTmp, iTmp);
//   } else
//      pLienRec->acHO[0] = '2';      // 'N'
//
//   // Land
//   long lLand = atoi(apTokens[SBX_2019_LANDVALUE]);
//   if (lLand > 0)
//   {
//      iTmp = sprintf(acTmp, "%*u", sizeof(pLienRec->acLand), lLand);
//      memcpy(pLienRec->acLand, acTmp, iTmp);
//   }
//
//   // Improve
//   long lImpr = atoi(apTokens[SBX_2019_STRIMPR]);
//   if (lImpr > 0)
//   {
//      iTmp = sprintf(acTmp, "%*u", sizeof(pLienRec->acImpr), lImpr);
//      memcpy(pLienRec->acImpr, acTmp, iTmp);
//   }
//
//   // Other value: TradeFixture, LivingImp, PPDeclared, PPUnitVal, PPMH
//   long lLivImpr= atoi(apTokens[SBX_2019_LIVIMPR]);
//   long lFixture= atoi(apTokens[SBX_2019_TRADEFIX]);
//   long lPers   = atoi(apTokens[SBX_2019_PERPROPDEC]);
//   long lPP_MH  = atoi(apTokens[SBX_2019_MOBILEHOME]);
//   long lUnitVal= atoi(apTokens[SBX_2019_PERSPROPUN]);
//   lTmp = lPers + lLivImpr + lFixture + lPP_MH + lUnitVal;
//   if (lTmp > 0)
//   {
//      sprintf(acTmp, "%*u", SIZ_LIEN_FIXT, lTmp);
//      memcpy(pLienRec->acOther, acTmp, SIZ_LIEN_FIXT);
//
//      if (lFixture > 0)
//      {
//         iTmp = sprintf(acTmp, "%*u", sizeof(pLienRec->acME_Val), lFixture);
//         memcpy(pLienRec->acME_Val, acTmp, iTmp);
//      }
//
//      lPers += lUnitVal;
//      if (lPers > 0)
//      {
//         iTmp = sprintf(acTmp, "%*u", sizeof(pLienRec->acPP_Val), lPers);
//         memcpy(pLienRec->acPP_Val, acTmp, iTmp);
//      }
//      if (lPP_MH > 0)
//      {
//         iTmp = sprintf(acTmp, "%*u", sizeof(pLienRec->extra.Sbx.PP_MH), lPP_MH);
//         memcpy(pLienRec->extra.Sbx.PP_MH, acTmp, iTmp);
//      }
//      if (lLivImpr > 0)
//      {
//         iTmp = sprintf(acTmp, "%*u", sizeof(pLienRec->extra.Sbx.LivImpr), lLivImpr);
//         memcpy(pLienRec->extra.Sbx.LivImpr, acTmp, iTmp);
//      }
//
//      // This should be PP_VAL 20091001 -sn
//      //if (lUnitVal > 0)
//      //{
//      //   iTmp = sprintf(acTmp, "%*u", sizeof(pLienRec->extra.Sbx.UnitVal), lUnitVal);
//      //   memcpy(pLienRec->extra.Sbx.UnitVal, acTmp, iTmp);
//      //}
//   }
//
//   // Gross total
//   lTmp += lLand+lImpr;
//   if (lTmp > 0)
//   {
//      sprintf(acTmp, "%*u", SIZ_LIEN_GROSS, lTmp);
//      memcpy(pLienRec->acGross, acTmp, SIZ_LIEN_GROSS);
//   }
//
//   // Ratio
//   if (lImpr > 0)
//   {
//      sprintf(acTmp, "%*u", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
//      memcpy(pLienRec->acRatio, acTmp, SIZ_RATIO);
//   }
//
//   // TRA
//   lTmp = atol(apTokens[SBX_2019_TRA]);
//   if (lTmp > 0)
//   {
//      sprintf(acTmp, "%.6d", lTmp);
//      memcpy(pLienRec->acTRA, acTmp, 6);
//   }
//
//   // Set full exemption flag
//   if (*apTokens[SBX_2019_NONTAXCODE] > ' ')
//   {
//      pLienRec->SpclFlag = LX_FULLEXE_FLG;
//      memcpy(pLienRec->extra.Sbx.NonTaxCode, apTokens[SBX_2019_NONTAXCODE], SIZ_LIEN_EXECODE);
//   }
//
//   // Prop8
//   if (*apTokens[SBX_2019_VALREASON] == 'V')
//      pLienRec->SpclFlag |= LX_PROP8_FLG;
//
//   pLienRec->LF[0] = '\n';
//   pLienRec->LF[1] = 0;
//   return 0;
//}

/********************************* Sbx_ExtrLien *****************************
 *
 * Support 2013 format
 *
 ****************************************************************************/

int Sbx_ExtrLien()
{
   char  acBuf[MAX_RECSIZE], acRollRec[MAX_RECSIZE], acOutFile[_MAX_PATH];
   long  lCnt=0;
   int   iTmp;
   FILE  *fdLien;

   // Open roll file
   LogMsg("Open Roll file %s", acRollFile);
   fdRoll = fopen(acRollFile, "r");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", acRollFile);
      return -2;
   }

   // Skip header
   fgets(acRollRec, MAX_RECSIZE, fdRoll);

   // Open Output file
   sprintf(acOutFile, acLienTmpl, myCounty.acCntyCode, myCounty.acCntyCode);
   LogMsg("Open output file %s", acOutFile);
   fdLien = fopen(acOutFile, "w");
   if (fdLien == NULL)
   {
      LogMsg("***** Error opening Lien extract file: %s\n", acOutFile);
      return -4;
   }

   // Merge loop
   while (!feof(fdRoll))
   {
      // Get roll rec
      iTmp = myGetStrEQ(acRollRec, MAX_RECSIZE, fdRoll);
      if (iTmp > 0)
      {
         // Create new lien record
         Sbx_CreateLienRec(acBuf, acRollRec);

         fputs(acBuf, fdLien);
         if (!(++lCnt % 1000))
            printf("\r%u", lCnt);
      }
   }

   // Close files
   if (fdRoll)
      fclose(fdRoll);
   if (fdLien)
      fclose(fdLien);

   // Sort output as needed

   LogMsgD("Total records output:     %d\n", lCnt);

   lRecCnt = lCnt;
   return 0;
}

/******************************** Sbx_ExtrLien1 *****************************
 *
 * Support 2014-2015 format
 *
 ****************************************************************************/

int Sbx_ExtrLien1()
{
   char  acBuf[MAX_RECSIZE], acRollRec[MAX_RECSIZE], acOutFile[_MAX_PATH], *pTmp;
   long  lCnt=0;
   int   iTmp;
   FILE  *fdLien;

   // Open roll file
   LogMsg("Open Roll file %s", acRollFile);
   fdRoll = fopen(acRollFile, "r");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", acRollFile);
      return -2;
   }

   // Skip rec header
   iTmp = 0;
   while (iTmp++ < iHdrRows)
      pTmp = fgets((char *)&acRollRec[0], MAX_RECSIZE, fdRoll);

   // Open Output file
   sprintf(acOutFile, acLienTmpl, myCounty.acCntyCode, myCounty.acCntyCode);
   LogMsg("Open output file %s", acOutFile);
   fdLien = fopen(acOutFile, "w");
   if (fdLien == NULL)
   {
      LogMsg("***** Error opening Lien extract file: %s\n", acOutFile);
      return -4;
   }

   // Merge loop
   while (!feof(fdRoll))
   {
      // Get roll rec
      pTmp = fgets((char *)&acRollRec[0], MAX_RECSIZE, fdRoll);
      if (pTmp)
      {
         // Create new lien record
         Sbx_CreateLienRec(acBuf, acRollRec);

         fputs(acBuf, fdLien);
         if (!(++lCnt % 1000))
            printf("\r%u", lCnt);
      }
   }

   // Close files
   if (fdRoll)
      fclose(fdRoll);
   if (fdLien)
      fclose(fdLien);

   // Sort output as needed

   LogMsgD("Total records output:     %d\n", lCnt);

   lRecCnt = lCnt;
   return 0;
}

/******************************** Sbx_ExtrLien2 *****************************
 *
 * Support 2016 format
 *
 ****************************************************************************/

int Sbx_ExtrLien2()
{
   char  acBuf[MAX_RECSIZE], acRollRec[MAX_RECSIZE], acOutFile[_MAX_PATH], *pTmp;
   long  lCnt=0;
   int   iTmp;
   FILE  *fdLien;

   // Open roll file
   LogMsg("Open Roll file %s", acRollFile);
   fdRoll = fopen(acRollFile, "r");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", acRollFile);
      return -2;
   }

   // Skip rec header
   iTmp = 0;
   while (iTmp++ < iHdrRows)
      pTmp = fgets((char *)&acRollRec[0], MAX_RECSIZE, fdRoll);

   // Open Output file
   sprintf(acOutFile, acLienTmpl, myCounty.acCntyCode, myCounty.acCntyCode);
   LogMsg("Open output file %s", acOutFile);
   fdLien = fopen(acOutFile, "w");
   if (fdLien == NULL)
   {
      LogMsg("***** Error opening Lien extract file: %s\n", acOutFile);
      return -4;
   }

   // Merge loop
   while (!feof(fdRoll))
   {
      // Get roll rec
      pTmp = fgets((char *)&acRollRec[0], MAX_RECSIZE, fdRoll);
      if (pTmp)
      {
         //remChar(acRollRec, '^');
         // Create new lien record
         //Sbx_CreateLienRec(acBuf, acRollRec);
         Sbx_CreateLienRec2(acBuf, acRollRec);

         fputs(acBuf, fdLien);
         if (!(++lCnt % 1000))
            printf("\r%u", lCnt);
      }
   }

   // Close files
   if (fdRoll)
      fclose(fdRoll);
   if (fdLien)
      fclose(fdLien);

   // Sort output as needed

   LogMsgD("Total records output:     %d\n", lCnt);

   lRecCnt = lCnt;
   return 0;
}

/******************************** Sbx_ExtrLien3 *****************************
 *
 * Support 2019 format
 *
 ****************************************************************************/

//int Sbx_ExtrLien3()
//{
//   char  acBuf[MAX_RECSIZE], acRollRec[MAX_RECSIZE], acOutFile[_MAX_PATH], *pTmp;
//   long  lCnt=0;
//   int   iTmp;
//   FILE  *fdLien;
//
//   // Open roll file
//   LogMsg("Open Roll file %s", acRollFile);
//   fdRoll = fopen(acRollFile, "r");
//   if (fdRoll == NULL)
//   {
//      LogMsg("***** Error opening roll file: %s\n", acRollFile);
//      return -2;
//   }
//
//   // Skip rec header
//   iTmp = 0;
//   while (iTmp++ < iHdrRows)
//      pTmp = fgets((char *)&acRollRec[0], MAX_RECSIZE, fdRoll);
//
//   // Open Output file
//   sprintf(acOutFile, acLienTmpl, myCounty.acCntyCode, myCounty.acCntyCode);
//   LogMsg("Open output file %s", acOutFile);
//   fdLien = fopen(acOutFile, "w");
//   if (fdLien == NULL)
//   {
//      LogMsg("***** Error opening Lien extract file: %s\n", acOutFile);
//      return -4;
//   }
//
//   // Merge loop
//   while (!feof(fdRoll))
//   {
//      // Get roll rec
//      pTmp = fgets((char *)&acRollRec[0], MAX_RECSIZE, fdRoll);
//      if (pTmp)
//      {
//         //remChar(acRollRec, '^');
//         // Create new lien record
//         //Sbx_CreateLienRec(acBuf, acRollRec);
//         Sbx_CreateLienRec3(acBuf, acRollRec);
//
//         fputs(acBuf, fdLien);
//         if (!(++lCnt % 1000))
//            printf("\r%u", lCnt);
//      }
//   }
//
//   // Close files
//   if (fdRoll)
//      fclose(fdRoll);
//   if (fdLien)
//      fclose(fdLien);
//
//   // Sort output as needed
//
//   LogMsgD("Total records output:     %d\n", lCnt);
//
//   lRecCnt = lCnt;
//   return 0;
//}

/******************************** Sbx_ExtrProp8 *****************************
 *
 * Support 2013 format
 *
 ****************************************************************************/

int Sbx_ExtrProp8()
{
   char  *pTmp, acBuf[256], acRollRec[MAX_RECSIZE], acOutFile[_MAX_PATH];
   long  lCnt=0, iProp8Cnt=0, iRet;
   FILE  *fdOut;

   LogMsgD("\nExtract Prop8 flag from lien roll %s", acRollFile);

   // Open roll file
   fdRoll = fopen(acRollFile, "r");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", acRollFile);
      return 2;
   }

   // Open Output file
   sprintf(acOutFile, acProp8Tmpl, myCounty.acCntyCode, myCounty.acCntyCode, myCounty.acYearAssd);
   LogMsg("Create prop8 output file %s", acOutFile);
   fdOut = fopen(acOutFile, "w");
   if (fdOut == NULL)
   {
      LogMsg("***** Error opening Prop8 extract file: %s\n", acOutFile);
      return 4;
   }

   // Drop header
   pTmp = fgets(acRollRec, MAX_RECSIZE, fdRoll);

   // print header
   fputs("APN,Prop8_Flg\n", fdOut);

   // Merge loop
   while (!feof(fdRoll))
   {
      // Get roll rec
      // 2012/07/04
      //pTmp = fgets(acRollRec, MAX_RECSIZE, fdRoll);
      //if (!pTmp)
      // break;
      iRet = myGetStrEQ(acRollRec, MAX_RECSIZE, fdRoll);
      if (iRet > 40)
      {
         // Parse input string
         iRet = ParseStringNQ(acRollRec, ',', MAX_FLD_TOKEN, apTokens);
         if (iRet < SBX_ROLL_HOXMPAMT)
         {
            LogMsg("***** Error: bad input record for APN=%s", apTokens[SBX_ROLL_APN]);
         } else
         {
            // Prop8
            if (*apTokens[SBX_ROLL_VALREASON] == 'V')
            {
               sprintf(acBuf, "%s,Y\n", apTokens[SBX_ROLL_APN]);

               fputs(acBuf, fdOut);
               iProp8Cnt++;
            }
         }
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdRoll)
      fclose(fdRoll);
   if (fdOut)
      fclose(fdOut);

   LogMsgD("\nTotal prop8 records output: %u", iProp8Cnt);
   LogMsgD("Total records processed:    %u\n", lCnt);

   return 0;
}

/****************************** Sbx_ExtrProp8_1 *****************************
 *
 * Support 2014 format
 *
 ****************************************************************************/

int Sbx_ExtrProp8_1()
{
   char  *pTmp, acBuf[256], acRollRec[MAX_RECSIZE], acOutFile[_MAX_PATH];
   long  lCnt=0, iProp8Cnt=0, iRet;
   FILE  *fdOut;

   LogMsgD("\nExtract Prop8 flag from lien roll %s", acRollFile);

   // Open roll file
   fdRoll = fopen(acRollFile, "r");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", acRollFile);
      return -2;
   }

   // Open Output file
   sprintf(acOutFile, acProp8Tmpl, myCounty.acCntyCode, myCounty.acCntyCode, myCounty.acYearAssd);
   LogMsg("Create prop8 output file %s", acOutFile);
   fdOut = fopen(acOutFile, "w");
   if (fdOut == NULL)
   {
      LogMsg("***** Error opening Prop8 extract file: %s\n", acOutFile);
      return -4;
   }

   // Skip rec header
   iRet = 0;
   while (iRet++ < iHdrRows)
      pTmp = fgets((char *)&acRollRec[0], MAX_RECSIZE, fdRoll);

   // print header
   fputs("APN,Prop8_Flg\n", fdOut);

   // Merge loop
   while (!feof(fdRoll))
   {
      if (pTmp = fgets(acRollRec, MAX_RECSIZE, fdRoll))
      {
         // Parse input string
         if (cDelim == ',')
            iRet = ParseStringNQ(acRollRec, cDelim, MAX_FLD_TOKEN, apTokens);
         else
            iRet = ParseStringIQ(acRollRec, cDelim, MAX_FLD_TOKEN, apTokens);
         if (iRet < SBX_ROLL_BATHROOMS)
         {
            LogMsg("***** Error: bad input record for APN=%s", apTokens[SBX_ROLL_APN]);
         } else
         {
            // Prop8
            if (*apTokens[SBX_ROLL_VALREASON] == 'V')
            {
               sprintf(acBuf, "%s,Y\n", apTokens[SBX_ROLL_APN]);

               fputs(acBuf, fdOut);
               iProp8Cnt++;
            }
         }
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdRoll)
      fclose(fdRoll);
   if (fdOut)
      fclose(fdOut);

   LogMsgD("\nTotal prop8 records output: %u", iProp8Cnt);
   LogMsgD("Total records processed:    %u\n", lCnt);

   return 0;
}

/****************************** Sbx_MergeCChrRec *****************************
 *
 *
 *****************************************************************************/

void Sbx_MergeCChrRec(char *pOutbuf, char *pCharRec)
{
   char     acTmp[64];
   int      iTmp;
   long     lTmp;
   EX_CHAR *pRec;

   pRec = (EX_CHAR *)pCharRec;

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "001040012", 9))
   //   iTmp = 0;
#endif
   
   // LotAcres/LotSqft
   double  dTmp = atof(pRec->LotAcres) * 1000.0;
   if (dTmp > 0.0)
   {
      sprintf(acTmp, "%*u", SIZ_LOT_ACRES, (long)(dTmp+0.01));
      memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);
      lTmp = (long)(dTmp*SQFT_FACTOR_1000);
      sprintf(acTmp, "%*u", SIZ_LOT_SQFT, lTmp);
      memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);
   }

   // Residential/Bldg area
   memcpy(acTmp, pRec->Res_Area, 10);
   acTmp[10] = 0;
   remChar(acTmp, ',');
   iTmp = atol(acTmp);
   if (iTmp > 0)
   {
      sprintf(acTmp, "%*u", SIZ_BLDG_SF, iTmp);
      memcpy(pOutbuf+OFF_BLDG_SF, acTmp, SIZ_BLDG_SF);
   }

   // Yr Built
   if (pRec->YrBlt[0] > '0')
      memcpy(pOutbuf+OFF_YR_BLT, pRec->YrBlt, SIZ_YR_BLT);

   // Beds
   iTmp = atoin(pRec->Beds, 5);
   if (iTmp > 0)
   {
      sprintf(acTmp, "%*u", SIZ_BEDS, iTmp);
      memcpy(pOutbuf+OFF_BEDS, acTmp, SIZ_BEDS);
   }

   // Baths
   pRec->Baths[5] = 0;
   dTmp = atof(pRec->Baths);
   if (dTmp > 0.0)
   {
      iTmp = (int)(dTmp + 0.25);
      sprintf(acTmp, "%*u", SIZ_BATH_F, iTmp);
      memcpy(pOutbuf+OFF_BATH_F, acTmp, SIZ_BATH_F);
      memcpy(pOutbuf+OFF_BATH_4Q, acTmp, SIZ_BATH_F);

      char *pTmp = strchr(pRec->Baths, '.');
      if (pTmp++)
      {
         memcpy(pOutbuf+OFF_BATH_H, " 1", 2);
         if (*pTmp <= '3')
            *(pOutbuf+OFF_BATH_1Q) = '1';
         else if (*pTmp <= '6')
            *(pOutbuf+OFF_BATH_2Q) = '1';
         else if (*pTmp <= '9')
            *(pOutbuf+OFF_BATH_3Q) = '1';
      }
   }

   // Fire Place
   memset(pOutbuf+OFF_FIRE_PL, ' ', SIZ_FIRE_PL);
   iTmp = atoin(pRec->Fp, 4);
   if (iTmp > 9)
      *(pOutbuf+OFF_FIRE_PL) = 'M';
   else if (iTmp > 0)
      *(pOutbuf+OFF_FIRE_PL) = '0' | iTmp;
   else if (pRec->Fp[0] > '0')
      LogMsg("*** Bad FirePlace %.4s [%.*s]", pRec->Fp, iApnLen, pRec->APN);
}

/********************************* Sbx_MergeCChr *****************************
 *
 * Merge current chars pull down from the county website to R01 file.
 * If R01 is not avail, merge to S01.
 *
 *****************************************************************************/

int Sbx_MergeCChr(int iSkip)
{
   char     *pTmp, acBuf[MAX_RECSIZE], acCharRec[MAX_RECSIZE];
   char     acRawFile[_MAX_PATH], acOutFile[_MAX_PATH];

   HANDLE   fhIn, fhOut;
   FILE     *fdChar;

   int      iRet, iRollUpd=0;
   DWORD    nBytesRead;
   DWORD    nBytesWritten;
   BOOL     bRet, bEof, bUseS01=false;
   long     lRet=0, lCnt=0;

   LogMsgD("Merge CHAR to R01/S01 file");

   sprintf(acRawFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "R01");
   sprintf(acOutFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "N01");

   // Rename files for processing
   if (_access(acRawFile, 0))
   {
      sprintf(acRawFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "S01");
      if (!_access(acRawFile, 0))
         bUseS01 = true;
      else
      {
         LogMsg("***** Missing input file %s.  Please recheck!", acRawFile);
         return -1;
      }
   }

   // Open Chars file
   LogMsg("Open Chars file %s", acCChrFile);
   fdChar = fopen(acCChrFile, "r");
   if (fdChar == NULL)
   {
      LogMsg("***** Error opening Chars file: %s\n", acCChrFile);
      return -2;
   }

   // Open Input file
   LogMsg("Open input file %s", acRawFile);
   fhIn = CreateFile(acRawFile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);
   if (fhIn == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening input file: %s\n", acRawFile);
      return -3;
   }

   // Open Output file
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);
   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening output file: %s\n", acOutFile);
      return -4;
   }

   // Get first Chars rec
   pTmp = fgets((char *)&acCharRec[0], MAX_RECSIZE, fdChar);
   bEof = (pTmp ? false:true);

   // Copy skip record
   memset(acBuf, ' ', iRecLen);
   while (iSkip-- > 0)
   {
      ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);
      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
   }

   // Merge loop
   while (!bEof)
   {
      bRet = ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);

      // Check for EOF
      if (!bRet)
      {
         LogMsg("Error reading input file %s (%f)", acRawFile, GetLastError());
         break;
      }

      // EOF ?
      if (iRecLen != nBytesRead)
         break;

#ifdef _DEBUG
      //if (!memcmp(acBuf, "0167110120", 10))
      //   bRet = 0;
#endif
 
      if (fdChar)
      {
Sbx_Char_ReLoad:
         // Merge Char
         iRet = memcmp(acBuf, acCharRec, iApnLen);
         if (!iRet)
         {
            // Merge sale data
            Sbx_MergeCChrRec(acBuf, acCharRec);
            iRollUpd++;

            // Read next CHAR record
            pTmp = fgets((char *)&acCharRec[0], MAX_RECSIZE, fdChar);
            if (!pTmp)
            {
               fclose(fdChar);
               fdChar = NULL;
            }
         } else
         {
            if (iRet > 0)
            {
               // Char not match, advance to next char record
               if (bDebug)
                  LogMsg("CHAR not match : %.*s > %.*s (%d) ", iApnLen, acBuf, iApnLen, acCharRec, lCnt);
               
               pTmp = fgets((char *)&acCharRec[0], MAX_RECSIZE, fdChar);
               if (pTmp)
                  goto Sbx_Char_ReLoad;
               else
               {
                  fclose(fdChar);
                  fdChar = NULL;
               }
            } else if (bDebug)
               LogMsg("Skip APN : %.*s < %.*s (%d) ", iApnLen, acBuf, iApnLen, acCharRec, lCnt);
         }
      }

      if (!isdigit(acCharRec[0]))
         LogMsg("Invalid CHARS at APN=%.*s (%d)", iApnLen, acBuf, lCnt);

      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);

      if (!bRet)
      {
         LogMsg("Error occurs: %d\n", GetLastError());
         lRet = WRITE_ERR;
         break;
      }
   }

   // Close files
   if (fdChar)
      fclose(fdChar);
   if (fhOut)
      CloseHandle(fhOut);
   if (fhIn)
      CloseHandle(fhIn);

   if (bUseS01)
   {
      // Rename output file to R01
      sprintf(acRawFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "R01");
      rename(acOutFile, acRawFile);
   } else
   {
      DeleteFile(acRawFile);
      rename(acOutFile, acRawFile);
   }

   LogMsgD("\nTotal output records:       %u", lCnt);
   LogMsgD("Total records updated:      %u", iRollUpd);

   lRecCnt = lCnt;
   return lRet;
}

/****************************** Sbx_CChr2Sale ********************************
 *
 * Return:
 *       -1 = bad date
 *        0 = no date
 *        1 = has date
 *        2 = has DocNum
 *        4 = has price
 *
 *****************************************************************************/

int Sbx_CChr2Sale(char *pOutbuf, char *pCharRec)
{
   char     acTmp[64];
   int      iRet, iTmp;
   long     lTmp;
   double   dTax;

   EX_CHAR   *pRec;
   SCSAL_REC *pSale;

   pRec  = (EX_CHAR *)   pCharRec;
   pSale = (SCSAL_REC *) pOutbuf;
   iRet = 0;

   if (pRec->APN[0] < '0')
      return -1;

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "001040012", 9))
   //   iTmp = 0;
#endif

   // Check for transfer Amt
   if (isdigit(pRec->DocDate[0]))
   {
      char acDate[32], *pTmp;
         
      // Clear buffer
      memset(pOutbuf, ' ', sizeof(SCSAL_REC));

      // APN
      memcpy(pSale->Apn, pRec->APN, iApnLen);

      // Sale price
      pRec->DocTax[12] = 0;
      dollar2Num(pRec->DocTax, acTmp);
      if (ChkDocTax(acTmp))
      {
         dTax = atof(acTmp);
         lTmp = (long)(dTax*SALE_FACTOR);
         if (dTax > 10.0 && lTmp < 999999999)
         {
            memcpy(pSale->StampAmt, acTmp, strlen(acTmp));
            iTmp = sprintf(acTmp, "%d", lTmp);
            memcpy(pSale->SalePrice, acTmp, iTmp);
            iRet = 4;
         }
      }

      // DocDate
      memcpy(acTmp, pRec->DocDate, 10);
      acTmp[10] = 0;
      pTmp = dateConversion(acTmp, acDate, MM_DD_YYYY_1);
      if (pTmp)
      {
         memcpy(pSale->DocDate, acDate, 8);

         // Check for DocNum
         if (pRec->DocNum[0] > ' ')
            iRet |= 2;

         pSale->ARCode = 'W';
         pSale->CRLF[0] = '\n';
         pSale->CRLF[1] = 0;
         iRet |= 1;
      } else
      {
         LogMsg("*** Bad DocDate %.10s APN=%.10s", pRec->DocDate, pRec->APN);
         iRet = -1;
      }
   }

   return iRet;
}

/********************************* Sbx_ExtrSale ******************************
 *
 * Extract sale data from HtmlExtr file.  
 * Return 0 if success, < 0 if error, 1 if no input.
 *
 *****************************************************************************/

int Sbx_ExtrWebSale(char *pOutFile, bool bAppend)
{
   char     *pTmp, acBuf[1200], acCharRec[1200], acOutFile[_MAX_PATH];
   FILE     *fdChar;
   int      iRet, iPrice=0, iDocNum=0, iBad=0;
   long     lCnt=0, lSaleOut=0;

   // If file not available, ignore it
   if (_access(acCChrFile, 0))
   {
      LogMsg("*** Missing input file: %s", acCChrFile);
      return 0;
   }

   LogMsgD("Extract sale from HtmlExtr data: %s", acCChrFile);

   // Open Chars file
   LogMsg("Open Chars file %s", acCChrFile);
   fdChar = fopen(acCChrFile, "r");
   if (fdChar == NULL)
   {
      LogMsg("***** Error opening Chars file: %s\n", acCChrFile);
      return -2;
   }

   // Open Sales file
   sprintf(acOutFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "tmp");
   LogMsg("Open Sales file %s", acOutFile);
   fdSale = fopen(acOutFile, "w");
   if (fdSale == NULL)
   {
      LogMsg("***** Error creating Sales file: %s\n", acOutFile);
      return -3;
   }

   // Merge loop
   while (!feof(fdChar))
   {
      pTmp = fgets((char *)&acCharRec[0], 1200, fdChar);

#ifdef _DEBUG
      //if (!memcmp(acBuf, "0167110120", 10))
      //   bRet = 0;
#endif
 
      if (!pTmp)
         break;

      // Merge sale data
      iRet = Sbx_CChr2Sale(acBuf, acCharRec);
      if (iRet > 0)
      {
         if (iRet & 2)
            iDocNum++;
         if (iRet & 4)
            iPrice++;
         fputs(acBuf, fdSale);
         lSaleOut++;
      } else
         iBad++;
      
      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdChar)
      fclose(fdChar);
   if (fdSale)
      fclose(fdSale);

   LogMsg("Total records processed:      %u", lCnt);
   LogMsg("              output:         %u", lSaleOut);
   LogMsg("              has price:      %u", iPrice);
   LogMsg("              has DocNum:     %u", iDocNum);
   LogMsg("              bad/no date:    %u", iBad);

   // Merge with cum sale
   char acTmp[256];
   sprintf(acCharRec, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "srt");
   if (bAppend)
      sprintf(acBuf, "%s+%s", pOutFile, acOutFile);
   else
      strcpy(acBuf, acOutFile); 

   // Sort sale file - APN (asc), Saledate (asc), sale price (desc), DocNum (asc)
   sprintf(acTmp, "S(1,10,C,A,27,8,C,A,57,10,C,D,15,12,C,A) DUPO(B2000,1,34)");
   iRet = sortFile(acBuf, acCharRec, acTmp);
   if (!iRet)
   {
      // Something is wrong here
      LogMsg("***** Error extracting HtmlExtr file.  Please check %s.", acCChrFile);
      return -1;
   }

   LogMsg("Total cumulative web sale records:  %u", iRet);

   iRet = 0;
   sprintf(acOutFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "wav");
   if (!_access(acOutFile, 0))
      iRet = DeleteFile(acOutFile);

   // Rename old SBX_Sale.web to SBX_Sale.sav then rename .srt to .web
   if (!_access(pOutFile, 0))
      iRet = rename(pOutFile, acOutFile);

   if (!iRet)
   {
      // Rename .srt to .web
      iRet = rename(acCharRec, pOutFile);
   } else
   {
      sprintf(acTmp, "***** Error renaming %s to %s: (%d)", pOutFile, acOutFile, _errno);
      return -1;
   }

   if (!iRet)
   {
      // Rename input file
      strcpy(acTmp, acCChrFile);
      pTmp = strrchr(acTmp, '.');
      strcpy(pTmp, acToday);
      strcat(acTmp, ".dat");
      if (!_access(acTmp, 0))
         if (!DeleteFile(acTmp))
            LogMsg("***** Error deleting %s", acTmp);
      rename(acCChrFile, acTmp);
      strcpy(acCChrFile, acTmp);    // Allow -Ma to work correctly
   } else
      sprintf(acTmp, "***** Error renaming %s to %s: (%d)", acCharRec, pOutFile, _errno);

   return 0;
}

/***************************** Sbx_ExtrRollSale ******************************
 *
 * Extract sale data from roll file.  
 * Roll file format is changed every year. 
 *
 * Return 0 if success, < 0 if error, 1 if no input.
 *
 *****************************************************************************/

int Sbx_ExtrRollSale(char *pRollFile, int iYear=1900)
{
   char     *pTmp, acRollRec[MAX_RECSIZE], acBuf[1024], acTmp[256], acOutFile[_MAX_PATH];
   int      iRet, iDocNum, iDocDate, iOwner, iMaxItems;
   long     lCnt=0, lSaleOut=0, lTmp;
   double   dTmp;

   SCSAL_REC *pSale = (SCSAL_REC *)&acBuf[0];

   // If file not available, ignore it
   if (_access(pRollFile, 0))
      return 1;

   LogMsgD("Extract sale from: %s", pRollFile);

   // Open Chars file
   LogMsg("Open roll file %s", pRollFile);
   fdRoll = fopen(pRollFile, "r");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", pRollFile);
      return -2;
   }

   switch (iYear)
   {
      case 1900:                    // Roll update
         iDocNum   = SBX_UPDT_DOCNUM;
         iDocDate  = SBX_UPDT_DOCDATE; 
         iOwner    = SBX_UPDT_PRIMARYOWR;
         iMaxItems = SBX_UPDT_EXEMPTCATGRY;
         break;

      case 2011:                    // DocNum=34, DocDate=35 
      case 2013:
         iDocNum   = 34;
         iDocDate  = 35; 
         iOwner    = 3;
         iMaxItems = SBX_ROLL_HOXMPAMT;
         break;

      default:                      // DocNum=35, DocDate=36 
         iDocNum   = 35;
         iDocDate  = 36; 
         iOwner    = 4;
         iMaxItems = SBX_ROLL_HOXMPAMT;
         break;
   }

   // Open Sales file
   sprintf(acBuf, "%d", iYear);
   sprintf(acOutFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, acBuf);
   LogMsg("Open Sales file %s", acOutFile);
   fdSale = fopen(acOutFile, "w");
   if (fdSale == NULL)
   {
      LogMsg("***** Error creating Sales file: %s\n", acOutFile);
      return -3;
   }

   // Skip header
   iRet = myGetStrEQ(acRollRec, MAX_RECSIZE, fdRoll);

   // Merge loop
   while (!feof(fdRoll))
   {
      iRet = myGetStrEQ(acRollRec, MAX_RECSIZE, fdRoll);
      if (!iRet)
         break;

#ifdef _DEBUG
      //if (!memcmp(acBuf, "0167110120", 10))
      //   bRet = 0;
#endif
 
      iRet = ParseStringNQ(acRollRec, ',', MAX_FLD_TOKEN, apTokens);
      if (iRet < iMaxItems)
      {
         LogMsg("***** Error: bad input record for APN=%.10s", acRollRec);
         continue;
      }

      // Extract sale data

      if (*apTokens[iDocNum] > ' ' && *apTokens[iDocDate] > ' ')
      {
         memset(acBuf, 32, sizeof(SCSAL_REC));
         memcpy(pSale->Apn, apTokens[0], strlen(apTokens[0]));

         // % xfer
         dTmp = atof(apTokens[iDocDate+1]);
         if (dTmp > 0.01)
         {
            lTmp = (long)((dTmp * 100.0)+0.5);
            iRet = sprintf(acTmp, "%d", lTmp);
            memcpy(pSale->PctXfer, acTmp, iRet);
            if (lTmp > 98)
               pSale->SaleCode[0] = 'F';
            else
               pSale->SaleCode[0] = 'P';
         }

         if (*(apTokens[iDocNum]+4) == '-')
            remChar(apTokens[iDocNum], '-');
         memcpy(pSale->DocNum, apTokens[iDocNum], strlen(apTokens[iDocNum]));

         // Format on lien roll 2007 as mm/dd/yyyy
         //pTmp = dateConversion(apTokens[SBX_ROLL_DOCDATE], acTmp, 0);
         // Format on LDR 2010 as yyyy-mm-dd
         if (*(apTokens[iDocDate]+4) == '-')
            pTmp = dateConversion(apTokens[iDocDate], acTmp, YYYY_MM_DD);
         else if (*(apTokens[iDocDate]+2) == '/' || *(apTokens[iDocDate]+1) == '/')
            pTmp = dateConversion(apTokens[iDocDate], acTmp, 0);
         else
            pTmp = NULL;

         if (pTmp)
            memcpy(pSale->DocDate, acTmp, 8);
         else
            LogMsg("*** Bad sale date %s", apTokens[iDocDate]);

         // Current owner
         memcpy(pSale->Name1, apTokens[iOwner], strlen(apTokens[iOwner]));

         // Assessor flag - transfer data is from assessor
         pSale->ARCode = 'A';
         pSale->CRLF[0] = '\n';
         pSale->CRLF[1] = 0;
         fputs(acBuf, fdSale);
         lSaleOut++;
      }
      
      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdRoll)
      fclose(fdRoll);
   if (fdSale)
      fclose(fdSale);

   // Add current file to Sbx_Sale.rol
   char acSortFile[_MAX_PATH];
   sprintf(acSortFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "srt");
   if (!_access(acRollSale, 0))
      sprintf(acBuf, "%s+%s", acRollSale, acOutFile);
   else
      strcpy(acBuf, acOutFile);

   strcpy(acTmp, "S(1,14,C,A,27,8,C,A,15,12,C,A,57,10,C,D) OMIT(15,1,C,EQ,\" \") DUPO(1,34)");
   lSaleOut = sortFile(acBuf, acSortFile, acTmp, &iRet);
   if (lSaleOut <= 0)
   {
      LogMsg("***** Error sorting %s to %s (%d)", acBuf, acSortFile, iRet);
      iRet = -1;
   } else
   {
      // Rename files
      sprintf(acTmp, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "rav");
      if (!_access(acTmp, 0))
         remove(acTmp);
      if (!_access(acRollSale, 0))
         iRet = rename(acRollSale, acTmp);
      if (!iRet)
         iRet = rename(acSortFile, acRollSale);
      else
         LogMsg("***** Unable to rename %s to %s", acSortFile, acRollSale);
   }

   LogMsgD("\nTotal output records:         %u", lSaleOut);
   LogMsgD("Total records processed:      %u", lCnt);

   return iRet;
}

/********************************* Sbx_UpdateSale ****************************
 *
 * Match DOCDATE in sale history file against XFERDATE in R01 file. If natched,
 * copy DOCNUM from R01 file to sale file.
 *
 * Input:  SBX_Sales.sls, SSBX.R01
 * Output: SBX_Sales.sls
 *
 *****************************************************************************/

int Sbx_UpdateSale(int iSkip, bool bRename)
{
   char  *pTmp, sTmpFile[_MAX_PATH], acRawFile[_MAX_PATH], sR01Buf[2048], sSaleRec[2048];
   int   iTmp, iCnt, iDocUpd;
   DWORD nBytesRead;
   BOOL  bRet, bEof;
   
   SCSAL_REC *pSaleRec = (SCSAL_REC *)&sSaleRec[0];
   HANDLE    fhIn;
   FILE      *fdTmp;

   LogMsg("Update DocNum to sale history");

   // Prepare input file name
   sprintf(acRawFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "R01");
   if (_access(acRawFile, 0))
      sprintf(acRawFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "S01");

   // Open Input file
   LogMsg("Open raw file %s", acRawFile);
   fhIn = CreateFile(acRawFile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhIn == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening input file: %s\n", acRollFile);
      return -1;
   }

   // Open sale input
   LogMsg("Open sale history file %s", acCSalFile);
   if (!(fdSale = fopen(acCSalFile, "r")))
      return -2;

   // Prepare output file 
   sprintf(sTmpFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "tmp");
   LogMsg("Create temp sale file %s", sTmpFile);
   if (!(fdTmp = fopen(sTmpFile, "w")))
      return -4;
            
   // Skip header
   while (iSkip-- > 0)
      ReadFile(fhIn, sR01Buf, iRecLen, &nBytesRead, NULL);

   // Get first rec
   ReadFile(fhIn, sR01Buf, iRecLen, &nBytesRead, NULL);

   // Main loop
   iCnt = 0;
   iDocUpd = 0;
   bEof = false;
   while (!feof(fdSale) && !bEof)
   {
      // Read sale rec
      pTmp = fgets(sSaleRec, 1024, fdSale);
      if (!pTmp)
         break;

      NextRec:
      iTmp = memcmp(sR01Buf, pSaleRec->Apn, iApnLen);
      if (!iTmp)
      {
         // Merge data
         if (!memcmp((char *)&sR01Buf[OFF_TRANSFER_DT], pSaleRec->DocDate, SIZ_TRANSFER_DT))
         {
            memcpy(pSaleRec->DocNum, (char *)&sR01Buf[OFF_TRANSFER_DOC], SIZ_TRANSFER_DOC);
            iDocUpd++;
         }

         // Read next R01 record
         bRet = ReadFile(fhIn, sR01Buf, iRecLen, &nBytesRead, NULL);
         if (!nBytesRead)
            bEof = true;    // Signal to stop
         if (!bRet)
         {
            LogMsg("***** Error reading input file %s (%f)", acRawFile, GetLastError());
            break;
         }
      } else if (iTmp < 0)       // Not match, get new R01 record?
      {
         // Read next R01 record
         bRet = ReadFile(fhIn, sR01Buf, iRecLen, &nBytesRead, NULL);
         if (!bRet)
         {
            LogMsg("***** Error reading input file %s (%f)", acRawFile, GetLastError());
            break;
         }

         if (!nBytesRead)
            bEof = true;    // Signal to stop
         else
            goto NextRec;
      }

      fputs(sSaleRec, fdTmp);
      if (!(++iCnt % 1000))
         printf("\r%u", iCnt);
   }

   // Write out the rest
   while (!feof(fdSale))
   {
      pTmp = fgets(sSaleRec, 1024, fdSale);
      if (!pTmp)
         break;

      fputs(sSaleRec, fdTmp);
      if (!(++iCnt % 1000))
         printf("\r%u", iCnt);
   }

   if (fhIn)
      CloseHandle(fhIn);
   if (fdSale)
      fclose(fdSale);
   if (fdTmp)
      fclose(fdTmp);

   // Rename output file
   if (bRename)
   {
      iTmp = 0;
      sprintf(acRawFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "sav");
      if (!_access(acRawFile, 0))
      {
         iTmp = remove(acRawFile);
         if (iTmp)
            LogMsg("***** Error removing file: %s", acRawFile);
      }

      if (!iTmp)
      {
         LogMsg("Rename %s to %s", acCSalFile, acRawFile);
         rename(acCSalFile, acRawFile);
         LogMsg("Rename %s to %s", sTmpFile, acCSalFile);
         rename(sTmpFile, acCSalFile);
      }
   }

   LogMsg("Records updated: %d", iDocUpd);
   LogMsg("Records output:  %d", iCnt);

   return 0;
}

/**************************** Sbx_MergeSaleRec *******************************
 *
 * Merge Sale price
 *
 * Return 99=EOF. 2=price update, 4=sale code update, 8=multi-sale flag, 0=nothing change
 *       
 *****************************************************************************/

static int Sbx_MergeSaleRec(char *pOutbuf, FILE *fdOut, FILE *fd2)
{
   static   char acRec[1024], *pRec=NULL;
   int      iTmp, iRet=0;

   SCSAL_REC *pSale1  = (SCSAL_REC *)pOutbuf;
   SCSAL_REC *pSale2  = (SCSAL_REC *)&acRec[0];

   // Get rec
   if (!pRec)
      pRec = fgets(acRec, 1024, fd2);

   do 
   {
      if (!pRec)
         return 99;      // EOF 

#ifdef _DEBUG
      //if (!memcmp(pOutbuf, "019022002", 9))
      //   iTmp = 0;
#endif
      iTmp = memcmp(pSale1->Apn, pSale2->Apn, iApnLen);
      // If APN matched, compare date
      if (!iTmp)
      {
         iTmp = memcmp(pSale1->DocDate, pSale2->DocDate, 8);
         if (iTmp > 0)
         {
            if (bDebug)
               LogMsg("Skipping: %.34s", pRec);

            // Output unmatched record
            if (pSale2->SalePrice[0] > ' ' && pSale2->DocDate[0] > ' ')
               fputs(acRec, fdOut);

            pRec = fgets(acRec, 1024, fd2);
         }
      } else if (iTmp > 0)
      {
            if (bDebug)
               LogMsg("Skipping: %.34s", pRec);

            // Output unmatched record
            if (pSale2->SalePrice[0] > ' ' && pSale2->DocDate[0] > ' ')
               fputs(acRec, fdOut);

            pRec = fgets(acRec, 1024, fd2);
      }
   } while (iTmp > 0);

   // If not match, return
   if (iTmp)
      return iTmp;

   // Copy sale price if current record > 1000
   // Small value often not valid sale price
   iTmp = atoin(pSale2->SalePrice, SALE_SIZ_SALEPRICE);
   if (iTmp > 1000)
   {
      memcpy(pSale1->SalePrice, pSale2->SalePrice, SALE_SIZ_SALEPRICE); 
      memcpy(pSale1->StampAmt, pSale2->StampAmt, SALE_SIZ_STAMPAMT); 
      if (pSale1->Name1[0] == ' ' && pSale2->Name1[0] > '0')
         memcpy(pSale1->Name1, pSale2->Name1, SALE_SIZ_BUYER); 
      pSale1->ARCode = 'C';
      iRet = 2;
   } 

   if (pSale2->SaleCode[0] > ' ' && pSale1->SaleCode[0] == ' ')
   {
      iRet |= 4;
      pSale1->SaleCode[0] = pSale2->SaleCode[0];
   }
   if (pSale2->MultiSale_Flg > ' ' && pSale1->MultiSale_Flg == ' ')
   {
      iRet |= 8;
      pSale1->MultiSale_Flg = pSale2->MultiSale_Flg;
   }

   // Get next record
   pRec = fgets(acRec, 1024, fd2);

   return iRet;
}

/***************************** MergeSaleFiles **********************************
 *
 * Merge sale price from file2 into file one if sale price on file1 is 0.
 *
 * Return 0 if successful, otherwise error.
 *
 *****************************************************************************/

int Sbx_MergeSaleFiles(char *pFile1, char *pFile2, char *pOutfile, bool bResort=false)
{
   char     acInbuf[1024], acSortFile[_MAX_PATH], acOutFile[_MAX_PATH], *pRec;
   char     acFile1[_MAX_PATH], acFile2[_MAX_PATH];
   long     lCnt=0, lMatch=0, lUpdPrice=0, lUpdMFlag=0, lUpdCode=0, iTmp;
   FILE     *fdOut, *fd1, *fd2;

   LogMsgD("Combine sales from roll and web extract to cumsale");
   LogMsg("Roll sale file: %s", pFile1);
   LogMsg("Web sale file:  %s", pFile2);
   LogMsg("Cum sale file:  %s", pOutfile);

   if (_access(pFile1, 0))
   {
      LogMsg("***** Sbx_MergeSaleFiles(): Missing input file: %s", pFile1);
      return -1;
   }
   if (_access(pFile2, 0))
   {
      LogMsg("***** Sbx_MergeSaleFiles(): Missing input file: %s", pFile2);
      return -1;
   }

   // Resort sale file - APN ASC, DATE ASC, DOCNUM DESC
   strcpy(acFile1, pFile1);
   strcpy(acFile2, pFile2);
   if (bResort)
   {
      pRec = strrchr(acFile1, '.');
      strcpy(pRec, ".SR1");

      sprintf(acInbuf, "S(1,34,C,A,57,10,C,D) F(TXT) DUPO(B2000,1,34) ");
      iTmp = sortFile(pFile1, acFile1, acInbuf);
      if (!iTmp)
         return -10;

      pRec = strrchr(acFile2, '.');
      strcpy(pRec, ".SR2");

      sprintf(acInbuf, "S(1,34,C,A,57,10,C,D) F(TXT) DUPO(B2000,1,34) ");
      iTmp = sortFile(pFile2, acFile2, acInbuf);
      if (!iTmp)
         return -10;
   }

   // Open input file
   LogMsg("Open input sale file1 %s", acFile1);
   fd1 = fopen(acFile1, "r");
   if (fd1 == NULL)
   {
      LogMsg("***** Error opening sale file1: %s\n", acFile1);
      return -2;
   }
   LogMsg("Open input sale file2 %s", acFile2);
   fd2 = fopen(acFile2, "r");
   if (fd2 == NULL)
   {
      LogMsg("***** Error opening sale file2: %s\n", acFile2);
      return -2;
   }

   sprintf(acOutFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "tmp");

   // Open output file
   LogMsg("Create output sale file %s", acOutFile);
   fdOut = fopen(acOutFile, "w");
   if (fdOut == NULL)
   {
      LogMsg("***** Error creating output sale file: %s\n", acOutFile);
      return -3;
   }

   // Convert loop
   while (!feof(fd1))
   {
      if (!(pRec = fgets(acInbuf, 1024, fd1)))
         break;

#ifdef _DEBUG
      //if (!memcmp(acInbuf, "019022002", 9))
      //   iTmp = 0;
#endif
      iTmp = Sbx_MergeSaleRec(acInbuf, fdOut, fd2);
      fputs(acInbuf, fdOut);
      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);

      if (iTmp == 99)
         break;

      if (iTmp >= 0)
      {
         if (iTmp & 2)
            lUpdPrice++;
         if (iTmp & 4)
            lUpdCode++;
         if (iTmp & 8)
            lUpdMFlag++;
         lMatch++;
      } else if (bDebug)
         LogMsg("---> unmatched: %.34s (%d)", acInbuf, iTmp);
   }

   // Copy the rest of the file over
   while (!feof(fd1))
   {
      if (!(pRec = fgets(acInbuf, 1024, fd1)))
         break;

      fputs(acInbuf, fdOut);
      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fd1)
      fclose(fd1);
   if (fd2)
      fclose(fd2);
   if (fdOut)
      fclose(fdOut);

   if (bResort)
   {
      DeleteFile(acFile1);
      DeleteFile(acFile2);
   }

   sprintf(acSortFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "srt");
   strcpy(acInbuf, "S(1,14,C,A,27,8,C,A,57,10,C,D,15,12,C,A) F(TXT) DUPO(B2000,1,63)");
   iTmp = sortFile(acOutFile, acSortFile, acInbuf);
   if (iTmp <= 0)
   {
      LogMsg("***** Error sorting %s to %s", acOutFile, acSortFile);
      iTmp = -1;
   } else
   {
      // Remove tmp output file
      DeleteFile(acOutFile);

      // If no output file specified, output to cum sale file
      if (!pOutfile)
         strcpy(acOutFile, acCSalFile);
      else
         strcpy(acOutFile, pOutfile);

      // Rename files
      if (!_access(acOutFile, 0))
      {
         sprintf(acFile2, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "sav");
         if (!_access(acFile2, 0))
            remove(acFile2);
         iTmp = rename(acOutFile, acFile2);

         // Append srt file to out file
         sprintf(acFile1, "%s+%s", acSortFile, acFile2);
         iTmp = sortFile(acFile1, acOutFile, acInbuf);
         if (iTmp <= 0)
         {
            LogMsg("***** Error sorting %s to %s", acOutFile, acSortFile);
            iTmp = -1;
         } else
            iTmp = 0;
      } else
      {
         iTmp = rename(acSortFile, acOutFile);
         if (iTmp)
            LogMsg("***** Error renaming %s to %s", acSortFile, acOutFile);
      }
   }


   LogMsgD("\nTotal input records:      %u", lCnt);
   LogMsg("      matched records:    %u", lMatch);
   LogMsg("      price updated:      %u", lUpdPrice);
   LogMsg("      salecode updated:   %u", lUpdCode);
   LogMsg("      multi-sale updated: %u", lUpdMFlag);

   return iTmp;
}

/************************** Sbx_Load_TaxSuppMain *****************************
 *
 * Load tax supplement file SOBR46M.TXT
 *
 *****************************************************************************/

int Sbx_ParseTaxSupp(char *pBaseBuf, char *pInbuf)
{
   double   dTax1, dTax2, dPen1, dPen2, dTotalFee, dTotalTax, dTotalDue, dPaid1, dPaid2;
   int      iTmp;

   TAXBASE    *pBaseRec = (TAXBASE *)pBaseBuf;

   // Parse input tax data
   iTokens = ParseStringIQ(pInbuf, '|', MAX_FLD_TOKEN, apTokens);
   if (iTokens < TS_CANCELLEDDATE)
   {
      LogMsg("***** Error: bad SOBR46M record for APN=%.*s (#tokens=%d)", iApnLen, pInbuf, iTokens);
      return -1;
   } 

   // Clear output buffer
   memset(pBaseBuf, 0, sizeof(TAXBASE));
   iTmp = atol(apTokens[TS_TAXYEAR]);

   // Tax Year - process current year supplement only.  Prior years are inaccurate.
   if (iTmp < lTaxYear)
      return 2;

   strcpy(pBaseRec->TaxYear, apTokens[TS_TAXYEAR]);

   // APN
   strcpy(pBaseRec->Apn, apTokens[TS_PARCELNUMBER]);

   // TRA
   strcpy(pBaseRec->TRA, apTokens[TS_TAXRATEAREA]);

   // BillNum
   strcpy(pBaseRec->BillNum, apTokens[TS_BILLNUMBER]);

   // Tax Amount
   dTax1 = atof(apTokens[TS_FIRSTINSTALLTAX]);
   dTax2 = atof(apTokens[TS_SECONDINSTALLTAX]);
   sprintf(pBaseRec->TaxAmt1, "%.2f", dTax1);
   sprintf(pBaseRec->TaxAmt2, "%.2f", dTax2);

   // Total Tax 
   dTotalTax = atof(apTokens[TS_TOTALTAX]);
   pBaseRec->dTotalTax = dTotalTax;
   if (dTotalTax > 0.0)
      sprintf(pBaseRec->TotalTaxAmt, "%.2f", dTotalTax);
   else
      return 1;

   // Penalty
   dPen1 = atof(apTokens[TS_FIRSTPENALTY])+atof(apTokens[TS_FIRSTADDITIONALPENALTY]);
   if (dPen1 > 0.0)
      sprintf(pBaseRec->PenAmt1, "%.2f", dPen1);
   dPen2 = atof(apTokens[TS_SECONDPENALTY])+atof(apTokens[TS_SECONDADDITIONALPENALTY]);
   if (dPen2 > 0.0)
      sprintf(pBaseRec->PenAmt2, "%.2f", dPen2);

   // Delinquent cost
   dTotalFee = atof(apTokens[TS_FIRSTCOST]);
   if (dTotalFee > 0.0)
      sprintf(pBaseRec->TotalFees, "%.2f", dTotalFee);

   // Due Date
   dateConversion(apTokens[TS_FIRSTDELINQUENTDATE],  pBaseRec->DueDate1, YYYY_MM_DD);
   dateConversion(apTokens[TS_SECONDDELINQUENTDATE], pBaseRec->DueDate2, YYYY_MM_DD);

#ifdef _DEBUG
   //if (!memcmp(pBaseRec->Apn, "099390004", 9))
   //   dTax1 = 0;
#endif

   dPaid1=dPaid2=dTotalDue = 0;
   if (*apTokens[TS_INSTALLCODE] == '3')           // Both installments Paid
   {
      dateConversion(apTokens[TS_FIRSTINSTALLPAIDDATE],  pBaseRec->PaidDate1, YYYY_MM_DD);
      dateConversion(apTokens[TS_SECONDINSTALLPAIDDATE], pBaseRec->PaidDate2, YYYY_MM_DD);
      pBaseRec->Inst1Status[0] = TAX_BSTAT_PAID;
      pBaseRec->Inst2Status[0] = TAX_BSTAT_PAID;
      dPaid1 = atof(apTokens[TS_FIRSTPAYMENTS]);
      dPaid2 = atof(apTokens[TS_SECONDPAYMENTS]);
   } else if (*apTokens[TS_INSTALLCODE] == '2')    // Second Installment Unpaid
   {
      dateConversion(apTokens[TS_FIRSTINSTALLPAIDDATE],  pBaseRec->PaidDate1, YYYY_MM_DD);
      dPaid1 = atof(apTokens[TS_FIRSTPAYMENTS]);
      dTotalDue = atof(apTokens[TS_SECONDTOTALDUE]);
      pBaseRec->Inst1Status[0] = TAX_BSTAT_PAID;
      pBaseRec->Inst2Status[0] = TAX_BSTAT_UNPAID;
   } else                                          // First and second Installment unpaid
   {
      pBaseRec->Inst1Status[0] = TAX_BSTAT_UNPAID;
      pBaseRec->Inst2Status[0] = TAX_BSTAT_UNPAID;
      // When bill was sent to default, total due is set to blank.  We have to set TotalDue=TotalTax+Fee+Pen
      if (*apTokens[TS_FIRSTTOTALDUE] > '0')
         dTotalDue = atof(apTokens[TS_FIRSTTOTALDUE]) + atof(apTokens[TS_SECONDTOTALDUE]);
      else
         dTotalDue = dTotalTax+dTotalFee+dPen1+dPen2; 
   }

   // Paid
   if (dPaid1 > 0)
      sprintf(pBaseRec->PaidAmt1, "%.2f", dPaid1);
   if (dPaid2 > 0)
      sprintf(pBaseRec->PaidAmt2, "%.2f", dPaid2);

   // Total due
   if (dTotalDue > 0.0)
      sprintf(pBaseRec->TotalDue, "%.2f", dTotalDue);

   pBaseRec->isSecd[0] = '0';
   pBaseRec->isSupp[0] = '1';
   pBaseRec->BillType[0] = BILLTYPE_SECURED_SUPPL;

   return 0;
}

int Sbx_Load_TaxSuppMain(char *pTaxSupp, FILE *fdBase)
{
   char     *pTmp, acBaseBuf[1024], acRec[MAX_RECSIZE];
   int      iRet, lOut=0, lCnt=0, lDrop=0;
   FILE     *fdIn;
   TAXBASE  *pTaxBase = (TAXBASE *)&acBaseBuf[0];

   LogMsg0("Loading tax supplemental file");

   // Open input file
   LogMsg("Open tax supp file %s", pTaxSupp);
   fdIn = fopen(pTaxSupp, "r");
   if (fdIn == NULL)
   {
      LogMsg("***** Error opening tax supp file: %s\n", pTaxSupp);
      return -2;
   }  
   // Drop header record
   pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdIn);

   // Merge loop 
   while (!feof(fdIn))
   {
      pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdIn);
      if (!pTmp)
         break;

      // Create new R01 record
      iRet = Sbx_ParseTaxSupp(acBaseBuf, acRec);
      if (!iRet)
      {
         // Create TaxBase record
         Tax_CreateTaxBaseCsv(acRec, (TAXBASE *)&acBaseBuf);
         lOut++;
         fputs(acRec, fdBase);
      } else 
      {      
         if (iRet > 0)
         {
            if (bDebug)
            {
               if (iRet == 1)
                  LogMsg("---> Drop supp record TotalTax=%.2f [%s]", pTaxBase->dTotalTax, acRec); 
               else
                  LogMsg("---> Drop supp record of prior year [%s]", acRec); 
            }
         }
         lDrop++;
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdIn)
      fclose(fdIn);

   LogMsg("Total records processed:    %u", lCnt);
   LogMsg("        output records:     %u", lOut);
   LogMsg("          drop records:     %u\n", lDrop);

   iRet = 0;

   return iRet;
}

/***************************** Sbx_ParseTaxBase ******************************
 *
 * Parse TAXR46M record to Base
 *
 *****************************************************************************/

int Sbx_ParseTaxBase(char *pBaseBuf, char *pInbuf)
{
   double   dTax1, dTax2, dPen1, dPen2, dTotalFee, dTotalDue, dPaid1, dPaid2;
   int      iTmp;
   TAXBASE    *pBaseRec = (TAXBASE *)pBaseBuf;

   // Parse input tax data
   iTokens = ParseStringIQ(pInbuf, '|', MAX_FLD_TOKEN, apTokens);
   if (iTokens < TM_CORRECTEDBILLFLAG)
   {
      LogMsg("***** Error: bad TAXR46M record for APN=%.*s (#tokens=%d)", iApnLen, pInbuf, iTokens);
      return -1;
   } 

   // Clear output buffer
   memset(pBaseBuf, 0, sizeof(TAXBASE));
   iTmp = atol(apTokens[TM_TAXYEAR]);
   if (iTmp < lTaxYear)
      return 2;

   // Total Tax 
   pBaseRec->dTotalTax = atof(apTokens[TM_TOTALTAX]);
   if (pBaseRec->dTotalTax > 0.0)
      sprintf(pBaseRec->TotalTaxAmt, "%.2f", pBaseRec->dTotalTax);
   else
      return 1;

   // APN
   strcpy(pBaseRec->Apn, apTokens[TM_PARCELNUMBER]);

   // TRA
   strcpy(pBaseRec->TRA, apTokens[TM_TAXRATEAREA]);

   // BillNum
   strcpy(pBaseRec->BillNum, apTokens[TM_BILLNUMBER]);

   // Tax Year
   strcpy(pBaseRec->TaxYear, apTokens[TM_TAXYEAR]);

   // Tax Amount
   dTax1 = atof(apTokens[TM_FIRSTINSTALLTAX]);
   dTax2 = atof(apTokens[TM_SECONDINSTALLTAX]);
   sprintf(pBaseRec->TaxAmt1, "%.2f", dTax1);
   sprintf(pBaseRec->TaxAmt2, "%.2f", dTax2);

   // Penalty
   dPen1 = atof(apTokens[TM_FIRSTPENALTY]);
   if (dPen1 > 0.0)
      sprintf(pBaseRec->PenAmt1, "%.2f", dPen1);
   dPen2 = atof(apTokens[TM_SECONDPENALTY]);
   if (dPen2 > 0.0)
      sprintf(pBaseRec->PenAmt2, "%.2f", dPen2);

   // Delinquent cost
   dTotalFee = atof(apTokens[TM_FIRSTCOST]);
   if (dTotalFee > 0.0)
      sprintf(pBaseRec->TotalFees, "%.2f", dTotalFee);

   // Due Date
   dateConversion(apTokens[TM_FIRSTDELINQUENTDATE],  pBaseRec->DueDate1, YYYY_MM_DD);
   dateConversion(apTokens[TM_SECONDDELINQUENTDATE], pBaseRec->DueDate2, YYYY_MM_DD);

#ifdef _DEBUG
   //if (!memcmp(pBaseRec->Apn, "099390004", 9))
   //   dPaid1 = 0;
#endif

   dPaid1=dPaid2=dTotalDue = 0;
   if (*apTokens[TM_INSTALLCODE] == '3')           // Both installments Paid
   {
      dateConversion(apTokens[TM_FIRSTINSTALLPAIDDATE],  pBaseRec->PaidDate1, YYYY_MM_DD);
      dateConversion(apTokens[TM_SECONDINSTALLPAIDDATE], pBaseRec->PaidDate2, YYYY_MM_DD);
      pBaseRec->Inst1Status[0] = TAX_BSTAT_PAID;
      pBaseRec->Inst2Status[0] = TAX_BSTAT_PAID;
      dPaid1 = atof(apTokens[TM_FIRSTPAYMENTS]);
      dPaid2 = atof(apTokens[TM_SECONDPAYMENTS]);
   } else if (*apTokens[TM_INSTALLCODE] == '2')    // Second Installment Unpaid
   {
      dateConversion(apTokens[TM_FIRSTINSTALLPAIDDATE],  pBaseRec->PaidDate1, YYYY_MM_DD);
      dPaid1 = atof(apTokens[TM_FIRSTPAYMENTS]);
      dTotalDue = atof(apTokens[TM_SECONDTOTALDUE]);
      pBaseRec->Inst1Status[0] = TAX_BSTAT_PAID;
      pBaseRec->Inst2Status[0] = TAX_BSTAT_UNPAID;
   } else                                          // First and second Installment unpaid
   {
      pBaseRec->Inst1Status[0] = TAX_BSTAT_UNPAID;
      pBaseRec->Inst2Status[0] = TAX_BSTAT_UNPAID;
      dTotalDue = atof(apTokens[TM_FIRSTTOTALDUE]) + atof(apTokens[TM_SECONDTOTALDUE]);
   }

   // Paid
   if (dPaid1 > 0)
      sprintf(pBaseRec->PaidAmt1, "%.2f", dPaid1);
   if (dPaid2 > 0)
      sprintf(pBaseRec->PaidAmt2, "%.2f", dPaid2);

   // Total due
   if (dTotalDue > 0.0)
      sprintf(pBaseRec->TotalDue, "%.2f", dTotalDue);

   if (*apTokens[TM_BILLTYPE] == 'S')
   {
      pBaseRec->isSecd[0] = '1';
      pBaseRec->BillType[0] = BILLTYPE_SECURED;
   } else
      LogMsg("*** Unknown BillType: %s", apTokens[TM_BILLTYPE]);

   if (*apTokens[TM_CORRECTEDBILLFLAG] == 'Y')
      pBaseRec->BillType[0] = BILLTYPE_ROLL_CORRECTION;

   return 0;
}

/******************************* Sbx_Load_TaxBase ****************************
 *
 * Load tax file TAXR46M.TXT to populate TaxBase table
 *
 * Return 0 if success.
 *
 *****************************************************************************/

int Sbx_Load_TaxBase(char *pTaxSupp, bool bImport)
{
   char     *pTmp, acBaseBuf[1024], acRec[MAX_RECSIZE], acBaseFile[_MAX_PATH], acInFile[_MAX_PATH];

   int      iRet, lOut=0, lCnt=0, lDrop=0;
   FILE     *fdBase, *fdIn;
   TAXBASE  *pTaxBase = (TAXBASE *)&acBaseBuf[0];

   LogMsg0("Loading base tax file");

   // TaxMain file needs rebuild & resort since records may be broken
   GetIniString(myCounty.acCntyCode, "TaxMain", "", acInFile, _MAX_PATH, acIniFile);
   // Save file date
   lLastTaxFileDate = getFileDate(acInFile);
   // Only process if new tax file
   iRet = isNewTaxFile(NULL, myCounty.acCntyCode);
   if (iRet <= 0)
   {
      lLastTaxFileDate = 0;
      return iRet;
   }

   // Rebuild
   sprintf(acRec, "%s\\SBX\\Sbx_TaxM.txt", acTmpPath);
   iRet = RebuildCsv(acInFile, acRec, cDelim, TM_CORRECTEDBILLFLAG);
   if (iRet < 100000)
      return -1;

   // Sort
   sprintf(acBaseBuf, "%s\\SBX\\Sbx_TaxM.srt", acTmpPath);
   iRet = sortFile(acRec, acBaseBuf, "S(#1,C,A,#3,C,A) DEL(124) B(1225,R)");
   if (iRet < 100000)
      return -2;

   // Open input file
   LogMsg("Open main tax file %s", acBaseBuf);
   fdIn = fopen(acBaseBuf, "r");
   if (fdIn == NULL)
   {
      LogMsg("***** Error opening main tax file: %s\n", acBaseBuf);
      return -2;
   }  
   // Drop header record
   pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdIn);

   // Open base file
   NameTaxCsvFile(acBaseFile, myCounty.acCntyCode, "Base");
   LogMsg("Open Base file %s", acBaseFile);
   fdBase = fopen(acBaseFile, "w");
   if (fdBase == NULL)
   {
      LogMsg("***** Error creating base file: %s\n", acBaseFile);
      return -4;
   }

   // Merge loop 
   while (!feof(fdIn))
   {
      pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdIn);
      if (!pTmp)
         break;

      // Create new R01 record
      iRet = Sbx_ParseTaxBase(acBaseBuf, acRec);
      if (!iRet)
      {
         // Create TaxBase record
         Tax_CreateTaxBaseCsv(acRec, (TAXBASE *)&acBaseBuf);
         lOut++;
         fputs(acRec, fdBase);
      } else
      {
         if (iRet > 0)
         {
            if (bDebug)
            {
               if (iRet == 1)
                  LogMsg("---> Drop base record TotalTax=%.2f [%s]", pTaxBase->dTotalTax, acRec); 
               else
                  LogMsg("---> Drop base record of prior year [%s]", acRec); 
            }
         }
         lDrop++;
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdIn)
      fclose(fdIn);

   LogMsg("Total records processed:    %u", lCnt);
   LogMsg("         output records:    %u", lOut);
   LogMsg("           drop records:    %u\n", lDrop);

   // Load tax supplement
   if (!_access(pTaxSupp, 0))
      iRet = Sbx_Load_TaxSuppMain(pTaxSupp, fdBase);
   if (fdBase)
      fclose(fdBase);

   // Import into SQL
   if (bImport)
   {
      iRet = doTaxImport(myCounty.acCntyCode, TAX_BASE);
   } else
      iRet = 0;

   return iRet;
}

/***************************** Sbx_ParseTaxDetail ****************************
 *
 * Parsing Special Assessment record TAXR46F
 *
 *****************************************************************************/

int Sbx_ParseTaxDetail(char *pDetailBuf, char *pAgencyBuf, char *pInbuf)
{
   TAXDETAIL *pDetail = (TAXDETAIL *)pDetailBuf;
   TAXAGENCY *pAgency = (TAXAGENCY *)pAgencyBuf;
   TAXAGENCY *pTmpAgency;
   int       iTmp;

   // Parse input tax data
   iTokens = ParseStringIQ(pInbuf, cDelim, MAX_FLD_TOKEN, apTokens);
   if (iTokens < TF_FUNDAMOUNT)
   {
      LogMsg("***** Error: bad Tax Detail record for APN=%.*s (#tokens=%d)", iApnLen, pInbuf, iTokens);
      return -1;
   } 

   iTmp = atol(apTokens[TF_TAXYEAR]);
   if (iTmp < lTaxYear)
      return 2;

   // Clear output buffer
   memset(pDetailBuf, 0, sizeof(TAXDETAIL));
   memset(pAgencyBuf, 0, sizeof(TAXAGENCY));

   // APN
   strcpy(pDetail->Apn, apTokens[TF_PARCELNUMBER]);

   // Tax Year 
   strcpy(pDetail->TaxYear, apTokens[TF_TAXYEAR]);

   // BillNum
   strcpy(pDetail->BillNum, apTokens[TF_BILLNUMBER]);

   // Tax Amt
   pDetail->dTaxAmt = atof(apTokens[TF_FUNDAMOUNT]);
   if (pDetail->dTaxAmt > 0)
      sprintf(pDetail->TaxAmt, "%.2f", pDetail->dTaxAmt);
   else
      return 1;


   // Agency 
   strcpy(pDetail->TaxCode, apTokens[TF_FUNDNUMBER]);
   strcpy(pAgency->Code, pDetail->TaxCode);
   _strupr(apTokens[TF_FUNDNAME]);
   strcpy(pDetail->TaxDesc, apTokens[TF_FUNDNAME]);
   strcpy(pAgency->Agency, apTokens[TF_FUNDNAME]);
   
   // Tax Rate
   strcpy(pDetail->TaxRate, apTokens[TF_FUNDRATE]);
   // We don't want to put tax rate on agency table since same code may have different rate
   //strcpy(pAgency->TaxRate, pDetail->TaxRate);

   // Phone Number
   if (*apTokens[TF_PHONENUMBER] > ' ')
      sprintf(pAgency->Phone, "(%.3s)%.3s-%s", apTokens[TF_PHONENUMBER], apTokens[TF_PHONENUMBER]+3, apTokens[TF_PHONENUMBER]+6); 

   pTmpAgency = findTaxAgency(pAgency->Code);
   if (pTmpAgency)
   {
      strcpy(pAgency->Agency,  pTmpAgency->Agency);
      pAgency->TC_Flag[0] = pTmpAgency->TC_Flag[0];
      pDetail->TC_Flag[0] = pTmpAgency->TC_Flag[0];
   } else
   {
      LogMsg("New Agency: %s|%s|%s|%s||0", pAgency->Code, pAgency->Agency, pAgency->Phone, pDetail->TaxRate);
      pDetail->TC_Flag[0] = '0';
      pAgency->TC_Flag[0] = '0';
      iNewAgency++;
   }

   return 0;
}

/************************* Sbx_Load_TaxSuppDetail ****************************
 *
 * Load tax supplement file SOBR46F.TXT
 *
 *****************************************************************************/

int Sbx_Load_TaxSuppDetail(char *pTaxSupp, FILE *fdItems, FILE *fdAgency)
{
   char     *pTmp, acItemsRec[MAX_RECSIZE], acAgencyRec[512], acRec[512];
   int      iRet;
   long     lItems=0, lCnt=0, lAgency=0;
   FILE     *fdIn;
   TAXDETAIL *pTax    = (TAXDETAIL *)&acItemsRec[0];
   TAXAGENCY *pAgency = (TAXAGENCY *)&acAgencyRec[0];

   LogMsg0("Loading Detail Supplemental file");

   // Open input file
   LogMsg("Open Detail Supplemental file %s", pTaxSupp);
   fdIn = fopen(pTaxSupp, "r");
   if (fdIn == NULL)
   {
      LogMsg("***** Error opening Detail Supplemental file: %s\n", pTaxSupp);
      return -2;
   }  

   // Drop header rec & get first rec
   pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdIn);
   pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdIn);

   // Merge loop 
   while (!feof(fdIn))
   {
      // Create Items & Agency record
      iRet = Sbx_ParseTaxDetail(acItemsRec, acAgencyRec, acRec);
      if (!iRet)
      {
         // Create TaxBase record
         Tax_CreateDetailCsv(acRec, (TAXDETAIL *)&acItemsRec);
         lItems++;
         fputs(acRec, fdItems);

         // Create Agency record
         if (pAgency->Agency[0] > ' ')
         {
            Tax_CreateAgencyCsv(acRec, (TAXAGENCY *)&acAgencyRec);
            fputs(acRec, fdAgency);
         }
      } else if (iRet == 1)
      {
         if (bDebug)
            LogMsg("---> Drop no tax amt Detail Supplemental record %d [%.80s]", lCnt, acRec); 
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);

      pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdIn);
   }

   // Close files
   if (fdIn)
      fclose(fdIn);

   LogMsg("Total records processed:    %u", lCnt);
   LogMsg("      Items records:        %u", lItems);
   LogMsg("      New Agencies:         %u", iNewAgency);

   return 0;
}

/**************************** Sbx_Load_TaxDetail ******************************
 *
 * Loading TAXR46F.TXT to populate Tax_Items & Tax_Agency
 *
 * Return 0 if success.
 *
 *****************************************************************************/

int Sbx_Load_TaxDetail(char *pTaxSupp, bool bImport)
{
   char     *pTmp, acItemsRec[MAX_RECSIZE], acAgencyRec[512], acRec[512];
   char     acItemsFile[_MAX_PATH], acAgencyFile[_MAX_PATH], acTmpFile[_MAX_PATH], acInFile[_MAX_PATH];

   int      iRet;
   long     lItems=0, lCnt=0, lAgency=0;
   FILE     *fdItems, *fdIn, *fdAgency;
   TAXDETAIL *pTax    = (TAXDETAIL *)&acItemsRec[0];
   TAXAGENCY *pAgency = (TAXAGENCY *)&acAgencyRec[0];

   LogMsg0("Loading Detail file");

   GetIniString(myCounty.acCntyCode, "TaxDetail", "", acInFile, _MAX_PATH, acIniFile);
   NameTaxCsvFile(acItemsFile, myCounty.acCntyCode, "Items");
   NameTaxCsvFile(acAgencyFile, myCounty.acCntyCode, "Agency");

   // Open input file
   LogMsg("Open Detail file %s", acInFile);
   fdIn = fopen(acInFile, "r");
   if (fdIn == NULL)
   {
      LogMsg("***** Error opening Detail file: %s\n", acInFile);
      return -2;
   }  

   // Open Items file
   LogMsg("Append to Items file %s", acItemsFile);
   fdItems = fopen(acItemsFile, "w");
   if (fdItems == NULL)
   {
      LogMsg("***** Error creating Items file: %s\n", acItemsFile);
      return -4;
   }

   // Open Agency file
   LogMsg("Append to Agency file %s", acAgencyFile);
   fdAgency = fopen(acAgencyFile, "w");
   if (fdAgency == NULL)
   {
      LogMsg("***** Error Appending to Agency file: %s\n", acAgencyFile);
      return -4;
   }

   // Drop header rec & get first rec
   pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdIn);
   pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdIn);

   // Merge loop 
   while (!feof(fdIn))
   {
      // Create Items & Agency record
      iRet = Sbx_ParseTaxDetail(acItemsRec, acAgencyRec, acRec);
      if (!iRet)
      {
         // Create TaxBase record
         Tax_CreateDetailCsv(acRec, (TAXDETAIL *)&acItemsRec);
         lItems++;
         fputs(acRec, fdItems);

         // Create Agency record
         if (pAgency->Agency[0] > ' ')
         {
            Tax_CreateAgencyCsv(acRec, (TAXAGENCY *)&acAgencyRec);
            fputs(acRec, fdAgency);
         }
      } else if (iRet == 1)
      {
         if (bDebug)
            LogMsg("---> Drop no tax amt detail record %d [%.80s]", lCnt, acRec); 
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);

      pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdIn);
   }

   // Close files
   if (fdIn)
      fclose(fdIn);

   LogMsg("Total records processed:    %u", lCnt);
   LogMsg("      Items records:        %u", lItems);
   LogMsg("      New Agencies:         %u", iNewAgency);

   // Load tax supplement
   if (!_access(pTaxSupp, 0))
      iRet = Sbx_Load_TaxSuppDetail(pTaxSupp, fdItems, fdAgency);

   if (fdItems)
      fclose(fdItems);
   if (fdAgency)
      fclose(fdAgency);

   // Import into SQL
   if (bImport)
   {
      iRet = doTaxImport(myCounty.acCntyCode, TAX_DETAIL);
      if (!iRet)
      {
         // Dedup Agency file before import
         sprintf(acTmpFile, "%s\\%s\\%s_Agency.tmp", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);

         LogMsg("Dedup Agency file %s", acAgencyFile);
         iRet = sortFile(acAgencyFile, acTmpFile, "S(#1,C,A) DUPOUT F(TXT)");
         if (iRet > 0)
         {
            CopyFile(acTmpFile, acAgencyFile, false);
            iRet = doTaxImport(myCounty.acCntyCode, TAX_AGENCY);
         }
      }
   } else
      iRet = 0;

   return iRet;
}

/**************************** Sbx_Load_TaxDelq *******************************
 *
 *
 * Return 0 if success.
 *
 *****************************************************************************/

int Sbx_ParseTaxDelq(char *pOutbuf, char *pInbuf)
{
   char     *pTmp;
   double   dTaxAmt, dPenAmt, dFees, dPaidAmt, dTotalDue;
   TAXDELQ  *pOutRec = (TAXDELQ *)pOutbuf;

   iTokens = ParseStringIQ(pInbuf, cDelim, MAX_FLD_TOKEN, apTokens);
   if (iTokens < TR_CANCELLEDDATE)
   {
      LogMsg("***** Sbx_ParseTaxDelq -->Bad tax record 1 (%d)", iTokens);
      return -1;
   }

   // APN
   strcpy(pOutRec->Apn, apTokens[TR_PARCELNUMBER]);

   // Only process RED record
   if (!memcmp(apTokens[TR_BILLTYPE], "SEC", 3))
   {
      pOutRec->isSecd[0] = '1';
      pOutRec->isSupp[0] = '0';
      return 1;
   } else if (!memcmp(apTokens[TR_BILLTYPE], "SUP", 3))
   {
      pOutRec->isSupp[0] = '1';
      pOutRec->isSecd[0] = '0';
      return 1;
   }

   if (*apTokens[TR_CANCELLEDDATE] >= '0')
      return 1;

   // Default Number
   strcpy(pOutRec->BillNum, apTokens[TR_BILLNUMBER]);

   // Default date
   pTmp = dateConversion(apTokens[TR_SOLDTOSTATEDATE], pOutRec->Def_Date, YYYY_MM_DD);
   if (!pTmp)
      LogMsg("*** Invalid default date: APN=%s, Date=%s", pOutRec->Apn, apTokens[TR_SOLDTOSTATEDATE]);

   // Tax year
   strcpy(pOutRec->TaxYear, apTokens[TR_TAXYEAR]);

   // Delq Amt
   dTaxAmt = atof(apTokens[TR_TAX]);
   dPenAmt = atof(apTokens[TR_DELINQUENTPENALTY]);
   dPenAmt += atof(apTokens[TR_ADDITIONALPENALTY]);
   dFees = atof(apTokens[TR_COST]);
   dFees += atof(apTokens[TR_BADCHECKFEE]);
   dFees += atof(apTokens[TR_REDEMPTIONFEE]);
   dTotalDue = atof(apTokens[TR_TOTALDUE]);
   sprintf(pOutRec->Def_Amt, "%.2f", dTaxAmt);
   sprintf(pOutRec->Pen_Amt, "%.2f", dPenAmt);
   sprintf(pOutRec->Fee_Amt, "%.2f", dFees);
   sprintf(pOutRec->Red_Amt, "%.2f", dTotalDue);

#ifdef _DEBUG
   //if (!memcmp(pOutRec->Apn, "017082002", 9))
   //   dPaidAmt = 0;
#endif

   dPaidAmt = atof(apTokens[TR_PAYMENTS]);
   if (dPaidAmt > 0)
      dateConversion(apTokens[TR_PAIDINFULLDATE], pOutRec->Red_Date, YYYY_MM_DD);

   // Paid Status
   pOutRec->isDelq[0] = '0';
   if (*apTokens[TR_ACTIVEPAYMENTPLAN] == 'Y')
      pOutRec->DelqStatus[0] = TAX_STAT_ONPAYMENT;
   else if (dTotalDue > 0.0)
   {
      pOutRec->DelqStatus[0] = TAX_STAT_UNPAID;
      pOutRec->isDelq[0] = '1';
   } else
      pOutRec->DelqStatus[0] = TAX_STAT_REDEEMED;

   return 0;      
}

int Sbx_Load_TaxDelq(bool bImport)
{
   char     *pTmp, acBuf[MAX_RECSIZE], acRec[MAX_RECSIZE], acTmp[MAX_RECSIZE],
            acTmpFile[_MAX_PATH], acOutFile[_MAX_PATH], acInFile[_MAX_PATH];

   int      iRet;
   long     lOut=0, lCnt=0;
   FILE     *fdOut, *fdIn;
   TAXDELQ  *pOutRec = (TAXDELQ *)acBuf;

   LogMsg0("Loading Redemption file");

   NameTaxCsvFile(acOutFile, myCounty.acCntyCode, "Delq");
   GetIniString(myCounty.acCntyCode, "Redemption", "", acInFile, _MAX_PATH, acIniFile);
   lLastFileDate = getFileDate(acInFile);

   // Sort input file ParcelNumber, TaxYear, BillNumber
   sprintf(acTmpFile, "%s\\%s\\%s_Delq.srt", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   iRet = sortFile(acInFile, acTmpFile, "S(#1,C,A,#5,C,A,#2,C,A,#4,C,D,#3,C,A) DEL(124) DUPOUT F(TXT)");

   // Open input file
   LogMsg("Open delinquent tax file %s", acTmpFile);
   fdIn = fopen(acTmpFile, "r");
   if (fdIn == NULL)
   {
      LogMsg("***** Error opening delinquent tax file: %s\n", acTmpFile);
      return -2;
   }  

   // Open Output file
   LogMsg("Open output file %s", acOutFile);
   fdOut = fopen(acOutFile, "w");
   if (fdOut == NULL)
   {
      LogMsg("***** Error creating output file: %s\n", acOutFile);
      return -4;
   }

   // Merge loop    
   iRet = 0;
   memset(acBuf, 0, sizeof(TAXDELQ));
   while (!feof(fdIn))
   {
      if (!(pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdIn)))
         break;
#ifdef _DEBUG
      //if (!memcmp(acRec, "017082002", 9))
      //   iRet = 0;
#endif

      iRet = Sbx_ParseTaxDelq(acBuf, acRec);
      if (!iRet)
      {
         sprintf(pOutRec->Upd_Date, "%d", lLastFileDate);
         Tax_CreateDelqCsv(acTmp, (TAXDELQ *)pOutRec);

         // Output record			
         fputs(acTmp, fdOut);
         lOut++;
         memset(acBuf, 0, sizeof(TAXDELQ));
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdIn)
      fclose(fdIn);
   if (fdOut)
      fclose(fdOut);

   LogMsg("Total records processed:          %u", lCnt);
   LogMsg("Total delinquent records output:  %u\n", lOut);

   // Import into SQL
   if (bImport && lOut > 0)
   {
      iRet = doTaxImport(myCounty.acCntyCode, TAX_DELINQUENT);
   } else
      iRet = 0;

   return iRet;
}

/*********************************** loadSbx ********************************
 *
 * Run LoadOne with following options:
 *    -L -Xl -Ms [-Ma] -X8 -Mr         : to load lien
 *    -U [-Ms] [-Ma] [-Xsi] [-T] [-Mr] : to load update roll
 *
 ****************************************************************************/

int loadSbx(int iSkip)
{
   int   iRet=0;
   char  acTmpFile[_MAX_PATH], acWebSale[_MAX_PATH];

   iApnLen = myCounty.iApnLen;

   if (lOptProp8 == MYOPT_EXT)
      iRet = Sbx_ExtrProp8_1();

   // Load tax
   if (iLoadTax == TAX_LOADING)                    // -T or -Lt
   {
      iRet = GetIniString(myCounty.acCntyCode, "Main_TC", "", acTmpFile, _MAX_PATH, acIniFile);
      if (iRet > 5)
      {
         TC_SetDateFmt(MM_DD_YYYY_1, true);
         iRet = TC_LoadTax(myCounty.acCntyCode, bTaxImport);
      } else
      {
         iRet = GetIniString(myCounty.acCntyCode, "TaxDist", "", acTmpFile, _MAX_PATH, acIniFile);
         if (iRet > 0)
            iRet = LoadTaxCodeTable(acTmpFile);

         // SBX tax base
         GetIniString(myCounty.acCntyCode, "TaxSuppMain", "", acTmpFile, _MAX_PATH, acIniFile);
         iRet = Sbx_Load_TaxBase(acTmpFile, bTaxImport);

         if (!iRet && lLastTaxFileDate > 0)
         {
            // Load tax detail
            GetIniString(myCounty.acCntyCode, "TaxSuppDetail", "", acTmpFile, _MAX_PATH, acIniFile);
            iRet = Sbx_Load_TaxDetail(acTmpFile, bTaxImport);

            // Load redemption file
            iRet = Sbx_Load_TaxDelq(bTaxImport);

            // Update Delq flag tb_isDelq
            if (!iRet)
               iRet = updateDelqFlag(myCounty.acCntyCode, true, true, 1);
         }
      }
   }

   // Exit if load/update tax only
   if (!iLoadFlag)
      return iRet;

   //iRet = DedupCumSale(acCSalFile);

   /* Extract sales from roll file
   pTmp = "G:\\CO_DATA\\SBX\\LDR\\2005 Secured Assessment Roll.txt";
   iRet = Sbx_ExtrRollSale(pTmp, 2005);
   pTmp = "G:\\CO_DATA\\SBX\\LDR\\2006 SBCClosedRoll.txt";
   iRet = Sbx_ExtrRollSale(pTmp, 2006);
   pTmp = "G:\\CO_DATA\\SBX\\LDR\\Santa BarbaraCounty2007ClosedRoll.txt";
   iRet = Sbx_ExtrRollSale(pTmp, 2007);
   pTmp = "G:\\CO_DATA\\SBX\\LDR\\2008 Closed Roll.txt";
   iRet = Sbx_ExtrRollSale(pTmp, 2008);
   pTmp = "G:\\CO_DATA\\SBX\\LDR\\Santa Barbara County 2009 Certified Secured Roll.txt";
   iRet = Sbx_ExtrRollSale(pTmp, 2009);
   pTmp = "G:\\CO_DATA\\SBX\\LDR\\Santa Barbara Co CloseRoll_2010.txt";
   iRet = Sbx_ExtrRollSale(pTmp, 2010);
   pTmp = "G:\\CO_DATA\\SBX\\LDR\\Santa Barbara County 2011 Secured Assessment Roll.txt";
   iRet = Sbx_ExtrRollSale(pTmp, 2011);
   pTmp = "G:\\CO_DATA\\SBX\\LDR\\Santa Barbara Secured 2012 Closed Roll.txt";
   iRet = Sbx_ExtrRollSale(pTmp, 2012);
   pTmp = "G:\\CO_DATA\\SBX\\LDR\\2013 Santa Barbara County Secured Assessment Roll.txt";
   iRet = Sbx_ExtrRollSale(pTmp, 2013);
   */

   if (iLoadFlag & EXTR_SALE)                      // -Xs
   {
      // Extract sale from roll file
      iRet = Sbx_ExtrRollSale(acRollFile);
      if (iRet)
         return iRet;

      // Extract sales from Sbx_HtmlExtr.sls
      // Rename file to Sbx_HtmlExtr_yyyymmdd.dat after processing to avoid unneccessary reload
      sprintf(acWebSale, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "web");
      iRet = Sbx_ExtrWebSale(acWebSale, false);
      if (iRet)
         return iRet;

      if (!_access(acWebSale, 0))
      {
         // Merge roll sale with web sale to create Sbx_Sale.sls
         iRet = Sbx_MergeSaleFiles(acRollSale, acWebSale, acCSalFile, false);
         if (!iRet)
         {
            iRet = DedupCumSale(acCSalFile);
            iLoadFlag |= MERG_CSAL;
            iLoadFlag |= MERG_ATTR;
         } else
            return iRet;
      } else
         LogMsg("*** Missing web sale file: %s", acWebSale);
   }

   if (iLoadFlag & EXTR_LIEN)                      // -Xl
   {
      LogMsgD("Extract %s Lien file", myCounty.acCntyCode);
      // iRet = Sbx_ExtrLien1();    // 2015

      sprintf(acTmpFile, "%s\\SBX\\Sbx_Lien.txt", acTmpPath);
      iRet = RebuildCsv(acRollFile, acTmpFile, ',', SBX_ROLL_FLDS);
      strcpy(acRollFile, acTmpFile);
      iRet = Sbx_ExtrLien2();       // 2016
   }

   if (iLoadFlag & LOAD_LIEN)                      // -L
   {
      // Create Lien file
      LogMsgD("Load %s Lien file", myCounty.acCntyCode);

      iRet = Sbx_Load_LDR2(iSkip);
   } else if (iLoadFlag & LOAD_UPDT)
   {
      sprintf(acTmpFile, "%s\\SBX\\Sbx_Roll.txt", acTmpPath);
      iRet = RebuildCsv(acRollFile, acTmpFile, ',', SBX_UPDT_BATHROOMS+1);

      // Sort roll file
      LogMsg("Sort roll file: %s", acTmpFile);
      sprintf(acRollFile, "%s\\SBX\\Sbx_Roll.srt", acTmpPath);
      iRet = sortFile(acTmpFile, acRollFile, "S(#1,C,A) OMIT(2,1,C,GT,\"9\") F(TXT) DUPO(#1) ");

      LogMsg("Load %s roll update file", myCounty.acCntyCode);
      iRet = Sbx_Load_Roll(iSkip);
   }

   // Apply cum sale file to R01
   if (!iRet && (iLoadFlag & MERG_CSAL) )          // -Ms
   {
      // Apply Sbx_Sale.sls to R01 file
      //iRet = ApplyCumSaleDN(iSkip, acCSalFile, false, SALE_USE_SCSALREC, true);
      iRet = ApplyCumSale(iSkip, acCSalFile, false, SALE_USE_SCUPDXFR, true);
   }

   // Update chars extracted from the web
   if (!iRet && (iLoadFlag & MERG_ATTR))           // -Ma
   {
      if (!_access(acCChrFile, 0))
      {
         iRet = Sbx_MergeCChr(iSkip);
      } else
         LogMsgD("Missing chars file: %s", acCChrFile);
   }

   return iRet;
}