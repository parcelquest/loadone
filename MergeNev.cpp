/******************************************************************************
 *
 * Input files:
 *    - Asrech         70+1_A     +1=LF  Copyfile to sale
 *    - ASPUBROLL     600+1_A     +1=LF  Copyfile to roll
 *    - ASDESCROLL     50+1_A     +1=LF  Copyfile to char
 * 
 * Notes: - Do not take voided parcels (Status='V')
 *        - If mail street is the same as situs street, use mail city, zip for situs.
 *        - If not, use situs community to translate.
 *      
 *
 * To be reviewed:
 *    - Mail address
 *    - Owner name
 *
 * To be done:
 *    - Use CHARS from NCALLS1.TXT file
 *
 * Operation commands:
 *    - Normal update:  LoadOne -CNEV -U -Us
 *    - Load Lien:      LoadOne -CNEV -L -Xl -Us|-Ms -Xv
 *
 * Revision:
 * 08/15/2006  TCB
 * 09/15/2006  SPN      First release version
 * 01/24/2008 1.5.3.14  Add code to support standard usecode
 * 02/25/2008 1.5.5.1   Adding code to create update file
 * 03/24/2008 1.5.7.2   Format IMAPLINK.
 * 03/25/2008 1.5.8     Remove retired parcels
 * 07/01/2008 8.0.1     Adding Nev_LienExtr()
 * 10/01/2008 8.3.1     Change situs addr logic.  If mail street and number are the 
 *                      same as situs, use mail city & zip.  Otherwise translate
 *                      community code to city.  Also keep original owner name as county provided.
 * 01/17/2009 8.5.6     Fix TRA
 * 03/17/2009 8.7.1     Format STORIES as 99.9.  We no longer multiply by 10.
 * 06/18/2009 8.8.5     Modify Nev_MergeRoll() and Nev_CreateLienRec() to populate Fixture, PP_Val, 
 *                      and Full Exe flag.  Wait for Reaason code to populate Prop8 flag.
 * 07/28/2009 9.1.4     Clean up code.
 * 08/19/2009 9.1.6     Check for blank APN and drop them.  This can cause system crashed.
 * 09/30/2009 9.2.2     Modify Nev_MergeChar() to correct full/half baths, garage, 
 *                      a/c & heating, and pool.  Also adding View data.
 * 11/23/2009 9.3.2     Fix Situs Unit issue in Nev_MergeSAdr()
 * 01/13/2010 9.3.7     Fix sale price.  If stamp amt is 0, reset sale price to blank.
 * 02/23/2010 9.4.3     Fix CARE_OF issue.
 * 03/29/2010 9.5.1.1   Fix half bath bug in Nev_MergeChar() that puts half bath value in full bath field.
 *                      Add -Us option and Nev_UpdateCumSale() to accummulate sales from NCALLS1.TXT
 * 07/12/2010 10.1.1    Replace NEV_CHAR with STDCHAR for Nev_MergeChar() and Nev_ConvertChar().
 * 07/13/2010 10.1.2    Modify Load_Roll() to use acCChrFile instead of acCharFile.
 * 07/12/2011 11.0.2    Add S_HSENO.
 * 02/18/2013 12.4.6    Modify load CHAR & SALE functions to get delimeter from INI file
 *                      since county keeps changing them.
 * 09/11/1013 13.7.14   Add Etal flag, Topo code, Porch, Guest house, Misc impr, and basement sqft
 * 10/08/2013 13.10.4.2 Use updateVesting() to update Vesting and Etal flag.
 *                      Modify Nev_FmtChar() to prepare to add 1Q & 3Q bath.
 * 10/11/2013 13.11.0   Adding 1-4 Qbaths to R01.
 * 10/30/2013 13.11.7   Add Nev_MergeSitus() and use NCADDRSS.TXT to replace Nev_MergeSAdr(). 
 *                      StrName is more complete in this file than in roll file where it was cut off.
 * 04/30/2015 14.14.2   Add option to import value files.  Add createValueImport(), createValueOutrec(),
 *                      Nev_LoadValueFile(), Nev_ParseValues(), Nev_MergeValues(), Mev_ExtrCurValues, and 
 *                      Nev_ExtrRollValues() to create import value file.
 * 05/11/2015 14.14.3   Modify createValueOutrec() to include BaseYearVal & Misc.  Also modify
 *                      Nev_ParseValues() not to add base year record for each Prop8 one.
 * 05/26/2015 14.15.0   Remove createValueImport() & createValueOutrec() and use version from LoadValue.cpp instead
 * 06/07/2015 14.15.3   Modify not to include roll record into value extract file.
 * 06/30/2015 15.0.1    Modify Nev_ParseValues() to change set type value using logic desc at function level.
 * 08/10/2015 15.1.0    Fix -Us problem and modify Nev_ParseValues() to create extra base year record
 *                      when base year value is changed.
 * 07/14/2016 16.1.0    Assign "999" to USE_STD when there is no USECODE in Nev_MergeRoll()
 * 10/12/2016 16.3.3    Add Tax processing. Note that:
 *                         - Cost & Penalty are included in CHG.AMT
 * 							   - Tax Code are assigned to individual installment, may need to combine value and xlate taxcode.
 * 11/26/2016 16.7.3    Add Nev_ExtrSale() to extract sales from ASRECH.  This file now contains
 *                      more recent data than NCINDS1.TXT.  Nev_ExtrSale() will replace Nev_UpdateCumSale().
 * 03/03/2017 16.11.0   Add TC_Flag to Nev_ParseTaxDetail()
 * 03/28/2017 16.13.2   Fix minor bug in loadNev().
 * 04/06/2017 16.13.8   Modify Nev_ParseTaxDetail() to update TotalDue and adjust Tax1 when it includes penalty
 * 06/19/2017 16.15.5   Rewrite Nev_ParseTaxDetail() to combine Inst1 & 2 and use Code1 for it.
 * 07/30/2017 17.1.6    Modify Nev_ParseValues() to hard code of delimiter and create new Nev_QR_ParseValues()
 *                      to support new layout of RVAL & QVAL files. They are no longer the same as NVAL.
 * 08/01/2017 17.1.7    Fix bug (half bath) in Nev_MergeChar().
 * 01/03/2018 17.5.4    Modify Nev_ParseTaxDelq() to correct TaxYear & DelqYear, populate isSupp & isSecd flags,
 *                      remove letter 'S' from BillNum, and remove Redemption Amt. Use updateDelqFlag() to update
 *                      Delq info on Base table.
 * 05/17/2018 17.10.11  Modify Nev_ParseTaxBase() to add isSecd & BillType.
 * 07/31/2018 18.2.5    Modify Nev_MergeValues() to change delimiter for 2018 value file.
 *
 ******************************************************************************/

#include "stdafx.h"
#include "CountyInfo.h"
#include "hlAdo.h"
#include "Prodlib.h"
#include "R01.h"
#include "RecDef.h"
#include "Tables.h"
#include "Logs.h"
#include "LoadDef.h"
#include "Utils.h"
#include "XlatTbls.h"
#include "doSort.h"
#include "doOwner.h"
#include "formatApn.h"
#include "SaleRec.h"
#include "CharRec.h"
#include "LoadOne.h"
#include "MergeNev.h"
#include "UseCode.h"
#include "LOExtrn.h"
#include "PQ.h"
#include "LoadValue.h"
#include "Tax.h"

static   FILE  *fdChar, *fdSitus, *fdDelq;
static   int   iUseMailCity;
static   long  lCharMatch, lCharSkip, lSaleMatch, lSaleSkip, lSitusMatch, lSitusSkip;

/******************************** Nev_XlatUseCode ****************************
 *
 * Return pointer to the correct use code.
 *
 *****************************************************************************/

char *Nev_XlatUseCode(char *pCntyUse)
{
   int   iTmp;

   iTmp = 0;
   while (Nev_UseTbl3[iTmp].acCntyUse[0])
      if (!memcmp(Nev_UseTbl3[iTmp].acCntyUse, pCntyUse, 3))
         return (char *)&Nev_UseTbl3[iTmp].acStdUse[0];
      else
         iTmp++;

   iTmp = 0;
   while (Nev_UseTbl2[iTmp].acCntyUse[0])
      if (!memcmp(Nev_UseTbl2[iTmp].acCntyUse, pCntyUse, 2))
         return (char *)&Nev_UseTbl2[iTmp].acStdUse[0];
      else
         iTmp++;

   iTmp = 0;
   while (Nev_UseTbl1[iTmp].acCntyUse[0])
      if (Nev_UseTbl1[iTmp].acCntyUse[0] == *pCntyUse)
         return (char *)&Nev_UseTbl1[iTmp].acStdUse[0];
      else
         iTmp++;

   return NULL;
}

/******************************** Nev_MergeSitus *****************************
 *
 * Merge Situs address
 * Return 0 if successful, otherwise error.
 *
 *****************************************************************************/

int Nev_MergeSitus(char *pOutbuf, char cDelim)
{
   static   char acRec[512], *pRec=NULL;
   char     acTmp[256], acCode[32], acAddr1[256], *pTmp, *apItems[MAX_FLD_TOKEN],
            acStrSub[16], acUnit[16], acCity[32];
   int      iRet=0, iTmp, iStrNo;
   ADR_REC  sSitusAdr;

   if (!pRec)
   {
      // Get first rec
      do {
         pRec = fgets(acRec, 512, fdSitus);
      } while (acRec[1] != '0');
   }

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "001110003000", 9) || !memcmp(pOutbuf, "007113047", 9))
   //   iTmp = 0;
#endif

   do 
   {
      if (!pRec)
      {
         fclose(fdSitus);
         fdSitus = NULL;
         return 1;      // EOF 
      }

      // Add 1 to Situs rec to skip double quote
      iTmp = memcmp(pOutbuf, pRec+1, iApnLen);
      if (iTmp > 0)
      {
         if (bDebug)
            LogMsg0("Skip Situs rec: %s", pRec);
         pRec = fgets(acRec, 512, fdSitus);
         lSitusSkip++;
      }
   } while (iTmp > 0);

   // If not match, return
   if (iTmp)
      return 1;

   // Parse input
   iRet = ParseStringNQ(pRec, cDelim, NEV_S_CITY+1, apItems);
   if (iRet < NEV_S_CITY)
   {
      LogMsg("***** Error: bad situs record for APN=%.*s (#tokens=%d)", iApnLen, pOutbuf, iRet);
      return -1;
   }

   // Clear old data
   removeSitus(pOutbuf);

   // Ignore N/A
   if (!memcmp(apItems[NEV_S_STRNAME], "N/A", 3) || !memcmp(apItems[NEV_S_STRNAME], "   ", 3))
      return 0;

   acStrSub[0]=acUnit[0] = 0;

   // StrNum
   acAddr1[0] = 0;
   iStrNo = atol(apItems[NEV_S_STRNUM]);
   if (iStrNo < 1)
      return 0;

   // StrName
   strcpy(acTmp, apItems[NEV_S_STRNAME]);

   // Remove single quotes
   if (pTmp=strchr(acTmp, 39))
      remChar(acTmp, 39);

   if (pTmp=strchr(acTmp, '('))
      *pTmp = 0;

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "188100100", 9) )
   //   iTmp = 0;
#endif
   // Check for embeded unit#
   if (pTmp=strchr(acTmp, '*'))
   {
      if (!_memicmp(pTmp, "* Unit ", 7))
         strcpy(acUnit, pTmp+7);
      else if (!_memicmp(pTmp, "*Unit ", 6))
         strcpy(acUnit, pTmp+6);
      else if (!_memicmp(pTmp, "* Suite ", 8))
         strcpy(acUnit, pTmp+8);
      else if (!_memicmp(pTmp, "* Ste ", 6))     // PROVIDENCE MINE ROAD * STE 356A-H
         strcpy(acUnit, pTmp+6);
      else if (!_memicmp(pTmp, "* Apt ", 6))     // 
         strcpy(acUnit, pTmp+6);
      else if (!_memicmp(pTmp, "* Building", 7))
         iTmp = 0;   // Ignore this
      else if (!memcmp(pTmp, "* # ", 4))
      {
         strcpy(&acUnit[1], pTmp+4);
         acUnit[0] = '#';
      } 
      else if (!memcmp(pTmp, "* #", 3))
      {  // * #3 BUILD - 1881001000
         strcpy(acUnit, pTmp+2);

         char *pTmp1;
         if (pTmp1 = strchr(acUnit, ' '))
            *pTmp1 = 0;
      }
      else if (*(pTmp+3) == '/' && isdigit(*(pTmp+4)))
      {
         // Check 085105300
         strcpy(acStrSub, pTmp+2);
      } 
      else if (*(pTmp+3) == ' ' && isalpha(*(pTmp+2)))
      {
         acUnit[0] = *(pTmp+2);
         acUnit[1] = 0;
      } else
         iTmp = 0;

      *pTmp = 0;
   }

   parseAdr1N_1(&sSitusAdr, acTmp);
   if (iStrNo > 0)
   {
      iTmp = sprintf(acAddr1, "%d", iStrNo);
      memcpy(pOutbuf+OFF_S_STRNUM, acAddr1, iTmp);
      memcpy(pOutbuf+OFF_S_HSENO, acAddr1, iTmp);

      if (acStrSub[0] > ' ')
         strcpy(sSitusAdr.strSub, acStrSub);
      if (acUnit[0] > ' ')
         strcpy(sSitusAdr.Unit, acUnit);
   }

   if (sSitusAdr.strSub[0] > ' ')
   {
      strcat(acAddr1, " ");
      strcat(acAddr1, sSitusAdr.strSub);
      memcpy(pOutbuf+OFF_S_STR_SUB, sSitusAdr.strSub, strlen(sSitusAdr.strSub));
   }

   if (sSitusAdr.strDir[0] > ' ')
      memcpy(pOutbuf+OFF_S_DIR, sSitusAdr.strDir, strlen(sSitusAdr.strDir));

   if (sSitusAdr.strName[0] > ' ')
      memcpy(pOutbuf+OFF_S_STREET, sSitusAdr.strName, strlen(sSitusAdr.strName));

   if (sSitusAdr.SfxCode[0] > ' ')
      memcpy(pOutbuf+OFF_S_SUFF, sSitusAdr.SfxCode, strlen(sSitusAdr.SfxCode));

   if (sSitusAdr.Unit[0] > ' ')
      memcpy(pOutbuf+OFF_S_UNITNO, sSitusAdr.Unit, strlen(sSitusAdr.Unit));

   if (iStrNo > 0)
   {
      if (sSitusAdr.Unit[0] > ' ')
         sprintf(acAddr1, "%s %s %s %s #%s", acAddr1, sSitusAdr.strDir, sSitusAdr.strName, sSitusAdr.strSfx, sSitusAdr.Unit);
      else
         sprintf(acAddr1, "%s %s %s %s", acAddr1, sSitusAdr.strDir, sSitusAdr.strName, sSitusAdr.strSfx);

      blankRem(acAddr1);
      memcpy(pOutbuf+OFF_S_ADDR_D, acAddr1, strlen(acAddr1));
   }

   // If situs and mailing are the same, use mail city and zip
   if (!memcmp(pOutbuf+OFF_S_STREET, pOutbuf+OFF_M_STREET, 7) &&
       !memcmp(pOutbuf+OFF_S_STRNUM, pOutbuf+OFF_M_STRNUM, SIZ_M_STRNUM) &&
       memcmp(apItems[NEV_S_CITY], "TR", 2) )
   {
      memcpy(acCity, pOutbuf+OFF_M_CITY, SIZ_M_CITY);
      blankRem(acCity, SIZ_M_CITY);
      City2Code(acCity, acCode, pOutbuf);
      if (acCode[0] > ' ')
      {
         memcpy(pOutbuf+OFF_S_CITY, acCode, strlen(acCode));
         memcpy(pOutbuf+OFF_S_ST, "CA", 2);
         memcpy(pOutbuf+OFF_S_ZIP, pOutbuf+OFF_M_ZIP, SIZ_S_ZIP);
         iTmp = sprintf(acTmp, "%s CA", acCity);
         memcpy(pOutbuf+OFF_S_CTY_ST_D, acTmp, iTmp);
      }
   } else if (*apItems[NEV_S_CITY] > ' ')
   {
      strcpy(acTmp, apItems[NEV_S_CITY]);
      Abbr2Code(acTmp, acCode, acCity);
      if (acCode[0] > ' ')
      {
         memcpy(pOutbuf+OFF_S_CITY, acCode, strlen(acCode));
         memcpy(pOutbuf+OFF_S_ST, "CA", 2);

         // hardcode known zipcode
         if (!memcmp(acTmp, "TR", 2))
            memcpy(pOutbuf+OFF_S_ZIP, "96161", 5);

         myTrim(acCity);
         iTmp = sprintf(acTmp, "%s CA", acCity);
         memcpy(pOutbuf+OFF_S_CTY_ST_D, acTmp, iTmp);
      }
   }

   lSitusMatch++;

   // Get next record
   pRec = fgets(acRec, 512, fdSitus);

   return 0;
}

/******************************** Nev_MergeOwner *****************************
 *
 * Return 0 if successful, >0 if error
 *
 *****************************************************************************/

void Nev_MergeOwner(char *pOutbuf, char *pRollRec)
{
   int   iTmp, iTmp1;
   char  acTmp[128], acName1[64];
   char  *pTmp, *pTmp1;

   OWNER    myOwner;
   NEV_ROLL *pRec;

   pRec = (NEV_ROLL *)pRollRec;

   // Clear output buffer if needed
   removeNames(pOutbuf, false);

   memcpy(acName1, pRec->Name, RSIZ_NEV_NAME);
   blankRem(acName1, RSIZ_NEV_NAME);

   // Update vesting
   iTmp = updateVesting(myCounty.acCntyCode, acName1, pOutbuf+OFF_VEST, pOutbuf+OFF_ETAL_FLG);

   // Remove multiple spaces
   pTmp = acName1;
   iTmp = iTmp1 = 0;
   while (*pTmp)
   {
      // Mark name with numeric value
      if (isdigit(*pTmp))
         iTmp1++;

      acTmp[iTmp++] = *pTmp;
      // Remove multi-space
      if (*pTmp == ' ')
         while (*pTmp == ' ') pTmp++;
      else
         pTmp++;

      // Remove '.' too
      if (*pTmp == '.')
         pTmp++;
   }
   // Keep a space at the end so we can check for " TR "
   //if (acTmp[iTmp-1] == ' ')
   //   iTmp -= 1;
   acTmp[iTmp] = 0;

   if (pTmp = strchr(acTmp, '/'))
   {
      *pTmp++ = 0;
      if (!strstr(pTmp, " ETAL"))
      {
         sprintf(acName1, "%s & %s", acTmp, pTmp);
         strcpy(acTmp, acName1);
      }
   }

   // Save name1 - standardize AND with '&'
   if (pTmp = strstr(acTmp, " AND "))
   {
      *pTmp = 0;
      strcpy(acName1, acTmp);
      strcat(acName1, " &");
      strcat(acName1, pTmp + 4);
   } else
      strcpy(acName1, acTmp);

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "01321170", 8) )
   //   iTmp = 0;
#endif

   // Drop everything from these words
   if ((pTmp=strstr(acTmp, " ETAL")) || (pTmp=strstr(acTmp, " ET AL")) || (pTmp=strstr(acTmp, " DECEDENTS TR ET")))
      *pTmp = 0;

   if ((pTmp=strstr(acTmp, " TRETAL")) || (pTmp=strstr(acTmp, " TR ET")))
      *(pTmp+3) = 0;

   if ((pTmp=strstr(acTmp, " CO-TR"))  || (pTmp=strstr(acTmp, " LIFE EST")) ||
       (pTmp=strstr(acTmp, " EST OF")) || (pTmp=strstr(acTmp, " ESTATE OF")) )
      *pTmp = 0;

   if (pTmp1=strstr(acTmp, "TRUST"))
   {
      if ((pTmp=strstr(acTmp, " FAM"))   || (pTmp=strstr(acTmp, " REVOC")) ||
          (pTmp=strstr(acTmp, " INDIV")) || (pTmp=strstr(acTmp, " AMENDED ")) ||
          (pTmp=strstr(acTmp, " LIV")) )
         *pTmp = 0;
      else
         *pTmp1 = 0;
   }

   if ((pTmp=strstr(acTmp, " FAM RV TR"))  || (pTmp=strstr(acTmp, " REV TR")) ||
       (pTmp=strstr(acTmp, " RV TR"))      || (pTmp=strstr(acTmp, " FAM LIV TR")) ||
       (pTmp=strstr(acTmp, " FAM TR"))     || (pTmp=strstr(acTmp, " LIV TR")) ||
       (pTmp=strstr(acTmp, " RD LAND TR")) || (pTmp=strstr(acTmp, " MARITAL TR")) ||
       (pTmp=strstr(acTmp, " LAND TR"))    || (pTmp=strstr(acTmp, " EQUITY TR"))  ||
       (pTmp=strstr(acTmp, " TRSTE")) )
      *pTmp = 0;

   // Leave name as is if number present
   pTmp = acTmp;
   iTmp1 = 0;
   while (*pTmp)
   {
      if (isdigit(*pTmp))
      {
         iTmp1 = 1;
         break;
      }
      pTmp++;
   }
   if (iTmp1 > 0)
   {
      vmemcpy(pOutbuf+OFF_NAME1, acName1, SIZ_NAME1);
      vmemcpy(pOutbuf+OFF_NAME_SWAP, acName1, SIZ_NAME_SWAP);
      return;
   }

   // Now parse owners
   acTmp[63] = 0;
   iTmp = splitOwner(acTmp, &myOwner, 5);
   if (strstr(acTmp," PROJECT") || strstr(acTmp," UNPATENTED"))
      vmemcpy(pOutbuf+OFF_NAME_SWAP, acName1, SIZ_NAME_SWAP);
   else
   {
      if (iTmp >= 0)
         vmemcpy(pOutbuf+OFF_NAME_SWAP, myOwner.acSwapName, SIZ_NAME_SWAP);
      else
         vmemcpy(pOutbuf+OFF_NAME_SWAP, acName1, SIZ_NAME_SWAP);
   }

   vmemcpy(pOutbuf+OFF_NAME1, acName1, SIZ_NAME1);
}

/********************************* Nev_MergeSAdr *****************************
 *
 * Return 0 if successful, >0 if error
 *
 *****************************************************************************/

void Nev_MergeSAdr(char *pOutbuf, char *pRollRec)
{
   char  *pTmp, acAdr[64], acStrSub[8], acUnit[8], acTmp[128];
   char  acAddr1[128], acCode[64], acCity[64];
   int   iStrNo, iTmp;

   NEV_ROLL *pRec;
   ADR_REC  sSitusAdr;

   pRec = (NEV_ROLL *)pRollRec;

   // Reset old address
   removeSitus(pOutbuf);

   // Ignore N/A
   if (!memcmp(pRec->Situs_StrName, "N/A", 3) || !memcmp(pRec->Situs_StrName, "   ", 3))
      return;

   acStrSub[0]=acUnit[0] = 0;

   memcpy(pOutbuf+OFF_S_HSENO, pRec->Situs_StrNum, RSIZ_NEV_SITUS_STRNUM);
   memcpy(acTmp, pRec->Situs_StrNum, RSIZ_NEV_SITUS_STRNUM);

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "0541065", 7) )
   //   iTmp = 0;
#endif

   //acTmp[RSIZ_NEV_SITUS_STRNUM] = 0;
   blankRem(acTmp, RSIZ_NEV_SITUS_STRNUM);

   /* 10/25/2013
   if (acTmp[4] == '/')
   {
      strcpy(acStrSub, &acTmp[3]);
      acTmp[3] = 0;
   } else if (acTmp[4] == '&')
   {
      strcpy(acUnit, &acTmp[3]);
      acTmp[3] = 0;

   } else if (acTmp[4] == '-' || acTmp[4] == ' ')
   {
      strcpy(acStrSub, &acTmp[5]);
      acTmp[4] = 0;
   } else if (acTmp[4] == '#')
   {
      strcpy(acUnit, &acTmp[4]);
      acTmp[4] = 0;
   //} else if (pTmp = strpbrk(acTmp, "&/-#"))
   //{
   //   iTmp = 0;
   }
   */

   iStrNo = atoi(acTmp);
   if (!iStrNo)
      return;

   // Street name
   memcpy(acAdr, pRec->Situs_StrName, RSIZ_NEV_SITUS_STRNAME);
   acAdr[RSIZ_NEV_SITUS_STRNAME] = 0;

   // Remove single quotes
   remChar(acAdr, 39);

   if (pTmp=strchr(acAdr, '('))
      *pTmp = 0;

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "188100100", 9) )
   //   iTmp = 0;
#endif
   // Check for embeded unit#
   if (pTmp=strchr(acAdr, '*'))
   {
      if (!_memicmp(pTmp, "* Unit ", 7))
         strcpy(acUnit, pTmp+7);
      else if (!_memicmp(pTmp, "* Suite ", 8))
         strcpy(acUnit, pTmp+8);
      else if (!_memicmp(pTmp, "* Ste ", 6))     // PROVIDENCE MINE ROAD * STE 356A-H
         strcpy(acUnit, pTmp+6);
      else if (!_memicmp(pTmp, "* Apt ", 6))     // 
         strcpy(acUnit, pTmp+6);
      else if (!_memicmp(pTmp, "* Bldg ", 7) || !_memicmp(pTmp, "* Building", 7))
         iTmp = 0;   // Ignore this
      else if (!memcmp(pTmp, "* # ", 4))
      {
         strcpy(&acUnit[1], pTmp+4);
         acUnit[0] = '#';
      } 
      else if (!memcmp(pTmp, "* #", 3))
      {  // * #3 BUILD - 1881001000
         strcpy(acUnit, pTmp+2);

         char *pTmp1;
         if (pTmp1 = strchr(acUnit, ' '))
            *pTmp1 = 0;
      }
      else if (*(pTmp+3) == '/' && isdigit(*(pTmp+4)))
      {
         // Check 085105300
         strcpy(acStrSub, pTmp+2);
      } 
      else if (*(pTmp+3) == ' ' && isalpha(*(pTmp+2)))
      {
         acUnit[0] = *(pTmp+2);
         acUnit[1] = 0;
      } else
         iTmp = 0;

      *pTmp = 0;
   }

   parseAdr1N_1(&sSitusAdr, acAdr);
   if (iStrNo > 0)
   {
      iTmp = sprintf(acAddr1, "%d", iStrNo);
      memcpy(pOutbuf+OFF_S_STRNUM, acAddr1, iTmp);
      if (acStrSub[0] > ' ')
         strcpy(sSitusAdr.strSub, acStrSub);
      if (acUnit[0] > ' ')
         strcpy(sSitusAdr.Unit, acUnit);
   }

   if (sSitusAdr.strSub[0] > ' ')
   {
      strcat(acAddr1, " ");
      strcat(acAddr1, sSitusAdr.strSub);
      memcpy(pOutbuf+OFF_S_STR_SUB, sSitusAdr.strSub, strlen(sSitusAdr.strSub));
   }

   if (sSitusAdr.strDir[0] > ' ')
      memcpy(pOutbuf+OFF_S_DIR, sSitusAdr.strDir, strlen(sSitusAdr.strDir));

   if (sSitusAdr.strName[0] > ' ')
      memcpy(pOutbuf+OFF_S_STREET, sSitusAdr.strName, strlen(sSitusAdr.strName));

   if (sSitusAdr.SfxCode[0] > ' ')
      memcpy(pOutbuf+OFF_S_SUFF, sSitusAdr.SfxCode, strlen(sSitusAdr.SfxCode));

   if (sSitusAdr.Unit[0] > ' ')
      memcpy(pOutbuf+OFF_S_UNITNO, sSitusAdr.Unit, strlen(sSitusAdr.Unit));

   if (iStrNo > 0)
   {
      if (sSitusAdr.Unit[0] > ' ')
         sprintf(acAddr1, "%s %s %s %s %s", acAddr1, sSitusAdr.strDir, sSitusAdr.strName, sSitusAdr.strSfx, sSitusAdr.Unit);
      else
         sprintf(acAddr1, "%s %s %s %s", acAddr1, sSitusAdr.strDir, sSitusAdr.strName, sSitusAdr.strSfx);

      blankRem(acAddr1);
      memcpy(pOutbuf+OFF_S_ADDR_D, acAddr1, strlen(acAddr1));
   }

   // If situs and mailing are the same, use mail city and zip
   if (!memcmp(pOutbuf+OFF_S_STREET, pOutbuf+OFF_M_STREET, 7) &&
       !memcmp(pOutbuf+OFF_S_STRNUM, pOutbuf+OFF_M_STRNUM, SIZ_M_STRNUM) &&
       memcmp(pRec->Situs_City_Code, "TR", 2) )
   {
      memcpy(acCity, pOutbuf+OFF_M_CITY, SIZ_M_CITY);
      blankRem(acCity, SIZ_M_CITY);
      City2Code(acCity, acCode, pOutbuf);
      if (acCode[0] > ' ')
      {
         memcpy(pOutbuf+OFF_S_CITY, acCode, strlen(acCode));
         memcpy(pOutbuf+OFF_S_ST, "CA", 2);
         memcpy(pOutbuf+OFF_S_ZIP, pOutbuf+OFF_M_ZIP, SIZ_S_ZIP);
         iTmp = sprintf(acTmp, "%s CA", acCity);
         memcpy(pOutbuf+OFF_S_CTY_ST_D, acTmp, iTmp);
      }
   } else if (pRec->Situs_City_Code[0] > ' ')
   {
      memcpy(acTmp, pRec->Situs_City_Code, RSIZ_NEV_SITUS_CITY_CODE);
      acTmp[RSIZ_NEV_SITUS_CITY_CODE] = 0;
      Abbr2Code(acTmp, acCode, acCity);
      if (acCode[0] > ' ')
      {
         memcpy(pOutbuf+OFF_S_CITY, acCode, strlen(acCode));
         memcpy(pOutbuf+OFF_S_ST, "CA", 2);

         // hardcode known zipcode
         if (!memcmp(acTmp, "TR", 2))
            memcpy(pOutbuf+OFF_S_ZIP, "96161", 5);

         myTrim(acCity);
         iTmp = sprintf(acTmp, "%s CA", acCity);
         memcpy(pOutbuf+OFF_S_CTY_ST_D, acTmp, iTmp);
      }
   }

}

/********************************** Nev_MergeMAdr ****************************
 *
 * Return 0 if successful, >0 if error
 *
 *****************************************************************************/

void Nev_MergeMAdr(char *pOutbuf, char *pRollRec)
{
   NEV_ROLL  *pRec;

   char        *pAddr1, acTmp[256], acAddr1[64];
   int         iTmp;
   bool        bNoMail = false;

   ADR_REC  sMailAdr;

   pRec = (NEV_ROLL *)pRollRec;

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "0539627000", 10) )
   //   iTmp = 0;
#endif

   // Clear old Mailing
   removeMailing(pOutbuf, false);

   // Check for blank address
   if (memcmp(pRec->Mail_Addr, "     ", 5))
   {
      memcpy(acAddr1, pRec->Mail_Addr, RSIZ_NEV_MAIL_ADDR);
      pAddr1 = myTrim(acAddr1, RSIZ_NEV_MAIL_ADDR);

      // Parsing mail address
      memset((void *)&sMailAdr, 0, sizeof(ADR_REC));

      // Ignore PARK or PARKING
      if (memcmp(pRec->Mail_Addr, "PARK", 4) &&
          memcmp(pRec->Mail_Addr, "UNKNOWN", 7))
      {
         // 12077 ST HWY49
         // 150 S AUBURN ST #B-1
         // 225-P E SAN NICOLAS LN
         // 518-A SILVA AVE
         parseMAdr1(&sMailAdr, acAddr1);

         if (sMailAdr.lStrNum > 0)
         {
            iTmp = sprintf(acTmp, "%d", sMailAdr.lStrNum);
            memcpy(pOutbuf+OFF_M_STRNUM, acTmp, iTmp);
            if (sMailAdr.strSub[0] > '0')
            {
               sprintf(acTmp, "%s  ", sMailAdr.strSub);
               memcpy(pOutbuf+OFF_M_STR_SUB, acTmp, SIZ_M_STR_SUB);
            }
         }
         vmemcpy(pOutbuf+OFF_M_DIR, sMailAdr.strDir, SIZ_M_DIR);
         vmemcpy(pOutbuf+OFF_M_STREET, sMailAdr.strName, SIZ_M_STREET);
         vmemcpy(pOutbuf+OFF_M_SUFF, sMailAdr.strSfx, SIZ_M_SUFF);
         if (sMailAdr.Unit[0] > ' ')
         {
            if (sMailAdr.Unit[0] == '#')
               memcpy(pOutbuf+OFF_M_UNITNO, sMailAdr.Unit, strlen(sMailAdr.Unit));
            else
            {
               *(pOutbuf+OFF_M_UNITNO) = '#';
               memcpy(pOutbuf+OFF_M_UNITNO+1, sMailAdr.Unit, strlen(sMailAdr.Unit));
            }
         }
         memcpy(pOutbuf+OFF_M_ADDR_D, acAddr1, strlen(acAddr1));
      }

      // Parse city-st
      memcpy(acTmp, pRec->Mail_City_State, RSIZ_NEV_MAIL_CITY_STATE);
      blankRem(acTmp, RSIZ_NEV_MAIL_CITY_STATE);
      parseMAdr2(&sMailAdr, acTmp);

      if (sMailAdr.City[0] > ' ')
      {
         vmemcpy(pOutbuf+OFF_M_CITY, sMailAdr.City, SIZ_M_CITY);
         memcpy(pOutbuf+OFF_M_ST, sMailAdr.State, strlen(sMailAdr.State));

         iTmp = sprintf(acTmp, "%s %s", sMailAdr.City, sMailAdr.State);
         memcpy(pOutbuf+OFF_M_CTY_ST_D, acTmp, iTmp);
      }

      iTmp = atoin(pRec->Mail_Zip, RSIZ_NEV_MAIL_ZIP);
      if (iTmp > 500)
      {
         memcpy(pOutbuf+OFF_M_ZIP, pRec->Mail_Zip, SIZ_M_ZIP);
         iTmp = atoin(pRec->Mail_Zip4, RSIZ_NEV_MAIL_ZIP4);
         if (iTmp > 0)
            memcpy(pOutbuf+OFF_M_ZIP4, pRec->Mail_Zip4, SIZ_M_ZIP4);
      }
   }
}

/****************************** Nev_CreateLienRec ****************************
 *
 * Return 0 if successful, >0 if error
 *
 *****************************************************************************/

int Nev_CreateLienRec(char *pOutbuf, char *pRollRec)
{
   char     acTmp[256];
   long     lTmp, iTmp;
   LIENEXTR *pLienRec = (LIENEXTR *)pOutbuf;
   NEV_ROLL *pRec = (NEV_ROLL *)pRollRec;

   // Clear output buffer
   memset(pOutbuf, ' ', sizeof(LIENEXTR));

   // Start copying data
   memcpy(pLienRec->acApn, pRec->Apn, RSIZ_NEV_APN);
   memcpy(pLienRec->acPrevApn, pRec->Apn, RSIZ_NEV_APN);

   // TRA
   lTmp = atoin(pRec->Tra, RSIZ_NEV_TRA);
   if (lTmp > 0)
   {
      iTmp = sprintf(acTmp, "%.6d", lTmp);
      memcpy(pLienRec->acTRA, acTmp, iTmp);
   }

   // Year assessed
   memcpy(pLienRec->acYear, myCounty.acYearAssd, 4);

   // Land
   long lLand = atoin(pRec->Land,RSIZ_NEV_LAND);
   if (lLand > 0)
   {
      iTmp = sprintf(acTmp, "%*d", sizeof(pLienRec->acLand), lLand);
      memcpy(pLienRec->acLand, acTmp, iTmp);
   }

   // Improve
   long lImpr = atoin(pRec->Improv, RSIZ_NEV_IMPROV);
   if (lImpr > 0)
   {
      iTmp = sprintf(acTmp, "%*d", sizeof(pLienRec->acImpr), lImpr);
      memcpy(pLienRec->acImpr, acTmp, iTmp);
   }

   // PP value
   long lPers = atoin(pRec->Business_Pers_Prop, RSIZ_NEV_BUSINESS_PERS_PROP);
   // Fixture or ME val
   long lFixture = atoin(pRec->Trade_Fixt, RSIZ_NEV_TRADE_FIXT);

   long lOther = lFixture + lPers;
   if (lOther > 0)
   {
      iTmp = sprintf(acTmp, "%*d", sizeof(pLienRec->acOther), lOther);
      memcpy(pLienRec->acOther, acTmp, iTmp);

      if (lPers > 0)
      {
         iTmp = sprintf(acTmp, "%*d", sizeof(pLienRec->acPP_Val), lPers);
         memcpy(pLienRec->acPP_Val, acTmp, iTmp);
      }
      if (lFixture > 0)
      {
         iTmp = sprintf(acTmp, "%*d", sizeof(pLienRec->acME_Val), lFixture);
         memcpy(pLienRec->acME_Val, acTmp, iTmp);
      }
   }

   // Gross total
   lTmp = lLand+lImpr+lOther;
   if (lTmp > 0)
   {
      iTmp = sprintf(acTmp, "%*d", sizeof(pLienRec->acGross), lTmp);
      memcpy(pLienRec->acGross, acTmp, iTmp);
  }

   // Ratio
   if (lImpr > 0)
   {
      iTmp = sprintf(acTmp, "%*d", sizeof(pLienRec->acRatio), (LONGLONG)lImpr*100/(lLand+lImpr));
      memcpy(pLienRec->acRatio, acTmp, iTmp);
   }

   // HO Exempt
   if (pRec->Exemption_Type[0] == 'H')
      pLienRec->acHO[0] = '1';      // 'Y'
   else
      pLienRec->acHO[0] = '2';
   lTmp = atoin(pRec->Exemption_Value, RSIZ_NEV_EXEMPTION_VALUE);
   if (lTmp > 0)
   {
      iTmp = sprintf(acTmp, "%*d", sizeof(pLienRec->acExAmt), lTmp);
      memcpy(pLienRec->acExAmt, acTmp, iTmp);
   }

   // Save lot acre
   long lSqFtLand=0;
   double dAcreAge = atofn(pRec->Acres, RSIZ_NEV_ACRES);
   if (dAcreAge > 0.0)
   {
      sprintf(acTmp, "%*d", SIZ_LOT_ACRES, (long)(dAcreAge*1000));
      memcpy(pLienRec->extra.Nev.LotAcre, acTmp, SIZ_LOT_ACRES);
      if (lSqFtLand == 0)
      {
         lSqFtLand = (long)((dAcreAge * SQFT_PER_ACRE));
         sprintf(acTmp, "%*d", SIZ_LOT_SQFT, lSqFtLand);
         memcpy(pLienRec->extra.Nev.LotSqft, acTmp, SIZ_LOT_SQFT);
      }
   }

   // Set full exempt flag
   if (pRec->Status[0] == 'N')
      pLienRec->SpclFlag = LX_FULLEXE_FLG;

   // Prop8 - wait for logic
   //if (pRec->LandKey == '8')
   //   pLienRec->SpclFlag |= LX_PROP8_FLG;

   pLienRec->LF[0] = '\n';
   pLienRec->LF[1] = 0;
   return 0;
}

/******************************** Nev_ExtrLien ******************************
 *
 * Extract lien data 
 *
 ****************************************************************************/

int Nev_ExtrLien(LPCSTR pCnty, LPCSTR pLDRFile)
{
   char  *pTmp, acBuf[1024], acRec[MAX_RECSIZE];
   char  acOutFile[_MAX_PATH], acTmpFile[_MAX_PATH];
   int   iRet, iNewRec=0, lCnt=0;
   FILE  *fdLienExt;

   LogMsg("\nExtract lien roll for %s", pCnty);

   // Open lien file
   if (!pLDRFile)
      GetIniString(pCnty, "LienFile", "", acTmpFile, _MAX_PATH, acIniFile);
   else
      strcpy(acTmpFile, pLDRFile);

   LogMsg("Open Lien Date Roll file %s", acTmpFile);
   fdRoll = fopen(acTmpFile, "r");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening sorted roll file: %s\n", acTmpFile);
      return 2;
   }

   // Create lien extract
   sprintf(acOutFile, acLienTmpl, pCnty, pCnty);
   LogMsg("Open lien extract file %s", acOutFile);
   fdLienExt = fopen(acOutFile, "w");
   if (fdLienExt == NULL)
   {
      LogMsg("***** Error creating lien extract file: %s\n", acOutFile);
      return -2;
   }

   // Get 1st rec
   pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdRoll);

   // Merge loop
   while (pTmp && !feof(fdRoll))
   {
      if (acRec[ROFF_NEV_STATUS] != 'V')
      {
         // Create new record
         iRet = Nev_CreateLienRec(acBuf, acRec);

         // Write to output
         fputs(acBuf, fdLienExt);

         if (!(++lCnt % 1000))
            printf("\r%u", lCnt);
      }

      // Get next roll record
      pTmp = fgets(acRec, MAX_RECSIZE, fdRoll);
   }

   // Close files
   if (fdRoll)
      fclose(fdRoll);
   if (fdLienExt)
      fclose(fdLienExt);

   LogMsgD("Total output records:       %u", lCnt);

   lRecCnt = lCnt;
   return 0;
}

/********************************** Nev_MergeRoll *****************************
 *
 * Return 0 if successful, >0 if error
 *
 *****************************************************************************/

int Nev_MergeRoll(char *pOutbuf, char *pRollRec, int iFlag)
{
   char     acTmp[256], acTmp1[256], *pTmp;
   int      iRet;
   long     lTmp;

   NEV_ROLL *pRec;

   pRec = (NEV_ROLL *)pRollRec;
   if (iFlag & CREATE_R01)
   {
      // Clear output buffer
      memset(pOutbuf, ' ', iRecLen);

      // Start copying data
      memcpy(pOutbuf, pRec->Apn, RSIZ_NEV_APN);
      memcpy(pOutbuf+OFF_PREV_APN, pRec->Apn, RSIZ_NEV_APN);

      // Format APN
      iRet = formatApn(pRec->Apn, acTmp, &myCounty);
      memcpy(pOutbuf+OFF_APN_D, acTmp, iRet);

      // Create MapLink and output new record
      iRet = formatMapLink(pRec->Apn, acTmp, &myCounty);
      memcpy(pOutbuf+OFF_MAPLINK, acTmp, iRet);

      // Create index map link
      if (getIndexPage(acTmp, acTmp1, &myCounty))
         memcpy(pOutbuf+OFF_IMAPLINK, acTmp1, iRet);

      memcpy(pOutbuf+OFF_CO_NUM, "29NEV", 5);

      // Year assessed
      memcpy(pOutbuf+OFF_YR_ASSD, myCounty.acYearAssd, 4);


      long lSqFtLand=0;
      double dAcreAge = atofn(pRec->Acres, RSIZ_NEV_ACRES);
      if (dAcreAge > 0.0)
      {
         sprintf(acTmp, "%*d", SIZ_LOT_ACRES, (long)(dAcreAge*1000));
         memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);
         if (lSqFtLand == 0)
         {
            lSqFtLand = (long)((dAcreAge * SQFT_PER_ACRE));
            sprintf(acTmp, "%*d", SIZ_LOT_SQFT, lSqFtLand);
            memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);
         }
      }

      // Land
      long lLand = atoin(pRec->Land,RSIZ_NEV_LAND);
      if (lLand > 0)
      {
         sprintf(acTmp, "%*d", SIZ_LAND, lLand);
         memcpy(pOutbuf+OFF_LAND, acTmp, SIZ_LAND);
      }

      // Improve
      long lImpr = atoin(pRec->Improv, RSIZ_NEV_IMPROV);
      if (lImpr > 0)
      {
         sprintf(acTmp, "%*d", SIZ_IMPR, lImpr);
         memcpy(pOutbuf+OFF_IMPR, acTmp, SIZ_IMPR);
      }

      // PP value
      long lPers = atoin(pRec->Business_Pers_Prop, RSIZ_NEV_BUSINESS_PERS_PROP);
      // Fixture or ME val
      long lFixture = atoin(pRec->Trade_Fixt, RSIZ_NEV_TRADE_FIXT);
      long lOther = lFixture+lPers;
      if (lOther > 0)
      {
         sprintf(acTmp, "%*d", SIZ_OTHER, lOther);
         memcpy(pOutbuf+OFF_OTHER, acTmp, SIZ_OTHER);

         if (lFixture > 0)
         {
            sprintf(acTmp, "%d         ", lFixture);
            memcpy(pOutbuf+OFF_FIXTR, acTmp, SIZ_FIXTR);
         }
         if (lPers > 0)
         {
            sprintf(acTmp, "%d         ", lPers);
            memcpy(pOutbuf+OFF_PERSPROP, acTmp, SIZ_PERSPROP);
         }
      }

      // Gross total
      lTmp = lLand+lImpr+lOther;
      if (lTmp > 0)
      {
         sprintf(acTmp, "%*d", SIZ_GROSS, lTmp);
         memcpy(pOutbuf+OFF_GROSS, acTmp, SIZ_GROSS);
      }

      // Ratio
      if (lImpr > 0)
      {
         sprintf(acTmp, "%*d", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
         memcpy(pOutbuf+OFF_RATIO, acTmp, SIZ_RATIO);
      }

      // HO Exempt
      if (pRec->Exemption_Type[0] == 'H')
         *(pOutbuf+OFF_HO_FL) = '1';      // 'Y'
      else
         *(pOutbuf+OFF_HO_FL) = '2';
      lTmp = atoin(pRec->Exemption_Value, RSIZ_NEV_EXEMPTION_VALUE);
      if (lTmp > 0)
      {
         sprintf(acTmp, "%*d", SIZ_EXE_TOTAL, lTmp);
         memcpy(pOutbuf+OFF_EXE_TOTAL, acTmp, SIZ_EXE_TOTAL);
      }

   }

   // TRA
   lTmp = atoin(pRec->Tra, RSIZ_NEV_TRA);
   if (lTmp > 0)
   {
      iRet = sprintf(acTmp, "%.6d", lTmp);
      memcpy(pOutbuf+OFF_TRA, acTmp, iRet);
   }

   // Status
   if (pRec->Status[0] == 'N')
   {
      *(pOutbuf+OFF_STATUS) = 'O';
      *(pOutbuf+OFF_FULL_EXEMPT) = 'Y';
   } else
   {
      *(pOutbuf+OFF_STATUS) = 'A';
      *(pOutbuf+OFF_FULL_EXEMPT) = ' ';
   }

   // Use code
   memcpy(pOutbuf+OFF_USE_CO, pRec->Use_Code, RSIZ_NEV_USE_CODE);

   // Standard usecode
   if (pRec->Use_Code[0] > ' ')
   {
      memcpy(acTmp, pRec->Use_Code, RSIZ_NEV_USE_CODE);
      acTmp[RSIZ_NEV_USE_CODE] = 0;

      if (iNumUseCodes > 0)
      {
         pTmp = Cnty2StdUse(_strupr(acTmp));
         if (pTmp)
            memcpy(pOutbuf+OFF_USE_STD, pTmp, 3);
         else
         {
            memcpy(pOutbuf+OFF_USE_STD, USE_MISC, 3);
            logUsecode(acTmp, pOutbuf);
         }
      } else
      {
         pTmp = Nev_XlatUseCode(acTmp);
         if (pTmp)
            memcpy(pOutbuf+OFF_USE_STD, pTmp, 3);
      }
   } else
      memcpy(pOutbuf+OFF_USE_STD, "999", 3);

   // Legal
   if (pRec->Property_Lead_Line[0] > ' ')
   {
      iRet = replChar(pRec->Property_Lead_Line, 20, '/', RSIZ_NEV_PROPERTY_LEAD_LINE);
      if (iRet)
         LogMsg("*** Bad char found in Property Lead Line at pos [%d] on APN=%.12s", iRet+1, pOutbuf);
      memcpy(pOutbuf+OFF_LEGAL, pRec->Property_Lead_Line, RSIZ_NEV_PROPERTY_LEAD_LINE);
   }

   // CareOf
   updateCareOf(pOutbuf, pRec->Careof, RSIZ_NEV_CAREOF);

   // Transfer
   if (pRec->DocDate1[0] > ' ')
   {
      memcpy(acTmp1, pRec->DocDate1, RSIZ_NEV_DOCDATE1);
      acTmp1[RSIZ_NEV_DOCDATE1] = 0;
      pTmp = dateConversion(acTmp1, acTmp, MMDDYY1);
      if (pTmp)
      {
         memcpy(pOutbuf+OFF_TRANSFER_DT, acTmp, SIZ_TRANSFER_DT);
         //memcpy(pOutbuf+OFF_SALE1_DT, acTmp, SIZ_TRANSFER_DT);
         memcpy(acTmp, pRec->DocNum1, RSIZ_NEV_DOCNUM1);
         blankRem(acTmp, RSIZ_NEV_DOCNUM1);
         blankPad(acTmp, SIZ_TRANSFER_DOC);
         memcpy(pOutbuf+OFF_TRANSFER_DOC, acTmp, SIZ_TRANSFER_DOC);
         //memcpy(pOutbuf+OFF_SALE1_DOC, acTmp, SIZ_TRANSFER_DOC);
         /*
         dTmp = atofn(pRec->Stamp_Amt1, RSIZ_NEV_STAMP_AMT1);
         if (dTmp > 0.0)
         {
            lTmp = (long)(dTmp * SALE_FACTOR);
            sprintf(acTmp, "%*d", SIZ_SALE1_AMT, lTmp);
            memcpy(pOutbuf+OFF_SALE1_AMT, acTmp, SIZ_SALE1_AMT);
         } else
            memset(pOutbuf+OFF_SALE1_AMT, ' ', SIZ_SALE1_AMT);

         *(pOutbuf+OFF_AR_CODE1) = 'A';
         */
      }
   }

   /*
   if (pRec->DocDate2[0] > ' ')
   {
      memcpy(acTmp1, pRec->DocDate2, RSIZ_NEV_DOCDATE2);
      acTmp1[RSIZ_NEV_DOCDATE2] = 0;
      pTmp = dateConversion(acTmp1, acTmp, MMDDYY1);
      if (pTmp)
      {
         memcpy(pOutbuf+OFF_SALE2_DT, acTmp, SIZ_TRANSFER_DT);
         memcpy(acTmp, pRec->DocNum2, RSIZ_NEV_DOCNUM2);
         blankRem(acTmp, RSIZ_NEV_DOCNUM2);
         blankPad(acTmp, SIZ_TRANSFER_DOC);
         memcpy(pOutbuf+OFF_SALE2_DOC, acTmp, SIZ_TRANSFER_DOC);

         dTmp = atofn(pRec->Stamp_Amt2, RSIZ_NEV_STAMP_AMT1);
         if (dTmp > 0.0)
         {
            lTmp = (long)(dTmp * SALE_FACTOR);
            sprintf(acTmp, "%*d", SIZ_SALE1_AMT, lTmp);
            memcpy(pOutbuf+OFF_SALE2_AMT, acTmp, SIZ_SALE1_AMT);
         } else
            memset(pOutbuf+OFF_SALE2_AMT, ' ', SIZ_SALE1_AMT);

         *(pOutbuf+OFF_AR_CODE2) = 'A';
      }
   }

   if (pRec->DocDate3[0] > ' ')
   {
      memcpy(acTmp1, pRec->DocDate3, RSIZ_NEV_DOCDATE3);
      acTmp1[RSIZ_NEV_DOCDATE3] = 0;
      pTmp = dateConversion(acTmp1, acTmp, MMDDYY1);
      if (pTmp)
      {
         memcpy(pOutbuf+OFF_SALE3_DT, acTmp, SIZ_TRANSFER_DT);
         memcpy(acTmp, pRec->DocNum3, RSIZ_NEV_DOCNUM3);
         blankRem(acTmp, RSIZ_NEV_DOCNUM3);
         blankPad(acTmp, SIZ_TRANSFER_DOC);
         memcpy(pOutbuf+OFF_SALE3_DOC, acTmp, SIZ_TRANSFER_DOC);

         dTmp = atofn(pRec->Stamp_Amt3, RSIZ_NEV_STAMP_AMT1);
         if (dTmp > 0.0)
         {
            lTmp = (long)(dTmp * SALE_FACTOR);
            sprintf(acTmp, "%*d", SIZ_SALE1_AMT, lTmp);
            memcpy(pOutbuf+OFF_SALE3_AMT, acTmp, SIZ_SALE1_AMT);
         } else
            memset(pOutbuf+OFF_SALE3_AMT, ' ', SIZ_SALE1_AMT);

         *(pOutbuf+OFF_AR_CODE3) = 'A';
      }
   }
   */

   Nev_MergeOwner(pOutbuf, pRollRec);
   Nev_MergeMAdr(pOutbuf, pRollRec);
   // 10/25/2013 Nev_MergeSAdr(pOutbuf, pRollRec);
   //            Use Nev_MergeSitus() instead

   return 0;
}

/******************************** Nev_MergeChar ******************************
 *
 * Not all CHAR is from CHAR file.  Other fields like Zoning, Quality, Total rooms, 
 * EffYr, Water, Condition may have to pull from NCALLS1.TXT file.
 * Bug fix 8/1/2017
 *
 *****************************************************************************/

int Nev_MergeChar(char *pOutbuf)
{
   static char    acRec[2048], *pRec=NULL;
   char           acTmp[256];
   long           lTmp, lBldgSqft, lGarSqft;
   int            iRet, iLoop, iBeds, iFBath, iHBath, iUnits;
   double         fTmp;
   STDCHAR       *pChar;

   iRet=iBeds=iUnits=0;
   lBldgSqft=lGarSqft=0;

   // Get first Char rec for first call
   if (!pRec)
      pRec = fgets(acRec, 1024, fdChar);

   do
   {
      // Compare Apn
      iLoop = memcmp(pOutbuf, acRec, iApnLen);
      if (iLoop > 0)
      {
         if (bDebug)
            LogMsg0("Skip Char rec  %.*s", iApnLen, pRec);

         pRec = fgets(acRec, 1024, fdChar);
         if (!pRec)
         {
            fclose(fdChar);
            fdChar = NULL;
            return 1;      // EOF
         }
         lCharSkip++;
      }
   } while (iLoop > 0);

   // If not match, return
   if (iLoop)
      return 1;

   pChar = (STDCHAR *)acRec;

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "4511009000", 10) )
   //   lTmp = 0;
#endif

   // Zoning
   memcpy(pOutbuf+OFF_ZONE, pChar->Zoning, SIZ_ZONE);

   // BldgSqft
   lBldgSqft = atoin(pChar->BldgSqft, SIZ_CHAR_SQFT);
   if (lBldgSqft > 0)
   {
      sprintf(acTmp, "%*d", SIZ_BLDG_SF, lBldgSqft);
      memcpy(pOutbuf+OFF_BLDG_SF, acTmp, SIZ_BLDG_SF);
   }

   // Yrblt
   lTmp = atoin(pChar->YrBlt, SIZ_CHAR_YRBLT);
   if (lTmp > 1700 && lTmp < lToyear)
      memcpy(pOutbuf+OFF_YR_BLT, pChar->YrBlt, SIZ_YR_BLT);
   else
      memset(pOutbuf+OFF_YR_BLT, ' ', SIZ_YR_BLT); // Clean up bad data

   // YrEff
   lTmp = atoin(pChar->YrEff, SIZ_CHAR_YRBLT);
   if (lTmp > 1700 && lTmp < lToyear)
      memcpy(pOutbuf+OFF_YR_EFF, pChar->YrEff, SIZ_YR_BLT);
   else
      memset(pOutbuf+OFF_YR_EFF, ' ', SIZ_YR_BLT); // Clean up bad data

   // Stories   
   fTmp = atofn(pChar->Stories, SIZ_CHAR_SIZE4);
   if (fTmp > 0.0)
   {
      sprintf(acTmp, "%.1f  ", fTmp);
      memcpy(pOutbuf+OFF_STORIES, acTmp, SIZ_STORIES);
   }
   
   // Beds
   iBeds = atoin(pChar->Beds, SIZ_CHAR_BEDS);
   if (iBeds > 0)
   {
      sprintf(acTmp, "%*d", SIZ_BEDS, iBeds);
      memcpy(pOutbuf+OFF_BEDS, acTmp, SIZ_BEDS);
   }

   // Bath
   iFBath = atoin(pChar->FBaths, SIZ_CHAR_BATHS);
   if (iFBath > 0)
   {
      sprintf(acTmp, "%*d", SIZ_BATH_F, iFBath);
      memcpy(pOutbuf+OFF_BATH_F, acTmp, SIZ_BATH_F);
   }

   iHBath = atoin(pChar->HBaths, SIZ_CHAR_BATHS);
   if (iHBath > 0)
   {
      sprintf(acTmp, "%*d", SIZ_BATH_H, iHBath);
      memcpy(pOutbuf+OFF_BATH_H, acTmp, SIZ_BATH_H);
   }

   if (pChar->Bath_1Q[0] > '0')
      *(pOutbuf+OFF_BATH_1Q) = pChar->Bath_1Q[0];
   if (pChar->Bath_2Q[0] > '0')
      *(pOutbuf+OFF_BATH_2Q) = pChar->Bath_2Q[0];
   if (pChar->Bath_3Q[0] > '0')
      *(pOutbuf+OFF_BATH_3Q) = pChar->Bath_3Q[0];
   if (pChar->Bath_4Q[0] > '0')
      *(pOutbuf+OFF_BATH_4Q) = pChar->Bath_4Q[0];

   // Unit
   iUnits = atoin(pChar->Units, SIZ_CHAR_UNITS);
   if (iUnits > 0)
   {
      sprintf(acTmp, "%*d", SIZ_UNITS, iUnits);
      memcpy(pOutbuf+OFF_UNITS, acTmp, SIZ_UNITS);
   }

   // Rooms
   iRet = atoin(pChar->Rooms, SIZ_CHAR_ROOMS);
   if (iRet > 0)
   {
      sprintf(acTmp, "%*d", SIZ_ROOMS, iRet);
      memcpy(pOutbuf+OFF_ROOMS, acTmp, SIZ_ROOMS);
   }

   // Gargage Size
   iRet = atoin(pChar->GarSqft, SIZ_CHAR_SQFT);
   if (iRet > 0)
   {
      sprintf(acTmp, "%*d", SIZ_GAR_SQFT, iRet);
      memcpy(pOutbuf+OFF_GAR_SQFT, acTmp, SIZ_GAR_SQFT);
   }

   // Garage
   *(pOutbuf+OFF_PARK_TYPE) = pChar->ParkType[0];

   // Air/Heating
   *(pOutbuf+OFF_AIR_COND) = pChar->Cooling[0];
   *(pOutbuf+OFF_HEAT) = pChar->Heating[0];    

   // Fireplace
   *(pOutbuf+OFF_FIRE_PL) = pChar->Fireplace[0];

   // Pool/Spa
   *(pOutbuf+OFF_POOL) = pChar->Pool[0];

   // View
   *(pOutbuf+OFF_VIEW) = pChar->View[0];

   // Quality class
   *(pOutbuf+OFF_BLDG_CLASS) = pChar->BldgClass;
   *(pOutbuf+OFF_BLDG_QUAL)  = pChar->BldgQual;

   // Impr condition
   *(pOutbuf+OFF_IMPR_COND)  = pChar->ImprCond[0];

   // Water/Sewer
   *(pOutbuf+OFF_SEWER) = pChar->HasSewer;
   *(pOutbuf+OFF_WATER) = pChar->HasWater;

   // Floor 1 & 2
   lTmp = atoin(pChar->Sqft_1stFl, SIZ_CHAR_SQFT);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_FLOOR_SF, lTmp);
      memcpy(pOutbuf+OFF_FLOOR1_SF, acTmp, SIZ_FLOOR_SF);
   }
   lTmp = atoin(pChar->Sqft_2ndFl, SIZ_CHAR_SQFT);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_FLOOR_SF, lTmp);
      memcpy(pOutbuf+OFF_FLOOR2_SF, acTmp, SIZ_FLOOR_SF);
   }

   // Basement
   lTmp = atoin(pChar->BsmtSqft, SIZ_CHAR_SQFT);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_BSMT_SF, lTmp);
      memcpy(pOutbuf+OFF_BSMT_SF, acTmp, SIZ_BSMT_SF);
   }

   // Porch
   lTmp = atoin(pChar->PorchSqft, SIZ_CHAR_SQFT);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_PORCH_SF, lTmp);
      memcpy(pOutbuf+OFF_PORCH_SF, acTmp, SIZ_PORCH_SF);
   }

   // Guest house
   lTmp = atoin(pChar->GuestSqft, SIZ_CHAR_SQFT);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_GUESTHSE_SF, lTmp);
      memcpy(pOutbuf+OFF_GUESTHSE_SF, acTmp, SIZ_GUESTHSE_SF);
   }

   // Misc Impr
   lTmp = atoin(pChar->MiscSqft, SIZ_CHAR_SQFT);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_MISCIMPR_SF, lTmp);
      memcpy(pOutbuf+OFF_MISCIMPR_SF, acTmp, SIZ_MISCIMPR_SF);
   }

   // Topo
   *(pOutbuf+OFF_TOPO) = pChar->Topo[0];

   lCharMatch++;

   // Get next Char rec
   pRec = fgets(acRec, 1024, fdChar);

   return 0;
}

/***************************** Nev_MergeSaleRec ******************************
 *
 * Merge new sale data into current sale.  Move other sales accordingly.
 * Only update seller if that is the sale from assessor.
 * This is the modified version of MergeSale() which will not update sale
 * without SaleAmt.  Only transfer is updated.
 *
 *****************************************************************************/

int Nev_MergeSaleRec(char *pOutbuf, char *pSaleRec, bool bSaleFlag)
{
   long  lCurSaleDt, lLstSaleDt, lPrice, lLastAmt;
   char  *pTmp, acTmp[32], acTmp2[32], acDate[32];

   NEV_SALE    *pSale;

   pSale = (NEV_SALE *)pSaleRec;

   memcpy(acTmp2, pSale->DocDate, SSIZ_NEV_DOCDATE);
   acTmp2[SSIZ_NEV_DOCDATE] = 0;

   pTmp = dateConversion(acTmp2, acDate, MM_DD_YYYY_1);

   lCurSaleDt = atoi(acDate);
   lLstSaleDt = atoin(pOutbuf+OFF_SALE1_DT, SIZ_SALE1_DT);

   if (lCurSaleDt < lLstSaleDt)
      return -1;

   if (lCurSaleDt == lLstSaleDt)
   {
      lLastAmt = atoin(pOutbuf+OFF_SALE1_AMT, SIZ_SALE1_AMT);
      lPrice = atoin(pSale->DocTax, SSIZ_NEV_STAMP_AMOUNT);
      if (!lPrice && lLastAmt > 0)
         return 0;
   } else
   {
      // Move sale2 to sale3
      memcpy(pOutbuf+OFF_SALE3_DOC, pOutbuf+OFF_SALE2_DOC, SIZ_SALE3_DOC);
      memcpy(pOutbuf+OFF_SALE3_DT, pOutbuf+OFF_SALE2_DT, SIZ_SALE3_DT);
      memcpy(pOutbuf+OFF_SALE3_DOCTYPE, pOutbuf+OFF_SALE2_DOCTYPE, SIZ_SALE3_DOCTYPE);
      memcpy(pOutbuf+OFF_SALE3_AMT, pOutbuf+OFF_SALE2_AMT, SIZ_SALE3_AMT);
      memcpy(pOutbuf+OFF_SALE3_CODE, pOutbuf+OFF_SALE2_CODE, SIZ_SALE3_CODE);
      *(pOutbuf+OFF_AR_CODE3) = *(pOutbuf+OFF_AR_CODE2);

      // Move sale1 to sale2
      memcpy(pOutbuf+OFF_SALE2_DOC, pOutbuf+OFF_SALE1_DOC, SIZ_SALE3_DOC);
      memcpy(pOutbuf+OFF_SALE2_DT, pOutbuf+OFF_SALE1_DT, SIZ_SALE3_DT);
      memcpy(pOutbuf+OFF_SALE2_DOCTYPE, pOutbuf+OFF_SALE1_DOCTYPE, SIZ_SALE3_DOCTYPE);
      memcpy(pOutbuf+OFF_SALE2_AMT, pOutbuf+OFF_SALE1_AMT, SIZ_SALE3_AMT);
      memcpy(pOutbuf+OFF_SALE2_CODE, pOutbuf+OFF_SALE1_CODE, SIZ_SALE3_CODE);
      *(pOutbuf+OFF_AR_CODE2) = *(pOutbuf+OFF_AR_CODE1);
   }

   // Update current sale
   memset(pOutbuf+OFF_SALE1_DOC, ' ', SIZ_SALE1_DOC);
   memset(pOutbuf+OFF_SALE1_DT, ' ', SIZ_SALE1_DT);

   memcpy(pOutbuf+OFF_SALE1_DOC, pSale->DocNum, SSIZ_NEV_DOCNUM);
   memcpy(pOutbuf+OFF_SALE1_DT, acDate, SIZ_SALE1_DT);

   // Sale Code
   if (pSale->Full_Partial_Flag[0] > ' ')
      *(pOutbuf+OFF_SALE1_CODE) = pSale->Full_Partial_Flag[0];

   memcpy(pOutbuf+OFF_SELLER, pSale->Remarks, SIZ_SELLER);

   lPrice = atoin(pSale->DocTax, SSIZ_NEV_STAMP_AMOUNT);
   if (lPrice > 0)
   {
      sprintf(acTmp, "%*d", SIZ_SALE1_AMT, lPrice);
      memcpy(pOutbuf+OFF_SALE1_AMT, acTmp, SIZ_SALE1_AMT);
   }

   // Update transfers
   lLstSaleDt = atoin(pOutbuf+OFF_TRANSFER_DT, SIZ_TRANSFER_DT);
   if (lCurSaleDt >= lLstSaleDt)
   {
      memset(pOutbuf+OFF_TRANSFER_DOC, ' ', SIZ_SALE1_DOC);
      memset(pOutbuf+OFF_TRANSFER_DT, ' ', SIZ_SALE1_DT);

      memcpy(pOutbuf+OFF_TRANSFER_DOC, pSale->DocNum, SSIZ_NEV_DOCNUM);
      memcpy(pOutbuf+OFF_TRANSFER_DT, acDate, SIZ_TRANSFER_DT);
   }

   *(pOutbuf+OFF_AR_CODE1) = 'A';

   return 1;
}

/********************************* Nev_MergeSale *****************************
 *
 *
 *
 *****************************************************************************/

int Nev_MergeSale(char *pOutbuf, bool bNewRec=false)
{
   static   char  acRec[1024], *pRec=NULL;

   int      iRet, iLoop;
   int      iSaleCnt;
   char     *pTmp;

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "009000130", 9) )
   //   iRet = 0;
#endif

   // Get first sale rec for first call
   if (!pRec)
      pTmp = fgets(acRec,  1000, fdSale);

   do
   {
      iLoop = memcmp(pOutbuf, &acRec[SOFF_NEV_APN], SSIZ_NEV_APN);
      if (iLoop > 0)
      {
         pTmp = fgets(acRec,  1000, fdSale);
         if (!pTmp)
         {
            fclose(fdSale);
            fdSale = NULL;
            return 1;      // EOF
         }
      }
   } while (iLoop > 0);

   // If not match, return
   if (iLoop)
      return 1;

   iSaleCnt = 0;
   do
   {
      iSaleCnt++;
      iRet = Nev_MergeSaleRec(pOutbuf, acRec, bNewRec);
      // Get next sale record
      pTmp = fgets(acRec,  1000, fdSale);
      if (!pTmp)
      {
         fclose(fdSale);
         fdSale = NULL;
         break;      // EOF
      }

      // Update flag
      *(pOutbuf+OFF_AR_CODE1) = 'A';
   } while (!memcmp(pOutbuf, &acRec[SOFF_NEV_APN], SSIZ_NEV_APN) && iSaleCnt < 3);

   lSaleMatch++;
   return 0;

}

/********************************* Nev_Load_LDR *****************************
 *
 * Use sale data from roll file instead of sale file
 *
 ****************************************************************************/

int Nev_Load_LDR(int iLoadFlag, int iFirstRec)
{
   char     *pTmp, acTmpFile[_MAX_PATH], acBuf[MAX_RECSIZE], acRollRec[MAX_RECSIZE], cSitusDelim;
   int      iRet;
   DWORD    nBytesWritten;
   BOOL     bRet, bEof;
   HANDLE   fhOut;
   long     lRet=0, lCnt=0;

   iUseMailCity = 0;

   // Open roll file
   fdRoll = fopen(acRollFile, "r");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", acRollFile);
      return 2;
   }

   // Open Char file
   fdChar = fopen(acCChrFile, "r");
   if (fdChar == NULL)
   {
      LogMsg("***** Error opening Char file: %s\n", acCChrFile);
      return 2;
   }

   // Situs file
   GetIniString(myCounty.acCntyCode, "SitusDelim", ";", acBuf, _MAX_PATH, acIniFile);
   cSitusDelim = acBuf[0];
   GetIniString(myCounty.acCntyCode, "SitusFile", "", acBuf, _MAX_PATH, acIniFile);
   fdSitus = fopen(acBuf, "r");
   if (fdSitus == NULL)
   {
      LogMsg("***** Error opening Situs file: %s\n", acBuf);
      return -2;
   }

   // Open Output file
   sprintf(acTmpFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "R01");
   LogMsg("Open output file %s", acTmpFile);
   fhOut = CreateFile(acTmpFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);
   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening output file: %s\n", acTmpFile);
      return 4;
   }

   // Output first header record
   if (iFirstRec > 0)
   {
      memset(acBuf, '9', iRecLen);
      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
   }

   // Get first RollRec
   pTmp = fgets(acRollRec, 1024, fdRoll);
   bEof = false;

   // Init variables
   iNoMatch=iBadCity=iBadSuffix=0;

   // Merge loop
   while (!bEof)
   {
      lLDRRecCount++;

      // V - voided parcels.  A - Active N - Non-assessed (govt, public, util)
      if (acRollRec[ROFF_NEV_STATUS] != 'V')
      {
         iRet = Nev_MergeRoll(acBuf, acRollRec, CREATE_R01|CLEAR_R01);

         // Merge Char
         if (fdChar)
            iRet = Nev_MergeChar(acBuf);

         // Merge Situs
         if (fdSitus)
            iRet = Nev_MergeSitus(acBuf, cSitusDelim);

         // Save last recording date
         lRet = atoin((char *)&acBuf[OFF_TRANSFER_DT], 8);
         if (lRet > lLastRecDate && lRet < lToday)
            lLastRecDate = lRet;

#ifdef _DEBUG
         iRet = replChar(acBuf, 0, ' ', iRecLen);
         if (iRet)
            LogMsg("*** WARNING: Null char found at position %d for record# %d -->%.10s", iRet, lCnt, acBuf);
#endif

         bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
         if (!(++lCnt % 1000))
            printf("\r%u", lCnt);

         if (!bRet)
         {
            LogMsg("***** Error writing to output file at record %d\n", lCnt);
            lRet = WRITE_ERR;
            break;
         }
      }

      pTmp = fgets(acRollRec, 1024, fdRoll);
      if (!pTmp)
         bEof = true;
   }

   // Close files
   if (fdRoll)
      fclose(fdRoll);
   if (fdChar)
      fclose(fdChar);
   if (fdSitus)
      fclose(fdSitus);
   if (fhOut)
      CloseHandle(fhOut);

   LogMsg("Total input records:        %u", lLDRRecCount);
   LogMsg("Total output records:       %u", lCnt);
   LogMsg("Total bad-city records:     %u", iBadCity);
   LogMsg("Total mail city used:       %u", iUseMailCity);
   LogMsg("Total bad-suffix records:   %u", iBadSuffix);
   LogMsg("Last recording date:        %u", lLastRecDate);

   LogMsg("Number of Char matched:     %u", lCharMatch);
   LogMsg("Number of Char skiped:      %u", lCharSkip);
   LogMsg("Number of Situs matched:    %u", lSitusMatch);
   LogMsg("Number of Situs skiped:     %u", lSitusSkip);

   printf("\nTotal output records: %u", lCnt);

   lRecCnt = lCnt;
   return 0;
}

/********************************* Nev_Load_Roll ****************************
 *
 *
 ****************************************************************************/

int Nev_Load_Roll(int iLoadFlag, int iSkip)
{
   char     acRec[MAX_RECSIZE], acBuf[MAX_RECSIZE], acRollRec[MAX_RECSIZE];
   char     *pTmp, acRawFile[_MAX_PATH], acOutFile[_MAX_PATH], cSitusDelim;

   HANDLE   fhIn, fhOut;
   NEV_ROLL *pRec;

   int      iRet, iTmp, iRollUpd=0, iNewRec=0, iRetiredRec=0;
   DWORD    nBytesRead;
   DWORD    nBytesWritten;
   BOOL     bRet, bEof;
   long     lRet=0, lCnt=0;

   cSitusDelim = ';';
   iUseMailCity = 0;
   sprintf(acRawFile, acRawTmpl, "Nev", "Nev", "S01");
   sprintf(acOutFile, acRawTmpl, "Nev", "Nev", "R01");

   // Rename files for processing
   if (_access(acRawFile, 0))
   {
      if (!_access(acOutFile, 0))
         rename(acOutFile, acRawFile);
      else
      {
         LogMsg("***** Missing input file %s.  Please recheck!");
         return -1;
      }
   }

   // Open file
   LogMsg("Open Roll file %s", acRollFile);
   fdRoll = fopen(acRollFile, "rb");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", acRollFile);
      return -2;
   }

   // Open Char file
   fdChar = fopen(acCChrFile, "r");
   if (fdChar == NULL)
   {
      LogMsg("***** Error opening Char file: %s\n", acCChrFile);
      return -2;
   }

   // Situs file
   GetIniString(myCounty.acCntyCode, "SitusDelim", ";", acBuf, _MAX_PATH, acIniFile);
   cSitusDelim = acBuf[0];
   GetIniString(myCounty.acCntyCode, "SitusFile", "", acBuf, _MAX_PATH, acIniFile);
   fdSitus = fopen(acBuf, "r");
   if (fdSitus == NULL)
   {
      LogMsg("***** Error opening Situs file: %s\n", acBuf);
      return -2;
   }

   // Open Input file
   LogMsg("Open input file %s", acRawFile);
   fhIn = CreateFile(acRawFile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhIn == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening input file: %s\n", acRawFile);
      return 3;
   }

   // Open Output file
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening output file: %s\n", acOutFile);
      return 4;
   }

   // Get first RollRec
   do
   {
      pTmp = fgets(acRollRec, 1024, fdRoll);
   } while (pTmp && acRollRec[ROFF_NEV_STATUS] == 'V');
   bEof = false;

   // Copy skip record
   memset(acBuf, ' ', iRecLen);
   while (iSkip-- > 0)
   {
      ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);
      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
   }

   iNoMatch=iBadCity=iBadSuffix=0;
   pRec = (NEV_ROLL *)&acRollRec[0];

   // Merge loop
   while (!bEof)
   {
      bRet = ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);

      if (!bRet)
      {
         LogMsg("***** Error reading input file %s (%f)", acRawFile, GetLastError());
         break;
      }

      if (iRecLen != nBytesRead)
         break;

      NextRollRec:
#ifdef _DEBUG
      //if (!memcmp(acBuf, "0638053000", 10) )
      //   bRet = 0;
#endif

      iTmp = memcmp(acBuf, &acRollRec[ROFF_NEV_APN], iApnLen);
      if (!iTmp)
      {
         // Merge roll data
         iRet = Nev_MergeRoll(acBuf, acRollRec, UPDATE_R01);

         // Merge Char
         if (fdChar)
            lRet = Nev_MergeChar(acBuf);

         // Merge Situs
         if (fdSitus)
            Nev_MergeSitus(acBuf, cSitusDelim);

         iRollUpd++;
         bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);

         // Read next roll record
         do
         {
            pTmp = fgets(acRollRec, 1024, fdRoll);
         } while (pTmp && (acRollRec[ROFF_NEV_STATUS] == 'V' || acRollRec[ROFF_NEV_APN] == ' '));

         if (!pTmp)
            bEof = true;
      } else if (iTmp > 0)       // Roll not match, new roll record
      {
         if (bDebug)
            LogMsg0("New roll record : %.*s (%d)", iApnLen, &acRollRec[ROFF_NEV_APN], lCnt);

         // Create new R01 record
         iRet = Nev_MergeRoll(acRec, acRollRec, CREATE_R01);

         // Merge Char
         if (fdChar)
            lRet = Nev_MergeChar(acRec);

         // Merge Situs
         if (fdSitus)
            Nev_MergeSitus(acRec, cSitusDelim);

         iNewRec++;
         bRet = WriteFile(fhOut, acRec, iRecLen, &nBytesWritten, NULL);
         if (!(++lCnt % 1000))
            printf("\r%u", lCnt);

         do
         {
            pTmp = fgets(acRollRec, 1024, fdRoll);
         } while (pTmp && (acRollRec[ROFF_NEV_STATUS] == 'V' || acRollRec[ROFF_NEV_APN] == ' '));

         if (!pTmp)
            bEof = true;
         else
            goto NextRollRec;
      } else
      {
         // Record may be retired
         if (bDebug)
            LogMsg0("*** Roll not match (retired record?) : R01->%.*s < Roll->%.*s (%d) ***", iApnLen, acBuf, iApnLen, &acRollRec[ROFF_NEV_APN], lCnt);
         iRetiredRec++;
         continue;
      }

      // Save last recording date
      lRet = atoin((char *)&acBuf[OFF_TRANSFER_DT], 8);
      if (lRet > lLastRecDate && lRet < lToday)
         lLastRecDate = lRet;

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);

      if (!bRet)
      {
         LogMsg("***** Error occurs: %d\n", GetLastError());
         lRet = WRITE_ERR;
         break;
      }
   }

   // If there are more new records, add them
   while (!bEof)
   {
      if (bDebug)
         LogMsg0("*** New roll record : %.*s (%d) ***", iApnLen, &acRollRec[ROFF_NEV_APN], lCnt);

      if (acRollRec[ROFF_NEV_STATUS] != 'V')
      {
         // Create new R01 record
         iRet = Nev_MergeRoll(acRec, acRollRec, CREATE_R01);

         // Merge Char
         if (fdChar)
            lRet = Nev_MergeChar(acRec);

         // Merge Situs
         if (fdSitus)
            Nev_MergeSitus(acRec, cSitusDelim);

         iNewRec++;

         // Save last recording date
         lRet = atoin((char *)&acRec[OFF_TRANSFER_DT], 8);
         if (lRet > lLastRecDate && lRet < lToday)
            lLastRecDate = lRet;

         bRet = WriteFile(fhOut, acRec, iRecLen, &nBytesWritten, NULL);
      }

      lCnt++;

      // Get next roll record
      do
      {
         pTmp = fgets(acRollRec, 1024, fdRoll);
      } while (pTmp && (acRollRec[ROFF_NEV_STATUS] == 'V' || acRollRec[ROFF_NEV_APN] == ' '));

      if (!pTmp)
         break;
   }

   // Close files
   if (fdRoll)
      fclose(fdRoll);
   if (fdChar)
      fclose(fdChar);
   if (fdSitus)
      fclose(fdSitus);
   if (fhOut)
      CloseHandle(fhOut);
   if (fhIn)
      CloseHandle(fhIn);

   LogMsg("Total output records:       %u", lCnt);
   LogMsg("Total new records:          %u", iNewRec);
   LogMsg("Total retired records:      %u", iRetiredRec);
   LogMsg("Total bad-city records:     %u", iBadCity);
   LogMsg("Total mail city used:       %u", iUseMailCity);
   LogMsg("Total bad-suffix records:   %u", iBadSuffix);


   LogMsg("Number of Char matched:     %u", lCharMatch);
   LogMsg("Number of Char skiped:      %u", lCharSkip);
   LogMsg("Number of Situs matched:    %u", lSitusMatch);
   LogMsg("Number of Situs skiped:     %u", lSitusSkip);

   printf("\nTotal output records: %u", lCnt);

   lRecCnt = lCnt;
   return 0;
}

/********************************* formatSale ********************************
 *
 * Return sale price.
 *
 * "88-031824";"11/22/1988"
 * "86007859";"04/11/1986"    -> 86-007589
 * "80000727";"01/09/1980"    -> 80-000727
 * "01017519";"02/09/1979"    -> 1017/519
 * "00983513";"09/25/1978"    -> 983/513
 *
 *****************************************************************************/

int Nev_FormatSale(char *pFmtSale)
{
   SCSAL_REC *pCSale= (SCSAL_REC *)pFmtSale;
   long     lTmp, iTmp;
   char     acTmp[32], *pTmp;

   // Inititalize
   memset(pFmtSale, ' ', sizeof(SCSAL_REC));
   memcpy(pCSale->Apn, apTokens[NCALS1_APN], strlen(apTokens[NCALS1_APN]));

#ifdef _DEBUG
   //if (!memcmp(pCSale->Apn, "0119009000", 10) )
   //   lTmp = 0;
#endif

   // SaleDate
   pTmp = dateConversion(apTokens[NCALS1_DEEDDT], acTmp, MM_DD_YYYY_1);
   if (pTmp)
   {
      memcpy(pCSale->DocDate, acTmp, SALE_SIZ_DOCDATE);
      iTmp = atoin(acTmp, 4);
   } else
      iTmp = 0;

   // Sale Doc
   pTmp = apTokens[NCALS1_DEEDNUM];
   if (*(pTmp+2) == '-')
      strcpy(acTmp, pTmp);
   else if (iTmp < 1980)
   {
      iTmp = atoin(pTmp, 5);
      sprintf(acTmp, "%d/%.3s", iTmp, pTmp+5);
   } else if (iTmp > 0)
   {
      sprintf(acTmp, "%.2s-%.6s", pTmp, pTmp+2);
   } else
      acTmp[0] = 0;
   memcpy(pCSale->DocNum, acTmp, strlen(acTmp));

   // Sale price
   lTmp = atol(apTokens[NCALS1_SALEPR]);
   if (lTmp > 0)
   {
      iTmp = sprintf(acTmp, "%*d", SALE_SIZ_SALEPRICE, lTmp);
      memcpy(pCSale->SalePrice, acTmp, iTmp);
   }

   // Multi sale
   if (*apTokens[NCALS1_GROUPSL] == 'Y')
   {
      pCSale->NumOfPrclXfer[0] = 'M';
      pCSale->MultiSale_Flg = 'Y';
   }

   // DocType

   // Sale code

   pCSale->CRLF[0] = '\n';
   pCSale->CRLF[1] = 0;

   return lTmp;
}

/****************************** Nev_UpdateCumSale ****************************
 *
 * Update history sale.  This function adds new sale record to cumulative file.
 *
 * Return 0 if successful, otherwise error
 *
 *****************************************************************************/

int Nev_UpdateCumSale(int iMaxFlds, char bDelim, bool bChkPrice)
{
   char     *pTmp, acCSalRec[512], acSaleRec[1024], acTmp[256];
   char     acOutFile[_MAX_PATH], acHistSale[_MAX_PATH], acTmpSale[_MAX_PATH];

   int      iRet, iTmp, iUpdateSale=0;
   long     lLstSaleDt=0, lCnt=0;
   SCSAL_REC *pSaleRec=(SCSAL_REC *)&acCSalRec[0];

   // Check current sale file
   if (_access(acSaleFile, 0))
   {
      LogMsg("***** Missing current sale file %s", acSaleFile);
      return -1;
   }

   // Open current sale
   LogMsg("Open sale file %s", acSaleFile);
   fdSale = fopen(acSaleFile, "r");
   if (fdSale == NULL)
   {
      LogMsg("***** Error opening current sale file: %s\n", acSaleFile);
      return -2;
   }
   // Drop header record
   pTmp = fgets(acSaleRec, 1024, fdSale);
   //iRet = ParseStringNQ(acSaleRec, ';', NCALS1_ECON_OBS+2, apTokens);

   // Create output file
   sprintf(acTmpSale, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "tmp");
   LogMsg("Create updated sale file %s", acTmpSale);
   fdCSale = fopen(acTmpSale, "w");
   if (fdCSale == NULL)
   {
      LogMsg("***** Error creating updated sale file: %s\n", acTmpSale);
      return -3;
   }

   // Read first history sale
   acCSalRec[0] = 0;

   // Merge loop
   while (!feof(fdSale))
   {
      // Get current sale
      pTmp = fgets(acSaleRec, 1024, fdSale);
      if (!pTmp)
         break;

      //iRet = ParseStringNQ(acSaleRec, ';', NCALS1_ECON_OBS+2, apTokens);
      iRet = ParseStringNQ(acSaleRec, bDelim, iMaxFlds+2, apTokens);
      if (iRet >= iMaxFlds)
      {
         iRet = Nev_FormatSale(acCSalRec);
         if (iRet > 0 || !bChkPrice)
         {
            // Output only record with sale price or if it's not required
            fputs(acCSalRec, fdCSale);
            iUpdateSale++;
            iTmp = atoin(pSaleRec->DocDate, 8);
            if (iTmp > lLstSaleDt && iTmp < lToday)
               lLstSaleDt = iTmp;
         }
      } else
         LogMsg("Bad input record (%d): %s", iRet, apTokens[NCALS1_APN]);

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);

   }

   // Close files
   if (fdCSale)
      fclose(fdCSale);
   if (fdSale)
      fclose(fdSale);

   // Update sale history file
   sprintf(acHistSale, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "sls");
   sprintf(acOutFile,  acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "dat");

   // Sort/merge sale file - APN (asc), Saledate (asc), DocNum (asc)
   sprintf(acTmp, "S(1,%d,C,A,27,8,C,A,15,%d,C,A,57,10,C,D) DUPO(1,34)", iApnLen, SALE_SIZ_DOCNUM);
   iTmp = sortFile(acTmpSale, acOutFile, acTmp);
   if (iTmp > 0)
   {
      strcpy(acSaleFile, acOutFile);
      if (!_access(acHistSale, 0))
      {
         sprintf(acSaleRec, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "srt");
         sprintf(acTmpSale, "%s+%s", acOutFile, acHistSale);
         iTmp = sortFile(acTmpSale, acSaleRec, acTmp);
         if (iTmp > 0)
         {
            DeleteFile(acHistSale);
      
            // Rename srt to SLS file
            rename(acSaleRec, acHistSale);
            strcpy(acSaleFile, acHistSale);
         }
      } else
      {
         CopyFile(acOutFile, acHistSale, false);
      }
   }

   LogMsg("Total sale records processed:   %u", lCnt);
   LogMsg("Total cumulative sale records:  %u", iTmp);
   LogMsg("Total sale records updated:     %u", iUpdateSale);
   LogMsg("Latest recording date:          %u", lLstSaleDt);

   LogMsg("Update Sale History completed.");

   return 0;
}

/****************************** Nev_FmtChar **********************************
 *
 *
 *****************************************************************************/

int Nev_FmtChar(char *pCharbuf)
{
   char     *pTmp, acTmp[_MAX_PATH], acCode[16];
   double   dTmp;
   int      iTmp;
   STDCHAR  *pCharRec=(STDCHAR *)pCharbuf;

   // APN
   memcpy(pCharRec->Apn, apTokens[NCDESC1_APN], strlen(apTokens[NCDESC1_APN]));

#ifdef _DEBUG
   //if (!memcmp(pCharRec->Apn, "1858506000", 10) )
   //   iTmp = 0;
#endif

   // Acreage
   dTmp = atof(apTokens[NCDESC1_ACREAGE]);
   if (dTmp > 0.0)
   {
      iTmp = sprintf(acTmp, "%d", (long)(dTmp*1000));
      memcpy(pCharRec->LotAcre, acTmp, iTmp);

      // LotSqft
      dTmp *= SQFT_PER_ACRE;
      iTmp = sprintf(acTmp, "%d", (long)dTmp);
      memcpy(pCharRec->LotSqft, acTmp, iTmp);
   }

   // Units
   iTmp = atol(apTokens[NCDESC1_UNITS]);
   if (iTmp > 0)
   {
      iTmp = sprintf(acTmp, "%d", iTmp);
      memcpy(pCharRec->Units, acTmp, iTmp);
   }

   // Zoning
   if (*apTokens[NCDESC1_ZONE] > ' ')
   {
      iTmp = strlen(apTokens[NCDESC1_ZONE]);
      if (iTmp <= SIZ_CHAR_ZONING)
         memcpy(pCharRec->Zoning, apTokens[NCDESC1_ZONE], iTmp);
      //else
      //   LogMsg("Zoning ???: %s", apTokens[NCDESC1_ZONE]);
   }

   // Water
   pTmp = apTokens[NCDESC1_WATER];
   if (*pTmp > ' ' && *pTmp != '?')
   {
      if (!_memicmp(pTmp, "PUB", 3))
         pCharRec->HasWater = 'P';
      else if (!_memicmp(pTmp, "WEL", 3))
         pCharRec->HasWater = 'W';
      else if (!_memicmp(pTmp, "DIT", 3))
         pCharRec->HasWater = 'D';
      else if (!_memicmp(pTmp, "CRE", 3))
         pCharRec->HasWater = 'E';
      else
         LogMsg("Unknown Water Service [%s]", pTmp);
   }

   // Sewer
   if (*apTokens[NCDESC1_SEWER] > ' ')
      pCharRec->HasSewer = *apTokens[NCDESC1_SEWER];

   // View
   pTmp = apTokens[NCDESC1_VIEW];
   if (*pTmp > ' ' && *pTmp != '?')
   {
      if (!_memicmp(pTmp, "AV", 2))
         pCharRec->View[0] = '3';
      else if (!_memicmp(pTmp, "GO", 2))
         pCharRec->View[0] = '4';
      else if (!_memicmp(pTmp, "EX", 2))
         pCharRec->View[0] = '5';
      else if (!_memicmp(pTmp, "POOR", 4))
         pCharRec->View[0] = '9';
      else if (!_memicmp(pTmp, "LAKE", 4))
         pCharRec->View[0] = 'F';
      else
         LogMsg("Unknown View [%s]", pTmp);
   }

   // Frontage
   pTmp = apTokens[NCDESC1_FRONTAGE];
   if (*pTmp > ' ' && *pTmp != '?')
   {
      if (!_memicmp(pTmp, "BU", 2))
         pCharRec->Frontage[0] = 'B';
      else if (!_memicmp(pTmp, "CR", 2))
         pCharRec->Frontage[0] = 'C';
      else if (!_memicmp(pTmp, "FL", 2))
         pCharRec->Frontage[0] = 'F';
      else if (!_memicmp(pTmp, "GO", 2))
         pCharRec->Frontage[0] = 'G';
      else if (!_memicmp(pTmp, "HI", 2))
         pCharRec->Frontage[0] = 'H';
      else if (!_memicmp(pTmp, "LA", 2))
         pCharRec->Frontage[0] = 'L';
      else if (!_memicmp(pTmp, "PO", 2))
         pCharRec->Frontage[0] = 'P';
      else if (!_memicmp(pTmp, "RU", 2))
         pCharRec->Frontage[0] = 'R';
      else if (!_memicmp(pTmp, "TY", 2))
         pCharRec->Frontage[0] = 'T';
      else
         LogMsg("Unknown Frontage [%s]", pTmp);
   }

   // Topo
   pTmp = apTokens[NCDESC1_TOPO];
   if (*pTmp > ' ' && *pTmp != '?')
   {
      if (!_memicmp(pTmp, "DOWN ST", 7) || !_memicmp(pTmp, "UP ST", 5))
         pCharRec->Topo[0] = 'T';
      else if (!_memicmp(pTmp, "LEVEL", 5))
         pCharRec->Topo[0] = 'L';
      else if (!_memicmp(pTmp, "ROLLI", 5))
         pCharRec->Topo[0] = 'R';
      else if (!_memicmp(pTmp, "SIDEH", 5))    // Hill side
         pCharRec->Topo[0] = 'H';
      else if (!_memicmp(pTmp, "UPSLO", 5) || !_memicmp(pTmp, "DOWNSL", 6))
         pCharRec->Topo[0] = 'D';
      else if (!_memicmp(pTmp, "VAR", 3))
         pCharRec->Topo[0] = 'V';
      else
         LogMsg("Unknown Topo [%s]", pTmp);
   }

   // Quality - BldgCls
   pTmp = apTokens[NCDESC1_QUALITY];
   if (isalpha(*pTmp))
   {  // D45A
      memcpy(pCharRec->QualityClass, pTmp, strlen(pTmp));

      pCharRec->BldgClass = *pTmp++;
      iTmp = atoin(pTmp, 2);
      if (iTmp > 0)
      {
         if (isdigit(*(pTmp+1)))
            sprintf(acTmp, "%c.%c", *pTmp, *(pTmp+1));
         else
            sprintf(acTmp, "%c.0", *pTmp);
         iTmp = Value2Code(acTmp, acCode, NULL);
         if (iTmp >= 0)
            pCharRec->BldgQual = acCode[0]; 
      }
   }

   // Story
   memcpy(pCharRec->Stories, apTokens[NCDESC1_STORY], strlen(apTokens[NCDESC1_STORY]));

   // YrBlt
   iTmp = atol(apTokens[NCDESC1_YR_BLT]+6);
   if (iTmp > 1900 && iTmp <= lToyear)
      memcpy(pCharRec->YrBlt, apTokens[NCDESC1_YR_BLT]+6, 4);

   // Eff year
   iTmp = atol(apTokens[NCDESC1_EFF_YR]+6);
   if (iTmp > 1900 && iTmp <= lToyear)
      memcpy(pCharRec->YrEff, apTokens[NCDESC1_EFF_YR]+6, 4);

   // Condition
   pTmp = apTokens[NCDESC1_CONDITION];
   if (*pTmp > ' ' && *pTmp != '?')
   {
      if (strchr("AGFEP", *pTmp))
         pCharRec->ImprCond[0] = *pTmp;
      else
         LogMsg("Unknown Condition [%s]", pTmp);
   }

   // Fire place
   memcpy(pCharRec->Fireplace, apTokens[NCDESC1_FIREPL], strlen(apTokens[NCDESC1_FIREPL]));

   // Air/Heating
   pTmp = apTokens[NCDESC1_AC_HEAT];
   if (*pTmp > ' ' && *pTmp != '?')
   {
      if (!_memicmp(pTmp, "CENTRAL A/C,H", 13))
      {
         pCharRec->Heating[0] = 'Z';
         pCharRec->Cooling[0] = 'C';
      } 
      else if (!_memicmp(pTmp, "CENTRAL H", 9))
         pCharRec->Heating[0] = 'Z';
      else if (!_memicmp(pTmp, "CENTRAL A/C", 11))
         pCharRec->Cooling[0] = 'C';
      else if (!_memicmp(pTmp, "NONE", 4))
         pCharRec->Heating[0] = 'L';
      else if (!_memicmp(pTmp, "WALL HEAT", 9))
         pCharRec->Heating[0] = 'D';
      else if (!_memicmp(pTmp, "BASEBOARD, E", 12))
         pCharRec->Heating[0] = 'F';
      else if (!_memicmp(pTmp, "BASEBOARD, W", 12))
         pCharRec->Heating[0] = 'E';
      else if (!_memicmp(pTmp, "FLOOR HEA", 9))
         pCharRec->Heating[0] = 'C';
      else if (!_memicmp(pTmp, "SOLAR", 5))
         pCharRec->Heating[0] = 'K';
      else if (!_memicmp(pTmp, "HEATED", 6))
         pCharRec->Heating[0] = 'Y';
      else
         LogMsg("Unknown AC/Heating [%s]", pTmp);
   }

   // Pool/Spa
   pTmp = apTokens[NCDESC1_POOL];
   if (*pTmp > ' ' && *pTmp != '?')
   {
      if (!_memicmp(pTmp, "BOTH", 4))
         pCharRec->Pool[0] = 'C';
      else if (!_memicmp(pTmp, "POOL", 4))
         pCharRec->Pool[0] = 'P';
      else if (!_memicmp(pTmp, "SPA", 3))
         pCharRec->Pool[0] = 'S';
      else
         LogMsg("Unknown pool type [%s]", pTmp);
   }

   // Beds
   memcpy(pCharRec->Beds, apTokens[NCDESC1_BEDRMS], strlen(apTokens[NCDESC1_BEDRMS]));

   // Baths
   int iBath = atol(apTokens[NCDESC1_BATHS]);
   if ((pTmp = strchr(apTokens[NCDESC1_BATHS], '.')))
   {     
      pCharRec->Bath_4Q[0] = iBath|0x30;

      // After decimal point: 1, 2, 3, 4, 5, 6, 7, 8, 9
      // We can make BATH_1Q <=3, Bath_2Q <= 6, BATH_3Q <=9
      if (*(pTmp+1) > '0')
      {
         pCharRec->HBaths[0] = '1';
         if (*(pTmp+1) <= '3')
            pCharRec->Bath_1Q[0] = '1';
         else if (*(pTmp+1) <= '6')
            pCharRec->Bath_2Q[0] = '1';
         else 
            pCharRec->Bath_3Q[0] = '1';
      }

      iTmp = sprintf(acTmp, "%d", iBath);
      memcpy(pCharRec->FBaths, acTmp, iTmp);
   } else
   {
      memcpy(pCharRec->FBaths, apTokens[NCDESC1_BATHS], strlen(apTokens[NCDESC1_BATHS]));
      pCharRec->Bath_4Q[0] = iBath|0x30;
   }

   // Rooms
   memcpy(pCharRec->Rooms, apTokens[NCDESC1_ROOMS], strlen(apTokens[NCDESC1_ROOMS]));

   // Bldg Sqft
   memcpy(pCharRec->BldgSqft, apTokens[NCDESC1_SQFT], strlen(apTokens[NCDESC1_SQFT]));

   // SqftFlr1
   memcpy(pCharRec->Sqft_1stFl, apTokens[NCDESC1_1ST_FL_SQFT], strlen(apTokens[NCDESC1_1ST_FL_SQFT]));

   // SqftFlr2
   memcpy(pCharRec->Sqft_2ndFl, apTokens[NCDESC1_2ND_FL_SQFT], strlen(apTokens[NCDESC1_2ND_FL_SQFT]));

   // Basement Sqft
   memcpy(pCharRec->BsmtSqft, apTokens[NCDESC1_BASE_SQFT], strlen(apTokens[NCDESC1_BASE_SQFT]));

   // Garage type
   pTmp = apTokens[NCDESC1_GARAGE];
   if (*pTmp > ' ' && *pTmp != '?')
   {
      if (!_memicmp(pTmp, "GAR", 3))
         pCharRec->ParkType[0] = 'Z';
      else if (!_memicmp(pTmp, "CAR", 3))
         pCharRec->ParkType[0] = 'C';
      else if (!_memicmp(pTmp, "BO", 2))
         pCharRec->ParkType[0] = '2';
      else
         LogMsg("Unknown parking type [%s]", pTmp);

      // Garage Sqft
      memcpy(pCharRec->GarSqft, apTokens[NCDESC1_GAR_SQFT], strlen(apTokens[NCDESC1_GAR_SQFT]));
   }

   // Porch Sqft
   memcpy(pCharRec->PorchSqft, apTokens[NCDESC1_PORCH_SQFT], strlen(apTokens[NCDESC1_PORCH_SQFT]));

   // Flatwork Sqft
   memcpy(pCharRec->FlatWrkSqft, apTokens[NCDESC1_FLATWRK_SQFT], strlen(apTokens[NCDESC1_FLATWRK_SQFT]));

   // Misc Sqft
   memcpy(pCharRec->MiscSqft, apTokens[NCDESC1_MISC_SQFT], strlen(apTokens[NCDESC1_MISC_SQFT]));

   // Guest Sqft
   memcpy(pCharRec->GuestSqft, apTokens[NCDESC1_GUEST_SQFT], strlen(apTokens[NCDESC1_GUEST_SQFT]));

   // Power
   if (*apTokens[NCDESC1_POWER] > ' ')
      pCharRec->Power[0] = *apTokens[NCDESC1_POWER];

   // Gas
   if (*apTokens[NCDESC1_GAS] > ' ')
      pCharRec->Gas[0] = *apTokens[NCDESC1_GAS];

   pCharRec->CRLF[0] = '\n';
   pCharRec->CRLF[1] = '\0';

   return 0;
}

/***************************** Nev_ConvertChar *******************************
 *
 * Extract characteristics from NCDESC1.TXT.
 *
 * Return 0 if successful, otherwise error
 *
 *****************************************************************************/

int Nev_ConvertChar(void)
{
   char  *pTmp, bDelim, acCharRec[2048], acInRec[MAX_RECSIZE], acTmpFile[_MAX_PATH];
   long  lCnt=0, lChars=0, iRet;
   FILE  *fdIn;

   GetIniString(myCounty.acCntyCode, "AllCharDelim", ";", acTmpFile, _MAX_PATH, acIniFile);
   bDelim = acTmpFile[0];
   GetIniString(myCounty.acCntyCode, "AllChar", "", acTmpFile, _MAX_PATH, acIniFile);
   LogMsg("Extracting Chars from %s", acTmpFile);

   // Check current sale file
   if (_access(acTmpFile, 0))
   {
      LogMsg("***** Missing input file %s", acTmpFile);
      return -1;
   }

   // Open char file
   LogMsg("Open char file to extract CHARS %s", acTmpFile);
   fdIn = fopen(acTmpFile, "r");
   if (fdIn == NULL)
   {
      LogMsg("***** Error opening char file: %s\n", acTmpFile);
      return -2;
   }
   // Drop header record
   pTmp = fgets(acInRec, MAX_RECSIZE, fdIn);

   // Create output file
   sprintf(acTmpFile, "%s\\%s\\%s_Char.Dat", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode); 
   LogMsg("Create output chars file %s", acTmpFile);
   fdChar = fopen(acTmpFile, "w");
   if (fdChar == NULL)
   {
      LogMsg("***** Error creating char file: %s\n", acTmpFile);
      return -3;
   }

   // Merge loop
   while (!feof(fdIn))
   {
      // Get next record
      pTmp = fgets(acInRec, MAX_RECSIZE, fdIn);
      if (!pTmp)
         break;

      _strupr(acInRec);
      iRet = ParseStringNQ(acInRec, bDelim, NCDESC1_ECON_OBS+2, apTokens);
      if (iRet >= NCDESC1_ECON_OBS)
      {
         memset(acCharRec, ' ', sizeof(STDCHAR));
         iRet = Nev_FmtChar(acCharRec);
         fputs(acCharRec, fdChar);
         lChars++;
      } else
         LogMsg("Bad input record (%d): %s", iRet, apTokens[NCDESC1_APN]);

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);

   }

   // Close files
   if (fdChar)
      fclose(fdChar);
   if (fdIn)
      fclose(fdIn);

   LogMsg("Total input records processed: %u", lCnt);
   LogMsg("Total char records output:     %u", lChars);

   // Sort output
   sprintf(acInRec, "S(1,%d,C,A) ", iApnLen);
   iRet = sortFile(acTmpFile, acCChrFile, acInRec);

   return lCnt;
}

/******************************************************************************
 
   - Reason:
      N: Dual value of Prop8 & Prop 13
      Q: Prop 13 reinstatement
      9: Other
      A: 1975 Base Year Value
      B: Reassessment due to change in ownership
      C: Reassessment due to change in ownership
      D: Transfer of Base Year Value (Sec. 69.5)
      F: Mills Act
      G: Value change due to construction
      H: Value change due to construction
      I: Value change due to construction
      J: Value change due to construction
      M: Decline in value, Calamity
      P: Other
      R: Removal of improvement (Demolition)
      S: Other change in value (Roll correction, split/combo, etc)
      T: Property in timber preserve zone (TPZ)
      W: Open space (CLCA/Williamson Act)

   - iType = 1 for NVAL record and 2 for QVAL & RVAL

   - Input records has various reasons, most are N(prop8) & Q(reinstatement of original BYV)
   - If reason='N', set pv_vst_type='1', pv_reason='N'
   - If reason='Q', set pv_vst_type='1', pv_reason='Q' and 
     add another record with pv_vst_type='2' and pv_reason=' '
   - If reason=others, set pv_vst_type='1', pv_reason=others (county specific)

 ******************************************************************************/

int Nev_ParseValues(FILE *fdOut, LPSTR pInbuf, LPSTR pOutbuf, int iType, char cDel)
{
   VALUE_REC   *pVal = (VALUE_REC *)pOutbuf;
   int         iRet, lVal, lLand, lImpr, lOther;
   char        sTmp[32], *pTmp;

#ifdef _DEBUG
   //if (!memcmp(pInbuf, "075250001000", 9))
   //   iRet = 0;
#endif
   // Parse input rec
   iTokens = ParseStringNQ(pInbuf, cDel, 64, apTokens);
   if (iTokens >= NEV_REASON && *apTokens[NEV_REASON] > ' ')
   {
      memset(pOutbuf, ' ', sizeof(VALUE_REC));
      iRet = remChar(apTokens[NEV_APN], '-');
      memcpy(pVal->Apn, apTokens[NEV_APN], iRet);
      pTmp = strchr(apTokens[NEV_TAXYEAR], ',');
      if (pTmp)
      {
         pTmp++;
         memcpy(pVal->TaxYear, pTmp, strlen(pTmp));
      }

      // We always set first record to 1
      pVal->ValueSetType[0] = '1';   
      pVal->Reason[0] = *apTokens[NEV_REASON];
      dollar2Num(apTokens[NEV_LANDTOTAL], sTmp);
      lLand = atol(sTmp);
      if (lLand > 0)
      {
         iRet = sprintf(sTmp, "%d", lLand);
         memcpy(pVal->Land, sTmp, iRet);
      }

      dollar2Num(apTokens[NEV_IMPRTOTAL], sTmp);
      lImpr = atol(sTmp);
      if (lImpr > 0)
      {
         iRet = sprintf(sTmp, "%d", lImpr);
         memcpy(pVal->Impr, sTmp, iRet);
      }

      dollar2Num(apTokens[NEV_FIXTVAL], sTmp);
      lOther = atol(sTmp);
      if (lOther > 0)
      {
         iRet = sprintf(sTmp, "%d", lOther);
         memcpy(pVal->Fixtr, sTmp, iRet);
      }

      dollar2Num(apTokens[NEV_PPVAL], sTmp);
      lVal = atol(sTmp);
      if (lVal > 0)
      {
         iRet = sprintf(sTmp, "%d", lVal);
         memcpy(pVal->PersProp, sTmp, iRet);
         lOther += lVal;
      }

      if (lOther > 0)
      {
         iRet = sprintf(sTmp, "%d", lOther);
         memcpy(pVal->Other, sTmp, iRet);
      }

      // Gross
      dollar2Num(apTokens[NEV_GROSS], sTmp);
      lVal = atol(sTmp);
      if (lVal > 0)
      {
         iRet = sprintf(sTmp, "%d", lVal);
         memcpy(pVal->TotalVal, sTmp, iRet);
      }

      // Net
      dollar2Num(apTokens[NEV_VALUETOTAL], sTmp);
      lVal = atol(sTmp);
      if (lVal > 0)
      {
         iRet = sprintf(sTmp, "%d", lVal);
         memcpy(pVal->NetVal, sTmp, iRet);
      }

      // Exemption
      dollar2Num(apTokens[NEV_EXMPTNVAL], sTmp);
      lVal = atol(sTmp);
      if (lVal > 0)
      {
         iRet = sprintf(sTmp, "%d", lVal);
         memcpy(pVal->Exe, sTmp, iRet);
      }

      pVal->CRLF[0] = '\n';
      pVal->CRLF[1] = 0;
      if (iType == 1)
      {
         // Output current record
         fputs(pOutbuf, fdOut);

         // Create base year record - retain APN & TaxYear
         memset(pOutbuf+30, ' ', sizeof(VALUE_REC)-32);

         // Create a base year value record
         pVal->ValueSetType[0] = '2';  // Base year value record

         dollar2Num(apTokens[NEV_P13LANDTOTAL], sTmp);
         lVal = atol(sTmp);
         if (lVal > 0)
         {
            iRet = sprintf(sTmp, "%d", lVal);
            memcpy(pVal->Land, sTmp, iRet);
         }

         dollar2Num(apTokens[NEV_P13IMPRTOTAL], sTmp);
         lVal = atol(sTmp);
         if (lVal > 0)
         {
            iRet = sprintf(sTmp, "%d", lVal);
            memcpy(pVal->Impr, sTmp, iRet);
         }

         dollar2Num(apTokens[NEV_P13FIXTVAL], sTmp);
         lOther = atol(sTmp);
         if (lOther > 0)
         {
            iRet = sprintf(sTmp, "%d", lOther);
            memcpy(pVal->Fixtr, sTmp, iRet);
         }

         dollar2Num(apTokens[NEV_P13PPVAL], sTmp);
         lVal = atol(sTmp);
         if (lVal > 0)
         {
            iRet = sprintf(sTmp, "%d", lVal);
            memcpy(pVal->PersProp, sTmp, iRet);
            lOther += lVal;
         }

         if (lOther > 0)
         {
            iRet = sprintf(sTmp, "%d", lOther);
            memcpy(pVal->Other, sTmp, iRet);
         }

         // Gross
         dollar2Num(apTokens[NEV_P13GROSS], sTmp);
         lVal = atol(sTmp);
         if (lVal > 0)
         {
            iRet = sprintf(sTmp, "%d", lVal);
            memcpy(pVal->TotalVal, sTmp, iRet);
         }

         // Net
         dollar2Num(apTokens[NEV_P13VALUETOTAL], sTmp);
         lVal = atol(sTmp);
         if (lVal > 0)
         {
            iRet = sprintf(sTmp, "%d", lVal);
            memcpy(pVal->NetVal, sTmp, iRet);
         }

         // Exemption
         dollar2Num(apTokens[NEV_P13EXMPTNVAL], sTmp);
         lVal = atol(sTmp);
         if (lVal > 0)
         {
            iRet = sprintf(sTmp, "%d", lVal);
            memcpy(pVal->Exe, sTmp, iRet);
         }
         iRet = 2;
      } else if (iType == 2)
      {
         // Output current record
         fputs(pOutbuf, fdOut);

         // Create a base year value record
         pVal->ValueSetType[0] = '2';  // Base year value record
         pVal->Reason[0] = ' ';
         memset(pVal->Exe, ' ', PV_SIZ_EXE);
         memset(pVal->NetVal, ' ', PV_SIZ_EXE);
         iRet = 2;
      }
      else
         iRet = 1;

      // Output current record
      fputs(pOutbuf, fdOut);
   } else
   {
      LogMsg("*** Bad input record: %s", pInbuf);
      iRet = -1;
   }

   return iRet;
}

int Nev_QR_ParseValues(FILE *fdOut, LPSTR pInbuf, LPSTR pOutbuf, int iType, char cDel)
{
   VALUE_REC   *pVal = (VALUE_REC *)pOutbuf;
   int         iRet, lVal, lLand, lImpr, lOther;
   char        sTmp[32], *pTmp;

#ifdef _DEBUG
   //if (!memcmp(pInbuf, "075250001000", 9))
   //   iRet = 0;
#endif
   // Parse input rec
   iTokens = ParseStringNQ(pInbuf, cDel, 64, apTokens);

   if (iTokens >= NEV_QR_REASON && *apTokens[NEV_QR_REASON] > ' ')
   {
      memset(pOutbuf, ' ', sizeof(VALUE_REC));
      iRet = remChar(apTokens[NEV_QR_APN], '-');
      memcpy(pVal->Apn, apTokens[NEV_QR_APN], iRet);
      pTmp = strchr(apTokens[NEV_QR_TAXYEAR], ',');
      if (pTmp)
      {
         pTmp++;
         memcpy(pVal->TaxYear, pTmp, strlen(pTmp));
      }

      // We always set first record to 1
      pVal->ValueSetType[0] = '1';   
      pVal->Reason[0] = *apTokens[NEV_QR_REASON];
      dollar2Num(apTokens[NEV_QR_LANDTOTAL], sTmp);
      lLand = atol(sTmp);
      if (lLand > 0)
      {
         iRet = sprintf(sTmp, "%d", lLand);
         memcpy(pVal->Land, sTmp, iRet);
      }

      dollar2Num(apTokens[NEV_QR_IMPRTOTAL], sTmp);
      lImpr = atol(sTmp);
      if (lImpr > 0)
      {
         iRet = sprintf(sTmp, "%d", lImpr);
         memcpy(pVal->Impr, sTmp, iRet);
      }

      dollar2Num(apTokens[NEV_QR_FIXTVAL], sTmp);
      lOther = atol(sTmp);
      if (lOther > 0)
      {
         iRet = sprintf(sTmp, "%d", lOther);
         memcpy(pVal->Fixtr, sTmp, iRet);
      }

      dollar2Num(apTokens[NEV_QR_PPVAL], sTmp);
      lVal = atol(sTmp);
      if (lVal > 0)
      {
         iRet = sprintf(sTmp, "%d", lVal);
         memcpy(pVal->PersProp, sTmp, iRet);
         lOther += lVal;
      }

      if (lOther > 0)
      {
         iRet = sprintf(sTmp, "%d", lOther);
         memcpy(pVal->Other, sTmp, iRet);
      }

      // Gross
      //dollar2Num(apTokens[NEV_QR_GROSS], sTmp);
      //lVal = atol(sTmp);
      lVal = lLand+lImpr+lOther;
      if (lVal > 0)
      {
         iRet = sprintf(sTmp, "%d", lVal);
         memcpy(pVal->TotalVal, sTmp, iRet);
      }

      // Net
      dollar2Num(apTokens[NEV_QR_VALUETOTAL], sTmp);
      lVal = atol(sTmp);
      if (lVal > 0)
      {
         iRet = sprintf(sTmp, "%d", lVal);
         memcpy(pVal->NetVal, sTmp, iRet);
      }

      // Exemption
      dollar2Num(apTokens[NEV_QR_EXMPTNVAL], sTmp);
      lVal = atol(sTmp);
      if (lVal > 0)
      {
         iRet = sprintf(sTmp, "%d", lVal);
         memcpy(pVal->Exe, sTmp, iRet);
      }

      pVal->CRLF[0] = '\n';
      pVal->CRLF[1] = 0;
      if (iType == 2)
      {
         // Output current record
         fputs(pOutbuf, fdOut);

         // Create a base year value record
         pVal->ValueSetType[0] = '2';  // Base year value record
         pVal->Reason[0] = ' ';
         memset(pVal->Exe, ' ', PV_SIZ_EXE);
         memset(pVal->NetVal, ' ', PV_SIZ_EXE);
         iRet = 2;
      }
      else
         iRet = 1;

      // Output current record
      fputs(pOutbuf, fdOut);
   } else
   {
      LogMsg("*** Bad input record: %s", pInbuf);
      iRet = -1;
   }

   return iRet;
}

/******************************************************************************
 *
 * Return number of records output
 *
 ******************************************************************************/

int Nev_LoadValueFile(FILE *fdOut, char *pValueFile, int iType, char cDel)
{
   int   iRet, iCnt, iOut;
   char  sInbuf[1024], sOutbuf[1024];

   FILE        *fdIn;
   VALUE_REC   *pVal = (VALUE_REC *)&sOutbuf[0];

   LogMsg("Extract values from %s", pValueFile);

   // Open input
   LogMsg("Opening ... %s", pValueFile);
   fdIn = fopen(pValueFile, "r");
   if (!fdIn)
   {
      LogMsg("***** Error opening %s (%d)", pValueFile, _errno);
      return -1;
   }

   // Skip header
   fgets(sInbuf, 1024, fdIn);
   iOut = iCnt = 0;

   // Loop extract
   while (!feof(fdIn))
   {
      if (!fgets(sInbuf, 1024, fdIn))
         break;

      // Skip bad records
      if (sInbuf[0] != cDel)
      {
         if (iType == 1)
            iRet = Nev_ParseValues(fdOut, sInbuf, sOutbuf, iType, cDel);
         else
            iRet = Nev_QR_ParseValues(fdOut, sInbuf, sOutbuf, iType, cDel);
         if (iRet > 0)
            iOut += iRet;
      }

      if (!(++iCnt % 1000))
         printf("\r%u", iCnt);
   }

   fclose(fdIn);

   LogMsg("Number of records processed: %d", iCnt);
   LogMsg("Number of records output:    %d", iOut);

   return iOut;
}

/******************************* Nev_MergeValues *****************************
 *
 * Input files:
 *    - NVALNOTICE2014
 *    - QVALNOTICE2014
 *    - REGVALNOTICE2014
 * Process:
 *    - Merge the three files into single file in VALUE_REC format.
 *    - For each record in NVALNOTICE2014, create 2 records for type 2 & 8
 *    - Sort output file on APN and ValueSetType
 *    - Output to SNEV.Value
 * Notes:
 *    - Input files may have different delimiter
 *
 ****************************************************************************/

int Nev_MergeValues(char *pCnty, char *pOutfile)
{
   char  acNValFile[_MAX_PATH], acQValFile[_MAX_PATH], acOutFile[_MAX_PATH], acRValFile[_MAX_PATH];
   int   iRet, iOut=0;
   FILE  *fdOut;

   iRet = GetIniString(pCnty , "NValue", "", acNValFile, _MAX_PATH, acIniFile);
   iRet = GetIniString(pCnty , "QValue", "", acQValFile, _MAX_PATH, acIniFile);
   iRet = GetIniString(pCnty , "RValue", "", acRValFile, _MAX_PATH, acIniFile);
   if (_access(acQValFile, 0) || _access(acNValFile, 0) || _access(acNValFile, 0))
   {
      LogMsg("***** Missing input value files.  Please check INI file for correct path");
      return -1;
   }

   // Create output file
   sprintf(acOutFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "vmp");
   fdOut = fopen(acOutFile, "w");
   if (!fdOut)
   {
      LogMsg("***** Error creating output files %s.", acOutFile);
      return -2;
   }

   iRet = Nev_LoadValueFile(fdOut, acNValFile, 1, ';');
   if (iRet > 0)
   {
      iOut = iRet;
      iRet = Nev_LoadValueFile(fdOut, acQValFile, 2, ';');
      if (iRet > 0)
      {
         iOut += iRet;
         iRet = Nev_LoadValueFile(fdOut, acRValFile, 3, ';');
         if (iRet > 0)
         {
            iOut += iRet;
         }
      }
   }

   // CLose output file
   fclose(fdOut);

   if (iRet > 0)
   {
      // Sort on APN and ValueSetType
      iRet = sortFile(acOutFile, pOutfile, "S(1,12,C,A, 21,1,C,A) DUPO");
      if (iRet > 0)
      {
         LogMsg("Total record output: %d", iRet);
         // Remove temp file
         if (iRet == iOut)
         {
            LogMsg("Remove temp file: %s", acOutFile);
            DeleteFile(acOutFile);
         }
      } else
         iRet = -3;
   }

   return iRet;
}

/**************************** Nev_ExtrRollValues ****************************
 *
 * Desc: Extract current value from roll file
 *
 ****************************************************************************/

void Nev_ExtrRollValues(char *pInbuf, char *pOutbuf)
{
   int   iVal, iTmp;
   char  acTmp[256];

   VALUE_REC *pVal = (VALUE_REC *)pOutbuf;
   NEV_ROLL  *pRoll= (NEV_ROLL  *)pInbuf;

   memset(pOutbuf, ' ', sizeof(VALUE_REC));
   memcpy(pVal->Apn, pRoll->Apn, iApnLen);

   // Land
   long lLand = atoin(pRoll->Land, RSIZ_NEV_LAND);
   if (lLand > 0)
   {
      iTmp = sprintf(acTmp, "%d", lLand);
      memcpy(pVal->Land, acTmp, iTmp);
   }

   // Improve
   long lImpr = atoin(pRoll->Improv, RSIZ_NEV_IMPROV);
   if (lImpr > 0)
   {
      iTmp = sprintf(acTmp, "%d", lImpr);
      memcpy(pVal->Impr , acTmp, iTmp);
   }

   // PP value
   long lPers = atoin(pRoll->Business_Pers_Prop, RSIZ_NEV_BUSINESS_PERS_PROP);
   // Fixture or ME val
   long lFixture = atoin(pRoll->Trade_Fixt, RSIZ_NEV_TRADE_FIXT);
   long lOther = lFixture+lPers;
   if (lOther > 0)
   {
      iTmp = sprintf(acTmp, "%d", lOther);
      memcpy(pVal->Other, acTmp, iTmp);

      if (lFixture > 0)
      {
         iTmp = sprintf(acTmp, "%d",  lFixture);
         memcpy(pVal->Fixtr, acTmp, iTmp);
      }
      if (lPers > 0)
      {
         iTmp = sprintf(acTmp, "%d",  lPers);
         memcpy(pVal->PersProp, acTmp, iTmp);
      }
   }

   // Gross total
   long lGross = lLand+lImpr+lOther;
   if (lGross > 0)
   {
      iTmp = sprintf(acTmp, "%d", lGross);
      memcpy(pVal->TotalVal, acTmp, iTmp);
   }

   // Exempt
   iVal = atoin(pRoll->Exemption_Value, RSIZ_NEV_EXEMPTION_VALUE);
   if (iVal > 0)
   {
      iTmp = sprintf(acTmp, "%d", iVal);
      memcpy(pVal->Exe, acTmp, iTmp);
   }

   pVal->ValueSetType[0] = '1';
   pVal->CRLF[0] = '\n';
   pVal->CRLF[1] = 0;
}

/**************************** Mev_MergeCurValues ****************************
 *
 * Desc: Merge value extract with current roll to get current value
 *
 * Input files:
 *    - SNEV.ValEx
 *    - ASPUBROLL     
 * Output file:
 *    - SNEV.Value
 *
 ****************************************************************************

int Nev_ExtrCurValues(char *pRollFile, char *pOutFile)
{
   char  sInbuf[1024], sOutbuf[1024];
   int   iCnt=0;
   FILE  *fdIn, *fdOut;

   // Open roll file
   LogMsg("Open Roll file %s", pRollFile);
   fdIn = fopen(pRollFile, "r");
   if (fdIn == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", pRollFile);
      return -2;
   }

   // Create output file
   LogMsg("Open output file %s", pOutFile);
   if (!(fdOut = fopen(pOutFile, "w")))
   {
      LogMsg("***** Error creating output file %s", pOutFile);
      return -2;
   }

   while (!feof(fdIn))
   {
       if (!fgets(sInbuf, 1024, fdIn))
         break;

      Nev_ExtrRollValues(sInbuf, sOutbuf);

      fputs(sOutbuf, fdOut);

      if (!(++iCnt % 1000))
         printf("\r%u", iCnt);
   }

   fclose(fdIn);

   LogMsg("Number of records output: %d", iCnt);

   return iCnt;
}

/****************************** Nev_MergeTaxDelq *****************************
 *
 * Copy DelqYear from delq record to populate Tax_Base
 *
 *****************************************************************************/

int Nev_MergeTaxDelq(char *pOutbuf)
{
   static   char  acRec[2048], *apItems[MAX_FLD_TOKEN], *pRec=NULL;
   int      iLoop, iTmp;
   TAXBASE  *pTaxBase = (TAXBASE *)pOutbuf;

   // Get first Sale rec for first call
   if (!pRec)
      pRec = fgets(acRec, 2048, fdDelq);

   do
   {
      // Compare Apn
      iLoop = memcmp(pTaxBase->Apn, pRec+1, iApnLen);
      if (iLoop > 0)
      {
         if (bDebug)
            LogMsg0("Skip delq rec  %.*s", iApnLen, pRec+1);
         pRec = fgets(acRec, 2048, fdDelq);
         if (!pRec)
         {
            fclose(fdDelq);
            fdDelq = NULL;
            return -1;      // EOF
         }

         lSaleSkip++;
      }
   } while (iLoop > 0);

   // If not match, return
   if (iLoop)
      return 1;

   do 
   {
      // Parse input tax data
      iTmp = ParseStringNQ(acRec, ',', MAX_FLD_TOKEN, apItems);
      if (iTmp < NCDELQ_OTHER_DESC)
      {
         LogMsg("***** Error: bad Tax record for: %.50s (#tokens=%d)", acRec, iTmp);
         return -1;
      }

      // Update TaxBase
      //strcpy(pTaxBase->DelqYear, apItems[NCDELQ_TAXYEAR]);
      iTmp = atol(apTokens[NCDELQ_TAXYEAR]);
      sprintf(pTaxBase->Def_Date, "%d", iTmp+1);
     
      // Get next rec
      pRec = fgets(acRec, 2048, fdDelq);
   } while (!memcmp(pTaxBase->Apn, pRec+1, iApnLen));

   //// Default date
   //if (dateConversion(apItems[MEN_DELQ_TAX_DEF_DATE], acTmp, MMDDYYYY))
   //{
   //   strcpy(pTaxBase->Def_Date, acTmp);
   //   memcpy(pTaxBase->DelqYear, acTmp, 4);

   //   if (*apItems[MEN_DELQ_RDM_STAT] != 'P')
   //      pTaxBase->isDelq[0] = '1';
   //}

   lSaleMatch++;

   return 0;
}

/******************************* Nev_GetTaxAmt *******************************
 *
 * Parse array of tax amt and add them up
 *
 *****************************************************************************/

double Nev_GetTaxAmt(char *pAmt)
{
   char  *apItems[32];
   double dRet = 0.0;
   int   iCnt;

   iCnt = ParseString(pAmt, '|', 32, apItems);
   while (iCnt > 0)
   {
      iCnt--;
      dRet += dollar2Double(apItems[iCnt]);
   }

   return dRet;
}

/****************************** Nev_GetPaidAmt *******************************
 *
 * Parse array of tax amt and set Amt1 & Amt2
 *
 * Return number of items.  If Codes and Amts are not compatible, return 0.
 *
 *****************************************************************************/

int Nev_GetPaidAmt(char *pCode, char *pAmt, double *pdAmt1, double *pdAmt2)
{
   char  *apCodes[32], *apAmts[32];
   double dTmp, dAmt1, dAmt2;
   int    iCodeCnt, iAmtCnt, iIdx;

   *pdAmt1 = 0.0;
   *pdAmt2 = 0.0;
   iCodeCnt = ParseString(pCode, '|', 32, apCodes);
   iAmtCnt = ParseString(pAmt, '|', 32, apAmts);
   if (iAmtCnt != iCodeCnt)
      return 0;

   dTmp=dAmt1=dAmt2 = 0.0;
   for (iIdx = 0; iIdx < iAmtCnt; iIdx++)
   {
      if (*apCodes[iIdx] == '1')
         dAmt1 += dollar2Double(apAmts[iIdx]);
      else if (*apCodes[iIdx] == '2')
         dAmt2 += dollar2Double(apAmts[iIdx]);
      else
         dTmp += dollar2Double(apAmts[iIdx]);
   }

   if (dTmp > 0.0)
      dAmt1 = dAmt2 = dTmp/2.0;

   *pdAmt1 = dAmt1;
   *pdAmt2 = dAmt2;

   return iAmtCnt;
}

/***************************** Nev_ParseTaxDetail ****************************
 *
 * Return 0 if success
 *
 *****************************************************************************/

//int Nev_ParseTaxDetail1(FILE *fdDetail, FILE *fdAgency, char *pBaseRec)
//{
//   char     acTmp[256], asTmpVal[32][32];
//   char     acCodes1[256], acValues1[256], *apCodes1[32], *apValues1[32];
//   char     acCodes2[256], acValues2[256], *apCodes2[32], *apValues2[32];
//   int      iCode, iIdx, iCnt1, iCnt2, iTmp;
//   double   dTmp, dPen1, dPen2, dCost;
//   TAXDETAIL *pDetail, sDetail;
//   TAXAGENCY *pResult, sAgency, *pAgency;
//   TAXBASE   *pBase = (TAXBASE *)pBaseRec;
//
//   // Clear output buffer
//   memset(&sDetail, 0, sizeof(TAXDETAIL));
//   memset(&sAgency, 0, sizeof(TAXAGENCY));
//   pAgency = &sAgency;
//   pDetail = &sDetail;
//   
//   // APN
//   strcpy(pDetail->Apn, apTokens[NCSEC_APN]);
//
//   // BillNumber
//   strcpy(pDetail->BillNum, apTokens[NCSEC_ASSMT]);
//
//   // Tax Year
//   lTaxYear = atoin(apTokens[NCSEC_ROLL_YR], 2) + 2000;
//   sprintf(pDetail->TaxYear, "%d", lTaxYear);
//
//   dPen1=dPen2=dCost = 0.0;
//   if (*apTokens[NCSEC_1CHG_AMT] > '0')
//   {
//      strcpy(acValues1, apTokens[NCSEC_1CHG_AMT]);
//      strcpy(acCodes1, apTokens[NCSEC_1CHG_CODE]);
//      iCnt1 = ParseString(acValues1, '|', 32, apValues1);
//      iTmp = ParseString(acCodes1, '|', 32, apCodes1);
//      if (iCnt1 != iTmp)
//      {
//         LogMsg("*** Bad 1st inst values.  Please check %.12s", apTokens[NCSEC_APN]);
//         return -1;
//      }
//
//      strcpy(acValues2, apTokens[NCSEC_2CHG_AMT]);
//      strcpy(acCodes2, apTokens[NCSEC_2CHG_CODE]);
//      iCnt2 = ParseString(acValues2, '|', 32, apValues2);
//      iTmp = ParseString(acCodes2, '|', 32, apCodes2);
//      if (iCnt2 != iTmp)
//      {
//         LogMsg("*** Bad 2nd inst values.  Please check %.12s", apTokens[NCSEC_APN]);
//         return -1;
//      }
//
//#ifdef _DEBUG
//   if (!memcmp(pDetail->Apn,"0122019000", 10))
//      iTmp=0;
//#endif
//
//      // Balance group 1 & 2
//      if (iCnt2 > iCnt1)
//      {
//         // We need to remove items from group 2
//         for (iIdx = 0; iIdx < iCnt1; iIdx++)
//         {
//            if (strcmp(apValues1[iIdx], apValues2[iIdx]))
//            {
//               remChar(apValues2[iIdx], ',');
//               iCode = atol(apCodes2[iIdx]);
//               if (iCode == 4 || iCode == 5)         // Penalty
//                  dPen2 += atof(apValues2[iIdx]);
//               else if (iCode == 30)                // Cost
//                  dCost += atof(apValues2[iIdx]);
//               else if (iCode == 34)                // Total Penalty
//                  dPen2 = atof(apValues2[iIdx]);
//               else if (iCode == 169)               // Sale letter Penalty
//                  dPen2 += atof(apValues2[iIdx]);
//               else if (iCode > 17 && iCode < 21)    // Interest Penalty
//                  dPen2 += atof(apValues2[iIdx]);
//               else if (iCode > 715 && iCode < 719)  // UNPAID FINES/ABATEMENT COSTS
//                  dCost += atof(apValues2[iIdx]);
//               else if (iCode > 50 && iCode < 54)    // REDMP FEE
//                  dCost += atof(apValues2[iIdx]);
//               else
//                  iTmp = 0;
//
//               // Remove current value and move others up
//               iCnt2--;
//               for (iTmp = iIdx; iTmp < iCnt2; iTmp++)
//               {
//                  strcpy(apCodes2[iTmp], apCodes2[iTmp+1]);
//                  strcpy(&asTmpVal[iTmp][0], apValues2[iTmp+1]);
//                  apValues2[iTmp] = &asTmpVal[iTmp][0];
//               }
//            }
//
//            //if (iCnt1 >= iCnt2)
//            if (iCnt1 > iCnt2)
//               break;
//         }
//      }
//      
//      if (iCnt1 > iCnt2)
//      {
//         // We need to remove items from group 1
//         for (iIdx = 0; iIdx < iCnt2; iIdx++)
//         {
//            if (strcmp(apValues1[iIdx], apValues2[iIdx]))
//            {
//               remChar(apValues1[iIdx], ',');
//               iCode = atol(apCodes1[iIdx]);
//               if (iCode == 4 || iCode == 5)         // Penalty
//                  dPen1 += atof(apValues1[iIdx]);
//               else if (iCode == 30)                // Cost
//                  dCost += atof(apValues1[iIdx]);
//               else if (iCode == 34)                // Total Penalty
//                  dPen1 = atof(apValues1[iIdx]);
//               else if (iCode == 84 || iCode == 44 || iCode == 224 || iCode == 234 || iCode == 388 || iCode == 728) 
//               {
//                  strcpy(pDetail->TaxAmt, apValues1[iIdx]);
//                  strcpy(pDetail->TaxCode, apCodes1[iIdx]);
//                  strcpy(pAgency->Code, apCodes1[iIdx]);
//                  pResult = findTaxAgency(apCodes1[iIdx]);
//                  if (pResult)
//                  {
//                     strcpy(pAgency->Agency, pResult->Agency);
//                     strcpy(pAgency->Phone, pResult->Phone);
//                  }
//
//                  // Generate csv line and write to file
//                  Tax_CreateDetailCsv(acTmp, pDetail);
//                  fputs(acTmp, fdDetail);
//
//                  // Generate Agency record
//                  Tax_CreateAgencyCsv(acTmp, pAgency);
//                  fputs(acTmp, fdAgency);
//               } else if (iCode == 169)               // Sale letter Penalty
//                  dPen1 += atof(apValues1[iIdx]);
//               else if (iCode > 17 && iCode < 21)    // Interest Penalty
//                  dPen1 += atof(apValues1[iIdx]);
//               else if (iCode > 715 && iCode < 719)  // UNPAID FINES/ABATEMENT COSTS
//                  dCost += atof(apValues1[iIdx]);
//               else if (iCode > 50 && iCode < 54)    // REDMP FEE
//                  dCost += atof(apValues1[iIdx]);
//               else
//                  iTmp = 0;
//
//               // Remove current value and move others up
//               iCnt1--;
//               for (iTmp = iIdx; iTmp < iCnt1; iTmp++)
//               {
//                  strcpy(apCodes1[iTmp], apCodes1[iTmp+1]);
//                  strcpy(&asTmpVal[iTmp][0], apValues1[iTmp+1]);
//                  apValues1[iTmp] = &asTmpVal[iTmp][0];
//               }
//            }
//
//            if (iCnt2 > iCnt1)
//               break;
//         }
//      } else if (iCnt1 == iCnt2)
//      {
//         for (iIdx = 0; iIdx < iCnt1; iIdx++)
//         {
//            remChar(apValues1[iIdx], ',');
//            dTmp = atof(apValues1[iIdx]);
//            iCode = atol(apCodes1[iIdx]);
//            if (iCode == 4 || iCode == 5)         // Penalty
//            {
//               dPen1 += dTmp;
//               remChar(apValues2[iIdx], ',');
//               dPen2 += atof(apValues2[iIdx]);
//            } else if (iCode == 30)                // Cost
//               dCost += dTmp;
//            else if (iCode == 34)                // Total Penalty
//               dPen1 = dTmp;
//            else if (iCode == 169)               // Sale letter Penalty
//               dPen1 += dTmp;
//            else if (iCode > 17 && iCode < 21)    // Interest Penalty
//               dPen1 += dTmp;
//            else if (iCode > 715 && iCode < 719)  // UNPAID FINES/ABATEMENT COSTS
//               dCost += dTmp;
//            else if (iCode > 50 && iCode < 54)    // REDMP FEE
//               dCost += dTmp;
//            else
//            {
//               // Tax amt
//               sprintf(pDetail->TaxAmt, "%.2f", dTmp*2.0);
//
//               // Agency 
//               pResult = findTaxAgency(apCodes1[iIdx]);
//               if (pResult)
//               {
//                  strcpy(pDetail->TaxCode, pResult->CodeXL);
//                  strcpy(pAgency->Code, pResult->CodeXL);
//                  strcpy(pAgency->Agency, pResult->Agency);
//                  strcpy(pAgency->Phone, pResult->Phone);
//               } else
//               {
//                  strcpy(pDetail->TaxCode, apCodes1[iIdx]);
//                  strcpy(pAgency->Code, apCodes1[iIdx]);
//                  pAgency->Agency[0] = 0;
//                  LogMsg("+++ Unknown TaxCode: %s", pAgency->Code);
//               }
//
//               // Generate csv line and write to file
//               Tax_CreateDetailCsv(acTmp, pDetail);
//               fputs(acTmp, fdDetail);
//
//               // Generate Agency record
//               Tax_CreateAgencyCsv(acTmp, pAgency);
//               fputs(acTmp, fdAgency);
//            }
//         }
//      } else
//      {
//         // Group1 Only - something wrong?
//         iTmp = 0;
//      }
//   }
//
//   //if (*apTokens[NCSEC_2CHG_AMT] > ' ')
//   //{
//   //   strcpy(acValues2, apTokens[NCSEC_2CHG_AMT]);
//   //   strcpy(acCodes2, apTokens[NCSEC_2CHG_CODE]);
//   //   iCnt = ParseString(acValues2, '|', 32, apValues2);
//   //   iTmp = ParseString(acCodes2, '|', 32, apCodes2);
//   //   if (iCnt != iTmp)
//   //   {
//   //      LogMsg("*** Bad 2nd inst values.  Please check %.12s", apTokens[NCSEC_APN]);
//   //      return -1;
//   //   }
//
//   //   dPen1=dPen2=dCost = 0.0;
//   //   for (iIdx = 0; iIdx < iCnt; iIdx++)
//   //   {
//   //      remChar(apValues2[iIdx], ',');
//   //      iTmp = atol(apCodes2[iIdx]);
//   //      if (iTmp == 4 || iTmp == 5)         // Penalty
//   //         dPen1 += atof(apValues2[iIdx]);
//   //      else if (iTmp == 30)                // Cost
//   //         dCost += atof(apValues2[iIdx]);
//   //      else if (iTmp == 34)                // Total Penalty
//   //         dPen1 = atof(apValues2[iIdx]);
//   //      else if (iTmp == 169)               // Sale letter Penalty
//   //         dPen1 += atof(apValues2[iIdx]);
//   //      else if (iTmp > 17 && iTmp < 21)    // Interest Penalty
//   //         dPen1 += atof(apValues2[iIdx]);
//   //      else if (iTmp > 715 && iTmp < 719)  // UNPAID FINES/ABATEMENT COSTS
//   //         dCost += atof(apValues2[iIdx]);
//   //      else if (iTmp > 50 && iTmp < 54)    // REDMP FEE
//   //         dCost += atof(apValues2[iIdx]);
//   //      else
//   //      {
//   //         // Tax amt
//   //         strcpy(pDetail->TaxAmt, apValues2[iIdx]);
//
//   //         // Agency 
//   //         strcpy(pDetail->TaxCode, apCodes2[iIdx]);
//   //         strcpy(pAgency->Code, apCodes2[iIdx]);
//   //         pResult = findTaxAgency(pAgency->Code);
//   //         if (pResult)
//   //         {
//   //            strcpy(pAgency->Agency, pResult->Agency);
//   //            strcpy(pAgency->Phone, pResult->Phone);
//   //         } else
//   //         {
//   //            pAgency->Agency[0] = 0;
//   //            LogMsg("+++ Unknown TaxCode: %s", pAgency->Code);
//   //         }
//
//   //         // Generate csv line and write to file
//   //         Tax_CreateDetailCsv(acTmp, pDetail);
//   //         fputs(acTmp, fdDetail);
//
//   //         // Generate Agency record
//   //         Tax_CreateAgencyCsv(acTmp, pAgency);
//   //         fputs(acTmp, fdAgency);
//   //      }
//   //   }
//   //}
//
//   if (dCost > 0.0)
//      sprintf(pBase->TotalFees, "%.2f", dCost);
//   if (dPen1 > 0.0)
//      sprintf(pBase->PenAmt1, "%.2f", dPen1);
//   if (dPen2 > 0.0)
//      sprintf(pBase->PenAmt2, "%.2f", dPen2);
//
//   return 0;
//}

int Nev_ParseTaxDetail_x(FILE *fdDetail, FILE *fdAgency, char *pBaseRec)
{
   char     acTmp[256];
   char     acCodes1[256], acValues1[256], *apCodes1[32], *apValues1[32];
   char     acCodes2[256], acValues2[256], *apCodes2[32], *apValues2[32];
   int      iIdx, iCnt, iTmp;
   double   dPen1, dPen2, dCost, dTax1, dTax2, dTotalDue;
   TAXDETAIL *pDetail, sDetail;
   TAXAGENCY *pResult, sAgency, *pAgency;
   TAXBASE   *pBase = (TAXBASE *)pBaseRec;

   // Clear output buffer
   memset(&sDetail, 0, sizeof(TAXDETAIL));
   memset(&sAgency, 0, sizeof(TAXAGENCY));
   pAgency = &sAgency;
   pDetail = &sDetail;
   
   // APN
   strcpy(pDetail->Apn, apTokens[NCSEC_APN]);

   // BillNumber
   strcpy(pDetail->BillNum, apTokens[NCSEC_ASSMT]);

   // Tax Year
   iTmp = atoin(apTokens[NCSEC_ROLL_YR], 2) + 2000;
   sprintf(pDetail->TaxYear, "%d", iTmp);

   dTotalDue=dPen1=dPen2=dCost = 0.0;
   if (*apTokens[NCSEC_1CHG_AMT] > '0')
   {
      strcpy(acValues1, apTokens[NCSEC_1CHG_AMT]);
      strcpy(acCodes1, apTokens[NCSEC_1CHG_CODE]);
      iCnt = ParseString(acValues1, '|', 32, apValues1);
      iTmp = ParseString(acCodes1, '|', 32, apCodes1);
      if (iCnt != iTmp)
      {
         LogMsg("*** Bad 1st inst values.  Please check %.12s", apTokens[NCSEC_APN]);
         return -1;
      }

#ifdef _DEBUG
   if (!memcmp(pDetail->Apn,"0117038000", 10))
      iTmp=0;
#endif

      for (iIdx = 0; iIdx < iCnt; iIdx++)
      {
         remChar(apValues1[iIdx], ',');
         iTmp = atol(apCodes1[iIdx]);
         if (iTmp == 4)                      // Penalty
            dPen1 += atof(apValues1[iIdx]);
         else if (iTmp == 30)                // Cost
            dCost += atof(apValues1[iIdx]);
         else if (iTmp == 34)                // Total Penalty
            dPen1 = atof(apValues1[iIdx]);
         else if (iTmp == 169)               // Sale letter Penalty
            dPen1 += atof(apValues1[iIdx]);
         else if (iTmp > 17 && iTmp < 21)    // Interest Penalty
            dPen1 += atof(apValues1[iIdx]);
         else if (iTmp > 715 && iTmp < 719)  // UNPAID FINES/ABATEMENT COSTS
            dCost += atof(apValues1[iIdx]);
         else if (iTmp > 50 && iTmp < 54)    // REDMP FEE
            dCost += atof(apValues1[iIdx]);
         else
         {
            // Tax amt
            strcpy(pDetail->TaxAmt, apValues1[iIdx]);

            // Agency 
            strcpy(pDetail->TaxCode, apCodes1[iIdx]);
            strcpy(pAgency->Code, apCodes1[iIdx]);
            pResult = findTaxAgency(pAgency->Code);
            if (pResult)
            {
               strcpy(pAgency->Agency, pResult->Agency);
               strcpy(pAgency->Phone, pResult->Phone);
               pAgency->TC_Flag[0] = pResult->TC_Flag[0];
               pDetail->TC_Flag[0] = pResult->TC_Flag[0];
            } else
            {
               pAgency->TC_Flag[0] = '0';
               pDetail->TC_Flag[0] = '0';
               pAgency->Agency[0] = 0;
               LogMsg("+++ Unknown TaxCode: %s", pAgency->Code);
            }

            // Generate csv line and write to file
            Tax_CreateDetailCsv(acTmp, pDetail);
            fputs(acTmp, fdDetail);

            // Generate Agency record
            Tax_CreateAgencyCsv(acTmp, pAgency);
            fputs(acTmp, fdAgency);
         }
      }
   }

   if (*apTokens[NCSEC_2CHG_AMT] > ' ')
   {
      strcpy(acValues2, apTokens[NCSEC_2CHG_AMT]);
      strcpy(acCodes2, apTokens[NCSEC_2CHG_CODE]);
      iCnt = ParseString(acValues2, '|', 32, apValues2);
      iTmp = ParseString(acCodes2, '|', 32, apCodes2);
      if (iCnt != iTmp)
      {
         LogMsg("*** Bad 2nd inst values.  Please check %.12s", apTokens[NCSEC_APN]);
         return -1;
      }

      for (iIdx = 0; iIdx < iCnt; iIdx++)
      {
         remChar(apValues2[iIdx], ',');
         iTmp = atol(apCodes2[iIdx]);
         if (iTmp == 5)                      // Penalty
            dPen2 += atof(apValues2[iIdx]);
         else if (iTmp == 30)                // Cost
            dCost += atof(apValues2[iIdx]);
         else if (iTmp == 34)                // Total Penalty
            dPen2 = atof(apValues2[iIdx]);
         else if (iTmp == 169)               // Sale letter Penalty
            dPen2 += atof(apValues2[iIdx]);
         else if (iTmp > 17 && iTmp < 21)    // Interest Penalty
            dPen2 += atof(apValues2[iIdx]);
         else if (iTmp > 715 && iTmp < 719)  // UNPAID FINES/ABATEMENT COSTS
            dCost += atof(apValues2[iIdx]);
         else
         {
            // Tax amt
            strcpy(pDetail->TaxAmt, apValues2[iIdx]);

            // Agency 
            strcpy(pDetail->TaxCode, apCodes2[iIdx]);
            strcpy(pAgency->Code, apCodes2[iIdx]);
            pResult = findTaxAgency(pAgency->Code);
            if (pResult)
            {
               strcpy(pAgency->Agency, pResult->Agency);
               strcpy(pAgency->Phone, pResult->Phone);
               pAgency->TC_Flag[0] = pResult->TC_Flag[0];
               pDetail->TC_Flag[0] = pResult->TC_Flag[0];
            } else
            {
               pAgency->TC_Flag[0] = '0';
               pDetail->TC_Flag[0] = '0';
               pAgency->Agency[0] = 0;
               LogMsg("+++ Unknown TaxCode: %s", pAgency->Code);
            }

            // Generate csv line and write to file
            Tax_CreateDetailCsv(acTmp, pDetail);
            fputs(acTmp, fdDetail);

            // Generate Agency record
            Tax_CreateAgencyCsv(acTmp, pAgency);
            fputs(acTmp, fdAgency);
         }
      }
   }

   // Check other cost
   if (*apTokens[NCSEC_OTHER_COSTS] > '0')
      dCost += Nev_GetTaxAmt(apTokens[NCSEC_OTHER_COSTS]);
   if (dCost > 0.0)
      sprintf(pBase->TotalFees, "%.2f", dCost);

#ifdef _DEBUG
   //if (!memcmp(pDetail->Apn,"0117038000", 10))
   //   iTmp=0;
#endif

   // Penalty
   if (dPen1 > 0.0)
      sprintf(pBase->PenAmt1, "%.2f", dPen1);
   if (dPen2 > 0.0)
      sprintf(pBase->PenAmt2, "%.2f", dPen2);

   dTax1 = atof(pBase->TaxAmt1);
   dTax2 = atof(pBase->TaxAmt2);
   // Cast to float to avoid floating point error
   if ((float)dPen1 == (float)(dTax1-dTax2))
   {
      // TaxAmt1 already includes penalty, we have to take it out
      sprintf(pBase->TaxAmt1, "%.2f", dTax2);
      sprintf(pBase->TotalTaxAmt, "%.2f", dTax2*2);
      dTax1 = dTax2;
   }

   if (pBase->PaidAmt1[0] < '1')
      dTotalDue = dTax1+dPen1;
   if (pBase->PaidAmt2[0] < '1')
      dTotalDue += (dTax2+dPen2);

   if (dTotalDue > 0.0)
      sprintf(pBase->TotalDue, "%.2f", dTotalDue);

   return 0;
}

int Nev_ParseTaxDetail(FILE *fdDetail, FILE *fdAgency, char *pBaseRec)
{
   char     acTmp[256];
   char     acCodes1[256], acValues1[256], *apCodes1[32], *apValues1[32];
   char     acCodes2[256], acValues2[256], *apCodes2[32], *apValues2[32];
   int      iIdx, iIdx1, iIdx2, iCnt, iCnt1, iCnt2, iTmp, iCode1, iCode2;
   double   dOthCost, dOthPen, dPen1, dPen2, dCost1, dCost2, dTax1, dTax2, dTotalDue, dVal1, dVal2, dFee1, dFee2;
   TAXDETAIL *pDetail, sDetail;
   TAXAGENCY *pResult, sAgency, *pAgency;
   TAXBASE   *pBase = (TAXBASE *)pBaseRec;

   // Clear output buffer
   memset(&sDetail, 0, sizeof(TAXDETAIL));
   memset(&sAgency, 0, sizeof(TAXAGENCY));
   pAgency = &sAgency;
   pDetail = &sDetail;
   
   // APN
   strcpy(pDetail->Apn, apTokens[NCSEC_APN]);

   // BillNumber
   strcpy(pDetail->BillNum, apTokens[NCSEC_ASSMT]);

   // Tax Year
   iTmp = atoin(apTokens[NCSEC_ROLL_YR], 2) + 2000;
   sprintf(pDetail->TaxYear, "%d", iTmp);

   dOthCost=dOthPen=dFee1=dFee2=dTotalDue=dPen1=dPen2=dCost1=dCost2 = 0.0;
   if (*apTokens[NCSEC_1CHG_AMT] > '0')
   {
      strcpy(acValues1, apTokens[NCSEC_1CHG_AMT]);
      strcpy(acCodes1, apTokens[NCSEC_1CHG_CODE]);
      iCnt1 = ParseString(acValues1, '|', 32, apValues1);
      iTmp = ParseString(acCodes1, '|', 32, apCodes1);
      if (iCnt1 != iTmp)
      {
         LogMsg("*** Bad 1st inst values.  Please check %.12s", apTokens[NCSEC_APN]);
         return -1;
      }
   } else
   {
      iCnt1 = 0;
      acValues1[0] = 0;
   }
   if (*apTokens[NCSEC_2CHG_AMT] > ' ')
   {
      strcpy(acValues2, apTokens[NCSEC_2CHG_AMT]);
      strcpy(acCodes2, apTokens[NCSEC_2CHG_CODE]);
      iCnt2 = ParseString(acValues2, '|', 32, apValues2);
      iTmp = ParseString(acCodes2, '|', 32, apCodes2);
      if (iCnt2 != iTmp)
      {
         LogMsg("*** Bad 2nd inst values.  Please check %.12s", apTokens[NCSEC_APN]);
         return -1;
      }
   } else
   {
      iCnt2 = 0;
      acValues2[0] = 0;
   }

   // Pick the larger Cnt
   iCnt = (iCnt2 > iCnt1)?iCnt2:iCnt1;

#ifdef _DEBUG
   //if (!memcmp(pDetail->Apn,"0117038000", 10))
   //   iTmp=0;
#endif
   for (iIdx=iIdx1=iIdx2 = 0; iIdx < iCnt; iIdx++)
   {
      if (iIdx1 < iCnt1)
      {
         remChar(apValues1[iIdx1], ',');
         iCode1 = atol(apCodes1[iIdx1]);
         dVal1 = atof(apValues1[iIdx1]);
      } else
      {
         iCode1 = 0;
         dVal1 = 0;
      }
      if (iIdx2 < iCnt2)
      {
         remChar(apValues2[iIdx2], ',');
         iCode2 = atol(apCodes2[iIdx2]);
         dVal2 = atof(apValues2[iIdx2]);
      } else
      {
         iCode2 = 0;
         dVal2 = 0;
      }

      // Combine value1 & 2
      if (iCode1 == (iCode2-1) && dVal1 == dVal2)
      {
         if (iCode1 == 4)              // Penalty
         {
            dPen1 += dVal1;
            dPen2 += dVal2;
         } else if (iCode1 == 18)      // Interest Penalty
         {
            dPen1 += dVal1;
            dPen2 += dVal2;
         } else if (iCode1 == 51)      // REDMP FEE
         {
            dFee1 += dVal1;
            dFee2 += dVal2;
         } else
         {
            // Tax amt
            sprintf(pDetail->TaxAmt, "%.2f", dVal1*2);

            // Agency 
            strcpy(pDetail->TaxCode, apCodes1[iIdx1]);
            strcpy(pAgency->Code, apCodes1[iIdx1]);
            pResult = findTaxAgency(pAgency->Code);
            if (pResult)
            {
               strcpy(pAgency->Agency, pResult->Agency);
               strcpy(pAgency->Phone, pResult->Phone);
               pAgency->TC_Flag[0] = pResult->TC_Flag[0];
               pDetail->TC_Flag[0] = pResult->TC_Flag[0];
            } else
            {
               pAgency->TC_Flag[0] = '0';
               pDetail->TC_Flag[0] = '0';
               pAgency->Agency[0] = 0;
               LogMsg("+++ Unknown TaxCode: %s", pAgency->Code);
            }

            // Generate csv line and write to file
            Tax_CreateDetailCsv(acTmp, pDetail);
            fputs(acTmp, fdDetail);

            // Generate Agency record
            Tax_CreateAgencyCsv(acTmp, pAgency);
            fputs(acTmp, fdAgency);
         }
         iIdx1++;
         iIdx2++;
      } else if (iCode1 < iCode2 && dVal1 > 0.0)
      {
         if (iCode1 == 4)           // Penalty
         {
            dPen1 += dVal1;
         } else if (iCode1 == 30)   // Cost
         {
            dCost1 += dVal1;
         } else if (iCode1 == 34)   // Total Penalty
         {
            dPen1 += dVal1;
         } else if (iCode1 == 18)   // Interest Penalty
         {
            dOthPen += dVal1;
         } else if (iCode1 == 51)   // REDMP FEE
         {
            dFee1 += dVal1;
         } else
         {
            // Tax amt
            sprintf(pDetail->TaxAmt, "%.2f", dVal1);

            // Agency 
            strcpy(pDetail->TaxCode, apCodes1[iIdx1]);
            strcpy(pAgency->Code, apCodes1[iIdx1]);
            pResult = findTaxAgency(pAgency->Code);
            if (pResult)
            {
               strcpy(pAgency->Agency, pResult->Agency);
               strcpy(pAgency->Phone, pResult->Phone);
               pAgency->TC_Flag[0] = pResult->TC_Flag[0];
               pDetail->TC_Flag[0] = pResult->TC_Flag[0];
            } else
            {
               pAgency->TC_Flag[0] = '0';
               pDetail->TC_Flag[0] = '0';
               pAgency->Agency[0] = 0;
               LogMsg("+++ Unknown TaxCode1: %s", pAgency->Code);
               iNewAgency++;
            }

            // Generate csv line and write to file
            Tax_CreateDetailCsv(acTmp, pDetail);
            fputs(acTmp, fdDetail);

            // Generate Agency record
            Tax_CreateAgencyCsv(acTmp, pAgency);
            fputs(acTmp, fdAgency);
         }
         iIdx1++;
      } else if (dVal2 > 0.0)
      {
         if (iCode2 == 5)              // Penalty
         {
            dPen2 += dVal2;
         } else if (iCode2 == 30)      // Cost
         {
            dCost2 += dVal2;
         } else if (iCode2 == 34)      // Total Penalty
         {
            dOthPen += dVal2;
         } else if (iCode2 == 19)      // Interest Penalty
         {
            dOthPen += dVal2;
         } else if (iCode2 == 52)      // REDMP FEE
         {
            dFee2 += dVal2;
         } else
         {
            // Tax amt
            sprintf(pDetail->TaxAmt, "%.2f", dVal2);

            // Agency 
            pResult = findTaxAgency(apCodes2[iIdx2]);
            if (pResult)
            {
               strcpy(pDetail->TaxCode, pResult->CodeXL);
               strcpy(pAgency->Code, pResult->CodeXL);
               strcpy(pAgency->Agency, pResult->Agency);
               strcpy(pAgency->Phone, pResult->Phone);
               pAgency->TC_Flag[0] = pResult->TC_Flag[0];
               pDetail->TC_Flag[0] = pResult->TC_Flag[0];
            } else
            {
               pAgency->TC_Flag[0] = '0';
               pDetail->TC_Flag[0] = '0';
               pAgency->Agency[0] = 0;
               strcpy(pAgency->Code, apCodes2[iIdx2]);
               strcpy(pDetail->TaxCode, apCodes2[iIdx2]);
               LogMsg("+++ Unknown TaxCode2: %s", pAgency->Code);
               iNewAgency++;
            }

            // Generate csv line and write to file
            Tax_CreateDetailCsv(acTmp, pDetail);
            fputs(acTmp, fdDetail);

            // Generate Agency record
            Tax_CreateAgencyCsv(acTmp, pAgency);
            fputs(acTmp, fdAgency);
         }
         iIdx2++;
      }
   }

   // Check other cost
   if (*apTokens[NCSEC_OTHER_COSTS] > '0')
      dOthCost += Nev_GetTaxAmt(apTokens[NCSEC_OTHER_COSTS]);

   double dTotalCost = dCost1+dCost2+dOthCost;
   if (dTotalCost > 0.0)
      sprintf(pBase->TotalFees, "%.2f", dTotalCost);

#ifdef _DEBUG
   //if (!memcmp(pDetail->Apn,"2550037000", 10))
   //   iTmp=0;
#endif

   // Penalty
   if (dPen1 > 0.0)
      sprintf(pBase->PenAmt1, "%.2f", dPen1);
   if (dPen2 > 0.0)
      sprintf(pBase->PenAmt2, "%.2f", dPen2);

   dTax1 = atof(pBase->TaxAmt1);
   dTax2 = atof(pBase->TaxAmt2);

   // Cast to float to avoid floating point error
   if (dPen1 > 0.0)
   {
      // TaxAmt1 already includes penalty, we have to take it out
      if ((float)dPen1 == (float)(dTax1-dTax2))
      {
         sprintf(pBase->TaxAmt1, "%.2f", dTax2);
         sprintf(pBase->TotalTaxAmt, "%.2f", dTax2*2);
         dTax1 = dTax2;
      } else
      {
         dTax1 = dTax1-dPen1;
         sprintf(pBase->TaxAmt1, "%.2f", dTax1);
      }
   } 
   
   if ((float)dPen2 > 0.0)
   {
      // TaxAmt2 includes penalty, we have to take it out
      dTax2 = dTax2-dPen2;
      dTax2 = dTax2-dCost2;
      sprintf(pBase->TaxAmt2, "%.2f", dTax2);
   } 

   double dTotalTax = dTax1 + dTax2;
   if (dTotalTax > 0.0)
      sprintf(pBase->TotalTaxAmt, "%.2f", dTotalTax);

   if (pBase->PaidAmt1[0] < '1')
      dTotalDue = dTax1+dPen1+dTax2+dPen2+dTotalCost+dOthPen+dFee1+dFee2;
   else if (pBase->PaidAmt2[0] < '1')
      dTotalDue = dTax2+dPen2+dOthPen+dCost2+dOthCost+dFee2;

   if (dTotalDue > 0.0)
      sprintf(pBase->TotalDue, "%.2f", dTotalDue);

   return 0;
}

/***************************** Nev_ParseTaxBase ******************************
 *
 *
 *****************************************************************************/

int Nev_ParseTaxBase(char *pOutbuf, char *pInbuf)
{
   char     acTmp[256], sTaxAmt1[256], sTaxAmt2[256];
   int      iTmp, iTmpYear;
   double   dTmp, dTax1, dTax2, dPaid1, dPaid2;
   TAXBASE  *pOutRec = (TAXBASE *)pOutbuf;

   // Parse input tax data
   iTmp = ParseStringNQ(pInbuf, ',', MAX_FLD_TOKEN, apTokens);
   if (iTmp < NCSEC_OCCUP_TAX_FLAG)
   {
      LogMsg("***** Error: bad Tax record for APN=%.*s (#tokens=%d)", iApnLen, pInbuf, iTmp);
      return -1;
   }

   // Clear output buffer
   memset(pOutbuf, 0, sizeof(TAXBASE));

   // Tax Year - skip prior year
   iTmpYear = 2000 + atoin(apTokens[NCSEC_ROLL_YR], 2);
   if (iTmpYear != lTaxYear)
      return -2;

   // Skip corrected bill
   if (*apTokens[NCSEC_CORR_NUM] > ' ')
      return -3;

   iTmp = sprintf(pOutRec->TaxYear, "%d", iTmpYear);
   strcpy(pOutRec->OwnerInfo.TaxYear, pOutRec->TaxYear);

   // APN
   strcpy(pOutRec->Apn, apTokens[NCSEC_APN]);
   strcpy(pOutRec->BillNum, myBTrim(apTokens[NCSEC_ASSMT]));
   strcpy(pOutRec->OwnerInfo.Apn, apTokens[NCSEC_APN]);
   strcpy(pOutRec->OwnerInfo.BillNum, apTokens[NCSEC_ASSMT]);

   // Bill Type
   pOutRec->isSecd[0] = '1';
   pOutRec->BillType[0] = BILLTYPE_SECURED;

   // TRA
   iTmp = atol(apTokens[NCSEC_TAC]);
   sprintf(pOutRec->TRA, "%.6d", iTmp);

#ifdef _DEBUG
   //if (!memcmp(pOutRec->Apn,"0122019000", 10))
   //   dPaid1=0;
#endif

   // Check for Tax amount
   dTax1=dTax2=0.0;
   if (*apTokens[NCSEC_1CHG_AMT] > ' ')
   {
      strcpy(sTaxAmt1, apTokens[NCSEC_1CHG_AMT]);
      dTax1 = Nev_GetTaxAmt(sTaxAmt1);
      sprintf(pOutRec->TaxAmt1, "%.2f", dTax1);
   }
   if (*apTokens[NCSEC_2CHG_AMT] > ' ')
   {
      strcpy(sTaxAmt2, apTokens[NCSEC_2CHG_AMT]);
      dTax2 = Nev_GetTaxAmt(sTaxAmt2);
      sprintf(pOutRec->TaxAmt2, "%.2f", dTax2);
   }

   if (dTax1 > 0.0)
      sprintf(pOutRec->TotalTaxAmt, "%.2f", dTax1+dTax2);

   // Tax rate
   dTmp = atof(apTokens[NCSEC_COUNTY_RATE])*100.0;
   sprintf(pOutRec->TotalRate, "%.5f", dTmp);

   // Paid amt
   dPaid1=dPaid2=0.0;
   if (*apTokens[NCSEC_PAID_AMTS] > '0')
   {
      // If there are more than 2 payments, we have to add them up then divide by 2
      iTmp = countChar(apTokens[NCSEC_PAID_AMTS], '|');
      if (strchr(apTokens[NCSEC_PAID_AMTS], '|'))
      {
         iTmp = Nev_GetPaidAmt(apTokens[NCSEC_PYMT_CODE], apTokens[NCSEC_PAID_AMTS], &dPaid1, &dPaid2);
      } else
      {
         dTmp = dollar2Double(apTokens[NCSEC_PAID_AMTS]);
         if (*apTokens[NCSEC_PYMT_CODE] == '1')
            dPaid1 = dTmp;
         else if (*apTokens[NCSEC_PYMT_CODE] == '2')
            dPaid2 = dTmp;
         else
            dPaid1=dPaid2=dTmp/2;
      }
   }

   // Paid Date
   if (*apTokens[NCSEC_1ST_PAID] >= '0' && dateConversion(apTokens[NCSEC_1ST_PAID], acTmp, MM_DD_YYYY_2))
   {
      strcpy(pOutRec->PaidDate1, acTmp);
      sprintf(pOutRec->PaidAmt1, "%.2f", dPaid1);
      pOutRec->Inst1Status[0] = TAX_BSTAT_PAID;
   } else
   {
      if (dTax1 > 0.0)
         pOutRec->Inst1Status[0] = TAX_BSTAT_UNPAID;
      else
         pOutRec->Inst1Status[0] = TAX_BSTAT_NOTAX;
   }

   if (*apTokens[NCSEC_2ND_PAID] >= '0' && dateConversion(apTokens[NCSEC_2ND_PAID], acTmp, MM_DD_YYYY_2))
   {
      strcpy(pOutRec->PaidDate2, acTmp);
      sprintf(pOutRec->PaidAmt2, "%.2f", dPaid2);
      pOutRec->Inst2Status[0] = TAX_BSTAT_PAID;
   } else
   {
      if (dTax2 > 0.0)
         pOutRec->Inst2Status[0] = TAX_BSTAT_UNPAID;
      else
         pOutRec->Inst2Status[0] = TAX_BSTAT_NOTAX;
   }

   // Due date
   if (*apTokens[NCSEC_1ST_DUE] >= '0' && dateConversion(apTokens[NCSEC_1ST_DUE], acTmp, MM_DD_YYYY_2))
      strcpy(pOutRec->DueDate1, acTmp);
   if (*apTokens[NCSEC_2ND_DUE] >= '0' && dateConversion(apTokens[NCSEC_2ND_DUE], acTmp, MM_DD_YYYY_2))
      strcpy(pOutRec->DueDate2, acTmp);

#ifdef _DEBUG
   //if (!memcmp(pOutRec->Apn, "1670301800", 10))
   //   iTmp = 0;
#endif

   // Owner Info
   strcpy(pOutRec->OwnerInfo.Name1, apTokens[NCSEC_OWNER]);

   // CareOf
   if (*apTokens[NCSEC_CAREOF] > ' ')
      strcpy(pOutRec->OwnerInfo.CareOf, apTokens[NCSEC_CAREOF]);

   // Mailing address
   if (*apTokens[NCSEC_STREET] > ' ')
   {
      strcpy(pOutRec->OwnerInfo.MailAdr[0], apTokens[NCSEC_STREET]);
      sprintf(acTmp, "%s %s", apTokens[NCSEC_CITY_ST], apTokens[NCSEC_ZIP]);
      iTmp = blankRem(acTmp);
      strcpy(pOutRec->OwnerInfo.MailAdr[1], acTmp);
   }

   return 0;
}

/**************************** Nev_Load_TaxBase *******************************
 *
 * Create import file for Tax_Base, Tax_Items, Tax_Agency & Tax_Owner tables
 *
 * Return 0 if success.
 *
 *****************************************************************************/

int Nev_Load_TaxBase(bool bImport)
{
   char     *pTmp, acBuf[MAX_RECSIZE], acRec[MAX_RECSIZE], acTmpFile[_MAX_PATH];
   char     acAgencyFile[_MAX_PATH], acOwnerFile[_MAX_PATH], acBaseFile[_MAX_PATH], 
            acDetailFile[_MAX_PATH], acSecRollFile[_MAX_PATH]; //, acDelqFile[_MAX_PATH];

   int      iRet;
   long     lOut=0, lCnt=0;
   FILE     *fdAgency, *fdOwner, *fdBase, *fdDetail, *fdSecRoll;
   TAXBASE  *pTaxBase = (TAXBASE *)&acBuf[0];

   LogMsg("Loading Current tax file");

   sprintf(acDetailFile, sTaxOutTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "Items");
   sprintf(acOwnerFile, sTaxOutTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "Owner");
   sprintf(acBaseFile, sTaxOutTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "Base");
   sprintf(acAgencyFile, sTaxOutTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "Agency");

   pTmp = strrchr(acBaseFile, '\\');
   *pTmp = 0;
   if (_access(acBaseFile, 0))
      _mkdir(acBaseFile);
   *pTmp = '\\';

   GetIniString(myCounty.acCntyCode, "CurrTax", "", acSecRollFile, _MAX_PATH, acIniFile);
   lLastTaxFileDate = getFileDate(acSecRollFile);
   // Only process if new tax file
   iRet = isNewTaxFile(NULL, myCounty.acCntyCode);
   if (iRet <= 0)
   {
      lLastTaxFileDate = 0;
      return iRet;
   }

   // Open input file
   //GetIniString(myCounty.acCntyCode, "DelqTax", "", acDelqFile, _MAX_PATH, acIniFile);
   //LogMsg("Open delinquent tax file %s", acDelqFile);
   //fdDelq = fopen(acDelqFile, "r");
   //if (fdDelq == NULL)
   //{
   //   LogMsg("***** Error opening delinquent tax file: %s\n", acDelqFile);
   //   return -2;
   //}  
   // Skip header
   //for (iRet = 0; iRet < iHdrRows; iRet++)
   //   pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdDelq);

   // Open input file
   LogMsg("Open Current tax file %s", acSecRollFile);
   fdSecRoll = fopen(acSecRollFile, "r");
   if (fdSecRoll == NULL)
   {
      LogMsg("***** Error opening Current tax file: %s\n", acSecRollFile);
      return -2;
   }  
   // Skip header
   for (iRet = 0; iRet < iHdrRows; iRet++)
      pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdSecRoll);

   // Open Detail file
   LogMsg("Open Detail file %s", acDetailFile);
   fdDetail = fopen(acDetailFile, "w");
   if (fdDetail == NULL)
   {
      LogMsg("***** Error creating Detail file: %s\n", acDetailFile);
      return -4;
   }

   // Open base file
   LogMsg("Open Base file %s", acBaseFile);
   fdBase = fopen(acBaseFile, "w");
   if (fdBase == NULL)
   {
      LogMsg("***** Error creating base file: %s\n", acBaseFile);
      return -4;
   }

   // Open owner file
   LogMsg("Open Owner file %s", acOwnerFile);
   fdOwner = fopen(acOwnerFile, "w");
   if (fdOwner == NULL)
   {
      LogMsg("***** Error creating Owner file: %s\n", acOwnerFile);
      return -4;
   }

   // Open agency file
   sprintf(acTmpFile, "%s\\%s\\%s_Agency.lst", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   LogMsg("Open Agency file %s", acTmpFile);
   fdAgency = fopen(acTmpFile, "w");
   if (fdAgency == NULL)
   {
      LogMsg("***** Error creating Owner file: %s\n", acTmpFile);
      return -4;
   }

   // Init variables
   //lTaxYear = 0;

   // Merge loop 
   while (!feof(fdSecRoll))
   {
      pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdSecRoll);
      if (!pTmp || *pTmp >= 'A')
         break;

      // Create new R01 record
      iRet = Nev_ParseTaxBase(acBuf, acRec);
      if (!iRet)
      {
         // Create Detail & Agency records
         iRet = Nev_ParseTaxDetail(fdDetail, fdAgency, acBuf);

         // Update delq data
         //iRet = Nev_MergeTaxDelq(acBuf);

         // Create TaxBase record
         Tax_CreateTaxBaseCsv(acRec, (TAXBASE *)&acBuf);
         lOut++;
         fputs(acRec, fdBase);

         // Create Owner record
         Tax_CreateTaxOwnerCsv(acRec, &pTaxBase->OwnerInfo);
         fputs(acRec, fdOwner);
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdSecRoll)
      fclose(fdSecRoll);
   //if (fdDelq)
   //   fclose(fdDelq);
   if (fdBase)
      fclose(fdBase);
   if (fdDetail)
      fclose(fdDetail);
   if (fdOwner)
      fclose(fdOwner);
   if (fdAgency)
      fclose(fdAgency);

   LogMsg("Total records processed:    %u", lCnt);
   LogMsg("Total output records:       %u", lOut);

   // Import into SQL
   if (bImport)
   {
      iRet = doTaxImport(myCounty.acCntyCode, TAX_BASE);
      if (!iRet)
      {
         //iRet = doTaxImport(myCounty.acCntyCode, TAX_OWNER);
         iRet = doTaxImport(myCounty.acCntyCode, TAX_DETAIL);
      }

      if (!iRet)
      {
         // Dedup Agency file before import
         LogMsg("Dedup Agency file %s to %s", acTmpFile, acAgencyFile);
         iRet = sortFile(acTmpFile, acAgencyFile, "S(#1,C,A) DUPOUT F(TXT)");
         if (iRet > 0)
            iRet = doTaxImport(myCounty.acCntyCode, TAX_AGENCY);
      }
   } else
      iRet = 0;

   return iRet;
}

/****************************** Nev_ParseTaxDelq *****************************
 *
 * Input: NCDELINQTAX.TXT
 *
 *****************************************************************************/

int Nev_ParseTaxDelq(char *pOutbuf, char *pInbuf)
{
   int      iTmp;
   double   dTmp, dRedAmt;
   TAXDELQ  *pOutRec = (TAXDELQ *)pOutbuf;

   // Parse input tax data
   iTmp = ParseStringNQ(pInbuf, ',', MAX_FLD_TOKEN, apTokens);
   if (iTmp < NCDELQ_OTHER_DESC)
   {
      LogMsg("***** Error: bad Tax record for APN=%.*s (#tokens=%d)", iApnLen, pInbuf, iTmp);
      return -1;
   }

   // Clear output buffer
   memset(pOutbuf, 0, sizeof(TAXDELQ));

   // APN
   strcpy(pOutRec->Apn, apTokens[NCDELQ_APN]);
   pOutRec->isDelq[0] = '1';

   // Updated date
   sprintf(pOutRec->Upd_Date, "%d", lLastTaxFileDate);

   if (*apTokens[NCDELQ_BILL] == 'S')
   {
      pOutRec->isSupp[0] = '1';
      strcpy(pOutRec->BillNum, apTokens[NCDELQ_BILL]+1);
   } else
   {
      pOutRec->isSecd[0] = '1';
      strcpy(pOutRec->BillNum, apTokens[NCDELQ_BILL]);
   }

   // Tax Year
   strcpy(pOutRec->TaxYear, apTokens[NCDELQ_TAXYEAR]);

   // Year Default 
   iTmp = atol(apTokens[NCDELQ_TAXYEAR]);
   sprintf(pOutRec->Def_Date, "%d0630", iTmp+1);

   // Tax amt
   dRedAmt = Nev_GetTaxAmt(apTokens[NCDELQ_TAXAMT]);
   sprintf(pOutRec->Tax_Amt, "%.2f", dRedAmt);

   // Default amt
   strcpy(pOutRec->Def_Amt, pOutRec->Tax_Amt);

   // Pen
   remChar(apTokens[NCDELQ_PENAMT], ',');
   dTmp = atof(apTokens[NCDELQ_PENAMT]);
   sprintf(pOutRec->Pen_Amt, "%.2f", dTmp);
   dRedAmt += dTmp;

   // Fee
   dTmp = atof(apTokens[NCDELQ_COST]);
   dTmp += Nev_GetTaxAmt(apTokens[NCDELQ_OTHER_COSTS]);         
   sprintf(pOutRec->Fee_Amt, "%.2f", dTmp);
   dRedAmt += dTmp;

   // Redemption date
   //sprintf(pOutRec->Red_Amt, "%.2f", dRedAmt);

   return 0;
}

/***************************** Nev_Load_TaxDelq ****************************
 *
 *
 ***************************************************************************/

int Nev_Load_TaxDelq(bool bImport)
{
   char     *pTmp, acBuf[MAX_RECSIZE], acRec[MAX_RECSIZE];
   char     acOutFile[_MAX_PATH], acInFile[_MAX_PATH];

   int      iRet;
   long     lOut=0, lCnt=0;
   FILE     *fdOut, *fdIn;

   LogMsg("Loading delinquent tax file");
   sprintf(acOutFile, sTaxOutTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "Delq");

   GetIniString(myCounty.acCntyCode, "DelqTax", "", acInFile, _MAX_PATH, acIniFile);
   lLastTaxFileDate = getFileDate(acInFile);


   // Open input file
   LogMsg("Open delinquent tax file %s", acInFile);
   fdIn = fopen(acInFile, "r");
   if (fdIn == NULL)
   {
      LogMsg("***** Error opening delinquent tax file: %s\n", acInFile);
      return -2;
   }  

   // Open Output file
   LogMsg("Open output file %s", acOutFile);
   fdOut = fopen(acOutFile, "w");
   if (fdOut == NULL)
   {
      LogMsg("***** Error creating output file: %s\n", acOutFile);
      return -4;
   }

   // Skip header
   for (iRet = 0; iRet < iHdrRows; iRet++)
      pTmp = fgets(acRec, MAX_RECSIZE, fdIn);

   // Merge loop 
   while (!feof(fdIn))
   {
      pTmp = fgets(acRec, MAX_RECSIZE, fdIn);
      if (!pTmp || *pTmp >= 'A')
         break;

      // Create new R01 record
      iRet = Nev_ParseTaxDelq(acBuf, acRec);
      if (!iRet)
      {
         // Create delimited record
         Tax_CreateDelqCsv(acRec, (TAXDELQ *)&acBuf[0]);

         // Output record			
         fputs(acRec, fdOut);
         lOut++;
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdIn)
      fclose(fdIn);
   if (fdOut)
      fclose(fdOut);

   LogMsg("Total records processed:          %u", lCnt);
   LogMsg("Total delinquent records output:  %u", lOut);

   printf("\nTotal output records: %u", lCnt);

   // Import into SQL
   if (bImport && lOut > 0)
   {
      iRet = doTaxImport(myCounty.acCntyCode, TAX_DELINQUENT);
   } else
      iRet = 0;

   return iRet;
}

/********************************* formatCSale *******************************
 *
 * Return 0 if success, -1 if bad DocNum, -2 if bad DocDate
 *
 * "88-031824";"11/22/1988"
 * "86007859";"04/11/1986"    -> 86-007589
 * "80000727";"01/09/1980"    -> 80-000727
 * "01017519";"02/09/1979"    -> 1017/519
 * "00983513";"09/25/1978"    -> 983/513
 *
 *****************************************************************************/

int Nev_FormatCSale(char *pOutbuf, char *pInbuf)
{
   SCSAL_REC *pCSale= (SCSAL_REC *)pOutbuf;
   NEV_SALE  *pInRec= (NEV_SALE  *)pInbuf;

   int        iDocTax, iTmp, iLen;
   char       acTmp[32], *pTmp;

   // Inititalize
   memset(pOutbuf, ' ', sizeof(SCSAL_REC));
   memcpy(pCSale->Apn, pInRec->Apn, SSIZ_NEV_APN);

#ifdef _DEBUG
   //if (!memcmp(pCSale->Apn, "0101025000", 10) )
   //   iTmp = 0;
#endif

   // SaleDate
   pTmp = dateConversion(pInRec->DocDate, acTmp, MM_DD_YYYY_1);
   if (pTmp)
   {
      memcpy(pCSale->DocDate, acTmp, SALE_SIZ_DOCDATE);
      iTmp = atoin(acTmp, 4);

      // Check DocNum
      if (!memcmp(pCSale->DocDate, &pInRec->DocNum[4], 4) && 
          !memcmp(&pCSale->DocDate[4], &pInRec->DocNum, 4))
         return -1;
   } else
      return -2;

   // Sale Doc
   iLen = 0;
   if (pInRec->DocNum[2] == '-')
   {
      if (pInRec->DocNum[3] == '-')
         pInRec->DocNum[3] = '0';
      memcpy(acTmp, pInRec->DocNum, SSIZ_NEV_DOCNUM);
      iLen = SSIZ_NEV_DOCNUM;
      acTmp[iLen] = 0;
      if (strchr(&acTmp[3], '-'))
         return -1;
   } else if (iTmp < 1980)
   {
      memcpy(acTmp, pInRec->DocNum, SSIZ_NEV_DOCNUM);
      myTrim(acTmp, SSIZ_NEV_DOCNUM);
      iLen = strlen(acTmp);
      if (!strchr(acTmp, '/') && iLen > 7)
      {
         iTmp = atoin(pInRec->DocNum, 5);
         iLen = sprintf(acTmp, "%d/%.3s", iTmp, &pInRec->DocNum[5]);
      }
   } else if (iTmp > 0)
   {
      memcpy(acTmp, pInRec->DocNum, SSIZ_NEV_DOCNUM);
      myTrim(acTmp, SSIZ_NEV_DOCNUM);
      iLen = strlen(acTmp);
      if (!strchr(acTmp, '/') && iLen == 8)
         iLen = sprintf(acTmp, "%.2s-%.6s", pInRec->DocNum, &pInRec->DocNum[2]);
   }

   // Verify DocNum
   if (!memcmp(pCSale->DocDate, &pInRec->DocNum[4], 4) && 
       memcmp(&pCSale->DocDate[2], acTmp, 2))
      return -1;

   memcpy(pCSale->DocNum, acTmp, iLen);

   iDocTax = atoin(pInRec->DocTax, SSIZ_NEV_STAMP_AMOUNT);
   if (iDocTax > 0)
   {
      memcpy(pCSale->StampAmt, pInRec->DocTax, SSIZ_NEV_STAMP_AMOUNT);

      // Sale price
      iTmp = sprintf(acTmp, "%*d", SALE_SIZ_SALEPRICE, (INT)(iDocTax*SALE_FACTOR_100));
      memcpy(pCSale->SalePrice, acTmp, iTmp);
   }

   // Multi sale
   if (pInRec->Group_Sale[0] == 'Y')
      pCSale->MultiSale_Flg = 'Y';

   // DocType
   if (iDocTax > 110)
      pCSale->DocType[0] = '1';

   // Sale code
   iTmp = atoin(pInRec->Pct_Xfer, SSIZ_NEV_PCT_XFER);
   if (iTmp > 0 && iDocTax > 0)
   {
      if (iTmp == 100)
         pCSale->SaleCode[0] = 'F';
      else
         pCSale->SaleCode[0] = 'P';
   }
   pCSale->CRLF[0] = '\n';
   pCSale->CRLF[1] = 0;

   return 0;
}

/****************************** Nev_ExtrSale *********************************
 *
 * Extract history sale from ASRECH.  This file contains full sale history.
 * Create on 11/23/2016 to replace Nev_UpdateCumSale()
 *
 * Return 0 if successful, otherwise error
 *
 *****************************************************************************/

int Nev_ExtrSale(void)
{
   char     *pTmp, acCSalRec[512], acSaleRec[1024], acTmp[256];
   char     acTmpSale[_MAX_PATH];

   int      iRet, iTmp, iUpdateSale=0;
   long     lLstSaleDt=0, lCnt=0;
   SCSAL_REC *pSaleRec=(SCSAL_REC *)&acCSalRec[0];

   // Check current sale file
   if (_access(acSaleFile, 0))
   {
      LogMsg("***** Missing current sale file %s", acSaleFile);
      return -1;
   }

   // Open current sale
   LogMsg("Open sale file %s", acSaleFile);
   fdSale = fopen(acSaleFile, "r");
   if (fdSale == NULL)
   {
      LogMsg("***** Error opening current sale file: %s\n", acSaleFile);
      return -2;
   }

   // Create output file
   sprintf(acTmpSale, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "tmp");
   LogMsg("Create updated sale file %s", acTmpSale);
   fdCSale = fopen(acTmpSale, "w");
   if (fdCSale == NULL)
   {
      LogMsg("***** Error creating updated sale file: %s\n", acTmpSale);
      return -3;
   }

   // Read first history sale
   acCSalRec[0] = 0;

   // Merge loop
   while (!feof(fdSale))
   {
      // Get current sale
      pTmp = fgets(acSaleRec, 1024, fdSale);
      if (!pTmp)
         break;

      iRet = Nev_FormatCSale(acCSalRec, acSaleRec);
      if (!iRet)
      {
         // Output only record with sale price or if it's not required
         fputs(acCSalRec, fdCSale);
         iUpdateSale++;
         iTmp = atoin(pSaleRec->DocDate, 8);
         if (iTmp > lLstSaleDt && iTmp < lToday)
            lLstSaleDt = iTmp;
      } else if (bDebug)
         LogMsg("Bad input record (%d): %.70s", iRet, acSaleRec);

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);

   }

   // Close files
   if (fdCSale)
      fclose(fdCSale);
   if (fdSale)
      fclose(fdSale);

   // Sort/merge sale file - APN (asc), Saledate (asc), DocNum (asc)
   sprintf(acTmp, "S(1,%d,C,A,27,8,C,A,15,%d,C,A,57,10,C,D) DUPO(1,34)", iApnLen, SALE_SIZ_DOCNUM);
   iTmp = sortFile(acTmpSale, acCSalFile, acTmp);

   LogMsg("Total sale records processed:   %u", lCnt);
   LogMsg("Total cumulative sale records:  %u", iTmp);
   LogMsg("Total sale records updated:     %u", iUpdateSale);
   LogMsg("Latest recording date:          %u", lLstSaleDt);

   LogMsg("Update Sale History completed.");

   return 0;
}

/*********************************** loadNev ********************************
 *
 * Input files:
 *    - Asrech         70+1_A     +1=LF  Copyfile to sale
 *    - ASPUBROLL     600+1_A     +1=LF  Copyfile to roll
 *    - ASDESCROLL     50+1_A     +1=LF  Copyfile to char
 *
 * Operation commands:
 *    - Normal update:  LoadOne -CNEV -U -Xs[i] [-Xv[i]] [-Lc] 
 *    - Load Lien:      LoadOne -CNEV -L -Xl -Ms|Us -Lc [-Xv[i]] 
 *    - ??? Multiple Owners, other bldgs/impr, topo
 *
 ****************************************************************************/

int loadNev(int iSkip)
{
   char  acTmp[_MAX_PATH];
   int   iRet;

   iApnLen = myCounty.iApnLen;
 
   // Load tax
   if (iLoadTax == TAX_LOADING)              // -T or -Lt
   {
      // Load Tax Agency table
      iRet = GetIniString(myCounty.acCntyCode, "TaxDist", "", acTmp, _MAX_PATH, acIniFile);
      if (iRet > 0)
         iRet = LoadTaxCodeTable(acTmp, 1);

      iRet = Nev_Load_TaxBase(bTaxImport);
      if (!iRet && lLastTaxFileDate > 0)
      {
         iRet = Nev_Load_TaxDelq(bTaxImport);
         // To be tested
         //GetIniString(myCounty.acCntyCode, "SuppTax", "", acTmp, _MAX_PATH, acIniFile);
         //if (acTmp[0] > '0' && !_access(acTmp, 0))
         //   iRet = Nev_Load_TaxSupp(bTaxImport);

         if (!iRet)
            iRet = updateDelqFlag(myCounty.acCntyCode, false, true);

      }
   }

   if (!iLoadFlag)
      return iRet;

   // Extract lien file
   if (iLoadFlag & EXTR_LIEN)             // -Xl
      iRet = Nev_ExtrLien(myCounty.acCntyCode, acRollFile);

   // Load Value file
   if (iLoadFlag & EXTR_VALUE)            // -Xv
   {
      char sDbName[64], sBYVFile[_MAX_PATH];
      //char sCVFile[_MAX_PATH], sMergeFile[_MAX_PATH];

      // Merge value files - No current roll value
      sprintf(sBYVFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "Value");
      iRet = Nev_MergeValues(myCounty.acCntyCode, sBYVFile);
      if (iRet > 0)
      {
         sprintf(sDbName, "LDR%d", lLienYear);
         GetIniString("Data", "SqlValueFile", "", acTmp, _MAX_PATH, acIniFile);
         sprintf(acValueFile, acTmp, sDbName, myCounty.acCntyCode);
         lRecCnt = createValueImport(sBYVFile, acValueFile, false);
         if (lRecCnt > 0)
            iRet = 0;
         else if (iLoadFlag & EXTR_IVAL)
            iLoadFlag ^= EXTR_IVAL;      // Turn off import
      }

      // Merge value files - with current roll value
      //sprintf(sBYVFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "BYV");
      //iRet = Nev_MergeValues(myCounty.acCntyCode, sBYVFile);
      //if (iRet > 0)
      //{
      //   sprintf(sCVFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "CV");
      //   iRet = Mev_ExtrCurValues(acRollFile, sCVFile);
      //   if (iRet > 0)
      //   {
      //      // Merge data
      //      sprintf(sMergeFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "Value");
      //      sprintf(acTmp, "%s+%s", sBYVFile, sCVFile);
      //      // Sort on APN and ValueSetType
      //      iRet = sortFile(acTmp, sMergeFile, "S(1,12,C,A, 21,1,C,A) DUPO(1,75)");
      //      if (iRet > 0)
      //      {
      //         sprintf(sDbName, "LDR%d", lLienYear);
      //         GetIniString("Data", "SqlValueFile", "", acTmp, _MAX_PATH, acIniFile);
      //         sprintf(acValueFile, acTmp, sDbName, myCounty.acCntyCode);
      //         lRecCnt = createValueImport(sMergeFile, acValueFile, false);
      //         if (lRecCnt > 0)
      //            iRet = 0;
      //         else
      //            iLoadFlag ^= EXTR_IVAL;      // Turn off import
      //      }
      //   }
      //}
   }

   // Create CHAR file
   if (iLoadFlag & LOAD_ATTR)             // -Lc
   {
      //char acLUFile[_MAX_PATH];

      // Get Lookup file name
      //GetIniString("System", "LookUpTbl", "", acLUFile, _MAX_PATH, acIniFile);

      //// Load tables
      //iRet = LoadLUTable((char *)&acLUFile[0], "[Quality]", NULL, MAX_ATTR_ENTRIES);
      //if (!iRet)
      //{
      //   LogMsg("***** Error Looking for table [Quality] in %s", acLUFile);
      //   return 1;
      //}

      iRet = Nev_ConvertChar();
      if (iRet <= 0)
      {
         LogMsg("***** Error converting Char file to %s", acCChrFile);
         return -1;
      }

      iRet = 0;
   }

   // This has to be done before roll update
   if (iLoadFlag & (EXTR_SALE|UPDT_SALE))             // -Xs or -Us
   {
      // Update history sale 
      LogMsg("Update %s sale history file", myCounty.acCntyCode);
      
      iRet = Nev_ExtrSale();
      if (!iRet)
         iLoadFlag |= MERG_CSAL;

      //GetIniString(myCounty.acCntyCode, "AllSaleDelim", ",", acTmp, _MAX_PATH, acIniFile);
      //GetIniString(myCounty.acCntyCode, "AllSale", "", acSaleFile, _MAX_PATH, acIniFile);
      //iRet = Nev_UpdateCumSale(NCALS1_ECON_OBS, acTmp[0], false);
      //if (!iRet)
      //{
      //   GetIniString(myCounty.acCntyCode, "NewSaleDelim", ",", acTmp, _MAX_PATH, acIniFile);
      //   GetIniString(myCounty.acCntyCode, "NewSale", "", acSaleFile, _MAX_PATH, acIniFile);
      //   iRet = Nev_UpdateCumSale(NCALS1_GROUPSL, acTmp[0], true);
      //   if (!iRet)
      //      iLoadFlag |= MERG_CSAL;
      //}
   }

   if (iLoadFlag & LOAD_LIEN)
   {
      // Create Lien file
      LogMsg("Load %s Lien file", myCounty.acCntyCode);
      iRet = Nev_Load_LDR(iLoadFlag, iSkip);
   } else if (iLoadFlag & LOAD_UPDT)
   {
      LogMsg("Load %s roll update file", myCounty.acCntyCode);
      iRet = Nev_Load_Roll(iLoadFlag, iSkip);
   }

   // Apply cum sale file to R01
   if (!iRet && (iLoadFlag & MERG_CSAL) )          // -Ms
   {
      // Apply Nev_Sale.sls to R01 file
      sprintf(acCSalFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "sls");
      iRet = ApplyCumSale(iSkip, acCSalFile, true, SALE_USE_SCSALREC);
   }

   // Fix TRA
   if (!iRet && (lOptMisc & M_OPT_FIXTRA))
      iRet = PQ_FixR01(myCounty.acCntyCode, PQ_FIX_TRA, 0, true);

   if (!iRet && bMergeOthers)
   {
      sprintf(acTmp, acLienTmpl, myCounty.acCntyCode, myCounty.acCntyCode);
      if (!_access(acTmp, 0))
         iRet = PQ_MergeOthers(myCounty.acCntyCode, acTmp, GRP_NEV, iSkip);
      else
         LogMsg("***** Lien extract is missing: %s.  Please locate the file and retry", acTmp);
   }

   return iRet;
}