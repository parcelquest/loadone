/******************************************************************************
 *
 * Input files:
 *    - AssessorRoll.txt(Roll file, 1233-byte EBCDIC with CRLF)
 *    - Mpa_Lien.txt    (Lien file, 1233-byte EBCDIC with CRLF)
 *    - Unassessed.txt  (public parcels)
 *       * 2012 - same format as roll file
 *       * 2011 and before is csv format
 *
 * Operation commands:
 *    - Normal update:  LoadOne -CMPA -U
 *    - Load Lien:      LoadOne -CMPA -L
 *
 * Notes: MPA has 9-digit APN. The 10th digit is reserved for special case like
 *        a property with 2 separate tax bills.  This has no meaning other than 
 *        assessor internal use.  But here we display all 10 digits as we received 
 *        from the county.
 *
 * Revision:
 * 10/03/2006 TCB
 * 01/09/2007 1.3.9    Add error checking for doEBC2ASC() since county may send
 *                     different file name each time.
 * 03/15/2007 1.4.5    Add Char and Land sale data.
 * 03/11/2008 1.5.6    Set flag bCreateUpd=true to trigger update file creation
 * 03/24/2008 1.5.7.2  Format IMAPLINK.
 * 06/09/2008 1.6.3    Ignore record where APN starts with blank
 * 08/01/2008 8.0.6    Modify Mpa_MergeRoll() to get the most accurate DocDate and DocNum
 *                     from DeedRef field.  Adding Mpa_ExtrLien() and rename CreateMpaRoll()
 *                     to Mpa_Load_LDR() to make function name standard.
 * 08/07/2008 8.1      Fix Mpa_ExtrLien()
 * 09/04/2008 8.2.1    Correcting output records count in MergeMpaRoll().
 * 09/11/2008 8.3      Add public parcel.
 * 12/11/2008 8.5.1    Make StrNum left justified
 * 01/17/2009 8.5.6    Fix TRA
 * 07/29/2009 9.1.4    Modify Mpa_MergeRoll() to update other values in R01.
 * 02/23/2010 9.4.3    Fix CARE_OF issue.
 * 05/28/2010 9.6.2    Populate Zoning with data from Sale & Char files.  Cannot use 
 *                     Zoning translation from Roll file yet until we have good translation 
 *                     table that can fit in with curent layout.
 * 06/08/2010 9.6.3    Update using ZONING (principle only) from roll file.  Also update
 *                     USE_CODE & USE_STD.
 *            9.6.3.1  Comment out USE_CODE & USE_STD until assessor said it's OK.
 * 07/29/2011 11.0.5   Fix bug in Mpa_Load_LDR() that opens wrong sale file.  Resort cum sale
 *                     file before calling Mpa_Load_LDR(). Rewrite Mpa_MergeSAdr() to
 *                     better handling weird addresses.  Add S_HSENO.
 * 10/04/2011 11.4.10  Add -Xs to create standard history sale file from both impr & land sale files.
 * 03/01/2012 11.6.7   Fix LotSize calculation in Mpa_CreatePublR01() since value in unassessed file
 *                     are in acres. This is different from assessed roll.
 * 05/31/2012 11.9.8   Add new Mpa_CreatePublR01() and Mpa_MergePubl() to support new
 *                     unassessed data file. This version has the same format as current roll file.
 * 06/13/2012 11.9.10  Update TRA using current roll.
 * 05/31/2013 12.6.4   Redo sale extract due to new format from the county. Use ApplyCumSale() 
 *                     and add Mpa_FmtDocLinks() to add DocLinks to R01.
 * 07/26/2013 13.2.7   Format acCharFile when -Ms is used.
 * 10/08/2013 13.10.4  Use updateVesting() to update Vesting and Etal flag. Add 3/4 bath.  
 *                     There is only a few parcels with 1/4 bath, so ignore it.
 * 10/11/2013 13.11.0  Add all QBaths to R01
 * 06/03/2014 13.14.6  Modify MergeRoll() and FmtDocNum() to fix DocNum format.
 * 12/02/2014 14.10.0  Modify int Mpa_FmtDocLinks() & Mpa_MakeDocLink() to fix document path.
 * 01/19/2015 14.11.1  Add Mpa_Roll2Sale() to copy Transfer DocNum from roll file to sale file if they
 *                     have the same DocDate.
 * 03/03/2015 14.12.2  Fix Mpa_MergePubl() to ignore records with blank in the first position.
 * 03/13/2015 14.13.0  Replace Mpa_FmtDocLinks() with UpdateDocLinks().
 * 07/27/2015 15.0.6   Modify Mpa_Roll2Sale() to return 0 even when there is no record updated.
 * 08/03/2015 15.0.8   Change Mpa_Roll2Sale() to return 0 on successful updated, negative value if error.
 * 09/20/2016 16.2.6   Add Mpa_Load_TaxBase() and Mpa_Load_TaxDelq().  Still need functions to
 *                     update tax data using TX490 and TX491 file.
 * 12/15/2016 16.8.1   Add Mpa_Load_TaxBaseXls() and Mpa_Load_TaxDelqXls() to replace the CSV version.
 *                     With this, we can run automatically when tx490 available.
 *            16.8.1.1 Fix TaxYear and other bugs in Mpa_Load_TaxDelqXls()
 * 12/31/2016 16.8.4   Modify Mpa_Load_TaxBaseXls() to work with CSV update file.
 * 05/01/2017 16.14.6  Update tax bill status in Mpa_NewTaxBase() and Mpa_Load_TaxBaseXls().
 * 05/08/2017 16.14.8  Add Mpa_Load_TaxSuppXls() and related function to load supplemental file.
 *                     Modify Mpa_Load_TaxBaseXls() to use T491 to update delq status and create Delq records.
 * 08/04/2017 17.1.8   Modify Tax loading procedure to check for new files in sub folder.
 * 11/21/2017 17.5.0   Call updateDelqFlg() to populate delqYear & defaultAmt in Tax_Base table.
 * 11/28/2017 17.5.1   Replace Mpa_Load_TaxBaseXls() with Mpa_Load_TaxBaseCsv() because XLS file is not sorted
 *                     that makes it impossible to match up APN for update.
 * 02/19/2018 17.6.6   Modify Mpa_FmtDocNum(), Mpa_MergeRoll(), Mpa_Roll2Sale() to fixed up DocNum
 *                     and add log msg to display bad DocNum/DocDate.
 * 04/26/2018 17.10.3  Modify Mpa_Load_TaxBaseCsv() to fix Agency problem. Modify Mpa_Load_TaxSuppXls() to support
 *                     one file supplemental.  Assume all tax files now in TC folder.
 *                     Hard code "MCUSD 2016 SERIES A BOND 209-966-7606" to fix "MB17" tax code duplicate.
 * 11/06/2018 18.5.14  Modify APN to include 4th key from TX490 tax file.  Remove the translation of
 *                     "MCUSD 2016 SERIES A BOND" to "MCUSD 2016 SERIES A BOND 209-966-7606"
 * 11/08/2018 18.5.15  Add Mpa_Load_TaxSuppCsv() & Mpa_ParseTaxSupp() to load supplemental in CSV format.
 *                     TX490 also contains paid status of supplemental so we need to merge them too.
 * 02/01/2019 18.9.1.1 Add code to check for new tax file.
 * 02/03/2019 18.9.2   Process new tax file only.  Email when new county tax file arrived.
 * 02/07/2019 18.9.2.4 Fine tune tax processing.  Supplemental records in supp file are the same as those in tx490.  
 *                     No update needed when loading supplemental file, unless thing change in the future.
 * 03/18/2019 18.10.1  Change loading tax process to check current tax file for new update instead of base tax file.
 *                     Base tax file TX405 is annual but curr tax file TX490 is monthly.
 * 03/19/2019 18.10.2  Modify Mpa_ParseTaxDelq() to fix default amount calculation.
 * 11/28/2019 19.5.8   Add Mpa_ExtrCommSale() to process commercial sale file.
 * 06/20/2020 20.0.0   Modify Mpa_MergeRoll() & Mpa_CreateLienRec() to update exemption code.
 *                     Update exemption code even without exe amt.
 * 10/16/2020 20.3.0   Fix bug in loadMpa() that checks return code incorrectly when renaming sale file.
 * 10/28/2020 20.3.6   Modify Mpa_MergeChar(), Mpa_MergeRoll() & Mpa_CreatePublR01() to populate PQZoning.
 * 11/10/2020 20.4.4   Fix bug in Mpa_MergeChar().
 *
 ******************************************************************************/

#include "stdafx.h"
#include "CountyInfo.h"
#include "hlAdo.h"
#include "Prodlib.h"
#include "R01.h"
#include "RecDef.h"
#include "Tables.h"
#include "Logs.h"
#include "LoadDef.h"
#include "Utils.h"
#include "XlatTbls.h"
#include "doSort.h"
#include "doOwner.h"
#include "formatApn.h"
#include "SaleRec.h"
#include "LoadOne.h"
#include "MergeMpa.h"
#include "UseCode.h"
#include "LOExtrn.h"
#include "PQ.h"
#include "Tax.h"
#include "CSpreadSheet.h"

static   FILE  *fdChar, *fdDelq, *fdT491;
static   long  lSaleMatch, lSaleSkip, lCharMatch, lCharSkip, lUpdTaxFileDate, lDelqTaxFileDate;
static   char  *apLocalTokens[MAX_FLD_TOKEN];
static   int   iLocalTokens, iUnMatch490;

/******************************* GetZoning **********************************
 *
 *
 ****************************************************************************/

char *GetZoning(char *pUseZone)
{
   int   iTmp=0;
   char  *pRet=NULL;

   while (*tblUseZone[iTmp].pName)
   {
      // Compare first 4 bytes only
      if (!memcmp(pUseZone, tblUseZone[iTmp].pName, 4) )
      {
         pRet = tblUseZone[iTmp].pCode;
         break;
      }
      iTmp++;
   }
   return pRet;
}

/***************************** MergeLandSaleRec ******************************
 *
 * There is no DocNum in sale record.  So we update sale price and sale date only
 *
 *****************************************************************************/

int Mpa_MergeLandSaleRec(char *pOutbuf, char *pSale)
{
   long     lCurSaleDt, lLstSaleDt, lPrice, lLastAmt;
   char     acTmp[32];

   MPA_CHAR *pSaleRec = (MPA_CHAR *)pSale;
#ifdef _DEBUG
   //if (!memcmp(pSaleRec->Apn, "0020300050", 10))
   //   lPrice = 0;
#endif

   lPrice=0;
   lCurSaleDt = atoin(pSaleRec->SaleDate, MPA_CSIZ_DATE);
   lLstSaleDt = atoin(pOutbuf+OFF_SALE1_DT, SIZ_SALE1_DT);

   if (lCurSaleDt < lLstSaleDt)
      return -1;

   lPrice = atoin(pSaleRec->SalePrice, MPA_CSIZ_PRICE);
   if (lCurSaleDt == lLstSaleDt)
   {
      lLastAmt = atoin(pOutbuf+OFF_SALE1_AMT, SIZ_SALE1_AMT);
      if (!lPrice && lLastAmt > 0)
         return 0;
   } else
   {
      // Move sale2 to sale3
      //memcpy(pOutbuf+OFF_SALE3_DOC, pOutbuf+OFF_SALE2_DOC, SIZ_SALE3_DOC);
      memcpy(pOutbuf+OFF_SALE3_DT, pOutbuf+OFF_SALE2_DT, SIZ_SALE3_DT);
      memcpy(pOutbuf+OFF_SALE3_AMT, pOutbuf+OFF_SALE2_AMT, SIZ_SALE3_AMT);
      *(pOutbuf+OFF_AR_CODE3) = *(pOutbuf+OFF_AR_CODE2);

      // Move sale1 to sale2
      //memcpy(pOutbuf+OFF_SALE2_DOC, pOutbuf+OFF_SALE1_DOC, SIZ_SALE3_DOC);
      memcpy(pOutbuf+OFF_SALE2_DT, pOutbuf+OFF_SALE1_DT, SIZ_SALE3_DT);
      memcpy(pOutbuf+OFF_SALE2_AMT, pOutbuf+OFF_SALE1_AMT, SIZ_SALE3_AMT);
      *(pOutbuf+OFF_AR_CODE2) = *(pOutbuf+OFF_AR_CODE1);
   }

   memset(pOutbuf+OFF_SALE1_DOC, ' ', SIZ_SALE1_DOC);
   memcpy(pOutbuf+OFF_SALE1_DT, pSaleRec->SaleDate, SIZ_SALE1_DT);

   sprintf(acTmp, "%*d", SIZ_SALE1_AMT, lPrice);
   memcpy(pOutbuf+OFF_SALE1_AMT, acTmp, SIZ_SALE1_AMT);

   // Update transfers
   /*
   lLstSaleDt = atoin(pOutbuf+OFF_TRANSFER_DT, SIZ_TRANSFER_DT);
   if (lCurSaleDt >= lLstSaleDt)
   {
      if (lDocNum > 0)
         memcpy(pOutbuf+OFF_TRANSFER_DOC, acDocNum, SIZ_TRANSFER_DOC);
      else
         memset(pOutbuf+OFF_TRANSFER_DOC, ' ', SIZ_TRANSFER_DOC);

      if (isValidYMD(pSaleRec->Rec_Date))
         memcpy(pOutbuf+OFF_TRANSFER_DT, pSaleRec->Rec_Date, SIZ_TRANSFER_DT);
      else
         memset(pOutbuf+OFF_TRANSFER_DT, ' ', SIZ_TRANSFER_DT);
   }
   */

   *(pOutbuf+OFF_AR_CODE1) = 'A';
   return 1;
}

/********************************* MergeLandSale ******************************
 *
 *
 ******************************************************************************/

int Mpa_MergeLandSale(char *pOutbuf)
{
   static   char  acLandRec[512], *pRec=NULL;
   int      iRet, iLoop;
   MPA_CHAR *pSale;

   // Get first Sale rec for first call
   if (!pRec)
      pRec = fgets(acLandRec, 512, fdSale);
   pSale = (MPA_CHAR *)&acLandRec[0];

   do
   {
      // Compare Apn
      iLoop = memcmp(pOutbuf, pSale->Apn, iApnLen);
      if (iLoop > 0)
      {
         if (bDebug)
            LogMsg0("Skip Sale rec  %.*s", iApnLen, pSale->Apn);
         pRec = fgets(acLandRec, 512, fdSale);
         if (!pRec)
         {
            fclose(fdSale);
            fdSale = NULL;
            return 1;      // EOF
         }
         lSaleSkip++;
      }
   } while (iLoop > 0);

   if (iLoop)
      return 1;

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "015074002", 9))
   //   iRet = 0;
#endif

   do
   {
      iRet = Mpa_MergeLandSaleRec(pOutbuf, acLandRec);

      // Zoning
      if (pSale->Zoning[0] > ' ')
         memcpy(pOutbuf+OFF_ZONE, pSale->Zoning, MPA_CSIZ_ZONING);

      // Acres - Update only if it's not in roll file
      if (pSale->Acres[0] > ' ' && *(pOutbuf+OFF_LOT_ACRES+8) == ' ')
      {
         char  acTmp[64];
         long  lTmp;

         lTmp = atoin(pSale->Acres, MPA_SIZ_ACRES);
         sprintf(acTmp, "%*d", SIZ_LOT_ACRES, lTmp);
         memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);

         sprintf(acTmp, "%*d", SIZ_LOT_SQFT, (long)((double)lTmp*SQFT_FACTOR_1000+0.5));
         memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);
      }

      // Impr Type
      if (pSale->PropType[0] > ' ')
         memcpy(pOutbuf+OFF_IMPR_TYPE, pSale->PropType, MPA_CSIZ_PROPTYPE);

      pRec = fgets(acLandRec, 512, fdSale);
      if (!pRec)
      {
         fclose(fdSale);
         fdSale = NULL;
         break;
      }
   } while (!memcmp(pOutbuf, pSale->Apn, iApnLen));

   lSaleMatch++;

   return 0;
}

/******************************** Mpa_MergeChar ******************************
 *
 *
 *****************************************************************************/

int Mpa_MergeChar(char *pOutbuf)
{
   static   char acCharRec[512], *pRec=NULL;
   char     acTmp[256], acCode[32], *pTmp;
   long     lTmp, lBldgSqft;
   int      iRet, iLoop, iBeds, iFBath, iHBath, iFp;
   MPA_CHAR *pChar = (MPA_CHAR *)&acCharRec[0];

   iRet=iBeds=iFBath=iHBath=iFp=0;
   lBldgSqft=0;

   // Get first Char rec for first call
   if (!pRec)
      pRec = fgets(acCharRec, 512, fdChar);

   do
   {
      if (!pRec)
      {
         fclose(fdChar);
         fdChar = NULL;
         return 1;      // EOF
      }

      // Compare Asmt
      iLoop = memcmp(pOutbuf, pRec, iApnLen);
      if (iLoop > 0)
      {
         if (bDebug)
            LogMsg0("Skip Char rec  %.*s", iApnLen, pRec);
         pRec = fgets(acCharRec, 512, fdChar);
         lCharSkip++;
      }
   } while (iLoop > 0);

   // If not match, return
   if (iLoop)
      return 1;

   // Land value

   // Impr value

   // Yrblt
   lTmp = atoin(pChar->YrBlt, MPA_CSIZ_YRBLT);
   if (lTmp > 1700)
      memcpy(pOutbuf+OFF_YR_BLT, pChar->YrBlt, SIZ_YR_BLT);

   // BldgSqft
   lBldgSqft = atoin(pChar->BldgSqft, MPA_CSIZ_BLDGSQFT);
   if (lBldgSqft > 10)
   {
      sprintf(acTmp, "%*d", SIZ_BLDG_SF, lBldgSqft);
      memcpy(pOutbuf+OFF_BLDG_SF, acTmp, SIZ_BLDG_SF);
   }

   // Beds
   iBeds = atoin(pChar->Bed, MPA_CSIZ_BED);
   if (iBeds > 0)
   {
      sprintf(acTmp, "%*d", SIZ_BEDS, iBeds);
      memcpy(pOutbuf+OFF_BEDS, acTmp, SIZ_BEDS);
   }

   // Bath - There are only three parcels with 1/4 bath
   memcpy(acTmp, pChar->Bath, MPA_CSIZ_BATH);
   acTmp[MPA_CSIZ_BATH] = 0;

   // Clear old data
   *(pOutbuf+OFF_BATH_1Q) = ' ';
   *(pOutbuf+OFF_BATH_2Q) = ' ';
   *(pOutbuf+OFF_BATH_3Q) = ' ';
   *(pOutbuf+OFF_BATH_4Q) = ' ';
   memset(pOutbuf+OFF_BATH_H, ' ', 2);
   memset(pOutbuf+OFF_BATH_F, ' ', 2);

   if (pTmp = strchr(acTmp, '.'))
   {
      *pTmp++ = 0;
      if (*pTmp > '5')                    // If greater than .5, consider a full bath
      {
         iFBath++;
         *(pOutbuf+OFF_BATH_3Q) = '1';
      } else 
      {
         memcpy(pOutbuf+OFF_BATH_H, " 1", 2);
         if (*pTmp > '3')
            *(pOutbuf+OFF_BATH_2Q) = '1';
         else
            *(pOutbuf+OFF_BATH_1Q) = '1';
      } 
   }

   iFBath += atoi(acTmp);
   if (iFBath > 0)
   {
      sprintf(acTmp, "%*d", SIZ_BATH_F, iFBath);
      memcpy(pOutbuf+OFF_BATH_F, acTmp, SIZ_BATH_F);

      *(pOutbuf+OFF_BATH_4Q) = iFBath|0x30;
   }

   // Fireplace
   memcpy(acTmp, pChar->Fp, MPA_CSIZ_FP);
   acTmp[MPA_CSIZ_FP] = 0;
   iFp = atoin(acTmp, 1);
   if (iFp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_FIRE_PL, iFp);
      memcpy(pOutbuf+OFF_FIRE_PL, acTmp, SIZ_FIRE_PL);
   } else if (strstr(acTmp, "FP"))
   {
      iFp = 1;
      sprintf(acTmp, "%*d", SIZ_FIRE_PL, iFp);
      memcpy(pOutbuf+OFF_FIRE_PL, acTmp, SIZ_FIRE_PL);
   }

   // Heating
   if (strstr(acTmp, "/C") || !memcmp(acTmp, "CENT", 4))
      *(pOutbuf+OFF_HEAT) = 'Z';          // Central
   else if (!memcmp(acTmp, "WALL", 4))
      *(pOutbuf+OFF_HEAT) = 'D';          // Wall Furnace
   else if (!memcmp(acTmp, "FORC", 4))
      *(pOutbuf+OFF_HEAT) = 'B';          // Forced air
   else if (!memcmp(acTmp, "SOLAR", 5))
      *(pOutbuf+OFF_HEAT) = 'K';          // Solar heated
   else if (acTmp[0] == 'H')
      *(pOutbuf+OFF_HEAT) = 'Y';          // Heated

   // Pool-Spa
   if (pChar->Pool[0] == 'Y' && pChar->Spa[0] == 'Y')
      *(pOutbuf+OFF_POOL) = 'C';          // Both
   else if (pChar->Pool[0] == 'Y')
      *(pOutbuf+OFF_POOL) = 'P';          // Pool only
   else if (pChar->Spa[0] == 'Y')
      *(pOutbuf+OFF_POOL) = 'S';          // Spa only

   // Quality Class
   memcpy(acTmp, pChar->BldgCls, MPA_CSIZ_CLASS);
   acTmp[MPA_CSIZ_CLASS] = 0;
   pTmp = strchr(acTmp, ' ');
   if (pTmp) *pTmp = 0;

   acCode[0] = 0;
   if (isalpha(acTmp[0]))
   {
      *(pOutbuf+OFF_BLDG_CLASS) = acTmp[0];
      if (isdigit(acTmp[1]))
         iRet = Quality2Code((char *)&acTmp[1], acCode, NULL);
      else if (pTmp && isdigit(*(pTmp+1)))
         iRet = Quality2Code((char *)&acTmp[1], acCode, NULL);
   } else if (acTmp[0] > '0' && acTmp[0] <= '9')
      iRet = Quality2Code(acTmp, acCode, NULL);

   blankPad(acCode, SIZ_BLDG_QUAL);
   memcpy(pOutbuf+OFF_BLDG_QUAL, acCode, SIZ_BLDG_QUAL);

   // Garage
   if (!memcmp(pChar->Gar, "CP", 2))
   {
      // Car port
      *(pOutbuf+OFF_PARK_TYPE) = 'C';
   } else if (pChar->Gar[0] == 'A')
   {
      // Attached
      *(pOutbuf+OFF_PARK_TYPE) = 'A';
      if (isdigit(pChar->Gar[1]))
         *(pOutbuf+OFF_PARK_SPACE) = pChar->Gar[1];
   } else if (pChar->Gar[0] == 'D')
   {
      // Detached
      *(pOutbuf+OFF_PARK_TYPE) = 'D';
      if (isdigit(pChar->Gar[1]) )
         *(pOutbuf+OFF_PARK_SPACE) = pChar->Gar[1];
   }

   // Zoning
   if (pChar->Zoning[0] > ' ')
   {
      memcpy(pOutbuf+OFF_ZONE, pChar->Zoning, MPA_CSIZ_ZONING);
      if (*(pOutbuf+OFF_ZONE_X1) == ' ')
         vmemcpy(pOutbuf+OFF_ZONE_X1, pChar->Zoning, SIZ_ZONE_X1, MPA_CSIZ_ZONING);
}

   do {
      // Get next Char rec - skip duplicate one
      pRec = fgets(acCharRec, 512, fdChar);
      if (!pRec)
      {
         fclose(fdChar);
         fdChar = NULL;
         break;
      }
   } while (!memcmp(pOutbuf, acCharRec, iApnLen));

   lCharMatch++;

   return 0;
}

/******************************** Mpa_MergeOwner *****************************
 *
 * Return 0 if successful, >0 if error
 *
 *****************************************************************************/

void Mpa_MergeOwner(char *pOutbuf, char *pRollRec, bool bRoll = true)
{
   int   iTmp, iTmp1;
   char  acTmp[128];
   char  acName1[128];
   char  *pTmp, *pTmp1;

   OWNER    myOwner;
   MPA_ROLL *pRec;

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "0020400320", 10) )
   //   iTmp = 0;
#endif

   // Clear output buffer if needed
   removeNames(pOutbuf, false);
   if (bRoll)
   {
      pRec = (MPA_ROLL *)pRollRec;
      memcpy(acName1, pRec->Owner1, MPA_SIZ_OWNER1);
      acName1[MPA_SIZ_OWNER1] = 0;
   } else
      strcpy(acName1, pRollRec);

   blankRem(acName1, MPA_SIZ_OWNER1);

   // Remove multiple spaces
   pTmp = acName1;
   iTmp = iTmp1 = 0;
   while (*pTmp)
   {
      // Mark name with numeric value
      if (isdigit(*pTmp))
         iTmp1++;

      acTmp[iTmp++] = *pTmp;
      // Remove multi-space
      if (*pTmp == ' ')
         while (*pTmp == ' ') pTmp++;
      else
         pTmp++;

      // Remove ',' and single quote too
      if (*pTmp == ',' || *pTmp == 39)
         pTmp++;

   }

   // Keep a space at the end so we can check for " TR "
   acTmp[iTmp] = 0;

   // Update vesting
   iTmp = updateVesting(myCounty.acCntyCode, acName1, pOutbuf+OFF_VEST, pOutbuf+OFF_ETAL_FLG);


   if (pTmp = strchr(acTmp, '/'))
   {
      *pTmp++ = 0;
      if (!strstr(pTmp, " ETAL"))
      {
         sprintf(acName1, "%s & %s", acTmp, pTmp);
         strcpy(acTmp, acName1);
      }
   }

   // Drop thing in parenthesis
   if (pTmp = strchr(acTmp, '('))
      *pTmp = 0;

   // Check TR
   if (pTmp=strstr(acTmp, " CO-TR "))
      memcpy(pTmp, "      ", 6);
   if (pTmp=strstr(acTmp, " TR "))
      memcpy(pTmp, "   ", 3);

   // Drop everything from these words
   if ((pTmp=strstr(acTmp, " ETAL")) || (pTmp=strstr(acTmp, " ET AL")) || (pTmp=strstr(acTmp, " TR")))
      *pTmp = 0;

   if ((pTmp=strstr(acTmp, " TRETAL")) || (pTmp=strstr(acTmp, " TR ET")))
      *(pTmp+3) = 0;

   blankRem(acTmp);

   // Save name1
   strcpy(acName1, acTmp);

   // Leave name as is if number present
   if (iTmp1 > 0)
   {
      vmemcpy(pOutbuf+OFF_NAME1, acTmp, SIZ_NAME1);
      vmemcpy(pOutbuf+OFF_NAME_SWAP, acTmp, SIZ_NAME_SWAP);
      return;
   }

   if ((pTmp=strstr(acTmp, " CO-TR"))  || (pTmp=strstr(acTmp, " LIFE EST")) ||
       (pTmp=strstr(acTmp, " EST OF")) || (pTmp=strstr(acTmp, " ESTATE OF")) )
      *pTmp = 0;

   // Save data to append later
   if (pTmp1=strstr(acTmp, "TRUST"))
   {
      if ((pTmp=strstr(acTmp, " FAM"))   || (pTmp=strstr(acTmp, " REVOC")) ||
          (pTmp=strstr(acTmp, " INDIV")) || (pTmp=strstr(acTmp, " AMENDED ")) ||
          (pTmp=strstr(acTmp, " LIV")) )
         *pTmp = 0;
      else
         *pTmp1 = 0;
   }

   if ((pTmp=strstr(acTmp, " FAM RV TR"))  || (pTmp=strstr(acTmp, " REV TR")) ||
       (pTmp=strstr(acTmp, " RV TR"))      || (pTmp=strstr(acTmp, " FAM LIV TR")) ||
       (pTmp=strstr(acTmp, " FAM TR"))     || (pTmp=strstr(acTmp, " LIV TR")) ||
       (pTmp=strstr(acTmp, " RD LAND TR")) || (pTmp=strstr(acTmp, " MARITAL TR")) ||
       (pTmp=strstr(acTmp, " LAND TR"))    || (pTmp=strstr(acTmp, " DECEDENTS TR ET")) )
      *pTmp = 0;

   // Now parse owners
   acTmp[63] = 0;
   iTmp = splitOwner(acTmp, &myOwner, 0);
   if (iTmp >= 0)
      vmemcpy(pOutbuf+OFF_NAME_SWAP, myOwner.acSwapName, SIZ_NAME_SWAP);
   else
      vmemcpy(pOutbuf+OFF_NAME_SWAP, acName1, SIZ_NAME_SWAP);

   vmemcpy(pOutbuf+OFF_NAME1, acName1, SIZ_NAME1);
}

/********************************* Mpa_MergeSAdr *****************************
 *
 * Return 0 if successful, >0 if error
 *
 *****************************************************************************/

void Mpa_MergeSAdr(char *pOutbuf, char *pRollRec, bool bRoll = true)
{
   MPA_ROLL *pRec;
   char     acTmp[256], acAddr1[128], acCode[64], *pTmp, *pTmp1;

   int      iTmp, lStrNum;
   ADR_REC  sSitusAdr;

   if (bRoll)
   {
      pRec = (MPA_ROLL *)pRollRec;
      memcpy(acTmp, pRec->Situs_Addr, MPA_SIZ_SITUS_ADDR);
      acTmp[MPA_SIZ_SITUS_ADDR] = 0;
   } else
   {
      strcpy(acTmp, pRollRec);
   }

   // Situs
   acAddr1[0] = 0;
   memset((void *)&sSitusAdr, 0, sizeof(ADR_REC));

#ifdef _DEBUG 
   //if (!memcmp(pOutbuf, "0103500080", 9) || !memcmp(pOutbuf, "0172200270", 9) )
   //   iTmp = 0;
#endif
   if (acTmp[0] > ' ')
   {
      // Ignore unassigned situs
      if (pTmp = strstr(acTmp, "UNASSIGNED"))
      {
         if (pTmp1 = strchr(pTmp, ' '))
            parseAdr2(&sSitusAdr, pTmp1+1);
         else
            return;
      } else
      {
         replChar(acTmp, '*', ' ');

         // Have to remove everything in parenthesis
         pTmp = strchr(acTmp, '(');
         if (pTmp)
         {
            if (pTmp1 = strchr(pTmp, ')') )
               memset(pTmp, ' ', pTmp1-pTmp+1);
            else if (pTmp1 = strchr(pTmp, ' '))
               memset(pTmp, ' ', pTmp1-pTmp);
            else
               iTmp = 0;
         }

         iTmp = replChar(acTmp, '-', ' ');
         blankRem(acTmp, MPA_SIZ_SITUS_ADDR);

         parseAdr1_5(&sSitusAdr, acTmp, pOutbuf);
         iTmp = sprintf(acAddr1, "%s", sSitusAdr.strNum);
         if (isdigit(sSitusAdr.Xtras[0]))
         {
            lStrNum = atol(sSitusAdr.Xtras);
            if (lStrNum > sSitusAdr.lStrNum)
               iTmp = sprintf(acAddr1, "%s-%s", sSitusAdr.strNum, sSitusAdr.Xtras);
            else if ((sSitusAdr.lStrNum-lStrNum) < 11 && (sSitusAdr.lStrNum-lStrNum) > 0)
               iTmp = sprintf(acAddr1, "%s-%s", sSitusAdr.Xtras, sSitusAdr.strNum);
            else if (lStrNum == sSitusAdr.lStrNum && isalpha(sSitusAdr.Xtras[strlen(sSitusAdr.Xtras)-1]))
               iTmp = sprintf(acAddr1, "%s-%s", sSitusAdr.strNum, sSitusAdr.Xtras);
         }

         if ((sSitusAdr.lStrNum > 0) && (sSitusAdr.strName[0] > ' '))
         {
            memset(pOutbuf+OFF_S_HSENO, ' ', SIZ_S_HSENO);
            vmemcpy(pOutbuf+OFF_S_HSENO, acAddr1, SIZ_S_HSENO, iTmp);

            memcpy(pOutbuf+OFF_S_STRNUM, sSitusAdr.strNum, strlen(sSitusAdr.strNum));

            sprintf(acTmp, "%s %s %s %s %s", acAddr1, sSitusAdr.strDir,
               sSitusAdr.strName, sSitusAdr.strSfx, sSitusAdr.Unit);
            iTmp = blankRem(acTmp);
            vmemcpy(pOutbuf+OFF_S_ADDR_D, acTmp, SIZ_S_ADDR_D, iTmp);

            if (sSitusAdr.strDir[0] > ' ')
               memcpy(pOutbuf+OFF_S_DIR, sSitusAdr.strDir, strlen(sSitusAdr.strDir));

            blankPad(sSitusAdr.strName, SIZ_S_STREET);
            memcpy(pOutbuf+OFF_S_STREET, sSitusAdr.strName, SIZ_S_STREET);

            if (sSitusAdr.SfxCode[0] > ' ')
               memcpy(pOutbuf+OFF_S_SUFF, sSitusAdr.SfxCode, strlen(sSitusAdr.SfxCode));

            if (sSitusAdr.Unit[0] > ' ')
               memcpy(pOutbuf+OFF_S_UNITNO, sSitusAdr.Unit, strlen(sSitusAdr.Unit));

         }
      }

      City2Code(sSitusAdr.City, acCode, pOutbuf);
      if (acCode[0] > ' ')
      {
         memcpy(pOutbuf+OFF_S_CITY, acCode, strlen(acCode));
         memcpy(pOutbuf+OFF_S_ST, "CA",2);
         memcpy(pOutbuf+OFF_S_ZIP, sSitusAdr.Zip, 5);
         strcpy(acTmp, sSitusAdr.City);
         strcat(acTmp, " CA");

         memcpy(pOutbuf+OFF_S_CTY_ST_D, acTmp, strlen(acTmp));
      }
   }

}

// 07/26/2011
void Mpa_MergeSAdr_Old(char *pOutbuf, char *pRollRec, bool bRoll = true)
{
   MPA_ROLL *pRec;
   char     acTmp[256], acAddr1[128], acCode[64];

   int      iTmp;
   ADR_REC  sSitusAdr;

   if (bRoll)
   {
      pRec = (MPA_ROLL *)pRollRec;
      memcpy(acTmp, pRec->Situs_Addr, MPA_SIZ_SITUS_ADDR);
      acTmp[MPA_SIZ_SITUS_ADDR] = 0;
   } else
   {
      strcpy(acTmp, pRollRec);
   }

   // Situs
   acAddr1[0] = 0;
   memset((void *)&sSitusAdr, 0, sizeof(ADR_REC));

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "015310041", 9) )
   //  iTmp = 0;
#endif
   if (acTmp[0] > ' ')
   {
      replChar(acTmp, '*', ' ');
      replChar(acTmp, '(', ' ');
      replChar(acTmp, ')', ' ');
      replChar(acTmp, '-', ' ');
      blankRem(acTmp, MPA_SIZ_SITUS_ADDR);

      parseAdr1_5(&sSitusAdr, acTmp, pOutbuf);

      if ((sSitusAdr.lStrNum > 0) && (sSitusAdr.strName[0] > ' '))
      {
         sprintf(acAddr1, "%d       ", sSitusAdr.lStrNum);
         memcpy(pOutbuf+OFF_S_STRNUM, acAddr1, SIZ_S_STRNUM);

         sprintf(acAddr1, "%d %s %s %s %s", sSitusAdr.lStrNum, sSitusAdr.strDir,
            sSitusAdr.strName, sSitusAdr.strSfx, sSitusAdr.Unit);
         blankRem(acAddr1);
         iTmp = strlen(acAddr1);
         if (iTmp > SIZ_S_ADDR_D) 
            iTmp = SIZ_S_ADDR_D;
         memcpy(pOutbuf+OFF_S_ADDR_D, acAddr1, iTmp);

         if (sSitusAdr.strDir[0] > ' ')
            memcpy(pOutbuf+OFF_S_DIR, sSitusAdr.strDir, strlen(sSitusAdr.strDir));

         blankPad(sSitusAdr.strName, SIZ_S_STREET);
         memcpy(pOutbuf+OFF_S_STREET, sSitusAdr.strName, SIZ_S_STREET);

         if (sSitusAdr.SfxCode[0] > ' ')
            memcpy(pOutbuf+OFF_S_SUFF, sSitusAdr.SfxCode, strlen(sSitusAdr.SfxCode));

         if (sSitusAdr.Unit[0] > ' ')
            memcpy(pOutbuf+OFF_S_UNITNO, sSitusAdr.Unit, strlen(sSitusAdr.Unit));

      }
      memcpy(pOutbuf+OFF_S_ST, sSitusAdr.State, 2);
      memcpy(pOutbuf+OFF_S_ZIP, sSitusAdr.Zip, 5);

      City2Code(sSitusAdr.City, acCode, pOutbuf);
      if (acCode[0] > ' ')
      {
         memcpy(pOutbuf+OFF_S_CITY, acCode, strlen(acCode));
         memcpy(pOutbuf+OFF_S_ST, "CA",2);
         strcpy(acTmp, sSitusAdr.City);
         strcat(acTmp, " CA");

         memcpy(pOutbuf+OFF_S_CTY_ST_D, acTmp, strlen(acTmp));
      }
   }

}

/********************************** Mpa_MergeMAdr ****************************
 *
 * Return 0 if successful, >0 if error
 *
 *****************************************************************************/

void Mpa_MergeMAdr(char *pOutbuf, char *pRollRec)
{
   char     *pAddr1, acTmp[256], acAddr1[64];
   int      iTmp;
   bool     bNoMail = false;

   ADR_REC  sMailAdr;
   MPA_ROLL *pRec;

   pRec = (MPA_ROLL *)pRollRec;

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "003230241", 9) )
   //   lTmp = 0;
#endif

   // Clear old Mailing
   removeMailing(pOutbuf, false);

   // Check for blank address
   if (memcmp(pRec->Mail_St, "     ", 5))
   {
      memcpy(acAddr1, pRec->Mail_St, MPA_SIZ_MAIL_ST);
      pAddr1 = myTrim(acAddr1, MPA_SIZ_MAIL_ST);

      // Parsing mail address
      memset((void *)&sMailAdr, 0, sizeof(ADR_REC));
      parseMAdr1(&sMailAdr, acAddr1);

      if (sMailAdr.lStrNum > 0)
      {
         iTmp = sprintf(acTmp, "%d", sMailAdr.lStrNum);
         memcpy(pOutbuf+OFF_M_STRNUM, acTmp, iTmp);
         if (sMailAdr.strSub[0] > '0')
         {
            sprintf(acTmp, "%s  ", sMailAdr.strSub);
            memcpy(pOutbuf+OFF_M_STR_SUB, acTmp, SIZ_M_STR_SUB);
         }
      }
      blankPad(sMailAdr.strDir, SIZ_M_DIR);
      memcpy(pOutbuf+OFF_M_DIR, sMailAdr.strDir, SIZ_M_DIR);
      blankPad(sMailAdr.strName, SIZ_M_STREET);
      memcpy(pOutbuf+OFF_M_STREET, sMailAdr.strName, SIZ_M_STREET);
      blankPad(sMailAdr.strSfx, SIZ_M_SUFF);
      memcpy(pOutbuf+OFF_M_SUFF, sMailAdr.strSfx, SIZ_M_SUFF);
      if (sMailAdr.Unit[0] > ' ')
      {
         if (sMailAdr.Unit[0] == '#')
            memcpy(pOutbuf+OFF_M_UNITNO, sMailAdr.Unit, strlen(sMailAdr.Unit));
         else
         {
            *(pOutbuf+OFF_M_UNITNO) = '#';
            memcpy(pOutbuf+OFF_M_UNITNO+1, sMailAdr.Unit, strlen(sMailAdr.Unit));
         }
      }

      memcpy(pOutbuf+OFF_M_ADDR_D, acAddr1, strlen(acAddr1));

      if (pRec->Mail_City[0] > ' ')
      {
         memcpy(pOutbuf+OFF_M_CITY, pRec->Mail_City, SIZ_M_CITY);
         memcpy(pOutbuf+OFF_M_ST, pRec->Mail_State, SIZ_M_ST);

         sprintf(acAddr1, "%.*s %.2s", SIZ_M_CITY, pRec->Mail_City, pRec->Mail_State);
         blankRem(acAddr1);
         memcpy(pOutbuf+OFF_M_CTY_ST_D, acAddr1, strlen(acAddr1));
      }

      iTmp = atoin(pRec->Mail_Zip, 5);
      if (iTmp > 10001)
      {
         memcpy(pOutbuf+OFF_M_ZIP, pRec->Mail_Zip, SIZ_M_ZIP);
         memcpy(pOutbuf+OFF_M_ZIP4, (char *)&pRec->Mail_Zip[5], SIZ_M_ZIP4);
      } else
      {
         memcpy(pOutbuf+OFF_M_ZIP, BLANK32, SIZ_M_ZIP+SIZ_M_ZIP4);
      }
   }
}

void Mpa_MergeMAdr(char *pOutbuf, char *pStr, char *pCity, char *pSt, char *pZip)
{
   char     *pAddr1, acTmp[256], acAddr1[64];
   int      iTmp;
   bool     bNoMail = false;

   ADR_REC  sMailAdr;

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "003230241", 9) )
   //   lTmp = 0;
#endif

   // Clear old Mailing
   memset(pOutbuf+OFF_M_STRNUM, ' ', SIZ_M_ADDR);
   memset(pOutbuf+OFF_M_ADDR_D, ' ', SIZ_M_ADDRB_D);

   // Check for blank address
   if (*pStr > ' ')
   {
      strcpy(acAddr1, pStr);
      pAddr1 = myTrim(acAddr1);

      // Parsing mail address
      memset((void *)&sMailAdr, 0, sizeof(ADR_REC));
      parseMAdr1(&sMailAdr, acAddr1);

      if (sMailAdr.lStrNum > 0)
      {
         sprintf(acTmp, "%*d", SIZ_M_STRNUM, sMailAdr.lStrNum);
         memcpy(pOutbuf+OFF_M_STRNUM, acTmp, SIZ_M_STRNUM);
         if (sMailAdr.strSub[0] > '0')
         {
            sprintf(acTmp, "%s  ", sMailAdr.strSub);
            memcpy(pOutbuf+OFF_M_STR_SUB, acTmp, SIZ_M_STR_SUB);
         }
      }
      blankPad(sMailAdr.strDir, SIZ_M_DIR);
      memcpy(pOutbuf+OFF_M_DIR, sMailAdr.strDir, SIZ_M_DIR);
      blankPad(sMailAdr.strName, SIZ_M_STREET);
      memcpy(pOutbuf+OFF_M_STREET, sMailAdr.strName, SIZ_M_STREET);
      blankPad(sMailAdr.strSfx, SIZ_M_SUFF);
      memcpy(pOutbuf+OFF_M_SUFF, sMailAdr.strSfx, SIZ_M_SUFF);
      if (sMailAdr.Unit[0] > ' ')
      {
         if (sMailAdr.Unit[0] == '#')
            memcpy(pOutbuf+OFF_M_UNITNO, sMailAdr.Unit, strlen(sMailAdr.Unit));
         else
         {
            *(pOutbuf+OFF_M_UNITNO) = '#';
            memcpy(pOutbuf+OFF_M_UNITNO+1, sMailAdr.Unit, strlen(sMailAdr.Unit));
         }
      }

      memcpy(pOutbuf+OFF_M_ADDR_D, acAddr1, strlen(acAddr1));

      if (*pCity > ' ')
      {
         memcpy(pOutbuf+OFF_M_CITY, pCity, strlen(pCity));
         memcpy(pOutbuf+OFF_M_ST, pSt, strlen(pSt));

         sprintf(acAddr1, "%s %s", pCity, pSt);
         blankRem(acAddr1);
         memcpy(pOutbuf+OFF_M_CTY_ST_D, acAddr1, strlen(acAddr1));
      }

      iTmp = atoin(pZip, 5);
      if (iTmp > 10001)
      {
         memcpy(pOutbuf+OFF_M_ZIP, pZip, strlen(pZip));
      } else
      {
         memcpy(pOutbuf+OFF_M_ZIP, BLANK32, SIZ_M_ZIP+SIZ_M_ZIP4);
      }
   }
}

/********************************** Mpa_FmtDocNum *****************************
 *
 *  Pre 1987:   9999999YY or 999999YY or 999999
 *  1987-1998:  YY9999
 *  1999-2010:  YYY9999 (i.e. 1990102=0102 1999, 2030405=0405 2003)
 *  2011-now:   YYYY9999
 *
 * Return length of output string.
 *
 *****************************************************************************/

int Mpa_FmtDocNum(char *pResult, char *pDocNum, char *pDocDate, char *pApn)
{
   int   iRet, iDocLen, lTmp, lDate;
   char  *pTmp, *pDoc, acDoc[32];

   iDocLen = blankRem(pDocNum, SIZ_TRANSFER_DOC);
   lDate = atol(pDocDate);
   if (lDate < 19990101)
   {
      lDate = atoin(pDocDate, 4);
      if (lDate < 1987)
      {
         if (!memcmp(pDocDate+2, pDocNum+(iDocLen-2), 2))
         {
            // 9999999YY or 999999YY or 999999
            lTmp = atoin(pDocNum, iDocLen-2);
            iRet = sprintf(pResult, "%d        ", lTmp);
         } else
            iRet = sprintf(pResult, "%s        ", pDocNum);
      } else
      {
         // 1987-1998: YYYY999999 or YY9999 or YYY9999
         if (!memcmp(pDocDate, pDocNum, 4))
         {
            lTmp = atol(pDocNum+4);
            iRet = sprintf(pResult, "%.4s%.6d  ", pDocNum, lTmp);
         } else if (!memcmp(pDocDate+2, pDocNum, 2))
         {
            lTmp = atol(pDocNum+2);
            iRet = sprintf(pResult, "%.4s%.6d  ", pDocDate, lTmp);
         } else if (!memcmp(pDocDate+2, pDocNum+1, 2))
         {
            lTmp = atol(pDocNum+3);
            iRet = sprintf(pResult, "%.4s%.6d  ", pDocDate, lTmp);
         } else
         {
            LogMsg0("*** Bad Doc: APN=%s, DocNum=%s, DocDate=%.8s", pApn, pDocNum, pDocDate);
            iRet = 0;
         }
      } 
      return iRet;
   }

   strcpy(acDoc, pDocNum);
   pDoc = &acDoc[0];
   if (iDocLen > 8 && (pTmp = strchr(acDoc, '/')))
   {
      *pTmp++ = 0;
      iDocLen = strlen(pDoc);
      iRet = strlen(pTmp);
      if (iRet == iDocLen)
         pDoc = pTmp;
   }

   // 2056466-67 9/26/05
   if (pTmp = strchr(pDoc, '-'))
   {
      *pTmp = 0;
      iDocLen = strlen(pDoc);
   }

   switch (iDocLen)
   {
      case 9:
         // 013525572 6/4/72
         if (!memcmp(pDocDate+2, pDoc+7, 2) )
         {
            lTmp = atoin(pDoc,7);
            iRet = sprintf(pResult, "%.4s%.6d  ", pDocDate, lTmp);
         } else
         {
            //iRet = sprintf(pResult, "%s", pDoc);
            LogMsg0("*** 9-Drop Doc: APN=%s, DocNum=%s, DocDate=%s", pApn, pDocNum, pDocDate);
            iRet = 0;
         }
         break;
      case 8:
         // 20141835 6/4/14
         if (lDate >= 20110101)
         {
            lTmp = atol(pDoc+4);
            iRet = sprintf(pResult, "%.4s%.6d  ", pDoc, lTmp);
            if (memcmp(pDocDate, pDoc, 4))
               LogMsg0("+++ Unmatch Doc: APN=%s, DocNum=%s, DocDate=%s", pApn, pDocNum, pDocDate);
         } else
         {
            // Bad case: 20134309 10/18/03
            //iRet = sprintf(pResult, "%s", pDoc);
            LogMsg0("*** 8-Drop Doc: APN=%s, DocNum=%s, DocDate=%s", pApn, pDocNum, pDocDate);
            iRet = 0;
         }
         break;
      case 7:
         // 2040638 02/06/04
         if (!memcmp(pDocDate+2, pDoc+1, 2) )
         {
            lTmp = atol(pDoc+3);
            iRet = sprintf(pResult, "%.4s%.6d  ", pDocDate, lTmp);
         } else
         {
            LogMsg0("*** 7-Drop Doc: APN=%s, DocNum=%s, DocDate=%s", pApn, pDocNum, pDocDate);
            iRet = 0;
            //lTmp = atol(pDoc+3);
            //iRet = sprintf(pResult, "%.4s%.6d    ", pDocDate, lTmp);
         }
         break;
      case 6:
         // 955305 11/16/95
         if (!memcmp(pDocDate+2, pDoc, 2) )
         {
            lTmp = atol(pDoc+2);
            iRet = sprintf(pResult, "%.4s%.6d  ", pDocDate, lTmp);
         } else if (!memcmp(pDocDate+2, pDoc+1, 2) )
         {
            lTmp = atol(pDoc+3);
            iRet = sprintf(pResult, "%.4s%.6d  ", pDocDate, lTmp);
         } else
         {
            LogMsg0("*** 6-Drop Doc: APN=%s, DocNum=%s, DocDate=%s", pApn, pDocNum, pDocDate);
            iRet = 0;
         }
         break;
      default:
         iRet = sprintf(pResult, "%s        ", pDoc);
         break;
   }

   if (iRet > SIZ_TRANSFER_DOC)
      iRet = SIZ_TRANSFER_DOC;

   return iRet;
}

/********************************** Mpa_MergeRoll *****************************
 *
 * Return 0 if successful, >0 if error
 *
 *****************************************************************************/

int Mpa_MergeRoll(char *pOutbuf, char *pRollRec, int iFlag)
{
   char     acTmp[256], acTmp1[256], *apItems[16], *pTmp;
   char     acDate[256], acDoc[32], acFmtDoc[32];
   int      iRet, iCnt, iLen, iDateIdx, iDocIdx;
   long     lTmp;

   MPA_ROLL *pRec = (MPA_ROLL *)pRollRec;
   if (iFlag & CREATE_R01)
   {
      // Clear output buffer
      memset(pOutbuf, ' ', iRecLen);

      // Start copying data
      memcpy(pOutbuf, pRec->Apn, MPA_SIZ_APN);

      // Format APN
      iRet = formatApn(pRec->Apn, acTmp, &myCounty);
      memcpy(pOutbuf+OFF_APN_D, acTmp, iRet);

      // Create MapLink and output new record
      iRet = formatMapLink(pRec->Apn, acTmp, &myCounty);
      memcpy(pOutbuf+OFF_MAPLINK, acTmp, iRet);

      // Create index map link
      if (getIndexPage(acTmp, acTmp1, &myCounty))
         memcpy(pOutbuf+OFF_IMAPLINK, acTmp1, iRet);

      memcpy(pOutbuf+OFF_CO_NUM, "22MPA", 5);

      // Year assessed
      memcpy(pOutbuf+OFF_YR_ASSD, myCounty.acYearAssd, 4);

      long lSqFtLand=0;
      double dAcreAge = atofn(pRec->Acres, MPA_SIZ_ACRES);
      if (dAcreAge > 0.0)
      {
         dAcreAge /= 10.0;
         sprintf(acTmp, "%*d", SIZ_LOT_ACRES, (long)(dAcreAge));
         memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);

         lSqFtLand = (long)((dAcreAge * SQFT_FACTOR_1000));
         sprintf(acTmp, "%*d", SIZ_LOT_SQFT, lSqFtLand);
         memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);
      }

      // Land
      long lLand = atoin(pRec->Land,MPA_SIZ_LAND);
      if (lLand > 0)
      {
         sprintf(acTmp, "%*d", SIZ_LAND, lLand);
         memcpy(pOutbuf+OFF_LAND, acTmp, SIZ_LAND);
      }

      // Improve
      long lImpr = atoin(pRec->Improvements, MPA_SIZ_IMPROVEMENTS);
      if (lImpr > 0)
      {
         sprintf(acTmp, "%*d", SIZ_IMPR, lImpr);
         memcpy(pOutbuf+OFF_IMPR, acTmp, SIZ_IMPR);
      }

      long lTimber = atoin(pRec->Timber, MPA_SIZ_TIMBER);
      long lTree   = atoin(pRec->Trees_Vines, MPA_SIZ_TREES_VINES);
      long lWLand  = atoin(pRec->Will_Land, MPA_SIZ_WILL_LAND);
      long lWImpr  = atoin(pRec->Will_Market, MPA_SIZ_WILL_MARKET);
      lTmp = lTimber+lWLand+lWImpr+lTree;
      if (lTmp > 0)
      {
         sprintf(acTmp, "%*d", SIZ_OTHER, lTmp);
         memcpy(pOutbuf+OFF_OTHER, acTmp, SIZ_OTHER);

         if (lTimber > 0)
         {
            sprintf(acTmp, "%d          ", lTimber);
            memcpy(pOutbuf+OFF_TIMBER_VAL, acTmp, SIZ_TIMBER_VAL);
         }
         if (lTree > 0)
         {
            sprintf(acTmp, "%d          ", lTree);
            memcpy(pOutbuf+OFF_TREEVINES, acTmp, SIZ_TREEVINES);
         }
         if (lWLand > 0)
         {
            sprintf(acTmp, "%d          ", lWLand);
            memcpy(pOutbuf+OFF_CLCA_LAND, acTmp, SIZ_CLCA_LAND);
         }
         if (lWImpr > 0)
         {
            sprintf(acTmp, "%d          ", lWImpr);
            memcpy(pOutbuf+OFF_CLCA_IMPR, acTmp, SIZ_CLCA_IMPR);
         }
      }

      // Gross total
      lTmp += (lLand+lImpr);
      if (lTmp > 0)
      {
         sprintf(acTmp, "%*d", SIZ_GROSS, lTmp);
         memcpy(pOutbuf+OFF_GROSS, acTmp, SIZ_GROSS);
      }

      // Ratio
      if (lImpr > 0)
      {
         sprintf(acTmp, "%*d", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
         memcpy(pOutbuf+OFF_RATIO, acTmp, SIZ_RATIO);
      }
   }

   // Status is always 0
   pRec->Status[0] = 0;

   // TRA
   lTmp = atoin(pRec->Tax_Area, MPA_SIZ_TAX_AREA);
   if (lTmp > 0)
   {
      iRet = sprintf(acTmp, "%.6d", lTmp);
      memcpy(pOutbuf+OFF_TRA, acTmp, iRet);
   }

   // Legal
   if (pRec->Legal[0] > ' ')
      memcpy(pOutbuf+OFF_LEGAL, pRec->Legal, MPA_SIZ_LEGAL);

   // Zoning
   if (pRec->Zoning[0] == 'N')
   {
      pRec->Use_Zone[MPA_SIZ_USE_ZONE] = 0;

      // Translate to ZONING
      if (pTmp = GetZoning(pRec->Use_Zone))
      {
         iLen = strlen(pTmp);
         memset(pOutbuf+OFF_ZONE, ' ', SIZ_ZONE);
         memcpy(pOutbuf+OFF_ZONE, pTmp, iLen);

         if (*(pOutbuf+OFF_ZONE_X1) == ' ')
            vmemcpy(pOutbuf+OFF_ZONE_X1, pTmp, SIZ_ZONE_X1, iLen);
      }
   }

   // Use code
   /*
   if (pRec->Property_Use[0] > ' ')
   {
      memcpy(pOutbuf+OFF_USE_CO, pRec->Property_Use, MPA_SIZ_PROPERTY_USE);
   
      // Translate to standard use
      iRet = updateStdUse(pOutbuf+OFF_USE_STD, pRec->Property_Use, MPA_SIZ_PROPERTY_USE);
      if (!iRet)
         LogMsg("*** Unknown UseCode %.*s", MPA_SIZ_PROPERTY_USE, pRec->Property_Use);
   }
   */
   memset(pOutbuf+OFF_USE_CO, ' ', SIZ_USE_CO);
   memcpy(pOutbuf+OFF_USE_STD, USE_UNASGN, SIZ_USE_STD);

   // CareOf
   updateCareOf(pOutbuf, pRec->Careof, MPA_SIZ_CAREOF);
   acDate[0] = 0;

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "0112200050", 9)   )
   //   iRet = 0;
#endif

   // Transfer
   // Pre 1987:   9999999YY or 999999YY
   // 1987-1998:  YY9999
   // 1999-2010:  YYY9999 (i.e. 1990102=0102 1999, 2030405=0405 2003)
   // 2011-now:   YYYY9999
   // 2002445&46 06/21/00: create two docs 2445 & 2446
   memcpy(acTmp, pRec->Deed_Ref, MPA_SIZ_DEED_REF);
   replChar(acTmp, '&', ' ', MPA_SIZ_DEED_REF);
   //replChar(acTmp, '?', '/', MPA_SIZ_DEED_REF);
   acTmp[MPA_SIZ_DEED_REF] = 0;
   if (pTmp = strchr(acTmp, '('))
   {
      while (*pTmp && *pTmp != ')')
         *pTmp++ = ' ';
      if (*pTmp)
         *pTmp = ' ';
   }

   blankRem(acTmp, MPA_SIZ_DEED_REF);
   if (isdigit(acTmp[0]))
   {
      // If last token is not digit, drop it
      iCnt = ParseString(acTmp, ' ', 16, apItems);
      if (!isdigit(*apItems[iCnt-1]))
         iCnt--;
   } else
      iCnt = 0;

   // 1991415 & 1991416 4/5/99
   // 2025481 (2026279 10/24/02
   // 951406-3/95 953327-7/95
   if (iCnt == 1)
   {
      strcpy(acTmp1, apItems[0]);

MPA_One_Token:
      if (isdigit(acTmp1[0]))
      {
         // 028639186 = 286-391 1986
         // 022464581 = 224-645 1981
         // 12152970  = 121-529 1970
         // 911234    = 1234    1991
         // 112592    = 112-592
         // 218/262   = 218-262
         // 1984445/6 9/30/98
         // 2003750/51 09/06/00
         iLen = strlen(acTmp1);
         if (iLen > 7 && iLen < 14 && (pTmp = strchr(acTmp1, '/')))
         {
            *pTmp = 0;
            iLen = strlen(acTmp1);
         }
         if (iLen == 9)
         {
            // 028639186 = 286-391 1986
            sprintf(acDate, "19%.2s     ", &acTmp1[7]);
            memcpy(pOutbuf+OFF_TRANSFER_DT, acDate, SIZ_TRANSFER_DT);
            lTmp = atoin(acTmp1, 7);
            sprintf(acDoc, "%d         ", lTmp);
            memcpy(pOutbuf+OFF_TRANSFER_DOC, acDoc, SIZ_TRANSFER_DOC);
         } else if (iLen == 8)
         {
            // 28639186 = 286-391 1986
            // 20114492 = 2011004492
            lTmp = atoin(acTmp1, 4);
            if (lTmp > 2010 && lTmp <= lToyear)
            {
               sprintf(acDate, "%.4s       ", acTmp1);
            } else
            {
               sprintf(acDate, "19%.2s     ", &acTmp1[6]);
            }
            lTmp = atol(&acTmp1[4]);
            sprintf(acDoc,  "%.4s%.6d   ", acTmp1, lTmp);
            memcpy(pOutbuf+OFF_TRANSFER_DT, acDate, SIZ_TRANSFER_DT);
            memcpy(pOutbuf+OFF_TRANSFER_DOC, acDoc, SIZ_TRANSFER_DOC);
         } else if (iLen == 7)
         {
            if (acTmp1[3] == '/' || acTmp1[3] == '-')
            {
               // 218/262   = 218-262
               sprintf(acDoc, "%.3s-%.3s           ", acTmp1, &acTmp1[4]);
               memcpy(pOutbuf+OFF_TRANSFER_DOC, acDoc, SIZ_TRANSFER_DOC);
               memset(pOutbuf+OFF_TRANSFER_DT, ' ', SIZ_TRANSFER_DT);
            } else
            {
               // 1984445
               if (acTmp1[0] == '1')
                  sprintf(acDate, "19%.2s           ", &acTmp1[1]);
               else if (acTmp1[0] == '2')
                  sprintf(acDate, "20%.2s           ", &acTmp1[1]);
               else
                  acDate[0] = 0;

               if (acDate[0] >= '0')
               {
                  lTmp = atol(&acTmp1[3]);
                  sprintf(acDoc, "%.4s%.6d    ", acDate, lTmp);
                  memcpy(pOutbuf+OFF_TRANSFER_DOC, acDoc, SIZ_TRANSFER_DOC);
                  memcpy(pOutbuf+OFF_TRANSFER_DT, acDate, SIZ_TRANSFER_DT);
               } else
               {              
                  // 9233474
                  sprintf(acDoc, "%s              ", acTmp1);
                  memcpy(pOutbuf+OFF_TRANSFER_DOC, acDoc, SIZ_TRANSFER_DOC);
                  memset(pOutbuf+OFF_TRANSFER_DT, ' ', SIZ_TRANSFER_DT);
               }
            }
         } else if (iLen == 6)
         {
            if (pTmp = strchr(acTmp1, '/'))
            {
               // 286/64
               *pTmp++ = 0;
               sprintf(acDoc, "%s-%s           ", acTmp1, pTmp);
               memcpy(pOutbuf+OFF_TRANSFER_DOC, acDoc, SIZ_TRANSFER_DOC);
               memset(pOutbuf+OFF_TRANSFER_DT, ' ', SIZ_TRANSFER_DT);
            } else if (acTmp1[0] > '7')
            {
               // 911234    = 1234    1991
               sprintf(acDate, "19%.2s     ", acTmp1);
               memcpy(pOutbuf+OFF_TRANSFER_DT, acDate, SIZ_TRANSFER_DT);

               lTmp = atol(&acTmp1[2]);
               sprintf(acDoc, "%.4s%.6d    ", acDate, lTmp);
               memcpy(pOutbuf+OFF_TRANSFER_DOC, acDoc, SIZ_TRANSFER_DOC);
            } else
            {
               // 112592    = 112-592
               // 00NONE
               lTmp = atol(acTmp1);
               if (lTmp > 0)
               {
                  sprintf(acDoc, "%s           ", acTmp1);
                  memcpy(pOutbuf+OFF_TRANSFER_DOC, acDoc, SIZ_TRANSFER_DOC);
                  memset(pOutbuf+OFF_TRANSFER_DT, ' ', SIZ_TRANSFER_DT);
               }
            }
         } else if (iLen > 13 && acTmp1[iLen-3] == '/' && acTmp1[iLen-6] == '/')
         {
            // 93558610/05/93
            if (pTmp = dateConversion(&acTmp1[iLen-8], acDate, MMDDYY1))
            {
               memcpy(pOutbuf+OFF_TRANSFER_DT, acDate, SIZ_TRANSFER_DT);
               acTmp1[iLen-8] = 0;
               sprintf(acDoc, "%s           ", acTmp1);
               memcpy(pOutbuf+OFF_TRANSFER_DOC, acDoc, SIZ_TRANSFER_DOC);
            } 
         }
      }
   } else if (iCnt == 3)
   {
      // If first token is DocDate
      if (pTmp = dateConversion(apItems[0], acDate, 0))
      {
         // 12/27/04 2048018
         // 12/21/84 120/018
         if (pTmp = strchr(apItems[1], '/'))
            *pTmp = '-';

         strcpy(acDoc, apItems[1]);
         iLen = Mpa_FmtDocNum(acFmtDoc, acDoc, acDate, pRec->Apn);
         if (iLen >= SIZ_TRANSFER_DOC)
         {
            memcpy(pOutbuf+OFF_TRANSFER_DOC, acFmtDoc, SIZ_TRANSFER_DOC);
            memcpy(pOutbuf+OFF_TRANSFER_DT, acDate, SIZ_TRANSFER_DT);
         }
      } else
      {
         // 2002445&46 06/21/00
         if (strlen(apItems[1]) < 4) 
         {
            iDateIdx = 2;
            iDocIdx = 0;
         } else if (strchr(apItems[2], '/') && isdigit(*apItems[2]))
         {
            iDateIdx = 2;
            if (isdigit(*apItems[1]))
               iDocIdx = 1;
            else
               iDocIdx = 0;
         } else
         {
            iDateIdx = 1;
            iDocIdx = 0;
         }

         // Drop last item if it is in parenthesis
         if (iDateIdx == 2 && !isdigit(*apItems[2]))
         {
            iCnt--;
            iDateIdx = 1;
            iDocIdx = 0;
            LogMsg("*** 3-Drop DocNum: %s %s %s (APN=%.10s)", apItems[0], apItems[1], apItems[2], pRec->Apn);
         }

         // 2048018 12/27/04 
         // 936697/965782 12/24/96?
         pTmp = dateConversion(apItems[iDateIdx], acDate, MMDDYY1);             // mm/dd/yy
         if (pTmp)
         {
            strcpy(acDoc, apItems[iDocIdx]);
            iLen = Mpa_FmtDocNum(acFmtDoc, acDoc, acDate, pRec->Apn);
            if (iLen >= SIZ_TRANSFER_DOC)
            {
               memcpy(pOutbuf+OFF_TRANSFER_DOC, acFmtDoc, SIZ_TRANSFER_DOC);
               memcpy(pOutbuf+OFF_TRANSFER_DT, acDate, SIZ_TRANSFER_DT);
            }
         } else if (memcmp(apItems[iCnt-1], "12", 2) <= 0 && isNumber(apItems[iCnt-1]) &&                     
                   (pTmp = dateConversion(apItems[iCnt-1], acDate, MMDDYY2)) )   // mmddyy
         {
            strcpy(acDoc, apItems[0]);
            iLen = Mpa_FmtDocNum(acFmtDoc, acDoc, acDate, pRec->Apn);
            if (iLen >= SIZ_TRANSFER_DOC)
            {
               memcpy(pOutbuf+OFF_TRANSFER_DOC, acFmtDoc, SIZ_TRANSFER_DOC);
               memcpy(pOutbuf+OFF_TRANSFER_DT, acDate, SIZ_TRANSFER_DT);
            }
         } else
         {
            // 017527577 2032227 2038247
            if (isdigit(*apItems[iCnt-1]) && strlen(apItems[iCnt-1]) > 5)
            {
               strcpy(acTmp1, apItems[iCnt-1]);
               goto MPA_One_Token;
            } else if (isdigit(*apItems[iCnt-2]) && strlen(apItems[iCnt-2]) > 5)
            {
               strcpy(acTmp1, apItems[iCnt-2]);
               goto MPA_One_Token;
            } else
            {
               if (iCnt > 2 && (pTmp = dateConversion(apItems[iDateIdx], acDate, MMDDYY1)))
               {
                  // 982344 982345 06/03/98
                  memcpy(pOutbuf+OFF_TRANSFER_DT, acDate, SIZ_TRANSFER_DT);
                  strcpy(acTmp1, apItems[0]);
                  if (pTmp = strchr(acTmp1, ','))
                     *pTmp = 0;

                  iLen = Mpa_FmtDocNum(acFmtDoc, acTmp1, acDate, pRec->Apn);
                  if (iLen >= SIZ_TRANSFER_DOC)
                  {
                     memcpy(pOutbuf+OFF_TRANSFER_DOC, acFmtDoc, SIZ_TRANSFER_DOC);
                     memcpy(pOutbuf+OFF_TRANSFER_DT, acDate, SIZ_TRANSFER_DT);
                  }
               } else
               {
                  if (isdigit(*apItems[iCnt-1]) && strlen(apItems[iCnt-1]) > 5)
                     strcpy(acTmp1, apItems[iCnt-1]);
                  else if (iCnt > 2 && isdigit(*apItems[iCnt-2]) && strlen(apItems[iCnt-2]) > 5)
                     strcpy(acTmp1, apItems[iCnt-2]);
                  else
                     strcpy(acTmp1, apItems[0]);

                  if (isdigit(acTmp1[0]))
                     goto MPA_One_Token;
               }
            }
         }
      }
   } else if (iCnt == 2)
   {
      //if (pTmp = dateConversion(apItems[0], acDate, MMDDYY1))
      if (pTmp = dateConversion(apItems[0], acDate, 0))
      {
         // 12/27/04 2048018
         // 12/21/84 120/018
         if (pTmp = strchr(apItems[1], '/'))
            *pTmp = '-';

         strcpy(acDoc, apItems[1]);
         if (pTmp = strchr(acDoc, ','))
            *pTmp = 0;

         iLen = Mpa_FmtDocNum(acFmtDoc, acDoc, acDate, pRec->Apn);
         if (iLen >= SIZ_TRANSFER_DOC)
         {
            memcpy(pOutbuf+OFF_TRANSFER_DOC, acFmtDoc, SIZ_TRANSFER_DOC);
            memcpy(pOutbuf+OFF_TRANSFER_DT, acDate, SIZ_TRANSFER_DT);
         } else if (!iLen)
         {
            //LogMsg("*** Bad Doc: DocNum=%s DocDate=%s APN=%.10s", acDoc, acDate, pRec->Apn);
            memset(pOutbuf+OFF_TRANSFER_DOC, ' ',SIZ_TRANSFER_DOC);
            memset(pOutbuf+OFF_TRANSFER_DT, ' ', SIZ_TRANSFER_DT);
         }
      } else
      {
         // 2048018 12/27/04 
         // 936697/965782 12/24/96?
         if (*apItems[iCnt-1] > '9')
         {
            pTmp = apItems[iCnt-1];
            while (*pTmp > '9')
               pTmp++;
            strcpy(acTmp1, pTmp);
            pTmp = dateConversion(acTmp1, acDate, 0);             // mm/dd/yy
         } else
            pTmp = dateConversion(apItems[iCnt-1], acDate, 0);             // mm/dd/yy
         if (pTmp)
         {
            strcpy(acDoc, apItems[iCnt-2]);
            if (acDoc[3] == '/')
               acDoc[3] = '-';
            else if (acDoc[2] == '/')
               acDoc[2] = '-';

            if (pTmp = strchr(acDoc, '/'))
            {
               *pTmp++ = 0;
               if (strlen(pTmp) == strlen(acDoc))
                  strcpy(acDoc, pTmp);
            }

            // 004336553,1991652 4/21/99
            if (pTmp = strrchr(acDoc, ','))
            {
               if (strlen(pTmp) > 4)
                  strcpy(acDoc, pTmp+1);
               else
                  *pTmp = 0;
            }
            iLen = Mpa_FmtDocNum(acFmtDoc, acDoc, acDate, pRec->Apn);
            if (iLen >= SIZ_TRANSFER_DOC)
            {
               memcpy(pOutbuf+OFF_TRANSFER_DOC, acFmtDoc, SIZ_TRANSFER_DOC);
               memcpy(pOutbuf+OFF_TRANSFER_DT, acDate, SIZ_TRANSFER_DT);
            } else if (!iLen)
            {
               //LogMsg("*** Bad Doc: DocNum=%s DocDate=%s APN=%.10s", acDoc, acDate, pRec->Apn);
               memset(pOutbuf+OFF_TRANSFER_DOC, ' ',SIZ_TRANSFER_DOC);
               memset(pOutbuf+OFF_TRANSFER_DT, ' ', SIZ_TRANSFER_DT);
            }

         } else if (memcmp(apItems[iCnt-1], "12", 2) <= 0 && isNumber(apItems[iCnt-1]) &&                     
                   (pTmp = dateConversion(apItems[iCnt-1], acDate, MMDDYY2)) )   // mmddyy
         {  // 912604 052191
            strcpy(acDoc, apItems[iCnt-2]);
            iLen = Mpa_FmtDocNum(acFmtDoc, acDoc, acDate, pRec->Apn);
            if (iLen >= SIZ_TRANSFER_DOC)
            {
               memcpy(pOutbuf+OFF_TRANSFER_DOC, acFmtDoc, SIZ_TRANSFER_DOC);
               memcpy(pOutbuf+OFF_TRANSFER_DT, acDate, SIZ_TRANSFER_DT);
            } else if (!iLen)
            {
               //LogMsg("*** Bad Doc: DocNum=%s DocDate=%s APN=%.10s", acDoc, acDate, pRec->Apn);
               memset(pOutbuf+OFF_TRANSFER_DOC, ' ',SIZ_TRANSFER_DOC);
               memset(pOutbuf+OFF_TRANSFER_DT, ' ', SIZ_TRANSFER_DT);
            }
         } else
         {
            // 001912046  974011 = 1997004011
            // 928422   2000815  = 2000000815
            // 114/112 1969      = 114-112
            // 114/112 936697    = 1993006697
            // 982344 982345 06/03/98
            // 017527577 2032227 2038247

            if (isdigit(*apItems[1]) && strlen(apItems[1]) > 5)
            {
               if (strchr(apItems[1], '/'))
                  strcpy(acTmp1, apItems[0]);
               else
                  strcpy(acTmp1, apItems[1]);
               if (isdigit(acTmp1[0]))
                  goto MPA_One_Token;
            } else if (*(apItems[0]+3) == '/' || *(apItems[0]+3) == '-')
            {
               strcpy(acTmp1, apItems[0]);
               acTmp1[3] = '-';
               if (pTmp = strchr(acTmp1, ','))
                  *pTmp = 0;
               sprintf(acDoc, "%s           ", acTmp1);
               memcpy(pOutbuf+OFF_TRANSFER_DOC, acDoc, SIZ_TRANSFER_DOC);
               lTmp = atol(apItems[1]);
               if (lTmp > 1940 && lTmp < 2009)
               {
                  sprintf(acDate, "%s     ", apItems[1]);
                  memcpy(pOutbuf+OFF_TRANSFER_DT, acDate, SIZ_TRANSFER_DT);
               } else
                  memset(pOutbuf+OFF_TRANSFER_DT, ' ', SIZ_TRANSFER_DT);
            } else
            {
               if (pTmp = dateConversion(apItems[1], acDate, 0))
               {
                  // 982344 982345 06/03/98
                  memcpy(pOutbuf+OFF_TRANSFER_DT, acDate, SIZ_TRANSFER_DT);
                  strcpy(acTmp1, apItems[0]);
                  if (pTmp = strchr(acTmp1, ','))
                     *pTmp = 0;

                  iLen = Mpa_FmtDocNum(acFmtDoc, acTmp1, acDate, pRec->Apn);
                  if (iLen >= SIZ_TRANSFER_DOC)
                  {
                     memcpy(pOutbuf+OFF_TRANSFER_DOC, acFmtDoc, SIZ_TRANSFER_DOC);
                     memcpy(pOutbuf+OFF_TRANSFER_DT, acDate, SIZ_TRANSFER_DT);
                  } else if (!iLen)
                  {
                     //LogMsg("*** Bad Doc: DocNum=%s DocDate=%s APN=%.10s", acDoc, acDate, pRec->Apn);
                     memset(pOutbuf+OFF_TRANSFER_DOC, ' ',SIZ_TRANSFER_DOC);
                     memset(pOutbuf+OFF_TRANSFER_DT, ' ', SIZ_TRANSFER_DT);
                  }
               } else
               {
                  if (isdigit(*apItems[iCnt-1]) && strlen(apItems[iCnt-1]) > 5)
                     strcpy(acTmp1, apItems[iCnt-1]);
                  else if (iCnt > 2 && isdigit(*apItems[iCnt-2]) && strlen(apItems[iCnt-2]) > 5)
                     strcpy(acTmp1, apItems[iCnt-2]);
                  else
                     strcpy(acTmp1, apItems[0]);
                  if (isdigit(acTmp1[0]))
                     goto MPA_One_Token;
               }
            }
         }
      }
   } else if ( iCnt > 0 && isdigit(*apItems[0]) )
   {
      pTmp = NULL;
      if (iCnt > 2 && isdigit(*apItems[2]))
         pTmp = dateConversion(apItems[2], acDate, 0);
      if (pTmp)
      {
         //memcpy(pOutbuf+OFF_TRANSFER_DT, acDate, SIZ_TRANSFER_DT);
         strcpy(acDoc, apItems[0]);
         if (pTmp = strchr(acDoc, ','))
            *pTmp = 0;
         iLen = Mpa_FmtDocNum(acFmtDoc, acDoc, acDate, pRec->Apn);
         if (iLen >= SIZ_TRANSFER_DOC)
         {
            memcpy(pOutbuf+OFF_TRANSFER_DOC, acFmtDoc, SIZ_TRANSFER_DOC);
            memcpy(pOutbuf+OFF_TRANSFER_DT, acDate, SIZ_TRANSFER_DT);
         } else if (!iLen)
         {
            //LogMsg("*** Bad Doc: DocNum=%s DocDate=%s APN=%.10s", acDoc, acDate, pRec->Apn);
            memset(pOutbuf+OFF_TRANSFER_DOC, ' ',SIZ_TRANSFER_DOC);
            memset(pOutbuf+OFF_TRANSFER_DT, ' ', SIZ_TRANSFER_DT);
         }
      } else
      {
         // 027224985  CERT 2003090
         if (iCnt > 2 && isdigit(*apItems[2]) && strlen(apItems[2]) > 5)
            strcpy(acTmp1, apItems[2]);
         else
            strcpy(acTmp1, apItems[0]);
         if (isdigit(acTmp1[0]))
            goto MPA_One_Token;
      }
   }

   // HO Exempt
   *(pOutbuf+OFF_HO_FL) = '2';            // 'N'
   lTmp = atoin(pRec->Exemption, MPA_SIZ_EXEMPTION);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_EXE_TOTAL, lTmp);
      memcpy(pOutbuf+OFF_EXE_TOTAL, acTmp, SIZ_EXE_TOTAL);
      if (!memcmp(pRec->Exemp_Cd, "07", 2))
         *(pOutbuf+OFF_HO_FL) = '1';      // 'Y'
   }
   // Exemption code
   memcpy(pOutbuf+OFF_EXE_CD1, pRec->Exemp_Cd, SIZ_LIEN_EXECODE);
   
   Mpa_MergeOwner(pOutbuf, pRollRec);
   Mpa_MergeMAdr(pOutbuf, pRollRec);
   Mpa_MergeSAdr(pOutbuf, pRollRec);

   return 0;
}

/********************************* Mpa_Load_LDR *****************************
 *
 *
 ****************************************************************************/

int Mpa_Load_LDR(int iLoadFlag, int iFirstRec)
{
   char     acBuf[MAX_RECSIZE], acRollRec[MAX_RECSIZE];
   char     acOutFile[_MAX_PATH], acTmpFile[_MAX_PATH];

   HANDLE   fhOut;

   int      iRet;
   DWORD    nBytesWritten;
   BOOL     bRet, bEof;
   long     lRet=0, lCnt=0;

   // Open roll file
   LogMsg("Open roll file %s", acRollFile);
   fdRoll = fopen(acRollFile, "rb");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", acRollFile);
      return 2;
   }

   // Open Char file
   if (!_access(acCharFile, 0))
   {
      LogMsg("Open Char file %s", acCharFile);
      fdChar = fopen(acCharFile, "r");
      if (fdChar == NULL)
         LogMsg("***** Error opening Char file: %s\n", acCharFile);
   } else
      fdChar = NULL;

   // Open Sale file
   if (!_access(acSaleFile, 0))
   {
      LogMsg("Open Sale file %s", acSaleFile);
      fdSale = fopen(acSaleFile, "r");
      if (fdSale == NULL)
         LogMsg("***** Error opening Sale file: %s\n", acSaleFile);
   } else
      fdSale = NULL;

   // Open Output file
   sprintf(acTmpFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "R01");
   LogMsg("Open output file %s", acTmpFile);
   fhOut = CreateFile(acTmpFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);
   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening output file: %s\n", acOutFile);
      return 4;
   }

   // Output first header record
   if (iFirstRec > 0)
   {
      memset(acBuf, '9', iRecLen);
      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
   }

   // Get first RollRec
   iRet = fread(acRollRec,  1, iRollLen, fdRoll);
   while (acRollRec[0] < '0')
   {
      LogMsg0("*** Bad input record: %.*s", iApnLen, acRollRec);
      iRet = fread(acRollRec,  1, iRollLen, fdRoll);
   }
   bEof = false;

   // Init variables
   iNoMatch=iBadCity=iBadSuffix=0;

   // Merge loop
   while (iRet == iRollLen)
   {
      iRet = Mpa_MergeRoll(acBuf, acRollRec, CREATE_R01|CLEAR_R01);

      if (fdChar)
         Mpa_MergeChar(acBuf);

      if (fdSale)
         Mpa_MergeLandSale(acBuf);

      // Save last recording date
      lRet = atoin((char *)&acBuf[OFF_TRANSFER_DT], 8);
      if (lRet > lLastRecDate && lRet < lToday)
         lLastRecDate = lRet;

      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
      if (!bRet)
      {
         LogMsg("***** Error writing to output file at record %d\n", lCnt);
         lRet = WRITE_ERR;
         break;
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);

      iRet = fread(acRollRec,  1, iRollLen,fdRoll);
   }

   // Close files
   if (fdRoll)
      fclose(fdRoll);
   if (fdChar)
      fclose(fdChar);
   if (fdSale)
      fclose(fdSale);
   if (fhOut)
      CloseHandle(fhOut);

   LogMsg("Total input records:        %u", lCnt);
   LogMsg("Total output records:       %u", lCnt);
   LogMsg("Total bad-city records:     %u", iBadCity);
   LogMsg("Total bad-suffix records:   %u", iBadSuffix);
   LogMsg("Last recording date:        %u", lLastRecDate);
   LogMsg("Total char match:           %u", lCharMatch);
   LogMsg("Total char skip:            %u", lCharSkip);
   LogMsg("Total sale match:           %u", lSaleMatch);
   LogMsg("Total sale skip:            %u", lSaleSkip);
   printf("\nTotal output records: %u\n", lCnt);

   lRecCnt = lCnt;
   lLDRRecCount = lRecCnt;

   return 0;
}

/********************************** Mpa_LoadRoll ****************************
 *
 *
 ****************************************************************************/

int Mpa_LoadRoll(int iLoadFlag, int iSkip)
{
   char     acRec[MAX_RECSIZE], acBuf[MAX_RECSIZE], acRollRec[MAX_RECSIZE];
   char     acRawFile[_MAX_PATH], acOutFile[_MAX_PATH];

   HANDLE   fhIn, fhOut;

   int      iRet, iTmp, iRollUpd=0, iNewRec=0, iRetiredRec=0;
   DWORD    nBytesRead;
   DWORD    nBytesWritten;
   BOOL     bRet, bEof;
   long     lRet=0, lCnt=0;

   LogMsg0("Loading update roll");
   sprintf(acRawFile, acRawTmpl, "Mpa", "Mpa", "S01");
   sprintf(acOutFile, acRawTmpl, "Mpa", "Mpa", "R01");

   // Rename files for processing
   if (_access(acRawFile, 0))
   {
      if (!_access(acOutFile, 0))
         rename(acOutFile, acRawFile);
      else
      {
         LogMsg("***** Missing input file %s.  Please recheck!", acRawFile);
         return 1;
      }
   }

   // Open roll file
   LogMsg("Open roll file %s", acRollFile);
   fdRoll = fopen(acRollFile, "rb");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", acRollFile);
      return 2;
   }

   // Open Char file
   if (!_access(acCharFile, 0))
   {
      LogMsg("Open Char file %s", acCharFile);
      fdChar = fopen(acCharFile, "r");
      if (fdChar == NULL)
         LogMsg("***** Error opening Char file: %s\n", acCharFile);
   } else
      fdChar = NULL;

   // Open Sale file
   if (!_access(acSaleFile, 0))
   {
      LogMsg("Open Sale file %s", acSaleFile);
      fdSale = fopen(acSaleFile, "r");
      if (fdSale == NULL)
         LogMsg("***** Error opening Sale file: %s\n", acSaleFile);
   } else
      fdSale = NULL;

   // Open Input file
   LogMsg("Open input file %s", acRawFile);
   fhIn = CreateFile(acRawFile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhIn == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening input file: %s\n", acRawFile);
      return 3;
   }

   // Open Output file
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, FILE_SHARE_READ, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening output file: %s\n", acOutFile);
      return 4;
   }

   // Get first RollRec
   iRet = fread(acRollRec,  1, iRollLen, fdRoll);
   bEof = false;
   while (acRollRec[0] < '0')
   {
      LogMsg0("*** Bad input record: %.*s", iApnLen, acRollRec);
      iRet = fread(acRollRec,  1, iRollLen, fdRoll);
   }

   // Copy skip record
   memset(acBuf, ' ', iRecLen);
   while (iSkip-- > 0)
   {
      ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);
      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
   }

   iNoMatch=iBadCity=iBadSuffix=0;

   // Merge loop
   while (!bEof)
   {
      bRet = ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);
      if (!bRet)
      {
         LogMsg("***** Error reading input file %s (%f)", acRawFile, GetLastError());
         break;
      }

      if (!nBytesRead)
         break;

      NextRollRec:
      iTmp = memcmp(acBuf, &acRollRec[MPA_OFF_APN], iApnLen);
      if (!iTmp)
      {
         // Merge roll data
         iRet = Mpa_MergeRoll(acBuf, acRollRec, UPDATE_R01);
         iRollUpd++;

#ifdef _DEBUG
         //if (!memcmp(acBuf, "0020400320", 10) )
         //   iTmp = 0;
#endif

         if (fdChar)
            Mpa_MergeChar(acBuf);

#ifdef _DEBUG
         //if (acBuf[OFF_SALE1_DT] > ' ')
         //   memset(&acBuf[OFF_SALE1_DT], ' ', SIZ_SALE1_DT);
#endif
         if (fdSale)
            Mpa_MergeLandSale(acBuf);

         // Save last recording date
         lRet = atoin((char *)&acBuf[OFF_TRANSFER_DT], 8);
         if (lRet > lLastRecDate && lRet < lToday)
            lLastRecDate = lRet;

         bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
         lCnt++;

         // Read next roll record
         iRet = fread(acRollRec,  1, iRollLen,fdRoll);

         if (!iRet)
            bEof = true;
      } else if (iTmp > 0)       // Roll not match, new roll record
      {
         if (bDebug)
            LogMsg0("*** New roll record : %.*s (%d)", iApnLen, &acRollRec[MPA_OFF_APN], lCnt);

         // Create new R01 record
         iRet = Mpa_MergeRoll(acRec, acRollRec, CREATE_R01);
         iNewRec++;

         if (fdChar)
            Mpa_MergeChar(acRec);

         if (fdSale)
            Mpa_MergeLandSale(acRec);

         // Save last recording date
         lRet = atoin((char *)&acRec[OFF_TRANSFER_DT], 8);
         if (lRet > lLastRecDate && lRet < lToday)
            lLastRecDate = lRet;

         bRet = WriteFile(fhOut, acRec, iRecLen, &nBytesWritten, NULL);
         if (!(++lCnt % 1000))
            printf("\r%u", lCnt);

         iRet = fread(acRollRec,  1, iRollLen,fdRoll);
         if (!iRet)
            bEof = true;
         else
            goto NextRollRec;
      } else
      {
         iRetiredRec++;
         if (bDebug)
            LogMsg0("*** Retired record : %.*s (%d)", iApnLen, acBuf, lCnt);
         continue;
      }

      if (!(lCnt % 1000))
         printf("\r%u", lCnt);

      if (!bRet)
      {
         LogMsg("***** Error occurs: %d\n", GetLastError());
         lRet = WRITE_ERR;
         break;
      }
   }

   // If there are more new records, add them
   while (!bEof)
   {
      if (bDebug)
         LogMsg0("*** New roll record : %.*s (%d) ***", iApnLen, &acRollRec[MPA_OFF_APN], lCnt);

      // Create new R01 record
      iRet = Mpa_MergeRoll(acRec, acRollRec, CREATE_R01);
      iNewRec++;

      if (fdChar)
         Mpa_MergeChar(acRec);

      if (fdSale)
         Mpa_MergeLandSale(acRec);

      // Save last recording date
      lRet = atoin((char *)&acRec[OFF_TRANSFER_DT], 8);
      if (lRet > lLastRecDate && lRet < lToday)
         lLastRecDate = lRet;

      bRet = WriteFile(fhOut, acRec, iRecLen, &nBytesWritten, NULL);
      lCnt++;

      // Get next roll record
      iRet = fread(acRollRec,  1, iRollLen,fdRoll);

      if (!iRet)
         bEof = true;
   }

   // Close files
   if (fdRoll)
      fclose(fdRoll);
   if (fdChar)
      fclose(fdChar);
   if (fdSale)
      fclose(fdSale);
   if (fhOut)
      CloseHandle(fhOut);
   if (fhIn)
      CloseHandle(fhIn);

   LogMsg("Total output records:       %u", lCnt);
   LogMsg("Total new records:          %u", iNewRec);
   LogMsg("Total retired records:      %u", iRetiredRec);
   LogMsg("Total bad-city records:     %u", iBadCity);
   LogMsg("Total bad-suffix records:   %u", iBadSuffix);
   LogMsg("Total char match:           %u", lCharMatch);
   LogMsg("Total char skip:            %u", lCharSkip);
   LogMsg("Total sale match:           %u", lSaleMatch);
   LogMsg("Total sale skip:            %u", lSaleSkip);

   printf("\nTotal output records: %u\n", lCnt);

   lRecCnt = lCnt;

   return 0;
}

/****************************** Mpa_CreatePublR01 ****************************
 *
 * This version support pre 2012 format
 *
 * Return 0 if successful, >0 if error
 *
 *****************************************************************************/

int Mpa_CreatePublR01(char *pOutbuf, char *pRollRec)
{
   char     acTmp[256], acTmp1[256], *apItems[16], *pTmp;
   char     acDate[256], acDoc[32];
   int      iRet, iCnt;
   long     lTmp;

   iRet = ParseStringNQ(pRollRec, ',', MAX_FLD_TOKEN, apTokens);
   if (iRet < MPA_UA_LEGAL)
   {
      LogMsg("***** Error: bad input record for APN=%s", apTokens[iApnFld]);
      return -1;
   }

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "0101100240", 9))
   //   iRet = 0;
#endif
   // Clear output buffer
   memset(pOutbuf, ' ', iRecLen);

   // Start copying data
   memcpy(pOutbuf, apTokens[MPA_UA_APN], strlen(apTokens[MPA_UA_APN]));

   // Format APN
   iRet = formatApn(apTokens[MPA_UA_APN], acTmp, &myCounty);
   memcpy(pOutbuf+OFF_APN_D, acTmp, iRet);

   // Create MapLink and output new record
   iRet = formatMapLink(apTokens[MPA_UA_APN], acTmp, &myCounty);
   memcpy(pOutbuf+OFF_MAPLINK, acTmp, iRet);

   // Create index map link
   if (getIndexPage(acTmp, acTmp1, &myCounty))
      memcpy(pOutbuf+OFF_IMAPLINK, acTmp1, iRet);

   memcpy(pOutbuf+OFF_CO_NUM, "22MPA", 5);

   // Set publuc parcel flag
   *(pOutbuf+OFF_PUBL_FLG) = 'Y';

   // Year assessed
   memcpy(pOutbuf+OFF_YR_ASSD, myCounty.acYearAssd, 4);

   long lSqFtLand=0;
   double dAcreAge = atof(apTokens[MPA_UA_ACRES]);
   if (dAcreAge > 0.0)
   {
      /* Changed on 3/1/2012
      dAcreAge /= 10.0;
      sprintf(acTmp, "%*d", SIZ_LOT_ACRES, (long)(dAcreAge));
      memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);

      lSqFtLand = (long)((dAcreAge * SQFT_FACTOR_1000));
      sprintf(acTmp, "%*d", SIZ_LOT_SQFT, lSqFtLand);
      memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);
      */
      dAcreAge *= 1000.0;
      sprintf(acTmp, "%*d", SIZ_LOT_ACRES, (long)(dAcreAge));
      memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);

      lSqFtLand = (long)((dAcreAge * SQFT_FACTOR_1000));
      sprintf(acTmp, "%*d", SIZ_LOT_SQFT, lSqFtLand);
      memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);
   }

   // Land
   long lLand = atol(apTokens[MPA_UA_LAND]);
   if (lLand > 0)
   {
      sprintf(acTmp, "%*d", SIZ_LAND, lLand);
      memcpy(pOutbuf+OFF_LAND, acTmp, SIZ_LAND);
   }

   // Improve
   long lImpr = atol(apTokens[MPA_UA_IMPR]);
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*d", SIZ_IMPR, lImpr);
      memcpy(pOutbuf+OFF_IMPR, acTmp, SIZ_IMPR);
   }

   long lOtherImpr = atol(apTokens[MPA_UA_TIMBER]);
   lOtherImpr += atol(apTokens[MPA_UA_WIL_LAND]);
   lOtherImpr += atol(apTokens[MPA_UA_WIL_MARKET]);
   lOtherImpr += atol(apTokens[MPA_UA_TREES_VINES]);
   if (lOtherImpr > 0)
   {
      sprintf(acTmp, "%*d", SIZ_OTHER, lOtherImpr);
      memcpy(pOutbuf+OFF_OTHER, acTmp, SIZ_OTHER);
   }

   // Gross total
   lTmp = lLand+lImpr+lOtherImpr;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_GROSS, lTmp);
      memcpy(pOutbuf+OFF_GROSS, acTmp, SIZ_GROSS);
   }

   // Ratio
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*d", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
      memcpy(pOutbuf+OFF_RATIO, acTmp, SIZ_RATIO);
   }

   // TRA
   lTmp = atol(apTokens[MPA_UA_TRA]);
   if (lTmp > 0)
   {
      iRet = sprintf(acTmp, "%.6d", lTmp);
      memcpy(pOutbuf+OFF_TRA, acTmp, iRet);
   }

   // Legal
   if (*apTokens[MPA_UA_LEGAL] > ' ')
   {
      iCnt = updateLegal(pOutbuf, apTokens[MPA_UA_LEGAL]);
      if (iCnt > iMaxLegal)
         iMaxLegal = iCnt;
   }

   // CareOf
   if (*apTokens[MPA_UA_CAREOF] > ' ')
   {
      /*
      iCnt = strlen(apTokens[MPA_UA_CAREOF]);
      if (iCnt > SIZ_CARE_OF)
         iCnt = SIZ_CARE_OF;
      memcpy(pOutbuf+OFF_CARE_OF, apTokens[MPA_UA_CAREOF], iCnt);
      */
      updateCareOf(pOutbuf, apTokens[MPA_UA_CAREOF], strlen(apTokens[MPA_UA_CAREOF]));
   }

   acDate[0] = 0;

   // Transfer
   // Pre 1987:   9999999YY or 999999YY
   // 1987-1998:  YY9999
   // 1999-now:   YYY9999 (i.e. 1990102=0102 1999, 2030405=0405 2003)
   strcpy(acTmp, apTokens[MPA_UA_DEED_REF]);
   blankRem(acTmp, 0);
   iCnt = ParseString(acTmp, ' ', 16, apItems);

   // 1991415 & 1991416 4/5/99
   // 2025481 (2026279 10/24/02
   if (iCnt == 1)
   {
MPA_One_Token:
      strcpy(acTmp1, apItems[0]);
      if (pTmp = strchr(acTmp1, '('))
         *pTmp = 0;
      if (pTmp = strchr(acTmp1, ','))
         *pTmp = 0;

      if (isdigit(acTmp1[0]))
      {
         // 028639186 = 286-391 1986
         // 022464581 = 224-645 1981
         // 12152970  = 121-529 1970
         // 911234    = 1234    1991
         // 112592    = 112-592
         // 218/262   = 218-262
         // 1984445/6 9/30/98
         // 2003750/51 09/06/00
         lTmp = strlen(acTmp1);
         if (lTmp > 7 && lTmp < 14 && (pTmp = strchr(acTmp1, '/')))
         {
            *pTmp = 0;
            lTmp = strlen(acTmp1);
         }
         if (lTmp == 9)
         {
            // 028639186 = 286-391 1986
            sprintf(acDate, "19%.2s     ", &acTmp1[7]);
            memcpy(pOutbuf+OFF_TRANSFER_DT, acDate, SIZ_TRANSFER_DT);
            sprintf(acDoc, "%s         ", acTmp1);
            memcpy(pOutbuf+OFF_TRANSFER_DOC, acDoc, SIZ_TRANSFER_DOC);
         } else if (lTmp == 8)
         {
            // 28639186 = 286-391 1986
            sprintf(acDate, "19%.2s     ", &acTmp1[6]);
            memcpy(pOutbuf+OFF_TRANSFER_DT, acDate, SIZ_TRANSFER_DT);
            sprintf(acDoc, "%s         ", acTmp1);
            memcpy(pOutbuf+OFF_TRANSFER_DOC, acDoc, SIZ_TRANSFER_DOC);
         } else if (lTmp == 7)
         {
            if (acTmp1[3] == '/' || acTmp1[3] == '-')
            {
               // 218/262   = 218-262
               sprintf(acDoc, "%.3s-%.3s           ", acTmp1, &acTmp1[4]);
               memcpy(pOutbuf+OFF_TRANSFER_DOC, acDoc, SIZ_TRANSFER_DOC);
               memset(pOutbuf+OFF_TRANSFER_DT, ' ', SIZ_TRANSFER_DT);
            } else
            {
               if (acTmp[0] == '1')
                  sprintf(acDate, "19%.2s           ", &acTmp1[1]);
               else if (acTmp[0] == '2')
                  sprintf(acDate, "20%.2s           ", &acTmp1[1]);
               else
                  memset(acDate, ' ', SIZ_TRANSFER_DT);
               sprintf(acDoc, "%s             ", acTmp1);
               memcpy(pOutbuf+OFF_TRANSFER_DOC, acDoc, SIZ_TRANSFER_DOC);
               memcpy(pOutbuf+OFF_TRANSFER_DT, acDate, SIZ_TRANSFER_DT);
            }
         } else if (lTmp == 6)
         {
            if (pTmp = strchr(acTmp1, '/'))
            {
               // 286/64
               *pTmp++ = 0;
               sprintf(acDoc, "%s-%s           ", acTmp1, pTmp);
               memcpy(pOutbuf+OFF_TRANSFER_DOC, acDoc, SIZ_TRANSFER_DOC);
               memset(pOutbuf+OFF_TRANSFER_DT, ' ', SIZ_TRANSFER_DT);
            } else if (acTmp1[0] > '7')
            {
               // 911234    = 1234    1991
               sprintf(acDate, "19%.2s     ", acTmp1);
               memcpy(pOutbuf+OFF_TRANSFER_DT, acDate, SIZ_TRANSFER_DT);
               sprintf(acDoc, "%s           ", &acTmp1[2]);
               memcpy(pOutbuf+OFF_TRANSFER_DOC, acDoc, SIZ_TRANSFER_DOC);
            } else
            {
               // 112592    = 112-592
               sprintf(acDoc, "%s           ", acTmp1);
               memcpy(pOutbuf+OFF_TRANSFER_DOC, acDoc, SIZ_TRANSFER_DOC);
               memset(pOutbuf+OFF_TRANSFER_DT, ' ', SIZ_TRANSFER_DT);
            }
         } else if (acTmp1[lTmp-3] == '/' && acTmp1[lTmp-6] == '/')
         {
            // 93558610/05/93
            if (pTmp = dateConversion(&acTmp1[lTmp-8], acDate, MMDDYY1))
            {
               memcpy(pOutbuf+OFF_TRANSFER_DT, acDate, SIZ_TRANSFER_DT);
               acTmp1[lTmp-8] = 0;
               sprintf(acDoc, "%s           ", acTmp1);
               memcpy(pOutbuf+OFF_TRANSFER_DOC, acDoc, SIZ_TRANSFER_DOC);
            } 
         } else
         {
            sprintf(acDoc, "%s              ", acTmp1);
            memcpy(pOutbuf+OFF_TRANSFER_DOC, acDoc, SIZ_TRANSFER_DOC);
            memset(pOutbuf+OFF_TRANSFER_DT, ' ', SIZ_TRANSFER_DT);
         }
      }
   } else if (isdigit(*apItems[0]) && isdigit(*apItems[1]))
   {
      if (pTmp = dateConversion(apItems[0], acDate, MMDDYY1))
      {
         // 12/27/04 2048018
         // 12/21/84 120/018
         if (pTmp = strchr(apItems[1], '/'))
            *pTmp = '-';
         sprintf(acDoc, "%s           ", apItems[1]);
         memcpy(pOutbuf+OFF_TRANSFER_DOC, acDoc, SIZ_TRANSFER_DOC);
         memcpy(pOutbuf+OFF_TRANSFER_DT, acDate, SIZ_TRANSFER_DT);
      } else
      {
         // 2048018 12/27/04 
         // 936697/965782 12/24/96?
         pTmp = dateConversion(apItems[1], acDate, MMDDYY1);
         if (pTmp)
         {
            strcpy(acDoc, apItems[0]);
            if (acDoc[3] == '/')
               acDoc[3] = '-';
            else if (acDoc[2] == '/')
               acDoc[2] = '-';
            if (pTmp = strchr(acDoc, '/'))
               *pTmp = 0;
            if (pTmp = strchr(acDoc, '('))
               *pTmp = 0;
            if (pTmp = strchr(acDoc, ','))
               *pTmp = 0;
            blankPad(acDoc, SIZ_TRANSFER_DOC);
            memcpy(pOutbuf+OFF_TRANSFER_DOC, acDoc, SIZ_TRANSFER_DOC);
            memcpy(pOutbuf+OFF_TRANSFER_DT, acDate, SIZ_TRANSFER_DT);
         } else
         {
            // 001912046  974011
            // 928422   2000815
            // 114/112 1969
            // 114/112 936697
            // 982344 982345 06/03/98
            // Use first token and drop everything else
            if (*(apItems[0]+3) == '/' || *(apItems[0]+3) == '-')
            {
               strcpy(acTmp1, apItems[0]);
               acTmp1[3] = '-';
               if (pTmp = strchr(acTmp1, ','))
                  *pTmp = 0;
               sprintf(acDoc, "%s           ", acTmp1);
               memcpy(pOutbuf+OFF_TRANSFER_DOC, acDoc, SIZ_TRANSFER_DOC);
               lTmp = atol(apItems[1]);
               if (lTmp > 1940 && lTmp < 2009)
               {
                  sprintf(acDate, "%s     ", apItems[1]);
                  memcpy(pOutbuf+OFF_TRANSFER_DT, acDate, SIZ_TRANSFER_DT);
               } else
                  memset(pOutbuf+OFF_TRANSFER_DT, ' ', SIZ_TRANSFER_DT);
            } else
            {
               if (iCnt > 2 && (pTmp = dateConversion(apItems[2], acDate, MMDDYY1)))
               {
                  // 982344 982345 06/03/98
                  memcpy(pOutbuf+OFF_TRANSFER_DT, acDate, SIZ_TRANSFER_DT);
                  strcpy(acTmp1, apItems[0]);
                  if (pTmp = strchr(acTmp1, ','))
                     *pTmp = 0;
                  sprintf(acDoc, "%s           ", acTmp1);
                  memcpy(pOutbuf+OFF_TRANSFER_DOC, acDoc, SIZ_TRANSFER_DOC);
               } else
               {
                  goto MPA_One_Token;
               }
            }
         }
      }
   } else if (isdigit(*apItems[0]) )
   {
      pTmp = NULL;
      if (iCnt > 2)
         pTmp = dateConversion(apItems[2], acDate, MMDDYY1);
      if (pTmp)
      {
         memcpy(pOutbuf+OFF_TRANSFER_DT, acDate, SIZ_TRANSFER_DT);
         strcpy(acDoc, apItems[0]);
         if (pTmp = strchr(acDoc, ','))
            *pTmp = 0;
         blankPad(acDoc, SIZ_TRANSFER_DOC);
         memcpy(pOutbuf+OFF_TRANSFER_DOC, acDoc, SIZ_TRANSFER_DOC);
      } else
      {
         goto MPA_One_Token;
      }
   }

   // HO Exempt
   *(pOutbuf+OFF_HO_FL) = '2';      // 'N'

   // Owner
   iCnt = strlen(apTokens[MPA_UA_OWNER1]);
   if (iCnt > SIZ_NAME_SWAP)
      iCnt = SIZ_NAME_SWAP;
   memcpy(pOutbuf+OFF_NAME_SWAP, apTokens[MPA_UA_OWNER1], iCnt);
   memcpy(pOutbuf+OFF_NAME1, apTokens[MPA_UA_OWNER1], iCnt);

   // Situs
   Mpa_MergeSAdr(pOutbuf, apTokens[MPA_UA_SITUS], false);

   // Mailing
   Mpa_MergeMAdr(pOutbuf, apTokens[MPA_UA_M_STR], apTokens[MPA_UA_M_CITY], apTokens[MPA_UA_M_ST], apTokens[MPA_UA_M_ZIP]);

   return 0;
}

// New version to support fixed length EBCDIC format
// This is for 2012 and later
int Mpa_CreatePublR01(char *pOutbuf, char *pRollRec, int iRecType)
{
   char     acTmp[256], acTmp1[256], *apItems[16], *pTmp;
   char     acDate[256], acDoc[32];
   int      iRet, iCnt;
   long     lTmp;
   MPA_ROLL *pRec = (MPA_ROLL *)pRollRec;

#ifdef _DEBUG
   //if (!memcmp(pRollRec, "0101100240", 9))
   //   iRet = 0;
#endif
   // Clear output buffer
   memset(pOutbuf, ' ', iRecLen);

   // Start copying data
   memcpy(pOutbuf, pRec->Apn, MPA_SIZ_APN);

   // Format APN
   iRet = formatApn(pRec->Apn, acTmp, &myCounty);
   memcpy(pOutbuf+OFF_APN_D, acTmp, iRet);

   // Create MapLink and output new record
   iRet = formatMapLink(pRec->Apn, acTmp, &myCounty);
   memcpy(pOutbuf+OFF_MAPLINK, acTmp, iRet);

   // Create index map link
   if (getIndexPage(acTmp, acTmp1, &myCounty))
      memcpy(pOutbuf+OFF_IMAPLINK, acTmp1, iRet);

   memcpy(pOutbuf+OFF_CO_NUM, "22MPA", 5);

   // Year assessed
   memcpy(pOutbuf+OFF_YR_ASSD, myCounty.acYearAssd, 4);

   long lSqFtLand=0;
   double dAcreAge = atofn(pRec->Acres, MPA_SIZ_ACRES);
   if (dAcreAge > 0.0)
   {
      dAcreAge /= 10.0;
      sprintf(acTmp, "%*d", SIZ_LOT_ACRES, (long)(dAcreAge));
      memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);

      lSqFtLand = (long)((dAcreAge * SQFT_FACTOR_1000));
      sprintf(acTmp, "%*d", SIZ_LOT_SQFT, lSqFtLand);
      memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);
   }

   // Land
   long lLand = atoin(pRec->Land,MPA_SIZ_LAND);
   if (lLand > 0)
   {
      sprintf(acTmp, "%*d", SIZ_LAND, lLand);
      memcpy(pOutbuf+OFF_LAND, acTmp, SIZ_LAND);
   }

   // Improve
   long lImpr = atoin(pRec->Improvements, MPA_SIZ_IMPROVEMENTS);
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*d", SIZ_IMPR, lImpr);
      memcpy(pOutbuf+OFF_IMPR, acTmp, SIZ_IMPR);
   }

   long lTimber = atoin(pRec->Timber, MPA_SIZ_TIMBER);
   long lTree   = atoin(pRec->Trees_Vines, MPA_SIZ_TREES_VINES);
   long lWLand  = atoin(pRec->Will_Land, MPA_SIZ_WILL_LAND);
   long lWImpr  = atoin(pRec->Will_Market, MPA_SIZ_WILL_MARKET);
   lTmp = lTimber+lWLand+lWImpr+lTree;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_OTHER, lTmp);
      memcpy(pOutbuf+OFF_OTHER, acTmp, SIZ_OTHER);

      if (lTimber > 0)
      {
         sprintf(acTmp, "%d          ", lTimber);
         memcpy(pOutbuf+OFF_TIMBER_VAL, acTmp, SIZ_TIMBER_VAL);
      }
      if (lTree > 0)
      {
         sprintf(acTmp, "%d          ", lTree);
         memcpy(pOutbuf+OFF_TREEVINES, acTmp, SIZ_TREEVINES);
      }
      if (lWLand > 0)
      {
         sprintf(acTmp, "%d          ", lWLand);
         memcpy(pOutbuf+OFF_CLCA_LAND, acTmp, SIZ_CLCA_LAND);
      }
      if (lWImpr > 0)
      {
         sprintf(acTmp, "%d          ", lWImpr);
         memcpy(pOutbuf+OFF_CLCA_IMPR, acTmp, SIZ_CLCA_IMPR);
      }
   }

   // Gross total
   lTmp += (lLand+lImpr);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_GROSS, lTmp);
      memcpy(pOutbuf+OFF_GROSS, acTmp, SIZ_GROSS);
   }

   // Ratio
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*d", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
      memcpy(pOutbuf+OFF_RATIO, acTmp, SIZ_RATIO);
   }

   // TRA
   lTmp = atoin(pRec->Tax_Area, MPA_SIZ_TAX_AREA);
   if (lTmp > 0)
   {
      iRet = sprintf(acTmp, "%.6d", lTmp);
      memcpy(pOutbuf+OFF_TRA, acTmp, iRet);
   }

   // Set publuc parcel flag
   *(pOutbuf+OFF_PUBL_FLG) = 'Y';

   // Legal
   if (pRec->Legal[0] > ' ')
      memcpy(pOutbuf+OFF_LEGAL, pRec->Legal, MPA_SIZ_LEGAL);

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "0802700160", 10))
   //   iRet = 0;
#endif
   // Zoning
   if (pRec->Zoning[0] == 'N')
   {
      pRec->Use_Zone[MPA_SIZ_USE_ZONE] = 0;

      // Translate to ZONING
      if (pTmp = GetZoning(pRec->Use_Zone))
      {
         iRet = strlen(pTmp);
         memset(pOutbuf+OFF_ZONE, ' ', SIZ_ZONE);
         memcpy(pOutbuf+OFF_ZONE, pTmp, iRet);

         if (*(pOutbuf+OFF_ZONE_X1) == ' ')
            vmemcpy(pOutbuf+OFF_ZONE_X1, pTmp, SIZ_ZONE_X1, iRet);
      }
   }

   // CareOf
   updateCareOf(pOutbuf, pRec->Careof, MPA_SIZ_CAREOF);
   acDate[0] = 0;

   // Transfer
   // Pre 1987:   9999999YY or 999999YY
   // 1987-1998:  YY9999
   // 1999-now:   YYY9999 (i.e. 1990102=0102 1999, 2030405=0405 2003)
   memcpy(acTmp, pRec->Deed_Ref, MPA_SIZ_DEED_REF);
   blankRem(acTmp, MPA_SIZ_DEED_REF);
   iCnt = ParseString(acTmp, ' ', 16, apItems);

   // 1991415 & 1991416 4/5/99
   // 2025481 (2026279 10/24/02
   // 951406-3/95 953327-7/95
   if (iCnt == 1)
   {
MPA_One_Token:
      strcpy(acTmp1, apItems[0]);
      if (pTmp = strchr(acTmp1, '('))
         *pTmp = 0;
      if (pTmp = strchr(acTmp1, ','))
         *pTmp = 0;

      if (isdigit(acTmp1[0]))
      {
         // 028639186 = 286-391 1986
         // 022464581 = 224-645 1981
         // 12152970  = 121-529 1970
         // 911234    = 1234    1991
         // 112592    = 112-592
         // 218/262   = 218-262
         // 1984445/6 9/30/98
         // 2003750/51 09/06/00
         lTmp = strlen(acTmp1);
         if (lTmp > 7 && lTmp < 14 && (pTmp = strchr(acTmp1, '/')))
         {
            *pTmp = 0;
            lTmp = strlen(acTmp1);
         }
         if (lTmp == 9)
         {
            // 028639186 = 286-391 1986
            sprintf(acDate, "19%.2s     ", &acTmp1[7]);
            memcpy(pOutbuf+OFF_TRANSFER_DT, acDate, SIZ_TRANSFER_DT);
            sprintf(acDoc, "%s         ", acTmp1);
            memcpy(pOutbuf+OFF_TRANSFER_DOC, acDoc, SIZ_TRANSFER_DOC);
         } else if (lTmp == 8)
         {
            // 28639186 = 286-391 1986
            sprintf(acDate, "19%.2s     ", &acTmp1[6]);
            memcpy(pOutbuf+OFF_TRANSFER_DT, acDate, SIZ_TRANSFER_DT);
            sprintf(acDoc, "%s         ", acTmp1);
            memcpy(pOutbuf+OFF_TRANSFER_DOC, acDoc, SIZ_TRANSFER_DOC);
         } else if (lTmp == 7)
         {
            if (acTmp1[3] == '/' || acTmp1[3] == '-')
            {
               // 218/262   = 218-262
               sprintf(acDoc, "%.3s-%.3s           ", acTmp1, &acTmp1[4]);
               memcpy(pOutbuf+OFF_TRANSFER_DOC, acDoc, SIZ_TRANSFER_DOC);
               memset(pOutbuf+OFF_TRANSFER_DT, ' ', SIZ_TRANSFER_DT);
            } else
            {
               if (acTmp[0] == '1')
                  sprintf(acDate, "19%.2s           ", &acTmp1[1]);
               else if (acTmp[0] == '2')
                  sprintf(acDate, "20%.2s           ", &acTmp1[1]);
               else
                  memset(acDate, ' ', SIZ_TRANSFER_DT);
               sprintf(acDoc, "%s             ", acTmp1);
               memcpy(pOutbuf+OFF_TRANSFER_DOC, acDoc, SIZ_TRANSFER_DOC);
               memcpy(pOutbuf+OFF_TRANSFER_DT, acDate, SIZ_TRANSFER_DT);
            }
         } else if (lTmp == 6)
         {
            if (pTmp = strchr(acTmp1, '/'))
            {
               // 286/64
               *pTmp++ = 0;
               sprintf(acDoc, "%s-%s           ", acTmp1, pTmp);
               memcpy(pOutbuf+OFF_TRANSFER_DOC, acDoc, SIZ_TRANSFER_DOC);
               memset(pOutbuf+OFF_TRANSFER_DT, ' ', SIZ_TRANSFER_DT);
            } else if (acTmp1[0] > '7')
            {
               // 911234    = 1234    1991
               sprintf(acDate, "19%.2s     ", acTmp1);
               memcpy(pOutbuf+OFF_TRANSFER_DT, acDate, SIZ_TRANSFER_DT);
               sprintf(acDoc, "%s           ", &acTmp1[2]);
               memcpy(pOutbuf+OFF_TRANSFER_DOC, acDoc, SIZ_TRANSFER_DOC);
            } else
            {
               // 112592    = 112-592
               sprintf(acDoc, "%s           ", acTmp1);
               memcpy(pOutbuf+OFF_TRANSFER_DOC, acDoc, SIZ_TRANSFER_DOC);
               memset(pOutbuf+OFF_TRANSFER_DT, ' ', SIZ_TRANSFER_DT);
            }
         } else if (acTmp1[lTmp-3] == '/' && acTmp1[lTmp-6] == '/')
         {
            // 93558610/05/93
            if (pTmp = dateConversion(&acTmp1[lTmp-8], acDate, MMDDYY1))
            {
               memcpy(pOutbuf+OFF_TRANSFER_DT, acDate, SIZ_TRANSFER_DT);
               acTmp1[lTmp-8] = 0;
               sprintf(acDoc, "%s           ", acTmp1);
               memcpy(pOutbuf+OFF_TRANSFER_DOC, acDoc, SIZ_TRANSFER_DOC);
            } 
         } else
         {
            sprintf(acDoc, "%s              ", acTmp1);
            memcpy(pOutbuf+OFF_TRANSFER_DOC, acDoc, SIZ_TRANSFER_DOC);
            memset(pOutbuf+OFF_TRANSFER_DT, ' ', SIZ_TRANSFER_DT);
         }
      }
   } else if (isdigit(*apItems[0]) && isdigit(*apItems[1]))
   {
      if (pTmp = dateConversion(apItems[0], acDate, MMDDYY1))
      {
         // 12/27/04 2048018
         // 12/21/84 120/018
         if (pTmp = strchr(apItems[1], '/'))
            *pTmp = '-';
         sprintf(acDoc, "%s           ", apItems[1]);
         memcpy(pOutbuf+OFF_TRANSFER_DOC, acDoc, SIZ_TRANSFER_DOC);
         memcpy(pOutbuf+OFF_TRANSFER_DT, acDate, SIZ_TRANSFER_DT);
      } else
      {
         // 2048018 12/27/04 
         // 936697/965782 12/24/96?
         pTmp = dateConversion(apItems[1], acDate, MMDDYY1);
         if (pTmp)
         {
            strcpy(acDoc, apItems[0]);
            if (acDoc[3] == '/')
               acDoc[3] = '-';
            else if (acDoc[2] == '/')
               acDoc[2] = '-';
            if (pTmp = strchr(acDoc, '/'))
               *pTmp = 0;
            if (pTmp = strchr(acDoc, '('))
               *pTmp = 0;
            if (pTmp = strchr(acDoc, ','))
               *pTmp = 0;
            blankPad(acDoc, SIZ_TRANSFER_DOC);
            memcpy(pOutbuf+OFF_TRANSFER_DOC, acDoc, SIZ_TRANSFER_DOC);
            memcpy(pOutbuf+OFF_TRANSFER_DT, acDate, SIZ_TRANSFER_DT);
         } else
         {
            // 001912046  974011
            // 928422   2000815
            // 114/112 1969
            // 114/112 936697
            // 982344 982345 06/03/98
            // Use first token and drop everything else
            if (*(apItems[0]+3) == '/' || *(apItems[0]+3) == '-')
            {
               strcpy(acTmp1, apItems[0]);
               acTmp1[3] = '-';
               if (pTmp = strchr(acTmp1, ','))
                  *pTmp = 0;
               sprintf(acDoc, "%s           ", acTmp1);
               memcpy(pOutbuf+OFF_TRANSFER_DOC, acDoc, SIZ_TRANSFER_DOC);
               lTmp = atol(apItems[1]);
               if (lTmp > 1940 && lTmp < 2009)
               {
                  sprintf(acDate, "%s     ", apItems[1]);
                  memcpy(pOutbuf+OFF_TRANSFER_DT, acDate, SIZ_TRANSFER_DT);
               } else
                  memset(pOutbuf+OFF_TRANSFER_DT, ' ', SIZ_TRANSFER_DT);
            } else
            {
               if (iCnt > 2 && (pTmp = dateConversion(apItems[2], acDate, MMDDYY1)))
               {
                  // 982344 982345 06/03/98
                  memcpy(pOutbuf+OFF_TRANSFER_DT, acDate, SIZ_TRANSFER_DT);
                  strcpy(acTmp1, apItems[0]);
                  if (pTmp = strchr(acTmp1, ','))
                     *pTmp = 0;
                  sprintf(acDoc, "%s           ", acTmp1);
                  memcpy(pOutbuf+OFF_TRANSFER_DOC, acDoc, SIZ_TRANSFER_DOC);
               } else
               {
                  goto MPA_One_Token;
               }
            }
         }
      }
   } else if (isdigit(*apItems[0]) )
   {
      pTmp = NULL;
      if (iCnt > 2)
         pTmp = dateConversion(apItems[2], acDate, MMDDYY1);
      if (pTmp)
      {
         memcpy(pOutbuf+OFF_TRANSFER_DT, acDate, SIZ_TRANSFER_DT);
         strcpy(acDoc, apItems[0]);
         if (pTmp = strchr(acDoc, ','))
            *pTmp = 0;
         blankPad(acDoc, SIZ_TRANSFER_DOC);
         memcpy(pOutbuf+OFF_TRANSFER_DOC, acDoc, SIZ_TRANSFER_DOC);
      } else
      {
         goto MPA_One_Token;
      }
   }

   // HO Exempt
   *(pOutbuf+OFF_HO_FL) = '2';      // 'N'

   // Owner
   Mpa_MergeOwner(pOutbuf, pRollRec);
   Mpa_MergeMAdr(pOutbuf, pRollRec);
   Mpa_MergeSAdr(pOutbuf, pRollRec);

   return 0;
}

/********************************* Mpa_MergePubl ****************************
 *
 * Merge public parcel file to roll file
 *
 ****************************************************************************/

int Mpa_MergePubl(int iSkip)
{
   char     acRec[MAX_RECSIZE], acBuf[MAX_RECSIZE], acRollRec[MAX_RECSIZE];
   char     acRawFile[_MAX_PATH], acOutFile[_MAX_PATH], *pRoll;

   HANDLE   fhIn, fhOut;

   int      iRet, iTmp, iNewRec=0;
   DWORD    nBytesRead;
   DWORD    nBytesWritten;
   BOOL     bRet;
   long     lRet=0, lCnt=0;

   // Convert EBCDIC to ASCII
   strcpy(acOutFile, acPubParcelFile);
   if (pRoll = strrchr(acOutFile, '.'))
      strcpy(pRoll, ".ASC");
   else
      strcat(acOutFile, ".ASC");

   LogMsg("Translate %s to Ascii %s", acPubParcelFile, acOutFile);
   iRet = doEBC2ASCAddCR(acPubParcelFile, acOutFile, iRollLen);
   if (iRet)
   {
      LogMsg("Error converting %s to %s", acPubParcelFile, acOutFile);
      return iRet;
   }
   strcpy(acPubParcelFile, acOutFile);
   
   // Preparing output file
   sprintf(acRawFile, acRawTmpl, "Mpa", "Mpa", "Tmp");
   sprintf(acOutFile, acRawTmpl, "Mpa", "Mpa", "R01");

   // Remove tmp file
   if (!_access(acRawFile, 0))
      remove(acRawFile);

   // Make sure R01 file is avail.
   if (_access(acOutFile, 0))
   {
      LogMsg("***** Missing input file %s.  Please recheck!", acOutFile);
      return -1;
   }

   // Rename current R01 to TMP file
   rename(acOutFile, acRawFile);

   // Open unassessed file
   LogMsg("Open unassessed file %s", acPubParcelFile);
   fdRoll = fopen(acPubParcelFile, "r");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening unassessed file: %s\n", acPubParcelFile);
      return -2;
   }

   // Open Input file
   LogMsg("Open input file %s", acRawFile);
   fhIn = CreateFile(acRawFile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhIn == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening input file: %s\n", acRawFile);
      return -3;
   }

   // Open Output file
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, FILE_SHARE_READ, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening output file: %s\n", acOutFile);
      return -4;
   }

   // Copy skip record
   memset(acBuf, ' ', iRecLen);
   while (iSkip-- > 0)
   {
      ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);
      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
   }

   iNoMatch=iBadCity=iBadSuffix=0;

   // Get first rec - ignore blank record
   do {
      pRoll = fgets(acRollRec, MAX_RECSIZE, fdRoll);
      if (pRoll && *pRoll == ' ')
         LogMsg("*** Bad APN: '%.80s'", pRoll);
   } while (pRoll && *pRoll == ' ');

   // Merge loop
   while (pRoll)
   {
      bRet = ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);
      if (!bRet)
      {
         LogMsg("***** Error reading input file %s (%f)", acRawFile, GetLastError());
         break;
      }

      if (iRecLen != nBytesRead)
         break;

      NextRollRec:
      iTmp = memcmp(acBuf, &acRollRec[0], iApnLen);
      if (iTmp < 0)
      {
         // Write existing rec out
         bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
         lCnt++;
      } else if (iTmp > 0)       // Insert new roll record
      {
         // Create new R01 record from unassessed record
         iRet = Mpa_CreatePublR01(acRec, acRollRec, 1);
         iNewRec++;

         bRet = WriteFile(fhOut, acRec, iRecLen, &nBytesWritten, NULL);
         if (!(++lCnt % 1000))
            printf("\r%u", lCnt);

         pRoll = fgets(acRollRec, MAX_RECSIZE, fdRoll);
         if (!pRoll)
            break;
         else
            goto NextRollRec;
      } else
      {
         // This should not occur
         LogMsg0("***** Duplicate record found: %.*s (%d).  Throw away old one", iApnLen, acBuf, lCnt);
      }

      if (!(lCnt % 1000))
         printf("\r%u", lCnt);

      if (!bRet)
      {
         LogMsg("***** Error occurs: %d\n", GetLastError());
         lRet = WRITE_ERR;
         break;
      }
   }

   while (bRet && (iRecLen == nBytesRead))
   {
      // Write existing rec out
      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
      lCnt++;

      bRet = ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);
   }

   // If there are more new records, add them
   while (pRoll)
   {
      // Create new R01 record
      iRet = Mpa_CreatePublR01(acRec, acRollRec, 1);
      iNewRec++;

      bRet = WriteFile(fhOut, acRec, iRecLen, &nBytesWritten, NULL);
      lCnt++;

      // Get next roll record
      pRoll = fgets(acRollRec, MAX_RECSIZE, fdRoll);
   }

   // Close files
   if (fdRoll)
      fclose(fdRoll);
   if (fhOut)
      CloseHandle(fhOut);
   if (fhIn)
      CloseHandle(fhIn);

   LogMsg("Total output records:       %u", lCnt);
   LogMsg("Total unassessed records:   %u", iNewRec);
   LogMsg("Total bad-city records:     %u", iBadCity);
   LogMsg("Total bad-suffix records:   %u", iBadSuffix);

   printf("\nTotal output records: %u\n", lCnt);

   lRecCnt = lCnt;

   return 0;
}

int Mpa_MergePubl_Pre2012(int iSkip)
{
   char     acRec[MAX_RECSIZE], acBuf[MAX_RECSIZE], acRollRec[1024];
   char     acRawFile[_MAX_PATH], acOutFile[_MAX_PATH], *pRoll;

   HANDLE   fhIn, fhOut;

   int      iRet, iTmp, iNewRec=0;
   DWORD    nBytesRead;
   DWORD    nBytesWritten;
   BOOL     bRet;
   long     lRet=0, lCnt=0;

   sprintf(acRawFile, acRawTmpl, "Mpa", "Mpa", "Tmp");
   sprintf(acOutFile, acRawTmpl, "Mpa", "Mpa", "R01");

   // Remove tmp file
   if (!_access(acRawFile, 0))
      remove(acRawFile);

   // Make sure R01 file is avail.
   if (_access(acOutFile, 0))
   {
      LogMsg("***** Missing input file %s.  Please recheck!", acOutFile);
      return 1;
   }

   // Rename current R01 to TMP file
   rename(acOutFile, acRawFile);

   // Open unassessed file
   LogMsg("Open unassessed file %s", acPubParcelFile);
   fdRoll = fopen(acPubParcelFile, "r");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening unassessed file: %s\n", acPubParcelFile);
      return 2;
   }

   // Open Input file
   LogMsg("Open input file %s", acRawFile);
   fhIn = CreateFile(acRawFile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhIn == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening input file: %s\n", acRawFile);
      return 3;
   }

   // Open Output file
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, FILE_SHARE_READ, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening output file: %s\n", acOutFile);
      return 4;
   }

   // Copy skip record
   memset(acBuf, ' ', iRecLen);
   while (iSkip-- > 0)
   {
      ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);
      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
   }

   iNoMatch=iBadCity=iBadSuffix=0;

   // Get first rec
   pRoll = fgets(acRollRec, 1024, fdRoll);

   // Merge loop
   while (pRoll)
   {
      bRet = ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);
      if (!bRet)
      {
         LogMsg("***** Error reading input file %s (%f)", acRawFile, GetLastError());
         break;
      }

      if (iRecLen != nBytesRead)
         break;

      NextRollRec:
      iTmp = memcmp(acBuf, &acRollRec[1], iApnLen);
      if (iTmp < 0)
      {
         // Write existing rec out
         bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
         lCnt++;
      } else if (iTmp > 0)       // Insert new roll record
      {
         // Create new R01 record from unassessed record
         iRet = Mpa_CreatePublR01(acRec, acRollRec);
         iNewRec++;

         bRet = WriteFile(fhOut, acRec, iRecLen, &nBytesWritten, NULL);
         if (!(++lCnt % 1000))
            printf("\r%u", lCnt);

         pRoll = fgets(acRollRec, 1024, fdRoll);
         if (!pRoll)
            break;
         else
            goto NextRollRec;
      } else
      {
         // This should not occur
         LogMsg0("***** Duplicate record found: %.*s (%d).  Throw away old one", iApnLen, acBuf, lCnt);
      }

      if (!(lCnt % 1000))
         printf("\r%u", lCnt);

      if (!bRet)
      {
         LogMsg("***** Error occurs: %d\n", GetLastError());
         lRet = WRITE_ERR;
         break;
      }
   }

   while (bRet && (iRecLen == nBytesRead))
   {
      // Write existing rec out
      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
      lCnt++;

      bRet = ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);
   }

   // If there are more new records, add them
   while (pRoll)
   {
      // Create new R01 record
      iRet = Mpa_CreatePublR01(acRec, acRollRec);
      iNewRec++;

      bRet = WriteFile(fhOut, acRec, iRecLen, &nBytesWritten, NULL);
      lCnt++;

      // Get next roll record
      pRoll = fgets(acRollRec, 1024, fdRoll);
   }

   // Close files
   if (fdRoll)
      fclose(fdRoll);
   if (fhOut)
      CloseHandle(fhOut);
   if (fhIn)
      CloseHandle(fhIn);

   //
   LogMsg("Total output records:       %u", lCnt);
   LogMsg("Total unassessed records:   %u", iNewRec);
   LogMsg("Total bad-city records:     %u", iBadCity);
   LogMsg("Total bad-suffix records:   %u", iBadSuffix);

   printf("\nTotal output records: %u\n", lCnt);

   lRecCnt = lCnt;

   return 0;
}

/***************************** Mpa_ExtrImprSale *****************************
 *
 * Extract impr sale file
 * Output: Attr_Exp.Impr, Impr_Sale.dat
 *
 * Use Book-Page_Parcel for APN
 *
 ****************************************************************************/

int Mpa_ExtrImprSale(char *pMdbFile)
{
   hlAdo    hChar;
   hlAdoRs  rsChar;
   char     acTmp[_MAX_PATH], acCharTbl[64], acTmpFile[_MAX_PATH];
   char     acSaleBuf[512], acCharBuf[512], *pTmp;
   bool     bRet;
   int      iRet, iTmp, lTmp, iCnt=0;
   double   dTmp;

   sprintf(acTmp, acESalTmpl, "MPA", "Impr", "Dat");
   LogMsg0("Convert improved sale file %s to %s", pMdbFile, acTmp);

   // Prepare sale output file
   if (!(fdSale = fopen(acTmp, "w")))
   {
      LogMsg("***** Error creating Sale file: %s", acTmp);
      return -2;
   }

   // Prepare char output file
   sprintf(acTmpFile, acAttrTmpl, "MPA", "Tmp");
   if (!(fdChar = fopen(acTmpFile, "w")))
   {
      LogMsg("***** Error creating CHAR file: %s", acTmpFile);
      return -1;
   }

   // Open char file
   LogMsg("Open Improved Sale file %s", pMdbFile);
   try
   {
      strcpy(acTmp, acMdbProvider);
      strcat(acTmp, pMdbFile);
      strcat(acTmp, ";");

      bRet = hChar.Connect(acTmp);
      if (bRet)
      {
         GetIniString("MPA", "ImprTbl", "", acCharTbl, _MAX_PATH, acIniFile);
         sprintf(acTmp, "SELECT * FROM %s", acCharTbl);
         LogMsg("%s", acTmp);
         rsChar.Open(hChar, acTmp);
         //bRet = rsChar.next();
      }
   } AdoCatch(e)
   {
      LogMsg("***** Error opening %s (%s)", pMdbFile, ComError(e));
      return -1;
   }

   CString   sApn, sTmp;
   MPA_CHAR  *pChar = (MPA_CHAR *) &acCharBuf[0];
   SCSAL_REC *pSale = (SCSAL_REC *)&acSaleBuf[0];

   iRet = iCnt = 0;
   while (rsChar.next())
   {
      // Init output buffer
      memset(acCharBuf, ' ', sizeof(MPA_CHAR));
      memset(acSaleBuf, ' ', sizeof(SCSAL_REC));

      // Format APN
      //sApn = rsChar.GetItem("APN");
      //iTmp = atol(sApn);
      //if (!iTmp)
         sApn = rsChar.GetItem("BOOK") + rsChar.GetItem("PAGE") + rsChar.GetItem("PARCEL");
      //else
      //   sApn.Format("%.9d", iTmp);

      // Skip record without APN
      if (sApn.GetLength() < 9)
         continue;
      if (sApn.GetLength() == 9)
         sApn = sApn + "0";


#ifdef _DEBUG
      //if (sApn == "00101114")
      //   iTmp = 0;
#endif
      memcpy(pChar->Apn, sApn.GetBuffer(0), sApn.GetLength());
      memcpy(pSale->Apn, sApn.GetBuffer(0), sApn.GetLength());

      // Get sale date & format it to yyyymmdd
      sTmp = rsChar.GetItem("DATE");
      if (pTmp = dateConversion(sTmp.GetBuffer(0), acTmp, MM_DD_YYYY_1))
      {
         if (strlen(pTmp) != 8)
            LogMsg("*** Invalid date: %s on APN %s", sTmp, sApn);
         else
         {
            memcpy(pChar->SaleDate, acTmp, 8);
            memcpy(pSale->DocDate, acTmp, 8);
         }
      }

      // Get sale price & format it
      sTmp = rsChar.GetItem("SALE PRICE");
      lTmp = atol(sTmp);
      if (lTmp > 0)
      {
         iTmp = sprintf(acTmp, "%*d", MPA_CSIZ_PRICE, lTmp);
         memcpy(pChar->SalePrice, acTmp, iTmp);
         memcpy(pSale->SalePrice, acTmp, iTmp);
      }

      // Land
      sTmp = rsChar.GetItem("LAND VALUE");
      memcpy(pChar->Land, sTmp, sTmp.GetLength());

      // Other Impr
      sTmp = rsChar.GetItem("OTHER IMP VALUE");
      iTmp = atol(sTmp);
      if (iTmp > 0)
         memcpy(pChar->OthImpr, sTmp, sTmp.GetLength());

      // Get ACRES & format it by multiply to 1000
      sTmp = rsChar.GetItem("ACRES");
      if (!sTmp.IsEmpty())
      {
         dTmp = atof(sTmp);
         iTmp = sprintf(acTmp, "%*d", MPA_CSIZ_ACRES, (long)(dTmp * 1000.0));
         memcpy(pChar->Acres, acTmp, iTmp);
      }

      // YrBlt
      sTmp = rsChar.GetItem("YB");
      iTmp = sTmp.GetLength();
      if (iTmp > 1)
      {
         if (iTmp == 4)
            memcpy(pChar->YrBlt, sTmp.GetBuffer(0), iTmp);
         else
         {
            pTmp = dateConversion(sTmp.GetBuffer(0), acTmp, YY2YYYY);
            memcpy(pChar->YrBlt, acTmp, MPA_CSIZ_YRBLT);
         }
      }

      // BldgSqft
      sTmp = rsChar.GetItem("SQ FT");
      memcpy(pChar->BldgSqft, sTmp, sTmp.GetLength());

      // Bed
      sTmp = rsChar.GetItem("BED");
      memcpy(pChar->Bed, sTmp, sTmp.GetLength());

      // Bath - may contain .5 for halfbath
      sTmp = rsChar.GetItem("BATH");
      memcpy(pChar->Bath, sTmp, sTmp.GetLength());

      // FirePlace code
      sTmp = rsChar.GetItem("FP");
      memcpy(pChar->Fp, sTmp, sTmp.GetLength());

      // Deck
      sTmp = rsChar.GetItem("DECK");
      memcpy(pChar->Deck, sTmp, sTmp.GetLength());

      // Pool
      sTmp = rsChar.GetItem("POOL");
      if (!sTmp.IsEmpty())
         pChar->Pool[0] = toupper(sTmp.GetAt(0));

      // Spa
      sTmp = rsChar.GetItem("SPA");
      if (!sTmp.IsEmpty())
         pChar->Spa[0] = toupper(sTmp.GetAt(0));

      // Bldg Class
      sTmp = rsChar.GetItem("CLASS");
      memcpy(pChar->BldgCls, sTmp, sTmp.GetLength());

      // Heat type
      sTmp = rsChar.GetItem("HEAT");
      memcpy(pChar->Heat, sTmp, sTmp.GetLength());

      // Garage
      sTmp = rsChar.GetItem("GAR");
      memcpy(pChar->Gar, sTmp, sTmp.GetLength());

      // Address
      sTmp = rsChar.GetItem("ADDRESS");
      memcpy(pChar->Address, sTmp, sTmp.GetLength());

      // Zoning
      sTmp = rsChar.GetItem("ZONING");
      memcpy(pChar->Zoning, sTmp, sTmp.GetLength());

      // Property Type
      sTmp = rsChar.GetItem("PROPTYPE");
      memcpy(pChar->PropType, sTmp, sTmp.GetLength());

      // When
      sTmp = rsChar.GetItem("WHEN");
      if (pTmp = dateConversion(sTmp.GetBuffer(0), acTmp, MM_DD_YYYY_1))
      {
         if (strlen(pTmp) != 8)
            LogMsg("*** Invalid date: %s on APN %s", sTmp, sApn);
         else
            memcpy(pChar->When, acTmp, 8);
      }

      // Remark
      sTmp = rsChar.GetItem("REMARKS");
      memcpy(pChar->Remarks, sTmp, sTmp.GetLength());

      pChar->CrLf[0] = '\n';
      pChar->CrLf[1] = 0;

      // Output to Char file
      fputs(acCharBuf, fdChar);

      pSale->ARCode = 'A';
      pSale->CRLF[0] = '\n';
      pSale->CRLF[1] = 0;

      // Output to Sale file
      fputs(acSaleBuf, fdSale);

      if (!(++iCnt % 1000))
         printf("\r%u", iCnt);
   }

   rsChar.Close();

   fclose(fdChar);
   fclose(fdSale);

   // Sort output file on APN and date
   sprintf(acCharBuf, acAttrTmpl, "MPA", "Impr");
   sprintf(acTmp,"S(1,10,C,A,11,8,C,D) F(TXT) ");

   // Sort Attr_exp.Tmp to Attr_exp.Impr
   iRet = sortFile(acTmpFile, acCharBuf, acTmp);
   LogMsg("Total records extracted from %s: %d", pMdbFile, iRet);

   return iRet;
}

/**************************** Mpa_ExtrLandSale ******************************
 *
 * Extract land sale
 * Output: Attr_exp.Land, Land_Sale.dat
 *
 * Use Book-Page_Parcel if APN < 1
 *
 ****************************************************************************/

int Mpa_ExtrLandSale(char *pMdbFile)
{
   hlAdo    hChar;
   hlAdoRs  rsChar;
   char     acTmp[_MAX_PATH], acCharTbl[64], acTmpFile[_MAX_PATH];
   char     acSaleBuf[512], acCharBuf[512], *pTmp;
   bool     bRet;
   int      iRet, iTmp, lTmp, iCnt=0;
   double   dTmp;

   sprintf(acTmp, acESalTmpl, "MPA", "Land", "Dat");
   LogMsg0("Convert land sale file %s to %s", pMdbFile, acTmp);

   // Prepare sale output file
   if (!(fdSale = fopen(acTmp, "w")))
   {
      LogMsg("***** Error creating Sale file: %s", acTmp);
      return -2;
   }

   // Prepare char output file
   sprintf(acTmpFile, acAttrTmpl, "MPA", "Tmp");
   if (!(fdChar = fopen(acTmpFile, "w")))
   {
      LogMsg("***** Error creating CHAR file: %s", acTmpFile);
      return -1;
   }

   // Open char file
   LogMsg("Open CHAR file %s", pMdbFile);
   try
   {
      strcpy(acTmp, acMdbProvider);
      strcat(acTmp, pMdbFile);

      bRet = hChar.Connect(acTmp);
      if (bRet)
      {
         GetIniString("MPA", "LandTbl", "", acCharTbl, _MAX_PATH, acIniFile);
         sprintf(acTmp, "SELECT * FROM %s", acCharTbl);
         LogMsg("%s", acTmp);
         rsChar.Open(hChar, acTmp);
         //bRet = rsChar.next();
      }
   } AdoCatch(e)
   {
      LogMsg("***** Error opening %s (%s)", pMdbFile, ComError(e));
      return -1;
   }

   CString   sApn, sTmp;
   MPA_CHAR  *pChar = (MPA_CHAR *) &acCharBuf[0];
   SCSAL_REC *pSale = (SCSAL_REC *)&acSaleBuf[0];

   iRet = iCnt = 0;
   while (rsChar.next())
   {
      // Init output buffer
      memset(acCharBuf, ' ', sizeof(MPA_CHAR));
      memset(acSaleBuf, ' ', sizeof(SCSAL_REC));

      // Format APN
      sApn = rsChar.GetItem("APN");
      iTmp = atol(sApn);
      if (!iTmp)
         sApn = rsChar.GetItem("BOOK") + rsChar.GetItem("PAGE") + rsChar.GetItem("PARCEL");
      else
         sApn.Format("%.9d", iTmp);

      // Skip record without APN
      if (sApn.GetLength() < 9)
         continue;
      if (sApn.GetLength() == 9)
         sApn = sApn + "0";


#ifdef _DEBUG
      //if (sApn == "020230022")
      //   iTmp = 0;
#endif
      memcpy(pChar->Apn, sApn.GetBuffer(0), sApn.GetLength());
      memcpy(pSale->Apn, sApn.GetBuffer(0), sApn.GetLength());

      // Get sale date & format it to yyyymmdd
      sTmp = rsChar.GetItem("SALE DATE");
      if (pTmp = dateConversion(sTmp.GetBuffer(0), acTmp, MM_DD_YYYY_1))
      {
         if (strlen(pTmp) != 8)
            LogMsg("*** Invalid date: %s on APN %s", sTmp, sApn);
         else
         {
            memcpy(pChar->SaleDate, acTmp, 8);
            memcpy(pSale->DocDate, acTmp, 8);
         }
      }

      // Get sale price & format it
      sTmp = rsChar.GetItem("SALE PRICE");
      lTmp = atol(sTmp);
      if (lTmp > 0)
      {
         iTmp = sprintf(acTmp, "%*d", MPA_CSIZ_PRICE, lTmp);
         memcpy(pChar->SalePrice, acTmp, iTmp);
         memcpy(pSale->SalePrice, acTmp, iTmp);
      }

      // Get ACRES & format it by multiply to 1000
      sTmp = rsChar.GetItem("ACRES");
      if (!sTmp.IsEmpty())
      {
         dTmp = atof(sTmp);
         iTmp = sprintf(acTmp, "%*d", MPA_CSIZ_ACRES, (long)(dTmp * 1000.0));
         memcpy(pChar->Acres, acTmp, iTmp);
      }

      // Zoning
      sTmp = rsChar.GetItem("ZONING");
      memcpy(pChar->Zoning, sTmp, sTmp.GetLength());

      // Property Type
      sTmp = rsChar.GetItem("PROPERTY TYPE");
      memcpy(pChar->PropType, sTmp, sTmp.GetLength());

      // Utilities
      sTmp = rsChar.GetItem("UTILITIES");
      memcpy(pChar->Utilities, sTmp, sTmp.GetLength());

      // Access
      sTmp = rsChar.GetItem("ACCESS");
      memcpy(pChar->Access, sTmp, sTmp.GetLength());

      // Topography
      sTmp = rsChar.GetItem("TOPOGRAPHY");
      memcpy(pChar->Topography, sTmp, sTmp.GetLength());

      // When
      sTmp = rsChar.GetItem("WHEN");
      if (pTmp = dateConversion(sTmp.GetBuffer(0), acTmp, MM_DD_YYYY_1))
      {
         if (strlen(pTmp) != 8)
            LogMsg("*** Invalid date: %s on APN %s", sTmp, sApn);
         else
            memcpy(pChar->When, acTmp, 8);
      }

      // Remark
      sTmp = rsChar.GetItem("REMARKS");
      memcpy(pChar->Remarks, sTmp, sTmp.GetLength());

      // Address
      sTmp = rsChar.GetItem("ADDRESS-AREA");
      memcpy(pChar->Address, sTmp, sTmp.GetLength());

      pChar->CrLf[0] = '\n';
      pChar->CrLf[1] = 0;

      // Output to Char file
      fputs(acCharBuf, fdChar);

      pSale->ARCode = 'A';
      pSale->CRLF[0] = '\n';
      pSale->CRLF[1] = 0;

      // Output to Sale file
      fputs(acSaleBuf, fdSale);

      if (!(++iCnt % 1000))
         printf("\r%u", iCnt);
   }

   rsChar.Close();

   fclose(fdChar);
   fclose(fdSale);

   // Sort output file on APN and date
   sprintf(acCharBuf, acAttrTmpl, "MPA", "Land");
   sprintf(acTmp,"S(1,10,C,A,11,8,C,D) F(TXT) ");

   // Sort Attr_exp.Tmp to Attr_exp.Land
   iRet = sortFile(acTmpFile, acCharBuf, acTmp);
   LogMsg("Total records extracted from %s: %d", pMdbFile, iRet);

   return iRet;
}

/***************************** Mpa_ExtrCommSale *****************************
 *
 * Extract commercial sale file
 * Output: Attr_exp.comm, Comm_Sale.dat
 *
 * Use Book-Page_Parcel for APN
 *
 ****************************************************************************/

int Mpa_ExtrCommSale(char *pMdbFile)
{
   hlAdo    hChar;
   hlAdoRs  rsChar;
   char     acTmp[_MAX_PATH], acCharTbl[64], acTmpFile[_MAX_PATH];
   char     acSaleBuf[512], acCharBuf[512], *pTmp;
   bool     bRet;
   int      iRet, iTmp, lTmp, iCnt=0;
   double   dTmp;

   sprintf(acTmp, acESalTmpl, "MPA", "Comm", "Dat");
   LogMsg0("Convert commercial sale file %s to %s", pMdbFile, acTmp);

   // Prepare sale output file
   if (!(fdSale = fopen(acTmp, "w")))
   {
      LogMsg("***** Error creating Sale file: %s", acTmp);
      return -2;
   }

   // Prepare char output file
   sprintf(acTmpFile, acAttrTmpl, "MPA", "Tmp");
   if (!(fdChar = fopen(acTmpFile, "w")))
   {
      LogMsg("***** Error creating CHAR file: %s", acTmpFile);
      return -1;
   }

   // Open MDB file
   LogMsg("Open Commercial Sale file %s", pMdbFile);
   try
   {
      strcpy(acTmp, acMdbProvider);
      strcat(acTmp, pMdbFile);
      strcat(acTmp, ";");

      bRet = hChar.Connect(acTmp);
      if (bRet)
      {
         GetIniString("MPA", "CommTbl", "", acCharTbl, _MAX_PATH, acIniFile);
         sprintf(acTmp, "SELECT * FROM %s", acCharTbl);
         LogMsg("%s", acTmp);
         rsChar.Open(hChar, acTmp);
      }
   } AdoCatch(e)
   {
      LogMsg("***** Error opening %s (%s)", pMdbFile, ComError(e));
      return -1;
   }

   CString   sApn, sTmp;
   MPA_CHAR  *pChar = (MPA_CHAR *) &acCharBuf[0];
   SCSAL_REC *pSale = (SCSAL_REC *)&acSaleBuf[0];

   iRet = iCnt = 0;
   while (rsChar.next())
   {
      // Init output buffer
      memset(acCharBuf, ' ', sizeof(MPA_CHAR));
      memset(acSaleBuf, ' ', sizeof(SCSAL_REC));

      // Format APN
      sApn = rsChar.GetItem("BOOK") + rsChar.GetItem("PAGE") + rsChar.GetItem("PARCEL");

      // Skip record without APN
      if (sApn.GetLength() < 9)
         continue;
      if (sApn.GetLength() == 9)
         sApn = sApn + "0";

#ifdef _DEBUG
      //if (sApn == "00101114")
      //   iTmp = 0;
#endif
      memcpy(pChar->Apn, sApn.GetBuffer(0), sApn.GetLength());
      memcpy(pSale->Apn, sApn.GetBuffer(0), sApn.GetLength());

      // Get sale date & format it to yyyymmdd
      sTmp = rsChar.GetItem("DATE");
      if (pTmp = dateConversion(sTmp.GetBuffer(0), acTmp, MM_DD_YYYY_1))
      {
         if (strlen(pTmp) != 8)
            LogMsg("*** Invalid date: %s on APN %s", sTmp, sApn);
         else
         {
            memcpy(pChar->SaleDate, acTmp, 8);
            memcpy(pSale->DocDate, acTmp, 8);
         }
      }

      // Get sale price & format it
      sTmp = rsChar.GetItem("SALE PRICE");
      lTmp = atol(sTmp);
      if (lTmp > 0)
      {
         iTmp = sprintf(acTmp, "%*d", MPA_CSIZ_PRICE, lTmp);
         memcpy(pChar->SalePrice, acTmp, iTmp);
         memcpy(pSale->SalePrice, acTmp, iTmp);
      }

      // Land
      sTmp = rsChar.GetItem("LAND VALUE");
      memcpy(pChar->Land, sTmp, sTmp.GetLength());

      // Get ACRES & format it by multiply to 1000
      sTmp = rsChar.GetItem("TOTAL ACRES");
      if (!sTmp.IsEmpty())
      {
         dTmp = atof(sTmp);
         iTmp = sprintf(acTmp, "%*d", MPA_CSIZ_ACRES, (long)(dTmp * 1000.0));
         memcpy(pChar->Acres, acTmp, iTmp);
      }

      // YrBlt
      sTmp = rsChar.GetItem("YR BLT");
      iTmp = sTmp.GetLength();
      if (iTmp > 1)
      {
         if (iTmp == 4)
            memcpy(pChar->YrBlt, sTmp.GetBuffer(0), iTmp);
         else
         {
            pTmp = dateConversion(sTmp.GetBuffer(0), acTmp, YY2YYYY);
            memcpy(pChar->YrBlt, acTmp, MPA_CSIZ_YRBLT);
         }
      }

      // BldgSqft
      sTmp = rsChar.GetItem("GROSS SQ FT");
      memcpy(pChar->BldgSqft, sTmp, sTmp.GetLength());

      //// Bed
      //sTmp = rsChar.GetItem("BED");
      //memcpy(pChar->Bed, sTmp, sTmp.GetLength());

      //// Bath - may contain .5 for halfbath
      //sTmp = rsChar.GetItem("BATH");
      //memcpy(pChar->Bath, sTmp, sTmp.GetLength());

      //// FirePlace code
      //sTmp = rsChar.GetItem("FP");
      //memcpy(pChar->Fp, sTmp, sTmp.GetLength());

      //// Deck
      //sTmp = rsChar.GetItem("DECK");
      //memcpy(pChar->Deck, sTmp, sTmp.GetLength());

      //// Pool
      //sTmp = rsChar.GetItem("POOL");
      //if (!sTmp.IsEmpty())
      //   pChar->Pool[0] = toupper(sTmp.GetAt(0));

      //// Spa
      //sTmp = rsChar.GetItem("SPA");
      //if (!sTmp.IsEmpty())
      //   pChar->Spa[0] = toupper(sTmp.GetAt(0));

      // Bldg Class
      sTmp = rsChar.GetItem("BLDG CLASS");
      memcpy(pChar->BldgCls, sTmp, sTmp.GetLength());

      //// Heat type
      //sTmp = rsChar.GetItem("HEAT");
      //memcpy(pChar->Heat, sTmp, sTmp.GetLength());

      //// Garage
      //sTmp = rsChar.GetItem("GAR");
      //memcpy(pChar->Gar, sTmp, sTmp.GetLength());

      // Address
      sTmp = rsChar.GetItem("ADDRESS");
      memcpy(pChar->Address, sTmp, sTmp.GetLength());

      // Zoning
      sTmp = rsChar.GetItem("ZONING");
      memcpy(pChar->Zoning, sTmp, sTmp.GetLength());

      // Property Type
      sTmp = rsChar.GetItem("PROPTYPE");
      memcpy(pChar->PropType, sTmp, sTmp.GetLength());

      // When
      sTmp = rsChar.GetItem("WHEN");
      if (pTmp = dateConversion(sTmp.GetBuffer(0), acTmp, MM_DD_YYYY_1))
      {
         if (strlen(pTmp) != 8)
            LogMsg("*** Invalid date: %s on APN %s", sTmp, sApn);
         else
            memcpy(pChar->When, acTmp, 8);
      }

      // Remark
      sTmp = rsChar.GetItem("COMMENTS");
      memcpy(pChar->Remarks, sTmp, sTmp.GetLength());

      pChar->CrLf[0] = '\n';
      pChar->CrLf[1] = 0;

      // Output to Char file
      fputs(acCharBuf, fdChar);

      pSale->ARCode = 'A';
      pSale->CRLF[0] = '\n';
      pSale->CRLF[1] = 0;

      // Output to Sale file
      fputs(acSaleBuf, fdSale);

      if (!(++iCnt % 1000))
         printf("\r%u", iCnt);
   }

   rsChar.Close();

   fclose(fdChar);
   fclose(fdSale);

   LogMsg("Total records processed: %d", iCnt);

   // Sort output file on APN and date
   sprintf(acCharBuf, acAttrTmpl, "MPA", "Comm");
   sprintf(acTmp,"S(1,10,C,A,11,8,C,D) F(TXT) ");

   // Sort Attr_exp.Tmp to Attr_exp.Impr
   iRet = sortFile(acTmpFile, acCharBuf, acTmp);
   LogMsg("Total records extracted from %s: %d", pMdbFile, iRet);

   return iRet;
}

/******************************* Mpa_CreateLienRec ***************************
 *
 * Return 0 if successful, >0 if error
 *
 *****************************************************************************/

int Mpa_CreateLienRec(char *pOutbuf, char *pRollRec)
{
   char     acTmp[256];
   long     lTmp;
   int      iTmp;
   MPA_ROLL *pRec = (MPA_ROLL *)pRollRec;
   LIENEXTR *pLienRec = (LIENEXTR *)pOutbuf;

   // Clear output buffer
   memset(pOutbuf, ' ', sizeof(LIENEXTR));

   // Start copying data
   memcpy(pLienRec->acApn, pRec->Apn, MPA_SIZ_APN);

   // TRA
   lTmp = atoin(pRec->Tax_Area, MPA_SIZ_TAX_AREA);
   if (lTmp > 0)
   {
      iTmp = sprintf(acTmp, "%.6d", lTmp);
      memcpy(pLienRec->acTRA, acTmp, iTmp);
   }

   // Year assessed
   memcpy(pLienRec->acYear, myCounty.acYearAssd, 4);

   // Land
   long lLand = atoin(pRec->Land, MPA_SIZ_LAND);
   if (lLand > 0)
   {
      iTmp = sprintf(acTmp, "%*d", sizeof(pLienRec->acLand), lLand);
      memcpy(pLienRec->acLand, acTmp, iTmp);
   }

   // Improve - Ratio
   long lImpr = atoin(pRec->Improvements, MPA_SIZ_IMPROVEMENTS);
   if (lImpr > 0)
   {
      iTmp = sprintf(acTmp, "%*d", sizeof(pLienRec->acImpr), lImpr);
      memcpy(pLienRec->acImpr, acTmp, iTmp);

      iTmp = sprintf(acTmp, "%*d", sizeof(pLienRec->acRatio), (LONGLONG)lImpr*100/(lLand+lImpr));
      memcpy(pLienRec->acRatio, acTmp, iTmp);
   }

   // Timber value
   long lTimber = atoin(pRec->Timber, MPA_SIZ_TIMBER);

   // Williamson Land value
   long lWillLand = atoin(pRec->Will_Land, MPA_SIZ_WILL_LAND);

   // Williamson Market value
   long lWillMkt = atoin(pRec->Will_Market, MPA_SIZ_WILL_MARKET);

   // Tree/vines value
   long lTree = atoin(pRec->Trees_Vines, MPA_SIZ_TREES_VINES);

   // Total other
   long lOthers = lTimber + lWillLand + lWillMkt + lTree;
   if (lOthers > 0)
   {
      iTmp = sprintf(acTmp, "%*d", sizeof(pLienRec->acGross), lOthers);
      memcpy(pLienRec->acOther, acTmp, iTmp);

      if (lTimber > 0)
      {
         iTmp = sprintf(acTmp, "%*d", sizeof(pLienRec->extra.Mpa.Timber), lTimber);
         memcpy(pLienRec->extra.Mpa.Timber, acTmp, iTmp);
      }
      if (lWillLand > 0)
      {
         iTmp = sprintf(acTmp, "%*d", sizeof(pLienRec->extra.Mpa.Wil_Land), lWillLand);
         memcpy(pLienRec->extra.Mpa.Wil_Land, acTmp, iTmp);
      }
      if (lWillMkt > 0)
      {
         iTmp = sprintf(acTmp, "%*d", sizeof(pLienRec->extra.Mpa.Wil_Mkt), lWillMkt);
         memcpy(pLienRec->extra.Mpa.Wil_Mkt, acTmp, iTmp);
      }
      if (lTree > 0)
      {
         iTmp = sprintf(acTmp, "%*d", sizeof(pLienRec->extra.Mpa.TV_Val), lTree);
         memcpy(pLienRec->extra.Mpa.TV_Val, acTmp, iTmp);
      }
   }

   // Gross
   lTmp = lOthers + lLand + lImpr;
   if (lTmp > 0)
   {
      iTmp = sprintf(acTmp, "%*d", sizeof(pLienRec->acGross), lTmp);
      memcpy(pLienRec->acGross, acTmp, iTmp);
   }

   // HO Exe - Exemp total
   pLienRec->acHO[0] = '2';               // N
   long lExe = atoin(pRec->Exemption, MPA_SIZ_EXEMPTION);
   if (lExe > 0)
   {
      iTmp = sprintf(acTmp, "%*d", sizeof(pLienRec->acExAmt), lExe);
      memcpy(pLienRec->acExAmt, acTmp, iTmp);

      if (!memcmp(pRec->Exemp_Cd, "07", 2))
         pLienRec->acHO[0] = '1';         // Y
   } 
   
   // Exemption code
   memcpy(pLienRec->acExCode, pRec->Exemp_Cd, SIZ_LIEN_EXECODE);

   pLienRec->LF[0] = '\n';
   pLienRec->LF[1] = 0;
   return 0;
}

/********************************* Mpa_ExtrLien *****************************
 *
 *
 ****************************************************************************/

int Mpa_ExtrLien()
{
   char  acBuf[MAX_RECSIZE], acRollRec[MAX_RECSIZE], acOutFile[_MAX_PATH];
   FILE  *fdLien;
   long  lCnt=0, iRet;

   // Open roll file
   LogMsg("Open Roll file %s", acRollFile);
   fdRoll = fopen(acRollFile, "rb");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", acRollFile);
      return 2;
   }

   // Open Output file
   sprintf(acOutFile, acLienTmpl, myCounty.acCntyCode, myCounty.acCntyCode);
   LogMsg("Open output file %s", acOutFile);
   fdLien = fopen(acOutFile, "w");
   if (fdLien == NULL)
   {
      LogMsg("***** Error opening Lien extract file: %s\n", acOutFile);
      return 4;
   }

   // Merge loop
   while (!feof(fdRoll))
   {
      // Get roll rec
      iRet = fread(acRollRec,  1, iRollLen, fdRoll);
      if (iRollLen != iRet)
         break;

      // Create new lien record
      Mpa_CreateLienRec(acBuf, acRollRec);

      fputs(acBuf, fdLien);
      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdRoll)
      fclose(fdRoll);
   if (fdLien)
      fclose(fdLien);

   printf("\nTotal records output:     %d\n", lCnt);
   LogMsg("Total records output:     %d\n", lCnt);

   lRecCnt = lCnt;
   return 0;
}

/******************************* Mpa_MakeDocLink *******************************
 *
 * Format DocLink
 *
 ******************************************************************************/

void Mpa_MakeDocLink(LPSTR pDocLink, LPSTR pDoc, LPSTR pDate)
{
   char  acTmp[256], acDocName[256];
   int   iRet=0;

   *pDocLink = 0;
   if (*pDoc > ' ' && strlen(pDoc) == 10)
   {
      iRet = sprintf(acDocName, "%.4s\\%.3s\\%s", pDoc, pDoc+4, pDoc);
      sprintf(acTmp, "%s\\%s.pdf", acDocPath, acDocName);

      if (!_access(acTmp, 0))
         strcpy(pDocLink, acDocName);
   }
}

/******************************* Mpa_FmtDocLinks ****************************
 *
 * Format DocLinks as ,,,yyyy/bbb/yyyybbbppp
 * Only DocXferlink is populated since Sale1-Sale3 has no DocNum
 *
 ****************************************************************************

int Mpa_FmtDocLinks(int iSkip)
{
   char     acBuf[MAX_RECSIZE], acRawFile[_MAX_PATH], acOutFile[_MAX_PATH],
            acDocLink[256], acDocLinks[256], acDoc[32];

   HANDLE   fhIn, fhOut;

   DWORD    nBytesRead;
   DWORD    nBytesWritten;
   BOOL     bRet;
   long     lCnt=0, lLinkCnt=0;
   int      iTmp, iRet = 0;

   LogMsg("Format Doclinks ...");

   sprintf(acRawFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "R01");
   sprintf(acOutFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "N01");

   // Open Input file
   LogMsg("Open input file %s", acRawFile);
   fhIn = CreateFile(acRawFile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhIn == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening input file: %s\n", acRawFile);
      return -3;
   }
   // Open Output file
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening input file: %s\n", acOutFile);
      return -4;
   }

   // Copy skip record
   memset(acBuf, ' ', MAX_RECSIZE);
   while (iSkip-- > 0)
   {
      ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);
      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
   }

   // Merge loop
   while (true)
   {
      bRet = ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);

      // Check for EOF
      if (!bRet)
      {
         LogMsg("***** Error reading input file %s (%f)", acRawFile, GetLastError());
         break;
      }

      if (!nBytesRead)
         break;

      if (acBuf[OFF_TRANSFER_DOC] > ' ' && acBuf[OFF_TRANSFER_DT] > ' ')
      {
         // Reset old links
         memset((char *)&acBuf[OFF_DOCLINKS], ' ', SIZ_DOCLINKS);
         acDocLinks[0] = 0;

         // Sale1
         if (acBuf[OFF_SALE1_DOC] > ' ')
         {
            memcpy(acDoc, (char *)&acBuf[OFF_SALE1_DOC], SIZ_TRANSFER_DOC);
            myTrim(acDoc, SIZ_TRANSFER_DOC);
            iTmp = Mpa_MakeDocLink(acDocLink, acDoc, &acBuf[OFF_SALE1_DT]);
            if (iTmp > 0)
               strcat(acDocLinks, acDocLink);
         }
         strcat(acDocLinks, ",");

         // Sale2
         if (acBuf[OFF_SALE2_DOC] > ' ')
         {
            memcpy(acDoc, (char *)&acBuf[OFF_SALE2_DOC], SIZ_TRANSFER_DOC);
            myTrim(acDoc, SIZ_TRANSFER_DOC);
            iTmp = Mpa_MakeDocLink(acDocLink, acDoc, &acBuf[OFF_SALE2_DT]);
            if (iTmp > 0)
               strcat(acDocLinks, acDocLink);
         }
         strcat(acDocLinks, ",");

         // Sale3
         if (acBuf[OFF_SALE3_DOC] > ' ')
         {
            memcpy(acDoc, (char *)&acBuf[OFF_SALE3_DOC], SIZ_TRANSFER_DOC);
            myTrim(acDoc, SIZ_TRANSFER_DOC);
            iTmp = Mpa_MakeDocLink(acDocLink, acDoc, &acBuf[OFF_SALE1_DT]);
            if (iTmp > 0)
               strcat(acDocLinks, acDocLink);
         }
         strcat(acDocLinks, ",");

         // Transfer Doc
         memcpy(acDoc, (char *)&acBuf[OFF_TRANSFER_DOC], SIZ_TRANSFER_DOC);
         myTrim(acDoc, SIZ_TRANSFER_DOC);
         iTmp = Mpa_MakeDocLink(acDocLink, acDoc, &acBuf[OFF_TRANSFER_DT]);
         if (iTmp > 0)
         {
            lLinkCnt++;
            strcat(acDocLinks, acDocLink);
         }

         iTmp = strlen(acDocLinks);
         if (iTmp > 10)
            memcpy((char *)&acBuf[OFF_DOCLINKS], acDocLinks, iTmp);
      }

      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);

      if (!bRet)
      {
         LogMsg("***** Error occurs: %d\n", GetLastError());
         break;
      }
   }

   // Close files
   if (fhOut)
      CloseHandle(fhOut);
   if (fhIn)
      CloseHandle(fhIn);

   // Rename out file
   if (lCnt > 100)
   {
      iRet = MoveFileExA(acOutFile, acRawFile, MOVEFILE_REPLACE_EXISTING);
      if (!iRet)
      {
         LogMsg("***** Error renaming temp file: %s --> %s (%d)", acOutFile, acRawFile, errno);
         iRet = errno;
      } else
         iRet = 0;
   }

   LogMsg("FmtDocLinks completed.  %d doclinks assigned.\n", lLinkCnt);
   return iRet;
}

/******************************** Mpa_Roll2Sale *******************************
 *
 * Copy DOCNUM from roll file to sale file
 *
 ******************************************************************************/

int Mpa_Roll2Sale(char *pSaleFile)
{
   char     acSaleRec[SCSALREC_SIZE+2], acRollRec[G01_LEN+2], acTmpFile[_MAX_PATH], 
            acDocNum[32], acRawFile[_MAX_PATH], *pTmp;
   FILE     *fdIn, *fdOut;
   int      iRet, iTmp, iUpdated, lCnt;
   HANDLE   hRoll;
   DWORD    nBytesRead;
   bool     bEof;

   SCSAL_REC *pSale = (SCSAL_REC *)&acSaleRec[0];

   LogMsg("Match roll file for %s and copy DocNum to sale file", pSaleFile);

   // Open input file
   if (!(fdIn = fopen(pSaleFile, "r")))
   {
      LogMsg("***** Error opening %s", pSaleFile);
      return -1;
   }

   // Create output temp file
   strcpy(acTmpFile, pSaleFile);
   pTmp = strrchr(acTmpFile, '.');
   strcpy(pTmp, ".tmp");
   if (!(fdOut = fopen(acTmpFile, "w")))
   {
      LogMsg("***** Error creating %s", acTmpFile);
      fclose(fdIn);
      return -2;
   }

   // Open roll file
   sprintf(acRawFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "R01");
   LogMsg("Open input file %s", acRawFile);
   hRoll = CreateFile(acRawFile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (hRoll == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening %s in Mpa_Roll2Sale().  errno=%d", acRawFile, _errno);
      fclose(fdIn);
      fclose(fdOut);
      return -3;
   }

   // Skip first record as needed
   iRet = ReadFile(hRoll, acRollRec, iRecLen, &nBytesRead, NULL);
   if (!memcmp(acRollRec, "999999", 6))
      iRet = ReadFile(hRoll, acRollRec, iRecLen, &nBytesRead, NULL);

   // Initialize counters
   lCnt=iUpdated=0;
   bEof = false;

   // Loop through input file
   while (!bEof)
   {
      // Read input record
      if (!(pTmp = fgets(acSaleRec, SCSALREC_SIZE, fdIn)))
         break;

      // Skip if it's already matched
      if (pSale->DocNum[0] > ' ')
      {
         fputs(acSaleRec, fdOut);
         continue;
      }

#ifdef _DEBUG
      //if (!memcmp(pSale->Apn, "0010300180", 10))
      //   iRet = 0;
#endif
      // Match APN
      do
      {
         // Compare
         iRet = memcmp(acSaleRec, acRollRec, iApnLen);
         if (!iRet)
         {
            if (!memcmp(pSale->DocDate, &acRollRec[OFF_TRANSFER_DT], SIZ_TRANSFER_DT))
            {
               acRollRec[iApnLen] = 0;

               // Format DocNum
               iRet = Mpa_FmtDocNum(acDocNum, &acRollRec[OFF_TRANSFER_DOC], &acRollRec[OFF_TRANSFER_DT], acRollRec);
               if (iRet > 0)
               {
                  memcpy(pSale->DocNum, acDocNum, iRet);
                  iUpdated++;
               }
            }
            pSale->ApnMatched = 'Y';
            break;
         } else
         { 
            // Get next roll record
            if (iRet == 1)
            {
               // Read R01 record
               iTmp = ReadFile(hRoll, acRollRec, iRecLen, &nBytesRead, NULL);
               // Check for EOF
               if (!iTmp)
               {
                  LogMsg("***** Error reading roll file (%f)", GetLastError());
                  bEof = true;
                  break;
               }

               // EOF ?
               if (!nBytesRead)
               {
                  bEof = true;
                  break;
               }
            }
         } 
      } while (iRet > 0);
      
      // Output record
      fputs(acSaleRec, fdOut);

      if (!(lCnt++ % 1000))
         printf("\r%u", lCnt);
   }

   // Check for leftover
   while (!feof(fdIn))
   {
      // Read input record
      if (!(pTmp = fgets(acSaleRec, SCSALREC_SIZE, fdIn)))
         break;

      // Output record
      fputs(acSaleRec, fdOut);

      if (!(lCnt++ % 1000))
         printf("\r%u", lCnt);
   }

   if (fdIn)
      fclose(fdIn);
   if (fdOut)
      fclose(fdOut);
   if (hRoll)
      CloseHandle(hRoll);

   // If everything OK, rename output file to original file
   iRet = 0;
   if (iUpdated > 0)
   {
      iRet = MoveFileEx(acTmpFile, pSaleFile, MOVEFILE_REPLACE_EXISTING);
      if (!iRet)
      {
         LogMsg("***** Error renaming temp file: %s --> %s (%d)", acTmpFile, pSaleFile, errno);
         iRet = -10;
      } else
         iRet = 0;
   }

   LogMsg("Number of records read: %d", lCnt);
   LogMsg("               updated: %d", iUpdated);
   printf("\n");

   return iRet;
}

/****************************** Mpa_ParseTaxDelq *****************************
 *
 * Parse TX491 record and populate TAXDELQ
 * Use T491_TXREA1 for current delinquent amout which already includes all fee and penalties.
 *
 *****************************************************************************/

int Mpa_ParseTaxDelq(char *pOutbuf, char *pInbuf)
{
   char     *apItems[256];
   int      iTmp;
   double   dTmp, dTaxAmt, dPenAmt, dRedAmt;
   TAXDELQ  *pOutRec = (TAXDELQ *)pOutbuf;

   // Parse input tax data
   iTmp = ParseStringIQ(pInbuf, '|', MAX_FLD_TOKEN, apItems);
   if (iTmp < T491_TXBOND)
   {
      LogMsg("***** Error: bad Tax record for: %.50s (#tokens=%d)", pInbuf, iTmp);
      return -1;
   }

   // Clear output buffer
   memset(pOutbuf, 0, sizeof(TAXDELQ));

   // APN
   sprintf(pOutRec->Apn, "%s%s%s", apItems[T491_TXLK01], apItems[T491_TXLK02], apItems[T491_TXLK03]);

   // Updated date
   sprintf(pOutRec->Upd_Date, "%d", lLastTaxFileDate);

   // BillNum
   iTmp = atol(apItems[T491_TXBILL]);
   sprintf(pOutRec->BillNum, "%d", iTmp);

   // Year Default - Change this code after year 2049
   iTmp = atol(apItems[T491_TXYRDF]);
   if (iTmp > 0 && iTmp < 50)
   {
      sprintf(pOutRec->Def_Date, "20%.2d", iTmp);
      pOutRec->isDelq[0] = '1';

      // Tax Year
      sprintf(pOutRec->TaxYear, "20%.2d", iTmp-1);
   } else
      iTmp = 0;

   // Tax amt
   dTaxAmt = atof(apItems[T491_TXBSAM]);
   sprintf(pOutRec->Tax_Amt, "%.2f", dTaxAmt);

   // Redemption amt
   dRedAmt = atol(apItems[T491_TXREAM]);

   // Pen
   dPenAmt = atof(apItems[T491_TXPNLT]);
   sprintf(pOutRec->Pen_Amt, "%.2f", dPenAmt);

   // Fee
   dTmp =  atof(apItems[T491_TXCOST]);         // Delq Cost
   dTmp += atof(apItems[T491_TXREDE]);         // Redemption fee
   dTmp += atof(apItems[T491_TXMIFE]);         // Misc fee
   sprintf(pOutRec->Fee_Amt, "%.2f", dTmp);

   // Default amt
   dTmp = atof(apItems[T491_TXREA1]);
   sprintf(pOutRec->Def_Amt, "%.2f", dTmp);

   pOutRec->isDelq[0] = '0';
   if (dRedAmt > 0.0 && dTmp > 0.0)
      pOutRec->DelqStatus[0] = TAX_STAT_ONPAYMENT;
   else if (dRedAmt > 0)
      pOutRec->DelqStatus[0] = TAX_STAT_REDEEMED;
   else if (dTmp > 0.0)
   {
      pOutRec->DelqStatus[0] = TAX_STAT_UNPAID;
      pOutRec->isDelq[0] = '1';
   }

   // Redemption date
   if (*apItems[T491_TXREMM] > '0')
   {
      sprintf(pOutRec->Red_Date, "20%.2d%.2d%.2d", atol(apItems[T491_TXREYY]), atol(apItems[T491_TXREMM]), atol(apItems[T491_TXREDD]));
      if (dRedAmt > 0.0)
         sprintf(pOutRec->Red_Amt, "%.2f", dRedAmt);
   }

   return 0;
}

/*********************************** Mpa_NewTaxBase *******************************
 *
 * Create new TAXBASE record from T490 or Supplemental
 *
 **********************************************************************************/

int Mpa_NewTaxBase(CStringArray *asT490, char *pOutbuf)
{
   double   dTax1, dTax2, dPaid1, dPaid2, dPen1, dPen2, dTotalDue, dTotalTax;
   int      iTmp;
   CString  strTmp;
   TAXBASE  *pTaxBase = (TAXBASE *)pOutbuf;
   
   memset(pOutbuf, 0, sizeof(TAXBASE));
   sprintf(pTaxBase->Apn, "%s%s%s", asT490->GetAt(T490_TXLK01), asT490->GetAt(T490_TXLK02), asT490->GetAt(T490_TXLK03));
   if (asT490->GetAt(T490_TXLK04).GetAt(1) > ' ')
      sprintf(pTaxBase->Assmnt_No, "%s%s%s%s", asT490->GetAt(T490_TXLK01), asT490->GetAt(T490_TXLK02), asT490->GetAt(T490_TXLK03), asT490->GetAt(T490_TXLK04));

   // TaxYear
   strTmp = asT490->GetAt(T490_TXCUYR);
   sprintf(pTaxBase->TaxYear, "20%.2d", atol(asT490->GetAt(T490_TXCUYR)));

   // TRA
   sprintf(pTaxBase->TRA, "05%s", asT490->GetAt(T490_TXACCR));

   // BillNum
   sprintf(pTaxBase->BillNum, "%d", atol(asT490->GetAt(T490_TXBILL)));

   // Tax amt
   dTax1 = atof(asT490->GetAt(T490_TX1DUE));  
   dPaid1 = atof(asT490->GetAt(T490_TX1PMA));
   dPen1 = atof(asT490->GetAt(T490_TX1PNL));
   dTax2 = atof(asT490->GetAt(T490_TX2DUE));  
   dPaid2 = atof(asT490->GetAt(T490_TX2PMA));
   dPen2 = atof(asT490->GetAt(T490_TX2PNL));
   dTotalDue = 0;
   if (dTax1 > 0.0)
   {
      dTotalDue = dTax1+dPen1;
      pTaxBase->Inst1Status[0] = TAX_BSTAT_UNPAID;
      sprintf(pTaxBase->TaxAmt1, "%.2f", dTax1);
      sprintf(pTaxBase->DueDate1, "20%.2d%.2d%.2d", atol(asT490->GetAt(T490_TX1DLY)), atol(asT490->GetAt(T490_TX1DLM)), atol(asT490->GetAt(T490_TX1DLD)));
   } else if (dPaid1 > 0)
   {
      // When tax bill is paid, tax due will be 0.
      dTax1 = dPaid1;
      pTaxBase->Inst1Status[0] = TAX_BSTAT_PAID;
      sprintf(pTaxBase->TaxAmt1, "%.2f", dTax1);
   }

   if (dTax2 > 0.0)
   {
      dTotalDue += dTax2+dPen2;
      pTaxBase->Inst2Status[0] = TAX_BSTAT_UNPAID;
      sprintf(pTaxBase->TaxAmt2, "%.2f", dTax2);
      sprintf(pTaxBase->DueDate2, "20%.2d%.2d%.2d", atol(asT490->GetAt(T490_TX2DLY)), atol(asT490->GetAt(T490_TX2DLM)), atol(asT490->GetAt(T490_TX2DLD)));
   } else if (dPaid2 > 0)
   {
      // When tax bill is paid, tax due will be 0.
      dTax2 = dPaid2;
      pTaxBase->Inst2Status[0] = TAX_BSTAT_PAID;
      sprintf(pTaxBase->TaxAmt2, "%.2f", dTax2);
   }

   // Total Due 
   if (dTotalDue > 0.0)
      sprintf(pTaxBase->TotalDue, "%.2f", dTotalDue);

   // Total tax
   dTotalTax = dTax1+dTax2;
   if (dTotalTax > 0.0)
      sprintf(pTaxBase->TotalTaxAmt, "%.2f", dTotalTax);

   // Paid amt
   if (dPaid1 > 0.0)
   {
      pTaxBase->Inst1Status[0] = TAX_BSTAT_PAID;
      sprintf(pTaxBase->PaidAmt1, "%.2f", dPaid1);
      strTmp = asT490->GetAt(T490_TX1PMD);
      iTmp = atol(asT490->GetAt(T490_TX1PMD));
      if (iTmp > 0)
         sprintf(pTaxBase->PaidDate1, "20%.2d%.2d%.2d", atol(asT490->GetAt(T490_TX1PMY)), atol(asT490->GetAt(T490_TX1PMM)), iTmp);
   }
   if (dPaid2 > 0.0)
   {
      pTaxBase->Inst2Status[0] = TAX_BSTAT_PAID;
      sprintf(pTaxBase->PaidAmt2, "%.2f", dPaid2);
      iTmp = atol(asT490->GetAt(T490_TX2PMD));
      if (iTmp > 0)
         sprintf(pTaxBase->PaidDate2, "20%.2d%.2d%.2d", atol(asT490->GetAt(T490_TX2PMY)), atol(asT490->GetAt(T490_TX2PMM)), iTmp);
   }

   // Penalty
   if (dPen1 > 0.0)
      sprintf(pTaxBase->PenAmt1, "%.2f", dPen1);

   if (dPen2 > 0.0)
      sprintf(pTaxBase->PenAmt2, "%.2f", dPen2);

   // Due Amt
   if (!dTotalDue)
   {
      pTaxBase->Inst1Status[0] = TAX_BSTAT_PAID;
      pTaxBase->Inst2Status[0] = TAX_BSTAT_PAID;
   }

   return 0;
}

int Mpa_NewTaxBase(char *pOutbuf)
{
   double   dTax1, dTax2, dTotalDue;
   int      iTmp;
   CString  strTmp;
   TAXBASE  *pTaxBase = (TAXBASE *)pOutbuf;
   
   memset(pOutbuf, 0, sizeof(TAXBASE));
   sprintf(pTaxBase->Apn, "%s%s%s", apLocalTokens[T490_TXLK01], apLocalTokens[T490_TXLK02], apLocalTokens[T490_TXLK03], apLocalTokens[T490_TXLK04]);

   // TaxYear
   strTmp = apLocalTokens[T490_TXCUYR];
   sprintf(pTaxBase->TaxYear, "20%.2d", atol(apLocalTokens[T490_TXCUYR]));

   // TRA
   sprintf(pTaxBase->TRA, "05%s", apLocalTokens[T490_TXACCR]);

   // BillNum
   sprintf(pTaxBase->BillNum, "%d", atol(apLocalTokens[T490_TXBILL]));

   // Tax amt
   dTax1 = atof(apLocalTokens[T490_TX1DUE]);  
   if (dTax1 > 0.0)
   {
      sprintf(pTaxBase->TaxAmt1, "%.2f", dTax1);
      sprintf(pTaxBase->DueDate1, "20%.2d%.2d%.2d", atol(apLocalTokens[T490_TX1DLY]), atol(apLocalTokens[T490_TX1DLM]), atol(apLocalTokens[T490_TX1DLD]));
   }

   dTax2 = atof(apLocalTokens[T490_TX2DUE]);  
   if (dTax2 > 0.0)
   {
      sprintf(pTaxBase->TaxAmt2, "%.2f", dTax2);
      sprintf(pTaxBase->DueDate2, "20%.2d%.2d%.2d", atol(apLocalTokens[T490_TX2DLY]), atol(apLocalTokens[T490_TX2DLM]), atol(apLocalTokens[T490_TX2DLD]));
   }

   // Total Due Amt
   dTotalDue = dTax1+dTax2;
   if (dTotalDue > 0.0)
   {
      sprintf(pTaxBase->TotalTaxAmt, "%.2f", dTotalDue);
      sprintf(pTaxBase->TotalDue, "%.2f", dTotalDue);
   }

   // Paid amt
   dTax1 = atof(apLocalTokens[T490_TX1PMA]);
   if (dTax1 > 0.0)
   {
      pTaxBase->Inst1Status[0] = TAX_BSTAT_PAID;
      sprintf(pTaxBase->PaidAmt1, "%.2f", dTax1);
      iTmp = atol(apLocalTokens[T490_TX1PMD]);
      if (iTmp > 0)
         sprintf(pTaxBase->PaidDate1, "20%.2d%.2d%.2d", atol(apLocalTokens[T490_TX1PMY]), atol(apLocalTokens[T490_TX1PMM]), iTmp);
   }
   dTax2 = atof(apLocalTokens[T490_TX2PMA]);
   if (dTax2 > 0.0)
   {
      pTaxBase->Inst2Status[0] = TAX_BSTAT_PAID;
      sprintf(pTaxBase->PaidAmt2, "%.2f", dTax2);
      iTmp = atol(apLocalTokens[T490_TX2PMD]);
      if (iTmp > 0)
         sprintf(pTaxBase->PaidDate2, "20%.2d%.2d%.2d", atol(apLocalTokens[T490_TX2PMY]), atol(apLocalTokens[T490_TX2PMM]), iTmp);
   }

   // Penalty
   dTax1 = atof(apLocalTokens[T490_TX1PNL]);
   if (dTax1 > 0.0)
      sprintf(pTaxBase->PenAmt1, "%.2f", dTax1);

   dTax2 = atof(apLocalTokens[T490_TX2PNL]);
   if (dTax2 > 0.0)
      sprintf(pTaxBase->PenAmt2, "%.2f", dTax2);

   // Due Amt
   if (!dTotalDue)
   {
      pTaxBase->Inst1Status[0] = TAX_BSTAT_PAID;
      pTaxBase->Inst2Status[0] = TAX_BSTAT_PAID;
   }
   return 0;
}

/***************************** Mpa_ParseTaxBase ******************************
 *
 *
 *****************************************************************************/

int Mpa_ParseTaxBase(char *pOutbuf, char *pInbuf)
{
   char     acTmp[256], *pTmp;
   int      iTmp;
   double   dTmp, dTotalAmt, dPaidAmt;
   TAXBASE  *pTaxBase = (TAXBASE *)pOutbuf;

   // Parse input tax data
   iTmp = ParseStringIQ(pInbuf, '|', MAX_FLD_TOKEN, apTokens);
   if (iTmp < T405_OWN2ND)
   {
      LogMsg("***** Error: bad Tax record for APN=%.*s (#tokens=%d)", iApnLen, pInbuf, iTmp);
      return -1;
   }

   // Clear output buffer
   memset(pOutbuf, 0, sizeof(TAXBASE));

   // APN
   iTmp = remChar(apTokens[T405_APN], '-');
   if (iTmp < iApnLen)
      return -1;
   strcpy(pTaxBase->Apn, apTokens[T405_APN]);

#ifdef _DEBUG
   //if (!memcmp(pTaxBase->Apn, "021130008", 9) || !memcmp(pTaxBase->Apn, "021130020", 9))
   //   iTmp = 0;
#endif

   // BillNum
   iTmp = atol(apTokens[T405_TXLCID]);
   sprintf(pTaxBase->BillNum, "%d", iTmp);
   strcpy(pTaxBase->OwnerInfo.Apn, apTokens[T405_APN]);
   strcpy(pTaxBase->OwnerInfo.BillNum, pTaxBase->BillNum);

   // TRA
   remChar(apTokens[T405_TRA], '-');
   iTmp = atol(apTokens[T405_TRA]);
   sprintf(pTaxBase->TRA, "%.6d", iTmp);

   // Tax Year
   iTmp = atol(apTokens[T405_FRRY4]);
   sprintf(pTaxBase->TaxYear, "%d", iTmp);
   strcpy(pTaxBase->OwnerInfo.TaxYear, pTaxBase->TaxYear);

   // Default date 
   pTaxBase->isDelq[0] = '0';
   if (*apTokens[T405_DELFLG] == 'Y' )
   {
      pTaxBase->isDelq[0] = '1';
      pTmp = dateConversion(apTokens[T405_DFTDTE], pTaxBase->Def_Date, MMDDYY2);
      if (pTmp)
         memcpy(pTaxBase->DelqYear, pTaxBase->Def_Date, 4);
   }

   // Tax rate
   dTmp = atof(apTokens[T405_TXRATE]);
   sprintf(pTaxBase->TotalRate, "%.1f", dTmp);

   // Total Tax amt
   dTotalAmt = atof(apTokens[T405_TOTDUE]);
   if (dTotalAmt > 0.0)
   {
      sprintf(pTaxBase->TotalTaxAmt, "%.2f", dTotalAmt);
      sprintf(pTaxBase->TaxAmt1, "%.2f", dTotalAmt/2);
      strcpy(pTaxBase->TaxAmt2, pTaxBase->TaxAmt1);

      // Due Amt
      dTmp = atof(apTokens[T405_BALDUE]);
      if (dTmp > 0.0)
         sprintf(pTaxBase->TotalDue, "%.2f", dTmp);

      // Due date1
      iTmp = atol(apTokens[T405_DEDTX1]);
      if (iTmp > 0)
      {
         sprintf(acTmp, "%0.6d", iTmp);
         sprintf(pTaxBase->DueDate1, "20%.2s%.4s", &acTmp[4], acTmp);
      }
      // Due date2
      iTmp = atol(apTokens[T405_DEDTX2]);
      if (iTmp > 0)
      {
         sprintf(acTmp, "%0.6d", iTmp);
         sprintf(pTaxBase->DueDate2, "20%.2s%.4s", &acTmp[4], acTmp);
      }

   }

   // Total paid amt
   dPaidAmt = atof(apTokens[T405_PAYMNT]);
   if (dPaidAmt > 0.0)
   {
      if (dPaidAmt < dTotalAmt)
         sprintf(pTaxBase->PaidAmt1, "%.2f", dPaidAmt);
      else
      {
         dTmp = atof(apTokens[T405_DUEAM2]);
         if (dTmp > 0.0)
            sprintf(pTaxBase->PaidAmt1, "%.2f", dPaidAmt);
         else
         {
            sprintf(pTaxBase->PaidAmt1, "%.2f", dPaidAmt/2);
            strcpy(pTaxBase->PaidAmt2, pTaxBase->PaidAmt1);
         }
      }
   }

   // Owner Info
   myTrim(apTokens[T405_ASMOWN]);
   strcpy(pTaxBase->OwnerInfo.Name1, apTokens[T405_ASMOWN]);

   // CareOf
   if (*apTokens[T405_LMOWA2] > ' ')
   {
      if (!memcmp(apTokens[T405_LMOWA2], "C/O", 3))
         strcpy(pTaxBase->OwnerInfo.CareOf, apTokens[T405_LMOWA2]);
      else if (!memcmp(apTokens[T405_LMOWA2], "DBA", 3))
         strcpy(pTaxBase->OwnerInfo.Dba, apTokens[T405_LMOWA2]);
   }

   // Mailing address
   if (*apTokens[T405_LMOWA3] > ' ')
   {
      strcpy(pTaxBase->OwnerInfo.MailAdr[0], apTokens[T405_LMOWA3]);
      strcpy(pTaxBase->OwnerInfo.MailAdr[1], apTokens[T405_XXCSZ]);
   }

   pTaxBase->BillType[0] = BILLTYPE_SECURED;
   pTaxBase->isSecd[0] = '1';
   pTaxBase->isSupp[0] = '0';

   return 0;
}

/*********************************** Mpa_Load_TaxBase *****************************
 *
 * Load Secured Tax file "TX405 Secured.csv" and update file "tx490 excel.csv".  
 * tx490 contains APN that has due tax bill only.  If an APN that is not in this file, 
 * assume it has been paid.
 *
 * Import Tax_Base, Tax_Items, & Tax_Agency
 *
 **********************************************************************************/

int Mpa_Load_TaxBaseCsv(char *pTx405File, char *pTx490File, bool bImport)
{
   char     *pTmp, acRec[MAX_RECSIZE], acTmpFile[_MAX_PATH], acUpdApn[32], acTx490Rec[MAX_RECSIZE], 
            acBaseRec[2000], acDetailRec[2000], acAgencyRec[2000], acOutbuf[2000],
            acItemsFile[_MAX_PATH], acBaseFile[_MAX_PATH], acAgencyFile[_MAX_PATH];
   int      iRecCnt, iRet, iIdx, lCnt, iTmp, iUpdCnt;
   double   dTmp, dPaidAmt1, dPaidAmt2, dPenAmt1, dPenAmt2, dDueAmt;

   FILE        *fdTx405, *fdTx490, *fdBase, *fdItems, *fdAgency;
   TAXBASE     *pTaxBase = (TAXBASE *)&acBaseRec[0];
   TAXDETAIL   *pDetail  = (TAXDETAIL *)&acDetailRec[0];
   TAXAGENCY   *pAgency  = (TAXAGENCY *)&acAgencyRec[0];

   LogMsg0("\nLoading Tax Base");

   LogMsg("Open Secured tax roll file %s", pTx405File);
   if (!(fdTx405 = fopen(pTx405File, "r")))
   {
      LogMsg("***** Error opening Secured tax roll file: %s\n", pTx405File);
      return -4;
   }

   LogMsg("Open current tax bill file %s", pTx490File);
   if (!(fdTx490 = fopen(pTx490File, "r")))
   {
      LogMsg("***** Error opening current tax bill file: %s\n", pTx490File);
      return -4;
   }

   // Prepare output file
   sprintf(acItemsFile, sTaxOutTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "Items");
   sprintf(acAgencyFile, sTaxOutTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "Agency");
   sprintf(acBaseFile, sTaxOutTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "Base");
   pTmp = strrchr(acBaseFile, '\\');
   *pTmp = 0;
   if (_access(acBaseFile, 0))
      _mkdir(acBaseFile);
   *pTmp = '\\';

   // Open base file
   LogMsg("Open Base file %s", acBaseFile);
   if (!(fdBase = fopen(acBaseFile, "w")))
   {
      LogMsg("***** Error creating base file: %s\n", acBaseFile);
      return -4;
   }

   // Open detail file
   LogMsg("Open Items file %s", acItemsFile);
   if (!(fdItems = fopen(acItemsFile, "w")))
   {
      LogMsg("***** Error creating detail file: %s\n", acItemsFile);
      return -4;
   }

   // Open Agency file
   sprintf(acTmpFile, "%s\\%s\\%s_Agency.tmp", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   LogMsg("Open Agency file %s", acTmpFile);
   if (!(fdAgency = fopen(acTmpFile, "w")))
   {
      LogMsg("***** Error creating Agency file: %s\n", acTmpFile);
      return -4;
   }

   // Initialize variables
   lCnt = 0;
   iUnMatch490=iUpdCnt=iRecCnt=0;
   acUpdApn[0] = 0;

   // Process loop
   while (!feof(fdTx405))
   {
      pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdTx405);
      if (!pTmp)
         break;

      // Parse tax roll rec
      iRet = Mpa_ParseTaxBase(acBaseRec, acRec);
      if (!iRet)
      {
#ifdef _DEBUG
         //if (!memcmp(pTaxBase->Apn, "021130008", 9))
         //   iRet = 0;
#endif
         while ((iTmp = memcmp(acUpdApn, pTaxBase->Apn, 10)) < 0)
         {
            if (acUpdApn[0] > ' ' && acUpdApn[0] <= '9')
            {
               LogMsg("+++ New tax bill: APN=%s, BillNum=%s", acUpdApn, apLocalTokens[T490_TXBILL]);
               // This is supplemental bill.  It will be added when loading supplemental file
               //char  acNewBaseRec[2000];
               //iTmp = Mpa_NewTaxBase(acNewBaseRec);
               //if (!iTmp)
               //{
               //   // Create new TaxBase record
               //   Tax_CreateTaxBaseCsv(acRec, (TAXBASE *)&acNewBaseRec);
               //   fputs(acRec, fdBase);
               //   iRecCnt++;
               //}
            }

            // Read next record
            if (!fgets(acTx490Rec, MAX_RECSIZE, fdTx490))
               break;
            iTokens = ParseStringNQ(acTx490Rec, '|', T490_COLS, apLocalTokens);
            sprintf(acUpdApn, "%.3d%.3d%.4d%.2d", atol(apLocalTokens[T490_TXLK01]), atol(apLocalTokens[T490_TXLK02]), 
               atol(apLocalTokens[T490_TXLK03]), atol(apLocalTokens[T490_TXLK04]));
         }
            
         // APN matched, update current tax base record
         if (!(iTmp = memcmp(acUpdApn, pTaxBase->Apn, iApnLen)))
         {
            // At least one bill has not been paid
            dPaidAmt1 = atof(apLocalTokens[T490_TX1PMA]);
            dPenAmt1 = atof(apLocalTokens[T490_TX1PNL]);
            if (dPaidAmt1 > 0.0)
            {
               pTaxBase->Inst1Status[0] = TAX_BSTAT_PAID;
               sprintf(pTaxBase->PaidAmt1, "%.2f", dPaidAmt1);
               iTmp = atol(apLocalTokens[T490_TX1PMD]);
               if (iTmp > 0)
                  sprintf(pTaxBase->PaidDate1, "20%.2d%.2d%.2d", atol(apLocalTokens[T490_TX1PMY]), atol(apLocalTokens[T490_TX1PMM]), iTmp);
            } else
            {
               if (dPenAmt1 > 0)
               {
                  dTmp = atof(apLocalTokens[T490_TX1DUE]);
                  sprintf(pTaxBase->TaxAmt1, "%.2f", dPenAmt1+dTmp);
                  pTaxBase->Inst1Status[0] = TAX_BSTAT_PASTDUE;
               } else
                  pTaxBase->Inst1Status[0] = TAX_BSTAT_UNPAID;
            }

            dPaidAmt2 = atof(apLocalTokens[T490_TX2PMA]);
            dPenAmt2 = atof(apLocalTokens[T490_TX2PNL]);
            if (dPaidAmt2 > 0.0)
            {
               pTaxBase->Inst2Status[0] = TAX_BSTAT_PAID;
               sprintf(pTaxBase->PaidAmt2, "%.2f", dPaidAmt2);
               iTmp = atol(apLocalTokens[T490_TX2PMD]);
               if (iTmp > 0)
                  sprintf(pTaxBase->PaidDate2, "20%.2d%.2d%.2d", atol(apLocalTokens[T490_TX2PMY]), atol(apLocalTokens[T490_TX2PMM]), iTmp);
            } else
            {
               if (dPenAmt2 > 0)
               {
                  dTmp = atof(apLocalTokens[T490_TX2DUE]);
                  sprintf(pTaxBase->TaxAmt2, "%.2f", dPenAmt2+dTmp);
                  pTaxBase->Inst2Status[0] = TAX_BSTAT_PASTDUE;
               } else
                  pTaxBase->Inst2Status[0] = TAX_BSTAT_UNPAID;
            }

            // Penalty
            if (dPenAmt1 > 0.0)
               sprintf(pTaxBase->PenAmt1, "%.2f", dPenAmt1);

            if (dPenAmt2 > 0.0)
               sprintf(pTaxBase->PenAmt2, "%.2f", dPenAmt2);

            // Due Amt
            dDueAmt = atof(apLocalTokens[T490_TX1DUE]);
            dDueAmt += atof(apLocalTokens[T490_TX2DUE]);
            if (dDueAmt > 0.0)
               sprintf(pTaxBase->TotalDue, "%.2f", dDueAmt+dPenAmt1+dPenAmt2);
            else
            {
               pTaxBase->Inst1Status[0] = TAX_BSTAT_PAID;
               pTaxBase->Inst2Status[0] = TAX_BSTAT_PAID;
               if (dPaidAmt2 == 0.0)
                  strcpy(pTaxBase->PaidAmt2, pTaxBase->PaidAmt1);
            }

            // Last update date
            sprintf(pTaxBase->Upd_Date, "%d", lUpdTaxFileDate);
            iUpdCnt++;
            // Update record has been used, reset to get another one
            acUpdApn[0] = 0;     
         } else
         {
            if (bDebug)
               LogMsg("*** No update: %s (assume paid)", pTaxBase->Apn);

            // Record not found, this must have been paid
            if (pTaxBase->PaidAmt1[0] < '1')
               strcpy(pTaxBase->PaidAmt1, pTaxBase->TaxAmt1);
            if (pTaxBase->PaidAmt2[0] < '1')
               strcpy(pTaxBase->PaidAmt2, pTaxBase->TaxAmt2);
            pTaxBase->Inst2Status[0] = TAX_BSTAT_PAID;
            pTaxBase->Inst1Status[0] = TAX_BSTAT_PAID;

            // Last update date
            sprintf(pTaxBase->Upd_Date, "%d", lLastTaxFileDate);
            iUnMatch490++;
         }


         // Create TaxBase record
         Tax_CreateTaxBaseCsv(acOutbuf, (TAXBASE *)&acBaseRec);
         fputs(acOutbuf, fdBase);
         iRecCnt++;

         // Parse tax detail/agency
         memset(acDetailRec, 0, sizeof(TAXDETAIL));
         memset(acAgencyRec, 0, sizeof(TAXAGENCY));

         // APN
         strcpy(pDetail->Apn, pTaxBase->Apn);

         // BillNum
         strcpy(pDetail->BillNum, pTaxBase->BillNum);

         // Tax Year
         strcpy(pDetail->TaxYear, pTaxBase->TaxYear);

         iIdx = T405_ENTCD1;
         do
         {
            // Tax code
            if (*apTokens[iIdx] > ' ')
            {
               // Tax Code
               strcpy(pDetail->TaxCode,  apTokens[iIdx]);
               strcpy(pAgency->Code, apTokens[iIdx]);

               // Tax Desc
               strcpy(pDetail->TaxDesc,  apTokens[iIdx+1]);
               strcpy(pAgency->Agency, pDetail->TaxDesc);


               // Translate "MCUSD 2016 SERIES A BOND" to "MCUSD 2016 SERIES A BOND 209-966-7606"
               //if (!strcmp(pAgency->Agency, "MCUSD 2016 SERIES A BOND"))
               //   strcpy(pAgency->Agency, "MCUSD 2016 SERIES A BOND 209-966-7606");

               // tcflg
               pAgency->TC_Flag[0] = '0';
               pDetail->TC_Flag[0] = '0';

               // Tax Amount
               dTmp = atof(apTokens[iIdx+2]);
               sprintf(pDetail->TaxAmt, "%.2f", dTmp);

               // Create TaxBase record
               Tax_CreateDetailCsv(acOutbuf, pDetail);
               fputs(acOutbuf, fdItems);

               // Create Agency record
               Tax_CreateAgencyCsv(acOutbuf, pAgency);
               fputs(acOutbuf, fdAgency);
            } else if (iIdx > T405_ENTCD3)
               break;

            // Advance to next entry
            iIdx += 3;
         } while (iIdx < T405_TAXA17);
      }

      if (!(++lCnt % 1000))
         printf("\r%d", lCnt);
   }

   if (fdTx405)
      fclose(fdTx405);
   if (fdTx490)
      fclose(fdTx490);
   if (fdBase)
      fclose(fdBase);
   if (fdItems)
      fclose(fdItems);
   if (fdAgency)
      fclose(fdAgency);

   LogMsgD("\nTotal processed records : %u", lCnt);
   LogMsgD("               fully paid : %u", iUnMatch490);
   LogMsgD("           output records : %u\n", iRecCnt);

   // Dedup Agency file before import
   LogMsg("Dedup Agency file %s to %s", acTmpFile, acAgencyFile);
   iRet = sortFile(acTmpFile, acAgencyFile, "S(#1,C,A) DUPOUT F(TXT)");

   // Import into SQL
   if (bImport)
   {
      iRet = doTaxImport(myCounty.acCntyCode, TAX_BASE);
      if (!iRet)
      {
         iRet = doTaxImport(myCounty.acCntyCode, TAX_DETAIL);
         iRet = doTaxImport(myCounty.acCntyCode, TAX_AGENCY);
      }
   } else
      iRet = 0;

   return iRet;
}

/*********************************** Mpa_Load_TaxBase *****************************
 *
 * Load Secured Tax file "2016 TX405 Secured.xlsx"
 *    and update using "tx490 excel format 2016.xls"
 *
 * Import Tax_Base, Tax_Items, & Tax_Agency
 *
 **********************************************************************************/

int Mpa_Load_TaxBaseXls(bool bImport)
{
   char     *pTmp, acRec[MAX_RECSIZE], acTmp[256], acUpdBill[20];
   char     acBaseXlsFile[_MAX_PATH], acBaseShtName[64], acUpdXlsFile[_MAX_PATH], acUpdShtName[64], acTmpFile[_MAX_PATH];
   char     acItemsFile[_MAX_PATH], acBaseFile[_MAX_PATH], acAgencyFile[_MAX_PATH], acBaseRec[1024], acNewBaseRec[1024];
   int      iRows, iCols, iRowCnt, iSkipRows, iRecCnt, iRet, iTmp, iIdx, lCnt;
   int      iUpdIdx, iUpdRows, iUpdCols, iUpdCnt;
   double   dTmp, dTotalAmt, dPaidAmt, dDueAmt, dPenAmt;

   CString      strTmp, strApn;
   CStringArray asT405, asT490;
   FILE        *fdBase, *fdItems, *fdAgency;
   TAXBASE     *pTaxBase = (TAXBASE *)&acBaseRec[0];
   TAXDETAIL   sItemsRec;
   TAXAGENCY   sAgencyRec;

   LogMsg("Loading Tax Base");

   // Prepare output file
   sprintf(acItemsFile, sTaxOutTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "Items");
   sprintf(acAgencyFile, sTaxOutTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "Agency");
   sprintf(acBaseFile, sTaxOutTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "Base");
   pTmp = strrchr(acBaseFile, '\\');
   *pTmp = 0;
   if (_access(acBaseFile, 0))
      _mkdir(acBaseFile);
   *pTmp = '\\';

   // Open base file
   LogMsg("Open Base file %s", acBaseFile);
   fdBase = fopen(acBaseFile, "w");
   if (fdBase == NULL)
   {
      LogMsg("***** Error creating base file: %s\n", acBaseFile);
      return -4;
   }

   // Open detail file
   LogMsg("Open Items file %s", acItemsFile);
   fdItems = fopen(acItemsFile, "w");
   if (fdItems == NULL)
   {
      LogMsg("***** Error creating detail file: %s\n", acItemsFile);
      return -4;
   }

   // Open Agency file
   sprintf(acTmpFile, "%s\\%s\\%s_Agency.tmp", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   LogMsg("Open Agency file %s", acTmpFile);
   fdAgency = fopen(acTmpFile, "w");
   if (fdAgency == NULL)
   {
      LogMsg("***** Error creating Agency file: %s\n", acTmpFile);
      return -4;
   }

   // Get Secured tax file name
   GetIniString(myCounty.acCntyCode, "BaseTax", "", acBaseXlsFile, _MAX_PATH, acIniFile);
   GetIniString(myCounty.acCntyCode, "BaseSheet", "", acBaseShtName, _MAX_PATH, acIniFile);
   lLastTaxFileDate = getFileDate(acBaseXlsFile);
   if (strstr(acBaseXlsFile, ".xlsx"))
      strcpy(acTmp, "xlsx");
   else
      strcpy(acTmp, ".xls");

   // Open input file - read only, no backup, no need to close the file
   LogMsg("Open Secured tax file %s", acBaseXlsFile);
   CSpreadSheet sprT405(acBaseXlsFile, acBaseShtName, false, acTmp);
   iRows = sprT405.GetTotalRows();
   iCols = sprT405.GetTotalColumns();
   if (iCols < T405_OWN2ND)
   {
      LogMsg("*** Bad input file: %s.", acBaseXlsFile);
      return 0;
   }

   // Get update tax file name
   GetIniString(myCounty.acCntyCode, "CurrTax", "", acUpdXlsFile, _MAX_PATH, acIniFile);
   GetIniString(myCounty.acCntyCode, "CurrSheet", "", acUpdShtName, _MAX_PATH, acIniFile);
   lUpdTaxFileDate = getFileDate(acUpdXlsFile);
   if (strstr(acUpdXlsFile, ".xlsx"))
      strcpy(acTmp, "xlsx");
   else
      strcpy(acTmp, ".xls");

   LogMsg("Open Current tax file %s", acUpdXlsFile);
   CSpreadSheet sprT490(acUpdXlsFile, acUpdShtName, false, acTmp);
   iUpdRows = sprT490.GetTotalRows();
   iUpdCols = sprT490.GetTotalColumns();
   if (iUpdCols < T490_TXSCLC)
   {
      LogMsg("*** Bad input file: %s.", acUpdXlsFile);
      return 0;
   }
   sprT490.GetFieldNames(asT490);
   iUpdIdx = 2;
   iUpdCnt = 0;
   sprT490.ReadRow(asT490, iUpdIdx);
   sprintf(acUpdBill, "%d", atol(asT490.GetAt(T490_TXBILL)));

   // Initialize variables
   lCnt = 0;
   iSkipRows=iRecCnt=0;
   sprT405.GetFieldNames(asT405);

   for (iRowCnt = 2; iRowCnt <= iRows; iRowCnt++)
   {
      memset(&acBaseRec, 0, sizeof(TAXBASE));
      sprT405.ReadRow(asT405, iRowCnt);
   
      if (asT405.GetSize() < T405_OWN2ND)
      {
         iSkipRows++;
         LogMsg("Skip row# %d", iRowCnt);
      } else
      {
         strApn = asT405.GetAt(T405_APN);  
         if (!strApn.IsEmpty())
         {
            strApn.Remove('-');
            strcpy(pTaxBase->Apn, strApn);

            // TaxYear
            memcpy(pTaxBase->TaxYear, asT405.GetAt(T405_FRRY4), 4);

            // TRA
            strTmp = asT405.GetAt(T405_TRA);  
            if (!strTmp.IsEmpty())
            {
               strTmp.Remove('-');
               iTmp = atol(strTmp);
               sprintf(pTaxBase->TRA, "%.6d", iTmp);
            }

            // Default?
            pTaxBase->isDelq[0] = '0';
            if (asT405.GetAt(T405_DELFLG) == "Y")
            {
               pTaxBase->isDelq[0] = '1';
               strTmp = asT405.GetAt(T405_DFTDTE);  
               if (dateConversion(strTmp.GetBuffer(), acTmp, MMDDYY2))
               {
                  strcpy(pTaxBase->Def_Date, acTmp);
                  memcpy(pTaxBase->DelqYear, acTmp, 4);
               }
            }

            // BillNum
            dTmp = atof(asT405.GetAt(T405_TXLCID));
            if (dTmp > 0)
               sprintf(pTaxBase->BillNum, "%d", (long)dTmp);

            // Tax rate
            strcpy(pTaxBase->TotalRate, asT405.GetAt(T405_TXRATE));

            // Total Tax amt
            strTmp = asT405.GetAt(T405_TOTDUE);  
            dTotalAmt = atof(strTmp);
            if (dTotalAmt > 0.0)
            {
               sprintf(pTaxBase->TotalTaxAmt, "%.2f", dTotalAmt);
               sprintf(pTaxBase->TaxAmt1, "%.2f", dTotalAmt/2);
               strcpy(pTaxBase->TaxAmt2, pTaxBase->TaxAmt1);

               // Due Amt
               dTmp = atof(asT405.GetAt(T405_BALDUE));
               if (dTmp > 0.0)
                  sprintf(pTaxBase->TotalDue, "%.2f", dTmp);

               // Due date1
               iTmp = atol(asT405.GetAt(T405_DEDTX1));
               if (iTmp > 0)
               {
                  sprintf(acTmp, "%0.6d", iTmp);
                  sprintf(pTaxBase->DueDate1, "20%.2s%.4s", &acTmp[4], acTmp);
               }
               // Due date2
               iTmp = atol(asT405.GetAt(T405_DEDTX2));
               if (iTmp > 0)
               {
                  sprintf(acTmp, "%0.6d", iTmp);
                  sprintf(pTaxBase->DueDate2, "20%.2s%.4s", &acTmp[4], acTmp);
               }
            }

            // Total paid amt
            strTmp = asT405.GetAt(T405_PAYMNT);  
            dPaidAmt = atof(strTmp);
            if (dPaidAmt > 0.0)
            {
               if (dPaidAmt < dTotalAmt)
               {
                  sprintf(pTaxBase->PaidAmt1, "%.2f", dPaidAmt);
                  pTaxBase->PaidStatus[0] = '1';
               } else
               {
                  dTmp = atof(asT405.GetAt(T405_DUEAM2));
                  if (dTmp > 0.0)
                     sprintf(pTaxBase->PaidAmt1, "%.2f", dPaidAmt);
                  else
                  {
                     sprintf(pTaxBase->PaidAmt1, "%.2f", dPaidAmt/2);
                     strcpy(pTaxBase->PaidAmt2, pTaxBase->PaidAmt1);
                     pTaxBase->PaidStatus[0] = 'B';
                  }
               }
            }

            pTaxBase->isSecd[0] = '1';
            pTaxBase->isSupp[0] = '0';

            // Update tax bill
            while ((iTmp = strcmp(acUpdBill, pTaxBase->BillNum)) < 0)
            {
               if (acUpdBill[0] > ' ')
               {
                  LogMsg("+++ Add new bill: BILLNUM=%s", acUpdBill);
                  iTmp = Mpa_NewTaxBase(&asT490, acNewBaseRec);
                  if (!iTmp)
                  {
                     // Create new TaxBase record
                     Tax_CreateTaxBaseCsv(acRec, (TAXBASE *)&acNewBaseRec);
                     fputs(acRec, fdBase);
                     iRecCnt++;
                  }
               }

               // Read next record
               iUpdIdx++;
               if (iUpdIdx > iUpdRows)
                  break;
               sprT490.ReadRow(asT490, iUpdIdx);
               sprintf(acUpdBill, "%d", atol(asT490.GetAt(T490_TXBILL)));
            }

            if (!(iTmp = strcmp(acUpdBill, pTaxBase->BillNum)))
            {
               // At least one bill has not been paid
               strTmp = asT490.GetAt(T490_TX1PMA);  
               dPaidAmt = atof(asT490.GetAt(T490_TX1PMA));
               if (dPaidAmt > 0.0)
               {
                  sprintf(pTaxBase->PaidAmt1, "%.2f", dPaidAmt);
                  strTmp = asT490.GetAt(T490_TX1PMD);
                  iTmp = atol(asT490.GetAt(T490_TX1PMD));
                  if (iTmp > 0)
                  {
                     sprintf(pTaxBase->PaidDate1, "20%.2d%.2d%.2d", atol(asT490.GetAt(T490_TX1PMY)), atol(asT490.GetAt(T490_TX1PMM)), iTmp);
                     pTaxBase->PaidStatus[0] = '1';
                  }
               }
               dPaidAmt = atof(asT490.GetAt(T490_TX2PMA));
               if (dPaidAmt > 0.0)
               {
                  sprintf(pTaxBase->PaidAmt2, "%.2f", dPaidAmt);
                  iTmp = atol(asT490.GetAt(T490_TX2PMD));
                  if (iTmp > 0)
                     sprintf(pTaxBase->PaidDate2, "20%.2d%.2d%.2d", atol(asT490.GetAt(T490_TX2PMY)), atol(asT490.GetAt(T490_TX2PMM)), iTmp);
               }

               // Penalty
               dPenAmt = atof(asT490.GetAt(T490_TX1PNL));
               if (dPenAmt > 0.0)
                  sprintf(pTaxBase->PenAmt1, "%.2f", dPenAmt);

               dPenAmt = atof(asT490.GetAt(T490_TX2PNL));
               if (dPenAmt > 0.0)
                  sprintf(pTaxBase->PenAmt2, "%.2f", dPenAmt);

               // Due Amt
               dDueAmt = atof(asT490.GetAt(T490_TX1DUE));
               dDueAmt += atof(asT490.GetAt(T490_TX2DUE));
               if (dDueAmt > 0.0)
                  sprintf(pTaxBase->TotalDue, "%.2f", dDueAmt);
               else
               {
                  pTaxBase->PaidStatus[0] = 'B';
                  if (pTaxBase->PaidAmt2[0] < '1')
                     strcpy(pTaxBase->PaidAmt2, pTaxBase->PaidAmt1);
               }

               // Last update date
               sprintf(pTaxBase->Upd_Date, "%d", lUpdTaxFileDate);
               iUpdCnt++;
               // Update record has been used, reset to get another one
               acUpdBill[0] = 0;     
            } else
            {
               // Record not found, this must have been paid
               if (pTaxBase->PaidAmt1[0] < '1')
                  strcpy(pTaxBase->PaidAmt1, pTaxBase->TaxAmt1);
               if (pTaxBase->PaidAmt2[0] < '1')
                  strcpy(pTaxBase->PaidAmt2, pTaxBase->TaxAmt2);
               pTaxBase->PaidStatus[0] = 'B';

               // Last update date
               sprintf(pTaxBase->Upd_Date, "%d", lUpdTaxFileDate);
            }

            // Create TaxBase record
            Tax_CreateTaxBaseCsv(acRec, (TAXBASE *)&acBaseRec);
            fputs(acRec, fdBase);
            iRecCnt++;

            // Parse tax detail/agency
            memset(&sItemsRec, 0, sizeof(TAXDETAIL));
            memset(&sAgencyRec, 0, sizeof(TAXAGENCY));

            // APN
            strcpy(sItemsRec.Apn, pTaxBase->Apn);

            // BillNum
            strcpy(sItemsRec.BillNum, pTaxBase->BillNum);

            // Tax Year
            strcpy(sItemsRec.TaxYear, pTaxBase->TaxYear);

#ifdef _DEBUG
            //if (!memcmp(pTaxBase->Apn, "0020300050", 10))
            //   iTmp = 0;
#endif
            iIdx = T405_ENTCD1;
            do
            {
               // Tax code
               strTmp = asT405.GetAt(iIdx);  
               if (strTmp > "0")
               {
                  // Tax Code
                  strcpy(sItemsRec.TaxCode,  strTmp);
                  strcpy(sAgencyRec.Code, strTmp);

                  // Tax Desc
                  strTmp = asT405.GetAt(iIdx+1);  
                  strcpy(sItemsRec.TaxDesc,  strTmp);
                  strcpy(sAgencyRec.Agency, strTmp);

                  // Tax Amount
                  dTmp = atof(asT405.GetAt(iIdx+2));
                  sprintf(sItemsRec.TaxAmt, "%.2f", dTmp);

                  // Create TaxBase record
                  Tax_CreateDetailCsv(acRec, &sItemsRec);
                  fputs(acRec, fdItems);

                  // Create Agency record
                  Tax_CreateAgencyCsv(acRec, &sAgencyRec);
                  fputs(acRec, fdAgency);
               } else if (iIdx > T405_ENTCD3)
                  break;

               // Advance to next entry
               iIdx += 3;
            } while (iIdx < T405_TAXA17);
         }
      }

      if (!(++lCnt % 1000))
         printf("\r%d", lCnt);
   }

   if (fdBase)
      fclose(fdBase);
   if (fdItems)
      fclose(fdItems);
   if (fdAgency)
      fclose(fdAgency);

   LogMsgD("\nTotal records processed : %u", lCnt);
   LogMsgD("                skipped : %u", iSkipRows);
   LogMsgD("                 output : %u\n", iRecCnt);
   LogMsgD("                updated : %u\n", iUpdCnt);

   // Import into SQL
   if (bImport)
   {
      iRet = doTaxImport(myCounty.acCntyCode, TAX_BASE);
      if (!iRet)
      {
         iRet = doTaxImport(myCounty.acCntyCode, TAX_DETAIL);

         // Dedup Agency file before import
         LogMsg("Dedup Agency file %s to %s", acTmpFile, acAgencyFile);
         iRet = sortFile(acTmpFile, acAgencyFile, "S(#1,C,A) DUPOUT F(TXT)");
         if (iRet > 0)
            iRet = doTaxImport(myCounty.acCntyCode, TAX_AGENCY);
      }
   } else
      iRet = 0;

   return iRet;
}

/*********************************** Mpa_Load_TaxBase *****************************
 *
 * Load Secured Tax file "2016 TX405 Secured.xlsx"
 *    and update using TX490 & TX491
 *
 * Import Tax_Base, Tax_Items, Tax_Agency, & Tax_Delq
 *
 **********************************************************************************/

int Mpa_Load_TaxBaseXls(char *pCurrXlsFile, char *pCurrTaxFile, bool bImport)
{
   char     *pTmp, acRec[MAX_RECSIZE], acT490[MAX_RECSIZE], acTmp[256], acUpdApn[20];
   char     acBaseXlsFile[_MAX_PATH], acBaseShtName[64], acTmpFile[_MAX_PATH];
   char     acItemsFile[_MAX_PATH], acBaseFile[_MAX_PATH], acAgencyFile[_MAX_PATH], acBaseRec[1024], acNewBaseRec[1024];
   int      iRows, iCols, iRowCnt, iSkipRows, iRecCnt, iRet, iTmp, iIdx, lCnt, iUpdCnt;
   double   dTmp, dTotalAmt, dPaidAmt, dDueAmt, dPenAmt1, dPenAmt2;

   CString      strTmp, strApn;
   CStringArray asT405;
   FILE        *fdBase, *fdItems, *fdAgency, *fdCurr;
   TAXBASE     *pTaxBase = (TAXBASE *)&acBaseRec[0];
   TAXDETAIL   sItemsRec;
   TAXAGENCY   sAgencyRec;

   LogMsg("Loading Tax Base");

   // Get Secured tax file name
   GetIniString(myCounty.acCntyCode, "BaseTax", "", acBaseXlsFile, _MAX_PATH, acIniFile);
   lLastTaxFileDate = getFileDate(acBaseXlsFile);
   lUpdTaxFileDate = getFileDate(pCurrXlsFile);
   if (lLastTaxFileDate < lUpdTaxFileDate)
      lLastTaxFileDate = lUpdTaxFileDate;

   // Only process if new tax file
   iRet = isNewTaxFile(NULL, myCounty.acCntyCode);
   if (iRet <= 0)
   {
      lLastTaxFileDate = 0;
      return iRet;
   }

   // Prepare output file
   sprintf(acItemsFile, sTaxOutTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "Items");
   sprintf(acAgencyFile, sTaxOutTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "Agency");
   sprintf(acBaseFile, sTaxOutTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "Base");
   pTmp = strrchr(acBaseFile, '\\');
   *pTmp = 0;
   if (_access(acBaseFile, 0))
      _mkdir(acBaseFile);
   *pTmp = '\\';

   // Open base file
   LogMsg("Open Base file %s", acBaseFile);
   fdBase = fopen(acBaseFile, "w");
   if (fdBase == NULL)
   {
      LogMsg("***** Error creating base file: %s\n", acBaseFile);
      return -4;
   }

   // Open detail file
   LogMsg("Open Items file %s", acItemsFile);
   fdItems = fopen(acItemsFile, "w");
   if (fdItems == NULL)
   {
      LogMsg("***** Error creating detail file: %s\n", acItemsFile);
      return -4;
   }

   // Open Agency file
   sprintf(acTmpFile, "%s\\%s\\%s_Agency.tmp", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   LogMsg("Open Agency file %s", acTmpFile);
   fdAgency = fopen(acTmpFile, "w");
   if (fdAgency == NULL)
   {
      LogMsg("***** Error creating Agency file: %s\n", acTmpFile);
      return -4;
   }

   // Copy extension
   if (strstr(acBaseXlsFile, ".xlsx"))
      strcpy(acTmp, "xlsx");
   else
      strcpy(acTmp, ".xls");

   // Open input file - read only, no backup, no need to close the file
   LogMsg("Open Secured tax file %s", acBaseXlsFile);
   GetIniString(myCounty.acCntyCode, "BaseSheet", "", acBaseShtName, _MAX_PATH, acIniFile);
   CSpreadSheet sprT405(acBaseXlsFile, acBaseShtName, false, acTmp);
   iRows = sprT405.GetTotalRows();
   iCols = sprT405.GetTotalColumns();
   if (iCols < T405_OWN2ND)
   {
      LogMsg("*** Bad input file: %s.", acBaseXlsFile);
      return 0;
   }

   // Open current tax file 
   LogMsg("Open current tax bill file: %s", pCurrTaxFile);
   if (!(fdCurr = fopen(pCurrTaxFile, "r")))
   {
      LogMsg("***** Error opening current tax bill file: %s\n", pCurrTaxFile);
      return -4;
   }

   // Get first record
   fgets(acT490, MAX_RECSIZE, fdCurr);
   iTokens = ParseStringNQ(acT490, '|', T490_COLS, apTokens);
   sprintf(acUpdApn, "%s%s%s", apTokens[T490_TXLK01], apTokens[T490_TXLK02], apTokens[T490_TXLK03]);
   iUpdCnt = 0;

   // Initialize variables
   lCnt = 0;
   iSkipRows=iRecCnt=0;
   sprT405.GetFieldNames(asT405);

   for (iRowCnt = 2; iRowCnt <= iRows; iRowCnt++)
   {
      memset(&acBaseRec, 0, sizeof(TAXBASE));
      sprT405.ReadRow(asT405, iRowCnt);
   
      if (asT405.GetSize() < T405_OWN2ND)
      {
         iSkipRows++;
         LogMsg("Skip row# %d", iRowCnt);
      } else
      {
         strApn = asT405.GetAt(T405_APN);  
         if (!strApn.IsEmpty())
         {
            strApn.Remove('-');
            strcpy(pTaxBase->Apn, strApn);

            // TaxYear
            memcpy(pTaxBase->TaxYear, asT405.GetAt(T405_FRRY4), 4);

            // TRA
            strTmp = asT405.GetAt(T405_TRA);  
            if (!strTmp.IsEmpty())
            {
               strTmp.Remove('-');
               iTmp = atol(strTmp);
               sprintf(pTaxBase->TRA, "%.6d", iTmp);
            }

            // Default?
            pTaxBase->isDelq[0] = '0';
            if (asT405.GetAt(T405_DELFLG) == "Y")
            {
               pTaxBase->isDelq[0] = '1';
               strTmp = asT405.GetAt(T405_DFTDTE);  
               if (dateConversion(strTmp.GetBuffer(), acTmp, MMDDYY2))
               {
                  strcpy(pTaxBase->Def_Date, acTmp);
                  memcpy(pTaxBase->DelqYear, acTmp, 4);
               }
            }

            // BillNum
            dTmp = atof(asT405.GetAt(T405_TXLCID));
            if (dTmp > 0)
               sprintf(pTaxBase->BillNum, "%d", (long)dTmp);

            // Tax rate
            strcpy(pTaxBase->TotalRate, asT405.GetAt(T405_TXRATE));

            // Total Tax amt
            strTmp = asT405.GetAt(T405_TOTDUE);  
            dTotalAmt = atof(strTmp);
            if (dTotalAmt > 0.0)
            {
               sprintf(pTaxBase->TotalTaxAmt, "%.2f", dTotalAmt);
               sprintf(pTaxBase->TaxAmt1, "%.2f", dTotalAmt/2);
               strcpy(pTaxBase->TaxAmt2, pTaxBase->TaxAmt1);
               pTaxBase->Inst1Status[0] = TAX_BSTAT_UNPAID;
               pTaxBase->Inst2Status[0] = TAX_BSTAT_UNPAID;

               // Due Amt
               dTmp = atof(asT405.GetAt(T405_BALDUE));
               if (dTmp > 0.0)
                  sprintf(pTaxBase->TotalDue, "%.2f", dTmp);

               // Due date1
               iTmp = atol(asT405.GetAt(T405_DEDTX1));
               if (iTmp > 0)
               {
                  sprintf(acTmp, "%0.6d", iTmp);
                  sprintf(pTaxBase->DueDate1, "20%.2s%.4s", &acTmp[4], acTmp);
               }
               // Due date2
               iTmp = atol(asT405.GetAt(T405_DEDTX2));
               if (iTmp > 0)
               {
                  sprintf(acTmp, "%0.6d", iTmp);
                  sprintf(pTaxBase->DueDate2, "20%.2s%.4s", &acTmp[4], acTmp);
               }
            }

            // Total paid amt
            strTmp = asT405.GetAt(T405_PAYMNT);  
            dPaidAmt = atof(strTmp);
            if (dPaidAmt > 0.0)
            {
               if (dPaidAmt < dTotalAmt)
               {
                  sprintf(pTaxBase->PaidAmt1, "%.2f", dPaidAmt);
                  pTaxBase->Inst1Status[0] = TAX_BSTAT_PAID;
               } else
               {
                  dTmp = atof(asT405.GetAt(T405_DUEAM2));
                  if (dTmp > 0.0)
                  {
                     sprintf(pTaxBase->PaidAmt1, "%.2f", dPaidAmt);
                     pTaxBase->Inst1Status[0] = TAX_BSTAT_PAID;
                  } else
                  {
                     sprintf(pTaxBase->PaidAmt1, "%.2f", dPaidAmt/2);
                     strcpy(pTaxBase->PaidAmt2, pTaxBase->PaidAmt1);
                     pTaxBase->Inst2Status[0] = TAX_BSTAT_PAID;
                     pTaxBase->Inst1Status[0] = TAX_BSTAT_PAID;
                  }
               }
            }

            pTaxBase->isSecd[0] = '1';
            pTaxBase->BillType[0] = BILLTYPE_SECURED;
            pTaxBase->isSupp[0] = '0';

#ifdef _DEBUG
            //if (!memcmp(pTaxBase->Apn, "0011100130", 10))
            //   iTmp = 0;
#endif

            // Update tax bill
            while ((iTmp = memcmp(acUpdApn, pTaxBase->Apn, 10)) < 0)
            {
               if (acUpdApn[0] > ' ')
               {
                  LogMsg("+++ Add new bill: APN=%s, BillNum=%s", acUpdApn, apTokens[T490_TXBILL]);
                  iTmp = Mpa_NewTaxBase(acNewBaseRec);
                  if (!iTmp)
                  {
                     // Create new TaxBase record
                     Tax_CreateTaxBaseCsv(acRec, (TAXBASE *)&acNewBaseRec);
                     fputs(acRec, fdBase);
                     iRecCnt++;
                  }
               }

               // Read next record
               if (!fgets(acT490, MAX_RECSIZE, fdCurr))
                  break;
               iTokens = ParseStringNQ(acT490, '|', T490_COLS, apTokens);
               sprintf(acUpdApn, "%s%s%s", apTokens[T490_TXLK01], apTokens[T490_TXLK02], apTokens[T490_TXLK03]);
            }
            
            if (!(iTmp = memcmp(acUpdApn, pTaxBase->Apn, iApnLen)))
            {
               // At least one bill has not been paid
               dPaidAmt = atof(apTokens[T490_TX1PMA]);
               if (dPaidAmt > 0.0)
               {
                  pTaxBase->Inst1Status[0] = TAX_BSTAT_PAID;
                  sprintf(pTaxBase->PaidAmt1, "%.2f", dPaidAmt);
                  iTmp = atol(apTokens[T490_TX1PMD]);
                  if (iTmp > 0)
                     sprintf(pTaxBase->PaidDate1, "20%.2d%.2d%.2d", atol(apTokens[T490_TX1PMY]), atol(apTokens[T490_TX1PMM]), iTmp);
               }
               dPaidAmt = atof(apTokens[T490_TX2PMA]);
               if (dPaidAmt > 0.0)
               {
                  pTaxBase->Inst2Status[0] = TAX_BSTAT_PAID;
                  sprintf(pTaxBase->PaidAmt2, "%.2f", dPaidAmt);
                  iTmp = atol(apTokens[T490_TX2PMD]);
                  if (iTmp > 0)
                     sprintf(pTaxBase->PaidDate2, "20%.2d%.2d%.2d", atol(apTokens[T490_TX2PMY]), atol(apTokens[T490_TX2PMM]), iTmp);
               }

               // Penalty
               dPenAmt1 = atof(apTokens[T490_TX1PNL]);
               if (dPenAmt1 > 0.0)
                  sprintf(pTaxBase->PenAmt1, "%.2f", dPenAmt1);

               dPenAmt2 = atof(apTokens[T490_TX2PNL]);
               if (dPenAmt2 > 0.0)
                  sprintf(pTaxBase->PenAmt2, "%.2f", dPenAmt2);

               // Due Amt
               dDueAmt = atof(apTokens[T490_TX1DUE]);
               dDueAmt += atof(apTokens[T490_TX2DUE]);
               if (dDueAmt > 0.0)
                  sprintf(pTaxBase->TotalDue, "%.2f", dDueAmt+dPenAmt1+dPenAmt2);
               else
               {
                  pTaxBase->Inst1Status[0] = TAX_BSTAT_PAID;
                  pTaxBase->Inst2Status[0] = TAX_BSTAT_PAID;
                  if (pTaxBase->PaidAmt2[0] < '1')
                     strcpy(pTaxBase->PaidAmt2, pTaxBase->PaidAmt1);
               }

               // Last update date
               sprintf(pTaxBase->Upd_Date, "%d", lUpdTaxFileDate);
               iUpdCnt++;
               // Update record has been used, reset to get another one
               acUpdApn[0] = 0;     
            } else
            {
               // Record not found, this must have been paid
               if (pTaxBase->PaidAmt1[0] < '1')
                  strcpy(pTaxBase->PaidAmt1, pTaxBase->TaxAmt1);
               if (pTaxBase->PaidAmt2[0] < '1')
                  strcpy(pTaxBase->PaidAmt2, pTaxBase->TaxAmt2);
               pTaxBase->Inst2Status[0] = TAX_BSTAT_PAID;
               pTaxBase->Inst1Status[0] = TAX_BSTAT_PAID;

               // Last update date
               sprintf(pTaxBase->Upd_Date, "%d", lUpdTaxFileDate);
            }

            // Create TaxBase record
            Tax_CreateTaxBaseCsv(acRec, (TAXBASE *)&acBaseRec);
            fputs(acRec, fdBase);
            iRecCnt++;

            // Parse tax detail/agency
            memset(&sItemsRec, 0, sizeof(TAXDETAIL));
            memset(&sAgencyRec, 0, sizeof(TAXAGENCY));

            // APN
            strcpy(sItemsRec.Apn, pTaxBase->Apn);

            // BillNum
            strcpy(sItemsRec.BillNum, pTaxBase->BillNum);

            // Tax Year
            strcpy(sItemsRec.TaxYear, pTaxBase->TaxYear);

#ifdef _DEBUG
            //if (!memcmp(pTaxBase->Apn, "0011400320", 10))
            //   iTmp = 0;
#endif
            iIdx = T405_ENTCD1;
            do
            {
               // Tax code
               strTmp = asT405.GetAt(iIdx);  
               if (strTmp > "0")
               {
                  // Tax Code
                  strcpy(sItemsRec.TaxCode,  strTmp);
                  strcpy(sAgencyRec.Code, strTmp);

                  // Tax Desc
                  strTmp = asT405.GetAt(iIdx+1);  
                  strcpy(sItemsRec.TaxDesc,  strTmp);
                  strcpy(sAgencyRec.Agency, strTmp);

                  // Tax Amount
                  dTmp = atof(asT405.GetAt(iIdx+2));
                  sprintf(sItemsRec.TaxAmt, "%.2f", dTmp);

                  // Create TaxBase record
                  Tax_CreateDetailCsv(acRec, &sItemsRec);
                  fputs(acRec, fdItems);

                  // Create Agency record
                  Tax_CreateAgencyCsv(acRec, &sAgencyRec);
                  fputs(acRec, fdAgency);
               } else if (iIdx > T405_ENTCD3)
                  break;

               // Advance to next entry
               iIdx += 3;
            } while (iIdx < T405_TAXA17);
         }
      }

      if (!(++lCnt % 1000))
         printf("\r%d", lCnt);
   }

   if (fdCurr)
      fclose(fdCurr);
   if (fdBase)
      fclose(fdBase);
   if (fdItems)
      fclose(fdItems);
   if (fdAgency)
      fclose(fdAgency);

   LogMsgD("\nTotal records processed : %u", lCnt);
   LogMsgD("                skipped : %u", iSkipRows);
   LogMsgD("                 output : %u\n", iRecCnt);
   LogMsgD("                updated : %u\n", iUpdCnt);

   // Dedup Agency file before import
   LogMsg("Dedup Agency file %s to %s", acTmpFile, acAgencyFile);
   iRet = sortFile(acTmpFile, acAgencyFile, "S(#1,C,A) DUPOUT F(TXT)");

   // Import into SQL
   if (bImport)
   {
      iRet = doTaxImport(myCounty.acCntyCode, TAX_BASE);
      if (!iRet)
      {
         iRet = doTaxImport(myCounty.acCntyCode, TAX_DETAIL);
         if (!iRet)
            iRet = doTaxImport(myCounty.acCntyCode, TAX_AGENCY);
      }
   } else
      iRet = 0;

   return iRet;
}

/*************************** Mpa_Load_TaxDelqXls ***************************
 *
 *
 ***************************************************************************/

int Mpa_Load_TaxDelqXls(bool bImport)
{
   char     acBuf[MAX_RECSIZE], acRec[MAX_RECSIZE], acTmp[256];
   char     acOutFile[_MAX_PATH], acXlsFile[_MAX_PATH], acShtName[64];
   int      iRows, iCols, iRowCnt, iSkipRows, iRecCnt, iRet, iTmp;
   double   dTmp;

   CString      strTmp, strApn;
   CStringArray asRows;

   long     lOut=0, lCnt=0;
   FILE     *fdOut;
   TAXDELQ  *pOutRec = (TAXDELQ *)acBuf;

   LogMsg("Loading delinquent tax file");

   // Open Output file
   sprintf(acOutFile, sTaxOutTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "Delq");
   LogMsg("Open output file %s", acOutFile);
   fdOut = fopen(acOutFile, "w");
   if (fdOut == NULL)
   {
      LogMsg("***** Error creating output file: %s\n", acOutFile);
      return -4;
   }

   // Get XLS file name
   GetIniString(myCounty.acCntyCode, "DelqTax", "", acXlsFile, _MAX_PATH, acIniFile);
   GetIniString(myCounty.acCntyCode, "DelqSheet", "", acShtName, _MAX_PATH, acIniFile);
   lLastTaxFileDate = getFileDate(acXlsFile);

   if (strstr(acXlsFile, ".xlsx"))
      strcpy(acTmp, "xlsx");
   else
      strcpy(acTmp, ".xls");

   // Open input file - read only, no backup, no need to close the file
   LogMsg("Open Delq tax file %s", acXlsFile);
   CSpreadSheet sprSht(acXlsFile, acShtName, false, acTmp);

   iRows = sprSht.GetTotalRows();
   iCols = sprSht.GetTotalColumns();
   if (iCols < T491_TXBOND)
   {
      LogMsg("*** Bad input file: %s.", acXlsFile);
      return 0;
   }

   // Initialize variables
   lCnt = 0;
   iSkipRows=iRecCnt=0;
   sprSht.GetFieldNames(asRows);

   for (iRowCnt = 2; iRowCnt <= iRows; iRowCnt++)
   {
      memset(acBuf, 0, sizeof(TAXDELQ));
      sprSht.ReadRow(asRows, iRowCnt);
   
      if (asRows.GetSize() < T491_TXBOND)
      {
         iSkipRows++;
         LogMsg("Skip row# %d", iRowCnt);
      } else
      {
         // APN
         sprintf(pOutRec->Apn, "%s%s%s", asRows.GetAt(T491_TXLK01), asRows.GetAt(T491_TXLK02), asRows.GetAt(T491_TXLK03));

#ifdef _DEBUG
         //if (!memcmp(pOutRec->Apn, "0011600050", 10))
         //   iTmp = 0;
#endif
         // Updated date
         sprintf(pOutRec->Upd_Date, "%d", lLastTaxFileDate);

         // Tax BillNum
         iTmp = atol(asRows.GetAt(T491_TXBILL));
         sprintf(pOutRec->BillNum, "%d", iTmp);

         // Tax Year
         sprintf(pOutRec->TaxYear, "20%.2d", atol(asRows.GetAt(T491_TXEFYR)));

         // Year Default 
         iTmp = atol(asRows.GetAt(T491_TXYRDF));
         sprintf(pOutRec->Def_Date, "20%.2d", iTmp);
         pOutRec->isDelq[0] = '1';

         // Tax amt
         dTmp = atof(asRows.GetAt(T491_TXBSAM));
         sprintf(pOutRec->Tax_Amt, "%.2f", dTmp);

         // Default amt
         dTmp = atof(asRows.GetAt(T491_TXDLIN));
         sprintf(pOutRec->Def_Amt, "%.2f", dTmp);

         // Pen
         dTmp = atof(asRows.GetAt(T491_TXPNLT));
         sprintf(pOutRec->Pen_Amt, "%.2f", dTmp);

         // Fee
         dTmp =  atof(asRows.GetAt(T491_TXCOST));         // Delq Cost
         dTmp += atof(asRows.GetAt(T491_TXREDE));         // Redemption fee
         dTmp += atof(asRows.GetAt(T491_TXMIFE));         // Misc fee
         sprintf(pOutRec->Fee_Amt, "%.2f", dTmp);

         // Redemption date
         strTmp = asRows.GetAt(T491_TXREMM);
         if (strTmp > "00")
         {
            sprintf(pOutRec->Red_Date, "20%.2d%.2d%.2d", atol(asRows.GetAt(T491_TXREYY)), atol(asRows.GetAt(T491_TXREMM)), atol(asRows.GetAt(T491_TXREDD)));
            // Redemption amt
            dTmp = atof(asRows.GetAt(T491_TXREAM));
            sprintf(pOutRec->Red_Amt, "%.2f", dTmp);
            pOutRec->isDelq[0] = '0';
         }

         // Create delimited record
         Tax_CreateDelqCsv(acRec, (TAXDELQ *)&acBuf[0]);

         // Output record			
         fputs(acRec, fdOut);
         lOut++;
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdOut)
      fclose(fdOut);

   LogMsg("Total records processed:          %u", lCnt);
   LogMsg("Total delinquent records output:  %u", lOut);

   printf("\nTotal output records: %u", lCnt);

   // Import into SQL
   if (bImport && lOut > 0)
   {
      iRet = doTaxImport(myCounty.acCntyCode, TAX_DELINQUENT);
   } else
      iRet = 0;

   return iRet;
}
int Mpa_Load_TaxDelqCsv(char *pDelqFile, bool bImport)
{
   char     acOutFile[_MAX_PATH], acRec[MAX_RECSIZE], acT491Rec[2048], acDelqRec[2048], *pRec;
   TAXDELQ *pDelqRec = (TAXDELQ *)acDelqRec;

   int      iRecCnt, iRet;
   long     lOut=0, lCnt=0;

   LogMsg0("\nLoading delinquent tax file");

   // Open Output file
   sprintf(acOutFile, sTaxOutTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "Delq");
   LogMsg("Open output file %s", acOutFile);
   fdDelq = fopen(acOutFile, "w");
   if (fdDelq == NULL)
   {
      LogMsg("***** Error creating output file: %s\n", acOutFile);
      return -4;
   }

   // Open T491 tax file 
   LogMsg("Open delinquent tax bill: %s", pDelqFile);
   if (!(fdT491 = fopen(pDelqFile, "r")))
   {
      LogMsg("***** Error opening delinquent tax bill file: %s\n", pDelqFile);
      return -4;
   }

   // Initialize variables
   lCnt = 0;
   iRecCnt=0;

   while (!feof(fdT491))
   {
      // Get next record
      pRec = fgets(acT491Rec, 2048, fdT491);
      if (!pRec || *pRec > '9')
         break;      // EOF

      iRet = Mpa_ParseTaxDelq(acDelqRec, acT491Rec);
      if (!iRet)
      {
         Tax_CreateDelqCsv(acRec, (TAXDELQ *)&acDelqRec[0]);
         fputs(acRec, fdDelq);
         iRecCnt++;
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   } 

   // Close files
   if (fdT491)
      fclose(fdT491);
   if (fdDelq)
      fclose(fdDelq);

   LogMsg("Total records processed:          %u", lCnt);
   LogMsg("Total delinquent records output:  %u", iRecCnt);

   printf("\nTotal output records: %u", lCnt);

   // Import into SQL
   if (bImport && iRecCnt > 0)
   {
      iRet = doTaxImport(myCounty.acCntyCode, TAX_DELINQUENT);
   } else
      iRet = 0;

   return iRet;
}

/************************************ ConvXls2Csv ******************************
 *
 * Convert Excel file to CSV then sorted if SortCmd is supplied
 * 
 *
 *******************************************************************************/

int ConvXls2Csv(char *pCsvFile, char *pXlsFile, char *pShtName, char *pSortCmd, int iMinCols)
{
   char     *pTmp, acRec[MAX_RECSIZE], acTmp[256], acTmpFile[_MAX_PATH];
   int      iRows, iCols, iRowCnt, iSkipRows, iRecCnt, iRet, iTmp, iIdx, lCnt;

   CString      strTmp, strApn;
   CStringArray asRows;
   FILE        *fdCsv;

   LogMsg("Converting %s to %s", pXlsFile, pCsvFile);

   // Open Agency file
   LogMsg("Open Agency file %s", pCsvFile);
   fdCsv = fopen(pCsvFile, "w");
   if (fdCsv == NULL)
   {
      LogMsg("***** Error creating Agency file: %s\n", pCsvFile);
      return -4;
   }

   // Check file ex
   lLastTaxFileDate = getFileDate(pXlsFile);
   if (strstr(pXlsFile, ".xlsx"))
      strcpy(acTmp, "xlsx");
   else
      strcpy(acTmp, ".xls");

   LogMsg("Open Excel file %s", pXlsFile);
   CSpreadSheet sprSht(pXlsFile, pShtName, false, acTmp);
   iRows = sprSht.GetTotalRows();
   iCols = sprSht.GetTotalColumns();
   if (iCols < iMinCols)
   {
      LogMsg("*** Bad input file: %s.", pXlsFile);
      return 0;
   }
   sprSht.GetFieldNames(asRows);

   // Initialize variables
   lCnt = 0;
   iSkipRows=iRecCnt=0;

   for (iRowCnt = 1; iRowCnt <= iRows; iRowCnt++)
   {
      acRec[0] = 0;
      sprSht.ReadRow(asRows, iRowCnt);
   
      if (asRows.GetSize() < iMinCols)
      {
         iSkipRows++;
         LogMsg("Skip row# %d", iRowCnt);
      } else
      {
         for (iIdx = 0; iIdx < iCols; iIdx++)
         {
            if (iIdx > 0)
               strcat(acRec, "|");
            strTmp = asRows.GetAt(iIdx);  
            strcat(acRec, asRows.GetAt(iIdx));  
         }

         strcat(acRec, "\n");
         iRecCnt++;
         fputs(acRec, fdCsv);
      }

      if (!(++lCnt % 1000))
         printf("\r%d", lCnt);
   }

   if (fdCsv)
      fclose(fdCsv);

   LogMsgD("\nTotal records processed : %u", lCnt);
   LogMsgD("                skipped : %u", iSkipRows);
   LogMsgD("                 output : %u\n", iRecCnt);

   // Import into SQL
   if (pSortCmd)
   {
      strcpy(acTmpFile, pCsvFile);
      pTmp = strrchr(acTmpFile, '.');
      if (pTmp) *pTmp = 0;
      strcat(acTmpFile, ".srt");
      iRet = sortFile(pCsvFile, acTmpFile, pSortCmd, &iTmp);
      if (iRet > 0)
      {
         DeleteFile(pCsvFile);
         iRet = rename(acTmpFile, pCsvFile);
      } else
         iRet = iTmp;
   } else
      iRet = 0;

   return iRet;
}

/******************************* Mpa_Load_TaxSuppXls ******************************
 *
 * Loading supplemental data
 * Return number of records created
 *
 **********************************************************************************/

int Mpa_Load_TaxSuppXls(char *pTaxDir, int iFileNum)
{
   char     acRec[MAX_RECSIZE], acTmp[256], acXlsFile[_MAX_PATH], acShtName[64], sExt[16];
   char     acBaseFile[_MAX_PATH], acBaseRec[1024];
   int      iRows, iCols, iRowCnt, iSkipRows, iRecCnt, iTmp, lCnt;

   CStringArray asSupp;
   FILE        *fdBase;
   TAXBASE     *pTaxBase = (TAXBASE *)&acBaseRec[0];

   LogMsg("Loading Tax Supplemental");
   // Get Supplemental tax file name
   if (iFileNum > 0)
      sprintf(acTmp, "SuppFile%d", iFileNum);
   else
      strcpy(acTmp, "SuppFile");
   iTmp = GetIniString(myCounty.acCntyCode, acTmp, "", acXlsFile, _MAX_PATH, acIniFile);

   // If input file not defined, ignore it
   if (!iTmp)
      return 0;

   // If input file not avail, give it a warning
   if (_access(acXlsFile, 0))
   {
      LogMsgD("\n*** Missing input file: %s\n", acXlsFile);
      return 0;
   }

   // Prepare output file
   sprintf(acBaseFile, sTaxOutTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "Base");

   // Open base file
   LogMsg("Open Base file %s", acBaseFile);
   fdBase = fopen(acBaseFile, "a+");
   if (fdBase == NULL)
   {
      LogMsg("***** Error opening base file: %s\n", acBaseFile);
      return -1;
   }

   // Get sheet name
   GetIniString(myCounty.acCntyCode, "SuppSheet", "", acShtName, _MAX_PATH, acIniFile);
   if (strstr(acXlsFile, ".xlsx"))
      strcpy(sExt, "xlsx");
   else
      strcpy(sExt, ".xls");

   // Open input file - read only, no backup, no need to close the file
   LogMsg("Open Secured tax file %s", acXlsFile);
   CSpreadSheet sprSupp(acXlsFile, acShtName, false, sExt);
   iRows = sprSupp.GetTotalRows();
   iCols = sprSupp.GetTotalColumns();
   if (iCols < T490_COLS)
   {
      LogMsg("*** Bad input file: %s.", acXlsFile);
      return -2;
   }

   // Initialize variables
   lCnt = 0;
   iSkipRows=iRecCnt=0;
   sprSupp.GetFieldNames(asSupp);

   for (iRowCnt = 2; iRowCnt <= iRows; iRowCnt++)
   {
      memset(&acBaseRec, 0, sizeof(TAXBASE));
      sprSupp.ReadRow(asSupp, iRowCnt);
   
      // Check number of cols
      if (asSupp.GetSize() < T490_COLS)
      {
         iSkipRows++;
         LogMsg("Skip row# %d", iRowCnt);
      } else
      {
         iTmp = Mpa_NewTaxBase(&asSupp, acBaseRec);
         if (!iTmp)
         {
            pTaxBase->isSecd[0] = '0';
            pTaxBase->isSupp[0] = '1';
            pTaxBase->BillType[0] = BILLTYPE_SECURED_SUPPL;

            // Create new TaxBase record
            Tax_CreateTaxBaseCsv(acRec, (TAXBASE *)&acBaseRec);
            fputs(acRec, fdBase);
            iRecCnt++;
         }
      }

      if (!(++lCnt % 1000))
         printf("\r%d", lCnt);
   }

   if (fdBase)
      fclose(fdBase);

   LogMsgD("\nTotal records processed : %u", lCnt);
   LogMsgD("                skipped : %u", iSkipRows);
   LogMsgD("                 output : %u\n", iRecCnt);

   return iRecCnt;
}

/******************************* Mpa_Load_TaxSuppCsv ******************************
 *
 * Loading supplemental data from CSV file
 * Return number of records created
 *
 **********************************************************************************/

int Mpa_ParseTaxSupp(char *pOutbuf, char *pInbuf)
{
   double   dTax1, dTax2, dPaid1, dPaid2, dPen1, dPen2, dTotalDue, dTotalTax;
   int      iTmp;
   TAXBASE  *pTaxBase = (TAXBASE *)pOutbuf;
   
   memset(pOutbuf, 0, sizeof(TAXBASE));
   // Parse input tax data
   iTmp = ParseStringIQ(pInbuf, '|', MAX_FLD_TOKEN, apTokens);
   if (iTmp < TSUP_COLS)
   {
      LogMsg("***** Error: Bad Supplemental Tax record for APN=%s (#tokens=%d)", pInbuf, iTmp);
      return -1;
   }

   // Clear output buffer
   memset(pOutbuf, 0, sizeof(TAXBASE));

   // APN
   sprintf(pTaxBase->Apn, "%s%s%s", apTokens[TSUP_TXLK01], apTokens[TSUP_TXLK02], apTokens[TSUP_TXLK03]);
   sprintf(pTaxBase->Assmnt_No, "%s%s%s%s", apTokens[TSUP_TXLK01], apTokens[TSUP_TXLK02], apTokens[TSUP_TXLK03], apTokens[TSUP_TXLK04]);

#ifdef _DEBUG
   //if (!memcmp(pTaxBase->Apn, "0011300250", 10) || !memcmp(pTaxBase->Apn, "0011400270", 10))
   //   iTmp = 0;
#endif

   // BillNum
   sprintf(pTaxBase->BillNum, "%d", atol(apTokens[TSUP_TXBILL]));

   // TRA
   sprintf(pTaxBase->TRA, "05%s", apTokens[TSUP_TXACCR]);

   // Tax Year
   iTmp = atol(apTokens[TSUP_TXCUYR]);
   sprintf(pTaxBase->TaxYear, "20%d", iTmp);

   // Bill type
   pTaxBase->BillType[0] = BILLTYPE_SECURED_SUPPL;
   pTaxBase->isSecd[0] = '0';
   pTaxBase->isSupp[0] = '1';

   // Tax amt
   dTax1 = atof(apTokens[TSUP_TX1DUE]);  
   dPaid1 = atof(apTokens[TSUP_TX1PMA]);
   dPen1 = atof(apTokens[TSUP_TX1PNL]);
   dTax1 += dPen1;
   dTax2 = atof(apTokens[TSUP_TX2DUE]);  
   dPaid2 = atof(apTokens[TSUP_TX2PMA]);
   dPen2 = atof(apTokens[TSUP_TX2PNL]);
   dTax2 += dPen2;
   dTotalDue = 0;
   if (dTax1 > 0.0)
   {
      dTotalDue = dTax1;
      dTax1 += dPaid1;              // dTax1 is current inst1 due, portion of tax1 may have been paid
      if (dPen1 > 0.0)
         pTaxBase->Inst1Status[0] = TAX_BSTAT_PASTDUE;
      else
         pTaxBase->Inst1Status[0] = TAX_BSTAT_UNPAID;

      sprintf(pTaxBase->TaxAmt1, "%.2f", dTax1);
      sprintf(pTaxBase->DueDate1, "20%.2d%.2d%.2d", atol(apTokens[TSUP_TX1DLY]), atol(apTokens[TSUP_TX1DLM]), atol(apTokens[TSUP_TX1DLD]));
   } else if (dPaid1 > 0)
   {
      // When tax bill is paid, tax due will be 0.
      dTax1 = dPaid1;
      pTaxBase->Inst1Status[0] = TAX_BSTAT_PAID;
      sprintf(pTaxBase->TaxAmt1, "%.2f", dTax1);
   }

   if (dTax2 > 0.0)
   {
      dTotalDue += dTax2;
      dTax2 += dPaid2;              // dTax2 is current inst2 due, portion of tax2 may have been paid
      if (dPen2 > 0.0)
         pTaxBase->Inst2Status[0] = TAX_BSTAT_PASTDUE;
      else
         pTaxBase->Inst2Status[0] = TAX_BSTAT_UNPAID;

      sprintf(pTaxBase->TaxAmt2, "%.2f", dTax2);
      sprintf(pTaxBase->DueDate2, "20%.2d%.2d%.2d", atol(apTokens[TSUP_TX2DLY]), atol(apTokens[TSUP_TX2DLM]), atol(apTokens[TSUP_TX2DLD]));
   } else if (dPaid2 > 0)
   {
      // When tax bill is paid, tax due will be 0.
      dTax2 = dPaid2;
      pTaxBase->Inst2Status[0] = TAX_BSTAT_PAID;
      sprintf(pTaxBase->TaxAmt2, "%.2f", dTax2);
   }

   // Total Due 
   if (dTotalDue > 0.0)
      sprintf(pTaxBase->TotalDue, "%.2f", dTotalDue);

   // Total tax
   dTotalTax = dTax1+dTax2;
   if (dTotalTax > 0.0)
      sprintf(pTaxBase->TotalTaxAmt, "%.2f", dTotalTax);

   // Paid amt
   if (dPaid1 > 0.0)
   {
      pTaxBase->Inst1Status[0] = TAX_BSTAT_PAID;
      sprintf(pTaxBase->PaidAmt1, "%.2f", dPaid1);
      iTmp = atol(apTokens[TSUP_TX1PMD]);
      if (iTmp > 0)
         sprintf(pTaxBase->PaidDate1, "20%.2d%.2d%.2d", atol(apTokens[TSUP_TX1PMY]), atol(apTokens[TSUP_TX1PMM]), iTmp);
   }
   if (dPaid2 > 0.0)
   {
      pTaxBase->Inst2Status[0] = TAX_BSTAT_PAID;
      sprintf(pTaxBase->PaidAmt2, "%.2f", dPaid2);
      iTmp = atol(apTokens[TSUP_TX2PMD]);
      if (iTmp > 0)
         sprintf(pTaxBase->PaidDate2, "20%.2d%.2d%.2d", atol(apTokens[TSUP_TX2PMY]), atol(apTokens[TSUP_TX2PMM]), iTmp);
   }

   // Penalty
   if (dPen1 > 0.0)
      sprintf(pTaxBase->PenAmt1, "%.2f", dPen1);

   if (dPen2 > 0.0)
      sprintf(pTaxBase->PenAmt2, "%.2f", dPen2);

   // Due Amt
   if (!dTotalDue)
   {
      pTaxBase->Inst1Status[0] = TAX_BSTAT_PAID;
      pTaxBase->Inst2Status[0] = TAX_BSTAT_PAID;
   }

   return 0;
}

int Mpa_Load_TaxSuppCsv(char *pTxSuppFile, char *pTx490File, bool bImport)
{
   char     *pTmp, acRec[MAX_RECSIZE], acUpdApn[32], acTx490Rec[MAX_RECSIZE], 
            acBaseRec[2000], acOutbuf[2000], acBaseFile[_MAX_PATH];
            
   int      iRecCnt, iRet, lCnt, iTmp, iUpdCnt;
   //double   dPaidAmt1, dPaidAmt2, dPenAmt1, dPenAmt2, dDueAmt;

   FILE        *fdTxSupp, *fdTx490, *fdBase;
   TAXBASE     *pTaxBase = (TAXBASE *)&acBaseRec[0];

   LogMsg0("\nLoading Tax Supplemental");

   LogMsg("Open Supplemental tax file %s", pTxSuppFile);
   if (!(fdTxSupp = fopen(pTxSuppFile, "r")))
   {
      LogMsg("***** Error opening Secured tax roll file: %s\n", pTxSuppFile);
      return -4;
   }

   //LogMsg("Open current tax bill file %s", pTx490File);
   //if (!(fdTx490 = fopen(pTx490File, "r")))
   //{
   //   LogMsg("***** Error opening current tax bill file: %s\n", pTx490File);
   //   return -4;
   //}

   // Prepare output file
   sprintf(acBaseFile, sTaxOutTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "Base");

   // Open base file
   LogMsg("Append to Base file %s", acBaseFile);
   if (!(fdBase = fopen(acBaseFile, "a+")))
   {
      LogMsg("***** Error opening base file: %s\n", acBaseFile);
      return -4;
   }

   // Initialize variables
   lCnt = 0;
   iUpdCnt=iRecCnt=0;
   acUpdApn[0] = 0;

   // Process loop
   while (!feof(fdTxSupp))
   {
      pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdTxSupp);
      if (!pTmp)
         break;

      // Parse tax supp rec
      iRet = Mpa_ParseTaxSupp(acBaseRec, acRec);
      if (!iRet)
      {
#ifdef _DEBUG
         //if (!memcmp(pTaxBase->Apn, "0011400260", 10))
         //   iRet = 0;
#endif
         //while ((iTmp = memcmp(acUpdApn, pTaxBase->Assmnt_No, strlen(pTaxBase->Assmnt_No))) < 0)
         //{
         //   // Read next record
         //   if (!fgets(acTx490Rec, MAX_RECSIZE, fdTx490))
         //      break;
         //   iTokens = ParseStringNQ(acTx490Rec, '|', T490_COLS, apLocalTokens);
         //   sprintf(acUpdApn, "%s%s%s%s", apLocalTokens[T490_TXLK01], apLocalTokens[T490_TXLK02], apLocalTokens[T490_TXLK03], apLocalTokens[T490_TXLK04]);
         //}
            
         // APN matched, update current tax base record
         //if (!(iTmp = memcmp(acUpdApn, pTaxBase->Assmnt_No, strlen(pTaxBase->Assmnt_No))))
         //{
         //   // At least one bill has not been paid
         //   dPaidAmt1 = atof(apLocalTokens[T490_TX1PMA]);
         //   if (dPaidAmt1 > 0.0)
         //   {
         //      pTaxBase->Inst1Status[0] = TAX_BSTAT_PAID;
         //      sprintf(pTaxBase->PaidAmt1, "%.2f", dPaidAmt1);
         //      iTmp = atol(apLocalTokens[T490_TX1PMD]);
         //      if (iTmp > 0)
         //         sprintf(pTaxBase->PaidDate1, "20%.2d%.2d%.2d", atol(apLocalTokens[T490_TX1PMY]), atol(apLocalTokens[T490_TX1PMM]), iTmp);
         //   } else
         //      pTaxBase->Inst1Status[0] = TAX_BSTAT_UNPAID;

         //   dPaidAmt2 = atof(apLocalTokens[T490_TX2PMA]);
         //   if (dPaidAmt2 > 0.0)
         //   {
         //      pTaxBase->Inst2Status[0] = TAX_BSTAT_PAID;
         //      sprintf(pTaxBase->PaidAmt2, "%.2f", dPaidAmt2);
         //      iTmp = atol(apLocalTokens[T490_TX2PMD]);
         //      if (iTmp > 0)
         //         sprintf(pTaxBase->PaidDate2, "20%.2d%.2d%.2d", atol(apLocalTokens[T490_TX2PMY]), atol(apLocalTokens[T490_TX2PMM]), iTmp);
         //   } else
         //      pTaxBase->Inst2Status[0] = TAX_BSTAT_UNPAID;

         //   // Penalty
         //   dPenAmt1 = atof(apLocalTokens[T490_TX1PNL]);
         //   if (dPenAmt1 > 0.0)
         //      sprintf(pTaxBase->PenAmt1, "%.2f", dPenAmt1);

         //   dPenAmt2 = atof(apLocalTokens[T490_TX2PNL]);
         //   if (dPenAmt2 > 0.0)
         //      sprintf(pTaxBase->PenAmt2, "%.2f", dPenAmt2);

         //   // Due Amt
         //   dDueAmt = atof(apLocalTokens[T490_TX1DUE]);
         //   dDueAmt += atof(apLocalTokens[T490_TX2DUE]);
         //   if (dDueAmt > 0.0)
         //      sprintf(pTaxBase->TotalDue, "%.2f", dDueAmt+dPenAmt1+dPenAmt2);
         //   else
         //   {
         //      pTaxBase->Inst1Status[0] = TAX_BSTAT_PAID;
         //      pTaxBase->Inst2Status[0] = TAX_BSTAT_PAID;
         //      if (dPaidAmt2 == 0.0)
         //         strcpy(pTaxBase->PaidAmt2, pTaxBase->PaidAmt1);
         //   }

         //   // Last update date
         //   sprintf(pTaxBase->Upd_Date, "%d", lUpdTaxFileDate);
         //   iUpdCnt++;
         //   // Update record has been used, reset to get another one
         //   acUpdApn[0] = 0;     
         //} else
         //{
         //   // Record not found, this must have been paid
         //   if (pTaxBase->PaidAmt1[0] < '1')
         //      strcpy(pTaxBase->PaidAmt1, pTaxBase->TaxAmt1);
         //   if (pTaxBase->PaidAmt2[0] < '1')
         //      strcpy(pTaxBase->PaidAmt2, pTaxBase->TaxAmt2);
         //   pTaxBase->Inst2Status[0] = TAX_BSTAT_PAID;
         //   pTaxBase->Inst1Status[0] = TAX_BSTAT_PAID;

         //   // Last update date
         //   sprintf(pTaxBase->Upd_Date, "%d", lLastTaxFileDate);
         //}

         // Last update date
         sprintf(pTaxBase->Upd_Date, "%d", lLastTaxFileDate);

         // Create TaxBase record
         Tax_CreateTaxBaseCsv(acOutbuf, (TAXBASE *)&acBaseRec);
         fputs(acOutbuf, fdBase);
         iRecCnt++;
      }

      if (!(++lCnt % 1000))
         printf("\r%d", lCnt);
   }

   if (fdTxSupp)
      fclose(fdTxSupp);
   //if (fdTx490)
   //   fclose(fdTx490);
   if (fdBase)
      fclose(fdBase);

   LogMsgD("\nTotal processed records  : %u", lCnt);
   LogMsgD("         output records  : %u\n", iRecCnt);

   return 0;
}

/*********************************** loadMpa **************************************
 *
 * Input files:
 *    - Mpa_Lien.txt       (Lien file, 1233-byte EBCDIC with CRLF)
 *    - AssessorRoll.txt   (roll file, 1233-byte EBCDIC with CRLF)
 *
 * Operation commands:
 *    - Normal update:  LoadOne -CMPA -U [-Mp] -Xsi
 *    - Load Lien:      LoadOne -CMPA -L -Xl -Mp -Xs
 *    - Optional:       -Mp (merge public file. Do this whenever new file available)
 *
 * Notes:
 *    - DO NOT change the order of loading CHAR and SALE file.  ConvertSale
 *      has to be done after ConvertChar.
 *    - Copy current sale & char files before processing LDR
 *
 **********************************************************************************/

int loadMpa(int iSkip)
{
   int   iRet;
   bool  bCombine=false;
   char  *pTmp, acTmpFile[_MAX_PATH], acTmp[128];

   iApnLen = myCounty.iApnLen;

   // Load tax
   if (iLoadTax == TAX_LOADING)                    // -T or -Lt
   {
      char acXlsTaxFile[_MAX_PATH], acXlsDelqFile[_MAX_PATH], acCurTaxFile[_MAX_PATH], acDelqTaxFile[_MAX_PATH],
           acDefTaxDir[_MAX_PATH], acBaseTaxFile[_MAX_PATH], acXlsBaseFile[_MAX_PATH],
           acSuppTaxFile[_MAX_PATH], acXlsSuppFile[_MAX_PATH], acOldTaxFmt[_MAX_PATH];

      // Move file from sub folder for processing
      iRet = GetIniString(myCounty.acCntyCode, "TaxDir", "", acDefTaxDir, _MAX_PATH, acIniFile);
      GetIniString(myCounty.acCntyCode, "BaseTax", "", acXlsBaseFile, _MAX_PATH, acIniFile);
      GetIniString(myCounty.acCntyCode, "DelqTax", "", acXlsDelqFile, _MAX_PATH, acIniFile);
      GetIniString(myCounty.acCntyCode, "CurrTax", "", acXlsTaxFile, _MAX_PATH, acIniFile);
      GetIniString(myCounty.acCntyCode, "OldTaxFmt", "", acOldTaxFmt, _MAX_PATH, acIniFile);
      sprintf(acBaseTaxFile, "%s\\%s\\BaseTax.txt", acTmpPath, myCounty.acCntyCode);
      sprintf(acSuppTaxFile, "%s\\%s\\SuppTax.txt", acTmpPath, myCounty.acCntyCode);
      sprintf(acCurTaxFile, "%s\\%s\\CurTax.txt", acTmpPath, myCounty.acCntyCode);
      sprintf(acDelqTaxFile, "%s\\%s\\DelqTax.txt", acTmpPath, myCounty.acCntyCode);

      // Only process if new tax file
      iRet = isNewTaxFile(acXlsTaxFile, myCounty.acCntyCode);
      if (iRet <= 0)
      {
         lLastTaxFileDate = 0;
         return iRet;
      }

      // Check for new county file
      if (acOldTaxFmt[0] > ' ' && acXlsTaxFile[0] > ' ')
      {
         iRet = chkFileDate(acOldTaxFmt, acXlsTaxFile);
         if (iRet == 1)
         {
            LogMsg("*** New file arrival.  This file \"%s\" need to convert to XLSX before processing.", acOldTaxFmt);
            iRet = CNTY_MPA;
            return iRet;
         }
      }

      // Supplemental tax file
      GetIniString(myCounty.acCntyCode, "SuppFile", "", acXlsSuppFile, _MAX_PATH, acIniFile);
      GetIniString(myCounty.acCntyCode, "SuppSheet", "", acTmp, _MAX_PATH, acIniFile);
      iRet = ConvXls2Csv(acSuppTaxFile, acXlsSuppFile, acTmp, "S(#2,C,A,#3,C,A,#4,C,A,#5,C,A,#1,C,A) DEL(124) OMIT(#1,C,GT,\"9\")", TSUP_COLS);

      // T490 & T491 are not sorted by APN.  They need to be sorted if we try to merge with T405
      GetIniString(myCounty.acCntyCode, "CurrSheet", "", acTmp, _MAX_PATH, acIniFile);
      iRet = ConvXls2Csv(acCurTaxFile, acXlsTaxFile, acTmp, "S(#2,N,A,#3,N,A,#4,N,A,#5,N,A,#1,C,A) DEL(124) OMIT(#1,C,GT,\"9\")", T490_COLS);
      lUpdTaxFileDate = getFileDate(acXlsTaxFile);

      // Delinquent file
      GetIniString(myCounty.acCntyCode, "DelqTax", "", acXlsDelqFile, _MAX_PATH, acIniFile);
      GetIniString(myCounty.acCntyCode, "DelqSheet", "", acTmp, _MAX_PATH, acIniFile);
      iRet = ConvXls2Csv(acDelqTaxFile, acXlsDelqFile, acTmp, "S(#2,C,A,#3,C,A,#4,C,A,#5,C,A,#10,C,D) DEL(124) OMIT(#2,C,GT,\"9\")", T491_COLS);
      lDelqTaxFileDate = getFileDate(acXlsBaseFile);

      // Tax base
      GetIniString(myCounty.acCntyCode, "BaseSheet", "", acTmp, _MAX_PATH, acIniFile);
      iRet = ConvXls2Csv(acBaseTaxFile, acXlsBaseFile, acTmp, "S(#6,C,A,#7,C,A) DEL(124) OMIT(#6,C,GT,\"9\",OR,#6,C,LT,\"0\") ", T405_COLS);
      lLastTaxFileDate = getFileDate(acXlsBaseFile);
      if (lLastTaxFileDate < lUpdTaxFileDate)
         lLastTaxFileDate = lUpdTaxFileDate;

      //CStringArray asList;
      //CSpreadSheet sprSht;
      //iRet = sprSht.GetDriverList(asList);
      //while (iRet > 0) 
      //{
      //   iRet--;
      //   printf("%s\n", asList.GetAt(iRet)); 
      //}

      // Load Tax Base and Delq
      //iRet = Mpa_Load_TaxBaseXls(acXlsTaxFile, acCurTaxFile, false);
      iRet = Mpa_Load_TaxBaseCsv(acBaseTaxFile, acCurTaxFile, false);
      if (!iRet && lLastTaxFileDate > 0)
      {
         // Load Delq file
         iRet = Mpa_Load_TaxDelqCsv(acDelqTaxFile, false);

         // Load Supplemental #1
         iRet = Mpa_Load_TaxSuppCsv(acSuppTaxFile, acCurTaxFile, false);
         //iRet = Mpa_Load_TaxSuppXls(acCurTaxDir, 0);
         //if (iRet >= 0)
         //   iRet = Mpa_Load_TaxSuppXls(acCurTaxDir, 2);

         if (iRet >= 0 && bTaxImport)
         {
            iRet = doTaxImport(myCounty.acCntyCode, TAX_BASE);
            if (!iRet)
            {
               iRet = doTaxImport(myCounty.acCntyCode, TAX_DETAIL);
               if (!iRet)
                  iRet = doTaxImport(myCounty.acCntyCode, TAX_AGENCY);
               if (!iRet)
                  iRet = doTaxImport(myCounty.acCntyCode, TAX_DELINQUENT);
            }
         }

         // Update Base Delq
         if (!iRet)
            iRet = updateDelqFlag(myCounty.acCntyCode);
      } 
   }

   if (!iLoadFlag)
      return iRet;

   // Load lookup table
   iRet = LoadLUTable(acLookupTbl, "[Quality]", NULL, MAX_ATTR_ENTRIES);
   if (!iRet)
   {
      LogMsg("***** Error Loading table [Quality] in %s", acLookupTbl);
      return 1;
   }

   // Extract sale data
   if (iLoadFlag & EXTR_SALE)                      // -Xs
   {
      // Convert improved sale file - Convert MDB to fixed length format
      GetIniString(myCounty.acCntyCode, "ImprSale", "", acCharFile, _MAX_PATH, acIniFile);
      if (!_access(acCharFile, 0))
      {
         // Convert Improved Sales_be1.mdb to Impr_Sale.dat & Attr_Exp.Impr
         iRet = Mpa_ExtrImprSale(acCharFile);
         if (iRet <= 0)
            LogMsg("***** Error converting %s to fixed length format", acCharFile);
         else
            bCombine=true;
      }

      // Convert land sale file 
      GetIniString(myCounty.acCntyCode, "LandSale", "", acSaleFile, _MAX_PATH, acIniFile);
      if (!_access(acSaleFile, 0))
      {
         // Convert land 2000_be.mdb to Land_Sale.dat & Attr_Exp.Land
         iRet = Mpa_ExtrLandSale(acSaleFile);
         if (iRet <= 0)
         {
            LogMsg("***** Error converting %s to fixed length format", acSaleFile);
            return -1;
         } else
            bCombine=true;
      }

      // Convert commercial sale file 
      GetIniString(myCounty.acCntyCode, "CommSale", "", acCharFile, _MAX_PATH, acIniFile);
      if (!_access(acCharFile, 0))
      {
         // Convert Commercial_Sales.mdb to Comm_Sale.dat & Attr_Exp.Comm
         iRet = Mpa_ExtrCommSale(acCharFile);
         if (iRet <= 0)
            LogMsg("***** Error converting %s to fixed length format", acCharFile);
         else
            bCombine=true;
      }
   } 
   acCharFile[0] = 0;

   // Combine Lamd & Impr extracted data
   if (bCombine)
   {
      char acImprFile[_MAX_PATH], acCommFile[_MAX_PATH], acLandFile[_MAX_PATH], acSrtFile[_MAX_PATH];

      LogMsg0("Combine Land & Impr sale data");

      // Combine char file Attr_exp.Impr+Attr_Exp.Land+Attr_Exp.comm=Attr_Exp.dat
      sprintf(acCharFile, acAttrTmpl, "MPA", "Dat");
      sprintf(acLandFile, acAttrTmpl, "MPA", "Land");
      sprintf(acImprFile, acAttrTmpl, "MPA", "Impr");
      sprintf(acCommFile, acAttrTmpl, "MPA", "Comm");
      sprintf(acTmpFile, "%s+%s+%s", acImprFile, acLandFile, acCommFile);

      sprintf(acTmp, "S(1,10,C,A,11,8,C,D) F(TXT)");
      iRet = sortFile(acTmpFile, acCharFile, acTmp);

      // Combine sales Impr_Sale.dat+Land_Sale.dat+Comm_Sale.dat=Mpa_Sale.sls
      sprintf(acLandFile, acESalTmpl, "MPA", "Land", "Dat");
      sprintf(acImprFile, acESalTmpl, "MPA", "Impr", "Dat");
      sprintf(acCommFile, acESalTmpl, "MPA", "Comm", "Dat");
      sprintf(acTmpFile, "%s+%s+%s", acImprFile, acLandFile, acCommFile);

      // Sort sale file - APN (asc), Saledate (asc), Sale price (desc)
      sprintf(acTmp, "S(1,14,C,A,27,8,C,A,57,10,C,D) DUPO(1,34)");
      sprintf(acSrtFile, acESalTmpl, "MPA", "MPA", "srt");
      iRet = sortFile(acTmpFile, acSrtFile, acTmp);
      if (iRet > 0)
      {
         if (!_access(acCSalFile, 0))
         {
            sprintf(acTmpFile, acESalTmpl, "MPA", "MPA", "sav");
            iRet = MoveFileEx(acCSalFile, acTmpFile, MOVEFILE_REPLACE_EXISTING);
         }
         iRet = rename(acSrtFile, acCSalFile);
         if (iRet)
            LogMsg("***** Error renameing %s to %s: %d", acSrtFile, acCSalFile, GetLastError());
         else 
         {
            iLoadFlag |= MERG_CSAL;
            iRet = 0;
         }
      } else
         iRet = -1;
   } else
      sprintf(acCharFile, acAttrTmpl, "MPA", "Dat");

   acSaleFile[0] = 0;

   // If expected file name not found, remove spaces from file name
   if (_access(acRollFile, 0))
   {
      if (pTmp = strchr(acRollFile, ' '))
      {
         *pTmp = 0;
         strcat(acRollFile, ++pTmp);
      }
   }

   // Translate roll file
   strcpy(acTmpFile, acRollFile);
   if (pTmp = strrchr(acTmpFile, '.'))
      strcpy(pTmp, ".ASC");
   else
      strcat(acTmpFile, ".ASC");

   LogMsg("Translate %s to Ascii %s", acRollFile, acTmpFile);
   iRet = doEBC2ASC(acRollFile, acTmpFile);
   if (!iRet)
   {
      strcpy(acRollFile, acTmpFile);

      // Extract lien
      if (iLoadFlag & EXTR_LIEN)
         iRet = Mpa_ExtrLien();

      if (iLoadFlag & LOAD_LIEN)                   // -L
      {
         // Create Lien file
         LogMsg0("Load %s LDR file", myCounty.acCntyCode);
         iRet = Mpa_Load_LDR(iLoadFlag, iSkip);
      } else if (iLoadFlag & LOAD_UPDT)            // -U
      {
         // Update roll
         LogMsg0("Load %s roll update file", myCounty.acCntyCode);
         iRet = Mpa_LoadRoll(iLoadFlag, iSkip);
      }

      // Merge public file
      if (iLoadFlag & MERG_PUBL)                   // -Mp
      {
         if (!_access(acPubParcelFile, 0))
         {
            LogMsgD("Merge public parcel file %s", acPubParcelFile);
            iRet = Mpa_MergePubl(iSkip);
            if (!iRet && (iLoadFlag & LOAD_LIEN))
               lLDRRecCount = lRecCnt;
         } else
            LogMsg("***** Public parcel file <%s> is missing", acPubParcelFile);
      }

      // Copy DocNum to sale file
      if (!iRet && (iLoadFlag & EXTR_SALE))
         iRet = Mpa_Roll2Sale(acCSalFile);

      // Apply cum sale file to R01
      if (!iRet && (iLoadFlag & MERG_CSAL) )       // -Ms
      {
         // Apply Mpa_Sale.sls to R01 file
         iRet = ApplyCumSale(iSkip, acCSalFile, false, SALE_USE_SCSALREC, CLEAR_OLD_SALE|DONT_CHK_DOCNUM);
      }

      // Format DocLinks - Doclinks are concatenate fields separated by comma 
      if (!iRet && (iLoadFlag & (LOAD_UPDT|LOAD_LIEN)) )
         iRet = updateDocLinks(Mpa_MakeDocLink, myCounty.acCntyCode, iSkip);
         //iRet = Mpa_FmtDocLinks(iSkip);
   } else
   {
      if (iRet == NOTFOUND_ERR)
         LogMsg("***** Invalid file name or file not found %s.  Please check input file and modify LoadOne.ini if needed.", acRollFile);
      else if (iRet == WRITE_ERR)
         LogMsg("***** Error writing to %s.  Please check available disk space.", acTmpFile);
      else  // OPEN_ERR
         LogMsg("***** Error converting %s to ASCII.  Please check input and output file then retry.", acRollFile);
   }

   // Fix TRA
   if (!iRet && (lOptMisc & M_OPT_FIXTRA))
      iRet = PQ_FixR01(myCounty.acCntyCode, PQ_FIX_TRA, 0, true);

  return iRet;
}