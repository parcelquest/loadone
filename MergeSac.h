#ifndef  _MERGE_SDX_H
#define  _MERGE_SDX_H

// Old County roll
#define ROFF_APN              1-1
#define ROFF_OLD_TRA          15-1
#define ROFF_NEW_TRA          20-1
#define ROFF_LEIN_OWNER_FLAG  25-1
#define ROFF_UNUSED1          26-1
#define ROFF_LAND_USE         27-1
#define ROFF_ZONING           33-1
#define ROFF_UNUSED2          39-1
#define ROFF_S_STRNUM         46-1
#define ROFF_S_STRSUB         51-1
#define ROFF_S_STRNAME        54-1
#define ROFF_S_ZIP            70-1
#define ROFF_OWNER_CODE       75-1
#define ROFF_OWNER_NAME       77-1
#define ROFF_M_STREET         127-1
#define ROFF_M_CITY           151-1
#define ROFF_M_STATE          165-1
#define ROFF_M_ZIP            167-1
#define ROFF_CO_NAME          172-1
#define ROFF_LEIN_DATE_OWNER  190-1
#define ROFF_REC_DATE         240-1
#define ROFF_REC_PAGE         246-1
#define ROFF_DEED_TYPE        250-1
#define ROFF_UNUSED3          254-1
#define ROFF_LAND             282-1
#define ROFF_STRUCTURE        292-1
#define ROFF_FIXTURE          302-1
#define ROFF_PERS_PROP        312-1
#define ROFF_BUS_INVENTORY    322-1
#define ROFF_HO_EXE           332-1
#define ROFF_VET_EXE          342-1
#define ROFF_REL_EXE          352-1
#define ROFF_EXE_CODE         362-1
#define ROFF_UNUSED4          363-1

#define RSIZ_APN              14
#define RSIZ_TRA              5
#define RSIZ_LD_OWNER_FLAG    1
#define RSIZ_LAND_USE         6
#define RSIZ_ZONING           6
#define RSIZ_S_STRNUM         5
#define RSIZ_S_STRSUB         3
#define RSIZ_S_STRNAME        16
#define RSIZ_S_ZIP            5
#define RSIZ_OWNER_CODE       2
#define RSIZ_OWNER_NAME       50
#define RSIZ_M_STREET         24
#define RSIZ_M_CITY           14
#define RSIZ_M_STATE          2
#define RSIZ_M_ZIP            5
#define RSIZ_CO_NAME          18
#define RSIZ_LD_OWNER         50
#define RSIZ_REC_DATE         6
#define RSIZ_REC_PAGE         4
#define RSIZ_DEED_TYPE        4
#define RSIZ_LAND             10
#define RSIZ_STRUCTURE        10
#define RSIZ_FIXTURE          10
#define RSIZ_PERS_PROP        10
#define RSIZ_BUS_INVENTORY    10
#define RSIZ_HO_EXE           10
#define RSIZ_VET_EXE          10
#define RSIZ_REL_EXE          10
#define RSIZ_EXE_CODE         1
#define RSIZ_UNUSED4          13
#define RSIZ_SAC              375

typedef struct _tSacSitusRec
{
   char  S_StreetNum[RSIZ_S_STRNUM];
   char  S_StreetFract[RSIZ_S_STRSUB];
   char  S_StreetName[RSIZ_S_STRNAME]; // May contain dir, name, sfx, unit#
   char  S_Zip[RSIZ_S_ZIP];
} SAC_SADR;

typedef struct _tSacMailRec
{
   char  M_Addr1[RSIZ_M_STREET];
   char  M_City[RSIZ_M_CITY];
   char  M_St[RSIZ_M_STATE];
   char  M_Zip[RSIZ_M_ZIP];
} SAC_MADR;

typedef struct _tSacRollRec
{
   char  Apn[RSIZ_APN];
   char  OldTRA[RSIZ_TRA];
   char  TRA[RSIZ_TRA];
   char  LD_OwnerFlag[2];              // 'L'
   char  UseCode[RSIZ_LAND_USE];
   char  Zoning[RSIZ_ZONING];
   char  filler1[7];
   SAC_SADR Situs;
   char  OwnerCode[RSIZ_OWNER_CODE];
   char  OwnerName[RSIZ_OWNER_NAME];
   SAC_MADR Mailing;
   char  CareOf[RSIZ_CO_NAME];
   char  LD_Owner[RSIZ_LD_OWNER];
   char  RecDate[RSIZ_REC_DATE];
   char  RecPage[RSIZ_REC_PAGE];
   char  DeedType[RSIZ_DEED_TYPE];     // deed type or vesting
   char  filler2[28];
   char  Land[RSIZ_LAND];
   char  Impr[RSIZ_LAND];
   char  Fixt_Val[RSIZ_LAND];          // Fixed machinery & equiptment exempt
   char  PP_Val[RSIZ_LAND];
   char  Bus_Inv[RSIZ_LAND];
   char  HO_Exe[RSIZ_LAND];
   char  Vet_Exe[RSIZ_LAND];
   char  Rel_Exe[RSIZ_LAND];
   char  Exe_Code;
   char  filler3[13];
} SAC_ROLL;

#define UOFF_APN              1-1
#define UOFF_SUBPARCEL        11-1
#define UOFF_SUBPARCEL_UNS    15-1
#define UOFF_PROPCLS          19-1
#define UOFF_TRA              21-1
#define UOFF_S_STRNUM         26-1
#define UOFF_S_STRSUB         31-1
#define UOFF_S_STRNAME        32-1
#define UOFF_S_ZIP            52-1
#define UOFF_OWNER_NAME       57-1
#define UOFF_CO_NAME          91-1
#define UOFF_M_STRNUM         125-1
#define UOFF_M_STRSUB         130-1
#define UOFF_M_STREET         131-1
#define UOFF_M_CITY           151-1
#define UOFF_M_STATE          164-1
#define UOFF_M_ZIP            167-1
#define UOFF_REL_EXE          172-1
#define UOFF_VET_EXE          187-1
#define UOFF_HO_EXE           202-1
#define UOFF_LAND             217-1
#define UOFF_STRUCTURE        232-1
#define UOFF_FIXTURE          247-1
#define UOFF_PERS_PROP        262-1
#define UOFF_BOAT             277-1
#define UOFF_AIRCRAFT         292-1
#define UOFF_BOAT_ID          307-1
#define UOFF_AIRCRAFT_ID      313-1
#define UOFF_SVF              320-1

#define USIZ_APN              10
#define USIZ_SUBPARCEL        4
#define USIZ_SUBPARCEL_UNS    4
#define USIZ_PROPCLS          2
#define USIZ_TRA              5
#define USIZ_S_STRNUM         5
#define USIZ_S_STRSUB         1
#define USIZ_S_STRNAME        20
#define USIZ_S_ZIP            5
#define USIZ_OWNER_NAME       34
#define USIZ_CO_NAME          34
#define USIZ_M_STRNUM         5
#define USIZ_M_STRSUB         1
#define USIZ_M_STRNAME        20
#define USIZ_M_CITY           13
#define USIZ_M_STATE          3
#define USIZ_M_ZIP            5
#define USIZ_REL_EXE          15
#define USIZ_VET_EXE          15
#define USIZ_HO_EXE           15
#define USIZ_LAND             15
#define USIZ_STRUCTURE        15
#define USIZ_FIXTURE          15
#define USIZ_PERS_PROP        15
#define USIZ_BOAT             15
#define USIZ_AIRCRAFT         15
#define USIZ_BOAT_ID          6
#define USIZ_AIRCRAFT_ID      7
#define USIZ_SVF              5

typedef struct _tSacUnsRec
{
   char  Apn[USIZ_APN];
   char  SubParcel[USIZ_SUBPARCEL];
   char  SubParcelUns[USIZ_SUBPARCEL_UNS];
   char  PropCls[USIZ_PROPCLS];
   char  TRA[USIZ_TRA];
   char  S_StreetNum[USIZ_S_STRNUM];
   char  S_StreetSub[USIZ_S_STRSUB];
   char  S_StreetName[USIZ_S_STRNAME]; // May contain dir, name, sfx, unit#
   char  S_Zip[USIZ_S_ZIP];
   char  OwnerName[USIZ_OWNER_NAME];
   char  CareOf[USIZ_CO_NAME];
   char  M_StreetNum[USIZ_M_STRNUM];
   char  M_StreetSub[USIZ_M_STRSUB];
   char  M_StreetName[USIZ_M_STRNAME]; // May contain dir, name, sfx, unit#
   char  M_City[USIZ_M_CITY];
   char  M_St[USIZ_M_STATE];
   char  M_Zip[USIZ_M_ZIP];
   char  Rel_Exe[USIZ_REL_EXE];
   char  Vet_Exe[USIZ_VET_EXE];
   char  HO_Exe[USIZ_HO_EXE];
   char  Land[USIZ_LAND];
   char  Impr[USIZ_LAND];
   char  Fixt_Val[USIZ_LAND];          // Fixed machinery & equiptment exempt
   char  PP_Val[USIZ_LAND];
   char  Boat[USIZ_BOAT];
   char  Aircraft[USIZ_BOAT];
   char  BoatId[USIZ_BOAT_ID];
   char  AircraftId[USIZ_AIRCRAFT_ID];
   char  SecuredValue_Flg[USIZ_SVF];
} SAC_UNS;

/* PropCls table
02 SECURED ROLL TRANSFER
12 Agricultural
14 Apartments Personal Prop
15 Leasing Accounts - Lessor
16 Improvement on Leased Lan
17 Boat - DMV Registered
18 General Aircraft
19 Boats - Coast Guard Reg.
20 Commercial Accounts
22 Comm Account - Exempt
25 Oil Comp/Gas Station
27 Leased Equip - Lessee
28 Leased Equip - Cond Sales
40 Financial/Bank
44 Possessory Interest
60 Commercial Aircraft
75 Gas Pipeline
79 Commercial Fishing
*/

LU_ENTRY  Sac_PropCls[] =
{
   "12", "Agricultural",
   "14", "Apt - Pers Prop",
   "15", "Leasing Account",
   "16", "Impr - Leased Land",
   "17", "Boat - DMV Reg",
   "18", "General Aircraft",
   "19", "Boat - CG Reg",
   "20", "Commercial Accts",
   "22", "Comm Acct - Exempt",
   "25", "Oil/Gas Station",
   "27", "Leased Equip - Les",
   "28", "Leased Equip - CS",
   "40", "Financial/Bank",
   "44", "Possessory Int",
   "60", "Comm Aircraft",
   "75", "Gas Pipeline",
   "79", "Comm Fishing",
   "", ""
};

#define SOFF_APN                 1
#define SOFF_FILLER1             15
#define SOFF_S_STRNUM            17
#define SOFF_S_STRNAME           23
#define SOFF_QUALITY_CLASS       44
#define SOFF_MODEL_NUMBER        49
#define SOFF_STORIES             55
#define SOFF_FLOOR               60
#define SOFF_CENTRAL_AIR         61
#define SOFF_EFFYR               68
#define SOFF_FILLER2             72
#define SOFF_ENTRY_HALL          75
#define SOFF_DINING_ROOM         76
#define SOFF_BEDS                79
#define SOFF_UTIL_ROOM           80
#define SOFF_SUPL_ROOM           81
#define SOFF_ROOMS               82
#define SOFF_BATHS               85
#define SOFF_FILLER3             88
#define SOFF_CONDITION           95
#define SOFF_BUILT_INS           98
#define SOFF_PARKSPACES          99
#define SOFF_1STFLR_AREA         102
#define SOFF_2NDFLR_AREA         107
#define SOFF_CONV_GAR_AREA       115
#define SOFF_ADD_AREA            131
#define SOFF_AREA_4_MOD          139
#define SOFF_BASEMENT_AREA       144
#define SOFF_GAR_AREA            160
#define SOFF_PATIO_CODE          191
#define SOFF_FP_CODE             201
#define SOFF_POOL_CODE           226
#define SOFF_POOL_DATE           231
#define SOFF_LOTSQFT             257
#define SOFF_LOTACRES            273
#define SOFF_NBH_CODE            315
#define SOFF_ACT_SALE_PRICE      331
#define SOFF_REC_DATE            341   // yymmdd
#define SOFF_REC_PAGE            347
#define SOFF_RELIABILITY_CODE    351
#define SOFF_SALE_TYPE           353
#define SOFF_CASH_PRICE          357
#define SOFF_NUM_PARCELS         367
#define SOFF_FILLER4             370
#define SOFF_PARTIAL_INT         376
#define SOFF_DEED_TYPE           379
#define SOFF_FILLER5             383
#define SOFF_ROOF                385
#define SOFF_LANDUSE             391
#define SOFF_P_C_NUMBER          401
#define SOFF_FILLER6             407
#define SOFF_STAMP_AMOUNT        678
#define SOFF_STAMP_CODE          686
#define SOFF_GRANTOR             703
#define SOFF_GRANTEE             727
#define SOFF_CAREOF              751
#define SOFF_M_ADDR1             775
#define SOFF_M_CITY              799
#define SOFF_M_ST                819
#define SOFF_M_ZIP               821

#define SSIZ_APN                 14
#define SSIZ_FILLER0             2
#define SSIZ_S_STRNUM            6
#define SSIZ_S_STRNAME           19
#define SSIZ_FILLER1             2
#define SSIZ_QUALITY_CLASS       5
#define SSIZ_MODEL_NUMBER        6
#define SSIZ_STORIES             1
#define SSIZ_FILLER1A            4
#define SSIZ_FLOOR               1
#define SSIZ_CENTRAL_AIR         1
#define SSIZ_FILLER1B            6
#define SSIZ_EFFYR               4
#define SSIZ_FILLER2             3
#define SSIZ_ENTRY_HALL          1
#define SSIZ_DINING_ROOM         1
#define SSIZ_FILLER2A            2
#define SSIZ_BEDS                1
#define SSIZ_UTIL_ROOM           1
#define SSIZ_SUPL_ROOM           1
#define SSIZ_ROOMS               3
#define SSIZ_BATHS               3
#define SSIZ_FILLER3             7
#define SSIZ_CONDITION           1
#define SSIZ_FILLER3A            2
#define SSIZ_BUILT_INS           1
#define SSIZ_PARKSPACES          1
#define SSIZ_FILLER3B            2
#define SSIZ_1STFLR_AREA         5
#define SSIZ_2NDFLR_AREA         5
#define SSIZ_FILLER3C            3
#define SSIZ_CONV_GAR_AREA       5
#define SSIZ_FILLER3D            11
#define SSIZ_ADD_AREA            5
#define SSIZ_FILLER3E            3
#define SSIZ_AREA_4_MOD          5
#define SSIZ_BASEMENT_AREA       5
#define SSIZ_FILLER3F            11
#define SSIZ_GAR_AREA            5
#define SSIZ_FILLER3G            26
#define SSIZ_PATIO_CODE          5
#define SSIZ_FILLER3H            5
#define SSIZ_FP_CODE             5
#define SSIZ_FILLER3I            20
#define SSIZ_POOL_CODE           5
#define SSIZ_POLL_DATE           4
#define SSIZ_FILLER3J            22
#define SSIZ_LOTSQFT             8
#define SSIZ_FILLER3K            8
#define SSIZ_LOTACRES            6
#define SSIZ_FILLER3L            36
#define SSIZ_NBH_CODE            5
#define SSIZ_FILLER3M            11
#define SSIZ_SALE_PRICE          10
#define SSIZ_REC_DATE            6    
#define SSIZ_REC_PAGE            4   
#define SSIZ_DOCNUM              10 
#define SSIZ_RELIABILITY_CODE    2    
#define SSIZ_SALE_TYPE           4    
#define SSIZ_CASH_PRICE          10    
#define SSIZ_NUM_PARCELS         3    
#define SSIZ_FILLER4             6    
#define SSIZ_PARTIAL_INT         3    
#define SSIZ_DEED_TYPE           4    
#define SSIZ_FILLER5             2    
#define SSIZ_ROOF                1    
#define SSIZ_FILLER5A            5
#define SSIZ_LANDUSE             6
#define SSIZ_FILLER5B            4
#define SSIZ_P_C_NUMBER          6
#define SSIZ_FILLER6             271
#define SSIZ_STAMP_AMOUNT        8
#define SSIZ_STAMP_CODE          1
#define SSIZ_FILLER6A            16
#define SSIZ_GRANTOR             24
#define SSIZ_GRANTEE             24
#define SSIZ_CAREOF              24
#define SSIZ_M_ADDR              24
#define SSIZ_M_CITY              20 
#define SSIZ_M_ST                2 
#define SSIZ_M_ZIP               5 
#define SSIZ_FILLER7             25

/* 8/27/2012
typedef struct _tSacSaleRec
{
   char  Apn[RSIZ_APN];
   char  filler1[22];
   char  S_Zip[RSIZ_S_ZIP];
   char  filler2[289];
   char  ActualSalePrice[SSIZ_SALE_PRICE];
   char  RecDate[SSIZ_REC_DATE];             // YYMMDD
   char  RecPage[SSIZ_REC_PAGE];
   char  ReliabilityCode[SSIZ_RELIABILITY_CODE];
   char  SaleType[SSIZ_SALE_TYPE];
   char  AdjSalePrice[SSIZ_SALE_PRICE];
   char  NumParcels[SSIZ_NUM_PARCELS];
   char  DeedDate[SSIZ_DEED_DATE];
   char  PctTransfer[SSIZ_PARTIAL_INT];
   char  DeedType[SSIZ_DEED_TYPE];
   char  filler3[295];
   char  StampAmt[SSIZ_STAMP_AMOUNT];
   char  StampCode;
   char  filler4[16];
   char  Grantor[SSIZ_GRANTOR];
   char  Grantee[SSIZ_GRANTOR];
   char  filler5[100];
} SAC_SALE;
*/
typedef struct _tSacSaleRec
{
   char Apn                 [SSIZ_APN             ];
   char Filler0             [SSIZ_FILLER0         ];
   char S_Strnum            [SSIZ_S_STRNUM        ];
   char S_Strname           [SSIZ_S_STRNAME       ];
   char Filler1             [SSIZ_FILLER1         ];
   char Quality_Class       [SSIZ_QUALITY_CLASS   ];
   char Model_Number        [SSIZ_MODEL_NUMBER    ];
   char Stories             [SSIZ_STORIES         ];
   char Filler1A            [SSIZ_FILLER1A        ];
   char Floor               [SSIZ_FLOOR           ];
   char Central_Air         [SSIZ_CENTRAL_AIR     ];
   char Filler1B            [SSIZ_FILLER1B        ];
   char Effyr               [SSIZ_EFFYR           ];
   char Filler2             [SSIZ_FILLER2         ];
   char Entry_Hall          [SSIZ_ENTRY_HALL      ];
   char Dining_Room         [SSIZ_DINING_ROOM     ];
   char Filler2A            [SSIZ_FILLER2A        ];
   char Beds                [SSIZ_BEDS            ];
   char Util_Room           [SSIZ_UTIL_ROOM       ];
   char Supl_Room           [SSIZ_SUPL_ROOM       ];
   char Rooms               [SSIZ_ROOMS           ];
   char Baths               [SSIZ_BATHS           ];
   char Filler3             [SSIZ_FILLER3         ];
   char Condition           [SSIZ_CONDITION       ];
   char Filler3A            [SSIZ_FILLER3A        ];
   char Built_Ins           [SSIZ_BUILT_INS       ];
   char Parkspaces          [SSIZ_PARKSPACES      ];
   char Filler3B            [SSIZ_FILLER3B        ];
   char Floor1_Area         [SSIZ_1STFLR_AREA     ];
   char Floor2_Area         [SSIZ_2NDFLR_AREA     ];
   char Filler3C            [SSIZ_FILLER3C        ];
   char Conv_Gar_Area       [SSIZ_CONV_GAR_AREA   ];
   char Filler3D            [SSIZ_FILLER3D        ];
   char Add_Area            [SSIZ_ADD_AREA        ];
   char Filler3E            [SSIZ_FILLER3E        ];
   char Area_4_Mod          [SSIZ_AREA_4_MOD      ];
   char Basement_Area       [SSIZ_BASEMENT_AREA   ];
   char Filler3F            [SSIZ_FILLER3F        ];
   char Gar_Area            [SSIZ_GAR_AREA        ];
   char Filler3G            [SSIZ_FILLER3G        ];
   char Patio_Code          [SSIZ_PATIO_CODE      ];
   char Filler3H            [SSIZ_FILLER3H        ];
   char Fp_Code             [SSIZ_FP_CODE         ];
   char Filler3I            [SSIZ_FILLER3I        ];
   char Pool_Code           [SSIZ_POOL_CODE       ];
   char Poll_Date           [SSIZ_POLL_DATE       ];
   char Filler3J            [SSIZ_FILLER3J        ];
   char Lotsqft             [SSIZ_LOTSQFT         ];
   char Filler3K            [SSIZ_FILLER3K        ];
   char Lotacres            [SSIZ_LOTACRES        ];
   char Filler3L            [SSIZ_FILLER3L        ];
   char Nbh_Code            [SSIZ_NBH_CODE        ];
   char Filler3M            [SSIZ_FILLER3M        ];
   char SalePrice           [SSIZ_SALE_PRICE      ];
   char RecDate             [SSIZ_REC_DATE        ];
   char RecPage             [SSIZ_REC_PAGE        ];
   char ReliabilityCode     [SSIZ_RELIABILITY_CODE];
   char SaleType            [SSIZ_SALE_TYPE       ];
   char CashPrice           [SSIZ_CASH_PRICE      ];
   char NumParcels          [SSIZ_NUM_PARCELS     ];
   char Filler4             [SSIZ_FILLER4         ];
   char PctTransfer         [SSIZ_PARTIAL_INT     ];
   char DeedType            [SSIZ_DEED_TYPE       ];
   char Filler5             [SSIZ_FILLER5         ];
   char Roof                [SSIZ_ROOF            ];
   char Filler5A            [SSIZ_FILLER5A        ];
   char Landuse             [SSIZ_LANDUSE         ];
   char Filler5B            [SSIZ_FILLER5B        ];
   char P_C_Number          [SSIZ_P_C_NUMBER      ];
   char Filler6             [SSIZ_FILLER6         ];
   char StampAmt            [SSIZ_STAMP_AMOUNT    ];
   char StampCode           [SSIZ_STAMP_CODE      ];
   char Filler6A            [SSIZ_FILLER6A        ];
   char Grantor             [SSIZ_GRANTOR         ];
   char Grantee             [SSIZ_GRANTEE         ];
   char Careof              [SSIZ_CAREOF          ];
   char M_Addr              [SSIZ_M_ADDR          ];
   char M_City              [SSIZ_M_CITY          ];
   char M_St                [SSIZ_M_ST            ];
   char M_Zip               [SSIZ_M_ZIP           ];
   char Filler7             [SSIZ_FILLER7         ];
   char CrLf[2];
} SAC_SALE;

typedef struct t_SacCumSale
{
   char  Apn[RSIZ_APN];
   char  ActualSalePrice[SSIZ_SALE_PRICE];
   char  RecDate[8];                             // CCYYMMDD
   char  RecPage[SSIZ_REC_PAGE];
   char  ReliabilityCode[SSIZ_RELIABILITY_CODE];
   char  SaleType[SSIZ_SALE_TYPE];
   char  AdjSalePrice[SSIZ_SALE_PRICE];
   char  NumParcels[SSIZ_NUM_PARCELS];
   char  DeedDate[8];
   char  PctTransfer[SSIZ_PARTIAL_INT];
   char  DeedType[SSIZ_DEED_TYPE];
   char  StampAmt[9];
   char  StampCode;
   char  Grantor[SSIZ_GRANTOR];
   char  Grantee[SSIZ_GRANTOR];
   char  SalePrice[SSIZ_SALE_PRICE];
   char  S_Zip[RSIZ_S_ZIP];
   char  filler;
} SAC_CSAL;

// gax2100_apprpubl.txt
#define AOFF_APN                      1
#define AOFF_S_STRNUM                 15
#define AOFF_S_STRNAME                23
#define AOFF_QUALITY_CLASS            44
#define AOFF_MODEL                    49
#define AOFF_NUM_STORIES              55
#define AOFF_FLOOR_TYPE               60
#define AOFF_CENTRAL_AC_HEATING       61
#define AOFF_CONST_YEAR               62
#define AOFF_EFF_YEAR                 67
#define AOFF_ENTRY_HALL               75
#define AOFF_DINING_ROOM              76
#define AOFF_FAMILY_ROOM              78
#define AOFF_NUM_BEDROOMS             79
#define AOFF_UTIL_ROOM                80
#define AOFF_SUP_ROOMS                81
#define AOFF_TOTAL_ROOMS              82
#define AOFF_NUM_BATHS                85
#define AOFF_IMPRV_CONDITION_CODE     95
#define AOFF_BUILT_IN                 98
#define AOFF_GARAGE_SPCS              99
#define AOFF_ORIG_1ST_FLR_AREA        102
#define AOFF_ORIG_2ND_FLR_AREA        107
#define AOFF_CONV_GARG_AREA           115
#define AOFF_TOTAL_AREA_OF_ADDITIONS  131
#define AOFF_TOTAL_AREA_OF_RESIDENCE  139
#define AOFF_FINISHED_BASEMENT_AREA   144
#define AOFF_AREA_OF_GARAGE           160
#define AOFF_COVERED_PATIO_CODE       191
#define AOFF_FIREPLACE_CODE           201
#define AOFF_MISC_CODE                221
#define AOFF_SWIMMING_POOL_CODE       226
#define AOFF_YEAR_POOL_BUILT          231
#define AOFF_LOT_SQFT                 257
#define AOFF_ACRES                    273
#define AOFF_ZONE                     287
#define AOFF_EXCESS_TRAFFIC           296
#define AOFF_BASE_LOT_PERCENT         297
#define AOFF_NUISANCE_FACTOR          312
//#define AOFF_COMMUNITY                315
//#define AOFF_NEIGHBORHOOD_NUMBER      316
#define AOFF_NBH_CODE                 315
#define AOFF_SOLAR_WATER_HEATER       327
#define AOFF_SOLAR_SPACE_HEATER       329
#define AOFF_ACTUAL_SALE_PRICE        331
#define AOFF_RECDATE                  341
#define AOFF_RECPAGE                  347
#define AOFF_RELIABILITY_CODE         351
#define AOFF_SALE_TYPE                353
#define AOFF_ADJ_SALE_PRICE           357
#define AOFF_NUM_PARCELS              367
#define AOFF_DEED_DATE                370
#define AOFF_PARTIAL_INT              376
#define AOFF_DEED_TYPE                379
#define AOFF_ROOF_COVER               385
#define AOFF_SPA                      387
#define AOFF_USECODE                  391
#define AOFF_UNIQUE_CHAR_1            397
#define AOFF_UNIQUE_CHAR_2            398
#define AOFF_UNIQUE_CHAR_3            399
#define AOFF_UNIQUE_CHAR_4            400
#define AOFF_PC_MS4_NUMBER            401
#define AOFF_RESPONSE_CODE            407
#define AOFF_PERMIT_NUMBER_1          413
#define AOFF_PERMIT_DATE_1            421
#define AOFF_PERMIT_DESC_1            427
#define AOFF_PERMIT_VALUE_1           429
#define AOFF_PERMIT_COMPLETE_1        438
#define AOFF_PERMIT_LEIN_YR_1         439
#define AOFF_PERMIT_NUMBER_2          441
#define AOFF_PERMIT_DATE_2            449
#define AOFF_PERMIT_DESC_2            455
#define AOFF_PERMIT_VALUE_2           457
#define AOFF_PERMIT_COMPLETE_2        466
#define AOFF_PERMIT_LEIN_YR_2         467
#define AOFF_PERMIT_NUMBER_3          469
#define AOFF_PERMIT_DATE_3            477
#define AOFF_PERMIT_DESC_3            483
#define AOFF_PERMIT_VALUE_3           485
#define AOFF_PERMIT_COMPLETE_3        494
#define AOFF_PERMIT_LEIN_YR_3         495
#define AOFF_SITUS_SUBD               610
#define AOFF_STAMP_AMOUNT             678
#define AOFF_STAMP_CODE               686
#define AOFF_GRANTOR                  703
#define AOFF_GRANTEE                  727

#define ASIZ_APN                      14
#define ASIZ_S_STRNUM                 7
#define ASIZ_S_STRNAME                19
#define ASIZ_FILLER1                  2
#define ASIZ_QUALITY_CLASS            5
#define ASIZ_MODEL                    6
#define ASIZ_NUM_STORIES              1
#define ASIZ_FILLER2                  4
#define ASIZ_FLOOR_TYPE               1
#define ASIZ_CENTRAL_AC_HEATING       1
#define ASIZ_CONST_YEAR               5
#define ASIZ_EFF_YEAR                 5
#define ASIZ_FILLER3                  3
#define ASIZ_ENTRY_HALL               1
#define ASIZ_DINING_ROOM              1
#define ASIZ_FILLER4                  1
#define ASIZ_FAMILY_ROOM              1
#define ASIZ_NUM_BEDROOMS             1
#define ASIZ_UTIL_ROOM                1
#define ASIZ_SUP_ROOMS                1
#define ASIZ_TOTAL_ROOMS              3
#define ASIZ_NUM_BATHS                3
#define ASIZ_FILLER5                  7
#define ASIZ_IMPRV_CONDITION_CODE     1
#define ASIZ_FILLER6                  2
#define ASIZ_BUILT_IN                 1
#define ASIZ_GARAGE_SPCS              1
#define ASIZ_FILLER7                  2
#define ASIZ_FILLER8                  3
#define ASIZ_FILLER9                  11
#define ASIZ_FILLER10                 3
#define ASIZ_FILLER11                 11
#define ASIZ_FILLER12                 26
#define ASIZ_FILLER13                 5
#define ASIZ_FILLER14                 15
#define ASIZ_YEAR_POOL_BUILT          4
#define ASIZ_FILLER15                 22
#define ASIZ_LOT_SQFT                 8
#define ASIZ_FILLER16                 8
#define ASIZ_ACRES                    6
#define ASIZ_FILLER17                 8
#define ASIZ_ZONE                     6
#define ASIZ_FILLER18                 3
#define ASIZ_EXCESS_TRAFFIC           1
#define ASIZ_BASE_LOT_PERCENT         5
#define ASIZ_FILLER19                 10
#define ASIZ_NUISANCE_FACTOR          1
#define ASIZ_FILLER20                 2
//#define ASIZ_COMMUNITY                1
//#define ASIZ_NEIGHBORHOOD_NUMBER      4
#define ASIZ_NBH_CODE                 5
#define ASIZ_FILLER21                 7
#define ASIZ_SOLAR_WATER_HEATER       1
#define ASIZ_FILLER22                 1
#define ASIZ_SOLAR_SPACE_HEATER       1
#define ASIZ_FILLER23                 1
#define ASIZ_ACTUAL_SALE_PRICE        10
#define ASIZ_RECDATE                  6
#define ASIZ_RECPAGE                  4
#define ASIZ_RELIABILITY_CODE         2
#define ASIZ_SALE_TYPE                4
#define ASIZ_ADJ_SALE_PRICE           10
#define ASIZ_NUM_PARCELS              3
#define ASIZ_DEED_DATE                6
#define ASIZ_PARTIAL_INT              3
#define ASIZ_DEED_TYPE                4
#define ASIZ_FILLER24                 2
#define ASIZ_ROOF_COVER               1
#define ASIZ_FILLER25                 1
#define ASIZ_SPA                      1
#define ASIZ_FILLER26                 3
#define ASIZ_USECODE                  2
#define ASIZ_FILLER27                 4
#define ASIZ_UNIQUE_CHAR              4
#define ASIZ_PC_MS4_NUMBER            6
#define ASIZ_RESPONSE_CODE            2
#define ASIZ_SITUS_SUBD               4
#define ASIZ_FILLER30                 64
#define ASIZ_STAMP_AMOUNT             8
#define ASIZ_STAMP_CODE               1
#define ASIZ_FILLER31                 16
#define ASIZ_GRANTOR                  24
#define ASIZ_GRANTEE                  24
#define ASIZ_AREA_SQFT                5
#define ASIZ_CODE                     5

typedef struct _tSacCharRec
{
   char  Apn[ASIZ_APN];
   char  S_StrNum[ASIZ_S_STRNUM];
   char  filler;
   char  S_StrName[ASIZ_S_STRNAME];                // 23
   char  Filler1[2];
   char  QualityClass[ASIZ_QUALITY_CLASS];         // 44
   char  Model[ASIZ_MODEL];                        // 49
   char  Num_Stories[ASIZ_NUM_STORIES];            // 55
   char  Filler2[4];
   char  Floor_Type;                               // 60
   char  Central_Ac_Heating;                       // 61
   char  YearBuilt[ASIZ_CONST_YEAR];   // BYYYY       62
   char  EffYear[ASIZ_CONST_YEAR];     // BYYYY       67
   char  Filler3[3];
   char  Entry_Hall;                               // 75
   char  Dining_Room;                              // 76
   char  Filler4[1];
   char  Family_Room;                              // 78
   char  NumBedrooms[ASIZ_NUM_BEDROOMS];           // 79
   char  Util_Room;                                // 80
   char  Sup_Rooms;                                // 81
   char  Total_Rooms[ASIZ_TOTAL_ROOMS];            // 82
   char  NumBaths[ASIZ_NUM_BATHS];                 // 85
   char  Filler5[7];
   char  Imprv_Condition_Code;                     // 95
   char  Filler6[2];
   char  Built_In;                                 // 98
   char  Garage_Spcs;                              // 99
   char  Filler7[2];
   char  Orig_1St_Flr_Area[ASIZ_AREA_SQFT];        // 102
   char  Orig_2Nd_Flr_Area[ASIZ_AREA_SQFT];        // 107
   char  Filler8[3];
   char  Conv_Garg_Area[ASIZ_AREA_SQFT];           // 115
   char  Filler9[11];
   char  Total_Add_Area[ASIZ_AREA_SQFT];           // 131
   char  Filler10[3];
   char  Total_Res_Area[ASIZ_AREA_SQFT];           // 139
   char  Finished_Basement_Area[ASIZ_AREA_SQFT];   // 144
   char  Filler11[11];
   char  SqFTGarage[ASIZ_AREA_SQFT];               // 160
   char  Filler12[26];
   char  Covered_Patio_Code[ASIZ_CODE];            // 191
   char  Filler13[5];
   char  Fireplace_Code[ASIZ_CODE];                // 201
   char  Filler14[15];
   char  Misc_Code[ASIZ_CODE];                     // 221
   char  Swimming_Pool_Code[ASIZ_CODE];            // 226
   char  Year_Pool_Built[ASIZ_YEAR_POOL_BUILT];    // 231
   char  Filler15[22];
   char  Lot_Sqft[ASIZ_LOT_SQFT];                  // 257
   char  Filler16[8];
   char  Lot_Acres[ASIZ_ACRES];                    // 273
   char  Filler17[8];
   char  Zone[ASIZ_ZONE];                          // 287
   char  Filler18[3];
   char  Excess_Traffic;
   char  Base_Lot_Percent[ASIZ_BASE_LOT_PERCENT];  // 297
   char  Filler19[10];
   char  Nuisance_Factor;
   char  Filler20[2];
   //char  Community;
   //char  Neighborhood_Number[4];
   char  Nbh_Code[ASIZ_NBH_CODE];                  // 315
   char  Filler21[7];
   char  Solar_Water_Heater;                       // 327
   char  Filler22[1];                              
   char  Solar_Space_Heater;                       // 329
   char  Filler23[1];
   char  Actual_Sale_Price[ASIZ_ADJ_SALE_PRICE];   // 331
   char  RecDate[ASIZ_RECDATE];                    // 341
   char  RecPage[ASIZ_RECPAGE];                    // 347
   char  Reliability_Code[ASIZ_RELIABILITY_CODE];  // 351
   char  Sale_Type[ASIZ_SALE_TYPE];                // 353
   char  Adj_Sale_Price[ASIZ_ADJ_SALE_PRICE];      // 357
   char  Num_Parcels[ASIZ_NUM_PARCELS];            // 367
   char  Deed_Date[ASIZ_DEED_DATE];                // 370
   char  Partial_Int[ASIZ_PARTIAL_INT];            // 376
   char  Deed_Type[ASIZ_DEED_TYPE];                // 379
   char  Filler24[2];
   char  Roof;                                     // 385
   char  Filler25[1];
   char  Spa;                                      // 387
   char  Filler26[3];
   char  Usecode[ASIZ_USECODE];                    // 391
   char  Filler27[4];
   char  Unique_Char_1[4];
   char  Pc_Ms4_Number[6];
   char  Response_Code[2];
   char  Filler28[4];
   char  Permit_Number_1[8];
   char  Permit_Date_1[6];
   char  Permit_Desc_1[2];
   char  Permit_Value_1[9];
   char  Permit_Complete_1[1];
   char  Permit_Lein_Yr_1[2];
   char  Permit_Number_2[8];
   char  Permit_Date_2[6];
   char  Permit_Desc_2[2];
   char  Permit_Value_2[9];
   char  Permit_Complete_2[1];
   char  Permit_Lein_Yr_2[2];
   char  Permit_Number_3[8];
   char  Permit_Date_3[6];
   char  Permit_Desc_3[2];
   char  Permit_Value_3[9];
   char  Permit_Complete_3[1];
   char  Permit_Lein_Yr_3[2];
   char  Filler29[113];
   char  Situs_Subd[ASIZ_SITUS_SUBD];              // 610
   char  Filler30[64];
   char  Stamp_Amount[ASIZ_STAMP_AMOUNT];          // 678
   char  Stamp_Code[ASIZ_STAMP_CODE];              // 686
   char  Filler31[16];
   char  Grantor[ASIZ_GRANTOR];                    // 703
   char  Grantee[ASIZ_GRANTOR];
} SAC_CHAR;

typedef struct _tSacLandRec
{
   char  Apn[RSIZ_APN];
   char  LotAcres[9];
   char  LotSqft[9];
} SAC_LAND;

#define LOFF_APN              1-1
#define LOFF_TRA              15-1
#define LOFF_S_STRNUM         20-1
#define LOFF_S_STRSUB         25-1
#define LOFF_S_STRNAME        28-1
#define LOFF_S_ZIP            44-1
#define LOFF_FILLER           49-1
#define LOFF_OWNER_CODE       52-1
#define LOFF_OWNER_NAME       54-1
#define LOFF_M_ADDR           104-1
#define LOFF_M_CITY           128-1
#define LOFF_M_STATE          142-1
#define LOFF_M_ZIP            144-1
#define LOFF_CO_NAME          149-1
#define LOFF_CREATION_CODE    167-1
#define LOFF_ZONING           169-1
#define LOFF_LAND_USE         175-1
#define LOFF_REC_DATE         181-1
#define LOFF_REC_PAGE         187-1
#define LOFF_DEED_TYPE        191-1
#define LOFF_LAND             195-1
#define LOFF_STRUCTURE        210-1
#define LOFF_HO_EXE           225-1
#define LOFF_FIXTURE          240-1
#define LOFF_PERS_PROP        255-1
#define LOFF_TOTAL_EXE        270-1
#define LOFF_VALUE_DATE       285-1
#define LOFF_NBH_CODE         291-1
#define LOFF_ACTION_CODE      296-1
#define LOFF_CRLF             298-1

#define LSIZ_APN              14
#define LSIZ_TRA              5
#define LSIZ_S_STRNUM         5
#define LSIZ_S_STRSUB         3
#define LSIZ_S_STRNAME        16
#define LSIZ_S_ZIP            5
#define LSIZ_FILLER           3
#define LSIZ_OWNER_CODE       2
#define LSIZ_OWNER_NAME       50
#define LSIZ_M_ADDR           24
#define LSIZ_M_CITY           14
#define LSIZ_M_STATE          2
#define LSIZ_M_ZIP            5
#define LSIZ_CO_NAME          18
#define LSIZ_CREATION_CODE    2
#define LSIZ_ZONING           6
#define LSIZ_LAND_USE         6
#define LSIZ_REC_DATE         6
#define LSIZ_REC_PAGE         4
#define LSIZ_DEED_TYPE        4
#define LSIZ_LAND             15
#define LSIZ_STRUCTURE        15
#define LSIZ_HO_EXE           15
#define LSIZ_FIXTURE          15
#define LSIZ_PERS_PROP        15
#define LSIZ_TOTAL_EXE        15
#define LSIZ_VALUE_DATE       6
#define LSIZ_NBH_CODE         5
#define LSIZ_ACTION_CODE      2
#define LSIZ_CRLF             2

typedef struct _tSacLienValueRec
{
   char  Apn[LSIZ_APN];
   char  TRA[LSIZ_TRA];
   //char  S_StreetNum[LSIZ_S_STRNUM];
   //char  S_StreetFract[LSIZ_S_STRSUB];
   //char  S_StreetName[LSIZ_S_STRNAME]; // May contain dir, name, sfx, unit#
   //char  S_Zip[LSIZ_S_ZIP];
   SAC_SADR Situs;

   char  filler[LSIZ_FILLER];
   char  OwnerCode[LSIZ_OWNER_CODE];
   char  OwnerName[LSIZ_OWNER_NAME];
   //char  M_Addr1[LSIZ_M_ADDR];
   //char  M_City[LSIZ_M_CITY];
   //char  M_St[LSIZ_M_STATE];
   //char  M_Zip[LSIZ_M_ZIP];
   SAC_MADR Mailing;
   char  CareOf[LSIZ_CO_NAME];
   char  CreationCode[LSIZ_CREATION_CODE];
   char  Zoning[LSIZ_ZONING];
   char  UseCode[LSIZ_LAND_USE];    
   char  RecDate[LSIZ_REC_DATE];
   char  RecPage[LSIZ_REC_PAGE];
   char  DeedType[LSIZ_DEED_TYPE];     // deed type or vesting
   char  Land[LSIZ_LAND];
   char  Impr[LSIZ_LAND];
   char  HO_Exe[LSIZ_LAND];
   char  Fixt_Val[LSIZ_LAND];          // Fixed machinery & equiptment exempt
   char  PP_Val[LSIZ_LAND];
   char  Total_Exe[LSIZ_LAND];
   char  ValueDate[LSIZ_VALUE_DATE];
   char  Nbh_Code[LSIZ_NBH_CODE];      // Neighborhood code
   char  ActionCode[LSIZ_ACTION_CODE];
   char  CrLf[LSIZ_CRLF];
} SAC_LVAL;

// Long legal file
#define  LLS_PARCEL_NUMBER    14
#define  LLS_S_HOUSE_NUMBER   7
#define  LLS_S_STREET_DIR     4
#define  LLS_S_STREET_NAME    25
#define  LLS_S_STREET_SFX     4
#define  LLS_S_ZIP            5
#define  LLS_S_HOUSE_SUFFIX   3
#define  LLS_S_UNIT_DES       6
#define  LLS_SUBDIVISION      7
#define  LLS_BLOCK_NUMBER     3
#define  LLS_LOT_NUMBER       5
#define  LLS_UNIT_NUMBER      5
#define  LLS_LINE_NUMBER      3
#define  LLS_LEGAL_DESC       64
#define  LLS_FILLER           20

typedef  struct _tLongLegal
{
   char APN           [LLS_PARCEL_NUMBER ];  // 1
   char S_House_Number[LLS_S_HOUSE_NUMBER];  // 15
   char S_Street_Dir  [LLS_S_STREET_DIR  ];  // 22
   char S_Street_Name [LLS_S_STREET_NAME ];  // 26
   char S_Street_Sfx  [LLS_S_STREET_SFX  ];  // 51
   char S_Zip         [LLS_S_ZIP         ];  // 55
   char S_House_Sfx   [LLS_S_HOUSE_SUFFIX];  // 60
   char S_Unit        [LLS_S_UNIT_DES    ];  // 63
   char Subdivision   [LLS_SUBDIVISION   ];  // 69
   char Block_Number  [LLS_BLOCK_NUMBER  ];  // 76
   char Lot_Number    [LLS_LOT_NUMBER    ];  // 79
   char Unit_Number   [LLS_UNIT_NUMBER   ];  // 84
   char Line_Number   [LLS_LINE_NUMBER   ];  // 89
   char LglDesc       [LLS_LEGAL_DESC    ];  // 92
   char Filler        [LLS_FILLER        ];  // 156
} SACLEGAL;

/*
USEXREF  Sac_UseTbl1[] =
{
   "A","100",             //RESIDENTIAL
   "B","200",             //COMMERCIAL
   "C","300",             //OFFICE
   "D","751",             //PERSSONAL
   "E","726",             //CHURCH
   "F","600",             //RECREATIONAL
   "G","400",             //INDUSTRIAL
   "H","500",             //AGRICULTURAL
   "I","826",             //VACANT
   "W","801",             //PUBLIC & UTIL
   "", ""
};

USEXREF  Sac_UseTbl2[] =
{
   "A1","101",            //RESID. SINGLE FAMILY
   "A2","103",            //RESID. DUPLEX/TWO UNITS
   "A3","104",            //RESID. TRIPLEX/THREE UNITS
   "A4","105",            //RESID. FOURPLEX/FOUR UNITS
   "AD","111",            //RESID. CONVERSION
   "AE","112",            //RESID. LOW RISE APARTMENTS
   "AF","113",            //RESID. HIGH RISE APARTMENTS
   "AG","106",            //RESID. COURTS
   "AH","114",            //RESID. MOBILE HOME PARKS
   "AJ","110",            //RESID. HOTELS
   "AK","116",            //RESID. BOARDING HOUSES
   "AL","117",            //RESID. ROOMING HOUSES
   "AM","118",            //RESID. SORORITY/FRATERNITY
   "AN","109",            //RESID. MOTELS
   "AP","117",            //RESID. VACANT/UN-USEABLE
   "AQ","119",            //RESID. COMMON AREA
   "AT","115",            //RESID. MOBILEHOME
   "BD","219",            //COMM. RETAIL LARGE
   "BE","226",            //COMM. SHOPPING CENTERS
   "BF","233",            //COMM. VEHICLE ORIENTED
   "BG","244",            //COMM. AUCTION YARDS
   "BH","245",            //COMM. ADVERTISING
   "BI","246",            //COMM. NURSERIES
   "BQ","247",            //COMM. COMMON AREA(CONDO_PUD)
   "CA","301",            //OFFICE GENERAL
   "CC","312",            //OFFICE - BANK
   "CD","313",            //OFFICE - SAVINGS AND LOAN
   "CE","314",            //OFFICE - RADIO / TV
   "CF","315",            //OFFICE - POST OFFICE
   "CG","316",            //OFFICE - MEDICAL/DENTAL/LABS
   "CH","317",            //OFFICE - VETERINARIAN/HOSP
   "CJ","318",            //OFFICE - RESIDENT CONVERSION
   "CQ","319",            //OFFICE - COMMON AREA
   "DA","753",            //PERS. CARE ACUTE HOSPITAL
   "DB","754",            //PERS. CARE SKILLED NURSING
   "DC","755",            //PERS. CARE RESIDENTIAL
   "DD","756",            //PERS. CARE RETIREMENT HOME
   "DE","757",            //PERS. CARE DAY NURSERIES
   "DF","758",            //PERS. CARE CEMETERY/MORTUARY
   "EF","731",            //CHURCH PRIVATE SCHOOLS
   "EK","732",            //CHURCH SOCIAL SERVICE AGENCY
   "FA","601",            //REC. GOLF COURSE
   "FB","607",            //REC. BOWLING ALLEY
   "FC","608",            //REC. SKATING RINK
   "FD","609",            //REC. RACE TRACK
   "FE","610",            //REC. MARINA
   "FF","611",            //REC. THEATRES
   "FG","614",            //REC. PRIVATE CLUBS
   "FH","623",            //REC. SPORTS-OTHER
   "GA","402",            //INDUST. LIGHT
   "GB","403",            //INDUST. HEAVY
   "GC","404",            //INDUST. DISTRIBUTION/WAREHSE
   "GD","405",            //INDUST. BUILDING MATERIALS
   "GE","406",            //INDUST. AEROSPACE
   "GF","407",            //INDUST. BUS/TRUCK TERMINALS
   "GG","408",            //INDUST. MISC FOOD PROCESSING
   "GH","418",            //INDUST. INSPECT/WEIGHING STN
   "GI","419",            //INDUST. AIRPORTS (PRIVATE)
   "GK","424",            //INDUST. RAILROAD SPUR
   "GL","425",            //INDUST. MINI-STORAGE FACILTY
   "GM","426",            //INDUST. MULTI-TENANT
   "GQ","427",            //INDUST. COMMON AREA CONDO/PUD
   "HA","501",            //AG. SPECIAL
   "HB","502",            //AG. ROW CROP
   "HC","503",            //AG. ROW CROP & FIELD CROP
   "HD","504",            //AG. ROW CROP & IRRG PASTURE
   "HE","505",            //AG. ROW CROP & DRY PASTURE
   "HF","506",            //AG. FIELD CROP
   "HG","507",            //AG. FIELD CROP & ROW CROP
   "HH","508",            //AG. FIELD CROP & IRR PASTURE
   "HI","509",            //AG. FIELD CROP & DRY PASTURE
   "HJ","510",            //AG. IRRIGATED PASTURE
   "HK","511",            //AG. IRR.PASTURE & ROW CROP
   "HL","512",            //AG. IRR.PASTURE & FIELD CROP
   "HM","513",            //AG. IRRIGATED & DRY PASTURE
   "HN","514",            //AG. DRY PASTURE
   "HO","515",            //AG. DRY PASTURE & ROW CROP
   "HP","516",            //AG. DRY PASTURE & FIELD CROP
   "HQ","517",            //AG. DRY && IRRIGATED PASTURE
   "HR","518",            //AG. TAILINGS
   "HS","519",            //AG. DRY PASTURE & TAILINGS
   "HT","520",            //AG. FIELD CROP & TAILINGS
   "HU","521",            //AG. IRRIG.PASTURE & TAILINGS
   "IA","832",            //VACANT RESIDENTIAL
   "IB","833",            //VACANT RETAIL/COMMERCIAL
   "IC","830",            //VACANT OFFICE
   "ID","828",            //VACANT HEALTH CARE
   "IF","834",            //VACANT RECREATIONAL
   "IG","829",            //VACANT INDUSTRIAL
   "IH","827",            //VACANT AGRICULTURAL
   "MA","777",            //MISC. WALKWAY
   "MB","778",            //MISC. BRIDAL PATH/HIKING TRL
   "MD","779",            //MISC. DRAINAGE DITCH
   "ME","780",            //MISC. ERODED OR WASTE LAND
   "MF","781",            //MISC. FLOOD PLAIN LAND
   "MG","781",            //MISC. IRRIGATION GATE
   "MI","783",            //MISC. MINERAL RIGHTS
   "ML","784",            //MISC. LEVEE LAND
   "MP","785",            //MISC. PARK, GREENBELT, ETC.
   "MR","786",            //MISC. PRIVATE ROAD
   "MS","787",            //MISC. SMALL OR IRREG.  SHAPED
   "MT","788",            //MISC. DREDGER TAILING
   "MU","789",            //MISC. UTILITY
   "MW","790",            //MISC. WELL, PUMP, ETC.
   "WA","803",            //PUBLIC & UTIL FEDERAL
   "WB","804",            //PUBLIC & UTIL STATE
   "WC","805",            //PUBLIC & UTIL COUNTY
   "WD","806",            //PUBLIC & UTIL CITY
   "WF","807",            //PUBLIC & UTIL SCHOOLS
   "WG","808",            //PUBLIC & UTIL SPECIAL DIST.
   "WH","809",            //PUBLIC & UTIL SBE PROPERTY
   "WI","810",            //PUBLIC & UTIL VACANT
   "WJ","811",            //PUBLIC & UTIL SWIMMING POOL
   "WL","812",            //PUBLIC & UTIL DUMP
   "WM","813",            //PUBLIC & UTIL HOUSING
   "", ""
};

USEXREF  Sac_UseTbl3[] =
{
   "A1F","107",           //RESID. CONDOMINIUM
   "A1G","108",           //RESID. PLANNED UNIT DEV
   "BAA","202",           //COMM. RETAIL SMALL 1 TENNANT
   "BAB","203",           //COMM. RETAIL SMALL 2+TENNANT
   "BAC","204",           //COMM. RETAIL CONVENIENCE STORE
   "BAX","205",           //COMM. RETAIL SMALL-CONDO
   "BAY","206",           //COMM. RETAIL SMALL-PUD
   "BBA","207",           //COMM. STORE/OFFICE 1 TENNANT
   "BBB","208",           //COMM. STORE/OFFICE 2+TENNANT
   "BBX","209",           //COMM. STORE/OFFICE CONDO
   "BBY","210",           //COMM. STORE/OFFICE PUD
   "BCA","211",           //COMM. RESTAURANT-DINING
   "BCB","212",           //COMM. RESTAURANT-BAR
   "BCC","213",           //COMM. RESTAURANT-COFFEE SHOP
   "BCD","214",           //COMM. RESTAURANT-CAFE
   "BCE","215",           //COMM. RESTAURANT-FAST FOOD
   "BCF","216",           //COMM. RESTAURANT-TAKE-OUT
   "BCX","217",           //COMM. RESTAURANT-CONDO
   "BCY","218",           //COMM. RESTAURANT-PUD
   "BDA","220",           //COMM. RETAIL LARGE-FURNITURE
   "BDB","221",           //COMM. RETAIL LARGE-MARKETS
   "BDC","222",           //COMM. RETAIL LARGE-DISCOUNTS
   "BDD","223",           //COMM. RETAIL LARGE-DEPARTMNT
   "BDX","224",           //COMM. RETAIL LARGE-CONDO
   "BDY","225",           //COMM. RETAIL LARGE-PUD
   "BEA","230",           //COMM. CONVENIENCE CENTER
   "BEB","227",           //COMM. SHOPPING CENTER-LOCAL
   "BEC","231",           //COMM. COMMUNITY
   "BED","232",           //COMM. REGIONAL
   "BEX","228",           //COMM. SHOPPING CENTERS-CONDO
   "BEY","229",           //COMM. SHOPPING CENTERS-PUD
   "BFA","234",           //COMM. SERVICE STATION
   "BFB","236",           //COMM. CAR WASH
   "BFC","237",           //COMM. AUTO REPAIR GARAGE
   "BFD","239",           //COMM. NEW CAR SALES
   "BFE","240",           //COMM. USED CAR SALES
   "BFF","238",           //COMM. BOAT SALES
   "BFG","239",           //COMM. TRAILER SALES/SERVICE
   "BFH","240",           //COMM. PARKING LOT
   "BFI","241",           //COMM. PARKING STRUCTURES
   "BFK","235",           //COMM. ABANDON SERVICE STATION
   "BFL","238",           //COMM. MINI-LUBE GARAGE
   "BFX","242",           //COMM. CONDOMINIUM
   "BFY","243",           //COMM. PUD
   "CAA","302",           //OFFICE GENERAL - ONE STORY
   "CAB","303",           //OFFICE GENERAL - TWO STORY
   "CAC","304",           //OFFICE GENERAL - MULTI STORY
   "CAX","305",           //OFFICE GENERAL - CONDO
   "CAY","306",           //OFFICE GENERAL - PUD
   "CBA","307",           //OFFICE LARGE 1 TENANT 1 STORY
   "CBB","308",           //OFFICE LARGE 1 TENANT 2 STORY
   "CBC","309",           //OFFICE LARGE 1 TENANT 2+STORY
   "CBX","310",           //OFFICE LARGE CONDO
   "CBY","311",           //OFFICE LARGE PUD
   "EEA","728",           //CHURCH - EXEMPT
   "EEB","729",           //CHURCH - PARTIAL EXEMPT
   "EEC","730",           //CHURCH - NON EXEMPT
   "FAA","602",           //REC. GOLF COURSE-PRIVATE
   "FAB","603",           //REC. GOLF COURSE-PUBLIC
   "FAC","604",           //REC. GOLF COURSE-COUNTRY CLB
   "FAD","605",           //REC. GOLF COURSE-MINIATURE
   "FAE","606",           //REC. GOLF COURSE-DRIVING RNG
   "FFA","612",           //REC. THEATRES-DRIVE IN
   "FFB","613",           //REC. THEATRES-INDOOR
   "FGA","615",           //REC. FRATERNAL
   "FGB","616",           //REC. SHOOTING CLUB (TARGETS)
   "FGD","617",           //REC. FLYING CLUB
   "FGF","618",           //REC. RIDING STABLES
   "FGG","619",           //REC. SWIMMING-TENNIS CLUB
   "FGH","620",           //REC. NUDIST CAMP
   "FGJ","621",           //REC. HANDBALL-RACQUETBALL
   "FGK","622",           //REC. HEALTH-FIGURE SPAS
   "GGA","409",           //INDUST. BAKERY
   "GGB","410",           //INDUST. CANNERY
   "GGC","411",           //INDUST. WINERY
   "GGD","412",           //INDUST. CREAMERY
   "GGE","413",           //INDUST. MEATS
   "GGF","414",           //INDUST. FROZEN FOOD
   "GGG","415",           //INDUST. PACKING PLANTS
   "GGH","416",           //INDUST. SLAUGHTER YARDS
   "GGI","417",           //INDUST. GRAIN-FEED STORAGE
   "GJA","421",           //INDUST. MINING - GRAVEL PIT
   "GJA","422",           //INDUST. MINING - CLAY PIT
   "GJB","423",           //INDUST. MINING - GAS WELL
   "", ""
};
*/

/*
SACRCITB DC   0CL10' '
         DC    CL10'2589000055'
         DC    CL10'3221000056'
         DC    CL10'3428015157'
         DC    CL10'3668054958'
         DC    CL10'3791021959'
         DC    CL10'4173959460'
         DC    CL10'4367068061'
         DC    CL10'4578000162'
         DC    CL10'4853000163'
         DC    CL10'5149000164'
         DC    CL10'9999000165'
         DC    8X'FF',C'00'
*/

// TAXJ2710-2.TXT
#define  TSIZ_APN          14
#define  TSIZ_C_SITUS      28
#define  TSIZ_NAME         50
#define  TSIZ_C_CAREOF     18
#define  TSIZ_C_M_STR      24
#define  TSIZ_C_M_CITY     13
#define  TSIZ_M_ST         2
#define  TSIZ_M_ZIP        5
#define  TSIZ_TRA          5
#define  TSIZ_CORTACNUM    11
#define  TSIZ_MAX_LEVIES   16
#define  TSIZ_DFLTYEAR     4
#define  TSIZ_POSTPONE     4
#define  TSIZ_MAX_BASE     5
#define  TSIZ_DATE         6
#define  TSIZ_ASMTNUM      9
#define  TSIZ_YEAR         2
#define  TSIZ_TYPE         2
#define  TSIZ_BILLNUM      8
#define  TSIZ_LEVYNUM      4
#define  TSIZ_LEVYCODE     3
#define  TSIZ_AMT          11

typedef  struct _tDirectLevy
{
   char           LevyNum[TSIZ_LEVYNUM];
   char           LevyCode[TSIZ_LEVYCODE];
   char           LevyAmt[TSIZ_AMT];
} DIRLEVY;

typedef  struct _tTaxRollMailing
{
   char           CareOf[TSIZ_C_CAREOF];
   char           Assessee[TSIZ_NAME];
   char           M_Street[TSIZ_C_M_STR];
   char           M_City[TSIZ_C_M_CITY];
   char           M_St[TSIZ_M_ST];
   char           M_Zip[TSIZ_M_ZIP];
} TR_MAIL;

/*  CORTAC - Committee On Reciprocal Tax Accounting in California. This is an association
    of banks, mortgage companies and tax service agencies who act as agents for persons with
    mortgage property. The members of this committee request and receive property tax bills in
    the property owner's name and pay the taxing authority out of a trust fund taken as a
    predetermined portion of each mortgage payment and held until the tax due date.
*/
typedef  struct _tCortac
{
   char           Apn[TSIZ_APN];                      // 1
   char           Situs[TSIZ_C_SITUS];                // 15
   char           Owner[TSIZ_NAME];                   // 43
   TR_MAIL        M_Addr;                             // 93
   char           TRA[TSIZ_TRA];                      // 205
   char           CortacNum[TSIZ_CORTACNUM];          // 210
   DIRLEVY        asLevies[TSIZ_MAX_LEVIES];          // 221
   char           DfltYear[TSIZ_DFLTYEAR];            // 509 - YYyy (i.e. 1516 )
   char           SrTaxPostpone[TSIZ_POSTPONE];       // 513
   char           Land[TSIZ_AMT];                     // 517
   char           Impr[TSIZ_AMT];                     // 528
   char           Fixt[TSIZ_AMT];                     // 539
   char           PPVal[TSIZ_AMT];                    // 550
   char           HOExe[TSIZ_AMT];                    // 561
   char           OthExe[TSIZ_AMT];                   // 572
   char           BaseAmt[TSIZ_MAX_BASE][TSIZ_AMT];   // 583
   char           DateOwnshp[TSIZ_DATE];              // 638
   char           AsmtNum[TSIZ_ASMTNUM];              // 644
   char           AsmtYear[TSIZ_YEAR];                // 653
   char           BillDate[TSIZ_DATE];                // 655
   char           BillType[TSIZ_TYPE];                // 661   - S(ecured) or U(nsecured)
   char           BillNum[TSIZ_BILLNUM];              // 663
   char           BillAmt[TSIZ_AMT];                  // 671
   char           NumInst;                            // 682
   char           TaxAmt1[TSIZ_AMT];                  // 683
   char           TaxAmt2[TSIZ_AMT];                  // 694
   char           Inst1_DueDate[TSIZ_DATE];           // 705
   char           Inst2_DueDate[TSIZ_DATE];           // 711
   char           PenAmt1[TSIZ_AMT];                  // 717
   char           PenAmt2[TSIZ_AMT];                  // 728
   char           PaidStatus1;                        // 739
   // Space = unpaid
   // P or &=paid
   // R=installment payment reverse
   // L, V, or Z=installment cancelled (V appears in PenStatus only)
   // L or Z=bill cancelled (all installments)
   char           PenStatus1;                         // 740
   char           PaidStatus2;                        // 741
   char           PenStatus2;                         // 742
   char           AccessType[TSIZ_TYPE];              // 743
   /*
     ASSESS TYPE - Indicates the various types of assessments:
         00   From main bill production
         01   Supplemental Change in Ownership
         02   Supplemental New Construction
         03   Tax change values
         04   Non-supplemental values (not used)
         05   Protests (not used)
         06   (Not assigned)
         07   (Not assigned)
         08   Exemption period values
         09   Parcel create values
         10   Main bill calamity
         11   Supplemental calamity
         12   Segregation
         13   CIOS penalty

     Assess types 01, 02 and 11 denote supplemental assessments. Assess type 03 and an
     origin of 0 in assessment number denotes a main roll bill (as does assess type 00).
     Corrections to the main roll bill may result in additional bills - check for cancellation of
     main roll bill to determine if correction is replacement or addition.
   */
   char           Filler[56];                         // 745
} SAC_CORTAC;
/* Notes
      ASSESSMENT NUMBER - also called "Tax Assessment Number", is the number assigned 
      to the event that triggers the creation of a tax bill. It consists of the following:

           Year             YY               2 Characters
           Origin           X                1 Character
           Assessment No. XXXXXX             6 Characters

      Year - For main bill production, the year will be the tax roll year. For tax changes, 
             the year will be the assessment year the change is for.

      Origin - Denotes the source of the record: 
           '0' - Main bill production
           '1' - Assessor-Supplemental assessment       (from AIMS)
           '2' - Assessor-Tax Change                    (online)
           '3' - Auditor-Tax Change                     (online)
           '4' - Assessor-Tax Change to Supplemental    (online)
           '5' - Assessor-Supplemental assessment       (online)
           '6' - Assessor-Tax Change                    (from AIMS)
           '7' - Assessment Appeals Supplemental Change (online)
           '8' - Failure to File CIOS Statement         (from AIMS)

      Tax Changes consist of escaped bills, corrected bills and additional bills.

      - An Escaped bill meets the following criteria: Origin of 2, 3, 6, or 7 and the 
        Assessment Year is less than the Current Roll Year.
      
      - A Corrected bill meets the following criteria: Origin of 2, 3, 6, or 7 and the 
        Assessment Year is equal to the Current Roll Year and the Main Roll Bill is cancelled.
      
      - A Secured Additional bill meets the following criteria: Origin of 2, 3, 6, or 7 and the 
        Assessment Year is equal to the Current Roll Year and the Main Roll Bill is not cancelled.
*/

// RDMJ3410-2.txt
typedef  struct _tInstPlan
{
   char           PlanType[TSIZ_TYPE];      // '05'
   char           StartDate[TSIZ_DATE];
   char           DueDate[TSIZ_DATE];
   char           Def_Date[TSIZ_DATE];
   char           PlanNumberDue;
} INST_PLAN;

typedef  struct _tActivity
{
   char           ActDate[TSIZ_DATE];
   char           ActAmt[TSIZ_AMT];
   char           ActType[TSIZ_TYPE];
   // PO = payoff payment
   // RO = payoff payment reversal
   // 5P = 5 pay payment
   // 5R = 5 pay payment reversal
   // PR = rejected payment
   // 5D = 5 pay plan default
   // 5I = 5 pay plan initiated
   // 5C = 5 pay changed
   // RD = 5 pay reactivated
   // AD = aution date delete
   // AS = aution date set
   // BD = bankruptcy delete
   // BF = bankruptcy filed
   // CO = comment
   // LA = lien srch fee added
   // LC = lien srch fee cancelled
   // LR = lien srch fee reinstated
   // RA = release of equity fee added
   // RC = release of equity fee cancelled
   // RR = release of equity fee reinstated
   // SC = state fee cancelled
   // SR = state fee reinstated
   // IC = int cancelled
   // IR = int reinstated
} ACTIVITY;

typedef  struct _tBillDfltInfo
{  // 86-bytes
   char           RollYear[TSIZ_YEAR];       // 875
   char           BillNum[TSIZ_BILLNUM];     // 877
   //char           AccessType[TSIZ_TYPE];     // 883 - 00=regular bill, 01,02,11=supplemental bill
   char           TaxAmt[TSIZ_AMT];          // 885
   char           DefPenAmt[TSIZ_AMT];       // 896
   char           CostAmt[TSIZ_AMT];         // 907
   char           NumInst;                   // 918
   char           NumDefInst;                // 919
   char           Inst1_Amt[TSIZ_AMT];       // 920
   char           Inst2_Amt[TSIZ_AMT];       // 931
   char           RdmPenAmt[TSIZ_AMT];       // 942
   char           Def_Date[TSIZ_DATE];       // 953
   char           DirLevySubjFlg;            // 959
   /*
   The Direct Levy Subject Flag value set to 'Y' indicates that a foreclosable levy was removed
   from the bill in redemption.

   If the Bill-Direct-Levy-Subject-Flag is set to 'Y', it means that the defaulted bill 
   amount is not equal to the defaulted tax amount because a direct levy, subject to
   accelerated judicial foreclosure, was removed in redemption shortly after the Secured closeout.
   */
   char           TDL_Flg;                 // 960 
   /*
     0 or Blank - Property is not subject to the Tax Collector's power to sell.
     1          - Pending TDL - The property will be subject to sell the next July 1.
     2          - Active TDL - The property is subject to the Tax Collector's power to sell.
   */
} BILLDFLT;

#define  TSIZ_R_SITUS      54
#define  TSIZ_R_M_STR      50
#define  TSIZ_R_M_CITY     20
#define  TSIZ_DFLTNUM      12
#define  TSIZ_FEE          7
#define  TSIZ_MAX_INST     5
#define  TSIZ_MAX_ACT      20
#define  TSIZ_MAX_BILLDLFT 20

typedef  struct _tRdmMailing
{
   char           CareOf[TSIZ_NAME];
   char           M_Street[TSIZ_R_M_STR];
   char           M_City[TSIZ_R_M_CITY];
   char           M_St[TSIZ_M_ST];
   char           M_Zip[TSIZ_M_ZIP];
} RDM_MAIL;

typedef  struct _tRDMJ3410
{
   char           Apn[TSIZ_APN];                      // 1
   char           Prev_Apn[TSIZ_APN];                 // 15
   char           Situs[TSIZ_R_SITUS];                // 29
   char           Owner[TSIZ_NAME];                   // 83
   RDM_MAIL       M_Addr;                             // 133
   char           Def_Owner[TSIZ_NAME];               // 260
   char           Def_Num[TSIZ_DFLTNUM];              // 310 - YYyyNNNNNNSS (AsmtYear+BillNum+AccessType)
   char           Def_Stat[TSIZ_TYPE];                // 322 - 30=open,31=cancel,32=paid,35=corrd,38=minbill
   char           Def_Date[TSIZ_DATE];                // 324
   char           TotalDue[TSIZ_AMT];                 // 330
   char           DueAmt[TSIZ_AMT];                   // 341
   char           PenAmt[TSIZ_AMT];                   // 352
   char           StateFee[TSIZ_FEE];                 // 363
   char           RelFee[TSIZ_FEE];                   // 370
   char           LienSrchFee[TSIZ_FEE];              // 377
   char           AutionDate[TSIZ_DATE];              // 384
   INST_PLAN      asInstPlan[TSIZ_MAX_INST];          // 390
   ACTIVITY       asActivity[TSIZ_MAX_ACT];           // 495
   BILLDFLT       asBillDflt[TSIZ_MAX_BILLDLFT];      // 875
} SAC_RDM;

// gisx0105.txt
#define  GIS_MAPB          0
#define  GIS_PG            1
#define  GIS_PCL           2
#define  GIS_PSUB          3
#define  GIS_CNCTRA        4
#define  GIS_CTMTRA        5
#define  GIS_NUMB          6
#define  GIS_SUB           7
#define  GIS_STREET        8
#define  GIS_ZIP           9
#define  GIS_OCODE         10
#define  GIS_OACODE        11
#define  GIS_OADT          12
#define  GIS_OWNER         13
#define  GIS_MSTREET       14
#define  GIS_MCITY         15
#define  GIS_MSTATE        16
#define  GIS_MZIP          17
#define  GIS_COWNAM        18
#define  GIS_PCNO          19
#define  GIS_AFPCNO        20
#define  GIS_LMAPB         21
#define  GIS_LPG           22
#define  GIS_LPCL          23
#define  GIS_LPSUB         24
#define  GIS_PCCODE        25
#define  GIS_ZONE          26
#define  GIS_USECD         27
#define  GIS_NGH           28
#define  GIS_RESP          29
#define  GIS_RECD          30
#define  GIS_RECP          31
#define  GIS_EDO           32
#define  GIS_RTYP          33
#define  GIS_RCODE         34
#define  GIS_SCODE         35
#define  GIS_ICD           36
#define  GIS_IRS           37
#define  GIS_INDSAPR       38
#define  GIS_SLEPR         39
#define  GIS_MULTISL       40
#define  GIS_OINT          41
#define  GIS_SLEDT         42
#define  GIS_APPACT        43
#define  GIS_PPCLASS       44
#define  GIS_OLDAPP        45
#define  GIS_OVLAND        46
#define  GIS_OVSTRU        47
#define  GIS_OVFIX         48
#define  GIS_OVPP          49
#define  GIS_OVHEX         50
#define  GIS_OVEX          51
#define  GIS_OVDTE         52
#define  GIS_OVCODE        53
#define  GIS_VD            54
#define  GIS_NPPCL         55
#define  GIS_ANM           56
#define  GIS_NVLAND        57
#define  GIS_NVSTRU        58
#define  GIS_NVHEX         59
#define  GIS_NVDTE         60
#define  GIS_NVCODE        61
#define  GIS_NVD           62
#define  GIS_BSYR          63
#define  GIS_BLAND         64
#define  GIS_BSTRU         65
#define  GIS_NVPP          66
#define  GIS_NVFIX         67
#define  GIS_NVEX          68
#define  GIS_APNBR         69

// city_full_parcel.txt
#define  PARCEL_ID                  0
#define  PARCEL_NUMBER              1
#define  P_PARCEL_LOT_SQFT          2
#define  P_PARCEL_USE_CODE          3
#define  P_PARCEL_ZONE_CODE         4
#define  P_PARCEL_NBH_CODE          5
#define  P_PARCEL_TRA               6
#define  P_PARCEL_LONGITUDE         7
#define  P_PARCEL_LATITUDE          8
#define  P_PARCEL_STATE_PLANEX      9
#define  P_PARCEL_STATE_PLANEY      10
#define  P_SUBDIVISION_ID           11
#define  P_BLOCK                    12
#define  P_LOT                      13
#define  P_UNIT                     14
#define  P_SHORT_LEGAL              15
#define  P_LONG_LEGAL               16
#define  P_CREATE_DATE              17
#define  P_OBSOLETE_DATE            18
#define  P_PC_CREATED_BY            19
#define  P_PC_DELETED_BY            20
#define  P_LOAD_DATE                21

/* city_full_situs_address.txt
#define  SITUS_ADDRESS_ID           2
#define  SITUS_STREET_NUMBER        3
#define  SITUS_PRE_DIRECTION        4
#define  SITUS_STREET_NAME          5
#define  SITUS_STREET_SUFFIX        6
#define  SITUS_POST_DIRECTION       7
#define  SITUS_SEC_UNIT_INDICATOR   8
#define  SITUS_SEC_UNIT_NUMBER      9
#define  SITUS_CITY                 10
#define  SITUS_STATE                11
#define  SITUS_ZIP_CODE             12
#define  SITUS_PRIMARY_ADDRESS_FLAG 13
#define  SITUS_BAD_ADDRESS_FLAG     14
#define  SITUS_CREATE_DATE          15
#define  SITUS_OBSOLETE_DATE        16
#define  SITUS_COLS                 17
*/
// city_full_situs_address.dos 10/25/2019
#define  SITUS_ADDRESS_ID           2
#define  SITUS_MAP_ID               3
#define  SITUS_STREET_NUMBER        4
#define  SITUS_PRE_DIRECTION        5
#define  SITUS_STREET_NAME          6
#define  SITUS_STREET_SUFFIX        7
#define  SITUS_POST_DIRECTION       8
#define  SITUS_SEC_UNIT_INDICATOR   9
#define  SITUS_SEC_UNIT_NUMBER      10
#define  SITUS_CITY                 11
#define  SITUS_STATE                12
#define  SITUS_ZIP_CODE             13
#define  SITUS_PRIMARY_ADDRESS_FLAG 14
#define  SITUS_BAD_ADDRESS_FLAG     15
#define  SITUS_CREATE_DATE          16
#define  SITUS_OBSOLETE_DATE        17
#define  SITUS_COLS                 18

// city_full_mailing_address.txt
#define  MAILING_ADDRESS_ID         2
#define  MAILING_CARE_OF_NAME       3
#define  MAILING_STREET_NUMBER      4
#define  MAILING_PRE_DIRECTION      5
#define  MAILING_STREET_NAME        6
#define  MAILING_STREET_SUFFIX      7
#define  MAILING_POST_DIRECTION     8
#define  MAILING_SEC_UNIT_INDICATOR 9
#define  MAILING_SEC_UNIT_NUMBER    10
#define  MAILING_PO_BOX_NUMBER      11
#define  MAILING_CITY               12
#define  MAILING_STATE              13
#define  MAILING_ZIP_CODE           14
#define  MAILING_ZIP4               15
#define  MAILING_FOREIGN_ADDRESS    16
#define  MAILING_BAD_ADDRESS_FLAG   17
#define  MAILING_COLS               18

// city_full_owner.txt
// OWNER_ROLE are GRANTEE/GRANTOR/INACTIVE
// Ignore OWNER_ROLE=INACTIVE
#define  O_OWNER_ID                 2
#define  O_OWNER_CODE               3
#define  O_OWNER_FIRST_NAME         4
#define  O_OWNER_MIDDLE_NAME        5
#define  O_OWNER_LAST_NAME          6
#define  O_OWNER_SUFFIX             7
#define  O_OWNER_PREFIX             8
#define  O_OWNER_SEQ                9
#define  O_OWNER_ROLE               10          // GRANTOR, GRANTEE, INACTIVE
#define  O_PRIMARY_OWNER_FLAG       11
#define  O_COLS                     12

// city_full_characteristics.txt
#define  C_PARCEL_NUMBER            0
#define  C_EFFECTIVE_YEAR           1
#define  C_YEAR_BUILT               2
#define  C_NUMBER_OF_STORIES        3
#define  C_SLAB                     4
#define  C_AIR_CONDITIONING         5
#define  C_DINING_ROOM              6
#define  C_FAMILY_ROOM              7
#define  C_NUMBER_OF_BEDROOMS       8
#define  C_UTILITY_ROOM             9
#define  C_SUPPLEMENTAL_ROOMS       10
#define  C_TOTAL_ROOMS              11
#define  C_NUMBER_OF_FULL_BATHS     12
#define  C_NUMBER_OF_HALF_BATHS     13
#define  C_CONDITION                14
#define  C_PARKING_SPACE            15
#define  C_FIRST_FLOOR_AREA         16
#define  C_OTHER_FLOOR_AREA         17
#define  C_GARAGE_CONV_AREA         18
#define  C_ADDITION_AREA            19
#define  C_FINISHED_BSMT_AREA       20
#define  C_GARAGE_TOTAL_AREA        21
#define  C_TOTAL_LIVING_AREA        22
#define  C_SPA_HOT_TUB              23
#define  C_SECOND_FLOOR_AREA        24
#define  C_FLOOR_AREA_ABOVE_2       25
#define  C_CARPORT_AREA             26
#define  C_FIREPLACE_COUNT          27
#define  C_POOL_DATE                28
#define  C_ROOF_COVER               29
#define  C_QUALITY_CLASS            30
#define  C_BUILDING_COUNT           31
#define  C_ECON_UNIT                32
#define  C_ECON_UNIT_PRCL_CNT       33
#define  C_USE_CODE                 34
#define  C_COLS                     35

// city_full_comm_char.txt
#define  CC_BUILDING_SF             2
#define  CC_YEAR_BUILT              3
#define  CC_EFFECTIVE_YEAR          4
#define  CC_NUMBER_OF_STORIES       5
#define  CC_QUALITY_CLASS           6
#define  CC_GROUND_FLOOR_GROSS      7
#define  CC_NET_RENTAL              8
#define  CC_COLS                    9

// city_full_salesmaster.txt
#define  S_DOCUMENT_DT              2
#define  S_DOCUMENT_PAGE            3
#define  S_EVENT_DT                 4
#define  S_EVENT_PAGE               5
#define  S_TRANSFER_TAX_AMOUNT      6
#define  S_TRANSFER_TAX_CODE        7
#define  S_EST_SALES_PRICE          8
#define  S_PARCEL_CNT               9
#define  S_ACTIVE_DOC_FLAG          10
#define  S_APPRAISABLE_FLAG         11
#define  S_DOCUMENT_TYPE            12
#define  S_DOCUMENT_TYPE_DESC       13
#define  S_GRANTOR                  14
#define  S_GRANTEE                  15
#define  S_COLS                     16

// city_full_equalized_roll.txt (Weekly version)
#define  EU_PARCEL_NUMBER            0
#define  EU_LAND_VALUE               1
#define  EU_IMPROVEMENT_VALUE        2
#define  EU_FIXTURE_VALUE            3
#define  EU_PERSONAL_PROP_VALUE      4
#define  EU_HOMEOWNERS_EXEMP_VALUE   5
#define  EU_OTHER_EXEMP_VALUE        6
#define  EU_VALUE_DATE               7
#define  EU_COLS                     8

// 2018_equalized_roll.txt (LDR version)
#define  EL_MAPB                     0
#define  EL_PG                       1
#define  EL_PCL                      2
#define  EL_PSUB                     3
#define  EL_TRA                      4
#define  EL_LAND                     5
#define  EL_IMPR                     6
#define  EL_FIXTURE                  7
#define  EL_PP                       8
#define  EL_HO_EXE                   9
#define  EL_TOTAL_EXE                10
#define  EL_DATE                     11
#define  EL_COLS                     12

// city_full_equalized_roll.dos
#define  EC_APN                      0
#define  EC_LAND                     1
#define  EC_IMPR                     2
#define  EC_FIXTURE                  3
#define  EC_PP                       4
#define  EC_HO_EXE                   5
#define  EC_OTH_EXE                  6
#define  EC_DATE                     7
#define  EC_COLS                     8

// city_full_roll_being_prepared.txt
#define  R_ROLL_YEAR                0
#define  R_PARCEL_ID                1
#define  R_PARCEL_NUMBER            2
#define  R_CREATED_DT               3
#define  R_OBSOLETE_DT              4
#define  R_OWNER_CODE               5
#define  R_VALUE_DT                 6
#define  R_LAND_VALUE               7
#define  R_IMPROVEMENT_VALUE        8
#define  R_PERSONAL_PROPERTY_VALUE  9
#define  R_FIXTURE_VALUE            10
#define  R_HOMEOWNERS_EXEMP_VALUE   11
#define  R_OTHER_EXEMP_VALUE        12
#define  R_REBUILD_FLAG             13
#define  R_LAST_REBUILD_DT          14
#define  R_COLS                     15

static XLAT_CODE  asCond[] =
{
   // Value, lookup code, value length
   "A", "A", 1,               // Average
   "F", "F", 1,               // Fair
   "G", "G", 1,               // Good
   "P", "P", 1,               // Poor
   "",   "",  0
};

static XLAT_CODE  asRoof[] =
{
   // Value, lookup code, value length
   "SHAKE",       "B", 2,               // Wood Shake
   "CO",          "C", 2,               // Composition
   "WOOD SH",     "A", 6,               // Wood Shingle
   "TILE",        "I", 2,               // Tile
   "U",           "Z", 1,               // Unknown
   "M",           "L", 1,               // Metal
   "TAR GRAVEL",  "F", 2,               // Tar Gravel
   "SLATE",       "G", 2,               // Slate
   "",   "",  0
};

IDX_TBL5 SAC_DocCode[] =
{  // DocType, Index, Non-sale, len1, len2
   "GD",   "1 ", 'N', 2, 2,
   "HWJT", "1 ", 'N', 3, 2,
   "RTR ", "13", 'Y', 3, 2, // Include GD & QC
   "TDSL", "27", 'N', 3, 2, // Trustee's Deed Up On Sale
   "QC",   "4 ", 'Y', 2, 2,
   "JTGD", "1 ", 'N', 3, 2,
   "TD  ", "27", 'N', 3, 2, // Trustee's Deed
   "SU  ", "19", 'Y', 3, 2, // Other
   "DE  ", "13", 'N', 3, 2,
   "DEED", "13", 'N', 3, 2,
   "HWCP", "1 ", 'N', 3, 2, // Include GD
   "PR58", "19", 'Y', 3, 2,
   "HWGD", "1 ", 'N', 3, 2, // Include GD
   "PI58", "19", 'Y', 3, 2,
   "JTOT", "13", 'Y', 3, 2, // Include GD & QC
   "DETH", "19", 'Y', 3, 2, // Include Joint Tenancy & Affidavit of Death; not many searchable
   "NC  ", "19", 'Y', 3, 2,
   "TCGD", "1 ", 'N', 3, 2,
   "DD  ", "74", 'Y', 3, 2, // Include Settling Final Acct/Jdgt Final Acct/Order Final Acct/Order - Could be 60?
   "TR  ", "13", 'Y', 3, 2, // Include GD & QC
   "MHOM", "13", 'N', 3, 2, // Mobile home transfer
   "SI  ", "19", 'Y', 3, 2, // No search results
   "JT  ", "1 ", 'N', 3, 2,
   "AFDE", "6 ", 'Y', 3, 2, // Affidavit of Death; not many searchable
   "RF58", "13", 'Y', 3, 2, // Include GD & QC
   "RTOT", "13", 'Y', 3, 2, // Include Deed of Trust & QC
   "CO  ", "80", 'Y', 3, 2, // Include Affidavit & Court Order
   "RP58", "1 ", 'Y', 3, 2,
   "CTRL", "74", 'Y', 3, 2, // No search results
   "DI  ", "13", 'Y', 3, 2,
   "TXD ", "67", 'N', 3, 2, // Include Tax Deed & Notice of Sale
   "AS  ", "8 ", 'N', 3, 2, // Agreement to Sale
   "AFJT", "3 ", 'Y', 3, 2, // Term Joint Tenancy
   "TC  ", "1 ", 'N', 3, 2,
   "ORDR", "80", 'Y', 3, 2,
   "INT ", "13", 'N', 3, 2, // Include GD & QC
   "LE  ", "13", 'Y', 3, 2, // Include GD & QC
   "AFDT", "6 ", 'Y', 3, 2, // Affidavit
   "GF  ", "16", 'Y', 3, 2, // Gift Deed
   "RDMD", "74", 'Y', 3, 2, // Collectrs CTF Canc
   "CPWR", "1 ", 'N', 3, 2, // Include GD
   "HWTC", "1 ", 'N', 3, 2, // Include GD
   "WD  ", "28", 'N', 3, 2, // WARRANTY DEED
   "UC  ", "74", 'Y', 3, 2, // No search results
   "EXD ", "15", 'N', 3, 2, // Executor's Deed
   "ORDA", "74", 'Y', 3, 2, // Settling Final Acct - Could be 60?
   "ORDF", "74", 'Y', 3, 2, // Order Final Acct - Could be 60 or 80?
   "CD  ", "19", 'Y', 3, 2, // Include Release Agmt & Conservator Deed
   "STR ", "13", 'N', 3, 2, // Include GD, QC & Order
   "TRST", "13", 'N', 3, 2, // Include GD & QC
   "PAD ", "67", 'N', 3, 2, // Tax Deed
   "CS  ", "11", 'N', 3, 2, // Include Contract of Sale/Deed of Trust/Request for Notice/Mod Agmt
   "ADMD", "5 ", 'N', 3, 2, // Administrator's Deed
   "ASGN", "7 ", 'N', 3, 2, // Assignment Deed
   "LEAS", "44", 'Y', 3, 2, // Lease Agreement
   "MEMS", "8 ", 'N', 3, 2, // Include Agreement to Sell & Assignment Deed - Could be 7 or 8?
   "HWQC", "4 ", 'Y', 3, 2,
   "CP  ", "13", 'N', 3, 2, // Mobile home transfer
   "UCS ", "11", 'Y', 3, 2, // Contract of Sale
   "JTQC", "4 ", 'Y', 3, 2,
   "JDGF", "60", 'Y', 3, 2, // Judgment Final Acct - Could be 60?
   "SD  ", "25", 'N', 3, 2, // Sheriff Deed
   "DOT ", "13", 'Y', 3, 2, // Include Term Joint Tenancy & GD
   "QT  ", "4 ", 'Y', 3, 2,
   "JTDE", "3 ", 'Y', 3, 2, // Include Joint Tenancy Deed, GD & Deed - Could be 3?
   "DCLR", "12", 'Y', 3, 2, // Declaration
   "TCQC", "4 ", 'Y', 3, 2,
   "HWAS", "8 ", 'N', 3, 2, // Agreement to sale
   "CONS", "13", 'N', 3, 2, // Include Conservator's Deed & GD
   "DOLT", "74", 'Y', 3, 2, // Include Term Life Estate & Term Joint Tenancy - Could be 3?
   "JDOR", "80", 'Y', 3, 2, // Judgment & Order - Could be 60 or 80?
   "AFTL", "74", 'Y', 3, 2, // Term Life Estate
   "DC  ", "74", 'Y', 3, 2, // Include Final Condemnation & Deed
   "RLSE", "74", 'Y', 3, 2, // Release
   "CPGD", "1 ", 'N', 3, 2,
   "AMOR", "74", 'Y', 3, 2, // Amended Order - Could be 80?
   "HWDE", "1 ", 'N', 3, 2,
   "MISC", "74", 'Y', 3, 2,
   "DDLE", "60", 'Y', 3, 2, // Include Order Final Acct & Settling Final Acct - Could be 60?
   "DETR", "12", 'Y', 3, 2, // Declaration of Trust
   "CERT", "62", 'Y', 3, 2, // Certificate
   "AGRE", "8 ", 'Y', 3, 2, // Agreement of Sale
   "RTLT", "13", 'Y', 3, 2, // Include QC, GD & Revoc Deed
   "ORDS", "80", 'Y', 3, 2,
   "5PCT", "13", 'Y', 3, 2, // Include QC & GD
   "CTXD", "74", 'Y', 3, 2, // Collectrs CTF Canc
   "ASGL", "44", 'Y', 3, 2, // Include Assignment of Lease & Assumption Deed - Could be 29?
   "MAS ", "19", 'Y', 3, 2, // Include Mod Agreement & Notice
   "UNRC", "76", 'Y', 3, 2, // No search results; only 19 in SQL - probably Unrecorded?
   "TX  ", "67", 'N', 3, 2, // Tax Deed
   "ORCO", "74", 'Y', 3, 2, // Final Condemnation - Could be 60 or 80?
   "GR58", "13", 'Y', 3, 2, // Include QC & GD
   "GFTD", "74", 'Y', 3, 2,
   "MRGR", "74", 'Y', 3, 2, // Include Merger & GD
   "REVD", "74", 'Y', 3, 2, // Revocable Deed
   "NOCN", "74", 'Y', 3, 2, // Cancellation
   "DEQT", "74", 'Y', 3, 2, // Decree QuitG Title
   "COND", "13", 'N', 3, 2, // Conservator's Deed
   "CANC", "74", 'Y', 3, 2, // Include Rescission & Release
   "AC  ", "44", 'Y', 3, 2, // Include Assignment of Lease & Mod Agreement
   "CTD ", "74", 'Y', 3, 2, // Include QD, Cancelled Default & Rescission
   "SULE", "74", 'Y', 3, 2, // Include Term Joint Tenancy & Settling Final Acct
   "WHJT", "1 ", 'N', 3, 2,
   "","",0,0,0
};

// 2019 LDR file 2019_secured_roll.txt
#define  SAC_L_MAPB             0
#define  SAC_L_PG               1
#define  SAC_L_PCL              2
#define  SAC_L_PSUB             3
#define  SAC_L_TAX_RATE_AREA    4
#define  SAC_L_SITUS_NUMBER     5
#define  SAC_L_SITUS_CITY       6
#define  SAC_L_SITUS_STREET     7
#define  SAC_L_SITUS_ZIP        8
#define  SAC_L_OWNER_CODE       9
#define  SAC_L_OWNER            10
#define  SAC_L_MAIL_ADDRESS     11
#define  SAC_L_MAIL_CITY        12
#define  SAC_L_MAIL_STATE       13
#define  SAC_L_MAIL_ZIP         14
#define  SAC_L_CARE_OF          15
#define  SAC_L_ZONING           16
#define  SAC_L_LAND_USE_CODE    17
#define  SAC_L_RECORDING_DATE   18
#define  SAC_L_RECORDING_PAGE   19
#define  SAC_L_DEED_TYPE        20
#define  SAC_L_LAND             21
#define  SAC_L_IMPR             22
#define  SAC_L_FIXTURE          23
#define  SAC_L_PP               24
#define  SAC_L_HO_EX            25
#define  SAC_L_OTHER_EXE        26
#define  SAC_L_VALUE_DT         27
#define  SAC_L_NGH              28
#define  SAC_L_ACTION_CODE      29
#define  SAC_L_FLDS             30

// Standard codes
// H=Homeowners
// D=Disabled Veterans
// V=Veterans
// C=Church
// L=Library
// M=Museums
// R=Religion
// T=Tribal Housing
// W=Welfare
// U=College
// P=Public School
// E=Cemetery
// G=Government
// I=Hospital
// S=School
// X=Misc exempt
// Y=Military

IDX_TBL4 SAC_Exemption[] = 
{
   "00","H", 2,1,
   "02","D", 2,1,
   "03","C", 2,1,
   "04","P", 2,1,    // MUSEUM/PUBLIC SCHOOL/LIBRARY/VET ORG
   "05","I", 2,1,
   "06","W", 2,1,
   "07","R", 2,1,
   "08","S", 2,1,
   "09","S", 2,1,
   "10","G", 2,1,    // GOV'T ORG - PUBLICLY OWNED, NON-TAXABLE
   "11","G", 2,1,    // UTILITY OWNED - STATE ASSESSED (SBE)
   "12","X", 2,1,    // LEASED BY UTILITY (100%) - STATE ASSESSED (SBE)
   "13","E", 2,1,    // CEMETERY - GOV'T OWNED, NON-TAXABLE
   "14","X", 2,1,    // COMMON AREA
   "16","X", 2,1,    // MOBILE HOMES - LOW VALUE
   "20","X", 2,1,    // SENIOR CITIZENS TAX POSTPONEMENT
   "21","H", 2,1,    // 
   "22","X", 2,1,    // SECURED POSSESSORY INTEREST
   "24","E", 2,1,    // CEMETERY - NON-PROFIT
   "25","E", 2,1,    // CEMETERY - FOR-PROFIT
   "","",0,0
};

#endif