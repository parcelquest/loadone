#ifndef  _MERGECCX_H_
#define  _MERGECCX_H_

// CCX LDR definition since 2010 - 551-bytes ASCII+CRLF
#define OFF_CCX_PARCEL_NO               1
#define OFF_CCX_TRA                     11
#define OFF_CCX_OWNERSHIP_CODE          16
#define OFF_CCX_PRIME_OWNER_NAME        17
#define OFF_CCX_OWNERSHIP_PERCENT       47
#define OFF_CCX_SECOND_OWNER_ID         52
#define OFF_CCX_SECOND_OWNER_NAME       53
#define OFF_CCX_SFT_FLAG                83
#define OFF_CCX_SENIOR_POST_FLAG        84
#define OFF_CCX_RETIRED_FLAG            85
#define OFF_CCX_LIEN_YEAR               86
#define OFF_CCX_TRA_CHANGE_YEAR         90
#define OFF_CCX_NOTIF_STREET_NAME       94
#define OFF_CCX_NOTIF_STREET_SUFFIX     124
#define OFF_CCX_NOTIF_ODD_EVEN          128
#define OFF_CCX_NOTIF_STREET_NBR        129
#define OFF_CCX_NOTIF_FRACTION          134
#define OFF_CCX_NOTIF_APT_NBR           138
#define OFF_CCX_NOTIF_CITY_STATE        147
#define OFF_CCX_NOTIF_ZIP               177
#define OFF_CCX_DESCRIPTION             182
#define OFF_CCX_ACREAGE                 227
#define OFF_CCX_SITUS_STREET_NAME       233
#define OFF_CCX_SITUS_STREET_SUFFIX     263
#define OFF_CCX_SITUS_ODD_EVEN          267
#define OFF_CCX_SITUS_STREET_NBR        268
#define OFF_CCX_SITUS_FRACTION          273
#define OFF_CCX_SITUS_APT_NBR           277
#define OFF_CCX_SITUS_CITY_ABBR         286
#define OFF_CCX_ZONING                  291
#define OFF_CCX_ZONING_ATLAS            303
#define OFF_CCX_CENSUS_TRACT            307
#define OFF_CCX_TAX_CODE_A              313
#define OFF_CCX_TAX_CODE_B              314
#define OFF_CCX_REJECT_CODE             315
#define OFF_CCX_CURRENT_DEED_NBR        316
#define OFF_CCX_CURRENT_DEED_DATE       324
#define OFF_CCX_PRIOR_DEED_NBR          332
#define OFF_CCX_PRIOR_DEED_DATE         340
#define OFF_CCX_PRIOR_DEED_NBR_2        348
#define OFF_CCX_PRIOR_DEED_DATE_2       356
#define OFF_CCX_SALE_PRICE              364
#define OFF_CCX_FROM_PARCEL             373
#define OFF_CCX_TO_PARCEL               383
#define OFF_CCX_TRAFFIC_FEE_AREA        393
#define OFF_CCX_DWELL_PER_PCL           395
#define OFF_CCX_USE_CODE                398
#define OFF_CCX_RESP_CODE               402
#define OFF_CCX_AREA_CODE               406
#define OFF_CCX_HO_CODE                 410
#define OFF_CCX_LAND_VALUE              411
#define OFF_CCX_IMPROVEMENT_VALUE       420
#define OFF_CCX_PERSONAL_PROPERTY       429
#define OFF_CCX_INVALID_TRA             438
#define OFF_CCX_EXEMPTION_CODE_1        439
#define OFF_CCX_EXEMPTION_NBR_1         441
#define OFF_CCX_EXEMPTION_AMOUNT_1      444
#define OFF_CCX_EXEMPTION_CODE_2        453
#define OFF_CCX_EXEMPTION_NBR_2         455
#define OFF_CCX_EXEMPTION_AMOUNT_2      458
#define OFF_CCX_EXEMPTION_CODE_3        467
#define OFF_CCX_EXEMPTION_NBR_3         469
#define OFF_CCX_EXEMPTION_AMOUNT_3      472
#define OFF_CCX_AREA_SWEEP_DATE         481
#define OFF_CCX_FILLER1                 489
#define OFF_CCX_MAIL_ZIP4               494
#define OFF_CCX_SITUS_ZIP               498
#define OFF_CCX_SITUS_ZIP4              503
#define OFF_CCX_CURRENT_YEAR_ACTION     507
#define OFF_CCX_PARCEL_TYPE             508
#define OFF_CCX_APPRAISER_NUMBER        509
#define OFF_CCX_VRC                     514
#define OFF_CCX_PSI_VALUE               518
#define OFF_CCX_PERS_PROP_PENALTY       527
#define OFF_CCX_PSI_PENALTY             536
#define OFF_CCX_AUTO_GRANT_FLAG         545
#define OFF_CCX_AUTO_GRANT_YEAR         546
#define OFF_CCX_FILLER2                 550

#define SIZ_CCX_PARCEL_NO               10
#define SIZ_CCX_TRA                     5
#define SIZ_CCX_OWNERSHIP_CODE          1
#define SIZ_CCX_PRIME_OWNER_NAME        30
#define SIZ_CCX_OWNERSHIP_PERCENT       5
#define SIZ_CCX_SECOND_OWNER_ID         1
#define SIZ_CCX_SECOND_OWNER_NAME       30
#define SIZ_CCX_SFT_FLAG                1
#define SIZ_CCX_SENIOR_POST_FLAG        1
#define SIZ_CCX_RETIRED_FLAG            1
#define SIZ_CCX_LIEN_YEAR               4
#define SIZ_CCX_TRA_CHANGE_YEAR         4
#define SIZ_CCX_NOTIF_STREET_NAME       30
#define SIZ_CCX_NOTIF_STREET_SUFFIX     4
#define SIZ_CCX_NOTIF_ODD_EVEN          1
#define SIZ_CCX_NOTIF_STREET_NBR        5
#define SIZ_CCX_NOTIF_FRACTION          4
#define SIZ_CCX_NOTIF_APT_NBR           9
#define SIZ_CCX_NOTIF_CITY_STATE        30
#define SIZ_CCX_NOTIF_ZIP               5
#define SIZ_CCX_DESCRIPTION             45
#define SIZ_CCX_ACREAGE                 6
#define SIZ_CCX_SITUS_STREET_NAME       30
#define SIZ_CCX_SITUS_STREET_SUFFIX     4
#define SIZ_CCX_SITUS_ODD_EVEN          1
#define SIZ_CCX_SITUS_STREET_NBR        5
#define SIZ_CCX_SITUS_FRACTION          4
#define SIZ_CCX_SITUS_APT_NBR           9
#define SIZ_CCX_SITUS_CITY_ABBR         5
#define SIZ_CCX_ZONING                  12
#define SIZ_CCX_ZONING_ATLAS            4
#define SIZ_CCX_CENSUS_TRACT            6
#define SIZ_CCX_TAX_CODE_A              1
#define SIZ_CCX_TAX_CODE_B              1
#define SIZ_CCX_REJECT_CODE             1
#define SIZ_CCX_CURRENT_DEED_NBR        8
#define SIZ_CCX_CURRENT_DEED_DATE       8
#define SIZ_CCX_PRIOR_DEED_NBR          8
#define SIZ_CCX_PRIOR_DEED_DATE         8
#define SIZ_CCX_PRIOR_DEED_NBR_2        8
#define SIZ_CCX_PRIOR_DEED_DATE_2       8
#define SIZ_CCX_SALE_PRICE              9
#define SIZ_CCX_FROM_PARCEL             10
#define SIZ_CCX_TO_PARCEL               10
#define SIZ_CCX_TRAFFIC_FEE_AREA        2
#define SIZ_CCX_DWELL_PER_PCL           3
#define SIZ_CCX_USE_CODE                4
#define SIZ_CCX_RESP_CODE               4
#define SIZ_CCX_AREA_CODE               4
#define SIZ_CCX_HO_CODE                 1
#define SIZ_CCX_LAND_VALUE              9
#define SIZ_CCX_IMPROVEMENT_VALUE       9
#define SIZ_CCX_PERSONAL_PROPERTY       9
#define SIZ_CCX_INVALID_TRA             1
#define SIZ_CCX_EXEMPTION_CODE_1        2
#define SIZ_CCX_EXEMPTION_NBR_1         3
#define SIZ_CCX_EXEMPTION_AMOUNT_1      9
#define SIZ_CCX_EXEMPTION_CODE_2        2
#define SIZ_CCX_EXEMPTION_NBR_2         3
#define SIZ_CCX_EXEMPTION_AMOUNT_2      9
#define SIZ_CCX_EXEMPTION_CODE_3        2
#define SIZ_CCX_EXEMPTION_NBR_3         3
#define SIZ_CCX_EXEMPTION_AMOUNT_3      9
#define SIZ_CCX_AREA_SWEEP_DATE         8
#define SIZ_CCX_FILLER1                 5
#define SIZ_CCX_MAIL_ZIP4               4
#define SIZ_CCX_SITUS_ZIP               5
#define SIZ_CCX_SITUS_ZIP4              4
#define SIZ_CCX_CURRENT_YEAR_ACTION     1
#define SIZ_CCX_PARCEL_TYPE             1
#define SIZ_CCX_APPRAISER_NUMBER        5
#define SIZ_CCX_VRC                     4
#define SIZ_CCX_PSI_VALUE               9
#define SIZ_CCX_PERS_PROP_PENALTY       9
#define SIZ_CCX_PSI_PENALTY             9
#define SIZ_CCX_AUTO_GRANT_FLAG         1
#define SIZ_CCX_AUTO_GRANT_YEAR         4
#define SIZ_CCX_FILLER2                 2

typedef struct _tCcxMAdr
{
   char  M_StrName[SIZ_CCX_NOTIF_STREET_NAME];
   char  M_StrSfx[SIZ_CCX_NOTIF_STREET_SUFFIX];
   char  M_OddEven[SIZ_CCX_NOTIF_ODD_EVEN];     // "O" OR "E"
   char  M_StrNum[SIZ_CCX_NOTIF_STREET_NBR];               
   char  M_StrFra[SIZ_CCX_NOTIF_FRACTION];      // MAY ALSO BE "TO" STREET NUMBER
                                                // MAY ALSO BE NUMBERED STREET NAME (I.E. 3RD) IN WHICH
                                                // CASE THE STREET TYPE (I.E. ST) IS IN STRT NAME
   char  M_Unit[SIZ_CCX_NOTIF_APT_NBR];
   char  M_CitySt[SIZ_CCX_NOTIF_CITY_STATE];
   char  M_Zip[SIZ_CCX_NOTIF_ZIP];
} CCX_MADR;

typedef struct _tCcxSAdr
{
   char  S_StrName[SIZ_CCX_SITUS_STREET_NAME];
   char  S_StrSfx[SIZ_CCX_SITUS_STREET_SUFFIX];
   char  S_OddEven[SIZ_CCX_SITUS_ODD_EVEN];
   char  S_StrNum[SIZ_CCX_SITUS_STREET_NBR];
   char  S_StrFra[SIZ_CCX_SITUS_FRACTION];
   char  S_Unit[SIZ_CCX_SITUS_APT_NBR];
   char  S_CityAbbr[SIZ_CCX_SITUS_CITY_ABBR];
} CCX_SADR;

typedef struct _tCcxLien
{
   char  Apn[SIZ_CCX_PARCEL_NO];                      // 1
   char  TRA[SIZ_CCX_TRA];                            // 11
   char  OwnerCode1[SIZ_CCX_OWNERSHIP_CODE];          // 16
   char  Name1[SIZ_CCX_PRIME_OWNER_NAME];             // 17
   char  Ownership_Pct[SIZ_CCX_OWNERSHIP_PERCENT];    // 47
   char  OwnerCode2[SIZ_CCX_SECOND_OWNER_ID];         // 52
   char  Name2[SIZ_CCX_SECOND_OWNER_NAME];            // 53
   char  Sft_Flag[SIZ_CCX_SFT_FLAG];                  // 83
   char  Senior_Flag[SIZ_CCX_SENIOR_POST_FLAG];       // 84
   char  Retired_Flag[SIZ_CCX_RETIRED_FLAG];          // 85
   char  LienYr[SIZ_CCX_LIEN_YEAR];                   // 86
   char  Tra_ChgYr[SIZ_CCX_TRA_CHANGE_YEAR];          // 90
   //char  M_StrName[SIZ_CCX_NOTIF_STREET_NAME];
   //char  M_StrSfx[SIZ_CCX_NOTIF_STREET_SUFFIX];
   //char  M_OddEven[SIZ_CCX_NOTIF_ODD_EVEN];
   //char  M_StrNum[SIZ_CCX_NOTIF_STREET_NBR];
   //char  M_StrFra[SIZ_CCX_NOTIF_FRACTION];
   //char  M_Unit[SIZ_CCX_NOTIF_APT_NBR];
   //char  M_CitySt[SIZ_CCX_NOTIF_CITY_STATE];
   //char  M_Zip[SIZ_CCX_NOTIF_ZIP];
   CCX_MADR M_Adr;                                    // 94

   char  Description[SIZ_CCX_DESCRIPTION];            // 182
   char  Acreage[SIZ_CCX_ACREAGE];                    // 227
   //char  S_StrName[SIZ_CCX_SITUS_STREET_NAME];
   //char  S_StrSfx[SIZ_CCX_SITUS_STREET_SUFFIX];
   //char  S_OddEven[SIZ_CCX_SITUS_ODD_EVEN];
   //char  S_StrNum[SIZ_CCX_SITUS_STREET_NBR];
   //char  S_StrFra[SIZ_CCX_SITUS_FRACTION];
   //char  S_Unit[SIZ_CCX_SITUS_APT_NBR];
   //char  S_CityAbbr[SIZ_CCX_SITUS_CITY_ABBR];
   CCX_SADR S_Adr;                                    // 233
   char  Zoning[SIZ_CCX_ZONING];
   char  Zoning_Atlas[SIZ_CCX_ZONING_ATLAS];
   char  CensusTract[SIZ_CCX_CENSUS_TRACT];           // 307
   char  Tax_CodeA;                                   // 313
   char  Tax_CodeB;                                   // 314
   char  RejectCode[SIZ_CCX_REJECT_CODE];             // 315
   char  CurDeedNum[SIZ_CCX_CURRENT_DEED_NBR];        // 316 5-3
   char  CurDeedDate[SIZ_CCX_CURRENT_DEED_DATE];      // 324 YYYYMMDD
   char  PriDeedNum[SIZ_CCX_CURRENT_DEED_NBR];        // 332
   char  PriDeedDate[SIZ_CCX_CURRENT_DEED_DATE];      // 340
   char  PriDeedNum2[SIZ_CCX_CURRENT_DEED_NBR];       // 348
   char  PriDeedDate2[SIZ_CCX_CURRENT_DEED_DATE];     // 356
   char  SalePrice[SIZ_CCX_SALE_PRICE];               // 364 Sale price from stamps
   char  From_Parcel[SIZ_CCX_FROM_PARCEL];            // 373
   char  To_Parcel[SIZ_CCX_TO_PARCEL];                // 383
   char  Trafic_Fee_Area[SIZ_CCX_TRAFFIC_FEE_AREA];   // 393
   char  Dwell_Per_Parcel[SIZ_CCX_DWELL_PER_PCL];     // 395
   char  UseCode[SIZ_CCX_USE_CODE];                   // 398
   char  RespCode[SIZ_CCX_RESP_CODE];                 // 402
   char  AreaCode[SIZ_CCX_AREA_CODE];                 // 406
   char  HO_Code[SIZ_CCX_HO_CODE];                    // 410
   char  Land[SIZ_CCX_LAND_VALUE];                    // 411
   char  Impr[SIZ_CCX_IMPROVEMENT_VALUE];             // 420
   char  PP_Val[SIZ_CCX_PERSONAL_PROPERTY];           // 429
   char  Invalid_TRA;                                 // 438
   char  Exe_Code1[SIZ_CCX_EXEMPTION_CODE_1];         // 439
   char  Exe_Num1[SIZ_CCX_EXEMPTION_NBR_1];           // 441
   char  Exe_Amt1[SIZ_CCX_EXEMPTION_AMOUNT_1];        // 444
   char  Exe_Code2[SIZ_CCX_EXEMPTION_CODE_1];         // 453
   char  Exe_Num2[SIZ_CCX_EXEMPTION_NBR_1];           // 455
   char  Exe_Amt2[SIZ_CCX_EXEMPTION_AMOUNT_1];        // 458
   char  Exe_Code3[SIZ_CCX_EXEMPTION_CODE_1];         // 467
   char  Exe_Num3[SIZ_CCX_EXEMPTION_NBR_1];           // 469
   char  Exe_Amt3[SIZ_CCX_EXEMPTION_AMOUNT_1];        // 472
   char  Area_Sweep_Date[SIZ_CCX_AREA_SWEEP_DATE];    // 481
   char  Filler1[SIZ_CCX_FILLER1];                    // 489
   char  M_Zip4[SIZ_CCX_MAIL_ZIP4];                   // 494
   char  S_Zip[SIZ_CCX_SITUS_ZIP];                    // 498
   char  S_Zip4[SIZ_CCX_SITUS_ZIP4];                  // 503
   char  CurYearAction[SIZ_CCX_CURRENT_YEAR_ACTION];  // 507
   char  Parcel_type[SIZ_CCX_PARCEL_TYPE];            // 508
   char  AppraiserNo[SIZ_CCX_APPRAISER_NUMBER];       // 509
   char  ValueResonCode[SIZ_CCX_VRC];                 // 514
   char  Psi_Val[SIZ_CCX_PSI_VALUE];                  // 518
   char  PP_Pen[SIZ_CCX_PERS_PROP_PENALTY];           // 527
   char  Psi_Pen[SIZ_CCX_PSI_PENALTY];                // 536
   char  Auto_Flag[SIZ_CCX_AUTO_GRANT_FLAG];          // 545
   char  Auto_GrantYr[SIZ_CCX_AUTO_GRANT_YEAR];       // 546
   char  Filler2[SIZ_CCX_FILLER2];                    // 550
   char  CrLf[2];
} CCX_LIEN;

// 600-byte ebcdic record ==> 647-byte ascii
#define ROFF_APN                     1-1
#define ROFF_TRA                     11-1
#define ROFF_OWNERSHIP_CODE          16-1
#define ROFF_PRIME_OWNER_NAME        17-1
#define ROFF_OWNERSHIP_PERCENT       47-1
#define ROFF_SECOND_OWNER_ID         52-1
#define ROFF_SECOND_OWNER_NAME       53-1
#define ROFF_SFT_FLAG                83-1
#define ROFF_SENIOR_POST_FLAG        84-1
#define ROFF_RETIRED_FLAG            85-1
#define ROFF_LIEN_YEAR               86-1
#define ROFF_TRA_CHANGE_YEAR         88-1
#define ROFF_M_STRNAME               90-1
#define ROFF_M_STRSFX                120-1
#define ROFF_M_ODD_EVEN              124-1
#define ROFF_M_STRNUM                125-1
#define ROFF_M_FRACTION              130-1
#define ROFF_M_APT_NBR               134-1
#define ROFF_M_CITYST                143-1
#define ROFF_M_ZIP                   173-1
#define ROFF_DESCRIPTION             178-1
#define ROFF_ACREAGE                 223-1
#define ROFF_S_STRNAME               229-1
#define ROFF_S_STRSFX                259-1
#define ROFF_S_ODD_EVEN              263-1
#define ROFF_S_STRNUM                264-1
#define ROFF_S_FRACTION              269-1
#define ROFF_S_APT_NBR               273-1
#define ROFF_S_CITYABBR              282-1
#define ROFF_ZONING                  287-1
#define ROFF_ZONING_ATLAS            299-1
#define ROFF_CENSUS_TRACT            303-1
#define ROFF_TAX_CODE_A              307-1
#define ROFF_TAX_CODE_B              308-1
#define ROFF_REJECT_CODE             309-1
#define ROFF_CURRENT_DEED_NBR        310-1
#define ROFF_CURRENT_DEED_DATE       318-1
#define ROFF_PRIOR_DEED_NBR          324-1
#define ROFF_PRIOR_DEED_DATE         332-1
#define ROFF_PRIOR_DEED_NBR_2        338-1
#define ROFF_PRIOR_DEED_DATE_2       346-1
#define ROFF_SALE_PRICE              352-1
#define ROFF_FROM_PARCEL             361-1
#define ROFF_TO_PARCEL               371-1
#define ROFF_TRAFFIC_FEE_AREA        381-1
#define ROFF_DWELL_PER_PCL           383-1
#define ROFF_USE_CODE                385-1
#define ROFF_RESP_CODE               389-1
#define ROFF_AREA_CODE               393-1
#define ROFF_HO_CODE                 397-1
#define ROFF_LAND_VALUE              398-1
#define ROFF_IMPROVEMENT_VALUE       407-1
#define ROFF_PERSONAL_PROPERTY       416-1
#define ROFF_INVALID_TRA             425-1
#define ROFF_EXEMPTION_CODE_1        426-1
#define ROFF_EXEMPTION_NBR_1         428-1
#define ROFF_EXEMPTION_AMOUNT_1      431-1
#define ROFF_EXEMPTION_CODE_2        440-1
#define ROFF_EXEMPTION_NBR_2         442-1
#define ROFF_EXEMPTION_AMOUNT_2      445-1
#define ROFF_EXEMPTION_CODE_3        454-1
#define ROFF_EXEMPTION_NBR_3         456-1
#define ROFF_EXEMPTION_AMOUNT_3      459-1
#define ROFF_FILLER1                 468-1
#define ROFF_AREA_SWEEP_DATE         480-1
#define ROFF_FILLER2                 484-1
#define ROFF_SSN_ID                  485-1
#define ROFF_M_ZIP4                  491-1
#define ROFF_S_ZIP                   495-1
#define ROFF_S_ZIP4                  500-1
#define ROFF_FILLER3                 504-1
#define ROFF_CURRENT_YEAR_ACTION     594-1
#define ROFF_PARCEL_TYPE             595-1
#define ROFF_CENSUS_TRACT1           596-1
#define ROFF_APPRAISER_NUMBER        600-1
#define ROFF_VALUE_REASON_CODE       605-1
#define ROFF_CHANGE_YEAR             609-1
#define ROFF_PSI_VALUE               610-1
#define ROFF_FILLER4                 619-1
#define ROFF_PERS_PROP_PENALTY       621-1
#define ROFF_PSI_PENALTY             630-1
#define ROFF_AUTO_GRANT_FLAG         639-1
#define ROFF_AUTO_GRANT_YEAR         640-1
#define ROFF_FILLER5                 642-1

#define RSIZ_APN                     10
#define RSIZ_TRA                     5
#define RSIZ_OWNERSHIP_CODE          1
#define RSIZ_PRIME_OWNER_NAME        30
#define RSIZ_OWNERSHIP_PERCENT       5
#define RSIZ_SECOND_OWNER_ID         1
#define RSIZ_SECOND_OWNER_NAME       30
#define RSIZ_SFT_FLAG                1
#define RSIZ_SENIOR_POST_FLAG        1
#define RSIZ_RETIRED_FLAG            1
#define RSIZ_LIEN_YEAR               2
#define RSIZ_TRA_CHANGE_YEAR         2
#define RSIZ_M_STRNAME               30
#define RSIZ_M_STRSFX                4
#define RSIZ_M_ODD_EVEN              1
#define RSIZ_M_STRNUM                5
#define RSIZ_M_FRACTION              4
#define RSIZ_M_APT_NBR               9
#define RSIZ_M_CITYST                30
#define RSIZ_M_ZIP                   5
#define RSIZ_DESCRIPTION             45
#define RSIZ_ACREAGE                 6
#define RSIZ_S_STRNAME               30
#define RSIZ_S_STRSFX                4
#define RSIZ_S_ODD_EVEN              1
#define RSIZ_S_STRNUM                5
#define RSIZ_S_FRACTION              4
#define RSIZ_S_APT_NBR               9
#define RSIZ_S_CITYABBR              5
#define RSIZ_ZONING                  12
#define RSIZ_ZONING_ATLAS            4
#define RSIZ_CENSUS_TRACT            4
#define RSIZ_TAX_CODE_A              1
#define RSIZ_TAX_CODE_B              1
#define RSIZ_REJECT_CODE             1
#define RSIZ_CURRENT_DEED_NBR        8
#define RSIZ_CURRENT_DEED_DATE       6
#define RSIZ_PRIOR_DEED_NBR          8
#define RSIZ_PRIOR_DEED_DATE         6
#define RSIZ_PRIOR_DEED_NBR_2        8
#define RSIZ_PRIOR_DEED_DATE_2       6
#define RSIZ_SALE_PRICE              9
#define RSIZ_FROM_PARCEL             10
#define RSIZ_TO_PARCEL               10
#define RSIZ_TRAFFIC_FEE_AREA        2
#define RSIZ_DWELL_PER_PCL           2
#define RSIZ_USE_CODE                4
#define RSIZ_RESP_CODE               4
#define RSIZ_AREA_CODE               4
#define RSIZ_HO_CODE                 1
#define RSIZ_LAND_VALUE              9
#define RSIZ_INVALID_TRA             1
#define RSIZ_EXE_CODE                2
#define RSIZ_EXE_NBR                 3
#define RSIZ_EXE_AMT                 9
#define RSIZ_FILLER1                 12
#define RSIZ_AREA_SWEEP_DATE         4
#define RSIZ_FILLER2                 1
#define RSIZ_SSN_ID                  6
#define RSIZ_M_ZIP4                  4
#define RSIZ_S_ZIP                   5
#define RSIZ_S_ZIP4                  4
#define RSIZ_FILLER3                 90
#define RSIZ_CURRENT_YEAR_ACTION     1
#define RSIZ_PARCEL_TYPE             1
#define RSIZ_APPRAISER_NUMBER        5
#define RSIZ_VALUE_REASON_CODE       4
#define RSIZ_CHANGE_YEAR             1
#define RSIZ_PSI_VALUE               9
#define RSIZ_FILLER4                 2
#define RSIZ_PERS_PROP_PENALTY       9
#define RSIZ_PSI_PENALTY             9
#define RSIZ_AUTO_GRANT_FLAG         1
#define RSIZ_AUTO_GRANT_YEAR         2
#define RSIZ_FILLER5                 4

typedef struct _tCcxRoll
{ // 647 bytes
   char  Apn[RSIZ_APN];                         // 3-3-3-1
   char  TRA[RSIZ_TRA];
   char  OwnerCode1[RSIZ_OWNERSHIP_CODE];
   char  Name1[RSIZ_PRIME_OWNER_NAME];
   char  PercentOwn[RSIZ_OWNERSHIP_PERCENT];    // 999V99
   char  OwnerCode2;
   char  Name2[RSIZ_SECOND_OWNER_NAME];
   char  Sft_Flag;
   char  Senior_Flag;
   char  Retired_Flag;                          // "R" if retired
   char  LienYr[RSIZ_LIEN_YEAR];
   char  TRA_ChgYr[RSIZ_TRA_CHANGE_YEAR];
   //char  M_StrName[RSIZ_M_STRNAME];
   //char  M_StrSfx[RSIZ_M_STRSFX];
   //char  M_OddEven;                             // "O" OR "E"
   //char  M_StrNum[RSIZ_M_STRNUM];               
   //char  M_StrFra[RSIZ_M_FRACTION];             // MAY ALSO BE "TO" STREET NUMBER
                                                // MAY ALSO BE NUMBERED STREET NAME (I.E. 3RD) IN WHICH
                                                // CASE THE STREET TYPE (I.E. ST) IS IN STRT NAME
   //char  M_Unit[RSIZ_M_APT_NBR];
   //char  M_CitySt[RSIZ_M_CITYST];
   //char  M_Zip[RSIZ_M_ZIP];
   CCX_MADR M_Adr;
   char  Description[RSIZ_DESCRIPTION];
   char  Acreage[RSIZ_ACREAGE];
   //char  S_StrName[RSIZ_S_STRNAME];
   //char  S_StrSfx[RSIZ_S_STRSFX];
   //char  S_OddEven;
   //char  S_StrNum[RSIZ_S_STRNUM];
   //char  S_StrFra[RSIZ_S_FRACTION];
   //char  S_Unit[RSIZ_S_APT_NBR];
   //char  S_CityAbbr[RSIZ_S_CITYABBR];
   CCX_SADR S_Adr;
   char  Zoning[RSIZ_ZONING];
   char  Zoning_Atlas[RSIZ_ZONING_ATLAS];
   char  CensusTract[RSIZ_CENSUS_TRACT];
   char  Tax_CodeA;
   char  Tax_CodeB;
   char  RejectCode;
   char  CurDeedNum[RSIZ_CURRENT_DEED_NBR];     // 5-3
   char  CurDeedDate[RSIZ_CURRENT_DEED_DATE];   // YYMMDD
   char  PriDeedNum[RSIZ_CURRENT_DEED_NBR];
   char  PriDeedDate[RSIZ_CURRENT_DEED_DATE];
   char  PriDeedNum2[RSIZ_CURRENT_DEED_NBR];
   char  PriDeedDate2[RSIZ_CURRENT_DEED_DATE];
   char  SalePrice[RSIZ_SALE_PRICE];            // Sale price from stapms
   char  From_Parcel[RSIZ_FROM_PARCEL];
   char  To_Parcel[RSIZ_FROM_PARCEL];
   char  Trafic_Fee_Area[RSIZ_TRAFFIC_FEE_AREA];
   char  Dwell_Per_Parcel[RSIZ_DWELL_PER_PCL];
   char  UseCode[RSIZ_USE_CODE];
   char  RespCode[RSIZ_RESP_CODE];
   char  AreaCode[RSIZ_AREA_CODE];
   char  HO_Code;
   char  Land[RSIZ_LAND_VALUE];
   char  Impr[RSIZ_LAND_VALUE];
   char  PP_Val[RSIZ_LAND_VALUE];
   char  Invalid_TRA;
   char  Exe_Code1[RSIZ_EXE_CODE];
   char  Exe_Num1[RSIZ_EXE_NBR];
   char  Exe_Amt1[RSIZ_EXE_AMT];
   char  Exe_Code2[RSIZ_EXE_CODE];
   char  Exe_Num2[RSIZ_EXE_NBR];
   char  Exe_Amt2[RSIZ_EXE_AMT];
   char  Exe_Code3[RSIZ_EXE_CODE];
   char  Exe_Num3[RSIZ_EXE_NBR];
   char  Exe_Amt3[RSIZ_EXE_AMT];
   char  filler1[23];
   char  M_Zip4[RSIZ_M_ZIP4];
   char  S_Zip[RSIZ_S_ZIP];
   char  S_Zip4[RSIZ_S_ZIP4];
   char  filler2[90];
   char  CurYearAction;
   char  ParcelType;
   char  CensusTract1[RSIZ_CENSUS_TRACT];
   char  AppraiserNo[RSIZ_APPRAISER_NUMBER];
   char  ValueResonCode[RSIZ_VALUE_REASON_CODE];
   char  ChgYr[RSIZ_CHANGE_YEAR];
   char  Psi_Val[RSIZ_PSI_VALUE];
   char  filler4[2];
   char  PP_Pen[RSIZ_LAND_VALUE];
   char  Psi_Pen[RSIZ_PSI_PENALTY];
   char  Auto_Flag;
   char  Auto_GrantYr[RSIZ_AUTO_GRANT_YEAR];
   char  filler5[6];
} CCX_ROLL;

typedef struct _tCcxName
{
   char  OwnerCode1;
   char  Name1[RSIZ_PRIME_OWNER_NAME];
   char  PercentOwn[RSIZ_OWNERSHIP_PERCENT];    // 999V99
   char  OwnerCode2;
   char  Name2[RSIZ_SECOND_OWNER_NAME];
} CCX_NAME;

/*
// New layout 551-byte record
#define ROFF_APN                     1-1
#define ROFF_TRA                     11-1
#define ROFF_OWNERSHIP_CODE          16-1
#define ROFF_PRIME_OWNER_NAME        17-1
#define ROFF_OWNERSHIP_PERCENT       47-1
#define ROFF_SECOND_OWNER_ID         52-1
#define ROFF_SECOND_OWNER_NAME       53-1
#define ROFF_SFT_FLAG                83-1
#define ROFF_SENIOR_POST_FLAG        84-1
#define ROFF_RETIRED_FLAG            85-1       // R if retired
#define ROFF_LIEN_YEAR               86-1
#define ROFF_TRA_CHANGE_YEAR         90-1
#define ROFF_M_STRNAME               94-1
#define ROFF_M_STRSFX                124-1
#define ROFF_M_ODD_EVEN              128-1
#define ROFF_M_STRNUM                129-1
#define ROFF_M_FRACTION              134-1
#define ROFF_M_APT_NBR               138-1
#define ROFF_M_CITYST                147-1
#define ROFF_M_ZIP                   177-1
#define ROFF_DESCRIPTION             182-1
#define ROFF_ACREAGE                 227-1      // 9(3)V9(3)  as in 123.321  acres
#define ROFF_S_STRNAME               233-1
#define ROFF_S_STRSFX                263-1
#define ROFF_S_ODD_EVEN              267-1
#define ROFF_S_STRNUM                268-1
#define ROFF_S_FRACTION              273-1
#define ROFF_S_APT_NBR               277-1
#define ROFF_S_CITYABBR              286-1
#define ROFF_ZONING                  291-1
#define ROFF_ZONING_ATLAS            303-1
#define ROFF_CENSUS_TRACT            307-1      // S9(4)V9(2) BLANK
#define ROFF_TAX_CODE_A              313-1
#define ROFF_TAX_CODE_B              314-1
#define ROFF_REJECT_CODE             315-1
#define ROFF_CURRENT_DEED_NBR        316-1
#define ROFF_CURRENT_DEED_DATE       324-1
#define ROFF_PRIOR_DEED_NBR          332-1
#define ROFF_PRIOR_DEED_DATE         340-1
#define ROFF_PRIOR_DEED_NBR_2        348-1
#define ROFF_PRIOR_DEED_DATE_2       356-1
#define ROFF_SALE_PRICE              364-1
#define ROFF_FROM_PARCEL             373-1
#define ROFF_TO_PARCEL               383-1
#define ROFF_TRAFFIC_FEE_AREA        393-1
#define ROFF_DWELL_PER_PCL           395-1
#define ROFF_USE_CODE                398-1
#define ROFF_RESP_CODE               402-1
#define ROFF_AREA_CODE               406-1
#define ROFF_HO_CODE                 410-1
#define ROFF_LAND_VALUE              411-1
#define ROFF_IMPR_VALUE              420-1
#define ROFF_PERSONAL_PROPERTY       429-1
#define ROFF_INVALID_TRA             438-1
#define ROFF_EXEMPTION_CODE_1        439-1
#define ROFF_EXEMPTION_NBR_1         441-1
#define ROFF_EXEMPTION_AMOUNT_1      444-1
#define ROFF_EXEMPTION_CODE_2        453-1
#define ROFF_EXEMPTION_NBR_2         455-1
#define ROFF_EXEMPTION_AMOUNT_2      458-1
#define ROFF_EXEMPTION_CODE_3        467-1
#define ROFF_EXEMPTION_NBR_3         469-1
#define ROFF_EXEMPTION_AMOUNT_3      472-1
#define ROFF_AREA_SWEEP_DATE         481-1
#define ROFF_FILLER1                 489-1
#define ROFF_M_ZIP4                  494-1
#define ROFF_S_ZIP                   498-1
#define ROFF_S_ZIP4                  503-1
#define ROFF_CURRENT_YEAR_ACTION     507-1
#define ROFF_PARCEL_TYPE             508-1
#define ROFF_APPRAISER_NUMBER        509-1
#define ROFF_VRC                     514-1
#define ROFF_PSI_VALUE               518-1
#define ROFF_PERS_PROP_PENALTY       527-1
#define ROFF_PSI_PENALTY             536-1
#define ROFF_AUTO_GRANT_FLAG         545-1
#define ROFF_AUTO_GRANT_YEAR         546-1
#define ROFF_FILLER2                 550-1

#define RSIZ_APN                     10
#define RSIZ_TRA                     5
#define RSIZ_OWNERSHIP_CODE          1
#define RSIZ_PRIME_OWNER_NAME        30
#define RSIZ_OWNERSHIP_PERCENT       5
#define RSIZ_SECOND_OWNER_ID         1
#define RSIZ_SECOND_OWNER_NAME       30
#define RSIZ_SFT_FLAG                1
#define RSIZ_SENIOR_POST_FLAG        1
#define RSIZ_RETIRED_FLAG            1
#define RSIZ_LIEN_YEAR               4
#define RSIZ_TRA_CHANGE_YEAR         4
#define RSIZ_M_STRNAME               30
#define RSIZ_M_STRSFX                4
#define RSIZ_M_ODD_EVEN              1
#define RSIZ_M_STRNUM                5
#define RSIZ_M_FRACTION              4
#define RSIZ_M_APT_NBR               9
#define RSIZ_M_CITYST                30
#define RSIZ_M_ZIP                   5
#define RSIZ_DESCRIPTION             45
#define RSIZ_ACREAGE                 6
#define RSIZ_S_STRNAME               30
#define RSIZ_S_STRSFX                4
#define RSIZ_S_ODD_EVEN              1
#define RSIZ_S_STRNUM                5
#define RSIZ_S_FRACTION              4
#define RSIZ_S_APT_NBR               9
#define RSIZ_S_CITYABBR              5
#define RSIZ_ZONING                  12
#define RSIZ_ZONING_ATLAS            4
#define RSIZ_CENSUS_TRACT            6
#define RSIZ_TAX_CODE_A              1
#define RSIZ_TAX_CODE_B              1
#define RSIZ_REJECT_CODE             1
#define RSIZ_CURRENT_DEED_NBR        8
#define RSIZ_CURRENT_DEED_DATE       8
#define RSIZ_SALE_PRICE              9
#define RSIZ_FROM_PARCEL             10
#define RSIZ_TO_PARCEL               10
#define RSIZ_TRAFFIC_FEE_AREA        2
#define RSIZ_DWELL_PER_PCL           3
#define RSIZ_USE_CODE                4
#define RSIZ_RESP_CODE               4
#define RSIZ_AREA_CODE               4
#define RSIZ_HO_CODE                 1
#define RSIZ_LAND_VALUE              9
#define RSIZ_IMPR_VALUE              9
#define RSIZ_PERSONAL_PROPERTY       9
#define RSIZ_INVALID_TRA             1
#define RSIZ_EXE_CODE                2
#define RSIZ_EXE_NBR                 3
#define RSIZ_EXE_AMT                 9
#define RSIZ_AREA_SWEEP_DATE         8
#define RSIZ_FILLER1                 5
#define RSIZ_M_ZIP4                  4
#define RSIZ_S_ZIP                   5
#define RSIZ_S_ZIP4                  4
#define RSIZ_CURRENT_YEAR_ACTION     1
#define RSIZ_PARCEL_TYPE             1
#define RSIZ_APPRAISER_NUMBER        5
#define RSIZ_VRC                     4
#define RSIZ_PSI_VALUE               9
#define RSIZ_PERS_PROP_PENALTY       9
#define RSIZ_PSI_PENALTY             9
#define RSIZ_AUTO_GRANT_FLAG         1
#define RSIZ_AUTO_GRANT_YEAR         4
#define RSIZ_FILLER2                 2
                                         
typedef struct _tCcxRoll
{  // 551-byte record
   char  Apn[RSIZ_APN];
   char  TRA[RSIZ_TRA];
   char  OwnerCode1;
   char  Name1[RSIZ_PRIME_OWNER_NAME];
   char  PercentOwn[RSIZ_OWNERSHIP_PERCENT];    // 999V99
   char  OwnerCode2;
   char  Name2[RSIZ_SECOND_OWNER_NAME];
   char  Sft_Flag;
   char  Senior_Flag;
   char  Retired_Flag;                          // "R" if retired
   char  LienYr[RSIZ_LIEN_YEAR];
   char  TRA_ChgYr[RSIZ_TRA_CHANGE_YEAR];
   char  M_StrName[RSIZ_M_STRNAME];
   char  M_StrSfx[RSIZ_M_STRSFX];
   char  M_OddEven;                             // "O" OR "E"
   char  M_StrNum[RSIZ_M_STRNUM];               
   char  M_StrFra[RSIZ_M_FRACTION];             // MAY ALSO BE "TO" STREET NUMBER
                                                // MAY ALSO BE NUMBERED STREET NAME (I.E. 3RD) IN WHICH
                                                // CASE THE STREET TYPE (I.E. ST) IS IN STRT NAME
   char  M_Unit[RSIZ_M_APT_NBR];
   char  M_CitySt[RSIZ_M_CITYST];
   char  M_Zip[RSIZ_M_ZIP];
   char  Description[RSIZ_DESCRIPTION];
   char  Acreage[RSIZ_ACREAGE];
   char  S_StrName[RSIZ_S_STRNAME];
   char  S_StrSfx[RSIZ_S_STRSFX];
   char  S_OddEven;
   char  S_StrNum[RSIZ_S_STRNUM];
   char  S_StrFra[RSIZ_S_FRACTION];
   char  S_Unit[RSIZ_S_APT_NBR];
   char  S_CityAbbr[RSIZ_S_CITYABBR];
   char  Zoning[RSIZ_ZONING];
   char  Zoning_Atlas[RSIZ_ZONING_ATLAS];
   char  CensusTract[RSIZ_CENSUS_TRACT];
   char  Tax_CodeA;
   char  Tax_CodeB;
   char  RejectCode;
   char  CurDeedNum[RSIZ_CURRENT_DEED_NBR];     // 5-3
   char  CurDeedDate[RSIZ_CURRENT_DEED_DATE];   // YYMMDD
   char  PriDeedNum[RSIZ_CURRENT_DEED_NBR];
   char  PriDeedDate[RSIZ_CURRENT_DEED_DATE];
   char  PriDeedNum2[RSIZ_CURRENT_DEED_NBR];
   char  PriDeedDate2[RSIZ_CURRENT_DEED_DATE];
   char  SalePrice[RSIZ_SALE_PRICE];            // Sale price from stapms
   char  From_Parcel[RSIZ_APN];
   char  To_Parcel[RSIZ_APN];
   char  Trafic_Fee_Area[RSIZ_TRAFFIC_FEE_AREA];
   char  Dwell_Per_Parcel[RSIZ_DWELL_PER_PCL];
   char  UseCode[RSIZ_USE_CODE];
   char  RespCode[RSIZ_RESP_CODE];
   char  AreaCode[RSIZ_AREA_CODE];
   char  HO_Code;
   char  Land[RSIZ_LAND_VALUE];
   char  Impr[RSIZ_LAND_VALUE];
   char  PP_Val[RSIZ_LAND_VALUE];
   char  Invalid_TRA;
   char  Exe_Code1[RSIZ_EXE_CODE];
   char  Exe_Num1[RSIZ_EXE_NBR];
   char  Exe_Amt1[RSIZ_EXE_AMT];
   char  Exe_Code2[RSIZ_EXE_CODE];
   char  Exe_Num2[RSIZ_EXE_NBR];
   char  Exe_Amt2[RSIZ_EXE_AMT];
   char  Exe_Code3[RSIZ_EXE_CODE];
   char  Exe_Num3[RSIZ_EXE_NBR];
   char  Exe_Amt3[RSIZ_EXE_AMT];
   char  Area_Sweep_Date[RSIZ_AREA_SWEEP_DATE];
   char  filler1[RSIZ_FILLER1];
   char  M_Zip4[RSIZ_M_ZIP4];
   char  S_Zip[RSIZ_S_ZIP];
   char  S_Zip4[RSIZ_S_ZIP4];
   char  CurYearAction;
   char  ParcelType;
   char  Appr_Num[RSIZ_APPRAISER_NUMBER];
   char  VRC[RSIZ_VRC];
   char  Psi_Val[RSIZ_PSI_VALUE];
   char  PP_Pen[RSIZ_PSI_PENALTY];
   char  Psi_Pen[RSIZ_PSI_PENALTY];
   char  Auto_Flag;
   char  Auto_GrantYr[RSIZ_AUTO_GRANT_YEAR];
   char  filler2[RSIZ_FILLER2];
} CCX_ROLL;
*/
                                         
#define SOFF_DEED_TYPE             1-1
#define SOFF_PDR_TYPE              2-1
#define SOFF_TRA                   3-1
#define SOFF_USE_CODE              7-1
#define SOFF_MAP_MODEL             9-1
#define SOFF_SALE_DATE             15-1
#define SOFF_TOT_LIV_AREA          23-1
#define SOFF_CLASS_TYPE            30-1
#define SOFF_EFF_YR                35-1
#define SOFF_REJECT_CD             39-1   // 3, 4, 5, 8
#define SOFF_APN                   40-1
#define SOFF_MODEL                 49-1
#define SOFF_BEDROOMS              55-1
#define SOFF_BATHROOMS             58-1
#define SOFF_ROOMS                 61-1
#define SOFF_CONDITION             64-1
#define SOFF_GARAGE                65-1
#define SOFF_CARPORT               66-1
#define SOFF_PARKING_STALLS        67-1
#define SOFF_GARG_CONVRSN          70-1
#define SOFF_ATTACHED_GARG         71-1
#define SOFF_ADDITION              72-1
#define SOFF_ARCHITECTURE          73-1
#define SOFF_CENTRAL_AIR_HEAT      74-1
#define SOFF_FLOOR_LEVEL           75-1
#define SOFF_LEVEL_ACCESS          76-1
#define SOFF_BACK_TO_BACK          77-1
#define SOFF_END_UNIT              78-1
#define SOFF_PROXIMITES            79-1
#define SOFF_POOL_SQFT             82-1
#define SOFF_PATIO_DECK_SQFT       87-1
#define SOFF_BLDG_NUMBER           94-1
#define SOFF_RESPONSIBLITY_CODE    97-1
#define SOFF_LAND_PROBLEM          99-1
#define SOFF_TOPOG                 100-1
#define SOFF_SETTINGS              101-1
#define SOFF_NUISANCES             106-1
#define SOFF_BASE_LOT_FACTOR       109-1
#define SOFF_BASE_LOT_VALUE        112-1
#define SOFF_USEABLE_AREA          121-1
#define SOFF_DEED_REF_VOL          128-1
#define SOFF_DEED_REF_PAGE         133-1
#define SOFF_PRICE_FROM_STAMPS     138-1
#define SOFF_CONFIRMED_PRICE       149-1
#define SOFF_CONFIRMATION_CODE     159-1
#define SOFF_APPRAISAL_CD          160-1
#define SOFF_APPRAISAL_PCT         161-1
#define SOFF_APPRAISAL_BASE_YR     165-1
#define SOFF_APPRAISED_VAL         166-1
#define SOFF_SITUS_STREET_NUMBER   177-1
#define SOFF_SITUS_STREET_NAME     183-1
#define SOFF_SITUS_CITY_CODE       195-1
#define SOFF_ACTIVITY_NUMBER       197-1
#define SOFF_RCLND_TOT_LIV_AREA    200-1     // Est. replacement cost
#define SOFF_RCLND_UNFINISHED      207-1
#define SOFF_RCLND_POOL            214-1
#define SOFF_RCLND_MISC            221-1     // Est. replacement cost for misc building
#define SOFF_UNUSED1               228-1
#define SOFF_TOTAL_LIV_AREA        237-1
#define SOFF_BUILDING              244-1
#define SOFF_UNUSED2               247-1
#define SOFF_REMARK_1              257-1
#define SOFF_REMARK_2              302-1

#define SSIZ_DEED_TYPE             1
#define SSIZ_PDR_TYPE              1
#define SSIZ_TRA                   4
#define SSIZ_USE_CODE              2
#define SSIZ_MAP_MODEL             6
#define SSIZ_SALE_DATE             8
#define SSIZ_TOT_LIV_AREA          7
#define SSIZ_CLASS_TYPE            5
#define SSIZ_EFF_YR                4
#define SSIZ_REJECT_CD             1
#define SSIZ_APN                   9
#define SSIZ_MODEL                 6
#define SSIZ_BEDROOMS              3
#define SSIZ_BATHROOMS             3
#define SSIZ_ROOMS                 3
#define SSIZ_CONDITION             1
#define SSIZ_GARAGE                1
#define SSIZ_CARPORT               1
#define SSIZ_PARKING_STALLS        3
#define SSIZ_GARG_CONVRSN          1
#define SSIZ_ATTACHED_GARG         1
#define SSIZ_ADDITION              1
#define SSIZ_ARCHITECTURE          1
#define SSIZ_CENTRAL_AIR_HEAT      1
#define SSIZ_FLOOR_LEVEL           1
#define SSIZ_LEVEL_ACCESS          1
#define SSIZ_BACK_TO_BACK          1
#define SSIZ_END_UNIT              1
#define SSIZ_PROXIMITES            3
#define SSIZ_POOL_SQFT             5
#define SSIZ_PATIO_DECK_SQFT       7
#define SSIZ_BLDG_NUMBER           3
#define SSIZ_RESPONSIBLITY_CODE    2
#define SSIZ_LAND_PROBLEM          1
#define SSIZ_TOPOG                 1
#define SSIZ_SETTINGS              5
#define SSIZ_NUISANCES             3
#define SSIZ_BASE_LOT_FACTOR       3
#define SSIZ_BASE_LOT_VALUE        9
#define SSIZ_USEABLE_AREA          7
#define SSIZ_DEED_REF_VOL          5
#define SSIZ_DEED_REF_PAGE         5
#define SSIZ_PRICE_FROM_STAMPS     11
#define SSIZ_CONFIRMED_PRICE       10
#define SSIZ_CONFIRMATION_CODE     1
#define SSIZ_APPRAISAL_CD          1
#define SSIZ_APPRAISAL_PCT         4
#define SSIZ_APPRAISAL_BASE_YR     1
#define SSIZ_APPRAISED_VAL         11
#define SSIZ_S_STRNUM              6
#define SSIZ_S_STRNAME             12
#define SSIZ_S_CITYCODE            2
#define SSIZ_ACTIVITY_NUMBER       3
#define SSIZ_RCLND_TOT_LIV_AREA    7
#define SSIZ_RCLND_UNFINISHED      7
#define SSIZ_RCLND_POOL            7
#define SSIZ_RCLND_MISC            7
#define SSIZ_UNUSED1               9
#define SSIZ_TOTAL_LIV_AREA        7
#define SSIZ_BUILDING              3
#define SSIZ_UNUSED2               10
#define SSIZ_REMARK_1              45
#define SSIZ_REMARK_2              45
   
typedef struct _tCcxSale
{
   char  DeedType;
   char  PdrType;                         // M = ROSSMOOR, R = RESID
   char  TRA[SSIZ_TRA];
   char  UseCode[SSIZ_USE_CODE];
   char  MapModel[SSIZ_MAP_MODEL];
   char  SaleDate[SSIZ_SALE_DATE];        // YYYYMMDD
   char  Tot_Liv_Area[SSIZ_TOT_LIV_AREA];
   char  ClassType[SSIZ_CLASS_TYPE];
   char  EffYear[SSIZ_EFF_YR];
   char  Rej_Code;                        // 3=partial interest
   char  Apn[SSIZ_APN];
   char  Model[SSIZ_MODEL];
   char  Beds[SSIZ_BEDROOMS];
   char  Baths[SSIZ_BATHROOMS];
   char  Rooms[SSIZ_ROOMS];               // Units if commercial, rooms if residential
   char  Condition;                       // A/F/G/??
   char  Garage;                          // G=garage (Z)
   char  CarPort;                         // C=carport (C)
   char  ParkStalls[SSIZ_PARKING_STALLS];
   char  GarConv;                         // Garage conversion flag
   char  AttachedGarage;
   char  Addition;
   char  Architecture;
   char  Central_AirHeat;
   char  FloorLevel;
   char  Level_Access;
   char  Back2Back;
   char  EndUnit;
   char  Proximities[SSIZ_PROXIMITES];
   char  PoolSqft[SSIZ_POOL_SQFT];
   char  PatioSqft[SSIZ_PATIO_DECK_SQFT];
   char  BldgNum[SSIZ_BLDG_NUMBER];
   char  Resp_Code[SSIZ_RESPONSIBLITY_CODE];
   char  LandProblem;
   char  Topog;
   char  Settings[SSIZ_SETTINGS];
   char  Nuisances[SSIZ_NUISANCES];
   char  BaseLot_Factor[SSIZ_BASE_LOT_FACTOR];
   char  BaseLot_Value[SSIZ_BASE_LOT_VALUE];
   char  UsableArea[SSIZ_USEABLE_AREA];
   char  DeedVol[SSIZ_DEED_REF_VOL];            // left 2 bytes is YY, right 3 bytes is volume
   char  DeedPage[SSIZ_DEED_REF_PAGE];          // take right 3 bytes
   char  PriceFromStamp[SSIZ_PRICE_FROM_STAMPS];
   char  Conf_Price[SSIZ_CONFIRMED_PRICE];
   char  Conf_Code;
   char  Appr_Code;                             // R, P
   char  Appr_Pct[SSIZ_APPRAISAL_PCT];          // 999V9
   char  Appr_BaseYr;
   char  Appr_Val[SSIZ_APPRAISED_VAL];
   char  S_StrNum[SSIZ_S_STRNUM];
   char  S_StrName[SSIZ_S_STRNAME];
   char  S_CityCode[SSIZ_S_CITYCODE];
   char  Activity_Num[SSIZ_ACTIVITY_NUMBER];
   char  Rclnd_BldgSqft[SSIZ_TOT_LIV_AREA];
   char  Rclnd_Unfinished[SSIZ_TOT_LIV_AREA];
   char  Rclnd_Pool[SSIZ_TOT_LIV_AREA];
   char  Rclnd_Misc[SSIZ_TOT_LIV_AREA];
   char  filler1[SSIZ_UNUSED1];
   char  Total_Tot_Liv_Area[SSIZ_TOTAL_LIV_AREA];
   char  Bldg[SSIZ_BUILDING];
   char  filler2[SSIZ_UNUSED2];
   char  Remark1[SSIZ_REMARK_1];
   char  Remark2[SSIZ_REMARK_2];
} CCX_SALE;

#define  OFF_CSAL_SALEDATE    3
#define  OFF_CSAL_APN         11
#define  OFF_CSAL_BLDGNUM     20
#define  OFF_CSAL_DOCNUM      23
#define  OFF_CSAL_DEEDVOL     23
#define  OFF_CSAL_DEEDPG      28
#define  OFF_CSAL_CALCPR      33
#define  OFF_CSAL_CONFPR      44
#define  OFF_CSAL_CONFCODE    54
#define  OFF_CSAL_APPRCODE    55
#define  OFF_CSAL_APPRPCT     56
#define  OFF_CSAL_APPRBY      61
#define  OFF_CSAL_APPRVAL     62
#define  OFF_CSAL_ACT_NUM     73
#define  OFF_CSAL_REM1        76
#define  OFF_CSAL_REM2        121

typedef struct t_CcxCumSale
{  // 165 bytes
   char  DeedType;                     // G, Q, T
   char  PdrType;
   char  SaleDate[SSIZ_SALE_DATE];     // YYYYMMDD
   char  Apn[SSIZ_APN];
   char  BldgNum[SSIZ_BLDG_NUMBER];
   char  DeedVol[SSIZ_DEED_REF_VOL];   // left 2 bytes is YY, right 3 bytes is volume
   char  DeedPage[SSIZ_DEED_REF_PAGE]; // take right 3 bytes
   char  PriceFromStamp[SSIZ_PRICE_FROM_STAMPS];
   char  Conf_Price[SSIZ_CONFIRMED_PRICE];
   char  Conf_Code;
   char  Appr_Code;
   char  Appr_Pct[SSIZ_APPRAISAL_PCT+1];// 999.9
   char  Appr_BaseYr;
   char  Appr_Val[SSIZ_APPRAISED_VAL];
   char  Activity_Num[SSIZ_ACTIVITY_NUMBER];
   char  Remark1[SSIZ_REMARK_1];
   char  Remark2[SSIZ_REMARK_2];
} CCX_CSAL;


#define COFF_PDR_TYPE        1-1
#define COFF_APN             2-1
#define COFF_BLDG_NUMBER     11-1
#define COFF_EFFECTIVE_YEAR  14-1
#define COFF_BLDG_SQFT       18-1
#define COFF_BEDROOMS        25-1
#define COFF_BATHSROOMS      28-1
#define COFF_UNITS_ROOMS     31-1
#define COFF_POOL_FLAG       36-1
#define COFF_POOL_EFFYR      37-1
#define COFF_BLDG_FLAG       41-1
#define COFF_BLDG_EFFYR      42-1
#define COFF_ACRES           46-1
#define COFF_LOT_SQFT        53-1
#define COFF_VIEW_FLAG       60-1
#define COFF_ZONE            61-1
#define COFF_USE_CODE        73-1
#define COFF_VACANT_LOT      75-1
#define COFF_CONST_YEAR      76-1
#define COFF_BLDG2_SQFT      80-1
#define COFF_BASEMENT_SQFT   87-1
#define COFF_GARAGE_SQFT     94-1


#define CSIZ_PDR_TYPE        1
#define CSIZ_APN             9
#define CSIZ_BLDG_NUMBER     3
#define CSIZ_EFF_YR          4
#define CSIZ_BLDG_SQFT       7
#define CSIZ_BEDROOMS        3
#define CSIZ_BATHSROOMS      3
#define CSIZ_UNITS_ROOMS     5
#define CSIZ_POOL_FLAG       1
#define CSIZ_POOL_EFFYR      4
#define CSIZ_BLDG_FLAG       1
#define CSIZ_BLDG_EFFYR      4
#define CSIZ_ACRES           7
#define CSIZ_LOT_SQFT        7
#define CSIZ_VIEW_FLAG       1
#define CSIZ_ZONE            12
#define CSIZ_USE_CODE        2
#define CSIZ_VACANT_LOT      1
#define CSIZ_CONST_YEAR      4
#define CSIZ_BLDG2_SQFT      7
#define CSIZ_BASEMENT_SQFT   7
#define CSIZ_GARAGE_SQFT     7

// PDRFILE.TXT or PDRDATA.TXT
typedef struct _tCcxChar
{
   char  PdrType;                      // M=Rossmoor, R=Residential, O=Other
   char  Apn[CSIZ_APN];
   char  BldgNum[CSIZ_BLDG_NUMBER];
   char  EffYear[CSIZ_EFF_YR];
   char  BldgSqft[CSIZ_BLDG_SQFT];     // 1st floor area
   char  Beds[CSIZ_BEDROOMS];
   char  Baths[CSIZ_BATHSROOMS];
   char  Rooms[CSIZ_UNITS_ROOMS];      // Units if commercial (PdrType=O), rooms if residential
   char  Pool;                         // Y or N
   char  Pool_EffYr[CSIZ_EFF_YR];
   char  Bldg_Flag;                    // Y or N
   char  Bldg_EffYr[CSIZ_EFF_YR];
   char  Acres[CSIZ_GARAGE_SQFT];      // Don't use if present in srmfile.txt
                                       // If the PDR Type = O and Area <500 then the value is Acres; otherwise the value is square feet
   char  LotSqft[CSIZ_GARAGE_SQFT];    // Don't use if present in srmfile.txt
   char  View;                         // Y or N
   char  Zoning[CSIZ_ZONE];
   char  UseCode[CSIZ_USE_CODE];
   char  VacantLot;                    // V if vacant
   char  YearBuilt[CSIZ_EFF_YR];
   char  BldgSqft2[CSIZ_BLDG_SQFT];    // 2nd floor area
   char  BsmtSqft[CSIZ_BASEMENT_SQFT];
   char  GarSqft[CSIZ_GARAGE_SQFT];
} CCX_CHAR;

#define  OFF_CCHAR_APN                 1
#define  OFF_CCHAR_BLDGNUM             10
#define  OFF_CCHAR_LAST_ACTIVITY_NUM   13
#define  OFF_CCHAR_HAVE_SHORT_CHAR     16
#define  OFF_CCHAR_PDR_TYPE            17
#define  OFF_CCHAR_TRA                 18
#define  OFF_CCHAR_USE                 22
#define  OFF_CCHAR_MAP_MODEL           24
#define  OFF_CCHAR_TOT_LIV_AREA        30
#define  OFF_CCHAR_BLDG_SQFT           37
#define  OFF_CCHAR_CLASSTYPE           44
#define  OFF_CCHAR_EFF_YR              49
#define  OFF_CCHAR_REJECT_CD           53
#define  OFF_CCHAR_MODEL               54
#define  OFF_CCHAR_BEDROOMS            60
#define  OFF_CCHAR_BATHROOMS           63
#define  OFF_CCHAR_UNITS               66
#define  OFF_CCHAR_TOTAL_ROOMS         71
#define  OFF_CCHAR_CONDITION           74
#define  OFF_CCHAR_GARAGE              75
#define  OFF_CCHAR_CARPORT             76
#define  OFF_CCHAR_PARKING_STALLS      77
#define  OFF_CCHAR_GARAGE_CONVERSION   80
#define  OFF_CCHAR_ATTACHED_GARAGE     81
#define  OFF_CCHAR_ADDITION            82
#define  OFF_CCHAR_ARCHITECTURE        83
#define  OFF_CCHAR_CENTRAL_AIRHEAT     84
#define  OFF_CCHAR_FLOOR_LEVEL         85
#define  OFF_CCHAR_LEVEL_ACCESS        86
#define  OFF_CCHAR_BACK2BACK           87
#define  OFF_CCHAR_END_UNIT            88
#define  OFF_CCHAR_PROXIMITES          89
#define  OFF_CCHAR_POOL_SQFT           92
#define  OFF_CCHAR_PATIO_DECK_SQFT     97
#define  OFF_CCHAR_RESP_CODE           104
#define  OFF_CCHAR_LAND_PROBLEM        106
#define  OFF_CCHAR_TOPOG               107
#define  OFF_CCHAR_SETTINGS            108
#define  OFF_CCHAR_NUISANCES           113
#define  OFF_CCHAR_POOL_FLAG           116
#define  OFF_CCHAR_POOL_EFFYR          117
#define  OFF_CCHAR_BLDG_FLAG           121
#define  OFF_CCHAR_BLDG_EFFYR          122
#define  OFF_CCHAR_ACRES               126
#define  OFF_CCHAR_LOT_SQFT            133
#define  OFF_CCHAR_ZONE                140
#define  OFF_CCHAR_VACANT_LOT          152
#define  OFF_CCHAR_BASE_LOT_FACTOR     153
#define  OFF_CCHAR_BASE_LOT_VALUE      156
#define  OFF_CCHAR_USEABLE_AREA        165
#define  OFF_CCHAR_APPRAISAL_CODE      172
#define  OFF_CCHAR_APPRAISAL_PCT       173
#define  OFF_CCHAR_APPRAISAL_BASEYR    177
#define  OFF_CCHAR_APPRAISAL_VAL       178
#define  OFF_CCHAR_S_STRNUM            189
#define  OFF_CCHAR_S_STRNAME           195
#define  OFF_CCHAR_S_CITYCODE          207
#define  OFF_CCHAR_RCLND_TOT_LIV_AREA  209
#define  OFF_CCHAR_RCLND_UNFINISHED    216
#define  OFF_CCHAR_RCLND_POOL          223
#define  OFF_CCHAR_RCLND_MISC          230
#define  OFF_CCHAR_TOTAL_TOT_LIV_AREA  237
#define  OFF_CCHAR_VIEW                244
#define  OFF_CCHAR_BUILDING            245
#define  OFF_CCHAR_REMARK1             248
#define  OFF_CCHAR_REMARK2             293
#define  OFF_CCHAR_CONSTRUCTION_YEAR   338
#define  OFF_CCHAR_BLDG2_SQFT          342
#define  OFF_CCHAR_BASEMENT_SQFT       349
#define  OFF_CCHAR_GARAGE_SQFT         356
#define  OFF_CCHAR_STORIES         		363
#define  OFF_CCHAR_FULL_BATH         	367
#define  OFF_CCHAR_HALF_BATH         	369
#define  OFF_CCHAR_SALEDATE          	371
#define  OFF_CCHAR_FILLER           	379

#define  SIZ_CCHAR_UNITS               5

// ccx_char_cum.sls (extracted from salfile.txt)
typedef struct t_CcxCumChar
{  // 512 bytes
   char  Apn[SSIZ_APN];
   char  BldgNum[SSIZ_BLDG_NUMBER];
   char  Activity_Num[SSIZ_ACTIVITY_NUMBER];
   char  HaveShortChar;
   char  PdrType;                         // M, R, or O
   char  TRA[SSIZ_TRA];                   // May not usable
   char  UseCode[SSIZ_USE_CODE];
   char  MapModel[SSIZ_MAP_MODEL];
   char  Tot_Liv_Area[SSIZ_TOT_LIV_AREA]; // One of BldgSqft
   char  BldgSqft[SSIZ_TOT_LIV_AREA];
   char  ClassType[SSIZ_CLASS_TYPE];
   char  EffYear[SSIZ_EFF_YR];
   char  Rej_Code;
   char  Model[SSIZ_MODEL];
   char  Beds[SSIZ_BEDROOMS];
   char  Baths[SSIZ_BATHROOMS];
   char  Units[SIZ_CCHAR_UNITS];
   char  Rooms[SSIZ_ROOMS];            
   char  Condition;
   char  Garage;                          // G=garage (Z)
   char  CarPort;                         // C=carport (C)
   char  ParkStalls[SSIZ_PARKING_STALLS];
   char  GarConv;                         // Garage conversion flag
   char  AttachedGarage;
   char  Addition;
   char  Architecture;
   char  Central_AirHeat;                 // A,H,N
   char  FloorLevel;
   char  Level_Access;
   char  Back2Back;
   char  EndUnit;
   char  Proximities[SSIZ_PROXIMITES];
   char  PoolSqft[SSIZ_POOL_SQFT];
   char  PatioSqft[SSIZ_PATIO_DECK_SQFT];
   char  Resp_Code[SSIZ_RESPONSIBLITY_CODE];
   char  LandProblem;
   char  Topog;
   char  Settings[SSIZ_SETTINGS];
   char  Nuisances[SSIZ_NUISANCES];
   char  Pool;                            // Y or N
   char  Pool_EffYr[CSIZ_EFF_YR];
   char  Bldg_Flag;                       // Y or N
   char  Bldg_EffYr[CSIZ_EFF_YR];
   char  Acres[CSIZ_GARAGE_SQFT];         // Don't use if present in srmfile.txt
   char  LotSqft[CSIZ_GARAGE_SQFT];       // Don't use if present in srmfile.txt
   char  Zoning[CSIZ_ZONE];
   char  VacantLot;                       // V if vacant
   char  BaseLot_Factor[SSIZ_BASE_LOT_FACTOR];
   char  BaseLot_Value[SSIZ_BASE_LOT_VALUE];
   char  UsableArea[SSIZ_USEABLE_AREA];
   char  Appr_Code;
   char  Appr_Pct[SSIZ_APPRAISAL_PCT];    // 999V9
   char  Appr_BaseYr;
   char  Appr_Val[SSIZ_APPRAISED_VAL];
   char  S_StrNum[SSIZ_S_STRNUM];
   char  S_StrName[SSIZ_S_STRNAME];
   char  S_CityCode[SSIZ_S_CITYCODE];
   char  Rclnd_BldgSqft[SSIZ_TOT_LIV_AREA];
   char  Rclnd_Unfinished[SSIZ_TOT_LIV_AREA];
   char  Rclnd_Pool[SSIZ_TOT_LIV_AREA];
   char  Rclnd_Misc[SSIZ_TOT_LIV_AREA];
   char  Total_Tot_Liv_Area[SSIZ_TOTAL_LIV_AREA];  // 2nd floor area
   char  View;                         // Y or N
   char  Bldg[SSIZ_BUILDING];
   char  Remark1[SSIZ_REMARK_1];
   char  Remark2[SSIZ_REMARK_2];
   char  YearBuilt[CSIZ_EFF_YR];
   char  BldgSqft2[SSIZ_TOTAL_LIV_AREA];
   char  BsmtSqft[CSIZ_GARAGE_SQFT];
   char  GarSqft[CSIZ_GARAGE_SQFT];
   char  Stories[SIZ_STORIES];
   char  FBaths[SIZ_BATH_F];
   char  HBaths[SIZ_BATH_H];           // 369
   char  SaleDate[SIZ_TRANSFER_DT];    // 371
   char  filler[132];                  // 379
   char  CRLF[2];                      // 511
} CCX_CCHR;

#define  GEO_SIZ_APN          14
#define  GEO_SIZ_STRNUM       7
#define  GEO_SIZ_FRA          3
#define  GEO_SIZ_DIR          2
#define  GEO_SIZ_STREET       24
#define  GEO_SIZ_SUFFIX       5
#define  GEO_SIZ_UNIT         6
#define  GEO_SIZ_CITY         24
#define  GEO_SIZ_STATE        2
#define  GEO_SIZ_ZIP          5
#define  GEO_SIZ_ZIP4         4
#define  GEO_SIZ_HOE          1
#define  GEO_SIZ_ADDR1        52
#define  GEO_SIZ_ADDR2        38

// Geo field offset
#define  GEO_OFF_APN          0
#define  GEO_OFF_STRNUM       14
#define  GEO_OFF_FRA          21
#define  GEO_OFF_DIR          24
#define  GEO_OFF_STREET       26
#define  GEO_OFF_SUFFIX       50
#define  GEO_OFF_UNIT         55
#define  GEO_OFF_CITY         61
#define  GEO_OFF_STATE        85
#define  GEO_OFF_ZIP          87
#define  GEO_OFF_ZIP4         92
#define  GEO_OFF_HOE          96
#define  GEO_OFF_ADDR1        97
#define  GEO_OFF_ADDR2        149

typedef struct _tAdrExt
{
   char  Apn[GEO_SIZ_APN];
   char  StrNum[GEO_SIZ_STRNUM];
   char  StrFra[GEO_SIZ_FRA];
   char  StrDir[GEO_SIZ_DIR];
   char  StrName[GEO_SIZ_STREET];
   char  StrSfx[GEO_SIZ_SUFFIX];
   char  Unit[GEO_SIZ_UNIT];
   char  City[GEO_SIZ_CITY];
   char  State[GEO_SIZ_STATE];
   char  Zip[GEO_SIZ_ZIP];
   char  Zip4[GEO_SIZ_ZIP4];
   char  HO_Flag;
   char  Addr1[GEO_SIZ_ADDR1];
   char  Addr2[GEO_SIZ_ADDR2];
} ADREXT;
#define  SIZ_ADREXT           187

// Tax layout
// District Master
#define  OFF_DM_TRA           1
#define  OFF_DM_GROUP         6
#define  OFF_DM_DIST          10
#define  OFF_DM_TYPE          14
#define  OFF_DM_DESC          15
#define  OFF_DM_SPEC_LEVY_CD  33
#define  OFF_DM_LEVY_CD       34
#define  OFF_DM_REV_NBR       36

#define  SIZ_DM_TRA           5
#define  SIZ_DM_GROUP         4
#define  SIZ_DM_DIST          4
#define  SIZ_DM_TYPE          1
#define  SIZ_DM_DESC          18
#define  SIZ_DM_SPEC_LEVY_CD  1
#define  SIZ_DM_LEVY_CD       2
#define  SIZ_DM_REV_NBR       4

typedef struct _tDistMstr
{
   char  TRA[SIZ_DM_TRA];
   char  Group[SIZ_DM_GROUP];
   char  Dist[SIZ_DM_DIST];
   char  Type[SIZ_DM_TYPE];
   char  Desc[SIZ_DM_DESC];
   char  Spec_Levy_Cd[SIZ_DM_SPEC_LEVY_CD];
   char  Levy_Cd[SIZ_DM_LEVY_CD];
   char  Rev_Nbr[SIZ_DM_REV_NBR];
} DISTMSTR;

// CORTAC file
#define  OFF_CT_APN           1
#define  OFF_CT_NOTAX_FLG     12
#define  OFF_CT_NUMBER        13
#define  OFF_CT_LOAN_INFO     18
#define  OFF_CT_INSTAMT_1     48
#define  OFF_CT_PAIDFLG_1     61    // P=paid
#define  OFF_CT_INSTAMT_2     62
#define  OFF_CT_PAIDFLG_2     75
#define  OFF_CT_TAX_SALE      76  
#define  OFF_CT_EXEMPT        77
#define  OFF_CT_FILLER2       78
#define  OFF_CT_BILLNUM       81

#define  SIZ_CT_APN           10
#define  SIZ_CT_FILLER1       1
#define  SIZ_CT_NOTAX_FLG     1
#define  SIZ_CT_NUMBER        5
#define  SIZ_CT_LOAN_INFO     30
#define  SIZ_CT_INSTAMT       13
#define  SIZ_CT_PAIDFLG       1
#define  SIZ_CT_TAX_SALE      1  
#define  SIZ_CT_EXEMPT        1
#define  SIZ_CT_FILLER2       3
#define  SIZ_CT_BILLNUM       10

typedef struct _tCortac
{
   char  Apn         [SIZ_CT_APN];
   char  Filler1;
   char  NoTax_Flg;
   char  CortacNumber[SIZ_CT_NUMBER];
   char  Loan_Info   [SIZ_CT_LOAN_INFO];
   char  InstAmt_1   [SIZ_CT_INSTAMT];
   char  PaidFlg_1;
   char  InstAmt_2   [SIZ_CT_INSTAMT];
   char  PaidFlg_2;
   char  Tax_Sale;
   char  Exempt;
   char  Filler2     [SIZ_CT_FILLER2];
   char  BillNum     [SIZ_CT_BILLNUM];
} CORTAC;

// Levy record layout - 8 record types

// Levy type A
#define  SIZ_LV_CORR_SEG_NBR     4
#define  SIZ_LV_TAXAMT           12
#define  SIZ_LV_FILLER1          7
#define  SIZ_LV_TAXRATE          5
#define  SIZ_LV_DATE             8
#define  SIZ_LV_PAIDBATCH_NBR    3
#define  SIZ_LV_LEVYDESC         18
#define  SIZ_LV_TAXABILITYCODE   2
#define  SIZ_LV_VOLUME_NBR       3
#define  SIZ_LV_RAWTAX           14
#define  SIZ_LV_FILLER3          3
#define  SIZ_LV_FUND             4
#define  SIZ_LV_REVENUE          4
#define  SIZ_LV_NET_AV           10
#define  SIZ_LV_FILLER2          2
#define  SIZ_LV_YEAR             4
#define  SIZ_LV_BILLNUM          6

// County tax
typedef struct _tLevyRecordA
{
   char  Corr_Seg_Nbr   [SIZ_LV_CORR_SEG_NBR];     // 36
   char  TaxAmt         [SIZ_LV_TAXAMT];           // 40 - Tax Amt may include penalty amt
   char  InstAmt1       [SIZ_LV_TAXAMT];           // 52
   char  InstAmt2       [SIZ_LV_TAXAMT];           // 64
   char  PenAmt1        [SIZ_LV_TAXAMT];           // 76
   char  PenAmt2        [SIZ_LV_TAXAMT];           // 88
   char  DelqCost       [SIZ_LV_TAXAMT];           // 100
   char  Filler1        [SIZ_LV_FILLER1];          // 112
   char  TaxRate        [SIZ_LV_TAXRATE];          // 119 - 9V9999
   char  PaidDate1      [SIZ_LV_DATE];             // 124
   char  PaidBatch_Date1[SIZ_LV_DATE];             // 132
   char  PaidBatch_Nbr1 [SIZ_LV_PAIDBATCH_NBR];    // 140
   char  PaidDate2      [SIZ_LV_DATE];             // 143
   char  PaidBatch_Date2[SIZ_LV_DATE];             // 151
   char  PaidBatch_Nbr2 [SIZ_LV_PAIDBATCH_NBR];    // 159
   char  LevyDesc       [SIZ_LV_LEVYDESC];         // 162
   char  TaxabilityCode [SIZ_LV_TAXABILITYCODE];   // 180
   char  Volume_Nbr     [SIZ_LV_VOLUME_NBR];       // 182
   char  RawTax         [SIZ_LV_RAWTAX];           // 185 - -9(8)V99999
   char  BillStatus;                               // 199 - 
   char  Receipt;
   char  Special_Add;
   char  Cancel;
   char  Filler3        [SIZ_LV_FILLER3];          // 203
   char  Fund           [SIZ_LV_FUND];             // 206
   char  Revenue        [SIZ_LV_REVENUE];          // 210
   char  Net_AV         [SIZ_LV_NET_AV];           // 214
   char  Net_AV_Penalty;                           // 224
   char  Filler2        [SIZ_LV_FILLER2];
} LEVY_A;

#define  SIZ_LV_CODE             2
#define  SIZ_LV_FILLER4          2
#define  SIZ_LV_FILLER5          48
#define  SIZ_LV_FILLER6          21
#define  SIZ_LV_FILLER7          2

// Mello Roo
typedef struct _tLevyRecordB
{
   char  LevyCode       [SIZ_LV_CODE];
   char  Filler4        [SIZ_LV_FILLER4];
   char  TaxAmt         [SIZ_LV_TAXAMT];           // 40
   char  InstAmt1       [SIZ_LV_TAXAMT];
   char  InstAmt2       [SIZ_LV_TAXAMT];
   char  Filler5        [SIZ_LV_FILLER5];
   char  PaidDate1      [SIZ_LV_DATE];             // 124
   char  PaidBatch_Date1[SIZ_LV_DATE];
   char  PaidBatch_Nbr1 [SIZ_LV_PAIDBATCH_NBR];
   char  PaidDate2      [SIZ_LV_DATE];
   char  PaidBatch_Date2[SIZ_LV_DATE];
   char  PaidBatch_Nbr2 [SIZ_LV_PAIDBATCH_NBR];
   char  LevyDesc       [SIZ_LV_LEVYDESC];
   char  TaxabilityCode [SIZ_LV_TAXABILITYCODE];
   char  Volume_Nbr     [SIZ_LV_VOLUME_NBR];
   char  Filler6        [SIZ_LV_FILLER6];
   char  Fund           [SIZ_LV_FUND];
   char  Revenue        [SIZ_LV_REVENUE];
   char  Net_AV         [SIZ_LV_NET_AV];
   char  Net_AV_Penalty;
   char  Filler7        [SIZ_LV_FILLER7];
} LEVY_B;

#define  SIZ_LV_FILLER10         43
#define  SIZ_LV_FILLER11         7
#define  SIZ_LV_FILLER12         3

// Special district
typedef struct _tLevyRecordC
{
   char  LevyCode       [SIZ_LV_CODE];
   char  XY_Code;
   char  Filler9;
   char  TaxAmt         [SIZ_LV_TAXAMT];           // 40
   char  InstAmt1       [SIZ_LV_TAXAMT];
   char  InstAmt2       [SIZ_LV_TAXAMT];
   char  Filler10       [SIZ_LV_FILLER10];
   char  TaxRate        [SIZ_LV_TAXRATE];          // 119
   char  PaidDate1      [SIZ_LV_DATE];             // 124
   char  PaidBatch_Date1[SIZ_LV_DATE];
   char  PaidBatch_Nbr1 [SIZ_LV_PAIDBATCH_NBR];
   char  PaidDate2      [SIZ_LV_DATE];
   char  PaidBatch_Date2[SIZ_LV_DATE];
   char  PaidBatch_Nbr2 [SIZ_LV_PAIDBATCH_NBR];
   char  LevyDesc       [SIZ_LV_LEVYDESC];
   char  TaxabilityCode [SIZ_LV_TAXABILITYCODE];   // 180
   char  Volume_Nbr     [SIZ_LV_VOLUME_NBR];
   char  RawTax         [SIZ_LV_RAWTAX];           // 185
   char  Filler11       [SIZ_LV_FILLER11];         // 199
   char  Fund           [SIZ_LV_FUND];             // 206
   char  Revenue        [SIZ_LV_REVENUE];          // 210
   char  Net_AV         [SIZ_LV_NET_AV];
   char  Filler12       [SIZ_LV_FILLER12];
} LEVY_C;

#define  SIZ_LV_FILLER14         2
#define  SIZ_LV_FILLER15         48
#define  SIZ_LV_FILLER16         21
#define  SIZ_LV_FILLER17         13

// Weed Abatement
typedef struct _tLevyRecordD
{
   char  LevyCode       [SIZ_LV_CODE];
   char  Filler14       [SIZ_LV_FILLER14];
   char  TaxAmt         [SIZ_LV_TAXAMT];           // 40
   char  InstAmt1       [SIZ_LV_TAXAMT];
   char  InstAmt2       [SIZ_LV_TAXAMT];
   char  Filler15       [SIZ_LV_FILLER15];
   char  PaidDate1      [SIZ_LV_DATE];             // 124
   char  PaidBatch_Date1[SIZ_LV_DATE];
   char  PaidBatch_Nbr1 [SIZ_LV_PAIDBATCH_NBR];
   char  PaidDate2      [SIZ_LV_DATE];
   char  PaidBatch_Date2[SIZ_LV_DATE];
   char  PaidBatch_Nbr2 [SIZ_LV_PAIDBATCH_NBR];
   char  LevyDesc       [SIZ_LV_LEVYDESC];
   char  TaxabilityCode [SIZ_LV_TAXABILITYCODE];   // 180
   char  Volume_Nbr     [SIZ_LV_VOLUME_NBR];       // 182
   char  Filler16       [SIZ_LV_FILLER16];         // 185
   char  Fund           [SIZ_LV_FUND];             // 206
   char  Revenue        [SIZ_LV_REVENUE];
   char  Filler17       [SIZ_LV_FILLER17];
} LEVY_D;

#define  SIZ_LV_FILLER19         2
#define  SIZ_LV_FILLER20         48
#define  SIZ_LV_FILLER21         21
#define  SIZ_LV_FILLER22         3
#define  SIZ_LV_NET_AV1          9

// Special Levy - Prorated Esc tax, TAX CORR, ESCAPE ASMT TAX
typedef struct _tLevyRecordE
{
   char  LevyCode       [SIZ_LV_CODE];
   char  Filler19       [SIZ_LV_FILLER19];
   char  TaxAmt         [SIZ_LV_TAXAMT];           // 40
   char  InstAmt1       [SIZ_LV_TAXAMT];
   char  InstAmt2       [SIZ_LV_TAXAMT];
   char  Filler20       [SIZ_LV_FILLER20];
   char  PaidDate1      [SIZ_LV_DATE];             // 124
   char  PaidBatch_Date1[SIZ_LV_DATE];
   char  PaidBatch_Nbr1 [SIZ_LV_PAIDBATCH_NBR];
   char  PaidDate2      [SIZ_LV_DATE];
   char  PaidBatch_Date2[SIZ_LV_DATE];
   char  PaidBatch_Nbr2 [SIZ_LV_PAIDBATCH_NBR];
   char  LevyDesc       [SIZ_LV_LEVYDESC];         // 162
   char  TaxabilityCode [SIZ_LV_TAXABILITYCODE];   // 180
   char  Volume_Nbr     [SIZ_LV_VOLUME_NBR];       // 182
   char  Filler21       [SIZ_LV_FILLER21];         // 185
   char  Fund           [SIZ_LV_FUND];             // 206
   char  Revenue        [SIZ_LV_REVENUE];          // 210
   char  Net_AV         [SIZ_LV_NET_AV1];          // 214
   char  Net_AV_Penalty;
   char  Filler22       [SIZ_LV_FILLER22];
} LEVY_E;

#define  SIZ_LV_FILLER24         2
#define  SIZ_LV_FILLER25         10
#define  SIZ_LV_FILLER26         2
#define  SIZ_LV_BOND_ASSMNT_NBR  11
#define  SIZ_LV_INTFUND          7

// 1915 Act Bonds
typedef struct _tLevyRecordF
{
   char  LevyCode       [SIZ_LV_CODE];             // 36
   char  Filler24       [SIZ_LV_FILLER24];
   char  TaxAmt         [SIZ_LV_TAXAMT];           // 40
   char  InstAmt1       [SIZ_LV_TAXAMT];
   char  InstAmt2       [SIZ_LV_TAXAMT];
   char  Bond_Pri_Amt   [SIZ_LV_TAXAMT];
   char  Bond_Int_Amt   [SIZ_LV_TAXAMT];
   char  Bond_ColFee_Amt[SIZ_LV_TAXAMT];
   char  Bond_EngFee_Amt[SIZ_LV_TAXAMT];
   char  PaidDate1      [SIZ_LV_DATE];             // 124
   char  PaidBatch_Date1[SIZ_LV_DATE];
   char  PaidBatch_Nbr1 [SIZ_LV_PAIDBATCH_NBR];
   char  PaidDate2      [SIZ_LV_DATE];
   char  PaidBatch_Date2[SIZ_LV_DATE];
   char  PaidBatch_Nbr2 [SIZ_LV_PAIDBATCH_NBR];
   char  LevyDesc       [SIZ_LV_LEVYDESC];         // 162
   char  TaxabilityCode [SIZ_LV_TAXABILITYCODE];   // 180
   char  Volume_Nbr     [SIZ_LV_VOLUME_NBR];       // 182
   char  Bond_Assmnt_Nbr[SIZ_LV_BOND_ASSMNT_NBR];  // 185
   char  Filler25       [SIZ_LV_FILLER25];         // 196
   char  Fund           [SIZ_LV_FUND];             // 206
   char  Revenue        [SIZ_LV_REVENUE];          // 210
   char  Int_Fund       [SIZ_LV_INTFUND];          // 214
   char  Int_Revenue    [SIZ_LV_REVENUE];          // 221
   char  Filler26       [SIZ_LV_FILLER26];         // 225
} LEVY_F;

#define  SIZ_LV_FILLER28         89
#define  SIZ_LV_FILLER29         42
#define  SIZ_LV_FILLER30         5
#define  SIZ_LV_SALE_NBR         5
#define  SIZ_LV_SALE_SEG_SFX     3
#define  SIZ_LV_RED_CERT_NBR     6

// Tax Sale ?
typedef struct _tLevyRecordG
{
   char  Status_Ind;                               // 36
   char  SaleYear       [SIZ_LV_YEAR];             // 37
   char  SaleNbr        [SIZ_LV_SALE_NBR];         // 41
   char  Sale_Seq_Char;                            // 46
   char  Sale_Seg_Sfx   [SIZ_LV_SALE_SEG_SFX];     // 47
   char  BatchDate      [SIZ_LV_DATE];             // 50
   char  Sold4Tax_Status;                          // 58 - A/C/R
   char  Can_Red_Date   [SIZ_LV_DATE];             // 59 - Cancel/Redeem date
   char  Redeem_Cert_Nbr[SIZ_LV_RED_CERT_NBR];     // 67
   char  Filler28       [SIZ_LV_FILLER28];         // 73
   char  LevyDesc       [SIZ_LV_LEVYDESC];         // 162
   char  TaxabilityCode [SIZ_LV_TAXABILITYCODE];   // 180
   char  Volume_Nbr     [SIZ_LV_VOLUME_NBR];       // 182
   char  Filler29       [SIZ_LV_FILLER29];         // 191
} LEVY_G;

#define  SIZ_LV_FILLER31         103
#define  SIZ_LV_FILLER32         42
#define  SIZ_LV_ROLLMSG_NBR      2
#define  SIZ_LV_ROLLMSG          30

// Roll Messages
typedef struct _tLevyRecordZ
{
   char  Status_Ind;
   char  RollMsg_Nbr    [SIZ_LV_ROLLMSG_NBR];
   char  BatchDate      [SIZ_LV_DATE];
   char  RollMsg        [SIZ_LV_ROLLMSG];
   char  Filler31       [SIZ_LV_FILLER31];
   char  TaxabilityCode [SIZ_LV_TAXABILITYCODE];   // 180
   char  Volume_Nbr     [SIZ_LV_VOLUME_NBR];       // 182
   char  Filler32       [SIZ_LV_FILLER32];
} LEVY_Z;

#define  OFF_LV_GRP_CT_NBR       1
#define  OFF_LV_APN              6
#define  OFF_LV_TAXBILL_SFX      16
#define  OFF_LV_RECNUM           18
#define  OFF_LV_BATCH_DATE       21
#define  OFF_LV_TRA              30
#define  OFF_LV_RECTYPE          35

#define  SIZ_LV_GRP_CT_NBR       5
#define  SIZ_LV_APN              10
#define  SIZ_LV_TAXBILL_SFX      2
#define  SIZ_LV_RECNUM           3
#define  SIZ_LV_BATCH_DATE       9
#define  SIZ_LV_TRA              5  
#define  SIZ_LV_RECTYPE          1
#define  SIZ_LV_FILLER           5

typedef struct _tLevyRecord
{  // Max recsize 250
   char  Grp_Cortac_Nbr [SIZ_LV_GRP_CT_NBR];       // 1-5
   char  Apn            [SIZ_LV_APN];              // 6
   char  TaxBill_Sfx    [SIZ_LV_TAXBILL_SFX];      // 16
   char  RecNum         [SIZ_LV_RECNUM];           // 18
   char  Batch_Date     [SIZ_LV_BATCH_DATE];       // 21
   char  Tra            [SIZ_LV_TRA];              // 30
   char  RecType;                                  // 35
   union
   {  
      LEVY_A   RecA;
      LEVY_B   RecB;
      LEVY_C   RecC;
      LEVY_D   RecD;
      LEVY_E   RecE;
      LEVY_F   RecF;
      LEVY_G   RecG;
      LEVY_Z   RecZ;
      //char     sRec[191];
   } Spec;
   char  TaxYear        [SIZ_LV_YEAR];             // 227
   char  BillNum        [SIZ_LV_BILLNUM];          // 231
   char  Cancel_Date    [SIZ_LV_DATE];
   char  Filler         [SIZ_LV_FILLER];
   char  PS_Flg;                                   // 250
} LEVYREC;

// Delinquent file - SAM Delinquent File.$170906
#define  SIZ_DELQ_RECTYPE     2
#define  SIZ_DSL_APN          10
#define  SIZ_DSL_SALE_NBR     6
#define  SIZ_DSL_SFT_SEG_SFX  3

// Tax sale (Sale For Taxes)
typedef struct _tSR5500
{
   char  RecType           [SIZ_DELQ_RECTYPE];     // 1  - 00
   char  DSL_Sft_SaleYr    [SIZ_LV_YEAR];          // 3
   char  DSL_Sft_SaleNbr   [SIZ_DSL_SALE_NBR];     // 7
   char  DSL_Sft_Seg_Sfx   [SIZ_DSL_SFT_SEG_SFX];  // 13
   char  DSL_Sft_Status;                           // 16 - Blank or A=active, R=redeemed, X=delete, C=Canceled sale
   char  Apn               [SIZ_DSL_APN];          // 17
   char  DAD_Util_Prcl_Nbr [SIZ_DSL_APN];          // 27
   char  DAD_Old_Asmt_Nbr  [SIZ_DSL_APN];          // 37
   char  DTR_Prcl_Nbr      [SIZ_DSL_APN];          // 47
   char  DTR_Util_Prcl_Nbr [SIZ_DSL_APN];          // 57
   char  DTR_Old_Asmt_Nbr  [SIZ_DSL_APN];          // 67
} SR5500;

#define  SIZ_DPY_PAYAMT       11
#define  SIZ_BATCH_DATE       9
#define  SIZ_CORR_ID          4

// Delq payment
typedef struct _tSR5501
{
   char  RecType           [SIZ_DELQ_RECTYPE];     // 1  - 01
   char  PayType;                                  // 3 - F=full, I=installment, L=
   char  PayAmt            [SIZ_DPY_PAYAMT];       // 4 - monthly payment plan
   char  PayDate           [SIZ_BATCH_DATE];       // 15
   char  BatchDate         [SIZ_BATCH_DATE];       // 24
   char  BatchNbr;
   char  Corr_ID           [SIZ_CORR_ID];          // 34
   char  Corr_Type;                                // 38
   char  Sft_BatchDate     [SIZ_BATCH_DATE];       // 39
} SR5501;

// Delq tax
#define  SIZ_LEVY_CNT         4
#define  SIZ_COST_AMT         7
#define  SIZ_LEVY_CODE        2
#define  MAX_SUPL_LEVY        12

typedef struct _tLevyArea
{
   char  LevyCode          [SIZ_LEVY_CODE];        //
   char  LevyDelqAmt       [SIZ_DPY_PAYAMT];       //
} LEVYAREA;

// Delq record
typedef struct _tSR5502
{
   char  RecType           [SIZ_DELQ_RECTYPE];     // 1  - 02
   char  TaxYear           [SIZ_LV_YEAR];          // 3
   char  Apn               [SIZ_DSL_APN];          // 7
   char  PaidStatus;                               // 17 - 'P' = Fully Paid taxes
                                                   //    - 'C' = Cancelled 
                                                   //    - 'W' = Small Tax Write-off
                                                   //    - 'F' = First Installment of Secured Bill Paid (prior to transfer to Delinquent)
                                                   //    - 'N' = Not Taxable
                                                   //    - 'Z' = Zero Tax
                                                   
   char  CntyTaxDelqAmt    [SIZ_DPY_PAYAMT];       // 18
   char  LevyDelqAmt       [SIZ_DPY_PAYAMT];       // 29
   char  PenDelqAmt        [SIZ_DPY_PAYAMT];       // 40
   char  CostDelqAmt       [SIZ_COST_AMT];         // 51
   char  LevyCnt           [SIZ_LEVY_CNT];         // 58
   LEVYAREA Oth_Levy;
   LEVYAREA Supl_Levy      [MAX_SUPL_LEVY];
   char  Hist_Status;                              // 231 - 'A' or blank = Active 
                                                   //     - 'I' = Inactive Record (correction was made to the record)
                                                   //     - 'C' = Corrected delq inactive
   char  Data_Source;                              // 232 - ' ' = Auto transfer
                                                   //       'M' = Manual trans-add-sale
                                                   //       'C' = Correction
                                                   //       'T' = Manual trans-add-dlnq-to-prior
} SR5502;

#define  SIZ_DEED_VOL_NBR     5
#define  SIZ_DEED_PAGE_NBR    3

typedef struct _tSR5506
{
   char  RecType           [SIZ_DELQ_RECTYPE];     // 1  - 06
   char  DeedDate          [SIZ_BATCH_DATE];       // 3
   char  DeedVolNbr        [SIZ_DEED_VOL_NBR];     // 12
   char  DeedBeginPageNbr  [SIZ_DEED_PAGE_NBR];    // 17
   char  DeedEndPageNbr    [SIZ_DEED_PAGE_NBR];    // 20
} SR5506;

// Plan payment
typedef struct _tSR5508
{
   char  RecType           [SIZ_DELQ_RECTYPE];     // 1  - 08
   char  PayPlanDate       [SIZ_BATCH_DATE];       // 3
   char  PayPlanAmt        [SIZ_DPY_PAYAMT];       // 12
   char  PayPlanDfltDate   [SIZ_DEED_PAGE_NBR];    // 23
} SR5508;

typedef struct _tSR5515
{
   char  RecType           [SIZ_DELQ_RECTYPE];     // 1  - 15
   char  Filler            [22];
   char  Apn               [SIZ_DSL_APN];          // 25
} SR5515;

// Parcel Master Record - we may not need this file
#define  SIZ_BATCH_YEAR       4
#define  SIZ_BATCH_DAY        4
#define  SIZ_AUDIT_VOL        3
#define  SIZ_TAXABILITY_COSE  2
#define  SIZ_AV_AMT           10
#define  SIZ_AV_EXE_CODE      2
#define  SIZ_AV_EXE_NBR       4
#define  SIZ_OWNER_NAME       30
#define  SIZ_STR_ADDR         30
#define  SIZ_PROP_DESC        45
#define  SIZ_CITY_ABBR        5
#define  SIZ_TB_CITY_ST       30
#define  SIZ_PARCEL_ID        11
#define  SIZ_TAXBILL_SFX      2

typedef struct _tParcelMaster
{
   char  Grp_Cortac_Nbr [SIZ_LV_GRP_CT_NBR];
   char  Apn            [SIZ_LV_APN];
   char  TaxBill_Sfx    [SIZ_LV_TAXBILL_SFX];
   char  BatchYear      [SIZ_BATCH_YEAR];
   char  BatchDay       [SIZ_BATCH_DAY];
   char  Tra            [SIZ_LV_TRA];
   char  AuditVol       [SIZ_AUDIT_VOL];
   char  TaxabilityCose [SIZ_TAXABILITY_COSE];
   char  Av_Land        [SIZ_AV_AMT];
   char  Av_Impr        [SIZ_AV_AMT];
   char  Av_PP          [SIZ_AV_AMT];
   char  Av_PP_Impr     [SIZ_AV_AMT];
   char  Av_PP_Pen      [SIZ_AV_AMT];
   char  Av_PP_Impr_Pen [SIZ_AV_AMT];
   char  Av_ExeCode1    [SIZ_AV_EXE_CODE];
   char  Av_ExeNbr1     [SIZ_AV_EXE_NBR];
   char  Av_ExeAmt1     [SIZ_AV_AMT];
   char  Av_ExeCode2    [SIZ_AV_EXE_CODE];
   char  Av_ExeNbr2     [SIZ_AV_EXE_NBR];
   char  Av_ExeAmt2     [SIZ_AV_AMT];
   char  Av_ExeCode3    [SIZ_AV_EXE_CODE];
   char  Av_ExeNbr3     [SIZ_AV_EXE_NBR];
   char  Av_ExeAmt3     [SIZ_AV_AMT];
   char  OwnerId1;
   char  OwnerName1     [SIZ_OWNER_NAME];
   char  OwnerId2;
   char  OwnerName2     [SIZ_OWNER_NAME];
   char  S_Address      [SIZ_STR_ADDR];
   char  CityAbbr       [SIZ_CITY_ABBR];
   char  Bill_Name      [SIZ_OWNER_NAME];
   char  Bill_Addr      [SIZ_STR_ADDR];
   char  Bill_CitySt    [SIZ_TB_CITY_ST];
   char  Bill_Zip       [SIZ_CCX_SITUS_ZIP];
   char  Bill_Zip4      [SIZ_CCX_SITUS_ZIP4];
   char  PropDesc       [SIZ_PROP_DESC];
   char  CortacNumber   [SIZ_CT_NUMBER];
   char  Loan_Info      [SIZ_CT_LOAN_INFO];
   char  BillStatus;
   char  Retired;
   char  Cancelled;
   char  ManualBill;
   char  PublicAcq;
   char  BadCheck;
   char  SuplBill;
   char  NewBill;
   char  ReprintBill;
   char  NonRecalc;
   char  Refund;
   char  Pts_11;
   char  Pts_12;
   char  Pts_13;
   char  Pts_14;
   char  Pts_15;
   char  FromParcelID   [SIZ_PARCEL_ID];
   char  FromTaxBillSfx [SIZ_TAXBILL_SFX];
   char  BegParcelID    [SIZ_PARCEL_ID];
   char  BegTaxBillSfx  [SIZ_TAXBILL_SFX];
   char  EndParcelID    [SIZ_PARCEL_ID];
   char  EndTaxBillSfx  [SIZ_TAXBILL_SFX];
} PRCLMSTR;

// Daily tax files: PTS=Master file, SPT=Supplemental, BUD=Unsecured
// PTS/SPT layout
#define  PTS_BILLTYPE         0
#define  PTS_TAX_YEAR         1
#define  PTS_BILL_NO          2
#define  PTS_APN              3
#define  PTS_INST_NBR         4
#define  PTS_PAID_STATUS      5
#define  PTS_TAX_AMT          6
#define  PTS_PAID_DATE        7
#define  PTS_DUE_DATE         8
#define  PTS_S_ADDR           9
#define  PTS_S_CITY           10
#define  PTS_M_ADDR           11
#define  PTS_M_CITY           12
#define  PTS_M_ZIP            13
#define  PTS_SERIAL_NO        14
#define  PTS_SALE_ID          15
#define  PTS_BILL_SUFFIX      16
// 03/01/2023
#define  PTS_LAND             17
#define  PTS_IMPR             18
#define  PTS_PP               19
#define  PTS_EXE              20
#define  PTS_FLDS             21

// CCX exemption code
//    CG - College
//    CH - Church
//    CM - Cemetery
//    DV - Disable veteran
//    HO - Howmowners
//    HP - Hospital 
//    LC - School less then collegiate
//    PL - Public Library
//    PM - Public Museum
//    PS - Public School
//    RE - Religion
//    RV - Regular veteran
//    SC - Parochial school
//    WE - Welfare

// Standard codes
// H=Homeowners
// D=Disabled Veterans
// V=Veterans
// C=Church
// L=Library
// M=Museums
// R=Religion
// T=Tribal Housing
// W=Welfare
// U=College
// P=Public School
// E=Cemetery
// I=Hospital
// S=School
// X=Misc exempt

IDX_TBL4 CCX_Exemption[] = 
{
   "CG", "U",  2,1,
   "CH", "C" , 2,1,
   "CM", "E",  2,1,
   "DV", "D",  2,1,
   "HO", "H",  2,1,
   "HP", "I",  2,1,
   "LC", "S",  2,1,
   "PL", "L",  2,1,
   "PM", "M",  2,1,
   "PS", "P",  2,1,
   "RE", "R",  2,1,
   "RV", "V",  2,1,
   "SC", "S",  2,1,
   "WE", "W",  2,1,
   "","",0,0
};

#endif

