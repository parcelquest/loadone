#ifndef _MERGEMPA_H
#define _MERGEMPA_H

#define MPA_OFF_APN                              1-1
#define MPA_OFF_STATUS                          11-1
#define MPA_OFF_TAX_AREA                        12-1
#define MPA_OFF_SITUS_ADDR                      17-1
#define MPA_OFF_OWNER1                          97-1
#define MPA_OFF_MAIL_ST                         137-1
#define MPA_OFF_MAIL_CITY                       167-1
#define MPA_OFF_MAIL_STATE                      187-1
#define MPA_OFF_MAIL_ZIP                        189-1
#define MPA_OFF_OWNER2                          198-1
#define MPA_OFF_OWNER3                          238-1
#define MPA_OFF_OWNER4                          278-1
#define MPA_OFF_OWNER5                          318-1
#define MPA_OFF_OWNER6                          358-1
#define MPA_OFF_OWNER7                          398-1
#define MPA_OFF_OWNER8                          438-1
#define MPA_OFF_OWNER9                          478-1
#define MPA_OFF_OWNER10                         518-1
#define MPA_OFF_OWNER11                         558-1
#define MPA_OFF_OWNER12                         598-1
#define MPA_OFF_OWNER13                         638-1
#define MPA_OFF_OWNER14                         678-1
#define MPA_OFF_OWNER15                         718-1
#define MPA_OFF_OWNER16                         758-1
#define MPA_OFF_OWNER17                         798-1
#define MPA_OFF_OWNER18                         838-1
#define MPA_OFF_OWNER19                         878-1
#define MPA_OFF_OWNER20                         918-1
#define MPA_OFF_OWNER21                         958-1
#define MPA_OFF_DEED_REF                        998-1
#define MPA_OFF_BASE_YEAR                       1023-1
#define MPA_OFF_WILL_ACT                        1025-1
#define MPA_OFF_ACRES                           1028-1
#define MPA_OFF_LAND                            1037-1
#define MPA_OFF_IMPROVEMENTS                    1046-1
#define MPA_OFF_TIMBER                          1055-1
#define MPA_OFF_WILL_LAND                       1064-1
#define MPA_OFF_WILL_MARKET                     1073-1
#define MPA_OFF_EXEMPTION                       1082-1
#define MPA_OFF_EXEMP_CD                        1091-1
#define MPA_OFF_TREES_VINES                     1093-1
#define MPA_OFF_DPS                             1102-1
#define MPA_OFF_LIENDATE_OWNER                  1111-1
#define MPA_OFF_CAREOF                          1141-1
#define MPA_OFF_PROPERTY_USE                    1171-1
#define MPA_OFF_PLATBOOK_PAGE                   1175-1
#define MPA_OFF_USE_ZONE                        1195-1   // Usecode or Zoning
#define MPA_OFF_ZONING                          1201-1   // Y means previous field is Zoning
#define MPA_OFF_LEGAL                           1202-1


#define MPA_SIZ_APN                             10
#define MPA_SIZ_STATUS                          1
#define MPA_SIZ_TAX_AREA                        5
#define MPA_SIZ_SITUS_ADDR                      80
#define MPA_SIZ_OWNER1                          40
#define MPA_SIZ_MAIL_ST                         30
#define MPA_SIZ_MAIL_CITY                       20
#define MPA_SIZ_MAIL_STATE                      2
#define MPA_SIZ_MAIL_ZIP                        9
#define MPA_SIZ_OWNER2                          40
#define MPA_SIZ_OWNER3                          40
#define MPA_SIZ_OWNER4                          40
#define MPA_SIZ_OWNER5                          40
#define MPA_SIZ_OWNER6                          40
#define MPA_SIZ_OWNER7                          40
#define MPA_SIZ_OWNER8                          40
#define MPA_SIZ_OWNER9                          40
#define MPA_SIZ_OWNER10                         40
#define MPA_SIZ_OWNER11                         40
#define MPA_SIZ_OWNER12                         40
#define MPA_SIZ_OWNER13                         40
#define MPA_SIZ_OWNER14                         40
#define MPA_SIZ_OWNER15                         40
#define MPA_SIZ_OWNER16                         40
#define MPA_SIZ_OWNER17                         40
#define MPA_SIZ_OWNER18                         40
#define MPA_SIZ_OWNER19                         40
#define MPA_SIZ_OWNER20                         40
#define MPA_SIZ_OWNER21                         40
#define MPA_SIZ_DEED_REF                        25
#define MPA_SIZ_BASE_YEAR                       2
#define MPA_SIZ_WILL_ACT                        3
#define MPA_SIZ_ACRES                           9
#define MPA_SIZ_LAND                            9
#define MPA_SIZ_IMPROVEMENTS                    9
#define MPA_SIZ_TIMBER                          9
#define MPA_SIZ_WILL_LAND                       9
#define MPA_SIZ_WILL_MARKET                     9
#define MPA_SIZ_EXEMPTION                       9
#define MPA_SIZ_EXEMP_CD                        2
#define MPA_SIZ_TREES_VINES                     9
#define MPA_SIZ_DPS                             9
#define MPA_SIZ_LIENDATE_OWNER                  30
#define MPA_SIZ_CAREOF                          30
#define MPA_SIZ_PROPERTY_USE                    4
#define MPA_SIZ_PLATBOOK_PAGE                   20
#define MPA_SIZ_USE_ZONE                        6
#define MPA_SIZ_ZONING                          1
#define MPA_SIZ_LEGAL                           30

typedef struct _tMpaRoll
{
   char Apn[MPA_SIZ_APN];
   char Status[MPA_SIZ_STATUS];
   char Tax_Area[MPA_SIZ_TAX_AREA];
   char Situs_Addr[MPA_SIZ_SITUS_ADDR];
   char Owner1[MPA_SIZ_OWNER1];
   char Mail_St[MPA_SIZ_MAIL_ST];
   char Mail_City[MPA_SIZ_MAIL_CITY];
   char Mail_State[MPA_SIZ_MAIL_STATE];
   char Mail_Zip[MPA_SIZ_MAIL_ZIP];
   char Owner2[MPA_SIZ_OWNER2];
   char Owner3[MPA_SIZ_OWNER3];
   char Owner4[MPA_SIZ_OWNER4];
   char Owner5[MPA_SIZ_OWNER5];
   char Owner6[MPA_SIZ_OWNER6];
   char Owner7[MPA_SIZ_OWNER7];
   char Owner8[MPA_SIZ_OWNER8];
   char Owner9[MPA_SIZ_OWNER9];
   char Owner10[MPA_SIZ_OWNER10];
   char Owner11[MPA_SIZ_OWNER11];
   char Owner12[MPA_SIZ_OWNER12];
   char Owner13[MPA_SIZ_OWNER13];
   char Owner14[MPA_SIZ_OWNER14];
   char Owner15[MPA_SIZ_OWNER15];
   char Owner16[MPA_SIZ_OWNER16];
   char Owner17[MPA_SIZ_OWNER17];
   char Owner18[MPA_SIZ_OWNER18];
   char Owner19[MPA_SIZ_OWNER19];
   char Owner20[MPA_SIZ_OWNER20];
   char Owner21[MPA_SIZ_OWNER21];
   char Deed_Ref[MPA_SIZ_DEED_REF];
   char Base_Year[MPA_SIZ_BASE_YEAR];
   char Will_Act[MPA_SIZ_WILL_ACT];
   char Acres[MPA_SIZ_ACRES];
   char Land[MPA_SIZ_LAND];
   char Improvements [MPA_SIZ_IMPROVEMENTS];
   char Timber[MPA_SIZ_TIMBER];
   char Will_Land[MPA_SIZ_WILL_LAND];
   char Will_Market[MPA_SIZ_WILL_MARKET];
   char Exemption[MPA_SIZ_EXEMPTION];
   char Exemp_Cd[MPA_SIZ_EXEMP_CD];
   char Trees_Vines[MPA_SIZ_TREES_VINES];
   char Dps[MPA_SIZ_DPS];
   char Liendate_Owner[MPA_SIZ_LIENDATE_OWNER];
   char Careof[MPA_SIZ_CAREOF];
   char Property_Use[MPA_SIZ_PROPERTY_USE];
   char Platbook_Page[MPA_SIZ_PLATBOOK_PAGE];
   char Use_Zone[MPA_SIZ_USE_ZONE];
   char Zoning[MPA_SIZ_ZONING];
   char Legal[MPA_SIZ_LEGAL];
} MPA_ROLL;

#define  MPA_CSIZ_DATE           8
#define  MPA_CSIZ_PRICE          10
#define  MPA_CSIZ_ACRES          10
#define  MPA_CSIZ_ZONING         10
#define  MPA_CSIZ_PROPTYPE       20
#define  MPA_CSIZ_UTILITIES      20
#define  MPA_CSIZ_LAND           10
#define  MPA_CSIZ_OTH_IMPR       10
#define  MPA_CSIZ_YRBLT          4
#define  MPA_CSIZ_BLDGSQFT       8
#define  MPA_CSIZ_BED            2
#define  MPA_CSIZ_BATH           4
#define  MPA_CSIZ_FP             16
#define  MPA_CSIZ_DECK           6
#define  MPA_CSIZ_ADDRESS        48
#define  MPA_CSIZ_CLASS          6
#define  MPA_CSIZ_HEAT           18
#define  MPA_CSIZ_GARAGE         8
#define  MPA_CSIZ_REMARK         104
#define  MPA_CSIZ_ACCESS         8
#define  MPA_CSIZ_TOPO           50

typedef struct _tMpaChar
{  // recsize = 512
   char  Apn[MPA_SIZ_APN];             // 1
   char  SaleDate[MPA_CSIZ_DATE];      // 11
   char  SalePrice[MPA_CSIZ_PRICE];    // 19
   char  Acres[MPA_CSIZ_ACRES];        // 29
   char  Zoning[MPA_CSIZ_ZONING];      // 39
   char  PropType[MPA_CSIZ_PROPTYPE];  // 49
   char  Land[MPA_CSIZ_LAND];          // 69
   char  OthImpr[MPA_CSIZ_OTH_IMPR];   // 89
   char  YrBlt[MPA_CSIZ_YRBLT];        // 99
   char  BldgSqft[MPA_CSIZ_BLDGSQFT];  // 93
   char  Bed[MPA_CSIZ_BED];            // 101
   char  Bath[MPA_CSIZ_BATH];          // 103
   char  Fp[MPA_CSIZ_FP];              // 107
   char  Deck[MPA_CSIZ_DECK];          // 123
   char  Pool[1];                      // 129    Y/N
   char  Spa[1];                       // 130    Y/N
   char  Address[MPA_CSIZ_ADDRESS];    // 131
   char  BldgCls[MPA_CSIZ_CLASS];      // 179
   char  Heat[MPA_CSIZ_HEAT];          // 185
   char  Gar[MPA_CSIZ_GARAGE];         // 203
   char  When[MPA_CSIZ_DATE];          // 211
   char  Remarks[MPA_CSIZ_REMARK];     // 219
   char  Utilities[MPA_CSIZ_UTILITIES];// 323
   char  Access[MPA_CSIZ_ACCESS];      // 343 - Paved/Unpaved
   char  Topography[MPA_CSIZ_TOPO];    // 351 
   char  filler[110];                  // 401
   char  CrLf[2];                      // 511
} MPA_CHAR;

// Unassessed
#define  MPA_UA_APN           0
#define  MPA_UA_STATUS        1
#define  MPA_UA_TRA           2
#define  MPA_UA_SITUS         3
#define  MPA_UA_OWNER1        4
#define  MPA_UA_M_STR         5
#define  MPA_UA_M_CITY        6
#define  MPA_UA_M_ST          7
#define  MPA_UA_M_ZIP         8
#define  MPA_UA_OWNER2        9
#define  MPA_UA_DEED_REF      10
#define  MPA_UA_BASE_YR       11
#define  MPA_UA_WIL_ACT       12
#define  MPA_UA_ACRES         13
#define  MPA_UA_LAND          14
#define  MPA_UA_IMPR          15
#define  MPA_UA_TIMBER        16
#define  MPA_UA_WIL_LAND      17
#define  MPA_UA_WIL_MARKET    18
#define  MPA_UA_EXEMPTION     19
#define  MPA_UA_EXEMP_CD      20
#define  MPA_UA_TREES_VINES   21
#define  MPA_UA_DPS           22
#define  MPA_UA_LIEN_OWNER    23
#define  MPA_UA_CAREOF        24
#define  MPA_UA_PROP_USE      25
#define  MPA_UA_PLATBOOK_PG   26
#define  MPA_UA_USE_ZONE      27
#define  MPA_UA_ZONING        28
#define  MPA_UA_LEGAL         29

IDX_TBL tblUseZone[] =
{
   "220 ", "MH",
   "210 ", "RR",
   "330 ", "AE",
   "240 ", "MG",
   "N/A ", "N/A",
   "171 ", "MR",
   "161 ", "SFR 9K",
   "141 ", "SFR 1/2",
   "220B", "MH",
   "320 ", "MP",
   "211A", "RR/PDZ",
   "167E", "GC",
   "150 ", "GHTPA",
   "165 ", "GC",
   "162 ", "SFR 1/2",
   "220C", "MH",
   "160B", "AO",
   "310 ", "GF",
   "230 ", "MT",
   "120 ", "BJTPA",
   "125 ", "BVTPA",
   "115 ", "HTPA",
   "600 ", "CVTPA",
   "231 ", "MT",
   "360 ", "PS",
   "230G", "MT",
   "163 ", "MFR",
   "130A", "CC",
   "134A", "MDR",
   "320B", "MP",
   "240D", "MG",
   "174A", "MR/FP",
   "164 ", "PO",
   "166 ", "LI",
   "310B", "GF",
   "250 ", "MG/MH",
   "167C", "SFR-9K",
   "330C", "AE/MH",
   "136B", "LC",
   "340 ", "IM",
   "150A", "BDO",
   "143 ", "SFR2 1/2",
   "160C", "PUD/SFR-9K",
   "168E", "MF",
   "350 ", "PD",
   "230H", "MT",
   "310D", "GF/TEZ",
   "167H", "PO",
   "168A", "SR-5",
   "221B", "MH/MG",
   "331B", "AE",
   "222 ", "MH/TEZ",
   "220E", "MH",
   "132A", "HDRO",
   "330B", "MH/AE",
   "165D", "PUD/GC",
   "162A", "SFR 1/2",
   "164C", "DRO",
   "610A", "PDZ/MG",
   "145 ", "MF",
   "180 ", "EPTPA",
   "210B", "RR/MG",
   "137 ", "HWYC",
   "168B", "SR-20",
   "230F", "MT",
   "146 ", "RC",
   "240A", "MG",
   "320A", "MG/MP",
   "242 ", "MG/RR",
   "220F", "MH/RR",
   "220G", "MH/AE/RR",
   "167 ", "P-QP",
   "172 ", "LC",
   "174 ", "FP",
   "142 ", "SFR 1",
   "331 ", "AE",
   "450 ", "CR",
   "450A", "CR",
   "210A", "RR/MH",
   "139F", "MF",
   "420 ", "NC-2",
   "430 ", "GC-1",
   "510 ", "M1",
   "161D", "SR-20/MH",
   "320F", "MP/MG",
   "168C", "GC/SFR-9K",
   "180B", "EPTPA/CR",
   "164B", "OWS",
   "165C", "SFR-9K/PO",
   "330A", "AE/MG",
   "331A", "AE/OWS",
   "210C", "RR/AO",
   "221 ", "MH/AO",
   "242A", "MG/AE",
   "230E", "MT",
   "220A", "MH/MT",
   "139D", "DRO",
   "132D", "MF",
   "161A", "SR 20/MG",
   "165B", "SR-20",
   "167F", "PO/GC",
   "170B", "MR/LC",
   "221A", "MH/SR-20",
   "140 ", "FCTPA",
   "130C", "CC",
   "131B", "SFR2 1/2",
   "132B", "MG",
   "132C", "DRO",
   "136A", "LC",
   "138 ", "IM",
   "161B", "LI/GC",
   "163B", "SR-5/MH",
   "165A", "OWS",
   "166E", "PO/SFR-9K",
   "167B", "SFR-9K",
   "168D", "SFR-9K/G",
   "221C", "MH/MG",
   "320C", "MP/AE",
   "320G", "MP/AO",
   "330E", "AE/AO",
   "331D", "AE/MH",
   "332 ", "AE/TEZ",
   "110 ", "TPA",
   "115A", "AG",
   "120A", "RR",
   "125A", "MR",
   "130 ", "CC",
   "130B", "CC",
   "131 ", "SFR 1/4",
   "131A", "SFR2 1/2",
   "131C", "SFR2 1/2",
   "131D", "SFR2 1/2",
   "132 ", "RR",
   "133 ", "MF",
   "133A", "MF",
   "133B", "MF/FP",
   "133C", "MH",
   "134 ", "MDR",
   "134B", "MDR",
   "135 ", "GC",
   "135A", "P/QP",
   "135B", "P/QP",
   "135C", "P/QP",
   "135D", "P/QP",
   "136 ", "LC",
   "136C", "LC/P/QP",
   "137A", "HSC/SFR",
   "139 ", "FP",
   "139A", "MDR/FP",
   "139B", "FP",
   "139C", "FP/SFR",
   "139E", "MDR",
   "140A", "MF/SFR 1/2",
   "140B", "RC/SFR 1",
   "140C", "SFR 1/RC",
   "144 ", "SFR",
   "147 ", "GF",
   "150B", "MT",
   "160 ", "MTBTPA",
   "160A", "MT",
   "161C", "PUD",
   "162B", "SFR 1/2/PO",
   "162C", "SFR 1/2/MF",
   "162D", "SR20/MH",
   "162E", "SR20/MH",
   "163A", "MH",
   "163C", "AE",
   "163D", "LI/OWS",
   "164A", "HDRO",
   "164D", "AE",
   "165E", "MF/GC",
   "166A", "MT",
   "166B", "MT",
   "166C", "SFR 1/2",
   "166D", "SFR 1/2",
   "166F", "MF/SFR-9K",
   "166G", "SFR-9K/MF",
   "167A", "GC/MF",
   "167D", "PQ-P",
   "167G", "GC/MF",
   "170 ", "WTPA",
   "173 ", "EP",
   "180A", "EPTPA",
   "210E", "RR",
   "211 ", "RR",
   "220D", "MH/MP",
   "220H", "MH/RR",
   "221D", "MH/AE",
   "222A", "MH",
   "230A", "MT/MH",
   "230B", "MT/CN-2",
   "230C", "MH/PD",
   "230D", "MH/GF",
   "230I", "MT/MH",
   "240B", "MG/MH",
   "240C", "MG/CR",
   "241 ", "MG/MT",
   "241A", "MG/MT",
   "241B", "MG/MH",
   "241C", "MG/RR/MH",
   "241D", "MG/MH/SR",
   "242B", "MG/TEZ",
   "242C", "MG/PS",
   "310A", "GF/MH",
   "310C", "GF",
   "320D", "MP/AE/MG",
   "320E", "MP",
   "320H", "MP/MH",
   "320I", "MP/PD",
   "321 ", "MP/MG",
   "330D", "AE",
   "331C", "AE",
   "340A", "IM/MG",
   "340B", "IM",
   "350A", "PD/MG",
   "350B", "PD",
   "350C", "PD",
   "350D", "PD",
   "350E", "PD/MT",
   "350F", "PD/PS",
   "351 ", "PD/TEZ",
   "360A", "PS",
   "360B", "AG PRES",
   "360C", "PS",
   "360D", "PS/PD",
   "400 ", "CN",
   "400A", "CN/MH",
   "410 ", "NC-1",
   "420A", "NC-2/MH",
   "440 ", "GC-2",
   "520 ", "M2",
   "610 ", "PLNDEV",
   "700 ", " ",
   "710 ", "ROAD",
   "715 ", "IMPR",
   "725 ", "POS INT",
   "", ""
};

// Tax Data
#define  T405_TXLREF    0
#define  T405_FRRY4     1
#define  T405_THRUY4    2
#define  T405_TXLCCD    3
#define  T405_TRA       4
#define  T405_APN       5
#define  T405_TXLCID    6
#define  T405_LMAD      7
#define  T405_VALDS1    8
#define  T405_VALAM1    9
#define  T405_VALEX1    10
#define  T405_VALDS2    11
#define  T405_VALAM2    12
#define  T405_VALEX2    13
#define  T405_VALDS3    14
#define  T405_VALAM3    15
#define  T405_VALEX3    16
#define  T405_VALDS4    17
#define  T405_VALAM4    18
#define  T405_VALEX4    19
#define  T405_VALDS5    20
#define  T405_VALAM5    21
#define  T405_VALEX5    22
#define  T405_VALDS6    23
#define  T405_VALAM6    24
#define  T405_VALEX6    25
#define  T405_VALDS7    26
#define  T405_VALAM7    27
#define  T405_VALEX7    28
#define  T405_VALDS8    29
#define  T405_VALAM8    30
#define  T405_VALEX8    31
#define  T405_GRVAL1    32
#define  T405_NTVAL     33
#define  T405_TXRATE    34
#define  T405_ENTCD1    35
#define  T405_ENTDE1    36
#define  T405_TAXAM1    37
#define  T405_ENTCD2    38
#define  T405_ENTDE2    39
#define  T405_TAXAM2    40
#define  T405_ENTCD3    41
#define  T405_ENTDE3    42
#define  T405_TAXAM3    43
#define  T405_ENTCD4    44
#define  T405_ENTDE4    45
#define  T405_TAXAM4    46
#define  T405_ENTCD5    47
#define  T405_ENTDE5    48
#define  T405_TAXAM5    49
#define  T405_ENTCD6    50
#define  T405_ENTDE6    51
#define  T405_TAXAM6    52
#define  T405_ENTCD7    53
#define  T405_ENTDE7    54
#define  T405_TAXAM7    55
#define  T405_ENTCD8    56
#define  T405_ENTDE8    57
#define  T405_TAXAM8    58
#define  T405_ENTCD9    59
#define  T405_ENTDE9    60
#define  T405_TAXAM9    61
#define  T405_ENTC10    62
#define  T405_ENTD10    63
#define  T405_TAXA10    64
#define  T405_ENTC11    65
#define  T405_ENTD11    66
#define  T405_TAXA11    67
#define  T405_ENTC12    68
#define  T405_ENTD12    69
#define  T405_TAXA12    70
#define  T405_ENTC13    71
#define  T405_ENTD13    72
#define  T405_TAXA13    73
#define  T405_ENTC14    74
#define  T405_ENTD14    75
#define  T405_TAXA14    76
#define  T405_ENTC15    77
#define  T405_ENTD15    78
#define  T405_TAXA15    79
#define  T405_ENTC16    80
#define  T405_ENTD16    81
#define  T405_TAXA16    82
#define  T405_ENTC17    83
#define  T405_ENTD17    84
#define  T405_TAXA17    85
#define  T405_DFTDTE    86
#define  T405_GRSTAX    87
#define  T405_TOEXMP    88
#define  T405_TOTDUE    89
#define  T405_PAYMNT    90
#define  T405_BALDUE    91
#define  T405_DUDTE1    92
#define  T405_DEDTX1    93
#define  T405_DUEAM1    94
#define  T405_DUDTE2    95
#define  T405_DEDTX2    96
#define  T405_DUEAM2    97
#define  T405_ASMOWN    98
#define  T405_LMOWNM    99
#define  T405_LMOWA2    100
#define  T405_LMOWA3    101
#define  T405_XXCSZ     102
#define  T405_PENFLG    103
#define  T405_DELFLG    104
#define  T405_ESCPYR    105
#define  T405_OWN2ND    106
#define  T405_COLS    107

// The file TX490 is the current roll unpaid. We send this file to the
//	tax services monthly.  The only problem here is that once paid in full 
//	they drop of the list completely.  The next file is TX 490 which is our delinquent file.
#define  T490_TXBILL    0
#define  T490_TXLK01    1
#define  T490_TXLK02    2
#define  T490_TXLK03    3
#define  T490_TXLK04    4
#define  T490_TXLK05    5
#define  T490_TXLK06    6
#define  T490_TXCUYR    7        // Tax year
#define  T490_TXACCR    8        // TRA
#define  T490_TXASNM    9        // ASSESSEE NAME
#define  T490_TXOWNM    10       // PREVIOUS OWNERS NAME
#define  T490_TXPROP    11       // Situs
#define  T490_TXOWA1    12       // Mail Addr
#define  T490_TXOWA2    13
#define  T490_TXOWCT    14
#define  T490_TXOWST    15
#define  T490_TXOWZP    16
#define  T490_TXLAND    17
#define  T490_TXIMPV    18
#define  T490_TXOTHR    19
#define  T490_TXHOME    20
#define  T490_TXOTRE    21
#define  T490_TXNETV    22
#define  T490_TXBTYP    23       // Bill type: S=Secured Bill
                                 // E = Escape bill
                                 // P = Other tax types (e.g. a supplemental tax that went unpaid for the 2015 year and was then transferred to the main account for the 2016 year)
#define  T490_TXSTIN    24       // seven digits number?
#define  T490_TX1DUE    25
#define  T490_TX1PNL    26
#define  T490_TX1DLM    27
#define  T490_TX1DLD    28
#define  T490_TX1DLY    29
#define  T490_TX1PMM    30
#define  T490_TX1PMD    31
#define  T490_TX1PMY    32
#define  T490_TX1PMA    33       // Paid Amt 1 - 9V2
#define  T490_TX2DUE    34
#define  T490_TX2PNL    35
#define  T490_TX2DLM    36
#define  T490_TX2DLD    37
#define  T490_TX2DLY    38
#define  T490_TX2PMM    39
#define  T490_TX2PMD    40
#define  T490_TX2PMY    41
#define  T490_TX2PMA    42
#define  T490_TXSAC1    43       // SPECIAL ASSESSMENT CODE
#define  T490_TXSPA1    44       // SPECIAL ASSESSMENT AMT
#define  T490_TXSAC2    45
#define  T490_TXSPA2    46
#define  T490_TXSAC3    47
#define  T490_TXSPA3    48
#define  T490_TXSAC4    49
#define  T490_TXSPA4    50
#define  T490_TXSAC5    51
#define  T490_TXSPA5    52
#define  T490_TXDLQA    53       // PRIOR YEARS DELQ.AMT
#define  T490_TXBKYI    54       // BANKRUPTCY INDICATOR
#define  T490_TXSENC    55
#define  T490_TXMIS1    56       // MISCELLANEOUS FEES
#define  T490_TXMIS2    57
#define  T490_TXMIS3    58
#define  T490_TXFOUR    59       // FOUR PAY PLAN INDICATOR
#define  T490_TXLCCD    60
#define  T490_TXSCLC    61
#define  T490_COLS      62

// Delinquent file (prior year)
#define  T491_TXBILL    0
#define  T491_TXLK01    1
#define  T491_TXLK02    2
#define  T491_TXLK03    3
#define  T491_TXLK04    4
#define  T491_TXLK05    5
#define  T491_TXLK06    6
#define  T491_TXASNM    7
#define  T491_TXLCCD    8
#define  T491_TXPBTP    9
#define  T491_TXEFYR    10    // EFFECTIVE TAX YEAR
#define  T491_TXYRDF    11    // YEAR OF DEFAULT
#define  T491_TXBSAM    12    // BASE AMOUNT DELINQUENT
#define  T491_TXDLIN    13    // DELINQUENT INSTALLMENT (total of base+fee+cost+penalty)
#define  T491_TXPNLT    14
#define  T491_TXCOST    15
#define  T491_TXREDE    16    // REDEMPTION FEE
#define  T491_TXMIFE    17    // MISCELLANEOUS FEE
#define  T491_TXFYDF    18    // FIRST YEAR OF DEFAULT
#define  T491_TXREMM    19
#define  T491_TXREDD    20
#define  T491_TXREYY    21
#define  T491_TXREAM    22
#define  T491_TXPPST    23    // PAY PLAN STATUS
#define  T491_TXREA1    24    // REDEMPTION AMOUNT DUE (T491_TXDLIN+interest)
#define  T491_TXREA2    25
#define  T491_TXREA3    26
#define  T491_TXREA4    27
#define  T491_TXREA5    28
#define  T491_TXREA6    29
#define  T491_TXREA7    30
#define  T491_TXREA8    31
#define  T491_TXREA9    32
#define  T491_TXRE10    33
#define  T491_TXRE11    34
#define  T491_TXRE12    35
#define  T491_TXPPCD    36
#define  T491_TXBKYI    37
#define  T491_TXBOND    38
#define  T491_COLS      39

// Supplemental file
#define  TSUP_TXBILL    0
#define  TSUP_TXLK01    1
#define  TSUP_TXLK02    2
#define  TSUP_TXLK03    3
#define  TSUP_TXLK04    4
#define  TSUP_TXLK05    5
#define  TSUP_TXLK06    6
#define  TSUP_TXCUYR    7
#define  TSUP_TXACCR    8
#define  TSUP_TXASNM    9
#define  TSUP_TXOWNM    10
#define  TSUP_TXPROP    11
#define  TSUP_TXOWA1    12
#define  TSUP_TXOWA2    13
#define  TSUP_TXOWCT    14
#define  TSUP_TXOWST    15
#define  TSUP_TXOWZP    16
#define  TSUP_TXLAND    17
#define  TSUP_TXIMPV    18
#define  TSUP_TXOTHR    19
#define  TSUP_TXHOME    20
#define  TSUP_TXOTRE    21
#define  TSUP_TXNETV    22
#define  TSUP_TXBTYP    23
#define  TSUP_TXSTIN    24
#define  TSUP_TX1DUE    25
#define  TSUP_TX1PNL    26
#define  TSUP_TX1DLM    27
#define  TSUP_TX1DLD    28
#define  TSUP_TX1DLY    29
#define  TSUP_TX1PMM    30
#define  TSUP_TX1PMD    31
#define  TSUP_TX1PMY    32
#define  TSUP_TX1PMA    33
#define  TSUP_TX2DUE    34
#define  TSUP_TX2PNL    35
#define  TSUP_TX2DLM    36
#define  TSUP_TX2DLD    37
#define  TSUP_TX2DLY    38
#define  TSUP_TX2PMM    39
#define  TSUP_TX2PMD    40
#define  TSUP_TX2PMY    41
#define  TSUP_TX2PMA    42
#define  TSUP_TXSAC1    43
#define  TSUP_TXSPA1    44
#define  TSUP_TXSAC2    45
#define  TSUP_TXSPA2    46
#define  TSUP_TXSAC3    47
#define  TSUP_TXSPA3    48
#define  TSUP_TXSAC4    49
#define  TSUP_TXSPA4    50
#define  TSUP_TXSAC5    51
#define  TSUP_TXSPA5    52
#define  TSUP_TXDLQA    53
#define  TSUP_TXBKYI    54
#define  TSUP_TXSENC    55
#define  TSUP_TXMIS1    56
#define  TSUP_TXMIS2    57
#define  TSUP_TXMIS3    58
#define  TSUP_TXFOUR    59
#define  TSUP_TXLCCD    60
#define  TSUP_TXSCLC    61
#define  TSUP_COLS      62

#endif