/******************************************************************************
 *
 * 05/16/2008 Fix street name issue where the name is longer than space allowed.
 * 07/29/2008 Fix ParseAdr1_1() when pAdr has only one token.
 * 11/01/2009 Add parseAdr1S_1() for CAL
 * 11/10/2010 Increase buffer size for acTmp and acSfx to avoid buffer overflow.
 * 11/16/2010 Fix parseAdr1_1().
 * 12/19/2010 Fix parseMAdr1().  If strSub is too long, drop it.
 * 04/18/2011 Fix bug in locateRecInR01() by returning success only when exact match.
 * 05/03/2011 Fix parseMAdr1_3() for MER
 * 07/12/2011 Modify parseMAdr1() to check "LOT" for MRN, add parseMAdr1_4() for HUM.
 * 07/25/2011 Fix bug in parseMAdr1_3() for SBT where street name likes "ST REGIS DR"
 *            was parsed incorrectly with "ST" as suffix instead of "DR".
 * 10/03/2011 Fix parseAdr2() to ignore last token if it contains only one char.
 * 06/03/2013 Add extrZoning() to extract ZONING
 * 07/30/2013 Add updateCareOf()
 * 10/02/2013 Add removeNames()
 * 10/13/2013 Add removeSitus(), removeMailing(), and removeLegal().
 * 10/23/2013 Modify parseAdr1_2() to check for StrSub as part of StrNum (ALA).
 * 10/24/2013 Fix parseAdr1() and parseAdr1_2() for special case 460 B TAYLOR AVE
 * 11/28/2013 Remove leading zero of StrNum in parseAdr1C().
 * 07/28/2014 Fix removeSitus() & removeMailing()
 * 08/07/2014 Fix parseAdr1C_1() to correct formatting of "CO RD"
 * 09/09/2014 Add getNameDir() to translate letter direction to word direction.
 * 03/13/2015 Add updateDocLinks() from LoadOne.cpp
 * 07/10/2015 Fix bug in parseAdrND()
 * 07/18/2015 Add updateSitusCity() to copy city & zip from mailing to situs 
 *            if strname & strnum are the same.
 * 07/28/2015 Modify updateCareOf() to remove extra C/O in CareOf.
 * 08/25/2015 Modify parseMAdr1() to return SfxCode.  Add MergeArea() to merge lot area from GIS basemap.
 * 08/28/2015 Add isDir() to check for street direction and adjust parseAdr1U()
 * 09/11/2015 Modify MergeArea() to allow overwrite existing value
 * 10/02/2015 Modify parseAdr2() to break Zip and Zip4. - SBD
 * 11/12/2015 Add parseAdr1C_2() for SFX.
 * 11/13/2015 Fix parseAdr1C_2() in case unit number is put in the wrong place. i.e. "#1 MEACHAM PLACE" or "530 #1 SANCHEZ ST"
 * 12/21/2015 Fix bug in parseAdr2() when Zip code len is > 5 but < 9 (NAP)
 * 02/15/2016 Modify updateLegal() to remove legal before updating.
 * 02/16/2016 Modify removeNames() to add option to remove DBA.  Default TRUE.
 * 02/19/2016 Modify removeMailing() to add option to remove DBA.  Default FALSE.
 * 07/13/2016 Modify updateCareOf() to ignore C/O which is < 4 chars
 * 08/19/2016 Add updatePrevApn() to copy PREV_APN from X01 file to S01 file.
 * 09/24/2016 Modify parseAdr1() to support UNIT & APT (ORG)
 * 11/01/2016 Bug fix in parseAdr1_5() for MPA
 * 07/09/2017 Modify mergeXSitusRec() to update city/st only when street name was already there.
 * 07/29/2017 Modify updatePrevApn() to rename output file to S01.
 * 05/07/2018 Modify parseAdrND() CCX to remove post direction.
 * 07/20/2018 Add bAddSign to make adding '#' optional.
 *            Modify updatePrevApn() to take P01 as original input instead of X01.
 * 07/23/2018 Modify parseAdr1() & mergeXSitusRec() to update HseNo. Modify mergeXSitusFile() to add log msg
 * 08/02/2018 Modify updatePrevApn() to update R01 if it's avail, otherwise update S01.
 * 09/15/2018 Modify parseAdr1N_1() to keep suffix as is if there is post direction as in "TAMARACK ROAD EAST"
 * 05/28/2019 Add removeZoning().  Modify removeSitus() and removeMailing() to add UNITNOX
 * 06/30/2019 Fix bug in parseAdr1_1() to limit street name to it's fix length SIZ_M_STREET.
 * 07/03/2019 Modify parseAdrNSD() to check for Unit#
 * 07/24/2019 Modify updateLegal() to remove '\'
 * 07/31/2019 Modify updatePrevApn() to populate new parcel PREV_APN with current APN
 * 08/01/2019 Add parseStreet() & parseCitySt() from Tables.cpp
 * 08/08/2019 Modify updateCareOf() to set default iMaxLen=0
 * 08/09/2019 Modify updateCareOf() to remove Json sensitive char before apply CareOf
 * 08/21/2019 Add UPD_PREVAPN_SJX and modify updatePrevApn() to update special case for SJX.
 * 10/20/2019 Modify updateLegal() to fix problem when legal has blank prefix.
 * 07/01/2020 Fix bug in updateCareOf() that may mess up APN.
 * 07/18/2020 Add isDirx()
 * 07/19/2020 Update asDir[] to handle more variation of direction.
 *            Modify parseAdr1(), parseAdr1N(), parseAdr1N_1(), parseAdr1_2(), parseMAdr1_3(), parseAdr1_4(), parseMAdr1()
 *            and all functions using asDir[].
 * 07/20/2020 Modify removeZoning() to remove PQ Zoning fields only.
 * 08/25/2020 Modify parseMAdr1() to remove space after '#'
 * 01/17/2021 Modify updateDocLinks() to support LAX.
 * 07/02/2021 Remove '-' from zipcode in parseMAdr2().
 * 03/14/2022 Modify removeZoning() to remove PQZoningType.
 * 03/30/2022 Add removeDocLink()
 * 04/15/2022 Add removeLdrValue() to remove all value fields (SAC)
 * 05/03/2022 Add removeUseCode()
 * 09/18/2022 Modify parseMAdr1_3() to add special case for DNX where BOX # follows address.
 * 11/27/2022 Modify parseAdr1C() to handle "123 N HWY 20" for MEN where we want to keep file name as "HWY 20".
 * 10/11/2023 Modify updateDocLinks() to stop populating DocLinks
 * 01/12/2024 Modify all address parsing functions to populate UnitNox in ADR_REC.
 * 02/01/2024 Modify parseAdr1(), parseAdr1S() to fix UnitNox bug.
 * 05/15/2024 Modify parseMAdr1_1() Populate UnitNox when value not fit in Unit for PLA  
 *            "394 PACIFIC AVE 2ND FLR" & "1300 EVANS AVE #880154".
 *            Parse UnitNo even  when there is no StrNum - "THREE LAGOON DR SUITE 400"
 * 05/20/2024 Modify parseMAdr1_2() to populate HseNo with original StrNum.
 * 08/03/2024 Add parseMAdr1_5() to parse situs for SUT.
 * 08/22/2024 Modify removeMailing() to remove M_CITYX
 * 08/30/2024 Modify parseMAdr2() to remove limit city name at SIZ_M_CITY.
 * 10/30/2024 Update removeZoning() to also remove ZONE_DESC & ZONE_JURIS
 * 11/16/2024 Modify parseAdr1C_2() to remove suffix if it's has post direction - SFX
 * 02/04/2025 Modify parseMAdr1_2() to remove hyphen at the end of HseNo.
 *
 ******************************************************************************/

#include "stdafx.h"
#include "Prodlib.h"
#include "Utils.h"
#include "Logs.h"
#include "R01.h"
#include "Tables.h"
#include "DoSort.h"

char *asDir[]=
{
   "N", "S", "E", "W", "N ", "S ", "E ", "W ", "NE", "NW", "SE", "SW", NULL
};

char *asDirx[]=
{
   "N ", "S ", "E ", "W ", "NE", "NW", "SE", "SW", NULL
};

char *asDir1[]=
{
   "NORTH", "SOUTH", "EAST", "WEST", NULL
};

char *asDir2[]=
{
   "NO", "SO", NULL
};

FILE     *fdExtr;
long     lAdrSkip, lAdrMatch;

extern   int   iRecLen, iApnLen;
extern   bool  bEnCode, bDebug;
extern   long  lLastRecDate, lToday;

#define  UPD_PREVAPN_SJX \
   if (!memcmp(acS01Rec, "910", 3)) \
      iTmp = sprintf(sTmp, "70%.6s", &acS01Rec[OFF_APN_S]+3); \
   else if (!memcmp(acS01Rec, "920", 3)) \
      iTmp = sprintf(sTmp, "80%.6s", &acS01Rec[OFF_APN_S]+3); \
   else if (!memcmp(acS01Rec, "930", 3)) \
      iTmp = sprintf(sTmp, "000%.5s", &acS01Rec[OFF_APN_S]+3); \
   else if (!memcmp(acS01Rec, "9402", 4) || !memcmp(acS01Rec, "9403", 4)) \
      iTmp = sprintf(sTmp, "000%.5s", &acS01Rec[OFF_APN_S]+3); \
   else \
      iTmp = sprintf(sTmp, "%.8s", &acS01Rec[OFF_APN_S]); \
   memcpy(&acS01Rec[OFF_PREV_APN], sTmp, iTmp);

/********************************* isDir() **********************************
 *
 * Check input buffer for street direction.  
 * Return direction in pDir buffer if provided and set return to true, else false
 *
 ****************************************************************************/

bool isDir(char *pInbuf, char *pDir)
{
   bool bRet=false;
   int iTmp = 0;

   while (asDir[iTmp])
   {
      if (!strcmp(pInbuf, asDir[iTmp]))
      {
         bRet = true;
         if (pDir)
            strcpy(pDir, asDir[iTmp]);
         break;
      }
      iTmp++;
   }

   return bRet;
}

/********************************* isDirx() **********************************
 *
 * Check input buffer for street direction.  Compare two bytes only.
 * Return direction in pDir buffer if provided and set return to true, else false
 *
 ****************************************************************************/

bool isDirx(char *pInbuf, char *pDir)
{
   bool bRet=false;
   int iTmp = 0;

   while (asDirx[iTmp])
   {
      if (!memcmp(pInbuf, asDirx[iTmp], 2))
      {
         bRet = true;
         if (pDir)
            strcpy(pDir, asDirx[iTmp]);
         break;
      }
      iTmp++;
   }

   return bRet;
}

bool isDir1(char *pInbuf, char *pDir)
{
   bool bRet=false;
   int iTmp = 0;

   while (asDir1[iTmp])
   {
      if (!strcmp(pInbuf, asDir1[iTmp]))
      {
         bRet = true;
         if (pDir)
            strcpy(pDir, asDir[iTmp]);    // Copy the abbr version only
         break;
      }
      iTmp++;
   }

   return bRet;
}

bool isDir2(char *pInbuf, char *pDir)
{
   bool bRet=false;
   int iTmp = 0;

   while (asDir2[iTmp])
   {
      if (!strcmp(pInbuf, asDir2[iTmp]))
      {
         bRet = true;
         if (pDir)
            strcpy(pDir, asDir[iTmp]);
         break;
      }
      iTmp++;
   }

   return bRet;
}

/********************************* mergeOwners() ****************************
 *
 * Parse pOwners into Name1, Name2, and SwapName
 *
 ****************************************************************************/

void parseOwners(char *pOwners, char *pOwner1, char *pOwner2, char *pSwapName)
{
   char     acOF[52], acOL[52], acOM[52], acSF[52], acSM[52], acSL[52];
   char     acSpouse[64];

   // Initialize output names
   *pOwner2 = 0;
   *pSwapName = 0;

   // Parse names
   ParseOwnerName(pOwners, acOF, acOM, acOL, acSF, acSM, acSL);

   strcpy(pSwapName, acOF);
   if (acOM[0])
   {
      strcat(pSwapName, " ");
      strcat(pSwapName, acOM);
   }

   // If spouse last name avail, break into name1 and name2
   if (acSL[0])
   {
      strcat(pSwapName, " ");
      strcat(pSwapName, acOL);

      // Make owner2
      sprintf(pOwner2, "%s %s", acSL, acSF);
      if (acSM[0])
      {
         strcat(pOwner2, " ");
         strcat(pOwner2, acSM);
      }

      // Make owner1
      sprintf(pOwner1, "%s %s", acOL, acOF);
      if (acOM[0])
      {
         strcat(pOwner1, " ");
         strcat(pOwner1, acOM);
      }
   } else
   {
      // Make spouse name is exist
      acSpouse[0] = 0;
      if (acSF[0])
      {
         sprintf(acSpouse, " & %s", acSF);
         if (acSM[0])
         {
            strcat(acSpouse, " ");
            strcat(acSpouse, acSM);
         }
      }

      if (acOL[0])
      {
         // Make Owner1
         if (acOM[0])
            sprintf(pOwner1, "%s %s %s%s", acOL, acOF, acOM, acSpouse);
         else
            sprintf(pOwner1, "%s %s%s", acOL, acOF, acSpouse);

         // Add spouse to swap name
         strcat(acSpouse, " ");
         strcat(acSpouse, acOL);
         strcat(pSwapName, acSpouse);
      } else
      {
         strcpy(pSwapName, pOwners);
         strcpy(pOwner1, pOwners);
      }
   }
}

/********************************** parseAdr1() *****************************
 *
 * Parse address line 1 into individual elements.  If no street number, no parse.
 * This function is used to parse mailing address.   
 * Used in ALA, DNX, GLE, ORG, SCR, SLO.
 *
 * 08/21/2009 Add check for Unit# in street name.
 * 04/08/2011 Check for Unit# comes after House#
 * 08/11/2011 If StrSub > SIZ_M_STR_SUB, it's more likely bad addr format.
 *            Copy full addr to StrName and return if StrNum = 0
 * 09/16/2016 Modify for ORG M_Addr
 * 07/10/2017 Modify parseAdr1() to return SfxCode too for ORG.
 *
 ****************************************************************************/

void parseAdr1(ADR_REC *pAdrRec, char *pAdr)
{
   char  acAdr[256], acStr[64], acUnit[32], acTmp[64], *apItems[16], *pTmp, *pTmp1;
   int   iCnt, iTmp, iIdx;
   bool  bRet;

   // Clear output buffer
   memset((void *)pAdrRec, 0, sizeof(ADR_REC));

   // Remove dot in address line 1
   strcpy(acAdr, pAdr);
   remChar(acAdr, '.');

   // Check for PO Box
   if (pTmp = strstr(acAdr, "BOX "))
   {
      if ((pTmp - acAdr) < 10)
      {
         strcpy(pAdrRec->strName, acAdr);
         return;
      }

      // DNX: 16340 LOWER HARBOR RD BOX 112
      *pTmp = 0;
      strcpy(acUnit, pTmp+4);
   }

   // Check for Suite
   acUnit[0] = 0;
   if (pTmp = strstr(acAdr, " SUITE "))
   {
      *(pTmp) = 0;
      strcpy(acUnit, pTmp+7);
   } else if (pTmp = strstr(acAdr, " STE "))
   {
      *(pTmp) = 0;
      strcpy(acUnit, pTmp+5);
   } else if (pTmp = strstr(acAdr, " UNIT "))
   {
      *(pTmp) = 0;
      strcpy(acUnit, pTmp+6);
   } else if (pTmp = strstr(acAdr, " APT "))
   {
      *(pTmp) = 0;
      strcpy(acUnit, pTmp+5);
   } else if (pTmp = strstr(acAdr, " #"))
   {
      // Check for # as 2nd token i.e. 123 #B Main ST
      pTmp1 = strchr(acAdr, ' ');
      if (pTmp1 == pTmp)
      {
         pTmp++;
         if (pTmp1 = strchr(pTmp+2, ' '))
         {
            *pTmp1++ = 0;
            strcpy(acUnit, pTmp);
            while (*pTmp)
               *pTmp++ = ' ';
            *pTmp = ' ';
            blankRem(acAdr);
         }
      } else
      {
         *pTmp = 0;
         if (*(pTmp+2) == ' ')
            strcpy(acUnit, pTmp+3);
         else
            strcpy(acUnit, pTmp+2);
      }
   }

   // Prevent Unit overwrites buffer
   if (acUnit[0] > ' ') 
   {
      strcpy(pAdrRec->UnitNox, acUnit);
      if (strlen(acUnit) > SIZ_M_UNITNO &&  (pTmp = strchr(acUnit, ' ')))
      {
         remChar(pAdrRec->UnitNox, ' ');
         *pTmp = 0;
      }
      acUnit[SIZ_M_UNITNO] = 0;
      strcpy(pAdrRec->Unit, acUnit);
   }

   iCnt = ParseString(acAdr, ' ', 16, apItems);

   // If strNum is 0, do not parse address
   iTmp = atoi(apItems[0]);
   if (iTmp == 0 || iCnt == 1)
   {
      strcpy(pAdrRec->strName, pAdr);
      pAdrRec->lStrNum = 0;
   } else
   {
      iIdx = 0;
      acStr[0] = 0;
      
      // 460 B TAYLOR AVE (ALA)
      //if (apItems[0][strlen(apItems[0])-1] <= '9')
      if (isdigit(*apItems[0]))
      {
         strncpy(pAdrRec->HseNo, apItems[0], SIZ_S_HSENO);
         pAdrRec->lStrNum = iTmp;
         if (pTmp = strchr(apItems[0], '-'))
         {
            strcpy(pAdrRec->strSub, pTmp+1);
            *pTmp = 0;
         }
         strcpy(pAdrRec->strNum, apItems[0]);
         iIdx++;
      }

      // StrSub
      if (apItems[iIdx])
      {
         pTmp = strchr(apItems[iIdx], '/');
         if (pTmp)
         {
            if (strlen(apItems[iIdx]) <= SIZ_M_STR_SUB)
               strcpy(pAdrRec->strSub, apItems[iIdx++]);
            else
            {
               // Bad addr form
               if (pAdrRec->lStrNum == 0)
               {
                  strncpy(pAdrRec->strName, pAdr, SIZ_M_STREET);
                  return;
               }
            }
         }
      }

      // Dir
      //iTmp = 0;
      //while (asDir[iTmp])
      //{
      //   if (!strcmp(apItems[iIdx], asDir[iTmp]))
      //      break;
      //   iTmp++;
      //}
      bool bDir;
      bDir = isDir(apItems[iIdx]);

      // Possible cases E ST or W RD
      // Check for suffix
      bRet = false;
      //if (iTmp < 7 && iCnt > 2)
      if (bDir && iCnt > 2)
      {
         // If next item is suffix, then this one is not direction
         if (iTmp=GetSfxCode(apItems[iIdx+1]) && iCnt==iIdx+2)
         {
            strcpy(acStr, apItems[iIdx]);
            strcpy(pAdrRec->strSfx, GetSfxStr(iTmp));
            sprintf(pAdrRec->SfxCode, "%d", iTmp);
            bRet = true;         // Done
         } else
         {
            // This is direction
            strcpy(pAdrRec->strDir, apItems[iIdx]);
            iIdx++;
         }
      }

      if (!bRet)
      {
         // Not direction, token is str name
         acTmp[0] = 0;

         // Check for suffix
         if (iTmp=GetSfxCode(apItems[iCnt-1]))
         {
            sprintf(pAdrRec->SfxCode, "%d", iTmp);
            // Copy standard suffix
            strcpy(pAdrRec->strSfx, GetSfxStr(iTmp));
            while (iIdx < iCnt-1)
            {
               strcat(acStr, apItems[iIdx++]);
               if (iIdx < iCnt)
                  strcat(acStr, " ");
            }
         } else
         {
            while (iIdx < iCnt)
            {
               strcat(acStr, apItems[iIdx++]);
               if (iIdx < iCnt)
                  strcat(acStr, " ");
            }
         }
      }
      acStr[SIZ_M_STREET] = 0;
      strcpy(pAdrRec->strName, acStr);
   }
}

/******************************** parseAdr1C() ******************************
 *
 * Parse address line 1 into individual elements.  If no street number, parse
 * street name and suffix.  Suffix will be coded
 * Use in ALP, IMP, STA
 *
 * 06/27/2012 Add HSENO to output record.
 *
 ****************************************************************************/

void parseAdr1C(ADR_REC *pAdrRec, char *pAdr)
{
   char  acAdr[128], acStr[64], acTmp[64], *apItems[16], *pDir;
   int   iCnt, iTmp, iIdx, iSfxCode;
   bool  bRet, bHwy=false;

   // Clear output buffer
   memset((void *)pAdrRec, 0, sizeof(ADR_REC));

   // Remove dot and double space in address line 1
   iIdx = 0;
   while (*pAdr)
   {
      // Also remove single quote
      if (*pAdr != '.' && *pAdr != 39)
         acAdr[iIdx++] = *pAdr;

      // Remove multi-space
      if (*pAdr == ' ')
         while (*pAdr == ' ') pAdr++;
      else
         pAdr++;
   }
   acAdr[iIdx++] = 0;

   // Check for PO Box
   //if (!memcmp(acAdr, "PO BOX", 6) || !memcmp(acAdr, "P O ", 4))
   if (strstr(acAdr, "BOX "))
   {
      strcpy(pAdrRec->strName, acAdr);
      return;
   }

   if (strstr(acAdr, "HWY "))
      bHwy = true;

   iCnt = ParseString(acAdr, ' ', 16, apItems);
   if (iCnt < 2)
   {
      strcpy(pAdrRec->strName, acAdr);
      return;
   }

   // Check for street number
   iIdx = 0;
   if (isNumber(apItems[0]))
   {
      iIdx++;
      iTmp = atoi(apItems[0]);
      pAdrRec->lStrNum = iTmp;
      sprintf(acTmp, "%d", iTmp);
      strcpy(pAdrRec->strNum, acTmp);
      strcpy(pAdrRec->HseNo, apItems[0]);
   }

   // Dir
   pDir = GetStrDir(apItems[iIdx]);

   // Possible cases E ST or W RD
   // Check for suffix
   bRet = false;
   if (pDir && iCnt > 2)
   {
      // If next item is suffix, then this one might not be direction
      // ex: 123 S AVE B   --> S is direction and AVE B is street name
      //     321 N AVE     --> N is not a direction
      iSfxCode = GetSfxDev(apItems[iIdx+1]);
      if (iSfxCode && iCnt==iIdx+2)
      {
         strcpy(pAdrRec->strName, apItems[iIdx]);
         sprintf(pAdrRec->strSfx, "%d", iSfxCode);
         bRet = true;         // Done
      } else
      {
         // This is direction
         strcpy(pAdrRec->strDir, pDir);
         iIdx++;
      }
   }

   if (!bRet)
   {
      // Not direction, token is str name
      acTmp[0] = 0;
      acStr[0] = 0;

      // Check for unit #
      if (apItems[iCnt-1][0] == '#')
      {
         strncpy(pAdrRec->Unit, apItems[iCnt-1], SIZ_M_UNITNO);
         strncpy(pAdrRec->UnitNox, apItems[iCnt-1], SIZ_M_UNITNOX);
         iCnt--;

         // If last token is STE or SUITE, remove it.
         if (!strcmp(apItems[iCnt-1], "STE") || !strcmp(apItems[iCnt-1], "SUITE")
            || !strcmp(apItems[iCnt-1], "UNIT") || !strcmp(apItems[iCnt-1], "APT"))
            iCnt--;
      } else if (apItems[iCnt-2][0] == '#' 
         || !strcmp(apItems[iCnt-2], "STE") || !strcmp(apItems[iCnt-2], "SUITE")
         || !strcmp(apItems[iCnt-2], "UNIT") || !strcmp(apItems[iCnt-2], "APT"))
      {
         pAdrRec->Unit[0] = '#';
         pAdrRec->UnitNox[0] = '#';
         strncpy((char *)&pAdrRec->Unit[1], apItems[iCnt-1], SIZ_M_UNITNO-1);
         strncpy((char *)&pAdrRec->UnitNox[1], apItems[iCnt-1], SIZ_M_UNITNOX-1);
         iCnt -= 2;
      } else if (!bHwy && isNumber(apItems[iCnt-1]))
      {
         iTmp = atoi(apItems[iCnt-1]);
         if (iTmp > 0)
         {
            pAdrRec->Unit[0] = '#';
            pAdrRec->UnitNox[0] = '#';
            strncpy((char *)&pAdrRec->Unit[1], apItems[iCnt-1], SIZ_M_UNITNO-1);
            strncpy((char *)&pAdrRec->UnitNox[1], apItems[iCnt-1], SIZ_M_UNITNOX-1);
         }
         iCnt--;
      }

      if (!bHwy)
      {
         if (iSfxCode = GetSfxCodeX(apItems[iCnt-1], acTmp) )
         {
            sprintf(pAdrRec->strSfx, "%d", iSfxCode);
            strcpy(pAdrRec->SfxName, acTmp);
            iCnt--;
         } else if (iCnt > 2 && (iSfxCode = GetSfxCodeX(apItems[iCnt-2], acTmp)))
         {
            sprintf(pAdrRec->strSfx, "%d", iSfxCode);
            strcpy(pAdrRec->SfxName, acTmp);
            pAdrRec->Unit[0] = '#';
            pAdrRec->UnitNox[0] = '#';
            strncpy((char *)&pAdrRec->Unit[1], apItems[iCnt-1], SIZ_M_UNITNO-1);
            strncpy((char *)&pAdrRec->UnitNox[1], apItems[iCnt-1], SIZ_M_UNITNOX-1);
            iCnt -= 2;
         }
      }

      // Multi-word street name
      while (iIdx < iCnt)
      {
         // Skip range number
         if (*(apItems[iIdx]) == '-')
         {
            if (iIdx < iCnt-1)
            {
               iIdx++;
               if (isNumber(apItems[iIdx]))
               {
                  if (iIdx < iCnt-1)
                     iIdx++;
               }
            }
         }

         strcat(acStr, apItems[iIdx++]);
         if (iIdx < iCnt)
            strcat(acStr, " ");
      }
      acStr[SIZ_M_STREET] = 0;
      strcpy(pAdrRec->strName, acStr);
   }
}

/******************************** parseAdr1C_1() ******************************
 *
 * Parse address line 1 into individual elements.  If no street number, parse
 * street name and suffix.  Suffix will be coded.  For GLE
 *
 ****************************************************************************/

void parseAdr1C_1(ADR_REC *pAdrRec, char *pAdr)
{
   char  acAdr[128], acTmp[64], *apItems[16], *pDir, *pTmp;
   int   iCnt, iTmp, iIdx, iSfxCode, iSaveIdx=0;;
   bool  bRet;

   // Clear output buffer
   memset((void *)pAdrRec, 0, sizeof(ADR_REC));

   // Remove dot and double space in address line 1
   iIdx = 0;
   while (*pAdr)
   {
      // Also remove single quote
      if (*pAdr != '.' && *pAdr != 39)
         acAdr[iIdx++] = *pAdr;

      // Remove multi-space
      if (*pAdr == ' ')
         while (*pAdr == ' ') pAdr++;
      else
         pAdr++;
   }
   acAdr[iIdx++] = 0;

   // Check for PO Box
   if ((strstr(acAdr, "BOX ")) ||
       (strstr(acAdr, "HWY ")) ||
       (strstr(acAdr, "CO RD ")) ||
       (strstr(acAdr, "HIGHWAY ")) ||
       !memcmp(acAdr, "RD ", 3)
      )
   {
      strncpy(pAdrRec->strName, acAdr, SIZ_M_STREET);
      return;
   }

   iCnt = ParseString(acAdr, ' ', 16, apItems);
   if (iCnt < 2)
   {
      strncpy(pAdrRec->strName, acAdr, SIZ_M_STREET);
      return;
   }

   // Check for street number
   iIdx = 0;
   if (isNumber(apItems[0]))
   {
      iIdx++;
      iTmp = atoi(apItems[0]);
      pAdrRec->lStrNum = iTmp;
      strcpy(pAdrRec->strNum, apItems[0]);
   }

   // Dir
   pDir = GetStrDir(apItems[iIdx]);

   // Possible cases E ST or W RD
   // Check for suffix
   bRet = false;
   if (pDir && iCnt > 1)
   {
      // If next item is suffix, then this one might not be direction
      // ex: 123 S AVE B   --> S is direction and AVE B is street name
      //     321 N AVE     --> N is not a direction
      iSfxCode = GetSfxDev(apItems[iIdx+1]);
      if (iSfxCode && iCnt==iIdx+2)
      {
         strncpy(pAdrRec->strName, apItems[iIdx], SIZ_M_STREET);
         sprintf(pAdrRec->strSfx, "%d", iSfxCode);
         bRet = true;         // Done
      } else
      {
         // This is direction
         strcpy(pAdrRec->strDir, pDir);
         iIdx++;
      }
   }

   if (!bRet)
   {
      // Not direction, token is str name
      acTmp[0] = 0;
      iIdx=0;

      // Check for unit #
      while (iCnt > iIdx)
      {
         pDir = GetStrDir(apItems[iIdx]);
         if (pDir)
            iIdx++;
         else
         {
            if ((iIdx > 0) && (iSfxCode = GetSfxCodeX(apItems[iIdx], acTmp)))
               break;
            else if (!strcmp(apItems[iIdx], "STE") || !strcmp(apItems[iIdx], "SUITE") ||
                     !strcmp(apItems[iIdx], "UNIT") || !strcmp(apItems[iIdx], "APT"))
                     iIdx += 2;
            else if (apItems[iIdx][0] == '#')
                  iIdx++;
            else if ((pTmp=strchr(apItems[iIdx], '-')) ||
                       (pTmp=strchr(apItems[iIdx], '/')) ||
                       (pTmp=strchr(apItems[iIdx], '&'))  )
            {
               if (*(pTmp-1) >= 'A')
                  iIdx++;
            } else if (isNumber(apItems[iIdx]) == false)
            {
               if (strlen(apItems[iIdx]) == 1)
                  iIdx++;
               else
               {
                  if ((iIdx - iSaveIdx) <= 1)
                  {
                     strcat(pAdrRec->strName, apItems[iIdx]);
                     iSaveIdx = iIdx;
                  }
                  iIdx++;
                  if (iIdx > 0)
                     strcat(pAdrRec->strName, " ");
               }
            } else
               iIdx++;
         }
      }

      // Multi-word street name
      while (iCnt > 0)
      {
         // Skip range number
         if (*(apItems[iCnt-1]) == '-')
         {
            iCnt--;
            if (isNumber(apItems[iCnt-1]))
               iCnt--;
            if (iCnt == 0)
               break;
         }

         pDir = GetStrDir(apItems[iCnt-1]);
         if (pDir)
         {
            strcpy(pAdrRec->strDir, pDir);
            iCnt--;
         }
         else
         {
            if (apItems[iCnt-1][0] == '#')
            {
               strncpy(pAdrRec->Unit, apItems[iCnt-1], SIZ_M_UNITNO);
               strncpy(pAdrRec->UnitNox, apItems[iCnt-1], SIZ_M_UNITNOX);
               iCnt--;

               // If last token is STE or SUITE, remove it.
               //if (!strcmp(apItems[iCnt-1], "STE") || !strcmp(apItems[iCnt-1], "SUITE")
               // || !strcmp(apItems[iCnt-1], "UNIT") || !strcmp(apItems[iCnt-1], "APT"))
               // iCnt--;
            } else if (!strcmp(apItems[iCnt-1], "STE") || !strcmp(apItems[iCnt-1], "SUITE") ||
                       !strcmp(apItems[iCnt-1], "UNIT") || !strcmp(apItems[iCnt-1], "APT"))
            {
               pAdrRec->Unit[0] = '#';
               strncpy((char *)&pAdrRec->Unit[1], apItems[iCnt-2], SIZ_M_UNITNO-1);
               pAdrRec->UnitNox[0] = '#';
               strncpy((char *)&pAdrRec->UnitNox[1], apItems[iCnt-2], SIZ_M_UNITNOX-1);
               iCnt -= 2;
            } else if (isNumber(apItems[iCnt-1]))
            {
               pAdrRec->Unit[0] = '#';
               strncpy((char *)&pAdrRec->Unit[1], apItems[iCnt-1], SIZ_M_UNITNO-1);
               pAdrRec->UnitNox[0] = '#';
               strncpy((char *)&pAdrRec->UnitNox[1], apItems[iCnt-1], SIZ_M_UNITNOX-1);
               iCnt--;
            } else if ((iCnt > 0) && (iSfxCode = GetSfxCodeX(apItems[iCnt-1], acTmp)))
            {
               sprintf(pAdrRec->strSfx, "%d", iSfxCode);
               iCnt--;
            } else if ((pTmp=strchr(apItems[iCnt-1], '-')) ||
                        (pTmp=strchr(apItems[iCnt-1], '/')) ||
                        (pTmp=strchr(apItems[iCnt-1], '&'))  )
            {
               if (*(pTmp-1) >= 'A')
               {
                  pTmp--;
                  strcpy(pAdrRec->strSub, pTmp);
                  iCnt--;
               }
            } else if (isNumber(apItems[iCnt-1]) == false)
            {
               if (strlen(apItems[iCnt-1]) == 1)
               {
                  pAdrRec->Unit[0] = '#';
                  strncpy((char *)&pAdrRec->Unit[1], apItems[iCnt-1], SIZ_M_UNITNO-1);
                  pAdrRec->UnitNox[0] = '#';
                  strncpy((char *)&pAdrRec->UnitNox[1], apItems[iCnt-1], SIZ_M_UNITNOX-1);
                  iCnt--;
               } else if (pAdrRec->strName[0] < '0')
                  strncpy(pAdrRec->strName, apItems[iCnt-1], SIZ_M_STREET);
               else
               {
                  //strcat(pAdrRec->strName, apItems[iCnt-1]);
                  iCnt--;
                  //if (iCnt > 0)
                  // strcat(pAdrRec->strName, " ");
               }
            } else
               iCnt--;
         }
      }
   }

   // Check for streetname exist
   if (pAdrRec->strName[0] < '0' && pAdrRec->Unit[1] >= 'A')
   {
      strncpy(pAdrRec->strName, &pAdrRec->Unit[1], SIZ_M_STREET);
      pAdrRec->Unit[0] = 0;
   }
}

/****************************** parseAdr1C_2() ******************************
 *
 * Parse address line 1 into individual elements.  If no street number, parse
 * street name and suffix.  If alpha is attached to strnum, break it out and put
 * it in STRSUB. Suffix will be coded.
 *
 * For SFX only
 *
 ****************************************************************************/

void parseAdr1C_2(ADR_REC *pAdrRec, char *pAdr)
{
   char  acAdr[128], acStr[64], acTmp[64], *apItems[16], *pDir, *pTmp;
   int   iCnt, iTmp, iIdx, iSfxCode;
   bool  bRet;

   // Clear output buffer
   memset((void *)pAdrRec, 0, sizeof(ADR_REC));

   // Remove dot and double space in address line 1
   iIdx = 0;
   while (*pAdr)
   {
      // Also remove single quote
      if (*pAdr != '.' && *pAdr != 39)
         acAdr[iIdx++] = *pAdr;

      // Remove multi-space
      if (*pAdr == ' ')
         while (*pAdr == ' ') pAdr++;
      else
         pAdr++;
   }
   acAdr[iIdx++] = 0;
   if (pTmp = strchr(acAdr, '('))
      *pTmp = 0;

   // Check for PO Box
   //if (!memcmp(acAdr, "PO BOX", 6) || !memcmp(acAdr, "P O ", 4))
   if (strstr(acAdr, "BOX "))
   {
      strcpy(pAdrRec->strName, acAdr);
      return;
   }

   iCnt = ParseString(acAdr, ' ', 16, apItems);
   if (iCnt < 2)
   {
      strcpy(pAdrRec->strName, acAdr);
      return;
   }

   // Check for street number
   iIdx = 0;
   if (isNumber(apItems[0]))
   {
      iIdx++;
      iTmp = atoi(apItems[0]);
      pAdrRec->lStrNum = iTmp;
      sprintf(acTmp, "%d", iTmp);
      strcpy(pAdrRec->strNum, acTmp);
   } else if (*apItems[0] == '#')
   {
      iIdx++;
      iTmp = atoi(&apItems[0][1]);
      pAdrRec->lStrNum = iTmp;
      sprintf(acTmp, "%d", iTmp);
      strcpy(pAdrRec->strNum, acTmp);
   } else if (isdigit(*apItems[0]) && apItems[0][strlen(apItems[0])-1] > '9')
   {
      // StrSub ?
      pTmp = apItems[0];
      while (*pTmp && isdigit(*pTmp) )
         pTmp++;
      if (*pTmp && (*pTmp == '-' || *pTmp == '/'))
         pTmp++;
      if (*pTmp && strlen(pTmp) <= SIZ_M_STR_SUB)
         strcpy(pAdrRec->strSub, pTmp);

      iTmp = atoi(apItems[0]);
      pAdrRec->lStrNum = iTmp;
      sprintf(acTmp, "%d", iTmp);
      strcpy(pAdrRec->strNum, acTmp);
      iIdx++;

      if (*apItems[iIdx] == '-' || *apItems[iIdx] == '&')
      {
         // Skip "-" token
         if (apItems[iIdx][1] == 0)
            iIdx++;
         // Ignore token after hyphen
         iIdx++;
      }
   }

   // Save original strnum
   strcpy(pAdrRec->HseNo, apItems[0]);

   if (*apItems[iIdx] == '-' || *apItems[iIdx] == '&')
   {
      // Skip "-" token
      if (apItems[iIdx][1] == 0)
         iIdx++;
      iIdx++;
   }

   // Dir
   pDir = GetStrDir(apItems[iIdx]);

   // Possible cases E ST or W RD
   // Check for suffix
   bRet = false;
   if (pDir && iCnt > 2)
   {
      // If next item is suffix, then this one might not be direction
      // ex: 123 S AVE B   --> S is direction and AVE B is street name
      //     321 N AVE     --> N is not a direction
      iSfxCode = GetSfxDev(apItems[iIdx+1]);
      if (iSfxCode && iCnt==iIdx+2)
      {
         strcpy(pAdrRec->strName, apItems[iIdx]);
         sprintf(pAdrRec->strSfx, "%d", iSfxCode);
         bRet = true;         // Done
      } else
      {
         // This is direction
         strcpy(pAdrRec->strDir, pDir);
         iIdx++;
      }
   }

   if (!bRet)
   {
      // Not direction, token is str name
      acTmp[0] = 0;
      acStr[0] = 0;

      // Check for unit #
      if (apItems[iCnt-1][0] == '#')
      {
         strncpy(pAdrRec->Unit, apItems[iCnt-1], SIZ_M_UNITNO);
         strncpy(pAdrRec->UnitNox, apItems[iCnt-1], SIZ_M_UNITNOX);
         iCnt--;

         // If last token is STE or SUITE, remove it.
         if (!strcmp(apItems[iCnt-1], "STE") || !strcmp(apItems[iCnt-1], "SUITE")
            || !strcmp(apItems[iCnt-1], "UNIT") || !strcmp(apItems[iCnt-1], "APT"))
            iCnt--;
      } else if (apItems[iCnt-2][0] == '#' 
         || !strcmp(apItems[iCnt-2], "STE") || !strcmp(apItems[iCnt-2], "SUITE")
         || !strcmp(apItems[iCnt-2], "UNIT") || !strcmp(apItems[iCnt-2], "APT"))
      {
         strncpy((char *)&pAdrRec->Unit[0], apItems[iCnt-1], SIZ_M_UNITNO);
         strncpy((char *)&pAdrRec->UnitNox[0], apItems[iCnt-1], SIZ_M_UNITNOX);
         iCnt -= 2;
      } else if (iCnt > 4 && (!strcmp(apItems[iCnt-3], "STE") || !strcmp(apItems[iCnt-3], "SUITE")
         || !strcmp(apItems[iCnt-3], "UNIT") || !strcmp(apItems[iCnt-3], "APT")))
      {
         sprintf(acTmp, "%s%s", apItems[iCnt-2], apItems[iCnt-1]);
         strncpy((char *)&pAdrRec->Unit[0], acTmp, SIZ_M_UNITNO);
         strncpy((char *)&pAdrRec->UnitNox[0], acTmp, SIZ_M_UNITNOX);
         iCnt -= 3;
      } else if (isNumber(apItems[iCnt-1]))
      {
         iTmp = atoi(apItems[iCnt-1]);
         if (iTmp > 0)
         {
            strncpy((char *)&pAdrRec->Unit[0], apItems[iCnt-1], SIZ_M_UNITNO);
            strncpy((char *)&pAdrRec->UnitNox[0], apItems[iCnt-1], SIZ_M_UNITNOX);
         }
         iCnt--;
      }

      // Check 2nd token for Unit# as in 3584 139: 530 #1 SANCHEZ ST
      if (iIdx < iCnt && *apItems[iIdx] == '#')
      {
         strncpy((char *)&pAdrRec->Unit[0], apItems[iIdx], SIZ_M_UNITNO);
         strncpy((char *)&pAdrRec->UnitNox[0], apItems[iIdx], SIZ_M_UNITNOX);
         iIdx++;
      }

      //if (iSfxCode = GetSfxDev(apItems[iCnt-1]))
      if (iSfxCode = GetSfxCodeX(apItems[iCnt-1], acTmp) )
      {
         sprintf(pAdrRec->strSfx, "%d", iSfxCode);
         strcpy(pAdrRec->SfxName, acTmp);
         iCnt--;
      } else if (iCnt > 2 && (iSfxCode = GetSfxCodeX(apItems[iCnt-2], acTmp)))
      {
         // 483 BUENA VISTA AVE EAST
         if (!isDir1(apItems[iCnt-1]))
         {
            sprintf(pAdrRec->strSfx, "%d", iSfxCode);
            strcpy(pAdrRec->SfxName, acTmp);
            if (pAdrRec->Unit[0] <= ' ')
            {
               strncpy((char *)&pAdrRec->Unit[0], apItems[iCnt-1], SIZ_M_UNITNO);
               strncpy((char *)&pAdrRec->UnitNox[0], apItems[iCnt-1], SIZ_M_UNITNOX);
               iCnt -= 2;
            }
         }
      }

      // Multi-word street name
      while (iIdx < iCnt)
      {
         // Skip range number
         if (*(apItems[iIdx]) == '-')
         {
            if (iIdx < iCnt-1)
            {
               iIdx++;
               if (isNumber(apItems[iIdx]))
               {
                  if (iIdx < iCnt-1)
                     iIdx++;
               }
            }
         }

         strcat(acStr, apItems[iIdx++]);
         if (iIdx < iCnt)
            strcat(acStr, " ");
      }
      acStr[SIZ_M_STREET] = 0;
      strcpy(pAdrRec->strName, acStr);

      // Avoid duplicate suffix
      if (strstr(acStr, " AVE "))
      {
         pAdrRec->SfxCode[0] = 0;
         pAdrRec->SfxName[0] = 0;
         pAdrRec->strSfx[0] = 0;
      }
   }
}

/******************************** parseAdr1N() ******************************
 *
 * Parse address line 1 that has no street number, so parse
 * street name, direction and suffix.  Standard Suffix will not be coded.
 * SfxCode is added.  Used in SJX and SBX
 *
 ****************************************************************************/

void parseAdr1N(ADR_REC *pAdrRec, char *pAdr)
{
   char  acAdr[128], acStr[64], acTmp[64], acSfx[64], *apItems[16], *pTmp;
   int   iCnt, iTmp, iIdx, iSfxCode;
   bool  bRet;

   // Clear output buffer
   memset((void *)pAdrRec, 0, sizeof(ADR_REC));

   // Remove dot in address line 1
   pTmp = pAdr;
   iIdx = 0;
   while (*pTmp)
   {
      if (*pTmp != '.')
         acAdr[iIdx++] = *pTmp;
      pTmp++;
   }
   acAdr[iIdx++] = 0;

   iCnt = ParseString(acAdr, ' ', 16, apItems);
   if (iCnt == 1)
   {
      strcpy(pAdrRec->strName, apItems[0]);
      return;
   }

   iIdx = 0;
   iTmp = 0;

   // Remove period after direction if exist
   if (apItems[0][1] == '.')
      apItems[0][1] = 0;

   // Dir
   //while (asDir[iTmp])
   //{
   //   if (!strcmp(apItems[iIdx], asDir[iTmp]))
   //      break;
   //   iTmp++;
   //}

   // Possible cases E ST or W RD
   bRet = isDir(apItems[iIdx]);

   // Check for suffix
   if (bRet && iCnt > 1)
   {
      // If next item is suffix, then this one might not be direction
      // ex: 123 S AVE B   --> S is direction and AVE B is street name
      //     321 N AVE     --> N is not a direction
      //iSfxCode = GetSfxCode(apItems[iIdx+1]);
      iSfxCode = GetSfxCodeX(apItems[iIdx+1], acSfx);
      if (iSfxCode && iCnt==iIdx+2)
      {
         strcpy(pAdrRec->strName, apItems[iIdx]);
         strcpy(pAdrRec->strSfx, acSfx);
         sprintf(pAdrRec->SfxCode, "%d", iSfxCode);
         bRet = true;         // Done
      } else
      {
         // This is direction
         strcpy(pAdrRec->strDir, apItems[iIdx]);
         iIdx++;
      }
   }

   if (!bRet)
   {
      // Not direction, token is str name
      acTmp[0] = 0;
      acStr[0] = 0;

      // Check for unit #
      if (apItems[iCnt-1][0] == '#')
      {
         strncpy(pAdrRec->Unit, apItems[iCnt-1], SIZ_M_UNITNO);
         strncpy(pAdrRec->UnitNox, apItems[iCnt-1], SIZ_M_UNITNOX);
         iCnt--;

         // If last token is STE or SUITE, remove it.
         if (iCnt > 2 && (!strcmp(apItems[iCnt-1], "STE") || !strcmp(apItems[iCnt-1], "SUITE")))
            iCnt--;
      } else if (iCnt > 3 && (!strcmp(apItems[iCnt-2], "STE") || !strcmp(apItems[iCnt-2], "SUITE")))
      {
         pAdrRec->Unit[0] = '#';
         strncpy((char *)&pAdrRec->Unit[1], apItems[iCnt-1], SIZ_M_UNITNO);
         pAdrRec->UnitNox[0] = '#';
         strncpy((char *)&pAdrRec->UnitNox[1], apItems[iCnt-1], SIZ_M_UNITNOX);
         iCnt -= 2;
      }

      //iSfxCode = GetSfxCode(apItems[iCnt-1]);
      iSfxCode = GetSfxCodeX(apItems[iCnt-1], acSfx);
      if (iSfxCode)
      {
         // Multi-word street name with suffix
         strcpy(pAdrRec->strSfx, acSfx);
         sprintf(pAdrRec->SfxCode, "%d", iSfxCode);
         while (iIdx < iCnt-1)
         {
            strcat(acStr, apItems[iIdx++]);
            if (iIdx < iCnt)
               strcat(acStr, " ");
         }
      } else
      {
         // Multi-word street name without suffix
         while (iIdx < iCnt)
         {
            strcat(acStr, apItems[iIdx++]);
            if (iIdx < iCnt)
               strcat(acStr, " ");
         }
      }
      acStr[SIZ_M_STREET] = 0;
      strcpy(pAdrRec->strName, acStr);
   }
}

/*************************************************************************
 *
 * For NEV - StrNum may appear in StrName
 *
 *************************************************************************/

void parseAdr1N_1(ADR_REC *pAdrRec, char *pAdr)
{
   char  acAdr[128], acStr[64], acSfx[64], acDir[32], acTmp[64], *apItems[16], *pTmp;
   int   iCnt, iTmp, iIdx, iSfxCode;
   bool  bRet, bDir=false;

   // Clear output buffer
   memset((void *)pAdrRec, 0, sizeof(ADR_REC));

   // Remove dot in address line 1
   strcpy(acAdr, _strupr(pAdr));
   replCharEx(acAdr, ".", ' ');

   iCnt = ParseString(acAdr, ' ', 16, apItems);
   if (iCnt == 1)
   {
      strcpy(pAdrRec->strName, apItems[0]);
      return;
   }

   iIdx = 0;
   iTmp = 0;
   iSfxCode = 0;
   acDir[0] = 0;

   // Dir
   bDir = isDir(apItems[iIdx], acDir);
   if (!bDir)
   {
      bDir = isDir1(apItems[iIdx], acDir);
      if (!bDir)
         bDir = isDir2(apItems[iIdx], acDir);
   }

   // Possible cases E ST or W RD
   // Check for suffix
   bRet = false;
   acStr[0] = 0;
   if (bDir && iCnt > 1)
   {
      // If next item is suffix, then this one might not be direction
      // ex: 123 S AVE B   --> S is direction and AVE B is street name
      //     321 N AVE     --> N is not a direction
      iSfxCode = GetSfxCodeX(apItems[iIdx+1], acSfx);
      if (iSfxCode && strcmp(apItems[iIdx+1], "HWY"))
      {
         strcpy(acStr, apItems[iIdx]);
         sprintf(pAdrRec->SfxCode, "%d", iSfxCode);
         strcpy(pAdrRec->strSfx, acSfx);
         strcpy(pAdrRec->SfxName, acSfx);
         iIdx += 2;

         if (iCnt==iIdx)
            bRet = true;         // Done
      } else
      {
         // This is direction
         strcpy(pAdrRec->strDir, acDir);
         iIdx++;
         iSfxCode = 0;
      }
   }

   if (!bRet)
   {
      // Not direction, token is str name

      // Check for unit #
      if (*apItems[iCnt-1] == '#')
      {
         // NORTH SCHOOL ST #1
         strncpy(pAdrRec->Unit, apItems[iCnt-1], SIZ_M_UNITNO);
         iCnt--;

         // If last token is STE or SUITE, remove it.
         if (iCnt > 2 && (!strcmp(apItems[iCnt-1], "STE") || !strcmp(apItems[iCnt-1], "SUITE")))
            iCnt--;
      } else if (iCnt > 3 && (!strcmp(apItems[iCnt-2], "STE") ||
                              !strcmp(apItems[iCnt-2], "SUITE") ||
                              !strcmp(apItems[iCnt-2], "APT")))
      {
         strncpy(pAdrRec->Unit, apItems[iCnt-1], SIZ_M_UNITNO);
         iCnt -= 2;
      } else if (!memcmp(apItems[iCnt-1], "1/2", 3))
      {
         // WILSON ST 1/2
         strcpy(pAdrRec->strSub, "1/2");
         iCnt--;
      } else if (isdigit(*apItems[iCnt-1]) && strcmp(apItems[iCnt-2], "HIGHWAY") && strcmp(apItems[iCnt-2], "HWY"))
      {
         // MURPHY ST 116-1/2
         // W MAIN ST - 311
         // Drop 2nd number
         if (strchr(apItems[iCnt-1], '/'))
            iCnt--;
         else if (strchr(apItems[iCnt-1], '-'))
         {
            // SO AUBURN ST 1-4
            strncpy(pAdrRec->Unit, apItems[iCnt-1], SIZ_M_UNITNO);
            iCnt--;
         } else if (!strcmp(apItems[iCnt-2], "BLDG"))
         {
            // LOMA RICA DR. BLDG 1
            sprintf(acTmp, "BLDG %s", apItems[iCnt-1]);
            strncpy(pAdrRec->Unit, acTmp, SIZ_M_UNITNO);
            iCnt -= 2;
         } else
            iCnt--;

         if (*apItems[iCnt-1] == '-')
            iCnt--;
      } else if (*apItems[iCnt-1] == '-')
      {
         // BRIGHTON ST -AB
         // BUTLER ST -562
         if (isalpha(apItems[iCnt-1][1]))
            strncpy(pAdrRec->Unit, &apItems[iCnt-1][1], SIZ_M_UNITNO);
         iCnt--;
      } else if (pTmp = strchr(apItems[iCnt-1], '-'))
      {
         // TOWNSEND ST-1/2
         // BUTLER ST-554
         // EAST MAIN ST A-B
         // BITNEY SPRINGS RD-GRANNY
         if (*(pTmp+2) == 0)
         {
            strncpy(pAdrRec->Unit, apItems[iCnt-1], SIZ_M_UNITNO);
            iCnt--;
         } else
         {
            if (!memcmp(pTmp+1, "1/2", 3))
               strcpy(pAdrRec->strSub, "1/2");
            *pTmp = 0;
         }
      } else if (*apItems[iCnt-2] == '-')
      {
         // PLEASANT ST - B
         strncpy(pAdrRec->Unit, apItems[iCnt-1], SIZ_M_UNITNO);
         iCnt -= 2;
      } else if (pTmp = strchr(apItems[iCnt-2], '-'))
      {
         // RACE ST-APT ABC
         // N BLOOMFIELD-GRNTVILL RD
         if (!memcmp(pTmp+1, "APT", 3))
         {
            strncpy(pAdrRec->Unit, apItems[iCnt-1], SIZ_M_UNITNO);
            *pTmp = 0;
         }
      }

      if (!iSfxCode)
      {
         if (iSfxCode = GetSfxCodeX(apItems[iCnt-1], acSfx))
         {
            // Multi-word street name with suffix
            sprintf(pAdrRec->SfxCode, "%d", iSfxCode);
            strcpy(pAdrRec->strSfx, acSfx);
            strcpy(pAdrRec->SfxName, acSfx);
            while (iIdx < iCnt-1)
            {
               strcat(acStr, apItems[iIdx++]);
               if (iIdx < iCnt)
                  strcat(acStr, " ");
            }
         } else if (iCnt > 1 && (iSfxCode = GetSfxCodeX(apItems[iCnt-2], acSfx)))
         {
            // PARK AVE AB
            // EAST MAIN ST A
            // STATE HWY 49
            if (memcmp(acSfx, "HWY", 3) && memcmp(acSfx, "RD", 2))
            {
               sprintf(pAdrRec->SfxCode, "%d", iSfxCode);
               strcpy(pAdrRec->strSfx, acSfx);
               strncpy(pAdrRec->Unit, apItems[iCnt-1], SIZ_M_UNITNO);
               iCnt -= 2;
            }
            while (iIdx < iCnt)
            {
               strcat(acStr, apItems[iIdx++]);
               if (iIdx < iCnt)
                  strcat(acStr, " ");
            }
         } else
         {
            // Multi-word street name without suffix
            while (iIdx < iCnt)
            {
               strcat(acStr, apItems[iIdx++]);
               if (iIdx < iCnt)
                  strcat(acStr, " ");
            }
         }
      } else if (iIdx < iCnt)
      {
         // MAIN ST BC
         strncpy(pAdrRec->Unit, apItems[iCnt-1], SIZ_M_UNITNO);
      }
   }

   acStr[SIZ_M_STREET] = 0;
   strcpy(pAdrRec->strName, acStr);
}

/******************************** parseAdr1S() ******************************
 *
 * Parse address line 1 that has no street number, so parse
 * street name, direction and suffix.  Suffix will be coded
 *
 * DNX, INY, LAS, MOD, SIE, TEH, TRI, TUO
 * AMA, BUT, COL, HUM, LAK, MER, MON, PLA, PLU, SHA, SIS, STA, YOL, YUB
 * SAC, SCR, SOL
 *
 ****************************************************************************/

void parseAdr1S(ADR_REC *pAdrRec, char *pAdr)
{
   char  acAdr[128], acStr[64], acTmp[64], *apItems[16], *pTmp, *pDir;
   int   iCnt, iTmp, iIdx, iSfxCode;
   bool  bRet;

   // Clear output buffer
   memset((void *)pAdrRec, 0, sizeof(ADR_REC));

   // Remove dot in address line 1
   pTmp = pAdr;
   iIdx = 0;
   while (*pTmp)
   {
      // Remove space between # and unit number
      if (*pTmp == '#')
      {
         acAdr[iIdx++] = *pTmp++;
         while (*pTmp && *pTmp == ' ')
            pTmp++;
      }
      if (*pTmp != '.' && *pTmp != '"')
         acAdr[iIdx++] = *pTmp;
      pTmp++;
   }
   acAdr[iIdx++] = 0;

   iCnt = ParseString(acAdr, ' ', 16, apItems);
   if (iCnt == 1)
   {
      strcpy(pAdrRec->strName, apItems[0]);
      return;
   }

   iIdx = 0;
   iTmp = 0;
   acStr[0] = 0;

   // Remove period after direction if exist
   if (apItems[0][1] == '.')
      apItems[0][1] = 0;

   // Dir
   pDir = GetStrDir(apItems[iIdx]);

   // Possible cases E ST or W RD
   // Check for suffix
   bRet = false;
   if (pDir && iCnt >= 2)
   {
      // If next item is suffix, then this one might not be direction
      // ex: 123 S AVE B   --> S is direction and AVE B is street name
      //     321 N AVE     --> N is not a direction
      iSfxCode = GetSfxDev(apItems[iIdx+1]);
      if (iSfxCode && iCnt==iIdx+2)
      {
         strcpy(acStr, apItems[iIdx]);
         if (bEnCode)
         {
            sprintf(pAdrRec->strSfx, "%d", GetSfxCode(GetSfxStr(iSfxCode)));
            strcpy(pAdrRec->SfxName, GetSfxStr(iSfxCode));
         }	
         else
            strcpy(pAdrRec->strSfx, GetSfxStr(iSfxCode));
         bRet = true;         // Done
      } else
      {
         // This is direction
         strcpy(pAdrRec->strDir, pDir);
         iIdx++;
      }
   }

   if (!bRet)
   {
      // Not direction, token is str name
      acTmp[0] = 0;

      // Check for unit #
      if (apItems[iCnt-1][0] == '#')
      {
         strncpy(pAdrRec->Unit, apItems[iCnt-1], SIZ_M_UNITNO);
         strncpy(pAdrRec->UnitNox, apItems[iCnt-1], SIZ_M_UNITNOX);
         iCnt--;

         // If last token is STE or SUITE, remove it.
         if (iCnt > 2 && (!strcmp(apItems[iCnt-1], "STE") || !strcmp(apItems[iCnt-1], "SUITE")))
            iCnt--;
      } else if (iCnt > 3 && (!strcmp(apItems[iCnt-2], "STE") || !strcmp(apItems[iCnt-2], "SUITE")))
      {
         pAdrRec->Unit[0] = '#';
         pAdrRec->UnitNox[0] = '#';
         strncpy(&pAdrRec->Unit[1], apItems[iCnt-1], SIZ_M_UNITNO-1);
         strncpy(&pAdrRec->UnitNox[1], apItems[iCnt-1], SIZ_M_UNITNOX-1);
         iCnt -= 2;
      } else if (!memcmp(apItems[iCnt-1], "SP", 2) && apItems[iCnt-1][2] <= '9')
      {  // SP12
         strncpy(pAdrRec->Unit, apItems[iCnt-1], SIZ_M_UNITNO);
         strncpy(pAdrRec->UnitNox, apItems[iCnt-1], SIZ_M_UNITNOX);
         iCnt--;
      } else if (iCnt > 3 && (!strcmp(apItems[iCnt-2], "SP")))
      {
         memcpy(pAdrRec->Unit, "SP",2);
         memcpy(pAdrRec->UnitNox, "SP",2);
         strncpy(&pAdrRec->Unit[2], apItems[iCnt-1], SIZ_M_UNITNO);
         strncpy(&pAdrRec->UnitNox[2], apItems[iCnt-1], SIZ_M_UNITNOX);
         iCnt -= 2;
      }

      if (iSfxCode = GetSfxDev(apItems[iCnt-1]))
      {
         // Multi-word street name with suffix
         if (bEnCode)
         {
            sprintf(pAdrRec->strSfx, "%d", GetSfxCode(GetSfxStr(iSfxCode)));
            strcpy(pAdrRec->SfxName, GetSfxStr(iSfxCode));
         }
         else
            strcpy(pAdrRec->strSfx, GetSfxStr(iSfxCode));

         while (iIdx < iCnt-1)
         {
            strcat(acStr, apItems[iIdx++]);
            if (iIdx < iCnt)
               strcat(acStr, " ");
         }
      } else
      {
         // Multi-word street name without suffix
         while (iIdx < iCnt)
         {
            strcat(acStr, apItems[iIdx++]);
            if (iIdx < iCnt)
               strcat(acStr, " ");
         }
      }
   }

   // Copy street name
   acStr[SIZ_M_STREET] = 0;
   strcpy(pAdrRec->strName, acStr);
}

/******************************** parseAdr1S() ******************************
 *
 * Same as parseAdr1S().  Added check for direction in last token.  This occurs
 * quite often in CAL.
 *
 * CAL->020014001000: OLD EMIGRANT TR WEST
 *
 ****************************************************************************/

void parseAdr1S_1(ADR_REC *pAdrRec, char *pAdr)
{
   char  acAdr[128], acStr[64], acTmp[64], *apItems[16], *pTmp, *pDir;
   int   iCnt, iTmp, iIdx, iSfxCode;
   bool  bRet;

   // Clear output buffer
   memset((void *)pAdrRec, 0, sizeof(ADR_REC));

   // Remove dot in address line 1
   pTmp = pAdr;
   iIdx = 0;
   while (*pTmp)
   {
      // Remove space between # and unit number
      if (*pTmp == '#')
      {
         acAdr[iIdx++] = *pTmp++;
         while (*pTmp && *pTmp == ' ')
            pTmp++;
      }
      if (*pTmp != '.' && *pTmp != '"')
         acAdr[iIdx++] = *pTmp;
      pTmp++;
   }
   acAdr[iIdx++] = 0;

   iCnt = ParseString(acAdr, ' ', 16, apItems);
   if (iCnt == 1)
   {
      strcpy(pAdrRec->strName, apItems[0]);
      return;
   }

   iIdx = 0;
   iTmp = 0;
   acStr[0] = 0;

   // Remove period after direction if exist
   if (apItems[0][1] == '.')
      apItems[0][1] = 0;

   // Dir
   pDir = GetStrDir(apItems[iIdx]);

   // Possible cases E ST or W RD
   // Check for suffix
   bRet = false;
   if (pDir && iCnt >= 2)
   {
      // If next item is suffix, then this one might not be direction
      // ex: 123 S AVE B   --> S is direction and AVE B is street name
      //     321 N AVE     --> N is not a direction
      iSfxCode = GetSfxDev(apItems[iIdx+1]);
      if (iSfxCode && iCnt==iIdx+2)
      {
         strcpy(acStr, apItems[iIdx]);
         if (bEnCode)
         {
            sprintf(pAdrRec->strSfx, "%d", GetSfxCode(GetSfxStr(iSfxCode)));
            strcpy(pAdrRec->SfxName, GetSfxStr(iSfxCode));
         }	
         else
            strcpy(pAdrRec->strSfx, GetSfxStr(iSfxCode));
         bRet = true;         // Done
      } else
      {
         // This is direction
         strcpy(pAdrRec->strDir, pDir);
         iIdx++;
      }
   } else if (iCnt > 2 && (pDir = GetStrDir(apItems[iCnt-1])) )
   {
      strcpy(pAdrRec->strDir, pDir);
      iCnt--;
   }

   if (!bRet)
   {
      // Not direction, token is str name
      acTmp[0] = 0;

      // Check for unit #
      if (apItems[iCnt-1][0] == '#')
      {
         strncpy(pAdrRec->Unit, apItems[iCnt-1], SIZ_M_UNITNO);
         strncpy(pAdrRec->UnitNox, apItems[iCnt-1], SIZ_M_UNITNOX);
         iCnt--;

         // If last token is STE or SUITE, remove it.
         if (iCnt > 2 && (!strcmp(apItems[iCnt-1], "STE") || !strcmp(apItems[iCnt-1], "SUITE")))
            iCnt--;
      } else if (iCnt > 3 && (!strcmp(apItems[iCnt-2], "STE") || !strcmp(apItems[iCnt-2], "SUITE")))
      {
         pAdrRec->Unit[0] = '#';
         pAdrRec->UnitNox[0] = '#';
         strncpy(&pAdrRec->Unit[1], apItems[iCnt-1], SIZ_M_UNITNO-1);
         strncpy(&pAdrRec->UnitNox[1], apItems[iCnt-1], SIZ_M_UNITNOX-1);
         iCnt -= 2;
      } else if (!memcmp(apItems[iCnt-1], "SP", 2) && apItems[iCnt-1][2] <= '9')
      {  // SP12
         strncpy(pAdrRec->Unit, apItems[iCnt-1], SIZ_M_UNITNO);
         strncpy(pAdrRec->UnitNox, apItems[iCnt-1], SIZ_M_UNITNOX);
         iCnt--;
      } else if (iCnt > 3 && (!strcmp(apItems[iCnt-2], "SP")))
      {
         memcpy(pAdrRec->Unit, "SP",2);
         strncpy(&pAdrRec->Unit[2], apItems[iCnt-1], SIZ_M_UNITNO);
         memcpy(pAdrRec->UnitNox, "SP",2);
         strncpy(&pAdrRec->UnitNox[2], apItems[iCnt-1], SIZ_M_UNITNOX);
         iCnt -= 2;
      }

      if (iSfxCode = GetSfxDev(apItems[iCnt-1]))
      {
         // Multi-word street name with suffix
         if (bEnCode)
         {
            sprintf(pAdrRec->strSfx, "%d", GetSfxCode(GetSfxStr(iSfxCode)));
            strcpy(pAdrRec->SfxName, GetSfxStr(iSfxCode));
         }
         else
            strcpy(pAdrRec->strSfx, GetSfxStr(iSfxCode));

         while (iIdx < iCnt-1)
         {
            strcat(acStr, apItems[iIdx++]);
            if (iIdx < iCnt)
               strcat(acStr, " ");
         }
      } else
      {
         // Multi-word street name without suffix
         while (iIdx < iCnt)
         {
            strcat(acStr, apItems[iIdx++]);
            if (iIdx < iCnt)
               strcat(acStr, " ");
         }
      }
   }

   // Copy street name
   acStr[SIZ_M_STREET] = 0;
   strcpy(pAdrRec->strName, acStr);
}

/******************************* parseAdrNSD() ******************************
 *
 * Parse address line 1 that has no street number, so parse
 * street name, suffix and direction (last token is direction).
 * Output buffer won't be initialized.  Added for SDX.
 *
 ****************************************************************************/

void parseAdrNSD(ADR_REC *pAdrRec, char *pAdr)
{
   char  acAdr[128], acStr[64], *apItems[16], *pDir, *pTmp;
   int   iCnt, iIdx, iSfxCode;

   // Copy addr
   strcpy(acAdr, pAdr);

   // Check for Unit#
   if (pTmp = strchr(acAdr, '#'))
   {
      strcpy(pAdrRec->Unit, pTmp);
      strcpy(pAdrRec->UnitNox, pTmp);
      *pTmp = 0;
   } 

   iCnt = ParseString(acAdr, ' ', 16, apItems);
   if (iCnt == 1)
   {
      acAdr[SIZ_M_STREET] = 0;
      strcpy(pAdrRec->strName, acAdr);
      return;
   }

   // Dir
   pDir = GetStrDir(apItems[iCnt-1]);
   if (pDir)
   {
      strcpy(pAdrRec->strDir, pDir);
      iCnt--;
   }

   // Check for suffix
   if (iCnt > 1)
   {
      // ex: STAGECOACH LN NORTH
      iSfxCode = GetSfxDev(apItems[iCnt-1]);
      if (iSfxCode)
      {
         strcpy(pAdrRec->strSfx, GetSfxStr(iSfxCode));
         iCnt--;
      }
   }

   // Street name
   iIdx = 0;
   acStr[0] = 0;
   while (iIdx < iCnt)
   {
      strcat(acStr, apItems[iIdx++]);
      if (iIdx < iCnt)
         strcat(acStr, " ");
   }

   acStr[SIZ_M_STREET] = 0;
   strcpy(pAdrRec->strName, acStr);
}

/******************************* parseAdrDNS() ******************************
 *
 * Parse address line 1 that has no street number, so parse
 * direction, street name, suffix (suffix will be coded), and unit.
 * Added for FRE
 *
 ****************************************************************************/

void parseAdrDNS(ADR_REC *pAdrRec, char *pAdr)
{
   char  acAdr[128], acStr[64], acTmp[64], *apItems[16], *pTmp, *pDir;
   int   iCnt, iIdx, iSfxCode;

   // Initialize output
   memset(pAdrRec, 0, sizeof(ADR_REC)-1);

   // Copy addr
   strcpy(acAdr, pAdr);
   iCnt = ParseString(acAdr, ' ', 16, apItems);
   if (iCnt == 1)
   {
      acAdr[SIZ_M_STREET] = 0;
      strcpy(pAdrRec->strName, acAdr);
      return;
   }

   // Check for UnitNo
   if (*apItems[iCnt-1] == '#')
   {
      // If unitno is #101-103, we rather have it #101 than #101-1
      if (strlen(apItems[iCnt-1]) > SIZ_M_UNITNO && (pTmp=strchr(apItems[iCnt-1], '-')) )
         *pTmp = 0;
      strcpy(pAdrRec->Unit, apItems[iCnt-1]);
      strcpy(pAdrRec->UnitNox, apItems[iCnt-1]);
      iCnt--;
   }

   // Check for suffix
   iSfxCode = GetSfxDev(apItems[iCnt-1]);
   if (iSfxCode)
   {
      sprintf(acTmp, "%d", iSfxCode);
      strcpy(pAdrRec->strSfx, acTmp);
      strcpy(pAdrRec->SfxName, apItems[iCnt-1]);
      iCnt--;
   }

   // Check for direction
   iIdx = 0;
   if (iCnt > 1)
   {
      // Dir
      pDir = GetStrDir(apItems[0]);
      if (pDir)
      {
         strcpy(pAdrRec->strDir, pDir);
         iIdx++;
      }
   }

   // Street name
   acStr[0] = 0;
   while (iIdx < iCnt)
   {
      strcat(acStr, apItems[iIdx++]);
      if (iIdx < iCnt)
         strcat(acStr, " ");
   }

   acStr[SIZ_M_STREET] = 0;
   strcpy(pAdrRec->strName, acStr);
}

/******************************** parseAdrND() ******************************
 *
 * Parse address line 1 that has street name and direction only.  Direction comes
 * after street name. It may have strnum and strsfx, so this need to be checked too.
 * Design for CCX.
 *
 * - Extract last word and check if street name has more than one tokens
 * - If suffix is empty, check if last word is suffix ( as in CALIFORNIA N BLVD).
 *   Ignore suffix if '&' present (as in 7TH ST & VAQUEROS AVE)
 * - Check last word for direction
 * - If StrNum is empty, check first word for number (as in 1744-1750 PARKSIDE)
 * - 5/4/2018 CCX has changed addr format to remove post direction
 *
 ****************************************************************************/

void parseAdrND(ADR_REC *pAdrRec, char *pAdr)
{
   char  acAdr[128], acStr[64], *apItems[16], *pTmp, *pDir;
   int   iCnt, iIdx, iSfxCode;

   // Special case
   //if (!memcmp(pAdr, "KEY WEST", 8))
   //{
   //   memcpy(pAdrRec->strName, pAdr, 8);
   //   return;
   //}

   // Remove dot, quote and extra space
   pTmp = pAdr;
   iIdx = 0;
   while (*pTmp)
   {
      acAdr[iIdx++] = *pTmp;
      // Remove multi-space
      if (*pTmp == ' ')
         while (*pTmp == ' ') pTmp++;
      else
         pTmp++;

      // Remove '.' and single quote too
      if (*pTmp == '.' || *pTmp == ',' || *pTmp == '`' || *pTmp == 39)
         pTmp++;
      else if (*pTmp == ' ' && *(pTmp-1) == '#')
         while (*pTmp == ' ') pTmp++;

      if (*pTmp == '$')
         break;
   }
   acAdr[iIdx++] = 0;

   // Check for PO BOX
   if (!memcmp(acAdr, "P 0 BOX", 7) ||
       !memcmp(acAdr, "P O BOX", 7) ||
       !memcmp(acAdr, "PIO BOX", 7) ||
       !memcmp(acAdr, "PO BOIX", 7) ||
       !memcmp(acAdr, "PO BOPX", 7) ||
       !memcmp(acAdr, "PO BOXO", 7) ||
       !memcmp(acAdr, "P OB OX", 7) )
   {
      strcpy(pAdrRec->strName, "PO BOX");
      strcat(pAdrRec->strName, (char *)&acAdr[7]);
      return;
   } else if (!memcmp(acAdr, "P OBOX", 6) ||
       !memcmp(acAdr, "PO BIX", 6) ||
       !memcmp(acAdr, "PO BPX", 6) ||
       !memcmp(acAdr, "PO BXO", 6) ||
       !memcmp(acAdr, "PO OBX", 6) ||
       !memcmp(acAdr, "PO POX", 6) )
   {
      strcpy(pAdrRec->strName, "PO BOX");
      strcat(pAdrRec->strName, (char *)&acAdr[6]);
      return;
   } else if (!memcmp(acAdr, "P BOX", 5) || !memcmp(acAdr, "POBOX", 5) )
   {
      strcpy(pAdrRec->strName, "PO BOX");
      strcat(pAdrRec->strName, (char *)&acAdr[5]);
      return;
   }

   // Parse str name
   iCnt = ParseString(acAdr, ' ', 16, apItems);
   if (iCnt == 1)
   {
      strcpy(pAdrRec->strName, pAdr);
      return;
   }

   // Check suffix
   if (pAdrRec->strSfx[0] < 'A' && *apItems[iCnt-1] >= 'A')
   {
      if (iSfxCode = GetSfxDev(apItems[iCnt-1]))
      {
         strcpy(pAdrRec->strSfx, GetSfxStr(iSfxCode));
         iCnt -= 1;
      }
   }

   iIdx = 0;
   acStr[0] = 0;
   // Check for StrNum
   if (!pAdrRec->lStrNum && iCnt > 1)
   {
      if (isNumber(apItems[iIdx]))
      {
         pAdrRec->lStrNum = atol(apItems[iIdx]);
         iIdx++;
      }
   }

   // Check for unit#
   if (pAdrRec->Unit[0] <= ' ' && pAdrRec->lStrNum > 0)
   {
      if (*apItems[iIdx] == '#')
      {
         strcpy(pAdrRec->Unit, apItems[iIdx]);
         strcpy(pAdrRec->UnitNox, apItems[iIdx++]);
      } else if (strlen(apItems[iIdx]) == 1)
      {
         if (*apItems[iIdx] >= 'A' && *apItems[iIdx] <= 'D' && !GetSfxDev(apItems[iIdx+1]) )
         {
            strcpy(pAdrRec->Unit, apItems[iIdx]);
            strcpy(pAdrRec->UnitNox, apItems[iIdx++]);
         }
      }
   }

   // Check direction
   if (iCnt > iIdx+1)
   {
      pDir = apItems[iIdx];

      // Check for pre direction
      if ((strlen(pDir) == 1) && (*pDir=='N' || *pDir=='S' || *pDir=='E' || *pDir=='W') )
      {
         pAdrRec->strDir[0] = *pDir;
         iIdx++;
      }
   }

   // CCX has removed post direction on 5/4/2018
   //if (iCnt > iIdx+1)
   //{
   //   pDir = apItems[iCnt-1];
   //   pTmp = apItems[iCnt-2];

   //   // Check for post direction
   //   if ((strlen(pTmp) == 1) && (*pTmp == 'N' || *pTmp == 'S') &&
   //       (strlen(pDir) == 1) && (*pDir=='E' || *pDir=='W') )
   //   {
   //      strcat(pTmp, pDir);
   //      iCnt--;
   //   } else if ((pAdrRec->strDir[0] <= ' ') && (pTmp = GetStrDir(pDir)) )
   //   {
   //      strcpy(pAdrRec->strDir, pTmp);
   //      iCnt -= 1;
   //   }
   //}

   // Check for Suite#
   if (iCnt > iIdx+2 && pAdrRec->Unit[0] <= ' ')
   {
      if (!strcmp(apItems[iCnt-2], "STE") )
      {
         strcpy(pAdrRec->Unit, apItems[iCnt-1]);
         strcpy(pAdrRec->UnitNox, apItems[iCnt-1]);
         iCnt -= 2;
      } else if (*apItems[iCnt-1] == '#')
      {
         strcpy(pAdrRec->Unit, apItems[iCnt-1]);
         strcpy(pAdrRec->UnitNox, apItems[iCnt-1]);
         iCnt--;
      }
   }

   // Check suffix again
   if (pAdrRec->strSfx[0] < 'A')
   {
      if (iSfxCode = GetSfxDev(apItems[iCnt-1]))
      {
         strcpy(pAdrRec->strSfx, GetSfxStr(iSfxCode));
         iCnt -= 1;
      }
   }

   if (!strcmp(apItems[iIdx], "RD"))
   {
      strcpy(acStr, "ROAD ");
      iIdx++;
   } else if (!strcmp(apItems[iIdx], "AVE"))
   {
      strcpy(acStr, "AVENUE ");
      iIdx++;
   } else if (!strcmp(apItems[iIdx], "ST") && (iCnt > (iIdx+1) && !strcmp(apItems[iIdx+1], "HWY")))
   {
      strcpy(acStr, "STATE HIGHWAY ");
      iIdx += 2;
   } else if (!strcmp(apItems[iIdx], "-") )
      iIdx++;

   for (; iIdx < iCnt; iIdx++)
   {
      strcat(acStr, apItems[iIdx]);
      strcat(acStr, " ");
   }

   acStr[SIZ_M_STREET] = 0;
   strcpy(pAdrRec->strName, acStr);
}

/******************************** parseAdr1_1() *****************************
 *
 * Parse address line 1 into individual elements.  If no street number, parse
 * street name and suffix.  Suffix left as is.  Used in CREST counties, SOL, SBX.
 *
 ****************************************************************************/

void parseAdr1_1(ADR_REC *pAdrRec, char *pAdr)
{
   char  acAdr[128], acStr[64], acTmp[32], *apItems[16], *pTmp, *pDir;
   int   iCnt, iTmp, iIdx, iSfxCode;
   bool  bRet;

   // Clear output buffer
   memset((void *)pAdrRec, 0, sizeof(ADR_REC));

   // Remove dot in address line 1
   pTmp = pAdr;
   iIdx = 0;
   while (*pTmp)
   {
      if (*pTmp != '.')
         acAdr[iIdx++] = *pTmp;
      pTmp++;
   }
   acAdr[iIdx++] = 0;
   acStr[0] = 0;

   // Check for PO Box
   if (strstr(acAdr, "BOX "))
   {
      strncpy(pAdrRec->strName, acAdr, SIZ_M_STREET);
      return;
   }
   // Check for Suite
   if (pTmp = strstr(acAdr, "SUITE "))
   {
      if (pTmp > &acAdr[0])
         *(pTmp-1) = 0;
      strcpy(pAdrRec->Unit, pTmp+6);
      strcpy(pAdrRec->UnitNox, pTmp+6);
   } else if (pTmp = strstr(acAdr, "STE "))
   {
      if (pTmp > &acAdr[0])
         *(pTmp-1) = 0;
      strcpy(pAdrRec->Unit, pTmp+4);
      strcpy(pAdrRec->UnitNox, pTmp+4);
   }

   iCnt = ParseString(acAdr, ' ', 16, apItems);
   // If strNum is 0, do not parse address
   iTmp = atoi(apItems[0]);
   if (iTmp == 0 || iCnt < 2)
   {
      strncpy(pAdrRec->strName, pAdr, SIZ_M_STREET);
      pAdrRec->lStrNum = 0;
      return;
   }

   pAdrRec->lStrNum = iTmp;
   strcpy(pAdrRec->strNum, apItems[0]);
   iIdx = 1;

   // Dir
   pDir = GetStrDir(apItems[iIdx]);

   // Possible cases E ST or W RD
   // Check for suffix
   bRet = false;
   if (pDir && iCnt >= 3)
   {
      // If next item is suffix, then this one is not direction
      if (iSfxCode = GetSfxDev(apItems[iIdx+1]))
      {
        if (iCnt >= 4)
        {
          strcpy(pAdrRec->strDir, pDir);
          iIdx++;
        } else {
          strcpy(acStr, apItems[iIdx]);
          strcpy(pAdrRec->SfxName, apItems[iIdx+1]);
          strcpy(pAdrRec->strSfx, GetSfxStr(iSfxCode));
          bRet = true;         // Done
        }
      } else
      {
         // This is direction
         strcpy(pAdrRec->strDir, pDir);
         iIdx++;
      }
   }

   if (!bRet)
   {
      // Not direction, token is str name
      acTmp[0] = 0;

      // Check for unit #
      if (apItems[iCnt-1][0] == '#')
      {
         strncpy(pAdrRec->Unit, apItems[iCnt-1], SIZ_M_UNITNO);
         strncpy(pAdrRec->UnitNox, apItems[iCnt-1], SIZ_M_UNITNOX);
         iCnt--;
      }

      if (iSfxCode = GetSfxDev(apItems[iCnt-1]))
      {
         strcpy(pAdrRec->SfxName, apItems[iCnt-1]);
         strcpy(pAdrRec->strSfx, GetSfxStr(iSfxCode));
         while (iIdx < iCnt-1)
         {
            strcat(acStr, apItems[iIdx++]);
            if (iIdx < iCnt)
               strcat(acStr, " ");
         }
      } else
      {
         while (iIdx < iCnt)
         {
            strcat(acStr, apItems[iIdx++]);
            if (iIdx < iCnt)
               strcat(acStr, " ");
         }
      }
   }

   acStr[SIZ_M_STREET] = 0;
   strcpy(pAdrRec->strName, acStr);
}

/******************************** parseAdr1_2() *****************************
 *
 * Parse address line 1 into individual elements.  If no street number, parse
 * street name and suffix.  Suffix will be coded
 *
 ****************************************************************************/

void parseAdr1_2(ADR_REC *pAdrRec, char *pAdr)
{
   char  acAdr[128], acStr[64], acTmp[32], *apItems[16], *pTmp;
   int   iCnt, iTmp, iIdx, iSfxCode;
   bool  bRet;

   // Clear output buffer
   memset((void *)pAdrRec, 0, sizeof(ADR_REC));

   // Check for PO Box
   if (!memcmp(pAdr, "PO BOX", 6) || !memcmp(pAdr, "P.O. BOX", 8))
   {
      strcpy(pAdrRec->strName, pAdr);
      return;
   }

   strcpy(acAdr, pAdr);
   iCnt = ParseString(acAdr, ' ', 16, apItems);
   if (iCnt < 2)
   {
      strcpy(pAdrRec->strName, pAdr);
      pAdrRec->lStrNum = 0;
      return;
   }

   // If strNum is 0, do not parse address
   iIdx = 0;
   if (isdigit(*apItems[0]))
   {
      iIdx++;
      iTmp = atoi(apItems[0]);
      pAdrRec->lStrNum = iTmp;

      if (pTmp = strchr(apItems[0], '-'))
      {
         strcpy(pAdrRec->strSub, pTmp+1);
         *pTmp = 0;
      }
      strcpy(pAdrRec->strNum, apItems[0]);

   }

   // Check for sub hseno
   if (strchr(apItems[iIdx], '/'))
   {
      strcpy(pAdrRec->strSub, apItems[iIdx]);
      iIdx++;
   }

   // Dir
   bool bDir;
   bDir = isDir(apItems[iIdx]);

   // Possible cases E ST or W RD
   // Check for suffix
   bRet = false;
   acStr[0] = 0;
   if (bDir && iCnt > 2)
   {
      // If next item is suffix, then this one is not direction
      iSfxCode = GetSfxCode(apItems[iIdx+1]);
      if (iSfxCode)
      {
         strcpy(acStr, apItems[iIdx]);
         sprintf(pAdrRec->strSfx, "%d", iSfxCode);
         strcpy(pAdrRec->SfxName, apItems[iIdx+1]);
         bRet = true;         // Done
      } else
      {
         // This is direction
         strcpy(pAdrRec->strDir, apItems[iIdx]);
         iIdx++;
      }
   }

   if (!bRet)
   {
      // Not direction, token is str name
      acTmp[0] = 0;
      iSfxCode = GetSfxCode(apItems[iCnt-1]);
      if (iSfxCode)
      {
         // Multi-word street name with suffix
         sprintf(pAdrRec->strSfx, "%d", iSfxCode);
         strcpy(pAdrRec->SfxName, apItems[iCnt-1]);
         while (iIdx < iCnt-1)
         {
            strcat(acStr, apItems[iIdx++]);
            if (iIdx < iCnt)
               strcat(acStr, " ");
         }
      } else
      {
         // Multi-word street name without suffix
         while (iIdx < iCnt)
         {
            strcat(acStr, apItems[iIdx++]);
            if (iIdx < iCnt)
               strcat(acStr, " ");
         }
      }
   }

   acStr[SIZ_M_STREET] = 0;
   strcpy(pAdrRec->strName, acStr);
}

/******************************** parseAdr1_3() *****************************
 *
 * Parse street number - YOL
 *
 ****************************************************************************/

void parseAdr1_3(ADR_REC *pAdrRec, char *pAdr)
{
   char  acAdr[128], acTmp[128], *apItems[16];
   int   iCnt, iTmp, iTmp2, iTmp3, iIdx;
   char  *pTmp;

   // Clear output buffer
   memset((void *)pAdrRec, 0, sizeof(ADR_REC));

   strcpy(acAdr, pAdr);

   iCnt = ParseString(acAdr, ' ', 16, apItems);
   iIdx = 0;
   if (iCnt == 1)
   {
      if (isNumber(apItems[0]) && iCnt == 1)
      {
         // 718
         iTmp = atoi(apItems[0]);
         pAdrRec->lStrNum = iTmp;
         strcpy(pAdrRec->strNum, apItems[0]);
      }
      else if ((pTmp=strchr(apItems[0], '-')) ||
               (pTmp=strchr(apItems[0], '/')) ||
               (pTmp=strchr(apItems[0], '&'))  )
      {
         // 1014-26, 14-A&B, 1664A/B
         if (*(pTmp-1) >= 'A')
         {
            pTmp--;
            strcpy(pAdrRec->strSub, pTmp);
         }
         *pTmp++ = 0;
         strcpy(pAdrRec->strNum, apItems[0]);
         iTmp = atoi(pAdrRec->strNum);
         pAdrRec->lStrNum = iTmp;

         if (*pTmp >= 'A' && *pTmp <='Z')
            strcpy(pAdrRec->strSub, pTmp);
         else if (*pTmp == '#')
            strcpy(pAdrRec->Unit, pTmp);
         //else
         // 2918&50 or 746-746A or 630/630A - Ignore for now
      }
      else if (pTmp = strchr(apItems[0], '#'))
      {
         // 668#B
         strcpy(pAdrRec->Unit, pTmp);
         *pTmp = 0;
         strcpy(pAdrRec->strNum, apItems[0]);
         iTmp = atoi(pAdrRec->strNum);
         pAdrRec->lStrNum = iTmp;
      }
      else if (isdigit(*apItems[0]))
      {
         // 649B
         strcpy(acTmp, apItems[0]);
         iTmp = iTmp2 = 0;
         while ((acTmp[iTmp] >= '0') && (acTmp[iTmp] <= '9'))
            iTmp++;
         strncpy(pAdrRec->strNum, acTmp, iTmp);
         iTmp3 = atoi(pAdrRec->strNum);
         pAdrRec->lStrNum = iTmp3;

         // 627A
         if (acTmp[iTmp] >= 'A' && acTmp[iTmp] <= 'Z')
            strcpy(pAdrRec->strSub, &acTmp[iTmp]);
      }
   } else if (iCnt == 2)
   {
      if (pTmp = strchr(apItems[0], '#'))
      {
         // 803#  50
         *pTmp = 0;
         strcpy(pAdrRec->strNum, apItems[0]);
         iTmp = atoi(pAdrRec->strNum);
         pAdrRec->lStrNum = iTmp;
         sprintf(pAdrRec->Unit, "#%s", apItems[1]);
         *apItems[1] = 0;
      } else
      {
         iTmp = atoi(apItems[0]);
         pAdrRec->lStrNum = iTmp;
         strcpy(pAdrRec->strNum, apItems[0]);
      }

      // 937 &1/2
      if (*apItems[1] == '&')
      {
         strcpy(pAdrRec->strSub, &apItems[1][1]);
      } else
      {
         // 239 N
         iIdx = iTmp3 = 0;
         for (iIdx=0;iIdx < iCnt;iIdx++)
         {
            // WOLFSKILL ST
            while (asDir[iTmp3])
            {
               if (!strcmp(apItems[iIdx], asDir[iTmp3]))
               {
                  strcpy(pAdrRec->strDir, apItems[iIdx]);
                  break;
               }
               iTmp3++;
            }
         }

         if (strchr(apItems[1], '/'))
         {
            // 168 1/2 or 104 A/B
            strcpy(pAdrRec->strSub, apItems[1]);
         } else if (*apItems[1])
         {
            // 803#  50
            if (*apItems[1] == '#')
               strcpy(pAdrRec->Unit, apItems[1]);
            else if (!memcmp(apItems[1], "FL", 2) )
               strcpy(pAdrRec->strNum, pAdr);
            else
               strcpy(pAdrRec->strSub, apItems[1]);
         }
      }
   }
}

/******************************** parseAdr1_4() *****************************
 *
 * Parse street name - YOL
 *
 ****************************************************************************/

void parseAdr1_4(ADR_REC *pAdrRec, char *pAdr, bool bReset)
{
   char  acAdr[128], acStr[64], *apItems[16];
   int   iCnt, iIdx, iSfxCode, iDir, iNum;
   char  *pTmp;

   // Clear output buffer
   if (bReset)
      memset((void *)pAdrRec, 0, sizeof(ADR_REC));

   // Separate county road from road number
   if (!memcmp(pAdr, "CR", 2) && isdigit(*(pAdr+2)))
      sprintf(acAdr, "%.2s %s", pAdr, pAdr+2);
   else
      strcpy(acAdr, pAdr);

   // Strip off unwanted chars
   if ((pTmp = strchr(acAdr, ',')) || (pTmp = strchr(acAdr, '&')) || (pTmp = strchr(acAdr, '*')))
      *pTmp = 0;
   if ((pTmp = strchr(acAdr, '(')) || (pTmp = strchr(acAdr, '/')) )
      *pTmp = 0;

   // Check for unit#
   if (pTmp = strchr(acAdr, '#'))
   {
      strcpy(pAdrRec->Unit, pTmp);
      *pTmp = 0;
   } else if (pTmp = strstr(acAdr, " STE "))
   {
      sprintf(pAdrRec->Unit, "#%s", pTmp+5);
      *pTmp = 0;
   }

   // Parse strName
   iCnt = ParseString(acAdr, ' ', 16, apItems);
   if (iCnt == 1)
   {
      strcpy(pAdrRec->strName, apItems[0]);
      return;
   }

   // Check for direction
   iIdx=iDir=0;
   //while (asDir[iDir])
   //{
   //   if (!strcmp(apItems[0], asDir[iDir]))
   //   {
   //      iIdx = 1;
   //      strcpy(pAdrRec->strDir, apItems[0]);
   //      break;
   //   }
   //   iDir++;
   //}
   if (isDir(apItems[0]))
   {
      iIdx = 1;
      strcpy(pAdrRec->strDir, apItems[0]);
   }

   // Now next token must be strName
   // For now, we throw away every thing after suffix except unit# indicated by '#'
   strcpy(acStr, apItems[iIdx++]);
   strcat(acStr, " ");
   iNum=iSfxCode = 0;
   for (;iIdx < iCnt; iIdx++)
   {
      // Suffix
      if (!iSfxCode)
      {
         iSfxCode = GetSfxDev(apItems[iIdx]);
         if (iSfxCode)
         {
            sprintf(pAdrRec->SfxCode, "%d", iSfxCode);
            strcpy(pAdrRec->strSfx, apItems[iIdx]);
            strcpy(pAdrRec->SfxName, apItems[iIdx]);
            break;
         }
      }

      // COUNTY ROAD 42
      if (!iNum)
      {
         strcat(acStr, apItems[iIdx]);
         strcat(acStr, " ");
         iNum = atoi(apItems[iIdx]);
      } else
         break;   // It is invalid to have two number on a street name
   }

   // Translate CR to COUNTY ROAD
   if (!memcmp(acStr, "CR ", 3))
   {
      sprintf(acAdr, "COUNTY ROAD %s", &acStr[3]);
      strcpy(pAdrRec->strName, acAdr);
   } else
   {
      strncpy(pAdrRec->strName, acStr, SIZ_M_STREET);
   }
}

/******************************** parseAdr1_5() *****************************
 *
 * Parse address line 1 into individual elements.  If no street number, parse
 * street name and suffix.  Suffix will be coded - For MPA
 *
 ****************************************************************************/

//void parseAdr1_5(ADR_REC *pAdrRec, char *pAdr, char *pApn)
//{
//   char  acAdr[128], acStr[64], acTmp[128], *apItems[16], acCode[32], acSave[128];
//   int   i, j, iCnt, iTmp, iIdx, iName, iCity, iMax=0;
//
//   // Clear output buffer
//   memset((void *)pAdrRec, 0, sizeof(ADR_REC));
//
//   // Check for PO Box
//   if (!memcmp(pAdr, "PO BOX", 6) || !memcmp(pAdr, "P.O. BOX", 8))
//   {
//      strcpy(pAdrRec->strName, pAdr);
//      return;
//   }
//
//   strcpy(acAdr, pAdr);
//   iCnt = ParseString(acAdr, ' ', 16, apItems);
//   if (iCnt < 2)
//   {
//      strcpy(pAdrRec->strName, pAdr);
//      pAdrRec->lStrNum = 0;
//      return;
//   }
//
//   iCity = iCnt - 3;
//
//   // Pick street number
//   iIdx = 0;
//   if (isNumber(apItems[0]))
//   {
//      iIdx++;
//      iTmp = atoi(apItems[0]);
//      pAdrRec->lStrNum = iTmp;
//      strcpy(pAdrRec->strNum, apItems[0]);
//   }
//
//   strcpy(pAdrRec->Zip, apItems[iCnt-1]);
//   strcpy(pAdrRec->State, apItems[iCnt-2]);
//
//   // Parse city name
//   for (i=iCity; i > 0; i--)
//   {
//      strcpy(acSave, apItems[i]);
//      City2Code2(apItems[i], acCode, pApn);
//      if (acCode[0] > ' ')
//      {
//         strcpy(pAdrRec->City, apItems[i]);
//         iName = i;
//         break;
//      } else
//      {
//         if (iMax > 2)
//            break;
//         iMax++;
//         strcpy(acTmp, " ");
//         strcat(acTmp, apItems[i]);
//         strcat(apItems[i-1], acTmp);
//      }
//   }
//   if (acCode[0] < '0')
//   {
//      LogMsg0("Invalid city name %.30s [%.14s]", apItems[iCity], pApn);
//      iBadCity++;
//      iName = iCity;
//   }
//
//   // Parse strName
//   acStr[0] = 0;
//   for (j=1; j < iName; j++)
//   {
//      if (j == 1)
//      {
//         // If first letter is not number
//         if (!isdigit(apItems[j][0]) )
//         {
//            if (!memcmp(apItems[j], "SP", 2) && isdigit(*(apItems[j]+2)))
//            {
//               strcpy(pAdrRec->Unit, apItems[j]);
//            } else if (!strcmp(apItems[j], "SO") && !strcmp(apItems[j], "NO"))
//            {
//               pAdrRec->strDir[0] = *apItems[j];
//            } else if (acStr[0] > ' ')
//            {
//               strcat(acStr, " ");
//               strcat(acStr, apItems[j]);
//            } else
//               strcat(acStr, apItems[j]);
//         } else
//         {
//            // First letter is a number - could be number street name
//            strcpy(pAdrRec->Xtras, apItems[j]);
//         }
//      } else
//      {
//         if (!memcmp(apItems[j], "SP", 2) && isdigit(*(apItems[j]+2)))
//         {
//            strcpy(pAdrRec->Unit, apItems[j]);
//         } else if (!strcmp(apItems[j], "SO") || !strcmp(apItems[j], "NO"))
//         {
//            pAdrRec->strDir[0] = *apItems[j];
//         } else if (!strcmp(apItems[j], "W") || !strcmp(apItems[j], "E") ||
//                     !strcmp(apItems[j], "N") || !strcmp(apItems[j], "S") )
//         {
//            pAdrRec->strDir[0] = *apItems[j];
//         } else if (acStr[0] > ' ')
//         {
//            strcat(acStr, " ");
//            strcat(acStr, apItems[j]);
//         } else
//            strcat(acStr, apItems[j]);
//      }
//   }
//
//   if (acStr[0] > ' ')
//      strncpy(pAdrRec->strName, acStr, SIZ_S_STREET);
//   else if (pAdrRec->Xtras[0] > ' ')
//   {
//      strncpy(pAdrRec->strName, pAdrRec->Xtras, SIZ_S_STREET);
//      pAdrRec->Xtras[0] = 0;
//   } else 
//      iTmp = 0;
//
//}

/********************************** parseAdr2() *****************************
 *
 * Parse address line 2 into individual elements.  Take zip4
 * 10/01/2015: Modify to accommodate foreign zipcode in SBD
 * 12/21/2015: Fix bug when Zip code len is > 5 but < 9 (NAP)
 *
 ****************************************************************************/

void parseAdr2(ADR_REC *pAdrRec, char *pAdr)
{
   char  acAdr[256], acTmp[256], acZip[32], *apItems[10], *pTmp;
   int   iCnt, iTmp, iIdx, iZipLen;

   // Remove comma in address line 1
   pTmp = pAdr;
   iIdx = 0;
   while (*pTmp)
   {
      if (*pTmp == ',')
         acAdr[iIdx++] = ' ';
      else
         acAdr[iIdx++] = *pTmp;
      pTmp++;
   }
   acAdr[iIdx++] = 0;

   iCnt = ParseString(acAdr, 32, 10, apItems);
   // Possible bad char
   if (iCnt > 1 && strlen(apItems[iCnt-1]) == 1)
      iCnt--;

   iZipLen = strlen(apItems[iCnt-1]);
   iTmp = atoi(apItems[iCnt-1]);
   if (iTmp > 0 && (iZipLen == 5 || iZipLen == 9 || iZipLen == 10))
   {
      iCnt--;
      strcpy(acZip, apItems[iCnt]);
      if (pTmp = strchr(acZip, '-'))
      {
         *pTmp = 0;
         strcpy(pAdrRec->Zip, acZip);
         strcpy(pAdrRec->Zip4, pTmp+1);
      } else if (iZipLen == 9)
      {
         memcpy(pAdrRec->Zip, acZip, 5);
         memcpy(pAdrRec->Zip4, &acZip[5], 4);
      } else
         strcpy(pAdrRec->Zip, acZip);
   } else if (iTmp > 0)
   {
      apItems[iCnt-1][5] = 0;
      strcpy(pAdrRec->Zip, apItems[iCnt-1]);
   }

   if (iCnt > 1)
   {
      // If last item is not state abbr, it may be part of city name
      if (strlen(apItems[iCnt-1]) == 2)
      {
         iCnt--;
         memcpy(pAdrRec->State, apItems[iCnt], 2);
      }

      acTmp[0] = 0;
      for (iTmp = 0; iTmp < iCnt; iTmp++)
      {
         strcat(acTmp, apItems[iTmp]);
         if (iTmp < iCnt-1)
            strcat(acTmp, " ");
      }

      strncpy(pAdrRec->City, acTmp, SIZ_S_CITY);
   } else
   {
      pAdrRec->State[0] = 0;
      if (iCnt == 1)
         strncpy(pAdrRec->City, apItems[0], SIZ_S_CITY);
      else
         pAdrRec->City[0] = 0;
   }
}

/********************************** parseAdr2() *****************************
 *
 * Parse address line 2 into individual elements.  Ignore Zip4
 *
 ****************************************************************************/

void parseAdr2_1(ADR_REC *pAdrRec, char *pAdr)
{
   char  acAdr[256], acTmp[256], *apItems[10], *pTmp;
   int   iCnt, iTmp, iIdx;

   // Remove comma
   pTmp = pAdr;
   iIdx = 0;
   while (*pTmp)
   {
      if (*pTmp == ',')
         acAdr[iIdx++] = ' ';
      else
         acAdr[iIdx++] = *pTmp;
      pTmp++;
   }
   acAdr[iIdx++] = 0;

   iCnt = ParseString(acAdr, 32, 10, apItems);
   // Possible bad char
   if (iCnt > 1 && strlen(apItems[iCnt-1]) == 1)
      iCnt--;

   if (pTmp = strchr(apItems[iCnt-1], '-'))
   {
      *pTmp++ = 0;
      strncpy(pAdrRec->Zip4, pTmp, SIZ_M_ZIP4);
   } else
      pAdrRec->Zip4[0] = 0;

   iTmp = atoi(apItems[iCnt-1]);
   if (iTmp > 0)
   {
      iCnt--;
      strncpy(pAdrRec->Zip, apItems[iCnt], SIZ_M_ZIP);
   }

   if (iCnt > 1)
   {
      // If last item is not state abbr, it may be part of city name
      if (strlen(apItems[iCnt-1]) == 2)
      {
         iCnt--;
         memcpy(pAdrRec->State, apItems[iCnt], 2);
      }

      acTmp[0] = 0;
      for (iTmp = 0; iTmp < iCnt; iTmp++)
      {
         strcat(acTmp, apItems[iTmp]);
         if (iTmp < iCnt-1)
            strcat(acTmp, " ");
      }

      strncpy(pAdrRec->City, acTmp, SIZ_S_CITY);
   } else
   {
      if (iCnt == 1)
         strncpy(pAdrRec->City, apItems[0], SIZ_S_CITY);
      else
         pAdrRec->City[0] = 0;
      pAdrRec->State[0] = 0;
   }
}

/********************************* parseMAdr1() *****************************
 *
 * Parse address line 1 into individual elements (Design for YUB)
 * 11/04/2005 Adding # to unit number (spn)
 * 11/10/2005 Fix "123 HWY 70" problem
 * 09/12/2006 Modify to support NEV, YOL
 * 11/29/2007 Adding acStrName to avoid street name overlaps into suffix  
 * 12/19/2010 If strSub is too long, drop it.
 * 06/27/2012 Add HSENO to output record.
 * 06/20/2017 Fix mail addr for TUL when street name starts with suffix (RD 120)
 * 08/25/2020 Remove space after '#'
 *
 ****************************************************************************/

void parseMAdr1(ADR_REC *pAdrRec, char *pAddr)
{
   char  acAdr[256], acStrName[256], acSfx[64], acTmp[256], *apItems[16], *pTmp;
   char *pAdr;
   int   iCnt, iTmp, iIdx, iDir;
   bool  bRet, bDir;

   // Check for PO Box
   if (!memcmp(pAddr, "PO BOX", 6)   ||
       !memcmp(pAddr, "P.O. BOX", 8) ||
       !memcmp(pAddr, "P O ", 4)     ||
       !memcmp(pAddr, "PO BX", 5)    ||
       !memcmp(pAddr, "BOX ", 4)     ||
       !memcmp(pAddr, "POB ", 4) )
   {
      strcpy(pAdrRec->strName, pAddr);
      return;
   }

   acStrName[0] = 0;
   pAdr = pAddr;
   if (*pAdr == '%')
   {
      pTmp = strchr(pAdr+3, ' ');
      if (pTmp && isdigit(*++pTmp))
         pAdr = pTmp;
      else
         return;        // bad address
   }

   // Remove leading space - SFX
   while (*pAdr && *pAdr == ' ') pAdr++;

   // Remove '.' and extra space
   iTmp = 0;
   while (*pAdr)
   {
      // Remove dot, comma, and single quote
      if (*pAdr != '.' && *pAdr != ',' && *pAdr != 39)
         acAdr[iTmp++] = *pAdr;

      // Remove space between # and unit number
      if (*pAdr == '#' && *(pAdr+1) == ' ')
         pAdr++;

      // Remove multi-space
      if (*pAdr == ' ')
         while (*pAdr == ' ') pAdr++;
      else
         pAdr++;
   }
   acAdr[iTmp] = 0;

   // Tokenize address
   iCnt = ParseString(acAdr, 32, 16, apItems);

   // If strNum is 0, do not parse address
   iTmp = atoi(apItems[0]);
   if (iTmp == 0 || iCnt == 1)
   {
      strcpy(acStrName, pAddr);
      pAdrRec->lStrNum = 0;
   } else
   {
      if (apItems[0][strlen(apItems[0])-1] > '9')
      {
         // StrSub ?
         pTmp = apItems[0];
         while (*pTmp && isdigit(*pTmp) )
            pTmp++;
         if (*pTmp && (*pTmp == '-' || *pTmp == '/'))
            pTmp++;
         if (*pTmp && strlen(pTmp) <= SIZ_M_STR_SUB)
            strcpy(pAdrRec->strSub, pTmp);
      }

      strcpy(pAdrRec->HseNo, apItems[0]);
      pAdrRec->lStrNum = iTmp;
      sprintf(acTmp, "%d", iTmp);
      strcpy(pAdrRec->strNum, acTmp);
      iIdx = 1;

      // Fraction - Make sure this is not the last token
      if (strchr(apItems[iIdx], '/') && (iCnt > (iIdx+1)) && (strlen(apItems[iIdx]) <= SIZ_S_STR_SUB))
      {
         strcpy(pAdrRec->strSub, apItems[iIdx++]);
         if (!strcmp(apItems[iIdx], "RD") || !strcmp(apItems[iIdx], "ROAD") || !strcmp(apItems[iIdx], "AVE"))
         {
            if (iCnt > iIdx+1)
            {
               sprintf(acStrName, "%s %s", apItems[iIdx], apItems[iIdx+1]);
               iIdx += 2;
               if (iIdx >= iCnt)
               {
                  strcpy(pAdrRec->strName, acStrName);
                  return;
               }
            }
         }
      }

      // Unit is sometimes placed after strnum as in: 9416 #A CARLTON OAKS
      if (apItems[iIdx][0] == '#')
      {
         strncpy(pAdrRec->Unit, (char *)&apItems[iIdx][0], SIZ_M_UNITNO);
         strncpy(pAdrRec->UnitNox, (char *)&apItems[iIdx][0], SIZ_M_UNITNOX);
         iIdx++;
      }

      if (iIdx >= iCnt)
      {
         // Bad address
         return;
      }

      // Dir
      iDir = 0;
      bDir = false;
      strcpy(acSfx, apItems[iIdx]);
      if (strlen(apItems[iIdx]) < 3)
      {
         if (acSfx[1] == '.')
            acSfx[1] = 0;

         while (iDir <= 11)
         {
            if (!strcmp(acSfx, asDir[iDir]))
            {
               bDir = true;
               break;
            }
            iDir++;
         }
         if (!bDir)
         {
            iDir = 0;
            while (iDir <= 1)
            {
               if (!strcmp(acSfx, asDir2[iDir]))
               {
                  bDir = true;
                  break;
               }
               iDir++;
            }
         }
      } else
      {
         // Check for long direction
         while (iDir < 4)
         {
            if (!strcmp(apItems[iIdx], asDir1[iDir]))
            {
               bDir = true;
               break;
            }
            iDir++;
         }
      }

      // Possible cases E ST or W RD
      // Check for suffix
      bRet = false;
      //if (iTmp <= 7 && iCnt > 2)
      if (bDir && iCnt > 2)
      {
         // If next item is suffix, then this one is not direction
         // if (iTmp=GetSfxCode(apItems[iIdx+1]) && iCnt==iIdx+2)
         // The above statement won't work if Unit# is included.
         //if ((iTmp=GetSfxDev(apItems[iIdx+1])) &&
         if ((iTmp=GetSfxCodeX(apItems[iIdx+1], acTmp)) &&
             memcmp(apItems[iIdx+1], "HWY", 3) && 
             memcmp(apItems[iIdx+1], "HIGHWAY", 7)) 
         {
            strncpy(acStrName, apItems[iIdx], SIZ_M_STREET);
            //strcpy(pAdrRec->strSfx, GetSfxStr(iTmp));
            strcpy(pAdrRec->strSfx, acTmp);
            sprintf(acTmp, "%d", iTmp);
            strcpy(pAdrRec->SfxCode, acTmp);
            bRet = true;         // Done
         } else
         {
            // This is direction
            strcpy(pAdrRec->strDir, asDir[iDir]);
            iIdx++;
         }
      }

      // Check for unit #
      if (apItems[iCnt-1][0] == '#')
      {
         strncpy(pAdrRec->Unit, (char *)&apItems[iCnt-1][0], SIZ_M_UNITNO);
         strncpy(pAdrRec->UnitNox, (char *)&apItems[iCnt-1][0], SIZ_M_UNITNOX);
         iCnt--;

         // If last token is STE or SUITE, remove it.
         if (!strcmp(apItems[iCnt-1], "STE") || !strcmp(apItems[iCnt-1], "SUITE")
            || !strcmp(apItems[iCnt-1], "UNIT") || !strcmp(apItems[iCnt-1], "APT")
            || !strcmp(apItems[iCnt-1], "SP") || !strcmp(apItems[iCnt-1], "RM") )
            iCnt--;
      } else if (!strcmp(apItems[iCnt-2], "STE") || !strcmp(apItems[iCnt-2], "SUITE")
         || !strcmp(apItems[iCnt-2], "UNIT") || !strcmp(apItems[iCnt-2], "UNIT#")
         || !strcmp(apItems[iCnt-2], "APT")  || !strcmp(apItems[iCnt-2], "APTS") || !strcmp(apItems[iCnt-2], "APT#")
         || !strcmp(apItems[iCnt-2], "RM")   || !strcmp(apItems[iCnt-2], "SP")   || !strcmp(apItems[iCnt-2], "LOT") )
      {
         sprintf(acTmp, "#%s", apItems[iCnt-1]);
         strncpy(pAdrRec->Unit, acTmp, SIZ_M_UNITNO);
         strncpy(pAdrRec->UnitNox, acTmp, SIZ_M_UNITNOX);
         iCnt -= 2;
      } else if ((pTmp=strchr(apItems[iCnt-1], '#')))
      {  // 15025 NE SACRAMENTO ST#54, 838 S GRAND AVE APT#611
         if (!memcmp(apItems[iCnt-1], "SP", 2))
         {
            *pTmp = ' ';
         } else
         {
            strncpy(pAdrRec->Unit, pTmp, SIZ_M_UNITNO);
            strncpy(pAdrRec->UnitNox, pTmp, SIZ_M_UNITNOX);
            *pTmp = 0;
            if (!strcmp(apItems[iCnt-1], "APT"))
               iCnt--;
         }
      } else if (!memcmp(apItems[iCnt-1], "FL-", 3) || !memcmp(apItems[iCnt-1], "FLR-", 4))
      {
         strncpy(pAdrRec->Unit, apItems[iCnt-1], SIZ_M_UNITNO);
         strncpy(pAdrRec->UnitNox, apItems[iCnt-1], SIZ_M_UNITNOX);
         iCnt--;
      } else if (!strcmp(apItems[iCnt-2], "LOT") && atoi(apItems[iCnt-1]) > 0)
      {  // 245 WILDWOOD DR LOT 183, 45055 E FLORIDA AVE SP 36
         sprintf(acTmp, "#%s     ", apItems[iCnt-1]);
         strncpy(pAdrRec->Unit, acTmp, SIZ_M_UNITNO);
         strncpy(pAdrRec->UnitNox, acTmp, SIZ_M_UNITNOX);
         iCnt -= 2;
      } else if (iCnt > 3 && atoi(apItems[iCnt-1]) > 0 && acStrName[0] <= ' ')
      {  // 555 N DANEBO 33
         // Not 21051 STATE ROUTE 772, 5323 HERMITAGE AVE# 1, 3100-R WEST HWY 86
         if (strcmp(apItems[iCnt-2], "ROUTE") && strcmp(apItems[iCnt-2], "HWY")
            && strcmp(apItems[iCnt-2], "BOX") && strcmp(apItems[iCnt-2], "ROAD")
            && strcmp(apItems[iCnt-2], "SP")  && strcmp(apItems[iCnt-2], "HIGHWAY"))   // MER
         {
            // Check for 123 COUNTY RD 105 on SIS->001110050000
            if (!strstr(pAddr, "COUNTY RD"))
            {
               sprintf(acTmp, "#%s     ", apItems[iCnt-1]);
               strncpy(pAdrRec->Unit, acTmp, SIZ_M_UNITNO);
               strncpy(pAdrRec->UnitNox, acTmp, SIZ_M_UNITNOX);
               iCnt--;
            } else
            {
               sprintf(acStrName, "COUNTY ROAD %s", apItems[iCnt-1]);
               bRet = true;
            }

         }

         if (pTmp=strchr(apItems[iCnt-1], '#'))
            *pTmp = 0;
      }

      // Not direction, token is str name
      if (!bRet && iIdx < iCnt)
      {
         // Check suffix
         //if (iTmp=GetSfxDev(apItems[iCnt-1]))
         if (iTmp=GetSfxCodeX(apItems[iCnt-1], acTmp))
         {
            strcpy(pAdrRec->strSfx, acTmp);
            //strcpy(pAdrRec->strSfx, GetSfxStr(iTmp));
            sprintf(acTmp, "%d", iTmp);
            strcpy(pAdrRec->SfxCode, acTmp);

            while (iIdx < iCnt-1)
            {
               // If multiple address, take the first one.
               // This loop break after first suffix found, ignore the rest.
               // We also must avoid strname like 123 ST PAUL BLVD.  StrSfx must
               // be indexed > 1
               if ((iIdx > 1) &&
                   (iIdx < iCnt-2) &&
                   (iTmp=GetSfxCodeX(apItems[iIdx], acTmp)) )
                   //(iTmp=GetSfxDev(apItems[iIdx])) )
               {
                  //strcpy(pAdrRec->strSfx, GetSfxStr(iTmp));
                  strcpy(pAdrRec->strSfx, acTmp);
                  sprintf(acTmp, "%d", iTmp);
                  strcpy(pAdrRec->SfxCode, acTmp);
                  break;
               }
               strcat(acStrName, apItems[iIdx++]);
               if (iIdx < iCnt)
                  strcat(acStrName, " ");
            }
         } else if (!strcmp(apItems[iCnt-2], "HWY"))
         {  // 123 HWY 70
            //strcpy(pAdrRec->strName, apItems[iCnt-2]);
            //strcat(pAdrRec->strName, " ");
            //strcat(pAdrRec->strName, apItems[iCnt-1]);
            sprintf(acStrName, "%s %s", apItems[iCnt-2], apItems[iCnt-1]);
         } else
         {
            // Scan for suffix in multi address cases.  Keep the first one
            // 123 MAIN ST 540 N SANTA ANA BLVD
            while (iIdx < iCnt)
            {
               if (iIdx > 1)
               {
                  //if (iTmp = GetSfxDev(apItems[iIdx]))
                  if (iTmp = GetSfxCodeX(apItems[iIdx], acTmp))
                  {
                     //strcpy(pAdrRec->strSfx, pTmp);
                     strcpy(pAdrRec->strSfx, acTmp);
                     sprintf(acTmp, "%d", iTmp);
                     strcpy(pAdrRec->SfxCode, acTmp);
                     break;
                  }
               }

               //if (strlen(apItems[iIdx]) > SIZ_M_STREET)
               //   memcpy(acStrName, apItems[iIdx], SIZ_M_STREET);
               //else
               strcat(acStrName, apItems[iIdx]);
               iIdx++;
               if (iIdx < iCnt)
                  strcat(acStrName, " ");
            }
         }
      }
   }
   strncpy(pAdrRec->strName, acStrName, SIZ_M_STREET);
}

/******************************* parseMAdr1_1() *****************************
 *
 * Design for NAP, 
 * Fix 10/31/2007 for PLA
 * Fix 02/01/2011 for SHA  1010ANDOVER PARK EAST STE #200
 * Fix 05/15/2024 Populate UnitNox when value not fit in Unit for PLA  
 *                "394 PACIFIC AVE 2ND FLR" & "1300 EVANS AVE #880154"

 ****************************************************************************/

int parseMAdr1_1(ADR_REC *pAdrRec, char *pAddr)
{
   char  acAdr[256], acStr[256], acTmp[256], acSfx[64], *apItems[16], *pTmp;
   char *pAdr;
   int   iCnt, iTmp, iIdx, iDir, iSfx, iRet=0;
   bool  bRet, bDir;

   // Check for PO Box
   if (!memcmp(pAddr, "PO BOX", 6) ||
       !memcmp(pAddr, "P.O. BOX", 8) ||
       !memcmp(pAddr, "P O ", 4) ||
       !memcmp(pAddr, "PO BX", 5))
   {
      // Fix 11/27/2010 where StrName is too long and not parsable
      strncpy(pAdrRec->strName, pAddr, SIZ_M_STREET);
      return -1;
   }

   pAdr = pAddr;
   if (*pAdr == '%')
   {
      pTmp = strchr(pAdr+3, ' ');
      if (pTmp && isdigit(*++pTmp))
         pAdr = pTmp;
      else
         return -1;        // bad address
   }

   // Remove leading space - SFX
   while (*pAdr && *pAdr == ' ') pAdr++;

   // Remove '.' and extra space
   iTmp = 0;
   while (*pAdr)
   {
      // Also remove single quote
      if (*pAdr != '.' && *pAdr != ',' && *pAdr != 39)
         acAdr[iTmp++] = *pAdr;

      // Remove space between # and unit number
      if (*pAdr == '#' && *(pAdr+1) == ' ')
         pAdr++;

      // Remove multi-space
      if (*pAdr == ' ')
         while (*pAdr == ' ') pAdr++;
      else
         pAdr++;
   }
   
   acAdr[iTmp] = 0;
   acStr[0] = 0;

   // Tokenize address
   iCnt = ParseString(acAdr, 32, 16, apItems);

   // Check for possible miss keying
   if (strlen(apItems[0]) > 7 && iCnt < 3)
   {
      pTmp = apItems[0];
      while (*pTmp && isdigit(*pTmp))
         pTmp++;
      if (pTmp != apItems[0] && *pTmp && isalpha(*pTmp))
      {
         // Possible 12345MAIN ST
         if (strlen(pTmp) > 2)
         {
            strcpy(acStr, pTmp);
            *pTmp = 0;
         }
      }
   }

   // If strNum is 0, do not parse address
   iTmp = atoi(apItems[0]);
   if (iTmp == 0 || iCnt == 1)
   {
      if (iCnt > 3 && !strcmp(apItems[iCnt-2], "SUITE"))
      {
         sprintf(acTmp, "#%s", apItems[iCnt-1]);
         strncpy(pAdrRec->Unit, acTmp, SIZ_M_UNITNO);
         strncpy(pAdrRec->UnitNox, acTmp, SIZ_M_UNITNOX);
         iCnt -= 2;

         // THREE LAGOON DR SUITE 400
         if (iSfx=GetSfxDev(apItems[iCnt-1]))
         {
            strcpy(pAdrRec->strSfx, GetSfxStr(iSfx));
            iCnt--;
         }
         
         for (iIdx = 0; iIdx <iCnt; iIdx++)
         {
            strcat(acStr, apItems[iIdx]);
            strcat(acStr, " ");
         }
      } else
         strcpy(acStr, pAddr);
      pAdrRec->lStrNum = 0;
   } else
   {
      iIdx = 1;
      pAdrRec->lStrNum = iTmp;
      strncpy(pAdrRec->strNum, apItems[0], SIZ_M_STRNUM);
      if (apItems[0][strlen(apItems[0])-1] > '9')
      {
         // StrSub ?
         pTmp = apItems[0];
         while (*pTmp && isdigit(*pTmp) )
            pTmp++;
         if (*pTmp && (*pTmp == '-' || *pTmp == '/'))
            pTmp++;
         if (*pTmp)
         {
            memset(pAdrRec->strNum, 0, SIZ_M_STRNUM);
            sprintf(pAdrRec->strNum, "%d", pAdrRec->lStrNum);
            if (*pTmp && strlen(pTmp) <= SIZ_M_STR_SUB)
               strncpy(pAdrRec->strSub, pTmp, SIZ_M_STR_SUB);
            else // StrName ?
            {
               strcpy(apItems[0], pTmp);
               iIdx = 0;
            }
         }
      }

      // Fraction - Make sure this is not the last token
      if (strchr(apItems[iIdx], '/') && (iCnt > (iIdx+1)) && (strlen(apItems[iIdx]) <= SIZ_S_STR_SUB))
         strncpy(pAdrRec->strSub, apItems[iIdx++], SIZ_M_STR_SUB);

      // Unit is sometimes placed after strnum as in: 9416 #A CARLTON OAKS
      if (apItems[iIdx][0] == '#')
      {
         strncpy(pAdrRec->Unit, (char *)&apItems[iIdx][0], SIZ_M_UNITNO);
         strncpy(pAdrRec->UnitNox, (char *)&apItems[iIdx][0], SIZ_M_UNITNOX);
         iIdx++;
      }

      // Dir
      iDir = 0;
      bDir = false;
      if (iIdx < iCnt)
      {
         if (strlen(apItems[iIdx]) < 3)
         {
            strcpy(acSfx, apItems[iIdx]);
            if (acSfx[1] == '.')
               acSfx[1] = 0;

            while (iDir <= 11)
            {
               if (!strcmp(acSfx, asDir[iDir]))
               {
                  bDir = true;
                  break;
               }
               iDir++;
            }
         } else
         {
            // Check for long direction
            while (iDir < 4)
            {
               if (!strcmp(apItems[iIdx], asDir1[iDir]))
               {
                  bDir = true;
                  break;
               }
               iDir++;
            }
         }
      }

      // Possible cases E ST or W RD
      // Check for suffix
      bRet = false;
      //if (iTmp <= 7 && iCnt > 2)
      if (bDir && (iCnt > 2) && (iCnt > iIdx+1))
      {
         // If next item is suffix, then this one is not direction
         // if (iTmp=GetSfxCode(apItems[iIdx+1]) && iCnt==iIdx+2)
         // The above statement won't work if Unit# is included.
         if (iSfx=GetSfxDev(apItems[iIdx+1]))
         {
            strcpy(acStr, apItems[iIdx]);
            strcpy(pAdrRec->strSfx, GetSfxStr(iSfx));
            bRet = true;         // Done
         } else
         {
            // This is direction
            strcpy(pAdrRec->strDir, asDir[iDir]);
            iIdx++;
         }
      }

      // Check for unit #
      if (apItems[iCnt-1][0] == '#')
      {
         if (memcmp(apItems[iCnt-2], "SP", 2) && (strlen(apItems[iCnt-1]) < SIZ_M_UNITNO))
         {
            // Skip #
            strncpy(pAdrRec->Unit, (char *)&apItems[iCnt-1][0], SIZ_M_UNITNO);
            strncpy(pAdrRec->UnitNox, (char *)&apItems[iCnt-1][0], SIZ_M_UNITNOX);
            iCnt--;

            // If last token is STE or SUITE, remove it.
            if (!strcmp(apItems[iCnt-1], "STE") || !strcmp(apItems[iCnt-1], "SUITE")
               || !strcmp(apItems[iCnt-1], "UNIT") || !strcmp(apItems[iCnt-1], "APT")  )
               iCnt--;
         } else if (memcmp(apItems[iCnt-2], "SP", 2))
         {
            strncpy(pAdrRec->UnitNox, (char *)&apItems[iCnt-1][0], SIZ_M_UNITNOX);
            iCnt--;
         } else if (!memcmp(apItems[iCnt-2], "SP", 2))
         {
            strcpy(pAdrRec->Unit, apItems[iCnt-1]);
            sprintf(pAdrRec->UnitNox, "%s %s", apItems[iCnt-2], apItems[iCnt-1]);
            iCnt -= 2;
         }
      } else if (!strcmp(apItems[iCnt-2], "STE") || !strcmp(apItems[iCnt-2], "RM")
         || !strcmp(apItems[iCnt-2], "UNIT") || !strcmp(apItems[iCnt-2], "UNIT#")
         || !strcmp(apItems[iCnt-2], "APT") || !strcmp(apItems[iCnt-2], "APT#") )
      {
         sprintf(acTmp, "#%s     ", apItems[iCnt-1]);
         strncpy(pAdrRec->Unit, acTmp, SIZ_M_UNITNO);
         strncpy(pAdrRec->UnitNox, acTmp, SIZ_M_UNITNOX);
         iCnt -= 2;
      } else if (!strcmp(apItems[iCnt-2], "BLDG") )
      {
         sprintf(acTmp, "#%s", apItems[iCnt-1]);
         strcpy(pAdrRec->Unit, acTmp);
         strcpy(pAdrRec->UnitNox, acTmp);
         iCnt -= 2;
      } else if (!strcmp(apItems[iCnt-2], "SUITE") && iCnt > 3)
      {
         // Prevent case: 4611 SUITE DR HUNTINGTON BEACH
         if (!GetSfxDev(apItems[iCnt-1]))
         {
            sprintf(acTmp, "#%s     ", apItems[iCnt-1]);
            strncpy(pAdrRec->Unit, acTmp, SIZ_M_UNITNO);
            strncpy(pAdrRec->UnitNox, acTmp, SIZ_M_UNITNOX);
            iCnt -= 2;
         }
      } else if ((pTmp=strchr(apItems[iCnt-1], '#')))
      {  // 15025 NE SACRAMENTO ST#54, 838 S GRAND AVE APT#611
         if (!memcmp(apItems[iCnt-1], "SP", 2))
         {
            strncpy(pAdrRec->Unit, pTmp, SIZ_M_UNITNO);
            *pTmp = ' ';
            strncpy(pAdrRec->UnitNox, apItems[iCnt-1], SIZ_M_UNITNOX);
         } else
         {
            strncpy(pAdrRec->Unit, pTmp, SIZ_M_UNITNO);
            strncpy(pAdrRec->UnitNox, pTmp, SIZ_M_UNITNOX);
            *pTmp = 0;
            if (!strcmp(apItems[iCnt-1], "APT"))
               iCnt--;
         }
      } else if (!strcmp(apItems[iCnt-2], "LOT") && atoi(apItems[iCnt-1]) > 0)
      {  // 245 WILDWOOD DR LOT 183, 45055 E FLORIDA AVE SP 36
         sprintf(acTmp, "#%s     ", apItems[iCnt-1]);
         strncpy(pAdrRec->Unit, acTmp, SIZ_M_UNITNO);
         strncpy(pAdrRec->UnitNox, acTmp, SIZ_M_UNITNOX);
         iCnt -= 2;
      } else if (iCnt > 3 && atoi(apItems[iCnt-1]) > 0 && !strchr(apItems[iCnt-1], '/'))
      {  // 555 N DANEBO 33
         // Not 21051 STATE ROUTE 772, 5323 HERMITAGE AVE# 1, 3100-R WEST HWY 86
         if (strcmp(apItems[iCnt-2], "ROUTE") && strcmp(apItems[iCnt-2], "HWY")
            && strcmp(apItems[iCnt-2], "BOX") && strcmp(apItems[iCnt-2], "ROAD")
            && strcmp(apItems[iCnt-2], "SP")  && strcmp(apItems[iCnt-2], "HIGHWAY"))   // MER
         {
            // Check for 123 COUNTY RD 105 on SIS->001110050000
            if (!strstr(pAddr, "COUNTY RD"))
            {
               sprintf(acTmp, "#%s     ", apItems[iCnt-1]);
               strncpy(pAdrRec->Unit, acTmp, SIZ_M_UNITNO);
               strncpy(pAdrRec->UnitNox, acTmp, SIZ_M_UNITNOX);
               iCnt--;
            } else
            {
               sprintf(acStr, "COUNTY ROAD %s", apItems[iCnt-1]);
               bRet = true;
            }
         }

         if (pTmp=strchr(apItems[iCnt-1], '#'))
            *pTmp = 0;
      } else if (!strcmp(apItems[iCnt-1], "FLR"))
      {
         sprintf(pAdrRec->UnitNox, "%s %s", apItems[iCnt-2], apItems[iCnt-1]);
      }

      // Not direction, token is str name
      if (!bRet && iIdx < iCnt)
      {
         // Check suffix
         if (iSfx=GetSfxDev(apItems[iCnt-1]))
         {
            strcpy(pAdrRec->strSfx, GetSfxStr(iSfx));
            while (iIdx < iCnt-1)
            {
               strcat(acStr, apItems[iIdx++]);
               if (iIdx < iCnt)
                  strcat(acStr, " ");
            }
         } else if (!strcmp(apItems[iCnt-2], "HWY"))
         {  // 123 HWY 70
            sprintf(acStr, "%s %s", apItems[iCnt-2], apItems[iCnt-1]);
         } else
         {
            iTmp = 0;
            while (iIdx < iCnt)
            {
               strcat(acStr, apItems[iIdx++]);
               if (iIdx < iCnt)
               {
                  if (iSfx=GetSfxDev(apItems[iIdx]))
                  {
                     strcpy(pAdrRec->strSfx, GetSfxStr(iSfx));
                     iRet = 1;
                     break;
                  }
                  strcat(acStr, " ");
                  iTmp = strlen(acStr);

                  if ((iTmp + strlen(apItems[iIdx])) > SIZ_M_STREET)
                  {
                     strcat(acStr, apItems[iIdx]);
                     //sprintf(acTmp, "%s%s", pAdrRec->strName, apItems[iIdx]);
                     //acTmp[SIZ_M_STREET] = 0;
                     //strcpy(pAdrRec->strName, acTmp);
                     iRet = 2;
                     break;
                  }
               }
            }
         }
      }
   }

   strncpy(pAdrRec->strName, acStr, SIZ_M_STREET);

   return iRet;
}

/*****************************************************************************
 *
 * Design for SDX
 *
 * 11/15/2010 Fix bug when strNum and strName has no space in between (i.e. 5068SUNRISE RIDGE TRAIL").
 * 05/20/2024 Populate HseNo with original StrNum.
 * 02/04/2025 Remove hyphen at the end of HseNo.
 *
 *****************************************************************************/

void parseMAdr1_2(ADR_REC *pAdrRec, char *pAddr)
{
   char  acAdr[256], acStr[256], acTmp[256], acSfx[64], *apItems[16], *pTmp;
   char *pAdr;
   int   iCnt, iTmp, iIdx, iDir;
   bool  bRet, bDir;

   // Check for PO Box
   if (!memcmp(pAddr, "PO BOX", 6) ||
       !memcmp(pAddr, "P.O. BOX", 8) ||
       !memcmp(pAddr, "P O ", 4) ||
       !memcmp(pAddr, "PO BX", 5))
   {
      strcpy(pAdrRec->strName, pAddr);
      return;
   }

   pAdr = pAddr;
   if (*pAdr == '%')
   {
      pTmp = strchr(pAdr+3, ' ');
      if (pTmp && isdigit(*++pTmp))
         pAdr = pTmp;
      else
         return;        // bad address
   }

   // Remove leading space - SFX
   while (*pAdr && *pAdr == ' ') pAdr++;

   // Remove '.' and extra space
   iTmp = 0;
   while (*pAdr)
   {
      // Also remove single quote
      if (*pAdr != '.' && *pAdr != 39)
         acAdr[iTmp++] = *pAdr;

      // Remove space between # and unit number
      if (*pAdr == '#' && *(pAdr+1) == ' ')
         pAdr++;

      // Remove multi-space
      if (*pAdr == ' ')
         while (*pAdr == ' ') pAdr++;
      else
         pAdr++;
   }
   
   acAdr[iTmp] = 0;
   acStr[0] = 0;

   // Tokenize address
   iCnt = ParseString(acAdr, 32, 16, apItems);

   // Check for possible miss keying
   if (strlen(apItems[0]) > 7 && iCnt < 3)
   {
      pTmp = apItems[0];
      while (*pTmp && isdigit(*pTmp))
         pTmp++;
      if (pTmp != apItems[0] && *pTmp && isalpha(*pTmp))
      {
         // Possible 12345MAIN ST
         if (strlen(pTmp) > 2)
         {
            strcpy(acStr, pTmp);
            *pTmp = 0;
         }
      }
   }

   // If strNum is 0, do not parse address
   iTmp = atoi(apItems[0]);
   if (iTmp == 0 || iCnt == 1)
   {
      strcpy(acStr, pAddr);
      pAdrRec->lStrNum = 0;
   } else
   {
      if (apItems[0][strlen(apItems[0])-1] > '9')
      {
         // StrSub ?
         pTmp = apItems[0];
         while (*pTmp && isdigit(*pTmp) )
            pTmp++;
         if (*pTmp && (*pTmp == '-' || *pTmp == '/'))
            pTmp++;
         if (*pTmp && strlen(pTmp) < SIZ_M_STR_SUB)
            strcpy(pAdrRec->strSub, pTmp);
         else
            sprintf(acStr, "%s ", pTmp);    // It is part of street name
      }
      pAdrRec->lStrNum = iTmp;
      sprintf(acTmp, "%d", iTmp);
      strcpy(pAdrRec->strNum, acTmp);
      strcpy(pAdrRec->HseNo, apItems[0]);

      // Remove hyphen attached to StrNum
      iTmp = strlen(pAdrRec->HseNo);
      if (pAdrRec->HseNo[iTmp -1] == '-')
         pAdrRec->HseNo[iTmp-1] = 0;

      iIdx = 1;

      // Fraction - Make sure this is not the last token
      if (strchr(apItems[iIdx], '/') && (iCnt > (iIdx+1)) && (strlen(apItems[iIdx]) <= SIZ_S_STR_SUB))
         strcpy(pAdrRec->strSub, apItems[iIdx++]);

      // Unit is sometimes placed after strnum as in: 9416 #A CARLTON OAKS
      if (apItems[iIdx][0] == '#')
      {
         strncpy(pAdrRec->Unit, (char *)&apItems[iIdx][0], SIZ_M_UNITNO);
         strncpy(pAdrRec->UnitNox, (char *)&apItems[iIdx][0], SIZ_M_UNITNOX);
         iIdx++;
      }

      // Dir
      iDir = 0;
      bDir = false;
      if (iIdx < iCnt)
      {
         if (strlen(apItems[iIdx]) < 3)
         {
            strcpy(acSfx, apItems[iIdx]);
            if (acSfx[1] == '.')
               acSfx[1] = 0;

            while (iDir <= 11)
            {
               if (!strcmp(acSfx, asDir[iDir]))
               {
                  bDir = true;
                  break;
               }
               iDir++;
            }
         } else
         {
            // Check for long direction
            while (iDir < 4)
            {
               if (!strcmp(apItems[iIdx], asDir1[iDir]))
               {
                  bDir = true;
                  break;
               }
               iDir++;
            }
         }
      }

      // Possible cases E ST or W RD
      // Check for suffix
      bRet = false;
      //if (iTmp <= 7 && iCnt > 2)
      if (bDir && (iCnt > 2) && (iCnt > iIdx+1))
      {
         // If next item is suffix, then this one is not direction
         // if (iTmp=GetSfxCode(apItems[iIdx+1]) && iCnt==iIdx+2)
         // The above statement won't work if Unit# is included.
         if ((iTmp=GetSfxDev(apItems[iIdx+1])) && (memcmp(apItems[iIdx+1], "MT", 2)))
         {
            strcpy(acStr, apItems[iIdx]);
            strcpy(pAdrRec->strSfx, GetSfxStr(iTmp));
            bRet = true;         // Done
         } else
         {
            // This is direction
            strcpy(pAdrRec->strDir, asDir[iDir]);
            iIdx++;
         }
      }

      // Check for unit #
      if (apItems[iCnt-1][0] == '#')
      {
         if (memcmp(apItems[iCnt-2], "SP", 2))
         {
            // Skip #
            strncpy(pAdrRec->Unit, (char *)&apItems[iCnt-1][0], SIZ_M_UNITNO);
            strncpy(pAdrRec->UnitNox, (char *)&apItems[iCnt-1][0], SIZ_M_UNITNOX);
            iCnt--;

            // If last token is STE or SUITE, remove it.
            if (!strcmp(apItems[iCnt-1], "STE") || !strcmp(apItems[iCnt-1], "SUITE")
               || !strcmp(apItems[iCnt-1], "UNIT") || !strcmp(apItems[iCnt-1], "APT")  )
               iCnt--;
         }
      } else if (!strcmp(apItems[iCnt-2], "STE") || !strcmp(apItems[iCnt-2], "RM")
         || !strcmp(apItems[iCnt-2], "UNIT") || !strcmp(apItems[iCnt-2], "UNIT#")
         || !strcmp(apItems[iCnt-2], "APT") || !strcmp(apItems[iCnt-2], "APT#") )
      {
         sprintf(acTmp, "#%s     ", apItems[iCnt-1]);
         strncpy(pAdrRec->Unit, acTmp, SIZ_M_UNITNO);
         strncpy(pAdrRec->UnitNox, acTmp, SIZ_M_UNITNOX);
         iCnt -= 2;
      } else if (!strcmp(apItems[iCnt-2], "SUITE") && iCnt > 3)
      {
         // Prevent case: 4611 SUITE DR HUNTINGTON BEACH
         if (!GetSfxDev(apItems[iCnt-1]))
         {
            sprintf(acTmp, "#%s     ", apItems[iCnt-1]);
            strncpy(pAdrRec->Unit, acTmp, SIZ_M_UNITNO);
            strncpy(pAdrRec->UnitNox, acTmp, SIZ_M_UNITNOX);
            iCnt -= 2;
         }
      } else if ((pTmp=strchr(apItems[iCnt-1], '#')))
      {  // 15025 NE SACRAMENTO ST#54, 838 S GRAND AVE APT#611
         if (!memcmp(apItems[iCnt-1], "SP", 2))
         {
            *pTmp = ' ';
         } else
         {
            strncpy(pAdrRec->Unit, pTmp, SIZ_M_UNITNO);
            strncpy(pAdrRec->UnitNox, pTmp, SIZ_M_UNITNOX);
            *pTmp = 0;
            if (!strcmp(apItems[iCnt-1], "APT"))
               iCnt--;
         }
      } else if (!strcmp(apItems[iCnt-2], "LOT") && atoi(apItems[iCnt-1]) > 0)
      {  // 245 WILDWOOD DR LOT 183, 45055 E FLORIDA AVE SP 36
         sprintf(acTmp, "#%s     ", apItems[iCnt-1]);
         strncpy(pAdrRec->Unit, acTmp, SIZ_M_UNITNO);
         strncpy(pAdrRec->UnitNox, acTmp, SIZ_M_UNITNOX);
         iCnt -= 2;
      } else if (iCnt > 3 && atoi(apItems[iCnt-1]) > 0 && !strchr(apItems[iCnt-1], '/'))
      {  // 555 N DANEBO 33
         // Not 21051 STATE ROUTE 772, 5323 HERMITAGE AVE# 1, 3100-R WEST HWY 86
         if (strcmp(apItems[iCnt-2], "ROUTE") && strcmp(apItems[iCnt-2], "HWY")
            && strcmp(apItems[iCnt-2], "BOX") && strcmp(apItems[iCnt-2], "ROAD")
            && strcmp(apItems[iCnt-2], "SP")  && strcmp(apItems[iCnt-2], "HIGHWAY"))   // MER
         {
            // Check for 123 COUNTY RD 105 on SIS->001110050000
            if (!strstr(pAddr, "COUNTY RD"))
            {
               sprintf(acTmp, "#%s     ", apItems[iCnt-1]);
               strncpy(pAdrRec->Unit, acTmp, SIZ_M_UNITNO);
               strncpy(pAdrRec->UnitNox, acTmp, SIZ_M_UNITNOX);
               iCnt--;
            } else
            {
               sprintf(acStr, "COUNTY ROAD %s", apItems[iCnt-1]);
               bRet = true;
            }

         }

         if (pTmp=strchr(apItems[iCnt-1], '#'))
            *pTmp = 0;
      }

      // Not direction, token is str name
      if (!bRet && iIdx < iCnt)
      {
         // Check suffix
         if (iTmp=GetSfxDev(apItems[iCnt-1]))
         {
            strcpy(pAdrRec->strSfx, GetSfxStr(iTmp));
            while (iIdx < iCnt-1)
            {
               strcat(acStr, apItems[iIdx++]);
               if (iIdx < iCnt)
                  strcat(acStr, " ");
            }
         } else if (!strcmp(apItems[iCnt-2], "HWY"))
         {  // 123 HWY 70
            sprintf(acStr, "%s %s", apItems[iCnt-2], apItems[iCnt-1]);
         } else
         {
            while (iIdx < iCnt)
            {
               strcat(acStr, apItems[iIdx++]);
               if (iIdx < iCnt)
                  strcat(acStr, " ");
            }
         }
      }
   }

   strncpy(pAdrRec->strName, acStr, SIZ_M_STREET);
}

/*****************************************************************************
 *
 * Design for MRN, MER, SIS, SBT, SAC, YOL
 * 07/20/2018 Add bAddSign to make adding '#' optional.
 * 09/18/2022 Check special case for DNX
 *
 *****************************************************************************/

void parseMAdr1_3(ADR_REC *pAdrRec, char *pAddr, bool bAddSign)
{
   char  acAdr[256], acStr[256], acTmp[256], acSfx[64], acUnit[64], *apItems[16], *pTmp;
   char *pAdr;
   int   iCnt, iTmp, iIdx, iDir;
   bool  bRet, bDir;

   // Check for PO Box
   if (!memcmp(pAddr, "PO BOX", 6) ||
       !memcmp(pAddr, "P.O. BOX", 8) ||
       !memcmp(pAddr, "P O ", 4) ||
       !memcmp(pAddr, "PO BX", 5))
   {
      strncpy(pAdrRec->strName, pAddr, SIZ_M_STREET);
      return;
   } else if (pTmp = strstr(acAdr, "BOX "))
   {
      if ((pTmp - acAdr) < 10)
      {
         strcpy(pAdrRec->strName, acAdr);
         return;
      }

      // DNX: 16340 LOWER HARBOR RD BOX 112
      *pTmp = 0;
      strcpy(acUnit, pTmp+4);
   }

   pAdr = pAddr;
   if (*pAdr == '%')
   {
      pTmp = strchr(pAdr+3, ' ');
      if (pTmp && isdigit(*++pTmp))
         pAdr = pTmp;
      else
         return;        // bad address
   }

   // Remove leading space - SFX
   while (*pAdr && *pAdr == ' ') pAdr++;

   // Remove '.' and extra space
   iTmp = 0;
   while (*pAdr)
   {
      // Also remove single quote
      if (*pAdr != '.' && *pAdr != 39)
         acAdr[iTmp++] = *pAdr;

      // Remove multi-space
      if (*pAdr == ' ')
         while (*pAdr == ' ') pAdr++;
      else
         pAdr++;
   }

   acAdr[iTmp] = 0;
   acStr[0] = 0;
   acUnit[0] = 0;

   // 20110503 - MER
   if (pTmp = strstr(acAdr, " STE "))
   {
      strcpy(acUnit, pTmp+5);
      remChar(acUnit, ' ');
      strncpy(pAdrRec->Unit, acUnit, sizeof(pAdrRec->Unit));
      strncpy(pAdrRec->UnitNox, acUnit, sizeof(pAdrRec->UnitNox));
      *pTmp = 0;
   } else
   if (pTmp = strstr(acAdr, " SP "))
   {
      strcpy(acUnit, pTmp+4);
      remChar(acUnit, ' ');
      strncpy(pAdrRec->Unit, acUnit, sizeof(pAdrRec->Unit));
      strncpy(pAdrRec->UnitNox, acUnit, sizeof(pAdrRec->UnitNox));
      *pTmp = 0;
   }

   // Tokenize address
   iCnt = ParseString(acAdr, 32, 16, apItems);

   // Drop stand alone hyphen
   if (*apItems[iCnt-1] == '-')
      iCnt--;

   if (iCnt > 2 && *apItems[1] == '-')
   {
      for (iTmp = 1; iTmp < iCnt-1; iTmp++)
         apItems[iTmp] = apItems[iTmp+1];
      iCnt--;
   }

   // If strNum is 0, do not parse address
   iTmp = atoi(apItems[0]);
   if (iTmp == 0 || iCnt == 1)
   {
      strncpy(acStr, pAddr, SIZ_M_STREET);
      pAdrRec->lStrNum = 0;
   } else
   {
      iIdx = 0;
      if (apItems[0][strlen(apItems[0])-1] <= '9')
      {
         pAdrRec->lStrNum = iTmp;
      }
      strcpy(pAdrRec->strNum, apItems[0]);
      iIdx++;

      // Fraction - Make sure this is not the last token
      if (strchr(apItems[iIdx], '/') && (iCnt > (iIdx+1)) && (strlen(apItems[iIdx]) <= SIZ_S_STR_SUB))
         strcpy(pAdrRec->strSub, apItems[iIdx++]);

      // Unit is sometimes placed after strnum as in: 9416 #A CARLTON OAKS
      if (apItems[iIdx][0] == '#')
      {
         strncpy(pAdrRec->Unit, (char *)&apItems[iIdx][0], sizeof(pAdrRec->Unit));
         strncpy(pAdrRec->UnitNox, (char *)&apItems[iIdx][0], sizeof(pAdrRec->UnitNox));
         iIdx++;

         // Check for bad addr
         if (iIdx >= iCnt)
            return;
      }

      // Dir
      iDir = 0;
      bDir = false;
      strcpy(acSfx, apItems[iIdx]);
      if (strlen(apItems[iIdx]) < 3)
      {
         if (acSfx[1] == '.')
            acSfx[1] = 0;

         while (asDir[iDir])
         {
            if (!strcmp(acSfx, asDir[iDir]))
            {
               bDir = true;
               break;
            }
            iDir++;
         }
      } else
      {
         // Check for long direction
         while (iDir < 4)
         {
            if (!strcmp(apItems[iIdx], asDir1[iDir]))
            {
               bDir = true;
               break;
            }
            iDir++;
         }
      }

      // Possible cases E ST or W RD
      // Check for suffix
      bRet = false;
      if (bDir && iCnt > 2)
      {
         // If next item is suffix, then this one is not direction
         // if (iTmp=GetSfxCode(apItems[iIdx+1]) && iCnt==iIdx+2)
         // The above statement won't work if Unit# is included.
         if (iTmp=GetSfxDev(apItems[iIdx+1]))
         {
            strcpy(acStr, apItems[iIdx]);
            strcpy(pAdrRec->strSfx, GetSfxStr(iTmp));
            bRet = true;         // Done
         } else
         {
            // This is direction
            strcpy(pAdrRec->strDir, asDir[iDir]);
            iIdx++;
         }
      }

      // Check for unit #
      if (apItems[iCnt-1][0] == '#')
      {
         strncpy(pAdrRec->Unit, (char *)&apItems[iCnt-1][0], sizeof(pAdrRec->Unit));
         strncpy(pAdrRec->UnitNox, (char *)&apItems[iCnt-1][0], sizeof(pAdrRec->UnitNox));
         iCnt--;

         // If last token is STE or SUITE, remove it.
         if (!strcmp(apItems[iCnt-1], "STE") || !strcmp(apItems[iCnt-1], "SUITE")
            || !strcmp(apItems[iCnt-1], "UNIT") || !strcmp(apItems[iCnt-1], "APT")
            || !strcmp(apItems[iCnt-1], "SP") || !strcmp(apItems[iCnt-1], "RM") )
            iCnt--;
      } else if (!strcmp(apItems[iCnt-2], "STE") || !strcmp(apItems[iCnt-2], "SUITE")
         || !strcmp(apItems[iCnt-2], "UNIT") || !strcmp(apItems[iCnt-2], "UNIT#")
         || !strcmp(apItems[iCnt-2], "APT") || !strcmp(apItems[iCnt-2], "APT#")
         || !strcmp(apItems[iCnt-2], "RM") || (!strcmp(apItems[iCnt-2], "SP") && memcmp(apItems[iCnt-1], "AVE", 3)) )
      {
         if (bAddSign)
            sprintf(acTmp, "#%s", apItems[iCnt-1]);
         else
            sprintf(acTmp, "%s", apItems[iCnt-1]);
         strncpy(pAdrRec->Unit, acTmp, sizeof(pAdrRec->Unit));
         strncpy(pAdrRec->UnitNox, acTmp, sizeof(pAdrRec->UnitNox));
         iCnt -= 2;
      } else if ((pTmp=strchr(apItems[iCnt-1], '#')))
      {  // 15025 NE SACRAMENTO ST#54, 838 S GRAND AVE APT#611
         if (!memcmp(apItems[iCnt-1], "SP", 2))
         {
            *pTmp = ' ';
         } else
         {
            strncpy(pAdrRec->Unit, pTmp, sizeof(pAdrRec->Unit));
            strncpy(pAdrRec->UnitNox, pTmp, sizeof(pAdrRec->UnitNox));
            *pTmp = 0;
            if (!strcmp(apItems[iCnt-1], "APT"))
               iCnt--;
         }
      } else if (!strcmp(apItems[iCnt-2], "LOT") && atoi(apItems[iCnt-1]) > 0)
      {  // 245 WILDWOOD DR LOT 183, 45055 E FLORIDA AVE SP 36
         if (bAddSign)
            sprintf(acTmp, "#%s     ", apItems[iCnt-1]);
         else
            sprintf(acTmp, "%s     ", apItems[iCnt-1]);
         strncpy(pAdrRec->Unit, acTmp, sizeof(pAdrRec->Unit));
         strncpy(pAdrRec->UnitNox, acTmp, sizeof(pAdrRec->UnitNox));
         iCnt -= 2;
      } else if (iCnt > 3 && atoi(apItems[iCnt-1]) > 0)
      {  // 555 N DANEBO 33
         // Not 21051 STATE ROUTE 772, 5323 HERMITAGE AVE# 1, 3100-R WEST HWY 86
         if (strcmp(apItems[iCnt-2], "ROUTE") && strcmp(apItems[iCnt-2], "HWY")
            && strcmp(apItems[iCnt-2], "BOX") && strcmp(apItems[iCnt-2], "ROAD")
            && strcmp(apItems[iCnt-2], "SP")  && strcmp(apItems[iCnt-2], "HIGHWAY"))   // MER
         {
            // Check for 123 COUNTY RD 105 on SIS->001110050000
            if (!strstr(pAddr, "COUNTY RD"))
            {
               if (bAddSign)
                  sprintf(acTmp, "#%s", apItems[iCnt-1]);
               else
                  sprintf(acTmp, "%s", apItems[iCnt-1]);

               strncpy(pAdrRec->Unit, acTmp, sizeof(pAdrRec->Unit));
               strncpy(pAdrRec->UnitNox, acTmp, sizeof(pAdrRec->UnitNox));
               iCnt--;
            } else
            {
               sprintf(acStr, "COUNTY ROAD %s", apItems[iCnt-1]);
               bRet = true;
            }

         }

         if (pTmp=strchr(apItems[iCnt-1], '#'))
            *pTmp = 0;
      } 

      // Not direction, token is str name
      if (!bRet)
      {
         // Check suffix
         if (iTmp=GetSfxDev(apItems[iCnt-1]))
         {
            strcpy(pAdrRec->strSfx, GetSfxStr(iTmp));
            while (iIdx < iCnt-1)
            {
               // If multiple address, take the first one.
               // This loop break after first suffix found, ignore the rest.
               if ((iIdx < iCnt-2 && iIdx > 1) && (iTmp=GetSfxDev(apItems[iIdx])) )
               {
                  strcpy(pAdrRec->strSfx, GetSfxStr(iTmp));
                  break;
               }

               if (*apItems[iIdx] == '&' || *apItems[iIdx] == '-')
               {
                  iIdx++;
                  if (iIdx == 2 && iIdx < iCnt-1)
                     iIdx++;
               }
               strcat(acStr, apItems[iIdx++]);
               if (iIdx < iCnt)
                  strcat(acStr, " ");
            }
         } else if (iCnt > 2 && !strcmp(apItems[iCnt-2], "HWY"))
         {  // 123 HWY 70
            sprintf(acStr, "%s %s", apItems[iCnt-2], apItems[iCnt-1]);
         } else
         {
            // Scan for suffix in multi address cases.  Keep the first one
            // 123 MAIN ST 540 N SANTA ANA BLVD
            while (iIdx < iCnt)
            {
               if (iIdx > 1 && (iTmp=GetSfxDev(apItems[iIdx])) )
               {
                  strcpy(pAdrRec->strSfx, GetSfxStr(iTmp));
                  break;
               }

               if (*apItems[iIdx] == '&' || *apItems[iIdx] == '-')
               {
                  iIdx++;
                  if (iIdx == 2 && iIdx < iCnt-1)
                     iIdx++;
               }
               strcat(acStr, apItems[iIdx++]);
               if (iIdx < iCnt)
                  strcat(acStr, " ");
            }
         }
      }
   }

   strncpy(pAdrRec->strName, acStr, SIZ_M_STREET);
}

/******************************************************************************
 *
 * Copy parseMAdr1() and modify for HUM to parse situs from LDR file.
 *
 ******************************************************************************/

void parseMAdr1_4(ADR_REC *pAdrRec, char *pAddr)
{
   char  acAdr[256], acStrName[256], acSfx[64], acTmp[256], *apItems[16], *pTmp;
   char *pAdr;
   int   iCnt, iTmp, iIdx, iDir;
   bool  bRet, bDir;

   // Check for PO Box
   if (!memcmp(pAddr, "PO BOX", 6)   ||
       !memcmp(pAddr, "P.O. BOX", 8) ||
       !memcmp(pAddr, "P O ", 4)     ||
       !memcmp(pAddr, "PO BX", 5)    ||
       !memcmp(pAddr, "BOX ", 4)     ||
       !memcmp(pAddr, "POB ", 4) )
   {
      strcpy(pAdrRec->strName, pAddr);
      return;
   }

   acStrName[0] = 0;
   pAdr = pAddr;
   if (*pAdr == '%')
   {
      pTmp = strchr(pAdr+3, ' ');
      if (pTmp && isdigit(*++pTmp))
         pAdr = pTmp;
      else
         return;        // bad address
   }

   // Remove '.' and extra space
   iTmp = 0;
   while (*pAdr)
   {
      // Remove dot, comma, and single quote
      if (*pAdr != '.' && *pAdr != ',' && *pAdr != 39)
         acAdr[iTmp++] = *pAdr;

      // Remove multi-space
      if (*pAdr == ' ')
         while (*pAdr == ' ') pAdr++;
      else
         pAdr++;
   }
   acAdr[iTmp] = 0;

   // Tokenize address
   iCnt = ParseString(acAdr, 32, 16, apItems);

   // If strNum is 0, do not parse address
   iTmp = atoi(apItems[0]);
   if (iTmp == 0 || iCnt == 1)
   {
      strcpy(acStrName, pAddr);
      pAdrRec->lStrNum = 0;
   } else
   {
      if (apItems[0][strlen(apItems[0])-1] > '9')
      {
         // StrSub ?
         pTmp = apItems[0];
         while (*pTmp && isdigit(*pTmp) )
            pTmp++;
         if (*pTmp && (*pTmp == '-' || *pTmp == '/'))
            pTmp++;
         if (*pTmp && strlen(pTmp) <= SIZ_M_STR_SUB)
            strcpy(pAdrRec->strSub, pTmp);
      }
      pAdrRec->lStrNum = iTmp;
      sprintf(acTmp, "%d", iTmp);
      strcpy(pAdrRec->strNum, acTmp);
      iIdx = 1;

      // Fraction - Make sure this is not the last token
      if (strchr(apItems[iIdx], '/') && (iCnt > (iIdx+1)) && (strlen(apItems[iIdx]) <= SIZ_S_STR_SUB))
         strcpy(pAdrRec->strSub, apItems[iIdx++]);

      // Unit is sometimes placed after strnum as in: 9416 #A CARLTON OAKS
      if (apItems[iIdx][0] == '#')
      {
         strncpy(pAdrRec->Unit, (char *)&apItems[iIdx][0], SIZ_M_UNITNO);
         strncpy(pAdrRec->UnitNox, (char *)&apItems[iIdx][0], SIZ_M_UNITNOX);
         iIdx++;
      }

      if (iIdx >= iCnt)
      {
         // Bad address
         return;
      }

      // Dir
      iDir = 0;
      bDir = false;
      strcpy(acSfx, apItems[iIdx]);
      if (strlen(apItems[iIdx]) < 3)
      {
         if (acSfx[1] == '.')
            acSfx[1] = 0;

         while (iDir <= 11)
         {
            if (!strcmp(acSfx, asDir[iDir]))
            {
               bDir = true;
               break;
            }
            iDir++;
         }
         if (!bDir)
         {
            iDir = 0;
            while (iDir <= 1)
            {
               if (!strcmp(acSfx, asDir2[iDir]))
               {
                  bDir = true;
                  break;
               }
               iDir++;
            }
         }
      } else
      {
         // Check for long direction
         while (iDir < 4)
         {
            if (!strcmp(apItems[iIdx], asDir1[iDir]))
            {
               bDir = true;
               break;
            }
            iDir++;
         }
      }

      // Possible cases E ST or W RD
      // Check for suffix
      bRet = false;
      //if (iTmp <= 7 && iCnt > 2)
      if (bDir && iCnt > 2)
      {
         // If next item is suffix, then this one is not direction
         // if (iTmp=GetSfxCode(apItems[iIdx+1]) && iCnt==iIdx+2)
         // The above statement won't work if Unit# is included.
         if ((iTmp=GetSfxDev(apItems[iIdx+1])) &&
             memcmp(apItems[iIdx+1], "HWY", 3) && 
             memcmp(apItems[iIdx+1], "HIGHWAY", 7)) 
         {
            strncpy(acStrName, apItems[iIdx], SIZ_M_STREET);
            strcpy(pAdrRec->strSfx, GetSfxStr(iTmp));
            bRet = true;         // Done
         } else
         {
            // This is direction
            strcpy(pAdrRec->strDir, asDir[iDir]);
            iIdx++;
         }
      }

      // Not direction, token is str name
      if (!bRet && iIdx < iCnt)
      {
         // Check suffix
         if (iTmp=GetSfxDev(apItems[iCnt-1]))
         {
            strcpy(pAdrRec->strSfx, GetSfxStr(iTmp));
            iCnt--;
         }
      }

      // Check for unit #
      if (apItems[iCnt-1][0] == '#')
      {
         strncpy(pAdrRec->Unit, (char *)&apItems[iCnt-1][0], SIZ_M_UNITNO);
         strncpy(pAdrRec->UnitNox, (char *)&apItems[iCnt-1][0], SIZ_M_UNITNOX);
         iCnt--;

         // If last token is STE or SUITE, remove it.
         if (!strcmp(apItems[iCnt-1], "STE") || !strcmp(apItems[iCnt-1], "SUITE")
            || !strcmp(apItems[iCnt-1], "UNIT") || !strcmp(apItems[iCnt-1], "APT")
            || !strcmp(apItems[iCnt-1], "SP") || !strcmp(apItems[iCnt-1], "RM") )
            iCnt--;
      } else if ((iCnt > 3) &&
         (  !strcmp(apItems[iCnt-2], "STE")  || !strcmp(apItems[iCnt-2], "SUITE") || !strcmp(apItems[iCnt-2], "#STE")
         || !strcmp(apItems[iCnt-2], "UNIT") || !strcmp(apItems[iCnt-2], "UNIT#")
         || !strcmp(apItems[iCnt-2], "APT")  || !strcmp(apItems[iCnt-2], "APTS")  || !strcmp(apItems[iCnt-2], "APT#")
         || !strcmp(apItems[iCnt-2], "RM")   || !strcmp(apItems[iCnt-2], "SP")    || !strcmp(apItems[iCnt-2], "LOT") ) )
      {
         sprintf(acTmp, "#%s", apItems[iCnt-1]);
         strncpy(pAdrRec->Unit, acTmp, SIZ_M_UNITNO);
         strncpy(pAdrRec->UnitNox, acTmp, SIZ_M_UNITNOX);
         iCnt -= 2;
      } else if ((pTmp=strchr(apItems[iCnt-1], '#')))
      {  // 15025 NE SACRAMENTO ST#54, 838 S GRAND AVE APT#611
         if (!memcmp(apItems[iCnt-1], "SP", 2))
         {
            *pTmp = ' ';
         } else
         {
            strncpy(pAdrRec->Unit, pTmp, SIZ_M_UNITNO);
            strncpy(pAdrRec->UnitNox, pTmp, SIZ_M_UNITNOX);
            *pTmp = 0;
            if (!strcmp(apItems[iCnt-1], "APT"))
               iCnt--;
         }
      } else if (!memcmp(apItems[iCnt-1], "FL-", 3) || !memcmp(apItems[iCnt-1], "FLR-", 4))
      {
         strncpy(pAdrRec->Unit, apItems[iCnt-1], SIZ_M_UNITNO);
         strncpy(pAdrRec->UnitNox, apItems[iCnt-1], SIZ_M_UNITNOX);
         iCnt--;
      } else if (!strcmp(apItems[iCnt-2], "LOT") && atoi(apItems[iCnt-1]) > 0)
      {  // 245 WILDWOOD DR LOT 183, 45055 E FLORIDA AVE SP 36
         sprintf(acTmp, "#%s     ", apItems[iCnt-1]);
         strncpy(pAdrRec->Unit, acTmp, SIZ_M_UNITNO);
         strncpy(pAdrRec->UnitNox, acTmp, SIZ_M_UNITNOX);
         iCnt -= 2;
      } else if (iCnt > 3 && atoi(apItems[iCnt-1]) > 0)
      {  // 555 N DANEBO 33
         // Not 21051 STATE ROUTE 772, 5323 HERMITAGE AVE# 1, 3100-R WEST HWY 86
         if (strcmp(apItems[iCnt-2], "ROUTE") && strcmp(apItems[iCnt-2], "HWY")
            && strcmp(apItems[iCnt-2], "BOX") && strcmp(apItems[iCnt-2], "ROAD")
            && strcmp(apItems[iCnt-2], "SP")  && strcmp(apItems[iCnt-2], "HIGHWAY"))   // MER
         {
            // Check for 123 COUNTY RD 105 on SIS->001110050000
            if (!strstr(pAddr, "COUNTY RD"))
            {
               sprintf(acTmp, "#%s     ", apItems[iCnt-1]);
               strncpy(pAdrRec->Unit, acTmp, SIZ_M_UNITNO);
               strncpy(pAdrRec->UnitNox, acTmp, SIZ_M_UNITNOX);
               iCnt--;
            } else
            {
               sprintf(acStrName, "COUNTY ROAD %s", apItems[iCnt-1]);
               bRet = true;
            }

         }

         if (pTmp=strchr(apItems[iCnt-1], '#'))
            *pTmp = 0;
      }

      // Not direction, token is str name
      if (!bRet && iIdx < iCnt)
      {
         // Check suffix
         if (iTmp=GetSfxDev(apItems[iCnt-1]))
         {
            strcpy(pAdrRec->strSfx, GetSfxStr(iTmp));
            while (iIdx < iCnt-1)
            {
               // If multiple address, take the first one.
               // This loop break after first suffix found, ignore the rest.
               // We also must avoid strname like 123 ST PAUL BLVD.  StrSfx must
               // be indexed > 1
               if ((iIdx > 1) &&
                   (iIdx < iCnt-2) &&
                   (iTmp=GetSfxDev(apItems[iIdx])) )
               {
                  strcpy(pAdrRec->strSfx, GetSfxStr(iTmp));
                  break;
               }
               strcat(acStrName, apItems[iIdx++]);
               if (iIdx < iCnt)
                  strcat(acStrName, " ");
            }
         } else if (!strcmp(apItems[iCnt-2], "HWY"))
         {  // 123 HWY 70
            //strcpy(pAdrRec->strName, apItems[iCnt-2]);
            //strcat(pAdrRec->strName, " ");
            //strcat(pAdrRec->strName, apItems[iCnt-1]);
            sprintf(acStrName, "%s %s", apItems[iCnt-2], apItems[iCnt-1]);
         } else
         {
            // Scan for suffix in multi address cases.  Keep the first one
            // 123 MAIN ST 540 N SANTA ANA BLVD
            while (iIdx < iCnt)
            {
               if (iIdx > 1)
               {
                  if (iTmp = GetSfxDev(apItems[iIdx]))
                  {
                     try
                     {
                        pTmp = GetSfxStr(iTmp);
                        if (pTmp)
                           strcpy(pAdrRec->strSfx, pTmp);
                     } catch (...)
                     {
                        LogMsg("***** Error in GetSfxStr() with sfx=%s, %d\n", apItems[iIdx], iTmp);
                     }

                     break;
                  }
               }

               //if (strlen(apItems[iIdx]) > SIZ_M_STREET)
               //   memcpy(acStrName, apItems[iIdx], SIZ_M_STREET);
               //else
               strcat(acStrName, apItems[iIdx]);
               iIdx++;
               if (iIdx < iCnt)
                  strcat(acStrName, " ");
            }
         }
      }
   }
   strncpy(pAdrRec->strName, acStrName, SIZ_M_STREET);
}

/******************************************************************************
 *
 * Copy parseMAdr1() and modify for SUT to parse situs from LDR file. 08/02/2024
 * 1395 SANBORN RD E
 *
 ******************************************************************************/

void parseMAdr1_5(ADR_REC *pAdrRec, char *pAddr)
{
   char  acAdr[256], acStrName[256], acSfx[64], acTmp[256], *apItems[16], *pTmp;
   char *pAdr;
   int   iCnt, iTmp, iIdx, iDir;
   bool  bRet, bDir;

   acStrName[0] = 0;
   pAdr = pAddr;
   if (*pAdr == '%')
   {
      pTmp = strchr(pAdr+3, ' ');
      if (pTmp && isdigit(*++pTmp))
         pAdr = pTmp;
      else
         return;        // bad address
   }

   // Remove leading space - SFX
   while (*pAdr && *pAdr == ' ') pAdr++;

   // Remove '.' and extra space
   iTmp = 0;
   while (*pAdr)
   {
      // Remove dot, comma, and single quote
      if (*pAdr != '.' && *pAdr != ',' && *pAdr != 39)
         acAdr[iTmp++] = *pAdr;

      // Remove space between # and unit number
      if (*pAdr == '#' && *(pAdr+1) == ' ')
         pAdr++;

      // Remove multi-space
      if (*pAdr == ' ')
         while (*pAdr == ' ') pAdr++;
      else
         pAdr++;
   }
   acAdr[iTmp] = 0;

   // Tokenize address
   iCnt = ParseString(acAdr, 32, 16, apItems);

   // If strNum is 0, do not parse address
   iTmp = atoi(apItems[0]);
   iIdx = 0;
   if (iCnt == 1)
   {
      strcpy(acStrName, pAddr);
      pAdrRec->lStrNum = 0;
   } else if (iTmp == 0)
   {
      pAdrRec->lStrNum = 0;
      if ((iTmp=GetSfxCodeX(apItems[iCnt-1], acTmp)))
      {
         strcpy(pAdrRec->strSfx, acTmp);
         sprintf(acTmp, "%d", iTmp);
         strcpy(pAdrRec->SfxCode, acTmp);
         iCnt--;
      }
      strcpy(acStrName, apItems[0]);
      iIdx = 1;
      while (iIdx < iCnt)
      {
         strcat(acStrName, " ");
         strcat(acStrName, apItems[iIdx++]);
      }
   } else
   {
      if (apItems[0][strlen(apItems[0])-1] > '9')
      {
         // StrSub ?
         pTmp = apItems[0];
         while (*pTmp && isdigit(*pTmp) )
            pTmp++;
         if (*pTmp && (*pTmp == '-' || *pTmp == '/'))
            pTmp++;
         if (*pTmp && strlen(pTmp) <= SIZ_M_STR_SUB)
            strcpy(pAdrRec->strSub, pTmp);
      }

      strcpy(pAdrRec->HseNo, apItems[0]);
      pAdrRec->lStrNum = iTmp;
      sprintf(acTmp, "%d", iTmp);
      strcpy(pAdrRec->strNum, acTmp);
      iIdx = 1;

      // Fraction - Make sure this is not the last token
      if (strchr(apItems[iIdx], '/') && (iCnt > (iIdx+1)) && (strlen(apItems[iIdx]) <= SIZ_S_STR_SUB))
      {
         strcpy(pAdrRec->strSub, apItems[iIdx++]);
         if (!strcmp(apItems[iIdx], "RD") || !strcmp(apItems[iIdx], "ROAD") || !strcmp(apItems[iIdx], "AVE"))
         {
            if (iCnt > iIdx+1)
            {
               sprintf(acStrName, "%s %s", apItems[iIdx], apItems[iIdx+1]);
               iIdx += 2;
               if (iIdx >= iCnt)
               {
                  strcpy(pAdrRec->strName, acStrName);
                  return;
               }
            }
         }
      }

      // Unit is sometimes placed after strnum as in: 9416 #A CARLTON OAKS
      if (apItems[iIdx][0] == '#')
      {
         strncpy(pAdrRec->Unit, (char *)&apItems[iIdx][0], SIZ_M_UNITNO);
         strncpy(pAdrRec->UnitNox, (char *)&apItems[iIdx][0], SIZ_M_UNITNOX);
         iIdx++;
      }

      if (iIdx >= iCnt)
      {
         // Bad address
         return;
      }

      // Dir
      iDir = 0;
      bDir = false;
      strcpy(acSfx, apItems[iIdx]);
      if (strlen(apItems[iIdx]) < 3)
      {
         if (acSfx[1] == '.')
            acSfx[1] = 0;

         while (iDir <= 11)
         {
            if (!strcmp(acSfx, asDir[iDir]))
            {
               bDir = true;
               break;
            }
            iDir++;
         }
         if (!bDir)
         {
            iDir = 0;
            while (iDir <= 1)
            {
               if (!strcmp(acSfx, asDir2[iDir]))
               {
                  bDir = true;
                  break;
               }
               iDir++;
            }
         }
      } else
      {
         // Check for long direction
         while (iDir < 4)
         {
            if (!strcmp(apItems[iIdx], asDir1[iDir]))
            {
               bDir = true;
               break;
            }
            iDir++;
         }
      }

      // Possible cases E ST or W RD
      // Check for suffix
      bRet = false;
      //if (iTmp <= 7 && iCnt > 2)
      if (iCnt > 2)
      {
         // If next item is suffix, then this one is not direction
         // if (iTmp=GetSfxCode(apItems[iIdx+1]) && iCnt==iIdx+2)
         // The above statement won't work if Unit# is included.
         //if ((iTmp=GetSfxDev(apItems[iIdx+1])) &&
         if ((iTmp=GetSfxCodeX(apItems[iIdx+1], acTmp)))
         {
            strncpy(acStrName, apItems[iIdx], SIZ_M_STREET);
            strcpy(pAdrRec->strSfx, acTmp);
            sprintf(acTmp, "%d", iTmp);
            strcpy(pAdrRec->SfxCode, acTmp);
            bRet = true;         // Done
         } else
         {
            // This is direction
            strcpy(pAdrRec->strDir, asDir[iDir]);
            iIdx++;
         }
      }

      // Check for unit #
      if (bRet && (iIdx < iCnt-2) && *apItems[iIdx+2] > '0')
      {
         iIdx += 2;
         strncpy(pAdrRec->Unit, (char *)&apItems[iIdx][0], SIZ_M_UNITNO);
         strncpy(pAdrRec->UnitNox, (char *)&apItems[iIdx][0], SIZ_M_UNITNOX);
         iCnt -= 2;
      }

      // Not direction, token is str name
      if (!bRet && iIdx < iCnt)
      {
         // Check suffix
         if (iTmp=GetSfxCodeX(apItems[iCnt-1], acTmp))
         {
            strcpy(pAdrRec->strSfx, acTmp);
            sprintf(acTmp, "%d", iTmp);
            strcpy(pAdrRec->SfxCode, acTmp);

            while (iIdx < iCnt-1)
            {
               // If multiple address, take the first one.
               // This loop break after first suffix found, ignore the rest.
               // We also must avoid strname like 123 ST PAUL BLVD.  StrSfx must
               // be indexed > 1
               if ((iIdx > 1) &&
                   (iIdx < iCnt-2) &&
                   (iTmp=GetSfxCodeX(apItems[iIdx], acTmp)) )
               {
                  strcpy(pAdrRec->strSfx, acTmp);
                  sprintf(acTmp, "%d", iTmp);
                  strcpy(pAdrRec->SfxCode, acTmp);
                  break;
               }
               strcat(acStrName, apItems[iIdx++]);
               if (iIdx < iCnt)
                  strcat(acStrName, " ");
            }
         } else if (!strcmp(apItems[iCnt-2], "HWY"))
         {  // 123 HWY 70
            sprintf(acStrName, "%s %s", apItems[iCnt-2], apItems[iCnt-1]);
         } else
         {
            // Scan for suffix in multi address cases.  Keep the first one
            // 123 MAIN ST 540 N SANTA ANA BLVD
            while (iIdx < iCnt)
            {
               if (iIdx > 1)
               {
                  if (iTmp = GetSfxCodeX(apItems[iIdx], acTmp))
                  {
                     strcpy(pAdrRec->strSfx, acTmp);
                     sprintf(acTmp, "%d", iTmp);
                     strcpy(pAdrRec->SfxCode, acTmp);
                     break;
                  }
               }

               strcat(acStrName, apItems[iIdx]);
               iIdx++;
               if (iIdx < iCnt)
                  strcat(acStrName, " ");
            }
         }
      }
   }
   strncpy(pAdrRec->strName, acStrName, SIZ_M_STREET);
}

/********************************* parseMAdr2() *****************************
 *
 * Parse address line 2 into individual elements.  Designed for ORG, YOL
 *
 ****************************************************************************/

void parseMAdr2(ADR_REC *pAdrRec, char *pAdr)
{
   char  acAdr[256], acTmp[256], *apItems[10], *pTmp;
   int   iCnt, iTmp, iIdx;

   // Remove comma in address line 2
   pTmp = pAdr;
   iIdx = 0;
   while (*pTmp)
   {
      if (*pTmp != ',')
         acAdr[iIdx++] = *pTmp;
      pTmp++;
   }
   acAdr[iIdx++] = 0;

   iCnt = ParseString(acAdr, 32, 10, apItems);
   // Possible bad char
   if (iCnt > 1 && strlen(apItems[iCnt-1]) == 1)
      iCnt--;

   iTmp = atoi(apItems[iCnt-1]);
   if (iTmp > 0)
   {
      iCnt--;
      if (*(apItems[iCnt]+5) == '-' || *(apItems[iCnt]+5) == '`')
         memcpy(pAdrRec->Zip, apItems[iCnt], 5);
      else
         strcpy(pAdrRec->Zip, apItems[iCnt]);
   }

   pAdrRec->City[0] = 0;
   if (iCnt >= 2)
   {
      // If last item is not state abbr, it may be part of city name
      if (strlen(apItems[iCnt-1]) == 2)
      {
         iCnt--;
         strcpy(pAdrRec->State, apItems[iCnt]);
      } else if (!memcmp(apItems[iCnt-1], "CALIF", 5) || !memcmp(apItems[iCnt-1], "CAA", 3))
      {
         iCnt--;
         strcpy(pAdrRec->State, "CA");
      } else if (!memcmp(apItems[iCnt-1], "ORE", 3))
      {
         iCnt--;
         strcpy(pAdrRec->State, "OR");
      } else if (!memcmp(apItems[iCnt-1], "UTAH", 4))
      {
         iCnt--;
         strcpy(pAdrRec->State, "UT");
      } else if (!memcmp(apItems[iCnt-1], "NEVA", 4))
      {
         iCnt--;
         strcpy(pAdrRec->State, "NV");
      } else if (!memcmp(apItems[iCnt-1], "MISS", 4))
      {
         iCnt--;
         strcpy(pAdrRec->State, "MS");
      } else if (!memcmp(apItems[iCnt-1], "ALASKA", 6))
      {
         iCnt--;
         strcpy(pAdrRec->State, "AK");
      } else if (!memcmp(apItems[iCnt-1], "MONTANA", 7))
      {
         iCnt--;
         strcpy(pAdrRec->State, "MT");
      } else if (!memcmp(apItems[iCnt-1], "TEXAS", 5))
      {
         iCnt--;
         strcpy(pAdrRec->State, "TX");
      } else if (!memcmp(apItems[iCnt-1], "MEXICO", 6) && !memcmp(apItems[iCnt-2], "NEW", 3))
      {
         iCnt -= 2;
         strcpy(pAdrRec->State, "NM");
      } else
         strcpy(pAdrRec->State, "  ");

      acTmp[0] = 0;
      for (iTmp = 0; iTmp < iCnt; iTmp++)
      {
         strcat(acTmp, apItems[iTmp]);
         if (iTmp < iCnt-1)
            strcat(acTmp, " ");
         if (strlen(acTmp) > sizeof(pAdrRec->City))
            acTmp[sizeof(pAdrRec->City)-1] = 0;
      }
      strcpy(pAdrRec->City, acTmp);
   } else
   {
      if (iCnt == 1)
         strcpy(pAdrRec->City, apItems[0]);

      strcpy(pAdrRec->State, "  ");
   }
   _strupr(pAdrRec->City);
}

/*****************************************************************************
 *
 * Command:
 *    1 = Open file (pBuf should contain file name)
 *    2 = Close file
 *    4 = Search APN
 *
 * Return 0 if success, error otherwise
 *
 *****************************************************************************/

int locateRecInR01(LPSTR pBuf, LPCSTR pSrchStr, int iSrchLen, int iCmd)
{
   static HANDLE fh=0;
   static long   lRecNum=0;
   static char   acR01[R01_LEN+2], cFileCnt=1;
   static char   acR01File[_MAX_PATH];
   DWORD  nBytesRead=0;
   int    iRet = 0;
   BOOL   bRet;

   if (iCmd == R01_CLOSE)
   {
      if (fh > 0)
         CloseHandle(fh);
      fh = 0;
   }

   if (iCmd & R01_OPEN)
   {
      // Check file
      if (_access(pBuf, 0))
         return -1;

      LogMsg("Open R01 file %s", pBuf);
      fh = CreateFile(pBuf, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
           FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);
      if (fh == INVALID_HANDLE_VALUE)
      {
         fh = 0;
         return -2;
      }

      // Save file name
      strcpy(acR01File, pBuf);

      // Read first record
      bRet = ReadFile(fh, acR01, R01_LEN, &nBytesRead, NULL);
      // Skip header if first record start with 9
      if (acR01[0] == '9')
         bRet = ReadFile(fh, acR01, R01_LEN, &nBytesRead, NULL); 

      lRecNum = 1;
   }

#ifdef _DEBUG
   char  *pR01 = &acR01[0];
#endif

   if (fh && (iCmd & R01_SRCHAPN))
   {
      while ((iRet=memcmp(acR01, pSrchStr, iSrchLen)) < 0)
      {
         lRecNum++;
         bRet = ReadFile(fh, acR01, R01_LEN, &nBytesRead, NULL);
         if (!bRet)
            break;

         if (!nBytesRead)
         {
            // EOF
            cFileCnt++;
            acR01File[strlen(acR01File)-1] = cFileCnt | 0x30;
            if (!_access(acR01File, 0))
            {
               CloseHandle(fh);
               fh = 0;

               // Open next Input file
               LogMsg("Open input file %s", acR01File);
               fh = CreateFile(acR01File, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
                    FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

               if (fh == INVALID_HANDLE_VALUE)
                  break;
               bRet = ReadFile(fh, acR01, R01_LEN, &nBytesRead, NULL);
            } else
               break;
         }
      }

      if (!iRet)
      {
         memcpy(pBuf, acR01, R01_LEN);
      } else
         *pBuf = 0;
   }

   return iRet;
}

/********************************* UpdateBlkLot ******************************
 *
 * Parse legal and populate Tract, Lot, and block
 *
 *****************************************************************************/

void UpdateBlkLot(char *pOutbuf)
{
   char  acLegal[SIZ_LEGAL+1], acTmp[256], *pTmp;
   int   iTmp, iTractLot=0;

   memcpy(acLegal, pOutbuf+OFF_LEGAL, SIZ_LEGAL);
   blankRem(acLegal, SIZ_LEGAL);

   // Get Tract
   if (pTmp=strstr(acLegal, "TR "))
   {
      pTmp += 3;
      iTractLot = atoi(pTmp);
      if (iTractLot > 0)
      {
         iTmp = sprintf(acTmp, "%d", iTractLot);
         memcpy(pOutbuf+OFF_TRACT, acTmp, iTmp);
      }
   }

   // Get Lot
   if ((pTmp=strstr(acLegal, "LOT ")) || (pTmp=strstr(acLegal, "PAR ")) )
   {
      pTmp += 4;
      for (iTmp = 0; iTmp < SIZ_LOT; iTmp++)
      {
         if (!*pTmp || *pTmp == ' ' || *pTmp < '0')
            break;
         acTmp[iTmp] = *pTmp++;
      }
      memcpy(pOutbuf+OFF_LOT, acTmp, iTmp);
   }

   // Get Block - block may contain alpha
   if (pTmp=strstr(acLegal, "BLK "))
   {
      pTmp += 4;
      for (iTmp = 0; iTmp < SIZ_BLOCK; iTmp++)
      {
         if (!*pTmp || *pTmp == ' ' || *pTmp < '0')
            break;
         acTmp[iTmp] = *pTmp++;
      }
      if (memcmp(acTmp, "LOT", 3))
         memcpy(pOutbuf+OFF_BLOCK, acTmp, iTmp);
   }
}

/********************************* parseAdr1U() *****************************
 *
 * Parse address with unit number without mark
 * 123 MAIN DR A
 *
 * Develop for SAC.  Assuming there is no COUNTY RD
 *
 ****************************************************************************/

void parseAdr1U(ADR_REC *pAdrRec, char *pAddr, bool bNoStrNum)
{
   char  acAdr[256], acStr[256], acTmp[256], acSfx[64], *apItems[16], *pTmp;
   char *pAdr;
   int   iCnt, iTmp, iIdx, iDir;
   bool  bRet, bDir;

   // Check for PO Box
   if (!memcmp(pAddr, "PO B", 4) ||
       !memcmp(pAddr, "P.O. B", 6) ||
       !memcmp(pAddr, "P O B", 5) )
   {
      strcpy(pAdrRec->strName, pAddr);
      return;
   }

   pAdr = pAddr;
   if (*pAdr == '%')
   {
      pTmp = strchr(pAdr+3, ' ');
      if (pTmp && isdigit(*++pTmp))
         pAdr = pTmp;
      else
         return;        // bad address
   }

   // Remove leading space - SFX
   while (*pAdr && *pAdr == ' ') pAdr++;

   // Remove '.' and extra space
   iTmp = 0;
   while (*pAdr)
   {
      // Also remove single quote
      if (*pAdr != '.' && *pAdr != ',' && *pAdr != 39)
         acAdr[iTmp++] = *pAdr;

      // Remove multi-space
      if (*pAdr == ' ')
         while (*pAdr == ' ') pAdr++;
      else
         pAdr++;
   }

   acAdr[iTmp] = 0;
   acStr[0] = 0;

   // Tokenize address
   iCnt = ParseString(acAdr, 32, 16, apItems);

   // If strNum is 0, do not parse address
   iTmp = atoi(apItems[0]);
   if ((iTmp == 0 && !bNoStrNum) || iCnt == 1)
   {
      strcpy(acStr, pAddr);
      pAdrRec->lStrNum = 0;
   } else
   {
      if (!bNoStrNum && iTmp > 0)
      {
         pAdrRec->lStrNum = iTmp;
         sprintf(acTmp, "%d", iTmp);
         strcpy(pAdrRec->strNum, acTmp);
         iIdx = 1;
      } else
         iIdx = 0;

      // Fraction - Make sure this is not the last token
      if (strchr(apItems[iIdx], '/') && (iCnt > (iIdx+1)) && (strlen(apItems[iIdx]) <= SIZ_S_STR_SUB))
         strcpy(pAdrRec->strSub, apItems[iIdx++]);

      // Unit is sometimes placed after strnum as in: 9416 #A CARLTON OAKS
      if (apItems[iIdx][0] == '#')
      {
         strncpy(pAdrRec->Unit, apItems[iIdx], SIZ_M_UNITNO);
         iIdx++;
      } else if (iCnt > 2 && (iTmp=GetSfxDev(apItems[iCnt-2])) )
      {
         // Assume there is no COUNTY RD
         if (memcmp(apItems[iCnt-2], "RT", 2) &&
             memcmp(apItems[iCnt-2], "ROUTE", 5) &&
             memcmp(apItems[iCnt-2], "HWY", 3) && 
             memcmp(apItems[iCnt-2], "HIGHWAY", 7) &&
            (isdigit(*apItems[iCnt-1]) || strlen(apItems[iCnt-1]) == 1) // Unit is number or single letter
            )
         {
            strncpy(pAdrRec->Unit, apItems[iCnt-1], SIZ_M_UNITNO);
            iCnt--;
         }
      }


      if (iIdx >= iCnt)
      {
         // Bad address
         return;
      }

      // Dir
      iDir = 0;
      bDir = false;
      strcpy(acSfx, apItems[iIdx]);
      if (strlen(apItems[iIdx]) < 3)
      {
         if (acSfx[1] == '.')
            acSfx[1] = 0;

         while (iDir <= 11)
         {
            if (!strcmp(acSfx, asDir[iDir]))
            {
               bDir = true;
               break;
            }
            iDir++;
         }
      } else
      {
         // Check for long direction
         while (iDir < 4)
         {
            if (!strcmp(apItems[iIdx], asDir1[iDir]))
            {
               bDir = true;
               break;
            }
            iDir++;
         }
      }

      // Possible cases E ST or W RD
      // Check for suffix
      bRet = false;
      if (bDir && iCnt > 2)
      {
         // If next item is suffix, then this one is not direction
         // if (iTmp=GetSfxCode(apItems[iIdx+1]) && iCnt==iIdx+2)
         // The above statement won't work if Unit# is included.
         // Watch for S LAND PARK DR (SAC - 0160042004)
         if (iTmp=GetSfxCodeX(apItems[iIdx+1], acSfx))
         {
            strcpy(acStr, apItems[iIdx]);
            sprintf(pAdrRec->SfxCode, "%d", iTmp);
            strcpy(pAdrRec->strSfx, acSfx);
            strcpy(pAdrRec->SfxName, acSfx);
            bRet = true;         // Done
         } else
         {
            // This is direction
            strcpy(pAdrRec->strDir, asDir[iDir]);
            iIdx++;
         }
      }

      // Check for unit #
      if (apItems[iCnt-1][0] == '#')
      {
         strncpy(pAdrRec->Unit, (char *)&apItems[iCnt-1][0], SIZ_M_UNITNO);
         iCnt--;

         // If last token is STE or SUITE, remove it.
         if (!strcmp(apItems[iCnt-1], "STE") || !strcmp(apItems[iCnt-1], "SUITE")
            || !strcmp(apItems[iCnt-1], "UNIT") || !strcmp(apItems[iCnt-1], "APT")
            || !strcmp(apItems[iCnt-1], "SP") || !strcmp(apItems[iCnt-1], "RM") )
            iCnt--;
      } else if (!strcmp(apItems[iCnt-2], "STE") || !strcmp(apItems[iCnt-2], "SUITE")
         || !strcmp(apItems[iCnt-2], "UNIT") || !strcmp(apItems[iCnt-2], "UNIT#")
         || !strcmp(apItems[iCnt-2], "APT") || !strcmp(apItems[iCnt-2], "APT#")
         || !strcmp(apItems[iCnt-2], "RM") || !strcmp(apItems[iCnt-2], "SP") )
      {
         sprintf(acTmp, "#%s", apItems[iCnt-1]);
         strncpy(pAdrRec->Unit, acTmp, SIZ_M_UNITNO);
         iCnt -= 2;
      } else if ((pTmp=strchr(apItems[iCnt-1], '#')))
      {  // 15025 NE SACRAMENTO ST#54, 838 S GRAND AVE APT#611
         if (!memcmp(apItems[iCnt-1], "SP", 2))
         {
            *pTmp = ' ';
         } else
         {
            strncpy(pAdrRec->Unit, pTmp, SIZ_M_UNITNO);
            *pTmp = 0;
            if (!strcmp(apItems[iCnt-1], "APT"))
               iCnt--;
         }
      } else if (!strcmp(apItems[iCnt-2], "LOT") && atoi(apItems[iCnt-1]) > 0)
      {  // 245 WILDWOOD DR LOT 183, 45055 E FLORIDA AVE SP 36
         sprintf(acTmp, "#%s     ", apItems[iCnt-1]);
         strncpy(pAdrRec->Unit, acTmp, SIZ_M_UNITNO);
         iCnt -= 2;
      } else if (iCnt > 3 && atoi(apItems[iCnt-1]) > 0)
      {  // 555 N DANEBO 33
         // Not 21051 STATE ROUTE 772, 5323 HERMITAGE AVE# 1, 3100-R WEST HWY 86
         if (strcmp(apItems[iCnt-2], "ROUTE") && strcmp(apItems[iCnt-2], "HWY")
            && strcmp(apItems[iCnt-2], "BOX") && strcmp(apItems[iCnt-2], "ROAD")
            && strcmp(apItems[iCnt-2], "SP")  && strcmp(apItems[iCnt-2], "HIGHWAY"))   // MER
         {
            // Check for 123 COUNTY RD 105 on SIS->001110050000
            if (!strstr(pAddr, "COUNTY RD"))
            {
               sprintf(acTmp, "#%s     ", apItems[iCnt-1]);
               strncpy(pAdrRec->Unit, acTmp, SIZ_M_UNITNO);
               iCnt--;
            } else
            {
               sprintf(acStr, "COUNTY ROAD %s", apItems[iCnt-1]);
               bRet = true;
            }

         }

         if (pTmp=strchr(apItems[iCnt-1], '#'))
            *pTmp = 0;
      }

      // Not direction, token is str name
      if (!bRet && iIdx < iCnt)
      {
         // Check suffix
         if (iTmp=GetSfxCodeX(apItems[iCnt-1], acSfx))
         {
            sprintf(pAdrRec->SfxCode, "%d", iTmp);
            strcpy(pAdrRec->strSfx, acSfx);
            strcpy(pAdrRec->SfxName, acSfx);
            while (iIdx < iCnt-1)
            {
               // If multiple address, take the first one.
               // This loop break after first suffix found, ignore the rest.
               // We also must avoid strname like 123 ST PAUL BLVD.  StrSfx must
               // be indexed > 1
               if ((iIdx > 1) &&
                   (iIdx < iCnt-2) &&
                   (iTmp=GetSfxCodeX(apItems[iIdx], acSfx)) )
               {
                  sprintf(pAdrRec->SfxCode, "%d", iTmp);
                  strcpy(pAdrRec->strSfx, acSfx);
                  strcpy(pAdrRec->SfxName, acSfx);
                  break;
               }
               strcat(acStr, apItems[iIdx++]);
               if (iIdx < iCnt)
                  strcat(acStr, " ");
            }
         //} else if (iCnt > 1 && !strcmp(apItems[iCnt-2], "HWY"))
         //{  // 123 HWY 70
         //   strcpy(pAdrRec->strName, apItems[iCnt-2]);
         //   strcat(pAdrRec->strName, " ");
         //   strcat(pAdrRec->strName, apItems[iCnt-1]);
         } else
         {
            // Scan for suffix in multi address cases.  Keep the first one
            // 123 MAIN ST 540 N SANTA ANA BLVD
            while (iIdx < iCnt)
            {
               iTmp=GetSfxCodeX(apItems[iIdx], acSfx);
               if (iIdx > 1 && iTmp > 0 && strcmp(acSfx, "HWY") && strcmp(acSfx, "RT"))
               {
                  sprintf(pAdrRec->SfxCode, "%d", iTmp);
                  strcpy(pAdrRec->strSfx, acSfx);
                  strcpy(pAdrRec->SfxName, acSfx);
                  break;
               }

               strcat(acStr, apItems[iIdx++]);
               if (!strcmp(acSfx, "HWY") || !strcmp(acSfx, "RT"))
               {
                  if (!strcmp(apItems[iIdx], "16"))
                     strcpy(apItems[iIdx], "160");
                  if (!strcmp(acSfx, "RT"))
                     iTmp = 0;
               }

               if (iIdx < iCnt)
                  strcat(acStr, " ");
            }
         }
      }
   }
   strncpy(pAdrRec->strName, acStr, SIZ_M_STREET);
}

/*****************************************************************************
 *
 * This function copies situs addresses from R01 file to ???_SAdr.dat file.  It
 * copies the whole section without formating with intention to replace it
 * back into R01 file later when situs is no longer available.
 *
 * Return number of records extracted.  Otherwise error.
 *
 *****************************************************************************/

long extrSitus(char *pInfile, char *pOutfile, int iRecSize)
{
   int      iRet=0, iTmp;
   long     lCnt=0;
   unsigned long nBytesRead, iLen;
   char     acBuf[2048], acOutbuf[1024], *pInbuf, cFileCnt;

   HANDLE   fhIn;
   FILE     *fdOut;
   XADR_REC *pRec = (XADR_REC *)&acOutbuf;

   iLen = iRecSize;      // Default for R01 format
   // Check file is exist
   if (_access(pInfile, 0))
      return 0;

   LogMsg("Extract sale info from %s to %s", pInfile, pOutfile);
   LogMsg("Open input file %s", pInfile);
   if (INVALID_HANDLE_VALUE == (fhIn = CreateFile(pInfile, GENERIC_READ, FILE_SHARE_READ,
       NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL)) )
      return -3;

   // Skip test record
   ReadFile(fhIn, acBuf, iLen, &nBytesRead, NULL);

   // Open Output file
   if (!(fdOut = fopen(pOutfile, "w")))
      return -4;

   pRec->CRLF[0] = '\n';
   pRec->CRLF[1] = 0;
   pInbuf = acBuf;
   cFileCnt = 1;
   while (ReadFile(fhIn, acBuf, iLen, &nBytesRead, NULL))
   {
      if (!nBytesRead)
      {
         // EOF
         cFileCnt++;
         pInfile[strlen(pInfile)-1] = cFileCnt | 0x30;
         if (!_access(pInfile, 0))
         {
            CloseHandle(fhIn);
            fhIn = 0;

            // Open next Input file
            LogMsg("Open input file %s", pInfile);
            fhIn = CreateFile(pInfile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
                  FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

            if (fhIn == INVALID_HANDLE_VALUE)
            {
               fhIn = 0;
               break;
            }
            if (!ReadFile(fhIn, acBuf, iLen, &nBytesRead, NULL))
               break;
         } else
            break;
      }

#ifdef _DEBUG
      //if (!memcmp(pInbuf, "009600434", 9))
      //   iTmp = 0;
#endif
      memset(acOutbuf, ' ', sizeof(XADR_REC)-2);      // Do not overwrite CRLF
      iTmp = atoin(pInbuf+OFF_S_STRNUM, SIZ_S_STRNUM);
      if (iTmp > 0)
      {
         memcpy(pRec->Apn, pInbuf+OFF_APN_S, SIZ_APN_S);
         memcpy(pRec->strNum, pInbuf+OFF_S_STRNUM, SIZ_S_ADDR);
         memcpy(pRec->Addr1, pInbuf+OFF_S_ADDR_D, SIZ_S_ADDRB_D);
         fputs(acOutbuf, fdOut);
         iRet++;
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   if (fhIn)
      CloseHandle(fhIn);
   if (fdOut)
      fclose(fdOut);

   LogMsg("Total records processed: %u", lCnt);
   LogMsg("      records extracted: %u", iRet);

   return iRet;
}

/******************************* mergeXSitusRec ******************************
 *
 * Merge extracted situs rec
 * return 0: matched but not update
 *        1: not matched
 *        2: matched but only update addr2
 *        3: both addr line updated
 *
 *****************************************************************************/

int mergeXSitusRec(char *pOutbuf)
{
   static   char  acRec[1024], *pRec=NULL;
   int      iLoop, iRet=0;
   XADR_REC *pAdr;

   // Get first Char rec for first call
   if (!pRec)
      pRec = fgets(acRec, 1024, fdExtr);

   pAdr = (XADR_REC *)&acRec[0];
   do
   {
      if (!pRec)
      {
         fclose(fdExtr);
         fdExtr = NULL;
         return 1;      // EOF
      }

      // Compare Apn
      iLoop = memcmp(pOutbuf, pAdr->Apn, iApnLen);
      if (iLoop > 0)
      {
         if (bDebug)
            LogMsg0("Skip adr rec  %.*s", iApnLen, pAdr->Apn);
         pRec = fgets(acRec, 1024, fdExtr);
         lAdrSkip++;
      }
   } while (pRec && iLoop > 0);

   // If not match, return
   if (iLoop)
      return 1;

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "8764014005", 10))
   //   iLoop = 0;
#endif

   // Update situs
   if (*(pOutbuf+OFF_S_STREET) > ' ')
   {
      // Update city
      if (*(pOutbuf+OFF_S_CITY) == ' ')
      {
         memcpy(pOutbuf+OFF_S_CITY, pAdr->City, SIZ_S_CITY);
         memcpy(pOutbuf+OFF_S_ST, "CA", 2);
         memcpy(pOutbuf+OFF_S_CTY_ST_D,  pAdr->Addr2, SIZ_S_CTY_ST_D);
         iRet = 2;
      }
   } else
   {
      memcpy(pOutbuf+OFF_S_STRNUM, pAdr->strNum, SIZ_S_ADDR);
      memcpy(pOutbuf+OFF_S_HSENO, pAdr->strNum, SIZ_S_STRNUM);
      memcpy(pOutbuf+OFF_S_ADDR_D,  pAdr->Addr1, SIZ_S_ADDRB_D);
      iRet = 3;
   }

   // Get next sale record
   pRec = fgets(acRec, 1024, fdExtr);
   if (!pRec)
   {
      fclose(fdExtr);
      fdExtr = NULL;
   }

   lAdrMatch++;

   return iRet;
}

/******************************* mergeXSitusFile *****************************
 *
 *
 *****************************************************************************/

int mergeXSitusFile(char *pInfile, char *pOutfile, char *pExtrfile)
{
   int      iRet=0, iUpdCnt=0, iUpdAdr2=0;
   long     lCnt=0;
   unsigned long nBytesRead, nBytesWritten;
   char     acBuf[2048], *pInbuf, cFileCnt;

   HANDLE   fhIn, fhOut;

   // Check file is exist
   if (_access(pInfile, 0))
      return 0;

   LogMsg("Merge situs adr from %s to %s", pExtrfile, pInfile);

   // Open input file
   LogMsg("Open input file %s", pInfile);
   if (INVALID_HANDLE_VALUE == (fhIn = CreateFile(pInfile, GENERIC_READ, FILE_SHARE_READ,
       NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL)) )
   {
      LogMsg("***** Error opening input file: %s", pInfile);
      return -3;
   }

   // Open output file
   LogMsg("Open output file %s", pOutfile);
   if (INVALID_HANDLE_VALUE == (fhOut = CreateFile(pOutfile, GENERIC_WRITE, 0,
       NULL, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL)) )
   {
      LogMsg("***** Error creating output file: %s", pOutfile);
      return -4;
   }

   // Open sale file
   if (!(fdExtr = fopen(pExtrfile, "r")))
   {
      LogMsg("***** Error opening transfer file: %s", pExtrfile);
      return -5;
   }

   // Read record, no sale update for this one
   ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);
   WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);

   pInbuf = acBuf;
   cFileCnt = 1;
   lAdrSkip=lAdrMatch=0;
   while (ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL))
   {
      if (!nBytesRead)
      {
         CloseHandle(fhIn);
         CloseHandle(fhOut);
         fhIn = fhOut = 0;
         if (!iRet && iUpdCnt > 0)
         {
            // Remove R0? file
            remove(pInfile);
            // Rename N0? to R01 file
            rename(pOutfile, pInfile);
         } else
         {
            LogMsg("***** Something wrong in merging transfer data.  Please check !!!");
            break;
         }

         // EOF
         cFileCnt++;
         pInfile[strlen(pInfile)-1] = cFileCnt | 0x30;
         pOutfile[strlen(pOutfile)-1] = cFileCnt | 0x30;
         if (!_access(pInfile, 0))
         {

            // Open next Input file
            LogMsg("Open input file %s", pInfile);
            fhIn = CreateFile(pInfile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
                  FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

            if (fhIn == INVALID_HANDLE_VALUE)
            {
               fhIn = 0;
               break;
            }

            LogMsg("Open output file %s", pOutfile);
            if (INVALID_HANDLE_VALUE == (fhOut = CreateFile(pOutfile, GENERIC_WRITE, 0,
                NULL, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL)) )
            {
               iRet = -4;
               fhOut = 0;
               break;
            }

            if (!ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL))
               break;
         } else
            break;
      }

      // Update sale
      if (fdExtr)
      {
         iRet = mergeXSitusRec(acBuf);
         switch (iRet)
         {
            case 2:
               iUpdAdr2++;
               break;
            case 3:
               iUpdCnt++;
               break;
         }
         iRet = 0;
      }

      // Write output record
      WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   if (fhIn)
      CloseHandle(fhIn);
   if (fhOut)
      CloseHandle(fhOut);
   if (fdExtr)
      fclose(fdExtr);

   if (!iRet)
      iRet = iUpdCnt;

   LogMsg("Total records processed: %u", lCnt);
   LogMsg("        records matched: %u", lAdrMatch);
   LogMsg("     Full Situs updated: %u", iUpdCnt);
   LogMsg("          Addr2 updated: %u", iUpdAdr2);

   return iRet;
}

/******************************************************************************
 *
 * Copy situs address to mailing.  Convert situs city code to city name.
 *
 ******************************************************************************/

void CopySitusToMailing(char *pOutbuf)
{
   char *pTmp;
   int   iTmp;

   memcpy(pOutbuf+OFF_M_ADDR_D, pOutbuf+OFF_S_ADDR_D, SIZ_M_ADDRB_D);

   // Copy address line 1
   memcpy(pOutbuf+OFF_M_STRNUM, pOutbuf+OFF_S_STRNUM, SIZ_M_ADDR1);

   // Convert city code to city name
   memset(pOutbuf+OFF_M_CITY, ' ', SIZ_M_CITY);
   iTmp = atoin(pOutbuf+OFF_S_CITY, 3);
   pTmp = GetCityName(iTmp);
   if (pTmp)
   {
      iTmp = strlen(pTmp);
      if (iTmp > SIZ_M_CITY) iTmp = SIZ_M_CITY;
      memcpy(pOutbuf+OFF_M_CITY, pTmp, iTmp);
   } else if (*(pOutbuf+OFF_M_CTY_ST_D) > ' ' || iTmp > 0)
      LogMsg0("***** Invalid city code %d on APN=%.12s.  Please investigate", iTmp, pOutbuf);

   // Copy State and zipcode
   memcpy(pOutbuf+OFF_M_ST, pOutbuf+OFF_S_ST, 11);
}

/*********************************** writeR01 *****************************
 *
 *
 ****************************************************************************/

BOOL writeR01(HANDLE fh, LPSTR pBuf, int iLen)
{
   BOOL  bRet;
   long  lRet;
   DWORD nBytesWritten;

   lRet = atoin(pBuf+OFF_TRANSFER_DT, 8);
   if (lRet > lLastRecDate && lRet < lToday)
      lLastRecDate = lRet;

   bRet = WriteFile(fh, pBuf, iLen, &nBytesWritten, NULL);
   return bRet;
}

/********************************* fixSitus ***********************************
 *
 * Fix situs unit in ORG
 *
 ******************************************************************************/

long fixSitus(char *pInfile, char *pOutfile, int iSkip)
{
   long     lCnt=0, iSkipCnt=0, iUpdCnt=0, iTmp;
   unsigned long nBytesRead, nBytesWritten;
   char     acBuf[2048], *pInbuf, cFileCnt;

   HANDLE   fhIn, fhOut;

   // Check file is exist
   if (_access(pInfile, 0))
      return 0;

   // Open input file
   LogMsg("Open input file %s", pInfile);
   if (INVALID_HANDLE_VALUE == (fhIn = CreateFile(pInfile, GENERIC_READ, FILE_SHARE_READ,
       NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL)) )
   {
      LogMsg("***** Error opening input file: %s", pInfile);
      return -3;
   }

   // Open output file
   LogMsg("Open output file %s", pOutfile);
   if (INVALID_HANDLE_VALUE == (fhOut = CreateFile(pOutfile, GENERIC_WRITE, 0,
       NULL, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL)) )
   {
      LogMsg("***** Error creating output file: %s", pOutfile);
      return -4;
   }

   // Skip records
   for (iTmp = 0; iTmp < iSkip; iTmp++)
   {
      ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);
      WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
   }

   pInbuf = acBuf;
   cFileCnt = 1;
   while (ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL))
   {
      if (!nBytesRead)
      {
         CloseHandle(fhIn);
         CloseHandle(fhOut);
         fhIn = fhOut = 0;

         // EOF
         cFileCnt++;
         pInfile[strlen(pInfile)-1] = cFileCnt | 0x30;
         pOutfile[strlen(pOutfile)-1] = cFileCnt | 0x30;
         if (!_access(pInfile, 0))
         {
            // Open next Input file
            LogMsg("Open input file %s", pInfile);
            fhIn = CreateFile(pInfile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
                  FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

            if (fhIn == INVALID_HANDLE_VALUE)
            {
               fhIn = 0;
               break;
            }

            LogMsg("Open output file %s", pOutfile);
            if (INVALID_HANDLE_VALUE == (fhOut = CreateFile(pOutfile, GENERIC_WRITE, 0,
                NULL, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL)) )
            {
               LogMsg("***** Error creating output file %s", pOutfile);
               fhOut = 0;
               break;
            }

            if (!ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL))
               break;
         } else
            break;
      }

      // Fix situs
      if (acBuf[OFF_S_UNITNO] == ')' || acBuf[OFF_S_UNITNO] == '.')
         memset(&acBuf[OFF_S_UNITNO], ' ', SIZ_S_UNITNO);
      if (isdigit(acBuf[OFF_S_CITY-1]))
      {
         char sTmp[32], *pTmp;

         memcpy(sTmp, &acBuf[OFF_S_UNITNO], SIZ_S_UNITNO);
         sTmp[SIZ_S_UNITNO] = 0;
         if (pTmp = strrchr(sTmp, ' '))
         {
            if (memcmp(sTmp, "BLDG", 4) && memcmp(sTmp, "APT", 3) && 
                memcmp(sTmp, "UNIT", 4) && memcmp(sTmp, "STE", 3))
            {
               if (sTmp[SIZ_S_UNITNO-2] == '&' || sTmp[SIZ_S_UNITNO-3] == '&' ||
                   sTmp[SIZ_S_UNITNO-2] == '-')
               {
                  LogMsg("1. Skip %s [%.10s]", sTmp, acBuf);
                  iSkipCnt++;
               } else
               {
                  LogMsg("3. Fix %s [%.10s]", sTmp, acBuf);
                  strcpy(pTmp, "      ");
                  memcpy(&acBuf[OFF_S_UNITNO], sTmp, SIZ_S_UNITNO);
                  iUpdCnt++;
               }
            } else
            {
               LogMsg("1. Skip %s [%.10s]", sTmp, acBuf);
               iSkipCnt++;
            }
         } else
         {
            acBuf[OFF_S_CITY-1] = ' ';
            LogMsg("2. Clean %s --> %.6s [%.10s]", sTmp, &acBuf[OFF_S_UNITNO], acBuf);
            iUpdCnt++;
         }
      }

      // Write output record
      WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   if (fhIn)
      CloseHandle(fhIn);
   if (fhOut)
      CloseHandle(fhOut);

   LogMsg("Total records processed: %u", lCnt);
   LogMsg("      records updated:   %u", iUpdCnt);
   LogMsg("      records skipped:   %u", iSkipCnt);

   return iUpdCnt;
}

/********************************* fixOwners **********************************
 *
 * Fix owner names in ORG
 *
 ******************************************************************************/

extern   char  acRawTmpl[];
long fixOwners(char *pCnty, int iSkip)
{
   long     lCnt=0, iTmp;
   unsigned long nBytesRead, nBytesWritten;
   char     acBuf[2048], cFileCnt;
   char     acOutFile[_MAX_PATH], acRawFile[_MAX_PATH], acTmp[256];

   HANDLE   fhIn, fhOut;

   // Setup file names
   sprintf(acRawFile, acRawTmpl, pCnty, pCnty, "R01");
   sprintf(acOutFile, acRawTmpl, pCnty, pCnty, "TMP");

   if (_access(acRawFile, 0))
      sprintf(acRawFile, acRawTmpl, pCnty, pCnty, "S01");

   LogMsg("Fix Owner names in %s file for %s", acRawFile, pCnty);

   // Open input file
   LogMsg("Open input file %s", acRawFile);
   if (INVALID_HANDLE_VALUE == (fhIn = CreateFile(acRawFile, GENERIC_READ, FILE_SHARE_READ,
       NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL)) )
   {
      LogMsg("***** Error opening input file: %s", acRawFile);
      return -3;
   }

   // Open output file
   LogMsg("Open output file %s", acOutFile);
   if (INVALID_HANDLE_VALUE == (fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0,
       NULL, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL)) )
   {
      LogMsg("***** Error creating output file: %s", acOutFile);
      return -4;
   }

   // Skip records
   for (iTmp = 0; iTmp < iSkip; iTmp++)
   {
      ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);
      WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
   }

   cFileCnt = 1;
   while (ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL))
   {
      if (!nBytesRead)
      {
         CloseHandle(fhIn);
         CloseHandle(fhOut);
         fhIn = fhOut = 0;

         // Rename output file
         sprintf(acTmp, acRawTmpl, pCnty, pCnty, "T01");
         if (!_access(acTmp, 0))
            remove(acTmp);
         LogMsg("Rename %s to %s", acRawFile, acTmp);
         iTmp = rename(acRawFile, acTmp);
         LogMsg("Rename %s to %s", acOutFile, acRawFile);
         iTmp = rename(acOutFile, acRawFile);

         // EOF
         cFileCnt++;
         acRawFile[strlen(acRawFile)-1] = cFileCnt | 0x30;
         acOutFile[strlen(acOutFile)-1] = cFileCnt | 0x30;
         if (!_access(acRawFile, 0))
         {
            // Open next Input file
            LogMsg("Open input file %s", acRawFile);
            fhIn = CreateFile(acRawFile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
                  FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

            if (fhIn == INVALID_HANDLE_VALUE)
            {
               fhIn = 0;
               break;
            }

            LogMsg("Open output file %s", acOutFile);
            if (INVALID_HANDLE_VALUE == (fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0,
                NULL, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL)) )
            {
               LogMsg("***** Error creating output file %s", acOutFile);
               fhOut = 0;
               break;
            }

            if (!ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL))
               break;
         } else
            break;
      }

      // Fix owners
      strUpper(&acBuf[OFF_NAME1], &acBuf[OFF_NAME1], SIZ_NAME1);
      strUpper(&acBuf[OFF_NAME2], &acBuf[OFF_NAME2], SIZ_NAME2);
      strUpper(&acBuf[OFF_NAME_SWAP], &acBuf[OFF_NAME_SWAP], SIZ_NAME_SWAP);

      // Write output record
      WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   if (fhIn)
      CloseHandle(fhIn);
   if (fhOut)
      CloseHandle(fhOut);

   LogMsg("Total records processed: %u", lCnt);

   return lCnt;
}

/*********************************** updateLegal *****************************
 *
 * Return legal length
 *
 *****************************************************************************/

int updateLegal(LPSTR pOutbuf, LPSTR pLegal)
{
   int iTmp, iLen=0;

   // Clear old legal
   removeLegal(pOutbuf);
   remJSChar(pLegal);
   iLen = blankRem(pLegal);
   if (iLen > 2)
   {
      iTmp = iLen;
      if (iTmp > SIZ_LEGAL)
      {
         memcpy(pOutbuf+OFF_LEGAL, pLegal, SIZ_LEGAL);
         if (iTmp > SIZ_LEGAL+SIZ_LEGAL1)
         {
            if (iTmp > SIZ_LEGAL+SIZ_LEGAL1+SIZ_LEGAL2)
               iTmp = SIZ_LEGAL2;
            else
               iTmp = iTmp - (SIZ_LEGAL+SIZ_LEGAL1);
            memcpy(pOutbuf+OFF_LEGAL2, pLegal+SIZ_LEGAL+SIZ_LEGAL1, iTmp);
            iTmp = SIZ_LEGAL1;
         } else
            iTmp -= SIZ_LEGAL;
         memcpy(pOutbuf+OFF_LEGAL1, pLegal+SIZ_LEGAL, iTmp);
      } else
         memcpy(pOutbuf+OFF_LEGAL, pLegal, iTmp);
   } else
      iLen = 0;
   return iLen;
}

/*****************************************************************************
 *
 * Format and update DOCDESC field
 *
 *
 *****************************************************************************/

long UpdDocDesc(char *pCnty, int iSkip)
{
   long     lCnt=0, iTmp;
   unsigned long nBytesRead, nBytesWritten;
   char     acBuf[2048], cFileCnt;
   char     acOutFile[_MAX_PATH], acRawFile[_MAX_PATH], acTmp[256];

   HANDLE   fhIn, fhOut;

   // Setup file names
   sprintf(acRawFile, acRawTmpl, pCnty, pCnty, "R01");
   sprintf(acOutFile, acRawTmpl, pCnty, pCnty, "TMP");

   if (_access(acRawFile, 0))
      sprintf(acRawFile, acRawTmpl, pCnty, pCnty, "S01");

   LogMsg("Update DocDesc in %s file for %s", acRawFile, pCnty);

   // Open input file
   LogMsg("Open input file %s", acRawFile);
   if (INVALID_HANDLE_VALUE == (fhIn = CreateFile(acRawFile, GENERIC_READ, FILE_SHARE_READ,
       NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL)) )
   {
      LogMsg("***** Error opening input file: %s", acRawFile);
      return -3;
   }

   // Open output file
   LogMsg("Open output file %s", acOutFile);
   if (INVALID_HANDLE_VALUE == (fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0,
       NULL, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL)) )
   {
      LogMsg("***** Error creating output file: %s", acOutFile);
      return -4;
   }

   // Skip records
   for (iTmp = 0; iTmp < iSkip; iTmp++)
   {
      ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);
      WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
   }

   cFileCnt = 1;
   while (ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL))
   {
      if (!nBytesRead)
      {
         CloseHandle(fhIn);
         CloseHandle(fhOut);
         fhIn = fhOut = 0;

         // Rename output file
         sprintf(acTmp, acRawTmpl, pCnty, pCnty, "T01");
         if (!_access(acTmp, 0))
            remove(acTmp);
         LogMsg("Rename %s to %s", acRawFile, acTmp);
         iTmp = rename(acRawFile, acTmp);
         LogMsg("Rename %s to %s", acOutFile, acRawFile);
         iTmp = rename(acOutFile, acRawFile);

         // EOF
         cFileCnt++;
         acRawFile[strlen(acRawFile)-1] = cFileCnt | 0x30;
         acOutFile[strlen(acOutFile)-1] = cFileCnt | 0x30;
         if (!_access(acRawFile, 0))
         {
            // Open next Input file
            LogMsg("Open input file %s", acRawFile);
            fhIn = CreateFile(acRawFile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
                  FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

            if (fhIn == INVALID_HANDLE_VALUE)
            {
               fhIn = 0;
               break;
            }

            LogMsg("Open output file %s", acOutFile);
            if (INVALID_HANDLE_VALUE == (fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0,
                NULL, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL)) )
            {
               LogMsg("***** Error creating output file %s", acOutFile);
               fhOut = 0;
               break;
            }

            if (!ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL))
               break;
         } else
            break;
      }

      // Format DocDesc


      // Write output record
      WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   if (fhIn)
      CloseHandle(fhIn);
   if (fhOut)
      CloseHandle(fhOut);

   LogMsg("Total records processed: %u", lCnt);

   return lCnt;
}

/******************************** extrZoning **********************************
 *
 * Extract zoning for future use.
 * Output record is delimited format consists of APN & Zoning
 *
 ******************************************************************************/

long extrZoning(char *pCnty, char *pOutfile)
{
   int      iRet=0;
   long     lCnt=0;
   unsigned long nBytesRead, iLen;
   char     acBuf[2048], acOutbuf[1024], acInfile[_MAX_PATH], *pInbuf, cFileCnt;

   HANDLE   fhIn;
   FILE     *fdOut;

   iLen = iRecLen;      // Default for R01 format

   // Check file is exist
   sprintf(acInfile, acRawTmpl, pCnty, pCnty, "R01");
   if (_access(acInfile, 0))
   {
      sprintf(acInfile, acRawTmpl, pCnty, pCnty, "S01");
      return 0;
   }

   LogMsg("Extract sale info from %s to %s", acInfile, pOutfile);
   LogMsg("Open input file %s", acInfile);
   if (INVALID_HANDLE_VALUE == (fhIn = CreateFile(acInfile, GENERIC_READ, FILE_SHARE_READ,
       NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL)) )
   {
      LogMsg("***** Missing input file %s", acInfile);
      return -3;
   }

   // Skip test record
   ReadFile(fhIn, acBuf, iLen, &nBytesRead, NULL);

   // Open Output file
   if (!(fdOut = fopen(pOutfile, "w")))
      return -4;

   pInbuf = acBuf;
   cFileCnt = 1;
   while (ReadFile(fhIn, acBuf, iLen, &nBytesRead, NULL))
   {
      if (!nBytesRead)
      {
         // EOF
         cFileCnt++;
         acInfile[strlen(acInfile)-1] = cFileCnt | 0x30;
         if (!_access(acInfile, 0))
         {
            CloseHandle(fhIn);
            fhIn = 0;

            // Open next Input file
            LogMsg("Open input file %s", acInfile);
            fhIn = CreateFile(acInfile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
                  FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

            if (fhIn == INVALID_HANDLE_VALUE)
            {
               fhIn = 0;
               break;
            }
            if (!ReadFile(fhIn, acBuf, iLen, &nBytesRead, NULL))
               break;
         } else
            break;
      }

#ifdef _DEBUG
      //if (!memcmp(pInbuf, "009600434", 9))
      //   iTmp = 0;
#endif
      if (acBuf[OFF_ZONE] > ' ')
      {
         sprintf(acOutbuf, "%.*s|%.*s\n", SIZ_APN_S, pInbuf, SIZ_ZONE, pInbuf+OFF_ZONE);
         remChar(acOutbuf, ' ');
         fputs(acOutbuf, fdOut);
         iRet++;
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   if (fhIn)
      CloseHandle(fhIn);
   if (fdOut)
      fclose(fdOut);

   LogMsg("Total records processed: %u", lCnt);
   LogMsg("      records extracted: %u", iRet);

   return iRet;
}

/********************************** updateCareOf *****************************
 *
 * Return CareOf length
 *
 *****************************************************************************/

int updateCareOf(LPSTR pOutbuf, LPSTR pCareOf, int iMaxLen)
{
   int iTmp, iLen=0;
   char  sCareOf[128], *pTmp;

   if (pCareOf && *pCareOf > ' ')
   {
      if (iMaxLen > 0)
      {
         memcpy(sCareOf, pCareOf, iMaxLen);
         sCareOf[iMaxLen] = 0;
      } else
         strcpy(sCareOf, pCareOf);

      remJSChar(sCareOf);    // Remove Json sensitive char

      // Clear old CareOf
      memset(pOutbuf+OFF_CARE_OF, ' ', SIZ_CARE_OF);
      memset(pOutbuf+OFF_XCAREOF, ' ', SIZ_XCAREOF);

      iLen = blankRem(sCareOf);
      if (iLen > 3)
      {
         if (!memcmp(sCareOf, "C/O C/O ", 8))
            pTmp = &sCareOf[4];
         else
            pTmp = &sCareOf[0];

         iTmp = strlen(pTmp);
         if (iTmp > SIZ_CARE_OF)
         {
            memcpy(pOutbuf+OFF_CARE_OF, pTmp, SIZ_CARE_OF);
            iTmp -= SIZ_CARE_OF;
            if (iTmp > SIZ_XCAREOF)
               iTmp = SIZ_XCAREOF;
            memcpy(pOutbuf+OFF_XCAREOF, pTmp + SIZ_CARE_OF, iTmp);
         } else
            memcpy(pOutbuf+OFF_CARE_OF, pTmp, iTmp);
      } else
         iLen = 0;
   }
   
   return iLen;
}

/***************************** removeNames **************************
 *
 ********************************************************************/

void removeNames(char *pOutbuf, bool bRemCareof, bool bRemDBA)
{
   memset(pOutbuf+OFF_NAME1, ' ', SIZ_NAME1+SIZ_NAME2);
   memset(pOutbuf+OFF_VEST, ' ', SIZ_VEST);
   memset(pOutbuf+OFF_NAME_SWAP, ' ', SIZ_NAME_SWAP);
   *(pOutbuf+OFF_ETAL_FLG) = ' ';

   if (bRemCareof)
   {
      memset(pOutbuf+OFF_CARE_OF, ' ', SIZ_CARE_OF);
      memset(pOutbuf+OFF_XCAREOF, ' ', SIZ_XCAREOF);
   }

   if (bRemDBA)
      memset(pOutbuf+OFF_DBA, ' ', SIZ_DBA);
}

/***************************** removeSitus **************************
 *
 ********************************************************************/

void removeSitus(char *pOutbuf)
{
   memset(pOutbuf+OFF_S_STRNUM, ' ', SIZ_S_ADDR);
   memset(pOutbuf+OFF_S_ADDR_D, ' ', SIZ_S_ADDRB_D);
   memset(pOutbuf+OFF_S_HSENO, ' ', SIZ_S_HSENO);
   memset(pOutbuf+OFF_S_UNITNOX, ' ', SIZ_S_UNITNOX);
}

/*************************** removeMailing **************************
 *
 ********************************************************************/

void removeMailing(char *pOutbuf, bool bRemCareof, bool bRemDBA)
{
   memset(pOutbuf+OFF_M_STRNUM, ' ', SIZ_M_ADDR);
   memset(pOutbuf+OFF_M_ADDR_D, ' ', SIZ_M_ADDRB_D);
   memset(pOutbuf+OFF_M_UNITNOX, ' ', SIZ_M_UNITNOX);
   memset(pOutbuf+OFF_M_CITYX, ' ', SIZ_M_CITYX);

   if (bRemCareof)
   {
      memset(pOutbuf+OFF_CARE_OF, ' ', SIZ_CARE_OF);
      memset(pOutbuf+OFF_XCAREOF, ' ', SIZ_XCAREOF);
   }

   if (bRemDBA)
      memset(pOutbuf+OFF_DBA, ' ', SIZ_DBA);
}

/***************************** removeLegal **************************
 *
 ********************************************************************/

void removeLegal(char *pOutbuf)
{
   memset(pOutbuf+OFF_LEGAL, ' ', SIZ_LEGAL);
   memset(pOutbuf+OFF_LEGAL1, ' ', SIZ_LEGAL1);
   memset(pOutbuf+OFF_LEGAL2, ' ', SIZ_LEGAL2);
}

/***************************** removeZoning *************************
 * Only remove PQ Zoning.
 ********************************************************************/

void removeZoning(char *pOutbuf)
{
   memset(pOutbuf+OFF_ZONE_X1, ' ', SIZ_ZONE_X1);
   memset(pOutbuf+OFF_ZONE_X2, ' ', SIZ_ZONE_X2);
   memset(pOutbuf+OFF_ZONE_X3, ' ', SIZ_ZONE_X3);
   memset(pOutbuf+OFF_ZONE_X4, ' ', SIZ_ZONE_X4);
   memset(pOutbuf+OFF_ZONECAT, ' ', SIZ_ZONECAT);
   memset(pOutbuf+OFF_ZONE_DESC, ' ', SIZ_ZONE_DESC);
   memset(pOutbuf+OFF_ZONE_JURIS, ' ', SIZ_ZONE_JURIS);
}

/**************************** removeDocLink *************************
 * Only remove PQ Zoning.
 ********************************************************************/

void removeDocLink(char *pOutbuf)
{
   memset(pOutbuf+OFF_DOCLINKS, ' ', SIZ_DOCLINKS);
   memset(pOutbuf+OFF_DOCLINKX, ' ', SIZ_DOCLINKX);
}

/*************************** removeLdrValue *************************
 * Remove Land, Impr, OtherVal, Fixture, PPVal, Gross, Ratio, ExeAmt.
 ********************************************************************/

void removeLdrValue(char *pOutbuf)
{
   memset(pOutbuf+OFF_LAND, ' ', SIZ_LAND);
   memset(pOutbuf+OFF_IMPR, ' ', SIZ_IMPR);
   memset(pOutbuf+OFF_OTHER, ' ', SIZ_OTHER);
   memset(pOutbuf+OFF_FIXTR, ' ', SIZ_FIXTR);
   memset(pOutbuf+OFF_PERSPROP, ' ', SIZ_PERSPROP);
   memset(pOutbuf+OFF_GROSS, ' ', SIZ_GROSS);
   memset(pOutbuf+OFF_RATIO, ' ', SIZ_RATIO);
   memset(pOutbuf+OFF_EXE_TOTAL, ' ', SIZ_EXE_TOTAL);
}

/**************************** removeUseCode *************************
 * 
 ********************************************************************/

void removeUseCode(char *pOutbuf)
{
   memset(pOutbuf+OFF_USE_CO, ' ', SIZ_USE_CO);
   memset(pOutbuf+OFF_USE_STD, ' ', SIZ_USE_STD);
}

/***************************** getNameDir ***************************
 *
 ********************************************************************/

char *getNameDir(char *pDir)
{
   int   iTmp=0;
   char  *pRet=NULL;

   while (iTmp < 4)
   {
      if (*asDir[iTmp] == *pDir)
      {
         pRet = asDir1[iTmp];
         break;
      }
      iTmp++;
   }

   return pRet;
}

/******************************* updateDocLinks *****************************
 *
 * Format DocLinks using county specific formatting function
 * for Doclink1, Doclink2, Doclink3, DocXferlink.  TRANSFER DOC can be updated
 * by setting uUpdFlg=1 and formatted by pfMakeDocLink().
 * 
 ****************************************************************************/

extern char acDocPath[];
int updateDocLinks(void (*pfMakeDocLink)(char *, char *, char *), char *pCnty, int iSkip, int iUpdFlg, int iIdx)
{
   char     acBuf[2048], acRawFile[_MAX_PATH], acOutFile[_MAX_PATH],
            acDocLink[256], acDocLinks[256], acDoc[32], sRExt[32], sTExt[32];

   HANDLE   fhIn, fhOut;

   DWORD    nBytesRead;
   DWORD    nBytesWritten;
   BOOL     bRet;
   long     lCnt=0, lLinkCnt=0;
   int      iTmp, iRet = 0;

   // 10/11/2023 Turn off all DocLinks
   return 0;

   LogMsg0("Format Doclinks using DocPath: %s", acDocPath);
   if (*acDocPath <= ' ' || _access(acDocPath, 0))
   {
      LogMsg("*** WARNING: Invalid DocPath: %s.  Please verify INI file", acDocPath);
      return 0;
   }

   sprintf(sRExt, "R0%d", iIdx);
   sprintf(sTExt, "T0%d", iIdx);
   sprintf(acRawFile, acRawTmpl, pCnty, pCnty, sRExt);
   sprintf(acOutFile, acRawTmpl, pCnty, pCnty, sTExt);

   // Open Input file
   LogMsg("Open input file %s", acRawFile);
   fhIn = CreateFile(acRawFile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhIn == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening input file: %s\n", acRawFile);
      return -3;
   }
   // Open Output file
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening input file: %s\n", acOutFile);
      return -4;
   }

   // Copy skip record
   memset(acBuf, ' ', 2048);
   while (iSkip-- > 0)
   {
      ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);
      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
   }

   // Merge loop
   while (true)
   {
      bRet = ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);

      // Check for EOF
      if (!bRet)
      {
         LogMsg("***** Error reading input file %s (%f)", acRawFile, GetLastError());
         break;
      }

      if (!nBytesRead)
         break;

#ifdef _DEBUG
//      if (!memcmp(acBuf, "08529006", 8))
//         bRet = true;
#endif

      // Reset old links
      removeDocLink(acBuf);
      acDocLinks[0] = 0;

      // Doc #1
      memcpy(acDoc, (char *)&acBuf[OFF_SALE1_DOC], SIZ_SALE1_DOC);
      myTrim(acDoc, SIZ_SALE1_DOC);
      try
      {
         pfMakeDocLink(acDocLink, acDoc, (char *)&acBuf[OFF_SALE1_DT]);
         if (acDocLink[0] > ' ')
         {
            lLinkCnt++;
            strcat(acDocLinks, acDocLink);
         }
      } catch (...)
      {
         LogMsg("***** Error formatting DocLink for APN = %.12s Doc1Num=%.12s", acBuf, acDoc);
         break;
      }
      strcat(acDocLinks, ",");

      // Doc #2
      memcpy(acDoc, (char *)&acBuf[OFF_SALE2_DOC], SIZ_SALE2_DOC);
      myTrim(acDoc, SIZ_SALE2_DOC);
      try
      {
         pfMakeDocLink(acDocLink, acDoc, (char *)&acBuf[OFF_SALE2_DT]);
         if (acDocLink[0] > ' ')
         {
            lLinkCnt++;
            strcat(acDocLinks, acDocLink);
         }
      } catch (...)
      {
         LogMsg("***** Error formatting DocLink for APN = %.12s Doc2Num=%.12s", acBuf, acDoc);
         break;
      }
      strcat(acDocLinks, ",");

      // Doc #3
      memcpy(acDoc, (char *)&acBuf[OFF_SALE3_DOC], SIZ_SALE3_DOC);
      myTrim(acDoc, SIZ_SALE3_DOC);
      try
      {
         pfMakeDocLink(acDocLink, acDoc, (char *)&acBuf[OFF_SALE3_DT]);
         if (acDocLink[0] > ' ')
         {
            lLinkCnt++;
            strcat(acDocLinks, acDocLink);
         }
      } catch (...)
      {
         LogMsg("***** Error formatting DocLink for APN = %.12s Doc3Num=%.12s", acBuf, acDoc);
         break;
      }
      strcat(acDocLinks, ",");

      // Transfer Doc
      memcpy(acDoc, (char *)&acBuf[OFF_TRANSFER_DOC], SIZ_TRANSFER_DOC);
      myTrim(acDoc, SIZ_TRANSFER_DOC);
      try
      {
         acDocLink[0] = '1';
         pfMakeDocLink(acDocLink, acDoc, (char *)&acBuf[OFF_TRANSFER_DT]);
         if (acDocLink[0] > ' ')
         {
            lLinkCnt++;
            strcat(acDocLinks, acDocLink);
         }
      } catch (...)
      {
         LogMsg("***** Error formatting DocLink for APN = %.12s DocXNum=%.12s", acBuf, acDoc);
         acDoc[0] = 0;
      }

      // Update with formatted transfer DOCNUM
      if (iUpdFlg == 1 && acDoc[0] > ' ')
         memcpy((char *)&acBuf[OFF_TRANSFER_DOC], acDoc, strlen(acDoc));

      // Update DocLinks
      iTmp = strlen(acDocLinks);
      if (iTmp > 10)
      {
         if (iTmp > SIZ_DOCLINKS)
         {
            if (iTmp > SIZ_DOCLINKS+SIZ_DOCLINKX)
            {
               LogMsg("***** Error DOCLINKS is too long (%d): %s\n", iTmp, acDocLinks);
               iTmp = SIZ_DOCLINKS+SIZ_DOCLINKX;
            }
            memcpy((char *)&acBuf[OFF_DOCLINKS], acDocLinks, SIZ_DOCLINKS);
            memcpy((char *)&acBuf[OFF_DOCLINKX], &acDocLinks[SIZ_DOCLINKS], iTmp-SIZ_DOCLINKS);
         } else
            memcpy((char *)&acBuf[OFF_DOCLINKS], acDocLinks, iTmp);
      }

      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);

      if (!bRet)
      {
         LogMsg("***** Error occurs: %d\n", GetLastError());
         break;
      }
   }

   // Close files
   if (fhOut)
      CloseHandle(fhOut);
   if (fhIn)
      CloseHandle(fhIn);

   // Rename out file
   if (lCnt > 10000)
   {
      if (remove(acRawFile))
      {
         LogMsg("***** Error removing file: %s (%d)", acRawFile, errno);
         iRet = errno;
      } else if (rename(acOutFile, acRawFile))
      {
         LogMsg("***** Error renaming temp file: %s --> %s (%d)", acOutFile, acRawFile, errno);
         iRet = errno;
      }
   }

   LogMsg("FmtDocLinks completed.  %d doclinks assigned.\n", lLinkCnt);
   return iRet;
}

/********************************* updateSitusCity ***********************************
 *
 * If situs has the same strname as mailing, copy city and zip over to situs.
 *
 *************************************************************************************/

void updateSitusCity(char *pOutbuf)
{
   char  sTmp[64], sCityName[64], sCityCode[64];
   int   iRet;

   // If situs zip exists, return
   if (*(pOutbuf+OFF_S_ZIP) > ' ' || *(pOutbuf+OFF_S_CITY) > ' ' || *(pOutbuf+OFF_M_CITY) == ' ')
      return;

   // If different strname or strnum, return
   if (memcmp(pOutbuf+OFF_S_STREET, pOutbuf+OFF_M_STREET, SIZ_M_STREET) || memcmp(pOutbuf+OFF_S_STRNUM, pOutbuf+OFF_M_STRNUM, SIZ_M_STRNUM))
      return;

   memcpy(sCityName, pOutbuf+OFF_M_CITY, SIZ_M_CITY);
   blankRem(sCityName, SIZ_M_CITY);
   iRet = City2Code(sCityName, sCityCode, pOutbuf);
   if (iRet > 0)
   {
      Code2City(sCityCode, sCityName); // Do this to get the full official name
      memcpy(pOutbuf+OFF_S_CITY, sCityCode, strlen(sCityCode));
      memcpy(pOutbuf+OFF_S_ST, "CA", 2);
      memcpy(pOutbuf+OFF_S_ZIP, pOutbuf+OFF_M_ZIP, SIZ_M_ZIP);
      sprintf(sTmp, "%s, CA %.5s", sCityName, pOutbuf+OFF_M_ZIP);
      iRet = strlen(sTmp);
      if (iRet > SIZ_S_CTY_ST_D)
         iRet = SIZ_S_CTY_ST_D;
      memcpy(pOutbuf+OFF_S_CTY_ST_D, sTmp, iRet);
   }
}


/********************************* MergeArea **********************************
 *
 * Merge acreage data from GIS basemap to R01 file.  This version won't overwrite
 * existing value.
 *
 * Return 0 if updated
 *
 ******************************************************************************/

int MergeArea(FILE *fd, char *pOutbuf, bool bOverWrite)
{
   static char    acRec[256], *pRec=NULL;
   char           acTmp[256], *pVal;
   int            iLoop, iRet, lSqft;
   double         dAcres;

   // Get first rec for first call
   if (!pRec)
   {
      // Drop header record
      pRec = fgets(acRec, 128, fd);
      if (*pRec > '9')
         pRec = fgets(acRec, 128, fd);
   }

   do
   {
      // Compare Apn
      iLoop = memcmp(pOutbuf, acRec, iApnLen);
      if (iLoop > 0)
      {
         pRec = fgets(acRec, 128, fd);
         if (!pRec)
            return -1;      // EOF
      }
   } while (iLoop > 0);

#ifdef _DEBUG
   //if (!memcmp("00101114", acRec, iApnLen))
   //   pTmp = pRec;
#endif

   // Not match?
   if (iLoop)
      return 1;

   iRet = 1;
   pVal = strchr(acRec, '|');
   if (pVal)
   {
      dAcres = strtod(pVal+1, NULL);
      lSqft  = atoin(pOutbuf+OFF_LOT_SQFT, SIZ_LOT_SQFT);
      // Don't overwrite existing value
      if (dAcres > 0.0001 && (lSqft == 0 || bOverWrite))
      {
         iRet = sprintf(acTmp, "%*d", SIZ_LOT_ACRES, (long)(dAcres*1000.00));
         memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, iRet);
         iRet = sprintf(acTmp, "%*d", SIZ_LOT_SQFT, (long)(dAcres*SQFT_PER_ACRE));
         memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, iRet);
         iRet = 0;
      }
   }
   return iRet;
}

int MergeArea(char *pCnty, char *pAcreage, int iSkip, bool bOverWrite)
{
   char     acBuf[MAX_RECSIZE], acRawFile[_MAX_PATH], acOutFile[_MAX_PATH], acTmp[256];
   char     acAreaFile[_MAX_PATH];
   int      iRet, iRollUpd=0, lCnt=0;
   DWORD    nBytesRead, nBytesWritten;
   BOOL     bRet, bUseR01=true;
   HANDLE   fhIn, fhOut;
   FILE     *fdAcre;

   sprintf(acRawFile, acRawTmpl, pCnty, pCnty, "R01");
   sprintf(acOutFile, acRawTmpl, pCnty, pCnty, "T01");

   // Rename files for processing
   if (_access(acRawFile, 0))
   {
      sprintf(acRawFile, acRawTmpl, pCnty, pCnty, "S01");
      if (!_access(acRawFile, 0))
      {
         LogMsg("Use %s as input for MergeAcres", acRawFile);
         bUseR01 = false;
      } else
      {
         LogMsg("***** Error in MergeAcres(): Missing input file %s.  Please recheck!", acRawFile);
         return -1;
      }
   }

   // Sort area file
   sprintf(acAreaFile, acRawTmpl, pCnty, pCnty, "Area");
   printf("\nSorting area file %s --> %s\n", pAcreage, acAreaFile);
   iRet = sortFile(pAcreage, acAreaFile, "S(#1,C,A) F(TXT)");


   // Open acreage file
   LogMsg("Open acreage file %s", acAreaFile);
   fdAcre = fopen(acAreaFile, "r");
   if (fdAcre == NULL)
   {
      LogMsg("***** Error opening acreage file: %s\n", acAreaFile);
      return -2;
   }

   // Open Input file
   LogMsg("Open input file %s", acRawFile);
   fhIn = CreateFile(acRawFile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhIn == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening input file: %s\n", acRawFile);
      return -3;
   }

   // Open Output file
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening output file: %s\n", acOutFile);
      return -4;
   }

   // Copy skip record
   memset(acBuf, ' ', iRecLen);
   while (iSkip-- > 0)
   {
      ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);
      // Ignore record start with 99999999
      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
   }

   // Merge loop
   do
   {
      bRet = ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);

      // Check for EOF
      if (!bRet)
      {
         LogMsg("***** Error reading input file %s (%f)", acRawFile, GetLastError());
         break;
      }

      // EOF ?
      if (!nBytesRead)
         break;

      // Update acreage info
      if (fdAcre)
      {
         iRet = MergeArea(fdAcre, acBuf, bOverWrite);
         if (iRet == -1)
         {
            fclose(fdAcre);
            fdAcre = (FILE *)NULL;
         } else if (!iRet)
            iRollUpd++;
      }

      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);

      if (!bRet)
      {
         LogMsg("***** Error writing to out file %s: %d\n", acOutFile, GetLastError());
         break;
      }
   } while (bRet);

   // Close files
   if (fdAcre)
      fclose(fdAcre);
   if (fhOut)
      CloseHandle(fhOut);
   if (fhIn)
      CloseHandle(fhIn);

   // Rename R01 to SAV if R01 is used as input
   if (bUseR01)
   {
      sprintf(acTmp, acRawTmpl, pCnty, pCnty, "SAV");
      if (!_access(acTmp, 0))
      {
         LogMsg("Remove saved file %s", acTmp);
         if (iRet=remove(acTmp))
            LogMsg("***** Error removing %s to %s (%d)", acTmp, errno);
      }

      LogMsg("Rename %s to %s", acRawFile, acTmp);
      if (iRet=rename(acRawFile, acTmp))
         LogMsg("***** Error renaming %s to %s (%d)", acRawFile, acTmp, errno);
   } else
      sprintf(acRawFile, acRawTmpl, pCnty, pCnty, "R01");

   LogMsg("Rename %s to %s", acOutFile, acRawFile);
   if (iRet=rename(acOutFile, acRawFile))
      LogMsg("***** Error renaming %s to %s (%d)", acOutFile, acRawFile, errno);

   LogMsg("Total output records:       %u", lCnt);
   LogMsg("Total records updated:      %u", iRollUpd);

   return iRet;
}


/******************************* updatePrevApn ******************************
 *
 * Copy PREV_APN from old P01 to new R01. This function is used at LDR for
 * counties that convert from one APN format to another such as CRES to MB
 * in MOD, DNX, TUO, ...
 * If success, rename output file to S01.
 *
 ****************************************************************************/

int updatePrevApn(char *pCnty, int iPrevApnLen, int iSkip)
{
   FILE *fdS01, *fdR01, *fdP01;
   int   iTmp, iCmp, iCnt=0, iMatch=0;
   char  acS01Rec[2048], acP01Rec[2048], acR01[_MAX_PATH], acS01[_MAX_PATH], acP01[_MAX_PATH];
   char  sTmp[32];

   sprintf(acS01, acRawTmpl, pCnty, pCnty, "S01");
   sprintf(acR01, acRawTmpl, pCnty, pCnty, "R01");
   sprintf(acP01, acRawTmpl, pCnty, pCnty, "P01");

   // If R01 is available, use it
   if (!_access(acR01, 0))
   {
      strcpy(acS01, acR01);
      sprintf(acR01, acRawTmpl, pCnty, pCnty, "C01");
   }

   LogMsg0("Update PrevApn using old P01 file: %s", acP01);

   LogMsg("Open source input file %s", acS01);
   if (!(fdS01 = fopen(acS01, "rb")))
   {
      LogMsg("***** Error opening %s", acS01);
      return -1;
   }

   LogMsg("Open P01 file %s", acP01);
   if (!(fdP01 = fopen(acP01, "rb")))
   {
      LogMsg("***** Error opening %s", acP01);
      return -1;
   }

   LogMsg("Open output file %s", acR01);
   if (!(fdR01 = fopen(acR01, "wb")))
   {
      LogMsg("***** Error creating output file %s", acR01);
      return -1;
   }

   // Copy header
   for (iTmp = 0; iTmp < iSkip; iTmp++)
   {
      fread(acS01Rec, 1, iRecLen, fdP01);
      fread(acS01Rec, 1, iRecLen, fdS01);
      fwrite(acS01Rec, 1, iRecLen, fdR01);
   }

   // Get first P01 record
   fread(acP01Rec, 1, iRecLen, fdP01);
   while (!feof(fdS01))
   {
      iTmp = fread(acS01Rec, 1, iRecLen, fdS01);
      if (iTmp < iRecLen)
         break;

      // Clear buffer
      memset(&acS01Rec[OFF_PREV_APN], ' ', iApnLen);

RecheckApn:
      iCmp = memcmp(&acS01Rec[OFF_APN_S], &acP01Rec[OFF_APN_S], iApnLen);
      if (!iCmp)
      {
         iMatch++;
         if (acP01Rec[OFF_PREV_APN] > ' ')
            memcpy(&acS01Rec[OFF_PREV_APN], &acP01Rec[OFF_PREV_APN], iPrevApnLen);
         else
         {
            if (!memcmp(pCnty, "SJX", 3))
            {
               UPD_PREVAPN_SJX
               LogDbgMsg(bDebug, "New APN: %.*s", iApnLen, &acS01Rec[OFF_APN_S]);
            } else
               memcpy(&acS01Rec[OFF_PREV_APN], &acS01Rec[OFF_APN_S], iPrevApnLen);
         }
         iTmp = fread(acP01Rec, 1, iRecLen, fdP01);
         if (iTmp < iRecLen)
            break;            // EOF
      } else if (iCmp < 0)
      {
         // Populate new parcels with current APN
         if (!memcmp(pCnty, "SJX", 3))
         {
            UPD_PREVAPN_SJX
         } else
            memcpy(&acS01Rec[OFF_PREV_APN], &acS01Rec[OFF_APN_S], iApnLen);
         LogDbgMsg(bDebug, "New APN: %.*s", iApnLen, &acS01Rec[OFF_APN_S]);
      } else
      {
         iTmp = fread(acP01Rec, 1, iRecLen, fdP01);
         if (iTmp < iRecLen)
            break;            // EOF
         goto RecheckApn;
      }

      iTmp = fwrite(acS01Rec, 1, iRecLen, fdR01);

      if (!(++iCnt % 1000))
         printf("\r%u", iCnt);
   }

   while (!feof(fdS01))
   {
      iTmp = fwrite(acS01Rec, 1, iRecLen, fdR01);
      iTmp = fread(acS01Rec, 1, iRecLen, fdS01);
      if (iTmp < iRecLen)
         break;
   }

   fclose(fdS01);
   fclose(fdR01);
   fclose(fdP01);

   LogMsg("Number of records output:   %d", iCnt);
   LogMsg("Number of PREV_APN updated: %d", iMatch);

   // Rename output file
   if (iMatch > 0)
   {
      LogMsg("Rename %s to %s", acR01, acS01);
      remove(acS01);
      iTmp = rename(acR01, acS01);
   } else
      iTmp = 0;
   return iTmp;
}

/******************************************************************************
 *
 * Parse street and suffix.  Convert suffix into PQ4 code
 *
 ******************************************************************************/

void parseStreet(char *pInbuf, char *pStrName, char *pSuffix, char *pSfxCode)
{
   char *pTmp;
   int   iTmp;

   if (pTmp = strchr(pInbuf, '('))
      *--pTmp = 0;

   if (pSuffix) *pSuffix = 0;
   if (pSfxCode) *pSfxCode = 0;

   pTmp = strrchr(pInbuf, ' ');
   if (pTmp)
   {
      iTmp = GetSfxCode(pTmp+1);
      if (iTmp > 0)
      {
         *pTmp++ = 0;
         if (pSfxCode)
            sprintf(pSfxCode, "%d", iTmp);
         if (pSuffix)
            strcpy(pSuffix, pTmp);
      }
   }
   strcpy(pStrName, pInbuf);
}

void parseCitySt(char *pInbuf, char *pCity, char *pState, char *pCityCode)
{
   char *pTmp;

   strcpy(pState, "  ");
   if (pTmp = strrchr(pInbuf, ' '))
   {
      if (*(pTmp+3) == 0)
      {
         strcpy(pState, pTmp+1);
         *pTmp = 0;
      }
   }

   if (pCity)
      strcpy(pCity, pInbuf);

   if (pCityCode)
      City2Code(pInbuf, pCityCode);
}

