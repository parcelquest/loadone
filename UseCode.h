#ifndef T_USECODE_H

#define  STDUSE_SIZ     8
#define  CNTYUSE_SIZ    8
#define  USE_RES        "100"
#define  USE_COMM       "200"
#define  USE_AG         "400"
#define  USE_AGP        "401"
#define  USE_MISC       "600"
#define  USE_REC        "601"
#define  USE_VACANT     "700"
#define  USE_UNASGN     "999"

typedef struct _tUseTbl
{
   int   iCodeLen;      // input length of CntyUse
   char  acStdUse[STDUSE_SIZ];
   char  acCntyUse[CNTYUSE_SIZ];
} USETBL;

typedef struct _tIUseTbl
{
   int   iCntyUse;
   char  acStdUse[STDUSE_SIZ];
} IUSETBL;

LPSTR Cnty2StdUse(int iCntyUse);
LPSTR Cnty2StdUse(LPCSTR pCntyUse);
LPSTR Cnty2StdUse1(LPCSTR pCntyUse);
//LPSTR Cnty2UseCode(LPCSTR pCntyUse);
//LPSTR Std2CntyUse(LPCSTR pStdUse); 

//int   LoadUseTbl(LPSTR pUseTbl);
int   LoadUseTbl(LPSTR pUseTbl, USETBL **pTbl=NULL, int *piUseCnt=(int *)NULL);
int   LoadIUseTbl(LPSTR pUseTbl, IUSETBL **pTbl=NULL, int *piUseCnt=(int *)NULL);
//int   LoadUseCodeTbl(LPSTR pUseTbl);
int   updateStdUse(LPSTR pStdUse, LPCSTR pCntyUse, int iUseLen=8, LPCSTR pApn=NULL);
int   updateStdUse(LPSTR pStdUse, int iCntyUse, LPCSTR pApn=NULL);
int   updateStdUse1(LPSTR pStdUse, LPCSTR pCntyUse, LPCSTR pApn=NULL);

int   updateUseCode(LPSTR pStdUse, LPCSTR pCntyUse, int iUseLen=8);
int   updateR01StdUse(LPSTR pCnty, int iSkip);

bool  openUsecodeLog(char *pCnty, char *pIniFile);
void  closeUsecodeLog();
void  logUsecode(char *pUsecode, char *pApn=NULL);

extern int     iNumUseCodes, iNumUseCodes1;
extern USETBL  *pUseTable;
extern USETBL  *pUseTable1;
extern IUSETBL *pIUseTable;

#endif
