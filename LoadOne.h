
#if !defined(AFX_LOADONE_H__CEF47DA5_7684_407F_83DB_A2CCB44B477F__INCLUDED_)
#define AFX_LOADONE_H__CEF47DA5_7684_407F_83DB_A2CCB44B477F__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "resource.h"

#define  OPEN_ERR       0xF0000001
#define  READ_ERR       0xF0000002
#define  WRITE_ERR      0xF0000004
#define  NOTFOUND_ERR   0xF0000008
#define  BADRECSIZE_ERR 0xF0000010

#define  UPDATE_R01     0
#define  CREATE_R01     1
#define  CREATE_LIEN    2
#define  CLEAR_R01      4

#define  LOAD_LIEN      0x10000000
#define  LOAD_UPDT      0x01000000
#define  LOAD_GRGR      0x00100000
#define  LOAD_SALE      0x00010000
#define  LOAD_ATTR      0x00001000
#define  LOAD_ASSR      0x00000100
#define  LOAD_LAND      0x00000010
#define  LOAD_DAILY     0x00000001

// Special extract option used by lOptExtr
#define  EXTR_ZONE      0x00000001     // Extract zoning
#define  EXTR_MH        0x00000010     // Extract mobile home record
#define  EXTR_OWNER     0x00000100     // Extract owner from OWNER.TRANSFER - specific case for SBD
#define  EXTR_XCHAR     0x00001000		// Extract XC file for bulk customer
#define  EXTR_FVAL      0x00010000     // Extract final values

#define  EXTR_LIEN      0x20000000
#define  EXTR_CSAL      0x02000000
#define  EXTR_IGRGR     0x00200000
#define  EXTR_SALE      0x00020000
#define  EXTR_ATTR      0x00002000
#define  EXTR_ISAL      0x00000200
#define  EXTR_DESC      0x00000020
#define  EXTR_REGN      0x00000002

#define  MERG_LEGAL     0x40000000
#define  MERG_CSAL      0x04000000
#define  MERG_GRGR      0x00400000
#define  MERG_GISA      0x00040000     // Option to merge acreage from GIS basemap
#define  MERG_ATTR      0x00004000
#define  MERG_SADR      0x00000400
#define  MERG_PUBL      0x00000040
#define  MERG_ZONE      0x00000004

#define  EXTR_VALUE     0x80000000
#define  EXTR_IVAL      0x08000000
#define  UPDT_XSAL      0x00800000
#define  UPDT_SALE      0x00080000
#define  UPDT_ASSR      0x00008000
#define  UPDT_SUSE      0x00000800
#define  EXTR_NRSAL     0x00000080     // Extract sale from NDC Recorder data
#define  LOAD_ALL       0x00000008

// lOptProp8
#define  MYOPT_SET      0x10000000
#define  MYOPT_UDB      0x01000000
#define  MYOPT_EXT      0x00100000
#define  MYOPT_OP4      0x00010000     
#define  MYOPT_OP5      0x00001000
#define  MYOPT_OP6      0x00000100
#define  MYOPT_OP7      0x00000010
#define  MYOPT_OP8      0x00000001

// lOptMisc
#define  M_OPT_FIXTRA   0x10000000     // Fix TRA
#define  M_OPT_UPDZIP   0x01000000
#define  M_OPT_FIXOWNER 0x00100000
#define  M_OPT_UCITYZIP 0x00010000     // Update CityZip file
#define  M_OPT_FIXFEFLG 0x00001000     // Fix Full exemption flag
#define  M_OPT_FIXDOCT  0x00000100
#define  M_OPT_FIXDOCN  0x00000010
#define  M_OPT_FIXSAMT  0x00000001

#define  M_OPT_REMXFER  0x20000000		// Remove transfer
#define  M_OPT_ROLLCOR  0x02000000		// Load roll correction
#define  M_OPT_MERGVAL  0x00020000		// Merge value 

typedef struct _tRecCnt
{
   char  Prefix[8];
   long  RecCnt;
} REC_CNT;

int   MergeLotArea(char *pLotFile, char *pCnty);
int   doSaleImport(char *pCnty, char *pDbName, char *pOutfile, int iType);
int   doSaleImportEx(char *pCnty, char *pDbName, int iType);
//int   updateDocLinks(void (*pfMakeDocLink)(LPSTR, LPSTR, LPSTR), int iSkip=1, int iUpdFlg=0);

#endif // !defined(AFX_LOADONE_H__CEF47DA5_7684_407F_83DB_A2CCB44B477F__INCLUDED_)
