/******************************************************************************
 *
 * Options:
 * Load lien:   -CMRN -O -L -Ms -Xl
 * Load update: -CMRN -O -U -Xs
 * Load Tax:    -CMRN -T
 *
 * Input files:
 *    - Old:Mrn_Lien                   (lien file, 422-byte ebcdic)
 *    - Old:Mrn_Roll                   (roll update file, 422-byte ebcdic)
 *    - New:ASSRPROJ_T64731P_SYS004.txt(420-byte ascii)
 *    - Mrn_Char                       (char file, 250-byte ascii TAB-delimited)
 *    - Mrn_Sale                       (sale file, 791-byte ascii TAB-delimited)
 *    - Mrn_sale_newcum.srt            (290-byte, ascii)
 *    - ASSRPROJ_T64731P_SYS005.txt    (Etal file)
 *    - T615191_T61519P_SYS004.txt     (Tax file)
 *    - 2009 Secured Roll.txt          (lien file, 2009 LDR ascii CSV format)
 *
 * Notes:
 *    - Cumulative sale is updated with -Xs or -Us
 *    - Do not add confidential sale record into cumulative sale file.
 *    - Book 900: mobile home
 *           901: floating home
 *    - Currently we do not update sale for book 800 and above
 *    - Sale file contains trust deed info, not use at this time
 *    - Exemption is in roll file ASSRPROJ_T64731P_SYS004.txt, not in LDR.
 *
 * Revision:
 * 06/27/2006 1.2.22.4  First release
 * 03/18/2008 1.5.6     Use standard function to update usecode
 * 03/24/2008 1.5.7.2   Format IMAPLINK.
 * 03/24/2008 1.5.8     Fix bug that null char is in mailing zip.
 * 08/01/2008 8.0.6     Modify Mrn_MergeRoll() to keep TRA the same as we got from
 *                      the county.  Rename Mrn_CreatePQ4() to Mrn_Load_LDR().
 * 09/09/2008 8.2.2     Change to support new layout from the county.
 * 12/18/2008 8.5.3     MRN is sending us new file format.  This version has been
 *                      tested with test files dated 12/17/2008.  Also adding option
 *                      to merge Other Values to R01 file.  OtherVal now includes
 *                      both PP_Val and Bus_Inv values.
 * 02/16/2009 8.5.11    Use PQ_MergeLienExt() instead of PQ_MergeOthers() since MRN
 *                      needs to fix GROSS value also.
 * 03/17/2009 8.7.0     Populate owner name as is.  Remove ET AL and % ownership.
 *                      Do not combine names even with same last name.
 * 07/08/2009 9.1.1     Modify Mrn_MergeOwner(), Mrn_MergeRoll(), Mrn_Load_LDR(), & loadMrn().
 *                      Add new functions Mrn_MergeSitus(), Mrn_MergeMAdr(), Mrn_CreateLienRec2()
 *                      to support new LDR file format.
 * 07/29/2009 9.1.4     Modify Mrn_MergeRoll() to format other values left justify.
 * 02/23/2010 9.4.3     Fix CARE_OF issue.
 * 07/04/2010 10.0.1    Remove update TRANSFER from Mrn_MergeSaleRec().
 * 11/10/2010 10.3.1.1  Remove unneeded code in Mrn_UpdatePQ4() and add more error check in Mrn_MergeRoll().
 * 11/19/2010 10.3.3    Reformat TRA to default length.
 * 03/25/2011 10.5.0    Reverse update TRANSFER in Mrn_MergeSaleRec().
 * 07/12/2011 11.0.2    Fix Unit# and City Mrn_MergeSitus(). Add S_HSENO.
 * 10/07/2011 11.4.13   Add Mrn_ExtrSale() to replace Mrn_UpdateSaleHist(), Mrn_Load_Roll()
 *                      to replace Mrn_MergePQ4(), Mrn_MergeCSale() to replace Mrn_MergeSale(),
 *                      Mrn_ConvSale() to convert old Sale_exp.sls to Mrn_Sale.sls.
 *                      Remove Trust deed info from all PQ products.
 * 07/06/2012 12.1.1    Modify Mrn_Load_LDR() to use cum sale file.
 * 03/05/2013 12.4.8    Remove bad char in CARE_OF
 * 10/08/2013 13.10.4   Use updateVesting() to update Vesting and Etal flag.
 * 10/11/2013 13.11.0   Add all QBaths to R01
 * 11/06/2014 14.9.1    Fix bug in Mrn_MergeMAdr()
 * 03/02/2015 14.12.2   Add log msg. Add 5 mins delay to sale update if first try fails.
 * 11/16/2015 15.3.4    Fix bug in Mrn_MergeMAdr() caused by blankRem() on input record.
 *                      Copy to local variable then call blankRem() fixed the problem.
 * 02/16/2016 15.6.0    Remove duplicate owner name and change param to splitOwner().
 * 05/04/2016 15.9.3    Add option to load tax data.
 * 07/16/2016 16.1.0    Modify Mrn_MergeSitus() to take MZip as param.  Fix bad zipcode in Mrn_MergeMAdr()
 *                      Take out TRANSFER from Mrn_CreateRoll() since county doesn't supply in LDR file for 2016
 * 07/07/2017 17.1.1    LDR file was rolled back to 2015 format. Modify Mrn_CreateRoll() to add DocRef.
 * 07/18/2018 18.2.1    Add comments
 * 12/15/2018 18.7.2    Fix bug in Mrn_Load_LDR() & Mrn_ExtrLien() to check for bad record that doesn't start with number.
 *                      Modify Mrn_CreateLienRec2() to set default HO flag to '2' and set HO='1' if TOTAL_EXE is 7000.
 * 01/09/2019 18.8.0    Fix bug in Mrn_MergeCSaleRec() that overwrites PERS_VAL.
 * 02/28/2019 18.9.7    Modify Mrn_MergeSAdr() to increase Situs Unit# from 2 to 5 bytes.
 * 05/14/2019 18.12.2   Add -Xa and Mrn_ConvStdChar().  Also fix BATHS in Mrn_MergeChar().
 * 07/08/2019 19.0.2    Increase input buffer size in Mrn_ConvStdChar() & Mrn_MergeChar(). 
 *                      Fix APN matching issuse in Mrn_MergeChar().
 * 07/19/2019 19.0.4    Modify Mrn_ConvStdChar() to remove the accumulation of CHAR data.  Keep current only.
 * 07/24/2019 19.0.5    Modify Mrn_ExtrSale() to check for empty input file.
 * 01/29/2020 19.6.5    Modify Mrn_FormatSCSale() to fix sale date error for specific parcel.
 * 04/27/2020 19.9.0    Remove -Ut and use -T to load both full & partial tax file with TC_LoadTax().
 * 07/03/2020 20.1.1    Modify Mrn_Load_LDR() to merge exe code from roll file.
 *                      Modify Mrn_CreateRoll() to support new format of LDR file.
 * 05/05/2121 20.7.17   Modify Mrn_ConvStdChar() to change FirePlace code to 'M' if greater than 9.
 * 05/11/2021 20.7.18   Fix FirePlace in Mrn_MergeChar().  Need Mrn_MergeStdChar().
 * 07/09/2021 21.0.2    Modify Mrn_MergeChar() to populate QualityClass.
 * 03/03/2022 21.6.0    Add -Xf & -Mf options.  Add Mrn_ExtrVal() to extract values from equalized file.
 * 10/31/2022 22.2.11   Remove option to merge final value (it's moved to LoadOne.cpp).
 * 07/14/2023 23.0.3    Add Mrn_MergeLien() & Mrn_MergeMAdr1() and modify Mrn_Load_LDR() to support new LDR file format.
 * 07/08/2024 24.0.2    Modify Mrn_MergeLien() to add ExeType.
 * 10/17/2024 24.1.7    Modify Mrn_CreateValueRec() to set HO_FLG='1' when Exe_Amt=7000.
 *
 ******************************************************************************/

#include "stdafx.h"
#include "CountyInfo.h"
#include "Prodlib.h"
#include "R01.h"
#include "RecDef.h"
#include "Tables.h"
#include "Logs.h"
#include "LoadDef.h"
#include "Utils.h"
#include "XlatTbls.h"
#include "doSort.h"
#include "doOwner.h"
#include "formatApn.h"
#include "SaleRec.h"
#include "CharRec.h"
#include "LoadOne.h"
#include "MergeMrn.h"
#include "UseCode.h"
#include "LOExtrn.h"
#include "PQ.h"
#include "Tax.h"

static FILE     *fdChar;
static long     lCharSkip, lSaleSkip;
static long     lCharMatch, lSaleMatch, lExeMatch;

/********************************* Mrn_FormatDoc *****************************
 *
 *  - DocNum format : input as
 *        YY-99999
 *        YY-999999
 *        YY-99999-A
 *        YY-99999AA
 *        YY-999999A
 *        YY99999
 *        YY999999
 *        YY99999-A
 *        YY99999AA
 *        YY999999-A
 *        YY999999AA
 *        999999999A (067425351A)
 *        9999999-AA (3243266-2A)
 *
 *        99A99999A  (82I45563B, 93C062693A) - ignore
 *        999NOREF-A (391NOREF-A) - ignore
 *        NOREF99999 (NOREF00599) - ignore
 *
 *    THEY ONLY USED (UP TO) 10...CORRECT FORMAT USES 9, 11, OR 12
 *    SHOULD BE YY-NNNNNN
 *           OR YY-NNNNNN-S  (OR YY-NNNNNN-A )
 *           OR YY-NNNNNN-SS (OR YY-NNNNNN-AA)
 *
 *
 *****************************************************************************/

int Mrn_FormatDoc(char *pResult, char *pDocNum)
{
   int   iTmp, iRet;
   char  acTmp[16], acDoc[16];

   strcpy(acTmp, pDocNum);
   myTrim(acTmp);
   iTmp = strlen(acTmp);
   *pResult=acDoc[0] = 0;

   if (strstr(acTmp, "RE"))
      return -1;

   if (isdigit(acTmp[0]))
   {
      if (acTmp[2] == '-')
      {
         if (iTmp == 8)
         {  // YY-99999
            sprintf(acDoc, "%.3s0%s", acTmp, &acTmp[3]);
         } else if (iTmp == 9)
         {  // YY-999999 or YY-99999A
            if (isalpha(acTmp[8]))
               sprintf(acDoc, "%.3s0%.5s%s", acTmp, &acTmp[3], &acTmp[8]);
            else
               strcpy(acDoc, acTmp);
         } else if (acTmp[8] == '-')
         {  // YY-99999-A
            sprintf(acDoc, "%.3s0%s", acTmp, &acTmp[3]);
         } else if (isalpha(acTmp[8]))
         {  // YY-99999AA
            sprintf(acDoc, "%.3s0%.5s%s", acTmp, &acTmp[3], &acTmp[8]);
         } else if (isalpha(acTmp[9]))
         {  // YY-999999A
            strcpy(acDoc, acTmp);
         } else if (iTmp == 10 && isdigit(acTmp[9]))
         {  // YY-9999999
            sprintf(acDoc, "%.3s%s", acTmp, &acTmp[4]);
         } else
         {  // Should never get here
            if (bDebug)
               LogMsg0("-----> %s", acTmp);
            iRet = -1;
         }
      } else if (iTmp == 7)
      {  // YY99999
         sprintf(acDoc, "%.2s-0%s", acTmp, &acTmp[2]);
      } else if (iTmp == 8)
      {  // YY999999
         sprintf(acDoc, "%.2s-%s", acTmp, &acTmp[2]);
      } else
      {  // YY99999-A  or YY99999AA  or 82I123456A
         // 9999999-AA or 9999999-99 or 999999999A
         strcpy(acDoc, acTmp);
      }
   } else
   {  // DN-001984, NOREF*, NORE*, NREF*, NO REF*, A84-17447, P5M1042, PT1001
      // LBF*
      if (acTmp[0] == 'M' || (acTmp[0] != 'N' && isalpha(acTmp[1]) && isalpha(acTmp[2])))
      {
         strcpy(acDoc, acTmp);
         //LogMsg0("******** %s", acTmp);
      } else
      {
         iRet = -1;
         //LogMsg0("-----> %s", acTmp);
      }
   }

   if (acDoc[0])
   {
      strcpy(pResult, acDoc);
      iRet = 0;
   }

   return iRet;
}

/******************************** Mrn_MergeOwner *****************************
 *
 * Notes:
 *    - Name1 may have '&' at the end
 *    - Name1 & Name2 can be combined if they have the same last name.  However,
 *      we prefered to leave it as is in this case.
 *
 * Return 0 if successful, >0 if error
 *
 *****************************************************************************/

void Mrn_MergeOwner(char *pOutbuf, char *pName1, char *pName2)
{
   int   iTmp;
   char  acOwner1[128], acOwner2[128], acSave1[128];
   char  acTmp[128], acTmp1[128], acTmp2[128];
   char  *pTmp;
   bool  bAnd;

   OWNER myOwner1, myOwner2;

   // Clear old names
   removeNames(pOutbuf, false);

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "00110413", 8) )
   //   iTmp = 0;
#endif

   memcpy(acTmp1, pName1, RSIZ_NAME1);
   acTmp1[RSIZ_NAME1] = 0;
   memcpy(acTmp, pName2, RSIZ_NAME2);
   acTmp[RSIZ_NAME2] = 0;

   // Update vesting
   iTmp = updateVesting(myCounty.acCntyCode, acTmp1, pOutbuf+OFF_VEST, pOutbuf+OFF_ETAL_FLG);
   if (!iTmp && !isBlank(acTmp))
      iTmp = updateVesting(myCounty.acCntyCode, acTmp, pOutbuf+OFF_VEST, pOutbuf+OFF_ETAL_FLG);

   if (acTmp[0] == ' ' && acTmp[1] >= 'A')
   {
      // Append name2 to name 1
      strcat(acTmp1, acTmp);
      acOwner2[0] = 0;
   } else if (!isBlank(acTmp))
   {
      memcpy(acTmp2, acTmp, RSIZ_NAME2);
      acTmp2[RSIZ_NAME2] = 0;
      if (pTmp = strstr(acTmp2, "/TR"))
      {
         *pTmp = 0;
         if (*(pTmp+3) == '/')
            pTmp += 4;
         else
            pTmp += 3;
         sprintf(acOwner2, "%sTR %s", acTmp2, pTmp);
      } else
         strcpy(acOwner2, acTmp2);

      blankRem(acOwner2);

      // Remove ETAL
      if (pTmp=strstr(acOwner2, " ETAL"))
         *pTmp = 0;
      // Remove percentage owned
      if (pTmp=strchr(acOwner2, '%'))
      {
         while (*pTmp != ' ')
            pTmp--;
         *pTmp = 0;
      }

      iTmp = strlen(acOwner2);
      pTmp = &acOwner2[iTmp-1];
      if (*pTmp == '&')
         *pTmp = ' ';
      else if (!memcmp(&acOwner2[iTmp-4], "& TR", 4))
      {
         acOwner2[iTmp-4] = ' ';
         blankRem(acOwner2);
      }
   } else
      acOwner2[0] = 0;

   // Check for CO-TRS, TR, /TR, /TR/, EST OF, L/E, LIVING TRUST ...
   if (pTmp = strstr(acTmp1, "/TR"))
   {
      *pTmp = 0;
      if (*(pTmp+3) == '/')
         pTmp += 4;
      else
         pTmp += 3;
      sprintf(acOwner1, "%sTR %s", acTmp1, pTmp);
   } else
      strcpy(acOwner1, acTmp1);

   // Remove ETAL
   if (pTmp=strstr(acOwner1, " ETAL"))
      *pTmp = 0;
   // Remove percentage owned
   if (pTmp=strchr(acOwner1, '%'))
   {
      while (*pTmp != ' ')
         pTmp--;
      *pTmp = 0;
   }

   // Remove '&' if appeared at the end of name
   myTrim(acOwner1);
   iTmp = strlen(acOwner1);
   if (acOwner1[iTmp-1] == '&')
   {
      bAnd = true;
      acOwner1[iTmp-1] = 0;
   } else if (!memcmp(&acOwner1[iTmp-4], "& TR", 4))
   {
      bAnd = true;
      acOwner1[iTmp-4] = ' ';
   } else
      bAnd = false;

   // Save owner1
   remChar(acOwner1, '*');
   blankRem(acOwner1);
   strcpy(acSave1, acOwner1);
   memcpy(pOutbuf+OFF_NAME1, acOwner1, strlen(acOwner1));

   // Check owner2
   if (acOwner2[0] > ' ')
   {
      // Ignore Name2 if it's the same as Name1
      if (!strcmp(acOwner1, acOwner2))
      {
         acOwner2[0] = 0;
         lDupOwners++;
      } else
         vmemcpy(pOutbuf+OFF_NAME2, acOwner2, SIZ_NAME2);
   }

   // Strip off special words to form swapped name
   if ((pTmp=strstr(acOwner1, " EST OF")) || (pTmp=strstr(acOwner1, " ESTATE OF")) ||
       (pTmp=strstr(acOwner1, " L/E"))    || (pTmp=strstr(acOwner1, " LESSEE")) )
      *pTmp = 0;

   if (pTmp=strstr(acOwner1, " TRUST"))
   {
      *pTmp-- = 0;

      // Skip 1994 TRUST
      while (isdigit(*pTmp))
         pTmp--;
      if (*pTmp != ' ') pTmp++;
      *pTmp = 0;

      if ((pTmp=strstr(acOwner1, " FAMILY")) || (pTmp=strstr(acOwner1, " LIVING"))  ||
          (pTmp=strstr(acOwner1, " REVOC")) )
         *pTmp = 0;
   }

   // Now parse owners
   iTmp = splitOwner(acOwner1, &myOwner1);
   if (iTmp < 0)
      strcpy(myOwner1.acSwapName, acSave1);
   else if (bAnd && acOwner2[0] > ' ' && strcmp(acSave1, acOwner2) )
   {
      // Strip off special words to form swapped name
      if ((pTmp=strstr(acOwner2, " EST OF")) || (pTmp=strstr(acOwner2, " ESTATE OF")) ||
          (pTmp=strstr(acOwner2, " L/E"))    || (pTmp=strstr(acOwner2, " LESSEE")) )
         *pTmp = 0;

      if (pTmp=strstr(acOwner2, " TRUST"))
      {
         *pTmp = 0;
         if ((pTmp=strstr(acOwner2, " FAMILY")) || (pTmp=strstr(acOwner2, " LIVING"))  ||
             (pTmp=strstr(acOwner2, " REVOC")) )
            *pTmp = 0;
      }

      iTmp = splitOwner(acOwner2, &myOwner2, 3);
      if (!iTmp)
      {
         if (!strcmp(myOwner1.acOL, myOwner2.acOL))
         {
            sprintf(myOwner1.acSwapName, "%s %s & %s %s %s", myOwner1.acOF,
               myOwner1.acOM, myOwner2.acOF, myOwner2.acOM, myOwner1.acOL);
            blankRem(myOwner1.acSwapName);
         }
      }
   }

   // Save swap name
   if (myOwner1.acSwapName[0] > ' ')
      vmemcpy(pOutbuf+OFF_NAME_SWAP, myOwner1.acSwapName, SIZ_NAME_SWAP);
}

/********************************* Mrn_MergeSAdr *****************************
 *
 * Notes:
 *    - County provides zipcode but no city name.  If strName and zipcode matches with
 *      mailing addr, use city name from mailing.  Direction is also left off, so use
 *      mailing if possible.  Otherwise the geocoding process will fill it up.
 *
 *    - Sample Input: 3100    7   KERNER                    BLVD94901
 *
 *
 *****************************************************************************/

void Mrn_MergeSAdr(char *pOutbuf, char *pRollRec)
{
   MRN_ROLL *pRec;
   char     acTmp[256], acAddr1[64], acAddr2[64], acCode[64];
   int      iTmp, iStrNo, iSfxCode;

   pRec = (MRN_ROLL *)pRollRec;

   // Situs
   acAddr1[0] = 0;
   acAddr2[0] = 0;

   removeSitus(pOutbuf);

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "05244108", 8) )
   //   iTmp = 0;
#endif

   // If no street name, don't bother check anything else
   if (pRec->S_StrName[0] > ' ')
   {
      iStrNo = atoin(pRec->S_StrNum, RSIZ_S_STRNUM);
      if (iStrNo > 0)
      {
         iTmp = sprintf(acAddr1, "%d ", iStrNo);
         memcpy(pOutbuf+OFF_S_STRNUM, acAddr1, iTmp);
         memcpy(pOutbuf+OFF_S_HSENO, acAddr1, iTmp);
      }

      if (pRec->S_StrDir[0] > ' ')
         memcpy(pOutbuf+OFF_S_DIR, pRec->S_StrDir, RSIZ_S_STRDIR);

      // Copy street name - use SIZ_S_STREET since it is smaller then RSIZ_S_STRNAME
      memcpy(pOutbuf+OFF_S_STREET, pRec->S_StrName, SIZ_S_STREET);

      if (pRec->S_StrType[0] > ' ')
      {
         memcpy(acTmp, pRec->S_StrType, RSIZ_S_STRTYPE);
         acTmp[RSIZ_S_STRTYPE] = 0;
         myTrim(acTmp);
         iSfxCode = GetSfxCode(acTmp);
         if (iSfxCode)
         {
            iTmp = sprintf(acCode, "%d", iSfxCode);
            memcpy(pOutbuf+OFF_S_SUFF, acCode, iTmp);
         }
      }

      // Update Unit#
      if (pRec->S_Unit[0] > ' ' && memcmp(pRec->S_Unit, "UNIT", 4))
      {
         *(pOutbuf+OFF_S_UNITNO) = '#';
         memcpy(pOutbuf+OFF_S_UNITNO+1, pRec->S_Unit, RSIZ_S_UNIT);
         sprintf(acAddr1, "%d %.2s %.24s %.4s #%.5s", iStrNo, pRec->S_StrDir, pRec->S_StrName, pRec->S_StrType, pRec->S_Unit);
      } else
         sprintf(acAddr1, "%d %.2s %.24s %.4s", iStrNo, pRec->S_StrDir, pRec->S_StrName, pRec->S_StrType);

      blankRem(acAddr1);
      memcpy(pOutbuf+OFF_S_ADDR_D, acAddr1, strlen(acAddr1));
   } else
      return;

   // ciyt-state
   char  acCity[32];
   long  lZip;

   memcpy(pOutbuf+OFF_S_ST, "CA", 2);
   if (pRec->S_Zip[0] == '9')
      memcpy(pOutbuf+OFF_S_ZIP, pRec->S_Zip, SIZ_S_ZIP);

   // Find city name
   acCity[0] = 0;
   if (!memcmp(pOutbuf+OFF_M_ZIP,    pOutbuf+OFF_S_ZIP,    SIZ_M_ZIP) &&
       !memcmp(pOutbuf+OFF_M_STREET, pOutbuf+OFF_S_STREET, SIZ_M_STREET) )
   {
      memcpy(acCity, pOutbuf+OFF_M_CITY, SIZ_M_CITY);
      myTrim(acCity, SIZ_M_CITY);
   } else
   {
      // Known zip code
      lZip = atoin(pRec->S_Zip, SIZ_S_ZIP);
      //if (lZip == 94920)
      //   strcpy(acCity, "BELVEDERE TIBURON");
      //else if (lZip == 94956)
      //   strcpy(acCity, "POINT REYES");
      //else
      // We do not want to include 94920 and 94956 since these two zipcode are wrong
      // in many places.
      if (lZip == 94937)
         strcpy(acCity, "INVERNESS");
      else if (lZip == 94930)
         strcpy(acCity, "FAIRFAX");
      else if (lZip == 94960)
         strcpy(acCity, "SAN ANSELMO");
      else if (lZip == 94901)
         strcpy(acCity, "SAN RAFAEL");
   }

   if (acCity[0])
   {
      City2Code(acCity, acCode);
      if (acCode[0] > ' ')
      {
         memcpy(pOutbuf+OFF_S_CITY, acCode, SIZ_S_CITY);

         sprintf(acAddr2, "%s CA %.5s", acCity, pOutbuf+OFF_S_ZIP);
         vmemcpy(pOutbuf+OFF_S_CTY_ST_D, acAddr2, SIZ_S_CTY_ST_D);
      }
   }

   // If no mail addr, copy situs over
   if (*(pOutbuf+OFF_M_STREET) == ' ')
   {
      memcpy(pOutbuf+OFF_M_STRNUM, pOutbuf+OFF_S_STRNUM, SIZ_M_STRNUM);
      *(pOutbuf+OFF_M_DIR) = *(pOutbuf+OFF_S_DIR);
      memcpy(pOutbuf+OFF_M_STREET, pOutbuf+OFF_S_STREET, SIZ_M_STREET);
      if (pRec->S_StrType[0] > ' ')
         memcpy(pOutbuf+OFF_M_SUFF, pRec->S_StrType, RSIZ_S_STRTYPE);

      if (pRec->S_Unit[0] > ' ')
         memcpy(pOutbuf+OFF_M_UNITNO, pRec->S_Unit, RSIZ_S_UNIT);

      memcpy(pOutbuf+OFF_M_ADDR_D, pOutbuf+OFF_S_ADDR_D, SIZ_M_ADDR_D);
      memcpy(pOutbuf+OFF_M_ST, "CA", 2);
      if (pRec->S_Zip[0] > '0')
         memcpy(pOutbuf+OFF_M_ZIP, pRec->S_Zip, SIZ_S_ZIP);
   }
}

int Mrn_MergeSitus(char *pOutbuf, char *pLine1, char *pLine2, char *pZipCode=NULL)
{
   char     acTmp[256], acAddr1[128], *pTmp;
   ADR_REC  sSitusAdr;
   int      iTmp;

   // Reset buffer
   memset(&sSitusAdr, 0, sizeof(ADR_REC));

   strcpy(acAddr1, pLine1);
   iTmp = blankRem(acAddr1);
   memcpy(pOutbuf+OFF_S_ADDR_D, acAddr1, iTmp);

   if (pTmp = isCharIncluded(acAddr1, '-', 0))
   {
      pTmp++;
      if (*pTmp == ' ')
         *pTmp = '#';
   }

   parseMAdr1(&sSitusAdr, acAddr1);

   memcpy(pOutbuf+OFF_S_HSENO, sSitusAdr.strNum, strlen(sSitusAdr.strNum));
   memcpy(pOutbuf+OFF_S_STRNUM, sSitusAdr.strNum, strlen(sSitusAdr.strNum));
   if (sSitusAdr.strDir[0] > ' ')
      memcpy(pOutbuf+OFF_S_DIR, sSitusAdr.strDir, strlen(sSitusAdr.strDir));

   memcpy(pOutbuf+OFF_S_STREET, sSitusAdr.strName, strlen(sSitusAdr.strName));
   if (sSitusAdr.strSfx[0] > ' ')
   {
      Sfx2Code(sSitusAdr.strSfx, acTmp);
      memcpy(pOutbuf+OFF_S_SUFF, acTmp, SIZ_S_SUFF);
   }

   if (sSitusAdr.Unit[0] > ' ')
   {
      int iTmp = OFF_S_UNITNO;
      if (sSitusAdr.Unit[0] != '#')
      {
         *(pOutbuf+OFF_S_UNITNO) = '#';
         iTmp++;
      }
      memcpy(pOutbuf+iTmp, sSitusAdr.Unit, strlen(sSitusAdr.Unit));
      memcpy(pOutbuf+OFF_S_UNITNOX, sSitusAdr.UnitNox, strlen(sSitusAdr.UnitNox));
   }

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "03845006", 8))
   //   acTmp[0] = 0;
#endif

   if (*pLine2 < 'A')
      return 1;

   // Situs city
   _strupr(pLine2);
   City2Code(pLine2, acTmp, pOutbuf);
   if (acTmp[0] > ' ')
   {
      memcpy(pOutbuf+OFF_S_CITY, acTmp, SIZ_S_CITY);
      memcpy(pOutbuf+OFF_S_ST, "CA", 2);
   }
   if (pZipCode && *pZipCode == '9')
   {
      memcpy(pOutbuf+OFF_S_ZIP, pZipCode, SIZ_S_ZIP);
      sprintf(acTmp, "%s CA %.5s", pLine2, pZipCode);
   } else
      sprintf(acTmp, "%s CA", pLine2);

   vmemcpy(pOutbuf+OFF_S_CTY_ST_D, acTmp, SIZ_S_CTY_ST_D);

   return 0;
}

/********************************* Mrn_MergeMAdr *****************************
 *
 * Notes:
 *    - Mail addr is provided in various form.  We try the best we can since
 *      there is no way to have 100% covered.
 *
 *    - Sample Input:
 *      1 D/ 77 MACLEAY ST              POTTS PANT                      NSW 2011 AUSTRALIA              AUSTRALIA
 *      152 BEACH RD                    36TH FL_GATEWAY EAST            SINGAPORE 189721
 *      10186 POWERS LK                 TRAIL                           WOODBURY MN 55129
 *      C/O BANK OF AMERICA             ATTN: BURR WOLFF LP             PO BOX 2818                     ALPHARETTA GA 30023
 *      C/O CITIMORTGAGE, INC.          MAILSTOP 02-25                  27555 FARMINGTON RD             FARMINGTON HILLS MI 48334-3357
 *
 *****************************************************************************/

void Mrn_MergeMAdr(char *pOutbuf, char *pRollRec)
{
   MRN_ROLL *pRec;
   ADR_REC  sMailAdr;

   char  *pTmp, *p0, *p1, *p2;
   char  acAddr1[128], acAddr2[128], acTmp[128];
   int   iTmp;

   pRec = (MRN_ROLL *)pRollRec;

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "03845006", 8))
   //   iTmp = 0;
#endif

   // Initialize
   removeMailing(pOutbuf, true);

   // Init output
   memset(&sMailAdr, 0, sizeof(ADR_REC));

   if (pRec->M_Addr1[0] <= ' ')
      return;

   acAddr1[0] = 0;
   if (pRec->M_Addr4[0] > ' ')
   {
      p2 = pRec->M_Addr4;
      if (!_memicmp(pRec->M_Addr1, "C/O", 3)  ||
          !_memicmp(pRec->M_Addr1, "ATTN", 4) ||
          pRec->M_Addr1[0] == '%')
      {
         p0 = pRec->M_Addr1;

         // If last word is not a number, drop it
         strcpy(acTmp, pRec->M_Addr4);
         myTrim(acTmp);
         if (!isdigit(acTmp[strlen(acTmp)-1]))
         {
            // This is more likely foreign addr
            p2 = pRec->M_Addr3;
            p1 = pRec->M_Addr2;
         } else if (isdigit(pRec->M_Addr2[0]))
         {
            // Merge line 2 and 3
            sprintf(acAddr1, "%.*s %.*s", RSIZ_MAILADDR1, pRec->M_Addr2, RSIZ_MAILADDR1, pRec->M_Addr3);
            p1 = acAddr1;
         } else
            p1 = pRec->M_Addr3;
      } else
      {
         // If last word is not a number, drop it
         strcpy(acTmp, pRec->M_Addr4);
         myTrim(acTmp);
         if (acTmp[strlen(acTmp)-1] > '9')
            p2 = pRec->M_Addr3;

         // Merge line 1 and 2 - This is more likely foreign addr
         if (isdigit(pRec->M_Addr1[0]))
         {
            sprintf(acAddr1, "%.*s %.*s", RSIZ_MAILADDR1, pRec->M_Addr1, RSIZ_MAILADDR1, pRec->M_Addr2);
            p1 = acAddr1;
         } else if (isdigit(pRec->M_Addr2[0]))
            p1 = pRec->M_Addr2;
         else
            p1 = pRec->M_Addr1;

         p0 = NULL;
      }
   } else if (pRec->M_Addr3[0] > ' ' && pRec->M_Addr3[1] > ' ')
   {
      p2 = pRec->M_Addr3;
      if (!_memicmp(pRec->M_Addr1, "C/O", 3)  ||
          !_memicmp(pRec->M_Addr1, "ATTN", 4) ||
          pRec->M_Addr1[0] == '%')
      {
         p0 = pRec->M_Addr1;
         p1 = pRec->M_Addr2;
      } else
      {
         // If last word is not a number, drop it
         strncpy(acTmp, pRec->M_Addr3, RSIZ_MAILADDR1);
         iTmp = blankRem(acTmp, RSIZ_MAILADDR1);
         if (iTmp > 1 && !isdigit(acTmp[iTmp-1]))
         {
            // 54 M BOTSARI ST                 THESSALONIKI 54644              GREECE
            p2 = pRec->M_Addr2;
            p1 = pRec->M_Addr1;
         } else if (isdigit(pRec->M_Addr1[0]))
         {
            // Merge line 2 and 3
            sprintf(acAddr1, "%.*s %.*s", RSIZ_MAILADDR1, pRec->M_Addr1, RSIZ_MAILADDR1, pRec->M_Addr2);
            p1 = acAddr1;
         } else
         {
            // Ignore PO BOX since we cannot put both PO BOX and street address on the same line
            // Ignore line #1: PO BOX 463                      36 SPRING ROAD                  LAGUNITAS CA 94938
            // Ignore line #2: PO BOX 4055                     CIVIC CENTER                    SAN RAFAEL CA 94913
            if (isdigit(pRec->M_Addr2[0]))
               p1 = pRec->M_Addr2;
            else
               p1 = pRec->M_Addr1;
         }
         p0 = NULL;
      }
   } else if (memcmp(pRec->M_Addr2, "  ", 2))
   {
      p2 = pRec->M_Addr2;
      p1 = pRec->M_Addr1;
      p0 = NULL;
   } else
   {
      p2 = pRec->M_Addr1;
      p1 = NULL;
      p0 = NULL;
   }

   // Check for C/O
   if (p0)
   {
      memcpy(acTmp, p0, RSIZ_MAILADDR1);
      iTmp = blankRem(acTmp, RSIZ_MAILADDR1);
      updateCareOf(pOutbuf, acTmp, iTmp);
   }

   if (p1)
   {
      if (!acAddr1[0])
      {
         memcpy(acAddr1, p1, RSIZ_MAILADDR1);
         acAddr1[RSIZ_MAILADDR1] = 0;
      }

      // Check for valid string
      iTmp = 0;
      pTmp = (char *)&acAddr1[0];
      while (*pTmp)
      {
         if (*pTmp == '-')
         {
            if (strchr(acAddr1, '#') || strstr(acAddr1, " STE ") ||
                strstr(acAddr1, " APT ") || strstr(acAddr1, " RM ") )
               *pTmp = ' ';
            else
               *pTmp = '#';
         }
         pTmp++;
      }
      iTmp = blankRem(acAddr1);
      vmemcpy(pOutbuf+OFF_M_ADDR_D, acAddr1, SIZ_M_ADDR_D, iTmp);

      try
      {
         parseMAdr1_3(&sMailAdr, acAddr1);
      } catch (...)
      {
         LogMsgD("***** Exception in parseMAdr1_3().  APN=%.12s", pOutbuf);
         return;
      }

      if (sMailAdr.lStrNum > 0)
      {
         sprintf(acTmp, "%d       ", sMailAdr.lStrNum);
         memcpy(pOutbuf+OFF_M_STRNUM, acTmp, SIZ_M_STRNUM);
         if (sMailAdr.strSub[0] > '0')
         {
            sprintf(acTmp, "%s  ", sMailAdr.strSub);
            memcpy(pOutbuf+OFF_M_STR_SUB, acTmp, SIZ_M_STR_SUB);
         }
         memcpy(pOutbuf+OFF_M_DIR, sMailAdr.strDir, strlen(sMailAdr.strDir));
         memcpy(pOutbuf+OFF_M_STREET, sMailAdr.strName, strlen(sMailAdr.strName));
         memcpy(pOutbuf+OFF_M_SUFF, sMailAdr.strSfx, strlen(sMailAdr.strSfx));
         if (sMailAdr.Unit[0] > ' ')
            memcpy(pOutbuf+OFF_M_UNITNO, sMailAdr.Unit, strlen(sMailAdr.Unit));
      } else
      {
         vmemcpy(pOutbuf+OFF_M_STREET, acAddr1, SIZ_M_STREET);
      }
   }

   strncpy(acAddr2, p2, RSIZ_MAILADDR1);
   iTmp = blankRem(acAddr2, RSIZ_MAILADDR1);
   vmemcpy(pOutbuf+OFF_M_CTY_ST_D, acAddr2, SIZ_M_CTY_ST_D, iTmp);

   try
   {
      parseAdr2_1(&sMailAdr, acAddr2);
   } catch (...)
   {
      LogMsgD("***** Exception in parseAdr2_1().  APN=%.12s", pOutbuf);
      return;
   }

   if (sMailAdr.City[0] > ' ')
   {
      vmemcpy(pOutbuf+OFF_M_CITY, sMailAdr.City, SIZ_M_CITY);

      if (sMailAdr.State[0] > ' ')
         memcpy(pOutbuf+OFF_M_ST, sMailAdr.State, SIZ_M_ST);

      // Zipcode
      vmemcpy(pOutbuf+OFF_M_ZIP, sMailAdr.Zip, SIZ_M_ZIP);
   }
}

void Mrn_MergeMAdr(char *pOutbuf)
{
   ADR_REC  sMailAdr;

   char  *p1, *p2, *p3, *pUnit, *pBldg, *pVal;
   char  acAddr1[128], acAddr2[128], acTmp[128];
   int   iTmp;

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "00115038", 8))
   //   iTmp = 0;
#endif

   // Initialize
   removeMailing(pOutbuf, true);

   // Init output
   memset(&sMailAdr, 0, sizeof(ADR_REC));

   if (*apTokens[MRN_L_MAIL1] <= ' ')
      return;

   // Care of
   if (*apTokens[MRN_L_CAREOF] > ' ')
   {
      iTmp = blankRem(apTokens[MRN_L_CAREOF]);
      //memcpy(pOutbuf+OFF_CARE_OF, apTokens[MRN_L_CAREOF], iTmp);
      updateCareOf(pOutbuf, apTokens[MRN_L_CAREOF], iTmp);
   } else if (*apTokens[MRN_L_ATTNNAME] > ' ')
   {
      iTmp = blankRem(apTokens[MRN_L_ATTNNAME]);
      //memcpy(pOutbuf+OFF_CARE_OF, apTokens[MRN_L_ATTNNAME], iTmp);
      updateCareOf(pOutbuf, apTokens[MRN_L_ATTNNAME], iTmp);
   }

   acAddr1[0] = 0;
   acAddr2[0] = 0;
   p1=p2=p3=pUnit=pBldg=pVal=NULL;
   if (memcmp(apTokens[MRN_L_MAIL3], "  ", 2))
   {
      // US addr
      if (*apTokens[MRN_L_MAILSTATE] > ' ')
      {
         if (isdigit(*apTokens[MRN_L_MAIL3]) || !memcmp(apTokens[MRN_L_MAIL3], "PO ", 3) || !memcmp(apTokens[MRN_L_MAIL3], "P O", 3))
            p1 = apTokens[MRN_L_MAIL3];
         else if (!memcmp(apTokens[MRN_L_MAIL2], "PO ", 3) || !memcmp(apTokens[MRN_L_MAIL2], "P O", 3))
         {
            p1 = apTokens[MRN_L_MAIL2];
         } else if (isdigit(*apTokens[MRN_L_MAIL2]) )
         {
            p1 = apTokens[MRN_L_MAIL2];
            p2 = apTokens[MRN_L_MAIL3];
         } else if (!memcmp(apTokens[MRN_L_MAIL1], "PO ", 3) || !memcmp(apTokens[MRN_L_MAIL1], "P O", 3))
         {
            p1 = apTokens[MRN_L_MAIL1];
         } else
         {
            p1 = apTokens[MRN_L_MAIL1];
            p2 = apTokens[MRN_L_MAIL2];
         }
      } else
      {
         // It's propably foreign country
         p1 = apTokens[MRN_L_MAIL1];
         p2 = apTokens[MRN_L_MAIL2];
         p3 = apTokens[MRN_L_MAIL3];
      }
   } else if (memcmp(apTokens[MRN_L_MAIL2], "  ", 2))
   {
      p1 = apTokens[MRN_L_MAIL1];
      p2 = apTokens[MRN_L_MAIL2];
      replChar(p2, ':', ' ');
      if (((isdigit(*p2) || !memcmp(p2, "ONE ", 4)) && !isdigit(*p1)) || !memcmp(p2, "PO ", 3) || !memcmp(p2, "P O", 3))
      {
         p1 = p2;
         p2 = NULL;
      } else if (isdigit(*p2) || *p2 == '#')
      {
         pUnit = p2;
         if (strlen(p2) < SIZ_M_UNITNO)
            pVal = p2;
         p2 = NULL;
      } else if (!memcmp(p2, "STE ", 4) || !memcmp(p2, "SUITE", 5) || !memcmp(p2, "CONDO", 5)
         || !memcmp(p2, "APT ", 4) || !memcmp(p2, "UNIT", 4) || !memcmp(p2, "SPACE ", 6)
         || !memcmp(p2, "SP ", 3) || !memcmp(p2, "PMB ", 4) || !memcmp(p2, "ROOM ", 5))
      {
         pUnit = p2;
         pVal = strchr(p2, ' ');
         if (pVal)
            pVal++;
         else
            pVal = p2;
         p2 = NULL;
      } else if (!memcmp(p2, "BOX ", 4) )
      {
         pUnit = p2;
         p2 = NULL;
      } else if (!memcmp(p2, "BUILDING", 8) || !memcmp(p2, "BLD", 3))
      {
         pBldg = p2;
         p2 = NULL;
      } else if (!memcmp(p1, "PO ", 3) || !memcmp(p1, "P O", 3))
         p2 = NULL;
   } else
   {
      p1 = apTokens[MRN_L_MAIL1];
   }

   // Handling foreign addr
   if (p3)
   {
      vmemcpy(pOutbuf+OFF_M_ADDR_D, p1, SIZ_M_ADDR_D);
      vmemcpy(pOutbuf+OFF_M_STREET, p1, SIZ_M_STREET);

      sprintf(acAddr2, "%s %s", p2, p3);
      iTmp = blankRem(acAddr2);
      vmemcpy(pOutbuf+OFF_M_CTY_ST_D, acAddr2, SIZ_M_CTY_ST_D, iTmp);
      vmemcpy(pOutbuf+OFF_M_CITY, acAddr2, SIZ_M_CITY, iTmp);

      return;
   }

   if (p2)
   {
      // Append 1 & 2
      if (pUnit)
         sprintf(acAddr1, "%s %s %s", p1, p2, pUnit);
      else if (pBldg)
         sprintf(acAddr1, "%s %s %s", p1, p2, pBldg);
      else
         sprintf(acAddr1, "%s %s", p1, p2);
   } else
   {
      if (pUnit)
         sprintf(acAddr1, "%s %s", p1, pUnit);
      else if (pBldg)
         sprintf(acAddr1, "%s %s", p1, pBldg);
      else
         strcpy(acAddr1, p1);
   }

   iTmp = blankRem(acAddr1);
   vmemcpy(pOutbuf+OFF_M_ADDR_D, acAddr1, SIZ_M_ADDR_D, iTmp);

   parseMAdr1_3(&sMailAdr, acAddr1);
   if (sMailAdr.lStrNum > 0)
   {
      sprintf(acTmp, "%d       ", sMailAdr.lStrNum);
      memcpy(pOutbuf+OFF_M_STRNUM, acTmp, SIZ_M_STRNUM);
      if (sMailAdr.strSub[0] > '0')
      {
         sprintf(acTmp, "%s  ", sMailAdr.strSub);
         memcpy(pOutbuf+OFF_M_STR_SUB, acTmp, SIZ_M_STR_SUB);
      }
      memcpy(pOutbuf+OFF_M_DIR, sMailAdr.strDir, strlen(sMailAdr.strDir));
      memcpy(pOutbuf+OFF_M_STREET, sMailAdr.strName, strlen(sMailAdr.strName));
      memcpy(pOutbuf+OFF_M_SUFF, sMailAdr.strSfx, strlen(sMailAdr.strSfx));
      if (sMailAdr.Unit[0] > ' ')
         memcpy(pOutbuf+OFF_M_UNITNO, sMailAdr.Unit, strlen(sMailAdr.Unit));
      else if (pVal)
      {  // catching "# A8", "SUITE123"
         remChar(pVal, 32);
         iTmp = strlen(pVal);
         if (iTmp > SIZ_M_UNITNO)
         {
            if (!memcmp(pVal, "SUITE", 5))
            {
               pVal += 5;
               iTmp -= 5;
            } else
               iTmp = SIZ_M_UNITNO;
         }
         memcpy(pOutbuf+OFF_M_UNITNO, pVal, iTmp);
      }
   } else
      vmemcpy(pOutbuf+OFF_M_STREET, acAddr1, SIZ_M_STREET);

   if (*apTokens[MRN_L_MAILCITY] > ' ')
   {
      p2 = myTrim(apTokens[MRN_L_MAILCITY]);
      vmemcpy(pOutbuf+OFF_M_CITY, p2, SIZ_M_CITY);

      if (*apTokens[MRN_L_MAILSTATE] > ' ')
         memcpy(pOutbuf+OFF_M_ST, apTokens[MRN_L_MAILSTATE], SIZ_M_ST);

      // Zipcode
      if (!memcmp(apTokens[MRN_L_MAILZIP], "C4924", 5))
         *apTokens[MRN_L_MAILZIP] = '9';
      else if (*apTokens[MRN_L_MAILZIP] != '9')
         *apTokens[MRN_L_MAILZIP] = 0;

      vmemcpy(pOutbuf+OFF_M_ZIP, apTokens[MRN_L_MAILZIP], SIZ_M_ZIP);

      sprintf(acAddr2, "%s %s %.5s", apTokens[MRN_L_MAILCITY], apTokens[MRN_L_MAILSTATE], apTokens[MRN_L_MAILZIP]);
      iTmp = blankRem(acAddr2);
      vmemcpy(pOutbuf+OFF_M_CTY_ST_D, acAddr2, SIZ_M_CTY_ST_D, iTmp);
   }
}

// For 2023
void Mrn_MergeMAdr1(char *pOutbuf)
{
   ADR_REC  sMailAdr;

   char  *p1, *p2, *p3, *pUnit, *pBldg, *pVal;
   char  acAddr1[128], acAddr2[128], acTmp[128];
   int   iTmp;

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "00115038", 8))
   //   iTmp = 0;
#endif

   // Initialize
   removeMailing(pOutbuf, true);

   // Init output
   memset(&sMailAdr, 0, sizeof(ADR_REC));

   if (*apTokens[MRN_L_MAIL1] <= ' ')
      return;

   // Care of
   if (*apTokens[MRN_L_CAREOF] > ' ')
   {
      iTmp = blankRem(apTokens[MRN_L_CAREOF]);
      //memcpy(pOutbuf+OFF_CARE_OF, apTokens[MRN_L_CAREOF], iTmp);
      updateCareOf(pOutbuf, apTokens[MRN_L_CAREOF], iTmp);
   } else if (*apTokens[MRN_L_ATTNNAME] > ' ')
   {
      iTmp = blankRem(apTokens[MRN_L_ATTNNAME]);
      //memcpy(pOutbuf+OFF_CARE_OF, apTokens[MRN_L_ATTNNAME], iTmp);
      updateCareOf(pOutbuf, apTokens[MRN_L_ATTNNAME], iTmp);
   }

   acAddr1[0] = 0;
   acAddr2[0] = 0;
   p1=p2=p3=pUnit=pBldg=pVal=NULL;
   if (memcmp(apTokens[MRN_L_MAIL3], "  ", 2))
   {
      // US addr
      if (*apTokens[MRN_L_MAILSTATE] > ' ')
      {
         if (isdigit(*apTokens[MRN_L_MAIL3]) || !memcmp(apTokens[MRN_L_MAIL3], "PO ", 3) || !memcmp(apTokens[MRN_L_MAIL3], "P O", 3))
            p1 = apTokens[MRN_L_MAIL3];
         else if (!memcmp(apTokens[MRN_L_MAIL2], "PO ", 3) || !memcmp(apTokens[MRN_L_MAIL2], "P O", 3))
         {
            p1 = apTokens[MRN_L_MAIL2];
         } else if (isdigit(*apTokens[MRN_L_MAIL2]) )
         {
            p1 = apTokens[MRN_L_MAIL2];
            p2 = apTokens[MRN_L_MAIL3];
         } else if (!memcmp(apTokens[MRN_L_MAIL1], "PO ", 3) || !memcmp(apTokens[MRN_L_MAIL1], "P O", 3))
         {
            p1 = apTokens[MRN_L_MAIL1];
         } else
         {
            p1 = apTokens[MRN_L_MAIL1];
            p2 = apTokens[MRN_L_MAIL2];
         }
      } else
      {
         // It's propably foreign country
         p1 = apTokens[MRN_L_MAIL1];
         p2 = apTokens[MRN_L_MAIL2];
         p3 = apTokens[MRN_L_MAIL3];
      }
   } else if (memcmp(apTokens[MRN_L_MAIL2], "  ", 2))
   {
      p1 = apTokens[MRN_L_MAIL1];
      p2 = apTokens[MRN_L_MAIL2];
      replChar(p2, ':', ' ');
      if (((isdigit(*p2) || !memcmp(p2, "ONE ", 4)) && !isdigit(*p1)) || !memcmp(p2, "PO ", 3) || !memcmp(p2, "P O", 3))
      {
         p1 = p2;
         p2 = NULL;
      } else if (isdigit(*p2) || *p2 == '#')
      {
         pUnit = p2;
         if (strlen(p2) < SIZ_M_UNITNO)
            pVal = p2;
         p2 = NULL;
      } else if (!memcmp(p2, "STE ", 4) || !memcmp(p2, "SUITE", 5) || !memcmp(p2, "CONDO", 5)
         || !memcmp(p2, "APT ", 4) || !memcmp(p2, "UNIT", 4) || !memcmp(p2, "SPACE ", 6)
         || !memcmp(p2, "SP ", 3) || !memcmp(p2, "PMB ", 4) || !memcmp(p2, "ROOM ", 5))
      {
         pUnit = p2;
         pVal = strchr(p2, ' ');
         if (pVal)
            pVal++;
         else
            pVal = p2;
         p2 = NULL;
      } else if (!memcmp(p2, "BOX ", 4) )
      {
         pUnit = p2;
         p2 = NULL;
      } else if (!memcmp(p2, "BUILDING", 8) || !memcmp(p2, "BLD", 3))
      {
         pBldg = p2;
         p2 = NULL;
      } else if (!memcmp(p1, "PO ", 3) || !memcmp(p1, "P O", 3))
         p2 = NULL;
   } else
   {
      p1 = apTokens[MRN_L_MAIL1];
   }

   // Handling foreign addr
   if (p3)
   {
      vmemcpy(pOutbuf+OFF_M_ADDR_D, p1, SIZ_M_ADDR_D);
      vmemcpy(pOutbuf+OFF_M_STREET, p1, SIZ_M_STREET);

      sprintf(acAddr2, "%s %s", p2, p3);
      iTmp = blankRem(acAddr2);
      vmemcpy(pOutbuf+OFF_M_CTY_ST_D, acAddr2, SIZ_M_CTY_ST_D, iTmp);
      vmemcpy(pOutbuf+OFF_M_CITY, acAddr2, SIZ_M_CITY, iTmp);

      return;
   }

   if (p2)
   {
      // Append 1 & 2
      if (pUnit)
         sprintf(acAddr1, "%s %s %s", p1, p2, pUnit);
      else if (pBldg)
         sprintf(acAddr1, "%s %s %s", p1, p2, pBldg);
      else
         sprintf(acAddr1, "%s %s", p1, p2);
   } else
   {
      if (pUnit)
         sprintf(acAddr1, "%s %s", p1, pUnit);
      else if (pBldg)
         sprintf(acAddr1, "%s %s", p1, pBldg);
      else
         strcpy(acAddr1, p1);
   }

   iTmp = blankRem(acAddr1);
   vmemcpy(pOutbuf+OFF_M_ADDR_D, acAddr1, SIZ_M_ADDR_D, iTmp);

   parseMAdr1_3(&sMailAdr, acAddr1);
   if (sMailAdr.lStrNum > 0)
   {
      sprintf(acTmp, "%d       ", sMailAdr.lStrNum);
      memcpy(pOutbuf+OFF_M_STRNUM, acTmp, SIZ_M_STRNUM);
      if (sMailAdr.strSub[0] > '0')
      {
         sprintf(acTmp, "%s  ", sMailAdr.strSub);
         memcpy(pOutbuf+OFF_M_STR_SUB, acTmp, SIZ_M_STR_SUB);
      }
      memcpy(pOutbuf+OFF_M_DIR, sMailAdr.strDir, strlen(sMailAdr.strDir));
      memcpy(pOutbuf+OFF_M_STREET, sMailAdr.strName, strlen(sMailAdr.strName));
      memcpy(pOutbuf+OFF_M_SUFF, sMailAdr.strSfx, strlen(sMailAdr.strSfx));
      if (sMailAdr.Unit[0] > ' ')
         memcpy(pOutbuf+OFF_M_UNITNO, sMailAdr.Unit, strlen(sMailAdr.Unit));
      else if (pVal)
      {  // catching "# A8", "SUITE123"
         remChar(pVal, 32);
         iTmp = strlen(pVal);
         if (iTmp > SIZ_M_UNITNO)
         {
            if (!memcmp(pVal, "SUITE", 5))
            {
               pVal += 5;
               iTmp -= 5;
            } else
               iTmp = SIZ_M_UNITNO;
         }
         memcpy(pOutbuf+OFF_M_UNITNO, pVal, iTmp);
      }
   } else
      vmemcpy(pOutbuf+OFF_M_STREET, acAddr1, SIZ_M_STREET);

   if (*apTokens[MRN_L_MAILCITY] > ' ')
   {
      p2 = myTrim(apTokens[MRN_L_MAILCITY]);
      vmemcpy(pOutbuf+OFF_M_CITY, p2, SIZ_M_CITY);

      if (*apTokens[MRN_L_MAILSTATE] > ' ')
         memcpy(pOutbuf+OFF_M_ST, apTokens[MRN_L_MAILSTATE], SIZ_M_ST);

      // Zipcode
      if (!memcmp(apTokens[MRN_L_MAILZIP], "C4924", 5))
         *apTokens[MRN_L_MAILZIP] = '9';
      else if (*apTokens[MRN_L_MAILZIP] != '9')
         *apTokens[MRN_L_MAILZIP] = 0;

      vmemcpy(pOutbuf+OFF_M_ZIP, apTokens[MRN_L_MAILZIP], SIZ_M_ZIP);

      sprintf(acAddr2, "%s %s %.5s", apTokens[MRN_L_MAILCITY], apTokens[MRN_L_MAILSTATE], apTokens[MRN_L_MAILZIP]);
      iTmp = blankRem(acAddr2);
      vmemcpy(pOutbuf+OFF_M_CTY_ST_D, acAddr2, SIZ_M_CTY_ST_D, iTmp);
   }
}

/******************************** Mrn_ConvStdChar ****************************
 *
 *
 *****************************************************************************/

int Mrn_ConvStdChar(char *pInfile)
{
   FILE     *fdIn, *fdOut;
   char     acBuf[MAX_RECSIZE], acTmpFile[256], acTmp[256], acCode[4], *pRec, *pTmp, cTmp;
   int      iRet, iTmp, lTmp, iFldCnt, iCnt=0;
   double   dTmp;

   STDCHAR  myCharRec;

   LogMsg0("Extracting Chars");
   LogMsg("Open char file %s", pInfile);
   if (!(fdIn = fopen(pInfile, "r")))
      return -1;

   sprintf(acTmpFile, "%s\\%s\\%s_char.tmp", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   if (!(fdOut = fopen(acTmpFile, "w")))
   {
      fclose(fdIn);
      LogMsg("***** Error creating output file %s", acTmpFile);
      return -2;
   }

   // Start loop
   while (!feof(fdIn))
   {
      pRec = fgets(acBuf, MAX_RECSIZE, fdIn);

      if (!pRec)
         break;

      if (acBuf[0] <= ' ')
         continue;

      iFldCnt = ParseStringIQ(pRec, 9, MAX_FLD_TOKEN, apTokens);
      if (iFldCnt < C_TRA)
      {
         if (iFldCnt > 1)
            LogMsg("*** Bad CHAR record (%d): %s [%d]", iCnt, pRec, iFldCnt);
         continue;
      }

      memset((void *)&myCharRec, ' ', sizeof(STDCHAR));
      
      // Format APN
      memcpy(myCharRec.Apn_D, apTokens[C_FMT_APN], strlen(apTokens[C_FMT_APN]));
      iRet = remChar(apTokens[C_FMT_APN], '-');
      memcpy(myCharRec.Apn, apTokens[C_FMT_APN], iRet);

      // Land Use
      if (*apTokens[C_USECODE] >= ' ')
      {
         iRet = iTrim(apTokens[C_USECODE]);
         memcpy(myCharRec.LandUse, apTokens[C_USECODE], iRet);
      }

      // Units Count
      iTmp = atol(apTokens[C_NUM_OF_UNITS]);
      if (iTmp > 1)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.Units, acTmp, iRet);
      } 

      // LotSqft
      lTmp = atol(apTokens[C_LOT_SQFT]);
      if (lTmp > 0)
      {
         iTmp = sprintf(acTmp, "%d", lTmp);
         memcpy(myCharRec.LotSqft, acTmp, iTmp);

         dTmp = ((double)lTmp / SQFT_FACTOR_1000) + 0.5;
         iTmp = sprintf(acTmp, "%d", (long)dTmp);
         memcpy(myCharRec.LotAcre, acTmp, iTmp);
      }

      // Deck sqft
      lTmp = atol(apTokens[C_DECK_SQFT]);
      if (lTmp > 0)
      {
         iTmp = sprintf(acTmp, "%d", lTmp);
         memcpy(myCharRec.DeckSqft, acTmp, iTmp);
      }

      // Bldg Sqft
      lTmp = atoi(apTokens[C_LIV_AREA]);
      if (lTmp > 100)
      {
         iRet = sprintf(acTmp, "%d", lTmp);
         memcpy(myCharRec.BldgSqft, acTmp, iRet);
      }

#ifdef _DEBUG
      //if (!memcmp(myCharRec.Apn, "01508131", 8))
      //   iRet = 0;
#endif

      // Fireplace
      iTmp = atoi(apTokens[C_FIREPLACES]);
      if (iTmp > 0)
      {
         if (iTmp < 10)
            myCharRec.Fireplace[0] = *apTokens[C_FIREPLACES];
         else
            myCharRec.Fireplace[0] = 'M'; // 10 or more
      }

      // Heat type
      // Central                    69279 =Z
      // Electric Baseboard         291   =F
      // Floor/Wall                 1687  =M
      // None                       59    =L
      // Other                      213   =X
      // Radiant                    457   =I
      cTmp = *apTokens[C_HEAT_TYPE];
      if (cTmp > ' ')
      {
         if (cTmp == 'C')
            myCharRec.Heating[0] = 'Z';
         else if (cTmp == 'E')
            myCharRec.Heating[0] = 'F';
         else if (cTmp == 'F')
            myCharRec.Heating[0] = 'M';
         else if (cTmp == 'N')
            myCharRec.Heating[0] = 'L';
         else if (cTmp == 'O')
            myCharRec.Heating[0] = 'X';
         else if (cTmp == 'R')
            myCharRec.Heating[0] = 'I';
      }

      // Bldg Class
      if (*apTokens[C_SBE_CLASS] > ' ')
      {
         // 5, 5.5 ...
         vmemcpy(myCharRec.QualityClass, apTokens[C_SBE_CLASS], CSIZ_SBE_CLASS);
         dTmp = atof(apTokens[C_SBE_CLASS]);
         sprintf(acTmp, "%.1f", dTmp);
         iRet = Value2Code(acTmp, acCode, NULL);
         myCharRec.BldgQual = acCode[0];
      }

      // Yrblt
      lTmp = atoi(apTokens[C_YRBLT]);
      if (lTmp > 1800)
         vmemcpy(myCharRec.YrBlt, apTokens[C_YRBLT], SIZ_YR_BLT);

      // Eff Yr
      lTmp = atoi(apTokens[C_EFFYR]);
      if (lTmp > 1900)
         memcpy(myCharRec.YrEff, apTokens[C_EFFYR], SIZ_YR_BLT);

      // Beds
      iTmp = atoi(apTokens[C_BEDS]);
      if (iTmp > 0)
      {
         iRet = sprintf(acTmp, "%d", iTmp);
         memcpy(myCharRec.Beds, acTmp, iRet);
      }

      // Full baths
      if (*apTokens[C_BATHS] > '0')
      {
         iTmp = atoi(apTokens[C_BATHS]);
         if (iTmp > 10 && *(apTokens[C_BATHS]+1) == '5')
         {  // APN=01221401, 06023203, 10614016
            myCharRec.FBaths[0] = *apTokens[C_BATHS];
            myCharRec.HBaths[0] = '1';
            myCharRec.Bath_2Q[0] = '1';
         } else
         {
            iRet = sprintf(acTmp, "%d", iTmp);
            memcpy(myCharRec.FBaths, acTmp, iRet);
            memcpy(myCharRec.Bath_4Q, acTmp, iRet);
         }
      }

      // Half bath
      if ((pTmp=strchr(apTokens[C_BATHS], '.')) && *(pTmp+1) > '0')
      {
         myCharRec.HBaths[0] = '1';
         if (*(pTmp+1) > '5')
            myCharRec.Bath_3Q[0] = '1';
         else if (*(pTmp+1) < '5')
            myCharRec.Bath_1Q[0] = '1';
         else
            myCharRec.Bath_2Q[0] = '1';
      }

      // Garage Sqft
      lTmp = atoi(apTokens[C_GARAGE_SQFT]);
      if (lTmp > 100)
      {
         iRet = sprintf(acTmp, "%d", lTmp);
         memcpy(myCharRec.GarSqft, acTmp, iRet);
         myCharRec.ParkType[0] = 'Z';
      }

      // Unfinished Sqft
      lTmp = atoi(apTokens[C_UNFINISHED_SQFT]);
      if (lTmp > 10)
      {
         iRet = sprintf(acTmp, "%d", lTmp);
         memcpy(myCharRec.MiscSqft, acTmp, iRet);
      }

      // Pool
      iTmp = atoi(apTokens[C_POOL_SQFT]);
      if (iTmp > 1)
         myCharRec.Pool[0] = 'P';

      myCharRec.CRLF[0] = '\n';
      myCharRec.CRLF[1] = '\0';
      fputs((char *)&myCharRec.Apn[0], fdOut);

      if (!(++iCnt % 1000))
         printf("\r%u", iCnt);
   }

   if (fdIn) fclose(fdIn);
   if (fdOut) fclose(fdOut);

   LogMsg("Number of records processed: %d", iCnt);

   // Sort output on ASMT
   if (iCnt > 100)
   {
      // Rename old CHAR file if needed
      if (!_access(acCChrFile, 0))
      {
         strcpy(acBuf, acCChrFile);
         replStr(acBuf, ".dat", ".sav");
         if (!_access(acBuf, 0))
         {
            LogMsg("Delete old %s", acBuf);
            DeleteFile(acBuf);
         }
         LogMsg("Rename %s to %s", acCChrFile, acBuf);
         rename(acCChrFile, acBuf);
      } 

      LogMsgD("\nSorting char file %s --> %s", acTmpFile, acCChrFile);
      // Sort on APN, YrBlt to keep the latest record on top
      iRet = sortFile(acTmpFile, acCChrFile, "S(1,14,C,A,53,4,C,D) F(TXT) ");
   } else
   {
      printf("\n");
      iRet = 0;
   }

   return iRet;
}

/********************************* Mrn_MergeChar *****************************
 *
 * APN format: 999-999-99
 *
 *****************************************************************************/

int Mrn_MergeChar(char *pOutbuf)
{
   static char acRec[MAX_RECSIZE], *pRec=NULL;
   char        acTmp[256], acCode[16],  acApn[16];
   long        lTmp, lBldgSqft, lGarSqft;
   int         iRet, iTmp, iLoop, iBeds, iFBath;
   double      dTmp;
   MRN_CHAR    *pChar;

   // Get first Char rec for first call
   if (!pRec)
   {
      do {
         pRec = fgets(acRec, MAX_RECSIZE, fdChar);
      } while (pRec && !isdigit(acRec[0]));
   }

   do
   {
      memcpy(acApn, acRec, CSIZ_FMT_APN);
      acApn[CSIZ_FMT_APN] = 0;
      remCharEx(acApn, " -");
      iTmp = strlen(acApn);

      // Compare Apn
      iLoop = memcmp(pOutbuf, acApn, iTmp);
      if (iLoop > 0)
      {
         if (bDebug)
            LogMsg0("Skip Char rec  %s", acApn);

         pRec = fgets(acRec, 1400, fdChar);
         if (!pRec)
         {
            fclose(fdChar);
            fdChar = NULL;
            return 1;      // EOF
         }
         lCharSkip++;
      } 
      //else if (iLoop < 0)
      //   lCharSkip++;
   } while (iLoop > 0);

   // If not match, return
   if (iLoop)
   {
      return 0;
   }

   // Replace TAB character with null
   replChar(acRec, 9, 0);
   pChar = (MRN_CHAR *)acRec;

   // Use Code
   if (pChar->UseCode[0] > ' ')
   {
      memset(pOutbuf+OFF_USE_STD, ' ', SIZ_USE_STD);
      memcpy(pOutbuf+OFF_USE_CO, pChar->UseCode, CSIZ_USECODE);
      updateStdUse(pOutbuf+OFF_USE_STD, pChar->UseCode, CSIZ_USECODE, pOutbuf);
   } else
      memcpy(pOutbuf+OFF_USE_STD, USE_UNASGN, SIZ_USE_STD);

   // Number of units
   iTmp = atoi(pChar->Num_Of_Units);
   if (iTmp > 0)
   {
      sprintf(acTmp, "%*u", SIZ_UNITS, iTmp);
      memcpy(pOutbuf+OFF_UNITS, acTmp, SIZ_UNITS);
   }

   // Lot sqft
   lTmp = atoi(pChar->Lot_Sqft);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*u", SIZ_LOT_SQFT, lTmp);
      memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);

      dTmp = ((double)lTmp / SQFT_FACTOR_1000) + 0.5;
      sprintf(acTmp, "%*u", SIZ_LOT_ACRES, (long)dTmp);
      memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);
   }

   // Fireplace
   iTmp = atoi(pChar->Fireplaces);
   memset(pOutbuf+OFF_FIRE_PL, ' ', SIZ_FIRE_PL);
   if (iTmp > 0)
   {
      if (iTmp < 10)
         *(pOutbuf+OFF_FIRE_PL) = pChar->Fireplaces[0];
      else
         *(pOutbuf+OFF_FIRE_PL) = 'M'; // 10 or more
   }

   // Heat type
   // Central                    69279 =Z
   // Electric Baseboard         291   =F
   // Floor/Wall                 1687  =M
   // None                       59    =L
   // Other                      213   =X
   // Radiant                    457   =I
   if (pChar->Heat_Type[0] > ' ')
   {
      if (pChar->Heat_Type[0] == 'C')
         *(pOutbuf+OFF_HEAT) = 'Z';
      else if (pChar->Heat_Type[0] == 'E')
         *(pOutbuf+OFF_HEAT) = 'F';
      else if (pChar->Heat_Type[0] == 'F')
         *(pOutbuf+OFF_HEAT) = 'M';
      else if (pChar->Heat_Type[0] == 'N')
         *(pOutbuf+OFF_HEAT) = 'L';
      else if (pChar->Heat_Type[0] == 'O')
         *(pOutbuf+OFF_HEAT) = 'X';
      else if (pChar->Heat_Type[0] == 'R')
         *(pOutbuf+OFF_HEAT) = 'I';
   }

   // Bldg Class
   if (pChar->Sbe_Class[0] > ' ')
   {
      memcpy(pOutbuf+OFF_QUALITYCLASS, pChar->Sbe_Class, SIZ_QUALITYCLASS);
      // 5, 5.5 ...
      dTmp = atof(pChar->Sbe_Class);
      sprintf(acTmp, "%.1f", dTmp);
      iRet = Value2Code(acTmp, acCode, NULL);
      blankPad(acCode, SIZ_BLDG_QUAL);
      memcpy(pOutbuf+OFF_BLDG_QUAL, acCode, SIZ_BLDG_QUAL);
   }

   // Yrblt
   lTmp = atoi(pChar->YrBlt);
   if (lTmp > 1800)
      memcpy(pOutbuf+OFF_YR_BLT, pChar->YrBlt, SIZ_YR_BLT);

   // Eff Yr
   lTmp = atoi(pChar->YrEff);
   if (lTmp > 1900)
      memcpy(pOutbuf+OFF_YR_EFF, pChar->YrEff, SIZ_YR_BLT);

   // Bldg sqft
   lBldgSqft = atoi(pChar->Liv_Area);
   if (lBldgSqft > 0)
   {
      sprintf(acTmp, "%*u", SIZ_BLDG_SF, lBldgSqft);
      memcpy(pOutbuf+OFF_BLDG_SF, acTmp, SIZ_BLDG_SF);
   }

   // Beds
   iBeds = atoi(pChar->Beds);
   if (iBeds > 0)
   {
      sprintf(acTmp, "%*u", SIZ_BEDS, iBeds);
      memcpy(pOutbuf+OFF_BEDS, acTmp, SIZ_BEDS);
   }

   // Clear old data
   *(pOutbuf+OFF_BATH_1Q) = ' ';
   *(pOutbuf+OFF_BATH_2Q) = ' ';
   *(pOutbuf+OFF_BATH_3Q) = ' ';
   *(pOutbuf+OFF_BATH_4Q) = ' ';
   memset(pOutbuf+OFF_BATH_H, ' ', 2);
   memset(pOutbuf+OFF_BATH_F, ' ', 2);

   // Full Bath
   if (pChar->Baths[0] > '0')
   {
      iFBath = atoin(pChar->Baths, CSIZ_BATHS);
      if (iFBath > 10 && pChar->Baths[1] == '5')
      {
         *(pOutbuf+OFF_BATH_F+1) = pChar->Baths[0];
         *(pOutbuf+OFF_BATH_4Q) = pChar->Baths[0];
         *(pOutbuf+OFF_BATH_H+1) = '1';
         *(pOutbuf+OFF_BATH_2Q) = '1';
      } else
      {
         sprintf(acTmp, "%*u", SIZ_BATH_F, iFBath);
         memcpy(pOutbuf+OFF_BATH_F, acTmp, SIZ_BATH_F);
         *(pOutbuf+OFF_BATH_4Q) = pChar->Baths[0];
      }
   }

   // Half bath
   if (pChar->Baths[1] == '.' && pChar->Baths[2] > '0')
   {
      memcpy(pOutbuf+OFF_BATH_H, " 1", 2);
      if (pChar->Baths[2] > '5')
         *(pOutbuf+OFF_BATH_3Q) = '1';
      else if (pChar->Baths[2] < '5')
         *(pOutbuf+OFF_BATH_1Q) = '1';
      else
         *(pOutbuf+OFF_BATH_2Q) = '1';
   }

   // Garage Sqft
   lGarSqft = atoi(pChar->Garage_Sqft);
   if (lGarSqft > 10)
   {
      sprintf(acTmp, "%*u", SIZ_GAR_SQFT, lGarSqft);
      memcpy(pOutbuf+OFF_GAR_SQFT, acTmp, SIZ_GAR_SQFT);
      *(pOutbuf+OFF_PARK_TYPE) = 'Z';
   }

   // Pool
   iTmp = atoi(pChar->Pool_Sqft);
   if (iTmp > 10)
      *(pOutbuf+OFF_POOL) = 'P';       // Pool

   // Situs addr and City if not yet identified
   if (*(pOutbuf+OFF_S_STRNUM) == ' ' && pChar->S_Addr[0] > ' ')
   {
      ADR_REC  sAdr;
      char     acAddr1[256];

      parseAdr1_2(&sAdr, myTrim(pChar->S_Addr));
      if (sAdr.lStrNum > 0)
      {
         iTmp = sprintf(acAddr1, "%ld ", sAdr.lStrNum);
         memcpy(pOutbuf+OFF_S_STRNUM, acAddr1, iTmp);
      }

      if (sAdr.strDir[0] > ' ')
         *(pOutbuf+OFF_S_DIR) = sAdr.strDir[0];

      if (sAdr.strName[0] > ' ')
         vmemcpy(pOutbuf+OFF_S_STREET, sAdr.strName, SIZ_S_STREET);
      if (sAdr.strSfx[0] > ' ')
         memcpy(pOutbuf+OFF_S_SUFF, sAdr.strSfx, strlen(sAdr.strSfx));

      // Copy unit#
      if (sAdr.Unit[0] > '0')
      {
         vmemcpy(pOutbuf+OFF_S_UNITNO, sAdr.Unit, SIZ_S_UNITNO);
         if (sAdr.UnitNox[0] > '0')
            vmemcpy(pOutbuf+OFF_S_UNITNOX, sAdr.UnitNox, SIZ_S_UNITNOX);
         else
            vmemcpy(pOutbuf+OFF_S_UNITNOX, sAdr.Unit, SIZ_S_UNITNOX);
      }

      memcpy(pOutbuf+OFF_S_ADDR_D, pChar->S_Addr, strlen(pChar->S_Addr));
   }

   if (*(pOutbuf+OFF_S_CITY) == ' ' && pChar->S_City[0] >= 'A')
   {
      Abbr2Code(myTrim(pChar->S_City), acCode, acTmp, pOutbuf);
      if (acCode[0] > ' ')
      {
         memcpy(pOutbuf+OFF_S_CITY, acCode, strlen(acCode));
         memcpy(pOutbuf+OFF_S_ST, "CA", 2);
         strcat(acTmp, " CA");
         blankRem(acTmp);
         memcpy(pOutbuf+OFF_S_CTY_ST_D, acTmp, strlen(acTmp));
      }
   }

   lCharMatch++;

   // Get next Char rec
   pRec = fgets(acRec, MAX_RECSIZE, fdChar);
   if (!pRec)
   {
      fclose(fdChar);
      fdChar = NULL;
   }

   return 0;
}

/***************************** Mrn_MergeSaleRec ******************************
 *
 * Merge new sale data into current sale.  Move other sales accordingly.
 * Only update seller if that is the sale from assessor.
 *
 *****************************************************************************/

int Mrn_MergeSaleRec(char *pOutbuf, char *pSaleRec)
{
   long  lCurSaleDt, lLstSaleDt, lPrice, lLastAmt;
   char  acTmp[128];
   MRN_CSAL *pRec = (MRN_CSAL *)pSaleRec;

   lLstSaleDt = atoin(pOutbuf+OFF_SALE1_DT, SIZ_SALE1_DT);
   lCurSaleDt = atoin(pRec->RecDate, SSIZ_DATE);
   if (lCurSaleDt < lLstSaleDt || lCurSaleDt > lToday)
      return -1;

   lPrice = atoin(pRec->SalePrice, SSIZ_AMOUNT);
   if (lCurSaleDt == lLstSaleDt)
   {
      lLastAmt = atoin(pOutbuf+OFF_SALE1_AMT, SIZ_SALE1_AMT);
      if (!lPrice && lLastAmt > 0)
         return 0;
   } else if (lLstSaleDt > 0)
   {
      // Move sale2 to sale3
      memcpy(pOutbuf+OFF_SALE3_DOC, pOutbuf+OFF_SALE2_DOC, SIZ_SALE3_DOC);
      memcpy(pOutbuf+OFF_SALE3_DT, pOutbuf+OFF_SALE2_DT, SIZ_SALE3_DT);
      memcpy(pOutbuf+OFF_SALE3_DOCTYPE, pOutbuf+OFF_SALE2_DOCTYPE, SIZ_SALE3_DOCTYPE);
      memcpy(pOutbuf+OFF_SALE3_AMT, pOutbuf+OFF_SALE2_AMT, SIZ_SALE3_AMT);
      memcpy(pOutbuf+OFF_SALE3_CODE, pOutbuf+OFF_SALE2_CODE, SIZ_SALE3_CODE);
      *(pOutbuf+OFF_AR_CODE3) = *(pOutbuf+OFF_AR_CODE2);

      // Move sale1 to sale2
      memcpy(pOutbuf+OFF_SALE2_DOC, pOutbuf+OFF_SALE1_DOC, SIZ_SALE3_DOC);
      memcpy(pOutbuf+OFF_SALE2_DT, pOutbuf+OFF_SALE1_DT, SIZ_SALE3_DT);
      memcpy(pOutbuf+OFF_SALE2_DOCTYPE, pOutbuf+OFF_SALE1_DOCTYPE, SIZ_SALE3_DOCTYPE);
      memcpy(pOutbuf+OFF_SALE2_AMT, pOutbuf+OFF_SALE1_AMT, SIZ_SALE3_AMT);
      memcpy(pOutbuf+OFF_SALE2_CODE, pOutbuf+OFF_SALE1_CODE, SIZ_SALE3_CODE);
      *(pOutbuf+OFF_AR_CODE2) = *(pOutbuf+OFF_AR_CODE1);

      memset(pOutbuf+OFF_SALE1_AMT, ' ', SIZ_SALE3_AMT);
      memset(pOutbuf+OFF_SALE1_DT, ' ', SIZ_SALE1_DT+SIZ_SALE1_DOC);
   }

   // Update current sale
   memcpy(pOutbuf+OFF_SALE1_DOC, pRec->DocNum, SSIZ_DOCNUM);
   memcpy(pOutbuf+OFF_SALE1_DT, pRec->RecDate, SIZ_SALE3_DT);

   // Round off to 100
   if (lPrice > 0)
   {
      sprintf(acTmp, "%*u00", SIZ_SALE1_AMT-2, lPrice/100);
      memcpy(pOutbuf+OFF_SALE1_AMT, acTmp, SIZ_SALE1_AMT);

      // Seller
      if (pRec->Grantor[0] > ' ')
         memcpy(pOutbuf+OFF_SELLER, pRec->Grantor, SIZ_SELLER);
   }

   // Update transfers
   // Why? 07/03/2010 - drop this since this is updated in MergeRoll()
   // Put it back 03/25/2011
   lLstSaleDt = atoin(pOutbuf+OFF_TRANSFER_DT, SIZ_TRANSFER_DT);
   if (lCurSaleDt >= lLstSaleDt)
   {
      memcpy(pOutbuf+OFF_TRANSFER_DOC, pRec->DocNum, SSIZ_DOCNUM);
      memcpy(pOutbuf+OFF_TRANSFER_DT, pRec->RecDate, SIZ_TRANSFER_DT);
   }

   if (lLastRecDate < lCurSaleDt)
      lLastRecDate = lCurSaleDt;
   *(pOutbuf+OFF_AR_CODE1) = 'A';

   return 1;
}

int Mrn_MergeCSaleRec(char *pOutbuf, char *pSaleRec)
{
   long  lCurSaleDt, lLstSaleDt, lPrice, lLastAmt;
   char  acTmp[128];
   SCSAL_REC *pRec = (SCSAL_REC *)pSaleRec;

   lLstSaleDt = atoin(pOutbuf+OFF_SALE1_DT, SIZ_SALE1_DT);
   lCurSaleDt = atoin(pRec->DocDate, SSIZ_DATE);
   if (lCurSaleDt < lLstSaleDt || lCurSaleDt > lToday)
      return -1;

   lPrice = atoin(pRec->SalePrice, SSIZ_AMOUNT);
   if (lCurSaleDt == lLstSaleDt)
   {
      lLastAmt = atoin(pOutbuf+OFF_SALE1_AMT, SIZ_SALE1_AMT);
      if (!lPrice && lLastAmt > 0)
         return 0;
   } else if (lLstSaleDt > 0)
   {
      // Move sale2 to sale3
      memcpy(pOutbuf+OFF_SALE3_DOC, pOutbuf+OFF_SALE2_DOC, SIZ_SALE3_DOC);
      memcpy(pOutbuf+OFF_SALE3_DT, pOutbuf+OFF_SALE2_DT, SIZ_SALE3_DT);
      memcpy(pOutbuf+OFF_SALE3_DOCTYPE, pOutbuf+OFF_SALE2_DOCTYPE, SIZ_SALE3_DOCTYPE);
      memcpy(pOutbuf+OFF_SALE3_AMT, pOutbuf+OFF_SALE2_AMT, SIZ_SALE3_AMT);
      memcpy(pOutbuf+OFF_SALE3_CODE, pOutbuf+OFF_SALE2_CODE, SIZ_SALE3_CODE);
      *(pOutbuf+OFF_AR_CODE3) = *(pOutbuf+OFF_AR_CODE2);

      // Move sale1 to sale2
      memcpy(pOutbuf+OFF_SALE2_DOC, pOutbuf+OFF_SALE1_DOC, SIZ_SALE3_DOC);
      memcpy(pOutbuf+OFF_SALE2_DT, pOutbuf+OFF_SALE1_DT, SIZ_SALE3_DT);
      memcpy(pOutbuf+OFF_SALE2_DOCTYPE, pOutbuf+OFF_SALE1_DOCTYPE, SIZ_SALE3_DOCTYPE);
      memcpy(pOutbuf+OFF_SALE2_AMT, pOutbuf+OFF_SALE1_AMT, SIZ_SALE3_AMT);
      memcpy(pOutbuf+OFF_SALE2_CODE, pOutbuf+OFF_SALE1_CODE, SIZ_SALE3_CODE);
      *(pOutbuf+OFF_AR_CODE2) = *(pOutbuf+OFF_AR_CODE1);

      ClearOneSale(pOutbuf, 1);
   }

   // Update current sale
   memcpy(pOutbuf+OFF_SALE1_DOC, pRec->DocNum, SSIZ_DOCNUM);
   memcpy(pOutbuf+OFF_SALE1_DT, pRec->DocDate, SIZ_SALE3_DT);

   // Round off to 100
   if (lPrice > 0)
   {
      sprintf(acTmp, "%*u00", SIZ_SALE1_AMT-2, lPrice/100);
      memcpy(pOutbuf+OFF_SALE1_AMT, acTmp, SIZ_SALE1_AMT);

      // Seller
      if (pRec->Seller1[0] > ' ')
      {
         // Replace known bad char
         replUChar((unsigned char *)&pRec->Seller1[0], 0xCB, 'U', SIZ_SELLER);
         memcpy(pOutbuf+OFF_SELLER, pRec->Seller1, SIZ_SELLER);
      }
   }

   // Update transfers
   // Why? 07/03/2010 - drop this since this is updated in MergeRoll()
   // Put it back 03/25/2011
   lLstSaleDt = atoin(pOutbuf+OFF_TRANSFER_DT, SIZ_TRANSFER_DT);
   if (lCurSaleDt >= lLstSaleDt)
   {
      memcpy(pOutbuf+OFF_TRANSFER_DOC, pRec->DocNum, SSIZ_DOCNUM);
      memcpy(pOutbuf+OFF_TRANSFER_DT, pRec->DocDate, SIZ_TRANSFER_DT);
   }

   if (lLastRecDate < lCurSaleDt)
      lLastRecDate = lCurSaleDt;
   *(pOutbuf+OFF_AR_CODE1) = 'A';

   return 1;
}

/********************************* Mrn_MergeSale *****************************
 *
 *
 *
 *****************************************************************************/

int Mrn_MergeSale(char *pOutbuf, bool bNewRec=false)
{
   static   char  acRec[1024], *pRec=NULL;
   char     *pTmp;
   int      iRet, iTmp, iLoop;
   MRN_CSAL *pSaleRec;

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "009000130", 9) )
   //   iRet = 0;
#endif

   pSaleRec = (MRN_CSAL *)&acRec[0];

   // Get first sale rec for first call
   if (!pRec)
   {
      do {
         pRec = fgets(acRec, 1024, fdSale);
      } while (pRec && !isdigit(acRec[0]));
   }

   do
   {
      // Compare Apn
      iLoop = memcmp(pOutbuf, pSaleRec->Apn, myCounty.iApnLen);
      if (iLoop > 0)
      {
         if (bDebug)
            LogMsg0("Skip Sale rec  %.8s", pSaleRec->Apn);
         pRec = fgets(acRec, 1024, fdSale);
         if (!pRec)
         {
            fclose(fdSale);
            fdSale = NULL;
            return 1;      // EOF
         }

         lSaleSkip++;
      }
   } while (iLoop > 0);

   // If not match, return
   if (iLoop)
      return 0;

   iTmp = atoin(pSaleRec->Apn, 3);
   do
   {
#ifdef _DEBUG
      //if (!memcmp(pOutbuf, "90001202", 8))
      //   iTmp++;
#endif
      // Ignore this record if last byte is alpha
      pTmp = &pSaleRec->DocNum[SSIZ_DOCNUM-1];     
      while (*pTmp == ' ') pTmp--;
      if (isdigit(*pTmp) || iTmp > 800)
      {
         // Return 1 if new sale is insert
         iRet = Mrn_MergeSaleRec(pOutbuf, acRec);
      } else if (bDebug)
      {
         LogMsg0("---> Ignore sale %.12s", pSaleRec->DocNum);
         lSaleSkip++;
      }

      // Get next sale record
      pRec = fgets(acRec, 1024, fdSale);
      if (!pRec)
      {
         fclose(fdSale);
         fdSale = NULL;
         break;
      }

      // Update flag
      *(pOutbuf+OFF_AR_CODE1) = 'A';           // Assessor-Recorder code
   } while (!memcmp(pOutbuf, pSaleRec->Apn, myCounty.iApnLen));

   lSaleMatch++;

   return 0;
}

int Mrn_MergeCSale(char *pOutbuf)
{
   static    char  acRec[1024], *pRec=NULL;
   char      *pTmp;
   int       iRet, iTmp, iLoop;
   SCSAL_REC *pSaleRec;

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "009000130", 9) )
   //   iRet = 0;
#endif

   pSaleRec = (SCSAL_REC *)&acRec[0];

   // Get first sale rec for first call
   if (!pRec)
   {
      do {
         pRec = fgets(acRec, 1024, fdSale);
      } while (pRec && !isdigit(acRec[0]));
   }

   do
   {
      // Compare Apn
      iLoop = memcmp(pOutbuf, pSaleRec->Apn, iApnLen);
      if (iLoop > 0)
      {
         if (bDebug)
            LogMsg0("Skip Sale rec  %.*s", iApnLen, pSaleRec->Apn);
         pRec = fgets(acRec, 1024, fdSale);
         if (!pRec)
         {
            fclose(fdSale);
            fdSale = NULL;
            return 1;      // EOF
         }

         lSaleSkip++;
      }
   } while (iLoop > 0);

   // If not match, return
   if (iLoop)
      return 1;

   iTmp = atoin(pSaleRec->Apn, 3);
   do
   {
      // Ignore this record if last byte is alpha
      pTmp = &pSaleRec->DocNum[SSIZ_DOCNUM-1];      // point to last byte of DocNum
      while (*pTmp == ' ') pTmp--;
      if (isdigit(*pTmp) || iTmp > 800)
      {
         // Return 1 if new sale is insert
         iRet = Mrn_MergeCSaleRec(pOutbuf, acRec);
      } else if (bDebug)
      {
         LogMsg0("---> Ignore sale %.12s", pSaleRec->DocNum);
         lSaleSkip++;
      }

      // Get next sale record
      pRec = fgets(acRec, 1024, fdSale);
      if (!pRec)
      {
         fclose(fdSale);
         fdSale = NULL;
         break;
      }

      // Update flag
      *(pOutbuf+OFF_AR_CODE1) = 'A';           // Assessor-Recorder code
   } while (!memcmp(pOutbuf, pSaleRec->Apn, iApnLen));

   lSaleMatch++;

   return 0;
}

/********************************* Mrn_MergeRoll *****************************
 *
 * Use this function when running monthly update.
 * Return 0 if successful, >0 if error
 *
 *****************************************************************************/

int Mrn_MergeRoll(char *pOutbuf, char *pRollRec, int iLen, int iFlag)
{
   char     acTmp[256], acTmp1[256], acDocNum[32];
   LONGLONG lTmp;
   double   dTmp;
   int      iRet;

   MRN_ROLL *pRec = (MRN_ROLL *)pRollRec;

   if (iFlag & CREATE_R01)
   {
      // Clear output buffer
      memset(pOutbuf, ' ', iRecLen);

      // Start copying data
      memcpy(pOutbuf, pRec->Apn, RSIZ_APN);
      memcpy(pOutbuf+OFF_CO_NUM, "21MRN", 5);

      // Format APN
      iRet = formatApn(pRec->Apn, acTmp, &myCounty);
      memcpy(pOutbuf+OFF_APN_D, acTmp, iRet);

      // Create MapLink
      iRet = formatMapLink(pRec->Apn, acTmp, &myCounty);
      memcpy(pOutbuf+OFF_MAPLINK, acTmp, iRet);

      // Create index map link
      if (getIndexPage(acTmp, acTmp1, &myCounty))
         memcpy(pOutbuf+OFF_IMAPLINK, acTmp1, iRet);

      // Year assessed
      memcpy(pOutbuf+OFF_YR_ASSD, myCounty.acYearAssd, 4);

      // Land
      long lLand = atoin(pRec->Prior_Land, RSIZ_LAND);
      if (lLand > 0)
      {
         sprintf(acTmp, "%*u", SIZ_LAND, lLand);
         memcpy(pOutbuf+OFF_LAND, acTmp, SIZ_LAND);
      }

      // Improve
      long lImpr = atoin(pRec->Prior_Impr, RSIZ_LAND);
      if (lImpr > 0)
      {
         sprintf(acTmp, "%*u", SIZ_IMPR, lImpr);
         memcpy(pOutbuf+OFF_IMPR, acTmp, SIZ_IMPR);
      }

      // Personal value
      long lPers = atoin(pRec->Prior_PP_Val, RSIZ_LAND);
      // Fixture/Bus inventory
      long lBusInv = atoin(pRec->Prior_Bus_Inv, RSIZ_BUS_INV);

      // Other value
      lTmp = lPers+lBusInv;
      if (lTmp > 0)
      {
         sprintf(acTmp, "%*u", SIZ_OTHER, lTmp);
         memcpy(pOutbuf+OFF_OTHER, acTmp, SIZ_OTHER);

         if (lPers > 0)
         {
            sprintf(acTmp, "%*u", SIZ_PERSPROP, lPers);
            memcpy(pOutbuf+OFF_PERSPROP, acTmp, SIZ_PERSPROP);
         }
         if (lBusInv > 0)
         {
            sprintf(acTmp, "%*u", SIZ_BUSINV, lBusInv);
            memcpy(pOutbuf+OFF_BUSINV, acTmp, SIZ_BUSINV);
         }
      }

      // Gross total
      lTmp += lLand+lImpr;
      if (lTmp > 0)
      {
         sprintf(acTmp, "%*u", SIZ_GROSS, lTmp);
         memcpy(pOutbuf+OFF_GROSS, acTmp, SIZ_GROSS);

         // Ratio
         if (lImpr > 0)
         {
            dTmp = (LONGLONG)lImpr*100/(lLand+lImpr)+0.5;
            sprintf(acTmp, "%*u", SIZ_RATIO, (long)dTmp);
            memcpy(pOutbuf+OFF_RATIO, acTmp, SIZ_RATIO);
         }
      }
   }

   // Status
   //*(pOutbuf+OFF_STATUS) = 'A';

   // Acreage - Not usable
   // Units - Use the one from Char file

#ifdef _DEBUG
//   if (!memcmp(pRec->Apn, "10145008", 8))
//      iRet = 1;
#endif

   // TRA
   if (pRec->TRA[0] > ' ')
   {
      lTmp = atoin(pRec->TRA, RSIZ_TRA);
      sprintf(acTmp, "%.*d", DEFAULT_TRA_LEN, lTmp);
      memcpy(pOutbuf+OFF_TRA, acTmp, DEFAULT_TRA_LEN);
      //memcpy(pOutbuf+OFF_TRA, pRec->TRA, RSIZ_TRA);
   }

   // HO Exempt
   lTmp = atoin(pRec->Ho_Exe, RSIZ_HO_EXE);
   if (lTmp > 0)
      *(pOutbuf+OFF_HO_FL) = '1';      // 'Y'
   else
      *(pOutbuf+OFF_HO_FL) = '2';      // 'N'

   if (pRec->Exe_Code2[0] > '0')
   {
      lTmp += atoin(pRec->Exe_Amt2, RSIZ_EXE_AMT2);
      lTmp += atoin(pRec->Exe_Amt3, RSIZ_EXE_AMT2);
      lTmp += atoin(pRec->Exe_Amt4, RSIZ_EXE_AMT2);
   }

   if (lTmp > 0)
   {
      sprintf(acTmp, "%*u", SIZ_EXE_TOTAL, lTmp);
      memcpy(pOutbuf+OFF_EXE_TOTAL, acTmp, SIZ_EXE_TOTAL);
   }

   // Owner
   Mrn_MergeOwner(pOutbuf, pRec->Name1, pRec->Name2);

   // Mailing
   try
   {
      Mrn_MergeMAdr(pOutbuf, pRollRec);
   } catch (...)
   {
      LogMsgD("***** Exception in Mrn_MergeMAdr(),  errno=%d", errno);
      return 1;
   }

   // Situs
   Mrn_MergeSAdr(pOutbuf, pRollRec);

   // Transfer - YYYYMMDD
   long lPrevXfer;
   lPrevXfer = atoin(pOutbuf+OFF_TRANSFER_DT, SIZ_TRANSFER_DT);
   lTmp = atoin(pRec->DocDate, RSIZ_DOCDATE);

   if (lTmp > lPrevXfer)
   {
      // Apply transfer
      if (isValidYMD(pRec->DocDate))
      {
         memcpy(pOutbuf+OFF_TRANSFER_DT, pRec->DocDate, SIZ_TRANSFER_DT);
         if (pRec->DocNum[0] > ' ')
         {
            memcpy(acTmp, pRec->DocNum, RSIZ_DOCNUM);
            acTmp[RSIZ_DOCNUM] = 0;
            iRet = Mrn_FormatDoc(acDocNum, acTmp);
            if (!iRet)
               memcpy(pOutbuf+OFF_TRANSFER_DOC, acDocNum, strlen(acDocNum));
            else
               memcpy(pOutbuf+OFF_TRANSFER_DOC, pRec->DocNum, RSIZ_DOCNUM);
         }
      }
   }

   return 0;
}

/********************************** Mrn_UpdatePQ4 *****************************
 *
 * This function works the same as Load_Roll(), but also merge sale data.
 *
 ******************************************************************************/

int Mrn_UpdatePQ4(int iSkip)
{
   char     acRec[MAX_RECSIZE], acBuf[MAX_RECSIZE], acRollRec[MAX_RECSIZE];
   char     acRawFile[_MAX_PATH], acOutFile[_MAX_PATH], *pRoll;

   HANDLE   fhIn, fhOut;

   int      iRet, iResult, iRollUpd=0, iNewRec=0, iRetiredRec=0;
   DWORD    nBytesRead;
   DWORD    nBytesWritten;
   BOOL     bRet;
   long     lRet=0, lCnt=0;

   sprintf(acRawFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "S01");
   sprintf(acOutFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "R01");

   // Rename files for processing
   if (_access(acRawFile, 0))
   {
      if (!_access(acOutFile, 0))
         rename(acOutFile, acRawFile);
      else
      {
         LogMsg("***** Missing input file %s.  Please recheck!", acRawFile);
         return 1;
      }
   }

   // Open Roll file
   LogMsg("Open Roll file %s", acRollFile);
   fdRoll = fopen(acRollFile, "r");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", acRollFile);
      return 2;
   }

   // Open Char file
   LogMsg("Open Char file %s", acCharFile);
   fdChar = fopen(acCharFile, "r");
   if (fdChar == NULL)
   {
      LogMsg("***** Error opening Char file: %s\n", acCharFile);
      return 2;
   }

   // Open Sales file
   LogMsg("Open Sales file %s", acSaleFile);
   fdSale = fopen(acSaleFile, "r");
   if (fdSale == NULL)
   {
      LogMsg("***** Error opening Sales file: %s\n", acSaleFile);
      return 2;
   }

   // Open Input file
   LogMsg("Open input file %s", acRawFile);
   fhIn = CreateFile(acRawFile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhIn == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening input file: %s\n", acRawFile);
      return 2;
   }

   // Open Output file
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening output file: %s\n", acOutFile);
      return 2;
   }

   // Read first roll record
   //iRet = fread(acRollRec, 1, iRollLen, fdRoll);
   memset(acRollRec, ' ', iRollLen);
   pRoll = fgets(acRollRec, MAX_RECSIZE, fdRoll);

   // Copy skip record
   memset(acBuf, ' ', iRecLen);
   while (iSkip-- > 0)
   {
      ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);
      // Ignore record start with 99999999
      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
   }

   iNoMatch=iBadCity=iBadSuffix=0;

   // Merge loop
   while (pRoll)
   {
      bRet = ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);

#ifdef _DEBUG
      // Test problem that not all sale records go in
      //if (!memcmp(acBuf, "009000144", 9) )
      //   iRet = 0;
#endif

      // Check for EOF
      if (!bRet)
      {
         LogMsg("***** Error reading input file %s (%f)", acRawFile, GetLastError());
         break;
      }

      // EOF ?
      if (iRecLen != nBytesRead)
         break;

      NextRollRec:

      iResult = memcmp(acBuf, acRollRec, iApnLen);
      if (!iResult)
      {
         // Merge roll data
         Mrn_MergeRoll(acBuf, acRollRec, iRollLen, UPDATE_R01);
         iRollUpd++;

         // Merge Char
         if (fdChar)
            iRet = Mrn_MergeChar(acBuf);

         // Merge Sales
         if (fdSale)
            iRet = Mrn_MergeSale(acBuf);

         // Save last recording date
         lRet = atoin((char *)&acBuf[OFF_TRANSFER_DT], 8);
         if (lRet > lLastRecDate && lRet < lToday)
            lLastRecDate = lRet;

         bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);

         if (!(++lCnt % 1000))
            printf("\r%u update", lCnt);

         memset(acRollRec, ' ', iRollLen);
         pRoll = fgets(acRollRec, MAX_RECSIZE, fdRoll);
         //iRet = fread(acRollRec, 1, iRollLen, fdRoll);
         //if (iRet != iRollLen)
         //   bEof = true;
      } else if (iResult > 0)
      {
         if (bDebug)
            LogMsg0("*** New roll record : %.*s (%d) ***", iApnLen, acRollRec, lCnt);

         // Create new R01 record
         memset(acRec, ' ', iRecLen);
         Mrn_MergeRoll(acRec, acRollRec, iRollLen, CREATE_R01);
         iNewRec++;

         // Merge Char
         if (fdChar)
            iRet = Mrn_MergeChar(acRec);

         // Merge Sales
         if (fdSale)
            iRet = Mrn_MergeSale(acRec, true);

         // Save last recording date
         lRet = atoin((char *)&acRec[OFF_TRANSFER_DT], 8);
         if (lRet > lLastRecDate && lRet < lToday)
            lLastRecDate = lRet;

         bRet = WriteFile(fhOut, acRec, iRecLen, &nBytesWritten, NULL);

         if (!(++lCnt % 1000))
            printf("\r%u new", lCnt);

         //iRet = fread(acRollRec, 1, iRollLen, fdRoll);
         //if (iRet != iRollLen)
         //   bEof = true;    // Signal to stop
         //else
         //   goto NextRollRec;
         memset(acRollRec, ' ', iRollLen);
         pRoll = fgets(acRollRec, MAX_RECSIZE, fdRoll);
         if (pRoll)
            goto NextRollRec;
      } else
      {
         // Record may be retired
         if (bDebug)
            LogMsg0("*** Roll not match (retired record?) : R01->%.*s < Roll->%.*s (%d) ***", iApnLen, acBuf, iApnLen, acRollRec, lCnt);
         iRetiredRec++;
         continue;
      }
   }

   // Do the rest of the file
   while (pRoll)
   {
      if (bDebug)
         LogMsg0("*** New roll record : %.*s (%d) ***", iApnLen, acRollRec, lCnt);

      // Create new R01 record
      memset(acRec, ' ', iRecLen);
      Mrn_MergeRoll(acRec, acRollRec, iRollLen, CREATE_R01);
      iNewRec++;

      // Merge Char
      if (fdChar)
         iRet = Mrn_MergeChar(acRec);

      // Merge Sale
      if (fdSale)
         iRet = Mrn_MergeSale(acBuf);

      // Save last recording date
      lRet = atoin((char *)&acRec[OFF_TRANSFER_DT], 8);
      if (lRet > lLastRecDate && lRet < lToday)
         lLastRecDate = lRet;

      bRet = WriteFile(fhOut, acRec, iRecLen, &nBytesWritten, NULL);
      lCnt++;

      memset(acRollRec, ' ', iRollLen);
      pRoll = fgets(acRollRec, MAX_RECSIZE, fdRoll);
      //iRet = fread(acRollRec, 1, iRollLen, fdRoll);
      //if (iRet != iRollLen)
      //   break;
   }

   // Close files
   if (fdRoll)
      fclose(fdRoll);
   if (fdChar)
      fclose(fdChar);
   if (fdSale)
      fclose(fdSale);

   if (fhOut)
      CloseHandle(fhOut);
   if (fhIn)
      CloseHandle(fhIn);

   LogMsg("Total output records:       %u", lCnt);
   LogMsg("Total new records:          %u", iNewRec);
   LogMsg("Total retired records:      %u", iRetiredRec);
   LogMsg("Total bad-city records:     %u", iBadCity);
   LogMsg("Total bad-suffix records:   %u\n", iBadSuffix);

   LogMsg("Total sale matched:         %u", lSaleMatch);
   LogMsg("Total sale skipped:         %u", lSaleSkip);
   LogMsg("Total char matched:         %u", lCharMatch);
   LogMsg("Total char skipped:         %u", lCharSkip);

   printf("\nTotal output records: %u\n", lCnt);

   // Check for NULL embeded in file
   iRet = replEmbededChar(acOutFile, NULL, 32, 32, -1, iRecLen);
   if (iRet < 0)
      LogMsg("***** Error opening %s for checking null", acOutFile);
   else if (iRet > 0)
      LogMsg("*** Ctrl character found in %s.  Please contact Sony", acOutFile);

   lRecCnt = lCnt;

   return 0;
}

/********************************** Mrn_Load_Roll *****************************
 *
 * This function merges sale data to R01
 *
 ******************************************************************************/

int Mrn_Load_Roll(int iSkip)
{
   char     acRec[MAX_RECSIZE], acBuf[MAX_RECSIZE], acRollRec[MAX_RECSIZE];
   char     acRawFile[_MAX_PATH], acOutFile[_MAX_PATH], *pRoll;

   HANDLE   fhIn, fhOut;

   int      iRet, iResult, iRollUpd=0, iNewRec=0, iRetiredRec=0;
   DWORD    nBytesRead;
   DWORD    nBytesWritten;
   BOOL     bRet;
   long     lRet=0, lCnt=0;

   LogMsg0("Loading Roll update");

   sprintf(acRawFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "S01");
   sprintf(acOutFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "R01");

   // Rename files for processing
   if (_access(acRawFile, 0))
   {
      if (!_access(acOutFile, 0))
         rename(acOutFile, acRawFile);
      else
      {
         LogMsg("***** Missing input file %s.  Please recheck!", acRawFile);
         return 1;
      }
   }

   // Open Roll file
   LogMsg("Open Roll file %s", acRollFile);
   fdRoll = fopen(acRollFile, "r");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", acRollFile);
      return 2;
   }

   // Open Char file
   LogMsg("Open Char file %s", acCharFile);
   fdChar = fopen(acCharFile, "r");
   if (fdChar == NULL)
   {
      LogMsg("***** Error opening Char file: %s\n", acCharFile);
      return 2;
   }

   // Open Sales file
   LogMsg("Open Sales file %s", acCSalFile);
   fdSale = fopen(acCSalFile, "r");
   if (fdSale == NULL)
   {
      LogMsg("***** Error opening Sales file: %s\n", acCSalFile);
      return 2;
   }

   // Open Input file
   LogMsg("Open input file %s", acRawFile);
   fhIn = CreateFile(acRawFile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhIn == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening input file: %s\n", acRawFile);
      return 2;
   }

   // Open Output file
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening output file: %s\n", acOutFile);
      return 2;
   }

   // Read first roll record
   //iRet = fread(acRollRec, 1, iRollLen, fdRoll);
   memset(acRollRec, ' ', iRollLen);
   pRoll = fgets(acRollRec, MAX_RECSIZE, fdRoll);

   // Copy skip record
   memset(acBuf, ' ', iRecLen);
   while (iSkip-- > 0)
   {
      ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);
      // Ignore record start with 99999999
      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
   }

   iNoMatch=iBadCity=iBadSuffix=0;

   // Merge loop
   while (pRoll)
   {
      bRet = ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);

#ifdef _DEBUG
      // Test problem that not all sale records go in
      //if (!memcmp(acBuf, "009000144", 9) )
      //   iRet = 0;
#endif

      // Check for EOF
      if (!bRet)
      {
         LogMsg("***** Error reading input file %s (%f)", acRawFile, GetLastError());
         break;
      }

      // EOF ?
      if (iRecLen != nBytesRead)
         break;

      NextRollRec:

      iResult = memcmp(acBuf, acRollRec, iApnLen);
      if (!iResult)
      {
         // Merge roll data
         if (Mrn_MergeRoll(acBuf, acRollRec, iRollLen, UPDATE_R01))
            break;

         iRollUpd++;

         // Merge Char
         if (fdChar)
            iRet = Mrn_MergeChar(acBuf);

         // Merge Sales
         if (fdSale)
            iRet = Mrn_MergeCSale(acBuf);

         // Save last recording date
         lRet = atoin((char *)&acBuf[OFF_TRANSFER_DT], 8);
         if (lRet > lLastRecDate && lRet < lToday)
            lLastRecDate = lRet;

         bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);

         if (!(++lCnt % 1000))
            printf("\r%u update", lCnt);

         memset(acRollRec, ' ', iRollLen);
         pRoll = fgets(acRollRec, MAX_RECSIZE, fdRoll);
         //iRet = fread(acRollRec, 1, iRollLen, fdRoll);
         //if (iRet != iRollLen)
         //   bEof = true;
      } else if (iResult > 0)
      {
         if (bDebug)
            LogMsg0("*** New roll record : %.*s (%d) ***", iApnLen, acRollRec, lCnt);

         // Create new R01 record
         memset(acRec, ' ', iRecLen);
         if (Mrn_MergeRoll(acRec, acRollRec, iRollLen, CREATE_R01))
            break;

         iNewRec++;

         // Merge Char
         if (fdChar)
            iRet = Mrn_MergeChar(acRec);

         // Merge Sales
         if (fdSale)
            iRet = Mrn_MergeCSale(acRec);

         // Save last recording date
         lRet = atoin((char *)&acRec[OFF_TRANSFER_DT], 8);
         if (lRet > lLastRecDate && lRet < lToday)
            lLastRecDate = lRet;

         bRet = WriteFile(fhOut, acRec, iRecLen, &nBytesWritten, NULL);

         if (!(++lCnt % 1000))
            printf("\r%u new", lCnt);

         //iRet = fread(acRollRec, 1, iRollLen, fdRoll);
         //if (iRet != iRollLen)
         //   bEof = true;    // Signal to stop
         //else
         //   goto NextRollRec;
         memset(acRollRec, ' ', iRollLen);
         pRoll = fgets(acRollRec, MAX_RECSIZE, fdRoll);
         if (pRoll)
            goto NextRollRec;
      } else
      {
         // Record may be retired
         if (bDebug)
            LogMsg0("*** Roll not match (retired record?) : R01->%.*s < Roll->%.*s (%d) ***", iApnLen, acBuf, iApnLen, acRollRec, lCnt);
         iRetiredRec++;
         continue;
      }
   }

   // Do the rest of the file
   while (pRoll)
   {
      if (bDebug)
         LogMsg0("*** New roll record : %.*s (%d) ***", iApnLen, acRollRec, lCnt);

      // Create new R01 record
      memset(acRec, ' ', iRecLen);
      if (Mrn_MergeRoll(acRec, acRollRec, iRollLen, CREATE_R01))
         break;

      iNewRec++;

      // Merge Char
      if (fdChar)
         iRet = Mrn_MergeChar(acRec);

      // Merge Sale
      if (fdSale)
         iRet = Mrn_MergeCSale(acRec);

      // Save last recording date
      lRet = atoin((char *)&acRec[OFF_TRANSFER_DT], 8);
      if (lRet > lLastRecDate && lRet < lToday)
         lLastRecDate = lRet;

      bRet = WriteFile(fhOut, acRec, iRecLen, &nBytesWritten, NULL);
      lCnt++;

      memset(acRollRec, ' ', iRollLen);
      pRoll = fgets(acRollRec, MAX_RECSIZE, fdRoll);
   }

   // Close files
   if (fdRoll)
      fclose(fdRoll);
   if (fdChar)
      fclose(fdChar);
   if (fdSale)
      fclose(fdSale);

   if (fhOut)
      CloseHandle(fhOut);
   if (fhIn)
      CloseHandle(fhIn);

   LogMsg("Total output records:       %u", lCnt);
   LogMsg("Total new records:          %u", iNewRec);
   LogMsg("Total retired records:      %u", iRetiredRec);
   LogMsg("Total bad-city records:     %u", iBadCity);
   LogMsg("Total bad-suffix records:   %u\n", iBadSuffix);

   LogMsg("Total sale matched:         %u", lSaleMatch);
   LogMsg("Total sale skipped:         %u", lSaleSkip);
   LogMsg("Total char matched:         %u", lCharMatch);
   LogMsg("Total char skipped:         %u", lCharSkip);
   if (lDupOwners > 0)
      LogMsg("Total duplicate owners:     %u", lDupOwners);

   printf("\nTotal output records: %u\n", lCnt);

   // Check for NULL embeded in file
   iRet = replEmbededChar(acOutFile, NULL, 32, 32, -1, iRecLen);
   if (iRet < 0)
      LogMsg("***** Error opening %s for checking null", acOutFile);
   else if (iRet > 0)
      LogMsg("*** Ctrl character found in %s.  Please contact Sony", acOutFile);

   lRecCnt = lCnt;

   return 0;
}

/********************************* Mrn_MergeExe ******************************
 *
 * Merge exemption code from roll file during LDR processing
 *
 *****************************************************************************/

int Mrn_MergeExe(char *pOutbuf)
{
   static char acRec[MAX_RECSIZE], *pRec=NULL;
   int         iLoop;

   MRN_ROLL    *pRoll = (MRN_ROLL *)acRec;

   if (!pRec)
      pRec = fgets(acRec, MAX_RECSIZE, fdRoll);

   do
   {
      // Compare Apn
      iLoop = memcmp(pOutbuf, acRec, iApnLen);
      if (iLoop > 0)
      {
         pRec = fgets(acRec, 1400, fdRoll);
         if (!pRec)
         {
            fclose(fdRoll);
            fdRoll = NULL;
            return 1;      // EOF
         }
      } 
   } while (iLoop > 0);

   // If not match, return
   if (iLoop)
      return 1;

   if (pRoll->Exe_Code2[0] > ' ')
   {
      memcpy(pOutbuf+OFF_EXE_CD1, pRoll->Exe_Code2, RSIZ_EXECODE2);
      if (pRoll->Exe_Code3[0] > ' ')
      {
         memcpy(pOutbuf+OFF_EXE_CD2, pRoll->Exe_Code3, RSIZ_EXECODE2);
         if (pRoll->Exe_Code4[0] > ' ')
            memcpy(pOutbuf+OFF_EXE_CD3, pRoll->Exe_Code4, RSIZ_EXECODE2);
      }
   }

   lExeMatch++;

   return 0;
}

/******************************** Mrn_CreateRoll *****************************
 *
 * Use this function when running LDR 2015, 2017-2022.
 * Return 0 if successful, >0 if error
 *
 *****************************************************************************/

//int Mrn_CreateRoll(char *pOutbuf, char *pRollRec)
//{
//   char     acTmp[256], acTmp1[256], *pTmp;
//   double   dTmp;
//   int      iRet;
//   LONGLONG lTmp;
//
//   // 2019
//   //iTokens = ParseStringNQ(pRollRec, ',', MAX_FLD_TOKEN, apTokens);
//   // 2020
//   iTokens = ParseStringIQ(pRollRec, cLdrSep, MAX_FLD_TOKEN, apTokens);
//   if (iTokens < MRN_L_ROLLYEAR)
//   {
//      LogMsg("***** Error: bad input record for APN=%s [Tokens=%d]", apTokens[MRN_L_APN], iTokens);
//      return -1;
//   }
//
//   // Clear output buffer
//   memset(pOutbuf, ' ', iRecLen);
//
//   // Remove '-' in APN
//   strcpy(acTmp1, apTokens[MRN_L_APN]);
//   remChar(acTmp1, '-');
//
//   memcpy(pOutbuf, acTmp1, strlen(acTmp1));
//   memcpy(pOutbuf+OFF_CO_NUM, "21MRN", 5);
//
//   // Status
//   *(pOutbuf+OFF_STATUS) = 'A';
//
//   // Format APN
//   memcpy(pOutbuf+OFF_APN_D, apTokens[MRN_L_APN], strlen(apTokens[MRN_L_APN]));
//
//   // Create MapLink
//   iRet = formatMapLink(acTmp1, acTmp, &myCounty);
//   memcpy(pOutbuf+OFF_MAPLINK, acTmp, iRet);
//
//   // Create index map link
//   if (getIndexPage(acTmp, acTmp1, &myCounty))
//      memcpy(pOutbuf+OFF_IMAPLINK, acTmp1, iRet);
//
//   // Year assessed
//   memcpy(pOutbuf+OFF_YR_ASSD, myCounty.acYearAssd, 4);
//
//   // TRA
//   lTmp = atol(apTokens[MRN_L_TRA]);
//   sprintf(acTmp, "%.*d", DEFAULT_TRA_LEN, lTmp);
//   memcpy(pOutbuf+OFF_TRA, acTmp, DEFAULT_TRA_LEN);
//
//#ifdef _DEBUG
//   //if (!memcmp(pOutbuf, "00115031", 8) )
//   //   lTmp = 0;
//#endif
//
//   // Land
//   dollar2Num(apTokens[MRN_L_LAND], acTmp);
//   long lLand = atol(acTmp);
//   if (lLand > 0)
//   {
//      sprintf(acTmp, "%*u", SIZ_LAND, lLand);
//      memcpy(pOutbuf+OFF_LAND, acTmp, SIZ_LAND);
//   }
//
//   // Improve - Ratio
//   dollar2Num(apTokens[MRN_L_IMPR], acTmp);
//   long lImpr = atol(acTmp);
//   if (lImpr > 0)
//   {
//      sprintf(acTmp, "%*u", SIZ_IMPR, lImpr);
//      memcpy(pOutbuf+OFF_IMPR, acTmp, SIZ_IMPR);
//
//      // Ratio
//      dTmp = (LONGLONG)lImpr*100/(lLand+lImpr)+0.5;
//      sprintf(acTmp, "%*u", SIZ_RATIO, (long)dTmp);
//      memcpy(pOutbuf+OFF_RATIO, acTmp, SIZ_RATIO);
//   }
//
//   // PP value
//   dollar2Num(apTokens[MRN_L_PP_VALUE], acTmp);
//   long lPers = atol(acTmp);
//
//   // Bus Inventory
//   dollar2Num(apTokens[MRN_L_BUS_VALUE], acTmp);
//   long lBusInv = atol(acTmp);
//
//   // Total other
//   lTmp = lPers + lBusInv;
//   if (lTmp > 0)
//   {
//      sprintf(acTmp, "%*u", SIZ_OTHER, lTmp);
//      memcpy(pOutbuf+OFF_OTHER, acTmp, SIZ_OTHER);
//
//      if (lPers > 0)
//      {
//         sprintf(acTmp, "%d          ", lPers);
//         memcpy(pOutbuf+OFF_PERSPROP, acTmp, SIZ_OTHER);
//      }
//      if (lBusInv > 0)
//      {
//         sprintf(acTmp, "%d          ", lBusInv);
//         memcpy(pOutbuf+OFF_BUSINV, acTmp, SIZ_OTHER);
//      }
//   }
//
//   // Gross
//   lTmp += lLand + lImpr;
//   if (lTmp > 0)
//   {
//      sprintf(acTmp, "%*u", SIZ_GROSS, lTmp);
//      memcpy(pOutbuf+OFF_GROSS, acTmp, SIZ_GROSS);
//   }
//
//   // HO Exe - Exemp total
//   *(pOutbuf+OFF_HO_FL) = '2';         // N
//   dollar2Num(apTokens[MRN_L_TOTAL_EXE], acTmp);
//   long lExe = atol(acTmp);
//   if (lExe > 0)
//   {
//      sprintf(acTmp, "%*u", SIZ_EXE_TOTAL, lExe);
//      memcpy(pOutbuf+OFF_EXE_TOTAL, acTmp, SIZ_EXE_TOTAL);
//      if (lExe == 7000)
//      {
//         *(pOutbuf+OFF_HO_FL) = '1';      // 'Y'
//         memcpy(pOutbuf+OFF_EXE_CD1, "HO", 2);
//      } else
//      {
//         // Merge exemt code
//         if (fdRoll)
//            iRet = Mrn_MergeExe(pOutbuf);
//      }
//   }
//
//   // Owner
//   Mrn_MergeOwner(pOutbuf, apTokens[MRN_L_OWNER1], apTokens[MRN_L_OWNER2]);
//
//   // Mailing
//   Mrn_MergeMAdr(pOutbuf);
//
//   // Situs
//   // 2015 & 2017
//   if (iTokens > MRN_L_ROLLYEAR)
//   {
//      
//      if (!strcmp(myTrim(apTokens[MRN_L_SITUSADDR]), myTrim(apTokens[MRN_L_MAIL1])) && 
//          !strcmp(myTrim(apTokens[MRN_L_SITUSCITY]), myTrim(apTokens[MRN_L_MAILCITY])))
//         Mrn_MergeSitus(pOutbuf, apTokens[MRN_L_SITUSADDR], apTokens[MRN_L_SITUSCITY], apTokens[MRN_L_MAILZIP]);
//      else
//         Mrn_MergeSitus(pOutbuf, apTokens[MRN_L_SITUSADDR], apTokens[MRN_L_SITUSCITY]);
//
//      // Recorded Doc - take valid new docs only
//      pTmp = dateConversion(apTokens[MRN_L_RECDATE], acTmp, MM_DD_YYYY_1);
//      if (pTmp && *(apTokens[MRN_L_RECNUM]+2) == '-')
//      {
//         memcpy(pOutbuf+OFF_TRANSFER_DT, pTmp, SIZ_TRANSFER_DT);
//         iRet = sprintf(acTmp, "%.3s%.6d", apTokens[MRN_L_RECNUM], atol(apTokens[MRN_L_RECNUM]+3));
//         memcpy(pOutbuf+OFF_TRANSFER_DOC, acTmp, iRet);
//      }
//   } else
//   {
//      // 2016
//      if (!strcmp(apTokens[MRN_L_SITUSADDR], apTokens[MRN_L_MAIL1]))
//         Mrn_MergeSitus(pOutbuf, apTokens[MRN_L_SITUSADDR], apTokens[MRN_L_MAILCITY], apTokens[MRN_L_MAILZIP]);
//      else
//         Mrn_MergeSitus(pOutbuf, apTokens[MRN_L_SITUSADDR], " ");
//   }
//
//   // Usecode
//   if (*apTokens[MRN_L_USECODE] > ' ')
//   {
//      memcpy(pOutbuf+OFF_USE_CO, apTokens[MRN_L_USECODE], strlen(apTokens[MRN_L_USECODE]));
//      updateStdUse(pOutbuf+OFF_USE_STD, apTokens[MRN_L_USECODE], strlen(apTokens[MRN_L_USECODE]), pOutbuf);
//   } else
//      memcpy(pOutbuf+OFF_USE_STD, USE_UNASGN, SIZ_USE_STD);
//
//   // Units
//   lTmp = atol(apTokens[MRN_L_UNITS]);
//   if (lTmp > 0)
//   {
//      sprintf(acTmp, "%*u", SIZ_UNITS, lTmp);
//      memcpy(pOutbuf+OFF_UNITS, acTmp, SIZ_UNITS);
//   }
//
//   return 0;
//}

/******************************* Mrn_MergeLien *******************************
 *
 * Use this function when running LDR 2023.
 * Return 0 if successful, >0 if error
 *
 *****************************************************************************/

int Mrn_MergeLien(char *pOutbuf, char *pRollRec)
{
   char     acTmp[256], acTmp1[256];
   double   dTmp;
   int      iRet;
   LONGLONG lTmp;

   iTokens = ParseStringIQ(pRollRec, cLdrSep, MAX_FLD_TOKEN, apTokens);
   if (iTokens < MRN_L_ROLLYEAR)
   {
      LogMsg("***** Error: bad input record for APN=%s [Tokens=%d]", apTokens[MRN_L_APN], iTokens);
      return -1;
   }

   // Clear output buffer
   memset(pOutbuf, ' ', iRecLen);

   // Remove '-' in APN
   strcpy(acTmp1, apTokens[MRN_L_APN]);
   remChar(acTmp1, '-');

   memcpy(pOutbuf, acTmp1, strlen(acTmp1));
   memcpy(pOutbuf+OFF_CO_NUM, "21MRN", 5);

   // Status
   *(pOutbuf+OFF_STATUS) = 'A';

   // Format APN
   memcpy(pOutbuf+OFF_APN_D, apTokens[MRN_L_APN], strlen(apTokens[MRN_L_APN]));

   // Create MapLink
   iRet = formatMapLink(acTmp1, acTmp, &myCounty);
   memcpy(pOutbuf+OFF_MAPLINK, acTmp, iRet);

   // Create index map link
   if (getIndexPage(acTmp, acTmp1, &myCounty))
      memcpy(pOutbuf+OFF_IMAPLINK, acTmp1, iRet);

   // Year assessed
   memcpy(pOutbuf+OFF_YR_ASSD, myCounty.acYearAssd, 4);

   // TRA
   lTmp = atol(apTokens[MRN_L_TRA]);
   sprintf(acTmp, "%.*d", DEFAULT_TRA_LEN, lTmp);
   memcpy(pOutbuf+OFF_TRA, acTmp, DEFAULT_TRA_LEN);

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "00115031", 8) )
   //   lTmp = 0;
#endif

   // Land
   dollar2Num(apTokens[MRN_L_LAND], acTmp);
   long lLand = atol(acTmp);
   if (lLand > 0)
   {
      sprintf(acTmp, "%*u", SIZ_LAND, lLand);
      memcpy(pOutbuf+OFF_LAND, acTmp, SIZ_LAND);
   }

   // Improve - Ratio
   dollar2Num(apTokens[MRN_L_IMPR], acTmp);
   long lImpr = atol(acTmp);
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*u", SIZ_IMPR, lImpr);
      memcpy(pOutbuf+OFF_IMPR, acTmp, SIZ_IMPR);

      // Ratio
      dTmp = (LONGLONG)lImpr*100/(lLand+lImpr)+0.5;
      sprintf(acTmp, "%*u", SIZ_RATIO, (long)dTmp);
      memcpy(pOutbuf+OFF_RATIO, acTmp, SIZ_RATIO);
   }

   // PP value
   dollar2Num(apTokens[MRN_L_PP_VALUE], acTmp);
   long lPers = atol(acTmp);

   // Bus Inventory
   dollar2Num(apTokens[MRN_L_BUS_VALUE], acTmp);
   long lBusInv = atol(acTmp);

   // Total other
   lTmp = lPers + lBusInv;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*u", SIZ_OTHER, lTmp);
      memcpy(pOutbuf+OFF_OTHER, acTmp, SIZ_OTHER);

      if (lPers > 0)
      {
         sprintf(acTmp, "%d          ", lPers);
         memcpy(pOutbuf+OFF_PERSPROP, acTmp, SIZ_OTHER);
      }
      if (lBusInv > 0)
      {
         sprintf(acTmp, "%d          ", lBusInv);
         memcpy(pOutbuf+OFF_BUSINV, acTmp, SIZ_OTHER);
      }
   }

   // Gross
   lTmp += lLand + lImpr;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*u", SIZ_GROSS, lTmp);
      memcpy(pOutbuf+OFF_GROSS, acTmp, SIZ_GROSS);
   }

   // HO Exe - Exemp total
   *(pOutbuf+OFF_HO_FL) = '2';         // N
   dollar2Num(apTokens[MRN_L_TOTAL_EXE], acTmp);
   long lExe = atol(acTmp);
   if (lExe > 0)
   {
      sprintf(acTmp, "%*u", SIZ_EXE_TOTAL, lExe);
      memcpy(pOutbuf+OFF_EXE_TOTAL, acTmp, SIZ_EXE_TOTAL);
      if (lExe == 7000)
      {
         *(pOutbuf+OFF_HO_FL) = '1';      // 'Y'
         memcpy(pOutbuf+OFF_EXE_CD1, "HO", 2);
      } else
      {
         // Merge exemt code
         if (fdRoll)
            iRet = Mrn_MergeExe(pOutbuf);
      }

      // Create exemption type
      makeExeType(pOutbuf+OFF_EXE_TYPE, pOutbuf+OFF_EXE_CD1, pOutbuf+OFF_EXE_CD2, pOutbuf+OFF_EXE_CD3, (IDX_TBL4 *)&MRN_Exemption);
   }

   // Owner
   Mrn_MergeOwner(pOutbuf, apTokens[MRN_L_OWNER1], apTokens[MRN_L_OWNER2]);

   // Mailing
   Mrn_MergeMAdr(pOutbuf);

   // Situs
   if (*apTokens[MRN_L_SITUSADDR] > ' ' && !strcmp(myTrim(apTokens[MRN_L_SITUSADDR]), myTrim(apTokens[MRN_L_MAIL1])) )
      Mrn_MergeSitus(pOutbuf, apTokens[MRN_L_SITUSADDR], apTokens[MRN_L_MAILCITY], apTokens[MRN_L_MAILZIP]);
   else if (*apTokens[MRN_L_SITUSADDR] > ' ')
   {
      // May need to pull situs zip from roll update file
      Mrn_MergeSitus(pOutbuf, apTokens[MRN_L_SITUSADDR], "");
   }

   // Usecode
   if (*apTokens[MRN_L_USECODE] > ' ')
   {
      memcpy(pOutbuf+OFF_USE_CO, apTokens[MRN_L_USECODE], strlen(apTokens[MRN_L_USECODE]));
      updateStdUse(pOutbuf+OFF_USE_STD, apTokens[MRN_L_USECODE], strlen(apTokens[MRN_L_USECODE]), pOutbuf);
   } else
      memcpy(pOutbuf+OFF_USE_STD, USE_UNASGN, SIZ_USE_STD);

   // Units
   lTmp = atol(apTokens[MRN_L_UNITS]);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*u", SIZ_UNITS, lTmp);
      memcpy(pOutbuf+OFF_UNITS, acTmp, SIZ_UNITS);
   }

   return 0;
}

/********************************* Mrn_Load_LDR ****************************
 *
 *
 ****************************************************************************/

int Mrn_Load_LDR(int iFirstRec)
{
   char     acBuf[MAX_RECSIZE], acRollRec[1024], acOutFile[_MAX_PATH], *pTmp;
   long     iRet, lRet=0, lCnt=0;
   HANDLE   fhOut;
   DWORD    nBytesWritten;
   BOOL     bRet;
   FILE     *fdLdr;

   LogMsg0("Loading LDR data");

   // Open LDR file
   LogMsg("Open LDR file %s", acRollFile);
   fdLdr = fopen(acRollFile, "r");
   if (fdLdr == NULL)
   {
      printf("Error opening LDR file: %s\n", acRollFile);
      return -1;
   }

   // Open Roll update file to get exemption code
   GetIniString("MRN", "RollFile", "", acRollFile, _MAX_PATH, acIniFile);
   LogMsg("Open Roll file %s", acRollFile);
   fdRoll = fopen(acRollFile, "r");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", acRollFile);
      return -2;
   }

   // Open Char file
   LogMsg("Open Char file %s", acCharFile);
   fdChar = fopen(acCharFile, "r");
   if (fdChar == NULL)
   {
      LogMsg("***** Error opening Char file: %s\n", acCharFile);
      return -2;
   }

   // Open Sales file
   LogMsg("Open Sales file %s", acCSalFile);
   fdSale = fopen(acCSalFile, "r");
   if (fdSale == NULL)
   {
      LogMsg("***** Error opening Sales file: %s\n", acSaleFile);
      return -2;
   }

   // Open Output file
   sprintf(acOutFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "R01");
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
             FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      printf("Error opening output file: %s\n", acOutFile);
      return -3;
   }

   // Output first header record
   if (iFirstRec > 0)
   {
      memset(acBuf, '9', iRecLen);
      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
   }

   // Init variables
   iNoMatch=iBadCity=iBadSuffix=0;

   // Merge loop
   while (!feof(fdLdr))
   {
      if (!(pTmp = fgets(acRollRec, 1024, fdLdr) ))
         break;
      else if (acRollRec[1] > '9' || acRollRec[0] < '0')
         continue;

      lLDRRecCount++;

      // Create new R01 record
      //iRet = Mrn_CreateRoll(acBuf, acRollRec);
      iRet = Mrn_MergeLien(acBuf, acRollRec);
      if (!iRet)
      {
         // Merge Char
         if (fdChar)
            iRet = Mrn_MergeChar(acBuf);

         // Merge Sale
         if (fdSale)
            iRet = Mrn_MergeCSale(acBuf);

         bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
         if (!bRet)
         {
            LogMsg("***** Error occurs: %d\n", GetLastError());
            lRet = WRITE_ERR;
            break;
         }

         if (!(++lCnt % 1000))
            printf("\r%u", lCnt);
      }
   }

   // Close files
   if (fdRoll)
      fclose(fdRoll);
   if (fdLdr)
      fclose(fdLdr);
   if (fdChar)
      fclose(fdChar);
   if (fdSale)
      fclose(fdSale);
   if (fhOut)
      CloseHandle(fhOut);

   LogMsg("Total output records:       %u", lCnt);
   LogMsg("Total bad-city records:     %u", iBadCity);
   LogMsg("Total bad-suffix records:   %u\n", iBadSuffix);

   LogMsg("Total sale matched:         %u", lSaleMatch);
   LogMsg("Total sale skipped:         %u", lSaleSkip);
   LogMsg("Total char matched:         %u", lCharMatch);
   LogMsg("Total char skipped:         %u", lCharSkip);
   LogMsg("Total exe matched:          %u", lExeMatch);

   printf("\nTotal output records: %u\n", lCnt);

   lRecCnt = lCnt;
   return 0;
}

/****************************** Mrn_UpdateSaleHist ***************************
 *
 * Update history sale.  This function adds new sale record to cumulative file.
 *
 *
 * Return 0 if successful, otherwise error
 *
 *****************************************************************************/

int Mrn_FormatCSale(char *pFmtSale, char *pSaleRec)
{
   MRN_SALE *pSale = (MRN_SALE *)pSaleRec;
   MRN_CSAL *pCSal = (MRN_CSAL *)pFmtSale;
   int      iTmp;
   char     acTmp[32];
   double   dTmp;

   if (pSale->DocNum[0] < '0' || pSale->RecDate[0] < '0')
      return -1;

   // Inititalize
   memset(pFmtSale, ' ', sizeof(MRN_CSAL));

   // Confidential flag
   pCSal->Conf_Flag = pSale->Conf_Flag[0];
   pCSal->Wop = pSale->Wop[0];

   // APN
   sprintf(acTmp, "%.3s%.3s%.2s", pSale->Fmt_Apn, &pSale->Fmt_Apn[4], &pSale->Fmt_Apn[8]);
   memcpy(pCSal->Apn, acTmp, SSIZ_APN);

   // Recording info
   memcpy(pCSal->OrgRecNum, pSale->DocNum, SSIZ_DOCNUM);
   iTmp = Mrn_FormatDoc(acTmp, pSale->DocNum);
   if (acTmp[0] <= ' ')
      return 1;
   else
      memcpy(pCSal->DocNum, acTmp, strlen(acTmp));

   // Formatting RecDate mm/dd/yyyy -> yyyymmdd
   sprintf(acTmp, "%.4s%.2s%.2s", &pSale->RecDate[6], pSale->RecDate, &pSale->RecDate[3]);
   memcpy(pCSal->RecDate, acTmp, 8);

   // Sale price
   long lPrice = atol(pSale->SalePrice);
   if (lPrice > 0)
   {
      sprintf(acTmp, "%*u", SSIZ_AMOUNT, lPrice);
      memcpy(pCSal->SalePrice, acTmp, SSIZ_AMOUNT);
   }

   // DocTax
   dTmp = atof(pSale->StampAmt);
   if (dTmp > 0.0)
   {
      sprintf(acTmp, "%*.2f", SSIZ_AMOUNT, dTmp);
      memcpy(pCSal->StampAmt, acTmp, SSIZ_AMOUNT);
      if (lPrice <= 0)
      {
         lPrice = (long)(dTmp * SALE_FACTOR);
         sprintf(acTmp, "%*u", SSIZ_AMOUNT, lPrice);
         memcpy(pCSal->SalePrice, acTmp, SSIZ_AMOUNT);
      }
   }

   // Trust deed
   if (pSale->Td1_Amt[0] > '0')
   {
      memcpy(pCSal->Td1_Amt, pSale->Td1_Amt, SSIZ_AMOUNT);
      memcpy(pCSal->Td1_Lender, pSale->Td1_Lender, SSIZ_T_LENDER);
      if (pSale->Td2_Amt[0] > '0')
      {
         memcpy(pCSal->Td2_Amt, pSale->Td2_Amt, SSIZ_AMOUNT);
         memcpy(pCSal->Td2_Lender, pSale->Td2_Lender, SSIZ_T_LENDER);
         if (pSale->Td3_Amt[0] > '0')
         {
            memcpy(pCSal->Td3_Amt, pSale->Td3_Amt, SSIZ_AMOUNT);
            memcpy(pCSal->Td3_Lender, pSale->Td3_Lender, SSIZ_T_LENDER);
         }
      }
   }

   // Grantor
   if (pSale->Grantor[0] > ' ')
      memcpy(pCSal->Grantor, pSale->Grantor, SSIZ_NAME);

   // Grantee
   if (pSale->Grantee[0] > ' ')
      memcpy(pCSal->Grantee, pSale->Grantee, SSIZ_NAME);

   pCSal->CrLf[0] = '\n';
   pCSal->CrLf[1] = 0;

   return 0;
}

int Mrn_UpdateSaleHist(bool bUpdating)
{
   char     *pTmp, acCSalRec[512], acSaleRec[1024], acTmp[256];
   char     acOutFile[_MAX_PATH], acHistFile[256];
   int      iRet, iTmp, iConfSale, iUpdateSale;
   BOOL     bRet;
   long     lCnt=0;

   // Check current sale file
   if (_access(acSaleFile, 0))
   {
      LogMsg("***** Missing current sale file %s", acSaleFile);
      return -1;
   }

   // Open current sale
   LogMsg("Open current sale file %s", acSaleFile);
   fdSale = fopen(acSaleFile, "r");
   if (fdSale == NULL)
   {
      LogMsg("***** Error opening current sale file: %s\n", acSaleFile);
      return 2;
   }
   // Skip sale header
   MRN_SALE *pSale = (MRN_SALE *)&acSaleRec[0];
   do
   {
      pTmp = fgets(acSaleRec, 1024, fdSale);
   } while (!isdigit(pSale->DocNum[0]) || !isdigit(pSale->RecDate[0]));

   // Create output file
   sprintf(acOutFile, acSaleTmpl, myCounty.acCntyCode, "TMP");
   LogMsg("Create updated sale file %s", acOutFile);
   fdCSale = fopen(acOutFile, "w");
   if (fdCSale == NULL)
   {
      LogMsg("***** Error creating updated sale file: %s\n", acOutFile);
      return 2;
   }

   // Init counters
   iConfSale=iUpdateSale = 0;

   // Merge loop
   while (!feof(fdSale))
   {
      // Format cumsale record, skip confidential sale records
      if (acSaleRec[0] != 'Y')
      {
         // Remove tab
         replChar(acSaleRec, 9, 0);

         // Format sale record
         iRet = Mrn_FormatCSale(acCSalRec, acSaleRec);
         if (!iRet)
         {
            // Output current sale record
            fputs(acCSalRec, fdCSale);
            iUpdateSale++;
         }
      } else
         iConfSale++;

      // Get current sale
      pTmp = fgets(acSaleRec, 1024, fdSale);
      if (!pTmp)
         break;

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdCSale)
      fclose(fdCSale);
   if (fdSale)
      fclose(fdSale);

   if (bUpdating)
   {
      sprintf(acHistFile, acSaleTmpl, myCounty.acCntyCode, "SLS");
      strcat(acHistFile, "+");
      strcat(acHistFile, acOutFile);
   } else
      strcpy(acHistFile, acOutFile);
   sprintf(acSaleFile, acSaleTmpl, myCounty.acCntyCode, "DAT");

   // Sort sale file - APN (asc), Saledate (asc), DocNum (asc)
   sprintf(acTmp, "S(1,8,C,A,21,8,C,A,9,12,C,A) DUPOUT(1,28)");
   iTmp = sortFile(acHistFile, acSaleFile, acTmp);

   // Copy history file
   sprintf(acHistFile, acSaleTmpl, myCounty.acCntyCode, "SLS");
   bRet = CopyFile(acSaleFile, acHistFile, false);
   if (!bRet)
      LogMsg("***** Error copying %s to %s", acSaleFile, acHistFile);

   LogMsg("Total sale records processed:       %u", lCnt);
   LogMsg("Total confidential records skipped: %u", iConfSale);
   LogMsg("Total sale records updated:         %u", iUpdateSale);

   LogMsg("Update Sale History completed.");

   return 0;
}

/****************************** Mrn_ExtrSale *********************************
 *
 * Update history sale.  This function adds new sale record to cumulative file.
 *
 *
 * Return 0 if successful, otherwise error
 *
 *****************************************************************************/

int Mrn_FormatSCSale(char *pFmtSale, char *pSaleRec)
{
   MRN_SALE  *pSale = (MRN_SALE  *)pSaleRec;
   SCSAL_REC *pCSal = (SCSAL_REC *)pFmtSale;
   int      iTmp;
   char     acTmp[32];
   double   dTmp;

   if (pSale->DocNum[0] < '0' || pSale->RecDate[0] < '0')
      return -1;

   // Inititalize
   memset(pFmtSale, ' ', sizeof(SCSAL_REC));

   // APN
   iTmp = sprintf(acTmp, "%.3s%.3s%.2s", pSale->Fmt_Apn, &pSale->Fmt_Apn[4], &pSale->Fmt_Apn[8]);
   memcpy(pCSal->Apn, acTmp, iTmp);

   // Recording info
   iTmp = Mrn_FormatDoc(acTmp, pSale->DocNum);
   if (acTmp[0] <= ' ')
      return 1;
   else
      memcpy(pCSal->DocNum, acTmp, strlen(acTmp));

   // Formatting RecDate mm/dd/yyyy -> yyyymmdd
   sprintf(acTmp, "%.4s%.2s%.2s", &pSale->RecDate[6], pSale->RecDate, &pSale->RecDate[3]);
   memcpy(pCSal->DocDate, acTmp, 8);
   if (!isValidYMD(acTmp))
   {
      if (!memcmp(pCSal->Apn, "01307210", 8) && !memcmp(acTmp, "5964", 4))
         memcpy(pCSal->DocDate, "20010830", 8);
      else
         LogMsg("*** Invalid sale date %s [%.*s]", acTmp, iApnLen, pCSal->Apn);
   }

   // Sale price
   long lPrice = atol(pSale->SalePrice);
   if (lPrice > 0)
   {
      sprintf(acTmp, "%*u", SALE_SIZ_SALEPRICE, lPrice);
      memcpy(pCSal->SalePrice, acTmp, SALE_SIZ_SALEPRICE);
   }

   // DocTax
   dTmp = atof(pSale->StampAmt);
   if (dTmp > 0.0)
   {
      iTmp = sprintf(acTmp, "%*.2f", SSIZ_AMOUNT, dTmp);
      memcpy(pCSal->StampAmt, acTmp, iTmp);
      if (lPrice <= 0)
      {
         lPrice = (long)(dTmp * SALE_FACTOR);
         sprintf(acTmp, "%*u", SALE_SIZ_SALEPRICE, lPrice);
         memcpy(pCSal->SalePrice, acTmp, SALE_SIZ_SALEPRICE);
      }
   }

   // Grantor
   if (pSale->Grantor[0] > ' ')
      memcpy(pCSal->Seller1, pSale->Grantor, SALE_SIZ_SELLER);

   // Grantee
   if (pSale->Grantee[0] > ' ')
      memcpy(pCSal->Name1, pSale->Grantee, SALE_SIZ_BUYER);

   pCSal->CRLF[0] = '\n';
   pCSal->CRLF[1] = 0;

   return 0;
}

int Mrn_ExtrSale(char *pSaleFile)
{
   char     *pTmp, acCSalRec[1024], acSaleRec[1024], acTmp[256];
   char     acOutFile[_MAX_PATH], acHistFile[256];
   int      iRet, iConfSale, iUpdateSale;
   long     lCnt=0;

   LogMsg0("Extract MRN sale history with %s", pSaleFile);

   // Check current sale file
   if (_access(pSaleFile, 0))
   {
      LogMsg("***** Missing current sale file %s", pSaleFile);
      return -1;
   }

   // Open current sale
   LogMsg("Open current sale file %s", pSaleFile);
   fdSale = fopen(pSaleFile, "r");
   if (fdSale == NULL)
   {
      LogMsg("***** Error opening current sale file: %s\n", pSaleFile);
      return -2;
   }
   // Skip sale header
   MRN_SALE *pSale = (MRN_SALE *)&acSaleRec[0];
   do
   {
      pTmp = fgets(acSaleRec, 1024, fdSale);
   } while (pTmp && (!isdigit(pSale->DocNum[0]) || !isdigit(pSale->RecDate[0])));
   if (!pTmp)
   {
      LogMsg("***** Bad sale file: %s.  Please check input\n", pSaleFile);
      return -2;
   }


   // Create output file
   sprintf(acOutFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "dat");
   LogMsg("Create updated sale file %s", acOutFile);
   fdCSale = fopen(acOutFile, "w");
   if (fdCSale == NULL)
   {
      LogMsg("***** Error creating updated sale file: %s\n", acOutFile);
      return -2;
   }

   // Init counters
   iConfSale = 0;
   iUpdateSale = 0;
   lLastRecDate = 0;
   SCSAL_REC *pCSalRec = (SCSAL_REC *)&acCSalRec[0];

   // Merge loop
   while (!feof(fdSale))
   {
      // Format cumsale record, skip confidential sale records
      if (acSaleRec[0] != 'Y')
      {
         // Remove tab
         replChar(acSaleRec, 9, 0);

         // Format sale record
         iRet = Mrn_FormatSCSale(acCSalRec, acSaleRec);
         if (!iRet)
         {
            // Output current sale record
            fputs(acCSalRec, fdCSale);
            iUpdateSale++;
            iRet = atoin(pCSalRec->DocDate, 8);
            if (iRet > lLastRecDate)
               lLastRecDate = iRet;
         }
      } else
         iConfSale++;

      // Get current sale
      pTmp = fgets(acSaleRec, 1024, fdSale);
      if (!pTmp)
         break;

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdCSale)
      fclose(fdCSale);
   if (fdSale)
      fclose(fdSale);

   sprintf(acHistFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "srt");

   // Sort sale file - APN (asc), Saledate (asc), sale price (desc), DocNum (asc)
   sprintf(acTmp, "S(1,%d,C,A,27,8,C,A,57,10,C,D,15,%d,C,A) DUPO(B2000,1,34)", iApnLen, SALE_SIZ_DOCNUM);
   iRet = sortFile(acOutFile, acHistFile, acTmp);

   // Copy history file
   if (iRet > 0)
   {
      LogMsg("Total cumulative sale records:  %u", iRet);

      sprintf(acTmp, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "sav");
      sprintf(acCSalFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "sls");
      if (!_access(acTmp, 0))
         DeleteFile(acTmp);

      if (!_access(acCSalFile, 0))
         iRet = rename(acCSalFile, acTmp);
      else
         iRet = 0;

      if (!iRet)
      {
         iRet = rename(acHistFile, acCSalFile);
         if (iRet)
         {
            sprintf(acTmp, "***** Error renaming %s to %s: (%d)", acHistFile, acCSalFile, _errno);
            LogMsg(acTmp);
            perror(acTmp);
         }
      } else
      {
         sprintf(acTmp, "***** Error renaming %s to %s: (%d)", acCSalFile, acTmp, _errno);
         LogMsg(acTmp);
         perror(acTmp);
      }
   } else
   {
      // Something is wrong here
      LogMsg("***** Error loading sale file.  Please check %s.", pSaleFile);
      iRet = -1;
   }

   LogMsg("Total sale records processed:       %u", lCnt);
   LogMsg("Total confidential records skipped: %u", iConfSale);
   LogMsg("Total sale records updated:         %u", iUpdateSale);
   LogMsg("Last sale recording date:           %u", lLastRecDate);
   LogMsg("Update Sale History completed.");

   return iRet;
}

/******************************* Mrn_CreateLienRec ***************************
 *
 * Create lien record from fixed length format.
 *
 * Return 0 if successful, >0 if error
 *
 *****************************************************************************/

int Mrn_CreateLienRec1(char *pOutbuf, char *pRollRec)
{
   char     acTmp[256];
   int      iTmp;
   MRN_ROLL *pRec = (MRN_ROLL *)pRollRec;
   LIENEXTR *pLienRec = (LIENEXTR *)pOutbuf;
   ULONG    lTmp;

   // Clear output buffer
   memset(pOutbuf, ' ', sizeof(LIENEXTR));

   // Start copying data
   memcpy(pLienRec->acApn, pRec->Apn, RSIZ_APN);
   memcpy(pLienRec->acTRA, pRec->TRA, RSIZ_TRA);

#ifdef _DEBUG
   //if (!memcmp(pRec->Apn, "00110413", 8) )
   //   iTmp = 0;
#endif

   // Year assessed
   memcpy(pLienRec->acYear, myCounty.acYearAssd, 4);

   // Land
   long lLand = atoin(pRec->Prior_Land, RSIZ_LAND);
   if (lLand > 0)
   {
      iTmp = sprintf(acTmp, "%*u", sizeof(pLienRec->acLand), lLand);
      memcpy(pLienRec->acLand, acTmp, iTmp);
   }

   // Improve - Ratio
   long lImpr = atoin(pRec->Prior_Impr, RSIZ_IMPR);
   if (lImpr > 0)
   {
      iTmp = sprintf(acTmp, "%*u", sizeof(pLienRec->acImpr), lImpr);
      memcpy(pLienRec->acImpr, acTmp, iTmp);

      iTmp = sprintf(acTmp, "%*u", sizeof(pLienRec->acRatio), (LONGLONG)lImpr*100/(lLand+lImpr));
      memcpy(pLienRec->acRatio, acTmp, iTmp);
   }

   // PP value
   long lPers = atoin(pRec->Prior_PP_Val, RSIZ_PERSPROP);

   // Bus Inventory
   long lBusInv = atoin(pRec->Prior_Bus_Inv, RSIZ_BUS_INV);

   // Total other
   ULONG lOthers = lPers + lBusInv;
   if (lOthers > 0)
   {
      iTmp = sprintf(acTmp, "%*u", sizeof(pLienRec->acOther), lOthers);
      memcpy(pLienRec->acOther, acTmp, iTmp);

      if (lPers > 0)
      {
         iTmp = sprintf(acTmp, "%*u", sizeof(pLienRec->acPP_Val), lPers);
         memcpy(pLienRec->acPP_Val, acTmp, iTmp);
      }
      if (lBusInv > 0)
      {
         iTmp = sprintf(acTmp, "%*u", sizeof(pLienRec->extra.Mrn.Bus_Inv), lBusInv);
         memcpy(pLienRec->extra.Mrn.Bus_Inv, acTmp, iTmp);
      }
   }

   // Gross
   lTmp = lOthers + lLand + lImpr;
   if (lTmp > 0)
   {
      iTmp = sprintf(acTmp, "%*u", sizeof(pLienRec->acGross), lTmp);
      memcpy(pLienRec->acGross, acTmp, iTmp);
   }

   // HO Exe - Exemp total
   long lExe = atoin(pRec->Ho_Exe, RSIZ_HO_EXE);
   if (lExe > 0)
   {
      iTmp = sprintf(acTmp, "%*u", sizeof(pLienRec->acExAmt), lExe);
      memcpy(pLienRec->acExAmt, acTmp, iTmp);
      pLienRec->acHO[0] = '1';         // Y
   } else
      pLienRec->acHO[0] = '2';         // N

   lTmp = atoin(pRec->Exe_Amt2, RSIZ_EXE_AMT2);
   if (lTmp > 0)
   {
      iTmp = sprintf(acTmp, "%*u", sizeof(pLienRec->extra.Mrn.Exe_Amt2), lTmp);
      memcpy(pLienRec->extra.Mrn.Exe_Amt2, acTmp, iTmp);
      memcpy(pLienRec->extra.Mrn.Exe_Code2, pRec->Exe_Code2, SIZ_LIEN_EXECODE);
      lExe += lTmp;
   }
   lTmp = atoin(pRec->Exe_Amt3, RSIZ_EXE_AMT3);
   if (lTmp > 0)
   {
      iTmp = sprintf(acTmp, "%*u", sizeof(pLienRec->extra.Mrn.Exe_Amt3), lTmp);
      memcpy(pLienRec->extra.Mrn.Exe_Amt3, acTmp, iTmp);
      memcpy(pLienRec->extra.Mrn.Exe_Code3, pRec->Exe_Code3, SIZ_LIEN_EXECODE);
      lExe += lTmp;
   }
   lTmp = atoin(pRec->Exe_Amt4, RSIZ_EXE_AMT4);
   if (lTmp > 0)
   {
      iTmp = sprintf(acTmp, "%*u", sizeof(pLienRec->extra.Mrn.Exe_Amt4), lTmp);
      memcpy(pLienRec->extra.Mrn.Exe_Amt4, acTmp, iTmp);
      memcpy(pLienRec->extra.Mrn.Exe_Code4, pRec->Exe_Code4, SIZ_LIEN_EXECODE);
      lExe += lTmp;
   }
   if (lExe > 0)
   {
      iTmp = sprintf(acTmp, "%*u", sizeof(pLienRec->acExAmt), lExe);
      memcpy(pLienRec->acExAmt, acTmp, iTmp);
   }

   pLienRec->LF[0] = '\n';
   pLienRec->LF[1] = 0;
   return 0;
}

/******************************* Mrn_CreateLienRec ***************************
 *
 * Create lien record from delimited record.
 *
 * Return 0 if successful, >0 if error
 *
 *****************************************************************************/

int Mrn_CreateLienRec2(char *pOutbuf, char *pRollRec)
{
   char     acTmp[256];
   int      iTmp;
   LIENEXTR *pLienRec = (LIENEXTR *)pOutbuf;
   LONGLONG lTmp;

   // 2019
   //iTmp = ParseStringNQ(pRollRec, ',', MAX_FLD_TOKEN, apTokens);
   // 2020
   iTmp = ParseStringIQ(pRollRec, cLdrSep, MAX_FLD_TOKEN, apTokens);
   if (iTmp < MRN_L_ROLLYEAR)
   {
      LogMsg("***** Error: bad input record for APN=%s", apTokens[MRN_L_APN]);
      return -1;
   }

   // Clear output buffer
   memset(pOutbuf, ' ', sizeof(LIENEXTR));

   // Remove '-' in APN
   remChar(apTokens[MRN_L_APN], '-');
   memcpy(pLienRec->acApn, apTokens[MRN_L_APN], strlen(apTokens[MRN_L_APN]));
   memcpy(pLienRec->acTRA, apTokens[MRN_L_TRA], strlen(apTokens[MRN_L_TRA]));

#ifdef _DEBUG
   //if (!memcmp(pLienRec->acApn, "00102315", 8) )
   //   iTmp = 0;
#endif

   // Year assessed
   memcpy(pLienRec->acYear, myCounty.acYearAssd, 4);

   // Land
   dollar2Num(apTokens[MRN_L_LAND], acTmp);
   long lLand = atol(acTmp);
   if (lLand > 0)
   {
      iTmp = sprintf(acTmp, "%*u", sizeof(pLienRec->acLand), lLand);
      memcpy(pLienRec->acLand, acTmp, iTmp);
   }

   // Improve - Ratio
   dollar2Num(apTokens[MRN_L_IMPR], acTmp);
   long lImpr = atol(acTmp);
   if (lImpr > 0)
   {
      iTmp = sprintf(acTmp, "%*u", sizeof(pLienRec->acImpr), lImpr);
      memcpy(pLienRec->acImpr, acTmp, iTmp);

      iTmp = sprintf(acTmp, "%*u", sizeof(pLienRec->acRatio), (LONGLONG)lImpr*100/(lLand+lImpr));
      memcpy(pLienRec->acRatio, acTmp, iTmp);
   }

   // PP value
   dollar2Num(apTokens[MRN_L_PP_VALUE], acTmp);
   long lPers = atol(acTmp);

   // Bus Inventory
   dollar2Num(apTokens[MRN_L_BUS_VALUE], acTmp);
   long lBusInv = atol(acTmp);

   // Total other
   long lOthers = lPers + lBusInv;
   if (lOthers > 0)
   {
      iTmp = sprintf(acTmp, "%*u", sizeof(pLienRec->acOther), lOthers);
      memcpy(pLienRec->acOther, acTmp, iTmp);

      if (lPers > 0)
      {
         iTmp = sprintf(acTmp, "%*u", sizeof(pLienRec->acPP_Val), lPers);
         memcpy(pLienRec->acPP_Val, acTmp, iTmp);
      }
      if (lBusInv > 0)
      {
         iTmp = sprintf(acTmp, "%*u", sizeof(pLienRec->extra.Mrn.Bus_Inv), lBusInv);
         memcpy(pLienRec->extra.Mrn.Bus_Inv, acTmp, iTmp);
      }
   }

   // Gross
   lTmp = lOthers + lLand + lImpr;
   if (lTmp > 0)
   {
      iTmp = sprintf(acTmp, "%*u", sizeof(pLienRec->acGross), lTmp);
      memcpy(pLienRec->acGross, acTmp, iTmp);
   }

   // HO Exe - Exemp total
   pLienRec->acHO[0] = '2';               // N
   dollar2Num(apTokens[MRN_L_TOTAL_EXE], acTmp);
   long lExe = atol(acTmp);
   if (lExe > 0)
   {
      iTmp = sprintf(acTmp, "%*u", sizeof(pLienRec->acExAmt), lExe);
      memcpy(pLienRec->acExAmt, acTmp, iTmp);
      if (lExe == 7000)
         pLienRec->acHO[0] = '1';      // 'Y'
   }

   pLienRec->LF[0] = '\n';
   pLienRec->LF[1] = 0;
   return 0;
}

/********************************* Mrn_ExtrLien *****************************
 *
 *
 ****************************************************************************/

int Mrn_ExtrLien()
{
   char  acBuf[MAX_RECSIZE], acRollRec[MAX_RECSIZE], acOutFile[_MAX_PATH];
   char  *pTmp;
   FILE  *fdLien;
   long  lCnt=0;

   LogMsg0("Extract lien for MRN");

   // Open roll file
   LogMsg("Open Roll file %s", acRollFile);
   fdRoll = fopen(acRollFile, "r");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", acRollFile);
      return 2;
   }

   // Open Output file
   sprintf(acOutFile, acLienTmpl, myCounty.acCntyCode, myCounty.acCntyCode);
   LogMsg("Open output file %s", acOutFile);
   fdLien = fopen(acOutFile, "w");
   if (fdLien == NULL)
   {
      LogMsg("***** Error opening Lien extract file: %s\n", acOutFile);
      return 4;
   }

   // Skip header
   //pTmp = fgets(acRollRec, MAX_RECSIZE, fdRoll);

   // Merge loop
   while (!feof(fdRoll))
   {
      if (!(pTmp = fgets(acRollRec, MAX_RECSIZE, fdRoll) ))
         break;
      else if (acRollRec[1] > '9')
         continue;

      // Create new lien record
      Mrn_CreateLienRec2(acBuf, acRollRec);

      fputs(acBuf, fdLien);
      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdRoll)
      fclose(fdRoll);
   if (fdLien)
      fclose(fdLien);

   LogMsgD("Total records output:     %d\n", lCnt);
   lRecCnt = lCnt;
   return 0;
}

/****************************************************************************
 *
 * Convert MPA_LAND format to standard sale record
 *
 ****************************************************************************/

//int Mrn_ConvSale(char *pInfile)
//{
//   char     acInbuf[1024], acOutbuf[1024], *pRec;
//   char     acOutFile[_MAX_PATH], acTmp[256];
//   long     lCnt=0, lOut=0, iTmp;
//   FILE     *fdOut, *fdIn;
//
//   SCSAL_REC *pOutRec = (SCSAL_REC *)&acOutbuf[0];
//   MRN_CSAL  *pSale   = (MRN_CSAL  *)&acInbuf[0];
//
//   if (_access(pInfile, 0))
//   {
//      LogMsg("***** Mpa_ConvSale(): Missing input file: %s", pInfile);
//      return -1;
//   }
//
//   sprintf(acOutFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "sls");
//   LogMsg("Convert %s to %s.", pInfile, acOutFile);
//
//   // Open input file
//   LogMsg("Open input sale file %s", pInfile);
//   fdIn = fopen(pInfile, "r");
//   if (fdIn == NULL)
//   {
//      LogMsg("***** Error opening input sale file: %s\n", pInfile);
//      return -2;
//   }
//
//   // Open output file
//   LogMsg("Create output sale file %s", acOutFile);
//   fdOut = fopen(acOutFile, "w");
//   if (fdOut == NULL)
//   {
//      LogMsg("***** Error creating output sale file: %s\n", acOutFile);
//      return -3;
//   }
//
//   // Convert loop
//   while (!feof(fdIn))
//   {
//      if (!(pRec = fgets(acInbuf, 1024, fdIn)))
//         break;
//
//      // Reformat sale
//      memset(acOutbuf, ' ', sizeof(SCSAL_REC));
//
//      memcpy(pOutRec->Apn, pSale->Apn, iApnLen);
//      iTmp = atoin(pSale->SalePrice, SALE_SIZ_SALEPRICE);
//      if (iTmp > 0)
//      {
//         sprintf(acTmp, "%*u", SALE_SIZ_SALEPRICE, iTmp);
//         memcpy(pOutRec->SalePrice, acTmp, SALE_SIZ_SALEPRICE);
//      }
//
//      memcpy(pOutRec->DocDate, pSale->RecDate, SALE_SIZ_DOCDATE);
//      memcpy(pOutRec->DocNum, pSale->DocNum, SALE_SIZ_DOCNUM);
//
//      memcpy(pOutRec->Seller1, pSale->Grantor, SALE_SIZ_SELLER);
//      memcpy(pOutRec->Name1, pSale->Grantee, SALE_SIZ_BUYER);
//      memcpy(pOutRec->StampAmt, pSale->StampAmt, SSIZ_AMOUNT);
//
//      // Write to output file
//      pOutRec->CRLF[0] = '\n';
//      pOutRec->CRLF[1] = 0;
//      fputs(acOutbuf, fdOut);
//
//      if (!(++lCnt % 1000))
//         printf("\r%u", lCnt);
//   }
//
//   // Close files
//   if (fdIn)
//      fclose(fdIn);
//   if (fdOut)
//      fclose(fdOut);
//
//   LogMsg("Total sale records converted: %u", lOut);
//
//   return 0;
//}

/********************************* Mrn_ExtrVal ******************************
 *
 * Extract values from "Secured Equalized Roll.txt"
 *
 ****************************************************************************/

int Mrn_CreateValueRec(char *pOutbuf)
{
   char     acTmp[256];
   long     iTmp;
   LIENEXTR *pLienRec = (LIENEXTR *)pOutbuf;
   LONGLONG lTmp;

   // Clear output buffer
   memset(pOutbuf, ' ', sizeof(LIENEXTR));

   // Start copying data
   remChar(apTokens[MRN_ER_PROPERTY_ID], '-');
   vmemcpy(pLienRec->acApn, apTokens[MRN_ER_PROPERTY_ID], iApnLen);

   // Year assessed
   vmemcpy(pLienRec->acYear, apTokens[MRN_ER_ROLL_YR], 4);

   // Land
   remChar(apTokens[MRN_ER_LAND_VALUE], ',');
   long lLand = atol(apTokens[MRN_ER_LAND_VALUE]);
   if (lLand > 0)
   {
      iTmp = sprintf(acTmp, "%*u", SIZ_LIEN_LAND, lLand);
      memcpy(pLienRec->acLand, acTmp, iTmp);
   }

   // Improve
   remChar(apTokens[MRN_ER_IMP_VALUE], ',');
   long lImpr = atol(apTokens[MRN_ER_IMP_VALUE]);
   if (lImpr > 0)
   {
      iTmp = sprintf(acTmp, "%*u", SIZ_LIEN_IMPR, lImpr);
      memcpy(pLienRec->acImpr, acTmp, iTmp);
   }

   // Other values
   remChar(apTokens[MRN_ER_PERS_VALUE], ',');
   remChar(apTokens[MRN_ER_BUS_VALUE], ',');
   long lPers = atol(apTokens[MRN_ER_PERS_VALUE]);
   long lFixture = atol(apTokens[MRN_ER_BUS_VALUE]);
   lTmp = lPers + lFixture;
   if (lTmp > 0)
   {
      iTmp = sprintf(acTmp, "%*u", SIZ_LIEN_OTHERS, lTmp);
      memcpy(pLienRec->acOther, acTmp, iTmp);

      if (lPers > 0)
      {
         iTmp = sprintf(acTmp, "%*u", SIZ_LIEN_PERS, lPers);
         memcpy(pLienRec->acPP_Val, acTmp, iTmp);
      }

      if (lFixture > 0)
      {
         iTmp = sprintf(acTmp, "%*u", SIZ_LIEN_FIXT, lFixture);
         memcpy(pLienRec->acME_Val, acTmp, iTmp);
      }
   }

   // Gross total
   lTmp += (lLand+lImpr);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*u", SIZ_LIEN_FIXT, lTmp);
      memcpy(pLienRec->acGross, acTmp, SIZ_LIEN_FIXT);
   }

   // Ratio
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*u", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
      memcpy(pLienRec->acRatio, acTmp, SIZ_RATIO);
   }

   // HO Exempt
   remChar(apTokens[MRN_ER_TOT_EXE_VALUE], ',');
   long lExe = atol(apTokens[MRN_ER_TOT_EXE_VALUE]);
   if (lExe > 0)
   {
      if (lExe == 7000)
         pLienRec->acHO[0] = '1';   // 'Y'
      iTmp = sprintf(acTmp, "%*u", sizeof(pLienRec->acExAmt), lExe);
      memcpy(pLienRec->acExAmt, acTmp, iTmp);
   } else
      pLienRec->acHO[0] = '2';      // 'N'

   pLienRec->LF[0] = '\n';
   pLienRec->LF[1] = 0;
   return 0;
}

int Mrn_ExtrVal()
{
   char      *pTmp, acBuf[1024], acRec[MAX_RECSIZE], acTmpFile[_MAX_PATH];
   int       iRet;
   long      lOut=0, lCnt=0;
   FILE      *fdOut, *fdIn;

   LogMsg0("Extracting final values from equalized file.");

   // Open input file
   GetIniString(myCounty.acCntyCode, "EQRollFile", "", acTmpFile, _MAX_PATH, acIniFile);
   LogMsg("Open %s for input", acTmpFile);
   fdIn = fopen(acTmpFile, "r");
   if (fdIn == NULL)
   {
      LogMsg("***** Error opening input file: %s\n", acTmpFile);
      return -2;
   }  

   // Open out file
   sprintf(acTmpFile, acValueTmpl, myCounty.acCntyCode, myCounty.acCntyCode);
   LogMsg("Open file %s for output", acTmpFile);
   fdOut = fopen(acTmpFile, "w");
   if (fdOut == NULL)
   {
      LogMsg("***** Error creating output file: %s\n", acTmpFile);
      return -4;
   }

   // Start loop 
   while (!feof(fdIn))
   {
      pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdIn);
      if (!pTmp)
         break;

      if (*pTmp > '9')
         continue;

      // Parse input record
      iTokens = ParseStringIQ(acRec, cDelim, 256, apTokens);
      if (iTokens < MRN_ER_ROLL_YR)
      {
         LogMsg("*** Bad record: %s", apTokens[MRN_ER_PROPERTY_ID]);
         continue;
      }

      // Create new base record
      iRet = Mrn_CreateValueRec(acBuf);
      if (!iRet)
      {
         fputs(acBuf, fdOut);
         lOut++;
      } else
      {
         LogMsg("---> Drop record %d [APN=%s, TaxYear=%s]", lCnt, apTokens[MRN_ER_PROPERTY_ID], apTokens[MRN_ER_ROLL_YR]); 
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   LogMsg("Total records processed: %u", lCnt);
   LogMsg("Total records output:    %u", lOut);

   // Close files
   if (fdIn)
      fclose(fdIn);
   if (fdOut)
      fclose(fdOut);

   return iRet;
}

/*********************************** loadMrn ********************************
 *
 * Input files:
 *    - Mrn_Lien                 (lien file, 422-byte ebcdic)
 *    - Mrn_Roll                 (roll update file, 422-byte ebcdic)
 *    - Mrn_Char                 (char file, 250-byte ascii TAB-delimited)
 *    - Mrn_Sale                 (sale file, 791-byte ascii TAB-delimited)
 *    - Mrn_sale_newcum.srt      (290-byte, ascii)
 *
 * Following are tasks to be done here:
 *    - Convert ebcdic to ascii
 *    - Normal update: LoadOne -U -Us[i]
 *    - Load Lien: LoadOne -L -Us[i] -Xl
 *    - Use -Mo to merge other values
 *
 ****************************************************************************/

int loadMrn(int iSkip)
{
   int   iRet=0, iTmp;
   char  acTmpFile[_MAX_PATH];
   char  acTmp[256];

   iApnLen = myCounty.iApnLen;

   // Load tax 
   if (iLoadTax == TAX_LOADING)                    // -T
   {
      TC_SetDateFmt(MM_DD_YYYY_1);
      iRet = TC_LoadTax(myCounty.acCntyCode, bTaxImport);
   } 

   // Exit if load/update tax only
   if (!iLoadFlag && !lOptExtr)
      return iRet;

   // Extract final value
   if (lOptExtr & EXTR_FVAL)                       // -Xf
      iRet = Mrn_ExtrVal();

   // Load tables
   //iRet = LoadLUTable((char *)&acLookupTbl[0], "[Quality]", NULL, MAX_ATTR_ENTRIES);
   //if (!iRet)
   //{
   //   LogMsg("*** Error Looking for table [Quality] in %s", acLookupTbl);
   //   return 1;
   //} else
   //   iRet = 0;

 	// Load char file
   if (iLoadFlag & EXTR_ATTR)                      // -Xa
   {
      // Load residential char
      iRet = Mrn_ConvStdChar(acCharFile);
   }

   if (iLoadFlag & (EXTR_SALE|UPDT_SALE))          // -Xs or -Us
   {
      // Input:  Mrn_Sale
      // Output: Mrn_Sale.dat and Mrn_Sale.sls
      iRet = Mrn_ExtrSale(acSaleFile);             // Create/update sale history file
      if (iRet == -1)
      {
         // Delay 5 mins then try again
         LogMsg("... Delay 5 mins then try again ...");
         Sleep(5*60000);
         iRet = Mrn_ExtrSale(acSaleFile);          // Create/update sale history file
      }

      if (iRet)
         iLoadFlag = 0;                            // Stop all operation
   }
   
   // Translate to ascii
   //if (iLoadFlag & (LOAD_LIEN|LOAD_UPDT|EXTR_LIEN))
   if (iLoadFlag & LOAD_UPDT)
   {
      sprintf(acTmpFile, "%s\\MRN\\Mrn_Roll.srt", acTmpPath);
      if (!strstr(acRollFile, "SYS004"))
      {
         // Translate Roll file ebcdic to ascii
         LogMsg("Translate %s to Ascii %s then sort by APN", acRollFile, acTmpFile);
         sprintf(acTmp, "S(1,%d,C,A) F(FIX,%d) OUTREC(%%ETOA,1,%d,CRLF)  ALT(C:\\TOOLS\\EBCDIC.ALT)", iApnLen, iRollLen, iRollLen);
      } else
      {
         LogMsg("Sort roll file %s to %s by APN", acRollFile, acTmpFile);
         sprintf(acTmp, "S(1,%d,C,A) F(TXT) ", iApnLen);
      }

      iTmp = sortFile(acRollFile, acTmpFile, acTmp);
      if (iTmp <= 0)
      {
         iLoadFlag = 0;
         iRet = -1;
         LogMsg("***** ERROR sorting roll file %s to %s", acRollFile, acTmpFile);
      }
      strcpy(acRollFile, acTmpFile);
   }

   // Extract lien
   if (iLoadFlag & EXTR_LIEN)                      // -Xl
      iRet = Mrn_ExtrLien();

   // Merge roll file
   if (iLoadFlag & LOAD_LIEN)                      // -L
   {
      iRet = Mrn_Load_LDR(iSkip);
   } else if (iLoadFlag & LOAD_UPDT)               // -U
   {
      //iRet = Mrn_UpdatePQ4(iSkip);
      iRet = Mrn_Load_Roll(iSkip);
   }

   // This is to fix Lien data - use only if needed to correct lien value
   if (!iRet && bMergeOthers)
   {
      sprintf(acTmpFile, acLienTmpl, myCounty.acCntyCode, myCounty.acCntyCode);
      if (!_access(acTmpFile, 0))
         iRet = PQ_MergeLienExt(myCounty.acCntyCode, GRP_MRN, iSkip);
         //iRet = PQ_MergeOthers(myCounty.acCntyCode, acTmpFile, GRP_MRN, iSkip);
      else
         LogMsg("***** Lien extract is missing: %s.  Please locate the file and retry", acTmpFile);
   }

   return iRet;
}