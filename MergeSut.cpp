/******************************************************************************
 *
 * Input files:
 *    - SutRoll  (Roll file, 699-byte EBCDIC  with CRLF)
 *    - SutPropChar.txt (Char file, 1183-byte ASCII with CRLF)
 *
 * Operation commands:
 *    - Normal update:  LoadOne -CSUT -U [-Xs|-Mn]
 *    - Load Lien:      LoadOne -CSUT -L -Xl [-X8] [-Mn]
 *
 * Notes:
 *    - APN length is varied from 8 to 10 characters. For situs, use 8 for matching.
 *
 * Revision:
 * 06/13/2006 TCB
 * 10/03/2006 SPN       Format situs addr even without strNum.
 * 03/21/2008 1.5.7.1   Use standard function to update usecode.
 * 08/04/2008 8.0.7     Add Sut_ExtrLien()
 * 08/07/2008 8.1.0     Fix Sut_ExtrLien(), reorg loadSut(), rename CreateSutRoll()
 *                      to Sut_Load_LDR() and MergeSutRoll() to Sut_LoadRoll().
 * 01/17/2009 8.5.6     Fix TRA
 * 06/10/2009 8.8.3.1   Add Characteristics.  Add Prop8 flag.
 * 07/30/2009 9.1.4     Modify Sut_MergeRoll() & Sut_CreateLienRec(). Merge CHAR
 *                      in Sut_Load_LDR().
 * 09/26/2009 9.2.2     New format, use the last 7-bytes of UseCode. Ignore the first
 *                      portion of UseCode.
 * 10/01/2009 9.2.3     Fix HBath problem.
 * 01/22/2009 9.3.8.3   Add function Sut_ExtrProp8() to extract Prop8.
 * 02/23/2010 9.4.3     Prevent CARE_OF size change.
 * 03/01/2010 9.5.1     Fix MergeChar() problem that didn't remove old value when new one is blank.
 * 07/29/2011 11.0.5    Add S_HSENO.  Update Sut_HeatCool[].
 * 08/25/2011 11.2.10   Prepare code to blank out owner name where indicator code is 'K'.
 * 08/01/2012 12.2.4    Fix bug in Sut_CreateLienRec() by copying APN instead of Asmt_No.
 *                      Modify Sut_MergeRoll() to ignore book 90 and above.
 * 08/08/2012           Undo check for book 90 and above in Sut_MergeRoll().
 * 09/19/2012 12.2.8    Fix Sut_MergeOwner() to keep Owner1 the same as county provided.
 * 09/20/2013 13.8.2    Fix bug in Sut_LoadRoll() when input record has no APN.
 * 10/15/2013 13.11.2   Use updateVesting() to update Vesting and Etal flag.
 * 07/23/2014 14.1.2    Modify Sut_MergeSAdr() to apply city & zip from GIS file.
 * 06/21/2016 15.9.8    Modify Sut_MergeOwner() to ignore '/' at the end of owner name
 * 09/01/2016 16.2.4    Add option to load Tax Collector data.
 * 07/10/2017 17.1.1    Populate state in Sut_MergeSAdr() when populate city via zipcode.
 * 09/22/2017 17.2.5    Modify Sut_ParseTaxBase() to update TotalTaxDue and BillStatus
 *                      Remove Delq record due to incomplete data.
 * 10/28/2017 17.4.3    Add NR_CreateSCSale() to convert NDC sale extract to SCSAL_REC format.
 * 11/15/2017 17.4.6    Add option to unzip tax files to TC folder
 * 11/21/2017 17.5.0    Modify Sut_ParseTaxBase() to set Delq tax year.
 * 01/11/2018 17.6.1    Fix DelqYear in Sut_ParseTaxBase()
 * 03/23/2018 17.6.9    Remove automatic sale update from loadSut().  To update sale, use -Ms.
 * 05/22/2018 17.11.0   Modify -Xs option to use template ASH_File defined in INI for output sale file.
 * 08/09/2018 18.3.0    Add -Mn to merge sale from NDC.
 * 10/23/2018 18.5.9    Modify Sut_ParseTaxDetail() to ignore zero amt tax.
 * 07/08/2019 19.0.2    Add special case to Sut_MergeOwner().  Modify Sut_MergeChar() to add
 *                      all other building Sqft fields to MISCIMPR_SF. Add Sut_ConvStdChar().
 * 08/20/2019 19.0.7    Modify Sut_ParseTaxBase() to populate Paid Amt when Status is 'P'
 * 04/02/2020 19.8.7    Remove -Xs and replace it with -Xn option to format and merge NDC recorder data.
 * 07/01/2020 20.0.1    Modify Sut_MergeRoll() to add exemption code to EXE_CD1
 * 07/18/2020 20.1.6    Add Sut_ParseAdr() to fix situs issue that breaks street name incorrectly.  
 * 10/30/2020 20.4.1    Modify Sut_MergeChar() to populate PQZoning.
 * 05/07/2021 20.7.18   Set FirePlace='M' when greater than 9 in Sut_MergeChar() & Sut_FmtChar().
 * 05/17/2021 20.7.19   Tax file "Secured Assessment Detail(TCTU105)ASCII.TXT" has new layout.
 *                      Modify Sut_ParseTaxDetail() to use tax rate from input tax file.
 *
 ******************************************************************************/

#include "stdafx.h"
#include "CountyInfo.h"
#include "hlAdo.h"
#include "Prodlib.h"
#include "R01.h"
#include "RecDef.h"
#include "Tables.h"
#include "Logs.h"
#include "LoadDef.h"
#include "Utils.h"
#include "XlatTbls.h"
#include "doSort.h"
#include "doOwner.h"
#include "doZip.h"
#include "formatApn.h"
#include "SaleRec.h"
#include "LoadOne.h"
#include "Usecode.h"
#include "LOExtrn.h"
#include "PQ.h"
#include "Situs.h"
#include "MergeSut.h"
#include "Tax.h"
#include "NdcExtr.h"
#include "CharRec.h"

IDX_TBL  Sut_HeatCool[] =
{
   "HVAC"         ,"YU",
   "CENTRAL HEAT" ,"Z ",
   "WALL HEAT"    ,"D ",
   "FLOOR HEAT"   ,"C ",
   "RADIANT HEAT" ,"I ",
   "RADIENT HEAT" ,"I ",
   "ZONE HEAT"    ,"T ",
   "UNKNOWN"      ,"  ",
   "",""
};

XLAT_CODE  Sut_Water[] =
{
   "CITY","B",4,
   "PRIV","C",4,
   "PUB" ,"P",3,
   "WELL","W",4,
   "Y"   ,"A",1,
   "","",0
};
XLAT_CODE  Sut_Sewer[] =
{
   "CITY","B",4,
   "PRIV","I",4,
   "PUB" ,"P",3,
   "SEP" ,"S",3,
   "Y"   ,"Y",1,
   "","",0
};

static   FILE  *fdRoll, *fdChar;
static   long  lTaxSkip, lTaxMatch, lCharSkip, lCharMatch, lUseMailCity, lUseGis;

/******************************** Sut_MergeOwner *****************************
 *
 * Return 0 if successful, >0 if error
 *
 *****************************************************************************/

void Sut_MergeOwner(char *pOutbuf, char *pRollRec)
{
   int   iTmp, iTmp1;
   char  acTmp[128];
   char  acName1[64];
   char  *pTmp, *pTmp1;

   OWNER    myOwner;
   SUT_ROLL *pRec;

   pRec = (SUT_ROLL *)pRollRec;

   // Clear output buffer if needed
   removeNames(pOutbuf, false);

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "20093004", 8) )
   //   iTmp = 0;
#endif

   memcpy(acName1, pRec->Name_1, SUT_SIZ_NAME_1);
   blankRem(acName1, SUT_SIZ_NAME_1);

   // Remove multiple spaces
   pTmp = acName1;
   iTmp = iTmp1 = 0;
   while (*pTmp)
   {
      // Mark name with numeric value
      if (isdigit(*pTmp))
         iTmp1++;

      acTmp[iTmp++] = *pTmp;
      // Remove multi-space
      if (*pTmp == ' ')
         while (*pTmp == ' ') pTmp++;
      else
         pTmp++;

      // Remove '.' too
      if (*pTmp == '.')
         pTmp++;
   }
   // Keep a space at the end so we can check for " TR "
   //if (acTmp[iTmp-1] == ' ')
   //   iTmp -= 1;
   acTmp[iTmp] = 0;

   // Fix special case 
   if (!memcmp(&acTmp[iTmp-4], "ETAL", 4) && acTmp[iTmp-5] > ' ')
   {
      acTmp[iTmp-4] = ' ';
      strcpy(&acTmp[iTmp-3], "ETAL");
   }

   if (pTmp = strchr(acTmp, '/'))
   {
      *pTmp++ = 0;
      if (memcmp(pTmp, " ETAL", 5) && *pTmp >= 'A')
      {
         sprintf(acName1, "%s & %s", acTmp, pTmp);
         strcpy(acTmp, acName1);
      } else if (strstr(pTmp, " ETAL"))
         strcpy(pTmp-1, pTmp);
   }

   // Save name1
   vmemcpy(pOutbuf+OFF_NAME1, acTmp, SIZ_NAME1);
   strcpy(acName1, acTmp);

   // Update vesting
   updateVesting(myCounty.acCntyCode, acTmp, pOutbuf+OFF_VEST, pOutbuf+OFF_ETAL_FLG);

   // Drop everything from these words
   if ((pTmp=strstr(acTmp, " ETAL")) || (pTmp=strstr(acTmp, " ET AL")) )
      *pTmp = 0;

   if ((pTmp=strstr(acTmp, " TRETAL")) || (pTmp=strstr(acTmp, " TR ET")))
      *(pTmp+3) = 0;

   if (pTmp = strchr(acTmp, '('))
      *pTmp = 0;

   // Leave name as is if number present
   if (iTmp1 > 0)
   {
      vmemcpy(pOutbuf+OFF_NAME_SWAP, acTmp, SIZ_NAME_SWAP);
      return;
   }

   if ((pTmp=strstr(acTmp, " CO-TR"))  || (pTmp=strstr(acTmp, " LIFE EST")) ||
       (pTmp=strstr(acTmp, " EST OF")) || (pTmp=strstr(acTmp, " ESTATE OF")) )
      *pTmp = 0;

   // Save data to append later
   if (pTmp1=strstr(acTmp, "TRUST"))
   {
      if ((pTmp=strstr(acTmp, " FAM"))   || (pTmp=strstr(acTmp, " REVOC")) ||
          (pTmp=strstr(acTmp, " INDIV")) || (pTmp=strstr(acTmp, " AMENDED ")) ||
          (pTmp=strstr(acTmp, " LIV")) )
         *pTmp = 0;
      else
         *pTmp1 = 0;
   }

   if ((pTmp=strstr(acTmp, " FAM RV TR"))  || (pTmp=strstr(acTmp, " REV TR")) ||
       (pTmp=strstr(acTmp, " RV TR"))      || (pTmp=strstr(acTmp, " FAM LIV TR")) ||
       (pTmp=strstr(acTmp, " FAM TR"))     || (pTmp=strstr(acTmp, " LIV TR")) ||
       (pTmp=strstr(acTmp, " RD LAND TR")) || (pTmp=strstr(acTmp, " MARITAL TR")) ||
       (pTmp=strstr(acTmp, " LAND TR"))    || (pTmp=strstr(acTmp, " DECEDENTS TR ET")) )
      *pTmp = 0;

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "08110048", 8) )
   //   iTmp = 0;
#endif

   // Now parse owners
   acTmp[63] = 0;
   iTmp = splitOwner(acTmp, &myOwner, 0);
   if (iTmp >= 0)
      vmemcpy(pOutbuf+OFF_NAME_SWAP, myOwner.acSwapName, SIZ_NAME_SWAP);
   else
      vmemcpy(pOutbuf+OFF_NAME_SWAP, acName1, SIZ_NAME_SWAP);
}

/******************************** parseAdr1_2() *****************************
 *
 * Parse address line 1 into individual elements.  If no street number, parse
 * street name and suffix.  Suffix will be coded
 *
 ****************************************************************************/

void Sut_ParseAdr(ADR_REC *pAdrRec, char *pAdr)
{
   char  acAdr[128], acStr[64], acTmp[32], *apItems[16], *pTmp;
   int   iCnt, iTmp, iIdx, iSfxCode;
   bool  bRet;

   // Clear output buffer
   memset((void *)pAdrRec, 0, sizeof(ADR_REC));

   // Check for PO Box
   if (!memcmp(pAdr, "PO BOX", 6) || !memcmp(pAdr, "P.O. BOX", 8))
   {
      strcpy(pAdrRec->strName, pAdr);
      return;
   }

   strcpy(acAdr, pAdr);
   iCnt = ParseString(acAdr, ' ', 16, apItems);
   if (iCnt < 2)
   {
      strcpy(pAdrRec->strName, pAdr);
      pAdrRec->lStrNum = 0;
      return;
   }

   // If strNum is 0, do not parse address
   iIdx = 0;
   if (isdigit(*apItems[0]))
   {
      iIdx++;
      iTmp = atoi(apItems[0]);
      pAdrRec->lStrNum = iTmp;

      if ((pTmp = strchr(apItems[0], '-')) || (pTmp = strchr(apItems[iIdx], '/')))
      {
         strcpy(pAdrRec->strSub, pTmp+1);
         *pTmp = 0;
      }
      strcpy(pAdrRec->strNum, apItems[0]);
   }

   // Dir
   iTmp = 0;
   while (iTmp <= 7)
   {
      if (!strcmp(apItems[iIdx], asDir[iTmp]))
         break;
      iTmp++;
   }

   // Possible cases E ST or W RD
   // Check for suffix
   bRet = false;
   acStr[0] = 0;
   if (iTmp < 7 && iCnt > 2)
   {
      // If next item is suffix, then this one is not direction
      iSfxCode = GetSfxCode(apItems[iIdx+1]);
      if (iSfxCode)
      {
         strcpy(acStr, apItems[iIdx]);
         sprintf(pAdrRec->strSfx, "%d", iSfxCode);
         strcpy(pAdrRec->SfxName, apItems[iIdx+1]);
         bRet = true;         // Done
      } else
      {
         // This is direction
         strcpy(pAdrRec->strDir, apItems[iIdx]);
         iIdx++;
      }
   }

   if (!bRet)
   {
      // Not direction, token is str name
      acTmp[0] = 0;
      iSfxCode = GetSfxCode(apItems[iCnt-1]);
      if (iSfxCode)
      {
         // Multi-word street name with suffix
         sprintf(pAdrRec->strSfx, "%d", iSfxCode);
         strcpy(pAdrRec->SfxName, apItems[iCnt-1]);
         while (iIdx < iCnt-1)
         {
            strcat(acStr, apItems[iIdx++]);
            if (iIdx < iCnt)
               strcat(acStr, " ");
         }
      } else
      {
         // Multi-word street name without suffix
         while (iIdx < iCnt)
         {
            strcat(acStr, apItems[iIdx++]);
            if (iIdx < iCnt)
               strcat(acStr, " ");
         }
      }
   }

   acStr[SIZ_M_STREET] = 0;
   strcpy(pAdrRec->strName, acStr);
}

/********************************* Sut_MergeSAdr *****************************
 *
 * Return 0 if successful, >0 if error
 *
 *****************************************************************************/

void Sut_MergeSAdr(char *pOutbuf, char *pRollRec)
{
   SUT_ROLL *pRec;
   char     acTmp[256], acAddr1[128], acCode[64], acZip[16], acCity[64];

   int      iTmp, iStrNo;
   ADR_REC  sSitusAdr;

   pRec = (SUT_ROLL *)pRollRec;

   // Situs
   removeSitus(pOutbuf);
   acAddr1[0] = 0;
   acCode[0] = 0;
   pRec->Apn[iApnLen] = 0;
   memset((void *)&sSitusAdr, 0, sizeof(ADR_REC));

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "28010016", 8))
   //   iTmp = 0;
#endif

   iStrNo = atoin(pRec->S_StrNum, SUT_SIZ_SSTREET_NO);
   if (iStrNo > 0)
   {
      iTmp = sprintf(acAddr1, "%d ", iStrNo);
      memcpy(pOutbuf+OFF_S_STRNUM, acAddr1, iTmp);
      memcpy(pOutbuf+OFF_S_HSENO, acAddr1, iTmp);
   }

   memcpy(acTmp, pRec->S_StrName, SUT_SIZ_SSTREET);
   blankRem(acTmp, SUT_SIZ_SSTREET);
   strcat(acAddr1, acTmp);

   // Parse StrName into StrName, Dir, Suffix, and Unit#
   Sut_ParseAdr(&sSitusAdr, acTmp);

   if (sSitusAdr.strName[0] > ' ')
   {
      blankPad(sSitusAdr.strName, SIZ_S_STREET);
      memcpy(pOutbuf+OFF_S_STREET, sSitusAdr.strName, SIZ_S_STREET);
   }
   if (sSitusAdr.strDir[0] > ' ')
      vmemcpy(pOutbuf+OFF_S_DIR, sSitusAdr.strDir, SIZ_S_DIR);
   if (sSitusAdr.strSfx[0] > ' ')
      vmemcpy(pOutbuf+OFF_S_SUFF, sSitusAdr.strSfx, SIZ_S_SUFF);
   if (sSitusAdr.Unit[0] > ' ')
      vmemcpy(pOutbuf+OFF_S_UNITNO, sSitusAdr.Unit, SIZ_S_UNITNO);

   if (iStrNo > 0)
      vmemcpy(pOutbuf+OFF_S_ADDR_D, acAddr1, SIZ_S_ADDR_D);

   // Find city name
   if (!memcmp(pOutbuf+OFF_S_STRNUM, pOutbuf+OFF_M_STRNUM, SIZ_M_STRNUM) &&
       !memcmp(pOutbuf+OFF_S_STREET, pOutbuf+OFF_M_STREET, SIZ_M_STREET) &&
       *(pOutbuf+OFF_M_ZIP) == '9')
   {
      memcpy(acCity, pOutbuf+OFF_M_CITY, SIZ_M_CITY);
      myTrim(acCity, SIZ_M_CITY);

      // Encode city
      if (acCity[0] >= 'A')
      {
         City2Code(acCity, acCode, pRec->Apn);
         if (acCode[0] > ' ')
         {
            memcpy(pOutbuf+OFF_S_CITY, acCode, strlen(acCode));
            memcpy(pOutbuf+OFF_S_ST, "CA",2);
            strcat(acCity, " CA");
            vmemcpy(pOutbuf+OFF_S_CTY_ST_D, acCity, SIZ_S_CTY_ST_D);
            memcpy(pOutbuf+OFF_S_ZIP, pOutbuf+OFF_M_ZIP, SIZ_S_ZIP);
            lUseMailCity++;
         }
      }
   } 
   if (fdCity && acCode[0] <= ' ')
   {
      iTmp = getCityZip(pOutbuf, acCity, acZip, 8);
      if (!iTmp)
      {
         City2Code(acCity, acCode, pOutbuf);
         memcpy(pOutbuf+OFF_S_CITY, acCode, strlen(acCode));
         memcpy(pOutbuf+OFF_S_ST, "CA",2);
         memcpy(pOutbuf+OFF_S_ZIP, acZip, strlen(acZip));
         strcat(acCity, " CA");
         vmemcpy(pOutbuf+OFF_S_CTY_ST_D, acCity, SIZ_S_CTY_ST_D);  
         lUseGis++;
      }
   }
}

/********************************** Sut_MergeMAdr ****************************
 *
 * Return 0 if successful, >0 if error
 *
 *****************************************************************************/

void Sut_MergeMAdr(char *pOutbuf, char *pRollRec)
{
   SUT_ROLL  *pRec;

   char        *pAddr1, acTmp[256], acAddr1[64];
   int         iTmp;
   bool        bNoMail = false;

   ADR_REC  sMailAdr;

   pRec = (SUT_ROLL *)pRollRec;

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "003230241", 9) )
   //   lTmp = 0;
#endif

   // Clear old Mailing
   removeMailing(pOutbuf, false);

   // Check for blank address
   if (memcmp(pRec->M_Street, "     ", 5))
   {
      memcpy(acAddr1, pRec->M_Street, SUT_SIZ_MSTREET);
      pAddr1 = myTrim(acAddr1, SUT_SIZ_MSTREET);

      // Parsing mail address
      memset((void *)&sMailAdr, 0, sizeof(ADR_REC));
      parseMAdr1(&sMailAdr, acAddr1);

      if (sMailAdr.lStrNum > 0)
      {
         sprintf(acTmp, "%d      ", sMailAdr.lStrNum);
         memcpy(pOutbuf+OFF_M_STRNUM, acTmp, SIZ_M_STRNUM);
         if (sMailAdr.strSub[0] > '0')
            vmemcpy(pOutbuf+OFF_M_STR_SUB, sMailAdr.strSub, SIZ_M_STR_SUB);
      }
      vmemcpy(pOutbuf+OFF_M_DIR, sMailAdr.strDir, SIZ_M_DIR);
      vmemcpy(pOutbuf+OFF_M_STREET, sMailAdr.strName, SIZ_M_STREET);
      vmemcpy(pOutbuf+OFF_M_SUFF, sMailAdr.strSfx, SIZ_M_SUFF);
      if (sMailAdr.Unit[0] > ' ')
      {
         if (sMailAdr.Unit[0] == '#')
            vmemcpy(pOutbuf+OFF_M_UNITNO, sMailAdr.Unit, SIZ_M_UNITNO);
         else
         {
            *(pOutbuf+OFF_M_UNITNO) = '#';
            vmemcpy(pOutbuf+OFF_M_UNITNO+1, sMailAdr.Unit, SIZ_M_UNITNO-1);
         }
      }

      vmemcpy(pOutbuf+OFF_M_ADDR_D, acAddr1, SIZ_M_ADDR_D);

      // Parse city-st
      memcpy(acTmp, pRec->M_CitySt, SUT_SIZ_MCITY_STATE);
      blankRem(acTmp, SUT_SIZ_MCITY_STATE);
      parseMAdr2(&sMailAdr, acTmp);

      if (sMailAdr.City[0] > ' ')
      {
         vmemcpy(pOutbuf+OFF_M_CITY, sMailAdr.City, SIZ_M_CITY);
         vmemcpy(pOutbuf+OFF_M_ST, sMailAdr.State, SIZ_M_ST);

         iTmp = sprintf(acTmp, "%s %s", sMailAdr.City, sMailAdr.State);
         vmemcpy(pOutbuf+OFF_M_CTY_ST_D, acTmp, SIZ_M_CTY_ST_D, iTmp);
      }

      iTmp = atoin(pRec->M_Zip, SUT_SIZ_MZIP);
      if (iTmp > 500)
         vmemcpy(pOutbuf+OFF_M_ZIP, pRec->M_Zip, SIZ_M_ZIP);
   }
}

/********************************* Sut_MergeChar *****************************
 *
 *
 *
 *****************************************************************************/

int Sut_MergeChar(char *pOutbuf)
{

   static   char acRec[1200], *pRec=NULL;
   char     acTmp[256], acCode[32];
   long     lTmp, lBldgSqft;
   int      iRet, iTmp, iLoop, iBeds, iFBath, iHBath;
   SUT_CHAR *pChar;

   // Get first Char rec for first call
   if (!pRec)
   {
      iRet = fread(acRec, 1, iCharLen, fdChar);
      pRec = acRec;
   }

   pChar = (SUT_CHAR *)&acRec[0];

   do
   {
      if (!pRec)
      {
         fclose(fdChar);
         fdChar = NULL;
         return 1;      // EOF
      }

      // Compare Apn
      iLoop = memcmp(pOutbuf, pChar->Apn, CSIZ_APN);
      if (iLoop > 0)
      {
         if (bDebug)
            LogMsg0("Skip Char rec  %.*s", CSIZ_APN, pChar->Apn);
         iRet = fread(acRec, 1, iCharLen, fdChar);
         if (iRet != iCharLen)
            pRec = NULL;
         lCharSkip++;
      }
   } while (iLoop > 0);

   // If not match, return
   if (iLoop)
      return 1;

   // YrBlt
   lTmp = atoin(pChar->YearBuilt, CSIZ_YRBLT);
   if (lTmp > 1700)
      memcpy(pOutbuf+OFF_YR_BLT, pChar->YearBuilt, SIZ_YR_BLT);

   // Lot sqft/Acres
   double dAcres = atof(pChar->Acres);
   if (dAcres > 0.0)
   {
      sprintf(acTmp, "%*d", SIZ_LOT_ACRES, (long)(dAcres*1000.0));
      memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);
      lTmp = (long)(dAcres * SQFT_PER_ACRE);
      sprintf(acTmp, "%*d", SIZ_LOT_SQFT, lTmp);
      memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);
   }

   // BldgSqft - roll file already supply this
   lBldgSqft = atoin(pChar->BldgSqft, CSIZ_PRI_SQFT);
   if (lBldgSqft > 50)
   {
      sprintf(acTmp, "%*d", SIZ_BLDG_SF, lBldgSqft);
      memcpy(pOutbuf+OFF_BLDG_SF, acTmp, SIZ_BLDG_SF);
   }

   // Misc impr
   lBldgSqft  = atoin(pChar->OthBldg_Sqft, CSIZ_OTH_BLDG_SQFT);
   lBldgSqft += atoin(pChar->OthRoom1_Sqft, CSIZ_OTH_ROOM1_SQFT);
   lBldgSqft += atoin(pChar->OthRoom2_Sqft, CSIZ_OTH_ROOM2_SQFT);
   if (lBldgSqft > 10 && lBldgSqft < 100000)
   {
      sprintf(acTmp, "%*d", SIZ_MISCIMPR_SF, lBldgSqft);
      memcpy(pOutbuf+OFF_MISCIMPR_SF, acTmp, SIZ_MISCIMPR_SF);
   }

   // Park spaces
   int iGarage = atoin(pChar->Garages, CSIZ_GARAGES);
   // Carport
   int iCarport = atoin(pChar->CarPorts, CSIZ_CARPORT);
   // Garage Sqft
   int lGarSqft = atoin(pChar->GarSqft, CSIZ_GAR_SQFT);
   // Carport Sqft
   int lCarSqft = atoin(pChar->CarSqft, CSIZ_GAR_SQFT);

   if (iGarage > 0 && iCarport > 0)
   {
      sprintf(acTmp, "%d      ", iGarage+iCarport);
      memcpy(pOutbuf+OFF_PARK_SPACE, acTmp, SIZ_PARK_SPACE);
      *(pOutbuf+OFF_PARK_TYPE) = '2';
   } else if (iGarage > 0)
   {
      sprintf(acTmp, "%d      ", iGarage);
      memcpy(pOutbuf+OFF_PARK_SPACE, acTmp, SIZ_PARK_SPACE);
      *(pOutbuf+OFF_PARK_TYPE) = 'Z';
   } else if (iCarport > 0)
   {
      sprintf(acTmp, "%d      ", iCarport);
      memcpy(pOutbuf+OFF_PARK_SPACE, acTmp, SIZ_PARK_SPACE);
      *(pOutbuf+OFF_PARK_TYPE) = 'C';
   } else
   {
      *(pOutbuf+OFF_PARK_TYPE) = ' ';
      memset(pOutbuf+OFF_PARK_SPACE, ' ', SIZ_PARK_SPACE);
   }

   lGarSqft += lCarSqft;
   if (lGarSqft > 100)
   {
      sprintf(acTmp, "%*d", SIZ_GAR_SQFT, lGarSqft);
      memcpy(pOutbuf+OFF_GAR_SQFT, acTmp, SIZ_GAR_SQFT);
   }

   // Beds
   iBeds = atoin(pChar->Beds, CSIZ_BEDS);
   if (iBeds > 0)
   {
      sprintf(acTmp, "%*d", SIZ_BEDS, iBeds);
      memcpy(pOutbuf+OFF_BEDS, acTmp, SIZ_BEDS);
   } else
      memset(pOutbuf+OFF_BEDS, ' ', SIZ_BEDS);

   // Baths
   iFBath = atoin(pChar->Baths, CSIZ_BATHS);
   if (iFBath > 0)
   {
      sprintf(acTmp, "%*d", SIZ_BATH_F, iFBath);
      memcpy(pOutbuf+OFF_BATH_F, acTmp, SIZ_BATH_F);
   } else
      memset(pOutbuf+OFF_BATH_F, ' ', SIZ_BATH_F);

   // Half bath
   iHBath = atoin(pChar->HBath, CSIZ_HBATH);
   if (iHBath > 0)
   {
      sprintf(acTmp, "%*d", SIZ_BATH_H, iHBath);
      memcpy(pOutbuf+OFF_BATH_H, acTmp, SIZ_BATH_H);
   } else
      memset(pOutbuf+OFF_BATH_H, ' ', SIZ_BATH_H);

   // Fireplace
   memset(pOutbuf+OFF_FIRE_PL, ' ', SIZ_FIRE_PL);
   iTmp = atoin(pChar->FirePlaces, CSIZ_FIREPLACES);
   if (iTmp > 9)
      *(pOutbuf+OFF_FIRE_PL) = 'M';
   else if (iTmp > 0)
      *(pOutbuf+OFF_FIRE_PL) = '0' | iTmp;
   else if (pChar->FirePlaces[0] > '0')
      LogMsg("*** Bad FirePlace %.*s [%.*s]", CSIZ_FIREPLACES, pChar->FirePlaces, iApnLen, pChar->Apn);

   // Pool/Spa
   if (pChar->Pool[0] > '0')
   {
      char *pPoolType = &pChar->Pool[2];

      if (!memcmp(pPoolType, "GUNITE", 6))
         *(pOutbuf+OFF_POOL) = 'G';
      else if (!memcmp(pPoolType, "VINYL", 5))
         *(pOutbuf+OFF_POOL) = 'V';
      else if (!memcmp(pPoolType, "FIBER", 5))
         *(pOutbuf+OFF_POOL) = 'F';
      else
         iTmp = 0;
   } else
      *(pOutbuf+OFF_POOL) = ' ';

   // Zoning
   if (pChar->Zoning[0] > ' ')
   {
      memcpy(acTmp, pChar->Zoning, CSIZ_ZONING);
      acTmp[CSIZ_ZONING] = 0;
      _strupr(acTmp);
      memcpy(pOutbuf+OFF_ZONE, acTmp, CSIZ_ZONING);
      if (*(pOutbuf+OFF_ZONE_X1) == ' ')
         vmemcpy(pOutbuf+OFF_ZONE_X1, acTmp, SIZ_ZONE_X1, CSIZ_ZONING);
   } else
      memset(pOutbuf+OFF_ZONE, ' ', CSIZ_ZONING);

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "06550040", 8))
   //   lTmp = 0;
#endif

   // Quality/class
   if (isalpha(pChar->Class1[0]))
   {  // D 05.5BAVERAGE
      *(pOutbuf+OFF_BLDG_CLASS) = pChar->Class1[0];
      iTmp = atoin(pChar->Class2, 2);
      sprintf(acTmp, "%d.%c", iTmp, pChar->Class2[3]);
      iRet = Value2Code(acTmp, acCode, NULL);
      blankPad(acCode, SIZ_BLDG_QUAL);
      memcpy(pOutbuf+OFF_BLDG_QUAL, acCode, SIZ_BLDG_QUAL);
   } else
   {
      memset(pOutbuf+OFF_BLDG_QUAL, ' ', SIZ_BLDG_QUAL);
      *(pOutbuf+OFF_BLDG_CLASS) = ' ';
   }

   // Condition
   if (pChar->Condition[0] > ' ')
   {
      switch (pChar->Condition[0])
      {
         case 'P':
            acCode[0] = 'P';    // Poor
            break;
         case 'F':
            acCode[0] = 'F';    // Fair
            break;
         case 'A':
            acCode[0] = 'A';    // Average
            break;
         case 'G':
            acCode[0] = 'G';    // Good
            break;
         default:
            acCode[0] = ' ';
      }
      *(pOutbuf+OFF_IMPR_COND) = acCode[0];
   } else
      *(pOutbuf+OFF_IMPR_COND) = ' ';

   // Stories
   iTmp = atoin(pChar->Stories, CSIZ_STORIES);
   if (iTmp > 0)
   {
      if (iTmp > 9 && bDebug)
         LogMsg("*** Stories: %d, APN= %.10s", iTmp, pOutbuf);
      sprintf(acTmp, "%d.0  ", iTmp);
      memcpy(pOutbuf+OFF_STORIES, acTmp, SIZ_STORIES);
   } else
      memset(pOutbuf+OFF_STORIES, ' ', SIZ_STORIES);

   // Units
   iTmp = atoin(pChar->Units, CSIZ_UNITS);
   if (iTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_UNITS, iTmp);
      memcpy(pOutbuf+OFF_UNITS, acTmp, SIZ_UNITS);
   } else
      memset(pOutbuf+OFF_UNITS, ' ', SIZ_UNITS);

   // Water
   if (pChar->Water[0] > ' ')
   {
      iTmp = 0;
      while (Sut_Water[iTmp].iLen > 0)
      {
         if (!memcmp(pChar->Water, Sut_Water[iTmp].acSrc, Sut_Water[iTmp].iLen))
         {
            *(pOutbuf+OFF_WATER) = Sut_Water[iTmp].acCode[0];
            break;
         }
         iTmp++;
      }
   } else
      *(pOutbuf+OFF_WATER) = ' ';

   // Sewer
   if (pChar->Sewer[0] > ' ')
   {
      iTmp = 0;
      while (Sut_Sewer[iTmp].iLen > 0)
      {
         if (!memcmp(pChar->Sewer, Sut_Sewer[iTmp].acSrc, Sut_Sewer[iTmp].iLen))
         {
            *(pOutbuf+OFF_SEWER) = Sut_Sewer[iTmp].acCode[0];
            break;
         }
         iTmp++;
      }
   } else
      *(pOutbuf+OFF_SEWER) = ' ';

   // Heat/Cool - need explaination?
   iTmp = 0;
   while (*Sut_HeatCool[iTmp].pName > ' ')
   {
      if (!memcmp(pChar->Heat_Cool, Sut_HeatCool[iTmp].pName, strlen(Sut_HeatCool[iTmp].pName)))
      {
         *(pOutbuf+OFF_HEAT)     = Sut_HeatCool[iTmp].pCode[0];
         *(pOutbuf+OFF_AIR_COND) = Sut_HeatCool[iTmp].pCode[1];
         break;
      }
      iTmp++;
   }
   if (bDebug && (pChar->Heat_Cool[0] > ' ' && *Sut_HeatCool[iTmp].pName <= ' '))
      LogMsg0("+++ Unknown Heat/Cool: %.*s [%.10s]", CSIZ_HEAT_COOL, pChar->Heat_Cool, pOutbuf);


   // View

   // Others


   lCharMatch++;

   // Get next Char rec
   iRet = fread(acRec, 1, iCharLen, fdChar);
   if (iRet != iCharLen)
      pRec = NULL;

   return 0;
}

/********************************** Sut_MergeRoll *****************************
 *
 * Return 0 if successful, >0 if error
 *
 *****************************************************************************/

int Sut_MergeRoll(char *pOutbuf, char *pRollRec, int iFlag)
{
   char     acTmp[256], acTmp1[256];
   char     acUnit[256], acBlock[256], acLot[256];
   int      iRet, iTmp;
   long     lTmp;

   SUT_ROLL *pRec;

   pRec = (SUT_ROLL *)pRollRec;
   // Skip book 90 and above
   //if (memcmp(pRec->Apn, "89", 2) > 0)
   //   return 1;

   if (iFlag & CREATE_R01)
   {
      // Clear output buffer
      memset(pOutbuf, ' ', iRecLen);

      // Start copying data
      memcpy(pOutbuf, pRec->Apn, SUT_SIZ_APN);

      // Format APN
      iRet = formatApn(pRec->Apn, acTmp, &myCounty);
      memcpy(pOutbuf+OFF_APN_D, acTmp, iRet);

      // Create MapLink and output new record
      iRet = formatMapLink(pRec->Apn, acTmp, &myCounty);
      memcpy(pOutbuf+OFF_MAPLINK, acTmp, iRet);

      // Create index map link
      if (getIndexPage(acTmp, acTmp1, &myCounty))
         memcpy(pOutbuf+OFF_IMAPLINK, acTmp1, iRet);

      memcpy(pOutbuf+OFF_CO_NUM, "51SUT", 5);

      // Year assessed
      memcpy(pOutbuf+OFF_YR_ASSD, myCounty.acYearAssd, 4);

      long lBldgSqft = atoin(pRec->Pri_Sqft, SUT_SIZ_PRI_SQFT);
      if (lBldgSqft > 0)
      {
         sprintf(acTmp, "%*d", SIZ_BLDG_SF, lBldgSqft);
         memcpy(pOutbuf+OFF_BLDG_SF, acTmp, SIZ_BLDG_SF);
      }

      // Land
      long lLand = atoin(pRec->Land,SUT_SIZ_LAND);
      if (lLand > 0)
      {
         sprintf(acTmp, "%*d", SIZ_LAND, lLand);
         memcpy(pOutbuf+OFF_LAND, acTmp, SIZ_LAND);
      }

      // Improve
      long lImpr = atoin(pRec->Structures, SUT_SIZ_STRUCTURES);
      if (lImpr > 0)
      {
         sprintf(acTmp, "%*d", SIZ_IMPR, lImpr);
         memcpy(pOutbuf+OFF_IMPR, acTmp, SIZ_IMPR);

         sprintf(acTmp, "%*d", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
         memcpy(pOutbuf+OFF_RATIO, acTmp, SIZ_RATIO);
      }

      long lPers = atoin(pRec->Pers_Prop, SUT_SIZ_PERS_PROP);
      long lFixt = atoin(pRec->Fixed_Equip, SUT_SIZ_FIXED_EQUIP);
      long lTree = atoin(pRec->Trees, SUT_SIZ_TREES);
      lTmp = lPers + lFixt + lTree;
      if (lTmp > 0)
      {
         sprintf(acTmp, "%*d", SIZ_OTHER, lTmp);
         memcpy(pOutbuf+OFF_OTHER, acTmp, SIZ_OTHER);

         if (lPers > 0)
         {
            sprintf(acTmp, "%d          ", lPers);
            memcpy(pOutbuf+OFF_PERSPROP, acTmp, SIZ_PERSPROP);
         }
         if (lFixt > 0)
         {
            sprintf(acTmp, "%d          ", lFixt);
            memcpy(pOutbuf+OFF_FIXTR, acTmp, SIZ_FIXTR);
         }
         if (lTree > 0)
         {
            sprintf(acTmp, "%d          ", lTree);
            memcpy(pOutbuf+OFF_OTH_IMPR, acTmp, SIZ_OTH_IMPR);
         }
      }

      // Gross total
      lTmp += lLand+lImpr;
      if (lTmp > 0)
      {
         sprintf(acTmp, "%*d", SIZ_GROSS, lTmp);
         memcpy(pOutbuf+OFF_GROSS, acTmp, SIZ_GROSS);
      }

      // HO Exempt
      lTmp = atoin(pRec->Homeowners_Exemp, SUT_SIZ_HOMEOWNERS_EXEMP);
      if (lTmp > 0)
         *(pOutbuf+OFF_HO_FL) = '1';      // 'Y'
      else
         *(pOutbuf+OFF_HO_FL) = '2';      // 'N'

      // Exemp total
      lTmp += atoin(pRec->Misc_Exemp, SUT_SIZ_MISC_EXEMP);
      if (lTmp > 0)
      {
         sprintf(acTmp, "%*d", SIZ_EXE_TOTAL, lTmp);
         memcpy(pOutbuf+OFF_EXE_TOTAL, acTmp, SIZ_EXE_TOTAL);
      }

      // Exemption code
      if (pRec->Exemp_Code[0] > ' ')
         memcpy(pOutbuf+OFF_EXE_CD1, pRec->Exemp_Code, SUT_SIZ_EXEMP_CODE);

   }

   // TRA
   lTmp = atoin(pRec->Tra, SUT_SIZ_TRA);
   if (lTmp > 0)
   {
      iRet = sprintf(acTmp, "%.6d", lTmp);
      memcpy(pOutbuf+OFF_TRA, acTmp, iRet);
   }

   // Status
   if (pRec->Status == 'U')
      *(pOutbuf+OFF_STATUS) = 'O';
   else if (pRec->Status == 'R')
      *(pOutbuf+OFF_STATUS) = 'I';
   else
      *(pOutbuf+OFF_STATUS) = 'A';

   // Prop 8
   if (pRec->Indicator_Codes[0] == 'P' || pRec->Indicator_Codes[1] == 'P' || pRec->Indicator_Codes[2] == 'P')
      *(pOutbuf+OFF_PROP8_FLG) = 'Y';

   // Usecode
   if (pRec->Use_Code[3] != ' ')
   {
      // Use the last 7-bytes of UseCode as county suggested
      memcpy(pOutbuf+OFF_USE_CO, &pRec->Use_Code[3], 7);
      iTmp = sprintf(acTmp1, "%.2s-%.2s", &pRec->Use_Code[3], &pRec->Use_Code[7]);
      updateStdUse(pOutbuf+OFF_USE_STD, acTmp1, iTmp, pOutbuf);
   } else
      memcpy(pOutbuf+OFF_USE_STD, USE_UNASGN, SIZ_USE_STD);

   // Legal
   acTmp1[0] = 0;
   if (pRec->Legal_Unit[0] > ' ')
   {
      memcpy(acUnit, pRec->Legal_Unit, SUT_SIZ_LEGAL_UNIT);
      myTrim(acUnit, SUT_SIZ_LEGAL_UNIT);
      strcpy(acTmp1, "UNIT ");
      strcat(acTmp1, acUnit);
      strcat(acTmp1, " ");
   }
   if (pRec->Legal_Block[0] > ' ')
   {
      memcpy(acBlock,pRec->Legal_Block, SUT_SIZ_LEGAL_BLOCK);
      myTrim(acBlock, SUT_SIZ_LEGAL_BLOCK);
      strcat(acTmp1, "BLOCK ");
      strcat(acTmp1, acBlock);
      strcat(acTmp1, " ");
   }
   if (pRec->Legal_Lot[0] > ' ')
   {
      memcpy(acLot, pRec->Legal_Lot, SUT_SIZ_LEGAL_LOT);
      myTrim(acLot, SUT_SIZ_LEGAL_LOT);
      strcat(acTmp1, "LOT ");
      strcat(acTmp1, acLot);
   }
   if (acTmp1[0] > ' ')
   {
      iTmp = updateLegal(pOutbuf, acTmp1);
      if (iTmp > iMaxLegal)
         iMaxLegal = iTmp;
   }

   long lSqFtLand=0;
   double dAcreAge = atofn(pRec->Acres, SUT_SIZ_ACRES);
   if (dAcreAge > 0.0)
   {
      sprintf(acTmp, "%*d", SIZ_LOT_ACRES, (long)(dAcreAge*10));
      memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);
      if (lSqFtLand == 0)
      {
         lSqFtLand = (long)((dAcreAge * SQFT_PER_ACRE) / 100);
         sprintf(acTmp, "%*d", SIZ_LOT_SQFT, lSqFtLand);
         memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);
      }
   }

   // CareOf
   if (pRec->Careof[0] > ' ')
      updateCareOf(pOutbuf, pRec->Careof, SUT_SIZ_CAREOF);

   // Transfer
   if (isValidYMD(pRec->Recording_Date))
      memcpy(pOutbuf+OFF_TRANSFER_DT, pRec->Recording_Date, SIZ_TRANSFER_DT);

   memcpy(acTmp, pRec->Recording_No, SUT_SIZ_RECORDING_NO);
   blankRem(acTmp, SUT_SIZ_RECORDING_NO);
   blankPad(acTmp, SIZ_TRANSFER_DOC);
   memcpy(pOutbuf+OFF_TRANSFER_DOC, acTmp, SIZ_TRANSFER_DOC);

   // Use Usecode to determine Pool/Spa
   if (pRec->Use_Code[0] > ' ')
   {
      // Pool/Spa
      iTmp = atoin(&pRec->Use_Code[7], 2);
      switch (iTmp)
      {
         case 50:
         case 52:
         case 54:
         case 59:
         case 60:
            *(pOutbuf+OFF_POOL) = 'P';      // Pool
            break;
         case 55:
            *(pOutbuf+OFF_POOL) = 'S';      // Spa
            break;
         default:
            *(pOutbuf+OFF_POOL) = ' ';
      }
   }

   // Do not populate county official name
   // POS 410=E,P,6,R,0,1,2,L
   //if (pRec->Indicator_Codes[2] == 'K')
   //{
   // memset(pOutbuf+OFF_NAME1, ' ', SIZ_NAME1+SIZ_NAME2);
   // memset(pOutbuf+OFF_NAME_SWAP, ' ', SIZ_NAME_SWAP);
   //} else
      Sut_MergeOwner(pOutbuf, pRollRec);

   Sut_MergeMAdr(pOutbuf, pRollRec);
   Sut_MergeSAdr(pOutbuf, pRollRec);

   return 0;
}

/********************************* Sut_Load_LDR ****************************
 *
 * Rename CreateSutRoll() to Sut_Load_LDR()
 *
 ****************************************************************************/

int Sut_Load_LDR(int iLoadFlag, int iFirstRec)
{
   char     acBuf[MAX_RECSIZE], acRollRec[MAX_RECSIZE];
   char     acOutFile[_MAX_PATH], acTmpFile[_MAX_PATH];

   HANDLE   fhOut;

   int      iRet;
   DWORD    nBytesWritten;
   BOOL     bRet, bEof;
   long     lRet=0, lCnt=0;

   // Open lien file
   LogMsg("Creating LDR roll for SUT");
   LogMsg("Opening roll file %s", acRollFile);
   fdRoll = fopen(acRollFile, "r");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", acRollFile);
      return -1;
   }

   // Open Char file
   LogMsg("Open Char file %s", acCharFile);
   fdChar = fopen(acCharFile, "rb");
   if (fdChar == NULL)
   {
      LogMsg("***** Error opening Char file: %s\n", acCharFile);
      return -2;
   }

   // Open CityZip file
   iRet = initSitus(acIniFile, myCounty.acCntyCode);
   if (iRet < 0)
      return -3;

   // Open Output file
   sprintf(acTmpFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "R01");
   LogMsg("Open output file %s", acTmpFile);
   fhOut = CreateFile(acTmpFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);
   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening output file: %s\n", acOutFile);
      return -4;
   }

   // Output first header record
   if (iFirstRec > 0)
   {
      memset(acBuf, '9', iRecLen);
      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
   }

   // Get first RollRec
   iRet = fread(acRollRec,  1, iRollLen, fdRoll);
   bEof = false;

   // Init variables
   iNoMatch=iBadCity=iBadSuffix=0;
   lUseMailCity=lUseGis=0;

   // Merge loop
   while (!bEof)
   {
#ifdef _DEBUG
   //SUT_ROLL *pRec = (SUT_ROLL *)acRollRec;
   //if (!memcmp(pRec->Apn, "25140021", 8))
   //   iRet = 0;
#endif
      iRet = Sut_MergeRoll(acBuf, acRollRec, CREATE_R01|CLEAR_R01);
      if (!iRet)
      {
         if (fdChar)
            iRet = Sut_MergeChar(acBuf);

         // Save last recording date
         lRet = atoin((char *)&acBuf[OFF_TRANSFER_DT], 8);
         if (lRet > lLastRecDate && lRet < lToday)
            lLastRecDate = lRet;

         lLDRRecCount++;
         bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
         if (!bRet)
         {
            LogMsg("***** Error writing to output file at record %d\n", lCnt);
            lRet = WRITE_ERR;
            break;
         }
      }

      iRet = fread(acRollRec,  1, iRollLen, fdRoll);

      if (!iRet)
         bEof = true;
      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdRoll)
      fclose(fdRoll);
   if (fdChar)
      fclose(fdChar);
   if (fdCity)
      fclose(fdCity);
   if (fhOut)
      CloseHandle(fhOut);

   LogMsg("Total input records:        %u", lCnt);
   LogMsg("Total output records:       %u", lLDRRecCount);
   LogMsg("Total bad-city records:     %u", iBadCity);
   LogMsg("Total bad-suffix records:   %u", iBadSuffix);
   LogMsg("Last recording date:        %u\n", lLastRecDate);

   LogMsg("Use GIS city:               %u", lUseGis);
   LogMsg("Use Mail city:              %u\n", lUseMailCity);

   LogMsg("Total CHAR matched:         %u", lCharMatch);
   LogMsg("Total CHAR skipped:         %u\n", lCharSkip);

   printf("\nTotal output records: %u", lLDRRecCount);

   lRecCnt = lLDRRecCount;
   return 0;
}

/********************************** Sut_LoadRoll ****************************
 *
 *
 ****************************************************************************/

int Sut_LoadRoll(int iLoadFlag, int iSkip)
{
   char     acRec[MAX_RECSIZE], acBuf[MAX_RECSIZE], acRollRec[MAX_RECSIZE];
   char     cFileCnt=1;
   char     acRawFile[_MAX_PATH], acOutFile[_MAX_PATH];

   HANDLE   fhIn, fhOut;

   int      iRet, iTmp, iRollUpd=0, iNewRec=0, iRetiredRec=0;
   DWORD    nBytesRead;
   DWORD    nBytesWritten;
   BOOL     bRet, bEof;
   long     lRet=0, lCnt;

   sprintf(acRawFile, acRawTmpl, "SUT", "SUT", "S01");
   sprintf(acOutFile, acRawTmpl, "SUT", "SUT", "R01");

   // Rename files for processing
   if (_access(acRawFile, 0))
   {
      if (!_access(acOutFile, 0))
         rename(acOutFile, acRawFile);
      else
      {
         LogMsg("***** Missing input file %s.  Please recheck!");
         return -1;
      }
   }

   // Open roll file
   LogMsg("Open Roll file %s", acRollFile);
   fdRoll = fopen(acRollFile, "rb");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", acRollFile);
      return -2;
   }

   // Open Char file
   LogMsg("Open Char file %s", acCharFile);
   fdChar = fopen(acCharFile, "rb");
   if (fdChar == NULL)
   {
      LogMsg("***** Error opening Char file: %s\n", acCharFile);
      return -2;
   }

   // Open CityZip file
   iRet = initSitus(acIniFile, myCounty.acCntyCode);
   if (iRet < 0)
      return -3;

   // Open Input file
   LogMsg("Open input file %s", acRawFile);
   fhIn = CreateFile(acRawFile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhIn == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening input file: %s\n", acRawFile);
      return -4;
   }

   // Open Output file
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening output file: %s\n", acOutFile);
      return -4;
   }

   // Get first RollRec
   iRet = fread(acRollRec,  1, iRollLen,fdRoll);
   bEof = false;
   lCnt = 0;

   // Copy skip record
   memset(acBuf, ' ', iRecLen);
   while (iSkip-- > 0)
   {
      ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);
      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
   }

   iNoMatch=iBadCity=iBadSuffix=0;
   lUseMailCity=lUseGis=0;

   // Merge loop
   while (!bEof)
   {
      bRet = ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);
      if (!bRet)
      {
         LogMsg("***** Error reading input file %s (%f)", acRawFile, GetLastError());
         break;
      }

      // Check EOF
      if (iRecLen != nBytesRead)
         break;

      NextRollRec:
      iTmp = memcmp(acBuf, &acRollRec[SUT_OFF_APN], iApnLen);
      if (!iTmp)
      {
         lCnt++;

         // Merge roll data
         iRet = Sut_MergeRoll(acBuf, acRollRec, UPDATE_R01);
         iRollUpd++;

         if (fdChar)
            iRet = Sut_MergeChar(acBuf);

         // Read next roll record
         iRet = fread(acRollRec,  1, iRollLen,fdRoll);

         if (!iRet)
            bEof = true;
      } else if (iTmp > 0)       // Roll not match, new roll record
      {
         lCnt++;
         if (bDebug)
            LogMsg0("New roll record : %.*s (%d)", iApnLen, &acRollRec[SUT_OFF_APN], lCnt);

         // Create new R01 record
         if (acRollRec[SUT_OFF_APN] >= '0')
         {
            iRet = Sut_MergeRoll(acRec, acRollRec, CREATE_R01);
            if (!iRet)
            {
               iNewRec++;

               if (fdChar)
                  iRet = Sut_MergeChar(acBuf);

               // Save last recording date
               lRet = atoin((char *)&acRec[OFF_TRANSFER_DT], 8);
               if (lRet > lLastRecDate && lRet < lToday)
                  lLastRecDate = lRet;

               bRet = WriteFile(fhOut, acRec, iRecLen, &nBytesWritten, NULL);
            }
         } else
            LogMsg("*** Bad roll record, no APN at %d", lCnt);

         iRet = fread(acRollRec,  1, iRollLen, fdRoll);
         if (iRet == iRollLen)
            goto NextRollRec;
         else
            bEof = true;
      } else
      {
          iRetiredRec++;
          continue;
      }

      // Save last recording date
      lRet = atoin((char *)&acBuf[OFF_TRANSFER_DT], 8);
      if (lRet > lLastRecDate && lRet < lToday)
         lLastRecDate = lRet;

      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
      if (!(lCnt % 1000))
         printf("\r%u", lCnt);

      if (!bRet)
      {
         LogMsg("***** Error occurs: %d\n", GetLastError());
         lRet = WRITE_ERR;
         break;
      }
   }

   // If there are more new records, add them
   while (!bEof)
   {
      if (bDebug)
         LogMsg0("*** New roll record : %.*s (%d) ***", iApnLen, &acRollRec[SUT_OFF_APN], lCnt);

      // Create new R01 record
      iRet = Sut_MergeRoll(acRec, acRollRec, CREATE_R01);
      if (!iRet)
      {
         iNewRec++;

         if (fdChar)
            iRet = Sut_MergeChar(acBuf);

         // Save last recording date
         lRet = atoin((char *)&acBuf[OFF_TRANSFER_DT], 8);
         if (lRet > lLastRecDate && lRet < lToday)
            lLastRecDate = lRet;

         bRet = WriteFile(fhOut, acRec, iRecLen, &nBytesWritten, NULL);
      }

      // Get next roll record
      iRet = fread(acRollRec,  1, iRollLen,fdRoll);
      lCnt++;

      if (!iRet)
         bEof = true;
   }

   // Close files
   if (fdRoll)
      fclose(fdRoll);
   if (fdChar)
      fclose(fdChar);
   if (fdCity)
      fclose(fdCity);
   if (fhOut)
      CloseHandle(fhOut);
   if (fhIn)
      CloseHandle(fhIn);

   LogMsgD("Total records processed:    %u", lCnt);
   LogMsg("Total updated records:      %u", iRollUpd);
   LogMsg("Total new records:          %u", iNewRec);
   LogMsg("Total retired records:      %u", iRetiredRec);
   LogMsg("Total bad-city records:     %u", iBadCity);
   LogMsg("Total bad-suffix records:   %u", iBadSuffix);
   LogMsg("Last recording date:        %u\n", lLastRecDate);

   LogMsg("Use GIS city:               %u", lUseGis);
   LogMsg("Use Mail city:              %u\n", lUseMailCity);

   LogMsg("Total CHAR matched:         %u", lCharMatch);
   LogMsg("Total City matched:         %u\n", lCityMatch);

   LogMsg("Total CHAR skipped:         %u", lCharSkip);
   LogMsg("Total City skipped:         %u\n", lCitySkip);

   lRecCnt = lCnt;
   return 0;
}

/******************************* Sut_CreateLienRec ***************************
 *
 * Return 0 if successful, >0 if error
 *
 *****************************************************************************/

int Sut_CreateLienRec(char *pOutbuf, char *pRollRec)
{
   char     acTmp[256];
   long     lTmp;
   int      iTmp;
   SUT_ROLL *pRec = (SUT_ROLL *)pRollRec;
   LIENEXTR *pLienRec = (LIENEXTR *)pOutbuf;

   // Clear output buffer
   memset(pOutbuf, ' ', sizeof(LIENEXTR));

   // Start copying data
   memcpy(pLienRec->acApn, pRec->Apn, SUT_SIZ_APN);

   // TRA
   lTmp = atoin(pRec->Tra, SUT_SIZ_TRA);
   if (lTmp > 0)
   {
      iTmp = sprintf(acTmp, "%.6d", lTmp);
      memcpy(pLienRec->acTRA, acTmp, iTmp);
   }

   // Year assessed
   memcpy(pLienRec->acYear, myCounty.acYearAssd, 4);

   // Land
   long lLand = atoin(pRec->Land, SUT_SIZ_LAND);
   if (lLand > 0)
   {
      iTmp = sprintf(acTmp, "%*d", sizeof(pLienRec->acLand), lLand);
      memcpy(pLienRec->acLand, acTmp, iTmp);
   }

   // Improve - Ratio
   long lImpr = atoin(pRec->Structures, SUT_SIZ_STRUCTURES);
   if (lImpr > 0)
   {
      iTmp = sprintf(acTmp, "%*d", sizeof(pLienRec->acImpr), lImpr);
      memcpy(pLienRec->acImpr, acTmp, iTmp);

      iTmp = sprintf(acTmp, "%*d", sizeof(pLienRec->acRatio), (LONGLONG)lImpr*100/(lLand+lImpr));
      memcpy(pLienRec->acRatio, acTmp, iTmp);
   }

   // Pers Prop
   long lPers = atoin(pRec->Pers_Prop, SUT_SIZ_PERS_PROP);

   // Fixed Equitment
   long lFixt = atoin(pRec->Fixed_Equip, SUT_SIZ_FIXED_EQUIP);

   // Tree
   long lTree = atoin(pRec->Trees, SUT_SIZ_TREES);

   // Total other
   long lOthers = lPers + lFixt + lTree;
   if (lOthers > 0)
   {
      iTmp = sprintf(acTmp, "%*d", sizeof(pLienRec->acOther), lOthers);
      memcpy(pLienRec->acOther, acTmp, iTmp);

      if (lPers > 0)
      {
         iTmp = sprintf(acTmp, "%*d", sizeof(pLienRec->acPP_Val), lPers);
         memcpy(pLienRec->acPP_Val, acTmp, iTmp);
      }
      if (lFixt > 0)
      {
         iTmp = sprintf(acTmp, "%*d", sizeof(pLienRec->acME_Val), lFixt);
         memcpy(pLienRec->acME_Val, acTmp, iTmp);
      }
      if (lTree > 0)
      {
         iTmp = sprintf(acTmp, "%*d", sizeof(pLienRec->extra.Sut.LivingImpr), lTree);
         memcpy(pLienRec->extra.Sut.LivingImpr, acTmp, iTmp);
      }
   }

   // Gross
   lTmp = lOthers + lLand + lImpr;
   if (lTmp > 0)
   {
      iTmp = sprintf(acTmp, "%*d", sizeof(pLienRec->acGross), lTmp);
      memcpy(pLienRec->acGross, acTmp, iTmp);
   }

   // HO Exempt
   long lHOExe  = atoin(pRec->Homeowners_Exemp, SUT_SIZ_HOMEOWNERS_EXEMP);
   long lMiscExe= atoin(pRec->Misc_Exemp, SUT_SIZ_MISC_EXEMP);

   // Exemp total
   lTmp = lHOExe + lMiscExe;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_LIEN_EXEAMT, lTmp);
      memcpy(pLienRec->acExAmt, acTmp, SIZ_LIEN_EXEAMT);

      if (lHOExe > 0)
      {
         pLienRec->acHO[0] = '1';      // 'Y'
         iTmp = sprintf(acTmp, "%*d", sizeof(pLienRec->extra.Sut.HO_Exe), lHOExe);
         memcpy(pLienRec->extra.Sut.HO_Exe, acTmp, iTmp);
      } else
         pLienRec->acHO[0] = '2';      // 'N'

      // Misc Exempt
      if (lMiscExe > 0)
      {
         iTmp = sprintf(acTmp, "%*d", sizeof(pLienRec->extra.Sut.Misc_Exe), lMiscExe);
         memcpy(pLienRec->extra.Sut.Misc_Exe, acTmp, iTmp);
      }
   }

   // Exemption code
   memcpy(pLienRec->acExCode, pRec->Exemp_Code, SIZ_LIEN_EXECODE);

   // Prop 8
   if (pRec->Indicator_Codes[0] == 'P')
      pLienRec->SpclFlag = LX_PROP8_FLG;

   pLienRec->LF[0] = '\n';
   pLienRec->LF[1] = 0;
   return 0;
}

/******************************** Sut_ExtrProp8 *****************************
 *
 *
 ****************************************************************************/

int Sut_ExtrProp8()
{
   char  acBuf[MAX_RECSIZE], acRollRec[MAX_RECSIZE], acOutFile[_MAX_PATH];
   long  lCnt=0, iProp8Cnt=0, iRet;
   FILE  *fdOut;

   // Open roll file
   fdRoll = fopen(acRollFile, "r");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", acRollFile);
      return 2;
   }

   // Open Output file
   sprintf(acOutFile, acProp8Tmpl, myCounty.acCntyCode, myCounty.acCntyCode, myCounty.acYearAssd);
   LogMsg("Create prop8 output file %s", acOutFile);
   fdOut = fopen(acOutFile, "w");
   if (fdOut == NULL)
   {
      LogMsg("***** Error opening Prop8 extract file: %s\n", acOutFile);
      return 4;
   }

   // print header
   fputs("APN,Prop8_Flg\n", fdOut);

   // Merge loop
   while (!feof(fdRoll))
   {
      // Get roll rec
      iRet = fread(acRollRec, 1, iRollLen, fdRoll);
      if (iRollLen != iRet)
         break;

      // Prop8
      if (acRollRec[SUT_OFF_INDICATOR_CODES] == 'P')
      {
         acRollRec[SUT_OFF_APN+SUT_SIZ_APN] = 0;
         sprintf(acBuf, "%s,Y\n", myTrim(&acRollRec[SUT_OFF_APN]));

         fputs(acBuf, fdOut);
         iProp8Cnt++;
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdRoll)
      fclose(fdRoll);
   if (fdOut)
      fclose(fdOut);

   LogMsgD("\nTotal prop8 records output: %u", iProp8Cnt);
   LogMsgD("Total records processed:    %u\n", lCnt);

   return 0;
}

/********************************* Sut_ExtrLien *****************************
 *
 *
 ****************************************************************************/

int Sut_ExtrLien()
{
   char  acBuf[MAX_RECSIZE], acRollRec[MAX_RECSIZE], acOutFile[_MAX_PATH];
   FILE  *fdLien;
   long  lCnt=0, iRet;

   // Open roll file
   LogMsg("Open Roll file %s", acRollFile);
   fdRoll = fopen(acRollFile, "r");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", acRollFile);
      return 2;
   }

   // Open Output file
   sprintf(acOutFile, acLienTmpl, myCounty.acCntyCode, myCounty.acCntyCode);
   LogMsg("Open output file %s", acOutFile);
   fdLien = fopen(acOutFile, "w");
   if (fdLien == NULL)
   {
      LogMsg("***** Error opening Lien extract file: %s\n", acOutFile);
      return 4;
   }

   // Merge loop
   while (!feof(fdRoll))
   {
      // Get roll rec
      iRet = fread(acRollRec,  1, iRollLen,fdRoll);
      if (iRollLen != iRet)
         break;

      // Create new lien record
      Sut_CreateLienRec(acBuf, acRollRec);

      fputs(acBuf, fdLien);
      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdRoll)
      fclose(fdRoll);
   if (fdLien)
      fclose(fdLien);

   LogMsgD("Total records output:     %d\n", lCnt);

   lRecCnt = lCnt;
   return 0;
}

/***************************** Sut_ParseTaxDetail ****************************
 * 
 * Use Assessment Detail file to create Items, Agency
 *
 * Return 0 if success
 *
 *****************************************************************************/

int Sut_ParseTaxDetail(char *pDetailbuf, char *pAgencybuf, char *pInbuf)
{
   double   dTax;

   SUT_DETAIL *pInRec  = (SUT_DETAIL *)pInbuf;
   TAXDETAIL  *pDetail = (TAXDETAIL *)pDetailbuf;
   TAXAGENCY  *pAgency = (TAXAGENCY *)pAgencybuf, *pResult;

   // Clear output buffer
   memset(pDetailbuf, 0, sizeof(TAXDETAIL));
   memset(pAgencybuf, 0, sizeof(TAXAGENCY));

   // APN
   memcpy(pDetail->Apn, pInRec->Apn, iApnLen);
   myTrim(pDetail->Apn, iApnLen);

   // Tax Year
   memcpy(pDetail->TaxYear, pInRec->RollYear, 4);

   // Assessment Number
   memcpy(pDetail->Assmnt_No, pInRec->Assmt_No, SIZ_SUT_SA_ASSMT_NO);

#ifdef _DEBUG
   //if (!memcmp(pDetail->Apn, "101250018", 9) )
   //   iTmp = 0;
#endif

   // Tax Amount
   dTax  = atof(pInRec->TaxAmt1);
   dTax += atof(pInRec->TaxAmt2);
   if (!dTax)
      return 1;
   sprintf(pDetail->TaxAmt, "%.2f", dTax);

   // Tax Code
   memcpy(pDetail->TaxCode, pInRec->Assmt_Code, SIZ_SUT_SA_ASSMTCODE);
   memcpy(pAgency->Code, pInRec->Assmt_Code, SIZ_SUT_SA_ASSMTCODE);
   dTax = atof(pInRec->TaxRate);
   if (dTax != 0.0)
      memcpy(pAgency->TaxRate, pInRec->TaxRate, SIZ_SUT_TAXRATE);
   // Search on sorted list start at 0
   pResult = findTaxAgency(pAgency->Code, 0);
   if (pResult)
   {
      strcpy(pAgency->Agency, pResult->Agency);
      strcpy(pAgency->Phone, pResult->Phone);

      // Tax Rate - tax rate is now included in detail file 5/17/2021
      //if (pResult->TaxRate[0] > ' ')
      //{
      //   strcpy(pAgency->TaxRate, pResult->TaxRate);
      //   strcpy(pDetail->TaxRate, pResult->TaxRate);
      //}
   } else
   {
      pAgency->Agency[0] = 0;
      LogMsg("+++ Unknown TaxCode: %s", pDetail->TaxCode);
   }

   return 0;
}

int Sut_Load_TaxDetail(bool bImport)
{
   char     *pTmp, acItemsRec[MAX_RECSIZE], acAgencyRec[512], acRec[512];
   char     acItemsFile[_MAX_PATH], acAgencyFile[_MAX_PATH], acTmpFile[_MAX_PATH], acInFile[_MAX_PATH];

   int      iRet;
   long     lItems=0, lCnt=0, lAgency=0;
   FILE     *fdItems, *fdAgency, *fdIn;
   TAXDETAIL *pTax = (TAXDETAIL *)&acItemsRec[0];

   LogMsg("Loading Detail file");

   GetIniString(myCounty.acCntyCode, "TaxDetail", "", acInFile, _MAX_PATH, acIniFile);
   NameTaxCsvFile(acItemsFile, myCounty.acCntyCode, "Items");
   NameTaxCsvFile(acAgencyFile, myCounty.acCntyCode, "Agency");
   sprintf(acTmpFile, "%s\\%s\\%s_Agency.tmp", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);

   // Open input file
   LogMsg("Open Detail file %s", acInFile);
   fdIn = fopen(acInFile, "r");
   if (fdIn == NULL)
   {
      LogMsg("***** Error opening Detail file: %s\n", acInFile);
      return -2;
   }  

   // Open Items file
   LogMsg("Create Items file %s", acItemsFile);
   fdItems = fopen(acItemsFile, "w");
   if (fdItems == NULL)
   {
      LogMsg("***** Error creating Items file: %s\n", acItemsFile);
      return -4;
   }

   // Open Agency file
   LogMsg("Create Agency file %s", acTmpFile);
   fdAgency = fopen(acTmpFile, "w");
   if (fdAgency == NULL)
   {
      LogMsg("***** Error creating Agency file: %s\n", acTmpFile);
      return -4;
   }

   // Init variables

   // Merge loop 
   while (!feof(fdIn))
   {
      pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdIn);
      if (!pTmp)
         break;

      // Create Items & Agency record
      iRet = Sut_ParseTaxDetail(acItemsRec, acAgencyRec, acRec);
      if (!iRet)
      {
         // Create TaxBase record
         Tax_CreateDetailCsv(acRec, (TAXDETAIL *)&acItemsRec);
         lItems++;
         fputs(acRec, fdItems);

         // Create Agency record
         Tax_CreateAgencyCsv(acRec, (TAXAGENCY *)&acAgencyRec);
         fputs(acRec, fdAgency);
      } else
      {
         LogMsg0("---> No tax amount, ignore detail record# %d [%.45s]", lCnt, acRec); 
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdIn)
      fclose(fdIn);
   if (fdItems)
      fclose(fdItems);
   if (fdAgency)
      fclose(fdAgency);

   LogMsg("Total records processed:    %u", lCnt);
   LogMsg("Total Items records:        %u", lItems);

   // Import into SQL
   if (bImport)
   {
      iRet = doTaxImport(myCounty.acCntyCode, TAX_DETAIL);
      if (!iRet)
      {
         // Dedup Agency file before import
         LogMsg("Dedup Agency file %s to %s", acTmpFile, acAgencyFile);
         iRet = sortFile(acTmpFile, acAgencyFile, "S(#1,C,A) DUPOUT F(TXT)");
         if (iRet > 0)
            iRet = doTaxImport(myCounty.acCntyCode, TAX_AGENCY);
      }
   } else
      iRet = 0;

   return iRet;
}

/***************************** Sut_ParseTaxBase ******************************
 *
 * Use Paid/Unpaid file to create Base, Delq, Owner
 * We don't want to create Delq record since it has only Default Date
 *
 *****************************************************************************/

int Sut_ParseTaxBase(char *pBasebuf, char *pDelqbuf, char *pInbuf)
{
   double   dTmp, dTotalTax, dTax1, dTax2, dPen1, dPen2, dTotalDue;
   long     lTmp;

   TAXBASE  *pBaseRec = (TAXBASE *)pBasebuf;
   TAXDELQ  *pDelqRec = (TAXDELQ *)pDelqbuf;
   SUT_PAID *pInRec   = (SUT_PAID *)pInbuf;

   // Clear output buffer
   memset(pBasebuf, 0, sizeof(TAXBASE));
   memset(pDelqbuf, 0, sizeof(TAXDELQ));

   // APN
   memcpy(pBaseRec->Apn, pInRec->Apn, iApnLen);
   myTrim(pBaseRec->Apn, iApnLen);
   strcpy(pBaseRec->OwnerInfo.Apn, pBaseRec->Apn);

#ifdef _DEBUG
   //int iRet;
   //if (!memcmp(pBaseRec->Apn, "06040065", 8))
   //   iRet = 0;
#endif

   // Status default
   pBaseRec->isSecd[0] = '1';
   pBaseRec->BillType[0] = BILLTYPE_SECURED;
   pBaseRec->Inst1Status[0] = TAX_BSTAT_UNPAID;
   pBaseRec->Inst2Status[0] = TAX_BSTAT_UNPAID;

   // Assessment No/BillNum
   memcpy(pBaseRec->Assmnt_No, pInRec->Assmt_No, SIZ_SUT_PU_ASSMT_NO);
   memcpy(pBaseRec->OwnerInfo.BillNum, pInRec->Assmt_No, SIZ_SUT_PU_ASSMT_NO);

   // TRA
   lTmp = atoin(pInRec->TRA, SIZ_SUT_PU_TRA);
   if (lTmp > 0)
      sprintf(pBaseRec->TRA, "%.6d", lTmp);

   // Tax Year
   memcpy(pBaseRec->TaxYear, pInRec->RollYear, 4);
   strcpy(pBaseRec->OwnerInfo.TaxYear, pBaseRec->TaxYear);

   // Tax rate
   dTmp = atofn(pInRec->Total_Rate, SIZ_SUT_PU_TOTAL_RATE);
   if (dTmp > 0.0)
      sprintf(pBaseRec->TotalRate, "%.6f", dTmp);

   // Check for Tax amount
   dTax1 = atofn(pInRec->Inst1_TaxAmt, SIZ_SUT_TAXAMT, true);
   dTax2 = atofn(pInRec->Inst2_TaxAmt, SIZ_SUT_TAXAMT, true);
   dTotalTax = atofn(pInRec->Net_Tax, SIZ_SUT_TAXAMT, true);
   sprintf(pBaseRec->TotalTaxAmt, "%.2f", dTotalTax);
   sprintf(pBaseRec->TaxAmt1, "%.2f", dTax1);
   sprintf(pBaseRec->TaxAmt2, "%.2f", dTax2);

#ifdef _DEBUG
   //if ((long)dTotalTax != (long)(dTax1+dTax2))
   //   dTmp = dTax1+dTax2;
#endif

   // Tax rate
   dTmp = atofn(pInRec->Total_Rate, SIZ_SUT_PU_TOTAL_RATE);
   if (dTmp > 0.0)
      sprintf(pBaseRec->TotalRate, "%.6f", dTmp);

   memcpy(pBaseRec->DueDate1, pInRec->Inst1_DueDate, SIZ_SUT_PU_DATE);
   memcpy(pBaseRec->DueDate2, pInRec->Inst2_DueDate, SIZ_SUT_PU_DATE);
   dPen1 = atofn(pInRec->Inst1_PenAmt, SIZ_SUT_PU_INST1_PENAMT);
   dPen2 = atofn(pInRec->Inst2_PenAmt, SIZ_SUT_PU_INST2_PENAMT);

   // Paid
   dTotalDue = 0;
   if (pInRec->Inst1_Status[0] == 'P')
   {
      pBaseRec->Inst1Status[0] = TAX_BSTAT_PAID;
      memcpy(pBaseRec->PaidDate1, pInRec->Inst1_PaidDate, SIZ_SUT_PU_DATE);
      sprintf(pBaseRec->PaidAmt1, "%.2f", dTax1);

      // If paid after DueDate, add penalty
      if (memcmp(pInRec->Inst1_PaidDate, pInRec->Inst1_DueDate, SIZ_SUT_PU_DATE) > 0)
         sprintf(pBaseRec->PenAmt1, "%.2f", dPen1);
   } else if (pInRec->Inst1_Status[0] == ' ')
   {
      dTotalDue = dTax1;
      if (ChkDueDate(1, pInRec->Inst1_DueDate))
      {
         sprintf(pBaseRec->PenAmt1, "%.2f", dPen1);
         dTotalDue += dPen1;
         pBaseRec->Inst1Status[0] = TAX_BSTAT_PASTDUE;
      }
   } else if (pInRec->Inst1_Status[0] == 'C')    // Cancel
   {
      pBaseRec->Inst1Status[0] = TAX_BSTAT_CANCEL;
      dTotalDue = 0;
   } else if (pInRec->Inst1_Status[0] == 'N')    // Goveverment - no tax
   {
      pBaseRec->Inst1Status[0] = TAX_BSTAT_NOTAX;
      dTotalDue = 0;
   } else
      LogMsg("***** Unknown bill status: %c (APN=%s)", pInRec->Inst1_Status[0], pBaseRec->Apn);

   if (pInRec->Inst2_Status[0] == 'P')
   {
      pBaseRec->Inst2Status[0] = TAX_BSTAT_PAID;
      memcpy(pBaseRec->PaidDate2, pInRec->Inst2_PaidDate, SIZ_SUT_PU_DATE);
      sprintf(pBaseRec->PaidAmt2, "%.2f", dTax2);

      // If paid after DueDate, add penalty
      if (memcmp(pInRec->Inst2_PaidDate, pInRec->Inst2_DueDate, SIZ_SUT_PU_DATE) > 0)
         sprintf(pBaseRec->PenAmt2, "%.2f", dPen2);
   } else if (pInRec->Inst2_Status[0] == ' ')
   {
      dTotalDue += dTax2;
      if (ChkDueDate(2, pInRec->Inst2_DueDate))
      {
         dPen2 = atofn(pInRec->Inst2_PenAmt, SIZ_SUT_PU_INST2_PENAMT);
         sprintf(pBaseRec->PenAmt2, "%.2f", dPen2);
         dTotalDue += dPen2;
         pBaseRec->Inst2Status[0] = TAX_BSTAT_PASTDUE;
      }
   } else if (pInRec->Inst2_Status[0] == 'C')    // Cancel
   {
      pBaseRec->Inst2Status[0] = TAX_BSTAT_CANCEL;
      dTotalDue = 0;
   } else if (pInRec->Inst2_Status[0] == 'N')    // Goveverment - no tax
   {
      pBaseRec->Inst2Status[0] = TAX_BSTAT_NOTAX;
      dTotalDue = 0;
   } else if (pInRec->Inst2_Status[0] == 'R')    // ???
   {
      pBaseRec->Inst2Status[0] = TAX_BSTAT_REFUND;
   } else
      LogMsg("***** Unknown bill status: %c (APN=%s)", pInRec->Inst2_Status[0], pBaseRec->Apn);

   // Amount Due
   if (dTotalDue > 0.0)
      sprintf(pBaseRec->TotalDue, "%.2f", dTotalDue);

   // Default
   if (pInRec->DefaultDate[0] > '0')
   {
      lTmp = atoin(pInRec->DefaultDate, 4);
      if (lTmp > 1950 && lTmp <= lToyear)
      {
         sprintf(pBaseRec->DelqYear, "%d", lTmp-1);
         //pBaseRec->isDelq[0] = '1';
      }
   } 

   // Owner
   memcpy(pBaseRec->OwnerInfo.Name1, pInRec->Name, SIZ_SUT_PU_NAME);
   blankRem(pBaseRec->OwnerInfo.Name1, SIZ_SUT_PU_NAME);

   // CareOf
   if (pInRec->Careof[0] > ' ')
   {
      memcpy(pBaseRec->OwnerInfo.CareOf, pInRec->Careof, SIZ_SUT_PU_CAREOF);
      myTrim(pBaseRec->OwnerInfo.CareOf, SIZ_SUT_PU_CAREOF);
   }

   // DBA
   if (pInRec->Dba[0] > ' ')
   {
      memcpy(pBaseRec->OwnerInfo.Dba, pInRec->Dba, SIZ_SUT_PU_DBA);
      myTrim(pBaseRec->OwnerInfo.Dba, SIZ_SUT_PU_DBA);
   }

   // Mail addr
   memcpy(pBaseRec->OwnerInfo.MailAdr[0], pInRec->M_Addr, SIZ_SUT_PU_M_ADDR);
   myTrim(pBaseRec->OwnerInfo.MailAdr[0], SIZ_SUT_PU_M_ADDR);
   memcpy(pBaseRec->OwnerInfo.MailAdr[1], pInRec->M_CitySt, SIZ_SUT_PU_M_CITYST+SIZ_SUT_PU_M_ZIP);
   blankRem(pBaseRec->OwnerInfo.MailAdr[1], SIZ_SUT_PU_M_CITYST+SIZ_SUT_PU_M_ZIP);

   return 0;
}

/**************************** Sut_Load_TaxBase *******************************
 *
 * Use Paid/Unpaid file to create Base, Delq, Owner
 *
 * Return 0 if success.
 *
 *****************************************************************************/

int Sut_Load_TaxBase(bool bImport)
{
   char     *pTmp, acBasebuf[1024], acDelqbuf[1024], acRec[MAX_RECSIZE];
   char     acBaseFile[_MAX_PATH], acOwnerFile[_MAX_PATH], acInFile[_MAX_PATH];

   int      iRet;
   long     lOut=0, lCnt=0;
   FILE     *fdBase, *fdOwner, *fdIn;
   TAXBASE  *pTaxBase = (TAXBASE *)&acBasebuf[0];
   TAXDELQ  *pTaxDelq = (TAXDELQ *)&acDelqbuf[0];

   LogMsg("Loading tax file");

   GetIniString(myCounty.acCntyCode, "TaxPaid", "", acInFile, _MAX_PATH, acIniFile);
   NameTaxCsvFile(acBaseFile, myCounty.acCntyCode, "Base");
   NameTaxCsvFile(acOwnerFile, myCounty.acCntyCode, "Owner");
   pTmp = strrchr(acBaseFile, '\\');
   *pTmp = 0;
   if (_access(acBaseFile, 0))
      _mkdir(acBaseFile);
   *pTmp = '\\';

   // Open input file
   LogMsg("Open Paid/Unpaid file %s", acInFile);
   fdIn = fopen(acInFile, "r");
   if (fdIn == NULL)
   {
      LogMsg("***** Error opening Paid/Unpaid file: %s\n", acInFile);
      return -2;
   }  

   lLastTaxFileDate = getFileDate(acInFile);

   // Open base file
   LogMsg("Open Base file %s", acBaseFile);
   fdBase = fopen(acBaseFile, "w");
   if (fdBase == NULL)
   {
      LogMsg("***** Error creating base file: %s\n", acBaseFile);
      return -4;
   }

   // Open Owner file
   LogMsg("Open Owner file %s", acOwnerFile);
   fdOwner = fopen(acOwnerFile, "w");
   if (fdOwner == NULL)
   {
      LogMsg("***** Error creating Owner file: %s\n", acOwnerFile);
      return -4;
   }

   // Init variables

   // Merge loop 
   while (!feof(fdIn))
   {
      pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdIn);
      if (!pTmp)
         break;

      // Create new R01 record
      iRet = Sut_ParseTaxBase(acBasebuf, acDelqbuf, acRec);
      if (!iRet)
      {
         // Create TaxBase record
         Tax_CreateTaxBaseCsv(acRec, (TAXBASE *)&acBasebuf);
         lOut++;
         fputs(acRec, fdBase);

         Tax_CreateTaxOwnerCsv(acRec, (TAXOWNER *)&pTaxBase->OwnerInfo);
         fputs(acRec, fdOwner);
      } else
      {
         LogMsg("---> Drop record %d [%.40s]", lCnt, acRec); 
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdIn)
      fclose(fdIn);
   if (fdBase)
      fclose(fdBase);
   if (fdOwner)
      fclose(fdOwner);

   LogMsg("Total records processed:    %u", lCnt);
   LogMsg("Total output records:       %u", lOut);

   // Import into SQL
   if (bImport)
   {
      iRet = doTaxImport(myCounty.acCntyCode, TAX_BASE);
      //if (!iRet)
      //   iRet = doTaxImport(myCounty.acCntyCode, TAX_DELINQUENT);
      doTaxPrep(myCounty.acCntyCode, TAX_DELINQUENT);
      if (!iRet)
         iRet = doTaxImport(myCounty.acCntyCode, TAX_OWNER);
   } else
      iRet = 0;

   return iRet;
}

/****************************** Sut_FmtChar **********************************
 *
 * Convert SUT_CHAR record to STDCHAR format
 *
 *****************************************************************************/

int Sut_FmtChar(char *pOutbuf, char *pInbuf)
{
   char     acTmp[256], acCode[32];
   long     lTmp, lBldgSqft;
   int      iRet, iTmp, iBeds, iFBath, iHBath;

   STDCHAR  *pCharRec=(STDCHAR *)pOutbuf;
   SUT_CHAR *pRawRec =(SUT_CHAR *)pInbuf;
   
   // Init output buffer
   memset(pOutbuf, ' ', sizeof(STDCHAR));

   // APN
   memcpy(pCharRec->Apn, pRawRec->Apn, CSIZ_APN);

   // Format APN
   memcpy(pCharRec->Apn_D, pRawRec->FmtApn, CSIZ_FMTAPN);

#ifdef _DEBUG
   //if (!memcmp(pCharRec->Apn, "00001018", 8) )
   //   iTmp = 0;
#endif

   // YrBlt
   lTmp = atoin(pRawRec->YearBuilt, CSIZ_YRBLT);
   if (lTmp > 1700)
      memcpy(pCharRec->YrBlt, pRawRec->YearBuilt, CSIZ_YRBLT);

   // Lot sqft/Acres
   double dAcres = atof(pRawRec->Acres);
   if (dAcres > 0.0)
   {
      iTmp = sprintf(acTmp, "%d", (long)(dAcres*1000.0));
      memcpy(pCharRec->LotAcre, acTmp, iTmp);
      lTmp = (long)(dAcres * SQFT_PER_ACRE);
      iTmp = sprintf(acTmp, "%d", lTmp);
      memcpy(pCharRec->LotSqft, acTmp, iTmp);
   }

   // BldgSqft 
   lBldgSqft = atoin(pRawRec->BldgSqft, CSIZ_PRI_SQFT);
   if (lBldgSqft > 50)
   {
      iTmp = sprintf(acTmp, "%d", lBldgSqft);
      memcpy(pCharRec->BldgSqft, acTmp, iTmp);
   }

   // Misc impr
   lBldgSqft  = atoin(pRawRec->OthBldg_Sqft, CSIZ_OTH_BLDG_SQFT);
   lBldgSqft += atoin(pRawRec->OthRoom1_Sqft, CSIZ_OTH_ROOM1_SQFT);
   lBldgSqft += atoin(pRawRec->OthRoom2_Sqft, CSIZ_OTH_ROOM2_SQFT);
   if (lBldgSqft > 10)
   {
      iTmp = sprintf(acTmp, "%d", lBldgSqft);
      memcpy(pCharRec->MiscSqft, acTmp, iTmp);
   }

   // Park spaces
   int iGarage = atoin(pRawRec->Garages, CSIZ_GARAGES);
   // Carport
   int iCarport = atoin(pRawRec->CarPorts, CSIZ_CARPORT);
   // Garage Sqft
   int lGarSqft = atoin(pRawRec->GarSqft, CSIZ_GAR_SQFT);
   // Carport Sqft
   int lCarSqft = atoin(pRawRec->CarSqft, CSIZ_GAR_SQFT);

   if (iGarage > 0 && iCarport > 0)
   {
      iTmp = sprintf(acTmp, "%d", iGarage+iCarport);
      memcpy(pCharRec->ParkSpace, acTmp, iTmp);
      pCharRec->ParkType[0] = '2';
   } else if (iGarage > 0)
   {
      iTmp = sprintf(acTmp, "%d", iGarage);
      memcpy(pCharRec->ParkSpace, acTmp, iTmp);
      pCharRec->ParkType[0] = 'Z';
   } else if (iCarport > 0)
   {
      iTmp = sprintf(acTmp, "%d", iCarport);
      memcpy(pCharRec->ParkSpace, acTmp, iTmp);
      pCharRec->ParkType[0] = 'C';
   } 

   lGarSqft += lCarSqft;
   if (lGarSqft > 100)
   {
      iTmp = sprintf(acTmp, "%d", lGarSqft);
      memcpy(pCharRec->GarSqft, acTmp, iTmp);
   }

   // Beds
   iBeds = atoin(pRawRec->Beds, CSIZ_BEDS);
   if (iBeds > 0)
   {
      iTmp = sprintf(acTmp, "%d", iBeds);
      memcpy(pCharRec->Beds, acTmp, iTmp);
   }

   // Baths
   iFBath = atoin(pRawRec->Baths, CSIZ_BATHS);
   if (iFBath > 0)
   {
      iTmp = sprintf(acTmp, "%d", iFBath);
      memcpy(pCharRec->FBaths, acTmp, iTmp);
      memcpy(pCharRec->Bath_4Q, acTmp, iTmp);
   }

   // Half bath
   iHBath = atoin(pRawRec->HBath, CSIZ_HBATH);
   if (iHBath > 0)
   {
      iTmp = sprintf(acTmp, "%d", iHBath);
      memcpy(pCharRec->Bath_2Q, acTmp, iTmp);
   }

   // Fireplace
   iTmp = atoin(pRawRec->FirePlaces, CSIZ_FIREPLACES);
   if (iTmp > 9)
      pCharRec->Fireplace[0] = 'M';
   else if (iTmp > 0)
      pCharRec->Fireplace[0] = '0' | iTmp;
   else if (pRawRec->FirePlaces[0] > '0')
      LogMsg("*** Bad FirePlace %.*s [%.*s]", CSIZ_FIREPLACES, pRawRec->FirePlaces, iApnLen, pRawRec->Apn);

   // Pool
   if (pRawRec->Pool[0] > '0')
   {
      char *pPoolType = &pRawRec->Pool[2];

      if (!memcmp(pPoolType, "GUNITE", 6))
         pCharRec->Pool[0] = 'G';
      else if (!memcmp(pPoolType, "VINYL", 5))
         pCharRec->Pool[0] = 'V';
      else if (!memcmp(pPoolType, "FIBER", 5))
         pCharRec->Pool[0] = 'F';
      else
         iTmp = 0;
   } 

   // Spa
   if (pRawRec->Spa[0] == 'Y')
   {
      if (pCharRec->Pool[0] > ' ')
         pCharRec->Pool[0] = 'C';      // Pool/Spa
      else
         pCharRec->Spa[0] = 'S';       // Spa only
   }

   // Zoning
   if (pRawRec->Zoning[0] > ' ')
   {
      memcpy(acTmp, pRawRec->Zoning, CSIZ_ZONING);
      acTmp[CSIZ_ZONING] = 0;
      _strupr(acTmp);
      memcpy(pCharRec->Zoning, acTmp, CSIZ_ZONING);
   } 

   // Quality/class
   if (isalpha(pRawRec->Class1[0]))
   {  // D 05.5BAVERAGE
      pCharRec->BldgClass = pRawRec->Class1[0];
      iTmp = atoin(pRawRec->Class2, 2);
      sprintf(acTmp, "%d.%c", iTmp, pRawRec->Class2[3]);
      iRet = Value2Code(acTmp, acCode, NULL);
      if (iRet >= 0)
         pCharRec->BldgQual = acCode[0];
   } 

   // Condition
   if (pRawRec->Condition[0] > ' ')
   {
      switch (pRawRec->Condition[0])
      {
         case 'P':
            acCode[0] = 'P';    // Poor
            break;
         case 'F':
            acCode[0] = 'F';    // Fair
            break;
         case 'A':
            acCode[0] = 'A';    // Average
            break;
         case 'G':
            acCode[0] = 'G';    // Good
            break;
         default:
            acCode[0] = ' ';
      }
      pCharRec->ImprCond[0] = acCode[0];
   } 

   // Stories
   iTmp = atoin(pRawRec->Stories, CSIZ_STORIES);
   if (iTmp > 0 && iTmp < 100)
   {
      iRet = sprintf(acTmp, "%d.0", iTmp);
      memcpy(pCharRec->Stories, acTmp, iRet);
   } 

   // Units
   iTmp = atoin(pRawRec->Units, CSIZ_UNITS);
   if (iTmp > 0)
   {
      iRet = sprintf(acTmp, "%d", iTmp);
      memcpy(pCharRec->Units, acTmp, iRet);
   } 

   // Water
   if (pRawRec->Water[0] > ' ')
   {
      iTmp = 0;
      while (Sut_Water[iTmp].iLen > 0)
      {
         if (!memcmp(pRawRec->Water, Sut_Water[iTmp].acSrc, Sut_Water[iTmp].iLen))
         {
            pCharRec->Water = Sut_Water[iTmp].acCode[0];
            break;
         }
         iTmp++;
      }
   } 

   // Sewer
   if (pRawRec->Sewer[0] > ' ')
   {
      iTmp = 0;
      while (Sut_Sewer[iTmp].iLen > 0)
      {
         if (!memcmp(pRawRec->Sewer, Sut_Sewer[iTmp].acSrc, Sut_Sewer[iTmp].iLen))
         {
            pCharRec->Sewer = Sut_Sewer[iTmp].acCode[0];
            break;
         }
         iTmp++;
      }
   } 

   // Heat/Cool - need explaination?
   iTmp = 0;
   while (*Sut_HeatCool[iTmp].pName > ' ')
   {
      if (!memcmp(pRawRec->Heat_Cool, Sut_HeatCool[iTmp].pName, strlen(Sut_HeatCool[iTmp].pName)))
      {
         pCharRec->Heating[0] = Sut_HeatCool[iTmp].pCode[0];
         pCharRec->Cooling[0] = Sut_HeatCool[iTmp].pCode[1];
         break;
      }
      iTmp++;
   }

   pCharRec->CRLF[0] = '\n';
   pCharRec->CRLF[1] = '\0';

   return 0;
}

/***************************** Sut_ConvStdChar *******************************
 *
 * Return 0 if successful, otherwise error
 *
 *****************************************************************************/

int Sut_ConvStdChar(char *pInfile)
{
   char  acCharRec[2048], acInRec[MAX_RECSIZE], acTmpFile[_MAX_PATH], *pTmp;
   long  lCnt=0, lChars=0, iRet;
   FILE  *fdIn;

   LogMsg0("Extracting Chars from %s", pInfile);

   // Check current sale file
   if (_access(pInfile, 0))
   {
      LogMsg("***** Missing input file %s", pInfile);
      return -1;
   }

   // Open char file
   LogMsg("Open char file to extract CHARS %s", pInfile);
   fdIn = fopen(pInfile, "r");
   if (fdIn == NULL)
   {
      LogMsg("***** Error opening char file: %s\n", pInfile);
      return -2;
   }

   // Create output file
   sprintf(acTmpFile, "%s\\%s\\%s_Char.tmp", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode); 
   LogMsg("Create output chars file %s", acTmpFile);
   fdChar = fopen(acTmpFile, "w");
   if (fdChar == NULL)
   {
      LogMsg("***** Error creating char file: %s\n", acTmpFile);
      return -3;
   }

   // Merge loop
   while (!feof(fdIn))
   {
      // Get next record
      pTmp = fgets(acInRec, MAX_RECSIZE, fdIn);
      if (!pTmp)
         break;
#ifdef _DEBUG
   //if (!memcmp(acInRec, "00001018", 8) )
   //   iRet = 0;
#endif

      iRet = Sut_FmtChar(acCharRec, acInRec);
      if (!iRet)
      {
         fputs(acCharRec, fdChar);
         lChars++;
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdChar)
      fclose(fdChar);
   if (fdIn)
      fclose(fdIn);

   LogMsg("Total input records processed: %u", lCnt);
   LogMsg("Total char records output:     %u", lChars);

   return 0;
}

/*********************************** loadSut ********************************
 *
 * Input files:
 *    - Sut_Lien (Lien file, 599-byte EBCDIC with CRLF)
 *    - SutRoll  (roll file, 599-byte EBCDIC with CRLF)
 *
 * Operation commands:
 *    - Normal update:  LoadOne -CSUT -U [-Mn]
 *    - Load Lien:      LoadOne -CSUT -L -Xl [-X8]
 *
 ****************************************************************************/

int loadSut(int iSkip)
{
   char  acTmpFile[_MAX_PATH], acUnzipFolder[_MAX_PATH];
   int   iRet;

   iApnLen = myCounty.iApnLen;

   // Loading Tax
   if (iLoadTax == TAX_LOADING)                    // -T or -Lt
   {
      iRet = GetIniString(myCounty.acCntyCode, "TaxDist", "", acTmpFile, _MAX_PATH, acIniFile);
      if (iRet > 0)
         iRet = LoadTaxCodeTable(acTmpFile);

      iRet = GetIniString(myCounty.acCntyCode, "TaxFile", "", acTmpFile, _MAX_PATH, acIniFile);
      if (iRet > 0 && !_access(acTmpFile, 0))
      {
         GetIniString(myCounty.acCntyCode, "UnzipTCFolder", "", acUnzipFolder, _MAX_PATH, acIniFile);
         iRet = unzipOne(acTmpFile, acUnzipFolder, true);        
         if (iRet)
            return -1;
      }

      // Load Tax Base, Delq & Owmer tables
      iRet = Sut_Load_TaxBase(bTaxImport);

      // Load Tax Items & Agency tables
      if (!iRet && lLastTaxFileDate > 0)
         iRet = Sut_Load_TaxDetail(bTaxImport);
   }

   if (!iLoadFlag)
      return iRet;

 	// Load char file
   if (iLoadFlag & EXTR_ATTR)                      // -Xa
   {
      // Load residential char
      iRet = Sut_ConvStdChar(acCharFile);
   }

   // Extract NDC recorder sale 
   GetIniString("Data", "ASH_File", acESalTmpl, acESalTmpl, _MAX_PATH, acIniFile);
   if (iLoadFlag & EXTR_NRSAL)                     // -Xn
   {
      iRet = GetIniString(myCounty.acCntyCode, "NdcSale", "", acSaleFile, _MAX_PATH, acIniFile);
      if (!_access(acSaleFile, 0))
      {
         iRet = NR_CreateSCSale(myCounty.acCntyCode, acSaleFile, iApnLen-2);
         if (iRet)
            return iRet;
      } else
      {
         LogMsg("*** Sale file not available.  Please verify: %s", acSaleFile);
         return -1;
      }
   }

   // Load tables
   //if (!LoadLUTable((char *)&acLookupTbl[0], "[Quality]", NULL, MAX_ATTR_ENTRIES))
   //{
   //   LogMsg("*** Error Looking for table [Quality] in %s", acLookupTbl);
   //   return 1;
   //}

   // Open roll file
   if (iLoadFlag & (LOAD_LIEN|EXTR_LIEN))
      sprintf(acTmpFile, "%s\\%s\\%s_Lien.asc", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   else
      sprintf(acTmpFile, "%s\\%s\\%s_Roll.asc", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);

   LogMsg("Translate %s to Ascii %s", acRollFile, acTmpFile);
   iRet = doEBC2ASC(acRollFile, acTmpFile);
   if (!iRet)
   {
      strcpy(acRollFile, acTmpFile);

      if (iLoadFlag & EXTR_LIEN)                   // -Xl
      {
         LogMsg0("Extract %s LDR file", myCounty.acCntyCode);
         Sut_ExtrLien();
      }

      if (lOptProp8 == MYOPT_EXT)                  // -X8
      {
         LogMsg0("Extract Prop8 flag from LDR file %s", acRollFile);
         iRet = Sut_ExtrProp8();
      }

      if (iLoadFlag & LOAD_LIEN)                   // -L
      {
         // Create Lien file
         LogMsg0("Load %s Lien file", myCounty.acCntyCode);
         iRet = Sut_Load_LDR(iLoadFlag, iSkip);
      } else if (iLoadFlag & LOAD_UPDT)            // -U
      {
         LogMsg0("Load %s roll update file", myCounty.acCntyCode);
         iRet = Sut_LoadRoll(iLoadFlag, iSkip);
      }
   } else
   {
      if (iRet == NOTFOUND_ERR)
         LogMsg("***** Invalid file name or file not found %s.  Please check input file and modify LoadOne.ini if needed.", acRollFile);
      else if (iRet == WRITE_ERR)
         LogMsg("***** Error writing to %s.  Please check available disk space.", acTmpFile);
      else  // OPEN_ERR
         LogMsg("***** Error converting %s to ASCII.  Please check input and output file then retry.", acRollFile);
   }

   // Apply cum sale file to R01
   if (!iRet && (iLoadFlag & UPDT_XSAL) )          // -Mn
   {
      GetIniString("Data", "ASH_File", acESalTmpl, acESalTmpl, _MAX_PATH, acIniFile);
      sprintf(acCSalFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "Sls");

      // Apply Sut_Ash.sls to R01 file
      iRet = ApplyCumSale(iSkip, acCSalFile, false, SALE_USE_SCUPDXFR, CLEAR_OLD_SALE);
      if (!iRet)
         iLoadFlag |= LOAD_UPDT;
   }

   // Fix TRA
   if (!iRet && (lOptMisc & M_OPT_FIXTRA))
      iRet = PQ_FixR01(myCounty.acCntyCode, PQ_FIX_TRA, 0, true);

   return iRet;
}