Version 1.3.2
Final release for LoadOne.  All counties has been converted.
Version 1.3.7
Process Fre\PCTRA.TXT file to merge public parcels into roll file.

Version 8.0
Starting 2008 LDR, use last two digits year as major version number.
The minor number will be for change that affects all counties.
The revision number will be for specific county change.
The subrevision number will be for bug fix

Version 8.3
Change LoadOne.ini
   1) [FRE]
      TraFile -> PubParcelFile
      TraRecSize -> PubParcelSize
   2) [MPA]
      PubParcelFile=E:\ftpsvr\usr\MPA\Unassessed.txt
      PubParcelSize=1231


Notes: To change VSS effectively on system with multiple VSS,
		 delete *.opt then load project.