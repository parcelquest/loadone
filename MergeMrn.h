#ifndef  _MERGEMRN_H_
#define  _MERGEMRN_H_

// 2021 Secured Equalized Roll.txt
#define  MRN_ER_PROPERTY_ID         0
#define  MRN_ER_TAX_RATE_AREA       1
#define  MRN_ER_USE_CD              2
#define  MRN_ER_LIVING_UNITS        3
#define  MRN_ER_SITUS_FORMATTED     4
#define  MRN_ER_SITUS_CITY          5
#define  MRN_ER_MAIL_TO_NAME_1      6
#define  MRN_ER_MAIL_TO_NAME_2      7
#define  MRN_ER_CARE_OF_NAME        8
#define  MRN_ER_ATTENTION_NAME      9
#define  MRN_ER_MAIL_LINE_1         10
#define  MRN_ER_MAIL_LINE_2         11
#define  MRN_ER_MAIL_LINE_3         12
#define  MRN_ER_MAIL_CITY           13
#define  MRN_ER_MAIL_STATE          14
#define  MRN_ER_MAIL_COUNTRY        15
#define  MRN_ER_MAIL_ZIP_CODE       16
#define  MRN_ER_LAND_VALUE          17
#define  MRN_ER_IMP_VALUE           18
#define  MRN_ER_PERS_VALUE          19
#define  MRN_ER_BUS_VALUE           20
#define  MRN_ER_TOT_LAND_IMP_VALUE  21
#define  MRN_ER_TOT_EXE_VALUE       22
#define  MRN_ER_DEED_REF_ID         23
#define  MRN_ER_RECORDATION_DT      24
#define  MRN_ER_ROLL_YR             25

// MRN county definition
// 2016
//#define  MRN_L_APN               0
//#define  MRN_L_TRA               1
//#define  MRN_L_USECODE           2 
//#define  MRN_L_UNITS             3
//#define  MRN_L_SITUSADDR         4 
//#define  MRN_L_OWNER1            5
//#define  MRN_L_OWNER2            6
//#define  MRN_L_CAREOF            7
//#define  MRN_L_ATTNNAME          8
//#define  MRN_L_MAIL1             9 
//#define  MRN_L_MAIL2             10 
//#define  MRN_L_MAIL3             11 
//#define  MRN_L_MAILCITY          12 
//#define  MRN_L_MAILSTATE         13 
//#define  MRN_L_MAILCOUNTRY       14 
//#define  MRN_L_MAILZIP           15 
//#define  MRN_L_LAND              16 
//#define  MRN_L_IMPR              17 
//#define  MRN_L_PP_VALUE          18 
//#define  MRN_L_BUS_VALUE         19 
//#define  MRN_L_TOTAL_VALUE       20 
//#define  MRN_L_TOTAL_EXE         21 
//#define  MRN_L_RECNUM            22 
//#define  MRN_L_ROLLYEAR          23 

// 2015 & 2017-2022
//#define  MRN_L_APN               0
//#define  MRN_L_TRA               1
//#define  MRN_L_USECODE           2 
//#define  MRN_L_UNITS             3
//#define  MRN_L_SITUSADDR         4 
//#define  MRN_L_SITUSCITY         5
//#define  MRN_L_OWNER1            6
//#define  MRN_L_OWNER2            7
//#define  MRN_L_CAREOF            8
//#define  MRN_L_ATTNNAME          9
//#define  MRN_L_MAIL1             10 
//#define  MRN_L_MAIL2             11 
//#define  MRN_L_MAIL3             12 
//#define  MRN_L_MAILCITY          13 
//#define  MRN_L_MAILSTATE         14 
//#define  MRN_L_MAILCOUNTRY       15 
//#define  MRN_L_MAILZIP           16 
//#define  MRN_L_LAND              17 
//#define  MRN_L_IMPR              18 
//#define  MRN_L_PP_VALUE          19 
//#define  MRN_L_BUS_VALUE         20 
//#define  MRN_L_TOTAL_VALUE       21 
//#define  MRN_L_TOTAL_EXE         22 
//#define  MRN_L_RECNUM            23 
//#define  MRN_L_RECDATE           24 
//#define  MRN_L_ROLLYEAR          25 

// 2023 - 2023 Preliminary Secured Roll.txt
#define  MRN_L_APN               0
#define  MRN_L_TRA               1
#define  MRN_L_USECODE           2
#define  MRN_L_UNITS             3
#define  MRN_L_SITUSADDR         4
#define  MRN_L_OWNER1            5
#define  MRN_L_OWNER2            6
#define  MRN_L_CAREOF            7
#define  MRN_L_ATTNNAME          8
#define  MRN_L_MAIL1             9
#define  MRN_L_MAIL2             10
#define  MRN_L_MAIL3             11
#define  MRN_L_MAILCITY          12
#define  MRN_L_MAILSTATE         13
#define  MRN_L_MAILCOUNTRY       14
#define  MRN_L_MAILZIP           15
#define  MRN_L_LAND              16
#define  MRN_L_IMPR              17
#define  MRN_L_PP_VALUE          18
#define  MRN_L_BUS_VALUE         19
#define  MRN_L_TOTAL_VALUE       20
#define  MRN_L_TOTAL_EXE         21
#define  MRN_L_ROLLYEAR          22

#define  ROFF_APN                  1
#define  ROFF_TRA                  9
#define  ROFF_DOCTYPE             15
#define  ROFF_DOCNUM              17
#define  ROFF_DOCDATE             27
#define  ROFF_FILLER1             35
#define  ROFF_ACREAGE             41
#define  ROFF_UNITS               47
#define  ROFF_LAND                51
#define  ROFF_IMPR                59
#define  ROFF_PERSPROP            67
#define  ROFF_BUS_INV             75
#define  ROFF_PRIOR_LAND          83
#define  ROFF_PRIOR_IMPR          91
#define  ROFF_PRIOR_PERSPROP      99
#define  ROFF_PRIOR_BUS_INV      107
#define  ROFF_LAST_EVENT_DATE    115
#define  ROFF_LAST_EVENT_TYPE    123
#define  ROFF_BASE_YEAR          127
#define  ROFF_HO_EXE             131
#define  ROFF_EXECODE2           139
#define  ROFF_EXE_AMT2           141
#define  ROFF_EXECODE3           149
#define  ROFF_EXE_AMT3           151
#define  ROFF_EXECODE4           159
#define  ROFF_EXE_AMT4           161
#define  ROFF_FILLER2            169
#define  ROFF_SENIOR_CIT         173
#define  ROFF_S_STRNUM           175
#define  ROFF_S_UNIT             183
#define  ROFF_S_STRDIR           188   // updated 02/27/2019
#define  ROFF_S_STRNAME          190
#define  ROFF_S_STRTYPE          216
#define  ROFF_S_ZIP              220
#define  ROFF_FILLER3            230
#define  ROFF_NAME1              232
#define  ROFF_NAME2              264
#define  ROFF_MAILADDR1          296
#define  ROFF_MAILADDR2          328
#define  ROFF_MAILADDR3          360
#define  ROFF_MAILADDR4          392

#define  RSIZ_APN                8 
#define  RSIZ_TRA                6 
#define  RSIZ_DOCTYPE            2
#define  RSIZ_DOCNUM             10
#define  RSIZ_DOCDATE            8
#define  RSIZ_FILLER1            6
#define  RSIZ_ACREAGE            6 
#define  RSIZ_UNITS              4 
#define  RSIZ_LAND               8 
#define  RSIZ_IMPR               8 
#define  RSIZ_PERSPROP           8 
#define  RSIZ_BUS_INV            8 
#define  RSIZ_PRIOR_LAND         8 
#define  RSIZ_PRIOR_IMPR         8 
#define  RSIZ_PRIOR_PERSPROP     8 
#define  RSIZ_PRIOR_BUS_INV      8 
#define  RSIZ_LAST_EVENT_DATE    8 
#define  RSIZ_LAST_EVENT_TYPE    4 
#define  RSIZ_BASE_YEAR          4 
#define  RSIZ_HO_EXE             8 
#define  RSIZ_EXECODE2           2 
#define  RSIZ_EXE_AMT2           8 
#define  RSIZ_EXECODE3           2 
#define  RSIZ_EXE_AMT3           8 
#define  RSIZ_EXECODE4           2 
#define  RSIZ_EXE_AMT4           8 
#define  RSIZ_FILLER2            4 
#define  RSIZ_SENIOR_CIT         2 
#define  RSIZ_S_STRNUM           8 
#define  RSIZ_S_UNIT             5 
#define  RSIZ_S_STRDIR           2 
#define  RSIZ_S_STRNAME          26
#define  RSIZ_S_STRTYPE          4 
#define  RSIZ_S_ZIP              10
#define  RSIZ_FILLER3             2 
#define  RSIZ_NAME1              32
#define  RSIZ_NAME2              32
#define  RSIZ_MAILADDR1          32
#define  RSIZ_MAILADDR2          32
#define  RSIZ_MAILADDR3          32
#define  RSIZ_MAILADDR4          32

typedef struct _tMrnRoll
{  // 424 bytes                     
  char  Apn          [RSIZ_APN            ];   
  char  TRA          [RSIZ_TRA            ];   
  char  DocType      [RSIZ_DOCTYPE        ];   
  char  DocNum       [RSIZ_DOCNUM         ];   
  char  DocDate      [RSIZ_DOCDATE        ];   
  char  Filler1      [RSIZ_FILLER1        ];   
  char  Acreage      [RSIZ_ACREAGE        ];   
  char  Units        [RSIZ_UNITS          ];   
  char  Land         [RSIZ_LAND           ];   
  char  Impr         [RSIZ_IMPR           ];   
  char  PP_Val       [RSIZ_PERSPROP       ];   
  char  Bus_Inv      [RSIZ_BUS_INV        ];   
  char  Prior_Land   [RSIZ_PRIOR_LAND     ];   
  char  Prior_Impr   [RSIZ_PRIOR_IMPR     ];   
  char  Prior_PP_Val [RSIZ_PRIOR_PERSPROP ];
  char  Prior_Bus_Inv[RSIZ_PRIOR_BUS_INV  ];   
  char  LEvent_Date  [RSIZ_LAST_EVENT_DATE];
  char  LEvent_Type  [RSIZ_LAST_EVENT_TYPE];
  char  Base_Year    [RSIZ_BASE_YEAR      ];   
  char  Ho_Exe       [RSIZ_HO_EXE         ];   
  char  Exe_Code2    [RSIZ_EXECODE2       ];   
  char  Exe_Amt2     [RSIZ_EXE_AMT2       ];   
  char  Exe_Code3    [RSIZ_EXECODE3       ];   
  char  Exe_Amt3     [RSIZ_EXE_AMT3       ];   
  char  Exe_Code4    [RSIZ_EXECODE4       ];   
  char  Exe_Amt4     [RSIZ_EXE_AMT4       ];   
  char  Filler2      [RSIZ_FILLER2        ];   
  char  Senior_Cit   [RSIZ_SENIOR_CIT     ];   
  char  S_StrNum     [RSIZ_S_STRNUM       ];   
  char  S_Unit       [RSIZ_S_UNIT         ];   
  char  S_StrDir     [RSIZ_S_STRDIR       ];   
  char  S_StrName    [RSIZ_S_STRNAME      ];   
  char  S_StrType    [RSIZ_S_STRTYPE      ];   
  char  S_Zip        [RSIZ_S_ZIP          ];   
  char  Filler3      [RSIZ_FILLER3        ];   
  char  Name1        [RSIZ_NAME1          ];   
  char  Name2        [RSIZ_NAME2          ];   
  char  M_Addr1      [RSIZ_MAILADDR1      ]; 
  char  M_Addr2      [RSIZ_MAILADDR2      ]; 
  char  M_Addr3      [RSIZ_MAILADDR3      ]; 
  char  M_Addr4      [RSIZ_MAILADDR4      ]; 
} MRN_ROLL;

#define  COFF_FMT_APN            1
#define  COFF_S_ADDR            13
#define  COFF_S_CITY            64
#define  COFF_USECODE           69
#define  COFF_NUM_OF_UNITS      74
#define  COFF_LOT_SQFT          81
#define  COFF_DECK_SQFT         92
#define  COFF_FIREPLACES       104
#define  COFF_HEAT_TYPE        111
#define  COFF_SBE_CLASS        137
#define  COFF_YRBLT            144
#define  COFF_EFFYR            152
#define  COFF_LIV_AREA         160
#define  COFF_BEDS             172
#define  COFF_BATHS            178
#define  COFF_UNFINISHED_SQFT  184
#define  COFF_GARAGE_SQFT      196
#define  COFF_POOL_SQFT        208
#define  COFF_END_DATE         220
#define  COFF_ECO_UNIT         231
#define  COFF_TRA              240

#define  CSIZ_FMT_APN          11
#define  CSIZ_S_ADDR           50
#define  CSIZ_S_CITY           4 
#define  CSIZ_USECODE          4 
#define  CSIZ_NUM_OF_UNITS     6 
#define  CSIZ_LOT_SQFT         10
#define  CSIZ_DECK_SQFT        11
#define  CSIZ_FIREPLACES       6 
#define  CSIZ_HEAT_TYPE        25
#define  CSIZ_SBE_CLASS        6 
#define  CSIZ_YRBLT            7 
#define  CSIZ_EFFYR            7 
#define  CSIZ_LIV_AREA         11
#define  CSIZ_BEDS             5 
#define  CSIZ_BATHS            5 
#define  CSIZ_UNFINISHED_SQFT  11
#define  CSIZ_GARAGE_SQFT      11
#define  CSIZ_POOL_SQFT        11
#define  CSIZ_END_DATE         10
#define  CSIZ_ECO_UNIT         8 
#define  CSIZ_TRA              8 

#define  C_FMT_APN             0
#define  C_S_ADDR              1
#define  C_S_CITY              2
#define  C_USECODE             3
#define  C_NUM_OF_UNITS        4
#define  C_LOT_SQFT            5
#define  C_DECK_SQFT           6
#define  C_FIREPLACES          7
#define  C_HEAT_TYPE           8
#define  C_SBE_CLASS           9
#define  C_YRBLT               10
#define  C_EFFYR               11
#define  C_LIV_AREA            12
#define  C_BEDS                13
#define  C_BATHS               14
#define  C_UNFINISHED_SQFT     15
#define  C_GARAGE_SQFT         16
#define  C_POOL_SQFT           17
#define  C_END_DATE            18
#define  C_ECO_UNIT            19
#define  C_TRA                 20

typedef struct _tMrnChar
{  // 250-byte: input record is TAB-delimited
   char  Fmt_Apn        [CSIZ_FMT_APN        +1];
   char  S_Addr         [CSIZ_S_ADDR         +1];
   char  S_City         [CSIZ_S_CITY         +1];
   char  UseCode        [CSIZ_USECODE        +1];
   char  Num_Of_Units   [CSIZ_NUM_OF_UNITS   +1];
   char  Lot_Sqft       [CSIZ_LOT_SQFT       +1];
   char  Deck_Sqft      [CSIZ_DECK_SQFT      +1];
   char  Fireplaces     [CSIZ_FIREPLACES     +1];
   char  Heat_Type      [CSIZ_HEAT_TYPE      +1];
   char  Sbe_Class      [CSIZ_SBE_CLASS      +1];
   char  YrBlt          [CSIZ_YRBLT          +1];
   char  YrEff          [CSIZ_EFFYR          +1];
   char  Liv_Area       [CSIZ_LIV_AREA       +1];
   char  Beds           [CSIZ_BEDS           +1];
   char  Baths          [CSIZ_BATHS          +1];
   char  Unfinished_Sqft[CSIZ_UNFINISHED_SQFT+1];
   char  Garage_Sqft    [CSIZ_GARAGE_SQFT    +1];
   char  Pool_Sqft      [CSIZ_POOL_SQFT      +1];
   char  End_Date       [CSIZ_END_DATE       +1];
   char  Eco_Unit       [CSIZ_ECO_UNIT       +1];
   char  Tra            [CSIZ_TRA            +1];
   char  CrLf[2];
} MRN_CHAR;

#define  SOFF_CONF_FLAG         1
#define  SOFF_FMT_APN           15
#define  SOFF_DEEDNUM           31
#define  SOFF_RECDATE           61
#define  SOFF_USECODE           63
#define  SOFF_NUM_OF_UNITS      70
#define  SOFF_SBE_CLASS         77
#define  SOFF_LIV_AREA          95
#define  SOFF_YRBLD             113
#define  SOFF_EFFYR             122
#define  SOFF_LOT_SQFT          131
#define  SOFF_BEDS              149
#define  SOFF_BATHS             158
#define  SOFF_HEAT_TYPE         167
#define  SOFF_POOL_SQFT         193
#define  SOFF_GARAGE_SQFT       202
#define  SOFF_FIREPLACES        211
#define  SOFF_DECK_SQFT         221
#define  SOFF_UNFINISHED_SQFT   230
#define  SOFF_CREATE_DATE       248
#define  SOFF_GRANTOR           264
#define  SOFF_GRANTEE           365
#define  SOFF_SALE_PRICE        466
#define  SOFF_STAMP_AMT         484
#define  SOFF_1ST_TD_AMT        502
#define  SOFF_1ST_LENDER_TYPE   520
#define  SOFF_2ND_TD_AMT        571
#define  SOFF_2ND_LENDER_TYPE   589
#define  SOFF_3RD_TD_AMT        640
#define  SOFF_3RD_LENDER_TYPE   658
#define  SOFF_WOP               709
#define  SOFF_S_ADDR            718
#define  SOFF_S_CITY            769

#define  SSIZ_CONF_FLAG         13 
#define  SSIZ_FMT_APN           15 
#define  SSIZ_DEEDNUM           15 
#define  SSIZ_RECDATE           15 
#define  SSIZ_USECODE           6  
#define  SSIZ_NUM_OF_UNITS      6  
#define  SSIZ_SBE_CLASS         17 
#define  SSIZ_LIV_AREA          17 
#define  SSIZ_YRBLD             8  
#define  SSIZ_EFFYR             8  
#define  SSIZ_LOT_SQFT          17 
#define  SSIZ_BEDS              8  
#define  SSIZ_BATHS             8  
#define  SSIZ_HEAT_TYPE         25 
#define  SSIZ_POOL_SQFT         8  
#define  SSIZ_GARAGE_SQFT       8  
#define  SSIZ_FIREPLACES        9  
#define  SSIZ_DECK_SQFT         8  
#define  SSIZ_UNFINISHED_SQFT   17 
#define  SSIZ_CREATE_DATE       15 
#define  SSIZ_GRANTOR           100
#define  SSIZ_GRANTEE           100
#define  SSIZ_SALE_PRICE        17 
#define  SSIZ_STAMP_AMT         17 
#define  SSIZ_TD_AMT            17 
#define  SSIZ_LENDER            50 
#define  SSIZ_WOP               8  
#define  SSIZ_S_ADDR            50 
#define  SSIZ_S_CITY            20 

typedef struct t_MrnSale
{  // 791-byte: input record is TAB-delimited
   char  Conf_Flag      [SSIZ_CONF_FLAG      +1]; 
   char  Fmt_Apn        [SSIZ_FMT_APN        +1]; 
   char  DocNum         [SSIZ_DEEDNUM        +1]; 
   char  RecDate        [SSIZ_RECDATE        +1]; 
   char  Usecode        [SSIZ_USECODE        +1]; 
   char  Num_Of_Units   [SSIZ_NUM_OF_UNITS   +1]; 
   char  Sbe_Class      [SSIZ_SBE_CLASS      +1]; 
   char  Liv_Area       [SSIZ_LIV_AREA       +1]; 
   char  Yrbld          [SSIZ_YRBLD          +1]; 
   char  Effyr          [SSIZ_EFFYR          +1]; 
   char  Lot_Sqft       [SSIZ_LOT_SQFT       +1]; 
   char  Beds           [SSIZ_BEDS           +1]; 
   char  Baths          [SSIZ_BATHS          +1]; 
   char  Heat_Type      [SSIZ_HEAT_TYPE      +1]; 
   char  Pool_Sqft      [SSIZ_POOL_SQFT      +1]; 
   char  Garage_Sqft    [SSIZ_GARAGE_SQFT    +1]; 
   char  Fireplaces     [SSIZ_FIREPLACES     +1]; 
   char  Deck_Sqft      [SSIZ_DECK_SQFT      +1]; 
   char  Unfinished_Sqft[SSIZ_UNFINISHED_SQFT+1]; 
   char  Create_Date    [SSIZ_CREATE_DATE    +1]; 
   char  Grantor        [SSIZ_GRANTOR        +1]; 
   char  Grantee        [SSIZ_GRANTEE        +1]; 
   char  SalePrice      [SSIZ_SALE_PRICE     +1]; 
   char  StampAmt       [SSIZ_STAMP_AMT      +1]; 
   char  Td1_Amt        [SSIZ_TD_AMT         +1]; 
   char  Td1_Lender     [SSIZ_LENDER         +1]; 
   char  Td2_Amt        [SSIZ_TD_AMT         +1]; 
   char  Td2_Lender     [SSIZ_LENDER         +1]; 
   char  Td3_Amt        [SSIZ_TD_AMT         +1]; 
   char  Td3_Lender     [SSIZ_LENDER         +1]; 
   char  Wop            [SSIZ_WOP            +1]; 
   char  S_Addr         [SSIZ_S_ADDR         +1]; 
   char  S_City         [SSIZ_S_CITY         +1]; 
   char  CrLf[2];
} MRN_SALE;

#define  SSIZ_APN               8 
#define  SSIZ_DOCNUM            12
#define  SSIZ_DATE              8
#define  SSIZ_AMOUNT            10
#define  SSIZ_T_LENDER          15
#define  SSIZ_NAME              69


typedef struct t_MrnCSale
{  // 290-byte
   char  Apn            [SSIZ_APN];       //   1
   char  DocNum         [SSIZ_DOCNUM];    //   9  
   char  RecDate        [SSIZ_DATE];      //  21
   char  OrgRecNum      [SSIZ_DOCNUM];    //  29
   char  StampAmt       [SSIZ_AMOUNT];    //  41
   char  SalePrice      [SSIZ_AMOUNT];    //  51
   char  Conf_Flag;     // Y/N            //  61
   char  Wop;                             //  62
   char  Td1_Amt        [SSIZ_AMOUNT];    //  63
   char  Td1_Lender     [SSIZ_T_LENDER];  //  73
   char  Td2_Amt        [SSIZ_AMOUNT];    //  88
   char  Td2_Lender     [SSIZ_T_LENDER];  //  98
   char  Td3_Amt        [SSIZ_AMOUNT];    // 113
   char  Td3_Lender     [SSIZ_T_LENDER];  // 123
   char  Grantor        [SSIZ_NAME];      // 138
   char  Grantee        [SSIZ_NAME];      // 207
   char  Create_Date    [SSIZ_DATE];      // 276
   char  filler[5];                       // 284
   char  CrLf[2];
} MRN_CSAL;

typedef struct _tMrnUseXref
{
   char  CntyUse[4];
   char  StdUse[4];
} MRN_USEXREF;

IDX_TBL4 MRN_Exemption[] = 
{
   "HO", "H", 2,1,
   "CH", "C", 2,1,
   "CM", "E", 2,1,
   "CO", "U", 2,1,
   "DL", "D", 2,1,
   "DV", "D", 2,1,
   "HS", "I", 2,1,
   "LB", "L", 2,1,
   "MU", "M", 2,1,
   "PS", "P", 2,1,
   "PL", "D", 2,1,
   "PV", "D", 2,1,
   "RE", "R", 2,1,
   "VT", "V", 2,1,
   "WC", "W", 2,1,
   "WS", "S", 2,1,
   "AT", "X", 2,1,      // WORK OF ART
   "BX", "X", 2,1,      // BUILDER'S EXCLUSION
   "EX", "X", 2,1,      // EXHIBITION
   "MA", "X", 2,1,      // MILLS ACT EXCLUSION
   "SR", "X", 2,1,      // SENIOR CITIZEN PROPERTY TAX POSTPONEMENT
   "","",0,0
};

#endif
